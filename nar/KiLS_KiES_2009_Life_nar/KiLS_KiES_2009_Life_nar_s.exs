<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID0F83BD87-5F21-F8A5-5691-100C3A5F15FA">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KiLS_KiES_2009_Life_nar</transcription-name>
         <referenced-file url="KiLS_KiES_2009_Life_nar.wav" />
         <referenced-file url="KiLS_KiES_2009_Life_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KiLS_KiES_2009_Life_nar\KiLS_KiES_2009_Life_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">617</ud-information>
            <ud-information attribute-name="# HIAT:w">445</ud-information>
            <ud-information attribute-name="# e">450</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">6</ud-information>
            <ud-information attribute-name="# HIAT:u">53</ud-information>
            <ud-information attribute-name="# sc">26</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KiLS">
            <abbreviation>KiLS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KiES">
            <abbreviation>KiES</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.532" type="appl" />
         <tli id="T2" time="1.4492" type="appl" />
         <tli id="T3" time="2.3664" type="appl" />
         <tli id="T4" time="3.2836" type="appl" />
         <tli id="T5" time="4.2008" type="appl" />
         <tli id="T6" time="5.606660460738452" />
         <tli id="T7" time="5.9125000000000005" type="appl" />
         <tli id="T8" time="6.707" type="appl" />
         <tli id="T9" time="7.5015" type="appl" />
         <tli id="T10" time="8.296" type="appl" />
         <tli id="T11" time="9.090499999999999" type="appl" />
         <tli id="T12" time="10.299988599097395" />
         <tli id="T13" time="10.756142857142857" type="appl" />
         <tli id="T14" time="11.627285714285714" type="appl" />
         <tli id="T15" time="12.498428571428573" type="appl" />
         <tli id="T16" time="13.36957142857143" type="appl" />
         <tli id="T17" time="14.240714285714287" type="appl" />
         <tli id="T18" time="15.111857142857144" type="appl" />
         <tli id="T19" time="15.983" type="appl" />
         <tli id="T20" time="16.979" type="appl" />
         <tli id="T21" time="17.967" type="appl" />
         <tli id="T22" time="17.975" type="appl" />
         <tli id="T23" time="19.557333495510605" />
         <tli id="T24" time="19.572" type="appl" />
         <tli id="T25" time="20.2455" type="appl" />
         <tli id="T26" time="20.459" type="appl" />
         <tli id="T27" time="20.919" type="appl" />
         <tli id="T28" time="21.52664283914918" />
         <tli id="T29" time="21.6169991844476" />
         <tli id="T30" time="22.014666387504917" />
         <tli id="T31" time="22.018444444444444" type="appl" />
         <tli id="T32" time="22.499888888888886" type="appl" />
         <tli id="T33" time="22.981333333333332" type="appl" />
         <tli id="T34" time="23.462777777777777" type="appl" />
         <tli id="T35" time="23.944222222222223" type="appl" />
         <tli id="T36" time="24.425666666666665" type="appl" />
         <tli id="T37" time="24.90711111111111" type="appl" />
         <tli id="T38" time="25.388555555555556" type="appl" />
         <tli id="T39" time="25.869999999999997" type="appl" />
         <tli id="T40" time="26.351444444444443" type="appl" />
         <tli id="T41" time="26.83288888888889" type="appl" />
         <tli id="T42" time="27.31433333333333" type="appl" />
         <tli id="T43" time="27.795777777777776" type="appl" />
         <tli id="T44" time="28.27722222222222" type="appl" />
         <tli id="T45" time="28.758666666666667" type="appl" />
         <tli id="T46" time="29.240111111111112" type="appl" />
         <tli id="T47" time="29.721555555555554" type="appl" />
         <tli id="T48" time="30.449666816554686" />
         <tli id="T49" time="30.922666666666668" type="appl" />
         <tli id="T50" time="31.642333333333333" type="appl" />
         <tli id="T51" time="33.93996243236559" />
         <tli id="T52" time="33.95231454951613" type="intp" />
         <tli id="T53" time="33.964666666666666" type="appl" />
         <tli id="T54" time="34.602334746070625" />
         <tli id="T55" time="34.78599990735775" />
         <tli id="T56" time="35.23000006694231" />
         <tli id="T57" time="35.232666079616344" />
         <tli id="T58" time="35.98662683361018" />
         <tli id="T59" time="35.99266849358942" />
         <tli id="T60" time="36.5" type="appl" />
         <tli id="T61" time="36.741000998604754" />
         <tli id="T62" time="37.344333664118224" />
         <tli id="T63" time="37.75409090909091" type="appl" />
         <tli id="T64" time="38.09718181818182" type="appl" />
         <tli id="T65" time="38.44027272727273" type="appl" />
         <tli id="T66" time="38.78336363636364" type="appl" />
         <tli id="T67" time="39.12645454545455" type="appl" />
         <tli id="T68" time="39.469545454545454" type="appl" />
         <tli id="T69" time="39.812636363636365" type="appl" />
         <tli id="T70" time="40.155727272727276" type="appl" />
         <tli id="T71" time="40.49881818181819" type="appl" />
         <tli id="T72" time="40.84190909090909" type="appl" />
         <tli id="T73" time="41.19833330443394" />
         <tli id="T74" time="41.51864285714286" type="appl" />
         <tli id="T75" time="41.85228571428571" type="appl" />
         <tli id="T76" time="42.185928571428576" type="appl" />
         <tli id="T77" time="42.51957142857143" type="appl" />
         <tli id="T78" time="42.85321428571429" type="appl" />
         <tli id="T79" time="43.18685714285714" type="appl" />
         <tli id="T80" time="43.5205" type="appl" />
         <tli id="T81" time="43.85414285714286" type="appl" />
         <tli id="T82" time="44.18778571428572" type="appl" />
         <tli id="T83" time="44.52142857142857" type="appl" />
         <tli id="T84" time="44.85507142857143" type="appl" />
         <tli id="T85" time="45.18871428571428" type="appl" />
         <tli id="T86" time="45.522357142857146" type="appl" />
         <tli id="T87" time="46.539948485630354" />
         <tli id="T88" time="46.607" type="appl" />
         <tli id="T89" time="47.358000000000004" type="appl" />
         <tli id="T90" time="48.109" type="appl" />
         <tli id="T91" time="48.86" type="appl" />
         <tli id="T92" time="49.611000000000004" type="appl" />
         <tli id="T93" time="50.362" type="appl" />
         <tli id="T94" time="51.113" type="appl" />
         <tli id="T95" time="51.864000000000004" type="appl" />
         <tli id="T96" time="52.615" type="appl" />
         <tli id="T97" time="53.366" type="appl" />
         <tli id="T98" time="54.117000000000004" type="appl" />
         <tli id="T99" time="55.41327199721523" />
         <tli id="T100" time="55.50757142857143" type="appl" />
         <tli id="T101" time="56.14714285714286" type="appl" />
         <tli id="T102" time="56.78671428571429" type="appl" />
         <tli id="T103" time="57.42628571428571" type="appl" />
         <tli id="T104" time="58.06585714285714" type="appl" />
         <tli id="T105" time="58.70542857142857" type="appl" />
         <tli id="T106" time="59.38499937180322" />
         <tli id="T107" time="59.858" type="appl" />
         <tli id="T108" time="60.371" type="appl" />
         <tli id="T109" time="60.884" type="appl" />
         <tli id="T110" time="61.397" type="appl" />
         <tli id="T111" time="61.91" type="appl" />
         <tli id="T112" time="62.423" type="appl" />
         <tli id="T113" time="62.936" type="appl" />
         <tli id="T114" time="63.449" type="appl" />
         <tli id="T115" time="63.962" type="appl" />
         <tli id="T116" time="64.475" type="appl" />
         <tli id="T117" time="64.988" type="appl" />
         <tli id="T118" time="65.501" type="appl" />
         <tli id="T119" time="66.014" type="appl" />
         <tli id="T120" time="66.527" type="appl" />
         <tli id="T121" time="67.04" type="appl" />
         <tli id="T122" time="67.553" type="appl" />
         <tli id="T123" time="68.066" type="appl" />
         <tli id="T124" time="68.57900000000001" type="appl" />
         <tli id="T125" time="69.092" type="appl" />
         <tli id="T126" time="69.588" type="appl" />
         <tli id="T127" time="69.70500096958186" />
         <tli id="T128" time="70.26166666666666" type="appl" />
         <tli id="T129" time="70.93533333333333" type="appl" />
         <tli id="T130" time="70.95992145552923" />
         <tli id="T131" time="71.76600129245824" />
         <tli id="T132" time="72.23641666666667" type="appl" />
         <tli id="T133" time="72.84683333333334" type="appl" />
         <tli id="T134" time="73.45725" type="appl" />
         <tli id="T135" time="74.06766666666667" type="appl" />
         <tli id="T136" time="74.67808333333333" type="appl" />
         <tli id="T137" time="75.2885" type="appl" />
         <tli id="T138" time="75.89891666666666" type="appl" />
         <tli id="T139" time="76.50933333333333" type="appl" />
         <tli id="T140" time="78.11324687095414" />
         <tli id="T141" time="78.22691510214374" type="intp" />
         <tli id="T142" time="78.34058333333333" type="appl" />
         <tli id="T143" time="78.786" type="appl" />
         <tli id="T144" time="78.951" type="appl" />
         <tli id="T145" time="79.0732" type="appl" />
         <tli id="T146" time="80.16657793148619" />
         <tli id="T150" time="80.225" type="appl" />
         <tli id="T151" time="80.6987" type="appl" />
         <tli id="T152" time="81.1724" type="appl" />
         <tli id="T153" time="81.64609999999999" type="appl" />
         <tli id="T154" time="82.1198" type="appl" />
         <tli id="T155" time="82.5935" type="appl" />
         <tli id="T156" time="83.0672" type="appl" />
         <tli id="T157" time="83.5409" type="appl" />
         <tli id="T158" time="84.0146" type="appl" />
         <tli id="T159" time="84.48830000000001" type="appl" />
         <tli id="T160" time="85.00866892601509" />
         <tli id="T161" time="85.42275000000001" type="appl" />
         <tli id="T162" time="85.8835" type="appl" />
         <tli id="T163" time="86.34425" type="appl" />
         <tli id="T164" time="86.805" type="appl" />
         <tli id="T165" time="87.26575" type="appl" />
         <tli id="T166" time="87.7265" type="appl" />
         <tli id="T167" time="88.18725" type="appl" />
         <tli id="T168" time="88.648" type="appl" />
         <tli id="T169" time="89.10875" type="appl" />
         <tli id="T170" time="89.5695" type="appl" />
         <tli id="T171" time="90.03025" type="appl" />
         <tli id="T172" time="90.57989973847009" />
         <tli id="T173" time="91.115" type="appl" />
         <tli id="T174" time="91.739" type="appl" />
         <tli id="T175" time="92.363" type="appl" />
         <tli id="T176" time="92.987" type="appl" />
         <tli id="T177" time="93.611" type="appl" />
         <tli id="T178" time="94.235" type="appl" />
         <tli id="T179" time="94.85900000000001" type="appl" />
         <tli id="T180" time="95.9896658858979" />
         <tli id="T181" time="96.05144444444444" type="appl" />
         <tli id="T182" time="96.6198888888889" type="appl" />
         <tli id="T183" time="97.18833333333333" type="appl" />
         <tli id="T184" time="97.75677777777778" type="appl" />
         <tli id="T185" time="98.32522222222222" type="appl" />
         <tli id="T186" time="98.89366666666668" type="appl" />
         <tli id="T187" time="99.46211111111111" type="appl" />
         <tli id="T188" time="100.03055555555557" type="appl" />
         <tli id="T189" time="100.73233641755715" />
         <tli id="T190" time="101.46975" type="appl" />
         <tli id="T191" time="102.3405" type="appl" />
         <tli id="T192" time="103.21125" type="appl" />
         <tli id="T193" time="104.082" type="appl" />
         <tli id="T194" time="104.95275" type="appl" />
         <tli id="T195" time="105.8235" type="appl" />
         <tli id="T196" time="106.69425" type="appl" />
         <tli id="T197" time="107.67166467401897" />
         <tli id="T198" time="108.00881818181819" type="appl" />
         <tli id="T199" time="108.45263636363636" type="appl" />
         <tli id="T200" time="108.89645454545455" type="appl" />
         <tli id="T201" time="109.34027272727273" type="appl" />
         <tli id="T202" time="109.7840909090909" type="appl" />
         <tli id="T203" time="110.2279090909091" type="appl" />
         <tli id="T204" time="110.67172727272727" type="appl" />
         <tli id="T205" time="111.11554545454545" type="appl" />
         <tli id="T206" time="111.55936363636364" type="appl" />
         <tli id="T207" time="112.00318181818182" type="appl" />
         <tli id="T208" time="112.7803308943131" />
         <tli id="T209" time="113.03544444444445" type="appl" />
         <tli id="T210" time="113.62388888888889" type="appl" />
         <tli id="T211" time="114.21233333333333" type="appl" />
         <tli id="T212" time="114.80077777777778" type="appl" />
         <tli id="T213" time="115.38922222222222" type="appl" />
         <tli id="T214" time="115.97766666666666" type="appl" />
         <tli id="T215" time="116.56611111111111" type="appl" />
         <tli id="T216" time="117.15455555555555" type="appl" />
         <tli id="T217" time="117.78966128706055" />
         <tli id="T218" time="118.48641176470588" type="appl" />
         <tli id="T219" time="119.22982352941176" type="appl" />
         <tli id="T220" time="119.97323529411764" type="appl" />
         <tli id="T221" time="120.71664705882353" type="appl" />
         <tli id="T222" time="121.46005882352941" type="appl" />
         <tli id="T223" time="122.20347058823529" type="appl" />
         <tli id="T224" time="122.94688235294117" type="appl" />
         <tli id="T225" time="123.69029411764706" type="appl" />
         <tli id="T226" time="124.43370588235294" type="appl" />
         <tli id="T227" time="125.17711764705882" type="appl" />
         <tli id="T228" time="125.9205294117647" type="appl" />
         <tli id="T229" time="126.66394117647059" type="appl" />
         <tli id="T230" time="127.40735294117647" type="appl" />
         <tli id="T231" time="128.15076470588235" type="appl" />
         <tli id="T232" time="128.89417647058823" type="appl" />
         <tli id="T233" time="129.63758823529412" type="appl" />
         <tli id="T234" time="130.5810012951852" />
         <tli id="T235" time="130.8262" type="appl" />
         <tli id="T236" time="131.2714" type="appl" />
         <tli id="T237" time="131.7166" type="appl" />
         <tli id="T238" time="132.1618" type="appl" />
         <tli id="T239" time="133.09366778478574" />
         <tli id="T240" time="133.40663636363635" type="appl" />
         <tli id="T241" time="134.20627272727273" type="appl" />
         <tli id="T242" time="135.00590909090909" type="appl" />
         <tli id="T243" time="135.80554545454544" type="appl" />
         <tli id="T244" time="136.60518181818182" type="appl" />
         <tli id="T245" time="137.40481818181817" type="appl" />
         <tli id="T246" time="138.20445454545455" type="appl" />
         <tli id="T247" time="139.0040909090909" type="appl" />
         <tli id="T248" time="139.80372727272726" type="appl" />
         <tli id="T249" time="140.60336363636364" type="appl" />
         <tli id="T250" time="141.61632762201498" />
         <tli id="T251" time="141.77923076923076" type="appl" />
         <tli id="T252" time="142.15546153846154" type="appl" />
         <tli id="T253" time="142.5316923076923" type="appl" />
         <tli id="T254" time="142.90792307692308" type="appl" />
         <tli id="T255" time="143.28415384615386" type="appl" />
         <tli id="T256" time="143.66038461538463" type="appl" />
         <tli id="T257" time="144.03661538461537" type="appl" />
         <tli id="T258" time="144.41284615384615" type="appl" />
         <tli id="T259" time="144.78907692307692" type="appl" />
         <tli id="T260" time="145.1653076923077" type="appl" />
         <tli id="T261" time="145.54153846153847" type="appl" />
         <tli id="T262" time="145.91776923076924" type="appl" />
         <tli id="T263" time="146.5473377889539" />
         <tli id="T264" time="146.73575" type="appl" />
         <tli id="T265" time="147.1775" type="appl" />
         <tli id="T266" time="147.61925000000002" type="appl" />
         <tli id="T267" time="148.061" type="appl" />
         <tli id="T268" time="148.50275" type="appl" />
         <tli id="T269" time="148.9445" type="appl" />
         <tli id="T270" time="149.38625000000002" type="appl" />
         <tli id="T271" time="150.0079980209011" />
         <tli id="T272" time="150.45433333333335" type="appl" />
         <tli id="T273" time="151.08066666666667" type="appl" />
         <tli id="T274" time="151.707" type="appl" />
         <tli id="T275" time="152.33333333333334" type="appl" />
         <tli id="T276" time="152.9596666666667" type="appl" />
         <tli id="T277" time="153.6193351693996" />
         <tli id="T278" time="154.4326676024675" />
         <tli id="T279" time="155.268" type="appl" />
         <tli id="T280" time="156.01" type="appl" />
         <tli id="T281" time="156.752" type="appl" />
         <tli id="T282" time="157.94649183806823" />
         <tli id="T283" time="158.31285714285715" type="appl" />
         <tli id="T284" time="159.13171428571428" type="appl" />
         <tli id="T285" time="159.95057142857144" type="appl" />
         <tli id="T286" time="160.76942857142856" type="appl" />
         <tli id="T287" time="161.58828571428572" type="appl" />
         <tli id="T288" time="162.40714285714284" type="appl" />
         <tli id="T289" time="163.36600406845824" />
         <tli id="T290" time="163.85236363636363" type="appl" />
         <tli id="T291" time="164.47872727272727" type="appl" />
         <tli id="T292" time="165.1050909090909" type="appl" />
         <tli id="T293" time="165.73145454545454" type="appl" />
         <tli id="T294" time="166.35781818181817" type="appl" />
         <tli id="T295" time="166.98418181818184" type="appl" />
         <tli id="T296" time="167.61054545454547" type="appl" />
         <tli id="T297" time="168.2369090909091" type="appl" />
         <tli id="T298" time="168.86327272727274" type="appl" />
         <tli id="T299" time="169.48963636363638" type="appl" />
         <tli id="T300" time="170.1693298712925" />
         <tli id="T301" time="170.7464117647059" type="appl" />
         <tli id="T302" time="171.37682352941178" type="appl" />
         <tli id="T303" time="172.00723529411766" type="appl" />
         <tli id="T304" time="172.63764705882355" type="appl" />
         <tli id="T305" time="173.26805882352943" type="appl" />
         <tli id="T306" time="173.8984705882353" type="appl" />
         <tli id="T307" time="174.5288823529412" type="appl" />
         <tli id="T308" time="175.15929411764708" type="appl" />
         <tli id="T309" time="175.78970588235293" type="appl" />
         <tli id="T310" time="176.42011764705882" type="appl" />
         <tli id="T311" time="177.0505294117647" type="appl" />
         <tli id="T312" time="177.68094117647058" type="appl" />
         <tli id="T313" time="178.31135294117647" type="appl" />
         <tli id="T314" time="178.94176470588235" type="appl" />
         <tli id="T315" time="179.57217647058823" type="appl" />
         <tli id="T316" time="180.20258823529412" type="appl" />
         <tli id="T317" time="181.07966935730167" />
         <tli id="T318" time="181.4173076923077" type="appl" />
         <tli id="T319" time="182.00161538461538" type="appl" />
         <tli id="T320" time="182.58592307692308" type="appl" />
         <tli id="T321" time="183.17023076923076" type="appl" />
         <tli id="T322" time="183.75453846153846" type="appl" />
         <tli id="T323" time="184.33884615384616" type="appl" />
         <tli id="T324" time="184.92315384615384" type="appl" />
         <tli id="T325" time="185.50746153846154" type="appl" />
         <tli id="T326" time="186.09176923076924" type="appl" />
         <tli id="T327" time="186.67607692307692" type="appl" />
         <tli id="T328" time="187.26038461538462" type="appl" />
         <tli id="T329" time="187.8446923076923" type="appl" />
         <tli id="T330" time="188.74233014645094" />
         <tli id="T331" time="189.0135" type="appl" />
         <tli id="T332" time="189.598" type="appl" />
         <tli id="T333" time="190.1825" type="appl" />
         <tli id="T334" time="190.767" type="appl" />
         <tli id="T335" time="191.3515" type="appl" />
         <tli id="T336" time="191.936" type="appl" />
         <tli id="T337" time="192.5205" type="appl" />
         <tli id="T338" time="193.105" type="appl" />
         <tli id="T339" time="193.6895" type="appl" />
         <tli id="T340" time="194.274" type="appl" />
         <tli id="T341" time="194.8585" type="appl" />
         <tli id="T342" time="195.44299999999998" type="appl" />
         <tli id="T343" time="196.0275" type="appl" />
         <tli id="T344" time="197.0453287684865" />
         <tli id="T345" time="197.647" type="appl" />
         <tli id="T346" time="198.682" type="appl" />
         <tli id="T347" time="199.71699999999998" type="appl" />
         <tli id="T348" time="200.752" type="appl" />
         <tli id="T349" time="201.787" type="appl" />
         <tli id="T350" time="202.822" type="appl" />
         <tli id="T351" time="203.857" type="appl" />
         <tli id="T352" time="207.1731040165376" />
         <tli id="T353" time="207.21125982054886" type="intp" />
         <tli id="T354" time="207.24941562456013" type="intp" />
         <tli id="T355" time="207.28757142857143" type="appl" />
         <tli id="T356" time="208.08609523809523" type="appl" />
         <tli id="T357" time="208.88461904761905" type="appl" />
         <tli id="T358" time="209.68314285714285" type="appl" />
         <tli id="T359" time="210.48166666666665" type="appl" />
         <tli id="T360" time="211.28019047619048" type="appl" />
         <tli id="T361" time="212.07871428571428" type="appl" />
         <tli id="T362" time="212.87723809523808" type="appl" />
         <tli id="T363" time="213.6757619047619" type="appl" />
         <tli id="T364" time="214.4742857142857" type="appl" />
         <tli id="T365" time="215.2728095238095" type="appl" />
         <tli id="T366" time="216.07133333333334" type="appl" />
         <tli id="T367" time="216.86985714285714" type="appl" />
         <tli id="T368" time="217.66838095238094" type="appl" />
         <tli id="T369" time="218.46690476190477" type="appl" />
         <tli id="T370" time="219.26542857142857" type="appl" />
         <tli id="T371" time="220.06395238095237" type="appl" />
         <tli id="T372" time="220.8624761904762" type="appl" />
         <tli id="T373" time="222.29308728045532" />
         <tli id="T374" time="222.596376973561" type="intp" />
         <tli id="T375" time="222.89966666666666" type="appl" />
         <tli id="T376" time="223.519" type="appl" />
         <tli id="T377" time="224.13833333333335" type="appl" />
         <tli id="T378" time="224.75766666666667" type="appl" />
         <tli id="T379" time="225.377" type="appl" />
         <tli id="T380" time="225.99633333333335" type="appl" />
         <tli id="T381" time="226.61566666666667" type="appl" />
         <tli id="T382" time="227.235" type="appl" />
         <tli id="T383" time="228.1314" type="appl" />
         <tli id="T384" time="229.0278" type="appl" />
         <tli id="T385" time="229.9242" type="appl" />
         <tli id="T386" time="230.8206" type="appl" />
         <tli id="T387" time="231.94640992867087" />
         <tli id="T388" time="232.61333333333334" type="appl" />
         <tli id="T389" time="233.50966666666667" type="appl" />
         <tli id="T390" time="234.5526570438551" />
         <tli id="T391" time="234.95888" type="appl" />
         <tli id="T392" time="235.51176" type="appl" />
         <tli id="T393" time="236.06464" type="appl" />
         <tli id="T394" time="236.61752" type="appl" />
         <tli id="T395" time="237.1704" type="appl" />
         <tli id="T396" time="237.72328000000002" type="appl" />
         <tli id="T397" time="238.27616" type="appl" />
         <tli id="T398" time="238.82904000000002" type="appl" />
         <tli id="T399" time="239.38192" type="appl" />
         <tli id="T400" time="239.9348" type="appl" />
         <tli id="T401" time="240.48768" type="appl" />
         <tli id="T402" time="241.04056" type="appl" />
         <tli id="T403" time="241.59344000000002" type="appl" />
         <tli id="T404" time="242.14632" type="appl" />
         <tli id="T405" time="242.69920000000002" type="appl" />
         <tli id="T406" time="243.25208" type="appl" />
         <tli id="T407" time="243.80496" type="appl" />
         <tli id="T408" time="244.35784" type="appl" />
         <tli id="T409" time="244.91072" type="appl" />
         <tli id="T410" time="245.4636" type="appl" />
         <tli id="T411" time="246.01648" type="appl" />
         <tli id="T412" time="246.56936000000002" type="appl" />
         <tli id="T413" time="247.12224" type="appl" />
         <tli id="T414" time="247.67512000000002" type="appl" />
         <tli id="T415" time="248.66133934383333" />
         <tli id="T416" time="248.760625" type="appl" />
         <tli id="T417" time="249.29325" type="appl" />
         <tli id="T418" time="249.825875" type="appl" />
         <tli id="T419" time="250.3585" type="appl" />
         <tli id="T420" time="250.89112500000002" type="appl" />
         <tli id="T421" time="251.42375" type="appl" />
         <tli id="T422" time="251.956375" type="appl" />
         <tli id="T423" time="253.10638650701077" />
         <tli id="T424" time="253.16159325350537" type="intp" />
         <tli id="T425" time="253.2168" type="appl" />
         <tli id="T426" time="253.5807" type="appl" />
         <tli id="T427" time="253.9446" type="appl" />
         <tli id="T428" time="254.30849999999998" type="appl" />
         <tli id="T429" time="254.67239999999998" type="appl" />
         <tli id="T430" time="255.03629999999998" type="appl" />
         <tli id="T431" time="255.40019999999998" type="appl" />
         <tli id="T432" time="255.76409999999998" type="appl" />
         <tli id="T433" time="256.128" type="appl" />
         <tli id="T434" time="256.7500909090909" type="appl" />
         <tli id="T435" time="257.3721818181818" type="appl" />
         <tli id="T436" time="257.99427272727274" type="appl" />
         <tli id="T437" time="258.61636363636364" type="appl" />
         <tli id="T438" time="259.23845454545454" type="appl" />
         <tli id="T439" time="259.86054545454544" type="appl" />
         <tli id="T440" time="260.48263636363635" type="appl" />
         <tli id="T441" time="261.10472727272725" type="appl" />
         <tli id="T442" time="261.7268181818182" type="appl" />
         <tli id="T443" time="262.3489090909091" type="appl" />
         <tli id="T444" time="263.1310108275714" />
         <tli id="T445" time="263.38522222222224" type="appl" />
         <tli id="T446" time="263.7994444444445" type="appl" />
         <tli id="T447" time="264.21366666666665" type="appl" />
         <tli id="T448" time="264.6278888888889" type="appl" />
         <tli id="T449" time="265.0421111111111" type="appl" />
         <tli id="T450" time="265.45633333333336" type="appl" />
         <tli id="T451" time="265.87055555555554" type="appl" />
         <tli id="T452" time="266.2847777777778" type="appl" />
         <tli id="T453" time="266.74566828489475" />
         <tli id="T454" time="267.1855454545455" type="appl" />
         <tli id="T455" time="267.6720909090909" type="appl" />
         <tli id="T456" time="268.1586363636364" type="appl" />
         <tli id="T457" time="268.6451818181818" type="appl" />
         <tli id="T458" time="269.1317272727273" type="appl" />
         <tli id="T459" time="269.6182727272727" type="appl" />
         <tli id="T460" time="270.1048181818182" type="appl" />
         <tli id="T461" time="270.5913636363636" type="appl" />
         <tli id="T462" time="271.0779090909091" type="appl" />
         <tli id="T463" time="271.5644545454545" type="appl" />
         <tli id="T464" time="272.051" type="appl" />
         <tli id="T0" time="272.913" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KiLS"
                      id="tx-KiLS"
                      speaker="KiLS"
                      type="t">
         <timeline-fork end="T404" start="T403">
            <tli id="T403.tx-KiLS.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KiLS">
            <ts e="T22" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Min</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">aːtɨm</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">Kʼirgʼizava</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">Lʼina</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">Sʼemʼonavna</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_22" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">Min</ts>
                  <nts id="Seg_25" n="HIAT:ip">,</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">min</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">töröːbütüm</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">bu͡olla</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">Nosku͡oga</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">Sɨndasskaga</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Kergetterim</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">bu͡olla</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">tɨ͡ataːgɨ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">etilere</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">kimner</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">kim</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">tabahɨttar</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_69" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">Korgoːgo</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T20">töröːbütüm</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T27" id="Seg_77" n="sc" s="T24">
               <ts e="T27" id="Seg_79" n="HIAT:u" s="T24">
                  <nts id="Seg_80" n="HIAT:ip">–</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_83" n="HIAT:w" s="T24">Paːččɨttar</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_86" n="HIAT:w" s="T25">etilere</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T55" id="Seg_89" n="sc" s="T29">
               <ts e="T48" id="Seg_91" n="HIAT:u" s="T29">
                  <nts id="Seg_92" n="HIAT:ip">–</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_95" n="HIAT:w" s="T29">Inʼem</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_98" n="HIAT:w" s="T31">bu͡olla</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_101" n="HIAT:w" s="T32">i</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_104" n="HIAT:w" s="T33">inʼem</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_107" n="HIAT:w" s="T34">kepsiːre</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_110" n="HIAT:w" s="T35">bu͡olla</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_113" n="HIAT:w" s="T36">min</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_116" n="HIAT:w" s="T37">töröːbüpper</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_119" n="HIAT:w" s="T38">bu͡ol</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_122" n="HIAT:w" s="T39">min</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_125" n="HIAT:w" s="T40">bu͡o</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_128" n="HIAT:w" s="T41">kim</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_131" n="HIAT:w" s="T42">töröːbüpper</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_134" n="HIAT:w" s="T43">bu͡o</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_137" n="HIAT:w" s="T44">kim</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_140" n="HIAT:w" s="T45">tohogo</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_143" n="HIAT:w" s="T46">uːrbuttar</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_146" n="HIAT:w" s="T47">tɨ͡aga</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_150" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_152" n="HIAT:w" s="T48">Onno</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_155" n="HIAT:w" s="T49">bu͡o</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_158" n="HIAT:w" s="T50">kɨhɨ͡ak</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_162" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_164" n="HIAT:w" s="T51">Tu͡ok</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_167" n="HIAT:w" s="T52">baːr</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_170" n="HIAT:w" s="T53">ješʼo</ts>
                  <nts id="Seg_171" n="HIAT:ip">?</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T59" id="Seg_173" n="sc" s="T57">
               <ts e="T59" id="Seg_175" n="HIAT:u" s="T57">
                  <nts id="Seg_176" n="HIAT:ip">–</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_179" n="HIAT:w" s="T57">Gedereː</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T127" id="Seg_182" n="sc" s="T60">
               <ts e="T62" id="Seg_184" n="HIAT:u" s="T60">
                  <nts id="Seg_185" n="HIAT:ip">–</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_188" n="HIAT:w" s="T60">Hoŋohoːn</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_192" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_194" n="HIAT:w" s="T62">Onu</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_197" n="HIAT:w" s="T63">onu</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_200" n="HIAT:w" s="T64">onu</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_203" n="HIAT:w" s="T65">bu͡olla</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_206" n="HIAT:w" s="T66">maska</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_209" n="HIAT:w" s="T67">baːjan</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_212" n="HIAT:w" s="T68">baran</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_215" n="HIAT:w" s="T69">bu</ts>
                  <nts id="Seg_216" n="HIAT:ip">,</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_219" n="HIAT:w" s="T70">ol</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_222" n="HIAT:w" s="T71">aːta</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_225" n="HIAT:w" s="T72">tohogo</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_229" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_231" n="HIAT:w" s="T73">Bu</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_234" n="HIAT:w" s="T74">ke</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_237" n="HIAT:w" s="T75">hirge</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_240" n="HIAT:w" s="T76">uːran</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_243" n="HIAT:w" s="T77">keːspitter</ts>
                  <nts id="Seg_244" n="HIAT:ip">,</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_247" n="HIAT:w" s="T78">ontuŋ</ts>
                  <nts id="Seg_248" n="HIAT:ip">,</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_251" n="HIAT:w" s="T79">ol</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_254" n="HIAT:w" s="T80">tohogoŋ</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_257" n="HIAT:w" s="T81">ol</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_260" n="HIAT:w" s="T82">tohogoŋ</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_263" n="HIAT:w" s="T83">bu</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_266" n="HIAT:w" s="T84">bi͡ek</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_269" n="HIAT:w" s="T85">to</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_272" n="HIAT:w" s="T86">baːr</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_276" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_278" n="HIAT:w" s="T87">Onton</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_281" n="HIAT:w" s="T88">bu͡olla</ts>
                  <nts id="Seg_282" n="HIAT:ip">,</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_285" n="HIAT:w" s="T89">min</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_288" n="HIAT:w" s="T90">hette</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_291" n="HIAT:w" s="T91">hette</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_294" n="HIAT:w" s="T92">hette</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_297" n="HIAT:w" s="T93">dʼɨllaːk</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_300" n="HIAT:w" s="T94">erdeppinen</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_303" n="HIAT:w" s="T95">bu͡ollagɨna</ts>
                  <nts id="Seg_304" n="HIAT:ip">,</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_307" n="HIAT:w" s="T96">kim</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_310" n="HIAT:w" s="T97">Hɨndasskaga</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_313" n="HIAT:w" s="T98">kiːrbippit</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_317" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_319" n="HIAT:w" s="T99">Tak-ta</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_322" n="HIAT:w" s="T100">bihigi</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_325" n="HIAT:w" s="T101">bi͡ek</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_328" n="HIAT:w" s="T102">tɨ͡aga</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_331" n="HIAT:w" s="T103">hɨldʼaːččɨ</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_334" n="HIAT:w" s="T104">etibit</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_338" n="HIAT:w" s="T105">inʼe</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_342" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_344" n="HIAT:w" s="T106">Bu</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_347" n="HIAT:w" s="T107">elbek</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_350" n="HIAT:w" s="T108">bagajɨ</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_353" n="HIAT:w" s="T109">ü͡ördeːk</ts>
                  <nts id="Seg_354" n="HIAT:ip">,</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_356" n="HIAT:ip">(</nts>
                  <ts e="T111" id="Seg_358" n="HIAT:w" s="T110">elbe-</ts>
                  <nts id="Seg_359" n="HIAT:ip">)</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_362" n="HIAT:w" s="T111">elbek</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_365" n="HIAT:w" s="T112">bagajɨ</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_368" n="HIAT:w" s="T113">ü͡ördeːk</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_371" n="HIAT:w" s="T114">etibit</ts>
                  <nts id="Seg_372" n="HIAT:ip">,</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_375" n="HIAT:w" s="T115">elbek</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_378" n="HIAT:w" s="T116">ɨ͡allar</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_381" n="HIAT:w" s="T117">etilere</ts>
                  <nts id="Seg_382" n="HIAT:ip">,</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_385" n="HIAT:w" s="T118">kös</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_388" n="HIAT:w" s="T119">kihi</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_391" n="HIAT:w" s="T120">altalɨː</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_395" n="HIAT:w" s="T121">bi͡estiː</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_398" n="HIAT:w" s="T122">baloktor</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_401" n="HIAT:w" s="T123">urahalar</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_404" n="HIAT:w" s="T124">hɨldʼaːččɨbit</ts>
                  <nts id="Seg_405" n="HIAT:ip">,</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_408" n="HIAT:w" s="T125">inʼe</ts>
                  <nts id="Seg_409" n="HIAT:ip">.</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T144" id="Seg_411" n="sc" s="T131">
               <ts e="T144" id="Seg_413" n="HIAT:u" s="T131">
                  <nts id="Seg_414" n="HIAT:ip">–</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_417" n="HIAT:w" s="T131">Onton</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_420" n="HIAT:w" s="T132">bihigi</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_423" n="HIAT:w" s="T133">ješʼo</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_426" n="HIAT:w" s="T134">oŋu͡or</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_429" n="HIAT:w" s="T135">hɨppɨppɨt</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_432" n="HIAT:w" s="T136">ürdük</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_435" n="HIAT:w" s="T137">kajaga</ts>
                  <nts id="Seg_436" n="HIAT:ip">,</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_439" n="HIAT:w" s="T138">onno</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_442" n="HIAT:w" s="T139">altalɨː</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_445" n="HIAT:w" s="T140">bi͡estiː</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_448" n="HIAT:w" s="T141">uraha</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_451" n="HIAT:w" s="T142">dʼi͡e</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T464" id="Seg_454" n="sc" s="T150">
               <ts e="T160" id="Seg_456" n="HIAT:u" s="T150">
                  <nts id="Seg_457" n="HIAT:ip">–</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_460" n="HIAT:w" s="T150">Onno</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_463" n="HIAT:w" s="T151">kim</ts>
                  <nts id="Seg_464" n="HIAT:ip">,</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_467" n="HIAT:w" s="T152">ikki</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_470" n="HIAT:w" s="T153">ikki</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_473" n="HIAT:w" s="T154">ikki</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_476" n="HIAT:w" s="T155">ogo</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_479" n="HIAT:w" s="T156">ikki</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_482" n="HIAT:w" s="T157">agaj</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_485" n="HIAT:w" s="T158">ogoloːk</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_488" n="HIAT:w" s="T159">ete</ts>
                  <nts id="Seg_489" n="HIAT:ip">.</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_492" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_494" n="HIAT:w" s="T160">Onno</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_497" n="HIAT:w" s="T161">oŋu͡or</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_500" n="HIAT:w" s="T162">bu͡olla</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_503" n="HIAT:w" s="T163">kim</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_506" n="HIAT:w" s="T164">ürdük</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_509" n="HIAT:w" s="T165">kajaga</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_512" n="HIAT:w" s="T166">altalɨː</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_515" n="HIAT:w" s="T167">bi͡estiː</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_518" n="HIAT:w" s="T168">uraha</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_521" n="HIAT:w" s="T169">dʼi͡e</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_524" n="HIAT:w" s="T170">turaːččɨ</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_527" n="HIAT:w" s="T171">etilere</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_531" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_533" n="HIAT:w" s="T172">Onno</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_536" n="HIAT:w" s="T173">bu͡olla</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_539" n="HIAT:w" s="T174">rɨbaktar</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_542" n="HIAT:w" s="T175">haːjɨn</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_545" n="HIAT:w" s="T176">bu͡olla</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_548" n="HIAT:w" s="T177">rɨbaktaːččɨ</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_551" n="HIAT:w" s="T178">etilere</ts>
                  <nts id="Seg_552" n="HIAT:ip">,</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_555" n="HIAT:w" s="T179">vot</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_559" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_561" n="HIAT:w" s="T180">Onton</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_564" n="HIAT:w" s="T181">bu͡o</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_567" n="HIAT:w" s="T182">min</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_569" n="HIAT:ip">(</nts>
                  <ts e="T184" id="Seg_571" n="HIAT:w" s="T183">Hɨnda-</ts>
                  <nts id="Seg_572" n="HIAT:ip">)</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_575" n="HIAT:w" s="T184">Hɨndasskaga</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_578" n="HIAT:w" s="T185">kiːrbippit</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_582" n="HIAT:w" s="T186">teːtem</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_585" n="HIAT:w" s="T187">ölön</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_588" n="HIAT:w" s="T188">ölön</ts>
                  <nts id="Seg_589" n="HIAT:ip">.</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_592" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_594" n="HIAT:w" s="T189">Onno</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_597" n="HIAT:w" s="T190">bu͡o</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_600" n="HIAT:w" s="T191">bihigi</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_603" n="HIAT:w" s="T192">bu͡olla</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_606" n="HIAT:w" s="T193">ü͡öremmippit</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_609" n="HIAT:w" s="T194">ɨrɨbnajga</ts>
                  <nts id="Seg_610" n="HIAT:ip">,</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_613" n="HIAT:w" s="T195">Kasʼistajga</ts>
                  <nts id="Seg_614" n="HIAT:ip">,</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_617" n="HIAT:w" s="T196">Nasku͡oga</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_621" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_623" n="HIAT:w" s="T197">Nasku͡oga</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_626" n="HIAT:w" s="T198">bu͡o</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_629" n="HIAT:w" s="T199">agɨs</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_632" n="HIAT:w" s="T200">klaːhɨ</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_635" n="HIAT:w" s="T201">büten</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_638" n="HIAT:w" s="T202">baran</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_641" n="HIAT:w" s="T203">bu͡olla</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_644" n="HIAT:w" s="T204">bihigini</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_647" n="HIAT:w" s="T205">kimŋe</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_650" n="HIAT:w" s="T206">ɨːppɨttara</ts>
                  <nts id="Seg_651" n="HIAT:ip">,</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_654" n="HIAT:w" s="T207">Krasnajarskijga</ts>
                  <nts id="Seg_655" n="HIAT:ip">.</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_658" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_660" n="HIAT:w" s="T208">Onno</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_663" n="HIAT:w" s="T209">bu͡olla</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_666" n="HIAT:w" s="T210">hürbe</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_669" n="HIAT:w" s="T211">hettis</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_672" n="HIAT:w" s="T212">ošku͡olaga</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_675" n="HIAT:w" s="T213">ü͡öremmippit</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_679" n="HIAT:w" s="T214">dʼevʼatɨj</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_682" n="HIAT:w" s="T215">dʼesʼatɨj</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_685" n="HIAT:w" s="T216">kɨlaːhɨ</ts>
                  <nts id="Seg_686" n="HIAT:ip">.</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_689" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_691" n="HIAT:w" s="T217">Onton</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_694" n="HIAT:w" s="T218">bu͡olla</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_697" n="HIAT:w" s="T219">eː</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_699" n="HIAT:ip">(</nts>
                  <nts id="Seg_700" n="HIAT:ip">(</nts>
                  <ats e="T221" id="Seg_701" n="HIAT:non-pho" s="T220">PAUSE</ats>
                  <nts id="Seg_702" n="HIAT:ip">)</nts>
                  <nts id="Seg_703" n="HIAT:ip">)</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_706" n="HIAT:w" s="T221">agɨhu͡on</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_709" n="HIAT:w" s="T222">dʼɨllaːkka</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_712" n="HIAT:w" s="T223">bɨla</ts>
                  <nts id="Seg_713" n="HIAT:ip">,</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_716" n="HIAT:w" s="T224">bihigi</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_719" n="HIAT:w" s="T225">bu͡olla</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_721" n="HIAT:ip">(</nts>
                  <nts id="Seg_722" n="HIAT:ip">(</nts>
                  <ats e="T227" id="Seg_723" n="HIAT:non-pho" s="T226">…</ats>
                  <nts id="Seg_724" n="HIAT:ip">)</nts>
                  <nts id="Seg_725" n="HIAT:ip">)</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_728" n="HIAT:w" s="T227">kim</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_731" n="HIAT:w" s="T228">kimŋe</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_734" n="HIAT:w" s="T229">pastupajdaːbɨtɨm</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_737" n="HIAT:w" s="T230">Krasnajarskij</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_740" n="HIAT:w" s="T231">Gasudarstvʼennɨj</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_743" n="HIAT:w" s="T232">Mʼedʼicɨnskij</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_746" n="HIAT:w" s="T233">Instʼitut</ts>
                  <nts id="Seg_747" n="HIAT:ip">.</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_750" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_752" n="HIAT:w" s="T234">Onno</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_755" n="HIAT:w" s="T235">bu͡o</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_758" n="HIAT:w" s="T236">alta</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_761" n="HIAT:w" s="T237">dʼɨlɨ</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_764" n="HIAT:w" s="T238">ü͡öremmitim</ts>
                  <nts id="Seg_765" n="HIAT:ip">.</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_768" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_770" n="HIAT:w" s="T239">Onton</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_773" n="HIAT:w" s="T240">bu͡o</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_776" n="HIAT:w" s="T241">hettis</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_779" n="HIAT:w" s="T242">dʼɨlɨm</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_782" n="HIAT:w" s="T243">intʼernatuːra</ts>
                  <nts id="Seg_783" n="HIAT:ip">,</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_786" n="HIAT:w" s="T244">pa</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_789" n="HIAT:w" s="T245">tubʼerkuloːzu</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_792" n="HIAT:w" s="T246">intʼernatuːra</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_795" n="HIAT:w" s="T247">biːr</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_798" n="HIAT:w" s="T248">dʼɨlɨ</ts>
                  <nts id="Seg_799" n="HIAT:ip">,</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_802" n="HIAT:w" s="T249">vot</ts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_806" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_808" n="HIAT:w" s="T250">Onu</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_811" n="HIAT:w" s="T251">büten</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_814" n="HIAT:w" s="T252">baran</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_817" n="HIAT:w" s="T253">bu͡o</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_820" n="HIAT:w" s="T254">miːgin</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_823" n="HIAT:w" s="T255">bu͡o</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_826" n="HIAT:w" s="T256">Tajmɨːrga</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_829" n="HIAT:w" s="T257">ɨːppɨttara</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_832" n="HIAT:w" s="T258">töröːbüt</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_835" n="HIAT:w" s="T259">ke</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_838" n="HIAT:w" s="T260">hirber</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_841" n="HIAT:w" s="T261">ke</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_844" n="HIAT:w" s="T262">üleli͡eppin</ts>
                  <nts id="Seg_845" n="HIAT:ip">.</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_848" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_850" n="HIAT:w" s="T263">Dudʼinkaga</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_853" n="HIAT:w" s="T264">kelen</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_856" n="HIAT:w" s="T265">baran</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_859" n="HIAT:w" s="T266">bu͡o</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_862" n="HIAT:w" s="T267">minigin</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_865" n="HIAT:w" s="T268">bu͡o</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_868" n="HIAT:w" s="T269">Pataːpavaga</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_871" n="HIAT:w" s="T270">ɨːppɨttara</ts>
                  <nts id="Seg_872" n="HIAT:ip">.</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_875" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_877" n="HIAT:w" s="T271">Onno</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_880" n="HIAT:w" s="T272">kim</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_883" n="HIAT:w" s="T273">baːr</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_886" n="HIAT:w" s="T274">ete</ts>
                  <nts id="Seg_887" n="HIAT:ip">,</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_890" n="HIAT:w" s="T275">Lʼesnʼaja</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_893" n="HIAT:w" s="T276">škola</ts>
                  <nts id="Seg_894" n="HIAT:ip">.</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_897" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_899" n="HIAT:w" s="T277">Hu͡ok</ts>
                  <nts id="Seg_900" n="HIAT:ip">.</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_903" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_905" n="HIAT:w" s="T278">Pataːpavskaja</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_908" n="HIAT:w" s="T279">lʼesnaja</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_911" n="HIAT:w" s="T280">škoːla</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_914" n="HIAT:w" s="T281">ete</ts>
                  <nts id="Seg_915" n="HIAT:ip">.</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_918" n="HIAT:u" s="T282">
                  <nts id="Seg_919" n="HIAT:ip">(</nts>
                  <nts id="Seg_920" n="HIAT:ip">(</nts>
                  <ats e="T283" id="Seg_921" n="HIAT:non-pho" s="T282">COUGH</ats>
                  <nts id="Seg_922" n="HIAT:ip">)</nts>
                  <nts id="Seg_923" n="HIAT:ip">)</nts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_926" n="HIAT:w" s="T283">Vot</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_929" n="HIAT:w" s="T284">onno</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_932" n="HIAT:w" s="T285">bu͡olla</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_935" n="HIAT:w" s="T286">üs</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_938" n="HIAT:w" s="T287">dʼɨlɨ</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_941" n="HIAT:w" s="T288">üleleːbitim</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_945" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_947" n="HIAT:w" s="T289">Vot</ts>
                  <nts id="Seg_948" n="HIAT:ip">,</nts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_951" n="HIAT:w" s="T290">agɨhu͡on</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_954" n="HIAT:w" s="T291">tördüsteːk</ts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_958" n="HIAT:w" s="T292">agɨhu͡on</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_961" n="HIAT:w" s="T293">tördüsteːk</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_964" n="HIAT:w" s="T294">dʼɨlga</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_967" n="HIAT:w" s="T295">munna</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_970" n="HIAT:w" s="T296">kelbitim</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_973" n="HIAT:w" s="T297">köhön</ts>
                  <nts id="Seg_974" n="HIAT:ip">,</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_977" n="HIAT:w" s="T298">pʼerʼevoːdam</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_980" n="HIAT:w" s="T299">ɨːppɨttara</ts>
                  <nts id="Seg_981" n="HIAT:ip">.</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_984" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_986" n="HIAT:w" s="T300">I</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_989" n="HIAT:w" s="T301">hol</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_992" n="HIAT:w" s="T302">dʼɨltan</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_995" n="HIAT:w" s="T303">bu</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_998" n="HIAT:w" s="T304">anɨga</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1001" n="HIAT:w" s="T305">di͡eri</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1004" n="HIAT:w" s="T306">bu͡olla</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1007" n="HIAT:w" s="T307">kimi͡eke</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1010" n="HIAT:w" s="T308">üleliːbin</ts>
                  <nts id="Seg_1011" n="HIAT:ip">,</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1014" n="HIAT:w" s="T309">v</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1017" n="HIAT:w" s="T310">krajevom</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1020" n="HIAT:w" s="T311">pratʼivatubʼerkuloznam</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1023" n="HIAT:w" s="T312">dʼispanserʼe</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1026" n="HIAT:w" s="T313">nomʼer</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1029" n="HIAT:w" s="T314">dʼevʼatʼ</ts>
                  <nts id="Seg_1030" n="HIAT:ip">,</nts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1033" n="HIAT:w" s="T315">vračʼ-ftʼizʼiatʼer</ts>
                  <nts id="Seg_1034" n="HIAT:ip">.</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1037" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1039" n="HIAT:w" s="T317">Onton</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1042" n="HIAT:w" s="T318">bu͡o</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1045" n="HIAT:w" s="T319">agɨhu͡on</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1048" n="HIAT:w" s="T320">toksustaːk</ts>
                  <nts id="Seg_1049" n="HIAT:ip">,</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1052" n="HIAT:w" s="T321">i</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1055" n="HIAT:w" s="T322">toksustaːk</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1058" n="HIAT:w" s="T323">dʼɨlga</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1061" n="HIAT:w" s="T324">bu͡olla</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1064" n="HIAT:w" s="T325">kim</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1067" n="HIAT:w" s="T326">bu͡olbut</ts>
                  <nts id="Seg_1068" n="HIAT:ip">,</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1071" n="HIAT:w" s="T327">zʼemlʼetrʼasʼenʼije</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1074" n="HIAT:w" s="T328">bu͡olbut</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1077" n="HIAT:w" s="T329">Armʼenʼijaga</ts>
                  <nts id="Seg_1078" n="HIAT:ip">.</nts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1081" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1083" n="HIAT:w" s="T330">Onno</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1086" n="HIAT:w" s="T331">bejem</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1089" n="HIAT:w" s="T332">hanaːbɨnan</ts>
                  <nts id="Seg_1090" n="HIAT:ip">,</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1093" n="HIAT:w" s="T333">bʼesplaːtna</ts>
                  <nts id="Seg_1094" n="HIAT:ip">,</nts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1097" n="HIAT:w" s="T334">bʼesvazmʼezdna</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1100" n="HIAT:w" s="T335">di͡ebikke</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1103" n="HIAT:w" s="T336">dʼe</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1106" n="HIAT:w" s="T337">onno</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1109" n="HIAT:w" s="T338">üleleːbitim</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1112" n="HIAT:w" s="T339">ikki</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1114" n="HIAT:ip">(</nts>
                  <ts e="T341" id="Seg_1116" n="HIAT:w" s="T340">dʼ-</ts>
                  <nts id="Seg_1117" n="HIAT:ip">)</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1120" n="HIAT:w" s="T341">dva</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1123" n="HIAT:w" s="T342">goda</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1126" n="HIAT:w" s="T343">padrʼad</ts>
                  <nts id="Seg_1127" n="HIAT:ip">.</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1130" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1132" n="HIAT:w" s="T344">V</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1135" n="HIAT:w" s="T345">pratiʼvatubʼerkuloːznam</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1138" n="HIAT:w" s="T346">dʼispanserʼe</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1141" n="HIAT:w" s="T347">imʼenʼi</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1144" n="HIAT:w" s="T348">Mʼelkanʼaːna</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1147" n="HIAT:w" s="T349">goːrada</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1150" n="HIAT:w" s="T350">Lʼenʼinakana</ts>
                  <nts id="Seg_1151" n="HIAT:ip">,</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1154" n="HIAT:w" s="T351">vot</ts>
                  <nts id="Seg_1155" n="HIAT:ip">.</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1158" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1160" n="HIAT:w" s="T352">Agɨhu͡on</ts>
                  <nts id="Seg_1161" n="HIAT:ip">,</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1163" n="HIAT:ip">(</nts>
                  <nts id="Seg_1164" n="HIAT:ip">(</nts>
                  <ats e="T354" id="Seg_1165" n="HIAT:non-pho" s="T353">NOISE</ats>
                  <nts id="Seg_1166" n="HIAT:ip">)</nts>
                  <nts id="Seg_1167" n="HIAT:ip">)</nts>
                  <nts id="Seg_1168" n="HIAT:ip">,</nts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1171" n="HIAT:w" s="T354">v</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1174" n="HIAT:w" s="T355">dvuxtɨsʼicnɨj</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1177" n="HIAT:w" s="T356">pʼatam</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1180" n="HIAT:w" s="T357">bu</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1183" n="HIAT:w" s="T358">du͡o</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1186" n="HIAT:w" s="T359">kim</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1189" n="HIAT:w" s="T360">bi͡erbittere</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1192" n="HIAT:w" s="T361">mini͡eke</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1195" n="HIAT:w" s="T362">počʼotnuju</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1198" n="HIAT:w" s="T363">gramatu</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1201" n="HIAT:w" s="T364">Mʼinʼistʼerstva</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1204" n="HIAT:w" s="T365">zdravaaxranʼenʼija</ts>
                  <nts id="Seg_1205" n="HIAT:ip">,</nts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1208" n="HIAT:w" s="T366">eta</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1211" n="HIAT:w" s="T367">sajuznavo</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1214" n="HIAT:w" s="T368">značʼenʼije</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1217" n="HIAT:w" s="T369">kak</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1220" n="HIAT:w" s="T370">vʼetʼeran</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1223" n="HIAT:w" s="T371">truda</ts>
                  <nts id="Seg_1224" n="HIAT:ip">,</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1227" n="HIAT:w" s="T372">vot</ts>
                  <nts id="Seg_1228" n="HIAT:ip">.</nts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1231" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1233" n="HIAT:w" s="T373">Kergetterbin</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1236" n="HIAT:w" s="T374">gɨtta</ts>
                  <nts id="Seg_1237" n="HIAT:ip">,</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1240" n="HIAT:w" s="T375">inʼebin</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1243" n="HIAT:w" s="T376">gɨtta</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1246" n="HIAT:w" s="T377">olorobun</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1249" n="HIAT:w" s="T378">anɨ</ts>
                  <nts id="Seg_1250" n="HIAT:ip">,</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1252" n="HIAT:ip">(</nts>
                  <ts e="T380" id="Seg_1254" n="HIAT:w" s="T379">plemʼanʼi-</ts>
                  <nts id="Seg_1255" n="HIAT:ip">)</nts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1258" n="HIAT:w" s="T380">plemʼanʼisabɨn</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1261" n="HIAT:w" s="T381">gɨtta</ts>
                  <nts id="Seg_1262" n="HIAT:ip">.</nts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1265" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1267" n="HIAT:w" s="T382">Ontum</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1270" n="HIAT:w" s="T383">bu͡olla</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1273" n="HIAT:w" s="T384">onus</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1276" n="HIAT:w" s="T385">klaska</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1279" n="HIAT:w" s="T386">ü͡örener</ts>
                  <nts id="Seg_1280" n="HIAT:ip">.</nts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1283" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1285" n="HIAT:w" s="T387">Bejem</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1288" n="HIAT:w" s="T388">bi͡ek</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1291" n="HIAT:w" s="T389">üleliːbin</ts>
                  <nts id="Seg_1292" n="HIAT:ip">.</nts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1295" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1297" n="HIAT:w" s="T390">A</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1300" n="HIAT:w" s="T391">bihigini</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1303" n="HIAT:w" s="T392">bu͡o</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1306" n="HIAT:w" s="T393">behis</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1309" n="HIAT:w" s="T394">klasska</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1312" n="HIAT:w" s="T395">bu͡o</ts>
                  <nts id="Seg_1313" n="HIAT:ip">,</nts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1316" n="HIAT:w" s="T396">behis</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1319" n="HIAT:w" s="T397">klasska</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1322" n="HIAT:w" s="T398">ü͡örener</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1325" n="HIAT:w" s="T399">erdekpitinen</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1328" n="HIAT:w" s="T400">bu͡o</ts>
                  <nts id="Seg_1329" n="HIAT:ip">,</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1332" n="HIAT:w" s="T401">Nasku͡oga</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1335" n="HIAT:w" s="T402">bu͡olla</ts>
                  <nts id="Seg_1336" n="HIAT:ip">,</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403.tx-KiLS.1" id="Seg_1339" n="HIAT:w" s="T403">dom</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1342" n="HIAT:w" s="T403.tx-KiLS.1">kulʼturaga</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1345" n="HIAT:w" s="T404">bu͡olla</ts>
                  <nts id="Seg_1346" n="HIAT:ip">,</nts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1349" n="HIAT:w" s="T405">bihigini</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1352" n="HIAT:w" s="T406">kim</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1355" n="HIAT:w" s="T407">arganʼizavala</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1358" n="HIAT:w" s="T408">kružok</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1361" n="HIAT:w" s="T409">barganʼistav</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1364" n="HIAT:w" s="T410">naša</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1367" n="HIAT:w" s="T411">dalganskaja</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1370" n="HIAT:w" s="T412">paetessa</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1373" n="HIAT:w" s="T413">Ogdu͡o</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1376" n="HIAT:w" s="T414">Aksʼonava</ts>
                  <nts id="Seg_1377" n="HIAT:ip">.</nts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1380" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1382" n="HIAT:w" s="T415">Ol</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1385" n="HIAT:w" s="T416">onu</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1387" n="HIAT:ip">(</nts>
                  <ts e="T418" id="Seg_1389" n="HIAT:w" s="T417">bi-</ts>
                  <nts id="Seg_1390" n="HIAT:ip">)</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1393" n="HIAT:w" s="T418">bihigini</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1396" n="HIAT:w" s="T419">kimi͡eke</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1399" n="HIAT:w" s="T420">ü͡öreppite</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1402" n="HIAT:w" s="T421">bargaːŋŋa</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1405" n="HIAT:w" s="T422">oːnnʼuː</ts>
                  <nts id="Seg_1406" n="HIAT:ip">.</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1409" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_1411" n="HIAT:w" s="T423">Anɨ</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1414" n="HIAT:w" s="T424">bu͡olla</ts>
                  <nts id="Seg_1415" n="HIAT:ip">,</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1418" n="HIAT:w" s="T425">da</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1421" n="HIAT:w" s="T426">sʼix</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1424" n="HIAT:w" s="T427">por</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1427" n="HIAT:w" s="T428">kɨrdʼɨ͡akpar</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1430" n="HIAT:w" s="T429">di͡eri</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1433" n="HIAT:w" s="T430">bi͡ek</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1436" n="HIAT:w" s="T431">oːnnʼuːbun</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1439" n="HIAT:w" s="T432">anɨ</ts>
                  <nts id="Seg_1440" n="HIAT:ip">.</nts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_1443" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1445" n="HIAT:w" s="T433">Vot</ts>
                  <nts id="Seg_1446" n="HIAT:ip">,</nts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1449" n="HIAT:w" s="T434">Jakuːtskajgɨttan</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1452" n="HIAT:w" s="T435">kimnere</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1455" n="HIAT:w" s="T436">kelbit</ts>
                  <nts id="Seg_1456" n="HIAT:ip">,</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1459" n="HIAT:w" s="T437">artʼistar</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1462" n="HIAT:w" s="T438">kelbit</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1465" n="HIAT:w" s="T439">etilere</ts>
                  <nts id="Seg_1466" n="HIAT:ip">,</nts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1469" n="HIAT:w" s="T440">olor</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1472" n="HIAT:w" s="T441">bihi͡eke</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1475" n="HIAT:w" s="T442">bargan</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1478" n="HIAT:w" s="T443">atɨlaːbɨttara</ts>
                  <nts id="Seg_1479" n="HIAT:ip">.</nts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1482" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1484" n="HIAT:w" s="T444">Tagda</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1487" n="HIAT:w" s="T445">bu</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1490" n="HIAT:w" s="T446">ulakan</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1493" n="HIAT:w" s="T447">karčɨ</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1496" n="HIAT:w" s="T448">ete</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1499" n="HIAT:w" s="T449">bu͡o</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1502" n="HIAT:w" s="T450">agɨhu͡on</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1505" n="HIAT:w" s="T451">agɨhu͡on</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1508" n="HIAT:w" s="T452">rublʼ</ts>
                  <nts id="Seg_1509" n="HIAT:ip">.</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1512" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1514" n="HIAT:w" s="T453">Anɨ</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1517" n="HIAT:w" s="T454">bu͡o</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1520" n="HIAT:w" s="T455">bi͡ek</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1523" n="HIAT:w" s="T456">oːnnʼuːbun</ts>
                  <nts id="Seg_1524" n="HIAT:ip">,</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1527" n="HIAT:w" s="T457">hanaːm</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1530" n="HIAT:w" s="T458">kellegine</ts>
                  <nts id="Seg_1531" n="HIAT:ip">,</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1534" n="HIAT:w" s="T459">anɨ</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1537" n="HIAT:w" s="T460">emi͡e</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1540" n="HIAT:w" s="T461">ehi͡eke</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1543" n="HIAT:w" s="T462">min</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1546" n="HIAT:w" s="T463">oːnnʼu͡om</ts>
                  <nts id="Seg_1547" n="HIAT:ip">.</nts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KiLS">
            <ts e="T22" id="Seg_1549" n="sc" s="T1">
               <ts e="T2" id="Seg_1551" n="e" s="T1">– Min </ts>
               <ts e="T3" id="Seg_1553" n="e" s="T2">aːtɨm </ts>
               <ts e="T4" id="Seg_1555" n="e" s="T3">Kʼirgʼizava </ts>
               <ts e="T5" id="Seg_1557" n="e" s="T4">Lʼina </ts>
               <ts e="T6" id="Seg_1559" n="e" s="T5">Sʼemʼonavna. </ts>
               <ts e="T7" id="Seg_1561" n="e" s="T6">Min, </ts>
               <ts e="T8" id="Seg_1563" n="e" s="T7">min </ts>
               <ts e="T9" id="Seg_1565" n="e" s="T8">töröːbütüm </ts>
               <ts e="T10" id="Seg_1567" n="e" s="T9">bu͡olla </ts>
               <ts e="T11" id="Seg_1569" n="e" s="T10">Nosku͡oga </ts>
               <ts e="T12" id="Seg_1571" n="e" s="T11">Sɨndasskaga. </ts>
               <ts e="T13" id="Seg_1573" n="e" s="T12">Kergetterim </ts>
               <ts e="T14" id="Seg_1575" n="e" s="T13">bu͡olla </ts>
               <ts e="T15" id="Seg_1577" n="e" s="T14">tɨ͡ataːgɨ </ts>
               <ts e="T16" id="Seg_1579" n="e" s="T15">etilere, </ts>
               <ts e="T17" id="Seg_1581" n="e" s="T16">kimner </ts>
               <ts e="T18" id="Seg_1583" n="e" s="T17">kim </ts>
               <ts e="T19" id="Seg_1585" n="e" s="T18">tabahɨttar. </ts>
               <ts e="T20" id="Seg_1587" n="e" s="T19">Korgoːgo </ts>
               <ts e="T22" id="Seg_1589" n="e" s="T20">töröːbütüm. </ts>
            </ts>
            <ts e="T27" id="Seg_1590" n="sc" s="T24">
               <ts e="T25" id="Seg_1592" n="e" s="T24">– Paːččɨttar </ts>
               <ts e="T27" id="Seg_1594" n="e" s="T25">etilere. </ts>
            </ts>
            <ts e="T55" id="Seg_1595" n="sc" s="T29">
               <ts e="T31" id="Seg_1597" n="e" s="T29">– Inʼem </ts>
               <ts e="T32" id="Seg_1599" n="e" s="T31">bu͡olla </ts>
               <ts e="T33" id="Seg_1601" n="e" s="T32">i </ts>
               <ts e="T34" id="Seg_1603" n="e" s="T33">inʼem </ts>
               <ts e="T35" id="Seg_1605" n="e" s="T34">kepsiːre </ts>
               <ts e="T36" id="Seg_1607" n="e" s="T35">bu͡olla </ts>
               <ts e="T37" id="Seg_1609" n="e" s="T36">min </ts>
               <ts e="T38" id="Seg_1611" n="e" s="T37">töröːbüpper </ts>
               <ts e="T39" id="Seg_1613" n="e" s="T38">bu͡ol </ts>
               <ts e="T40" id="Seg_1615" n="e" s="T39">min </ts>
               <ts e="T41" id="Seg_1617" n="e" s="T40">bu͡o </ts>
               <ts e="T42" id="Seg_1619" n="e" s="T41">kim </ts>
               <ts e="T43" id="Seg_1621" n="e" s="T42">töröːbüpper </ts>
               <ts e="T44" id="Seg_1623" n="e" s="T43">bu͡o </ts>
               <ts e="T45" id="Seg_1625" n="e" s="T44">kim </ts>
               <ts e="T46" id="Seg_1627" n="e" s="T45">tohogo </ts>
               <ts e="T47" id="Seg_1629" n="e" s="T46">uːrbuttar </ts>
               <ts e="T48" id="Seg_1631" n="e" s="T47">tɨ͡aga. </ts>
               <ts e="T49" id="Seg_1633" n="e" s="T48">Onno </ts>
               <ts e="T50" id="Seg_1635" n="e" s="T49">bu͡o </ts>
               <ts e="T51" id="Seg_1637" n="e" s="T50">kɨhɨ͡ak. </ts>
               <ts e="T52" id="Seg_1639" n="e" s="T51">Tu͡ok </ts>
               <ts e="T53" id="Seg_1641" n="e" s="T52">baːr </ts>
               <ts e="T55" id="Seg_1643" n="e" s="T53">ješʼo? </ts>
            </ts>
            <ts e="T59" id="Seg_1644" n="sc" s="T57">
               <ts e="T59" id="Seg_1646" n="e" s="T57">– Gedereː. </ts>
            </ts>
            <ts e="T127" id="Seg_1647" n="sc" s="T60">
               <ts e="T62" id="Seg_1649" n="e" s="T60">– Hoŋohoːn. </ts>
               <ts e="T63" id="Seg_1651" n="e" s="T62">Onu </ts>
               <ts e="T64" id="Seg_1653" n="e" s="T63">onu </ts>
               <ts e="T65" id="Seg_1655" n="e" s="T64">onu </ts>
               <ts e="T66" id="Seg_1657" n="e" s="T65">bu͡olla </ts>
               <ts e="T67" id="Seg_1659" n="e" s="T66">maska </ts>
               <ts e="T68" id="Seg_1661" n="e" s="T67">baːjan </ts>
               <ts e="T69" id="Seg_1663" n="e" s="T68">baran </ts>
               <ts e="T70" id="Seg_1665" n="e" s="T69">bu, </ts>
               <ts e="T71" id="Seg_1667" n="e" s="T70">ol </ts>
               <ts e="T72" id="Seg_1669" n="e" s="T71">aːta </ts>
               <ts e="T73" id="Seg_1671" n="e" s="T72">tohogo. </ts>
               <ts e="T74" id="Seg_1673" n="e" s="T73">Bu </ts>
               <ts e="T75" id="Seg_1675" n="e" s="T74">ke </ts>
               <ts e="T76" id="Seg_1677" n="e" s="T75">hirge </ts>
               <ts e="T77" id="Seg_1679" n="e" s="T76">uːran </ts>
               <ts e="T78" id="Seg_1681" n="e" s="T77">keːspitter, </ts>
               <ts e="T79" id="Seg_1683" n="e" s="T78">ontuŋ, </ts>
               <ts e="T80" id="Seg_1685" n="e" s="T79">ol </ts>
               <ts e="T81" id="Seg_1687" n="e" s="T80">tohogoŋ </ts>
               <ts e="T82" id="Seg_1689" n="e" s="T81">ol </ts>
               <ts e="T83" id="Seg_1691" n="e" s="T82">tohogoŋ </ts>
               <ts e="T84" id="Seg_1693" n="e" s="T83">bu </ts>
               <ts e="T85" id="Seg_1695" n="e" s="T84">bi͡ek </ts>
               <ts e="T86" id="Seg_1697" n="e" s="T85">to </ts>
               <ts e="T87" id="Seg_1699" n="e" s="T86">baːr. </ts>
               <ts e="T88" id="Seg_1701" n="e" s="T87">Onton </ts>
               <ts e="T89" id="Seg_1703" n="e" s="T88">bu͡olla, </ts>
               <ts e="T90" id="Seg_1705" n="e" s="T89">min </ts>
               <ts e="T91" id="Seg_1707" n="e" s="T90">hette </ts>
               <ts e="T92" id="Seg_1709" n="e" s="T91">hette </ts>
               <ts e="T93" id="Seg_1711" n="e" s="T92">hette </ts>
               <ts e="T94" id="Seg_1713" n="e" s="T93">dʼɨllaːk </ts>
               <ts e="T95" id="Seg_1715" n="e" s="T94">erdeppinen </ts>
               <ts e="T96" id="Seg_1717" n="e" s="T95">bu͡ollagɨna, </ts>
               <ts e="T97" id="Seg_1719" n="e" s="T96">kim </ts>
               <ts e="T98" id="Seg_1721" n="e" s="T97">Hɨndasskaga </ts>
               <ts e="T99" id="Seg_1723" n="e" s="T98">kiːrbippit. </ts>
               <ts e="T100" id="Seg_1725" n="e" s="T99">Tak-ta </ts>
               <ts e="T101" id="Seg_1727" n="e" s="T100">bihigi </ts>
               <ts e="T102" id="Seg_1729" n="e" s="T101">bi͡ek </ts>
               <ts e="T103" id="Seg_1731" n="e" s="T102">tɨ͡aga </ts>
               <ts e="T104" id="Seg_1733" n="e" s="T103">hɨldʼaːččɨ </ts>
               <ts e="T105" id="Seg_1735" n="e" s="T104">etibit, </ts>
               <ts e="T106" id="Seg_1737" n="e" s="T105">inʼe. </ts>
               <ts e="T107" id="Seg_1739" n="e" s="T106">Bu </ts>
               <ts e="T108" id="Seg_1741" n="e" s="T107">elbek </ts>
               <ts e="T109" id="Seg_1743" n="e" s="T108">bagajɨ </ts>
               <ts e="T110" id="Seg_1745" n="e" s="T109">ü͡ördeːk, </ts>
               <ts e="T111" id="Seg_1747" n="e" s="T110">(elbe-) </ts>
               <ts e="T112" id="Seg_1749" n="e" s="T111">elbek </ts>
               <ts e="T113" id="Seg_1751" n="e" s="T112">bagajɨ </ts>
               <ts e="T114" id="Seg_1753" n="e" s="T113">ü͡ördeːk </ts>
               <ts e="T115" id="Seg_1755" n="e" s="T114">etibit, </ts>
               <ts e="T116" id="Seg_1757" n="e" s="T115">elbek </ts>
               <ts e="T117" id="Seg_1759" n="e" s="T116">ɨ͡allar </ts>
               <ts e="T118" id="Seg_1761" n="e" s="T117">etilere, </ts>
               <ts e="T119" id="Seg_1763" n="e" s="T118">kös </ts>
               <ts e="T120" id="Seg_1765" n="e" s="T119">kihi </ts>
               <ts e="T121" id="Seg_1767" n="e" s="T120">altalɨː, </ts>
               <ts e="T122" id="Seg_1769" n="e" s="T121">bi͡estiː </ts>
               <ts e="T123" id="Seg_1771" n="e" s="T122">baloktor </ts>
               <ts e="T124" id="Seg_1773" n="e" s="T123">urahalar </ts>
               <ts e="T125" id="Seg_1775" n="e" s="T124">hɨldʼaːččɨbit, </ts>
               <ts e="T127" id="Seg_1777" n="e" s="T125">inʼe. </ts>
            </ts>
            <ts e="T144" id="Seg_1778" n="sc" s="T131">
               <ts e="T132" id="Seg_1780" n="e" s="T131">– Onton </ts>
               <ts e="T133" id="Seg_1782" n="e" s="T132">bihigi </ts>
               <ts e="T134" id="Seg_1784" n="e" s="T133">ješʼo </ts>
               <ts e="T135" id="Seg_1786" n="e" s="T134">oŋu͡or </ts>
               <ts e="T136" id="Seg_1788" n="e" s="T135">hɨppɨppɨt </ts>
               <ts e="T137" id="Seg_1790" n="e" s="T136">ürdük </ts>
               <ts e="T138" id="Seg_1792" n="e" s="T137">kajaga, </ts>
               <ts e="T139" id="Seg_1794" n="e" s="T138">onno </ts>
               <ts e="T140" id="Seg_1796" n="e" s="T139">altalɨː </ts>
               <ts e="T141" id="Seg_1798" n="e" s="T140">bi͡estiː </ts>
               <ts e="T142" id="Seg_1800" n="e" s="T141">uraha </ts>
               <ts e="T144" id="Seg_1802" n="e" s="T142">dʼi͡e. </ts>
            </ts>
            <ts e="T464" id="Seg_1803" n="sc" s="T150">
               <ts e="T151" id="Seg_1805" n="e" s="T150">– Onno </ts>
               <ts e="T152" id="Seg_1807" n="e" s="T151">kim, </ts>
               <ts e="T153" id="Seg_1809" n="e" s="T152">ikki </ts>
               <ts e="T154" id="Seg_1811" n="e" s="T153">ikki </ts>
               <ts e="T155" id="Seg_1813" n="e" s="T154">ikki </ts>
               <ts e="T156" id="Seg_1815" n="e" s="T155">ogo </ts>
               <ts e="T157" id="Seg_1817" n="e" s="T156">ikki </ts>
               <ts e="T158" id="Seg_1819" n="e" s="T157">agaj </ts>
               <ts e="T159" id="Seg_1821" n="e" s="T158">ogoloːk </ts>
               <ts e="T160" id="Seg_1823" n="e" s="T159">ete. </ts>
               <ts e="T161" id="Seg_1825" n="e" s="T160">Onno </ts>
               <ts e="T162" id="Seg_1827" n="e" s="T161">oŋu͡or </ts>
               <ts e="T163" id="Seg_1829" n="e" s="T162">bu͡olla </ts>
               <ts e="T164" id="Seg_1831" n="e" s="T163">kim </ts>
               <ts e="T165" id="Seg_1833" n="e" s="T164">ürdük </ts>
               <ts e="T166" id="Seg_1835" n="e" s="T165">kajaga </ts>
               <ts e="T167" id="Seg_1837" n="e" s="T166">altalɨː </ts>
               <ts e="T168" id="Seg_1839" n="e" s="T167">bi͡estiː </ts>
               <ts e="T169" id="Seg_1841" n="e" s="T168">uraha </ts>
               <ts e="T170" id="Seg_1843" n="e" s="T169">dʼi͡e </ts>
               <ts e="T171" id="Seg_1845" n="e" s="T170">turaːččɨ </ts>
               <ts e="T172" id="Seg_1847" n="e" s="T171">etilere. </ts>
               <ts e="T173" id="Seg_1849" n="e" s="T172">Onno </ts>
               <ts e="T174" id="Seg_1851" n="e" s="T173">bu͡olla </ts>
               <ts e="T175" id="Seg_1853" n="e" s="T174">rɨbaktar </ts>
               <ts e="T176" id="Seg_1855" n="e" s="T175">haːjɨn </ts>
               <ts e="T177" id="Seg_1857" n="e" s="T176">bu͡olla </ts>
               <ts e="T178" id="Seg_1859" n="e" s="T177">rɨbaktaːččɨ </ts>
               <ts e="T179" id="Seg_1861" n="e" s="T178">etilere, </ts>
               <ts e="T180" id="Seg_1863" n="e" s="T179">vot. </ts>
               <ts e="T181" id="Seg_1865" n="e" s="T180">Onton </ts>
               <ts e="T182" id="Seg_1867" n="e" s="T181">bu͡o </ts>
               <ts e="T183" id="Seg_1869" n="e" s="T182">min </ts>
               <ts e="T184" id="Seg_1871" n="e" s="T183">(Hɨnda-) </ts>
               <ts e="T185" id="Seg_1873" n="e" s="T184">Hɨndasskaga </ts>
               <ts e="T186" id="Seg_1875" n="e" s="T185">kiːrbippit, </ts>
               <ts e="T187" id="Seg_1877" n="e" s="T186">teːtem </ts>
               <ts e="T188" id="Seg_1879" n="e" s="T187">ölön </ts>
               <ts e="T189" id="Seg_1881" n="e" s="T188">ölön. </ts>
               <ts e="T190" id="Seg_1883" n="e" s="T189">Onno </ts>
               <ts e="T191" id="Seg_1885" n="e" s="T190">bu͡o </ts>
               <ts e="T192" id="Seg_1887" n="e" s="T191">bihigi </ts>
               <ts e="T193" id="Seg_1889" n="e" s="T192">bu͡olla </ts>
               <ts e="T194" id="Seg_1891" n="e" s="T193">ü͡öremmippit </ts>
               <ts e="T195" id="Seg_1893" n="e" s="T194">ɨrɨbnajga, </ts>
               <ts e="T196" id="Seg_1895" n="e" s="T195">Kasʼistajga, </ts>
               <ts e="T197" id="Seg_1897" n="e" s="T196">Nasku͡oga. </ts>
               <ts e="T198" id="Seg_1899" n="e" s="T197">Nasku͡oga </ts>
               <ts e="T199" id="Seg_1901" n="e" s="T198">bu͡o </ts>
               <ts e="T200" id="Seg_1903" n="e" s="T199">agɨs </ts>
               <ts e="T201" id="Seg_1905" n="e" s="T200">klaːhɨ </ts>
               <ts e="T202" id="Seg_1907" n="e" s="T201">büten </ts>
               <ts e="T203" id="Seg_1909" n="e" s="T202">baran </ts>
               <ts e="T204" id="Seg_1911" n="e" s="T203">bu͡olla </ts>
               <ts e="T205" id="Seg_1913" n="e" s="T204">bihigini </ts>
               <ts e="T206" id="Seg_1915" n="e" s="T205">kimŋe </ts>
               <ts e="T207" id="Seg_1917" n="e" s="T206">ɨːppɨttara, </ts>
               <ts e="T208" id="Seg_1919" n="e" s="T207">Krasnajarskijga. </ts>
               <ts e="T209" id="Seg_1921" n="e" s="T208">Onno </ts>
               <ts e="T210" id="Seg_1923" n="e" s="T209">bu͡olla </ts>
               <ts e="T211" id="Seg_1925" n="e" s="T210">hürbe </ts>
               <ts e="T212" id="Seg_1927" n="e" s="T211">hettis </ts>
               <ts e="T213" id="Seg_1929" n="e" s="T212">ošku͡olaga </ts>
               <ts e="T214" id="Seg_1931" n="e" s="T213">ü͡öremmippit, </ts>
               <ts e="T215" id="Seg_1933" n="e" s="T214">dʼevʼatɨj </ts>
               <ts e="T216" id="Seg_1935" n="e" s="T215">dʼesʼatɨj </ts>
               <ts e="T217" id="Seg_1937" n="e" s="T216">kɨlaːhɨ. </ts>
               <ts e="T218" id="Seg_1939" n="e" s="T217">Onton </ts>
               <ts e="T219" id="Seg_1941" n="e" s="T218">bu͡olla </ts>
               <ts e="T220" id="Seg_1943" n="e" s="T219">eː </ts>
               <ts e="T221" id="Seg_1945" n="e" s="T220">((PAUSE)) </ts>
               <ts e="T222" id="Seg_1947" n="e" s="T221">agɨhu͡on </ts>
               <ts e="T223" id="Seg_1949" n="e" s="T222">dʼɨllaːkka </ts>
               <ts e="T224" id="Seg_1951" n="e" s="T223">bɨla, </ts>
               <ts e="T225" id="Seg_1953" n="e" s="T224">bihigi </ts>
               <ts e="T226" id="Seg_1955" n="e" s="T225">bu͡olla </ts>
               <ts e="T227" id="Seg_1957" n="e" s="T226">((…)) </ts>
               <ts e="T228" id="Seg_1959" n="e" s="T227">kim </ts>
               <ts e="T229" id="Seg_1961" n="e" s="T228">kimŋe </ts>
               <ts e="T230" id="Seg_1963" n="e" s="T229">pastupajdaːbɨtɨm </ts>
               <ts e="T231" id="Seg_1965" n="e" s="T230">Krasnajarskij </ts>
               <ts e="T232" id="Seg_1967" n="e" s="T231">Gasudarstvʼennɨj </ts>
               <ts e="T233" id="Seg_1969" n="e" s="T232">Mʼedʼicɨnskij </ts>
               <ts e="T234" id="Seg_1971" n="e" s="T233">Instʼitut. </ts>
               <ts e="T235" id="Seg_1973" n="e" s="T234">Onno </ts>
               <ts e="T236" id="Seg_1975" n="e" s="T235">bu͡o </ts>
               <ts e="T237" id="Seg_1977" n="e" s="T236">alta </ts>
               <ts e="T238" id="Seg_1979" n="e" s="T237">dʼɨlɨ </ts>
               <ts e="T239" id="Seg_1981" n="e" s="T238">ü͡öremmitim. </ts>
               <ts e="T240" id="Seg_1983" n="e" s="T239">Onton </ts>
               <ts e="T241" id="Seg_1985" n="e" s="T240">bu͡o </ts>
               <ts e="T242" id="Seg_1987" n="e" s="T241">hettis </ts>
               <ts e="T243" id="Seg_1989" n="e" s="T242">dʼɨlɨm </ts>
               <ts e="T244" id="Seg_1991" n="e" s="T243">intʼernatuːra, </ts>
               <ts e="T245" id="Seg_1993" n="e" s="T244">pa </ts>
               <ts e="T246" id="Seg_1995" n="e" s="T245">tubʼerkuloːzu </ts>
               <ts e="T247" id="Seg_1997" n="e" s="T246">intʼernatuːra </ts>
               <ts e="T248" id="Seg_1999" n="e" s="T247">biːr </ts>
               <ts e="T249" id="Seg_2001" n="e" s="T248">dʼɨlɨ, </ts>
               <ts e="T250" id="Seg_2003" n="e" s="T249">vot. </ts>
               <ts e="T251" id="Seg_2005" n="e" s="T250">Onu </ts>
               <ts e="T252" id="Seg_2007" n="e" s="T251">büten </ts>
               <ts e="T253" id="Seg_2009" n="e" s="T252">baran </ts>
               <ts e="T254" id="Seg_2011" n="e" s="T253">bu͡o </ts>
               <ts e="T255" id="Seg_2013" n="e" s="T254">miːgin </ts>
               <ts e="T256" id="Seg_2015" n="e" s="T255">bu͡o </ts>
               <ts e="T257" id="Seg_2017" n="e" s="T256">Tajmɨːrga </ts>
               <ts e="T258" id="Seg_2019" n="e" s="T257">ɨːppɨttara </ts>
               <ts e="T259" id="Seg_2021" n="e" s="T258">töröːbüt </ts>
               <ts e="T260" id="Seg_2023" n="e" s="T259">ke </ts>
               <ts e="T261" id="Seg_2025" n="e" s="T260">hirber </ts>
               <ts e="T262" id="Seg_2027" n="e" s="T261">ke </ts>
               <ts e="T263" id="Seg_2029" n="e" s="T262">üleli͡eppin. </ts>
               <ts e="T264" id="Seg_2031" n="e" s="T263">Dudʼinkaga </ts>
               <ts e="T265" id="Seg_2033" n="e" s="T264">kelen </ts>
               <ts e="T266" id="Seg_2035" n="e" s="T265">baran </ts>
               <ts e="T267" id="Seg_2037" n="e" s="T266">bu͡o </ts>
               <ts e="T268" id="Seg_2039" n="e" s="T267">minigin </ts>
               <ts e="T269" id="Seg_2041" n="e" s="T268">bu͡o </ts>
               <ts e="T270" id="Seg_2043" n="e" s="T269">Pataːpavaga </ts>
               <ts e="T271" id="Seg_2045" n="e" s="T270">ɨːppɨttara. </ts>
               <ts e="T272" id="Seg_2047" n="e" s="T271">Onno </ts>
               <ts e="T273" id="Seg_2049" n="e" s="T272">kim </ts>
               <ts e="T274" id="Seg_2051" n="e" s="T273">baːr </ts>
               <ts e="T275" id="Seg_2053" n="e" s="T274">ete, </ts>
               <ts e="T276" id="Seg_2055" n="e" s="T275">Lʼesnʼaja </ts>
               <ts e="T277" id="Seg_2057" n="e" s="T276">škola. </ts>
               <ts e="T278" id="Seg_2059" n="e" s="T277">Hu͡ok. </ts>
               <ts e="T279" id="Seg_2061" n="e" s="T278">Pataːpavskaja </ts>
               <ts e="T280" id="Seg_2063" n="e" s="T279">lʼesnaja </ts>
               <ts e="T281" id="Seg_2065" n="e" s="T280">škoːla </ts>
               <ts e="T282" id="Seg_2067" n="e" s="T281">ete. </ts>
               <ts e="T283" id="Seg_2069" n="e" s="T282">((COUGH)) </ts>
               <ts e="T284" id="Seg_2071" n="e" s="T283">Vot </ts>
               <ts e="T285" id="Seg_2073" n="e" s="T284">onno </ts>
               <ts e="T286" id="Seg_2075" n="e" s="T285">bu͡olla </ts>
               <ts e="T287" id="Seg_2077" n="e" s="T286">üs </ts>
               <ts e="T288" id="Seg_2079" n="e" s="T287">dʼɨlɨ </ts>
               <ts e="T289" id="Seg_2081" n="e" s="T288">üleleːbitim. </ts>
               <ts e="T290" id="Seg_2083" n="e" s="T289">Vot, </ts>
               <ts e="T291" id="Seg_2085" n="e" s="T290">agɨhu͡on </ts>
               <ts e="T292" id="Seg_2087" n="e" s="T291">tördüsteːk, </ts>
               <ts e="T293" id="Seg_2089" n="e" s="T292">agɨhu͡on </ts>
               <ts e="T294" id="Seg_2091" n="e" s="T293">tördüsteːk </ts>
               <ts e="T295" id="Seg_2093" n="e" s="T294">dʼɨlga </ts>
               <ts e="T296" id="Seg_2095" n="e" s="T295">munna </ts>
               <ts e="T297" id="Seg_2097" n="e" s="T296">kelbitim </ts>
               <ts e="T298" id="Seg_2099" n="e" s="T297">köhön, </ts>
               <ts e="T299" id="Seg_2101" n="e" s="T298">pʼerʼevoːdam </ts>
               <ts e="T300" id="Seg_2103" n="e" s="T299">ɨːppɨttara. </ts>
               <ts e="T301" id="Seg_2105" n="e" s="T300">I </ts>
               <ts e="T302" id="Seg_2107" n="e" s="T301">hol </ts>
               <ts e="T303" id="Seg_2109" n="e" s="T302">dʼɨltan </ts>
               <ts e="T304" id="Seg_2111" n="e" s="T303">bu </ts>
               <ts e="T305" id="Seg_2113" n="e" s="T304">anɨga </ts>
               <ts e="T306" id="Seg_2115" n="e" s="T305">di͡eri </ts>
               <ts e="T307" id="Seg_2117" n="e" s="T306">bu͡olla </ts>
               <ts e="T308" id="Seg_2119" n="e" s="T307">kimi͡eke </ts>
               <ts e="T309" id="Seg_2121" n="e" s="T308">üleliːbin, </ts>
               <ts e="T310" id="Seg_2123" n="e" s="T309">v </ts>
               <ts e="T311" id="Seg_2125" n="e" s="T310">krajevom </ts>
               <ts e="T312" id="Seg_2127" n="e" s="T311">pratʼivatubʼerkuloznam </ts>
               <ts e="T313" id="Seg_2129" n="e" s="T312">dʼispanserʼe </ts>
               <ts e="T314" id="Seg_2131" n="e" s="T313">nomʼer </ts>
               <ts e="T315" id="Seg_2133" n="e" s="T314">dʼevʼatʼ, </ts>
               <ts e="T317" id="Seg_2135" n="e" s="T315">vračʼ-ftʼizʼiatʼer. </ts>
               <ts e="T318" id="Seg_2137" n="e" s="T317">Onton </ts>
               <ts e="T319" id="Seg_2139" n="e" s="T318">bu͡o </ts>
               <ts e="T320" id="Seg_2141" n="e" s="T319">agɨhu͡on </ts>
               <ts e="T321" id="Seg_2143" n="e" s="T320">toksustaːk, </ts>
               <ts e="T322" id="Seg_2145" n="e" s="T321">i </ts>
               <ts e="T323" id="Seg_2147" n="e" s="T322">toksustaːk </ts>
               <ts e="T324" id="Seg_2149" n="e" s="T323">dʼɨlga </ts>
               <ts e="T325" id="Seg_2151" n="e" s="T324">bu͡olla </ts>
               <ts e="T326" id="Seg_2153" n="e" s="T325">kim </ts>
               <ts e="T327" id="Seg_2155" n="e" s="T326">bu͡olbut, </ts>
               <ts e="T328" id="Seg_2157" n="e" s="T327">zʼemlʼetrʼasʼenʼije </ts>
               <ts e="T329" id="Seg_2159" n="e" s="T328">bu͡olbut </ts>
               <ts e="T330" id="Seg_2161" n="e" s="T329">Armʼenʼijaga. </ts>
               <ts e="T331" id="Seg_2163" n="e" s="T330">Onno </ts>
               <ts e="T332" id="Seg_2165" n="e" s="T331">bejem </ts>
               <ts e="T333" id="Seg_2167" n="e" s="T332">hanaːbɨnan, </ts>
               <ts e="T334" id="Seg_2169" n="e" s="T333">bʼesplaːtna, </ts>
               <ts e="T335" id="Seg_2171" n="e" s="T334">bʼesvazmʼezdna </ts>
               <ts e="T336" id="Seg_2173" n="e" s="T335">di͡ebikke </ts>
               <ts e="T337" id="Seg_2175" n="e" s="T336">dʼe </ts>
               <ts e="T338" id="Seg_2177" n="e" s="T337">onno </ts>
               <ts e="T339" id="Seg_2179" n="e" s="T338">üleleːbitim </ts>
               <ts e="T340" id="Seg_2181" n="e" s="T339">ikki </ts>
               <ts e="T341" id="Seg_2183" n="e" s="T340">(dʼ-) </ts>
               <ts e="T342" id="Seg_2185" n="e" s="T341">dva </ts>
               <ts e="T343" id="Seg_2187" n="e" s="T342">goda </ts>
               <ts e="T344" id="Seg_2189" n="e" s="T343">padrʼad. </ts>
               <ts e="T345" id="Seg_2191" n="e" s="T344">V </ts>
               <ts e="T346" id="Seg_2193" n="e" s="T345">pratiʼvatubʼerkuloːznam </ts>
               <ts e="T347" id="Seg_2195" n="e" s="T346">dʼispanserʼe </ts>
               <ts e="T348" id="Seg_2197" n="e" s="T347">imʼenʼi </ts>
               <ts e="T349" id="Seg_2199" n="e" s="T348">Mʼelkanʼaːna </ts>
               <ts e="T350" id="Seg_2201" n="e" s="T349">goːrada </ts>
               <ts e="T351" id="Seg_2203" n="e" s="T350">Lʼenʼinakana, </ts>
               <ts e="T352" id="Seg_2205" n="e" s="T351">vot. </ts>
               <ts e="T353" id="Seg_2207" n="e" s="T352">Agɨhu͡on, </ts>
               <ts e="T354" id="Seg_2209" n="e" s="T353">((NOISE)), </ts>
               <ts e="T355" id="Seg_2211" n="e" s="T354">v </ts>
               <ts e="T356" id="Seg_2213" n="e" s="T355">dvuxtɨsʼicnɨj </ts>
               <ts e="T357" id="Seg_2215" n="e" s="T356">pʼatam </ts>
               <ts e="T358" id="Seg_2217" n="e" s="T357">bu </ts>
               <ts e="T359" id="Seg_2219" n="e" s="T358">du͡o </ts>
               <ts e="T360" id="Seg_2221" n="e" s="T359">kim </ts>
               <ts e="T361" id="Seg_2223" n="e" s="T360">bi͡erbittere </ts>
               <ts e="T362" id="Seg_2225" n="e" s="T361">mini͡eke </ts>
               <ts e="T363" id="Seg_2227" n="e" s="T362">počʼotnuju </ts>
               <ts e="T364" id="Seg_2229" n="e" s="T363">gramatu </ts>
               <ts e="T365" id="Seg_2231" n="e" s="T364">Mʼinʼistʼerstva </ts>
               <ts e="T366" id="Seg_2233" n="e" s="T365">zdravaaxranʼenʼija, </ts>
               <ts e="T367" id="Seg_2235" n="e" s="T366">eta </ts>
               <ts e="T368" id="Seg_2237" n="e" s="T367">sajuznavo </ts>
               <ts e="T369" id="Seg_2239" n="e" s="T368">značʼenʼije </ts>
               <ts e="T370" id="Seg_2241" n="e" s="T369">kak </ts>
               <ts e="T371" id="Seg_2243" n="e" s="T370">vʼetʼeran </ts>
               <ts e="T372" id="Seg_2245" n="e" s="T371">truda, </ts>
               <ts e="T373" id="Seg_2247" n="e" s="T372">vot. </ts>
               <ts e="T374" id="Seg_2249" n="e" s="T373">Kergetterbin </ts>
               <ts e="T375" id="Seg_2251" n="e" s="T374">gɨtta, </ts>
               <ts e="T376" id="Seg_2253" n="e" s="T375">inʼebin </ts>
               <ts e="T377" id="Seg_2255" n="e" s="T376">gɨtta </ts>
               <ts e="T378" id="Seg_2257" n="e" s="T377">olorobun </ts>
               <ts e="T379" id="Seg_2259" n="e" s="T378">anɨ, </ts>
               <ts e="T380" id="Seg_2261" n="e" s="T379">(plemʼanʼi-) </ts>
               <ts e="T381" id="Seg_2263" n="e" s="T380">plemʼanʼisabɨn </ts>
               <ts e="T382" id="Seg_2265" n="e" s="T381">gɨtta. </ts>
               <ts e="T383" id="Seg_2267" n="e" s="T382">Ontum </ts>
               <ts e="T384" id="Seg_2269" n="e" s="T383">bu͡olla </ts>
               <ts e="T385" id="Seg_2271" n="e" s="T384">onus </ts>
               <ts e="T386" id="Seg_2273" n="e" s="T385">klaska </ts>
               <ts e="T387" id="Seg_2275" n="e" s="T386">ü͡örener. </ts>
               <ts e="T388" id="Seg_2277" n="e" s="T387">Bejem </ts>
               <ts e="T389" id="Seg_2279" n="e" s="T388">bi͡ek </ts>
               <ts e="T390" id="Seg_2281" n="e" s="T389">üleliːbin. </ts>
               <ts e="T391" id="Seg_2283" n="e" s="T390">A </ts>
               <ts e="T392" id="Seg_2285" n="e" s="T391">bihigini </ts>
               <ts e="T393" id="Seg_2287" n="e" s="T392">bu͡o </ts>
               <ts e="T394" id="Seg_2289" n="e" s="T393">behis </ts>
               <ts e="T395" id="Seg_2291" n="e" s="T394">klasska </ts>
               <ts e="T396" id="Seg_2293" n="e" s="T395">bu͡o, </ts>
               <ts e="T397" id="Seg_2295" n="e" s="T396">behis </ts>
               <ts e="T398" id="Seg_2297" n="e" s="T397">klasska </ts>
               <ts e="T399" id="Seg_2299" n="e" s="T398">ü͡örener </ts>
               <ts e="T400" id="Seg_2301" n="e" s="T399">erdekpitinen </ts>
               <ts e="T401" id="Seg_2303" n="e" s="T400">bu͡o, </ts>
               <ts e="T402" id="Seg_2305" n="e" s="T401">Nasku͡oga </ts>
               <ts e="T403" id="Seg_2307" n="e" s="T402">bu͡olla, </ts>
               <ts e="T404" id="Seg_2309" n="e" s="T403">dom kulʼturaga </ts>
               <ts e="T405" id="Seg_2311" n="e" s="T404">bu͡olla, </ts>
               <ts e="T406" id="Seg_2313" n="e" s="T405">bihigini </ts>
               <ts e="T407" id="Seg_2315" n="e" s="T406">kim </ts>
               <ts e="T408" id="Seg_2317" n="e" s="T407">arganʼizavala </ts>
               <ts e="T409" id="Seg_2319" n="e" s="T408">kružok </ts>
               <ts e="T410" id="Seg_2321" n="e" s="T409">barganʼistav </ts>
               <ts e="T411" id="Seg_2323" n="e" s="T410">naša </ts>
               <ts e="T412" id="Seg_2325" n="e" s="T411">dalganskaja </ts>
               <ts e="T413" id="Seg_2327" n="e" s="T412">paetessa </ts>
               <ts e="T414" id="Seg_2329" n="e" s="T413">Ogdu͡o </ts>
               <ts e="T415" id="Seg_2331" n="e" s="T414">Aksʼonava. </ts>
               <ts e="T416" id="Seg_2333" n="e" s="T415">Ol </ts>
               <ts e="T417" id="Seg_2335" n="e" s="T416">onu </ts>
               <ts e="T418" id="Seg_2337" n="e" s="T417">(bi-) </ts>
               <ts e="T419" id="Seg_2339" n="e" s="T418">bihigini </ts>
               <ts e="T420" id="Seg_2341" n="e" s="T419">kimi͡eke </ts>
               <ts e="T421" id="Seg_2343" n="e" s="T420">ü͡öreppite </ts>
               <ts e="T422" id="Seg_2345" n="e" s="T421">bargaːŋŋa </ts>
               <ts e="T423" id="Seg_2347" n="e" s="T422">oːnnʼuː. </ts>
               <ts e="T424" id="Seg_2349" n="e" s="T423">Anɨ </ts>
               <ts e="T425" id="Seg_2351" n="e" s="T424">bu͡olla, </ts>
               <ts e="T426" id="Seg_2353" n="e" s="T425">da </ts>
               <ts e="T427" id="Seg_2355" n="e" s="T426">sʼix </ts>
               <ts e="T428" id="Seg_2357" n="e" s="T427">por </ts>
               <ts e="T429" id="Seg_2359" n="e" s="T428">kɨrdʼɨ͡akpar </ts>
               <ts e="T430" id="Seg_2361" n="e" s="T429">di͡eri </ts>
               <ts e="T431" id="Seg_2363" n="e" s="T430">bi͡ek </ts>
               <ts e="T432" id="Seg_2365" n="e" s="T431">oːnnʼuːbun </ts>
               <ts e="T433" id="Seg_2367" n="e" s="T432">anɨ. </ts>
               <ts e="T434" id="Seg_2369" n="e" s="T433">Vot, </ts>
               <ts e="T435" id="Seg_2371" n="e" s="T434">Jakuːtskajgɨttan </ts>
               <ts e="T436" id="Seg_2373" n="e" s="T435">kimnere </ts>
               <ts e="T437" id="Seg_2375" n="e" s="T436">kelbit, </ts>
               <ts e="T438" id="Seg_2377" n="e" s="T437">artʼistar </ts>
               <ts e="T439" id="Seg_2379" n="e" s="T438">kelbit </ts>
               <ts e="T440" id="Seg_2381" n="e" s="T439">etilere, </ts>
               <ts e="T441" id="Seg_2383" n="e" s="T440">olor </ts>
               <ts e="T442" id="Seg_2385" n="e" s="T441">bihi͡eke </ts>
               <ts e="T443" id="Seg_2387" n="e" s="T442">bargan </ts>
               <ts e="T444" id="Seg_2389" n="e" s="T443">atɨlaːbɨttara. </ts>
               <ts e="T445" id="Seg_2391" n="e" s="T444">Tagda </ts>
               <ts e="T446" id="Seg_2393" n="e" s="T445">bu </ts>
               <ts e="T447" id="Seg_2395" n="e" s="T446">ulakan </ts>
               <ts e="T448" id="Seg_2397" n="e" s="T447">karčɨ </ts>
               <ts e="T449" id="Seg_2399" n="e" s="T448">ete </ts>
               <ts e="T450" id="Seg_2401" n="e" s="T449">bu͡o </ts>
               <ts e="T451" id="Seg_2403" n="e" s="T450">agɨhu͡on </ts>
               <ts e="T452" id="Seg_2405" n="e" s="T451">agɨhu͡on </ts>
               <ts e="T453" id="Seg_2407" n="e" s="T452">rublʼ. </ts>
               <ts e="T454" id="Seg_2409" n="e" s="T453">Anɨ </ts>
               <ts e="T455" id="Seg_2411" n="e" s="T454">bu͡o </ts>
               <ts e="T456" id="Seg_2413" n="e" s="T455">bi͡ek </ts>
               <ts e="T457" id="Seg_2415" n="e" s="T456">oːnnʼuːbun, </ts>
               <ts e="T458" id="Seg_2417" n="e" s="T457">hanaːm </ts>
               <ts e="T459" id="Seg_2419" n="e" s="T458">kellegine, </ts>
               <ts e="T460" id="Seg_2421" n="e" s="T459">anɨ </ts>
               <ts e="T461" id="Seg_2423" n="e" s="T460">emi͡e </ts>
               <ts e="T462" id="Seg_2425" n="e" s="T461">ehi͡eke </ts>
               <ts e="T463" id="Seg_2427" n="e" s="T462">min </ts>
               <ts e="T464" id="Seg_2429" n="e" s="T463">oːnnʼu͡om. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KiLS">
            <ta e="T6" id="Seg_2430" s="T1">KiLS_KiES_2009_Life_nar.KiLS.001 (001.001)</ta>
            <ta e="T12" id="Seg_2431" s="T6">KiLS_KiES_2009_Life_nar.KiLS.002 (001.002)</ta>
            <ta e="T19" id="Seg_2432" s="T12">KiLS_KiES_2009_Life_nar.KiLS.003 (001.003)</ta>
            <ta e="T22" id="Seg_2433" s="T19">KiLS_KiES_2009_Life_nar.KiLS.004 (001.004)</ta>
            <ta e="T27" id="Seg_2434" s="T24">KiLS_KiES_2009_Life_nar.KiLS.005 (001.006)</ta>
            <ta e="T48" id="Seg_2435" s="T29">KiLS_KiES_2009_Life_nar.KiLS.006 (001.008)</ta>
            <ta e="T51" id="Seg_2436" s="T48">KiLS_KiES_2009_Life_nar.KiLS.007 (001.009)</ta>
            <ta e="T55" id="Seg_2437" s="T51">KiLS_KiES_2009_Life_nar.KiLS.008 (001.010)</ta>
            <ta e="T59" id="Seg_2438" s="T57">KiLS_KiES_2009_Life_nar.KiLS.009 (001.012)</ta>
            <ta e="T62" id="Seg_2439" s="T60">KiLS_KiES_2009_Life_nar.KiLS.010 (001.014)</ta>
            <ta e="T73" id="Seg_2440" s="T62">KiLS_KiES_2009_Life_nar.KiLS.011 (001.015)</ta>
            <ta e="T87" id="Seg_2441" s="T73">KiLS_KiES_2009_Life_nar.KiLS.012 (001.016)</ta>
            <ta e="T99" id="Seg_2442" s="T87">KiLS_KiES_2009_Life_nar.KiLS.013 (001.017)</ta>
            <ta e="T106" id="Seg_2443" s="T99">KiLS_KiES_2009_Life_nar.KiLS.014 (001.018)</ta>
            <ta e="T127" id="Seg_2444" s="T106">KiLS_KiES_2009_Life_nar.KiLS.015 (001.019)</ta>
            <ta e="T144" id="Seg_2445" s="T131">KiLS_KiES_2009_Life_nar.KiLS.016 (001.021)</ta>
            <ta e="T160" id="Seg_2446" s="T150">KiLS_KiES_2009_Life_nar.KiLS.017 (001.023)</ta>
            <ta e="T172" id="Seg_2447" s="T160">KiLS_KiES_2009_Life_nar.KiLS.018 (001.024)</ta>
            <ta e="T180" id="Seg_2448" s="T172">KiLS_KiES_2009_Life_nar.KiLS.019 (001.025)</ta>
            <ta e="T189" id="Seg_2449" s="T180">KiLS_KiES_2009_Life_nar.KiLS.020 (001.026)</ta>
            <ta e="T197" id="Seg_2450" s="T189">KiLS_KiES_2009_Life_nar.KiLS.021 (001.027)</ta>
            <ta e="T208" id="Seg_2451" s="T197">KiLS_KiES_2009_Life_nar.KiLS.022 (001.028)</ta>
            <ta e="T217" id="Seg_2452" s="T208">KiLS_KiES_2009_Life_nar.KiLS.023 (001.029)</ta>
            <ta e="T234" id="Seg_2453" s="T217">KiLS_KiES_2009_Life_nar.KiLS.024 (001.030)</ta>
            <ta e="T239" id="Seg_2454" s="T234">KiLS_KiES_2009_Life_nar.KiLS.025 (001.031)</ta>
            <ta e="T250" id="Seg_2455" s="T239">KiLS_KiES_2009_Life_nar.KiLS.026 (001.032)</ta>
            <ta e="T263" id="Seg_2456" s="T250">KiLS_KiES_2009_Life_nar.KiLS.027 (001.033)</ta>
            <ta e="T271" id="Seg_2457" s="T263">KiLS_KiES_2009_Life_nar.KiLS.028 (001.034)</ta>
            <ta e="T277" id="Seg_2458" s="T271">KiLS_KiES_2009_Life_nar.KiLS.029 (001.035)</ta>
            <ta e="T278" id="Seg_2459" s="T277">KiLS_KiES_2009_Life_nar.KiLS.030 (001.036)</ta>
            <ta e="T282" id="Seg_2460" s="T278">KiLS_KiES_2009_Life_nar.KiLS.031 (001.037)</ta>
            <ta e="T289" id="Seg_2461" s="T282">KiLS_KiES_2009_Life_nar.KiLS.032 (001.038)</ta>
            <ta e="T300" id="Seg_2462" s="T289">KiLS_KiES_2009_Life_nar.KiLS.033 (001.039)</ta>
            <ta e="T317" id="Seg_2463" s="T300">KiLS_KiES_2009_Life_nar.KiLS.034 (001.040)</ta>
            <ta e="T330" id="Seg_2464" s="T317">KiLS_KiES_2009_Life_nar.KiLS.035 (001.041)</ta>
            <ta e="T344" id="Seg_2465" s="T330">KiLS_KiES_2009_Life_nar.KiLS.036 (001.042)</ta>
            <ta e="T352" id="Seg_2466" s="T344">KiLS_KiES_2009_Life_nar.KiLS.037 (001.043)</ta>
            <ta e="T373" id="Seg_2467" s="T352">KiLS_KiES_2009_Life_nar.KiLS.038 (001.044)</ta>
            <ta e="T382" id="Seg_2468" s="T373">KiLS_KiES_2009_Life_nar.KiLS.039 (001.045)</ta>
            <ta e="T387" id="Seg_2469" s="T382">KiLS_KiES_2009_Life_nar.KiLS.040 (001.046)</ta>
            <ta e="T390" id="Seg_2470" s="T387">KiLS_KiES_2009_Life_nar.KiLS.041 (001.047)</ta>
            <ta e="T415" id="Seg_2471" s="T390">KiLS_KiES_2009_Life_nar.KiLS.042 (001.048)</ta>
            <ta e="T423" id="Seg_2472" s="T415">KiLS_KiES_2009_Life_nar.KiLS.043 (001.049)</ta>
            <ta e="T433" id="Seg_2473" s="T423">KiLS_KiES_2009_Life_nar.KiLS.044 (001.050)</ta>
            <ta e="T444" id="Seg_2474" s="T433">KiLS_KiES_2009_Life_nar.KiLS.045 (001.051)</ta>
            <ta e="T453" id="Seg_2475" s="T444">KiLS_KiES_2009_Life_nar.KiLS.046 (001.052)</ta>
            <ta e="T464" id="Seg_2476" s="T453">KiLS_KiES_2009_Life_nar.KiLS.047 (001.053)</ta>
         </annotation>
         <annotation name="st" tierref="st-KiLS" />
         <annotation name="ts" tierref="ts-KiLS">
            <ta e="T6" id="Seg_2477" s="T1">– Min aːtɨm Kʼirgʼizava Lʼina Sʼemʼonavna. </ta>
            <ta e="T12" id="Seg_2478" s="T6">Min, min töröːbütüm bu͡olla Nosku͡oga Sɨndasskaga. </ta>
            <ta e="T19" id="Seg_2479" s="T12">Kergetterim bu͡olla tɨ͡ataːgɨ etilere, kimner kim tabahɨttar. </ta>
            <ta e="T22" id="Seg_2480" s="T19">Korgoːgo töröːbütüm. </ta>
            <ta e="T27" id="Seg_2481" s="T24">– Paːččɨttar etilere. </ta>
            <ta e="T48" id="Seg_2482" s="T29">– Inʼem bu͡olla i inʼem kepsiːre bu͡olla min töröːbüpper bu͡ol min bu͡o kim töröːbüpper bu͡o kim tohogo uːrbuttar tɨ͡aga. </ta>
            <ta e="T51" id="Seg_2483" s="T48">Onno bu͡o kɨhɨ͡ak. </ta>
            <ta e="T55" id="Seg_2484" s="T51">Tu͡ok baːr ješʼo? </ta>
            <ta e="T59" id="Seg_2485" s="T57">– Gedereː. </ta>
            <ta e="T62" id="Seg_2486" s="T60">– Hoŋohoːn. </ta>
            <ta e="T73" id="Seg_2487" s="T62">Onu onu onu bu͡olla maska baːjan baran bu, ol aːta tohogo. </ta>
            <ta e="T87" id="Seg_2488" s="T73">Bu ke hirge uːran keːspitter, ontuŋ, ol tohogoŋ ol tohogoŋ bu bi͡ek to baːr. </ta>
            <ta e="T99" id="Seg_2489" s="T87">Onton bu͡olla, min hette hette hette dʼɨllaːk erdeppinen bu͡ollagɨna, kim Hɨndasskaga kiːrbippit. </ta>
            <ta e="T106" id="Seg_2490" s="T99">Tak-ta bihigi bi͡ek tɨ͡aga hɨldʼaːččɨ etibit, inʼe. </ta>
            <ta e="T127" id="Seg_2491" s="T106">Bu elbek bagajɨ ü͡ördeːk, (elbe-) elbek bagajɨ ü͡ördeːk etibit, elbek ɨ͡allar etilere, kös kihi altalɨː, bi͡estiː baloktor urahalar hɨldʼaːččɨbit, inʼe. </ta>
            <ta e="T144" id="Seg_2492" s="T131">– Onton bihigi ješʼo oŋu͡or hɨppɨppɨt ürdük kajaga, onno altalɨː bi͡estiː uraha dʼi͡e. </ta>
            <ta e="T160" id="Seg_2493" s="T150">– Onno kim, ikki ikki ikki ogo ikki agaj ogoloːk ete. </ta>
            <ta e="T172" id="Seg_2494" s="T160">Onno oŋu͡or bu͡olla kim ürdük kajaga altalɨː bi͡estiː uraha dʼi͡e turaːččɨ etilere. </ta>
            <ta e="T180" id="Seg_2495" s="T172">Onno bu͡olla rɨbaktar haːjɨn bu͡olla rɨbaktaːččɨ etilere, vot. </ta>
            <ta e="T189" id="Seg_2496" s="T180">Onton bu͡o min (Hɨnda-) Hɨndasskaga kiːrbippit, teːtem ölön ölön. </ta>
            <ta e="T197" id="Seg_2497" s="T189">Onno bu͡o bihigi bu͡olla ü͡öremmippit ɨrɨbnajga, Kasʼistajga, Nasku͡oga. </ta>
            <ta e="T208" id="Seg_2498" s="T197">Nasku͡oga bu͡o agɨs klaːhɨ büten baran bu͡olla bihigini kimŋe ɨːppɨttara, Krasnajarskijga. </ta>
            <ta e="T217" id="Seg_2499" s="T208">Onno bu͡olla hürbe hettis ošku͡olaga ü͡öremmippit, dʼevʼatɨj dʼesʼatɨj kɨlaːhɨ. </ta>
            <ta e="T234" id="Seg_2500" s="T217">Onton bu͡olla eː ((PAUSE)) agɨhu͡on dʼɨllaːkka bɨla, bihigi bu͡olla ((…)) kim kimŋe pastupajdaːbɨtɨm Krasnajarskij Gasudarstvʼennɨj Mʼedʼicɨnskij Instʼitut. </ta>
            <ta e="T239" id="Seg_2501" s="T234">Onno bu͡o alta dʼɨlɨ ü͡öremmitim. </ta>
            <ta e="T250" id="Seg_2502" s="T239">Onton bu͡o hettis dʼɨlɨm intʼernatuːra, pa tubʼerkuloːzu intʼernatuːra biːr dʼɨlɨ, vot. </ta>
            <ta e="T263" id="Seg_2503" s="T250">Onu büten baran bu͡o miːgin bu͡o Tajmɨːrga ɨːppɨttara töröːbüt ke hirber ke üleli͡eppin. </ta>
            <ta e="T271" id="Seg_2504" s="T263">Dudʼinkaga kelen baran bu͡o minigin bu͡o Pataːpavaga ɨːppɨttara. </ta>
            <ta e="T277" id="Seg_2505" s="T271">Onno kim baːr ete, Lʼesnʼaja škola. </ta>
            <ta e="T278" id="Seg_2506" s="T277">Hu͡ok. </ta>
            <ta e="T282" id="Seg_2507" s="T278">Pataːpavskaja lʼesnaja škoːla ete. </ta>
            <ta e="T289" id="Seg_2508" s="T282">((COUGH)) Vot onno bu͡olla üs dʼɨlɨ üleleːbitim. </ta>
            <ta e="T300" id="Seg_2509" s="T289">Vot, agɨhu͡on tördüsteːk, agɨhu͡on tördüsteːk dʼɨlga munna kelbitim köhön, pʼerʼevoːdam ɨːppɨttara. </ta>
            <ta e="T317" id="Seg_2510" s="T300">I hol dʼɨltan bu anɨga di͡eri bu͡olla kimi͡eke üleliːbin, v krajevom pratʼivatubʼerkuloznam dʼispanserʼe nomʼer dʼevʼatʼ, vračʼ-ftʼizʼiatʼer. </ta>
            <ta e="T330" id="Seg_2511" s="T317">Onton bu͡o agɨhu͡on toksustaːk, i toksustaːk dʼɨlga bu͡olla kim bu͡olbut, zʼemlʼetrʼasʼenʼije bu͡olbut Armʼenʼijaga. </ta>
            <ta e="T344" id="Seg_2512" s="T330">Onno bejem hanaːbɨnan, bʼesplaːtna, bʼesvazmʼezdna di͡ebikke dʼe onno üleleːbitim ikki (dʼ-) dva goda padrʼad. </ta>
            <ta e="T352" id="Seg_2513" s="T344">V pratiʼvatubʼerkuloːznam dʼispanserʼe imʼenʼi Mʼelkanʼaːna goːrada Lʼenʼinakana, vot. </ta>
            <ta e="T373" id="Seg_2514" s="T352">Agɨhu͡on, ((NOISE)), v dvuxtɨsʼicnɨj pʼatam bu du͡o kim bi͡erbittere mini͡eke počʼotnuju gramatu Mʼinʼistʼerstva zdravaaxranʼenʼija, eta sajuznavo značʼenʼije kak vʼetʼeran truda, vot. </ta>
            <ta e="T382" id="Seg_2515" s="T373">Kergetterbin gɨtta, inʼebin gɨtta olorobun anɨ, (plemʼanʼi-) plemʼanʼisabɨn gɨtta. </ta>
            <ta e="T387" id="Seg_2516" s="T382">Ontum bu͡olla onus klaska ü͡örener. </ta>
            <ta e="T390" id="Seg_2517" s="T387">Bejem bi͡ek üleliːbin. </ta>
            <ta e="T415" id="Seg_2518" s="T390">A bihigini bu͡o behis klasska bu͡o, behis klasska ü͡örener erdekpitinen bu͡o, Nasku͡oga bu͡olla, dom kulʼturaga bu͡olla, bihigini kim arganʼizavala kružok barganʼistav naša dalganskaja paetessa Ogdu͡o Aksʼonava. </ta>
            <ta e="T423" id="Seg_2519" s="T415">Ol onu (bi-) ((COUGH)) bihigini kimi͡eke ü͡öreppite bargaːŋŋa oːnnʼuː. </ta>
            <ta e="T433" id="Seg_2520" s="T423">Anɨ bu͡olla, da sʼix por kɨrdʼɨ͡akpar di͡eri bi͡ek oːnnʼuːbun anɨ. </ta>
            <ta e="T444" id="Seg_2521" s="T433">Vot, Jakuːtskajgɨttan kimnere kelbit, artʼistar kelbit etilere, olor bihi͡eke bargan atɨlaːbɨttara. </ta>
            <ta e="T453" id="Seg_2522" s="T444">Tagda bu ulakan karčɨ ete bu͡o agɨhu͡on agɨhu͡on rublʼ. </ta>
            <ta e="T464" id="Seg_2523" s="T453">Anɨ bu͡o bi͡ek oːnnʼuːbun, hanaːm kellegine, anɨ emi͡e ehi͡eke min oːnnʼu͡om. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KiLS">
            <ta e="T2" id="Seg_2524" s="T1">min</ta>
            <ta e="T3" id="Seg_2525" s="T2">aːt-ɨ-m</ta>
            <ta e="T4" id="Seg_2526" s="T3">Kʼirgʼizava</ta>
            <ta e="T5" id="Seg_2527" s="T4">Lʼina</ta>
            <ta e="T6" id="Seg_2528" s="T5">Sʼemʼonavna</ta>
            <ta e="T7" id="Seg_2529" s="T6">min</ta>
            <ta e="T8" id="Seg_2530" s="T7">min</ta>
            <ta e="T9" id="Seg_2531" s="T8">töröː-büt-ü-m</ta>
            <ta e="T10" id="Seg_2532" s="T9">bu͡olla</ta>
            <ta e="T11" id="Seg_2533" s="T10">Nosku͡o-ga</ta>
            <ta e="T12" id="Seg_2534" s="T11">Sɨndasska-ga</ta>
            <ta e="T13" id="Seg_2535" s="T12">kerget-ter-i-m</ta>
            <ta e="T14" id="Seg_2536" s="T13">bu͡olla</ta>
            <ta e="T15" id="Seg_2537" s="T14">tɨ͡a-taːgɨ</ta>
            <ta e="T16" id="Seg_2538" s="T15">e-ti-lere</ta>
            <ta e="T17" id="Seg_2539" s="T16">kim-ner</ta>
            <ta e="T18" id="Seg_2540" s="T17">kim</ta>
            <ta e="T19" id="Seg_2541" s="T18">taba-hɨt-tar</ta>
            <ta e="T20" id="Seg_2542" s="T19">Korgoː-go</ta>
            <ta e="T22" id="Seg_2543" s="T20">töröː-büt-ü-m</ta>
            <ta e="T25" id="Seg_2544" s="T24">paːč-čɨt-tar</ta>
            <ta e="T27" id="Seg_2545" s="T25">e-ti-lere</ta>
            <ta e="T31" id="Seg_2546" s="T29">inʼe-m</ta>
            <ta e="T32" id="Seg_2547" s="T31">bu͡olla</ta>
            <ta e="T33" id="Seg_2548" s="T32">i</ta>
            <ta e="T34" id="Seg_2549" s="T33">inʼe-m</ta>
            <ta e="T35" id="Seg_2550" s="T34">kepsiː-r-e</ta>
            <ta e="T36" id="Seg_2551" s="T35">bu͡olla</ta>
            <ta e="T37" id="Seg_2552" s="T36">min</ta>
            <ta e="T38" id="Seg_2553" s="T37">töröː-büp-pe-r</ta>
            <ta e="T39" id="Seg_2554" s="T38">bu͡ol</ta>
            <ta e="T40" id="Seg_2555" s="T39">min</ta>
            <ta e="T41" id="Seg_2556" s="T40">bu͡o</ta>
            <ta e="T42" id="Seg_2557" s="T41">kim</ta>
            <ta e="T43" id="Seg_2558" s="T42">töröː-büp-pe-r</ta>
            <ta e="T44" id="Seg_2559" s="T43">bu͡o</ta>
            <ta e="T45" id="Seg_2560" s="T44">kim</ta>
            <ta e="T46" id="Seg_2561" s="T45">tohogo</ta>
            <ta e="T47" id="Seg_2562" s="T46">uːr-but-tar</ta>
            <ta e="T48" id="Seg_2563" s="T47">tɨ͡a-ga</ta>
            <ta e="T49" id="Seg_2564" s="T48">onno</ta>
            <ta e="T50" id="Seg_2565" s="T49">bu͡o</ta>
            <ta e="T51" id="Seg_2566" s="T50">kɨhɨ͡ak</ta>
            <ta e="T52" id="Seg_2567" s="T51">tu͡ok</ta>
            <ta e="T53" id="Seg_2568" s="T52">baːr</ta>
            <ta e="T55" id="Seg_2569" s="T53">ješʼo</ta>
            <ta e="T59" id="Seg_2570" s="T57">gedereː</ta>
            <ta e="T62" id="Seg_2571" s="T60">hoŋohoːn</ta>
            <ta e="T63" id="Seg_2572" s="T62">o-nu</ta>
            <ta e="T64" id="Seg_2573" s="T63">o-nu</ta>
            <ta e="T65" id="Seg_2574" s="T64">o-nu</ta>
            <ta e="T66" id="Seg_2575" s="T65">bu͡olla</ta>
            <ta e="T67" id="Seg_2576" s="T66">mas-ka</ta>
            <ta e="T68" id="Seg_2577" s="T67">baːj-an</ta>
            <ta e="T69" id="Seg_2578" s="T68">baran</ta>
            <ta e="T70" id="Seg_2579" s="T69">bu</ta>
            <ta e="T71" id="Seg_2580" s="T70">ol</ta>
            <ta e="T72" id="Seg_2581" s="T71">aːt-a</ta>
            <ta e="T73" id="Seg_2582" s="T72">tohogo</ta>
            <ta e="T74" id="Seg_2583" s="T73">bu</ta>
            <ta e="T75" id="Seg_2584" s="T74">ke</ta>
            <ta e="T76" id="Seg_2585" s="T75">hir-ge</ta>
            <ta e="T77" id="Seg_2586" s="T76">uːr-an</ta>
            <ta e="T78" id="Seg_2587" s="T77">keːs-pit-ter</ta>
            <ta e="T79" id="Seg_2588" s="T78">on-tu-ŋ</ta>
            <ta e="T80" id="Seg_2589" s="T79">ol</ta>
            <ta e="T81" id="Seg_2590" s="T80">tohogo-ŋ</ta>
            <ta e="T82" id="Seg_2591" s="T81">ol</ta>
            <ta e="T83" id="Seg_2592" s="T82">tohogo-ŋ</ta>
            <ta e="T84" id="Seg_2593" s="T83">bu</ta>
            <ta e="T85" id="Seg_2594" s="T84">bi͡ek</ta>
            <ta e="T86" id="Seg_2595" s="T85">to</ta>
            <ta e="T87" id="Seg_2596" s="T86">baːr</ta>
            <ta e="T88" id="Seg_2597" s="T87">onton</ta>
            <ta e="T89" id="Seg_2598" s="T88">bu͡olla</ta>
            <ta e="T90" id="Seg_2599" s="T89">min</ta>
            <ta e="T91" id="Seg_2600" s="T90">hette</ta>
            <ta e="T92" id="Seg_2601" s="T91">hette</ta>
            <ta e="T93" id="Seg_2602" s="T92">hette</ta>
            <ta e="T94" id="Seg_2603" s="T93">dʼɨl-laːk</ta>
            <ta e="T95" id="Seg_2604" s="T94">er-dep-pinen</ta>
            <ta e="T96" id="Seg_2605" s="T95">bu͡ollagɨna</ta>
            <ta e="T97" id="Seg_2606" s="T96">kim</ta>
            <ta e="T98" id="Seg_2607" s="T97">Hɨndasska-ga</ta>
            <ta e="T99" id="Seg_2608" s="T98">kiːr-bip-pit</ta>
            <ta e="T100" id="Seg_2609" s="T99">tak-ta</ta>
            <ta e="T101" id="Seg_2610" s="T100">bihigi</ta>
            <ta e="T102" id="Seg_2611" s="T101">bi͡ek</ta>
            <ta e="T103" id="Seg_2612" s="T102">tɨ͡a-ga</ta>
            <ta e="T104" id="Seg_2613" s="T103">hɨldʼ-aːččɨ</ta>
            <ta e="T105" id="Seg_2614" s="T104">e-ti-bit</ta>
            <ta e="T106" id="Seg_2615" s="T105">inʼe</ta>
            <ta e="T107" id="Seg_2616" s="T106">bu</ta>
            <ta e="T108" id="Seg_2617" s="T107">elbek</ta>
            <ta e="T109" id="Seg_2618" s="T108">bagajɨ</ta>
            <ta e="T110" id="Seg_2619" s="T109">ü͡ör-deːk</ta>
            <ta e="T112" id="Seg_2620" s="T111">elbek</ta>
            <ta e="T113" id="Seg_2621" s="T112">bagajɨ</ta>
            <ta e="T114" id="Seg_2622" s="T113">ü͡ör-deːk</ta>
            <ta e="T115" id="Seg_2623" s="T114">e-ti-bit</ta>
            <ta e="T116" id="Seg_2624" s="T115">elbek</ta>
            <ta e="T117" id="Seg_2625" s="T116">ɨ͡al-lar</ta>
            <ta e="T118" id="Seg_2626" s="T117">e-ti-lere</ta>
            <ta e="T119" id="Seg_2627" s="T118">kös</ta>
            <ta e="T120" id="Seg_2628" s="T119">kihi</ta>
            <ta e="T121" id="Seg_2629" s="T120">alta-lɨː</ta>
            <ta e="T122" id="Seg_2630" s="T121">bi͡es-tiː</ta>
            <ta e="T123" id="Seg_2631" s="T122">balok-tor</ta>
            <ta e="T124" id="Seg_2632" s="T123">uraha-lar</ta>
            <ta e="T125" id="Seg_2633" s="T124">hɨldʼ-aːččɨ-bit</ta>
            <ta e="T127" id="Seg_2634" s="T125">inʼe</ta>
            <ta e="T132" id="Seg_2635" s="T131">onton</ta>
            <ta e="T133" id="Seg_2636" s="T132">bihigi</ta>
            <ta e="T134" id="Seg_2637" s="T133">ješʼo</ta>
            <ta e="T135" id="Seg_2638" s="T134">oŋu͡or</ta>
            <ta e="T136" id="Seg_2639" s="T135">hɨp-pɨp-pɨt</ta>
            <ta e="T137" id="Seg_2640" s="T136">ürdük</ta>
            <ta e="T138" id="Seg_2641" s="T137">kaja-ga</ta>
            <ta e="T139" id="Seg_2642" s="T138">onno</ta>
            <ta e="T140" id="Seg_2643" s="T139">alta-lɨː</ta>
            <ta e="T141" id="Seg_2644" s="T140">bi͡es-tiː</ta>
            <ta e="T142" id="Seg_2645" s="T141">uraha</ta>
            <ta e="T144" id="Seg_2646" s="T142">dʼi͡e</ta>
            <ta e="T151" id="Seg_2647" s="T150">onno</ta>
            <ta e="T152" id="Seg_2648" s="T151">kim</ta>
            <ta e="T153" id="Seg_2649" s="T152">ikki</ta>
            <ta e="T154" id="Seg_2650" s="T153">ikki</ta>
            <ta e="T155" id="Seg_2651" s="T154">ikki</ta>
            <ta e="T156" id="Seg_2652" s="T155">ogo</ta>
            <ta e="T157" id="Seg_2653" s="T156">ikki</ta>
            <ta e="T158" id="Seg_2654" s="T157">agaj</ta>
            <ta e="T159" id="Seg_2655" s="T158">ogo-loːk</ta>
            <ta e="T160" id="Seg_2656" s="T159">e-t-e</ta>
            <ta e="T161" id="Seg_2657" s="T160">onno</ta>
            <ta e="T162" id="Seg_2658" s="T161">oŋu͡or</ta>
            <ta e="T163" id="Seg_2659" s="T162">bu͡olla</ta>
            <ta e="T164" id="Seg_2660" s="T163">kim</ta>
            <ta e="T165" id="Seg_2661" s="T164">ürdük</ta>
            <ta e="T166" id="Seg_2662" s="T165">kaja-ga</ta>
            <ta e="T167" id="Seg_2663" s="T166">alta-lɨː</ta>
            <ta e="T168" id="Seg_2664" s="T167">bi͡es-tiː</ta>
            <ta e="T169" id="Seg_2665" s="T168">uraha</ta>
            <ta e="T170" id="Seg_2666" s="T169">dʼi͡e</ta>
            <ta e="T171" id="Seg_2667" s="T170">tur-aːččɨ</ta>
            <ta e="T172" id="Seg_2668" s="T171">e-ti-lere</ta>
            <ta e="T173" id="Seg_2669" s="T172">onno</ta>
            <ta e="T174" id="Seg_2670" s="T173">bu͡olla</ta>
            <ta e="T175" id="Seg_2671" s="T174">rɨbak-tar</ta>
            <ta e="T176" id="Seg_2672" s="T175">haːjɨn</ta>
            <ta e="T177" id="Seg_2673" s="T176">bu͡olla</ta>
            <ta e="T178" id="Seg_2674" s="T177">rɨbak-taː-ččɨ</ta>
            <ta e="T179" id="Seg_2675" s="T178">e-ti-lere</ta>
            <ta e="T180" id="Seg_2676" s="T179">vot</ta>
            <ta e="T181" id="Seg_2677" s="T180">onton</ta>
            <ta e="T182" id="Seg_2678" s="T181">bu͡o</ta>
            <ta e="T183" id="Seg_2679" s="T182">min</ta>
            <ta e="T185" id="Seg_2680" s="T184">Hɨndasska-ga</ta>
            <ta e="T186" id="Seg_2681" s="T185">kiːr-bip-pit</ta>
            <ta e="T187" id="Seg_2682" s="T186">teːte-m</ta>
            <ta e="T188" id="Seg_2683" s="T187">öl-ön</ta>
            <ta e="T189" id="Seg_2684" s="T188">öl-ön</ta>
            <ta e="T190" id="Seg_2685" s="T189">onno</ta>
            <ta e="T191" id="Seg_2686" s="T190">bu͡o</ta>
            <ta e="T192" id="Seg_2687" s="T191">bihigi</ta>
            <ta e="T193" id="Seg_2688" s="T192">bu͡olla</ta>
            <ta e="T194" id="Seg_2689" s="T193">ü͡örem-mip-pit</ta>
            <ta e="T195" id="Seg_2690" s="T194">ɨrɨbnaj-ga</ta>
            <ta e="T196" id="Seg_2691" s="T195">Kasʼistaj-ga</ta>
            <ta e="T197" id="Seg_2692" s="T196">Nasku͡o-ga</ta>
            <ta e="T198" id="Seg_2693" s="T197">Nasku͡o-ga</ta>
            <ta e="T199" id="Seg_2694" s="T198">bu͡o</ta>
            <ta e="T200" id="Seg_2695" s="T199">agɨs</ta>
            <ta e="T201" id="Seg_2696" s="T200">klaːh-ɨ</ta>
            <ta e="T202" id="Seg_2697" s="T201">büt-en</ta>
            <ta e="T203" id="Seg_2698" s="T202">baran</ta>
            <ta e="T204" id="Seg_2699" s="T203">bu͡olla</ta>
            <ta e="T205" id="Seg_2700" s="T204">bihigi-ni</ta>
            <ta e="T206" id="Seg_2701" s="T205">kim-ŋe</ta>
            <ta e="T207" id="Seg_2702" s="T206">ɨːp-pɨt-tara</ta>
            <ta e="T208" id="Seg_2703" s="T207">Krasnajarskij-ga</ta>
            <ta e="T209" id="Seg_2704" s="T208">onno</ta>
            <ta e="T210" id="Seg_2705" s="T209">bu͡olla</ta>
            <ta e="T211" id="Seg_2706" s="T210">hürbe</ta>
            <ta e="T212" id="Seg_2707" s="T211">hett-is</ta>
            <ta e="T213" id="Seg_2708" s="T212">ošku͡ola-ga</ta>
            <ta e="T214" id="Seg_2709" s="T213">ü͡örem-mip-pit</ta>
            <ta e="T217" id="Seg_2710" s="T216">kɨlaːh-ɨ</ta>
            <ta e="T218" id="Seg_2711" s="T217">onton</ta>
            <ta e="T219" id="Seg_2712" s="T218">bu͡olla</ta>
            <ta e="T220" id="Seg_2713" s="T219">eː</ta>
            <ta e="T222" id="Seg_2714" s="T221">agɨhu͡on</ta>
            <ta e="T223" id="Seg_2715" s="T222">dʼɨl-laːk-ka</ta>
            <ta e="T225" id="Seg_2716" s="T224">bihigi</ta>
            <ta e="T226" id="Seg_2717" s="T225">bu͡olla</ta>
            <ta e="T228" id="Seg_2718" s="T227">kim</ta>
            <ta e="T229" id="Seg_2719" s="T228">kim-ŋe</ta>
            <ta e="T230" id="Seg_2720" s="T229">pastupaj-daː-bɨt-ɨ-m</ta>
            <ta e="T235" id="Seg_2721" s="T234">onno</ta>
            <ta e="T236" id="Seg_2722" s="T235">bu͡o</ta>
            <ta e="T237" id="Seg_2723" s="T236">alta</ta>
            <ta e="T238" id="Seg_2724" s="T237">dʼɨl-ɨ</ta>
            <ta e="T239" id="Seg_2725" s="T238">ü͡örem-mit-i-m</ta>
            <ta e="T240" id="Seg_2726" s="T239">onton</ta>
            <ta e="T241" id="Seg_2727" s="T240">bu͡o</ta>
            <ta e="T242" id="Seg_2728" s="T241">hett-is</ta>
            <ta e="T243" id="Seg_2729" s="T242">dʼɨl-ɨ-m</ta>
            <ta e="T244" id="Seg_2730" s="T243">intʼernatuːra</ta>
            <ta e="T247" id="Seg_2731" s="T246">intʼernatuːra</ta>
            <ta e="T248" id="Seg_2732" s="T247">biːr</ta>
            <ta e="T249" id="Seg_2733" s="T248">dʼɨl-ɨ</ta>
            <ta e="T250" id="Seg_2734" s="T249">vot</ta>
            <ta e="T251" id="Seg_2735" s="T250">o-nu</ta>
            <ta e="T252" id="Seg_2736" s="T251">büt-en</ta>
            <ta e="T253" id="Seg_2737" s="T252">baran</ta>
            <ta e="T254" id="Seg_2738" s="T253">bu͡o</ta>
            <ta e="T255" id="Seg_2739" s="T254">miːgi-n</ta>
            <ta e="T256" id="Seg_2740" s="T255">bu͡o</ta>
            <ta e="T257" id="Seg_2741" s="T256">Tajmɨːr-ga</ta>
            <ta e="T258" id="Seg_2742" s="T257">ɨːp-pɨt-tara</ta>
            <ta e="T259" id="Seg_2743" s="T258">töröː-büt</ta>
            <ta e="T260" id="Seg_2744" s="T259">ke</ta>
            <ta e="T261" id="Seg_2745" s="T260">hir-be-r</ta>
            <ta e="T262" id="Seg_2746" s="T261">ke</ta>
            <ta e="T263" id="Seg_2747" s="T262">ülel-i͡ep-pi-n</ta>
            <ta e="T264" id="Seg_2748" s="T263">Dudʼinka-ga</ta>
            <ta e="T265" id="Seg_2749" s="T264">kel-en</ta>
            <ta e="T266" id="Seg_2750" s="T265">baran</ta>
            <ta e="T267" id="Seg_2751" s="T266">bu͡o</ta>
            <ta e="T268" id="Seg_2752" s="T267">minigi-n</ta>
            <ta e="T269" id="Seg_2753" s="T268">bu͡o</ta>
            <ta e="T270" id="Seg_2754" s="T269">Pataːpava-ga</ta>
            <ta e="T271" id="Seg_2755" s="T270">ɨːp-pɨt-tara</ta>
            <ta e="T272" id="Seg_2756" s="T271">onno</ta>
            <ta e="T273" id="Seg_2757" s="T272">kim</ta>
            <ta e="T274" id="Seg_2758" s="T273">baːr</ta>
            <ta e="T275" id="Seg_2759" s="T274">e-t-e</ta>
            <ta e="T278" id="Seg_2760" s="T277">hu͡ok</ta>
            <ta e="T282" id="Seg_2761" s="T281">e-t-e</ta>
            <ta e="T284" id="Seg_2762" s="T283">vot</ta>
            <ta e="T285" id="Seg_2763" s="T284">onno</ta>
            <ta e="T286" id="Seg_2764" s="T285">bu͡olla</ta>
            <ta e="T287" id="Seg_2765" s="T286">üs</ta>
            <ta e="T288" id="Seg_2766" s="T287">dʼɨl-ɨ</ta>
            <ta e="T289" id="Seg_2767" s="T288">üleleː-bit-i-m</ta>
            <ta e="T290" id="Seg_2768" s="T289">vot</ta>
            <ta e="T291" id="Seg_2769" s="T290">agɨhu͡on</ta>
            <ta e="T292" id="Seg_2770" s="T291">törd-üs-teːk</ta>
            <ta e="T293" id="Seg_2771" s="T292">agɨhu͡on</ta>
            <ta e="T294" id="Seg_2772" s="T293">törd-üs-teːk</ta>
            <ta e="T295" id="Seg_2773" s="T294">dʼɨl-ga</ta>
            <ta e="T296" id="Seg_2774" s="T295">munna</ta>
            <ta e="T297" id="Seg_2775" s="T296">kel-bit-i-m</ta>
            <ta e="T298" id="Seg_2776" s="T297">köh-ön</ta>
            <ta e="T300" id="Seg_2777" s="T299">ɨːp-pɨt-tara</ta>
            <ta e="T301" id="Seg_2778" s="T300">i</ta>
            <ta e="T302" id="Seg_2779" s="T301">hol</ta>
            <ta e="T303" id="Seg_2780" s="T302">dʼɨl-tan</ta>
            <ta e="T304" id="Seg_2781" s="T303">bu</ta>
            <ta e="T305" id="Seg_2782" s="T304">anɨ-ga</ta>
            <ta e="T306" id="Seg_2783" s="T305">di͡eri</ta>
            <ta e="T307" id="Seg_2784" s="T306">bu͡olla</ta>
            <ta e="T308" id="Seg_2785" s="T307">kimi͡e-ke</ta>
            <ta e="T309" id="Seg_2786" s="T308">ülel-iː-bin</ta>
            <ta e="T318" id="Seg_2787" s="T317">onton</ta>
            <ta e="T319" id="Seg_2788" s="T318">bu͡o</ta>
            <ta e="T320" id="Seg_2789" s="T319">agɨhu͡on</ta>
            <ta e="T321" id="Seg_2790" s="T320">toks-us-taːk</ta>
            <ta e="T322" id="Seg_2791" s="T321">i</ta>
            <ta e="T323" id="Seg_2792" s="T322">toks-us-taːk</ta>
            <ta e="T324" id="Seg_2793" s="T323">dʼɨl-ga</ta>
            <ta e="T325" id="Seg_2794" s="T324">bu͡olla</ta>
            <ta e="T326" id="Seg_2795" s="T325">kim</ta>
            <ta e="T327" id="Seg_2796" s="T326">bu͡ol-but</ta>
            <ta e="T328" id="Seg_2797" s="T327">zʼemlʼetrʼasʼenʼije</ta>
            <ta e="T329" id="Seg_2798" s="T328">bu͡ol-but</ta>
            <ta e="T330" id="Seg_2799" s="T329">Armʼenʼija-ga</ta>
            <ta e="T331" id="Seg_2800" s="T330">onno</ta>
            <ta e="T332" id="Seg_2801" s="T331">beje-m</ta>
            <ta e="T333" id="Seg_2802" s="T332">hanaː-bɨ-nan</ta>
            <ta e="T336" id="Seg_2803" s="T335">di͡e-bik-ke</ta>
            <ta e="T337" id="Seg_2804" s="T336">dʼe</ta>
            <ta e="T338" id="Seg_2805" s="T337">onno</ta>
            <ta e="T339" id="Seg_2806" s="T338">üleleː-bit-i-m</ta>
            <ta e="T340" id="Seg_2807" s="T339">ikki</ta>
            <ta e="T353" id="Seg_2808" s="T352">agɨhu͡on</ta>
            <ta e="T358" id="Seg_2809" s="T357">bu</ta>
            <ta e="T359" id="Seg_2810" s="T358">du͡o</ta>
            <ta e="T360" id="Seg_2811" s="T359">kim</ta>
            <ta e="T361" id="Seg_2812" s="T360">bi͡er-bit-tere</ta>
            <ta e="T362" id="Seg_2813" s="T361">mini͡e-ke</ta>
            <ta e="T374" id="Seg_2814" s="T373">kerget-ter-bi-n</ta>
            <ta e="T375" id="Seg_2815" s="T374">gɨtta</ta>
            <ta e="T376" id="Seg_2816" s="T375">inʼe-bi-n</ta>
            <ta e="T377" id="Seg_2817" s="T376">gɨtta</ta>
            <ta e="T378" id="Seg_2818" s="T377">olor-o-bun</ta>
            <ta e="T379" id="Seg_2819" s="T378">anɨ</ta>
            <ta e="T381" id="Seg_2820" s="T380">plemʼanʼisa-bɨ-n</ta>
            <ta e="T382" id="Seg_2821" s="T381">gɨtta</ta>
            <ta e="T383" id="Seg_2822" s="T382">on-tu-m</ta>
            <ta e="T384" id="Seg_2823" s="T383">bu͡olla</ta>
            <ta e="T385" id="Seg_2824" s="T384">on-us</ta>
            <ta e="T386" id="Seg_2825" s="T385">klas-ka</ta>
            <ta e="T387" id="Seg_2826" s="T386">ü͡ören-er</ta>
            <ta e="T388" id="Seg_2827" s="T387">beje-m</ta>
            <ta e="T389" id="Seg_2828" s="T388">bi͡ek</ta>
            <ta e="T390" id="Seg_2829" s="T389">ülel-iː-bin</ta>
            <ta e="T391" id="Seg_2830" s="T390">a</ta>
            <ta e="T392" id="Seg_2831" s="T391">bihigi-ni</ta>
            <ta e="T393" id="Seg_2832" s="T392">bu͡o</ta>
            <ta e="T394" id="Seg_2833" s="T393">beh-is</ta>
            <ta e="T395" id="Seg_2834" s="T394">klass-ka</ta>
            <ta e="T396" id="Seg_2835" s="T395">bu͡o</ta>
            <ta e="T397" id="Seg_2836" s="T396">beh-is</ta>
            <ta e="T398" id="Seg_2837" s="T397">klass-ka</ta>
            <ta e="T399" id="Seg_2838" s="T398">ü͡ören-er</ta>
            <ta e="T400" id="Seg_2839" s="T399">er-dek-pitinen</ta>
            <ta e="T401" id="Seg_2840" s="T400">bu͡o</ta>
            <ta e="T402" id="Seg_2841" s="T401">Nasku͡o-ga</ta>
            <ta e="T403" id="Seg_2842" s="T402">bu͡olla</ta>
            <ta e="T404" id="Seg_2843" s="T403">dom kulʼtura-ga</ta>
            <ta e="T405" id="Seg_2844" s="T404">bu͡olla</ta>
            <ta e="T406" id="Seg_2845" s="T405">bihigi-ni</ta>
            <ta e="T407" id="Seg_2846" s="T406">kim</ta>
            <ta e="T416" id="Seg_2847" s="T415">ol</ta>
            <ta e="T417" id="Seg_2848" s="T416">o-nu</ta>
            <ta e="T419" id="Seg_2849" s="T418">bihigi-ni</ta>
            <ta e="T420" id="Seg_2850" s="T419">kimi͡e-ke</ta>
            <ta e="T421" id="Seg_2851" s="T420">ü͡örep-pit-e</ta>
            <ta e="T422" id="Seg_2852" s="T421">bargaːŋ-ŋa</ta>
            <ta e="T423" id="Seg_2853" s="T422">oːnnʼ-uː</ta>
            <ta e="T424" id="Seg_2854" s="T423">anɨ</ta>
            <ta e="T425" id="Seg_2855" s="T424">bu͡olla</ta>
            <ta e="T429" id="Seg_2856" s="T428">kɨrdʼ-ɨ͡ak-pa-r</ta>
            <ta e="T430" id="Seg_2857" s="T429">di͡eri</ta>
            <ta e="T431" id="Seg_2858" s="T430">bi͡ek</ta>
            <ta e="T432" id="Seg_2859" s="T431">oːnnʼ-uː-bun</ta>
            <ta e="T433" id="Seg_2860" s="T432">anɨ</ta>
            <ta e="T434" id="Seg_2861" s="T433">vot</ta>
            <ta e="T435" id="Seg_2862" s="T434">Jakuːtskaj-gɨ-ttan</ta>
            <ta e="T436" id="Seg_2863" s="T435">kim-nere</ta>
            <ta e="T437" id="Seg_2864" s="T436">kel-bit</ta>
            <ta e="T438" id="Seg_2865" s="T437">artʼis-tar</ta>
            <ta e="T439" id="Seg_2866" s="T438">kel-bit</ta>
            <ta e="T440" id="Seg_2867" s="T439">e-ti-lere</ta>
            <ta e="T441" id="Seg_2868" s="T440">o-lor</ta>
            <ta e="T442" id="Seg_2869" s="T441">bihi͡e-ke</ta>
            <ta e="T443" id="Seg_2870" s="T442">bargan</ta>
            <ta e="T444" id="Seg_2871" s="T443">atɨlaː-bɨt-tara</ta>
            <ta e="T446" id="Seg_2872" s="T445">bu</ta>
            <ta e="T447" id="Seg_2873" s="T446">ulakan</ta>
            <ta e="T448" id="Seg_2874" s="T447">karčɨ</ta>
            <ta e="T449" id="Seg_2875" s="T448">e-t-e</ta>
            <ta e="T450" id="Seg_2876" s="T449">bu͡o</ta>
            <ta e="T451" id="Seg_2877" s="T450">agɨhu͡on</ta>
            <ta e="T452" id="Seg_2878" s="T451">agɨhu͡on</ta>
            <ta e="T453" id="Seg_2879" s="T452">rublʼ</ta>
            <ta e="T454" id="Seg_2880" s="T453">anɨ</ta>
            <ta e="T455" id="Seg_2881" s="T454">bu͡o</ta>
            <ta e="T456" id="Seg_2882" s="T455">bi͡ek</ta>
            <ta e="T457" id="Seg_2883" s="T456">oːnnʼ-uː-bun</ta>
            <ta e="T458" id="Seg_2884" s="T457">hanaː-m</ta>
            <ta e="T459" id="Seg_2885" s="T458">kel-leg-ine</ta>
            <ta e="T460" id="Seg_2886" s="T459">anɨ</ta>
            <ta e="T461" id="Seg_2887" s="T460">emi͡e</ta>
            <ta e="T462" id="Seg_2888" s="T461">ehi͡e-ke</ta>
            <ta e="T463" id="Seg_2889" s="T462">min</ta>
            <ta e="T464" id="Seg_2890" s="T463">oːnnʼ-u͡o-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KiLS">
            <ta e="T2" id="Seg_2891" s="T1">min</ta>
            <ta e="T3" id="Seg_2892" s="T2">aːt-I-m</ta>
            <ta e="T4" id="Seg_2893" s="T3">Kʼirgʼizava</ta>
            <ta e="T5" id="Seg_2894" s="T4">Lʼina</ta>
            <ta e="T6" id="Seg_2895" s="T5">Sʼemʼonovna</ta>
            <ta e="T7" id="Seg_2896" s="T6">min</ta>
            <ta e="T8" id="Seg_2897" s="T7">min</ta>
            <ta e="T9" id="Seg_2898" s="T8">töröː-BIT-I-m</ta>
            <ta e="T10" id="Seg_2899" s="T9">bu͡olla</ta>
            <ta e="T11" id="Seg_2900" s="T10">Nosku͡o-GA</ta>
            <ta e="T12" id="Seg_2901" s="T11">Hɨndaːska-GA</ta>
            <ta e="T13" id="Seg_2902" s="T12">kergen-LAr-I-m</ta>
            <ta e="T14" id="Seg_2903" s="T13">bu͡olla</ta>
            <ta e="T15" id="Seg_2904" s="T14">tɨ͡a-LAːgI</ta>
            <ta e="T16" id="Seg_2905" s="T15">e-TI-LArA</ta>
            <ta e="T17" id="Seg_2906" s="T16">kim-LAr</ta>
            <ta e="T18" id="Seg_2907" s="T17">kim</ta>
            <ta e="T19" id="Seg_2908" s="T18">taba-ČIt-LAr</ta>
            <ta e="T20" id="Seg_2909" s="T19">Korgo-GA</ta>
            <ta e="T22" id="Seg_2910" s="T20">töröː-BIT-I-m</ta>
            <ta e="T25" id="Seg_2911" s="T24">paːs-ČIt-LAr</ta>
            <ta e="T27" id="Seg_2912" s="T25">e-TI-LArA</ta>
            <ta e="T31" id="Seg_2913" s="T29">inʼe-m</ta>
            <ta e="T32" id="Seg_2914" s="T31">bu͡olla</ta>
            <ta e="T33" id="Seg_2915" s="T32">i</ta>
            <ta e="T34" id="Seg_2916" s="T33">inʼe-m</ta>
            <ta e="T35" id="Seg_2917" s="T34">kepseː-Ar-tA</ta>
            <ta e="T36" id="Seg_2918" s="T35">bu͡olla</ta>
            <ta e="T37" id="Seg_2919" s="T36">min</ta>
            <ta e="T38" id="Seg_2920" s="T37">töröː-BIT-BA-r</ta>
            <ta e="T39" id="Seg_2921" s="T38">bu͡ol</ta>
            <ta e="T40" id="Seg_2922" s="T39">min</ta>
            <ta e="T41" id="Seg_2923" s="T40">bu͡o</ta>
            <ta e="T42" id="Seg_2924" s="T41">kim</ta>
            <ta e="T43" id="Seg_2925" s="T42">töröː-BIT-BA-r</ta>
            <ta e="T44" id="Seg_2926" s="T43">bu͡o</ta>
            <ta e="T45" id="Seg_2927" s="T44">kim</ta>
            <ta e="T46" id="Seg_2928" s="T45">tohogo</ta>
            <ta e="T47" id="Seg_2929" s="T46">uːr-BIT-LAr</ta>
            <ta e="T48" id="Seg_2930" s="T47">tɨ͡a-GA</ta>
            <ta e="T49" id="Seg_2931" s="T48">onno</ta>
            <ta e="T50" id="Seg_2932" s="T49">bu͡o</ta>
            <ta e="T51" id="Seg_2933" s="T50">kɨhɨ͡ak</ta>
            <ta e="T52" id="Seg_2934" s="T51">tu͡ok</ta>
            <ta e="T53" id="Seg_2935" s="T52">baːr</ta>
            <ta e="T55" id="Seg_2936" s="T53">össü͡ö</ta>
            <ta e="T59" id="Seg_2937" s="T57">gedereː</ta>
            <ta e="T62" id="Seg_2938" s="T60">hoŋohoːn</ta>
            <ta e="T63" id="Seg_2939" s="T62">ol-nI</ta>
            <ta e="T64" id="Seg_2940" s="T63">ol-nI</ta>
            <ta e="T65" id="Seg_2941" s="T64">ol-nI</ta>
            <ta e="T66" id="Seg_2942" s="T65">bu͡olla</ta>
            <ta e="T67" id="Seg_2943" s="T66">mas-GA</ta>
            <ta e="T68" id="Seg_2944" s="T67">baːj-An</ta>
            <ta e="T69" id="Seg_2945" s="T68">baran</ta>
            <ta e="T70" id="Seg_2946" s="T69">bu</ta>
            <ta e="T71" id="Seg_2947" s="T70">ol</ta>
            <ta e="T72" id="Seg_2948" s="T71">aːt-tA</ta>
            <ta e="T73" id="Seg_2949" s="T72">tohogo</ta>
            <ta e="T74" id="Seg_2950" s="T73">bu</ta>
            <ta e="T75" id="Seg_2951" s="T74">ka</ta>
            <ta e="T76" id="Seg_2952" s="T75">hir-GA</ta>
            <ta e="T77" id="Seg_2953" s="T76">uːr-An</ta>
            <ta e="T78" id="Seg_2954" s="T77">keːs-BIT-LAr</ta>
            <ta e="T79" id="Seg_2955" s="T78">ol-tI-ŋ</ta>
            <ta e="T80" id="Seg_2956" s="T79">ol</ta>
            <ta e="T81" id="Seg_2957" s="T80">tohogo-ŋ</ta>
            <ta e="T82" id="Seg_2958" s="T81">ol</ta>
            <ta e="T83" id="Seg_2959" s="T82">tohogo-ŋ</ta>
            <ta e="T84" id="Seg_2960" s="T83">bu</ta>
            <ta e="T85" id="Seg_2961" s="T84">bi͡ek</ta>
            <ta e="T86" id="Seg_2962" s="T85">to</ta>
            <ta e="T87" id="Seg_2963" s="T86">baːr</ta>
            <ta e="T88" id="Seg_2964" s="T87">onton</ta>
            <ta e="T89" id="Seg_2965" s="T88">bu͡olla</ta>
            <ta e="T90" id="Seg_2966" s="T89">min</ta>
            <ta e="T91" id="Seg_2967" s="T90">hette</ta>
            <ta e="T92" id="Seg_2968" s="T91">hette</ta>
            <ta e="T93" id="Seg_2969" s="T92">hette</ta>
            <ta e="T94" id="Seg_2970" s="T93">dʼɨl-LAːK</ta>
            <ta e="T95" id="Seg_2971" s="T94">er-TAK-BInA</ta>
            <ta e="T96" id="Seg_2972" s="T95">bu͡ollagɨna</ta>
            <ta e="T97" id="Seg_2973" s="T96">kim</ta>
            <ta e="T98" id="Seg_2974" s="T97">Hɨndaːska-GA</ta>
            <ta e="T99" id="Seg_2975" s="T98">kiːr-BIT-BIt</ta>
            <ta e="T100" id="Seg_2976" s="T99">taːk-ta</ta>
            <ta e="T101" id="Seg_2977" s="T100">bihigi</ta>
            <ta e="T102" id="Seg_2978" s="T101">bi͡ek</ta>
            <ta e="T103" id="Seg_2979" s="T102">tɨ͡a-GA</ta>
            <ta e="T104" id="Seg_2980" s="T103">hɨrɨt-AːččI</ta>
            <ta e="T105" id="Seg_2981" s="T104">e-TI-BIt</ta>
            <ta e="T106" id="Seg_2982" s="T105">inʼe</ta>
            <ta e="T107" id="Seg_2983" s="T106">bu</ta>
            <ta e="T108" id="Seg_2984" s="T107">elbek</ta>
            <ta e="T109" id="Seg_2985" s="T108">bagajɨ</ta>
            <ta e="T110" id="Seg_2986" s="T109">ü͡ör-LAːK</ta>
            <ta e="T112" id="Seg_2987" s="T111">elbek</ta>
            <ta e="T113" id="Seg_2988" s="T112">bagajɨ</ta>
            <ta e="T114" id="Seg_2989" s="T113">ü͡ör-LAːK</ta>
            <ta e="T115" id="Seg_2990" s="T114">e-TI-BIt</ta>
            <ta e="T116" id="Seg_2991" s="T115">elbek</ta>
            <ta e="T117" id="Seg_2992" s="T116">ɨ͡al-LAr</ta>
            <ta e="T118" id="Seg_2993" s="T117">e-TI-LArA</ta>
            <ta e="T119" id="Seg_2994" s="T118">kös</ta>
            <ta e="T120" id="Seg_2995" s="T119">kihi</ta>
            <ta e="T121" id="Seg_2996" s="T120">alta-LIː</ta>
            <ta e="T122" id="Seg_2997" s="T121">bi͡es-LIː</ta>
            <ta e="T123" id="Seg_2998" s="T122">balok-LAr</ta>
            <ta e="T124" id="Seg_2999" s="T123">uraha-LAr</ta>
            <ta e="T125" id="Seg_3000" s="T124">hɨrɨt-AːččI-BIt</ta>
            <ta e="T127" id="Seg_3001" s="T125">inʼe</ta>
            <ta e="T132" id="Seg_3002" s="T131">onton</ta>
            <ta e="T133" id="Seg_3003" s="T132">bihigi</ta>
            <ta e="T134" id="Seg_3004" s="T133">össü͡ö</ta>
            <ta e="T135" id="Seg_3005" s="T134">onu͡or</ta>
            <ta e="T136" id="Seg_3006" s="T135">hɨt-BIT-BIt</ta>
            <ta e="T137" id="Seg_3007" s="T136">ürdük</ta>
            <ta e="T138" id="Seg_3008" s="T137">kaja-GA</ta>
            <ta e="T139" id="Seg_3009" s="T138">onno</ta>
            <ta e="T140" id="Seg_3010" s="T139">alta-LIː</ta>
            <ta e="T141" id="Seg_3011" s="T140">bi͡es-LIː</ta>
            <ta e="T142" id="Seg_3012" s="T141">uraha</ta>
            <ta e="T144" id="Seg_3013" s="T142">dʼi͡e</ta>
            <ta e="T151" id="Seg_3014" s="T150">onno</ta>
            <ta e="T152" id="Seg_3015" s="T151">kim</ta>
            <ta e="T153" id="Seg_3016" s="T152">ikki</ta>
            <ta e="T154" id="Seg_3017" s="T153">ikki</ta>
            <ta e="T155" id="Seg_3018" s="T154">ikki</ta>
            <ta e="T156" id="Seg_3019" s="T155">ogo</ta>
            <ta e="T157" id="Seg_3020" s="T156">ikki</ta>
            <ta e="T158" id="Seg_3021" s="T157">agaj</ta>
            <ta e="T159" id="Seg_3022" s="T158">ogo-LAːK</ta>
            <ta e="T160" id="Seg_3023" s="T159">e-TI-tA</ta>
            <ta e="T161" id="Seg_3024" s="T160">onno</ta>
            <ta e="T162" id="Seg_3025" s="T161">onu͡or</ta>
            <ta e="T163" id="Seg_3026" s="T162">bu͡olla</ta>
            <ta e="T164" id="Seg_3027" s="T163">kim</ta>
            <ta e="T165" id="Seg_3028" s="T164">ürdük</ta>
            <ta e="T166" id="Seg_3029" s="T165">kaja-GA</ta>
            <ta e="T167" id="Seg_3030" s="T166">alta-LIː</ta>
            <ta e="T168" id="Seg_3031" s="T167">bi͡es-LIː</ta>
            <ta e="T169" id="Seg_3032" s="T168">uraha</ta>
            <ta e="T170" id="Seg_3033" s="T169">dʼi͡e</ta>
            <ta e="T171" id="Seg_3034" s="T170">tur-AːččI</ta>
            <ta e="T172" id="Seg_3035" s="T171">e-TI-LArA</ta>
            <ta e="T173" id="Seg_3036" s="T172">onno</ta>
            <ta e="T174" id="Seg_3037" s="T173">bu͡olla</ta>
            <ta e="T175" id="Seg_3038" s="T174">rɨbak-LAr</ta>
            <ta e="T176" id="Seg_3039" s="T175">hajɨn</ta>
            <ta e="T177" id="Seg_3040" s="T176">bu͡olla</ta>
            <ta e="T178" id="Seg_3041" s="T177">rɨbak-LAː-AːččI</ta>
            <ta e="T179" id="Seg_3042" s="T178">e-TI-LArA</ta>
            <ta e="T180" id="Seg_3043" s="T179">vot</ta>
            <ta e="T181" id="Seg_3044" s="T180">onton</ta>
            <ta e="T182" id="Seg_3045" s="T181">bu͡o</ta>
            <ta e="T183" id="Seg_3046" s="T182">min</ta>
            <ta e="T185" id="Seg_3047" s="T184">Hɨndaːska-GA</ta>
            <ta e="T186" id="Seg_3048" s="T185">kiːr-BIT-BIt</ta>
            <ta e="T187" id="Seg_3049" s="T186">teːte-m</ta>
            <ta e="T188" id="Seg_3050" s="T187">öl-An</ta>
            <ta e="T189" id="Seg_3051" s="T188">öl-An</ta>
            <ta e="T190" id="Seg_3052" s="T189">onno</ta>
            <ta e="T191" id="Seg_3053" s="T190">bu͡o</ta>
            <ta e="T192" id="Seg_3054" s="T191">bihigi</ta>
            <ta e="T193" id="Seg_3055" s="T192">bu͡olla</ta>
            <ta e="T194" id="Seg_3056" s="T193">ü͡ören-BIT-BIt</ta>
            <ta e="T195" id="Seg_3057" s="T194">Rɨmnaj-GA</ta>
            <ta e="T196" id="Seg_3058" s="T195">Kasʼistaj-GA</ta>
            <ta e="T197" id="Seg_3059" s="T196">Nosku͡o-GA</ta>
            <ta e="T198" id="Seg_3060" s="T197">Nosku͡o-GA</ta>
            <ta e="T199" id="Seg_3061" s="T198">bu͡o</ta>
            <ta e="T200" id="Seg_3062" s="T199">agɨs</ta>
            <ta e="T201" id="Seg_3063" s="T200">kɨlaːs-nI</ta>
            <ta e="T202" id="Seg_3064" s="T201">büt-An</ta>
            <ta e="T203" id="Seg_3065" s="T202">baran</ta>
            <ta e="T204" id="Seg_3066" s="T203">bu͡olla</ta>
            <ta e="T205" id="Seg_3067" s="T204">bihigi-nI</ta>
            <ta e="T206" id="Seg_3068" s="T205">kim-GA</ta>
            <ta e="T207" id="Seg_3069" s="T206">ɨːt-BIT-LArA</ta>
            <ta e="T208" id="Seg_3070" s="T207">Krasnajarskaj-GA</ta>
            <ta e="T209" id="Seg_3071" s="T208">onno</ta>
            <ta e="T210" id="Seg_3072" s="T209">bu͡olla</ta>
            <ta e="T211" id="Seg_3073" s="T210">hüːrbe</ta>
            <ta e="T212" id="Seg_3074" s="T211">hette-Is</ta>
            <ta e="T213" id="Seg_3075" s="T212">usku͡ola-GA</ta>
            <ta e="T214" id="Seg_3076" s="T213">ü͡ören-BIT-BIt</ta>
            <ta e="T217" id="Seg_3077" s="T216">kɨlaːs-nI</ta>
            <ta e="T218" id="Seg_3078" s="T217">onton</ta>
            <ta e="T219" id="Seg_3079" s="T218">bu͡olla</ta>
            <ta e="T220" id="Seg_3080" s="T219">eː</ta>
            <ta e="T222" id="Seg_3081" s="T221">agɨhu͡on</ta>
            <ta e="T223" id="Seg_3082" s="T222">dʼɨl-LAːK-GA</ta>
            <ta e="T225" id="Seg_3083" s="T224">bihigi</ta>
            <ta e="T226" id="Seg_3084" s="T225">bu͡olla</ta>
            <ta e="T228" id="Seg_3085" s="T227">kim</ta>
            <ta e="T229" id="Seg_3086" s="T228">kim-GA</ta>
            <ta e="T230" id="Seg_3087" s="T229">pastupaj-LAː-BIT-I-m</ta>
            <ta e="T235" id="Seg_3088" s="T234">onno</ta>
            <ta e="T236" id="Seg_3089" s="T235">bu͡o</ta>
            <ta e="T237" id="Seg_3090" s="T236">alta</ta>
            <ta e="T238" id="Seg_3091" s="T237">dʼɨl-nI</ta>
            <ta e="T239" id="Seg_3092" s="T238">ü͡ören-BIT-I-m</ta>
            <ta e="T240" id="Seg_3093" s="T239">onton</ta>
            <ta e="T241" id="Seg_3094" s="T240">bu͡o</ta>
            <ta e="T242" id="Seg_3095" s="T241">hette-Is</ta>
            <ta e="T243" id="Seg_3096" s="T242">dʼɨl-I-m</ta>
            <ta e="T244" id="Seg_3097" s="T243">intʼernatuːra</ta>
            <ta e="T247" id="Seg_3098" s="T246">intʼernatuːra</ta>
            <ta e="T248" id="Seg_3099" s="T247">biːr</ta>
            <ta e="T249" id="Seg_3100" s="T248">dʼɨl-nI</ta>
            <ta e="T250" id="Seg_3101" s="T249">vot</ta>
            <ta e="T251" id="Seg_3102" s="T250">ol-nI</ta>
            <ta e="T252" id="Seg_3103" s="T251">büt-An</ta>
            <ta e="T253" id="Seg_3104" s="T252">baran</ta>
            <ta e="T254" id="Seg_3105" s="T253">bu͡o</ta>
            <ta e="T255" id="Seg_3106" s="T254">min-n</ta>
            <ta e="T256" id="Seg_3107" s="T255">bu͡o</ta>
            <ta e="T257" id="Seg_3108" s="T256">Tajmɨr-GA</ta>
            <ta e="T258" id="Seg_3109" s="T257">ɨːt-BIT-LArA</ta>
            <ta e="T259" id="Seg_3110" s="T258">töröː-BIT</ta>
            <ta e="T260" id="Seg_3111" s="T259">ka</ta>
            <ta e="T261" id="Seg_3112" s="T260">hir-BA-r</ta>
            <ta e="T262" id="Seg_3113" s="T261">ka</ta>
            <ta e="T263" id="Seg_3114" s="T262">üleleː-IAK-BI-n</ta>
            <ta e="T264" id="Seg_3115" s="T263">Dudʼinka-GA</ta>
            <ta e="T265" id="Seg_3116" s="T264">kel-An</ta>
            <ta e="T266" id="Seg_3117" s="T265">baran</ta>
            <ta e="T267" id="Seg_3118" s="T266">bu͡o</ta>
            <ta e="T268" id="Seg_3119" s="T267">min-n</ta>
            <ta e="T269" id="Seg_3120" s="T268">bu͡o</ta>
            <ta e="T270" id="Seg_3121" s="T269">Pataːpava-GA</ta>
            <ta e="T271" id="Seg_3122" s="T270">ɨːt-BIT-LArA</ta>
            <ta e="T272" id="Seg_3123" s="T271">onno</ta>
            <ta e="T273" id="Seg_3124" s="T272">kim</ta>
            <ta e="T274" id="Seg_3125" s="T273">baːr</ta>
            <ta e="T275" id="Seg_3126" s="T274">e-TI-tA</ta>
            <ta e="T278" id="Seg_3127" s="T277">hu͡ok</ta>
            <ta e="T282" id="Seg_3128" s="T281">e-TI-tA</ta>
            <ta e="T284" id="Seg_3129" s="T283">vot</ta>
            <ta e="T285" id="Seg_3130" s="T284">onno</ta>
            <ta e="T286" id="Seg_3131" s="T285">bu͡olla</ta>
            <ta e="T287" id="Seg_3132" s="T286">üs</ta>
            <ta e="T288" id="Seg_3133" s="T287">dʼɨl-nI</ta>
            <ta e="T289" id="Seg_3134" s="T288">üleleː-BIT-I-m</ta>
            <ta e="T290" id="Seg_3135" s="T289">vot</ta>
            <ta e="T291" id="Seg_3136" s="T290">agɨhu͡on</ta>
            <ta e="T292" id="Seg_3137" s="T291">tü͡ört-Is-LAːK</ta>
            <ta e="T293" id="Seg_3138" s="T292">agɨhu͡on</ta>
            <ta e="T294" id="Seg_3139" s="T293">tü͡ört-Is-LAːK</ta>
            <ta e="T295" id="Seg_3140" s="T294">dʼɨl-GA</ta>
            <ta e="T296" id="Seg_3141" s="T295">manna</ta>
            <ta e="T297" id="Seg_3142" s="T296">kel-BIT-I-m</ta>
            <ta e="T298" id="Seg_3143" s="T297">kös-An</ta>
            <ta e="T300" id="Seg_3144" s="T299">ɨːt-BIT-LArA</ta>
            <ta e="T301" id="Seg_3145" s="T300">i</ta>
            <ta e="T302" id="Seg_3146" s="T301">hol</ta>
            <ta e="T303" id="Seg_3147" s="T302">dʼɨl-ttAn</ta>
            <ta e="T304" id="Seg_3148" s="T303">bu</ta>
            <ta e="T305" id="Seg_3149" s="T304">anɨ-GA</ta>
            <ta e="T306" id="Seg_3150" s="T305">di͡eri</ta>
            <ta e="T307" id="Seg_3151" s="T306">bu͡olla</ta>
            <ta e="T308" id="Seg_3152" s="T307">kim-GA</ta>
            <ta e="T309" id="Seg_3153" s="T308">üleleː-A-BIn</ta>
            <ta e="T318" id="Seg_3154" s="T317">onton</ta>
            <ta e="T319" id="Seg_3155" s="T318">bu͡o</ta>
            <ta e="T320" id="Seg_3156" s="T319">agɨhu͡on</ta>
            <ta e="T321" id="Seg_3157" s="T320">togus-Is-LAːK</ta>
            <ta e="T322" id="Seg_3158" s="T321">i</ta>
            <ta e="T323" id="Seg_3159" s="T322">togus-Is-LAːK</ta>
            <ta e="T324" id="Seg_3160" s="T323">dʼɨl-GA</ta>
            <ta e="T325" id="Seg_3161" s="T324">bu͡olla</ta>
            <ta e="T326" id="Seg_3162" s="T325">kim</ta>
            <ta e="T327" id="Seg_3163" s="T326">bu͡ol-BIT</ta>
            <ta e="T328" id="Seg_3164" s="T327">zʼemlʼetrʼasʼenʼije</ta>
            <ta e="T329" id="Seg_3165" s="T328">bu͡ol-BIT</ta>
            <ta e="T330" id="Seg_3166" s="T329">Armʼenʼija-GA</ta>
            <ta e="T331" id="Seg_3167" s="T330">onno</ta>
            <ta e="T332" id="Seg_3168" s="T331">beje-m</ta>
            <ta e="T333" id="Seg_3169" s="T332">hanaː-BI-nAn</ta>
            <ta e="T336" id="Seg_3170" s="T335">di͡e-BIT-GA</ta>
            <ta e="T337" id="Seg_3171" s="T336">dʼe</ta>
            <ta e="T338" id="Seg_3172" s="T337">onno</ta>
            <ta e="T339" id="Seg_3173" s="T338">üleleː-BIT-I-m</ta>
            <ta e="T340" id="Seg_3174" s="T339">ikki</ta>
            <ta e="T353" id="Seg_3175" s="T352">agɨhu͡on</ta>
            <ta e="T358" id="Seg_3176" s="T357">bu</ta>
            <ta e="T359" id="Seg_3177" s="T358">du͡o</ta>
            <ta e="T360" id="Seg_3178" s="T359">kim</ta>
            <ta e="T361" id="Seg_3179" s="T360">bi͡er-BIT-LArA</ta>
            <ta e="T362" id="Seg_3180" s="T361">min-GA</ta>
            <ta e="T374" id="Seg_3181" s="T373">kergen-LAr-BI-n</ta>
            <ta e="T375" id="Seg_3182" s="T374">kɨtta</ta>
            <ta e="T376" id="Seg_3183" s="T375">inʼe-BI-n</ta>
            <ta e="T377" id="Seg_3184" s="T376">kɨtta</ta>
            <ta e="T378" id="Seg_3185" s="T377">olor-A-BIn</ta>
            <ta e="T379" id="Seg_3186" s="T378">anɨ</ta>
            <ta e="T381" id="Seg_3187" s="T380">plemʼanʼisa-BI-n</ta>
            <ta e="T382" id="Seg_3188" s="T381">kɨtta</ta>
            <ta e="T383" id="Seg_3189" s="T382">ol-tI-m</ta>
            <ta e="T384" id="Seg_3190" s="T383">bu͡olla</ta>
            <ta e="T385" id="Seg_3191" s="T384">u͡on-Is</ta>
            <ta e="T386" id="Seg_3192" s="T385">kɨlaːs-GA</ta>
            <ta e="T387" id="Seg_3193" s="T386">ü͡ören-Ar</ta>
            <ta e="T388" id="Seg_3194" s="T387">beje-m</ta>
            <ta e="T389" id="Seg_3195" s="T388">bi͡ek</ta>
            <ta e="T390" id="Seg_3196" s="T389">üleleː-A-BIn</ta>
            <ta e="T391" id="Seg_3197" s="T390">a</ta>
            <ta e="T392" id="Seg_3198" s="T391">bihigi-nI</ta>
            <ta e="T393" id="Seg_3199" s="T392">bu͡o</ta>
            <ta e="T394" id="Seg_3200" s="T393">bi͡es-Is</ta>
            <ta e="T395" id="Seg_3201" s="T394">kɨlaːs-GA</ta>
            <ta e="T396" id="Seg_3202" s="T395">bu͡o</ta>
            <ta e="T397" id="Seg_3203" s="T396">bi͡es-Is</ta>
            <ta e="T398" id="Seg_3204" s="T397">kɨlaːs-GA</ta>
            <ta e="T399" id="Seg_3205" s="T398">ü͡ören-Ar</ta>
            <ta e="T400" id="Seg_3206" s="T399">er-TAK-BItInA</ta>
            <ta e="T401" id="Seg_3207" s="T400">bu͡o</ta>
            <ta e="T402" id="Seg_3208" s="T401">Nosku͡o-GA</ta>
            <ta e="T403" id="Seg_3209" s="T402">bu͡olla</ta>
            <ta e="T404" id="Seg_3210" s="T403">dom kulʼtura-GA</ta>
            <ta e="T405" id="Seg_3211" s="T404">bu͡olla</ta>
            <ta e="T406" id="Seg_3212" s="T405">bihigi-nI</ta>
            <ta e="T407" id="Seg_3213" s="T406">kim</ta>
            <ta e="T416" id="Seg_3214" s="T415">ol</ta>
            <ta e="T417" id="Seg_3215" s="T416">ol-nI</ta>
            <ta e="T419" id="Seg_3216" s="T418">bihigi-nI</ta>
            <ta e="T420" id="Seg_3217" s="T419">kim-GA</ta>
            <ta e="T421" id="Seg_3218" s="T420">ü͡öret-BIT-tA</ta>
            <ta e="T422" id="Seg_3219" s="T421">bargaːn-GA</ta>
            <ta e="T423" id="Seg_3220" s="T422">oːnnʼoː-A</ta>
            <ta e="T424" id="Seg_3221" s="T423">anɨ</ta>
            <ta e="T425" id="Seg_3222" s="T424">bu͡olla</ta>
            <ta e="T429" id="Seg_3223" s="T428">kɨrɨj-IAK-BA-r</ta>
            <ta e="T430" id="Seg_3224" s="T429">di͡eri</ta>
            <ta e="T431" id="Seg_3225" s="T430">bi͡ek</ta>
            <ta e="T432" id="Seg_3226" s="T431">oːnnʼoː-A-BIn</ta>
            <ta e="T433" id="Seg_3227" s="T432">anɨ</ta>
            <ta e="T434" id="Seg_3228" s="T433">vot</ta>
            <ta e="T435" id="Seg_3229" s="T434">Jakuːtskaj-GI-ttAn</ta>
            <ta e="T436" id="Seg_3230" s="T435">kim-LArA</ta>
            <ta e="T437" id="Seg_3231" s="T436">kel-BIT</ta>
            <ta e="T438" id="Seg_3232" s="T437">artʼist-LAr</ta>
            <ta e="T439" id="Seg_3233" s="T438">kel-BIT</ta>
            <ta e="T440" id="Seg_3234" s="T439">e-TI-LArA</ta>
            <ta e="T441" id="Seg_3235" s="T440">ol-LAr</ta>
            <ta e="T442" id="Seg_3236" s="T441">bihigi-GA</ta>
            <ta e="T443" id="Seg_3237" s="T442">bargaːn</ta>
            <ta e="T444" id="Seg_3238" s="T443">atɨːlaː-BIT-LArA</ta>
            <ta e="T446" id="Seg_3239" s="T445">bu</ta>
            <ta e="T447" id="Seg_3240" s="T446">ulakan</ta>
            <ta e="T448" id="Seg_3241" s="T447">karčɨ</ta>
            <ta e="T449" id="Seg_3242" s="T448">e-TI-tA</ta>
            <ta e="T450" id="Seg_3243" s="T449">bu͡o</ta>
            <ta e="T451" id="Seg_3244" s="T450">agɨhu͡on</ta>
            <ta e="T452" id="Seg_3245" s="T451">agɨhu͡on</ta>
            <ta e="T453" id="Seg_3246" s="T452">rublʼ</ta>
            <ta e="T454" id="Seg_3247" s="T453">anɨ</ta>
            <ta e="T455" id="Seg_3248" s="T454">bu͡o</ta>
            <ta e="T456" id="Seg_3249" s="T455">bi͡ek</ta>
            <ta e="T457" id="Seg_3250" s="T456">oːnnʼoː-A-BIn</ta>
            <ta e="T458" id="Seg_3251" s="T457">hanaː-m</ta>
            <ta e="T459" id="Seg_3252" s="T458">kel-TAK-InA</ta>
            <ta e="T460" id="Seg_3253" s="T459">anɨ</ta>
            <ta e="T461" id="Seg_3254" s="T460">emi͡e</ta>
            <ta e="T462" id="Seg_3255" s="T461">ehigi-GA</ta>
            <ta e="T463" id="Seg_3256" s="T462">min</ta>
            <ta e="T464" id="Seg_3257" s="T463">oːnnʼoː-IAK-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KiLS">
            <ta e="T2" id="Seg_3258" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_3259" s="T2">name-EP-1SG</ta>
            <ta e="T4" id="Seg_3260" s="T3">Kirgizova</ta>
            <ta e="T5" id="Seg_3261" s="T4">Lina</ta>
            <ta e="T6" id="Seg_3262" s="T5">Semyonovna.[NOM]</ta>
            <ta e="T7" id="Seg_3263" s="T6">1SG.[NOM]</ta>
            <ta e="T8" id="Seg_3264" s="T7">1SG.[NOM]</ta>
            <ta e="T9" id="Seg_3265" s="T8">be.born-PST2-EP-1SG</ta>
            <ta e="T10" id="Seg_3266" s="T9">MOD</ta>
            <ta e="T11" id="Seg_3267" s="T10">Khatanga-DAT/LOC</ta>
            <ta e="T12" id="Seg_3268" s="T11">Syndassko-DAT/LOC</ta>
            <ta e="T13" id="Seg_3269" s="T12">parents-PL-EP-1SG</ta>
            <ta e="T14" id="Seg_3270" s="T13">MOD</ta>
            <ta e="T15" id="Seg_3271" s="T14">tundra-ADJZ.[NOM]</ta>
            <ta e="T16" id="Seg_3272" s="T15">be-PST1-3PL</ta>
            <ta e="T17" id="Seg_3273" s="T16">who-PL.[NOM]</ta>
            <ta e="T18" id="Seg_3274" s="T17">who.[NOM]</ta>
            <ta e="T19" id="Seg_3275" s="T18">reindeer-AG-PL.[NOM]</ta>
            <ta e="T20" id="Seg_3276" s="T19">Korgo-DAT/LOC</ta>
            <ta e="T22" id="Seg_3277" s="T20">be.born-PST2-EP-1SG</ta>
            <ta e="T25" id="Seg_3278" s="T24">deadfall-AG-PL.[NOM]</ta>
            <ta e="T27" id="Seg_3279" s="T25">be-PST1-3PL</ta>
            <ta e="T31" id="Seg_3280" s="T29">mother-1SG.[NOM]</ta>
            <ta e="T32" id="Seg_3281" s="T31">MOD</ta>
            <ta e="T33" id="Seg_3282" s="T32">and</ta>
            <ta e="T34" id="Seg_3283" s="T33">mother-1SG.[NOM]</ta>
            <ta e="T35" id="Seg_3284" s="T34">tell-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T36" id="Seg_3285" s="T35">MOD</ta>
            <ta e="T37" id="Seg_3286" s="T36">1SG.[NOM]</ta>
            <ta e="T38" id="Seg_3287" s="T37">be.born-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T39" id="Seg_3288" s="T38">EMPH</ta>
            <ta e="T40" id="Seg_3289" s="T39">1SG.[NOM]</ta>
            <ta e="T41" id="Seg_3290" s="T40">EMPH</ta>
            <ta e="T42" id="Seg_3291" s="T41">who.[NOM]</ta>
            <ta e="T43" id="Seg_3292" s="T42">be.born-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T44" id="Seg_3293" s="T43">EMPH</ta>
            <ta e="T45" id="Seg_3294" s="T44">who.[NOM]</ta>
            <ta e="T46" id="Seg_3295" s="T45">nail.[NOM]</ta>
            <ta e="T47" id="Seg_3296" s="T46">lay-PST2-3PL</ta>
            <ta e="T48" id="Seg_3297" s="T47">tundra-DAT/LOC</ta>
            <ta e="T49" id="Seg_3298" s="T48">there</ta>
            <ta e="T50" id="Seg_3299" s="T49">EMPH</ta>
            <ta e="T51" id="Seg_3300" s="T50">scraper.[NOM]</ta>
            <ta e="T52" id="Seg_3301" s="T51">what.[NOM]</ta>
            <ta e="T53" id="Seg_3302" s="T52">there.is</ta>
            <ta e="T55" id="Seg_3303" s="T53">still</ta>
            <ta e="T59" id="Seg_3304" s="T57">scraper.for.leather.[NOM]</ta>
            <ta e="T62" id="Seg_3305" s="T60">scraper.with.toothed.end.[NOM]</ta>
            <ta e="T63" id="Seg_3306" s="T62">that -ACC</ta>
            <ta e="T64" id="Seg_3307" s="T63">that -ACC</ta>
            <ta e="T65" id="Seg_3308" s="T64">that -ACC</ta>
            <ta e="T66" id="Seg_3309" s="T65">MOD</ta>
            <ta e="T67" id="Seg_3310" s="T66">wood-DAT/LOC</ta>
            <ta e="T68" id="Seg_3311" s="T67">tie-CVB.SEQ</ta>
            <ta e="T69" id="Seg_3312" s="T68">after</ta>
            <ta e="T70" id="Seg_3313" s="T69">this</ta>
            <ta e="T71" id="Seg_3314" s="T70">that </ta>
            <ta e="T72" id="Seg_3315" s="T71">name-3SG.[NOM]</ta>
            <ta e="T73" id="Seg_3316" s="T72">pole</ta>
            <ta e="T74" id="Seg_3317" s="T73">this</ta>
            <ta e="T75" id="Seg_3318" s="T74">well</ta>
            <ta e="T76" id="Seg_3319" s="T75">earth-DAT/LOC</ta>
            <ta e="T77" id="Seg_3320" s="T76">lay-CVB.SEQ</ta>
            <ta e="T78" id="Seg_3321" s="T77">throw-PST2-3PL</ta>
            <ta e="T79" id="Seg_3322" s="T78">that -3SG-2SG.[NOM]</ta>
            <ta e="T80" id="Seg_3323" s="T79">that </ta>
            <ta e="T81" id="Seg_3324" s="T80">pole-2SG.[NOM]</ta>
            <ta e="T82" id="Seg_3325" s="T81">that </ta>
            <ta e="T83" id="Seg_3326" s="T82">pole-2SG.[NOM]</ta>
            <ta e="T84" id="Seg_3327" s="T83">this</ta>
            <ta e="T85" id="Seg_3328" s="T84">always</ta>
            <ta e="T86" id="Seg_3329" s="T85">then</ta>
            <ta e="T87" id="Seg_3330" s="T86">there.is</ta>
            <ta e="T88" id="Seg_3331" s="T87">then</ta>
            <ta e="T89" id="Seg_3332" s="T88">MOD</ta>
            <ta e="T90" id="Seg_3333" s="T89">1SG.[NOM]</ta>
            <ta e="T91" id="Seg_3334" s="T90">seven</ta>
            <ta e="T92" id="Seg_3335" s="T91">seven</ta>
            <ta e="T93" id="Seg_3336" s="T92">seven</ta>
            <ta e="T94" id="Seg_3337" s="T93">year-PROPR.[NOM]</ta>
            <ta e="T95" id="Seg_3338" s="T94">be-TEMP-1SG</ta>
            <ta e="T96" id="Seg_3339" s="T95">though</ta>
            <ta e="T97" id="Seg_3340" s="T96">who.[NOM]</ta>
            <ta e="T98" id="Seg_3341" s="T97">Syndassko-DAT/LOC</ta>
            <ta e="T99" id="Seg_3342" s="T98">go.in-PST2-1PL</ta>
            <ta e="T100" id="Seg_3343" s="T99">so-EMPH</ta>
            <ta e="T101" id="Seg_3344" s="T100">1PL.[NOM]</ta>
            <ta e="T102" id="Seg_3345" s="T101">always</ta>
            <ta e="T103" id="Seg_3346" s="T102">tundra-DAT/LOC</ta>
            <ta e="T104" id="Seg_3347" s="T103">go-PTCP.HAB</ta>
            <ta e="T105" id="Seg_3348" s="T104">be-PST1-1PL</ta>
            <ta e="T106" id="Seg_3349" s="T105">mother.[NOM]</ta>
            <ta e="T107" id="Seg_3350" s="T106">this</ta>
            <ta e="T108" id="Seg_3351" s="T107">many</ta>
            <ta e="T109" id="Seg_3352" s="T108">very</ta>
            <ta e="T110" id="Seg_3353" s="T109">herd-PROPR.[NOM]</ta>
            <ta e="T112" id="Seg_3354" s="T111">many</ta>
            <ta e="T113" id="Seg_3355" s="T112">very</ta>
            <ta e="T114" id="Seg_3356" s="T113">herd-PROPR.[NOM]</ta>
            <ta e="T115" id="Seg_3357" s="T114">be-PST1-1PL</ta>
            <ta e="T116" id="Seg_3358" s="T115">many</ta>
            <ta e="T117" id="Seg_3359" s="T116">family-PL.[NOM]</ta>
            <ta e="T118" id="Seg_3360" s="T117">be-PST1-3PL</ta>
            <ta e="T119" id="Seg_3361" s="T118">nomad.[NOM]</ta>
            <ta e="T120" id="Seg_3362" s="T119">human.being.[NOM]</ta>
            <ta e="T121" id="Seg_3363" s="T120">six-DISTR</ta>
            <ta e="T122" id="Seg_3364" s="T121">five-DISTR</ta>
            <ta e="T123" id="Seg_3365" s="T122">balok-PL.[NOM]</ta>
            <ta e="T124" id="Seg_3366" s="T123">pole.tent-PL.[NOM]</ta>
            <ta e="T125" id="Seg_3367" s="T124">go-HAB-1PL</ta>
            <ta e="T127" id="Seg_3368" s="T125">mother.[NOM]</ta>
            <ta e="T132" id="Seg_3369" s="T131">then</ta>
            <ta e="T133" id="Seg_3370" s="T132">1PL.[NOM]</ta>
            <ta e="T134" id="Seg_3371" s="T133">still</ta>
            <ta e="T135" id="Seg_3372" s="T134">on.the.other.shore</ta>
            <ta e="T136" id="Seg_3373" s="T135">lie-PST2-1PL</ta>
            <ta e="T137" id="Seg_3374" s="T136">high</ta>
            <ta e="T138" id="Seg_3375" s="T137">mountain-DAT/LOC</ta>
            <ta e="T139" id="Seg_3376" s="T138">there</ta>
            <ta e="T140" id="Seg_3377" s="T139">six-DISTR</ta>
            <ta e="T141" id="Seg_3378" s="T140">five-DISTR</ta>
            <ta e="T142" id="Seg_3379" s="T141">pole.[NOM]</ta>
            <ta e="T144" id="Seg_3380" s="T142">tent.[NOM]</ta>
            <ta e="T151" id="Seg_3381" s="T150">there</ta>
            <ta e="T152" id="Seg_3382" s="T151">who.[NOM]</ta>
            <ta e="T153" id="Seg_3383" s="T152">two</ta>
            <ta e="T154" id="Seg_3384" s="T153">two</ta>
            <ta e="T155" id="Seg_3385" s="T154">two</ta>
            <ta e="T156" id="Seg_3386" s="T155">child.[NOM]</ta>
            <ta e="T157" id="Seg_3387" s="T156">two</ta>
            <ta e="T158" id="Seg_3388" s="T157">only</ta>
            <ta e="T159" id="Seg_3389" s="T158">child-PROPR.[NOM]</ta>
            <ta e="T160" id="Seg_3390" s="T159">be-PST1-3SG</ta>
            <ta e="T161" id="Seg_3391" s="T160">there</ta>
            <ta e="T162" id="Seg_3392" s="T161">on.the.other.shore</ta>
            <ta e="T163" id="Seg_3393" s="T162">MOD</ta>
            <ta e="T164" id="Seg_3394" s="T163">who.[NOM]</ta>
            <ta e="T165" id="Seg_3395" s="T164">high</ta>
            <ta e="T166" id="Seg_3396" s="T165">mountain-DAT/LOC</ta>
            <ta e="T167" id="Seg_3397" s="T166">six-DISTR</ta>
            <ta e="T168" id="Seg_3398" s="T167">five-DISTR</ta>
            <ta e="T169" id="Seg_3399" s="T168">pole.[NOM]</ta>
            <ta e="T170" id="Seg_3400" s="T169">tent.[NOM]</ta>
            <ta e="T171" id="Seg_3401" s="T170">stand-PTCP.HAB</ta>
            <ta e="T172" id="Seg_3402" s="T171">be-PST1-3PL</ta>
            <ta e="T173" id="Seg_3403" s="T172">there</ta>
            <ta e="T174" id="Seg_3404" s="T173">MOD</ta>
            <ta e="T175" id="Seg_3405" s="T174">fisherman-PL.[NOM]</ta>
            <ta e="T176" id="Seg_3406" s="T175">in.summer</ta>
            <ta e="T177" id="Seg_3407" s="T176">MOD</ta>
            <ta e="T178" id="Seg_3408" s="T177">fisherman-VBZ-PTCP.HAB</ta>
            <ta e="T179" id="Seg_3409" s="T178">be-PST1-3PL</ta>
            <ta e="T180" id="Seg_3410" s="T179">well</ta>
            <ta e="T181" id="Seg_3411" s="T180">then</ta>
            <ta e="T182" id="Seg_3412" s="T181">EMPH</ta>
            <ta e="T183" id="Seg_3413" s="T182">1SG.[NOM]</ta>
            <ta e="T185" id="Seg_3414" s="T184">Syndassko-DAT/LOC</ta>
            <ta e="T186" id="Seg_3415" s="T185">go.in-PST2-1PL</ta>
            <ta e="T187" id="Seg_3416" s="T186">father-1SG.[NOM]</ta>
            <ta e="T188" id="Seg_3417" s="T187">die-CVB.SEQ</ta>
            <ta e="T189" id="Seg_3418" s="T188">die-CVB.SEQ</ta>
            <ta e="T190" id="Seg_3419" s="T189">there</ta>
            <ta e="T191" id="Seg_3420" s="T190">EMPH</ta>
            <ta e="T192" id="Seg_3421" s="T191">1PL.[NOM]</ta>
            <ta e="T193" id="Seg_3422" s="T192">MOD</ta>
            <ta e="T194" id="Seg_3423" s="T193">learn-PST2-1PL</ta>
            <ta e="T195" id="Seg_3424" s="T194">Novorybnoe-DAT/LOC</ta>
            <ta e="T196" id="Seg_3425" s="T195">Kosistiy-DAT/LOC</ta>
            <ta e="T197" id="Seg_3426" s="T196">Khatanga-DAT/LOC</ta>
            <ta e="T198" id="Seg_3427" s="T197">Khatanga-DAT/LOC</ta>
            <ta e="T199" id="Seg_3428" s="T198">EMPH</ta>
            <ta e="T200" id="Seg_3429" s="T199">eight</ta>
            <ta e="T201" id="Seg_3430" s="T200">class-ACC</ta>
            <ta e="T202" id="Seg_3431" s="T201">stop-CVB.SEQ</ta>
            <ta e="T203" id="Seg_3432" s="T202">after</ta>
            <ta e="T204" id="Seg_3433" s="T203">MOD</ta>
            <ta e="T205" id="Seg_3434" s="T204">1PL-ACC</ta>
            <ta e="T206" id="Seg_3435" s="T205">who-DAT/LOC</ta>
            <ta e="T207" id="Seg_3436" s="T206">send-PST2-3PL</ta>
            <ta e="T208" id="Seg_3437" s="T207">Krasnoyarsk-DAT/LOC</ta>
            <ta e="T209" id="Seg_3438" s="T208">there</ta>
            <ta e="T210" id="Seg_3439" s="T209">MOD</ta>
            <ta e="T211" id="Seg_3440" s="T210">twenty</ta>
            <ta e="T212" id="Seg_3441" s="T211">seven-ORD</ta>
            <ta e="T213" id="Seg_3442" s="T212">school-DAT/LOC</ta>
            <ta e="T214" id="Seg_3443" s="T213">learn-PST2-1PL</ta>
            <ta e="T217" id="Seg_3444" s="T216">class-ACC</ta>
            <ta e="T218" id="Seg_3445" s="T217">then</ta>
            <ta e="T219" id="Seg_3446" s="T218">MOD</ta>
            <ta e="T220" id="Seg_3447" s="T219">eh</ta>
            <ta e="T222" id="Seg_3448" s="T221">eighty</ta>
            <ta e="T223" id="Seg_3449" s="T222">year-PROPR-DAT/LOC</ta>
            <ta e="T225" id="Seg_3450" s="T224">1PL.[NOM]</ta>
            <ta e="T226" id="Seg_3451" s="T225">MOD</ta>
            <ta e="T228" id="Seg_3452" s="T227">who.[NOM]</ta>
            <ta e="T229" id="Seg_3453" s="T228">who-DAT/LOC</ta>
            <ta e="T230" id="Seg_3454" s="T229">enter-VBZ-PST2-EP-1SG</ta>
            <ta e="T235" id="Seg_3455" s="T234">there</ta>
            <ta e="T236" id="Seg_3456" s="T235">EMPH</ta>
            <ta e="T237" id="Seg_3457" s="T236">six</ta>
            <ta e="T238" id="Seg_3458" s="T237">year-ACC</ta>
            <ta e="T239" id="Seg_3459" s="T238">learn-PST2-EP-1SG</ta>
            <ta e="T240" id="Seg_3460" s="T239">then</ta>
            <ta e="T241" id="Seg_3461" s="T240">EMPH</ta>
            <ta e="T242" id="Seg_3462" s="T241">seven-ORD</ta>
            <ta e="T243" id="Seg_3463" s="T242">year-EP-1SG.[NOM]</ta>
            <ta e="T244" id="Seg_3464" s="T243">internship.[NOM]</ta>
            <ta e="T247" id="Seg_3465" s="T246">internship.[NOM]</ta>
            <ta e="T248" id="Seg_3466" s="T247">one</ta>
            <ta e="T249" id="Seg_3467" s="T248">year-ACC</ta>
            <ta e="T250" id="Seg_3468" s="T249">well</ta>
            <ta e="T251" id="Seg_3469" s="T250">that -ACC</ta>
            <ta e="T252" id="Seg_3470" s="T251">stop-CVB.SEQ</ta>
            <ta e="T253" id="Seg_3471" s="T252">after</ta>
            <ta e="T254" id="Seg_3472" s="T253">EMPH</ta>
            <ta e="T255" id="Seg_3473" s="T254">1SG-ACC</ta>
            <ta e="T256" id="Seg_3474" s="T255">EMPH</ta>
            <ta e="T257" id="Seg_3475" s="T256">Taymyr-DAT/LOC</ta>
            <ta e="T258" id="Seg_3476" s="T257">send-PST2-3PL</ta>
            <ta e="T259" id="Seg_3477" s="T258">be.born-PTCP.PST</ta>
            <ta e="T260" id="Seg_3478" s="T259">well</ta>
            <ta e="T261" id="Seg_3479" s="T260">place-1SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_3480" s="T261">well</ta>
            <ta e="T263" id="Seg_3481" s="T262">work-PTCP.FUT-1SG-ACC</ta>
            <ta e="T264" id="Seg_3482" s="T263">Dudinka-DAT/LOC</ta>
            <ta e="T265" id="Seg_3483" s="T264">come-CVB.SEQ</ta>
            <ta e="T266" id="Seg_3484" s="T265">after</ta>
            <ta e="T267" id="Seg_3485" s="T266">EMPH</ta>
            <ta e="T268" id="Seg_3486" s="T267">1SG-ACC</ta>
            <ta e="T269" id="Seg_3487" s="T268">EMPH</ta>
            <ta e="T270" id="Seg_3488" s="T269">Potapovo-DAT/LOC</ta>
            <ta e="T271" id="Seg_3489" s="T270">send-PST2-3PL</ta>
            <ta e="T272" id="Seg_3490" s="T271">there</ta>
            <ta e="T273" id="Seg_3491" s="T272">who.[NOM]</ta>
            <ta e="T274" id="Seg_3492" s="T273">there.is</ta>
            <ta e="T275" id="Seg_3493" s="T274">be-PST1-3SG</ta>
            <ta e="T278" id="Seg_3494" s="T277">no</ta>
            <ta e="T282" id="Seg_3495" s="T281">be-PST1-3SG</ta>
            <ta e="T284" id="Seg_3496" s="T283">well</ta>
            <ta e="T285" id="Seg_3497" s="T284">there</ta>
            <ta e="T286" id="Seg_3498" s="T285">MOD</ta>
            <ta e="T287" id="Seg_3499" s="T286">three</ta>
            <ta e="T288" id="Seg_3500" s="T287">year-ACC</ta>
            <ta e="T289" id="Seg_3501" s="T288">work-PST2-EP-1SG</ta>
            <ta e="T290" id="Seg_3502" s="T289">well</ta>
            <ta e="T291" id="Seg_3503" s="T290">eighty</ta>
            <ta e="T292" id="Seg_3504" s="T291">four-ORD-PROPR</ta>
            <ta e="T293" id="Seg_3505" s="T292">eighty</ta>
            <ta e="T294" id="Seg_3506" s="T293">four-ORD-PROPR</ta>
            <ta e="T295" id="Seg_3507" s="T294">year-DAT/LOC</ta>
            <ta e="T296" id="Seg_3508" s="T295">hither</ta>
            <ta e="T297" id="Seg_3509" s="T296">come-PST2-EP-1SG</ta>
            <ta e="T298" id="Seg_3510" s="T297">nomadize-CVB.SEQ</ta>
            <ta e="T300" id="Seg_3511" s="T299">send-PST2-3PL</ta>
            <ta e="T301" id="Seg_3512" s="T300">and</ta>
            <ta e="T302" id="Seg_3513" s="T301">that.EMPH.[NOM]</ta>
            <ta e="T303" id="Seg_3514" s="T302">year-ABL</ta>
            <ta e="T304" id="Seg_3515" s="T303">this</ta>
            <ta e="T305" id="Seg_3516" s="T304">now-DAT/LOC</ta>
            <ta e="T306" id="Seg_3517" s="T305">until</ta>
            <ta e="T307" id="Seg_3518" s="T306">MOD</ta>
            <ta e="T308" id="Seg_3519" s="T307">who-DAT/LOC</ta>
            <ta e="T309" id="Seg_3520" s="T308">work-PRS-1SG</ta>
            <ta e="T318" id="Seg_3521" s="T317">then</ta>
            <ta e="T319" id="Seg_3522" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_3523" s="T319">eighty</ta>
            <ta e="T321" id="Seg_3524" s="T320">nine-ORD-PROPR</ta>
            <ta e="T322" id="Seg_3525" s="T321">and</ta>
            <ta e="T323" id="Seg_3526" s="T322">nine-ORD-PROPR</ta>
            <ta e="T324" id="Seg_3527" s="T323">year-DAT/LOC</ta>
            <ta e="T325" id="Seg_3528" s="T324">MOD</ta>
            <ta e="T326" id="Seg_3529" s="T325">who.[NOM]</ta>
            <ta e="T327" id="Seg_3530" s="T326">be-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_3531" s="T327">earthquake.[NOM]</ta>
            <ta e="T329" id="Seg_3532" s="T328">be-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_3533" s="T329">Armenia-DAT/LOC</ta>
            <ta e="T331" id="Seg_3534" s="T330">there</ta>
            <ta e="T332" id="Seg_3535" s="T331">self-1SG.[NOM]</ta>
            <ta e="T333" id="Seg_3536" s="T332">wish-1SG-INSTR</ta>
            <ta e="T336" id="Seg_3537" s="T335">say-PTCP.PST-DAT/LOC</ta>
            <ta e="T337" id="Seg_3538" s="T336">well</ta>
            <ta e="T338" id="Seg_3539" s="T337">there</ta>
            <ta e="T339" id="Seg_3540" s="T338">work-PST2-EP-1SG</ta>
            <ta e="T340" id="Seg_3541" s="T339">two</ta>
            <ta e="T353" id="Seg_3542" s="T352">eighty</ta>
            <ta e="T358" id="Seg_3543" s="T357">this</ta>
            <ta e="T359" id="Seg_3544" s="T358">MOD</ta>
            <ta e="T360" id="Seg_3545" s="T359">who.[NOM]</ta>
            <ta e="T361" id="Seg_3546" s="T360">give-PST2-3PL</ta>
            <ta e="T362" id="Seg_3547" s="T361">1SG-DAT/LOC</ta>
            <ta e="T374" id="Seg_3548" s="T373">family-PL-1SG-ACC</ta>
            <ta e="T375" id="Seg_3549" s="T374">with</ta>
            <ta e="T376" id="Seg_3550" s="T375">mother-1SG-ACC</ta>
            <ta e="T377" id="Seg_3551" s="T376">with</ta>
            <ta e="T378" id="Seg_3552" s="T377">live-PRS-1SG</ta>
            <ta e="T379" id="Seg_3553" s="T378">now</ta>
            <ta e="T381" id="Seg_3554" s="T380">niece-1SG-ACC</ta>
            <ta e="T382" id="Seg_3555" s="T381">with</ta>
            <ta e="T383" id="Seg_3556" s="T382">that -3SG-1SG.[NOM]</ta>
            <ta e="T384" id="Seg_3557" s="T383">MOD</ta>
            <ta e="T385" id="Seg_3558" s="T384">ten-ORD</ta>
            <ta e="T386" id="Seg_3559" s="T385">class-DAT/LOC</ta>
            <ta e="T387" id="Seg_3560" s="T386">learn-PRS.[3SG]</ta>
            <ta e="T388" id="Seg_3561" s="T387">self-1SG.[NOM]</ta>
            <ta e="T389" id="Seg_3562" s="T388">always</ta>
            <ta e="T390" id="Seg_3563" s="T389">work-PRS-1SG</ta>
            <ta e="T391" id="Seg_3564" s="T390">and</ta>
            <ta e="T392" id="Seg_3565" s="T391">1PL-ACC</ta>
            <ta e="T393" id="Seg_3566" s="T392">EMPH</ta>
            <ta e="T394" id="Seg_3567" s="T393">five-ORD</ta>
            <ta e="T395" id="Seg_3568" s="T394">class-DAT/LOC</ta>
            <ta e="T396" id="Seg_3569" s="T395">EMPH</ta>
            <ta e="T397" id="Seg_3570" s="T396">five-ORD</ta>
            <ta e="T398" id="Seg_3571" s="T397">class-DAT/LOC</ta>
            <ta e="T399" id="Seg_3572" s="T398">learn-PTCP.PRS</ta>
            <ta e="T400" id="Seg_3573" s="T399">be-TEMP-1PL</ta>
            <ta e="T401" id="Seg_3574" s="T400">EMPH</ta>
            <ta e="T402" id="Seg_3575" s="T401">Khatanga-DAT/LOC</ta>
            <ta e="T403" id="Seg_3576" s="T402">MOD</ta>
            <ta e="T404" id="Seg_3577" s="T403">house.of.culture-DAT/LOC</ta>
            <ta e="T405" id="Seg_3578" s="T404">MOD</ta>
            <ta e="T406" id="Seg_3579" s="T405">1PL-ACC</ta>
            <ta e="T407" id="Seg_3580" s="T406">who.[NOM]</ta>
            <ta e="T416" id="Seg_3581" s="T415">that </ta>
            <ta e="T417" id="Seg_3582" s="T416">that -ACC</ta>
            <ta e="T419" id="Seg_3583" s="T418">1PL-ACC</ta>
            <ta e="T420" id="Seg_3584" s="T419">who-DAT/LOC</ta>
            <ta e="T421" id="Seg_3585" s="T420">teach-PST2-3SG</ta>
            <ta e="T422" id="Seg_3586" s="T421">Bargan-DAT/LOC</ta>
            <ta e="T423" id="Seg_3587" s="T422">play-CVB.SIM</ta>
            <ta e="T424" id="Seg_3588" s="T423">now</ta>
            <ta e="T425" id="Seg_3589" s="T424">MOD</ta>
            <ta e="T429" id="Seg_3590" s="T428">age-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T430" id="Seg_3591" s="T429">until</ta>
            <ta e="T431" id="Seg_3592" s="T430">always</ta>
            <ta e="T432" id="Seg_3593" s="T431">play-PRS-1SG</ta>
            <ta e="T433" id="Seg_3594" s="T432">now</ta>
            <ta e="T434" id="Seg_3595" s="T433">well</ta>
            <ta e="T435" id="Seg_3596" s="T434">Yakutsk-2SG-ABL</ta>
            <ta e="T436" id="Seg_3597" s="T435">who-3PL.[NOM]</ta>
            <ta e="T437" id="Seg_3598" s="T436">come-PST2.[3SG]</ta>
            <ta e="T438" id="Seg_3599" s="T437">artist-PL.[NOM]</ta>
            <ta e="T439" id="Seg_3600" s="T438">come-PTCP.PST</ta>
            <ta e="T440" id="Seg_3601" s="T439">be-PST1-3PL</ta>
            <ta e="T441" id="Seg_3602" s="T440">that -PL.[NOM]</ta>
            <ta e="T442" id="Seg_3603" s="T441">1PL-DAT/LOC</ta>
            <ta e="T443" id="Seg_3604" s="T442">Bargan.[NOM]</ta>
            <ta e="T444" id="Seg_3605" s="T443">sell-PST2-3PL</ta>
            <ta e="T446" id="Seg_3606" s="T445">this</ta>
            <ta e="T447" id="Seg_3607" s="T446">big</ta>
            <ta e="T448" id="Seg_3608" s="T447">money.[NOM]</ta>
            <ta e="T449" id="Seg_3609" s="T448">be-PST1-3SG</ta>
            <ta e="T450" id="Seg_3610" s="T449">EMPH</ta>
            <ta e="T451" id="Seg_3611" s="T450">eighty</ta>
            <ta e="T452" id="Seg_3612" s="T451">eighty</ta>
            <ta e="T453" id="Seg_3613" s="T452">ruble.[NOM]</ta>
            <ta e="T454" id="Seg_3614" s="T453">now</ta>
            <ta e="T455" id="Seg_3615" s="T454">EMPH</ta>
            <ta e="T456" id="Seg_3616" s="T455">always</ta>
            <ta e="T457" id="Seg_3617" s="T456">play-PRS-1SG</ta>
            <ta e="T458" id="Seg_3618" s="T457">wish-1SG.[NOM]</ta>
            <ta e="T459" id="Seg_3619" s="T458">come-TEMP-3SG</ta>
            <ta e="T460" id="Seg_3620" s="T459">now</ta>
            <ta e="T461" id="Seg_3621" s="T460">again</ta>
            <ta e="T462" id="Seg_3622" s="T461">2PL-DAT/LOC</ta>
            <ta e="T463" id="Seg_3623" s="T462">1SG.[NOM]</ta>
            <ta e="T464" id="Seg_3624" s="T463">play-FUT-1SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KiLS">
            <ta e="T2" id="Seg_3625" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_3626" s="T2">Name-EP-1SG</ta>
            <ta e="T4" id="Seg_3627" s="T3">Kirgizova</ta>
            <ta e="T5" id="Seg_3628" s="T4">Lina</ta>
            <ta e="T6" id="Seg_3629" s="T5">Semjonovna.[NOM]</ta>
            <ta e="T7" id="Seg_3630" s="T6">1SG.[NOM]</ta>
            <ta e="T8" id="Seg_3631" s="T7">1SG.[NOM]</ta>
            <ta e="T9" id="Seg_3632" s="T8">geboren.werden-PST2-EP-1SG</ta>
            <ta e="T10" id="Seg_3633" s="T9">MOD</ta>
            <ta e="T11" id="Seg_3634" s="T10">Chatanga-DAT/LOC</ta>
            <ta e="T12" id="Seg_3635" s="T11">Syndassko-DAT/LOC</ta>
            <ta e="T13" id="Seg_3636" s="T12">Eltern-PL-EP-1SG</ta>
            <ta e="T14" id="Seg_3637" s="T13">MOD</ta>
            <ta e="T15" id="Seg_3638" s="T14">Tundra-ADJZ.[NOM]</ta>
            <ta e="T16" id="Seg_3639" s="T15">sein-PST1-3PL</ta>
            <ta e="T17" id="Seg_3640" s="T16">wer-PL.[NOM]</ta>
            <ta e="T18" id="Seg_3641" s="T17">wer.[NOM]</ta>
            <ta e="T19" id="Seg_3642" s="T18">Rentier-AG-PL.[NOM]</ta>
            <ta e="T20" id="Seg_3643" s="T19">Korgo-DAT/LOC</ta>
            <ta e="T22" id="Seg_3644" s="T20">geboren.werden-PST2-EP-1SG</ta>
            <ta e="T25" id="Seg_3645" s="T24">Totfalle-AG-PL.[NOM]</ta>
            <ta e="T27" id="Seg_3646" s="T25">sein-PST1-3PL</ta>
            <ta e="T31" id="Seg_3647" s="T29">Mutter-1SG.[NOM]</ta>
            <ta e="T32" id="Seg_3648" s="T31">MOD</ta>
            <ta e="T33" id="Seg_3649" s="T32">und</ta>
            <ta e="T34" id="Seg_3650" s="T33">Mutter-1SG.[NOM]</ta>
            <ta e="T35" id="Seg_3651" s="T34">erzählen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T36" id="Seg_3652" s="T35">MOD</ta>
            <ta e="T37" id="Seg_3653" s="T36">1SG.[NOM]</ta>
            <ta e="T38" id="Seg_3654" s="T37">geboren.werden-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T39" id="Seg_3655" s="T38">EMPH</ta>
            <ta e="T40" id="Seg_3656" s="T39">1SG.[NOM]</ta>
            <ta e="T41" id="Seg_3657" s="T40">EMPH</ta>
            <ta e="T42" id="Seg_3658" s="T41">wer.[NOM]</ta>
            <ta e="T43" id="Seg_3659" s="T42">geboren.werden-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T44" id="Seg_3660" s="T43">EMPH</ta>
            <ta e="T45" id="Seg_3661" s="T44">wer.[NOM]</ta>
            <ta e="T46" id="Seg_3662" s="T45">Nagel.[NOM]</ta>
            <ta e="T47" id="Seg_3663" s="T46">legen-PST2-3PL</ta>
            <ta e="T48" id="Seg_3664" s="T47">Tundra-DAT/LOC</ta>
            <ta e="T49" id="Seg_3665" s="T48">dort</ta>
            <ta e="T50" id="Seg_3666" s="T49">EMPH</ta>
            <ta e="T51" id="Seg_3667" s="T50">Schabeisen.[NOM]</ta>
            <ta e="T52" id="Seg_3668" s="T51">was.[NOM]</ta>
            <ta e="T53" id="Seg_3669" s="T52">es.gibt</ta>
            <ta e="T55" id="Seg_3670" s="T53">noch</ta>
            <ta e="T59" id="Seg_3671" s="T57">Lederschaber.[NOM]</ta>
            <ta e="T62" id="Seg_3672" s="T60">Schaber.mit.Zähnen.[NOM]</ta>
            <ta e="T63" id="Seg_3673" s="T62">jenes-ACC</ta>
            <ta e="T64" id="Seg_3674" s="T63">jenes-ACC</ta>
            <ta e="T65" id="Seg_3675" s="T64">jenes-ACC</ta>
            <ta e="T66" id="Seg_3676" s="T65">MOD</ta>
            <ta e="T67" id="Seg_3677" s="T66">Holz-DAT/LOC</ta>
            <ta e="T68" id="Seg_3678" s="T67">binden-CVB.SEQ</ta>
            <ta e="T69" id="Seg_3679" s="T68">nachdem</ta>
            <ta e="T70" id="Seg_3680" s="T69">dieses</ta>
            <ta e="T71" id="Seg_3681" s="T70">jenes</ta>
            <ta e="T72" id="Seg_3682" s="T71">Name-3SG.[NOM]</ta>
            <ta e="T73" id="Seg_3683" s="T72">Pfahl</ta>
            <ta e="T74" id="Seg_3684" s="T73">dieses</ta>
            <ta e="T75" id="Seg_3685" s="T74">nun</ta>
            <ta e="T76" id="Seg_3686" s="T75">Erde-DAT/LOC</ta>
            <ta e="T77" id="Seg_3687" s="T76">legen-CVB.SEQ</ta>
            <ta e="T78" id="Seg_3688" s="T77">werfen-PST2-3PL</ta>
            <ta e="T79" id="Seg_3689" s="T78">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T80" id="Seg_3690" s="T79">jenes</ta>
            <ta e="T81" id="Seg_3691" s="T80">Pfahl-2SG.[NOM]</ta>
            <ta e="T82" id="Seg_3692" s="T81">jenes</ta>
            <ta e="T83" id="Seg_3693" s="T82">Pfahl-2SG.[NOM]</ta>
            <ta e="T84" id="Seg_3694" s="T83">dieses</ta>
            <ta e="T85" id="Seg_3695" s="T84">immer</ta>
            <ta e="T86" id="Seg_3696" s="T85">dann</ta>
            <ta e="T87" id="Seg_3697" s="T86">es.gibt</ta>
            <ta e="T88" id="Seg_3698" s="T87">dann</ta>
            <ta e="T89" id="Seg_3699" s="T88">MOD</ta>
            <ta e="T90" id="Seg_3700" s="T89">1SG.[NOM]</ta>
            <ta e="T91" id="Seg_3701" s="T90">sieben</ta>
            <ta e="T92" id="Seg_3702" s="T91">sieben</ta>
            <ta e="T93" id="Seg_3703" s="T92">sieben</ta>
            <ta e="T94" id="Seg_3704" s="T93">Jahr-PROPR.[NOM]</ta>
            <ta e="T95" id="Seg_3705" s="T94">sein-TEMP-1SG</ta>
            <ta e="T96" id="Seg_3706" s="T95">aber</ta>
            <ta e="T97" id="Seg_3707" s="T96">wer.[NOM]</ta>
            <ta e="T98" id="Seg_3708" s="T97">Syndassko-DAT/LOC</ta>
            <ta e="T99" id="Seg_3709" s="T98">hineingehen-PST2-1PL</ta>
            <ta e="T100" id="Seg_3710" s="T99">so-EMPH</ta>
            <ta e="T101" id="Seg_3711" s="T100">1PL.[NOM]</ta>
            <ta e="T102" id="Seg_3712" s="T101">immer</ta>
            <ta e="T103" id="Seg_3713" s="T102">Tundra-DAT/LOC</ta>
            <ta e="T104" id="Seg_3714" s="T103">gehen-PTCP.HAB</ta>
            <ta e="T105" id="Seg_3715" s="T104">sein-PST1-1PL</ta>
            <ta e="T106" id="Seg_3716" s="T105">Mutter.[NOM]</ta>
            <ta e="T107" id="Seg_3717" s="T106">dieses</ta>
            <ta e="T108" id="Seg_3718" s="T107">viel</ta>
            <ta e="T109" id="Seg_3719" s="T108">sehr</ta>
            <ta e="T110" id="Seg_3720" s="T109">Herde-PROPR.[NOM]</ta>
            <ta e="T112" id="Seg_3721" s="T111">viel</ta>
            <ta e="T113" id="Seg_3722" s="T112">sehr</ta>
            <ta e="T114" id="Seg_3723" s="T113">Herde-PROPR.[NOM]</ta>
            <ta e="T115" id="Seg_3724" s="T114">sein-PST1-1PL</ta>
            <ta e="T116" id="Seg_3725" s="T115">viel</ta>
            <ta e="T117" id="Seg_3726" s="T116">Familie-PL.[NOM]</ta>
            <ta e="T118" id="Seg_3727" s="T117">sein-PST1-3PL</ta>
            <ta e="T119" id="Seg_3728" s="T118">Nomade.[NOM]</ta>
            <ta e="T120" id="Seg_3729" s="T119">Mensch.[NOM]</ta>
            <ta e="T121" id="Seg_3730" s="T120">sechs-DISTR</ta>
            <ta e="T122" id="Seg_3731" s="T121">fünf-DISTR</ta>
            <ta e="T123" id="Seg_3732" s="T122">Balok-PL.[NOM]</ta>
            <ta e="T124" id="Seg_3733" s="T123">Stangenzelt-PL.[NOM]</ta>
            <ta e="T125" id="Seg_3734" s="T124">gehen-HAB-1PL</ta>
            <ta e="T127" id="Seg_3735" s="T125">Mutter.[NOM]</ta>
            <ta e="T132" id="Seg_3736" s="T131">dann</ta>
            <ta e="T133" id="Seg_3737" s="T132">1PL.[NOM]</ta>
            <ta e="T134" id="Seg_3738" s="T133">noch</ta>
            <ta e="T135" id="Seg_3739" s="T134">am.anderen.Ufer</ta>
            <ta e="T136" id="Seg_3740" s="T135">liegen-PST2-1PL</ta>
            <ta e="T137" id="Seg_3741" s="T136">hoch</ta>
            <ta e="T138" id="Seg_3742" s="T137">Berg-DAT/LOC</ta>
            <ta e="T139" id="Seg_3743" s="T138">dort</ta>
            <ta e="T140" id="Seg_3744" s="T139">sechs-DISTR</ta>
            <ta e="T141" id="Seg_3745" s="T140">fünf-DISTR</ta>
            <ta e="T142" id="Seg_3746" s="T141">Stange.[NOM]</ta>
            <ta e="T144" id="Seg_3747" s="T142">Zelt.[NOM]</ta>
            <ta e="T151" id="Seg_3748" s="T150">dort</ta>
            <ta e="T152" id="Seg_3749" s="T151">wer.[NOM]</ta>
            <ta e="T153" id="Seg_3750" s="T152">zwei</ta>
            <ta e="T154" id="Seg_3751" s="T153">zwei</ta>
            <ta e="T155" id="Seg_3752" s="T154">zwei</ta>
            <ta e="T156" id="Seg_3753" s="T155">Kind.[NOM]</ta>
            <ta e="T157" id="Seg_3754" s="T156">zwei</ta>
            <ta e="T158" id="Seg_3755" s="T157">nur</ta>
            <ta e="T159" id="Seg_3756" s="T158">Kind-PROPR.[NOM]</ta>
            <ta e="T160" id="Seg_3757" s="T159">sein-PST1-3SG</ta>
            <ta e="T161" id="Seg_3758" s="T160">dort</ta>
            <ta e="T162" id="Seg_3759" s="T161">am.anderen.Ufer</ta>
            <ta e="T163" id="Seg_3760" s="T162">MOD</ta>
            <ta e="T164" id="Seg_3761" s="T163">wer.[NOM]</ta>
            <ta e="T165" id="Seg_3762" s="T164">hoch</ta>
            <ta e="T166" id="Seg_3763" s="T165">Berg-DAT/LOC</ta>
            <ta e="T167" id="Seg_3764" s="T166">sechs-DISTR</ta>
            <ta e="T168" id="Seg_3765" s="T167">fünf-DISTR</ta>
            <ta e="T169" id="Seg_3766" s="T168">Stange.[NOM]</ta>
            <ta e="T170" id="Seg_3767" s="T169">Zelt.[NOM]</ta>
            <ta e="T171" id="Seg_3768" s="T170">stehen-PTCP.HAB</ta>
            <ta e="T172" id="Seg_3769" s="T171">sein-PST1-3PL</ta>
            <ta e="T173" id="Seg_3770" s="T172">dort</ta>
            <ta e="T174" id="Seg_3771" s="T173">MOD</ta>
            <ta e="T175" id="Seg_3772" s="T174">Fischer-PL.[NOM]</ta>
            <ta e="T176" id="Seg_3773" s="T175">im.Sommer</ta>
            <ta e="T177" id="Seg_3774" s="T176">MOD</ta>
            <ta e="T178" id="Seg_3775" s="T177">Fischer-VBZ-PTCP.HAB</ta>
            <ta e="T179" id="Seg_3776" s="T178">sein-PST1-3PL</ta>
            <ta e="T180" id="Seg_3777" s="T179">so</ta>
            <ta e="T181" id="Seg_3778" s="T180">dann</ta>
            <ta e="T182" id="Seg_3779" s="T181">EMPH</ta>
            <ta e="T183" id="Seg_3780" s="T182">1SG.[NOM]</ta>
            <ta e="T185" id="Seg_3781" s="T184">Syndassko-DAT/LOC</ta>
            <ta e="T186" id="Seg_3782" s="T185">hineingehen-PST2-1PL</ta>
            <ta e="T187" id="Seg_3783" s="T186">Vater-1SG.[NOM]</ta>
            <ta e="T188" id="Seg_3784" s="T187">sterben-CVB.SEQ</ta>
            <ta e="T189" id="Seg_3785" s="T188">sterben-CVB.SEQ</ta>
            <ta e="T190" id="Seg_3786" s="T189">dort</ta>
            <ta e="T191" id="Seg_3787" s="T190">EMPH</ta>
            <ta e="T192" id="Seg_3788" s="T191">1PL.[NOM]</ta>
            <ta e="T193" id="Seg_3789" s="T192">MOD</ta>
            <ta e="T194" id="Seg_3790" s="T193">lernen-PST2-1PL</ta>
            <ta e="T195" id="Seg_3791" s="T194">Novorybnoe-DAT/LOC</ta>
            <ta e="T196" id="Seg_3792" s="T195">Kosistyj-DAT/LOC</ta>
            <ta e="T197" id="Seg_3793" s="T196">Chatanga-DAT/LOC</ta>
            <ta e="T198" id="Seg_3794" s="T197">Chatanga-DAT/LOC</ta>
            <ta e="T199" id="Seg_3795" s="T198">EMPH</ta>
            <ta e="T200" id="Seg_3796" s="T199">acht</ta>
            <ta e="T201" id="Seg_3797" s="T200">Klasse-ACC</ta>
            <ta e="T202" id="Seg_3798" s="T201">aufhören-CVB.SEQ</ta>
            <ta e="T203" id="Seg_3799" s="T202">nachdem</ta>
            <ta e="T204" id="Seg_3800" s="T203">MOD</ta>
            <ta e="T205" id="Seg_3801" s="T204">1PL-ACC</ta>
            <ta e="T206" id="Seg_3802" s="T205">wer-DAT/LOC</ta>
            <ta e="T207" id="Seg_3803" s="T206">schicken-PST2-3PL</ta>
            <ta e="T208" id="Seg_3804" s="T207">Krasnojarsk-DAT/LOC</ta>
            <ta e="T209" id="Seg_3805" s="T208">dort</ta>
            <ta e="T210" id="Seg_3806" s="T209">MOD</ta>
            <ta e="T211" id="Seg_3807" s="T210">zwanzig</ta>
            <ta e="T212" id="Seg_3808" s="T211">sieben-ORD</ta>
            <ta e="T213" id="Seg_3809" s="T212">Schule-DAT/LOC</ta>
            <ta e="T214" id="Seg_3810" s="T213">lernen-PST2-1PL</ta>
            <ta e="T217" id="Seg_3811" s="T216">Klasse-ACC</ta>
            <ta e="T218" id="Seg_3812" s="T217">dann</ta>
            <ta e="T219" id="Seg_3813" s="T218">MOD</ta>
            <ta e="T220" id="Seg_3814" s="T219">äh</ta>
            <ta e="T222" id="Seg_3815" s="T221">achtzig</ta>
            <ta e="T223" id="Seg_3816" s="T222">Jahr-PROPR-DAT/LOC</ta>
            <ta e="T225" id="Seg_3817" s="T224">1PL.[NOM]</ta>
            <ta e="T226" id="Seg_3818" s="T225">MOD</ta>
            <ta e="T228" id="Seg_3819" s="T227">wer.[NOM]</ta>
            <ta e="T229" id="Seg_3820" s="T228">wer-DAT/LOC</ta>
            <ta e="T230" id="Seg_3821" s="T229">betreten-VBZ-PST2-EP-1SG</ta>
            <ta e="T235" id="Seg_3822" s="T234">dort</ta>
            <ta e="T236" id="Seg_3823" s="T235">EMPH</ta>
            <ta e="T237" id="Seg_3824" s="T236">sechs</ta>
            <ta e="T238" id="Seg_3825" s="T237">Jahr-ACC</ta>
            <ta e="T239" id="Seg_3826" s="T238">lernen-PST2-EP-1SG</ta>
            <ta e="T240" id="Seg_3827" s="T239">dann</ta>
            <ta e="T241" id="Seg_3828" s="T240">EMPH</ta>
            <ta e="T242" id="Seg_3829" s="T241">sieben-ORD</ta>
            <ta e="T243" id="Seg_3830" s="T242">Jahr-EP-1SG.[NOM]</ta>
            <ta e="T244" id="Seg_3831" s="T243">Praktikum.[NOM]</ta>
            <ta e="T247" id="Seg_3832" s="T246">Praktikum.[NOM]</ta>
            <ta e="T248" id="Seg_3833" s="T247">eins</ta>
            <ta e="T249" id="Seg_3834" s="T248">Jahr-ACC</ta>
            <ta e="T250" id="Seg_3835" s="T249">so</ta>
            <ta e="T251" id="Seg_3836" s="T250">jenes-ACC</ta>
            <ta e="T252" id="Seg_3837" s="T251">aufhören-CVB.SEQ</ta>
            <ta e="T253" id="Seg_3838" s="T252">nachdem</ta>
            <ta e="T254" id="Seg_3839" s="T253">EMPH</ta>
            <ta e="T255" id="Seg_3840" s="T254">1SG-ACC</ta>
            <ta e="T256" id="Seg_3841" s="T255">EMPH</ta>
            <ta e="T257" id="Seg_3842" s="T256">Taimyr-DAT/LOC</ta>
            <ta e="T258" id="Seg_3843" s="T257">schicken-PST2-3PL</ta>
            <ta e="T259" id="Seg_3844" s="T258">geboren.werden-PTCP.PST</ta>
            <ta e="T260" id="Seg_3845" s="T259">nun</ta>
            <ta e="T261" id="Seg_3846" s="T260">Ort-1SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_3847" s="T261">nun</ta>
            <ta e="T263" id="Seg_3848" s="T262">arbeiten-PTCP.FUT-1SG-ACC</ta>
            <ta e="T264" id="Seg_3849" s="T263">Dudinka-DAT/LOC</ta>
            <ta e="T265" id="Seg_3850" s="T264">kommen-CVB.SEQ</ta>
            <ta e="T266" id="Seg_3851" s="T265">nachdem</ta>
            <ta e="T267" id="Seg_3852" s="T266">EMPH</ta>
            <ta e="T268" id="Seg_3853" s="T267">1SG-ACC</ta>
            <ta e="T269" id="Seg_3854" s="T268">EMPH</ta>
            <ta e="T270" id="Seg_3855" s="T269">Potapovo-DAT/LOC</ta>
            <ta e="T271" id="Seg_3856" s="T270">schicken-PST2-3PL</ta>
            <ta e="T272" id="Seg_3857" s="T271">dort</ta>
            <ta e="T273" id="Seg_3858" s="T272">wer.[NOM]</ta>
            <ta e="T274" id="Seg_3859" s="T273">es.gibt</ta>
            <ta e="T275" id="Seg_3860" s="T274">sein-PST1-3SG</ta>
            <ta e="T278" id="Seg_3861" s="T277">nein</ta>
            <ta e="T282" id="Seg_3862" s="T281">sein-PST1-3SG</ta>
            <ta e="T284" id="Seg_3863" s="T283">so</ta>
            <ta e="T285" id="Seg_3864" s="T284">dort</ta>
            <ta e="T286" id="Seg_3865" s="T285">MOD</ta>
            <ta e="T287" id="Seg_3866" s="T286">drei</ta>
            <ta e="T288" id="Seg_3867" s="T287">Jahr-ACC</ta>
            <ta e="T289" id="Seg_3868" s="T288">arbeiten-PST2-EP-1SG</ta>
            <ta e="T290" id="Seg_3869" s="T289">so</ta>
            <ta e="T291" id="Seg_3870" s="T290">achtzig</ta>
            <ta e="T292" id="Seg_3871" s="T291">vier-ORD-PROPR</ta>
            <ta e="T293" id="Seg_3872" s="T292">achtzig</ta>
            <ta e="T294" id="Seg_3873" s="T293">vier-ORD-PROPR</ta>
            <ta e="T295" id="Seg_3874" s="T294">Jahr-DAT/LOC</ta>
            <ta e="T296" id="Seg_3875" s="T295">hierher</ta>
            <ta e="T297" id="Seg_3876" s="T296">kommen-PST2-EP-1SG</ta>
            <ta e="T298" id="Seg_3877" s="T297">nomadisieren-CVB.SEQ</ta>
            <ta e="T300" id="Seg_3878" s="T299">schicken-PST2-3PL</ta>
            <ta e="T301" id="Seg_3879" s="T300">und</ta>
            <ta e="T302" id="Seg_3880" s="T301">jenes.EMPH.[NOM]</ta>
            <ta e="T303" id="Seg_3881" s="T302">Jahr-ABL</ta>
            <ta e="T304" id="Seg_3882" s="T303">dieses</ta>
            <ta e="T305" id="Seg_3883" s="T304">jetzt-DAT/LOC</ta>
            <ta e="T306" id="Seg_3884" s="T305">bis.zu</ta>
            <ta e="T307" id="Seg_3885" s="T306">MOD</ta>
            <ta e="T308" id="Seg_3886" s="T307">wer-DAT/LOC</ta>
            <ta e="T309" id="Seg_3887" s="T308">arbeiten-PRS-1SG</ta>
            <ta e="T318" id="Seg_3888" s="T317">dann</ta>
            <ta e="T319" id="Seg_3889" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_3890" s="T319">achtzig</ta>
            <ta e="T321" id="Seg_3891" s="T320">neun-ORD-PROPR</ta>
            <ta e="T322" id="Seg_3892" s="T321">und</ta>
            <ta e="T323" id="Seg_3893" s="T322">neun-ORD-PROPR</ta>
            <ta e="T324" id="Seg_3894" s="T323">Jahr-DAT/LOC</ta>
            <ta e="T325" id="Seg_3895" s="T324">MOD</ta>
            <ta e="T326" id="Seg_3896" s="T325">wer.[NOM]</ta>
            <ta e="T327" id="Seg_3897" s="T326">sein-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_3898" s="T327">Erdbeben.[NOM]</ta>
            <ta e="T329" id="Seg_3899" s="T328">sein-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_3900" s="T329">Armenien-DAT/LOC</ta>
            <ta e="T331" id="Seg_3901" s="T330">dort</ta>
            <ta e="T332" id="Seg_3902" s="T331">selbst-1SG.[NOM]</ta>
            <ta e="T333" id="Seg_3903" s="T332">Wunsch-1SG-INSTR</ta>
            <ta e="T336" id="Seg_3904" s="T335">sagen-PTCP.PST-DAT/LOC</ta>
            <ta e="T337" id="Seg_3905" s="T336">doch</ta>
            <ta e="T338" id="Seg_3906" s="T337">dort</ta>
            <ta e="T339" id="Seg_3907" s="T338">arbeiten-PST2-EP-1SG</ta>
            <ta e="T340" id="Seg_3908" s="T339">zwei</ta>
            <ta e="T353" id="Seg_3909" s="T352">achtzig</ta>
            <ta e="T358" id="Seg_3910" s="T357">dieses</ta>
            <ta e="T359" id="Seg_3911" s="T358">MOD</ta>
            <ta e="T360" id="Seg_3912" s="T359">wer.[NOM]</ta>
            <ta e="T361" id="Seg_3913" s="T360">geben-PST2-3PL</ta>
            <ta e="T362" id="Seg_3914" s="T361">1SG-DAT/LOC</ta>
            <ta e="T374" id="Seg_3915" s="T373">Familie-PL-1SG-ACC</ta>
            <ta e="T375" id="Seg_3916" s="T374">mit</ta>
            <ta e="T376" id="Seg_3917" s="T375">Mutter-1SG-ACC</ta>
            <ta e="T377" id="Seg_3918" s="T376">mit</ta>
            <ta e="T378" id="Seg_3919" s="T377">leben-PRS-1SG</ta>
            <ta e="T379" id="Seg_3920" s="T378">jetzt</ta>
            <ta e="T381" id="Seg_3921" s="T380">Nichte-1SG-ACC</ta>
            <ta e="T382" id="Seg_3922" s="T381">mit</ta>
            <ta e="T383" id="Seg_3923" s="T382">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T384" id="Seg_3924" s="T383">MOD</ta>
            <ta e="T385" id="Seg_3925" s="T384">zehn-ORD</ta>
            <ta e="T386" id="Seg_3926" s="T385">Klasse-DAT/LOC</ta>
            <ta e="T387" id="Seg_3927" s="T386">lernen-PRS.[3SG]</ta>
            <ta e="T388" id="Seg_3928" s="T387">selbst-1SG.[NOM]</ta>
            <ta e="T389" id="Seg_3929" s="T388">immer</ta>
            <ta e="T390" id="Seg_3930" s="T389">arbeiten-PRS-1SG</ta>
            <ta e="T391" id="Seg_3931" s="T390">und</ta>
            <ta e="T392" id="Seg_3932" s="T391">1PL-ACC</ta>
            <ta e="T393" id="Seg_3933" s="T392">EMPH</ta>
            <ta e="T394" id="Seg_3934" s="T393">fünf-ORD</ta>
            <ta e="T395" id="Seg_3935" s="T394">Klasse-DAT/LOC</ta>
            <ta e="T396" id="Seg_3936" s="T395">EMPH</ta>
            <ta e="T397" id="Seg_3937" s="T396">fünf-ORD</ta>
            <ta e="T398" id="Seg_3938" s="T397">Klasse-DAT/LOC</ta>
            <ta e="T399" id="Seg_3939" s="T398">lernen-PTCP.PRS</ta>
            <ta e="T400" id="Seg_3940" s="T399">sein-TEMP-1PL</ta>
            <ta e="T401" id="Seg_3941" s="T400">EMPH</ta>
            <ta e="T402" id="Seg_3942" s="T401">Chatanga-DAT/LOC</ta>
            <ta e="T403" id="Seg_3943" s="T402">MOD</ta>
            <ta e="T404" id="Seg_3944" s="T403">Kulturhaus-DAT/LOC</ta>
            <ta e="T405" id="Seg_3945" s="T404">MOD</ta>
            <ta e="T406" id="Seg_3946" s="T405">1PL-ACC</ta>
            <ta e="T407" id="Seg_3947" s="T406">wer.[NOM]</ta>
            <ta e="T416" id="Seg_3948" s="T415">jenes</ta>
            <ta e="T417" id="Seg_3949" s="T416">jenes-ACC</ta>
            <ta e="T419" id="Seg_3950" s="T418">1PL-ACC</ta>
            <ta e="T420" id="Seg_3951" s="T419">wer-DAT/LOC</ta>
            <ta e="T421" id="Seg_3952" s="T420">lehren-PST2-3SG</ta>
            <ta e="T422" id="Seg_3953" s="T421">Bargan-DAT/LOC</ta>
            <ta e="T423" id="Seg_3954" s="T422">spielen-CVB.SIM</ta>
            <ta e="T424" id="Seg_3955" s="T423">jetzt</ta>
            <ta e="T425" id="Seg_3956" s="T424">MOD</ta>
            <ta e="T429" id="Seg_3957" s="T428">altern-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T430" id="Seg_3958" s="T429">bis.zu</ta>
            <ta e="T431" id="Seg_3959" s="T430">immer</ta>
            <ta e="T432" id="Seg_3960" s="T431">spielen-PRS-1SG</ta>
            <ta e="T433" id="Seg_3961" s="T432">jetzt</ta>
            <ta e="T434" id="Seg_3962" s="T433">so</ta>
            <ta e="T435" id="Seg_3963" s="T434">Jakutsk-2SG-ABL</ta>
            <ta e="T436" id="Seg_3964" s="T435">wer-3PL.[NOM]</ta>
            <ta e="T437" id="Seg_3965" s="T436">kommen-PST2.[3SG]</ta>
            <ta e="T438" id="Seg_3966" s="T437">Künstler-PL.[NOM]</ta>
            <ta e="T439" id="Seg_3967" s="T438">kommen-PTCP.PST</ta>
            <ta e="T440" id="Seg_3968" s="T439">sein-PST1-3PL</ta>
            <ta e="T441" id="Seg_3969" s="T440">jenes-PL.[NOM]</ta>
            <ta e="T442" id="Seg_3970" s="T441">1PL-DAT/LOC</ta>
            <ta e="T443" id="Seg_3971" s="T442">Bargan.[NOM]</ta>
            <ta e="T444" id="Seg_3972" s="T443">verkaufen-PST2-3PL</ta>
            <ta e="T446" id="Seg_3973" s="T445">dieses</ta>
            <ta e="T447" id="Seg_3974" s="T446">groß</ta>
            <ta e="T448" id="Seg_3975" s="T447">Geld.[NOM]</ta>
            <ta e="T449" id="Seg_3976" s="T448">sein-PST1-3SG</ta>
            <ta e="T450" id="Seg_3977" s="T449">EMPH</ta>
            <ta e="T451" id="Seg_3978" s="T450">achtzig</ta>
            <ta e="T452" id="Seg_3979" s="T451">achtzig</ta>
            <ta e="T453" id="Seg_3980" s="T452">Rubel.[NOM]</ta>
            <ta e="T454" id="Seg_3981" s="T453">jetzt</ta>
            <ta e="T455" id="Seg_3982" s="T454">EMPH</ta>
            <ta e="T456" id="Seg_3983" s="T455">immer</ta>
            <ta e="T457" id="Seg_3984" s="T456">spielen-PRS-1SG</ta>
            <ta e="T458" id="Seg_3985" s="T457">Wunsch-1SG.[NOM]</ta>
            <ta e="T459" id="Seg_3986" s="T458">kommen-TEMP-3SG</ta>
            <ta e="T460" id="Seg_3987" s="T459">jetzt</ta>
            <ta e="T461" id="Seg_3988" s="T460">wieder</ta>
            <ta e="T462" id="Seg_3989" s="T461">2PL-DAT/LOC</ta>
            <ta e="T463" id="Seg_3990" s="T462">1SG.[NOM]</ta>
            <ta e="T464" id="Seg_3991" s="T463">spielen-FUT-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KiLS">
            <ta e="T2" id="Seg_3992" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_3993" s="T2">имя-EP-1SG</ta>
            <ta e="T4" id="Seg_3994" s="T3">Киргизова</ta>
            <ta e="T5" id="Seg_3995" s="T4">Лина</ta>
            <ta e="T6" id="Seg_3996" s="T5">Семеновна.[NOM]</ta>
            <ta e="T7" id="Seg_3997" s="T6">1SG.[NOM]</ta>
            <ta e="T8" id="Seg_3998" s="T7">1SG.[NOM]</ta>
            <ta e="T9" id="Seg_3999" s="T8">родиться-PST2-EP-1SG</ta>
            <ta e="T10" id="Seg_4000" s="T9">MOD</ta>
            <ta e="T11" id="Seg_4001" s="T10">Хатанга-DAT/LOC</ta>
            <ta e="T12" id="Seg_4002" s="T11">Сындасско-DAT/LOC</ta>
            <ta e="T13" id="Seg_4003" s="T12">родители-PL-EP-1SG</ta>
            <ta e="T14" id="Seg_4004" s="T13">MOD</ta>
            <ta e="T15" id="Seg_4005" s="T14">тундра-ADJZ.[NOM]</ta>
            <ta e="T16" id="Seg_4006" s="T15">быть-PST1-3PL</ta>
            <ta e="T17" id="Seg_4007" s="T16">кто-PL.[NOM]</ta>
            <ta e="T18" id="Seg_4008" s="T17">кто.[NOM]</ta>
            <ta e="T19" id="Seg_4009" s="T18">олень-AG-PL.[NOM]</ta>
            <ta e="T20" id="Seg_4010" s="T19">Корго-DAT/LOC</ta>
            <ta e="T22" id="Seg_4011" s="T20">родиться-PST2-EP-1SG</ta>
            <ta e="T25" id="Seg_4012" s="T24">пасть-AG-PL.[NOM]</ta>
            <ta e="T27" id="Seg_4013" s="T25">быть-PST1-3PL</ta>
            <ta e="T31" id="Seg_4014" s="T29">мать-1SG.[NOM]</ta>
            <ta e="T32" id="Seg_4015" s="T31">MOD</ta>
            <ta e="T33" id="Seg_4016" s="T32">и</ta>
            <ta e="T34" id="Seg_4017" s="T33">мать-1SG.[NOM]</ta>
            <ta e="T35" id="Seg_4018" s="T34">рассказывать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T36" id="Seg_4019" s="T35">MOD</ta>
            <ta e="T37" id="Seg_4020" s="T36">1SG.[NOM]</ta>
            <ta e="T38" id="Seg_4021" s="T37">родиться-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T39" id="Seg_4022" s="T38">EMPH</ta>
            <ta e="T40" id="Seg_4023" s="T39">1SG.[NOM]</ta>
            <ta e="T41" id="Seg_4024" s="T40">EMPH</ta>
            <ta e="T42" id="Seg_4025" s="T41">кто.[NOM]</ta>
            <ta e="T43" id="Seg_4026" s="T42">родиться-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T44" id="Seg_4027" s="T43">EMPH</ta>
            <ta e="T45" id="Seg_4028" s="T44">кто.[NOM]</ta>
            <ta e="T46" id="Seg_4029" s="T45">гвоздь.[NOM]</ta>
            <ta e="T47" id="Seg_4030" s="T46">класть-PST2-3PL</ta>
            <ta e="T48" id="Seg_4031" s="T47">тундра-DAT/LOC</ta>
            <ta e="T49" id="Seg_4032" s="T48">там</ta>
            <ta e="T50" id="Seg_4033" s="T49">EMPH</ta>
            <ta e="T51" id="Seg_4034" s="T50">скребок.[NOM]</ta>
            <ta e="T52" id="Seg_4035" s="T51">что.[NOM]</ta>
            <ta e="T53" id="Seg_4036" s="T52">есть</ta>
            <ta e="T55" id="Seg_4037" s="T53">еще</ta>
            <ta e="T59" id="Seg_4038" s="T57">кожемялка.[NOM]</ta>
            <ta e="T62" id="Seg_4039" s="T60">скребок.зубчатый.[NOM]</ta>
            <ta e="T63" id="Seg_4040" s="T62">тот -ACC</ta>
            <ta e="T64" id="Seg_4041" s="T63">тот -ACC</ta>
            <ta e="T65" id="Seg_4042" s="T64">тот -ACC</ta>
            <ta e="T66" id="Seg_4043" s="T65">MOD</ta>
            <ta e="T67" id="Seg_4044" s="T66">дерево-DAT/LOC</ta>
            <ta e="T68" id="Seg_4045" s="T67">связывать-CVB.SEQ</ta>
            <ta e="T69" id="Seg_4046" s="T68">после</ta>
            <ta e="T70" id="Seg_4047" s="T69">этот</ta>
            <ta e="T71" id="Seg_4048" s="T70">тот </ta>
            <ta e="T72" id="Seg_4049" s="T71">имя-3SG.[NOM]</ta>
            <ta e="T73" id="Seg_4050" s="T72">кол</ta>
            <ta e="T74" id="Seg_4051" s="T73">этот</ta>
            <ta e="T75" id="Seg_4052" s="T74">вот</ta>
            <ta e="T76" id="Seg_4053" s="T75">земля-DAT/LOC</ta>
            <ta e="T77" id="Seg_4054" s="T76">класть-CVB.SEQ</ta>
            <ta e="T78" id="Seg_4055" s="T77">бросать-PST2-3PL</ta>
            <ta e="T79" id="Seg_4056" s="T78">тот -3SG-2SG.[NOM]</ta>
            <ta e="T80" id="Seg_4057" s="T79">тот </ta>
            <ta e="T81" id="Seg_4058" s="T80">кол-2SG.[NOM]</ta>
            <ta e="T82" id="Seg_4059" s="T81">тот </ta>
            <ta e="T83" id="Seg_4060" s="T82">кол-2SG.[NOM]</ta>
            <ta e="T84" id="Seg_4061" s="T83">этот</ta>
            <ta e="T85" id="Seg_4062" s="T84">всегда</ta>
            <ta e="T86" id="Seg_4063" s="T85">то</ta>
            <ta e="T87" id="Seg_4064" s="T86">есть</ta>
            <ta e="T88" id="Seg_4065" s="T87">потом</ta>
            <ta e="T89" id="Seg_4066" s="T88">MOD</ta>
            <ta e="T90" id="Seg_4067" s="T89">1SG.[NOM]</ta>
            <ta e="T91" id="Seg_4068" s="T90">семь</ta>
            <ta e="T92" id="Seg_4069" s="T91">семь</ta>
            <ta e="T93" id="Seg_4070" s="T92">семь</ta>
            <ta e="T94" id="Seg_4071" s="T93">год-PROPR.[NOM]</ta>
            <ta e="T95" id="Seg_4072" s="T94">быть-TEMP-1SG</ta>
            <ta e="T96" id="Seg_4073" s="T95">однако</ta>
            <ta e="T97" id="Seg_4074" s="T96">кто.[NOM]</ta>
            <ta e="T98" id="Seg_4075" s="T97">Сындасско-DAT/LOC</ta>
            <ta e="T99" id="Seg_4076" s="T98">входить-PST2-1PL</ta>
            <ta e="T100" id="Seg_4077" s="T99">так-EMPH</ta>
            <ta e="T101" id="Seg_4078" s="T100">1PL.[NOM]</ta>
            <ta e="T102" id="Seg_4079" s="T101">всегда</ta>
            <ta e="T103" id="Seg_4080" s="T102">тундра-DAT/LOC</ta>
            <ta e="T104" id="Seg_4081" s="T103">идти-PTCP.HAB</ta>
            <ta e="T105" id="Seg_4082" s="T104">быть-PST1-1PL</ta>
            <ta e="T106" id="Seg_4083" s="T105">мать.[NOM]</ta>
            <ta e="T107" id="Seg_4084" s="T106">этот</ta>
            <ta e="T108" id="Seg_4085" s="T107">много</ta>
            <ta e="T109" id="Seg_4086" s="T108">очень</ta>
            <ta e="T110" id="Seg_4087" s="T109">стадо-PROPR.[NOM]</ta>
            <ta e="T112" id="Seg_4088" s="T111">много</ta>
            <ta e="T113" id="Seg_4089" s="T112">очень</ta>
            <ta e="T114" id="Seg_4090" s="T113">стадо-PROPR.[NOM]</ta>
            <ta e="T115" id="Seg_4091" s="T114">быть-PST1-1PL</ta>
            <ta e="T116" id="Seg_4092" s="T115">много</ta>
            <ta e="T117" id="Seg_4093" s="T116">семья-PL.[NOM]</ta>
            <ta e="T118" id="Seg_4094" s="T117">быть-PST1-3PL</ta>
            <ta e="T119" id="Seg_4095" s="T118">кочевник.[NOM]</ta>
            <ta e="T120" id="Seg_4096" s="T119">человек.[NOM]</ta>
            <ta e="T121" id="Seg_4097" s="T120">шесть-DISTR</ta>
            <ta e="T122" id="Seg_4098" s="T121">пять-DISTR</ta>
            <ta e="T123" id="Seg_4099" s="T122">балок-PL.[NOM]</ta>
            <ta e="T124" id="Seg_4100" s="T123">чум-PL.[NOM]</ta>
            <ta e="T125" id="Seg_4101" s="T124">идти-HAB-1PL</ta>
            <ta e="T127" id="Seg_4102" s="T125">мать.[NOM]</ta>
            <ta e="T132" id="Seg_4103" s="T131">потом</ta>
            <ta e="T133" id="Seg_4104" s="T132">1PL.[NOM]</ta>
            <ta e="T134" id="Seg_4105" s="T133">еще</ta>
            <ta e="T135" id="Seg_4106" s="T134">на.том.берегу</ta>
            <ta e="T136" id="Seg_4107" s="T135">лежать-PST2-1PL</ta>
            <ta e="T137" id="Seg_4108" s="T136">высокий</ta>
            <ta e="T138" id="Seg_4109" s="T137">гора-DAT/LOC</ta>
            <ta e="T139" id="Seg_4110" s="T138">там</ta>
            <ta e="T140" id="Seg_4111" s="T139">шесть-DISTR</ta>
            <ta e="T141" id="Seg_4112" s="T140">пять-DISTR</ta>
            <ta e="T142" id="Seg_4113" s="T141">шест.[NOM]</ta>
            <ta e="T144" id="Seg_4114" s="T142">чум.[NOM]</ta>
            <ta e="T151" id="Seg_4115" s="T150">там</ta>
            <ta e="T152" id="Seg_4116" s="T151">кто.[NOM]</ta>
            <ta e="T153" id="Seg_4117" s="T152">два</ta>
            <ta e="T154" id="Seg_4118" s="T153">два</ta>
            <ta e="T155" id="Seg_4119" s="T154">два</ta>
            <ta e="T156" id="Seg_4120" s="T155">ребенок.[NOM]</ta>
            <ta e="T157" id="Seg_4121" s="T156">два</ta>
            <ta e="T158" id="Seg_4122" s="T157">только</ta>
            <ta e="T159" id="Seg_4123" s="T158">ребенок-PROPR.[NOM]</ta>
            <ta e="T160" id="Seg_4124" s="T159">быть-PST1-3SG</ta>
            <ta e="T161" id="Seg_4125" s="T160">там</ta>
            <ta e="T162" id="Seg_4126" s="T161">на.том.берегу</ta>
            <ta e="T163" id="Seg_4127" s="T162">MOD</ta>
            <ta e="T164" id="Seg_4128" s="T163">кто.[NOM]</ta>
            <ta e="T165" id="Seg_4129" s="T164">высокий</ta>
            <ta e="T166" id="Seg_4130" s="T165">гора-DAT/LOC</ta>
            <ta e="T167" id="Seg_4131" s="T166">шесть-DISTR</ta>
            <ta e="T168" id="Seg_4132" s="T167">пять-DISTR</ta>
            <ta e="T169" id="Seg_4133" s="T168">шест.[NOM]</ta>
            <ta e="T170" id="Seg_4134" s="T169">чум.[NOM]</ta>
            <ta e="T171" id="Seg_4135" s="T170">стоять-PTCP.HAB</ta>
            <ta e="T172" id="Seg_4136" s="T171">быть-PST1-3PL</ta>
            <ta e="T173" id="Seg_4137" s="T172">там</ta>
            <ta e="T174" id="Seg_4138" s="T173">MOD</ta>
            <ta e="T175" id="Seg_4139" s="T174">рыбак-PL.[NOM]</ta>
            <ta e="T176" id="Seg_4140" s="T175">летом</ta>
            <ta e="T177" id="Seg_4141" s="T176">MOD</ta>
            <ta e="T178" id="Seg_4142" s="T177">рыбак-VBZ-PTCP.HAB</ta>
            <ta e="T179" id="Seg_4143" s="T178">быть-PST1-3PL</ta>
            <ta e="T180" id="Seg_4144" s="T179">вот</ta>
            <ta e="T181" id="Seg_4145" s="T180">потом</ta>
            <ta e="T182" id="Seg_4146" s="T181">EMPH</ta>
            <ta e="T183" id="Seg_4147" s="T182">1SG.[NOM]</ta>
            <ta e="T185" id="Seg_4148" s="T184">Сындасско-DAT/LOC</ta>
            <ta e="T186" id="Seg_4149" s="T185">входить-PST2-1PL</ta>
            <ta e="T187" id="Seg_4150" s="T186">отец-1SG.[NOM]</ta>
            <ta e="T188" id="Seg_4151" s="T187">умирать-CVB.SEQ</ta>
            <ta e="T189" id="Seg_4152" s="T188">умирать-CVB.SEQ</ta>
            <ta e="T190" id="Seg_4153" s="T189">там</ta>
            <ta e="T191" id="Seg_4154" s="T190">EMPH</ta>
            <ta e="T192" id="Seg_4155" s="T191">1PL.[NOM]</ta>
            <ta e="T193" id="Seg_4156" s="T192">MOD</ta>
            <ta e="T194" id="Seg_4157" s="T193">учиться-PST2-1PL</ta>
            <ta e="T195" id="Seg_4158" s="T194">Новорыбное-DAT/LOC</ta>
            <ta e="T196" id="Seg_4159" s="T195">Косистый-DAT/LOC</ta>
            <ta e="T197" id="Seg_4160" s="T196">Хатанга-DAT/LOC</ta>
            <ta e="T198" id="Seg_4161" s="T197">Хатанга-DAT/LOC</ta>
            <ta e="T199" id="Seg_4162" s="T198">EMPH</ta>
            <ta e="T200" id="Seg_4163" s="T199">восемь</ta>
            <ta e="T201" id="Seg_4164" s="T200">класс-ACC</ta>
            <ta e="T202" id="Seg_4165" s="T201">кончать-CVB.SEQ</ta>
            <ta e="T203" id="Seg_4166" s="T202">после</ta>
            <ta e="T204" id="Seg_4167" s="T203">MOD</ta>
            <ta e="T205" id="Seg_4168" s="T204">1PL-ACC</ta>
            <ta e="T206" id="Seg_4169" s="T205">кто-DAT/LOC</ta>
            <ta e="T207" id="Seg_4170" s="T206">послать-PST2-3PL</ta>
            <ta e="T208" id="Seg_4171" s="T207">Красноярск-DAT/LOC</ta>
            <ta e="T209" id="Seg_4172" s="T208">там</ta>
            <ta e="T210" id="Seg_4173" s="T209">MOD</ta>
            <ta e="T211" id="Seg_4174" s="T210">двадцать</ta>
            <ta e="T212" id="Seg_4175" s="T211">семь-ORD</ta>
            <ta e="T213" id="Seg_4176" s="T212">школа-DAT/LOC</ta>
            <ta e="T214" id="Seg_4177" s="T213">учиться-PST2-1PL</ta>
            <ta e="T217" id="Seg_4178" s="T216">класс-ACC</ta>
            <ta e="T218" id="Seg_4179" s="T217">потом</ta>
            <ta e="T219" id="Seg_4180" s="T218">MOD</ta>
            <ta e="T220" id="Seg_4181" s="T219">ээ</ta>
            <ta e="T222" id="Seg_4182" s="T221">восемьдесять</ta>
            <ta e="T223" id="Seg_4183" s="T222">год-PROPR-DAT/LOC</ta>
            <ta e="T225" id="Seg_4184" s="T224">1PL.[NOM]</ta>
            <ta e="T226" id="Seg_4185" s="T225">MOD</ta>
            <ta e="T228" id="Seg_4186" s="T227">кто.[NOM]</ta>
            <ta e="T229" id="Seg_4187" s="T228">кто-DAT/LOC</ta>
            <ta e="T230" id="Seg_4188" s="T229">поступать-VBZ-PST2-EP-1SG</ta>
            <ta e="T235" id="Seg_4189" s="T234">там</ta>
            <ta e="T236" id="Seg_4190" s="T235">EMPH</ta>
            <ta e="T237" id="Seg_4191" s="T236">шесть</ta>
            <ta e="T238" id="Seg_4192" s="T237">год-ACC</ta>
            <ta e="T239" id="Seg_4193" s="T238">учиться-PST2-EP-1SG</ta>
            <ta e="T240" id="Seg_4194" s="T239">потом</ta>
            <ta e="T241" id="Seg_4195" s="T240">EMPH</ta>
            <ta e="T242" id="Seg_4196" s="T241">семь-ORD</ta>
            <ta e="T243" id="Seg_4197" s="T242">год-EP-1SG.[NOM]</ta>
            <ta e="T244" id="Seg_4198" s="T243">интернатура.[NOM]</ta>
            <ta e="T247" id="Seg_4199" s="T246">интернатура.[NOM]</ta>
            <ta e="T248" id="Seg_4200" s="T247">один</ta>
            <ta e="T249" id="Seg_4201" s="T248">год-ACC</ta>
            <ta e="T250" id="Seg_4202" s="T249">вот</ta>
            <ta e="T251" id="Seg_4203" s="T250">тот -ACC</ta>
            <ta e="T252" id="Seg_4204" s="T251">кончать-CVB.SEQ</ta>
            <ta e="T253" id="Seg_4205" s="T252">после</ta>
            <ta e="T254" id="Seg_4206" s="T253">EMPH</ta>
            <ta e="T255" id="Seg_4207" s="T254">1SG-ACC</ta>
            <ta e="T256" id="Seg_4208" s="T255">EMPH</ta>
            <ta e="T257" id="Seg_4209" s="T256">Таймыр-DAT/LOC</ta>
            <ta e="T258" id="Seg_4210" s="T257">послать-PST2-3PL</ta>
            <ta e="T259" id="Seg_4211" s="T258">родиться-PTCP.PST</ta>
            <ta e="T260" id="Seg_4212" s="T259">вот</ta>
            <ta e="T261" id="Seg_4213" s="T260">место-1SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_4214" s="T261">вот</ta>
            <ta e="T263" id="Seg_4215" s="T262">работать-PTCP.FUT-1SG-ACC</ta>
            <ta e="T264" id="Seg_4216" s="T263">Дудинка-DAT/LOC</ta>
            <ta e="T265" id="Seg_4217" s="T264">приходить-CVB.SEQ</ta>
            <ta e="T266" id="Seg_4218" s="T265">после</ta>
            <ta e="T267" id="Seg_4219" s="T266">EMPH</ta>
            <ta e="T268" id="Seg_4220" s="T267">1SG-ACC</ta>
            <ta e="T269" id="Seg_4221" s="T268">EMPH</ta>
            <ta e="T270" id="Seg_4222" s="T269">Потапово-DAT/LOC</ta>
            <ta e="T271" id="Seg_4223" s="T270">послать-PST2-3PL</ta>
            <ta e="T272" id="Seg_4224" s="T271">там</ta>
            <ta e="T273" id="Seg_4225" s="T272">кто.[NOM]</ta>
            <ta e="T274" id="Seg_4226" s="T273">есть</ta>
            <ta e="T275" id="Seg_4227" s="T274">быть-PST1-3SG</ta>
            <ta e="T278" id="Seg_4228" s="T277">нет</ta>
            <ta e="T282" id="Seg_4229" s="T281">быть-PST1-3SG</ta>
            <ta e="T284" id="Seg_4230" s="T283">вот</ta>
            <ta e="T285" id="Seg_4231" s="T284">там</ta>
            <ta e="T286" id="Seg_4232" s="T285">MOD</ta>
            <ta e="T287" id="Seg_4233" s="T286">три</ta>
            <ta e="T288" id="Seg_4234" s="T287">год-ACC</ta>
            <ta e="T289" id="Seg_4235" s="T288">работать-PST2-EP-1SG</ta>
            <ta e="T290" id="Seg_4236" s="T289">вот</ta>
            <ta e="T291" id="Seg_4237" s="T290">восемьдесять</ta>
            <ta e="T292" id="Seg_4238" s="T291">четыре-ORD-PROPR</ta>
            <ta e="T293" id="Seg_4239" s="T292">восемьдесять</ta>
            <ta e="T294" id="Seg_4240" s="T293">четыре-ORD-PROPR</ta>
            <ta e="T295" id="Seg_4241" s="T294">год-DAT/LOC</ta>
            <ta e="T296" id="Seg_4242" s="T295">сюда</ta>
            <ta e="T297" id="Seg_4243" s="T296">приходить-PST2-EP-1SG</ta>
            <ta e="T298" id="Seg_4244" s="T297">кочевать-CVB.SEQ</ta>
            <ta e="T300" id="Seg_4245" s="T299">послать-PST2-3PL</ta>
            <ta e="T301" id="Seg_4246" s="T300">и</ta>
            <ta e="T302" id="Seg_4247" s="T301">тот.EMPH.[NOM]</ta>
            <ta e="T303" id="Seg_4248" s="T302">год-ABL</ta>
            <ta e="T304" id="Seg_4249" s="T303">этот</ta>
            <ta e="T305" id="Seg_4250" s="T304">теперь-DAT/LOC</ta>
            <ta e="T306" id="Seg_4251" s="T305">пока</ta>
            <ta e="T307" id="Seg_4252" s="T306">MOD</ta>
            <ta e="T308" id="Seg_4253" s="T307">кто-DAT/LOC</ta>
            <ta e="T309" id="Seg_4254" s="T308">работать-PRS-1SG</ta>
            <ta e="T318" id="Seg_4255" s="T317">потом</ta>
            <ta e="T319" id="Seg_4256" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_4257" s="T319">восемьдесять</ta>
            <ta e="T321" id="Seg_4258" s="T320">девять-ORD-PROPR</ta>
            <ta e="T322" id="Seg_4259" s="T321">и</ta>
            <ta e="T323" id="Seg_4260" s="T322">девять-ORD-PROPR</ta>
            <ta e="T324" id="Seg_4261" s="T323">год-DAT/LOC</ta>
            <ta e="T325" id="Seg_4262" s="T324">MOD</ta>
            <ta e="T326" id="Seg_4263" s="T325">кто.[NOM]</ta>
            <ta e="T327" id="Seg_4264" s="T326">быть-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_4265" s="T327">землетрясение.[NOM]</ta>
            <ta e="T329" id="Seg_4266" s="T328">быть-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_4267" s="T329">Армения-DAT/LOC</ta>
            <ta e="T331" id="Seg_4268" s="T330">там</ta>
            <ta e="T332" id="Seg_4269" s="T331">сам-1SG.[NOM]</ta>
            <ta e="T333" id="Seg_4270" s="T332">желание-1SG-INSTR</ta>
            <ta e="T336" id="Seg_4271" s="T335">говорить-PTCP.PST-DAT/LOC</ta>
            <ta e="T337" id="Seg_4272" s="T336">вот</ta>
            <ta e="T338" id="Seg_4273" s="T337">там</ta>
            <ta e="T339" id="Seg_4274" s="T338">работать-PST2-EP-1SG</ta>
            <ta e="T340" id="Seg_4275" s="T339">два</ta>
            <ta e="T353" id="Seg_4276" s="T352">восемьдесять</ta>
            <ta e="T358" id="Seg_4277" s="T357">этот</ta>
            <ta e="T359" id="Seg_4278" s="T358">MOD</ta>
            <ta e="T360" id="Seg_4279" s="T359">кто.[NOM]</ta>
            <ta e="T361" id="Seg_4280" s="T360">давать-PST2-3PL</ta>
            <ta e="T362" id="Seg_4281" s="T361">1SG-DAT/LOC</ta>
            <ta e="T374" id="Seg_4282" s="T373">семья-PL-1SG-ACC</ta>
            <ta e="T375" id="Seg_4283" s="T374">с</ta>
            <ta e="T376" id="Seg_4284" s="T375">мать-1SG-ACC</ta>
            <ta e="T377" id="Seg_4285" s="T376">с</ta>
            <ta e="T378" id="Seg_4286" s="T377">жить-PRS-1SG</ta>
            <ta e="T379" id="Seg_4287" s="T378">теперь</ta>
            <ta e="T381" id="Seg_4288" s="T380">племянница-1SG-ACC</ta>
            <ta e="T382" id="Seg_4289" s="T381">с</ta>
            <ta e="T383" id="Seg_4290" s="T382">тот -3SG-1SG.[NOM]</ta>
            <ta e="T384" id="Seg_4291" s="T383">MOD</ta>
            <ta e="T385" id="Seg_4292" s="T384">десять-ORD</ta>
            <ta e="T386" id="Seg_4293" s="T385">класс-DAT/LOC</ta>
            <ta e="T387" id="Seg_4294" s="T386">учиться-PRS.[3SG]</ta>
            <ta e="T388" id="Seg_4295" s="T387">сам-1SG.[NOM]</ta>
            <ta e="T389" id="Seg_4296" s="T388">всегда</ta>
            <ta e="T390" id="Seg_4297" s="T389">работать-PRS-1SG</ta>
            <ta e="T391" id="Seg_4298" s="T390">а</ta>
            <ta e="T392" id="Seg_4299" s="T391">1PL-ACC</ta>
            <ta e="T393" id="Seg_4300" s="T392">EMPH</ta>
            <ta e="T394" id="Seg_4301" s="T393">пять-ORD</ta>
            <ta e="T395" id="Seg_4302" s="T394">класс-DAT/LOC</ta>
            <ta e="T396" id="Seg_4303" s="T395">EMPH</ta>
            <ta e="T397" id="Seg_4304" s="T396">пять-ORD</ta>
            <ta e="T398" id="Seg_4305" s="T397">класс-DAT/LOC</ta>
            <ta e="T399" id="Seg_4306" s="T398">учиться-PTCP.PRS</ta>
            <ta e="T400" id="Seg_4307" s="T399">быть-TEMP-1PL</ta>
            <ta e="T401" id="Seg_4308" s="T400">EMPH</ta>
            <ta e="T402" id="Seg_4309" s="T401">Хатанга-DAT/LOC</ta>
            <ta e="T403" id="Seg_4310" s="T402">MOD</ta>
            <ta e="T404" id="Seg_4311" s="T403">дом.культуры-DAT/LOC</ta>
            <ta e="T405" id="Seg_4312" s="T404">MOD</ta>
            <ta e="T406" id="Seg_4313" s="T405">1PL-ACC</ta>
            <ta e="T407" id="Seg_4314" s="T406">кто.[NOM]</ta>
            <ta e="T416" id="Seg_4315" s="T415">тот </ta>
            <ta e="T417" id="Seg_4316" s="T416">тот -ACC</ta>
            <ta e="T419" id="Seg_4317" s="T418">1PL-ACC</ta>
            <ta e="T420" id="Seg_4318" s="T419">кто-DAT/LOC</ta>
            <ta e="T421" id="Seg_4319" s="T420">учить-PST2-3SG</ta>
            <ta e="T422" id="Seg_4320" s="T421">барган-DAT/LOC</ta>
            <ta e="T423" id="Seg_4321" s="T422">играть-CVB.SIM</ta>
            <ta e="T424" id="Seg_4322" s="T423">теперь</ta>
            <ta e="T425" id="Seg_4323" s="T424">MOD</ta>
            <ta e="T429" id="Seg_4324" s="T428">постареть-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T430" id="Seg_4325" s="T429">пока</ta>
            <ta e="T431" id="Seg_4326" s="T430">всегда</ta>
            <ta e="T432" id="Seg_4327" s="T431">играть-PRS-1SG</ta>
            <ta e="T433" id="Seg_4328" s="T432">теперь</ta>
            <ta e="T434" id="Seg_4329" s="T433">вот</ta>
            <ta e="T435" id="Seg_4330" s="T434">Якутск-2SG-ABL</ta>
            <ta e="T436" id="Seg_4331" s="T435">кто-3PL.[NOM]</ta>
            <ta e="T437" id="Seg_4332" s="T436">приходить-PST2.[3SG]</ta>
            <ta e="T438" id="Seg_4333" s="T437">артист-PL.[NOM]</ta>
            <ta e="T439" id="Seg_4334" s="T438">приходить-PTCP.PST</ta>
            <ta e="T440" id="Seg_4335" s="T439">быть-PST1-3PL</ta>
            <ta e="T441" id="Seg_4336" s="T440">тот -PL.[NOM]</ta>
            <ta e="T442" id="Seg_4337" s="T441">1PL-DAT/LOC</ta>
            <ta e="T443" id="Seg_4338" s="T442">барган.[NOM]</ta>
            <ta e="T444" id="Seg_4339" s="T443">продавать-PST2-3PL</ta>
            <ta e="T446" id="Seg_4340" s="T445">этот</ta>
            <ta e="T447" id="Seg_4341" s="T446">большой</ta>
            <ta e="T448" id="Seg_4342" s="T447">деньги.[NOM]</ta>
            <ta e="T449" id="Seg_4343" s="T448">быть-PST1-3SG</ta>
            <ta e="T450" id="Seg_4344" s="T449">EMPH</ta>
            <ta e="T451" id="Seg_4345" s="T450">восемьдесять</ta>
            <ta e="T452" id="Seg_4346" s="T451">восемьдесять</ta>
            <ta e="T453" id="Seg_4347" s="T452">рубль.[NOM]</ta>
            <ta e="T454" id="Seg_4348" s="T453">теперь</ta>
            <ta e="T455" id="Seg_4349" s="T454">EMPH</ta>
            <ta e="T456" id="Seg_4350" s="T455">всегда</ta>
            <ta e="T457" id="Seg_4351" s="T456">играть-PRS-1SG</ta>
            <ta e="T458" id="Seg_4352" s="T457">желание-1SG.[NOM]</ta>
            <ta e="T459" id="Seg_4353" s="T458">приходить-TEMP-3SG</ta>
            <ta e="T460" id="Seg_4354" s="T459">теперь</ta>
            <ta e="T461" id="Seg_4355" s="T460">опять</ta>
            <ta e="T462" id="Seg_4356" s="T461">2PL-DAT/LOC</ta>
            <ta e="T463" id="Seg_4357" s="T462">1SG.[NOM]</ta>
            <ta e="T464" id="Seg_4358" s="T463">играть-FUT-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KiLS">
            <ta e="T2" id="Seg_4359" s="T1">pers.[pro:case]</ta>
            <ta e="T3" id="Seg_4360" s="T2">n-n:(ins)-n:(poss)</ta>
            <ta e="T4" id="Seg_4361" s="T3">propr</ta>
            <ta e="T5" id="Seg_4362" s="T4">propr</ta>
            <ta e="T6" id="Seg_4363" s="T5">propr.[n:case]</ta>
            <ta e="T7" id="Seg_4364" s="T6">pers.[pro:case]</ta>
            <ta e="T8" id="Seg_4365" s="T7">pers.[pro:case]</ta>
            <ta e="T9" id="Seg_4366" s="T8">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T10" id="Seg_4367" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_4368" s="T10">propr-n:case</ta>
            <ta e="T12" id="Seg_4369" s="T11">propr-n:case</ta>
            <ta e="T13" id="Seg_4370" s="T12">n-n:(num)-n:(ins)-n:(poss)</ta>
            <ta e="T14" id="Seg_4371" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_4372" s="T14">n-n&gt;adj.[n:case]</ta>
            <ta e="T16" id="Seg_4373" s="T15">v-v:tense-v:poss.pn</ta>
            <ta e="T17" id="Seg_4374" s="T16">que-pro:(num).[pro:case]</ta>
            <ta e="T18" id="Seg_4375" s="T17">que.[pro:case]</ta>
            <ta e="T19" id="Seg_4376" s="T18">n-n&gt;n-n:(num).[n:case]</ta>
            <ta e="T20" id="Seg_4377" s="T19">propr-n:case</ta>
            <ta e="T22" id="Seg_4378" s="T20">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T25" id="Seg_4379" s="T24">n-n&gt;n-n:(num).[n:case]</ta>
            <ta e="T27" id="Seg_4380" s="T25">v-v:tense-v:poss.pn</ta>
            <ta e="T31" id="Seg_4381" s="T29">n-n:(poss).[n:case]</ta>
            <ta e="T32" id="Seg_4382" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_4383" s="T32">conj</ta>
            <ta e="T34" id="Seg_4384" s="T33">n-n:(poss).[n:case]</ta>
            <ta e="T35" id="Seg_4385" s="T34">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T36" id="Seg_4386" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_4387" s="T36">pers.[pro:case]</ta>
            <ta e="T38" id="Seg_4388" s="T37">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T39" id="Seg_4389" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_4390" s="T39">pers.[pro:case]</ta>
            <ta e="T41" id="Seg_4391" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_4392" s="T41">que.[pro:case]</ta>
            <ta e="T43" id="Seg_4393" s="T42">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T44" id="Seg_4394" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_4395" s="T44">que.[pro:case]</ta>
            <ta e="T46" id="Seg_4396" s="T45">n.[n:case]</ta>
            <ta e="T47" id="Seg_4397" s="T46">v-v:tense-v:pred.pn</ta>
            <ta e="T48" id="Seg_4398" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_4399" s="T48">adv</ta>
            <ta e="T50" id="Seg_4400" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_4401" s="T50">n.[n:case]</ta>
            <ta e="T52" id="Seg_4402" s="T51">que.[pro:case]</ta>
            <ta e="T53" id="Seg_4403" s="T52">ptcl</ta>
            <ta e="T55" id="Seg_4404" s="T53">adv</ta>
            <ta e="T59" id="Seg_4405" s="T57">n.[n:case]</ta>
            <ta e="T62" id="Seg_4406" s="T60">n.[n:case]</ta>
            <ta e="T63" id="Seg_4407" s="T62">dempro-pro:case</ta>
            <ta e="T64" id="Seg_4408" s="T63">dempro-pro:case</ta>
            <ta e="T65" id="Seg_4409" s="T64">dempro-pro:case</ta>
            <ta e="T66" id="Seg_4410" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_4411" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_4412" s="T67">v-v:cvb</ta>
            <ta e="T69" id="Seg_4413" s="T68">post</ta>
            <ta e="T70" id="Seg_4414" s="T69">dempro</ta>
            <ta e="T71" id="Seg_4415" s="T70">dempro</ta>
            <ta e="T72" id="Seg_4416" s="T71">n-n:(poss).[n:case]</ta>
            <ta e="T73" id="Seg_4417" s="T72">n</ta>
            <ta e="T74" id="Seg_4418" s="T73">dempro</ta>
            <ta e="T75" id="Seg_4419" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_4420" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_4421" s="T76">v-v:cvb</ta>
            <ta e="T78" id="Seg_4422" s="T77">v-v:tense-v:pred.pn</ta>
            <ta e="T79" id="Seg_4423" s="T78">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T80" id="Seg_4424" s="T79">dempro</ta>
            <ta e="T81" id="Seg_4425" s="T80">n-n:(poss).[n:case]</ta>
            <ta e="T82" id="Seg_4426" s="T81">dempro</ta>
            <ta e="T83" id="Seg_4427" s="T82">n-n:(poss).[n:case]</ta>
            <ta e="T84" id="Seg_4428" s="T83">dempro</ta>
            <ta e="T85" id="Seg_4429" s="T84">adv</ta>
            <ta e="T86" id="Seg_4430" s="T85">conj</ta>
            <ta e="T87" id="Seg_4431" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_4432" s="T87">adv</ta>
            <ta e="T89" id="Seg_4433" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_4434" s="T89">pers.[pro:case]</ta>
            <ta e="T91" id="Seg_4435" s="T90">cardnum</ta>
            <ta e="T92" id="Seg_4436" s="T91">cardnum</ta>
            <ta e="T93" id="Seg_4437" s="T92">cardnum</ta>
            <ta e="T94" id="Seg_4438" s="T93">n-n&gt;adj.[n:case]</ta>
            <ta e="T95" id="Seg_4439" s="T94">v-v:mood-v:temp.pn</ta>
            <ta e="T96" id="Seg_4440" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_4441" s="T96">que.[pro:case]</ta>
            <ta e="T98" id="Seg_4442" s="T97">propr-n:case</ta>
            <ta e="T99" id="Seg_4443" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_4444" s="T99">ptcl-ptcl</ta>
            <ta e="T101" id="Seg_4445" s="T100">pers.[pro:case]</ta>
            <ta e="T102" id="Seg_4446" s="T101">adv</ta>
            <ta e="T103" id="Seg_4447" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_4448" s="T103">v-v:ptcp</ta>
            <ta e="T105" id="Seg_4449" s="T104">v-v:tense-v:pred.pn</ta>
            <ta e="T106" id="Seg_4450" s="T105">n.[n:case]</ta>
            <ta e="T107" id="Seg_4451" s="T106">dempro</ta>
            <ta e="T108" id="Seg_4452" s="T107">quant</ta>
            <ta e="T109" id="Seg_4453" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_4454" s="T109">n-n&gt;adj.[n:case]</ta>
            <ta e="T112" id="Seg_4455" s="T111">quant</ta>
            <ta e="T113" id="Seg_4456" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_4457" s="T113">n-n&gt;adj.[n:case]</ta>
            <ta e="T115" id="Seg_4458" s="T114">v-v:tense-v:poss.pn</ta>
            <ta e="T116" id="Seg_4459" s="T115">quant</ta>
            <ta e="T117" id="Seg_4460" s="T116">n-n:(num).[n:case]</ta>
            <ta e="T118" id="Seg_4461" s="T117">v-v:tense-v:poss.pn</ta>
            <ta e="T119" id="Seg_4462" s="T118">n.[n:case]</ta>
            <ta e="T120" id="Seg_4463" s="T119">n.[n:case]</ta>
            <ta e="T121" id="Seg_4464" s="T120">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T122" id="Seg_4465" s="T121">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T123" id="Seg_4466" s="T122">n-n:(num).[n:case]</ta>
            <ta e="T124" id="Seg_4467" s="T123">n-n:(num).[n:case]</ta>
            <ta e="T125" id="Seg_4468" s="T124">v-v:mood-v:pred.pn</ta>
            <ta e="T127" id="Seg_4469" s="T125">n.[n:case]</ta>
            <ta e="T132" id="Seg_4470" s="T131">adv</ta>
            <ta e="T133" id="Seg_4471" s="T132">pers.[pro:case]</ta>
            <ta e="T134" id="Seg_4472" s="T133">adv</ta>
            <ta e="T135" id="Seg_4473" s="T134">adv</ta>
            <ta e="T136" id="Seg_4474" s="T135">v-v:tense-v:pred.pn</ta>
            <ta e="T137" id="Seg_4475" s="T136">adj</ta>
            <ta e="T138" id="Seg_4476" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_4477" s="T138">adv</ta>
            <ta e="T140" id="Seg_4478" s="T139">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T141" id="Seg_4479" s="T140">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T142" id="Seg_4480" s="T141">n.[n:case]</ta>
            <ta e="T144" id="Seg_4481" s="T142">n.[n:case]</ta>
            <ta e="T151" id="Seg_4482" s="T150">adv</ta>
            <ta e="T152" id="Seg_4483" s="T151">que.[pro:case]</ta>
            <ta e="T153" id="Seg_4484" s="T152">cardnum</ta>
            <ta e="T154" id="Seg_4485" s="T153">cardnum</ta>
            <ta e="T155" id="Seg_4486" s="T154">cardnum</ta>
            <ta e="T156" id="Seg_4487" s="T155">n.[n:case]</ta>
            <ta e="T157" id="Seg_4488" s="T156">cardnum</ta>
            <ta e="T158" id="Seg_4489" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_4490" s="T158">n-n&gt;adj.[n:case]</ta>
            <ta e="T160" id="Seg_4491" s="T159">v-v:tense-v:poss.pn</ta>
            <ta e="T161" id="Seg_4492" s="T160">adv</ta>
            <ta e="T162" id="Seg_4493" s="T161">adv</ta>
            <ta e="T163" id="Seg_4494" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_4495" s="T163">que.[pro:case]</ta>
            <ta e="T165" id="Seg_4496" s="T164">adj</ta>
            <ta e="T166" id="Seg_4497" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_4498" s="T166">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T168" id="Seg_4499" s="T167">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T169" id="Seg_4500" s="T168">n.[n:case]</ta>
            <ta e="T170" id="Seg_4501" s="T169">n.[n:case]</ta>
            <ta e="T171" id="Seg_4502" s="T170">v-v:ptcp</ta>
            <ta e="T172" id="Seg_4503" s="T171">v-v:tense-v:poss.pn</ta>
            <ta e="T173" id="Seg_4504" s="T172">adv</ta>
            <ta e="T174" id="Seg_4505" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_4506" s="T174">n-n:(num).[n:case]</ta>
            <ta e="T176" id="Seg_4507" s="T175">adv</ta>
            <ta e="T177" id="Seg_4508" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_4509" s="T177">n-n&gt;v-v:ptcp</ta>
            <ta e="T179" id="Seg_4510" s="T178">v-v:tense-v:poss.pn</ta>
            <ta e="T180" id="Seg_4511" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_4512" s="T180">adv</ta>
            <ta e="T182" id="Seg_4513" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_4514" s="T182">pers.[pro:case]</ta>
            <ta e="T185" id="Seg_4515" s="T184">propr-n:case</ta>
            <ta e="T186" id="Seg_4516" s="T185">v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_4517" s="T186">n-n:(poss).[n:case]</ta>
            <ta e="T188" id="Seg_4518" s="T187">v-v:cvb</ta>
            <ta e="T189" id="Seg_4519" s="T188">v-v:cvb</ta>
            <ta e="T190" id="Seg_4520" s="T189">adv</ta>
            <ta e="T191" id="Seg_4521" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_4522" s="T191">pers.[pro:case]</ta>
            <ta e="T193" id="Seg_4523" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_4524" s="T193">v-v:tense-v:pred.pn</ta>
            <ta e="T195" id="Seg_4525" s="T194">propr-n:case</ta>
            <ta e="T196" id="Seg_4526" s="T195">propr-n:case</ta>
            <ta e="T197" id="Seg_4527" s="T196">propr-n:case</ta>
            <ta e="T198" id="Seg_4528" s="T197">propr-n:case</ta>
            <ta e="T199" id="Seg_4529" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_4530" s="T199">cardnum</ta>
            <ta e="T201" id="Seg_4531" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_4532" s="T201">v-v:cvb</ta>
            <ta e="T203" id="Seg_4533" s="T202">post</ta>
            <ta e="T204" id="Seg_4534" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_4535" s="T204">pers-pro:case</ta>
            <ta e="T206" id="Seg_4536" s="T205">que-pro:case</ta>
            <ta e="T207" id="Seg_4537" s="T206">v-v:tense-v:poss.pn</ta>
            <ta e="T208" id="Seg_4538" s="T207">propr-n:case</ta>
            <ta e="T209" id="Seg_4539" s="T208">adv</ta>
            <ta e="T210" id="Seg_4540" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_4541" s="T210">cardnum</ta>
            <ta e="T212" id="Seg_4542" s="T211">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T213" id="Seg_4543" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_4544" s="T213">v-v:tense-v:pred.pn</ta>
            <ta e="T217" id="Seg_4545" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_4546" s="T217">adv</ta>
            <ta e="T219" id="Seg_4547" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_4548" s="T219">interj</ta>
            <ta e="T222" id="Seg_4549" s="T221">cardnum</ta>
            <ta e="T223" id="Seg_4550" s="T222">n-n&gt;adj-n:case</ta>
            <ta e="T225" id="Seg_4551" s="T224">pers.[pro:case]</ta>
            <ta e="T226" id="Seg_4552" s="T225">ptcl</ta>
            <ta e="T228" id="Seg_4553" s="T227">que.[pro:case]</ta>
            <ta e="T229" id="Seg_4554" s="T228">que-pro:case</ta>
            <ta e="T230" id="Seg_4555" s="T229">v-v&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T235" id="Seg_4556" s="T234">adv</ta>
            <ta e="T236" id="Seg_4557" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_4558" s="T236">cardnum</ta>
            <ta e="T238" id="Seg_4559" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_4560" s="T238">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T240" id="Seg_4561" s="T239">adv</ta>
            <ta e="T241" id="Seg_4562" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_4563" s="T241">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T243" id="Seg_4564" s="T242">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T244" id="Seg_4565" s="T243">n.[n:case]</ta>
            <ta e="T247" id="Seg_4566" s="T246">n.[n:case]</ta>
            <ta e="T248" id="Seg_4567" s="T247">cardnum</ta>
            <ta e="T249" id="Seg_4568" s="T248">n-n:case</ta>
            <ta e="T250" id="Seg_4569" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_4570" s="T250">dempro-pro:case</ta>
            <ta e="T252" id="Seg_4571" s="T251">v-v:cvb</ta>
            <ta e="T253" id="Seg_4572" s="T252">post</ta>
            <ta e="T254" id="Seg_4573" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_4574" s="T254">pers-pro:case</ta>
            <ta e="T256" id="Seg_4575" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_4576" s="T256">propr-n:case</ta>
            <ta e="T258" id="Seg_4577" s="T257">v-v:tense-v:poss.pn</ta>
            <ta e="T259" id="Seg_4578" s="T258">v-v:ptcp</ta>
            <ta e="T260" id="Seg_4579" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_4580" s="T260">n-n:poss-n:case</ta>
            <ta e="T262" id="Seg_4581" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_4582" s="T262">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T264" id="Seg_4583" s="T263">propr-n:case</ta>
            <ta e="T265" id="Seg_4584" s="T264">v-v:cvb</ta>
            <ta e="T266" id="Seg_4585" s="T265">post</ta>
            <ta e="T267" id="Seg_4586" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_4587" s="T267">pers-pro:case</ta>
            <ta e="T269" id="Seg_4588" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_4589" s="T269">propr-n:case</ta>
            <ta e="T271" id="Seg_4590" s="T270">v-v:tense-v:poss.pn</ta>
            <ta e="T272" id="Seg_4591" s="T271">adv</ta>
            <ta e="T273" id="Seg_4592" s="T272">que.[pro:case]</ta>
            <ta e="T274" id="Seg_4593" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_4594" s="T274">v-v:tense-v:poss.pn</ta>
            <ta e="T278" id="Seg_4595" s="T277">ptcl</ta>
            <ta e="T282" id="Seg_4596" s="T281">v-v:tense-v:poss.pn</ta>
            <ta e="T284" id="Seg_4597" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_4598" s="T284">adv</ta>
            <ta e="T286" id="Seg_4599" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_4600" s="T286">cardnum</ta>
            <ta e="T288" id="Seg_4601" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_4602" s="T288">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T290" id="Seg_4603" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_4604" s="T290">cardnum</ta>
            <ta e="T292" id="Seg_4605" s="T291">cardnum-cardnum&gt;ordnum-ordnum&gt;adj</ta>
            <ta e="T293" id="Seg_4606" s="T292">cardnum</ta>
            <ta e="T294" id="Seg_4607" s="T293">cardnum-cardnum&gt;ordnum-ordnum&gt;adj</ta>
            <ta e="T295" id="Seg_4608" s="T294">n-n:case</ta>
            <ta e="T296" id="Seg_4609" s="T295">adv</ta>
            <ta e="T297" id="Seg_4610" s="T296">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T298" id="Seg_4611" s="T297">v-v:cvb</ta>
            <ta e="T300" id="Seg_4612" s="T299">v-v:tense-v:poss.pn</ta>
            <ta e="T301" id="Seg_4613" s="T300">conj</ta>
            <ta e="T302" id="Seg_4614" s="T301">dempro.[pro:case]</ta>
            <ta e="T303" id="Seg_4615" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_4616" s="T303">dempro</ta>
            <ta e="T305" id="Seg_4617" s="T304">adv-n:case</ta>
            <ta e="T306" id="Seg_4618" s="T305">post</ta>
            <ta e="T307" id="Seg_4619" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_4620" s="T307">que-pro:case</ta>
            <ta e="T309" id="Seg_4621" s="T308">v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_4622" s="T317">adv</ta>
            <ta e="T319" id="Seg_4623" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_4624" s="T319">cardnum</ta>
            <ta e="T321" id="Seg_4625" s="T320">cardnum-cardnum&gt;ordnum-ordnum&gt;adj</ta>
            <ta e="T322" id="Seg_4626" s="T321">conj</ta>
            <ta e="T323" id="Seg_4627" s="T322">cardnum-cardnum&gt;ordnum-ordnum&gt;adj</ta>
            <ta e="T324" id="Seg_4628" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_4629" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_4630" s="T325">que.[pro:case]</ta>
            <ta e="T327" id="Seg_4631" s="T326">v-v:tense.[v:pred.pn]</ta>
            <ta e="T328" id="Seg_4632" s="T327">n.[n:case]</ta>
            <ta e="T329" id="Seg_4633" s="T328">v-v:tense.[v:pred.pn]</ta>
            <ta e="T330" id="Seg_4634" s="T329">propr-n:case</ta>
            <ta e="T331" id="Seg_4635" s="T330">adv</ta>
            <ta e="T332" id="Seg_4636" s="T331">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T333" id="Seg_4637" s="T332">n-n:poss-n:case</ta>
            <ta e="T336" id="Seg_4638" s="T335">v-v:ptcp-v:(case)</ta>
            <ta e="T337" id="Seg_4639" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_4640" s="T337">adv</ta>
            <ta e="T339" id="Seg_4641" s="T338">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T340" id="Seg_4642" s="T339">cardnum</ta>
            <ta e="T353" id="Seg_4643" s="T352">cardnum</ta>
            <ta e="T358" id="Seg_4644" s="T357">dempro</ta>
            <ta e="T359" id="Seg_4645" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_4646" s="T359">que.[pro:case]</ta>
            <ta e="T361" id="Seg_4647" s="T360">v-v:tense-v:poss.pn</ta>
            <ta e="T362" id="Seg_4648" s="T361">pers-pro:case</ta>
            <ta e="T374" id="Seg_4649" s="T373">n-n:(num)-n:poss-n:case</ta>
            <ta e="T375" id="Seg_4650" s="T374">post</ta>
            <ta e="T376" id="Seg_4651" s="T375">n-n:poss-n:case</ta>
            <ta e="T377" id="Seg_4652" s="T376">post</ta>
            <ta e="T378" id="Seg_4653" s="T377">v-v:tense-v:pred.pn</ta>
            <ta e="T379" id="Seg_4654" s="T378">adv</ta>
            <ta e="T381" id="Seg_4655" s="T380">n-n:poss-n:case</ta>
            <ta e="T382" id="Seg_4656" s="T381">post</ta>
            <ta e="T383" id="Seg_4657" s="T382">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T384" id="Seg_4658" s="T383">ptcl</ta>
            <ta e="T385" id="Seg_4659" s="T384">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T386" id="Seg_4660" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_4661" s="T386">v-v:tense.[v:pred.pn]</ta>
            <ta e="T388" id="Seg_4662" s="T387">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T389" id="Seg_4663" s="T388">adv</ta>
            <ta e="T390" id="Seg_4664" s="T389">v-v:tense-v:pred.pn</ta>
            <ta e="T391" id="Seg_4665" s="T390">conj</ta>
            <ta e="T392" id="Seg_4666" s="T391">pers-pro:case</ta>
            <ta e="T393" id="Seg_4667" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_4668" s="T393">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T395" id="Seg_4669" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_4670" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_4671" s="T396">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T398" id="Seg_4672" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_4673" s="T398">v-v:ptcp</ta>
            <ta e="T400" id="Seg_4674" s="T399">v-v:mood-v:temp.pn</ta>
            <ta e="T401" id="Seg_4675" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_4676" s="T401">propr-n:case</ta>
            <ta e="T403" id="Seg_4677" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_4678" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_4679" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_4680" s="T405">pers-pro:case</ta>
            <ta e="T407" id="Seg_4681" s="T406">que.[pro:case]</ta>
            <ta e="T416" id="Seg_4682" s="T415">dempro</ta>
            <ta e="T417" id="Seg_4683" s="T416">dempro-pro:case</ta>
            <ta e="T419" id="Seg_4684" s="T418">pers-pro:case</ta>
            <ta e="T420" id="Seg_4685" s="T419">que-pro:case</ta>
            <ta e="T421" id="Seg_4686" s="T420">v-v:tense-v:poss.pn</ta>
            <ta e="T422" id="Seg_4687" s="T421">n-n:case</ta>
            <ta e="T423" id="Seg_4688" s="T422">v-v:cvb</ta>
            <ta e="T424" id="Seg_4689" s="T423">adv</ta>
            <ta e="T425" id="Seg_4690" s="T424">ptcl</ta>
            <ta e="T429" id="Seg_4691" s="T428">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T430" id="Seg_4692" s="T429">post</ta>
            <ta e="T431" id="Seg_4693" s="T430">adv</ta>
            <ta e="T432" id="Seg_4694" s="T431">v-v:tense-v:pred.pn</ta>
            <ta e="T433" id="Seg_4695" s="T432">adv</ta>
            <ta e="T434" id="Seg_4696" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_4697" s="T434">propr-n:poss-n:case</ta>
            <ta e="T436" id="Seg_4698" s="T435">que-pro:(poss).[pro:case]</ta>
            <ta e="T437" id="Seg_4699" s="T436">v-v:tense.[v:pred.pn]</ta>
            <ta e="T438" id="Seg_4700" s="T437">n-n:(num).[n:case]</ta>
            <ta e="T439" id="Seg_4701" s="T438">v-v:ptcp</ta>
            <ta e="T440" id="Seg_4702" s="T439">v-v:tense-v:poss.pn</ta>
            <ta e="T441" id="Seg_4703" s="T440">dempro-pro:(num).[pro:case]</ta>
            <ta e="T442" id="Seg_4704" s="T441">pers-pro:case</ta>
            <ta e="T443" id="Seg_4705" s="T442">n.[n:case]</ta>
            <ta e="T444" id="Seg_4706" s="T443">v-v:tense-v:poss.pn</ta>
            <ta e="T446" id="Seg_4707" s="T445">dempro</ta>
            <ta e="T447" id="Seg_4708" s="T446">adj</ta>
            <ta e="T448" id="Seg_4709" s="T447">n.[n:case]</ta>
            <ta e="T449" id="Seg_4710" s="T448">v-v:tense-v:poss.pn</ta>
            <ta e="T450" id="Seg_4711" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_4712" s="T450">cardnum</ta>
            <ta e="T452" id="Seg_4713" s="T451">cardnum</ta>
            <ta e="T453" id="Seg_4714" s="T452">n.[n:case]</ta>
            <ta e="T454" id="Seg_4715" s="T453">adv</ta>
            <ta e="T455" id="Seg_4716" s="T454">ptcl</ta>
            <ta e="T456" id="Seg_4717" s="T455">adv</ta>
            <ta e="T457" id="Seg_4718" s="T456">v-v:tense-v:pred.pn</ta>
            <ta e="T458" id="Seg_4719" s="T457">n-n:(poss).[n:case]</ta>
            <ta e="T459" id="Seg_4720" s="T458">v-v:mood-v:temp.pn</ta>
            <ta e="T460" id="Seg_4721" s="T459">adv</ta>
            <ta e="T461" id="Seg_4722" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_4723" s="T461">pers-pro:case</ta>
            <ta e="T463" id="Seg_4724" s="T462">pers.[pro:case]</ta>
            <ta e="T464" id="Seg_4725" s="T463">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KiLS">
            <ta e="T2" id="Seg_4726" s="T1">pers</ta>
            <ta e="T3" id="Seg_4727" s="T2">n</ta>
            <ta e="T4" id="Seg_4728" s="T3">propr</ta>
            <ta e="T5" id="Seg_4729" s="T4">propr</ta>
            <ta e="T6" id="Seg_4730" s="T5">propr</ta>
            <ta e="T7" id="Seg_4731" s="T6">pers</ta>
            <ta e="T8" id="Seg_4732" s="T7">pers</ta>
            <ta e="T9" id="Seg_4733" s="T8">v</ta>
            <ta e="T10" id="Seg_4734" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_4735" s="T10">propr</ta>
            <ta e="T12" id="Seg_4736" s="T11">propr</ta>
            <ta e="T13" id="Seg_4737" s="T12">n</ta>
            <ta e="T14" id="Seg_4738" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_4739" s="T14">adj</ta>
            <ta e="T16" id="Seg_4740" s="T15">cop</ta>
            <ta e="T17" id="Seg_4741" s="T16">que</ta>
            <ta e="T18" id="Seg_4742" s="T17">que</ta>
            <ta e="T19" id="Seg_4743" s="T18">n</ta>
            <ta e="T20" id="Seg_4744" s="T19">propr</ta>
            <ta e="T22" id="Seg_4745" s="T20">v</ta>
            <ta e="T25" id="Seg_4746" s="T24">n</ta>
            <ta e="T27" id="Seg_4747" s="T25">cop</ta>
            <ta e="T31" id="Seg_4748" s="T29">n</ta>
            <ta e="T32" id="Seg_4749" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_4750" s="T32">conj</ta>
            <ta e="T34" id="Seg_4751" s="T33">n</ta>
            <ta e="T35" id="Seg_4752" s="T34">v</ta>
            <ta e="T36" id="Seg_4753" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_4754" s="T36">pers</ta>
            <ta e="T38" id="Seg_4755" s="T37">v</ta>
            <ta e="T39" id="Seg_4756" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_4757" s="T39">pers</ta>
            <ta e="T41" id="Seg_4758" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_4759" s="T41">que</ta>
            <ta e="T43" id="Seg_4760" s="T42">v</ta>
            <ta e="T44" id="Seg_4761" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_4762" s="T44">que</ta>
            <ta e="T46" id="Seg_4763" s="T45">n</ta>
            <ta e="T47" id="Seg_4764" s="T46">v</ta>
            <ta e="T48" id="Seg_4765" s="T47">n</ta>
            <ta e="T49" id="Seg_4766" s="T48">adv</ta>
            <ta e="T50" id="Seg_4767" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_4768" s="T50">n</ta>
            <ta e="T52" id="Seg_4769" s="T51">que</ta>
            <ta e="T53" id="Seg_4770" s="T52">ptcl</ta>
            <ta e="T55" id="Seg_4771" s="T53">adv</ta>
            <ta e="T59" id="Seg_4772" s="T57">n</ta>
            <ta e="T62" id="Seg_4773" s="T60">n</ta>
            <ta e="T63" id="Seg_4774" s="T62">dempro</ta>
            <ta e="T64" id="Seg_4775" s="T63">dempro</ta>
            <ta e="T65" id="Seg_4776" s="T64">dempro</ta>
            <ta e="T66" id="Seg_4777" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_4778" s="T66">n</ta>
            <ta e="T68" id="Seg_4779" s="T67">v</ta>
            <ta e="T69" id="Seg_4780" s="T68">post</ta>
            <ta e="T70" id="Seg_4781" s="T69">dempro</ta>
            <ta e="T71" id="Seg_4782" s="T70">dempro</ta>
            <ta e="T72" id="Seg_4783" s="T71">n</ta>
            <ta e="T73" id="Seg_4784" s="T72">n</ta>
            <ta e="T74" id="Seg_4785" s="T73">dempro</ta>
            <ta e="T75" id="Seg_4786" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_4787" s="T75">n</ta>
            <ta e="T77" id="Seg_4788" s="T76">v</ta>
            <ta e="T78" id="Seg_4789" s="T77">aux</ta>
            <ta e="T79" id="Seg_4790" s="T78">dempro</ta>
            <ta e="T80" id="Seg_4791" s="T79">dempro</ta>
            <ta e="T81" id="Seg_4792" s="T80">n</ta>
            <ta e="T82" id="Seg_4793" s="T81">dempro</ta>
            <ta e="T83" id="Seg_4794" s="T82">n</ta>
            <ta e="T84" id="Seg_4795" s="T83">dempro</ta>
            <ta e="T85" id="Seg_4796" s="T84">adv</ta>
            <ta e="T86" id="Seg_4797" s="T85">conj</ta>
            <ta e="T87" id="Seg_4798" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_4799" s="T87">adv</ta>
            <ta e="T89" id="Seg_4800" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_4801" s="T89">pers</ta>
            <ta e="T91" id="Seg_4802" s="T90">cardnum</ta>
            <ta e="T92" id="Seg_4803" s="T91">cardnum</ta>
            <ta e="T93" id="Seg_4804" s="T92">cardnum</ta>
            <ta e="T94" id="Seg_4805" s="T93">adj</ta>
            <ta e="T95" id="Seg_4806" s="T94">cop</ta>
            <ta e="T96" id="Seg_4807" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_4808" s="T96">que</ta>
            <ta e="T98" id="Seg_4809" s="T97">propr</ta>
            <ta e="T99" id="Seg_4810" s="T98">v</ta>
            <ta e="T100" id="Seg_4811" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_4812" s="T100">pers</ta>
            <ta e="T102" id="Seg_4813" s="T101">adv</ta>
            <ta e="T103" id="Seg_4814" s="T102">n</ta>
            <ta e="T104" id="Seg_4815" s="T103">v</ta>
            <ta e="T105" id="Seg_4816" s="T104">aux</ta>
            <ta e="T106" id="Seg_4817" s="T105">n</ta>
            <ta e="T107" id="Seg_4818" s="T106">dempro</ta>
            <ta e="T108" id="Seg_4819" s="T107">quant</ta>
            <ta e="T109" id="Seg_4820" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_4821" s="T109">adj</ta>
            <ta e="T112" id="Seg_4822" s="T111">quant</ta>
            <ta e="T113" id="Seg_4823" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_4824" s="T113">adj</ta>
            <ta e="T115" id="Seg_4825" s="T114">cop</ta>
            <ta e="T116" id="Seg_4826" s="T115">quant</ta>
            <ta e="T117" id="Seg_4827" s="T116">n</ta>
            <ta e="T118" id="Seg_4828" s="T117">cop</ta>
            <ta e="T119" id="Seg_4829" s="T118">n</ta>
            <ta e="T120" id="Seg_4830" s="T119">n</ta>
            <ta e="T121" id="Seg_4831" s="T120">distrnum</ta>
            <ta e="T122" id="Seg_4832" s="T121">distrnum</ta>
            <ta e="T123" id="Seg_4833" s="T122">n</ta>
            <ta e="T124" id="Seg_4834" s="T123">n</ta>
            <ta e="T125" id="Seg_4835" s="T124">v</ta>
            <ta e="T127" id="Seg_4836" s="T125">n</ta>
            <ta e="T132" id="Seg_4837" s="T131">adv</ta>
            <ta e="T133" id="Seg_4838" s="T132">pers</ta>
            <ta e="T134" id="Seg_4839" s="T133">adv</ta>
            <ta e="T135" id="Seg_4840" s="T134">adv</ta>
            <ta e="T136" id="Seg_4841" s="T135">v</ta>
            <ta e="T137" id="Seg_4842" s="T136">adj</ta>
            <ta e="T138" id="Seg_4843" s="T137">n</ta>
            <ta e="T139" id="Seg_4844" s="T138">adv</ta>
            <ta e="T140" id="Seg_4845" s="T139">distrnum</ta>
            <ta e="T141" id="Seg_4846" s="T140">distrnum</ta>
            <ta e="T142" id="Seg_4847" s="T141">n</ta>
            <ta e="T144" id="Seg_4848" s="T142">n</ta>
            <ta e="T151" id="Seg_4849" s="T150">adv</ta>
            <ta e="T152" id="Seg_4850" s="T151">que</ta>
            <ta e="T153" id="Seg_4851" s="T152">cardnum</ta>
            <ta e="T154" id="Seg_4852" s="T153">cardnum</ta>
            <ta e="T155" id="Seg_4853" s="T154">cardnum</ta>
            <ta e="T156" id="Seg_4854" s="T155">n</ta>
            <ta e="T157" id="Seg_4855" s="T156">cardnum</ta>
            <ta e="T158" id="Seg_4856" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_4857" s="T158">adj</ta>
            <ta e="T160" id="Seg_4858" s="T159">cop</ta>
            <ta e="T161" id="Seg_4859" s="T160">adv</ta>
            <ta e="T162" id="Seg_4860" s="T161">adv</ta>
            <ta e="T163" id="Seg_4861" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_4862" s="T163">que</ta>
            <ta e="T165" id="Seg_4863" s="T164">adj</ta>
            <ta e="T166" id="Seg_4864" s="T165">n</ta>
            <ta e="T167" id="Seg_4865" s="T166">distrnum</ta>
            <ta e="T168" id="Seg_4866" s="T167">distrnum</ta>
            <ta e="T169" id="Seg_4867" s="T168">n</ta>
            <ta e="T170" id="Seg_4868" s="T169">n</ta>
            <ta e="T171" id="Seg_4869" s="T170">v</ta>
            <ta e="T172" id="Seg_4870" s="T171">aux</ta>
            <ta e="T173" id="Seg_4871" s="T172">adv</ta>
            <ta e="T174" id="Seg_4872" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_4873" s="T174">n</ta>
            <ta e="T176" id="Seg_4874" s="T175">adv</ta>
            <ta e="T177" id="Seg_4875" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_4876" s="T177">v</ta>
            <ta e="T179" id="Seg_4877" s="T178">aux</ta>
            <ta e="T180" id="Seg_4878" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_4879" s="T180">adv</ta>
            <ta e="T182" id="Seg_4880" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_4881" s="T182">pers</ta>
            <ta e="T185" id="Seg_4882" s="T184">propr</ta>
            <ta e="T186" id="Seg_4883" s="T185">v</ta>
            <ta e="T187" id="Seg_4884" s="T186">n</ta>
            <ta e="T188" id="Seg_4885" s="T187">v</ta>
            <ta e="T189" id="Seg_4886" s="T188">v</ta>
            <ta e="T190" id="Seg_4887" s="T189">adv</ta>
            <ta e="T191" id="Seg_4888" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_4889" s="T191">pers</ta>
            <ta e="T193" id="Seg_4890" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_4891" s="T193">v</ta>
            <ta e="T195" id="Seg_4892" s="T194">propr</ta>
            <ta e="T196" id="Seg_4893" s="T195">propr</ta>
            <ta e="T197" id="Seg_4894" s="T196">propr</ta>
            <ta e="T198" id="Seg_4895" s="T197">propr</ta>
            <ta e="T199" id="Seg_4896" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_4897" s="T199">cardnum</ta>
            <ta e="T201" id="Seg_4898" s="T200">n</ta>
            <ta e="T202" id="Seg_4899" s="T201">v</ta>
            <ta e="T203" id="Seg_4900" s="T202">post</ta>
            <ta e="T204" id="Seg_4901" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_4902" s="T204">pers</ta>
            <ta e="T206" id="Seg_4903" s="T205">que</ta>
            <ta e="T207" id="Seg_4904" s="T206">v</ta>
            <ta e="T208" id="Seg_4905" s="T207">propr</ta>
            <ta e="T209" id="Seg_4906" s="T208">adv</ta>
            <ta e="T210" id="Seg_4907" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_4908" s="T210">cardnum</ta>
            <ta e="T212" id="Seg_4909" s="T211">ordnum</ta>
            <ta e="T213" id="Seg_4910" s="T212">n</ta>
            <ta e="T214" id="Seg_4911" s="T213">v</ta>
            <ta e="T217" id="Seg_4912" s="T216">n</ta>
            <ta e="T218" id="Seg_4913" s="T217">adv</ta>
            <ta e="T219" id="Seg_4914" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_4915" s="T219">interj</ta>
            <ta e="T222" id="Seg_4916" s="T221">cardnum</ta>
            <ta e="T223" id="Seg_4917" s="T222">adj</ta>
            <ta e="T225" id="Seg_4918" s="T224">pers</ta>
            <ta e="T226" id="Seg_4919" s="T225">ptcl</ta>
            <ta e="T228" id="Seg_4920" s="T227">que</ta>
            <ta e="T229" id="Seg_4921" s="T228">que</ta>
            <ta e="T230" id="Seg_4922" s="T229">v</ta>
            <ta e="T235" id="Seg_4923" s="T234">adv</ta>
            <ta e="T236" id="Seg_4924" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_4925" s="T236">cardnum</ta>
            <ta e="T238" id="Seg_4926" s="T237">n</ta>
            <ta e="T239" id="Seg_4927" s="T238">v</ta>
            <ta e="T240" id="Seg_4928" s="T239">adv</ta>
            <ta e="T241" id="Seg_4929" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_4930" s="T241">ordnum</ta>
            <ta e="T243" id="Seg_4931" s="T242">n</ta>
            <ta e="T244" id="Seg_4932" s="T243">n</ta>
            <ta e="T247" id="Seg_4933" s="T246">n</ta>
            <ta e="T248" id="Seg_4934" s="T247">cardnum</ta>
            <ta e="T249" id="Seg_4935" s="T248">n</ta>
            <ta e="T250" id="Seg_4936" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_4937" s="T250">dempro</ta>
            <ta e="T252" id="Seg_4938" s="T251">v</ta>
            <ta e="T253" id="Seg_4939" s="T252">post</ta>
            <ta e="T254" id="Seg_4940" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_4941" s="T254">pers</ta>
            <ta e="T256" id="Seg_4942" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_4943" s="T256">propr</ta>
            <ta e="T258" id="Seg_4944" s="T257">v</ta>
            <ta e="T259" id="Seg_4945" s="T258">v</ta>
            <ta e="T260" id="Seg_4946" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_4947" s="T260">n</ta>
            <ta e="T262" id="Seg_4948" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_4949" s="T262">v</ta>
            <ta e="T264" id="Seg_4950" s="T263">propr</ta>
            <ta e="T265" id="Seg_4951" s="T264">v</ta>
            <ta e="T266" id="Seg_4952" s="T265">post</ta>
            <ta e="T267" id="Seg_4953" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_4954" s="T267">pers</ta>
            <ta e="T269" id="Seg_4955" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_4956" s="T269">propr</ta>
            <ta e="T271" id="Seg_4957" s="T270">v</ta>
            <ta e="T272" id="Seg_4958" s="T271">adv</ta>
            <ta e="T273" id="Seg_4959" s="T272">que</ta>
            <ta e="T274" id="Seg_4960" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_4961" s="T274">cop</ta>
            <ta e="T278" id="Seg_4962" s="T277">ptcl</ta>
            <ta e="T282" id="Seg_4963" s="T281">cop</ta>
            <ta e="T284" id="Seg_4964" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_4965" s="T284">adv</ta>
            <ta e="T286" id="Seg_4966" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_4967" s="T286">cardnum</ta>
            <ta e="T288" id="Seg_4968" s="T287">n</ta>
            <ta e="T289" id="Seg_4969" s="T288">v</ta>
            <ta e="T290" id="Seg_4970" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_4971" s="T290">cardnum</ta>
            <ta e="T292" id="Seg_4972" s="T291">adj</ta>
            <ta e="T293" id="Seg_4973" s="T292">cardnum</ta>
            <ta e="T294" id="Seg_4974" s="T293">adj</ta>
            <ta e="T295" id="Seg_4975" s="T294">n</ta>
            <ta e="T296" id="Seg_4976" s="T295">adv</ta>
            <ta e="T297" id="Seg_4977" s="T296">v</ta>
            <ta e="T298" id="Seg_4978" s="T297">v</ta>
            <ta e="T300" id="Seg_4979" s="T299">v</ta>
            <ta e="T301" id="Seg_4980" s="T300">conj</ta>
            <ta e="T302" id="Seg_4981" s="T301">dempro</ta>
            <ta e="T303" id="Seg_4982" s="T302">n</ta>
            <ta e="T304" id="Seg_4983" s="T303">dempro</ta>
            <ta e="T305" id="Seg_4984" s="T304">adv</ta>
            <ta e="T306" id="Seg_4985" s="T305">post</ta>
            <ta e="T307" id="Seg_4986" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_4987" s="T307">que</ta>
            <ta e="T309" id="Seg_4988" s="T308">v</ta>
            <ta e="T318" id="Seg_4989" s="T317">adv</ta>
            <ta e="T319" id="Seg_4990" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_4991" s="T319">cardnum</ta>
            <ta e="T321" id="Seg_4992" s="T320">adj</ta>
            <ta e="T322" id="Seg_4993" s="T321">conj</ta>
            <ta e="T323" id="Seg_4994" s="T322">adj</ta>
            <ta e="T324" id="Seg_4995" s="T323">n</ta>
            <ta e="T325" id="Seg_4996" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_4997" s="T325">que</ta>
            <ta e="T327" id="Seg_4998" s="T326">cop</ta>
            <ta e="T328" id="Seg_4999" s="T327">n</ta>
            <ta e="T329" id="Seg_5000" s="T328">cop</ta>
            <ta e="T330" id="Seg_5001" s="T329">propr</ta>
            <ta e="T331" id="Seg_5002" s="T330">adv</ta>
            <ta e="T332" id="Seg_5003" s="T331">emphpro</ta>
            <ta e="T333" id="Seg_5004" s="T332">n</ta>
            <ta e="T336" id="Seg_5005" s="T335">v</ta>
            <ta e="T337" id="Seg_5006" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_5007" s="T337">adv</ta>
            <ta e="T339" id="Seg_5008" s="T338">v</ta>
            <ta e="T340" id="Seg_5009" s="T339">cardnum</ta>
            <ta e="T353" id="Seg_5010" s="T352">cardnum</ta>
            <ta e="T358" id="Seg_5011" s="T357">dempro</ta>
            <ta e="T359" id="Seg_5012" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_5013" s="T359">que</ta>
            <ta e="T361" id="Seg_5014" s="T360">v</ta>
            <ta e="T362" id="Seg_5015" s="T361">pers</ta>
            <ta e="T374" id="Seg_5016" s="T373">n</ta>
            <ta e="T375" id="Seg_5017" s="T374">post</ta>
            <ta e="T376" id="Seg_5018" s="T375">n</ta>
            <ta e="T377" id="Seg_5019" s="T376">post</ta>
            <ta e="T378" id="Seg_5020" s="T377">v</ta>
            <ta e="T379" id="Seg_5021" s="T378">adv</ta>
            <ta e="T381" id="Seg_5022" s="T380">n</ta>
            <ta e="T382" id="Seg_5023" s="T381">post</ta>
            <ta e="T383" id="Seg_5024" s="T382">dempro</ta>
            <ta e="T384" id="Seg_5025" s="T383">ptcl</ta>
            <ta e="T385" id="Seg_5026" s="T384">ordnum</ta>
            <ta e="T386" id="Seg_5027" s="T385">n</ta>
            <ta e="T387" id="Seg_5028" s="T386">v</ta>
            <ta e="T388" id="Seg_5029" s="T387">emphpro</ta>
            <ta e="T389" id="Seg_5030" s="T388">adv</ta>
            <ta e="T390" id="Seg_5031" s="T389">v</ta>
            <ta e="T391" id="Seg_5032" s="T390">conj</ta>
            <ta e="T392" id="Seg_5033" s="T391">pers</ta>
            <ta e="T393" id="Seg_5034" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_5035" s="T393">ordnum</ta>
            <ta e="T395" id="Seg_5036" s="T394">n</ta>
            <ta e="T396" id="Seg_5037" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_5038" s="T396">ordnum</ta>
            <ta e="T398" id="Seg_5039" s="T397">n</ta>
            <ta e="T399" id="Seg_5040" s="T398">v</ta>
            <ta e="T400" id="Seg_5041" s="T399">aux</ta>
            <ta e="T401" id="Seg_5042" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_5043" s="T401">propr</ta>
            <ta e="T403" id="Seg_5044" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_5045" s="T403">n</ta>
            <ta e="T405" id="Seg_5046" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_5047" s="T405">pers</ta>
            <ta e="T407" id="Seg_5048" s="T406">que</ta>
            <ta e="T416" id="Seg_5049" s="T415">dempro</ta>
            <ta e="T417" id="Seg_5050" s="T416">dempro</ta>
            <ta e="T419" id="Seg_5051" s="T418">pers</ta>
            <ta e="T420" id="Seg_5052" s="T419">que</ta>
            <ta e="T421" id="Seg_5053" s="T420">v</ta>
            <ta e="T422" id="Seg_5054" s="T421">n</ta>
            <ta e="T423" id="Seg_5055" s="T422">v</ta>
            <ta e="T424" id="Seg_5056" s="T423">adv</ta>
            <ta e="T425" id="Seg_5057" s="T424">ptcl</ta>
            <ta e="T429" id="Seg_5058" s="T428">v</ta>
            <ta e="T430" id="Seg_5059" s="T429">post</ta>
            <ta e="T431" id="Seg_5060" s="T430">adv</ta>
            <ta e="T432" id="Seg_5061" s="T431">v</ta>
            <ta e="T433" id="Seg_5062" s="T432">adv</ta>
            <ta e="T434" id="Seg_5063" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_5064" s="T434">propr</ta>
            <ta e="T436" id="Seg_5065" s="T435">que</ta>
            <ta e="T437" id="Seg_5066" s="T436">v</ta>
            <ta e="T438" id="Seg_5067" s="T437">n</ta>
            <ta e="T439" id="Seg_5068" s="T438">v</ta>
            <ta e="T440" id="Seg_5069" s="T439">aux</ta>
            <ta e="T441" id="Seg_5070" s="T440">dempro</ta>
            <ta e="T442" id="Seg_5071" s="T441">pers</ta>
            <ta e="T443" id="Seg_5072" s="T442">n</ta>
            <ta e="T444" id="Seg_5073" s="T443">v</ta>
            <ta e="T446" id="Seg_5074" s="T445">dempro</ta>
            <ta e="T447" id="Seg_5075" s="T446">adj</ta>
            <ta e="T448" id="Seg_5076" s="T447">n</ta>
            <ta e="T449" id="Seg_5077" s="T448">cop</ta>
            <ta e="T450" id="Seg_5078" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_5079" s="T450">cardnum</ta>
            <ta e="T452" id="Seg_5080" s="T451">cardnum</ta>
            <ta e="T453" id="Seg_5081" s="T452">n</ta>
            <ta e="T454" id="Seg_5082" s="T453">adv</ta>
            <ta e="T455" id="Seg_5083" s="T454">ptcl</ta>
            <ta e="T456" id="Seg_5084" s="T455">adv</ta>
            <ta e="T457" id="Seg_5085" s="T456">v</ta>
            <ta e="T458" id="Seg_5086" s="T457">n</ta>
            <ta e="T459" id="Seg_5087" s="T458">v</ta>
            <ta e="T460" id="Seg_5088" s="T459">adv</ta>
            <ta e="T461" id="Seg_5089" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_5090" s="T461">pers</ta>
            <ta e="T463" id="Seg_5091" s="T462">pers</ta>
            <ta e="T464" id="Seg_5092" s="T463">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KiLS" />
         <annotation name="SyF" tierref="SyF-KiLS" />
         <annotation name="IST" tierref="IST-KiLS" />
         <annotation name="Top" tierref="Top-KiLS" />
         <annotation name="Foc" tierref="Foc-KiLS" />
         <annotation name="BOR" tierref="BOR-KiLS">
            <ta e="T4" id="Seg_5093" s="T3">RUS:cult</ta>
            <ta e="T5" id="Seg_5094" s="T4">RUS:cult</ta>
            <ta e="T6" id="Seg_5095" s="T5">RUS:cult</ta>
            <ta e="T12" id="Seg_5096" s="T11">RUS:cult</ta>
            <ta e="T25" id="Seg_5097" s="T24">RUS:cult</ta>
            <ta e="T33" id="Seg_5098" s="T32">RUS:gram</ta>
            <ta e="T55" id="Seg_5099" s="T53">RUS:mod</ta>
            <ta e="T59" id="Seg_5100" s="T57">EV:cult</ta>
            <ta e="T85" id="Seg_5101" s="T84">RUS:core</ta>
            <ta e="T86" id="Seg_5102" s="T85">RUS:gram</ta>
            <ta e="T98" id="Seg_5103" s="T97">RUS:cult</ta>
            <ta e="T100" id="Seg_5104" s="T99">RUS:discRUS:disc</ta>
            <ta e="T102" id="Seg_5105" s="T101">RUS:core</ta>
            <ta e="T123" id="Seg_5106" s="T122">RUS:cult</ta>
            <ta e="T134" id="Seg_5107" s="T133">RUS:mod</ta>
            <ta e="T175" id="Seg_5108" s="T174">RUS:core</ta>
            <ta e="T178" id="Seg_5109" s="T177">RUS:core</ta>
            <ta e="T180" id="Seg_5110" s="T179">RUS:disc</ta>
            <ta e="T185" id="Seg_5111" s="T184">RUS:cult</ta>
            <ta e="T195" id="Seg_5112" s="T194">RUS:cult</ta>
            <ta e="T196" id="Seg_5113" s="T195">RUS:cult</ta>
            <ta e="T201" id="Seg_5114" s="T200">RUS:cult</ta>
            <ta e="T208" id="Seg_5115" s="T207">RUS:cult</ta>
            <ta e="T213" id="Seg_5116" s="T212">RUS:cult</ta>
            <ta e="T217" id="Seg_5117" s="T216">RUS:cult</ta>
            <ta e="T230" id="Seg_5118" s="T229">RUS:cult</ta>
            <ta e="T244" id="Seg_5119" s="T243">RUS:cult</ta>
            <ta e="T247" id="Seg_5120" s="T246">RUS:cult</ta>
            <ta e="T250" id="Seg_5121" s="T249">RUS:disc</ta>
            <ta e="T264" id="Seg_5122" s="T263">RUS:cult</ta>
            <ta e="T270" id="Seg_5123" s="T269">RUS:cult</ta>
            <ta e="T284" id="Seg_5124" s="T283">RUS:disc</ta>
            <ta e="T290" id="Seg_5125" s="T289">RUS:disc</ta>
            <ta e="T301" id="Seg_5126" s="T300">RUS:gram</ta>
            <ta e="T322" id="Seg_5127" s="T321">RUS:gram</ta>
            <ta e="T328" id="Seg_5128" s="T327">RUS:cult</ta>
            <ta e="T330" id="Seg_5129" s="T329">RUS:cult</ta>
            <ta e="T381" id="Seg_5130" s="T380">RUS:cult</ta>
            <ta e="T386" id="Seg_5131" s="T385">RUS:cult</ta>
            <ta e="T389" id="Seg_5132" s="T388">RUS:core</ta>
            <ta e="T391" id="Seg_5133" s="T390">RUS:gram</ta>
            <ta e="T395" id="Seg_5134" s="T394">RUS:cult</ta>
            <ta e="T398" id="Seg_5135" s="T397">RUS:cult</ta>
            <ta e="T404" id="Seg_5136" s="T403">RUS:cult</ta>
            <ta e="T431" id="Seg_5137" s="T430">RUS:core</ta>
            <ta e="T434" id="Seg_5138" s="T433">RUS:disc</ta>
            <ta e="T435" id="Seg_5139" s="T434">RUS:cult</ta>
            <ta e="T438" id="Seg_5140" s="T437">RUS:cult</ta>
            <ta e="T453" id="Seg_5141" s="T452">RUS:cult</ta>
            <ta e="T456" id="Seg_5142" s="T455">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KiLS" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KiLS" />
         <annotation name="CS" tierref="CS-KiLS" />
         <annotation name="fe" tierref="fe-KiLS">
            <ta e="T6" id="Seg_5143" s="T1">– My name is Lina Semyonovna Kirgizova.</ta>
            <ta e="T12" id="Seg_5144" s="T6">I was born in Khatanga, in Syndassko.</ta>
            <ta e="T19" id="Seg_5145" s="T12">My parents were tundra people, whatchamacallit, reindeer herders. </ta>
            <ta e="T22" id="Seg_5146" s="T19">I was born in Korgo.</ta>
            <ta e="T27" id="Seg_5147" s="T24">– They were trappers.</ta>
            <ta e="T48" id="Seg_5148" s="T29">– According to my mother, when I was born, when I was born, they put a pole into the tundra.</ta>
            <ta e="T51" id="Seg_5149" s="T48">There's a scraper.</ta>
            <ta e="T55" id="Seg_5150" s="T51">What else is there?</ta>
            <ta e="T59" id="Seg_5151" s="T57">– A scraper for leather.</ta>
            <ta e="T62" id="Seg_5152" s="T60">– A toothed scraper.</ta>
            <ta e="T73" id="Seg_5153" s="T62">That is bound to piece of wood, that is named "togoho".</ta>
            <ta e="T87" id="Seg_5154" s="T73">They stuck [it] into the earth, that one, the pole, the pole is still there.</ta>
            <ta e="T99" id="Seg_5155" s="T87">And then, when I was seven years old, we went to Syndassko.</ta>
            <ta e="T106" id="Seg_5156" s="T99">So we always nomadized in the tundra, mum.</ta>
            <ta e="T127" id="Seg_5157" s="T106">We had a very big herd, there were many families, we nomads nomadized with six, five baloks, pole tents each, mum.</ta>
            <ta e="T144" id="Seg_5158" s="T131">– Then we also live on the other shore, on a high mountain, there [were] six, five pole tents.</ta>
            <ta e="T160" id="Seg_5159" s="T150">– There she had whatchamacallit, only two children she had there.</ta>
            <ta e="T172" id="Seg_5160" s="T160">There on the other shore on the high mountain, there were six, five pole tents standing.</ta>
            <ta e="T180" id="Seg_5161" s="T172">There the fishermen were fishing in summer.</ta>
            <ta e="T189" id="Seg_5162" s="T180">Then we went to Syndassko, after my father had died.</ta>
            <ta e="T208" id="Seg_5163" s="T197">Having finished eight classes in Khatanga, they sent [us] to whatchamacallit, Krasnoyarsk.</ta>
            <ta e="T217" id="Seg_5164" s="T208">There we learned in the 27th school, the ninth, tenth grade.</ta>
            <ta e="T234" id="Seg_5165" s="T217">But then, eh, it was in 1980, we (…) I entered the state medical institute in Krasnoyarsk.</ta>
            <ta e="T239" id="Seg_5166" s="T234">There I studied for six years.</ta>
            <ta e="T250" id="Seg_5167" s="T239">Then my seventh year was an internship, an internship on tuberculosis one year.</ta>
            <ta e="T263" id="Seg_5168" s="T250">When I finished, I was sent to Taimyr, for that I work in my homeland.</ta>
            <ta e="T271" id="Seg_5169" s="T263">Having arrived in Dudinka I was sent to Potapovo.</ta>
            <ta e="T277" id="Seg_5170" s="T271">There was whatchamacallit, a forestry school.</ta>
            <ta e="T278" id="Seg_5171" s="T277">No.</ta>
            <ta e="T282" id="Seg_5172" s="T278">It was the forestry school of Potapovo.</ta>
            <ta e="T289" id="Seg_5173" s="T282">There I worked for three years.</ta>
            <ta e="T300" id="Seg_5174" s="T289">In 1984, in 1984 I came here, I was sent as a translator.</ta>
            <ta e="T317" id="Seg_5175" s="T300">And since then until now I work in whatchamacallit, in the district dispensary for tuberculosis no. 9, as a specialist for tuberculosis.</ta>
            <ta e="T330" id="Seg_5176" s="T317">Then in 1989, in the year 1989 there was a whatchamacallit, there was an earthquake in Armenia.</ta>
            <ta e="T344" id="Seg_5177" s="T330">There I worked voluntarily, for free so to say, two years in a row.</ta>
            <ta e="T352" id="Seg_5178" s="T344">In the dispensary for tuberculosis in the name of Melkonyan of the town of Leninakan.</ta>
            <ta e="T373" id="Seg_5179" s="T352">Eighty, in 2005 I was given the honorary certificate of the ministry of health, that is an equivalent to the Veteran of Work.</ta>
            <ta e="T382" id="Seg_5180" s="T373">I am living now with my family, with my mother, with my niece.</ta>
            <ta e="T387" id="Seg_5181" s="T382">She learns in the tenth grade.</ta>
            <ta e="T390" id="Seg_5182" s="T387">I myself am always working.</ta>
            <ta e="T415" id="Seg_5183" s="T390">And us, when we were learning in the fifth grade, in Khatanga, we were organized by our Dolgan poetess Ogduo Aksyonova in a circle of barganists.</ta>
            <ta e="T423" id="Seg_5184" s="T415">She taught us to play the bargan.</ta>
            <ta e="T433" id="Seg_5185" s="T423">Now, until today, until I am old I always play.</ta>
            <ta e="T444" id="Seg_5186" s="T433">Well, there came whatchamacallit, artists from Yakutsk, they sold us bargans.</ta>
            <ta e="T453" id="Seg_5187" s="T444">At that time it was much money, eighty rubles.</ta>
            <ta e="T464" id="Seg_5188" s="T453">Now I always play, when I want, now I will play for you.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KiLS">
            <ta e="T6" id="Seg_5189" s="T1">– Mein Name ist Lina Semjonovna Kirgizova.</ta>
            <ta e="T12" id="Seg_5190" s="T6">Ich wurde in Chatanga geboren, in Syndassko.</ta>
            <ta e="T19" id="Seg_5191" s="T12">Meine Eltern waren Tundraleute, dings, Rentierhirten.</ta>
            <ta e="T22" id="Seg_5192" s="T19">Ich wurde in Korgo geboren.</ta>
            <ta e="T27" id="Seg_5193" s="T24">– Sie waren Fallensteller.</ta>
            <ta e="T48" id="Seg_5194" s="T29">– Gemäß meiner Mutter, als ich geboren wurde, als ich geboren wurde, stellten sie einen Pfahl in der Tundra auf.</ta>
            <ta e="T51" id="Seg_5195" s="T48">Dort ist ein Schabeisen.</ta>
            <ta e="T55" id="Seg_5196" s="T51">Was gibt es noch?</ta>
            <ta e="T59" id="Seg_5197" s="T57">– Мялка для кожи.</ta>
            <ta e="T62" id="Seg_5198" s="T60">– Ein Schaber mit Zähnen.</ta>
            <ta e="T73" id="Seg_5199" s="T62">Das wird an ein Holz gebunden, das heißt dann "togoho".</ta>
            <ta e="T87" id="Seg_5200" s="T73">[Den] haben sie in die Erde gesteckt, dieser, der Pfahl, der Pfahl ist immer noch da.</ta>
            <ta e="T99" id="Seg_5201" s="T87">Und dann, als ich sieben Jahre alt war, gingen wir nach Syndassko.</ta>
            <ta e="T106" id="Seg_5202" s="T99">So nomadisierten wir immer in der Tundra, Mama.</ta>
            <ta e="T127" id="Seg_5203" s="T106">Wir hatten ein sehr große Herde, es waren viele Familien, wir Nomaden sind mit je sechs, fünf Baloks, Stangenzelte nomadisiert, Mama.</ta>
            <ta e="T144" id="Seg_5204" s="T131">– Dann lebten wir noch auf jenem Ufer, auf einem hohen Berg, dort [waren] sechs, fünf Stangenzelte.</ta>
            <ta e="T160" id="Seg_5205" s="T150">– Dort hatte sie Dings, nur zwei Kinder hatte sie.</ta>
            <ta e="T172" id="Seg_5206" s="T160">Dort am anderen Ufer auf dem hohen Berg standen sechs, fünf Stangenzelte.</ta>
            <ta e="T180" id="Seg_5207" s="T172">Dort fischten die Fischer im Sommer.</ta>
            <ta e="T189" id="Seg_5208" s="T180">Dann gingen wir nach Syndassko, nachdem mein Vater gestorben war.</ta>
            <ta e="T197" id="Seg_5209" s="T189">Dann lernten wir in Novorybnoe, in Kosistyj, in Chantanga.</ta>
            <ta e="T208" id="Seg_5210" s="T197">Nachdem ich in Chatanga acht Klassen abgeschlossen hatte, wurden [wir] nach Dings, nach Krasnojarsk geschickt.</ta>
            <ta e="T217" id="Seg_5211" s="T208">Dort lernten wir in der 27. Schule, die neunte, zehnte Klasse.</ta>
            <ta e="T234" id="Seg_5212" s="T217">Dann aber, äh, es war im Jahr 1980, wir aber (…) ich kam auf das staatliche medizinische Institut in Krasnojarsk.</ta>
            <ta e="T239" id="Seg_5213" s="T234">Dort studierte ich sechs Jahre.</ta>
            <ta e="T250" id="Seg_5214" s="T239">Dann mein siebtes Jahr war ein Praktikum, ein Praktikum zu Tuberkulose ein Jahr.</ta>
            <ta e="T263" id="Seg_5215" s="T250">Als ich fertig war, wurde ich auf die Taimyr geschickt, damit ich in der Heimat arbeite.</ta>
            <ta e="T271" id="Seg_5216" s="T263">Als ich in Dudinka ankam, schickte man mich nach Potapovo.</ta>
            <ta e="T277" id="Seg_5217" s="T271">Dort gab es Dings, eine Forstschule.</ta>
            <ta e="T278" id="Seg_5218" s="T277">Nein.</ta>
            <ta e="T282" id="Seg_5219" s="T278">Es war die Forstschule von Potapovo.</ta>
            <ta e="T289" id="Seg_5220" s="T282">Dort arbeitete ich drei Jahre.</ta>
            <ta e="T300" id="Seg_5221" s="T289">1984, 1984 kam ich hierher, ich wurde als Übersetzer geschickt.</ta>
            <ta e="T317" id="Seg_5222" s="T300">Und seit jenem Jahr bis heute arbeite ich in dem Dings, im Kreisdispensaire für Tuberkulose Nr. 9, als Spezialarzt für Tuberkulose.</ta>
            <ta e="T330" id="Seg_5223" s="T317">Dann 1989, im Jahr 1989 war ein Dings, war ein Erdbeben in Armenien.</ta>
            <ta e="T344" id="Seg_5224" s="T330">Dort arbeitet ich freiwillig, umsonst sozusagen, zwei Jahre hintereinander.</ta>
            <ta e="T352" id="Seg_5225" s="T344">Im Dispensaire für Tuberkulose im Namen Melkonjans der Stadt Leninakan.</ta>
            <ta e="T373" id="Seg_5226" s="T352">Achtzig, 2005 verlieh man mir die Ehrenurkunde des Gesundheitsministeriums, das ist die Entsprechung zum Veteran der Arbeit.</ta>
            <ta e="T382" id="Seg_5227" s="T373">Ich lebe jetzt mit meiner Familie, mit meiner Mutter, mit meiner Nichte.</ta>
            <ta e="T387" id="Seg_5228" s="T382">Die lernt in der zehnte Klasse.</ta>
            <ta e="T390" id="Seg_5229" s="T387">Ich selber arbeite immer.</ta>
            <ta e="T415" id="Seg_5230" s="T390">Und uns, als wir in der fünften Klasse lernten, in Chatanga, wir wurden im Kulturhaus von unserer dolganischen Dichterin Ogduo Aksjonova in einem Kreis der Barganisten organisiert.</ta>
            <ta e="T423" id="Seg_5231" s="T415">Sie brachte uns bei Bargan zu spielen.</ta>
            <ta e="T433" id="Seg_5232" s="T423">Jetzt, bis heute, bis ich alt werde spiele ich immer.</ta>
            <ta e="T444" id="Seg_5233" s="T433">Nun, aus Jakutsk kamen Dings, kamen Künstler, die verkauften uns Bargans.</ta>
            <ta e="T453" id="Seg_5234" s="T444">Damals war das viel Geld, 80 Rubel.</ta>
            <ta e="T464" id="Seg_5235" s="T453">Jetzt spiele ich immer, wenn ich Lust habe, nun spiele ich für euch.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KiLS">
            <ta e="T6" id="Seg_5236" s="T1">– Меня зовут Лина Семёновна Киргизова.</ta>
            <ta e="T12" id="Seg_5237" s="T6">Я родилась в Хатанге, в Сындасско.</ta>
            <ta e="T19" id="Seg_5238" s="T12">Мои родители были тундровики, это, оленеводы.</ta>
            <ta e="T22" id="Seg_5239" s="T19">Я родилась в Корго.</ta>
            <ta e="T27" id="Seg_5240" s="T24">– Они были охотниками.</ta>
            <ta e="T48" id="Seg_5241" s="T29">– Как рассказывала мать, когда я родилась, … я … когда я родилась, в тундре поставили столб (досл. "тохого").</ta>
            <ta e="T51" id="Seg_5242" s="T48">Там скребок.</ta>
            <ta e="T55" id="Seg_5243" s="T51">Что еще там?</ta>
            <ta e="T59" id="Seg_5244" s="T57">skrebok gederee dvumja ruchkami bez zubov</ta>
            <ta e="T62" id="Seg_5245" s="T60">– Зубчатый скребок.</ta>
            <ta e="T73" id="Seg_5246" s="T62">Это все привязывают к куску дерева, это называется "тохого".</ta>
            <ta e="T87" id="Seg_5247" s="T73">Вот его воткнули в землю, этот, тохого, этот тохого до сих пор там стоит.</ta>
            <ta e="T99" id="Seg_5248" s="T87">А потом, когда мне было семь лет, мы поехали в Сындасско.</ta>
            <ta e="T106" id="Seg_5249" s="T99">А так мы все время аргишили в тудре, да, мам?</ta>
            <ta e="T127" id="Seg_5250" s="T106">Большое стадо, у нас было большое стадо, много семепй было, у нас по шесть, пять балков, чумов было, да, мам?</ta>
            <ta e="T144" id="Seg_5251" s="T131">Потом мы еще на другом берегу жили, на высокой горе, там шесть, пять чумов было.</ta>
            <ta e="T160" id="Seg_5252" s="T150">– Там что, два ребенка, два только ребенка у нее было.</ta>
            <ta e="T172" id="Seg_5253" s="T160">Там на другом берегу на высокой горе шесть, пять чумов стояло.</ta>
            <ta e="T180" id="Seg_5254" s="T172">Там рыбаки рыбачили летом, вот.</ta>
            <ta e="T189" id="Seg_5255" s="T180">Потом мы в Сындасско заехали, после смрети отца.</ta>
            <ta e="T197" id="Seg_5256" s="T189">Потом мы учились в Новорыбном, в Косистом, в Хатанге.</ta>
            <ta e="T208" id="Seg_5257" s="T197">Когда мы в Хатанге окончили восемь классов, нас направили в этот, в Красноярск.</ta>
            <ta e="T217" id="Seg_5258" s="T208">Там мы учились в двадцать седьмой школе, в девятом, десятом классе.</ta>
            <ta e="T234" id="Seg_5259" s="T217">А потом, ээ, в восьмидесятом году было, мы (…) я поступила в Красноярский государственный медицинский институт.</ta>
            <ta e="T239" id="Seg_5260" s="T234">Там я училась шесть лет.</ta>
            <ta e="T250" id="Seg_5261" s="T239">Потом у мнея на седьмом году интернатуры интернатура по туберкулёзу один год, вот.</ta>
            <ta e="T263" id="Seg_5262" s="T250">После окончания меня направили на Таймыр, чтобы я работала на родине.</ta>
            <ta e="T271" id="Seg_5263" s="T263">После приезда в Дудинку меня направили в Потапово.</ta>
            <ta e="T277" id="Seg_5264" s="T271">Там это, была лесная школа.</ta>
            <ta e="T278" id="Seg_5265" s="T277">Нет.</ta>
            <ta e="T282" id="Seg_5266" s="T278">Это было Потаповская лесная школа.</ta>
            <ta e="T289" id="Seg_5267" s="T282">Там я работала три года.</ta>
            <ta e="T300" id="Seg_5268" s="T289">Вот, в восемьдесят четвертом, в восемьдесят четвертом году я переехала сюда, переводом отправили.</ta>
            <ta e="T317" id="Seg_5269" s="T300">И с того года по сей день я работаю в краевом противотуберкулёзном диспансере номере девять, врач-фтизиатор.</ta>
            <ta e="T330" id="Seg_5270" s="T317">Потом в восемьдесят девятом и девностом, было это, было землетрясение в Армении.</ta>
            <ta e="T344" id="Seg_5271" s="T330">Там по собственному желанию, как говорится, бесплатно, безвозмездно, я работала два года подряд.</ta>
            <ta e="T352" id="Seg_5272" s="T344">В противотуберкулёзном диспансере имени Мелконяна города Ленинакана, вот.</ta>
            <ta e="T373" id="Seg_5273" s="T352">Восемьдесят, в в 2005 мне дали это, почетную грамоту Министерства здравоохранения, это союзного значения как ветеран труда, вот.</ta>
            <ta e="T382" id="Seg_5274" s="T373">Я живу со своей семьей, с мамой и с племянницей.</ta>
            <ta e="T387" id="Seg_5275" s="T382">Которая учится в десятом классе.</ta>
            <ta e="T390" id="Seg_5276" s="T387">Сама я все еще работаю.</ta>
            <ta e="T415" id="Seg_5277" s="T390">А нас, когда мы учились в пятом классе, в Хатанге, в доме культуры у нас это, организовала кружок баянистов наша долганская поэтесса Огдуо Аксёнова.</ta>
            <ta e="T423" id="Seg_5278" s="T415">Она научила нас играть на баргане.</ta>
            <ta e="T433" id="Seg_5279" s="T423">Теперь я до сих пор, в зрелом возрасте, потостоянно играю.</ta>
            <ta e="T444" id="Seg_5280" s="T433">Вот, из Якутска приезжали эти, приезжали артисты, они нам продали барганы.</ta>
            <ta e="T453" id="Seg_5281" s="T444">Тогда это были большие деньги, восемьдесят рублей.</ta>
            <ta e="T464" id="Seg_5282" s="T453">Теперь я всегда играю, когда душа просит, сейчас вам сыграю.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-KiLS">
            <ta e="T6" id="Seg_5283" s="T1">menja zovut LSK</ta>
            <ta e="T12" id="Seg_5284" s="T6">ja rodilas v Khatange v syndassko</ta>
            <ta e="T19" id="Seg_5285" s="T12">roditeli byli tundraviki, olenevody</ta>
            <ta e="T22" id="Seg_5286" s="T19">ja rodilas na Korgo</ta>
            <ta e="T27" id="Seg_5287" s="T24">ohotniki byli</ta>
            <ta e="T48" id="Seg_5288" s="T29">po slovam materi pri moem rozhdenii… ja… pri moem rozhdenii postavili tohogo</ta>
            <ta e="T51" id="Seg_5289" s="T48">tam skrebok kihiak</ta>
            <ta e="T55" id="Seg_5290" s="T51">chto esh'o tam est'</ta>
            <ta e="T59" id="Seg_5291" s="T57">skrebok gederee dvumja ruchkami bez zubov</ta>
            <ta e="T62" id="Seg_5292" s="T60">hongohoon ruchka dlinnaja zubchataja</ta>
            <ta e="T73" id="Seg_5293" s="T62">eto privjazyvajut k derevu eto nazyvaetsja tohogo</ta>
            <ta e="T87" id="Seg_5294" s="T73">v zemlju votknuli i tohogoo do sih por stoit</ta>
            <ta e="T99" id="Seg_5295" s="T87">a potom kogda mne ispolnilos sem let my zaehali v Syndassko</ta>
            <ta e="T106" id="Seg_5296" s="T99">a tak my vse vremja argishili v tundre, ne tak li mama?</ta>
            <ta e="T127" id="Seg_5297" s="T106">stado bylo bolshoe bylo mnogo semej bylo kochujush'ih ljudej po pjat shest balkov chumov kochevali, ne tak li mama?</ta>
            <ta e="T144" id="Seg_5298" s="T131">potom esh' my na tom beregu zhili, na vysokoj gore, tam shest pjat chumov bylo, </ta>
            <ta e="T160" id="Seg_5299" s="T150">skazhi, togda u mamy bylo dva rebenka</ta>
            <ta e="T172" id="Seg_5300" s="T160">na tom beregu (na poberezhe) na vysokoj gore, shest pjat chumov stojalo,</ta>
            <ta e="T180" id="Seg_5301" s="T172">tam rybaki rybachili letom, vot</ta>
            <ta e="T189" id="Seg_5302" s="T180">potom min…. my v Syndassko zaehali, posle smerti otsa</ta>
            <ta e="T197" id="Seg_5303" s="T189">potom my uchilis v Novorybnom, Kosistom, v Khatange</ta>
            <ta e="T208" id="Seg_5304" s="T197">V Khatange posle okonchania vosmi klasov, nas napravili v Krasnojarsk</ta>
            <ta e="T217" id="Seg_5305" s="T208">tam my uchili v dvadsat sedmoj shkole, v devjatom desjatom klasse</ta>
            <ta e="T234" id="Seg_5306" s="T217">potom v vosmidesjatom godu ja postupila v KGM Institut</ta>
            <ta e="T239" id="Seg_5307" s="T234">tam ja uchilas' shest' let</ta>
            <ta e="T250" id="Seg_5308" s="T239">potom na sedmom godu internatura tuberkulosnaja internatura odin god, vot</ta>
            <ta e="T263" id="Seg_5309" s="T250">posle okonchania menja napravili na Taimyr chtoby ja rabotala na rodine</ta>
            <ta e="T271" id="Seg_5310" s="T263">[posle priezda v DUdinku menja napravili v Potapovo</ta>
            <ta e="T277" id="Seg_5311" s="T271">tam byla lesnaja shkola</ta>
            <ta e="T278" id="Seg_5312" s="T277">net</ta>
            <ta e="T282" id="Seg_5313" s="T278">eto bylo pota l sh</ta>
            <ta e="T289" id="Seg_5314" s="T282">tam ja prorabotala tri goda</ta>
            <ta e="T300" id="Seg_5315" s="T289">v vosemdesjat chetvertom godu ja pereehala sjuda, perevodom otpravili</ta>
            <ta e="T317" id="Seg_5316" s="T300">s togo goda po sej den' ja rabotaju v KPD nomer devjat vrachom vtisiarom </ta>
            <ta e="T330" id="Seg_5317" s="T317">potom v vosemdesjat devjaton v devjanostom proizoshlo zemljetrjazenie v Armenii</ta>
            <ta e="T344" id="Seg_5318" s="T330">toda po sobstvennom zhelaniju besplatno bezvozmezdno kak by govorja, rabotala tam dva goda podrjad </ta>
            <ta e="T352" id="Seg_5319" s="T344">v PTD imeni Melkonjana goroda Leninakana vot</ta>
            <ta e="T373" id="Seg_5320" s="T352">v 2005 godu dali mne pochotnuju gramotu MZD </ta>
            <ta e="T382" id="Seg_5321" s="T373">zhivu s svoej semoj, s mamoj i s plemjanitsej</ta>
            <ta e="T387" id="Seg_5322" s="T382">kotoraja uchitsja v desatom klase</ta>
            <ta e="T390" id="Seg_5323" s="T387">sama vse esho rabotaju</ta>
            <ta e="T415" id="Seg_5324" s="T390">a nas kogda my uchilis v pjatom klase, v Khatange v dome kultury organizovala kruzhok barganistov nasha dolganskaja poetessa Ogduo Aksenova</ta>
            <ta e="T423" id="Seg_5325" s="T415">ona nauchila nas igre na bargane</ta>
            <ta e="T433" id="Seg_5326" s="T423">teper do sik por v zrelom vozraste postojanno igraju</ta>
            <ta e="T444" id="Seg_5327" s="T433">s Jakutstka priezzhale artisty, oni nam prodali bargany</ta>
            <ta e="T453" id="Seg_5328" s="T444">togda eto byli bolshie dengi, vosemdesjat rublej</ta>
            <ta e="T464" id="Seg_5329" s="T453">seichas postojanno igraju kogda dusha prosit, teper sigraju vam</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KiLS">
            <ta e="T352" id="Seg_5330" s="T344">[DCh]: "Leninakan" was the name of a town in Armenia, which is called today "Gyumri".</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KiES"
                      id="tx-KiES"
                      speaker="KiES"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KiES">
            <ts e="T23" id="Seg_5331" n="sc" s="T21">
               <ts e="T23" id="Seg_5333" n="HIAT:u" s="T21">
                  <nts id="Seg_5334" n="HIAT:ip">–</nts>
                  <nts id="Seg_5335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_5337" n="HIAT:w" s="T21">Paːččɨttar</ts>
                  <nts id="Seg_5338" n="HIAT:ip">.</nts>
                  <nts id="Seg_5339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T29" id="Seg_5340" n="sc" s="T26">
               <ts e="T29" id="Seg_5342" n="HIAT:u" s="T26">
                  <nts id="Seg_5343" n="HIAT:ip">–</nts>
                  <nts id="Seg_5344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_5346" n="HIAT:w" s="T26">Kɨrsa</ts>
                  <nts id="Seg_5347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5348" n="HIAT:ip">(</nts>
                  <ts e="T29" id="Seg_5350" n="HIAT:w" s="T28">eni</ts>
                  <nts id="Seg_5351" n="HIAT:ip">)</nts>
                  <nts id="Seg_5352" n="HIAT:ip">.</nts>
                  <nts id="Seg_5353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T56" id="Seg_5354" n="sc" s="T54">
               <ts e="T56" id="Seg_5356" n="HIAT:u" s="T54">
                  <nts id="Seg_5357" n="HIAT:ip">–</nts>
                  <nts id="Seg_5358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_5360" n="HIAT:w" s="T54">Gedereː</ts>
                  <nts id="Seg_5361" n="HIAT:ip">.</nts>
                  <nts id="Seg_5362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_5363" n="sc" s="T59">
               <ts e="T61" id="Seg_5365" n="HIAT:u" s="T59">
                  <nts id="Seg_5366" n="HIAT:ip">–</nts>
                  <nts id="Seg_5367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_5369" n="HIAT:w" s="T59">Hoŋohoːn</ts>
                  <nts id="Seg_5370" n="HIAT:ip">.</nts>
                  <nts id="Seg_5371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T130" id="Seg_5372" n="sc" s="T126">
               <ts e="T130" id="Seg_5374" n="HIAT:u" s="T126">
                  <nts id="Seg_5375" n="HIAT:ip">–</nts>
                  <nts id="Seg_5376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_5378" n="HIAT:w" s="T126">Uraha</ts>
                  <nts id="Seg_5379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_5381" n="HIAT:w" s="T128">dʼi͡e</ts>
                  <nts id="Seg_5382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5383" n="HIAT:ip">(</nts>
                  <nts id="Seg_5384" n="HIAT:ip">(</nts>
                  <ats e="T130" id="Seg_5385" n="HIAT:non-pho" s="T129">…</ats>
                  <nts id="Seg_5386" n="HIAT:ip">)</nts>
                  <nts id="Seg_5387" n="HIAT:ip">)</nts>
                  <nts id="Seg_5388" n="HIAT:ip">.</nts>
                  <nts id="Seg_5389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T146" id="Seg_5390" n="sc" s="T140">
               <ts e="T146" id="Seg_5392" n="HIAT:u" s="T140">
                  <nts id="Seg_5393" n="HIAT:ip">–</nts>
                  <nts id="Seg_5394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_5396" n="HIAT:w" s="T140">Inʼem</ts>
                  <nts id="Seg_5397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_5399" n="HIAT:w" s="T142">eder</ts>
                  <nts id="Seg_5400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_5402" n="HIAT:w" s="T143">ogo</ts>
                  <nts id="Seg_5403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_5405" n="HIAT:w" s="T144">ete</ts>
                  <nts id="Seg_5406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5407" n="HIAT:ip">(</nts>
                  <nts id="Seg_5408" n="HIAT:ip">(</nts>
                  <ats e="T146" id="Seg_5409" n="HIAT:non-pho" s="T145">…</ats>
                  <nts id="Seg_5410" n="HIAT:ip">)</nts>
                  <nts id="Seg_5411" n="HIAT:ip">)</nts>
                  <nts id="Seg_5412" n="HIAT:ip">.</nts>
                  <nts id="Seg_5413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KiES">
            <ts e="T23" id="Seg_5414" n="sc" s="T21">
               <ts e="T23" id="Seg_5416" n="e" s="T21">– Paːččɨttar. </ts>
            </ts>
            <ts e="T29" id="Seg_5417" n="sc" s="T26">
               <ts e="T28" id="Seg_5419" n="e" s="T26">– Kɨrsa </ts>
               <ts e="T29" id="Seg_5421" n="e" s="T28">(eni). </ts>
            </ts>
            <ts e="T56" id="Seg_5422" n="sc" s="T54">
               <ts e="T56" id="Seg_5424" n="e" s="T54">– Gedereː. </ts>
            </ts>
            <ts e="T61" id="Seg_5425" n="sc" s="T59">
               <ts e="T61" id="Seg_5427" n="e" s="T59">– Hoŋohoːn. </ts>
            </ts>
            <ts e="T130" id="Seg_5428" n="sc" s="T126">
               <ts e="T128" id="Seg_5430" n="e" s="T126">– Uraha </ts>
               <ts e="T129" id="Seg_5432" n="e" s="T128">dʼi͡e </ts>
               <ts e="T130" id="Seg_5434" n="e" s="T129">((…)). </ts>
            </ts>
            <ts e="T146" id="Seg_5435" n="sc" s="T140">
               <ts e="T142" id="Seg_5437" n="e" s="T140">– Inʼem </ts>
               <ts e="T143" id="Seg_5439" n="e" s="T142">eder </ts>
               <ts e="T144" id="Seg_5441" n="e" s="T143">ogo </ts>
               <ts e="T145" id="Seg_5443" n="e" s="T144">ete </ts>
               <ts e="T146" id="Seg_5445" n="e" s="T145">((…)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KiES">
            <ta e="T23" id="Seg_5446" s="T21">KiLS_KiES_2009_Life_nar.KiES.001 (001.005)</ta>
            <ta e="T29" id="Seg_5447" s="T26">KiLS_KiES_2009_Life_nar.KiES.002 (001.007)</ta>
            <ta e="T56" id="Seg_5448" s="T54">KiLS_KiES_2009_Life_nar.KiES.003 (001.011)</ta>
            <ta e="T61" id="Seg_5449" s="T59">KiLS_KiES_2009_Life_nar.KiES.004 (001.013)</ta>
            <ta e="T130" id="Seg_5450" s="T126">KiLS_KiES_2009_Life_nar.KiES.005 (001.020)</ta>
            <ta e="T146" id="Seg_5451" s="T140">KiLS_KiES_2009_Life_nar.KiES.006 (001.022)</ta>
         </annotation>
         <annotation name="st" tierref="st-KiES" />
         <annotation name="ts" tierref="ts-KiES">
            <ta e="T23" id="Seg_5452" s="T21">– Paːččɨttar. </ta>
            <ta e="T29" id="Seg_5453" s="T26">– Kɨrsa (eni). </ta>
            <ta e="T56" id="Seg_5454" s="T54">– Gedereː. </ta>
            <ta e="T61" id="Seg_5455" s="T59">– Hoŋohoːn. </ta>
            <ta e="T130" id="Seg_5456" s="T126">– Uraha dʼi͡e ((…)). </ta>
            <ta e="T146" id="Seg_5457" s="T140">– Inʼem eder ogo ete ((…)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KiES">
            <ta e="T23" id="Seg_5458" s="T21">paːč-čɨt-tar</ta>
            <ta e="T28" id="Seg_5459" s="T26">kɨrsa</ta>
            <ta e="T29" id="Seg_5460" s="T28">eni</ta>
            <ta e="T56" id="Seg_5461" s="T54">gedereː</ta>
            <ta e="T61" id="Seg_5462" s="T59">hoŋohoːn</ta>
            <ta e="T128" id="Seg_5463" s="T126">uraha</ta>
            <ta e="T129" id="Seg_5464" s="T128">dʼi͡e</ta>
            <ta e="T142" id="Seg_5465" s="T140">inʼe-m</ta>
            <ta e="T143" id="Seg_5466" s="T142">eder</ta>
            <ta e="T144" id="Seg_5467" s="T143">ogo</ta>
            <ta e="T145" id="Seg_5468" s="T144">e-t-e</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KiES">
            <ta e="T23" id="Seg_5469" s="T21">paːs-ČIt-LAr</ta>
            <ta e="T28" id="Seg_5470" s="T26">kɨrsa</ta>
            <ta e="T29" id="Seg_5471" s="T28">eni</ta>
            <ta e="T56" id="Seg_5472" s="T54">gedereː</ta>
            <ta e="T61" id="Seg_5473" s="T59">hoŋohoːn</ta>
            <ta e="T128" id="Seg_5474" s="T126">uraha</ta>
            <ta e="T129" id="Seg_5475" s="T128">dʼi͡e</ta>
            <ta e="T142" id="Seg_5476" s="T140">inʼe-m</ta>
            <ta e="T143" id="Seg_5477" s="T142">eder</ta>
            <ta e="T144" id="Seg_5478" s="T143">ogo</ta>
            <ta e="T145" id="Seg_5479" s="T144">e-TI-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KiES">
            <ta e="T23" id="Seg_5480" s="T21">deadfall-AG-PL.[NOM]</ta>
            <ta e="T28" id="Seg_5481" s="T26">polar.fox.[NOM]</ta>
            <ta e="T29" id="Seg_5482" s="T28">apparently</ta>
            <ta e="T56" id="Seg_5483" s="T54">scraper.for.leather.[NOM]</ta>
            <ta e="T61" id="Seg_5484" s="T59">scraper.with.toothed.end.[NOM]</ta>
            <ta e="T128" id="Seg_5485" s="T126">pole.[NOM]</ta>
            <ta e="T129" id="Seg_5486" s="T128">tent.[NOM]</ta>
            <ta e="T142" id="Seg_5487" s="T140">mother-1SG.[NOM]</ta>
            <ta e="T143" id="Seg_5488" s="T142">young</ta>
            <ta e="T144" id="Seg_5489" s="T143">child.[NOM]</ta>
            <ta e="T145" id="Seg_5490" s="T144">be-PST1-3SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KiES">
            <ta e="T23" id="Seg_5491" s="T21">Totfalle-AG-PL.[NOM]</ta>
            <ta e="T28" id="Seg_5492" s="T26">Polarfuchs.[NOM]</ta>
            <ta e="T29" id="Seg_5493" s="T28">offenbar</ta>
            <ta e="T56" id="Seg_5494" s="T54">Lederschaber.[NOM]</ta>
            <ta e="T61" id="Seg_5495" s="T59">Schaber.mit.Zähnen.[NOM]</ta>
            <ta e="T128" id="Seg_5496" s="T126">Stange.[NOM]</ta>
            <ta e="T129" id="Seg_5497" s="T128">Zelt.[NOM]</ta>
            <ta e="T142" id="Seg_5498" s="T140">Mutter-1SG.[NOM]</ta>
            <ta e="T143" id="Seg_5499" s="T142">jung</ta>
            <ta e="T144" id="Seg_5500" s="T143">Kind.[NOM]</ta>
            <ta e="T145" id="Seg_5501" s="T144">sein-PST1-3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KiES">
            <ta e="T23" id="Seg_5502" s="T21">пасть-AG-PL.[NOM]</ta>
            <ta e="T28" id="Seg_5503" s="T26">песец.[NOM]</ta>
            <ta e="T29" id="Seg_5504" s="T28">очевидно</ta>
            <ta e="T56" id="Seg_5505" s="T54">кожемялка.[NOM]</ta>
            <ta e="T61" id="Seg_5506" s="T59">скребок.зубчатый.[NOM]</ta>
            <ta e="T128" id="Seg_5507" s="T126">шест.[NOM]</ta>
            <ta e="T129" id="Seg_5508" s="T128">чум.[NOM]</ta>
            <ta e="T142" id="Seg_5509" s="T140">мать-1SG.[NOM]</ta>
            <ta e="T143" id="Seg_5510" s="T142">молодой</ta>
            <ta e="T144" id="Seg_5511" s="T143">ребенок.[NOM]</ta>
            <ta e="T145" id="Seg_5512" s="T144">быть-PST1-3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KiES">
            <ta e="T23" id="Seg_5513" s="T21">n-n&gt;n-n:(num).[n:case]</ta>
            <ta e="T28" id="Seg_5514" s="T26">n.[n:case]</ta>
            <ta e="T29" id="Seg_5515" s="T28">ptcl</ta>
            <ta e="T56" id="Seg_5516" s="T54">n.[n:case]</ta>
            <ta e="T61" id="Seg_5517" s="T59">n.[n:case]</ta>
            <ta e="T128" id="Seg_5518" s="T126">n.[n:case]</ta>
            <ta e="T129" id="Seg_5519" s="T128">n.[n:case]</ta>
            <ta e="T142" id="Seg_5520" s="T140">n-n:(poss).[n:case]</ta>
            <ta e="T143" id="Seg_5521" s="T142">adj</ta>
            <ta e="T144" id="Seg_5522" s="T143">n.[n:case]</ta>
            <ta e="T145" id="Seg_5523" s="T144">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KiES">
            <ta e="T23" id="Seg_5524" s="T21">n</ta>
            <ta e="T28" id="Seg_5525" s="T26">n</ta>
            <ta e="T29" id="Seg_5526" s="T28">ptcl</ta>
            <ta e="T56" id="Seg_5527" s="T54">n</ta>
            <ta e="T61" id="Seg_5528" s="T59">n</ta>
            <ta e="T128" id="Seg_5529" s="T126">n</ta>
            <ta e="T129" id="Seg_5530" s="T128">n</ta>
            <ta e="T142" id="Seg_5531" s="T140">n</ta>
            <ta e="T143" id="Seg_5532" s="T142">adj</ta>
            <ta e="T144" id="Seg_5533" s="T143">n</ta>
            <ta e="T145" id="Seg_5534" s="T144">cop</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KiES" />
         <annotation name="SyF" tierref="SyF-KiES" />
         <annotation name="IST" tierref="IST-KiES" />
         <annotation name="Top" tierref="Top-KiES" />
         <annotation name="Foc" tierref="Foc-KiES" />
         <annotation name="BOR" tierref="BOR-KiES">
            <ta e="T23" id="Seg_5535" s="T21">RUS:cult</ta>
            <ta e="T56" id="Seg_5536" s="T54">EV:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KiES" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KiES" />
         <annotation name="CS" tierref="CS-KiES" />
         <annotation name="fe" tierref="fe-KiES">
            <ta e="T23" id="Seg_5537" s="T21">– Trappers.</ta>
            <ta e="T29" id="Seg_5538" s="T26">– Polar fox (apparently).</ta>
            <ta e="T56" id="Seg_5539" s="T54">– A scraper for leather.</ta>
            <ta e="T61" id="Seg_5540" s="T59">– A toothed scraper.</ta>
            <ta e="T130" id="Seg_5541" s="T126">– Pole tent (…).</ta>
            <ta e="T146" id="Seg_5542" s="T140">– My mother was young (…).</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KiES">
            <ta e="T23" id="Seg_5543" s="T21">– Fallensteller.</ta>
            <ta e="T29" id="Seg_5544" s="T26">– Polarfuchs (offenbar).</ta>
            <ta e="T56" id="Seg_5545" s="T54">– Ein Lederschaber. </ta>
            <ta e="T61" id="Seg_5546" s="T59">– Ein Schaber mit Zähnen.</ta>
            <ta e="T130" id="Seg_5547" s="T126">– Stangenzelt (…).</ta>
            <ta e="T146" id="Seg_5548" s="T140">– Meine Mutter war jung (…).</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KiES">
            <ta e="T23" id="Seg_5549" s="T21">– Охотники.</ta>
            <ta e="T29" id="Seg_5550" s="T26">– Песец (видимо).</ta>
            <ta e="T56" id="Seg_5551" s="T54">– Мялка для кожи.</ta>
            <ta e="T61" id="Seg_5552" s="T59">– Зубчатый скребок.</ta>
            <ta e="T130" id="Seg_5553" s="T126">– Чумов (…).</ta>
            <ta e="T146" id="Seg_5554" s="T140">– Моя мама (тогда) молодая была (…).</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-KiES">
            <ta e="T130" id="Seg_5555" s="T126">chumy</ta>
            <ta e="T146" id="Seg_5556" s="T140">ES mama togda molodaja byla</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KiES">
            <ta e="T56" id="Seg_5557" s="T54">[DCh]: "Gedere" is a scraper with two handels and without teeth.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KiLS"
                          name="ref"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KiLS"
                          name="st"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KiLS"
                          name="ts"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KiLS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KiLS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KiLS"
                          name="mb"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KiLS"
                          name="mp"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KiLS"
                          name="ge"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KiLS"
                          name="gg"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KiLS"
                          name="gr"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KiLS"
                          name="mc"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KiLS"
                          name="ps"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KiLS"
                          name="SeR"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KiLS"
                          name="SyF"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KiLS"
                          name="IST"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KiLS"
                          name="Top"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KiLS"
                          name="Foc"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KiLS"
                          name="BOR"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KiLS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KiLS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KiLS"
                          name="CS"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KiLS"
                          name="fe"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KiLS"
                          name="fg"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KiLS"
                          name="fr"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KiLS"
                          name="ltr"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KiLS"
                          name="nt"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KiES"
                          name="ref"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KiES"
                          name="st"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KiES"
                          name="ts"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KiES"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KiES"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KiES"
                          name="mb"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KiES"
                          name="mp"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KiES"
                          name="ge"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KiES"
                          name="gg"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KiES"
                          name="gr"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KiES"
                          name="mc"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KiES"
                          name="ps"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KiES"
                          name="SeR"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KiES"
                          name="SyF"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KiES"
                          name="IST"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KiES"
                          name="Top"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KiES"
                          name="Foc"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KiES"
                          name="BOR"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KiES"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KiES"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KiES"
                          name="CS"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KiES"
                          name="fe"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KiES"
                          name="fg"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KiES"
                          name="fr"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KiES"
                          name="ltr"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KiES"
                          name="nt"
                          segmented-tier-id="tx-KiES"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
