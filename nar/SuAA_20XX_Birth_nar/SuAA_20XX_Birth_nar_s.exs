<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID9CE9ABEF-8B0B-1525-FB86-5BF2557C7338">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>SuAA_20XX_Birth_nar</transcription-name>
         <referenced-file url="SuAA_20XX_Birth_nar.wav" />
         <referenced-file url="SuAA_20XX_Birth_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\SuAA_20XX_Birth_nar\SuAA_20XX_Birth_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1201</ud-information>
            <ud-information attribute-name="# HIAT:w">846</ud-information>
            <ud-information attribute-name="# e">848</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">97</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SuAA">
            <abbreviation>SuAA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.7475" type="appl" />
         <tli id="T3" time="1.495" type="appl" />
         <tli id="T4" time="2.2425" type="appl" />
         <tli id="T5" time="2.99" type="appl" />
         <tli id="T6" time="3.626181818181818" type="appl" />
         <tli id="T7" time="4.262363636363636" type="appl" />
         <tli id="T8" time="4.898545454545454" type="appl" />
         <tli id="T9" time="5.534727272727272" type="appl" />
         <tli id="T10" time="6.17090909090909" type="appl" />
         <tli id="T11" time="6.807090909090909" type="appl" />
         <tli id="T12" time="7.443272727272727" type="appl" />
         <tli id="T13" time="8.079454545454546" type="appl" />
         <tli id="T14" time="8.715636363636364" type="appl" />
         <tli id="T15" time="9.351818181818182" type="appl" />
         <tli id="T16" time="9.988" type="appl" />
         <tli id="T17" time="10.53" type="appl" />
         <tli id="T18" time="11.072" type="appl" />
         <tli id="T19" time="11.614" type="appl" />
         <tli id="T20" time="12.156" type="appl" />
         <tli id="T21" time="12.698" type="appl" />
         <tli id="T22" time="13.210857142857144" type="appl" />
         <tli id="T23" time="13.723714285714285" type="appl" />
         <tli id="T24" time="14.236571428571429" type="appl" />
         <tli id="T25" time="14.749428571428572" type="appl" />
         <tli id="T26" time="15.262285714285714" type="appl" />
         <tli id="T27" time="15.775142857142857" type="appl" />
         <tli id="T28" time="16.288" type="appl" />
         <tli id="T29" time="17.096" type="appl" />
         <tli id="T30" time="17.904" type="appl" />
         <tli id="T31" time="18.712" type="appl" />
         <tli id="T32" time="19.52" type="appl" />
         <tli id="T33" time="20.328" type="appl" />
         <tli id="T34" time="21.136" type="appl" />
         <tli id="T35" time="21.944" type="appl" />
         <tli id="T36" time="22.53630769230769" type="appl" />
         <tli id="T37" time="23.128615384615383" type="appl" />
         <tli id="T38" time="23.720923076923075" type="appl" />
         <tli id="T39" time="24.313230769230767" type="appl" />
         <tli id="T40" time="24.90553846153846" type="appl" />
         <tli id="T41" time="25.49784615384615" type="appl" />
         <tli id="T42" time="26.090153846153846" type="appl" />
         <tli id="T43" time="26.68246153846154" type="appl" />
         <tli id="T44" time="27.27476923076923" type="appl" />
         <tli id="T45" time="27.867076923076922" type="appl" />
         <tli id="T46" time="28.459384615384614" type="appl" />
         <tli id="T47" time="29.051692307692306" type="appl" />
         <tli id="T48" time="29.743998523381833" />
         <tli id="T49" time="30.146461538461537" type="appl" />
         <tli id="T50" time="30.648923076923076" type="appl" />
         <tli id="T51" time="31.151384615384615" type="appl" />
         <tli id="T52" time="31.653846153846153" type="appl" />
         <tli id="T53" time="32.15630769230769" type="appl" />
         <tli id="T54" time="32.65876923076923" type="appl" />
         <tli id="T55" time="33.16123076923077" type="appl" />
         <tli id="T56" time="33.66369230769231" type="appl" />
         <tli id="T57" time="34.16615384615385" type="appl" />
         <tli id="T58" time="34.668615384615386" type="appl" />
         <tli id="T59" time="35.171076923076924" type="appl" />
         <tli id="T60" time="35.67353846153846" type="appl" />
         <tli id="T61" time="36.335997716344984" />
         <tli id="T62" time="36.74625" type="appl" />
         <tli id="T63" time="37.316500000000005" type="appl" />
         <tli id="T64" time="37.88675" type="appl" />
         <tli id="T65" time="38.457" type="appl" />
         <tli id="T66" time="39.02725" type="appl" />
         <tli id="T67" time="39.5975" type="appl" />
         <tli id="T68" time="40.16775" type="appl" />
         <tli id="T69" time="40.87133248347955" />
         <tli id="T70" time="41.4332" type="appl" />
         <tli id="T71" time="42.1284" type="appl" />
         <tli id="T72" time="42.8236" type="appl" />
         <tli id="T73" time="43.5188" type="appl" />
         <tli id="T74" time="44.214" type="appl" />
         <tli id="T75" time="44.782" type="appl" />
         <tli id="T76" time="45.35" type="appl" />
         <tli id="T77" time="45.918" type="appl" />
         <tli id="T78" time="46.486" type="appl" />
         <tli id="T79" time="47.054" type="appl" />
         <tli id="T80" time="47.622" type="appl" />
         <tli id="T81" time="48.19" type="appl" />
         <tli id="T82" time="48.758" type="appl" />
         <tli id="T83" time="49.37266739911562" />
         <tli id="T84" time="49.730000000000004" type="appl" />
         <tli id="T85" time="50.134" type="appl" />
         <tli id="T86" time="50.538" type="appl" />
         <tli id="T87" time="50.942" type="appl" />
         <tli id="T88" time="51.346000000000004" type="appl" />
         <tli id="T89" time="51.75" type="appl" />
         <tli id="T90" time="52.32225" type="appl" />
         <tli id="T91" time="52.8945" type="appl" />
         <tli id="T92" time="53.466750000000005" type="appl" />
         <tli id="T93" time="54.039" type="appl" />
         <tli id="T94" time="54.47325" type="appl" />
         <tli id="T95" time="54.9075" type="appl" />
         <tli id="T96" time="55.341750000000005" type="appl" />
         <tli id="T97" time="55.776" type="appl" />
         <tli id="T98" time="56.21025" type="appl" />
         <tli id="T99" time="56.6445" type="appl" />
         <tli id="T100" time="57.07875" type="appl" />
         <tli id="T101" time="57.513" type="appl" />
         <tli id="T102" time="57.94725" type="appl" />
         <tli id="T103" time="58.3815" type="appl" />
         <tli id="T104" time="58.81575" type="appl" />
         <tli id="T105" time="59.43666311727261" />
         <tli id="T106" time="60.077666666666666" type="appl" />
         <tli id="T107" time="60.90533333333333" type="appl" />
         <tli id="T108" time="61.75966502635787" />
         <tli id="T109" time="62.28881818181818" type="appl" />
         <tli id="T110" time="62.84463636363636" type="appl" />
         <tli id="T111" time="63.400454545454544" type="appl" />
         <tli id="T112" time="63.956272727272726" type="appl" />
         <tli id="T113" time="64.5120909090909" type="appl" />
         <tli id="T114" time="65.06790909090908" type="appl" />
         <tli id="T115" time="65.62372727272727" type="appl" />
         <tli id="T116" time="66.17954545454545" type="appl" />
         <tli id="T117" time="66.73536363636363" type="appl" />
         <tli id="T118" time="67.29118181818181" type="appl" />
         <tli id="T119" time="67.847" type="appl" />
         <tli id="T120" time="68.36053333333332" type="appl" />
         <tli id="T121" time="68.87406666666666" type="appl" />
         <tli id="T122" time="69.38759999999999" type="appl" />
         <tli id="T123" time="69.90113333333333" type="appl" />
         <tli id="T124" time="70.41466666666666" type="appl" />
         <tli id="T125" time="70.92819999999999" type="appl" />
         <tli id="T126" time="71.44173333333333" type="appl" />
         <tli id="T127" time="71.95526666666666" type="appl" />
         <tli id="T128" time="72.4688" type="appl" />
         <tli id="T129" time="72.98233333333333" type="appl" />
         <tli id="T130" time="73.49586666666666" type="appl" />
         <tli id="T131" time="74.0094" type="appl" />
         <tli id="T132" time="74.52293333333333" type="appl" />
         <tli id="T133" time="75.03646666666667" type="appl" />
         <tli id="T134" time="75.59000218031738" />
         <tli id="T135" time="76.00399999999999" type="appl" />
         <tli id="T136" time="76.458" type="appl" />
         <tli id="T137" time="76.912" type="appl" />
         <tli id="T138" time="77.366" type="appl" />
         <tli id="T139" time="77.82" type="appl" />
         <tli id="T140" time="78.274" type="appl" />
         <tli id="T141" time="78.72800000000001" type="appl" />
         <tli id="T142" time="79.182" type="appl" />
         <tli id="T143" time="79.636" type="appl" />
         <tli id="T144" time="80.19666409158688" />
         <tli id="T145" time="80.88775000000001" type="appl" />
         <tli id="T146" time="81.6855" type="appl" />
         <tli id="T147" time="82.48325" type="appl" />
         <tli id="T148" time="83.3276692122211" />
         <tli id="T149" time="83.97777777777779" type="appl" />
         <tli id="T150" time="84.67455555555556" type="appl" />
         <tli id="T151" time="85.37133333333334" type="appl" />
         <tli id="T152" time="86.06811111111112" type="appl" />
         <tli id="T153" time="86.76488888888889" type="appl" />
         <tli id="T154" time="87.46166666666667" type="appl" />
         <tli id="T155" time="88.15844444444446" type="appl" />
         <tli id="T156" time="88.85522222222222" type="appl" />
         <tli id="T157" time="89.72532862975783" />
         <tli id="T158" time="90.1448" type="appl" />
         <tli id="T159" time="90.7376" type="appl" />
         <tli id="T160" time="91.33040000000001" type="appl" />
         <tli id="T161" time="91.92320000000001" type="appl" />
         <tli id="T162" time="92.516" type="appl" />
         <tli id="T163" time="93.1088" type="appl" />
         <tli id="T164" time="93.7016" type="appl" />
         <tli id="T165" time="94.29440000000001" type="appl" />
         <tli id="T166" time="94.8872" type="appl" />
         <tli id="T167" time="95.89999632212938" />
         <tli id="T168" time="96.20071428571428" type="appl" />
         <tli id="T169" time="96.92142857142858" type="appl" />
         <tli id="T170" time="97.64214285714286" type="appl" />
         <tli id="T171" time="98.36285714285715" type="appl" />
         <tli id="T172" time="99.08357142857143" type="appl" />
         <tli id="T173" time="99.80428571428573" type="appl" />
         <tli id="T174" time="100.525" type="appl" />
         <tli id="T175" time="101.15577777777779" type="appl" />
         <tli id="T176" time="101.78655555555557" type="appl" />
         <tli id="T177" time="102.41733333333333" type="appl" />
         <tli id="T178" time="103.04811111111111" type="appl" />
         <tli id="T179" time="103.67888888888889" type="appl" />
         <tli id="T180" time="104.30966666666667" type="appl" />
         <tli id="T181" time="104.94044444444444" type="appl" />
         <tli id="T182" time="105.57122222222222" type="appl" />
         <tli id="T183" time="106.1420039152482" />
         <tli id="T184" time="107.0988" type="appl" />
         <tli id="T185" time="107.9956" type="appl" />
         <tli id="T186" time="108.89240000000001" type="appl" />
         <tli id="T187" time="109.78920000000001" type="appl" />
         <tli id="T188" time="110.686" type="appl" />
         <tli id="T189" time="111.11878947368422" type="appl" />
         <tli id="T190" time="111.55157894736843" type="appl" />
         <tli id="T191" time="111.98436842105264" type="appl" />
         <tli id="T192" time="112.41715789473685" type="appl" />
         <tli id="T193" time="112.84994736842106" type="appl" />
         <tli id="T194" time="113.28273684210527" type="appl" />
         <tli id="T195" time="113.71552631578948" type="appl" />
         <tli id="T196" time="114.14831578947368" type="appl" />
         <tli id="T197" time="114.5811052631579" type="appl" />
         <tli id="T198" time="115.0138947368421" type="appl" />
         <tli id="T199" time="115.44668421052633" type="appl" />
         <tli id="T200" time="115.87947368421054" type="appl" />
         <tli id="T201" time="116.31226315789475" type="appl" />
         <tli id="T202" time="116.74505263157896" type="appl" />
         <tli id="T203" time="117.17784210526317" type="appl" />
         <tli id="T204" time="117.61063157894738" type="appl" />
         <tli id="T205" time="118.04342105263159" type="appl" />
         <tli id="T206" time="118.4762105263158" type="appl" />
         <tli id="T207" time="118.84233707736358" />
         <tli id="T208" time="119.60544444444444" type="appl" />
         <tli id="T209" time="120.3018888888889" type="appl" />
         <tli id="T210" time="120.99833333333333" type="appl" />
         <tli id="T211" time="121.69477777777779" type="appl" />
         <tli id="T212" time="122.39122222222223" type="appl" />
         <tli id="T213" time="123.08766666666668" type="appl" />
         <tli id="T214" time="123.78411111111112" type="appl" />
         <tli id="T215" time="124.48055555555557" type="appl" />
         <tli id="T216" time="125.20366886946466" />
         <tli id="T217" time="125.64233333333334" type="appl" />
         <tli id="T218" time="126.10766666666667" type="appl" />
         <tli id="T219" time="126.57300000000001" type="appl" />
         <tli id="T220" time="127.03833333333334" type="appl" />
         <tli id="T221" time="127.50366666666667" type="appl" />
         <tli id="T222" time="127.96900000000001" type="appl" />
         <tli id="T223" time="128.43433333333334" type="appl" />
         <tli id="T224" time="128.8996666666667" type="appl" />
         <tli id="T225" time="129.365" type="appl" />
         <tli id="T226" time="129.83583333333334" type="appl" />
         <tli id="T227" time="130.30666666666667" type="appl" />
         <tli id="T228" time="130.7775" type="appl" />
         <tli id="T229" time="131.24833333333333" type="appl" />
         <tli id="T230" time="131.71916666666667" type="appl" />
         <tli id="T231" time="132.23000453750646" />
         <tli id="T232" time="132.58791666666667" type="appl" />
         <tli id="T233" time="132.98583333333335" type="appl" />
         <tli id="T234" time="133.38375" type="appl" />
         <tli id="T235" time="133.78166666666667" type="appl" />
         <tli id="T236" time="134.17958333333334" type="appl" />
         <tli id="T237" time="134.5775" type="appl" />
         <tli id="T238" time="134.97541666666666" type="appl" />
         <tli id="T239" time="135.37333333333333" type="appl" />
         <tli id="T240" time="135.77125" type="appl" />
         <tli id="T241" time="136.16916666666668" type="appl" />
         <tli id="T242" time="136.56708333333333" type="appl" />
         <tli id="T243" time="137.03833685256092" />
         <tli id="T244" time="137.54181818181817" type="appl" />
         <tli id="T245" time="138.11863636363637" type="appl" />
         <tli id="T246" time="138.69545454545454" type="appl" />
         <tli id="T247" time="139.27227272727274" type="appl" />
         <tli id="T248" time="139.8490909090909" type="appl" />
         <tli id="T249" time="140.4259090909091" type="appl" />
         <tli id="T250" time="141.00272727272727" type="appl" />
         <tli id="T251" time="141.57954545454547" type="appl" />
         <tli id="T252" time="142.15636363636364" type="appl" />
         <tli id="T253" time="142.73318181818183" type="appl" />
         <tli id="T254" time="143.42332717348515" />
         <tli id="T255" time="143.81525" type="appl" />
         <tli id="T256" time="144.3205" type="appl" />
         <tli id="T257" time="144.82575" type="appl" />
         <tli id="T258" time="145.33100000000002" type="appl" />
         <tli id="T259" time="145.83625" type="appl" />
         <tli id="T260" time="146.3415" type="appl" />
         <tli id="T261" time="146.84675000000001" type="appl" />
         <tli id="T262" time="147.352" type="appl" />
         <tli id="T263" time="147.9185" type="appl" />
         <tli id="T264" time="148.485" type="appl" />
         <tli id="T265" time="149.0515" type="appl" />
         <tli id="T266" time="149.618" type="appl" />
         <tli id="T267" time="150.1845" type="appl" />
         <tli id="T268" time="150.751" type="appl" />
         <tli id="T269" time="151.3175" type="appl" />
         <tli id="T270" time="151.88400000000001" type="appl" />
         <tli id="T271" time="152.4505" type="appl" />
         <tli id="T272" time="153.017" type="appl" />
         <tli id="T273" time="153.58350000000002" type="appl" />
         <tli id="T274" time="154.2366680556687" />
         <tli id="T275" time="155.3262" type="appl" />
         <tli id="T276" time="156.5024" type="appl" />
         <tli id="T277" time="157.67860000000002" type="appl" />
         <tli id="T278" time="158.8548" type="appl" />
         <tli id="T279" time="159.9176706617438" />
         <tli id="T280" time="160.64712500000002" type="appl" />
         <tli id="T281" time="161.26325" type="appl" />
         <tli id="T282" time="161.879375" type="appl" />
         <tli id="T283" time="162.4955" type="appl" />
         <tli id="T284" time="163.111625" type="appl" />
         <tli id="T285" time="163.72775000000001" type="appl" />
         <tli id="T286" time="164.343875" type="appl" />
         <tli id="T287" time="165.00666317976075" />
         <tli id="T288" time="165.501375" type="appl" />
         <tli id="T289" time="166.04275" type="appl" />
         <tli id="T290" time="166.584125" type="appl" />
         <tli id="T291" time="167.1255" type="appl" />
         <tli id="T292" time="167.666875" type="appl" />
         <tli id="T293" time="168.20825" type="appl" />
         <tli id="T294" time="168.749625" type="appl" />
         <tli id="T295" time="169.291" type="appl" />
         <tli id="T296" time="169.98909090909092" type="appl" />
         <tli id="T297" time="170.6871818181818" type="appl" />
         <tli id="T298" time="171.38527272727273" type="appl" />
         <tli id="T299" time="172.08336363636363" type="appl" />
         <tli id="T300" time="172.78145454545455" type="appl" />
         <tli id="T301" time="173.47954545454544" type="appl" />
         <tli id="T302" time="174.17763636363637" type="appl" />
         <tli id="T303" time="174.87572727272726" type="appl" />
         <tli id="T304" time="175.57381818181818" type="appl" />
         <tli id="T305" time="176.27190909090908" type="appl" />
         <tli id="T306" time="176.97" type="appl" />
         <tli id="T307" time="177.518" type="appl" />
         <tli id="T308" time="178.066" type="appl" />
         <tli id="T309" time="178.614" type="appl" />
         <tli id="T310" time="179.162" type="appl" />
         <tli id="T311" time="179.7700018263381" />
         <tli id="T312" time="180.41614285714286" type="appl" />
         <tli id="T313" time="181.1222857142857" type="appl" />
         <tli id="T314" time="181.82842857142856" type="appl" />
         <tli id="T315" time="182.53457142857144" type="appl" />
         <tli id="T316" time="183.2407142857143" type="appl" />
         <tli id="T317" time="183.94685714285714" type="appl" />
         <tli id="T318" time="184.653" type="appl" />
         <tli id="T319" time="185.328" type="appl" />
         <tli id="T320" time="186.003" type="appl" />
         <tli id="T321" time="186.678" type="appl" />
         <tli id="T322" time="187.35299999999998" type="appl" />
         <tli id="T323" time="188.028" type="appl" />
         <tli id="T324" time="188.70299999999997" type="appl" />
         <tli id="T325" time="189.378" type="appl" />
         <tli id="T326" time="190.053" type="appl" />
         <tli id="T327" time="190.72799999999998" type="appl" />
         <tli id="T328" time="191.403" type="appl" />
         <tli id="T329" time="192.07799999999997" type="appl" />
         <tli id="T330" time="192.68633462227137" />
         <tli id="T331" time="193.4375" type="appl" />
         <tli id="T332" time="194.12199999999999" type="appl" />
         <tli id="T333" time="194.8065" type="appl" />
         <tli id="T334" time="195.491" type="appl" />
         <tli id="T335" time="196.1755" type="appl" />
         <tli id="T336" time="196.9000006357949" />
         <tli id="T337" time="197.58550000000002" type="appl" />
         <tli id="T338" time="198.32433000726624" />
         <tli id="T339" time="198.70311111111113" type="appl" />
         <tli id="T340" time="199.09522222222222" type="appl" />
         <tli id="T341" time="199.48733333333334" type="appl" />
         <tli id="T342" time="199.87944444444446" type="appl" />
         <tli id="T343" time="200.27155555555555" type="appl" />
         <tli id="T344" time="200.66366666666667" type="appl" />
         <tli id="T345" time="201.0557777777778" type="appl" />
         <tli id="T346" time="201.44788888888888" type="appl" />
         <tli id="T347" time="201.8933304068313" />
         <tli id="T348" time="202.44975" type="appl" />
         <tli id="T349" time="203.0595" type="appl" />
         <tli id="T350" time="203.66925" type="appl" />
         <tli id="T351" time="204.31233359990827" />
         <tli id="T352" time="204.7122222222222" type="appl" />
         <tli id="T353" time="205.14544444444445" type="appl" />
         <tli id="T354" time="205.57866666666666" type="appl" />
         <tli id="T355" time="206.01188888888888" type="appl" />
         <tli id="T356" time="206.44511111111112" type="appl" />
         <tli id="T357" time="206.87833333333333" type="appl" />
         <tli id="T358" time="207.31155555555554" type="appl" />
         <tli id="T359" time="207.74477777777778" type="appl" />
         <tli id="T360" time="208.1513302865821" />
         <tli id="T361" time="208.84414285714286" type="appl" />
         <tli id="T362" time="209.51028571428571" type="appl" />
         <tli id="T363" time="210.17642857142857" type="appl" />
         <tli id="T364" time="210.84257142857143" type="appl" />
         <tli id="T365" time="211.5087142857143" type="appl" />
         <tli id="T366" time="212.17485714285715" type="appl" />
         <tli id="T367" time="212.841" type="appl" />
         <tli id="T368" time="213.35583333333335" type="appl" />
         <tli id="T369" time="213.87066666666666" type="appl" />
         <tli id="T370" time="214.3855" type="appl" />
         <tli id="T371" time="214.90033333333335" type="appl" />
         <tli id="T372" time="215.41516666666666" type="appl" />
         <tli id="T373" time="216.00999825869133" />
         <tli id="T374" time="216.3524285714286" type="appl" />
         <tli id="T375" time="216.77485714285714" type="appl" />
         <tli id="T376" time="217.19728571428573" type="appl" />
         <tli id="T377" time="217.61971428571428" type="appl" />
         <tli id="T378" time="218.04214285714286" type="appl" />
         <tli id="T379" time="218.46457142857142" type="appl" />
         <tli id="T380" time="218.887" type="appl" />
         <tli id="T381" time="219.43110000000001" type="appl" />
         <tli id="T382" time="219.9752" type="appl" />
         <tli id="T383" time="220.5193" type="appl" />
         <tli id="T384" time="221.0634" type="appl" />
         <tli id="T385" time="221.60750000000002" type="appl" />
         <tli id="T386" time="222.1516" type="appl" />
         <tli id="T387" time="222.6957" type="appl" />
         <tli id="T388" time="223.2398" type="appl" />
         <tli id="T389" time="223.78390000000002" type="appl" />
         <tli id="T390" time="224.328" type="appl" />
         <tli id="T391" time="224.92111111111112" type="appl" />
         <tli id="T392" time="225.51422222222223" type="appl" />
         <tli id="T393" time="226.10733333333334" type="appl" />
         <tli id="T394" time="226.70044444444446" type="appl" />
         <tli id="T395" time="227.29355555555554" type="appl" />
         <tli id="T396" time="227.88666666666666" type="appl" />
         <tli id="T397" time="228.47977777777777" type="appl" />
         <tli id="T398" time="229.07288888888888" type="appl" />
         <tli id="T399" time="229.61934235791736" />
         <tli id="T400" time="230.1316923076923" type="appl" />
         <tli id="T401" time="230.5973846153846" type="appl" />
         <tli id="T402" time="231.06307692307692" type="appl" />
         <tli id="T403" time="231.52876923076923" type="appl" />
         <tli id="T404" time="231.99446153846154" type="appl" />
         <tli id="T405" time="232.46015384615384" type="appl" />
         <tli id="T406" time="232.92584615384615" type="appl" />
         <tli id="T407" time="233.39153846153846" type="appl" />
         <tli id="T408" time="233.85723076923077" type="appl" />
         <tli id="T409" time="234.32292307692308" type="appl" />
         <tli id="T410" time="234.78861538461538" type="appl" />
         <tli id="T411" time="235.2543076923077" type="appl" />
         <tli id="T412" time="235.72" type="appl" />
         <tli id="T413" time="236.20925" type="appl" />
         <tli id="T414" time="236.6985" type="appl" />
         <tli id="T415" time="237.18775" type="appl" />
         <tli id="T416" time="237.7369911436162" />
         <tli id="T417" time="238.1671" type="appl" />
         <tli id="T418" time="238.6572" type="appl" />
         <tli id="T419" time="239.1473" type="appl" />
         <tli id="T420" time="239.63739999999999" type="appl" />
         <tli id="T421" time="240.1275" type="appl" />
         <tli id="T422" time="240.6176" type="appl" />
         <tli id="T423" time="241.1077" type="appl" />
         <tli id="T424" time="241.5978" type="appl" />
         <tli id="T425" time="242.0879" type="appl" />
         <tli id="T426" time="242.60465858021553" />
         <tli id="T427" time="243.38077777777778" type="appl" />
         <tli id="T428" time="244.18355555555556" type="appl" />
         <tli id="T429" time="244.98633333333333" type="appl" />
         <tli id="T430" time="245.7891111111111" type="appl" />
         <tli id="T431" time="246.5918888888889" type="appl" />
         <tli id="T432" time="247.39466666666667" type="appl" />
         <tli id="T433" time="248.19744444444444" type="appl" />
         <tli id="T434" time="249.00022222222222" type="appl" />
         <tli id="T435" time="249.8096636662602" />
         <tli id="T436" time="250.51545454545453" type="appl" />
         <tli id="T437" time="251.2279090909091" type="appl" />
         <tli id="T438" time="251.94036363636363" type="appl" />
         <tli id="T439" time="252.65281818181816" type="appl" />
         <tli id="T440" />
         <tli id="T441" time="254.07772727272726" type="appl" />
         <tli id="T442" time="254.79018181818182" type="appl" />
         <tli id="T443" time="255.50263636363636" type="appl" />
         <tli id="T444" time="256.2150909090909" type="appl" />
         <tli id="T445" time="256.92754545454545" type="appl" />
         <tli id="T446" time="257.64" type="appl" />
         <tli id="T447" time="258.1414" type="appl" />
         <tli id="T448" time="258.64279999999997" type="appl" />
         <tli id="T449" time="259.1442" type="appl" />
         <tli id="T450" time="259.6456" type="appl" />
         <tli id="T451" time="260.147" type="appl" />
         <tli id="T452" time="260.6484" type="appl" />
         <tli id="T453" time="261.14979999999997" type="appl" />
         <tli id="T454" time="261.6512" type="appl" />
         <tli id="T455" time="262.1526" type="appl" />
         <tli id="T456" time="262.654" type="appl" />
         <tli id="T457" time="263.2777142857143" type="appl" />
         <tli id="T458" time="263.90142857142854" type="appl" />
         <tli id="T459" time="264.52514285714284" type="appl" />
         <tli id="T460" time="265.14885714285714" type="appl" />
         <tli id="T461" time="265.77257142857144" type="appl" />
         <tli id="T462" time="266.3962857142857" type="appl" />
         <tli id="T463" time="267.02" type="appl" />
         <tli id="T464" time="267.5315" type="appl" />
         <tli id="T465" time="268.043" type="appl" />
         <tli id="T466" time="268.55449999999996" type="appl" />
         <tli id="T467" time="269.00598822701977" />
         <tli id="T468" time="269.6205" type="appl" />
         <tli id="T469" time="270.17499999999995" type="appl" />
         <tli id="T470" time="270.7295" type="appl" />
         <tli id="T471" time="271.284" type="appl" />
         <tli id="T472" time="271.8385" type="appl" />
         <tli id="T473" time="272.393" type="appl" />
         <tli id="T474" time="272.9475" type="appl" />
         <tli id="T475" time="273.502" type="appl" />
         <tli id="T476" time="274.05649999999997" type="appl" />
         <tli id="T477" time="274.611" type="appl" />
         <tli id="T478" time="275.1655" type="appl" />
         <tli id="T479" time="275.72" type="appl" />
         <tli id="T480" time="276.2745" type="appl" />
         <tli id="T481" time="276.8756716726483" />
         <tli id="T482" time="277.49942857142855" type="appl" />
         <tli id="T483" time="278.16985714285715" type="appl" />
         <tli id="T484" time="278.8402857142857" type="appl" />
         <tli id="T485" time="279.5107142857143" type="appl" />
         <tli id="T486" time="280.18114285714285" type="appl" />
         <tli id="T487" time="280.85157142857145" type="appl" />
         <tli id="T488" time="281.9153159109515" />
         <tli id="T489" time="282.178" type="appl" />
         <tli id="T490" time="282.834" type="appl" />
         <tli id="T491" time="283.59667299787077" />
         <tli id="T492" time="284.0037" type="appl" />
         <tli id="T493" time="284.5174" type="appl" />
         <tli id="T494" time="285.03110000000004" type="appl" />
         <tli id="T495" time="285.5448" type="appl" />
         <tli id="T496" time="286.0585" type="appl" />
         <tli id="T497" time="286.5722" type="appl" />
         <tli id="T498" time="287.08590000000004" type="appl" />
         <tli id="T499" time="287.5996" type="appl" />
         <tli id="T500" time="288.1133" type="appl" />
         <tli id="T501" time="288.627" type="appl" />
         <tli id="T502" time="289.03360000000004" type="appl" />
         <tli id="T503" time="289.4402" type="appl" />
         <tli id="T504" time="289.84680000000003" type="appl" />
         <tli id="T505" time="290.2534" type="appl" />
         <tli id="T506" time="290.6866536240235" />
         <tli id="T507" time="291.20444444444445" type="appl" />
         <tli id="T508" time="291.7488888888889" type="appl" />
         <tli id="T509" time="292.29333333333335" type="appl" />
         <tli id="T510" time="292.8377777777778" type="appl" />
         <tli id="T511" time="293.38222222222225" type="appl" />
         <tli id="T512" time="293.9266666666667" type="appl" />
         <tli id="T513" time="294.4711111111111" type="appl" />
         <tli id="T514" time="295.0155555555556" type="appl" />
         <tli id="T515" time="295.4933323159847" />
         <tli id="T516" time="295.99825" type="appl" />
         <tli id="T517" time="296.4365" type="appl" />
         <tli id="T518" time="296.87475" type="appl" />
         <tli id="T519" time="297.40632684250966" />
         <tli id="T520" time="297.7392222222222" type="appl" />
         <tli id="T521" time="298.16544444444446" type="appl" />
         <tli id="T522" time="298.59166666666664" type="appl" />
         <tli id="T523" time="299.0178888888889" type="appl" />
         <tli id="T524" time="299.4441111111111" type="appl" />
         <tli id="T525" time="299.87033333333335" type="appl" />
         <tli id="T526" time="300.2965555555555" type="appl" />
         <tli id="T527" time="300.72277777777776" type="appl" />
         <tli id="T528" time="301.149" type="appl" />
         <tli id="T529" time="301.57522222222224" type="appl" />
         <tli id="T530" time="302.0014444444445" type="appl" />
         <tli id="T531" time="302.42766666666665" type="appl" />
         <tli id="T532" time="302.8538888888889" type="appl" />
         <tli id="T533" time="303.2801111111111" type="appl" />
         <tli id="T534" time="303.70633333333336" type="appl" />
         <tli id="T535" time="304.13255555555554" type="appl" />
         <tli id="T536" time="304.5587777777778" type="appl" />
         <tli id="T537" time="304.985" type="appl" />
         <tli id="T538" time="305.3379090909091" type="appl" />
         <tli id="T539" time="305.6908181818182" type="appl" />
         <tli id="T540" time="306.04372727272727" type="appl" />
         <tli id="T541" time="306.3966363636364" type="appl" />
         <tli id="T542" time="306.74954545454545" type="appl" />
         <tli id="T543" time="307.1024545454546" type="appl" />
         <tli id="T544" time="307.45536363636364" type="appl" />
         <tli id="T545" time="307.80827272727277" type="appl" />
         <tli id="T546" time="308.16118181818183" type="appl" />
         <tli id="T547" time="308.51409090909095" type="appl" />
         <tli id="T548" time="309.05366667250826" />
         <tli id="T549" time="309.4303" type="appl" />
         <tli id="T550" time="309.9936" type="appl" />
         <tli id="T551" time="310.55690000000004" type="appl" />
         <tli id="T552" time="311.1202" type="appl" />
         <tli id="T553" time="311.6835" type="appl" />
         <tli id="T554" time="312.2468" type="appl" />
         <tli id="T555" time="312.81010000000003" type="appl" />
         <tli id="T556" time="313.3734" type="appl" />
         <tli id="T557" time="313.9367" type="appl" />
         <tli id="T558" time="314.5" type="appl" />
         <tli id="T559" time="314.855" type="appl" />
         <tli id="T560" time="315.21" type="appl" />
         <tli id="T561" time="315.565" type="appl" />
         <tli id="T562" time="315.92" type="appl" />
         <tli id="T563" time="316.275" type="appl" />
         <tli id="T564" time="316.63" type="appl" />
         <tli id="T565" time="316.985" type="appl" />
         <tli id="T566" time="317.34000000000003" type="appl" />
         <tli id="T567" time="317.695" type="appl" />
         <tli id="T568" time="318.05" type="appl" />
         <tli id="T569" time="318.58625" type="appl" />
         <tli id="T570" time="319.1225" type="appl" />
         <tli id="T571" time="319.65875" type="appl" />
         <tli id="T572" time="320.195" type="appl" />
         <tli id="T573" time="320.73125" type="appl" />
         <tli id="T574" time="321.2675" type="appl" />
         <tli id="T575" time="321.80375" type="appl" />
         <tli id="T576" time="322.25332890553096" />
         <tli id="T577" time="322.78799999999995" type="appl" />
         <tli id="T578" time="323.236" type="appl" />
         <tli id="T579" time="323.68399999999997" type="appl" />
         <tli id="T580" time="324.132" type="appl" />
         <tli id="T581" time="324.58" type="appl" />
         <tli id="T582" time="325.0589545454545" type="appl" />
         <tli id="T583" time="325.53790909090907" type="appl" />
         <tli id="T584" time="326.0168636363636" type="appl" />
         <tli id="T585" time="326.49581818181815" type="appl" />
         <tli id="T586" time="326.9747727272727" type="appl" />
         <tli id="T587" time="327.4537272727273" type="appl" />
         <tli id="T588" time="327.93268181818183" type="appl" />
         <tli id="T589" time="328.4116363636364" type="appl" />
         <tli id="T590" time="328.8905909090909" type="appl" />
         <tli id="T591" time="329.36954545454546" type="appl" />
         <tli id="T592" time="329.8485" type="appl" />
         <tli id="T593" time="330.32745454545454" type="appl" />
         <tli id="T594" time="330.8064090909091" type="appl" />
         <tli id="T595" time="331.2853636363636" type="appl" />
         <tli id="T596" time="331.76431818181817" type="appl" />
         <tli id="T597" time="332.2432727272727" type="appl" />
         <tli id="T598" time="332.7222272727273" type="appl" />
         <tli id="T599" time="333.20118181818185" type="appl" />
         <tli id="T600" time="333.6801363636364" type="appl" />
         <tli id="T601" time="334.15909090909093" type="appl" />
         <tli id="T602" time="334.6380454545455" type="appl" />
         <tli id="T603" time="335.12367275055436" />
         <tli id="T604" time="335.61668421052633" type="appl" />
         <tli id="T605" time="336.11636842105264" type="appl" />
         <tli id="T606" time="336.61605263157895" type="appl" />
         <tli id="T607" time="337.11573684210526" type="appl" />
         <tli id="T608" time="337.6154210526316" type="appl" />
         <tli id="T609" time="338.1151052631579" type="appl" />
         <tli id="T610" time="338.6147894736842" type="appl" />
         <tli id="T611" time="339.1144736842105" type="appl" />
         <tli id="T612" time="339.6141578947368" type="appl" />
         <tli id="T613" time="340.1138421052632" type="appl" />
         <tli id="T614" time="340.6135263157895" type="appl" />
         <tli id="T615" time="341.1132105263158" type="appl" />
         <tli id="T616" time="341.6128947368421" type="appl" />
         <tli id="T617" time="342.11257894736843" type="appl" />
         <tli id="T618" time="342.61226315789474" type="appl" />
         <tli id="T619" time="343.11194736842106" type="appl" />
         <tli id="T620" time="343.61163157894737" type="appl" />
         <tli id="T621" time="344.1113157894737" type="appl" />
         <tli id="T622" time="344.6843225259592" />
         <tli id="T623" time="345.14119999999997" type="appl" />
         <tli id="T624" time="345.6714" type="appl" />
         <tli id="T625" time="346.2016" type="appl" />
         <tli id="T626" time="346.7318" type="appl" />
         <tli id="T627" time="347.262" type="appl" />
         <tli id="T628" time="347.7922" type="appl" />
         <tli id="T629" time="348.3224" type="appl" />
         <tli id="T630" time="348.8526" type="appl" />
         <tli id="T631" time="349.38280000000003" type="appl" />
         <tli id="T632" time="349.913" type="appl" />
         <tli id="T633" time="350.4432" type="appl" />
         <tli id="T634" time="350.9734" type="appl" />
         <tli id="T635" time="351.5036" type="appl" />
         <tli id="T636" time="352.03380000000004" type="appl" />
         <tli id="T637" time="352.5973387090249" />
         <tli id="T638" time="352.9726" type="appl" />
         <tli id="T639" time="353.38120000000004" type="appl" />
         <tli id="T640" time="353.7898" type="appl" />
         <tli id="T641" time="354.1984" type="appl" />
         <tli id="T642" time="354.60699999999997" type="appl" />
         <tli id="T643" time="355.0156" type="appl" />
         <tli id="T644" time="355.4242" type="appl" />
         <tli id="T645" time="355.83279999999996" type="appl" />
         <tli id="T646" time="356.2414" type="appl" />
         <tli id="T647" time="356.69665733667887" />
         <tli id="T648" time="357.1240909090909" type="appl" />
         <tli id="T649" time="357.5981818181818" type="appl" />
         <tli id="T650" time="358.0722727272727" type="appl" />
         <tli id="T651" time="358.54636363636365" type="appl" />
         <tli id="T652" time="359.0204545454545" type="appl" />
         <tli id="T653" time="359.49454545454546" type="appl" />
         <tli id="T654" time="359.96863636363634" type="appl" />
         <tli id="T655" time="360.44272727272727" type="appl" />
         <tli id="T656" time="360.9168181818182" type="appl" />
         <tli id="T657" time="361.3909090909091" type="appl" />
         <tli id="T658" time="361.91166374679835" />
         <tli id="T659" time="362.4625" type="appl" />
         <tli id="T660" time="363.06" type="appl" />
         <tli id="T661" time="363.6575" type="appl" />
         <tli id="T662" time="364.255" type="appl" />
         <tli id="T663" time="364.85249999999996" type="appl" />
         <tli id="T664" time="365.45" type="appl" />
         <tli id="T665" time="366.0475" type="appl" />
         <tli id="T666" time="366.645" type="appl" />
         <tli id="T667" time="367.24249999999995" type="appl" />
         <tli id="T668" time="368.0399909305945" />
         <tli id="T669" time="368.4554" type="appl" />
         <tli id="T670" time="369.07079999999996" type="appl" />
         <tli id="T671" time="369.6862" type="appl" />
         <tli id="T672" time="370.30159999999995" type="appl" />
         <tli id="T673" time="370.917" type="appl" />
         <tli id="T674" time="371.4102857142857" type="appl" />
         <tli id="T675" time="371.9035714285714" type="appl" />
         <tli id="T676" time="372.39685714285713" type="appl" />
         <tli id="T677" time="372.89014285714285" type="appl" />
         <tli id="T678" time="373.38342857142857" type="appl" />
         <tli id="T679" time="373.8767142857143" type="appl" />
         <tli id="T680" time="374.49667297397343" />
         <tli id="T681" time="374.85825" type="appl" />
         <tli id="T682" time="375.3465" type="appl" />
         <tli id="T683" time="375.83475" type="appl" />
         <tli id="T684" time="376.323" type="appl" />
         <tli id="T685" time="376.81125" type="appl" />
         <tli id="T686" time="377.29949999999997" type="appl" />
         <tli id="T687" time="377.78775" type="appl" />
         <tli id="T688" time="378.276" type="appl" />
         <tli id="T689" time="378.76425" type="appl" />
         <tli id="T690" time="379.2525" type="appl" />
         <tli id="T691" time="379.74075" type="appl" />
         <tli id="T692" time="380.229" type="appl" />
         <tli id="T693" time="380.93636363636364" type="appl" />
         <tli id="T694" time="381.64372727272723" type="appl" />
         <tli id="T695" time="382.3510909090909" type="appl" />
         <tli id="T696" time="383.05845454545454" type="appl" />
         <tli id="T697" time="383.7658181818182" type="appl" />
         <tli id="T698" time="384.4731818181818" type="appl" />
         <tli id="T699" time="385.18054545454544" type="appl" />
         <tli id="T700" time="385.8879090909091" type="appl" />
         <tli id="T701" time="386.59527272727274" type="appl" />
         <tli id="T702" time="387.30263636363634" type="appl" />
         <tli id="T703" time="388.2633326181864" />
         <tli id="T704" time="388.5385714285714" type="appl" />
         <tli id="T705" time="389.06714285714287" type="appl" />
         <tli id="T706" time="389.5957142857143" type="appl" />
         <tli id="T707" time="390.1242857142857" type="appl" />
         <tli id="T708" time="390.6528571428571" type="appl" />
         <tli id="T709" time="391.18142857142857" type="appl" />
         <tli id="T710" time="391.74333684576123" />
         <tli id="T711" time="392.4344444444444" type="appl" />
         <tli id="T712" time="393.1588888888889" type="appl" />
         <tli id="T713" time="393.8833333333333" type="appl" />
         <tli id="T714" time="394.60777777777776" type="appl" />
         <tli id="T715" time="395.33222222222224" type="appl" />
         <tli id="T716" time="396.0566666666667" type="appl" />
         <tli id="T717" time="396.7811111111111" type="appl" />
         <tli id="T718" time="397.5055555555556" type="appl" />
         <tli id="T719" time="398.25665873524946" />
         <tli id="T720" time="398.846" type="appl" />
         <tli id="T721" time="399.462" type="appl" />
         <tli id="T722" time="400.07800000000003" type="appl" />
         <tli id="T723" time="400.694" type="appl" />
         <tli id="T724" time="401.52999913812374" />
         <tli id="T725" time="401.8706666666667" type="appl" />
         <tli id="T726" time="402.4313333333333" type="appl" />
         <tli id="T727" time="402.99199999999996" type="appl" />
         <tli id="T728" time="403.55266666666665" type="appl" />
         <tli id="T729" time="404.11333333333334" type="appl" />
         <tli id="T730" time="404.80065728621673" />
         <tli id="T731" time="405.05062499999997" type="appl" />
         <tli id="T732" time="405.42724999999996" type="appl" />
         <tli id="T733" time="405.803875" type="appl" />
         <tli id="T734" time="406.1805" type="appl" />
         <tli id="T735" time="406.557125" type="appl" />
         <tli id="T736" time="406.93375" type="appl" />
         <tli id="T737" time="407.31037499999996" type="appl" />
         <tli id="T738" time="407.687" type="appl" />
         <tli id="T739" time="408.063625" type="appl" />
         <tli id="T740" time="408.44025" type="appl" />
         <tli id="T741" time="408.816875" type="appl" />
         <tli id="T742" time="409.1935" type="appl" />
         <tli id="T743" time="409.57012499999996" type="appl" />
         <tli id="T744" time="409.94675" type="appl" />
         <tli id="T745" time="410.323375" type="appl" />
         <tli id="T746" time="410.78000214376385" />
         <tli id="T747" time="411.1766666666667" type="appl" />
         <tli id="T748" time="411.6533333333333" type="appl" />
         <tli id="T749" time="412.1366761956067" />
         <tli id="T750" time="412.45666666666665" type="appl" />
         <tli id="T751" time="412.7833333333333" type="appl" />
         <tli id="T752" time="413.11" type="appl" />
         <tli id="T753" time="413.43666666666667" type="appl" />
         <tli id="T754" time="413.7633333333333" type="appl" />
         <tli id="T755" time="414.09000000000003" type="appl" />
         <tli id="T756" time="414.4166666666667" type="appl" />
         <tli id="T757" time="414.74333333333334" type="appl" />
         <tli id="T758" time="415.07" type="appl" />
         <tli id="T759" time="415.39666666666665" type="appl" />
         <tli id="T760" time="415.72333333333336" type="appl" />
         <tli id="T761" time="416.1033403974764" />
         <tli id="T762" time="416.3507142857143" type="appl" />
         <tli id="T763" time="416.6514285714286" type="appl" />
         <tli id="T764" time="416.95214285714286" type="appl" />
         <tli id="T765" time="417.2528571428571" type="appl" />
         <tli id="T766" time="417.5535714285714" type="appl" />
         <tli id="T767" time="417.8542857142857" type="appl" />
         <tli id="T768" time="418.2150040669753" />
         <tli id="T769" time="418.71549999999996" type="appl" />
         <tli id="T770" time="419.27599999999995" type="appl" />
         <tli id="T771" time="419.8365" type="appl" />
         <tli id="T772" time="420.397" type="appl" />
         <tli id="T773" time="420.9575" type="appl" />
         <tli id="T774" time="421.518" type="appl" />
         <tli id="T775" time="422.07849999999996" type="appl" />
         <tli id="T776" time="422.639" type="appl" />
         <tli id="T777" time="423.1995" type="appl" />
         <tli id="T778" time="423.70667333919386" />
         <tli id="T779" time="424.558" type="appl" />
         <tli id="T780" time="425.30933359165687" />
         <tli id="T781" time="425.9901538461538" type="appl" />
         <tli id="T782" time="426.6243076923077" type="appl" />
         <tli id="T783" time="427.2584615384615" type="appl" />
         <tli id="T784" time="427.8926153846154" type="appl" />
         <tli id="T785" time="428.5267692307692" type="appl" />
         <tli id="T786" time="429.1609230769231" type="appl" />
         <tli id="T787" time="429.7950769230769" type="appl" />
         <tli id="T788" time="430.4292307692308" type="appl" />
         <tli id="T789" time="431.0633846153846" type="appl" />
         <tli id="T790" time="431.6975384615385" type="appl" />
         <tli id="T791" time="432.3316923076923" type="appl" />
         <tli id="T792" time="432.9658461538462" type="appl" />
         <tli id="T793" time="433.5933342564116" />
         <tli id="T794" time="434.20458333333335" type="appl" />
         <tli id="T795" time="434.80916666666667" type="appl" />
         <tli id="T796" time="435.41375000000005" type="appl" />
         <tli id="T797" time="436.0183333333334" type="appl" />
         <tli id="T798" time="436.6229166666667" type="appl" />
         <tli id="T799" time="437.2275" type="appl" />
         <tli id="T800" time="437.83208333333334" type="appl" />
         <tli id="T801" time="438.43666666666667" type="appl" />
         <tli id="T802" time="439.04125" type="appl" />
         <tli id="T803" time="439.64583333333337" type="appl" />
         <tli id="T804" time="440.2504166666667" type="appl" />
         <tli id="T805" time="441.0616430132661" />
         <tli id="T806" time="441.51800000000003" type="appl" />
         <tli id="T807" time="442.18100000000004" type="appl" />
         <tli id="T808" time="442.844" type="appl" />
         <tli id="T809" time="443.507" type="appl" />
         <tli id="T810" time="444.2299848600426" />
         <tli id="T811" time="444.70736363636365" type="appl" />
         <tli id="T812" time="445.2447272727273" type="appl" />
         <tli id="T813" time="445.7820909090909" type="appl" />
         <tli id="T814" time="446.31945454545456" type="appl" />
         <tli id="T815" time="446.8568181818182" type="appl" />
         <tli id="T816" time="447.39418181818183" type="appl" />
         <tli id="T817" time="447.93154545454547" type="appl" />
         <tli id="T818" time="448.4689090909091" type="appl" />
         <tli id="T819" time="449.00627272727274" type="appl" />
         <tli id="T820" time="449.5436363636364" type="appl" />
         <tli id="T0" time="450.03000000000003" type="intp" />
         <tli id="T821" time="450.147663898929" />
         <tli id="T822" time="450.731" type="appl" />
         <tli id="T823" time="451.38100000000003" type="appl" />
         <tli id="T824" time="452.031" type="appl" />
         <tli id="T825" time="452.681" type="appl" />
         <tli id="T826" time="453.331" type="appl" />
         <tli id="T827" time="453.981" type="appl" />
         <tli id="T828" time="454.631" type="appl" />
         <tli id="T829" time="455.281" type="appl" />
         <tli id="T830" time="455.931" type="appl" />
         <tli id="T831" time="456.58099999999996" type="appl" />
         <tli id="T832" time="457.231" type="appl" />
         <tli id="T833" time="457.8943282029389" />
         <tli id="T834" time="458.3784" type="appl" />
         <tli id="T835" time="458.87579999999997" type="appl" />
         <tli id="T836" time="459.3732" type="appl" />
         <tli id="T837" time="459.87059999999997" type="appl" />
         <tli id="T838" time="460.368" type="appl" />
         <tli id="T839" time="460.918" type="appl" />
         <tli id="T840" time="461.468" type="appl" />
         <tli id="T841" time="462.018" type="appl" />
         <tli id="T842" time="462.568" type="appl" />
         <tli id="T843" time="463.118" type="appl" />
         <tli id="T844" time="463.668" type="appl" />
         <tli id="T845" time="464.218" type="appl" />
         <tli id="T846" time="464.768" type="appl" />
         <tli id="T847" time="465.318" type="appl" />
         <tli id="T848" time="465.868" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SuAA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T848" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Bɨlɨrgɨ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ogo</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">törüːre</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">togohogo</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_17" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Togohogo</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">ogoloro</ts>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_26" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ogoː</ts>
                  <nts id="Seg_29" n="HIAT:ip">)</nts>
                  <nts id="Seg_30" n="HIAT:ip">,</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">ɨ͡arakan</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">dʼaktar</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">törüːr</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">kemiger</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">bɨlɨrgɨ</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">üjetten</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">togohonu</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">belemneːččiler</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_58" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">Tuspa</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">uraha</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">dʼi͡eni</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">tutannar</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">belemneːččiler</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_76" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">Ogo</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">törüːr</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">dʼukaːta</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">töröːbüt</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">dʼukaːta</ts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">di͡eččiler</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">onton</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_102" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">Onno</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">bu͡ollagɨna</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">oːl</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">togohonu</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">oŋoroːččular</ts>
                  <nts id="Seg_117" n="HIAT:ip">,</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">kergettere</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">turu͡orallar</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_127" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">Ehete</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">duː</ts>
                  <nts id="Seg_133" n="HIAT:ip">,</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">agata</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">duː</ts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">inʼete</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">duː</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">kömölöhönnör</ts>
                  <nts id="Seg_150" n="HIAT:ip">,</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">baːjallar</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">togohonu</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">urahaga</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">kɨhajɨː</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">tu͡ora</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">mahɨ</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_172" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">Onton</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_178" n="HIAT:w" s="T49">e</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_182" n="HIAT:w" s="T50">togohotun</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_185" n="HIAT:w" s="T51">uhugugar</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_188" n="HIAT:w" s="T52">du͡o</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_191" n="HIAT:w" s="T53">končočču</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_194" n="HIAT:w" s="T54">annʼɨllan</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_197" n="HIAT:w" s="T55">turar</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_199" n="HIAT:ip">(</nts>
                  <ts e="T57" id="Seg_201" n="HIAT:w" s="T56">baha</ts>
                  <nts id="Seg_202" n="HIAT:ip">)</nts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_206" n="HIAT:w" s="T57">eː</ts>
                  <nts id="Seg_207" n="HIAT:ip">,</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_210" n="HIAT:w" s="T58">mas</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_213" n="HIAT:w" s="T59">bu͡olaːččɨ</ts>
                  <nts id="Seg_214" n="HIAT:ip">,</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_216" n="HIAT:ip">(</nts>
                  <ts e="T61" id="Seg_218" n="HIAT:w" s="T60">urahakaːta</ts>
                  <nts id="Seg_219" n="HIAT:ip">)</nts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_223" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_225" n="HIAT:w" s="T61">Oččogo</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_228" n="HIAT:w" s="T62">bu͡ollagɨna</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_231" n="HIAT:w" s="T63">ol</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_234" n="HIAT:w" s="T64">togohoŋ</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_237" n="HIAT:w" s="T65">bu͡o</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_240" n="HIAT:w" s="T66">tü͡öhün</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_243" n="HIAT:w" s="T67">kuraːgɨnan</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_246" n="HIAT:w" s="T68">bu͡olaːččɨ</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_250" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_252" n="HIAT:w" s="T69">Dʼaktar</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_255" n="HIAT:w" s="T70">onton</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_258" n="HIAT:w" s="T71">ɨjaːllan</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_261" n="HIAT:w" s="T72">oloron</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_264" n="HIAT:w" s="T73">ogolonoːčču</ts>
                  <nts id="Seg_265" n="HIAT:ip">.</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_268" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_270" n="HIAT:w" s="T74">Ol</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_273" n="HIAT:w" s="T75">ogolonorun</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_276" n="HIAT:w" s="T76">annɨtɨgar</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_279" n="HIAT:w" s="T77">du͡o</ts>
                  <nts id="Seg_280" n="HIAT:ip">,</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_283" n="HIAT:w" s="T78">eː</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_286" n="HIAT:w" s="T79">oloror</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_289" n="HIAT:w" s="T80">hiriger</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_292" n="HIAT:w" s="T81">lekeː</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_295" n="HIAT:w" s="T82">bu͡olaːččɨ</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_299" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_301" n="HIAT:w" s="T83">Dʼe</ts>
                  <nts id="Seg_302" n="HIAT:ip">,</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_305" n="HIAT:w" s="T84">ol</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_308" n="HIAT:w" s="T85">lekeːge</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_311" n="HIAT:w" s="T86">du͡o</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_314" n="HIAT:w" s="T87">ot</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_317" n="HIAT:w" s="T88">bu͡olaːččɨ</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_321" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_323" n="HIAT:w" s="T89">Otu</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_326" n="HIAT:w" s="T90">uːraːččɨlar</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_329" n="HIAT:w" s="T91">tuːmugar</ts>
                  <nts id="Seg_330" n="HIAT:ip">,</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_333" n="HIAT:w" s="T92">di͡enner</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_337" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_339" n="HIAT:w" s="T93">Onuga</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_342" n="HIAT:w" s="T94">du͡o</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_345" n="HIAT:w" s="T95">ogo</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_348" n="HIAT:w" s="T96">tüstegine</ts>
                  <nts id="Seg_349" n="HIAT:ip">,</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_351" n="HIAT:ip">"</nts>
                  <ts e="T98" id="Seg_353" n="HIAT:w" s="T97">otugar</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_356" n="HIAT:w" s="T98">tüher</ts>
                  <nts id="Seg_357" n="HIAT:ip">"</nts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_361" n="HIAT:w" s="T99">di͡eččiler</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_364" n="HIAT:w" s="T100">ol</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_367" n="HIAT:w" s="T101">ihin</ts>
                  <nts id="Seg_368" n="HIAT:ip">,</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_371" n="HIAT:w" s="T102">ottoːk</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_374" n="HIAT:w" s="T103">ottoːgun</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_377" n="HIAT:w" s="T104">ihin</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_381" n="HIAT:u" s="T105">
                  <nts id="Seg_382" n="HIAT:ip">"</nts>
                  <ts e="T106" id="Seg_384" n="HIAT:w" s="T105">Okko</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_387" n="HIAT:w" s="T106">tüspüt</ts>
                  <nts id="Seg_388" n="HIAT:ip">"</nts>
                  <nts id="Seg_389" n="HIAT:ip">,</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_392" n="HIAT:w" s="T107">di͡eččiler</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_396" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_398" n="HIAT:w" s="T108">Eː</ts>
                  <nts id="Seg_399" n="HIAT:ip">,</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_402" n="HIAT:w" s="T109">dʼaktar</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_405" n="HIAT:w" s="T110">ogolonorugar</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_407" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_409" n="HIAT:w" s="T111">ɨraː-</ts>
                  <nts id="Seg_410" n="HIAT:ip">)</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_413" n="HIAT:w" s="T112">bɨlɨrgɨ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_416" n="HIAT:w" s="T113">üjetten</ts>
                  <nts id="Seg_417" n="HIAT:ip">,</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_420" n="HIAT:w" s="T114">ɨraːkka</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_423" n="HIAT:w" s="T115">baːr</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_426" n="HIAT:w" s="T116">bu͡ollagɨna</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_429" n="HIAT:w" s="T117">ol</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_432" n="HIAT:w" s="T118">eː</ts>
                  <nts id="Seg_433" n="HIAT:ip">…</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_436" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_438" n="HIAT:w" s="T119">Baːbɨska</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_441" n="HIAT:w" s="T120">emeːksinnere</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_444" n="HIAT:w" s="T121">ɨraːk</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_447" n="HIAT:w" s="T122">baːr</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_449" n="HIAT:ip">(</nts>
                  <ts e="T124" id="Seg_451" n="HIAT:w" s="T123">bu͡ol-</ts>
                  <nts id="Seg_452" n="HIAT:ip">)</nts>
                  <nts id="Seg_453" n="HIAT:ip">,</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_456" n="HIAT:w" s="T124">ogo</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_459" n="HIAT:w" s="T125">törüː</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_462" n="HIAT:w" s="T126">tühüttere</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_465" n="HIAT:w" s="T127">dʼaktardara</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_468" n="HIAT:w" s="T128">ɨraːk</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_471" n="HIAT:w" s="T129">baːr</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_474" n="HIAT:w" s="T130">bu͡ollagɨna</ts>
                  <nts id="Seg_475" n="HIAT:ip">,</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_478" n="HIAT:w" s="T131">hɨrgalaːk</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_481" n="HIAT:w" s="T132">barannar</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_484" n="HIAT:w" s="T133">egeleːččiler</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_488" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_490" n="HIAT:w" s="T134">Kördöhönnör</ts>
                  <nts id="Seg_491" n="HIAT:ip">,</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_493" n="HIAT:ip">"</nts>
                  <ts e="T136" id="Seg_495" n="HIAT:w" s="T135">kaja</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_498" n="HIAT:w" s="T136">ogobut</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_501" n="HIAT:w" s="T137">oččogo</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_504" n="HIAT:w" s="T138">oččogo</ts>
                  <nts id="Seg_505" n="HIAT:ip">,</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_508" n="HIAT:w" s="T139">ol</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_511" n="HIAT:w" s="T140">kɨːspɨt</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_513" n="HIAT:ip">(</nts>
                  <ts e="T142" id="Seg_515" n="HIAT:w" s="T141">ü͡ö-</ts>
                  <nts id="Seg_516" n="HIAT:ip">)</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_519" n="HIAT:w" s="T142">törü͡öge</ts>
                  <nts id="Seg_520" n="HIAT:ip">"</nts>
                  <nts id="Seg_521" n="HIAT:ip">,</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_524" n="HIAT:w" s="T143">di͡enner</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_528" n="HIAT:u" s="T144">
                  <nts id="Seg_529" n="HIAT:ip">"</nts>
                  <ts e="T145" id="Seg_531" n="HIAT:w" s="T144">Dʼaktarbɨt</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_534" n="HIAT:w" s="T145">törü͡öge</ts>
                  <nts id="Seg_535" n="HIAT:ip">"</nts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_539" n="HIAT:w" s="T146">di͡enner</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_542" n="HIAT:w" s="T147">egeleːččiler</ts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_546" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_548" n="HIAT:w" s="T148">Eː</ts>
                  <nts id="Seg_549" n="HIAT:ip">,</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_552" n="HIAT:w" s="T149">čugas</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_555" n="HIAT:w" s="T150">baːr</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_558" n="HIAT:w" s="T151">bu͡ollagɨna</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_561" n="HIAT:w" s="T152">du͡o</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_564" n="HIAT:w" s="T153">ɨgɨraːččɨlar</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_567" n="HIAT:w" s="T154">ol</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_570" n="HIAT:w" s="T155">baːbɨska</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_573" n="HIAT:w" s="T156">emeːksini</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_577" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_579" n="HIAT:w" s="T157">Innʼe</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_582" n="HIAT:w" s="T158">gɨnan</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_585" n="HIAT:w" s="T159">ol</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_588" n="HIAT:w" s="T160">baːbɨska</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_591" n="HIAT:w" s="T161">iliː</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_594" n="HIAT:w" s="T162">hottoro</ts>
                  <nts id="Seg_595" n="HIAT:ip">,</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_598" n="HIAT:w" s="T163">tu͡oga</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_601" n="HIAT:w" s="T164">barɨta</ts>
                  <nts id="Seg_602" n="HIAT:ip">,</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_605" n="HIAT:w" s="T165">čeber</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_608" n="HIAT:w" s="T166">bu͡olu͡ogun</ts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_612" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_614" n="HIAT:w" s="T167">Höp</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_617" n="HIAT:w" s="T168">bu͡olaːččɨ</ts>
                  <nts id="Seg_618" n="HIAT:ip">,</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_621" n="HIAT:w" s="T169">barɨkaːna</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_624" n="HIAT:w" s="T170">haŋa</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_627" n="HIAT:w" s="T171">tutullubatak</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_630" n="HIAT:w" s="T172">ebi͡enne</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_633" n="HIAT:w" s="T173">bu͡olardaːk</ts>
                  <nts id="Seg_634" n="HIAT:ip">.</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_637" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_639" n="HIAT:w" s="T174">Hottoro</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_642" n="HIAT:w" s="T175">da</ts>
                  <nts id="Seg_643" n="HIAT:ip">,</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_646" n="HIAT:w" s="T176">iliː</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_649" n="HIAT:w" s="T177">hottoro</ts>
                  <nts id="Seg_650" n="HIAT:ip">,</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_653" n="HIAT:w" s="T178">ogonu</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_656" n="HIAT:w" s="T179">töröttörüger</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_658" n="HIAT:ip">(</nts>
                  <ts e="T181" id="Seg_660" n="HIAT:w" s="T180">tabɨskɨː-</ts>
                  <nts id="Seg_661" n="HIAT:ip">)</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_664" n="HIAT:w" s="T181">dʼaktar</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_667" n="HIAT:w" s="T182">tabɨskɨːrɨgar</ts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_671" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_673" n="HIAT:w" s="T183">Oččogo</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_676" n="HIAT:w" s="T184">bu͡ollagɨna</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_679" n="HIAT:w" s="T185">du͡o</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_682" n="HIAT:w" s="T186">ajɨːhɨttaːk</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_685" n="HIAT:w" s="T187">bu͡olaːččɨ</ts>
                  <nts id="Seg_686" n="HIAT:ip">.</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_689" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_691" n="HIAT:w" s="T188">Ol</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_694" n="HIAT:w" s="T189">ajɨːhɨtɨ</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_697" n="HIAT:w" s="T190">bu͡ollagɨna</ts>
                  <nts id="Seg_698" n="HIAT:ip">,</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_701" n="HIAT:w" s="T191">maŋnaj</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_704" n="HIAT:w" s="T192">ɨla</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_707" n="HIAT:w" s="T193">kele</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_710" n="HIAT:w" s="T194">keleller</ts>
                  <nts id="Seg_711" n="HIAT:ip">,</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_714" n="HIAT:w" s="T195">dʼaktardara</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_717" n="HIAT:w" s="T196">töröːrü</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_720" n="HIAT:w" s="T197">gɨnnagɨna</ts>
                  <nts id="Seg_721" n="HIAT:ip">,</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_724" n="HIAT:w" s="T198">ol</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_727" n="HIAT:w" s="T199">tuspa</ts>
                  <nts id="Seg_728" n="HIAT:ip">,</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_731" n="HIAT:w" s="T200">ol</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_734" n="HIAT:w" s="T201">tuspa</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_737" n="HIAT:w" s="T202">dʼukaːga</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_740" n="HIAT:w" s="T203">kimi</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_743" n="HIAT:w" s="T204">da</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_746" n="HIAT:w" s="T205">kiːllerbetter</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_749" n="HIAT:w" s="T206">bu͡ollaga</ts>
                  <nts id="Seg_750" n="HIAT:ip">.</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_753" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_755" n="HIAT:w" s="T207">Kim</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_758" n="HIAT:w" s="T208">tuspa</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_761" n="HIAT:w" s="T209">dʼukaː</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_764" n="HIAT:w" s="T210">tuppattar</ts>
                  <nts id="Seg_765" n="HIAT:ip">,</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_768" n="HIAT:w" s="T211">dʼi͡ege</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_771" n="HIAT:w" s="T212">törötöːččüler</ts>
                  <nts id="Seg_772" n="HIAT:ip">,</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_775" n="HIAT:w" s="T213">dʼi͡ege</ts>
                  <nts id="Seg_776" n="HIAT:ip">,</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_779" n="HIAT:w" s="T214">oloror</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_782" n="HIAT:w" s="T215">dʼukaːlarɨgar</ts>
                  <nts id="Seg_783" n="HIAT:ip">.</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_786" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_788" n="HIAT:w" s="T216">Ol</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_791" n="HIAT:w" s="T217">gɨnan</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_794" n="HIAT:w" s="T218">baran</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_797" n="HIAT:w" s="T219">oloror</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_800" n="HIAT:w" s="T220">dʼukaːlarɨttan</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_803" n="HIAT:w" s="T221">tuspa</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_806" n="HIAT:w" s="T222">oŋoroːččular</ts>
                  <nts id="Seg_807" n="HIAT:ip">,</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_810" n="HIAT:w" s="T223">togohonu</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_813" n="HIAT:w" s="T224">turu͡oraːččɨlar</ts>
                  <nts id="Seg_814" n="HIAT:ip">.</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_817" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_819" n="HIAT:w" s="T225">Če</ts>
                  <nts id="Seg_820" n="HIAT:ip">,</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_823" n="HIAT:w" s="T226">ol</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_826" n="HIAT:w" s="T227">gɨnan</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_829" n="HIAT:w" s="T228">kihini</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_832" n="HIAT:w" s="T229">barɨkaːnɨn</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_835" n="HIAT:w" s="T230">taharaːččɨlar</ts>
                  <nts id="Seg_836" n="HIAT:ip">.</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_839" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_841" n="HIAT:w" s="T231">Kim</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_844" n="HIAT:w" s="T232">da</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_847" n="HIAT:w" s="T233">hu͡ok</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_850" n="HIAT:w" s="T234">bu͡olu͡oktaːk</ts>
                  <nts id="Seg_851" n="HIAT:ip">,</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_854" n="HIAT:w" s="T235">hogotok</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_857" n="HIAT:w" s="T236">baːbuskanɨ</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_860" n="HIAT:w" s="T237">gɨtta</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_863" n="HIAT:w" s="T238">törüːr</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_866" n="HIAT:w" s="T239">dʼaktar</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_869" n="HIAT:w" s="T240">ere</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_872" n="HIAT:w" s="T241">bu͡olu͡oktaːktar</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_875" n="HIAT:w" s="T242">onno</ts>
                  <nts id="Seg_876" n="HIAT:ip">.</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_879" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_881" n="HIAT:w" s="T243">Oččogo</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_884" n="HIAT:w" s="T244">bu͡ollagɨna</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_887" n="HIAT:w" s="T245">du͡o</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_890" n="HIAT:w" s="T246">bu</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_893" n="HIAT:w" s="T247">baːbuskaŋ</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_896" n="HIAT:w" s="T248">keler</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_899" n="HIAT:w" s="T249">uraha</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_902" n="HIAT:w" s="T250">dʼi͡ege</ts>
                  <nts id="Seg_903" n="HIAT:ip">,</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_906" n="HIAT:w" s="T251">törüːr</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_909" n="HIAT:w" s="T252">dʼi͡ege</ts>
                  <nts id="Seg_910" n="HIAT:ip">,</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_913" n="HIAT:w" s="T253">dʼukaːga</ts>
                  <nts id="Seg_914" n="HIAT:ip">.</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_917" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_919" n="HIAT:w" s="T254">Tuspa</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_922" n="HIAT:w" s="T255">tutullubut</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_925" n="HIAT:w" s="T256">bu͡ollagɨna</ts>
                  <nts id="Seg_926" n="HIAT:ip">,</nts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_929" n="HIAT:w" s="T257">duː</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_932" n="HIAT:w" s="T258">dʼi͡ege</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_935" n="HIAT:w" s="T259">ba</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_938" n="HIAT:w" s="T260">oŋohullubut</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_941" n="HIAT:w" s="T261">bu͡ollagɨna</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_945" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_947" n="HIAT:w" s="T262">Ol</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_950" n="HIAT:w" s="T263">kelen</ts>
                  <nts id="Seg_951" n="HIAT:ip">,</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_954" n="HIAT:w" s="T264">ke</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_957" n="HIAT:w" s="T265">egelen</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_960" n="HIAT:w" s="T266">kelenner</ts>
                  <nts id="Seg_961" n="HIAT:ip">,</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_964" n="HIAT:w" s="T267">ol</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_967" n="HIAT:w" s="T268">baːbuska</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_970" n="HIAT:w" s="T269">emeːksine</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_973" n="HIAT:w" s="T270">kiːrdegine</ts>
                  <nts id="Seg_974" n="HIAT:ip">,</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_977" n="HIAT:w" s="T271">onu</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_980" n="HIAT:w" s="T272">barɨkaːnɨ</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_983" n="HIAT:w" s="T273">jaːntardaːn</ts>
                  <nts id="Seg_984" n="HIAT:ip">.</nts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_987" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_989" n="HIAT:w" s="T274">Jaːntarɨnan</ts>
                  <nts id="Seg_990" n="HIAT:ip">,</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_993" n="HIAT:w" s="T275">tɨmtɨgɨnan</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_996" n="HIAT:w" s="T276">ɨbatan</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_999" n="HIAT:w" s="T277">barɨkaːnɨn</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1002" n="HIAT:w" s="T278">ɨraːstɨːr</ts>
                  <nts id="Seg_1003" n="HIAT:ip">.</nts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1006" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1008" n="HIAT:w" s="T279">Ol</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1011" n="HIAT:w" s="T280">dʼaktar</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1014" n="HIAT:w" s="T281">oloror</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1017" n="HIAT:w" s="T282">hirin</ts>
                  <nts id="Seg_1018" n="HIAT:ip">,</nts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1021" n="HIAT:w" s="T283">oloror</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1024" n="HIAT:w" s="T284">dʼukaːtɨn</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1027" n="HIAT:w" s="T285">tögürüččü</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1030" n="HIAT:w" s="T286">alastɨːr</ts>
                  <nts id="Seg_1031" n="HIAT:ip">:</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1034" n="HIAT:u" s="T287">
                  <nts id="Seg_1035" n="HIAT:ip">"</nts>
                  <ts e="T288" id="Seg_1037" n="HIAT:w" s="T287">Türkeːt</ts>
                  <nts id="Seg_1038" n="HIAT:ip">,</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1041" n="HIAT:w" s="T288">türkeːt</ts>
                  <nts id="Seg_1042" n="HIAT:ip">,</nts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1045" n="HIAT:w" s="T289">türkeːt</ts>
                  <nts id="Seg_1046" n="HIAT:ip">,</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1049" n="HIAT:w" s="T290">türkeːt</ts>
                  <nts id="Seg_1050" n="HIAT:ip">,</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1053" n="HIAT:w" s="T291">bɨrtagɨŋ</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1056" n="HIAT:w" s="T292">bɨlɨt</ts>
                  <nts id="Seg_1057" n="HIAT:ip">,</nts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1060" n="HIAT:w" s="T293">kɨraːhɨŋ</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1063" n="HIAT:w" s="T294">ɨjga</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1067" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1069" n="HIAT:w" s="T295">Türkeːt</ts>
                  <nts id="Seg_1070" n="HIAT:ip">,</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1073" n="HIAT:w" s="T296">türkeːt</ts>
                  <nts id="Seg_1074" n="HIAT:ip">,</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1077" n="HIAT:w" s="T297">türkeːt</ts>
                  <nts id="Seg_1078" n="HIAT:ip">,</nts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1081" n="HIAT:w" s="T298">bɨrtagɨŋ</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1084" n="HIAT:w" s="T299">bulat</ts>
                  <nts id="Seg_1085" n="HIAT:ip">,</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1088" n="HIAT:w" s="T300">kɨraːhɨŋ</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1091" n="HIAT:w" s="T301">ɨjga</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1094" n="HIAT:w" s="T302">tu͡omuŋ-sɨramuːŋ</ts>
                  <nts id="Seg_1095" n="HIAT:ip">,</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1098" n="HIAT:w" s="T303">tu͡omuŋ-sɨramuŋ</ts>
                  <nts id="Seg_1099" n="HIAT:ip">,</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1102" n="HIAT:w" s="T304">tu͡omuŋ-sɨramuːŋ</ts>
                  <nts id="Seg_1103" n="HIAT:ip">,</nts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1106" n="HIAT:w" s="T305">tu͡omuŋ-sɨramuŋ</ts>
                  <nts id="Seg_1107" n="HIAT:ip">!</nts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1110" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1112" n="HIAT:w" s="T306">Türkeːt</ts>
                  <nts id="Seg_1113" n="HIAT:ip">,</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1116" n="HIAT:w" s="T307">bɨrtagɨŋ</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1119" n="HIAT:w" s="T308">bulat</ts>
                  <nts id="Seg_1120" n="HIAT:ip">,</nts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1123" n="HIAT:w" s="T309">kɨraːhɨŋ</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1126" n="HIAT:w" s="T310">ɨjga</ts>
                  <nts id="Seg_1127" n="HIAT:ip">.</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T318" id="Seg_1130" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1132" n="HIAT:w" s="T311">Tu͡omuŋ-sɨramuːŋ</ts>
                  <nts id="Seg_1133" n="HIAT:ip">,</nts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1136" n="HIAT:w" s="T312">tu͡omuŋ-sɨramuŋ</ts>
                  <nts id="Seg_1137" n="HIAT:ip">"</nts>
                  <nts id="Seg_1138" n="HIAT:ip">,</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1141" n="HIAT:w" s="T313">diː-diː</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1144" n="HIAT:w" s="T314">alastɨːr</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1147" n="HIAT:w" s="T315">tögürüččü</ts>
                  <nts id="Seg_1148" n="HIAT:ip">,</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1151" n="HIAT:w" s="T316">üste</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1154" n="HIAT:w" s="T317">kat</ts>
                  <nts id="Seg_1155" n="HIAT:ip">.</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1158" n="HIAT:u" s="T318">
                  <ts e="T319" id="Seg_1160" n="HIAT:w" s="T318">Onton</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1163" n="HIAT:w" s="T319">dʼaktarɨ</ts>
                  <nts id="Seg_1164" n="HIAT:ip">,</nts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1167" n="HIAT:w" s="T320">oloror</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1170" n="HIAT:w" s="T321">dʼaktarɨ</ts>
                  <nts id="Seg_1171" n="HIAT:ip">,</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1174" n="HIAT:w" s="T322">oloror</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1177" n="HIAT:w" s="T323">iligine</ts>
                  <nts id="Seg_1178" n="HIAT:ip">,</nts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1181" n="HIAT:w" s="T324">konnogun</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1184" n="HIAT:w" s="T325">annɨnan</ts>
                  <nts id="Seg_1185" n="HIAT:ip">,</nts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1188" n="HIAT:w" s="T326">dʼogustuk</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1191" n="HIAT:w" s="T327">oloru͡ogun</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1194" n="HIAT:w" s="T328">di͡en</ts>
                  <nts id="Seg_1195" n="HIAT:ip">,</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1198" n="HIAT:w" s="T329">alastɨːr</ts>
                  <nts id="Seg_1199" n="HIAT:ip">.</nts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1202" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1204" n="HIAT:w" s="T330">Hol</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1207" n="HIAT:w" s="T331">alastɨːr</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1210" n="HIAT:w" s="T332">ebi͡ennetinen</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1213" n="HIAT:w" s="T333">tögürüččü</ts>
                  <nts id="Seg_1214" n="HIAT:ip">,</nts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1217" n="HIAT:w" s="T334">kün</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1220" n="HIAT:w" s="T335">kotu</ts>
                  <nts id="Seg_1221" n="HIAT:ip">.</nts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1224" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1226" n="HIAT:w" s="T336">Kennitiger</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1229" n="HIAT:w" s="T337">barbat</ts>
                  <nts id="Seg_1230" n="HIAT:ip">.</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1233" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1235" n="HIAT:w" s="T338">Kennitin</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1238" n="HIAT:w" s="T339">di͡ek</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1241" n="HIAT:w" s="T340">kimi</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1244" n="HIAT:w" s="T341">da</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1247" n="HIAT:w" s="T342">kaːmtaraːččɨta</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1250" n="HIAT:w" s="T343">hu͡oktar</ts>
                  <nts id="Seg_1251" n="HIAT:ip">,</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1254" n="HIAT:w" s="T344">ɨ͡arakan</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1257" n="HIAT:w" s="T345">dʼaktarɨ</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1260" n="HIAT:w" s="T346">törüt</ts>
                  <nts id="Seg_1261" n="HIAT:ip">.</nts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1264" n="HIAT:u" s="T347">
                  <nts id="Seg_1265" n="HIAT:ip">"</nts>
                  <ts e="T348" id="Seg_1267" n="HIAT:w" s="T347">Kennili͡ege</ts>
                  <nts id="Seg_1268" n="HIAT:ip">"</nts>
                  <nts id="Seg_1269" n="HIAT:ip">,</nts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1272" n="HIAT:w" s="T348">di͡enner</ts>
                  <nts id="Seg_1273" n="HIAT:ip">,</nts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1276" n="HIAT:w" s="T349">ol</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1279" n="HIAT:w" s="T350">ɨjaːk</ts>
                  <nts id="Seg_1280" n="HIAT:ip">.</nts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1283" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1285" n="HIAT:w" s="T351">Oččogo</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1288" n="HIAT:w" s="T352">bu͡ollagɨna</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1291" n="HIAT:w" s="T353">du͡o</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1294" n="HIAT:w" s="T354">bu</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1296" n="HIAT:ip">(</nts>
                  <ts e="T356" id="Seg_1298" n="HIAT:w" s="T355">ɨra-</ts>
                  <nts id="Seg_1299" n="HIAT:ip">)</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1302" n="HIAT:w" s="T356">üste</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1305" n="HIAT:w" s="T357">kat</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1308" n="HIAT:w" s="T358">ergiter</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1311" n="HIAT:w" s="T359">emi͡e</ts>
                  <nts id="Seg_1312" n="HIAT:ip">:</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1315" n="HIAT:u" s="T360">
                  <nts id="Seg_1316" n="HIAT:ip">"</nts>
                  <ts e="T361" id="Seg_1318" n="HIAT:w" s="T360">Türkeːt</ts>
                  <nts id="Seg_1319" n="HIAT:ip">,</nts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1322" n="HIAT:w" s="T361">türkeːt</ts>
                  <nts id="Seg_1323" n="HIAT:ip">,</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1326" n="HIAT:w" s="T362">türkeːt</ts>
                  <nts id="Seg_1327" n="HIAT:ip">,</nts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1330" n="HIAT:w" s="T363">eteŋŋe</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1333" n="HIAT:w" s="T364">oloru͡ogun</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1336" n="HIAT:w" s="T365">kɨːhɨm</ts>
                  <nts id="Seg_1337" n="HIAT:ip">,</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1340" n="HIAT:w" s="T366">ogom</ts>
                  <nts id="Seg_1341" n="HIAT:ip">.</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1344" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1346" n="HIAT:w" s="T367">Türkeːt</ts>
                  <nts id="Seg_1347" n="HIAT:ip">,</nts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1350" n="HIAT:w" s="T368">türkeːt</ts>
                  <nts id="Seg_1351" n="HIAT:ip">,</nts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1354" n="HIAT:w" s="T369">türkeːt</ts>
                  <nts id="Seg_1355" n="HIAT:ip">,</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1358" n="HIAT:w" s="T370">eteŋŋe</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1361" n="HIAT:w" s="T371">oloru͡ogun</ts>
                  <nts id="Seg_1362" n="HIAT:ip">,</nts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1365" n="HIAT:w" s="T372">tu͡omuŋ-suramuŋ</ts>
                  <nts id="Seg_1366" n="HIAT:ip">.</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_1369" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1371" n="HIAT:w" s="T373">Pürkeːt</ts>
                  <nts id="Seg_1372" n="HIAT:ip">,</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1375" n="HIAT:w" s="T374">pürket</ts>
                  <nts id="Seg_1376" n="HIAT:ip">,</nts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1379" n="HIAT:w" s="T375">pürkeːt</ts>
                  <nts id="Seg_1380" n="HIAT:ip">,</nts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1383" n="HIAT:w" s="T376">ɨraːhɨŋ</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1386" n="HIAT:w" s="T377">ɨjga</ts>
                  <nts id="Seg_1387" n="HIAT:ip">,</nts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1390" n="HIAT:w" s="T378">bɨrtagɨŋ</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1393" n="HIAT:w" s="T379">bɨlɨkka</ts>
                  <nts id="Seg_1394" n="HIAT:ip">.</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1397" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_1399" n="HIAT:w" s="T380">ɨraːhɨŋ</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1402" n="HIAT:w" s="T381">ɨjga</ts>
                  <nts id="Seg_1403" n="HIAT:ip">,</nts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1406" n="HIAT:w" s="T382">bɨrtagɨŋ</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1409" n="HIAT:w" s="T383">bɨlɨkka</ts>
                  <nts id="Seg_1410" n="HIAT:ip">,</nts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1413" n="HIAT:w" s="T384">tu͡omuŋ-sɨramuːŋ</ts>
                  <nts id="Seg_1414" n="HIAT:ip">,</nts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1417" n="HIAT:w" s="T385">tu͡omuŋ-sɨramuːŋ</ts>
                  <nts id="Seg_1418" n="HIAT:ip">"</nts>
                  <nts id="Seg_1419" n="HIAT:ip">,</nts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1422" n="HIAT:w" s="T386">di͡eččiler</ts>
                  <nts id="Seg_1423" n="HIAT:ip">,</nts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1426" n="HIAT:w" s="T387">üste</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1429" n="HIAT:w" s="T388">kat</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1432" n="HIAT:w" s="T389">ergiteːččiler</ts>
                  <nts id="Seg_1433" n="HIAT:ip">.</nts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1436" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1438" n="HIAT:w" s="T390">Ol</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1441" n="HIAT:w" s="T391">gɨnan</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1444" n="HIAT:w" s="T392">togohotuttan</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1447" n="HIAT:w" s="T393">tuttar</ts>
                  <nts id="Seg_1448" n="HIAT:ip">,</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1451" n="HIAT:w" s="T394">ol</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1454" n="HIAT:w" s="T395">oloror</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1457" n="HIAT:w" s="T396">dʼaktarɨŋ</ts>
                  <nts id="Seg_1458" n="HIAT:ip">,</nts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1461" n="HIAT:w" s="T397">oloror</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1464" n="HIAT:w" s="T398">togohotuttan</ts>
                  <nts id="Seg_1465" n="HIAT:ip">.</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1468" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1470" n="HIAT:w" s="T399">Oččogo</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1473" n="HIAT:w" s="T400">bu͡ollagɨna</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1476" n="HIAT:w" s="T401">ajɨːhɨt</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1479" n="HIAT:w" s="T402">ɨlar</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1482" n="HIAT:w" s="T403">baːbɨska</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1485" n="HIAT:w" s="T404">emeːksin</ts>
                  <nts id="Seg_1486" n="HIAT:ip">,</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1489" n="HIAT:w" s="T405">ol</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1492" n="HIAT:w" s="T406">ajɨːhɨtɨn</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1495" n="HIAT:w" s="T407">ɨlar</ts>
                  <nts id="Seg_1496" n="HIAT:ip">,</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1499" n="HIAT:w" s="T408">uskaːn</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1502" n="HIAT:w" s="T409">tiriːte</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1505" n="HIAT:w" s="T410">ol</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1508" n="HIAT:w" s="T411">ajɨːhɨtkaːna</ts>
                  <nts id="Seg_1509" n="HIAT:ip">.</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1512" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1514" n="HIAT:w" s="T412">Uskaːnɨŋ</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1517" n="HIAT:w" s="T413">ajɨːhɨt</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1520" n="HIAT:w" s="T414">aːttaːk</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1523" n="HIAT:w" s="T415">bu͡olaːččɨ</ts>
                  <nts id="Seg_1524" n="HIAT:ip">.</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1527" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1529" n="HIAT:w" s="T416">Oččogo</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1532" n="HIAT:w" s="T417">uskaːnɨ</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1535" n="HIAT:w" s="T418">tutan</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1538" n="HIAT:w" s="T419">baran</ts>
                  <nts id="Seg_1539" n="HIAT:ip">,</nts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1542" n="HIAT:w" s="T420">uskaːnɨ</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1545" n="HIAT:w" s="T421">u͡okka</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1548" n="HIAT:w" s="T422">ahatar</ts>
                  <nts id="Seg_1549" n="HIAT:ip">,</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1552" n="HIAT:w" s="T423">u͡otu</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1555" n="HIAT:w" s="T424">emi͡e</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1558" n="HIAT:w" s="T425">ahatar</ts>
                  <nts id="Seg_1559" n="HIAT:ip">.</nts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1562" n="HIAT:u" s="T426">
                  <nts id="Seg_1563" n="HIAT:ip">"</nts>
                  <ts e="T427" id="Seg_1565" n="HIAT:w" s="T426">U͡ot-ehekeːniːm</ts>
                  <nts id="Seg_1566" n="HIAT:ip">,</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1569" n="HIAT:w" s="T427">ajɨːhɨppɨn</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1572" n="HIAT:w" s="T428">ahatabɨn</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1575" n="HIAT:w" s="T429">ogobun</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1578" n="HIAT:w" s="T430">eteŋŋe</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1581" n="HIAT:w" s="T431">olordu͡ogun</ts>
                  <nts id="Seg_1582" n="HIAT:ip">,</nts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1585" n="HIAT:w" s="T432">eteŋŋe</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1588" n="HIAT:w" s="T433">tabɨskɨ͡agɨn</ts>
                  <nts id="Seg_1589" n="HIAT:ip">"</nts>
                  <nts id="Seg_1590" n="HIAT:ip">,</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1593" n="HIAT:w" s="T434">di͡en</ts>
                  <nts id="Seg_1594" n="HIAT:ip">.</nts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1597" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1599" n="HIAT:w" s="T435">Oččogo</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1602" n="HIAT:w" s="T436">bu͡ollagɨna</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1605" n="HIAT:w" s="T437">u͡ot</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1608" n="HIAT:w" s="T438">ehekeːmmin</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1611" n="HIAT:w" s="T439">ahatabɨn</ts>
                  <nts id="Seg_1612" n="HIAT:ip">:</nts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1615" n="HIAT:u" s="T440">
                  <nts id="Seg_1616" n="HIAT:ip">"</nts>
                  <ts e="T441" id="Seg_1618" n="HIAT:w" s="T440">U͡ot</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1621" n="HIAT:w" s="T441">ehekeːniːm</ts>
                  <nts id="Seg_1622" n="HIAT:ip">,</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1625" n="HIAT:w" s="T442">ahaː</ts>
                  <nts id="Seg_1626" n="HIAT:ip">,</nts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1629" n="HIAT:w" s="T443">eteŋŋe</ts>
                  <nts id="Seg_1630" n="HIAT:ip">,</nts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1633" n="HIAT:w" s="T444">dʼollo</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1636" n="HIAT:w" s="T445">keteː</ts>
                  <nts id="Seg_1637" n="HIAT:ip">.</nts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1640" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1642" n="HIAT:w" s="T446">Tu͡ok</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1645" n="HIAT:w" s="T447">da</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1648" n="HIAT:w" s="T448">kuhaganɨ</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1651" n="HIAT:w" s="T449">hugahaːtɨma</ts>
                  <nts id="Seg_1652" n="HIAT:ip">,</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1655" n="HIAT:w" s="T450">eteŋŋe</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1658" n="HIAT:w" s="T451">olort</ts>
                  <nts id="Seg_1659" n="HIAT:ip">,</nts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1662" n="HIAT:w" s="T452">ogobun</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1665" n="HIAT:w" s="T453">eteŋŋe</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1668" n="HIAT:w" s="T454">boskoloː</ts>
                  <nts id="Seg_1669" n="HIAT:ip">"</nts>
                  <nts id="Seg_1670" n="HIAT:ip">,</nts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1673" n="HIAT:w" s="T455">di͡en</ts>
                  <nts id="Seg_1674" n="HIAT:ip">.</nts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_1677" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1679" n="HIAT:w" s="T456">Onton</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1682" n="HIAT:w" s="T457">ol</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1685" n="HIAT:w" s="T458">ajɨːhɨkkaːnɨn</ts>
                  <nts id="Seg_1686" n="HIAT:ip">,</nts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1689" n="HIAT:w" s="T459">ahatan</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1692" n="HIAT:w" s="T460">baraːn</ts>
                  <nts id="Seg_1693" n="HIAT:ip">,</nts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1696" n="HIAT:w" s="T461">togohogo</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1699" n="HIAT:w" s="T462">ɨjɨːr</ts>
                  <nts id="Seg_1700" n="HIAT:ip">.</nts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1703" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_1705" n="HIAT:w" s="T463">Ol</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1708" n="HIAT:w" s="T464">togohogo</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1711" n="HIAT:w" s="T465">baːjan</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1714" n="HIAT:w" s="T466">keːher</ts>
                  <nts id="Seg_1715" n="HIAT:ip">.</nts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_1718" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1720" n="HIAT:w" s="T467">Ol</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1723" n="HIAT:w" s="T468">baːjan</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1726" n="HIAT:w" s="T469">keːhen</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1729" n="HIAT:w" s="T470">baraːn</ts>
                  <nts id="Seg_1730" n="HIAT:ip">,</nts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1733" n="HIAT:w" s="T471">ol</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1736" n="HIAT:w" s="T472">ogolonor</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1739" n="HIAT:w" s="T473">dʼaktar</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1742" n="HIAT:w" s="T474">kɨ͡ajan</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1745" n="HIAT:w" s="T475">ol</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1748" n="HIAT:w" s="T476">kuhagannɨk</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1750" n="HIAT:ip">(</nts>
                  <ts e="T478" id="Seg_1752" n="HIAT:w" s="T477">üːnner</ts>
                  <nts id="Seg_1753" n="HIAT:ip">)</nts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1756" n="HIAT:w" s="T478">bu͡ollagɨna</ts>
                  <nts id="Seg_1757" n="HIAT:ip">,</nts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1760" n="HIAT:w" s="T479">kömölöhör</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1763" n="HIAT:w" s="T480">baːbɨska</ts>
                  <nts id="Seg_1764" n="HIAT:ip">.</nts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1767" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1769" n="HIAT:w" s="T481">Tobugugar</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1772" n="HIAT:w" s="T482">olordor</ts>
                  <nts id="Seg_1773" n="HIAT:ip">,</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1775" n="HIAT:ip">(</nts>
                  <ts e="T484" id="Seg_1777" n="HIAT:w" s="T483">kuturugun</ts>
                  <nts id="Seg_1778" n="HIAT:ip">)</nts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1781" n="HIAT:w" s="T484">tördütünen</ts>
                  <nts id="Seg_1782" n="HIAT:ip">,</nts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1785" n="HIAT:w" s="T485">ogoto</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1788" n="HIAT:w" s="T486">kennileːmi͡egin</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1791" n="HIAT:w" s="T487">di͡en</ts>
                  <nts id="Seg_1792" n="HIAT:ip">.</nts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1795" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_1797" n="HIAT:w" s="T488">Onton</ts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1800" n="HIAT:w" s="T489">bɨ͡arɨn</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1803" n="HIAT:w" s="T490">ilbijer</ts>
                  <nts id="Seg_1804" n="HIAT:ip">.</nts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_1807" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1809" n="HIAT:w" s="T491">Ol</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1812" n="HIAT:w" s="T492">bɨ͡arɨn</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1815" n="HIAT:w" s="T493">ilbijen</ts>
                  <nts id="Seg_1816" n="HIAT:ip">,</nts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1819" n="HIAT:w" s="T494">kömölöhön</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1822" n="HIAT:w" s="T495">ol</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1825" n="HIAT:w" s="T496">ogotun</ts>
                  <nts id="Seg_1826" n="HIAT:ip">,</nts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1829" n="HIAT:w" s="T497">kömölöhön</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1832" n="HIAT:w" s="T498">tabɨskɨːr</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1835" n="HIAT:w" s="T499">ol</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1838" n="HIAT:w" s="T500">dʼaktar</ts>
                  <nts id="Seg_1839" n="HIAT:ip">.</nts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_1842" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1844" n="HIAT:w" s="T501">Ol</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1847" n="HIAT:w" s="T502">tabɨskɨːr</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1850" n="HIAT:w" s="T503">aːta</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1853" n="HIAT:w" s="T504">ogo</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1856" n="HIAT:w" s="T505">törüːre</ts>
                  <nts id="Seg_1857" n="HIAT:ip">.</nts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T515" id="Seg_1860" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_1862" n="HIAT:w" s="T506">Ol</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1865" n="HIAT:w" s="T507">tabɨskaːn</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1868" n="HIAT:w" s="T508">ogo</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1871" n="HIAT:w" s="T509">tüspütüger</ts>
                  <nts id="Seg_1872" n="HIAT:ip">,</nts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1875" n="HIAT:w" s="T510">ol</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1878" n="HIAT:w" s="T511">ogonu</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1881" n="HIAT:w" s="T512">kiːnin</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1884" n="HIAT:w" s="T513">bɨhallar</ts>
                  <nts id="Seg_1885" n="HIAT:ip">,</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1888" n="HIAT:w" s="T514">bɨhabɨt</ts>
                  <nts id="Seg_1889" n="HIAT:ip">.</nts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_1892" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_1894" n="HIAT:w" s="T515">Ol</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1897" n="HIAT:w" s="T516">baːbɨska</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1900" n="HIAT:w" s="T517">emeːksin</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1903" n="HIAT:w" s="T518">bɨhar</ts>
                  <nts id="Seg_1904" n="HIAT:ip">.</nts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_1907" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_1909" n="HIAT:w" s="T519">Ol</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1912" n="HIAT:w" s="T520">kiːnin</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1915" n="HIAT:w" s="T521">bɨhan</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1918" n="HIAT:w" s="T522">baraːn</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1921" n="HIAT:w" s="T523">du͡o</ts>
                  <nts id="Seg_1922" n="HIAT:ip">,</nts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1925" n="HIAT:w" s="T524">baːjan</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1928" n="HIAT:w" s="T525">keːher</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1931" n="HIAT:w" s="T526">iŋiːr</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1934" n="HIAT:w" s="T527">habɨnan</ts>
                  <nts id="Seg_1935" n="HIAT:ip">,</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1938" n="HIAT:w" s="T528">katɨllɨbɨt</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1941" n="HIAT:w" s="T529">iŋiːr</ts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1944" n="HIAT:w" s="T530">habɨnan</ts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1947" n="HIAT:w" s="T531">baːjar</ts>
                  <nts id="Seg_1948" n="HIAT:ip">,</nts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1951" n="HIAT:w" s="T532">tullan</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1954" n="HIAT:w" s="T533">tühü͡ögün</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1957" n="HIAT:w" s="T534">köjüt</ts>
                  <nts id="Seg_1958" n="HIAT:ip">,</nts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1961" n="HIAT:w" s="T535">bɨha</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1964" n="HIAT:w" s="T536">hɨtɨjan</ts>
                  <nts id="Seg_1965" n="HIAT:ip">.</nts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_1968" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_1970" n="HIAT:w" s="T537">Bi͡es</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1973" n="HIAT:w" s="T538">kün</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1976" n="HIAT:w" s="T539">duː</ts>
                  <nts id="Seg_1977" n="HIAT:ip">,</nts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1980" n="HIAT:w" s="T540">kas</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1983" n="HIAT:w" s="T541">duː</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1986" n="HIAT:w" s="T542">kün</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1989" n="HIAT:w" s="T543">bu͡olan</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1992" n="HIAT:w" s="T544">baraːn</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1995" n="HIAT:w" s="T545">tüheːčči</ts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1998" n="HIAT:w" s="T546">ogo</ts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2001" n="HIAT:w" s="T547">kiːne</ts>
                  <nts id="Seg_2002" n="HIAT:ip">.</nts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_2005" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_2007" n="HIAT:w" s="T548">Onton</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2010" n="HIAT:w" s="T549">bu͡ollagɨna</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2013" n="HIAT:w" s="T550">du͡o</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2016" n="HIAT:w" s="T551">ogotun</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2019" n="HIAT:w" s="T552">inʼete</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2022" n="HIAT:w" s="T553">tüspütün</ts>
                  <nts id="Seg_2023" n="HIAT:ip">,</nts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2026" n="HIAT:w" s="T554">ol</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2029" n="HIAT:w" s="T555">ogotun</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2032" n="HIAT:w" s="T556">inʼetin</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2035" n="HIAT:w" s="T557">huːjar</ts>
                  <nts id="Seg_2036" n="HIAT:ip">.</nts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_2039" n="HIAT:u" s="T558">
                  <ts e="T559" id="Seg_2041" n="HIAT:w" s="T558">Ogotun</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2044" n="HIAT:w" s="T559">emi͡e</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2047" n="HIAT:w" s="T560">huːjar</ts>
                  <nts id="Seg_2048" n="HIAT:ip">,</nts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2051" n="HIAT:w" s="T561">kahɨn</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2054" n="HIAT:w" s="T562">daːganɨ</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2057" n="HIAT:w" s="T563">hujan</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2060" n="HIAT:w" s="T564">baraːn</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2063" n="HIAT:w" s="T565">huːlaːn</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2066" n="HIAT:w" s="T566">keːher</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2069" n="HIAT:w" s="T567">bu͡olla</ts>
                  <nts id="Seg_2070" n="HIAT:ip">.</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_2073" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_2075" n="HIAT:w" s="T568">Onton</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2078" n="HIAT:w" s="T569">bu͡ollagɨna</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2081" n="HIAT:w" s="T570">du͡o</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2083" n="HIAT:ip">(</nts>
                  <nts id="Seg_2084" n="HIAT:ip">(</nts>
                  <ats e="T572" id="Seg_2085" n="HIAT:non-pho" s="T571">PAUSE</ats>
                  <nts id="Seg_2086" n="HIAT:ip">)</nts>
                  <nts id="Seg_2087" n="HIAT:ip">)</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2090" n="HIAT:w" s="T572">bihikke</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2093" n="HIAT:w" s="T573">össü͡ö</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2096" n="HIAT:w" s="T574">bileːbetter</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2099" n="HIAT:w" s="T575">bu͡ollaga</ts>
                  <nts id="Seg_2100" n="HIAT:ip">.</nts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_2103" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_2105" n="HIAT:w" s="T576">Huːlaːn</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2108" n="HIAT:w" s="T577">keːher</ts>
                  <nts id="Seg_2109" n="HIAT:ip">,</nts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2112" n="HIAT:w" s="T578">maːmatɨn</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2115" n="HIAT:w" s="T579">attɨgar</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2118" n="HIAT:w" s="T580">uːrar</ts>
                  <nts id="Seg_2119" n="HIAT:ip">.</nts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2122" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_2124" n="HIAT:w" s="T581">Onton</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2127" n="HIAT:w" s="T582">du͡o</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2130" n="HIAT:w" s="T583">ajɨːhɨtɨn</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2132" n="HIAT:ip">(</nts>
                  <ts e="T585" id="Seg_2134" n="HIAT:w" s="T584">hu-</ts>
                  <nts id="Seg_2135" n="HIAT:ip">)</nts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2138" n="HIAT:w" s="T585">ajɨːhɨtɨn</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2141" n="HIAT:w" s="T586">huːjan</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2144" n="HIAT:w" s="T587">baraːn</ts>
                  <nts id="Seg_2145" n="HIAT:ip">,</nts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2148" n="HIAT:w" s="T588">ol</ts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2151" n="HIAT:w" s="T589">ajɨːhɨtɨn</ts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2154" n="HIAT:w" s="T590">möhöːgüger</ts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2157" n="HIAT:w" s="T591">ugan</ts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2160" n="HIAT:w" s="T592">keːhen</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2163" n="HIAT:w" s="T593">baraːn</ts>
                  <nts id="Seg_2164" n="HIAT:ip">,</nts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2167" n="HIAT:w" s="T594">ol</ts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2170" n="HIAT:w" s="T595">matakatɨgar</ts>
                  <nts id="Seg_2171" n="HIAT:ip">,</nts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2174" n="HIAT:w" s="T596">ol</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2177" n="HIAT:w" s="T597">matakalɨːn</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2180" n="HIAT:w" s="T598">ol</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2182" n="HIAT:ip">(</nts>
                  <ts e="T600" id="Seg_2184" n="HIAT:w" s="T599">tog-</ts>
                  <nts id="Seg_2185" n="HIAT:ip">)</nts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2188" n="HIAT:w" s="T600">togohogo</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2191" n="HIAT:w" s="T601">baːjan</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2194" n="HIAT:w" s="T602">keːher</ts>
                  <nts id="Seg_2195" n="HIAT:ip">.</nts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2198" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2200" n="HIAT:w" s="T603">Ol</ts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2203" n="HIAT:w" s="T604">togohogo</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2206" n="HIAT:w" s="T605">turar</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2209" n="HIAT:w" s="T606">bu</ts>
                  <nts id="Seg_2210" n="HIAT:ip">,</nts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2213" n="HIAT:w" s="T607">ogo</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2216" n="HIAT:w" s="T608">inʼete</ts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2219" n="HIAT:w" s="T609">üjetin</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2222" n="HIAT:w" s="T610">turkarɨ</ts>
                  <nts id="Seg_2223" n="HIAT:ip">,</nts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2226" n="HIAT:w" s="T611">bu</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2229" n="HIAT:w" s="T612">ogo</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2232" n="HIAT:w" s="T613">töröːbüt</ts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2235" n="HIAT:w" s="T614">dʼukaːtɨn</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2238" n="HIAT:w" s="T615">ihiger</ts>
                  <nts id="Seg_2239" n="HIAT:ip">,</nts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2242" n="HIAT:w" s="T616">ogo</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2245" n="HIAT:w" s="T617">töröːbüt</ts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2248" n="HIAT:w" s="T618">togohotugar</ts>
                  <nts id="Seg_2249" n="HIAT:ip">,</nts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2252" n="HIAT:w" s="T619">dʼaktar</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2255" n="HIAT:w" s="T620">töröːbüt</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2258" n="HIAT:w" s="T621">togohotugar</ts>
                  <nts id="Seg_2259" n="HIAT:ip">.</nts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2262" n="HIAT:u" s="T622">
                  <ts e="T623" id="Seg_2264" n="HIAT:w" s="T622">Oččogo</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2267" n="HIAT:w" s="T623">di͡eččiler</ts>
                  <nts id="Seg_2268" n="HIAT:ip">,</nts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2271" n="HIAT:w" s="T624">bɨlɨrgɨ</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2274" n="HIAT:w" s="T625">üjetten</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2277" n="HIAT:w" s="T626">togoholoːk</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2280" n="HIAT:w" s="T627">huːrdu</ts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2283" n="HIAT:w" s="T628">aːstaktarɨnan</ts>
                  <nts id="Seg_2284" n="HIAT:ip">,</nts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2287" n="HIAT:w" s="T629">dʼukaːta</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2290" n="HIAT:w" s="T630">da</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2293" n="HIAT:w" s="T631">hu͡ok</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2296" n="HIAT:w" s="T632">bu͡ollagɨna</ts>
                  <nts id="Seg_2297" n="HIAT:ip">,</nts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2300" n="HIAT:w" s="T633">togohonu</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2303" n="HIAT:w" s="T634">tuta</ts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2306" n="HIAT:w" s="T635">turarɨn</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2309" n="HIAT:w" s="T636">kördöktörüne</ts>
                  <nts id="Seg_2310" n="HIAT:ip">.</nts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2313" n="HIAT:u" s="T637">
                  <nts id="Seg_2314" n="HIAT:ip">"</nts>
                  <ts e="T638" id="Seg_2316" n="HIAT:w" s="T637">Kaː</ts>
                  <nts id="Seg_2317" n="HIAT:ip">,</nts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2320" n="HIAT:w" s="T638">bu</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2323" n="HIAT:w" s="T639">huːrka</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2326" n="HIAT:w" s="T640">dʼaktar</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2329" n="HIAT:w" s="T641">töröːbüt</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2332" n="HIAT:w" s="T642">ebit</ts>
                  <nts id="Seg_2333" n="HIAT:ip">,</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2336" n="HIAT:w" s="T643">togohoto</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2339" n="HIAT:w" s="T644">turar</ts>
                  <nts id="Seg_2340" n="HIAT:ip">,</nts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2343" n="HIAT:w" s="T645">töröːbüt</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2346" n="HIAT:w" s="T646">togohoto</ts>
                  <nts id="Seg_2347" n="HIAT:ip">.</nts>
                  <nts id="Seg_2348" n="HIAT:ip">"</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T658" id="Seg_2351" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_2353" n="HIAT:w" s="T647">Dʼukaːlaːk</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2356" n="HIAT:w" s="T648">bu͡ollagɨna</ts>
                  <nts id="Seg_2357" n="HIAT:ip">,</nts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2359" n="HIAT:ip">"</nts>
                  <ts e="T650" id="Seg_2361" n="HIAT:w" s="T649">onnuk</ts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2364" n="HIAT:w" s="T650">ogo</ts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2367" n="HIAT:w" s="T651">töröːbüt</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2370" n="HIAT:w" s="T652">ebit</ts>
                  <nts id="Seg_2371" n="HIAT:ip">,</nts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2374" n="HIAT:w" s="T653">ol</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2377" n="HIAT:w" s="T654">ogo</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2380" n="HIAT:w" s="T655">töröːbüt</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2383" n="HIAT:w" s="T656">dʼukaːta</ts>
                  <nts id="Seg_2384" n="HIAT:ip">"</nts>
                  <nts id="Seg_2385" n="HIAT:ip">,</nts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2388" n="HIAT:w" s="T657">di͡eččiler</ts>
                  <nts id="Seg_2389" n="HIAT:ip">.</nts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2392" n="HIAT:u" s="T658">
                  <ts e="T659" id="Seg_2394" n="HIAT:w" s="T658">Oččogo</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2397" n="HIAT:w" s="T659">bu͡ollagɨna</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2400" n="HIAT:w" s="T660">du͡o</ts>
                  <nts id="Seg_2401" n="HIAT:ip">,</nts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2404" n="HIAT:w" s="T661">onton</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2407" n="HIAT:w" s="T662">bu</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2410" n="HIAT:w" s="T663">dʼaktarɨ</ts>
                  <nts id="Seg_2411" n="HIAT:ip">,</nts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2414" n="HIAT:w" s="T664">ogolommut</ts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2417" n="HIAT:w" s="T665">dʼaktarɨŋ</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2420" n="HIAT:w" s="T666">bɨrtak</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2423" n="HIAT:w" s="T667">bu͡o</ts>
                  <nts id="Seg_2424" n="HIAT:ip">.</nts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_2427" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2429" n="HIAT:w" s="T668">Töröːbüt</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2432" n="HIAT:w" s="T669">bɨrtagɨn</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2435" n="HIAT:w" s="T670">ɨraːstaːn</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2438" n="HIAT:w" s="T671">emi͡e</ts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2441" n="HIAT:w" s="T672">alastɨːgɨn</ts>
                  <nts id="Seg_2442" n="HIAT:ip">.</nts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_2445" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_2447" n="HIAT:w" s="T673">Hɨrajɨn</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2450" n="HIAT:w" s="T674">annɨnan</ts>
                  <nts id="Seg_2451" n="HIAT:ip">,</nts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2454" n="HIAT:w" s="T675">mu͡ojun</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2457" n="HIAT:w" s="T676">tögürüččü</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2460" n="HIAT:w" s="T677">üste</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2463" n="HIAT:w" s="T678">kat</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2466" n="HIAT:w" s="T679">alastɨːgɨn</ts>
                  <nts id="Seg_2467" n="HIAT:ip">:</nts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T692" id="Seg_2470" n="HIAT:u" s="T680">
                  <nts id="Seg_2471" n="HIAT:ip">"</nts>
                  <ts e="T681" id="Seg_2473" n="HIAT:w" s="T680">Türkeːt</ts>
                  <nts id="Seg_2474" n="HIAT:ip">,</nts>
                  <nts id="Seg_2475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2477" n="HIAT:w" s="T681">türkeːt</ts>
                  <nts id="Seg_2478" n="HIAT:ip">,</nts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2481" n="HIAT:w" s="T682">bɨrtagɨŋ</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2484" n="HIAT:w" s="T683">bɨlɨkka</ts>
                  <nts id="Seg_2485" n="HIAT:ip">,</nts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2488" n="HIAT:w" s="T684">ɨraːhɨŋ</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2491" n="HIAT:w" s="T685">ɨjga</ts>
                  <nts id="Seg_2492" n="HIAT:ip">,</nts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2495" n="HIAT:w" s="T686">türkeːt</ts>
                  <nts id="Seg_2496" n="HIAT:ip">,</nts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2499" n="HIAT:w" s="T687">türkeːt</ts>
                  <nts id="Seg_2500" n="HIAT:ip">,</nts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2503" n="HIAT:w" s="T688">bɨrtagɨŋ</ts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2506" n="HIAT:w" s="T689">bɨlɨkka</ts>
                  <nts id="Seg_2507" n="HIAT:ip">,</nts>
                  <nts id="Seg_2508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2510" n="HIAT:w" s="T690">ɨraːhɨŋ</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2513" n="HIAT:w" s="T691">ɨjga</ts>
                  <nts id="Seg_2514" n="HIAT:ip">.</nts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2517" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_2519" n="HIAT:w" s="T692">Türkeːt-türkeːt</ts>
                  <nts id="Seg_2520" n="HIAT:ip">,</nts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2523" n="HIAT:w" s="T693">bɨrtagɨn</ts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2526" n="HIAT:w" s="T694">bɨlɨkka</ts>
                  <nts id="Seg_2527" n="HIAT:ip">,</nts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2530" n="HIAT:w" s="T695">ɨraːhɨŋ</ts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2533" n="HIAT:w" s="T696">ɨjga</ts>
                  <nts id="Seg_2534" n="HIAT:ip">,</nts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2537" n="HIAT:w" s="T697">tu͡omuŋ-sɨramuːŋ</ts>
                  <nts id="Seg_2538" n="HIAT:ip">,</nts>
                  <nts id="Seg_2539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2541" n="HIAT:w" s="T698">tu͡omuŋ-sɨramuːŋ</ts>
                  <nts id="Seg_2542" n="HIAT:ip">,</nts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2545" n="HIAT:w" s="T699">tu͡omuŋ-sɨramuːŋ</ts>
                  <nts id="Seg_2546" n="HIAT:ip">"</nts>
                  <nts id="Seg_2547" n="HIAT:ip">,</nts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2550" n="HIAT:w" s="T700">di͡eŋŋin</ts>
                  <nts id="Seg_2551" n="HIAT:ip">,</nts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2554" n="HIAT:w" s="T701">di͡eŋŋin</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2557" n="HIAT:w" s="T702">alastɨːgɨn</ts>
                  <nts id="Seg_2558" n="HIAT:ip">.</nts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2561" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2563" n="HIAT:w" s="T703">Ol</ts>
                  <nts id="Seg_2564" n="HIAT:ip">,</nts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2567" n="HIAT:w" s="T704">ol</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2570" n="HIAT:w" s="T705">gɨnan</ts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2573" n="HIAT:w" s="T706">baraːn</ts>
                  <nts id="Seg_2574" n="HIAT:ip">,</nts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2577" n="HIAT:w" s="T707">ogotun</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2580" n="HIAT:w" s="T708">ɨlar</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2583" n="HIAT:w" s="T709">baːbɨska</ts>
                  <nts id="Seg_2584" n="HIAT:ip">.</nts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_2587" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2589" n="HIAT:w" s="T710">Onton</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2592" n="HIAT:w" s="T711">du͡o</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2595" n="HIAT:w" s="T712">dʼaktarɨ</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2598" n="HIAT:w" s="T713">turu͡orar</ts>
                  <nts id="Seg_2599" n="HIAT:ip">,</nts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2602" n="HIAT:w" s="T714">hɨtar</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2605" n="HIAT:w" s="T715">tellegitten</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2608" n="HIAT:w" s="T716">ol</ts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2611" n="HIAT:w" s="T717">töröːbüt</ts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2614" n="HIAT:w" s="T718">dʼukaːtɨgar</ts>
                  <nts id="Seg_2615" n="HIAT:ip">.</nts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_2618" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_2620" n="HIAT:w" s="T719">Onton</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2623" n="HIAT:w" s="T720">turan</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2626" n="HIAT:w" s="T721">tahaːrabɨt</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2629" n="HIAT:w" s="T722">dʼi͡ebitiger</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2632" n="HIAT:w" s="T723">barabɨt</ts>
                  <nts id="Seg_2633" n="HIAT:ip">.</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T730" id="Seg_2636" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_2638" n="HIAT:w" s="T724">Hi͡eten</ts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2641" n="HIAT:w" s="T725">illebit</ts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2644" n="HIAT:w" s="T726">töröːbüt</ts>
                  <nts id="Seg_2645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2647" n="HIAT:w" s="T727">dʼaktarɨ</ts>
                  <nts id="Seg_2648" n="HIAT:ip">,</nts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2651" n="HIAT:w" s="T728">ogolommut</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2654" n="HIAT:w" s="T729">dʼaktarɨ</ts>
                  <nts id="Seg_2655" n="HIAT:ip">.</nts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T746" id="Seg_2658" n="HIAT:u" s="T730">
                  <ts e="T731" id="Seg_2660" n="HIAT:w" s="T730">Ol</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2663" n="HIAT:w" s="T731">gɨnan</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2666" n="HIAT:w" s="T732">baraːn</ts>
                  <nts id="Seg_2667" n="HIAT:ip">,</nts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2670" n="HIAT:w" s="T733">ol</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2673" n="HIAT:w" s="T734">hi͡eten</ts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2675" n="HIAT:ip">(</nts>
                  <ts e="T736" id="Seg_2677" n="HIAT:w" s="T735">eren</ts>
                  <nts id="Seg_2678" n="HIAT:ip">)</nts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2681" n="HIAT:w" s="T736">ol</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2684" n="HIAT:w" s="T737">kepsiːr</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2687" n="HIAT:w" s="T738">ol</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2689" n="HIAT:ip">"</nts>
                  <ts e="T740" id="Seg_2691" n="HIAT:w" s="T739">kaja</ts>
                  <nts id="Seg_2692" n="HIAT:ip">,</nts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2695" n="HIAT:w" s="T740">ogo</ts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2698" n="HIAT:w" s="T741">töröːtö</ts>
                  <nts id="Seg_2699" n="HIAT:ip">"</nts>
                  <nts id="Seg_2700" n="HIAT:ip">,</nts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2703" n="HIAT:w" s="T742">di͡en</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2706" n="HIAT:w" s="T743">kergetteriger</ts>
                  <nts id="Seg_2707" n="HIAT:ip">,</nts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2710" n="HIAT:w" s="T744">dʼi͡eleːk</ts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2713" n="HIAT:w" s="T745">dʼi͡eleriger</ts>
                  <nts id="Seg_2714" n="HIAT:ip">.</nts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T749" id="Seg_2717" n="HIAT:u" s="T746">
                  <ts e="T747" id="Seg_2719" n="HIAT:w" s="T746">Dʼi͡eleːk</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2722" n="HIAT:w" s="T747">uraha</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2725" n="HIAT:w" s="T748">dʼi͡ege</ts>
                  <nts id="Seg_2726" n="HIAT:ip">.</nts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_2729" n="HIAT:u" s="T749">
                  <ts e="T750" id="Seg_2731" n="HIAT:w" s="T749">Ol</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2734" n="HIAT:w" s="T750">kepseːn</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2737" n="HIAT:w" s="T751">kepsiːr</ts>
                  <nts id="Seg_2738" n="HIAT:ip">,</nts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2741" n="HIAT:w" s="T752">kajaː</ts>
                  <nts id="Seg_2742" n="HIAT:ip">,</nts>
                  <nts id="Seg_2743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2745" n="HIAT:w" s="T753">ol</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2748" n="HIAT:w" s="T754">kɨːh</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2751" n="HIAT:w" s="T755">ogo</ts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2754" n="HIAT:w" s="T756">töröːbüt</ts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2757" n="HIAT:w" s="T757">bu͡ollagɨna</ts>
                  <nts id="Seg_2758" n="HIAT:ip">,</nts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2760" n="HIAT:ip">"</nts>
                  <ts e="T759" id="Seg_2762" n="HIAT:w" s="T758">kɨːh</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2765" n="HIAT:w" s="T759">ogo</ts>
                  <nts id="Seg_2766" n="HIAT:ip">"</nts>
                  <nts id="Seg_2767" n="HIAT:ip">,</nts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2770" n="HIAT:w" s="T760">diːller</ts>
                  <nts id="Seg_2771" n="HIAT:ip">.</nts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T768" id="Seg_2774" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_2776" n="HIAT:w" s="T761">U͡ol</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2779" n="HIAT:w" s="T762">ogo</ts>
                  <nts id="Seg_2780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2782" n="HIAT:w" s="T763">töröːbüt</ts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2785" n="HIAT:w" s="T764">bu͡ollagɨna</ts>
                  <nts id="Seg_2786" n="HIAT:ip">,</nts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2788" n="HIAT:ip">"</nts>
                  <ts e="T766" id="Seg_2790" n="HIAT:w" s="T765">u͡ol</ts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2793" n="HIAT:w" s="T766">ogo</ts>
                  <nts id="Seg_2794" n="HIAT:ip">"</nts>
                  <nts id="Seg_2795" n="HIAT:ip">,</nts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2798" n="HIAT:w" s="T767">diːller</ts>
                  <nts id="Seg_2799" n="HIAT:ip">.</nts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T778" id="Seg_2802" n="HIAT:u" s="T768">
                  <ts e="T769" id="Seg_2804" n="HIAT:w" s="T768">Onton</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2807" n="HIAT:w" s="T769">bu</ts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2810" n="HIAT:w" s="T770">üs</ts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2813" n="HIAT:w" s="T771">kün</ts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2816" n="HIAT:w" s="T772">bu͡olan</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2819" n="HIAT:w" s="T773">baraːn</ts>
                  <nts id="Seg_2820" n="HIAT:ip">,</nts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2823" n="HIAT:w" s="T774">agata</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2826" n="HIAT:w" s="T775">bihigi</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2829" n="HIAT:w" s="T776">oŋoror</ts>
                  <nts id="Seg_2830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2832" n="HIAT:w" s="T777">ogotugar</ts>
                  <nts id="Seg_2833" n="HIAT:ip">.</nts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T780" id="Seg_2836" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_2838" n="HIAT:w" s="T778">Ehete</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2841" n="HIAT:w" s="T779">kömölöhör</ts>
                  <nts id="Seg_2842" n="HIAT:ip">.</nts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T793" id="Seg_2845" n="HIAT:u" s="T780">
                  <ts e="T781" id="Seg_2847" n="HIAT:w" s="T780">Oččogo</ts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2850" n="HIAT:w" s="T781">bu͡o</ts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2853" n="HIAT:w" s="T782">bu</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2856" n="HIAT:w" s="T783">bihigi</ts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2859" n="HIAT:w" s="T784">du͡o</ts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2862" n="HIAT:w" s="T785">tabɨllɨbɨt</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2865" n="HIAT:w" s="T786">bige</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2868" n="HIAT:w" s="T787">doskoːlarɨnan</ts>
                  <nts id="Seg_2869" n="HIAT:ip">,</nts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2872" n="HIAT:w" s="T788">bɨ͡annan</ts>
                  <nts id="Seg_2873" n="HIAT:ip">,</nts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2876" n="HIAT:w" s="T789">harɨː</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2879" n="HIAT:w" s="T790">bɨ͡annan</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2882" n="HIAT:w" s="T791">üːjen</ts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2885" n="HIAT:w" s="T792">oŋoroːččular</ts>
                  <nts id="Seg_2886" n="HIAT:ip">.</nts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T805" id="Seg_2889" n="HIAT:u" s="T793">
                  <ts e="T794" id="Seg_2891" n="HIAT:w" s="T793">Karɨjanɨ</ts>
                  <nts id="Seg_2892" n="HIAT:ip">,</nts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2895" n="HIAT:w" s="T794">bu</ts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2898" n="HIAT:w" s="T795">kim</ts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2901" n="HIAT:w" s="T796">eː</ts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2904" n="HIAT:w" s="T797">fanʼera</ts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2907" n="HIAT:w" s="T798">kördük</ts>
                  <nts id="Seg_2908" n="HIAT:ip">,</nts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2911" n="HIAT:w" s="T799">di͡eččiler</ts>
                  <nts id="Seg_2912" n="HIAT:ip">,</nts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2915" n="HIAT:w" s="T800">iti</ts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2918" n="HIAT:w" s="T801">itinnigi</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2921" n="HIAT:w" s="T802">du͡o</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2924" n="HIAT:w" s="T803">oŋortoroːččuta</ts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2927" n="HIAT:w" s="T804">hu͡oktar</ts>
                  <nts id="Seg_2928" n="HIAT:ip">.</nts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T810" id="Seg_2931" n="HIAT:u" s="T805">
                  <ts e="T806" id="Seg_2933" n="HIAT:w" s="T805">Kappɨt</ts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2936" n="HIAT:w" s="T806">mas</ts>
                  <nts id="Seg_2937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2939" n="HIAT:w" s="T807">tiriːte</ts>
                  <nts id="Seg_2940" n="HIAT:ip">,</nts>
                  <nts id="Seg_2941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2943" n="HIAT:w" s="T808">iti</ts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2946" n="HIAT:w" s="T809">fanʼera</ts>
                  <nts id="Seg_2947" n="HIAT:ip">.</nts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T821" id="Seg_2950" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_2952" n="HIAT:w" s="T810">Ol</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2955" n="HIAT:w" s="T811">ihin</ts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2958" n="HIAT:w" s="T812">bu͡ollagɨna</ts>
                  <nts id="Seg_2959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2961" n="HIAT:w" s="T813">du͡o</ts>
                  <nts id="Seg_2962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2964" n="HIAT:w" s="T814">ogo</ts>
                  <nts id="Seg_2965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2967" n="HIAT:w" s="T815">katɨ͡a</ts>
                  <nts id="Seg_2968" n="HIAT:ip">,</nts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2971" n="HIAT:w" s="T816">di͡eččiler</ts>
                  <nts id="Seg_2972" n="HIAT:ip">,</nts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2975" n="HIAT:w" s="T817">ol</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2978" n="HIAT:w" s="T818">ihin</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2980" n="HIAT:ip">(</nts>
                  <ts e="T820" id="Seg_2982" n="HIAT:w" s="T819">do-</ts>
                  <nts id="Seg_2983" n="HIAT:ip">)</nts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_2986" n="HIAT:w" s="T820">doskonnon</ts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2988" n="HIAT:ip">(</nts>
                  <nts id="Seg_2989" n="HIAT:ip">(</nts>
                  <ats e="T821" id="Seg_2990" n="HIAT:non-pho" s="T0">…</ats>
                  <nts id="Seg_2991" n="HIAT:ip">)</nts>
                  <nts id="Seg_2992" n="HIAT:ip">)</nts>
                  <nts id="Seg_2993" n="HIAT:ip">.</nts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T833" id="Seg_2996" n="HIAT:u" s="T821">
                  <ts e="T822" id="Seg_2998" n="HIAT:w" s="T821">A</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3001" n="HIAT:w" s="T822">doskoːŋ</ts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3004" n="HIAT:w" s="T823">üːjülünnegine</ts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3007" n="HIAT:w" s="T824">du͡o</ts>
                  <nts id="Seg_3008" n="HIAT:ip">,</nts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3011" n="HIAT:w" s="T825">üːjülünneginen</ts>
                  <nts id="Seg_3012" n="HIAT:ip">,</nts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3015" n="HIAT:w" s="T826">üːjülünnegine</ts>
                  <nts id="Seg_3016" n="HIAT:ip">,</nts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3019" n="HIAT:w" s="T827">bɨ͡annan</ts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3022" n="HIAT:w" s="T828">üːjen</ts>
                  <nts id="Seg_3023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3025" n="HIAT:w" s="T829">oŋordoktoruna</ts>
                  <nts id="Seg_3026" n="HIAT:ip">,</nts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3029" n="HIAT:w" s="T830">ontuŋ</ts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3032" n="HIAT:w" s="T831">bɨːstaːk</ts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3035" n="HIAT:w" s="T832">bu͡olaːččɨ</ts>
                  <nts id="Seg_3036" n="HIAT:ip">.</nts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T838" id="Seg_3039" n="HIAT:u" s="T833">
                  <ts e="T834" id="Seg_3041" n="HIAT:w" s="T833">Dosko</ts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3044" n="HIAT:w" s="T834">doskotuttan</ts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3047" n="HIAT:w" s="T835">emi͡e</ts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3050" n="HIAT:w" s="T836">bɨːstaːk</ts>
                  <nts id="Seg_3051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3053" n="HIAT:w" s="T837">bu͡olaːččɨ</ts>
                  <nts id="Seg_3054" n="HIAT:ip">.</nts>
                  <nts id="Seg_3055" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_3057" n="HIAT:u" s="T838">
                  <ts e="T839" id="Seg_3059" n="HIAT:w" s="T838">Oččogo</ts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3062" n="HIAT:w" s="T839">tɨːn</ts>
                  <nts id="Seg_3063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3065" n="HIAT:w" s="T840">kiːrer</ts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3068" n="HIAT:w" s="T841">bu͡ollaga</ts>
                  <nts id="Seg_3069" n="HIAT:ip">,</nts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3072" n="HIAT:w" s="T842">ol</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3075" n="HIAT:w" s="T843">ihin</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3078" n="HIAT:w" s="T844">ogoŋ</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3081" n="HIAT:w" s="T845">zdarovaj</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3084" n="HIAT:w" s="T846">ü͡ösküːr</ts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3087" n="HIAT:w" s="T847">bu͡olla</ts>
                  <nts id="Seg_3088" n="HIAT:ip">.</nts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T848" id="Seg_3090" n="sc" s="T1">
               <ts e="T2" id="Seg_3092" n="e" s="T1">Bɨlɨrgɨ </ts>
               <ts e="T3" id="Seg_3094" n="e" s="T2">ogo </ts>
               <ts e="T4" id="Seg_3096" n="e" s="T3">törüːre </ts>
               <ts e="T5" id="Seg_3098" n="e" s="T4">togohogo. </ts>
               <ts e="T6" id="Seg_3100" n="e" s="T5">Togohogo </ts>
               <ts e="T7" id="Seg_3102" n="e" s="T6">(ogoloro) </ts>
               <ts e="T8" id="Seg_3104" n="e" s="T7">(ogoː), </ts>
               <ts e="T9" id="Seg_3106" n="e" s="T8">ɨ͡arakan </ts>
               <ts e="T10" id="Seg_3108" n="e" s="T9">dʼaktar </ts>
               <ts e="T11" id="Seg_3110" n="e" s="T10">törüːr </ts>
               <ts e="T12" id="Seg_3112" n="e" s="T11">kemiger </ts>
               <ts e="T13" id="Seg_3114" n="e" s="T12">bɨlɨrgɨ </ts>
               <ts e="T14" id="Seg_3116" n="e" s="T13">üjetten </ts>
               <ts e="T15" id="Seg_3118" n="e" s="T14">togohonu </ts>
               <ts e="T16" id="Seg_3120" n="e" s="T15">belemneːččiler. </ts>
               <ts e="T17" id="Seg_3122" n="e" s="T16">Tuspa </ts>
               <ts e="T18" id="Seg_3124" n="e" s="T17">uraha </ts>
               <ts e="T19" id="Seg_3126" n="e" s="T18">dʼi͡eni </ts>
               <ts e="T20" id="Seg_3128" n="e" s="T19">tutannar </ts>
               <ts e="T21" id="Seg_3130" n="e" s="T20">belemneːččiler. </ts>
               <ts e="T22" id="Seg_3132" n="e" s="T21">Ogo </ts>
               <ts e="T23" id="Seg_3134" n="e" s="T22">törüːr </ts>
               <ts e="T24" id="Seg_3136" n="e" s="T23">dʼukaːta, </ts>
               <ts e="T25" id="Seg_3138" n="e" s="T24">töröːbüt </ts>
               <ts e="T26" id="Seg_3140" n="e" s="T25">dʼukaːta, </ts>
               <ts e="T27" id="Seg_3142" n="e" s="T26">di͡eččiler </ts>
               <ts e="T28" id="Seg_3144" n="e" s="T27">onton. </ts>
               <ts e="T29" id="Seg_3146" n="e" s="T28">Onno </ts>
               <ts e="T30" id="Seg_3148" n="e" s="T29">bu͡ollagɨna </ts>
               <ts e="T31" id="Seg_3150" n="e" s="T30">oːl </ts>
               <ts e="T32" id="Seg_3152" n="e" s="T31">togohonu </ts>
               <ts e="T33" id="Seg_3154" n="e" s="T32">oŋoroːččular, </ts>
               <ts e="T34" id="Seg_3156" n="e" s="T33">kergettere </ts>
               <ts e="T35" id="Seg_3158" n="e" s="T34">turu͡orallar. </ts>
               <ts e="T36" id="Seg_3160" n="e" s="T35">Ehete </ts>
               <ts e="T37" id="Seg_3162" n="e" s="T36">duː, </ts>
               <ts e="T38" id="Seg_3164" n="e" s="T37">agata </ts>
               <ts e="T39" id="Seg_3166" n="e" s="T38">duː, </ts>
               <ts e="T40" id="Seg_3168" n="e" s="T39">inʼete </ts>
               <ts e="T41" id="Seg_3170" n="e" s="T40">duː </ts>
               <ts e="T42" id="Seg_3172" n="e" s="T41">kömölöhönnör, </ts>
               <ts e="T43" id="Seg_3174" n="e" s="T42">baːjallar </ts>
               <ts e="T44" id="Seg_3176" n="e" s="T43">togohonu </ts>
               <ts e="T45" id="Seg_3178" n="e" s="T44">urahaga </ts>
               <ts e="T46" id="Seg_3180" n="e" s="T45">kɨhajɨː </ts>
               <ts e="T47" id="Seg_3182" n="e" s="T46">tu͡ora </ts>
               <ts e="T48" id="Seg_3184" n="e" s="T47">mahɨ. </ts>
               <ts e="T49" id="Seg_3186" n="e" s="T48">Onton, </ts>
               <ts e="T50" id="Seg_3188" n="e" s="T49">e, </ts>
               <ts e="T51" id="Seg_3190" n="e" s="T50">togohotun </ts>
               <ts e="T52" id="Seg_3192" n="e" s="T51">uhugugar </ts>
               <ts e="T53" id="Seg_3194" n="e" s="T52">du͡o </ts>
               <ts e="T54" id="Seg_3196" n="e" s="T53">končočču </ts>
               <ts e="T55" id="Seg_3198" n="e" s="T54">annʼɨllan </ts>
               <ts e="T56" id="Seg_3200" n="e" s="T55">turar </ts>
               <ts e="T57" id="Seg_3202" n="e" s="T56">(baha), </ts>
               <ts e="T58" id="Seg_3204" n="e" s="T57">eː, </ts>
               <ts e="T59" id="Seg_3206" n="e" s="T58">mas </ts>
               <ts e="T60" id="Seg_3208" n="e" s="T59">bu͡olaːččɨ, </ts>
               <ts e="T61" id="Seg_3210" n="e" s="T60">(urahakaːta). </ts>
               <ts e="T62" id="Seg_3212" n="e" s="T61">Oččogo </ts>
               <ts e="T63" id="Seg_3214" n="e" s="T62">bu͡ollagɨna </ts>
               <ts e="T64" id="Seg_3216" n="e" s="T63">ol </ts>
               <ts e="T65" id="Seg_3218" n="e" s="T64">togohoŋ </ts>
               <ts e="T66" id="Seg_3220" n="e" s="T65">bu͡o </ts>
               <ts e="T67" id="Seg_3222" n="e" s="T66">tü͡öhün </ts>
               <ts e="T68" id="Seg_3224" n="e" s="T67">kuraːgɨnan </ts>
               <ts e="T69" id="Seg_3226" n="e" s="T68">bu͡olaːččɨ. </ts>
               <ts e="T70" id="Seg_3228" n="e" s="T69">Dʼaktar </ts>
               <ts e="T71" id="Seg_3230" n="e" s="T70">onton </ts>
               <ts e="T72" id="Seg_3232" n="e" s="T71">ɨjaːllan </ts>
               <ts e="T73" id="Seg_3234" n="e" s="T72">oloron </ts>
               <ts e="T74" id="Seg_3236" n="e" s="T73">ogolonoːčču. </ts>
               <ts e="T75" id="Seg_3238" n="e" s="T74">Ol </ts>
               <ts e="T76" id="Seg_3240" n="e" s="T75">ogolonorun </ts>
               <ts e="T77" id="Seg_3242" n="e" s="T76">annɨtɨgar </ts>
               <ts e="T78" id="Seg_3244" n="e" s="T77">du͡o, </ts>
               <ts e="T79" id="Seg_3246" n="e" s="T78">eː </ts>
               <ts e="T80" id="Seg_3248" n="e" s="T79">oloror </ts>
               <ts e="T81" id="Seg_3250" n="e" s="T80">hiriger </ts>
               <ts e="T82" id="Seg_3252" n="e" s="T81">lekeː </ts>
               <ts e="T83" id="Seg_3254" n="e" s="T82">bu͡olaːččɨ. </ts>
               <ts e="T84" id="Seg_3256" n="e" s="T83">Dʼe, </ts>
               <ts e="T85" id="Seg_3258" n="e" s="T84">ol </ts>
               <ts e="T86" id="Seg_3260" n="e" s="T85">lekeːge </ts>
               <ts e="T87" id="Seg_3262" n="e" s="T86">du͡o </ts>
               <ts e="T88" id="Seg_3264" n="e" s="T87">ot </ts>
               <ts e="T89" id="Seg_3266" n="e" s="T88">bu͡olaːččɨ. </ts>
               <ts e="T90" id="Seg_3268" n="e" s="T89">Otu </ts>
               <ts e="T91" id="Seg_3270" n="e" s="T90">uːraːččɨlar </ts>
               <ts e="T92" id="Seg_3272" n="e" s="T91">tuːmugar, </ts>
               <ts e="T93" id="Seg_3274" n="e" s="T92">di͡enner. </ts>
               <ts e="T94" id="Seg_3276" n="e" s="T93">Onuga </ts>
               <ts e="T95" id="Seg_3278" n="e" s="T94">du͡o </ts>
               <ts e="T96" id="Seg_3280" n="e" s="T95">ogo </ts>
               <ts e="T97" id="Seg_3282" n="e" s="T96">tüstegine, </ts>
               <ts e="T98" id="Seg_3284" n="e" s="T97">"otugar </ts>
               <ts e="T99" id="Seg_3286" n="e" s="T98">tüher", </ts>
               <ts e="T100" id="Seg_3288" n="e" s="T99">di͡eččiler </ts>
               <ts e="T101" id="Seg_3290" n="e" s="T100">ol </ts>
               <ts e="T102" id="Seg_3292" n="e" s="T101">ihin, </ts>
               <ts e="T103" id="Seg_3294" n="e" s="T102">ottoːk </ts>
               <ts e="T104" id="Seg_3296" n="e" s="T103">ottoːgun </ts>
               <ts e="T105" id="Seg_3298" n="e" s="T104">ihin. </ts>
               <ts e="T106" id="Seg_3300" n="e" s="T105">"Okko </ts>
               <ts e="T107" id="Seg_3302" n="e" s="T106">tüspüt", </ts>
               <ts e="T108" id="Seg_3304" n="e" s="T107">di͡eččiler. </ts>
               <ts e="T109" id="Seg_3306" n="e" s="T108">Eː, </ts>
               <ts e="T110" id="Seg_3308" n="e" s="T109">dʼaktar </ts>
               <ts e="T111" id="Seg_3310" n="e" s="T110">ogolonorugar </ts>
               <ts e="T112" id="Seg_3312" n="e" s="T111">(ɨraː-) </ts>
               <ts e="T113" id="Seg_3314" n="e" s="T112">bɨlɨrgɨ </ts>
               <ts e="T114" id="Seg_3316" n="e" s="T113">üjetten, </ts>
               <ts e="T115" id="Seg_3318" n="e" s="T114">ɨraːkka </ts>
               <ts e="T116" id="Seg_3320" n="e" s="T115">baːr </ts>
               <ts e="T117" id="Seg_3322" n="e" s="T116">bu͡ollagɨna </ts>
               <ts e="T118" id="Seg_3324" n="e" s="T117">ol </ts>
               <ts e="T119" id="Seg_3326" n="e" s="T118">eː… </ts>
               <ts e="T120" id="Seg_3328" n="e" s="T119">Baːbɨska </ts>
               <ts e="T121" id="Seg_3330" n="e" s="T120">emeːksinnere </ts>
               <ts e="T122" id="Seg_3332" n="e" s="T121">ɨraːk </ts>
               <ts e="T123" id="Seg_3334" n="e" s="T122">baːr </ts>
               <ts e="T124" id="Seg_3336" n="e" s="T123">(bu͡ol-), </ts>
               <ts e="T125" id="Seg_3338" n="e" s="T124">ogo </ts>
               <ts e="T126" id="Seg_3340" n="e" s="T125">törüː </ts>
               <ts e="T127" id="Seg_3342" n="e" s="T126">tühüttere </ts>
               <ts e="T128" id="Seg_3344" n="e" s="T127">dʼaktardara </ts>
               <ts e="T129" id="Seg_3346" n="e" s="T128">ɨraːk </ts>
               <ts e="T130" id="Seg_3348" n="e" s="T129">baːr </ts>
               <ts e="T131" id="Seg_3350" n="e" s="T130">bu͡ollagɨna, </ts>
               <ts e="T132" id="Seg_3352" n="e" s="T131">hɨrgalaːk </ts>
               <ts e="T133" id="Seg_3354" n="e" s="T132">barannar </ts>
               <ts e="T134" id="Seg_3356" n="e" s="T133">egeleːččiler. </ts>
               <ts e="T135" id="Seg_3358" n="e" s="T134">Kördöhönnör, </ts>
               <ts e="T136" id="Seg_3360" n="e" s="T135">"kaja </ts>
               <ts e="T137" id="Seg_3362" n="e" s="T136">ogobut </ts>
               <ts e="T138" id="Seg_3364" n="e" s="T137">oččogo </ts>
               <ts e="T139" id="Seg_3366" n="e" s="T138">oččogo, </ts>
               <ts e="T140" id="Seg_3368" n="e" s="T139">ol </ts>
               <ts e="T141" id="Seg_3370" n="e" s="T140">kɨːspɨt </ts>
               <ts e="T142" id="Seg_3372" n="e" s="T141">(ü͡ö-) </ts>
               <ts e="T143" id="Seg_3374" n="e" s="T142">törü͡öge", </ts>
               <ts e="T144" id="Seg_3376" n="e" s="T143">di͡enner. </ts>
               <ts e="T145" id="Seg_3378" n="e" s="T144">"Dʼaktarbɨt </ts>
               <ts e="T146" id="Seg_3380" n="e" s="T145">törü͡öge", </ts>
               <ts e="T147" id="Seg_3382" n="e" s="T146">di͡enner </ts>
               <ts e="T148" id="Seg_3384" n="e" s="T147">egeleːččiler. </ts>
               <ts e="T149" id="Seg_3386" n="e" s="T148">Eː, </ts>
               <ts e="T150" id="Seg_3388" n="e" s="T149">čugas </ts>
               <ts e="T151" id="Seg_3390" n="e" s="T150">baːr </ts>
               <ts e="T152" id="Seg_3392" n="e" s="T151">bu͡ollagɨna </ts>
               <ts e="T153" id="Seg_3394" n="e" s="T152">du͡o </ts>
               <ts e="T154" id="Seg_3396" n="e" s="T153">ɨgɨraːččɨlar </ts>
               <ts e="T155" id="Seg_3398" n="e" s="T154">ol </ts>
               <ts e="T156" id="Seg_3400" n="e" s="T155">baːbɨska </ts>
               <ts e="T157" id="Seg_3402" n="e" s="T156">emeːksini. </ts>
               <ts e="T158" id="Seg_3404" n="e" s="T157">Innʼe </ts>
               <ts e="T159" id="Seg_3406" n="e" s="T158">gɨnan </ts>
               <ts e="T160" id="Seg_3408" n="e" s="T159">ol </ts>
               <ts e="T161" id="Seg_3410" n="e" s="T160">baːbɨska </ts>
               <ts e="T162" id="Seg_3412" n="e" s="T161">iliː </ts>
               <ts e="T163" id="Seg_3414" n="e" s="T162">hottoro, </ts>
               <ts e="T164" id="Seg_3416" n="e" s="T163">tu͡oga </ts>
               <ts e="T165" id="Seg_3418" n="e" s="T164">barɨta, </ts>
               <ts e="T166" id="Seg_3420" n="e" s="T165">čeber </ts>
               <ts e="T167" id="Seg_3422" n="e" s="T166">bu͡olu͡ogun. </ts>
               <ts e="T168" id="Seg_3424" n="e" s="T167">Höp </ts>
               <ts e="T169" id="Seg_3426" n="e" s="T168">bu͡olaːččɨ, </ts>
               <ts e="T170" id="Seg_3428" n="e" s="T169">barɨkaːna </ts>
               <ts e="T171" id="Seg_3430" n="e" s="T170">haŋa </ts>
               <ts e="T172" id="Seg_3432" n="e" s="T171">tutullubatak </ts>
               <ts e="T173" id="Seg_3434" n="e" s="T172">ebi͡enne </ts>
               <ts e="T174" id="Seg_3436" n="e" s="T173">bu͡olardaːk. </ts>
               <ts e="T175" id="Seg_3438" n="e" s="T174">Hottoro </ts>
               <ts e="T176" id="Seg_3440" n="e" s="T175">da, </ts>
               <ts e="T177" id="Seg_3442" n="e" s="T176">iliː </ts>
               <ts e="T178" id="Seg_3444" n="e" s="T177">hottoro, </ts>
               <ts e="T179" id="Seg_3446" n="e" s="T178">ogonu </ts>
               <ts e="T180" id="Seg_3448" n="e" s="T179">töröttörüger </ts>
               <ts e="T181" id="Seg_3450" n="e" s="T180">(tabɨskɨː-) </ts>
               <ts e="T182" id="Seg_3452" n="e" s="T181">dʼaktar </ts>
               <ts e="T183" id="Seg_3454" n="e" s="T182">tabɨskɨːrɨgar. </ts>
               <ts e="T184" id="Seg_3456" n="e" s="T183">Oččogo </ts>
               <ts e="T185" id="Seg_3458" n="e" s="T184">bu͡ollagɨna </ts>
               <ts e="T186" id="Seg_3460" n="e" s="T185">du͡o </ts>
               <ts e="T187" id="Seg_3462" n="e" s="T186">ajɨːhɨttaːk </ts>
               <ts e="T188" id="Seg_3464" n="e" s="T187">bu͡olaːččɨ. </ts>
               <ts e="T189" id="Seg_3466" n="e" s="T188">Ol </ts>
               <ts e="T190" id="Seg_3468" n="e" s="T189">ajɨːhɨtɨ </ts>
               <ts e="T191" id="Seg_3470" n="e" s="T190">bu͡ollagɨna, </ts>
               <ts e="T192" id="Seg_3472" n="e" s="T191">maŋnaj </ts>
               <ts e="T193" id="Seg_3474" n="e" s="T192">ɨla </ts>
               <ts e="T194" id="Seg_3476" n="e" s="T193">kele </ts>
               <ts e="T195" id="Seg_3478" n="e" s="T194">keleller, </ts>
               <ts e="T196" id="Seg_3480" n="e" s="T195">dʼaktardara </ts>
               <ts e="T197" id="Seg_3482" n="e" s="T196">töröːrü </ts>
               <ts e="T198" id="Seg_3484" n="e" s="T197">gɨnnagɨna, </ts>
               <ts e="T199" id="Seg_3486" n="e" s="T198">ol </ts>
               <ts e="T200" id="Seg_3488" n="e" s="T199">tuspa, </ts>
               <ts e="T201" id="Seg_3490" n="e" s="T200">ol </ts>
               <ts e="T202" id="Seg_3492" n="e" s="T201">tuspa </ts>
               <ts e="T203" id="Seg_3494" n="e" s="T202">dʼukaːga </ts>
               <ts e="T204" id="Seg_3496" n="e" s="T203">kimi </ts>
               <ts e="T205" id="Seg_3498" n="e" s="T204">da </ts>
               <ts e="T206" id="Seg_3500" n="e" s="T205">kiːllerbetter </ts>
               <ts e="T207" id="Seg_3502" n="e" s="T206">bu͡ollaga. </ts>
               <ts e="T208" id="Seg_3504" n="e" s="T207">Kim </ts>
               <ts e="T209" id="Seg_3506" n="e" s="T208">tuspa </ts>
               <ts e="T210" id="Seg_3508" n="e" s="T209">dʼukaː </ts>
               <ts e="T211" id="Seg_3510" n="e" s="T210">tuppattar, </ts>
               <ts e="T212" id="Seg_3512" n="e" s="T211">dʼi͡ege </ts>
               <ts e="T213" id="Seg_3514" n="e" s="T212">törötöːččüler, </ts>
               <ts e="T214" id="Seg_3516" n="e" s="T213">dʼi͡ege, </ts>
               <ts e="T215" id="Seg_3518" n="e" s="T214">oloror </ts>
               <ts e="T216" id="Seg_3520" n="e" s="T215">dʼukaːlarɨgar. </ts>
               <ts e="T217" id="Seg_3522" n="e" s="T216">Ol </ts>
               <ts e="T218" id="Seg_3524" n="e" s="T217">gɨnan </ts>
               <ts e="T219" id="Seg_3526" n="e" s="T218">baran </ts>
               <ts e="T220" id="Seg_3528" n="e" s="T219">oloror </ts>
               <ts e="T221" id="Seg_3530" n="e" s="T220">dʼukaːlarɨttan </ts>
               <ts e="T222" id="Seg_3532" n="e" s="T221">tuspa </ts>
               <ts e="T223" id="Seg_3534" n="e" s="T222">oŋoroːččular, </ts>
               <ts e="T224" id="Seg_3536" n="e" s="T223">togohonu </ts>
               <ts e="T225" id="Seg_3538" n="e" s="T224">turu͡oraːččɨlar. </ts>
               <ts e="T226" id="Seg_3540" n="e" s="T225">Če, </ts>
               <ts e="T227" id="Seg_3542" n="e" s="T226">ol </ts>
               <ts e="T228" id="Seg_3544" n="e" s="T227">gɨnan </ts>
               <ts e="T229" id="Seg_3546" n="e" s="T228">kihini </ts>
               <ts e="T230" id="Seg_3548" n="e" s="T229">barɨkaːnɨn </ts>
               <ts e="T231" id="Seg_3550" n="e" s="T230">taharaːččɨlar. </ts>
               <ts e="T232" id="Seg_3552" n="e" s="T231">Kim </ts>
               <ts e="T233" id="Seg_3554" n="e" s="T232">da </ts>
               <ts e="T234" id="Seg_3556" n="e" s="T233">hu͡ok </ts>
               <ts e="T235" id="Seg_3558" n="e" s="T234">bu͡olu͡oktaːk, </ts>
               <ts e="T236" id="Seg_3560" n="e" s="T235">hogotok </ts>
               <ts e="T237" id="Seg_3562" n="e" s="T236">baːbuskanɨ </ts>
               <ts e="T238" id="Seg_3564" n="e" s="T237">gɨtta </ts>
               <ts e="T239" id="Seg_3566" n="e" s="T238">törüːr </ts>
               <ts e="T240" id="Seg_3568" n="e" s="T239">dʼaktar </ts>
               <ts e="T241" id="Seg_3570" n="e" s="T240">ere </ts>
               <ts e="T242" id="Seg_3572" n="e" s="T241">bu͡olu͡oktaːktar </ts>
               <ts e="T243" id="Seg_3574" n="e" s="T242">onno. </ts>
               <ts e="T244" id="Seg_3576" n="e" s="T243">Oččogo </ts>
               <ts e="T245" id="Seg_3578" n="e" s="T244">bu͡ollagɨna </ts>
               <ts e="T246" id="Seg_3580" n="e" s="T245">du͡o </ts>
               <ts e="T247" id="Seg_3582" n="e" s="T246">bu </ts>
               <ts e="T248" id="Seg_3584" n="e" s="T247">baːbuskaŋ </ts>
               <ts e="T249" id="Seg_3586" n="e" s="T248">keler </ts>
               <ts e="T250" id="Seg_3588" n="e" s="T249">uraha </ts>
               <ts e="T251" id="Seg_3590" n="e" s="T250">dʼi͡ege, </ts>
               <ts e="T252" id="Seg_3592" n="e" s="T251">törüːr </ts>
               <ts e="T253" id="Seg_3594" n="e" s="T252">dʼi͡ege, </ts>
               <ts e="T254" id="Seg_3596" n="e" s="T253">dʼukaːga. </ts>
               <ts e="T255" id="Seg_3598" n="e" s="T254">Tuspa </ts>
               <ts e="T256" id="Seg_3600" n="e" s="T255">tutullubut </ts>
               <ts e="T257" id="Seg_3602" n="e" s="T256">bu͡ollagɨna, </ts>
               <ts e="T258" id="Seg_3604" n="e" s="T257">duː </ts>
               <ts e="T259" id="Seg_3606" n="e" s="T258">dʼi͡ege </ts>
               <ts e="T260" id="Seg_3608" n="e" s="T259">ba </ts>
               <ts e="T261" id="Seg_3610" n="e" s="T260">oŋohullubut </ts>
               <ts e="T262" id="Seg_3612" n="e" s="T261">bu͡ollagɨna. </ts>
               <ts e="T263" id="Seg_3614" n="e" s="T262">Ol </ts>
               <ts e="T264" id="Seg_3616" n="e" s="T263">kelen, </ts>
               <ts e="T265" id="Seg_3618" n="e" s="T264">ke </ts>
               <ts e="T266" id="Seg_3620" n="e" s="T265">egelen </ts>
               <ts e="T267" id="Seg_3622" n="e" s="T266">kelenner, </ts>
               <ts e="T268" id="Seg_3624" n="e" s="T267">ol </ts>
               <ts e="T269" id="Seg_3626" n="e" s="T268">baːbuska </ts>
               <ts e="T270" id="Seg_3628" n="e" s="T269">emeːksine </ts>
               <ts e="T271" id="Seg_3630" n="e" s="T270">kiːrdegine, </ts>
               <ts e="T272" id="Seg_3632" n="e" s="T271">onu </ts>
               <ts e="T273" id="Seg_3634" n="e" s="T272">barɨkaːnɨ </ts>
               <ts e="T274" id="Seg_3636" n="e" s="T273">jaːntardaːn. </ts>
               <ts e="T275" id="Seg_3638" n="e" s="T274">Jaːntarɨnan, </ts>
               <ts e="T276" id="Seg_3640" n="e" s="T275">tɨmtɨgɨnan </ts>
               <ts e="T277" id="Seg_3642" n="e" s="T276">ɨbatan </ts>
               <ts e="T278" id="Seg_3644" n="e" s="T277">barɨkaːnɨn </ts>
               <ts e="T279" id="Seg_3646" n="e" s="T278">ɨraːstɨːr. </ts>
               <ts e="T280" id="Seg_3648" n="e" s="T279">Ol </ts>
               <ts e="T281" id="Seg_3650" n="e" s="T280">dʼaktar </ts>
               <ts e="T282" id="Seg_3652" n="e" s="T281">oloror </ts>
               <ts e="T283" id="Seg_3654" n="e" s="T282">hirin, </ts>
               <ts e="T284" id="Seg_3656" n="e" s="T283">oloror </ts>
               <ts e="T285" id="Seg_3658" n="e" s="T284">dʼukaːtɨn </ts>
               <ts e="T286" id="Seg_3660" n="e" s="T285">tögürüččü </ts>
               <ts e="T287" id="Seg_3662" n="e" s="T286">alastɨːr: </ts>
               <ts e="T288" id="Seg_3664" n="e" s="T287">"Türkeːt, </ts>
               <ts e="T289" id="Seg_3666" n="e" s="T288">türkeːt, </ts>
               <ts e="T290" id="Seg_3668" n="e" s="T289">türkeːt, </ts>
               <ts e="T291" id="Seg_3670" n="e" s="T290">türkeːt, </ts>
               <ts e="T292" id="Seg_3672" n="e" s="T291">bɨrtagɨŋ </ts>
               <ts e="T293" id="Seg_3674" n="e" s="T292">bɨlɨt, </ts>
               <ts e="T294" id="Seg_3676" n="e" s="T293">kɨraːhɨŋ </ts>
               <ts e="T295" id="Seg_3678" n="e" s="T294">ɨjga. </ts>
               <ts e="T296" id="Seg_3680" n="e" s="T295">Türkeːt, </ts>
               <ts e="T297" id="Seg_3682" n="e" s="T296">türkeːt, </ts>
               <ts e="T298" id="Seg_3684" n="e" s="T297">türkeːt, </ts>
               <ts e="T299" id="Seg_3686" n="e" s="T298">bɨrtagɨŋ </ts>
               <ts e="T300" id="Seg_3688" n="e" s="T299">bulat, </ts>
               <ts e="T301" id="Seg_3690" n="e" s="T300">kɨraːhɨŋ </ts>
               <ts e="T302" id="Seg_3692" n="e" s="T301">ɨjga </ts>
               <ts e="T303" id="Seg_3694" n="e" s="T302">tu͡omuŋ-sɨramuːŋ, </ts>
               <ts e="T304" id="Seg_3696" n="e" s="T303">tu͡omuŋ-sɨramuŋ, </ts>
               <ts e="T305" id="Seg_3698" n="e" s="T304">tu͡omuŋ-sɨramuːŋ, </ts>
               <ts e="T306" id="Seg_3700" n="e" s="T305">tu͡omuŋ-sɨramuŋ! </ts>
               <ts e="T307" id="Seg_3702" n="e" s="T306">Türkeːt, </ts>
               <ts e="T308" id="Seg_3704" n="e" s="T307">bɨrtagɨŋ </ts>
               <ts e="T309" id="Seg_3706" n="e" s="T308">bulat, </ts>
               <ts e="T310" id="Seg_3708" n="e" s="T309">kɨraːhɨŋ </ts>
               <ts e="T311" id="Seg_3710" n="e" s="T310">ɨjga. </ts>
               <ts e="T312" id="Seg_3712" n="e" s="T311">Tu͡omuŋ-sɨramuːŋ, </ts>
               <ts e="T313" id="Seg_3714" n="e" s="T312">tu͡omuŋ-sɨramuŋ", </ts>
               <ts e="T314" id="Seg_3716" n="e" s="T313">diː-diː </ts>
               <ts e="T315" id="Seg_3718" n="e" s="T314">alastɨːr </ts>
               <ts e="T316" id="Seg_3720" n="e" s="T315">tögürüččü, </ts>
               <ts e="T317" id="Seg_3722" n="e" s="T316">üste </ts>
               <ts e="T318" id="Seg_3724" n="e" s="T317">kat. </ts>
               <ts e="T319" id="Seg_3726" n="e" s="T318">Onton </ts>
               <ts e="T320" id="Seg_3728" n="e" s="T319">dʼaktarɨ, </ts>
               <ts e="T321" id="Seg_3730" n="e" s="T320">oloror </ts>
               <ts e="T322" id="Seg_3732" n="e" s="T321">dʼaktarɨ, </ts>
               <ts e="T323" id="Seg_3734" n="e" s="T322">oloror </ts>
               <ts e="T324" id="Seg_3736" n="e" s="T323">iligine, </ts>
               <ts e="T325" id="Seg_3738" n="e" s="T324">konnogun </ts>
               <ts e="T326" id="Seg_3740" n="e" s="T325">annɨnan, </ts>
               <ts e="T327" id="Seg_3742" n="e" s="T326">dʼogustuk </ts>
               <ts e="T328" id="Seg_3744" n="e" s="T327">oloru͡ogun </ts>
               <ts e="T329" id="Seg_3746" n="e" s="T328">di͡en, </ts>
               <ts e="T330" id="Seg_3748" n="e" s="T329">alastɨːr. </ts>
               <ts e="T331" id="Seg_3750" n="e" s="T330">Hol </ts>
               <ts e="T332" id="Seg_3752" n="e" s="T331">alastɨːr </ts>
               <ts e="T333" id="Seg_3754" n="e" s="T332">ebi͡ennetinen </ts>
               <ts e="T334" id="Seg_3756" n="e" s="T333">tögürüččü, </ts>
               <ts e="T335" id="Seg_3758" n="e" s="T334">kün </ts>
               <ts e="T336" id="Seg_3760" n="e" s="T335">kotu. </ts>
               <ts e="T337" id="Seg_3762" n="e" s="T336">Kennitiger </ts>
               <ts e="T338" id="Seg_3764" n="e" s="T337">barbat. </ts>
               <ts e="T339" id="Seg_3766" n="e" s="T338">Kennitin </ts>
               <ts e="T340" id="Seg_3768" n="e" s="T339">di͡ek </ts>
               <ts e="T341" id="Seg_3770" n="e" s="T340">kimi </ts>
               <ts e="T342" id="Seg_3772" n="e" s="T341">da </ts>
               <ts e="T343" id="Seg_3774" n="e" s="T342">kaːmtaraːččɨta </ts>
               <ts e="T344" id="Seg_3776" n="e" s="T343">hu͡oktar, </ts>
               <ts e="T345" id="Seg_3778" n="e" s="T344">ɨ͡arakan </ts>
               <ts e="T346" id="Seg_3780" n="e" s="T345">dʼaktarɨ </ts>
               <ts e="T347" id="Seg_3782" n="e" s="T346">törüt. </ts>
               <ts e="T348" id="Seg_3784" n="e" s="T347">"Kennili͡ege", </ts>
               <ts e="T349" id="Seg_3786" n="e" s="T348">di͡enner, </ts>
               <ts e="T350" id="Seg_3788" n="e" s="T349">ol </ts>
               <ts e="T351" id="Seg_3790" n="e" s="T350">ɨjaːk. </ts>
               <ts e="T352" id="Seg_3792" n="e" s="T351">Oččogo </ts>
               <ts e="T353" id="Seg_3794" n="e" s="T352">bu͡ollagɨna </ts>
               <ts e="T354" id="Seg_3796" n="e" s="T353">du͡o </ts>
               <ts e="T355" id="Seg_3798" n="e" s="T354">bu </ts>
               <ts e="T356" id="Seg_3800" n="e" s="T355">(ɨra-) </ts>
               <ts e="T357" id="Seg_3802" n="e" s="T356">üste </ts>
               <ts e="T358" id="Seg_3804" n="e" s="T357">kat </ts>
               <ts e="T359" id="Seg_3806" n="e" s="T358">ergiter </ts>
               <ts e="T360" id="Seg_3808" n="e" s="T359">emi͡e: </ts>
               <ts e="T361" id="Seg_3810" n="e" s="T360">"Türkeːt, </ts>
               <ts e="T362" id="Seg_3812" n="e" s="T361">türkeːt, </ts>
               <ts e="T363" id="Seg_3814" n="e" s="T362">türkeːt, </ts>
               <ts e="T364" id="Seg_3816" n="e" s="T363">eteŋŋe </ts>
               <ts e="T365" id="Seg_3818" n="e" s="T364">oloru͡ogun </ts>
               <ts e="T366" id="Seg_3820" n="e" s="T365">kɨːhɨm, </ts>
               <ts e="T367" id="Seg_3822" n="e" s="T366">ogom. </ts>
               <ts e="T368" id="Seg_3824" n="e" s="T367">Türkeːt, </ts>
               <ts e="T369" id="Seg_3826" n="e" s="T368">türkeːt, </ts>
               <ts e="T370" id="Seg_3828" n="e" s="T369">türkeːt, </ts>
               <ts e="T371" id="Seg_3830" n="e" s="T370">eteŋŋe </ts>
               <ts e="T372" id="Seg_3832" n="e" s="T371">oloru͡ogun, </ts>
               <ts e="T373" id="Seg_3834" n="e" s="T372">tu͡omuŋ-suramuŋ. </ts>
               <ts e="T374" id="Seg_3836" n="e" s="T373">Pürkeːt, </ts>
               <ts e="T375" id="Seg_3838" n="e" s="T374">pürket, </ts>
               <ts e="T376" id="Seg_3840" n="e" s="T375">pürkeːt, </ts>
               <ts e="T377" id="Seg_3842" n="e" s="T376">ɨraːhɨŋ </ts>
               <ts e="T378" id="Seg_3844" n="e" s="T377">ɨjga, </ts>
               <ts e="T379" id="Seg_3846" n="e" s="T378">bɨrtagɨŋ </ts>
               <ts e="T380" id="Seg_3848" n="e" s="T379">bɨlɨkka. </ts>
               <ts e="T381" id="Seg_3850" n="e" s="T380">ɨraːhɨŋ </ts>
               <ts e="T382" id="Seg_3852" n="e" s="T381">ɨjga, </ts>
               <ts e="T383" id="Seg_3854" n="e" s="T382">bɨrtagɨŋ </ts>
               <ts e="T384" id="Seg_3856" n="e" s="T383">bɨlɨkka, </ts>
               <ts e="T385" id="Seg_3858" n="e" s="T384">tu͡omuŋ-sɨramuːŋ, </ts>
               <ts e="T386" id="Seg_3860" n="e" s="T385">tu͡omuŋ-sɨramuːŋ", </ts>
               <ts e="T387" id="Seg_3862" n="e" s="T386">di͡eččiler, </ts>
               <ts e="T388" id="Seg_3864" n="e" s="T387">üste </ts>
               <ts e="T389" id="Seg_3866" n="e" s="T388">kat </ts>
               <ts e="T390" id="Seg_3868" n="e" s="T389">ergiteːččiler. </ts>
               <ts e="T391" id="Seg_3870" n="e" s="T390">Ol </ts>
               <ts e="T392" id="Seg_3872" n="e" s="T391">gɨnan </ts>
               <ts e="T393" id="Seg_3874" n="e" s="T392">togohotuttan </ts>
               <ts e="T394" id="Seg_3876" n="e" s="T393">tuttar, </ts>
               <ts e="T395" id="Seg_3878" n="e" s="T394">ol </ts>
               <ts e="T396" id="Seg_3880" n="e" s="T395">oloror </ts>
               <ts e="T397" id="Seg_3882" n="e" s="T396">dʼaktarɨŋ, </ts>
               <ts e="T398" id="Seg_3884" n="e" s="T397">oloror </ts>
               <ts e="T399" id="Seg_3886" n="e" s="T398">togohotuttan. </ts>
               <ts e="T400" id="Seg_3888" n="e" s="T399">Oččogo </ts>
               <ts e="T401" id="Seg_3890" n="e" s="T400">bu͡ollagɨna </ts>
               <ts e="T402" id="Seg_3892" n="e" s="T401">ajɨːhɨt </ts>
               <ts e="T403" id="Seg_3894" n="e" s="T402">ɨlar </ts>
               <ts e="T404" id="Seg_3896" n="e" s="T403">baːbɨska </ts>
               <ts e="T405" id="Seg_3898" n="e" s="T404">emeːksin, </ts>
               <ts e="T406" id="Seg_3900" n="e" s="T405">ol </ts>
               <ts e="T407" id="Seg_3902" n="e" s="T406">ajɨːhɨtɨn </ts>
               <ts e="T408" id="Seg_3904" n="e" s="T407">ɨlar, </ts>
               <ts e="T409" id="Seg_3906" n="e" s="T408">uskaːn </ts>
               <ts e="T410" id="Seg_3908" n="e" s="T409">tiriːte </ts>
               <ts e="T411" id="Seg_3910" n="e" s="T410">ol </ts>
               <ts e="T412" id="Seg_3912" n="e" s="T411">ajɨːhɨtkaːna. </ts>
               <ts e="T413" id="Seg_3914" n="e" s="T412">Uskaːnɨŋ </ts>
               <ts e="T414" id="Seg_3916" n="e" s="T413">ajɨːhɨt </ts>
               <ts e="T415" id="Seg_3918" n="e" s="T414">aːttaːk </ts>
               <ts e="T416" id="Seg_3920" n="e" s="T415">bu͡olaːččɨ. </ts>
               <ts e="T417" id="Seg_3922" n="e" s="T416">Oččogo </ts>
               <ts e="T418" id="Seg_3924" n="e" s="T417">uskaːnɨ </ts>
               <ts e="T419" id="Seg_3926" n="e" s="T418">tutan </ts>
               <ts e="T420" id="Seg_3928" n="e" s="T419">baran, </ts>
               <ts e="T421" id="Seg_3930" n="e" s="T420">uskaːnɨ </ts>
               <ts e="T422" id="Seg_3932" n="e" s="T421">u͡okka </ts>
               <ts e="T423" id="Seg_3934" n="e" s="T422">ahatar, </ts>
               <ts e="T424" id="Seg_3936" n="e" s="T423">u͡otu </ts>
               <ts e="T425" id="Seg_3938" n="e" s="T424">emi͡e </ts>
               <ts e="T426" id="Seg_3940" n="e" s="T425">ahatar. </ts>
               <ts e="T427" id="Seg_3942" n="e" s="T426">"U͡ot-ehekeːniːm, </ts>
               <ts e="T428" id="Seg_3944" n="e" s="T427">ajɨːhɨppɨn </ts>
               <ts e="T429" id="Seg_3946" n="e" s="T428">ahatabɨn </ts>
               <ts e="T430" id="Seg_3948" n="e" s="T429">ogobun </ts>
               <ts e="T431" id="Seg_3950" n="e" s="T430">eteŋŋe </ts>
               <ts e="T432" id="Seg_3952" n="e" s="T431">olordu͡ogun, </ts>
               <ts e="T433" id="Seg_3954" n="e" s="T432">eteŋŋe </ts>
               <ts e="T434" id="Seg_3956" n="e" s="T433">tabɨskɨ͡agɨn", </ts>
               <ts e="T435" id="Seg_3958" n="e" s="T434">di͡en. </ts>
               <ts e="T436" id="Seg_3960" n="e" s="T435">Oččogo </ts>
               <ts e="T437" id="Seg_3962" n="e" s="T436">bu͡ollagɨna </ts>
               <ts e="T438" id="Seg_3964" n="e" s="T437">u͡ot </ts>
               <ts e="T439" id="Seg_3966" n="e" s="T438">ehekeːmmin </ts>
               <ts e="T440" id="Seg_3968" n="e" s="T439">ahatabɨn: </ts>
               <ts e="T441" id="Seg_3970" n="e" s="T440">"U͡ot </ts>
               <ts e="T442" id="Seg_3972" n="e" s="T441">ehekeːniːm, </ts>
               <ts e="T443" id="Seg_3974" n="e" s="T442">ahaː, </ts>
               <ts e="T444" id="Seg_3976" n="e" s="T443">eteŋŋe, </ts>
               <ts e="T445" id="Seg_3978" n="e" s="T444">dʼollo </ts>
               <ts e="T446" id="Seg_3980" n="e" s="T445">keteː. </ts>
               <ts e="T447" id="Seg_3982" n="e" s="T446">Tu͡ok </ts>
               <ts e="T448" id="Seg_3984" n="e" s="T447">da </ts>
               <ts e="T449" id="Seg_3986" n="e" s="T448">kuhaganɨ </ts>
               <ts e="T450" id="Seg_3988" n="e" s="T449">hugahaːtɨma, </ts>
               <ts e="T451" id="Seg_3990" n="e" s="T450">eteŋŋe </ts>
               <ts e="T452" id="Seg_3992" n="e" s="T451">olort, </ts>
               <ts e="T453" id="Seg_3994" n="e" s="T452">ogobun </ts>
               <ts e="T454" id="Seg_3996" n="e" s="T453">eteŋŋe </ts>
               <ts e="T455" id="Seg_3998" n="e" s="T454">boskoloː", </ts>
               <ts e="T456" id="Seg_4000" n="e" s="T455">di͡en. </ts>
               <ts e="T457" id="Seg_4002" n="e" s="T456">Onton </ts>
               <ts e="T458" id="Seg_4004" n="e" s="T457">ol </ts>
               <ts e="T459" id="Seg_4006" n="e" s="T458">ajɨːhɨkkaːnɨn, </ts>
               <ts e="T460" id="Seg_4008" n="e" s="T459">ahatan </ts>
               <ts e="T461" id="Seg_4010" n="e" s="T460">baraːn, </ts>
               <ts e="T462" id="Seg_4012" n="e" s="T461">togohogo </ts>
               <ts e="T463" id="Seg_4014" n="e" s="T462">ɨjɨːr. </ts>
               <ts e="T464" id="Seg_4016" n="e" s="T463">Ol </ts>
               <ts e="T465" id="Seg_4018" n="e" s="T464">togohogo </ts>
               <ts e="T466" id="Seg_4020" n="e" s="T465">baːjan </ts>
               <ts e="T467" id="Seg_4022" n="e" s="T466">keːher. </ts>
               <ts e="T468" id="Seg_4024" n="e" s="T467">Ol </ts>
               <ts e="T469" id="Seg_4026" n="e" s="T468">baːjan </ts>
               <ts e="T470" id="Seg_4028" n="e" s="T469">keːhen </ts>
               <ts e="T471" id="Seg_4030" n="e" s="T470">baraːn, </ts>
               <ts e="T472" id="Seg_4032" n="e" s="T471">ol </ts>
               <ts e="T473" id="Seg_4034" n="e" s="T472">ogolonor </ts>
               <ts e="T474" id="Seg_4036" n="e" s="T473">dʼaktar </ts>
               <ts e="T475" id="Seg_4038" n="e" s="T474">kɨ͡ajan </ts>
               <ts e="T476" id="Seg_4040" n="e" s="T475">ol </ts>
               <ts e="T477" id="Seg_4042" n="e" s="T476">kuhagannɨk </ts>
               <ts e="T478" id="Seg_4044" n="e" s="T477">(üːnner) </ts>
               <ts e="T479" id="Seg_4046" n="e" s="T478">bu͡ollagɨna, </ts>
               <ts e="T480" id="Seg_4048" n="e" s="T479">kömölöhör </ts>
               <ts e="T481" id="Seg_4050" n="e" s="T480">baːbɨska. </ts>
               <ts e="T482" id="Seg_4052" n="e" s="T481">Tobugugar </ts>
               <ts e="T483" id="Seg_4054" n="e" s="T482">olordor, </ts>
               <ts e="T484" id="Seg_4056" n="e" s="T483">(kuturugun) </ts>
               <ts e="T485" id="Seg_4058" n="e" s="T484">tördütünen, </ts>
               <ts e="T486" id="Seg_4060" n="e" s="T485">ogoto </ts>
               <ts e="T487" id="Seg_4062" n="e" s="T486">kennileːmi͡egin </ts>
               <ts e="T488" id="Seg_4064" n="e" s="T487">di͡en. </ts>
               <ts e="T489" id="Seg_4066" n="e" s="T488">Onton </ts>
               <ts e="T490" id="Seg_4068" n="e" s="T489">bɨ͡arɨn </ts>
               <ts e="T491" id="Seg_4070" n="e" s="T490">ilbijer. </ts>
               <ts e="T492" id="Seg_4072" n="e" s="T491">Ol </ts>
               <ts e="T493" id="Seg_4074" n="e" s="T492">bɨ͡arɨn </ts>
               <ts e="T494" id="Seg_4076" n="e" s="T493">ilbijen, </ts>
               <ts e="T495" id="Seg_4078" n="e" s="T494">kömölöhön </ts>
               <ts e="T496" id="Seg_4080" n="e" s="T495">ol </ts>
               <ts e="T497" id="Seg_4082" n="e" s="T496">ogotun, </ts>
               <ts e="T498" id="Seg_4084" n="e" s="T497">kömölöhön </ts>
               <ts e="T499" id="Seg_4086" n="e" s="T498">tabɨskɨːr </ts>
               <ts e="T500" id="Seg_4088" n="e" s="T499">ol </ts>
               <ts e="T501" id="Seg_4090" n="e" s="T500">dʼaktar. </ts>
               <ts e="T502" id="Seg_4092" n="e" s="T501">Ol </ts>
               <ts e="T503" id="Seg_4094" n="e" s="T502">tabɨskɨːr </ts>
               <ts e="T504" id="Seg_4096" n="e" s="T503">aːta </ts>
               <ts e="T505" id="Seg_4098" n="e" s="T504">ogo </ts>
               <ts e="T506" id="Seg_4100" n="e" s="T505">törüːre. </ts>
               <ts e="T507" id="Seg_4102" n="e" s="T506">Ol </ts>
               <ts e="T508" id="Seg_4104" n="e" s="T507">tabɨskaːn </ts>
               <ts e="T509" id="Seg_4106" n="e" s="T508">ogo </ts>
               <ts e="T510" id="Seg_4108" n="e" s="T509">tüspütüger, </ts>
               <ts e="T511" id="Seg_4110" n="e" s="T510">ol </ts>
               <ts e="T512" id="Seg_4112" n="e" s="T511">ogonu </ts>
               <ts e="T513" id="Seg_4114" n="e" s="T512">kiːnin </ts>
               <ts e="T514" id="Seg_4116" n="e" s="T513">bɨhallar, </ts>
               <ts e="T515" id="Seg_4118" n="e" s="T514">bɨhabɨt. </ts>
               <ts e="T516" id="Seg_4120" n="e" s="T515">Ol </ts>
               <ts e="T517" id="Seg_4122" n="e" s="T516">baːbɨska </ts>
               <ts e="T518" id="Seg_4124" n="e" s="T517">emeːksin </ts>
               <ts e="T519" id="Seg_4126" n="e" s="T518">bɨhar. </ts>
               <ts e="T520" id="Seg_4128" n="e" s="T519">Ol </ts>
               <ts e="T521" id="Seg_4130" n="e" s="T520">kiːnin </ts>
               <ts e="T522" id="Seg_4132" n="e" s="T521">bɨhan </ts>
               <ts e="T523" id="Seg_4134" n="e" s="T522">baraːn </ts>
               <ts e="T524" id="Seg_4136" n="e" s="T523">du͡o, </ts>
               <ts e="T525" id="Seg_4138" n="e" s="T524">baːjan </ts>
               <ts e="T526" id="Seg_4140" n="e" s="T525">keːher </ts>
               <ts e="T527" id="Seg_4142" n="e" s="T526">iŋiːr </ts>
               <ts e="T528" id="Seg_4144" n="e" s="T527">habɨnan, </ts>
               <ts e="T529" id="Seg_4146" n="e" s="T528">katɨllɨbɨt </ts>
               <ts e="T530" id="Seg_4148" n="e" s="T529">iŋiːr </ts>
               <ts e="T531" id="Seg_4150" n="e" s="T530">habɨnan </ts>
               <ts e="T532" id="Seg_4152" n="e" s="T531">baːjar, </ts>
               <ts e="T533" id="Seg_4154" n="e" s="T532">tullan </ts>
               <ts e="T534" id="Seg_4156" n="e" s="T533">tühü͡ögün </ts>
               <ts e="T535" id="Seg_4158" n="e" s="T534">köjüt, </ts>
               <ts e="T536" id="Seg_4160" n="e" s="T535">bɨha </ts>
               <ts e="T537" id="Seg_4162" n="e" s="T536">hɨtɨjan. </ts>
               <ts e="T538" id="Seg_4164" n="e" s="T537">Bi͡es </ts>
               <ts e="T539" id="Seg_4166" n="e" s="T538">kün </ts>
               <ts e="T540" id="Seg_4168" n="e" s="T539">duː, </ts>
               <ts e="T541" id="Seg_4170" n="e" s="T540">kas </ts>
               <ts e="T542" id="Seg_4172" n="e" s="T541">duː </ts>
               <ts e="T543" id="Seg_4174" n="e" s="T542">kün </ts>
               <ts e="T544" id="Seg_4176" n="e" s="T543">bu͡olan </ts>
               <ts e="T545" id="Seg_4178" n="e" s="T544">baraːn </ts>
               <ts e="T546" id="Seg_4180" n="e" s="T545">tüheːčči </ts>
               <ts e="T547" id="Seg_4182" n="e" s="T546">ogo </ts>
               <ts e="T548" id="Seg_4184" n="e" s="T547">kiːne. </ts>
               <ts e="T549" id="Seg_4186" n="e" s="T548">Onton </ts>
               <ts e="T550" id="Seg_4188" n="e" s="T549">bu͡ollagɨna </ts>
               <ts e="T551" id="Seg_4190" n="e" s="T550">du͡o </ts>
               <ts e="T552" id="Seg_4192" n="e" s="T551">ogotun </ts>
               <ts e="T553" id="Seg_4194" n="e" s="T552">inʼete </ts>
               <ts e="T554" id="Seg_4196" n="e" s="T553">tüspütün, </ts>
               <ts e="T555" id="Seg_4198" n="e" s="T554">ol </ts>
               <ts e="T556" id="Seg_4200" n="e" s="T555">ogotun </ts>
               <ts e="T557" id="Seg_4202" n="e" s="T556">inʼetin </ts>
               <ts e="T558" id="Seg_4204" n="e" s="T557">huːjar. </ts>
               <ts e="T559" id="Seg_4206" n="e" s="T558">Ogotun </ts>
               <ts e="T560" id="Seg_4208" n="e" s="T559">emi͡e </ts>
               <ts e="T561" id="Seg_4210" n="e" s="T560">huːjar, </ts>
               <ts e="T562" id="Seg_4212" n="e" s="T561">kahɨn </ts>
               <ts e="T563" id="Seg_4214" n="e" s="T562">daːganɨ </ts>
               <ts e="T564" id="Seg_4216" n="e" s="T563">hujan </ts>
               <ts e="T565" id="Seg_4218" n="e" s="T564">baraːn </ts>
               <ts e="T566" id="Seg_4220" n="e" s="T565">huːlaːn </ts>
               <ts e="T567" id="Seg_4222" n="e" s="T566">keːher </ts>
               <ts e="T568" id="Seg_4224" n="e" s="T567">bu͡olla. </ts>
               <ts e="T569" id="Seg_4226" n="e" s="T568">Onton </ts>
               <ts e="T570" id="Seg_4228" n="e" s="T569">bu͡ollagɨna </ts>
               <ts e="T571" id="Seg_4230" n="e" s="T570">du͡o </ts>
               <ts e="T572" id="Seg_4232" n="e" s="T571">((PAUSE)) </ts>
               <ts e="T573" id="Seg_4234" n="e" s="T572">bihikke </ts>
               <ts e="T574" id="Seg_4236" n="e" s="T573">össü͡ö </ts>
               <ts e="T575" id="Seg_4238" n="e" s="T574">bileːbetter </ts>
               <ts e="T576" id="Seg_4240" n="e" s="T575">bu͡ollaga. </ts>
               <ts e="T577" id="Seg_4242" n="e" s="T576">Huːlaːn </ts>
               <ts e="T578" id="Seg_4244" n="e" s="T577">keːher, </ts>
               <ts e="T579" id="Seg_4246" n="e" s="T578">maːmatɨn </ts>
               <ts e="T580" id="Seg_4248" n="e" s="T579">attɨgar </ts>
               <ts e="T581" id="Seg_4250" n="e" s="T580">uːrar. </ts>
               <ts e="T582" id="Seg_4252" n="e" s="T581">Onton </ts>
               <ts e="T583" id="Seg_4254" n="e" s="T582">du͡o </ts>
               <ts e="T584" id="Seg_4256" n="e" s="T583">ajɨːhɨtɨn </ts>
               <ts e="T585" id="Seg_4258" n="e" s="T584">(hu-) </ts>
               <ts e="T586" id="Seg_4260" n="e" s="T585">ajɨːhɨtɨn </ts>
               <ts e="T587" id="Seg_4262" n="e" s="T586">huːjan </ts>
               <ts e="T588" id="Seg_4264" n="e" s="T587">baraːn, </ts>
               <ts e="T589" id="Seg_4266" n="e" s="T588">ol </ts>
               <ts e="T590" id="Seg_4268" n="e" s="T589">ajɨːhɨtɨn </ts>
               <ts e="T591" id="Seg_4270" n="e" s="T590">möhöːgüger </ts>
               <ts e="T592" id="Seg_4272" n="e" s="T591">ugan </ts>
               <ts e="T593" id="Seg_4274" n="e" s="T592">keːhen </ts>
               <ts e="T594" id="Seg_4276" n="e" s="T593">baraːn, </ts>
               <ts e="T595" id="Seg_4278" n="e" s="T594">ol </ts>
               <ts e="T596" id="Seg_4280" n="e" s="T595">matakatɨgar, </ts>
               <ts e="T597" id="Seg_4282" n="e" s="T596">ol </ts>
               <ts e="T598" id="Seg_4284" n="e" s="T597">matakalɨːn </ts>
               <ts e="T599" id="Seg_4286" n="e" s="T598">ol </ts>
               <ts e="T600" id="Seg_4288" n="e" s="T599">(tog-) </ts>
               <ts e="T601" id="Seg_4290" n="e" s="T600">togohogo </ts>
               <ts e="T602" id="Seg_4292" n="e" s="T601">baːjan </ts>
               <ts e="T603" id="Seg_4294" n="e" s="T602">keːher. </ts>
               <ts e="T604" id="Seg_4296" n="e" s="T603">Ol </ts>
               <ts e="T605" id="Seg_4298" n="e" s="T604">togohogo </ts>
               <ts e="T606" id="Seg_4300" n="e" s="T605">turar </ts>
               <ts e="T607" id="Seg_4302" n="e" s="T606">bu, </ts>
               <ts e="T608" id="Seg_4304" n="e" s="T607">ogo </ts>
               <ts e="T609" id="Seg_4306" n="e" s="T608">inʼete </ts>
               <ts e="T610" id="Seg_4308" n="e" s="T609">üjetin </ts>
               <ts e="T611" id="Seg_4310" n="e" s="T610">turkarɨ, </ts>
               <ts e="T612" id="Seg_4312" n="e" s="T611">bu </ts>
               <ts e="T613" id="Seg_4314" n="e" s="T612">ogo </ts>
               <ts e="T614" id="Seg_4316" n="e" s="T613">töröːbüt </ts>
               <ts e="T615" id="Seg_4318" n="e" s="T614">dʼukaːtɨn </ts>
               <ts e="T616" id="Seg_4320" n="e" s="T615">ihiger, </ts>
               <ts e="T617" id="Seg_4322" n="e" s="T616">ogo </ts>
               <ts e="T618" id="Seg_4324" n="e" s="T617">töröːbüt </ts>
               <ts e="T619" id="Seg_4326" n="e" s="T618">togohotugar, </ts>
               <ts e="T620" id="Seg_4328" n="e" s="T619">dʼaktar </ts>
               <ts e="T621" id="Seg_4330" n="e" s="T620">töröːbüt </ts>
               <ts e="T622" id="Seg_4332" n="e" s="T621">togohotugar. </ts>
               <ts e="T623" id="Seg_4334" n="e" s="T622">Oččogo </ts>
               <ts e="T624" id="Seg_4336" n="e" s="T623">di͡eččiler, </ts>
               <ts e="T625" id="Seg_4338" n="e" s="T624">bɨlɨrgɨ </ts>
               <ts e="T626" id="Seg_4340" n="e" s="T625">üjetten </ts>
               <ts e="T627" id="Seg_4342" n="e" s="T626">togoholoːk </ts>
               <ts e="T628" id="Seg_4344" n="e" s="T627">huːrdu </ts>
               <ts e="T629" id="Seg_4346" n="e" s="T628">aːstaktarɨnan, </ts>
               <ts e="T630" id="Seg_4348" n="e" s="T629">dʼukaːta </ts>
               <ts e="T631" id="Seg_4350" n="e" s="T630">da </ts>
               <ts e="T632" id="Seg_4352" n="e" s="T631">hu͡ok </ts>
               <ts e="T633" id="Seg_4354" n="e" s="T632">bu͡ollagɨna, </ts>
               <ts e="T634" id="Seg_4356" n="e" s="T633">togohonu </ts>
               <ts e="T635" id="Seg_4358" n="e" s="T634">tuta </ts>
               <ts e="T636" id="Seg_4360" n="e" s="T635">turarɨn </ts>
               <ts e="T637" id="Seg_4362" n="e" s="T636">kördöktörüne. </ts>
               <ts e="T638" id="Seg_4364" n="e" s="T637">"Kaː, </ts>
               <ts e="T639" id="Seg_4366" n="e" s="T638">bu </ts>
               <ts e="T640" id="Seg_4368" n="e" s="T639">huːrka </ts>
               <ts e="T641" id="Seg_4370" n="e" s="T640">dʼaktar </ts>
               <ts e="T642" id="Seg_4372" n="e" s="T641">töröːbüt </ts>
               <ts e="T643" id="Seg_4374" n="e" s="T642">ebit, </ts>
               <ts e="T644" id="Seg_4376" n="e" s="T643">togohoto </ts>
               <ts e="T645" id="Seg_4378" n="e" s="T644">turar, </ts>
               <ts e="T646" id="Seg_4380" n="e" s="T645">töröːbüt </ts>
               <ts e="T647" id="Seg_4382" n="e" s="T646">togohoto." </ts>
               <ts e="T648" id="Seg_4384" n="e" s="T647">Dʼukaːlaːk </ts>
               <ts e="T649" id="Seg_4386" n="e" s="T648">bu͡ollagɨna, </ts>
               <ts e="T650" id="Seg_4388" n="e" s="T649">"onnuk </ts>
               <ts e="T651" id="Seg_4390" n="e" s="T650">ogo </ts>
               <ts e="T652" id="Seg_4392" n="e" s="T651">töröːbüt </ts>
               <ts e="T653" id="Seg_4394" n="e" s="T652">ebit, </ts>
               <ts e="T654" id="Seg_4396" n="e" s="T653">ol </ts>
               <ts e="T655" id="Seg_4398" n="e" s="T654">ogo </ts>
               <ts e="T656" id="Seg_4400" n="e" s="T655">töröːbüt </ts>
               <ts e="T657" id="Seg_4402" n="e" s="T656">dʼukaːta", </ts>
               <ts e="T658" id="Seg_4404" n="e" s="T657">di͡eččiler. </ts>
               <ts e="T659" id="Seg_4406" n="e" s="T658">Oččogo </ts>
               <ts e="T660" id="Seg_4408" n="e" s="T659">bu͡ollagɨna </ts>
               <ts e="T661" id="Seg_4410" n="e" s="T660">du͡o, </ts>
               <ts e="T662" id="Seg_4412" n="e" s="T661">onton </ts>
               <ts e="T663" id="Seg_4414" n="e" s="T662">bu </ts>
               <ts e="T664" id="Seg_4416" n="e" s="T663">dʼaktarɨ, </ts>
               <ts e="T665" id="Seg_4418" n="e" s="T664">ogolommut </ts>
               <ts e="T666" id="Seg_4420" n="e" s="T665">dʼaktarɨŋ </ts>
               <ts e="T667" id="Seg_4422" n="e" s="T666">bɨrtak </ts>
               <ts e="T668" id="Seg_4424" n="e" s="T667">bu͡o. </ts>
               <ts e="T669" id="Seg_4426" n="e" s="T668">Töröːbüt </ts>
               <ts e="T670" id="Seg_4428" n="e" s="T669">bɨrtagɨn </ts>
               <ts e="T671" id="Seg_4430" n="e" s="T670">ɨraːstaːn </ts>
               <ts e="T672" id="Seg_4432" n="e" s="T671">emi͡e </ts>
               <ts e="T673" id="Seg_4434" n="e" s="T672">alastɨːgɨn. </ts>
               <ts e="T674" id="Seg_4436" n="e" s="T673">Hɨrajɨn </ts>
               <ts e="T675" id="Seg_4438" n="e" s="T674">annɨnan, </ts>
               <ts e="T676" id="Seg_4440" n="e" s="T675">mu͡ojun </ts>
               <ts e="T677" id="Seg_4442" n="e" s="T676">tögürüččü </ts>
               <ts e="T678" id="Seg_4444" n="e" s="T677">üste </ts>
               <ts e="T679" id="Seg_4446" n="e" s="T678">kat </ts>
               <ts e="T680" id="Seg_4448" n="e" s="T679">alastɨːgɨn: </ts>
               <ts e="T681" id="Seg_4450" n="e" s="T680">"Türkeːt, </ts>
               <ts e="T682" id="Seg_4452" n="e" s="T681">türkeːt, </ts>
               <ts e="T683" id="Seg_4454" n="e" s="T682">bɨrtagɨŋ </ts>
               <ts e="T684" id="Seg_4456" n="e" s="T683">bɨlɨkka, </ts>
               <ts e="T685" id="Seg_4458" n="e" s="T684">ɨraːhɨŋ </ts>
               <ts e="T686" id="Seg_4460" n="e" s="T685">ɨjga, </ts>
               <ts e="T687" id="Seg_4462" n="e" s="T686">türkeːt, </ts>
               <ts e="T688" id="Seg_4464" n="e" s="T687">türkeːt, </ts>
               <ts e="T689" id="Seg_4466" n="e" s="T688">bɨrtagɨŋ </ts>
               <ts e="T690" id="Seg_4468" n="e" s="T689">bɨlɨkka, </ts>
               <ts e="T691" id="Seg_4470" n="e" s="T690">ɨraːhɨŋ </ts>
               <ts e="T692" id="Seg_4472" n="e" s="T691">ɨjga. </ts>
               <ts e="T693" id="Seg_4474" n="e" s="T692">Türkeːt-türkeːt, </ts>
               <ts e="T694" id="Seg_4476" n="e" s="T693">bɨrtagɨn </ts>
               <ts e="T695" id="Seg_4478" n="e" s="T694">bɨlɨkka, </ts>
               <ts e="T696" id="Seg_4480" n="e" s="T695">ɨraːhɨŋ </ts>
               <ts e="T697" id="Seg_4482" n="e" s="T696">ɨjga, </ts>
               <ts e="T698" id="Seg_4484" n="e" s="T697">tu͡omuŋ-sɨramuːŋ, </ts>
               <ts e="T699" id="Seg_4486" n="e" s="T698">tu͡omuŋ-sɨramuːŋ, </ts>
               <ts e="T700" id="Seg_4488" n="e" s="T699">tu͡omuŋ-sɨramuːŋ", </ts>
               <ts e="T701" id="Seg_4490" n="e" s="T700">di͡eŋŋin, </ts>
               <ts e="T702" id="Seg_4492" n="e" s="T701">di͡eŋŋin </ts>
               <ts e="T703" id="Seg_4494" n="e" s="T702">alastɨːgɨn. </ts>
               <ts e="T704" id="Seg_4496" n="e" s="T703">Ol, </ts>
               <ts e="T705" id="Seg_4498" n="e" s="T704">ol </ts>
               <ts e="T706" id="Seg_4500" n="e" s="T705">gɨnan </ts>
               <ts e="T707" id="Seg_4502" n="e" s="T706">baraːn, </ts>
               <ts e="T708" id="Seg_4504" n="e" s="T707">ogotun </ts>
               <ts e="T709" id="Seg_4506" n="e" s="T708">ɨlar </ts>
               <ts e="T710" id="Seg_4508" n="e" s="T709">baːbɨska. </ts>
               <ts e="T711" id="Seg_4510" n="e" s="T710">Onton </ts>
               <ts e="T712" id="Seg_4512" n="e" s="T711">du͡o </ts>
               <ts e="T713" id="Seg_4514" n="e" s="T712">dʼaktarɨ </ts>
               <ts e="T714" id="Seg_4516" n="e" s="T713">turu͡orar, </ts>
               <ts e="T715" id="Seg_4518" n="e" s="T714">hɨtar </ts>
               <ts e="T716" id="Seg_4520" n="e" s="T715">tellegitten </ts>
               <ts e="T717" id="Seg_4522" n="e" s="T716">ol </ts>
               <ts e="T718" id="Seg_4524" n="e" s="T717">töröːbüt </ts>
               <ts e="T719" id="Seg_4526" n="e" s="T718">dʼukaːtɨgar. </ts>
               <ts e="T720" id="Seg_4528" n="e" s="T719">Onton </ts>
               <ts e="T721" id="Seg_4530" n="e" s="T720">turan </ts>
               <ts e="T722" id="Seg_4532" n="e" s="T721">tahaːrabɨt </ts>
               <ts e="T723" id="Seg_4534" n="e" s="T722">dʼi͡ebitiger </ts>
               <ts e="T724" id="Seg_4536" n="e" s="T723">barabɨt. </ts>
               <ts e="T725" id="Seg_4538" n="e" s="T724">Hi͡eten </ts>
               <ts e="T726" id="Seg_4540" n="e" s="T725">illebit </ts>
               <ts e="T727" id="Seg_4542" n="e" s="T726">töröːbüt </ts>
               <ts e="T728" id="Seg_4544" n="e" s="T727">dʼaktarɨ, </ts>
               <ts e="T729" id="Seg_4546" n="e" s="T728">ogolommut </ts>
               <ts e="T730" id="Seg_4548" n="e" s="T729">dʼaktarɨ. </ts>
               <ts e="T731" id="Seg_4550" n="e" s="T730">Ol </ts>
               <ts e="T732" id="Seg_4552" n="e" s="T731">gɨnan </ts>
               <ts e="T733" id="Seg_4554" n="e" s="T732">baraːn, </ts>
               <ts e="T734" id="Seg_4556" n="e" s="T733">ol </ts>
               <ts e="T735" id="Seg_4558" n="e" s="T734">hi͡eten </ts>
               <ts e="T736" id="Seg_4560" n="e" s="T735">(eren) </ts>
               <ts e="T737" id="Seg_4562" n="e" s="T736">ol </ts>
               <ts e="T738" id="Seg_4564" n="e" s="T737">kepsiːr </ts>
               <ts e="T739" id="Seg_4566" n="e" s="T738">ol </ts>
               <ts e="T740" id="Seg_4568" n="e" s="T739">"kaja, </ts>
               <ts e="T741" id="Seg_4570" n="e" s="T740">ogo </ts>
               <ts e="T742" id="Seg_4572" n="e" s="T741">töröːtö", </ts>
               <ts e="T743" id="Seg_4574" n="e" s="T742">di͡en </ts>
               <ts e="T744" id="Seg_4576" n="e" s="T743">kergetteriger, </ts>
               <ts e="T745" id="Seg_4578" n="e" s="T744">dʼi͡eleːk </ts>
               <ts e="T746" id="Seg_4580" n="e" s="T745">dʼi͡eleriger. </ts>
               <ts e="T747" id="Seg_4582" n="e" s="T746">Dʼi͡eleːk </ts>
               <ts e="T748" id="Seg_4584" n="e" s="T747">uraha </ts>
               <ts e="T749" id="Seg_4586" n="e" s="T748">dʼi͡ege. </ts>
               <ts e="T750" id="Seg_4588" n="e" s="T749">Ol </ts>
               <ts e="T751" id="Seg_4590" n="e" s="T750">kepseːn </ts>
               <ts e="T752" id="Seg_4592" n="e" s="T751">kepsiːr, </ts>
               <ts e="T753" id="Seg_4594" n="e" s="T752">kajaː, </ts>
               <ts e="T754" id="Seg_4596" n="e" s="T753">ol </ts>
               <ts e="T755" id="Seg_4598" n="e" s="T754">kɨːh </ts>
               <ts e="T756" id="Seg_4600" n="e" s="T755">ogo </ts>
               <ts e="T757" id="Seg_4602" n="e" s="T756">töröːbüt </ts>
               <ts e="T758" id="Seg_4604" n="e" s="T757">bu͡ollagɨna, </ts>
               <ts e="T759" id="Seg_4606" n="e" s="T758">"kɨːh </ts>
               <ts e="T760" id="Seg_4608" n="e" s="T759">ogo", </ts>
               <ts e="T761" id="Seg_4610" n="e" s="T760">diːller. </ts>
               <ts e="T762" id="Seg_4612" n="e" s="T761">U͡ol </ts>
               <ts e="T763" id="Seg_4614" n="e" s="T762">ogo </ts>
               <ts e="T764" id="Seg_4616" n="e" s="T763">töröːbüt </ts>
               <ts e="T765" id="Seg_4618" n="e" s="T764">bu͡ollagɨna, </ts>
               <ts e="T766" id="Seg_4620" n="e" s="T765">"u͡ol </ts>
               <ts e="T767" id="Seg_4622" n="e" s="T766">ogo", </ts>
               <ts e="T768" id="Seg_4624" n="e" s="T767">diːller. </ts>
               <ts e="T769" id="Seg_4626" n="e" s="T768">Onton </ts>
               <ts e="T770" id="Seg_4628" n="e" s="T769">bu </ts>
               <ts e="T771" id="Seg_4630" n="e" s="T770">üs </ts>
               <ts e="T772" id="Seg_4632" n="e" s="T771">kün </ts>
               <ts e="T773" id="Seg_4634" n="e" s="T772">bu͡olan </ts>
               <ts e="T774" id="Seg_4636" n="e" s="T773">baraːn, </ts>
               <ts e="T775" id="Seg_4638" n="e" s="T774">agata </ts>
               <ts e="T776" id="Seg_4640" n="e" s="T775">bihigi </ts>
               <ts e="T777" id="Seg_4642" n="e" s="T776">oŋoror </ts>
               <ts e="T778" id="Seg_4644" n="e" s="T777">ogotugar. </ts>
               <ts e="T779" id="Seg_4646" n="e" s="T778">Ehete </ts>
               <ts e="T780" id="Seg_4648" n="e" s="T779">kömölöhör. </ts>
               <ts e="T781" id="Seg_4650" n="e" s="T780">Oččogo </ts>
               <ts e="T782" id="Seg_4652" n="e" s="T781">bu͡o </ts>
               <ts e="T783" id="Seg_4654" n="e" s="T782">bu </ts>
               <ts e="T784" id="Seg_4656" n="e" s="T783">bihigi </ts>
               <ts e="T785" id="Seg_4658" n="e" s="T784">du͡o </ts>
               <ts e="T786" id="Seg_4660" n="e" s="T785">tabɨllɨbɨt </ts>
               <ts e="T787" id="Seg_4662" n="e" s="T786">bige </ts>
               <ts e="T788" id="Seg_4664" n="e" s="T787">doskoːlarɨnan, </ts>
               <ts e="T789" id="Seg_4666" n="e" s="T788">bɨ͡annan, </ts>
               <ts e="T790" id="Seg_4668" n="e" s="T789">harɨː </ts>
               <ts e="T791" id="Seg_4670" n="e" s="T790">bɨ͡annan </ts>
               <ts e="T792" id="Seg_4672" n="e" s="T791">üːjen </ts>
               <ts e="T793" id="Seg_4674" n="e" s="T792">oŋoroːččular. </ts>
               <ts e="T794" id="Seg_4676" n="e" s="T793">Karɨjanɨ, </ts>
               <ts e="T795" id="Seg_4678" n="e" s="T794">bu </ts>
               <ts e="T796" id="Seg_4680" n="e" s="T795">kim </ts>
               <ts e="T797" id="Seg_4682" n="e" s="T796">eː </ts>
               <ts e="T798" id="Seg_4684" n="e" s="T797">fanʼera </ts>
               <ts e="T799" id="Seg_4686" n="e" s="T798">kördük, </ts>
               <ts e="T800" id="Seg_4688" n="e" s="T799">di͡eččiler, </ts>
               <ts e="T801" id="Seg_4690" n="e" s="T800">iti </ts>
               <ts e="T802" id="Seg_4692" n="e" s="T801">itinnigi </ts>
               <ts e="T803" id="Seg_4694" n="e" s="T802">du͡o </ts>
               <ts e="T804" id="Seg_4696" n="e" s="T803">oŋortoroːččuta </ts>
               <ts e="T805" id="Seg_4698" n="e" s="T804">hu͡oktar. </ts>
               <ts e="T806" id="Seg_4700" n="e" s="T805">Kappɨt </ts>
               <ts e="T807" id="Seg_4702" n="e" s="T806">mas </ts>
               <ts e="T808" id="Seg_4704" n="e" s="T807">tiriːte, </ts>
               <ts e="T809" id="Seg_4706" n="e" s="T808">iti </ts>
               <ts e="T810" id="Seg_4708" n="e" s="T809">fanʼera. </ts>
               <ts e="T811" id="Seg_4710" n="e" s="T810">Ol </ts>
               <ts e="T812" id="Seg_4712" n="e" s="T811">ihin </ts>
               <ts e="T813" id="Seg_4714" n="e" s="T812">bu͡ollagɨna </ts>
               <ts e="T814" id="Seg_4716" n="e" s="T813">du͡o </ts>
               <ts e="T815" id="Seg_4718" n="e" s="T814">ogo </ts>
               <ts e="T816" id="Seg_4720" n="e" s="T815">katɨ͡a, </ts>
               <ts e="T817" id="Seg_4722" n="e" s="T816">di͡eččiler, </ts>
               <ts e="T818" id="Seg_4724" n="e" s="T817">ol </ts>
               <ts e="T819" id="Seg_4726" n="e" s="T818">ihin </ts>
               <ts e="T820" id="Seg_4728" n="e" s="T819">(do-) </ts>
               <ts e="T0" id="Seg_4730" n="e" s="T820">doskonnon </ts>
               <ts e="T821" id="Seg_4732" n="e" s="T0">((…)). </ts>
               <ts e="T822" id="Seg_4734" n="e" s="T821">A </ts>
               <ts e="T823" id="Seg_4736" n="e" s="T822">doskoːŋ </ts>
               <ts e="T824" id="Seg_4738" n="e" s="T823">üːjülünnegine </ts>
               <ts e="T825" id="Seg_4740" n="e" s="T824">du͡o, </ts>
               <ts e="T826" id="Seg_4742" n="e" s="T825">üːjülünneginen, </ts>
               <ts e="T827" id="Seg_4744" n="e" s="T826">üːjülünnegine, </ts>
               <ts e="T828" id="Seg_4746" n="e" s="T827">bɨ͡annan </ts>
               <ts e="T829" id="Seg_4748" n="e" s="T828">üːjen </ts>
               <ts e="T830" id="Seg_4750" n="e" s="T829">oŋordoktoruna, </ts>
               <ts e="T831" id="Seg_4752" n="e" s="T830">ontuŋ </ts>
               <ts e="T832" id="Seg_4754" n="e" s="T831">bɨːstaːk </ts>
               <ts e="T833" id="Seg_4756" n="e" s="T832">bu͡olaːččɨ. </ts>
               <ts e="T834" id="Seg_4758" n="e" s="T833">Dosko </ts>
               <ts e="T835" id="Seg_4760" n="e" s="T834">doskotuttan </ts>
               <ts e="T836" id="Seg_4762" n="e" s="T835">emi͡e </ts>
               <ts e="T837" id="Seg_4764" n="e" s="T836">bɨːstaːk </ts>
               <ts e="T838" id="Seg_4766" n="e" s="T837">bu͡olaːččɨ. </ts>
               <ts e="T839" id="Seg_4768" n="e" s="T838">Oččogo </ts>
               <ts e="T840" id="Seg_4770" n="e" s="T839">tɨːn </ts>
               <ts e="T841" id="Seg_4772" n="e" s="T840">kiːrer </ts>
               <ts e="T842" id="Seg_4774" n="e" s="T841">bu͡ollaga, </ts>
               <ts e="T843" id="Seg_4776" n="e" s="T842">ol </ts>
               <ts e="T844" id="Seg_4778" n="e" s="T843">ihin </ts>
               <ts e="T845" id="Seg_4780" n="e" s="T844">ogoŋ </ts>
               <ts e="T846" id="Seg_4782" n="e" s="T845">zdarovaj </ts>
               <ts e="T847" id="Seg_4784" n="e" s="T846">ü͡ösküːr </ts>
               <ts e="T848" id="Seg_4786" n="e" s="T847">bu͡olla. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_4787" s="T1">SuAA_20XX_Birth_nar.001 (001.001)</ta>
            <ta e="T16" id="Seg_4788" s="T5">SuAA_20XX_Birth_nar.002 (001.002)</ta>
            <ta e="T21" id="Seg_4789" s="T16">SuAA_20XX_Birth_nar.003 (001.003)</ta>
            <ta e="T28" id="Seg_4790" s="T21">SuAA_20XX_Birth_nar.004 (001.004)</ta>
            <ta e="T35" id="Seg_4791" s="T28">SuAA_20XX_Birth_nar.005 (001.005)</ta>
            <ta e="T48" id="Seg_4792" s="T35">SuAA_20XX_Birth_nar.006 (001.006)</ta>
            <ta e="T61" id="Seg_4793" s="T48">SuAA_20XX_Birth_nar.007 (001.007)</ta>
            <ta e="T69" id="Seg_4794" s="T61">SuAA_20XX_Birth_nar.008 (001.008)</ta>
            <ta e="T74" id="Seg_4795" s="T69">SuAA_20XX_Birth_nar.009 (001.009)</ta>
            <ta e="T83" id="Seg_4796" s="T74">SuAA_20XX_Birth_nar.010 (001.010)</ta>
            <ta e="T89" id="Seg_4797" s="T83">SuAA_20XX_Birth_nar.011 (001.011)</ta>
            <ta e="T93" id="Seg_4798" s="T89">SuAA_20XX_Birth_nar.012 (001.012)</ta>
            <ta e="T105" id="Seg_4799" s="T93">SuAA_20XX_Birth_nar.013 (001.013)</ta>
            <ta e="T108" id="Seg_4800" s="T105">SuAA_20XX_Birth_nar.014 (001.014)</ta>
            <ta e="T119" id="Seg_4801" s="T108">SuAA_20XX_Birth_nar.015 (001.015)</ta>
            <ta e="T134" id="Seg_4802" s="T119">SuAA_20XX_Birth_nar.016 (001.016)</ta>
            <ta e="T144" id="Seg_4803" s="T134">SuAA_20XX_Birth_nar.017 (001.017)</ta>
            <ta e="T148" id="Seg_4804" s="T144">SuAA_20XX_Birth_nar.018 (001.018)</ta>
            <ta e="T157" id="Seg_4805" s="T148">SuAA_20XX_Birth_nar.019 (001.019)</ta>
            <ta e="T167" id="Seg_4806" s="T157">SuAA_20XX_Birth_nar.020 (001.020)</ta>
            <ta e="T174" id="Seg_4807" s="T167">SuAA_20XX_Birth_nar.021 (001.021)</ta>
            <ta e="T183" id="Seg_4808" s="T174">SuAA_20XX_Birth_nar.022 (001.022)</ta>
            <ta e="T188" id="Seg_4809" s="T183">SuAA_20XX_Birth_nar.023 (001.023)</ta>
            <ta e="T207" id="Seg_4810" s="T188">SuAA_20XX_Birth_nar.024 (001.024)</ta>
            <ta e="T216" id="Seg_4811" s="T207">SuAA_20XX_Birth_nar.025 (001.025)</ta>
            <ta e="T225" id="Seg_4812" s="T216">SuAA_20XX_Birth_nar.026 (001.026)</ta>
            <ta e="T231" id="Seg_4813" s="T225">SuAA_20XX_Birth_nar.027 (001.027)</ta>
            <ta e="T243" id="Seg_4814" s="T231">SuAA_20XX_Birth_nar.028 (001.028)</ta>
            <ta e="T254" id="Seg_4815" s="T243">SuAA_20XX_Birth_nar.029 (001.029)</ta>
            <ta e="T262" id="Seg_4816" s="T254">SuAA_20XX_Birth_nar.030 (001.030)</ta>
            <ta e="T274" id="Seg_4817" s="T262">SuAA_20XX_Birth_nar.031 (001.031)</ta>
            <ta e="T279" id="Seg_4818" s="T274">SuAA_20XX_Birth_nar.032 (001.032)</ta>
            <ta e="T287" id="Seg_4819" s="T279">SuAA_20XX_Birth_nar.033 (001.033)</ta>
            <ta e="T295" id="Seg_4820" s="T287">SuAA_20XX_Birth_nar.034 (001.034)</ta>
            <ta e="T306" id="Seg_4821" s="T295">SuAA_20XX_Birth_nar.035 (001.035)</ta>
            <ta e="T311" id="Seg_4822" s="T306">SuAA_20XX_Birth_nar.036 (001.036)</ta>
            <ta e="T318" id="Seg_4823" s="T311">SuAA_20XX_Birth_nar.037 (001.037)</ta>
            <ta e="T330" id="Seg_4824" s="T318">SuAA_20XX_Birth_nar.038 (001.038)</ta>
            <ta e="T336" id="Seg_4825" s="T330">SuAA_20XX_Birth_nar.039 (001.039)</ta>
            <ta e="T338" id="Seg_4826" s="T336">SuAA_20XX_Birth_nar.040 (001.040)</ta>
            <ta e="T347" id="Seg_4827" s="T338">SuAA_20XX_Birth_nar.041 (001.041)</ta>
            <ta e="T351" id="Seg_4828" s="T347">SuAA_20XX_Birth_nar.042 (001.042)</ta>
            <ta e="T360" id="Seg_4829" s="T351">SuAA_20XX_Birth_nar.043 (001.043)</ta>
            <ta e="T367" id="Seg_4830" s="T360">SuAA_20XX_Birth_nar.044 (001.044)</ta>
            <ta e="T373" id="Seg_4831" s="T367">SuAA_20XX_Birth_nar.045 (001.045)</ta>
            <ta e="T380" id="Seg_4832" s="T373">SuAA_20XX_Birth_nar.046 (001.046)</ta>
            <ta e="T390" id="Seg_4833" s="T380">SuAA_20XX_Birth_nar.047 (001.047)</ta>
            <ta e="T399" id="Seg_4834" s="T390">SuAA_20XX_Birth_nar.048 (001.048)</ta>
            <ta e="T412" id="Seg_4835" s="T399">SuAA_20XX_Birth_nar.049 (001.049)</ta>
            <ta e="T416" id="Seg_4836" s="T412">SuAA_20XX_Birth_nar.050 (001.050)</ta>
            <ta e="T426" id="Seg_4837" s="T416">SuAA_20XX_Birth_nar.051 (001.051)</ta>
            <ta e="T435" id="Seg_4838" s="T426">SuAA_20XX_Birth_nar.052 (001.052)</ta>
            <ta e="T440" id="Seg_4839" s="T435">SuAA_20XX_Birth_nar.053 (001.053)</ta>
            <ta e="T446" id="Seg_4840" s="T440">SuAA_20XX_Birth_nar.054 (001.053)</ta>
            <ta e="T456" id="Seg_4841" s="T446">SuAA_20XX_Birth_nar.055 (001.054)</ta>
            <ta e="T463" id="Seg_4842" s="T456">SuAA_20XX_Birth_nar.056 (001.055)</ta>
            <ta e="T467" id="Seg_4843" s="T463">SuAA_20XX_Birth_nar.057 (001.056)</ta>
            <ta e="T481" id="Seg_4844" s="T467">SuAA_20XX_Birth_nar.058 (001.057)</ta>
            <ta e="T488" id="Seg_4845" s="T481">SuAA_20XX_Birth_nar.059 (001.058)</ta>
            <ta e="T491" id="Seg_4846" s="T488">SuAA_20XX_Birth_nar.060 (001.059)</ta>
            <ta e="T501" id="Seg_4847" s="T491">SuAA_20XX_Birth_nar.061 (001.060)</ta>
            <ta e="T506" id="Seg_4848" s="T501">SuAA_20XX_Birth_nar.062 (001.061)</ta>
            <ta e="T515" id="Seg_4849" s="T506">SuAA_20XX_Birth_nar.063 (001.062)</ta>
            <ta e="T519" id="Seg_4850" s="T515">SuAA_20XX_Birth_nar.064 (001.063)</ta>
            <ta e="T537" id="Seg_4851" s="T519">SuAA_20XX_Birth_nar.065 (001.064)</ta>
            <ta e="T548" id="Seg_4852" s="T537">SuAA_20XX_Birth_nar.066 (001.065)</ta>
            <ta e="T558" id="Seg_4853" s="T548">SuAA_20XX_Birth_nar.067 (001.066)</ta>
            <ta e="T568" id="Seg_4854" s="T558">SuAA_20XX_Birth_nar.068 (001.067)</ta>
            <ta e="T576" id="Seg_4855" s="T568">SuAA_20XX_Birth_nar.069 (001.068)</ta>
            <ta e="T581" id="Seg_4856" s="T576">SuAA_20XX_Birth_nar.070 (001.069)</ta>
            <ta e="T603" id="Seg_4857" s="T581">SuAA_20XX_Birth_nar.071 (001.070)</ta>
            <ta e="T622" id="Seg_4858" s="T603">SuAA_20XX_Birth_nar.072 (001.071)</ta>
            <ta e="T637" id="Seg_4859" s="T622">SuAA_20XX_Birth_nar.073 (001.072)</ta>
            <ta e="T647" id="Seg_4860" s="T637">SuAA_20XX_Birth_nar.074 (001.073)</ta>
            <ta e="T658" id="Seg_4861" s="T647">SuAA_20XX_Birth_nar.075 (001.074)</ta>
            <ta e="T668" id="Seg_4862" s="T658">SuAA_20XX_Birth_nar.076 (001.075)</ta>
            <ta e="T673" id="Seg_4863" s="T668">SuAA_20XX_Birth_nar.077 (001.076)</ta>
            <ta e="T680" id="Seg_4864" s="T673">SuAA_20XX_Birth_nar.078 (001.077)</ta>
            <ta e="T692" id="Seg_4865" s="T680">SuAA_20XX_Birth_nar.079 (001.078)</ta>
            <ta e="T703" id="Seg_4866" s="T692">SuAA_20XX_Birth_nar.080 (001.079)</ta>
            <ta e="T710" id="Seg_4867" s="T703">SuAA_20XX_Birth_nar.081 (001.080)</ta>
            <ta e="T719" id="Seg_4868" s="T710">SuAA_20XX_Birth_nar.082 (001.081)</ta>
            <ta e="T724" id="Seg_4869" s="T719">SuAA_20XX_Birth_nar.083 (001.082)</ta>
            <ta e="T730" id="Seg_4870" s="T724">SuAA_20XX_Birth_nar.084 (001.083)</ta>
            <ta e="T746" id="Seg_4871" s="T730">SuAA_20XX_Birth_nar.085 (001.084)</ta>
            <ta e="T749" id="Seg_4872" s="T746">SuAA_20XX_Birth_nar.086 (001.085)</ta>
            <ta e="T761" id="Seg_4873" s="T749">SuAA_20XX_Birth_nar.087 (001.086)</ta>
            <ta e="T768" id="Seg_4874" s="T761">SuAA_20XX_Birth_nar.088 (001.087)</ta>
            <ta e="T778" id="Seg_4875" s="T768">SuAA_20XX_Birth_nar.089 (001.088)</ta>
            <ta e="T780" id="Seg_4876" s="T778">SuAA_20XX_Birth_nar.090 (001.089)</ta>
            <ta e="T793" id="Seg_4877" s="T780">SuAA_20XX_Birth_nar.091 (001.090)</ta>
            <ta e="T805" id="Seg_4878" s="T793">SuAA_20XX_Birth_nar.092 (001.091)</ta>
            <ta e="T810" id="Seg_4879" s="T805">SuAA_20XX_Birth_nar.093 (001.092)</ta>
            <ta e="T821" id="Seg_4880" s="T810">SuAA_20XX_Birth_nar.094 (001.093)</ta>
            <ta e="T833" id="Seg_4881" s="T821">SuAA_20XX_Birth_nar.095 (001.094)</ta>
            <ta e="T838" id="Seg_4882" s="T833">SuAA_20XX_Birth_nar.096 (001.095)</ta>
            <ta e="T848" id="Seg_4883" s="T838">SuAA_20XX_Birth_nar.097 (001.096)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_4884" s="T1">Былыргы ого төрүүрэ тогоһого. </ta>
            <ta e="T16" id="Seg_4885" s="T5">Тоһогоһо оголоро огоо… Ыаракан дьактар төрүүр кэмигэр былыргы үйэттэн тогоһогу бэлэмнээччилэр.</ta>
            <ta e="T21" id="Seg_4886" s="T16"> Туспа ураһа дьиэни тутаннар бэлэмнээччилэр.</ta>
            <ta e="T28" id="Seg_4887" s="T21">Ого төрүүр дьукаата, төрөөбүт дьукаата, диэччилэр онтон.</ta>
            <ta e="T35" id="Seg_4888" s="T28">Онно буоллагына о-о-оол тогоһону оӈорооччулар. Кэргэттэрэ туруораллар.</ta>
            <ta e="T48" id="Seg_4889" s="T35">Эһэтэ дуу, агата дууу, иньэтэ дуу көмөлөһөннөр, баайаллар тогоһону ураһага кыһайы - туора маһы.</ta>
            <ta e="T61" id="Seg_4890" s="T48">Онтон э тогоһотун уһугугар дуо кончоччу анньыллан турар баһа э.. мас буолааччы, ураһа каата.</ta>
            <ta e="T69" id="Seg_4891" s="T61">Оччого буоллагына ол тогоһоӈ буо түөһүн кураӈынан буолааччы.</ta>
            <ta e="T74" id="Seg_4892" s="T69">Дьактар онтон ыйааллан олорон оголонооччу.</ta>
            <ta e="T83" id="Seg_4893" s="T74">Ол оголонорун аннытыгар дуо, э-э олорор һиригэр лэкээ буолааччы.</ta>
            <ta e="T89" id="Seg_4894" s="T83">Дьэ, ол лэкээгэ дуо от буолааччы. </ta>
            <ta e="T93" id="Seg_4895" s="T89">Оту уураачылар туугмугар, диэннэр.</ta>
            <ta e="T105" id="Seg_4896" s="T93">Онуга дуо ого түстэгинэ:"Отугар түһэр", - диэччилэр ол иһин, оттоок оттоогун иһин.</ta>
            <ta e="T108" id="Seg_4897" s="T105">"Окко түспүт", -- диэччилэр.</ta>
            <ta e="T119" id="Seg_4898" s="T108">Э-э, дьактар оголоноругар ыраа.. былыргы үйэттэн, ыраакка баар буоллагына ол о…,</ta>
            <ta e="T134" id="Seg_4899" s="T119">баабыска - эмээксиннэрэ ыраак буол, ого төрү түһүттэрэ дьактардара ыраак баар буоллагына, һыргалаак бараннар эгэлээччилэр.</ta>
            <ta e="T144" id="Seg_4900" s="T134">Көрдөһөннөр: "Кайа огобут оччого оччого, ол кыыспыт үө..төрүөгэ,"- диэннэр. </ta>
            <ta e="T148" id="Seg_4901" s="T144">"Дьактарбыт төрүөгэ", - диэннэр эгэлээччилэр. </ta>
            <ta e="T157" id="Seg_4902" s="T148">Э-э, чугас баар буоллагына дуо ыгырааччылар ол баабыска - эмээксини. </ta>
            <ta e="T167" id="Seg_4903" s="T157">Инньэ гынан ол баабыска илии һотторо, туога барыта, чэбэр буолуогун.</ta>
            <ta e="T174" id="Seg_4904" s="T167">Һөп буолааччы, барыкаана һаӈа тутуллубатак эбиэннэ буолардаак.</ta>
            <ta e="T183" id="Seg_4905" s="T174">Һотторо да илии һотторо -- огону төрөттөрүгэр табыскыы.. дьактар табыскыырыгар.</ta>
            <ta e="T188" id="Seg_4906" s="T183">Оччого буоллагына дуо айыыһыттаак буолааччы.</ta>
            <ta e="T207" id="Seg_4907" s="T188">Ол айыыһыты буоллагына .. Маӈнай ыла кэлэ кэлэллэр, дьактардара төрөөрү гыннагына, ол туспа.. Ол туспа дьукаага кими да кииллэрбэттэр буоллага.</ta>
            <ta e="T216" id="Seg_4908" s="T207">Ким туспа дьукаа туппаттар, дьиэгэ төрөтөөччүлэр -- дьиэгэ, олорор дьукааларыгар.</ta>
            <ta e="T225" id="Seg_4909" s="T216">Ол гына баран олорор дьукааларыттан туспа оӈорооччулар, тогоһону туруорааччылар.</ta>
            <ta e="T231" id="Seg_4910" s="T225">Чэ, ол гынан киһини барыкаанын таһарааччылар. </ta>
            <ta e="T243" id="Seg_4911" s="T231">Ким да һуок буолуоктаак. Һоготок -- баабусканы гытта төрүүр дьактар эрэ буолуоктаактар онно.</ta>
            <ta e="T254" id="Seg_4912" s="T243">Оччого буоллагына дуо бу баабускаӈ кэлэр ураһа дьиэгэ, төрүүр дьиэгэ, дьукаага. </ta>
            <ta e="T262" id="Seg_4913" s="T254">Туспа тутуллубут буоллагына, дуу дьиэгэ ба оӈоһуллубут буоллагына.</ta>
            <ta e="T274" id="Seg_4914" s="T262">Ол кэлэн, кэ эгэлэн кэлэннэр, ол баабуска - эмээксинэ киирдэгинэ, ону барыкааны янтарьдаан. </ta>
            <ta e="T279" id="Seg_4915" s="T274">Янтаарынан, тымтыгынан ыбатан (убатан) барыкаанын ыраастыыр.</ta>
            <ta e="T287" id="Seg_4916" s="T279">Ол дьактар олорор һирин, олорор дьукаатын төгүрүччү аластыыр: </ta>
            <ta e="T295" id="Seg_4917" s="T287">"Түркээ-э-эт, түркээт, түркээт, түркээт, быртагыӈ -- былыт, кыраһаӈ --ыйга,</ta>
            <ta e="T306" id="Seg_4918" s="T295">Түркэ-э-эт, түркээт, түркээт, быртагыӈ -- булат, кырааһыӈ -- ыйга туомуӈ-сырамуу, туомуӈ-сырамуӈ, туомуӈ-сырамуӈ, туомуӈ-сырамуӈ!"</ta>
            <ta e="T311" id="Seg_4919" s="T306">"Түркэ-э-эт, быртагыӈ -- булат, кырааһыӈ -- ыйга. </ta>
            <ta e="T318" id="Seg_4920" s="T311">Туомуӈ-сырамууӈ, туомуӈ-сырамуӈ", - дии-дии аластыыр төгүрүччү, үстэ кат.</ta>
            <ta e="T330" id="Seg_4921" s="T318">Онтон дьактары, олорор дьактары, олорор илигинэ, конногун анныгынан, дьогустук олоруогун диэн, аластыыр.</ta>
            <ta e="T336" id="Seg_4922" s="T330">Һол аластыыр эбиэннэтинэн төгүрүччу, күн коту. </ta>
            <ta e="T338" id="Seg_4923" s="T336">Кэннитигэр барбат.</ta>
            <ta e="T347" id="Seg_4924" s="T338">Кэннитин диэк кими да каамтарааччыта һуоктар, ыаракан дьактары төрүт. </ta>
            <ta e="T351" id="Seg_4925" s="T347">Кэннилээгэ, диэннэр. Ол ыйаак.</ta>
            <ta e="T360" id="Seg_4926" s="T351">Оччого буоллагына дуо бу ыра.. үстэ кат эргитэр эмиэ:</ta>
            <ta e="T367" id="Seg_4927" s="T360">"Түркэ-э-эт, түркээт,түркээт, этэӈӈэ олоруогун кыыһым, огом.</ta>
            <ta e="T373" id="Seg_4928" s="T367">Түркээт, түркээт, түркээт, этэӈӈэ олоруогун. Туомуӈ- сурамуӈ. </ta>
            <ta e="T380" id="Seg_4929" s="T373">Пүркээт, пүркэт, прук, ырааһын --ыйга, бырдагыӈ -- былыкка.</ta>
            <ta e="T390" id="Seg_4930" s="T380">Ырааһыӈ -- ыйга, бырдагыӈ -- былыкка. Туомуӈ- сырамууӈ, туомуӈ-сырамууӈ", -- диэччилэр . Устэ кат эргитээччилэр.</ta>
            <ta e="T399" id="Seg_4931" s="T390">Уг гыма ( ол гынан) тогоһотуттан туттар, ол олорор дьактарыӈ, олорор тогоһотуттан.</ta>
            <ta e="T412" id="Seg_4932" s="T399">Оччого буоллагына айыыһыт ылар баабыска - эмээксин, ол айыыһытын, ускаан тириитэ ол айыыһыткаанна. </ta>
            <ta e="T416" id="Seg_4933" s="T412">Ускааныӈ айыыһыт ааттаак буолааччы.</ta>
            <ta e="T426" id="Seg_4934" s="T416">Оччого ускааны тутан баран, ускааны уокка аһатар. Уоту эмиэ аһатар.</ta>
            <ta e="T435" id="Seg_4935" s="T426">" Уот-эһэкэнээм, айыыһыппын аһатабын, огобун этэӈӈэ олордуогун, этэӈӈэ табыскыагын!" - диэн.</ta>
            <ta e="T440" id="Seg_4936" s="T435">Оччого буоллагына уот эһэкээммин аһатабын: </ta>
            <ta e="T446" id="Seg_4937" s="T440">"Уот эһэкээниэ-э-эм, аһа, этэӈӈэ, дьолло кэтээ. </ta>
            <ta e="T456" id="Seg_4938" s="T446">Туок да куһаганы һугаһаатыма. Этэӈӈэ олорт, огобун этэӈӈэ босколоо,"-- диэн. </ta>
            <ta e="T463" id="Seg_4939" s="T456">Онтон ол айыыһыккаанын, аһатан бараан, тогоһого ыйыыр. </ta>
            <ta e="T467" id="Seg_4940" s="T463">Ол тогоһого баайан кээһэр.</ta>
            <ta e="T481" id="Seg_4941" s="T467">Ол баайан кээһэн бараан, ол оголонор дьактар кыайан ол куһаганнык үүннэр буоллагына, көмөлөһөр баабыска.</ta>
            <ta e="T488" id="Seg_4942" s="T481">Тобугугар олордор, кутуругун төрдүтүнэн, огото кэннилээмиэгин диэн.</ta>
            <ta e="T491" id="Seg_4943" s="T488">Онтон быарын илбийэр. </ta>
            <ta e="T501" id="Seg_4944" s="T491">Ол быарын илбийэн, көмөлөһөн ол оготун, көмөлөһөн табыскыыр ол дьактар.</ta>
            <ta e="T506" id="Seg_4945" s="T501">Ол табыскыыр -- аата ого төрүүрэ. </ta>
            <ta e="T515" id="Seg_4946" s="T506">Ол табыскаан ого түспүтүгэр, ол огону киинин быһаллар, быһабыт. </ta>
            <ta e="T519" id="Seg_4947" s="T515">Ол баабыска - эмээксин быһар.</ta>
            <ta e="T537" id="Seg_4948" s="T519">Ол киинин быһан бараан дуо, баайан кээһэр иӈиир һабынан. Катыллыбын иӈиир һабынан баайар, туллан түһүөгүн койүт, быһа һытыйан.</ta>
            <ta e="T548" id="Seg_4949" s="T537">Биэс күн дуу, каас дуу күн буолан бараан түһээччи ого киинэ.</ta>
            <ta e="T558" id="Seg_4950" s="T548">Онтон буоллагына дуо оготун иньэтэ түспүтүн, ол оготун иньэтин һууйар. </ta>
            <ta e="T568" id="Seg_4951" s="T558">Оготун эмиэ һууйар, каһын дааганы һуйан бараан һулаан кээһэр буолла. </ta>
            <ta e="T576" id="Seg_4952" s="T568">Онтон буоллагына дуо… Биһиккэ өссүө билээбэттэр буоллага. </ta>
            <ta e="T581" id="Seg_4953" s="T576">Һулаан кээһэр, мааматын аттыгар уурар. </ta>
            <ta e="T603" id="Seg_4954" s="T581">Онтон дуо айыыһытын һу.. айыыһытын һууйайан бараан… Ол айыһытын мөһөөгүгэр уган кээһэн бараан, ол матакатыгар, ол матакалыын ол тогоһого баайан кэһээр.</ta>
            <ta e="T622" id="Seg_4955" s="T603">Ол тогоһогого турар бу, ого иньэтэ үйэтин туркары. Бу ого төрөөбүт дьукаатын иһигэр ого төрөөбүт тогоһотугар. Дьактар төрөөбүт тогоһотугар.</ta>
            <ta e="T637" id="Seg_4956" s="T622">Оччого диэччилэр, былыргы үйэттэн тогоһолоок һурду аастактарынан, дьукаатата да һуок буоллагына, тогоһону тута турарын көрдөктөрүнэ.</ta>
            <ta e="T647" id="Seg_4957" s="T637">"Каа, бу һууртка дьактар төрөөбүт эбит! Тогоһого турар, төрөөбүт тогоһото". </ta>
            <ta e="T658" id="Seg_4958" s="T647">Дьукаалаак буоллагына -- "Оннук ого төрөөбүт эбит, ол ого төрөөбүт дьукаата", - диэччилэр.</ta>
            <ta e="T668" id="Seg_4959" s="T658">Оччого буоллагына дуо, онтон бу дьактары, оголоммут дьактарыӈ быртак буо. </ta>
            <ta e="T673" id="Seg_4960" s="T668">Төрөөбүт быртагын ыраастаан эмиэ аластыыгын. </ta>
            <ta e="T680" id="Seg_4961" s="T673">Һырайын анныннан, муойун төгүрүччү үстэ кат аластыыгын: </ta>
            <ta e="T692" id="Seg_4962" s="T680">"Түркээт, түркээт, быртагыӈ -- былытка, арааһын -- ыйга. Түркэ-эт, түркээт, быртагыӈ -- былыкка, ырааһыӈ -- ыйга.</ta>
            <ta e="T703" id="Seg_4963" s="T692">Түркэт-түркэт, быртагын -- былыкка, кырааһыӈ -- ыйга, туомуӈ-сырамууӈ, туомуӈ-сурамууӈ, томуӈ- сурамууӈ", -- диэӈӈин, диэӈӈин аластыыгын. </ta>
            <ta e="T710" id="Seg_4964" s="T703">Ол.. ол гынан бараан, оготун ылар баабыска. </ta>
            <ta e="T719" id="Seg_4965" s="T710">Онтон дуо дьактары туруорар, һытар тэллэгиттэн ол төрөөбүт дьукаатыгар.</ta>
            <ta e="T724" id="Seg_4966" s="T719">Онтон туран таһаарабыт дьиэбитигэр барабыт. </ta>
            <ta e="T730" id="Seg_4967" s="T724">Һиэтэн иллэбит төрөөбүт дьактары, оголоммут дьактары.</ta>
            <ta e="T746" id="Seg_4968" s="T730">Ол гынан бараан, ол һиэтэн эрэн ол кэпсиир ол: "Кайа, ого төрөөтө", -- диэн кэргэттэригэр, дьиэлээк дьиэлэригэр. </ta>
            <ta e="T749" id="Seg_4969" s="T746">Дьиэлээк ураһа дьиэгэ. </ta>
            <ta e="T761" id="Seg_4970" s="T749">Ол кэпсээн кэпсиир -- кайаа, ол кыы ого төрөөбүт буоллагына, кыыс ого, дииллэр. </ta>
            <ta e="T768" id="Seg_4971" s="T761">Уол ого төрөөбүт буоллагына, уол ого, дииллэр.</ta>
            <ta e="T778" id="Seg_4972" s="T768">Онтон бу үс күн буолан бараан, агата биһиги оӈорор оготугар. </ta>
            <ta e="T780" id="Seg_4973" s="T778">Эһэтэ көмөлөһөр.</ta>
            <ta e="T793" id="Seg_4974" s="T780">Оччого буо бу биһиги дуо табыллыбыт бигэ доскооларынан, быаннан, һарыы быаннан үүйэн оӈорооччулар.</ta>
            <ta e="T805" id="Seg_4975" s="T793">Карыйаны, бу ким э-э фанера көрдүк, диэччилэр, ити итинниги дуо оӈорторооччута һуоктар.</ta>
            <ta e="T810" id="Seg_4976" s="T805">Каппыт мас тириитэ -- ити фанера. </ta>
            <ta e="T821" id="Seg_4977" s="T810">Ол иһин буоллагына дуо ого катыа, диэччилэр.</ta>
            <ta e="T833" id="Seg_4978" s="T821">А доскооӈ үүйүлүннэгинэ дуо, үүйүлүннэгинэн, үүйүлүннэгинэ, быаннан үүйэн оӈордокторуна, онтуӈ быыстаак буолааччы.</ta>
            <ta e="T838" id="Seg_4979" s="T833">Доско доскотуттаан эмиэ быыстаак буолааччы.</ta>
            <ta e="T848" id="Seg_4980" s="T838">Оччого тыын киирэр буоллага, ол иһин огоӈ здоровай үөскүүр буолла.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_4981" s="T1">Bɨlɨrgɨ ogo törüːre togohogo. </ta>
            <ta e="T16" id="Seg_4982" s="T5">Togohogo (ogoloro) (ogoː), ɨ͡arakan dʼaktar törüːr kemiger bɨlɨrgɨ üjetten togohonu belemneːččiler. </ta>
            <ta e="T21" id="Seg_4983" s="T16">Tuspa uraha dʼi͡eni tutannar belemneːččiler. </ta>
            <ta e="T28" id="Seg_4984" s="T21">Ogo törüːr dʼukaːta, töröːbüt dʼukaːta, di͡eččiler onton. </ta>
            <ta e="T35" id="Seg_4985" s="T28">Onno bu͡ollagɨna oːl togohonu oŋoroːččular, kergettere turu͡orallar. </ta>
            <ta e="T48" id="Seg_4986" s="T35">Ehete duː, agata duː, inʼete duː kömölöhönnör, baːjallar togohonu urahaga kɨhajɨː tu͡ora mahɨ. </ta>
            <ta e="T61" id="Seg_4987" s="T48">Onton, e, togohotun uhugugar du͡o končočču annʼɨllan turar (baha), eː, mas bu͡olaːččɨ, (urahakaːta). </ta>
            <ta e="T69" id="Seg_4988" s="T61">Oččogo bu͡ollagɨna ol togohoŋ bu͡o tü͡öhün kuraːgɨnan bu͡olaːččɨ. </ta>
            <ta e="T74" id="Seg_4989" s="T69">Dʼaktar onton ɨjaːllan oloron ogolonoːčču. </ta>
            <ta e="T83" id="Seg_4990" s="T74">Ol ogolonorun annɨtɨgar du͡o, eː oloror hiriger lekeː bu͡olaːččɨ. </ta>
            <ta e="T89" id="Seg_4991" s="T83">Dʼe, ol lekeːge du͡o ot bu͡olaːččɨ. </ta>
            <ta e="T93" id="Seg_4992" s="T89">Otu uːraːččɨlar tuːmugar, di͡enner. </ta>
            <ta e="T105" id="Seg_4993" s="T93">Onuga du͡o ogo tüstegine, "otugar tüher" di͡eččiler ol ihin, ottoːk ottoːgun ihin. </ta>
            <ta e="T108" id="Seg_4994" s="T105">"Okko tüspüt", di͡eččiler. </ta>
            <ta e="T119" id="Seg_4995" s="T108">Eː, dʼaktar ogolonorugar (ɨraː-) bɨlɨrgɨ üjetten, ɨraːkka baːr bu͡ollagɨna ol eː… </ta>
            <ta e="T134" id="Seg_4996" s="T119">Baːbɨska emeːksinnere ɨraːk baːr (bu͡ol-), ogo törüː tühüttere dʼaktardara ɨraːk baːr bu͡ollagɨna, hɨrgalaːk barannar egeleːččiler. </ta>
            <ta e="T144" id="Seg_4997" s="T134">Kördöhönnör, "kaja ogobut oččogo oččogo, ol kɨːspɨt (ü͡ö-) törü͡öge", di͡enner. </ta>
            <ta e="T148" id="Seg_4998" s="T144">"Dʼaktarbɨt törü͡öge", di͡enner egeleːččiler. </ta>
            <ta e="T157" id="Seg_4999" s="T148">Eː, čugas baːr bu͡ollagɨna du͡o ɨgɨraːččɨlar ol baːbɨska emeːksini. </ta>
            <ta e="T167" id="Seg_5000" s="T157">Innʼe gɨnan ol baːbɨska iliː hottoro, tu͡oga barɨta, čeber bu͡olu͡ogun. </ta>
            <ta e="T174" id="Seg_5001" s="T167">Höp bu͡olaːččɨ, barɨkaːna haŋa tutullubatak ebi͡enne bu͡olardaːk. </ta>
            <ta e="T183" id="Seg_5002" s="T174">Hottoro da, iliː hottoro, ogonu töröttörüger (tabɨskɨː-) dʼaktar tabɨskɨːrɨgar. </ta>
            <ta e="T188" id="Seg_5003" s="T183">Oččogo bu͡ollagɨna du͡o ajɨːhɨttaːk bu͡olaːččɨ. </ta>
            <ta e="T207" id="Seg_5004" s="T188">Ol ajɨːhɨtɨ bu͡ollagɨna, maŋnaj ɨla kele keleller, dʼaktardara töröːrü gɨnnagɨna, ol tuspa, ol tuspa dʼukaːga kimi da kiːllerbetter bu͡ollaga. </ta>
            <ta e="T216" id="Seg_5005" s="T207">Kim tuspa dʼukaː tuppattar, dʼi͡ege törötöːččüler, dʼi͡ege, oloror dʼukaːlarɨgar. </ta>
            <ta e="T225" id="Seg_5006" s="T216">Ol gɨnan baran oloror dʼukaːlarɨttan tuspa oŋoroːččular, togohonu turu͡oraːččɨlar. </ta>
            <ta e="T231" id="Seg_5007" s="T225">Če, ol gɨnan kihini barɨkaːnɨn taharaːččɨlar. </ta>
            <ta e="T243" id="Seg_5008" s="T231">Kim da hu͡ok bu͡olu͡oktaːk, hogotok baːbuskanɨ gɨtta törüːr dʼaktar ere bu͡olu͡oktaːktar onno. </ta>
            <ta e="T254" id="Seg_5009" s="T243">Oččogo bu͡ollagɨna du͡o bu baːbuskaŋ keler uraha dʼi͡ege, törüːr dʼi͡ege, dʼukaːga. </ta>
            <ta e="T262" id="Seg_5010" s="T254">Tuspa tutullubut bu͡ollagɨna, duː dʼi͡ege ba oŋohullubut bu͡ollagɨna. </ta>
            <ta e="T274" id="Seg_5011" s="T262">Ol kelen, ke egelen kelenner, ol baːbuska emeːksine kiːrdegine, onu barɨkaːnɨ jaːntardaːn. </ta>
            <ta e="T279" id="Seg_5012" s="T274">Jaːntarɨnan, tɨmtɨgɨnan ɨbatan barɨkaːnɨn ɨraːstɨːr. </ta>
            <ta e="T287" id="Seg_5013" s="T279">Ol dʼaktar oloror hirin, oloror dʼukaːtɨn tögürüččü alastɨːr: </ta>
            <ta e="T295" id="Seg_5014" s="T287">"Türkeːt, türkeːt, türkeːt, türkeːt, bɨrtagɨŋ bɨlɨt, kɨraːhɨŋ ɨjga. </ta>
            <ta e="T306" id="Seg_5015" s="T295">Türkeːt, türkeːt, türkeːt, bɨrtagɨŋ bulat, kɨraːhɨŋ ɨjga tu͡omuŋ-sɨramuːŋ, tu͡omuŋ-sɨramuŋ, tu͡omuŋ-sɨramuːŋ, tu͡omuŋ-sɨramuŋ! </ta>
            <ta e="T311" id="Seg_5016" s="T306">Türkeːt, bɨrtagɨŋ bulat, kɨraːhɨŋ ɨjga. </ta>
            <ta e="T318" id="Seg_5017" s="T311">Tu͡omuŋ-sɨramuːŋ, tu͡omuŋ-sɨramuŋ", diː-diː alastɨːr tögürüččü, üste kat. </ta>
            <ta e="T330" id="Seg_5018" s="T318">Onton dʼaktarɨ, oloror dʼaktarɨ, oloror iligine, konnogun annɨnan, dʼogustuk oloru͡ogun di͡en, alastɨːr. </ta>
            <ta e="T336" id="Seg_5019" s="T330">Hol alastɨːr ebi͡ennetinen tögürüččü, kün kotu. </ta>
            <ta e="T338" id="Seg_5020" s="T336">Kennitiger barbat. </ta>
            <ta e="T347" id="Seg_5021" s="T338">Kennitin di͡ek kimi da kaːmtaraːččɨta hu͡oktar, ɨ͡arakan dʼaktarɨ törüt. </ta>
            <ta e="T351" id="Seg_5022" s="T347">"Kennili͡ege", di͡enner, ol ɨjaːk. </ta>
            <ta e="T360" id="Seg_5023" s="T351">Oččogo bu͡ollagɨna du͡o bu (ɨra-) üste kat ergiter emi͡e: </ta>
            <ta e="T367" id="Seg_5024" s="T360">"Türkeːt, türkeːt, türkeːt, eteŋŋe oloru͡ogun kɨːhɨm, ogom. </ta>
            <ta e="T373" id="Seg_5025" s="T367">Türkeːt, türkeːt, türkeːt, eteŋŋe oloru͡ogun, tu͡omuŋ-suramuŋ. </ta>
            <ta e="T380" id="Seg_5026" s="T373">Pürkeːt, pürket, pürkeːt, ɨraːhɨŋ ɨjga, bɨrtagɨŋ bɨlɨkka. </ta>
            <ta e="T390" id="Seg_5027" s="T380">ɨraːhɨŋ ɨjga, bɨrtagɨŋ bɨlɨkka, tu͡omuŋ-sɨramuːŋ, tu͡omuŋ-sɨramuːŋ", di͡eččiler, üste kat ergiteːččiler. </ta>
            <ta e="T399" id="Seg_5028" s="T390">Ol gɨnan togohotuttan tuttar, ol oloror dʼaktarɨŋ, oloror togohotuttan. </ta>
            <ta e="T412" id="Seg_5029" s="T399">Oččogo bu͡ollagɨna ajɨːhɨt ɨlar baːbɨska emeːksin, ol ajɨːhɨtɨn ɨlar, uskaːn tiriːte ol ajɨːhɨtkaːna. </ta>
            <ta e="T416" id="Seg_5030" s="T412">Uskaːnɨŋ ajɨːhɨt aːttaːk bu͡olaːččɨ. </ta>
            <ta e="T426" id="Seg_5031" s="T416">Oččogo uskaːnɨ tutan baran, uskaːnɨ u͡okka ahatar, u͡otu emi͡e ahatar. </ta>
            <ta e="T435" id="Seg_5032" s="T426">"U͡ot-ehekeːniːm, ajɨːhɨppɨn ahatabɨn ogobun eteŋŋe olordu͡ogun, eteŋŋe tabɨskɨ͡agɨn", di͡en. </ta>
            <ta e="T440" id="Seg_5033" s="T435">Oččogo bu͡ollagɨna u͡ot ehekeːmmin ahatabɨn: </ta>
            <ta e="T446" id="Seg_5034" s="T440">"U͡ot ehekeːniːm, ahaː, eteŋŋe, dʼollo keteː. </ta>
            <ta e="T456" id="Seg_5035" s="T446">Tu͡ok da kuhaganɨ hugahaːtɨma, eteŋŋe olort, ogobun eteŋŋe boskoloː", di͡en. </ta>
            <ta e="T463" id="Seg_5036" s="T456">Onton ol ajɨːhɨkkaːnɨn, ahatan baraːn, togohogo ɨjɨːr. </ta>
            <ta e="T467" id="Seg_5037" s="T463">Ol togohogo baːjan keːher. </ta>
            <ta e="T481" id="Seg_5038" s="T467">Ol baːjan keːhen baraːn, ol ogolonor dʼaktar kɨ͡ajan ol kuhagannɨk (üːnner) bu͡ollagɨna, kömölöhör baːbɨska. </ta>
            <ta e="T488" id="Seg_5039" s="T481">Tobugugar olordor, (kuturugun) tördütünen, ogoto kennileːmi͡egin di͡en. </ta>
            <ta e="T491" id="Seg_5040" s="T488">Onton bɨ͡arɨn ilbijer. </ta>
            <ta e="T501" id="Seg_5041" s="T491">Ol bɨ͡arɨn ilbijen, kömölöhön ol ogotun, kömölöhön tabɨskɨːr ol dʼaktar. </ta>
            <ta e="T506" id="Seg_5042" s="T501">Ol tabɨskɨːr aːta ogo törüːre. </ta>
            <ta e="T515" id="Seg_5043" s="T506">Ol tabɨskaːn ogo tüspütüger, ol ogonu kiːnin bɨhallar, bɨhabɨt. </ta>
            <ta e="T519" id="Seg_5044" s="T515">Ol baːbɨska emeːksin bɨhar. </ta>
            <ta e="T537" id="Seg_5045" s="T519">Ol kiːnin bɨhan baraːn du͡o, baːjan keːher iŋiːr habɨnan, katɨllɨbɨt iŋiːr habɨnan baːjar, tullan tühü͡ögün köjüt, bɨha hɨtɨjan. </ta>
            <ta e="T548" id="Seg_5046" s="T537">Bi͡es kün duː, kas duː kün bu͡olan baraːn tüheːčči ogo kiːne. </ta>
            <ta e="T558" id="Seg_5047" s="T548">Onton bu͡ollagɨna du͡o ogotun inʼete tüspütün, ol ogotun inʼetin huːjar. </ta>
            <ta e="T568" id="Seg_5048" s="T558">Ogotun emi͡e huːjar, kahɨn daːganɨ hujan baraːn huːlaːn keːher bu͡olla. </ta>
            <ta e="T576" id="Seg_5049" s="T568">Onton bu͡ollagɨna du͡o ((PAUSE)) bihikke össü͡ö bileːbetter bu͡ollaga. </ta>
            <ta e="T581" id="Seg_5050" s="T576">Huːlaːn keːher, maːmatɨn attɨgar uːrar. </ta>
            <ta e="T603" id="Seg_5051" s="T581">Onton du͡o ajɨːhɨtɨn (hu-) ajɨːhɨtɨn huːjan baraːn, ol ajɨːhɨtɨn möhöːgüger ugan keːhen baraːn, ol matakatɨgar, ol matakalɨːn ol (tog-) togohogo baːjan keːher. </ta>
            <ta e="T622" id="Seg_5052" s="T603">Ol togohogo turar bu, ogo inʼete üjetin turkarɨ, bu ogo töröːbüt dʼukaːtɨn ihiger, ogo töröːbüt togohotugar, dʼaktar töröːbüt togohotugar. </ta>
            <ta e="T637" id="Seg_5053" s="T622">Oččogo di͡eččiler, bɨlɨrgɨ üjetten togoholoːk huːrdu aːstaktarɨnan, dʼukaːta da hu͡ok bu͡ollagɨna, togohonu tuta turarɨn kördöktörüne. </ta>
            <ta e="T647" id="Seg_5054" s="T637">"Kaː, bu huːrka dʼaktar töröːbüt ebit, togohoto turar, töröːbüt togohoto." </ta>
            <ta e="T658" id="Seg_5055" s="T647">Dʼukaːlaːk bu͡ollagɨna, "onnuk ogo töröːbüt ebit, ol ogo töröːbüt dʼukaːta", di͡eččiler. </ta>
            <ta e="T668" id="Seg_5056" s="T658">Oččogo bu͡ollagɨna du͡o, onton bu dʼaktarɨ, ogolommut dʼaktarɨŋ bɨrtak bu͡o. </ta>
            <ta e="T673" id="Seg_5057" s="T668">Töröːbüt bɨrtagɨn ɨraːstaːn emi͡e alastɨːgɨn. </ta>
            <ta e="T680" id="Seg_5058" s="T673">Hɨrajɨn annɨnan, mu͡ojun tögürüččü üste kat alastɨːgɨn: </ta>
            <ta e="T692" id="Seg_5059" s="T680">"Türkeːt, türkeːt, bɨrtagɨŋ bɨlɨkka, ɨraːhɨŋ ɨjga, türkeːt, türkeːt, bɨrtagɨŋ bɨlɨkka, ɨraːhɨŋ ɨjga. </ta>
            <ta e="T703" id="Seg_5060" s="T692">Türkeːt-türkeːt, bɨrtagɨn bɨlɨkka, ɨraːhɨŋ ɨjga, tu͡omuŋ-sɨramuːŋ, tu͡omuŋ-sɨramuːŋ, tu͡omuŋ-sɨramuːŋ", di͡eŋŋin, di͡eŋŋin alastɨːgɨn. </ta>
            <ta e="T710" id="Seg_5061" s="T703">Ol, ol gɨnan baraːn, ogotun ɨlar baːbɨska. </ta>
            <ta e="T719" id="Seg_5062" s="T710">Onton du͡o dʼaktarɨ turu͡orar, hɨtar tellegitten ol töröːbüt dʼukaːtɨgar. </ta>
            <ta e="T724" id="Seg_5063" s="T719">Onton turan tahaːrabɨt dʼi͡ebitiger barabɨt. </ta>
            <ta e="T730" id="Seg_5064" s="T724">Hi͡eten illebit töröːbüt dʼaktarɨ, ogolommut dʼaktarɨ. </ta>
            <ta e="T746" id="Seg_5065" s="T730">Ol gɨnan baraːn, ol hi͡eten (eren) ol kepsiːr ol "kaja, ogo töröːtö", di͡en kergetteriger, dʼi͡eleːk dʼi͡eleriger. </ta>
            <ta e="T749" id="Seg_5066" s="T746">Dʼi͡eleːk uraha dʼi͡ege. </ta>
            <ta e="T761" id="Seg_5067" s="T749">Ol kepseːn kepsiːr, kajaː, ol kɨːh ogo töröːbüt bu͡ollagɨna, "kɨːh ogo", diːller. </ta>
            <ta e="T768" id="Seg_5068" s="T761">U͡ol ogo töröːbüt bu͡ollagɨna, "u͡ol ogo", diːller. </ta>
            <ta e="T778" id="Seg_5069" s="T768">Onton bu üs kün bu͡olan baraːn, agata bihigi oŋoror ogotugar. </ta>
            <ta e="T780" id="Seg_5070" s="T778">Ehete kömölöhör. </ta>
            <ta e="T793" id="Seg_5071" s="T780">Oččogo bu͡o bu bihigi du͡o tabɨllɨbɨt bige doskoːlarɨnan, bɨ͡annan, harɨː bɨ͡annan üːjen oŋoroːččular. </ta>
            <ta e="T805" id="Seg_5072" s="T793">Karɨjanɨ, bu kim eː fanʼera kördük, di͡eččiler, iti itinnigi du͡o oŋortoroːččuta hu͡oktar. </ta>
            <ta e="T810" id="Seg_5073" s="T805">Kappɨt mas tiriːte, iti fanʼera. </ta>
            <ta e="T821" id="Seg_5074" s="T810">Ol ihin bu͡ollagɨna du͡o ogo katɨ͡a, di͡eččiler, ol ihin (do-) doskonnon ((…)).</ta>
            <ta e="T833" id="Seg_5075" s="T821">A doskoːŋ üːjülünnegine du͡o, üːjülünneginen, üːjülünnegine, bɨ͡annan üːjen oŋordoktoruna, ontuŋ bɨːstaːk bu͡olaːččɨ. </ta>
            <ta e="T838" id="Seg_5076" s="T833">Dosko doskotuttan emi͡e bɨːstaːk bu͡olaːččɨ. </ta>
            <ta e="T848" id="Seg_5077" s="T838">Oččogo tɨːn kiːrer bu͡ollaga, ol ihin ogoŋ zdarovaj ü͡ösküːr bu͡olla. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_5078" s="T1">bɨlɨr-gɨ</ta>
            <ta e="T3" id="Seg_5079" s="T2">ogo</ta>
            <ta e="T4" id="Seg_5080" s="T3">törüː-r-e</ta>
            <ta e="T5" id="Seg_5081" s="T4">togoho-go</ta>
            <ta e="T6" id="Seg_5082" s="T5">togoho-go</ta>
            <ta e="T7" id="Seg_5083" s="T6">ogo-loro</ta>
            <ta e="T8" id="Seg_5084" s="T7">ogoː</ta>
            <ta e="T9" id="Seg_5085" s="T8">ɨ͡arakan</ta>
            <ta e="T10" id="Seg_5086" s="T9">dʼaktar</ta>
            <ta e="T11" id="Seg_5087" s="T10">törüː-r</ta>
            <ta e="T12" id="Seg_5088" s="T11">kem-i-ger</ta>
            <ta e="T13" id="Seg_5089" s="T12">bɨlɨr-gɨ</ta>
            <ta e="T14" id="Seg_5090" s="T13">üje-tten</ta>
            <ta e="T15" id="Seg_5091" s="T14">togoho-nu</ta>
            <ta e="T16" id="Seg_5092" s="T15">belemneː-čči-ler</ta>
            <ta e="T17" id="Seg_5093" s="T16">tuspa</ta>
            <ta e="T18" id="Seg_5094" s="T17">uraha</ta>
            <ta e="T19" id="Seg_5095" s="T18">dʼi͡e-ni</ta>
            <ta e="T20" id="Seg_5096" s="T19">tut-an-nar</ta>
            <ta e="T21" id="Seg_5097" s="T20">belemneː-čči-ler</ta>
            <ta e="T22" id="Seg_5098" s="T21">ogo</ta>
            <ta e="T23" id="Seg_5099" s="T22">törüː-r</ta>
            <ta e="T24" id="Seg_5100" s="T23">dʼukaː-ta</ta>
            <ta e="T25" id="Seg_5101" s="T24">töröː-büt</ta>
            <ta e="T26" id="Seg_5102" s="T25">dʼukaː-ta</ta>
            <ta e="T27" id="Seg_5103" s="T26">di͡e-čči-ler</ta>
            <ta e="T28" id="Seg_5104" s="T27">onton</ta>
            <ta e="T29" id="Seg_5105" s="T28">onno</ta>
            <ta e="T30" id="Seg_5106" s="T29">bu͡ollagɨna</ta>
            <ta e="T31" id="Seg_5107" s="T30">oːl</ta>
            <ta e="T32" id="Seg_5108" s="T31">togoho-nu</ta>
            <ta e="T33" id="Seg_5109" s="T32">oŋor-oːčču-lar</ta>
            <ta e="T34" id="Seg_5110" s="T33">kerget-tere</ta>
            <ta e="T35" id="Seg_5111" s="T34">tur-u͡or-al-lar</ta>
            <ta e="T36" id="Seg_5112" s="T35">ehe-te</ta>
            <ta e="T37" id="Seg_5113" s="T36">duː</ta>
            <ta e="T38" id="Seg_5114" s="T37">aga-ta</ta>
            <ta e="T39" id="Seg_5115" s="T38">duː</ta>
            <ta e="T40" id="Seg_5116" s="T39">inʼe-te</ta>
            <ta e="T41" id="Seg_5117" s="T40">duː</ta>
            <ta e="T42" id="Seg_5118" s="T41">kömölöh-ön-nör</ta>
            <ta e="T43" id="Seg_5119" s="T42">baːj-al-lar</ta>
            <ta e="T44" id="Seg_5120" s="T43">togoho-nu</ta>
            <ta e="T45" id="Seg_5121" s="T44">uraha-ga</ta>
            <ta e="T46" id="Seg_5122" s="T45">kɨhaj-ɨː</ta>
            <ta e="T47" id="Seg_5123" s="T46">tu͡ora</ta>
            <ta e="T48" id="Seg_5124" s="T47">mah-ɨ</ta>
            <ta e="T49" id="Seg_5125" s="T48">onton</ta>
            <ta e="T50" id="Seg_5126" s="T49">e</ta>
            <ta e="T51" id="Seg_5127" s="T50">togoho-tu-n</ta>
            <ta e="T52" id="Seg_5128" s="T51">uhug-u-gar</ta>
            <ta e="T53" id="Seg_5129" s="T52">du͡o</ta>
            <ta e="T54" id="Seg_5130" s="T53">končo-čču</ta>
            <ta e="T55" id="Seg_5131" s="T54">annʼ-ɨ-ll-an</ta>
            <ta e="T56" id="Seg_5132" s="T55">tur-ar</ta>
            <ta e="T57" id="Seg_5133" s="T56">bah-a</ta>
            <ta e="T58" id="Seg_5134" s="T57">eː</ta>
            <ta e="T59" id="Seg_5135" s="T58">mas</ta>
            <ta e="T60" id="Seg_5136" s="T59">bu͡ol-aːččɨ</ta>
            <ta e="T61" id="Seg_5137" s="T60">uraha-kaːt-a</ta>
            <ta e="T62" id="Seg_5138" s="T61">oččogo</ta>
            <ta e="T63" id="Seg_5139" s="T62">bu͡ollagɨna</ta>
            <ta e="T64" id="Seg_5140" s="T63">ol</ta>
            <ta e="T65" id="Seg_5141" s="T64">togoho-ŋ</ta>
            <ta e="T66" id="Seg_5142" s="T65">bu͡o</ta>
            <ta e="T67" id="Seg_5143" s="T66">tü͡öh-ü-n</ta>
            <ta e="T68" id="Seg_5144" s="T67">kuraːg-ɨ-nan</ta>
            <ta e="T69" id="Seg_5145" s="T68">bu͡ol-aːččɨ</ta>
            <ta e="T70" id="Seg_5146" s="T69">dʼaktar</ta>
            <ta e="T71" id="Seg_5147" s="T70">onton</ta>
            <ta e="T72" id="Seg_5148" s="T71">ɨjaː-ll-an</ta>
            <ta e="T73" id="Seg_5149" s="T72">olor-on</ta>
            <ta e="T74" id="Seg_5150" s="T73">ogolon-oːčču</ta>
            <ta e="T75" id="Seg_5151" s="T74">ol</ta>
            <ta e="T76" id="Seg_5152" s="T75">ogolon-or-u-n</ta>
            <ta e="T77" id="Seg_5153" s="T76">annɨ-tɨ-gar</ta>
            <ta e="T78" id="Seg_5154" s="T77">du͡o</ta>
            <ta e="T79" id="Seg_5155" s="T78">eː</ta>
            <ta e="T80" id="Seg_5156" s="T79">olor-or</ta>
            <ta e="T81" id="Seg_5157" s="T80">hir-i-ger</ta>
            <ta e="T82" id="Seg_5158" s="T81">lekeː</ta>
            <ta e="T83" id="Seg_5159" s="T82">bu͡ol-aːččɨ</ta>
            <ta e="T84" id="Seg_5160" s="T83">dʼe</ta>
            <ta e="T85" id="Seg_5161" s="T84">ol</ta>
            <ta e="T86" id="Seg_5162" s="T85">lekeː-ge</ta>
            <ta e="T87" id="Seg_5163" s="T86">du͡o</ta>
            <ta e="T88" id="Seg_5164" s="T87">ot</ta>
            <ta e="T89" id="Seg_5165" s="T88">bu͡ol-aːččɨ</ta>
            <ta e="T90" id="Seg_5166" s="T89">ot-u</ta>
            <ta e="T91" id="Seg_5167" s="T90">uːr-aːččɨ-lar</ta>
            <ta e="T92" id="Seg_5168" s="T91">tuːm-u-gar</ta>
            <ta e="T93" id="Seg_5169" s="T92">di͡e-n-ner</ta>
            <ta e="T94" id="Seg_5170" s="T93">onu-ga</ta>
            <ta e="T95" id="Seg_5171" s="T94">du͡o</ta>
            <ta e="T96" id="Seg_5172" s="T95">ogo</ta>
            <ta e="T97" id="Seg_5173" s="T96">tüs-teg-ine</ta>
            <ta e="T98" id="Seg_5174" s="T97">ot-u-gar</ta>
            <ta e="T99" id="Seg_5175" s="T98">tüh-er</ta>
            <ta e="T100" id="Seg_5176" s="T99">di͡e-čči-ler</ta>
            <ta e="T101" id="Seg_5177" s="T100">ol</ta>
            <ta e="T102" id="Seg_5178" s="T101">ihin</ta>
            <ta e="T103" id="Seg_5179" s="T102">ot-toːk</ta>
            <ta e="T104" id="Seg_5180" s="T103">ot-toːg-u-n</ta>
            <ta e="T105" id="Seg_5181" s="T104">ihin</ta>
            <ta e="T106" id="Seg_5182" s="T105">ok-ko</ta>
            <ta e="T107" id="Seg_5183" s="T106">tüs-püt</ta>
            <ta e="T108" id="Seg_5184" s="T107">di͡e-čči-ler</ta>
            <ta e="T109" id="Seg_5185" s="T108">eː</ta>
            <ta e="T110" id="Seg_5186" s="T109">dʼaktar</ta>
            <ta e="T111" id="Seg_5187" s="T110">ogolon-or-u-gar</ta>
            <ta e="T113" id="Seg_5188" s="T112">bɨlɨr-gɨ</ta>
            <ta e="T114" id="Seg_5189" s="T113">üje-tten</ta>
            <ta e="T115" id="Seg_5190" s="T114">ɨraːk-ka</ta>
            <ta e="T116" id="Seg_5191" s="T115">baːr</ta>
            <ta e="T117" id="Seg_5192" s="T116">bu͡ol-lag-ɨna</ta>
            <ta e="T118" id="Seg_5193" s="T117">ol</ta>
            <ta e="T119" id="Seg_5194" s="T118">eː</ta>
            <ta e="T120" id="Seg_5195" s="T119">baːbɨska</ta>
            <ta e="T121" id="Seg_5196" s="T120">emeːksin-nere</ta>
            <ta e="T122" id="Seg_5197" s="T121">ɨraːk</ta>
            <ta e="T123" id="Seg_5198" s="T122">baːr</ta>
            <ta e="T124" id="Seg_5199" s="T123">bu͡ol</ta>
            <ta e="T125" id="Seg_5200" s="T124">ogo</ta>
            <ta e="T126" id="Seg_5201" s="T125">tör-üː</ta>
            <ta e="T127" id="Seg_5202" s="T126">tü-hüt-tere</ta>
            <ta e="T128" id="Seg_5203" s="T127">dʼaktar-dara</ta>
            <ta e="T129" id="Seg_5204" s="T128">ɨraːk</ta>
            <ta e="T130" id="Seg_5205" s="T129">baːr</ta>
            <ta e="T131" id="Seg_5206" s="T130">bu͡ol-lag-ɨna</ta>
            <ta e="T132" id="Seg_5207" s="T131">hɨrga-laːk</ta>
            <ta e="T133" id="Seg_5208" s="T132">bar-an-nar</ta>
            <ta e="T134" id="Seg_5209" s="T133">egel-eːčči-ler</ta>
            <ta e="T135" id="Seg_5210" s="T134">kördöh-ön-nör</ta>
            <ta e="T136" id="Seg_5211" s="T135">kaja</ta>
            <ta e="T137" id="Seg_5212" s="T136">ogo-but</ta>
            <ta e="T138" id="Seg_5213" s="T137">oččogo</ta>
            <ta e="T139" id="Seg_5214" s="T138">oččogo</ta>
            <ta e="T140" id="Seg_5215" s="T139">ol</ta>
            <ta e="T141" id="Seg_5216" s="T140">kɨːs-pɨt</ta>
            <ta e="T143" id="Seg_5217" s="T142">tör-ü͡ög-e</ta>
            <ta e="T144" id="Seg_5218" s="T143">di͡e-n-ner</ta>
            <ta e="T145" id="Seg_5219" s="T144">dʼaktar-bɨt</ta>
            <ta e="T146" id="Seg_5220" s="T145">tör-ü͡ög-e</ta>
            <ta e="T147" id="Seg_5221" s="T146">di͡e-n-ner</ta>
            <ta e="T148" id="Seg_5222" s="T147">egel-eːčči-ler</ta>
            <ta e="T149" id="Seg_5223" s="T148">eː</ta>
            <ta e="T150" id="Seg_5224" s="T149">čugas</ta>
            <ta e="T151" id="Seg_5225" s="T150">baːr</ta>
            <ta e="T152" id="Seg_5226" s="T151">bu͡ol-lag-ɨna</ta>
            <ta e="T153" id="Seg_5227" s="T152">du͡o</ta>
            <ta e="T154" id="Seg_5228" s="T153">ɨgɨr-aːččɨ-lar</ta>
            <ta e="T155" id="Seg_5229" s="T154">ol</ta>
            <ta e="T156" id="Seg_5230" s="T155">baːbɨska</ta>
            <ta e="T157" id="Seg_5231" s="T156">emeːksin-i</ta>
            <ta e="T158" id="Seg_5232" s="T157">innʼe</ta>
            <ta e="T159" id="Seg_5233" s="T158">gɨn-an</ta>
            <ta e="T160" id="Seg_5234" s="T159">ol</ta>
            <ta e="T161" id="Seg_5235" s="T160">baːbɨska</ta>
            <ta e="T162" id="Seg_5236" s="T161">iliː</ta>
            <ta e="T163" id="Seg_5237" s="T162">hottor-o</ta>
            <ta e="T164" id="Seg_5238" s="T163">tu͡og-a</ta>
            <ta e="T165" id="Seg_5239" s="T164">barɨ-ta</ta>
            <ta e="T166" id="Seg_5240" s="T165">čeber</ta>
            <ta e="T167" id="Seg_5241" s="T166">bu͡ol-u͡og-u-n</ta>
            <ta e="T168" id="Seg_5242" s="T167">höp</ta>
            <ta e="T169" id="Seg_5243" s="T168">bu͡ol-aːččɨ</ta>
            <ta e="T170" id="Seg_5244" s="T169">barɨ-kaːn-a</ta>
            <ta e="T171" id="Seg_5245" s="T170">haŋa</ta>
            <ta e="T172" id="Seg_5246" s="T171">tut-u-ll-u-batak</ta>
            <ta e="T173" id="Seg_5247" s="T172">ebi͡enne</ta>
            <ta e="T174" id="Seg_5248" s="T173">bu͡ol-ar-daːk</ta>
            <ta e="T175" id="Seg_5249" s="T174">hottor-o</ta>
            <ta e="T176" id="Seg_5250" s="T175">da</ta>
            <ta e="T177" id="Seg_5251" s="T176">iliː</ta>
            <ta e="T178" id="Seg_5252" s="T177">hottor-o</ta>
            <ta e="T179" id="Seg_5253" s="T178">ogo-nu</ta>
            <ta e="T180" id="Seg_5254" s="T179">tör-ött-ör-ü-ger</ta>
            <ta e="T181" id="Seg_5255" s="T180">tabɨskɨː</ta>
            <ta e="T182" id="Seg_5256" s="T181">dʼaktar</ta>
            <ta e="T183" id="Seg_5257" s="T182">tabɨskɨː-r-ɨ-gar</ta>
            <ta e="T184" id="Seg_5258" s="T183">oččogo</ta>
            <ta e="T185" id="Seg_5259" s="T184">bu͡ollagɨna</ta>
            <ta e="T186" id="Seg_5260" s="T185">du͡o</ta>
            <ta e="T187" id="Seg_5261" s="T186">ajɨːhɨt-taːk</ta>
            <ta e="T188" id="Seg_5262" s="T187">bu͡ol-aːččɨ</ta>
            <ta e="T189" id="Seg_5263" s="T188">ol</ta>
            <ta e="T190" id="Seg_5264" s="T189">ajɨːhɨt-ɨ</ta>
            <ta e="T191" id="Seg_5265" s="T190">bu͡ollagɨna</ta>
            <ta e="T192" id="Seg_5266" s="T191">maŋnaj</ta>
            <ta e="T193" id="Seg_5267" s="T192">ɨl-a</ta>
            <ta e="T194" id="Seg_5268" s="T193">kel-e</ta>
            <ta e="T195" id="Seg_5269" s="T194">kel-el-ler</ta>
            <ta e="T196" id="Seg_5270" s="T195">dʼaktar-dara</ta>
            <ta e="T197" id="Seg_5271" s="T196">tör-öːrü</ta>
            <ta e="T198" id="Seg_5272" s="T197">gɨn-nag-ɨna</ta>
            <ta e="T199" id="Seg_5273" s="T198">ol</ta>
            <ta e="T200" id="Seg_5274" s="T199">tuspa</ta>
            <ta e="T201" id="Seg_5275" s="T200">ol</ta>
            <ta e="T202" id="Seg_5276" s="T201">tuspa</ta>
            <ta e="T203" id="Seg_5277" s="T202">dʼukaː-ga</ta>
            <ta e="T204" id="Seg_5278" s="T203">kim-i</ta>
            <ta e="T205" id="Seg_5279" s="T204">da</ta>
            <ta e="T206" id="Seg_5280" s="T205">kiːl-ler-bet-ter</ta>
            <ta e="T207" id="Seg_5281" s="T206">bu͡ollaga</ta>
            <ta e="T208" id="Seg_5282" s="T207">kim</ta>
            <ta e="T209" id="Seg_5283" s="T208">tuspa</ta>
            <ta e="T210" id="Seg_5284" s="T209">dʼukaː</ta>
            <ta e="T211" id="Seg_5285" s="T210">tup-pat-tar</ta>
            <ta e="T212" id="Seg_5286" s="T211">dʼi͡e-ge</ta>
            <ta e="T213" id="Seg_5287" s="T212">tör-ö-t-öːččü-ler</ta>
            <ta e="T214" id="Seg_5288" s="T213">dʼi͡e-ge</ta>
            <ta e="T215" id="Seg_5289" s="T214">olor-or</ta>
            <ta e="T216" id="Seg_5290" s="T215">dʼukaː-larɨ-gar</ta>
            <ta e="T217" id="Seg_5291" s="T216">ol</ta>
            <ta e="T218" id="Seg_5292" s="T217">gɨn-an</ta>
            <ta e="T219" id="Seg_5293" s="T218">baran</ta>
            <ta e="T220" id="Seg_5294" s="T219">olor-or</ta>
            <ta e="T221" id="Seg_5295" s="T220">dʼukaː-larɨ-ttan</ta>
            <ta e="T222" id="Seg_5296" s="T221">tuspa</ta>
            <ta e="T223" id="Seg_5297" s="T222">oŋor-oːčču-lar</ta>
            <ta e="T224" id="Seg_5298" s="T223">togoho-nu</ta>
            <ta e="T225" id="Seg_5299" s="T224">tur-u͡or-aːččɨ-lar</ta>
            <ta e="T226" id="Seg_5300" s="T225">če</ta>
            <ta e="T227" id="Seg_5301" s="T226">ol</ta>
            <ta e="T228" id="Seg_5302" s="T227">gɨn-an</ta>
            <ta e="T229" id="Seg_5303" s="T228">kihi-ni</ta>
            <ta e="T230" id="Seg_5304" s="T229">barɨ-kaːn-ɨ-n</ta>
            <ta e="T231" id="Seg_5305" s="T230">tahar-aːččɨ-lar</ta>
            <ta e="T232" id="Seg_5306" s="T231">kim</ta>
            <ta e="T233" id="Seg_5307" s="T232">da</ta>
            <ta e="T234" id="Seg_5308" s="T233">hu͡ok</ta>
            <ta e="T235" id="Seg_5309" s="T234">bu͡ol-u͡ok-taːk</ta>
            <ta e="T236" id="Seg_5310" s="T235">hogotok</ta>
            <ta e="T237" id="Seg_5311" s="T236">baːbuska-nɨ</ta>
            <ta e="T238" id="Seg_5312" s="T237">gɨtta</ta>
            <ta e="T239" id="Seg_5313" s="T238">törüː-r</ta>
            <ta e="T240" id="Seg_5314" s="T239">dʼaktar</ta>
            <ta e="T241" id="Seg_5315" s="T240">ere</ta>
            <ta e="T242" id="Seg_5316" s="T241">bu͡ol-u͡ok-taːk-tar</ta>
            <ta e="T243" id="Seg_5317" s="T242">onno</ta>
            <ta e="T244" id="Seg_5318" s="T243">oččogo</ta>
            <ta e="T245" id="Seg_5319" s="T244">bu͡ollagɨna</ta>
            <ta e="T246" id="Seg_5320" s="T245">du͡o</ta>
            <ta e="T247" id="Seg_5321" s="T246">bu</ta>
            <ta e="T248" id="Seg_5322" s="T247">baːbuska-ŋ</ta>
            <ta e="T249" id="Seg_5323" s="T248">kel-er</ta>
            <ta e="T250" id="Seg_5324" s="T249">uraha</ta>
            <ta e="T251" id="Seg_5325" s="T250">dʼi͡e-ge</ta>
            <ta e="T252" id="Seg_5326" s="T251">törüː-r</ta>
            <ta e="T253" id="Seg_5327" s="T252">dʼi͡e-ge</ta>
            <ta e="T254" id="Seg_5328" s="T253">dʼukaː-ga</ta>
            <ta e="T255" id="Seg_5329" s="T254">tuspa</ta>
            <ta e="T256" id="Seg_5330" s="T255">tut-u-ll-u-but</ta>
            <ta e="T257" id="Seg_5331" s="T256">bu͡ol-lag-ɨna</ta>
            <ta e="T258" id="Seg_5332" s="T257">duː</ta>
            <ta e="T259" id="Seg_5333" s="T258">dʼi͡e-ge</ta>
            <ta e="T260" id="Seg_5334" s="T259">ba</ta>
            <ta e="T261" id="Seg_5335" s="T260">oŋohull-u-but</ta>
            <ta e="T262" id="Seg_5336" s="T261">bu͡ol-lag-ɨna</ta>
            <ta e="T263" id="Seg_5337" s="T262">ol</ta>
            <ta e="T264" id="Seg_5338" s="T263">kel-en</ta>
            <ta e="T265" id="Seg_5339" s="T264">ke</ta>
            <ta e="T266" id="Seg_5340" s="T265">egel-en</ta>
            <ta e="T267" id="Seg_5341" s="T266">kel-en-ner</ta>
            <ta e="T268" id="Seg_5342" s="T267">ol</ta>
            <ta e="T269" id="Seg_5343" s="T268">baːbuska</ta>
            <ta e="T270" id="Seg_5344" s="T269">emeːksin-e</ta>
            <ta e="T271" id="Seg_5345" s="T270">kiːr-deg-ine</ta>
            <ta e="T272" id="Seg_5346" s="T271">o-nu</ta>
            <ta e="T273" id="Seg_5347" s="T272">barɨ-kaːn-ɨ</ta>
            <ta e="T274" id="Seg_5348" s="T273">jaːntar-daː-n</ta>
            <ta e="T275" id="Seg_5349" s="T274">jaːntar-ɨ-nan</ta>
            <ta e="T276" id="Seg_5350" s="T275">tɨmtɨg-ɨ-nan</ta>
            <ta e="T277" id="Seg_5351" s="T276">ɨbat-an</ta>
            <ta e="T278" id="Seg_5352" s="T277">barɨ-kaːn-ɨ-n</ta>
            <ta e="T279" id="Seg_5353" s="T278">ɨraːstɨː-r</ta>
            <ta e="T280" id="Seg_5354" s="T279">ol</ta>
            <ta e="T281" id="Seg_5355" s="T280">dʼaktar</ta>
            <ta e="T282" id="Seg_5356" s="T281">olor-or</ta>
            <ta e="T283" id="Seg_5357" s="T282">hir-i-n</ta>
            <ta e="T284" id="Seg_5358" s="T283">olor-or</ta>
            <ta e="T285" id="Seg_5359" s="T284">dʼukaː-tɨ-n</ta>
            <ta e="T286" id="Seg_5360" s="T285">tögürüččü</ta>
            <ta e="T287" id="Seg_5361" s="T286">alastɨː-r</ta>
            <ta e="T288" id="Seg_5362" s="T287">türkeːt</ta>
            <ta e="T289" id="Seg_5363" s="T288">türkeːt</ta>
            <ta e="T290" id="Seg_5364" s="T289">türkeːt</ta>
            <ta e="T291" id="Seg_5365" s="T290">türkeːt</ta>
            <ta e="T292" id="Seg_5366" s="T291">bɨrtag-ɨ-ŋ</ta>
            <ta e="T293" id="Seg_5367" s="T292">bɨlɨt</ta>
            <ta e="T294" id="Seg_5368" s="T293">kɨraːh-ɨ-ŋ</ta>
            <ta e="T295" id="Seg_5369" s="T294">ɨj-ga</ta>
            <ta e="T296" id="Seg_5370" s="T295">türkeːt</ta>
            <ta e="T297" id="Seg_5371" s="T296">türkeːt</ta>
            <ta e="T298" id="Seg_5372" s="T297">türkeːt</ta>
            <ta e="T299" id="Seg_5373" s="T298">bɨrtag-ɨ-ŋ</ta>
            <ta e="T300" id="Seg_5374" s="T299">bulat</ta>
            <ta e="T301" id="Seg_5375" s="T300">kɨraːh-ɨ-ŋ</ta>
            <ta e="T302" id="Seg_5376" s="T301">ɨj-ga</ta>
            <ta e="T303" id="Seg_5377" s="T302">tu͡om-u-ŋ-sɨram-uː-ŋ</ta>
            <ta e="T304" id="Seg_5378" s="T303">tu͡om-u-ŋ-sɨram-u-ŋ</ta>
            <ta e="T305" id="Seg_5379" s="T304">tu͡om-u-ŋ-sɨram-uː-ŋ</ta>
            <ta e="T306" id="Seg_5380" s="T305">tu͡om-u-ŋ-sɨram-u-ŋ</ta>
            <ta e="T307" id="Seg_5381" s="T306">türkeːt</ta>
            <ta e="T308" id="Seg_5382" s="T307">bɨrtag-ɨ-ŋ</ta>
            <ta e="T309" id="Seg_5383" s="T308">bulat</ta>
            <ta e="T310" id="Seg_5384" s="T309">kɨraːh-ɨ-ŋ</ta>
            <ta e="T311" id="Seg_5385" s="T310">ɨj-ga</ta>
            <ta e="T312" id="Seg_5386" s="T311">tu͡om-u-ŋ-sɨram-uː-ŋ</ta>
            <ta e="T313" id="Seg_5387" s="T312">tu͡om-u-ŋ-sɨram-u-ŋ</ta>
            <ta e="T314" id="Seg_5388" s="T313">d-iː-d-iː</ta>
            <ta e="T315" id="Seg_5389" s="T314">alastɨː-r</ta>
            <ta e="T316" id="Seg_5390" s="T315">tögürüččü</ta>
            <ta e="T317" id="Seg_5391" s="T316">üs-te</ta>
            <ta e="T318" id="Seg_5392" s="T317">kat</ta>
            <ta e="T319" id="Seg_5393" s="T318">onton</ta>
            <ta e="T320" id="Seg_5394" s="T319">dʼaktar-ɨ</ta>
            <ta e="T321" id="Seg_5395" s="T320">olor-or</ta>
            <ta e="T322" id="Seg_5396" s="T321">dʼaktar-ɨ</ta>
            <ta e="T323" id="Seg_5397" s="T322">olor-or</ta>
            <ta e="T324" id="Seg_5398" s="T323">ilig-ine</ta>
            <ta e="T325" id="Seg_5399" s="T324">konnog-u-n</ta>
            <ta e="T326" id="Seg_5400" s="T325">ann-ɨ-nan</ta>
            <ta e="T327" id="Seg_5401" s="T326">dʼogus-tuk</ta>
            <ta e="T328" id="Seg_5402" s="T327">olor-u͡og-u-n</ta>
            <ta e="T329" id="Seg_5403" s="T328">di͡e-n</ta>
            <ta e="T330" id="Seg_5404" s="T329">alastɨː-r</ta>
            <ta e="T331" id="Seg_5405" s="T330">hol</ta>
            <ta e="T332" id="Seg_5406" s="T331">alastɨː-r</ta>
            <ta e="T333" id="Seg_5407" s="T332">ebi͡enne-ti-nen</ta>
            <ta e="T334" id="Seg_5408" s="T333">tögürüččü</ta>
            <ta e="T335" id="Seg_5409" s="T334">kün</ta>
            <ta e="T336" id="Seg_5410" s="T335">kotu</ta>
            <ta e="T337" id="Seg_5411" s="T336">kenni-ti-ger</ta>
            <ta e="T338" id="Seg_5412" s="T337">bar-bat</ta>
            <ta e="T339" id="Seg_5413" s="T338">kenni-ti-n</ta>
            <ta e="T340" id="Seg_5414" s="T339">di͡ek</ta>
            <ta e="T341" id="Seg_5415" s="T340">kim-i</ta>
            <ta e="T342" id="Seg_5416" s="T341">da</ta>
            <ta e="T343" id="Seg_5417" s="T342">kaːm-tar-aːččɨ-ta</ta>
            <ta e="T344" id="Seg_5418" s="T343">hu͡ok-tar</ta>
            <ta e="T345" id="Seg_5419" s="T344">ɨ͡arakan</ta>
            <ta e="T346" id="Seg_5420" s="T345">dʼaktar-ɨ</ta>
            <ta e="T347" id="Seg_5421" s="T346">törüt</ta>
            <ta e="T348" id="Seg_5422" s="T347">kenn-i-l-i͡eg-e</ta>
            <ta e="T349" id="Seg_5423" s="T348">di͡e-n-ner</ta>
            <ta e="T350" id="Seg_5424" s="T349">ol</ta>
            <ta e="T351" id="Seg_5425" s="T350">ɨjaːk</ta>
            <ta e="T352" id="Seg_5426" s="T351">oččogo</ta>
            <ta e="T353" id="Seg_5427" s="T352">bu͡ollagɨna</ta>
            <ta e="T354" id="Seg_5428" s="T353">du͡o</ta>
            <ta e="T355" id="Seg_5429" s="T354">bu</ta>
            <ta e="T357" id="Seg_5430" s="T356">üs-te</ta>
            <ta e="T358" id="Seg_5431" s="T357">kat</ta>
            <ta e="T359" id="Seg_5432" s="T358">ergi-t-er</ta>
            <ta e="T360" id="Seg_5433" s="T359">emi͡e</ta>
            <ta e="T361" id="Seg_5434" s="T360">türkeːt</ta>
            <ta e="T362" id="Seg_5435" s="T361">türkeːt</ta>
            <ta e="T363" id="Seg_5436" s="T362">türkeːt</ta>
            <ta e="T364" id="Seg_5437" s="T363">eteŋŋe</ta>
            <ta e="T365" id="Seg_5438" s="T364">olor-u͡og-u-n</ta>
            <ta e="T366" id="Seg_5439" s="T365">kɨːh-ɨ-m</ta>
            <ta e="T367" id="Seg_5440" s="T366">ogo-m</ta>
            <ta e="T368" id="Seg_5441" s="T367">türkeːt</ta>
            <ta e="T369" id="Seg_5442" s="T368">türkeːt</ta>
            <ta e="T370" id="Seg_5443" s="T369">türkeːt</ta>
            <ta e="T371" id="Seg_5444" s="T370">eteŋŋe</ta>
            <ta e="T372" id="Seg_5445" s="T371">olor-u͡og-u-n</ta>
            <ta e="T373" id="Seg_5446" s="T372">tu͡om-u-ŋ-suram-u-ŋ</ta>
            <ta e="T374" id="Seg_5447" s="T373">pürkeːt</ta>
            <ta e="T375" id="Seg_5448" s="T374">pürket</ta>
            <ta e="T376" id="Seg_5449" s="T375">pürkeːt</ta>
            <ta e="T377" id="Seg_5450" s="T376">ɨraːh-ɨ-ŋ</ta>
            <ta e="T378" id="Seg_5451" s="T377">ɨj-ga</ta>
            <ta e="T379" id="Seg_5452" s="T378">bɨrtag-ɨ-ŋ</ta>
            <ta e="T380" id="Seg_5453" s="T379">bɨlɨk-ka</ta>
            <ta e="T381" id="Seg_5454" s="T380">ɨraːh-ɨ-ŋ</ta>
            <ta e="T382" id="Seg_5455" s="T381">ɨj-ga</ta>
            <ta e="T383" id="Seg_5456" s="T382">bɨrtag-ɨ-ŋ</ta>
            <ta e="T384" id="Seg_5457" s="T383">bɨlɨk-ka</ta>
            <ta e="T385" id="Seg_5458" s="T384">tu͡om-u-ŋ-sɨram-uː-ŋ</ta>
            <ta e="T386" id="Seg_5459" s="T385">tu͡om-u-ŋ-sɨram-uː-ŋ</ta>
            <ta e="T387" id="Seg_5460" s="T386">di͡e-čči-ler</ta>
            <ta e="T388" id="Seg_5461" s="T387">üs-te</ta>
            <ta e="T389" id="Seg_5462" s="T388">kat</ta>
            <ta e="T390" id="Seg_5463" s="T389">ergi-t-eːčči-ler</ta>
            <ta e="T391" id="Seg_5464" s="T390">ol</ta>
            <ta e="T392" id="Seg_5465" s="T391">gɨn-an</ta>
            <ta e="T393" id="Seg_5466" s="T392">togoho-tu-ttan</ta>
            <ta e="T394" id="Seg_5467" s="T393">tut-t-ar</ta>
            <ta e="T395" id="Seg_5468" s="T394">ol</ta>
            <ta e="T396" id="Seg_5469" s="T395">olor-or</ta>
            <ta e="T397" id="Seg_5470" s="T396">dʼaktar-ɨ-ŋ</ta>
            <ta e="T398" id="Seg_5471" s="T397">olor-or</ta>
            <ta e="T399" id="Seg_5472" s="T398">togoho-tu-ttan</ta>
            <ta e="T400" id="Seg_5473" s="T399">oččogo</ta>
            <ta e="T401" id="Seg_5474" s="T400">bu͡ollagɨna</ta>
            <ta e="T402" id="Seg_5475" s="T401">ajɨːhɨt</ta>
            <ta e="T403" id="Seg_5476" s="T402">ɨl-ar</ta>
            <ta e="T404" id="Seg_5477" s="T403">baːbɨska</ta>
            <ta e="T405" id="Seg_5478" s="T404">emeːksin</ta>
            <ta e="T406" id="Seg_5479" s="T405">ol</ta>
            <ta e="T407" id="Seg_5480" s="T406">ajɨːhɨt-ɨ-n</ta>
            <ta e="T408" id="Seg_5481" s="T407">ɨl-ar</ta>
            <ta e="T409" id="Seg_5482" s="T408">uskaːn</ta>
            <ta e="T410" id="Seg_5483" s="T409">tiriː-te</ta>
            <ta e="T411" id="Seg_5484" s="T410">ol</ta>
            <ta e="T412" id="Seg_5485" s="T411">ajɨːhɨt-kaːn-a</ta>
            <ta e="T413" id="Seg_5486" s="T412">uskaːn-ɨ-ŋ</ta>
            <ta e="T414" id="Seg_5487" s="T413">ajɨːhɨt</ta>
            <ta e="T415" id="Seg_5488" s="T414">aːt-taːk</ta>
            <ta e="T416" id="Seg_5489" s="T415">bu͡ol-aːččɨ</ta>
            <ta e="T417" id="Seg_5490" s="T416">oččogo</ta>
            <ta e="T418" id="Seg_5491" s="T417">uskaːn-ɨ</ta>
            <ta e="T419" id="Seg_5492" s="T418">tut-an</ta>
            <ta e="T420" id="Seg_5493" s="T419">baran</ta>
            <ta e="T421" id="Seg_5494" s="T420">uskaːn-ɨ</ta>
            <ta e="T422" id="Seg_5495" s="T421">u͡ok-ka</ta>
            <ta e="T423" id="Seg_5496" s="T422">ah-a-t-ar</ta>
            <ta e="T424" id="Seg_5497" s="T423">u͡ot-u</ta>
            <ta e="T425" id="Seg_5498" s="T424">emi͡e</ta>
            <ta e="T426" id="Seg_5499" s="T425">ah-a-t-ar</ta>
            <ta e="T427" id="Seg_5500" s="T426">u͡ot-ehe-keːn-iː-m</ta>
            <ta e="T428" id="Seg_5501" s="T427">ajɨːhɨp-pɨ-n</ta>
            <ta e="T429" id="Seg_5502" s="T428">ah-a-t-a-bɨn</ta>
            <ta e="T430" id="Seg_5503" s="T429">ogo-bu-n</ta>
            <ta e="T431" id="Seg_5504" s="T430">eteŋŋe</ta>
            <ta e="T432" id="Seg_5505" s="T431">olor-d-u͡og-u-n</ta>
            <ta e="T433" id="Seg_5506" s="T432">eteŋŋe</ta>
            <ta e="T434" id="Seg_5507" s="T433">tabɨsk-ɨ͡ag-ɨ-n</ta>
            <ta e="T435" id="Seg_5508" s="T434">di͡e-n</ta>
            <ta e="T436" id="Seg_5509" s="T435">oččogo</ta>
            <ta e="T437" id="Seg_5510" s="T436">bu͡ollagɨna</ta>
            <ta e="T438" id="Seg_5511" s="T437">u͡ot</ta>
            <ta e="T439" id="Seg_5512" s="T438">ehe-keːm-mi-n</ta>
            <ta e="T440" id="Seg_5513" s="T439">ah-a-t-a-bɨn</ta>
            <ta e="T441" id="Seg_5514" s="T440">u͡ot</ta>
            <ta e="T442" id="Seg_5515" s="T441">ehe-keːn-iː-m</ta>
            <ta e="T443" id="Seg_5516" s="T442">ahaː</ta>
            <ta e="T444" id="Seg_5517" s="T443">eteŋŋe</ta>
            <ta e="T445" id="Seg_5518" s="T444">dʼol-lo</ta>
            <ta e="T446" id="Seg_5519" s="T445">keteː</ta>
            <ta e="T447" id="Seg_5520" s="T446">tu͡ok</ta>
            <ta e="T448" id="Seg_5521" s="T447">da</ta>
            <ta e="T449" id="Seg_5522" s="T448">kuhagan-ɨ</ta>
            <ta e="T450" id="Seg_5523" s="T449">hugahaː-t-ɨ-ma</ta>
            <ta e="T451" id="Seg_5524" s="T450">eteŋŋe</ta>
            <ta e="T452" id="Seg_5525" s="T451">olor-t</ta>
            <ta e="T453" id="Seg_5526" s="T452">ogo-bu-n</ta>
            <ta e="T454" id="Seg_5527" s="T453">eteŋŋe</ta>
            <ta e="T455" id="Seg_5528" s="T454">boskoloː</ta>
            <ta e="T456" id="Seg_5529" s="T455">di͡e-n</ta>
            <ta e="T457" id="Seg_5530" s="T456">onton</ta>
            <ta e="T458" id="Seg_5531" s="T457">ol</ta>
            <ta e="T459" id="Seg_5532" s="T458">ajɨːhɨk-kaːn-ɨ-n</ta>
            <ta e="T460" id="Seg_5533" s="T459">ah-a-t-an</ta>
            <ta e="T461" id="Seg_5534" s="T460">baraːn</ta>
            <ta e="T462" id="Seg_5535" s="T461">togoho-go</ta>
            <ta e="T463" id="Seg_5536" s="T462">ɨjɨː-r</ta>
            <ta e="T464" id="Seg_5537" s="T463">ol</ta>
            <ta e="T465" id="Seg_5538" s="T464">togoho-go</ta>
            <ta e="T466" id="Seg_5539" s="T465">baːj-an</ta>
            <ta e="T467" id="Seg_5540" s="T466">keːh-er</ta>
            <ta e="T468" id="Seg_5541" s="T467">ol</ta>
            <ta e="T469" id="Seg_5542" s="T468">baːj-an</ta>
            <ta e="T470" id="Seg_5543" s="T469">keːh-en</ta>
            <ta e="T471" id="Seg_5544" s="T470">baraːn</ta>
            <ta e="T472" id="Seg_5545" s="T471">ol</ta>
            <ta e="T473" id="Seg_5546" s="T472">ogolon-or</ta>
            <ta e="T474" id="Seg_5547" s="T473">dʼaktar</ta>
            <ta e="T475" id="Seg_5548" s="T474">kɨ͡aj-an</ta>
            <ta e="T476" id="Seg_5549" s="T475">ol</ta>
            <ta e="T477" id="Seg_5550" s="T476">kuhagan-nɨk</ta>
            <ta e="T478" id="Seg_5551" s="T477">üːn-n-er</ta>
            <ta e="T479" id="Seg_5552" s="T478">bu͡ol-lag-ɨna</ta>
            <ta e="T480" id="Seg_5553" s="T479">kömölöh-ör</ta>
            <ta e="T481" id="Seg_5554" s="T480">baːbɨska</ta>
            <ta e="T482" id="Seg_5555" s="T481">tobug-u-gar</ta>
            <ta e="T483" id="Seg_5556" s="T482">olord-or</ta>
            <ta e="T484" id="Seg_5557" s="T483">kuturug-u-n</ta>
            <ta e="T485" id="Seg_5558" s="T484">tördü-tü-nen</ta>
            <ta e="T486" id="Seg_5559" s="T485">ogo-to</ta>
            <ta e="T487" id="Seg_5560" s="T486">kenni-leː-m-i͡eg-i-n</ta>
            <ta e="T488" id="Seg_5561" s="T487">di͡e-n</ta>
            <ta e="T489" id="Seg_5562" s="T488">onton</ta>
            <ta e="T490" id="Seg_5563" s="T489">bɨ͡ar-ɨ-n</ta>
            <ta e="T491" id="Seg_5564" s="T490">ilbij-er</ta>
            <ta e="T492" id="Seg_5565" s="T491">ol</ta>
            <ta e="T493" id="Seg_5566" s="T492">bɨ͡ar-ɨ-n</ta>
            <ta e="T494" id="Seg_5567" s="T493">ilbij-en</ta>
            <ta e="T495" id="Seg_5568" s="T494">kömölöh-ön</ta>
            <ta e="T496" id="Seg_5569" s="T495">ol</ta>
            <ta e="T497" id="Seg_5570" s="T496">ogo-tu-n</ta>
            <ta e="T498" id="Seg_5571" s="T497">kömölöh-ön</ta>
            <ta e="T499" id="Seg_5572" s="T498">tabɨskɨː-r</ta>
            <ta e="T500" id="Seg_5573" s="T499">ol</ta>
            <ta e="T501" id="Seg_5574" s="T500">dʼaktar</ta>
            <ta e="T502" id="Seg_5575" s="T501">ol</ta>
            <ta e="T503" id="Seg_5576" s="T502">tabɨskɨː-r</ta>
            <ta e="T504" id="Seg_5577" s="T503">aːt-a</ta>
            <ta e="T505" id="Seg_5578" s="T504">ogo</ta>
            <ta e="T506" id="Seg_5579" s="T505">törüː-r-e</ta>
            <ta e="T507" id="Seg_5580" s="T506">ol</ta>
            <ta e="T508" id="Seg_5581" s="T507">tabɨskaː-n</ta>
            <ta e="T509" id="Seg_5582" s="T508">ogo</ta>
            <ta e="T510" id="Seg_5583" s="T509">tüs-püt-ü-ger</ta>
            <ta e="T511" id="Seg_5584" s="T510">ol</ta>
            <ta e="T512" id="Seg_5585" s="T511">ogo-nu</ta>
            <ta e="T513" id="Seg_5586" s="T512">kiːn-i-n</ta>
            <ta e="T514" id="Seg_5587" s="T513">bɨh-al-lar</ta>
            <ta e="T515" id="Seg_5588" s="T514">bɨh-a-bɨt</ta>
            <ta e="T516" id="Seg_5589" s="T515">ol</ta>
            <ta e="T517" id="Seg_5590" s="T516">baːbɨska</ta>
            <ta e="T518" id="Seg_5591" s="T517">emeːksin</ta>
            <ta e="T519" id="Seg_5592" s="T518">bɨh-ar</ta>
            <ta e="T520" id="Seg_5593" s="T519">ol</ta>
            <ta e="T521" id="Seg_5594" s="T520">kiːn-i-n</ta>
            <ta e="T522" id="Seg_5595" s="T521">bɨh-an</ta>
            <ta e="T523" id="Seg_5596" s="T522">baraːn</ta>
            <ta e="T524" id="Seg_5597" s="T523">du͡o</ta>
            <ta e="T525" id="Seg_5598" s="T524">baːj-an</ta>
            <ta e="T526" id="Seg_5599" s="T525">keːh-er</ta>
            <ta e="T527" id="Seg_5600" s="T526">iŋiːr</ta>
            <ta e="T528" id="Seg_5601" s="T527">hab-ɨ-nan</ta>
            <ta e="T529" id="Seg_5602" s="T528">kat-ɨ-ll-ɨ-bɨt</ta>
            <ta e="T530" id="Seg_5603" s="T529">iŋiːr</ta>
            <ta e="T531" id="Seg_5604" s="T530">hab-ɨ-nan</ta>
            <ta e="T532" id="Seg_5605" s="T531">baːj-ar</ta>
            <ta e="T533" id="Seg_5606" s="T532">tull-an</ta>
            <ta e="T534" id="Seg_5607" s="T533">tüh-ü͡ög-ü-n</ta>
            <ta e="T535" id="Seg_5608" s="T534">köjüt</ta>
            <ta e="T536" id="Seg_5609" s="T535">bɨha</ta>
            <ta e="T537" id="Seg_5610" s="T536">hɨtɨj-an</ta>
            <ta e="T538" id="Seg_5611" s="T537">bi͡es</ta>
            <ta e="T539" id="Seg_5612" s="T538">kün</ta>
            <ta e="T540" id="Seg_5613" s="T539">duː</ta>
            <ta e="T541" id="Seg_5614" s="T540">kas</ta>
            <ta e="T542" id="Seg_5615" s="T541">duː</ta>
            <ta e="T543" id="Seg_5616" s="T542">kün</ta>
            <ta e="T544" id="Seg_5617" s="T543">bu͡ol-an</ta>
            <ta e="T545" id="Seg_5618" s="T544">baraːn</ta>
            <ta e="T546" id="Seg_5619" s="T545">tüh-eːčči</ta>
            <ta e="T547" id="Seg_5620" s="T546">ogo</ta>
            <ta e="T548" id="Seg_5621" s="T547">kiːn-e</ta>
            <ta e="T549" id="Seg_5622" s="T548">onton</ta>
            <ta e="T550" id="Seg_5623" s="T549">bu͡ollagɨna</ta>
            <ta e="T551" id="Seg_5624" s="T550">du͡o</ta>
            <ta e="T552" id="Seg_5625" s="T551">ogo-tu-n</ta>
            <ta e="T553" id="Seg_5626" s="T552">inʼe-te</ta>
            <ta e="T554" id="Seg_5627" s="T553">tüs-püt-ü-n</ta>
            <ta e="T555" id="Seg_5628" s="T554">ol</ta>
            <ta e="T556" id="Seg_5629" s="T555">ogo-tu-n</ta>
            <ta e="T557" id="Seg_5630" s="T556">inʼe-ti-n</ta>
            <ta e="T558" id="Seg_5631" s="T557">huːj-ar</ta>
            <ta e="T559" id="Seg_5632" s="T558">ogo-tu-n</ta>
            <ta e="T560" id="Seg_5633" s="T559">emi͡e</ta>
            <ta e="T561" id="Seg_5634" s="T560">huːj-ar</ta>
            <ta e="T562" id="Seg_5635" s="T561">kahɨn</ta>
            <ta e="T563" id="Seg_5636" s="T562">daːganɨ</ta>
            <ta e="T564" id="Seg_5637" s="T563">huj-an</ta>
            <ta e="T565" id="Seg_5638" s="T564">baraːn</ta>
            <ta e="T566" id="Seg_5639" s="T565">huːlaː-n</ta>
            <ta e="T567" id="Seg_5640" s="T566">keːh-er</ta>
            <ta e="T568" id="Seg_5641" s="T567">bu͡olla</ta>
            <ta e="T569" id="Seg_5642" s="T568">onton</ta>
            <ta e="T570" id="Seg_5643" s="T569">bu͡ollagɨna</ta>
            <ta e="T571" id="Seg_5644" s="T570">du͡o</ta>
            <ta e="T573" id="Seg_5645" s="T572">bihik-ke</ta>
            <ta e="T574" id="Seg_5646" s="T573">össü͡ö</ta>
            <ta e="T575" id="Seg_5647" s="T574">bileː-bet-ter</ta>
            <ta e="T576" id="Seg_5648" s="T575">bu͡ollaga</ta>
            <ta e="T577" id="Seg_5649" s="T576">huːlaː-n</ta>
            <ta e="T578" id="Seg_5650" s="T577">keːh-er</ta>
            <ta e="T579" id="Seg_5651" s="T578">maːma-tɨ-n</ta>
            <ta e="T580" id="Seg_5652" s="T579">attɨ-gar</ta>
            <ta e="T581" id="Seg_5653" s="T580">uːr-ar</ta>
            <ta e="T582" id="Seg_5654" s="T581">onton</ta>
            <ta e="T583" id="Seg_5655" s="T582">du͡o</ta>
            <ta e="T584" id="Seg_5656" s="T583">ajɨːhɨt-ɨ-n</ta>
            <ta e="T586" id="Seg_5657" s="T585">ajɨːhɨt-ɨ-n</ta>
            <ta e="T587" id="Seg_5658" s="T586">huːj-an</ta>
            <ta e="T588" id="Seg_5659" s="T587">baraːn</ta>
            <ta e="T589" id="Seg_5660" s="T588">ol</ta>
            <ta e="T590" id="Seg_5661" s="T589">ajɨːhɨt-ɨ-n</ta>
            <ta e="T591" id="Seg_5662" s="T590">möhöːg-ü-ger</ta>
            <ta e="T592" id="Seg_5663" s="T591">ug-an</ta>
            <ta e="T593" id="Seg_5664" s="T592">keːh-en</ta>
            <ta e="T594" id="Seg_5665" s="T593">baraːn</ta>
            <ta e="T595" id="Seg_5666" s="T594">ol</ta>
            <ta e="T596" id="Seg_5667" s="T595">mataka-tɨ-gar</ta>
            <ta e="T597" id="Seg_5668" s="T596">ol</ta>
            <ta e="T598" id="Seg_5669" s="T597">mataka-lɨːn</ta>
            <ta e="T599" id="Seg_5670" s="T598">ol</ta>
            <ta e="T601" id="Seg_5671" s="T600">togoho-go</ta>
            <ta e="T602" id="Seg_5672" s="T601">baːj-an</ta>
            <ta e="T603" id="Seg_5673" s="T602">keːh-er</ta>
            <ta e="T604" id="Seg_5674" s="T603">ol</ta>
            <ta e="T605" id="Seg_5675" s="T604">togoho-go</ta>
            <ta e="T606" id="Seg_5676" s="T605">tur-ar</ta>
            <ta e="T607" id="Seg_5677" s="T606">bu</ta>
            <ta e="T608" id="Seg_5678" s="T607">ogo</ta>
            <ta e="T609" id="Seg_5679" s="T608">inʼe-te</ta>
            <ta e="T610" id="Seg_5680" s="T609">üje-ti-n</ta>
            <ta e="T611" id="Seg_5681" s="T610">turkarɨ</ta>
            <ta e="T612" id="Seg_5682" s="T611">bu</ta>
            <ta e="T613" id="Seg_5683" s="T612">ogo</ta>
            <ta e="T614" id="Seg_5684" s="T613">töröː-büt</ta>
            <ta e="T615" id="Seg_5685" s="T614">dʼukaː-tɨ-n</ta>
            <ta e="T616" id="Seg_5686" s="T615">ih-i-ger</ta>
            <ta e="T617" id="Seg_5687" s="T616">ogo</ta>
            <ta e="T618" id="Seg_5688" s="T617">töröː-büt</ta>
            <ta e="T619" id="Seg_5689" s="T618">togoho-tu-gar</ta>
            <ta e="T620" id="Seg_5690" s="T619">dʼaktar</ta>
            <ta e="T621" id="Seg_5691" s="T620">töröː-büt</ta>
            <ta e="T622" id="Seg_5692" s="T621">togoho-tu-gar</ta>
            <ta e="T623" id="Seg_5693" s="T622">oččogo</ta>
            <ta e="T624" id="Seg_5694" s="T623">di͡e-čči-ler</ta>
            <ta e="T625" id="Seg_5695" s="T624">bɨlɨr-gɨ</ta>
            <ta e="T626" id="Seg_5696" s="T625">üje-tten</ta>
            <ta e="T627" id="Seg_5697" s="T626">togoho-loːk</ta>
            <ta e="T628" id="Seg_5698" s="T627">huːrd-u</ta>
            <ta e="T629" id="Seg_5699" s="T628">aːs-tak-tarɨnan</ta>
            <ta e="T630" id="Seg_5700" s="T629">dʼukaː-ta</ta>
            <ta e="T631" id="Seg_5701" s="T630">da</ta>
            <ta e="T632" id="Seg_5702" s="T631">hu͡ok</ta>
            <ta e="T633" id="Seg_5703" s="T632">bu͡ol-lag-ɨna</ta>
            <ta e="T634" id="Seg_5704" s="T633">togoho-nu</ta>
            <ta e="T635" id="Seg_5705" s="T634">tut-a</ta>
            <ta e="T636" id="Seg_5706" s="T635">tur-ar-ɨ-n</ta>
            <ta e="T637" id="Seg_5707" s="T636">kör-dök-törüne</ta>
            <ta e="T638" id="Seg_5708" s="T637">kaː</ta>
            <ta e="T639" id="Seg_5709" s="T638">bu</ta>
            <ta e="T640" id="Seg_5710" s="T639">huːr-ka</ta>
            <ta e="T641" id="Seg_5711" s="T640">dʼaktar</ta>
            <ta e="T642" id="Seg_5712" s="T641">töröː-büt</ta>
            <ta e="T643" id="Seg_5713" s="T642">e-bit</ta>
            <ta e="T644" id="Seg_5714" s="T643">togoho-to</ta>
            <ta e="T645" id="Seg_5715" s="T644">tur-ar</ta>
            <ta e="T646" id="Seg_5716" s="T645">töröː-büt</ta>
            <ta e="T647" id="Seg_5717" s="T646">togoho-to</ta>
            <ta e="T648" id="Seg_5718" s="T647">dʼukaː-laːk</ta>
            <ta e="T649" id="Seg_5719" s="T648">bu͡ol-lag-ɨna</ta>
            <ta e="T650" id="Seg_5720" s="T649">onnuk</ta>
            <ta e="T651" id="Seg_5721" s="T650">ogo</ta>
            <ta e="T652" id="Seg_5722" s="T651">töröː-büt</ta>
            <ta e="T653" id="Seg_5723" s="T652">e-bit</ta>
            <ta e="T654" id="Seg_5724" s="T653">ol</ta>
            <ta e="T655" id="Seg_5725" s="T654">ogo</ta>
            <ta e="T656" id="Seg_5726" s="T655">töröː-büt</ta>
            <ta e="T657" id="Seg_5727" s="T656">dʼukaː-ta</ta>
            <ta e="T658" id="Seg_5728" s="T657">di͡e-čči-ler</ta>
            <ta e="T659" id="Seg_5729" s="T658">oččogo</ta>
            <ta e="T660" id="Seg_5730" s="T659">bu͡ollagɨna</ta>
            <ta e="T661" id="Seg_5731" s="T660">du͡o</ta>
            <ta e="T662" id="Seg_5732" s="T661">onton</ta>
            <ta e="T663" id="Seg_5733" s="T662">bu</ta>
            <ta e="T664" id="Seg_5734" s="T663">dʼaktar-ɨ</ta>
            <ta e="T665" id="Seg_5735" s="T664">ogo-lom-mut</ta>
            <ta e="T666" id="Seg_5736" s="T665">dʼaktar-ɨ-ŋ</ta>
            <ta e="T667" id="Seg_5737" s="T666">bɨrtak</ta>
            <ta e="T668" id="Seg_5738" s="T667">bu͡o</ta>
            <ta e="T669" id="Seg_5739" s="T668">töröː-büt</ta>
            <ta e="T670" id="Seg_5740" s="T669">bɨrtag-ɨ-n</ta>
            <ta e="T671" id="Seg_5741" s="T670">ɨraːstaː-n</ta>
            <ta e="T672" id="Seg_5742" s="T671">emi͡e</ta>
            <ta e="T673" id="Seg_5743" s="T672">alast-ɨː-gɨn</ta>
            <ta e="T674" id="Seg_5744" s="T673">hɨraj-ɨ-n</ta>
            <ta e="T675" id="Seg_5745" s="T674">ann-ɨ-nan</ta>
            <ta e="T676" id="Seg_5746" s="T675">mu͡oj-u-n</ta>
            <ta e="T677" id="Seg_5747" s="T676">tögürüččü</ta>
            <ta e="T678" id="Seg_5748" s="T677">üs-te</ta>
            <ta e="T679" id="Seg_5749" s="T678">kat</ta>
            <ta e="T680" id="Seg_5750" s="T679">alast-ɨː-gɨn</ta>
            <ta e="T681" id="Seg_5751" s="T680">türkeːt</ta>
            <ta e="T682" id="Seg_5752" s="T681">türkeːt</ta>
            <ta e="T683" id="Seg_5753" s="T682">bɨrtag-ɨ-ŋ</ta>
            <ta e="T684" id="Seg_5754" s="T683">bɨlɨk-ka</ta>
            <ta e="T685" id="Seg_5755" s="T684">ɨraːh-ɨ-ŋ</ta>
            <ta e="T686" id="Seg_5756" s="T685">ɨj-ga</ta>
            <ta e="T687" id="Seg_5757" s="T686">türkeːt</ta>
            <ta e="T688" id="Seg_5758" s="T687">türkeːt</ta>
            <ta e="T689" id="Seg_5759" s="T688">bɨrtag-ɨ-ŋ</ta>
            <ta e="T690" id="Seg_5760" s="T689">bɨlɨk-ka</ta>
            <ta e="T691" id="Seg_5761" s="T690">ɨraːh-ɨ-ŋ</ta>
            <ta e="T692" id="Seg_5762" s="T691">ɨj-ga</ta>
            <ta e="T693" id="Seg_5763" s="T692">türkeːt-türkeːt</ta>
            <ta e="T694" id="Seg_5764" s="T693">bɨrtag-ɨ-n</ta>
            <ta e="T695" id="Seg_5765" s="T694">bɨlɨk-ka</ta>
            <ta e="T696" id="Seg_5766" s="T695">ɨraːh-ɨ-ŋ</ta>
            <ta e="T697" id="Seg_5767" s="T696">ɨj-ga</ta>
            <ta e="T698" id="Seg_5768" s="T697">tu͡om-u-ŋ-sɨram-uː-ŋ</ta>
            <ta e="T699" id="Seg_5769" s="T698">tu͡om-u-ŋ-sɨram-uː-ŋ</ta>
            <ta e="T700" id="Seg_5770" s="T699">tu͡om-u-ŋ-sɨram-uː-ŋ</ta>
            <ta e="T701" id="Seg_5771" s="T700">di͡e-ŋ-ŋin</ta>
            <ta e="T702" id="Seg_5772" s="T701">di͡e-ŋ-ŋin</ta>
            <ta e="T703" id="Seg_5773" s="T702">alast-ɨː-gɨn</ta>
            <ta e="T704" id="Seg_5774" s="T703">ol</ta>
            <ta e="T705" id="Seg_5775" s="T704">ol</ta>
            <ta e="T706" id="Seg_5776" s="T705">gɨn-an</ta>
            <ta e="T707" id="Seg_5777" s="T706">baraːn</ta>
            <ta e="T708" id="Seg_5778" s="T707">ogo-tu-n</ta>
            <ta e="T709" id="Seg_5779" s="T708">ɨl-ar</ta>
            <ta e="T710" id="Seg_5780" s="T709">baːbɨska</ta>
            <ta e="T711" id="Seg_5781" s="T710">onton</ta>
            <ta e="T712" id="Seg_5782" s="T711">du͡o</ta>
            <ta e="T713" id="Seg_5783" s="T712">dʼaktar-ɨ</ta>
            <ta e="T714" id="Seg_5784" s="T713">tur-u͡or-ar</ta>
            <ta e="T715" id="Seg_5785" s="T714">hɨt-ar</ta>
            <ta e="T716" id="Seg_5786" s="T715">telleg-i-tten</ta>
            <ta e="T717" id="Seg_5787" s="T716">ol</ta>
            <ta e="T718" id="Seg_5788" s="T717">töröː-büt</ta>
            <ta e="T719" id="Seg_5789" s="T718">dʼukaː-tɨ-gar</ta>
            <ta e="T720" id="Seg_5790" s="T719">onton</ta>
            <ta e="T721" id="Seg_5791" s="T720">tur-an</ta>
            <ta e="T722" id="Seg_5792" s="T721">tahaːr-a-bɨt</ta>
            <ta e="T723" id="Seg_5793" s="T722">dʼi͡e-biti-ger</ta>
            <ta e="T724" id="Seg_5794" s="T723">bar-a-bɨt</ta>
            <ta e="T725" id="Seg_5795" s="T724">hi͡et-en</ta>
            <ta e="T726" id="Seg_5796" s="T725">ill-e-bit</ta>
            <ta e="T727" id="Seg_5797" s="T726">töröː-büt</ta>
            <ta e="T728" id="Seg_5798" s="T727">dʼaktar-ɨ</ta>
            <ta e="T729" id="Seg_5799" s="T728">ogo-lom-mut</ta>
            <ta e="T730" id="Seg_5800" s="T729">dʼaktar-ɨ</ta>
            <ta e="T731" id="Seg_5801" s="T730">ol</ta>
            <ta e="T732" id="Seg_5802" s="T731">gɨn-an</ta>
            <ta e="T733" id="Seg_5803" s="T732">baraːn</ta>
            <ta e="T734" id="Seg_5804" s="T733">ol</ta>
            <ta e="T735" id="Seg_5805" s="T734">hi͡et-en</ta>
            <ta e="T736" id="Seg_5806" s="T735">er-en</ta>
            <ta e="T737" id="Seg_5807" s="T736">ol</ta>
            <ta e="T738" id="Seg_5808" s="T737">kepsiː-r</ta>
            <ta e="T739" id="Seg_5809" s="T738">ol</ta>
            <ta e="T740" id="Seg_5810" s="T739">kaja</ta>
            <ta e="T741" id="Seg_5811" s="T740">ogo</ta>
            <ta e="T742" id="Seg_5812" s="T741">töröː-t-ö</ta>
            <ta e="T743" id="Seg_5813" s="T742">di͡e-n</ta>
            <ta e="T744" id="Seg_5814" s="T743">kerget-teri-ger</ta>
            <ta e="T745" id="Seg_5815" s="T744">dʼi͡e-leːk</ta>
            <ta e="T746" id="Seg_5816" s="T745">dʼi͡e-leri-ger</ta>
            <ta e="T747" id="Seg_5817" s="T746">dʼi͡e-leːk</ta>
            <ta e="T748" id="Seg_5818" s="T747">uraha</ta>
            <ta e="T749" id="Seg_5819" s="T748">dʼi͡e-ge</ta>
            <ta e="T750" id="Seg_5820" s="T749">ol</ta>
            <ta e="T751" id="Seg_5821" s="T750">kepseː-n</ta>
            <ta e="T752" id="Seg_5822" s="T751">kepsiː-r</ta>
            <ta e="T753" id="Seg_5823" s="T752">kajaː</ta>
            <ta e="T754" id="Seg_5824" s="T753">ol</ta>
            <ta e="T755" id="Seg_5825" s="T754">kɨːh</ta>
            <ta e="T756" id="Seg_5826" s="T755">ogo</ta>
            <ta e="T757" id="Seg_5827" s="T756">töröː-büt</ta>
            <ta e="T758" id="Seg_5828" s="T757">bu͡ol-lag-ɨna</ta>
            <ta e="T759" id="Seg_5829" s="T758">kɨːh</ta>
            <ta e="T760" id="Seg_5830" s="T759">ogo</ta>
            <ta e="T761" id="Seg_5831" s="T760">diː-l-ler</ta>
            <ta e="T762" id="Seg_5832" s="T761">u͡ol</ta>
            <ta e="T763" id="Seg_5833" s="T762">ogo</ta>
            <ta e="T764" id="Seg_5834" s="T763">töröː-büt</ta>
            <ta e="T765" id="Seg_5835" s="T764">bu͡ol-lag-ɨna</ta>
            <ta e="T766" id="Seg_5836" s="T765">u͡ol</ta>
            <ta e="T767" id="Seg_5837" s="T766">ogo</ta>
            <ta e="T768" id="Seg_5838" s="T767">diː-l-ler</ta>
            <ta e="T769" id="Seg_5839" s="T768">onton</ta>
            <ta e="T770" id="Seg_5840" s="T769">bu</ta>
            <ta e="T771" id="Seg_5841" s="T770">üs</ta>
            <ta e="T772" id="Seg_5842" s="T771">kün</ta>
            <ta e="T773" id="Seg_5843" s="T772">bu͡ol-an</ta>
            <ta e="T774" id="Seg_5844" s="T773">baraːn</ta>
            <ta e="T775" id="Seg_5845" s="T774">aga-ta</ta>
            <ta e="T776" id="Seg_5846" s="T775">bihig-i</ta>
            <ta e="T777" id="Seg_5847" s="T776">oŋor-or</ta>
            <ta e="T778" id="Seg_5848" s="T777">ogo-tu-gar</ta>
            <ta e="T779" id="Seg_5849" s="T778">ehe-te</ta>
            <ta e="T780" id="Seg_5850" s="T779">kömölöh-ör</ta>
            <ta e="T781" id="Seg_5851" s="T780">oččogo</ta>
            <ta e="T782" id="Seg_5852" s="T781">bu͡o</ta>
            <ta e="T783" id="Seg_5853" s="T782">bu</ta>
            <ta e="T784" id="Seg_5854" s="T783">bihig-i</ta>
            <ta e="T785" id="Seg_5855" s="T784">du͡o</ta>
            <ta e="T786" id="Seg_5856" s="T785">tab-ɨ-ll-ɨ-bɨt</ta>
            <ta e="T787" id="Seg_5857" s="T786">bige</ta>
            <ta e="T788" id="Seg_5858" s="T787">doskoː-lar-ɨ-nan</ta>
            <ta e="T789" id="Seg_5859" s="T788">bɨ͡a-nnan</ta>
            <ta e="T790" id="Seg_5860" s="T789">harɨː</ta>
            <ta e="T791" id="Seg_5861" s="T790">bɨ͡a-nnan</ta>
            <ta e="T792" id="Seg_5862" s="T791">üːj-en</ta>
            <ta e="T793" id="Seg_5863" s="T792">oŋor-oːčču-lar</ta>
            <ta e="T794" id="Seg_5864" s="T793">karɨja-nɨ</ta>
            <ta e="T795" id="Seg_5865" s="T794">bu</ta>
            <ta e="T796" id="Seg_5866" s="T795">kim</ta>
            <ta e="T797" id="Seg_5867" s="T796">eː</ta>
            <ta e="T798" id="Seg_5868" s="T797">fanʼera</ta>
            <ta e="T799" id="Seg_5869" s="T798">kördük</ta>
            <ta e="T800" id="Seg_5870" s="T799">di͡e-čči-ler</ta>
            <ta e="T801" id="Seg_5871" s="T800">iti</ta>
            <ta e="T802" id="Seg_5872" s="T801">itinnig-i</ta>
            <ta e="T803" id="Seg_5873" s="T802">du͡o</ta>
            <ta e="T804" id="Seg_5874" s="T803">oŋor-tor-oːčču-ta</ta>
            <ta e="T805" id="Seg_5875" s="T804">hu͡ok-tar</ta>
            <ta e="T806" id="Seg_5876" s="T805">kap-pɨt</ta>
            <ta e="T807" id="Seg_5877" s="T806">mas</ta>
            <ta e="T808" id="Seg_5878" s="T807">tiriː-te</ta>
            <ta e="T809" id="Seg_5879" s="T808">iti</ta>
            <ta e="T810" id="Seg_5880" s="T809">fanʼera</ta>
            <ta e="T811" id="Seg_5881" s="T810">ol</ta>
            <ta e="T812" id="Seg_5882" s="T811">ihin</ta>
            <ta e="T813" id="Seg_5883" s="T812">bu͡ollagɨna</ta>
            <ta e="T814" id="Seg_5884" s="T813">du͡o</ta>
            <ta e="T815" id="Seg_5885" s="T814">ogo</ta>
            <ta e="T816" id="Seg_5886" s="T815">kat-ɨ͡a</ta>
            <ta e="T817" id="Seg_5887" s="T816">di͡e-čči-ler</ta>
            <ta e="T818" id="Seg_5888" s="T817">ol</ta>
            <ta e="T819" id="Seg_5889" s="T818">ihin</ta>
            <ta e="T0" id="Seg_5890" s="T820">dosko-nnon</ta>
            <ta e="T822" id="Seg_5891" s="T821">a</ta>
            <ta e="T823" id="Seg_5892" s="T822">doskoː-ŋ</ta>
            <ta e="T824" id="Seg_5893" s="T823">üːj-ü-lün-neg-ine</ta>
            <ta e="T825" id="Seg_5894" s="T824">du͡o</ta>
            <ta e="T826" id="Seg_5895" s="T825">üːj-ü-lün-neg-inen</ta>
            <ta e="T827" id="Seg_5896" s="T826">üːj-ü-lün-neg-ine</ta>
            <ta e="T828" id="Seg_5897" s="T827">bɨ͡a-nnan</ta>
            <ta e="T829" id="Seg_5898" s="T828">üːj-en</ta>
            <ta e="T830" id="Seg_5899" s="T829">oŋor-dok-toruna</ta>
            <ta e="T831" id="Seg_5900" s="T830">on-tu-ŋ</ta>
            <ta e="T832" id="Seg_5901" s="T831">bɨːs-taːk</ta>
            <ta e="T833" id="Seg_5902" s="T832">bu͡ol-aːččɨ</ta>
            <ta e="T834" id="Seg_5903" s="T833">dosko</ta>
            <ta e="T835" id="Seg_5904" s="T834">dosko-tu-ttan</ta>
            <ta e="T836" id="Seg_5905" s="T835">emi͡e</ta>
            <ta e="T837" id="Seg_5906" s="T836">bɨːs-taːk</ta>
            <ta e="T838" id="Seg_5907" s="T837">bu͡ol-aːččɨ</ta>
            <ta e="T839" id="Seg_5908" s="T838">oččogo</ta>
            <ta e="T840" id="Seg_5909" s="T839">tɨːn</ta>
            <ta e="T841" id="Seg_5910" s="T840">kiːr-er</ta>
            <ta e="T842" id="Seg_5911" s="T841">bu͡ollaga</ta>
            <ta e="T843" id="Seg_5912" s="T842">ol</ta>
            <ta e="T844" id="Seg_5913" s="T843">ihin</ta>
            <ta e="T845" id="Seg_5914" s="T844">ogo-ŋ</ta>
            <ta e="T846" id="Seg_5915" s="T845">zdarovaj</ta>
            <ta e="T847" id="Seg_5916" s="T846">ü͡ösküː-r</ta>
            <ta e="T848" id="Seg_5917" s="T847">bu͡olla</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_5918" s="T1">bɨlɨr-GI</ta>
            <ta e="T3" id="Seg_5919" s="T2">ogo</ta>
            <ta e="T4" id="Seg_5920" s="T3">töröː-Ar-tA</ta>
            <ta e="T5" id="Seg_5921" s="T4">togoho-GA</ta>
            <ta e="T6" id="Seg_5922" s="T5">togoho-GA</ta>
            <ta e="T7" id="Seg_5923" s="T6">ogo-LArA</ta>
            <ta e="T8" id="Seg_5924" s="T7">ogo</ta>
            <ta e="T9" id="Seg_5925" s="T8">ɨ͡arakan</ta>
            <ta e="T10" id="Seg_5926" s="T9">dʼaktar</ta>
            <ta e="T11" id="Seg_5927" s="T10">töröː-Ar</ta>
            <ta e="T12" id="Seg_5928" s="T11">kem-tI-GAr</ta>
            <ta e="T13" id="Seg_5929" s="T12">bɨlɨr-GI</ta>
            <ta e="T14" id="Seg_5930" s="T13">üje-ttAn</ta>
            <ta e="T15" id="Seg_5931" s="T14">togoho-nI</ta>
            <ta e="T16" id="Seg_5932" s="T15">belemneː-AːččI-LAr</ta>
            <ta e="T17" id="Seg_5933" s="T16">tuspa</ta>
            <ta e="T18" id="Seg_5934" s="T17">uraha</ta>
            <ta e="T19" id="Seg_5935" s="T18">dʼi͡e-nI</ta>
            <ta e="T20" id="Seg_5936" s="T19">tut-An-LAr</ta>
            <ta e="T21" id="Seg_5937" s="T20">belemneː-AːččI-LAr</ta>
            <ta e="T22" id="Seg_5938" s="T21">ogo</ta>
            <ta e="T23" id="Seg_5939" s="T22">töröː-Ar</ta>
            <ta e="T24" id="Seg_5940" s="T23">dʼuka-tA</ta>
            <ta e="T25" id="Seg_5941" s="T24">töröː-BIT</ta>
            <ta e="T26" id="Seg_5942" s="T25">dʼuka-tA</ta>
            <ta e="T27" id="Seg_5943" s="T26">di͡e-AːččI-LAr</ta>
            <ta e="T28" id="Seg_5944" s="T27">onton</ta>
            <ta e="T29" id="Seg_5945" s="T28">onno</ta>
            <ta e="T30" id="Seg_5946" s="T29">bu͡ollagɨna</ta>
            <ta e="T31" id="Seg_5947" s="T30">ol</ta>
            <ta e="T32" id="Seg_5948" s="T31">togoho-nI</ta>
            <ta e="T33" id="Seg_5949" s="T32">oŋor-AːččI-LAr</ta>
            <ta e="T34" id="Seg_5950" s="T33">kergen-LArA</ta>
            <ta e="T35" id="Seg_5951" s="T34">tur-IAr-Ar-LAr</ta>
            <ta e="T36" id="Seg_5952" s="T35">ehe-tA</ta>
            <ta e="T37" id="Seg_5953" s="T36">du͡o</ta>
            <ta e="T38" id="Seg_5954" s="T37">aga-tA</ta>
            <ta e="T39" id="Seg_5955" s="T38">du͡o</ta>
            <ta e="T40" id="Seg_5956" s="T39">inʼe-tA</ta>
            <ta e="T41" id="Seg_5957" s="T40">du͡o</ta>
            <ta e="T42" id="Seg_5958" s="T41">kömölös-An-LAr</ta>
            <ta e="T43" id="Seg_5959" s="T42">baːj-Ar-LAr</ta>
            <ta e="T44" id="Seg_5960" s="T43">togoho-nI</ta>
            <ta e="T45" id="Seg_5961" s="T44">uraha-GA</ta>
            <ta e="T46" id="Seg_5962" s="T45">kɨhaj-A</ta>
            <ta e="T47" id="Seg_5963" s="T46">tu͡ora</ta>
            <ta e="T48" id="Seg_5964" s="T47">mas-nI</ta>
            <ta e="T49" id="Seg_5965" s="T48">onton</ta>
            <ta e="T50" id="Seg_5966" s="T49">e</ta>
            <ta e="T51" id="Seg_5967" s="T50">togoho-tI-n</ta>
            <ta e="T52" id="Seg_5968" s="T51">uhuk-tI-GAr</ta>
            <ta e="T53" id="Seg_5969" s="T52">du͡o</ta>
            <ta e="T54" id="Seg_5970" s="T53">končoj-ččI</ta>
            <ta e="T55" id="Seg_5971" s="T54">as-I-LIN-An</ta>
            <ta e="T56" id="Seg_5972" s="T55">tur-Ar</ta>
            <ta e="T57" id="Seg_5973" s="T56">bas-tA</ta>
            <ta e="T58" id="Seg_5974" s="T57">eː</ta>
            <ta e="T59" id="Seg_5975" s="T58">mas</ta>
            <ta e="T60" id="Seg_5976" s="T59">bu͡ol-AːččI</ta>
            <ta e="T61" id="Seg_5977" s="T60">uraha-kAːN-tA</ta>
            <ta e="T62" id="Seg_5978" s="T61">oččogo</ta>
            <ta e="T63" id="Seg_5979" s="T62">bu͡ollagɨna</ta>
            <ta e="T64" id="Seg_5980" s="T63">ol</ta>
            <ta e="T65" id="Seg_5981" s="T64">togoho-ŋ</ta>
            <ta e="T66" id="Seg_5982" s="T65">bu͡o</ta>
            <ta e="T67" id="Seg_5983" s="T66">tü͡ös-tI-n</ta>
            <ta e="T68" id="Seg_5984" s="T67">kuraːk-tI-nAn</ta>
            <ta e="T69" id="Seg_5985" s="T68">bu͡ol-AːččI</ta>
            <ta e="T70" id="Seg_5986" s="T69">dʼaktar</ta>
            <ta e="T71" id="Seg_5987" s="T70">onton</ta>
            <ta e="T72" id="Seg_5988" s="T71">ɨjaː-LIN-An</ta>
            <ta e="T73" id="Seg_5989" s="T72">olor-An</ta>
            <ta e="T74" id="Seg_5990" s="T73">ogolon-AːččI</ta>
            <ta e="T75" id="Seg_5991" s="T74">ol</ta>
            <ta e="T76" id="Seg_5992" s="T75">ogolon-Ar-tI-n</ta>
            <ta e="T77" id="Seg_5993" s="T76">alɨn-tI-GAr</ta>
            <ta e="T78" id="Seg_5994" s="T77">du͡o</ta>
            <ta e="T79" id="Seg_5995" s="T78">eː</ta>
            <ta e="T80" id="Seg_5996" s="T79">olor-Ar</ta>
            <ta e="T81" id="Seg_5997" s="T80">hir-tI-GAr</ta>
            <ta e="T82" id="Seg_5998" s="T81">lekeː</ta>
            <ta e="T83" id="Seg_5999" s="T82">bu͡ol-AːččI</ta>
            <ta e="T84" id="Seg_6000" s="T83">dʼe</ta>
            <ta e="T85" id="Seg_6001" s="T84">ol</ta>
            <ta e="T86" id="Seg_6002" s="T85">lekeː-GA</ta>
            <ta e="T87" id="Seg_6003" s="T86">du͡o</ta>
            <ta e="T88" id="Seg_6004" s="T87">ot</ta>
            <ta e="T89" id="Seg_6005" s="T88">bu͡ol-AːččI</ta>
            <ta e="T90" id="Seg_6006" s="T89">ot-nI</ta>
            <ta e="T91" id="Seg_6007" s="T90">uːr-AːččI-LAr</ta>
            <ta e="T92" id="Seg_6008" s="T91">tuːm-tI-GAr</ta>
            <ta e="T93" id="Seg_6009" s="T92">di͡e-An-LAr</ta>
            <ta e="T94" id="Seg_6010" s="T93">ol-GA</ta>
            <ta e="T95" id="Seg_6011" s="T94">du͡o</ta>
            <ta e="T96" id="Seg_6012" s="T95">ogo</ta>
            <ta e="T97" id="Seg_6013" s="T96">tüs-TAK-InA</ta>
            <ta e="T98" id="Seg_6014" s="T97">ot-tI-GAr</ta>
            <ta e="T99" id="Seg_6015" s="T98">tüs-Ar</ta>
            <ta e="T100" id="Seg_6016" s="T99">di͡e-AːččI-LAr</ta>
            <ta e="T101" id="Seg_6017" s="T100">ol</ta>
            <ta e="T102" id="Seg_6018" s="T101">ihin</ta>
            <ta e="T103" id="Seg_6019" s="T102">ot-LAːK</ta>
            <ta e="T104" id="Seg_6020" s="T103">ot-LAːK-tI-n</ta>
            <ta e="T105" id="Seg_6021" s="T104">ihin</ta>
            <ta e="T106" id="Seg_6022" s="T105">ot-GA</ta>
            <ta e="T107" id="Seg_6023" s="T106">tüs-BIT</ta>
            <ta e="T108" id="Seg_6024" s="T107">di͡e-AːččI-LAr</ta>
            <ta e="T109" id="Seg_6025" s="T108">eː</ta>
            <ta e="T110" id="Seg_6026" s="T109">dʼaktar</ta>
            <ta e="T111" id="Seg_6027" s="T110">ogolon-Ar-tI-GAr</ta>
            <ta e="T113" id="Seg_6028" s="T112">bɨlɨr-GI</ta>
            <ta e="T114" id="Seg_6029" s="T113">üje-ttAn</ta>
            <ta e="T115" id="Seg_6030" s="T114">ɨraːk-GA</ta>
            <ta e="T116" id="Seg_6031" s="T115">baːr</ta>
            <ta e="T117" id="Seg_6032" s="T116">bu͡ol-TAK-InA</ta>
            <ta e="T118" id="Seg_6033" s="T117">ol</ta>
            <ta e="T119" id="Seg_6034" s="T118">eː</ta>
            <ta e="T120" id="Seg_6035" s="T119">baːbɨska</ta>
            <ta e="T121" id="Seg_6036" s="T120">emeːksin-LArA</ta>
            <ta e="T122" id="Seg_6037" s="T121">ɨraːk</ta>
            <ta e="T123" id="Seg_6038" s="T122">baːr</ta>
            <ta e="T124" id="Seg_6039" s="T123">bu͡ol</ta>
            <ta e="T125" id="Seg_6040" s="T124">ogo</ta>
            <ta e="T126" id="Seg_6041" s="T125">töröː-Iː</ta>
            <ta e="T127" id="Seg_6042" s="T126">tüs-ČIt-LArA</ta>
            <ta e="T128" id="Seg_6043" s="T127">dʼaktar-LArA</ta>
            <ta e="T129" id="Seg_6044" s="T128">ɨraːk</ta>
            <ta e="T130" id="Seg_6045" s="T129">baːr</ta>
            <ta e="T131" id="Seg_6046" s="T130">bu͡ol-TAK-InA</ta>
            <ta e="T132" id="Seg_6047" s="T131">hɨrga-LAːK</ta>
            <ta e="T133" id="Seg_6048" s="T132">bar-An-LAr</ta>
            <ta e="T134" id="Seg_6049" s="T133">egel-AːččI-LAr</ta>
            <ta e="T135" id="Seg_6050" s="T134">kördös-An-LAr</ta>
            <ta e="T136" id="Seg_6051" s="T135">kaja</ta>
            <ta e="T137" id="Seg_6052" s="T136">ogo-BIt</ta>
            <ta e="T138" id="Seg_6053" s="T137">oččogo</ta>
            <ta e="T139" id="Seg_6054" s="T138">oččogo</ta>
            <ta e="T140" id="Seg_6055" s="T139">ol</ta>
            <ta e="T141" id="Seg_6056" s="T140">kɨːs-BIt</ta>
            <ta e="T143" id="Seg_6057" s="T142">töröː-IAK-tA</ta>
            <ta e="T144" id="Seg_6058" s="T143">di͡e-An-LAr</ta>
            <ta e="T145" id="Seg_6059" s="T144">dʼaktar-BIt</ta>
            <ta e="T146" id="Seg_6060" s="T145">töröː-IAK-tA</ta>
            <ta e="T147" id="Seg_6061" s="T146">di͡e-An-LAr</ta>
            <ta e="T148" id="Seg_6062" s="T147">egel-AːččI-LAr</ta>
            <ta e="T149" id="Seg_6063" s="T148">eː</ta>
            <ta e="T150" id="Seg_6064" s="T149">hugas</ta>
            <ta e="T151" id="Seg_6065" s="T150">baːr</ta>
            <ta e="T152" id="Seg_6066" s="T151">bu͡ol-TAK-InA</ta>
            <ta e="T153" id="Seg_6067" s="T152">du͡o</ta>
            <ta e="T154" id="Seg_6068" s="T153">ɨgɨr-AːččI-LAr</ta>
            <ta e="T155" id="Seg_6069" s="T154">ol</ta>
            <ta e="T156" id="Seg_6070" s="T155">baːbɨska</ta>
            <ta e="T157" id="Seg_6071" s="T156">emeːksin-nI</ta>
            <ta e="T158" id="Seg_6072" s="T157">innʼe</ta>
            <ta e="T159" id="Seg_6073" s="T158">gɨn-An</ta>
            <ta e="T160" id="Seg_6074" s="T159">ol</ta>
            <ta e="T161" id="Seg_6075" s="T160">baːbɨska</ta>
            <ta e="T162" id="Seg_6076" s="T161">iliː</ta>
            <ta e="T163" id="Seg_6077" s="T162">hottor-tA</ta>
            <ta e="T164" id="Seg_6078" s="T163">tu͡ok-tA</ta>
            <ta e="T165" id="Seg_6079" s="T164">barɨ-tA</ta>
            <ta e="T166" id="Seg_6080" s="T165">čeber</ta>
            <ta e="T167" id="Seg_6081" s="T166">bu͡ol-IAK-tI-n</ta>
            <ta e="T168" id="Seg_6082" s="T167">höp</ta>
            <ta e="T169" id="Seg_6083" s="T168">bu͡ol-AːččI</ta>
            <ta e="T170" id="Seg_6084" s="T169">barɨ-kAːN-tA</ta>
            <ta e="T171" id="Seg_6085" s="T170">haŋa</ta>
            <ta e="T172" id="Seg_6086" s="T171">tut-I-LIN-I-BAtAK</ta>
            <ta e="T173" id="Seg_6087" s="T172">ebi͡ennʼe</ta>
            <ta e="T174" id="Seg_6088" s="T173">bu͡ol-Ar-LAːK</ta>
            <ta e="T175" id="Seg_6089" s="T174">hottor-tA</ta>
            <ta e="T176" id="Seg_6090" s="T175">da</ta>
            <ta e="T177" id="Seg_6091" s="T176">iliː</ta>
            <ta e="T178" id="Seg_6092" s="T177">hottor-tA</ta>
            <ta e="T179" id="Seg_6093" s="T178">ogo-nI</ta>
            <ta e="T180" id="Seg_6094" s="T179">töröː-AttAː-Ar-tI-GAr</ta>
            <ta e="T181" id="Seg_6095" s="T180">tabɨskaː</ta>
            <ta e="T182" id="Seg_6096" s="T181">dʼaktar</ta>
            <ta e="T183" id="Seg_6097" s="T182">tabɨskaː-Ar-tI-GAr</ta>
            <ta e="T184" id="Seg_6098" s="T183">oččogo</ta>
            <ta e="T185" id="Seg_6099" s="T184">bu͡ollagɨna</ta>
            <ta e="T186" id="Seg_6100" s="T185">du͡o</ta>
            <ta e="T187" id="Seg_6101" s="T186">ajɨːhɨt-LAːK</ta>
            <ta e="T188" id="Seg_6102" s="T187">bu͡ol-AːččI</ta>
            <ta e="T189" id="Seg_6103" s="T188">ol</ta>
            <ta e="T190" id="Seg_6104" s="T189">ajɨːhɨt-nI</ta>
            <ta e="T191" id="Seg_6105" s="T190">bu͡ollagɨna</ta>
            <ta e="T192" id="Seg_6106" s="T191">maŋnaj</ta>
            <ta e="T193" id="Seg_6107" s="T192">ɨl-A</ta>
            <ta e="T194" id="Seg_6108" s="T193">kel-A</ta>
            <ta e="T195" id="Seg_6109" s="T194">kel-Ar-LAr</ta>
            <ta e="T196" id="Seg_6110" s="T195">dʼaktar-LArA</ta>
            <ta e="T197" id="Seg_6111" s="T196">töröː-AːrI</ta>
            <ta e="T198" id="Seg_6112" s="T197">gɨn-TAK-InA</ta>
            <ta e="T199" id="Seg_6113" s="T198">ol</ta>
            <ta e="T200" id="Seg_6114" s="T199">tuspa</ta>
            <ta e="T201" id="Seg_6115" s="T200">ol</ta>
            <ta e="T202" id="Seg_6116" s="T201">tuspa</ta>
            <ta e="T203" id="Seg_6117" s="T202">dʼuka-GA</ta>
            <ta e="T204" id="Seg_6118" s="T203">kim-nI</ta>
            <ta e="T205" id="Seg_6119" s="T204">da</ta>
            <ta e="T206" id="Seg_6120" s="T205">kiːr-TAr-BAT-LAr</ta>
            <ta e="T207" id="Seg_6121" s="T206">bu͡ollaga</ta>
            <ta e="T208" id="Seg_6122" s="T207">kim</ta>
            <ta e="T209" id="Seg_6123" s="T208">tuspa</ta>
            <ta e="T210" id="Seg_6124" s="T209">dʼuka</ta>
            <ta e="T211" id="Seg_6125" s="T210">tut-BAT-LAr</ta>
            <ta e="T212" id="Seg_6126" s="T211">dʼi͡e-GA</ta>
            <ta e="T213" id="Seg_6127" s="T212">töröː-A-t-AːččI-LAr</ta>
            <ta e="T214" id="Seg_6128" s="T213">dʼi͡e-GA</ta>
            <ta e="T215" id="Seg_6129" s="T214">olor-Ar</ta>
            <ta e="T216" id="Seg_6130" s="T215">dʼuka-LArI-GAr</ta>
            <ta e="T217" id="Seg_6131" s="T216">ol</ta>
            <ta e="T218" id="Seg_6132" s="T217">gɨn-An</ta>
            <ta e="T219" id="Seg_6133" s="T218">baran</ta>
            <ta e="T220" id="Seg_6134" s="T219">olor-Ar</ta>
            <ta e="T221" id="Seg_6135" s="T220">dʼuka-LArI-ttAn</ta>
            <ta e="T222" id="Seg_6136" s="T221">tuspa</ta>
            <ta e="T223" id="Seg_6137" s="T222">oŋor-AːččI-LAr</ta>
            <ta e="T224" id="Seg_6138" s="T223">togoho-nI</ta>
            <ta e="T225" id="Seg_6139" s="T224">tur-IAr-AːččI-LAr</ta>
            <ta e="T226" id="Seg_6140" s="T225">dʼe</ta>
            <ta e="T227" id="Seg_6141" s="T226">ol</ta>
            <ta e="T228" id="Seg_6142" s="T227">gɨn-An</ta>
            <ta e="T229" id="Seg_6143" s="T228">kihi-nI</ta>
            <ta e="T230" id="Seg_6144" s="T229">barɨ-kAːN-tI-n</ta>
            <ta e="T231" id="Seg_6145" s="T230">tahaːr-AːččI-LAr</ta>
            <ta e="T232" id="Seg_6146" s="T231">kim</ta>
            <ta e="T233" id="Seg_6147" s="T232">da</ta>
            <ta e="T234" id="Seg_6148" s="T233">hu͡ok</ta>
            <ta e="T235" id="Seg_6149" s="T234">bu͡ol-IAK-LAːK</ta>
            <ta e="T236" id="Seg_6150" s="T235">čogotok</ta>
            <ta e="T237" id="Seg_6151" s="T236">baːbɨska-nI</ta>
            <ta e="T238" id="Seg_6152" s="T237">kɨtta</ta>
            <ta e="T239" id="Seg_6153" s="T238">töröː-Ar</ta>
            <ta e="T240" id="Seg_6154" s="T239">dʼaktar</ta>
            <ta e="T241" id="Seg_6155" s="T240">ere</ta>
            <ta e="T242" id="Seg_6156" s="T241">bu͡ol-IAK-LAːK-LAr</ta>
            <ta e="T243" id="Seg_6157" s="T242">onno</ta>
            <ta e="T244" id="Seg_6158" s="T243">oččogo</ta>
            <ta e="T245" id="Seg_6159" s="T244">bu͡ollagɨna</ta>
            <ta e="T246" id="Seg_6160" s="T245">du͡o</ta>
            <ta e="T247" id="Seg_6161" s="T246">bu</ta>
            <ta e="T248" id="Seg_6162" s="T247">baːbɨska-ŋ</ta>
            <ta e="T249" id="Seg_6163" s="T248">kel-Ar</ta>
            <ta e="T250" id="Seg_6164" s="T249">uraha</ta>
            <ta e="T251" id="Seg_6165" s="T250">dʼi͡e-GA</ta>
            <ta e="T252" id="Seg_6166" s="T251">töröː-Ar</ta>
            <ta e="T253" id="Seg_6167" s="T252">dʼi͡e-GA</ta>
            <ta e="T254" id="Seg_6168" s="T253">dʼuka-GA</ta>
            <ta e="T255" id="Seg_6169" s="T254">tuspa</ta>
            <ta e="T256" id="Seg_6170" s="T255">tut-I-LIN-I-BIT</ta>
            <ta e="T257" id="Seg_6171" s="T256">bu͡ol-TAK-InA</ta>
            <ta e="T258" id="Seg_6172" s="T257">du͡o</ta>
            <ta e="T259" id="Seg_6173" s="T258">dʼi͡e-GA</ta>
            <ta e="T260" id="Seg_6174" s="T259">bu</ta>
            <ta e="T261" id="Seg_6175" s="T260">oŋohulun-I-BIT</ta>
            <ta e="T262" id="Seg_6176" s="T261">bu͡ol-TAK-InA</ta>
            <ta e="T263" id="Seg_6177" s="T262">ol</ta>
            <ta e="T264" id="Seg_6178" s="T263">kel-An</ta>
            <ta e="T265" id="Seg_6179" s="T264">ka</ta>
            <ta e="T266" id="Seg_6180" s="T265">egel-An</ta>
            <ta e="T267" id="Seg_6181" s="T266">kel-An-LAr</ta>
            <ta e="T268" id="Seg_6182" s="T267">ol</ta>
            <ta e="T269" id="Seg_6183" s="T268">baːbɨska</ta>
            <ta e="T270" id="Seg_6184" s="T269">emeːksin-tA</ta>
            <ta e="T271" id="Seg_6185" s="T270">kiːr-TAK-InA</ta>
            <ta e="T272" id="Seg_6186" s="T271">ol-nI</ta>
            <ta e="T273" id="Seg_6187" s="T272">barɨ-kAːN-nI</ta>
            <ta e="T274" id="Seg_6188" s="T273">jantar-LAː-An</ta>
            <ta e="T275" id="Seg_6189" s="T274">jantar-I-nAn</ta>
            <ta e="T276" id="Seg_6190" s="T275">tɨmtɨk-I-nAn</ta>
            <ta e="T277" id="Seg_6191" s="T276">ubat-An</ta>
            <ta e="T278" id="Seg_6192" s="T277">barɨ-kAːN-tI-n</ta>
            <ta e="T279" id="Seg_6193" s="T278">ɨraːstaː-Ar</ta>
            <ta e="T280" id="Seg_6194" s="T279">ol</ta>
            <ta e="T281" id="Seg_6195" s="T280">dʼaktar</ta>
            <ta e="T282" id="Seg_6196" s="T281">olor-Ar</ta>
            <ta e="T283" id="Seg_6197" s="T282">hir-tI-n</ta>
            <ta e="T284" id="Seg_6198" s="T283">olor-Ar</ta>
            <ta e="T285" id="Seg_6199" s="T284">dʼuka-tI-n</ta>
            <ta e="T286" id="Seg_6200" s="T285">tögürüččü</ta>
            <ta e="T287" id="Seg_6201" s="T286">alastaː-Ar</ta>
            <ta e="T288" id="Seg_6202" s="T287">türkeːt</ta>
            <ta e="T289" id="Seg_6203" s="T288">türkeːt</ta>
            <ta e="T290" id="Seg_6204" s="T289">türkeːt</ta>
            <ta e="T291" id="Seg_6205" s="T290">türkeːt</ta>
            <ta e="T292" id="Seg_6206" s="T291">bɨrtak-I-ŋ</ta>
            <ta e="T293" id="Seg_6207" s="T292">bɨlɨt</ta>
            <ta e="T294" id="Seg_6208" s="T293">kɨraːs-I-ŋ</ta>
            <ta e="T295" id="Seg_6209" s="T294">ɨj-GA</ta>
            <ta e="T296" id="Seg_6210" s="T295">türkeːt</ta>
            <ta e="T297" id="Seg_6211" s="T296">türkeːt</ta>
            <ta e="T298" id="Seg_6212" s="T297">türkeːt</ta>
            <ta e="T299" id="Seg_6213" s="T298">bɨrtak-I-ŋ</ta>
            <ta e="T300" id="Seg_6214" s="T299">bɨlɨt</ta>
            <ta e="T301" id="Seg_6215" s="T300">kɨraːs-I-ŋ</ta>
            <ta e="T302" id="Seg_6216" s="T301">ɨj-GA</ta>
            <ta e="T303" id="Seg_6217" s="T302">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T304" id="Seg_6218" s="T303">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T305" id="Seg_6219" s="T304">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T306" id="Seg_6220" s="T305">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T307" id="Seg_6221" s="T306">türkeːt</ta>
            <ta e="T308" id="Seg_6222" s="T307">bɨrtak-I-ŋ</ta>
            <ta e="T309" id="Seg_6223" s="T308">bɨlɨt</ta>
            <ta e="T310" id="Seg_6224" s="T309">kɨraːs-I-ŋ</ta>
            <ta e="T311" id="Seg_6225" s="T310">ɨj-GA</ta>
            <ta e="T312" id="Seg_6226" s="T311">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T313" id="Seg_6227" s="T312">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T314" id="Seg_6228" s="T313">di͡e-A-di͡e-A</ta>
            <ta e="T315" id="Seg_6229" s="T314">alastaː-Ar</ta>
            <ta e="T316" id="Seg_6230" s="T315">tögürüččü</ta>
            <ta e="T317" id="Seg_6231" s="T316">üs-TA</ta>
            <ta e="T318" id="Seg_6232" s="T317">kat</ta>
            <ta e="T319" id="Seg_6233" s="T318">onton</ta>
            <ta e="T320" id="Seg_6234" s="T319">dʼaktar-nI</ta>
            <ta e="T321" id="Seg_6235" s="T320">olor-Ar</ta>
            <ta e="T322" id="Seg_6236" s="T321">dʼaktar-nI</ta>
            <ta e="T323" id="Seg_6237" s="T322">olor-Ar</ta>
            <ta e="T324" id="Seg_6238" s="T323">ilik-InA</ta>
            <ta e="T325" id="Seg_6239" s="T324">konnok-tI-n</ta>
            <ta e="T326" id="Seg_6240" s="T325">alɨn-tI-nAn</ta>
            <ta e="T327" id="Seg_6241" s="T326">dʼogus-LIk</ta>
            <ta e="T328" id="Seg_6242" s="T327">olor-IAK-tI-n</ta>
            <ta e="T329" id="Seg_6243" s="T328">di͡e-An</ta>
            <ta e="T330" id="Seg_6244" s="T329">alastaː-Ar</ta>
            <ta e="T331" id="Seg_6245" s="T330">hol</ta>
            <ta e="T332" id="Seg_6246" s="T331">alastaː-Ar</ta>
            <ta e="T333" id="Seg_6247" s="T332">ebi͡ennʼe-tI-nAn</ta>
            <ta e="T334" id="Seg_6248" s="T333">tögürüččü</ta>
            <ta e="T335" id="Seg_6249" s="T334">kün</ta>
            <ta e="T336" id="Seg_6250" s="T335">kotun</ta>
            <ta e="T337" id="Seg_6251" s="T336">kelin-tI-GAr</ta>
            <ta e="T338" id="Seg_6252" s="T337">bar-BAT</ta>
            <ta e="T339" id="Seg_6253" s="T338">kelin-tI-n</ta>
            <ta e="T340" id="Seg_6254" s="T339">dek</ta>
            <ta e="T341" id="Seg_6255" s="T340">kim-nI</ta>
            <ta e="T342" id="Seg_6256" s="T341">da</ta>
            <ta e="T343" id="Seg_6257" s="T342">kaːm-TAr-AːččI-tA</ta>
            <ta e="T344" id="Seg_6258" s="T343">hu͡ok-LAr</ta>
            <ta e="T345" id="Seg_6259" s="T344">ɨ͡arakan</ta>
            <ta e="T346" id="Seg_6260" s="T345">dʼaktar-nI</ta>
            <ta e="T347" id="Seg_6261" s="T346">törüt</ta>
            <ta e="T348" id="Seg_6262" s="T347">kelin-I-LAː-IAK-tA</ta>
            <ta e="T349" id="Seg_6263" s="T348">di͡e-An-LAr</ta>
            <ta e="T350" id="Seg_6264" s="T349">ol</ta>
            <ta e="T351" id="Seg_6265" s="T350">ɨjaːk</ta>
            <ta e="T352" id="Seg_6266" s="T351">oččogo</ta>
            <ta e="T353" id="Seg_6267" s="T352">bu͡ollagɨna</ta>
            <ta e="T354" id="Seg_6268" s="T353">du͡o</ta>
            <ta e="T355" id="Seg_6269" s="T354">bu</ta>
            <ta e="T357" id="Seg_6270" s="T356">üs-TA</ta>
            <ta e="T358" id="Seg_6271" s="T357">kat</ta>
            <ta e="T359" id="Seg_6272" s="T358">ergij-t-Ar</ta>
            <ta e="T360" id="Seg_6273" s="T359">emi͡e</ta>
            <ta e="T361" id="Seg_6274" s="T360">türkeːt</ta>
            <ta e="T362" id="Seg_6275" s="T361">türkeːt</ta>
            <ta e="T363" id="Seg_6276" s="T362">türkeːt</ta>
            <ta e="T364" id="Seg_6277" s="T363">eteŋŋe</ta>
            <ta e="T365" id="Seg_6278" s="T364">olor-IAK-tI-n</ta>
            <ta e="T366" id="Seg_6279" s="T365">kɨːs-I-m</ta>
            <ta e="T367" id="Seg_6280" s="T366">ogo-m</ta>
            <ta e="T368" id="Seg_6281" s="T367">türkeːt</ta>
            <ta e="T369" id="Seg_6282" s="T368">türkeːt</ta>
            <ta e="T370" id="Seg_6283" s="T369">türkeːt</ta>
            <ta e="T371" id="Seg_6284" s="T370">eteŋŋe</ta>
            <ta e="T372" id="Seg_6285" s="T371">olor-IAK-tI-n</ta>
            <ta e="T373" id="Seg_6286" s="T372">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T374" id="Seg_6287" s="T373">türkeːt</ta>
            <ta e="T375" id="Seg_6288" s="T374">türkeːt</ta>
            <ta e="T376" id="Seg_6289" s="T375">türkeːt</ta>
            <ta e="T377" id="Seg_6290" s="T376">kɨraːs-I-ŋ</ta>
            <ta e="T378" id="Seg_6291" s="T377">ɨj-GA</ta>
            <ta e="T379" id="Seg_6292" s="T378">bɨrtak-I-ŋ</ta>
            <ta e="T380" id="Seg_6293" s="T379">bɨlɨt-GA</ta>
            <ta e="T381" id="Seg_6294" s="T380">kɨraːs-I-ŋ</ta>
            <ta e="T382" id="Seg_6295" s="T381">ɨj-GA</ta>
            <ta e="T383" id="Seg_6296" s="T382">bɨrtak-I-ŋ</ta>
            <ta e="T384" id="Seg_6297" s="T383">bɨlɨt-GA</ta>
            <ta e="T385" id="Seg_6298" s="T384">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T386" id="Seg_6299" s="T385">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T387" id="Seg_6300" s="T386">di͡e-AːččI-LAr</ta>
            <ta e="T388" id="Seg_6301" s="T387">üs-TA</ta>
            <ta e="T389" id="Seg_6302" s="T388">kat</ta>
            <ta e="T390" id="Seg_6303" s="T389">ergij-t-AːččI-LAr</ta>
            <ta e="T391" id="Seg_6304" s="T390">ol</ta>
            <ta e="T392" id="Seg_6305" s="T391">gɨn-An</ta>
            <ta e="T393" id="Seg_6306" s="T392">togoho-tI-ttAn</ta>
            <ta e="T394" id="Seg_6307" s="T393">tut-n-Ar</ta>
            <ta e="T395" id="Seg_6308" s="T394">ol</ta>
            <ta e="T396" id="Seg_6309" s="T395">olor-Ar</ta>
            <ta e="T397" id="Seg_6310" s="T396">dʼaktar-I-ŋ</ta>
            <ta e="T398" id="Seg_6311" s="T397">olor-Ar</ta>
            <ta e="T399" id="Seg_6312" s="T398">togoho-tI-ttAn</ta>
            <ta e="T400" id="Seg_6313" s="T399">oččogo</ta>
            <ta e="T401" id="Seg_6314" s="T400">bu͡ollagɨna</ta>
            <ta e="T402" id="Seg_6315" s="T401">ajɨːhɨt</ta>
            <ta e="T403" id="Seg_6316" s="T402">ɨl-Ar</ta>
            <ta e="T404" id="Seg_6317" s="T403">baːbɨska</ta>
            <ta e="T405" id="Seg_6318" s="T404">emeːksin</ta>
            <ta e="T406" id="Seg_6319" s="T405">ol</ta>
            <ta e="T407" id="Seg_6320" s="T406">ajɨːhɨt-tI-n</ta>
            <ta e="T408" id="Seg_6321" s="T407">ɨl-Ar</ta>
            <ta e="T409" id="Seg_6322" s="T408">uskaːn</ta>
            <ta e="T410" id="Seg_6323" s="T409">tiriː-tA</ta>
            <ta e="T411" id="Seg_6324" s="T410">ol</ta>
            <ta e="T412" id="Seg_6325" s="T411">ajɨːhɨt-kAːN-tA</ta>
            <ta e="T413" id="Seg_6326" s="T412">uskaːn-I-ŋ</ta>
            <ta e="T414" id="Seg_6327" s="T413">ajɨːhɨt</ta>
            <ta e="T415" id="Seg_6328" s="T414">aːt-LAːK</ta>
            <ta e="T416" id="Seg_6329" s="T415">bu͡ol-AːččI</ta>
            <ta e="T417" id="Seg_6330" s="T416">oččogo</ta>
            <ta e="T418" id="Seg_6331" s="T417">uskaːn-nI</ta>
            <ta e="T419" id="Seg_6332" s="T418">tut-An</ta>
            <ta e="T420" id="Seg_6333" s="T419">baran</ta>
            <ta e="T421" id="Seg_6334" s="T420">uskaːn-nI</ta>
            <ta e="T422" id="Seg_6335" s="T421">u͡ot-GA</ta>
            <ta e="T423" id="Seg_6336" s="T422">ahaː-A-t-Ar</ta>
            <ta e="T424" id="Seg_6337" s="T423">u͡ot-nI</ta>
            <ta e="T425" id="Seg_6338" s="T424">emi͡e</ta>
            <ta e="T426" id="Seg_6339" s="T425">ahaː-A-t-Ar</ta>
            <ta e="T427" id="Seg_6340" s="T426">u͡ot-ehe-kAːN-I-m</ta>
            <ta e="T428" id="Seg_6341" s="T427">ajɨːhɨt-BI-n</ta>
            <ta e="T429" id="Seg_6342" s="T428">ahaː-A-t-A-BIn</ta>
            <ta e="T430" id="Seg_6343" s="T429">ogo-BI-n</ta>
            <ta e="T431" id="Seg_6344" s="T430">eteŋŋe</ta>
            <ta e="T432" id="Seg_6345" s="T431">olor-t-IAK-tI-n</ta>
            <ta e="T433" id="Seg_6346" s="T432">eteŋŋe</ta>
            <ta e="T434" id="Seg_6347" s="T433">tabɨskaː-IAK-tI-n</ta>
            <ta e="T435" id="Seg_6348" s="T434">di͡e-An</ta>
            <ta e="T436" id="Seg_6349" s="T435">oččogo</ta>
            <ta e="T437" id="Seg_6350" s="T436">bu͡ollagɨna</ta>
            <ta e="T438" id="Seg_6351" s="T437">u͡ot</ta>
            <ta e="T439" id="Seg_6352" s="T438">ehe-kAːN-BI-n</ta>
            <ta e="T440" id="Seg_6353" s="T439">ahaː-A-t-A-BIn</ta>
            <ta e="T441" id="Seg_6354" s="T440">u͡ot</ta>
            <ta e="T442" id="Seg_6355" s="T441">ehe-kAːN-I-m</ta>
            <ta e="T443" id="Seg_6356" s="T442">ahaː</ta>
            <ta e="T444" id="Seg_6357" s="T443">eteŋŋe</ta>
            <ta e="T445" id="Seg_6358" s="T444">dʼol-TA</ta>
            <ta e="T446" id="Seg_6359" s="T445">keteː</ta>
            <ta e="T447" id="Seg_6360" s="T446">tu͡ok</ta>
            <ta e="T448" id="Seg_6361" s="T447">da</ta>
            <ta e="T449" id="Seg_6362" s="T448">kuhagan-nI</ta>
            <ta e="T450" id="Seg_6363" s="T449">čugahaː-t-I-BA</ta>
            <ta e="T451" id="Seg_6364" s="T450">eteŋŋe</ta>
            <ta e="T452" id="Seg_6365" s="T451">olor-t</ta>
            <ta e="T453" id="Seg_6366" s="T452">ogo-BI-n</ta>
            <ta e="T454" id="Seg_6367" s="T453">eteŋŋe</ta>
            <ta e="T455" id="Seg_6368" s="T454">boskoloː</ta>
            <ta e="T456" id="Seg_6369" s="T455">di͡e-An</ta>
            <ta e="T457" id="Seg_6370" s="T456">onton</ta>
            <ta e="T458" id="Seg_6371" s="T457">ol</ta>
            <ta e="T459" id="Seg_6372" s="T458">ajɨːhɨt-kAːN-tI-n</ta>
            <ta e="T460" id="Seg_6373" s="T459">ahaː-A-t-An</ta>
            <ta e="T461" id="Seg_6374" s="T460">baran</ta>
            <ta e="T462" id="Seg_6375" s="T461">togoho-GA</ta>
            <ta e="T463" id="Seg_6376" s="T462">ɨjaː-Ar</ta>
            <ta e="T464" id="Seg_6377" s="T463">ol</ta>
            <ta e="T465" id="Seg_6378" s="T464">togoho-GA</ta>
            <ta e="T466" id="Seg_6379" s="T465">baːj-An</ta>
            <ta e="T467" id="Seg_6380" s="T466">keːs-Ar</ta>
            <ta e="T468" id="Seg_6381" s="T467">ol</ta>
            <ta e="T469" id="Seg_6382" s="T468">baːj-An</ta>
            <ta e="T470" id="Seg_6383" s="T469">keːs-An</ta>
            <ta e="T471" id="Seg_6384" s="T470">baran</ta>
            <ta e="T472" id="Seg_6385" s="T471">ol</ta>
            <ta e="T473" id="Seg_6386" s="T472">ogolon-Ar</ta>
            <ta e="T474" id="Seg_6387" s="T473">dʼaktar</ta>
            <ta e="T475" id="Seg_6388" s="T474">kɨ͡aj-An</ta>
            <ta e="T476" id="Seg_6389" s="T475">ol</ta>
            <ta e="T477" id="Seg_6390" s="T476">kuhagan-LIk</ta>
            <ta e="T478" id="Seg_6391" s="T477">üːn-n-Ar</ta>
            <ta e="T479" id="Seg_6392" s="T478">bu͡ol-TAK-InA</ta>
            <ta e="T480" id="Seg_6393" s="T479">kömölös-Ar</ta>
            <ta e="T481" id="Seg_6394" s="T480">baːbɨska</ta>
            <ta e="T482" id="Seg_6395" s="T481">tobuk-tI-GAr</ta>
            <ta e="T483" id="Seg_6396" s="T482">olort-Ar</ta>
            <ta e="T484" id="Seg_6397" s="T483">kuturuk-tI-n</ta>
            <ta e="T485" id="Seg_6398" s="T484">tördü-tI-nAn</ta>
            <ta e="T486" id="Seg_6399" s="T485">ogo-tA</ta>
            <ta e="T487" id="Seg_6400" s="T486">kelin-LAː-m-IAK-tI-n</ta>
            <ta e="T488" id="Seg_6401" s="T487">di͡e-An</ta>
            <ta e="T489" id="Seg_6402" s="T488">onton</ta>
            <ta e="T490" id="Seg_6403" s="T489">bɨ͡ar-tI-n</ta>
            <ta e="T491" id="Seg_6404" s="T490">ilbij-Ar</ta>
            <ta e="T492" id="Seg_6405" s="T491">ol</ta>
            <ta e="T493" id="Seg_6406" s="T492">bɨ͡ar-tI-n</ta>
            <ta e="T494" id="Seg_6407" s="T493">ilbij-An</ta>
            <ta e="T495" id="Seg_6408" s="T494">kömölös-An</ta>
            <ta e="T496" id="Seg_6409" s="T495">ol</ta>
            <ta e="T497" id="Seg_6410" s="T496">ogo-tI-n</ta>
            <ta e="T498" id="Seg_6411" s="T497">kömölös-An</ta>
            <ta e="T499" id="Seg_6412" s="T498">tabɨskaː-Ar</ta>
            <ta e="T500" id="Seg_6413" s="T499">ol</ta>
            <ta e="T501" id="Seg_6414" s="T500">dʼaktar</ta>
            <ta e="T502" id="Seg_6415" s="T501">ol</ta>
            <ta e="T503" id="Seg_6416" s="T502">tabɨskaː-Ar</ta>
            <ta e="T504" id="Seg_6417" s="T503">aːt-tA</ta>
            <ta e="T505" id="Seg_6418" s="T504">ogo</ta>
            <ta e="T506" id="Seg_6419" s="T505">töröː-Ar-tA</ta>
            <ta e="T507" id="Seg_6420" s="T506">ol</ta>
            <ta e="T508" id="Seg_6421" s="T507">tabɨskaː-An</ta>
            <ta e="T509" id="Seg_6422" s="T508">ogo</ta>
            <ta e="T510" id="Seg_6423" s="T509">tüs-BIT-tI-GAr</ta>
            <ta e="T511" id="Seg_6424" s="T510">ol</ta>
            <ta e="T512" id="Seg_6425" s="T511">ogo-nI</ta>
            <ta e="T513" id="Seg_6426" s="T512">kiːn-tI-n</ta>
            <ta e="T514" id="Seg_6427" s="T513">bɨs-Ar-LAr</ta>
            <ta e="T515" id="Seg_6428" s="T514">bɨs-A-BIt</ta>
            <ta e="T516" id="Seg_6429" s="T515">ol</ta>
            <ta e="T517" id="Seg_6430" s="T516">baːbɨska</ta>
            <ta e="T518" id="Seg_6431" s="T517">emeːksin</ta>
            <ta e="T519" id="Seg_6432" s="T518">bɨs-Ar</ta>
            <ta e="T520" id="Seg_6433" s="T519">ol</ta>
            <ta e="T521" id="Seg_6434" s="T520">kiːn-tI-n</ta>
            <ta e="T522" id="Seg_6435" s="T521">bɨs-An</ta>
            <ta e="T523" id="Seg_6436" s="T522">baran</ta>
            <ta e="T524" id="Seg_6437" s="T523">du͡o</ta>
            <ta e="T525" id="Seg_6438" s="T524">baːj-An</ta>
            <ta e="T526" id="Seg_6439" s="T525">keːs-Ar</ta>
            <ta e="T527" id="Seg_6440" s="T526">iŋiːr</ta>
            <ta e="T528" id="Seg_6441" s="T527">hap-I-nAn</ta>
            <ta e="T529" id="Seg_6442" s="T528">kat-I-LIN-I-BIT</ta>
            <ta e="T530" id="Seg_6443" s="T529">iŋiːr</ta>
            <ta e="T531" id="Seg_6444" s="T530">hap-I-nAn</ta>
            <ta e="T532" id="Seg_6445" s="T531">baːj-Ar</ta>
            <ta e="T533" id="Seg_6446" s="T532">tulun-An</ta>
            <ta e="T534" id="Seg_6447" s="T533">tüs-IAK-tI-n</ta>
            <ta e="T535" id="Seg_6448" s="T534">kojut</ta>
            <ta e="T536" id="Seg_6449" s="T535">bɨha</ta>
            <ta e="T537" id="Seg_6450" s="T536">hɨtɨj-An</ta>
            <ta e="T538" id="Seg_6451" s="T537">bi͡es</ta>
            <ta e="T539" id="Seg_6452" s="T538">kün</ta>
            <ta e="T540" id="Seg_6453" s="T539">du͡o</ta>
            <ta e="T541" id="Seg_6454" s="T540">kas</ta>
            <ta e="T542" id="Seg_6455" s="T541">du͡o</ta>
            <ta e="T543" id="Seg_6456" s="T542">kün</ta>
            <ta e="T544" id="Seg_6457" s="T543">bu͡ol-An</ta>
            <ta e="T545" id="Seg_6458" s="T544">baran</ta>
            <ta e="T546" id="Seg_6459" s="T545">tüs-AːččI</ta>
            <ta e="T547" id="Seg_6460" s="T546">ogo</ta>
            <ta e="T548" id="Seg_6461" s="T547">kiːn-tA</ta>
            <ta e="T549" id="Seg_6462" s="T548">onton</ta>
            <ta e="T550" id="Seg_6463" s="T549">bu͡ollagɨna</ta>
            <ta e="T551" id="Seg_6464" s="T550">du͡o</ta>
            <ta e="T552" id="Seg_6465" s="T551">ogo-tI-n</ta>
            <ta e="T553" id="Seg_6466" s="T552">inʼe-tA</ta>
            <ta e="T554" id="Seg_6467" s="T553">tüs-BIT-tI-n</ta>
            <ta e="T555" id="Seg_6468" s="T554">ol</ta>
            <ta e="T556" id="Seg_6469" s="T555">ogo-tI-n</ta>
            <ta e="T557" id="Seg_6470" s="T556">inʼe-tI-n</ta>
            <ta e="T558" id="Seg_6471" s="T557">huːj-Ar</ta>
            <ta e="T559" id="Seg_6472" s="T558">ogo-tI-n</ta>
            <ta e="T560" id="Seg_6473" s="T559">emi͡e</ta>
            <ta e="T561" id="Seg_6474" s="T560">huːj-Ar</ta>
            <ta e="T562" id="Seg_6475" s="T561">kahan</ta>
            <ta e="T563" id="Seg_6476" s="T562">daːganɨ</ta>
            <ta e="T564" id="Seg_6477" s="T563">huːj-An</ta>
            <ta e="T565" id="Seg_6478" s="T564">baran</ta>
            <ta e="T566" id="Seg_6479" s="T565">huːlaː-An</ta>
            <ta e="T567" id="Seg_6480" s="T566">keːs-Ar</ta>
            <ta e="T568" id="Seg_6481" s="T567">bu͡olla</ta>
            <ta e="T569" id="Seg_6482" s="T568">onton</ta>
            <ta e="T570" id="Seg_6483" s="T569">bu͡ollagɨna</ta>
            <ta e="T571" id="Seg_6484" s="T570">du͡o</ta>
            <ta e="T573" id="Seg_6485" s="T572">bihik-GA</ta>
            <ta e="T574" id="Seg_6486" s="T573">össü͡ö</ta>
            <ta e="T575" id="Seg_6487" s="T574">bileː-BAT-LAr</ta>
            <ta e="T576" id="Seg_6488" s="T575">bu͡ollaga</ta>
            <ta e="T577" id="Seg_6489" s="T576">huːlaː-An</ta>
            <ta e="T578" id="Seg_6490" s="T577">keːs-Ar</ta>
            <ta e="T579" id="Seg_6491" s="T578">maːma-tI-n</ta>
            <ta e="T580" id="Seg_6492" s="T579">attɨ-GAr</ta>
            <ta e="T581" id="Seg_6493" s="T580">uːr-Ar</ta>
            <ta e="T582" id="Seg_6494" s="T581">onton</ta>
            <ta e="T583" id="Seg_6495" s="T582">du͡o</ta>
            <ta e="T584" id="Seg_6496" s="T583">ajɨːhɨt-tI-n</ta>
            <ta e="T586" id="Seg_6497" s="T585">ajɨːhɨt-tI-n</ta>
            <ta e="T587" id="Seg_6498" s="T586">huːj-An</ta>
            <ta e="T588" id="Seg_6499" s="T587">baran</ta>
            <ta e="T589" id="Seg_6500" s="T588">ol</ta>
            <ta e="T590" id="Seg_6501" s="T589">ajɨːhɨt-tI-n</ta>
            <ta e="T591" id="Seg_6502" s="T590">möhöːk-tI-GAr</ta>
            <ta e="T592" id="Seg_6503" s="T591">uk-An</ta>
            <ta e="T593" id="Seg_6504" s="T592">keːs-An</ta>
            <ta e="T594" id="Seg_6505" s="T593">baran</ta>
            <ta e="T595" id="Seg_6506" s="T594">ol</ta>
            <ta e="T596" id="Seg_6507" s="T595">mataŋa-tI-GAr</ta>
            <ta e="T597" id="Seg_6508" s="T596">ol</ta>
            <ta e="T598" id="Seg_6509" s="T597">mataŋa-LIːN</ta>
            <ta e="T599" id="Seg_6510" s="T598">ol</ta>
            <ta e="T601" id="Seg_6511" s="T600">togoho-GA</ta>
            <ta e="T602" id="Seg_6512" s="T601">baːj-An</ta>
            <ta e="T603" id="Seg_6513" s="T602">keːs-Ar</ta>
            <ta e="T604" id="Seg_6514" s="T603">ol</ta>
            <ta e="T605" id="Seg_6515" s="T604">togoho-GA</ta>
            <ta e="T606" id="Seg_6516" s="T605">tur-Ar</ta>
            <ta e="T607" id="Seg_6517" s="T606">bu</ta>
            <ta e="T608" id="Seg_6518" s="T607">ogo</ta>
            <ta e="T609" id="Seg_6519" s="T608">inʼe-tA</ta>
            <ta e="T610" id="Seg_6520" s="T609">üje-tI-n</ta>
            <ta e="T611" id="Seg_6521" s="T610">turkarɨ</ta>
            <ta e="T612" id="Seg_6522" s="T611">bu</ta>
            <ta e="T613" id="Seg_6523" s="T612">ogo</ta>
            <ta e="T614" id="Seg_6524" s="T613">töröː-BIT</ta>
            <ta e="T615" id="Seg_6525" s="T614">dʼuka-tI-n</ta>
            <ta e="T616" id="Seg_6526" s="T615">is-tI-GAr</ta>
            <ta e="T617" id="Seg_6527" s="T616">ogo</ta>
            <ta e="T618" id="Seg_6528" s="T617">töröː-BIT</ta>
            <ta e="T619" id="Seg_6529" s="T618">togoho-tI-GAr</ta>
            <ta e="T620" id="Seg_6530" s="T619">dʼaktar</ta>
            <ta e="T621" id="Seg_6531" s="T620">töröː-BIT</ta>
            <ta e="T622" id="Seg_6532" s="T621">togoho-tI-GAr</ta>
            <ta e="T623" id="Seg_6533" s="T622">oččogo</ta>
            <ta e="T624" id="Seg_6534" s="T623">di͡e-AːččI-LAr</ta>
            <ta e="T625" id="Seg_6535" s="T624">bɨlɨr-GI</ta>
            <ta e="T626" id="Seg_6536" s="T625">üje-ttAn</ta>
            <ta e="T627" id="Seg_6537" s="T626">togoho-LAːK</ta>
            <ta e="T628" id="Seg_6538" s="T627">huːrt-nI</ta>
            <ta e="T629" id="Seg_6539" s="T628">aːs-TAK-TArInA</ta>
            <ta e="T630" id="Seg_6540" s="T629">dʼuka-tA</ta>
            <ta e="T631" id="Seg_6541" s="T630">da</ta>
            <ta e="T632" id="Seg_6542" s="T631">hu͡ok</ta>
            <ta e="T633" id="Seg_6543" s="T632">bu͡ol-TAK-InA</ta>
            <ta e="T634" id="Seg_6544" s="T633">togoho-nI</ta>
            <ta e="T635" id="Seg_6545" s="T634">tut-A</ta>
            <ta e="T636" id="Seg_6546" s="T635">tur-Ar-tI-n</ta>
            <ta e="T637" id="Seg_6547" s="T636">kör-TAK-TArInA</ta>
            <ta e="T638" id="Seg_6548" s="T637">ka</ta>
            <ta e="T639" id="Seg_6549" s="T638">bu</ta>
            <ta e="T640" id="Seg_6550" s="T639">huːrt-GA</ta>
            <ta e="T641" id="Seg_6551" s="T640">dʼaktar</ta>
            <ta e="T642" id="Seg_6552" s="T641">töröː-BIT</ta>
            <ta e="T643" id="Seg_6553" s="T642">e-BIT</ta>
            <ta e="T644" id="Seg_6554" s="T643">togoho-tA</ta>
            <ta e="T645" id="Seg_6555" s="T644">tur-Ar</ta>
            <ta e="T646" id="Seg_6556" s="T645">töröː-BIT</ta>
            <ta e="T647" id="Seg_6557" s="T646">togoho-tA</ta>
            <ta e="T648" id="Seg_6558" s="T647">dʼuka-LAːK</ta>
            <ta e="T649" id="Seg_6559" s="T648">bu͡ol-TAK-InA</ta>
            <ta e="T650" id="Seg_6560" s="T649">onnuk</ta>
            <ta e="T651" id="Seg_6561" s="T650">ogo</ta>
            <ta e="T652" id="Seg_6562" s="T651">töröː-BIT</ta>
            <ta e="T653" id="Seg_6563" s="T652">e-BIT</ta>
            <ta e="T654" id="Seg_6564" s="T653">ol</ta>
            <ta e="T655" id="Seg_6565" s="T654">ogo</ta>
            <ta e="T656" id="Seg_6566" s="T655">töröː-BIT</ta>
            <ta e="T657" id="Seg_6567" s="T656">dʼuka-tA</ta>
            <ta e="T658" id="Seg_6568" s="T657">di͡e-AːččI-LAr</ta>
            <ta e="T659" id="Seg_6569" s="T658">oččogo</ta>
            <ta e="T660" id="Seg_6570" s="T659">bu͡ollagɨna</ta>
            <ta e="T661" id="Seg_6571" s="T660">du͡o</ta>
            <ta e="T662" id="Seg_6572" s="T661">onton</ta>
            <ta e="T663" id="Seg_6573" s="T662">bu</ta>
            <ta e="T664" id="Seg_6574" s="T663">dʼaktar-nI</ta>
            <ta e="T665" id="Seg_6575" s="T664">ogo-LAN-BIT</ta>
            <ta e="T666" id="Seg_6576" s="T665">dʼaktar-I-ŋ</ta>
            <ta e="T667" id="Seg_6577" s="T666">bɨrtak</ta>
            <ta e="T668" id="Seg_6578" s="T667">bu͡o</ta>
            <ta e="T669" id="Seg_6579" s="T668">töröː-BIT</ta>
            <ta e="T670" id="Seg_6580" s="T669">bɨrtak-tI-n</ta>
            <ta e="T671" id="Seg_6581" s="T670">ɨraːstaː-An</ta>
            <ta e="T672" id="Seg_6582" s="T671">emi͡e</ta>
            <ta e="T673" id="Seg_6583" s="T672">alastaː-A-GIn</ta>
            <ta e="T674" id="Seg_6584" s="T673">hɨraj-tI-n</ta>
            <ta e="T675" id="Seg_6585" s="T674">alɨn-tI-nAn</ta>
            <ta e="T676" id="Seg_6586" s="T675">moːj-tI-n</ta>
            <ta e="T677" id="Seg_6587" s="T676">tögürüččü</ta>
            <ta e="T678" id="Seg_6588" s="T677">üs-TA</ta>
            <ta e="T679" id="Seg_6589" s="T678">kat</ta>
            <ta e="T680" id="Seg_6590" s="T679">alastaː-A-GIn</ta>
            <ta e="T681" id="Seg_6591" s="T680">türkeːt</ta>
            <ta e="T682" id="Seg_6592" s="T681">türkeːt</ta>
            <ta e="T683" id="Seg_6593" s="T682">bɨrtak-I-ŋ</ta>
            <ta e="T684" id="Seg_6594" s="T683">bɨlɨt-GA</ta>
            <ta e="T685" id="Seg_6595" s="T684">kɨraːs-I-ŋ</ta>
            <ta e="T686" id="Seg_6596" s="T685">ɨj-GA</ta>
            <ta e="T687" id="Seg_6597" s="T686">türkeːt</ta>
            <ta e="T688" id="Seg_6598" s="T687">türkeːt</ta>
            <ta e="T689" id="Seg_6599" s="T688">bɨrtak-I-ŋ</ta>
            <ta e="T690" id="Seg_6600" s="T689">bɨlɨt-GA</ta>
            <ta e="T691" id="Seg_6601" s="T690">kɨraːs-I-ŋ</ta>
            <ta e="T692" id="Seg_6602" s="T691">ɨj-GA</ta>
            <ta e="T693" id="Seg_6603" s="T692">türkeːt-türkeːt</ta>
            <ta e="T694" id="Seg_6604" s="T693">bɨrtak-tI-n</ta>
            <ta e="T695" id="Seg_6605" s="T694">bɨlɨt-GA</ta>
            <ta e="T696" id="Seg_6606" s="T695">kɨraːs-I-ŋ</ta>
            <ta e="T697" id="Seg_6607" s="T696">ɨj-GA</ta>
            <ta e="T698" id="Seg_6608" s="T697">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T699" id="Seg_6609" s="T698">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T700" id="Seg_6610" s="T699">tu͡om-I-ŋ-sɨram-I-ŋ</ta>
            <ta e="T701" id="Seg_6611" s="T700">di͡e-An-GIn</ta>
            <ta e="T702" id="Seg_6612" s="T701">di͡e-An-GIn</ta>
            <ta e="T703" id="Seg_6613" s="T702">alastaː-A-GIn</ta>
            <ta e="T704" id="Seg_6614" s="T703">ol</ta>
            <ta e="T705" id="Seg_6615" s="T704">ol</ta>
            <ta e="T706" id="Seg_6616" s="T705">gɨn-An</ta>
            <ta e="T707" id="Seg_6617" s="T706">baran</ta>
            <ta e="T708" id="Seg_6618" s="T707">ogo-tI-n</ta>
            <ta e="T709" id="Seg_6619" s="T708">ɨl-Ar</ta>
            <ta e="T710" id="Seg_6620" s="T709">baːbɨska</ta>
            <ta e="T711" id="Seg_6621" s="T710">onton</ta>
            <ta e="T712" id="Seg_6622" s="T711">du͡o</ta>
            <ta e="T713" id="Seg_6623" s="T712">dʼaktar-nI</ta>
            <ta e="T714" id="Seg_6624" s="T713">tur-IAr-Ar</ta>
            <ta e="T715" id="Seg_6625" s="T714">hɨt-Ar</ta>
            <ta e="T716" id="Seg_6626" s="T715">tellek-tI-ttAn</ta>
            <ta e="T717" id="Seg_6627" s="T716">ol</ta>
            <ta e="T718" id="Seg_6628" s="T717">töröː-BIT</ta>
            <ta e="T719" id="Seg_6629" s="T718">dʼuka-tI-GAr</ta>
            <ta e="T720" id="Seg_6630" s="T719">onton</ta>
            <ta e="T721" id="Seg_6631" s="T720">tur-An</ta>
            <ta e="T722" id="Seg_6632" s="T721">tahaːr-A-BIt</ta>
            <ta e="T723" id="Seg_6633" s="T722">dʼi͡e-BItI-GAr</ta>
            <ta e="T724" id="Seg_6634" s="T723">bar-A-BIt</ta>
            <ta e="T725" id="Seg_6635" s="T724">hi͡et-An</ta>
            <ta e="T726" id="Seg_6636" s="T725">ilin-A-BIt</ta>
            <ta e="T727" id="Seg_6637" s="T726">töröː-BIT</ta>
            <ta e="T728" id="Seg_6638" s="T727">dʼaktar-nI</ta>
            <ta e="T729" id="Seg_6639" s="T728">ogo-LAN-BIT</ta>
            <ta e="T730" id="Seg_6640" s="T729">dʼaktar-nI</ta>
            <ta e="T731" id="Seg_6641" s="T730">ol</ta>
            <ta e="T732" id="Seg_6642" s="T731">gɨn-An</ta>
            <ta e="T733" id="Seg_6643" s="T732">baran</ta>
            <ta e="T734" id="Seg_6644" s="T733">ol</ta>
            <ta e="T735" id="Seg_6645" s="T734">hi͡et-An</ta>
            <ta e="T736" id="Seg_6646" s="T735">er-An</ta>
            <ta e="T737" id="Seg_6647" s="T736">ol</ta>
            <ta e="T738" id="Seg_6648" s="T737">kepseː-Ar</ta>
            <ta e="T739" id="Seg_6649" s="T738">ol</ta>
            <ta e="T740" id="Seg_6650" s="T739">kaja</ta>
            <ta e="T741" id="Seg_6651" s="T740">ogo</ta>
            <ta e="T742" id="Seg_6652" s="T741">töröː-TI-tA</ta>
            <ta e="T743" id="Seg_6653" s="T742">di͡e-An</ta>
            <ta e="T744" id="Seg_6654" s="T743">kergen-LArI-GAr</ta>
            <ta e="T745" id="Seg_6655" s="T744">dʼi͡e-LAːK</ta>
            <ta e="T746" id="Seg_6656" s="T745">dʼi͡e-LArI-GAr</ta>
            <ta e="T747" id="Seg_6657" s="T746">dʼi͡e-LAːK</ta>
            <ta e="T748" id="Seg_6658" s="T747">uraha</ta>
            <ta e="T749" id="Seg_6659" s="T748">dʼi͡e-GA</ta>
            <ta e="T750" id="Seg_6660" s="T749">ol</ta>
            <ta e="T751" id="Seg_6661" s="T750">kepseː-An</ta>
            <ta e="T752" id="Seg_6662" s="T751">kepseː-Ar</ta>
            <ta e="T753" id="Seg_6663" s="T752">kajaː</ta>
            <ta e="T754" id="Seg_6664" s="T753">ol</ta>
            <ta e="T755" id="Seg_6665" s="T754">kɨːs</ta>
            <ta e="T756" id="Seg_6666" s="T755">ogo</ta>
            <ta e="T757" id="Seg_6667" s="T756">töröː-BIT</ta>
            <ta e="T758" id="Seg_6668" s="T757">bu͡ol-TAK-InA</ta>
            <ta e="T759" id="Seg_6669" s="T758">kɨːs</ta>
            <ta e="T760" id="Seg_6670" s="T759">ogo</ta>
            <ta e="T761" id="Seg_6671" s="T760">di͡e-Ar-LAr</ta>
            <ta e="T762" id="Seg_6672" s="T761">u͡ol</ta>
            <ta e="T763" id="Seg_6673" s="T762">ogo</ta>
            <ta e="T764" id="Seg_6674" s="T763">töröː-BIT</ta>
            <ta e="T765" id="Seg_6675" s="T764">bu͡ol-TAK-InA</ta>
            <ta e="T766" id="Seg_6676" s="T765">u͡ol</ta>
            <ta e="T767" id="Seg_6677" s="T766">ogo</ta>
            <ta e="T768" id="Seg_6678" s="T767">di͡e-Ar-LAr</ta>
            <ta e="T769" id="Seg_6679" s="T768">onton</ta>
            <ta e="T770" id="Seg_6680" s="T769">bu</ta>
            <ta e="T771" id="Seg_6681" s="T770">üs</ta>
            <ta e="T772" id="Seg_6682" s="T771">kün</ta>
            <ta e="T773" id="Seg_6683" s="T772">bu͡ol-An</ta>
            <ta e="T774" id="Seg_6684" s="T773">baran</ta>
            <ta e="T775" id="Seg_6685" s="T774">aga-tA</ta>
            <ta e="T776" id="Seg_6686" s="T775">bihik-nI</ta>
            <ta e="T777" id="Seg_6687" s="T776">oŋor-Ar</ta>
            <ta e="T778" id="Seg_6688" s="T777">ogo-tI-GAr</ta>
            <ta e="T779" id="Seg_6689" s="T778">ehe-tA</ta>
            <ta e="T780" id="Seg_6690" s="T779">kömölös-Ar</ta>
            <ta e="T781" id="Seg_6691" s="T780">oččogo</ta>
            <ta e="T782" id="Seg_6692" s="T781">bu͡o</ta>
            <ta e="T783" id="Seg_6693" s="T782">bu</ta>
            <ta e="T784" id="Seg_6694" s="T783">bihik-nI</ta>
            <ta e="T785" id="Seg_6695" s="T784">du͡o</ta>
            <ta e="T786" id="Seg_6696" s="T785">tap-I-LIN-I-BIT</ta>
            <ta e="T787" id="Seg_6697" s="T786">bige</ta>
            <ta e="T788" id="Seg_6698" s="T787">doskoː-LAr-I-nAn</ta>
            <ta e="T789" id="Seg_6699" s="T788">bɨ͡a-nAn</ta>
            <ta e="T790" id="Seg_6700" s="T789">harɨː</ta>
            <ta e="T791" id="Seg_6701" s="T790">bɨ͡a-nAn</ta>
            <ta e="T792" id="Seg_6702" s="T791">üːj-An</ta>
            <ta e="T793" id="Seg_6703" s="T792">oŋor-AːččI-LAr</ta>
            <ta e="T794" id="Seg_6704" s="T793">karɨja-nI</ta>
            <ta e="T795" id="Seg_6705" s="T794">bu</ta>
            <ta e="T796" id="Seg_6706" s="T795">kim</ta>
            <ta e="T797" id="Seg_6707" s="T796">eː</ta>
            <ta e="T798" id="Seg_6708" s="T797">fanʼera</ta>
            <ta e="T799" id="Seg_6709" s="T798">kördük</ta>
            <ta e="T800" id="Seg_6710" s="T799">di͡e-AːččI-LAr</ta>
            <ta e="T801" id="Seg_6711" s="T800">iti</ta>
            <ta e="T802" id="Seg_6712" s="T801">itinnik-nI</ta>
            <ta e="T803" id="Seg_6713" s="T802">du͡o</ta>
            <ta e="T804" id="Seg_6714" s="T803">oŋor-TAr-AːččI-tA</ta>
            <ta e="T805" id="Seg_6715" s="T804">hu͡ok-LAr</ta>
            <ta e="T806" id="Seg_6716" s="T805">kat-BIT</ta>
            <ta e="T807" id="Seg_6717" s="T806">mas</ta>
            <ta e="T808" id="Seg_6718" s="T807">tiriː-tA</ta>
            <ta e="T809" id="Seg_6719" s="T808">iti</ta>
            <ta e="T810" id="Seg_6720" s="T809">fanʼera</ta>
            <ta e="T811" id="Seg_6721" s="T810">ol</ta>
            <ta e="T812" id="Seg_6722" s="T811">ihin</ta>
            <ta e="T813" id="Seg_6723" s="T812">bu͡ollagɨna</ta>
            <ta e="T814" id="Seg_6724" s="T813">du͡o</ta>
            <ta e="T815" id="Seg_6725" s="T814">ogo</ta>
            <ta e="T816" id="Seg_6726" s="T815">kat-IAK.[tA]</ta>
            <ta e="T817" id="Seg_6727" s="T816">di͡e-AːččI-LAr</ta>
            <ta e="T818" id="Seg_6728" s="T817">ol</ta>
            <ta e="T819" id="Seg_6729" s="T818">ihin</ta>
            <ta e="T0" id="Seg_6730" s="T820">doskoː-nAn</ta>
            <ta e="T822" id="Seg_6731" s="T821">a</ta>
            <ta e="T823" id="Seg_6732" s="T822">doskoː-ŋ</ta>
            <ta e="T824" id="Seg_6733" s="T823">üːj-I-LIN-TAK-InA</ta>
            <ta e="T825" id="Seg_6734" s="T824">du͡o</ta>
            <ta e="T826" id="Seg_6735" s="T825">üːj-I-LIN-TAK-InA</ta>
            <ta e="T827" id="Seg_6736" s="T826">üːj-I-LIN-TAK-InA</ta>
            <ta e="T828" id="Seg_6737" s="T827">bɨ͡a-nAn</ta>
            <ta e="T829" id="Seg_6738" s="T828">üːj-An</ta>
            <ta e="T830" id="Seg_6739" s="T829">oŋor-TAK-TArInA</ta>
            <ta e="T831" id="Seg_6740" s="T830">ol-tI-ŋ</ta>
            <ta e="T832" id="Seg_6741" s="T831">bɨːs-LAːK</ta>
            <ta e="T833" id="Seg_6742" s="T832">bu͡ol-AːččI</ta>
            <ta e="T834" id="Seg_6743" s="T833">doskoː</ta>
            <ta e="T835" id="Seg_6744" s="T834">doskoː-tI-ttAn</ta>
            <ta e="T836" id="Seg_6745" s="T835">emi͡e</ta>
            <ta e="T837" id="Seg_6746" s="T836">bɨːs-LAːK</ta>
            <ta e="T838" id="Seg_6747" s="T837">bu͡ol-AːččI</ta>
            <ta e="T839" id="Seg_6748" s="T838">oččogo</ta>
            <ta e="T840" id="Seg_6749" s="T839">tɨːn</ta>
            <ta e="T841" id="Seg_6750" s="T840">kiːr-Ar</ta>
            <ta e="T842" id="Seg_6751" s="T841">bu͡ollaga</ta>
            <ta e="T843" id="Seg_6752" s="T842">ol</ta>
            <ta e="T844" id="Seg_6753" s="T843">ihin</ta>
            <ta e="T845" id="Seg_6754" s="T844">ogo-ŋ</ta>
            <ta e="T846" id="Seg_6755" s="T845">zdarovaj</ta>
            <ta e="T847" id="Seg_6756" s="T846">ü͡öskeː-Ar</ta>
            <ta e="T848" id="Seg_6757" s="T847">bu͡olla</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_6758" s="T1">long.ago-ADJZ</ta>
            <ta e="T3" id="Seg_6759" s="T2">child.[NOM]</ta>
            <ta e="T4" id="Seg_6760" s="T3">give.birth-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T5" id="Seg_6761" s="T4">birth.pole-DAT/LOC</ta>
            <ta e="T6" id="Seg_6762" s="T5">birth.pole-DAT/LOC</ta>
            <ta e="T7" id="Seg_6763" s="T6">child-3PL.[NOM]</ta>
            <ta e="T8" id="Seg_6764" s="T7">child</ta>
            <ta e="T9" id="Seg_6765" s="T8">pregnant.[NOM]</ta>
            <ta e="T10" id="Seg_6766" s="T9">woman.[NOM]</ta>
            <ta e="T11" id="Seg_6767" s="T10">give.birth-PTCP.PRS</ta>
            <ta e="T12" id="Seg_6768" s="T11">time-3SG-DAT/LOC</ta>
            <ta e="T13" id="Seg_6769" s="T12">long.ago-ADJZ</ta>
            <ta e="T14" id="Seg_6770" s="T13">time-ABL</ta>
            <ta e="T15" id="Seg_6771" s="T14">birth.pole-ACC</ta>
            <ta e="T16" id="Seg_6772" s="T15">prepare-HAB-3PL</ta>
            <ta e="T17" id="Seg_6773" s="T16">individually</ta>
            <ta e="T18" id="Seg_6774" s="T17">pole.[NOM]</ta>
            <ta e="T19" id="Seg_6775" s="T18">tent-ACC</ta>
            <ta e="T20" id="Seg_6776" s="T19">build-CVB.SEQ-3PL</ta>
            <ta e="T21" id="Seg_6777" s="T20">prepare-HAB-3PL</ta>
            <ta e="T22" id="Seg_6778" s="T21">child.[NOM]</ta>
            <ta e="T23" id="Seg_6779" s="T22">give.birth-PTCP.PRS</ta>
            <ta e="T24" id="Seg_6780" s="T23">tent-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_6781" s="T24">be.born-PTCP.PST</ta>
            <ta e="T26" id="Seg_6782" s="T25">tent-3SG.[NOM]</ta>
            <ta e="T27" id="Seg_6783" s="T26">say-HAB-3PL</ta>
            <ta e="T28" id="Seg_6784" s="T27">then</ta>
            <ta e="T29" id="Seg_6785" s="T28">there</ta>
            <ta e="T30" id="Seg_6786" s="T29">though</ta>
            <ta e="T31" id="Seg_6787" s="T30">that</ta>
            <ta e="T32" id="Seg_6788" s="T31">birth.pole-ACC</ta>
            <ta e="T33" id="Seg_6789" s="T32">make-HAB-3PL</ta>
            <ta e="T34" id="Seg_6790" s="T33">family-3PL.[NOM]</ta>
            <ta e="T35" id="Seg_6791" s="T34">stand-CAUS-PRS-3PL</ta>
            <ta e="T36" id="Seg_6792" s="T35">grandfather-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_6793" s="T36">Q</ta>
            <ta e="T38" id="Seg_6794" s="T37">father-3SG.[NOM]</ta>
            <ta e="T39" id="Seg_6795" s="T38">Q</ta>
            <ta e="T40" id="Seg_6796" s="T39">mother-3SG.[NOM]</ta>
            <ta e="T41" id="Seg_6797" s="T40">Q</ta>
            <ta e="T42" id="Seg_6798" s="T41">help-CVB.SEQ-3PL</ta>
            <ta e="T43" id="Seg_6799" s="T42">tie-PRS-3PL</ta>
            <ta e="T44" id="Seg_6800" s="T43">birth.pole-ACC</ta>
            <ta e="T45" id="Seg_6801" s="T44">pole-DAT/LOC</ta>
            <ta e="T46" id="Seg_6802" s="T45">press-CVB.SIM</ta>
            <ta e="T47" id="Seg_6803" s="T46">across</ta>
            <ta e="T48" id="Seg_6804" s="T47">wood-ACC</ta>
            <ta e="T49" id="Seg_6805" s="T48">then</ta>
            <ta e="T50" id="Seg_6806" s="T49">eh</ta>
            <ta e="T51" id="Seg_6807" s="T50">birth.pole-3SG-GEN</ta>
            <ta e="T52" id="Seg_6808" s="T51">end-3SG-DAT/LOC</ta>
            <ta e="T53" id="Seg_6809" s="T52">MOD</ta>
            <ta e="T54" id="Seg_6810" s="T53">stand.upright-ADVZ</ta>
            <ta e="T55" id="Seg_6811" s="T54">push-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T56" id="Seg_6812" s="T55">stand-PTCP.PRS</ta>
            <ta e="T57" id="Seg_6813" s="T56">head-3SG.[NOM]</ta>
            <ta e="T58" id="Seg_6814" s="T57">eh</ta>
            <ta e="T59" id="Seg_6815" s="T58">wood.[NOM]</ta>
            <ta e="T60" id="Seg_6816" s="T59">be-HAB.[3SG]</ta>
            <ta e="T61" id="Seg_6817" s="T60">pole-DIM-3SG.[NOM]</ta>
            <ta e="T62" id="Seg_6818" s="T61">then</ta>
            <ta e="T63" id="Seg_6819" s="T62">though</ta>
            <ta e="T64" id="Seg_6820" s="T63">that</ta>
            <ta e="T65" id="Seg_6821" s="T64">birth.pole-2SG.[NOM]</ta>
            <ta e="T66" id="Seg_6822" s="T65">EMPH</ta>
            <ta e="T67" id="Seg_6823" s="T66">breast-3SG-GEN</ta>
            <ta e="T68" id="Seg_6824" s="T67">curve-3SG-INSTR</ta>
            <ta e="T69" id="Seg_6825" s="T68">be-HAB.[3SG]</ta>
            <ta e="T70" id="Seg_6826" s="T69">woman.[NOM]</ta>
            <ta e="T71" id="Seg_6827" s="T70">then</ta>
            <ta e="T72" id="Seg_6828" s="T71">hang.up-PASS/REFL-CVB.SEQ</ta>
            <ta e="T73" id="Seg_6829" s="T72">sit-CVB.SEQ</ta>
            <ta e="T74" id="Seg_6830" s="T73">give.birth-HAB.[3SG]</ta>
            <ta e="T75" id="Seg_6831" s="T74">that</ta>
            <ta e="T76" id="Seg_6832" s="T75">give.birth-PTCP.PRS-3SG-GEN</ta>
            <ta e="T77" id="Seg_6833" s="T76">lower.part-3SG-DAT/LOC</ta>
            <ta e="T78" id="Seg_6834" s="T77">MOD</ta>
            <ta e="T79" id="Seg_6835" s="T78">eh</ta>
            <ta e="T80" id="Seg_6836" s="T79">sit-PTCP.PRS</ta>
            <ta e="T81" id="Seg_6837" s="T80">place-3SG-DAT/LOC</ta>
            <ta e="T82" id="Seg_6838" s="T81">pieces.of.fur.[NOM]</ta>
            <ta e="T83" id="Seg_6839" s="T82">be-HAB.[3SG]</ta>
            <ta e="T84" id="Seg_6840" s="T83">well</ta>
            <ta e="T85" id="Seg_6841" s="T84">that</ta>
            <ta e="T86" id="Seg_6842" s="T85">pieces.of.fur-DAT/LOC</ta>
            <ta e="T87" id="Seg_6843" s="T86">MOD</ta>
            <ta e="T88" id="Seg_6844" s="T87">grass.[NOM]</ta>
            <ta e="T89" id="Seg_6845" s="T88">be-HAB.[3SG]</ta>
            <ta e="T90" id="Seg_6846" s="T89">grass-ACC</ta>
            <ta e="T91" id="Seg_6847" s="T90">lay-HAB-3PL</ta>
            <ta e="T92" id="Seg_6848" s="T91">relief-3SG-DAT/LOC</ta>
            <ta e="T93" id="Seg_6849" s="T92">say-CVB.SEQ-3PL</ta>
            <ta e="T94" id="Seg_6850" s="T93">that-DAT/LOC</ta>
            <ta e="T95" id="Seg_6851" s="T94">MOD</ta>
            <ta e="T96" id="Seg_6852" s="T95">child.[NOM]</ta>
            <ta e="T97" id="Seg_6853" s="T96">fall-TEMP-3SG</ta>
            <ta e="T98" id="Seg_6854" s="T97">grass-3SG-DAT/LOC</ta>
            <ta e="T99" id="Seg_6855" s="T98">fall-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_6856" s="T99">say-HAB-3PL</ta>
            <ta e="T101" id="Seg_6857" s="T100">that.[NOM]</ta>
            <ta e="T102" id="Seg_6858" s="T101">because.of</ta>
            <ta e="T103" id="Seg_6859" s="T102">grass-PROPR</ta>
            <ta e="T104" id="Seg_6860" s="T103">grass-PROPR-3SG-ACC</ta>
            <ta e="T105" id="Seg_6861" s="T104">because.of</ta>
            <ta e="T106" id="Seg_6862" s="T105">grass-DAT/LOC</ta>
            <ta e="T107" id="Seg_6863" s="T106">fall-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_6864" s="T107">say-HAB-3PL</ta>
            <ta e="T109" id="Seg_6865" s="T108">eh</ta>
            <ta e="T110" id="Seg_6866" s="T109">woman.[NOM]</ta>
            <ta e="T111" id="Seg_6867" s="T110">give.birth-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_6868" s="T112">long.ago-ADJZ</ta>
            <ta e="T114" id="Seg_6869" s="T113">time-ABL</ta>
            <ta e="T115" id="Seg_6870" s="T114">distant-DAT/LOC</ta>
            <ta e="T116" id="Seg_6871" s="T115">there.is</ta>
            <ta e="T117" id="Seg_6872" s="T116">be-TEMP-3SG</ta>
            <ta e="T118" id="Seg_6873" s="T117">that</ta>
            <ta e="T119" id="Seg_6874" s="T118">eh</ta>
            <ta e="T120" id="Seg_6875" s="T119">midwife.[NOM]</ta>
            <ta e="T121" id="Seg_6876" s="T120">old.woman-3PL.[NOM]</ta>
            <ta e="T122" id="Seg_6877" s="T121">distant.[NOM]</ta>
            <ta e="T123" id="Seg_6878" s="T122">there.is</ta>
            <ta e="T124" id="Seg_6879" s="T123">be</ta>
            <ta e="T125" id="Seg_6880" s="T124">child.[NOM]</ta>
            <ta e="T126" id="Seg_6881" s="T125">give.birth-NMNZ.[NOM]</ta>
            <ta e="T127" id="Seg_6882" s="T126">fall-AG-3PL.[NOM]</ta>
            <ta e="T128" id="Seg_6883" s="T127">woman-3PL.[NOM]</ta>
            <ta e="T129" id="Seg_6884" s="T128">distant.[NOM]</ta>
            <ta e="T130" id="Seg_6885" s="T129">there.is</ta>
            <ta e="T131" id="Seg_6886" s="T130">be-TEMP-3SG</ta>
            <ta e="T132" id="Seg_6887" s="T131">sledge-PROPR.[NOM]</ta>
            <ta e="T133" id="Seg_6888" s="T132">go-CVB.SEQ-3PL</ta>
            <ta e="T134" id="Seg_6889" s="T133">get-HAB-3PL</ta>
            <ta e="T135" id="Seg_6890" s="T134">beg-CVB.SEQ-3PL</ta>
            <ta e="T136" id="Seg_6891" s="T135">well</ta>
            <ta e="T137" id="Seg_6892" s="T136">child-1PL.[NOM]</ta>
            <ta e="T138" id="Seg_6893" s="T137">then</ta>
            <ta e="T139" id="Seg_6894" s="T138">then</ta>
            <ta e="T140" id="Seg_6895" s="T139">that</ta>
            <ta e="T141" id="Seg_6896" s="T140">daughter-1PL.[NOM]</ta>
            <ta e="T143" id="Seg_6897" s="T142">give.birth-FUT-3SG</ta>
            <ta e="T144" id="Seg_6898" s="T143">say-CVB.SEQ-3PL</ta>
            <ta e="T145" id="Seg_6899" s="T144">woman-1PL.[NOM]</ta>
            <ta e="T146" id="Seg_6900" s="T145">give.birth-FUT-3SG</ta>
            <ta e="T147" id="Seg_6901" s="T146">say-CVB.SEQ-3PL</ta>
            <ta e="T148" id="Seg_6902" s="T147">get-HAB-3PL</ta>
            <ta e="T149" id="Seg_6903" s="T148">eh</ta>
            <ta e="T150" id="Seg_6904" s="T149">close.[NOM]</ta>
            <ta e="T151" id="Seg_6905" s="T150">there.is</ta>
            <ta e="T152" id="Seg_6906" s="T151">be-TEMP-3SG</ta>
            <ta e="T153" id="Seg_6907" s="T152">MOD</ta>
            <ta e="T154" id="Seg_6908" s="T153">call-HAB-3PL</ta>
            <ta e="T155" id="Seg_6909" s="T154">that</ta>
            <ta e="T156" id="Seg_6910" s="T155">midwife.[NOM]</ta>
            <ta e="T157" id="Seg_6911" s="T156">old.woman-ACC</ta>
            <ta e="T158" id="Seg_6912" s="T157">so</ta>
            <ta e="T159" id="Seg_6913" s="T158">make-CVB.SEQ</ta>
            <ta e="T160" id="Seg_6914" s="T159">that</ta>
            <ta e="T161" id="Seg_6915" s="T160">midwife.[NOM]</ta>
            <ta e="T162" id="Seg_6916" s="T161">hand.[NOM]</ta>
            <ta e="T163" id="Seg_6917" s="T162">towel-3SG.[NOM]</ta>
            <ta e="T164" id="Seg_6918" s="T163">what-3SG.[NOM]</ta>
            <ta e="T165" id="Seg_6919" s="T164">every-3SG.[NOM]</ta>
            <ta e="T166" id="Seg_6920" s="T165">clean.[NOM]</ta>
            <ta e="T167" id="Seg_6921" s="T166">become-PTCP.FUT-3SG-ACC</ta>
            <ta e="T168" id="Seg_6922" s="T167">right</ta>
            <ta e="T169" id="Seg_6923" s="T168">be-HAB.[3SG]</ta>
            <ta e="T170" id="Seg_6924" s="T169">every-INTNS-3SG.[NOM]</ta>
            <ta e="T171" id="Seg_6925" s="T170">new.[NOM]</ta>
            <ta e="T172" id="Seg_6926" s="T171">grab-EP-PASS/REFL-EP-NEG.PTCP.PST</ta>
            <ta e="T173" id="Seg_6927" s="T172">thing.[NOM]</ta>
            <ta e="T174" id="Seg_6928" s="T173">be-PRS-NEC.[3SG]</ta>
            <ta e="T175" id="Seg_6929" s="T174">towel-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_6930" s="T175">EMPH</ta>
            <ta e="T177" id="Seg_6931" s="T176">hand.[NOM]</ta>
            <ta e="T178" id="Seg_6932" s="T177">towel-3SG.[NOM]</ta>
            <ta e="T179" id="Seg_6933" s="T178">child-ACC</ta>
            <ta e="T180" id="Seg_6934" s="T179">give.birth-MULT-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_6935" s="T180">be.in.labor</ta>
            <ta e="T182" id="Seg_6936" s="T181">woman.[NOM]</ta>
            <ta e="T183" id="Seg_6937" s="T182">be.in.labor-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T184" id="Seg_6938" s="T183">then</ta>
            <ta e="T185" id="Seg_6939" s="T184">though</ta>
            <ta e="T186" id="Seg_6940" s="T185">MOD</ta>
            <ta e="T187" id="Seg_6941" s="T186">totem-PROPR.[NOM]</ta>
            <ta e="T188" id="Seg_6942" s="T187">be-HAB.[3SG]</ta>
            <ta e="T189" id="Seg_6943" s="T188">that</ta>
            <ta e="T190" id="Seg_6944" s="T189">totem-ACC</ta>
            <ta e="T191" id="Seg_6945" s="T190">though</ta>
            <ta e="T192" id="Seg_6946" s="T191">at.first</ta>
            <ta e="T193" id="Seg_6947" s="T192">take-CVB.SIM</ta>
            <ta e="T194" id="Seg_6948" s="T193">come-CVB.SIM</ta>
            <ta e="T195" id="Seg_6949" s="T194">come-PRS-3PL</ta>
            <ta e="T196" id="Seg_6950" s="T195">woman-3PL.[NOM]</ta>
            <ta e="T197" id="Seg_6951" s="T196">give.birth-CVB.PURP</ta>
            <ta e="T198" id="Seg_6952" s="T197">make-TEMP-3SG</ta>
            <ta e="T199" id="Seg_6953" s="T198">that</ta>
            <ta e="T200" id="Seg_6954" s="T199">individually</ta>
            <ta e="T201" id="Seg_6955" s="T200">that</ta>
            <ta e="T202" id="Seg_6956" s="T201">individually</ta>
            <ta e="T203" id="Seg_6957" s="T202">tent-DAT/LOC</ta>
            <ta e="T204" id="Seg_6958" s="T203">who-ACC</ta>
            <ta e="T205" id="Seg_6959" s="T204">NEG</ta>
            <ta e="T206" id="Seg_6960" s="T205">go.in-CAUS-NEG-3PL</ta>
            <ta e="T207" id="Seg_6961" s="T206">MOD</ta>
            <ta e="T208" id="Seg_6962" s="T207">who.[NOM]</ta>
            <ta e="T209" id="Seg_6963" s="T208">individually</ta>
            <ta e="T210" id="Seg_6964" s="T209">tent.[NOM]</ta>
            <ta e="T211" id="Seg_6965" s="T210">build-NEG-3PL</ta>
            <ta e="T212" id="Seg_6966" s="T211">house-DAT/LOC</ta>
            <ta e="T213" id="Seg_6967" s="T212">give.birth-EP-CAUS-HAB-3PL</ta>
            <ta e="T214" id="Seg_6968" s="T213">house-DAT/LOC</ta>
            <ta e="T215" id="Seg_6969" s="T214">live-PTCP.PRS</ta>
            <ta e="T216" id="Seg_6970" s="T215">tent-3PL-DAT/LOC</ta>
            <ta e="T217" id="Seg_6971" s="T216">that</ta>
            <ta e="T218" id="Seg_6972" s="T217">make-CVB.SEQ</ta>
            <ta e="T219" id="Seg_6973" s="T218">after</ta>
            <ta e="T220" id="Seg_6974" s="T219">live-PTCP.PRS</ta>
            <ta e="T221" id="Seg_6975" s="T220">tent-3PL-ABL</ta>
            <ta e="T222" id="Seg_6976" s="T221">individually</ta>
            <ta e="T223" id="Seg_6977" s="T222">make-HAB-3PL</ta>
            <ta e="T224" id="Seg_6978" s="T223">birth.pole-ACC</ta>
            <ta e="T225" id="Seg_6979" s="T224">stand-CAUS-HAB-3PL</ta>
            <ta e="T226" id="Seg_6980" s="T225">well</ta>
            <ta e="T227" id="Seg_6981" s="T226">that</ta>
            <ta e="T228" id="Seg_6982" s="T227">make-CVB.SEQ</ta>
            <ta e="T229" id="Seg_6983" s="T228">human.being-ACC</ta>
            <ta e="T230" id="Seg_6984" s="T229">every-DIM-3SG-ACC</ta>
            <ta e="T231" id="Seg_6985" s="T230">take.out-HAB-3PL</ta>
            <ta e="T232" id="Seg_6986" s="T231">who.[NOM]</ta>
            <ta e="T233" id="Seg_6987" s="T232">NEG</ta>
            <ta e="T234" id="Seg_6988" s="T233">NEG</ta>
            <ta e="T235" id="Seg_6989" s="T234">be-FUT-NEC.[3SG]</ta>
            <ta e="T236" id="Seg_6990" s="T235">lonely</ta>
            <ta e="T237" id="Seg_6991" s="T236">midwife-ACC</ta>
            <ta e="T238" id="Seg_6992" s="T237">with</ta>
            <ta e="T239" id="Seg_6993" s="T238">give.birth-PTCP.PRS</ta>
            <ta e="T240" id="Seg_6994" s="T239">woman.[NOM]</ta>
            <ta e="T241" id="Seg_6995" s="T240">just</ta>
            <ta e="T242" id="Seg_6996" s="T241">be-PTCP.FUT-NEC-3PL</ta>
            <ta e="T243" id="Seg_6997" s="T242">there</ta>
            <ta e="T244" id="Seg_6998" s="T243">then</ta>
            <ta e="T245" id="Seg_6999" s="T244">though</ta>
            <ta e="T246" id="Seg_7000" s="T245">MOD</ta>
            <ta e="T247" id="Seg_7001" s="T246">this</ta>
            <ta e="T248" id="Seg_7002" s="T247">midwife-2SG.[NOM]</ta>
            <ta e="T249" id="Seg_7003" s="T248">come-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_7004" s="T249">pole.[NOM]</ta>
            <ta e="T251" id="Seg_7005" s="T250">tent-DAT/LOC</ta>
            <ta e="T252" id="Seg_7006" s="T251">give.birth-PTCP.PRS</ta>
            <ta e="T253" id="Seg_7007" s="T252">tent-DAT/LOC</ta>
            <ta e="T254" id="Seg_7008" s="T253">tent-DAT/LOC</ta>
            <ta e="T255" id="Seg_7009" s="T254">individually</ta>
            <ta e="T256" id="Seg_7010" s="T255">build-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T257" id="Seg_7011" s="T256">be-TEMP-3SG</ta>
            <ta e="T258" id="Seg_7012" s="T257">MOD</ta>
            <ta e="T259" id="Seg_7013" s="T258">house-DAT/LOC</ta>
            <ta e="T260" id="Seg_7014" s="T259">this.[NOM]</ta>
            <ta e="T261" id="Seg_7015" s="T260">be.made-EP-PTCP.PST</ta>
            <ta e="T262" id="Seg_7016" s="T261">be-TEMP-3SG</ta>
            <ta e="T263" id="Seg_7017" s="T262">that</ta>
            <ta e="T264" id="Seg_7018" s="T263">come-CVB.SEQ</ta>
            <ta e="T265" id="Seg_7019" s="T264">well</ta>
            <ta e="T266" id="Seg_7020" s="T265">get-CVB.SEQ</ta>
            <ta e="T267" id="Seg_7021" s="T266">come-CVB.SEQ-3PL</ta>
            <ta e="T268" id="Seg_7022" s="T267">that</ta>
            <ta e="T269" id="Seg_7023" s="T268">midwife.[NOM]</ta>
            <ta e="T270" id="Seg_7024" s="T269">old.woman-3SG.[NOM]</ta>
            <ta e="T271" id="Seg_7025" s="T270">go.in-TEMP-3SG</ta>
            <ta e="T272" id="Seg_7026" s="T271">that-ACC</ta>
            <ta e="T273" id="Seg_7027" s="T272">every-DIM-ACC</ta>
            <ta e="T274" id="Seg_7028" s="T273">amber-VBZ-CVB.SEQ</ta>
            <ta e="T275" id="Seg_7029" s="T274">amber-EP-INSTR</ta>
            <ta e="T276" id="Seg_7030" s="T275">splinter-EP-INSTR</ta>
            <ta e="T277" id="Seg_7031" s="T276">light-CVB.SEQ</ta>
            <ta e="T278" id="Seg_7032" s="T277">every-DIM-3SG-ACC</ta>
            <ta e="T279" id="Seg_7033" s="T278">clean-PRS.[3SG]</ta>
            <ta e="T280" id="Seg_7034" s="T279">that</ta>
            <ta e="T281" id="Seg_7035" s="T280">woman.[NOM]</ta>
            <ta e="T282" id="Seg_7036" s="T281">sit-PTCP.PRS</ta>
            <ta e="T283" id="Seg_7037" s="T282">place-3SG-ACC</ta>
            <ta e="T284" id="Seg_7038" s="T283">sit-PTCP.PRS</ta>
            <ta e="T285" id="Seg_7039" s="T284">tent-3SG-ACC</ta>
            <ta e="T286" id="Seg_7040" s="T285">around</ta>
            <ta e="T287" id="Seg_7041" s="T286">smoke-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_7042" s="T287">INTJ</ta>
            <ta e="T289" id="Seg_7043" s="T288">INTJ</ta>
            <ta e="T290" id="Seg_7044" s="T289">INTJ</ta>
            <ta e="T291" id="Seg_7045" s="T290">INTJ</ta>
            <ta e="T292" id="Seg_7046" s="T291">dirty-EP-2SG.[NOM]</ta>
            <ta e="T293" id="Seg_7047" s="T292">cloud.[NOM]</ta>
            <ta e="T294" id="Seg_7048" s="T293">bad.thought-EP-2SG.[NOM]</ta>
            <ta e="T295" id="Seg_7049" s="T294">moon-DAT/LOC</ta>
            <ta e="T296" id="Seg_7050" s="T295">INTJ</ta>
            <ta e="T297" id="Seg_7051" s="T296">INTJ</ta>
            <ta e="T298" id="Seg_7052" s="T297">INTJ</ta>
            <ta e="T299" id="Seg_7053" s="T298">dirty-EP-2SG.[NOM]</ta>
            <ta e="T300" id="Seg_7054" s="T299">cloud.[NOM]</ta>
            <ta e="T301" id="Seg_7055" s="T300">bad.thought-EP-2SG.[NOM]</ta>
            <ta e="T302" id="Seg_7056" s="T301">moon-DAT/LOC</ta>
            <ta e="T303" id="Seg_7057" s="T302">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T304" id="Seg_7058" s="T303">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T305" id="Seg_7059" s="T304">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T306" id="Seg_7060" s="T305">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T307" id="Seg_7061" s="T306">INTJ</ta>
            <ta e="T308" id="Seg_7062" s="T307">dirty-EP-2SG.[NOM]</ta>
            <ta e="T309" id="Seg_7063" s="T308">cloud.[NOM]</ta>
            <ta e="T310" id="Seg_7064" s="T309">bad.thought-EP-2SG.[NOM]</ta>
            <ta e="T311" id="Seg_7065" s="T310">moon-DAT/LOC</ta>
            <ta e="T312" id="Seg_7066" s="T311">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T313" id="Seg_7067" s="T312">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T314" id="Seg_7068" s="T313">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T315" id="Seg_7069" s="T314">smoke-PRS.[3SG]</ta>
            <ta e="T316" id="Seg_7070" s="T315">around</ta>
            <ta e="T317" id="Seg_7071" s="T316">three-MLTP</ta>
            <ta e="T318" id="Seg_7072" s="T317">time.[NOM]</ta>
            <ta e="T319" id="Seg_7073" s="T318">then</ta>
            <ta e="T320" id="Seg_7074" s="T319">woman-ACC</ta>
            <ta e="T321" id="Seg_7075" s="T320">sit-PTCP.PRS</ta>
            <ta e="T322" id="Seg_7076" s="T321">woman-ACC</ta>
            <ta e="T323" id="Seg_7077" s="T322">sit-PTCP.PRS</ta>
            <ta e="T324" id="Seg_7078" s="T323">not.yet-3SG</ta>
            <ta e="T325" id="Seg_7079" s="T324">arm.pit-3SG-GEN</ta>
            <ta e="T326" id="Seg_7080" s="T325">lower.part-3SG-INSTR</ta>
            <ta e="T327" id="Seg_7081" s="T326">easy-ADVZ</ta>
            <ta e="T328" id="Seg_7082" s="T327">sit-PTCP.FUT-3SG-ACC</ta>
            <ta e="T329" id="Seg_7083" s="T328">say-CVB.SEQ</ta>
            <ta e="T330" id="Seg_7084" s="T329">smoke-PRS.[3SG]</ta>
            <ta e="T331" id="Seg_7085" s="T330">that.EMPH.[NOM]</ta>
            <ta e="T332" id="Seg_7086" s="T331">smoke-PTCP.PRS</ta>
            <ta e="T333" id="Seg_7087" s="T332">thing-3SG-INSTR</ta>
            <ta e="T334" id="Seg_7088" s="T333">around</ta>
            <ta e="T335" id="Seg_7089" s="T334">sun.[NOM]</ta>
            <ta e="T336" id="Seg_7090" s="T335">in.the.direction</ta>
            <ta e="T337" id="Seg_7091" s="T336">back-3SG-DAT/LOC</ta>
            <ta e="T338" id="Seg_7092" s="T337">go-NEG.[3SG]</ta>
            <ta e="T339" id="Seg_7093" s="T338">back-3SG-ACC</ta>
            <ta e="T340" id="Seg_7094" s="T339">to</ta>
            <ta e="T341" id="Seg_7095" s="T340">who-ACC</ta>
            <ta e="T342" id="Seg_7096" s="T341">NEG</ta>
            <ta e="T343" id="Seg_7097" s="T342">walk-CAUS-PTCP.HAB-3SG</ta>
            <ta e="T344" id="Seg_7098" s="T343">no-3PL</ta>
            <ta e="T345" id="Seg_7099" s="T344">pregnant.[NOM]</ta>
            <ta e="T346" id="Seg_7100" s="T345">woman-ACC</ta>
            <ta e="T347" id="Seg_7101" s="T346">at.all</ta>
            <ta e="T348" id="Seg_7102" s="T347">back-EP-VBZ-FUT-3SG</ta>
            <ta e="T349" id="Seg_7103" s="T348">think-CVB.SEQ-3PL</ta>
            <ta e="T350" id="Seg_7104" s="T349">that.[NOM]</ta>
            <ta e="T351" id="Seg_7105" s="T350">destiny.[NOM]</ta>
            <ta e="T352" id="Seg_7106" s="T351">then</ta>
            <ta e="T353" id="Seg_7107" s="T352">though</ta>
            <ta e="T354" id="Seg_7108" s="T353">MOD</ta>
            <ta e="T355" id="Seg_7109" s="T354">this</ta>
            <ta e="T357" id="Seg_7110" s="T356">three-MLTP</ta>
            <ta e="T358" id="Seg_7111" s="T357">time.[NOM]</ta>
            <ta e="T359" id="Seg_7112" s="T358">turn-CAUS-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_7113" s="T359">again</ta>
            <ta e="T361" id="Seg_7114" s="T360">INTJ</ta>
            <ta e="T362" id="Seg_7115" s="T361">INTJ</ta>
            <ta e="T363" id="Seg_7116" s="T362">INTJ</ta>
            <ta e="T364" id="Seg_7117" s="T363">happy.[NOM]</ta>
            <ta e="T365" id="Seg_7118" s="T364">live-PTCP.FUT-3SG-ACC</ta>
            <ta e="T366" id="Seg_7119" s="T365">daughter-EP-1SG.[NOM]</ta>
            <ta e="T367" id="Seg_7120" s="T366">child-1SG.[NOM]</ta>
            <ta e="T368" id="Seg_7121" s="T367">INTJ</ta>
            <ta e="T369" id="Seg_7122" s="T368">INTJ</ta>
            <ta e="T370" id="Seg_7123" s="T369">INTJ</ta>
            <ta e="T371" id="Seg_7124" s="T370">happy.[NOM]</ta>
            <ta e="T372" id="Seg_7125" s="T371">live-PTCP.FUT-3SG-ACC</ta>
            <ta e="T373" id="Seg_7126" s="T372">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T374" id="Seg_7127" s="T373">INTJ</ta>
            <ta e="T375" id="Seg_7128" s="T374">INTJ</ta>
            <ta e="T376" id="Seg_7129" s="T375">INTJ</ta>
            <ta e="T377" id="Seg_7130" s="T376">bad.thought-EP-2SG.[NOM]</ta>
            <ta e="T378" id="Seg_7131" s="T377">moon-DAT/LOC</ta>
            <ta e="T379" id="Seg_7132" s="T378">dirty-EP-2SG.[NOM]</ta>
            <ta e="T380" id="Seg_7133" s="T379">cloud-DAT/LOC</ta>
            <ta e="T381" id="Seg_7134" s="T380">bad.thought-EP-2SG.[NOM]</ta>
            <ta e="T382" id="Seg_7135" s="T381">moon-DAT/LOC</ta>
            <ta e="T383" id="Seg_7136" s="T382">dirty-EP-2SG.[NOM]</ta>
            <ta e="T384" id="Seg_7137" s="T383">cloud-DAT/LOC</ta>
            <ta e="T385" id="Seg_7138" s="T384">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T386" id="Seg_7139" s="T385">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T387" id="Seg_7140" s="T386">say-HAB-3PL</ta>
            <ta e="T388" id="Seg_7141" s="T387">three-MLTP</ta>
            <ta e="T389" id="Seg_7142" s="T388">time.[NOM]</ta>
            <ta e="T390" id="Seg_7143" s="T389">turn-CAUS-HAB-3PL</ta>
            <ta e="T391" id="Seg_7144" s="T390">that</ta>
            <ta e="T392" id="Seg_7145" s="T391">make-CVB.SEQ</ta>
            <ta e="T393" id="Seg_7146" s="T392">birth.pole-3SG-ABL</ta>
            <ta e="T394" id="Seg_7147" s="T393">hold-REFL-PRS.[3SG]</ta>
            <ta e="T395" id="Seg_7148" s="T394">that</ta>
            <ta e="T396" id="Seg_7149" s="T395">sit-PTCP.PRS</ta>
            <ta e="T397" id="Seg_7150" s="T396">woman-EP-2SG.[NOM]</ta>
            <ta e="T398" id="Seg_7151" s="T397">sit-PTCP.PRS</ta>
            <ta e="T399" id="Seg_7152" s="T398">birth.pole-3SG-ABL</ta>
            <ta e="T400" id="Seg_7153" s="T399">then</ta>
            <ta e="T401" id="Seg_7154" s="T400">though</ta>
            <ta e="T402" id="Seg_7155" s="T401">totem.[NOM]</ta>
            <ta e="T403" id="Seg_7156" s="T402">take-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_7157" s="T403">midwife.[NOM]</ta>
            <ta e="T405" id="Seg_7158" s="T404">old.woman.[NOM]</ta>
            <ta e="T406" id="Seg_7159" s="T405">that</ta>
            <ta e="T407" id="Seg_7160" s="T406">totem-3SG-ACC</ta>
            <ta e="T408" id="Seg_7161" s="T407">take-PRS.[3SG]</ta>
            <ta e="T409" id="Seg_7162" s="T408">hare.[NOM]</ta>
            <ta e="T410" id="Seg_7163" s="T409">fur-3SG.[NOM]</ta>
            <ta e="T411" id="Seg_7164" s="T410">that</ta>
            <ta e="T412" id="Seg_7165" s="T411">totem-DIM-3SG.[NOM]</ta>
            <ta e="T413" id="Seg_7166" s="T412">hare-EP-2SG.[NOM]</ta>
            <ta e="T414" id="Seg_7167" s="T413">totem.[NOM]</ta>
            <ta e="T415" id="Seg_7168" s="T414">name-PROPR.[NOM]</ta>
            <ta e="T416" id="Seg_7169" s="T415">be-HAB.[3SG]</ta>
            <ta e="T417" id="Seg_7170" s="T416">then</ta>
            <ta e="T418" id="Seg_7171" s="T417">hare-ACC</ta>
            <ta e="T419" id="Seg_7172" s="T418">grab-CVB.SEQ</ta>
            <ta e="T420" id="Seg_7173" s="T419">after</ta>
            <ta e="T421" id="Seg_7174" s="T420">hare-ACC</ta>
            <ta e="T422" id="Seg_7175" s="T421">fire-DAT/LOC</ta>
            <ta e="T423" id="Seg_7176" s="T422">eat-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T424" id="Seg_7177" s="T423">fire-ACC</ta>
            <ta e="T425" id="Seg_7178" s="T424">again</ta>
            <ta e="T426" id="Seg_7179" s="T425">eat-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T427" id="Seg_7180" s="T426">fire-grandfather-DIM-EP-1SG.[NOM]</ta>
            <ta e="T428" id="Seg_7181" s="T427">totem-1SG-ACC</ta>
            <ta e="T429" id="Seg_7182" s="T428">eat-EP-CAUS-PRS-1SG</ta>
            <ta e="T430" id="Seg_7183" s="T429">child-1SG-ACC</ta>
            <ta e="T431" id="Seg_7184" s="T430">happy.[NOM]</ta>
            <ta e="T432" id="Seg_7185" s="T431">live-CAUS-PTCP.FUT-3SG-ACC</ta>
            <ta e="T433" id="Seg_7186" s="T432">happy.[NOM]</ta>
            <ta e="T434" id="Seg_7187" s="T433">be.in.labor-PTCP.FUT-3SG-ACC</ta>
            <ta e="T435" id="Seg_7188" s="T434">say-CVB.SEQ</ta>
            <ta e="T436" id="Seg_7189" s="T435">then</ta>
            <ta e="T437" id="Seg_7190" s="T436">though</ta>
            <ta e="T438" id="Seg_7191" s="T437">fire.[NOM]</ta>
            <ta e="T439" id="Seg_7192" s="T438">grandfather-DIM-1SG-ACC</ta>
            <ta e="T440" id="Seg_7193" s="T439">eat-EP-CAUS-PRS-1SG</ta>
            <ta e="T441" id="Seg_7194" s="T440">fire.[NOM]</ta>
            <ta e="T442" id="Seg_7195" s="T441">grandfather-DIM-EP-1SG.[NOM]</ta>
            <ta e="T443" id="Seg_7196" s="T442">eat.[IMP.2SG]</ta>
            <ta e="T444" id="Seg_7197" s="T443">happy</ta>
            <ta e="T445" id="Seg_7198" s="T444">luck-PART</ta>
            <ta e="T446" id="Seg_7199" s="T445">guard.[IMP.2SG]</ta>
            <ta e="T447" id="Seg_7200" s="T446">what.[NOM]</ta>
            <ta e="T448" id="Seg_7201" s="T447">NEG</ta>
            <ta e="T449" id="Seg_7202" s="T448">bad-ACC</ta>
            <ta e="T450" id="Seg_7203" s="T449">come.closer-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T451" id="Seg_7204" s="T450">happy</ta>
            <ta e="T452" id="Seg_7205" s="T451">live-CAUS.[IMP.2SG]</ta>
            <ta e="T453" id="Seg_7206" s="T452">child-1SG-ACC</ta>
            <ta e="T454" id="Seg_7207" s="T453">happy</ta>
            <ta e="T455" id="Seg_7208" s="T454">free.[IMP.2SG]</ta>
            <ta e="T456" id="Seg_7209" s="T455">say-CVB.SEQ</ta>
            <ta e="T457" id="Seg_7210" s="T456">then</ta>
            <ta e="T458" id="Seg_7211" s="T457">that</ta>
            <ta e="T459" id="Seg_7212" s="T458">totem-DIM-3SG-ACC</ta>
            <ta e="T460" id="Seg_7213" s="T459">eat-EP-CAUS-CVB.SEQ</ta>
            <ta e="T461" id="Seg_7214" s="T460">after</ta>
            <ta e="T462" id="Seg_7215" s="T461">birth.pole-DAT/LOC</ta>
            <ta e="T463" id="Seg_7216" s="T462">hang.up-PRS.[3SG]</ta>
            <ta e="T464" id="Seg_7217" s="T463">that</ta>
            <ta e="T465" id="Seg_7218" s="T464">birth.pole-DAT/LOC</ta>
            <ta e="T466" id="Seg_7219" s="T465">tie-CVB.SEQ</ta>
            <ta e="T467" id="Seg_7220" s="T466">throw-PRS.[3SG]</ta>
            <ta e="T468" id="Seg_7221" s="T467">that</ta>
            <ta e="T469" id="Seg_7222" s="T468">tie-CVB.SEQ</ta>
            <ta e="T470" id="Seg_7223" s="T469">throw-CVB.SEQ</ta>
            <ta e="T471" id="Seg_7224" s="T470">after</ta>
            <ta e="T472" id="Seg_7225" s="T471">that</ta>
            <ta e="T473" id="Seg_7226" s="T472">give.birth-PTCP.PRS</ta>
            <ta e="T474" id="Seg_7227" s="T473">woman.[NOM]</ta>
            <ta e="T475" id="Seg_7228" s="T474">can-CVB.SEQ</ta>
            <ta e="T476" id="Seg_7229" s="T475">that</ta>
            <ta e="T477" id="Seg_7230" s="T476">bad-ADVZ</ta>
            <ta e="T478" id="Seg_7231" s="T477">grow-MED-PTCP.PRS</ta>
            <ta e="T479" id="Seg_7232" s="T478">be-TEMP-3SG</ta>
            <ta e="T480" id="Seg_7233" s="T479">help-PRS.[3SG]</ta>
            <ta e="T481" id="Seg_7234" s="T480">midwife.[NOM]</ta>
            <ta e="T482" id="Seg_7235" s="T481">knee-3SG-DAT/LOC</ta>
            <ta e="T483" id="Seg_7236" s="T482">seat-PRS.[3SG]</ta>
            <ta e="T484" id="Seg_7237" s="T483">tail-3SG-ACC</ta>
            <ta e="T485" id="Seg_7238" s="T484">root-3SG-INSTR</ta>
            <ta e="T486" id="Seg_7239" s="T485">child-3SG.[NOM]</ta>
            <ta e="T487" id="Seg_7240" s="T486">back-VBZ-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T488" id="Seg_7241" s="T487">say-CVB.SEQ</ta>
            <ta e="T489" id="Seg_7242" s="T488">then</ta>
            <ta e="T490" id="Seg_7243" s="T489">belly-3SG-ACC</ta>
            <ta e="T491" id="Seg_7244" s="T490">stroke-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_7245" s="T491">that</ta>
            <ta e="T493" id="Seg_7246" s="T492">belly-3SG-ACC</ta>
            <ta e="T494" id="Seg_7247" s="T493">stroke-CVB.SEQ</ta>
            <ta e="T495" id="Seg_7248" s="T494">help-CVB.SEQ</ta>
            <ta e="T496" id="Seg_7249" s="T495">that</ta>
            <ta e="T497" id="Seg_7250" s="T496">child-3SG-ACC</ta>
            <ta e="T498" id="Seg_7251" s="T497">help-CVB.SEQ</ta>
            <ta e="T499" id="Seg_7252" s="T498">give.birth-PRS.[3SG]</ta>
            <ta e="T500" id="Seg_7253" s="T499">that</ta>
            <ta e="T501" id="Seg_7254" s="T500">woman.[NOM]</ta>
            <ta e="T502" id="Seg_7255" s="T501">that</ta>
            <ta e="T503" id="Seg_7256" s="T502">give.birth-PTCP.PRS</ta>
            <ta e="T504" id="Seg_7257" s="T503">name-3SG.[NOM]</ta>
            <ta e="T505" id="Seg_7258" s="T504">child.[NOM]</ta>
            <ta e="T506" id="Seg_7259" s="T505">give.birth-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T507" id="Seg_7260" s="T506">that</ta>
            <ta e="T508" id="Seg_7261" s="T507">be.in.labor-CVB.SEQ</ta>
            <ta e="T509" id="Seg_7262" s="T508">child.[NOM]</ta>
            <ta e="T510" id="Seg_7263" s="T509">fall-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T511" id="Seg_7264" s="T510">that</ta>
            <ta e="T512" id="Seg_7265" s="T511">child-ACC</ta>
            <ta e="T513" id="Seg_7266" s="T512">navel-3SG-ACC</ta>
            <ta e="T514" id="Seg_7267" s="T513">cut-PRS-3PL</ta>
            <ta e="T515" id="Seg_7268" s="T514">cut-PRS-1PL</ta>
            <ta e="T516" id="Seg_7269" s="T515">that</ta>
            <ta e="T517" id="Seg_7270" s="T516">midwife.[NOM]</ta>
            <ta e="T518" id="Seg_7271" s="T517">old.woman.[NOM]</ta>
            <ta e="T519" id="Seg_7272" s="T518">cut-PRS.[3SG]</ta>
            <ta e="T520" id="Seg_7273" s="T519">that</ta>
            <ta e="T521" id="Seg_7274" s="T520">navel-3SG-ACC</ta>
            <ta e="T522" id="Seg_7275" s="T521">cut-CVB.SEQ</ta>
            <ta e="T523" id="Seg_7276" s="T522">after</ta>
            <ta e="T524" id="Seg_7277" s="T523">MOD</ta>
            <ta e="T525" id="Seg_7278" s="T524">tie-CVB.SEQ</ta>
            <ta e="T526" id="Seg_7279" s="T525">throw-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_7280" s="T526">sinew.[NOM]</ta>
            <ta e="T528" id="Seg_7281" s="T527">thread-EP-INSTR</ta>
            <ta e="T529" id="Seg_7282" s="T528">get.dry-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T530" id="Seg_7283" s="T529">sinew.[NOM]</ta>
            <ta e="T531" id="Seg_7284" s="T530">thread-EP-INSTR</ta>
            <ta e="T532" id="Seg_7285" s="T531">tie-PRS.[3SG]</ta>
            <ta e="T533" id="Seg_7286" s="T532">separate-CVB.SEQ</ta>
            <ta e="T534" id="Seg_7287" s="T533">fall-PTCP.FUT-3SG-ACC</ta>
            <ta e="T535" id="Seg_7288" s="T534">later</ta>
            <ta e="T536" id="Seg_7289" s="T535">through</ta>
            <ta e="T537" id="Seg_7290" s="T536">rot-CVB.SEQ</ta>
            <ta e="T538" id="Seg_7291" s="T537">five</ta>
            <ta e="T539" id="Seg_7292" s="T538">day.[NOM]</ta>
            <ta e="T540" id="Seg_7293" s="T539">Q</ta>
            <ta e="T541" id="Seg_7294" s="T540">how.much</ta>
            <ta e="T542" id="Seg_7295" s="T541">Q</ta>
            <ta e="T543" id="Seg_7296" s="T542">day.[NOM]</ta>
            <ta e="T544" id="Seg_7297" s="T543">be-CVB.SEQ</ta>
            <ta e="T545" id="Seg_7298" s="T544">after</ta>
            <ta e="T546" id="Seg_7299" s="T545">fall-HAB.[3SG]</ta>
            <ta e="T547" id="Seg_7300" s="T546">child.[NOM]</ta>
            <ta e="T548" id="Seg_7301" s="T547">navel-3SG.[NOM]</ta>
            <ta e="T549" id="Seg_7302" s="T548">then</ta>
            <ta e="T550" id="Seg_7303" s="T549">though</ta>
            <ta e="T551" id="Seg_7304" s="T550">MOD</ta>
            <ta e="T552" id="Seg_7305" s="T551">child-3SG-GEN</ta>
            <ta e="T553" id="Seg_7306" s="T552">mother-3SG.[NOM]</ta>
            <ta e="T554" id="Seg_7307" s="T553">fall-PTCP.PST-3SG-GEN</ta>
            <ta e="T555" id="Seg_7308" s="T554">that</ta>
            <ta e="T556" id="Seg_7309" s="T555">child-3SG-GEN</ta>
            <ta e="T557" id="Seg_7310" s="T556">mother-3SG-ACC</ta>
            <ta e="T558" id="Seg_7311" s="T557">wash-PRS.[3SG]</ta>
            <ta e="T559" id="Seg_7312" s="T558">child-3SG-ACC</ta>
            <ta e="T560" id="Seg_7313" s="T559">also</ta>
            <ta e="T561" id="Seg_7314" s="T560">wash-PRS.[3SG]</ta>
            <ta e="T562" id="Seg_7315" s="T561">when</ta>
            <ta e="T563" id="Seg_7316" s="T562">EMPH</ta>
            <ta e="T564" id="Seg_7317" s="T563">wash-CVB.SEQ</ta>
            <ta e="T565" id="Seg_7318" s="T564">after</ta>
            <ta e="T566" id="Seg_7319" s="T565">wrap.up-CVB.SEQ</ta>
            <ta e="T567" id="Seg_7320" s="T566">throw-PRS.[3SG]</ta>
            <ta e="T568" id="Seg_7321" s="T567">MOD</ta>
            <ta e="T569" id="Seg_7322" s="T568">then</ta>
            <ta e="T570" id="Seg_7323" s="T569">though</ta>
            <ta e="T571" id="Seg_7324" s="T570">MOD</ta>
            <ta e="T573" id="Seg_7325" s="T572">cradle-DAT/LOC</ta>
            <ta e="T574" id="Seg_7326" s="T573">still</ta>
            <ta e="T575" id="Seg_7327" s="T574">lay-NEG-3PL</ta>
            <ta e="T576" id="Seg_7328" s="T575">MOD</ta>
            <ta e="T577" id="Seg_7329" s="T576">wrap.up-CVB.SEQ</ta>
            <ta e="T578" id="Seg_7330" s="T577">throw-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_7331" s="T578">mum-3SG-GEN</ta>
            <ta e="T580" id="Seg_7332" s="T579">place.beneath-DAT/LOC</ta>
            <ta e="T581" id="Seg_7333" s="T580">lay-PRS.[3SG]</ta>
            <ta e="T582" id="Seg_7334" s="T581">then</ta>
            <ta e="T583" id="Seg_7335" s="T582">MOD</ta>
            <ta e="T584" id="Seg_7336" s="T583">totem-3SG-ACC</ta>
            <ta e="T586" id="Seg_7337" s="T585">totem-3SG-ACC</ta>
            <ta e="T587" id="Seg_7338" s="T586">wash-CVB.SEQ</ta>
            <ta e="T588" id="Seg_7339" s="T587">after</ta>
            <ta e="T589" id="Seg_7340" s="T588">that</ta>
            <ta e="T590" id="Seg_7341" s="T589">totem-3SG-ACC</ta>
            <ta e="T591" id="Seg_7342" s="T590">little.sack-3SG-DAT/LOC</ta>
            <ta e="T592" id="Seg_7343" s="T591">stick-CVB.SEQ</ta>
            <ta e="T593" id="Seg_7344" s="T592">throw-CVB.SEQ</ta>
            <ta e="T594" id="Seg_7345" s="T593">after</ta>
            <ta e="T595" id="Seg_7346" s="T594">that</ta>
            <ta e="T596" id="Seg_7347" s="T595">bag-3SG-DAT/LOC</ta>
            <ta e="T597" id="Seg_7348" s="T596">that</ta>
            <ta e="T598" id="Seg_7349" s="T597">bag-COM</ta>
            <ta e="T599" id="Seg_7350" s="T598">that</ta>
            <ta e="T601" id="Seg_7351" s="T600">birth.pole-DAT/LOC</ta>
            <ta e="T602" id="Seg_7352" s="T601">tie-CVB.SEQ</ta>
            <ta e="T603" id="Seg_7353" s="T602">throw-PRS.[3SG]</ta>
            <ta e="T604" id="Seg_7354" s="T603">that</ta>
            <ta e="T605" id="Seg_7355" s="T604">birth.pole-DAT/LOC</ta>
            <ta e="T606" id="Seg_7356" s="T605">stand-PRS.[3SG]</ta>
            <ta e="T607" id="Seg_7357" s="T606">this</ta>
            <ta e="T608" id="Seg_7358" s="T607">child.[NOM]</ta>
            <ta e="T609" id="Seg_7359" s="T608">mother-3SG.[NOM]</ta>
            <ta e="T610" id="Seg_7360" s="T609">life-3SG-ACC</ta>
            <ta e="T611" id="Seg_7361" s="T610">as.long.as</ta>
            <ta e="T612" id="Seg_7362" s="T611">this</ta>
            <ta e="T613" id="Seg_7363" s="T612">child.[NOM]</ta>
            <ta e="T614" id="Seg_7364" s="T613">be.born-PTCP.PST</ta>
            <ta e="T615" id="Seg_7365" s="T614">tent-3SG-GEN</ta>
            <ta e="T616" id="Seg_7366" s="T615">inside-3SG-DAT/LOC</ta>
            <ta e="T617" id="Seg_7367" s="T616">child.[NOM]</ta>
            <ta e="T618" id="Seg_7368" s="T617">be.born-PTCP.PST</ta>
            <ta e="T619" id="Seg_7369" s="T618">birth.pole-3SG-DAT/LOC</ta>
            <ta e="T620" id="Seg_7370" s="T619">woman.[NOM]</ta>
            <ta e="T621" id="Seg_7371" s="T620">give.birth-PTCP.PST</ta>
            <ta e="T622" id="Seg_7372" s="T621">birth.pole-3SG-DAT/LOC</ta>
            <ta e="T623" id="Seg_7373" s="T622">then</ta>
            <ta e="T624" id="Seg_7374" s="T623">say-HAB-3PL</ta>
            <ta e="T625" id="Seg_7375" s="T624">long.ago-ADJZ</ta>
            <ta e="T626" id="Seg_7376" s="T625">time-ABL</ta>
            <ta e="T627" id="Seg_7377" s="T626">birth.pole-PROPR.[NOM]</ta>
            <ta e="T628" id="Seg_7378" s="T627">temporary.settlement-ACC</ta>
            <ta e="T629" id="Seg_7379" s="T628">pass.by-TEMP-3PL</ta>
            <ta e="T630" id="Seg_7380" s="T629">tent-3SG.[NOM]</ta>
            <ta e="T631" id="Seg_7381" s="T630">NEG</ta>
            <ta e="T632" id="Seg_7382" s="T631">NEG.EX</ta>
            <ta e="T633" id="Seg_7383" s="T632">be-TEMP-3SG</ta>
            <ta e="T634" id="Seg_7384" s="T633">birth.pole-ACC</ta>
            <ta e="T635" id="Seg_7385" s="T634">build-CVB.SIM</ta>
            <ta e="T636" id="Seg_7386" s="T635">stand-PTCP.PRS-3SG-ACC</ta>
            <ta e="T637" id="Seg_7387" s="T636">see-TEMP-3PL</ta>
            <ta e="T638" id="Seg_7388" s="T637">well</ta>
            <ta e="T639" id="Seg_7389" s="T638">this</ta>
            <ta e="T640" id="Seg_7390" s="T639">temporary.settlement-DAT/LOC</ta>
            <ta e="T641" id="Seg_7391" s="T640">woman.[NOM]</ta>
            <ta e="T642" id="Seg_7392" s="T641">be.born-PST2.[3SG]</ta>
            <ta e="T643" id="Seg_7393" s="T642">be-PST2.[3SG]</ta>
            <ta e="T644" id="Seg_7394" s="T643">birth.pole-3SG.[NOM]</ta>
            <ta e="T645" id="Seg_7395" s="T644">stand-PRS.[3SG]</ta>
            <ta e="T646" id="Seg_7396" s="T645">be.born-PTCP.PST</ta>
            <ta e="T647" id="Seg_7397" s="T646">birth.pole-3SG.[NOM]</ta>
            <ta e="T648" id="Seg_7398" s="T647">tent-PROPR.[NOM]</ta>
            <ta e="T649" id="Seg_7399" s="T648">be-TEMP-3SG</ta>
            <ta e="T650" id="Seg_7400" s="T649">such</ta>
            <ta e="T651" id="Seg_7401" s="T650">child.[NOM]</ta>
            <ta e="T652" id="Seg_7402" s="T651">be.born-PST2.[3SG]</ta>
            <ta e="T653" id="Seg_7403" s="T652">be-PST2.[3SG]</ta>
            <ta e="T654" id="Seg_7404" s="T653">that</ta>
            <ta e="T655" id="Seg_7405" s="T654">child.[NOM]</ta>
            <ta e="T656" id="Seg_7406" s="T655">be.born-PTCP.PST</ta>
            <ta e="T657" id="Seg_7407" s="T656">tent-3SG.[NOM]</ta>
            <ta e="T658" id="Seg_7408" s="T657">say-HAB-3PL</ta>
            <ta e="T659" id="Seg_7409" s="T658">then</ta>
            <ta e="T660" id="Seg_7410" s="T659">though</ta>
            <ta e="T661" id="Seg_7411" s="T660">MOD</ta>
            <ta e="T662" id="Seg_7412" s="T661">then</ta>
            <ta e="T663" id="Seg_7413" s="T662">this</ta>
            <ta e="T664" id="Seg_7414" s="T663">woman-ACC</ta>
            <ta e="T665" id="Seg_7415" s="T664">child-VBZ-PTCP.PST</ta>
            <ta e="T666" id="Seg_7416" s="T665">woman-EP-2SG.[NOM]</ta>
            <ta e="T667" id="Seg_7417" s="T666">dirty.[NOM]</ta>
            <ta e="T668" id="Seg_7418" s="T667">EMPH</ta>
            <ta e="T669" id="Seg_7419" s="T668">be.born-PTCP.PST</ta>
            <ta e="T670" id="Seg_7420" s="T669">dirty-3SG-ACC</ta>
            <ta e="T671" id="Seg_7421" s="T670">clean-CVB.SEQ</ta>
            <ta e="T672" id="Seg_7422" s="T671">again</ta>
            <ta e="T673" id="Seg_7423" s="T672">smoke-PRS-2SG</ta>
            <ta e="T674" id="Seg_7424" s="T673">face-3SG-GEN</ta>
            <ta e="T675" id="Seg_7425" s="T674">lower.part-3SG-INSTR</ta>
            <ta e="T676" id="Seg_7426" s="T675">neck-3SG-ACC</ta>
            <ta e="T677" id="Seg_7427" s="T676">around</ta>
            <ta e="T678" id="Seg_7428" s="T677">three-MLTP</ta>
            <ta e="T679" id="Seg_7429" s="T678">time.[NOM]</ta>
            <ta e="T680" id="Seg_7430" s="T679">smoke-PRS-2SG</ta>
            <ta e="T681" id="Seg_7431" s="T680">INTJ</ta>
            <ta e="T682" id="Seg_7432" s="T681">INTJ</ta>
            <ta e="T683" id="Seg_7433" s="T682">dirty-EP-2SG.[NOM]</ta>
            <ta e="T684" id="Seg_7434" s="T683">cloud-DAT/LOC</ta>
            <ta e="T685" id="Seg_7435" s="T684">bad.thought-EP-2SG.[NOM]</ta>
            <ta e="T686" id="Seg_7436" s="T685">moon-DAT/LOC</ta>
            <ta e="T687" id="Seg_7437" s="T686">INTJ</ta>
            <ta e="T688" id="Seg_7438" s="T687">INTJ</ta>
            <ta e="T689" id="Seg_7439" s="T688">dirty-EP-2SG.[NOM]</ta>
            <ta e="T690" id="Seg_7440" s="T689">cloud-DAT/LOC</ta>
            <ta e="T691" id="Seg_7441" s="T690">bad.thought-EP-2SG.[NOM]</ta>
            <ta e="T692" id="Seg_7442" s="T691">moon-DAT/LOC</ta>
            <ta e="T693" id="Seg_7443" s="T692">INTJ-INTJ</ta>
            <ta e="T694" id="Seg_7444" s="T693">dirty-3SG-ACC</ta>
            <ta e="T695" id="Seg_7445" s="T694">cloud-DAT/LOC</ta>
            <ta e="T696" id="Seg_7446" s="T695">bad.thought-EP-2SG.[NOM]</ta>
            <ta e="T697" id="Seg_7447" s="T696">moon-DAT/LOC</ta>
            <ta e="T698" id="Seg_7448" s="T697">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T699" id="Seg_7449" s="T698">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T700" id="Seg_7450" s="T699">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T701" id="Seg_7451" s="T700">say-CVB.SEQ-2SG</ta>
            <ta e="T702" id="Seg_7452" s="T701">say-CVB.SEQ-2SG</ta>
            <ta e="T703" id="Seg_7453" s="T702">smoke-PRS-2SG</ta>
            <ta e="T704" id="Seg_7454" s="T703">that</ta>
            <ta e="T705" id="Seg_7455" s="T704">that</ta>
            <ta e="T706" id="Seg_7456" s="T705">make-CVB.SEQ</ta>
            <ta e="T707" id="Seg_7457" s="T706">after</ta>
            <ta e="T708" id="Seg_7458" s="T707">child-3SG-ACC</ta>
            <ta e="T709" id="Seg_7459" s="T708">take-PRS.[3SG]</ta>
            <ta e="T710" id="Seg_7460" s="T709">midwife.[NOM]</ta>
            <ta e="T711" id="Seg_7461" s="T710">then</ta>
            <ta e="T712" id="Seg_7462" s="T711">MOD</ta>
            <ta e="T713" id="Seg_7463" s="T712">woman-ACC</ta>
            <ta e="T714" id="Seg_7464" s="T713">stand-CAUS-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_7465" s="T714">lie-PTCP.PRS</ta>
            <ta e="T716" id="Seg_7466" s="T715">bedding-3SG-ABL</ta>
            <ta e="T717" id="Seg_7467" s="T716">that</ta>
            <ta e="T718" id="Seg_7468" s="T717">be.born-PTCP.PST</ta>
            <ta e="T719" id="Seg_7469" s="T718">tent-3SG-DAT/LOC</ta>
            <ta e="T720" id="Seg_7470" s="T719">then</ta>
            <ta e="T721" id="Seg_7471" s="T720">stand.up-CVB.SEQ</ta>
            <ta e="T722" id="Seg_7472" s="T721">take.out-PRS-1PL</ta>
            <ta e="T723" id="Seg_7473" s="T722">house-1PL-DAT/LOC</ta>
            <ta e="T724" id="Seg_7474" s="T723">go-PRS-1PL</ta>
            <ta e="T725" id="Seg_7475" s="T724">lead-CVB.SEQ</ta>
            <ta e="T726" id="Seg_7476" s="T725">take.away-PRS-1PL</ta>
            <ta e="T727" id="Seg_7477" s="T726">be.born-PTCP.PST</ta>
            <ta e="T728" id="Seg_7478" s="T727">woman-ACC</ta>
            <ta e="T729" id="Seg_7479" s="T728">child-VBZ-PTCP.PST</ta>
            <ta e="T730" id="Seg_7480" s="T729">woman-ACC</ta>
            <ta e="T731" id="Seg_7481" s="T730">that</ta>
            <ta e="T732" id="Seg_7482" s="T731">make-CVB.SEQ</ta>
            <ta e="T733" id="Seg_7483" s="T732">after</ta>
            <ta e="T734" id="Seg_7484" s="T733">that</ta>
            <ta e="T735" id="Seg_7485" s="T734">lead-CVB.SEQ</ta>
            <ta e="T736" id="Seg_7486" s="T735">be-CVB.SEQ</ta>
            <ta e="T737" id="Seg_7487" s="T736">that</ta>
            <ta e="T738" id="Seg_7488" s="T737">tell-PRS.[3SG]</ta>
            <ta e="T739" id="Seg_7489" s="T738">that</ta>
            <ta e="T740" id="Seg_7490" s="T739">well</ta>
            <ta e="T741" id="Seg_7491" s="T740">child.[NOM]</ta>
            <ta e="T742" id="Seg_7492" s="T741">be.born-PST1-3SG</ta>
            <ta e="T743" id="Seg_7493" s="T742">say-CVB.SEQ</ta>
            <ta e="T744" id="Seg_7494" s="T743">family-3PL-DAT/LOC</ta>
            <ta e="T745" id="Seg_7495" s="T744">tent-PROPR</ta>
            <ta e="T746" id="Seg_7496" s="T745">tent-3PL-DAT/LOC</ta>
            <ta e="T747" id="Seg_7497" s="T746">tent-PROPR</ta>
            <ta e="T748" id="Seg_7498" s="T747">pole.[NOM]</ta>
            <ta e="T749" id="Seg_7499" s="T748">tent-DAT/LOC</ta>
            <ta e="T750" id="Seg_7500" s="T749">that</ta>
            <ta e="T751" id="Seg_7501" s="T750">tell-CVB.SEQ</ta>
            <ta e="T752" id="Seg_7502" s="T751">tell-PRS.[3SG]</ta>
            <ta e="T753" id="Seg_7503" s="T752">hey</ta>
            <ta e="T754" id="Seg_7504" s="T753">that</ta>
            <ta e="T755" id="Seg_7505" s="T754">girl.[NOM]</ta>
            <ta e="T756" id="Seg_7506" s="T755">child.[NOM]</ta>
            <ta e="T757" id="Seg_7507" s="T756">be.born-PTCP.PST</ta>
            <ta e="T758" id="Seg_7508" s="T757">be-TEMP-3SG</ta>
            <ta e="T759" id="Seg_7509" s="T758">girl.[NOM]</ta>
            <ta e="T760" id="Seg_7510" s="T759">child.[NOM]</ta>
            <ta e="T761" id="Seg_7511" s="T760">say-PRS-3PL</ta>
            <ta e="T762" id="Seg_7512" s="T761">boy.[NOM]</ta>
            <ta e="T763" id="Seg_7513" s="T762">child.[NOM]</ta>
            <ta e="T764" id="Seg_7514" s="T763">be.born-PTCP.PST</ta>
            <ta e="T765" id="Seg_7515" s="T764">be-TEMP-3SG</ta>
            <ta e="T766" id="Seg_7516" s="T765">boy.[NOM]</ta>
            <ta e="T767" id="Seg_7517" s="T766">child.[NOM]</ta>
            <ta e="T768" id="Seg_7518" s="T767">say-PRS-3PL</ta>
            <ta e="T769" id="Seg_7519" s="T768">then</ta>
            <ta e="T770" id="Seg_7520" s="T769">this</ta>
            <ta e="T771" id="Seg_7521" s="T770">three</ta>
            <ta e="T772" id="Seg_7522" s="T771">day.[NOM]</ta>
            <ta e="T773" id="Seg_7523" s="T772">be-CVB.SEQ</ta>
            <ta e="T774" id="Seg_7524" s="T773">after</ta>
            <ta e="T775" id="Seg_7525" s="T774">father-3SG.[NOM]</ta>
            <ta e="T776" id="Seg_7526" s="T775">cradle-ACC</ta>
            <ta e="T777" id="Seg_7527" s="T776">make-PRS.[3SG]</ta>
            <ta e="T778" id="Seg_7528" s="T777">child-3SG-DAT/LOC</ta>
            <ta e="T779" id="Seg_7529" s="T778">grandfather-3SG.[NOM]</ta>
            <ta e="T780" id="Seg_7530" s="T779">help-PRS.[3SG]</ta>
            <ta e="T781" id="Seg_7531" s="T780">then</ta>
            <ta e="T782" id="Seg_7532" s="T781">EMPH</ta>
            <ta e="T783" id="Seg_7533" s="T782">this</ta>
            <ta e="T784" id="Seg_7534" s="T783">cradle-ACC</ta>
            <ta e="T785" id="Seg_7535" s="T784">MOD</ta>
            <ta e="T786" id="Seg_7536" s="T785">strike-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T787" id="Seg_7537" s="T786">strong</ta>
            <ta e="T788" id="Seg_7538" s="T787">plank-PL-EP-INSTR</ta>
            <ta e="T789" id="Seg_7539" s="T788">string-INSTR</ta>
            <ta e="T790" id="Seg_7540" s="T789">thin.layer.of.leather.[NOM]</ta>
            <ta e="T791" id="Seg_7541" s="T790">string-INSTR</ta>
            <ta e="T792" id="Seg_7542" s="T791">fasten-CVB.SEQ</ta>
            <ta e="T793" id="Seg_7543" s="T792">make-HAB-3PL</ta>
            <ta e="T794" id="Seg_7544" s="T793">spruce-ACC</ta>
            <ta e="T795" id="Seg_7545" s="T794">this</ta>
            <ta e="T796" id="Seg_7546" s="T795">who.[NOM]</ta>
            <ta e="T797" id="Seg_7547" s="T796">eh</ta>
            <ta e="T798" id="Seg_7548" s="T797">plywood.[NOM]</ta>
            <ta e="T799" id="Seg_7549" s="T798">similar</ta>
            <ta e="T800" id="Seg_7550" s="T799">say-HAB-3PL</ta>
            <ta e="T801" id="Seg_7551" s="T800">that.[NOM]</ta>
            <ta e="T802" id="Seg_7552" s="T801">such-ACC</ta>
            <ta e="T803" id="Seg_7553" s="T802">MOD</ta>
            <ta e="T804" id="Seg_7554" s="T803">make-CAUS-PTCP.HAB-3SG</ta>
            <ta e="T805" id="Seg_7555" s="T804">no-3PL</ta>
            <ta e="T806" id="Seg_7556" s="T805">get.dry-PTCP.PST</ta>
            <ta e="T807" id="Seg_7557" s="T806">wood.[NOM]</ta>
            <ta e="T808" id="Seg_7558" s="T807">fur-3SG.[NOM]</ta>
            <ta e="T809" id="Seg_7559" s="T808">that.[NOM]</ta>
            <ta e="T810" id="Seg_7560" s="T809">plywood.[NOM]</ta>
            <ta e="T811" id="Seg_7561" s="T810">that.[NOM]</ta>
            <ta e="T812" id="Seg_7562" s="T811">because.of</ta>
            <ta e="T813" id="Seg_7563" s="T812">though</ta>
            <ta e="T814" id="Seg_7564" s="T813">MOD</ta>
            <ta e="T815" id="Seg_7565" s="T814">child.[NOM]</ta>
            <ta e="T816" id="Seg_7566" s="T815">get.dry-FUT.[3SG]</ta>
            <ta e="T817" id="Seg_7567" s="T816">say-HAB-3PL</ta>
            <ta e="T818" id="Seg_7568" s="T817">that.[NOM]</ta>
            <ta e="T819" id="Seg_7569" s="T818">because.of</ta>
            <ta e="T0" id="Seg_7570" s="T820">plank-INSTR</ta>
            <ta e="T822" id="Seg_7571" s="T821">and</ta>
            <ta e="T823" id="Seg_7572" s="T822">plank-2SG.[NOM]</ta>
            <ta e="T824" id="Seg_7573" s="T823">fasten-EP-PASS/REFL-TEMP-3SG</ta>
            <ta e="T825" id="Seg_7574" s="T824">Q</ta>
            <ta e="T826" id="Seg_7575" s="T825">fasten-EP-PASS/REFL-TEMP-3SG</ta>
            <ta e="T827" id="Seg_7576" s="T826">fasten-EP-PASS/REFL-TEMP-3SG</ta>
            <ta e="T828" id="Seg_7577" s="T827">string-INSTR</ta>
            <ta e="T829" id="Seg_7578" s="T828">fasten-CVB.SEQ</ta>
            <ta e="T830" id="Seg_7579" s="T829">make-TEMP-3PL</ta>
            <ta e="T831" id="Seg_7580" s="T830">that-3SG-2SG.[NOM]</ta>
            <ta e="T832" id="Seg_7581" s="T831">place.between-PROPR.[NOM]</ta>
            <ta e="T833" id="Seg_7582" s="T832">be-HAB.[3SG]</ta>
            <ta e="T834" id="Seg_7583" s="T833">plank.[NOM]</ta>
            <ta e="T835" id="Seg_7584" s="T834">plank-3SG-ABL</ta>
            <ta e="T836" id="Seg_7585" s="T835">again</ta>
            <ta e="T837" id="Seg_7586" s="T836">place.between-PROPR.[NOM]</ta>
            <ta e="T838" id="Seg_7587" s="T837">be-HAB.[3SG]</ta>
            <ta e="T839" id="Seg_7588" s="T838">then</ta>
            <ta e="T840" id="Seg_7589" s="T839">breath.[NOM]</ta>
            <ta e="T841" id="Seg_7590" s="T840">go.in-PRS.[3SG]</ta>
            <ta e="T842" id="Seg_7591" s="T841">MOD</ta>
            <ta e="T843" id="Seg_7592" s="T842">that.[NOM]</ta>
            <ta e="T844" id="Seg_7593" s="T843">because.of</ta>
            <ta e="T845" id="Seg_7594" s="T844">child-2SG.[NOM]</ta>
            <ta e="T846" id="Seg_7595" s="T845">healthy.[NOM]</ta>
            <ta e="T847" id="Seg_7596" s="T846">be.born-PRS.[3SG]</ta>
            <ta e="T848" id="Seg_7597" s="T847">MOD</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_7598" s="T1">vor.langer.Zeit-ADJZ</ta>
            <ta e="T3" id="Seg_7599" s="T2">Kind.[NOM]</ta>
            <ta e="T4" id="Seg_7600" s="T3">gebären-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T5" id="Seg_7601" s="T4">Gebärstange-DAT/LOC</ta>
            <ta e="T6" id="Seg_7602" s="T5">Gebärstange-DAT/LOC</ta>
            <ta e="T7" id="Seg_7603" s="T6">Kind-3PL.[NOM]</ta>
            <ta e="T8" id="Seg_7604" s="T7">Kind</ta>
            <ta e="T9" id="Seg_7605" s="T8">schwanger.[NOM]</ta>
            <ta e="T10" id="Seg_7606" s="T9">Frau.[NOM]</ta>
            <ta e="T11" id="Seg_7607" s="T10">gebären-PTCP.PRS</ta>
            <ta e="T12" id="Seg_7608" s="T11">Zeit-3SG-DAT/LOC</ta>
            <ta e="T13" id="Seg_7609" s="T12">vor.langer.Zeit-ADJZ</ta>
            <ta e="T14" id="Seg_7610" s="T13">Zeit-ABL</ta>
            <ta e="T15" id="Seg_7611" s="T14">Gebärstange-ACC</ta>
            <ta e="T16" id="Seg_7612" s="T15">vorbereiten-HAB-3PL</ta>
            <ta e="T17" id="Seg_7613" s="T16">einzeln</ta>
            <ta e="T18" id="Seg_7614" s="T17">Stange.[NOM]</ta>
            <ta e="T19" id="Seg_7615" s="T18">Zelt-ACC</ta>
            <ta e="T20" id="Seg_7616" s="T19">bauen-CVB.SEQ-3PL</ta>
            <ta e="T21" id="Seg_7617" s="T20">vorbereiten-HAB-3PL</ta>
            <ta e="T22" id="Seg_7618" s="T21">Kind.[NOM]</ta>
            <ta e="T23" id="Seg_7619" s="T22">gebären-PTCP.PRS</ta>
            <ta e="T24" id="Seg_7620" s="T23">Zelt-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_7621" s="T24">geboren.werden-PTCP.PST</ta>
            <ta e="T26" id="Seg_7622" s="T25">Zelt-3SG.[NOM]</ta>
            <ta e="T27" id="Seg_7623" s="T26">sagen-HAB-3PL</ta>
            <ta e="T28" id="Seg_7624" s="T27">dann</ta>
            <ta e="T29" id="Seg_7625" s="T28">dort</ta>
            <ta e="T30" id="Seg_7626" s="T29">aber</ta>
            <ta e="T31" id="Seg_7627" s="T30">jenes</ta>
            <ta e="T32" id="Seg_7628" s="T31">Gebärstange-ACC</ta>
            <ta e="T33" id="Seg_7629" s="T32">machen-HAB-3PL</ta>
            <ta e="T34" id="Seg_7630" s="T33">Familie-3PL.[NOM]</ta>
            <ta e="T35" id="Seg_7631" s="T34">stehen-CAUS-PRS-3PL</ta>
            <ta e="T36" id="Seg_7632" s="T35">Großvater-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_7633" s="T36">Q</ta>
            <ta e="T38" id="Seg_7634" s="T37">Vater-3SG.[NOM]</ta>
            <ta e="T39" id="Seg_7635" s="T38">Q</ta>
            <ta e="T40" id="Seg_7636" s="T39">Mutter-3SG.[NOM]</ta>
            <ta e="T41" id="Seg_7637" s="T40">Q</ta>
            <ta e="T42" id="Seg_7638" s="T41">helfen-CVB.SEQ-3PL</ta>
            <ta e="T43" id="Seg_7639" s="T42">binden-PRS-3PL</ta>
            <ta e="T44" id="Seg_7640" s="T43">Gebärstange-ACC</ta>
            <ta e="T45" id="Seg_7641" s="T44">Stange-DAT/LOC</ta>
            <ta e="T46" id="Seg_7642" s="T45">drücken-CVB.SIM</ta>
            <ta e="T47" id="Seg_7643" s="T46">quer.über</ta>
            <ta e="T48" id="Seg_7644" s="T47">Holz-ACC</ta>
            <ta e="T49" id="Seg_7645" s="T48">dann</ta>
            <ta e="T50" id="Seg_7646" s="T49">äh</ta>
            <ta e="T51" id="Seg_7647" s="T50">Gebärstange-3SG-GEN</ta>
            <ta e="T52" id="Seg_7648" s="T51">Ende-3SG-DAT/LOC</ta>
            <ta e="T53" id="Seg_7649" s="T52">MOD</ta>
            <ta e="T54" id="Seg_7650" s="T53">aufrecht.stehen-ADVZ</ta>
            <ta e="T55" id="Seg_7651" s="T54">stoßen-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T56" id="Seg_7652" s="T55">stehen-PTCP.PRS</ta>
            <ta e="T57" id="Seg_7653" s="T56">Kopf-3SG.[NOM]</ta>
            <ta e="T58" id="Seg_7654" s="T57">äh</ta>
            <ta e="T59" id="Seg_7655" s="T58">Holz.[NOM]</ta>
            <ta e="T60" id="Seg_7656" s="T59">sein-HAB.[3SG]</ta>
            <ta e="T61" id="Seg_7657" s="T60">Stange-DIM-3SG.[NOM]</ta>
            <ta e="T62" id="Seg_7658" s="T61">dann</ta>
            <ta e="T63" id="Seg_7659" s="T62">aber</ta>
            <ta e="T64" id="Seg_7660" s="T63">jenes</ta>
            <ta e="T65" id="Seg_7661" s="T64">Gebärstange-2SG.[NOM]</ta>
            <ta e="T66" id="Seg_7662" s="T65">EMPH</ta>
            <ta e="T67" id="Seg_7663" s="T66">Brust-3SG-GEN</ta>
            <ta e="T68" id="Seg_7664" s="T67">Biegung-3SG-INSTR</ta>
            <ta e="T69" id="Seg_7665" s="T68">sein-HAB.[3SG]</ta>
            <ta e="T70" id="Seg_7666" s="T69">Frau.[NOM]</ta>
            <ta e="T71" id="Seg_7667" s="T70">dann</ta>
            <ta e="T72" id="Seg_7668" s="T71">aufhängen-PASS/REFL-CVB.SEQ</ta>
            <ta e="T73" id="Seg_7669" s="T72">sitzen-CVB.SEQ</ta>
            <ta e="T74" id="Seg_7670" s="T73">gebären-HAB.[3SG]</ta>
            <ta e="T75" id="Seg_7671" s="T74">jenes</ta>
            <ta e="T76" id="Seg_7672" s="T75">gebären-PTCP.PRS-3SG-GEN</ta>
            <ta e="T77" id="Seg_7673" s="T76">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T78" id="Seg_7674" s="T77">MOD</ta>
            <ta e="T79" id="Seg_7675" s="T78">äh</ta>
            <ta e="T80" id="Seg_7676" s="T79">sitzen-PTCP.PRS</ta>
            <ta e="T81" id="Seg_7677" s="T80">Ort-3SG-DAT/LOC</ta>
            <ta e="T82" id="Seg_7678" s="T81">Fellstücke.[NOM]</ta>
            <ta e="T83" id="Seg_7679" s="T82">sein-HAB.[3SG]</ta>
            <ta e="T84" id="Seg_7680" s="T83">doch</ta>
            <ta e="T85" id="Seg_7681" s="T84">jenes</ta>
            <ta e="T86" id="Seg_7682" s="T85">Fellstücke-DAT/LOC</ta>
            <ta e="T87" id="Seg_7683" s="T86">MOD</ta>
            <ta e="T88" id="Seg_7684" s="T87">Gras.[NOM]</ta>
            <ta e="T89" id="Seg_7685" s="T88">sein-HAB.[3SG]</ta>
            <ta e="T90" id="Seg_7686" s="T89">Gras-ACC</ta>
            <ta e="T91" id="Seg_7687" s="T90">legen-HAB-3PL</ta>
            <ta e="T92" id="Seg_7688" s="T91">Erleichterung-3SG-DAT/LOC</ta>
            <ta e="T93" id="Seg_7689" s="T92">sagen-CVB.SEQ-3PL</ta>
            <ta e="T94" id="Seg_7690" s="T93">jenes-DAT/LOC</ta>
            <ta e="T95" id="Seg_7691" s="T94">MOD</ta>
            <ta e="T96" id="Seg_7692" s="T95">Kind.[NOM]</ta>
            <ta e="T97" id="Seg_7693" s="T96">fallen-TEMP-3SG</ta>
            <ta e="T98" id="Seg_7694" s="T97">Gras-3SG-DAT/LOC</ta>
            <ta e="T99" id="Seg_7695" s="T98">fallen-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_7696" s="T99">sagen-HAB-3PL</ta>
            <ta e="T101" id="Seg_7697" s="T100">jenes.[NOM]</ta>
            <ta e="T102" id="Seg_7698" s="T101">wegen</ta>
            <ta e="T103" id="Seg_7699" s="T102">Gras-PROPR</ta>
            <ta e="T104" id="Seg_7700" s="T103">Gras-PROPR-3SG-ACC</ta>
            <ta e="T105" id="Seg_7701" s="T104">wegen</ta>
            <ta e="T106" id="Seg_7702" s="T105">Gras-DAT/LOC</ta>
            <ta e="T107" id="Seg_7703" s="T106">fallen-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_7704" s="T107">sagen-HAB-3PL</ta>
            <ta e="T109" id="Seg_7705" s="T108">äh</ta>
            <ta e="T110" id="Seg_7706" s="T109">Frau.[NOM]</ta>
            <ta e="T111" id="Seg_7707" s="T110">gebären-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_7708" s="T112">vor.langer.Zeit-ADJZ</ta>
            <ta e="T114" id="Seg_7709" s="T113">Zeit-ABL</ta>
            <ta e="T115" id="Seg_7710" s="T114">fern-DAT/LOC</ta>
            <ta e="T116" id="Seg_7711" s="T115">es.gibt</ta>
            <ta e="T117" id="Seg_7712" s="T116">sein-TEMP-3SG</ta>
            <ta e="T118" id="Seg_7713" s="T117">jenes</ta>
            <ta e="T119" id="Seg_7714" s="T118">äh</ta>
            <ta e="T120" id="Seg_7715" s="T119">Hebamme.[NOM]</ta>
            <ta e="T121" id="Seg_7716" s="T120">Alte-3PL.[NOM]</ta>
            <ta e="T122" id="Seg_7717" s="T121">fern.[NOM]</ta>
            <ta e="T123" id="Seg_7718" s="T122">es.gibt</ta>
            <ta e="T124" id="Seg_7719" s="T123">sein</ta>
            <ta e="T125" id="Seg_7720" s="T124">Kind.[NOM]</ta>
            <ta e="T126" id="Seg_7721" s="T125">gebären-NMNZ.[NOM]</ta>
            <ta e="T127" id="Seg_7722" s="T126">fallen-AG-3PL.[NOM]</ta>
            <ta e="T128" id="Seg_7723" s="T127">Frau-3PL.[NOM]</ta>
            <ta e="T129" id="Seg_7724" s="T128">fern.[NOM]</ta>
            <ta e="T130" id="Seg_7725" s="T129">es.gibt</ta>
            <ta e="T131" id="Seg_7726" s="T130">sein-TEMP-3SG</ta>
            <ta e="T132" id="Seg_7727" s="T131">Schlitten-PROPR.[NOM]</ta>
            <ta e="T133" id="Seg_7728" s="T132">gehen-CVB.SEQ-3PL</ta>
            <ta e="T134" id="Seg_7729" s="T133">holen-HAB-3PL</ta>
            <ta e="T135" id="Seg_7730" s="T134">bitten-CVB.SEQ-3PL</ta>
            <ta e="T136" id="Seg_7731" s="T135">na</ta>
            <ta e="T137" id="Seg_7732" s="T136">Kind-1PL.[NOM]</ta>
            <ta e="T138" id="Seg_7733" s="T137">dann</ta>
            <ta e="T139" id="Seg_7734" s="T138">dann</ta>
            <ta e="T140" id="Seg_7735" s="T139">jenes</ta>
            <ta e="T141" id="Seg_7736" s="T140">Tochter-1PL.[NOM]</ta>
            <ta e="T143" id="Seg_7737" s="T142">gebären-FUT-3SG</ta>
            <ta e="T144" id="Seg_7738" s="T143">sagen-CVB.SEQ-3PL</ta>
            <ta e="T145" id="Seg_7739" s="T144">Frau-1PL.[NOM]</ta>
            <ta e="T146" id="Seg_7740" s="T145">gebären-FUT-3SG</ta>
            <ta e="T147" id="Seg_7741" s="T146">sagen-CVB.SEQ-3PL</ta>
            <ta e="T148" id="Seg_7742" s="T147">holen-HAB-3PL</ta>
            <ta e="T149" id="Seg_7743" s="T148">äh</ta>
            <ta e="T150" id="Seg_7744" s="T149">nah.[NOM]</ta>
            <ta e="T151" id="Seg_7745" s="T150">es.gibt</ta>
            <ta e="T152" id="Seg_7746" s="T151">sein-TEMP-3SG</ta>
            <ta e="T153" id="Seg_7747" s="T152">MOD</ta>
            <ta e="T154" id="Seg_7748" s="T153">rufen-HAB-3PL</ta>
            <ta e="T155" id="Seg_7749" s="T154">jenes</ta>
            <ta e="T156" id="Seg_7750" s="T155">Hebamme.[NOM]</ta>
            <ta e="T157" id="Seg_7751" s="T156">Alte-ACC</ta>
            <ta e="T158" id="Seg_7752" s="T157">so</ta>
            <ta e="T159" id="Seg_7753" s="T158">machen-CVB.SEQ</ta>
            <ta e="T160" id="Seg_7754" s="T159">jenes</ta>
            <ta e="T161" id="Seg_7755" s="T160">Hebamme.[NOM]</ta>
            <ta e="T162" id="Seg_7756" s="T161">Hand.[NOM]</ta>
            <ta e="T163" id="Seg_7757" s="T162">Handtuch-3SG.[NOM]</ta>
            <ta e="T164" id="Seg_7758" s="T163">was-3SG.[NOM]</ta>
            <ta e="T165" id="Seg_7759" s="T164">jeder-3SG.[NOM]</ta>
            <ta e="T166" id="Seg_7760" s="T165">sauber.[NOM]</ta>
            <ta e="T167" id="Seg_7761" s="T166">werden-PTCP.FUT-3SG-ACC</ta>
            <ta e="T168" id="Seg_7762" s="T167">richtig</ta>
            <ta e="T169" id="Seg_7763" s="T168">sein-HAB.[3SG]</ta>
            <ta e="T170" id="Seg_7764" s="T169">jeder-INTNS-3SG.[NOM]</ta>
            <ta e="T171" id="Seg_7765" s="T170">neu.[NOM]</ta>
            <ta e="T172" id="Seg_7766" s="T171">greifen-EP-PASS/REFL-EP-NEG.PTCP.PST</ta>
            <ta e="T173" id="Seg_7767" s="T172">Ding.[NOM]</ta>
            <ta e="T174" id="Seg_7768" s="T173">sein-PRS-NEC.[3SG]</ta>
            <ta e="T175" id="Seg_7769" s="T174">Handtuch-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_7770" s="T175">EMPH</ta>
            <ta e="T177" id="Seg_7771" s="T176">Hand.[NOM]</ta>
            <ta e="T178" id="Seg_7772" s="T177">Handtuch-3SG.[NOM]</ta>
            <ta e="T179" id="Seg_7773" s="T178">Kind-ACC</ta>
            <ta e="T180" id="Seg_7774" s="T179">gebären-MULT-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_7775" s="T180">Wehen.haben</ta>
            <ta e="T182" id="Seg_7776" s="T181">Frau.[NOM]</ta>
            <ta e="T183" id="Seg_7777" s="T182">Wehen.haben-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T184" id="Seg_7778" s="T183">dann</ta>
            <ta e="T185" id="Seg_7779" s="T184">aber</ta>
            <ta e="T186" id="Seg_7780" s="T185">MOD</ta>
            <ta e="T187" id="Seg_7781" s="T186">Totem-PROPR.[NOM]</ta>
            <ta e="T188" id="Seg_7782" s="T187">sein-HAB.[3SG]</ta>
            <ta e="T189" id="Seg_7783" s="T188">jenes</ta>
            <ta e="T190" id="Seg_7784" s="T189">Totem-ACC</ta>
            <ta e="T191" id="Seg_7785" s="T190">aber</ta>
            <ta e="T192" id="Seg_7786" s="T191">zuerst</ta>
            <ta e="T193" id="Seg_7787" s="T192">nehmen-CVB.SIM</ta>
            <ta e="T194" id="Seg_7788" s="T193">kommen-CVB.SIM</ta>
            <ta e="T195" id="Seg_7789" s="T194">kommen-PRS-3PL</ta>
            <ta e="T196" id="Seg_7790" s="T195">Frau-3PL.[NOM]</ta>
            <ta e="T197" id="Seg_7791" s="T196">gebären-CVB.PURP</ta>
            <ta e="T198" id="Seg_7792" s="T197">machen-TEMP-3SG</ta>
            <ta e="T199" id="Seg_7793" s="T198">jenes</ta>
            <ta e="T200" id="Seg_7794" s="T199">einzeln</ta>
            <ta e="T201" id="Seg_7795" s="T200">jenes</ta>
            <ta e="T202" id="Seg_7796" s="T201">einzeln</ta>
            <ta e="T203" id="Seg_7797" s="T202">Zelt-DAT/LOC</ta>
            <ta e="T204" id="Seg_7798" s="T203">wer-ACC</ta>
            <ta e="T205" id="Seg_7799" s="T204">NEG</ta>
            <ta e="T206" id="Seg_7800" s="T205">hineingehen-CAUS-NEG-3PL</ta>
            <ta e="T207" id="Seg_7801" s="T206">MOD</ta>
            <ta e="T208" id="Seg_7802" s="T207">wer.[NOM]</ta>
            <ta e="T209" id="Seg_7803" s="T208">einzeln</ta>
            <ta e="T210" id="Seg_7804" s="T209">Zelt.[NOM]</ta>
            <ta e="T211" id="Seg_7805" s="T210">bauen-NEG-3PL</ta>
            <ta e="T212" id="Seg_7806" s="T211">Haus-DAT/LOC</ta>
            <ta e="T213" id="Seg_7807" s="T212">gebären-EP-CAUS-HAB-3PL</ta>
            <ta e="T214" id="Seg_7808" s="T213">Haus-DAT/LOC</ta>
            <ta e="T215" id="Seg_7809" s="T214">leben-PTCP.PRS</ta>
            <ta e="T216" id="Seg_7810" s="T215">Zelt-3PL-DAT/LOC</ta>
            <ta e="T217" id="Seg_7811" s="T216">jenes</ta>
            <ta e="T218" id="Seg_7812" s="T217">machen-CVB.SEQ</ta>
            <ta e="T219" id="Seg_7813" s="T218">nachdem</ta>
            <ta e="T220" id="Seg_7814" s="T219">leben-PTCP.PRS</ta>
            <ta e="T221" id="Seg_7815" s="T220">Zelt-3PL-ABL</ta>
            <ta e="T222" id="Seg_7816" s="T221">einzeln</ta>
            <ta e="T223" id="Seg_7817" s="T222">machen-HAB-3PL</ta>
            <ta e="T224" id="Seg_7818" s="T223">Gebärstange-ACC</ta>
            <ta e="T225" id="Seg_7819" s="T224">stehen-CAUS-HAB-3PL</ta>
            <ta e="T226" id="Seg_7820" s="T225">doch</ta>
            <ta e="T227" id="Seg_7821" s="T226">jenes</ta>
            <ta e="T228" id="Seg_7822" s="T227">machen-CVB.SEQ</ta>
            <ta e="T229" id="Seg_7823" s="T228">Mensch-ACC</ta>
            <ta e="T230" id="Seg_7824" s="T229">jeder-DIM-3SG-ACC</ta>
            <ta e="T231" id="Seg_7825" s="T230">herausnehmen-HAB-3PL</ta>
            <ta e="T232" id="Seg_7826" s="T231">wer.[NOM]</ta>
            <ta e="T233" id="Seg_7827" s="T232">NEG</ta>
            <ta e="T234" id="Seg_7828" s="T233">NEG</ta>
            <ta e="T235" id="Seg_7829" s="T234">sein-FUT-NEC.[3SG]</ta>
            <ta e="T236" id="Seg_7830" s="T235">einsam</ta>
            <ta e="T237" id="Seg_7831" s="T236">Hebamme-ACC</ta>
            <ta e="T238" id="Seg_7832" s="T237">mit</ta>
            <ta e="T239" id="Seg_7833" s="T238">gebären-PTCP.PRS</ta>
            <ta e="T240" id="Seg_7834" s="T239">Frau.[NOM]</ta>
            <ta e="T241" id="Seg_7835" s="T240">nur</ta>
            <ta e="T242" id="Seg_7836" s="T241">sein-PTCP.FUT-NEC-3PL</ta>
            <ta e="T243" id="Seg_7837" s="T242">dort</ta>
            <ta e="T244" id="Seg_7838" s="T243">dann</ta>
            <ta e="T245" id="Seg_7839" s="T244">aber</ta>
            <ta e="T246" id="Seg_7840" s="T245">MOD</ta>
            <ta e="T247" id="Seg_7841" s="T246">dieses</ta>
            <ta e="T248" id="Seg_7842" s="T247">Hebamme-2SG.[NOM]</ta>
            <ta e="T249" id="Seg_7843" s="T248">kommen-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_7844" s="T249">Stange.[NOM]</ta>
            <ta e="T251" id="Seg_7845" s="T250">Zelt-DAT/LOC</ta>
            <ta e="T252" id="Seg_7846" s="T251">gebären-PTCP.PRS</ta>
            <ta e="T253" id="Seg_7847" s="T252">Zelt-DAT/LOC</ta>
            <ta e="T254" id="Seg_7848" s="T253">Zelt-DAT/LOC</ta>
            <ta e="T255" id="Seg_7849" s="T254">einzeln</ta>
            <ta e="T256" id="Seg_7850" s="T255">bauen-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T257" id="Seg_7851" s="T256">sein-TEMP-3SG</ta>
            <ta e="T258" id="Seg_7852" s="T257">MOD</ta>
            <ta e="T259" id="Seg_7853" s="T258">Haus-DAT/LOC</ta>
            <ta e="T260" id="Seg_7854" s="T259">dieses.[NOM]</ta>
            <ta e="T261" id="Seg_7855" s="T260">gemacht.werden-EP-PTCP.PST</ta>
            <ta e="T262" id="Seg_7856" s="T261">sein-TEMP-3SG</ta>
            <ta e="T263" id="Seg_7857" s="T262">jenes</ta>
            <ta e="T264" id="Seg_7858" s="T263">kommen-CVB.SEQ</ta>
            <ta e="T265" id="Seg_7859" s="T264">nun</ta>
            <ta e="T266" id="Seg_7860" s="T265">holen-CVB.SEQ</ta>
            <ta e="T267" id="Seg_7861" s="T266">kommen-CVB.SEQ-3PL</ta>
            <ta e="T268" id="Seg_7862" s="T267">jenes</ta>
            <ta e="T269" id="Seg_7863" s="T268">Hebamme.[NOM]</ta>
            <ta e="T270" id="Seg_7864" s="T269">Alte-3SG.[NOM]</ta>
            <ta e="T271" id="Seg_7865" s="T270">hineingehen-TEMP-3SG</ta>
            <ta e="T272" id="Seg_7866" s="T271">jenes-ACC</ta>
            <ta e="T273" id="Seg_7867" s="T272">jeder-DIM-ACC</ta>
            <ta e="T274" id="Seg_7868" s="T273">Bernstein-VBZ-CVB.SEQ</ta>
            <ta e="T275" id="Seg_7869" s="T274">Bernstein-EP-INSTR</ta>
            <ta e="T276" id="Seg_7870" s="T275">Span-EP-INSTR</ta>
            <ta e="T277" id="Seg_7871" s="T276">anzünden-CVB.SEQ</ta>
            <ta e="T278" id="Seg_7872" s="T277">jeder-DIM-3SG-ACC</ta>
            <ta e="T279" id="Seg_7873" s="T278">säubern-PRS.[3SG]</ta>
            <ta e="T280" id="Seg_7874" s="T279">jenes</ta>
            <ta e="T281" id="Seg_7875" s="T280">Frau.[NOM]</ta>
            <ta e="T282" id="Seg_7876" s="T281">sitzen-PTCP.PRS</ta>
            <ta e="T283" id="Seg_7877" s="T282">Ort-3SG-ACC</ta>
            <ta e="T284" id="Seg_7878" s="T283">sitzen-PTCP.PRS</ta>
            <ta e="T285" id="Seg_7879" s="T284">Zelt-3SG-ACC</ta>
            <ta e="T286" id="Seg_7880" s="T285">um.herum</ta>
            <ta e="T287" id="Seg_7881" s="T286">räuchern-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_7882" s="T287">INTJ</ta>
            <ta e="T289" id="Seg_7883" s="T288">INTJ</ta>
            <ta e="T290" id="Seg_7884" s="T289">INTJ</ta>
            <ta e="T291" id="Seg_7885" s="T290">INTJ</ta>
            <ta e="T292" id="Seg_7886" s="T291">schmutzig-EP-2SG.[NOM]</ta>
            <ta e="T293" id="Seg_7887" s="T292">Wolke.[NOM]</ta>
            <ta e="T294" id="Seg_7888" s="T293">schlechter.Gedanke-EP-2SG.[NOM]</ta>
            <ta e="T295" id="Seg_7889" s="T294">Mond-DAT/LOC</ta>
            <ta e="T296" id="Seg_7890" s="T295">INTJ</ta>
            <ta e="T297" id="Seg_7891" s="T296">INTJ</ta>
            <ta e="T298" id="Seg_7892" s="T297">INTJ</ta>
            <ta e="T299" id="Seg_7893" s="T298">schmutzig-EP-2SG.[NOM]</ta>
            <ta e="T300" id="Seg_7894" s="T299">Wolke.[NOM]</ta>
            <ta e="T301" id="Seg_7895" s="T300">schlechter.Gedanke-EP-2SG.[NOM]</ta>
            <ta e="T302" id="Seg_7896" s="T301">Mond-DAT/LOC</ta>
            <ta e="T303" id="Seg_7897" s="T302">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T304" id="Seg_7898" s="T303">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T305" id="Seg_7899" s="T304">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T306" id="Seg_7900" s="T305">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T307" id="Seg_7901" s="T306">INTJ</ta>
            <ta e="T308" id="Seg_7902" s="T307">schmutzig-EP-2SG.[NOM]</ta>
            <ta e="T309" id="Seg_7903" s="T308">Wolke.[NOM]</ta>
            <ta e="T310" id="Seg_7904" s="T309">schlechter.Gedanke-EP-2SG.[NOM]</ta>
            <ta e="T311" id="Seg_7905" s="T310">Mond-DAT/LOC</ta>
            <ta e="T312" id="Seg_7906" s="T311">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T313" id="Seg_7907" s="T312">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T314" id="Seg_7908" s="T313">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T315" id="Seg_7909" s="T314">räuchern-PRS.[3SG]</ta>
            <ta e="T316" id="Seg_7910" s="T315">umher</ta>
            <ta e="T317" id="Seg_7911" s="T316">drei-MLTP</ta>
            <ta e="T318" id="Seg_7912" s="T317">Mal.[NOM]</ta>
            <ta e="T319" id="Seg_7913" s="T318">dann</ta>
            <ta e="T320" id="Seg_7914" s="T319">Frau-ACC</ta>
            <ta e="T321" id="Seg_7915" s="T320">sitzen-PTCP.PRS</ta>
            <ta e="T322" id="Seg_7916" s="T321">Frau-ACC</ta>
            <ta e="T323" id="Seg_7917" s="T322">sitzen-PTCP.PRS</ta>
            <ta e="T324" id="Seg_7918" s="T323">noch.nicht-3SG</ta>
            <ta e="T325" id="Seg_7919" s="T324">Achselhöhle-3SG-GEN</ta>
            <ta e="T326" id="Seg_7920" s="T325">Unterteil-3SG-INSTR</ta>
            <ta e="T327" id="Seg_7921" s="T326">leicht-ADVZ</ta>
            <ta e="T328" id="Seg_7922" s="T327">sitzen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T329" id="Seg_7923" s="T328">sagen-CVB.SEQ</ta>
            <ta e="T330" id="Seg_7924" s="T329">räuchern-PRS.[3SG]</ta>
            <ta e="T331" id="Seg_7925" s="T330">jenes.EMPH.[NOM]</ta>
            <ta e="T332" id="Seg_7926" s="T331">räuchern-PTCP.PRS</ta>
            <ta e="T333" id="Seg_7927" s="T332">Ding-3SG-INSTR</ta>
            <ta e="T334" id="Seg_7928" s="T333">um.herum</ta>
            <ta e="T335" id="Seg_7929" s="T334">Sonne.[NOM]</ta>
            <ta e="T336" id="Seg_7930" s="T335">in.die.Richtung</ta>
            <ta e="T337" id="Seg_7931" s="T336">Hinterteil-3SG-DAT/LOC</ta>
            <ta e="T338" id="Seg_7932" s="T337">gehen-NEG.[3SG]</ta>
            <ta e="T339" id="Seg_7933" s="T338">Hinterteil-3SG-ACC</ta>
            <ta e="T340" id="Seg_7934" s="T339">zu</ta>
            <ta e="T341" id="Seg_7935" s="T340">wer-ACC</ta>
            <ta e="T342" id="Seg_7936" s="T341">NEG</ta>
            <ta e="T343" id="Seg_7937" s="T342">go-CAUS-PTCP.HAB-3SG</ta>
            <ta e="T344" id="Seg_7938" s="T343">nein-3PL</ta>
            <ta e="T345" id="Seg_7939" s="T344">schwanger.[NOM]</ta>
            <ta e="T346" id="Seg_7940" s="T345">Frau-ACC</ta>
            <ta e="T347" id="Seg_7941" s="T346">ganz</ta>
            <ta e="T348" id="Seg_7942" s="T347">Hinterteil-EP-VBZ-FUT-3SG</ta>
            <ta e="T349" id="Seg_7943" s="T348">denken-CVB.SEQ-3PL</ta>
            <ta e="T350" id="Seg_7944" s="T349">jenes.[NOM]</ta>
            <ta e="T351" id="Seg_7945" s="T350">Bestimmung.[NOM]</ta>
            <ta e="T352" id="Seg_7946" s="T351">dann</ta>
            <ta e="T353" id="Seg_7947" s="T352">aber</ta>
            <ta e="T354" id="Seg_7948" s="T353">MOD</ta>
            <ta e="T355" id="Seg_7949" s="T354">dieses</ta>
            <ta e="T357" id="Seg_7950" s="T356">drei-MLTP</ta>
            <ta e="T358" id="Seg_7951" s="T357">Mal.[NOM]</ta>
            <ta e="T359" id="Seg_7952" s="T358">sich.drehen-CAUS-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_7953" s="T359">wieder</ta>
            <ta e="T361" id="Seg_7954" s="T360">INTJ</ta>
            <ta e="T362" id="Seg_7955" s="T361">INTJ</ta>
            <ta e="T363" id="Seg_7956" s="T362">INTJ</ta>
            <ta e="T364" id="Seg_7957" s="T363">glücklich.[NOM]</ta>
            <ta e="T365" id="Seg_7958" s="T364">leben-PTCP.FUT-3SG-ACC</ta>
            <ta e="T366" id="Seg_7959" s="T365">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T367" id="Seg_7960" s="T366">Kind-1SG.[NOM]</ta>
            <ta e="T368" id="Seg_7961" s="T367">INTJ</ta>
            <ta e="T369" id="Seg_7962" s="T368">INTJ</ta>
            <ta e="T370" id="Seg_7963" s="T369">INTJ</ta>
            <ta e="T371" id="Seg_7964" s="T370">glücklich.[NOM]</ta>
            <ta e="T372" id="Seg_7965" s="T371">leben-PTCP.FUT-3SG-ACC</ta>
            <ta e="T373" id="Seg_7966" s="T372">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T374" id="Seg_7967" s="T373">INTJ</ta>
            <ta e="T375" id="Seg_7968" s="T374">INTJ</ta>
            <ta e="T376" id="Seg_7969" s="T375">INTJ</ta>
            <ta e="T377" id="Seg_7970" s="T376">schlechter.Gedanke-EP-2SG.[NOM]</ta>
            <ta e="T378" id="Seg_7971" s="T377">Mond-DAT/LOC</ta>
            <ta e="T379" id="Seg_7972" s="T378">schmutzig-EP-2SG.[NOM]</ta>
            <ta e="T380" id="Seg_7973" s="T379">Wolke-DAT/LOC</ta>
            <ta e="T381" id="Seg_7974" s="T380">schlechter.Gedanke-EP-2SG.[NOM]</ta>
            <ta e="T382" id="Seg_7975" s="T381">Mond-DAT/LOC</ta>
            <ta e="T383" id="Seg_7976" s="T382">schmutzig-EP-2SG.[NOM]</ta>
            <ta e="T384" id="Seg_7977" s="T383">Wolke-DAT/LOC</ta>
            <ta e="T385" id="Seg_7978" s="T384">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T386" id="Seg_7979" s="T385">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T387" id="Seg_7980" s="T386">sagen-HAB-3PL</ta>
            <ta e="T388" id="Seg_7981" s="T387">drei-MLTP</ta>
            <ta e="T389" id="Seg_7982" s="T388">Mal.[NOM]</ta>
            <ta e="T390" id="Seg_7983" s="T389">sich.drehen-CAUS-HAB-3PL</ta>
            <ta e="T391" id="Seg_7984" s="T390">jenes</ta>
            <ta e="T392" id="Seg_7985" s="T391">machen-CVB.SEQ</ta>
            <ta e="T393" id="Seg_7986" s="T392">Gebärstange-3SG-ABL</ta>
            <ta e="T394" id="Seg_7987" s="T393">halten-REFL-PRS.[3SG]</ta>
            <ta e="T395" id="Seg_7988" s="T394">jenes</ta>
            <ta e="T396" id="Seg_7989" s="T395">sitzen-PTCP.PRS</ta>
            <ta e="T397" id="Seg_7990" s="T396">Frau-EP-2SG.[NOM]</ta>
            <ta e="T398" id="Seg_7991" s="T397">sitzen-PTCP.PRS</ta>
            <ta e="T399" id="Seg_7992" s="T398">Gebärstange-3SG-ABL</ta>
            <ta e="T400" id="Seg_7993" s="T399">dann</ta>
            <ta e="T401" id="Seg_7994" s="T400">aber</ta>
            <ta e="T402" id="Seg_7995" s="T401">Totem.[NOM]</ta>
            <ta e="T403" id="Seg_7996" s="T402">nehmen-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_7997" s="T403">Hebamme.[NOM]</ta>
            <ta e="T405" id="Seg_7998" s="T404">Alte.[NOM]</ta>
            <ta e="T406" id="Seg_7999" s="T405">jenes</ta>
            <ta e="T407" id="Seg_8000" s="T406">Totem-3SG-ACC</ta>
            <ta e="T408" id="Seg_8001" s="T407">nehmen-PRS.[3SG]</ta>
            <ta e="T409" id="Seg_8002" s="T408">Hase.[NOM]</ta>
            <ta e="T410" id="Seg_8003" s="T409">Fell-3SG.[NOM]</ta>
            <ta e="T411" id="Seg_8004" s="T410">jenes</ta>
            <ta e="T412" id="Seg_8005" s="T411">Totem-DIM-3SG.[NOM]</ta>
            <ta e="T413" id="Seg_8006" s="T412">Hase-EP-2SG.[NOM]</ta>
            <ta e="T414" id="Seg_8007" s="T413">Totem.[NOM]</ta>
            <ta e="T415" id="Seg_8008" s="T414">Name-PROPR.[NOM]</ta>
            <ta e="T416" id="Seg_8009" s="T415">sein-HAB.[3SG]</ta>
            <ta e="T417" id="Seg_8010" s="T416">dann</ta>
            <ta e="T418" id="Seg_8011" s="T417">Hase-ACC</ta>
            <ta e="T419" id="Seg_8012" s="T418">greifen-CVB.SEQ</ta>
            <ta e="T420" id="Seg_8013" s="T419">nachdem</ta>
            <ta e="T421" id="Seg_8014" s="T420">Hase-ACC</ta>
            <ta e="T422" id="Seg_8015" s="T421">Feuer-DAT/LOC</ta>
            <ta e="T423" id="Seg_8016" s="T422">essen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T424" id="Seg_8017" s="T423">Feuer-ACC</ta>
            <ta e="T425" id="Seg_8018" s="T424">wieder</ta>
            <ta e="T426" id="Seg_8019" s="T425">essen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T427" id="Seg_8020" s="T426">Feuer-Großvater-DIM-EP-1SG.[NOM]</ta>
            <ta e="T428" id="Seg_8021" s="T427">Totem-1SG-ACC</ta>
            <ta e="T429" id="Seg_8022" s="T428">essen-EP-CAUS-PRS-1SG</ta>
            <ta e="T430" id="Seg_8023" s="T429">Kind-1SG-ACC</ta>
            <ta e="T431" id="Seg_8024" s="T430">glücklich.[NOM]</ta>
            <ta e="T432" id="Seg_8025" s="T431">leben-CAUS-PTCP.FUT-3SG-ACC</ta>
            <ta e="T433" id="Seg_8026" s="T432">glücklich.[NOM]</ta>
            <ta e="T434" id="Seg_8027" s="T433">Wehen.haben-PTCP.FUT-3SG-ACC</ta>
            <ta e="T435" id="Seg_8028" s="T434">sagen-CVB.SEQ</ta>
            <ta e="T436" id="Seg_8029" s="T435">dann</ta>
            <ta e="T437" id="Seg_8030" s="T436">aber</ta>
            <ta e="T438" id="Seg_8031" s="T437">Feuer.[NOM]</ta>
            <ta e="T439" id="Seg_8032" s="T438">Großvater-DIM-1SG-ACC</ta>
            <ta e="T440" id="Seg_8033" s="T439">essen-EP-CAUS-PRS-1SG</ta>
            <ta e="T441" id="Seg_8034" s="T440">Feuer.[NOM]</ta>
            <ta e="T442" id="Seg_8035" s="T441">Großvater-DIM-EP-1SG.[NOM]</ta>
            <ta e="T443" id="Seg_8036" s="T442">essen.[IMP.2SG]</ta>
            <ta e="T444" id="Seg_8037" s="T443">glücklich</ta>
            <ta e="T445" id="Seg_8038" s="T444">Glück-PART</ta>
            <ta e="T446" id="Seg_8039" s="T445">hüten.[IMP.2SG]</ta>
            <ta e="T447" id="Seg_8040" s="T446">was.[NOM]</ta>
            <ta e="T448" id="Seg_8041" s="T447">NEG</ta>
            <ta e="T449" id="Seg_8042" s="T448">schlecht-ACC</ta>
            <ta e="T450" id="Seg_8043" s="T449">sich.nähern-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T451" id="Seg_8044" s="T450">glücklich</ta>
            <ta e="T452" id="Seg_8045" s="T451">leben-CAUS.[IMP.2SG]</ta>
            <ta e="T453" id="Seg_8046" s="T452">Kind-1SG-ACC</ta>
            <ta e="T454" id="Seg_8047" s="T453">glücklich</ta>
            <ta e="T455" id="Seg_8048" s="T454">befreien.[IMP.2SG]</ta>
            <ta e="T456" id="Seg_8049" s="T455">sagen-CVB.SEQ</ta>
            <ta e="T457" id="Seg_8050" s="T456">dann</ta>
            <ta e="T458" id="Seg_8051" s="T457">jenes</ta>
            <ta e="T459" id="Seg_8052" s="T458">Totem-DIM-3SG-ACC</ta>
            <ta e="T460" id="Seg_8053" s="T459">essen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T461" id="Seg_8054" s="T460">nachdem</ta>
            <ta e="T462" id="Seg_8055" s="T461">Gebärstange-DAT/LOC</ta>
            <ta e="T463" id="Seg_8056" s="T462">aufhängen-PRS.[3SG]</ta>
            <ta e="T464" id="Seg_8057" s="T463">jenes</ta>
            <ta e="T465" id="Seg_8058" s="T464">Gebärstange-DAT/LOC</ta>
            <ta e="T466" id="Seg_8059" s="T465">binden-CVB.SEQ</ta>
            <ta e="T467" id="Seg_8060" s="T466">werfen-PRS.[3SG]</ta>
            <ta e="T468" id="Seg_8061" s="T467">jenes</ta>
            <ta e="T469" id="Seg_8062" s="T468">binden-CVB.SEQ</ta>
            <ta e="T470" id="Seg_8063" s="T469">werfen-CVB.SEQ</ta>
            <ta e="T471" id="Seg_8064" s="T470">nachdem</ta>
            <ta e="T472" id="Seg_8065" s="T471">jenes</ta>
            <ta e="T473" id="Seg_8066" s="T472">gebären-PTCP.PRS</ta>
            <ta e="T474" id="Seg_8067" s="T473">Frau.[NOM]</ta>
            <ta e="T475" id="Seg_8068" s="T474">können-CVB.SEQ</ta>
            <ta e="T476" id="Seg_8069" s="T475">jenes</ta>
            <ta e="T477" id="Seg_8070" s="T476">schlecht-ADVZ</ta>
            <ta e="T478" id="Seg_8071" s="T477">wachsen-MED-PTCP.PRS</ta>
            <ta e="T479" id="Seg_8072" s="T478">sein-TEMP-3SG</ta>
            <ta e="T480" id="Seg_8073" s="T479">helfen-PRS.[3SG]</ta>
            <ta e="T481" id="Seg_8074" s="T480">Hebamme.[NOM]</ta>
            <ta e="T482" id="Seg_8075" s="T481">Knie-3SG-DAT/LOC</ta>
            <ta e="T483" id="Seg_8076" s="T482">setzen-PRS.[3SG]</ta>
            <ta e="T484" id="Seg_8077" s="T483">Schwanz-3SG-ACC</ta>
            <ta e="T485" id="Seg_8078" s="T484">Wurzel-3SG-INSTR</ta>
            <ta e="T486" id="Seg_8079" s="T485">Kind-3SG.[NOM]</ta>
            <ta e="T487" id="Seg_8080" s="T486">Hinterteil-VBZ-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T488" id="Seg_8081" s="T487">sagen-CVB.SEQ</ta>
            <ta e="T489" id="Seg_8082" s="T488">dann</ta>
            <ta e="T490" id="Seg_8083" s="T489">Bauch-3SG-ACC</ta>
            <ta e="T491" id="Seg_8084" s="T490">streichen.über-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_8085" s="T491">jenes</ta>
            <ta e="T493" id="Seg_8086" s="T492">Bauch-3SG-ACC</ta>
            <ta e="T494" id="Seg_8087" s="T493">streichen.über-CVB.SEQ</ta>
            <ta e="T495" id="Seg_8088" s="T494">helfen-CVB.SEQ</ta>
            <ta e="T496" id="Seg_8089" s="T495">jenes</ta>
            <ta e="T497" id="Seg_8090" s="T496">Kind-3SG-ACC</ta>
            <ta e="T498" id="Seg_8091" s="T497">helfen-CVB.SEQ</ta>
            <ta e="T499" id="Seg_8092" s="T498">gebären-PRS.[3SG]</ta>
            <ta e="T500" id="Seg_8093" s="T499">jenes</ta>
            <ta e="T501" id="Seg_8094" s="T500">Frau.[NOM]</ta>
            <ta e="T502" id="Seg_8095" s="T501">jenes</ta>
            <ta e="T503" id="Seg_8096" s="T502">gebären-PTCP.PRS</ta>
            <ta e="T504" id="Seg_8097" s="T503">Name-3SG.[NOM]</ta>
            <ta e="T505" id="Seg_8098" s="T504">Kind.[NOM]</ta>
            <ta e="T506" id="Seg_8099" s="T505">gebären-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T507" id="Seg_8100" s="T506">jenes</ta>
            <ta e="T508" id="Seg_8101" s="T507">Wehen.haben-CVB.SEQ</ta>
            <ta e="T509" id="Seg_8102" s="T508">Kind.[NOM]</ta>
            <ta e="T510" id="Seg_8103" s="T509">fallen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T511" id="Seg_8104" s="T510">jenes</ta>
            <ta e="T512" id="Seg_8105" s="T511">Kind-ACC</ta>
            <ta e="T513" id="Seg_8106" s="T512">Nabel-3SG-ACC</ta>
            <ta e="T514" id="Seg_8107" s="T513">schneiden-PRS-3PL</ta>
            <ta e="T515" id="Seg_8108" s="T514">schneiden-PRS-1PL</ta>
            <ta e="T516" id="Seg_8109" s="T515">jenes</ta>
            <ta e="T517" id="Seg_8110" s="T516">Hebamme.[NOM]</ta>
            <ta e="T518" id="Seg_8111" s="T517">Alte.[NOM]</ta>
            <ta e="T519" id="Seg_8112" s="T518">schneiden-PRS.[3SG]</ta>
            <ta e="T520" id="Seg_8113" s="T519">jenes</ta>
            <ta e="T521" id="Seg_8114" s="T520">Nabel-3SG-ACC</ta>
            <ta e="T522" id="Seg_8115" s="T521">schneiden-CVB.SEQ</ta>
            <ta e="T523" id="Seg_8116" s="T522">nachdem</ta>
            <ta e="T524" id="Seg_8117" s="T523">MOD</ta>
            <ta e="T525" id="Seg_8118" s="T524">binden-CVB.SEQ</ta>
            <ta e="T526" id="Seg_8119" s="T525">werfen-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_8120" s="T526">Sehne.[NOM]</ta>
            <ta e="T528" id="Seg_8121" s="T527">Faden-EP-INSTR</ta>
            <ta e="T529" id="Seg_8122" s="T528">trocken.werden-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T530" id="Seg_8123" s="T529">Sehne.[NOM]</ta>
            <ta e="T531" id="Seg_8124" s="T530">Faden-EP-INSTR</ta>
            <ta e="T532" id="Seg_8125" s="T531">binden-PRS.[3SG]</ta>
            <ta e="T533" id="Seg_8126" s="T532">sich.abtrennen-CVB.SEQ</ta>
            <ta e="T534" id="Seg_8127" s="T533">fallen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T535" id="Seg_8128" s="T534">später</ta>
            <ta e="T536" id="Seg_8129" s="T535">durch</ta>
            <ta e="T537" id="Seg_8130" s="T536">faulen-CVB.SEQ</ta>
            <ta e="T538" id="Seg_8131" s="T537">fünf</ta>
            <ta e="T539" id="Seg_8132" s="T538">Tag.[NOM]</ta>
            <ta e="T540" id="Seg_8133" s="T539">Q</ta>
            <ta e="T541" id="Seg_8134" s="T540">wie.viel</ta>
            <ta e="T542" id="Seg_8135" s="T541">Q</ta>
            <ta e="T543" id="Seg_8136" s="T542">Tag.[NOM]</ta>
            <ta e="T544" id="Seg_8137" s="T543">sein-CVB.SEQ</ta>
            <ta e="T545" id="Seg_8138" s="T544">nachdem</ta>
            <ta e="T546" id="Seg_8139" s="T545">fallen-HAB.[3SG]</ta>
            <ta e="T547" id="Seg_8140" s="T546">Kind.[NOM]</ta>
            <ta e="T548" id="Seg_8141" s="T547">Nabel-3SG.[NOM]</ta>
            <ta e="T549" id="Seg_8142" s="T548">dann</ta>
            <ta e="T550" id="Seg_8143" s="T549">aber</ta>
            <ta e="T551" id="Seg_8144" s="T550">MOD</ta>
            <ta e="T552" id="Seg_8145" s="T551">Kind-3SG-GEN</ta>
            <ta e="T553" id="Seg_8146" s="T552">Mutter-3SG.[NOM]</ta>
            <ta e="T554" id="Seg_8147" s="T553">fallen-PTCP.PST-3SG-GEN</ta>
            <ta e="T555" id="Seg_8148" s="T554">jenes</ta>
            <ta e="T556" id="Seg_8149" s="T555">Kind-3SG-GEN</ta>
            <ta e="T557" id="Seg_8150" s="T556">Mutter-3SG-ACC</ta>
            <ta e="T558" id="Seg_8151" s="T557">abwaschen-PRS.[3SG]</ta>
            <ta e="T559" id="Seg_8152" s="T558">Kind-3SG-ACC</ta>
            <ta e="T560" id="Seg_8153" s="T559">auch</ta>
            <ta e="T561" id="Seg_8154" s="T560">abwaschen-PRS.[3SG]</ta>
            <ta e="T562" id="Seg_8155" s="T561">wann</ta>
            <ta e="T563" id="Seg_8156" s="T562">EMPH</ta>
            <ta e="T564" id="Seg_8157" s="T563">abwaschen-CVB.SEQ</ta>
            <ta e="T565" id="Seg_8158" s="T564">nachdem</ta>
            <ta e="T566" id="Seg_8159" s="T565">einwickeln-CVB.SEQ</ta>
            <ta e="T567" id="Seg_8160" s="T566">werfen-PRS.[3SG]</ta>
            <ta e="T568" id="Seg_8161" s="T567">MOD</ta>
            <ta e="T569" id="Seg_8162" s="T568">dann</ta>
            <ta e="T570" id="Seg_8163" s="T569">aber</ta>
            <ta e="T571" id="Seg_8164" s="T570">MOD</ta>
            <ta e="T573" id="Seg_8165" s="T572">Wiege-DAT/LOC</ta>
            <ta e="T574" id="Seg_8166" s="T573">noch</ta>
            <ta e="T575" id="Seg_8167" s="T574">legen-NEG-3PL</ta>
            <ta e="T576" id="Seg_8168" s="T575">MOD</ta>
            <ta e="T577" id="Seg_8169" s="T576">einwickeln-CVB.SEQ</ta>
            <ta e="T578" id="Seg_8170" s="T577">werfen-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_8171" s="T578">Mama-3SG-GEN</ta>
            <ta e="T580" id="Seg_8172" s="T579">Platz.neben-DAT/LOC</ta>
            <ta e="T581" id="Seg_8173" s="T580">legen-PRS.[3SG]</ta>
            <ta e="T582" id="Seg_8174" s="T581">dann</ta>
            <ta e="T583" id="Seg_8175" s="T582">MOD</ta>
            <ta e="T584" id="Seg_8176" s="T583">Totem-3SG-ACC</ta>
            <ta e="T586" id="Seg_8177" s="T585">Totem-3SG-ACC</ta>
            <ta e="T587" id="Seg_8178" s="T586">abwaschen-CVB.SEQ</ta>
            <ta e="T588" id="Seg_8179" s="T587">nachdem</ta>
            <ta e="T589" id="Seg_8180" s="T588">jenes</ta>
            <ta e="T590" id="Seg_8181" s="T589">Totem-3SG-ACC</ta>
            <ta e="T591" id="Seg_8182" s="T590">Säckchen-3SG-DAT/LOC</ta>
            <ta e="T592" id="Seg_8183" s="T591">stecken-CVB.SEQ</ta>
            <ta e="T593" id="Seg_8184" s="T592">werfen-CVB.SEQ</ta>
            <ta e="T594" id="Seg_8185" s="T593">nachdem</ta>
            <ta e="T595" id="Seg_8186" s="T594">jenes</ta>
            <ta e="T596" id="Seg_8187" s="T595">Tasche-3SG-DAT/LOC</ta>
            <ta e="T597" id="Seg_8188" s="T596">jenes</ta>
            <ta e="T598" id="Seg_8189" s="T597">Tasche-COM</ta>
            <ta e="T599" id="Seg_8190" s="T598">jenes</ta>
            <ta e="T601" id="Seg_8191" s="T600">Gebärstange-DAT/LOC</ta>
            <ta e="T602" id="Seg_8192" s="T601">binden-CVB.SEQ</ta>
            <ta e="T603" id="Seg_8193" s="T602">werfen-PRS.[3SG]</ta>
            <ta e="T604" id="Seg_8194" s="T603">jenes</ta>
            <ta e="T605" id="Seg_8195" s="T604">Gebärstange-DAT/LOC</ta>
            <ta e="T606" id="Seg_8196" s="T605">stehen-PRS.[3SG]</ta>
            <ta e="T607" id="Seg_8197" s="T606">dieses</ta>
            <ta e="T608" id="Seg_8198" s="T607">Kind.[NOM]</ta>
            <ta e="T609" id="Seg_8199" s="T608">Mutter-3SG.[NOM]</ta>
            <ta e="T610" id="Seg_8200" s="T609">Leben-3SG-ACC</ta>
            <ta e="T611" id="Seg_8201" s="T610">solange</ta>
            <ta e="T612" id="Seg_8202" s="T611">dieses</ta>
            <ta e="T613" id="Seg_8203" s="T612">Kind.[NOM]</ta>
            <ta e="T614" id="Seg_8204" s="T613">geboren.werden-PTCP.PST</ta>
            <ta e="T615" id="Seg_8205" s="T614">Zelt-3SG-GEN</ta>
            <ta e="T616" id="Seg_8206" s="T615">Inneres-3SG-DAT/LOC</ta>
            <ta e="T617" id="Seg_8207" s="T616">Kind.[NOM]</ta>
            <ta e="T618" id="Seg_8208" s="T617">geboren.werden-PTCP.PST</ta>
            <ta e="T619" id="Seg_8209" s="T618">Gebärstange-3SG-DAT/LOC</ta>
            <ta e="T620" id="Seg_8210" s="T619">Frau.[NOM]</ta>
            <ta e="T621" id="Seg_8211" s="T620">gebären-PTCP.PST</ta>
            <ta e="T622" id="Seg_8212" s="T621">Gebärstange-3SG-DAT/LOC</ta>
            <ta e="T623" id="Seg_8213" s="T622">dann</ta>
            <ta e="T624" id="Seg_8214" s="T623">sagen-HAB-3PL</ta>
            <ta e="T625" id="Seg_8215" s="T624">vor.langer.Zeit-ADJZ</ta>
            <ta e="T626" id="Seg_8216" s="T625">Zeit-ABL</ta>
            <ta e="T627" id="Seg_8217" s="T626">Gebärstange-PROPR.[NOM]</ta>
            <ta e="T628" id="Seg_8218" s="T627">vorübergehende.Siedlung-ACC</ta>
            <ta e="T629" id="Seg_8219" s="T628">vorbeigehen-TEMP-3PL</ta>
            <ta e="T630" id="Seg_8220" s="T629">Zelt-3SG.[NOM]</ta>
            <ta e="T631" id="Seg_8221" s="T630">NEG</ta>
            <ta e="T632" id="Seg_8222" s="T631">NEG.EX</ta>
            <ta e="T633" id="Seg_8223" s="T632">sein-TEMP-3SG</ta>
            <ta e="T634" id="Seg_8224" s="T633">Gebärstange-ACC</ta>
            <ta e="T635" id="Seg_8225" s="T634">bauen-CVB.SIM</ta>
            <ta e="T636" id="Seg_8226" s="T635">stehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T637" id="Seg_8227" s="T636">sehen-TEMP-3PL</ta>
            <ta e="T638" id="Seg_8228" s="T637">nun</ta>
            <ta e="T639" id="Seg_8229" s="T638">dieses</ta>
            <ta e="T640" id="Seg_8230" s="T639">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T641" id="Seg_8231" s="T640">Frau.[NOM]</ta>
            <ta e="T642" id="Seg_8232" s="T641">geboren.werden-PST2.[3SG]</ta>
            <ta e="T643" id="Seg_8233" s="T642">sein-PST2.[3SG]</ta>
            <ta e="T644" id="Seg_8234" s="T643">Gebärstange-3SG.[NOM]</ta>
            <ta e="T645" id="Seg_8235" s="T644">stehen-PRS.[3SG]</ta>
            <ta e="T646" id="Seg_8236" s="T645">geboren.werden-PTCP.PST</ta>
            <ta e="T647" id="Seg_8237" s="T646">Gebärstange-3SG.[NOM]</ta>
            <ta e="T648" id="Seg_8238" s="T647">Zelt-PROPR.[NOM]</ta>
            <ta e="T649" id="Seg_8239" s="T648">sein-TEMP-3SG</ta>
            <ta e="T650" id="Seg_8240" s="T649">solch</ta>
            <ta e="T651" id="Seg_8241" s="T650">Kind.[NOM]</ta>
            <ta e="T652" id="Seg_8242" s="T651">geboren.werden-PST2.[3SG]</ta>
            <ta e="T653" id="Seg_8243" s="T652">sein-PST2.[3SG]</ta>
            <ta e="T654" id="Seg_8244" s="T653">jenes</ta>
            <ta e="T655" id="Seg_8245" s="T654">Kind.[NOM]</ta>
            <ta e="T656" id="Seg_8246" s="T655">geboren.werden-PTCP.PST</ta>
            <ta e="T657" id="Seg_8247" s="T656">Zelt-3SG.[NOM]</ta>
            <ta e="T658" id="Seg_8248" s="T657">sagen-HAB-3PL</ta>
            <ta e="T659" id="Seg_8249" s="T658">dann</ta>
            <ta e="T660" id="Seg_8250" s="T659">aber</ta>
            <ta e="T661" id="Seg_8251" s="T660">MOD</ta>
            <ta e="T662" id="Seg_8252" s="T661">dann</ta>
            <ta e="T663" id="Seg_8253" s="T662">dieses</ta>
            <ta e="T664" id="Seg_8254" s="T663">Frau-ACC</ta>
            <ta e="T665" id="Seg_8255" s="T664">Kind-VBZ-PTCP.PST</ta>
            <ta e="T666" id="Seg_8256" s="T665">Frau-EP-2SG.[NOM]</ta>
            <ta e="T667" id="Seg_8257" s="T666">schmutzig.[NOM]</ta>
            <ta e="T668" id="Seg_8258" s="T667">EMPH</ta>
            <ta e="T669" id="Seg_8259" s="T668">geboren.werden-PTCP.PST</ta>
            <ta e="T670" id="Seg_8260" s="T669">schmutzig-3SG-ACC</ta>
            <ta e="T671" id="Seg_8261" s="T670">säubern-CVB.SEQ</ta>
            <ta e="T672" id="Seg_8262" s="T671">wieder</ta>
            <ta e="T673" id="Seg_8263" s="T672">räuchern-PRS-2SG</ta>
            <ta e="T674" id="Seg_8264" s="T673">Gesicht-3SG-GEN</ta>
            <ta e="T675" id="Seg_8265" s="T674">Unterteil-3SG-INSTR</ta>
            <ta e="T676" id="Seg_8266" s="T675">Hals-3SG-ACC</ta>
            <ta e="T677" id="Seg_8267" s="T676">um.herum</ta>
            <ta e="T678" id="Seg_8268" s="T677">drei-MLTP</ta>
            <ta e="T679" id="Seg_8269" s="T678">Mal.[NOM]</ta>
            <ta e="T680" id="Seg_8270" s="T679">räuchern-PRS-2SG</ta>
            <ta e="T681" id="Seg_8271" s="T680">INTJ</ta>
            <ta e="T682" id="Seg_8272" s="T681">INTJ</ta>
            <ta e="T683" id="Seg_8273" s="T682">schmutzig-EP-2SG.[NOM]</ta>
            <ta e="T684" id="Seg_8274" s="T683">Wolke-DAT/LOC</ta>
            <ta e="T685" id="Seg_8275" s="T684">schlechter.Gedanke-EP-2SG.[NOM]</ta>
            <ta e="T686" id="Seg_8276" s="T685">Mond-DAT/LOC</ta>
            <ta e="T687" id="Seg_8277" s="T686">INTJ</ta>
            <ta e="T688" id="Seg_8278" s="T687">INTJ</ta>
            <ta e="T689" id="Seg_8279" s="T688">schmutzig-EP-2SG.[NOM]</ta>
            <ta e="T690" id="Seg_8280" s="T689">Wolke-DAT/LOC</ta>
            <ta e="T691" id="Seg_8281" s="T690">schlechter.Gedanke-EP-2SG.[NOM]</ta>
            <ta e="T692" id="Seg_8282" s="T691">Mond-DAT/LOC</ta>
            <ta e="T693" id="Seg_8283" s="T692">INTJ-INTJ</ta>
            <ta e="T694" id="Seg_8284" s="T693">schmutzig-3SG-ACC</ta>
            <ta e="T695" id="Seg_8285" s="T694">Wolke-DAT/LOC</ta>
            <ta e="T696" id="Seg_8286" s="T695">schlechter.Gedanke-EP-2SG.[NOM]</ta>
            <ta e="T697" id="Seg_8287" s="T696">Mond-DAT/LOC</ta>
            <ta e="T698" id="Seg_8288" s="T697">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T699" id="Seg_8289" s="T698">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T700" id="Seg_8290" s="T699">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T701" id="Seg_8291" s="T700">sagen-CVB.SEQ-2SG</ta>
            <ta e="T702" id="Seg_8292" s="T701">sagen-CVB.SEQ-2SG</ta>
            <ta e="T703" id="Seg_8293" s="T702">räuchern-PRS-2SG</ta>
            <ta e="T704" id="Seg_8294" s="T703">jenes</ta>
            <ta e="T705" id="Seg_8295" s="T704">jenes</ta>
            <ta e="T706" id="Seg_8296" s="T705">machen-CVB.SEQ</ta>
            <ta e="T707" id="Seg_8297" s="T706">nachdem</ta>
            <ta e="T708" id="Seg_8298" s="T707">Kind-3SG-ACC</ta>
            <ta e="T709" id="Seg_8299" s="T708">nehmen-PRS.[3SG]</ta>
            <ta e="T710" id="Seg_8300" s="T709">Hebamme.[NOM]</ta>
            <ta e="T711" id="Seg_8301" s="T710">dann</ta>
            <ta e="T712" id="Seg_8302" s="T711">MOD</ta>
            <ta e="T713" id="Seg_8303" s="T712">Frau-ACC</ta>
            <ta e="T714" id="Seg_8304" s="T713">stehen-CAUS-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_8305" s="T714">liegen-PTCP.PRS</ta>
            <ta e="T716" id="Seg_8306" s="T715">Bettwäsche-3SG-ABL</ta>
            <ta e="T717" id="Seg_8307" s="T716">jenes</ta>
            <ta e="T718" id="Seg_8308" s="T717">geboren.werden-PTCP.PST</ta>
            <ta e="T719" id="Seg_8309" s="T718">Zelt-3SG-DAT/LOC</ta>
            <ta e="T720" id="Seg_8310" s="T719">dann</ta>
            <ta e="T721" id="Seg_8311" s="T720">aufstehen-CVB.SEQ</ta>
            <ta e="T722" id="Seg_8312" s="T721">herausnehmen-PRS-1PL</ta>
            <ta e="T723" id="Seg_8313" s="T722">Haus-1PL-DAT/LOC</ta>
            <ta e="T724" id="Seg_8314" s="T723">gehen-PRS-1PL</ta>
            <ta e="T725" id="Seg_8315" s="T724">führen-CVB.SEQ</ta>
            <ta e="T726" id="Seg_8316" s="T725">mitnehmen-PRS-1PL</ta>
            <ta e="T727" id="Seg_8317" s="T726">geboren.werden-PTCP.PST</ta>
            <ta e="T728" id="Seg_8318" s="T727">Frau-ACC</ta>
            <ta e="T729" id="Seg_8319" s="T728">Kind-VBZ-PTCP.PST</ta>
            <ta e="T730" id="Seg_8320" s="T729">Frau-ACC</ta>
            <ta e="T731" id="Seg_8321" s="T730">jenes</ta>
            <ta e="T732" id="Seg_8322" s="T731">machen-CVB.SEQ</ta>
            <ta e="T733" id="Seg_8323" s="T732">nachdem</ta>
            <ta e="T734" id="Seg_8324" s="T733">jenes</ta>
            <ta e="T735" id="Seg_8325" s="T734">führen-CVB.SEQ</ta>
            <ta e="T736" id="Seg_8326" s="T735">sein-CVB.SEQ</ta>
            <ta e="T737" id="Seg_8327" s="T736">jenes</ta>
            <ta e="T738" id="Seg_8328" s="T737">erzählen-PRS.[3SG]</ta>
            <ta e="T739" id="Seg_8329" s="T738">jenes</ta>
            <ta e="T740" id="Seg_8330" s="T739">na</ta>
            <ta e="T741" id="Seg_8331" s="T740">Kind.[NOM]</ta>
            <ta e="T742" id="Seg_8332" s="T741">geboren.werden-PST1-3SG</ta>
            <ta e="T743" id="Seg_8333" s="T742">sagen-CVB.SEQ</ta>
            <ta e="T744" id="Seg_8334" s="T743">Familie-3PL-DAT/LOC</ta>
            <ta e="T745" id="Seg_8335" s="T744">Zelt-PROPR</ta>
            <ta e="T746" id="Seg_8336" s="T745">Zelt-3PL-DAT/LOC</ta>
            <ta e="T747" id="Seg_8337" s="T746">Zelt-PROPR</ta>
            <ta e="T748" id="Seg_8338" s="T747">Stange.[NOM]</ta>
            <ta e="T749" id="Seg_8339" s="T748">Zelt-DAT/LOC</ta>
            <ta e="T750" id="Seg_8340" s="T749">jenes</ta>
            <ta e="T751" id="Seg_8341" s="T750">erzählen-CVB.SEQ</ta>
            <ta e="T752" id="Seg_8342" s="T751">erzählen-PRS.[3SG]</ta>
            <ta e="T753" id="Seg_8343" s="T752">hey</ta>
            <ta e="T754" id="Seg_8344" s="T753">jenes</ta>
            <ta e="T755" id="Seg_8345" s="T754">Mädchen.[NOM]</ta>
            <ta e="T756" id="Seg_8346" s="T755">Kind.[NOM]</ta>
            <ta e="T757" id="Seg_8347" s="T756">geboren.werden-PTCP.PST</ta>
            <ta e="T758" id="Seg_8348" s="T757">sein-TEMP-3SG</ta>
            <ta e="T759" id="Seg_8349" s="T758">Mädchen.[NOM]</ta>
            <ta e="T760" id="Seg_8350" s="T759">Kind.[NOM]</ta>
            <ta e="T761" id="Seg_8351" s="T760">sagen-PRS-3PL</ta>
            <ta e="T762" id="Seg_8352" s="T761">Junge.[NOM]</ta>
            <ta e="T763" id="Seg_8353" s="T762">Kind.[NOM]</ta>
            <ta e="T764" id="Seg_8354" s="T763">geboren.werden-PTCP.PST</ta>
            <ta e="T765" id="Seg_8355" s="T764">sein-TEMP-3SG</ta>
            <ta e="T766" id="Seg_8356" s="T765">Junge.[NOM]</ta>
            <ta e="T767" id="Seg_8357" s="T766">Kind.[NOM]</ta>
            <ta e="T768" id="Seg_8358" s="T767">sagen-PRS-3PL</ta>
            <ta e="T769" id="Seg_8359" s="T768">dann</ta>
            <ta e="T770" id="Seg_8360" s="T769">dieses</ta>
            <ta e="T771" id="Seg_8361" s="T770">drei</ta>
            <ta e="T772" id="Seg_8362" s="T771">Tag.[NOM]</ta>
            <ta e="T773" id="Seg_8363" s="T772">sein-CVB.SEQ</ta>
            <ta e="T774" id="Seg_8364" s="T773">nachdem</ta>
            <ta e="T775" id="Seg_8365" s="T774">Vater-3SG.[NOM]</ta>
            <ta e="T776" id="Seg_8366" s="T775">Wiege-ACC</ta>
            <ta e="T777" id="Seg_8367" s="T776">machen-PRS.[3SG]</ta>
            <ta e="T778" id="Seg_8368" s="T777">Kind-3SG-DAT/LOC</ta>
            <ta e="T779" id="Seg_8369" s="T778">Großvater-3SG.[NOM]</ta>
            <ta e="T780" id="Seg_8370" s="T779">helfen-PRS.[3SG]</ta>
            <ta e="T781" id="Seg_8371" s="T780">dann</ta>
            <ta e="T782" id="Seg_8372" s="T781">EMPH</ta>
            <ta e="T783" id="Seg_8373" s="T782">dieses</ta>
            <ta e="T784" id="Seg_8374" s="T783">Wiege-ACC</ta>
            <ta e="T785" id="Seg_8375" s="T784">MOD</ta>
            <ta e="T786" id="Seg_8376" s="T785">treffen-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T787" id="Seg_8377" s="T786">kräftig</ta>
            <ta e="T788" id="Seg_8378" s="T787">Brett-PL-EP-INSTR</ta>
            <ta e="T789" id="Seg_8379" s="T788">Schnur-INSTR</ta>
            <ta e="T790" id="Seg_8380" s="T789">dünne.Lederschicht.[NOM]</ta>
            <ta e="T791" id="Seg_8381" s="T790">Schnur-INSTR</ta>
            <ta e="T792" id="Seg_8382" s="T791">befestigen-CVB.SEQ</ta>
            <ta e="T793" id="Seg_8383" s="T792">machen-HAB-3PL</ta>
            <ta e="T794" id="Seg_8384" s="T793">Fichte-ACC</ta>
            <ta e="T795" id="Seg_8385" s="T794">dieses</ta>
            <ta e="T796" id="Seg_8386" s="T795">wer.[NOM]</ta>
            <ta e="T797" id="Seg_8387" s="T796">äh</ta>
            <ta e="T798" id="Seg_8388" s="T797">Furnier.[NOM]</ta>
            <ta e="T799" id="Seg_8389" s="T798">ähnlich</ta>
            <ta e="T800" id="Seg_8390" s="T799">sagen-HAB-3PL</ta>
            <ta e="T801" id="Seg_8391" s="T800">dieses.[NOM]</ta>
            <ta e="T802" id="Seg_8392" s="T801">solch-ACC</ta>
            <ta e="T803" id="Seg_8393" s="T802">MOD</ta>
            <ta e="T804" id="Seg_8394" s="T803">machen-CAUS-PTCP.HAB-3SG</ta>
            <ta e="T805" id="Seg_8395" s="T804">nein-3PL</ta>
            <ta e="T806" id="Seg_8396" s="T805">trocken.werden-PTCP.PST</ta>
            <ta e="T807" id="Seg_8397" s="T806">Holz.[NOM]</ta>
            <ta e="T808" id="Seg_8398" s="T807">Fell-3SG.[NOM]</ta>
            <ta e="T809" id="Seg_8399" s="T808">dieses.[NOM]</ta>
            <ta e="T810" id="Seg_8400" s="T809">Furnier.[NOM]</ta>
            <ta e="T811" id="Seg_8401" s="T810">jenes.[NOM]</ta>
            <ta e="T812" id="Seg_8402" s="T811">wegen</ta>
            <ta e="T813" id="Seg_8403" s="T812">aber</ta>
            <ta e="T814" id="Seg_8404" s="T813">MOD</ta>
            <ta e="T815" id="Seg_8405" s="T814">Kind.[NOM]</ta>
            <ta e="T816" id="Seg_8406" s="T815">trocken.werden-FUT.[3SG]</ta>
            <ta e="T817" id="Seg_8407" s="T816">sagen-HAB-3PL</ta>
            <ta e="T818" id="Seg_8408" s="T817">jenes.[NOM]</ta>
            <ta e="T819" id="Seg_8409" s="T818">wegen</ta>
            <ta e="T0" id="Seg_8410" s="T820">Brett-INSTR</ta>
            <ta e="T822" id="Seg_8411" s="T821">und</ta>
            <ta e="T823" id="Seg_8412" s="T822">Brett-2SG.[NOM]</ta>
            <ta e="T824" id="Seg_8413" s="T823">befestigen-EP-PASS/REFL-TEMP-3SG</ta>
            <ta e="T825" id="Seg_8414" s="T824">Q</ta>
            <ta e="T826" id="Seg_8415" s="T825">befestigen-EP-PASS/REFL-TEMP-3SG</ta>
            <ta e="T827" id="Seg_8416" s="T826">befestigen-EP-PASS/REFL-TEMP-3SG</ta>
            <ta e="T828" id="Seg_8417" s="T827">Schnur-INSTR</ta>
            <ta e="T829" id="Seg_8418" s="T828">befestigen-CVB.SEQ</ta>
            <ta e="T830" id="Seg_8419" s="T829">machen-TEMP-3PL</ta>
            <ta e="T831" id="Seg_8420" s="T830">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T832" id="Seg_8421" s="T831">Zwischenraum-PROPR.[NOM]</ta>
            <ta e="T833" id="Seg_8422" s="T832">sein-HAB.[3SG]</ta>
            <ta e="T834" id="Seg_8423" s="T833">Brett.[NOM]</ta>
            <ta e="T835" id="Seg_8424" s="T834">Brett-3SG-ABL</ta>
            <ta e="T836" id="Seg_8425" s="T835">wieder</ta>
            <ta e="T837" id="Seg_8426" s="T836">Zwischenraum-PROPR.[NOM]</ta>
            <ta e="T838" id="Seg_8427" s="T837">sein-HAB.[3SG]</ta>
            <ta e="T839" id="Seg_8428" s="T838">dann</ta>
            <ta e="T840" id="Seg_8429" s="T839">Atem.[NOM]</ta>
            <ta e="T841" id="Seg_8430" s="T840">hineingehen-PRS.[3SG]</ta>
            <ta e="T842" id="Seg_8431" s="T841">MOD</ta>
            <ta e="T843" id="Seg_8432" s="T842">jenes.[NOM]</ta>
            <ta e="T844" id="Seg_8433" s="T843">wegen</ta>
            <ta e="T845" id="Seg_8434" s="T844">Kind-2SG.[NOM]</ta>
            <ta e="T846" id="Seg_8435" s="T845">gesund.[NOM]</ta>
            <ta e="T847" id="Seg_8436" s="T846">geboren.werden-PRS.[3SG]</ta>
            <ta e="T848" id="Seg_8437" s="T847">MOD</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_8438" s="T1">давно-ADJZ</ta>
            <ta e="T3" id="Seg_8439" s="T2">ребенок.[NOM]</ta>
            <ta e="T4" id="Seg_8440" s="T3">родить-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T5" id="Seg_8441" s="T4">кол.для.рождения-DAT/LOC</ta>
            <ta e="T6" id="Seg_8442" s="T5">кол.для.рождения-DAT/LOC</ta>
            <ta e="T7" id="Seg_8443" s="T6">ребенок-3PL.[NOM]</ta>
            <ta e="T8" id="Seg_8444" s="T7">ребенок</ta>
            <ta e="T9" id="Seg_8445" s="T8">беременная.[NOM]</ta>
            <ta e="T10" id="Seg_8446" s="T9">жена.[NOM]</ta>
            <ta e="T11" id="Seg_8447" s="T10">родить-PTCP.PRS</ta>
            <ta e="T12" id="Seg_8448" s="T11">час-3SG-DAT/LOC</ta>
            <ta e="T13" id="Seg_8449" s="T12">давно-ADJZ</ta>
            <ta e="T14" id="Seg_8450" s="T13">время-ABL</ta>
            <ta e="T15" id="Seg_8451" s="T14">кол.для.рождения-ACC</ta>
            <ta e="T16" id="Seg_8452" s="T15">готовить-HAB-3PL</ta>
            <ta e="T17" id="Seg_8453" s="T16">отдельно</ta>
            <ta e="T18" id="Seg_8454" s="T17">шест.[NOM]</ta>
            <ta e="T19" id="Seg_8455" s="T18">чум-ACC</ta>
            <ta e="T20" id="Seg_8456" s="T19">строить-CVB.SEQ-3PL</ta>
            <ta e="T21" id="Seg_8457" s="T20">готовить-HAB-3PL</ta>
            <ta e="T22" id="Seg_8458" s="T21">ребенок.[NOM]</ta>
            <ta e="T23" id="Seg_8459" s="T22">родить-PTCP.PRS</ta>
            <ta e="T24" id="Seg_8460" s="T23">чум-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_8461" s="T24">родиться-PTCP.PST</ta>
            <ta e="T26" id="Seg_8462" s="T25">чум-3SG.[NOM]</ta>
            <ta e="T27" id="Seg_8463" s="T26">говорить-HAB-3PL</ta>
            <ta e="T28" id="Seg_8464" s="T27">потом</ta>
            <ta e="T29" id="Seg_8465" s="T28">там</ta>
            <ta e="T30" id="Seg_8466" s="T29">однако</ta>
            <ta e="T31" id="Seg_8467" s="T30">тот</ta>
            <ta e="T32" id="Seg_8468" s="T31">кол.для.рождения-ACC</ta>
            <ta e="T33" id="Seg_8469" s="T32">делать-HAB-3PL</ta>
            <ta e="T34" id="Seg_8470" s="T33">семья-3PL.[NOM]</ta>
            <ta e="T35" id="Seg_8471" s="T34">стоять-CAUS-PRS-3PL</ta>
            <ta e="T36" id="Seg_8472" s="T35">дедушка-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_8473" s="T36">Q</ta>
            <ta e="T38" id="Seg_8474" s="T37">отец-3SG.[NOM]</ta>
            <ta e="T39" id="Seg_8475" s="T38">Q</ta>
            <ta e="T40" id="Seg_8476" s="T39">мать-3SG.[NOM]</ta>
            <ta e="T41" id="Seg_8477" s="T40">Q</ta>
            <ta e="T42" id="Seg_8478" s="T41">помогать-CVB.SEQ-3PL</ta>
            <ta e="T43" id="Seg_8479" s="T42">связывать-PRS-3PL</ta>
            <ta e="T44" id="Seg_8480" s="T43">кол.для.рождения-ACC</ta>
            <ta e="T45" id="Seg_8481" s="T44">шест-DAT/LOC</ta>
            <ta e="T46" id="Seg_8482" s="T45">прижать-CVB.SIM</ta>
            <ta e="T47" id="Seg_8483" s="T46">поперек</ta>
            <ta e="T48" id="Seg_8484" s="T47">дерево-ACC</ta>
            <ta e="T49" id="Seg_8485" s="T48">потом</ta>
            <ta e="T50" id="Seg_8486" s="T49">э</ta>
            <ta e="T51" id="Seg_8487" s="T50">кол.для.рождения-3SG-GEN</ta>
            <ta e="T52" id="Seg_8488" s="T51">конец-3SG-DAT/LOC</ta>
            <ta e="T53" id="Seg_8489" s="T52">MOD</ta>
            <ta e="T54" id="Seg_8490" s="T53">стоять.прямо-ADVZ</ta>
            <ta e="T55" id="Seg_8491" s="T54">толкать-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T56" id="Seg_8492" s="T55">стоять-PTCP.PRS</ta>
            <ta e="T57" id="Seg_8493" s="T56">голова-3SG.[NOM]</ta>
            <ta e="T58" id="Seg_8494" s="T57">ээ</ta>
            <ta e="T59" id="Seg_8495" s="T58">дерево.[NOM]</ta>
            <ta e="T60" id="Seg_8496" s="T59">быть-HAB.[3SG]</ta>
            <ta e="T61" id="Seg_8497" s="T60">шест-DIM-3SG.[NOM]</ta>
            <ta e="T62" id="Seg_8498" s="T61">тогда</ta>
            <ta e="T63" id="Seg_8499" s="T62">однако</ta>
            <ta e="T64" id="Seg_8500" s="T63">тот</ta>
            <ta e="T65" id="Seg_8501" s="T64">кол.для.рождения-2SG.[NOM]</ta>
            <ta e="T66" id="Seg_8502" s="T65">EMPH</ta>
            <ta e="T67" id="Seg_8503" s="T66">грудь-3SG-GEN</ta>
            <ta e="T68" id="Seg_8504" s="T67">изгиб-3SG-INSTR</ta>
            <ta e="T69" id="Seg_8505" s="T68">быть-HAB.[3SG]</ta>
            <ta e="T70" id="Seg_8506" s="T69">жена.[NOM]</ta>
            <ta e="T71" id="Seg_8507" s="T70">потом</ta>
            <ta e="T72" id="Seg_8508" s="T71">повесить-PASS/REFL-CVB.SEQ</ta>
            <ta e="T73" id="Seg_8509" s="T72">сидеть-CVB.SEQ</ta>
            <ta e="T74" id="Seg_8510" s="T73">родить-HAB.[3SG]</ta>
            <ta e="T75" id="Seg_8511" s="T74">тот</ta>
            <ta e="T76" id="Seg_8512" s="T75">родить-PTCP.PRS-3SG-GEN</ta>
            <ta e="T77" id="Seg_8513" s="T76">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T78" id="Seg_8514" s="T77">MOD</ta>
            <ta e="T79" id="Seg_8515" s="T78">ээ</ta>
            <ta e="T80" id="Seg_8516" s="T79">сидеть-PTCP.PRS</ta>
            <ta e="T81" id="Seg_8517" s="T80">место-3SG-DAT/LOC</ta>
            <ta e="T82" id="Seg_8518" s="T81">куски.шкуры.[NOM]</ta>
            <ta e="T83" id="Seg_8519" s="T82">быть-HAB.[3SG]</ta>
            <ta e="T84" id="Seg_8520" s="T83">вот</ta>
            <ta e="T85" id="Seg_8521" s="T84">тот</ta>
            <ta e="T86" id="Seg_8522" s="T85">куски.шкуры-DAT/LOC</ta>
            <ta e="T87" id="Seg_8523" s="T86">MOD</ta>
            <ta e="T88" id="Seg_8524" s="T87">трава.[NOM]</ta>
            <ta e="T89" id="Seg_8525" s="T88">быть-HAB.[3SG]</ta>
            <ta e="T90" id="Seg_8526" s="T89">трава-ACC</ta>
            <ta e="T91" id="Seg_8527" s="T90">класть-HAB-3PL</ta>
            <ta e="T92" id="Seg_8528" s="T91">облегчение-3SG-DAT/LOC</ta>
            <ta e="T93" id="Seg_8529" s="T92">говорить-CVB.SEQ-3PL</ta>
            <ta e="T94" id="Seg_8530" s="T93">тот-DAT/LOC</ta>
            <ta e="T95" id="Seg_8531" s="T94">MOD</ta>
            <ta e="T96" id="Seg_8532" s="T95">ребенок.[NOM]</ta>
            <ta e="T97" id="Seg_8533" s="T96">падать-TEMP-3SG</ta>
            <ta e="T98" id="Seg_8534" s="T97">трава-3SG-DAT/LOC</ta>
            <ta e="T99" id="Seg_8535" s="T98">падать-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_8536" s="T99">говорить-HAB-3PL</ta>
            <ta e="T101" id="Seg_8537" s="T100">тот.[NOM]</ta>
            <ta e="T102" id="Seg_8538" s="T101">из_за</ta>
            <ta e="T103" id="Seg_8539" s="T102">трава-PROPR</ta>
            <ta e="T104" id="Seg_8540" s="T103">трава-PROPR-3SG-ACC</ta>
            <ta e="T105" id="Seg_8541" s="T104">из_за</ta>
            <ta e="T106" id="Seg_8542" s="T105">трава-DAT/LOC</ta>
            <ta e="T107" id="Seg_8543" s="T106">падать-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_8544" s="T107">говорить-HAB-3PL</ta>
            <ta e="T109" id="Seg_8545" s="T108">ээ</ta>
            <ta e="T110" id="Seg_8546" s="T109">жена.[NOM]</ta>
            <ta e="T111" id="Seg_8547" s="T110">родить-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_8548" s="T112">давно-ADJZ</ta>
            <ta e="T114" id="Seg_8549" s="T113">время-ABL</ta>
            <ta e="T115" id="Seg_8550" s="T114">далекий-DAT/LOC</ta>
            <ta e="T116" id="Seg_8551" s="T115">есть</ta>
            <ta e="T117" id="Seg_8552" s="T116">быть-TEMP-3SG</ta>
            <ta e="T118" id="Seg_8553" s="T117">тот</ta>
            <ta e="T119" id="Seg_8554" s="T118">ээ</ta>
            <ta e="T120" id="Seg_8555" s="T119">повитуха.[NOM]</ta>
            <ta e="T121" id="Seg_8556" s="T120">старуха-3PL.[NOM]</ta>
            <ta e="T122" id="Seg_8557" s="T121">далекий.[NOM]</ta>
            <ta e="T123" id="Seg_8558" s="T122">есть</ta>
            <ta e="T124" id="Seg_8559" s="T123">быть</ta>
            <ta e="T125" id="Seg_8560" s="T124">ребенок.[NOM]</ta>
            <ta e="T126" id="Seg_8561" s="T125">родить-NMNZ.[NOM]</ta>
            <ta e="T127" id="Seg_8562" s="T126">падать-AG-3PL.[NOM]</ta>
            <ta e="T128" id="Seg_8563" s="T127">жена-3PL.[NOM]</ta>
            <ta e="T129" id="Seg_8564" s="T128">далекий.[NOM]</ta>
            <ta e="T130" id="Seg_8565" s="T129">есть</ta>
            <ta e="T131" id="Seg_8566" s="T130">быть-TEMP-3SG</ta>
            <ta e="T132" id="Seg_8567" s="T131">сани-PROPR.[NOM]</ta>
            <ta e="T133" id="Seg_8568" s="T132">идти-CVB.SEQ-3PL</ta>
            <ta e="T134" id="Seg_8569" s="T133">приносить-HAB-3PL</ta>
            <ta e="T135" id="Seg_8570" s="T134">просить-CVB.SEQ-3PL</ta>
            <ta e="T136" id="Seg_8571" s="T135">эй</ta>
            <ta e="T137" id="Seg_8572" s="T136">ребенок-1PL.[NOM]</ta>
            <ta e="T138" id="Seg_8573" s="T137">тогда</ta>
            <ta e="T139" id="Seg_8574" s="T138">тогда</ta>
            <ta e="T140" id="Seg_8575" s="T139">тот</ta>
            <ta e="T141" id="Seg_8576" s="T140">дочь-1PL.[NOM]</ta>
            <ta e="T143" id="Seg_8577" s="T142">родить-FUT-3SG</ta>
            <ta e="T144" id="Seg_8578" s="T143">говорить-CVB.SEQ-3PL</ta>
            <ta e="T145" id="Seg_8579" s="T144">жена-1PL.[NOM]</ta>
            <ta e="T146" id="Seg_8580" s="T145">родить-FUT-3SG</ta>
            <ta e="T147" id="Seg_8581" s="T146">говорить-CVB.SEQ-3PL</ta>
            <ta e="T148" id="Seg_8582" s="T147">приносить-HAB-3PL</ta>
            <ta e="T149" id="Seg_8583" s="T148">ээ</ta>
            <ta e="T150" id="Seg_8584" s="T149">близкий.[NOM]</ta>
            <ta e="T151" id="Seg_8585" s="T150">есть</ta>
            <ta e="T152" id="Seg_8586" s="T151">быть-TEMP-3SG</ta>
            <ta e="T153" id="Seg_8587" s="T152">MOD</ta>
            <ta e="T154" id="Seg_8588" s="T153">звать-HAB-3PL</ta>
            <ta e="T155" id="Seg_8589" s="T154">тот</ta>
            <ta e="T156" id="Seg_8590" s="T155">повитуха.[NOM]</ta>
            <ta e="T157" id="Seg_8591" s="T156">старуха-ACC</ta>
            <ta e="T158" id="Seg_8592" s="T157">так</ta>
            <ta e="T159" id="Seg_8593" s="T158">делать-CVB.SEQ</ta>
            <ta e="T160" id="Seg_8594" s="T159">тот</ta>
            <ta e="T161" id="Seg_8595" s="T160">повитуха.[NOM]</ta>
            <ta e="T162" id="Seg_8596" s="T161">рука.[NOM]</ta>
            <ta e="T163" id="Seg_8597" s="T162">полотенце-3SG.[NOM]</ta>
            <ta e="T164" id="Seg_8598" s="T163">что-3SG.[NOM]</ta>
            <ta e="T165" id="Seg_8599" s="T164">каждый-3SG.[NOM]</ta>
            <ta e="T166" id="Seg_8600" s="T165">чистый.[NOM]</ta>
            <ta e="T167" id="Seg_8601" s="T166">становиться-PTCP.FUT-3SG-ACC</ta>
            <ta e="T168" id="Seg_8602" s="T167">правильно</ta>
            <ta e="T169" id="Seg_8603" s="T168">быть-HAB.[3SG]</ta>
            <ta e="T170" id="Seg_8604" s="T169">каждый-INTNS-3SG.[NOM]</ta>
            <ta e="T171" id="Seg_8605" s="T170">новый.[NOM]</ta>
            <ta e="T172" id="Seg_8606" s="T171">хватать-EP-PASS/REFL-EP-NEG.PTCP.PST</ta>
            <ta e="T173" id="Seg_8607" s="T172">вещь.[NOM]</ta>
            <ta e="T174" id="Seg_8608" s="T173">быть-PRS-NEC.[3SG]</ta>
            <ta e="T175" id="Seg_8609" s="T174">полотенце-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_8610" s="T175">EMPH</ta>
            <ta e="T177" id="Seg_8611" s="T176">рука.[NOM]</ta>
            <ta e="T178" id="Seg_8612" s="T177">полотенце-3SG.[NOM]</ta>
            <ta e="T179" id="Seg_8613" s="T178">ребенок-ACC</ta>
            <ta e="T180" id="Seg_8614" s="T179">родить-MULT-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_8615" s="T180">быть.в.сватках</ta>
            <ta e="T182" id="Seg_8616" s="T181">жена.[NOM]</ta>
            <ta e="T183" id="Seg_8617" s="T182">быть.в.сватках-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T184" id="Seg_8618" s="T183">тогда</ta>
            <ta e="T185" id="Seg_8619" s="T184">однако</ta>
            <ta e="T186" id="Seg_8620" s="T185">MOD</ta>
            <ta e="T187" id="Seg_8621" s="T186">тотем-PROPR.[NOM]</ta>
            <ta e="T188" id="Seg_8622" s="T187">быть-HAB.[3SG]</ta>
            <ta e="T189" id="Seg_8623" s="T188">тот</ta>
            <ta e="T190" id="Seg_8624" s="T189">тотем-ACC</ta>
            <ta e="T191" id="Seg_8625" s="T190">однако</ta>
            <ta e="T192" id="Seg_8626" s="T191">сначала</ta>
            <ta e="T193" id="Seg_8627" s="T192">взять-CVB.SIM</ta>
            <ta e="T194" id="Seg_8628" s="T193">приходить-CVB.SIM</ta>
            <ta e="T195" id="Seg_8629" s="T194">приходить-PRS-3PL</ta>
            <ta e="T196" id="Seg_8630" s="T195">жена-3PL.[NOM]</ta>
            <ta e="T197" id="Seg_8631" s="T196">родить-CVB.PURP</ta>
            <ta e="T198" id="Seg_8632" s="T197">делать-TEMP-3SG</ta>
            <ta e="T199" id="Seg_8633" s="T198">тот</ta>
            <ta e="T200" id="Seg_8634" s="T199">отдельно</ta>
            <ta e="T201" id="Seg_8635" s="T200">тот</ta>
            <ta e="T202" id="Seg_8636" s="T201">отдельно</ta>
            <ta e="T203" id="Seg_8637" s="T202">чум-DAT/LOC</ta>
            <ta e="T204" id="Seg_8638" s="T203">кто-ACC</ta>
            <ta e="T205" id="Seg_8639" s="T204">NEG</ta>
            <ta e="T206" id="Seg_8640" s="T205">входить-CAUS-NEG-3PL</ta>
            <ta e="T207" id="Seg_8641" s="T206">MOD</ta>
            <ta e="T208" id="Seg_8642" s="T207">кто.[NOM]</ta>
            <ta e="T209" id="Seg_8643" s="T208">отдельно</ta>
            <ta e="T210" id="Seg_8644" s="T209">чум.[NOM]</ta>
            <ta e="T211" id="Seg_8645" s="T210">строить-NEG-3PL</ta>
            <ta e="T212" id="Seg_8646" s="T211">дом-DAT/LOC</ta>
            <ta e="T213" id="Seg_8647" s="T212">родить-EP-CAUS-HAB-3PL</ta>
            <ta e="T214" id="Seg_8648" s="T213">дом-DAT/LOC</ta>
            <ta e="T215" id="Seg_8649" s="T214">жить-PTCP.PRS</ta>
            <ta e="T216" id="Seg_8650" s="T215">чум-3PL-DAT/LOC</ta>
            <ta e="T217" id="Seg_8651" s="T216">тот</ta>
            <ta e="T218" id="Seg_8652" s="T217">делать-CVB.SEQ</ta>
            <ta e="T219" id="Seg_8653" s="T218">после</ta>
            <ta e="T220" id="Seg_8654" s="T219">жить-PTCP.PRS</ta>
            <ta e="T221" id="Seg_8655" s="T220">чум-3PL-ABL</ta>
            <ta e="T222" id="Seg_8656" s="T221">отдельно</ta>
            <ta e="T223" id="Seg_8657" s="T222">делать-HAB-3PL</ta>
            <ta e="T224" id="Seg_8658" s="T223">кол.для.рождения-ACC</ta>
            <ta e="T225" id="Seg_8659" s="T224">стоять-CAUS-HAB-3PL</ta>
            <ta e="T226" id="Seg_8660" s="T225">вот</ta>
            <ta e="T227" id="Seg_8661" s="T226">тот</ta>
            <ta e="T228" id="Seg_8662" s="T227">делать-CVB.SEQ</ta>
            <ta e="T229" id="Seg_8663" s="T228">человек-ACC</ta>
            <ta e="T230" id="Seg_8664" s="T229">каждый-DIM-3SG-ACC</ta>
            <ta e="T231" id="Seg_8665" s="T230">вынимать-HAB-3PL</ta>
            <ta e="T232" id="Seg_8666" s="T231">кто.[NOM]</ta>
            <ta e="T233" id="Seg_8667" s="T232">NEG</ta>
            <ta e="T234" id="Seg_8668" s="T233">NEG</ta>
            <ta e="T235" id="Seg_8669" s="T234">быть-FUT-NEC.[3SG]</ta>
            <ta e="T236" id="Seg_8670" s="T235">одинокий</ta>
            <ta e="T237" id="Seg_8671" s="T236">повитуха-ACC</ta>
            <ta e="T238" id="Seg_8672" s="T237">с</ta>
            <ta e="T239" id="Seg_8673" s="T238">родить-PTCP.PRS</ta>
            <ta e="T240" id="Seg_8674" s="T239">жена.[NOM]</ta>
            <ta e="T241" id="Seg_8675" s="T240">только</ta>
            <ta e="T242" id="Seg_8676" s="T241">быть-PTCP.FUT-NEC-3PL</ta>
            <ta e="T243" id="Seg_8677" s="T242">там</ta>
            <ta e="T244" id="Seg_8678" s="T243">тогда</ta>
            <ta e="T245" id="Seg_8679" s="T244">однако</ta>
            <ta e="T246" id="Seg_8680" s="T245">MOD</ta>
            <ta e="T247" id="Seg_8681" s="T246">этот</ta>
            <ta e="T248" id="Seg_8682" s="T247">повитуха-2SG.[NOM]</ta>
            <ta e="T249" id="Seg_8683" s="T248">приходить-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_8684" s="T249">шест.[NOM]</ta>
            <ta e="T251" id="Seg_8685" s="T250">чум-DAT/LOC</ta>
            <ta e="T252" id="Seg_8686" s="T251">родить-PTCP.PRS</ta>
            <ta e="T253" id="Seg_8687" s="T252">чум-DAT/LOC</ta>
            <ta e="T254" id="Seg_8688" s="T253">чум-DAT/LOC</ta>
            <ta e="T255" id="Seg_8689" s="T254">отдельно</ta>
            <ta e="T256" id="Seg_8690" s="T255">строить-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T257" id="Seg_8691" s="T256">быть-TEMP-3SG</ta>
            <ta e="T258" id="Seg_8692" s="T257">MOD</ta>
            <ta e="T259" id="Seg_8693" s="T258">дом-DAT/LOC</ta>
            <ta e="T260" id="Seg_8694" s="T259">этот.[NOM]</ta>
            <ta e="T261" id="Seg_8695" s="T260">делаться-EP-PTCP.PST</ta>
            <ta e="T262" id="Seg_8696" s="T261">быть-TEMP-3SG</ta>
            <ta e="T263" id="Seg_8697" s="T262">тот</ta>
            <ta e="T264" id="Seg_8698" s="T263">приходить-CVB.SEQ</ta>
            <ta e="T265" id="Seg_8699" s="T264">вот</ta>
            <ta e="T266" id="Seg_8700" s="T265">приносить-CVB.SEQ</ta>
            <ta e="T267" id="Seg_8701" s="T266">приходить-CVB.SEQ-3PL</ta>
            <ta e="T268" id="Seg_8702" s="T267">тот</ta>
            <ta e="T269" id="Seg_8703" s="T268">повитуха.[NOM]</ta>
            <ta e="T270" id="Seg_8704" s="T269">старуха-3SG.[NOM]</ta>
            <ta e="T271" id="Seg_8705" s="T270">входить-TEMP-3SG</ta>
            <ta e="T272" id="Seg_8706" s="T271">тот-ACC</ta>
            <ta e="T273" id="Seg_8707" s="T272">каждый-DIM-ACC</ta>
            <ta e="T274" id="Seg_8708" s="T273">янтарь-VBZ-CVB.SEQ</ta>
            <ta e="T275" id="Seg_8709" s="T274">янтарь-EP-INSTR</ta>
            <ta e="T276" id="Seg_8710" s="T275">стружка-EP-INSTR</ta>
            <ta e="T277" id="Seg_8711" s="T276">зажигать-CVB.SEQ</ta>
            <ta e="T278" id="Seg_8712" s="T277">каждый-DIM-3SG-ACC</ta>
            <ta e="T279" id="Seg_8713" s="T278">очистить-PRS.[3SG]</ta>
            <ta e="T280" id="Seg_8714" s="T279">тот</ta>
            <ta e="T281" id="Seg_8715" s="T280">жена.[NOM]</ta>
            <ta e="T282" id="Seg_8716" s="T281">сидеть-PTCP.PRS</ta>
            <ta e="T283" id="Seg_8717" s="T282">место-3SG-ACC</ta>
            <ta e="T284" id="Seg_8718" s="T283">сидеть-PTCP.PRS</ta>
            <ta e="T285" id="Seg_8719" s="T284">чум-3SG-ACC</ta>
            <ta e="T286" id="Seg_8720" s="T285">вокруг</ta>
            <ta e="T287" id="Seg_8721" s="T286">дымить-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_8722" s="T287">INTJ</ta>
            <ta e="T289" id="Seg_8723" s="T288">INTJ</ta>
            <ta e="T290" id="Seg_8724" s="T289">INTJ</ta>
            <ta e="T291" id="Seg_8725" s="T290">INTJ</ta>
            <ta e="T292" id="Seg_8726" s="T291">грязный-EP-2SG.[NOM]</ta>
            <ta e="T293" id="Seg_8727" s="T292">туча.[NOM]</ta>
            <ta e="T294" id="Seg_8728" s="T293">плохая.дума-EP-2SG.[NOM]</ta>
            <ta e="T295" id="Seg_8729" s="T294">луна-DAT/LOC</ta>
            <ta e="T296" id="Seg_8730" s="T295">INTJ</ta>
            <ta e="T297" id="Seg_8731" s="T296">INTJ</ta>
            <ta e="T298" id="Seg_8732" s="T297">INTJ</ta>
            <ta e="T299" id="Seg_8733" s="T298">грязный-EP-2SG.[NOM]</ta>
            <ta e="T300" id="Seg_8734" s="T299">туча.[NOM]</ta>
            <ta e="T301" id="Seg_8735" s="T300">плохая.дума-EP-2SG.[NOM]</ta>
            <ta e="T302" id="Seg_8736" s="T301">луна-DAT/LOC</ta>
            <ta e="T303" id="Seg_8737" s="T302">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T304" id="Seg_8738" s="T303">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T305" id="Seg_8739" s="T304">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T306" id="Seg_8740" s="T305">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T307" id="Seg_8741" s="T306">INTJ</ta>
            <ta e="T308" id="Seg_8742" s="T307">грязный-EP-2SG.[NOM]</ta>
            <ta e="T309" id="Seg_8743" s="T308">туча.[NOM]</ta>
            <ta e="T310" id="Seg_8744" s="T309">плохая.дума-EP-2SG.[NOM]</ta>
            <ta e="T311" id="Seg_8745" s="T310">луна-DAT/LOC</ta>
            <ta e="T312" id="Seg_8746" s="T311">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T313" id="Seg_8747" s="T312">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T314" id="Seg_8748" s="T313">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T315" id="Seg_8749" s="T314">дымить-PRS.[3SG]</ta>
            <ta e="T316" id="Seg_8750" s="T315">вокруг</ta>
            <ta e="T317" id="Seg_8751" s="T316">три-MLTP</ta>
            <ta e="T318" id="Seg_8752" s="T317">раз.[NOM]</ta>
            <ta e="T319" id="Seg_8753" s="T318">потом</ta>
            <ta e="T320" id="Seg_8754" s="T319">жена-ACC</ta>
            <ta e="T321" id="Seg_8755" s="T320">сидеть-PTCP.PRS</ta>
            <ta e="T322" id="Seg_8756" s="T321">жена-ACC</ta>
            <ta e="T323" id="Seg_8757" s="T322">сидеть-PTCP.PRS</ta>
            <ta e="T324" id="Seg_8758" s="T323">еще.не-3SG</ta>
            <ta e="T325" id="Seg_8759" s="T324">подмышка-3SG-GEN</ta>
            <ta e="T326" id="Seg_8760" s="T325">нижняя.часть-3SG-INSTR</ta>
            <ta e="T327" id="Seg_8761" s="T326">легкий-ADVZ</ta>
            <ta e="T328" id="Seg_8762" s="T327">сидеть-PTCP.FUT-3SG-ACC</ta>
            <ta e="T329" id="Seg_8763" s="T328">говорить-CVB.SEQ</ta>
            <ta e="T330" id="Seg_8764" s="T329">дымить-PRS.[3SG]</ta>
            <ta e="T331" id="Seg_8765" s="T330">тот.EMPH.[NOM]</ta>
            <ta e="T332" id="Seg_8766" s="T331">дымить-PTCP.PRS</ta>
            <ta e="T333" id="Seg_8767" s="T332">вещь-3SG-INSTR</ta>
            <ta e="T334" id="Seg_8768" s="T333">вокруг</ta>
            <ta e="T335" id="Seg_8769" s="T334">солнце.[NOM]</ta>
            <ta e="T336" id="Seg_8770" s="T335">в.сторону</ta>
            <ta e="T337" id="Seg_8771" s="T336">задняя.часть-3SG-DAT/LOC</ta>
            <ta e="T338" id="Seg_8772" s="T337">идти-NEG.[3SG]</ta>
            <ta e="T339" id="Seg_8773" s="T338">задняя.часть-3SG-ACC</ta>
            <ta e="T340" id="Seg_8774" s="T339">к</ta>
            <ta e="T341" id="Seg_8775" s="T340">кто-ACC</ta>
            <ta e="T342" id="Seg_8776" s="T341">NEG</ta>
            <ta e="T343" id="Seg_8777" s="T342">идти-CAUS-PTCP.HAB-3SG</ta>
            <ta e="T344" id="Seg_8778" s="T343">нет-3PL</ta>
            <ta e="T345" id="Seg_8779" s="T344">беременная.[NOM]</ta>
            <ta e="T346" id="Seg_8780" s="T345">жена-ACC</ta>
            <ta e="T347" id="Seg_8781" s="T346">совсем</ta>
            <ta e="T348" id="Seg_8782" s="T347">задняя.часть-EP-VBZ-FUT-3SG</ta>
            <ta e="T349" id="Seg_8783" s="T348">думать-CVB.SEQ-3PL</ta>
            <ta e="T350" id="Seg_8784" s="T349">тот.[NOM]</ta>
            <ta e="T351" id="Seg_8785" s="T350">предназначение.[NOM]</ta>
            <ta e="T352" id="Seg_8786" s="T351">тогда</ta>
            <ta e="T353" id="Seg_8787" s="T352">однако</ta>
            <ta e="T354" id="Seg_8788" s="T353">MOD</ta>
            <ta e="T355" id="Seg_8789" s="T354">этот</ta>
            <ta e="T357" id="Seg_8790" s="T356">три-MLTP</ta>
            <ta e="T358" id="Seg_8791" s="T357">раз.[NOM]</ta>
            <ta e="T359" id="Seg_8792" s="T358">вертеться-CAUS-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_8793" s="T359">опять</ta>
            <ta e="T361" id="Seg_8794" s="T360">INTJ</ta>
            <ta e="T362" id="Seg_8795" s="T361">INTJ</ta>
            <ta e="T363" id="Seg_8796" s="T362">INTJ</ta>
            <ta e="T364" id="Seg_8797" s="T363">счастливый.[NOM]</ta>
            <ta e="T365" id="Seg_8798" s="T364">жить-PTCP.FUT-3SG-ACC</ta>
            <ta e="T366" id="Seg_8799" s="T365">дочь-EP-1SG.[NOM]</ta>
            <ta e="T367" id="Seg_8800" s="T366">ребенок-1SG.[NOM]</ta>
            <ta e="T368" id="Seg_8801" s="T367">INTJ</ta>
            <ta e="T369" id="Seg_8802" s="T368">INTJ</ta>
            <ta e="T370" id="Seg_8803" s="T369">INTJ</ta>
            <ta e="T371" id="Seg_8804" s="T370">счастливый.[NOM]</ta>
            <ta e="T372" id="Seg_8805" s="T371">жить-PTCP.FUT-3SG-ACC</ta>
            <ta e="T373" id="Seg_8806" s="T372">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T374" id="Seg_8807" s="T373">INTJ</ta>
            <ta e="T375" id="Seg_8808" s="T374">INTJ</ta>
            <ta e="T376" id="Seg_8809" s="T375">INTJ</ta>
            <ta e="T377" id="Seg_8810" s="T376">плохая.дума-EP-2SG.[NOM]</ta>
            <ta e="T378" id="Seg_8811" s="T377">луна-DAT/LOC</ta>
            <ta e="T379" id="Seg_8812" s="T378">грязный-EP-2SG.[NOM]</ta>
            <ta e="T380" id="Seg_8813" s="T379">туча-DAT/LOC</ta>
            <ta e="T381" id="Seg_8814" s="T380">плохая.дума-EP-2SG.[NOM]</ta>
            <ta e="T382" id="Seg_8815" s="T381">луна-DAT/LOC</ta>
            <ta e="T383" id="Seg_8816" s="T382">грязный-EP-2SG.[NOM]</ta>
            <ta e="T384" id="Seg_8817" s="T383">туча-DAT/LOC</ta>
            <ta e="T385" id="Seg_8818" s="T384">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T386" id="Seg_8819" s="T385">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T387" id="Seg_8820" s="T386">говорить-HAB-3PL</ta>
            <ta e="T388" id="Seg_8821" s="T387">три-MLTP</ta>
            <ta e="T389" id="Seg_8822" s="T388">раз.[NOM]</ta>
            <ta e="T390" id="Seg_8823" s="T389">вертеться-CAUS-HAB-3PL</ta>
            <ta e="T391" id="Seg_8824" s="T390">тот</ta>
            <ta e="T392" id="Seg_8825" s="T391">делать-CVB.SEQ</ta>
            <ta e="T393" id="Seg_8826" s="T392">кол.для.рождения-3SG-ABL</ta>
            <ta e="T394" id="Seg_8827" s="T393">держать-REFL-PRS.[3SG]</ta>
            <ta e="T395" id="Seg_8828" s="T394">тот</ta>
            <ta e="T396" id="Seg_8829" s="T395">сидеть-PTCP.PRS</ta>
            <ta e="T397" id="Seg_8830" s="T396">жена-EP-2SG.[NOM]</ta>
            <ta e="T398" id="Seg_8831" s="T397">сидеть-PTCP.PRS</ta>
            <ta e="T399" id="Seg_8832" s="T398">кол.для.рождения-3SG-ABL</ta>
            <ta e="T400" id="Seg_8833" s="T399">тогда</ta>
            <ta e="T401" id="Seg_8834" s="T400">однако</ta>
            <ta e="T402" id="Seg_8835" s="T401">тотем.[NOM]</ta>
            <ta e="T403" id="Seg_8836" s="T402">взять-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_8837" s="T403">повитуха.[NOM]</ta>
            <ta e="T405" id="Seg_8838" s="T404">старуха.[NOM]</ta>
            <ta e="T406" id="Seg_8839" s="T405">тот</ta>
            <ta e="T407" id="Seg_8840" s="T406">тотем-3SG-ACC</ta>
            <ta e="T408" id="Seg_8841" s="T407">взять-PRS.[3SG]</ta>
            <ta e="T409" id="Seg_8842" s="T408">заяц.[NOM]</ta>
            <ta e="T410" id="Seg_8843" s="T409">шкура-3SG.[NOM]</ta>
            <ta e="T411" id="Seg_8844" s="T410">тот</ta>
            <ta e="T412" id="Seg_8845" s="T411">тотем-DIM-3SG.[NOM]</ta>
            <ta e="T413" id="Seg_8846" s="T412">заяц-EP-2SG.[NOM]</ta>
            <ta e="T414" id="Seg_8847" s="T413">тотем.[NOM]</ta>
            <ta e="T415" id="Seg_8848" s="T414">имя-PROPR.[NOM]</ta>
            <ta e="T416" id="Seg_8849" s="T415">быть-HAB.[3SG]</ta>
            <ta e="T417" id="Seg_8850" s="T416">тогда</ta>
            <ta e="T418" id="Seg_8851" s="T417">заяц-ACC</ta>
            <ta e="T419" id="Seg_8852" s="T418">хватать-CVB.SEQ</ta>
            <ta e="T420" id="Seg_8853" s="T419">после</ta>
            <ta e="T421" id="Seg_8854" s="T420">заяц-ACC</ta>
            <ta e="T422" id="Seg_8855" s="T421">огонь-DAT/LOC</ta>
            <ta e="T423" id="Seg_8856" s="T422">есть-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T424" id="Seg_8857" s="T423">огонь-ACC</ta>
            <ta e="T425" id="Seg_8858" s="T424">опять</ta>
            <ta e="T426" id="Seg_8859" s="T425">есть-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T427" id="Seg_8860" s="T426">огонь-дедушка-DIM-EP-1SG.[NOM]</ta>
            <ta e="T428" id="Seg_8861" s="T427">тотем-1SG-ACC</ta>
            <ta e="T429" id="Seg_8862" s="T428">есть-EP-CAUS-PRS-1SG</ta>
            <ta e="T430" id="Seg_8863" s="T429">ребенок-1SG-ACC</ta>
            <ta e="T431" id="Seg_8864" s="T430">счастливый.[NOM]</ta>
            <ta e="T432" id="Seg_8865" s="T431">жить-CAUS-PTCP.FUT-3SG-ACC</ta>
            <ta e="T433" id="Seg_8866" s="T432">счастливый.[NOM]</ta>
            <ta e="T434" id="Seg_8867" s="T433">быть.в.сватках-PTCP.FUT-3SG-ACC</ta>
            <ta e="T435" id="Seg_8868" s="T434">говорить-CVB.SEQ</ta>
            <ta e="T436" id="Seg_8869" s="T435">тогда</ta>
            <ta e="T437" id="Seg_8870" s="T436">однако</ta>
            <ta e="T438" id="Seg_8871" s="T437">огонь.[NOM]</ta>
            <ta e="T439" id="Seg_8872" s="T438">дедушка-DIM-1SG-ACC</ta>
            <ta e="T440" id="Seg_8873" s="T439">есть-EP-CAUS-PRS-1SG</ta>
            <ta e="T441" id="Seg_8874" s="T440">огонь.[NOM]</ta>
            <ta e="T442" id="Seg_8875" s="T441">дедушка-DIM-EP-1SG.[NOM]</ta>
            <ta e="T443" id="Seg_8876" s="T442">есть.[IMP.2SG]</ta>
            <ta e="T444" id="Seg_8877" s="T443">счастливый</ta>
            <ta e="T445" id="Seg_8878" s="T444">счастье-PART</ta>
            <ta e="T446" id="Seg_8879" s="T445">охранять.[IMP.2SG]</ta>
            <ta e="T447" id="Seg_8880" s="T446">что.[NOM]</ta>
            <ta e="T448" id="Seg_8881" s="T447">NEG</ta>
            <ta e="T449" id="Seg_8882" s="T448">плохой-ACC</ta>
            <ta e="T450" id="Seg_8883" s="T449">приближаться-CAUS-EP-NEG.[IMP.2SG]</ta>
            <ta e="T451" id="Seg_8884" s="T450">счастливый</ta>
            <ta e="T452" id="Seg_8885" s="T451">жить-CAUS.[IMP.2SG]</ta>
            <ta e="T453" id="Seg_8886" s="T452">ребенок-1SG-ACC</ta>
            <ta e="T454" id="Seg_8887" s="T453">счастливый</ta>
            <ta e="T455" id="Seg_8888" s="T454">освободить.[IMP.2SG]</ta>
            <ta e="T456" id="Seg_8889" s="T455">говорить-CVB.SEQ</ta>
            <ta e="T457" id="Seg_8890" s="T456">потом</ta>
            <ta e="T458" id="Seg_8891" s="T457">тот</ta>
            <ta e="T459" id="Seg_8892" s="T458">тотем-DIM-3SG-ACC</ta>
            <ta e="T460" id="Seg_8893" s="T459">есть-EP-CAUS-CVB.SEQ</ta>
            <ta e="T461" id="Seg_8894" s="T460">после</ta>
            <ta e="T462" id="Seg_8895" s="T461">кол.для.рождения-DAT/LOC</ta>
            <ta e="T463" id="Seg_8896" s="T462">повесить-PRS.[3SG]</ta>
            <ta e="T464" id="Seg_8897" s="T463">тот</ta>
            <ta e="T465" id="Seg_8898" s="T464">кол.для.рождения-DAT/LOC</ta>
            <ta e="T466" id="Seg_8899" s="T465">связывать-CVB.SEQ</ta>
            <ta e="T467" id="Seg_8900" s="T466">бросать-PRS.[3SG]</ta>
            <ta e="T468" id="Seg_8901" s="T467">тот</ta>
            <ta e="T469" id="Seg_8902" s="T468">связывать-CVB.SEQ</ta>
            <ta e="T470" id="Seg_8903" s="T469">бросать-CVB.SEQ</ta>
            <ta e="T471" id="Seg_8904" s="T470">после</ta>
            <ta e="T472" id="Seg_8905" s="T471">тот</ta>
            <ta e="T473" id="Seg_8906" s="T472">родить-PTCP.PRS</ta>
            <ta e="T474" id="Seg_8907" s="T473">жена.[NOM]</ta>
            <ta e="T475" id="Seg_8908" s="T474">мочь-CVB.SEQ</ta>
            <ta e="T476" id="Seg_8909" s="T475">тот</ta>
            <ta e="T477" id="Seg_8910" s="T476">плохой-ADVZ</ta>
            <ta e="T478" id="Seg_8911" s="T477">расти-MED-PTCP.PRS</ta>
            <ta e="T479" id="Seg_8912" s="T478">быть-TEMP-3SG</ta>
            <ta e="T480" id="Seg_8913" s="T479">помогать-PRS.[3SG]</ta>
            <ta e="T481" id="Seg_8914" s="T480">повитуха.[NOM]</ta>
            <ta e="T482" id="Seg_8915" s="T481">колено-3SG-DAT/LOC</ta>
            <ta e="T483" id="Seg_8916" s="T482">посадить-PRS.[3SG]</ta>
            <ta e="T484" id="Seg_8917" s="T483">хвост-3SG-ACC</ta>
            <ta e="T485" id="Seg_8918" s="T484">корень-3SG-INSTR</ta>
            <ta e="T486" id="Seg_8919" s="T485">ребенок-3SG.[NOM]</ta>
            <ta e="T487" id="Seg_8920" s="T486">задняя.часть-VBZ-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T488" id="Seg_8921" s="T487">говорить-CVB.SEQ</ta>
            <ta e="T489" id="Seg_8922" s="T488">потом</ta>
            <ta e="T490" id="Seg_8923" s="T489">живот-3SG-ACC</ta>
            <ta e="T491" id="Seg_8924" s="T490">гладить-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_8925" s="T491">тот</ta>
            <ta e="T493" id="Seg_8926" s="T492">живот-3SG-ACC</ta>
            <ta e="T494" id="Seg_8927" s="T493">гладить-CVB.SEQ</ta>
            <ta e="T495" id="Seg_8928" s="T494">помогать-CVB.SEQ</ta>
            <ta e="T496" id="Seg_8929" s="T495">тот</ta>
            <ta e="T497" id="Seg_8930" s="T496">ребенок-3SG-ACC</ta>
            <ta e="T498" id="Seg_8931" s="T497">помогать-CVB.SEQ</ta>
            <ta e="T499" id="Seg_8932" s="T498">родить-PRS.[3SG]</ta>
            <ta e="T500" id="Seg_8933" s="T499">тот</ta>
            <ta e="T501" id="Seg_8934" s="T500">жена.[NOM]</ta>
            <ta e="T502" id="Seg_8935" s="T501">тот</ta>
            <ta e="T503" id="Seg_8936" s="T502">родить-PTCP.PRS</ta>
            <ta e="T504" id="Seg_8937" s="T503">имя-3SG.[NOM]</ta>
            <ta e="T505" id="Seg_8938" s="T504">ребенок.[NOM]</ta>
            <ta e="T506" id="Seg_8939" s="T505">родить-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T507" id="Seg_8940" s="T506">тот</ta>
            <ta e="T508" id="Seg_8941" s="T507">быть.в.сватках-CVB.SEQ</ta>
            <ta e="T509" id="Seg_8942" s="T508">ребенок.[NOM]</ta>
            <ta e="T510" id="Seg_8943" s="T509">падать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T511" id="Seg_8944" s="T510">тот</ta>
            <ta e="T512" id="Seg_8945" s="T511">ребенок-ACC</ta>
            <ta e="T513" id="Seg_8946" s="T512">пуп-3SG-ACC</ta>
            <ta e="T514" id="Seg_8947" s="T513">резать-PRS-3PL</ta>
            <ta e="T515" id="Seg_8948" s="T514">резать-PRS-1PL</ta>
            <ta e="T516" id="Seg_8949" s="T515">тот</ta>
            <ta e="T517" id="Seg_8950" s="T516">повитуха.[NOM]</ta>
            <ta e="T518" id="Seg_8951" s="T517">старуха.[NOM]</ta>
            <ta e="T519" id="Seg_8952" s="T518">резать-PRS.[3SG]</ta>
            <ta e="T520" id="Seg_8953" s="T519">тот</ta>
            <ta e="T521" id="Seg_8954" s="T520">пуп-3SG-ACC</ta>
            <ta e="T522" id="Seg_8955" s="T521">резать-CVB.SEQ</ta>
            <ta e="T523" id="Seg_8956" s="T522">после</ta>
            <ta e="T524" id="Seg_8957" s="T523">MOD</ta>
            <ta e="T525" id="Seg_8958" s="T524">связывать-CVB.SEQ</ta>
            <ta e="T526" id="Seg_8959" s="T525">бросать-PRS.[3SG]</ta>
            <ta e="T527" id="Seg_8960" s="T526">сухожилие.[NOM]</ta>
            <ta e="T528" id="Seg_8961" s="T527">нить-EP-INSTR</ta>
            <ta e="T529" id="Seg_8962" s="T528">высыхать-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T530" id="Seg_8963" s="T529">сухожилие.[NOM]</ta>
            <ta e="T531" id="Seg_8964" s="T530">нить-EP-INSTR</ta>
            <ta e="T532" id="Seg_8965" s="T531">связывать-PRS.[3SG]</ta>
            <ta e="T533" id="Seg_8966" s="T532">отделяться-CVB.SEQ</ta>
            <ta e="T534" id="Seg_8967" s="T533">падать-PTCP.FUT-3SG-ACC</ta>
            <ta e="T535" id="Seg_8968" s="T534">позже</ta>
            <ta e="T536" id="Seg_8969" s="T535">сквозь</ta>
            <ta e="T537" id="Seg_8970" s="T536">гнить-CVB.SEQ</ta>
            <ta e="T538" id="Seg_8971" s="T537">пять</ta>
            <ta e="T539" id="Seg_8972" s="T538">день.[NOM]</ta>
            <ta e="T540" id="Seg_8973" s="T539">Q</ta>
            <ta e="T541" id="Seg_8974" s="T540">сколько</ta>
            <ta e="T542" id="Seg_8975" s="T541">Q</ta>
            <ta e="T543" id="Seg_8976" s="T542">день.[NOM]</ta>
            <ta e="T544" id="Seg_8977" s="T543">быть-CVB.SEQ</ta>
            <ta e="T545" id="Seg_8978" s="T544">после</ta>
            <ta e="T546" id="Seg_8979" s="T545">падать-HAB.[3SG]</ta>
            <ta e="T547" id="Seg_8980" s="T546">ребенок.[NOM]</ta>
            <ta e="T548" id="Seg_8981" s="T547">пуп-3SG.[NOM]</ta>
            <ta e="T549" id="Seg_8982" s="T548">потом</ta>
            <ta e="T550" id="Seg_8983" s="T549">однако</ta>
            <ta e="T551" id="Seg_8984" s="T550">MOD</ta>
            <ta e="T552" id="Seg_8985" s="T551">ребенок-3SG-GEN</ta>
            <ta e="T553" id="Seg_8986" s="T552">мать-3SG.[NOM]</ta>
            <ta e="T554" id="Seg_8987" s="T553">падать-PTCP.PST-3SG-GEN</ta>
            <ta e="T555" id="Seg_8988" s="T554">тот</ta>
            <ta e="T556" id="Seg_8989" s="T555">ребенок-3SG-GEN</ta>
            <ta e="T557" id="Seg_8990" s="T556">мать-3SG-ACC</ta>
            <ta e="T558" id="Seg_8991" s="T557">обмыть-PRS.[3SG]</ta>
            <ta e="T559" id="Seg_8992" s="T558">ребенок-3SG-ACC</ta>
            <ta e="T560" id="Seg_8993" s="T559">тоже</ta>
            <ta e="T561" id="Seg_8994" s="T560">обмыть-PRS.[3SG]</ta>
            <ta e="T562" id="Seg_8995" s="T561">когда</ta>
            <ta e="T563" id="Seg_8996" s="T562">EMPH</ta>
            <ta e="T564" id="Seg_8997" s="T563">обмыть-CVB.SEQ</ta>
            <ta e="T565" id="Seg_8998" s="T564">после</ta>
            <ta e="T566" id="Seg_8999" s="T565">укутать-CVB.SEQ</ta>
            <ta e="T567" id="Seg_9000" s="T566">бросать-PRS.[3SG]</ta>
            <ta e="T568" id="Seg_9001" s="T567">MOD</ta>
            <ta e="T569" id="Seg_9002" s="T568">потом</ta>
            <ta e="T570" id="Seg_9003" s="T569">однако</ta>
            <ta e="T571" id="Seg_9004" s="T570">MOD</ta>
            <ta e="T573" id="Seg_9005" s="T572">колыбель-DAT/LOC</ta>
            <ta e="T574" id="Seg_9006" s="T573">еще</ta>
            <ta e="T575" id="Seg_9007" s="T574">класть-NEG-3PL</ta>
            <ta e="T576" id="Seg_9008" s="T575">MOD</ta>
            <ta e="T577" id="Seg_9009" s="T576">укутать-CVB.SEQ</ta>
            <ta e="T578" id="Seg_9010" s="T577">бросать-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_9011" s="T578">мама-3SG-GEN</ta>
            <ta e="T580" id="Seg_9012" s="T579">место.около-DAT/LOC</ta>
            <ta e="T581" id="Seg_9013" s="T580">класть-PRS.[3SG]</ta>
            <ta e="T582" id="Seg_9014" s="T581">потом</ta>
            <ta e="T583" id="Seg_9015" s="T582">MOD</ta>
            <ta e="T584" id="Seg_9016" s="T583">тотем-3SG-ACC</ta>
            <ta e="T586" id="Seg_9017" s="T585">тотем-3SG-ACC</ta>
            <ta e="T587" id="Seg_9018" s="T586">обмыть-CVB.SEQ</ta>
            <ta e="T588" id="Seg_9019" s="T587">после</ta>
            <ta e="T589" id="Seg_9020" s="T588">тот</ta>
            <ta e="T590" id="Seg_9021" s="T589">тотем-3SG-ACC</ta>
            <ta e="T591" id="Seg_9022" s="T590">мешочек-3SG-DAT/LOC</ta>
            <ta e="T592" id="Seg_9023" s="T591">вставлять-CVB.SEQ</ta>
            <ta e="T593" id="Seg_9024" s="T592">бросать-CVB.SEQ</ta>
            <ta e="T594" id="Seg_9025" s="T593">после</ta>
            <ta e="T595" id="Seg_9026" s="T594">тот</ta>
            <ta e="T596" id="Seg_9027" s="T595">сума-3SG-DAT/LOC</ta>
            <ta e="T597" id="Seg_9028" s="T596">тот</ta>
            <ta e="T598" id="Seg_9029" s="T597">сума-COM</ta>
            <ta e="T599" id="Seg_9030" s="T598">тот</ta>
            <ta e="T601" id="Seg_9031" s="T600">кол.для.рождения-DAT/LOC</ta>
            <ta e="T602" id="Seg_9032" s="T601">связывать-CVB.SEQ</ta>
            <ta e="T603" id="Seg_9033" s="T602">бросать-PRS.[3SG]</ta>
            <ta e="T604" id="Seg_9034" s="T603">тот</ta>
            <ta e="T605" id="Seg_9035" s="T604">кол.для.рождения-DAT/LOC</ta>
            <ta e="T606" id="Seg_9036" s="T605">стоять-PRS.[3SG]</ta>
            <ta e="T607" id="Seg_9037" s="T606">этот</ta>
            <ta e="T608" id="Seg_9038" s="T607">ребенок.[NOM]</ta>
            <ta e="T609" id="Seg_9039" s="T608">мать-3SG.[NOM]</ta>
            <ta e="T610" id="Seg_9040" s="T609">жизнь-3SG-ACC</ta>
            <ta e="T611" id="Seg_9041" s="T610">так.долго</ta>
            <ta e="T612" id="Seg_9042" s="T611">этот</ta>
            <ta e="T613" id="Seg_9043" s="T612">ребенок.[NOM]</ta>
            <ta e="T614" id="Seg_9044" s="T613">родиться-PTCP.PST</ta>
            <ta e="T615" id="Seg_9045" s="T614">чум-3SG-GEN</ta>
            <ta e="T616" id="Seg_9046" s="T615">нутро-3SG-DAT/LOC</ta>
            <ta e="T617" id="Seg_9047" s="T616">ребенок.[NOM]</ta>
            <ta e="T618" id="Seg_9048" s="T617">родиться-PTCP.PST</ta>
            <ta e="T619" id="Seg_9049" s="T618">кол.для.рождения-3SG-DAT/LOC</ta>
            <ta e="T620" id="Seg_9050" s="T619">жена.[NOM]</ta>
            <ta e="T621" id="Seg_9051" s="T620">родить-PTCP.PST</ta>
            <ta e="T622" id="Seg_9052" s="T621">кол.для.рождения-3SG-DAT/LOC</ta>
            <ta e="T623" id="Seg_9053" s="T622">тогда</ta>
            <ta e="T624" id="Seg_9054" s="T623">говорить-HAB-3PL</ta>
            <ta e="T625" id="Seg_9055" s="T624">давно-ADJZ</ta>
            <ta e="T626" id="Seg_9056" s="T625">время-ABL</ta>
            <ta e="T627" id="Seg_9057" s="T626">кол.для.рождения-PROPR.[NOM]</ta>
            <ta e="T628" id="Seg_9058" s="T627">стоянка-ACC</ta>
            <ta e="T629" id="Seg_9059" s="T628">проехать-TEMP-3PL</ta>
            <ta e="T630" id="Seg_9060" s="T629">чум-3SG.[NOM]</ta>
            <ta e="T631" id="Seg_9061" s="T630">NEG</ta>
            <ta e="T632" id="Seg_9062" s="T631">NEG.EX</ta>
            <ta e="T633" id="Seg_9063" s="T632">быть-TEMP-3SG</ta>
            <ta e="T634" id="Seg_9064" s="T633">кол.для.рождения-ACC</ta>
            <ta e="T635" id="Seg_9065" s="T634">строить-CVB.SIM</ta>
            <ta e="T636" id="Seg_9066" s="T635">стоять-PTCP.PRS-3SG-ACC</ta>
            <ta e="T637" id="Seg_9067" s="T636">видеть-TEMP-3PL</ta>
            <ta e="T638" id="Seg_9068" s="T637">вот</ta>
            <ta e="T639" id="Seg_9069" s="T638">этот</ta>
            <ta e="T640" id="Seg_9070" s="T639">стоянка-DAT/LOC</ta>
            <ta e="T641" id="Seg_9071" s="T640">жена.[NOM]</ta>
            <ta e="T642" id="Seg_9072" s="T641">родиться-PST2.[3SG]</ta>
            <ta e="T643" id="Seg_9073" s="T642">быть-PST2.[3SG]</ta>
            <ta e="T644" id="Seg_9074" s="T643">кол.для.рождения-3SG.[NOM]</ta>
            <ta e="T645" id="Seg_9075" s="T644">стоять-PRS.[3SG]</ta>
            <ta e="T646" id="Seg_9076" s="T645">родиться-PTCP.PST</ta>
            <ta e="T647" id="Seg_9077" s="T646">кол.для.рождения-3SG.[NOM]</ta>
            <ta e="T648" id="Seg_9078" s="T647">чум-PROPR.[NOM]</ta>
            <ta e="T649" id="Seg_9079" s="T648">быть-TEMP-3SG</ta>
            <ta e="T650" id="Seg_9080" s="T649">такой</ta>
            <ta e="T651" id="Seg_9081" s="T650">ребенок.[NOM]</ta>
            <ta e="T652" id="Seg_9082" s="T651">родиться-PST2.[3SG]</ta>
            <ta e="T653" id="Seg_9083" s="T652">быть-PST2.[3SG]</ta>
            <ta e="T654" id="Seg_9084" s="T653">тот</ta>
            <ta e="T655" id="Seg_9085" s="T654">ребенок.[NOM]</ta>
            <ta e="T656" id="Seg_9086" s="T655">родиться-PTCP.PST</ta>
            <ta e="T657" id="Seg_9087" s="T656">чум-3SG.[NOM]</ta>
            <ta e="T658" id="Seg_9088" s="T657">говорить-HAB-3PL</ta>
            <ta e="T659" id="Seg_9089" s="T658">тогда</ta>
            <ta e="T660" id="Seg_9090" s="T659">однако</ta>
            <ta e="T661" id="Seg_9091" s="T660">MOD</ta>
            <ta e="T662" id="Seg_9092" s="T661">потом</ta>
            <ta e="T663" id="Seg_9093" s="T662">этот</ta>
            <ta e="T664" id="Seg_9094" s="T663">жена-ACC</ta>
            <ta e="T665" id="Seg_9095" s="T664">ребенок-VBZ-PTCP.PST</ta>
            <ta e="T666" id="Seg_9096" s="T665">жена-EP-2SG.[NOM]</ta>
            <ta e="T667" id="Seg_9097" s="T666">грязный.[NOM]</ta>
            <ta e="T668" id="Seg_9098" s="T667">EMPH</ta>
            <ta e="T669" id="Seg_9099" s="T668">родиться-PTCP.PST</ta>
            <ta e="T670" id="Seg_9100" s="T669">грязный-3SG-ACC</ta>
            <ta e="T671" id="Seg_9101" s="T670">очистить-CVB.SEQ</ta>
            <ta e="T672" id="Seg_9102" s="T671">опять</ta>
            <ta e="T673" id="Seg_9103" s="T672">дымить-PRS-2SG</ta>
            <ta e="T674" id="Seg_9104" s="T673">лицо-3SG-GEN</ta>
            <ta e="T675" id="Seg_9105" s="T674">нижняя.часть-3SG-INSTR</ta>
            <ta e="T676" id="Seg_9106" s="T675">шея-3SG-ACC</ta>
            <ta e="T677" id="Seg_9107" s="T676">вокруг</ta>
            <ta e="T678" id="Seg_9108" s="T677">три-MLTP</ta>
            <ta e="T679" id="Seg_9109" s="T678">раз.[NOM]</ta>
            <ta e="T680" id="Seg_9110" s="T679">дымить-PRS-2SG</ta>
            <ta e="T681" id="Seg_9111" s="T680">INTJ</ta>
            <ta e="T682" id="Seg_9112" s="T681">INTJ</ta>
            <ta e="T683" id="Seg_9113" s="T682">грязный-EP-2SG.[NOM]</ta>
            <ta e="T684" id="Seg_9114" s="T683">туча-DAT/LOC</ta>
            <ta e="T685" id="Seg_9115" s="T684">плохая.дума-EP-2SG.[NOM]</ta>
            <ta e="T686" id="Seg_9116" s="T685">луна-DAT/LOC</ta>
            <ta e="T687" id="Seg_9117" s="T686">INTJ</ta>
            <ta e="T688" id="Seg_9118" s="T687">INTJ</ta>
            <ta e="T689" id="Seg_9119" s="T688">грязный-EP-2SG.[NOM]</ta>
            <ta e="T690" id="Seg_9120" s="T689">туча-DAT/LOC</ta>
            <ta e="T691" id="Seg_9121" s="T690">плохая.дума-EP-2SG.[NOM]</ta>
            <ta e="T692" id="Seg_9122" s="T691">луна-DAT/LOC</ta>
            <ta e="T693" id="Seg_9123" s="T692">INTJ-INTJ</ta>
            <ta e="T694" id="Seg_9124" s="T693">грязный-3SG-ACC</ta>
            <ta e="T695" id="Seg_9125" s="T694">туча-DAT/LOC</ta>
            <ta e="T696" id="Seg_9126" s="T695">плохая.дума-EP-2SG.[NOM]</ta>
            <ta e="T697" id="Seg_9127" s="T696">луна-DAT/LOC</ta>
            <ta e="T698" id="Seg_9128" s="T697">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T699" id="Seg_9129" s="T698">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T700" id="Seg_9130" s="T699">%%-EP-IMP.2PL-%%-EP-IMP.2PL</ta>
            <ta e="T701" id="Seg_9131" s="T700">говорить-CVB.SEQ-2SG</ta>
            <ta e="T702" id="Seg_9132" s="T701">говорить-CVB.SEQ-2SG</ta>
            <ta e="T703" id="Seg_9133" s="T702">дымить-PRS-2SG</ta>
            <ta e="T704" id="Seg_9134" s="T703">тот</ta>
            <ta e="T705" id="Seg_9135" s="T704">тот</ta>
            <ta e="T706" id="Seg_9136" s="T705">делать-CVB.SEQ</ta>
            <ta e="T707" id="Seg_9137" s="T706">после</ta>
            <ta e="T708" id="Seg_9138" s="T707">ребенок-3SG-ACC</ta>
            <ta e="T709" id="Seg_9139" s="T708">взять-PRS.[3SG]</ta>
            <ta e="T710" id="Seg_9140" s="T709">повитуха.[NOM]</ta>
            <ta e="T711" id="Seg_9141" s="T710">потом</ta>
            <ta e="T712" id="Seg_9142" s="T711">MOD</ta>
            <ta e="T713" id="Seg_9143" s="T712">жена-ACC</ta>
            <ta e="T714" id="Seg_9144" s="T713">стоять-CAUS-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_9145" s="T714">лежать-PTCP.PRS</ta>
            <ta e="T716" id="Seg_9146" s="T715">постельное.белье-3SG-ABL</ta>
            <ta e="T717" id="Seg_9147" s="T716">тот</ta>
            <ta e="T718" id="Seg_9148" s="T717">родиться-PTCP.PST</ta>
            <ta e="T719" id="Seg_9149" s="T718">чум-3SG-DAT/LOC</ta>
            <ta e="T720" id="Seg_9150" s="T719">потом</ta>
            <ta e="T721" id="Seg_9151" s="T720">вставать-CVB.SEQ</ta>
            <ta e="T722" id="Seg_9152" s="T721">вынимать-PRS-1PL</ta>
            <ta e="T723" id="Seg_9153" s="T722">дом-1PL-DAT/LOC</ta>
            <ta e="T724" id="Seg_9154" s="T723">идти-PRS-1PL</ta>
            <ta e="T725" id="Seg_9155" s="T724">водить-CVB.SEQ</ta>
            <ta e="T726" id="Seg_9156" s="T725">уносить-PRS-1PL</ta>
            <ta e="T727" id="Seg_9157" s="T726">родиться-PTCP.PST</ta>
            <ta e="T728" id="Seg_9158" s="T727">жена-ACC</ta>
            <ta e="T729" id="Seg_9159" s="T728">ребенок-VBZ-PTCP.PST</ta>
            <ta e="T730" id="Seg_9160" s="T729">жена-ACC</ta>
            <ta e="T731" id="Seg_9161" s="T730">тот</ta>
            <ta e="T732" id="Seg_9162" s="T731">делать-CVB.SEQ</ta>
            <ta e="T733" id="Seg_9163" s="T732">после</ta>
            <ta e="T734" id="Seg_9164" s="T733">тот</ta>
            <ta e="T735" id="Seg_9165" s="T734">водить-CVB.SEQ</ta>
            <ta e="T736" id="Seg_9166" s="T735">быть-CVB.SEQ</ta>
            <ta e="T737" id="Seg_9167" s="T736">тот</ta>
            <ta e="T738" id="Seg_9168" s="T737">рассказывать-PRS.[3SG]</ta>
            <ta e="T739" id="Seg_9169" s="T738">тот</ta>
            <ta e="T740" id="Seg_9170" s="T739">эй</ta>
            <ta e="T741" id="Seg_9171" s="T740">ребенок.[NOM]</ta>
            <ta e="T742" id="Seg_9172" s="T741">родиться-PST1-3SG</ta>
            <ta e="T743" id="Seg_9173" s="T742">говорить-CVB.SEQ</ta>
            <ta e="T744" id="Seg_9174" s="T743">семья-3PL-DAT/LOC</ta>
            <ta e="T745" id="Seg_9175" s="T744">чум-PROPR</ta>
            <ta e="T746" id="Seg_9176" s="T745">чум-3PL-DAT/LOC</ta>
            <ta e="T747" id="Seg_9177" s="T746">чум-PROPR</ta>
            <ta e="T748" id="Seg_9178" s="T747">шест.[NOM]</ta>
            <ta e="T749" id="Seg_9179" s="T748">чум-DAT/LOC</ta>
            <ta e="T750" id="Seg_9180" s="T749">тот</ta>
            <ta e="T751" id="Seg_9181" s="T750">рассказывать-CVB.SEQ</ta>
            <ta e="T752" id="Seg_9182" s="T751">рассказывать-PRS.[3SG]</ta>
            <ta e="T753" id="Seg_9183" s="T752">ой</ta>
            <ta e="T754" id="Seg_9184" s="T753">тот</ta>
            <ta e="T755" id="Seg_9185" s="T754">девушка.[NOM]</ta>
            <ta e="T756" id="Seg_9186" s="T755">ребенок.[NOM]</ta>
            <ta e="T757" id="Seg_9187" s="T756">родиться-PTCP.PST</ta>
            <ta e="T758" id="Seg_9188" s="T757">быть-TEMP-3SG</ta>
            <ta e="T759" id="Seg_9189" s="T758">девушка.[NOM]</ta>
            <ta e="T760" id="Seg_9190" s="T759">ребенок.[NOM]</ta>
            <ta e="T761" id="Seg_9191" s="T760">говорить-PRS-3PL</ta>
            <ta e="T762" id="Seg_9192" s="T761">мальчик.[NOM]</ta>
            <ta e="T763" id="Seg_9193" s="T762">ребенок.[NOM]</ta>
            <ta e="T764" id="Seg_9194" s="T763">родиться-PTCP.PST</ta>
            <ta e="T765" id="Seg_9195" s="T764">быть-TEMP-3SG</ta>
            <ta e="T766" id="Seg_9196" s="T765">мальчик.[NOM]</ta>
            <ta e="T767" id="Seg_9197" s="T766">ребенок.[NOM]</ta>
            <ta e="T768" id="Seg_9198" s="T767">говорить-PRS-3PL</ta>
            <ta e="T769" id="Seg_9199" s="T768">потом</ta>
            <ta e="T770" id="Seg_9200" s="T769">этот</ta>
            <ta e="T771" id="Seg_9201" s="T770">три</ta>
            <ta e="T772" id="Seg_9202" s="T771">день.[NOM]</ta>
            <ta e="T773" id="Seg_9203" s="T772">быть-CVB.SEQ</ta>
            <ta e="T774" id="Seg_9204" s="T773">после</ta>
            <ta e="T775" id="Seg_9205" s="T774">отец-3SG.[NOM]</ta>
            <ta e="T776" id="Seg_9206" s="T775">колыбель-ACC</ta>
            <ta e="T777" id="Seg_9207" s="T776">делать-PRS.[3SG]</ta>
            <ta e="T778" id="Seg_9208" s="T777">ребенок-3SG-DAT/LOC</ta>
            <ta e="T779" id="Seg_9209" s="T778">дедушка-3SG.[NOM]</ta>
            <ta e="T780" id="Seg_9210" s="T779">помогать-PRS.[3SG]</ta>
            <ta e="T781" id="Seg_9211" s="T780">тогда</ta>
            <ta e="T782" id="Seg_9212" s="T781">EMPH</ta>
            <ta e="T783" id="Seg_9213" s="T782">этот</ta>
            <ta e="T784" id="Seg_9214" s="T783">колыбель-ACC</ta>
            <ta e="T785" id="Seg_9215" s="T784">MOD</ta>
            <ta e="T786" id="Seg_9216" s="T785">попадать.в-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T787" id="Seg_9217" s="T786">крепкий</ta>
            <ta e="T788" id="Seg_9218" s="T787">доска-PL-EP-INSTR</ta>
            <ta e="T789" id="Seg_9219" s="T788">веревка-INSTR</ta>
            <ta e="T790" id="Seg_9220" s="T789">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T791" id="Seg_9221" s="T790">веревка-INSTR</ta>
            <ta e="T792" id="Seg_9222" s="T791">скреплять-CVB.SEQ</ta>
            <ta e="T793" id="Seg_9223" s="T792">делать-HAB-3PL</ta>
            <ta e="T794" id="Seg_9224" s="T793">ель-ACC</ta>
            <ta e="T795" id="Seg_9225" s="T794">этот</ta>
            <ta e="T796" id="Seg_9226" s="T795">кто.[NOM]</ta>
            <ta e="T797" id="Seg_9227" s="T796">ээ</ta>
            <ta e="T798" id="Seg_9228" s="T797">фарнер.[NOM]</ta>
            <ta e="T799" id="Seg_9229" s="T798">подобно</ta>
            <ta e="T800" id="Seg_9230" s="T799">говорить-HAB-3PL</ta>
            <ta e="T801" id="Seg_9231" s="T800">тот.[NOM]</ta>
            <ta e="T802" id="Seg_9232" s="T801">такой-ACC</ta>
            <ta e="T803" id="Seg_9233" s="T802">MOD</ta>
            <ta e="T804" id="Seg_9234" s="T803">делать-CAUS-PTCP.HAB-3SG</ta>
            <ta e="T805" id="Seg_9235" s="T804">нет-3PL</ta>
            <ta e="T806" id="Seg_9236" s="T805">высыхать-PTCP.PST</ta>
            <ta e="T807" id="Seg_9237" s="T806">дерево.[NOM]</ta>
            <ta e="T808" id="Seg_9238" s="T807">шкура-3SG.[NOM]</ta>
            <ta e="T809" id="Seg_9239" s="T808">тот.[NOM]</ta>
            <ta e="T810" id="Seg_9240" s="T809">фарнер.[NOM]</ta>
            <ta e="T811" id="Seg_9241" s="T810">тот.[NOM]</ta>
            <ta e="T812" id="Seg_9242" s="T811">из_за</ta>
            <ta e="T813" id="Seg_9243" s="T812">однако</ta>
            <ta e="T814" id="Seg_9244" s="T813">MOD</ta>
            <ta e="T815" id="Seg_9245" s="T814">ребенок.[NOM]</ta>
            <ta e="T816" id="Seg_9246" s="T815">высыхать-FUT.[3SG]</ta>
            <ta e="T817" id="Seg_9247" s="T816">говорить-HAB-3PL</ta>
            <ta e="T818" id="Seg_9248" s="T817">тот.[NOM]</ta>
            <ta e="T819" id="Seg_9249" s="T818">из_за</ta>
            <ta e="T0" id="Seg_9250" s="T820">доска-INSTR</ta>
            <ta e="T822" id="Seg_9251" s="T821">а</ta>
            <ta e="T823" id="Seg_9252" s="T822">доска-2SG.[NOM]</ta>
            <ta e="T824" id="Seg_9253" s="T823">скреплять-EP-PASS/REFL-TEMP-3SG</ta>
            <ta e="T825" id="Seg_9254" s="T824">Q</ta>
            <ta e="T826" id="Seg_9255" s="T825">скреплять-EP-PASS/REFL-TEMP-3SG</ta>
            <ta e="T827" id="Seg_9256" s="T826">скреплять-EP-PASS/REFL-TEMP-3SG</ta>
            <ta e="T828" id="Seg_9257" s="T827">веревка-INSTR</ta>
            <ta e="T829" id="Seg_9258" s="T828">скреплять-CVB.SEQ</ta>
            <ta e="T830" id="Seg_9259" s="T829">делать-TEMP-3PL</ta>
            <ta e="T831" id="Seg_9260" s="T830">тот-3SG-2SG.[NOM]</ta>
            <ta e="T832" id="Seg_9261" s="T831">промежуток-PROPR.[NOM]</ta>
            <ta e="T833" id="Seg_9262" s="T832">быть-HAB.[3SG]</ta>
            <ta e="T834" id="Seg_9263" s="T833">доска.[NOM]</ta>
            <ta e="T835" id="Seg_9264" s="T834">доска-3SG-ABL</ta>
            <ta e="T836" id="Seg_9265" s="T835">опять</ta>
            <ta e="T837" id="Seg_9266" s="T836">промежуток-PROPR.[NOM]</ta>
            <ta e="T838" id="Seg_9267" s="T837">быть-HAB.[3SG]</ta>
            <ta e="T839" id="Seg_9268" s="T838">тогда</ta>
            <ta e="T840" id="Seg_9269" s="T839">дыхание.[NOM]</ta>
            <ta e="T841" id="Seg_9270" s="T840">входить-PRS.[3SG]</ta>
            <ta e="T842" id="Seg_9271" s="T841">MOD</ta>
            <ta e="T843" id="Seg_9272" s="T842">тот.[NOM]</ta>
            <ta e="T844" id="Seg_9273" s="T843">из_за</ta>
            <ta e="T845" id="Seg_9274" s="T844">ребенок-2SG.[NOM]</ta>
            <ta e="T846" id="Seg_9275" s="T845">здоровый.[NOM]</ta>
            <ta e="T847" id="Seg_9276" s="T846">рождаться-PRS.[3SG]</ta>
            <ta e="T848" id="Seg_9277" s="T847">MOD</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_9278" s="T1">adv-adv&gt;adj</ta>
            <ta e="T3" id="Seg_9279" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_9280" s="T3">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T5" id="Seg_9281" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_9282" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_9283" s="T6">n-n:(poss).[n:case]</ta>
            <ta e="T8" id="Seg_9284" s="T7">n</ta>
            <ta e="T9" id="Seg_9285" s="T8">adj.[n:case]</ta>
            <ta e="T10" id="Seg_9286" s="T9">n.[n:case]</ta>
            <ta e="T11" id="Seg_9287" s="T10">v-v:ptcp</ta>
            <ta e="T12" id="Seg_9288" s="T11">n-n:poss-n:case</ta>
            <ta e="T13" id="Seg_9289" s="T12">adv-adv&gt;adj</ta>
            <ta e="T14" id="Seg_9290" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_9291" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_9292" s="T15">v-v:mood-v:pred.pn</ta>
            <ta e="T17" id="Seg_9293" s="T16">adv</ta>
            <ta e="T18" id="Seg_9294" s="T17">n.[n:case]</ta>
            <ta e="T19" id="Seg_9295" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_9296" s="T19">v-v:cvb-v:pred.pn</ta>
            <ta e="T21" id="Seg_9297" s="T20">v-v:mood-v:pred.pn</ta>
            <ta e="T22" id="Seg_9298" s="T21">n.[n:case]</ta>
            <ta e="T23" id="Seg_9299" s="T22">v-v:ptcp</ta>
            <ta e="T24" id="Seg_9300" s="T23">n-n:(poss).[n:case]</ta>
            <ta e="T25" id="Seg_9301" s="T24">v-v:ptcp</ta>
            <ta e="T26" id="Seg_9302" s="T25">n-n:(poss).[n:case]</ta>
            <ta e="T27" id="Seg_9303" s="T26">v-v:mood-v:pred.pn</ta>
            <ta e="T28" id="Seg_9304" s="T27">adv</ta>
            <ta e="T29" id="Seg_9305" s="T28">adv</ta>
            <ta e="T30" id="Seg_9306" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_9307" s="T30">dempro</ta>
            <ta e="T32" id="Seg_9308" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_9309" s="T32">v-v:mood-v:pred.pn</ta>
            <ta e="T34" id="Seg_9310" s="T33">n-n:(poss).[n:case]</ta>
            <ta e="T35" id="Seg_9311" s="T34">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_9312" s="T35">n-n:(poss).[n:case]</ta>
            <ta e="T37" id="Seg_9313" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_9314" s="T37">n-n:(poss).[n:case]</ta>
            <ta e="T39" id="Seg_9315" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_9316" s="T39">n-n:(poss).[n:case]</ta>
            <ta e="T41" id="Seg_9317" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_9318" s="T41">v-v:cvb-v:pred.pn</ta>
            <ta e="T43" id="Seg_9319" s="T42">v-v:tense-v:pred.pn</ta>
            <ta e="T44" id="Seg_9320" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_9321" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_9322" s="T45">v-v:cvb</ta>
            <ta e="T47" id="Seg_9323" s="T46">adv</ta>
            <ta e="T48" id="Seg_9324" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_9325" s="T48">adv</ta>
            <ta e="T50" id="Seg_9326" s="T49">interj</ta>
            <ta e="T51" id="Seg_9327" s="T50">n-n:poss-n:case</ta>
            <ta e="T52" id="Seg_9328" s="T51">n-n:poss-n:case</ta>
            <ta e="T53" id="Seg_9329" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_9330" s="T53">v-v&gt;adv</ta>
            <ta e="T55" id="Seg_9331" s="T54">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T56" id="Seg_9332" s="T55">v-v:ptcp</ta>
            <ta e="T57" id="Seg_9333" s="T56">n-n:(poss).[n:case]</ta>
            <ta e="T58" id="Seg_9334" s="T57">interj</ta>
            <ta e="T59" id="Seg_9335" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_9336" s="T59">v-v:mood.[v:pred.pn]</ta>
            <ta e="T61" id="Seg_9337" s="T60">n-n&gt;n-n:(poss).[n:case]</ta>
            <ta e="T62" id="Seg_9338" s="T61">adv</ta>
            <ta e="T63" id="Seg_9339" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_9340" s="T63">dempro</ta>
            <ta e="T65" id="Seg_9341" s="T64">n-n:(poss).[n:case]</ta>
            <ta e="T66" id="Seg_9342" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_9343" s="T66">n-n:poss-n:case</ta>
            <ta e="T68" id="Seg_9344" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_9345" s="T68">v-v:mood.[v:pred.pn]</ta>
            <ta e="T70" id="Seg_9346" s="T69">n.[n:case]</ta>
            <ta e="T71" id="Seg_9347" s="T70">adv</ta>
            <ta e="T72" id="Seg_9348" s="T71">v-v&gt;v-v:cvb</ta>
            <ta e="T73" id="Seg_9349" s="T72">v-v:cvb</ta>
            <ta e="T74" id="Seg_9350" s="T73">v-v:mood.[v:pred.pn]</ta>
            <ta e="T75" id="Seg_9351" s="T74">dempro</ta>
            <ta e="T76" id="Seg_9352" s="T75">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T77" id="Seg_9353" s="T76">n-n:poss-n:case</ta>
            <ta e="T78" id="Seg_9354" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_9355" s="T78">interj</ta>
            <ta e="T80" id="Seg_9356" s="T79">v-v:ptcp</ta>
            <ta e="T81" id="Seg_9357" s="T80">n-n:poss-n:case</ta>
            <ta e="T82" id="Seg_9358" s="T81">n.[n:case]</ta>
            <ta e="T83" id="Seg_9359" s="T82">v-v:mood.[v:pred.pn]</ta>
            <ta e="T84" id="Seg_9360" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_9361" s="T84">dempro</ta>
            <ta e="T86" id="Seg_9362" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_9363" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_9364" s="T87">n.[n:case]</ta>
            <ta e="T89" id="Seg_9365" s="T88">v-v:mood.[v:pred.pn]</ta>
            <ta e="T90" id="Seg_9366" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_9367" s="T90">v-v:mood-v:pred.pn</ta>
            <ta e="T92" id="Seg_9368" s="T91">n-n:poss-n:case</ta>
            <ta e="T93" id="Seg_9369" s="T92">v-v:cvb-v:pred.pn</ta>
            <ta e="T94" id="Seg_9370" s="T93">dempro-pro:case</ta>
            <ta e="T95" id="Seg_9371" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_9372" s="T95">n.[n:case]</ta>
            <ta e="T97" id="Seg_9373" s="T96">v-v:mood-v:temp.pn</ta>
            <ta e="T98" id="Seg_9374" s="T97">n-n:poss-n:case</ta>
            <ta e="T99" id="Seg_9375" s="T98">v-v:tense.[v:pred.pn]</ta>
            <ta e="T100" id="Seg_9376" s="T99">v-v:mood-v:pred.pn</ta>
            <ta e="T101" id="Seg_9377" s="T100">dempro.[pro:case]</ta>
            <ta e="T102" id="Seg_9378" s="T101">post</ta>
            <ta e="T103" id="Seg_9379" s="T102">n-n&gt;adj</ta>
            <ta e="T104" id="Seg_9380" s="T103">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T105" id="Seg_9381" s="T104">post</ta>
            <ta e="T106" id="Seg_9382" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_9383" s="T106">v-v:tense.[v:pred.pn]</ta>
            <ta e="T108" id="Seg_9384" s="T107">v-v:mood-v:pred.pn</ta>
            <ta e="T109" id="Seg_9385" s="T108">interj</ta>
            <ta e="T110" id="Seg_9386" s="T109">n.[n:case]</ta>
            <ta e="T111" id="Seg_9387" s="T110">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T113" id="Seg_9388" s="T112">adv-adv&gt;adj</ta>
            <ta e="T114" id="Seg_9389" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_9390" s="T114">adj-n:case</ta>
            <ta e="T116" id="Seg_9391" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_9392" s="T116">v-v:mood-v:temp.pn</ta>
            <ta e="T118" id="Seg_9393" s="T117">dempro</ta>
            <ta e="T119" id="Seg_9394" s="T118">interj</ta>
            <ta e="T120" id="Seg_9395" s="T119">n.[n:case]</ta>
            <ta e="T121" id="Seg_9396" s="T120">n-n:(poss).[n:case]</ta>
            <ta e="T122" id="Seg_9397" s="T121">adj.[n:case]</ta>
            <ta e="T123" id="Seg_9398" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_9399" s="T123">v</ta>
            <ta e="T125" id="Seg_9400" s="T124">n.[n:case]</ta>
            <ta e="T126" id="Seg_9401" s="T125">v-v&gt;n.[n:case]</ta>
            <ta e="T127" id="Seg_9402" s="T126">v-v&gt;n-n:(poss).[n:case]</ta>
            <ta e="T128" id="Seg_9403" s="T127">n-n:(poss).[n:case]</ta>
            <ta e="T129" id="Seg_9404" s="T128">adj.[n:case]</ta>
            <ta e="T130" id="Seg_9405" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_9406" s="T130">v-v:mood-v:temp.pn</ta>
            <ta e="T132" id="Seg_9407" s="T131">n-n&gt;adj.[n:case]</ta>
            <ta e="T133" id="Seg_9408" s="T132">v-v:cvb-v:pred.pn</ta>
            <ta e="T134" id="Seg_9409" s="T133">v-v:mood-v:pred.pn</ta>
            <ta e="T135" id="Seg_9410" s="T134">v-v:cvb-v:pred.pn</ta>
            <ta e="T136" id="Seg_9411" s="T135">interj</ta>
            <ta e="T137" id="Seg_9412" s="T136">n-n:(poss).[n:case]</ta>
            <ta e="T138" id="Seg_9413" s="T137">adv</ta>
            <ta e="T139" id="Seg_9414" s="T138">adv</ta>
            <ta e="T140" id="Seg_9415" s="T139">dempro</ta>
            <ta e="T141" id="Seg_9416" s="T140">n-n:(poss).[n:case]</ta>
            <ta e="T143" id="Seg_9417" s="T142">v-v:tense-v:poss.pn</ta>
            <ta e="T144" id="Seg_9418" s="T143">v-v:cvb-v:pred.pn</ta>
            <ta e="T145" id="Seg_9419" s="T144">n-n:(poss).[n:case]</ta>
            <ta e="T146" id="Seg_9420" s="T145">v-v:tense-v:poss.pn</ta>
            <ta e="T147" id="Seg_9421" s="T146">v-v:cvb-v:pred.pn</ta>
            <ta e="T148" id="Seg_9422" s="T147">v-v:mood-v:pred.pn</ta>
            <ta e="T149" id="Seg_9423" s="T148">interj</ta>
            <ta e="T150" id="Seg_9424" s="T149">adj.[n:case]</ta>
            <ta e="T151" id="Seg_9425" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_9426" s="T151">v-v:mood-v:temp.pn</ta>
            <ta e="T153" id="Seg_9427" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_9428" s="T153">v-v:mood-v:pred.pn</ta>
            <ta e="T155" id="Seg_9429" s="T154">dempro</ta>
            <ta e="T156" id="Seg_9430" s="T155">n.[n:case]</ta>
            <ta e="T157" id="Seg_9431" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_9432" s="T157">adv</ta>
            <ta e="T159" id="Seg_9433" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_9434" s="T159">dempro</ta>
            <ta e="T161" id="Seg_9435" s="T160">n.[n:case]</ta>
            <ta e="T162" id="Seg_9436" s="T161">n.[n:case]</ta>
            <ta e="T163" id="Seg_9437" s="T162">n-n:(poss).[n:case]</ta>
            <ta e="T164" id="Seg_9438" s="T163">que-pro:(poss).[pro:case]</ta>
            <ta e="T165" id="Seg_9439" s="T164">adj-n:(poss).[n:case]</ta>
            <ta e="T166" id="Seg_9440" s="T165">adj.[n:case]</ta>
            <ta e="T167" id="Seg_9441" s="T166">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T168" id="Seg_9442" s="T167">adv</ta>
            <ta e="T169" id="Seg_9443" s="T168">v-v:mood.[v:pred.pn]</ta>
            <ta e="T170" id="Seg_9444" s="T169">adj-adj&gt;adj-n:(poss).[n:case]</ta>
            <ta e="T171" id="Seg_9445" s="T170">adj.[n:case]</ta>
            <ta e="T172" id="Seg_9446" s="T171">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp</ta>
            <ta e="T173" id="Seg_9447" s="T172">n.[n:case]</ta>
            <ta e="T174" id="Seg_9448" s="T173">v-v:tense-v:mood.[v:pred.pn]</ta>
            <ta e="T175" id="Seg_9449" s="T174">n-n:(poss).[n:case]</ta>
            <ta e="T176" id="Seg_9450" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_9451" s="T176">n.[n:case]</ta>
            <ta e="T178" id="Seg_9452" s="T177">n-n:(poss).[n:case]</ta>
            <ta e="T179" id="Seg_9453" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_9454" s="T179">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T181" id="Seg_9455" s="T180">v</ta>
            <ta e="T182" id="Seg_9456" s="T181">n.[n:case]</ta>
            <ta e="T183" id="Seg_9457" s="T182">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T184" id="Seg_9458" s="T183">adv</ta>
            <ta e="T185" id="Seg_9459" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_9460" s="T185">ptcl</ta>
            <ta e="T187" id="Seg_9461" s="T186">n-n&gt;adj.[n:case]</ta>
            <ta e="T188" id="Seg_9462" s="T187">v-v:mood.[v:pred.pn]</ta>
            <ta e="T189" id="Seg_9463" s="T188">dempro</ta>
            <ta e="T190" id="Seg_9464" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_9465" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_9466" s="T191">adv</ta>
            <ta e="T193" id="Seg_9467" s="T192">v-v:cvb</ta>
            <ta e="T194" id="Seg_9468" s="T193">v-v:cvb</ta>
            <ta e="T195" id="Seg_9469" s="T194">v-v:tense-v:pred.pn</ta>
            <ta e="T196" id="Seg_9470" s="T195">n-n:(poss).[n:case]</ta>
            <ta e="T197" id="Seg_9471" s="T196">v-v:cvb</ta>
            <ta e="T198" id="Seg_9472" s="T197">v-v:mood-v:temp.pn</ta>
            <ta e="T199" id="Seg_9473" s="T198">dempro</ta>
            <ta e="T200" id="Seg_9474" s="T199">adv</ta>
            <ta e="T201" id="Seg_9475" s="T200">dempro</ta>
            <ta e="T202" id="Seg_9476" s="T201">adv</ta>
            <ta e="T203" id="Seg_9477" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_9478" s="T203">que-pro:case</ta>
            <ta e="T205" id="Seg_9479" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_9480" s="T205">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T207" id="Seg_9481" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_9482" s="T207">que.[pro:case]</ta>
            <ta e="T209" id="Seg_9483" s="T208">adv</ta>
            <ta e="T210" id="Seg_9484" s="T209">n.[n:case]</ta>
            <ta e="T211" id="Seg_9485" s="T210">v-v:(neg)-v:pred.pn</ta>
            <ta e="T212" id="Seg_9486" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_9487" s="T212">v-v:(ins)-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T214" id="Seg_9488" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_9489" s="T214">v-v:ptcp</ta>
            <ta e="T216" id="Seg_9490" s="T215">n-n:poss-n:case</ta>
            <ta e="T217" id="Seg_9491" s="T216">dempro</ta>
            <ta e="T218" id="Seg_9492" s="T217">v-v:cvb</ta>
            <ta e="T219" id="Seg_9493" s="T218">post</ta>
            <ta e="T220" id="Seg_9494" s="T219">v-v:ptcp</ta>
            <ta e="T221" id="Seg_9495" s="T220">n-n:poss-n:case</ta>
            <ta e="T222" id="Seg_9496" s="T221">adv</ta>
            <ta e="T223" id="Seg_9497" s="T222">v-v:mood-v:pred.pn</ta>
            <ta e="T224" id="Seg_9498" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_9499" s="T224">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T226" id="Seg_9500" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_9501" s="T226">dempro</ta>
            <ta e="T228" id="Seg_9502" s="T227">v-v:cvb</ta>
            <ta e="T229" id="Seg_9503" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_9504" s="T229">adj-adj&gt;adj-n:poss-n:case</ta>
            <ta e="T231" id="Seg_9505" s="T230">v-v:mood-v:pred.pn</ta>
            <ta e="T232" id="Seg_9506" s="T231">que.[pro:case]</ta>
            <ta e="T233" id="Seg_9507" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_9508" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_9509" s="T234">v-v:tense-v:mood.[v:pred.pn]</ta>
            <ta e="T236" id="Seg_9510" s="T235">adj</ta>
            <ta e="T237" id="Seg_9511" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_9512" s="T237">post</ta>
            <ta e="T239" id="Seg_9513" s="T238">v-v:ptcp</ta>
            <ta e="T240" id="Seg_9514" s="T239">n.[n:case]</ta>
            <ta e="T241" id="Seg_9515" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_9516" s="T241">v-v:ptcp-v:mood-v:pred.pn</ta>
            <ta e="T243" id="Seg_9517" s="T242">adv</ta>
            <ta e="T244" id="Seg_9518" s="T243">adv</ta>
            <ta e="T245" id="Seg_9519" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_9520" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_9521" s="T246">dempro</ta>
            <ta e="T248" id="Seg_9522" s="T247">n-n:(poss).[n:case]</ta>
            <ta e="T249" id="Seg_9523" s="T248">v-v:tense.[v:pred.pn]</ta>
            <ta e="T250" id="Seg_9524" s="T249">n.[n:case]</ta>
            <ta e="T251" id="Seg_9525" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_9526" s="T251">v-v:ptcp</ta>
            <ta e="T253" id="Seg_9527" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_9528" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_9529" s="T254">adv</ta>
            <ta e="T256" id="Seg_9530" s="T255">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp</ta>
            <ta e="T257" id="Seg_9531" s="T256">v-v:mood-v:temp.pn</ta>
            <ta e="T258" id="Seg_9532" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_9533" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_9534" s="T259">dempro.[pro:case]</ta>
            <ta e="T261" id="Seg_9535" s="T260">v-v:(ins)-v:ptcp</ta>
            <ta e="T262" id="Seg_9536" s="T261">v-v:mood-v:temp.pn</ta>
            <ta e="T263" id="Seg_9537" s="T262">dempro</ta>
            <ta e="T264" id="Seg_9538" s="T263">v-v:cvb</ta>
            <ta e="T265" id="Seg_9539" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_9540" s="T265">v-v:cvb</ta>
            <ta e="T267" id="Seg_9541" s="T266">v-v:cvb-v:pred.pn</ta>
            <ta e="T268" id="Seg_9542" s="T267">dempro</ta>
            <ta e="T269" id="Seg_9543" s="T268">n.[n:case]</ta>
            <ta e="T270" id="Seg_9544" s="T269">n-n:(poss).[n:case]</ta>
            <ta e="T271" id="Seg_9545" s="T270">v-v:mood-v:temp.pn</ta>
            <ta e="T272" id="Seg_9546" s="T271">dempro-pro:case</ta>
            <ta e="T273" id="Seg_9547" s="T272">adj-n&gt;n-n:case</ta>
            <ta e="T274" id="Seg_9548" s="T273">n-n&gt;v-v:cvb</ta>
            <ta e="T275" id="Seg_9549" s="T274">n-n:(ins)-n:case</ta>
            <ta e="T276" id="Seg_9550" s="T275">n-n:(ins)-n:case</ta>
            <ta e="T277" id="Seg_9551" s="T276">v-v:cvb</ta>
            <ta e="T278" id="Seg_9552" s="T277">adj-adj&gt;adj-n:poss-n:case</ta>
            <ta e="T279" id="Seg_9553" s="T278">v-v:tense.[v:pred.pn]</ta>
            <ta e="T280" id="Seg_9554" s="T279">dempro</ta>
            <ta e="T281" id="Seg_9555" s="T280">n.[n:case]</ta>
            <ta e="T282" id="Seg_9556" s="T281">v-v:ptcp</ta>
            <ta e="T283" id="Seg_9557" s="T282">n-n:poss-n:case</ta>
            <ta e="T284" id="Seg_9558" s="T283">v-v:ptcp</ta>
            <ta e="T285" id="Seg_9559" s="T284">n-n:poss-n:case</ta>
            <ta e="T286" id="Seg_9560" s="T285">post</ta>
            <ta e="T287" id="Seg_9561" s="T286">v-v:tense.[v:pred.pn]</ta>
            <ta e="T288" id="Seg_9562" s="T287">interj</ta>
            <ta e="T289" id="Seg_9563" s="T288">interj</ta>
            <ta e="T290" id="Seg_9564" s="T289">interj</ta>
            <ta e="T291" id="Seg_9565" s="T290">interj</ta>
            <ta e="T292" id="Seg_9566" s="T291">adj-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T293" id="Seg_9567" s="T292">n.[n:case]</ta>
            <ta e="T294" id="Seg_9568" s="T293">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T295" id="Seg_9569" s="T294">n-n:case</ta>
            <ta e="T296" id="Seg_9570" s="T295">interj</ta>
            <ta e="T297" id="Seg_9571" s="T296">interj</ta>
            <ta e="T298" id="Seg_9572" s="T297">interj</ta>
            <ta e="T299" id="Seg_9573" s="T298">adj-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T300" id="Seg_9574" s="T299">n.[n:case]</ta>
            <ta e="T301" id="Seg_9575" s="T300">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T302" id="Seg_9576" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_9577" s="T302">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T304" id="Seg_9578" s="T303">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T305" id="Seg_9579" s="T304">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T306" id="Seg_9580" s="T305">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T307" id="Seg_9581" s="T306">interj</ta>
            <ta e="T308" id="Seg_9582" s="T307">adj-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T309" id="Seg_9583" s="T308">n.[n:case]</ta>
            <ta e="T310" id="Seg_9584" s="T309">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T311" id="Seg_9585" s="T310">n-n:case</ta>
            <ta e="T312" id="Seg_9586" s="T311">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T313" id="Seg_9587" s="T312">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T314" id="Seg_9588" s="T313">v-v:cvb-v-v:cvb</ta>
            <ta e="T315" id="Seg_9589" s="T314">v-v:tense.[v:pred.pn]</ta>
            <ta e="T316" id="Seg_9590" s="T315">adv</ta>
            <ta e="T317" id="Seg_9591" s="T316">cardnum-cardnum&gt;adv</ta>
            <ta e="T318" id="Seg_9592" s="T317">n.[n:case]</ta>
            <ta e="T319" id="Seg_9593" s="T318">adv</ta>
            <ta e="T320" id="Seg_9594" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_9595" s="T320">v-v:ptcp</ta>
            <ta e="T322" id="Seg_9596" s="T321">n-n:case</ta>
            <ta e="T323" id="Seg_9597" s="T322">v-v:ptcp</ta>
            <ta e="T324" id="Seg_9598" s="T323">ptcl-ptcl:(temp.pn)</ta>
            <ta e="T325" id="Seg_9599" s="T324">n-n:poss-n:case</ta>
            <ta e="T326" id="Seg_9600" s="T325">n-n:poss-n:case</ta>
            <ta e="T327" id="Seg_9601" s="T326">adj-adj&gt;adv</ta>
            <ta e="T328" id="Seg_9602" s="T327">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T329" id="Seg_9603" s="T328">v-v:cvb</ta>
            <ta e="T330" id="Seg_9604" s="T329">v-v:tense.[v:pred.pn]</ta>
            <ta e="T331" id="Seg_9605" s="T330">dempro.[pro:case]</ta>
            <ta e="T332" id="Seg_9606" s="T331">v-v:ptcp</ta>
            <ta e="T333" id="Seg_9607" s="T332">n-n:poss-n:case</ta>
            <ta e="T334" id="Seg_9608" s="T333">post</ta>
            <ta e="T335" id="Seg_9609" s="T334">n.[n:case]</ta>
            <ta e="T336" id="Seg_9610" s="T335">post</ta>
            <ta e="T337" id="Seg_9611" s="T336">n-n:poss-n:case</ta>
            <ta e="T338" id="Seg_9612" s="T337">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T339" id="Seg_9613" s="T338">n-n:poss-n:case</ta>
            <ta e="T340" id="Seg_9614" s="T339">post</ta>
            <ta e="T341" id="Seg_9615" s="T340">que-pro:case</ta>
            <ta e="T342" id="Seg_9616" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_9617" s="T342">v-v&gt;v-v:ptcp-v:(poss)</ta>
            <ta e="T344" id="Seg_9618" s="T343">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T345" id="Seg_9619" s="T344">adj.[n:case]</ta>
            <ta e="T346" id="Seg_9620" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_9621" s="T346">adv</ta>
            <ta e="T348" id="Seg_9622" s="T347">n-n:(ins)-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T349" id="Seg_9623" s="T348">v-v:cvb-v:pred.pn</ta>
            <ta e="T350" id="Seg_9624" s="T349">dempro.[pro:case]</ta>
            <ta e="T351" id="Seg_9625" s="T350">n.[n:case]</ta>
            <ta e="T352" id="Seg_9626" s="T351">adv</ta>
            <ta e="T353" id="Seg_9627" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_9628" s="T353">ptcl</ta>
            <ta e="T355" id="Seg_9629" s="T354">dempro</ta>
            <ta e="T357" id="Seg_9630" s="T356">cardnum-cardnum&gt;adv</ta>
            <ta e="T358" id="Seg_9631" s="T357">n.[n:case]</ta>
            <ta e="T359" id="Seg_9632" s="T358">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T360" id="Seg_9633" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_9634" s="T360">interj</ta>
            <ta e="T362" id="Seg_9635" s="T361">interj</ta>
            <ta e="T363" id="Seg_9636" s="T362">interj</ta>
            <ta e="T364" id="Seg_9637" s="T363">adj.[n:case]</ta>
            <ta e="T365" id="Seg_9638" s="T364">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T366" id="Seg_9639" s="T365">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T367" id="Seg_9640" s="T366">n-n:(poss).[n:case]</ta>
            <ta e="T368" id="Seg_9641" s="T367">interj</ta>
            <ta e="T369" id="Seg_9642" s="T368">interj</ta>
            <ta e="T370" id="Seg_9643" s="T369">interj</ta>
            <ta e="T371" id="Seg_9644" s="T370">adj.[n:case]</ta>
            <ta e="T372" id="Seg_9645" s="T371">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T373" id="Seg_9646" s="T372">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T374" id="Seg_9647" s="T373">interj</ta>
            <ta e="T375" id="Seg_9648" s="T374">interj</ta>
            <ta e="T376" id="Seg_9649" s="T375">interj</ta>
            <ta e="T377" id="Seg_9650" s="T376">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T378" id="Seg_9651" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_9652" s="T378">adj-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T380" id="Seg_9653" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_9654" s="T380">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T382" id="Seg_9655" s="T381">n-n:case</ta>
            <ta e="T383" id="Seg_9656" s="T382">adj-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T384" id="Seg_9657" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_9658" s="T384">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T386" id="Seg_9659" s="T385">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T387" id="Seg_9660" s="T386">v-v:mood-v:pred.pn</ta>
            <ta e="T388" id="Seg_9661" s="T387">cardnum-cardnum&gt;adv</ta>
            <ta e="T389" id="Seg_9662" s="T388">n.[n:case]</ta>
            <ta e="T390" id="Seg_9663" s="T389">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T391" id="Seg_9664" s="T390">dempro</ta>
            <ta e="T392" id="Seg_9665" s="T391">v-v:cvb</ta>
            <ta e="T393" id="Seg_9666" s="T392">n-n:poss-n:case</ta>
            <ta e="T394" id="Seg_9667" s="T393">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T395" id="Seg_9668" s="T394">dempro</ta>
            <ta e="T396" id="Seg_9669" s="T395">v-v:ptcp</ta>
            <ta e="T397" id="Seg_9670" s="T396">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T398" id="Seg_9671" s="T397">v-v:ptcp</ta>
            <ta e="T399" id="Seg_9672" s="T398">n-n:poss-n:case</ta>
            <ta e="T400" id="Seg_9673" s="T399">adv</ta>
            <ta e="T401" id="Seg_9674" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_9675" s="T401">n.[n:case]</ta>
            <ta e="T403" id="Seg_9676" s="T402">v-v:tense.[v:pred.pn]</ta>
            <ta e="T404" id="Seg_9677" s="T403">n.[n:case]</ta>
            <ta e="T405" id="Seg_9678" s="T404">n.[n:case]</ta>
            <ta e="T406" id="Seg_9679" s="T405">dempro</ta>
            <ta e="T407" id="Seg_9680" s="T406">n-n:poss-n:case</ta>
            <ta e="T408" id="Seg_9681" s="T407">v-v:tense.[v:pred.pn]</ta>
            <ta e="T409" id="Seg_9682" s="T408">n.[n:case]</ta>
            <ta e="T410" id="Seg_9683" s="T409">n-n:(poss).[n:case]</ta>
            <ta e="T411" id="Seg_9684" s="T410">dempro</ta>
            <ta e="T412" id="Seg_9685" s="T411">n-n&gt;n-n:(poss).[n:case]</ta>
            <ta e="T413" id="Seg_9686" s="T412">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T414" id="Seg_9687" s="T413">n.[n:case]</ta>
            <ta e="T415" id="Seg_9688" s="T414">n-n&gt;adj.[n:case]</ta>
            <ta e="T416" id="Seg_9689" s="T415">v-v:mood.[v:pred.pn]</ta>
            <ta e="T417" id="Seg_9690" s="T416">adv</ta>
            <ta e="T418" id="Seg_9691" s="T417">n-n:case</ta>
            <ta e="T419" id="Seg_9692" s="T418">v-v:cvb</ta>
            <ta e="T420" id="Seg_9693" s="T419">post</ta>
            <ta e="T421" id="Seg_9694" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_9695" s="T421">n-n:case</ta>
            <ta e="T423" id="Seg_9696" s="T422">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T424" id="Seg_9697" s="T423">n-n:case</ta>
            <ta e="T425" id="Seg_9698" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_9699" s="T425">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T427" id="Seg_9700" s="T426">n-n-n&gt;n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T428" id="Seg_9701" s="T427">n-n:poss-n:case</ta>
            <ta e="T429" id="Seg_9702" s="T428">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T430" id="Seg_9703" s="T429">n-n:poss-n:case</ta>
            <ta e="T431" id="Seg_9704" s="T430">adj.[n:case]</ta>
            <ta e="T432" id="Seg_9705" s="T431">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T433" id="Seg_9706" s="T432">adj.[n:case]</ta>
            <ta e="T434" id="Seg_9707" s="T433">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T435" id="Seg_9708" s="T434">v-v:cvb</ta>
            <ta e="T436" id="Seg_9709" s="T435">adv</ta>
            <ta e="T437" id="Seg_9710" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_9711" s="T437">n.[n:case]</ta>
            <ta e="T439" id="Seg_9712" s="T438">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T440" id="Seg_9713" s="T439">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T441" id="Seg_9714" s="T440">n.[n:case]</ta>
            <ta e="T442" id="Seg_9715" s="T441">n-n&gt;n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T443" id="Seg_9716" s="T442">v.[v:mood.pn]</ta>
            <ta e="T444" id="Seg_9717" s="T443">adj</ta>
            <ta e="T445" id="Seg_9718" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_9719" s="T445">v.[v:mood.pn]</ta>
            <ta e="T447" id="Seg_9720" s="T446">que.[pro:case]</ta>
            <ta e="T448" id="Seg_9721" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_9722" s="T448">adj-n:case</ta>
            <ta e="T450" id="Seg_9723" s="T449">v-v&gt;v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T451" id="Seg_9724" s="T450">adj</ta>
            <ta e="T452" id="Seg_9725" s="T451">v-v&gt;v.[v:mood.pn]</ta>
            <ta e="T453" id="Seg_9726" s="T452">n-n:poss-n:case</ta>
            <ta e="T454" id="Seg_9727" s="T453">adj</ta>
            <ta e="T455" id="Seg_9728" s="T454">v.[v:mood.pn]</ta>
            <ta e="T456" id="Seg_9729" s="T455">v-v:cvb</ta>
            <ta e="T457" id="Seg_9730" s="T456">adv</ta>
            <ta e="T458" id="Seg_9731" s="T457">dempro</ta>
            <ta e="T459" id="Seg_9732" s="T458">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T460" id="Seg_9733" s="T459">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T461" id="Seg_9734" s="T460">post</ta>
            <ta e="T462" id="Seg_9735" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_9736" s="T462">v-v:tense.[v:pred.pn]</ta>
            <ta e="T464" id="Seg_9737" s="T463">dempro</ta>
            <ta e="T465" id="Seg_9738" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_9739" s="T465">v-v:cvb</ta>
            <ta e="T467" id="Seg_9740" s="T466">v-v:tense.[v:pred.pn]</ta>
            <ta e="T468" id="Seg_9741" s="T467">dempro</ta>
            <ta e="T469" id="Seg_9742" s="T468">v-v:cvb</ta>
            <ta e="T470" id="Seg_9743" s="T469">v-v:cvb</ta>
            <ta e="T471" id="Seg_9744" s="T470">post</ta>
            <ta e="T472" id="Seg_9745" s="T471">dempro</ta>
            <ta e="T473" id="Seg_9746" s="T472">v-v:ptcp</ta>
            <ta e="T474" id="Seg_9747" s="T473">n.[n:case]</ta>
            <ta e="T475" id="Seg_9748" s="T474">v-v:cvb</ta>
            <ta e="T476" id="Seg_9749" s="T475">dempro</ta>
            <ta e="T477" id="Seg_9750" s="T476">adj-adj&gt;adv</ta>
            <ta e="T478" id="Seg_9751" s="T477">v-v&gt;v-v:ptcp</ta>
            <ta e="T479" id="Seg_9752" s="T478">v-v:mood-v:temp.pn</ta>
            <ta e="T480" id="Seg_9753" s="T479">v-v:tense.[v:pred.pn]</ta>
            <ta e="T481" id="Seg_9754" s="T480">n.[n:case]</ta>
            <ta e="T482" id="Seg_9755" s="T481">n-n:poss-n:case</ta>
            <ta e="T483" id="Seg_9756" s="T482">v-v:tense.[v:pred.pn]</ta>
            <ta e="T484" id="Seg_9757" s="T483">n-n:poss-n:case</ta>
            <ta e="T485" id="Seg_9758" s="T484">n-n:poss-n:case</ta>
            <ta e="T486" id="Seg_9759" s="T485">n-n:(poss).[n:case]</ta>
            <ta e="T487" id="Seg_9760" s="T486">n-n&gt;v-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T488" id="Seg_9761" s="T487">v-v:cvb</ta>
            <ta e="T489" id="Seg_9762" s="T488">adv</ta>
            <ta e="T490" id="Seg_9763" s="T489">n-n:poss-n:case</ta>
            <ta e="T491" id="Seg_9764" s="T490">v-v:tense.[v:pred.pn]</ta>
            <ta e="T492" id="Seg_9765" s="T491">dempro</ta>
            <ta e="T493" id="Seg_9766" s="T492">n-n:poss-n:case</ta>
            <ta e="T494" id="Seg_9767" s="T493">v-v:cvb</ta>
            <ta e="T495" id="Seg_9768" s="T494">v-v:cvb</ta>
            <ta e="T496" id="Seg_9769" s="T495">dempro</ta>
            <ta e="T497" id="Seg_9770" s="T496">n-n:poss-n:case</ta>
            <ta e="T498" id="Seg_9771" s="T497">v-v:cvb</ta>
            <ta e="T499" id="Seg_9772" s="T498">v-v:tense.[v:pred.pn]</ta>
            <ta e="T500" id="Seg_9773" s="T499">dempro</ta>
            <ta e="T501" id="Seg_9774" s="T500">n.[n:case]</ta>
            <ta e="T502" id="Seg_9775" s="T501">dempro</ta>
            <ta e="T503" id="Seg_9776" s="T502">v-v:ptcp</ta>
            <ta e="T504" id="Seg_9777" s="T503">n-n:(poss).[n:case]</ta>
            <ta e="T505" id="Seg_9778" s="T504">n.[n:case]</ta>
            <ta e="T506" id="Seg_9779" s="T505">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T507" id="Seg_9780" s="T506">dempro</ta>
            <ta e="T508" id="Seg_9781" s="T507">v-v:cvb</ta>
            <ta e="T509" id="Seg_9782" s="T508">n.[n:case]</ta>
            <ta e="T510" id="Seg_9783" s="T509">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T511" id="Seg_9784" s="T510">dempro</ta>
            <ta e="T512" id="Seg_9785" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_9786" s="T512">n-n:poss-n:case</ta>
            <ta e="T514" id="Seg_9787" s="T513">v-v:tense-v:pred.pn</ta>
            <ta e="T515" id="Seg_9788" s="T514">v-v:tense-v:pred.pn</ta>
            <ta e="T516" id="Seg_9789" s="T515">dempro</ta>
            <ta e="T517" id="Seg_9790" s="T516">n.[n:case]</ta>
            <ta e="T518" id="Seg_9791" s="T517">n.[n:case]</ta>
            <ta e="T519" id="Seg_9792" s="T518">v-v:tense.[v:pred.pn]</ta>
            <ta e="T520" id="Seg_9793" s="T519">dempro</ta>
            <ta e="T521" id="Seg_9794" s="T520">n-n:poss-n:case</ta>
            <ta e="T522" id="Seg_9795" s="T521">v-v:cvb</ta>
            <ta e="T523" id="Seg_9796" s="T522">post</ta>
            <ta e="T524" id="Seg_9797" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_9798" s="T524">v-v:cvb</ta>
            <ta e="T526" id="Seg_9799" s="T525">v-v:tense.[v:pred.pn]</ta>
            <ta e="T527" id="Seg_9800" s="T526">n.[n:case]</ta>
            <ta e="T528" id="Seg_9801" s="T527">n-n:(ins)-n:case</ta>
            <ta e="T529" id="Seg_9802" s="T528">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp</ta>
            <ta e="T530" id="Seg_9803" s="T529">n.[n:case]</ta>
            <ta e="T531" id="Seg_9804" s="T530">n-n:(ins)-n:case</ta>
            <ta e="T532" id="Seg_9805" s="T531">v-v:tense.[v:pred.pn]</ta>
            <ta e="T533" id="Seg_9806" s="T532">v-v:cvb</ta>
            <ta e="T534" id="Seg_9807" s="T533">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T535" id="Seg_9808" s="T534">adv</ta>
            <ta e="T536" id="Seg_9809" s="T535">adv</ta>
            <ta e="T537" id="Seg_9810" s="T536">v-v:cvb</ta>
            <ta e="T538" id="Seg_9811" s="T537">cardnum</ta>
            <ta e="T539" id="Seg_9812" s="T538">n.[n:case]</ta>
            <ta e="T540" id="Seg_9813" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_9814" s="T540">que</ta>
            <ta e="T542" id="Seg_9815" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_9816" s="T542">n.[n:case]</ta>
            <ta e="T544" id="Seg_9817" s="T543">v-v:cvb</ta>
            <ta e="T545" id="Seg_9818" s="T544">post</ta>
            <ta e="T546" id="Seg_9819" s="T545">v-v:mood.[v:pred.pn]</ta>
            <ta e="T547" id="Seg_9820" s="T546">n.[n:case]</ta>
            <ta e="T548" id="Seg_9821" s="T547">n-n:(poss).[n:case]</ta>
            <ta e="T549" id="Seg_9822" s="T548">adv</ta>
            <ta e="T550" id="Seg_9823" s="T549">ptcl</ta>
            <ta e="T551" id="Seg_9824" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_9825" s="T551">n-n:poss-n:case</ta>
            <ta e="T553" id="Seg_9826" s="T552">n-n:(poss).[n:case]</ta>
            <ta e="T554" id="Seg_9827" s="T553">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T555" id="Seg_9828" s="T554">dempro</ta>
            <ta e="T556" id="Seg_9829" s="T555">n-n:poss-n:case</ta>
            <ta e="T557" id="Seg_9830" s="T556">n-n:poss-n:case</ta>
            <ta e="T558" id="Seg_9831" s="T557">v-v:tense.[v:pred.pn]</ta>
            <ta e="T559" id="Seg_9832" s="T558">n-n:poss-n:case</ta>
            <ta e="T560" id="Seg_9833" s="T559">ptcl</ta>
            <ta e="T561" id="Seg_9834" s="T560">v-v:tense.[v:pred.pn]</ta>
            <ta e="T562" id="Seg_9835" s="T561">que</ta>
            <ta e="T563" id="Seg_9836" s="T562">ptcl</ta>
            <ta e="T564" id="Seg_9837" s="T563">v-v:cvb</ta>
            <ta e="T565" id="Seg_9838" s="T564">post</ta>
            <ta e="T566" id="Seg_9839" s="T565">v-v:cvb</ta>
            <ta e="T567" id="Seg_9840" s="T566">v-v:tense.[v:pred.pn]</ta>
            <ta e="T568" id="Seg_9841" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_9842" s="T568">adv</ta>
            <ta e="T570" id="Seg_9843" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_9844" s="T570">ptcl</ta>
            <ta e="T573" id="Seg_9845" s="T572">n-n:case</ta>
            <ta e="T574" id="Seg_9846" s="T573">adv</ta>
            <ta e="T575" id="Seg_9847" s="T574">v-v:(neg)-v:pred.pn</ta>
            <ta e="T576" id="Seg_9848" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_9849" s="T576">v-v:cvb</ta>
            <ta e="T578" id="Seg_9850" s="T577">v-v:tense.[v:pred.pn]</ta>
            <ta e="T579" id="Seg_9851" s="T578">n-n:poss-n:case</ta>
            <ta e="T580" id="Seg_9852" s="T579">n-n:case</ta>
            <ta e="T581" id="Seg_9853" s="T580">v-v:tense.[v:pred.pn]</ta>
            <ta e="T582" id="Seg_9854" s="T581">adv</ta>
            <ta e="T583" id="Seg_9855" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_9856" s="T583">n-n:poss-n:case</ta>
            <ta e="T586" id="Seg_9857" s="T585">n-n:poss-n:case</ta>
            <ta e="T587" id="Seg_9858" s="T586">v-v:cvb</ta>
            <ta e="T588" id="Seg_9859" s="T587">post</ta>
            <ta e="T589" id="Seg_9860" s="T588">dempro</ta>
            <ta e="T590" id="Seg_9861" s="T589">n-n:poss-n:case</ta>
            <ta e="T591" id="Seg_9862" s="T590">n-n:poss-n:case</ta>
            <ta e="T592" id="Seg_9863" s="T591">v-v:cvb</ta>
            <ta e="T593" id="Seg_9864" s="T592">v-v:cvb</ta>
            <ta e="T594" id="Seg_9865" s="T593">post</ta>
            <ta e="T595" id="Seg_9866" s="T594">dempro</ta>
            <ta e="T596" id="Seg_9867" s="T595">n-n:poss-n:case</ta>
            <ta e="T597" id="Seg_9868" s="T596">dempro</ta>
            <ta e="T598" id="Seg_9869" s="T597">n-n:case</ta>
            <ta e="T599" id="Seg_9870" s="T598">dempro</ta>
            <ta e="T601" id="Seg_9871" s="T600">n-n:case</ta>
            <ta e="T602" id="Seg_9872" s="T601">v-v:cvb</ta>
            <ta e="T603" id="Seg_9873" s="T602">v-v:tense.[v:pred.pn]</ta>
            <ta e="T604" id="Seg_9874" s="T603">dempro</ta>
            <ta e="T605" id="Seg_9875" s="T604">n-n:case</ta>
            <ta e="T606" id="Seg_9876" s="T605">v-v:tense.[v:pred.pn]</ta>
            <ta e="T607" id="Seg_9877" s="T606">dempro</ta>
            <ta e="T608" id="Seg_9878" s="T607">n.[n:case]</ta>
            <ta e="T609" id="Seg_9879" s="T608">n-n:(poss).[n:case]</ta>
            <ta e="T610" id="Seg_9880" s="T609">n-n:poss-n:case</ta>
            <ta e="T611" id="Seg_9881" s="T610">post</ta>
            <ta e="T612" id="Seg_9882" s="T611">dempro</ta>
            <ta e="T613" id="Seg_9883" s="T612">n.[n:case]</ta>
            <ta e="T614" id="Seg_9884" s="T613">v-v:ptcp</ta>
            <ta e="T615" id="Seg_9885" s="T614">n-n:poss-n:case</ta>
            <ta e="T616" id="Seg_9886" s="T615">n-n:poss-n:case</ta>
            <ta e="T617" id="Seg_9887" s="T616">n.[n:case]</ta>
            <ta e="T618" id="Seg_9888" s="T617">v-v:ptcp</ta>
            <ta e="T619" id="Seg_9889" s="T618">n-n:poss-n:case</ta>
            <ta e="T620" id="Seg_9890" s="T619">n.[n:case]</ta>
            <ta e="T621" id="Seg_9891" s="T620">v-v:ptcp</ta>
            <ta e="T622" id="Seg_9892" s="T621">n-n:poss-n:case</ta>
            <ta e="T623" id="Seg_9893" s="T622">adv</ta>
            <ta e="T624" id="Seg_9894" s="T623">v-v:mood-v:pred.pn</ta>
            <ta e="T625" id="Seg_9895" s="T624">adv-adv&gt;adj</ta>
            <ta e="T626" id="Seg_9896" s="T625">n-n:case</ta>
            <ta e="T627" id="Seg_9897" s="T626">n-n&gt;adj.[n:case]</ta>
            <ta e="T628" id="Seg_9898" s="T627">n-n:case</ta>
            <ta e="T629" id="Seg_9899" s="T628">v-v:mood-v:temp.pn</ta>
            <ta e="T630" id="Seg_9900" s="T629">n-n:(poss).[n:case]</ta>
            <ta e="T631" id="Seg_9901" s="T630">ptcl</ta>
            <ta e="T632" id="Seg_9902" s="T631">ptcl</ta>
            <ta e="T633" id="Seg_9903" s="T632">v-v:mood-v:temp.pn</ta>
            <ta e="T634" id="Seg_9904" s="T633">n-n:case</ta>
            <ta e="T635" id="Seg_9905" s="T634">v-v:cvb</ta>
            <ta e="T636" id="Seg_9906" s="T635">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T637" id="Seg_9907" s="T636">v-v:mood-v:temp.pn</ta>
            <ta e="T638" id="Seg_9908" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_9909" s="T638">dempro</ta>
            <ta e="T640" id="Seg_9910" s="T639">n-n:case</ta>
            <ta e="T641" id="Seg_9911" s="T640">n.[n:case]</ta>
            <ta e="T642" id="Seg_9912" s="T641">v-v:tense.[v:pred.pn]</ta>
            <ta e="T643" id="Seg_9913" s="T642">v-v:tense.[v:pred.pn]</ta>
            <ta e="T644" id="Seg_9914" s="T643">n-n:(poss).[n:case]</ta>
            <ta e="T645" id="Seg_9915" s="T644">v-v:tense.[v:pred.pn]</ta>
            <ta e="T646" id="Seg_9916" s="T645">v-v:ptcp</ta>
            <ta e="T647" id="Seg_9917" s="T646">n-n:(poss).[n:case]</ta>
            <ta e="T648" id="Seg_9918" s="T647">n-n&gt;adj.[n:case]</ta>
            <ta e="T649" id="Seg_9919" s="T648">v-v:mood-v:temp.pn</ta>
            <ta e="T650" id="Seg_9920" s="T649">dempro</ta>
            <ta e="T651" id="Seg_9921" s="T650">n.[n:case]</ta>
            <ta e="T652" id="Seg_9922" s="T651">v-v:tense.[v:pred.pn]</ta>
            <ta e="T653" id="Seg_9923" s="T652">v-v:tense.[v:pred.pn]</ta>
            <ta e="T654" id="Seg_9924" s="T653">dempro</ta>
            <ta e="T655" id="Seg_9925" s="T654">n.[n:case]</ta>
            <ta e="T656" id="Seg_9926" s="T655">v-v:ptcp</ta>
            <ta e="T657" id="Seg_9927" s="T656">n-n:(poss).[n:case]</ta>
            <ta e="T658" id="Seg_9928" s="T657">v-v:mood-v:pred.pn</ta>
            <ta e="T659" id="Seg_9929" s="T658">adv</ta>
            <ta e="T660" id="Seg_9930" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_9931" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_9932" s="T661">adv</ta>
            <ta e="T663" id="Seg_9933" s="T662">dempro</ta>
            <ta e="T664" id="Seg_9934" s="T663">n-n:case</ta>
            <ta e="T665" id="Seg_9935" s="T664">n-n&gt;v-v:ptcp</ta>
            <ta e="T666" id="Seg_9936" s="T665">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T667" id="Seg_9937" s="T666">adj.[n:case]</ta>
            <ta e="T668" id="Seg_9938" s="T667">ptcl</ta>
            <ta e="T669" id="Seg_9939" s="T668">v-v:ptcp</ta>
            <ta e="T670" id="Seg_9940" s="T669">adj-n:poss-n:case</ta>
            <ta e="T671" id="Seg_9941" s="T670">v-v:cvb</ta>
            <ta e="T672" id="Seg_9942" s="T671">ptcl</ta>
            <ta e="T673" id="Seg_9943" s="T672">v-v:tense-v:pred.pn</ta>
            <ta e="T674" id="Seg_9944" s="T673">n-n:poss-n:case</ta>
            <ta e="T675" id="Seg_9945" s="T674">n-n:poss-n:case</ta>
            <ta e="T676" id="Seg_9946" s="T675">n-n:poss-n:case</ta>
            <ta e="T677" id="Seg_9947" s="T676">post</ta>
            <ta e="T678" id="Seg_9948" s="T677">cardnum-cardnum&gt;adv</ta>
            <ta e="T679" id="Seg_9949" s="T678">n.[n:case]</ta>
            <ta e="T680" id="Seg_9950" s="T679">v-v:tense-v:pred.pn</ta>
            <ta e="T681" id="Seg_9951" s="T680">interj</ta>
            <ta e="T682" id="Seg_9952" s="T681">interj</ta>
            <ta e="T683" id="Seg_9953" s="T682">adj-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T684" id="Seg_9954" s="T683">n-n:case</ta>
            <ta e="T685" id="Seg_9955" s="T684">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T686" id="Seg_9956" s="T685">n-n:case</ta>
            <ta e="T687" id="Seg_9957" s="T686">interj</ta>
            <ta e="T688" id="Seg_9958" s="T687">interj</ta>
            <ta e="T689" id="Seg_9959" s="T688">adj-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T690" id="Seg_9960" s="T689">n-n:case</ta>
            <ta e="T691" id="Seg_9961" s="T690">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T692" id="Seg_9962" s="T691">n-n:case</ta>
            <ta e="T693" id="Seg_9963" s="T692">interj-interj</ta>
            <ta e="T694" id="Seg_9964" s="T693">adj-n:poss-n:case</ta>
            <ta e="T695" id="Seg_9965" s="T694">n-n:case</ta>
            <ta e="T696" id="Seg_9966" s="T695">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T697" id="Seg_9967" s="T696">n-n:case</ta>
            <ta e="T698" id="Seg_9968" s="T697">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T699" id="Seg_9969" s="T698">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T700" id="Seg_9970" s="T699">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T701" id="Seg_9971" s="T700">v-v:cvb-v:pred.pn</ta>
            <ta e="T702" id="Seg_9972" s="T701">v-v:cvb-v:pred.pn</ta>
            <ta e="T703" id="Seg_9973" s="T702">v-v:tense-v:pred.pn</ta>
            <ta e="T704" id="Seg_9974" s="T703">dempro</ta>
            <ta e="T705" id="Seg_9975" s="T704">dempro</ta>
            <ta e="T706" id="Seg_9976" s="T705">v-v:cvb</ta>
            <ta e="T707" id="Seg_9977" s="T706">post</ta>
            <ta e="T708" id="Seg_9978" s="T707">n-n:poss-n:case</ta>
            <ta e="T709" id="Seg_9979" s="T708">v-v:tense.[v:pred.pn]</ta>
            <ta e="T710" id="Seg_9980" s="T709">n.[n:case]</ta>
            <ta e="T711" id="Seg_9981" s="T710">adv</ta>
            <ta e="T712" id="Seg_9982" s="T711">ptcl</ta>
            <ta e="T713" id="Seg_9983" s="T712">n-n:case</ta>
            <ta e="T714" id="Seg_9984" s="T713">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T715" id="Seg_9985" s="T714">v-v:ptcp</ta>
            <ta e="T716" id="Seg_9986" s="T715">n-n:poss-n:case</ta>
            <ta e="T717" id="Seg_9987" s="T716">dempro</ta>
            <ta e="T718" id="Seg_9988" s="T717">v-v:ptcp</ta>
            <ta e="T719" id="Seg_9989" s="T718">n-n:poss-n:case</ta>
            <ta e="T720" id="Seg_9990" s="T719">adv</ta>
            <ta e="T721" id="Seg_9991" s="T720">v-v:cvb</ta>
            <ta e="T722" id="Seg_9992" s="T721">v-v:tense-v:pred.pn</ta>
            <ta e="T723" id="Seg_9993" s="T722">n-n:poss-n:case</ta>
            <ta e="T724" id="Seg_9994" s="T723">v-v:tense-v:pred.pn</ta>
            <ta e="T725" id="Seg_9995" s="T724">v-v:cvb</ta>
            <ta e="T726" id="Seg_9996" s="T725">v-v:tense-v:pred.pn</ta>
            <ta e="T727" id="Seg_9997" s="T726">v-v:ptcp</ta>
            <ta e="T728" id="Seg_9998" s="T727">n-n:case</ta>
            <ta e="T729" id="Seg_9999" s="T728">n-n&gt;v-v:ptcp</ta>
            <ta e="T730" id="Seg_10000" s="T729">n-n:case</ta>
            <ta e="T731" id="Seg_10001" s="T730">dempro</ta>
            <ta e="T732" id="Seg_10002" s="T731">v-v:cvb</ta>
            <ta e="T733" id="Seg_10003" s="T732">post</ta>
            <ta e="T734" id="Seg_10004" s="T733">dempro</ta>
            <ta e="T735" id="Seg_10005" s="T734">v-v:cvb</ta>
            <ta e="T736" id="Seg_10006" s="T735">v-v:cvb</ta>
            <ta e="T737" id="Seg_10007" s="T736">dempro</ta>
            <ta e="T738" id="Seg_10008" s="T737">v-v:tense.[v:pred.pn]</ta>
            <ta e="T739" id="Seg_10009" s="T738">dempro</ta>
            <ta e="T740" id="Seg_10010" s="T739">interj</ta>
            <ta e="T741" id="Seg_10011" s="T740">n.[n:case]</ta>
            <ta e="T742" id="Seg_10012" s="T741">v-v:tense-v:poss.pn</ta>
            <ta e="T743" id="Seg_10013" s="T742">v-v:cvb</ta>
            <ta e="T744" id="Seg_10014" s="T743">n-n:poss-n:case</ta>
            <ta e="T745" id="Seg_10015" s="T744">n-n&gt;adj</ta>
            <ta e="T746" id="Seg_10016" s="T745">n-n:poss-n:case</ta>
            <ta e="T747" id="Seg_10017" s="T746">n-n&gt;adj</ta>
            <ta e="T748" id="Seg_10018" s="T747">n.[n:case]</ta>
            <ta e="T749" id="Seg_10019" s="T748">n-n:case</ta>
            <ta e="T750" id="Seg_10020" s="T749">dempro</ta>
            <ta e="T751" id="Seg_10021" s="T750">v-v:cvb</ta>
            <ta e="T752" id="Seg_10022" s="T751">v-v:tense.[v:pred.pn]</ta>
            <ta e="T753" id="Seg_10023" s="T752">interj</ta>
            <ta e="T754" id="Seg_10024" s="T753">dempro</ta>
            <ta e="T755" id="Seg_10025" s="T754">n.[n:case]</ta>
            <ta e="T756" id="Seg_10026" s="T755">n.[n:case]</ta>
            <ta e="T757" id="Seg_10027" s="T756">v-v:ptcp</ta>
            <ta e="T758" id="Seg_10028" s="T757">v-v:mood-v:temp.pn</ta>
            <ta e="T759" id="Seg_10029" s="T758">n.[n:case]</ta>
            <ta e="T760" id="Seg_10030" s="T759">n.[n:case]</ta>
            <ta e="T761" id="Seg_10031" s="T760">v-v:tense-v:pred.pn</ta>
            <ta e="T762" id="Seg_10032" s="T761">n.[n:case]</ta>
            <ta e="T763" id="Seg_10033" s="T762">n.[n:case]</ta>
            <ta e="T764" id="Seg_10034" s="T763">v-v:ptcp</ta>
            <ta e="T765" id="Seg_10035" s="T764">v-v:mood-v:temp.pn</ta>
            <ta e="T766" id="Seg_10036" s="T765">n.[n:case]</ta>
            <ta e="T767" id="Seg_10037" s="T766">n.[n:case]</ta>
            <ta e="T768" id="Seg_10038" s="T767">v-v:tense-v:pred.pn</ta>
            <ta e="T769" id="Seg_10039" s="T768">adv</ta>
            <ta e="T770" id="Seg_10040" s="T769">dempro</ta>
            <ta e="T771" id="Seg_10041" s="T770">cardnum</ta>
            <ta e="T772" id="Seg_10042" s="T771">n.[n:case]</ta>
            <ta e="T773" id="Seg_10043" s="T772">v-v:cvb</ta>
            <ta e="T774" id="Seg_10044" s="T773">post</ta>
            <ta e="T775" id="Seg_10045" s="T774">n-n:(poss).[n:case]</ta>
            <ta e="T776" id="Seg_10046" s="T775">n-n:case</ta>
            <ta e="T777" id="Seg_10047" s="T776">v-v:tense.[v:pred.pn]</ta>
            <ta e="T778" id="Seg_10048" s="T777">n-n:poss-n:case</ta>
            <ta e="T779" id="Seg_10049" s="T778">n-n:(poss).[n:case]</ta>
            <ta e="T780" id="Seg_10050" s="T779">v-v:tense.[v:pred.pn]</ta>
            <ta e="T781" id="Seg_10051" s="T780">adv</ta>
            <ta e="T782" id="Seg_10052" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_10053" s="T782">dempro</ta>
            <ta e="T784" id="Seg_10054" s="T783">n-n:case</ta>
            <ta e="T785" id="Seg_10055" s="T784">ptcl</ta>
            <ta e="T786" id="Seg_10056" s="T785">v-n:(ins)-v&gt;v-n:(ins)-v:ptcp</ta>
            <ta e="T787" id="Seg_10057" s="T786">adj</ta>
            <ta e="T788" id="Seg_10058" s="T787">n-n:(num)-n:(ins)-n:case</ta>
            <ta e="T789" id="Seg_10059" s="T788">n-n:case</ta>
            <ta e="T790" id="Seg_10060" s="T789">n.[n:case]</ta>
            <ta e="T791" id="Seg_10061" s="T790">n-n:case</ta>
            <ta e="T792" id="Seg_10062" s="T791">v-v:cvb</ta>
            <ta e="T793" id="Seg_10063" s="T792">v-v:mood-v:pred.pn</ta>
            <ta e="T794" id="Seg_10064" s="T793">n-n:case</ta>
            <ta e="T795" id="Seg_10065" s="T794">dempro</ta>
            <ta e="T796" id="Seg_10066" s="T795">que.[pro:case]</ta>
            <ta e="T797" id="Seg_10067" s="T796">interj</ta>
            <ta e="T798" id="Seg_10068" s="T797">n.[n:case]</ta>
            <ta e="T799" id="Seg_10069" s="T798">post</ta>
            <ta e="T800" id="Seg_10070" s="T799">v-v:mood-v:pred.pn</ta>
            <ta e="T801" id="Seg_10071" s="T800">dempro.[pro:case]</ta>
            <ta e="T802" id="Seg_10072" s="T801">dempro-pro:case</ta>
            <ta e="T803" id="Seg_10073" s="T802">ptcl</ta>
            <ta e="T804" id="Seg_10074" s="T803">v-v&gt;v-v:ptcp-v:(poss)</ta>
            <ta e="T805" id="Seg_10075" s="T804">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T806" id="Seg_10076" s="T805">v-v:ptcp</ta>
            <ta e="T807" id="Seg_10077" s="T806">n.[n:case]</ta>
            <ta e="T808" id="Seg_10078" s="T807">n-n:(poss).[n:case]</ta>
            <ta e="T809" id="Seg_10079" s="T808">dempro.[pro:case]</ta>
            <ta e="T810" id="Seg_10080" s="T809">n.[n:case]</ta>
            <ta e="T811" id="Seg_10081" s="T810">dempro.[pro:case]</ta>
            <ta e="T812" id="Seg_10082" s="T811">post</ta>
            <ta e="T813" id="Seg_10083" s="T812">ptcl</ta>
            <ta e="T814" id="Seg_10084" s="T813">ptcl</ta>
            <ta e="T815" id="Seg_10085" s="T814">n.[n:case]</ta>
            <ta e="T816" id="Seg_10086" s="T815">v-v:tense.[v:poss.pn]</ta>
            <ta e="T817" id="Seg_10087" s="T816">v-v:mood-v:pred.pn</ta>
            <ta e="T818" id="Seg_10088" s="T817">dempro.[pro:case]</ta>
            <ta e="T819" id="Seg_10089" s="T818">post</ta>
            <ta e="T0" id="Seg_10090" s="T820">n-n:case</ta>
            <ta e="T822" id="Seg_10091" s="T821">conj</ta>
            <ta e="T823" id="Seg_10092" s="T822">n-n:(poss).[n:case]</ta>
            <ta e="T824" id="Seg_10093" s="T823">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T825" id="Seg_10094" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_10095" s="T825">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T827" id="Seg_10096" s="T826">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T828" id="Seg_10097" s="T827">n-n:case</ta>
            <ta e="T829" id="Seg_10098" s="T828">v-v:cvb</ta>
            <ta e="T830" id="Seg_10099" s="T829">v-v:mood-v:temp.pn</ta>
            <ta e="T831" id="Seg_10100" s="T830">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T832" id="Seg_10101" s="T831">n-n&gt;adj.[n:case]</ta>
            <ta e="T833" id="Seg_10102" s="T832">v-v:mood.[v:pred.pn]</ta>
            <ta e="T834" id="Seg_10103" s="T833">n.[n:case]</ta>
            <ta e="T835" id="Seg_10104" s="T834">n-n:poss-n:case</ta>
            <ta e="T836" id="Seg_10105" s="T835">ptcl</ta>
            <ta e="T837" id="Seg_10106" s="T836">n-n&gt;adj.[n:case]</ta>
            <ta e="T838" id="Seg_10107" s="T837">v-v:mood.[v:pred.pn]</ta>
            <ta e="T839" id="Seg_10108" s="T838">adv</ta>
            <ta e="T840" id="Seg_10109" s="T839">n.[n:case]</ta>
            <ta e="T841" id="Seg_10110" s="T840">v-v:tense.[v:pred.pn]</ta>
            <ta e="T842" id="Seg_10111" s="T841">ptcl</ta>
            <ta e="T843" id="Seg_10112" s="T842">dempro.[pro:case]</ta>
            <ta e="T844" id="Seg_10113" s="T843">post</ta>
            <ta e="T845" id="Seg_10114" s="T844">n-n:(poss).[n:case]</ta>
            <ta e="T846" id="Seg_10115" s="T845">adj.[n:case]</ta>
            <ta e="T847" id="Seg_10116" s="T846">v-v:tense.[v:pred.pn]</ta>
            <ta e="T848" id="Seg_10117" s="T847">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_10118" s="T1">adj</ta>
            <ta e="T3" id="Seg_10119" s="T2">n</ta>
            <ta e="T4" id="Seg_10120" s="T3">n</ta>
            <ta e="T5" id="Seg_10121" s="T4">n</ta>
            <ta e="T6" id="Seg_10122" s="T5">n</ta>
            <ta e="T7" id="Seg_10123" s="T6">n</ta>
            <ta e="T8" id="Seg_10124" s="T7">n</ta>
            <ta e="T9" id="Seg_10125" s="T8">adj</ta>
            <ta e="T10" id="Seg_10126" s="T9">n</ta>
            <ta e="T11" id="Seg_10127" s="T10">v</ta>
            <ta e="T12" id="Seg_10128" s="T11">n</ta>
            <ta e="T13" id="Seg_10129" s="T12">adj</ta>
            <ta e="T14" id="Seg_10130" s="T13">n</ta>
            <ta e="T15" id="Seg_10131" s="T14">n</ta>
            <ta e="T16" id="Seg_10132" s="T15">v</ta>
            <ta e="T17" id="Seg_10133" s="T16">adv</ta>
            <ta e="T18" id="Seg_10134" s="T17">n</ta>
            <ta e="T19" id="Seg_10135" s="T18">n</ta>
            <ta e="T20" id="Seg_10136" s="T19">v</ta>
            <ta e="T21" id="Seg_10137" s="T20">v</ta>
            <ta e="T22" id="Seg_10138" s="T21">n</ta>
            <ta e="T23" id="Seg_10139" s="T22">v</ta>
            <ta e="T24" id="Seg_10140" s="T23">n</ta>
            <ta e="T25" id="Seg_10141" s="T24">v</ta>
            <ta e="T26" id="Seg_10142" s="T25">n</ta>
            <ta e="T27" id="Seg_10143" s="T26">v</ta>
            <ta e="T28" id="Seg_10144" s="T27">adv</ta>
            <ta e="T29" id="Seg_10145" s="T28">adv</ta>
            <ta e="T30" id="Seg_10146" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_10147" s="T30">dempro</ta>
            <ta e="T32" id="Seg_10148" s="T31">n</ta>
            <ta e="T33" id="Seg_10149" s="T32">v</ta>
            <ta e="T34" id="Seg_10150" s="T33">n</ta>
            <ta e="T35" id="Seg_10151" s="T34">v</ta>
            <ta e="T36" id="Seg_10152" s="T35">n</ta>
            <ta e="T37" id="Seg_10153" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_10154" s="T37">n</ta>
            <ta e="T39" id="Seg_10155" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_10156" s="T39">n</ta>
            <ta e="T41" id="Seg_10157" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_10158" s="T41">v</ta>
            <ta e="T43" id="Seg_10159" s="T42">v</ta>
            <ta e="T44" id="Seg_10160" s="T43">n</ta>
            <ta e="T45" id="Seg_10161" s="T44">n</ta>
            <ta e="T46" id="Seg_10162" s="T45">v</ta>
            <ta e="T47" id="Seg_10163" s="T46">adv</ta>
            <ta e="T48" id="Seg_10164" s="T47">n</ta>
            <ta e="T49" id="Seg_10165" s="T48">adv</ta>
            <ta e="T50" id="Seg_10166" s="T49">interj</ta>
            <ta e="T51" id="Seg_10167" s="T50">n</ta>
            <ta e="T52" id="Seg_10168" s="T51">n</ta>
            <ta e="T53" id="Seg_10169" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_10170" s="T53">adv</ta>
            <ta e="T55" id="Seg_10171" s="T54">v</ta>
            <ta e="T56" id="Seg_10172" s="T55">v</ta>
            <ta e="T57" id="Seg_10173" s="T56">n</ta>
            <ta e="T58" id="Seg_10174" s="T57">interj</ta>
            <ta e="T59" id="Seg_10175" s="T58">n</ta>
            <ta e="T60" id="Seg_10176" s="T59">cop</ta>
            <ta e="T61" id="Seg_10177" s="T60">n</ta>
            <ta e="T62" id="Seg_10178" s="T61">adv</ta>
            <ta e="T63" id="Seg_10179" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_10180" s="T63">dempro</ta>
            <ta e="T65" id="Seg_10181" s="T64">n</ta>
            <ta e="T66" id="Seg_10182" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_10183" s="T66">n</ta>
            <ta e="T68" id="Seg_10184" s="T67">n</ta>
            <ta e="T69" id="Seg_10185" s="T68">cop</ta>
            <ta e="T70" id="Seg_10186" s="T69">n</ta>
            <ta e="T71" id="Seg_10187" s="T70">adv</ta>
            <ta e="T72" id="Seg_10188" s="T71">v</ta>
            <ta e="T73" id="Seg_10189" s="T72">aux</ta>
            <ta e="T74" id="Seg_10190" s="T73">v</ta>
            <ta e="T75" id="Seg_10191" s="T74">dempro</ta>
            <ta e="T76" id="Seg_10192" s="T75">v</ta>
            <ta e="T77" id="Seg_10193" s="T76">n</ta>
            <ta e="T78" id="Seg_10194" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_10195" s="T78">interj</ta>
            <ta e="T80" id="Seg_10196" s="T79">v</ta>
            <ta e="T81" id="Seg_10197" s="T80">n</ta>
            <ta e="T82" id="Seg_10198" s="T81">n</ta>
            <ta e="T83" id="Seg_10199" s="T82">v</ta>
            <ta e="T84" id="Seg_10200" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_10201" s="T84">dempro</ta>
            <ta e="T86" id="Seg_10202" s="T85">n</ta>
            <ta e="T87" id="Seg_10203" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_10204" s="T87">n</ta>
            <ta e="T89" id="Seg_10205" s="T88">v</ta>
            <ta e="T90" id="Seg_10206" s="T89">n</ta>
            <ta e="T91" id="Seg_10207" s="T90">v</ta>
            <ta e="T92" id="Seg_10208" s="T91">n</ta>
            <ta e="T93" id="Seg_10209" s="T92">v</ta>
            <ta e="T94" id="Seg_10210" s="T93">dempro</ta>
            <ta e="T95" id="Seg_10211" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_10212" s="T95">n</ta>
            <ta e="T97" id="Seg_10213" s="T96">v</ta>
            <ta e="T98" id="Seg_10214" s="T97">n</ta>
            <ta e="T99" id="Seg_10215" s="T98">v</ta>
            <ta e="T100" id="Seg_10216" s="T99">v</ta>
            <ta e="T101" id="Seg_10217" s="T100">dempro</ta>
            <ta e="T102" id="Seg_10218" s="T101">post</ta>
            <ta e="T103" id="Seg_10219" s="T102">adj</ta>
            <ta e="T104" id="Seg_10220" s="T103">adj</ta>
            <ta e="T105" id="Seg_10221" s="T104">post</ta>
            <ta e="T106" id="Seg_10222" s="T105">n</ta>
            <ta e="T107" id="Seg_10223" s="T106">v</ta>
            <ta e="T108" id="Seg_10224" s="T107">v</ta>
            <ta e="T109" id="Seg_10225" s="T108">interj</ta>
            <ta e="T110" id="Seg_10226" s="T109">n</ta>
            <ta e="T111" id="Seg_10227" s="T110">v</ta>
            <ta e="T113" id="Seg_10228" s="T112">adj</ta>
            <ta e="T114" id="Seg_10229" s="T113">n</ta>
            <ta e="T115" id="Seg_10230" s="T114">adj</ta>
            <ta e="T116" id="Seg_10231" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_10232" s="T116">cop</ta>
            <ta e="T118" id="Seg_10233" s="T117">dempro</ta>
            <ta e="T119" id="Seg_10234" s="T118">interj</ta>
            <ta e="T120" id="Seg_10235" s="T119">n</ta>
            <ta e="T121" id="Seg_10236" s="T120">n</ta>
            <ta e="T122" id="Seg_10237" s="T121">adj</ta>
            <ta e="T123" id="Seg_10238" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_10239" s="T123">cop</ta>
            <ta e="T125" id="Seg_10240" s="T124">n</ta>
            <ta e="T126" id="Seg_10241" s="T125">n</ta>
            <ta e="T127" id="Seg_10242" s="T126">n</ta>
            <ta e="T128" id="Seg_10243" s="T127">n</ta>
            <ta e="T129" id="Seg_10244" s="T128">adj</ta>
            <ta e="T130" id="Seg_10245" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_10246" s="T130">cop</ta>
            <ta e="T132" id="Seg_10247" s="T131">adj</ta>
            <ta e="T133" id="Seg_10248" s="T132">v</ta>
            <ta e="T134" id="Seg_10249" s="T133">v</ta>
            <ta e="T135" id="Seg_10250" s="T134">v</ta>
            <ta e="T136" id="Seg_10251" s="T135">interj</ta>
            <ta e="T137" id="Seg_10252" s="T136">n</ta>
            <ta e="T138" id="Seg_10253" s="T137">adv</ta>
            <ta e="T139" id="Seg_10254" s="T138">adv</ta>
            <ta e="T140" id="Seg_10255" s="T139">dempro</ta>
            <ta e="T141" id="Seg_10256" s="T140">n</ta>
            <ta e="T143" id="Seg_10257" s="T142">v</ta>
            <ta e="T144" id="Seg_10258" s="T143">v</ta>
            <ta e="T145" id="Seg_10259" s="T144">n</ta>
            <ta e="T146" id="Seg_10260" s="T145">v</ta>
            <ta e="T147" id="Seg_10261" s="T146">v</ta>
            <ta e="T148" id="Seg_10262" s="T147">v</ta>
            <ta e="T149" id="Seg_10263" s="T148">interj</ta>
            <ta e="T150" id="Seg_10264" s="T149">adj</ta>
            <ta e="T151" id="Seg_10265" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_10266" s="T151">cop</ta>
            <ta e="T153" id="Seg_10267" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_10268" s="T153">v</ta>
            <ta e="T155" id="Seg_10269" s="T154">dempro</ta>
            <ta e="T156" id="Seg_10270" s="T155">n</ta>
            <ta e="T157" id="Seg_10271" s="T156">n</ta>
            <ta e="T158" id="Seg_10272" s="T157">adv</ta>
            <ta e="T159" id="Seg_10273" s="T158">v</ta>
            <ta e="T160" id="Seg_10274" s="T159">dempro</ta>
            <ta e="T161" id="Seg_10275" s="T160">n</ta>
            <ta e="T162" id="Seg_10276" s="T161">n</ta>
            <ta e="T163" id="Seg_10277" s="T162">n</ta>
            <ta e="T164" id="Seg_10278" s="T163">que</ta>
            <ta e="T165" id="Seg_10279" s="T164">adj</ta>
            <ta e="T166" id="Seg_10280" s="T165">adj</ta>
            <ta e="T167" id="Seg_10281" s="T166">cop</ta>
            <ta e="T168" id="Seg_10282" s="T167">adv</ta>
            <ta e="T169" id="Seg_10283" s="T168">cop</ta>
            <ta e="T170" id="Seg_10284" s="T169">n</ta>
            <ta e="T171" id="Seg_10285" s="T170">adj</ta>
            <ta e="T172" id="Seg_10286" s="T171">v</ta>
            <ta e="T173" id="Seg_10287" s="T172">n</ta>
            <ta e="T174" id="Seg_10288" s="T173">cop</ta>
            <ta e="T175" id="Seg_10289" s="T174">n</ta>
            <ta e="T176" id="Seg_10290" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_10291" s="T176">n</ta>
            <ta e="T178" id="Seg_10292" s="T177">n</ta>
            <ta e="T179" id="Seg_10293" s="T178">n</ta>
            <ta e="T180" id="Seg_10294" s="T179">v</ta>
            <ta e="T181" id="Seg_10295" s="T180">v</ta>
            <ta e="T182" id="Seg_10296" s="T181">n</ta>
            <ta e="T183" id="Seg_10297" s="T182">v</ta>
            <ta e="T184" id="Seg_10298" s="T183">adv</ta>
            <ta e="T185" id="Seg_10299" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_10300" s="T185">ptcl</ta>
            <ta e="T187" id="Seg_10301" s="T186">adj</ta>
            <ta e="T188" id="Seg_10302" s="T187">cop</ta>
            <ta e="T189" id="Seg_10303" s="T188">dempro</ta>
            <ta e="T190" id="Seg_10304" s="T189">n</ta>
            <ta e="T191" id="Seg_10305" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_10306" s="T191">adv</ta>
            <ta e="T193" id="Seg_10307" s="T192">v</ta>
            <ta e="T194" id="Seg_10308" s="T193">v</ta>
            <ta e="T195" id="Seg_10309" s="T194">v</ta>
            <ta e="T196" id="Seg_10310" s="T195">n</ta>
            <ta e="T197" id="Seg_10311" s="T196">v</ta>
            <ta e="T198" id="Seg_10312" s="T197">aux</ta>
            <ta e="T199" id="Seg_10313" s="T198">dempro</ta>
            <ta e="T200" id="Seg_10314" s="T199">adv</ta>
            <ta e="T201" id="Seg_10315" s="T200">dempro</ta>
            <ta e="T202" id="Seg_10316" s="T201">adv</ta>
            <ta e="T203" id="Seg_10317" s="T202">n</ta>
            <ta e="T204" id="Seg_10318" s="T203">que</ta>
            <ta e="T205" id="Seg_10319" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_10320" s="T205">v</ta>
            <ta e="T207" id="Seg_10321" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_10322" s="T207">que</ta>
            <ta e="T209" id="Seg_10323" s="T208">adv</ta>
            <ta e="T210" id="Seg_10324" s="T209">n</ta>
            <ta e="T211" id="Seg_10325" s="T210">v</ta>
            <ta e="T212" id="Seg_10326" s="T211">n</ta>
            <ta e="T213" id="Seg_10327" s="T212">v</ta>
            <ta e="T214" id="Seg_10328" s="T213">n</ta>
            <ta e="T215" id="Seg_10329" s="T214">v</ta>
            <ta e="T216" id="Seg_10330" s="T215">n</ta>
            <ta e="T217" id="Seg_10331" s="T216">dempro</ta>
            <ta e="T218" id="Seg_10332" s="T217">v</ta>
            <ta e="T219" id="Seg_10333" s="T218">post</ta>
            <ta e="T220" id="Seg_10334" s="T219">v</ta>
            <ta e="T221" id="Seg_10335" s="T220">n</ta>
            <ta e="T222" id="Seg_10336" s="T221">adv</ta>
            <ta e="T223" id="Seg_10337" s="T222">v</ta>
            <ta e="T224" id="Seg_10338" s="T223">n</ta>
            <ta e="T225" id="Seg_10339" s="T224">v</ta>
            <ta e="T226" id="Seg_10340" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_10341" s="T226">dempro</ta>
            <ta e="T228" id="Seg_10342" s="T227">v</ta>
            <ta e="T229" id="Seg_10343" s="T228">n</ta>
            <ta e="T230" id="Seg_10344" s="T229">adj</ta>
            <ta e="T231" id="Seg_10345" s="T230">v</ta>
            <ta e="T232" id="Seg_10346" s="T231">que</ta>
            <ta e="T233" id="Seg_10347" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_10348" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_10349" s="T234">cop</ta>
            <ta e="T236" id="Seg_10350" s="T235">adj</ta>
            <ta e="T237" id="Seg_10351" s="T236">n</ta>
            <ta e="T238" id="Seg_10352" s="T237">post</ta>
            <ta e="T239" id="Seg_10353" s="T238">v</ta>
            <ta e="T240" id="Seg_10354" s="T239">n</ta>
            <ta e="T241" id="Seg_10355" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_10356" s="T241">cop</ta>
            <ta e="T243" id="Seg_10357" s="T242">adv</ta>
            <ta e="T244" id="Seg_10358" s="T243">adv</ta>
            <ta e="T245" id="Seg_10359" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_10360" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_10361" s="T246">dempro</ta>
            <ta e="T248" id="Seg_10362" s="T247">n</ta>
            <ta e="T249" id="Seg_10363" s="T248">v</ta>
            <ta e="T250" id="Seg_10364" s="T249">n</ta>
            <ta e="T251" id="Seg_10365" s="T250">n</ta>
            <ta e="T252" id="Seg_10366" s="T251">v</ta>
            <ta e="T253" id="Seg_10367" s="T252">n</ta>
            <ta e="T254" id="Seg_10368" s="T253">n</ta>
            <ta e="T255" id="Seg_10369" s="T254">adv</ta>
            <ta e="T256" id="Seg_10370" s="T255">v</ta>
            <ta e="T257" id="Seg_10371" s="T256">aux</ta>
            <ta e="T258" id="Seg_10372" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_10373" s="T258">n</ta>
            <ta e="T260" id="Seg_10374" s="T259">dempro</ta>
            <ta e="T261" id="Seg_10375" s="T260">v</ta>
            <ta e="T262" id="Seg_10376" s="T261">aux</ta>
            <ta e="T263" id="Seg_10377" s="T262">dempro</ta>
            <ta e="T264" id="Seg_10378" s="T263">v</ta>
            <ta e="T265" id="Seg_10379" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_10380" s="T265">v</ta>
            <ta e="T267" id="Seg_10381" s="T266">v</ta>
            <ta e="T268" id="Seg_10382" s="T267">dempro</ta>
            <ta e="T269" id="Seg_10383" s="T268">n</ta>
            <ta e="T270" id="Seg_10384" s="T269">n</ta>
            <ta e="T271" id="Seg_10385" s="T270">v</ta>
            <ta e="T272" id="Seg_10386" s="T271">dempro</ta>
            <ta e="T273" id="Seg_10387" s="T272">adj</ta>
            <ta e="T274" id="Seg_10388" s="T273">v</ta>
            <ta e="T275" id="Seg_10389" s="T274">n</ta>
            <ta e="T276" id="Seg_10390" s="T275">n</ta>
            <ta e="T277" id="Seg_10391" s="T276">v</ta>
            <ta e="T278" id="Seg_10392" s="T277">adj</ta>
            <ta e="T279" id="Seg_10393" s="T278">v</ta>
            <ta e="T280" id="Seg_10394" s="T279">dempro</ta>
            <ta e="T281" id="Seg_10395" s="T280">n</ta>
            <ta e="T282" id="Seg_10396" s="T281">v</ta>
            <ta e="T283" id="Seg_10397" s="T282">n</ta>
            <ta e="T284" id="Seg_10398" s="T283">v</ta>
            <ta e="T285" id="Seg_10399" s="T284">n</ta>
            <ta e="T286" id="Seg_10400" s="T285">post</ta>
            <ta e="T287" id="Seg_10401" s="T286">v</ta>
            <ta e="T288" id="Seg_10402" s="T287">interj</ta>
            <ta e="T289" id="Seg_10403" s="T288">interj</ta>
            <ta e="T290" id="Seg_10404" s="T289">interj</ta>
            <ta e="T291" id="Seg_10405" s="T290">interj</ta>
            <ta e="T292" id="Seg_10406" s="T291">n</ta>
            <ta e="T293" id="Seg_10407" s="T292">n</ta>
            <ta e="T294" id="Seg_10408" s="T293">n</ta>
            <ta e="T295" id="Seg_10409" s="T294">n</ta>
            <ta e="T296" id="Seg_10410" s="T295">interj</ta>
            <ta e="T297" id="Seg_10411" s="T296">interj</ta>
            <ta e="T298" id="Seg_10412" s="T297">interj</ta>
            <ta e="T299" id="Seg_10413" s="T298">n</ta>
            <ta e="T300" id="Seg_10414" s="T299">n</ta>
            <ta e="T301" id="Seg_10415" s="T300">n</ta>
            <ta e="T302" id="Seg_10416" s="T301">n</ta>
            <ta e="T303" id="Seg_10417" s="T302">v</ta>
            <ta e="T304" id="Seg_10418" s="T303">v</ta>
            <ta e="T305" id="Seg_10419" s="T304">v</ta>
            <ta e="T306" id="Seg_10420" s="T305">v</ta>
            <ta e="T307" id="Seg_10421" s="T306">interj</ta>
            <ta e="T308" id="Seg_10422" s="T307">n</ta>
            <ta e="T309" id="Seg_10423" s="T308">n</ta>
            <ta e="T310" id="Seg_10424" s="T309">n</ta>
            <ta e="T311" id="Seg_10425" s="T310">n</ta>
            <ta e="T312" id="Seg_10426" s="T311">v</ta>
            <ta e="T313" id="Seg_10427" s="T312">v</ta>
            <ta e="T314" id="Seg_10428" s="T313">v</ta>
            <ta e="T315" id="Seg_10429" s="T314">v</ta>
            <ta e="T316" id="Seg_10430" s="T315">adv</ta>
            <ta e="T317" id="Seg_10431" s="T316">adv</ta>
            <ta e="T318" id="Seg_10432" s="T317">n</ta>
            <ta e="T319" id="Seg_10433" s="T318">adv</ta>
            <ta e="T320" id="Seg_10434" s="T319">n</ta>
            <ta e="T321" id="Seg_10435" s="T320">v</ta>
            <ta e="T322" id="Seg_10436" s="T321">n</ta>
            <ta e="T323" id="Seg_10437" s="T322">v</ta>
            <ta e="T324" id="Seg_10438" s="T323">ptcl</ta>
            <ta e="T325" id="Seg_10439" s="T324">n</ta>
            <ta e="T326" id="Seg_10440" s="T325">n</ta>
            <ta e="T327" id="Seg_10441" s="T326">adv</ta>
            <ta e="T328" id="Seg_10442" s="T327">v</ta>
            <ta e="T329" id="Seg_10443" s="T328">v</ta>
            <ta e="T330" id="Seg_10444" s="T329">v</ta>
            <ta e="T331" id="Seg_10445" s="T330">dempro</ta>
            <ta e="T332" id="Seg_10446" s="T331">v</ta>
            <ta e="T333" id="Seg_10447" s="T332">n</ta>
            <ta e="T334" id="Seg_10448" s="T333">post</ta>
            <ta e="T335" id="Seg_10449" s="T334">n</ta>
            <ta e="T336" id="Seg_10450" s="T335">post</ta>
            <ta e="T337" id="Seg_10451" s="T336">n</ta>
            <ta e="T338" id="Seg_10452" s="T337">v</ta>
            <ta e="T339" id="Seg_10453" s="T338">n</ta>
            <ta e="T340" id="Seg_10454" s="T339">post</ta>
            <ta e="T341" id="Seg_10455" s="T340">que</ta>
            <ta e="T342" id="Seg_10456" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_10457" s="T342">v</ta>
            <ta e="T344" id="Seg_10458" s="T343">ptcl</ta>
            <ta e="T345" id="Seg_10459" s="T344">adj</ta>
            <ta e="T346" id="Seg_10460" s="T345">n</ta>
            <ta e="T347" id="Seg_10461" s="T346">adv</ta>
            <ta e="T348" id="Seg_10462" s="T347">v</ta>
            <ta e="T349" id="Seg_10463" s="T348">v</ta>
            <ta e="T350" id="Seg_10464" s="T349">dempro</ta>
            <ta e="T351" id="Seg_10465" s="T350">n</ta>
            <ta e="T352" id="Seg_10466" s="T351">adv</ta>
            <ta e="T353" id="Seg_10467" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_10468" s="T353">ptcl</ta>
            <ta e="T355" id="Seg_10469" s="T354">dempro</ta>
            <ta e="T357" id="Seg_10470" s="T356">adv</ta>
            <ta e="T358" id="Seg_10471" s="T357">n</ta>
            <ta e="T359" id="Seg_10472" s="T358">v</ta>
            <ta e="T360" id="Seg_10473" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_10474" s="T360">interj</ta>
            <ta e="T362" id="Seg_10475" s="T361">interj</ta>
            <ta e="T363" id="Seg_10476" s="T362">interj</ta>
            <ta e="T364" id="Seg_10477" s="T363">adj</ta>
            <ta e="T365" id="Seg_10478" s="T364">v</ta>
            <ta e="T366" id="Seg_10479" s="T365">n</ta>
            <ta e="T367" id="Seg_10480" s="T366">n</ta>
            <ta e="T368" id="Seg_10481" s="T367">interj</ta>
            <ta e="T369" id="Seg_10482" s="T368">interj</ta>
            <ta e="T370" id="Seg_10483" s="T369">interj</ta>
            <ta e="T371" id="Seg_10484" s="T370">adj</ta>
            <ta e="T372" id="Seg_10485" s="T371">v</ta>
            <ta e="T373" id="Seg_10486" s="T372">v</ta>
            <ta e="T374" id="Seg_10487" s="T373">interj</ta>
            <ta e="T375" id="Seg_10488" s="T374">interj</ta>
            <ta e="T376" id="Seg_10489" s="T375">interj</ta>
            <ta e="T377" id="Seg_10490" s="T376">n</ta>
            <ta e="T378" id="Seg_10491" s="T377">n</ta>
            <ta e="T379" id="Seg_10492" s="T378">n</ta>
            <ta e="T380" id="Seg_10493" s="T379">n</ta>
            <ta e="T381" id="Seg_10494" s="T380">n</ta>
            <ta e="T382" id="Seg_10495" s="T381">n</ta>
            <ta e="T383" id="Seg_10496" s="T382">n</ta>
            <ta e="T384" id="Seg_10497" s="T383">n</ta>
            <ta e="T385" id="Seg_10498" s="T384">v</ta>
            <ta e="T386" id="Seg_10499" s="T385">v</ta>
            <ta e="T387" id="Seg_10500" s="T386">v</ta>
            <ta e="T388" id="Seg_10501" s="T387">adv</ta>
            <ta e="T389" id="Seg_10502" s="T388">n</ta>
            <ta e="T390" id="Seg_10503" s="T389">v</ta>
            <ta e="T391" id="Seg_10504" s="T390">dempro</ta>
            <ta e="T392" id="Seg_10505" s="T391">v</ta>
            <ta e="T393" id="Seg_10506" s="T392">n</ta>
            <ta e="T394" id="Seg_10507" s="T393">v</ta>
            <ta e="T395" id="Seg_10508" s="T394">dempro</ta>
            <ta e="T396" id="Seg_10509" s="T395">v</ta>
            <ta e="T397" id="Seg_10510" s="T396">n</ta>
            <ta e="T398" id="Seg_10511" s="T397">v</ta>
            <ta e="T399" id="Seg_10512" s="T398">n</ta>
            <ta e="T400" id="Seg_10513" s="T399">adv</ta>
            <ta e="T401" id="Seg_10514" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_10515" s="T401">n</ta>
            <ta e="T403" id="Seg_10516" s="T402">v</ta>
            <ta e="T404" id="Seg_10517" s="T403">n</ta>
            <ta e="T405" id="Seg_10518" s="T404">n</ta>
            <ta e="T406" id="Seg_10519" s="T405">dempro</ta>
            <ta e="T407" id="Seg_10520" s="T406">n</ta>
            <ta e="T408" id="Seg_10521" s="T407">v</ta>
            <ta e="T409" id="Seg_10522" s="T408">n</ta>
            <ta e="T410" id="Seg_10523" s="T409">n</ta>
            <ta e="T411" id="Seg_10524" s="T410">dempro</ta>
            <ta e="T412" id="Seg_10525" s="T411">n</ta>
            <ta e="T413" id="Seg_10526" s="T412">n</ta>
            <ta e="T414" id="Seg_10527" s="T413">n</ta>
            <ta e="T415" id="Seg_10528" s="T414">adj</ta>
            <ta e="T416" id="Seg_10529" s="T415">cop</ta>
            <ta e="T417" id="Seg_10530" s="T416">adv</ta>
            <ta e="T418" id="Seg_10531" s="T417">n</ta>
            <ta e="T419" id="Seg_10532" s="T418">v</ta>
            <ta e="T420" id="Seg_10533" s="T419">post</ta>
            <ta e="T421" id="Seg_10534" s="T420">n</ta>
            <ta e="T422" id="Seg_10535" s="T421">n</ta>
            <ta e="T423" id="Seg_10536" s="T422">v</ta>
            <ta e="T424" id="Seg_10537" s="T423">n</ta>
            <ta e="T425" id="Seg_10538" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_10539" s="T425">v</ta>
            <ta e="T427" id="Seg_10540" s="T426">n</ta>
            <ta e="T428" id="Seg_10541" s="T427">n</ta>
            <ta e="T429" id="Seg_10542" s="T428">v</ta>
            <ta e="T430" id="Seg_10543" s="T429">n</ta>
            <ta e="T431" id="Seg_10544" s="T430">adj</ta>
            <ta e="T432" id="Seg_10545" s="T431">v</ta>
            <ta e="T433" id="Seg_10546" s="T432">adj</ta>
            <ta e="T434" id="Seg_10547" s="T433">v</ta>
            <ta e="T435" id="Seg_10548" s="T434">v</ta>
            <ta e="T436" id="Seg_10549" s="T435">adv</ta>
            <ta e="T437" id="Seg_10550" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_10551" s="T437">n</ta>
            <ta e="T439" id="Seg_10552" s="T438">n</ta>
            <ta e="T440" id="Seg_10553" s="T439">v</ta>
            <ta e="T441" id="Seg_10554" s="T440">n</ta>
            <ta e="T442" id="Seg_10555" s="T441">n</ta>
            <ta e="T443" id="Seg_10556" s="T442">v</ta>
            <ta e="T444" id="Seg_10557" s="T443">adj</ta>
            <ta e="T445" id="Seg_10558" s="T444">n</ta>
            <ta e="T446" id="Seg_10559" s="T445">v</ta>
            <ta e="T447" id="Seg_10560" s="T446">que</ta>
            <ta e="T448" id="Seg_10561" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_10562" s="T448">n</ta>
            <ta e="T450" id="Seg_10563" s="T449">v</ta>
            <ta e="T451" id="Seg_10564" s="T450">adv</ta>
            <ta e="T452" id="Seg_10565" s="T451">v</ta>
            <ta e="T453" id="Seg_10566" s="T452">n</ta>
            <ta e="T454" id="Seg_10567" s="T453">adv</ta>
            <ta e="T455" id="Seg_10568" s="T454">v</ta>
            <ta e="T456" id="Seg_10569" s="T455">v</ta>
            <ta e="T457" id="Seg_10570" s="T456">adv</ta>
            <ta e="T458" id="Seg_10571" s="T457">dempro</ta>
            <ta e="T459" id="Seg_10572" s="T458">n</ta>
            <ta e="T460" id="Seg_10573" s="T459">v</ta>
            <ta e="T461" id="Seg_10574" s="T460">post</ta>
            <ta e="T462" id="Seg_10575" s="T461">n</ta>
            <ta e="T463" id="Seg_10576" s="T462">v</ta>
            <ta e="T464" id="Seg_10577" s="T463">dempro</ta>
            <ta e="T465" id="Seg_10578" s="T464">n</ta>
            <ta e="T466" id="Seg_10579" s="T465">v</ta>
            <ta e="T467" id="Seg_10580" s="T466">aux</ta>
            <ta e="T468" id="Seg_10581" s="T467">dempro</ta>
            <ta e="T469" id="Seg_10582" s="T468">v</ta>
            <ta e="T470" id="Seg_10583" s="T469">aux</ta>
            <ta e="T471" id="Seg_10584" s="T470">post</ta>
            <ta e="T472" id="Seg_10585" s="T471">dempro</ta>
            <ta e="T473" id="Seg_10586" s="T472">v</ta>
            <ta e="T474" id="Seg_10587" s="T473">n</ta>
            <ta e="T475" id="Seg_10588" s="T474">v</ta>
            <ta e="T476" id="Seg_10589" s="T475">dempro</ta>
            <ta e="T477" id="Seg_10590" s="T476">adv</ta>
            <ta e="T478" id="Seg_10591" s="T477">v</ta>
            <ta e="T479" id="Seg_10592" s="T478">aux</ta>
            <ta e="T480" id="Seg_10593" s="T479">v</ta>
            <ta e="T481" id="Seg_10594" s="T480">n</ta>
            <ta e="T482" id="Seg_10595" s="T481">n</ta>
            <ta e="T483" id="Seg_10596" s="T482">v</ta>
            <ta e="T484" id="Seg_10597" s="T483">n</ta>
            <ta e="T485" id="Seg_10598" s="T484">n</ta>
            <ta e="T486" id="Seg_10599" s="T485">n</ta>
            <ta e="T487" id="Seg_10600" s="T486">v</ta>
            <ta e="T488" id="Seg_10601" s="T487">v</ta>
            <ta e="T489" id="Seg_10602" s="T488">adv</ta>
            <ta e="T490" id="Seg_10603" s="T489">n</ta>
            <ta e="T491" id="Seg_10604" s="T490">v</ta>
            <ta e="T492" id="Seg_10605" s="T491">dempro</ta>
            <ta e="T493" id="Seg_10606" s="T492">n</ta>
            <ta e="T494" id="Seg_10607" s="T493">v</ta>
            <ta e="T495" id="Seg_10608" s="T494">v</ta>
            <ta e="T496" id="Seg_10609" s="T495">dempro</ta>
            <ta e="T497" id="Seg_10610" s="T496">n</ta>
            <ta e="T498" id="Seg_10611" s="T497">v</ta>
            <ta e="T499" id="Seg_10612" s="T498">v</ta>
            <ta e="T500" id="Seg_10613" s="T499">dempro</ta>
            <ta e="T501" id="Seg_10614" s="T500">n</ta>
            <ta e="T502" id="Seg_10615" s="T501">dempro</ta>
            <ta e="T503" id="Seg_10616" s="T502">v</ta>
            <ta e="T504" id="Seg_10617" s="T503">n</ta>
            <ta e="T505" id="Seg_10618" s="T504">n</ta>
            <ta e="T506" id="Seg_10619" s="T505">n</ta>
            <ta e="T507" id="Seg_10620" s="T506">dempro</ta>
            <ta e="T508" id="Seg_10621" s="T507">v</ta>
            <ta e="T509" id="Seg_10622" s="T508">n</ta>
            <ta e="T510" id="Seg_10623" s="T509">v</ta>
            <ta e="T511" id="Seg_10624" s="T510">dempro</ta>
            <ta e="T512" id="Seg_10625" s="T511">n</ta>
            <ta e="T513" id="Seg_10626" s="T512">n</ta>
            <ta e="T514" id="Seg_10627" s="T513">v</ta>
            <ta e="T515" id="Seg_10628" s="T514">v</ta>
            <ta e="T516" id="Seg_10629" s="T515">dempro</ta>
            <ta e="T517" id="Seg_10630" s="T516">n</ta>
            <ta e="T518" id="Seg_10631" s="T517">n</ta>
            <ta e="T519" id="Seg_10632" s="T518">v</ta>
            <ta e="T520" id="Seg_10633" s="T519">dempro</ta>
            <ta e="T521" id="Seg_10634" s="T520">n</ta>
            <ta e="T522" id="Seg_10635" s="T521">v</ta>
            <ta e="T523" id="Seg_10636" s="T522">post</ta>
            <ta e="T524" id="Seg_10637" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_10638" s="T524">v</ta>
            <ta e="T526" id="Seg_10639" s="T525">aux</ta>
            <ta e="T527" id="Seg_10640" s="T526">n</ta>
            <ta e="T528" id="Seg_10641" s="T527">n</ta>
            <ta e="T529" id="Seg_10642" s="T528">v</ta>
            <ta e="T530" id="Seg_10643" s="T529">n</ta>
            <ta e="T531" id="Seg_10644" s="T530">n</ta>
            <ta e="T532" id="Seg_10645" s="T531">v</ta>
            <ta e="T533" id="Seg_10646" s="T532">v</ta>
            <ta e="T534" id="Seg_10647" s="T533">aux</ta>
            <ta e="T535" id="Seg_10648" s="T534">adv</ta>
            <ta e="T536" id="Seg_10649" s="T535">adv</ta>
            <ta e="T537" id="Seg_10650" s="T536">v</ta>
            <ta e="T538" id="Seg_10651" s="T537">cardnum</ta>
            <ta e="T539" id="Seg_10652" s="T538">n</ta>
            <ta e="T540" id="Seg_10653" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_10654" s="T540">que</ta>
            <ta e="T542" id="Seg_10655" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_10656" s="T542">n</ta>
            <ta e="T544" id="Seg_10657" s="T543">cop</ta>
            <ta e="T545" id="Seg_10658" s="T544">post</ta>
            <ta e="T546" id="Seg_10659" s="T545">v</ta>
            <ta e="T547" id="Seg_10660" s="T546">n</ta>
            <ta e="T548" id="Seg_10661" s="T547">n</ta>
            <ta e="T549" id="Seg_10662" s="T548">adv</ta>
            <ta e="T550" id="Seg_10663" s="T549">ptcl</ta>
            <ta e="T551" id="Seg_10664" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_10665" s="T551">n</ta>
            <ta e="T553" id="Seg_10666" s="T552">n</ta>
            <ta e="T554" id="Seg_10667" s="T553">v</ta>
            <ta e="T555" id="Seg_10668" s="T554">dempro</ta>
            <ta e="T556" id="Seg_10669" s="T555">n</ta>
            <ta e="T557" id="Seg_10670" s="T556">n</ta>
            <ta e="T558" id="Seg_10671" s="T557">v</ta>
            <ta e="T559" id="Seg_10672" s="T558">n</ta>
            <ta e="T560" id="Seg_10673" s="T559">ptcl</ta>
            <ta e="T561" id="Seg_10674" s="T560">v</ta>
            <ta e="T562" id="Seg_10675" s="T561">que</ta>
            <ta e="T563" id="Seg_10676" s="T562">ptcl</ta>
            <ta e="T564" id="Seg_10677" s="T563">v</ta>
            <ta e="T565" id="Seg_10678" s="T564">post</ta>
            <ta e="T566" id="Seg_10679" s="T565">v</ta>
            <ta e="T567" id="Seg_10680" s="T566">aux</ta>
            <ta e="T568" id="Seg_10681" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_10682" s="T568">adv</ta>
            <ta e="T570" id="Seg_10683" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_10684" s="T570">ptcl</ta>
            <ta e="T573" id="Seg_10685" s="T572">n</ta>
            <ta e="T574" id="Seg_10686" s="T573">adv</ta>
            <ta e="T575" id="Seg_10687" s="T574">v</ta>
            <ta e="T576" id="Seg_10688" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_10689" s="T576">v</ta>
            <ta e="T578" id="Seg_10690" s="T577">aux</ta>
            <ta e="T579" id="Seg_10691" s="T578">n</ta>
            <ta e="T580" id="Seg_10692" s="T579">n</ta>
            <ta e="T581" id="Seg_10693" s="T580">v</ta>
            <ta e="T582" id="Seg_10694" s="T581">adv</ta>
            <ta e="T583" id="Seg_10695" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_10696" s="T583">n</ta>
            <ta e="T586" id="Seg_10697" s="T585">n</ta>
            <ta e="T587" id="Seg_10698" s="T586">v</ta>
            <ta e="T588" id="Seg_10699" s="T587">post</ta>
            <ta e="T589" id="Seg_10700" s="T588">dempro</ta>
            <ta e="T590" id="Seg_10701" s="T589">n</ta>
            <ta e="T591" id="Seg_10702" s="T590">n</ta>
            <ta e="T592" id="Seg_10703" s="T591">v</ta>
            <ta e="T593" id="Seg_10704" s="T592">aux</ta>
            <ta e="T594" id="Seg_10705" s="T593">post</ta>
            <ta e="T595" id="Seg_10706" s="T594">dempro</ta>
            <ta e="T596" id="Seg_10707" s="T595">n</ta>
            <ta e="T597" id="Seg_10708" s="T596">dempro</ta>
            <ta e="T598" id="Seg_10709" s="T597">n</ta>
            <ta e="T599" id="Seg_10710" s="T598">dempro</ta>
            <ta e="T601" id="Seg_10711" s="T600">n</ta>
            <ta e="T602" id="Seg_10712" s="T601">v</ta>
            <ta e="T603" id="Seg_10713" s="T602">aux</ta>
            <ta e="T604" id="Seg_10714" s="T603">dempro</ta>
            <ta e="T605" id="Seg_10715" s="T604">n</ta>
            <ta e="T606" id="Seg_10716" s="T605">v</ta>
            <ta e="T607" id="Seg_10717" s="T606">dempro</ta>
            <ta e="T608" id="Seg_10718" s="T607">n</ta>
            <ta e="T609" id="Seg_10719" s="T608">n</ta>
            <ta e="T610" id="Seg_10720" s="T609">n</ta>
            <ta e="T611" id="Seg_10721" s="T610">post</ta>
            <ta e="T612" id="Seg_10722" s="T611">dempro</ta>
            <ta e="T613" id="Seg_10723" s="T612">n</ta>
            <ta e="T614" id="Seg_10724" s="T613">v</ta>
            <ta e="T615" id="Seg_10725" s="T614">n</ta>
            <ta e="T616" id="Seg_10726" s="T615">n</ta>
            <ta e="T617" id="Seg_10727" s="T616">n</ta>
            <ta e="T618" id="Seg_10728" s="T617">v</ta>
            <ta e="T619" id="Seg_10729" s="T618">n</ta>
            <ta e="T620" id="Seg_10730" s="T619">n</ta>
            <ta e="T621" id="Seg_10731" s="T620">v</ta>
            <ta e="T622" id="Seg_10732" s="T621">n</ta>
            <ta e="T623" id="Seg_10733" s="T622">adv</ta>
            <ta e="T624" id="Seg_10734" s="T623">v</ta>
            <ta e="T625" id="Seg_10735" s="T624">adj</ta>
            <ta e="T626" id="Seg_10736" s="T625">n</ta>
            <ta e="T627" id="Seg_10737" s="T626">adj</ta>
            <ta e="T628" id="Seg_10738" s="T627">n</ta>
            <ta e="T629" id="Seg_10739" s="T628">v</ta>
            <ta e="T630" id="Seg_10740" s="T629">n</ta>
            <ta e="T631" id="Seg_10741" s="T630">ptcl</ta>
            <ta e="T632" id="Seg_10742" s="T631">ptcl</ta>
            <ta e="T633" id="Seg_10743" s="T632">cop</ta>
            <ta e="T634" id="Seg_10744" s="T633">n</ta>
            <ta e="T635" id="Seg_10745" s="T634">v</ta>
            <ta e="T636" id="Seg_10746" s="T635">v</ta>
            <ta e="T637" id="Seg_10747" s="T636">v</ta>
            <ta e="T638" id="Seg_10748" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_10749" s="T638">dempro</ta>
            <ta e="T640" id="Seg_10750" s="T639">n</ta>
            <ta e="T641" id="Seg_10751" s="T640">n</ta>
            <ta e="T642" id="Seg_10752" s="T641">v</ta>
            <ta e="T643" id="Seg_10753" s="T642">aux</ta>
            <ta e="T644" id="Seg_10754" s="T643">n</ta>
            <ta e="T645" id="Seg_10755" s="T644">v</ta>
            <ta e="T646" id="Seg_10756" s="T645">v</ta>
            <ta e="T647" id="Seg_10757" s="T646">n</ta>
            <ta e="T648" id="Seg_10758" s="T647">adj</ta>
            <ta e="T649" id="Seg_10759" s="T648">cop</ta>
            <ta e="T650" id="Seg_10760" s="T649">dempro</ta>
            <ta e="T651" id="Seg_10761" s="T650">n</ta>
            <ta e="T652" id="Seg_10762" s="T651">v</ta>
            <ta e="T653" id="Seg_10763" s="T652">aux</ta>
            <ta e="T654" id="Seg_10764" s="T653">dempro</ta>
            <ta e="T655" id="Seg_10765" s="T654">n</ta>
            <ta e="T656" id="Seg_10766" s="T655">v</ta>
            <ta e="T657" id="Seg_10767" s="T656">n</ta>
            <ta e="T658" id="Seg_10768" s="T657">v</ta>
            <ta e="T659" id="Seg_10769" s="T658">adv</ta>
            <ta e="T660" id="Seg_10770" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_10771" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_10772" s="T661">adv</ta>
            <ta e="T663" id="Seg_10773" s="T662">dempro</ta>
            <ta e="T664" id="Seg_10774" s="T663">n</ta>
            <ta e="T665" id="Seg_10775" s="T664">v</ta>
            <ta e="T666" id="Seg_10776" s="T665">n</ta>
            <ta e="T667" id="Seg_10777" s="T666">adj</ta>
            <ta e="T668" id="Seg_10778" s="T667">ptcl</ta>
            <ta e="T669" id="Seg_10779" s="T668">v</ta>
            <ta e="T670" id="Seg_10780" s="T669">n</ta>
            <ta e="T671" id="Seg_10781" s="T670">v</ta>
            <ta e="T672" id="Seg_10782" s="T671">ptcl</ta>
            <ta e="T673" id="Seg_10783" s="T672">v</ta>
            <ta e="T674" id="Seg_10784" s="T673">n</ta>
            <ta e="T675" id="Seg_10785" s="T674">n</ta>
            <ta e="T676" id="Seg_10786" s="T675">n</ta>
            <ta e="T677" id="Seg_10787" s="T676">post</ta>
            <ta e="T678" id="Seg_10788" s="T677">adv</ta>
            <ta e="T679" id="Seg_10789" s="T678">n</ta>
            <ta e="T680" id="Seg_10790" s="T679">v</ta>
            <ta e="T681" id="Seg_10791" s="T680">interj</ta>
            <ta e="T682" id="Seg_10792" s="T681">interj</ta>
            <ta e="T683" id="Seg_10793" s="T682">n</ta>
            <ta e="T684" id="Seg_10794" s="T683">n</ta>
            <ta e="T685" id="Seg_10795" s="T684">n</ta>
            <ta e="T686" id="Seg_10796" s="T685">n</ta>
            <ta e="T687" id="Seg_10797" s="T686">interj</ta>
            <ta e="T688" id="Seg_10798" s="T687">interj</ta>
            <ta e="T689" id="Seg_10799" s="T688">n</ta>
            <ta e="T690" id="Seg_10800" s="T689">n</ta>
            <ta e="T691" id="Seg_10801" s="T690">n</ta>
            <ta e="T692" id="Seg_10802" s="T691">n</ta>
            <ta e="T693" id="Seg_10803" s="T692">interj</ta>
            <ta e="T694" id="Seg_10804" s="T693">n</ta>
            <ta e="T695" id="Seg_10805" s="T694">n</ta>
            <ta e="T696" id="Seg_10806" s="T695">n</ta>
            <ta e="T697" id="Seg_10807" s="T696">n</ta>
            <ta e="T698" id="Seg_10808" s="T697">v</ta>
            <ta e="T699" id="Seg_10809" s="T698">v</ta>
            <ta e="T700" id="Seg_10810" s="T699">v</ta>
            <ta e="T701" id="Seg_10811" s="T700">v</ta>
            <ta e="T702" id="Seg_10812" s="T701">v</ta>
            <ta e="T703" id="Seg_10813" s="T702">v</ta>
            <ta e="T704" id="Seg_10814" s="T703">dempro</ta>
            <ta e="T705" id="Seg_10815" s="T704">dempro</ta>
            <ta e="T706" id="Seg_10816" s="T705">v</ta>
            <ta e="T707" id="Seg_10817" s="T706">post</ta>
            <ta e="T708" id="Seg_10818" s="T707">n</ta>
            <ta e="T709" id="Seg_10819" s="T708">v</ta>
            <ta e="T710" id="Seg_10820" s="T709">n</ta>
            <ta e="T711" id="Seg_10821" s="T710">adv</ta>
            <ta e="T712" id="Seg_10822" s="T711">ptcl</ta>
            <ta e="T713" id="Seg_10823" s="T712">n</ta>
            <ta e="T714" id="Seg_10824" s="T713">v</ta>
            <ta e="T715" id="Seg_10825" s="T714">v</ta>
            <ta e="T716" id="Seg_10826" s="T715">n</ta>
            <ta e="T717" id="Seg_10827" s="T716">dempro</ta>
            <ta e="T718" id="Seg_10828" s="T717">v</ta>
            <ta e="T719" id="Seg_10829" s="T718">n</ta>
            <ta e="T720" id="Seg_10830" s="T719">adv</ta>
            <ta e="T721" id="Seg_10831" s="T720">v</ta>
            <ta e="T722" id="Seg_10832" s="T721">v</ta>
            <ta e="T723" id="Seg_10833" s="T722">n</ta>
            <ta e="T724" id="Seg_10834" s="T723">v</ta>
            <ta e="T725" id="Seg_10835" s="T724">v</ta>
            <ta e="T726" id="Seg_10836" s="T725">aux</ta>
            <ta e="T727" id="Seg_10837" s="T726">v</ta>
            <ta e="T728" id="Seg_10838" s="T727">n</ta>
            <ta e="T729" id="Seg_10839" s="T728">v</ta>
            <ta e="T730" id="Seg_10840" s="T729">n</ta>
            <ta e="T731" id="Seg_10841" s="T730">dempro</ta>
            <ta e="T732" id="Seg_10842" s="T731">v</ta>
            <ta e="T733" id="Seg_10843" s="T732">post</ta>
            <ta e="T734" id="Seg_10844" s="T733">dempro</ta>
            <ta e="T735" id="Seg_10845" s="T734">v</ta>
            <ta e="T736" id="Seg_10846" s="T735">aux</ta>
            <ta e="T737" id="Seg_10847" s="T736">dempro</ta>
            <ta e="T738" id="Seg_10848" s="T737">v</ta>
            <ta e="T739" id="Seg_10849" s="T738">dempro</ta>
            <ta e="T740" id="Seg_10850" s="T739">interj</ta>
            <ta e="T741" id="Seg_10851" s="T740">n</ta>
            <ta e="T742" id="Seg_10852" s="T741">v</ta>
            <ta e="T743" id="Seg_10853" s="T742">v</ta>
            <ta e="T744" id="Seg_10854" s="T743">n</ta>
            <ta e="T745" id="Seg_10855" s="T744">adj</ta>
            <ta e="T746" id="Seg_10856" s="T745">n</ta>
            <ta e="T747" id="Seg_10857" s="T746">adj</ta>
            <ta e="T748" id="Seg_10858" s="T747">n</ta>
            <ta e="T749" id="Seg_10859" s="T748">n</ta>
            <ta e="T750" id="Seg_10860" s="T749">dempro</ta>
            <ta e="T751" id="Seg_10861" s="T750">v</ta>
            <ta e="T752" id="Seg_10862" s="T751">v</ta>
            <ta e="T753" id="Seg_10863" s="T752">interj</ta>
            <ta e="T754" id="Seg_10864" s="T753">dempro</ta>
            <ta e="T755" id="Seg_10865" s="T754">n</ta>
            <ta e="T756" id="Seg_10866" s="T755">n</ta>
            <ta e="T757" id="Seg_10867" s="T756">v</ta>
            <ta e="T758" id="Seg_10868" s="T757">aux</ta>
            <ta e="T759" id="Seg_10869" s="T758">n</ta>
            <ta e="T760" id="Seg_10870" s="T759">n</ta>
            <ta e="T761" id="Seg_10871" s="T760">v</ta>
            <ta e="T762" id="Seg_10872" s="T761">n</ta>
            <ta e="T763" id="Seg_10873" s="T762">n</ta>
            <ta e="T764" id="Seg_10874" s="T763">v</ta>
            <ta e="T765" id="Seg_10875" s="T764">aux</ta>
            <ta e="T766" id="Seg_10876" s="T765">n</ta>
            <ta e="T767" id="Seg_10877" s="T766">n</ta>
            <ta e="T768" id="Seg_10878" s="T767">v</ta>
            <ta e="T769" id="Seg_10879" s="T768">adv</ta>
            <ta e="T770" id="Seg_10880" s="T769">dempro</ta>
            <ta e="T771" id="Seg_10881" s="T770">cardnum</ta>
            <ta e="T772" id="Seg_10882" s="T771">n</ta>
            <ta e="T773" id="Seg_10883" s="T772">cop</ta>
            <ta e="T774" id="Seg_10884" s="T773">post</ta>
            <ta e="T775" id="Seg_10885" s="T774">n</ta>
            <ta e="T776" id="Seg_10886" s="T775">n</ta>
            <ta e="T777" id="Seg_10887" s="T776">v</ta>
            <ta e="T778" id="Seg_10888" s="T777">n</ta>
            <ta e="T779" id="Seg_10889" s="T778">n</ta>
            <ta e="T780" id="Seg_10890" s="T779">v</ta>
            <ta e="T781" id="Seg_10891" s="T780">adv</ta>
            <ta e="T782" id="Seg_10892" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_10893" s="T782">dempro</ta>
            <ta e="T784" id="Seg_10894" s="T783">n</ta>
            <ta e="T785" id="Seg_10895" s="T784">ptcl</ta>
            <ta e="T786" id="Seg_10896" s="T785">v</ta>
            <ta e="T787" id="Seg_10897" s="T786">adj</ta>
            <ta e="T788" id="Seg_10898" s="T787">n</ta>
            <ta e="T789" id="Seg_10899" s="T788">n</ta>
            <ta e="T790" id="Seg_10900" s="T789">n</ta>
            <ta e="T791" id="Seg_10901" s="T790">n</ta>
            <ta e="T792" id="Seg_10902" s="T791">v</ta>
            <ta e="T793" id="Seg_10903" s="T792">v</ta>
            <ta e="T794" id="Seg_10904" s="T793">n</ta>
            <ta e="T795" id="Seg_10905" s="T794">dempro</ta>
            <ta e="T796" id="Seg_10906" s="T795">que</ta>
            <ta e="T797" id="Seg_10907" s="T796">interj</ta>
            <ta e="T798" id="Seg_10908" s="T797">n</ta>
            <ta e="T799" id="Seg_10909" s="T798">post</ta>
            <ta e="T800" id="Seg_10910" s="T799">v</ta>
            <ta e="T801" id="Seg_10911" s="T800">dempro</ta>
            <ta e="T802" id="Seg_10912" s="T801">dempro</ta>
            <ta e="T803" id="Seg_10913" s="T802">ptcl</ta>
            <ta e="T804" id="Seg_10914" s="T803">v</ta>
            <ta e="T805" id="Seg_10915" s="T804">ptcl</ta>
            <ta e="T806" id="Seg_10916" s="T805">v</ta>
            <ta e="T807" id="Seg_10917" s="T806">n</ta>
            <ta e="T808" id="Seg_10918" s="T807">n</ta>
            <ta e="T809" id="Seg_10919" s="T808">dempro</ta>
            <ta e="T810" id="Seg_10920" s="T809">n</ta>
            <ta e="T811" id="Seg_10921" s="T810">dempro</ta>
            <ta e="T812" id="Seg_10922" s="T811">post</ta>
            <ta e="T813" id="Seg_10923" s="T812">ptcl</ta>
            <ta e="T814" id="Seg_10924" s="T813">ptcl</ta>
            <ta e="T815" id="Seg_10925" s="T814">n</ta>
            <ta e="T816" id="Seg_10926" s="T815">v</ta>
            <ta e="T817" id="Seg_10927" s="T816">v</ta>
            <ta e="T818" id="Seg_10928" s="T817">dempro</ta>
            <ta e="T819" id="Seg_10929" s="T818">post</ta>
            <ta e="T0" id="Seg_10930" s="T820">n</ta>
            <ta e="T822" id="Seg_10931" s="T821">conj</ta>
            <ta e="T823" id="Seg_10932" s="T822">n</ta>
            <ta e="T824" id="Seg_10933" s="T823">v</ta>
            <ta e="T825" id="Seg_10934" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_10935" s="T825">v</ta>
            <ta e="T827" id="Seg_10936" s="T826">v</ta>
            <ta e="T828" id="Seg_10937" s="T827">n</ta>
            <ta e="T829" id="Seg_10938" s="T828">v</ta>
            <ta e="T830" id="Seg_10939" s="T829">v</ta>
            <ta e="T831" id="Seg_10940" s="T830">dempro</ta>
            <ta e="T832" id="Seg_10941" s="T831">n</ta>
            <ta e="T833" id="Seg_10942" s="T832">cop</ta>
            <ta e="T834" id="Seg_10943" s="T833">n</ta>
            <ta e="T835" id="Seg_10944" s="T834">n</ta>
            <ta e="T836" id="Seg_10945" s="T835">ptcl</ta>
            <ta e="T837" id="Seg_10946" s="T836">adj</ta>
            <ta e="T838" id="Seg_10947" s="T837">cop</ta>
            <ta e="T839" id="Seg_10948" s="T838">adv</ta>
            <ta e="T840" id="Seg_10949" s="T839">n</ta>
            <ta e="T841" id="Seg_10950" s="T840">v</ta>
            <ta e="T842" id="Seg_10951" s="T841">ptcl</ta>
            <ta e="T843" id="Seg_10952" s="T842">dempro</ta>
            <ta e="T844" id="Seg_10953" s="T843">post</ta>
            <ta e="T845" id="Seg_10954" s="T844">n</ta>
            <ta e="T846" id="Seg_10955" s="T845">adj</ta>
            <ta e="T847" id="Seg_10956" s="T846">v</ta>
            <ta e="T848" id="Seg_10957" s="T847">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_10958" s="T3">np:Th</ta>
            <ta e="T6" id="Seg_10959" s="T5">np:L</ta>
            <ta e="T12" id="Seg_10960" s="T11">n:Time</ta>
            <ta e="T14" id="Seg_10961" s="T13">n:Time</ta>
            <ta e="T15" id="Seg_10962" s="T14">np:P</ta>
            <ta e="T16" id="Seg_10963" s="T15">0.3.h:A</ta>
            <ta e="T19" id="Seg_10964" s="T18">np:P</ta>
            <ta e="T20" id="Seg_10965" s="T19">0.3.h:A</ta>
            <ta e="T21" id="Seg_10966" s="T20">0.3.h:A</ta>
            <ta e="T27" id="Seg_10967" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_10968" s="T27">adv:Time</ta>
            <ta e="T29" id="Seg_10969" s="T28">adv:L</ta>
            <ta e="T32" id="Seg_10970" s="T31">np:P</ta>
            <ta e="T33" id="Seg_10971" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_10972" s="T33">np.h:A</ta>
            <ta e="T36" id="Seg_10973" s="T35">np.h:A</ta>
            <ta e="T38" id="Seg_10974" s="T37">np.h:A</ta>
            <ta e="T40" id="Seg_10975" s="T39">np.h:A</ta>
            <ta e="T43" id="Seg_10976" s="T42">0.3.h:A</ta>
            <ta e="T44" id="Seg_10977" s="T43">np:Th</ta>
            <ta e="T45" id="Seg_10978" s="T44">np:G</ta>
            <ta e="T51" id="Seg_10979" s="T50">np:Poss</ta>
            <ta e="T52" id="Seg_10980" s="T51">np:L</ta>
            <ta e="T59" id="Seg_10981" s="T58">np:Th</ta>
            <ta e="T65" id="Seg_10982" s="T64">np:Th</ta>
            <ta e="T68" id="Seg_10983" s="T67">np:L</ta>
            <ta e="T70" id="Seg_10984" s="T69">np.h:A</ta>
            <ta e="T71" id="Seg_10985" s="T70">adv:Time</ta>
            <ta e="T77" id="Seg_10986" s="T76">np:L</ta>
            <ta e="T81" id="Seg_10987" s="T80">np:L</ta>
            <ta e="T82" id="Seg_10988" s="T81">np:Th</ta>
            <ta e="T86" id="Seg_10989" s="T85">np:L</ta>
            <ta e="T88" id="Seg_10990" s="T87">np:Th</ta>
            <ta e="T90" id="Seg_10991" s="T89">np:Th</ta>
            <ta e="T91" id="Seg_10992" s="T90">0.3.h:A</ta>
            <ta e="T94" id="Seg_10993" s="T93">pro:G</ta>
            <ta e="T96" id="Seg_10994" s="T95">np.h:P</ta>
            <ta e="T98" id="Seg_10995" s="T97">np:G</ta>
            <ta e="T99" id="Seg_10996" s="T98">0.3.h:P</ta>
            <ta e="T100" id="Seg_10997" s="T99">0.3.h:A</ta>
            <ta e="T106" id="Seg_10998" s="T105">np:G</ta>
            <ta e="T107" id="Seg_10999" s="T106">0.3.h:P</ta>
            <ta e="T108" id="Seg_11000" s="T107">0.3.h:A</ta>
            <ta e="T110" id="Seg_11001" s="T109">np.h:A</ta>
            <ta e="T114" id="Seg_11002" s="T113">n:Time</ta>
            <ta e="T115" id="Seg_11003" s="T114">np:L</ta>
            <ta e="T121" id="Seg_11004" s="T120">np.h:Th</ta>
            <ta e="T122" id="Seg_11005" s="T121">np:L</ta>
            <ta e="T128" id="Seg_11006" s="T127">np.h:Th</ta>
            <ta e="T129" id="Seg_11007" s="T128">np:L</ta>
            <ta e="T133" id="Seg_11008" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_11009" s="T133">0.3.h:A</ta>
            <ta e="T135" id="Seg_11010" s="T134">0.3.h:A</ta>
            <ta e="T137" id="Seg_11011" s="T136">0.1.h:Poss</ta>
            <ta e="T141" id="Seg_11012" s="T140">0.1.h:Poss np.h:A</ta>
            <ta e="T144" id="Seg_11013" s="T143">0.3.h:A</ta>
            <ta e="T145" id="Seg_11014" s="T144">0.1.h:Poss np.h:A</ta>
            <ta e="T147" id="Seg_11015" s="T146">0.3.h:A</ta>
            <ta e="T148" id="Seg_11016" s="T147">0.3.h:A</ta>
            <ta e="T150" id="Seg_11017" s="T149">np:L</ta>
            <ta e="T152" id="Seg_11018" s="T151">0.3.h:Th</ta>
            <ta e="T154" id="Seg_11019" s="T153">0.3.h:A</ta>
            <ta e="T157" id="Seg_11020" s="T156">np.h:Th</ta>
            <ta e="T161" id="Seg_11021" s="T160">np.h:Poss</ta>
            <ta e="T167" id="Seg_11022" s="T166">0.3:Th</ta>
            <ta e="T170" id="Seg_11023" s="T169">np:Th</ta>
            <ta e="T179" id="Seg_11024" s="T178">np.h:P</ta>
            <ta e="T180" id="Seg_11025" s="T179">0.3.h:A</ta>
            <ta e="T182" id="Seg_11026" s="T181">np.h:Th</ta>
            <ta e="T187" id="Seg_11027" s="T186">np:Th</ta>
            <ta e="T188" id="Seg_11028" s="T187">0.3.h:Poss</ta>
            <ta e="T190" id="Seg_11029" s="T189">np:Th</ta>
            <ta e="T192" id="Seg_11030" s="T191">adv:Time</ta>
            <ta e="T195" id="Seg_11031" s="T194">0.3.h:A</ta>
            <ta e="T196" id="Seg_11032" s="T195">np.h:A</ta>
            <ta e="T203" id="Seg_11033" s="T202">np:G</ta>
            <ta e="T204" id="Seg_11034" s="T203">pro.h:Th</ta>
            <ta e="T206" id="Seg_11035" s="T205">0.3.h:A</ta>
            <ta e="T208" id="Seg_11036" s="T207">pro.h:A</ta>
            <ta e="T210" id="Seg_11037" s="T209">np:P</ta>
            <ta e="T212" id="Seg_11038" s="T211">np:L</ta>
            <ta e="T213" id="Seg_11039" s="T212">0.3.h:A</ta>
            <ta e="T214" id="Seg_11040" s="T213">np:L</ta>
            <ta e="T216" id="Seg_11041" s="T215">np:L</ta>
            <ta e="T223" id="Seg_11042" s="T222">0.3.h:A</ta>
            <ta e="T224" id="Seg_11043" s="T223">np:Th</ta>
            <ta e="T225" id="Seg_11044" s="T224">0.3.h:A</ta>
            <ta e="T229" id="Seg_11045" s="T228">np.h:Th</ta>
            <ta e="T231" id="Seg_11046" s="T230">0.3.h:A</ta>
            <ta e="T232" id="Seg_11047" s="T231">pro.h:Th</ta>
            <ta e="T238" id="Seg_11048" s="T236">pp:Com</ta>
            <ta e="T240" id="Seg_11049" s="T239">np.h:Th</ta>
            <ta e="T243" id="Seg_11050" s="T242">adv:L</ta>
            <ta e="T244" id="Seg_11051" s="T243">adv:Time</ta>
            <ta e="T248" id="Seg_11052" s="T247">np.h:A</ta>
            <ta e="T251" id="Seg_11053" s="T250">np:G</ta>
            <ta e="T253" id="Seg_11054" s="T252">np:G</ta>
            <ta e="T254" id="Seg_11055" s="T253">np:G</ta>
            <ta e="T257" id="Seg_11056" s="T256">0.3:Th</ta>
            <ta e="T259" id="Seg_11057" s="T258">np:G</ta>
            <ta e="T262" id="Seg_11058" s="T261">0.3:Th</ta>
            <ta e="T270" id="Seg_11059" s="T269">np.h:A</ta>
            <ta e="T272" id="Seg_11060" s="T271">pro:P</ta>
            <ta e="T275" id="Seg_11061" s="T274">np:Ins</ta>
            <ta e="T276" id="Seg_11062" s="T275">np:Ins</ta>
            <ta e="T278" id="Seg_11063" s="T277">np:P</ta>
            <ta e="T279" id="Seg_11064" s="T278">0.3.h:A</ta>
            <ta e="T287" id="Seg_11065" s="T286">0.3.h:A</ta>
            <ta e="T315" id="Seg_11066" s="T314">0.3.h:A</ta>
            <ta e="T320" id="Seg_11067" s="T319">np.h:Th</ta>
            <ta e="T322" id="Seg_11068" s="T321">np.h:Th</ta>
            <ta e="T324" id="Seg_11069" s="T322">0.3.h:Th</ta>
            <ta e="T326" id="Seg_11070" s="T325">np:L</ta>
            <ta e="T328" id="Seg_11071" s="T327">0.3.h:Th</ta>
            <ta e="T330" id="Seg_11072" s="T329">0.3.h:A</ta>
            <ta e="T333" id="Seg_11073" s="T332">np:Ins</ta>
            <ta e="T337" id="Seg_11074" s="T336">np:G</ta>
            <ta e="T338" id="Seg_11075" s="T337">0.3.h:A</ta>
            <ta e="T340" id="Seg_11076" s="T338">pp:G</ta>
            <ta e="T341" id="Seg_11077" s="T340">pro.h:Th</ta>
            <ta e="T344" id="Seg_11078" s="T342">0.3.h:A</ta>
            <ta e="T346" id="Seg_11079" s="T345">np.h:Th</ta>
            <ta e="T348" id="Seg_11080" s="T347">0.3.h:P</ta>
            <ta e="T349" id="Seg_11081" s="T348">0.3.h:E</ta>
            <ta e="T350" id="Seg_11082" s="T349">pro:Th</ta>
            <ta e="T352" id="Seg_11083" s="T351">adv:Time</ta>
            <ta e="T359" id="Seg_11084" s="T358">0.3.h:A</ta>
            <ta e="T366" id="Seg_11085" s="T365">0.1.h:Poss np.h:Th</ta>
            <ta e="T367" id="Seg_11086" s="T366">0.1.h:Poss np.h:Th</ta>
            <ta e="T372" id="Seg_11087" s="T371">0.3.h:Th</ta>
            <ta e="T387" id="Seg_11088" s="T386">0.3.h:A</ta>
            <ta e="T390" id="Seg_11089" s="T389">0.3.h:A</ta>
            <ta e="T394" id="Seg_11090" s="T393">0.3.h:A</ta>
            <ta e="T400" id="Seg_11091" s="T399">adv:Time</ta>
            <ta e="T402" id="Seg_11092" s="T401">np:Th</ta>
            <ta e="T405" id="Seg_11093" s="T404">np.h:A</ta>
            <ta e="T407" id="Seg_11094" s="T406">np:Th</ta>
            <ta e="T408" id="Seg_11095" s="T407">0.3.h:A</ta>
            <ta e="T412" id="Seg_11096" s="T411">np:Th</ta>
            <ta e="T413" id="Seg_11097" s="T412">np:Th</ta>
            <ta e="T418" id="Seg_11098" s="T417">np:Th</ta>
            <ta e="T421" id="Seg_11099" s="T420">np:P</ta>
            <ta e="T422" id="Seg_11100" s="T421">np:R</ta>
            <ta e="T423" id="Seg_11101" s="T422">0.3.h:A</ta>
            <ta e="T424" id="Seg_11102" s="T423">np:Th</ta>
            <ta e="T426" id="Seg_11103" s="T425">0.3.h:A</ta>
            <ta e="T428" id="Seg_11104" s="T427">np:P</ta>
            <ta e="T429" id="Seg_11105" s="T428">0.1.h:A</ta>
            <ta e="T430" id="Seg_11106" s="T429">np.h:P</ta>
            <ta e="T434" id="Seg_11107" s="T433">0.3.h:A</ta>
            <ta e="T439" id="Seg_11108" s="T438">np:P</ta>
            <ta e="T440" id="Seg_11109" s="T439">0.1.h:A</ta>
            <ta e="T443" id="Seg_11110" s="T442">0.2.h:A</ta>
            <ta e="T445" id="Seg_11111" s="T444">np:Th</ta>
            <ta e="T446" id="Seg_11112" s="T445">0.2.h:A</ta>
            <ta e="T449" id="Seg_11113" s="T448">np:Th</ta>
            <ta e="T450" id="Seg_11114" s="T449">0.2.h:A</ta>
            <ta e="T452" id="Seg_11115" s="T451">0.2.h:A</ta>
            <ta e="T453" id="Seg_11116" s="T452">0.1.h:Poss np.h:Th</ta>
            <ta e="T455" id="Seg_11117" s="T454">0.2.h:A</ta>
            <ta e="T459" id="Seg_11118" s="T458">np:Th</ta>
            <ta e="T462" id="Seg_11119" s="T461">np:G</ta>
            <ta e="T463" id="Seg_11120" s="T462">0.3.h:A</ta>
            <ta e="T465" id="Seg_11121" s="T464">np:G</ta>
            <ta e="T467" id="Seg_11122" s="T465">0.3.h:A</ta>
            <ta e="T481" id="Seg_11123" s="T480">np.h:A</ta>
            <ta e="T482" id="Seg_11124" s="T481">np:G</ta>
            <ta e="T483" id="Seg_11125" s="T482">0.3.h:A</ta>
            <ta e="T486" id="Seg_11126" s="T485">np.h:A</ta>
            <ta e="T489" id="Seg_11127" s="T488">adv:Time</ta>
            <ta e="T490" id="Seg_11128" s="T489">0.3.h:Poss np:Th</ta>
            <ta e="T491" id="Seg_11129" s="T490">0.3.h:A</ta>
            <ta e="T493" id="Seg_11130" s="T492">0.3.h:Poss np:Th</ta>
            <ta e="T497" id="Seg_11131" s="T496">np.h:B</ta>
            <ta e="T501" id="Seg_11132" s="T500">np.h:A</ta>
            <ta e="T509" id="Seg_11133" s="T508">np.h:P</ta>
            <ta e="T512" id="Seg_11134" s="T511">np.h:B</ta>
            <ta e="T513" id="Seg_11135" s="T512">0.3.h:Poss np:P</ta>
            <ta e="T514" id="Seg_11136" s="T513">0.3.h:A</ta>
            <ta e="T515" id="Seg_11137" s="T514">0.1.h:A</ta>
            <ta e="T518" id="Seg_11138" s="T517">np.h:A</ta>
            <ta e="T521" id="Seg_11139" s="T520">0.3.h:Poss np:P</ta>
            <ta e="T526" id="Seg_11140" s="T524">0.3.h:A</ta>
            <ta e="T528" id="Seg_11141" s="T527">np:Ins</ta>
            <ta e="T531" id="Seg_11142" s="T530">np:Ins</ta>
            <ta e="T532" id="Seg_11143" s="T531">0.3.h:A</ta>
            <ta e="T534" id="Seg_11144" s="T532">0.3:P</ta>
            <ta e="T535" id="Seg_11145" s="T534">adv:Time</ta>
            <ta e="T539" id="Seg_11146" s="T538">n:Time</ta>
            <ta e="T543" id="Seg_11147" s="T542">n:Time</ta>
            <ta e="T547" id="Seg_11148" s="T546">np.h:Poss</ta>
            <ta e="T548" id="Seg_11149" s="T547">np:P</ta>
            <ta e="T552" id="Seg_11150" s="T551">np.h:Poss</ta>
            <ta e="T556" id="Seg_11151" s="T555">np.h:Poss</ta>
            <ta e="T557" id="Seg_11152" s="T556">np.h:P</ta>
            <ta e="T558" id="Seg_11153" s="T557">0.3.h:A</ta>
            <ta e="T559" id="Seg_11154" s="T558">np.h:P</ta>
            <ta e="T561" id="Seg_11155" s="T560">0.3.h:A</ta>
            <ta e="T567" id="Seg_11156" s="T565">0.3.h:A</ta>
            <ta e="T569" id="Seg_11157" s="T568">adv:Time</ta>
            <ta e="T573" id="Seg_11158" s="T572">np:G</ta>
            <ta e="T575" id="Seg_11159" s="T574">0.3.h:A</ta>
            <ta e="T578" id="Seg_11160" s="T576">0.3.h:A</ta>
            <ta e="T580" id="Seg_11161" s="T579">np:G</ta>
            <ta e="T581" id="Seg_11162" s="T580">0.3.h:A</ta>
            <ta e="T582" id="Seg_11163" s="T581">adv:Time</ta>
            <ta e="T584" id="Seg_11164" s="T583">np:P</ta>
            <ta e="T586" id="Seg_11165" s="T585">np:P</ta>
            <ta e="T590" id="Seg_11166" s="T589">np:P</ta>
            <ta e="T591" id="Seg_11167" s="T590">np:G</ta>
            <ta e="T596" id="Seg_11168" s="T595">np:G</ta>
            <ta e="T598" id="Seg_11169" s="T597">np:Com</ta>
            <ta e="T601" id="Seg_11170" s="T600">np:G</ta>
            <ta e="T603" id="Seg_11171" s="T601">0.3.h:A</ta>
            <ta e="T605" id="Seg_11172" s="T604">np:L</ta>
            <ta e="T606" id="Seg_11173" s="T605">0.3:Th</ta>
            <ta e="T609" id="Seg_11174" s="T608">np.h:Poss</ta>
            <ta e="T611" id="Seg_11175" s="T609">pp:Time</ta>
            <ta e="T616" id="Seg_11176" s="T615">np:L</ta>
            <ta e="T619" id="Seg_11177" s="T618">np:L</ta>
            <ta e="T622" id="Seg_11178" s="T621">np:L</ta>
            <ta e="T624" id="Seg_11179" s="T623">0.3.h:A</ta>
            <ta e="T626" id="Seg_11180" s="T625">n:Time</ta>
            <ta e="T628" id="Seg_11181" s="T627">np:Path</ta>
            <ta e="T629" id="Seg_11182" s="T628">0.3.h:A</ta>
            <ta e="T630" id="Seg_11183" s="T629">np:Th</ta>
            <ta e="T634" id="Seg_11184" s="T633">np:Th</ta>
            <ta e="T637" id="Seg_11185" s="T636">0.3.h:E</ta>
            <ta e="T640" id="Seg_11186" s="T639">np:L</ta>
            <ta e="T641" id="Seg_11187" s="T640">np.h:A</ta>
            <ta e="T644" id="Seg_11188" s="T643">0.3.h:Poss np:Th</ta>
            <ta e="T647" id="Seg_11189" s="T646">0.3.h:Poss</ta>
            <ta e="T648" id="Seg_11190" s="T647">np:Th</ta>
            <ta e="T651" id="Seg_11191" s="T650">np.h:P</ta>
            <ta e="T658" id="Seg_11192" s="T657">0.3.h:A</ta>
            <ta e="T659" id="Seg_11193" s="T658">adv:Time</ta>
            <ta e="T666" id="Seg_11194" s="T665">np.h:Th</ta>
            <ta e="T670" id="Seg_11195" s="T669">np:P</ta>
            <ta e="T673" id="Seg_11196" s="T672">0.2.h:A</ta>
            <ta e="T675" id="Seg_11197" s="T674">np:L</ta>
            <ta e="T677" id="Seg_11198" s="T675">pp:L</ta>
            <ta e="T680" id="Seg_11199" s="T679">0.2.h:A</ta>
            <ta e="T701" id="Seg_11200" s="T700">0.2.h:A</ta>
            <ta e="T702" id="Seg_11201" s="T701">0.2.h:A</ta>
            <ta e="T703" id="Seg_11202" s="T702">0.2.h:A</ta>
            <ta e="T708" id="Seg_11203" s="T707">np.h:Th</ta>
            <ta e="T710" id="Seg_11204" s="T709">np.h:A</ta>
            <ta e="T713" id="Seg_11205" s="T712">np.h:Th</ta>
            <ta e="T714" id="Seg_11206" s="T713">0.3.h:A</ta>
            <ta e="T716" id="Seg_11207" s="T715">np:So</ta>
            <ta e="T719" id="Seg_11208" s="T718">np:L</ta>
            <ta e="T722" id="Seg_11209" s="T721">0.1.h:A</ta>
            <ta e="T723" id="Seg_11210" s="T722">np:G</ta>
            <ta e="T724" id="Seg_11211" s="T723">0.1.h:A</ta>
            <ta e="T726" id="Seg_11212" s="T724">0.1.h:A</ta>
            <ta e="T728" id="Seg_11213" s="T727">np.h:Th</ta>
            <ta e="T730" id="Seg_11214" s="T729">np.h:Th</ta>
            <ta e="T738" id="Seg_11215" s="T737">0.3.h:A</ta>
            <ta e="T741" id="Seg_11216" s="T740">np.h:P</ta>
            <ta e="T744" id="Seg_11217" s="T743">np.h:R</ta>
            <ta e="T746" id="Seg_11218" s="T745">np.h:R</ta>
            <ta e="T749" id="Seg_11219" s="T748">np:L</ta>
            <ta e="T752" id="Seg_11220" s="T751">0.3.h:A</ta>
            <ta e="T756" id="Seg_11221" s="T755">np.h:P</ta>
            <ta e="T761" id="Seg_11222" s="T760">0.3.h:A</ta>
            <ta e="T763" id="Seg_11223" s="T762">np.h:P</ta>
            <ta e="T768" id="Seg_11224" s="T767">0.3.h:A</ta>
            <ta e="T769" id="Seg_11225" s="T768">adv:Time</ta>
            <ta e="T775" id="Seg_11226" s="T774">np.h:A</ta>
            <ta e="T776" id="Seg_11227" s="T775">np:P</ta>
            <ta e="T778" id="Seg_11228" s="T777">np.h:B</ta>
            <ta e="T779" id="Seg_11229" s="T778">np.h:A</ta>
            <ta e="T784" id="Seg_11230" s="T783">np:P</ta>
            <ta e="T788" id="Seg_11231" s="T787">np:Ins</ta>
            <ta e="T789" id="Seg_11232" s="T788">np:Ins</ta>
            <ta e="T791" id="Seg_11233" s="T790">np:Ins</ta>
            <ta e="T793" id="Seg_11234" s="T792">0.3.h:A</ta>
            <ta e="T795" id="Seg_11235" s="T794">pro:Th</ta>
            <ta e="T800" id="Seg_11236" s="T799">0.3.h:A</ta>
            <ta e="T802" id="Seg_11237" s="T801">pro:P</ta>
            <ta e="T805" id="Seg_11238" s="T803">0.3.h:A</ta>
            <ta e="T815" id="Seg_11239" s="T814">np.h:Th</ta>
            <ta e="T817" id="Seg_11240" s="T816">0.3.h:A</ta>
            <ta e="T0" id="Seg_11241" s="T820">np:Ins</ta>
            <ta e="T823" id="Seg_11242" s="T822">np:Th</ta>
            <ta e="T826" id="Seg_11243" s="T825">0.3:Th</ta>
            <ta e="T827" id="Seg_11244" s="T826">0.3:Th</ta>
            <ta e="T828" id="Seg_11245" s="T827">np:Ins</ta>
            <ta e="T830" id="Seg_11246" s="T829">0.3.h:A</ta>
            <ta e="T831" id="Seg_11247" s="T830">pro:Poss</ta>
            <ta e="T832" id="Seg_11248" s="T831">np:Th</ta>
            <ta e="T837" id="Seg_11249" s="T836">np:Th</ta>
            <ta e="T838" id="Seg_11250" s="T837">0.3:Poss</ta>
            <ta e="T840" id="Seg_11251" s="T839">np:Th</ta>
            <ta e="T845" id="Seg_11252" s="T844">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_11253" s="T3">np:S</ta>
            <ta e="T5" id="Seg_11254" s="T4">n:pred</ta>
            <ta e="T11" id="Seg_11255" s="T8">s:rel</ta>
            <ta e="T15" id="Seg_11256" s="T14">np:O</ta>
            <ta e="T16" id="Seg_11257" s="T15">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_11258" s="T16">s:temp</ta>
            <ta e="T21" id="Seg_11259" s="T20">0.3.h:S v:pred</ta>
            <ta e="T23" id="Seg_11260" s="T21">s:rel</ta>
            <ta e="T25" id="Seg_11261" s="T24">s:rel</ta>
            <ta e="T27" id="Seg_11262" s="T26">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_11263" s="T31">np:O</ta>
            <ta e="T33" id="Seg_11264" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_11265" s="T33">np.h:S</ta>
            <ta e="T35" id="Seg_11266" s="T34">v:pred</ta>
            <ta e="T42" id="Seg_11267" s="T35">s:temp</ta>
            <ta e="T43" id="Seg_11268" s="T42">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_11269" s="T43">np:O</ta>
            <ta e="T48" id="Seg_11270" s="T45">s:adv</ta>
            <ta e="T59" id="Seg_11271" s="T58">np:S</ta>
            <ta e="T60" id="Seg_11272" s="T59">v:pred</ta>
            <ta e="T65" id="Seg_11273" s="T64">np:S</ta>
            <ta e="T68" id="Seg_11274" s="T67">n:pred</ta>
            <ta e="T69" id="Seg_11275" s="T68">cop</ta>
            <ta e="T70" id="Seg_11276" s="T69">np.h:S</ta>
            <ta e="T73" id="Seg_11277" s="T71">s:temp</ta>
            <ta e="T74" id="Seg_11278" s="T73">v:pred</ta>
            <ta e="T82" id="Seg_11279" s="T81">np:S</ta>
            <ta e="T83" id="Seg_11280" s="T82">v:pred</ta>
            <ta e="T88" id="Seg_11281" s="T87">np:S</ta>
            <ta e="T89" id="Seg_11282" s="T88">v:pred</ta>
            <ta e="T90" id="Seg_11283" s="T89">np:O</ta>
            <ta e="T91" id="Seg_11284" s="T90">0.3.h:S v:pred</ta>
            <ta e="T93" id="Seg_11285" s="T92">s:adv</ta>
            <ta e="T97" id="Seg_11286" s="T93">s:cond</ta>
            <ta e="T99" id="Seg_11287" s="T98">0.3.h:S v:pred</ta>
            <ta e="T100" id="Seg_11288" s="T99">0.3.h:S v:pred</ta>
            <ta e="T107" id="Seg_11289" s="T106">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_11290" s="T107">0.3.h:S v:pred</ta>
            <ta e="T111" id="Seg_11291" s="T109">s:cond</ta>
            <ta e="T118" id="Seg_11292" s="T114">s:cond</ta>
            <ta e="T124" id="Seg_11293" s="T119">s:cond</ta>
            <ta e="T131" id="Seg_11294" s="T124">s:cond</ta>
            <ta e="T133" id="Seg_11295" s="T131">s:temp</ta>
            <ta e="T134" id="Seg_11296" s="T133">0.3.h:S v:pred</ta>
            <ta e="T135" id="Seg_11297" s="T134">s:adv</ta>
            <ta e="T141" id="Seg_11298" s="T140">np.h:S</ta>
            <ta e="T143" id="Seg_11299" s="T142">v:pred</ta>
            <ta e="T144" id="Seg_11300" s="T143">s:adv</ta>
            <ta e="T145" id="Seg_11301" s="T144">np.h:S</ta>
            <ta e="T146" id="Seg_11302" s="T145">v:pred</ta>
            <ta e="T147" id="Seg_11303" s="T146">s:temp</ta>
            <ta e="T148" id="Seg_11304" s="T147">0.3.h:S v:pred</ta>
            <ta e="T152" id="Seg_11305" s="T149">s:cond</ta>
            <ta e="T154" id="Seg_11306" s="T153">0.3.h:S v:pred</ta>
            <ta e="T157" id="Seg_11307" s="T156">np.h:O</ta>
            <ta e="T167" id="Seg_11308" s="T165">s:purp</ta>
            <ta e="T170" id="Seg_11309" s="T169">np:S</ta>
            <ta e="T172" id="Seg_11310" s="T171">s:rel</ta>
            <ta e="T173" id="Seg_11311" s="T172">n:pred</ta>
            <ta e="T174" id="Seg_11312" s="T173">cop</ta>
            <ta e="T180" id="Seg_11313" s="T178">s:temp</ta>
            <ta e="T183" id="Seg_11314" s="T181">s:temp</ta>
            <ta e="T187" id="Seg_11315" s="T186">adj:pred</ta>
            <ta e="T188" id="Seg_11316" s="T187">0.3.h:S cop</ta>
            <ta e="T193" id="Seg_11317" s="T192">s:temp</ta>
            <ta e="T194" id="Seg_11318" s="T193">s:temp</ta>
            <ta e="T195" id="Seg_11319" s="T194">0.3.h:S v:pred</ta>
            <ta e="T198" id="Seg_11320" s="T195">s:temp</ta>
            <ta e="T204" id="Seg_11321" s="T203">pro.h:O</ta>
            <ta e="T206" id="Seg_11322" s="T205">0.3.h:S v:pred</ta>
            <ta e="T211" id="Seg_11323" s="T207">s:comp</ta>
            <ta e="T213" id="Seg_11324" s="T212">0.3.h:S v:pred</ta>
            <ta e="T215" id="Seg_11325" s="T214">s:rel</ta>
            <ta e="T220" id="Seg_11326" s="T219">s:rel</ta>
            <ta e="T223" id="Seg_11327" s="T222">0.3.h:S v:pred</ta>
            <ta e="T224" id="Seg_11328" s="T223">np:O</ta>
            <ta e="T225" id="Seg_11329" s="T224">0.3.h:S v:pred</ta>
            <ta e="T229" id="Seg_11330" s="T228">np.h:O</ta>
            <ta e="T231" id="Seg_11331" s="T230">0.3.h:S v:pred</ta>
            <ta e="T232" id="Seg_11332" s="T231">pro.h:S</ta>
            <ta e="T234" id="Seg_11333" s="T233">ptcl:pred</ta>
            <ta e="T235" id="Seg_11334" s="T234">cop</ta>
            <ta e="T240" id="Seg_11335" s="T239">np.h:S</ta>
            <ta e="T242" id="Seg_11336" s="T241">v:pred</ta>
            <ta e="T248" id="Seg_11337" s="T247">np.h:S</ta>
            <ta e="T249" id="Seg_11338" s="T248">v:pred</ta>
            <ta e="T252" id="Seg_11339" s="T251">s:rel</ta>
            <ta e="T257" id="Seg_11340" s="T254">s:cond</ta>
            <ta e="T262" id="Seg_11341" s="T257">s:cond</ta>
            <ta e="T264" id="Seg_11342" s="T262">s:temp</ta>
            <ta e="T267" id="Seg_11343" s="T265">s:temp</ta>
            <ta e="T271" id="Seg_11344" s="T267">s:temp</ta>
            <ta e="T274" id="Seg_11345" s="T271">s:adv</ta>
            <ta e="T277" id="Seg_11346" s="T276">s:adv</ta>
            <ta e="T278" id="Seg_11347" s="T277">np:O</ta>
            <ta e="T279" id="Seg_11348" s="T278">0.3.h:S v:pred</ta>
            <ta e="T282" id="Seg_11349" s="T279">s:rel</ta>
            <ta e="T284" id="Seg_11350" s="T283">s:rel</ta>
            <ta e="T287" id="Seg_11351" s="T286">0.3.h:S v:pred</ta>
            <ta e="T314" id="Seg_11352" s="T313">s:adv</ta>
            <ta e="T315" id="Seg_11353" s="T314">0.3.h:S v:pred</ta>
            <ta e="T320" id="Seg_11354" s="T319">np.h:O</ta>
            <ta e="T321" id="Seg_11355" s="T320">s:rel</ta>
            <ta e="T322" id="Seg_11356" s="T321">np.h:O</ta>
            <ta e="T324" id="Seg_11357" s="T322">0.3.h:S v:pred</ta>
            <ta e="T328" id="Seg_11358" s="T326">s:purp</ta>
            <ta e="T330" id="Seg_11359" s="T329">0.3.h:S v:pred</ta>
            <ta e="T338" id="Seg_11360" s="T337">0.3.h:S v:pred</ta>
            <ta e="T341" id="Seg_11361" s="T340">pro.h:O</ta>
            <ta e="T344" id="Seg_11362" s="T342">0.3.h:S v:pred</ta>
            <ta e="T346" id="Seg_11363" s="T345">np.h:O</ta>
            <ta e="T348" id="Seg_11364" s="T347">0.3.h:S v:pred</ta>
            <ta e="T349" id="Seg_11365" s="T348">s:adv</ta>
            <ta e="T350" id="Seg_11366" s="T349">pro:S</ta>
            <ta e="T351" id="Seg_11367" s="T350">n:pred</ta>
            <ta e="T359" id="Seg_11368" s="T358">0.3.h:S v:pred</ta>
            <ta e="T366" id="Seg_11369" s="T363">s:purp</ta>
            <ta e="T372" id="Seg_11370" s="T370">s:purp</ta>
            <ta e="T387" id="Seg_11371" s="T386">0.3.h:S v:pred</ta>
            <ta e="T390" id="Seg_11372" s="T389">0.3.h:S v:pred</ta>
            <ta e="T394" id="Seg_11373" s="T393">0.3.h:S v:pred</ta>
            <ta e="T396" id="Seg_11374" s="T395">s:rel</ta>
            <ta e="T398" id="Seg_11375" s="T397">s:rel</ta>
            <ta e="T402" id="Seg_11376" s="T401">np:O</ta>
            <ta e="T403" id="Seg_11377" s="T402">v:pred</ta>
            <ta e="T405" id="Seg_11378" s="T404">np.h:S</ta>
            <ta e="T407" id="Seg_11379" s="T406">np:O</ta>
            <ta e="T408" id="Seg_11380" s="T407">0.3.h:S v:pred</ta>
            <ta e="T410" id="Seg_11381" s="T409">n:pred</ta>
            <ta e="T412" id="Seg_11382" s="T411">np:S</ta>
            <ta e="T413" id="Seg_11383" s="T412">np:S</ta>
            <ta e="T415" id="Seg_11384" s="T414">adj:pred</ta>
            <ta e="T416" id="Seg_11385" s="T415">cop</ta>
            <ta e="T420" id="Seg_11386" s="T417">s:temp</ta>
            <ta e="T421" id="Seg_11387" s="T420">np:O</ta>
            <ta e="T423" id="Seg_11388" s="T422">0.3.h:S v:pred</ta>
            <ta e="T424" id="Seg_11389" s="T423">np:O</ta>
            <ta e="T426" id="Seg_11390" s="T425">0.3.h:S v:pred</ta>
            <ta e="T428" id="Seg_11391" s="T427">np:O</ta>
            <ta e="T429" id="Seg_11392" s="T428">0.1.h:S v:pred</ta>
            <ta e="T432" id="Seg_11393" s="T429">s:purp</ta>
            <ta e="T434" id="Seg_11394" s="T432">s:purp</ta>
            <ta e="T439" id="Seg_11395" s="T438">np:O</ta>
            <ta e="T440" id="Seg_11396" s="T439">0.1.h:S v:pred</ta>
            <ta e="T443" id="Seg_11397" s="T442">0.2.h:S v:pred</ta>
            <ta e="T445" id="Seg_11398" s="T444">np:O</ta>
            <ta e="T446" id="Seg_11399" s="T445">0.2.h:S v:pred</ta>
            <ta e="T449" id="Seg_11400" s="T448">np:O</ta>
            <ta e="T450" id="Seg_11401" s="T449">0.2.h:S v:pred</ta>
            <ta e="T452" id="Seg_11402" s="T451">0.2.h:S v:pred</ta>
            <ta e="T453" id="Seg_11403" s="T452">np.h:O</ta>
            <ta e="T455" id="Seg_11404" s="T454">0.2.h:S v:pred</ta>
            <ta e="T459" id="Seg_11405" s="T458">np:O</ta>
            <ta e="T461" id="Seg_11406" s="T459">s:temp</ta>
            <ta e="T463" id="Seg_11407" s="T462">0.3.h:S v:pred</ta>
            <ta e="T467" id="Seg_11408" s="T465">0.3.h:S v:pred</ta>
            <ta e="T471" id="Seg_11409" s="T467">s:temp</ta>
            <ta e="T479" id="Seg_11410" s="T471">s:cond</ta>
            <ta e="T480" id="Seg_11411" s="T479">v:pred</ta>
            <ta e="T481" id="Seg_11412" s="T480">np.h:S</ta>
            <ta e="T483" id="Seg_11413" s="T482">0.3.h:S v:pred</ta>
            <ta e="T487" id="Seg_11414" s="T485">s:purp</ta>
            <ta e="T490" id="Seg_11415" s="T489">np:O</ta>
            <ta e="T491" id="Seg_11416" s="T490">0.3.h:S v:pred</ta>
            <ta e="T494" id="Seg_11417" s="T491">s:adv</ta>
            <ta e="T497" id="Seg_11418" s="T494">s:adv</ta>
            <ta e="T498" id="Seg_11419" s="T497">s:adv</ta>
            <ta e="T499" id="Seg_11420" s="T498">v:pred</ta>
            <ta e="T501" id="Seg_11421" s="T500">np.h:S</ta>
            <ta e="T510" id="Seg_11422" s="T506">s:temp</ta>
            <ta e="T513" id="Seg_11423" s="T512">np:O</ta>
            <ta e="T514" id="Seg_11424" s="T513">0.3.h:S v:pred</ta>
            <ta e="T515" id="Seg_11425" s="T514">0.1.h:S v:pred</ta>
            <ta e="T518" id="Seg_11426" s="T517">np.h:S</ta>
            <ta e="T519" id="Seg_11427" s="T518">v:pred</ta>
            <ta e="T524" id="Seg_11428" s="T519">s:temp</ta>
            <ta e="T526" id="Seg_11429" s="T524">0.3.h:S v:pred</ta>
            <ta e="T532" id="Seg_11430" s="T531">0.3.h:S v:pred</ta>
            <ta e="T535" id="Seg_11431" s="T532">s:purp</ta>
            <ta e="T537" id="Seg_11432" s="T535">s:temp</ta>
            <ta e="T545" id="Seg_11433" s="T537">s:temp</ta>
            <ta e="T546" id="Seg_11434" s="T545">v:pred</ta>
            <ta e="T548" id="Seg_11435" s="T547">np.h:S</ta>
            <ta e="T557" id="Seg_11436" s="T556">np.h:O</ta>
            <ta e="T558" id="Seg_11437" s="T557">0.3.h:S v:pred</ta>
            <ta e="T559" id="Seg_11438" s="T558">np.h:O</ta>
            <ta e="T561" id="Seg_11439" s="T560">0.3.h:S v:pred</ta>
            <ta e="T565" id="Seg_11440" s="T561">s:temp</ta>
            <ta e="T567" id="Seg_11441" s="T565">0.3.h:S v:pred</ta>
            <ta e="T575" id="Seg_11442" s="T574">0.3.h:S v:pred</ta>
            <ta e="T578" id="Seg_11443" s="T576">0.3.h:S v:pred</ta>
            <ta e="T581" id="Seg_11444" s="T580">0.3.h:S v:pred</ta>
            <ta e="T588" id="Seg_11445" s="T583">s:temp</ta>
            <ta e="T594" id="Seg_11446" s="T588">s:temp</ta>
            <ta e="T603" id="Seg_11447" s="T601">0.3.h:S v:pred</ta>
            <ta e="T606" id="Seg_11448" s="T605">0.3.h:S v:pred</ta>
            <ta e="T614" id="Seg_11449" s="T611">s:rel</ta>
            <ta e="T618" id="Seg_11450" s="T616">s:rel</ta>
            <ta e="T621" id="Seg_11451" s="T619">s:rel</ta>
            <ta e="T624" id="Seg_11452" s="T623">0.3.h:S v:pred</ta>
            <ta e="T629" id="Seg_11453" s="T626">s:cond</ta>
            <ta e="T633" id="Seg_11454" s="T629">s:cond</ta>
            <ta e="T637" id="Seg_11455" s="T633">s:cond</ta>
            <ta e="T641" id="Seg_11456" s="T640">np.h:S</ta>
            <ta e="T642" id="Seg_11457" s="T641">v:pred</ta>
            <ta e="T644" id="Seg_11458" s="T643">np:S</ta>
            <ta e="T645" id="Seg_11459" s="T644">v:pred</ta>
            <ta e="T649" id="Seg_11460" s="T647">s:cond</ta>
            <ta e="T651" id="Seg_11461" s="T650">np.h:S</ta>
            <ta e="T652" id="Seg_11462" s="T651">v:pred</ta>
            <ta e="T656" id="Seg_11463" s="T654">s:rel</ta>
            <ta e="T658" id="Seg_11464" s="T657">0.3.h:S v:pred</ta>
            <ta e="T665" id="Seg_11465" s="T664">s:rel</ta>
            <ta e="T666" id="Seg_11466" s="T665">np.h:S</ta>
            <ta e="T667" id="Seg_11467" s="T666">adj:pred</ta>
            <ta e="T671" id="Seg_11468" s="T668">s:temp</ta>
            <ta e="T673" id="Seg_11469" s="T672">0.2.h:S v:pred</ta>
            <ta e="T680" id="Seg_11470" s="T679">0.2.h:S v:pred</ta>
            <ta e="T701" id="Seg_11471" s="T700">s:temp</ta>
            <ta e="T702" id="Seg_11472" s="T701">s:temp</ta>
            <ta e="T703" id="Seg_11473" s="T702">0.2.h:S v:pred</ta>
            <ta e="T708" id="Seg_11474" s="T707">np.h:O</ta>
            <ta e="T709" id="Seg_11475" s="T708">v:pred</ta>
            <ta e="T710" id="Seg_11476" s="T709">np.h:S</ta>
            <ta e="T713" id="Seg_11477" s="T712">np.h:O</ta>
            <ta e="T714" id="Seg_11478" s="T713">0.3.h:S v:pred</ta>
            <ta e="T715" id="Seg_11479" s="T714">s:rel</ta>
            <ta e="T718" id="Seg_11480" s="T717">s:rel</ta>
            <ta e="T721" id="Seg_11481" s="T720">s:temp</ta>
            <ta e="T722" id="Seg_11482" s="T721">0.1.h:S v:pred</ta>
            <ta e="T724" id="Seg_11483" s="T723">0.1.h:S v:pred</ta>
            <ta e="T726" id="Seg_11484" s="T724">0.1.h:S v:pred</ta>
            <ta e="T727" id="Seg_11485" s="T726">s:rel</ta>
            <ta e="T728" id="Seg_11486" s="T727">np.h:O</ta>
            <ta e="T729" id="Seg_11487" s="T728">s:rel</ta>
            <ta e="T730" id="Seg_11488" s="T729">np.h:O</ta>
            <ta e="T736" id="Seg_11489" s="T733">s:temp</ta>
            <ta e="T738" id="Seg_11490" s="T737">0.3.h:S v:pred</ta>
            <ta e="T741" id="Seg_11491" s="T740">np.h:S</ta>
            <ta e="T742" id="Seg_11492" s="T741">v:pred</ta>
            <ta e="T751" id="Seg_11493" s="T750">s:adv</ta>
            <ta e="T752" id="Seg_11494" s="T751">0.3.h:S v:pred</ta>
            <ta e="T758" id="Seg_11495" s="T753">s:cond</ta>
            <ta e="T761" id="Seg_11496" s="T760">0.3.h:S v:pred</ta>
            <ta e="T765" id="Seg_11497" s="T761">s:cond</ta>
            <ta e="T768" id="Seg_11498" s="T767">0.3.h:S v:pred</ta>
            <ta e="T774" id="Seg_11499" s="T769">s:temp</ta>
            <ta e="T775" id="Seg_11500" s="T774">np.h:S</ta>
            <ta e="T776" id="Seg_11501" s="T775">np:O</ta>
            <ta e="T777" id="Seg_11502" s="T776">v:pred</ta>
            <ta e="T779" id="Seg_11503" s="T778">np.h:S</ta>
            <ta e="T780" id="Seg_11504" s="T779">v:pred</ta>
            <ta e="T784" id="Seg_11505" s="T783">np:O</ta>
            <ta e="T792" id="Seg_11506" s="T785">s:adv</ta>
            <ta e="T793" id="Seg_11507" s="T792">0.3.h:S v:pred</ta>
            <ta e="T795" id="Seg_11508" s="T794">pro:S</ta>
            <ta e="T798" id="Seg_11509" s="T797">n:pred</ta>
            <ta e="T800" id="Seg_11510" s="T799">0.3.h:S v:pred</ta>
            <ta e="T802" id="Seg_11511" s="T801">pro:O</ta>
            <ta e="T805" id="Seg_11512" s="T803">0.3.h:S v:pred</ta>
            <ta e="T815" id="Seg_11513" s="T814">np.h:S</ta>
            <ta e="T816" id="Seg_11514" s="T815">v:pred</ta>
            <ta e="T817" id="Seg_11515" s="T816">0.3.h:S v:pred</ta>
            <ta e="T825" id="Seg_11516" s="T821">s:temp</ta>
            <ta e="T826" id="Seg_11517" s="T825">s:temp</ta>
            <ta e="T827" id="Seg_11518" s="T826">s:temp</ta>
            <ta e="T830" id="Seg_11519" s="T827">s:temp</ta>
            <ta e="T831" id="Seg_11520" s="T830">pro:S</ta>
            <ta e="T832" id="Seg_11521" s="T831">adj:pred</ta>
            <ta e="T833" id="Seg_11522" s="T832">cop</ta>
            <ta e="T837" id="Seg_11523" s="T836">adj:pred</ta>
            <ta e="T838" id="Seg_11524" s="T837">0.3:S cop</ta>
            <ta e="T840" id="Seg_11525" s="T839">np:S</ta>
            <ta e="T841" id="Seg_11526" s="T840">v:pred</ta>
            <ta e="T845" id="Seg_11527" s="T844">np.h:S</ta>
            <ta e="T847" id="Seg_11528" s="T846">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T24" id="Seg_11529" s="T23">EV:cult</ta>
            <ta e="T26" id="Seg_11530" s="T25">EV:cult</ta>
            <ta e="T61" id="Seg_11531" s="T60">EV:gram (DIM)</ta>
            <ta e="T120" id="Seg_11532" s="T119">RUS:cult</ta>
            <ta e="T156" id="Seg_11533" s="T155">RUS:cult</ta>
            <ta e="T161" id="Seg_11534" s="T160">RUS:cult</ta>
            <ta e="T170" id="Seg_11535" s="T169">EV:gram (DIM)</ta>
            <ta e="T203" id="Seg_11536" s="T202">EV:cult</ta>
            <ta e="T210" id="Seg_11537" s="T209">EV:cult</ta>
            <ta e="T216" id="Seg_11538" s="T215">EV:cult</ta>
            <ta e="T221" id="Seg_11539" s="T220">EV:cult</ta>
            <ta e="T230" id="Seg_11540" s="T229">EV:gram (DIM)</ta>
            <ta e="T237" id="Seg_11541" s="T236">RUS:cult</ta>
            <ta e="T248" id="Seg_11542" s="T247">RUS:cult</ta>
            <ta e="T254" id="Seg_11543" s="T253">EV:cult</ta>
            <ta e="T269" id="Seg_11544" s="T268">RUS:cult</ta>
            <ta e="T273" id="Seg_11545" s="T272">EV:gram (DIM)</ta>
            <ta e="T274" id="Seg_11546" s="T273">RUS:cult</ta>
            <ta e="T275" id="Seg_11547" s="T274">RUS:cult</ta>
            <ta e="T278" id="Seg_11548" s="T277">EV:gram (DIM)</ta>
            <ta e="T285" id="Seg_11549" s="T284">EV:cult</ta>
            <ta e="T404" id="Seg_11550" s="T403">RUS:cult</ta>
            <ta e="T412" id="Seg_11551" s="T411">EV:gram (DIM)</ta>
            <ta e="T427" id="Seg_11552" s="T426">EV:gram (DIM)</ta>
            <ta e="T439" id="Seg_11553" s="T438">EV:gram (DIM)</ta>
            <ta e="T442" id="Seg_11554" s="T441">EV:gram (DIM)</ta>
            <ta e="T459" id="Seg_11555" s="T458">EV:gram (DIM)</ta>
            <ta e="T481" id="Seg_11556" s="T480">RUS:cult</ta>
            <ta e="T517" id="Seg_11557" s="T516">RUS:cult</ta>
            <ta e="T574" id="Seg_11558" s="T573">RUS:mod</ta>
            <ta e="T579" id="Seg_11559" s="T578">RUS:cult</ta>
            <ta e="T591" id="Seg_11560" s="T590">RUS:cult</ta>
            <ta e="T615" id="Seg_11561" s="T614">EV:cult</ta>
            <ta e="T630" id="Seg_11562" s="T629">EV:cult</ta>
            <ta e="T648" id="Seg_11563" s="T647">EV:cult</ta>
            <ta e="T657" id="Seg_11564" s="T656">EV:cult</ta>
            <ta e="T710" id="Seg_11565" s="T709">RUS:cult</ta>
            <ta e="T719" id="Seg_11566" s="T718">EV:cult</ta>
            <ta e="T788" id="Seg_11567" s="T787">RUS:cult</ta>
            <ta e="T798" id="Seg_11568" s="T797">RUS:cult</ta>
            <ta e="T810" id="Seg_11569" s="T809">RUS:cult</ta>
            <ta e="T0" id="Seg_11570" s="T820">RUS:cult</ta>
            <ta e="T822" id="Seg_11571" s="T821">RUS:gram</ta>
            <ta e="T823" id="Seg_11572" s="T822">RUS:cult</ta>
            <ta e="T834" id="Seg_11573" s="T833">RUS:cult</ta>
            <ta e="T835" id="Seg_11574" s="T834">RUS:cult</ta>
            <ta e="T846" id="Seg_11575" s="T845">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T24" id="Seg_11576" s="T23">Csub</ta>
            <ta e="T26" id="Seg_11577" s="T25">Csub</ta>
            <ta e="T120" id="Seg_11578" s="T119">Vsub Vsub Csub</ta>
            <ta e="T156" id="Seg_11579" s="T155">Vsub Vsub Csub</ta>
            <ta e="T161" id="Seg_11580" s="T160">Vsub Vsub Csub</ta>
            <ta e="T203" id="Seg_11581" s="T202">Csub</ta>
            <ta e="T210" id="Seg_11582" s="T209">Csub</ta>
            <ta e="T216" id="Seg_11583" s="T215">Csub</ta>
            <ta e="T221" id="Seg_11584" s="T220">Csub</ta>
            <ta e="T237" id="Seg_11585" s="T236">Vsub Csub</ta>
            <ta e="T248" id="Seg_11586" s="T247">Vsub Csub</ta>
            <ta e="T254" id="Seg_11587" s="T253">Csub</ta>
            <ta e="T269" id="Seg_11588" s="T268">Vsub Csub</ta>
            <ta e="T274" id="Seg_11589" s="T273">Vsub</ta>
            <ta e="T275" id="Seg_11590" s="T274">Vsub</ta>
            <ta e="T285" id="Seg_11591" s="T284">Csub</ta>
            <ta e="T404" id="Seg_11592" s="T403">Vsub Vsub Csub</ta>
            <ta e="T481" id="Seg_11593" s="T480">Vsub Vsub Csub</ta>
            <ta e="T517" id="Seg_11594" s="T516">Vsub Vsub Csub</ta>
            <ta e="T574" id="Seg_11595" s="T573">Vsub Csub Vsub</ta>
            <ta e="T591" id="Seg_11596" s="T590">Vsub Csub Vsub</ta>
            <ta e="T615" id="Seg_11597" s="T614">Csub</ta>
            <ta e="T630" id="Seg_11598" s="T629">Csub</ta>
            <ta e="T648" id="Seg_11599" s="T647">Csub</ta>
            <ta e="T657" id="Seg_11600" s="T656">Csub</ta>
            <ta e="T719" id="Seg_11601" s="T718">Csub</ta>
            <ta e="T788" id="Seg_11602" s="T787">Vsub</ta>
            <ta e="T798" id="Seg_11603" s="T797">finVins</ta>
            <ta e="T810" id="Seg_11604" s="T809">finVins</ta>
            <ta e="T0" id="Seg_11605" s="T820">Vsub</ta>
            <ta e="T823" id="Seg_11606" s="T822">Vsub</ta>
            <ta e="T834" id="Seg_11607" s="T833">Vsub</ta>
            <ta e="T835" id="Seg_11608" s="T834">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T24" id="Seg_11609" s="T23">dir:infl</ta>
            <ta e="T26" id="Seg_11610" s="T25">dir:infl</ta>
            <ta e="T120" id="Seg_11611" s="T119">dir:bare</ta>
            <ta e="T203" id="Seg_11612" s="T202">dir:infl</ta>
            <ta e="T210" id="Seg_11613" s="T209">dir:bare</ta>
            <ta e="T216" id="Seg_11614" s="T215">dir:infl</ta>
            <ta e="T221" id="Seg_11615" s="T220">dir:infl</ta>
            <ta e="T237" id="Seg_11616" s="T236">dir:infl</ta>
            <ta e="T248" id="Seg_11617" s="T247">dir:infl</ta>
            <ta e="T254" id="Seg_11618" s="T253">dir:infl</ta>
            <ta e="T269" id="Seg_11619" s="T268">dir:bare</ta>
            <ta e="T274" id="Seg_11620" s="T273">indir:infl</ta>
            <ta e="T275" id="Seg_11621" s="T274">dir:infl</ta>
            <ta e="T285" id="Seg_11622" s="T284">dir:infl</ta>
            <ta e="T481" id="Seg_11623" s="T480">dir:bare</ta>
            <ta e="T574" id="Seg_11624" s="T573">dir:bare</ta>
            <ta e="T591" id="Seg_11625" s="T590">dir:infl</ta>
            <ta e="T615" id="Seg_11626" s="T614">dir:infl</ta>
            <ta e="T630" id="Seg_11627" s="T629">dir:infl</ta>
            <ta e="T648" id="Seg_11628" s="T647">dir:infl</ta>
            <ta e="T657" id="Seg_11629" s="T656">dir:infl</ta>
            <ta e="T719" id="Seg_11630" s="T718">dir:infl</ta>
            <ta e="T788" id="Seg_11631" s="T787">dir:infl</ta>
            <ta e="T798" id="Seg_11632" s="T797">dir:bare</ta>
            <ta e="T810" id="Seg_11633" s="T809">dir:bare</ta>
            <ta e="T0" id="Seg_11634" s="T820">dir:infl</ta>
            <ta e="T822" id="Seg_11635" s="T821">dir:bare</ta>
            <ta e="T823" id="Seg_11636" s="T822">dir:infl</ta>
            <ta e="T834" id="Seg_11637" s="T833">dir:bare</ta>
            <ta e="T835" id="Seg_11638" s="T834">dir:infl</ta>
            <ta e="T846" id="Seg_11639" s="T845">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_11640" s="T1">Earlier the birth of children [was] on a birth pole. </ta>
            <ta e="T16" id="Seg_11641" s="T5">On the birth pole the children, at the time of the pregnant woman's birth one prepared a birth pole earlier.</ta>
            <ta e="T21" id="Seg_11642" s="T16">Especially building a chum, they prepared [it].</ta>
            <ta e="T28" id="Seg_11643" s="T21">The tent of the child's birth, a birth tent it was called.</ta>
            <ta e="T35" id="Seg_11644" s="T28">There they made the birth pole, the family erected it.</ta>
            <ta e="T48" id="Seg_11645" s="T35">The grandfather or the father or the mother are helping, they tie the pirth pole to a pole, pressing it onto the wood.</ta>
            <ta e="T61" id="Seg_11646" s="T48">Then, eh, at the end of the birth pole there is such an upright standing piece of wood, a small pole(?).</ta>
            <ta e="T69" id="Seg_11647" s="T61">And then the birth pole is along the bosom. </ta>
            <ta e="T74" id="Seg_11648" s="T69">The woman is then hung up and she gives birth.</ta>
            <ta e="T83" id="Seg_11649" s="T74">Under the birth, eh, on the sitting place there are pieces of fur.</ta>
            <ta e="T89" id="Seg_11650" s="T83">And on the pieces of fur, there is grass.</ta>
            <ta e="T93" id="Seg_11651" s="T89">They lay the grass there for relief, it is said. </ta>
            <ta e="T105" id="Seg_11652" s="T93">When the child falls onto it, they say "it falls onto grass", because it is with grass.</ta>
            <ta e="T108" id="Seg_11653" s="T105">"It fell onto the grass", they say.</ta>
            <ta e="T119" id="Seg_11654" s="T108">Eh, if the woman gives birth, since olden times, if it is far away…</ta>
            <ta e="T134" id="Seg_11655" s="T119">If the midwife [is] far away, if the woman, who accompanies the birth, is far away, they go with sledges and bring her.</ta>
            <ta e="T144" id="Seg_11656" s="T134">Begging "well, our child then, our daughter is going to give birth", saying.</ta>
            <ta e="T148" id="Seg_11657" s="T144">"Our woman is going to give birth", they say and bring [the midwife].</ta>
            <ta e="T157" id="Seg_11658" s="T148">Eh, if she is close, then they call this old midwife.</ta>
            <ta e="T167" id="Seg_11659" s="T157">Then the towel [for] the midwife's hands, and everything, for that it becomes clean. </ta>
            <ta e="T174" id="Seg_11660" s="T167">It's right, everything should be new, untouched things. </ta>
            <ta e="T183" id="Seg_11661" s="T174">Even a towel, a towel [for] the hands, when she gives birth to the child, the the woman is in labor.</ta>
            <ta e="T188" id="Seg_11662" s="T183">And then she has a totem.</ta>
            <ta e="T207" id="Seg_11663" s="T188">This totem, at first they come and get [it], when the woman is about to give birth, they don't let anyone into the separate tent.</ta>
            <ta e="T216" id="Seg_11664" s="T207">Who doesn't build up a tent especially, lets her give birth at home, in the living tent.</ta>
            <ta e="T225" id="Seg_11665" s="T216">Thereby they do [it] separated from the living tent, they erect the birth pole.</ta>
            <ta e="T231" id="Seg_11666" s="T225">Well and then they bring all people out.</ta>
            <ta e="T243" id="Seg_11667" s="T231">Nobody may be there, only the midwife and the woman giving birth may be there.</ta>
            <ta e="T254" id="Seg_11668" s="T243">And then the midwife comes into the tent, into the birth tent. </ta>
            <ta e="T262" id="Seg_11669" s="T254">If it is built up separately or if it is at home.</ta>
            <ta e="T274" id="Seg_11670" s="T262">And coming, when they bring her, when the old midwife goes in, cleaning everything with amber.</ta>
            <ta e="T279" id="Seg_11671" s="T274">With amber, with splinters, lighting [them], she cleans everything.</ta>
            <ta e="T287" id="Seg_11672" s="T279">She is smoking around the place, where the woman sits, around the tent: </ta>
            <ta e="T295" id="Seg_11673" s="T287">"Türkeet, türkeet, türkeet, türkeet, the dirt a cloud, the bad thoughts to the moon.</ta>
            <ta e="T306" id="Seg_11674" s="T295">Türkeet, türkeet, türkeet, the dirt a cloud, the bad thoughts to the moon, (…)!</ta>
            <ta e="T311" id="Seg_11675" s="T306">Türkeet, the dirt a cloud, the bad thoughts to the moon.</ta>
            <ta e="T318" id="Seg_11676" s="T311">(…)", saying she smokes around, three times.</ta>
            <ta e="T330" id="Seg_11677" s="T318">Then the woman, the sitting woman, [when] she doesn't sit yet, she smokes under the arm pits, for that she sits easily.</ta>
            <ta e="T336" id="Seg_11678" s="T330">Around with this smoking tool, clockwise.</ta>
            <ta e="T338" id="Seg_11679" s="T336">She doesn't go back.</ta>
            <ta e="T347" id="Seg_11680" s="T338">They don't let anybody go back, the pregnant woman not at all.</ta>
            <ta e="T351" id="Seg_11681" s="T347">"She will stay back [=will die?]", thinking, that's destiny.</ta>
            <ta e="T360" id="Seg_11682" s="T351">And then, again she turns around three times:</ta>
            <ta e="T367" id="Seg_11683" s="T360">"Türkeet, türkeet, türkeet, for that my daughter will be happy, my child.</ta>
            <ta e="T373" id="Seg_11684" s="T367">Türkeet, türkeet, türkeet, for that she will live happily, (…).</ta>
            <ta e="T380" id="Seg_11685" s="T373">Pürkeet, pürkeet, pürkeet, the bad thoughts to the moon, the dirt onto a cloud.</ta>
            <ta e="T390" id="Seg_11686" s="T380">The bad thoughts to the moon, the dirt onto a cloud, (…)", they say, they turn around three times.</ta>
            <ta e="T399" id="Seg_11687" s="T390">Then she holds fast onto the birthpole, the sitting woman, onto the birthpole where [she is] sitting. </ta>
            <ta e="T412" id="Seg_11688" s="T399">Then the old midwife takes the totem, she takes the totem, it's a hare fur, this small totem.</ta>
            <ta e="T416" id="Seg_11689" s="T412">The hare is called totem.</ta>
            <ta e="T426" id="Seg_11690" s="T416">Then, having taken tghe hare, she gives the hare to the fire to eat, she feeds the fire again. </ta>
            <ta e="T435" id="Seg_11691" s="T426">"Grandfather fire, I feed [you] with my totem, for that my child is made happy, for that she gives birth happily", saying.</ta>
            <ta e="T440" id="Seg_11692" s="T435">And then I feed my grandfather fire: </ta>
            <ta e="T446" id="Seg_11693" s="T440">"Grandfather fire, eat, [give] luck, keep the luck.</ta>
            <ta e="T456" id="Seg_11694" s="T446">Don't let any bad come closer, make her live happily, free my child happily", saying.</ta>
            <ta e="T463" id="Seg_11695" s="T456">Then this small totem, having fed [the fire], she hangs it up on the birth pole.</ta>
            <ta e="T467" id="Seg_11696" s="T463">She ties it to the birth pole.</ta>
            <ta e="T481" id="Seg_11697" s="T467">Having tied it up, if the woman giving birth absolutely can't give birth(?), the midwife helps. </ta>
            <ta e="T488" id="Seg_11698" s="T481">She seats her onto her knees, with the bottom(?), for that the child can't go back.</ta>
            <ta e="T491" id="Seg_11699" s="T488">Then she kneads her belly.</ta>
            <ta e="T501" id="Seg_11700" s="T491">And kneading the belly, helping the child, helping, that woman gives birth.</ta>
            <ta e="T506" id="Seg_11701" s="T501">The child's birth is called "Tabyskyyr".</ta>
            <ta e="T515" id="Seg_11702" s="T506">When the child falls down in the labors, they cut the child's umbilical cord, we cut.</ta>
            <ta e="T519" id="Seg_11703" s="T515">The old midwife cuts.</ta>
            <ta e="T537" id="Seg_11704" s="T519">Having cut the umbilical cord, she ties [the navel] up with a thread made of sinew, she ties [it] up with a thread made of dried sinew, for that it will fall off, when it has rotten. </ta>
            <ta e="T548" id="Seg_11705" s="T537">After five or how much days the child's navels falls off.</ta>
            <ta e="T558" id="Seg_11706" s="T548">And then the born child's mother, she washes the child's mother.</ta>
            <ta e="T568" id="Seg_11707" s="T558">She washes also the child, having washed a several times, she wraps it up.</ta>
            <ta e="T576" id="Seg_11708" s="T568">And then, they still don't lay it into the cradle.</ta>
            <ta e="T581" id="Seg_11709" s="T576">She wraps it up and lays it beneath the mum.</ta>
            <ta e="T603" id="Seg_11710" s="T581">Having washed the totem, having put the totem into a little sack, in this bag, she ties it together with the bag to the birth pole.</ta>
            <ta e="T622" id="Seg_11711" s="T603">And it hangs [lit. stands] at the birth pole, as long as the child's mother lives, in the tent where the child was born, at the birth pole where the child was born, at the birth pole where the woman gave birth.</ta>
            <ta e="T637" id="Seg_11712" s="T622">Then they say, since olden times, if one passes a campground with a birth pole, even if there is no tent, if one sees the birth pole standing there.</ta>
            <ta e="T647" id="Seg_11713" s="T637">"Well, at this campsite a woman gave birth apparently, her birth pole is standing there, the birth pole, where she gave birth."</ta>
            <ta e="T658" id="Seg_11714" s="T647">If there is a tent: "Such a child was born, apparently, the tent, where the child was born", one says.</ta>
            <ta e="T668" id="Seg_11715" s="T658">And then, then this woman, the woman having given birth is impure.</ta>
            <ta e="T673" id="Seg_11716" s="T668">Having cleaned the dirt of the birth, you'll smoke again.</ta>
            <ta e="T680" id="Seg_11717" s="T673">Under the face, around the neck, you'll smoke three times:</ta>
            <ta e="T692" id="Seg_11718" s="T680">"Türkeet, türkeet, the dirt onto a cloud, the bad thoughts to the moon, türkeet, türkeet, the dirt onto a cloud, the bad thoughts to the moon.</ta>
            <ta e="T703" id="Seg_11719" s="T692">Türkeet, türkeet, the dirt onto a cloud, the bad thoughts to the moon, (…)", you say and smoke.</ta>
            <ta e="T710" id="Seg_11720" s="T703">Then, after that, the midwife takes the child.</ta>
            <ta e="T719" id="Seg_11721" s="T710">Then she lets the woman stand up, from the bedding where she was lying in the birth tent.</ta>
            <ta e="T724" id="Seg_11722" s="T719">Then, when she has stood up, we bring her out, we go home.</ta>
            <ta e="T730" id="Seg_11723" s="T724">We lead the woman, the woman who has given birth.</ta>
            <ta e="T746" id="Seg_11724" s="T730">Then, leading her one tells: "Well, a child is born", one says to the family, to the people living in the tent.</ta>
            <ta e="T749" id="Seg_11725" s="T746">In one's own tent.</ta>
            <ta e="T761" id="Seg_11726" s="T749">She(?) tells, well, if a girl was born, one says "a girl".</ta>
            <ta e="T768" id="Seg_11727" s="T761">If a boy was born, one says "a boy".</ta>
            <ta e="T778" id="Seg_11728" s="T768">Then, when three days have passed, the father makes a cradle for the child.</ta>
            <ta e="T780" id="Seg_11729" s="T778">The grandfather helps.</ta>
            <ta e="T793" id="Seg_11730" s="T780">Then they make this cradle, tying up strong planks, strings and leather strings.</ta>
            <ta e="T805" id="Seg_11731" s="T793">Spruce wood, that's like eh plywood, they say, such stuff one doesn't have made.</ta>
            <ta e="T810" id="Seg_11732" s="T805">The bark(?) of dried wood, that's plywood.</ta>
            <ta e="T821" id="Seg_11733" s="T810">Therefore the child gets dry, they say, therefore with planks (…).</ta>
            <ta e="T833" id="Seg_11734" s="T821">And when the plank is fastened, when it is fastened, when it is fastened, when they fasten it with a string, it has a space in between.</ta>
            <ta e="T838" id="Seg_11735" s="T833">From plank to plank, there is always space in between.</ta>
            <ta e="T848" id="Seg_11736" s="T838">Then air comes in, therefore the child grows up healthily. </ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_11737" s="T1">Die Geburt der früheren Kinder [war] auf einer Gebärstange.</ta>
            <ta e="T16" id="Seg_11738" s="T5">Auf der Gebärstange die Kinder, zur Zeit der Geburt der schwangeren Frau bereitete man früher eine Gebärstange vor.</ta>
            <ta e="T21" id="Seg_11739" s="T16">Ein Tschum wurde speziell aufgebaut und vorbereitet.</ta>
            <ta e="T28" id="Seg_11740" s="T21">Das Zelt der Geburt des Kindes, das Gebärzelt, sagte man.</ta>
            <ta e="T35" id="Seg_11741" s="T28">Dort machte man die Gebärstange, die Familie stellte sie auf. </ta>
            <ta e="T48" id="Seg_11742" s="T35">Der Großvater oder der Vater oder die Mutter helfen, sie binden die Gebärstange an eine Stange und drücken sie fest auf das Holz.</ta>
            <ta e="T61" id="Seg_11743" s="T48">Dann, äh, am Ende der Gebärstange gibt es so ein aufrecht stehendes Holz, eine kleine Stange(?).</ta>
            <ta e="T69" id="Seg_11744" s="T61">Dann aber ist die Gebärstange entlang dem Busen. </ta>
            <ta e="T74" id="Seg_11745" s="T69">Die Frau ist dann aufgehängt und sie gebiert. </ta>
            <ta e="T83" id="Seg_11746" s="T74">Unter der Geburt, eh, auf dem Sitzplatz sind Fellstücke. </ta>
            <ta e="T89" id="Seg_11747" s="T83">Und auf den Fellstücken ist Gras.</ta>
            <ta e="T93" id="Seg_11748" s="T89">Das Gras legt man zur Erleichterung hin, sagt man.</ta>
            <ta e="T105" id="Seg_11749" s="T93">Wenn das Kind darauf fällt, sagt man "es fällt aufs Gras", deshalb, weil es mit Gras ist.</ta>
            <ta e="T108" id="Seg_11750" s="T105">"Es ist aufs Gras gefallen", sagt man.</ta>
            <ta e="T119" id="Seg_11751" s="T108">Äh, wenn die Frau gebiert, seit langer Zeit, wenn es weit weg ist…</ta>
            <ta e="T134" id="Seg_11752" s="T119">Wenn die Hebamme weit weg [ist], wenn die Frau, die die Geburt begleitet, weit weg ist, fährt man mit Schlitten und holt sie.</ta>
            <ta e="T144" id="Seg_11753" s="T134">Bittend "nun, unser Kind dann, unsere Tochter wird gebären", sagend.</ta>
            <ta e="T148" id="Seg_11754" s="T144">"Unsere Frau wird gebären", sagte man und holt [die Hebamme].</ta>
            <ta e="T157" id="Seg_11755" s="T148">Äh, wenn sie in der Nähe ist, dann ruft man diese alte Hebamme.</ta>
            <ta e="T167" id="Seg_11756" s="T157">Dann das Handtuch [für] die Hände der Hebamme und alles, damit es sauber wird.</ta>
            <ta e="T174" id="Seg_11757" s="T167">Es ist richtig, alles sollen neue, noch nicht angefasste Sachen sein.</ta>
            <ta e="T183" id="Seg_11758" s="T174">Sogar ein Handtuch, ein Handtuch [für] die Hände, wenn das Kind geboren wird, wenn die Frau in den Wehen liegt.</ta>
            <ta e="T188" id="Seg_11759" s="T183">Und dann hat sie einen Totem.</ta>
            <ta e="T207" id="Seg_11760" s="T188">Diesen Totem, zuerst kommt man und holt [den], wenn die Frau anfängt zu gebären, in das einzelne Zelt lässt man niemanden hinein.</ta>
            <ta e="T216" id="Seg_11761" s="T207">Wer nicht speziell ein Zelt aufbaut, der lässt [sie] zuhause gebären, in dem Zelt, wo man lebt.</ta>
            <ta e="T225" id="Seg_11762" s="T216">Dabei getrennt von dem Zelt, wo man wohnt, macht man und stellt die Gebärstange auf.</ta>
            <ta e="T231" id="Seg_11763" s="T225">Nun und dann bringt man alle Leute hinaus.</ta>
            <ta e="T243" id="Seg_11764" s="T231">Niemand darf dort sein, nur die Hebamme und die gebärende Frau dürfen dort sein.</ta>
            <ta e="T254" id="Seg_11765" s="T243">Dann aber kommt diese Hebamme ins Zelt, ins Geburtszelt.</ta>
            <ta e="T262" id="Seg_11766" s="T254">Wenn es separat aufgebaut ist oder wenn es zuhause ist.</ta>
            <ta e="T274" id="Seg_11767" s="T262">Wenn sie kommt, wenn man sie bringt, wenn die alte Hebamme hinein geht, reinigt sie alles mit Bernstein.</ta>
            <ta e="T279" id="Seg_11768" s="T274">Mit Bernstein, mit Spänen, die sie angezündet hat, säubert sie alles.</ta>
            <ta e="T287" id="Seg_11769" s="T279">Um den Platz herum, wo die Frau sitzt, um das Zelt herum, wo sie sitzt, räuchert sie:</ta>
            <ta e="T295" id="Seg_11770" s="T287">"Türkeet, türkeet, türkeet, türkeet, der Schmutz eine Wolke, die schlechten Gedanken auf den Mond.</ta>
            <ta e="T306" id="Seg_11771" s="T295">Türkeet, türkeet, türkeet, der Schmutz eine Wolke, die schlechten Gedanken auf den Mond, (…)!</ta>
            <ta e="T311" id="Seg_11772" s="T306">Türkeet, der Schmutz eine Wolke, die schlechten Gedanken auf den Mond.</ta>
            <ta e="T318" id="Seg_11773" s="T311">(…)", sagt sie und räuchert drumherum, drei mal.</ta>
            <ta e="T330" id="Seg_11774" s="T318">Dann die Frau, die sitzende Frau, [wenn] sie noch nicht sitzt, räuchert sie unter den Achselhöhlen, damit sie leicht sitzt.</ta>
            <ta e="T336" id="Seg_11775" s="T330">Mit diesem Räuchergerät drumherum, im Uhrzeigersinn.</ta>
            <ta e="T338" id="Seg_11776" s="T336">Zurück geht sie nicht. </ta>
            <ta e="T347" id="Seg_11777" s="T338">Zurück lassen sie niemanden gehen, die schwangere Frau gar nicht.</ta>
            <ta e="T351" id="Seg_11778" s="T347">"Sie bleibt zurück [=stirbt?]", denkend, das ist das Schicksal.</ta>
            <ta e="T360" id="Seg_11779" s="T351">Dann aber, drei mal dreht sie sich wieder:</ta>
            <ta e="T367" id="Seg_11780" s="T360">"Türkeet, türkeet, türkeet, damit meine Tochter glücklich ist, mein Kind.</ta>
            <ta e="T373" id="Seg_11781" s="T367">Türkeet, türkeet, türkeet, damit sie glücklich lebt, (…).</ta>
            <ta e="T380" id="Seg_11782" s="T373">Pürkeet, pürkeet, pürkeet, die schlechten Gedanken auf den Mond, den Schmutz auf eine Wolke.</ta>
            <ta e="T390" id="Seg_11783" s="T380">Die schlechten Gedanken auf den Mond, den Schmutz auf eine Wolke, (…)", sagen sie, drei mal drehen sie sich.</ta>
            <ta e="T399" id="Seg_11784" s="T390">Dann hält sie sich an der Gebärstange fest, die sitzende Frau, an der Gebärstange, wo [sie] sitzt. </ta>
            <ta e="T412" id="Seg_11785" s="T399">Dann nimmt die alte Hebamme den Totem, sie nimmt den Totem, es ist ein Hasenfell, dieser kleine Totem.</ta>
            <ta e="T416" id="Seg_11786" s="T412">Der Hase wird Totem genannt.</ta>
            <ta e="T426" id="Seg_11787" s="T416">Nachdem sie den Hasen genommen hat, gibt sie den Hasen dem Feuer zu essen, sie gibt dem Feuer wieder zu essen.</ta>
            <ta e="T435" id="Seg_11788" s="T426">"Väterchen Feuer, ich gebe [dir] meinen Totem zu essen, damit mein Kind glücklich gemacht wird, damit sie glücklich gebiert", sagt sie.</ta>
            <ta e="T440" id="Seg_11789" s="T435">Dann aber gebe ich meinem Väterchen Feuer zu essen: </ta>
            <ta e="T446" id="Seg_11790" s="T440">"Väterchen Feuer, iss, [gib] Glück, bewahre das Glück.</ta>
            <ta e="T456" id="Seg_11791" s="T446">Lass nicht Schlechtes heran, lass sie glücklich leben, befreie mein Kind glücklich", sagt sie.</ta>
            <ta e="T463" id="Seg_11792" s="T456">Dann diesen kleinen Totem, nachdem sie [dem Feuer] zu essen gegeben hat, hängt sie an der Gebärstange auf.</ta>
            <ta e="T467" id="Seg_11793" s="T463">Sie bindet [ihn] an die Gebärstange.</ta>
            <ta e="T481" id="Seg_11794" s="T467">Nachdem sie ihn festgebunden hat, wenn die gebärende Frau überhaupt nicht gebären(?) kann, hilft die Hebamme.</ta>
            <ta e="T488" id="Seg_11795" s="T481">Sie setzt sie auf die Kniee, mit dem Hintern(?), damit das Kind nicht zurück kann.</ta>
            <ta e="T491" id="Seg_11796" s="T488">Dann massiert sie den Bauch.</ta>
            <ta e="T501" id="Seg_11797" s="T491">Und wenn sie den Bauch massiert, wenn sie dem Kind hilft, dann gebiert die Frau.</ta>
            <ta e="T506" id="Seg_11798" s="T501">"Tabyskyyr" heißt die Geburt des Kindes.</ta>
            <ta e="T515" id="Seg_11799" s="T506">Wenn das Kind in den Geburtswehen hinunterfällt, schneidet man dem Kind die Nabelschnur ab, wir schneiden.</ta>
            <ta e="T519" id="Seg_11800" s="T515">Die alte Hebamme schneidet.</ta>
            <ta e="T537" id="Seg_11801" s="T519">Wenn sie die Nabelschnur abgeschnitten hat, dann bindet sie [den Nabel] mit einem Faden aus Sehnen, sie bindet mit einem Faden aus getrockneter Sehne, damit er später abfällt, wenn er durchgefault ist.</ta>
            <ta e="T548" id="Seg_11802" s="T537">Nach fünf oder wie viel Tagen fällt der Nabel des Kindes ab.</ta>
            <ta e="T558" id="Seg_11803" s="T548">Dann aber die Mutter des geborenen Kindes, sie wäscht die Mutter des Kindes.</ta>
            <ta e="T568" id="Seg_11804" s="T558">Das Kind wäscht sie auch, nachdem sie es ein paar mal gewaschen hat, wickelt sie es ein.</ta>
            <ta e="T576" id="Seg_11805" s="T568">Dann aber, man legt es noch nicht in die Wiege.</ta>
            <ta e="T581" id="Seg_11806" s="T576">Sie wickelt es ein und legt es neben die Mama.</ta>
            <ta e="T603" id="Seg_11807" s="T581">Nachdem sie den Totem abgewaschen hat, nachdem sie den Totem in ein kleines Säckchen gesteckt hat, in dieser Tasche, sie bindet ihn mit dieser Tasche an die Gebärstange.</ta>
            <ta e="T622" id="Seg_11808" s="T603">Und es hängt [wörtl. steht] an der Gebärstange, solange die Mutter des Kindes lebt, im Zelt, wo das Kind geboren wurde, an der Gebärstange, wo das Kind geboren wurde, an der Gebärstange, wo die Frau geboren hat.</ta>
            <ta e="T637" id="Seg_11809" s="T622">Dann sagt man, seit langer Zeit, wenn man an einem Lagerplatz mit Gebärstange vorbeikommt, sogar wenn es kein Zelt gibt, wenn man die Gebärstange stehen sieht.</ta>
            <ta e="T647" id="Seg_11810" s="T637">"Nun, an diesem Lagerplatz hat offenbar eine Frau geboren, ihre Gebärstange steht dort, die Gebärstange, an der sie geboren hat."</ta>
            <ta e="T658" id="Seg_11811" s="T647">Wenn es ein Zelt gibt: "So ein Kind wurde offenbar geboren, das Zelt, wo das Kind geboren wurde", sagt man.</ta>
            <ta e="T668" id="Seg_11812" s="T658">Dann aber, dann diese Frau, die Frau, die geboren hat, ist unrein.</ta>
            <ta e="T673" id="Seg_11813" s="T668">Wenn man den Schmutz der Geburt reinigt, räuchert man wieder.</ta>
            <ta e="T680" id="Seg_11814" s="T673">Unter dem Gesicht, um den Hals herum räuchert man drei mal:</ta>
            <ta e="T692" id="Seg_11815" s="T680">"Türkeet, türkeet, der Schmutz auf eine Wolke, die schlechten Gedanken auf den Mond, türkeet, türkeet, der Schmutz auf eine Wolke, die schlechten Gedanken auf den Mond.</ta>
            <ta e="T703" id="Seg_11816" s="T692">Türkeet, türkeet, den Schmutz auf eine Wolke, die schlechten Gedanken auf den Mond, (…)", sagt man und räuchert.</ta>
            <ta e="T710" id="Seg_11817" s="T703">Dann, danach nimmt die Hebamme das Kind.</ta>
            <ta e="T719" id="Seg_11818" s="T710">Dann lässt sie die Frau aufstehen, von der Bettwäsche, auf der sie gelegen hat im Geburtszelt.</ta>
            <ta e="T724" id="Seg_11819" s="T719">Dann, nachdem sie aufgestanden ist, bringen wir sie hinaus, wir gehen nach Hause.</ta>
            <ta e="T730" id="Seg_11820" s="T724">Wir führen die Frau, die geboren habende Frau.</ta>
            <ta e="T746" id="Seg_11821" s="T730">Danach, man führt sie und erzählt: "Nun, ein Kind wurde geboren", sagt man der Familie, den Leuten, die im Zelt wohnen.</ta>
            <ta e="T749" id="Seg_11822" s="T746">Im eigenen Zelt.</ta>
            <ta e="T761" id="Seg_11823" s="T749">Sie(?) erzählt, nun, wenn ein Mädchen geboren wurde, sagt man "ein Mädchen".</ta>
            <ta e="T768" id="Seg_11824" s="T761">Wenn ein Junge geboren wird, sagt man "ein Junge".</ta>
            <ta e="T778" id="Seg_11825" s="T768">Dann, wenn drei Tage vergangen sind, macht der Vater eine Wiege für das Kind.</ta>
            <ta e="T780" id="Seg_11826" s="T778">Der Großvater hilft.</ta>
            <ta e="T793" id="Seg_11827" s="T780">Dann machen sie diese Wiege, indem sie [sie] mit starken Brettern, Schnur und Lederschnüren zusammenbinden.</ta>
            <ta e="T805" id="Seg_11828" s="T793">Fichtenholz, das ist eh wie Furnier, sagt man, so etwas lässt man nicht machen.</ta>
            <ta e="T810" id="Seg_11829" s="T805">Die Rinde(?) von getrocknetem Holz, das ist Furnier.</ta>
            <ta e="T821" id="Seg_11830" s="T810">Deshalb wird das Kind trocken, sagt man, deshalb mit Brettern (…).</ta>
            <ta e="T833" id="Seg_11831" s="T821">Und wenn das Brett befestigt wird, wenn es befestigt wird, wenn man es mit einer Schnur befestigt, dann hat es einen Zwischenraum.</ta>
            <ta e="T838" id="Seg_11832" s="T833">Von Brett zu Brett ist [immer] ein Zwischenraum.</ta>
            <ta e="T848" id="Seg_11833" s="T838">Dann kommt Luft hindurch, deshalb wächst das Kind gesund auf.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_11834" s="T1">В давние времена ребёнок рождался с помощью специального приспособления для рожениц. </ta>
            <ta e="T16" id="Seg_11835" s="T5">В присспособлении ребё.. Беременной женщине к её родам приспособление для рожениц подготавливали.</ta>
            <ta e="T21" id="Seg_11836" s="T16">Отдельно чум соорудив, готовили.</ta>
            <ta e="T28" id="Seg_11837" s="T21">Младенца появления чум, родился в котором чум, называли его.</ta>
            <ta e="T35" id="Seg_11838" s="T28">В нём тогда то-оо - о приспособление для роженицы делали. Родители устанавливали.</ta>
            <ta e="T48" id="Seg_11839" s="T35">Дед ли, отец ли, мать ли вместе сообща привязывали это приспособление к шесту впритык - поперёк палку.</ta>
            <ta e="T61" id="Seg_11840" s="T48">Затем этого приспособления с краю вертикально воткнутый шест бывает, чума покрытие.</ta>
            <ta e="T69" id="Seg_11841" s="T61">Тогда вот то приспособление ведь по грудную клетки бывает. </ta>
            <ta e="T74" id="Seg_11842" s="T69">Женщина, на нём повиснув, рожает.</ta>
            <ta e="T83" id="Seg_11843" s="T74">Где роды будут, под ней, э-э там где она сидит, шкурка бывает.</ta>
            <ta e="T89" id="Seg_11844" s="T83">И вот на этой шкуре трава бывает. </ta>
            <ta e="T93" id="Seg_11845" s="T89">Траву кладут для облегчения, думая.</ta>
            <ta e="T105" id="Seg_11846" s="T93">На него ребёнок когда падает: "На траву падает ", - говорят так, потому что с травой. </ta>
            <ta e="T108" id="Seg_11847" s="T105">" На траву упал",-- говорят.</ta>
            <ta e="T119" id="Seg_11848" s="T108">Э-э, если женщина рожает с давних времен, далеко если находится вот …</ta>
            <ta e="T134" id="Seg_11849" s="T119">повитуха - старушка далеко если находится, роды принимающая женщина если далеко находится, на оленьих упряжках съездив, привозят. </ta>
            <ta e="T144" id="Seg_11850" s="T134">Прося: " Вот, дочь наша тогда -то тогда, дочь родит," -- говоря. </ta>
            <ta e="T148" id="Seg_11851" s="T144">"Женщина родит", -- говоря, привозят.</ta>
            <ta e="T157" id="Seg_11852" s="T148">Э-э, если рядом находится, то зовут ту бабушку - повитуху.</ta>
            <ta e="T167" id="Seg_11853" s="T157">При этом той повитухи ручное полотенце, всё такое, чтобы чистым было.</ta>
            <ta e="T174" id="Seg_11854" s="T167">Хорошо бывает, когда всё новое, не тронутые вещи должны быть.</ta>
            <ta e="T183" id="Seg_11855" s="T174">Полотенце даже, ручное полотенце -- родить время когда придёт, в схватках.. при женских схватках.</ta>
            <ta e="T188" id="Seg_11856" s="T183">Тогда вот шайтана имеет.</ta>
            <ta e="T207" id="Seg_11857" s="T188">Этого шайтана вот… Сначала забирать приходят, женщина рожать когда начинает, вот отдельно.. В тот отдельный чум никого не пускают ведь.</ta>
            <ta e="T216" id="Seg_11858" s="T207">Кто если отдельный чум не ставит, дома принимают роды дома, где живут в чуме. </ta>
            <ta e="T225" id="Seg_11859" s="T216">И при этом от чума, где живут, отдельно ставят приспособление для родов устанавливают.</ta>
            <ta e="T231" id="Seg_11860" s="T225">И при этом людей всех выводят. </ta>
            <ta e="T243" id="Seg_11861" s="T231">Никого не должно быть. Одни -- павитуха с роженицей должны быть там.</ta>
            <ta e="T254" id="Seg_11862" s="T243">Тогда вот эта павитуха приходит в чум, для родов дом, в чумик. </ta>
            <ta e="T262" id="Seg_11863" s="T254">Отдельно если поставлен будет или же в дом если подготовлен.</ta>
            <ta e="T274" id="Seg_11864" s="T262">И вот придя, как приведут, бабка - павитуха как зайдёт, и всё вокруг янтарём очищая. </ta>
            <ta e="T279" id="Seg_11865" s="T274">Янтарём, стружками, поджигая, всё очищает.</ta>
            <ta e="T287" id="Seg_11866" s="T279">То женщина где сидит место, где сидит чум вокруг очищает: </ta>
            <ta e="T295" id="Seg_11867" s="T287">"Т6ркэ-э-эт, т6ркээт, т6ркээт, т6ркээт, нечистота -- облако, плохие думы -- в месяц,</ta>
            <ta e="T306" id="Seg_11868" s="T295">Т6ркэ-э-эт, т6ркээт, т6ркээт, нечистота твоя -- облако, плохие думы -- в месяц, туомун-сыраму8, туому8-сыраму8, туому8-сыраму8!" </ta>
            <ta e="T311" id="Seg_11869" s="T306">"Т6ркэ-э-эт, нечистота -- облако, плохие думы -- в месяц. </ta>
            <ta e="T318" id="Seg_11870" s="T311">Туому8-сырамуу8, туому8-сыраму8 ", -- приговаривая очищает в круговую, трижды.</ta>
            <ta e="T330" id="Seg_11871" s="T318">Затем женщину, сидящую женщину, до того как не присела, под мышками, чтобы легко села, желая, очищает.</ta>
            <ta e="T336" id="Seg_11872" s="T330">Тем же очищающим средством вокруг, по солнцу. </ta>
            <ta e="T338" id="Seg_11873" s="T336">За её спину не идёт.</ta>
            <ta e="T347" id="Seg_11874" s="T338">Назад никого не пускают, беременную женщину ни вкакую. </ta>
            <ta e="T351" id="Seg_11875" s="T347">А не то умрёт, думая. Это судьба.</ta>
            <ta e="T360" id="Seg_11876" s="T351">Тогда вот ведь это дале.. трижды возвраается опять:</ta>
            <ta e="T367" id="Seg_11877" s="T360">"Т6ркээт, т6ркээт, т6ркээт, счастливо чтобы жила дочь моя, мой ребёнок.</ta>
            <ta e="T373" id="Seg_11878" s="T367">Т6ркээт, т6ркээт, т6ркээт, счастливо чтобы жила Туому8-сураму8. </ta>
            <ta e="T380" id="Seg_11879" s="T373">Т6ркээт, т6ркээт, прук, думы - в месяце, нечистота - в облаке.</ta>
            <ta e="T390" id="Seg_11880" s="T380">Думы -- в месяце, нечистота - в облаке. Туому8-сырамуу8, туому8--сыраму8", -- приговаривают. Трижды поворачивают. </ta>
            <ta e="T399" id="Seg_11881" s="T390">Тем самым за приспособление держится, эта сидящая женщина, где сидит -- специальным приспособлением.</ta>
            <ta e="T412" id="Seg_11882" s="T399">Тогда вот шайтана берёт бабушка - павитуха. Этот шайтан из заячьего меха тот шайтанчик. </ta>
            <ta e="T416" id="Seg_11883" s="T412">Заяц шайтаном прозван бывает. </ta>
            <ta e="T426" id="Seg_11884" s="T416">Тогда зайчика взяв в руки, зайчика огню кормит. Огонь она тоже кормит.</ta>
            <ta e="T435" id="Seg_11885" s="T426">"Огонь-дедушка, шайтанчика кормлю, чтобы дочь мою осчастливил, удачно родила чтобы!" - приговаривая. </ta>
            <ta e="T440" id="Seg_11886" s="T435">Тогда вот дедушку-огня прикармливаю: </ta>
            <ta e="T446" id="Seg_11887" s="T440">"Огонь - дедушка, кушай, дай счастья , удачу оберегай. </ta>
            <ta e="T456" id="Seg_11888" s="T446">Ничего плохого не подноси. Счастливой жизни дай, дочь мою счастливо освободи", - приговаривая.</ta>
            <ta e="T463" id="Seg_11889" s="T456">Затем этого шайтанчика, покормив, на приспособление для родов вешает. </ta>
            <ta e="T467" id="Seg_11890" s="T463">И к этому приспособлению привязывает.</ta>
            <ta e="T481" id="Seg_11891" s="T467">И привязав, если роженица никак не может родить, помогает повитуха.</ta>
            <ta e="T488" id="Seg_11892" s="T481">На колени сажает, основанием хвоста, чтобы ребёнок назад не пошёл, думая.</ta>
            <ta e="T491" id="Seg_11893" s="T488">Затем живот массирует. </ta>
            <ta e="T501" id="Seg_11894" s="T491">И вот живот массируя, помогая, того малыша, помогая, рожает эта женщина.</ta>
            <ta e="T506" id="Seg_11895" s="T501">Вот рожает -- то называется ребёнка рождение. </ta>
            <ta e="T515" id="Seg_11896" s="T506">При потугах, когда ребёнок падает, этому младенцу пупок обрезаем. </ta>
            <ta e="T519" id="Seg_11897" s="T515">Та бабушка-повитуха отрезает.</ta>
            <ta e="T537" id="Seg_11898" s="T519">Этот пупок обрезав вот, завязывает жильными нитками. Засушенными (обработанными) жильными нитками завязывает, чтобы отвалился потом, напрочь прогнив.</ta>
            <ta e="T548" id="Seg_11899" s="T537">Пять дней спустя или через несколько дней спустя отпадывает младенца пупок.</ta>
            <ta e="T558" id="Seg_11900" s="T548">Затем вот ребёнка мать упавшего, этого ребёнка мать моет. </ta>
            <ta e="T568" id="Seg_11901" s="T558">Ребёнка тоже моет несколько раз помыв заворачивает. </ta>
            <ta e="T576" id="Seg_11902" s="T568">Затем вот… В люльку ещё не укладывают ведь. </ta>
            <ta e="T581" id="Seg_11903" s="T576">Заворачивет и рядом с мамой кладёт. </ta>
            <ta e="T603" id="Seg_11904" s="T581">Затем шайтана промыв… Этого шайтана в мешочек кладёт. В ту меховую сумку, с тем меховым мешком же к приспособлению для родов привязывает.</ta>
            <ta e="T622" id="Seg_11905" s="T603">И на этом приспособлении висит этого ребёнка мамы на протяжении всей её жизни. Этот ребёнок где появивился на свет чуме внутри. Женщина рожала на котором приспособлении.</ta>
            <ta e="T637" id="Seg_11906" s="T622">Тогда говорят, с давних времён, если с приспособлением старое стоянку проедут, чума даже если не будет, приспособление если увидят…</ta>
            <ta e="T647" id="Seg_11907" s="T637">" Ээй, на этом кочевье женщина рожала, оказывется! Приспособление её стоит, при родах использованное приспособление. </ta>
            <ta e="T658" id="Seg_11908" s="T647">Если чум будет, то такой - то ребёнок родился, оказвается, тот ребёнок когда появился на свет чум", - говорят. </ta>
            <ta e="T668" id="Seg_11909" s="T658">Таким образом, затем эта женщина, рожавшая женщина, ребёнка родившая женщина, нечистая ведь. </ta>
            <ta e="T673" id="Seg_11910" s="T668">Послеродовую грязь очищая, опять заговорами очищаешь.</ta>
            <ta e="T680" id="Seg_11911" s="T673">Снизу лица, шеи вокруг три раза приговаривая очищаешь: </ta>
            <ta e="T692" id="Seg_11912" s="T680">"Т6ркээт, т6ркээт, нечистота - в облоко, думы -- в месяце. Т6ркэ-эт, т6ркээт, нечистота -- в облако, думы -- в месяце.</ta>
            <ta e="T703" id="Seg_11913" s="T692">Т6ркэт-т6ркэт, нечистота -- в облако, думы -- в месяц, туому8-сырамуу8, туому8-сураму8, туому8- сурамуу8", -- приговаривая, приговаривая очищаешь. </ta>
            <ta e="T710" id="Seg_11914" s="T703">После всего этого ребёнка забирает повитуха. </ta>
            <ta e="T719" id="Seg_11915" s="T710">Затем женщину поднимает с перины, на котором лежала, в рожавшем чуме.</ta>
            <ta e="T724" id="Seg_11916" s="T719">Затем встав выводим её, домой идём. </ta>
            <ta e="T730" id="Seg_11917" s="T724">Под руки ведём женщину, роженицу.</ta>
            <ta e="T746" id="Seg_11918" s="T730">Затем взяв за руки, сопровождая и тут же рассказывает: " О-о, ребёнок родился, -- рассказывает родителям, домочадцам. </ta>
            <ta e="T749" id="Seg_11919" s="T746">В домашнем чуме.</ta>
            <ta e="T761" id="Seg_11920" s="T749">Тут рассказывая говорит -- о-о, если девочка родилась - девочка, говорят. </ta>
            <ta e="T768" id="Seg_11921" s="T761">Если мальчик родится, то мальчик, говорят.</ta>
            <ta e="T778" id="Seg_11922" s="T768">Затем спустя три дня, отец люльку мастерит для ребёнка. </ta>
            <ta e="T780" id="Seg_11923" s="T778">Дедушка помогает.</ta>
            <ta e="T793" id="Seg_11924" s="T780">Тогда вот эту люльку из прочных досок, из верёвки, из замши верёвкой, натягивая, мастерят.</ta>
            <ta e="T805" id="Seg_11925" s="T793">Тонкую доску, это как э-э фанера как, говорят, такое вот не заставляют делать.</ta>
            <ta e="T810" id="Seg_11926" s="T805">Засохшего дерева шкура --- эта фанера. </ta>
            <ta e="T821" id="Seg_11927" s="T810">Поэтому вот ребёнок зосохнет, говорят. </ta>
            <ta e="T833" id="Seg_11928" s="T821">А доска как натянется вот, натянется, натянется, из верёвки натянув, если сделают, то оно с зазором бывает.</ta>
            <ta e="T838" id="Seg_11929" s="T833">Доска от доске тоже с зазором бывает.</ta>
            <ta e="T848" id="Seg_11930" s="T838">Потому и воздух проходит ведь, поэтому ребёнок здоровенький растёт.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T0" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
