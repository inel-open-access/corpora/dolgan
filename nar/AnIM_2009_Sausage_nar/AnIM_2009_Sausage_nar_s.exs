<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD0A441F8-25A4-E74E-6FA8-F649BFA06A16">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnIM_2009_Sausage_narr</transcription-name>
         <referenced-file url="AnIM_2009_Sausage_nar.wav" />
         <referenced-file url="AnIM_2009_Sausage_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\AnIM_2009_Sausage_nar\AnIM_2009_Sausage_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">411</ud-information>
            <ud-information attribute-name="# HIAT:w">328</ud-information>
            <ud-information attribute-name="# e">278</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">40</ud-information>
            <ud-information attribute-name="# sc">4</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnIM">
            <abbreviation>AnIM</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="2.84" type="appl" />
         <tli id="T1" time="3.371" type="appl" />
         <tli id="T2" time="3.903" type="appl" />
         <tli id="T3" time="4.434" type="appl" />
         <tli id="T4" time="4.966" type="appl" />
         <tli id="T5" time="5.497" type="appl" />
         <tli id="T6" time="6.029" type="appl" />
         <tli id="T7" time="6.833212120160799" />
         <tli id="T8" time="7.34" type="appl" />
         <tli id="T9" time="7.82" type="appl" />
         <tli id="T10" time="8.3" type="appl" />
         <tli id="T11" time="8.78" type="appl" />
         <tli id="T12" time="9.26" type="appl" />
         <tli id="T13" time="9.74" type="appl" />
         <tli id="T14" time="10.22" type="appl" />
         <tli id="T15" time="10.7" type="appl" />
         <tli id="T16" time="11.18" type="appl" />
         <tli id="T17" time="11.66" type="appl" />
         <tli id="T18" time="12.14" type="appl" />
         <tli id="T19" time="12.786663641816201" />
         <tli id="T20" time="13.318" type="appl" />
         <tli id="T21" time="13.656" type="appl" />
         <tli id="T22" time="13.993" type="appl" />
         <tli id="T23" time="14.331" type="appl" />
         <tli id="T24" time="14.669" type="appl" />
         <tli id="T25" time="15.007" type="appl" />
         <tli id="T26" time="15.344" type="appl" />
         <tli id="T27" time="15.682" type="appl" />
         <tli id="T28" time="16.346376701106614" />
         <tli id="T29" time="16.825" type="appl" />
         <tli id="T30" time="17.29" type="appl" />
         <tli id="T31" time="17.755" type="appl" />
         <tli id="T32" time="18.22" type="appl" />
         <tli id="T33" time="18.685" type="appl" />
         <tli id="T34" time="19.15" type="appl" />
         <tli id="T35" time="19.615" type="appl" />
         <tli id="T36" time="20.08" type="appl" />
         <tli id="T37" time="20.819" type="appl" />
         <tli id="T38" time="21.347" type="appl" />
         <tli id="T39" time="21.876" type="appl" />
         <tli id="T40" time="22.404" type="appl" />
         <tli id="T41" time="22.933" type="appl" />
         <tli id="T42" time="23.461" type="appl" />
         <tli id="T43" time="23.99" type="appl" />
         <tli id="T44" time="24.519" type="appl" />
         <tli id="T45" time="25.047" type="appl" />
         <tli id="T46" time="25.576" type="appl" />
         <tli id="T47" time="26.104" type="appl" />
         <tli id="T48" time="26.633" type="appl" />
         <tli id="T49" time="27.161" type="appl" />
         <tli id="T50" time="27.69" type="appl" />
         <tli id="T51" time="28.244" type="appl" />
         <tli id="T52" time="28.728" type="appl" />
         <tli id="T53" time="29.212" type="appl" />
         <tli id="T54" time="29.696" type="appl" />
         <tli id="T55" time="30.18" type="appl" />
         <tli id="T56" time="30.664" type="appl" />
         <tli id="T57" time="31.148" type="appl" />
         <tli id="T58" time="31.632" type="appl" />
         <tli id="T59" time="32.116" type="appl" />
         <tli id="T60" time="32.906662368135606" />
         <tli id="T61" time="33.362" type="appl" />
         <tli id="T62" time="33.835" type="appl" />
         <tli id="T63" time="34.307" type="appl" />
         <tli id="T64" time="34.78" type="appl" />
         <tli id="T65" time="35.252" type="appl" />
         <tli id="T66" time="35.724" type="appl" />
         <tli id="T67" time="36.197" type="appl" />
         <tli id="T68" time="36.669" type="appl" />
         <tli id="T69" time="37.141" type="appl" />
         <tli id="T70" time="37.614" type="appl" />
         <tli id="T71" time="38.086" type="appl" />
         <tli id="T72" time="38.559" type="appl" />
         <tli id="T73" time="39.031" type="appl" />
         <tli id="T74" time="39.503" type="appl" />
         <tli id="T75" time="39.976" type="appl" />
         <tli id="T76" time="40.448" type="appl" />
         <tli id="T77" time="40.92" type="appl" />
         <tli id="T78" time="41.393" type="appl" />
         <tli id="T79" time="41.865" type="appl" />
         <tli id="T80" time="42.338" type="appl" />
         <tli id="T81" time="42.81" type="appl" />
         <tli id="T82" time="43.503" type="appl" />
         <tli id="T83" time="44.105" type="appl" />
         <tli id="T84" time="44.708" type="appl" />
         <tli id="T85" time="45.311" type="appl" />
         <tli id="T86" time="45.913" type="appl" />
         <tli id="T87" time="46.516" type="appl" />
         <tli id="T88" time="47.119" type="appl" />
         <tli id="T89" time="47.721" type="appl" />
         <tli id="T90" time="48.324" type="appl" />
         <tli id="T91" time="48.927" type="appl" />
         <tli id="T92" time="49.529" type="appl" />
         <tli id="T93" time="50.132" type="appl" />
         <tli id="T94" time="50.735" type="appl" />
         <tli id="T95" time="51.337" type="appl" />
         <tli id="T96" time="52.426657903172824" />
         <tli id="T97" time="53.363" type="appl" />
         <tli id="T98" time="53.837" type="appl" />
         <tli id="T99" time="54.94569199450273" />
         <tli id="T100" time="55.107" type="appl" />
         <tli id="T101" time="55.763" type="appl" />
         <tli id="T102" time="56.42" type="appl" />
         <tli id="T103" time="57.077" type="appl" />
         <tli id="T104" time="57.733" type="appl" />
         <tli id="T105" time="58.50333070907918" />
         <tli id="T106" time="58.884" type="appl" />
         <tli id="T107" time="59.328" type="appl" />
         <tli id="T108" time="59.772" type="appl" />
         <tli id="T109" time="60.216" type="appl" />
         <tli id="T110" time="60.8066622465112" />
         <tli id="T331" time="61.43224359734807" />
         <tli id="T111" time="62.44555895565483" />
         <tli id="T112" time="63.01888211890735" />
         <tli id="T113" time="63.32554334576335" />
         <tli id="T114" time="63.60553837897969" />
         <tli id="T115" time="63.95886544470509" />
         <tli id="T116" time="65.31217477191741" />
         <tli id="T117" time="65.43883919170577" />
         <tli id="T118" time="65.63216909559324" />
         <tli id="T119" time="66.19215916202593" />
         <tli id="T120" time="66.45215455001254" />
         <tli id="T121" time="66.81881471255775" />
         <tli id="T123" time="67.29213964966156" />
         <tli id="T124" time="67.753" type="appl" />
         <tli id="T125" time="68.125" type="appl" />
         <tli id="T126" time="68.498" type="appl" />
         <tli id="T127" time="68.87" type="appl" />
         <tli id="T128" time="69.243" type="appl" />
         <tli id="T129" time="69.616" type="appl" />
         <tli id="T131" time="70.361" type="appl" />
         <tli id="T132" time="70.733" type="appl" />
         <tli id="T133" time="71.106" type="appl" />
         <tli id="T134" time="71.479" type="appl" />
         <tli id="T135" time="71.851" type="appl" />
         <tli id="T136" time="72.224" type="appl" />
         <tli id="T137" time="72.597" type="appl" />
         <tli id="T138" time="72.969" type="appl" />
         <tli id="T140" time="73.714" type="appl" />
         <tli id="T142" time="74.46" type="appl" />
         <tli id="T143" time="74.832" type="appl" />
         <tli id="T144" time="75.205" type="appl" />
         <tli id="T146" time="75.95" type="appl" />
         <tli id="T329" time="76.1965" type="intp" />
         <tli id="T147" time="76.443" type="appl" />
         <tli id="T148" time="76.927" type="appl" />
         <tli id="T149" time="77.41" type="appl" />
         <tli id="T150" time="77.893" type="appl" />
         <tli id="T151" time="78.377" type="appl" />
         <tli id="T152" time="78.86" type="appl" />
         <tli id="T153" time="79.343" type="appl" />
         <tli id="T155" time="80.31" type="appl" />
         <tli id="T332" time="80.535" type="intp" />
         <tli id="T156" time="80.76" type="appl" />
         <tli id="T157" time="81.17" type="appl" />
         <tli id="T158" time="81.58" type="appl" />
         <tli id="T159" time="81.99" type="appl" />
         <tli id="T160" time="82.4" type="appl" />
         <tli id="T161" time="82.81" type="appl" />
         <tli id="T162" time="83.22" type="appl" />
         <tli id="T163" time="83.63" type="appl" />
         <tli id="T165" time="85.00515877480035" />
         <tli id="T166" time="85.36" type="appl" />
         <tli id="T167" time="85.77" type="appl" />
         <tli id="T168" time="86.18" type="appl" />
         <tli id="T169" time="86.59" type="appl" />
         <tli id="T170" time="87.0" type="appl" />
         <tli id="T171" time="87.41" type="appl" />
         <tli id="T172" time="87.82" type="appl" />
         <tli id="T173" time="88.23" type="appl" />
         <tli id="T174" time="88.64" type="appl" />
         <tli id="T175" time="89.05" type="appl" />
         <tli id="T176" time="89.46" type="appl" />
         <tli id="T177" time="89.87" type="appl" />
         <tli id="T178" time="90.28" type="appl" />
         <tli id="T179" time="90.69" type="appl" />
         <tli id="T180" time="91.1" type="appl" />
         <tli id="T181" time="91.51" type="appl" />
         <tli id="T182" time="91.92" type="appl" />
         <tli id="T183" time="92.33" type="appl" />
         <tli id="T184" time="92.74" type="appl" />
         <tli id="T185" time="93.15" type="appl" />
         <tli id="T186" time="93.56" type="appl" />
         <tli id="T187" time="93.97" type="appl" />
         <tli id="T188" time="94.693" type="appl" />
         <tli id="T189" time="95.367" type="appl" />
         <tli id="T190" time="96.04" type="appl" />
         <tli id="T191" time="96.713" type="appl" />
         <tli id="T192" time="97.387" type="appl" />
         <tli id="T193" time="98.06" type="appl" />
         <tli id="T194" time="98.733" type="appl" />
         <tli id="T195" time="99.407" type="appl" />
         <tli id="T196" time="100.08" type="appl" />
         <tli id="T197" time="100.869" type="appl" />
         <tli id="T198" time="101.229" type="appl" />
         <tli id="T199" time="101.589" type="appl" />
         <tli id="T200" time="101.95" type="appl" />
         <tli id="T201" time="102.31" type="appl" />
         <tli id="T202" time="102.67" type="appl" />
         <tli id="T203" time="103.11000037655104" />
         <tli id="T204" time="103.904" type="appl" />
         <tli id="T205" time="104.609" type="appl" />
         <tli id="T206" time="105.313" type="appl" />
         <tli id="T207" time="106.017" type="appl" />
         <tli id="T208" time="106.721" type="appl" />
         <tli id="T209" time="107.426" type="appl" />
         <tli id="T210" time="108.39666440982857" />
         <tli id="T333" time="108.66083220491429" type="intp" />
         <tli id="T211" time="108.925" type="appl" />
         <tli id="T212" time="109.379" type="appl" />
         <tli id="T213" time="109.834" type="appl" />
         <tli id="T214" time="110.289" type="appl" />
         <tli id="T215" time="110.744" type="appl" />
         <tli id="T216" time="111.198" type="appl" />
         <tli id="T217" time="111.653" type="appl" />
         <tli id="T218" time="112.108" type="appl" />
         <tli id="T219" time="112.563" type="appl" />
         <tli id="T220" time="113.017" type="appl" />
         <tli id="T221" time="113.472" type="appl" />
         <tli id="T222" time="113.927" type="appl" />
         <tli id="T223" time="114.382" type="appl" />
         <tli id="T224" time="114.836" type="appl" />
         <tli id="T225" time="115.291" type="appl" />
         <tli id="T226" time="115.746" type="appl" />
         <tli id="T227" time="116.201" type="appl" />
         <tli id="T229" time="117.41000061756047" />
         <tli id="T230" time="120.304" type="appl" />
         <tli id="T231" time="121.158" type="appl" />
         <tli id="T232" time="122.012" type="appl" />
         <tli id="T233" time="122.866" type="appl" />
         <tli id="T234" time="123.72" type="appl" />
         <tli id="T235" time="124.574" type="appl" />
         <tli id="T236" time="125.428" type="appl" />
         <tli id="T237" time="126.282" type="appl" />
         <tli id="T238" time="127.136" type="appl" />
         <tli id="T239" time="127.50440488799555" />
         <tli id="T334" time="128.11120244399777" type="intp" />
         <tli id="T240" time="128.718" type="appl" />
         <tli id="T241" time="129.136" type="appl" />
         <tli id="T243" time="130.1863234597374" />
         <tli id="T244" time="130.592" type="appl" />
         <tli id="T245" time="131.055" type="appl" />
         <tli id="T246" time="131.518" type="appl" />
         <tli id="T247" time="131.98" type="appl" />
         <tli id="T248" time="132.442" type="appl" />
         <tli id="T249" time="132.905" type="appl" />
         <tli id="T250" time="133.368" type="appl" />
         <tli id="T251" time="133.836662352838" />
         <tli id="T252" time="135.155" type="appl" />
         <tli id="T253" time="135.8779933295195" />
         <tli id="T335" time="137.92349666475974" type="intp" />
         <tli id="T254" time="139.969" type="appl" />
         <tli id="T255" time="140.217" type="appl" />
         <tli id="T256" time="140.465" type="appl" />
         <tli id="T257" time="140.713" type="appl" />
         <tli id="T259" time="141.209" type="appl" />
         <tli id="T260" time="142.4241402293808" />
         <tli id="T336" time="142.52709348625388" type="intp" />
         <tli id="T261" time="142.63004674312694" type="intp" />
         <tli id="T262" time="142.733" type="appl" />
         <tli id="T263" time="143.428" type="appl" />
         <tli id="T264" time="144.122" type="appl" />
         <tli id="T265" time="144.816" type="appl" />
         <tli id="T266" time="145.511" type="appl" />
         <tli id="T122" time="147.43750856205008" type="intp" />
         <tli id="T130" time="148.40076284307514" type="intp" />
         <tli id="T268" time="149.3640171241002" />
         <tli id="T269" time="149.365" type="appl" />
         <tli id="T270" time="149.89" type="appl" />
         <tli id="T271" time="150.415" type="appl" />
         <tli id="T272" time="150.94" type="appl" />
         <tli id="T273" time="151.465" type="appl" />
         <tli id="T274" time="151.99" type="appl" />
         <tli id="T275" time="152.515" type="appl" />
         <tli id="T276" time="153.0533266800736" />
         <tli id="T277" time="153.78" type="appl" />
         <tli id="T278" time="154.44" type="appl" />
         <tli id="T279" time="155.1" type="appl" />
         <tli id="T280" time="155.553" type="appl" />
         <tli id="T281" time="155.967" type="appl" />
         <tli id="T282" time="157.7905343142302" />
         <tli id="T337" time="157.87968954282013" type="intp" />
         <tli id="T283" time="157.96884477141006" type="intp" />
         <tli id="T284" time="158.058" type="appl" />
         <tli id="T285" time="158.676" type="appl" />
         <tli id="T286" time="159.293" type="appl" />
         <tli id="T287" time="159.91" type="appl" />
         <tli id="T288" time="160.528" type="appl" />
         <tli id="T289" time="161.145" type="appl" />
         <tli id="T290" time="161.762" type="appl" />
         <tli id="T291" time="162.379" type="appl" />
         <tli id="T292" time="162.996" type="appl" />
         <tli id="T294" time="164.231" type="appl" />
         <tli id="T295" time="164.662" type="appl" />
         <tli id="T296" time="165.145" type="appl" />
         <tli id="T297" time="165.685" type="appl" />
         <tli id="T298" time="165.973" type="appl" />
         <tli id="T299" time="166.33333850406515" />
         <tli id="T300" time="167.07" type="appl" />
         <tli id="T301" time="167.665" type="appl" />
         <tli id="T302" time="168.259" type="appl" />
         <tli id="T303" time="168.854" type="appl" />
         <tli id="T304" time="170.09031613051965" />
         <tli id="T305" time="170.991" type="appl" />
         <tli id="T306" time="171.486" type="appl" />
         <tli id="T307" time="171.98" type="appl" />
         <tli id="T308" time="172.474" type="appl" />
         <tli id="T309" time="172.968" type="appl" />
         <tli id="T310" time="173.463" type="appl" />
         <tli id="T311" time="174.1703349016029" />
         <tli id="T338" time="174.41899999999998" type="intp" />
         <tli id="T312" time="174.881" type="appl" />
         <tli id="T313" time="175.502" type="appl" />
         <tli id="T314" time="176.124" type="appl" />
         <tli id="T315" time="176.745" type="appl" />
         <tli id="T316" time="177.366" type="appl" />
         <tli id="T317" time="177.987" type="appl" />
         <tli id="T318" time="178.609" type="appl" />
         <tli id="T319" time="179.23" type="appl" />
         <tli id="T320" time="179.851" type="appl" />
         <tli id="T321" time="180.472" type="appl" />
         <tli id="T322" time="181.094" type="appl" />
         <tli id="T323" time="181.715" type="appl" />
         <tli id="T324" time="182.336" type="appl" />
         <tli id="T325" time="182.957" type="appl" />
         <tli id="T327" time="184.69672369663897" />
         <tli id="T328" time="185.9" type="appl" />
         <tli id="T330" time="186.15" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AnIM"
                      type="t">
         <timeline-fork end="T44" start="T41">
            <tli id="T41.tx.1" />
            <tli id="T41.tx.2" />
         </timeline-fork>
         <timeline-fork end="T50" start="T48">
            <tli id="T48.tx.1" />
         </timeline-fork>
         <timeline-fork end="T77" start="T73">
            <tli id="T73.tx.1" />
            <tli id="T73.tx.2" />
            <tli id="T73.tx.3" />
         </timeline-fork>
         <timeline-fork end="T111" start="T110">
            <tli id="T110.tx.1" />
         </timeline-fork>
         <timeline-fork end="T131" start="T123">
            <tli id="T123.tx.1" />
            <tli id="T123.tx.2" />
            <tli id="T123.tx.3" />
            <tli id="T123.tx.4" />
            <tli id="T123.tx.5" />
            <tli id="T123.tx.6" />
         </timeline-fork>
         <timeline-fork end="T146" start="T134">
            <tli id="T134.tx.1" />
            <tli id="T134.tx.2" />
            <tli id="T134.tx.3" />
            <tli id="T134.tx.4" />
            <tli id="T134.tx.5" />
            <tli id="T134.tx.6" />
            <tli id="T134.tx.7" />
            <tli id="T134.tx.8" />
         </timeline-fork>
         <timeline-fork end="T151" start="T149">
            <tli id="T149.tx.1" />
         </timeline-fork>
         <timeline-fork end="T212" start="T210">
            <tli id="T210.tx.1" />
            <tli id="T210.tx.2" />
         </timeline-fork>
         <timeline-fork end="T230" start="T229">
            <tli id="T229.tx.1" />
            <tli id="T229.tx.2" />
            <tli id="T229.tx.3" />
            <tli id="T229.tx.4" />
            <tli id="T229.tx.5" />
            <tli id="T229.tx.6" />
         </timeline-fork>
         <timeline-fork end="T259" start="T253">
            <tli id="T253.tx.1" />
            <tli id="T253.tx.2" />
            <tli id="T253.tx.3" />
            <tli id="T253.tx.4" />
            <tli id="T253.tx.5" />
         </timeline-fork>
         <timeline-fork end="T264" start="T260">
            <tli id="T260.tx.1" />
            <tli id="T260.tx.2" />
            <tli id="T260.tx.3" />
            <tli id="T260.tx.4" />
         </timeline-fork>
         <timeline-fork end="T266" start="T264">
            <tli id="T264.tx.1" />
         </timeline-fork>
         <timeline-fork end="T290" start="T282">
            <tli id="T282.tx.1" />
            <tli id="T282.tx.2" />
            <tli id="T282.tx.3" />
            <tli id="T282.tx.4" />
            <tli id="T282.tx.5" />
            <tli id="T282.tx.6" />
            <tli id="T282.tx.7" />
            <tli id="T282.tx.8" />
         </timeline-fork>
         <timeline-fork end="T315" start="T311">
            <tli id="T311.tx.1" />
            <tli id="T311.tx.2" />
            <tli id="T311.tx.3" />
            <tli id="T311.tx.4" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T130" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Taba</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kaːnɨnan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">bu͡ollagɨnan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">kimi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">oŋorobut</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">kolbasa</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">ke</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_27" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">Iti</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">bu͡ollagɨna</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">kim</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">tabanɨ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">ölöröllör</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">bu͡o</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">innʼe</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">gɨnan</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">baran</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">tazikka</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">kaːn</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_62" n="HIAT:w" s="T18">ɨlabɨt</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_66" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">I</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">onu</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">bu͡ollagɨna</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_77" n="HIAT:w" s="T22">turu͡orabɨt</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_80" n="HIAT:w" s="T23">bu͡o</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_83" n="HIAT:w" s="T24">štobɨ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_86" n="HIAT:w" s="T25">sɨvorotka</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_89" n="HIAT:w" s="T26">bu͡olu͡ogun</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_92" n="HIAT:w" s="T27">ke</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_96" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T28">Sɨvorotkanɨ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_101" n="HIAT:w" s="T29">kimniː</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_104" n="HIAT:w" s="T30">mʼattɨːbɨt</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_107" n="HIAT:w" s="T31">mjattɨːbɨt</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_110" n="HIAT:w" s="T32">sɨvorotka</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_113" n="HIAT:w" s="T33">sɨvorotka</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_116" n="HIAT:w" s="T34">bu͡olar</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_119" n="HIAT:w" s="T35">ontuŋ</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_123" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_125" n="HIAT:w" s="T36">I</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_128" n="HIAT:w" s="T37">onu</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_131" n="HIAT:w" s="T38">bu͡ollagɨna</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_134" n="HIAT:w" s="T39">ihigineːgileri</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_137" n="HIAT:w" s="T40">ɨlabɨt</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41.tx.1" id="Seg_141" n="HIAT:w" s="T41">vot</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41.tx.2" id="Seg_144" n="HIAT:w" s="T41.tx.1">eto</ts>
                  <nts id="Seg_145" n="HIAT:ip">,</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_148" n="HIAT:w" s="T41.tx.2">vnutrennosti</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_152" n="HIAT:w" s="T44">onton</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_155" n="HIAT:w" s="T45">kimi</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_158" n="HIAT:w" s="T46">hu͡on</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_161" n="HIAT:w" s="T47">haːga</ts>
                  <nts id="Seg_162" n="HIAT:ip">,</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48.tx.1" id="Seg_165" n="HIAT:w" s="T48">est</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_168" n="HIAT:w" s="T48.tx.1">tam</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_172" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_174" n="HIAT:w" s="T50">Hu͡on</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_177" n="HIAT:w" s="T51">haːgɨn</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_180" n="HIAT:w" s="T52">ɨlagɨn</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_183" n="HIAT:w" s="T53">bu͡o</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_186" n="HIAT:w" s="T54">homorsotun</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_189" n="HIAT:w" s="T55">ɨlagɨn</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_192" n="HIAT:w" s="T56">bu͡o</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_195" n="HIAT:w" s="T57">ačatɨttan</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_198" n="HIAT:w" s="T58">emi͡e</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_201" n="HIAT:w" s="T59">ɨlagɨn</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_205" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_207" n="HIAT:w" s="T60">Innʼe</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_210" n="HIAT:w" s="T61">barɨkaːn</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_213" n="HIAT:w" s="T62">huːjan</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_216" n="HIAT:w" s="T63">huːjan</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_219" n="HIAT:w" s="T64">kannʼan</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_222" n="HIAT:w" s="T65">baran</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_225" n="HIAT:w" s="T66">huːjattɨːgɨn</ts>
                  <nts id="Seg_226" n="HIAT:ip">,</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_229" n="HIAT:w" s="T67">innʼe</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_232" n="HIAT:w" s="T68">gɨnan</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_235" n="HIAT:w" s="T69">baran</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_238" n="HIAT:w" s="T70">kaːnɨŋ</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_241" n="HIAT:w" s="T71">bu͡ollagɨna</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_245" n="HIAT:w" s="T72">sɨvorotka</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73.tx.1" id="Seg_248" n="HIAT:w" s="T73">polučaetsa</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73.tx.2" id="Seg_251" n="HIAT:w" s="T73.tx.1">kak</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73.tx.3" id="Seg_254" n="HIAT:w" s="T73.tx.2">budto</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_257" n="HIAT:w" s="T73.tx.3">prozračnaj</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_260" n="HIAT:w" s="T77">bagajɨ</ts>
                  <nts id="Seg_261" n="HIAT:ip">,</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_264" n="HIAT:w" s="T78">uː</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_267" n="HIAT:w" s="T79">kaːn</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_270" n="HIAT:w" s="T80">bu͡olar</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_274" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_276" n="HIAT:w" s="T81">I</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_279" n="HIAT:w" s="T82">onu</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_282" n="HIAT:w" s="T83">bu͡ollagɨna</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_285" n="HIAT:w" s="T84">kimi͡eke</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_288" n="HIAT:w" s="T85">kutagɨn</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_291" n="HIAT:w" s="T86">ol</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_294" n="HIAT:w" s="T87">homorsolorga</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_297" n="HIAT:w" s="T88">kutagɨn</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_300" n="HIAT:w" s="T89">bɨːčastarga</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_303" n="HIAT:w" s="T90">kutagɨn</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_306" n="HIAT:w" s="T91">baːjagɨn</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_309" n="HIAT:w" s="T92">i</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_312" n="HIAT:w" s="T93">kim</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_315" n="HIAT:w" s="T94">kimi͡eke</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_318" n="HIAT:w" s="T95">buharagɨn</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_322" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_324" n="HIAT:w" s="T96">Itiː</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_327" n="HIAT:w" s="T97">uːga</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_330" n="HIAT:w" s="T98">uːragɨn</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_334" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_336" n="HIAT:w" s="T99">I</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_339" n="HIAT:w" s="T100">minut</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_342" n="HIAT:w" s="T101">pjat</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_345" n="HIAT:w" s="T102">desjat</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_348" n="HIAT:w" s="T103">buhar</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_351" n="HIAT:w" s="T104">ontuŋ</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_355" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_357" n="HIAT:w" s="T105">I</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_360" n="HIAT:w" s="T106">oruːgun</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_363" n="HIAT:w" s="T107">i</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_366" n="HIAT:w" s="T108">kolbasa</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_369" n="HIAT:w" s="T109">bu͡olar</ts>
                  <nts id="Seg_370" n="HIAT:ip">.</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_373" n="HIAT:u" s="T110">
                  <ts e="T110.tx.1" id="Seg_375" n="HIAT:w" s="T110">Tak</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_378" n="HIAT:w" s="T110.tx.1">bɨstro</ts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_382" n="HIAT:w" s="T111">türgennik</ts>
                  <nts id="Seg_383" n="HIAT:ip">!</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_386" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_388" n="HIAT:w" s="T112">Heee</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_391" n="HIAT:w" s="T113">türgen</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_394" n="HIAT:w" s="T114">bagaj</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_397" n="HIAT:w" s="T115">vot</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_400" n="HIAT:w" s="T116">eto</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_403" n="HIAT:w" s="T117">že</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_406" n="HIAT:w" s="T118">kaːnɨŋ</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_409" n="HIAT:w" s="T119">uhunnuk</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_412" n="HIAT:w" s="T120">turu͡ogun</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_415" n="HIAT:w" s="T121">naːda</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_419" n="HIAT:u" s="T123">
                  <ts e="T123.tx.1" id="Seg_421" n="HIAT:w" s="T123">Celɨj</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123.tx.2" id="Seg_424" n="HIAT:w" s="T123.tx.1">denʼ</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123.tx.3" id="Seg_427" n="HIAT:w" s="T123.tx.2">počti</ts>
                  <nts id="Seg_428" n="HIAT:ip">,</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123.tx.4" id="Seg_431" n="HIAT:w" s="T123.tx.3">utrom</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123.tx.5" id="Seg_434" n="HIAT:w" s="T123.tx.4">naprimer</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123.tx.6" id="Seg_437" n="HIAT:w" s="T123.tx.5">v</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_440" n="HIAT:w" s="T123.tx.6">odinnadtsatʼ</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_443" n="HIAT:w" s="T131">kaːn</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_446" n="HIAT:w" s="T132">ɨlagɨn</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_449" n="HIAT:w" s="T133">da</ts>
                  <nts id="Seg_450" n="HIAT:ip">,</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx.1" id="Seg_453" n="HIAT:w" s="T134">i</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx.2" id="Seg_456" n="HIAT:w" s="T134.tx.1">večeram</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx.3" id="Seg_459" n="HIAT:w" s="T134.tx.2">možno</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx.4" id="Seg_462" n="HIAT:w" s="T134.tx.3">v</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx.5" id="Seg_465" n="HIAT:w" s="T134.tx.4">semʼ</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx.6" id="Seg_468" n="HIAT:w" s="T134.tx.5">vosemʼ</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx.7" id="Seg_471" n="HIAT:w" s="T134.tx.6">uže</ts>
                  <nts id="Seg_472" n="HIAT:ip">,</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx.8" id="Seg_475" n="HIAT:w" s="T134.tx.7">sɨvorotka</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_478" n="HIAT:w" s="T134.tx.8">polučaetsa</ts>
                  <nts id="Seg_479" n="HIAT:ip">.</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_482" n="HIAT:u" s="T146">
                  <nts id="Seg_483" n="HIAT:ip">(</nts>
                  <nts id="Seg_484" n="HIAT:ip">(</nts>
                  <ats e="T329" id="Seg_485" n="HIAT:non-pho" s="T146">…</ats>
                  <nts id="Seg_486" n="HIAT:ip">)</nts>
                  <nts id="Seg_487" n="HIAT:ip">)</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_490" n="HIAT:w" s="T329">tɨmnɨː</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_493" n="HIAT:w" s="T147">hirge</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_496" n="HIAT:w" s="T148">onton</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149.tx.1" id="Seg_499" n="HIAT:w" s="T149">ne</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_502" n="HIAT:w" s="T149.tx.1">polučaetsa</ts>
                  <nts id="Seg_503" n="HIAT:ip">,</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_506" n="HIAT:w" s="T151">buhar</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_509" n="HIAT:w" s="T152">ɨllɨbat</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_512" n="HIAT:w" s="T153">bu͡o</ts>
                  <nts id="Seg_513" n="HIAT:ip">.</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_516" n="HIAT:u" s="T155">
                  <ts e="T332" id="Seg_518" n="HIAT:w" s="T155">Na</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_521" n="HIAT:w" s="T332">teplom</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_524" n="HIAT:w" s="T156">meste</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_527" n="HIAT:w" s="T157">turu͡ogun</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_530" n="HIAT:w" s="T158">naːda</ts>
                  <nts id="Seg_531" n="HIAT:ip">,</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_534" n="HIAT:w" s="T159">kaːnɨŋ</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_537" n="HIAT:w" s="T160">ke</ts>
                  <nts id="Seg_538" n="HIAT:ip">,</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_541" n="HIAT:w" s="T161">štob</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_544" n="HIAT:w" s="T162">sɨvorotkalaːnɨ͡agɨn</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_547" n="HIAT:w" s="T163">ke</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_551" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_553" n="HIAT:w" s="T165">A</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_556" n="HIAT:w" s="T166">sɨvorotka</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_559" n="HIAT:w" s="T167">komullar</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_562" n="HIAT:w" s="T168">i</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_565" n="HIAT:w" s="T169">onu</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_568" n="HIAT:w" s="T170">možno</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_571" n="HIAT:w" s="T171">onu</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_574" n="HIAT:w" s="T172">dʼe</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_577" n="HIAT:w" s="T173">ol</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_580" n="HIAT:w" s="T174">kimni͡egin</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_583" n="HIAT:w" s="T175">bu͡o</ts>
                  <nts id="Seg_584" n="HIAT:ip">,</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_587" n="HIAT:w" s="T176">poka</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_590" n="HIAT:w" s="T177">sɨvorotka</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_593" n="HIAT:w" s="T178">ontuŋ</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_596" n="HIAT:w" s="T179">iti</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_599" n="HIAT:w" s="T180">ol</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_602" n="HIAT:w" s="T181">homorso</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_605" n="HIAT:w" s="T182">ihiger</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_608" n="HIAT:w" s="T183">uːragɨn</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_611" n="HIAT:w" s="T184">bu͡o</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_614" n="HIAT:w" s="T185">kutagɨn</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_617" n="HIAT:w" s="T186">bu͡o</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_621" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_623" n="HIAT:w" s="T187">Baːjagɨn</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_626" n="HIAT:w" s="T188">uːragɨn</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_629" n="HIAT:w" s="T189">onton</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_632" n="HIAT:w" s="T190">emi͡e</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_635" n="HIAT:w" s="T191">kutuja</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_638" n="HIAT:w" s="T192">baːr</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_641" n="HIAT:w" s="T193">kutujalaːk</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_644" n="HIAT:w" s="T194">vnutrennosti</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_647" n="HIAT:w" s="T195">kutuja</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_651" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_653" n="HIAT:w" s="T196">Kutuja</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_656" n="HIAT:w" s="T197">ol</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_659" n="HIAT:w" s="T198">ihiger</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_662" n="HIAT:w" s="T199">emi͡e</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_665" n="HIAT:w" s="T200">možno</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_668" n="HIAT:w" s="T201">kutu͡okkun</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_671" n="HIAT:w" s="T202">kaːnɨ</ts>
                  <nts id="Seg_672" n="HIAT:ip">.</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_675" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_677" n="HIAT:w" s="T203">Sɨvorotka</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_680" n="HIAT:w" s="T204">onno</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_683" n="HIAT:w" s="T205">kim</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_686" n="HIAT:w" s="T206">bu͡olaːččɨ</ts>
                  <nts id="Seg_687" n="HIAT:ip">,</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_690" n="HIAT:w" s="T207">agɨjak</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_693" n="HIAT:w" s="T208">sɨvorotka</ts>
                  <nts id="Seg_694" n="HIAT:ip">,</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_697" n="HIAT:w" s="T209">nemnožko</ts>
                  <nts id="Seg_698" n="HIAT:ip">.</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_701" n="HIAT:u" s="T210">
                  <ts e="T210.tx.1" id="Seg_703" n="HIAT:w" s="T210">Kolbasa</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210.tx.2" id="Seg_706" n="HIAT:w" s="T210.tx.1">polučaetsa</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_709" n="HIAT:w" s="T210.tx.2">malo</ts>
                  <nts id="Seg_710" n="HIAT:ip">,</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_713" n="HIAT:w" s="T212">eto</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_716" n="HIAT:w" s="T213">elbek</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_719" n="HIAT:w" s="T214">tabanɨ</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_722" n="HIAT:w" s="T215">ölördökküne</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_725" n="HIAT:w" s="T216">sɨvorotka</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_728" n="HIAT:w" s="T217">mnogo</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_731" n="HIAT:w" s="T218">bu͡olar</ts>
                  <nts id="Seg_732" n="HIAT:ip">,</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_735" n="HIAT:w" s="T219">elbek</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_738" n="HIAT:w" s="T220">bu͡olar</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_741" n="HIAT:w" s="T221">bu͡o</ts>
                  <nts id="Seg_742" n="HIAT:ip">,</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_745" n="HIAT:w" s="T222">oččogo</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_748" n="HIAT:w" s="T223">onu</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_751" n="HIAT:w" s="T224">bu͡o</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_754" n="HIAT:w" s="T225">elbek</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_757" n="HIAT:w" s="T226">kolbasa</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_760" n="HIAT:w" s="T227">bu͡olar</ts>
                  <nts id="Seg_761" n="HIAT:ip">.</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_764" n="HIAT:u" s="T229">
                  <ts e="T229.tx.1" id="Seg_766" n="HIAT:w" s="T229">A</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229.tx.2" id="Seg_769" n="HIAT:w" s="T229.tx.1">to</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229.tx.3" id="Seg_772" n="HIAT:w" s="T229.tx.2">iz</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229.tx.4" id="Seg_775" n="HIAT:w" s="T229.tx.3">odnogo</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229.tx.5" id="Seg_778" n="HIAT:w" s="T229.tx.4">oleni</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_780" n="HIAT:ip">(</nts>
                  <nts id="Seg_781" n="HIAT:ip">(</nts>
                  <ats e="T229.tx.6" id="Seg_782" n="HIAT:non-pho" s="T229.tx.5">…</ats>
                  <nts id="Seg_783" n="HIAT:ip">)</nts>
                  <nts id="Seg_784" n="HIAT:ip">)</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_787" n="HIAT:w" s="T229.tx.6">tam</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_790" n="HIAT:w" s="T230">homorsotugar</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_793" n="HIAT:w" s="T231">kutagɨn</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_796" n="HIAT:w" s="T232">hu͡on</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_799" n="HIAT:w" s="T233">hu͡on</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_802" n="HIAT:w" s="T234">hu͡on</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_805" n="HIAT:w" s="T235">haːgɨgar</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_808" n="HIAT:w" s="T236">kutagɨn</ts>
                  <nts id="Seg_809" n="HIAT:ip">,</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_812" n="HIAT:w" s="T237">bɨːčagɨgar</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_815" n="HIAT:w" s="T238">ke</ts>
                  <nts id="Seg_816" n="HIAT:ip">.</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_819" n="HIAT:u" s="T239">
                  <ts e="T334" id="Seg_821" n="HIAT:w" s="T239">Eto</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_824" n="HIAT:w" s="T334">kiska</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_827" n="HIAT:w" s="T240">naverno</ts>
                  <nts id="Seg_828" n="HIAT:ip">,</nts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_831" n="HIAT:w" s="T241">bɨːčas</ts>
                  <nts id="Seg_832" n="HIAT:ip">.</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_835" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_837" n="HIAT:w" s="T243">Onnugu</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_840" n="HIAT:w" s="T244">kutagɨn</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_843" n="HIAT:w" s="T245">bu͡o</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_846" n="HIAT:w" s="T246">ontuŋ</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_849" n="HIAT:w" s="T247">kolbasa</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_852" n="HIAT:w" s="T248">bu͡olar</ts>
                  <nts id="Seg_853" n="HIAT:ip">,</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_856" n="HIAT:w" s="T249">minnʼiges</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_859" n="HIAT:w" s="T250">bagajɨ</ts>
                  <nts id="Seg_860" n="HIAT:ip">.</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_863" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_865" n="HIAT:w" s="T251">Da</ts>
                  <nts id="Seg_866" n="HIAT:ip">?</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_869" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_871" n="HIAT:w" s="T252">Vkusnaja</ts>
                  <nts id="Seg_872" n="HIAT:ip">.</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_875" n="HIAT:u" s="T253">
                  <ts e="T253.tx.1" id="Seg_877" n="HIAT:w" s="T253">A</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253.tx.2" id="Seg_880" n="HIAT:w" s="T253.tx.1">sto</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253.tx.3" id="Seg_883" n="HIAT:w" s="T253.tx.2">vɨ</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253.tx.4" id="Seg_886" n="HIAT:w" s="T253.tx.3">ljubite</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253.tx.5" id="Seg_889" n="HIAT:w" s="T253.tx.4">bolshe</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_892" n="HIAT:w" s="T253.tx.5">vsevo</ts>
                  <nts id="Seg_893" n="HIAT:ip">?</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_896" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_898" n="HIAT:w" s="T259">Ja</ts>
                  <nts id="Seg_899" n="HIAT:ip">?</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_902" n="HIAT:u" s="T260">
                  <ts e="T260.tx.1" id="Seg_904" n="HIAT:w" s="T260">Ja</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260.tx.2" id="Seg_907" n="HIAT:w" s="T260.tx.1">ljublju</ts>
                  <nts id="Seg_908" n="HIAT:ip">,</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260.tx.3" id="Seg_911" n="HIAT:w" s="T260.tx.2">etu</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260.tx.4" id="Seg_914" n="HIAT:w" s="T260.tx.3">kalbasu</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_917" n="HIAT:w" s="T260.tx.4">ljublju</ts>
                  <nts id="Seg_918" n="HIAT:ip">,</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264.tx.1" id="Seg_921" n="HIAT:w" s="T264">voobshe</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_924" n="HIAT:w" s="T264.tx.1">ljublju</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_927" n="HIAT:w" s="T266">tɨl</ts>
                  <nts id="Seg_928" n="HIAT:ip">,</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_931" n="HIAT:w" s="T122">fkusnɨj</ts>
                  <nts id="Seg_932" n="HIAT:ip">.</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T328" id="Seg_934" n="sc" s="T268">
               <ts e="T276" id="Seg_936" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_938" n="HIAT:w" s="T268">I</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_941" n="HIAT:w" s="T269">öttükke</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_944" n="HIAT:w" s="T270">bu͡olaːččɨ</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_947" n="HIAT:w" s="T271">minnʼiges</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_950" n="HIAT:w" s="T272">hɨ͡a</ts>
                  <nts id="Seg_951" n="HIAT:ip">,</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_954" n="HIAT:w" s="T273">hɨ͡alaːk</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_957" n="HIAT:w" s="T274">öttükke</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_960" n="HIAT:w" s="T275">ke</ts>
                  <nts id="Seg_961" n="HIAT:ip">.</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_964" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_966" n="HIAT:w" s="T276">Hɨ͡a</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_969" n="HIAT:w" s="T277">bu͡olaːččɨ</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_972" n="HIAT:w" s="T278">öttükke</ts>
                  <nts id="Seg_973" n="HIAT:ip">.</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_976" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_978" n="HIAT:w" s="T279">Onnuk</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_981" n="HIAT:w" s="T280">minnʼiges</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_984" n="HIAT:w" s="T281">vkusnɨj</ts>
                  <nts id="Seg_985" n="HIAT:ip">.</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_988" n="HIAT:u" s="T282">
                  <ts e="T282.tx.1" id="Seg_990" n="HIAT:w" s="T282">Ja</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.2" id="Seg_993" n="HIAT:w" s="T282.tx.1">v</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.3" id="Seg_996" n="HIAT:w" s="T282.tx.2">asnavnom</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.4" id="Seg_999" n="HIAT:w" s="T282.tx.3">damashnij</ts>
                  <nts id="Seg_1000" n="HIAT:ip">,</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.5" id="Seg_1003" n="HIAT:w" s="T282.tx.4">ot</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.6" id="Seg_1006" n="HIAT:w" s="T282.tx.5">dikogo</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.7" id="Seg_1009" n="HIAT:w" s="T282.tx.6">olenja</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282.tx.8" id="Seg_1012" n="HIAT:w" s="T282.tx.7">mjasa</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1015" n="HIAT:w" s="T282.tx.8">voobshe</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1018" n="HIAT:w" s="T290">kimniːller</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1021" n="HIAT:w" s="T291">različajutsa</ts>
                  <nts id="Seg_1022" n="HIAT:ip">.</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1025" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1027" n="HIAT:w" s="T294">Da</ts>
                  <nts id="Seg_1028" n="HIAT:ip">?</nts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1031" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1033" n="HIAT:w" s="T295">Da</ts>
                  <nts id="Seg_1034" n="HIAT:ip">.</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1037" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1039" n="HIAT:w" s="T296">A</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1042" n="HIAT:w" s="T297">shto</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1045" n="HIAT:w" s="T298">vkusnee</ts>
                  <nts id="Seg_1046" n="HIAT:ip">?</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1049" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1051" n="HIAT:w" s="T299">Mne</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1054" n="HIAT:w" s="T300">kazhetsja</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1058" n="HIAT:w" s="T301">vkusnee</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1061" n="HIAT:w" s="T302">dikogo</ts>
                  <nts id="Seg_1062" n="HIAT:ip">,</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1065" n="HIAT:w" s="T303">dikij</ts>
                  <nts id="Seg_1066" n="HIAT:ip">.</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1069" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1071" n="HIAT:w" s="T304">Kɨːlɨn</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1074" n="HIAT:w" s="T305">kɨːlɨn</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1077" n="HIAT:w" s="T306">ete</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1080" n="HIAT:w" s="T307">minnʼiges</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1083" n="HIAT:w" s="T308">bu͡o</ts>
                  <nts id="Seg_1084" n="HIAT:ip">,</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1087" n="HIAT:w" s="T309">čem</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1090" n="HIAT:w" s="T310">damashnij</ts>
                  <nts id="Seg_1091" n="HIAT:ip">.</nts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1094" n="HIAT:u" s="T311">
                  <ts e="T311.tx.1" id="Seg_1096" n="HIAT:w" s="T311">To</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311.tx.2" id="Seg_1099" n="HIAT:w" s="T311.tx.1">li</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311.tx.3" id="Seg_1102" n="HIAT:w" s="T311.tx.2">mɨ</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311.tx.4" id="Seg_1105" n="HIAT:w" s="T311.tx.3">sami</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1108" n="HIAT:w" s="T311.tx.4">ne</ts>
                  <nts id="Seg_1109" n="HIAT:ip">,</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1112" n="HIAT:w" s="T315">bejebitiger</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1115" n="HIAT:w" s="T316">taba</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1118" n="HIAT:w" s="T317">etin</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1121" n="HIAT:w" s="T318">olus</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1124" n="HIAT:w" s="T319">oččoloːččuta</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1127" n="HIAT:w" s="T320">hu͡okpun</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1130" n="HIAT:w" s="T321">kɨːlɨn</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1133" n="HIAT:w" s="T322">ete</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1136" n="HIAT:w" s="T323">vkusnee</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1139" n="HIAT:w" s="T324">taba</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1142" n="HIAT:w" s="T325">etineːger</ts>
                  <nts id="Seg_1143" n="HIAT:ip">.</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1146" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1148" n="HIAT:w" s="T327">Minnʼiges</ts>
                  <nts id="Seg_1149" n="HIAT:ip">.</nts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T130" id="Seg_1151" n="sc" s="T0">
               <ts e="T1" id="Seg_1153" n="e" s="T0">Taba </ts>
               <ts e="T2" id="Seg_1155" n="e" s="T1">kaːnɨnan </ts>
               <ts e="T3" id="Seg_1157" n="e" s="T2">bu͡ollagɨnan </ts>
               <ts e="T4" id="Seg_1159" n="e" s="T3">kimi </ts>
               <ts e="T5" id="Seg_1161" n="e" s="T4">oŋorobut, </ts>
               <ts e="T6" id="Seg_1163" n="e" s="T5">kolbasa </ts>
               <ts e="T7" id="Seg_1165" n="e" s="T6">ke. </ts>
               <ts e="T8" id="Seg_1167" n="e" s="T7">Iti </ts>
               <ts e="T9" id="Seg_1169" n="e" s="T8">bu͡ollagɨna </ts>
               <ts e="T10" id="Seg_1171" n="e" s="T9">kim </ts>
               <ts e="T11" id="Seg_1173" n="e" s="T10">tabanɨ </ts>
               <ts e="T12" id="Seg_1175" n="e" s="T11">ölöröllör </ts>
               <ts e="T13" id="Seg_1177" n="e" s="T12">bu͡o </ts>
               <ts e="T14" id="Seg_1179" n="e" s="T13">innʼe </ts>
               <ts e="T15" id="Seg_1181" n="e" s="T14">gɨnan </ts>
               <ts e="T16" id="Seg_1183" n="e" s="T15">baran </ts>
               <ts e="T17" id="Seg_1185" n="e" s="T16">tazikka </ts>
               <ts e="T18" id="Seg_1187" n="e" s="T17">kaːn </ts>
               <ts e="T19" id="Seg_1189" n="e" s="T18">ɨlabɨt. </ts>
               <ts e="T20" id="Seg_1191" n="e" s="T19">I </ts>
               <ts e="T21" id="Seg_1193" n="e" s="T20">onu </ts>
               <ts e="T22" id="Seg_1195" n="e" s="T21">bu͡ollagɨna </ts>
               <ts e="T23" id="Seg_1197" n="e" s="T22">turu͡orabɨt </ts>
               <ts e="T24" id="Seg_1199" n="e" s="T23">bu͡o </ts>
               <ts e="T25" id="Seg_1201" n="e" s="T24">štobɨ </ts>
               <ts e="T26" id="Seg_1203" n="e" s="T25">sɨvorotka </ts>
               <ts e="T27" id="Seg_1205" n="e" s="T26">bu͡olu͡ogun </ts>
               <ts e="T28" id="Seg_1207" n="e" s="T27">ke. </ts>
               <ts e="T29" id="Seg_1209" n="e" s="T28">Sɨvorotkanɨ </ts>
               <ts e="T30" id="Seg_1211" n="e" s="T29">kimniː </ts>
               <ts e="T31" id="Seg_1213" n="e" s="T30">mʼattɨːbɨt </ts>
               <ts e="T32" id="Seg_1215" n="e" s="T31">mjattɨːbɨt </ts>
               <ts e="T33" id="Seg_1217" n="e" s="T32">sɨvorotka </ts>
               <ts e="T34" id="Seg_1219" n="e" s="T33">sɨvorotka </ts>
               <ts e="T35" id="Seg_1221" n="e" s="T34">bu͡olar </ts>
               <ts e="T36" id="Seg_1223" n="e" s="T35">ontuŋ. </ts>
               <ts e="T37" id="Seg_1225" n="e" s="T36">I </ts>
               <ts e="T38" id="Seg_1227" n="e" s="T37">onu </ts>
               <ts e="T39" id="Seg_1229" n="e" s="T38">bu͡ollagɨna </ts>
               <ts e="T40" id="Seg_1231" n="e" s="T39">ihigineːgileri </ts>
               <ts e="T41" id="Seg_1233" n="e" s="T40">ɨlabɨt, </ts>
               <ts e="T44" id="Seg_1235" n="e" s="T41">vot eto, vnutrennosti, </ts>
               <ts e="T45" id="Seg_1237" n="e" s="T44">onton </ts>
               <ts e="T46" id="Seg_1239" n="e" s="T45">kimi </ts>
               <ts e="T47" id="Seg_1241" n="e" s="T46">hu͡on </ts>
               <ts e="T48" id="Seg_1243" n="e" s="T47">haːga, </ts>
               <ts e="T50" id="Seg_1245" n="e" s="T48">est tam. </ts>
               <ts e="T51" id="Seg_1247" n="e" s="T50">Hu͡on </ts>
               <ts e="T52" id="Seg_1249" n="e" s="T51">haːgɨn </ts>
               <ts e="T53" id="Seg_1251" n="e" s="T52">ɨlagɨn </ts>
               <ts e="T54" id="Seg_1253" n="e" s="T53">bu͡o </ts>
               <ts e="T55" id="Seg_1255" n="e" s="T54">homorsotun </ts>
               <ts e="T56" id="Seg_1257" n="e" s="T55">ɨlagɨn </ts>
               <ts e="T57" id="Seg_1259" n="e" s="T56">bu͡o </ts>
               <ts e="T58" id="Seg_1261" n="e" s="T57">ačatɨttan </ts>
               <ts e="T59" id="Seg_1263" n="e" s="T58">emi͡e </ts>
               <ts e="T60" id="Seg_1265" n="e" s="T59">ɨlagɨn. </ts>
               <ts e="T61" id="Seg_1267" n="e" s="T60">Innʼe </ts>
               <ts e="T62" id="Seg_1269" n="e" s="T61">barɨkaːn </ts>
               <ts e="T63" id="Seg_1271" n="e" s="T62">huːjan </ts>
               <ts e="T64" id="Seg_1273" n="e" s="T63">huːjan </ts>
               <ts e="T65" id="Seg_1275" n="e" s="T64">kannʼan </ts>
               <ts e="T66" id="Seg_1277" n="e" s="T65">baran </ts>
               <ts e="T67" id="Seg_1279" n="e" s="T66">huːjattɨːgɨn, </ts>
               <ts e="T68" id="Seg_1281" n="e" s="T67">innʼe </ts>
               <ts e="T69" id="Seg_1283" n="e" s="T68">gɨnan </ts>
               <ts e="T70" id="Seg_1285" n="e" s="T69">baran </ts>
               <ts e="T71" id="Seg_1287" n="e" s="T70">kaːnɨŋ </ts>
               <ts e="T72" id="Seg_1289" n="e" s="T71">bu͡ollagɨna, </ts>
               <ts e="T73" id="Seg_1291" n="e" s="T72">sɨvorotka </ts>
               <ts e="T77" id="Seg_1293" n="e" s="T73">polučaetsa kak budto prozračnaj </ts>
               <ts e="T78" id="Seg_1295" n="e" s="T77">bagajɨ, </ts>
               <ts e="T79" id="Seg_1297" n="e" s="T78">uː </ts>
               <ts e="T80" id="Seg_1299" n="e" s="T79">kaːn </ts>
               <ts e="T81" id="Seg_1301" n="e" s="T80">bu͡olar. </ts>
               <ts e="T82" id="Seg_1303" n="e" s="T81">I </ts>
               <ts e="T83" id="Seg_1305" n="e" s="T82">onu </ts>
               <ts e="T84" id="Seg_1307" n="e" s="T83">bu͡ollagɨna </ts>
               <ts e="T85" id="Seg_1309" n="e" s="T84">kimi͡eke </ts>
               <ts e="T86" id="Seg_1311" n="e" s="T85">kutagɨn </ts>
               <ts e="T87" id="Seg_1313" n="e" s="T86">ol </ts>
               <ts e="T88" id="Seg_1315" n="e" s="T87">homorsolorga </ts>
               <ts e="T89" id="Seg_1317" n="e" s="T88">kutagɨn </ts>
               <ts e="T90" id="Seg_1319" n="e" s="T89">bɨːčastarga </ts>
               <ts e="T91" id="Seg_1321" n="e" s="T90">kutagɨn </ts>
               <ts e="T92" id="Seg_1323" n="e" s="T91">baːjagɨn </ts>
               <ts e="T93" id="Seg_1325" n="e" s="T92">i </ts>
               <ts e="T94" id="Seg_1327" n="e" s="T93">kim </ts>
               <ts e="T95" id="Seg_1329" n="e" s="T94">kimi͡eke </ts>
               <ts e="T96" id="Seg_1331" n="e" s="T95">buharagɨn. </ts>
               <ts e="T97" id="Seg_1333" n="e" s="T96">Itiː </ts>
               <ts e="T98" id="Seg_1335" n="e" s="T97">uːga </ts>
               <ts e="T99" id="Seg_1337" n="e" s="T98">uːragɨn. </ts>
               <ts e="T100" id="Seg_1339" n="e" s="T99">I </ts>
               <ts e="T101" id="Seg_1341" n="e" s="T100">minut </ts>
               <ts e="T102" id="Seg_1343" n="e" s="T101">pjat </ts>
               <ts e="T103" id="Seg_1345" n="e" s="T102">desjat </ts>
               <ts e="T104" id="Seg_1347" n="e" s="T103">buhar </ts>
               <ts e="T105" id="Seg_1349" n="e" s="T104">ontuŋ. </ts>
               <ts e="T106" id="Seg_1351" n="e" s="T105">I </ts>
               <ts e="T107" id="Seg_1353" n="e" s="T106">oruːgun </ts>
               <ts e="T108" id="Seg_1355" n="e" s="T107">i </ts>
               <ts e="T109" id="Seg_1357" n="e" s="T108">kolbasa </ts>
               <ts e="T110" id="Seg_1359" n="e" s="T109">bu͡olar. </ts>
               <ts e="T111" id="Seg_1361" n="e" s="T110">Tak bɨstro, </ts>
               <ts e="T112" id="Seg_1363" n="e" s="T111">türgennik! </ts>
               <ts e="T113" id="Seg_1365" n="e" s="T112">Heee </ts>
               <ts e="T114" id="Seg_1367" n="e" s="T113">türgen </ts>
               <ts e="T115" id="Seg_1369" n="e" s="T114">bagaj </ts>
               <ts e="T116" id="Seg_1371" n="e" s="T115">vot </ts>
               <ts e="T117" id="Seg_1373" n="e" s="T116">eto </ts>
               <ts e="T118" id="Seg_1375" n="e" s="T117">že </ts>
               <ts e="T119" id="Seg_1377" n="e" s="T118">kaːnɨŋ </ts>
               <ts e="T120" id="Seg_1379" n="e" s="T119">uhunnuk </ts>
               <ts e="T121" id="Seg_1381" n="e" s="T120">turu͡ogun </ts>
               <ts e="T123" id="Seg_1383" n="e" s="T121">naːda. </ts>
               <ts e="T131" id="Seg_1385" n="e" s="T123">Celɨj denʼ počti, utrom naprimer v odinnadtsatʼ </ts>
               <ts e="T132" id="Seg_1387" n="e" s="T131">kaːn </ts>
               <ts e="T133" id="Seg_1389" n="e" s="T132">ɨlagɨn </ts>
               <ts e="T134" id="Seg_1391" n="e" s="T133">da, </ts>
               <ts e="T146" id="Seg_1393" n="e" s="T134">i večeram možno v semʼ vosemʼ uže, sɨvorotka polučaetsa. </ts>
               <ts e="T329" id="Seg_1395" n="e" s="T146">((…)) </ts>
               <ts e="T147" id="Seg_1397" n="e" s="T329">tɨmnɨː </ts>
               <ts e="T148" id="Seg_1399" n="e" s="T147">hirge </ts>
               <ts e="T149" id="Seg_1401" n="e" s="T148">onton </ts>
               <ts e="T151" id="Seg_1403" n="e" s="T149">ne polučaetsa, </ts>
               <ts e="T152" id="Seg_1405" n="e" s="T151">buhar </ts>
               <ts e="T153" id="Seg_1407" n="e" s="T152">ɨllɨbat </ts>
               <ts e="T155" id="Seg_1409" n="e" s="T153">bu͡o. </ts>
               <ts e="T332" id="Seg_1411" n="e" s="T155">Na </ts>
               <ts e="T156" id="Seg_1413" n="e" s="T332">teplom </ts>
               <ts e="T157" id="Seg_1415" n="e" s="T156">meste </ts>
               <ts e="T158" id="Seg_1417" n="e" s="T157">turu͡ogun </ts>
               <ts e="T159" id="Seg_1419" n="e" s="T158">naːda, </ts>
               <ts e="T160" id="Seg_1421" n="e" s="T159">kaːnɨŋ </ts>
               <ts e="T161" id="Seg_1423" n="e" s="T160">ke, </ts>
               <ts e="T162" id="Seg_1425" n="e" s="T161">štob </ts>
               <ts e="T163" id="Seg_1427" n="e" s="T162">sɨvorotkalaːnɨ͡agɨn </ts>
               <ts e="T165" id="Seg_1429" n="e" s="T163">ke. </ts>
               <ts e="T166" id="Seg_1431" n="e" s="T165">A </ts>
               <ts e="T167" id="Seg_1433" n="e" s="T166">sɨvorotka </ts>
               <ts e="T168" id="Seg_1435" n="e" s="T167">komullar </ts>
               <ts e="T169" id="Seg_1437" n="e" s="T168">i </ts>
               <ts e="T170" id="Seg_1439" n="e" s="T169">onu </ts>
               <ts e="T171" id="Seg_1441" n="e" s="T170">možno </ts>
               <ts e="T172" id="Seg_1443" n="e" s="T171">onu </ts>
               <ts e="T173" id="Seg_1445" n="e" s="T172">dʼe </ts>
               <ts e="T174" id="Seg_1447" n="e" s="T173">ol </ts>
               <ts e="T175" id="Seg_1449" n="e" s="T174">kimni͡egin </ts>
               <ts e="T176" id="Seg_1451" n="e" s="T175">bu͡o, </ts>
               <ts e="T177" id="Seg_1453" n="e" s="T176">poka </ts>
               <ts e="T178" id="Seg_1455" n="e" s="T177">sɨvorotka </ts>
               <ts e="T179" id="Seg_1457" n="e" s="T178">ontuŋ </ts>
               <ts e="T180" id="Seg_1459" n="e" s="T179">iti </ts>
               <ts e="T181" id="Seg_1461" n="e" s="T180">ol </ts>
               <ts e="T182" id="Seg_1463" n="e" s="T181">homorso </ts>
               <ts e="T183" id="Seg_1465" n="e" s="T182">ihiger </ts>
               <ts e="T184" id="Seg_1467" n="e" s="T183">uːragɨn </ts>
               <ts e="T185" id="Seg_1469" n="e" s="T184">bu͡o </ts>
               <ts e="T186" id="Seg_1471" n="e" s="T185">kutagɨn </ts>
               <ts e="T187" id="Seg_1473" n="e" s="T186">bu͡o. </ts>
               <ts e="T188" id="Seg_1475" n="e" s="T187">Baːjagɨn </ts>
               <ts e="T189" id="Seg_1477" n="e" s="T188">uːragɨn </ts>
               <ts e="T190" id="Seg_1479" n="e" s="T189">onton </ts>
               <ts e="T191" id="Seg_1481" n="e" s="T190">emi͡e </ts>
               <ts e="T192" id="Seg_1483" n="e" s="T191">kutuja </ts>
               <ts e="T193" id="Seg_1485" n="e" s="T192">baːr </ts>
               <ts e="T194" id="Seg_1487" n="e" s="T193">kutujalaːk </ts>
               <ts e="T195" id="Seg_1489" n="e" s="T194">vnutrennosti </ts>
               <ts e="T196" id="Seg_1491" n="e" s="T195">kutuja. </ts>
               <ts e="T197" id="Seg_1493" n="e" s="T196">Kutuja </ts>
               <ts e="T198" id="Seg_1495" n="e" s="T197">ol </ts>
               <ts e="T199" id="Seg_1497" n="e" s="T198">ihiger </ts>
               <ts e="T200" id="Seg_1499" n="e" s="T199">emi͡e </ts>
               <ts e="T201" id="Seg_1501" n="e" s="T200">možno </ts>
               <ts e="T202" id="Seg_1503" n="e" s="T201">kutu͡okkun </ts>
               <ts e="T203" id="Seg_1505" n="e" s="T202">kaːnɨ. </ts>
               <ts e="T204" id="Seg_1507" n="e" s="T203">Sɨvorotka </ts>
               <ts e="T205" id="Seg_1509" n="e" s="T204">onno </ts>
               <ts e="T206" id="Seg_1511" n="e" s="T205">kim </ts>
               <ts e="T207" id="Seg_1513" n="e" s="T206">bu͡olaːččɨ, </ts>
               <ts e="T208" id="Seg_1515" n="e" s="T207">agɨjak </ts>
               <ts e="T209" id="Seg_1517" n="e" s="T208">sɨvorotka, </ts>
               <ts e="T210" id="Seg_1519" n="e" s="T209">nemnožko. </ts>
               <ts e="T212" id="Seg_1521" n="e" s="T210">Kolbasa polučaetsa malo, </ts>
               <ts e="T213" id="Seg_1523" n="e" s="T212">eto </ts>
               <ts e="T214" id="Seg_1525" n="e" s="T213">elbek </ts>
               <ts e="T215" id="Seg_1527" n="e" s="T214">tabanɨ </ts>
               <ts e="T216" id="Seg_1529" n="e" s="T215">ölördökküne </ts>
               <ts e="T217" id="Seg_1531" n="e" s="T216">sɨvorotka </ts>
               <ts e="T218" id="Seg_1533" n="e" s="T217">mnogo </ts>
               <ts e="T219" id="Seg_1535" n="e" s="T218">bu͡olar, </ts>
               <ts e="T220" id="Seg_1537" n="e" s="T219">elbek </ts>
               <ts e="T221" id="Seg_1539" n="e" s="T220">bu͡olar </ts>
               <ts e="T222" id="Seg_1541" n="e" s="T221">bu͡o, </ts>
               <ts e="T223" id="Seg_1543" n="e" s="T222">oččogo </ts>
               <ts e="T224" id="Seg_1545" n="e" s="T223">onu </ts>
               <ts e="T225" id="Seg_1547" n="e" s="T224">bu͡o </ts>
               <ts e="T226" id="Seg_1549" n="e" s="T225">elbek </ts>
               <ts e="T227" id="Seg_1551" n="e" s="T226">kolbasa </ts>
               <ts e="T229" id="Seg_1553" n="e" s="T227">bu͡olar. </ts>
               <ts e="T230" id="Seg_1555" n="e" s="T229">A to iz odnogo oleni ((…)) tam </ts>
               <ts e="T231" id="Seg_1557" n="e" s="T230">homorsotugar </ts>
               <ts e="T232" id="Seg_1559" n="e" s="T231">kutagɨn </ts>
               <ts e="T233" id="Seg_1561" n="e" s="T232">hu͡on </ts>
               <ts e="T234" id="Seg_1563" n="e" s="T233">hu͡on </ts>
               <ts e="T235" id="Seg_1565" n="e" s="T234">hu͡on </ts>
               <ts e="T236" id="Seg_1567" n="e" s="T235">haːgɨgar </ts>
               <ts e="T237" id="Seg_1569" n="e" s="T236">kutagɨn, </ts>
               <ts e="T238" id="Seg_1571" n="e" s="T237">bɨːčagɨgar </ts>
               <ts e="T239" id="Seg_1573" n="e" s="T238">ke. </ts>
               <ts e="T334" id="Seg_1575" n="e" s="T239">Eto </ts>
               <ts e="T240" id="Seg_1577" n="e" s="T334">kiska </ts>
               <ts e="T241" id="Seg_1579" n="e" s="T240">naverno, </ts>
               <ts e="T243" id="Seg_1581" n="e" s="T241">bɨːčas. </ts>
               <ts e="T244" id="Seg_1583" n="e" s="T243">Onnugu </ts>
               <ts e="T245" id="Seg_1585" n="e" s="T244">kutagɨn </ts>
               <ts e="T246" id="Seg_1587" n="e" s="T245">bu͡o </ts>
               <ts e="T247" id="Seg_1589" n="e" s="T246">ontuŋ </ts>
               <ts e="T248" id="Seg_1591" n="e" s="T247">kolbasa </ts>
               <ts e="T249" id="Seg_1593" n="e" s="T248">bu͡olar, </ts>
               <ts e="T250" id="Seg_1595" n="e" s="T249">minnʼiges </ts>
               <ts e="T251" id="Seg_1597" n="e" s="T250">bagajɨ. </ts>
               <ts e="T252" id="Seg_1599" n="e" s="T251">Da? </ts>
               <ts e="T253" id="Seg_1601" n="e" s="T252">Vkusnaja. </ts>
               <ts e="T259" id="Seg_1603" n="e" s="T253">A sto vɨ ljubite bolshe vsevo? </ts>
               <ts e="T260" id="Seg_1605" n="e" s="T259">Ja? </ts>
               <ts e="T264" id="Seg_1607" n="e" s="T260">Ja ljublju, etu kalbasu ljublju, </ts>
               <ts e="T266" id="Seg_1609" n="e" s="T264">voobshe ljublju </ts>
               <ts e="T122" id="Seg_1611" n="e" s="T266">tɨl, </ts>
               <ts e="T130" id="Seg_1613" n="e" s="T122">fkusnɨj. </ts>
            </ts>
            <ts e="T328" id="Seg_1614" n="sc" s="T268">
               <ts e="T269" id="Seg_1616" n="e" s="T268">I </ts>
               <ts e="T270" id="Seg_1618" n="e" s="T269">öttükke </ts>
               <ts e="T271" id="Seg_1620" n="e" s="T270">bu͡olaːččɨ </ts>
               <ts e="T272" id="Seg_1622" n="e" s="T271">minnʼiges </ts>
               <ts e="T273" id="Seg_1624" n="e" s="T272">hɨ͡a, </ts>
               <ts e="T274" id="Seg_1626" n="e" s="T273">hɨ͡alaːk </ts>
               <ts e="T275" id="Seg_1628" n="e" s="T274">öttükke </ts>
               <ts e="T276" id="Seg_1630" n="e" s="T275">ke. </ts>
               <ts e="T277" id="Seg_1632" n="e" s="T276">Hɨ͡a </ts>
               <ts e="T278" id="Seg_1634" n="e" s="T277">bu͡olaːččɨ </ts>
               <ts e="T279" id="Seg_1636" n="e" s="T278">öttükke. </ts>
               <ts e="T280" id="Seg_1638" n="e" s="T279">Onnuk </ts>
               <ts e="T281" id="Seg_1640" n="e" s="T280">minnʼiges </ts>
               <ts e="T282" id="Seg_1642" n="e" s="T281">vkusnɨj. </ts>
               <ts e="T290" id="Seg_1644" n="e" s="T282">Ja v asnavnom damashnij, ot dikogo olenja mjasa voobshe </ts>
               <ts e="T291" id="Seg_1646" n="e" s="T290">kimniːller </ts>
               <ts e="T294" id="Seg_1648" n="e" s="T291">različajutsa. </ts>
               <ts e="T295" id="Seg_1650" n="e" s="T294">Da? </ts>
               <ts e="T296" id="Seg_1652" n="e" s="T295">Da. </ts>
               <ts e="T297" id="Seg_1654" n="e" s="T296">A </ts>
               <ts e="T298" id="Seg_1656" n="e" s="T297">shto </ts>
               <ts e="T299" id="Seg_1658" n="e" s="T298">vkusnee? </ts>
               <ts e="T300" id="Seg_1660" n="e" s="T299">Mne </ts>
               <ts e="T301" id="Seg_1662" n="e" s="T300">kazhetsja </ts>
               <ts e="T302" id="Seg_1664" n="e" s="T301"> vkusnee </ts>
               <ts e="T303" id="Seg_1666" n="e" s="T302">dikogo, </ts>
               <ts e="T304" id="Seg_1668" n="e" s="T303">dikij. </ts>
               <ts e="T305" id="Seg_1670" n="e" s="T304">Kɨːlɨn </ts>
               <ts e="T306" id="Seg_1672" n="e" s="T305">kɨːlɨn </ts>
               <ts e="T307" id="Seg_1674" n="e" s="T306">ete </ts>
               <ts e="T308" id="Seg_1676" n="e" s="T307">minnʼiges </ts>
               <ts e="T309" id="Seg_1678" n="e" s="T308">bu͡o, </ts>
               <ts e="T310" id="Seg_1680" n="e" s="T309">čem </ts>
               <ts e="T311" id="Seg_1682" n="e" s="T310">damashnij. </ts>
               <ts e="T315" id="Seg_1684" n="e" s="T311">To li mɨ sami ne, </ts>
               <ts e="T316" id="Seg_1686" n="e" s="T315">bejebitiger </ts>
               <ts e="T317" id="Seg_1688" n="e" s="T316">taba </ts>
               <ts e="T318" id="Seg_1690" n="e" s="T317">etin </ts>
               <ts e="T319" id="Seg_1692" n="e" s="T318">olus </ts>
               <ts e="T320" id="Seg_1694" n="e" s="T319">oččoloːččuta </ts>
               <ts e="T321" id="Seg_1696" n="e" s="T320">hu͡okpun </ts>
               <ts e="T322" id="Seg_1698" n="e" s="T321">kɨːlɨn </ts>
               <ts e="T323" id="Seg_1700" n="e" s="T322">ete </ts>
               <ts e="T324" id="Seg_1702" n="e" s="T323">vkusnee </ts>
               <ts e="T325" id="Seg_1704" n="e" s="T324">taba </ts>
               <ts e="T327" id="Seg_1706" n="e" s="T325">etineːger. </ts>
               <ts e="T328" id="Seg_1708" n="e" s="T327">Minnʼiges. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_1709" s="T0">AnIM_2009_Sausage_nar.001 (001.001)</ta>
            <ta e="T19" id="Seg_1710" s="T7">AnIM_2009_Sausage_nar.002 (001.002)</ta>
            <ta e="T28" id="Seg_1711" s="T19">AnIM_2009_Sausage_nar.003 (001.003)</ta>
            <ta e="T36" id="Seg_1712" s="T28">AnIM_2009_Sausage_nar.004 (001.004)</ta>
            <ta e="T50" id="Seg_1713" s="T36">AnIM_2009_Sausage_nar.005 (001.005)</ta>
            <ta e="T60" id="Seg_1714" s="T50">AnIM_2009_Sausage_nar.006 (001.006)</ta>
            <ta e="T81" id="Seg_1715" s="T60">AnIM_2009_Sausage_nar.007 (001.007)</ta>
            <ta e="T96" id="Seg_1716" s="T81">AnIM_2009_Sausage_nar.008 (001.008)</ta>
            <ta e="T99" id="Seg_1717" s="T96">AnIM_2009_Sausage_nar.009 (001.009)</ta>
            <ta e="T105" id="Seg_1718" s="T99">AnIM_2009_Sausage_nar.010 (001.010)</ta>
            <ta e="T110" id="Seg_1719" s="T105">AnIM_2009_Sausage_nar.011 (001.011)</ta>
            <ta e="T112" id="Seg_1720" s="T110">AnIM_2009_Sausage_nar.012 (001.012)</ta>
            <ta e="T123" id="Seg_1721" s="T112">AnIM_2009_Sausage_nar.013 (001.012)</ta>
            <ta e="T146" id="Seg_1722" s="T123">AnIM_2009_Sausage_nar.014 (001.013)</ta>
            <ta e="T155" id="Seg_1723" s="T146">AnIM_2009_Sausage_nar.015 (001.014)</ta>
            <ta e="T165" id="Seg_1724" s="T155">AnIM_2009_Sausage_nar.016 (001.015)</ta>
            <ta e="T187" id="Seg_1725" s="T165">AnIM_2009_Sausage_nar.017 (001.016)</ta>
            <ta e="T196" id="Seg_1726" s="T187">AnIM_2009_Sausage_nar.018 (001.017)</ta>
            <ta e="T203" id="Seg_1727" s="T196">AnIM_2009_Sausage_nar.019 (001.018)</ta>
            <ta e="T210" id="Seg_1728" s="T203">AnIM_2009_Sausage_nar.020 (001.019)</ta>
            <ta e="T229" id="Seg_1729" s="T210">AnIM_2009_Sausage_nar.021 (001.020)</ta>
            <ta e="T239" id="Seg_1730" s="T229">AnIM_2009_Sausage_nar.022 (001.021)</ta>
            <ta e="T243" id="Seg_1731" s="T239">AnIM_2009_Sausage_nar.023 (001.022)</ta>
            <ta e="T251" id="Seg_1732" s="T243">AnIM_2009_Sausage_nar.024 (001.023)</ta>
            <ta e="T252" id="Seg_1733" s="T251">AnIM_2009_Sausage_nar.025 (001.024)</ta>
            <ta e="T253" id="Seg_1734" s="T252">AnIM_2009_Sausage_nar.026 (001.025)</ta>
            <ta e="T259" id="Seg_1735" s="T253">AnIM_2009_Sausage_nar.027 (001.026)</ta>
            <ta e="T260" id="Seg_1736" s="T259">AnIM_2009_Sausage_nar.028 (001.027)</ta>
            <ta e="T130" id="Seg_1737" s="T260">AnIM_2009_Sausage_nar.029 (001.028)</ta>
            <ta e="T276" id="Seg_1738" s="T268">AnIM_2009_Sausage_nar.030 (001.029)</ta>
            <ta e="T279" id="Seg_1739" s="T276">AnIM_2009_Sausage_nar.031 (001.030)</ta>
            <ta e="T282" id="Seg_1740" s="T279">AnIM_2009_Sausage_nar.032 (001.031)</ta>
            <ta e="T294" id="Seg_1741" s="T282">AnIM_2009_Sausage_nar.033 (001.032)</ta>
            <ta e="T295" id="Seg_1742" s="T294">AnIM_2009_Sausage_nar.034 (001.033)</ta>
            <ta e="T296" id="Seg_1743" s="T295">AnIM_2009_Sausage_nar.035 (001.034)</ta>
            <ta e="T299" id="Seg_1744" s="T296">AnIM_2009_Sausage_nar.036 (001.035)</ta>
            <ta e="T304" id="Seg_1745" s="T299">AnIM_2009_Sausage_nar.037 (001.036)</ta>
            <ta e="T311" id="Seg_1746" s="T304">AnIM_2009_Sausage_nar.038 (001.037)</ta>
            <ta e="T327" id="Seg_1747" s="T311">AnIM_2009_Sausage_nar.039 (001.038)</ta>
            <ta e="T328" id="Seg_1748" s="T327">AnIM_2009_Sausage_nar.040 (001.039)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_1749" s="T0">Taba kaːnɨnan bu͡ollagɨnan kimi oŋorobut, kolbasa ke. </ta>
            <ta e="T19" id="Seg_1750" s="T7">Iti bu͡ollagɨna kim tabanɨ ölöröllör bu͡o innʼe gɨnan baran tazikka kaːn ɨlabɨt. </ta>
            <ta e="T28" id="Seg_1751" s="T19">I onu bu͡ollagɨna turu͡orabɨt bu͡o štobɨ sɨvorotka bu͡olu͡ogun ke. </ta>
            <ta e="T36" id="Seg_1752" s="T28">Sɨvorotkanɨ kimniː mʼattɨːbɨt mjattɨːbɨt sɨvorotka sɨvorotka bu͡olar ontuŋ. </ta>
            <ta e="T50" id="Seg_1753" s="T36">I onu bu͡ollagɨna ihigineːgileri ɨlabɨt vot eto vnutrennosti onton kimi hu͡on haːga, est tam. </ta>
            <ta e="T60" id="Seg_1754" s="T50">Hu͡on haːgɨn ɨlagɨn bu͡o homorsotun ɨlagɨn bu͡o ačatɨttan emi͡e ɨlagɨn. </ta>
            <ta e="T81" id="Seg_1755" s="T60">Innʼe barɨkaːn huːjan huːjan kannʼan baran huːjattɨːgɨn, innʼe gɨnan baran kaːnɨŋ bu͡ollagɨna, sɨvorotka polučaetsa kak budto prozračnaj bagajɨ, uː kaːn bu͡olar. </ta>
            <ta e="T96" id="Seg_1756" s="T81">I onu bu͡ollagɨna kimi͡eke kutagɨn ol homorsolorga kutagɨn bɨːčastarga kutagɨn baːjagɨn i kim kimi͡eke buharagɨn. </ta>
            <ta e="T99" id="Seg_1757" s="T96">Itiː uːga uːragɨn. </ta>
            <ta e="T105" id="Seg_1758" s="T99">I minut pjat desjat buhar ontuŋ. </ta>
            <ta e="T110" id="Seg_1759" s="T105">I oruːgun i kolbasa bu͡olar. </ta>
            <ta e="T112" id="Seg_1760" s="T110">[SE] Tak bɨstro, türgennik! </ta>
            <ta e="T123" id="Seg_1761" s="T112">Heee türgen bagaj (…) vot eto že kaːnɨŋ uhunnuk turu͡ogun naːda. </ta>
            <ta e="T146" id="Seg_1762" s="T123">Celɨj denʼ počti, utrom naprimer v odinnadtsatʼ kaːn ɨlagɨn da, i večeram možno v semʼ vosemʼ uže, sɨvorotka polučaetsa. </ta>
            <ta e="T155" id="Seg_1763" s="T146">((…)) tɨmnɨː hirge onton ne polučaetsa, buhar ɨllɨbat bu͡o. </ta>
            <ta e="T165" id="Seg_1764" s="T155">Na teplom meste turu͡ogun naːda, kaːnɨŋ ke, štob sɨvorotkalaːnɨ͡agɨn ke. </ta>
            <ta e="T187" id="Seg_1765" s="T165">A sɨvorotka komullar i onu možno onu dʼe ol kimni͡egin bu͡o, poka sɨvorotka ontuŋ iti ol homorso ihiger uːragɨn bu͡o kutagɨn bu͡o. </ta>
            <ta e="T196" id="Seg_1766" s="T187">Baːjagɨn uːragɨn onton emi͡e kutuja baːr kutujalaːk vnutrennosti kutuja. </ta>
            <ta e="T203" id="Seg_1767" s="T196">Kutuja ol ihiger emi͡e možno kutu͡okkun kaːnɨ. </ta>
            <ta e="T210" id="Seg_1768" s="T203">Sɨvorotka onno kim bu͡olaːččɨ, agɨjak sɨvorotka, nemnožko. </ta>
            <ta e="T229" id="Seg_1769" s="T210">Kolbasa polučaetsa malo, eto elbek tabanɨ ölördökküne sɨvorotka mnogo bu͡olar, elbek bu͡olar bu͡o, oččogo onu bu͡o elbek kolbasa bu͡olar. </ta>
            <ta e="T239" id="Seg_1770" s="T229">A to iz odnogo oleni ((…)) tam homorsotugar kutagɨn hu͡on hu͡on hu͡on haːgɨgar kutagɨn, bɨːčagɨgar ke. </ta>
            <ta e="T243" id="Seg_1771" s="T239">Eto kiska naverno, bɨːčas. </ta>
            <ta e="T251" id="Seg_1772" s="T243">Onnugu kutagɨn bu͡o ontuŋ kolbasa bu͡olar, minnʼiges bagajɨ. </ta>
            <ta e="T252" id="Seg_1773" s="T251">[SE] Da? </ta>
            <ta e="T253" id="Seg_1774" s="T252">Vkusnaja. </ta>
            <ta e="T259" id="Seg_1775" s="T253">[SE] A sto vɨ ljubite bolshe vsevo? </ta>
            <ta e="T260" id="Seg_1776" s="T259">Ja? </ta>
            <ta e="T130" id="Seg_1777" s="T260">Ja ljublju, etu kalbasu ljublju voobshe ljublju tɨl. </ta>
            <ta e="T276" id="Seg_1778" s="T268">I öttükke bu͡olaːččɨ minnʼiges hɨ͡a, hɨ͡alaːk öttükke ke. </ta>
            <ta e="T279" id="Seg_1779" s="T276">Hɨ͡a bu͡olaːččɨ öttükke. </ta>
            <ta e="T282" id="Seg_1780" s="T279">Onnuk minnʼiges vkusnɨj. </ta>
            <ta e="T294" id="Seg_1781" s="T282">Ja v asnavnom damashnij, ot dikogo olenja mjasa voobshe kimniːller različajutsa. </ta>
            <ta e="T295" id="Seg_1782" s="T294">[SE] Da? </ta>
            <ta e="T296" id="Seg_1783" s="T295">Da. </ta>
            <ta e="T299" id="Seg_1784" s="T296">[SE] A shto vkusnee? </ta>
            <ta e="T304" id="Seg_1785" s="T299">Mne kazhetsja vkusnee dikogo, dikij. </ta>
            <ta e="T311" id="Seg_1786" s="T304">kɨːlɨn kɨːlɨn ete minnʼiges bu͡o, čem damashnij. </ta>
            <ta e="T327" id="Seg_1787" s="T311">To li mɨ sami ne, bejebitiger taba etin olus oččoloːččuta hu͡okpun kɨːlɨn ete vkusnee taba etineːger. </ta>
            <ta e="T328" id="Seg_1788" s="T327">Minnʼiges. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1789" s="T0">taba</ta>
            <ta e="T2" id="Seg_1790" s="T1">kaːn-ɨ-nan</ta>
            <ta e="T3" id="Seg_1791" s="T2">bu͡ollagɨnan</ta>
            <ta e="T4" id="Seg_1792" s="T3">kim-i</ta>
            <ta e="T5" id="Seg_1793" s="T4">oŋor-o-but</ta>
            <ta e="T6" id="Seg_1794" s="T5">kolbasa</ta>
            <ta e="T7" id="Seg_1795" s="T6">ke</ta>
            <ta e="T8" id="Seg_1796" s="T7">iti</ta>
            <ta e="T9" id="Seg_1797" s="T8">bu͡ollagɨna</ta>
            <ta e="T10" id="Seg_1798" s="T9">kim</ta>
            <ta e="T11" id="Seg_1799" s="T10">taba-nɨ</ta>
            <ta e="T12" id="Seg_1800" s="T11">ölör-öl-lör</ta>
            <ta e="T13" id="Seg_1801" s="T12">bu͡o</ta>
            <ta e="T14" id="Seg_1802" s="T13">innʼe</ta>
            <ta e="T15" id="Seg_1803" s="T14">gɨn-an</ta>
            <ta e="T16" id="Seg_1804" s="T15">bar-an</ta>
            <ta e="T17" id="Seg_1805" s="T16">tazik-ka</ta>
            <ta e="T18" id="Seg_1806" s="T17">kaːn</ta>
            <ta e="T19" id="Seg_1807" s="T18">ɨl-a-bɨt</ta>
            <ta e="T20" id="Seg_1808" s="T19">i</ta>
            <ta e="T21" id="Seg_1809" s="T20">o-nu</ta>
            <ta e="T22" id="Seg_1810" s="T21">bu͡ollagɨna</ta>
            <ta e="T23" id="Seg_1811" s="T22">tur-u͡or-a-bɨt</ta>
            <ta e="T24" id="Seg_1812" s="T23">bu͡o</ta>
            <ta e="T25" id="Seg_1813" s="T24">štobɨ</ta>
            <ta e="T26" id="Seg_1814" s="T25">sɨvorotka</ta>
            <ta e="T27" id="Seg_1815" s="T26">bu͡ol-u͡og-u-n</ta>
            <ta e="T28" id="Seg_1816" s="T27">ke</ta>
            <ta e="T29" id="Seg_1817" s="T28">sɨvorotka-nɨ</ta>
            <ta e="T30" id="Seg_1818" s="T29">kim-n-iː</ta>
            <ta e="T31" id="Seg_1819" s="T30">mʼat-t-ɨː-bɨt</ta>
            <ta e="T32" id="Seg_1820" s="T31">mʼat-t-ɨː-bɨt</ta>
            <ta e="T33" id="Seg_1821" s="T32">sɨvorotka</ta>
            <ta e="T34" id="Seg_1822" s="T33">sɨvorotka</ta>
            <ta e="T35" id="Seg_1823" s="T34">bu͡ol-ar</ta>
            <ta e="T36" id="Seg_1824" s="T35">on-tu-ŋ</ta>
            <ta e="T37" id="Seg_1825" s="T36">i</ta>
            <ta e="T38" id="Seg_1826" s="T37">o-nu</ta>
            <ta e="T39" id="Seg_1827" s="T38">bu͡ollagɨna</ta>
            <ta e="T40" id="Seg_1828" s="T39">ihigineːgi-ler-i</ta>
            <ta e="T41" id="Seg_1829" s="T40">ɨl-a-bɨt</ta>
            <ta e="T45" id="Seg_1830" s="T44">onton</ta>
            <ta e="T46" id="Seg_1831" s="T45">kim-i</ta>
            <ta e="T47" id="Seg_1832" s="T46">hu͡on</ta>
            <ta e="T48" id="Seg_1833" s="T47">haːg-a</ta>
            <ta e="T51" id="Seg_1834" s="T50">hu͡on</ta>
            <ta e="T52" id="Seg_1835" s="T51">haːg-ɨ-n</ta>
            <ta e="T53" id="Seg_1836" s="T52">ɨl-a-gɨn</ta>
            <ta e="T54" id="Seg_1837" s="T53">bu͡o</ta>
            <ta e="T55" id="Seg_1838" s="T54">homorso-tu-n</ta>
            <ta e="T56" id="Seg_1839" s="T55">ɨl-a-gɨn</ta>
            <ta e="T57" id="Seg_1840" s="T56">bu͡o</ta>
            <ta e="T58" id="Seg_1841" s="T57">ača-tɨ-ttan</ta>
            <ta e="T59" id="Seg_1842" s="T58">emi͡e</ta>
            <ta e="T60" id="Seg_1843" s="T59">ɨl-a-gɨn</ta>
            <ta e="T61" id="Seg_1844" s="T60">innʼe</ta>
            <ta e="T62" id="Seg_1845" s="T61">barɨ-kaːn</ta>
            <ta e="T63" id="Seg_1846" s="T62">huːj-an</ta>
            <ta e="T64" id="Seg_1847" s="T63">huːj-an</ta>
            <ta e="T65" id="Seg_1848" s="T64">kanʼaː-n</ta>
            <ta e="T66" id="Seg_1849" s="T65">bar-an</ta>
            <ta e="T67" id="Seg_1850" s="T66">huːj-att-ɨː-gɨn</ta>
            <ta e="T68" id="Seg_1851" s="T67">innʼe</ta>
            <ta e="T69" id="Seg_1852" s="T68">gɨn-an</ta>
            <ta e="T70" id="Seg_1853" s="T69">bar-an</ta>
            <ta e="T71" id="Seg_1854" s="T70">kaːn-ɨ-ŋ</ta>
            <ta e="T72" id="Seg_1855" s="T71">bu͡ollagɨna</ta>
            <ta e="T73" id="Seg_1856" s="T72">sɨvorotka</ta>
            <ta e="T78" id="Seg_1857" s="T77">bagajɨ</ta>
            <ta e="T79" id="Seg_1858" s="T78">uː</ta>
            <ta e="T80" id="Seg_1859" s="T79">kaːn</ta>
            <ta e="T81" id="Seg_1860" s="T80">bu͡ol-ar</ta>
            <ta e="T82" id="Seg_1861" s="T81">i</ta>
            <ta e="T83" id="Seg_1862" s="T82">o-nu</ta>
            <ta e="T84" id="Seg_1863" s="T83">bu͡ollagɨna</ta>
            <ta e="T85" id="Seg_1864" s="T84">kimi͡e-ke</ta>
            <ta e="T86" id="Seg_1865" s="T85">kut-a-gɨn</ta>
            <ta e="T87" id="Seg_1866" s="T86">ol</ta>
            <ta e="T88" id="Seg_1867" s="T87">homorso-lor-ga</ta>
            <ta e="T89" id="Seg_1868" s="T88">kut-a-gɨn</ta>
            <ta e="T90" id="Seg_1869" s="T89">bɨːčas-tar-ga</ta>
            <ta e="T91" id="Seg_1870" s="T90">kut-a-gɨn</ta>
            <ta e="T92" id="Seg_1871" s="T91">baːj-a-gɨn</ta>
            <ta e="T93" id="Seg_1872" s="T92">i</ta>
            <ta e="T94" id="Seg_1873" s="T93">kim</ta>
            <ta e="T95" id="Seg_1874" s="T94">kimi͡e-ke</ta>
            <ta e="T96" id="Seg_1875" s="T95">buh-ar-a-gɨn</ta>
            <ta e="T97" id="Seg_1876" s="T96">itiː</ta>
            <ta e="T98" id="Seg_1877" s="T97">uː-ga</ta>
            <ta e="T99" id="Seg_1878" s="T98">uːr-a-gɨn</ta>
            <ta e="T100" id="Seg_1879" s="T99">i</ta>
            <ta e="T104" id="Seg_1880" s="T103">buh-ar</ta>
            <ta e="T105" id="Seg_1881" s="T104">on-tu-ŋ</ta>
            <ta e="T106" id="Seg_1882" s="T105">i</ta>
            <ta e="T107" id="Seg_1883" s="T106">or-uː-gun</ta>
            <ta e="T108" id="Seg_1884" s="T107">i</ta>
            <ta e="T109" id="Seg_1885" s="T108">kolbasa</ta>
            <ta e="T110" id="Seg_1886" s="T109">bu͡ol-ar</ta>
            <ta e="T112" id="Seg_1887" s="T111">türgen-nik</ta>
            <ta e="T113" id="Seg_1888" s="T112">heː</ta>
            <ta e="T114" id="Seg_1889" s="T113">türgen</ta>
            <ta e="T115" id="Seg_1890" s="T114">bagaj</ta>
            <ta e="T116" id="Seg_1891" s="T115">vot</ta>
            <ta e="T117" id="Seg_1892" s="T116">eto</ta>
            <ta e="T118" id="Seg_1893" s="T117">že</ta>
            <ta e="T119" id="Seg_1894" s="T118">kaːn-ɨ-ŋ</ta>
            <ta e="T120" id="Seg_1895" s="T119">uhun-nuk</ta>
            <ta e="T121" id="Seg_1896" s="T120">tur-u͡og-u-n</ta>
            <ta e="T123" id="Seg_1897" s="T121">naːda</ta>
            <ta e="T132" id="Seg_1898" s="T131">kaːn</ta>
            <ta e="T133" id="Seg_1899" s="T132">ɨl-a-gɨn</ta>
            <ta e="T134" id="Seg_1900" s="T133">da</ta>
            <ta e="T147" id="Seg_1901" s="T329">tɨmnɨː</ta>
            <ta e="T148" id="Seg_1902" s="T147">hir-ge</ta>
            <ta e="T149" id="Seg_1903" s="T148">onton</ta>
            <ta e="T152" id="Seg_1904" s="T151">buh-ar</ta>
            <ta e="T153" id="Seg_1905" s="T152">ɨl-l-ɨ-bat</ta>
            <ta e="T155" id="Seg_1906" s="T153">bu͡o</ta>
            <ta e="T158" id="Seg_1907" s="T157">tur-u͡og-u-n</ta>
            <ta e="T159" id="Seg_1908" s="T158">naːda</ta>
            <ta e="T160" id="Seg_1909" s="T159">kaːn-ɨ-ŋ</ta>
            <ta e="T161" id="Seg_1910" s="T160">ke</ta>
            <ta e="T162" id="Seg_1911" s="T161">štob</ta>
            <ta e="T163" id="Seg_1912" s="T162">sɨvorotka-laː-n-ɨ͡ag-ɨ-n</ta>
            <ta e="T165" id="Seg_1913" s="T163">ke</ta>
            <ta e="T166" id="Seg_1914" s="T165">a</ta>
            <ta e="T167" id="Seg_1915" s="T166">sɨvorotka</ta>
            <ta e="T168" id="Seg_1916" s="T167">komu-ll-ar</ta>
            <ta e="T169" id="Seg_1917" s="T168">i</ta>
            <ta e="T170" id="Seg_1918" s="T169">o-nu</ta>
            <ta e="T171" id="Seg_1919" s="T170">možno</ta>
            <ta e="T172" id="Seg_1920" s="T171">o-nu</ta>
            <ta e="T173" id="Seg_1921" s="T172">dʼe</ta>
            <ta e="T174" id="Seg_1922" s="T173">ol</ta>
            <ta e="T175" id="Seg_1923" s="T174">kim-n-i͡eg-i-n</ta>
            <ta e="T176" id="Seg_1924" s="T175">bu͡o</ta>
            <ta e="T177" id="Seg_1925" s="T176">poka</ta>
            <ta e="T178" id="Seg_1926" s="T177">sɨvorotka</ta>
            <ta e="T179" id="Seg_1927" s="T178">on-tu-ŋ</ta>
            <ta e="T180" id="Seg_1928" s="T179">iti</ta>
            <ta e="T181" id="Seg_1929" s="T180">ol</ta>
            <ta e="T182" id="Seg_1930" s="T181">homorso</ta>
            <ta e="T183" id="Seg_1931" s="T182">ih-i-ger</ta>
            <ta e="T184" id="Seg_1932" s="T183">uːr-a-gɨn</ta>
            <ta e="T185" id="Seg_1933" s="T184">bu͡o</ta>
            <ta e="T186" id="Seg_1934" s="T185">kut-a-gɨn</ta>
            <ta e="T187" id="Seg_1935" s="T186">bu͡o</ta>
            <ta e="T188" id="Seg_1936" s="T187">baːj-a-gɨn</ta>
            <ta e="T189" id="Seg_1937" s="T188">uːr-a-gɨn</ta>
            <ta e="T190" id="Seg_1938" s="T189">onton</ta>
            <ta e="T191" id="Seg_1939" s="T190">emi͡e</ta>
            <ta e="T192" id="Seg_1940" s="T191">kutuja</ta>
            <ta e="T193" id="Seg_1941" s="T192">baːr</ta>
            <ta e="T194" id="Seg_1942" s="T193">kutuja-laːk</ta>
            <ta e="T196" id="Seg_1943" s="T195">kutuja</ta>
            <ta e="T197" id="Seg_1944" s="T196">kutuja</ta>
            <ta e="T198" id="Seg_1945" s="T197">ol</ta>
            <ta e="T199" id="Seg_1946" s="T198">ih-i-ger</ta>
            <ta e="T200" id="Seg_1947" s="T199">emi͡e</ta>
            <ta e="T201" id="Seg_1948" s="T200">možno</ta>
            <ta e="T202" id="Seg_1949" s="T201">kut-u͡ok-ku-n</ta>
            <ta e="T203" id="Seg_1950" s="T202">kaːn-ɨ</ta>
            <ta e="T204" id="Seg_1951" s="T203">sɨvorotka</ta>
            <ta e="T205" id="Seg_1952" s="T204">onno</ta>
            <ta e="T206" id="Seg_1953" s="T205">kim</ta>
            <ta e="T207" id="Seg_1954" s="T206">bu͡ol-aːččɨ</ta>
            <ta e="T208" id="Seg_1955" s="T207">agɨjak</ta>
            <ta e="T209" id="Seg_1956" s="T208">sɨvorotka</ta>
            <ta e="T210" id="Seg_1957" s="T209">nemnožko</ta>
            <ta e="T213" id="Seg_1958" s="T212">eto</ta>
            <ta e="T214" id="Seg_1959" s="T213">elbek</ta>
            <ta e="T215" id="Seg_1960" s="T214">taba-nɨ</ta>
            <ta e="T216" id="Seg_1961" s="T215">ölör-dök-küne</ta>
            <ta e="T217" id="Seg_1962" s="T216">sɨvorotka</ta>
            <ta e="T218" id="Seg_1963" s="T217">mnogo</ta>
            <ta e="T219" id="Seg_1964" s="T218">bu͡ol-ar</ta>
            <ta e="T220" id="Seg_1965" s="T219">elbek</ta>
            <ta e="T221" id="Seg_1966" s="T220">bu͡ol-ar</ta>
            <ta e="T222" id="Seg_1967" s="T221">bu͡o</ta>
            <ta e="T223" id="Seg_1968" s="T222">oččogo</ta>
            <ta e="T224" id="Seg_1969" s="T223">o-nu</ta>
            <ta e="T225" id="Seg_1970" s="T224">bu͡o</ta>
            <ta e="T226" id="Seg_1971" s="T225">elbek</ta>
            <ta e="T227" id="Seg_1972" s="T226">kolbasa</ta>
            <ta e="T229" id="Seg_1973" s="T227">bu͡ol-ar</ta>
            <ta e="T231" id="Seg_1974" s="T230">homorso-tu-gar</ta>
            <ta e="T232" id="Seg_1975" s="T231">kut-a-gɨn</ta>
            <ta e="T233" id="Seg_1976" s="T232">hu͡on</ta>
            <ta e="T234" id="Seg_1977" s="T233">hu͡on</ta>
            <ta e="T235" id="Seg_1978" s="T234">hu͡on</ta>
            <ta e="T236" id="Seg_1979" s="T235">haːg-ɨ-gar</ta>
            <ta e="T237" id="Seg_1980" s="T236">kut-a-gɨn</ta>
            <ta e="T238" id="Seg_1981" s="T237">bɨːčag-ɨ-gar</ta>
            <ta e="T239" id="Seg_1982" s="T238">ke</ta>
            <ta e="T243" id="Seg_1983" s="T241">bɨːčas</ta>
            <ta e="T244" id="Seg_1984" s="T243">onnug-u</ta>
            <ta e="T245" id="Seg_1985" s="T244">kut-a-gɨn</ta>
            <ta e="T246" id="Seg_1986" s="T245">bu͡o</ta>
            <ta e="T247" id="Seg_1987" s="T246">on-tu-ŋ</ta>
            <ta e="T248" id="Seg_1988" s="T247">kolbasa</ta>
            <ta e="T249" id="Seg_1989" s="T248">bu͡ol-ar</ta>
            <ta e="T250" id="Seg_1990" s="T249">minnʼiges</ta>
            <ta e="T251" id="Seg_1991" s="T250">bagajɨ</ta>
            <ta e="T122" id="Seg_1992" s="T266">tɨl</ta>
            <ta e="T269" id="Seg_1993" s="T268">i</ta>
            <ta e="T270" id="Seg_1994" s="T269">öttük-ke</ta>
            <ta e="T271" id="Seg_1995" s="T270">bu͡ol-aːččɨ</ta>
            <ta e="T272" id="Seg_1996" s="T271">minnʼiges</ta>
            <ta e="T273" id="Seg_1997" s="T272">hɨ͡a</ta>
            <ta e="T274" id="Seg_1998" s="T273">hɨ͡a-laːk</ta>
            <ta e="T275" id="Seg_1999" s="T274">öttük-ke</ta>
            <ta e="T276" id="Seg_2000" s="T275">ke</ta>
            <ta e="T277" id="Seg_2001" s="T276">hɨ͡a</ta>
            <ta e="T278" id="Seg_2002" s="T277">bu͡ol-aːččɨ</ta>
            <ta e="T279" id="Seg_2003" s="T278">öttük-ke</ta>
            <ta e="T280" id="Seg_2004" s="T279">onnuk</ta>
            <ta e="T281" id="Seg_2005" s="T280">minnʼiges</ta>
            <ta e="T291" id="Seg_2006" s="T290">kim-niː-l-ler</ta>
            <ta e="T305" id="Seg_2007" s="T304">Kɨːl-ɨ-n</ta>
            <ta e="T306" id="Seg_2008" s="T305">kɨːl-ɨ-n</ta>
            <ta e="T307" id="Seg_2009" s="T306">et-e</ta>
            <ta e="T308" id="Seg_2010" s="T307">minnʼiges</ta>
            <ta e="T309" id="Seg_2011" s="T308">bu͡o</ta>
            <ta e="T316" id="Seg_2012" s="T315">beje-biti-ger</ta>
            <ta e="T317" id="Seg_2013" s="T316">taba</ta>
            <ta e="T318" id="Seg_2014" s="T317">et-i-n</ta>
            <ta e="T319" id="Seg_2015" s="T318">olus</ta>
            <ta e="T320" id="Seg_2016" s="T319">oččo-loː-čču-ta</ta>
            <ta e="T321" id="Seg_2017" s="T320">hu͡ok-pun</ta>
            <ta e="T322" id="Seg_2018" s="T321">kɨːl-ɨ-n</ta>
            <ta e="T323" id="Seg_2019" s="T322">et-e</ta>
            <ta e="T325" id="Seg_2020" s="T324">taba</ta>
            <ta e="T327" id="Seg_2021" s="T325">et-i-neːger</ta>
            <ta e="T328" id="Seg_2022" s="T327">minnʼiges</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2023" s="T0">taba</ta>
            <ta e="T2" id="Seg_2024" s="T1">kaːn-I-nAn</ta>
            <ta e="T3" id="Seg_2025" s="T2">bu͡ollagɨna</ta>
            <ta e="T4" id="Seg_2026" s="T3">kim-nI</ta>
            <ta e="T5" id="Seg_2027" s="T4">oŋor-A-BIt</ta>
            <ta e="T6" id="Seg_2028" s="T5">kolbasa</ta>
            <ta e="T7" id="Seg_2029" s="T6">ka</ta>
            <ta e="T8" id="Seg_2030" s="T7">iti</ta>
            <ta e="T9" id="Seg_2031" s="T8">bu͡ollagɨna</ta>
            <ta e="T10" id="Seg_2032" s="T9">kim</ta>
            <ta e="T11" id="Seg_2033" s="T10">taba-nI</ta>
            <ta e="T12" id="Seg_2034" s="T11">ölör-Ar-LAr</ta>
            <ta e="T13" id="Seg_2035" s="T12">bu͡o</ta>
            <ta e="T14" id="Seg_2036" s="T13">innʼe</ta>
            <ta e="T15" id="Seg_2037" s="T14">gɨn-An</ta>
            <ta e="T16" id="Seg_2038" s="T15">bar-An</ta>
            <ta e="T17" id="Seg_2039" s="T16">taːsɨk-GA</ta>
            <ta e="T18" id="Seg_2040" s="T17">kaːn</ta>
            <ta e="T19" id="Seg_2041" s="T18">ɨl-A-BIt</ta>
            <ta e="T20" id="Seg_2042" s="T19">i</ta>
            <ta e="T21" id="Seg_2043" s="T20">ol-nI</ta>
            <ta e="T22" id="Seg_2044" s="T21">bu͡ollagɨna</ta>
            <ta e="T23" id="Seg_2045" s="T22">tur-IAr-A-BIt</ta>
            <ta e="T24" id="Seg_2046" s="T23">bu͡o</ta>
            <ta e="T25" id="Seg_2047" s="T24">štobɨ</ta>
            <ta e="T26" id="Seg_2048" s="T25">sɨvorotka</ta>
            <ta e="T27" id="Seg_2049" s="T26">bu͡ol-IAK-tI-n</ta>
            <ta e="T28" id="Seg_2050" s="T27">ka</ta>
            <ta e="T29" id="Seg_2051" s="T28">sɨvorotka-nI</ta>
            <ta e="T30" id="Seg_2052" s="T29">kim-LAː-A</ta>
            <ta e="T31" id="Seg_2053" s="T30">mʼat-LAː-A-BIt</ta>
            <ta e="T32" id="Seg_2054" s="T31">mʼat-LAː-A-BIt</ta>
            <ta e="T33" id="Seg_2055" s="T32">sɨvorotka</ta>
            <ta e="T34" id="Seg_2056" s="T33">sɨvorotka</ta>
            <ta e="T35" id="Seg_2057" s="T34">bu͡ol-Ar</ta>
            <ta e="T36" id="Seg_2058" s="T35">ol-tI-ŋ</ta>
            <ta e="T37" id="Seg_2059" s="T36">i</ta>
            <ta e="T38" id="Seg_2060" s="T37">ol-nI</ta>
            <ta e="T39" id="Seg_2061" s="T38">bu͡ollagɨna</ta>
            <ta e="T40" id="Seg_2062" s="T39">ihigineːgi-LAr-nI</ta>
            <ta e="T41" id="Seg_2063" s="T40">ɨl-A-BIt</ta>
            <ta e="T45" id="Seg_2064" s="T44">onton</ta>
            <ta e="T46" id="Seg_2065" s="T45">kim-nI</ta>
            <ta e="T47" id="Seg_2066" s="T46">hu͡on</ta>
            <ta e="T48" id="Seg_2067" s="T47">haːk-tA</ta>
            <ta e="T51" id="Seg_2068" s="T50">hu͡on</ta>
            <ta e="T52" id="Seg_2069" s="T51">haːk-tI-n</ta>
            <ta e="T53" id="Seg_2070" s="T52">ɨl-A-GIn</ta>
            <ta e="T54" id="Seg_2071" s="T53">bu͡o</ta>
            <ta e="T55" id="Seg_2072" s="T54">homorso-tI-n</ta>
            <ta e="T56" id="Seg_2073" s="T55">ɨl-A-GIn</ta>
            <ta e="T57" id="Seg_2074" s="T56">bu͡o</ta>
            <ta e="T58" id="Seg_2075" s="T57">hača-tI-ttAn</ta>
            <ta e="T59" id="Seg_2076" s="T58">emi͡e</ta>
            <ta e="T60" id="Seg_2077" s="T59">ɨl-A-GIn</ta>
            <ta e="T61" id="Seg_2078" s="T60">innʼe</ta>
            <ta e="T62" id="Seg_2079" s="T61">barɨ-kAːN</ta>
            <ta e="T63" id="Seg_2080" s="T62">huːj-An</ta>
            <ta e="T64" id="Seg_2081" s="T63">huːj-An</ta>
            <ta e="T65" id="Seg_2082" s="T64">kanʼaː-An</ta>
            <ta e="T66" id="Seg_2083" s="T65">bar-An</ta>
            <ta e="T67" id="Seg_2084" s="T66">huːj-AttAː-A-GIn</ta>
            <ta e="T68" id="Seg_2085" s="T67">innʼe</ta>
            <ta e="T69" id="Seg_2086" s="T68">gɨn-An</ta>
            <ta e="T70" id="Seg_2087" s="T69">bar-An</ta>
            <ta e="T71" id="Seg_2088" s="T70">kaːn-I-ŋ</ta>
            <ta e="T72" id="Seg_2089" s="T71">bu͡ollagɨna</ta>
            <ta e="T73" id="Seg_2090" s="T72">sɨvorotka</ta>
            <ta e="T78" id="Seg_2091" s="T77">bagajɨ</ta>
            <ta e="T79" id="Seg_2092" s="T78">uː</ta>
            <ta e="T80" id="Seg_2093" s="T79">kaːn</ta>
            <ta e="T81" id="Seg_2094" s="T80">bu͡ol-Ar</ta>
            <ta e="T82" id="Seg_2095" s="T81">i</ta>
            <ta e="T83" id="Seg_2096" s="T82">ol-nI</ta>
            <ta e="T84" id="Seg_2097" s="T83">bu͡ollagɨna</ta>
            <ta e="T85" id="Seg_2098" s="T84">kim-GA</ta>
            <ta e="T86" id="Seg_2099" s="T85">kut-A-GIn</ta>
            <ta e="T87" id="Seg_2100" s="T86">ol</ta>
            <ta e="T88" id="Seg_2101" s="T87">homorso-LAr-GA</ta>
            <ta e="T89" id="Seg_2102" s="T88">kut-A-GIn</ta>
            <ta e="T90" id="Seg_2103" s="T89">bɨːčas-LAr-GA</ta>
            <ta e="T91" id="Seg_2104" s="T90">kut-A-GIn</ta>
            <ta e="T92" id="Seg_2105" s="T91">baːj-A-GIn</ta>
            <ta e="T93" id="Seg_2106" s="T92">i</ta>
            <ta e="T94" id="Seg_2107" s="T93">kim</ta>
            <ta e="T95" id="Seg_2108" s="T94">kim-GA</ta>
            <ta e="T96" id="Seg_2109" s="T95">bus-IAr-A-GIn</ta>
            <ta e="T97" id="Seg_2110" s="T96">itiː</ta>
            <ta e="T98" id="Seg_2111" s="T97">uː-GA</ta>
            <ta e="T99" id="Seg_2112" s="T98">uːr-A-GIn</ta>
            <ta e="T100" id="Seg_2113" s="T99">i</ta>
            <ta e="T104" id="Seg_2114" s="T103">bus-Ar</ta>
            <ta e="T105" id="Seg_2115" s="T104">ol-tI-ŋ</ta>
            <ta e="T106" id="Seg_2116" s="T105">i</ta>
            <ta e="T107" id="Seg_2117" s="T106">oroː-A-GIn</ta>
            <ta e="T108" id="Seg_2118" s="T107">i</ta>
            <ta e="T109" id="Seg_2119" s="T108">kolbasa</ta>
            <ta e="T110" id="Seg_2120" s="T109">bu͡ol-Ar</ta>
            <ta e="T112" id="Seg_2121" s="T111">türgen-LIk</ta>
            <ta e="T113" id="Seg_2122" s="T112">eː</ta>
            <ta e="T114" id="Seg_2123" s="T113">türgen</ta>
            <ta e="T115" id="Seg_2124" s="T114">bagajɨ</ta>
            <ta e="T116" id="Seg_2125" s="T115">vot</ta>
            <ta e="T117" id="Seg_2126" s="T116">eto</ta>
            <ta e="T118" id="Seg_2127" s="T117">že</ta>
            <ta e="T119" id="Seg_2128" s="T118">kaːn-I-ŋ</ta>
            <ta e="T120" id="Seg_2129" s="T119">uhun-LIk</ta>
            <ta e="T121" id="Seg_2130" s="T120">tur-IAK-tI-n</ta>
            <ta e="T123" id="Seg_2131" s="T121">naːda</ta>
            <ta e="T132" id="Seg_2132" s="T131">kaːn</ta>
            <ta e="T133" id="Seg_2133" s="T132">ɨl-A-GIn</ta>
            <ta e="T134" id="Seg_2134" s="T133">da</ta>
            <ta e="T147" id="Seg_2135" s="T329">tɨmnɨː</ta>
            <ta e="T148" id="Seg_2136" s="T147">hir-GA</ta>
            <ta e="T149" id="Seg_2137" s="T148">onton</ta>
            <ta e="T152" id="Seg_2138" s="T151">bus-Ar</ta>
            <ta e="T153" id="Seg_2139" s="T152">ɨl-LIN-I-BAT</ta>
            <ta e="T155" id="Seg_2140" s="T153">bu͡o</ta>
            <ta e="T158" id="Seg_2141" s="T157">tur-IAK-tI-n</ta>
            <ta e="T159" id="Seg_2142" s="T158">naːda</ta>
            <ta e="T160" id="Seg_2143" s="T159">kaːn-I-ŋ</ta>
            <ta e="T161" id="Seg_2144" s="T160">ka</ta>
            <ta e="T162" id="Seg_2145" s="T161">štobɨ</ta>
            <ta e="T163" id="Seg_2146" s="T162">sɨvorotka-LAː-n-IAK-tI-n</ta>
            <ta e="T165" id="Seg_2147" s="T163">ka</ta>
            <ta e="T166" id="Seg_2148" s="T165">a</ta>
            <ta e="T167" id="Seg_2149" s="T166">sɨvorotka</ta>
            <ta e="T168" id="Seg_2150" s="T167">komuj-LIN-Ar</ta>
            <ta e="T169" id="Seg_2151" s="T168">i</ta>
            <ta e="T170" id="Seg_2152" s="T169">ol-nI</ta>
            <ta e="T171" id="Seg_2153" s="T170">možno</ta>
            <ta e="T172" id="Seg_2154" s="T171">ol-nI</ta>
            <ta e="T173" id="Seg_2155" s="T172">dʼe</ta>
            <ta e="T174" id="Seg_2156" s="T173">ol</ta>
            <ta e="T175" id="Seg_2157" s="T174">kim-LAː-IAK-tI-n</ta>
            <ta e="T176" id="Seg_2158" s="T175">bu͡o</ta>
            <ta e="T177" id="Seg_2159" s="T176">paka</ta>
            <ta e="T178" id="Seg_2160" s="T177">sɨvorotka</ta>
            <ta e="T179" id="Seg_2161" s="T178">ol-tI-ŋ</ta>
            <ta e="T180" id="Seg_2162" s="T179">iti</ta>
            <ta e="T181" id="Seg_2163" s="T180">ol</ta>
            <ta e="T182" id="Seg_2164" s="T181">homorso</ta>
            <ta e="T183" id="Seg_2165" s="T182">is-tI-GAr</ta>
            <ta e="T184" id="Seg_2166" s="T183">uːr-A-GIn</ta>
            <ta e="T185" id="Seg_2167" s="T184">bu͡o</ta>
            <ta e="T186" id="Seg_2168" s="T185">kut-A-GIn</ta>
            <ta e="T187" id="Seg_2169" s="T186">bu͡o</ta>
            <ta e="T188" id="Seg_2170" s="T187">baːj-A-GIn</ta>
            <ta e="T189" id="Seg_2171" s="T188">uːr-A-GIn</ta>
            <ta e="T190" id="Seg_2172" s="T189">onton</ta>
            <ta e="T191" id="Seg_2173" s="T190">emi͡e</ta>
            <ta e="T192" id="Seg_2174" s="T191">kutuja</ta>
            <ta e="T193" id="Seg_2175" s="T192">baːr</ta>
            <ta e="T194" id="Seg_2176" s="T193">kutuja-LAːK</ta>
            <ta e="T196" id="Seg_2177" s="T195">kutuja</ta>
            <ta e="T197" id="Seg_2178" s="T196">kutuja</ta>
            <ta e="T198" id="Seg_2179" s="T197">ol</ta>
            <ta e="T199" id="Seg_2180" s="T198">is-I-GAr</ta>
            <ta e="T200" id="Seg_2181" s="T199">emi͡e</ta>
            <ta e="T201" id="Seg_2182" s="T200">možno</ta>
            <ta e="T202" id="Seg_2183" s="T201">kut-IAK-GI-n</ta>
            <ta e="T203" id="Seg_2184" s="T202">kaːn-nI</ta>
            <ta e="T204" id="Seg_2185" s="T203">sɨvorotka</ta>
            <ta e="T205" id="Seg_2186" s="T204">onno</ta>
            <ta e="T206" id="Seg_2187" s="T205">kim</ta>
            <ta e="T207" id="Seg_2188" s="T206">bu͡ol-AːččI</ta>
            <ta e="T208" id="Seg_2189" s="T207">agɨjak</ta>
            <ta e="T209" id="Seg_2190" s="T208">sɨvorotka</ta>
            <ta e="T210" id="Seg_2191" s="T209">nemnožko</ta>
            <ta e="T213" id="Seg_2192" s="T212">eto</ta>
            <ta e="T214" id="Seg_2193" s="T213">elbek</ta>
            <ta e="T215" id="Seg_2194" s="T214">taba-nI</ta>
            <ta e="T216" id="Seg_2195" s="T215">ölör-TAK-GInA</ta>
            <ta e="T217" id="Seg_2196" s="T216">sɨvorotka</ta>
            <ta e="T218" id="Seg_2197" s="T217">mnogo</ta>
            <ta e="T219" id="Seg_2198" s="T218">bu͡ol-Ar</ta>
            <ta e="T220" id="Seg_2199" s="T219">elbek</ta>
            <ta e="T221" id="Seg_2200" s="T220">bu͡ol-Ar</ta>
            <ta e="T222" id="Seg_2201" s="T221">bu͡o</ta>
            <ta e="T223" id="Seg_2202" s="T222">oččogo</ta>
            <ta e="T224" id="Seg_2203" s="T223">ol-nI</ta>
            <ta e="T225" id="Seg_2204" s="T224">bu͡o</ta>
            <ta e="T226" id="Seg_2205" s="T225">elbek</ta>
            <ta e="T227" id="Seg_2206" s="T226">kolbasa</ta>
            <ta e="T229" id="Seg_2207" s="T227">bu͡ol-Ar</ta>
            <ta e="T231" id="Seg_2208" s="T230">homorso-tI-GAr</ta>
            <ta e="T232" id="Seg_2209" s="T231">kut-A-GIn</ta>
            <ta e="T233" id="Seg_2210" s="T232">hu͡on</ta>
            <ta e="T234" id="Seg_2211" s="T233">hu͡on</ta>
            <ta e="T235" id="Seg_2212" s="T234">hu͡on</ta>
            <ta e="T236" id="Seg_2213" s="T235">haːk-tI-GAr</ta>
            <ta e="T237" id="Seg_2214" s="T236">kut-A-GIn</ta>
            <ta e="T238" id="Seg_2215" s="T237">bɨːčas-tI-GAr</ta>
            <ta e="T239" id="Seg_2216" s="T238">ka</ta>
            <ta e="T243" id="Seg_2217" s="T241">bɨːčas</ta>
            <ta e="T244" id="Seg_2218" s="T243">onnuk-nI</ta>
            <ta e="T245" id="Seg_2219" s="T244">kut-A-GIn</ta>
            <ta e="T246" id="Seg_2220" s="T245">bu͡o</ta>
            <ta e="T247" id="Seg_2221" s="T246">ol-tI-ŋ</ta>
            <ta e="T248" id="Seg_2222" s="T247">kolbasa</ta>
            <ta e="T249" id="Seg_2223" s="T248">bu͡ol-Ar</ta>
            <ta e="T250" id="Seg_2224" s="T249">minnʼiges</ta>
            <ta e="T251" id="Seg_2225" s="T250">bagajɨ</ta>
            <ta e="T122" id="Seg_2226" s="T266">tɨl</ta>
            <ta e="T269" id="Seg_2227" s="T268">i</ta>
            <ta e="T270" id="Seg_2228" s="T269">öttük-GA</ta>
            <ta e="T271" id="Seg_2229" s="T270">bu͡ol-AːččI</ta>
            <ta e="T272" id="Seg_2230" s="T271">minnʼiges</ta>
            <ta e="T273" id="Seg_2231" s="T272">hɨ͡a</ta>
            <ta e="T274" id="Seg_2232" s="T273">hɨ͡a-LAːK</ta>
            <ta e="T275" id="Seg_2233" s="T274">öttük-GA</ta>
            <ta e="T276" id="Seg_2234" s="T275">ka</ta>
            <ta e="T277" id="Seg_2235" s="T276">hɨ͡a</ta>
            <ta e="T278" id="Seg_2236" s="T277">bu͡ol-AːččI</ta>
            <ta e="T279" id="Seg_2237" s="T278">öttük-GA</ta>
            <ta e="T280" id="Seg_2238" s="T279">onnuk</ta>
            <ta e="T281" id="Seg_2239" s="T280">minnʼiges</ta>
            <ta e="T291" id="Seg_2240" s="T290">kim-LAː-Ar-LAr</ta>
            <ta e="T305" id="Seg_2241" s="T304">kɨːl-tI-n</ta>
            <ta e="T306" id="Seg_2242" s="T305">kɨːl-tI-n</ta>
            <ta e="T307" id="Seg_2243" s="T306">et-tA</ta>
            <ta e="T308" id="Seg_2244" s="T307">minnʼiges</ta>
            <ta e="T309" id="Seg_2245" s="T308">bu͡o</ta>
            <ta e="T316" id="Seg_2246" s="T315">beje-BItI-GAr</ta>
            <ta e="T317" id="Seg_2247" s="T316">taba</ta>
            <ta e="T318" id="Seg_2248" s="T317">et-tI-n</ta>
            <ta e="T319" id="Seg_2249" s="T318">olus</ta>
            <ta e="T320" id="Seg_2250" s="T319">oččo-LAː-AːččI-tA</ta>
            <ta e="T321" id="Seg_2251" s="T320">hu͡ok-BIn</ta>
            <ta e="T322" id="Seg_2252" s="T321">kɨːl-tI-n</ta>
            <ta e="T323" id="Seg_2253" s="T322">et-tA</ta>
            <ta e="T325" id="Seg_2254" s="T324">taba</ta>
            <ta e="T327" id="Seg_2255" s="T325">et-tI-TAːgAr</ta>
            <ta e="T328" id="Seg_2256" s="T327">minnʼiges</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2257" s="T0">reindeer.[NOM]</ta>
            <ta e="T2" id="Seg_2258" s="T1">blood-3SG-INSTR</ta>
            <ta e="T3" id="Seg_2259" s="T2">though</ta>
            <ta e="T4" id="Seg_2260" s="T3">who-ACC</ta>
            <ta e="T5" id="Seg_2261" s="T4">make-PRS-1PL</ta>
            <ta e="T6" id="Seg_2262" s="T5">sausage.[NOM]</ta>
            <ta e="T7" id="Seg_2263" s="T6">well</ta>
            <ta e="T8" id="Seg_2264" s="T7">that.[NOM]</ta>
            <ta e="T9" id="Seg_2265" s="T8">though</ta>
            <ta e="T10" id="Seg_2266" s="T9">who.[NOM]</ta>
            <ta e="T11" id="Seg_2267" s="T10">reindeer-ACC</ta>
            <ta e="T12" id="Seg_2268" s="T11">kill-PRS-3PL</ta>
            <ta e="T13" id="Seg_2269" s="T12">EMPH</ta>
            <ta e="T14" id="Seg_2270" s="T13">so</ta>
            <ta e="T15" id="Seg_2271" s="T14">make-CVB.SEQ</ta>
            <ta e="T16" id="Seg_2272" s="T15">go-CVB.SEQ</ta>
            <ta e="T17" id="Seg_2273" s="T16">small.basin-DAT/LOC</ta>
            <ta e="T18" id="Seg_2274" s="T17">blood.[NOM]</ta>
            <ta e="T19" id="Seg_2275" s="T18">take-PRS-1PL</ta>
            <ta e="T20" id="Seg_2276" s="T19">and</ta>
            <ta e="T21" id="Seg_2277" s="T20">that-ACC</ta>
            <ta e="T22" id="Seg_2278" s="T21">though</ta>
            <ta e="T23" id="Seg_2279" s="T22">stand-CAUS-PRS-1PL</ta>
            <ta e="T24" id="Seg_2280" s="T23">EMPH</ta>
            <ta e="T25" id="Seg_2281" s="T24">in.order</ta>
            <ta e="T26" id="Seg_2282" s="T25">serum.[NOM]</ta>
            <ta e="T27" id="Seg_2283" s="T26">become-PTCP.FUT-3SG-ACC</ta>
            <ta e="T28" id="Seg_2284" s="T27">well</ta>
            <ta e="T29" id="Seg_2285" s="T28">serum-ACC</ta>
            <ta e="T30" id="Seg_2286" s="T29">who-VBZ-CVB.SIM</ta>
            <ta e="T31" id="Seg_2287" s="T30">knead-VBZ-PRS-1PL</ta>
            <ta e="T32" id="Seg_2288" s="T31">knead-VBZ-PRS-1PL</ta>
            <ta e="T33" id="Seg_2289" s="T32">serum.[NOM]</ta>
            <ta e="T34" id="Seg_2290" s="T33">serum.[NOM]</ta>
            <ta e="T35" id="Seg_2291" s="T34">be-PRS.[3SG]</ta>
            <ta e="T36" id="Seg_2292" s="T35">that-3SG-2SG.[NOM]</ta>
            <ta e="T37" id="Seg_2293" s="T36">and</ta>
            <ta e="T38" id="Seg_2294" s="T37">that-ACC</ta>
            <ta e="T39" id="Seg_2295" s="T38">though</ta>
            <ta e="T40" id="Seg_2296" s="T39">innards-PL-ACC</ta>
            <ta e="T41" id="Seg_2297" s="T40">take-PRS-1PL</ta>
            <ta e="T45" id="Seg_2298" s="T44">then</ta>
            <ta e="T46" id="Seg_2299" s="T45">who-ACC</ta>
            <ta e="T47" id="Seg_2300" s="T46">fat</ta>
            <ta e="T48" id="Seg_2301" s="T47">faeces-3SG.[NOM]</ta>
            <ta e="T51" id="Seg_2302" s="T50">fat</ta>
            <ta e="T52" id="Seg_2303" s="T51">faeces-3SG-ACC</ta>
            <ta e="T53" id="Seg_2304" s="T52">take-PRS-2SG</ta>
            <ta e="T54" id="Seg_2305" s="T53">EMPH</ta>
            <ta e="T55" id="Seg_2306" s="T54">abomasum-3SG-ACC</ta>
            <ta e="T56" id="Seg_2307" s="T55">take-PRS-2SG</ta>
            <ta e="T57" id="Seg_2308" s="T56">EMPH</ta>
            <ta e="T58" id="Seg_2309" s="T57">gut-3SG-ABL</ta>
            <ta e="T59" id="Seg_2310" s="T58">again</ta>
            <ta e="T60" id="Seg_2311" s="T59">take-PRS-2SG</ta>
            <ta e="T61" id="Seg_2312" s="T60">so</ta>
            <ta e="T62" id="Seg_2313" s="T61">whole-INTNS.[NOM]</ta>
            <ta e="T63" id="Seg_2314" s="T62">wash-CVB.SEQ</ta>
            <ta e="T64" id="Seg_2315" s="T63">wash-CVB.SEQ</ta>
            <ta e="T65" id="Seg_2316" s="T64">and.so.on-CVB.SEQ</ta>
            <ta e="T66" id="Seg_2317" s="T65">go-CVB.SEQ</ta>
            <ta e="T67" id="Seg_2318" s="T66">wash-MULT-PRS-2SG</ta>
            <ta e="T68" id="Seg_2319" s="T67">so</ta>
            <ta e="T69" id="Seg_2320" s="T68">make-CVB.SEQ</ta>
            <ta e="T70" id="Seg_2321" s="T69">go-CVB.SEQ</ta>
            <ta e="T71" id="Seg_2322" s="T70">blood-EP-2SG.[NOM]</ta>
            <ta e="T72" id="Seg_2323" s="T71">though</ta>
            <ta e="T73" id="Seg_2324" s="T72">serum.[NOM]</ta>
            <ta e="T78" id="Seg_2325" s="T77">very</ta>
            <ta e="T79" id="Seg_2326" s="T78">water.[NOM]</ta>
            <ta e="T80" id="Seg_2327" s="T79">blood.[NOM]</ta>
            <ta e="T81" id="Seg_2328" s="T80">be-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_2329" s="T81">and</ta>
            <ta e="T83" id="Seg_2330" s="T82">that-ACC</ta>
            <ta e="T84" id="Seg_2331" s="T83">though</ta>
            <ta e="T85" id="Seg_2332" s="T84">who-DAT/LOC</ta>
            <ta e="T86" id="Seg_2333" s="T85">pour-PRS-2SG</ta>
            <ta e="T87" id="Seg_2334" s="T86">that</ta>
            <ta e="T88" id="Seg_2335" s="T87">abomasum-PL-DAT/LOC</ta>
            <ta e="T89" id="Seg_2336" s="T88">pour-PRS-2SG</ta>
            <ta e="T90" id="Seg_2337" s="T89">small.intestine-PL-DAT/LOC</ta>
            <ta e="T91" id="Seg_2338" s="T90">pour-PRS-2SG</ta>
            <ta e="T92" id="Seg_2339" s="T91">tie-PRS-2SG</ta>
            <ta e="T93" id="Seg_2340" s="T92">and</ta>
            <ta e="T94" id="Seg_2341" s="T93">who.[NOM]</ta>
            <ta e="T95" id="Seg_2342" s="T94">who-DAT/LOC</ta>
            <ta e="T96" id="Seg_2343" s="T95">boil-CAUS-PRS-2SG</ta>
            <ta e="T97" id="Seg_2344" s="T96">warm</ta>
            <ta e="T98" id="Seg_2345" s="T97">water-DAT/LOC</ta>
            <ta e="T99" id="Seg_2346" s="T98">lay-PRS-2SG</ta>
            <ta e="T100" id="Seg_2347" s="T99">and</ta>
            <ta e="T104" id="Seg_2348" s="T103">boil-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_2349" s="T104">that-3SG-2SG.[NOM]</ta>
            <ta e="T106" id="Seg_2350" s="T105">and</ta>
            <ta e="T107" id="Seg_2351" s="T106">take.out-PRS-2SG</ta>
            <ta e="T108" id="Seg_2352" s="T107">and</ta>
            <ta e="T109" id="Seg_2353" s="T108">sausage.[NOM]</ta>
            <ta e="T110" id="Seg_2354" s="T109">be-PRS.[3SG]</ta>
            <ta e="T112" id="Seg_2355" s="T111">fast-ADVZ</ta>
            <ta e="T113" id="Seg_2356" s="T112">AFFIRM</ta>
            <ta e="T114" id="Seg_2357" s="T113">fast</ta>
            <ta e="T115" id="Seg_2358" s="T114">very</ta>
            <ta e="T116" id="Seg_2359" s="T115">well</ta>
            <ta e="T117" id="Seg_2360" s="T116">this</ta>
            <ta e="T118" id="Seg_2361" s="T117">EMPH</ta>
            <ta e="T119" id="Seg_2362" s="T118">blood-EP-2SG.[NOM]</ta>
            <ta e="T120" id="Seg_2363" s="T119">long-ADVZ</ta>
            <ta e="T121" id="Seg_2364" s="T120">stand-PTCP.FUT-3SG-ACC</ta>
            <ta e="T123" id="Seg_2365" s="T121">need.to</ta>
            <ta e="T132" id="Seg_2366" s="T131">blood.[NOM]</ta>
            <ta e="T133" id="Seg_2367" s="T132">take-PRS-2SG</ta>
            <ta e="T134" id="Seg_2368" s="T133">and</ta>
            <ta e="T147" id="Seg_2369" s="T329">cold</ta>
            <ta e="T148" id="Seg_2370" s="T147">place-DAT/LOC</ta>
            <ta e="T149" id="Seg_2371" s="T148">then</ta>
            <ta e="T152" id="Seg_2372" s="T151">boil-PTCP.PRS</ta>
            <ta e="T153" id="Seg_2373" s="T152">get-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T155" id="Seg_2374" s="T153">EMPH</ta>
            <ta e="T158" id="Seg_2375" s="T157">stand-PTCP.FUT-3SG-ACC</ta>
            <ta e="T159" id="Seg_2376" s="T158">need.to</ta>
            <ta e="T160" id="Seg_2377" s="T159">blood-EP-2SG.[NOM]</ta>
            <ta e="T161" id="Seg_2378" s="T160">well</ta>
            <ta e="T162" id="Seg_2379" s="T161">in.order</ta>
            <ta e="T163" id="Seg_2380" s="T162">serum-VBZ-REFL-PTCP.FUT-3SG-ACC</ta>
            <ta e="T165" id="Seg_2381" s="T163">well</ta>
            <ta e="T166" id="Seg_2382" s="T165">and</ta>
            <ta e="T167" id="Seg_2383" s="T166">serum.[NOM]</ta>
            <ta e="T168" id="Seg_2384" s="T167">gather-PASS/REFL-PRS.[3SG]</ta>
            <ta e="T169" id="Seg_2385" s="T168">and</ta>
            <ta e="T170" id="Seg_2386" s="T169">that-ACC</ta>
            <ta e="T171" id="Seg_2387" s="T170">it.is.possible</ta>
            <ta e="T172" id="Seg_2388" s="T171">that-ACC</ta>
            <ta e="T173" id="Seg_2389" s="T172">well</ta>
            <ta e="T174" id="Seg_2390" s="T173">that</ta>
            <ta e="T175" id="Seg_2391" s="T174">who-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T176" id="Seg_2392" s="T175">EMPH</ta>
            <ta e="T177" id="Seg_2393" s="T176">as.long</ta>
            <ta e="T178" id="Seg_2394" s="T177">serum.[NOM]</ta>
            <ta e="T179" id="Seg_2395" s="T178">that-3SG-2SG.[NOM]</ta>
            <ta e="T180" id="Seg_2396" s="T179">that.[NOM]</ta>
            <ta e="T181" id="Seg_2397" s="T180">that</ta>
            <ta e="T182" id="Seg_2398" s="T181">abomasum.[NOM]</ta>
            <ta e="T183" id="Seg_2399" s="T182">inside-3SG-DAT/LOC</ta>
            <ta e="T184" id="Seg_2400" s="T183">lay-PRS-2SG</ta>
            <ta e="T185" id="Seg_2401" s="T184">EMPH</ta>
            <ta e="T186" id="Seg_2402" s="T185">pour-PRS-2SG</ta>
            <ta e="T187" id="Seg_2403" s="T186">EMPH</ta>
            <ta e="T188" id="Seg_2404" s="T187">tie-PRS-2SG</ta>
            <ta e="T189" id="Seg_2405" s="T188">lay-PRS-2SG</ta>
            <ta e="T190" id="Seg_2406" s="T189">then</ta>
            <ta e="T191" id="Seg_2407" s="T190">again</ta>
            <ta e="T192" id="Seg_2408" s="T191">reticulum.[NOM]</ta>
            <ta e="T193" id="Seg_2409" s="T192">there.is</ta>
            <ta e="T194" id="Seg_2410" s="T193">reticulum-PROPR</ta>
            <ta e="T196" id="Seg_2411" s="T195">reticulum.[NOM]</ta>
            <ta e="T197" id="Seg_2412" s="T196">reticulum.[NOM]</ta>
            <ta e="T198" id="Seg_2413" s="T197">that</ta>
            <ta e="T199" id="Seg_2414" s="T198">inside-3SG-DAT/LOC</ta>
            <ta e="T200" id="Seg_2415" s="T199">again</ta>
            <ta e="T201" id="Seg_2416" s="T200">it.is.possible</ta>
            <ta e="T202" id="Seg_2417" s="T201">pour-PTCP.FUT-2SG-ACC</ta>
            <ta e="T203" id="Seg_2418" s="T202">blood-ACC</ta>
            <ta e="T204" id="Seg_2419" s="T203">serum.[NOM]</ta>
            <ta e="T205" id="Seg_2420" s="T204">there</ta>
            <ta e="T206" id="Seg_2421" s="T205">who.[NOM]</ta>
            <ta e="T207" id="Seg_2422" s="T206">be-HAB.[3SG]</ta>
            <ta e="T208" id="Seg_2423" s="T207">little</ta>
            <ta e="T209" id="Seg_2424" s="T208">serum.[NOM]</ta>
            <ta e="T210" id="Seg_2425" s="T209">a.little.bit</ta>
            <ta e="T213" id="Seg_2426" s="T212">this</ta>
            <ta e="T214" id="Seg_2427" s="T213">many</ta>
            <ta e="T215" id="Seg_2428" s="T214">reindeer-ACC</ta>
            <ta e="T216" id="Seg_2429" s="T215">kill-TEMP-2SG</ta>
            <ta e="T217" id="Seg_2430" s="T216">serum.[NOM]</ta>
            <ta e="T218" id="Seg_2431" s="T217">a.lot</ta>
            <ta e="T219" id="Seg_2432" s="T218">be-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_2433" s="T219">many</ta>
            <ta e="T221" id="Seg_2434" s="T220">be-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_2435" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_2436" s="T222">then</ta>
            <ta e="T224" id="Seg_2437" s="T223">that-ACC</ta>
            <ta e="T225" id="Seg_2438" s="T224">EMPH</ta>
            <ta e="T226" id="Seg_2439" s="T225">many</ta>
            <ta e="T227" id="Seg_2440" s="T226">sausage.[NOM]</ta>
            <ta e="T229" id="Seg_2441" s="T227">be-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_2442" s="T230">abomasum-3SG-DAT/LOC</ta>
            <ta e="T232" id="Seg_2443" s="T231">pour-PRS-2SG</ta>
            <ta e="T233" id="Seg_2444" s="T232">fat</ta>
            <ta e="T234" id="Seg_2445" s="T233">fat</ta>
            <ta e="T235" id="Seg_2446" s="T234">fat</ta>
            <ta e="T236" id="Seg_2447" s="T235">faeces-3SG-DAT/LOC</ta>
            <ta e="T237" id="Seg_2448" s="T236">pour-PRS-2SG</ta>
            <ta e="T238" id="Seg_2449" s="T237">small.intestine-3SG-DAT/LOC</ta>
            <ta e="T239" id="Seg_2450" s="T238">well</ta>
            <ta e="T243" id="Seg_2451" s="T241">small.intestine</ta>
            <ta e="T244" id="Seg_2452" s="T243">such-ACC</ta>
            <ta e="T245" id="Seg_2453" s="T244">pour-PRS-2SG</ta>
            <ta e="T246" id="Seg_2454" s="T245">EMPH</ta>
            <ta e="T247" id="Seg_2455" s="T246">that-3SG-2SG.[NOM]</ta>
            <ta e="T248" id="Seg_2456" s="T247">sausage.[NOM]</ta>
            <ta e="T249" id="Seg_2457" s="T248">be-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_2458" s="T249">tasty</ta>
            <ta e="T251" id="Seg_2459" s="T250">very</ta>
            <ta e="T122" id="Seg_2460" s="T266">tongue.[NOM]</ta>
            <ta e="T269" id="Seg_2461" s="T268">and</ta>
            <ta e="T270" id="Seg_2462" s="T269">hip-DAT/LOC</ta>
            <ta e="T271" id="Seg_2463" s="T270">be-HAB.[3SG]</ta>
            <ta e="T272" id="Seg_2464" s="T271">tasty</ta>
            <ta e="T273" id="Seg_2465" s="T272">fat.[NOM]</ta>
            <ta e="T274" id="Seg_2466" s="T273">fat-PROPR</ta>
            <ta e="T275" id="Seg_2467" s="T274">hip-DAT/LOC</ta>
            <ta e="T276" id="Seg_2468" s="T275">well</ta>
            <ta e="T277" id="Seg_2469" s="T276">fat.[NOM]</ta>
            <ta e="T278" id="Seg_2470" s="T277">be-HAB.[3SG]</ta>
            <ta e="T279" id="Seg_2471" s="T278">hip-DAT/LOC</ta>
            <ta e="T280" id="Seg_2472" s="T279">such.[NOM]</ta>
            <ta e="T281" id="Seg_2473" s="T280">tasty</ta>
            <ta e="T291" id="Seg_2474" s="T290">who-VBZ-PRS-3PL</ta>
            <ta e="T305" id="Seg_2475" s="T304">wild.reindeer-3SG-GEN</ta>
            <ta e="T306" id="Seg_2476" s="T305">wild.reindeer-3SG-GEN</ta>
            <ta e="T307" id="Seg_2477" s="T306">meat-3SG.[NOM]</ta>
            <ta e="T308" id="Seg_2478" s="T307">sweet.[NOM]</ta>
            <ta e="T309" id="Seg_2479" s="T308">EMPH</ta>
            <ta e="T316" id="Seg_2480" s="T315">self-1PL-DAT/LOC</ta>
            <ta e="T317" id="Seg_2481" s="T316">reindeer.[NOM]</ta>
            <ta e="T318" id="Seg_2482" s="T317">meat-3SG-ACC</ta>
            <ta e="T319" id="Seg_2483" s="T318">very</ta>
            <ta e="T320" id="Seg_2484" s="T319">so.much-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T321" id="Seg_2485" s="T320">NEG-1SG</ta>
            <ta e="T322" id="Seg_2486" s="T321">wild.reindeer-3SG-GEN</ta>
            <ta e="T323" id="Seg_2487" s="T322">meat-3SG.[NOM]</ta>
            <ta e="T325" id="Seg_2488" s="T324">reindeer.[NOM]</ta>
            <ta e="T327" id="Seg_2489" s="T325">meat-3SG-COMP</ta>
            <ta e="T328" id="Seg_2490" s="T327">tasty</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2491" s="T0">Rentier.[NOM]</ta>
            <ta e="T2" id="Seg_2492" s="T1">Blut-3SG-INSTR</ta>
            <ta e="T3" id="Seg_2493" s="T2">aber</ta>
            <ta e="T4" id="Seg_2494" s="T3">wer-ACC</ta>
            <ta e="T5" id="Seg_2495" s="T4">machen-PRS-1PL</ta>
            <ta e="T6" id="Seg_2496" s="T5">Wurst.[NOM]</ta>
            <ta e="T7" id="Seg_2497" s="T6">nun</ta>
            <ta e="T8" id="Seg_2498" s="T7">dieses.[NOM]</ta>
            <ta e="T9" id="Seg_2499" s="T8">aber</ta>
            <ta e="T10" id="Seg_2500" s="T9">wer.[NOM]</ta>
            <ta e="T11" id="Seg_2501" s="T10">Rentier-ACC</ta>
            <ta e="T12" id="Seg_2502" s="T11">töten-PRS-3PL</ta>
            <ta e="T13" id="Seg_2503" s="T12">EMPH</ta>
            <ta e="T14" id="Seg_2504" s="T13">so</ta>
            <ta e="T15" id="Seg_2505" s="T14">machen-CVB.SEQ</ta>
            <ta e="T16" id="Seg_2506" s="T15">gehen-CVB.SEQ</ta>
            <ta e="T17" id="Seg_2507" s="T16">kleines.Waschbecken-DAT/LOC</ta>
            <ta e="T18" id="Seg_2508" s="T17">Blut.[NOM]</ta>
            <ta e="T19" id="Seg_2509" s="T18">nehmen-PRS-1PL</ta>
            <ta e="T20" id="Seg_2510" s="T19">und</ta>
            <ta e="T21" id="Seg_2511" s="T20">jenes-ACC</ta>
            <ta e="T22" id="Seg_2512" s="T21">aber</ta>
            <ta e="T23" id="Seg_2513" s="T22">stehen-CAUS-PRS-1PL</ta>
            <ta e="T24" id="Seg_2514" s="T23">EMPH</ta>
            <ta e="T25" id="Seg_2515" s="T24">damit</ta>
            <ta e="T26" id="Seg_2516" s="T25">Serum.[NOM]</ta>
            <ta e="T27" id="Seg_2517" s="T26">werden-PTCP.FUT-3SG-ACC</ta>
            <ta e="T28" id="Seg_2518" s="T27">nun</ta>
            <ta e="T29" id="Seg_2519" s="T28">Serum-ACC</ta>
            <ta e="T30" id="Seg_2520" s="T29">wer-VBZ-CVB.SIM</ta>
            <ta e="T31" id="Seg_2521" s="T30">kneten-VBZ-PRS-1PL</ta>
            <ta e="T32" id="Seg_2522" s="T31">kneten-VBZ-PRS-1PL</ta>
            <ta e="T33" id="Seg_2523" s="T32">Serum.[NOM]</ta>
            <ta e="T34" id="Seg_2524" s="T33">Serum.[NOM]</ta>
            <ta e="T35" id="Seg_2525" s="T34">sein-PRS.[3SG]</ta>
            <ta e="T36" id="Seg_2526" s="T35">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T37" id="Seg_2527" s="T36">und</ta>
            <ta e="T38" id="Seg_2528" s="T37">jenes-ACC</ta>
            <ta e="T39" id="Seg_2529" s="T38">aber</ta>
            <ta e="T40" id="Seg_2530" s="T39">Innereien-PL-ACC</ta>
            <ta e="T41" id="Seg_2531" s="T40">nehmen-PRS-1PL</ta>
            <ta e="T45" id="Seg_2532" s="T44">dann</ta>
            <ta e="T46" id="Seg_2533" s="T45">wer-ACC</ta>
            <ta e="T47" id="Seg_2534" s="T46">dick</ta>
            <ta e="T48" id="Seg_2535" s="T47">Kot-3SG.[NOM]</ta>
            <ta e="T51" id="Seg_2536" s="T50">dick</ta>
            <ta e="T52" id="Seg_2537" s="T51">Kot-3SG-ACC</ta>
            <ta e="T53" id="Seg_2538" s="T52">nehmen-PRS-2SG</ta>
            <ta e="T54" id="Seg_2539" s="T53">EMPH</ta>
            <ta e="T55" id="Seg_2540" s="T54">Labmagen-3SG-ACC</ta>
            <ta e="T56" id="Seg_2541" s="T55">nehmen-PRS-2SG</ta>
            <ta e="T57" id="Seg_2542" s="T56">EMPH</ta>
            <ta e="T58" id="Seg_2543" s="T57">Darm-3SG-ABL</ta>
            <ta e="T59" id="Seg_2544" s="T58">wieder</ta>
            <ta e="T60" id="Seg_2545" s="T59">nehmen-PRS-2SG</ta>
            <ta e="T61" id="Seg_2546" s="T60">so</ta>
            <ta e="T62" id="Seg_2547" s="T61">ganz-INTNS.[NOM]</ta>
            <ta e="T63" id="Seg_2548" s="T62">abwaschen-CVB.SEQ</ta>
            <ta e="T64" id="Seg_2549" s="T63">abwaschen-CVB.SEQ</ta>
            <ta e="T65" id="Seg_2550" s="T64">und.so.weiter-CVB.SEQ</ta>
            <ta e="T66" id="Seg_2551" s="T65">gehen-CVB.SEQ</ta>
            <ta e="T67" id="Seg_2552" s="T66">abwaschen-MULT-PRS-2SG</ta>
            <ta e="T68" id="Seg_2553" s="T67">so</ta>
            <ta e="T69" id="Seg_2554" s="T68">machen-CVB.SEQ</ta>
            <ta e="T70" id="Seg_2555" s="T69">gehen-CVB.SEQ</ta>
            <ta e="T71" id="Seg_2556" s="T70">Blut-EP-2SG.[NOM]</ta>
            <ta e="T72" id="Seg_2557" s="T71">aber</ta>
            <ta e="T73" id="Seg_2558" s="T72">Serum.[NOM]</ta>
            <ta e="T78" id="Seg_2559" s="T77">sehr</ta>
            <ta e="T79" id="Seg_2560" s="T78">Wasser.[NOM]</ta>
            <ta e="T80" id="Seg_2561" s="T79">Blut.[NOM]</ta>
            <ta e="T81" id="Seg_2562" s="T80">sein-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_2563" s="T81">und</ta>
            <ta e="T83" id="Seg_2564" s="T82">jenes-ACC</ta>
            <ta e="T84" id="Seg_2565" s="T83">aber</ta>
            <ta e="T85" id="Seg_2566" s="T84">wer-DAT/LOC</ta>
            <ta e="T86" id="Seg_2567" s="T85">gießen-PRS-2SG</ta>
            <ta e="T87" id="Seg_2568" s="T86">jenes</ta>
            <ta e="T88" id="Seg_2569" s="T87">Labmagen-PL-DAT/LOC</ta>
            <ta e="T89" id="Seg_2570" s="T88">gießen-PRS-2SG</ta>
            <ta e="T90" id="Seg_2571" s="T89">Dünndarm-PL-DAT/LOC</ta>
            <ta e="T91" id="Seg_2572" s="T90">gießen-PRS-2SG</ta>
            <ta e="T92" id="Seg_2573" s="T91">binden-PRS-2SG</ta>
            <ta e="T93" id="Seg_2574" s="T92">und</ta>
            <ta e="T94" id="Seg_2575" s="T93">wer.[NOM]</ta>
            <ta e="T95" id="Seg_2576" s="T94">wer-DAT/LOC</ta>
            <ta e="T96" id="Seg_2577" s="T95">kochen-CAUS-PRS-2SG</ta>
            <ta e="T97" id="Seg_2578" s="T96">warm</ta>
            <ta e="T98" id="Seg_2579" s="T97">Wasser-DAT/LOC</ta>
            <ta e="T99" id="Seg_2580" s="T98">legen-PRS-2SG</ta>
            <ta e="T100" id="Seg_2581" s="T99">und</ta>
            <ta e="T104" id="Seg_2582" s="T103">kochen-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_2583" s="T104">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T106" id="Seg_2584" s="T105">und</ta>
            <ta e="T107" id="Seg_2585" s="T106">herausnehmen-PRS-2SG</ta>
            <ta e="T108" id="Seg_2586" s="T107">und</ta>
            <ta e="T109" id="Seg_2587" s="T108">Wurst.[NOM]</ta>
            <ta e="T110" id="Seg_2588" s="T109">sein-PRS.[3SG]</ta>
            <ta e="T112" id="Seg_2589" s="T111">schnell-ADVZ</ta>
            <ta e="T113" id="Seg_2590" s="T112">AFFIRM</ta>
            <ta e="T114" id="Seg_2591" s="T113">schnell</ta>
            <ta e="T115" id="Seg_2592" s="T114">sehr</ta>
            <ta e="T116" id="Seg_2593" s="T115">so</ta>
            <ta e="T117" id="Seg_2594" s="T116">dies</ta>
            <ta e="T118" id="Seg_2595" s="T117">EMPH</ta>
            <ta e="T119" id="Seg_2596" s="T118">Blut-EP-2SG.[NOM]</ta>
            <ta e="T120" id="Seg_2597" s="T119">lang-ADVZ</ta>
            <ta e="T121" id="Seg_2598" s="T120">stehen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T123" id="Seg_2599" s="T121">man.muss</ta>
            <ta e="T132" id="Seg_2600" s="T131">Blut.[NOM]</ta>
            <ta e="T133" id="Seg_2601" s="T132">nehmen-PRS-2SG</ta>
            <ta e="T134" id="Seg_2602" s="T133">und</ta>
            <ta e="T147" id="Seg_2603" s="T329">kalt</ta>
            <ta e="T148" id="Seg_2604" s="T147">Ort-DAT/LOC</ta>
            <ta e="T149" id="Seg_2605" s="T148">dann</ta>
            <ta e="T152" id="Seg_2606" s="T151">kochen-PTCP.PRS</ta>
            <ta e="T153" id="Seg_2607" s="T152">bekommen-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T155" id="Seg_2608" s="T153">EMPH</ta>
            <ta e="T158" id="Seg_2609" s="T157">stehen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T159" id="Seg_2610" s="T158">man.muss</ta>
            <ta e="T160" id="Seg_2611" s="T159">Blut-EP-2SG.[NOM]</ta>
            <ta e="T161" id="Seg_2612" s="T160">nun</ta>
            <ta e="T162" id="Seg_2613" s="T161">damit</ta>
            <ta e="T163" id="Seg_2614" s="T162">Serum-VBZ-REFL-PTCP.FUT-3SG-ACC</ta>
            <ta e="T165" id="Seg_2615" s="T163">nun</ta>
            <ta e="T166" id="Seg_2616" s="T165">und</ta>
            <ta e="T167" id="Seg_2617" s="T166">Serum.[NOM]</ta>
            <ta e="T168" id="Seg_2618" s="T167">sammeln-PASS/REFL-PRS.[3SG]</ta>
            <ta e="T169" id="Seg_2619" s="T168">und</ta>
            <ta e="T170" id="Seg_2620" s="T169">jenes-ACC</ta>
            <ta e="T171" id="Seg_2621" s="T170">es.ist.möglich</ta>
            <ta e="T172" id="Seg_2622" s="T171">jenes-ACC</ta>
            <ta e="T173" id="Seg_2623" s="T172">doch</ta>
            <ta e="T174" id="Seg_2624" s="T173">jenes</ta>
            <ta e="T175" id="Seg_2625" s="T174">wer-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T176" id="Seg_2626" s="T175">EMPH</ta>
            <ta e="T177" id="Seg_2627" s="T176">solange</ta>
            <ta e="T178" id="Seg_2628" s="T177">Serum.[NOM]</ta>
            <ta e="T179" id="Seg_2629" s="T178">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T180" id="Seg_2630" s="T179">dieses.[NOM]</ta>
            <ta e="T181" id="Seg_2631" s="T180">jenes</ta>
            <ta e="T182" id="Seg_2632" s="T181">Labmagen.[NOM]</ta>
            <ta e="T183" id="Seg_2633" s="T182">Inneres-3SG-DAT/LOC</ta>
            <ta e="T184" id="Seg_2634" s="T183">legen-PRS-2SG</ta>
            <ta e="T185" id="Seg_2635" s="T184">EMPH</ta>
            <ta e="T186" id="Seg_2636" s="T185">gießen-PRS-2SG</ta>
            <ta e="T187" id="Seg_2637" s="T186">EMPH</ta>
            <ta e="T188" id="Seg_2638" s="T187">binden-PRS-2SG</ta>
            <ta e="T189" id="Seg_2639" s="T188">legen-PRS-2SG</ta>
            <ta e="T190" id="Seg_2640" s="T189">dann</ta>
            <ta e="T191" id="Seg_2641" s="T190">wieder</ta>
            <ta e="T192" id="Seg_2642" s="T191">Netzmagen.[NOM]</ta>
            <ta e="T193" id="Seg_2643" s="T192">es.gibt</ta>
            <ta e="T194" id="Seg_2644" s="T193">Netzmagen-PROPR</ta>
            <ta e="T196" id="Seg_2645" s="T195">Netzmagen.[NOM]</ta>
            <ta e="T197" id="Seg_2646" s="T196">Netzmagen.[NOM]</ta>
            <ta e="T198" id="Seg_2647" s="T197">jenes</ta>
            <ta e="T199" id="Seg_2648" s="T198">Inneres-3SG-DAT/LOC</ta>
            <ta e="T200" id="Seg_2649" s="T199">wieder</ta>
            <ta e="T201" id="Seg_2650" s="T200">es.ist.möglich</ta>
            <ta e="T202" id="Seg_2651" s="T201">gießen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T203" id="Seg_2652" s="T202">Blut-ACC</ta>
            <ta e="T204" id="Seg_2653" s="T203">Serum.[NOM]</ta>
            <ta e="T205" id="Seg_2654" s="T204">dort</ta>
            <ta e="T206" id="Seg_2655" s="T205">wer.[NOM]</ta>
            <ta e="T207" id="Seg_2656" s="T206">sein-HAB.[3SG]</ta>
            <ta e="T208" id="Seg_2657" s="T207">wenig</ta>
            <ta e="T209" id="Seg_2658" s="T208">Serum.[NOM]</ta>
            <ta e="T210" id="Seg_2659" s="T209">ein.kleines.Bisschen</ta>
            <ta e="T213" id="Seg_2660" s="T212">dies</ta>
            <ta e="T214" id="Seg_2661" s="T213">viel</ta>
            <ta e="T215" id="Seg_2662" s="T214">Rentier-ACC</ta>
            <ta e="T216" id="Seg_2663" s="T215">töten-TEMP-2SG</ta>
            <ta e="T217" id="Seg_2664" s="T216">Serum.[NOM]</ta>
            <ta e="T218" id="Seg_2665" s="T217">viel</ta>
            <ta e="T219" id="Seg_2666" s="T218">sein-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_2667" s="T219">viel</ta>
            <ta e="T221" id="Seg_2668" s="T220">sein-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_2669" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_2670" s="T222">dann</ta>
            <ta e="T224" id="Seg_2671" s="T223">jenes-ACC</ta>
            <ta e="T225" id="Seg_2672" s="T224">EMPH</ta>
            <ta e="T226" id="Seg_2673" s="T225">viel</ta>
            <ta e="T227" id="Seg_2674" s="T226">Wurst.[NOM]</ta>
            <ta e="T229" id="Seg_2675" s="T227">sein-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_2676" s="T230">Labmagen-3SG-DAT/LOC</ta>
            <ta e="T232" id="Seg_2677" s="T231">gießen-PRS-2SG</ta>
            <ta e="T233" id="Seg_2678" s="T232">dick</ta>
            <ta e="T234" id="Seg_2679" s="T233">dick</ta>
            <ta e="T235" id="Seg_2680" s="T234">dick</ta>
            <ta e="T236" id="Seg_2681" s="T235">Kot-3SG-DAT/LOC</ta>
            <ta e="T237" id="Seg_2682" s="T236">gießen-PRS-2SG</ta>
            <ta e="T238" id="Seg_2683" s="T237">Dünndarm-3SG-DAT/LOC</ta>
            <ta e="T239" id="Seg_2684" s="T238">nun</ta>
            <ta e="T243" id="Seg_2685" s="T241">Dünndarm</ta>
            <ta e="T244" id="Seg_2686" s="T243">solch-ACC</ta>
            <ta e="T245" id="Seg_2687" s="T244">gießen-PRS-2SG</ta>
            <ta e="T246" id="Seg_2688" s="T245">EMPH</ta>
            <ta e="T247" id="Seg_2689" s="T246">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T248" id="Seg_2690" s="T247">Wurst.[NOM]</ta>
            <ta e="T249" id="Seg_2691" s="T248">sein-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_2692" s="T249">schmackhaft</ta>
            <ta e="T251" id="Seg_2693" s="T250">sehr</ta>
            <ta e="T122" id="Seg_2694" s="T266">Zunge.[NOM]</ta>
            <ta e="T269" id="Seg_2695" s="T268">und</ta>
            <ta e="T270" id="Seg_2696" s="T269">Hüfte-DAT/LOC</ta>
            <ta e="T271" id="Seg_2697" s="T270">sein-HAB.[3SG]</ta>
            <ta e="T272" id="Seg_2698" s="T271">schmackhaft</ta>
            <ta e="T273" id="Seg_2699" s="T272">Fett.[NOM]</ta>
            <ta e="T274" id="Seg_2700" s="T273">Fett-PROPR</ta>
            <ta e="T275" id="Seg_2701" s="T274">Hüfte-DAT/LOC</ta>
            <ta e="T276" id="Seg_2702" s="T275">nun</ta>
            <ta e="T277" id="Seg_2703" s="T276">Fett.[NOM]</ta>
            <ta e="T278" id="Seg_2704" s="T277">sein-HAB.[3SG]</ta>
            <ta e="T279" id="Seg_2705" s="T278">Hüfte-DAT/LOC</ta>
            <ta e="T280" id="Seg_2706" s="T279">solch.[NOM]</ta>
            <ta e="T281" id="Seg_2707" s="T280">schmackhaft</ta>
            <ta e="T291" id="Seg_2708" s="T290">wer-VBZ-PRS-3PL</ta>
            <ta e="T305" id="Seg_2709" s="T304">wildes.Rentier-3SG-GEN</ta>
            <ta e="T306" id="Seg_2710" s="T305">wildes.Rentier-3SG-GEN</ta>
            <ta e="T307" id="Seg_2711" s="T306">Fleisch-3SG.[NOM]</ta>
            <ta e="T308" id="Seg_2712" s="T307">süß.[NOM]</ta>
            <ta e="T309" id="Seg_2713" s="T308">EMPH</ta>
            <ta e="T316" id="Seg_2714" s="T315">selbst-1PL-DAT/LOC</ta>
            <ta e="T317" id="Seg_2715" s="T316">Rentier.[NOM]</ta>
            <ta e="T318" id="Seg_2716" s="T317">Fleisch-3SG-ACC</ta>
            <ta e="T319" id="Seg_2717" s="T318">sehr</ta>
            <ta e="T320" id="Seg_2718" s="T319">so.viel-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T321" id="Seg_2719" s="T320">NEG-1SG</ta>
            <ta e="T322" id="Seg_2720" s="T321">wildes.Rentier-3SG-GEN</ta>
            <ta e="T323" id="Seg_2721" s="T322">Fleisch-3SG.[NOM]</ta>
            <ta e="T325" id="Seg_2722" s="T324">Rentier.[NOM]</ta>
            <ta e="T327" id="Seg_2723" s="T325">Fleisch-3SG-COMP</ta>
            <ta e="T328" id="Seg_2724" s="T327">schmackhaft</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2725" s="T0">олень.[NOM]</ta>
            <ta e="T2" id="Seg_2726" s="T1">кровь-3SG-INSTR</ta>
            <ta e="T3" id="Seg_2727" s="T2">однако</ta>
            <ta e="T4" id="Seg_2728" s="T3">кто-ACC</ta>
            <ta e="T5" id="Seg_2729" s="T4">делать-PRS-1PL</ta>
            <ta e="T6" id="Seg_2730" s="T5">колбаса.[NOM]</ta>
            <ta e="T7" id="Seg_2731" s="T6">вот</ta>
            <ta e="T8" id="Seg_2732" s="T7">тот.[NOM]</ta>
            <ta e="T9" id="Seg_2733" s="T8">однако</ta>
            <ta e="T10" id="Seg_2734" s="T9">кто.[NOM]</ta>
            <ta e="T11" id="Seg_2735" s="T10">олень-ACC</ta>
            <ta e="T12" id="Seg_2736" s="T11">убить-PRS-3PL</ta>
            <ta e="T13" id="Seg_2737" s="T12">EMPH</ta>
            <ta e="T14" id="Seg_2738" s="T13">так</ta>
            <ta e="T15" id="Seg_2739" s="T14">делать-CVB.SEQ</ta>
            <ta e="T16" id="Seg_2740" s="T15">идти-CVB.SEQ</ta>
            <ta e="T17" id="Seg_2741" s="T16">тазик-DAT/LOC</ta>
            <ta e="T18" id="Seg_2742" s="T17">кровь.[NOM]</ta>
            <ta e="T19" id="Seg_2743" s="T18">взять-PRS-1PL</ta>
            <ta e="T20" id="Seg_2744" s="T19">и</ta>
            <ta e="T21" id="Seg_2745" s="T20">тот-ACC</ta>
            <ta e="T22" id="Seg_2746" s="T21">однако</ta>
            <ta e="T23" id="Seg_2747" s="T22">стоять-CAUS-PRS-1PL</ta>
            <ta e="T24" id="Seg_2748" s="T23">EMPH</ta>
            <ta e="T25" id="Seg_2749" s="T24">чтобы</ta>
            <ta e="T26" id="Seg_2750" s="T25">сыворотка.[NOM]</ta>
            <ta e="T27" id="Seg_2751" s="T26">становиться-PTCP.FUT-3SG-ACC</ta>
            <ta e="T28" id="Seg_2752" s="T27">вот</ta>
            <ta e="T29" id="Seg_2753" s="T28">сыворотка-ACC</ta>
            <ta e="T30" id="Seg_2754" s="T29">кто-VBZ-CVB.SIM</ta>
            <ta e="T31" id="Seg_2755" s="T30">мять-VBZ-PRS-1PL</ta>
            <ta e="T32" id="Seg_2756" s="T31">мять-VBZ-PRS-1PL</ta>
            <ta e="T33" id="Seg_2757" s="T32">сыворотка.[NOM]</ta>
            <ta e="T34" id="Seg_2758" s="T33">сыворотка.[NOM]</ta>
            <ta e="T35" id="Seg_2759" s="T34">быть-PRS.[3SG]</ta>
            <ta e="T36" id="Seg_2760" s="T35">тот-3SG-2SG.[NOM]</ta>
            <ta e="T37" id="Seg_2761" s="T36">и</ta>
            <ta e="T38" id="Seg_2762" s="T37">тот-ACC</ta>
            <ta e="T39" id="Seg_2763" s="T38">однако</ta>
            <ta e="T40" id="Seg_2764" s="T39">внутренность-PL-ACC</ta>
            <ta e="T41" id="Seg_2765" s="T40">взять-PRS-1PL</ta>
            <ta e="T45" id="Seg_2766" s="T44">потом</ta>
            <ta e="T46" id="Seg_2767" s="T45">кто-ACC</ta>
            <ta e="T47" id="Seg_2768" s="T46">толстый</ta>
            <ta e="T48" id="Seg_2769" s="T47">кал-3SG.[NOM]</ta>
            <ta e="T51" id="Seg_2770" s="T50">толстый</ta>
            <ta e="T52" id="Seg_2771" s="T51">кал-3SG-ACC</ta>
            <ta e="T53" id="Seg_2772" s="T52">взять-PRS-2SG</ta>
            <ta e="T54" id="Seg_2773" s="T53">EMPH</ta>
            <ta e="T55" id="Seg_2774" s="T54">сычуг-3SG-ACC</ta>
            <ta e="T56" id="Seg_2775" s="T55">взять-PRS-2SG</ta>
            <ta e="T57" id="Seg_2776" s="T56">EMPH</ta>
            <ta e="T58" id="Seg_2777" s="T57">кишка-3SG-ABL</ta>
            <ta e="T59" id="Seg_2778" s="T58">опять</ta>
            <ta e="T60" id="Seg_2779" s="T59">взять-PRS-2SG</ta>
            <ta e="T61" id="Seg_2780" s="T60">так</ta>
            <ta e="T62" id="Seg_2781" s="T61">целый-INTNS.[NOM]</ta>
            <ta e="T63" id="Seg_2782" s="T62">обмыть-CVB.SEQ</ta>
            <ta e="T64" id="Seg_2783" s="T63">обмыть-CVB.SEQ</ta>
            <ta e="T65" id="Seg_2784" s="T64">и.так.далее-CVB.SEQ</ta>
            <ta e="T66" id="Seg_2785" s="T65">идти-CVB.SEQ</ta>
            <ta e="T67" id="Seg_2786" s="T66">обмыть-MULT-PRS-2SG</ta>
            <ta e="T68" id="Seg_2787" s="T67">так</ta>
            <ta e="T69" id="Seg_2788" s="T68">делать-CVB.SEQ</ta>
            <ta e="T70" id="Seg_2789" s="T69">идти-CVB.SEQ</ta>
            <ta e="T71" id="Seg_2790" s="T70">кровь-EP-2SG.[NOM]</ta>
            <ta e="T72" id="Seg_2791" s="T71">однако</ta>
            <ta e="T73" id="Seg_2792" s="T72">сыворотка.[NOM]</ta>
            <ta e="T78" id="Seg_2793" s="T77">очень</ta>
            <ta e="T79" id="Seg_2794" s="T78">вода.[NOM]</ta>
            <ta e="T80" id="Seg_2795" s="T79">кровь.[NOM]</ta>
            <ta e="T81" id="Seg_2796" s="T80">быть-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_2797" s="T81">и</ta>
            <ta e="T83" id="Seg_2798" s="T82">тот-ACC</ta>
            <ta e="T84" id="Seg_2799" s="T83">однако</ta>
            <ta e="T85" id="Seg_2800" s="T84">кто-DAT/LOC</ta>
            <ta e="T86" id="Seg_2801" s="T85">лить-PRS-2SG</ta>
            <ta e="T87" id="Seg_2802" s="T86">тот</ta>
            <ta e="T88" id="Seg_2803" s="T87">сычуг-PL-DAT/LOC</ta>
            <ta e="T89" id="Seg_2804" s="T88">лить-PRS-2SG</ta>
            <ta e="T90" id="Seg_2805" s="T89">тонкая.кишка-PL-DAT/LOC</ta>
            <ta e="T91" id="Seg_2806" s="T90">лить-PRS-2SG</ta>
            <ta e="T92" id="Seg_2807" s="T91">связывать-PRS-2SG</ta>
            <ta e="T93" id="Seg_2808" s="T92">и</ta>
            <ta e="T94" id="Seg_2809" s="T93">кто.[NOM]</ta>
            <ta e="T95" id="Seg_2810" s="T94">кто-DAT/LOC</ta>
            <ta e="T96" id="Seg_2811" s="T95">вариться-CAUS-PRS-2SG</ta>
            <ta e="T97" id="Seg_2812" s="T96">теплый</ta>
            <ta e="T98" id="Seg_2813" s="T97">вода-DAT/LOC</ta>
            <ta e="T99" id="Seg_2814" s="T98">класть-PRS-2SG</ta>
            <ta e="T100" id="Seg_2815" s="T99">и</ta>
            <ta e="T104" id="Seg_2816" s="T103">вариться-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_2817" s="T104">тот-3SG-2SG.[NOM]</ta>
            <ta e="T106" id="Seg_2818" s="T105">и</ta>
            <ta e="T107" id="Seg_2819" s="T106">вынимать-PRS-2SG</ta>
            <ta e="T108" id="Seg_2820" s="T107">и</ta>
            <ta e="T109" id="Seg_2821" s="T108">колбаса.[NOM]</ta>
            <ta e="T110" id="Seg_2822" s="T109">быть-PRS.[3SG]</ta>
            <ta e="T112" id="Seg_2823" s="T111">быстрый-ADVZ</ta>
            <ta e="T113" id="Seg_2824" s="T112">AFFIRM</ta>
            <ta e="T114" id="Seg_2825" s="T113">быстрый</ta>
            <ta e="T115" id="Seg_2826" s="T114">очень</ta>
            <ta e="T116" id="Seg_2827" s="T115">вот</ta>
            <ta e="T117" id="Seg_2828" s="T116">это</ta>
            <ta e="T118" id="Seg_2829" s="T117">EMPH</ta>
            <ta e="T119" id="Seg_2830" s="T118">кровь-EP-2SG.[NOM]</ta>
            <ta e="T120" id="Seg_2831" s="T119">длинный-ADVZ</ta>
            <ta e="T121" id="Seg_2832" s="T120">стоять-PTCP.FUT-3SG-ACC</ta>
            <ta e="T123" id="Seg_2833" s="T121">надо</ta>
            <ta e="T132" id="Seg_2834" s="T131">кровь.[NOM]</ta>
            <ta e="T133" id="Seg_2835" s="T132">взять-PRS-2SG</ta>
            <ta e="T134" id="Seg_2836" s="T133">да</ta>
            <ta e="T147" id="Seg_2837" s="T329">холодний</ta>
            <ta e="T148" id="Seg_2838" s="T147">место-DAT/LOC</ta>
            <ta e="T149" id="Seg_2839" s="T148">потом</ta>
            <ta e="T152" id="Seg_2840" s="T151">вариться-PTCP.PRS</ta>
            <ta e="T153" id="Seg_2841" s="T152">получить-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T155" id="Seg_2842" s="T153">EMPH</ta>
            <ta e="T158" id="Seg_2843" s="T157">стоять-PTCP.FUT-3SG-ACC</ta>
            <ta e="T159" id="Seg_2844" s="T158">надо</ta>
            <ta e="T160" id="Seg_2845" s="T159">кровь-EP-2SG.[NOM]</ta>
            <ta e="T161" id="Seg_2846" s="T160">вот</ta>
            <ta e="T162" id="Seg_2847" s="T161">чтобы</ta>
            <ta e="T163" id="Seg_2848" s="T162">сыворотка-VBZ-REFL-PTCP.FUT-3SG-ACC</ta>
            <ta e="T165" id="Seg_2849" s="T163">вот</ta>
            <ta e="T166" id="Seg_2850" s="T165">а</ta>
            <ta e="T167" id="Seg_2851" s="T166">сыворотка.[NOM]</ta>
            <ta e="T168" id="Seg_2852" s="T167">собирать-PASS/REFL-PRS.[3SG]</ta>
            <ta e="T169" id="Seg_2853" s="T168">и</ta>
            <ta e="T170" id="Seg_2854" s="T169">тот-ACC</ta>
            <ta e="T171" id="Seg_2855" s="T170">можно</ta>
            <ta e="T172" id="Seg_2856" s="T171">тот-ACC</ta>
            <ta e="T173" id="Seg_2857" s="T172">вот</ta>
            <ta e="T174" id="Seg_2858" s="T173">тот</ta>
            <ta e="T175" id="Seg_2859" s="T174">кто-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T176" id="Seg_2860" s="T175">EMPH</ta>
            <ta e="T177" id="Seg_2861" s="T176">пока</ta>
            <ta e="T178" id="Seg_2862" s="T177">сыворотка.[NOM]</ta>
            <ta e="T179" id="Seg_2863" s="T178">тот-3SG-2SG.[NOM]</ta>
            <ta e="T180" id="Seg_2864" s="T179">тот.[NOM]</ta>
            <ta e="T181" id="Seg_2865" s="T180">тот</ta>
            <ta e="T182" id="Seg_2866" s="T181">сычуг.[NOM]</ta>
            <ta e="T183" id="Seg_2867" s="T182">нутро-3SG-DAT/LOC</ta>
            <ta e="T184" id="Seg_2868" s="T183">класть-PRS-2SG</ta>
            <ta e="T185" id="Seg_2869" s="T184">EMPH</ta>
            <ta e="T186" id="Seg_2870" s="T185">лить-PRS-2SG</ta>
            <ta e="T187" id="Seg_2871" s="T186">EMPH</ta>
            <ta e="T188" id="Seg_2872" s="T187">связывать-PRS-2SG</ta>
            <ta e="T189" id="Seg_2873" s="T188">класть-PRS-2SG</ta>
            <ta e="T190" id="Seg_2874" s="T189">потом</ta>
            <ta e="T191" id="Seg_2875" s="T190">опять</ta>
            <ta e="T192" id="Seg_2876" s="T191">сетка.[NOM]</ta>
            <ta e="T193" id="Seg_2877" s="T192">есть</ta>
            <ta e="T194" id="Seg_2878" s="T193">сетка-PROPR</ta>
            <ta e="T196" id="Seg_2879" s="T195">сетка.[NOM]</ta>
            <ta e="T197" id="Seg_2880" s="T196">сетка.[NOM]</ta>
            <ta e="T198" id="Seg_2881" s="T197">тот</ta>
            <ta e="T199" id="Seg_2882" s="T198">нутро-3SG-DAT/LOC</ta>
            <ta e="T200" id="Seg_2883" s="T199">опять</ta>
            <ta e="T201" id="Seg_2884" s="T200">можно</ta>
            <ta e="T202" id="Seg_2885" s="T201">лить-PTCP.FUT-2SG-ACC</ta>
            <ta e="T203" id="Seg_2886" s="T202">кровь-ACC</ta>
            <ta e="T204" id="Seg_2887" s="T203">сыворотка.[NOM]</ta>
            <ta e="T205" id="Seg_2888" s="T204">там</ta>
            <ta e="T206" id="Seg_2889" s="T205">кто.[NOM]</ta>
            <ta e="T207" id="Seg_2890" s="T206">быть-HAB.[3SG]</ta>
            <ta e="T208" id="Seg_2891" s="T207">мало</ta>
            <ta e="T209" id="Seg_2892" s="T208">сыворотка.[NOM]</ta>
            <ta e="T210" id="Seg_2893" s="T209">немножко</ta>
            <ta e="T213" id="Seg_2894" s="T212">это</ta>
            <ta e="T214" id="Seg_2895" s="T213">много</ta>
            <ta e="T215" id="Seg_2896" s="T214">олень-ACC</ta>
            <ta e="T216" id="Seg_2897" s="T215">убить-TEMP-2SG</ta>
            <ta e="T217" id="Seg_2898" s="T216">сыворотка.[NOM]</ta>
            <ta e="T218" id="Seg_2899" s="T217">много</ta>
            <ta e="T219" id="Seg_2900" s="T218">быть-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_2901" s="T219">много</ta>
            <ta e="T221" id="Seg_2902" s="T220">быть-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_2903" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_2904" s="T222">тогда</ta>
            <ta e="T224" id="Seg_2905" s="T223">тот-ACC</ta>
            <ta e="T225" id="Seg_2906" s="T224">EMPH</ta>
            <ta e="T226" id="Seg_2907" s="T225">много</ta>
            <ta e="T227" id="Seg_2908" s="T226">колбаса.[NOM]</ta>
            <ta e="T229" id="Seg_2909" s="T227">быть-PRS.[3SG]</ta>
            <ta e="T231" id="Seg_2910" s="T230">сычуг-3SG-DAT/LOC</ta>
            <ta e="T232" id="Seg_2911" s="T231">лить-PRS-2SG</ta>
            <ta e="T233" id="Seg_2912" s="T232">толстый</ta>
            <ta e="T234" id="Seg_2913" s="T233">толстый</ta>
            <ta e="T235" id="Seg_2914" s="T234">толстый</ta>
            <ta e="T236" id="Seg_2915" s="T235">кал-3SG-DAT/LOC</ta>
            <ta e="T237" id="Seg_2916" s="T236">лить-PRS-2SG</ta>
            <ta e="T238" id="Seg_2917" s="T237">тонкая.кишка-3SG-DAT/LOC</ta>
            <ta e="T239" id="Seg_2918" s="T238">вот</ta>
            <ta e="T243" id="Seg_2919" s="T241">тонкая.кишка</ta>
            <ta e="T244" id="Seg_2920" s="T243">такой-ACC</ta>
            <ta e="T245" id="Seg_2921" s="T244">лить-PRS-2SG</ta>
            <ta e="T246" id="Seg_2922" s="T245">EMPH</ta>
            <ta e="T247" id="Seg_2923" s="T246">тот-3SG-2SG.[NOM]</ta>
            <ta e="T248" id="Seg_2924" s="T247">колбаса.[NOM]</ta>
            <ta e="T249" id="Seg_2925" s="T248">быть-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_2926" s="T249">вкусный</ta>
            <ta e="T251" id="Seg_2927" s="T250">очень</ta>
            <ta e="T122" id="Seg_2928" s="T266">язык.[NOM]</ta>
            <ta e="T269" id="Seg_2929" s="T268">и</ta>
            <ta e="T270" id="Seg_2930" s="T269">бедро-DAT/LOC</ta>
            <ta e="T271" id="Seg_2931" s="T270">быть-HAB.[3SG]</ta>
            <ta e="T272" id="Seg_2932" s="T271">вкусный</ta>
            <ta e="T273" id="Seg_2933" s="T272">сало.[NOM]</ta>
            <ta e="T274" id="Seg_2934" s="T273">сало-PROPR</ta>
            <ta e="T275" id="Seg_2935" s="T274">бедро-DAT/LOC</ta>
            <ta e="T276" id="Seg_2936" s="T275">вот</ta>
            <ta e="T277" id="Seg_2937" s="T276">сало.[NOM]</ta>
            <ta e="T278" id="Seg_2938" s="T277">быть-HAB.[3SG]</ta>
            <ta e="T279" id="Seg_2939" s="T278">бедро-DAT/LOC</ta>
            <ta e="T280" id="Seg_2940" s="T279">такой.[NOM]</ta>
            <ta e="T281" id="Seg_2941" s="T280">вкусный</ta>
            <ta e="T291" id="Seg_2942" s="T290">кто-VBZ-PRS-3PL</ta>
            <ta e="T305" id="Seg_2943" s="T304">дикий.олень-3SG-GEN</ta>
            <ta e="T306" id="Seg_2944" s="T305">дикий.олень-3SG-GEN</ta>
            <ta e="T307" id="Seg_2945" s="T306">мясо-3SG.[NOM]</ta>
            <ta e="T308" id="Seg_2946" s="T307">сладкий.[NOM]</ta>
            <ta e="T309" id="Seg_2947" s="T308">EMPH</ta>
            <ta e="T316" id="Seg_2948" s="T315">сам-1PL-DAT/LOC</ta>
            <ta e="T317" id="Seg_2949" s="T316">олень.[NOM]</ta>
            <ta e="T318" id="Seg_2950" s="T317">мясо-3SG-ACC</ta>
            <ta e="T319" id="Seg_2951" s="T318">очень</ta>
            <ta e="T320" id="Seg_2952" s="T319">столько-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T321" id="Seg_2953" s="T320">NEG-1SG</ta>
            <ta e="T322" id="Seg_2954" s="T321">дикий.олень-3SG-GEN</ta>
            <ta e="T323" id="Seg_2955" s="T322">мясо-3SG.[NOM]</ta>
            <ta e="T325" id="Seg_2956" s="T324">олень.[NOM]</ta>
            <ta e="T327" id="Seg_2957" s="T325">мясо-3SG-COMP</ta>
            <ta e="T328" id="Seg_2958" s="T327">вкусный</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2959" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_2960" s="T1">n-n:poss-n:case</ta>
            <ta e="T3" id="Seg_2961" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_2962" s="T3">que-pro:case</ta>
            <ta e="T5" id="Seg_2963" s="T4">v-v:tense-v:pred.pn</ta>
            <ta e="T6" id="Seg_2964" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_2965" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_2966" s="T7">dempro-pro:case</ta>
            <ta e="T9" id="Seg_2967" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_2968" s="T9">que-pro:case</ta>
            <ta e="T11" id="Seg_2969" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_2970" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_2971" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_2972" s="T13">adv</ta>
            <ta e="T15" id="Seg_2973" s="T14">v-v:cvb</ta>
            <ta e="T16" id="Seg_2974" s="T15">v-v:cvb</ta>
            <ta e="T17" id="Seg_2975" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_2976" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_2977" s="T18">v-v:tense-v:pred.pn</ta>
            <ta e="T20" id="Seg_2978" s="T19">conj</ta>
            <ta e="T21" id="Seg_2979" s="T20">dempro-pro:case</ta>
            <ta e="T22" id="Seg_2980" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_2981" s="T22">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_2982" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_2983" s="T24">conj</ta>
            <ta e="T26" id="Seg_2984" s="T25">n.[n:case]</ta>
            <ta e="T27" id="Seg_2985" s="T26">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T28" id="Seg_2986" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_2987" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_2988" s="T29">que-que&gt;v-v:cvb</ta>
            <ta e="T31" id="Seg_2989" s="T30">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_2990" s="T31">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T33" id="Seg_2991" s="T32">n.[n:case]</ta>
            <ta e="T34" id="Seg_2992" s="T33">n.[n:case]</ta>
            <ta e="T35" id="Seg_2993" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_2994" s="T35">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T37" id="Seg_2995" s="T36">conj</ta>
            <ta e="T38" id="Seg_2996" s="T37">dempro-pro:case</ta>
            <ta e="T39" id="Seg_2997" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_2998" s="T39">n-n:(num)-n:case</ta>
            <ta e="T41" id="Seg_2999" s="T40">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_3000" s="T44">adv</ta>
            <ta e="T46" id="Seg_3001" s="T45">que-pro:case</ta>
            <ta e="T47" id="Seg_3002" s="T46">adj</ta>
            <ta e="T48" id="Seg_3003" s="T47">n-n:(poss)-n:case</ta>
            <ta e="T51" id="Seg_3004" s="T50">adj</ta>
            <ta e="T52" id="Seg_3005" s="T51">n-n:poss-n:case</ta>
            <ta e="T53" id="Seg_3006" s="T52">v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_3007" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_3008" s="T54">n-n:poss-n:case</ta>
            <ta e="T56" id="Seg_3009" s="T55">v-v:tense-v:pred.pn</ta>
            <ta e="T57" id="Seg_3010" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_3011" s="T57">n-n:poss-n:case</ta>
            <ta e="T59" id="Seg_3012" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_3013" s="T59">v-v:tense-v:pred.pn</ta>
            <ta e="T61" id="Seg_3014" s="T60">adv</ta>
            <ta e="T62" id="Seg_3015" s="T61">adj-adj&gt;adj.[n:case]</ta>
            <ta e="T63" id="Seg_3016" s="T62">v-v:cvb</ta>
            <ta e="T64" id="Seg_3017" s="T63">v-v:cvb</ta>
            <ta e="T65" id="Seg_3018" s="T64">v-v:cvb</ta>
            <ta e="T66" id="Seg_3019" s="T65">v-v:cvb</ta>
            <ta e="T67" id="Seg_3020" s="T66">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T68" id="Seg_3021" s="T67">adv</ta>
            <ta e="T69" id="Seg_3022" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_3023" s="T69">v-v:cvb</ta>
            <ta e="T71" id="Seg_3024" s="T70">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T72" id="Seg_3025" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_3026" s="T72">n-n:case</ta>
            <ta e="T78" id="Seg_3027" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_3028" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_3029" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_3030" s="T80">v-v:tense-v:pred.pn</ta>
            <ta e="T82" id="Seg_3031" s="T81">conj</ta>
            <ta e="T83" id="Seg_3032" s="T82">dempro-pro:case</ta>
            <ta e="T84" id="Seg_3033" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_3034" s="T84">que-pro:case</ta>
            <ta e="T86" id="Seg_3035" s="T85">v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_3036" s="T86">dempro</ta>
            <ta e="T88" id="Seg_3037" s="T87">n-n:(num)-n:case</ta>
            <ta e="T89" id="Seg_3038" s="T88">v-v:tense-v:pred.pn</ta>
            <ta e="T90" id="Seg_3039" s="T89">n-n:(num)-n:case</ta>
            <ta e="T91" id="Seg_3040" s="T90">v-v:tense-v:pred.pn</ta>
            <ta e="T92" id="Seg_3041" s="T91">v-v:tense-v:pred.pn</ta>
            <ta e="T93" id="Seg_3042" s="T92">conj</ta>
            <ta e="T94" id="Seg_3043" s="T93">que-pro:case</ta>
            <ta e="T95" id="Seg_3044" s="T94">que-pro:case</ta>
            <ta e="T96" id="Seg_3045" s="T95">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_3046" s="T96">adj</ta>
            <ta e="T98" id="Seg_3047" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_3048" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_3049" s="T99">conj</ta>
            <ta e="T104" id="Seg_3050" s="T103">v-v:tense-v:pred.pn</ta>
            <ta e="T105" id="Seg_3051" s="T104">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T106" id="Seg_3052" s="T105">conj</ta>
            <ta e="T107" id="Seg_3053" s="T106">v-v:tense-v:pred.pn</ta>
            <ta e="T108" id="Seg_3054" s="T107">conj</ta>
            <ta e="T109" id="Seg_3055" s="T108">n.[n:case]</ta>
            <ta e="T110" id="Seg_3056" s="T109">v-v:tense-v:pred.pn</ta>
            <ta e="T112" id="Seg_3057" s="T111">adj-adj&gt;adv</ta>
            <ta e="T113" id="Seg_3058" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_3059" s="T113">adj</ta>
            <ta e="T115" id="Seg_3060" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_3061" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_3062" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_3063" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_3064" s="T118">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T120" id="Seg_3065" s="T119">adj-adj&gt;adv</ta>
            <ta e="T121" id="Seg_3066" s="T120">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T123" id="Seg_3067" s="T121">ptcl</ta>
            <ta e="T132" id="Seg_3068" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_3069" s="T132">v-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_3070" s="T133">conj</ta>
            <ta e="T147" id="Seg_3071" s="T329">adj</ta>
            <ta e="T148" id="Seg_3072" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_3073" s="T148">adv</ta>
            <ta e="T152" id="Seg_3074" s="T151">v-v:ptcp</ta>
            <ta e="T153" id="Seg_3075" s="T152">v-v&gt;v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T155" id="Seg_3076" s="T153">ptcl</ta>
            <ta e="T158" id="Seg_3077" s="T157">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T159" id="Seg_3078" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_3079" s="T159">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T161" id="Seg_3080" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_3081" s="T161">conj</ta>
            <ta e="T163" id="Seg_3082" s="T162">n-n&gt;v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T165" id="Seg_3083" s="T163">ptcl</ta>
            <ta e="T166" id="Seg_3084" s="T165">conj</ta>
            <ta e="T167" id="Seg_3085" s="T166">n.[n:case]</ta>
            <ta e="T168" id="Seg_3086" s="T167">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T169" id="Seg_3087" s="T168">conj</ta>
            <ta e="T170" id="Seg_3088" s="T169">dempro-pro:case</ta>
            <ta e="T171" id="Seg_3089" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_3090" s="T171">dempro-pro:case</ta>
            <ta e="T173" id="Seg_3091" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_3092" s="T173">dempro</ta>
            <ta e="T175" id="Seg_3093" s="T174">que-que&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T176" id="Seg_3094" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_3095" s="T176">conj</ta>
            <ta e="T178" id="Seg_3096" s="T177">n.[n:case]</ta>
            <ta e="T179" id="Seg_3097" s="T178">dempro-n:poss-n:(poss)-n:case</ta>
            <ta e="T180" id="Seg_3098" s="T179">dempro-pro:case</ta>
            <ta e="T181" id="Seg_3099" s="T180">dempro</ta>
            <ta e="T182" id="Seg_3100" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_3101" s="T182">n-n:poss-n:case</ta>
            <ta e="T184" id="Seg_3102" s="T183">v-v:tense-v:pred.pn</ta>
            <ta e="T185" id="Seg_3103" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_3104" s="T185">v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_3105" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_3106" s="T187">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_3107" s="T188">v-v:tense-v:pred.pn</ta>
            <ta e="T190" id="Seg_3108" s="T189">adv</ta>
            <ta e="T191" id="Seg_3109" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_3110" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_3111" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_3112" s="T193">n-n&gt;adj</ta>
            <ta e="T196" id="Seg_3113" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_3114" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_3115" s="T197">dempro</ta>
            <ta e="T199" id="Seg_3116" s="T198">n-n:poss-n:case</ta>
            <ta e="T200" id="Seg_3117" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_3118" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_3119" s="T201">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T203" id="Seg_3120" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_3121" s="T203">n.[n:case]</ta>
            <ta e="T205" id="Seg_3122" s="T204">adv</ta>
            <ta e="T206" id="Seg_3123" s="T205">que-pro:case</ta>
            <ta e="T207" id="Seg_3124" s="T206">v-v:mood-v:pred.pn</ta>
            <ta e="T208" id="Seg_3125" s="T207">quant</ta>
            <ta e="T209" id="Seg_3126" s="T208">n.[n:case]</ta>
            <ta e="T210" id="Seg_3127" s="T209">adv</ta>
            <ta e="T213" id="Seg_3128" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_3129" s="T213">quant</ta>
            <ta e="T215" id="Seg_3130" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_3131" s="T215">v-v:mood-v:temp.pn</ta>
            <ta e="T217" id="Seg_3132" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_3133" s="T217">quant</ta>
            <ta e="T219" id="Seg_3134" s="T218">v-v:tense-v:pred.pn</ta>
            <ta e="T220" id="Seg_3135" s="T219">quant</ta>
            <ta e="T221" id="Seg_3136" s="T220">v-v:tense-v:pred.pn</ta>
            <ta e="T222" id="Seg_3137" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_3138" s="T222">adv</ta>
            <ta e="T224" id="Seg_3139" s="T223">dempro-pro:case</ta>
            <ta e="T225" id="Seg_3140" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_3141" s="T225">quant</ta>
            <ta e="T227" id="Seg_3142" s="T226">n-n:case</ta>
            <ta e="T229" id="Seg_3143" s="T227">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_3144" s="T230">n-n:poss-n:case</ta>
            <ta e="T232" id="Seg_3145" s="T231">v-v:tense-v:pred.pn</ta>
            <ta e="T233" id="Seg_3146" s="T232">adj</ta>
            <ta e="T234" id="Seg_3147" s="T233">adj</ta>
            <ta e="T235" id="Seg_3148" s="T234">adj</ta>
            <ta e="T236" id="Seg_3149" s="T235">n-n:poss-n:case</ta>
            <ta e="T237" id="Seg_3150" s="T236">v-v:tense-v:pred.pn</ta>
            <ta e="T238" id="Seg_3151" s="T237">n-n:poss-n:case</ta>
            <ta e="T239" id="Seg_3152" s="T238">ptcl</ta>
            <ta e="T243" id="Seg_3153" s="T241">n</ta>
            <ta e="T244" id="Seg_3154" s="T243">dempro-pro:case</ta>
            <ta e="T245" id="Seg_3155" s="T244">v-v:tense-v:pred.pn</ta>
            <ta e="T246" id="Seg_3156" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_3157" s="T246">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T248" id="Seg_3158" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_3159" s="T248">v-v:tense-v:pred.pn</ta>
            <ta e="T250" id="Seg_3160" s="T249">adj</ta>
            <ta e="T251" id="Seg_3161" s="T250">ptcl</ta>
            <ta e="T122" id="Seg_3162" s="T266">n-n:case</ta>
            <ta e="T269" id="Seg_3163" s="T268">conj</ta>
            <ta e="T270" id="Seg_3164" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_3165" s="T270">v-v:mood-v:pred.pn</ta>
            <ta e="T272" id="Seg_3166" s="T271">adj</ta>
            <ta e="T273" id="Seg_3167" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_3168" s="T273">n-n&gt;adj</ta>
            <ta e="T275" id="Seg_3169" s="T274">n-n:case</ta>
            <ta e="T276" id="Seg_3170" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_3171" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_3172" s="T277">v-v:mood-v:pred.pn</ta>
            <ta e="T279" id="Seg_3173" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_3174" s="T279">dempro.[pro:case]</ta>
            <ta e="T281" id="Seg_3175" s="T280">adj</ta>
            <ta e="T291" id="Seg_3176" s="T290">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T305" id="Seg_3177" s="T304">n-n:poss-n:case</ta>
            <ta e="T306" id="Seg_3178" s="T305">n-n:poss-n:case</ta>
            <ta e="T307" id="Seg_3179" s="T306">n-n:(poss).[n:case]</ta>
            <ta e="T308" id="Seg_3180" s="T307">adj.[n:case]</ta>
            <ta e="T309" id="Seg_3181" s="T308">ptcl</ta>
            <ta e="T316" id="Seg_3182" s="T315">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T317" id="Seg_3183" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_3184" s="T317">n-n:poss-n:case</ta>
            <ta e="T319" id="Seg_3185" s="T318">adv</ta>
            <ta e="T320" id="Seg_3186" s="T319">adv-adv&gt;v-v:ptcp-v:(poss)</ta>
            <ta e="T321" id="Seg_3187" s="T320">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T322" id="Seg_3188" s="T321">n-n:poss-n:case</ta>
            <ta e="T323" id="Seg_3189" s="T322">n-n:(poss)-n:case</ta>
            <ta e="T325" id="Seg_3190" s="T324">n-n:case</ta>
            <ta e="T327" id="Seg_3191" s="T325">n-n:poss-n:case</ta>
            <ta e="T328" id="Seg_3192" s="T327">adj</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3193" s="T0">n</ta>
            <ta e="T2" id="Seg_3194" s="T1">n</ta>
            <ta e="T3" id="Seg_3195" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_3196" s="T3">que</ta>
            <ta e="T5" id="Seg_3197" s="T4">v</ta>
            <ta e="T6" id="Seg_3198" s="T5">n</ta>
            <ta e="T7" id="Seg_3199" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_3200" s="T7">dempro</ta>
            <ta e="T9" id="Seg_3201" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_3202" s="T9">que</ta>
            <ta e="T11" id="Seg_3203" s="T10">n</ta>
            <ta e="T12" id="Seg_3204" s="T11">v</ta>
            <ta e="T13" id="Seg_3205" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_3206" s="T13">adv</ta>
            <ta e="T15" id="Seg_3207" s="T14">v</ta>
            <ta e="T16" id="Seg_3208" s="T15">post</ta>
            <ta e="T17" id="Seg_3209" s="T16">n</ta>
            <ta e="T18" id="Seg_3210" s="T17">n</ta>
            <ta e="T19" id="Seg_3211" s="T18">v</ta>
            <ta e="T20" id="Seg_3212" s="T19">conj</ta>
            <ta e="T21" id="Seg_3213" s="T20">dempro</ta>
            <ta e="T22" id="Seg_3214" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_3215" s="T22">v</ta>
            <ta e="T24" id="Seg_3216" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_3217" s="T24">conj</ta>
            <ta e="T26" id="Seg_3218" s="T25">n</ta>
            <ta e="T27" id="Seg_3219" s="T26">cop</ta>
            <ta e="T28" id="Seg_3220" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_3221" s="T28">n</ta>
            <ta e="T30" id="Seg_3222" s="T29">v</ta>
            <ta e="T31" id="Seg_3223" s="T30">v</ta>
            <ta e="T32" id="Seg_3224" s="T31">v</ta>
            <ta e="T33" id="Seg_3225" s="T32">n</ta>
            <ta e="T34" id="Seg_3226" s="T33">n</ta>
            <ta e="T35" id="Seg_3227" s="T34">cop</ta>
            <ta e="T36" id="Seg_3228" s="T35">dempro</ta>
            <ta e="T37" id="Seg_3229" s="T36">conj</ta>
            <ta e="T38" id="Seg_3230" s="T37">dempro</ta>
            <ta e="T39" id="Seg_3231" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_3232" s="T39">n</ta>
            <ta e="T41" id="Seg_3233" s="T40">v</ta>
            <ta e="T45" id="Seg_3234" s="T44">adv</ta>
            <ta e="T46" id="Seg_3235" s="T45">que</ta>
            <ta e="T47" id="Seg_3236" s="T46">adj</ta>
            <ta e="T48" id="Seg_3237" s="T47">n</ta>
            <ta e="T51" id="Seg_3238" s="T50">adj</ta>
            <ta e="T52" id="Seg_3239" s="T51">n</ta>
            <ta e="T53" id="Seg_3240" s="T52">v</ta>
            <ta e="T54" id="Seg_3241" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_3242" s="T54">n</ta>
            <ta e="T56" id="Seg_3243" s="T55">v</ta>
            <ta e="T57" id="Seg_3244" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_3245" s="T57">n</ta>
            <ta e="T59" id="Seg_3246" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_3247" s="T59">v</ta>
            <ta e="T61" id="Seg_3248" s="T60">adv</ta>
            <ta e="T62" id="Seg_3249" s="T61">adj</ta>
            <ta e="T63" id="Seg_3250" s="T62">v</ta>
            <ta e="T64" id="Seg_3251" s="T63">v</ta>
            <ta e="T65" id="Seg_3252" s="T64">v</ta>
            <ta e="T66" id="Seg_3253" s="T65">post</ta>
            <ta e="T67" id="Seg_3254" s="T66">v</ta>
            <ta e="T68" id="Seg_3255" s="T67">adv</ta>
            <ta e="T69" id="Seg_3256" s="T68">v</ta>
            <ta e="T70" id="Seg_3257" s="T69">post</ta>
            <ta e="T71" id="Seg_3258" s="T70">n</ta>
            <ta e="T72" id="Seg_3259" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_3260" s="T72">n</ta>
            <ta e="T78" id="Seg_3261" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_3262" s="T78">n</ta>
            <ta e="T80" id="Seg_3263" s="T79">n</ta>
            <ta e="T81" id="Seg_3264" s="T80">cop</ta>
            <ta e="T82" id="Seg_3265" s="T81">conj</ta>
            <ta e="T83" id="Seg_3266" s="T82">dempro</ta>
            <ta e="T84" id="Seg_3267" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_3268" s="T84">que</ta>
            <ta e="T86" id="Seg_3269" s="T85">v</ta>
            <ta e="T87" id="Seg_3270" s="T86">dempro</ta>
            <ta e="T88" id="Seg_3271" s="T87">n</ta>
            <ta e="T89" id="Seg_3272" s="T88">v</ta>
            <ta e="T90" id="Seg_3273" s="T89">n</ta>
            <ta e="T91" id="Seg_3274" s="T90">v</ta>
            <ta e="T92" id="Seg_3275" s="T91">v</ta>
            <ta e="T93" id="Seg_3276" s="T92">conj</ta>
            <ta e="T94" id="Seg_3277" s="T93">que</ta>
            <ta e="T95" id="Seg_3278" s="T94">que</ta>
            <ta e="T96" id="Seg_3279" s="T95">v</ta>
            <ta e="T97" id="Seg_3280" s="T96">adj</ta>
            <ta e="T98" id="Seg_3281" s="T97">n</ta>
            <ta e="T99" id="Seg_3282" s="T98">v</ta>
            <ta e="T100" id="Seg_3283" s="T99">conj</ta>
            <ta e="T104" id="Seg_3284" s="T103">v</ta>
            <ta e="T105" id="Seg_3285" s="T104">dempro</ta>
            <ta e="T106" id="Seg_3286" s="T105">conj</ta>
            <ta e="T107" id="Seg_3287" s="T106">v</ta>
            <ta e="T108" id="Seg_3288" s="T107">conj</ta>
            <ta e="T109" id="Seg_3289" s="T108">n</ta>
            <ta e="T110" id="Seg_3290" s="T109">cop</ta>
            <ta e="T112" id="Seg_3291" s="T111">adv</ta>
            <ta e="T113" id="Seg_3292" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_3293" s="T113">adj</ta>
            <ta e="T115" id="Seg_3294" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_3295" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_3296" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_3297" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_3298" s="T118">n</ta>
            <ta e="T120" id="Seg_3299" s="T119">adv</ta>
            <ta e="T121" id="Seg_3300" s="T120">v</ta>
            <ta e="T123" id="Seg_3301" s="T121">ptcl</ta>
            <ta e="T132" id="Seg_3302" s="T131">n</ta>
            <ta e="T133" id="Seg_3303" s="T132">v</ta>
            <ta e="T134" id="Seg_3304" s="T133">conj</ta>
            <ta e="T147" id="Seg_3305" s="T329">adj</ta>
            <ta e="T148" id="Seg_3306" s="T147">n</ta>
            <ta e="T149" id="Seg_3307" s="T148">adv</ta>
            <ta e="T152" id="Seg_3308" s="T151">v</ta>
            <ta e="T153" id="Seg_3309" s="T152">v</ta>
            <ta e="T155" id="Seg_3310" s="T153">ptcl</ta>
            <ta e="T158" id="Seg_3311" s="T157">v</ta>
            <ta e="T159" id="Seg_3312" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_3313" s="T159">n</ta>
            <ta e="T161" id="Seg_3314" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_3315" s="T161">conj</ta>
            <ta e="T163" id="Seg_3316" s="T162">v</ta>
            <ta e="T165" id="Seg_3317" s="T163">ptcl</ta>
            <ta e="T166" id="Seg_3318" s="T165">conj</ta>
            <ta e="T167" id="Seg_3319" s="T166">n</ta>
            <ta e="T168" id="Seg_3320" s="T167">v</ta>
            <ta e="T169" id="Seg_3321" s="T168">conj</ta>
            <ta e="T170" id="Seg_3322" s="T169">dempro</ta>
            <ta e="T171" id="Seg_3323" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_3324" s="T171">dempro</ta>
            <ta e="T173" id="Seg_3325" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_3326" s="T173">dempro</ta>
            <ta e="T175" id="Seg_3327" s="T174">que</ta>
            <ta e="T176" id="Seg_3328" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_3329" s="T176">conj</ta>
            <ta e="T178" id="Seg_3330" s="T177">n</ta>
            <ta e="T179" id="Seg_3331" s="T178">dempro</ta>
            <ta e="T180" id="Seg_3332" s="T179">dempro</ta>
            <ta e="T181" id="Seg_3333" s="T180">dempro</ta>
            <ta e="T182" id="Seg_3334" s="T181">n</ta>
            <ta e="T183" id="Seg_3335" s="T182">n</ta>
            <ta e="T184" id="Seg_3336" s="T183">v</ta>
            <ta e="T185" id="Seg_3337" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_3338" s="T185">v</ta>
            <ta e="T187" id="Seg_3339" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_3340" s="T187">v</ta>
            <ta e="T189" id="Seg_3341" s="T188">v</ta>
            <ta e="T190" id="Seg_3342" s="T189">adv</ta>
            <ta e="T191" id="Seg_3343" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_3344" s="T191">n</ta>
            <ta e="T193" id="Seg_3345" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_3346" s="T193">n</ta>
            <ta e="T196" id="Seg_3347" s="T195">n</ta>
            <ta e="T197" id="Seg_3348" s="T196">n</ta>
            <ta e="T198" id="Seg_3349" s="T197">dempro</ta>
            <ta e="T199" id="Seg_3350" s="T198">n</ta>
            <ta e="T200" id="Seg_3351" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_3352" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_3353" s="T201">v</ta>
            <ta e="T203" id="Seg_3354" s="T202">n</ta>
            <ta e="T204" id="Seg_3355" s="T203">n</ta>
            <ta e="T205" id="Seg_3356" s="T204">adv</ta>
            <ta e="T206" id="Seg_3357" s="T205">que</ta>
            <ta e="T207" id="Seg_3358" s="T206">cop</ta>
            <ta e="T208" id="Seg_3359" s="T207">quant</ta>
            <ta e="T209" id="Seg_3360" s="T208">n</ta>
            <ta e="T210" id="Seg_3361" s="T209">adv</ta>
            <ta e="T213" id="Seg_3362" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_3363" s="T213">quant</ta>
            <ta e="T215" id="Seg_3364" s="T214">n</ta>
            <ta e="T216" id="Seg_3365" s="T215">v</ta>
            <ta e="T217" id="Seg_3366" s="T216">n</ta>
            <ta e="T218" id="Seg_3367" s="T217">quant</ta>
            <ta e="T219" id="Seg_3368" s="T218">cop</ta>
            <ta e="T220" id="Seg_3369" s="T219">quant</ta>
            <ta e="T221" id="Seg_3370" s="T220">cop</ta>
            <ta e="T222" id="Seg_3371" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_3372" s="T222">adv</ta>
            <ta e="T224" id="Seg_3373" s="T223">dempro</ta>
            <ta e="T225" id="Seg_3374" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_3375" s="T225">quant</ta>
            <ta e="T227" id="Seg_3376" s="T226">n</ta>
            <ta e="T229" id="Seg_3377" s="T227">cop</ta>
            <ta e="T231" id="Seg_3378" s="T230">n</ta>
            <ta e="T232" id="Seg_3379" s="T231">v</ta>
            <ta e="T233" id="Seg_3380" s="T232">adj</ta>
            <ta e="T234" id="Seg_3381" s="T233">adj</ta>
            <ta e="T235" id="Seg_3382" s="T234">adj</ta>
            <ta e="T236" id="Seg_3383" s="T235">n</ta>
            <ta e="T237" id="Seg_3384" s="T236">v</ta>
            <ta e="T238" id="Seg_3385" s="T237">n</ta>
            <ta e="T239" id="Seg_3386" s="T238">ptcl</ta>
            <ta e="T243" id="Seg_3387" s="T241">n</ta>
            <ta e="T244" id="Seg_3388" s="T243">dempro</ta>
            <ta e="T245" id="Seg_3389" s="T244">v</ta>
            <ta e="T246" id="Seg_3390" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_3391" s="T246">dempro</ta>
            <ta e="T248" id="Seg_3392" s="T247">n</ta>
            <ta e="T249" id="Seg_3393" s="T248">cop</ta>
            <ta e="T250" id="Seg_3394" s="T249">adj</ta>
            <ta e="T251" id="Seg_3395" s="T250">ptcl</ta>
            <ta e="T122" id="Seg_3396" s="T266">n</ta>
            <ta e="T269" id="Seg_3397" s="T268">conj</ta>
            <ta e="T270" id="Seg_3398" s="T269">n</ta>
            <ta e="T271" id="Seg_3399" s="T270">cop</ta>
            <ta e="T272" id="Seg_3400" s="T271">adj</ta>
            <ta e="T273" id="Seg_3401" s="T272">n</ta>
            <ta e="T274" id="Seg_3402" s="T273">adj</ta>
            <ta e="T275" id="Seg_3403" s="T274">n</ta>
            <ta e="T276" id="Seg_3404" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_3405" s="T276">n</ta>
            <ta e="T278" id="Seg_3406" s="T277">cop</ta>
            <ta e="T279" id="Seg_3407" s="T278">n</ta>
            <ta e="T280" id="Seg_3408" s="T279">dempro</ta>
            <ta e="T281" id="Seg_3409" s="T280">adj</ta>
            <ta e="T291" id="Seg_3410" s="T290">v</ta>
            <ta e="T305" id="Seg_3411" s="T304">n</ta>
            <ta e="T306" id="Seg_3412" s="T305">n</ta>
            <ta e="T307" id="Seg_3413" s="T306">n</ta>
            <ta e="T308" id="Seg_3414" s="T307">adj</ta>
            <ta e="T309" id="Seg_3415" s="T308">ptcl</ta>
            <ta e="T316" id="Seg_3416" s="T315">emphpro</ta>
            <ta e="T317" id="Seg_3417" s="T316">n</ta>
            <ta e="T318" id="Seg_3418" s="T317">n</ta>
            <ta e="T319" id="Seg_3419" s="T318">adv</ta>
            <ta e="T320" id="Seg_3420" s="T319">v</ta>
            <ta e="T321" id="Seg_3421" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_3422" s="T321">n</ta>
            <ta e="T323" id="Seg_3423" s="T322">n</ta>
            <ta e="T325" id="Seg_3424" s="T324">n</ta>
            <ta e="T327" id="Seg_3425" s="T325">n</ta>
            <ta e="T328" id="Seg_3426" s="T327">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_3427" s="T5">RUS:cult</ta>
            <ta e="T17" id="Seg_3428" s="T16">RUS:cult</ta>
            <ta e="T20" id="Seg_3429" s="T19">RUS:gram</ta>
            <ta e="T25" id="Seg_3430" s="T24">RUS:gram</ta>
            <ta e="T26" id="Seg_3431" s="T25">RUS:cult</ta>
            <ta e="T29" id="Seg_3432" s="T28">RUS:cult</ta>
            <ta e="T31" id="Seg_3433" s="T30">RUS:cult</ta>
            <ta e="T32" id="Seg_3434" s="T31">RUS:cult</ta>
            <ta e="T33" id="Seg_3435" s="T32">RUS:cult</ta>
            <ta e="T34" id="Seg_3436" s="T33">RUS:cult</ta>
            <ta e="T37" id="Seg_3437" s="T36">RUS:gram</ta>
            <ta e="T58" id="Seg_3438" s="T57">EV:core</ta>
            <ta e="T62" id="Seg_3439" s="T61">EV:gram (DIM)</ta>
            <ta e="T73" id="Seg_3440" s="T72">RUS:cult</ta>
            <ta e="T82" id="Seg_3441" s="T81">RUS:gram</ta>
            <ta e="T93" id="Seg_3442" s="T92">RUS:gram</ta>
            <ta e="T100" id="Seg_3443" s="T99">RUS:gram</ta>
            <ta e="T106" id="Seg_3444" s="T105">RUS:gram</ta>
            <ta e="T108" id="Seg_3445" s="T107">RUS:gram</ta>
            <ta e="T109" id="Seg_3446" s="T108">RUS:cult</ta>
            <ta e="T116" id="Seg_3447" s="T115">RUS:disc</ta>
            <ta e="T117" id="Seg_3448" s="T116">RUS:disc</ta>
            <ta e="T118" id="Seg_3449" s="T117">RUS:mod</ta>
            <ta e="T123" id="Seg_3450" s="T121">RUS:mod</ta>
            <ta e="T134" id="Seg_3451" s="T133">RUS:gram</ta>
            <ta e="T159" id="Seg_3452" s="T158">RUS:mod</ta>
            <ta e="T162" id="Seg_3453" s="T161">RUS:gram</ta>
            <ta e="T163" id="Seg_3454" s="T162">RUS:cult</ta>
            <ta e="T166" id="Seg_3455" s="T165">RUS:gram</ta>
            <ta e="T167" id="Seg_3456" s="T166">RUS:cult</ta>
            <ta e="T169" id="Seg_3457" s="T168">RUS:gram</ta>
            <ta e="T171" id="Seg_3458" s="T170">RUS:mod</ta>
            <ta e="T177" id="Seg_3459" s="T176">RUS:gram</ta>
            <ta e="T178" id="Seg_3460" s="T177">RUS:cult</ta>
            <ta e="T201" id="Seg_3461" s="T200">RUS:mod</ta>
            <ta e="T204" id="Seg_3462" s="T203">RUS:cult</ta>
            <ta e="T209" id="Seg_3463" s="T208">RUS:cult</ta>
            <ta e="T210" id="Seg_3464" s="T209">RUS:mod</ta>
            <ta e="T213" id="Seg_3465" s="T212">RUS:disc</ta>
            <ta e="T217" id="Seg_3466" s="T216">RUS:cult</ta>
            <ta e="T218" id="Seg_3467" s="T217">RUS:core</ta>
            <ta e="T227" id="Seg_3468" s="T226">RUS:cult</ta>
            <ta e="T248" id="Seg_3469" s="T247">RUS:cult</ta>
            <ta e="T269" id="Seg_3470" s="T268">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T58" id="Seg_3471" s="T57">inCdel</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T6" id="Seg_3472" s="T5">dir:bare</ta>
            <ta e="T17" id="Seg_3473" s="T16">dir:infl</ta>
            <ta e="T20" id="Seg_3474" s="T19">dir:bare</ta>
            <ta e="T25" id="Seg_3475" s="T24">dir:bare</ta>
            <ta e="T26" id="Seg_3476" s="T25">dir:bare</ta>
            <ta e="T29" id="Seg_3477" s="T28">dir:infl</ta>
            <ta e="T31" id="Seg_3478" s="T30">indir:infl</ta>
            <ta e="T32" id="Seg_3479" s="T31">indir:infl</ta>
            <ta e="T33" id="Seg_3480" s="T32">dir:bare</ta>
            <ta e="T34" id="Seg_3481" s="T33">dir:bare</ta>
            <ta e="T37" id="Seg_3482" s="T36">dir:bare</ta>
            <ta e="T58" id="Seg_3483" s="T57">dir:infl</ta>
            <ta e="T73" id="Seg_3484" s="T72">dir:bare</ta>
            <ta e="T82" id="Seg_3485" s="T81">dir:bare</ta>
            <ta e="T93" id="Seg_3486" s="T92">dir:bare</ta>
            <ta e="T100" id="Seg_3487" s="T99">dir:bare</ta>
            <ta e="T106" id="Seg_3488" s="T105">dir:bare</ta>
            <ta e="T108" id="Seg_3489" s="T107">dir:bare</ta>
            <ta e="T109" id="Seg_3490" s="T108">dir:bare</ta>
            <ta e="T116" id="Seg_3491" s="T115">dir:bare</ta>
            <ta e="T117" id="Seg_3492" s="T116">dir:bare</ta>
            <ta e="T118" id="Seg_3493" s="T117">dir:bare</ta>
            <ta e="T123" id="Seg_3494" s="T121">dir:bare</ta>
            <ta e="T134" id="Seg_3495" s="T133">dir:bare</ta>
            <ta e="T159" id="Seg_3496" s="T158">dir:bare</ta>
            <ta e="T162" id="Seg_3497" s="T161">dir:bare</ta>
            <ta e="T163" id="Seg_3498" s="T162">dir:infl</ta>
            <ta e="T166" id="Seg_3499" s="T165">dir:bare</ta>
            <ta e="T167" id="Seg_3500" s="T166">dir:bare</ta>
            <ta e="T169" id="Seg_3501" s="T168">dir:bare</ta>
            <ta e="T171" id="Seg_3502" s="T170">dir:bare</ta>
            <ta e="T177" id="Seg_3503" s="T176">dir:bare</ta>
            <ta e="T178" id="Seg_3504" s="T177">dir:bare</ta>
            <ta e="T201" id="Seg_3505" s="T200">dir:bare</ta>
            <ta e="T204" id="Seg_3506" s="T203">dir:bare</ta>
            <ta e="T209" id="Seg_3507" s="T208">dir:bare</ta>
            <ta e="T210" id="Seg_3508" s="T209">dir:bare</ta>
            <ta e="T213" id="Seg_3509" s="T212">dir:bare</ta>
            <ta e="T217" id="Seg_3510" s="T216">dir:bare</ta>
            <ta e="T218" id="Seg_3511" s="T217">dir:bare</ta>
            <ta e="T227" id="Seg_3512" s="T226">dir:bare</ta>
            <ta e="T248" id="Seg_3513" s="T247">dir:bare</ta>
            <ta e="T269" id="Seg_3514" s="T268">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T44" id="Seg_3515" s="T41">RUS:int.ins</ta>
            <ta e="T50" id="Seg_3516" s="T48">RUS:int.ins</ta>
            <ta e="T77" id="Seg_3517" s="T73">RUS:int.ins</ta>
            <ta e="T103" id="Seg_3518" s="T100">RUS:int.ins</ta>
            <ta e="T111" id="Seg_3519" s="T110">RUS:ext</ta>
            <ta e="T131" id="Seg_3520" s="T123">RUS:int.ins</ta>
            <ta e="T146" id="Seg_3521" s="T134">RUS:int.ins</ta>
            <ta e="T212" id="Seg_3522" s="T210">RUS:int.ins</ta>
            <ta e="T230" id="Seg_3523" s="T229">RUS:int.ins</ta>
            <ta e="T241" id="Seg_3524" s="T239">RUS:int.ins</ta>
            <ta e="T252" id="Seg_3525" s="T251">RUS:ext</ta>
            <ta e="T253" id="Seg_3526" s="T252">RUS:ext</ta>
            <ta e="T259" id="Seg_3527" s="T253">RUS:ext</ta>
            <ta e="T260" id="Seg_3528" s="T259">RUS:ext</ta>
            <ta e="T266" id="Seg_3529" s="T260">RUS:ext</ta>
            <ta e="T122" id="Seg_3530" s="T266">DLG:int.ins</ta>
            <ta e="T130" id="Seg_3531" s="T122">RUS:ext</ta>
            <ta e="T282" id="Seg_3532" s="T281">RUS:int.ins</ta>
            <ta e="T290" id="Seg_3533" s="T282">RUS:ext</ta>
            <ta e="T291" id="Seg_3534" s="T290">DLG:int.ins</ta>
            <ta e="T294" id="Seg_3535" s="T291">RUS:ext</ta>
            <ta e="T295" id="Seg_3536" s="T294">RUS:ext</ta>
            <ta e="T296" id="Seg_3537" s="T295">RUS:ext</ta>
            <ta e="T299" id="Seg_3538" s="T296">RUS:ext</ta>
            <ta e="T304" id="Seg_3539" s="T299">RUS:ext</ta>
            <ta e="T311" id="Seg_3540" s="T309">RUS:int.ins</ta>
            <ta e="T324" id="Seg_3541" s="T323">RUS:int.ins</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_3542" s="T0">From reindeer blood we make what, well, sausage.</ta>
            <ta e="T19" id="Seg_3543" s="T7">They kill a reindeer and after that we take the blood in a tub.</ta>
            <ta e="T28" id="Seg_3544" s="T19">And we leave it to sit, so it changes into serum.</ta>
            <ta e="T36" id="Seg_3545" s="T28">We knead and knead the serum, it becomes serum.</ta>
            <ta e="T50" id="Seg_3546" s="T36">And then we take the innards, these innards, and then, whatever, there is the rectum.</ta>
            <ta e="T60" id="Seg_3547" s="T50">You take the rectum, you take the abomasum, and you take from the gut.</ta>
            <ta e="T81" id="Seg_3548" s="T60">So washing, washing and so on you wash it all, and after that the blood… the serum becomes very transparent as it were, it becomes water blood (serum).</ta>
            <ta e="T96" id="Seg_3549" s="T81">and that you pour in whatchamacallit the abomasum, you pour it in the small intestine, you tie it and you boil it.</ta>
            <ta e="T99" id="Seg_3550" s="T96">You put it in hot water.</ta>
            <ta e="T105" id="Seg_3551" s="T99">And it cooks for five or ten minutes.</ta>
            <ta e="T110" id="Seg_3552" s="T105">And you take it out and it becomes sausage.</ta>
            <ta e="T112" id="Seg_3553" s="T110">[SE] So fast! Quick! </ta>
            <ta e="T123" id="Seg_3554" s="T112">Yes, very quick, well, that blood has to sit for a long time.</ta>
            <ta e="T146" id="Seg_3555" s="T123">Almost a whole day, for example at eleven in the morning you take blood, and in the evening at seven or eight you can already, it becomes serum.</ta>
            <ta e="T155" id="Seg_3556" s="T146">On a cold spot it doesnʼt work, it doesnʼt cook.</ta>
            <ta e="T165" id="Seg_3557" s="T155">It has to sit on a warm spot, the blood, in order to let it become serum.</ta>
            <ta e="T187" id="Seg_3558" s="T165">The serum gathers, and then you can whatchamacallit, while the serum, you put it in the abomasum, you pour it.</ta>
            <ta e="T196" id="Seg_3559" s="T187">You tie it, you put it and then there is also the reticulum, with the reticulum, the innards.</ta>
            <ta e="T203" id="Seg_3560" s="T196">The reticulum, in its inside you can also pour blood.</ta>
            <ta e="T210" id="Seg_3561" s="T203">Serum is often what, a little.</ta>
            <ta e="T229" id="Seg_3562" s="T210">There is little sausage, when you kill many reindeer, there is a lot of serum, and then there is a lot of sausage.</ta>
            <ta e="T239" id="Seg_3563" s="T229">And out of one reindeer (…) there you pour it in the abomasum, you pour it in the rectum, in the small intestine.</ta>
            <ta e="T243" id="Seg_3564" s="T239">It is the gut probably.</ta>
            <ta e="T251" id="Seg_3565" s="T243">You pour it and it will become sausage, very tasty.</ta>
            <ta e="T252" id="Seg_3566" s="T251">[SE] Yes?</ta>
            <ta e="T253" id="Seg_3567" s="T252">Tasty.</ta>
            <ta e="T259" id="Seg_3568" s="T253">[SE] And what do you like most?</ta>
            <ta e="T260" id="Seg_3569" s="T259">Me?</ta>
            <ta e="T130" id="Seg_3570" s="T260">I like, I like this sausage, in general, I like tongue, [itʼs] tasty.</ta>
            <ta e="T276" id="Seg_3571" s="T268">And in the hip there is tasty fat, in the fat hip.</ta>
            <ta e="T279" id="Seg_3572" s="T276">There is fat in the hip.</ta>
            <ta e="T282" id="Seg_3573" s="T279">This is tasty.</ta>
            <ta e="T294" id="Seg_3574" s="T282">I [prefer] especially… the domesticated, its meat is distinct from wild reindeer.</ta>
            <ta e="T295" id="Seg_3575" s="T294">[SE] Yes?</ta>
            <ta e="T296" id="Seg_3576" s="T295">Yes.</ta>
            <ta e="T299" id="Seg_3577" s="T296">[SE] And what is tastier?</ta>
            <ta e="T304" id="Seg_3578" s="T299">I think the wild reindeer is tastier.</ta>
            <ta e="T311" id="Seg_3579" s="T304">The meat of wild reindeer is tasty, than domesticated.</ta>
            <ta e="T327" id="Seg_3580" s="T311">Either we ourselves… for ourselves I donʼt like the meat of domesticated reindeer so much, the meat of the wild reindeer is tastier than the meat of the domesticated reindeer.</ta>
            <ta e="T328" id="Seg_3581" s="T327">Tasty.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_3582" s="T0">Aus Rentierblut machen wir dings, nun, Wurst.</ta>
            <ta e="T19" id="Seg_3583" s="T7">Das… sie töten ein Rentier und danach tun wir das Blut in eine Wanne.</ta>
            <ta e="T28" id="Seg_3584" s="T19">Und wir lassen es stehen, damit es Blutserum wird.</ta>
            <ta e="T36" id="Seg_3585" s="T28">Wir kneten und kneten das Blutserum, das wird zu Blutserum.</ta>
            <ta e="T50" id="Seg_3586" s="T36">Und dann nehmen wir die Innereien, diese Innereien, dann dings, da ist der Enddarm.</ta>
            <ta e="T60" id="Seg_3587" s="T50">Du nimmst den Enddarm, du nimmst den Labmagen, und du nimmst [etwas] vom Darm.</ta>
            <ta e="T81" id="Seg_3588" s="T60">Und das alles waschend, waschend, du wäschst alles, und danach das Blut, das Blutserum wird sehr durchsichtig, es wird Wasserblut. </ta>
            <ta e="T96" id="Seg_3589" s="T81">Und das gießt du in dings, in den Labmagen, du gießt es in den Dünndarm, du bindest es zu und kochst es.</ta>
            <ta e="T99" id="Seg_3590" s="T96">Du legst es in heißes Wasser.</ta>
            <ta e="T105" id="Seg_3591" s="T99">Und es kocht fünf oder zehn Minuten.</ta>
            <ta e="T110" id="Seg_3592" s="T105">Du nimmst es heraus und es ist Wurst.</ta>
            <ta e="T112" id="Seg_3593" s="T110">[SE] So schnell! Schnell!</ta>
            <ta e="T123" id="Seg_3594" s="T112">Ja, sehr schnell, nun das Blut muss lange stehen.</ta>
            <ta e="T146" id="Seg_3595" s="T123">Fast einen ganzen Tag, morgens zum Beispiel um elf nimmst du Blut, und abends um sieben oder acht kann man schon, ist es Blutserum.</ta>
            <ta e="T155" id="Seg_3596" s="T146">An einem kalten Platz funktioniert das nicht, es kocht nicht.</ta>
            <ta e="T165" id="Seg_3597" s="T155">Es muss an einem warmen Ort stehen, das Blut, damit es zu Blutserum wird.</ta>
            <ta e="T187" id="Seg_3598" s="T165">Das Blutserum sammelt sich, und dann kann man dingsen, solange das Blutserum, das tust du in den Labmagen, du gießt es.</ta>
            <ta e="T196" id="Seg_3599" s="T187">Du bindest es zu, du legst es hin, und dann gibt es noch den Netzmagen, die Innereien mit dem Netzmagen.</ta>
            <ta e="T203" id="Seg_3600" s="T196">Der Netzmagen, da kannst du auch Blut hineingießen.</ta>
            <ta e="T210" id="Seg_3601" s="T203">Blutserum ist oft dings, ein bisschen wenig.</ta>
            <ta e="T229" id="Seg_3602" s="T210">Es entsteht wenig Wurst, wenn du viele Rentiere tötest, dann gibt es viel Blutserum, und dann gibt es viel Wurst.</ta>
            <ta e="T239" id="Seg_3603" s="T229">Und dann aus einem Rentier (…), du gießt es in den Labmagen, du gießt es in den Enddarm, in den Dünndarm.</ta>
            <ta e="T243" id="Seg_3604" s="T239">Das ist wohl der Darm, der Dünndarm.</ta>
            <ta e="T251" id="Seg_3605" s="T243">Du gießt es und das wird Wurst, sehr lecker.</ta>
            <ta e="T252" id="Seg_3606" s="T251">[SE] Ja?</ta>
            <ta e="T253" id="Seg_3607" s="T252">Lecker.</ta>
            <ta e="T259" id="Seg_3608" s="T253">[SE] Und was mögen Sie am liebsten?</ta>
            <ta e="T260" id="Seg_3609" s="T259">Ich?</ta>
            <ta e="T130" id="Seg_3610" s="T260">Ich mag, ich mag allgemein diese Wurst, ich mag Zunge, [es ist] lecker.</ta>
            <ta e="T276" id="Seg_3611" s="T268">Und in der Hüfte gibt es leckeres Fett, in der fetten Hüfte.</ta>
            <ta e="T279" id="Seg_3612" s="T276">Es gibt Fett in der Hüfte.</ta>
            <ta e="T282" id="Seg_3613" s="T279">Das ist lecker.</ta>
            <ta e="T294" id="Seg_3614" s="T282">Ich [mag] vor allem… das Haus[rentier], sein Fleisch unterscheidet sich vom wilden Rentier.</ta>
            <ta e="T295" id="Seg_3615" s="T294">[SE] Ja?</ta>
            <ta e="T296" id="Seg_3616" s="T295">Ja.</ta>
            <ta e="T299" id="Seg_3617" s="T296">[SE] Und was ist leckerer?</ta>
            <ta e="T304" id="Seg_3618" s="T299">Ich finde, das wilde Rentier ist leckerer.</ta>
            <ta e="T311" id="Seg_3619" s="T304">Das Fleisch vom wilden Rentier is lecker[er] als vom Haus[rentier].</ta>
            <ta e="T327" id="Seg_3620" s="T311">Entweder wir selbst… für uns selbst, ich mag das Fleisch vom Hausrentier nicht so, das Fleisch vom wilden Rentier ist leckerer als das Fleisch vom Hausrentier.</ta>
            <ta e="T328" id="Seg_3621" s="T327">Lecker.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_3622" s="T0">Из оленьей крови мы эту делаем, колбасу.</ta>
            <ta e="T19" id="Seg_3623" s="T7">Этого… оленя убивают, после этого в тазике кровь берём.</ta>
            <ta e="T28" id="Seg_3624" s="T19">И ставим её постоять, чтобы сыворотка получилась.</ta>
            <ta e="T36" id="Seg_3625" s="T28">Сыворотку это, мнём, мнём, потом сыворотка получается.</ta>
            <ta e="T50" id="Seg_3626" s="T36">И потом внутренности берём, вот это, внутренности, потом там есть это, прямая кишка.</ta>
            <ta e="T60" id="Seg_3627" s="T50">Прямую кишку собираешь, потроха берёшь, от кишок тоже собираешь.</ta>
            <ta e="T81" id="Seg_3628" s="T60">И всё это моешь, моешь и всё такое, помыла, потом эта получается сыворотка, как будто очень прозрачная, как вода кровь становится.</ta>
            <ta e="T96" id="Seg_3629" s="T81">И это наливаешь в эти, в потроха, в кишку наливаешь, завязываешь и это, варишь.</ta>
            <ta e="T99" id="Seg_3630" s="T96">В горячую воду кладёшь.</ta>
            <ta e="T105" id="Seg_3631" s="T99">И минут пять-десять оно варится.</ta>
            <ta e="T110" id="Seg_3632" s="T105">И вынимаешь, и получается колбаса.</ta>
            <ta e="T112" id="Seg_3633" s="T110">[SE] Так быстро! Быстро! </ta>
            <ta e="T123" id="Seg_3634" s="T112">Да, очень быстро, вот это же, кровь, долго надо постоять.</ta>
            <ta e="T146" id="Seg_3635" s="T123">Целый день почти, утром, например, в одиннадцать кровь берёшь, и вечером можно в семь-восемь уже… сыворотка получается.</ta>
            <ta e="T155" id="Seg_3636" s="T146">В холодном месте не получается, не варится же.</ta>
            <ta e="T165" id="Seg_3637" s="T155">В тёплом месте должна стоять кровь, эта кровь, чтобы сыворотка получилась.</ta>
            <ta e="T187" id="Seg_3638" s="T165">А сыворотка собирается и это можно, это делать же, пока сыворотка это, в потроха внутрь кладёшь, наливаешь [сыворотку].</ta>
            <ta e="T196" id="Seg_3639" s="T187">Связываешь, кладёшь, и потом [там] есть сетка, внутренности с сеткой.</ta>
            <ta e="T203" id="Seg_3640" s="T196">Внутрь этой сетки тоже можно налить кровь.</ta>
            <ta e="T210" id="Seg_3641" s="T203">Сыворотки обычно бывает мало.</ta>
            <ta e="T229" id="Seg_3642" s="T210">Колбасы получается мало, когда много оленей убиваешь, сыворотки много получается, тогда много колбасы получается.</ta>
            <ta e="T239" id="Seg_3643" s="T229">А то из одного олени (…) там в потороха наливаешь, в прямую кишку наливаешь, в кишку же.</ta>
            <ta e="T243" id="Seg_3644" s="T239">Это кишка, наверное, тонкая кишка.</ta>
            <ta e="T251" id="Seg_3645" s="T243">Наливаешь её, тогда колбаса получается, это очень вкусно.</ta>
            <ta e="T252" id="Seg_3646" s="T251">[SE] Да?</ta>
            <ta e="T253" id="Seg_3647" s="T252">Вкусная.</ta>
            <ta e="T259" id="Seg_3648" s="T253">[SE] А что вы любите больше всего?</ta>
            <ta e="T260" id="Seg_3649" s="T259">Я?</ta>
            <ta e="T130" id="Seg_3650" s="T260">Я люблю, эту колбасу люблю, вообще люблю язык, вкусный.</ta>
            <ta e="T276" id="Seg_3651" s="T268">И в бедре бывает вкусный жир, в жирном бедре.</ta>
            <ta e="T279" id="Seg_3652" s="T276">Жир бывает в бедре.</ta>
            <ta e="T282" id="Seg_3653" s="T279">И это вкусно бывает.</ta>
            <ta e="T294" id="Seg_3654" s="T282">Я в основном… у домашнего от дикого оленя мясо сильно отличается.</ta>
            <ta e="T295" id="Seg_3655" s="T294">[SE] Да?</ta>
            <ta e="T296" id="Seg_3656" s="T295">Да.</ta>
            <ta e="T299" id="Seg_3657" s="T296">[SE] А что вкуснее?</ta>
            <ta e="T304" id="Seg_3658" s="T299">Мне кажется, вкуснее у дикого, дикий.</ta>
            <ta e="T311" id="Seg_3659" s="T304">У дикого мясо вкуснее, чем у домашнего.</ta>
            <ta e="T327" id="Seg_3660" s="T311">То ли мы сами не… для себя оленье мясо не очень люблю, дикое мясо вкуснее домашнего мяса.</ta>
            <ta e="T328" id="Seg_3661" s="T327">Вкусное.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_3662" s="T0">из оленьей крови, что делаем, колбасу</ta>
            <ta e="T19" id="Seg_3663" s="T7">это…оленя убивают, потом в тазике кровь берём</ta>
            <ta e="T28" id="Seg_3664" s="T19">и это…ставим, чтобы сыворотка получилась</ta>
            <ta e="T36" id="Seg_3665" s="T28">сыворотку мнём, потом сыворотка получается</ta>
            <ta e="T50" id="Seg_3666" s="T36">и потом внутренности берём, прямая кишка</ta>
            <ta e="T60" id="Seg_3667" s="T50">прямую кишку собираешь, потроха берёшь, от кишков тоже собираешь</ta>
            <ta e="T81" id="Seg_3668" s="T60">и всё это моешь, омыл помыл, потом эта получается сыворотка, как буд-то очень прозрачно, вот так кровь становится</ta>
            <ta e="T96" id="Seg_3669" s="T81">и это наливаешь в потроха, в кишку наливаешь, завязываешь и это варишь</ta>
            <ta e="T99" id="Seg_3670" s="T96">в горячую воду положишь</ta>
            <ta e="T105" id="Seg_3671" s="T99">и минут пять- десять это варится</ta>
            <ta e="T110" id="Seg_3672" s="T105">и вынимаешь, получается колбаса</ta>
            <ta e="T123" id="Seg_3673" s="T112">и это быстро и вот это же кровь долго надо постоять</ta>
            <ta e="T155" id="Seg_3674" s="T146">на холодном месте не получается, не варится же</ta>
            <ta e="T165" id="Seg_3675" s="T155">и на тёплом месте должна стоять кровь, эта кровь, чтобы сыворотка получилась</ta>
            <ta e="T187" id="Seg_3676" s="T165">сыворотка собирается и это можно этовать же пока сыворотка это, это в потроха внутри положишь, наливаешь (сыворотку)</ta>
            <ta e="T196" id="Seg_3677" s="T187">привязываешь положишь и потом есть сетка, внутренности с сеткой</ta>
            <ta e="T203" id="Seg_3678" s="T196">и во внутрь этого тоже можно налить кровь</ta>
            <ta e="T210" id="Seg_3679" s="T203">сыворотки обычно бывает мало</ta>
            <ta e="T229" id="Seg_3680" s="T210">когда много оленей убиваешь, сыворотки много получается, тогда много колбасы получается</ta>
            <ta e="T239" id="Seg_3681" s="T229">там в потороха наливаешь, в прямую кишку наливаешь, в кишку же</ta>
            <ta e="T251" id="Seg_3682" s="T243">наливаешь, тогда колбаса получается, это очень вкусно</ta>
            <ta e="T130" id="Seg_3683" s="T260">люблю язык</ta>
            <ta e="T276" id="Seg_3684" s="T268">и в бедре бывает вкусное жирное, в жирное бедро</ta>
            <ta e="T279" id="Seg_3685" s="T276">жир бывает в бедре</ta>
            <ta e="T282" id="Seg_3686" s="T279">и это вкусно бывает</ta>
            <ta e="T327" id="Seg_3687" s="T311">для себя оленье мясо не очень люблю, дикое мясо вкуснее домашнего мяса</ta>
            <ta e="T328" id="Seg_3688" s="T327">вкусно</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T252" id="Seg_3689" s="T251">[DCh]: ES in Russian.</ta>
            <ta e="T259" id="Seg_3690" s="T253">[DCh]: ES in Russian.</ta>
            <ta e="T295" id="Seg_3691" s="T294">[DCh]: ES in Russian.</ta>
            <ta e="T299" id="Seg_3692" s="T296">[DCh]: ES in Russian.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T331" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T140" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T146" />
            <conversion-tli id="T329" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T155" />
            <conversion-tli id="T332" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T333" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T334" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T335" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T336" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T122" />
            <conversion-tli id="T130" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T337" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T338" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T330" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
