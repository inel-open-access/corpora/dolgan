<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID0B3C4F44-C240-5444-BC69-66788110DFFE">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KiPP_XXXX_2009_Clothes_nar</transcription-name>
         <referenced-file url="KiPP_XXXX_2009_Clothes_nar.wav" />
         <referenced-file url="KiPP_XXXX_2009_Clothes_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KiPP_XXXX_2009_Clothes_nar\KiPP_XXXX_2009_Clothes_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">221</ud-information>
            <ud-information attribute-name="# HIAT:w">126</ud-information>
            <ud-information attribute-name="# e">128</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">32</ud-information>
            <ud-information attribute-name="# sc">44</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KiPP">
            <abbreviation>KiPP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="SE">
            <abbreviation>SE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="XXXX">
            <abbreviation>XXXX</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.7999598896966659" />
         <tli id="T2" time="0.9794999999999999" type="appl" />
         <tli id="T3" time="1.75" type="appl" />
         <tli id="T4" time="2.5204999999999997" type="appl" />
         <tli id="T5" time="2.866522938079719" />
         <tli id="T6" time="3.915980002134777" />
         <tli id="T7" time="4.2708" type="appl" />
         <tli id="T8" time="5.0256" type="appl" />
         <tli id="T9" time="5.7804" type="appl" />
         <tli id="T10" time="6.5352" type="appl" />
         <tli id="T11" time="7.21000388470638" />
         <tli id="T12" time="7.962" type="appl" />
         <tli id="T13" time="8.634" type="appl" />
         <tli id="T14" time="9.306000000000001" type="appl" />
         <tli id="T15" time="10.011331523995675" />
         <tli id="T16" time="11.205324391098959" />
         <tli id="T17" time="11.9088" type="appl" />
         <tli id="T18" time="12.7856" type="appl" />
         <tli id="T19" time="13.6624" type="appl" />
         <tli id="T20" time="14.539200000000001" type="appl" />
         <tli id="T21" time="15.416" type="appl" />
         <tli id="T22" time="16.2928" type="appl" />
         <tli id="T23" time="17.1696" type="appl" />
         <tli id="T24" time="18.0464" type="appl" />
         <tli id="T25" time="18.9232" type="appl" />
         <tli id="T26" time="20.252317874153924" />
         <tli id="T27" time="20.2955" type="appl" />
         <tli id="T28" time="20.791" type="appl" />
         <tli id="T29" time="21.2865" type="appl" />
         <tli id="T30" time="21.8819952760717" />
         <tli id="T31" time="21.911" type="appl" />
         <tli id="T32" time="22.38236363636364" type="appl" />
         <tli id="T33" time="22.853727272727273" type="appl" />
         <tli id="T34" time="23.32509090909091" type="appl" />
         <tli id="T35" time="23.796454545454548" type="appl" />
         <tli id="T36" time="24.267818181818182" type="appl" />
         <tli id="T37" time="24.73918181818182" type="appl" />
         <tli id="T38" time="25.210545454545453" type="appl" />
         <tli id="T39" time="25.68190909090909" type="appl" />
         <tli id="T40" time="26.15327272727273" type="appl" />
         <tli id="T41" time="26.624636363636363" type="appl" />
         <tli id="T42" time="27.149330453669464" />
         <tli id="T43" time="27.5" type="appl" />
         <tli id="T44" time="27.904" type="appl" />
         <tli id="T45" time="28.308" type="appl" />
         <tli id="T46" time="28.712" type="appl" />
         <tli id="T47" time="28.911883680120333" />
         <tli id="T48" time="39.25803158686388" />
         <tli id="T49" time="39.4513552268739" />
         <tli id="T50" time="39.58075629073703" />
         <tli id="T51" time="40.10963636363636" type="appl" />
         <tli id="T52" time="40.64518181818182" type="appl" />
         <tli id="T53" time="41.180727272727275" type="appl" />
         <tli id="T54" time="41.716272727272724" type="appl" />
         <tli id="T55" time="42.25181818181818" type="appl" />
         <tli id="T56" time="42.78736363636364" type="appl" />
         <tli id="T57" time="43.32290909090909" type="appl" />
         <tli id="T58" time="43.85845454545454" type="appl" />
         <tli id="T59" time="45.257730759588874" />
         <tli id="T60" time="45.324394083730255" />
         <tli id="T61" time="45.89566666666666" type="appl" />
         <tli id="T62" time="46.6465" type="appl" />
         <tli id="T63" time="47.397333333333336" type="appl" />
         <tli id="T64" time="48.14816666666667" type="appl" />
         <tli id="T65" time="49.39085685635498" />
         <tli id="T66" time="49.453833333333336" type="appl" />
         <tli id="T67" time="50.00866666666667" type="appl" />
         <tli id="T68" time="50.563500000000005" type="appl" />
         <tli id="T69" time="51.11833333333333" type="appl" />
         <tli id="T70" time="51.67316666666667" type="appl" />
         <tli id="T71" time="52.657359739283024" />
         <tli id="T72" time="52.94675" type="appl" />
         <tli id="T73" time="53.6655" type="appl" />
         <tli id="T74" time="54.38425" type="appl" />
         <tli id="T75" time="55.06966717065681" />
         <tli id="T76" time="55.918" type="appl" />
         <tli id="T77" time="56.14" type="appl" />
         <tli id="T78" time="56.733" type="appl" />
         <tli id="T79" time="56.93432628008273" />
         <tli id="T80" time="57.548" type="appl" />
         <tli id="T81" time="58.363" type="appl" />
         <tli id="T82" time="58.803718225119084" />
         <tli id="T83" time="61.39692153421911" />
         <tli id="T84" time="63.142" type="appl" />
         <tli id="T85" time="64.25" type="appl" />
         <tli id="T86" time="65.00975" type="appl" />
         <tli id="T87" time="65.7695" type="appl" />
         <tli id="T88" time="66.52925" type="appl" />
         <tli id="T89" time="67.289" type="appl" />
         <tli id="T90" time="67.918" type="appl" />
         <tli id="T91" time="68.80031985616695" />
         <tli id="T92" time="68.98666666666666" type="appl" />
         <tli id="T93" time="69.42633333333333" type="appl" />
         <tli id="T94" time="69.866" type="appl" />
         <tli id="T95" time="70.30566666666667" type="appl" />
         <tli id="T96" time="70.74533333333333" type="appl" />
         <tli id="T97" time="71.2983261155678" />
         <tli id="T98" time="71.87066666666666" type="appl" />
         <tli id="T99" time="72.55633333333334" type="appl" />
         <tli id="T100" time="73.70963750313362" />
         <tli id="T101" time="74.15725" type="appl" />
         <tli id="T102" time="75.0725" type="appl" />
         <tli id="T103" time="75.98775" type="appl" />
         <tli id="T104" time="76.903" type="appl" />
         <tli id="T105" time="79.16832213587365" />
         <tli id="T106" time="79.77799999999999" type="appl" />
         <tli id="T107" time="80.7692835297067" />
         <tli id="T108" time="81.18842857142857" type="appl" />
         <tli id="T109" time="81.75585714285714" type="appl" />
         <tli id="T110" time="82.32328571428572" type="appl" />
         <tli id="T111" time="82.89071428571428" type="appl" />
         <tli id="T112" time="83.45814285714286" type="appl" />
         <tli id="T113" time="84.02557142857142" type="appl" />
         <tli id="T114" time="84.36243670092755" />
         <tli id="T115" time="85.403" type="appl" />
         <tli id="T116" time="85.903" type="appl" />
         <tli id="T117" time="85.911" type="appl" />
         <tli id="T118" time="86.27075" type="appl" />
         <tli id="T119" time="86.6385" type="appl" />
         <tli id="T120" time="87.00625" type="appl" />
         <tli id="T121" time="87.32733361274757" />
         <tli id="T122" time="87.382" type="appl" />
         <tli id="T123" time="87.98225520180496" />
         <tli id="T124" time="88.36890248182502" />
         <tli id="T125" time="88.48223013286538" />
         <tli id="T126" time="89.298" type="appl" />
         <tli id="T127" time="89.338" type="appl" />
         <tli id="T128" time="89.5488433191276" />
         <tli id="T129" time="89.68883629982452" />
         <tli id="T144" time="90.11548157432941" />
         <tli id="T130" time="90.8021138129857" />
         <tli id="T131" time="91.34975" type="appl" />
         <tli id="T132" time="92.16749999999999" type="appl" />
         <tli id="T133" time="92.98525" type="appl" />
         <tli id="T134" time="93.803" type="appl" />
         <tli id="T135" time="94.49266666666666" type="appl" />
         <tli id="T136" time="95.18233333333333" type="appl" />
         <tli id="T137" time="95.872" type="appl" />
         <tli id="T138" time="102.55485785911256" />
         <tli id="T139" time="102.86817548257709" />
         <tli id="T140" time="103.62199836268488" />
         <tli id="T141" time="104.862" type="appl" />
         <tli id="T142" time="105.478" type="appl" />
         <tli id="T143" time="106.33" type="appl" />
         <tli id="T0" time="106.368" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KiPP"
                      id="tx-KiPP"
                      speaker="KiPP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KiPP">
            <ts e="T5" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Baːta</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">hu͡organ</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">bejebit</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">tigebit</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T15" id="Seg_18" n="sc" s="T6">
               <ts e="T11" id="Seg_20" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">Itte</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">barɨta</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">bejebit</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">tikteːččibit</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">oččogo</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_38" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">Gatoːbaj</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">laːpkɨga</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">hu͡ok</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">bu͡o</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T30" id="Seg_52" n="sc" s="T16">
               <ts e="T26" id="Seg_54" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">Osku͡olaga</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">kiːremmit</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_62" n="HIAT:w" s="T18">usku͡olaga</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_65" n="HIAT:w" s="T19">mastʼerskojga</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_69" n="HIAT:w" s="T20">skola</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_72" n="HIAT:w" s="T21">ogotugar</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_75" n="HIAT:w" s="T22">taŋas</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_78" n="HIAT:w" s="T23">tigeːččibit</ts>
                  <nts id="Seg_79" n="HIAT:ip">,</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_82" n="HIAT:w" s="T24">köstʼümnarɨ</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_86" n="HIAT:w" s="T25">pɨlaːtɨjalarɨ</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_90" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">Onu</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_95" n="HIAT:w" s="T27">barɨtɨn</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T28">hatɨːbɨn</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_101" n="HIAT:w" s="T29">min</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T47" id="Seg_104" n="sc" s="T31">
               <ts e="T42" id="Seg_106" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_108" n="HIAT:w" s="T31">Haka</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_111" n="HIAT:w" s="T32">ületin</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_114" n="HIAT:w" s="T33">da</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_117" n="HIAT:w" s="T34">hatɨːbɨn</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_119" n="HIAT:ip">(</nts>
                  <ts e="T36" id="Seg_121" n="HIAT:w" s="T35">iːste-</ts>
                  <nts id="Seg_122" n="HIAT:ip">)</nts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_126" n="HIAT:w" s="T36">nʼuččalɨː</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_129" n="HIAT:w" s="T37">da</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_132" n="HIAT:w" s="T38">taŋahɨ</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_135" n="HIAT:w" s="T39">tigeːččibin</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_138" n="HIAT:w" s="T40">etim</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_141" n="HIAT:w" s="T41">urut</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_145" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_147" n="HIAT:w" s="T42">Anɨ</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_150" n="HIAT:w" s="T43">tuːgu</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_153" n="HIAT:w" s="T44">da</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_156" n="HIAT:w" s="T45">gɨna</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_159" n="HIAT:w" s="T46">koppoppun</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T82" id="Seg_162" n="sc" s="T48">
               <ts e="T59" id="Seg_164" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_166" n="HIAT:w" s="T48">Kiniːtterim</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_169" n="HIAT:w" s="T49">üčügej</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_172" n="HIAT:w" s="T50">bu͡olan</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_175" n="HIAT:w" s="T51">olorobun</ts>
                  <nts id="Seg_176" n="HIAT:ip">,</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_179" n="HIAT:w" s="T52">kiniːpper</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_182" n="HIAT:w" s="T53">küččügüj</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_185" n="HIAT:w" s="T54">kiniːpper</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_187" n="HIAT:ip">(</nts>
                  <ts e="T56" id="Seg_189" n="HIAT:w" s="T55">olor-</ts>
                  <nts id="Seg_190" n="HIAT:ip">)</nts>
                  <nts id="Seg_191" n="HIAT:ip">,</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_194" n="HIAT:w" s="T56">ortoku</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_197" n="HIAT:w" s="T57">kiniːkke</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_200" n="HIAT:w" s="T58">olorobun</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_204" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_206" n="HIAT:w" s="T59">Barɨ͡ak</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_209" n="HIAT:w" s="T60">ebippin</ts>
                  <nts id="Seg_210" n="HIAT:ip">,</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_213" n="HIAT:w" s="T61">koppoppun</ts>
                  <nts id="Seg_214" n="HIAT:ip">,</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_217" n="HIAT:w" s="T62">hir</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_220" n="HIAT:w" s="T63">ajɨ</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_223" n="HIAT:w" s="T64">ogoloːkpun</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_227" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_229" n="HIAT:w" s="T65">Hir</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_232" n="HIAT:w" s="T66">ajɨ</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_235" n="HIAT:w" s="T67">baːllar</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_238" n="HIAT:w" s="T68">da</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_241" n="HIAT:w" s="T69">kɨ͡ajan</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_244" n="HIAT:w" s="T70">tiːjbeppin</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_248" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_250" n="HIAT:w" s="T71">Gu͡orakka</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_252" n="HIAT:ip">(</nts>
                  <ts e="T73" id="Seg_254" n="HIAT:w" s="T72">bara-</ts>
                  <nts id="Seg_255" n="HIAT:ip">)</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_258" n="HIAT:w" s="T73">bagarbappɨn</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_261" n="HIAT:w" s="T74">törüt</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_265" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_267" n="HIAT:w" s="T75">Tundraga</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_270" n="HIAT:w" s="T76">törüt</ts>
                  <nts id="Seg_271" n="HIAT:ip">,</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_274" n="HIAT:w" s="T78">tundraga</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_277" n="HIAT:w" s="T80">ere</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_280" n="HIAT:w" s="T81">bagarabɨn</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T84" id="Seg_283" n="sc" s="T83">
               <ts e="T84" id="Seg_285" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_287" n="HIAT:w" s="T83">Hürdeːk</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T89" id="Seg_290" n="sc" s="T85">
               <ts e="T89" id="Seg_292" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_294" n="HIAT:w" s="T85">Aː</ts>
                  <nts id="Seg_295" n="HIAT:ip">,</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_298" n="HIAT:w" s="T86">tugu</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_301" n="HIAT:w" s="T87">di͡eppinij</ts>
                  <nts id="Seg_302" n="HIAT:ip">,</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_305" n="HIAT:w" s="T88">elete</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T104" id="Seg_308" n="sc" s="T91">
               <ts e="T97" id="Seg_310" n="HIAT:u" s="T91">
                  <nts id="Seg_311" n="HIAT:ip">–</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_314" n="HIAT:w" s="T91">Araː</ts>
                  <nts id="Seg_315" n="HIAT:ip">,</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_318" n="HIAT:w" s="T92">kaja</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_321" n="HIAT:w" s="T93">anɨ</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_324" n="HIAT:w" s="T94">da</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_327" n="HIAT:w" s="T95">bagarabɨn</ts>
                  <nts id="Seg_328" n="HIAT:ip">,</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_331" n="HIAT:w" s="T96">kotummappɨn</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_335" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_337" n="HIAT:w" s="T97">Bulkuj</ts>
                  <nts id="Seg_338" n="HIAT:ip">,</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_341" n="HIAT:w" s="T98">bulkuj</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_344" n="HIAT:ip">(</nts>
                  <nts id="Seg_345" n="HIAT:ip">(</nts>
                  <ats e="T100" id="Seg_346" n="HIAT:non-pho" s="T99">…</ats>
                  <nts id="Seg_347" n="HIAT:ip">)</nts>
                  <nts id="Seg_348" n="HIAT:ip">)</nts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_352" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_354" n="HIAT:w" s="T100">Ülehitter</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_357" n="HIAT:w" s="T101">keleller</ts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_361" n="HIAT:w" s="T102">avsʼebɨgɨ</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_364" n="HIAT:w" s="T103">kɨjnʼarabɨt</ts>
                  <nts id="Seg_365" n="HIAT:ip">.</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T114" id="Seg_367" n="sc" s="T107">
               <ts e="T114" id="Seg_369" n="HIAT:u" s="T107">
                  <nts id="Seg_370" n="HIAT:ip">–</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_373" n="HIAT:w" s="T107">Üčügej</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_376" n="HIAT:w" s="T108">bu͡o</ts>
                  <nts id="Seg_377" n="HIAT:ip">,</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_380" n="HIAT:w" s="T109">tundra</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_383" n="HIAT:w" s="T110">kördük</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_386" n="HIAT:w" s="T111">him</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_389" n="HIAT:w" s="T112">biːr</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_392" n="HIAT:w" s="T113">pasʼolakka</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T117" id="Seg_395" n="sc" s="T115">
               <ts e="T117" id="Seg_397" n="HIAT:u" s="T115">
                  <nts id="Seg_398" n="HIAT:ip">–</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_401" n="HIAT:w" s="T115">Ej</ts>
                  <nts id="Seg_402" n="HIAT:ip">.</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T124" id="Seg_404" n="sc" s="T122">
               <ts e="T124" id="Seg_406" n="HIAT:u" s="T122">
                  <nts id="Seg_407" n="HIAT:ip">–</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_410" n="HIAT:w" s="T122">Ahaːtɨnnar</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T144" id="Seg_413" n="sc" s="T127">
               <ts e="T144" id="Seg_415" n="HIAT:u" s="T127">
                  <nts id="Seg_416" n="HIAT:ip">–</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_419" n="HIAT:w" s="T127">Ol</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_422" n="HIAT:w" s="T128">baːr</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_425" n="HIAT:w" s="T129">ostol</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_428" n="sc" s="T130">
               <ts e="T134" id="Seg_430" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_432" n="HIAT:w" s="T130">Ogolor</ts>
                  <nts id="Seg_433" n="HIAT:ip">,</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_436" n="HIAT:w" s="T131">ordoroːruŋ</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_439" n="HIAT:w" s="T132">u͡ol</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_442" n="HIAT:w" s="T133">ülehittergitiger</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_446" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_448" n="HIAT:w" s="T134">Ahat</ts>
                  <nts id="Seg_449" n="HIAT:ip">,</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_452" n="HIAT:w" s="T135">ahat</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_455" n="HIAT:w" s="T136">ogoloru</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T140" id="Seg_458" n="sc" s="T138">
               <ts e="T140" id="Seg_460" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_462" n="HIAT:w" s="T138">Dʼe</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_465" n="HIAT:w" s="T139">elete</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T143" id="Seg_468" n="sc" s="T142">
               <ts e="T143" id="Seg_470" n="HIAT:u" s="T142">
                  <nts id="Seg_471" n="HIAT:ip">–</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_474" n="HIAT:w" s="T142">Elete</ts>
                  <nts id="Seg_475" n="HIAT:ip">.</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KiPP">
            <ts e="T5" id="Seg_477" n="sc" s="T1">
               <ts e="T2" id="Seg_479" n="e" s="T1">– Baːta </ts>
               <ts e="T3" id="Seg_481" n="e" s="T2">hu͡organ </ts>
               <ts e="T4" id="Seg_483" n="e" s="T3">bejebit </ts>
               <ts e="T5" id="Seg_485" n="e" s="T4">tigebit. </ts>
            </ts>
            <ts e="T15" id="Seg_486" n="sc" s="T6">
               <ts e="T7" id="Seg_488" n="e" s="T6">Itte </ts>
               <ts e="T8" id="Seg_490" n="e" s="T7">barɨta </ts>
               <ts e="T9" id="Seg_492" n="e" s="T8">bejebit </ts>
               <ts e="T10" id="Seg_494" n="e" s="T9">tikteːččibit </ts>
               <ts e="T11" id="Seg_496" n="e" s="T10">oččogo. </ts>
               <ts e="T12" id="Seg_498" n="e" s="T11">Gatoːbaj </ts>
               <ts e="T13" id="Seg_500" n="e" s="T12">laːpkɨga </ts>
               <ts e="T14" id="Seg_502" n="e" s="T13">hu͡ok </ts>
               <ts e="T15" id="Seg_504" n="e" s="T14">bu͡o. </ts>
            </ts>
            <ts e="T30" id="Seg_505" n="sc" s="T16">
               <ts e="T17" id="Seg_507" n="e" s="T16">Osku͡olaga </ts>
               <ts e="T18" id="Seg_509" n="e" s="T17">kiːremmit </ts>
               <ts e="T19" id="Seg_511" n="e" s="T18">usku͡olaga </ts>
               <ts e="T20" id="Seg_513" n="e" s="T19">mastʼerskojga, </ts>
               <ts e="T21" id="Seg_515" n="e" s="T20">skola </ts>
               <ts e="T22" id="Seg_517" n="e" s="T21">ogotugar </ts>
               <ts e="T23" id="Seg_519" n="e" s="T22">taŋas </ts>
               <ts e="T24" id="Seg_521" n="e" s="T23">tigeːččibit, </ts>
               <ts e="T25" id="Seg_523" n="e" s="T24">köstʼümnarɨ, </ts>
               <ts e="T26" id="Seg_525" n="e" s="T25">pɨlaːtɨjalarɨ. </ts>
               <ts e="T27" id="Seg_527" n="e" s="T26">Onu </ts>
               <ts e="T28" id="Seg_529" n="e" s="T27">barɨtɨn </ts>
               <ts e="T29" id="Seg_531" n="e" s="T28">hatɨːbɨn </ts>
               <ts e="T30" id="Seg_533" n="e" s="T29">min. </ts>
            </ts>
            <ts e="T47" id="Seg_534" n="sc" s="T31">
               <ts e="T32" id="Seg_536" n="e" s="T31">Haka </ts>
               <ts e="T33" id="Seg_538" n="e" s="T32">ületin </ts>
               <ts e="T34" id="Seg_540" n="e" s="T33">da </ts>
               <ts e="T35" id="Seg_542" n="e" s="T34">hatɨːbɨn </ts>
               <ts e="T36" id="Seg_544" n="e" s="T35">(iːste-), </ts>
               <ts e="T37" id="Seg_546" n="e" s="T36">nʼuččalɨː </ts>
               <ts e="T38" id="Seg_548" n="e" s="T37">da </ts>
               <ts e="T39" id="Seg_550" n="e" s="T38">taŋahɨ </ts>
               <ts e="T40" id="Seg_552" n="e" s="T39">tigeːččibin </ts>
               <ts e="T41" id="Seg_554" n="e" s="T40">etim </ts>
               <ts e="T42" id="Seg_556" n="e" s="T41">urut. </ts>
               <ts e="T43" id="Seg_558" n="e" s="T42">Anɨ </ts>
               <ts e="T44" id="Seg_560" n="e" s="T43">tuːgu </ts>
               <ts e="T45" id="Seg_562" n="e" s="T44">da </ts>
               <ts e="T46" id="Seg_564" n="e" s="T45">gɨna </ts>
               <ts e="T47" id="Seg_566" n="e" s="T46">koppoppun. </ts>
            </ts>
            <ts e="T82" id="Seg_567" n="sc" s="T48">
               <ts e="T49" id="Seg_569" n="e" s="T48">Kiniːtterim </ts>
               <ts e="T50" id="Seg_571" n="e" s="T49">üčügej </ts>
               <ts e="T51" id="Seg_573" n="e" s="T50">bu͡olan </ts>
               <ts e="T52" id="Seg_575" n="e" s="T51">olorobun, </ts>
               <ts e="T53" id="Seg_577" n="e" s="T52">kiniːpper </ts>
               <ts e="T54" id="Seg_579" n="e" s="T53">küččügüj </ts>
               <ts e="T55" id="Seg_581" n="e" s="T54">kiniːpper </ts>
               <ts e="T56" id="Seg_583" n="e" s="T55">(olor-), </ts>
               <ts e="T57" id="Seg_585" n="e" s="T56">ortoku </ts>
               <ts e="T58" id="Seg_587" n="e" s="T57">kiniːkke </ts>
               <ts e="T59" id="Seg_589" n="e" s="T58">olorobun. </ts>
               <ts e="T60" id="Seg_591" n="e" s="T59">Barɨ͡ak </ts>
               <ts e="T61" id="Seg_593" n="e" s="T60">ebippin, </ts>
               <ts e="T62" id="Seg_595" n="e" s="T61">koppoppun, </ts>
               <ts e="T63" id="Seg_597" n="e" s="T62">hir </ts>
               <ts e="T64" id="Seg_599" n="e" s="T63">ajɨ </ts>
               <ts e="T65" id="Seg_601" n="e" s="T64">ogoloːkpun. </ts>
               <ts e="T66" id="Seg_603" n="e" s="T65">Hir </ts>
               <ts e="T67" id="Seg_605" n="e" s="T66">ajɨ </ts>
               <ts e="T68" id="Seg_607" n="e" s="T67">baːllar </ts>
               <ts e="T69" id="Seg_609" n="e" s="T68">da </ts>
               <ts e="T70" id="Seg_611" n="e" s="T69">kɨ͡ajan </ts>
               <ts e="T71" id="Seg_613" n="e" s="T70">tiːjbeppin. </ts>
               <ts e="T72" id="Seg_615" n="e" s="T71">Gu͡orakka </ts>
               <ts e="T73" id="Seg_617" n="e" s="T72">(bara-) </ts>
               <ts e="T74" id="Seg_619" n="e" s="T73">bagarbappɨn </ts>
               <ts e="T75" id="Seg_621" n="e" s="T74">törüt. </ts>
               <ts e="T76" id="Seg_623" n="e" s="T75">Tundraga </ts>
               <ts e="T78" id="Seg_625" n="e" s="T76">törüt, </ts>
               <ts e="T80" id="Seg_627" n="e" s="T78">tundraga </ts>
               <ts e="T81" id="Seg_629" n="e" s="T80">ere </ts>
               <ts e="T82" id="Seg_631" n="e" s="T81">bagarabɨn. </ts>
            </ts>
            <ts e="T84" id="Seg_632" n="sc" s="T83">
               <ts e="T84" id="Seg_634" n="e" s="T83">Hürdeːk. </ts>
            </ts>
            <ts e="T89" id="Seg_635" n="sc" s="T85">
               <ts e="T86" id="Seg_637" n="e" s="T85">Aː, </ts>
               <ts e="T87" id="Seg_639" n="e" s="T86">tugu </ts>
               <ts e="T88" id="Seg_641" n="e" s="T87">di͡eppinij, </ts>
               <ts e="T89" id="Seg_643" n="e" s="T88">elete. </ts>
            </ts>
            <ts e="T104" id="Seg_644" n="sc" s="T91">
               <ts e="T92" id="Seg_646" n="e" s="T91">– Araː, </ts>
               <ts e="T93" id="Seg_648" n="e" s="T92">kaja </ts>
               <ts e="T94" id="Seg_650" n="e" s="T93">anɨ </ts>
               <ts e="T95" id="Seg_652" n="e" s="T94">da </ts>
               <ts e="T96" id="Seg_654" n="e" s="T95">bagarabɨn, </ts>
               <ts e="T97" id="Seg_656" n="e" s="T96">kotummappɨn. </ts>
               <ts e="T98" id="Seg_658" n="e" s="T97">Bulkuj, </ts>
               <ts e="T99" id="Seg_660" n="e" s="T98">bulkuj, </ts>
               <ts e="T100" id="Seg_662" n="e" s="T99">((…)). </ts>
               <ts e="T101" id="Seg_664" n="e" s="T100">Ülehitter </ts>
               <ts e="T102" id="Seg_666" n="e" s="T101">keleller, </ts>
               <ts e="T103" id="Seg_668" n="e" s="T102">avsʼebɨgɨ </ts>
               <ts e="T104" id="Seg_670" n="e" s="T103">kɨjnʼarabɨt. </ts>
            </ts>
            <ts e="T114" id="Seg_671" n="sc" s="T107">
               <ts e="T108" id="Seg_673" n="e" s="T107">– Üčügej </ts>
               <ts e="T109" id="Seg_675" n="e" s="T108">bu͡o, </ts>
               <ts e="T110" id="Seg_677" n="e" s="T109">tundra </ts>
               <ts e="T111" id="Seg_679" n="e" s="T110">kördük </ts>
               <ts e="T112" id="Seg_681" n="e" s="T111">him </ts>
               <ts e="T113" id="Seg_683" n="e" s="T112">biːr </ts>
               <ts e="T114" id="Seg_685" n="e" s="T113">pasʼolakka. </ts>
            </ts>
            <ts e="T117" id="Seg_686" n="sc" s="T115">
               <ts e="T117" id="Seg_688" n="e" s="T115">– Ej. </ts>
            </ts>
            <ts e="T124" id="Seg_689" n="sc" s="T122">
               <ts e="T124" id="Seg_691" n="e" s="T122">– Ahaːtɨnnar. </ts>
            </ts>
            <ts e="T144" id="Seg_692" n="sc" s="T127">
               <ts e="T128" id="Seg_694" n="e" s="T127">– Ol </ts>
               <ts e="T129" id="Seg_696" n="e" s="T128">baːr </ts>
               <ts e="T144" id="Seg_698" n="e" s="T129">ostol. </ts>
            </ts>
            <ts e="T137" id="Seg_699" n="sc" s="T130">
               <ts e="T131" id="Seg_701" n="e" s="T130">Ogolor, </ts>
               <ts e="T132" id="Seg_703" n="e" s="T131">ordoroːruŋ </ts>
               <ts e="T133" id="Seg_705" n="e" s="T132">u͡ol </ts>
               <ts e="T134" id="Seg_707" n="e" s="T133">ülehittergitiger. </ts>
               <ts e="T135" id="Seg_709" n="e" s="T134">Ahat, </ts>
               <ts e="T136" id="Seg_711" n="e" s="T135">ahat </ts>
               <ts e="T137" id="Seg_713" n="e" s="T136">ogoloru. </ts>
            </ts>
            <ts e="T140" id="Seg_714" n="sc" s="T138">
               <ts e="T139" id="Seg_716" n="e" s="T138">Dʼe </ts>
               <ts e="T140" id="Seg_718" n="e" s="T139">elete. </ts>
            </ts>
            <ts e="T143" id="Seg_719" n="sc" s="T142">
               <ts e="T143" id="Seg_721" n="e" s="T142">– Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KiPP">
            <ta e="T5" id="Seg_722" s="T1">KiPP_XXXX_2009_Clothes_nar.KiPP.001 (001.001)</ta>
            <ta e="T11" id="Seg_723" s="T6">KiPP_XXXX_2009_Clothes_nar.KiPP.002 (001.002)</ta>
            <ta e="T15" id="Seg_724" s="T11">KiPP_XXXX_2009_Clothes_nar.KiPP.003 (001.003)</ta>
            <ta e="T26" id="Seg_725" s="T16">KiPP_XXXX_2009_Clothes_nar.KiPP.004 (001.004)</ta>
            <ta e="T30" id="Seg_726" s="T26">KiPP_XXXX_2009_Clothes_nar.KiPP.005 (001.005)</ta>
            <ta e="T42" id="Seg_727" s="T31">KiPP_XXXX_2009_Clothes_nar.KiPP.006 (001.006)</ta>
            <ta e="T47" id="Seg_728" s="T42">KiPP_XXXX_2009_Clothes_nar.KiPP.007 (001.007)</ta>
            <ta e="T59" id="Seg_729" s="T48">KiPP_XXXX_2009_Clothes_nar.KiPP.008 (001.008)</ta>
            <ta e="T65" id="Seg_730" s="T59">KiPP_XXXX_2009_Clothes_nar.KiPP.009 (001.009)</ta>
            <ta e="T71" id="Seg_731" s="T65">KiPP_XXXX_2009_Clothes_nar.KiPP.010 (001.010)</ta>
            <ta e="T75" id="Seg_732" s="T71">KiPP_XXXX_2009_Clothes_nar.KiPP.011 (001.011)</ta>
            <ta e="T82" id="Seg_733" s="T75">KiPP_XXXX_2009_Clothes_nar.KiPP.012 (001.012)</ta>
            <ta e="T84" id="Seg_734" s="T83">KiPP_XXXX_2009_Clothes_nar.KiPP.013 (001.014)</ta>
            <ta e="T89" id="Seg_735" s="T85">KiPP_XXXX_2009_Clothes_nar.KiPP.014 (001.015)</ta>
            <ta e="T97" id="Seg_736" s="T91">KiPP_XXXX_2009_Clothes_nar.KiPP.015 (001.017)</ta>
            <ta e="T100" id="Seg_737" s="T97">KiPP_XXXX_2009_Clothes_nar.KiPP.016 (001.018)</ta>
            <ta e="T104" id="Seg_738" s="T100">KiPP_XXXX_2009_Clothes_nar.KiPP.017 (001.019)</ta>
            <ta e="T114" id="Seg_739" s="T107">KiPP_XXXX_2009_Clothes_nar.KiPP.018 (001.021)</ta>
            <ta e="T117" id="Seg_740" s="T115">KiPP_XXXX_2009_Clothes_nar.KiPP.019 (001.023)</ta>
            <ta e="T124" id="Seg_741" s="T122">KiPP_XXXX_2009_Clothes_nar.KiPP.020 (001.025)</ta>
            <ta e="T144" id="Seg_742" s="T127">KiPP_XXXX_2009_Clothes_nar.KiPP.021 (001.027)</ta>
            <ta e="T134" id="Seg_743" s="T130">KiPP_XXXX_2009_Clothes_nar.KiPP.022 (001.028)</ta>
            <ta e="T137" id="Seg_744" s="T134">KiPP_XXXX_2009_Clothes_nar.KiPP.023 (001.029)</ta>
            <ta e="T140" id="Seg_745" s="T138">KiPP_XXXX_2009_Clothes_nar.KiPP.024 (001.030)</ta>
            <ta e="T143" id="Seg_746" s="T142">KiPP_XXXX_2009_Clothes_nar.KiPP.025 (001.032)</ta>
         </annotation>
         <annotation name="st" tierref="st-KiPP" />
         <annotation name="ts" tierref="ts-KiPP">
            <ta e="T5" id="Seg_747" s="T1">– Baːta hu͡organ bejebit tigebit. </ta>
            <ta e="T11" id="Seg_748" s="T6">Itte barɨta bejebit tikteːččibit oččogo. </ta>
            <ta e="T15" id="Seg_749" s="T11">Gatoːbaj laːpkɨga hu͡ok bu͡o. </ta>
            <ta e="T26" id="Seg_750" s="T16">Osku͡olaga kiːremmit usku͡olaga mastʼerskojga, skola ogotugar taŋas tigeːččibit, köstʼümnarɨ, pɨlaːtɨjalarɨ. </ta>
            <ta e="T30" id="Seg_751" s="T26">Onu barɨtɨn hatɨːbɨn min. </ta>
            <ta e="T42" id="Seg_752" s="T31">((NOISE)) Haka ületin da hatɨːbɨn (iːste-), nʼuččalɨː da taŋahɨ tigeːččibin etim urut. </ta>
            <ta e="T47" id="Seg_753" s="T42">Anɨ tuːgu da gɨna koppoppun. </ta>
            <ta e="T59" id="Seg_754" s="T48">Kiniːtterim üčügej bu͡olan olorobun, ((NOISE)) kiniːpper küččügüj kiniːpper (olor-), ortoku kiniːkke olorobun. </ta>
            <ta e="T65" id="Seg_755" s="T59">Barɨ͡ak ebippin, koppoppun, hir ajɨ ogoloːkpun. ((LAUGH))</ta>
            <ta e="T71" id="Seg_756" s="T65">Hir ajɨ baːllar ((NOISE)) da kɨ͡ajan tiːjbeppin. ((NOISE)) </ta>
            <ta e="T75" id="Seg_757" s="T71">Gu͡orakka (bara-) bagarbappɨn törüt. </ta>
            <ta e="T82" id="Seg_758" s="T75">((NOISE)) Tundraga törüt, tundraga ere bagarabɨn. </ta>
            <ta e="T84" id="Seg_759" s="T83">Hürdeːk. </ta>
            <ta e="T89" id="Seg_760" s="T85">Aː, tugu di͡eppinij, elete. </ta>
            <ta e="T97" id="Seg_761" s="T91">– Araː, kaja anɨ da bagarabɨn, kotummappɨn. </ta>
            <ta e="T100" id="Seg_762" s="T97">Bulkuj, bulkuj, ((…)). </ta>
            <ta e="T104" id="Seg_763" s="T100">Ülehitter keleller, avsʼebɨgɨ kɨjnʼarabɨt. </ta>
            <ta e="T114" id="Seg_764" s="T107">– Üčügej bu͡o, tundra kördük him biːr pasʼolakka. </ta>
            <ta e="T117" id="Seg_765" s="T115">– Ej. </ta>
            <ta e="T124" id="Seg_766" s="T122">– Ahaːtɨnnar. </ta>
            <ta e="T144" id="Seg_767" s="T127">– Ol baːr ostol. </ta>
            <ta e="T134" id="Seg_768" s="T130">Ogolor, ordoroːruŋ ((NOISE)) u͡ol ülehittergitiger. </ta>
            <ta e="T137" id="Seg_769" s="T134">Ahat, ahat ogoloru. </ta>
            <ta e="T140" id="Seg_770" s="T138">Dʼe elete. </ta>
            <ta e="T143" id="Seg_771" s="T142">– Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KiPP">
            <ta e="T2" id="Seg_772" s="T1">baːta</ta>
            <ta e="T3" id="Seg_773" s="T2">hu͡organ</ta>
            <ta e="T4" id="Seg_774" s="T3">beje-bit</ta>
            <ta e="T5" id="Seg_775" s="T4">tig-e-bit</ta>
            <ta e="T7" id="Seg_776" s="T6">itte</ta>
            <ta e="T8" id="Seg_777" s="T7">barɨ-ta</ta>
            <ta e="T9" id="Seg_778" s="T8">beje-bit</ta>
            <ta e="T10" id="Seg_779" s="T9">tik-teː-čči-bit</ta>
            <ta e="T11" id="Seg_780" s="T10">oččogo</ta>
            <ta e="T12" id="Seg_781" s="T11">gatoːbaj</ta>
            <ta e="T13" id="Seg_782" s="T12">laːpkɨ-ga</ta>
            <ta e="T14" id="Seg_783" s="T13">hu͡ok</ta>
            <ta e="T15" id="Seg_784" s="T14">bu͡o</ta>
            <ta e="T17" id="Seg_785" s="T16">osku͡ola-ga</ta>
            <ta e="T18" id="Seg_786" s="T17">kiːr-em-mit</ta>
            <ta e="T19" id="Seg_787" s="T18">usku͡ola-ga</ta>
            <ta e="T20" id="Seg_788" s="T19">mastʼerskoj-ga</ta>
            <ta e="T21" id="Seg_789" s="T20">skola</ta>
            <ta e="T22" id="Seg_790" s="T21">ogo-tu-gar</ta>
            <ta e="T23" id="Seg_791" s="T22">taŋas</ta>
            <ta e="T24" id="Seg_792" s="T23">tig-eːčči-bit</ta>
            <ta e="T25" id="Seg_793" s="T24">köstʼüm-nar-ɨ</ta>
            <ta e="T26" id="Seg_794" s="T25">pɨlaːtɨja-lar-ɨ</ta>
            <ta e="T27" id="Seg_795" s="T26">o-nu</ta>
            <ta e="T28" id="Seg_796" s="T27">barɨ-tɨ-n</ta>
            <ta e="T29" id="Seg_797" s="T28">hat-ɨː-bɨn</ta>
            <ta e="T30" id="Seg_798" s="T29">min</ta>
            <ta e="T32" id="Seg_799" s="T31">haka</ta>
            <ta e="T33" id="Seg_800" s="T32">üle-ti-n</ta>
            <ta e="T34" id="Seg_801" s="T33">da</ta>
            <ta e="T35" id="Seg_802" s="T34">hat-ɨː-bɨn</ta>
            <ta e="T37" id="Seg_803" s="T36">nʼučča-lɨː</ta>
            <ta e="T38" id="Seg_804" s="T37">da</ta>
            <ta e="T39" id="Seg_805" s="T38">taŋah-ɨ</ta>
            <ta e="T40" id="Seg_806" s="T39">tig-eːčči-bin</ta>
            <ta e="T41" id="Seg_807" s="T40">e-ti-m</ta>
            <ta e="T42" id="Seg_808" s="T41">urut</ta>
            <ta e="T43" id="Seg_809" s="T42">anɨ</ta>
            <ta e="T44" id="Seg_810" s="T43">tuːg-u</ta>
            <ta e="T45" id="Seg_811" s="T44">da</ta>
            <ta e="T46" id="Seg_812" s="T45">gɨn-a</ta>
            <ta e="T47" id="Seg_813" s="T46">kop-pop-pun</ta>
            <ta e="T49" id="Seg_814" s="T48">kiniːt-ter-i-m</ta>
            <ta e="T50" id="Seg_815" s="T49">üčügej</ta>
            <ta e="T51" id="Seg_816" s="T50">bu͡ol-an</ta>
            <ta e="T52" id="Seg_817" s="T51">olor-o-bun</ta>
            <ta e="T53" id="Seg_818" s="T52">kiniːp-pe-r</ta>
            <ta e="T54" id="Seg_819" s="T53">küččügüj</ta>
            <ta e="T55" id="Seg_820" s="T54">kiniːp-pe-r</ta>
            <ta e="T56" id="Seg_821" s="T55">olor</ta>
            <ta e="T57" id="Seg_822" s="T56">orto-ku</ta>
            <ta e="T58" id="Seg_823" s="T57">kiniːk-ke</ta>
            <ta e="T59" id="Seg_824" s="T58">olor-o-bun</ta>
            <ta e="T60" id="Seg_825" s="T59">bar-ɨ͡ak</ta>
            <ta e="T61" id="Seg_826" s="T60">e-bip-pin</ta>
            <ta e="T62" id="Seg_827" s="T61">kop-pop-pun</ta>
            <ta e="T63" id="Seg_828" s="T62">hir</ta>
            <ta e="T64" id="Seg_829" s="T63">ajɨ</ta>
            <ta e="T65" id="Seg_830" s="T64">ogo-loːk-pun</ta>
            <ta e="T66" id="Seg_831" s="T65">hir</ta>
            <ta e="T67" id="Seg_832" s="T66">ajɨ</ta>
            <ta e="T68" id="Seg_833" s="T67">baːl-lar</ta>
            <ta e="T69" id="Seg_834" s="T68">da</ta>
            <ta e="T70" id="Seg_835" s="T69">kɨ͡aj-an</ta>
            <ta e="T71" id="Seg_836" s="T70">tiːj-bep-pin</ta>
            <ta e="T72" id="Seg_837" s="T71">gu͡orak-ka</ta>
            <ta e="T74" id="Seg_838" s="T73">bagar-bap-pɨn</ta>
            <ta e="T75" id="Seg_839" s="T74">törüt</ta>
            <ta e="T76" id="Seg_840" s="T75">tundra-ga</ta>
            <ta e="T78" id="Seg_841" s="T76">törüt</ta>
            <ta e="T80" id="Seg_842" s="T78">tundra-ga</ta>
            <ta e="T81" id="Seg_843" s="T80">ere</ta>
            <ta e="T82" id="Seg_844" s="T81">bagar-a-bɨn</ta>
            <ta e="T84" id="Seg_845" s="T83">hürdeːk</ta>
            <ta e="T86" id="Seg_846" s="T85">aː</ta>
            <ta e="T87" id="Seg_847" s="T86">tug-u</ta>
            <ta e="T88" id="Seg_848" s="T87">d-i͡ep-pin=ij</ta>
            <ta e="T89" id="Seg_849" s="T88">ele-te</ta>
            <ta e="T92" id="Seg_850" s="T91">araː</ta>
            <ta e="T93" id="Seg_851" s="T92">kaja</ta>
            <ta e="T94" id="Seg_852" s="T93">anɨ</ta>
            <ta e="T95" id="Seg_853" s="T94">da</ta>
            <ta e="T96" id="Seg_854" s="T95">bagar-a-bɨn</ta>
            <ta e="T97" id="Seg_855" s="T96">kot-u-m-map-pɨn</ta>
            <ta e="T98" id="Seg_856" s="T97">bulkuj</ta>
            <ta e="T99" id="Seg_857" s="T98">bulkuj</ta>
            <ta e="T101" id="Seg_858" s="T100">ülehit-ter</ta>
            <ta e="T102" id="Seg_859" s="T101">kel-el-ler</ta>
            <ta e="T103" id="Seg_860" s="T102">avsʼebɨg-ɨ</ta>
            <ta e="T104" id="Seg_861" s="T103">kɨjnʼ-a-r-a-bɨt</ta>
            <ta e="T108" id="Seg_862" s="T107">üčügej</ta>
            <ta e="T109" id="Seg_863" s="T108">bu͡o</ta>
            <ta e="T110" id="Seg_864" s="T109">tundra</ta>
            <ta e="T111" id="Seg_865" s="T110">kördük</ta>
            <ta e="T112" id="Seg_866" s="T111">him</ta>
            <ta e="T113" id="Seg_867" s="T112">biːr</ta>
            <ta e="T114" id="Seg_868" s="T113">pasʼolak-ka</ta>
            <ta e="T117" id="Seg_869" s="T115">ej</ta>
            <ta e="T124" id="Seg_870" s="T122">ahaː-tɨnnar</ta>
            <ta e="T128" id="Seg_871" s="T127">ol</ta>
            <ta e="T129" id="Seg_872" s="T128">baːr</ta>
            <ta e="T144" id="Seg_873" s="T129">ostol</ta>
            <ta e="T131" id="Seg_874" s="T130">ogo-lor</ta>
            <ta e="T132" id="Seg_875" s="T131">ord-o-r-oːr-u-ŋ</ta>
            <ta e="T133" id="Seg_876" s="T132">u͡ol</ta>
            <ta e="T134" id="Seg_877" s="T133">ülehit-ter-giti-ger</ta>
            <ta e="T135" id="Seg_878" s="T134">ah-a-t</ta>
            <ta e="T136" id="Seg_879" s="T135">ah-a-t</ta>
            <ta e="T137" id="Seg_880" s="T136">ogo-lor-u</ta>
            <ta e="T139" id="Seg_881" s="T138">dʼe</ta>
            <ta e="T140" id="Seg_882" s="T139">ele-te</ta>
            <ta e="T143" id="Seg_883" s="T142">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KiPP">
            <ta e="T2" id="Seg_884" s="T1">baːta</ta>
            <ta e="T3" id="Seg_885" s="T2">hu͡organ</ta>
            <ta e="T4" id="Seg_886" s="T3">beje-BIt</ta>
            <ta e="T5" id="Seg_887" s="T4">tik-A-BIt</ta>
            <ta e="T7" id="Seg_888" s="T6">itte</ta>
            <ta e="T8" id="Seg_889" s="T7">barɨ-tA</ta>
            <ta e="T9" id="Seg_890" s="T8">beje-BIt</ta>
            <ta e="T10" id="Seg_891" s="T9">tik-TAː-AːččI-BIt</ta>
            <ta e="T11" id="Seg_892" s="T10">oččogo</ta>
            <ta e="T12" id="Seg_893" s="T11">gotu͡obaj</ta>
            <ta e="T13" id="Seg_894" s="T12">laːpkɨ-GA</ta>
            <ta e="T14" id="Seg_895" s="T13">hu͡ok</ta>
            <ta e="T15" id="Seg_896" s="T14">bu͡o</ta>
            <ta e="T17" id="Seg_897" s="T16">usku͡ola-GA</ta>
            <ta e="T18" id="Seg_898" s="T17">kiːr-An-BIt</ta>
            <ta e="T19" id="Seg_899" s="T18">usku͡ola-GA</ta>
            <ta e="T20" id="Seg_900" s="T19">mastersku͡oj-GA</ta>
            <ta e="T21" id="Seg_901" s="T20">usku͡ola</ta>
            <ta e="T22" id="Seg_902" s="T21">ogo-tI-GAr</ta>
            <ta e="T23" id="Seg_903" s="T22">taŋas</ta>
            <ta e="T24" id="Seg_904" s="T23">tik-AːččI-BIt</ta>
            <ta e="T25" id="Seg_905" s="T24">köstüːm-LAr-nI</ta>
            <ta e="T26" id="Seg_906" s="T25">platʼje-LAr-nI</ta>
            <ta e="T27" id="Seg_907" s="T26">ol-nI</ta>
            <ta e="T28" id="Seg_908" s="T27">barɨ-tI-n</ta>
            <ta e="T29" id="Seg_909" s="T28">hataː-A-BIn</ta>
            <ta e="T30" id="Seg_910" s="T29">min</ta>
            <ta e="T32" id="Seg_911" s="T31">haka</ta>
            <ta e="T33" id="Seg_912" s="T32">üle-tI-n</ta>
            <ta e="T34" id="Seg_913" s="T33">da</ta>
            <ta e="T35" id="Seg_914" s="T34">hataː-A-BIn</ta>
            <ta e="T37" id="Seg_915" s="T36">nuːčča-LIː</ta>
            <ta e="T38" id="Seg_916" s="T37">da</ta>
            <ta e="T39" id="Seg_917" s="T38">taŋas-nI</ta>
            <ta e="T40" id="Seg_918" s="T39">tik-AːččI-BIn</ta>
            <ta e="T41" id="Seg_919" s="T40">e-TI-m</ta>
            <ta e="T42" id="Seg_920" s="T41">urut</ta>
            <ta e="T43" id="Seg_921" s="T42">anɨ</ta>
            <ta e="T44" id="Seg_922" s="T43">tu͡ok-nI</ta>
            <ta e="T45" id="Seg_923" s="T44">da</ta>
            <ta e="T46" id="Seg_924" s="T45">gɨn-A</ta>
            <ta e="T47" id="Seg_925" s="T46">kot-BAT-BIn</ta>
            <ta e="T49" id="Seg_926" s="T48">kiniːt-LAr-I-m</ta>
            <ta e="T50" id="Seg_927" s="T49">üčügej</ta>
            <ta e="T51" id="Seg_928" s="T50">bu͡ol-An</ta>
            <ta e="T52" id="Seg_929" s="T51">olor-A-BIn</ta>
            <ta e="T53" id="Seg_930" s="T52">kiniːt-BA-r</ta>
            <ta e="T54" id="Seg_931" s="T53">küččügüj</ta>
            <ta e="T55" id="Seg_932" s="T54">kiniːt-BA-r</ta>
            <ta e="T56" id="Seg_933" s="T55">olor</ta>
            <ta e="T57" id="Seg_934" s="T56">orto-GI</ta>
            <ta e="T58" id="Seg_935" s="T57">kiniːt-GA</ta>
            <ta e="T59" id="Seg_936" s="T58">olor-A-BIn</ta>
            <ta e="T60" id="Seg_937" s="T59">bar-IAK</ta>
            <ta e="T61" id="Seg_938" s="T60">e-BIT-BIn</ta>
            <ta e="T62" id="Seg_939" s="T61">kot-BAT-BIn</ta>
            <ta e="T63" id="Seg_940" s="T62">hir</ta>
            <ta e="T64" id="Seg_941" s="T63">aːjɨ</ta>
            <ta e="T65" id="Seg_942" s="T64">ogo-LAːK-BIn</ta>
            <ta e="T66" id="Seg_943" s="T65">hir</ta>
            <ta e="T67" id="Seg_944" s="T66">aːjɨ</ta>
            <ta e="T68" id="Seg_945" s="T67">baːr-LAr</ta>
            <ta e="T69" id="Seg_946" s="T68">da</ta>
            <ta e="T70" id="Seg_947" s="T69">kɨ͡aj-An</ta>
            <ta e="T71" id="Seg_948" s="T70">tij-BAT-BIn</ta>
            <ta e="T72" id="Seg_949" s="T71">gu͡orat-GA</ta>
            <ta e="T74" id="Seg_950" s="T73">bagar-BAT-BIn</ta>
            <ta e="T75" id="Seg_951" s="T74">törüt</ta>
            <ta e="T76" id="Seg_952" s="T75">tundra-GA</ta>
            <ta e="T78" id="Seg_953" s="T76">törüt</ta>
            <ta e="T80" id="Seg_954" s="T78">tundra-GA</ta>
            <ta e="T81" id="Seg_955" s="T80">ere</ta>
            <ta e="T82" id="Seg_956" s="T81">bagar-A-BIn</ta>
            <ta e="T84" id="Seg_957" s="T83">hürdeːk</ta>
            <ta e="T86" id="Seg_958" s="T85">aː</ta>
            <ta e="T87" id="Seg_959" s="T86">tu͡ok-nI</ta>
            <ta e="T88" id="Seg_960" s="T87">di͡e-IAK-BIn=Ij</ta>
            <ta e="T89" id="Seg_961" s="T88">ele-tA</ta>
            <ta e="T92" id="Seg_962" s="T91">araː</ta>
            <ta e="T93" id="Seg_963" s="T92">kaja</ta>
            <ta e="T94" id="Seg_964" s="T93">anɨ</ta>
            <ta e="T95" id="Seg_965" s="T94">da</ta>
            <ta e="T96" id="Seg_966" s="T95">bagar-A-BIn</ta>
            <ta e="T97" id="Seg_967" s="T96">kot-I-n-BAT-BIn</ta>
            <ta e="T98" id="Seg_968" s="T97">bulkuj</ta>
            <ta e="T99" id="Seg_969" s="T98">bulkuj</ta>
            <ta e="T101" id="Seg_970" s="T100">ülehit-LAr</ta>
            <ta e="T102" id="Seg_971" s="T101">kel-Ar-LAr</ta>
            <ta e="T103" id="Seg_972" s="T102">avsʼebɨk-nI</ta>
            <ta e="T104" id="Seg_973" s="T103">kɨːj-A-r-A-BIt</ta>
            <ta e="T108" id="Seg_974" s="T107">üčügej</ta>
            <ta e="T109" id="Seg_975" s="T108">bu͡o</ta>
            <ta e="T110" id="Seg_976" s="T109">tundra</ta>
            <ta e="T111" id="Seg_977" s="T110">kördük</ta>
            <ta e="T112" id="Seg_978" s="T111">hin</ta>
            <ta e="T113" id="Seg_979" s="T112">biːr</ta>
            <ta e="T114" id="Seg_980" s="T113">pasʼolak-GA</ta>
            <ta e="T117" id="Seg_981" s="T115">eː</ta>
            <ta e="T124" id="Seg_982" s="T122">ahaː-TInnAr</ta>
            <ta e="T128" id="Seg_983" s="T127">ol</ta>
            <ta e="T129" id="Seg_984" s="T128">baːr</ta>
            <ta e="T144" id="Seg_985" s="T129">ostu͡ol</ta>
            <ta e="T131" id="Seg_986" s="T130">ogo-LAr</ta>
            <ta e="T132" id="Seg_987" s="T131">ort-A-r-Aːr-I-ŋ</ta>
            <ta e="T133" id="Seg_988" s="T132">u͡ol</ta>
            <ta e="T134" id="Seg_989" s="T133">ülehit-LAr-GItI-GAr</ta>
            <ta e="T135" id="Seg_990" s="T134">ahaː-A-t</ta>
            <ta e="T136" id="Seg_991" s="T135">ahaː-A-t</ta>
            <ta e="T137" id="Seg_992" s="T136">ogo-LAr-nI</ta>
            <ta e="T139" id="Seg_993" s="T138">dʼe</ta>
            <ta e="T140" id="Seg_994" s="T139">ele-tA</ta>
            <ta e="T143" id="Seg_995" s="T142">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KiPP">
            <ta e="T2" id="Seg_996" s="T1">cotton</ta>
            <ta e="T3" id="Seg_997" s="T2">blanket.[NOM]</ta>
            <ta e="T4" id="Seg_998" s="T3">self-1PL.[NOM]</ta>
            <ta e="T5" id="Seg_999" s="T4">sew-PRS-1PL</ta>
            <ta e="T7" id="Seg_1000" s="T6">EMPH</ta>
            <ta e="T8" id="Seg_1001" s="T7">whole-3SG.[NOM]</ta>
            <ta e="T9" id="Seg_1002" s="T8">self-1PL.[NOM]</ta>
            <ta e="T10" id="Seg_1003" s="T9">sew-ITER-HAB-1PL</ta>
            <ta e="T11" id="Seg_1004" s="T10">then</ta>
            <ta e="T12" id="Seg_1005" s="T11">prepared.[NOM]</ta>
            <ta e="T13" id="Seg_1006" s="T12">shop-DAT/LOC</ta>
            <ta e="T14" id="Seg_1007" s="T13">NEG.EX</ta>
            <ta e="T15" id="Seg_1008" s="T14">EMPH</ta>
            <ta e="T17" id="Seg_1009" s="T16">school-DAT/LOC</ta>
            <ta e="T18" id="Seg_1010" s="T17">go.in-CVB.SEQ-1PL</ta>
            <ta e="T19" id="Seg_1011" s="T18">school-DAT/LOC</ta>
            <ta e="T20" id="Seg_1012" s="T19">workshop-DAT/LOC</ta>
            <ta e="T21" id="Seg_1013" s="T20">school.[NOM]</ta>
            <ta e="T22" id="Seg_1014" s="T21">child-3SG-DAT/LOC</ta>
            <ta e="T23" id="Seg_1015" s="T22">clothes.[NOM]</ta>
            <ta e="T24" id="Seg_1016" s="T23">sew-HAB-1PL</ta>
            <ta e="T25" id="Seg_1017" s="T24">suit-PL-ACC</ta>
            <ta e="T26" id="Seg_1018" s="T25">dress-PL-ACC</ta>
            <ta e="T27" id="Seg_1019" s="T26">that-ACC</ta>
            <ta e="T28" id="Seg_1020" s="T27">whole-3SG-ACC</ta>
            <ta e="T29" id="Seg_1021" s="T28">can-PRS-1SG</ta>
            <ta e="T30" id="Seg_1022" s="T29">1SG.[NOM]</ta>
            <ta e="T32" id="Seg_1023" s="T31">Dolgan</ta>
            <ta e="T33" id="Seg_1024" s="T32">work-3SG-ACC</ta>
            <ta e="T34" id="Seg_1025" s="T33">and</ta>
            <ta e="T35" id="Seg_1026" s="T34">can-PRS-1SG</ta>
            <ta e="T37" id="Seg_1027" s="T36">Russian-SIM</ta>
            <ta e="T38" id="Seg_1028" s="T37">and</ta>
            <ta e="T39" id="Seg_1029" s="T38">clothes-ACC</ta>
            <ta e="T40" id="Seg_1030" s="T39">sew-HAB-1SG</ta>
            <ta e="T41" id="Seg_1031" s="T40">be-PST1-1SG</ta>
            <ta e="T42" id="Seg_1032" s="T41">before</ta>
            <ta e="T43" id="Seg_1033" s="T42">now</ta>
            <ta e="T44" id="Seg_1034" s="T43">what-ACC</ta>
            <ta e="T45" id="Seg_1035" s="T44">NEG</ta>
            <ta e="T46" id="Seg_1036" s="T45">make-CVB.SIM</ta>
            <ta e="T47" id="Seg_1037" s="T46">make.it-NEG-1SG</ta>
            <ta e="T49" id="Seg_1038" s="T48">daughter_in_law-PL-EP-1SG.[NOM]</ta>
            <ta e="T50" id="Seg_1039" s="T49">good.[NOM]</ta>
            <ta e="T51" id="Seg_1040" s="T50">be-CVB.SEQ</ta>
            <ta e="T52" id="Seg_1041" s="T51">live-PRS-1SG</ta>
            <ta e="T53" id="Seg_1042" s="T52">daughter_in_law-1SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_1043" s="T53">small</ta>
            <ta e="T55" id="Seg_1044" s="T54">daughter_in_law-1SG-DAT/LOC</ta>
            <ta e="T56" id="Seg_1045" s="T55">live</ta>
            <ta e="T57" id="Seg_1046" s="T56">middle-ADJZ</ta>
            <ta e="T58" id="Seg_1047" s="T57">daughter_in_law-DAT/LOC</ta>
            <ta e="T59" id="Seg_1048" s="T58">live-PRS-1SG</ta>
            <ta e="T60" id="Seg_1049" s="T59">go-PTCP.FUT</ta>
            <ta e="T61" id="Seg_1050" s="T60">be-PST2-1SG</ta>
            <ta e="T62" id="Seg_1051" s="T61">make.it-NEG-1SG</ta>
            <ta e="T63" id="Seg_1052" s="T62">earth.[NOM]</ta>
            <ta e="T64" id="Seg_1053" s="T63">every</ta>
            <ta e="T65" id="Seg_1054" s="T64">child-PROPR-1SG</ta>
            <ta e="T66" id="Seg_1055" s="T65">earth.[NOM]</ta>
            <ta e="T67" id="Seg_1056" s="T66">every</ta>
            <ta e="T68" id="Seg_1057" s="T67">there.is-3PL</ta>
            <ta e="T69" id="Seg_1058" s="T68">and</ta>
            <ta e="T70" id="Seg_1059" s="T69">can-CVB.SEQ</ta>
            <ta e="T71" id="Seg_1060" s="T70">reach-NEG-1SG</ta>
            <ta e="T72" id="Seg_1061" s="T71">city-DAT/LOC</ta>
            <ta e="T74" id="Seg_1062" s="T73">want-NEG-1SG</ta>
            <ta e="T75" id="Seg_1063" s="T74">at.all</ta>
            <ta e="T76" id="Seg_1064" s="T75">tundra-DAT/LOC</ta>
            <ta e="T78" id="Seg_1065" s="T76">at.all</ta>
            <ta e="T80" id="Seg_1066" s="T78">tundra-DAT/LOC</ta>
            <ta e="T81" id="Seg_1067" s="T80">just</ta>
            <ta e="T82" id="Seg_1068" s="T81">want-PRS-1SG</ta>
            <ta e="T84" id="Seg_1069" s="T83">very</ta>
            <ta e="T86" id="Seg_1070" s="T85">ah</ta>
            <ta e="T87" id="Seg_1071" s="T86">what-ACC</ta>
            <ta e="T88" id="Seg_1072" s="T87">say-FUT-1SG=Q</ta>
            <ta e="T89" id="Seg_1073" s="T88">last-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_1074" s="T91">oh.dear</ta>
            <ta e="T93" id="Seg_1075" s="T92">what.kind.of</ta>
            <ta e="T94" id="Seg_1076" s="T93">now</ta>
            <ta e="T95" id="Seg_1077" s="T94">EMPH</ta>
            <ta e="T96" id="Seg_1078" s="T95">want-PRS-1SG</ta>
            <ta e="T97" id="Seg_1079" s="T96">make.it-EP-MED-NEG-1SG</ta>
            <ta e="T98" id="Seg_1080" s="T97">mix.[IMP.2SG]</ta>
            <ta e="T99" id="Seg_1081" s="T98">mix.[IMP.2SG]</ta>
            <ta e="T101" id="Seg_1082" s="T100">worker-PL.[NOM]</ta>
            <ta e="T102" id="Seg_1083" s="T101">come-PRS-3PL</ta>
            <ta e="T103" id="Seg_1084" s="T102">muskox-ACC</ta>
            <ta e="T104" id="Seg_1085" s="T103">boil-EP-CAUS-PRS-1PL</ta>
            <ta e="T108" id="Seg_1086" s="T107">good.[NOM]</ta>
            <ta e="T109" id="Seg_1087" s="T108">EMPH</ta>
            <ta e="T110" id="Seg_1088" s="T109">tundra.[NOM]</ta>
            <ta e="T111" id="Seg_1089" s="T110">similar</ta>
            <ta e="T112" id="Seg_1090" s="T111">however</ta>
            <ta e="T113" id="Seg_1091" s="T112">one</ta>
            <ta e="T114" id="Seg_1092" s="T113">village-DAT/LOC</ta>
            <ta e="T117" id="Seg_1093" s="T115">AFFIRM</ta>
            <ta e="T124" id="Seg_1094" s="T122">eat-IMP.3PL</ta>
            <ta e="T128" id="Seg_1095" s="T127">that</ta>
            <ta e="T129" id="Seg_1096" s="T128">there.is</ta>
            <ta e="T144" id="Seg_1097" s="T129">table.[NOM]</ta>
            <ta e="T131" id="Seg_1098" s="T130">child-PL.[NOM]</ta>
            <ta e="T132" id="Seg_1099" s="T131">remain-EP-CAUS-FUT-EP-IMP.2PL</ta>
            <ta e="T133" id="Seg_1100" s="T132">boy.[NOM]</ta>
            <ta e="T134" id="Seg_1101" s="T133">worker-PL-2PL-DAT/LOC</ta>
            <ta e="T135" id="Seg_1102" s="T134">eat-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T136" id="Seg_1103" s="T135">eat-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T137" id="Seg_1104" s="T136">child-PL-ACC</ta>
            <ta e="T139" id="Seg_1105" s="T138">well</ta>
            <ta e="T140" id="Seg_1106" s="T139">last-3SG.[NOM]</ta>
            <ta e="T143" id="Seg_1107" s="T142">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KiPP">
            <ta e="T2" id="Seg_1108" s="T1">Watte</ta>
            <ta e="T3" id="Seg_1109" s="T2">Bettdecke.[NOM]</ta>
            <ta e="T4" id="Seg_1110" s="T3">selbst-1PL.[NOM]</ta>
            <ta e="T5" id="Seg_1111" s="T4">nähen-PRS-1PL</ta>
            <ta e="T7" id="Seg_1112" s="T6">EMPH</ta>
            <ta e="T8" id="Seg_1113" s="T7">ganz-3SG.[NOM]</ta>
            <ta e="T9" id="Seg_1114" s="T8">selbst-1PL.[NOM]</ta>
            <ta e="T10" id="Seg_1115" s="T9">nähen-ITER-HAB-1PL</ta>
            <ta e="T11" id="Seg_1116" s="T10">dann</ta>
            <ta e="T12" id="Seg_1117" s="T11">fertig.[NOM]</ta>
            <ta e="T13" id="Seg_1118" s="T12">Laden-DAT/LOC</ta>
            <ta e="T14" id="Seg_1119" s="T13">NEG.EX</ta>
            <ta e="T15" id="Seg_1120" s="T14">EMPH</ta>
            <ta e="T17" id="Seg_1121" s="T16">Schule-DAT/LOC</ta>
            <ta e="T18" id="Seg_1122" s="T17">hineingehen-CVB.SEQ-1PL</ta>
            <ta e="T19" id="Seg_1123" s="T18">Schule-DAT/LOC</ta>
            <ta e="T20" id="Seg_1124" s="T19">Werkstatt-DAT/LOC</ta>
            <ta e="T21" id="Seg_1125" s="T20">Schule.[NOM]</ta>
            <ta e="T22" id="Seg_1126" s="T21">Kind-3SG-DAT/LOC</ta>
            <ta e="T23" id="Seg_1127" s="T22">Kleidung.[NOM]</ta>
            <ta e="T24" id="Seg_1128" s="T23">nähen-HAB-1PL</ta>
            <ta e="T25" id="Seg_1129" s="T24">Anzug-PL-ACC</ta>
            <ta e="T26" id="Seg_1130" s="T25">Kleid-PL-ACC</ta>
            <ta e="T27" id="Seg_1131" s="T26">jenes-ACC</ta>
            <ta e="T28" id="Seg_1132" s="T27">ganz-3SG-ACC</ta>
            <ta e="T29" id="Seg_1133" s="T28">können-PRS-1SG</ta>
            <ta e="T30" id="Seg_1134" s="T29">1SG.[NOM]</ta>
            <ta e="T32" id="Seg_1135" s="T31">dolganisch</ta>
            <ta e="T33" id="Seg_1136" s="T32">Arbeit-3SG-ACC</ta>
            <ta e="T34" id="Seg_1137" s="T33">und</ta>
            <ta e="T35" id="Seg_1138" s="T34">können-PRS-1SG</ta>
            <ta e="T37" id="Seg_1139" s="T36">russisch-SIM</ta>
            <ta e="T38" id="Seg_1140" s="T37">und</ta>
            <ta e="T39" id="Seg_1141" s="T38">Kleidung-ACC</ta>
            <ta e="T40" id="Seg_1142" s="T39">nähen-HAB-1SG</ta>
            <ta e="T41" id="Seg_1143" s="T40">sein-PST1-1SG</ta>
            <ta e="T42" id="Seg_1144" s="T41">früher</ta>
            <ta e="T43" id="Seg_1145" s="T42">jetzt</ta>
            <ta e="T44" id="Seg_1146" s="T43">was-ACC</ta>
            <ta e="T45" id="Seg_1147" s="T44">NEG</ta>
            <ta e="T46" id="Seg_1148" s="T45">machen-CVB.SIM</ta>
            <ta e="T47" id="Seg_1149" s="T46">schaffen-NEG-1SG</ta>
            <ta e="T49" id="Seg_1150" s="T48">Schwiegertochter-PL-EP-1SG.[NOM]</ta>
            <ta e="T50" id="Seg_1151" s="T49">gut.[NOM]</ta>
            <ta e="T51" id="Seg_1152" s="T50">sein-CVB.SEQ</ta>
            <ta e="T52" id="Seg_1153" s="T51">leben-PRS-1SG</ta>
            <ta e="T53" id="Seg_1154" s="T52">Schwiegertochter-1SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_1155" s="T53">klein</ta>
            <ta e="T55" id="Seg_1156" s="T54">Schwiegertochter-1SG-DAT/LOC</ta>
            <ta e="T56" id="Seg_1157" s="T55">leben</ta>
            <ta e="T57" id="Seg_1158" s="T56">Mitte-ADJZ</ta>
            <ta e="T58" id="Seg_1159" s="T57">Schwiegertochter-DAT/LOC</ta>
            <ta e="T59" id="Seg_1160" s="T58">leben-PRS-1SG</ta>
            <ta e="T60" id="Seg_1161" s="T59">gehen-PTCP.FUT</ta>
            <ta e="T61" id="Seg_1162" s="T60">sein-PST2-1SG</ta>
            <ta e="T62" id="Seg_1163" s="T61">schaffen-NEG-1SG</ta>
            <ta e="T63" id="Seg_1164" s="T62">Erde.[NOM]</ta>
            <ta e="T64" id="Seg_1165" s="T63">jeder</ta>
            <ta e="T65" id="Seg_1166" s="T64">Kind-PROPR-1SG</ta>
            <ta e="T66" id="Seg_1167" s="T65">Erde.[NOM]</ta>
            <ta e="T67" id="Seg_1168" s="T66">jeder</ta>
            <ta e="T68" id="Seg_1169" s="T67">es.gibt-3PL</ta>
            <ta e="T69" id="Seg_1170" s="T68">und</ta>
            <ta e="T70" id="Seg_1171" s="T69">können-CVB.SEQ</ta>
            <ta e="T71" id="Seg_1172" s="T70">ankommen-NEG-1SG</ta>
            <ta e="T72" id="Seg_1173" s="T71">Stadt-DAT/LOC</ta>
            <ta e="T74" id="Seg_1174" s="T73">wollen-NEG-1SG</ta>
            <ta e="T75" id="Seg_1175" s="T74">ganz</ta>
            <ta e="T76" id="Seg_1176" s="T75">Tundra-DAT/LOC</ta>
            <ta e="T78" id="Seg_1177" s="T76">ganz</ta>
            <ta e="T80" id="Seg_1178" s="T78">Tundra-DAT/LOC</ta>
            <ta e="T81" id="Seg_1179" s="T80">nur</ta>
            <ta e="T82" id="Seg_1180" s="T81">wollen-PRS-1SG</ta>
            <ta e="T84" id="Seg_1181" s="T83">sehr</ta>
            <ta e="T86" id="Seg_1182" s="T85">ah</ta>
            <ta e="T87" id="Seg_1183" s="T86">was-ACC</ta>
            <ta e="T88" id="Seg_1184" s="T87">sagen-FUT-1SG=Q</ta>
            <ta e="T89" id="Seg_1185" s="T88">letzter-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_1186" s="T91">oh.nein</ta>
            <ta e="T93" id="Seg_1187" s="T92">was.für.ein</ta>
            <ta e="T94" id="Seg_1188" s="T93">jetzt</ta>
            <ta e="T95" id="Seg_1189" s="T94">EMPH</ta>
            <ta e="T96" id="Seg_1190" s="T95">wollen-PRS-1SG</ta>
            <ta e="T97" id="Seg_1191" s="T96">schaffen-EP-MED-NEG-1SG</ta>
            <ta e="T98" id="Seg_1192" s="T97">mischen.[IMP.2SG]</ta>
            <ta e="T99" id="Seg_1193" s="T98">mischen.[IMP.2SG]</ta>
            <ta e="T101" id="Seg_1194" s="T100">Arbeiter-PL.[NOM]</ta>
            <ta e="T102" id="Seg_1195" s="T101">kommen-PRS-3PL</ta>
            <ta e="T103" id="Seg_1196" s="T102">Moschusochse-ACC</ta>
            <ta e="T104" id="Seg_1197" s="T103">kochen-EP-CAUS-PRS-1PL</ta>
            <ta e="T108" id="Seg_1198" s="T107">gut.[NOM]</ta>
            <ta e="T109" id="Seg_1199" s="T108">EMPH</ta>
            <ta e="T110" id="Seg_1200" s="T109">Tundra.[NOM]</ta>
            <ta e="T111" id="Seg_1201" s="T110">ähnlich</ta>
            <ta e="T112" id="Seg_1202" s="T111">doch</ta>
            <ta e="T113" id="Seg_1203" s="T112">eins</ta>
            <ta e="T114" id="Seg_1204" s="T113">Dorf-DAT/LOC</ta>
            <ta e="T117" id="Seg_1205" s="T115">AFFIRM</ta>
            <ta e="T124" id="Seg_1206" s="T122">essen-IMP.3PL</ta>
            <ta e="T128" id="Seg_1207" s="T127">jenes</ta>
            <ta e="T129" id="Seg_1208" s="T128">es.gibt</ta>
            <ta e="T144" id="Seg_1209" s="T129">Tisch.[NOM]</ta>
            <ta e="T131" id="Seg_1210" s="T130">Kind-PL.[NOM]</ta>
            <ta e="T132" id="Seg_1211" s="T131">übrig.bleiben-EP-CAUS-FUT-EP-IMP.2PL</ta>
            <ta e="T133" id="Seg_1212" s="T132">Junge.[NOM]</ta>
            <ta e="T134" id="Seg_1213" s="T133">Arbeiter-PL-2PL-DAT/LOC</ta>
            <ta e="T135" id="Seg_1214" s="T134">essen-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T136" id="Seg_1215" s="T135">essen-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T137" id="Seg_1216" s="T136">Kind-PL-ACC</ta>
            <ta e="T139" id="Seg_1217" s="T138">doch</ta>
            <ta e="T140" id="Seg_1218" s="T139">letzter-3SG.[NOM]</ta>
            <ta e="T143" id="Seg_1219" s="T142">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KiPP">
            <ta e="T2" id="Seg_1220" s="T1">вата</ta>
            <ta e="T3" id="Seg_1221" s="T2">одеяло.[NOM]</ta>
            <ta e="T4" id="Seg_1222" s="T3">сам-1PL.[NOM]</ta>
            <ta e="T5" id="Seg_1223" s="T4">шить-PRS-1PL</ta>
            <ta e="T7" id="Seg_1224" s="T6">EMPH</ta>
            <ta e="T8" id="Seg_1225" s="T7">целый-3SG.[NOM]</ta>
            <ta e="T9" id="Seg_1226" s="T8">сам-1PL.[NOM]</ta>
            <ta e="T10" id="Seg_1227" s="T9">шить-ITER-HAB-1PL</ta>
            <ta e="T11" id="Seg_1228" s="T10">тогда</ta>
            <ta e="T12" id="Seg_1229" s="T11">готовый.[NOM]</ta>
            <ta e="T13" id="Seg_1230" s="T12">лапка-DAT/LOC</ta>
            <ta e="T14" id="Seg_1231" s="T13">NEG.EX</ta>
            <ta e="T15" id="Seg_1232" s="T14">EMPH</ta>
            <ta e="T17" id="Seg_1233" s="T16">школа-DAT/LOC</ta>
            <ta e="T18" id="Seg_1234" s="T17">входить-CVB.SEQ-1PL</ta>
            <ta e="T19" id="Seg_1235" s="T18">школа-DAT/LOC</ta>
            <ta e="T20" id="Seg_1236" s="T19">мастерская-DAT/LOC</ta>
            <ta e="T21" id="Seg_1237" s="T20">школа.[NOM]</ta>
            <ta e="T22" id="Seg_1238" s="T21">ребенок-3SG-DAT/LOC</ta>
            <ta e="T23" id="Seg_1239" s="T22">одежда.[NOM]</ta>
            <ta e="T24" id="Seg_1240" s="T23">шить-HAB-1PL</ta>
            <ta e="T25" id="Seg_1241" s="T24">костюм-PL-ACC</ta>
            <ta e="T26" id="Seg_1242" s="T25">платье-PL-ACC</ta>
            <ta e="T27" id="Seg_1243" s="T26">тот-ACC</ta>
            <ta e="T28" id="Seg_1244" s="T27">целый-3SG-ACC</ta>
            <ta e="T29" id="Seg_1245" s="T28">мочь-PRS-1SG</ta>
            <ta e="T30" id="Seg_1246" s="T29">1SG.[NOM]</ta>
            <ta e="T32" id="Seg_1247" s="T31">долганский</ta>
            <ta e="T33" id="Seg_1248" s="T32">работа-3SG-ACC</ta>
            <ta e="T34" id="Seg_1249" s="T33">да</ta>
            <ta e="T35" id="Seg_1250" s="T34">мочь-PRS-1SG</ta>
            <ta e="T37" id="Seg_1251" s="T36">русский-SIM</ta>
            <ta e="T38" id="Seg_1252" s="T37">да</ta>
            <ta e="T39" id="Seg_1253" s="T38">одежда-ACC</ta>
            <ta e="T40" id="Seg_1254" s="T39">шить-HAB-1SG</ta>
            <ta e="T41" id="Seg_1255" s="T40">быть-PST1-1SG</ta>
            <ta e="T42" id="Seg_1256" s="T41">раньше</ta>
            <ta e="T43" id="Seg_1257" s="T42">теперь</ta>
            <ta e="T44" id="Seg_1258" s="T43">что-ACC</ta>
            <ta e="T45" id="Seg_1259" s="T44">NEG</ta>
            <ta e="T46" id="Seg_1260" s="T45">делать-CVB.SIM</ta>
            <ta e="T47" id="Seg_1261" s="T46">мочь-NEG-1SG</ta>
            <ta e="T49" id="Seg_1262" s="T48">невестка-PL-EP-1SG.[NOM]</ta>
            <ta e="T50" id="Seg_1263" s="T49">хороший.[NOM]</ta>
            <ta e="T51" id="Seg_1264" s="T50">быть-CVB.SEQ</ta>
            <ta e="T52" id="Seg_1265" s="T51">жить-PRS-1SG</ta>
            <ta e="T53" id="Seg_1266" s="T52">невестка-1SG-DAT/LOC</ta>
            <ta e="T54" id="Seg_1267" s="T53">маленький</ta>
            <ta e="T55" id="Seg_1268" s="T54">невестка-1SG-DAT/LOC</ta>
            <ta e="T56" id="Seg_1269" s="T55">жить</ta>
            <ta e="T57" id="Seg_1270" s="T56">середина-ADJZ</ta>
            <ta e="T58" id="Seg_1271" s="T57">невестка-DAT/LOC</ta>
            <ta e="T59" id="Seg_1272" s="T58">жить-PRS-1SG</ta>
            <ta e="T60" id="Seg_1273" s="T59">идти-PTCP.FUT</ta>
            <ta e="T61" id="Seg_1274" s="T60">быть-PST2-1SG</ta>
            <ta e="T62" id="Seg_1275" s="T61">мочь-NEG-1SG</ta>
            <ta e="T63" id="Seg_1276" s="T62">земля.[NOM]</ta>
            <ta e="T64" id="Seg_1277" s="T63">каждый</ta>
            <ta e="T65" id="Seg_1278" s="T64">ребенок-PROPR-1SG</ta>
            <ta e="T66" id="Seg_1279" s="T65">земля.[NOM]</ta>
            <ta e="T67" id="Seg_1280" s="T66">каждый</ta>
            <ta e="T68" id="Seg_1281" s="T67">есть-3PL</ta>
            <ta e="T69" id="Seg_1282" s="T68">да</ta>
            <ta e="T70" id="Seg_1283" s="T69">мочь-CVB.SEQ</ta>
            <ta e="T71" id="Seg_1284" s="T70">доезжать-NEG-1SG</ta>
            <ta e="T72" id="Seg_1285" s="T71">город-DAT/LOC</ta>
            <ta e="T74" id="Seg_1286" s="T73">хотеть-NEG-1SG</ta>
            <ta e="T75" id="Seg_1287" s="T74">совсем</ta>
            <ta e="T76" id="Seg_1288" s="T75">тундра-DAT/LOC</ta>
            <ta e="T78" id="Seg_1289" s="T76">совсем</ta>
            <ta e="T80" id="Seg_1290" s="T78">тундра-DAT/LOC</ta>
            <ta e="T81" id="Seg_1291" s="T80">только</ta>
            <ta e="T82" id="Seg_1292" s="T81">хотеть-PRS-1SG</ta>
            <ta e="T84" id="Seg_1293" s="T83">очень</ta>
            <ta e="T86" id="Seg_1294" s="T85">ах</ta>
            <ta e="T87" id="Seg_1295" s="T86">что-ACC</ta>
            <ta e="T88" id="Seg_1296" s="T87">говорить-FUT-1SG=Q</ta>
            <ta e="T89" id="Seg_1297" s="T88">последний-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_1298" s="T91">вот.беда</ta>
            <ta e="T93" id="Seg_1299" s="T92">какой</ta>
            <ta e="T94" id="Seg_1300" s="T93">теперь</ta>
            <ta e="T95" id="Seg_1301" s="T94">EMPH</ta>
            <ta e="T96" id="Seg_1302" s="T95">хотеть-PRS-1SG</ta>
            <ta e="T97" id="Seg_1303" s="T96">мочь-EP-MED-NEG-1SG</ta>
            <ta e="T98" id="Seg_1304" s="T97">мешать.[IMP.2SG]</ta>
            <ta e="T99" id="Seg_1305" s="T98">мешать.[IMP.2SG]</ta>
            <ta e="T101" id="Seg_1306" s="T100">работник-PL.[NOM]</ta>
            <ta e="T102" id="Seg_1307" s="T101">приходить-PRS-3PL</ta>
            <ta e="T103" id="Seg_1308" s="T102">овцебык-ACC</ta>
            <ta e="T104" id="Seg_1309" s="T103">кипеть-EP-CAUS-PRS-1PL</ta>
            <ta e="T108" id="Seg_1310" s="T107">хороший.[NOM]</ta>
            <ta e="T109" id="Seg_1311" s="T108">EMPH</ta>
            <ta e="T110" id="Seg_1312" s="T109">тундра.[NOM]</ta>
            <ta e="T111" id="Seg_1313" s="T110">подобно</ta>
            <ta e="T112" id="Seg_1314" s="T111">ведь</ta>
            <ta e="T113" id="Seg_1315" s="T112">один</ta>
            <ta e="T114" id="Seg_1316" s="T113">поселок-DAT/LOC</ta>
            <ta e="T117" id="Seg_1317" s="T115">AFFIRM</ta>
            <ta e="T124" id="Seg_1318" s="T122">есть-IMP.3PL</ta>
            <ta e="T128" id="Seg_1319" s="T127">тот</ta>
            <ta e="T129" id="Seg_1320" s="T128">есть</ta>
            <ta e="T144" id="Seg_1321" s="T129">стол.[NOM]</ta>
            <ta e="T131" id="Seg_1322" s="T130">ребенок-PL.[NOM]</ta>
            <ta e="T132" id="Seg_1323" s="T131">быть.лишным-EP-CAUS-FUT-EP-IMP.2PL</ta>
            <ta e="T133" id="Seg_1324" s="T132">мальчик.[NOM]</ta>
            <ta e="T134" id="Seg_1325" s="T133">работник-PL-2PL-DAT/LOC</ta>
            <ta e="T135" id="Seg_1326" s="T134">есть-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T136" id="Seg_1327" s="T135">есть-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T137" id="Seg_1328" s="T136">ребенок-PL-ACC</ta>
            <ta e="T139" id="Seg_1329" s="T138">вот</ta>
            <ta e="T140" id="Seg_1330" s="T139">последний-3SG.[NOM]</ta>
            <ta e="T143" id="Seg_1331" s="T142">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KiPP">
            <ta e="T2" id="Seg_1332" s="T1">n</ta>
            <ta e="T3" id="Seg_1333" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_1334" s="T3">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T5" id="Seg_1335" s="T4">v-v:tense-v:pred.pn</ta>
            <ta e="T7" id="Seg_1336" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_1337" s="T7">adj-n:(poss).[n:case]</ta>
            <ta e="T9" id="Seg_1338" s="T8">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T10" id="Seg_1339" s="T9">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T11" id="Seg_1340" s="T10">adv</ta>
            <ta e="T12" id="Seg_1341" s="T11">adj.[n:case]</ta>
            <ta e="T13" id="Seg_1342" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_1343" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_1344" s="T14">ptcl</ta>
            <ta e="T17" id="Seg_1345" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1346" s="T17">v-v:cvb-v:pred.pn</ta>
            <ta e="T19" id="Seg_1347" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_1348" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_1349" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_1350" s="T21">n-n:poss-n:case</ta>
            <ta e="T23" id="Seg_1351" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_1352" s="T23">v-v:mood-v:pred.pn</ta>
            <ta e="T25" id="Seg_1353" s="T24">n-n:(num)-n:case</ta>
            <ta e="T26" id="Seg_1354" s="T25">n-n:(num)-n:case</ta>
            <ta e="T27" id="Seg_1355" s="T26">dempro-pro:case</ta>
            <ta e="T28" id="Seg_1356" s="T27">adj-n:poss-n:case</ta>
            <ta e="T29" id="Seg_1357" s="T28">v-v:tense-v:pred.pn</ta>
            <ta e="T30" id="Seg_1358" s="T29">pers.[pro:case]</ta>
            <ta e="T32" id="Seg_1359" s="T31">adj</ta>
            <ta e="T33" id="Seg_1360" s="T32">n-n:poss-n:case</ta>
            <ta e="T34" id="Seg_1361" s="T33">conj</ta>
            <ta e="T35" id="Seg_1362" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T37" id="Seg_1363" s="T36">adj-adj&gt;adv</ta>
            <ta e="T38" id="Seg_1364" s="T37">conj</ta>
            <ta e="T39" id="Seg_1365" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1366" s="T39">v-v:mood-v:pred.pn</ta>
            <ta e="T41" id="Seg_1367" s="T40">v-v:tense-v:poss.pn</ta>
            <ta e="T42" id="Seg_1368" s="T41">adv</ta>
            <ta e="T43" id="Seg_1369" s="T42">adv</ta>
            <ta e="T44" id="Seg_1370" s="T43">que-pro:case</ta>
            <ta e="T45" id="Seg_1371" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_1372" s="T45">v-v:cvb</ta>
            <ta e="T47" id="Seg_1373" s="T46">v-v:(neg)-v:pred.pn</ta>
            <ta e="T49" id="Seg_1374" s="T48">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T50" id="Seg_1375" s="T49">adj.[n:case]</ta>
            <ta e="T51" id="Seg_1376" s="T50">v-v:cvb</ta>
            <ta e="T52" id="Seg_1377" s="T51">v-v:tense-v:pred.pn</ta>
            <ta e="T53" id="Seg_1378" s="T52">n-n:poss-n:case</ta>
            <ta e="T54" id="Seg_1379" s="T53">adj</ta>
            <ta e="T55" id="Seg_1380" s="T54">n-n:poss-n:case</ta>
            <ta e="T56" id="Seg_1381" s="T55">v</ta>
            <ta e="T57" id="Seg_1382" s="T56">n-n&gt;adj</ta>
            <ta e="T58" id="Seg_1383" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_1384" s="T58">v-v:tense-v:pred.pn</ta>
            <ta e="T60" id="Seg_1385" s="T59">v-v:ptcp</ta>
            <ta e="T61" id="Seg_1386" s="T60">v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_1387" s="T61">v-v:(neg)-v:pred.pn</ta>
            <ta e="T63" id="Seg_1388" s="T62">n.[n:case]</ta>
            <ta e="T64" id="Seg_1389" s="T63">adj</ta>
            <ta e="T65" id="Seg_1390" s="T64">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T66" id="Seg_1391" s="T65">n.[n:case]</ta>
            <ta e="T67" id="Seg_1392" s="T66">adj</ta>
            <ta e="T68" id="Seg_1393" s="T67">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T69" id="Seg_1394" s="T68">conj</ta>
            <ta e="T70" id="Seg_1395" s="T69">v-v:cvb</ta>
            <ta e="T71" id="Seg_1396" s="T70">v-v:(neg)-v:pred.pn</ta>
            <ta e="T72" id="Seg_1397" s="T71">n-n:case</ta>
            <ta e="T74" id="Seg_1398" s="T73">v-v:(neg)-v:pred.pn</ta>
            <ta e="T75" id="Seg_1399" s="T74">adv</ta>
            <ta e="T76" id="Seg_1400" s="T75">n-n:case</ta>
            <ta e="T78" id="Seg_1401" s="T76">adv</ta>
            <ta e="T80" id="Seg_1402" s="T78">n-n:case</ta>
            <ta e="T81" id="Seg_1403" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1404" s="T81">v-v:tense-v:pred.pn</ta>
            <ta e="T84" id="Seg_1405" s="T83">adv</ta>
            <ta e="T86" id="Seg_1406" s="T85">interj</ta>
            <ta e="T87" id="Seg_1407" s="T86">que-pro:case</ta>
            <ta e="T88" id="Seg_1408" s="T87">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T89" id="Seg_1409" s="T88">adj-n:(poss).[n:case]</ta>
            <ta e="T92" id="Seg_1410" s="T91">interj</ta>
            <ta e="T93" id="Seg_1411" s="T92">que</ta>
            <ta e="T94" id="Seg_1412" s="T93">adv</ta>
            <ta e="T95" id="Seg_1413" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_1414" s="T95">v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_1415" s="T96">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T98" id="Seg_1416" s="T97">v.[v:mood.pn]</ta>
            <ta e="T99" id="Seg_1417" s="T98">v.[v:mood.pn]</ta>
            <ta e="T101" id="Seg_1418" s="T100">n-n:(num).[n:case]</ta>
            <ta e="T102" id="Seg_1419" s="T101">v-v:tense-v:pred.pn</ta>
            <ta e="T103" id="Seg_1420" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_1421" s="T103">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T108" id="Seg_1422" s="T107">adj.[n:case]</ta>
            <ta e="T109" id="Seg_1423" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1424" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1425" s="T110">post</ta>
            <ta e="T112" id="Seg_1426" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_1427" s="T112">cardnum</ta>
            <ta e="T114" id="Seg_1428" s="T113">n-n:case</ta>
            <ta e="T117" id="Seg_1429" s="T115">ptcl</ta>
            <ta e="T124" id="Seg_1430" s="T122">v-v:mood.pn</ta>
            <ta e="T128" id="Seg_1431" s="T127">dempro</ta>
            <ta e="T129" id="Seg_1432" s="T128">ptcl</ta>
            <ta e="T144" id="Seg_1433" s="T129">n.[n:case]</ta>
            <ta e="T131" id="Seg_1434" s="T130">n-n:(num).[n:case]</ta>
            <ta e="T132" id="Seg_1435" s="T131">v-v:(ins)-v&gt;v-v:(tense)-v:(ins)-v:mood.pn</ta>
            <ta e="T133" id="Seg_1436" s="T132">n.[n:case]</ta>
            <ta e="T134" id="Seg_1437" s="T133">n-n:(num)-n:poss-n:case</ta>
            <ta e="T135" id="Seg_1438" s="T134">v-v:(ins)-v&gt;v.[v:mood.pn]</ta>
            <ta e="T136" id="Seg_1439" s="T135">v-v:(ins)-v&gt;v.[v:mood.pn]</ta>
            <ta e="T137" id="Seg_1440" s="T136">n-n:(num)-n:case</ta>
            <ta e="T139" id="Seg_1441" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_1442" s="T139">adj-n:(poss).[n:case]</ta>
            <ta e="T143" id="Seg_1443" s="T142">adj-n:(poss).[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KiPP">
            <ta e="T2" id="Seg_1444" s="T1">n</ta>
            <ta e="T3" id="Seg_1445" s="T2">n</ta>
            <ta e="T4" id="Seg_1446" s="T3">emphpro</ta>
            <ta e="T5" id="Seg_1447" s="T4">v</ta>
            <ta e="T7" id="Seg_1448" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_1449" s="T7">adj</ta>
            <ta e="T9" id="Seg_1450" s="T8">emphpro</ta>
            <ta e="T10" id="Seg_1451" s="T9">v</ta>
            <ta e="T11" id="Seg_1452" s="T10">adv</ta>
            <ta e="T12" id="Seg_1453" s="T11">adj</ta>
            <ta e="T13" id="Seg_1454" s="T12">n</ta>
            <ta e="T14" id="Seg_1455" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_1456" s="T14">ptcl</ta>
            <ta e="T17" id="Seg_1457" s="T16">n</ta>
            <ta e="T18" id="Seg_1458" s="T17">v</ta>
            <ta e="T19" id="Seg_1459" s="T18">n</ta>
            <ta e="T20" id="Seg_1460" s="T19">n</ta>
            <ta e="T21" id="Seg_1461" s="T20">n</ta>
            <ta e="T22" id="Seg_1462" s="T21">n</ta>
            <ta e="T23" id="Seg_1463" s="T22">n</ta>
            <ta e="T24" id="Seg_1464" s="T23">v</ta>
            <ta e="T25" id="Seg_1465" s="T24">n</ta>
            <ta e="T26" id="Seg_1466" s="T25">n</ta>
            <ta e="T27" id="Seg_1467" s="T26">dempro</ta>
            <ta e="T28" id="Seg_1468" s="T27">adj</ta>
            <ta e="T29" id="Seg_1469" s="T28">v</ta>
            <ta e="T30" id="Seg_1470" s="T29">pers</ta>
            <ta e="T32" id="Seg_1471" s="T31">adj</ta>
            <ta e="T33" id="Seg_1472" s="T32">n</ta>
            <ta e="T34" id="Seg_1473" s="T33">conj</ta>
            <ta e="T35" id="Seg_1474" s="T34">v</ta>
            <ta e="T37" id="Seg_1475" s="T36">adv</ta>
            <ta e="T38" id="Seg_1476" s="T37">conj</ta>
            <ta e="T39" id="Seg_1477" s="T38">n</ta>
            <ta e="T40" id="Seg_1478" s="T39">v</ta>
            <ta e="T41" id="Seg_1479" s="T40">aux</ta>
            <ta e="T42" id="Seg_1480" s="T41">adv</ta>
            <ta e="T43" id="Seg_1481" s="T42">adv</ta>
            <ta e="T44" id="Seg_1482" s="T43">que</ta>
            <ta e="T45" id="Seg_1483" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_1484" s="T45">v</ta>
            <ta e="T47" id="Seg_1485" s="T46">v</ta>
            <ta e="T49" id="Seg_1486" s="T48">n</ta>
            <ta e="T50" id="Seg_1487" s="T49">adj</ta>
            <ta e="T51" id="Seg_1488" s="T50">cop</ta>
            <ta e="T52" id="Seg_1489" s="T51">v</ta>
            <ta e="T53" id="Seg_1490" s="T52">n</ta>
            <ta e="T54" id="Seg_1491" s="T53">adj</ta>
            <ta e="T55" id="Seg_1492" s="T54">n</ta>
            <ta e="T56" id="Seg_1493" s="T55">v</ta>
            <ta e="T57" id="Seg_1494" s="T56">adj</ta>
            <ta e="T58" id="Seg_1495" s="T57">n</ta>
            <ta e="T59" id="Seg_1496" s="T58">v</ta>
            <ta e="T60" id="Seg_1497" s="T59">v</ta>
            <ta e="T61" id="Seg_1498" s="T60">aux</ta>
            <ta e="T62" id="Seg_1499" s="T61">v</ta>
            <ta e="T63" id="Seg_1500" s="T62">n</ta>
            <ta e="T64" id="Seg_1501" s="T63">adj</ta>
            <ta e="T65" id="Seg_1502" s="T64">adj</ta>
            <ta e="T66" id="Seg_1503" s="T65">n</ta>
            <ta e="T67" id="Seg_1504" s="T66">adj</ta>
            <ta e="T68" id="Seg_1505" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_1506" s="T68">conj</ta>
            <ta e="T70" id="Seg_1507" s="T69">v</ta>
            <ta e="T71" id="Seg_1508" s="T70">v</ta>
            <ta e="T72" id="Seg_1509" s="T71">n</ta>
            <ta e="T74" id="Seg_1510" s="T73">v</ta>
            <ta e="T75" id="Seg_1511" s="T74">adv</ta>
            <ta e="T76" id="Seg_1512" s="T75">n</ta>
            <ta e="T78" id="Seg_1513" s="T76">adv</ta>
            <ta e="T80" id="Seg_1514" s="T78">n</ta>
            <ta e="T81" id="Seg_1515" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1516" s="T81">v</ta>
            <ta e="T84" id="Seg_1517" s="T83">adv</ta>
            <ta e="T86" id="Seg_1518" s="T85">interj</ta>
            <ta e="T87" id="Seg_1519" s="T86">que</ta>
            <ta e="T88" id="Seg_1520" s="T87">v</ta>
            <ta e="T89" id="Seg_1521" s="T88">adj</ta>
            <ta e="T92" id="Seg_1522" s="T91">interj</ta>
            <ta e="T93" id="Seg_1523" s="T92">que</ta>
            <ta e="T94" id="Seg_1524" s="T93">adv</ta>
            <ta e="T95" id="Seg_1525" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_1526" s="T95">v</ta>
            <ta e="T97" id="Seg_1527" s="T96">v</ta>
            <ta e="T98" id="Seg_1528" s="T97">v</ta>
            <ta e="T99" id="Seg_1529" s="T98">v</ta>
            <ta e="T101" id="Seg_1530" s="T100">n</ta>
            <ta e="T102" id="Seg_1531" s="T101">v</ta>
            <ta e="T103" id="Seg_1532" s="T102">n</ta>
            <ta e="T104" id="Seg_1533" s="T103">v</ta>
            <ta e="T108" id="Seg_1534" s="T107">adj</ta>
            <ta e="T109" id="Seg_1535" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1536" s="T109">n</ta>
            <ta e="T111" id="Seg_1537" s="T110">post</ta>
            <ta e="T112" id="Seg_1538" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_1539" s="T112">cardnum</ta>
            <ta e="T114" id="Seg_1540" s="T113">n</ta>
            <ta e="T117" id="Seg_1541" s="T115">ptcl</ta>
            <ta e="T124" id="Seg_1542" s="T122">v</ta>
            <ta e="T128" id="Seg_1543" s="T127">dempro</ta>
            <ta e="T129" id="Seg_1544" s="T128">ptcl</ta>
            <ta e="T144" id="Seg_1545" s="T129">n</ta>
            <ta e="T131" id="Seg_1546" s="T130">n</ta>
            <ta e="T132" id="Seg_1547" s="T131">v</ta>
            <ta e="T133" id="Seg_1548" s="T132">n</ta>
            <ta e="T134" id="Seg_1549" s="T133">n</ta>
            <ta e="T135" id="Seg_1550" s="T134">v</ta>
            <ta e="T136" id="Seg_1551" s="T135">v</ta>
            <ta e="T137" id="Seg_1552" s="T136">n</ta>
            <ta e="T139" id="Seg_1553" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_1554" s="T139">adj</ta>
            <ta e="T143" id="Seg_1555" s="T142">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KiPP" />
         <annotation name="SyF" tierref="SyF-KiPP" />
         <annotation name="IST" tierref="IST-KiPP" />
         <annotation name="Top" tierref="Top-KiPP" />
         <annotation name="Foc" tierref="Foc-KiPP" />
         <annotation name="BOR" tierref="BOR-KiPP">
            <ta e="T2" id="Seg_1556" s="T1">RUS:cult</ta>
            <ta e="T12" id="Seg_1557" s="T11">RUS:cult</ta>
            <ta e="T13" id="Seg_1558" s="T12">RUS:cult</ta>
            <ta e="T17" id="Seg_1559" s="T16">RUS:cult</ta>
            <ta e="T19" id="Seg_1560" s="T18">RUS:cult</ta>
            <ta e="T20" id="Seg_1561" s="T19">RUS:cult</ta>
            <ta e="T21" id="Seg_1562" s="T20">RUS:cult</ta>
            <ta e="T25" id="Seg_1563" s="T24">RUS:cult</ta>
            <ta e="T26" id="Seg_1564" s="T25">RUS:cult</ta>
            <ta e="T34" id="Seg_1565" s="T33">RUS:gram</ta>
            <ta e="T38" id="Seg_1566" s="T37">RUS:gram</ta>
            <ta e="T69" id="Seg_1567" s="T68">RUS:gram</ta>
            <ta e="T72" id="Seg_1568" s="T71">RUS:cult</ta>
            <ta e="T76" id="Seg_1569" s="T75">RUS:core</ta>
            <ta e="T80" id="Seg_1570" s="T78">RUS:core</ta>
            <ta e="T103" id="Seg_1571" s="T102">RUS:cult</ta>
            <ta e="T110" id="Seg_1572" s="T109">RUS:core</ta>
            <ta e="T114" id="Seg_1573" s="T113">RUS:cult</ta>
            <ta e="T144" id="Seg_1574" s="T129">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KiPP" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KiPP" />
         <annotation name="CS" tierref="CS-KiPP" />
         <annotation name="fe" tierref="fe-KiPP">
            <ta e="T5" id="Seg_1575" s="T1">– Cotton blankets we do sew ourselves.</ta>
            <ta e="T11" id="Seg_1576" s="T6">We sew everything ourselves, then.</ta>
            <ta e="T15" id="Seg_1577" s="T11">There is nothing prepared in the shop.</ta>
            <ta e="T26" id="Seg_1578" s="T16">As we came to the school, to the workshop [i.e., went working there], we sewed clothes for the school children: suits, dresses.</ta>
            <ta e="T30" id="Seg_1579" s="T26">All that I am able to do.</ta>
            <ta e="T42" id="Seg_1580" s="T31">Dolgan work I also can do, earlier I sewed Russian clothes.</ta>
            <ta e="T47" id="Seg_1581" s="T42">Now I am not able to do anything.</ta>
            <ta e="T59" id="Seg_1582" s="T48">My daughters-in-law are good, so I live, I live with my daughter-in-law, with my small daughter-in-law, with the middle daughter-in-law I am living.</ta>
            <ta e="T65" id="Seg_1583" s="T59">I would go, but I am not able to, I have children everywhere.</ta>
            <ta e="T71" id="Seg_1584" s="T65">They are everywhere I can't go there.</ta>
            <ta e="T75" id="Seg_1585" s="T71">To town I don't want at all.</ta>
            <ta e="T82" id="Seg_1586" s="T75">I want to the tundra, only to the tundra.</ta>
            <ta e="T84" id="Seg_1587" s="T83">– Very.</ta>
            <ta e="T89" id="Seg_1588" s="T85">Ah, what shall I say, enough.</ta>
            <ta e="T97" id="Seg_1589" s="T91">– Oh yes, even now I want, but I am not able to.</ta>
            <ta e="T100" id="Seg_1590" s="T97">Mix, mix (…).</ta>
            <ta e="T104" id="Seg_1591" s="T100">The workers come, we are cooking a muskox.</ta>
            <ta e="T114" id="Seg_1592" s="T107">– It is good, it is like tundra in the village, though.</ta>
            <ta e="T117" id="Seg_1593" s="T115">– Yeah.</ta>
            <ta e="T124" id="Seg_1594" s="T122">– They shall eat.</ta>
            <ta e="T144" id="Seg_1595" s="T127">– Here is the table.</ta>
            <ta e="T134" id="Seg_1596" s="T130">Children, leave something for the working guys.</ta>
            <ta e="T137" id="Seg_1597" s="T134">Feed, feed the children.</ta>
            <ta e="T140" id="Seg_1598" s="T138">Well, enough.</ta>
            <ta e="T143" id="Seg_1599" s="T142">– That's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KiPP">
            <ta e="T5" id="Seg_1600" s="T1">– Wattedecken nähen wir selber. </ta>
            <ta e="T11" id="Seg_1601" s="T6">Das nähen wir dann alles selber. </ta>
            <ta e="T15" id="Seg_1602" s="T11">Fertig im Laden gibt es nichts.</ta>
            <ta e="T26" id="Seg_1603" s="T16">Als wir in die Schule, in der Werkstatt kamen [d.h., Arbeit dort bekamen], nähten wir den Schulkindern Kleidung: Anzüge, Kleider.</ta>
            <ta e="T30" id="Seg_1604" s="T26">Das alles kann ich.</ta>
            <ta e="T42" id="Seg_1605" s="T31">Dolganische Arbeit kann ich auch, russische Kleidung habe ich früher genäht.</ta>
            <ta e="T47" id="Seg_1606" s="T42">Jetzt kann ich gar nichts mehr machen.</ta>
            <ta e="T59" id="Seg_1607" s="T48">Weil meine Schwiegertöchter gut sind, lebe ich, ich lebe bei meiner Schwiegertochter, bei meiner kleinen Schwiegertochter, bei der mittleren Schwiegertochter lebe ich.</ta>
            <ta e="T65" id="Seg_1608" s="T59">Ich würde fahren, ich kann es nicht, überall habe ich Kinder.</ta>
            <ta e="T71" id="Seg_1609" s="T65">Sie sind überall und ich komme nicht hin.</ta>
            <ta e="T75" id="Seg_1610" s="T71">In die Stadt möchte ich überhaupt nicht.</ta>
            <ta e="T82" id="Seg_1611" s="T75">In die Tundra, nur in die Tundra möchte.</ta>
            <ta e="T84" id="Seg_1612" s="T83">– Sehr.</ta>
            <ta e="T89" id="Seg_1613" s="T85">Ach, was soll ich sagen, Schluss.</ta>
            <ta e="T97" id="Seg_1614" s="T91">– Und wie, sogar jetzt möchte ich, ich schaffe es nicht.</ta>
            <ta e="T100" id="Seg_1615" s="T97">Vermisch das, vermisch das (…).</ta>
            <ta e="T104" id="Seg_1616" s="T100">Die Arbeiter kommen, wir kochen einen Moschusochsen.</ta>
            <ta e="T114" id="Seg_1617" s="T107">– Es ist gut, es ist doch wie Tundra im Dorf.</ta>
            <ta e="T117" id="Seg_1618" s="T115">– Ja.</ta>
            <ta e="T124" id="Seg_1619" s="T122">– Sie sollen essen.</ta>
            <ta e="T144" id="Seg_1620" s="T127">– Hier ist der Tisch.</ta>
            <ta e="T134" id="Seg_1621" s="T130">Kinder, lasst was für die Arbeiterjungs übrig.</ta>
            <ta e="T137" id="Seg_1622" s="T134">Gib den Kindern zu essen, gib ihnen zu essen.</ta>
            <ta e="T140" id="Seg_1623" s="T138">Nun, Schluss.</ta>
            <ta e="T143" id="Seg_1624" s="T142">– Das war's.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KiPP">
            <ta e="T5" id="Seg_1625" s="T1">– Ватное одеяло мы сами шьем.</ta>
            <ta e="T11" id="Seg_1626" s="T6">Мы все сами шьем, тогда.</ta>
            <ta e="T15" id="Seg_1627" s="T11">Готового в магазине нет.</ta>
            <ta e="T26" id="Seg_1628" s="T16">Когда мы пришли [работать] в школу, в мастерскую, мы шили школьникам одежду: костюмы, платья.</ta>
            <ta e="T30" id="Seg_1629" s="T26">И все это я умею.</ta>
            <ta e="T42" id="Seg_1630" s="T31">И долганскую работу я знаю, и русскую одежду я шила, это было раньше.</ta>
            <ta e="T47" id="Seg_1631" s="T42">Теперь я ничего не могу делать.</ta>
            <ta e="T59" id="Seg_1632" s="T48">Благодаря моим хорошим невесткам я живу, у младшей невестки, у средней невестки я живу.</ta>
            <ta e="T65" id="Seg_1633" s="T59">Я бы поехала, но я не в состоянии, у меня повсюду дети.</ta>
            <ta e="T71" id="Seg_1634" s="T65">Они везде, да не могу доехать.</ta>
            <ta e="T75" id="Seg_1635" s="T71">Я совсем не хочу в город.</ta>
            <ta e="T82" id="Seg_1636" s="T75">В тундру, только в тундру я хочу.</ta>
            <ta e="T84" id="Seg_1637" s="T83">– Очень.</ta>
            <ta e="T89" id="Seg_1638" s="T85">Ну что я скажу, всё.</ta>
            <ta e="T97" id="Seg_1639" s="T91">– Еще как, и теперь я хочу, но не могу.</ta>
            <ta e="T100" id="Seg_1640" s="T97">Мешай, мешай.</ta>
            <ta e="T104" id="Seg_1641" s="T100">k prК приходу рабочих варим овцебыка.</ta>
            <ta e="T114" id="Seg_1642" s="T107">– Хорошо, в поселке хорошо, как в тундре.</ta>
            <ta e="T117" id="Seg_1643" s="T115">ej</ta>
            <ta e="T124" id="Seg_1644" s="T122">– Пусть они поедят.</ta>
            <ta e="T144" id="Seg_1645" s="T127">– Вот есть стол.</ta>
            <ta e="T134" id="Seg_1646" s="T130">Детям, парням работающим оставьте.</ta>
            <ta e="T137" id="Seg_1647" s="T134">Корми, корми детей.</ta>
            <ta e="T140" id="Seg_1648" s="T138">Ну всё.</ta>
            <ta e="T143" id="Seg_1649" s="T142">– Всё.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-KiPP">
            <ta e="T5" id="Seg_1650" s="T1">vatnoeo odeljalo sami shjem</ta>
            <ta e="T11" id="Seg_1651" s="T6">vse eto sami shili togda</ta>
            <ta e="T15" id="Seg_1652" s="T11">gotovogo v magazine net</ta>
            <ta e="T26" id="Seg_1653" s="T16">ustroivshijs v shkolu v shkolnoj masterskoj, shkolnikam shili odezhdu shili: kostjumɨ platja</ta>
            <ta e="T30" id="Seg_1654" s="T26">i vse eto ja umeju</ta>
            <ta e="T42" id="Seg_1655" s="T31">dolganskuju rabotu (s mexom shkurami) umeju, shju russkuju odezhdu eto bɨlo rahshe </ta>
            <ta e="T47" id="Seg_1656" s="T42">teper ja nichego ne mogu osilit</ta>
            <ta e="T59" id="Seg_1657" s="T48">blagodarja horoshim nevestkam zhivu, u mladshej nevestki usrednej nevestki zhivu</ta>
            <ta e="T65" id="Seg_1658" s="T59">poehala bɨ ne v sostojanii vezde u menja deti</ta>
            <ta e="T71" id="Seg_1659" s="T65">oni est vezde, ne mogu doehat</ta>
            <ta e="T75" id="Seg_1660" s="T71">sovsem ne hochu v gorod</ta>
            <ta e="T82" id="Seg_1661" s="T75">hochu tolko v tundru</ta>
            <ta e="T84" id="Seg_1662" s="T83">silno</ta>
            <ta e="T89" id="Seg_1663" s="T85">nu chto ja skazhu, vse</ta>
            <ta e="T97" id="Seg_1664" s="T91">araa kaja seichas dazhe hochu, no mogu</ta>
            <ta e="T100" id="Seg_1665" s="T97">meshaj meshaj dochka</ta>
            <ta e="T104" id="Seg_1666" s="T100">k prihodu rabochih varim ovtsebɨka</ta>
            <ta e="T114" id="Seg_1667" s="T107">v poselke horosho kak v tundre</ta>
            <ta e="T117" id="Seg_1668" s="T115">ej</ta>
            <ta e="T124" id="Seg_1669" s="T122">poedjat</ta>
            <ta e="T144" id="Seg_1670" s="T127">vot est stol</ta>
            <ta e="T134" id="Seg_1671" s="T130">dejtjam ostavite parnjam rabotajushim</ta>
            <ta e="T137" id="Seg_1672" s="T134">kormi kormi detej</ta>
            <ta e="T140" id="Seg_1673" s="T138">nu vse</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KiPP" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-SE"
                      id="tx-SE"
                      speaker="SE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-SE">
            <ts e="T79" id="Seg_1674" n="sc" s="T77">
               <ts e="T79" id="Seg_1676" n="HIAT:u" s="T77">
                  <nts id="Seg_1677" n="HIAT:ip">–</nts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_1680" n="HIAT:w" s="T77">Bagarbakkɨn</ts>
                  <nts id="Seg_1681" n="HIAT:ip">.</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_1683" n="sc" s="T89">
               <ts e="T91" id="Seg_1685" n="HIAT:u" s="T89">
                  <nts id="Seg_1686" n="HIAT:ip">–</nts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_1689" n="HIAT:w" s="T89">Tundraga</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_1692" n="HIAT:w" s="T90">bagaragɨn</ts>
                  <nts id="Seg_1693" n="HIAT:ip">?</nts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T107" id="Seg_1695" n="sc" s="T105">
               <ts e="T107" id="Seg_1697" n="HIAT:u" s="T105">
                  <nts id="Seg_1698" n="HIAT:ip">–</nts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_1701" n="HIAT:w" s="T105">Pasʼolakka</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_1704" n="HIAT:w" s="T106">üčügej</ts>
                  <nts id="Seg_1705" n="HIAT:ip">?</nts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T142" id="Seg_1707" n="sc" s="T141">
               <ts e="T142" id="Seg_1709" n="HIAT:u" s="T141">
                  <nts id="Seg_1710" n="HIAT:ip">–</nts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_1713" n="HIAT:w" s="T141">Elete</ts>
                  <nts id="Seg_1714" n="HIAT:ip">?</nts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-SE">
            <ts e="T79" id="Seg_1716" n="sc" s="T77">
               <ts e="T79" id="Seg_1718" n="e" s="T77">– Bagarbakkɨn. </ts>
            </ts>
            <ts e="T91" id="Seg_1719" n="sc" s="T89">
               <ts e="T90" id="Seg_1721" n="e" s="T89">– Tundraga </ts>
               <ts e="T91" id="Seg_1723" n="e" s="T90">bagaragɨn? </ts>
            </ts>
            <ts e="T107" id="Seg_1724" n="sc" s="T105">
               <ts e="T106" id="Seg_1726" n="e" s="T105">– Pasʼolakka </ts>
               <ts e="T107" id="Seg_1728" n="e" s="T106">üčügej? </ts>
            </ts>
            <ts e="T142" id="Seg_1729" n="sc" s="T141">
               <ts e="T142" id="Seg_1731" n="e" s="T141">– Elete? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-SE">
            <ta e="T79" id="Seg_1732" s="T77">KiPP_XXXX_2009_Clothes_nar.ES.001 (001.013)</ta>
            <ta e="T91" id="Seg_1733" s="T89">KiPP_XXXX_2009_Clothes_nar.ES.002 (001.016)</ta>
            <ta e="T107" id="Seg_1734" s="T105">KiPP_XXXX_2009_Clothes_nar.ES.003 (001.020)</ta>
            <ta e="T142" id="Seg_1735" s="T141">KiPP_XXXX_2009_Clothes_nar.ES.004 (001.031)</ta>
         </annotation>
         <annotation name="st" tierref="st-SE" />
         <annotation name="ts" tierref="ts-SE">
            <ta e="T79" id="Seg_1736" s="T77">– Bagarbakkɨn. </ta>
            <ta e="T91" id="Seg_1737" s="T89">– Tundraga bagaragɨn? </ta>
            <ta e="T107" id="Seg_1738" s="T105">– Pasʼolakka üčügej? </ta>
            <ta e="T142" id="Seg_1739" s="T141">– Elete? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-SE">
            <ta e="T79" id="Seg_1740" s="T77">bagar-bak-kɨn</ta>
            <ta e="T90" id="Seg_1741" s="T89">tundra-ga</ta>
            <ta e="T91" id="Seg_1742" s="T90">bagar-a-gɨn</ta>
            <ta e="T106" id="Seg_1743" s="T105">pasʼolak-ka</ta>
            <ta e="T107" id="Seg_1744" s="T106">üčügej</ta>
            <ta e="T142" id="Seg_1745" s="T141">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp-SE">
            <ta e="T79" id="Seg_1746" s="T77">bagar-BAT-GIn</ta>
            <ta e="T90" id="Seg_1747" s="T89">tundra-GA</ta>
            <ta e="T91" id="Seg_1748" s="T90">bagar-A-GIn</ta>
            <ta e="T106" id="Seg_1749" s="T105">pasʼolak-GA</ta>
            <ta e="T107" id="Seg_1750" s="T106">üčügej</ta>
            <ta e="T142" id="Seg_1751" s="T141">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge-SE">
            <ta e="T79" id="Seg_1752" s="T77">want-NEG-2SG</ta>
            <ta e="T90" id="Seg_1753" s="T89">tundra-DAT/LOC</ta>
            <ta e="T91" id="Seg_1754" s="T90">want-PRS-2SG</ta>
            <ta e="T106" id="Seg_1755" s="T105">village-DAT/LOC</ta>
            <ta e="T107" id="Seg_1756" s="T106">good.[NOM]</ta>
            <ta e="T142" id="Seg_1757" s="T141">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-SE">
            <ta e="T79" id="Seg_1758" s="T77">wollen-NEG-2SG</ta>
            <ta e="T90" id="Seg_1759" s="T89">Tundra-DAT/LOC</ta>
            <ta e="T91" id="Seg_1760" s="T90">wollen-PRS-2SG</ta>
            <ta e="T106" id="Seg_1761" s="T105">Dorf-DAT/LOC</ta>
            <ta e="T107" id="Seg_1762" s="T106">gut.[NOM]</ta>
            <ta e="T142" id="Seg_1763" s="T141">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-SE">
            <ta e="T79" id="Seg_1764" s="T77">хотеть-NEG-2SG</ta>
            <ta e="T90" id="Seg_1765" s="T89">тундра-DAT/LOC</ta>
            <ta e="T91" id="Seg_1766" s="T90">хотеть-PRS-2SG</ta>
            <ta e="T106" id="Seg_1767" s="T105">поселок-DAT/LOC</ta>
            <ta e="T107" id="Seg_1768" s="T106">хороший.[NOM]</ta>
            <ta e="T142" id="Seg_1769" s="T141">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-SE">
            <ta e="T79" id="Seg_1770" s="T77">v-v:(neg)-v:pred.pn</ta>
            <ta e="T90" id="Seg_1771" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_1772" s="T90">v-v:tense-v:pred.pn</ta>
            <ta e="T106" id="Seg_1773" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_1774" s="T106">adj.[n:case]</ta>
            <ta e="T142" id="Seg_1775" s="T141">adj-n:(poss).[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-SE">
            <ta e="T79" id="Seg_1776" s="T77">v</ta>
            <ta e="T90" id="Seg_1777" s="T89">n</ta>
            <ta e="T91" id="Seg_1778" s="T90">v</ta>
            <ta e="T106" id="Seg_1779" s="T105">n</ta>
            <ta e="T107" id="Seg_1780" s="T106">adj</ta>
            <ta e="T142" id="Seg_1781" s="T141">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-SE" />
         <annotation name="SyF" tierref="SyF-SE" />
         <annotation name="IST" tierref="IST-SE" />
         <annotation name="Top" tierref="Top-SE" />
         <annotation name="Foc" tierref="Foc-SE" />
         <annotation name="BOR" tierref="BOR-SE">
            <ta e="T90" id="Seg_1782" s="T89">RUS:core</ta>
            <ta e="T106" id="Seg_1783" s="T105">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-SE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-SE" />
         <annotation name="CS" tierref="CS-SE" />
         <annotation name="fe" tierref="fe-SE">
            <ta e="T79" id="Seg_1784" s="T77">– You don't want. </ta>
            <ta e="T91" id="Seg_1785" s="T89">– You want to the tundra?</ta>
            <ta e="T107" id="Seg_1786" s="T105">– Is it good in the village?</ta>
            <ta e="T142" id="Seg_1787" s="T141">– That's all?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-SE">
            <ta e="T79" id="Seg_1788" s="T77">– Du möchtest nicht.</ta>
            <ta e="T91" id="Seg_1789" s="T89">– In die Tundra möchtest du?</ta>
            <ta e="T107" id="Seg_1790" s="T105">– Ist es gut im Dorf?</ta>
            <ta e="T142" id="Seg_1791" s="T141">– War's das?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-SE">
            <ta e="T79" id="Seg_1792" s="T77">– Ты не хочешь.</ta>
            <ta e="T91" id="Seg_1793" s="T89">– Ты хочешь в тундру?</ta>
            <ta e="T107" id="Seg_1794" s="T105">– В деревне хорошо?</ta>
            <ta e="T142" id="Seg_1795" s="T141">– Всё?</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-SE" />
         <annotation name="nt" tierref="nt-SE" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-XXXX"
                      id="tx-XXXX"
                      speaker="XXXX"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-XXXX">
            <ts e="T115" id="Seg_1796" n="sc" s="T114">
               <ts e="T115" id="Seg_1798" n="HIAT:u" s="T114">
                  <nts id="Seg_1799" n="HIAT:ip">–</nts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_1802" n="HIAT:w" s="T114">Maːma</ts>
                  <nts id="Seg_1803" n="HIAT:ip">.</nts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T121" id="Seg_1805" n="sc" s="T116">
               <ts e="T121" id="Seg_1807" n="HIAT:u" s="T116">
                  <nts id="Seg_1808" n="HIAT:ip">–</nts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_1811" n="HIAT:w" s="T116">Kɨrgɨttarɨŋ</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_1814" n="HIAT:w" s="T118">ahaːtɨnnar</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_1817" n="HIAT:w" s="T119">du</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_1820" n="HIAT:w" s="T120">dʼe</ts>
                  <nts id="Seg_1821" n="HIAT:ip">?</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_1823" n="sc" s="T123">
               <ts e="T126" id="Seg_1825" n="HIAT:u" s="T123">
                  <nts id="Seg_1826" n="HIAT:ip">–</nts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1828" n="HIAT:ip">(</nts>
                  <nts id="Seg_1829" n="HIAT:ip">(</nts>
                  <ats e="T125" id="Seg_1830" n="HIAT:non-pho" s="T123">…</ats>
                  <nts id="Seg_1831" n="HIAT:ip">)</nts>
                  <nts id="Seg_1832" n="HIAT:ip">)</nts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_1835" n="HIAT:w" s="T125">ahaːrɨŋ</ts>
                  <nts id="Seg_1836" n="HIAT:ip">.</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-XXXX">
            <ts e="T115" id="Seg_1838" n="sc" s="T114">
               <ts e="T115" id="Seg_1840" n="e" s="T114">– Maːma. </ts>
            </ts>
            <ts e="T121" id="Seg_1841" n="sc" s="T116">
               <ts e="T118" id="Seg_1843" n="e" s="T116">– Kɨrgɨttarɨŋ </ts>
               <ts e="T119" id="Seg_1845" n="e" s="T118">ahaːtɨnnar </ts>
               <ts e="T120" id="Seg_1847" n="e" s="T119">du </ts>
               <ts e="T121" id="Seg_1849" n="e" s="T120">dʼe? </ts>
            </ts>
            <ts e="T126" id="Seg_1850" n="sc" s="T123">
               <ts e="T125" id="Seg_1852" n="e" s="T123">– ((…)) </ts>
               <ts e="T126" id="Seg_1854" n="e" s="T125">ahaːrɨŋ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-XXXX">
            <ta e="T115" id="Seg_1855" s="T114">KiPP_XXXX_2009_Clothes_nar.XXXX.001 (001.022)</ta>
            <ta e="T121" id="Seg_1856" s="T116">KiPP_XXXX_2009_Clothes_nar.XXXX.002 (001.024)</ta>
            <ta e="T126" id="Seg_1857" s="T123">KiPP_XXXX_2009_Clothes_nar.XXXX.003 (001.026)</ta>
         </annotation>
         <annotation name="st" tierref="st-XXXX" />
         <annotation name="ts" tierref="ts-XXXX">
            <ta e="T115" id="Seg_1858" s="T114">– Maːma. </ta>
            <ta e="T121" id="Seg_1859" s="T116">– Kɨrgɨttarɨŋ ahaːtɨnnar du dʼe? </ta>
            <ta e="T126" id="Seg_1860" s="T123">– ((…)) ahaːrɨŋ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-XXXX">
            <ta e="T115" id="Seg_1861" s="T114">maːma</ta>
            <ta e="T118" id="Seg_1862" s="T116">kɨrgɨt-tar-ɨ-ŋ</ta>
            <ta e="T119" id="Seg_1863" s="T118">ahaː-tɨnnar</ta>
            <ta e="T120" id="Seg_1864" s="T119">du</ta>
            <ta e="T121" id="Seg_1865" s="T120">dʼe</ta>
            <ta e="T126" id="Seg_1866" s="T125">ah-aːr-ɨ-ŋ</ta>
         </annotation>
         <annotation name="mp" tierref="mp-XXXX">
            <ta e="T115" id="Seg_1867" s="T114">maːma</ta>
            <ta e="T118" id="Seg_1868" s="T116">kɨːs-LAr-I-ŋ</ta>
            <ta e="T119" id="Seg_1869" s="T118">ahaː-TInnAr</ta>
            <ta e="T120" id="Seg_1870" s="T119">du͡o</ta>
            <ta e="T121" id="Seg_1871" s="T120">dʼe</ta>
            <ta e="T126" id="Seg_1872" s="T125">ahaː-Aːr-I-ŋ</ta>
         </annotation>
         <annotation name="ge" tierref="ge-XXXX">
            <ta e="T115" id="Seg_1873" s="T114">mum.[NOM]</ta>
            <ta e="T118" id="Seg_1874" s="T116">girl-PL-EP-2SG.[NOM]</ta>
            <ta e="T119" id="Seg_1875" s="T118">eat-IMP.3PL</ta>
            <ta e="T120" id="Seg_1876" s="T119">Q</ta>
            <ta e="T121" id="Seg_1877" s="T120">well</ta>
            <ta e="T126" id="Seg_1878" s="T125">eat-FUT-EP-IMP.2PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg-XXXX">
            <ta e="T115" id="Seg_1879" s="T114">Mama.[NOM]</ta>
            <ta e="T118" id="Seg_1880" s="T116">Mädchen-PL-EP-2SG.[NOM]</ta>
            <ta e="T119" id="Seg_1881" s="T118">essen-IMP.3PL</ta>
            <ta e="T120" id="Seg_1882" s="T119">Q</ta>
            <ta e="T121" id="Seg_1883" s="T120">doch</ta>
            <ta e="T126" id="Seg_1884" s="T125">essen-FUT-EP-IMP.2PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr-XXXX">
            <ta e="T115" id="Seg_1885" s="T114">мама.[NOM]</ta>
            <ta e="T118" id="Seg_1886" s="T116">девушка-PL-EP-2SG.[NOM]</ta>
            <ta e="T119" id="Seg_1887" s="T118">есть-IMP.3PL</ta>
            <ta e="T120" id="Seg_1888" s="T119">Q</ta>
            <ta e="T121" id="Seg_1889" s="T120">вот</ta>
            <ta e="T126" id="Seg_1890" s="T125">есть-FUT-EP-IMP.2PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc-XXXX">
            <ta e="T115" id="Seg_1891" s="T114">n.[n:case]</ta>
            <ta e="T118" id="Seg_1892" s="T116">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T119" id="Seg_1893" s="T118">v-v:mood.pn</ta>
            <ta e="T120" id="Seg_1894" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1895" s="T120">ptcl</ta>
            <ta e="T126" id="Seg_1896" s="T125">v-v:(tense)-v:(ins)-v:mood.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-XXXX">
            <ta e="T115" id="Seg_1897" s="T114">n</ta>
            <ta e="T118" id="Seg_1898" s="T116">n</ta>
            <ta e="T119" id="Seg_1899" s="T118">v</ta>
            <ta e="T120" id="Seg_1900" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1901" s="T120">ptcl</ta>
            <ta e="T126" id="Seg_1902" s="T125">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-XXXX" />
         <annotation name="SyF" tierref="SyF-XXXX" />
         <annotation name="IST" tierref="IST-XXXX" />
         <annotation name="Top" tierref="Top-XXXX" />
         <annotation name="Foc" tierref="Foc-XXXX" />
         <annotation name="BOR" tierref="BOR-XXXX">
            <ta e="T115" id="Seg_1903" s="T114">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-XXXX" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-XXXX" />
         <annotation name="CS" tierref="CS-XXXX" />
         <annotation name="fe" tierref="fe-XXXX">
            <ta e="T115" id="Seg_1904" s="T114">– Mama.</ta>
            <ta e="T121" id="Seg_1905" s="T116">– Shall the girls eat?</ta>
            <ta e="T126" id="Seg_1906" s="T123">– (…) eat.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-XXXX">
            <ta e="T115" id="Seg_1907" s="T114">– Mama.</ta>
            <ta e="T121" id="Seg_1908" s="T116">– Sollen die Mädchen essen?</ta>
            <ta e="T126" id="Seg_1909" s="T123">– (…) esst.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-XXXX" />
         <annotation name="ltr" tierref="ltr-XXXX">
            <ta e="T121" id="Seg_1910" s="T116">devushki pust pokushajut</ta>
            <ta e="T126" id="Seg_1911" s="T123"> če (aanslpring) pkushajte</ta>
         </annotation>
         <annotation name="nt" tierref="nt-XXXX" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T144" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KiPP"
                          name="ref"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KiPP"
                          name="st"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KiPP"
                          name="ts"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KiPP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KiPP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KiPP"
                          name="mb"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KiPP"
                          name="mp"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KiPP"
                          name="ge"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KiPP"
                          name="gg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KiPP"
                          name="gr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KiPP"
                          name="mc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KiPP"
                          name="ps"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KiPP"
                          name="SeR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KiPP"
                          name="SyF"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KiPP"
                          name="IST"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KiPP"
                          name="Top"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KiPP"
                          name="Foc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KiPP"
                          name="BOR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KiPP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KiPP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KiPP"
                          name="CS"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KiPP"
                          name="fe"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KiPP"
                          name="fg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KiPP"
                          name="fr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KiPP"
                          name="ltr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KiPP"
                          name="nt"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-SE"
                          name="ref"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-SE"
                          name="st"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-SE"
                          name="ts"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-SE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-SE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-SE"
                          name="mb"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-SE"
                          name="mp"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-SE"
                          name="ge"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-SE"
                          name="gg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-SE"
                          name="gr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-SE"
                          name="mc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-SE"
                          name="ps"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-SE"
                          name="SeR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-SE"
                          name="SyF"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-SE"
                          name="IST"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-SE"
                          name="Top"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-SE"
                          name="Foc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-SE"
                          name="BOR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-SE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-SE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-SE"
                          name="CS"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-SE"
                          name="fe"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-SE"
                          name="fg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-SE"
                          name="fr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-SE"
                          name="ltr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-SE"
                          name="nt"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-XXXX"
                          name="ref"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-XXXX"
                          name="st"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-XXXX"
                          name="ts"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-XXXX"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-XXXX"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-XXXX"
                          name="mb"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-XXXX"
                          name="mp"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-XXXX"
                          name="ge"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-XXXX"
                          name="gg"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-XXXX"
                          name="gr"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-XXXX"
                          name="mc"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-XXXX"
                          name="ps"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-XXXX"
                          name="SeR"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-XXXX"
                          name="SyF"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-XXXX"
                          name="IST"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-XXXX"
                          name="Top"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-XXXX"
                          name="Foc"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-XXXX"
                          name="BOR"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-XXXX"
                          name="BOR-Phon"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-XXXX"
                          name="BOR-Morph"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-XXXX"
                          name="CS"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-XXXX"
                          name="fe"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-XXXX"
                          name="fg"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-XXXX"
                          name="fr"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-XXXX"
                          name="ltr"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-XXXX"
                          name="nt"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
