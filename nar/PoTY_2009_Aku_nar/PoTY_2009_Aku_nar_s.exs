<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID699FFDDF-54F4-F42B-07C2-0EC5CB6AD867">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoTY_2009_Aku_nar</transcription-name>
         <referenced-file url="PoTY_2009_Aku_nar.wav" />
         <referenced-file url="PoTY_2009_Aku_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\PoTY_2009_Aku_nar\PoTY_2009_Aku_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1276</ud-information>
            <ud-information attribute-name="# HIAT:w">1000</ud-information>
            <ud-information attribute-name="# e">1001</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">155</ud-information>
            <ud-information attribute-name="# sc">298</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="unknown">
            <abbreviation>PoTY</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="1.14" type="appl" />
         <tli id="T2" time="2.4219999999999997" type="appl" />
         <tli id="T3" time="3.7039999999999997" type="appl" />
         <tli id="T4" time="4.986" type="appl" />
         <tli id="T5" time="6.268" type="appl" />
         <tli id="T6" time="7.55" type="appl" />
         <tli id="T7" time="8.832" type="appl" />
         <tli id="T8" time="10.114" type="appl" />
         <tli id="T9" time="11.396" type="appl" />
         <tli id="T10" time="12.678" type="appl" />
         <tli id="T11" time="13.96" type="appl" />
         <tli id="T12" time="14.5875" type="appl" />
         <tli id="T13" time="15.215" type="appl" />
         <tli id="T14" time="15.8425" type="appl" />
         <tli id="T15" time="16.47" type="appl" />
         <tli id="T16" time="17.07" type="appl" />
         <tli id="T17" time="17.674" type="appl" />
         <tli id="T18" time="18.278" type="appl" />
         <tli id="T19" time="18.882" type="appl" />
         <tli id="T20" time="19.486" type="appl" />
         <tli id="T21" time="20.09" type="appl" />
         <tli id="T22" time="20.5" type="appl" />
         <tli id="T23" time="20.95" type="appl" />
         <tli id="T24" time="21.4" type="appl" />
         <tli id="T25" time="21.85" type="appl" />
         <tli id="T26" time="22.3" type="appl" />
         <tli id="T27" time="22.75" type="appl" />
         <tli id="T28" time="22.87" type="appl" />
         <tli id="T29" time="23.4775" type="appl" />
         <tli id="T30" time="24.085" type="appl" />
         <tli id="T31" time="24.692500000000003" type="appl" />
         <tli id="T32" time="25.3" type="appl" />
         <tli id="T33" time="26.05" type="appl" />
         <tli id="T34" time="26.830000000000002" type="appl" />
         <tli id="T35" time="27.61" type="appl" />
         <tli id="T36" time="28.39" type="appl" />
         <tli id="T37" time="29.17" type="appl" />
         <tli id="T38" time="29.95" type="appl" />
         <tli id="T39" time="30.73" type="appl" />
         <tli id="T40" time="31.509999999999998" type="appl" />
         <tli id="T41" time="32.29" type="appl" />
         <tli id="T42" time="33.07" type="appl" />
         <tli id="T43" time="33.85" type="appl" />
         <tli id="T44" time="34.629999999999995" type="appl" />
         <tli id="T45" time="35.41" type="appl" />
         <tli id="T46" time="36.19" type="appl" />
         <tli id="T47" time="36.97" type="appl" />
         <tli id="T48" time="37.75" type="appl" />
         <tli id="T49" time="38.53" type="appl" />
         <tli id="T50" time="39.309999999999995" type="appl" />
         <tli id="T51" time="40.089999999999996" type="appl" />
         <tli id="T52" time="40.87" type="appl" />
         <tli id="T53" time="41.6" type="appl" />
         <tli id="T54" time="42.21142857142857" type="appl" />
         <tli id="T55" time="42.822857142857146" type="appl" />
         <tli id="T56" time="43.434285714285714" type="appl" />
         <tli id="T57" time="44.04571428571429" type="appl" />
         <tli id="T58" time="44.65714285714286" type="appl" />
         <tli id="T59" time="45.268571428571434" type="appl" />
         <tli id="T60" time="45.88" type="appl" />
         <tli id="T61" time="47.25" type="appl" />
         <tli id="T62" time="48.10181818181818" type="appl" />
         <tli id="T63" time="48.95363636363636" type="appl" />
         <tli id="T64" time="49.805454545454545" type="appl" />
         <tli id="T65" time="50.657272727272726" type="appl" />
         <tli id="T66" time="51.50909090909091" type="appl" />
         <tli id="T67" time="52.36090909090909" type="appl" />
         <tli id="T68" time="53.21272727272727" type="appl" />
         <tli id="T69" time="54.06454545454545" type="appl" />
         <tli id="T70" time="54.916363636363634" type="appl" />
         <tli id="T71" time="55.768181818181816" type="appl" />
         <tli id="T72" time="56.62" type="appl" />
         <tli id="T73" time="56.92" type="appl" />
         <tli id="T74" time="57.89" type="appl" />
         <tli id="T75" time="58.86" type="appl" />
         <tli id="T76" time="59.83" type="appl" />
         <tli id="T77" time="61.31" type="appl" />
         <tli id="T78" time="62.2" type="appl" />
         <tli id="T79" time="63.09" type="appl" />
         <tli id="T80" time="63.68" type="appl" />
         <tli id="T81" time="64.39333333333333" type="appl" />
         <tli id="T82" time="65.10666666666665" type="appl" />
         <tli id="T83" time="65.82" type="appl" />
         <tli id="T84" time="66.45" type="appl" />
         <tli id="T85" time="67.39" type="appl" />
         <tli id="T86" time="68.33" type="appl" />
         <tli id="T87" time="69.36" type="appl" />
         <tli id="T88" time="69.9825" type="appl" />
         <tli id="T89" time="70.60499999999999" type="appl" />
         <tli id="T90" time="71.22749999999999" type="appl" />
         <tli id="T91" time="71.85" type="appl" />
         <tli id="T92" time="72.22" type="appl" />
         <tli id="T93" time="72.73076923076923" type="appl" />
         <tli id="T94" time="73.24153846153845" type="appl" />
         <tli id="T95" time="73.7523076923077" type="appl" />
         <tli id="T96" time="74.26307692307692" type="appl" />
         <tli id="T97" time="74.77384615384615" type="appl" />
         <tli id="T98" time="75.28461538461538" type="appl" />
         <tli id="T99" time="75.79538461538462" type="appl" />
         <tli id="T100" time="76.30615384615385" type="appl" />
         <tli id="T101" time="76.81692307692308" type="appl" />
         <tli id="T102" time="77.3276923076923" type="appl" />
         <tli id="T103" time="77.83846153846154" type="appl" />
         <tli id="T104" time="78.34923076923077" type="appl" />
         <tli id="T105" time="78.86" type="appl" />
         <tli id="T106" time="78.94" type="appl" />
         <tli id="T107" time="79.38333333333333" type="appl" />
         <tli id="T108" time="79.82666666666667" type="appl" />
         <tli id="T109" time="80.27" type="appl" />
         <tli id="T110" time="80.71333333333332" type="appl" />
         <tli id="T111" time="81.15666666666667" type="appl" />
         <tli id="T112" time="81.6" type="appl" />
         <tli id="T113" time="82.69" type="appl" />
         <tli id="T114" time="83.30199999999999" type="appl" />
         <tli id="T115" time="83.914" type="appl" />
         <tli id="T116" time="84.526" type="appl" />
         <tli id="T117" time="85.138" type="appl" />
         <tli id="T118" time="85.75" type="appl" />
         <tli id="T119" time="86.03" type="appl" />
         <tli id="T120" time="86.70857142857143" type="appl" />
         <tli id="T121" time="87.38714285714286" type="appl" />
         <tli id="T122" time="88.0657142857143" type="appl" />
         <tli id="T123" time="88.74428571428571" type="appl" />
         <tli id="T124" time="89.42285714285714" type="appl" />
         <tli id="T125" time="90.10142857142857" type="appl" />
         <tli id="T126" time="90.78" type="appl" />
         <tli id="T127" time="91.89" type="appl" />
         <tli id="T128" time="92.30857142857143" type="appl" />
         <tli id="T129" time="92.72714285714285" type="appl" />
         <tli id="T130" time="93.14571428571428" type="appl" />
         <tli id="T131" time="93.56428571428572" type="appl" />
         <tli id="T132" time="93.98285714285714" type="appl" />
         <tli id="T133" time="94.40142857142857" type="appl" />
         <tli id="T134" time="94.82" type="appl" />
         <tli id="T135" time="94.95" type="appl" />
         <tli id="T136" time="95.51428571428572" type="appl" />
         <tli id="T137" time="96.07857142857144" type="appl" />
         <tli id="T138" time="96.64285714285715" type="appl" />
         <tli id="T139" time="97.20714285714286" type="appl" />
         <tli id="T140" time="97.77142857142857" type="appl" />
         <tli id="T141" time="98.33571428571429" type="appl" />
         <tli id="T142" time="98.9" type="appl" />
         <tli id="T143" time="99.36" type="appl" />
         <tli id="T144" time="99.72333333333333" type="appl" />
         <tli id="T145" time="100.08666666666666" type="appl" />
         <tli id="T146" time="100.45" type="appl" />
         <tli id="T147" time="100.81333333333333" type="appl" />
         <tli id="T148" time="101.17666666666666" type="appl" />
         <tli id="T149" time="101.53999999999999" type="appl" />
         <tli id="T150" time="101.90333333333334" type="appl" />
         <tli id="T151" time="102.26666666666667" type="appl" />
         <tli id="T152" time="102.63" type="appl" />
         <tli id="T153" time="104.16" type="appl" />
         <tli id="T154" time="104.63842857142856" type="appl" />
         <tli id="T155" time="105.11685714285714" type="appl" />
         <tli id="T156" time="105.59528571428571" type="appl" />
         <tli id="T157" time="106.07371428571429" type="appl" />
         <tli id="T158" time="106.55214285714285" type="appl" />
         <tli id="T159" time="107.03057142857143" type="appl" />
         <tli id="T160" time="107.509" type="appl" />
         <tli id="T161" time="108.28" type="appl" />
         <tli id="T162" time="108.91375" type="appl" />
         <tli id="T163" time="109.5475" type="appl" />
         <tli id="T164" time="110.18125" type="appl" />
         <tli id="T165" time="110.815" type="appl" />
         <tli id="T166" time="111.44874999999999" type="appl" />
         <tli id="T167" time="112.0825" type="appl" />
         <tli id="T168" time="112.71625" type="appl" />
         <tli id="T169" time="113.35" type="appl" />
         <tli id="T170" time="113.43" type="appl" />
         <tli id="T171" time="113.91" type="appl" />
         <tli id="T172" time="114.39" type="appl" />
         <tli id="T173" time="114.87" type="appl" />
         <tli id="T174" time="115.35" type="appl" />
         <tli id="T175" time="115.89" type="appl" />
         <tli id="T176" time="116.362" type="appl" />
         <tli id="T177" time="116.834" type="appl" />
         <tli id="T178" time="117.306" type="appl" />
         <tli id="T179" time="117.778" type="appl" />
         <tli id="T180" time="118.25" type="appl" />
         <tli id="T181" time="118.479" type="appl" />
         <tli id="T182" time="118.90022222222223" type="appl" />
         <tli id="T183" time="119.32144444444444" type="appl" />
         <tli id="T184" time="119.74266666666666" type="appl" />
         <tli id="T185" time="120.16388888888889" type="appl" />
         <tli id="T186" time="120.5851111111111" type="appl" />
         <tli id="T187" time="121.00633333333333" type="appl" />
         <tli id="T188" time="121.42755555555556" type="appl" />
         <tli id="T189" time="121.84877777777777" type="appl" />
         <tli id="T190" time="122.27" type="appl" />
         <tli id="T191" time="122.31" type="appl" />
         <tli id="T192" time="122.83" type="appl" />
         <tli id="T193" time="123.35" type="appl" />
         <tli id="T194" time="123.87" type="appl" />
         <tli id="T195" time="124.39" type="appl" />
         <tli id="T196" time="124.91" type="appl" />
         <tli id="T197" time="125.4" type="appl" />
         <tli id="T198" time="126.12181818181818" type="appl" />
         <tli id="T199" time="126.84363636363636" type="appl" />
         <tli id="T200" time="127.56545454545456" type="appl" />
         <tli id="T201" time="128.28727272727272" type="appl" />
         <tli id="T202" time="129.0090909090909" type="appl" />
         <tli id="T203" time="129.7309090909091" type="appl" />
         <tli id="T204" time="130.4527272727273" type="appl" />
         <tli id="T205" time="131.17454545454547" type="appl" />
         <tli id="T206" time="131.89636363636365" type="appl" />
         <tli id="T207" time="132.61818181818182" type="appl" />
         <tli id="T208" time="133.34" type="appl" />
         <tli id="T209" time="133.62" type="appl" />
         <tli id="T210" time="133.97875" type="appl" />
         <tli id="T211" time="134.3375" type="appl" />
         <tli id="T212" time="134.69625000000002" type="appl" />
         <tli id="T213" time="135.055" type="appl" />
         <tli id="T214" time="135.41375" type="appl" />
         <tli id="T215" time="135.7725" type="appl" />
         <tli id="T216" time="136.13125000000002" type="appl" />
         <tli id="T217" time="136.49" type="appl" />
         <tli id="T218" time="137.17" type="appl" />
         <tli id="T219" time="137.66" type="appl" />
         <tli id="T220" time="138.14999999999998" type="appl" />
         <tli id="T221" time="138.64" type="appl" />
         <tli id="T222" time="139.13" type="appl" />
         <tli id="T223" time="139.61999999999998" type="appl" />
         <tli id="T224" time="140.10999999999999" type="appl" />
         <tli id="T225" time="140.6" type="appl" />
         <tli id="T226" time="141.09" type="appl" />
         <tli id="T227" time="141.57999999999998" type="appl" />
         <tli id="T228" time="142.07" type="appl" />
         <tli id="T229" time="142.56" type="appl" />
         <tli id="T230" time="143.04999999999998" type="appl" />
         <tli id="T231" time="143.54" type="appl" />
         <tli id="T232" time="143.93" type="appl" />
         <tli id="T233" time="144.2018181818182" type="appl" />
         <tli id="T234" time="144.47363636363636" type="appl" />
         <tli id="T235" time="144.74545454545455" type="appl" />
         <tli id="T236" time="145.01727272727274" type="appl" />
         <tli id="T237" time="145.2890909090909" type="appl" />
         <tli id="T238" time="145.5609090909091" type="appl" />
         <tli id="T239" time="145.83272727272725" type="appl" />
         <tli id="T240" time="146.10454545454544" type="appl" />
         <tli id="T241" time="146.37636363636364" type="appl" />
         <tli id="T242" time="146.6481818181818" type="appl" />
         <tli id="T243" time="146.92" type="appl" />
         <tli id="T244" time="149.26" type="appl" />
         <tli id="T245" time="149.666" type="appl" />
         <tli id="T246" time="150.072" type="appl" />
         <tli id="T247" time="150.47799999999998" type="appl" />
         <tli id="T248" time="150.884" type="appl" />
         <tli id="T249" time="151.29" type="appl" />
         <tli id="T250" time="151.45" type="appl" />
         <tli id="T251" time="151.98666666666665" type="appl" />
         <tli id="T252" time="152.52333333333334" type="appl" />
         <tli id="T253" time="153.06" type="appl" />
         <tli id="T254" time="153.7" type="appl" />
         <tli id="T255" time="154.33333333333331" type="appl" />
         <tli id="T256" time="154.96666666666667" type="appl" />
         <tli id="T257" time="155.6" type="appl" />
         <tli id="T258" time="155.69" type="appl" />
         <tli id="T259" time="156.216" type="appl" />
         <tli id="T260" time="156.742" type="appl" />
         <tli id="T261" time="157.268" type="appl" />
         <tli id="T262" time="157.79399999999998" type="appl" />
         <tli id="T263" time="158.32" type="appl" />
         <tli id="T264" time="158.4" type="appl" />
         <tli id="T265" time="158.95499999999998" type="appl" />
         <tli id="T266" time="159.51" type="appl" />
         <tli id="T267" time="159.75" type="appl" />
         <tli id="T268" time="160.0975" type="appl" />
         <tli id="T269" time="160.445" type="appl" />
         <tli id="T270" time="160.7925" type="appl" />
         <tli id="T271" time="161.14" type="appl" />
         <tli id="T272" time="161.25" type="appl" />
         <tli id="T273" time="161.59" type="appl" />
         <tli id="T274" time="161.93" type="appl" />
         <tli id="T275" time="162.27" type="appl" />
         <tli id="T276" time="162.34" type="appl" />
         <tli id="T277" time="162.83529411764707" type="appl" />
         <tli id="T278" time="163.33058823529413" type="appl" />
         <tli id="T279" time="163.82588235294116" type="appl" />
         <tli id="T280" time="164.32117647058823" type="appl" />
         <tli id="T281" time="164.8164705882353" type="appl" />
         <tli id="T282" time="165.31176470588235" type="appl" />
         <tli id="T283" time="165.80705882352942" type="appl" />
         <tli id="T284" time="166.30235294117648" type="appl" />
         <tli id="T285" time="166.79764705882351" type="appl" />
         <tli id="T286" time="167.29294117647058" type="appl" />
         <tli id="T287" time="167.78823529411764" type="appl" />
         <tli id="T288" time="168.2835294117647" type="appl" />
         <tli id="T289" time="168.77882352941177" type="appl" />
         <tli id="T290" time="169.27411764705883" type="appl" />
         <tli id="T291" time="169.76941176470586" type="appl" />
         <tli id="T292" time="170.26470588235293" type="appl" />
         <tli id="T293" time="170.76" type="appl" />
         <tli id="T294" time="170.88" type="appl" />
         <tli id="T295" time="171.628" type="appl" />
         <tli id="T296" time="172.376" type="appl" />
         <tli id="T297" time="173.124" type="appl" />
         <tli id="T298" time="173.872" type="appl" />
         <tli id="T299" time="174.62" type="appl" />
         <tli id="T300" time="174.66" type="appl" />
         <tli id="T301" time="175.2" type="appl" />
         <tli id="T302" time="175.74" type="appl" />
         <tli id="T303" time="176.28" type="appl" />
         <tli id="T304" time="176.82000000000002" type="appl" />
         <tli id="T305" time="177.36" type="appl" />
         <tli id="T306" time="177.41" type="appl" />
         <tli id="T307" time="178.34" type="appl" />
         <tli id="T308" time="179.27" type="appl" />
         <tli id="T309" time="179.54" type="appl" />
         <tli id="T310" time="180.13" type="appl" />
         <tli id="T311" time="180.72" type="appl" />
         <tli id="T312" time="181.31" type="appl" />
         <tli id="T313" time="181.9" type="appl" />
         <tli id="T314" time="182.49" type="appl" />
         <tli id="T315" time="184.3" type="appl" />
         <tli id="T316" time="185.095" type="appl" />
         <tli id="T317" time="185.89000000000001" type="appl" />
         <tli id="T318" time="186.685" type="appl" />
         <tli id="T319" time="187.48" type="appl" />
         <tli id="T320" time="188.275" type="appl" />
         <tli id="T321" time="189.07" type="appl" />
         <tli id="T322" time="189.13" type="appl" />
         <tli id="T323" time="189.57999999999998" type="appl" />
         <tli id="T324" time="190.03" type="appl" />
         <tli id="T325" time="190.48000000000002" type="appl" />
         <tli id="T326" time="190.93" type="appl" />
         <tli id="T327" time="191.38" type="appl" />
         <tli id="T328" time="191.83" type="appl" />
         <tli id="T329" time="193.73" type="appl" />
         <tli id="T330" time="194.518" type="appl" />
         <tli id="T331" time="195.30599999999998" type="appl" />
         <tli id="T332" time="196.094" type="appl" />
         <tli id="T333" time="196.88199999999998" type="appl" />
         <tli id="T334" time="197.67" type="appl" />
         <tli id="T335" time="197.829" type="appl" />
         <tli id="T336" time="198.44158333333334" type="appl" />
         <tli id="T337" time="199.05416666666667" type="appl" />
         <tli id="T338" time="199.66675" type="appl" />
         <tli id="T339" time="200.27933333333334" type="appl" />
         <tli id="T340" time="200.89191666666667" type="appl" />
         <tli id="T341" time="201.5045" type="appl" />
         <tli id="T342" time="202.11708333333334" type="appl" />
         <tli id="T343" time="202.72966666666667" type="appl" />
         <tli id="T344" time="203.34225" type="appl" />
         <tli id="T345" time="203.95483333333334" type="appl" />
         <tli id="T346" time="204.56741666666667" type="appl" />
         <tli id="T347" time="205.18" type="appl" />
         <tli id="T348" time="205.49" type="appl" />
         <tli id="T349" time="205.97333333333333" type="appl" />
         <tli id="T350" time="206.45666666666668" type="appl" />
         <tli id="T351" time="206.94" type="appl" />
         <tli id="T352" time="207.42333333333335" type="appl" />
         <tli id="T353" time="207.90666666666667" type="appl" />
         <tli id="T354" time="208.39000000000001" type="appl" />
         <tli id="T355" time="208.87333333333333" type="appl" />
         <tli id="T356" time="209.35666666666668" type="appl" />
         <tli id="T357" time="209.84" type="appl" />
         <tli id="T358" time="209.93" type="appl" />
         <tli id="T359" time="210.47" type="appl" />
         <tli id="T360" time="211.01000000000002" type="appl" />
         <tli id="T361" time="211.55" type="appl" />
         <tli id="T362" time="212.09" type="appl" />
         <tli id="T363" time="212.63" type="appl" />
         <tli id="T364" time="213.17000000000002" type="appl" />
         <tli id="T365" time="213.71" type="appl" />
         <tli id="T366" time="213.76" type="appl" />
         <tli id="T367" time="214.2425" type="appl" />
         <tli id="T368" time="214.725" type="appl" />
         <tli id="T369" time="215.20749999999998" type="appl" />
         <tli id="T370" time="215.69" type="appl" />
         <tli id="T371" time="216.38" type="appl" />
         <tli id="T372" time="217.02272727272728" type="appl" />
         <tli id="T373" time="217.66545454545454" type="appl" />
         <tli id="T374" time="218.30818181818182" type="appl" />
         <tli id="T375" time="218.95090909090908" type="appl" />
         <tli id="T376" time="219.59363636363636" type="appl" />
         <tli id="T377" time="220.23636363636362" type="appl" />
         <tli id="T378" time="220.8790909090909" type="appl" />
         <tli id="T379" time="221.52181818181816" type="appl" />
         <tli id="T380" time="222.16454545454545" type="appl" />
         <tli id="T381" time="222.8072727272727" type="appl" />
         <tli id="T382" time="223.45" type="appl" />
         <tli id="T383" time="223.86" type="appl" />
         <tli id="T384" time="224.56214285714287" type="appl" />
         <tli id="T385" time="225.26428571428573" type="appl" />
         <tli id="T386" time="225.9664285714286" type="appl" />
         <tli id="T387" time="226.66857142857143" type="appl" />
         <tli id="T388" time="227.37071428571429" type="appl" />
         <tli id="T389" time="228.07285714285715" type="appl" />
         <tli id="T390" time="228.775" type="appl" />
         <tli id="T391" time="229.47714285714287" type="appl" />
         <tli id="T392" time="230.17928571428573" type="appl" />
         <tli id="T393" time="230.8814285714286" type="appl" />
         <tli id="T394" time="231.58357142857142" type="appl" />
         <tli id="T395" time="232.28571428571428" type="appl" />
         <tli id="T396" time="232.98785714285714" type="appl" />
         <tli id="T397" time="233.69" type="appl" />
         <tli id="T398" time="234.38" type="appl" />
         <tli id="T399" time="234.78866666666667" type="appl" />
         <tli id="T400" time="235.19733333333332" type="appl" />
         <tli id="T401" time="235.606" type="appl" />
         <tli id="T402" time="236.01466666666667" type="appl" />
         <tli id="T403" time="236.42333333333332" type="appl" />
         <tli id="T404" time="236.832" type="appl" />
         <tli id="T405" time="237.24066666666667" type="appl" />
         <tli id="T406" time="237.64933333333332" type="appl" />
         <tli id="T407" time="238.058" type="appl" />
         <tli id="T408" time="238.46666666666667" type="appl" />
         <tli id="T409" time="238.87533333333332" type="appl" />
         <tli id="T410" time="239.284" type="appl" />
         <tli id="T411" time="239.69266666666667" type="appl" />
         <tli id="T412" time="240.10133333333332" type="appl" />
         <tli id="T413" time="240.51" type="appl" />
         <tli id="T414" time="240.57" type="appl" />
         <tli id="T415" time="241.07999999999998" type="appl" />
         <tli id="T416" time="241.59" type="appl" />
         <tli id="T417" time="242.1" type="appl" />
         <tli id="T418" time="242.61" type="appl" />
         <tli id="T419" time="243.12" type="appl" />
         <tli id="T420" time="243.63" type="appl" />
         <tli id="T421" time="244.14000000000001" type="appl" />
         <tli id="T422" time="244.65" type="appl" />
         <tli id="T423" time="245.324" type="appl" />
         <tli id="T424" time="245.998" type="appl" />
         <tli id="T425" time="246.672" type="appl" />
         <tli id="T426" time="247.346" type="appl" />
         <tli id="T1150" time="247.52981818181817" type="intp" />
         <tli id="T427" time="248.01999999999998" type="appl" />
         <tli id="T428" time="248.694" type="appl" />
         <tli id="T429" time="249.368" type="appl" />
         <tli id="T430" time="250.042" type="appl" />
         <tli id="T431" time="250.71599999999998" type="appl" />
         <tli id="T432" time="251.39" type="appl" />
         <tli id="T433" time="251.87" type="appl" />
         <tli id="T434" time="252.35" type="appl" />
         <tli id="T435" time="252.82999999999998" type="appl" />
         <tli id="T436" time="253.31" type="appl" />
         <tli id="T437" time="253.79000000000002" type="appl" />
         <tli id="T438" time="254.27" type="appl" />
         <tli id="T439" time="254.34" type="appl" />
         <tli id="T440" time="254.83666666666667" type="appl" />
         <tli id="T441" time="255.33333333333334" type="appl" />
         <tli id="T442" time="255.83" type="appl" />
         <tli id="T443" time="255.85" type="appl" />
         <tli id="T444" time="256.46" type="appl" />
         <tli id="T445" time="257.07" type="appl" />
         <tli id="T446" time="257.11" type="appl" />
         <tli id="T447" time="257.4842857142857" type="appl" />
         <tli id="T448" time="257.85857142857145" type="appl" />
         <tli id="T449" time="258.23285714285714" type="appl" />
         <tli id="T450" time="258.6071428571429" type="appl" />
         <tli id="T451" time="258.9814285714286" type="appl" />
         <tli id="T452" time="259.3557142857143" type="appl" />
         <tli id="T453" time="259.73" type="appl" />
         <tli id="T454" time="260.11" type="appl" />
         <tli id="T455" time="260.68" type="appl" />
         <tli id="T456" time="261.25" type="appl" />
         <tli id="T457" time="261.82" type="appl" />
         <tli id="T458" time="262.39" type="appl" />
         <tli id="T459" time="262.46" type="appl" />
         <tli id="T460" time="263.046" type="appl" />
         <tli id="T461" time="263.632" type="appl" />
         <tli id="T462" time="264.21799999999996" type="appl" />
         <tli id="T463" time="264.804" type="appl" />
         <tli id="T464" time="265.39" type="appl" />
         <tli id="T465" time="266.75" type="appl" />
         <tli id="T466" time="267.13875" type="appl" />
         <tli id="T467" time="267.52750000000003" type="appl" />
         <tli id="T468" time="267.91625" type="appl" />
         <tli id="T469" time="268.305" type="appl" />
         <tli id="T470" time="268.69375" type="appl" />
         <tli id="T471" time="269.0825" type="appl" />
         <tli id="T472" time="269.47125" type="appl" />
         <tli id="T473" time="269.86" type="appl" />
         <tli id="T474" time="269.93" type="appl" />
         <tli id="T475" time="270.32166666666666" type="appl" />
         <tli id="T476" time="270.7133333333333" type="appl" />
         <tli id="T477" time="271.105" type="appl" />
         <tli id="T478" time="271.49666666666667" type="appl" />
         <tli id="T479" time="271.8883333333333" type="appl" />
         <tli id="T480" time="272.28" type="appl" />
         <tli id="T481" time="272.35" type="appl" />
         <tli id="T482" time="272.9614285714286" type="appl" />
         <tli id="T483" time="273.5728571428572" type="appl" />
         <tli id="T484" time="274.18428571428575" type="appl" />
         <tli id="T485" time="274.79571428571427" type="appl" />
         <tli id="T486" time="275.40714285714284" type="appl" />
         <tli id="T487" time="276.0185714285714" type="appl" />
         <tli id="T488" time="276.63" type="appl" />
         <tli id="T489" time="276.67" type="appl" />
         <tli id="T490" time="277.2425" type="appl" />
         <tli id="T491" time="277.815" type="appl" />
         <tli id="T492" time="278.3875" type="appl" />
         <tli id="T493" time="278.96" type="appl" />
         <tli id="T494" time="278.99" type="appl" />
         <tli id="T495" time="279.55" type="appl" />
         <tli id="T496" time="280.11" type="appl" />
         <tli id="T497" time="280.67" type="appl" />
         <tli id="T498" time="281.23" type="appl" />
         <tli id="T499" time="281.79" type="appl" />
         <tli id="T500" time="282.35" type="appl" />
         <tli id="T501" time="282.38" type="appl" />
         <tli id="T502" time="282.7914285714286" type="appl" />
         <tli id="T503" time="283.2028571428571" type="appl" />
         <tli id="T504" time="283.6142857142857" type="appl" />
         <tli id="T505" time="284.0257142857143" type="appl" />
         <tli id="T506" time="284.4371428571429" type="appl" />
         <tli id="T507" time="284.8485714285714" type="appl" />
         <tli id="T508" time="285.26" type="appl" />
         <tli id="T509" time="285.3" type="appl" />
         <tli id="T510" time="286.21500000000003" type="appl" />
         <tli id="T511" time="287.13" type="appl" />
         <tli id="T512" time="287.17" type="appl" />
         <tli id="T513" time="287.595" type="appl" />
         <tli id="T514" time="288.02" type="appl" />
         <tli id="T515" time="288.445" type="appl" />
         <tli id="T516" time="288.87" type="appl" />
         <tli id="T517" time="289.19" type="appl" />
         <tli id="T518" time="289.8" type="appl" />
         <tli id="T519" time="290.40999999999997" type="appl" />
         <tli id="T520" time="291.02" type="appl" />
         <tli id="T521" time="291.63" type="appl" />
         <tli id="T522" time="291.68" type="appl" />
         <tli id="T523" time="292.5366666666667" type="appl" />
         <tli id="T524" time="293.3933333333333" type="appl" />
         <tli id="T525" time="294.25" type="appl" />
         <tli id="T526" time="294.59" type="appl" />
         <tli id="T527" time="294.95" type="appl" />
         <tli id="T528" time="295.31" type="appl" />
         <tli id="T529" time="295.66999999999996" type="appl" />
         <tli id="T530" time="296.03" type="appl" />
         <tli id="T531" time="296.39" type="appl" />
         <tli id="T532" time="296.75" type="appl" />
         <tli id="T533" time="297.11" type="appl" />
         <tli id="T534" time="297.47" type="appl" />
         <tli id="T535" time="297.83" type="appl" />
         <tli id="T536" time="298.19" type="appl" />
         <tli id="T537" time="298.55" type="appl" />
         <tli id="T538" time="298.76" type="appl" />
         <tli id="T539" time="299.3775" type="appl" />
         <tli id="T540" time="299.995" type="appl" />
         <tli id="T541" time="300.6125" type="appl" />
         <tli id="T542" time="301.23" type="appl" />
         <tli id="T543" time="301.28" type="appl" />
         <tli id="T544" time="301.91499999999996" type="appl" />
         <tli id="T545" time="302.55" type="appl" />
         <tli id="T546" time="302.66" type="appl" />
         <tli id="T547" time="303.264" type="appl" />
         <tli id="T548" time="303.868" type="appl" />
         <tli id="T549" time="304.47200000000004" type="appl" />
         <tli id="T550" time="305.076" type="appl" />
         <tli id="T551" time="305.68" type="appl" />
         <tli id="T552" time="306.52" type="appl" />
         <tli id="T553" time="306.9483333333333" type="appl" />
         <tli id="T554" time="307.37666666666667" type="appl" />
         <tli id="T555" time="307.80499999999995" type="appl" />
         <tli id="T556" time="308.2333333333333" type="appl" />
         <tli id="T557" time="308.66166666666663" type="appl" />
         <tli id="T558" time="309.09" type="appl" />
         <tli id="T559" time="309.17" type="appl" />
         <tli id="T560" time="309.61055555555555" type="appl" />
         <tli id="T561" time="310.05111111111114" type="appl" />
         <tli id="T562" time="310.4916666666667" type="appl" />
         <tli id="T563" time="310.93222222222226" type="appl" />
         <tli id="T564" time="311.3727777777778" type="appl" />
         <tli id="T565" time="311.81333333333333" type="appl" />
         <tli id="T566" time="312.2538888888889" type="appl" />
         <tli id="T567" time="312.69444444444446" type="appl" />
         <tli id="T568" time="313.135" type="appl" />
         <tli id="T569" time="313.5755555555556" type="appl" />
         <tli id="T570" time="314.0161111111111" type="appl" />
         <tli id="T571" time="314.4566666666667" type="appl" />
         <tli id="T572" time="314.89722222222224" type="appl" />
         <tli id="T573" time="315.3377777777778" type="appl" />
         <tli id="T574" time="315.77833333333336" type="appl" />
         <tli id="T575" time="316.2188888888889" type="appl" />
         <tli id="T576" time="316.6594444444445" type="appl" />
         <tli id="T577" time="317.1" type="appl" />
         <tli id="T578" time="318.93" type="appl" />
         <tli id="T579" time="319.568" type="appl" />
         <tli id="T580" time="320.206" type="appl" />
         <tli id="T581" time="320.844" type="appl" />
         <tli id="T582" time="321.482" type="appl" />
         <tli id="T583" time="322.12" type="appl" />
         <tli id="T584" time="322.24" type="appl" />
         <tli id="T585" time="323.08" type="appl" />
         <tli id="T586" time="323.92" type="appl" />
         <tli id="T587" time="324.76" type="appl" />
         <tli id="T588" time="325.6" type="appl" />
         <tli id="T589" time="326.44" type="appl" />
         <tli id="T590" time="326.6" type="appl" />
         <tli id="T591" time="327.27000000000004" type="appl" />
         <tli id="T592" time="327.94" type="appl" />
         <tli id="T593" time="328.61" type="appl" />
         <tli id="T594" time="328.65" type="appl" />
         <tli id="T595" time="329.179" type="appl" />
         <tli id="T596" time="329.70799999999997" type="appl" />
         <tli id="T597" time="330.23699999999997" type="appl" />
         <tli id="T598" time="330.76599999999996" type="appl" />
         <tli id="T599" time="331.29499999999996" type="appl" />
         <tli id="T600" time="331.824" type="appl" />
         <tli id="T601" time="332.353" type="appl" />
         <tli id="T602" time="332.882" type="appl" />
         <tli id="T603" time="333.411" type="appl" />
         <tli id="T604" time="333.94" type="appl" />
         <tli id="T605" time="334.24" type="appl" />
         <tli id="T606" time="334.9616666666667" type="appl" />
         <tli id="T607" time="335.68333333333334" type="appl" />
         <tli id="T608" time="336.405" type="appl" />
         <tli id="T609" time="337.12666666666667" type="appl" />
         <tli id="T610" time="337.84833333333336" type="appl" />
         <tli id="T611" time="338.57" type="appl" />
         <tli id="T612" time="338.62" type="appl" />
         <tli id="T613" time="339.25666666666666" type="appl" />
         <tli id="T614" time="339.8933333333333" type="appl" />
         <tli id="T615" time="340.53" type="appl" />
         <tli id="T616" time="341.1666666666667" type="appl" />
         <tli id="T617" time="341.80333333333334" type="appl" />
         <tli id="T618" time="342.44" type="appl" />
         <tli id="T619" time="342.62" type="appl" />
         <tli id="T620" time="343.13" type="appl" />
         <tli id="T621" time="343.64" type="appl" />
         <tli id="T622" time="344.15" type="appl" />
         <tli id="T623" time="344.66" type="appl" />
         <tli id="T624" time="345.17" type="appl" />
         <tli id="T625" time="345.68" type="appl" />
         <tli id="T626" time="345.71" type="appl" />
         <tli id="T627" time="346.477" type="appl" />
         <tli id="T628" time="347.24399999999997" type="appl" />
         <tli id="T629" time="348.01099999999997" type="appl" />
         <tli id="T630" time="348.77799999999996" type="appl" />
         <tli id="T631" time="349.54499999999996" type="appl" />
         <tli id="T632" time="350.312" type="appl" />
         <tli id="T633" time="351.079" type="appl" />
         <tli id="T634" time="351.846" type="appl" />
         <tli id="T635" time="352.613" type="appl" />
         <tli id="T636" time="353.38" type="appl" />
         <tli id="T637" time="355.93" type="appl" />
         <tli id="T638" time="356.49" type="appl" />
         <tli id="T639" time="357.05" type="appl" />
         <tli id="T640" time="357.61" type="appl" />
         <tli id="T641" time="358.17" type="appl" />
         <tli id="T642" time="358.2" type="appl" />
         <tli id="T643" time="358.8175" type="appl" />
         <tli id="T644" time="359.435" type="appl" />
         <tli id="T645" time="360.0525" type="appl" />
         <tli id="T646" time="360.66999999999996" type="appl" />
         <tli id="T647" time="361.28749999999997" type="appl" />
         <tli id="T648" time="361.905" type="appl" />
         <tli id="T649" time="362.5225" type="appl" />
         <tli id="T650" time="363.14" type="appl" />
         <tli id="T651" time="363.25" type="appl" />
         <tli id="T652" time="363.86" type="appl" />
         <tli id="T653" time="364.46999999999997" type="appl" />
         <tli id="T654" time="365.08" type="appl" />
         <tli id="T655" time="365.15" type="appl" />
         <tli id="T656" time="367.6716666666666" type="appl" />
         <tli id="T657" time="370.1933333333333" type="appl" />
         <tli id="T658" time="372.715" type="appl" />
         <tli id="T659" time="375.2366666666666" type="appl" />
         <tli id="T660" time="377.7583333333333" type="appl" />
         <tli id="T661" time="380.28" type="appl" />
         <tli id="T662" time="380.39" type="appl" />
         <tli id="T663" time="380.8225" type="appl" />
         <tli id="T664" time="381.255" type="appl" />
         <tli id="T665" time="381.6875" type="appl" />
         <tli id="T666" time="382.12" type="appl" />
         <tli id="T667" time="382.16" type="appl" />
         <tli id="T668" time="382.78000000000003" type="appl" />
         <tli id="T669" time="383.40000000000003" type="appl" />
         <tli id="T670" time="384.02" type="appl" />
         <tli id="T671" time="384.64" type="appl" />
         <tli id="T672" time="385.26" type="appl" />
         <tli id="T673" time="385.31" type="appl" />
         <tli id="T674" time="385.7" type="appl" />
         <tli id="T675" time="386.09000000000003" type="appl" />
         <tli id="T676" time="386.48" type="appl" />
         <tli id="T677" time="386.87" type="appl" />
         <tli id="T678" time="387.26" type="appl" />
         <tli id="T679" time="387.65000000000003" type="appl" />
         <tli id="T680" time="388.04" type="appl" />
         <tli id="T681" time="388.66" type="appl" />
         <tli id="T682" time="389.1066666666667" type="appl" />
         <tli id="T683" time="389.55333333333334" type="appl" />
         <tli id="T684" time="390.0" type="appl" />
         <tli id="T685" time="390.44666666666666" type="appl" />
         <tli id="T686" time="390.8933333333333" type="appl" />
         <tli id="T687" time="391.34" type="appl" />
         <tli id="T688" time="391.38" type="appl" />
         <tli id="T689" time="391.86375" type="appl" />
         <tli id="T690" time="392.34749999999997" type="appl" />
         <tli id="T691" time="392.83125" type="appl" />
         <tli id="T692" time="393.315" type="appl" />
         <tli id="T693" time="393.79875" type="appl" />
         <tli id="T694" time="394.2825" type="appl" />
         <tli id="T695" time="394.76625" type="appl" />
         <tli id="T696" time="395.25" type="appl" />
         <tli id="T697" time="395.46" type="appl" />
         <tli id="T698" time="396.191" type="appl" />
         <tli id="T699" time="396.92199999999997" type="appl" />
         <tli id="T700" time="397.65299999999996" type="appl" />
         <tli id="T701" time="398.38399999999996" type="appl" />
         <tli id="T702" time="399.115" type="appl" />
         <tli id="T703" time="399.846" type="appl" />
         <tli id="T704" time="400.577" type="appl" />
         <tli id="T705" time="401.308" type="appl" />
         <tli id="T706" time="402.039" type="appl" />
         <tli id="T707" time="402.77" type="appl" />
         <tli id="T708" time="403.13" type="appl" />
         <tli id="T709" time="403.6933333333333" type="appl" />
         <tli id="T710" time="404.25666666666666" type="appl" />
         <tli id="T711" time="404.82" type="appl" />
         <tli id="T712" time="405.3833333333333" type="appl" />
         <tli id="T713" time="405.94666666666666" type="appl" />
         <tli id="T714" time="406.51" type="appl" />
         <tli id="T715" time="407.6" type="appl" />
         <tli id="T716" time="408.2375" type="appl" />
         <tli id="T717" time="408.875" type="appl" />
         <tli id="T718" time="409.5125" type="appl" />
         <tli id="T719" time="410.15" type="appl" />
         <tli id="T720" time="410.7875" type="appl" />
         <tli id="T721" time="411.425" type="appl" />
         <tli id="T722" time="412.0625" type="appl" />
         <tli id="T723" time="412.7" type="appl" />
         <tli id="T724" time="413.49" type="appl" />
         <tli id="T725" time="414.32" type="appl" />
         <tli id="T726" time="415.15000000000003" type="appl" />
         <tli id="T727" time="415.98" type="appl" />
         <tli id="T728" time="416.81" type="appl" />
         <tli id="T729" time="417.64" type="appl" />
         <tli id="T730" time="418.47" type="appl" />
         <tli id="T731" time="419.3" type="appl" />
         <tli id="T732" time="421.47" type="appl" />
         <tli id="T733" time="422.58500000000004" type="appl" />
         <tli id="T734" time="423.7" type="appl" />
         <tli id="T735" time="424.06" type="appl" />
         <tli id="T736" time="424.48333333333335" type="appl" />
         <tli id="T737" time="424.90666666666664" type="appl" />
         <tli id="T738" time="425.33" type="appl" />
         <tli id="T739" time="425.39" type="appl" />
         <tli id="T740" time="426.0925" type="appl" />
         <tli id="T741" time="426.79499999999996" type="appl" />
         <tli id="T742" time="427.4975" type="appl" />
         <tli id="T743" time="428.2" type="appl" />
         <tli id="T744" time="428.82" type="appl" />
         <tli id="T745" time="430.0233333333333" type="appl" />
         <tli id="T746" time="431.2266666666667" type="appl" />
         <tli id="T747" time="432.43" type="appl" />
         <tli id="T748" time="433.6333333333333" type="appl" />
         <tli id="T749" time="434.8366666666667" type="appl" />
         <tli id="T750" time="436.04" type="appl" />
         <tli id="T751" time="436.12" type="appl" />
         <tli id="T752" time="436.60818181818183" type="appl" />
         <tli id="T753" time="437.09636363636366" type="appl" />
         <tli id="T754" time="437.58454545454543" type="appl" />
         <tli id="T755" time="438.07272727272726" type="appl" />
         <tli id="T756" time="438.5609090909091" type="appl" />
         <tli id="T757" time="439.0490909090909" type="appl" />
         <tli id="T758" time="439.53727272727275" type="appl" />
         <tli id="T759" time="440.0254545454546" type="appl" />
         <tli id="T760" time="440.51363636363635" type="appl" />
         <tli id="T761" time="441.0018181818182" type="appl" />
         <tli id="T762" time="441.49" type="appl" />
         <tli id="T763" time="441.63" type="appl" />
         <tli id="T764" time="442.17333333333335" type="appl" />
         <tli id="T765" time="442.71666666666664" type="appl" />
         <tli id="T766" time="443.26" type="appl" />
         <tli id="T767" time="443.29" type="appl" />
         <tli id="T768" time="443.934" type="appl" />
         <tli id="T769" time="444.57800000000003" type="appl" />
         <tli id="T770" time="445.222" type="appl" />
         <tli id="T771" time="445.866" type="appl" />
         <tli id="T772" time="446.51" type="appl" />
         <tli id="T773" time="446.7" type="appl" />
         <tli id="T774" time="447.2675" type="appl" />
         <tli id="T775" time="447.83500000000004" type="appl" />
         <tli id="T776" time="448.40250000000003" type="appl" />
         <tli id="T777" time="448.97" type="appl" />
         <tli id="T778" time="449.08" type="appl" />
         <tli id="T779" time="449.77625" type="appl" />
         <tli id="T780" time="450.47249999999997" type="appl" />
         <tli id="T781" time="451.16875" type="appl" />
         <tli id="T782" time="451.865" type="appl" />
         <tli id="T783" time="452.56125" type="appl" />
         <tli id="T784" time="453.2575" type="appl" />
         <tli id="T785" time="453.95374999999996" type="appl" />
         <tli id="T786" time="454.65" type="appl" />
         <tli id="T787" time="454.69" type="appl" />
         <tli id="T788" time="455.14" type="appl" />
         <tli id="T789" time="455.59" type="appl" />
         <tli id="T790" time="456.04" type="appl" />
         <tli id="T791" time="456.49" type="appl" />
         <tli id="T792" time="456.94" type="appl" />
         <tli id="T793" time="457.39" type="appl" />
         <tli id="T794" time="457.84000000000003" type="appl" />
         <tli id="T795" time="458.29" type="appl" />
         <tli id="T796" time="458.74" type="appl" />
         <tli id="T797" time="459.21714285714285" type="appl" />
         <tli id="T798" time="459.69428571428574" type="appl" />
         <tli id="T799" time="460.1714285714286" type="appl" />
         <tli id="T800" time="460.6485714285714" type="appl" />
         <tli id="T801" time="461.12571428571425" type="appl" />
         <tli id="T802" time="461.60285714285715" type="appl" />
         <tli id="T803" time="462.08" type="appl" />
         <tli id="T804" time="462.2" type="appl" />
         <tli id="T805" time="462.6485714285714" type="appl" />
         <tli id="T806" time="463.09714285714284" type="appl" />
         <tli id="T807" time="463.54571428571427" type="appl" />
         <tli id="T808" time="463.9942857142857" type="appl" />
         <tli id="T809" time="464.4428571428571" type="appl" />
         <tli id="T810" time="464.89142857142855" type="appl" />
         <tli id="T811" time="465.34" type="appl" />
         <tli id="T812" time="467.4" type="appl" />
         <tli id="T813" time="467.94624999999996" type="appl" />
         <tli id="T814" time="468.49249999999995" type="appl" />
         <tli id="T815" time="469.03875" type="appl" />
         <tli id="T816" time="469.585" type="appl" />
         <tli id="T817" time="470.13124999999997" type="appl" />
         <tli id="T818" time="470.6775" type="appl" />
         <tli id="T819" time="471.22375" type="appl" />
         <tli id="T820" time="471.77" type="appl" />
         <tli id="T821" time="472.0" type="appl" />
         <tli id="T822" time="472.47725" type="appl" />
         <tli id="T823" time="472.9545" type="appl" />
         <tli id="T824" time="473.43174999999997" type="appl" />
         <tli id="T825" time="473.909" type="appl" />
         <tli id="T826" time="473.94" type="appl" />
         <tli id="T827" time="474.53272727272724" type="appl" />
         <tli id="T828" time="475.12545454545455" type="appl" />
         <tli id="T829" time="475.7181818181818" type="appl" />
         <tli id="T830" time="476.3109090909091" type="appl" />
         <tli id="T831" time="476.90363636363634" type="appl" />
         <tli id="T832" time="477.49636363636364" type="appl" />
         <tli id="T833" time="478.0890909090909" type="appl" />
         <tli id="T834" time="478.6818181818182" type="appl" />
         <tli id="T835" time="479.27454545454543" type="appl" />
         <tli id="T836" time="479.86727272727273" type="appl" />
         <tli id="T837" time="480.46" type="appl" />
         <tli id="T838" time="481.59" type="appl" />
         <tli id="T839" time="482.6775" type="appl" />
         <tli id="T840" time="483.765" type="appl" />
         <tli id="T841" time="484.85249999999996" type="appl" />
         <tli id="T842" time="485.94" type="appl" />
         <tli id="T843" time="486.49" type="appl" />
         <tli id="T844" time="487.4116666666667" type="appl" />
         <tli id="T845" time="488.33333333333337" type="appl" />
         <tli id="T846" time="489.255" type="appl" />
         <tli id="T847" time="490.1766666666667" type="appl" />
         <tli id="T848" time="491.09833333333336" type="appl" />
         <tli id="T849" time="492.02" type="appl" />
         <tli id="T850" time="492.94166666666666" type="appl" />
         <tli id="T851" time="493.86333333333334" type="appl" />
         <tli id="T852" time="494.785" type="appl" />
         <tli id="T853" time="495.7066666666667" type="appl" />
         <tli id="T854" time="496.62833333333333" type="appl" />
         <tli id="T855" time="497.55" type="appl" />
         <tli id="T856" time="499.23" type="appl" />
         <tli id="T857" time="500.341" type="appl" />
         <tli id="T858" time="501.452" type="appl" />
         <tli id="T859" time="502.563" type="appl" />
         <tli id="T860" time="503.674" type="appl" />
         <tli id="T861" time="504.78499999999997" type="appl" />
         <tli id="T862" time="505.896" type="appl" />
         <tli id="T863" time="507.007" type="appl" />
         <tli id="T864" time="508.118" type="appl" />
         <tli id="T865" time="509.229" type="appl" />
         <tli id="T866" time="510.34" type="appl" />
         <tli id="T867" time="511.09" type="appl" />
         <tli id="T868" time="512.0577999999999" type="appl" />
         <tli id="T869" time="513.0255999999999" type="appl" />
         <tli id="T870" time="513.9934" type="appl" />
         <tli id="T871" time="514.9612" type="appl" />
         <tli id="T872" time="515.929" type="appl" />
         <tli id="T873" time="516.76" type="appl" />
         <tli id="T874" time="517.4485714285714" type="appl" />
         <tli id="T875" time="518.1371428571429" type="appl" />
         <tli id="T876" time="518.8257142857143" type="appl" />
         <tli id="T877" time="519.5142857142857" type="appl" />
         <tli id="T878" time="520.2028571428572" type="appl" />
         <tli id="T879" time="520.8914285714286" type="appl" />
         <tli id="T880" time="521.58" type="appl" />
         <tli id="T881" time="523.02" type="appl" />
         <tli id="T882" time="523.9133333333333" type="appl" />
         <tli id="T883" time="524.8066666666667" type="appl" />
         <tli id="T884" time="525.7" type="appl" />
         <tli id="T885" time="528.21" type="appl" />
         <tli id="T886" time="528.5523529411765" type="appl" />
         <tli id="T887" time="528.8947058823529" type="appl" />
         <tli id="T888" time="529.2370588235294" type="appl" />
         <tli id="T889" time="529.5794117647059" type="appl" />
         <tli id="T890" time="529.9217647058824" type="appl" />
         <tli id="T891" time="530.2641176470588" type="appl" />
         <tli id="T892" time="530.6064705882353" type="appl" />
         <tli id="T893" time="530.9488235294118" type="appl" />
         <tli id="T894" time="531.2911764705882" type="appl" />
         <tli id="T895" time="531.6335294117647" type="appl" />
         <tli id="T896" time="531.9758823529412" type="appl" />
         <tli id="T897" time="532.3182352941176" type="appl" />
         <tli id="T898" time="532.6605882352941" type="appl" />
         <tli id="T899" time="533.0029411764706" type="appl" />
         <tli id="T900" time="533.3452941176471" type="appl" />
         <tli id="T901" time="533.6876470588235" type="appl" />
         <tli id="T902" time="534.03" type="appl" />
         <tli id="T903" time="535.22" type="appl" />
         <tli id="T904" time="535.7166666666667" type="appl" />
         <tli id="T905" time="536.2133333333334" type="appl" />
         <tli id="T906" time="536.71" type="appl" />
         <tli id="T907" time="536.76" type="appl" />
         <tli id="T908" time="537.16" type="appl" />
         <tli id="T909" time="537.56" type="appl" />
         <tli id="T910" time="537.96" type="appl" />
         <tli id="T911" time="538.36" type="appl" />
         <tli id="T912" time="538.38" type="appl" />
         <tli id="T913" time="538.8566666666667" type="appl" />
         <tli id="T914" time="539.3333333333333" type="appl" />
         <tli id="T915" time="539.81" type="appl" />
         <tli id="T916" time="540.16" type="appl" />
         <tli id="T917" time="540.7314285714285" type="appl" />
         <tli id="T918" time="541.3028571428571" type="appl" />
         <tli id="T919" time="541.8742857142856" type="appl" />
         <tli id="T920" time="542.4457142857143" type="appl" />
         <tli id="T921" time="543.0171428571429" type="appl" />
         <tli id="T922" time="543.5885714285714" type="appl" />
         <tli id="T923" time="544.16" type="appl" />
         <tli id="T924" time="544.75" type="appl" />
         <tli id="T925" time="545.3399999999999" type="appl" />
         <tli id="T926" time="545.93" type="appl" />
         <tli id="T927" time="546.08" type="appl" />
         <tli id="T928" time="546.9675" type="appl" />
         <tli id="T929" time="547.855" type="appl" />
         <tli id="T930" time="548.7425000000001" type="appl" />
         <tli id="T931" time="549.63" type="appl" />
         <tli id="T932" time="549.71" type="appl" />
         <tli id="T933" time="551.495" type="appl" />
         <tli id="T934" time="553.28" type="appl" />
         <tli id="T935" time="553.4" type="appl" />
         <tli id="T936" time="554.05" type="appl" />
         <tli id="T937" time="554.7" type="appl" />
         <tli id="T938" time="555.1560000000001" type="appl" />
         <tli id="T939" time="555.6120000000001" type="appl" />
         <tli id="T940" time="556.068" type="appl" />
         <tli id="T941" time="556.524" type="appl" />
         <tli id="T942" time="556.98" type="appl" />
         <tli id="T943" time="557.02" type="appl" />
         <tli id="T944" time="557.496" type="appl" />
         <tli id="T945" time="557.972" type="appl" />
         <tli id="T946" time="558.448" type="appl" />
         <tli id="T947" time="558.924" type="appl" />
         <tli id="T948" time="559.4" type="appl" />
         <tli id="T949" time="559.87" type="appl" />
         <tli id="T950" time="560.42875" type="appl" />
         <tli id="T951" time="560.9875" type="appl" />
         <tli id="T952" time="561.54625" type="appl" />
         <tli id="T953" time="562.105" type="appl" />
         <tli id="T954" time="562.66375" type="appl" />
         <tli id="T955" time="563.2225000000001" type="appl" />
         <tli id="T956" time="563.78125" type="appl" />
         <tli id="T957" time="564.34" type="appl" />
         <tli id="T958" time="564.54" type="appl" />
         <tli id="T959" time="565.006" type="appl" />
         <tli id="T960" time="565.472" type="appl" />
         <tli id="T961" time="565.938" type="appl" />
         <tli id="T962" time="566.404" type="appl" />
         <tli id="T963" time="566.87" type="appl" />
         <tli id="T964" time="567.336" type="appl" />
         <tli id="T965" time="567.802" type="appl" />
         <tli id="T966" time="568.268" type="appl" />
         <tli id="T967" time="568.734" type="appl" />
         <tli id="T968" time="569.2" type="appl" />
         <tli id="T969" time="569.29" type="appl" />
         <tli id="T970" time="569.752" type="appl" />
         <tli id="T971" time="570.2139999999999" type="appl" />
         <tli id="T972" time="570.676" type="appl" />
         <tli id="T973" time="571.138" type="appl" />
         <tli id="T974" time="571.6" type="appl" />
         <tli id="T975" time="571.64" type="appl" />
         <tli id="T976" time="572.1566666666666" type="appl" />
         <tli id="T977" time="572.6733333333334" type="appl" />
         <tli id="T978" time="573.19" type="appl" />
         <tli id="T979" time="573.24" type="appl" />
         <tli id="T980" time="573.73" type="appl" />
         <tli id="T981" time="574.22" type="appl" />
         <tli id="T982" time="574.71" type="appl" />
         <tli id="T983" time="574.81" type="appl" />
         <tli id="T984" time="575.2662499999999" type="appl" />
         <tli id="T985" time="575.7225" type="appl" />
         <tli id="T986" time="576.17875" type="appl" />
         <tli id="T987" time="576.635" type="appl" />
         <tli id="T988" time="577.09125" type="appl" />
         <tli id="T989" time="577.5475" type="appl" />
         <tli id="T990" time="578.0037500000001" type="appl" />
         <tli id="T991" time="578.46" type="appl" />
         <tli id="T992" time="579.03" type="appl" />
         <tli id="T993" time="580.20625" type="appl" />
         <tli id="T994" time="581.3824999999999" type="appl" />
         <tli id="T995" time="582.55875" type="appl" />
         <tli id="T996" time="583.735" type="appl" />
         <tli id="T997" time="584.91125" type="appl" />
         <tli id="T998" time="586.0875000000001" type="appl" />
         <tli id="T999" time="587.2637500000001" type="appl" />
         <tli id="T1000" time="588.44" type="appl" />
         <tli id="T1001" time="588.47" type="appl" />
         <tli id="T1002" time="588.9933333333333" type="appl" />
         <tli id="T1003" time="589.5166666666667" type="appl" />
         <tli id="T1004" time="590.0400000000001" type="appl" />
         <tli id="T1005" time="590.5633333333334" type="appl" />
         <tli id="T1006" time="591.0866666666667" type="appl" />
         <tli id="T1007" time="591.61" type="appl" />
         <tli id="T1008" time="592.1333333333333" type="appl" />
         <tli id="T1009" time="592.6566666666668" type="appl" />
         <tli id="T1010" time="593.1800000000001" type="appl" />
         <tli id="T1011" time="593.7033333333334" type="appl" />
         <tli id="T1012" time="594.2266666666667" type="appl" />
         <tli id="T1013" time="594.75" type="appl" />
         <tli id="T1014" time="595.2733333333334" type="appl" />
         <tli id="T1015" time="595.7966666666667" type="appl" />
         <tli id="T1016" time="596.32" type="appl" />
         <tli id="T1017" time="596.45" type="appl" />
         <tli id="T1018" time="597.01" type="appl" />
         <tli id="T1019" time="597.57" type="appl" />
         <tli id="T1020" time="598.13" type="appl" />
         <tli id="T1021" time="598.69" type="appl" />
         <tli id="T1022" time="599.25" type="appl" />
         <tli id="T1023" time="599.8100000000001" type="appl" />
         <tli id="T1024" time="600.37" type="appl" />
         <tli id="T1025" time="600.48" type="appl" />
         <tli id="T1026" time="601.0975000000001" type="appl" />
         <tli id="T1027" time="601.715" type="appl" />
         <tli id="T1028" time="602.3325" type="appl" />
         <tli id="T1029" time="602.95" type="appl" />
         <tli id="T1030" time="603.3" type="appl" />
         <tli id="T1031" time="603.72375" type="appl" />
         <tli id="T1032" time="604.1475" type="appl" />
         <tli id="T1033" time="604.57125" type="appl" />
         <tli id="T1034" time="604.995" type="appl" />
         <tli id="T1035" time="605.41875" type="appl" />
         <tli id="T1036" time="605.8425" type="appl" />
         <tli id="T1037" time="606.26625" type="appl" />
         <tli id="T1038" time="606.69" type="appl" />
         <tli id="T1039" time="607.53" type="appl" />
         <tli id="T1040" time="607.9812499999999" type="appl" />
         <tli id="T1041" time="608.4325" type="appl" />
         <tli id="T1042" time="608.88375" type="appl" />
         <tli id="T1043" time="609.335" type="appl" />
         <tli id="T1044" time="609.78625" type="appl" />
         <tli id="T1045" time="610.2375" type="appl" />
         <tli id="T1046" time="610.68875" type="appl" />
         <tli id="T1047" time="611.14" type="appl" />
         <tli id="T1048" time="611.44" type="appl" />
         <tli id="T1049" time="611.7242857142858" type="appl" />
         <tli id="T1050" time="612.0085714285715" type="appl" />
         <tli id="T1051" time="612.2928571428572" type="appl" />
         <tli id="T1052" time="612.5771428571428" type="appl" />
         <tli id="T1053" time="612.8614285714285" type="appl" />
         <tli id="T1054" time="613.1457142857142" type="appl" />
         <tli id="T1055" time="613.43" type="appl" />
         <tli id="T1056" time="613.5" type="appl" />
         <tli id="T1057" time="614.02" type="appl" />
         <tli id="T1058" time="614.54" type="appl" />
         <tli id="T1059" time="615.0600000000001" type="appl" />
         <tli id="T1060" time="615.58" type="appl" />
         <tli id="T1061" time="615.65" type="appl" />
         <tli id="T1062" time="616.645" type="appl" />
         <tli id="T1063" time="617.64" type="appl" />
         <tli id="T1064" time="618.01" type="appl" />
         <tli id="T1065" time="618.48" type="appl" />
         <tli id="T1066" time="618.9499999999999" type="appl" />
         <tli id="T1067" time="619.42" type="appl" />
         <tli id="T1068" time="619.89" type="appl" />
         <tli id="T1069" time="620.36" type="appl" />
         <tli id="T1070" time="620.8299999999999" type="appl" />
         <tli id="T1071" time="621.3" type="appl" />
         <tli id="T1072" time="623.33" type="appl" />
         <tli id="T1073" time="623.9725000000001" type="appl" />
         <tli id="T1074" time="624.615" type="appl" />
         <tli id="T1075" time="625.2575" type="appl" />
         <tli id="T1076" time="625.9" type="appl" />
         <tli id="T1077" time="626.5425" type="appl" />
         <tli id="T1078" time="627.185" type="appl" />
         <tli id="T1079" time="627.8275" type="appl" />
         <tli id="T1080" time="628.47" type="appl" />
         <tli id="T1081" time="629.1125" type="appl" />
         <tli id="T1082" time="629.755" type="appl" />
         <tli id="T1083" time="630.3974999999999" type="appl" />
         <tli id="T1084" time="631.04" type="appl" />
         <tli id="T1085" time="631.33" type="appl" />
         <tli id="T1086" time="632.092" type="appl" />
         <tli id="T1087" time="632.854" type="appl" />
         <tli id="T1088" time="633.616" type="appl" />
         <tli id="T1089" time="634.378" type="appl" />
         <tli id="T1090" time="635.14" type="appl" />
         <tli id="T1091" time="635.28" type="appl" />
         <tli id="T1092" time="636.184" type="appl" />
         <tli id="T1093" time="637.088" type="appl" />
         <tli id="T1094" time="637.992" type="appl" />
         <tli id="T1095" time="638.896" type="appl" />
         <tli id="T1096" time="639.8" type="appl" />
         <tli id="T1097" time="640.63" type="appl" />
         <tli id="T1098" time="641.2833333333333" type="appl" />
         <tli id="T1099" time="641.9366666666667" type="appl" />
         <tli id="T1100" time="642.59" type="appl" />
         <tli id="T1101" time="642.63" type="appl" />
         <tli id="T1102" time="643.3266666666666" type="appl" />
         <tli id="T1103" time="644.0233333333333" type="appl" />
         <tli id="T1104" time="644.72" type="appl" />
         <tli id="T1105" time="645.4166666666666" type="appl" />
         <tli id="T1106" time="646.1133333333332" type="appl" />
         <tli id="T1107" time="646.81" type="appl" />
         <tli id="T1108" time="647.01" type="appl" />
         <tli id="T1109" time="647.585" type="appl" />
         <tli id="T1110" time="648.16" type="appl" />
         <tli id="T1111" time="648.735" type="appl" />
         <tli id="T1112" time="649.3100000000001" type="appl" />
         <tli id="T1113" time="649.885" type="appl" />
         <tli id="T1114" time="650.46" type="appl" />
         <tli id="T1115" time="650.54" type="appl" />
         <tli id="T1116" time="651.184" type="appl" />
         <tli id="T1117" time="651.828" type="appl" />
         <tli id="T1118" time="652.472" type="appl" />
         <tli id="T1119" time="653.116" type="appl" />
         <tli id="T1120" time="653.76" type="appl" />
         <tli id="T1121" time="654.25" type="appl" />
         <tli id="T1122" time="655.0133333333333" type="appl" />
         <tli id="T1123" time="655.7766666666666" type="appl" />
         <tli id="T1124" time="656.54" type="appl" />
         <tli id="T1125" time="656.65" type="appl" />
         <tli id="T1126" time="657.2188888888888" type="appl" />
         <tli id="T1127" time="657.7877777777777" type="appl" />
         <tli id="T1128" time="658.3566666666667" type="appl" />
         <tli id="T1129" time="658.9255555555555" type="appl" />
         <tli id="T1130" time="659.4944444444444" type="appl" />
         <tli id="T1131" time="660.0633333333333" type="appl" />
         <tli id="T1132" time="660.6322222222223" type="appl" />
         <tli id="T1133" time="661.2011111111111" type="appl" />
         <tli id="T1134" time="661.77" type="appl" />
         <tli id="T1135" time="663.91" type="appl" />
         <tli id="T1136" time="664.3414285714285" type="appl" />
         <tli id="T1137" time="664.7728571428571" type="appl" />
         <tli id="T1138" time="665.2042857142857" type="appl" />
         <tli id="T1139" time="665.6357142857142" type="appl" />
         <tli id="T1140" time="666.0671428571428" type="appl" />
         <tli id="T1141" time="666.4985714285714" type="appl" />
         <tli id="T1142" time="666.9300000000001" type="appl" />
         <tli id="T1143" time="667.3614285714286" type="appl" />
         <tli id="T1144" time="667.7928571428572" type="appl" />
         <tli id="T1145" time="668.2242857142858" type="appl" />
         <tli id="T1146" time="668.6557142857143" type="appl" />
         <tli id="T1147" time="669.0871428571429" type="appl" />
         <tli id="T1148" time="669.5185714285715" type="appl" />
         <tli id="T1149" time="669.95" type="appl" />
         <tli id="T0" time="681.384" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="unknown"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T15" id="Seg_0" n="sc" s="T1">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Biːrde</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">öjdüːbün</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">küččügüj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">erdeppinen</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">Nahoŋŋa</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">točkabɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">Kiresten</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T8">u͡on</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_28" n="HIAT:w" s="T9">ikki</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_31" n="HIAT:w" s="T10">kilometr</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_35" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">Onno</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">töhölöːk</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_43" n="HIAT:w" s="T13">kihi</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_46" n="HIAT:w" s="T14">olorbutaj</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T21" id="Seg_49" n="sc" s="T16">
               <ts e="T21" id="Seg_51" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_53" n="HIAT:w" s="T16">Kɨrdʼagastar</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_56" n="HIAT:w" s="T17">barɨlara</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_59" n="HIAT:w" s="T18">onno</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_62" n="HIAT:w" s="T19">oloroːčču</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_65" n="HIAT:w" s="T20">etilere</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T27" id="Seg_68" n="sc" s="T22">
               <ts e="T27" id="Seg_70" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_72" n="HIAT:w" s="T22">Hɨrga</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_75" n="HIAT:w" s="T23">di͡e</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_78" n="HIAT:w" s="T24">toloru</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_81" n="HIAT:w" s="T25">ete</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_84" n="HIAT:w" s="T26">onno</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T32" id="Seg_87" n="sc" s="T28">
               <ts e="T32" id="Seg_89" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_91" n="HIAT:w" s="T28">Tabalar</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_94" n="HIAT:w" s="T29">tu͡oktar</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_97" n="HIAT:w" s="T30">baːr</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_100" n="HIAT:w" s="T31">etilere</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T52" id="Seg_103" n="sc" s="T33">
               <ts e="T52" id="Seg_105" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_107" n="HIAT:w" s="T33">Onton</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_110" n="HIAT:w" s="T34">biːrde</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_113" n="HIAT:w" s="T35">teːtemeːk</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_117" n="HIAT:w" s="T36">dʼi͡eleriger</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_120" n="HIAT:w" s="T37">teːtem</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_123" n="HIAT:w" s="T38">kɨrdʼagas</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_126" n="HIAT:w" s="T39">ehebin</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_129" n="HIAT:w" s="T40">di͡eččibin</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_132" n="HIAT:w" s="T41">bu͡o</ts>
                  <nts id="Seg_133" n="HIAT:ip">,</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_136" n="HIAT:w" s="T42">teːte</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_139" n="HIAT:w" s="T43">di͡eččibin</ts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_143" n="HIAT:w" s="T44">kɨrdʼagas</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_146" n="HIAT:w" s="T45">maːmabɨn</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_149" n="HIAT:w" s="T46">maːma</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_152" n="HIAT:w" s="T47">di͡eččibin</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_155" n="HIAT:w" s="T48">kɨrdʼagas</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_158" n="HIAT:w" s="T49">ebebin</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_161" n="HIAT:w" s="T50">ginilerge</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_164" n="HIAT:w" s="T51">iːtillibitim</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T60" id="Seg_167" n="sc" s="T53">
               <ts e="T60" id="Seg_169" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_171" n="HIAT:w" s="T53">De</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_174" n="HIAT:w" s="T54">ol</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_177" n="HIAT:w" s="T55">teːtebit</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_180" n="HIAT:w" s="T56">kajdi͡ek</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_183" n="HIAT:w" s="T57">ere</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_186" n="HIAT:w" s="T58">baran</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_189" n="HIAT:w" s="T59">kaːlla</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T72" id="Seg_192" n="sc" s="T61">
               <ts e="T72" id="Seg_194" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_196" n="HIAT:w" s="T61">Mastana</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_199" n="HIAT:w" s="T62">barda</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_202" n="HIAT:w" s="T63">eni</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_205" n="HIAT:w" s="T64">de</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_208" n="HIAT:w" s="T65">olorobut</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_211" n="HIAT:w" s="T66">tabalarbɨt</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_214" n="HIAT:w" s="T67">prjamo</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_217" n="HIAT:w" s="T68">kimŋe</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_220" n="HIAT:w" s="T69">dʼi͡ebit</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_223" n="HIAT:w" s="T70">tahɨgar</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_226" n="HIAT:w" s="T71">hɨldʼallar</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T76" id="Seg_229" n="sc" s="T73">
               <ts e="T76" id="Seg_231" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_233" n="HIAT:w" s="T73">Tabɨja</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_236" n="HIAT:w" s="T74">hɨldʼallar</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_239" n="HIAT:w" s="T75">budto</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T79" id="Seg_242" n="sc" s="T77">
               <ts e="T79" id="Seg_244" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_246" n="HIAT:w" s="T77">Aːkulaːk</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_249" n="HIAT:w" s="T78">etibit</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T83" id="Seg_252" n="sc" s="T80">
               <ts e="T83" id="Seg_254" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_256" n="HIAT:w" s="T80">Bu</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_259" n="HIAT:w" s="T81">iliːten</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_262" n="HIAT:w" s="T82">ahɨːr</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T86" id="Seg_265" n="sc" s="T84">
               <ts e="T86" id="Seg_267" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_269" n="HIAT:w" s="T84">Hili͡ebimsek</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_272" n="HIAT:w" s="T85">bagajɨ</ts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_275" n="sc" s="T87">
               <ts e="T91" id="Seg_277" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_279" n="HIAT:w" s="T87">Tünnükke</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_282" n="HIAT:w" s="T88">keleːčči</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_285" n="HIAT:w" s="T89">hili͡eb</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_288" n="HIAT:w" s="T90">kördönö</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T105" id="Seg_291" n="sc" s="T92">
               <ts e="T105" id="Seg_293" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_295" n="HIAT:w" s="T92">O</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_298" n="HIAT:w" s="T93">munnuta</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_301" n="HIAT:w" s="T94">nʼaltajan</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_304" n="HIAT:w" s="T95">keli͡e</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_307" n="HIAT:w" s="T96">di͡en</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_310" n="HIAT:w" s="T97">oː</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_313" n="HIAT:w" s="T98">hohujabɨn</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_316" n="HIAT:w" s="T99">küččügüj</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_319" n="HIAT:w" s="T100">kihi</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_322" n="HIAT:w" s="T101">ke</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_325" n="HIAT:w" s="T102">kuttanaːččɨ</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_328" n="HIAT:w" s="T103">etim</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_331" n="HIAT:w" s="T104">bu͡o</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T112" id="Seg_334" n="sc" s="T106">
               <ts e="T112" id="Seg_336" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_338" n="HIAT:w" s="T106">Uː</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_341" n="HIAT:w" s="T107">bu</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_344" n="HIAT:w" s="T108">kihi</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_347" n="HIAT:w" s="T109">kisten</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_350" n="HIAT:w" s="T110">arɨjdɨm</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_353" n="HIAT:w" s="T111">bu͡o</ts>
                  <nts id="Seg_354" n="HIAT:ip">.</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T118" id="Seg_356" n="sc" s="T113">
               <ts e="T118" id="Seg_358" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_360" n="HIAT:w" s="T113">Ostolton</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_363" n="HIAT:w" s="T114">bu</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_366" n="HIAT:w" s="T115">hili͡eb</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_369" n="HIAT:w" s="T116">lepi͡eske</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_372" n="HIAT:w" s="T117">bagajɨlar</ts>
                  <nts id="Seg_373" n="HIAT:ip">.</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T126" id="Seg_375" n="sc" s="T119">
               <ts e="T126" id="Seg_377" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_379" n="HIAT:w" s="T119">Oː</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_382" n="HIAT:w" s="T120">ontum</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_385" n="HIAT:w" s="T121">tülejbit</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_388" n="HIAT:w" s="T122">hiː</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_391" n="HIAT:w" s="T123">hi͡ektiːr</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_394" n="HIAT:w" s="T124">di͡en</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_397" n="HIAT:w" s="T125">araːrakata</ts>
                  <nts id="Seg_398" n="HIAT:ip">.</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T134" id="Seg_400" n="sc" s="T127">
               <ts e="T134" id="Seg_402" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_404" n="HIAT:w" s="T127">De</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_407" n="HIAT:w" s="T128">ol</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_410" n="HIAT:w" s="T129">maːmam</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_413" n="HIAT:w" s="T130">diːr</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_416" n="HIAT:w" s="T131">kɨrdʼagas</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_419" n="HIAT:w" s="T132">maːmam</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_422" n="HIAT:w" s="T133">ke</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T142" id="Seg_425" n="sc" s="T135">
               <ts e="T142" id="Seg_427" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_429" n="HIAT:w" s="T135">Oː</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_432" n="HIAT:w" s="T136">ü͡öretime</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_435" n="HIAT:w" s="T137">dʼe</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_438" n="HIAT:w" s="T138">en</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_441" n="HIAT:w" s="T139">tünnükpütün</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_444" n="HIAT:w" s="T140">aldʼatɨ͡a</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_447" n="HIAT:w" s="T141">kanʼɨ͡a</ts>
                  <nts id="Seg_448" n="HIAT:ip">.</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T152" id="Seg_450" n="sc" s="T143">
               <ts e="T152" id="Seg_452" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_454" n="HIAT:w" s="T143">Bar</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_457" n="HIAT:w" s="T144">bar</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_460" n="HIAT:w" s="T145">bar</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_463" n="HIAT:w" s="T146">kelime</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_466" n="HIAT:w" s="T147">bolše</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_469" n="HIAT:w" s="T148">diːr</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_472" n="HIAT:w" s="T149">tu͡ok</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_475" n="HIAT:w" s="T150">da</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_478" n="HIAT:w" s="T151">hu͡ok</ts>
                  <nts id="Seg_479" n="HIAT:ip">.</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T160" id="Seg_481" n="sc" s="T153">
               <ts e="T160" id="Seg_483" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_485" n="HIAT:w" s="T153">Okkun</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_488" n="HIAT:w" s="T154">hi͡e</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_491" n="HIAT:w" s="T155">bar</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_494" n="HIAT:w" s="T156">diːr</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_497" n="HIAT:w" s="T157">heː</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_500" n="HIAT:w" s="T158">haban</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_503" n="HIAT:w" s="T159">keːste</ts>
                  <nts id="Seg_504" n="HIAT:ip">.</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_506" n="sc" s="T161">
               <ts e="T169" id="Seg_508" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_510" n="HIAT:w" s="T161">Onton</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_513" n="HIAT:w" s="T162">biːrges</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_516" n="HIAT:w" s="T163">tünnügünen</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_519" n="HIAT:w" s="T164">keler</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_522" n="HIAT:w" s="T165">onno</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_525" n="HIAT:w" s="T166">peredok</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_528" n="HIAT:w" s="T167">bu͡olaːččɨ</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_531" n="HIAT:w" s="T168">bu͡o</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T174" id="Seg_534" n="sc" s="T170">
               <ts e="T174" id="Seg_536" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_538" n="HIAT:w" s="T170">Onno</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_541" n="HIAT:w" s="T171">buːs</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_544" n="HIAT:w" s="T172">tu͡ok</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_547" n="HIAT:w" s="T173">uːrunaːččɨbɨt</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T180" id="Seg_550" n="sc" s="T175">
               <ts e="T180" id="Seg_552" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_554" n="HIAT:w" s="T175">Bu</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_557" n="HIAT:w" s="T176">ke</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_560" n="HIAT:w" s="T177">uːbut</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_563" n="HIAT:w" s="T178">bu͡o</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_566" n="HIAT:w" s="T179">buːsput</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T190" id="Seg_569" n="sc" s="T181">
               <ts e="T190" id="Seg_571" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_573" n="HIAT:w" s="T181">Onno</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_576" n="HIAT:w" s="T182">ɨtta</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_579" n="HIAT:w" s="T183">ɨtta</ts>
                  <nts id="Seg_580" n="HIAT:ip">,</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_583" n="HIAT:w" s="T184">tünnükpüt</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_586" n="HIAT:w" s="T185">arɨllan</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_589" n="HIAT:w" s="T186">turar</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_592" n="HIAT:w" s="T187">bu͡o</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_595" n="HIAT:w" s="T188">innikiːbit</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_598" n="HIAT:w" s="T189">ke</ts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T196" id="Seg_601" n="sc" s="T191">
               <ts e="T196" id="Seg_603" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_605" n="HIAT:w" s="T191">Onno</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_608" n="HIAT:w" s="T192">moltojo</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_611" n="HIAT:w" s="T193">moltojo</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_614" n="HIAT:w" s="T194">mu͡ostardaːk</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_617" n="HIAT:w" s="T195">bu͡o</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T208" id="Seg_620" n="sc" s="T197">
               <ts e="T208" id="Seg_622" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_624" n="HIAT:w" s="T197">Mu͡ostara</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_627" n="HIAT:w" s="T198">bappattar</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_630" n="HIAT:w" s="T199">kim</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_633" n="HIAT:w" s="T200">čoŋalge</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_636" n="HIAT:w" s="T201">lepi͡eske</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_639" n="HIAT:w" s="T202">turar</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_642" n="HIAT:w" s="T203">ete</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_644" n="HIAT:ip">(</nts>
                  <ts e="T205" id="Seg_646" n="HIAT:w" s="T204">lepi͡es-</ts>
                  <nts id="Seg_647" n="HIAT:ip">)</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_649" n="HIAT:ip">(</nts>
                  <ts e="T206" id="Seg_651" n="HIAT:w" s="T205">aldʼal-</ts>
                  <nts id="Seg_652" n="HIAT:ip">)</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_655" n="HIAT:w" s="T206">kimneːbit</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_658" n="HIAT:w" s="T207">bɨhɨllɨbɨttar</ts>
                  <nts id="Seg_659" n="HIAT:ip">.</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T217" id="Seg_661" n="sc" s="T209">
               <ts e="T217" id="Seg_663" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_665" n="HIAT:w" s="T209">De</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_668" n="HIAT:w" s="T210">onu</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_671" n="HIAT:w" s="T211">tiːje</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_674" n="HIAT:w" s="T212">hataːta</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_677" n="HIAT:w" s="T213">bu͡o</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_680" n="HIAT:w" s="T214">tɨlɨn</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_683" n="HIAT:w" s="T215">uːna</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_686" n="HIAT:w" s="T216">uːna</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T231" id="Seg_689" n="sc" s="T218">
               <ts e="T231" id="Seg_691" n="HIAT:u" s="T218">
                  <nts id="Seg_692" n="HIAT:ip">"</nts>
                  <ts e="T219" id="Seg_694" n="HIAT:w" s="T218">Maːma</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_697" n="HIAT:w" s="T219">kör</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_700" n="HIAT:w" s="T220">kör</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_703" n="HIAT:w" s="T221">kör</ts>
                  <nts id="Seg_704" n="HIAT:ip">"</nts>
                  <nts id="Seg_705" n="HIAT:ip">,</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_708" n="HIAT:w" s="T222">diːbin</ts>
                  <nts id="Seg_709" n="HIAT:ip">,</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_711" n="HIAT:ip">"</nts>
                  <ts e="T224" id="Seg_713" n="HIAT:w" s="T223">kimŋin</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_716" n="HIAT:w" s="T224">hi͡eri</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_719" n="HIAT:w" s="T225">gɨnna</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_722" n="HIAT:w" s="T226">kili͡eptergin</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_725" n="HIAT:w" s="T227">hi͡eri</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_728" n="HIAT:w" s="T228">gɨnna</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_731" n="HIAT:w" s="T229">diːbin</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_734" n="HIAT:w" s="T230">kili͡eptergin</ts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip">"</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_738" n="sc" s="T232">
               <ts e="T243" id="Seg_740" n="HIAT:u" s="T232">
                  <nts id="Seg_741" n="HIAT:ip">"</nts>
                  <ts e="T233" id="Seg_743" n="HIAT:w" s="T232">Oj</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_746" n="HIAT:w" s="T233">togo</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_749" n="HIAT:w" s="T234">satana</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_752" n="HIAT:w" s="T235">kɨːha</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_755" n="HIAT:w" s="T236">togo</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_758" n="HIAT:w" s="T237">ü͡öreppikkinij</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_761" n="HIAT:w" s="T238">bu͡o</ts>
                  <nts id="Seg_762" n="HIAT:ip">,</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_765" n="HIAT:w" s="T239">dʼi͡etten</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_768" n="HIAT:w" s="T240">ahaːta</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_771" n="HIAT:w" s="T241">tünnükten</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_774" n="HIAT:w" s="T242">ahaːta</ts>
                  <nts id="Seg_775" n="HIAT:ip">.</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T249" id="Seg_777" n="sc" s="T244">
               <ts e="T249" id="Seg_779" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_781" n="HIAT:w" s="T244">Kör</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_784" n="HIAT:w" s="T245">anɨ</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_787" n="HIAT:w" s="T246">barɨtɨn</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_790" n="HIAT:w" s="T247">u͡oru͡oga</ts>
                  <nts id="Seg_791" n="HIAT:ip">"</nts>
                  <nts id="Seg_792" n="HIAT:ip">,</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_795" n="HIAT:w" s="T248">diːr</ts>
                  <nts id="Seg_796" n="HIAT:ip">.</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T253" id="Seg_798" n="sc" s="T250">
               <ts e="T253" id="Seg_800" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_802" n="HIAT:w" s="T250">Araː</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_805" n="HIAT:w" s="T251">bɨ͡ar</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_808" n="HIAT:w" s="T252">agajbɨn</ts>
                  <nts id="Seg_809" n="HIAT:ip">.</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T257" id="Seg_811" n="sc" s="T254">
               <ts e="T257" id="Seg_813" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_815" n="HIAT:w" s="T254">Uː</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_818" n="HIAT:w" s="T255">bar</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_821" n="HIAT:w" s="T256">bar</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T263" id="Seg_824" n="sc" s="T258">
               <ts e="T263" id="Seg_826" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_828" n="HIAT:w" s="T258">Tu͡ok</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_831" n="HIAT:w" s="T259">ere</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_834" n="HIAT:w" s="T260">trepkenen</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_837" n="HIAT:w" s="T261">leš</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_840" n="HIAT:w" s="T262">leš</ts>
                  <nts id="Seg_841" n="HIAT:ip">.</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T266" id="Seg_843" n="sc" s="T264">
               <ts e="T266" id="Seg_845" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_847" n="HIAT:w" s="T264">Onton</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_850" n="HIAT:w" s="T265">barbat</ts>
                  <nts id="Seg_851" n="HIAT:ip">.</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T271" id="Seg_853" n="sc" s="T267">
               <ts e="T271" id="Seg_855" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_857" n="HIAT:w" s="T267">De</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_860" n="HIAT:w" s="T268">na</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_863" n="HIAT:w" s="T269">vso</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_866" n="HIAT:w" s="T270">bar</ts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T275" id="Seg_869" n="sc" s="T272">
               <ts e="T275" id="Seg_871" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_873" n="HIAT:w" s="T272">Bi͡eren</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_876" n="HIAT:w" s="T273">keːher</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_879" n="HIAT:w" s="T274">bu͡o</ts>
                  <nts id="Seg_880" n="HIAT:ip">.</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T293" id="Seg_882" n="sc" s="T276">
               <ts e="T293" id="Seg_884" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_886" n="HIAT:w" s="T276">Ontuŋ</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_889" n="HIAT:w" s="T277">momuja</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_892" n="HIAT:w" s="T278">momuja</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_895" n="HIAT:w" s="T279">voːpse</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_898" n="HIAT:w" s="T280">baraːktɨːr</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_901" n="HIAT:w" s="T281">di͡en</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_904" n="HIAT:w" s="T282">hi͡ese</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_907" n="HIAT:w" s="T283">hili͡ebi</ts>
                  <nts id="Seg_908" n="HIAT:ip">,</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_911" n="HIAT:w" s="T284">hi͡ese</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_914" n="HIAT:w" s="T285">totto</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_917" n="HIAT:w" s="T286">bɨhɨlaːk</ts>
                  <nts id="Seg_918" n="HIAT:ip">,</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_921" n="HIAT:w" s="T287">tu͡ok</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_924" n="HIAT:w" s="T288">toholaːk</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_927" n="HIAT:w" s="T289">totu͡oj</ts>
                  <nts id="Seg_928" n="HIAT:ip">,</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_931" n="HIAT:w" s="T290">de</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_934" n="HIAT:w" s="T291">ol</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_937" n="HIAT:w" s="T292">kačɨgɨrɨtar</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T299" id="Seg_940" n="sc" s="T294">
               <ts e="T299" id="Seg_942" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_944" n="HIAT:w" s="T294">Okazɨvaetsa</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_947" n="HIAT:w" s="T295">kimi</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_950" n="HIAT:w" s="T296">bi͡erbit</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_953" n="HIAT:w" s="T297">kappɨt</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_956" n="HIAT:w" s="T298">hili͡ebi</ts>
                  <nts id="Seg_957" n="HIAT:ip">.</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T305" id="Seg_959" n="sc" s="T300">
               <ts e="T305" id="Seg_961" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_963" n="HIAT:w" s="T300">De</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_966" n="HIAT:w" s="T301">onno</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_969" n="HIAT:w" s="T302">ɨstɨː</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_972" n="HIAT:w" s="T303">hataːta</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_975" n="HIAT:w" s="T304">bu͡o</ts>
                  <nts id="Seg_976" n="HIAT:ip">.</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T308" id="Seg_978" n="sc" s="T306">
               <ts e="T308" id="Seg_980" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_982" n="HIAT:w" s="T306">Araːle</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_985" n="HIAT:w" s="T307">beseleːtin</ts>
                  <nts id="Seg_986" n="HIAT:ip">.</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T314" id="Seg_988" n="sc" s="T309">
               <ts e="T314" id="Seg_990" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_992" n="HIAT:w" s="T309">Oj</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_995" n="HIAT:w" s="T310">tiːsterin</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_998" n="HIAT:w" s="T311">barɨtɨn</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1001" n="HIAT:w" s="T312">kimneːte</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1004" n="HIAT:w" s="T313">bɨhɨlak</ts>
                  <nts id="Seg_1005" n="HIAT:ip">.</nts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T321" id="Seg_1007" n="sc" s="T315">
               <ts e="T321" id="Seg_1009" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1011" n="HIAT:w" s="T315">De</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1014" n="HIAT:w" s="T316">ol</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1017" n="HIAT:w" s="T317">barbɨta</ts>
                  <nts id="Seg_1018" n="HIAT:ip">,</nts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1021" n="HIAT:w" s="T318">hi͡ese</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1024" n="HIAT:w" s="T319">dogottorun</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1027" n="HIAT:w" s="T320">di͡ek</ts>
                  <nts id="Seg_1028" n="HIAT:ip">.</nts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T328" id="Seg_1030" n="sc" s="T322">
               <ts e="T328" id="Seg_1032" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1034" n="HIAT:w" s="T322">Hɨːr</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1037" n="HIAT:w" s="T323">annɨtɨgar</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1040" n="HIAT:w" s="T324">tüspüttere</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1043" n="HIAT:w" s="T325">onno</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1046" n="HIAT:w" s="T326">ebege</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1049" n="HIAT:w" s="T327">hɨldʼallar</ts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T334" id="Seg_1052" n="sc" s="T329">
               <ts e="T334" id="Seg_1054" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1056" n="HIAT:w" s="T329">Tahaːrabɨt</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1059" n="HIAT:w" s="T330">boru͡orda</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1062" n="HIAT:w" s="T331">karaŋarar</ts>
                  <nts id="Seg_1063" n="HIAT:ip">,</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1066" n="HIAT:w" s="T332">karaŋaran</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1069" n="HIAT:w" s="T333">iher</ts>
                  <nts id="Seg_1070" n="HIAT:ip">.</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T347" id="Seg_1072" n="sc" s="T335">
               <ts e="T347" id="Seg_1074" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1076" n="HIAT:w" s="T335">Kim</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1079" n="HIAT:w" s="T336">ɨttaːk</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1082" n="HIAT:w" s="T337">etibit</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1085" n="HIAT:w" s="T338">bihigi</ts>
                  <nts id="Seg_1086" n="HIAT:ip">,</nts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1089" n="HIAT:w" s="T339">Djoŋgo</ts>
                  <nts id="Seg_1090" n="HIAT:ip">,</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1093" n="HIAT:w" s="T340">čeːlke</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1096" n="HIAT:w" s="T341">bagajɨ</ts>
                  <nts id="Seg_1097" n="HIAT:ip">,</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1100" n="HIAT:w" s="T342">bosku͡oj</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1103" n="HIAT:w" s="T343">bagajɨ</ts>
                  <nts id="Seg_1104" n="HIAT:ip">,</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1107" n="HIAT:w" s="T344">tabahɨt</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1110" n="HIAT:w" s="T345">bagajɨ</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1113" n="HIAT:w" s="T346">ontuŋ</ts>
                  <nts id="Seg_1114" n="HIAT:ip">.</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T357" id="Seg_1116" n="sc" s="T348">
               <ts e="T357" id="Seg_1118" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1120" n="HIAT:w" s="T348">Kim</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1123" n="HIAT:w" s="T349">hüs</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1126" n="HIAT:w" s="T350">di͡eŋ</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1129" n="HIAT:w" s="T351">du͡o</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1132" n="HIAT:w" s="T352">kimi</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1135" n="HIAT:w" s="T353">ü͡örü</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1138" n="HIAT:w" s="T354">barɨtɨn</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1141" n="HIAT:w" s="T355">egelen</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1144" n="HIAT:w" s="T356">keli͡e</ts>
                  <nts id="Seg_1145" n="HIAT:ip">.</nts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T365" id="Seg_1147" n="sc" s="T358">
               <ts e="T365" id="Seg_1149" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1151" n="HIAT:w" s="T358">Oː</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1154" n="HIAT:w" s="T359">erijekti͡ege</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1157" n="HIAT:w" s="T360">di͡en</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1160" n="HIAT:w" s="T361">tögürüččü</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1163" n="HIAT:w" s="T362">biːr</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1166" n="HIAT:w" s="T363">hirge</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1169" n="HIAT:w" s="T364">turu͡oga</ts>
                  <nts id="Seg_1170" n="HIAT:ip">.</nts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T370" id="Seg_1172" n="sc" s="T366">
               <ts e="T370" id="Seg_1174" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1176" n="HIAT:w" s="T366">Bu</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1179" n="HIAT:w" s="T367">egel</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1182" n="HIAT:w" s="T368">di͡etekke</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1185" n="HIAT:w" s="T369">ke</ts>
                  <nts id="Seg_1186" n="HIAT:ip">.</nts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T382" id="Seg_1188" n="sc" s="T371">
               <ts e="T382" id="Seg_1190" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1192" n="HIAT:w" s="T371">De</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1195" n="HIAT:w" s="T372">ontubut</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1198" n="HIAT:w" s="T373">kimi</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1201" n="HIAT:w" s="T374">kimneːčči</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1204" n="HIAT:w" s="T375">ete</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1207" n="HIAT:w" s="T376">maːjdɨn</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1210" n="HIAT:w" s="T377">tabalarɨŋ</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1213" n="HIAT:w" s="T378">baːr</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1216" n="HIAT:w" s="T379">erdekterinen</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1219" n="HIAT:w" s="T380">ke</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1222" n="HIAT:w" s="T381">dʼi͡ege</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T397" id="Seg_1225" n="sc" s="T383">
               <ts e="T397" id="Seg_1227" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1229" n="HIAT:w" s="T383">Tugut</ts>
                  <nts id="Seg_1230" n="HIAT:ip">,</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1233" n="HIAT:w" s="T384">abɨlakaːn</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1236" n="HIAT:w" s="T385">du</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1239" n="HIAT:w" s="T386">tu͡ok</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1242" n="HIAT:w" s="T387">du</ts>
                  <nts id="Seg_1243" n="HIAT:ip">,</nts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1246" n="HIAT:w" s="T388">abɨlakaːn</ts>
                  <nts id="Seg_1247" n="HIAT:ip">,</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1250" n="HIAT:w" s="T389">tugut</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1253" n="HIAT:w" s="T390">eː</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1256" n="HIAT:w" s="T391">ulakan</ts>
                  <nts id="Seg_1257" n="HIAT:ip">,</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1260" n="HIAT:w" s="T392">tuguttan</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1263" n="HIAT:w" s="T393">ulakankaːn</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1266" n="HIAT:w" s="T394">dʼɨlɨnan</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1269" n="HIAT:w" s="T395">ere</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1272" n="HIAT:w" s="T396">ulakan</ts>
                  <nts id="Seg_1273" n="HIAT:ip">.</nts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T413" id="Seg_1275" n="sc" s="T398">
               <ts e="T413" id="Seg_1277" n="HIAT:u" s="T398">
                  <ts e="T399" id="Seg_1279" n="HIAT:w" s="T398">Ol</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1282" n="HIAT:w" s="T399">kimi</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1285" n="HIAT:w" s="T400">de</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1288" n="HIAT:w" s="T401">erije</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1291" n="HIAT:w" s="T402">hɨtɨ͡aga</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1294" n="HIAT:w" s="T403">bu͡o</ts>
                  <nts id="Seg_1295" n="HIAT:ip">,</nts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1298" n="HIAT:w" s="T404">muŋnu</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1301" n="HIAT:w" s="T405">hɨtaːččɨ</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1304" n="HIAT:w" s="T406">iti</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1307" n="HIAT:w" s="T407">tu͡ok</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1310" n="HIAT:w" s="T408">tugukkaːn</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1313" n="HIAT:w" s="T409">bagaraːččɨ</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1316" n="HIAT:w" s="T410">dürü</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1319" n="HIAT:w" s="T411">möŋö</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1322" n="HIAT:w" s="T412">möŋö</ts>
                  <nts id="Seg_1323" n="HIAT:ip">.</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T438" id="Seg_1325" n="sc" s="T414">
               <ts e="T422" id="Seg_1327" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1329" n="HIAT:w" s="T414">Hɨtɨ͡aga</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1332" n="HIAT:w" s="T415">huh</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1335" n="HIAT:w" s="T416">bu͡o</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1338" n="HIAT:w" s="T417">oːnnʼuːr</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1341" n="HIAT:w" s="T418">bu͡o</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1344" n="HIAT:w" s="T419">ginini</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1347" n="HIAT:w" s="T420">gɨtta</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1350" n="HIAT:w" s="T421">ke</ts>
                  <nts id="Seg_1351" n="HIAT:ip">.</nts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1354" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_1356" n="HIAT:w" s="T422">Ontuŋ</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1359" n="HIAT:w" s="T423">kajdak</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1362" n="HIAT:w" s="T424">da</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1365" n="HIAT:w" s="T425">bu͡olla</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1367" n="HIAT:ip">(</nts>
                  <nts id="Seg_1368" n="HIAT:ip">(</nts>
                  <ats e="T1150" id="Seg_1369" n="HIAT:non-pho" s="T426">…</ats>
                  <nts id="Seg_1370" n="HIAT:ip">)</nts>
                  <nts id="Seg_1371" n="HIAT:ip">)</nts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1374" n="HIAT:w" s="T1150">tabɨhas</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1377" n="HIAT:w" s="T427">tabɨsar</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1380" n="HIAT:w" s="T428">bu</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1383" n="HIAT:w" s="T429">kurduk</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1386" n="HIAT:w" s="T430">kajdak</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1389" n="HIAT:w" s="T431">gɨnɨj</ts>
                  <nts id="Seg_1390" n="HIAT:ip">.</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1393" n="HIAT:u" s="T432">
                  <nts id="Seg_1394" n="HIAT:ip">"</nts>
                  <ts e="T433" id="Seg_1396" n="HIAT:w" s="T432">Maːma</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1399" n="HIAT:w" s="T433">maːma</ts>
                  <nts id="Seg_1400" n="HIAT:ip">"</nts>
                  <nts id="Seg_1401" n="HIAT:ip">,</nts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1404" n="HIAT:w" s="T434">diːbin</ts>
                  <nts id="Seg_1405" n="HIAT:ip">,</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1407" n="HIAT:ip">"</nts>
                  <ts e="T436" id="Seg_1409" n="HIAT:w" s="T435">kör</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1412" n="HIAT:w" s="T436">kör</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1415" n="HIAT:w" s="T437">Djoŋgonu</ts>
                  <nts id="Seg_1416" n="HIAT:ip">.</nts>
                  <nts id="Seg_1417" n="HIAT:ip">"</nts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T442" id="Seg_1419" n="sc" s="T439">
               <ts e="T442" id="Seg_1421" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_1423" n="HIAT:w" s="T439">Čoko</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1426" n="HIAT:w" s="T440">Čoko</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1429" n="HIAT:w" s="T441">di͡eččibin</ts>
                  <nts id="Seg_1430" n="HIAT:ip">.</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T445" id="Seg_1432" n="sc" s="T443">
               <ts e="T445" id="Seg_1434" n="HIAT:u" s="T443">
                  <nts id="Seg_1435" n="HIAT:ip">"</nts>
                  <ts e="T444" id="Seg_1437" n="HIAT:w" s="T443">Čoko</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1440" n="HIAT:w" s="T444">kör</ts>
                  <nts id="Seg_1441" n="HIAT:ip">.</nts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T453" id="Seg_1443" n="sc" s="T446">
               <ts e="T453" id="Seg_1445" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1447" n="HIAT:w" s="T446">Kimi</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1450" n="HIAT:w" s="T447">hi͡eri</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1453" n="HIAT:w" s="T448">gɨnna</ts>
                  <nts id="Seg_1454" n="HIAT:ip">"</nts>
                  <nts id="Seg_1455" n="HIAT:ip">,</nts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1458" n="HIAT:w" s="T449">diːbin</ts>
                  <nts id="Seg_1459" n="HIAT:ip">,</nts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1461" n="HIAT:ip">"</nts>
                  <ts e="T451" id="Seg_1463" n="HIAT:w" s="T450">tugutu</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1466" n="HIAT:w" s="T451">hi͡eri</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1469" n="HIAT:w" s="T452">gɨnna</ts>
                  <nts id="Seg_1470" n="HIAT:ip">.</nts>
                  <nts id="Seg_1471" n="HIAT:ip">"</nts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T458" id="Seg_1473" n="sc" s="T454">
               <ts e="T458" id="Seg_1475" n="HIAT:u" s="T454">
                  <nts id="Seg_1476" n="HIAT:ip">"</nts>
                  <ts e="T455" id="Seg_1478" n="HIAT:w" s="T454">Oː</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1481" n="HIAT:w" s="T455">satana</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1484" n="HIAT:w" s="T456">ɨgɨr</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1487" n="HIAT:w" s="T457">ɨgɨr</ts>
                  <nts id="Seg_1488" n="HIAT:ip">.</nts>
                  <nts id="Seg_1489" n="HIAT:ip">"</nts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T464" id="Seg_1491" n="sc" s="T459">
               <ts e="T464" id="Seg_1493" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1495" n="HIAT:w" s="T459">Hɨlatta</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1498" n="HIAT:w" s="T460">bɨhɨlak</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1501" n="HIAT:w" s="T461">iti</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1504" n="HIAT:w" s="T462">tugutu</ts>
                  <nts id="Seg_1505" n="HIAT:ip">,</nts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1508" n="HIAT:w" s="T463">kuttaːta</ts>
                  <nts id="Seg_1509" n="HIAT:ip">.</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T473" id="Seg_1511" n="sc" s="T465">
               <ts e="T473" id="Seg_1513" n="HIAT:u" s="T465">
                  <nts id="Seg_1514" n="HIAT:ip">"</nts>
                  <ts e="T466" id="Seg_1516" n="HIAT:w" s="T465">Čoko</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1519" n="HIAT:w" s="T466">Čoko</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1522" n="HIAT:w" s="T467">Čoko</ts>
                  <nts id="Seg_1523" n="HIAT:ip">"</nts>
                  <nts id="Seg_1524" n="HIAT:ip">,</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1527" n="HIAT:w" s="T468">diːbin</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1530" n="HIAT:w" s="T469">tahaːra</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1533" n="HIAT:w" s="T470">taksan</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1536" n="HIAT:w" s="T471">baran</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1539" n="HIAT:w" s="T472">ke</ts>
                  <nts id="Seg_1540" n="HIAT:ip">.</nts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T480" id="Seg_1542" n="sc" s="T474">
               <ts e="T480" id="Seg_1544" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_1546" n="HIAT:w" s="T474">Oː</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1549" n="HIAT:w" s="T475">de</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1552" n="HIAT:w" s="T476">ontuŋ</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1555" n="HIAT:w" s="T477">kötüten</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1558" n="HIAT:w" s="T478">iheːktiːr</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1561" n="HIAT:w" s="T479">di͡en</ts>
                  <nts id="Seg_1562" n="HIAT:ip">.</nts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T488" id="Seg_1564" n="sc" s="T481">
               <ts e="T488" id="Seg_1566" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1568" n="HIAT:w" s="T481">De</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1570" n="HIAT:ip">(</nts>
                  <ts e="T483" id="Seg_1572" n="HIAT:w" s="T482">ɨstan-</ts>
                  <nts id="Seg_1573" n="HIAT:ip">)</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1576" n="HIAT:w" s="T483">de</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1579" n="HIAT:w" s="T484">hɨrajbar</ts>
                  <nts id="Seg_1580" n="HIAT:ip">,</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1583" n="HIAT:w" s="T485">min</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1586" n="HIAT:w" s="T486">taras</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1589" n="HIAT:w" s="T487">hahaha</ts>
                  <nts id="Seg_1590" n="HIAT:ip">.</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T493" id="Seg_1592" n="sc" s="T489">
               <ts e="T493" id="Seg_1594" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1596" n="HIAT:w" s="T489">Ittenne</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1599" n="HIAT:w" s="T490">taraja</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1602" n="HIAT:w" s="T491">hɨtaːktɨːbɨn</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1605" n="HIAT:w" s="T492">di͡en</ts>
                  <nts id="Seg_1606" n="HIAT:ip">.</nts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T500" id="Seg_1608" n="sc" s="T494">
               <ts e="T500" id="Seg_1610" n="HIAT:u" s="T494">
                  <nts id="Seg_1611" n="HIAT:ip">"</nts>
                  <ts e="T495" id="Seg_1613" n="HIAT:w" s="T494">Eː</ts>
                  <nts id="Seg_1614" n="HIAT:ip">"</nts>
                  <nts id="Seg_1615" n="HIAT:ip">,</nts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1618" n="HIAT:w" s="T495">diːbin</ts>
                  <nts id="Seg_1619" n="HIAT:ip">,</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1621" n="HIAT:ip">"</nts>
                  <ts e="T497" id="Seg_1623" n="HIAT:w" s="T496">maːma</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1626" n="HIAT:w" s="T497">ɨl</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1629" n="HIAT:w" s="T498">dʼa</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1632" n="HIAT:w" s="T499">bu͡o</ts>
                  <nts id="Seg_1633" n="HIAT:ip">.</nts>
                  <nts id="Seg_1634" n="HIAT:ip">"</nts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T508" id="Seg_1636" n="sc" s="T501">
               <ts e="T508" id="Seg_1638" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1640" n="HIAT:w" s="T501">Halɨːr</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1643" n="HIAT:w" s="T502">kanʼɨːr</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1645" n="HIAT:ip">(</nts>
                  <ts e="T504" id="Seg_1647" n="HIAT:w" s="T503">möŋ-</ts>
                  <nts id="Seg_1648" n="HIAT:ip">)</nts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1651" n="HIAT:w" s="T504">möŋtörör</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1654" n="HIAT:w" s="T505">bu͡o</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1657" n="HIAT:w" s="T506">kihini</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1660" n="HIAT:w" s="T507">ke</ts>
                  <nts id="Seg_1661" n="HIAT:ip">.</nts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T511" id="Seg_1663" n="sc" s="T509">
               <ts e="T511" id="Seg_1665" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_1667" n="HIAT:w" s="T509">Möŋü͡ögün</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1670" n="HIAT:w" s="T510">bagarar</ts>
                  <nts id="Seg_1671" n="HIAT:ip">.</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T516" id="Seg_1673" n="sc" s="T512">
               <ts e="T516" id="Seg_1675" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_1677" n="HIAT:w" s="T512">Minigin</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1680" n="HIAT:w" s="T513">hogostuː</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1683" n="HIAT:w" s="T514">hɨtaːktɨːr</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1686" n="HIAT:w" s="T515">di͡en</ts>
                  <nts id="Seg_1687" n="HIAT:ip">.</nts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T521" id="Seg_1689" n="sc" s="T517">
               <ts e="T521" id="Seg_1691" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1693" n="HIAT:w" s="T517">De</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1696" n="HIAT:w" s="T518">ol</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1699" n="HIAT:w" s="T519">maːmam</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1702" n="HIAT:w" s="T520">tagɨsta</ts>
                  <nts id="Seg_1703" n="HIAT:ip">.</nts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T525" id="Seg_1705" n="sc" s="T522">
               <ts e="T525" id="Seg_1707" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_1709" n="HIAT:w" s="T522">De</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1712" n="HIAT:w" s="T523">tu͡ok</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1715" n="HIAT:w" s="T524">bu͡olagɨn</ts>
                  <nts id="Seg_1716" n="HIAT:ip">.</nts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T537" id="Seg_1718" n="sc" s="T526">
               <ts e="T537" id="Seg_1720" n="HIAT:u" s="T526">
                  <nts id="Seg_1721" n="HIAT:ip">"</nts>
                  <ts e="T527" id="Seg_1723" n="HIAT:w" s="T526">Kör</ts>
                  <nts id="Seg_1724" n="HIAT:ip">"</nts>
                  <nts id="Seg_1725" n="HIAT:ip">,</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1728" n="HIAT:w" s="T527">diːbin</ts>
                  <nts id="Seg_1729" n="HIAT:ip">,</nts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1731" n="HIAT:ip">"</nts>
                  <ts e="T529" id="Seg_1733" n="HIAT:w" s="T528">minigin</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1736" n="HIAT:w" s="T529">kɨrbɨːr</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1739" n="HIAT:w" s="T530">du͡o</ts>
                  <nts id="Seg_1740" n="HIAT:ip">"</nts>
                  <nts id="Seg_1741" n="HIAT:ip">,</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1744" n="HIAT:w" s="T531">diːbin</ts>
                  <nts id="Seg_1745" n="HIAT:ip">,</nts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1747" n="HIAT:ip">"</nts>
                  <ts e="T533" id="Seg_1749" n="HIAT:w" s="T532">oːnnʼuːr</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1752" n="HIAT:w" s="T533">kihini</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1755" n="HIAT:w" s="T534">ke</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1758" n="HIAT:w" s="T535">araː</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1761" n="HIAT:w" s="T536">da</ts>
                  <nts id="Seg_1762" n="HIAT:ip">.</nts>
                  <nts id="Seg_1763" n="HIAT:ip">"</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T542" id="Seg_1765" n="sc" s="T538">
               <ts e="T542" id="Seg_1767" n="HIAT:u" s="T538">
                  <ts e="T539" id="Seg_1769" n="HIAT:w" s="T538">Čičigeren</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1772" n="HIAT:w" s="T539">ɨtaːn</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1775" n="HIAT:w" s="T540">mörüleːktiːbin</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1778" n="HIAT:w" s="T541">heː</ts>
                  <nts id="Seg_1779" n="HIAT:ip">.</nts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T545" id="Seg_1781" n="sc" s="T543">
               <ts e="T545" id="Seg_1783" n="HIAT:u" s="T543">
                  <nts id="Seg_1784" n="HIAT:ip">"</nts>
                  <ts e="T544" id="Seg_1786" n="HIAT:w" s="T543">Ɨtaː</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1789" n="HIAT:w" s="T544">ɨtaːma</ts>
                  <nts id="Seg_1790" n="HIAT:ip">.</nts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T551" id="Seg_1792" n="sc" s="T546">
               <ts e="T551" id="Seg_1794" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_1796" n="HIAT:w" s="T546">Bejeŋ</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1799" n="HIAT:w" s="T547">ɨgɨrbɨtɨŋ</ts>
                  <nts id="Seg_1800" n="HIAT:ip">"</nts>
                  <nts id="Seg_1801" n="HIAT:ip">,</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1804" n="HIAT:w" s="T548">diːr</ts>
                  <nts id="Seg_1805" n="HIAT:ip">,</nts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1807" n="HIAT:ip">"</nts>
                  <ts e="T550" id="Seg_1809" n="HIAT:w" s="T549">kihi</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1812" n="HIAT:w" s="T550">külü͡ök</ts>
                  <nts id="Seg_1813" n="HIAT:ip">.</nts>
                  <nts id="Seg_1814" n="HIAT:ip">"</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T558" id="Seg_1816" n="sc" s="T552">
               <ts e="T558" id="Seg_1818" n="HIAT:u" s="T552">
                  <ts e="T553" id="Seg_1820" n="HIAT:w" s="T552">Ontuŋ</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1823" n="HIAT:w" s="T553">emi͡e</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1826" n="HIAT:w" s="T554">baran</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1829" n="HIAT:w" s="T555">kaːlla</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1832" n="HIAT:w" s="T556">ol</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1835" n="HIAT:w" s="T557">tugukka</ts>
                  <nts id="Seg_1836" n="HIAT:ip">.</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T577" id="Seg_1838" n="sc" s="T559">
               <ts e="T577" id="Seg_1840" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_1842" n="HIAT:w" s="T559">De</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1845" n="HIAT:w" s="T560">bolše</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1848" n="HIAT:w" s="T561">kimneːbetegim</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1851" n="HIAT:w" s="T562">ol</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1854" n="HIAT:w" s="T563">bi͡ek</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1857" n="HIAT:w" s="T564">kün</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1860" n="HIAT:w" s="T565">eːje</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1863" n="HIAT:w" s="T566">oːnnʼoːčču</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1866" n="HIAT:w" s="T567">ol</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1869" n="HIAT:w" s="T568">tugutu</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1872" n="HIAT:w" s="T569">gɨtta</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1875" n="HIAT:w" s="T570">tu͡okka</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1878" n="HIAT:w" s="T571">bagarbɨt</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1881" n="HIAT:w" s="T572">imenno</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1884" n="HIAT:w" s="T573">iti</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1887" n="HIAT:w" s="T574">tugut</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1890" n="HIAT:w" s="T575">bu͡olu͡ogun</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1893" n="HIAT:w" s="T576">naːda</ts>
                  <nts id="Seg_1894" n="HIAT:ip">.</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T583" id="Seg_1896" n="sc" s="T578">
               <ts e="T583" id="Seg_1898" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_1900" n="HIAT:w" s="T578">De</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1903" n="HIAT:w" s="T579">onton</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1906" n="HIAT:w" s="T580">tahaːrabɨt</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1909" n="HIAT:w" s="T581">karaŋarda</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1912" n="HIAT:w" s="T582">bu͡o</ts>
                  <nts id="Seg_1913" n="HIAT:ip">.</nts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T589" id="Seg_1915" n="sc" s="T584">
               <ts e="T589" id="Seg_1917" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_1919" n="HIAT:w" s="T584">Bosku͡ojɨn</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1922" n="HIAT:w" s="T585">da</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1925" n="HIAT:w" s="T586">kimmit</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1928" n="HIAT:w" s="T587">ohokput</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_1931" n="HIAT:w" s="T588">ke</ts>
                  <nts id="Seg_1932" n="HIAT:ip">.</nts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T593" id="Seg_1934" n="sc" s="T590">
               <ts e="T593" id="Seg_1936" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_1938" n="HIAT:w" s="T590">Maːmam</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_1941" n="HIAT:w" s="T591">delbi</ts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_1944" n="HIAT:w" s="T592">kaːlaːbɨt</ts>
                  <nts id="Seg_1945" n="HIAT:ip">.</nts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T604" id="Seg_1947" n="sc" s="T594">
               <ts e="T604" id="Seg_1949" n="HIAT:u" s="T594">
                  <ts e="T595" id="Seg_1951" n="HIAT:w" s="T594">Ps</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_1954" n="HIAT:w" s="T595">ps</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_1957" n="HIAT:w" s="T596">ps</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_1960" n="HIAT:w" s="T597">kim</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1963" n="HIAT:w" s="T598">mas</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_1966" n="HIAT:w" s="T599">ke</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_1969" n="HIAT:w" s="T600">ubajara</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_1972" n="HIAT:w" s="T601">ke</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_1975" n="HIAT:w" s="T602">kɨtarčɨ</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_1978" n="HIAT:w" s="T603">ohopput</ts>
                  <nts id="Seg_1979" n="HIAT:ip">.</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T611" id="Seg_1981" n="sc" s="T605">
               <ts e="T611" id="Seg_1983" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_1985" n="HIAT:w" s="T605">Čajniktar</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_1988" n="HIAT:w" s="T606">kötüten</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_1991" n="HIAT:w" s="T607">voːpsetɨn</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_1994" n="HIAT:w" s="T608">telibireːktiːller</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_1997" n="HIAT:w" s="T609">di͡en</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2000" n="HIAT:w" s="T610">kappaktara</ts>
                  <nts id="Seg_2001" n="HIAT:ip">.</nts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T618" id="Seg_2003" n="sc" s="T612">
               <ts e="T618" id="Seg_2005" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2007" n="HIAT:w" s="T612">Kü͡öspüt</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2010" n="HIAT:w" s="T613">laglɨ͡a</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2013" n="HIAT:w" s="T614">turaːktɨːr</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2016" n="HIAT:w" s="T615">di͡en</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2019" n="HIAT:w" s="T616">laglɨ͡a</ts>
                  <nts id="Seg_2020" n="HIAT:ip">,</nts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2023" n="HIAT:w" s="T617">voːpse</ts>
                  <nts id="Seg_2024" n="HIAT:ip">.</nts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T625" id="Seg_2026" n="sc" s="T619">
               <ts e="T625" id="Seg_2028" n="HIAT:u" s="T619">
                  <nts id="Seg_2029" n="HIAT:ip">(</nts>
                  <ts e="T620" id="Seg_2031" n="HIAT:w" s="T619">Itij-</ts>
                  <nts id="Seg_2032" n="HIAT:ip">)</nts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2035" n="HIAT:w" s="T620">aːn</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2038" n="HIAT:w" s="T621">tünnügü</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2041" n="HIAT:w" s="T622">arɨjda</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2044" n="HIAT:w" s="T623">ü͡ölehe</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2047" n="HIAT:w" s="T624">arɨjda</ts>
                  <nts id="Seg_2048" n="HIAT:ip">.</nts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T636" id="Seg_2050" n="sc" s="T626">
               <ts e="T636" id="Seg_2052" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2054" n="HIAT:w" s="T626">Ü͡öles</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2057" n="HIAT:w" s="T627">bu͡olaːččɨ</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2060" n="HIAT:w" s="T628">kim</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2063" n="HIAT:w" s="T629">bü͡öleːččiler</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2066" n="HIAT:w" s="T630">nu</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2069" n="HIAT:w" s="T631">štobɨ</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2072" n="HIAT:w" s="T632">veter</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2075" n="HIAT:w" s="T633">tɨ͡al</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2078" n="HIAT:w" s="T634">kiːri͡egin</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2081" n="HIAT:w" s="T635">ke</ts>
                  <nts id="Seg_2082" n="HIAT:ip">.</nts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T641" id="Seg_2084" n="sc" s="T637">
               <ts e="T641" id="Seg_2086" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2088" n="HIAT:w" s="T637">De</ts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2091" n="HIAT:w" s="T638">ol</ts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2094" n="HIAT:w" s="T639">arɨjda</ts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2097" n="HIAT:w" s="T640">heː</ts>
                  <nts id="Seg_2098" n="HIAT:ip">.</nts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T650" id="Seg_2100" n="sc" s="T642">
               <ts e="T650" id="Seg_2102" n="HIAT:u" s="T642">
                  <ts e="T643" id="Seg_2104" n="HIAT:w" s="T642">Min</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2107" n="HIAT:w" s="T643">du</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2110" n="HIAT:w" s="T644">hürdeːk</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2113" n="HIAT:w" s="T645">bagaraːččɨbɨn</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2116" n="HIAT:w" s="T646">tünnükke</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2119" n="HIAT:w" s="T647">tünnükke</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2122" n="HIAT:w" s="T648">körü͡öppün</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2125" n="HIAT:w" s="T649">ke</ts>
                  <nts id="Seg_2126" n="HIAT:ip">.</nts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T654" id="Seg_2128" n="sc" s="T651">
               <ts e="T654" id="Seg_2130" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2132" n="HIAT:w" s="T651">Mɨlaja</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2135" n="HIAT:w" s="T652">mɨlaja</ts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2138" n="HIAT:w" s="T653">oloroːččubun</ts>
                  <nts id="Seg_2139" n="HIAT:ip">.</nts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T661" id="Seg_2141" n="sc" s="T655">
               <ts e="T661" id="Seg_2143" n="HIAT:u" s="T655">
                  <ts e="T656" id="Seg_2145" n="HIAT:w" s="T655">Bosku͡ojin</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2148" n="HIAT:w" s="T656">da</ts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2151" n="HIAT:w" s="T657">i͡e</ts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2154" n="HIAT:w" s="T658">kim</ts>
                  <nts id="Seg_2155" n="HIAT:ip">,</nts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2158" n="HIAT:w" s="T659">ɨj</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2161" n="HIAT:w" s="T660">hahaha</ts>
                  <nts id="Seg_2162" n="HIAT:ip">.</nts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T666" id="Seg_2164" n="sc" s="T662">
               <ts e="T666" id="Seg_2166" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2168" n="HIAT:w" s="T662">Ɨj</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2171" n="HIAT:w" s="T663">erejkeːn</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2174" n="HIAT:w" s="T664">ebite</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2177" n="HIAT:w" s="T665">eh</ts>
                  <nts id="Seg_2178" n="HIAT:ip">.</nts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T672" id="Seg_2180" n="sc" s="T667">
               <ts e="T672" id="Seg_2182" n="HIAT:u" s="T667">
                  <ts e="T668" id="Seg_2184" n="HIAT:w" s="T667">Ɨjbɨt</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2187" n="HIAT:w" s="T668">bosku͡oj</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2190" n="HIAT:w" s="T669">bagajɨ</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2193" n="HIAT:w" s="T670">no</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2196" n="HIAT:w" s="T671">kap-karaŋa</ts>
                  <nts id="Seg_2197" n="HIAT:ip">.</nts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T680" id="Seg_2199" n="sc" s="T673">
               <ts e="T680" id="Seg_2201" n="HIAT:u" s="T673">
                  <ts e="T674" id="Seg_2203" n="HIAT:w" s="T673">Tu͡oktar</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2206" n="HIAT:w" s="T674">kötöːččüler</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2209" n="HIAT:w" s="T675">ebit</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2212" n="HIAT:w" s="T676">min</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2215" n="HIAT:w" s="T677">že</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2218" n="HIAT:w" s="T678">bilbet</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2221" n="HIAT:w" s="T679">etim</ts>
                  <nts id="Seg_2222" n="HIAT:ip">.</nts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T687" id="Seg_2224" n="sc" s="T681">
               <ts e="T687" id="Seg_2226" n="HIAT:u" s="T681">
                  <ts e="T682" id="Seg_2228" n="HIAT:w" s="T681">Leŋkej</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2231" n="HIAT:w" s="T682">kördük</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2234" n="HIAT:w" s="T683">leŋkejdar</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2237" n="HIAT:w" s="T684">di͡en</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2240" n="HIAT:w" s="T685">hu͡oga</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2243" n="HIAT:w" s="T686">küččügüjdar</ts>
                  <nts id="Seg_2244" n="HIAT:ip">.</nts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T696" id="Seg_2246" n="sc" s="T688">
               <ts e="T696" id="Seg_2248" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2250" n="HIAT:w" s="T688">Menʼiːte</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2253" n="HIAT:w" s="T689">hu͡oktar</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2256" n="HIAT:w" s="T690">di͡eččibin</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2259" n="HIAT:w" s="T691">menʼiːte</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2262" n="HIAT:w" s="T692">hu͡oktar</ts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2265" n="HIAT:w" s="T693">hɨldʼallar</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2268" n="HIAT:w" s="T694">maːma</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2271" n="HIAT:w" s="T695">kör</ts>
                  <nts id="Seg_2272" n="HIAT:ip">.</nts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T707" id="Seg_2274" n="sc" s="T697">
               <ts e="T707" id="Seg_2276" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_2278" n="HIAT:w" s="T697">Okazɨvaetsa</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2281" n="HIAT:w" s="T698">iti</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2284" n="HIAT:w" s="T699">kim</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2287" n="HIAT:w" s="T700">eh</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2290" n="HIAT:w" s="T701">bu</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2293" n="HIAT:w" s="T702">čok</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2296" n="HIAT:w" s="T703">kötör</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2299" n="HIAT:w" s="T704">bu͡o</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2302" n="HIAT:w" s="T705">trubatan</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2305" n="HIAT:w" s="T706">ke</ts>
                  <nts id="Seg_2306" n="HIAT:ip">.</nts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T714" id="Seg_2308" n="sc" s="T708">
               <ts e="T714" id="Seg_2310" n="HIAT:u" s="T708">
                  <ts e="T709" id="Seg_2312" n="HIAT:w" s="T708">Onuga</ts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2315" n="HIAT:w" s="T709">möŋsöllör</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2318" n="HIAT:w" s="T710">ebit</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2321" n="HIAT:w" s="T711">itileriŋ</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2324" n="HIAT:w" s="T712">interes</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2327" n="HIAT:w" s="T713">bagajɨ</ts>
                  <nts id="Seg_2328" n="HIAT:ip">.</nts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T723" id="Seg_2330" n="sc" s="T715">
               <ts e="T723" id="Seg_2332" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2334" n="HIAT:w" s="T715">Onton</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2337" n="HIAT:w" s="T716">töhö</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2340" n="HIAT:w" s="T717">eme</ts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2343" n="HIAT:w" s="T718">maːmam</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2345" n="HIAT:ip">(</nts>
                  <ts e="T720" id="Seg_2347" n="HIAT:w" s="T719">iːsten-</ts>
                  <nts id="Seg_2348" n="HIAT:ip">)</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2351" n="HIAT:w" s="T720">oloror</ts>
                  <nts id="Seg_2352" n="HIAT:ip">,</nts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2354" n="HIAT:ip">(</nts>
                  <ts e="T722" id="Seg_2356" n="HIAT:w" s="T721">iːsten-</ts>
                  <nts id="Seg_2357" n="HIAT:ip">)</nts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2360" n="HIAT:w" s="T722">oloror</ts>
                  <nts id="Seg_2361" n="HIAT:ip">.</nts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T731" id="Seg_2363" n="sc" s="T724">
               <ts e="T731" id="Seg_2365" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_2367" n="HIAT:w" s="T724">Orguːjakaːn</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2370" n="HIAT:w" s="T725">orguːjakaːn</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2373" n="HIAT:w" s="T726">diː</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2376" n="HIAT:w" s="T727">diː</ts>
                  <nts id="Seg_2377" n="HIAT:ip">,</nts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2380" n="HIAT:w" s="T728">ɨllaːn</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2383" n="HIAT:w" s="T729">oloror</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2386" n="HIAT:w" s="T730">orguːjak</ts>
                  <nts id="Seg_2387" n="HIAT:ip">.</nts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T734" id="Seg_2389" n="sc" s="T732">
               <ts e="T734" id="Seg_2391" n="HIAT:u" s="T732">
                  <ts e="T733" id="Seg_2393" n="HIAT:w" s="T732">Djakubɨt</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2396" n="HIAT:w" s="T733">hɨtar</ts>
                  <nts id="Seg_2397" n="HIAT:ip">.</nts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T738" id="Seg_2399" n="sc" s="T735">
               <ts e="T738" id="Seg_2401" n="HIAT:u" s="T735">
                  <ts e="T736" id="Seg_2403" n="HIAT:w" s="T735">Nʼik</ts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2406" n="HIAT:w" s="T736">da</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2409" n="HIAT:w" s="T737">gɨmmat</ts>
                  <nts id="Seg_2410" n="HIAT:ip">.</nts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T743" id="Seg_2412" n="sc" s="T739">
               <ts e="T743" id="Seg_2414" n="HIAT:u" s="T739">
                  <ts e="T740" id="Seg_2416" n="HIAT:w" s="T739">Hɨlajbɨt</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2419" n="HIAT:w" s="T740">bɨhɨlak</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2422" n="HIAT:w" s="T741">tugutun</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2425" n="HIAT:w" s="T742">gɨtta</ts>
                  <nts id="Seg_2426" n="HIAT:ip">.</nts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T750" id="Seg_2428" n="sc" s="T744">
               <ts e="T750" id="Seg_2430" n="HIAT:u" s="T744">
                  <ts e="T745" id="Seg_2432" n="HIAT:w" s="T744">Onton</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2435" n="HIAT:w" s="T745">kim</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2438" n="HIAT:w" s="T746">vremja</ts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2441" n="HIAT:w" s="T747">vremja</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2444" n="HIAT:w" s="T748">barda</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2447" n="HIAT:w" s="T749">bu͡o</ts>
                  <nts id="Seg_2448" n="HIAT:ip">.</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T762" id="Seg_2450" n="sc" s="T751">
               <ts e="T762" id="Seg_2452" n="HIAT:u" s="T751">
                  <ts e="T752" id="Seg_2454" n="HIAT:w" s="T751">Poka</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2457" n="HIAT:w" s="T752">ol</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2460" n="HIAT:w" s="T753">tu͡ok</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2463" n="HIAT:w" s="T754">itte</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2466" n="HIAT:w" s="T755">bütte</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2469" n="HIAT:w" s="T756">min</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2472" n="HIAT:w" s="T757">ješo</ts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2475" n="HIAT:w" s="T758">teːtem</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2478" n="HIAT:w" s="T759">hu͡okkaːnɨnan</ts>
                  <nts id="Seg_2479" n="HIAT:ip">,</nts>
                  <nts id="Seg_2480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2482" n="HIAT:w" s="T760">radiona</ts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2485" n="HIAT:w" s="T761">vklučajdɨnabɨn</ts>
                  <nts id="Seg_2486" n="HIAT:ip">.</nts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T766" id="Seg_2488" n="sc" s="T763">
               <ts e="T766" id="Seg_2490" n="HIAT:u" s="T763">
                  <ts e="T764" id="Seg_2492" n="HIAT:w" s="T763">Heee</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2495" n="HIAT:w" s="T764">gaːgɨnataːktɨːbɨn</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2498" n="HIAT:w" s="T765">de</ts>
                  <nts id="Seg_2499" n="HIAT:ip">.</nts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T772" id="Seg_2501" n="sc" s="T767">
               <ts e="T772" id="Seg_2503" n="HIAT:u" s="T767">
                  <nts id="Seg_2504" n="HIAT:ip">"</nts>
                  <ts e="T768" id="Seg_2506" n="HIAT:w" s="T767">Eː</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2509" n="HIAT:w" s="T768">orguːj</ts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2512" n="HIAT:w" s="T769">gɨn</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2515" n="HIAT:w" s="T770">beji</ts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2518" n="HIAT:w" s="T771">talɨgɨratɨma</ts>
                  <nts id="Seg_2519" n="HIAT:ip">.</nts>
                  <nts id="Seg_2520" n="HIAT:ip">"</nts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T777" id="Seg_2522" n="sc" s="T773">
               <ts e="T777" id="Seg_2524" n="HIAT:u" s="T773">
                  <ts e="T774" id="Seg_2526" n="HIAT:w" s="T773">Ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2529" n="HIAT:w" s="T774">ihilliː</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2532" n="HIAT:w" s="T775">hɨtɨ͡am</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2535" n="HIAT:w" s="T776">bu͡o</ts>
                  <nts id="Seg_2536" n="HIAT:ip">.</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T786" id="Seg_2538" n="sc" s="T778">
               <ts e="T786" id="Seg_2540" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_2542" n="HIAT:w" s="T778">Henʼooo</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2545" n="HIAT:w" s="T779">kimneri</ts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2548" n="HIAT:w" s="T780">di͡e</ts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2551" n="HIAT:w" s="T781">ihilliːbin</ts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2554" n="HIAT:w" s="T782">okazɨvaetsa</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2557" n="HIAT:w" s="T783">Kitajetsardarlar</ts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2560" n="HIAT:w" s="T784">tu͡oktar</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2563" n="HIAT:w" s="T785">du</ts>
                  <nts id="Seg_2564" n="HIAT:ip">.</nts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T803" id="Seg_2566" n="sc" s="T787">
               <ts e="T796" id="Seg_2568" n="HIAT:u" s="T787">
                  <ts e="T788" id="Seg_2570" n="HIAT:w" s="T787">Oloru</ts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2573" n="HIAT:w" s="T788">ihillen</ts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2576" n="HIAT:w" s="T789">bu͡ola</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2579" n="HIAT:w" s="T790">hɨtaːktɨːbɨn</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2582" n="HIAT:w" s="T791">di͡en</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2585" n="HIAT:w" s="T792">oččogo</ts>
                  <nts id="Seg_2586" n="HIAT:ip">,</nts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2589" n="HIAT:w" s="T793">beseleː</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2592" n="HIAT:w" s="T794">bagajɨ</ts>
                  <nts id="Seg_2593" n="HIAT:ip">,</nts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2596" n="HIAT:w" s="T795">bilebin</ts>
                  <nts id="Seg_2597" n="HIAT:ip">.</nts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T803" id="Seg_2600" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_2602" n="HIAT:w" s="T796">Onton</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2605" n="HIAT:w" s="T797">hi͡ese</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2608" n="HIAT:w" s="T798">nʼuːččalar</ts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2611" n="HIAT:w" s="T799">bullum</ts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2614" n="HIAT:w" s="T800">onton</ts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2617" n="HIAT:w" s="T801">büten</ts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2620" n="HIAT:w" s="T802">kaːlbɨt</ts>
                  <nts id="Seg_2621" n="HIAT:ip">.</nts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T811" id="Seg_2623" n="sc" s="T804">
               <ts e="T811" id="Seg_2625" n="HIAT:u" s="T804">
                  <nts id="Seg_2626" n="HIAT:ip">"</nts>
                  <ts e="T805" id="Seg_2628" n="HIAT:w" s="T804">De</ts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2631" n="HIAT:w" s="T805">büteren</ts>
                  <nts id="Seg_2632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2634" n="HIAT:w" s="T806">keːhi͡ekke</ts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2637" n="HIAT:w" s="T807">büter</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2640" n="HIAT:w" s="T808">teːteŋ</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2643" n="HIAT:w" s="T809">keli͡e</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2646" n="HIAT:w" s="T810">heː</ts>
                  <nts id="Seg_2647" n="HIAT:ip">.</nts>
                  <nts id="Seg_2648" n="HIAT:ip">"</nts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T820" id="Seg_2650" n="sc" s="T812">
               <ts e="T820" id="Seg_2652" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_2654" n="HIAT:w" s="T812">Kümmüt</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2657" n="HIAT:w" s="T813">ɨjbɨt</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2660" n="HIAT:w" s="T814">ɨraːtta</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2663" n="HIAT:w" s="T815">bu͡o</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2666" n="HIAT:w" s="T816">ü͡öhe</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2669" n="HIAT:w" s="T817">bu͡olla</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2672" n="HIAT:w" s="T818">ama</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2675" n="HIAT:w" s="T819">bosku͡oj</ts>
                  <nts id="Seg_2676" n="HIAT:ip">.</nts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T825" id="Seg_2678" n="sc" s="T821">
               <ts e="T825" id="Seg_2680" n="HIAT:u" s="T821">
                  <ts e="T822" id="Seg_2682" n="HIAT:w" s="T821">Ama</ts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2685" n="HIAT:w" s="T822">hɨrdɨk</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2688" n="HIAT:w" s="T823">künüs</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2691" n="HIAT:w" s="T824">kördük</ts>
                  <nts id="Seg_2692" n="HIAT:ip">.</nts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T837" id="Seg_2694" n="sc" s="T826">
               <ts e="T837" id="Seg_2696" n="HIAT:u" s="T826">
                  <ts e="T827" id="Seg_2698" n="HIAT:w" s="T826">Oː</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_2701" n="HIAT:w" s="T827">tabalarbɨt</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_2704" n="HIAT:w" s="T828">voːpse</ts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2707" n="HIAT:w" s="T829">ebe</ts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_2710" n="HIAT:w" s="T830">üstün</ts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_2713" n="HIAT:w" s="T831">kötüte</ts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_2716" n="HIAT:w" s="T832">hɨldʼaːktɨːllar</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2719" n="HIAT:w" s="T833">de</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2722" n="HIAT:w" s="T834">ama</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_2725" n="HIAT:w" s="T835">bosku͡ojin</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_2728" n="HIAT:w" s="T836">da</ts>
                  <nts id="Seg_2729" n="HIAT:ip">.</nts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T842" id="Seg_2731" n="sc" s="T838">
               <ts e="T842" id="Seg_2733" n="HIAT:u" s="T838">
                  <ts e="T839" id="Seg_2735" n="HIAT:w" s="T838">Kim</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_2738" n="HIAT:w" s="T839">buru͡olara</ts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_2741" n="HIAT:w" s="T840">aš</ts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_2744" n="HIAT:w" s="T841">kötör</ts>
                  <nts id="Seg_2745" n="HIAT:ip">.</nts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T855" id="Seg_2747" n="sc" s="T843">
               <ts e="T855" id="Seg_2749" n="HIAT:u" s="T843">
                  <ts e="T844" id="Seg_2751" n="HIAT:w" s="T843">Oj</ts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_2754" n="HIAT:w" s="T844">ama</ts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_2757" n="HIAT:w" s="T845">bosku͡oj</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_2760" n="HIAT:w" s="T846">kim</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2763" n="HIAT:w" s="T847">ke</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2766" n="HIAT:w" s="T848">kaːrɨŋ</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_2769" n="HIAT:w" s="T849">ke</ts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_2772" n="HIAT:w" s="T850">kimŋe</ts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_2775" n="HIAT:w" s="T851">lunaːtan</ts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2778" n="HIAT:w" s="T852">ama</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2781" n="HIAT:w" s="T853">kimniːr</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2784" n="HIAT:w" s="T854">gɨlbaŋnɨːr</ts>
                  <nts id="Seg_2785" n="HIAT:ip">.</nts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T866" id="Seg_2787" n="sc" s="T856">
               <ts e="T866" id="Seg_2789" n="HIAT:u" s="T856">
                  <ts e="T857" id="Seg_2791" n="HIAT:w" s="T856">Onton</ts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_2794" n="HIAT:w" s="T857">onton</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_2797" n="HIAT:w" s="T858">beːbe</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_2800" n="HIAT:w" s="T859">ɨppɨt</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_2803" n="HIAT:w" s="T860">mm</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_2806" n="HIAT:w" s="T861">mm</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_2809" n="HIAT:w" s="T862">gɨnar</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_2812" n="HIAT:w" s="T863">bu</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_2815" n="HIAT:w" s="T864">kimniːr</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2818" n="HIAT:w" s="T865">muŋurguːr</ts>
                  <nts id="Seg_2819" n="HIAT:ip">.</nts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T872" id="Seg_2821" n="sc" s="T867">
               <ts e="T872" id="Seg_2823" n="HIAT:u" s="T867">
                  <ts e="T868" id="Seg_2825" n="HIAT:w" s="T867">Beː</ts>
                  <nts id="Seg_2826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_2828" n="HIAT:w" s="T868">ču͡oraːn</ts>
                  <nts id="Seg_2829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_2831" n="HIAT:w" s="T869">tɨ͡ahaːta</ts>
                  <nts id="Seg_2832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_2834" n="HIAT:w" s="T870">araj</ts>
                  <nts id="Seg_2835" n="HIAT:ip">,</nts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_2838" n="HIAT:w" s="T871">küpüleːn</ts>
                  <nts id="Seg_2839" n="HIAT:ip">.</nts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T880" id="Seg_2841" n="sc" s="T873">
               <ts e="T880" id="Seg_2843" n="HIAT:u" s="T873">
                  <ts e="T874" id="Seg_2845" n="HIAT:w" s="T873">Teleŋ</ts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_2848" n="HIAT:w" s="T874">teleŋ</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_2851" n="HIAT:w" s="T875">teleŋ</ts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_2854" n="HIAT:w" s="T876">teːtebit</ts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_2857" n="HIAT:w" s="T877">iher</ts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_2860" n="HIAT:w" s="T878">teːtebit</ts>
                  <nts id="Seg_2861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_2863" n="HIAT:w" s="T879">heː</ts>
                  <nts id="Seg_2864" n="HIAT:ip">.</nts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T884" id="Seg_2866" n="sc" s="T881">
               <ts e="T884" id="Seg_2868" n="HIAT:u" s="T881">
                  <ts e="T882" id="Seg_2870" n="HIAT:w" s="T881">De</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_2873" n="HIAT:w" s="T882">ol</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_2876" n="HIAT:w" s="T883">kelle</ts>
                  <nts id="Seg_2877" n="HIAT:ip">.</nts>
                  <nts id="Seg_2878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T902" id="Seg_2879" n="sc" s="T885">
               <ts e="T902" id="Seg_2881" n="HIAT:u" s="T885">
                  <ts e="T886" id="Seg_2883" n="HIAT:w" s="T885">Min</ts>
                  <nts id="Seg_2884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_2886" n="HIAT:w" s="T886">du</ts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_2889" n="HIAT:w" s="T887">bilbetegim</ts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_2892" n="HIAT:w" s="T888">bu͡o</ts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_2895" n="HIAT:w" s="T889">mastana</ts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_2898" n="HIAT:w" s="T890">barbɨt</ts>
                  <nts id="Seg_2899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_2901" n="HIAT:w" s="T891">diːbin</ts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_2904" n="HIAT:w" s="T892">bu͡o</ts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_2907" n="HIAT:w" s="T893">okazɨvaetsa</ts>
                  <nts id="Seg_2908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_2910" n="HIAT:w" s="T894">giniŋ</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_2913" n="HIAT:w" s="T895">bu͡o</ts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_2916" n="HIAT:w" s="T896">Dudinka</ts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_2919" n="HIAT:w" s="T897">di͡ek</ts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_2922" n="HIAT:w" s="T898">hɨldʼabɨt</ts>
                  <nts id="Seg_2923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_2925" n="HIAT:w" s="T899">i</ts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_2928" n="HIAT:w" s="T900">Dujunaː</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_2931" n="HIAT:w" s="T901">di͡ek</ts>
                  <nts id="Seg_2932" n="HIAT:ip">.</nts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T906" id="Seg_2934" n="sc" s="T903">
               <ts e="T906" id="Seg_2936" n="HIAT:u" s="T903">
                  <ts e="T904" id="Seg_2938" n="HIAT:w" s="T903">De</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_2941" n="HIAT:w" s="T904">ol</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_2944" n="HIAT:w" s="T905">kelle</ts>
                  <nts id="Seg_2945" n="HIAT:ip">.</nts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T911" id="Seg_2947" n="sc" s="T907">
               <ts e="T911" id="Seg_2949" n="HIAT:u" s="T907">
                  <ts e="T908" id="Seg_2951" n="HIAT:w" s="T907">Uže</ts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_2954" n="HIAT:w" s="T908">karaŋarbɨtɨn</ts>
                  <nts id="Seg_2955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_2957" n="HIAT:w" s="T909">kenne</ts>
                  <nts id="Seg_2958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_2960" n="HIAT:w" s="T910">ke</ts>
                  <nts id="Seg_2961" n="HIAT:ip">.</nts>
                  <nts id="Seg_2962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T915" id="Seg_2963" n="sc" s="T912">
               <ts e="T915" id="Seg_2965" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_2967" n="HIAT:w" s="T912">Oː</ts>
                  <nts id="Seg_2968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_2970" n="HIAT:w" s="T913">teːtem</ts>
                  <nts id="Seg_2971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_2973" n="HIAT:w" s="T914">kelle</ts>
                  <nts id="Seg_2974" n="HIAT:ip">.</nts>
                  <nts id="Seg_2975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T926" id="Seg_2976" n="sc" s="T916">
               <ts e="T923" id="Seg_2978" n="HIAT:u" s="T916">
                  <ts e="T917" id="Seg_2980" n="HIAT:w" s="T916">Oː</ts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_2983" n="HIAT:w" s="T917">maːmam</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_2986" n="HIAT:w" s="T918">kömölöhö</ts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_2989" n="HIAT:w" s="T919">barda</ts>
                  <nts id="Seg_2990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_2992" n="HIAT:w" s="T920">hü͡örde</ts>
                  <nts id="Seg_2993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_2995" n="HIAT:w" s="T921">kanʼaːta</ts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_2998" n="HIAT:w" s="T922">küpüleːnten</ts>
                  <nts id="Seg_2999" n="HIAT:ip">.</nts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T926" id="Seg_3002" n="HIAT:u" s="T923">
                  <ts e="T924" id="Seg_3004" n="HIAT:w" s="T923">Tabalarɨn</ts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3007" n="HIAT:w" s="T924">ɨːttɨlar</ts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3010" n="HIAT:w" s="T925">kanʼaːtɨlar</ts>
                  <nts id="Seg_3011" n="HIAT:ip">.</nts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T931" id="Seg_3013" n="sc" s="T927">
               <ts e="T931" id="Seg_3015" n="HIAT:u" s="T927">
                  <ts e="T928" id="Seg_3017" n="HIAT:w" s="T927">Oː</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3020" n="HIAT:w" s="T928">teːtem</ts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3023" n="HIAT:w" s="T929">mesoktardaːk</ts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3026" n="HIAT:w" s="T930">kelle</ts>
                  <nts id="Seg_3027" n="HIAT:ip">.</nts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T934" id="Seg_3029" n="sc" s="T932">
               <ts e="T934" id="Seg_3031" n="HIAT:u" s="T932">
                  <ts e="T933" id="Seg_3033" n="HIAT:w" s="T932">Küːllerin</ts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3036" n="HIAT:w" s="T933">kiːllerde</ts>
                  <nts id="Seg_3037" n="HIAT:ip">.</nts>
                  <nts id="Seg_3038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T942" id="Seg_3039" n="sc" s="T935">
               <ts e="T937" id="Seg_3041" n="HIAT:u" s="T935">
                  <ts e="T936" id="Seg_3043" n="HIAT:w" s="T935">De</ts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3046" n="HIAT:w" s="T936">kostoːtoloːto</ts>
                  <nts id="Seg_3047" n="HIAT:ip">.</nts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T942" id="Seg_3050" n="HIAT:u" s="T937">
                  <ts e="T938" id="Seg_3052" n="HIAT:w" s="T937">Oː</ts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3055" n="HIAT:w" s="T938">ča</ts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3058" n="HIAT:w" s="T939">kempi͡et</ts>
                  <nts id="Seg_3059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3061" n="HIAT:w" s="T940">ülegerin</ts>
                  <nts id="Seg_3062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3064" n="HIAT:w" s="T941">egelbit</ts>
                  <nts id="Seg_3065" n="HIAT:ip">.</nts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T948" id="Seg_3067" n="sc" s="T943">
               <ts e="T948" id="Seg_3069" n="HIAT:u" s="T943">
                  <ts e="T944" id="Seg_3071" n="HIAT:w" s="T943">Onton</ts>
                  <nts id="Seg_3072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3074" n="HIAT:w" s="T944">bosku͡oj</ts>
                  <nts id="Seg_3075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3077" n="HIAT:w" s="T945">bagajɨ</ts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3080" n="HIAT:w" s="T946">bi͡ek</ts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3083" n="HIAT:w" s="T947">öjdüːbün</ts>
                  <nts id="Seg_3084" n="HIAT:ip">.</nts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T957" id="Seg_3086" n="sc" s="T949">
               <ts e="T957" id="Seg_3088" n="HIAT:u" s="T949">
                  <ts e="T950" id="Seg_3090" n="HIAT:w" s="T949">Kim</ts>
                  <nts id="Seg_3091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3093" n="HIAT:w" s="T950">čaːstar</ts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3096" n="HIAT:w" s="T951">du͡o</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3099" n="HIAT:w" s="T952">bosku͡oj</ts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3102" n="HIAT:w" s="T953">bagajɨ</ts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3105" n="HIAT:w" s="T954">kim</ts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3108" n="HIAT:w" s="T955">hirkilinnen</ts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3111" n="HIAT:w" s="T956">oŋohullubut</ts>
                  <nts id="Seg_3112" n="HIAT:ip">.</nts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T968" id="Seg_3114" n="sc" s="T958">
               <ts e="T968" id="Seg_3116" n="HIAT:u" s="T958">
                  <ts e="T959" id="Seg_3118" n="HIAT:w" s="T958">Ontuŋ</ts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3121" n="HIAT:w" s="T959">ješo</ts>
                  <nts id="Seg_3122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3124" n="HIAT:w" s="T960">knopkalardaːk</ts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3127" n="HIAT:w" s="T961">tu͡ok</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3130" n="HIAT:w" s="T962">ere</ts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3133" n="HIAT:w" s="T963">ama</ts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3136" n="HIAT:w" s="T964">bosku͡oj</ts>
                  <nts id="Seg_3137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3139" n="HIAT:w" s="T965">oduːrgaːktɨːbɨn</ts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3142" n="HIAT:w" s="T966">ama</ts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3145" n="HIAT:w" s="T967">bosku͡oj</ts>
                  <nts id="Seg_3146" n="HIAT:ip">.</nts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T974" id="Seg_3148" n="sc" s="T969">
               <ts e="T974" id="Seg_3150" n="HIAT:u" s="T969">
                  <nts id="Seg_3151" n="HIAT:ip">"</nts>
                  <ts e="T970" id="Seg_3153" n="HIAT:w" s="T969">De</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3156" n="HIAT:w" s="T970">tɨːtɨma</ts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3159" n="HIAT:w" s="T971">höp</ts>
                  <nts id="Seg_3160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_3162" n="HIAT:w" s="T972">bu͡olu͡o</ts>
                  <nts id="Seg_3163" n="HIAT:ip">"</nts>
                  <nts id="Seg_3164" n="HIAT:ip">,</nts>
                  <nts id="Seg_3165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3167" n="HIAT:w" s="T973">diːller</ts>
                  <nts id="Seg_3168" n="HIAT:ip">.</nts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T978" id="Seg_3170" n="sc" s="T975">
               <ts e="T978" id="Seg_3172" n="HIAT:u" s="T975">
                  <ts e="T976" id="Seg_3174" n="HIAT:w" s="T975">Tɨːttaraːččɨta</ts>
                  <nts id="Seg_3175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3177" n="HIAT:w" s="T976">hu͡oktar</ts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3180" n="HIAT:w" s="T977">bu͡o</ts>
                  <nts id="Seg_3181" n="HIAT:ip">.</nts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T982" id="Seg_3183" n="sc" s="T979">
               <ts e="T982" id="Seg_3185" n="HIAT:u" s="T979">
                  <nts id="Seg_3186" n="HIAT:ip">"</nts>
                  <ts e="T980" id="Seg_3188" n="HIAT:w" s="T979">Aldʼatɨ͡aŋ</ts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_3191" n="HIAT:w" s="T980">kaja</ts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3194" n="HIAT:w" s="T981">heː</ts>
                  <nts id="Seg_3195" n="HIAT:ip">"</nts>
                  <nts id="Seg_3196" n="HIAT:ip">.</nts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T991" id="Seg_3198" n="sc" s="T983">
               <ts e="T991" id="Seg_3200" n="HIAT:u" s="T983">
                  <ts e="T984" id="Seg_3202" n="HIAT:w" s="T983">De</ts>
                  <nts id="Seg_3203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3205" n="HIAT:w" s="T984">uːrdum</ts>
                  <nts id="Seg_3206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3208" n="HIAT:w" s="T985">hürdeːk</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3211" n="HIAT:w" s="T986">körü͡öppün</ts>
                  <nts id="Seg_3212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_3214" n="HIAT:w" s="T987">bagarabɨn</ts>
                  <nts id="Seg_3215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_3217" n="HIAT:w" s="T988">tɨːtalɨ͡appɨn</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3220" n="HIAT:w" s="T989">bagarabɨn</ts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3223" n="HIAT:w" s="T990">bu͡o</ts>
                  <nts id="Seg_3224" n="HIAT:ip">.</nts>
                  <nts id="Seg_3225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1000" id="Seg_3226" n="sc" s="T992">
               <ts e="T1000" id="Seg_3228" n="HIAT:u" s="T992">
                  <ts e="T993" id="Seg_3230" n="HIAT:w" s="T992">Onton</ts>
                  <nts id="Seg_3231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_3233" n="HIAT:w" s="T993">kim</ts>
                  <nts id="Seg_3234" n="HIAT:ip">,</nts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3237" n="HIAT:w" s="T994">kimi</ts>
                  <nts id="Seg_3238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3240" n="HIAT:w" s="T995">egelbite</ts>
                  <nts id="Seg_3241" n="HIAT:ip">,</nts>
                  <nts id="Seg_3242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_3244" n="HIAT:w" s="T996">mini͡eke</ts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_3247" n="HIAT:w" s="T997">podarok</ts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_3250" n="HIAT:w" s="T998">kompaskaːn</ts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_3253" n="HIAT:w" s="T999">kuččuguj</ts>
                  <nts id="Seg_3254" n="HIAT:ip">.</nts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1016" id="Seg_3256" n="sc" s="T1001">
               <ts e="T1016" id="Seg_3258" n="HIAT:u" s="T1001">
                  <ts e="T1002" id="Seg_3260" n="HIAT:w" s="T1001">Onu</ts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_3263" n="HIAT:w" s="T1002">bi͡ek</ts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_3266" n="HIAT:w" s="T1003">oːnnʼuː</ts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_3269" n="HIAT:w" s="T1004">hɨtaːččɨbɨn</ts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_3272" n="HIAT:w" s="T1005">araː</ts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_3275" n="HIAT:w" s="T1006">bosku͡ojɨn</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_3278" n="HIAT:w" s="T1007">du͡o</ts>
                  <nts id="Seg_3279" n="HIAT:ip">,</nts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_3282" n="HIAT:w" s="T1008">urut</ts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_3285" n="HIAT:w" s="T1009">pervɨj</ts>
                  <nts id="Seg_3286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_3288" n="HIAT:w" s="T1010">raz</ts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_3291" n="HIAT:w" s="T1011">egelbite</ts>
                  <nts id="Seg_3292" n="HIAT:ip">,</nts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_3295" n="HIAT:w" s="T1012">bu</ts>
                  <nts id="Seg_3296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_3298" n="HIAT:w" s="T1013">korduk</ts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_3301" n="HIAT:w" s="T1014">oːnnʼuːru</ts>
                  <nts id="Seg_3302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_3304" n="HIAT:w" s="T1015">ke</ts>
                  <nts id="Seg_3305" n="HIAT:ip">.</nts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1024" id="Seg_3307" n="sc" s="T1017">
               <ts e="T1024" id="Seg_3309" n="HIAT:u" s="T1017">
                  <ts e="T1018" id="Seg_3311" n="HIAT:w" s="T1017">Oː</ts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_3314" n="HIAT:w" s="T1018">min</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_3317" n="HIAT:w" s="T1019">muŋ</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_3320" n="HIAT:w" s="T1020">hu͡ok</ts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_3323" n="HIAT:w" s="T1021">voːpse</ts>
                  <nts id="Seg_3324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_3326" n="HIAT:w" s="T1022">bosku͡ojɨn</ts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_3329" n="HIAT:w" s="T1023">da</ts>
                  <nts id="Seg_3330" n="HIAT:ip">.</nts>
                  <nts id="Seg_3331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1029" id="Seg_3332" n="sc" s="T1025">
               <ts e="T1029" id="Seg_3334" n="HIAT:u" s="T1025">
                  <nts id="Seg_3335" n="HIAT:ip">"</nts>
                  <ts e="T1026" id="Seg_3337" n="HIAT:w" s="T1025">Oː</ts>
                  <nts id="Seg_3338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_3340" n="HIAT:w" s="T1026">eni͡eke</ts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_3343" n="HIAT:w" s="T1027">podarok</ts>
                  <nts id="Seg_3344" n="HIAT:ip">"</nts>
                  <nts id="Seg_3345" n="HIAT:ip">,</nts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_3348" n="HIAT:w" s="T1028">diːr</ts>
                  <nts id="Seg_3349" n="HIAT:ip">.</nts>
                  <nts id="Seg_3350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1038" id="Seg_3351" n="sc" s="T1030">
               <ts e="T1038" id="Seg_3353" n="HIAT:u" s="T1030">
                  <ts e="T1031" id="Seg_3355" n="HIAT:w" s="T1030">De</ts>
                  <nts id="Seg_3356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1032" id="Seg_3358" n="HIAT:w" s="T1031">ol</ts>
                  <nts id="Seg_3359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_3361" n="HIAT:w" s="T1032">kepseteller</ts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_3364" n="HIAT:w" s="T1033">eni</ts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_3367" n="HIAT:w" s="T1034">ke</ts>
                  <nts id="Seg_3368" n="HIAT:ip">,</nts>
                  <nts id="Seg_3369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1036" id="Seg_3371" n="HIAT:w" s="T1035">min</ts>
                  <nts id="Seg_3372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_3374" n="HIAT:w" s="T1036">utujan</ts>
                  <nts id="Seg_3375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1038" id="Seg_3377" n="HIAT:w" s="T1037">kaːlbɨppɨn</ts>
                  <nts id="Seg_3378" n="HIAT:ip">.</nts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1047" id="Seg_3380" n="sc" s="T1039">
               <ts e="T1047" id="Seg_3382" n="HIAT:u" s="T1039">
                  <ts e="T1040" id="Seg_3384" n="HIAT:w" s="T1039">Harsɨ͡arda</ts>
                  <nts id="Seg_3385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_3387" n="HIAT:w" s="T1040">uže</ts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_3390" n="HIAT:w" s="T1041">uhuktabɨn</ts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_3393" n="HIAT:w" s="T1042">oː</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_3396" n="HIAT:w" s="T1043">ohok</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_3399" n="HIAT:w" s="T1044">emi͡e</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1046" id="Seg_3402" n="HIAT:w" s="T1045">kötüte</ts>
                  <nts id="Seg_3403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_3405" n="HIAT:w" s="T1046">turar</ts>
                  <nts id="Seg_3406" n="HIAT:ip">.</nts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1055" id="Seg_3408" n="sc" s="T1048">
               <ts e="T1055" id="Seg_3410" n="HIAT:u" s="T1048">
                  <ts e="T1049" id="Seg_3412" n="HIAT:w" s="T1048">Bosku͡ojɨn</ts>
                  <nts id="Seg_3413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_3415" n="HIAT:w" s="T1049">du͡o</ts>
                  <nts id="Seg_3416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_3418" n="HIAT:w" s="T1050">kim</ts>
                  <nts id="Seg_3419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1052" id="Seg_3421" n="HIAT:w" s="T1051">da</ts>
                  <nts id="Seg_3422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_3424" n="HIAT:w" s="T1052">hu͡ok</ts>
                  <nts id="Seg_3425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1054" id="Seg_3427" n="HIAT:w" s="T1053">dʼi͡ebit</ts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_3430" n="HIAT:w" s="T1054">ihiger</ts>
                  <nts id="Seg_3431" n="HIAT:ip">.</nts>
                  <nts id="Seg_3432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1060" id="Seg_3433" n="sc" s="T1056">
               <ts e="T1060" id="Seg_3435" n="HIAT:u" s="T1056">
                  <ts e="T1057" id="Seg_3437" n="HIAT:w" s="T1056">Ol</ts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1058" id="Seg_3440" n="HIAT:w" s="T1057">tabalarɨn</ts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_3443" n="HIAT:w" s="T1058">tuttallar</ts>
                  <nts id="Seg_3444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_3446" n="HIAT:w" s="T1059">ebit</ts>
                  <nts id="Seg_3447" n="HIAT:ip">.</nts>
                  <nts id="Seg_3448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1063" id="Seg_3449" n="sc" s="T1061">
               <ts e="T1063" id="Seg_3451" n="HIAT:u" s="T1061">
                  <ts e="T1062" id="Seg_3453" n="HIAT:w" s="T1061">Tutta</ts>
                  <nts id="Seg_3454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_3456" n="HIAT:w" s="T1062">taksɨbɨttar</ts>
                  <nts id="Seg_3457" n="HIAT:ip">.</nts>
                  <nts id="Seg_3458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1071" id="Seg_3459" n="sc" s="T1064">
               <ts e="T1071" id="Seg_3461" n="HIAT:u" s="T1064">
                  <ts e="T1065" id="Seg_3463" n="HIAT:w" s="T1064">Ɨ͡allardaːk</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1066" id="Seg_3466" n="HIAT:w" s="T1065">etibit</ts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_3469" n="HIAT:w" s="T1066">tak</ts>
                  <nts id="Seg_3470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1068" id="Seg_3472" n="HIAT:w" s="T1067">da</ts>
                  <nts id="Seg_3473" n="HIAT:ip">,</nts>
                  <nts id="Seg_3474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_3476" n="HIAT:w" s="T1068">baːr</ts>
                  <nts id="Seg_3477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_3479" n="HIAT:w" s="T1069">etilere</ts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1071" id="Seg_3482" n="HIAT:w" s="T1070">ɨ͡allarbɨt</ts>
                  <nts id="Seg_3483" n="HIAT:ip">.</nts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1084" id="Seg_3485" n="sc" s="T1072">
               <ts e="T1084" id="Seg_3487" n="HIAT:u" s="T1072">
                  <ts e="T1073" id="Seg_3489" n="HIAT:w" s="T1072">De</ts>
                  <nts id="Seg_3490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_3492" n="HIAT:w" s="T1073">ol</ts>
                  <nts id="Seg_3493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1075" id="Seg_3495" n="HIAT:w" s="T1074">komujdular</ts>
                  <nts id="Seg_3496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1076" id="Seg_3498" n="HIAT:w" s="T1075">ü͡örderin</ts>
                  <nts id="Seg_3499" n="HIAT:ip">,</nts>
                  <nts id="Seg_3500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1077" id="Seg_3502" n="HIAT:w" s="T1076">komunallar</ts>
                  <nts id="Seg_3503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1078" id="Seg_3505" n="HIAT:w" s="T1077">taba</ts>
                  <nts id="Seg_3506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_3508" n="HIAT:w" s="T1078">tu͡ok</ts>
                  <nts id="Seg_3509" n="HIAT:ip">,</nts>
                  <nts id="Seg_3510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_3512" n="HIAT:w" s="T1079">köhöbüt</ts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1081" id="Seg_3515" n="HIAT:w" s="T1080">bɨhɨlak</ts>
                  <nts id="Seg_3516" n="HIAT:ip">,</nts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_3519" n="HIAT:w" s="T1081">öjdüːbün</ts>
                  <nts id="Seg_3520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1083" id="Seg_3522" n="HIAT:w" s="T1082">kanna</ts>
                  <nts id="Seg_3523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1084" id="Seg_3525" n="HIAT:w" s="T1083">ere</ts>
                  <nts id="Seg_3526" n="HIAT:ip">.</nts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1090" id="Seg_3528" n="sc" s="T1085">
               <ts e="T1090" id="Seg_3530" n="HIAT:u" s="T1085">
                  <ts e="T1086" id="Seg_3532" n="HIAT:w" s="T1085">Oː</ts>
                  <nts id="Seg_3533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1087" id="Seg_3535" n="HIAT:w" s="T1086">de</ts>
                  <nts id="Seg_3536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1088" id="Seg_3538" n="HIAT:w" s="T1087">minigin</ts>
                  <nts id="Seg_3539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1089" id="Seg_3541" n="HIAT:w" s="T1088">taŋɨnnaran</ts>
                  <nts id="Seg_3542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1090" id="Seg_3544" n="HIAT:w" s="T1089">čičigeretiler</ts>
                  <nts id="Seg_3545" n="HIAT:ip">.</nts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1096" id="Seg_3547" n="sc" s="T1091">
               <ts e="T1096" id="Seg_3549" n="HIAT:u" s="T1091">
                  <ts e="T1092" id="Seg_3551" n="HIAT:w" s="T1091">Hɨrgaga</ts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1093" id="Seg_3554" n="HIAT:w" s="T1092">baːjan</ts>
                  <nts id="Seg_3555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1094" id="Seg_3557" n="HIAT:w" s="T1093">keːspitter</ts>
                  <nts id="Seg_3558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1095" id="Seg_3560" n="HIAT:w" s="T1094">önülen</ts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_3563" n="HIAT:w" s="T1095">keːspitter</ts>
                  <nts id="Seg_3564" n="HIAT:ip">.</nts>
                  <nts id="Seg_3565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1100" id="Seg_3566" n="sc" s="T1097">
               <ts e="T1100" id="Seg_3568" n="HIAT:u" s="T1097">
                  <ts e="T1098" id="Seg_3570" n="HIAT:w" s="T1097">De</ts>
                  <nts id="Seg_3571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1099" id="Seg_3573" n="HIAT:w" s="T1098">kajdi͡ek</ts>
                  <nts id="Seg_3574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1100" id="Seg_3576" n="HIAT:w" s="T1099">barabɨt</ts>
                  <nts id="Seg_3577" n="HIAT:ip">?</nts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1107" id="Seg_3579" n="sc" s="T1101">
               <ts e="T1107" id="Seg_3581" n="HIAT:u" s="T1101">
                  <ts e="T1102" id="Seg_3583" n="HIAT:w" s="T1101">I</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1103" id="Seg_3586" n="HIAT:w" s="T1102">kennibitten</ts>
                  <nts id="Seg_3587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_3589" n="HIAT:w" s="T1103">du͡o</ts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_3592" n="HIAT:w" s="T1104">tabalarɨ</ts>
                  <nts id="Seg_3593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1106" id="Seg_3595" n="HIAT:w" s="T1105">hi͡eteːččiler</ts>
                  <nts id="Seg_3596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_3598" n="HIAT:w" s="T1106">kosobu͡ojga</ts>
                  <nts id="Seg_3599" n="HIAT:ip">.</nts>
                  <nts id="Seg_3600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1114" id="Seg_3601" n="sc" s="T1108">
               <ts e="T1114" id="Seg_3603" n="HIAT:u" s="T1108">
                  <ts e="T1109" id="Seg_3605" n="HIAT:w" s="T1108">Astaːk</ts>
                  <nts id="Seg_3606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1110" id="Seg_3608" n="HIAT:w" s="T1109">ili</ts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1111" id="Seg_3611" n="HIAT:w" s="T1110">mastaːk</ts>
                  <nts id="Seg_3612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_3614" n="HIAT:w" s="T1111">kosobu͡oj</ts>
                  <nts id="Seg_3615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_3617" n="HIAT:w" s="T1112">hi͡eteːčči</ts>
                  <nts id="Seg_3618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1114" id="Seg_3620" n="HIAT:w" s="T1113">etibit</ts>
                  <nts id="Seg_3621" n="HIAT:ip">.</nts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1120" id="Seg_3623" n="sc" s="T1115">
               <ts e="T1120" id="Seg_3625" n="HIAT:u" s="T1115">
                  <ts e="T1116" id="Seg_3627" n="HIAT:w" s="T1115">Olorton</ts>
                  <nts id="Seg_3628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1117" id="Seg_3630" n="HIAT:w" s="T1116">kuttanan</ts>
                  <nts id="Seg_3631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1118" id="Seg_3633" n="HIAT:w" s="T1117">ɨtaːn</ts>
                  <nts id="Seg_3634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1119" id="Seg_3636" n="HIAT:w" s="T1118">mörüleːktiːbin</ts>
                  <nts id="Seg_3637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1120" id="Seg_3639" n="HIAT:w" s="T1119">di͡en</ts>
                  <nts id="Seg_3640" n="HIAT:ip">.</nts>
                  <nts id="Seg_3641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1124" id="Seg_3642" n="sc" s="T1121">
               <ts e="T1124" id="Seg_3644" n="HIAT:u" s="T1121">
                  <nts id="Seg_3645" n="HIAT:ip">"</nts>
                  <ts e="T1122" id="Seg_3647" n="HIAT:w" s="T1121">Kuttanɨma</ts>
                  <nts id="Seg_3648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1123" id="Seg_3650" n="HIAT:w" s="T1122">ɨtɨrɨ͡a</ts>
                  <nts id="Seg_3651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1124" id="Seg_3653" n="HIAT:w" s="T1123">hu͡oga</ts>
                  <nts id="Seg_3654" n="HIAT:ip">.</nts>
                  <nts id="Seg_3655" n="HIAT:ip">"</nts>
                  <nts id="Seg_3656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1134" id="Seg_3657" n="sc" s="T1125">
               <ts e="T1134" id="Seg_3659" n="HIAT:u" s="T1125">
                  <ts e="T1126" id="Seg_3661" n="HIAT:w" s="T1125">Bu</ts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1127" id="Seg_3664" n="HIAT:w" s="T1126">ke</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1128" id="Seg_3667" n="HIAT:w" s="T1127">hɨlajbɨttar</ts>
                  <nts id="Seg_3668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1129" id="Seg_3670" n="HIAT:w" s="T1128">ke</ts>
                  <nts id="Seg_3671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1130" id="Seg_3673" n="HIAT:w" s="T1129">tɨːnallar</ts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1131" id="Seg_3676" n="HIAT:w" s="T1130">prjamo</ts>
                  <nts id="Seg_3677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1132" id="Seg_3679" n="HIAT:w" s="T1131">prjamo</ts>
                  <nts id="Seg_3680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1133" id="Seg_3682" n="HIAT:w" s="T1132">hɨrajbar</ts>
                  <nts id="Seg_3683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1134" id="Seg_3685" n="HIAT:w" s="T1133">munna</ts>
                  <nts id="Seg_3686" n="HIAT:ip">.</nts>
                  <nts id="Seg_3687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1149" id="Seg_3688" n="sc" s="T1135">
               <ts e="T1149" id="Seg_3690" n="HIAT:u" s="T1135">
                  <ts e="T1136" id="Seg_3692" n="HIAT:w" s="T1135">De</ts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1137" id="Seg_3695" n="HIAT:w" s="T1136">ol</ts>
                  <nts id="Seg_3696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1138" id="Seg_3698" n="HIAT:w" s="T1137">iti</ts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1139" id="Seg_3701" n="HIAT:w" s="T1138">korduk</ts>
                  <nts id="Seg_3702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1140" id="Seg_3704" n="HIAT:w" s="T1139">köhö</ts>
                  <nts id="Seg_3705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1141" id="Seg_3707" n="HIAT:w" s="T1140">hɨldʼaːččɨ</ts>
                  <nts id="Seg_3708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1142" id="Seg_3710" n="HIAT:w" s="T1141">etibit</ts>
                  <nts id="Seg_3711" n="HIAT:ip">,</nts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1143" id="Seg_3714" n="HIAT:w" s="T1142">köhö</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1144" id="Seg_3717" n="HIAT:w" s="T1143">hɨldʼaːččɨ</ts>
                  <nts id="Seg_3718" n="HIAT:ip">,</nts>
                  <nts id="Seg_3719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1145" id="Seg_3721" n="HIAT:w" s="T1144">köhüse</ts>
                  <nts id="Seg_3722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1146" id="Seg_3724" n="HIAT:w" s="T1145">hɨldʼaːččɨ</ts>
                  <nts id="Seg_3725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1147" id="Seg_3727" n="HIAT:w" s="T1146">etim</ts>
                  <nts id="Seg_3728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1148" id="Seg_3730" n="HIAT:w" s="T1147">ginileri</ts>
                  <nts id="Seg_3731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1149" id="Seg_3733" n="HIAT:w" s="T1148">gɨtta</ts>
                  <nts id="Seg_3734" n="HIAT:ip">.</nts>
                  <nts id="Seg_3735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T15" id="Seg_3736" n="sc" s="T1">
               <ts e="T2" id="Seg_3738" n="e" s="T1">Biːrde </ts>
               <ts e="T3" id="Seg_3740" n="e" s="T2">öjdüːbün </ts>
               <ts e="T4" id="Seg_3742" n="e" s="T3">küččügüj </ts>
               <ts e="T5" id="Seg_3744" n="e" s="T4">erdeppinen </ts>
               <ts e="T6" id="Seg_3746" n="e" s="T5">Nahoŋŋa </ts>
               <ts e="T7" id="Seg_3748" n="e" s="T6">točkabɨt </ts>
               <ts e="T8" id="Seg_3750" n="e" s="T7">Kiresten </ts>
               <ts e="T9" id="Seg_3752" n="e" s="T8">u͡on </ts>
               <ts e="T10" id="Seg_3754" n="e" s="T9">ikki </ts>
               <ts e="T11" id="Seg_3756" n="e" s="T10">kilometr. </ts>
               <ts e="T12" id="Seg_3758" n="e" s="T11">Onno </ts>
               <ts e="T13" id="Seg_3760" n="e" s="T12">töhölöːk </ts>
               <ts e="T14" id="Seg_3762" n="e" s="T13">kihi </ts>
               <ts e="T15" id="Seg_3764" n="e" s="T14">olorbutaj. </ts>
            </ts>
            <ts e="T21" id="Seg_3765" n="sc" s="T16">
               <ts e="T17" id="Seg_3767" n="e" s="T16">Kɨrdʼagastar </ts>
               <ts e="T18" id="Seg_3769" n="e" s="T17">barɨlara </ts>
               <ts e="T19" id="Seg_3771" n="e" s="T18">onno </ts>
               <ts e="T20" id="Seg_3773" n="e" s="T19">oloroːčču </ts>
               <ts e="T21" id="Seg_3775" n="e" s="T20">etilere. </ts>
            </ts>
            <ts e="T27" id="Seg_3776" n="sc" s="T22">
               <ts e="T23" id="Seg_3778" n="e" s="T22">Hɨrga </ts>
               <ts e="T24" id="Seg_3780" n="e" s="T23">di͡e </ts>
               <ts e="T25" id="Seg_3782" n="e" s="T24">toloru </ts>
               <ts e="T26" id="Seg_3784" n="e" s="T25">ete </ts>
               <ts e="T27" id="Seg_3786" n="e" s="T26">onno. </ts>
            </ts>
            <ts e="T32" id="Seg_3787" n="sc" s="T28">
               <ts e="T29" id="Seg_3789" n="e" s="T28">Tabalar </ts>
               <ts e="T30" id="Seg_3791" n="e" s="T29">tu͡oktar </ts>
               <ts e="T31" id="Seg_3793" n="e" s="T30">baːr </ts>
               <ts e="T32" id="Seg_3795" n="e" s="T31">etilere. </ts>
            </ts>
            <ts e="T52" id="Seg_3796" n="sc" s="T33">
               <ts e="T34" id="Seg_3798" n="e" s="T33">Onton </ts>
               <ts e="T35" id="Seg_3800" n="e" s="T34">biːrde </ts>
               <ts e="T36" id="Seg_3802" n="e" s="T35">teːtemeːk, </ts>
               <ts e="T37" id="Seg_3804" n="e" s="T36">dʼi͡eleriger </ts>
               <ts e="T38" id="Seg_3806" n="e" s="T37">teːtem </ts>
               <ts e="T39" id="Seg_3808" n="e" s="T38">kɨrdʼagas </ts>
               <ts e="T40" id="Seg_3810" n="e" s="T39">ehebin </ts>
               <ts e="T41" id="Seg_3812" n="e" s="T40">di͡eččibin </ts>
               <ts e="T42" id="Seg_3814" n="e" s="T41">bu͡o, </ts>
               <ts e="T43" id="Seg_3816" n="e" s="T42">teːte </ts>
               <ts e="T44" id="Seg_3818" n="e" s="T43">di͡eččibin, </ts>
               <ts e="T45" id="Seg_3820" n="e" s="T44">kɨrdʼagas </ts>
               <ts e="T46" id="Seg_3822" n="e" s="T45">maːmabɨn </ts>
               <ts e="T47" id="Seg_3824" n="e" s="T46">maːma </ts>
               <ts e="T48" id="Seg_3826" n="e" s="T47">di͡eččibin </ts>
               <ts e="T49" id="Seg_3828" n="e" s="T48">kɨrdʼagas </ts>
               <ts e="T50" id="Seg_3830" n="e" s="T49">ebebin </ts>
               <ts e="T51" id="Seg_3832" n="e" s="T50">ginilerge </ts>
               <ts e="T52" id="Seg_3834" n="e" s="T51">iːtillibitim. </ts>
            </ts>
            <ts e="T60" id="Seg_3835" n="sc" s="T53">
               <ts e="T54" id="Seg_3837" n="e" s="T53">De </ts>
               <ts e="T55" id="Seg_3839" n="e" s="T54">ol </ts>
               <ts e="T56" id="Seg_3841" n="e" s="T55">teːtebit </ts>
               <ts e="T57" id="Seg_3843" n="e" s="T56">kajdi͡ek </ts>
               <ts e="T58" id="Seg_3845" n="e" s="T57">ere </ts>
               <ts e="T59" id="Seg_3847" n="e" s="T58">baran </ts>
               <ts e="T60" id="Seg_3849" n="e" s="T59">kaːlla. </ts>
            </ts>
            <ts e="T72" id="Seg_3850" n="sc" s="T61">
               <ts e="T62" id="Seg_3852" n="e" s="T61">Mastana </ts>
               <ts e="T63" id="Seg_3854" n="e" s="T62">barda </ts>
               <ts e="T64" id="Seg_3856" n="e" s="T63">eni </ts>
               <ts e="T65" id="Seg_3858" n="e" s="T64">de </ts>
               <ts e="T66" id="Seg_3860" n="e" s="T65">olorobut </ts>
               <ts e="T67" id="Seg_3862" n="e" s="T66">tabalarbɨt </ts>
               <ts e="T68" id="Seg_3864" n="e" s="T67">prjamo </ts>
               <ts e="T69" id="Seg_3866" n="e" s="T68">kimŋe </ts>
               <ts e="T70" id="Seg_3868" n="e" s="T69">dʼi͡ebit </ts>
               <ts e="T71" id="Seg_3870" n="e" s="T70">tahɨgar </ts>
               <ts e="T72" id="Seg_3872" n="e" s="T71">hɨldʼallar. </ts>
            </ts>
            <ts e="T76" id="Seg_3873" n="sc" s="T73">
               <ts e="T74" id="Seg_3875" n="e" s="T73">Tabɨja </ts>
               <ts e="T75" id="Seg_3877" n="e" s="T74">hɨldʼallar </ts>
               <ts e="T76" id="Seg_3879" n="e" s="T75">budto. </ts>
            </ts>
            <ts e="T79" id="Seg_3880" n="sc" s="T77">
               <ts e="T78" id="Seg_3882" n="e" s="T77">Aːkulaːk </ts>
               <ts e="T79" id="Seg_3884" n="e" s="T78">etibit. </ts>
            </ts>
            <ts e="T83" id="Seg_3885" n="sc" s="T80">
               <ts e="T81" id="Seg_3887" n="e" s="T80">Bu </ts>
               <ts e="T82" id="Seg_3889" n="e" s="T81">iliːten </ts>
               <ts e="T83" id="Seg_3891" n="e" s="T82">ahɨːr. </ts>
            </ts>
            <ts e="T86" id="Seg_3892" n="sc" s="T84">
               <ts e="T85" id="Seg_3894" n="e" s="T84">Hili͡ebimsek </ts>
               <ts e="T86" id="Seg_3896" n="e" s="T85">bagajɨ. </ts>
            </ts>
            <ts e="T91" id="Seg_3897" n="sc" s="T87">
               <ts e="T88" id="Seg_3899" n="e" s="T87">Tünnükke </ts>
               <ts e="T89" id="Seg_3901" n="e" s="T88">keleːčči </ts>
               <ts e="T90" id="Seg_3903" n="e" s="T89">hili͡eb </ts>
               <ts e="T91" id="Seg_3905" n="e" s="T90">kördönö. </ts>
            </ts>
            <ts e="T105" id="Seg_3906" n="sc" s="T92">
               <ts e="T93" id="Seg_3908" n="e" s="T92">O </ts>
               <ts e="T94" id="Seg_3910" n="e" s="T93">munnuta </ts>
               <ts e="T95" id="Seg_3912" n="e" s="T94">nʼaltajan </ts>
               <ts e="T96" id="Seg_3914" n="e" s="T95">keli͡e </ts>
               <ts e="T97" id="Seg_3916" n="e" s="T96">di͡en </ts>
               <ts e="T98" id="Seg_3918" n="e" s="T97">oː </ts>
               <ts e="T99" id="Seg_3920" n="e" s="T98">hohujabɨn </ts>
               <ts e="T100" id="Seg_3922" n="e" s="T99">küččügüj </ts>
               <ts e="T101" id="Seg_3924" n="e" s="T100">kihi </ts>
               <ts e="T102" id="Seg_3926" n="e" s="T101">ke </ts>
               <ts e="T103" id="Seg_3928" n="e" s="T102">kuttanaːččɨ </ts>
               <ts e="T104" id="Seg_3930" n="e" s="T103">etim </ts>
               <ts e="T105" id="Seg_3932" n="e" s="T104">bu͡o. </ts>
            </ts>
            <ts e="T112" id="Seg_3933" n="sc" s="T106">
               <ts e="T107" id="Seg_3935" n="e" s="T106">Uː </ts>
               <ts e="T108" id="Seg_3937" n="e" s="T107">bu </ts>
               <ts e="T109" id="Seg_3939" n="e" s="T108">kihi </ts>
               <ts e="T110" id="Seg_3941" n="e" s="T109">kisten </ts>
               <ts e="T111" id="Seg_3943" n="e" s="T110">arɨjdɨm </ts>
               <ts e="T112" id="Seg_3945" n="e" s="T111">bu͡o. </ts>
            </ts>
            <ts e="T118" id="Seg_3946" n="sc" s="T113">
               <ts e="T114" id="Seg_3948" n="e" s="T113">Ostolton </ts>
               <ts e="T115" id="Seg_3950" n="e" s="T114">bu </ts>
               <ts e="T116" id="Seg_3952" n="e" s="T115">hili͡eb </ts>
               <ts e="T117" id="Seg_3954" n="e" s="T116">lepi͡eske </ts>
               <ts e="T118" id="Seg_3956" n="e" s="T117">bagajɨlar. </ts>
            </ts>
            <ts e="T126" id="Seg_3957" n="sc" s="T119">
               <ts e="T120" id="Seg_3959" n="e" s="T119">Oː </ts>
               <ts e="T121" id="Seg_3961" n="e" s="T120">ontum </ts>
               <ts e="T122" id="Seg_3963" n="e" s="T121">tülejbit </ts>
               <ts e="T123" id="Seg_3965" n="e" s="T122">hiː </ts>
               <ts e="T124" id="Seg_3967" n="e" s="T123">hi͡ektiːr </ts>
               <ts e="T125" id="Seg_3969" n="e" s="T124">di͡en </ts>
               <ts e="T126" id="Seg_3971" n="e" s="T125">araːrakata. </ts>
            </ts>
            <ts e="T134" id="Seg_3972" n="sc" s="T127">
               <ts e="T128" id="Seg_3974" n="e" s="T127">De </ts>
               <ts e="T129" id="Seg_3976" n="e" s="T128">ol </ts>
               <ts e="T130" id="Seg_3978" n="e" s="T129">maːmam </ts>
               <ts e="T131" id="Seg_3980" n="e" s="T130">diːr </ts>
               <ts e="T132" id="Seg_3982" n="e" s="T131">kɨrdʼagas </ts>
               <ts e="T133" id="Seg_3984" n="e" s="T132">maːmam </ts>
               <ts e="T134" id="Seg_3986" n="e" s="T133">ke. </ts>
            </ts>
            <ts e="T142" id="Seg_3987" n="sc" s="T135">
               <ts e="T136" id="Seg_3989" n="e" s="T135">Oː </ts>
               <ts e="T137" id="Seg_3991" n="e" s="T136">ü͡öretime </ts>
               <ts e="T138" id="Seg_3993" n="e" s="T137">dʼe </ts>
               <ts e="T139" id="Seg_3995" n="e" s="T138">en </ts>
               <ts e="T140" id="Seg_3997" n="e" s="T139">tünnükpütün </ts>
               <ts e="T141" id="Seg_3999" n="e" s="T140">aldʼatɨ͡a </ts>
               <ts e="T142" id="Seg_4001" n="e" s="T141">kanʼɨ͡a. </ts>
            </ts>
            <ts e="T152" id="Seg_4002" n="sc" s="T143">
               <ts e="T144" id="Seg_4004" n="e" s="T143">Bar </ts>
               <ts e="T145" id="Seg_4006" n="e" s="T144">bar </ts>
               <ts e="T146" id="Seg_4008" n="e" s="T145">bar </ts>
               <ts e="T147" id="Seg_4010" n="e" s="T146">kelime </ts>
               <ts e="T148" id="Seg_4012" n="e" s="T147">bolše </ts>
               <ts e="T149" id="Seg_4014" n="e" s="T148">diːr </ts>
               <ts e="T150" id="Seg_4016" n="e" s="T149">tu͡ok </ts>
               <ts e="T151" id="Seg_4018" n="e" s="T150">da </ts>
               <ts e="T152" id="Seg_4020" n="e" s="T151">hu͡ok. </ts>
            </ts>
            <ts e="T160" id="Seg_4021" n="sc" s="T153">
               <ts e="T154" id="Seg_4023" n="e" s="T153">Okkun </ts>
               <ts e="T155" id="Seg_4025" n="e" s="T154">hi͡e </ts>
               <ts e="T156" id="Seg_4027" n="e" s="T155">bar </ts>
               <ts e="T157" id="Seg_4029" n="e" s="T156">diːr </ts>
               <ts e="T158" id="Seg_4031" n="e" s="T157">heː </ts>
               <ts e="T159" id="Seg_4033" n="e" s="T158">haban </ts>
               <ts e="T160" id="Seg_4035" n="e" s="T159">keːste. </ts>
            </ts>
            <ts e="T169" id="Seg_4036" n="sc" s="T161">
               <ts e="T162" id="Seg_4038" n="e" s="T161">Onton </ts>
               <ts e="T163" id="Seg_4040" n="e" s="T162">biːrges </ts>
               <ts e="T164" id="Seg_4042" n="e" s="T163">tünnügünen </ts>
               <ts e="T165" id="Seg_4044" n="e" s="T164">keler </ts>
               <ts e="T166" id="Seg_4046" n="e" s="T165">onno </ts>
               <ts e="T167" id="Seg_4048" n="e" s="T166">peredok </ts>
               <ts e="T168" id="Seg_4050" n="e" s="T167">bu͡olaːččɨ </ts>
               <ts e="T169" id="Seg_4052" n="e" s="T168">bu͡o. </ts>
            </ts>
            <ts e="T174" id="Seg_4053" n="sc" s="T170">
               <ts e="T171" id="Seg_4055" n="e" s="T170">Onno </ts>
               <ts e="T172" id="Seg_4057" n="e" s="T171">buːs </ts>
               <ts e="T173" id="Seg_4059" n="e" s="T172">tu͡ok </ts>
               <ts e="T174" id="Seg_4061" n="e" s="T173">uːrunaːččɨbɨt. </ts>
            </ts>
            <ts e="T180" id="Seg_4062" n="sc" s="T175">
               <ts e="T176" id="Seg_4064" n="e" s="T175">Bu </ts>
               <ts e="T177" id="Seg_4066" n="e" s="T176">ke </ts>
               <ts e="T178" id="Seg_4068" n="e" s="T177">uːbut </ts>
               <ts e="T179" id="Seg_4070" n="e" s="T178">bu͡o </ts>
               <ts e="T180" id="Seg_4072" n="e" s="T179">buːsput. </ts>
            </ts>
            <ts e="T190" id="Seg_4073" n="sc" s="T181">
               <ts e="T182" id="Seg_4075" n="e" s="T181">Onno </ts>
               <ts e="T183" id="Seg_4077" n="e" s="T182">ɨtta </ts>
               <ts e="T184" id="Seg_4079" n="e" s="T183">ɨtta, </ts>
               <ts e="T185" id="Seg_4081" n="e" s="T184">tünnükpüt </ts>
               <ts e="T186" id="Seg_4083" n="e" s="T185">arɨllan </ts>
               <ts e="T187" id="Seg_4085" n="e" s="T186">turar </ts>
               <ts e="T188" id="Seg_4087" n="e" s="T187">bu͡o </ts>
               <ts e="T189" id="Seg_4089" n="e" s="T188">innikiːbit </ts>
               <ts e="T190" id="Seg_4091" n="e" s="T189">ke. </ts>
            </ts>
            <ts e="T196" id="Seg_4092" n="sc" s="T191">
               <ts e="T192" id="Seg_4094" n="e" s="T191">Onno </ts>
               <ts e="T193" id="Seg_4096" n="e" s="T192">moltojo </ts>
               <ts e="T194" id="Seg_4098" n="e" s="T193">moltojo </ts>
               <ts e="T195" id="Seg_4100" n="e" s="T194">mu͡ostardaːk </ts>
               <ts e="T196" id="Seg_4102" n="e" s="T195">bu͡o. </ts>
            </ts>
            <ts e="T208" id="Seg_4103" n="sc" s="T197">
               <ts e="T198" id="Seg_4105" n="e" s="T197">Mu͡ostara </ts>
               <ts e="T199" id="Seg_4107" n="e" s="T198">bappattar </ts>
               <ts e="T200" id="Seg_4109" n="e" s="T199">kim </ts>
               <ts e="T201" id="Seg_4111" n="e" s="T200">čoŋalge </ts>
               <ts e="T202" id="Seg_4113" n="e" s="T201">lepi͡eske </ts>
               <ts e="T203" id="Seg_4115" n="e" s="T202">turar </ts>
               <ts e="T204" id="Seg_4117" n="e" s="T203">ete </ts>
               <ts e="T205" id="Seg_4119" n="e" s="T204">(lepi͡es-) </ts>
               <ts e="T206" id="Seg_4121" n="e" s="T205">(aldʼal-) </ts>
               <ts e="T207" id="Seg_4123" n="e" s="T206">kimneːbit </ts>
               <ts e="T208" id="Seg_4125" n="e" s="T207">bɨhɨllɨbɨttar. </ts>
            </ts>
            <ts e="T217" id="Seg_4126" n="sc" s="T209">
               <ts e="T210" id="Seg_4128" n="e" s="T209">De </ts>
               <ts e="T211" id="Seg_4130" n="e" s="T210">onu </ts>
               <ts e="T212" id="Seg_4132" n="e" s="T211">tiːje </ts>
               <ts e="T213" id="Seg_4134" n="e" s="T212">hataːta </ts>
               <ts e="T214" id="Seg_4136" n="e" s="T213">bu͡o </ts>
               <ts e="T215" id="Seg_4138" n="e" s="T214">tɨlɨn </ts>
               <ts e="T216" id="Seg_4140" n="e" s="T215">uːna </ts>
               <ts e="T217" id="Seg_4142" n="e" s="T216">uːna. </ts>
            </ts>
            <ts e="T231" id="Seg_4143" n="sc" s="T218">
               <ts e="T219" id="Seg_4145" n="e" s="T218">"Maːma </ts>
               <ts e="T220" id="Seg_4147" n="e" s="T219">kör </ts>
               <ts e="T221" id="Seg_4149" n="e" s="T220">kör </ts>
               <ts e="T222" id="Seg_4151" n="e" s="T221">kör", </ts>
               <ts e="T223" id="Seg_4153" n="e" s="T222">diːbin, </ts>
               <ts e="T224" id="Seg_4155" n="e" s="T223">"kimŋin </ts>
               <ts e="T225" id="Seg_4157" n="e" s="T224">hi͡eri </ts>
               <ts e="T226" id="Seg_4159" n="e" s="T225">gɨnna </ts>
               <ts e="T227" id="Seg_4161" n="e" s="T226">kili͡eptergin </ts>
               <ts e="T228" id="Seg_4163" n="e" s="T227">hi͡eri </ts>
               <ts e="T229" id="Seg_4165" n="e" s="T228">gɨnna </ts>
               <ts e="T230" id="Seg_4167" n="e" s="T229">diːbin </ts>
               <ts e="T231" id="Seg_4169" n="e" s="T230">kili͡eptergin." </ts>
            </ts>
            <ts e="T243" id="Seg_4170" n="sc" s="T232">
               <ts e="T233" id="Seg_4172" n="e" s="T232">"Oj </ts>
               <ts e="T234" id="Seg_4174" n="e" s="T233">togo </ts>
               <ts e="T235" id="Seg_4176" n="e" s="T234">satana </ts>
               <ts e="T236" id="Seg_4178" n="e" s="T235">kɨːha </ts>
               <ts e="T237" id="Seg_4180" n="e" s="T236">togo </ts>
               <ts e="T238" id="Seg_4182" n="e" s="T237">ü͡öreppikkinij </ts>
               <ts e="T239" id="Seg_4184" n="e" s="T238">bu͡o, </ts>
               <ts e="T240" id="Seg_4186" n="e" s="T239">dʼi͡etten </ts>
               <ts e="T241" id="Seg_4188" n="e" s="T240">ahaːta </ts>
               <ts e="T242" id="Seg_4190" n="e" s="T241">tünnükten </ts>
               <ts e="T243" id="Seg_4192" n="e" s="T242">ahaːta. </ts>
            </ts>
            <ts e="T249" id="Seg_4193" n="sc" s="T244">
               <ts e="T245" id="Seg_4195" n="e" s="T244">Kör </ts>
               <ts e="T246" id="Seg_4197" n="e" s="T245">anɨ </ts>
               <ts e="T247" id="Seg_4199" n="e" s="T246">barɨtɨn </ts>
               <ts e="T248" id="Seg_4201" n="e" s="T247">u͡oru͡oga", </ts>
               <ts e="T249" id="Seg_4203" n="e" s="T248">diːr. </ts>
            </ts>
            <ts e="T253" id="Seg_4204" n="sc" s="T250">
               <ts e="T251" id="Seg_4206" n="e" s="T250">Araː </ts>
               <ts e="T252" id="Seg_4208" n="e" s="T251">bɨ͡ar </ts>
               <ts e="T253" id="Seg_4210" n="e" s="T252">agajbɨn. </ts>
            </ts>
            <ts e="T257" id="Seg_4211" n="sc" s="T254">
               <ts e="T255" id="Seg_4213" n="e" s="T254">Uː </ts>
               <ts e="T256" id="Seg_4215" n="e" s="T255">bar </ts>
               <ts e="T257" id="Seg_4217" n="e" s="T256">bar. </ts>
            </ts>
            <ts e="T263" id="Seg_4218" n="sc" s="T258">
               <ts e="T259" id="Seg_4220" n="e" s="T258">Tu͡ok </ts>
               <ts e="T260" id="Seg_4222" n="e" s="T259">ere </ts>
               <ts e="T261" id="Seg_4224" n="e" s="T260">trepkenen </ts>
               <ts e="T262" id="Seg_4226" n="e" s="T261">leš </ts>
               <ts e="T263" id="Seg_4228" n="e" s="T262">leš. </ts>
            </ts>
            <ts e="T266" id="Seg_4229" n="sc" s="T264">
               <ts e="T265" id="Seg_4231" n="e" s="T264">Onton </ts>
               <ts e="T266" id="Seg_4233" n="e" s="T265">barbat. </ts>
            </ts>
            <ts e="T271" id="Seg_4234" n="sc" s="T267">
               <ts e="T268" id="Seg_4236" n="e" s="T267">De </ts>
               <ts e="T269" id="Seg_4238" n="e" s="T268">na </ts>
               <ts e="T270" id="Seg_4240" n="e" s="T269">vso </ts>
               <ts e="T271" id="Seg_4242" n="e" s="T270">bar. </ts>
            </ts>
            <ts e="T275" id="Seg_4243" n="sc" s="T272">
               <ts e="T273" id="Seg_4245" n="e" s="T272">Bi͡eren </ts>
               <ts e="T274" id="Seg_4247" n="e" s="T273">keːher </ts>
               <ts e="T275" id="Seg_4249" n="e" s="T274">bu͡o. </ts>
            </ts>
            <ts e="T293" id="Seg_4250" n="sc" s="T276">
               <ts e="T277" id="Seg_4252" n="e" s="T276">Ontuŋ </ts>
               <ts e="T278" id="Seg_4254" n="e" s="T277">momuja </ts>
               <ts e="T279" id="Seg_4256" n="e" s="T278">momuja </ts>
               <ts e="T280" id="Seg_4258" n="e" s="T279">voːpse </ts>
               <ts e="T281" id="Seg_4260" n="e" s="T280">baraːktɨːr </ts>
               <ts e="T282" id="Seg_4262" n="e" s="T281">di͡en </ts>
               <ts e="T283" id="Seg_4264" n="e" s="T282">hi͡ese </ts>
               <ts e="T284" id="Seg_4266" n="e" s="T283">hili͡ebi, </ts>
               <ts e="T285" id="Seg_4268" n="e" s="T284">hi͡ese </ts>
               <ts e="T286" id="Seg_4270" n="e" s="T285">totto </ts>
               <ts e="T287" id="Seg_4272" n="e" s="T286">bɨhɨlaːk, </ts>
               <ts e="T288" id="Seg_4274" n="e" s="T287">tu͡ok </ts>
               <ts e="T289" id="Seg_4276" n="e" s="T288">toholaːk </ts>
               <ts e="T290" id="Seg_4278" n="e" s="T289">totu͡oj, </ts>
               <ts e="T291" id="Seg_4280" n="e" s="T290">de </ts>
               <ts e="T292" id="Seg_4282" n="e" s="T291">ol </ts>
               <ts e="T293" id="Seg_4284" n="e" s="T292">kačɨgɨrɨtar. </ts>
            </ts>
            <ts e="T299" id="Seg_4285" n="sc" s="T294">
               <ts e="T295" id="Seg_4287" n="e" s="T294">Okazɨvaetsa </ts>
               <ts e="T296" id="Seg_4289" n="e" s="T295">kimi </ts>
               <ts e="T297" id="Seg_4291" n="e" s="T296">bi͡erbit </ts>
               <ts e="T298" id="Seg_4293" n="e" s="T297">kappɨt </ts>
               <ts e="T299" id="Seg_4295" n="e" s="T298">hili͡ebi. </ts>
            </ts>
            <ts e="T305" id="Seg_4296" n="sc" s="T300">
               <ts e="T301" id="Seg_4298" n="e" s="T300">De </ts>
               <ts e="T302" id="Seg_4300" n="e" s="T301">onno </ts>
               <ts e="T303" id="Seg_4302" n="e" s="T302">ɨstɨː </ts>
               <ts e="T304" id="Seg_4304" n="e" s="T303">hataːta </ts>
               <ts e="T305" id="Seg_4306" n="e" s="T304">bu͡o. </ts>
            </ts>
            <ts e="T308" id="Seg_4307" n="sc" s="T306">
               <ts e="T307" id="Seg_4309" n="e" s="T306">Araːle </ts>
               <ts e="T308" id="Seg_4311" n="e" s="T307">beseleːtin. </ts>
            </ts>
            <ts e="T314" id="Seg_4312" n="sc" s="T309">
               <ts e="T310" id="Seg_4314" n="e" s="T309">Oj </ts>
               <ts e="T311" id="Seg_4316" n="e" s="T310">tiːsterin </ts>
               <ts e="T312" id="Seg_4318" n="e" s="T311">barɨtɨn </ts>
               <ts e="T313" id="Seg_4320" n="e" s="T312">kimneːte </ts>
               <ts e="T314" id="Seg_4322" n="e" s="T313">bɨhɨlak. </ts>
            </ts>
            <ts e="T321" id="Seg_4323" n="sc" s="T315">
               <ts e="T316" id="Seg_4325" n="e" s="T315">De </ts>
               <ts e="T317" id="Seg_4327" n="e" s="T316">ol </ts>
               <ts e="T318" id="Seg_4329" n="e" s="T317">barbɨta, </ts>
               <ts e="T319" id="Seg_4331" n="e" s="T318">hi͡ese </ts>
               <ts e="T320" id="Seg_4333" n="e" s="T319">dogottorun </ts>
               <ts e="T321" id="Seg_4335" n="e" s="T320">di͡ek. </ts>
            </ts>
            <ts e="T328" id="Seg_4336" n="sc" s="T322">
               <ts e="T323" id="Seg_4338" n="e" s="T322">Hɨːr </ts>
               <ts e="T324" id="Seg_4340" n="e" s="T323">annɨtɨgar </ts>
               <ts e="T325" id="Seg_4342" n="e" s="T324">tüspüttere </ts>
               <ts e="T326" id="Seg_4344" n="e" s="T325">onno </ts>
               <ts e="T327" id="Seg_4346" n="e" s="T326">ebege </ts>
               <ts e="T328" id="Seg_4348" n="e" s="T327">hɨldʼallar. </ts>
            </ts>
            <ts e="T334" id="Seg_4349" n="sc" s="T329">
               <ts e="T330" id="Seg_4351" n="e" s="T329">Tahaːrabɨt </ts>
               <ts e="T331" id="Seg_4353" n="e" s="T330">boru͡orda </ts>
               <ts e="T332" id="Seg_4355" n="e" s="T331">karaŋarar, </ts>
               <ts e="T333" id="Seg_4357" n="e" s="T332">karaŋaran </ts>
               <ts e="T334" id="Seg_4359" n="e" s="T333">iher. </ts>
            </ts>
            <ts e="T347" id="Seg_4360" n="sc" s="T335">
               <ts e="T336" id="Seg_4362" n="e" s="T335">Kim </ts>
               <ts e="T337" id="Seg_4364" n="e" s="T336">ɨttaːk </ts>
               <ts e="T338" id="Seg_4366" n="e" s="T337">etibit </ts>
               <ts e="T339" id="Seg_4368" n="e" s="T338">bihigi, </ts>
               <ts e="T340" id="Seg_4370" n="e" s="T339">Djoŋgo, </ts>
               <ts e="T341" id="Seg_4372" n="e" s="T340">čeːlke </ts>
               <ts e="T342" id="Seg_4374" n="e" s="T341">bagajɨ, </ts>
               <ts e="T343" id="Seg_4376" n="e" s="T342">bosku͡oj </ts>
               <ts e="T344" id="Seg_4378" n="e" s="T343">bagajɨ, </ts>
               <ts e="T345" id="Seg_4380" n="e" s="T344">tabahɨt </ts>
               <ts e="T346" id="Seg_4382" n="e" s="T345">bagajɨ </ts>
               <ts e="T347" id="Seg_4384" n="e" s="T346">ontuŋ. </ts>
            </ts>
            <ts e="T357" id="Seg_4385" n="sc" s="T348">
               <ts e="T349" id="Seg_4387" n="e" s="T348">Kim </ts>
               <ts e="T350" id="Seg_4389" n="e" s="T349">hüs </ts>
               <ts e="T351" id="Seg_4391" n="e" s="T350">di͡eŋ </ts>
               <ts e="T352" id="Seg_4393" n="e" s="T351">du͡o </ts>
               <ts e="T353" id="Seg_4395" n="e" s="T352">kimi </ts>
               <ts e="T354" id="Seg_4397" n="e" s="T353">ü͡örü </ts>
               <ts e="T355" id="Seg_4399" n="e" s="T354">barɨtɨn </ts>
               <ts e="T356" id="Seg_4401" n="e" s="T355">egelen </ts>
               <ts e="T357" id="Seg_4403" n="e" s="T356">keli͡e. </ts>
            </ts>
            <ts e="T365" id="Seg_4404" n="sc" s="T358">
               <ts e="T359" id="Seg_4406" n="e" s="T358">Oː </ts>
               <ts e="T360" id="Seg_4408" n="e" s="T359">erijekti͡ege </ts>
               <ts e="T361" id="Seg_4410" n="e" s="T360">di͡en </ts>
               <ts e="T362" id="Seg_4412" n="e" s="T361">tögürüččü </ts>
               <ts e="T363" id="Seg_4414" n="e" s="T362">biːr </ts>
               <ts e="T364" id="Seg_4416" n="e" s="T363">hirge </ts>
               <ts e="T365" id="Seg_4418" n="e" s="T364">turu͡oga. </ts>
            </ts>
            <ts e="T370" id="Seg_4419" n="sc" s="T366">
               <ts e="T367" id="Seg_4421" n="e" s="T366">Bu </ts>
               <ts e="T368" id="Seg_4423" n="e" s="T367">egel </ts>
               <ts e="T369" id="Seg_4425" n="e" s="T368">di͡etekke </ts>
               <ts e="T370" id="Seg_4427" n="e" s="T369">ke. </ts>
            </ts>
            <ts e="T382" id="Seg_4428" n="sc" s="T371">
               <ts e="T372" id="Seg_4430" n="e" s="T371">De </ts>
               <ts e="T373" id="Seg_4432" n="e" s="T372">ontubut </ts>
               <ts e="T374" id="Seg_4434" n="e" s="T373">kimi </ts>
               <ts e="T375" id="Seg_4436" n="e" s="T374">kimneːčči </ts>
               <ts e="T376" id="Seg_4438" n="e" s="T375">ete </ts>
               <ts e="T377" id="Seg_4440" n="e" s="T376">maːjdɨn </ts>
               <ts e="T378" id="Seg_4442" n="e" s="T377">tabalarɨŋ </ts>
               <ts e="T379" id="Seg_4444" n="e" s="T378">baːr </ts>
               <ts e="T380" id="Seg_4446" n="e" s="T379">erdekterinen </ts>
               <ts e="T381" id="Seg_4448" n="e" s="T380">ke </ts>
               <ts e="T382" id="Seg_4450" n="e" s="T381">dʼi͡ege. </ts>
            </ts>
            <ts e="T397" id="Seg_4451" n="sc" s="T383">
               <ts e="T384" id="Seg_4453" n="e" s="T383">Tugut, </ts>
               <ts e="T385" id="Seg_4455" n="e" s="T384">abɨlakaːn </ts>
               <ts e="T386" id="Seg_4457" n="e" s="T385">du </ts>
               <ts e="T387" id="Seg_4459" n="e" s="T386">tu͡ok </ts>
               <ts e="T388" id="Seg_4461" n="e" s="T387">du, </ts>
               <ts e="T389" id="Seg_4463" n="e" s="T388">abɨlakaːn, </ts>
               <ts e="T390" id="Seg_4465" n="e" s="T389">tugut </ts>
               <ts e="T391" id="Seg_4467" n="e" s="T390">eː </ts>
               <ts e="T392" id="Seg_4469" n="e" s="T391">ulakan, </ts>
               <ts e="T393" id="Seg_4471" n="e" s="T392">tuguttan </ts>
               <ts e="T394" id="Seg_4473" n="e" s="T393">ulakankaːn </ts>
               <ts e="T395" id="Seg_4475" n="e" s="T394">dʼɨlɨnan </ts>
               <ts e="T396" id="Seg_4477" n="e" s="T395">ere </ts>
               <ts e="T397" id="Seg_4479" n="e" s="T396">ulakan. </ts>
            </ts>
            <ts e="T413" id="Seg_4480" n="sc" s="T398">
               <ts e="T399" id="Seg_4482" n="e" s="T398">Ol </ts>
               <ts e="T400" id="Seg_4484" n="e" s="T399">kimi </ts>
               <ts e="T401" id="Seg_4486" n="e" s="T400">de </ts>
               <ts e="T402" id="Seg_4488" n="e" s="T401">erije </ts>
               <ts e="T403" id="Seg_4490" n="e" s="T402">hɨtɨ͡aga </ts>
               <ts e="T404" id="Seg_4492" n="e" s="T403">bu͡o, </ts>
               <ts e="T405" id="Seg_4494" n="e" s="T404">muŋnu </ts>
               <ts e="T406" id="Seg_4496" n="e" s="T405">hɨtaːččɨ </ts>
               <ts e="T407" id="Seg_4498" n="e" s="T406">iti </ts>
               <ts e="T408" id="Seg_4500" n="e" s="T407">tu͡ok </ts>
               <ts e="T409" id="Seg_4502" n="e" s="T408">tugukkaːn </ts>
               <ts e="T410" id="Seg_4504" n="e" s="T409">bagaraːččɨ </ts>
               <ts e="T411" id="Seg_4506" n="e" s="T410">dürü </ts>
               <ts e="T412" id="Seg_4508" n="e" s="T411">möŋö </ts>
               <ts e="T413" id="Seg_4510" n="e" s="T412">möŋö. </ts>
            </ts>
            <ts e="T438" id="Seg_4511" n="sc" s="T414">
               <ts e="T415" id="Seg_4513" n="e" s="T414">Hɨtɨ͡aga </ts>
               <ts e="T416" id="Seg_4515" n="e" s="T415">huh </ts>
               <ts e="T417" id="Seg_4517" n="e" s="T416">bu͡o </ts>
               <ts e="T418" id="Seg_4519" n="e" s="T417">oːnnʼuːr </ts>
               <ts e="T419" id="Seg_4521" n="e" s="T418">bu͡o </ts>
               <ts e="T420" id="Seg_4523" n="e" s="T419">ginini </ts>
               <ts e="T421" id="Seg_4525" n="e" s="T420">gɨtta </ts>
               <ts e="T422" id="Seg_4527" n="e" s="T421">ke. </ts>
               <ts e="T423" id="Seg_4529" n="e" s="T422">Ontuŋ </ts>
               <ts e="T424" id="Seg_4531" n="e" s="T423">kajdak </ts>
               <ts e="T425" id="Seg_4533" n="e" s="T424">da </ts>
               <ts e="T426" id="Seg_4535" n="e" s="T425">bu͡olla </ts>
               <ts e="T1150" id="Seg_4537" n="e" s="T426">((…)) </ts>
               <ts e="T427" id="Seg_4539" n="e" s="T1150">tabɨhas </ts>
               <ts e="T428" id="Seg_4541" n="e" s="T427">tabɨsar </ts>
               <ts e="T429" id="Seg_4543" n="e" s="T428">bu </ts>
               <ts e="T430" id="Seg_4545" n="e" s="T429">kurduk </ts>
               <ts e="T431" id="Seg_4547" n="e" s="T430">kajdak </ts>
               <ts e="T432" id="Seg_4549" n="e" s="T431">gɨnɨj. </ts>
               <ts e="T433" id="Seg_4551" n="e" s="T432">"Maːma </ts>
               <ts e="T434" id="Seg_4553" n="e" s="T433">maːma", </ts>
               <ts e="T435" id="Seg_4555" n="e" s="T434">diːbin, </ts>
               <ts e="T436" id="Seg_4557" n="e" s="T435">"kör </ts>
               <ts e="T437" id="Seg_4559" n="e" s="T436">kör </ts>
               <ts e="T438" id="Seg_4561" n="e" s="T437">Djoŋgonu." </ts>
            </ts>
            <ts e="T442" id="Seg_4562" n="sc" s="T439">
               <ts e="T440" id="Seg_4564" n="e" s="T439">Čoko </ts>
               <ts e="T441" id="Seg_4566" n="e" s="T440">Čoko </ts>
               <ts e="T442" id="Seg_4568" n="e" s="T441">di͡eččibin. </ts>
            </ts>
            <ts e="T445" id="Seg_4569" n="sc" s="T443">
               <ts e="T444" id="Seg_4571" n="e" s="T443">"Čoko </ts>
               <ts e="T445" id="Seg_4573" n="e" s="T444">kör. </ts>
            </ts>
            <ts e="T453" id="Seg_4574" n="sc" s="T446">
               <ts e="T447" id="Seg_4576" n="e" s="T446">Kimi </ts>
               <ts e="T448" id="Seg_4578" n="e" s="T447">hi͡eri </ts>
               <ts e="T449" id="Seg_4580" n="e" s="T448">gɨnna", </ts>
               <ts e="T450" id="Seg_4582" n="e" s="T449">diːbin, </ts>
               <ts e="T451" id="Seg_4584" n="e" s="T450">"tugutu </ts>
               <ts e="T452" id="Seg_4586" n="e" s="T451">hi͡eri </ts>
               <ts e="T453" id="Seg_4588" n="e" s="T452">gɨnna." </ts>
            </ts>
            <ts e="T458" id="Seg_4589" n="sc" s="T454">
               <ts e="T455" id="Seg_4591" n="e" s="T454">"Oː </ts>
               <ts e="T456" id="Seg_4593" n="e" s="T455">satana </ts>
               <ts e="T457" id="Seg_4595" n="e" s="T456">ɨgɨr </ts>
               <ts e="T458" id="Seg_4597" n="e" s="T457">ɨgɨr." </ts>
            </ts>
            <ts e="T464" id="Seg_4598" n="sc" s="T459">
               <ts e="T460" id="Seg_4600" n="e" s="T459">Hɨlatta </ts>
               <ts e="T461" id="Seg_4602" n="e" s="T460">bɨhɨlak </ts>
               <ts e="T462" id="Seg_4604" n="e" s="T461">iti </ts>
               <ts e="T463" id="Seg_4606" n="e" s="T462">tugutu, </ts>
               <ts e="T464" id="Seg_4608" n="e" s="T463">kuttaːta. </ts>
            </ts>
            <ts e="T473" id="Seg_4609" n="sc" s="T465">
               <ts e="T466" id="Seg_4611" n="e" s="T465">"Čoko </ts>
               <ts e="T467" id="Seg_4613" n="e" s="T466">Čoko </ts>
               <ts e="T468" id="Seg_4615" n="e" s="T467">Čoko", </ts>
               <ts e="T469" id="Seg_4617" n="e" s="T468">diːbin </ts>
               <ts e="T470" id="Seg_4619" n="e" s="T469">tahaːra </ts>
               <ts e="T471" id="Seg_4621" n="e" s="T470">taksan </ts>
               <ts e="T472" id="Seg_4623" n="e" s="T471">baran </ts>
               <ts e="T473" id="Seg_4625" n="e" s="T472">ke. </ts>
            </ts>
            <ts e="T480" id="Seg_4626" n="sc" s="T474">
               <ts e="T475" id="Seg_4628" n="e" s="T474">Oː </ts>
               <ts e="T476" id="Seg_4630" n="e" s="T475">de </ts>
               <ts e="T477" id="Seg_4632" n="e" s="T476">ontuŋ </ts>
               <ts e="T478" id="Seg_4634" n="e" s="T477">kötüten </ts>
               <ts e="T479" id="Seg_4636" n="e" s="T478">iheːktiːr </ts>
               <ts e="T480" id="Seg_4638" n="e" s="T479">di͡en. </ts>
            </ts>
            <ts e="T488" id="Seg_4639" n="sc" s="T481">
               <ts e="T482" id="Seg_4641" n="e" s="T481">De </ts>
               <ts e="T483" id="Seg_4643" n="e" s="T482">(ɨstan-) </ts>
               <ts e="T484" id="Seg_4645" n="e" s="T483">de </ts>
               <ts e="T485" id="Seg_4647" n="e" s="T484">hɨrajbar, </ts>
               <ts e="T486" id="Seg_4649" n="e" s="T485">min </ts>
               <ts e="T487" id="Seg_4651" n="e" s="T486">taras </ts>
               <ts e="T488" id="Seg_4653" n="e" s="T487">hahaha. </ts>
            </ts>
            <ts e="T493" id="Seg_4654" n="sc" s="T489">
               <ts e="T490" id="Seg_4656" n="e" s="T489">Ittenne </ts>
               <ts e="T491" id="Seg_4658" n="e" s="T490">taraja </ts>
               <ts e="T492" id="Seg_4660" n="e" s="T491">hɨtaːktɨːbɨn </ts>
               <ts e="T493" id="Seg_4662" n="e" s="T492">di͡en. </ts>
            </ts>
            <ts e="T500" id="Seg_4663" n="sc" s="T494">
               <ts e="T495" id="Seg_4665" n="e" s="T494">"Eː", </ts>
               <ts e="T496" id="Seg_4667" n="e" s="T495">diːbin, </ts>
               <ts e="T497" id="Seg_4669" n="e" s="T496">"maːma </ts>
               <ts e="T498" id="Seg_4671" n="e" s="T497">ɨl </ts>
               <ts e="T499" id="Seg_4673" n="e" s="T498">dʼa </ts>
               <ts e="T500" id="Seg_4675" n="e" s="T499">bu͡o." </ts>
            </ts>
            <ts e="T508" id="Seg_4676" n="sc" s="T501">
               <ts e="T502" id="Seg_4678" n="e" s="T501">Halɨːr </ts>
               <ts e="T503" id="Seg_4680" n="e" s="T502">kanʼɨːr </ts>
               <ts e="T504" id="Seg_4682" n="e" s="T503">(möŋ-) </ts>
               <ts e="T505" id="Seg_4684" n="e" s="T504">möŋtörör </ts>
               <ts e="T506" id="Seg_4686" n="e" s="T505">bu͡o </ts>
               <ts e="T507" id="Seg_4688" n="e" s="T506">kihini </ts>
               <ts e="T508" id="Seg_4690" n="e" s="T507">ke. </ts>
            </ts>
            <ts e="T511" id="Seg_4691" n="sc" s="T509">
               <ts e="T510" id="Seg_4693" n="e" s="T509">Möŋü͡ögün </ts>
               <ts e="T511" id="Seg_4695" n="e" s="T510">bagarar. </ts>
            </ts>
            <ts e="T516" id="Seg_4696" n="sc" s="T512">
               <ts e="T513" id="Seg_4698" n="e" s="T512">Minigin </ts>
               <ts e="T514" id="Seg_4700" n="e" s="T513">hogostuː </ts>
               <ts e="T515" id="Seg_4702" n="e" s="T514">hɨtaːktɨːr </ts>
               <ts e="T516" id="Seg_4704" n="e" s="T515">di͡en. </ts>
            </ts>
            <ts e="T521" id="Seg_4705" n="sc" s="T517">
               <ts e="T518" id="Seg_4707" n="e" s="T517">De </ts>
               <ts e="T519" id="Seg_4709" n="e" s="T518">ol </ts>
               <ts e="T520" id="Seg_4711" n="e" s="T519">maːmam </ts>
               <ts e="T521" id="Seg_4713" n="e" s="T520">tagɨsta. </ts>
            </ts>
            <ts e="T525" id="Seg_4714" n="sc" s="T522">
               <ts e="T523" id="Seg_4716" n="e" s="T522">De </ts>
               <ts e="T524" id="Seg_4718" n="e" s="T523">tu͡ok </ts>
               <ts e="T525" id="Seg_4720" n="e" s="T524">bu͡olagɨn. </ts>
            </ts>
            <ts e="T537" id="Seg_4721" n="sc" s="T526">
               <ts e="T527" id="Seg_4723" n="e" s="T526">"Kör", </ts>
               <ts e="T528" id="Seg_4725" n="e" s="T527">diːbin, </ts>
               <ts e="T529" id="Seg_4727" n="e" s="T528">"minigin </ts>
               <ts e="T530" id="Seg_4729" n="e" s="T529">kɨrbɨːr </ts>
               <ts e="T531" id="Seg_4731" n="e" s="T530">du͡o", </ts>
               <ts e="T532" id="Seg_4733" n="e" s="T531">diːbin, </ts>
               <ts e="T533" id="Seg_4735" n="e" s="T532">"oːnnʼuːr </ts>
               <ts e="T534" id="Seg_4737" n="e" s="T533">kihini </ts>
               <ts e="T535" id="Seg_4739" n="e" s="T534">ke </ts>
               <ts e="T536" id="Seg_4741" n="e" s="T535">araː </ts>
               <ts e="T537" id="Seg_4743" n="e" s="T536">da." </ts>
            </ts>
            <ts e="T542" id="Seg_4744" n="sc" s="T538">
               <ts e="T539" id="Seg_4746" n="e" s="T538">Čičigeren </ts>
               <ts e="T540" id="Seg_4748" n="e" s="T539">ɨtaːn </ts>
               <ts e="T541" id="Seg_4750" n="e" s="T540">mörüleːktiːbin </ts>
               <ts e="T542" id="Seg_4752" n="e" s="T541">heː. </ts>
            </ts>
            <ts e="T545" id="Seg_4753" n="sc" s="T543">
               <ts e="T544" id="Seg_4755" n="e" s="T543">"Ɨtaː </ts>
               <ts e="T545" id="Seg_4757" n="e" s="T544">ɨtaːma. </ts>
            </ts>
            <ts e="T551" id="Seg_4758" n="sc" s="T546">
               <ts e="T547" id="Seg_4760" n="e" s="T546">Bejeŋ </ts>
               <ts e="T548" id="Seg_4762" n="e" s="T547">ɨgɨrbɨtɨŋ", </ts>
               <ts e="T549" id="Seg_4764" n="e" s="T548">diːr, </ts>
               <ts e="T550" id="Seg_4766" n="e" s="T549">"kihi </ts>
               <ts e="T551" id="Seg_4768" n="e" s="T550">külü͡ök." </ts>
            </ts>
            <ts e="T558" id="Seg_4769" n="sc" s="T552">
               <ts e="T553" id="Seg_4771" n="e" s="T552">Ontuŋ </ts>
               <ts e="T554" id="Seg_4773" n="e" s="T553">emi͡e </ts>
               <ts e="T555" id="Seg_4775" n="e" s="T554">baran </ts>
               <ts e="T556" id="Seg_4777" n="e" s="T555">kaːlla </ts>
               <ts e="T557" id="Seg_4779" n="e" s="T556">ol </ts>
               <ts e="T558" id="Seg_4781" n="e" s="T557">tugukka. </ts>
            </ts>
            <ts e="T577" id="Seg_4782" n="sc" s="T559">
               <ts e="T560" id="Seg_4784" n="e" s="T559">De </ts>
               <ts e="T561" id="Seg_4786" n="e" s="T560">bolše </ts>
               <ts e="T562" id="Seg_4788" n="e" s="T561">kimneːbetegim </ts>
               <ts e="T563" id="Seg_4790" n="e" s="T562">ol </ts>
               <ts e="T564" id="Seg_4792" n="e" s="T563">bi͡ek </ts>
               <ts e="T565" id="Seg_4794" n="e" s="T564">kün </ts>
               <ts e="T566" id="Seg_4796" n="e" s="T565">eːje </ts>
               <ts e="T567" id="Seg_4798" n="e" s="T566">oːnnʼoːčču </ts>
               <ts e="T568" id="Seg_4800" n="e" s="T567">ol </ts>
               <ts e="T569" id="Seg_4802" n="e" s="T568">tugutu </ts>
               <ts e="T570" id="Seg_4804" n="e" s="T569">gɨtta </ts>
               <ts e="T571" id="Seg_4806" n="e" s="T570">tu͡okka </ts>
               <ts e="T572" id="Seg_4808" n="e" s="T571">bagarbɨt </ts>
               <ts e="T573" id="Seg_4810" n="e" s="T572">imenno </ts>
               <ts e="T574" id="Seg_4812" n="e" s="T573">iti </ts>
               <ts e="T575" id="Seg_4814" n="e" s="T574">tugut </ts>
               <ts e="T576" id="Seg_4816" n="e" s="T575">bu͡olu͡ogun </ts>
               <ts e="T577" id="Seg_4818" n="e" s="T576">naːda. </ts>
            </ts>
            <ts e="T583" id="Seg_4819" n="sc" s="T578">
               <ts e="T579" id="Seg_4821" n="e" s="T578">De </ts>
               <ts e="T580" id="Seg_4823" n="e" s="T579">onton </ts>
               <ts e="T581" id="Seg_4825" n="e" s="T580">tahaːrabɨt </ts>
               <ts e="T582" id="Seg_4827" n="e" s="T581">karaŋarda </ts>
               <ts e="T583" id="Seg_4829" n="e" s="T582">bu͡o. </ts>
            </ts>
            <ts e="T589" id="Seg_4830" n="sc" s="T584">
               <ts e="T585" id="Seg_4832" n="e" s="T584">Bosku͡ojɨn </ts>
               <ts e="T586" id="Seg_4834" n="e" s="T585">da </ts>
               <ts e="T587" id="Seg_4836" n="e" s="T586">kimmit </ts>
               <ts e="T588" id="Seg_4838" n="e" s="T587">ohokput </ts>
               <ts e="T589" id="Seg_4840" n="e" s="T588">ke. </ts>
            </ts>
            <ts e="T593" id="Seg_4841" n="sc" s="T590">
               <ts e="T591" id="Seg_4843" n="e" s="T590">Maːmam </ts>
               <ts e="T592" id="Seg_4845" n="e" s="T591">delbi </ts>
               <ts e="T593" id="Seg_4847" n="e" s="T592">kaːlaːbɨt. </ts>
            </ts>
            <ts e="T604" id="Seg_4848" n="sc" s="T594">
               <ts e="T595" id="Seg_4850" n="e" s="T594">Ps </ts>
               <ts e="T596" id="Seg_4852" n="e" s="T595">ps </ts>
               <ts e="T597" id="Seg_4854" n="e" s="T596">ps </ts>
               <ts e="T598" id="Seg_4856" n="e" s="T597">kim </ts>
               <ts e="T599" id="Seg_4858" n="e" s="T598">mas </ts>
               <ts e="T600" id="Seg_4860" n="e" s="T599">ke </ts>
               <ts e="T601" id="Seg_4862" n="e" s="T600">ubajara </ts>
               <ts e="T602" id="Seg_4864" n="e" s="T601">ke </ts>
               <ts e="T603" id="Seg_4866" n="e" s="T602">kɨtarčɨ </ts>
               <ts e="T604" id="Seg_4868" n="e" s="T603">ohopput. </ts>
            </ts>
            <ts e="T611" id="Seg_4869" n="sc" s="T605">
               <ts e="T606" id="Seg_4871" n="e" s="T605">Čajniktar </ts>
               <ts e="T607" id="Seg_4873" n="e" s="T606">kötüten </ts>
               <ts e="T608" id="Seg_4875" n="e" s="T607">voːpsetɨn </ts>
               <ts e="T609" id="Seg_4877" n="e" s="T608">telibireːktiːller </ts>
               <ts e="T610" id="Seg_4879" n="e" s="T609">di͡en </ts>
               <ts e="T611" id="Seg_4881" n="e" s="T610">kappaktara. </ts>
            </ts>
            <ts e="T618" id="Seg_4882" n="sc" s="T612">
               <ts e="T613" id="Seg_4884" n="e" s="T612">Kü͡öspüt </ts>
               <ts e="T614" id="Seg_4886" n="e" s="T613">laglɨ͡a </ts>
               <ts e="T615" id="Seg_4888" n="e" s="T614">turaːktɨːr </ts>
               <ts e="T616" id="Seg_4890" n="e" s="T615">di͡en </ts>
               <ts e="T617" id="Seg_4892" n="e" s="T616">laglɨ͡a, </ts>
               <ts e="T618" id="Seg_4894" n="e" s="T617">voːpse. </ts>
            </ts>
            <ts e="T625" id="Seg_4895" n="sc" s="T619">
               <ts e="T620" id="Seg_4897" n="e" s="T619">(Itij-) </ts>
               <ts e="T621" id="Seg_4899" n="e" s="T620">aːn </ts>
               <ts e="T622" id="Seg_4901" n="e" s="T621">tünnügü </ts>
               <ts e="T623" id="Seg_4903" n="e" s="T622">arɨjda </ts>
               <ts e="T624" id="Seg_4905" n="e" s="T623">ü͡ölehe </ts>
               <ts e="T625" id="Seg_4907" n="e" s="T624">arɨjda. </ts>
            </ts>
            <ts e="T636" id="Seg_4908" n="sc" s="T626">
               <ts e="T627" id="Seg_4910" n="e" s="T626">Ü͡öles </ts>
               <ts e="T628" id="Seg_4912" n="e" s="T627">bu͡olaːččɨ </ts>
               <ts e="T629" id="Seg_4914" n="e" s="T628">kim </ts>
               <ts e="T630" id="Seg_4916" n="e" s="T629">bü͡öleːččiler </ts>
               <ts e="T631" id="Seg_4918" n="e" s="T630">nu </ts>
               <ts e="T632" id="Seg_4920" n="e" s="T631">štobɨ </ts>
               <ts e="T633" id="Seg_4922" n="e" s="T632">veter </ts>
               <ts e="T634" id="Seg_4924" n="e" s="T633">tɨ͡al </ts>
               <ts e="T635" id="Seg_4926" n="e" s="T634">kiːri͡egin </ts>
               <ts e="T636" id="Seg_4928" n="e" s="T635">ke. </ts>
            </ts>
            <ts e="T641" id="Seg_4929" n="sc" s="T637">
               <ts e="T638" id="Seg_4931" n="e" s="T637">De </ts>
               <ts e="T639" id="Seg_4933" n="e" s="T638">ol </ts>
               <ts e="T640" id="Seg_4935" n="e" s="T639">arɨjda </ts>
               <ts e="T641" id="Seg_4937" n="e" s="T640">heː. </ts>
            </ts>
            <ts e="T650" id="Seg_4938" n="sc" s="T642">
               <ts e="T643" id="Seg_4940" n="e" s="T642">Min </ts>
               <ts e="T644" id="Seg_4942" n="e" s="T643">du </ts>
               <ts e="T645" id="Seg_4944" n="e" s="T644">hürdeːk </ts>
               <ts e="T646" id="Seg_4946" n="e" s="T645">bagaraːččɨbɨn </ts>
               <ts e="T647" id="Seg_4948" n="e" s="T646">tünnükke </ts>
               <ts e="T648" id="Seg_4950" n="e" s="T647">tünnükke </ts>
               <ts e="T649" id="Seg_4952" n="e" s="T648">körü͡öppün </ts>
               <ts e="T650" id="Seg_4954" n="e" s="T649">ke. </ts>
            </ts>
            <ts e="T654" id="Seg_4955" n="sc" s="T651">
               <ts e="T652" id="Seg_4957" n="e" s="T651">Mɨlaja </ts>
               <ts e="T653" id="Seg_4959" n="e" s="T652">mɨlaja </ts>
               <ts e="T654" id="Seg_4961" n="e" s="T653">oloroːččubun. </ts>
            </ts>
            <ts e="T661" id="Seg_4962" n="sc" s="T655">
               <ts e="T656" id="Seg_4964" n="e" s="T655">Bosku͡ojin </ts>
               <ts e="T657" id="Seg_4966" n="e" s="T656">da </ts>
               <ts e="T658" id="Seg_4968" n="e" s="T657">i͡e </ts>
               <ts e="T659" id="Seg_4970" n="e" s="T658">kim, </ts>
               <ts e="T660" id="Seg_4972" n="e" s="T659">ɨj </ts>
               <ts e="T661" id="Seg_4974" n="e" s="T660">hahaha. </ts>
            </ts>
            <ts e="T666" id="Seg_4975" n="sc" s="T662">
               <ts e="T663" id="Seg_4977" n="e" s="T662">Ɨj </ts>
               <ts e="T664" id="Seg_4979" n="e" s="T663">erejkeːn </ts>
               <ts e="T665" id="Seg_4981" n="e" s="T664">ebite </ts>
               <ts e="T666" id="Seg_4983" n="e" s="T665">eh. </ts>
            </ts>
            <ts e="T672" id="Seg_4984" n="sc" s="T667">
               <ts e="T668" id="Seg_4986" n="e" s="T667">Ɨjbɨt </ts>
               <ts e="T669" id="Seg_4988" n="e" s="T668">bosku͡oj </ts>
               <ts e="T670" id="Seg_4990" n="e" s="T669">bagajɨ </ts>
               <ts e="T671" id="Seg_4992" n="e" s="T670">no </ts>
               <ts e="T672" id="Seg_4994" n="e" s="T671">kap-karaŋa. </ts>
            </ts>
            <ts e="T680" id="Seg_4995" n="sc" s="T673">
               <ts e="T674" id="Seg_4997" n="e" s="T673">Tu͡oktar </ts>
               <ts e="T675" id="Seg_4999" n="e" s="T674">kötöːččüler </ts>
               <ts e="T676" id="Seg_5001" n="e" s="T675">ebit </ts>
               <ts e="T677" id="Seg_5003" n="e" s="T676">min </ts>
               <ts e="T678" id="Seg_5005" n="e" s="T677">že </ts>
               <ts e="T679" id="Seg_5007" n="e" s="T678">bilbet </ts>
               <ts e="T680" id="Seg_5009" n="e" s="T679">etim. </ts>
            </ts>
            <ts e="T687" id="Seg_5010" n="sc" s="T681">
               <ts e="T682" id="Seg_5012" n="e" s="T681">Leŋkej </ts>
               <ts e="T683" id="Seg_5014" n="e" s="T682">kördük </ts>
               <ts e="T684" id="Seg_5016" n="e" s="T683">leŋkejdar </ts>
               <ts e="T685" id="Seg_5018" n="e" s="T684">di͡en </ts>
               <ts e="T686" id="Seg_5020" n="e" s="T685">hu͡oga </ts>
               <ts e="T687" id="Seg_5022" n="e" s="T686">küččügüjdar. </ts>
            </ts>
            <ts e="T696" id="Seg_5023" n="sc" s="T688">
               <ts e="T689" id="Seg_5025" n="e" s="T688">Menʼiːte </ts>
               <ts e="T690" id="Seg_5027" n="e" s="T689">hu͡oktar </ts>
               <ts e="T691" id="Seg_5029" n="e" s="T690">di͡eččibin </ts>
               <ts e="T692" id="Seg_5031" n="e" s="T691">menʼiːte </ts>
               <ts e="T693" id="Seg_5033" n="e" s="T692">hu͡oktar </ts>
               <ts e="T694" id="Seg_5035" n="e" s="T693">hɨldʼallar </ts>
               <ts e="T695" id="Seg_5037" n="e" s="T694">maːma </ts>
               <ts e="T696" id="Seg_5039" n="e" s="T695">kör. </ts>
            </ts>
            <ts e="T707" id="Seg_5040" n="sc" s="T697">
               <ts e="T698" id="Seg_5042" n="e" s="T697">Okazɨvaetsa </ts>
               <ts e="T699" id="Seg_5044" n="e" s="T698">iti </ts>
               <ts e="T700" id="Seg_5046" n="e" s="T699">kim </ts>
               <ts e="T701" id="Seg_5048" n="e" s="T700">eh </ts>
               <ts e="T702" id="Seg_5050" n="e" s="T701">bu </ts>
               <ts e="T703" id="Seg_5052" n="e" s="T702">čok </ts>
               <ts e="T704" id="Seg_5054" n="e" s="T703">kötör </ts>
               <ts e="T705" id="Seg_5056" n="e" s="T704">bu͡o </ts>
               <ts e="T706" id="Seg_5058" n="e" s="T705">trubatan </ts>
               <ts e="T707" id="Seg_5060" n="e" s="T706">ke. </ts>
            </ts>
            <ts e="T714" id="Seg_5061" n="sc" s="T708">
               <ts e="T709" id="Seg_5063" n="e" s="T708">Onuga </ts>
               <ts e="T710" id="Seg_5065" n="e" s="T709">möŋsöllör </ts>
               <ts e="T711" id="Seg_5067" n="e" s="T710">ebit </ts>
               <ts e="T712" id="Seg_5069" n="e" s="T711">itileriŋ </ts>
               <ts e="T713" id="Seg_5071" n="e" s="T712">interes </ts>
               <ts e="T714" id="Seg_5073" n="e" s="T713">bagajɨ. </ts>
            </ts>
            <ts e="T723" id="Seg_5074" n="sc" s="T715">
               <ts e="T716" id="Seg_5076" n="e" s="T715">Onton </ts>
               <ts e="T717" id="Seg_5078" n="e" s="T716">töhö </ts>
               <ts e="T718" id="Seg_5080" n="e" s="T717">eme </ts>
               <ts e="T719" id="Seg_5082" n="e" s="T718">maːmam </ts>
               <ts e="T720" id="Seg_5084" n="e" s="T719">(iːsten-) </ts>
               <ts e="T721" id="Seg_5086" n="e" s="T720">oloror, </ts>
               <ts e="T722" id="Seg_5088" n="e" s="T721">(iːsten-) </ts>
               <ts e="T723" id="Seg_5090" n="e" s="T722">oloror. </ts>
            </ts>
            <ts e="T731" id="Seg_5091" n="sc" s="T724">
               <ts e="T725" id="Seg_5093" n="e" s="T724">Orguːjakaːn </ts>
               <ts e="T726" id="Seg_5095" n="e" s="T725">orguːjakaːn </ts>
               <ts e="T727" id="Seg_5097" n="e" s="T726">diː </ts>
               <ts e="T728" id="Seg_5099" n="e" s="T727">diː, </ts>
               <ts e="T729" id="Seg_5101" n="e" s="T728">ɨllaːn </ts>
               <ts e="T730" id="Seg_5103" n="e" s="T729">oloror </ts>
               <ts e="T731" id="Seg_5105" n="e" s="T730">orguːjak. </ts>
            </ts>
            <ts e="T734" id="Seg_5106" n="sc" s="T732">
               <ts e="T733" id="Seg_5108" n="e" s="T732">Djakubɨt </ts>
               <ts e="T734" id="Seg_5110" n="e" s="T733">hɨtar. </ts>
            </ts>
            <ts e="T738" id="Seg_5111" n="sc" s="T735">
               <ts e="T736" id="Seg_5113" n="e" s="T735">Nʼik </ts>
               <ts e="T737" id="Seg_5115" n="e" s="T736">da </ts>
               <ts e="T738" id="Seg_5117" n="e" s="T737">gɨmmat. </ts>
            </ts>
            <ts e="T743" id="Seg_5118" n="sc" s="T739">
               <ts e="T740" id="Seg_5120" n="e" s="T739">Hɨlajbɨt </ts>
               <ts e="T741" id="Seg_5122" n="e" s="T740">bɨhɨlak </ts>
               <ts e="T742" id="Seg_5124" n="e" s="T741">tugutun </ts>
               <ts e="T743" id="Seg_5126" n="e" s="T742">gɨtta. </ts>
            </ts>
            <ts e="T750" id="Seg_5127" n="sc" s="T744">
               <ts e="T745" id="Seg_5129" n="e" s="T744">Onton </ts>
               <ts e="T746" id="Seg_5131" n="e" s="T745">kim </ts>
               <ts e="T747" id="Seg_5133" n="e" s="T746">vremja </ts>
               <ts e="T748" id="Seg_5135" n="e" s="T747">vremja </ts>
               <ts e="T749" id="Seg_5137" n="e" s="T748">barda </ts>
               <ts e="T750" id="Seg_5139" n="e" s="T749">bu͡o. </ts>
            </ts>
            <ts e="T762" id="Seg_5140" n="sc" s="T751">
               <ts e="T752" id="Seg_5142" n="e" s="T751">Poka </ts>
               <ts e="T753" id="Seg_5144" n="e" s="T752">ol </ts>
               <ts e="T754" id="Seg_5146" n="e" s="T753">tu͡ok </ts>
               <ts e="T755" id="Seg_5148" n="e" s="T754">itte </ts>
               <ts e="T756" id="Seg_5150" n="e" s="T755">bütte </ts>
               <ts e="T757" id="Seg_5152" n="e" s="T756">min </ts>
               <ts e="T758" id="Seg_5154" n="e" s="T757">ješo </ts>
               <ts e="T759" id="Seg_5156" n="e" s="T758">teːtem </ts>
               <ts e="T760" id="Seg_5158" n="e" s="T759">hu͡okkaːnɨnan, </ts>
               <ts e="T761" id="Seg_5160" n="e" s="T760">radiona </ts>
               <ts e="T762" id="Seg_5162" n="e" s="T761">vklučajdɨnabɨn. </ts>
            </ts>
            <ts e="T766" id="Seg_5163" n="sc" s="T763">
               <ts e="T764" id="Seg_5165" n="e" s="T763">Heee </ts>
               <ts e="T765" id="Seg_5167" n="e" s="T764">gaːgɨnataːktɨːbɨn </ts>
               <ts e="T766" id="Seg_5169" n="e" s="T765">de. </ts>
            </ts>
            <ts e="T772" id="Seg_5170" n="sc" s="T767">
               <ts e="T768" id="Seg_5172" n="e" s="T767">"Eː </ts>
               <ts e="T769" id="Seg_5174" n="e" s="T768">orguːj </ts>
               <ts e="T770" id="Seg_5176" n="e" s="T769">gɨn </ts>
               <ts e="T771" id="Seg_5178" n="e" s="T770">beji </ts>
               <ts e="T772" id="Seg_5180" n="e" s="T771">talɨgɨratɨma." </ts>
            </ts>
            <ts e="T777" id="Seg_5181" n="sc" s="T773">
               <ts e="T774" id="Seg_5183" n="e" s="T773">Ɨrɨ͡alarɨ </ts>
               <ts e="T775" id="Seg_5185" n="e" s="T774">ihilliː </ts>
               <ts e="T776" id="Seg_5187" n="e" s="T775">hɨtɨ͡am </ts>
               <ts e="T777" id="Seg_5189" n="e" s="T776">bu͡o. </ts>
            </ts>
            <ts e="T786" id="Seg_5190" n="sc" s="T778">
               <ts e="T779" id="Seg_5192" n="e" s="T778">Henʼooo </ts>
               <ts e="T780" id="Seg_5194" n="e" s="T779">kimneri </ts>
               <ts e="T781" id="Seg_5196" n="e" s="T780">di͡e </ts>
               <ts e="T782" id="Seg_5198" n="e" s="T781">ihilliːbin </ts>
               <ts e="T783" id="Seg_5200" n="e" s="T782">okazɨvaetsa </ts>
               <ts e="T784" id="Seg_5202" n="e" s="T783">Kitajetsardarlar </ts>
               <ts e="T785" id="Seg_5204" n="e" s="T784">tu͡oktar </ts>
               <ts e="T786" id="Seg_5206" n="e" s="T785">du. </ts>
            </ts>
            <ts e="T803" id="Seg_5207" n="sc" s="T787">
               <ts e="T788" id="Seg_5209" n="e" s="T787">Oloru </ts>
               <ts e="T789" id="Seg_5211" n="e" s="T788">ihillen </ts>
               <ts e="T790" id="Seg_5213" n="e" s="T789">bu͡ola </ts>
               <ts e="T791" id="Seg_5215" n="e" s="T790">hɨtaːktɨːbɨn </ts>
               <ts e="T792" id="Seg_5217" n="e" s="T791">di͡en </ts>
               <ts e="T793" id="Seg_5219" n="e" s="T792">oččogo, </ts>
               <ts e="T794" id="Seg_5221" n="e" s="T793">beseleː </ts>
               <ts e="T795" id="Seg_5223" n="e" s="T794">bagajɨ, </ts>
               <ts e="T796" id="Seg_5225" n="e" s="T795">bilebin. </ts>
               <ts e="T797" id="Seg_5227" n="e" s="T796">Onton </ts>
               <ts e="T798" id="Seg_5229" n="e" s="T797">hi͡ese </ts>
               <ts e="T799" id="Seg_5231" n="e" s="T798">nʼuːččalar </ts>
               <ts e="T800" id="Seg_5233" n="e" s="T799">bullum </ts>
               <ts e="T801" id="Seg_5235" n="e" s="T800">onton </ts>
               <ts e="T802" id="Seg_5237" n="e" s="T801">büten </ts>
               <ts e="T803" id="Seg_5239" n="e" s="T802">kaːlbɨt. </ts>
            </ts>
            <ts e="T811" id="Seg_5240" n="sc" s="T804">
               <ts e="T805" id="Seg_5242" n="e" s="T804">"De </ts>
               <ts e="T806" id="Seg_5244" n="e" s="T805">büteren </ts>
               <ts e="T807" id="Seg_5246" n="e" s="T806">keːhi͡ekke </ts>
               <ts e="T808" id="Seg_5248" n="e" s="T807">büter </ts>
               <ts e="T809" id="Seg_5250" n="e" s="T808">teːteŋ </ts>
               <ts e="T810" id="Seg_5252" n="e" s="T809">keli͡e </ts>
               <ts e="T811" id="Seg_5254" n="e" s="T810">heː." </ts>
            </ts>
            <ts e="T820" id="Seg_5255" n="sc" s="T812">
               <ts e="T813" id="Seg_5257" n="e" s="T812">Kümmüt </ts>
               <ts e="T814" id="Seg_5259" n="e" s="T813">ɨjbɨt </ts>
               <ts e="T815" id="Seg_5261" n="e" s="T814">ɨraːtta </ts>
               <ts e="T816" id="Seg_5263" n="e" s="T815">bu͡o </ts>
               <ts e="T817" id="Seg_5265" n="e" s="T816">ü͡öhe </ts>
               <ts e="T818" id="Seg_5267" n="e" s="T817">bu͡olla </ts>
               <ts e="T819" id="Seg_5269" n="e" s="T818">ama </ts>
               <ts e="T820" id="Seg_5271" n="e" s="T819">bosku͡oj. </ts>
            </ts>
            <ts e="T825" id="Seg_5272" n="sc" s="T821">
               <ts e="T822" id="Seg_5274" n="e" s="T821">Ama </ts>
               <ts e="T823" id="Seg_5276" n="e" s="T822">hɨrdɨk </ts>
               <ts e="T824" id="Seg_5278" n="e" s="T823">künüs </ts>
               <ts e="T825" id="Seg_5280" n="e" s="T824">kördük. </ts>
            </ts>
            <ts e="T837" id="Seg_5281" n="sc" s="T826">
               <ts e="T827" id="Seg_5283" n="e" s="T826">Oː </ts>
               <ts e="T828" id="Seg_5285" n="e" s="T827">tabalarbɨt </ts>
               <ts e="T829" id="Seg_5287" n="e" s="T828">voːpse </ts>
               <ts e="T830" id="Seg_5289" n="e" s="T829">ebe </ts>
               <ts e="T831" id="Seg_5291" n="e" s="T830">üstün </ts>
               <ts e="T832" id="Seg_5293" n="e" s="T831">kötüte </ts>
               <ts e="T833" id="Seg_5295" n="e" s="T832">hɨldʼaːktɨːllar </ts>
               <ts e="T834" id="Seg_5297" n="e" s="T833">de </ts>
               <ts e="T835" id="Seg_5299" n="e" s="T834">ama </ts>
               <ts e="T836" id="Seg_5301" n="e" s="T835">bosku͡ojin </ts>
               <ts e="T837" id="Seg_5303" n="e" s="T836">da. </ts>
            </ts>
            <ts e="T842" id="Seg_5304" n="sc" s="T838">
               <ts e="T839" id="Seg_5306" n="e" s="T838">Kim </ts>
               <ts e="T840" id="Seg_5308" n="e" s="T839">buru͡olara </ts>
               <ts e="T841" id="Seg_5310" n="e" s="T840">aš </ts>
               <ts e="T842" id="Seg_5312" n="e" s="T841">kötör. </ts>
            </ts>
            <ts e="T855" id="Seg_5313" n="sc" s="T843">
               <ts e="T844" id="Seg_5315" n="e" s="T843">Oj </ts>
               <ts e="T845" id="Seg_5317" n="e" s="T844">ama </ts>
               <ts e="T846" id="Seg_5319" n="e" s="T845">bosku͡oj </ts>
               <ts e="T847" id="Seg_5321" n="e" s="T846">kim </ts>
               <ts e="T848" id="Seg_5323" n="e" s="T847">ke </ts>
               <ts e="T849" id="Seg_5325" n="e" s="T848">kaːrɨŋ </ts>
               <ts e="T850" id="Seg_5327" n="e" s="T849">ke </ts>
               <ts e="T851" id="Seg_5329" n="e" s="T850">kimŋe </ts>
               <ts e="T852" id="Seg_5331" n="e" s="T851">lunaːtan </ts>
               <ts e="T853" id="Seg_5333" n="e" s="T852">ama </ts>
               <ts e="T854" id="Seg_5335" n="e" s="T853">kimniːr </ts>
               <ts e="T855" id="Seg_5337" n="e" s="T854">gɨlbaŋnɨːr. </ts>
            </ts>
            <ts e="T866" id="Seg_5338" n="sc" s="T856">
               <ts e="T857" id="Seg_5340" n="e" s="T856">Onton </ts>
               <ts e="T858" id="Seg_5342" n="e" s="T857">onton </ts>
               <ts e="T859" id="Seg_5344" n="e" s="T858">beːbe </ts>
               <ts e="T860" id="Seg_5346" n="e" s="T859">ɨppɨt </ts>
               <ts e="T861" id="Seg_5348" n="e" s="T860">mm </ts>
               <ts e="T862" id="Seg_5350" n="e" s="T861">mm </ts>
               <ts e="T863" id="Seg_5352" n="e" s="T862">gɨnar </ts>
               <ts e="T864" id="Seg_5354" n="e" s="T863">bu </ts>
               <ts e="T865" id="Seg_5356" n="e" s="T864">kimniːr </ts>
               <ts e="T866" id="Seg_5358" n="e" s="T865">muŋurguːr. </ts>
            </ts>
            <ts e="T872" id="Seg_5359" n="sc" s="T867">
               <ts e="T868" id="Seg_5361" n="e" s="T867">Beː </ts>
               <ts e="T869" id="Seg_5363" n="e" s="T868">ču͡oraːn </ts>
               <ts e="T870" id="Seg_5365" n="e" s="T869">tɨ͡ahaːta </ts>
               <ts e="T871" id="Seg_5367" n="e" s="T870">araj, </ts>
               <ts e="T872" id="Seg_5369" n="e" s="T871">küpüleːn. </ts>
            </ts>
            <ts e="T880" id="Seg_5370" n="sc" s="T873">
               <ts e="T874" id="Seg_5372" n="e" s="T873">Teleŋ </ts>
               <ts e="T875" id="Seg_5374" n="e" s="T874">teleŋ </ts>
               <ts e="T876" id="Seg_5376" n="e" s="T875">teleŋ </ts>
               <ts e="T877" id="Seg_5378" n="e" s="T876">teːtebit </ts>
               <ts e="T878" id="Seg_5380" n="e" s="T877">iher </ts>
               <ts e="T879" id="Seg_5382" n="e" s="T878">teːtebit </ts>
               <ts e="T880" id="Seg_5384" n="e" s="T879">heː. </ts>
            </ts>
            <ts e="T884" id="Seg_5385" n="sc" s="T881">
               <ts e="T882" id="Seg_5387" n="e" s="T881">De </ts>
               <ts e="T883" id="Seg_5389" n="e" s="T882">ol </ts>
               <ts e="T884" id="Seg_5391" n="e" s="T883">kelle. </ts>
            </ts>
            <ts e="T902" id="Seg_5392" n="sc" s="T885">
               <ts e="T886" id="Seg_5394" n="e" s="T885">Min </ts>
               <ts e="T887" id="Seg_5396" n="e" s="T886">du </ts>
               <ts e="T888" id="Seg_5398" n="e" s="T887">bilbetegim </ts>
               <ts e="T889" id="Seg_5400" n="e" s="T888">bu͡o </ts>
               <ts e="T890" id="Seg_5402" n="e" s="T889">mastana </ts>
               <ts e="T891" id="Seg_5404" n="e" s="T890">barbɨt </ts>
               <ts e="T892" id="Seg_5406" n="e" s="T891">diːbin </ts>
               <ts e="T893" id="Seg_5408" n="e" s="T892">bu͡o </ts>
               <ts e="T894" id="Seg_5410" n="e" s="T893">okazɨvaetsa </ts>
               <ts e="T895" id="Seg_5412" n="e" s="T894">giniŋ </ts>
               <ts e="T896" id="Seg_5414" n="e" s="T895">bu͡o </ts>
               <ts e="T897" id="Seg_5416" n="e" s="T896">Dudinka </ts>
               <ts e="T898" id="Seg_5418" n="e" s="T897">di͡ek </ts>
               <ts e="T899" id="Seg_5420" n="e" s="T898">hɨldʼabɨt </ts>
               <ts e="T900" id="Seg_5422" n="e" s="T899">i </ts>
               <ts e="T901" id="Seg_5424" n="e" s="T900">Dujunaː </ts>
               <ts e="T902" id="Seg_5426" n="e" s="T901">di͡ek. </ts>
            </ts>
            <ts e="T906" id="Seg_5427" n="sc" s="T903">
               <ts e="T904" id="Seg_5429" n="e" s="T903">De </ts>
               <ts e="T905" id="Seg_5431" n="e" s="T904">ol </ts>
               <ts e="T906" id="Seg_5433" n="e" s="T905">kelle. </ts>
            </ts>
            <ts e="T911" id="Seg_5434" n="sc" s="T907">
               <ts e="T908" id="Seg_5436" n="e" s="T907">Uže </ts>
               <ts e="T909" id="Seg_5438" n="e" s="T908">karaŋarbɨtɨn </ts>
               <ts e="T910" id="Seg_5440" n="e" s="T909">kenne </ts>
               <ts e="T911" id="Seg_5442" n="e" s="T910">ke. </ts>
            </ts>
            <ts e="T915" id="Seg_5443" n="sc" s="T912">
               <ts e="T913" id="Seg_5445" n="e" s="T912">Oː </ts>
               <ts e="T914" id="Seg_5447" n="e" s="T913">teːtem </ts>
               <ts e="T915" id="Seg_5449" n="e" s="T914">kelle. </ts>
            </ts>
            <ts e="T926" id="Seg_5450" n="sc" s="T916">
               <ts e="T917" id="Seg_5452" n="e" s="T916">Oː </ts>
               <ts e="T918" id="Seg_5454" n="e" s="T917">maːmam </ts>
               <ts e="T919" id="Seg_5456" n="e" s="T918">kömölöhö </ts>
               <ts e="T920" id="Seg_5458" n="e" s="T919">barda </ts>
               <ts e="T921" id="Seg_5460" n="e" s="T920">hü͡örde </ts>
               <ts e="T922" id="Seg_5462" n="e" s="T921">kanʼaːta </ts>
               <ts e="T923" id="Seg_5464" n="e" s="T922">küpüleːnten. </ts>
               <ts e="T924" id="Seg_5466" n="e" s="T923">Tabalarɨn </ts>
               <ts e="T925" id="Seg_5468" n="e" s="T924">ɨːttɨlar </ts>
               <ts e="T926" id="Seg_5470" n="e" s="T925">kanʼaːtɨlar. </ts>
            </ts>
            <ts e="T931" id="Seg_5471" n="sc" s="T927">
               <ts e="T928" id="Seg_5473" n="e" s="T927">Oː </ts>
               <ts e="T929" id="Seg_5475" n="e" s="T928">teːtem </ts>
               <ts e="T930" id="Seg_5477" n="e" s="T929">mesoktardaːk </ts>
               <ts e="T931" id="Seg_5479" n="e" s="T930">kelle. </ts>
            </ts>
            <ts e="T934" id="Seg_5480" n="sc" s="T932">
               <ts e="T933" id="Seg_5482" n="e" s="T932">Küːllerin </ts>
               <ts e="T934" id="Seg_5484" n="e" s="T933">kiːllerde. </ts>
            </ts>
            <ts e="T942" id="Seg_5485" n="sc" s="T935">
               <ts e="T936" id="Seg_5487" n="e" s="T935">De </ts>
               <ts e="T937" id="Seg_5489" n="e" s="T936">kostoːtoloːto. </ts>
               <ts e="T938" id="Seg_5491" n="e" s="T937">Oː </ts>
               <ts e="T939" id="Seg_5493" n="e" s="T938">ča </ts>
               <ts e="T940" id="Seg_5495" n="e" s="T939">kempi͡et </ts>
               <ts e="T941" id="Seg_5497" n="e" s="T940">ülegerin </ts>
               <ts e="T942" id="Seg_5499" n="e" s="T941">egelbit. </ts>
            </ts>
            <ts e="T948" id="Seg_5500" n="sc" s="T943">
               <ts e="T944" id="Seg_5502" n="e" s="T943">Onton </ts>
               <ts e="T945" id="Seg_5504" n="e" s="T944">bosku͡oj </ts>
               <ts e="T946" id="Seg_5506" n="e" s="T945">bagajɨ </ts>
               <ts e="T947" id="Seg_5508" n="e" s="T946">bi͡ek </ts>
               <ts e="T948" id="Seg_5510" n="e" s="T947">öjdüːbün. </ts>
            </ts>
            <ts e="T957" id="Seg_5511" n="sc" s="T949">
               <ts e="T950" id="Seg_5513" n="e" s="T949">Kim </ts>
               <ts e="T951" id="Seg_5515" n="e" s="T950">čaːstar </ts>
               <ts e="T952" id="Seg_5517" n="e" s="T951">du͡o </ts>
               <ts e="T953" id="Seg_5519" n="e" s="T952">bosku͡oj </ts>
               <ts e="T954" id="Seg_5521" n="e" s="T953">bagajɨ </ts>
               <ts e="T955" id="Seg_5523" n="e" s="T954">kim </ts>
               <ts e="T956" id="Seg_5525" n="e" s="T955">hirkilinnen </ts>
               <ts e="T957" id="Seg_5527" n="e" s="T956">oŋohullubut. </ts>
            </ts>
            <ts e="T968" id="Seg_5528" n="sc" s="T958">
               <ts e="T959" id="Seg_5530" n="e" s="T958">Ontuŋ </ts>
               <ts e="T960" id="Seg_5532" n="e" s="T959">ješo </ts>
               <ts e="T961" id="Seg_5534" n="e" s="T960">knopkalardaːk </ts>
               <ts e="T962" id="Seg_5536" n="e" s="T961">tu͡ok </ts>
               <ts e="T963" id="Seg_5538" n="e" s="T962">ere </ts>
               <ts e="T964" id="Seg_5540" n="e" s="T963">ama </ts>
               <ts e="T965" id="Seg_5542" n="e" s="T964">bosku͡oj </ts>
               <ts e="T966" id="Seg_5544" n="e" s="T965">oduːrgaːktɨːbɨn </ts>
               <ts e="T967" id="Seg_5546" n="e" s="T966">ama </ts>
               <ts e="T968" id="Seg_5548" n="e" s="T967">bosku͡oj. </ts>
            </ts>
            <ts e="T974" id="Seg_5549" n="sc" s="T969">
               <ts e="T970" id="Seg_5551" n="e" s="T969">"De </ts>
               <ts e="T971" id="Seg_5553" n="e" s="T970">tɨːtɨma </ts>
               <ts e="T972" id="Seg_5555" n="e" s="T971">höp </ts>
               <ts e="T973" id="Seg_5557" n="e" s="T972">bu͡olu͡o", </ts>
               <ts e="T974" id="Seg_5559" n="e" s="T973">diːller. </ts>
            </ts>
            <ts e="T978" id="Seg_5560" n="sc" s="T975">
               <ts e="T976" id="Seg_5562" n="e" s="T975">Tɨːttaraːččɨta </ts>
               <ts e="T977" id="Seg_5564" n="e" s="T976">hu͡oktar </ts>
               <ts e="T978" id="Seg_5566" n="e" s="T977">bu͡o. </ts>
            </ts>
            <ts e="T982" id="Seg_5567" n="sc" s="T979">
               <ts e="T980" id="Seg_5569" n="e" s="T979">"Aldʼatɨ͡aŋ </ts>
               <ts e="T981" id="Seg_5571" n="e" s="T980">kaja </ts>
               <ts e="T982" id="Seg_5573" n="e" s="T981">heː". </ts>
            </ts>
            <ts e="T991" id="Seg_5574" n="sc" s="T983">
               <ts e="T984" id="Seg_5576" n="e" s="T983">De </ts>
               <ts e="T985" id="Seg_5578" n="e" s="T984">uːrdum </ts>
               <ts e="T986" id="Seg_5580" n="e" s="T985">hürdeːk </ts>
               <ts e="T987" id="Seg_5582" n="e" s="T986">körü͡öppün </ts>
               <ts e="T988" id="Seg_5584" n="e" s="T987">bagarabɨn </ts>
               <ts e="T989" id="Seg_5586" n="e" s="T988">tɨːtalɨ͡appɨn </ts>
               <ts e="T990" id="Seg_5588" n="e" s="T989">bagarabɨn </ts>
               <ts e="T991" id="Seg_5590" n="e" s="T990">bu͡o. </ts>
            </ts>
            <ts e="T1000" id="Seg_5591" n="sc" s="T992">
               <ts e="T993" id="Seg_5593" n="e" s="T992">Onton </ts>
               <ts e="T994" id="Seg_5595" n="e" s="T993">kim, </ts>
               <ts e="T995" id="Seg_5597" n="e" s="T994">kimi </ts>
               <ts e="T996" id="Seg_5599" n="e" s="T995">egelbite, </ts>
               <ts e="T997" id="Seg_5601" n="e" s="T996">mini͡eke </ts>
               <ts e="T998" id="Seg_5603" n="e" s="T997">podarok </ts>
               <ts e="T999" id="Seg_5605" n="e" s="T998">kompaskaːn </ts>
               <ts e="T1000" id="Seg_5607" n="e" s="T999">kuččuguj. </ts>
            </ts>
            <ts e="T1016" id="Seg_5608" n="sc" s="T1001">
               <ts e="T1002" id="Seg_5610" n="e" s="T1001">Onu </ts>
               <ts e="T1003" id="Seg_5612" n="e" s="T1002">bi͡ek </ts>
               <ts e="T1004" id="Seg_5614" n="e" s="T1003">oːnnʼuː </ts>
               <ts e="T1005" id="Seg_5616" n="e" s="T1004">hɨtaːččɨbɨn </ts>
               <ts e="T1006" id="Seg_5618" n="e" s="T1005">araː </ts>
               <ts e="T1007" id="Seg_5620" n="e" s="T1006">bosku͡ojɨn </ts>
               <ts e="T1008" id="Seg_5622" n="e" s="T1007">du͡o, </ts>
               <ts e="T1009" id="Seg_5624" n="e" s="T1008">urut </ts>
               <ts e="T1010" id="Seg_5626" n="e" s="T1009">pervɨj </ts>
               <ts e="T1011" id="Seg_5628" n="e" s="T1010">raz </ts>
               <ts e="T1012" id="Seg_5630" n="e" s="T1011">egelbite, </ts>
               <ts e="T1013" id="Seg_5632" n="e" s="T1012">bu </ts>
               <ts e="T1014" id="Seg_5634" n="e" s="T1013">korduk </ts>
               <ts e="T1015" id="Seg_5636" n="e" s="T1014">oːnnʼuːru </ts>
               <ts e="T1016" id="Seg_5638" n="e" s="T1015">ke. </ts>
            </ts>
            <ts e="T1024" id="Seg_5639" n="sc" s="T1017">
               <ts e="T1018" id="Seg_5641" n="e" s="T1017">Oː </ts>
               <ts e="T1019" id="Seg_5643" n="e" s="T1018">min </ts>
               <ts e="T1020" id="Seg_5645" n="e" s="T1019">muŋ </ts>
               <ts e="T1021" id="Seg_5647" n="e" s="T1020">hu͡ok </ts>
               <ts e="T1022" id="Seg_5649" n="e" s="T1021">voːpse </ts>
               <ts e="T1023" id="Seg_5651" n="e" s="T1022">bosku͡ojɨn </ts>
               <ts e="T1024" id="Seg_5653" n="e" s="T1023">da. </ts>
            </ts>
            <ts e="T1029" id="Seg_5654" n="sc" s="T1025">
               <ts e="T1026" id="Seg_5656" n="e" s="T1025">"Oː </ts>
               <ts e="T1027" id="Seg_5658" n="e" s="T1026">eni͡eke </ts>
               <ts e="T1028" id="Seg_5660" n="e" s="T1027">podarok", </ts>
               <ts e="T1029" id="Seg_5662" n="e" s="T1028">diːr. </ts>
            </ts>
            <ts e="T1038" id="Seg_5663" n="sc" s="T1030">
               <ts e="T1031" id="Seg_5665" n="e" s="T1030">De </ts>
               <ts e="T1032" id="Seg_5667" n="e" s="T1031">ol </ts>
               <ts e="T1033" id="Seg_5669" n="e" s="T1032">kepseteller </ts>
               <ts e="T1034" id="Seg_5671" n="e" s="T1033">eni </ts>
               <ts e="T1035" id="Seg_5673" n="e" s="T1034">ke, </ts>
               <ts e="T1036" id="Seg_5675" n="e" s="T1035">min </ts>
               <ts e="T1037" id="Seg_5677" n="e" s="T1036">utujan </ts>
               <ts e="T1038" id="Seg_5679" n="e" s="T1037">kaːlbɨppɨn. </ts>
            </ts>
            <ts e="T1047" id="Seg_5680" n="sc" s="T1039">
               <ts e="T1040" id="Seg_5682" n="e" s="T1039">Harsɨ͡arda </ts>
               <ts e="T1041" id="Seg_5684" n="e" s="T1040">uže </ts>
               <ts e="T1042" id="Seg_5686" n="e" s="T1041">uhuktabɨn </ts>
               <ts e="T1043" id="Seg_5688" n="e" s="T1042">oː </ts>
               <ts e="T1044" id="Seg_5690" n="e" s="T1043">ohok </ts>
               <ts e="T1045" id="Seg_5692" n="e" s="T1044">emi͡e </ts>
               <ts e="T1046" id="Seg_5694" n="e" s="T1045">kötüte </ts>
               <ts e="T1047" id="Seg_5696" n="e" s="T1046">turar. </ts>
            </ts>
            <ts e="T1055" id="Seg_5697" n="sc" s="T1048">
               <ts e="T1049" id="Seg_5699" n="e" s="T1048">Bosku͡ojɨn </ts>
               <ts e="T1050" id="Seg_5701" n="e" s="T1049">du͡o </ts>
               <ts e="T1051" id="Seg_5703" n="e" s="T1050">kim </ts>
               <ts e="T1052" id="Seg_5705" n="e" s="T1051">da </ts>
               <ts e="T1053" id="Seg_5707" n="e" s="T1052">hu͡ok </ts>
               <ts e="T1054" id="Seg_5709" n="e" s="T1053">dʼi͡ebit </ts>
               <ts e="T1055" id="Seg_5711" n="e" s="T1054">ihiger. </ts>
            </ts>
            <ts e="T1060" id="Seg_5712" n="sc" s="T1056">
               <ts e="T1057" id="Seg_5714" n="e" s="T1056">Ol </ts>
               <ts e="T1058" id="Seg_5716" n="e" s="T1057">tabalarɨn </ts>
               <ts e="T1059" id="Seg_5718" n="e" s="T1058">tuttallar </ts>
               <ts e="T1060" id="Seg_5720" n="e" s="T1059">ebit. </ts>
            </ts>
            <ts e="T1063" id="Seg_5721" n="sc" s="T1061">
               <ts e="T1062" id="Seg_5723" n="e" s="T1061">Tutta </ts>
               <ts e="T1063" id="Seg_5725" n="e" s="T1062">taksɨbɨttar. </ts>
            </ts>
            <ts e="T1071" id="Seg_5726" n="sc" s="T1064">
               <ts e="T1065" id="Seg_5728" n="e" s="T1064">Ɨ͡allardaːk </ts>
               <ts e="T1066" id="Seg_5730" n="e" s="T1065">etibit </ts>
               <ts e="T1067" id="Seg_5732" n="e" s="T1066">tak </ts>
               <ts e="T1068" id="Seg_5734" n="e" s="T1067">da, </ts>
               <ts e="T1069" id="Seg_5736" n="e" s="T1068">baːr </ts>
               <ts e="T1070" id="Seg_5738" n="e" s="T1069">etilere </ts>
               <ts e="T1071" id="Seg_5740" n="e" s="T1070">ɨ͡allarbɨt. </ts>
            </ts>
            <ts e="T1084" id="Seg_5741" n="sc" s="T1072">
               <ts e="T1073" id="Seg_5743" n="e" s="T1072">De </ts>
               <ts e="T1074" id="Seg_5745" n="e" s="T1073">ol </ts>
               <ts e="T1075" id="Seg_5747" n="e" s="T1074">komujdular </ts>
               <ts e="T1076" id="Seg_5749" n="e" s="T1075">ü͡örderin, </ts>
               <ts e="T1077" id="Seg_5751" n="e" s="T1076">komunallar </ts>
               <ts e="T1078" id="Seg_5753" n="e" s="T1077">taba </ts>
               <ts e="T1079" id="Seg_5755" n="e" s="T1078">tu͡ok, </ts>
               <ts e="T1080" id="Seg_5757" n="e" s="T1079">köhöbüt </ts>
               <ts e="T1081" id="Seg_5759" n="e" s="T1080">bɨhɨlak, </ts>
               <ts e="T1082" id="Seg_5761" n="e" s="T1081">öjdüːbün </ts>
               <ts e="T1083" id="Seg_5763" n="e" s="T1082">kanna </ts>
               <ts e="T1084" id="Seg_5765" n="e" s="T1083">ere. </ts>
            </ts>
            <ts e="T1090" id="Seg_5766" n="sc" s="T1085">
               <ts e="T1086" id="Seg_5768" n="e" s="T1085">Oː </ts>
               <ts e="T1087" id="Seg_5770" n="e" s="T1086">de </ts>
               <ts e="T1088" id="Seg_5772" n="e" s="T1087">minigin </ts>
               <ts e="T1089" id="Seg_5774" n="e" s="T1088">taŋɨnnaran </ts>
               <ts e="T1090" id="Seg_5776" n="e" s="T1089">čičigeretiler. </ts>
            </ts>
            <ts e="T1096" id="Seg_5777" n="sc" s="T1091">
               <ts e="T1092" id="Seg_5779" n="e" s="T1091">Hɨrgaga </ts>
               <ts e="T1093" id="Seg_5781" n="e" s="T1092">baːjan </ts>
               <ts e="T1094" id="Seg_5783" n="e" s="T1093">keːspitter </ts>
               <ts e="T1095" id="Seg_5785" n="e" s="T1094">önülen </ts>
               <ts e="T1096" id="Seg_5787" n="e" s="T1095">keːspitter. </ts>
            </ts>
            <ts e="T1100" id="Seg_5788" n="sc" s="T1097">
               <ts e="T1098" id="Seg_5790" n="e" s="T1097">De </ts>
               <ts e="T1099" id="Seg_5792" n="e" s="T1098">kajdi͡ek </ts>
               <ts e="T1100" id="Seg_5794" n="e" s="T1099">barabɨt? </ts>
            </ts>
            <ts e="T1107" id="Seg_5795" n="sc" s="T1101">
               <ts e="T1102" id="Seg_5797" n="e" s="T1101">I </ts>
               <ts e="T1103" id="Seg_5799" n="e" s="T1102">kennibitten </ts>
               <ts e="T1104" id="Seg_5801" n="e" s="T1103">du͡o </ts>
               <ts e="T1105" id="Seg_5803" n="e" s="T1104">tabalarɨ </ts>
               <ts e="T1106" id="Seg_5805" n="e" s="T1105">hi͡eteːččiler </ts>
               <ts e="T1107" id="Seg_5807" n="e" s="T1106">kosobu͡ojga. </ts>
            </ts>
            <ts e="T1114" id="Seg_5808" n="sc" s="T1108">
               <ts e="T1109" id="Seg_5810" n="e" s="T1108">Astaːk </ts>
               <ts e="T1110" id="Seg_5812" n="e" s="T1109">ili </ts>
               <ts e="T1111" id="Seg_5814" n="e" s="T1110">mastaːk </ts>
               <ts e="T1112" id="Seg_5816" n="e" s="T1111">kosobu͡oj </ts>
               <ts e="T1113" id="Seg_5818" n="e" s="T1112">hi͡eteːčči </ts>
               <ts e="T1114" id="Seg_5820" n="e" s="T1113">etibit. </ts>
            </ts>
            <ts e="T1120" id="Seg_5821" n="sc" s="T1115">
               <ts e="T1116" id="Seg_5823" n="e" s="T1115">Olorton </ts>
               <ts e="T1117" id="Seg_5825" n="e" s="T1116">kuttanan </ts>
               <ts e="T1118" id="Seg_5827" n="e" s="T1117">ɨtaːn </ts>
               <ts e="T1119" id="Seg_5829" n="e" s="T1118">mörüleːktiːbin </ts>
               <ts e="T1120" id="Seg_5831" n="e" s="T1119">di͡en. </ts>
            </ts>
            <ts e="T1124" id="Seg_5832" n="sc" s="T1121">
               <ts e="T1122" id="Seg_5834" n="e" s="T1121">"Kuttanɨma </ts>
               <ts e="T1123" id="Seg_5836" n="e" s="T1122">ɨtɨrɨ͡a </ts>
               <ts e="T1124" id="Seg_5838" n="e" s="T1123">hu͡oga." </ts>
            </ts>
            <ts e="T1134" id="Seg_5839" n="sc" s="T1125">
               <ts e="T1126" id="Seg_5841" n="e" s="T1125">Bu </ts>
               <ts e="T1127" id="Seg_5843" n="e" s="T1126">ke </ts>
               <ts e="T1128" id="Seg_5845" n="e" s="T1127">hɨlajbɨttar </ts>
               <ts e="T1129" id="Seg_5847" n="e" s="T1128">ke </ts>
               <ts e="T1130" id="Seg_5849" n="e" s="T1129">tɨːnallar </ts>
               <ts e="T1131" id="Seg_5851" n="e" s="T1130">prjamo </ts>
               <ts e="T1132" id="Seg_5853" n="e" s="T1131">prjamo </ts>
               <ts e="T1133" id="Seg_5855" n="e" s="T1132">hɨrajbar </ts>
               <ts e="T1134" id="Seg_5857" n="e" s="T1133">munna. </ts>
            </ts>
            <ts e="T1149" id="Seg_5858" n="sc" s="T1135">
               <ts e="T1136" id="Seg_5860" n="e" s="T1135">De </ts>
               <ts e="T1137" id="Seg_5862" n="e" s="T1136">ol </ts>
               <ts e="T1138" id="Seg_5864" n="e" s="T1137">iti </ts>
               <ts e="T1139" id="Seg_5866" n="e" s="T1138">korduk </ts>
               <ts e="T1140" id="Seg_5868" n="e" s="T1139">köhö </ts>
               <ts e="T1141" id="Seg_5870" n="e" s="T1140">hɨldʼaːččɨ </ts>
               <ts e="T1142" id="Seg_5872" n="e" s="T1141">etibit, </ts>
               <ts e="T1143" id="Seg_5874" n="e" s="T1142">köhö </ts>
               <ts e="T1144" id="Seg_5876" n="e" s="T1143">hɨldʼaːččɨ, </ts>
               <ts e="T1145" id="Seg_5878" n="e" s="T1144">köhüse </ts>
               <ts e="T1146" id="Seg_5880" n="e" s="T1145">hɨldʼaːččɨ </ts>
               <ts e="T1147" id="Seg_5882" n="e" s="T1146">etim </ts>
               <ts e="T1148" id="Seg_5884" n="e" s="T1147">ginileri </ts>
               <ts e="T1149" id="Seg_5886" n="e" s="T1148">gɨtta. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T11" id="Seg_5887" s="T1">PoTY_2009_Aku_nar.001 (001)</ta>
            <ta e="T15" id="Seg_5888" s="T11">PoTY_2009_Aku_nar.002 (002)</ta>
            <ta e="T21" id="Seg_5889" s="T16">PoTY_2009_Aku_nar.003 (003)</ta>
            <ta e="T27" id="Seg_5890" s="T22">PoTY_2009_Aku_nar.004 (004)</ta>
            <ta e="T32" id="Seg_5891" s="T28">PoTY_2009_Aku_nar.005 (005)</ta>
            <ta e="T52" id="Seg_5892" s="T33">PoTY_2009_Aku_nar.006 (006)</ta>
            <ta e="T60" id="Seg_5893" s="T53">PoTY_2009_Aku_nar.007 (007)</ta>
            <ta e="T72" id="Seg_5894" s="T61">PoTY_2009_Aku_nar.008 (008)</ta>
            <ta e="T76" id="Seg_5895" s="T73">PoTY_2009_Aku_nar.009 (009)</ta>
            <ta e="T79" id="Seg_5896" s="T77">PoTY_2009_Aku_nar.010 (010)</ta>
            <ta e="T83" id="Seg_5897" s="T80">PoTY_2009_Aku_nar.011 (011)</ta>
            <ta e="T86" id="Seg_5898" s="T84">PoTY_2009_Aku_nar.012 (012)</ta>
            <ta e="T91" id="Seg_5899" s="T87">PoTY_2009_Aku_nar.013 (013)</ta>
            <ta e="T105" id="Seg_5900" s="T92">PoTY_2009_Aku_nar.014 (014)</ta>
            <ta e="T112" id="Seg_5901" s="T106">PoTY_2009_Aku_nar.015 (015)</ta>
            <ta e="T118" id="Seg_5902" s="T113">PoTY_2009_Aku_nar.016 (016)</ta>
            <ta e="T126" id="Seg_5903" s="T119">PoTY_2009_Aku_nar.017 (017)</ta>
            <ta e="T134" id="Seg_5904" s="T127">PoTY_2009_Aku_nar.018 (018)</ta>
            <ta e="T142" id="Seg_5905" s="T135">PoTY_2009_Aku_nar.019 (019)</ta>
            <ta e="T152" id="Seg_5906" s="T143">PoTY_2009_Aku_nar.020 (020)</ta>
            <ta e="T160" id="Seg_5907" s="T153">PoTY_2009_Aku_nar.021 (021)</ta>
            <ta e="T169" id="Seg_5908" s="T161">PoTY_2009_Aku_nar.022 (022)</ta>
            <ta e="T174" id="Seg_5909" s="T170">PoTY_2009_Aku_nar.023 (023)</ta>
            <ta e="T180" id="Seg_5910" s="T175">PoTY_2009_Aku_nar.024 (024)</ta>
            <ta e="T190" id="Seg_5911" s="T181">PoTY_2009_Aku_nar.025 (025)</ta>
            <ta e="T196" id="Seg_5912" s="T191">PoTY_2009_Aku_nar.026 (026)</ta>
            <ta e="T208" id="Seg_5913" s="T197">PoTY_2009_Aku_nar.027 (027)</ta>
            <ta e="T217" id="Seg_5914" s="T209">PoTY_2009_Aku_nar.028 (028)</ta>
            <ta e="T231" id="Seg_5915" s="T218">PoTY_2009_Aku_nar.029 (029)</ta>
            <ta e="T243" id="Seg_5916" s="T232">PoTY_2009_Aku_nar.030 (030)</ta>
            <ta e="T249" id="Seg_5917" s="T244">PoTY_2009_Aku_nar.031 (031)</ta>
            <ta e="T253" id="Seg_5918" s="T250">PoTY_2009_Aku_nar.032 (032)</ta>
            <ta e="T257" id="Seg_5919" s="T254">PoTY_2009_Aku_nar.033 (033)</ta>
            <ta e="T263" id="Seg_5920" s="T258">PoTY_2009_Aku_nar.034 (034)</ta>
            <ta e="T266" id="Seg_5921" s="T264">PoTY_2009_Aku_nar.035 (035)</ta>
            <ta e="T271" id="Seg_5922" s="T267">PoTY_2009_Aku_nar.036 (036)</ta>
            <ta e="T275" id="Seg_5923" s="T272">PoTY_2009_Aku_nar.037 (037)</ta>
            <ta e="T293" id="Seg_5924" s="T276">PoTY_2009_Aku_nar.038 (038)</ta>
            <ta e="T299" id="Seg_5925" s="T294">PoTY_2009_Aku_nar.039 (039)</ta>
            <ta e="T305" id="Seg_5926" s="T300">PoTY_2009_Aku_nar.040 (040)</ta>
            <ta e="T308" id="Seg_5927" s="T306">PoTY_2009_Aku_nar.041 (041)</ta>
            <ta e="T314" id="Seg_5928" s="T309">PoTY_2009_Aku_nar.042 (042)</ta>
            <ta e="T321" id="Seg_5929" s="T315">PoTY_2009_Aku_nar.043 (043)</ta>
            <ta e="T328" id="Seg_5930" s="T322">PoTY_2009_Aku_nar.044 (044)</ta>
            <ta e="T334" id="Seg_5931" s="T329">PoTY_2009_Aku_nar.045 (045)</ta>
            <ta e="T347" id="Seg_5932" s="T335">PoTY_2009_Aku_nar.046 (046)</ta>
            <ta e="T357" id="Seg_5933" s="T348">PoTY_2009_Aku_nar.047 (047)</ta>
            <ta e="T365" id="Seg_5934" s="T358">PoTY_2009_Aku_nar.048 (048)</ta>
            <ta e="T370" id="Seg_5935" s="T366">PoTY_2009_Aku_nar.049 (049)</ta>
            <ta e="T382" id="Seg_5936" s="T371">PoTY_2009_Aku_nar.050 (050)</ta>
            <ta e="T397" id="Seg_5937" s="T383">PoTY_2009_Aku_nar.051 (051)</ta>
            <ta e="T413" id="Seg_5938" s="T398">PoTY_2009_Aku_nar.052 (052)</ta>
            <ta e="T422" id="Seg_5939" s="T414">PoTY_2009_Aku_nar.053 (053)</ta>
            <ta e="T432" id="Seg_5940" s="T422">PoTY_2009_Aku_nar.054 (054)</ta>
            <ta e="T438" id="Seg_5941" s="T432">PoTY_2009_Aku_nar.055 (055)</ta>
            <ta e="T442" id="Seg_5942" s="T439">PoTY_2009_Aku_nar.056 (056)</ta>
            <ta e="T445" id="Seg_5943" s="T443">PoTY_2009_Aku_nar.057 (057)</ta>
            <ta e="T453" id="Seg_5944" s="T446">PoTY_2009_Aku_nar.058 (058)</ta>
            <ta e="T458" id="Seg_5945" s="T454">PoTY_2009_Aku_nar.059 (059)</ta>
            <ta e="T464" id="Seg_5946" s="T459">PoTY_2009_Aku_nar.060 (060)</ta>
            <ta e="T473" id="Seg_5947" s="T465">PoTY_2009_Aku_nar.061 (061)</ta>
            <ta e="T480" id="Seg_5948" s="T474">PoTY_2009_Aku_nar.062 (062)</ta>
            <ta e="T488" id="Seg_5949" s="T481">PoTY_2009_Aku_nar.063 (063)</ta>
            <ta e="T493" id="Seg_5950" s="T489">PoTY_2009_Aku_nar.064 (064)</ta>
            <ta e="T500" id="Seg_5951" s="T494">PoTY_2009_Aku_nar.065 (065)</ta>
            <ta e="T508" id="Seg_5952" s="T501">PoTY_2009_Aku_nar.066 (066)</ta>
            <ta e="T511" id="Seg_5953" s="T509">PoTY_2009_Aku_nar.067 (067)</ta>
            <ta e="T516" id="Seg_5954" s="T512">PoTY_2009_Aku_nar.068 (068)</ta>
            <ta e="T521" id="Seg_5955" s="T517">PoTY_2009_Aku_nar.069 (069)</ta>
            <ta e="T525" id="Seg_5956" s="T522">PoTY_2009_Aku_nar.070 (070)</ta>
            <ta e="T537" id="Seg_5957" s="T526">PoTY_2009_Aku_nar.071 (071)</ta>
            <ta e="T542" id="Seg_5958" s="T538">PoTY_2009_Aku_nar.072 (072)</ta>
            <ta e="T545" id="Seg_5959" s="T543">PoTY_2009_Aku_nar.073 (073)</ta>
            <ta e="T551" id="Seg_5960" s="T546">PoTY_2009_Aku_nar.074 (074)</ta>
            <ta e="T558" id="Seg_5961" s="T552">PoTY_2009_Aku_nar.075 (075)</ta>
            <ta e="T577" id="Seg_5962" s="T559">PoTY_2009_Aku_nar.076 (076)</ta>
            <ta e="T583" id="Seg_5963" s="T578">PoTY_2009_Aku_nar.077 (077)</ta>
            <ta e="T589" id="Seg_5964" s="T584">PoTY_2009_Aku_nar.078 (078)</ta>
            <ta e="T593" id="Seg_5965" s="T590">PoTY_2009_Aku_nar.079 (079)</ta>
            <ta e="T604" id="Seg_5966" s="T594">PoTY_2009_Aku_nar.080 (080)</ta>
            <ta e="T611" id="Seg_5967" s="T605">PoTY_2009_Aku_nar.081 (081)</ta>
            <ta e="T618" id="Seg_5968" s="T612">PoTY_2009_Aku_nar.082 (082)</ta>
            <ta e="T625" id="Seg_5969" s="T619">PoTY_2009_Aku_nar.083 (083)</ta>
            <ta e="T636" id="Seg_5970" s="T626">PoTY_2009_Aku_nar.084 (084)</ta>
            <ta e="T641" id="Seg_5971" s="T637">PoTY_2009_Aku_nar.085 (085)</ta>
            <ta e="T650" id="Seg_5972" s="T642">PoTY_2009_Aku_nar.086 (086)</ta>
            <ta e="T654" id="Seg_5973" s="T651">PoTY_2009_Aku_nar.087 (087)</ta>
            <ta e="T661" id="Seg_5974" s="T655">PoTY_2009_Aku_nar.088 (088)</ta>
            <ta e="T666" id="Seg_5975" s="T662">PoTY_2009_Aku_nar.089 (089)</ta>
            <ta e="T672" id="Seg_5976" s="T667">PoTY_2009_Aku_nar.090 (090)</ta>
            <ta e="T680" id="Seg_5977" s="T673">PoTY_2009_Aku_nar.091 (091)</ta>
            <ta e="T687" id="Seg_5978" s="T681">PoTY_2009_Aku_nar.092 (092)</ta>
            <ta e="T696" id="Seg_5979" s="T688">PoTY_2009_Aku_nar.093 (093)</ta>
            <ta e="T707" id="Seg_5980" s="T697">PoTY_2009_Aku_nar.094 (094)</ta>
            <ta e="T714" id="Seg_5981" s="T708">PoTY_2009_Aku_nar.095 (095)</ta>
            <ta e="T723" id="Seg_5982" s="T715">PoTY_2009_Aku_nar.096 (096)</ta>
            <ta e="T731" id="Seg_5983" s="T724">PoTY_2009_Aku_nar.097 (097)</ta>
            <ta e="T734" id="Seg_5984" s="T732">PoTY_2009_Aku_nar.098 (098)</ta>
            <ta e="T738" id="Seg_5985" s="T735">PoTY_2009_Aku_nar.099 (099)</ta>
            <ta e="T743" id="Seg_5986" s="T739">PoTY_2009_Aku_nar.100 (100)</ta>
            <ta e="T750" id="Seg_5987" s="T744">PoTY_2009_Aku_nar.101 (101)</ta>
            <ta e="T762" id="Seg_5988" s="T751">PoTY_2009_Aku_nar.102 (102)</ta>
            <ta e="T766" id="Seg_5989" s="T763">PoTY_2009_Aku_nar.103 (103)</ta>
            <ta e="T772" id="Seg_5990" s="T767">PoTY_2009_Aku_nar.104 (104)</ta>
            <ta e="T777" id="Seg_5991" s="T773">PoTY_2009_Aku_nar.105 (105)</ta>
            <ta e="T786" id="Seg_5992" s="T778">PoTY_2009_Aku_nar.106 (106)</ta>
            <ta e="T796" id="Seg_5993" s="T787">PoTY_2009_Aku_nar.107 (107)</ta>
            <ta e="T803" id="Seg_5994" s="T796">PoTY_2009_Aku_nar.108 (108)</ta>
            <ta e="T811" id="Seg_5995" s="T804">PoTY_2009_Aku_nar.109 (109)</ta>
            <ta e="T820" id="Seg_5996" s="T812">PoTY_2009_Aku_nar.110 (110)</ta>
            <ta e="T825" id="Seg_5997" s="T821">PoTY_2009_Aku_nar.111 (111)</ta>
            <ta e="T837" id="Seg_5998" s="T826">PoTY_2009_Aku_nar.112 (112)</ta>
            <ta e="T842" id="Seg_5999" s="T838">PoTY_2009_Aku_nar.113 (113)</ta>
            <ta e="T855" id="Seg_6000" s="T843">PoTY_2009_Aku_nar.114 (114)</ta>
            <ta e="T866" id="Seg_6001" s="T856">PoTY_2009_Aku_nar.115 (115)</ta>
            <ta e="T872" id="Seg_6002" s="T867">PoTY_2009_Aku_nar.116 (116)</ta>
            <ta e="T880" id="Seg_6003" s="T873">PoTY_2009_Aku_nar.117 (117)</ta>
            <ta e="T884" id="Seg_6004" s="T881">PoTY_2009_Aku_nar.118 (118)</ta>
            <ta e="T902" id="Seg_6005" s="T885">PoTY_2009_Aku_nar.119 (119)</ta>
            <ta e="T906" id="Seg_6006" s="T903">PoTY_2009_Aku_nar.120 (120)</ta>
            <ta e="T911" id="Seg_6007" s="T907">PoTY_2009_Aku_nar.121 (121)</ta>
            <ta e="T915" id="Seg_6008" s="T912">PoTY_2009_Aku_nar.122 (122)</ta>
            <ta e="T923" id="Seg_6009" s="T916">PoTY_2009_Aku_nar.123 (123)</ta>
            <ta e="T926" id="Seg_6010" s="T923">PoTY_2009_Aku_nar.124 (124)</ta>
            <ta e="T931" id="Seg_6011" s="T927">PoTY_2009_Aku_nar.125 (125)</ta>
            <ta e="T934" id="Seg_6012" s="T932">PoTY_2009_Aku_nar.126 (126)</ta>
            <ta e="T937" id="Seg_6013" s="T935">PoTY_2009_Aku_nar.127 (127)</ta>
            <ta e="T942" id="Seg_6014" s="T937">PoTY_2009_Aku_nar.128 (128)</ta>
            <ta e="T948" id="Seg_6015" s="T943">PoTY_2009_Aku_nar.129 (129)</ta>
            <ta e="T957" id="Seg_6016" s="T949">PoTY_2009_Aku_nar.130 (130)</ta>
            <ta e="T968" id="Seg_6017" s="T958">PoTY_2009_Aku_nar.131 (131)</ta>
            <ta e="T974" id="Seg_6018" s="T969">PoTY_2009_Aku_nar.132 (132)</ta>
            <ta e="T978" id="Seg_6019" s="T975">PoTY_2009_Aku_nar.133 (133)</ta>
            <ta e="T982" id="Seg_6020" s="T979">PoTY_2009_Aku_nar.134 (134)</ta>
            <ta e="T991" id="Seg_6021" s="T983">PoTY_2009_Aku_nar.135 (135)</ta>
            <ta e="T1000" id="Seg_6022" s="T992">PoTY_2009_Aku_nar.136 (136)</ta>
            <ta e="T1016" id="Seg_6023" s="T1001">PoTY_2009_Aku_nar.137 (137)</ta>
            <ta e="T1024" id="Seg_6024" s="T1017">PoTY_2009_Aku_nar.138 (138)</ta>
            <ta e="T1029" id="Seg_6025" s="T1025">PoTY_2009_Aku_nar.139 (139)</ta>
            <ta e="T1038" id="Seg_6026" s="T1030">PoTY_2009_Aku_nar.140 (140)</ta>
            <ta e="T1047" id="Seg_6027" s="T1039">PoTY_2009_Aku_nar.141 (141)</ta>
            <ta e="T1055" id="Seg_6028" s="T1048">PoTY_2009_Aku_nar.142 (142)</ta>
            <ta e="T1060" id="Seg_6029" s="T1056">PoTY_2009_Aku_nar.143 (143)</ta>
            <ta e="T1063" id="Seg_6030" s="T1061">PoTY_2009_Aku_nar.144 (144)</ta>
            <ta e="T1071" id="Seg_6031" s="T1064">PoTY_2009_Aku_nar.145 (145)</ta>
            <ta e="T1084" id="Seg_6032" s="T1072">PoTY_2009_Aku_nar.146 (146)</ta>
            <ta e="T1090" id="Seg_6033" s="T1085">PoTY_2009_Aku_nar.147 (147)</ta>
            <ta e="T1096" id="Seg_6034" s="T1091">PoTY_2009_Aku_nar.148 (148)</ta>
            <ta e="T1100" id="Seg_6035" s="T1097">PoTY_2009_Aku_nar.149 (149)</ta>
            <ta e="T1107" id="Seg_6036" s="T1101">PoTY_2009_Aku_nar.150 (150)</ta>
            <ta e="T1114" id="Seg_6037" s="T1108">PoTY_2009_Aku_nar.151 (151)</ta>
            <ta e="T1120" id="Seg_6038" s="T1115">PoTY_2009_Aku_nar.152 (152)</ta>
            <ta e="T1124" id="Seg_6039" s="T1121">PoTY_2009_Aku_nar.153 (153)</ta>
            <ta e="T1134" id="Seg_6040" s="T1125">PoTY_2009_Aku_nar.154 (154)</ta>
            <ta e="T1149" id="Seg_6041" s="T1135">PoTY_2009_Aku_nar.155 (155)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="ts" tierref="ts">
            <ta e="T11" id="Seg_6042" s="T1">Biːrde öjdüːbün küččügüj erdeppinen Nahoŋŋa točkabɨt Kiresten u͡on ikki kilometr. </ta>
            <ta e="T15" id="Seg_6043" s="T11">Onno töhölöːk kihi olorbutaj. </ta>
            <ta e="T21" id="Seg_6044" s="T16">Kɨrdʼagastar barɨlara onno oloroːčču etilere. </ta>
            <ta e="T27" id="Seg_6045" s="T22">Hɨrga di͡e toloru ete onno. </ta>
            <ta e="T32" id="Seg_6046" s="T28">Tabalar tu͡oktar baːr etilere. </ta>
            <ta e="T52" id="Seg_6047" s="T33">Onton biːrde teːtemeːk, dʼi͡eleriger teːtem kɨrdʼagas ehebin di͡eččibin bu͡o, teːte di͡eččibin, kɨrdʼagas maːmabɨn maːma di͡eččibin kɨrdʼagas ebebin ginilerge iːtillibitim. </ta>
            <ta e="T60" id="Seg_6048" s="T53">De ol teːtebit kajdi͡ek ere baran kaːlla. </ta>
            <ta e="T72" id="Seg_6049" s="T61">Mastana barda eni de olorobut tabalarbɨt prjamo kimŋe dʼi͡ebit tahɨgar hɨldʼallar. </ta>
            <ta e="T76" id="Seg_6050" s="T73">Tabɨja hɨldʼallar budto. </ta>
            <ta e="T79" id="Seg_6051" s="T77">Aːkulaːk etibit. </ta>
            <ta e="T83" id="Seg_6052" s="T80">Bu iliːten ahɨːr. </ta>
            <ta e="T86" id="Seg_6053" s="T84">Hili͡ebimsek bagajɨ. </ta>
            <ta e="T91" id="Seg_6054" s="T87">Tünnükke keleːčči hili͡eb kördönö. </ta>
            <ta e="T105" id="Seg_6055" s="T92">O munnuta nʼaltajan keli͡e di͡en oː hohujabɨn küččügüj kihi ke kuttanaːččɨ etim bu͡o. </ta>
            <ta e="T112" id="Seg_6056" s="T106">Uː bu kihi kisten arɨjdɨm bu͡o. </ta>
            <ta e="T118" id="Seg_6057" s="T113">Ostolton bu hili͡eb lepi͡eske bagajɨlar. </ta>
            <ta e="T126" id="Seg_6058" s="T119">Oː ontum tülejbit hiː hi͡ektiːr di͡en araːrakata. </ta>
            <ta e="T134" id="Seg_6059" s="T127">De ol maːmam diːr kɨrdʼagas maːmam ke. </ta>
            <ta e="T142" id="Seg_6060" s="T135">Oː ü͡öretime dʼe en tünnükpütün aldʼatɨ͡a kanʼɨ͡a. </ta>
            <ta e="T152" id="Seg_6061" s="T143">Bar bar bar kelime bolše diːr tu͡ok da hu͡ok. </ta>
            <ta e="T160" id="Seg_6062" s="T153">Okkun hi͡e bar diːr heː haban keːste. </ta>
            <ta e="T169" id="Seg_6063" s="T161">Onton biːrges tünnügünen keler onno peredok bu͡olaːččɨ bu͡o. </ta>
            <ta e="T174" id="Seg_6064" s="T170">Onno buːs tu͡ok uːrunaːččɨbɨt. </ta>
            <ta e="T180" id="Seg_6065" s="T175">Bu ke uːbut bu͡o buːsput. </ta>
            <ta e="T190" id="Seg_6066" s="T181">Onno ɨtta ɨtta, tünnükpüt arɨllan turar bu͡o innikiːbit ke. </ta>
            <ta e="T196" id="Seg_6067" s="T191">Onno moltojo moltojo mu͡ostardaːk bu͡o. </ta>
            <ta e="T208" id="Seg_6068" s="T197">Mu͡ostara bappattar kim čoŋalge lepi͡eske turar ete (lepi͡es-) (aldʼal-) kimneːbit bɨhɨllɨbɨttar. </ta>
            <ta e="T217" id="Seg_6069" s="T209">De onu tiːje hataːta bu͡o tɨlɨn uːna uːna. </ta>
            <ta e="T231" id="Seg_6070" s="T218">"Maːma kör kör kör", diːbin, "kimŋin hi͡eri gɨnna kili͡eptergin hi͡eri gɨnna diːbin kili͡eptergin." </ta>
            <ta e="T243" id="Seg_6071" s="T232">"Oj togo satana kɨːha togo ü͡öreppikkinij bu͡o, dʼi͡etten ahaːta tünnükten ahaːta. </ta>
            <ta e="T249" id="Seg_6072" s="T244">Kör anɨ barɨtɨn u͡oru͡oga", diːr. </ta>
            <ta e="T253" id="Seg_6073" s="T250">Araː bɨ͡ar agajbɨn. </ta>
            <ta e="T257" id="Seg_6074" s="T254">Uː bar bar. </ta>
            <ta e="T263" id="Seg_6075" s="T258">Tu͡ok ere trepkenen leš leš. </ta>
            <ta e="T266" id="Seg_6076" s="T264">Onton barbat. </ta>
            <ta e="T271" id="Seg_6077" s="T267">De na vso bar. </ta>
            <ta e="T275" id="Seg_6078" s="T272">Bi͡eren keːher bu͡o. </ta>
            <ta e="T293" id="Seg_6079" s="T276">Ontuŋ momuja momuja voːpse baraːktɨːr di͡en hi͡ese hili͡ebi, hi͡ese totto bɨhɨlaːk, tu͡ok toholaːk totu͡oj, de ol kačɨgɨrɨtar. </ta>
            <ta e="T299" id="Seg_6080" s="T294">Okazɨvaetsa kimi bi͡erbit kappɨt hili͡ebi. </ta>
            <ta e="T305" id="Seg_6081" s="T300">De onno ɨstɨː hataːta bu͡o. </ta>
            <ta e="T308" id="Seg_6082" s="T306">Araːle beseleːtin. </ta>
            <ta e="T314" id="Seg_6083" s="T309">Oj tiːsterin barɨtɨn kimneːte bɨhɨlak. </ta>
            <ta e="T321" id="Seg_6084" s="T315">De ol barbɨta, hi͡ese dogottorun di͡ek. </ta>
            <ta e="T328" id="Seg_6085" s="T322">Hɨːr annɨtɨgar tüspüttere onno ebege hɨldʼallar. </ta>
            <ta e="T334" id="Seg_6086" s="T329">Tahaːrabɨt boru͡orda karaŋarar, karaŋaran iher. </ta>
            <ta e="T347" id="Seg_6087" s="T335">Kim ɨttaːk etibit bihigi, Djoŋgo, čeːlke bagajɨ, bosku͡oj bagajɨ, tabahɨt bagajɨ ontuŋ. </ta>
            <ta e="T357" id="Seg_6088" s="T348">Kim hüs di͡eŋ du͡o kimi ü͡örü barɨtɨn egelen keli͡e. </ta>
            <ta e="T365" id="Seg_6089" s="T358">Oː erijekti͡ege di͡en tögürüččü biːr hirge turu͡oga. </ta>
            <ta e="T370" id="Seg_6090" s="T366">Bu egel di͡etekke ke. </ta>
            <ta e="T382" id="Seg_6091" s="T371">De ontubut kimi kimneːčči ete maːjdɨn tabalarɨŋ baːr erdekterinen ke dʼi͡ege. </ta>
            <ta e="T397" id="Seg_6092" s="T383">Tugut, abɨlakaːn du tu͡ok du, abɨlakaːn, tugut eː ulakan, tuguttan ulakankaːn dʼɨlɨnan ere ulakan. </ta>
            <ta e="T413" id="Seg_6093" s="T398">Ol kimi de erije hɨtɨ͡aga bu͡o, muŋnu hɨtaːččɨ iti tu͡ok tugukkaːn bagaraːččɨ dürü möŋö möŋö. </ta>
            <ta e="T422" id="Seg_6094" s="T414">Hɨtɨ͡aga huh bu͡o oːnnʼuːr bu͡o ginini gɨtta ke. </ta>
            <ta e="T432" id="Seg_6095" s="T422">Ontuŋ kajdak da bu͡olla ((…)) tabɨhas tabɨsar bu kurduk kajdak gɨnɨj. </ta>
            <ta e="T438" id="Seg_6096" s="T432">"Maːma maːma", diːbin, "kör kör Djoŋgonu". </ta>
            <ta e="T442" id="Seg_6097" s="T439">Čoko Čoko di͡eččibin. </ta>
            <ta e="T445" id="Seg_6098" s="T443">"Čoko kör. </ta>
            <ta e="T453" id="Seg_6099" s="T446">Kimi hi͡eri gɨnna", diːbin, "tugutu hi͡eri gɨnna." </ta>
            <ta e="T458" id="Seg_6100" s="T454">"Oː satana ɨgɨr ɨgɨr." </ta>
            <ta e="T464" id="Seg_6101" s="T459">Hɨlatta bɨhɨlak iti tugutu, kuttaːta. </ta>
            <ta e="T473" id="Seg_6102" s="T465">"Čoko Čoko Čoko", diːbin tahaːra taksan baran ke. </ta>
            <ta e="T480" id="Seg_6103" s="T474">Oː de ontuŋ kötüten iheːktiːr di͡en. </ta>
            <ta e="T488" id="Seg_6104" s="T481">De (ɨstan-) de hɨrajbar, min taras hahaha. </ta>
            <ta e="T493" id="Seg_6105" s="T489">Ittenne taraja hɨtaːktɨːbɨn di͡en. </ta>
            <ta e="T500" id="Seg_6106" s="T494">"Eː", diːbin, "maːma ɨl dʼa bu͡o." </ta>
            <ta e="T508" id="Seg_6107" s="T501">Halɨːr kanʼɨːr (möŋ-) möŋtörör bu͡o kihini ke. </ta>
            <ta e="T511" id="Seg_6108" s="T509">Möŋü͡ögün bagarar. </ta>
            <ta e="T516" id="Seg_6109" s="T512">Minigin hogostuː hɨtaːktɨːr di͡en. </ta>
            <ta e="T521" id="Seg_6110" s="T517">De ol maːmam tagɨsta. </ta>
            <ta e="T525" id="Seg_6111" s="T522">De tu͡ok bu͡olagɨn. </ta>
            <ta e="T537" id="Seg_6112" s="T526">"Kör", diːbin, "minigin kɨrbɨːr du͡o", diːbin, "oːnnʼuːr kihini ke araː da." </ta>
            <ta e="T542" id="Seg_6113" s="T538">Čičigeren ɨtaːn mörüleːktiːbin heee. </ta>
            <ta e="T545" id="Seg_6114" s="T543">"Ɨtaː ɨtaːma. </ta>
            <ta e="T551" id="Seg_6115" s="T546">Bejeŋ ɨgɨrbɨtɨŋ", diːr, "kihi külü͡ök". </ta>
            <ta e="T558" id="Seg_6116" s="T552">Ontuŋ emi͡e baran kaːlla ol tugukka. </ta>
            <ta e="T577" id="Seg_6117" s="T559">De bolše kimneːbetegim ol bi͡ek kün eːje oːnnʼoːčču ol tugutu gɨtta tu͡okka bagarbɨt imenno iti tugut bu͡olu͡ogun naːda. </ta>
            <ta e="T583" id="Seg_6118" s="T578">De onton tahaːrabɨt karaŋarda bu͡o. </ta>
            <ta e="T589" id="Seg_6119" s="T584">Bosku͡ojɨn da kimmit ohokput ke. </ta>
            <ta e="T593" id="Seg_6120" s="T590">Maːmam delbi kaːlaːbɨt. </ta>
            <ta e="T604" id="Seg_6121" s="T594">Ps ps ps kim mas ke ubajara ke kɨtarčɨ ohopput. </ta>
            <ta e="T611" id="Seg_6122" s="T605">Čajniktar kötüten voːpsetɨn telibireːktiːller di͡en kappaktara. </ta>
            <ta e="T618" id="Seg_6123" s="T612">Kü͡öspüt laglɨ͡a turaːktɨːr di͡en laglɨ͡a, voːpse. </ta>
            <ta e="T625" id="Seg_6124" s="T619">(Itij-) aːn tünnügü arɨjda ü͡ölehe arɨjda. </ta>
            <ta e="T636" id="Seg_6125" s="T626">Ü͡öles bu͡olaːččɨ kim bü͡öleːččiler nu štobɨ veter tɨ͡al kiːri͡egin ke. </ta>
            <ta e="T641" id="Seg_6126" s="T637">De ol arɨjda heː. </ta>
            <ta e="T650" id="Seg_6127" s="T642">Min du hürdeːk bagaraːččɨbɨn tünnükke tünnükke körü͡öppün ke. </ta>
            <ta e="T654" id="Seg_6128" s="T651">Mɨlaja mɨlaja oloroːččubun. </ta>
            <ta e="T661" id="Seg_6129" s="T655">Bosku͡ojin da i͡e kim, ɨj hahaha. </ta>
            <ta e="T666" id="Seg_6130" s="T662">Ɨj erejkeːn ebite eh. </ta>
            <ta e="T672" id="Seg_6131" s="T667">Ɨjbɨt bosku͡oj bagajɨ no kap-karaŋa. </ta>
            <ta e="T680" id="Seg_6132" s="T673">Tu͡oktar kötöːččüler ebit min že bilbet etim. </ta>
            <ta e="T687" id="Seg_6133" s="T681">Leŋkej kördük leŋkejdar di͡en hu͡oga küččügüjdar. </ta>
            <ta e="T696" id="Seg_6134" s="T688">Menʼiːte hu͡oktar di͡eččibin menʼiːte hu͡oktar hɨldʼallar maːma kör. </ta>
            <ta e="T707" id="Seg_6135" s="T697">Okazɨvaetsa iti kim eh bu čok kötör bu͡o trubatan ke. </ta>
            <ta e="T714" id="Seg_6136" s="T708">Onuga möŋsöllör ebit itileriŋ interes bagajɨ. </ta>
            <ta e="T723" id="Seg_6137" s="T715">Onton töhö eme maːmam (iːsten-) oloror, (iːsten-) oloror. </ta>
            <ta e="T731" id="Seg_6138" s="T724">Orguːjakaːn orguːjakaːn diː diː, ɨllaːn oloror orguːjak. </ta>
            <ta e="T734" id="Seg_6139" s="T732">Djakubɨt hɨtar. </ta>
            <ta e="T738" id="Seg_6140" s="T735">Nʼik da gɨmmat. </ta>
            <ta e="T743" id="Seg_6141" s="T739">Hɨlajbɨt bɨhɨlak tugutun gɨtta. </ta>
            <ta e="T750" id="Seg_6142" s="T744">Onton kim vremja vremja barda bu͡o. </ta>
            <ta e="T762" id="Seg_6143" s="T751">Poka ol tu͡ok itte bütte min ješo teːtem hu͡okkaːnɨnan, radiona vklučajdɨnabɨn. </ta>
            <ta e="T766" id="Seg_6144" s="T763">Heee gaːgɨnataːktɨːbɨn de. </ta>
            <ta e="T772" id="Seg_6145" s="T767">"Eː orguːj gɨn beji talɨgɨratɨma." </ta>
            <ta e="T777" id="Seg_6146" s="T773">Ɨrɨ͡alarɨ ihilliː hɨtɨ͡am bu͡o. </ta>
            <ta e="T786" id="Seg_6147" s="T778">Henʼooo kimneri di͡e ihilliːbin okazɨvaetsa Kitajetsardarlar tu͡oktar du. </ta>
            <ta e="T796" id="Seg_6148" s="T787">Oloru ihillen bu͡ola hɨtaːktɨːbɨn di͡en oččogo, beseleː bagajɨ, bilebin. </ta>
            <ta e="T803" id="Seg_6149" s="T796">Onton hi͡ese nʼuːččalar bullum onton büten kaːlbɨt. </ta>
            <ta e="T811" id="Seg_6150" s="T804">"De büteren keːhi͡ekke büter teːteŋ keli͡e heː." </ta>
            <ta e="T820" id="Seg_6151" s="T812">Kümmüt ɨjbɨt ɨraːtta bu͡o ü͡öhe bu͡olla ama bosku͡oj. </ta>
            <ta e="T825" id="Seg_6152" s="T821">Ama hɨrdɨk künüs kördük. </ta>
            <ta e="T837" id="Seg_6153" s="T826">Oː tabalarbɨt voːpse ebe üstün kötüte hɨldʼaːktɨːllar de ama bosku͡ojin da. </ta>
            <ta e="T842" id="Seg_6154" s="T838">Kim buru͡olara aš kötör. </ta>
            <ta e="T855" id="Seg_6155" s="T843">Oj ama bosku͡oj kim ke kaːrɨŋ ke kimŋe lunaːtan ama kimniːr gɨlbaŋnɨːr. </ta>
            <ta e="T866" id="Seg_6156" s="T856">Onton onton beːbe ɨppɨt mm mm gɨnar bu kimniːr muŋurguːr. </ta>
            <ta e="T872" id="Seg_6157" s="T867">Beː ču͡oraːn tɨ͡ahaːta araj, küpüleːn. </ta>
            <ta e="T880" id="Seg_6158" s="T873">Teleŋ teleŋ teleŋ teːtebit iher teːtebit heː. </ta>
            <ta e="T884" id="Seg_6159" s="T881">De ol kelle. </ta>
            <ta e="T902" id="Seg_6160" s="T885">Min du bilbetegim bu͡o mastana barbɨt diːbin bu͡o okazɨvaetsa giniŋ bu͡o Dudinka di͡ek hɨldʼabɨt i Dujunaː di͡ek. </ta>
            <ta e="T906" id="Seg_6161" s="T903">De ol kelle. </ta>
            <ta e="T911" id="Seg_6162" s="T907">Uže karaŋarbɨtɨn kenne ke. </ta>
            <ta e="T915" id="Seg_6163" s="T912">Oː teːtem kelle. </ta>
            <ta e="T923" id="Seg_6164" s="T916">Oː maːmam kömölöhö barda hü͡örde kanʼaːta küpüleːnten. </ta>
            <ta e="T926" id="Seg_6165" s="T923">Tabalarɨn ɨːttɨlar kanʼaːtɨlar. </ta>
            <ta e="T931" id="Seg_6166" s="T927">Oː teːtem mesoktardaːk kelle. </ta>
            <ta e="T934" id="Seg_6167" s="T932">Küːllerin kiːllerde. </ta>
            <ta e="T937" id="Seg_6168" s="T935">De kostoːtoloːto. </ta>
            <ta e="T942" id="Seg_6169" s="T937">Oː ča kempi͡et ülegerin egelbit. </ta>
            <ta e="T948" id="Seg_6170" s="T943">Onton bosku͡oj bagajɨ bi͡ek öjdüːbün. </ta>
            <ta e="T957" id="Seg_6171" s="T949">Kim čaːstar du͡o bosku͡oj bagajɨ kim hirkilinnen oŋohullubut. </ta>
            <ta e="T968" id="Seg_6172" s="T958">Ontuŋ ješo knopkalardaːk tu͡ok ere ama bosku͡oj oduːrgaːktɨːbɨn ama bosku͡oj. </ta>
            <ta e="T974" id="Seg_6173" s="T969">"De tɨːtɨma höp bu͡olu͡o", diːller. </ta>
            <ta e="T978" id="Seg_6174" s="T975">Tɨːttaraːččɨta hu͡oktar bu͡o. </ta>
            <ta e="T982" id="Seg_6175" s="T979">"Aldʼatɨ͡aŋ kaja heː". </ta>
            <ta e="T991" id="Seg_6176" s="T983">De uːrdum hürdeːk körü͡öppün bagarabɨn tɨːtalɨ͡appɨn bagarabɨn bu͡o. </ta>
            <ta e="T1000" id="Seg_6177" s="T992">Onton kim, kimi egelbite, mini͡eke podarok kompaskaːn kuččuguj. </ta>
            <ta e="T1016" id="Seg_6178" s="T1001">Onu bi͡ek oːnnʼuː hɨtaːččɨbɨn araː bosku͡ojɨn du͡o, urut pervɨj raz egelbite, bu korduk oːnnʼuːru ke. </ta>
            <ta e="T1024" id="Seg_6179" s="T1017">Oː min muŋ hu͡ok voːpse bosku͡ojɨn da. </ta>
            <ta e="T1029" id="Seg_6180" s="T1025">"Oː eni͡eke podarok", diːr. </ta>
            <ta e="T1038" id="Seg_6181" s="T1030">De ol kepseteller eni ke, min utujan kaːlbɨppɨn. </ta>
            <ta e="T1047" id="Seg_6182" s="T1039">Harsɨ͡arda uže uhuktabɨn oː ohok emi͡e kötüte turar. </ta>
            <ta e="T1055" id="Seg_6183" s="T1048">Bosku͡ojɨn du͡o kim da hu͡ok dʼi͡ebit ihiger. </ta>
            <ta e="T1060" id="Seg_6184" s="T1056">Ol tabalarɨn tuttallar ebit. </ta>
            <ta e="T1063" id="Seg_6185" s="T1061">Tutta taksɨbɨttar. </ta>
            <ta e="T1071" id="Seg_6186" s="T1064">Ɨ͡allardaːk etibit tak da, baːr etilere ɨ͡allarbɨt. </ta>
            <ta e="T1084" id="Seg_6187" s="T1072">De ol komujdular ü͡örderin, komunallar taba tu͡ok, köhöbüt bɨhɨlak, öjdüːbün kanna ere. </ta>
            <ta e="T1090" id="Seg_6188" s="T1085">Oː de minigin taŋɨnnaran čičigeretiler. </ta>
            <ta e="T1096" id="Seg_6189" s="T1091">Hɨrgaga baːjan keːspitter önülen keːspitter. </ta>
            <ta e="T1100" id="Seg_6190" s="T1097">De kajdi͡ek barabɨt? </ta>
            <ta e="T1107" id="Seg_6191" s="T1101">I kennibitten du͡o tabalarɨ hi͡eteːččiler kosobu͡ojga. </ta>
            <ta e="T1114" id="Seg_6192" s="T1108">Astaːk ili mastaːk kosobu͡oj hi͡eteːčči etibit. </ta>
            <ta e="T1120" id="Seg_6193" s="T1115">Olorton kuttanan ɨtaːn mörüleːktiːbin di͡en. </ta>
            <ta e="T1124" id="Seg_6194" s="T1121">"Kuttanɨma ɨtɨrɨ͡a hu͡oga." </ta>
            <ta e="T1134" id="Seg_6195" s="T1125">Bu ke hɨlajbɨttar ke tɨːnallar prjamo prjamo hɨrajbar munna. </ta>
            <ta e="T1149" id="Seg_6196" s="T1135">De ol iti korduk köhö hɨldʼaːččɨ etibit, köhö hɨldʼaːččɨ, köhüse hɨldʼaːččɨ etim ginileri gɨtta. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_6197" s="T1">biːrde</ta>
            <ta e="T3" id="Seg_6198" s="T2">öjd-üː-bün</ta>
            <ta e="T4" id="Seg_6199" s="T3">küččügüj</ta>
            <ta e="T5" id="Seg_6200" s="T4">er-dep-pinen</ta>
            <ta e="T6" id="Seg_6201" s="T5">Nahoŋ-ŋa</ta>
            <ta e="T7" id="Seg_6202" s="T6">točka-bɨt</ta>
            <ta e="T8" id="Seg_6203" s="T7">Kires-ten</ta>
            <ta e="T9" id="Seg_6204" s="T8">u͡on</ta>
            <ta e="T10" id="Seg_6205" s="T9">ikki</ta>
            <ta e="T11" id="Seg_6206" s="T10">kilometr</ta>
            <ta e="T12" id="Seg_6207" s="T11">onno</ta>
            <ta e="T13" id="Seg_6208" s="T12">töhö-löːk</ta>
            <ta e="T14" id="Seg_6209" s="T13">kihi</ta>
            <ta e="T15" id="Seg_6210" s="T14">olor-but-a=j</ta>
            <ta e="T17" id="Seg_6211" s="T16">kɨrdʼagas-tar</ta>
            <ta e="T18" id="Seg_6212" s="T17">barɨ-lara</ta>
            <ta e="T19" id="Seg_6213" s="T18">onno</ta>
            <ta e="T20" id="Seg_6214" s="T19">olor-oːčču</ta>
            <ta e="T21" id="Seg_6215" s="T20">e-ti-lere</ta>
            <ta e="T23" id="Seg_6216" s="T22">hɨrga</ta>
            <ta e="T24" id="Seg_6217" s="T23">dʼi͡e</ta>
            <ta e="T25" id="Seg_6218" s="T24">toloru</ta>
            <ta e="T26" id="Seg_6219" s="T25">e-t-e</ta>
            <ta e="T27" id="Seg_6220" s="T26">onno</ta>
            <ta e="T29" id="Seg_6221" s="T28">taba-LAr</ta>
            <ta e="T30" id="Seg_6222" s="T29">tu͡ok-tar</ta>
            <ta e="T31" id="Seg_6223" s="T30">baːr</ta>
            <ta e="T32" id="Seg_6224" s="T31">e-ti-lere</ta>
            <ta e="T34" id="Seg_6225" s="T33">on-ton</ta>
            <ta e="T35" id="Seg_6226" s="T34">biːrde</ta>
            <ta e="T36" id="Seg_6227" s="T35">teːte-m-eːk</ta>
            <ta e="T37" id="Seg_6228" s="T36">dʼi͡e-leri-ger</ta>
            <ta e="T38" id="Seg_6229" s="T37">teːte-m</ta>
            <ta e="T39" id="Seg_6230" s="T38">kɨrdʼagas</ta>
            <ta e="T40" id="Seg_6231" s="T39">ehe-bi-n</ta>
            <ta e="T41" id="Seg_6232" s="T40">di͡e-čči-bin</ta>
            <ta e="T42" id="Seg_6233" s="T41">bu͡o</ta>
            <ta e="T43" id="Seg_6234" s="T42">teːte</ta>
            <ta e="T44" id="Seg_6235" s="T43">di͡e-čči-bin</ta>
            <ta e="T45" id="Seg_6236" s="T44">kɨrdʼagas</ta>
            <ta e="T46" id="Seg_6237" s="T45">maːma-bɨ-n</ta>
            <ta e="T47" id="Seg_6238" s="T46">maːma</ta>
            <ta e="T48" id="Seg_6239" s="T47">di͡e-čči-bin</ta>
            <ta e="T49" id="Seg_6240" s="T48">kɨrdʼagas</ta>
            <ta e="T50" id="Seg_6241" s="T49">ebe-bi-n</ta>
            <ta e="T51" id="Seg_6242" s="T50">giniler-ge</ta>
            <ta e="T52" id="Seg_6243" s="T51">iːt-i-ll-i-bit-i-m</ta>
            <ta e="T54" id="Seg_6244" s="T53">de</ta>
            <ta e="T55" id="Seg_6245" s="T54">ol</ta>
            <ta e="T56" id="Seg_6246" s="T55">teːte-bit</ta>
            <ta e="T57" id="Seg_6247" s="T56">kajdi͡ek</ta>
            <ta e="T58" id="Seg_6248" s="T57">ere</ta>
            <ta e="T59" id="Seg_6249" s="T58">bar-an</ta>
            <ta e="T60" id="Seg_6250" s="T59">kaːl-l-a</ta>
            <ta e="T62" id="Seg_6251" s="T61">mas-tan-a</ta>
            <ta e="T63" id="Seg_6252" s="T62">bar-d-a</ta>
            <ta e="T64" id="Seg_6253" s="T63">eni</ta>
            <ta e="T65" id="Seg_6254" s="T64">de</ta>
            <ta e="T66" id="Seg_6255" s="T65">olor-o-but</ta>
            <ta e="T67" id="Seg_6256" s="T66">taba-lar-bɨt</ta>
            <ta e="T69" id="Seg_6257" s="T68">kim-ŋe</ta>
            <ta e="T70" id="Seg_6258" s="T69">dʼi͡e-bit</ta>
            <ta e="T71" id="Seg_6259" s="T70">tah-ɨ-gar</ta>
            <ta e="T72" id="Seg_6260" s="T71">hɨldʼ-al-lar</ta>
            <ta e="T74" id="Seg_6261" s="T73">tabɨj-a</ta>
            <ta e="T75" id="Seg_6262" s="T74">hɨldʼ-al-lar</ta>
            <ta e="T78" id="Seg_6263" s="T77">aːku-laːk</ta>
            <ta e="T79" id="Seg_6264" s="T78">e-ti-bit</ta>
            <ta e="T81" id="Seg_6265" s="T80">bu</ta>
            <ta e="T82" id="Seg_6266" s="T81">iliː-ten</ta>
            <ta e="T83" id="Seg_6267" s="T82">ahɨː-r</ta>
            <ta e="T85" id="Seg_6268" s="T84">hili͡eb-i-msek</ta>
            <ta e="T86" id="Seg_6269" s="T85">bagajɨ</ta>
            <ta e="T88" id="Seg_6270" s="T87">tünnük-ke</ta>
            <ta e="T89" id="Seg_6271" s="T88">kel-eːčči</ta>
            <ta e="T90" id="Seg_6272" s="T89">hili͡eb</ta>
            <ta e="T91" id="Seg_6273" s="T90">kördön-ö</ta>
            <ta e="T93" id="Seg_6274" s="T92">o</ta>
            <ta e="T94" id="Seg_6275" s="T93">munnu-ta</ta>
            <ta e="T95" id="Seg_6276" s="T94">nʼaltaj-an</ta>
            <ta e="T96" id="Seg_6277" s="T95">kel-i͡e</ta>
            <ta e="T97" id="Seg_6278" s="T96">di͡e-n</ta>
            <ta e="T98" id="Seg_6279" s="T97">o</ta>
            <ta e="T99" id="Seg_6280" s="T98">hohuj-a-bɨn</ta>
            <ta e="T100" id="Seg_6281" s="T99">küččügüj</ta>
            <ta e="T101" id="Seg_6282" s="T100">kihi</ta>
            <ta e="T102" id="Seg_6283" s="T101">ke</ta>
            <ta e="T103" id="Seg_6284" s="T102">kuttan-aːččɨ</ta>
            <ta e="T104" id="Seg_6285" s="T103">e-ti-m</ta>
            <ta e="T105" id="Seg_6286" s="T104">bu͡o</ta>
            <ta e="T108" id="Seg_6287" s="T107">bu</ta>
            <ta e="T109" id="Seg_6288" s="T108">kihi</ta>
            <ta e="T110" id="Seg_6289" s="T109">kiste-n</ta>
            <ta e="T111" id="Seg_6290" s="T110">arɨj-dɨ-m</ta>
            <ta e="T112" id="Seg_6291" s="T111">bu͡o</ta>
            <ta e="T114" id="Seg_6292" s="T113">ostol-ton</ta>
            <ta e="T115" id="Seg_6293" s="T114">bu</ta>
            <ta e="T116" id="Seg_6294" s="T115">hili͡eb</ta>
            <ta e="T117" id="Seg_6295" s="T116">lepi͡eske</ta>
            <ta e="T118" id="Seg_6296" s="T117">bagajɨ-lar</ta>
            <ta e="T120" id="Seg_6297" s="T119">o</ta>
            <ta e="T121" id="Seg_6298" s="T120">on-tu-m</ta>
            <ta e="T122" id="Seg_6299" s="T121">tülej-bit</ta>
            <ta e="T123" id="Seg_6300" s="T122">h-iː</ta>
            <ta e="T124" id="Seg_6301" s="T123">hi͡e-ktiː-r</ta>
            <ta e="T125" id="Seg_6302" s="T124">di͡e-n</ta>
            <ta e="T126" id="Seg_6303" s="T125">araːrakata</ta>
            <ta e="T128" id="Seg_6304" s="T127">de</ta>
            <ta e="T129" id="Seg_6305" s="T128">ol</ta>
            <ta e="T130" id="Seg_6306" s="T129">maːma-m</ta>
            <ta e="T131" id="Seg_6307" s="T130">diː-r</ta>
            <ta e="T132" id="Seg_6308" s="T131">kɨrdʼagas</ta>
            <ta e="T133" id="Seg_6309" s="T132">maːma-m</ta>
            <ta e="T134" id="Seg_6310" s="T133">ke</ta>
            <ta e="T136" id="Seg_6311" s="T135">o</ta>
            <ta e="T137" id="Seg_6312" s="T136">ü͡öret-i-me</ta>
            <ta e="T138" id="Seg_6313" s="T137">dʼe</ta>
            <ta e="T139" id="Seg_6314" s="T138">en</ta>
            <ta e="T140" id="Seg_6315" s="T139">tünnük-pütü-n</ta>
            <ta e="T141" id="Seg_6316" s="T140">aldʼat-ɨ͡a</ta>
            <ta e="T142" id="Seg_6317" s="T141">kanʼ-ɨ͡a</ta>
            <ta e="T144" id="Seg_6318" s="T143">bar</ta>
            <ta e="T145" id="Seg_6319" s="T144">bar</ta>
            <ta e="T146" id="Seg_6320" s="T145">bar</ta>
            <ta e="T147" id="Seg_6321" s="T146">kel-i-me</ta>
            <ta e="T149" id="Seg_6322" s="T148">diː-r</ta>
            <ta e="T150" id="Seg_6323" s="T149">tu͡ok</ta>
            <ta e="T151" id="Seg_6324" s="T150">da</ta>
            <ta e="T152" id="Seg_6325" s="T151">hu͡ok</ta>
            <ta e="T154" id="Seg_6326" s="T153">ok-ku-n</ta>
            <ta e="T155" id="Seg_6327" s="T154">hi͡e</ta>
            <ta e="T156" id="Seg_6328" s="T155">bar</ta>
            <ta e="T157" id="Seg_6329" s="T156">diː-r</ta>
            <ta e="T158" id="Seg_6330" s="T157">heː</ta>
            <ta e="T159" id="Seg_6331" s="T158">hab-an</ta>
            <ta e="T160" id="Seg_6332" s="T159">keːs-t-e</ta>
            <ta e="T162" id="Seg_6333" s="T161">on-ton</ta>
            <ta e="T163" id="Seg_6334" s="T162">biːrges</ta>
            <ta e="T164" id="Seg_6335" s="T163">tünnüg-ü-nen</ta>
            <ta e="T165" id="Seg_6336" s="T164">kel-er</ta>
            <ta e="T166" id="Seg_6337" s="T165">onno</ta>
            <ta e="T168" id="Seg_6338" s="T167">bu͡ol-aːččɨ</ta>
            <ta e="T169" id="Seg_6339" s="T168">bu͡o</ta>
            <ta e="T171" id="Seg_6340" s="T170">onno</ta>
            <ta e="T172" id="Seg_6341" s="T171">buːs</ta>
            <ta e="T173" id="Seg_6342" s="T172">tu͡ok</ta>
            <ta e="T174" id="Seg_6343" s="T173">uːr-u-n-aːččɨ-bɨt</ta>
            <ta e="T176" id="Seg_6344" s="T175">bu</ta>
            <ta e="T177" id="Seg_6345" s="T176">ke</ta>
            <ta e="T178" id="Seg_6346" s="T177">uː-but</ta>
            <ta e="T179" id="Seg_6347" s="T178">bu͡o</ta>
            <ta e="T180" id="Seg_6348" s="T179">buːs-put</ta>
            <ta e="T182" id="Seg_6349" s="T181">onno</ta>
            <ta e="T183" id="Seg_6350" s="T182">ɨtt-a</ta>
            <ta e="T184" id="Seg_6351" s="T183">ɨtt-a</ta>
            <ta e="T185" id="Seg_6352" s="T184">tünnük-püt</ta>
            <ta e="T186" id="Seg_6353" s="T185">arɨ-ll-an</ta>
            <ta e="T187" id="Seg_6354" s="T186">tur-ar</ta>
            <ta e="T188" id="Seg_6355" s="T187">bu͡o</ta>
            <ta e="T189" id="Seg_6356" s="T188">innikiː-bit</ta>
            <ta e="T190" id="Seg_6357" s="T189">ke</ta>
            <ta e="T192" id="Seg_6358" s="T191">onno</ta>
            <ta e="T193" id="Seg_6359" s="T192">moltoj-o</ta>
            <ta e="T194" id="Seg_6360" s="T193">moltoj-o</ta>
            <ta e="T195" id="Seg_6361" s="T194">mu͡os-tar-daːk</ta>
            <ta e="T196" id="Seg_6362" s="T195">bu͡o</ta>
            <ta e="T198" id="Seg_6363" s="T197">mu͡os-tar-a</ta>
            <ta e="T199" id="Seg_6364" s="T198">bap-pat-tar</ta>
            <ta e="T200" id="Seg_6365" s="T199">kim</ta>
            <ta e="T201" id="Seg_6366" s="T200">čoŋal-ge</ta>
            <ta e="T202" id="Seg_6367" s="T201">lepi͡eske</ta>
            <ta e="T203" id="Seg_6368" s="T202">tur-ar</ta>
            <ta e="T204" id="Seg_6369" s="T203">e-t-e</ta>
            <ta e="T207" id="Seg_6370" s="T206">kim-neː-bit</ta>
            <ta e="T208" id="Seg_6371" s="T207">bɨh-ɨ-ll-ɨ-bɨt-tar</ta>
            <ta e="T210" id="Seg_6372" s="T209">de</ta>
            <ta e="T211" id="Seg_6373" s="T210">o-nu</ta>
            <ta e="T212" id="Seg_6374" s="T211">tiːj-e</ta>
            <ta e="T213" id="Seg_6375" s="T212">hataː-t-a</ta>
            <ta e="T214" id="Seg_6376" s="T213">bu͡o</ta>
            <ta e="T215" id="Seg_6377" s="T214">tɨl-ɨ-n</ta>
            <ta e="T216" id="Seg_6378" s="T215">uːn-a</ta>
            <ta e="T217" id="Seg_6379" s="T216">uːn-a</ta>
            <ta e="T219" id="Seg_6380" s="T218">maːma</ta>
            <ta e="T220" id="Seg_6381" s="T219">kör</ta>
            <ta e="T221" id="Seg_6382" s="T220">kör</ta>
            <ta e="T222" id="Seg_6383" s="T221">kör</ta>
            <ta e="T223" id="Seg_6384" s="T222">d-iː-bin</ta>
            <ta e="T224" id="Seg_6385" s="T223">kim-ŋi-n</ta>
            <ta e="T225" id="Seg_6386" s="T224">hi͡e-ri</ta>
            <ta e="T226" id="Seg_6387" s="T225">gɨn-n-a</ta>
            <ta e="T227" id="Seg_6388" s="T226">kili͡ep-ter-gi-n</ta>
            <ta e="T228" id="Seg_6389" s="T227">hi͡e-ri</ta>
            <ta e="T229" id="Seg_6390" s="T228">gɨn-n-a</ta>
            <ta e="T230" id="Seg_6391" s="T229">d-iː-bin</ta>
            <ta e="T231" id="Seg_6392" s="T230">kili͡ep-ter-gi-n</ta>
            <ta e="T233" id="Seg_6393" s="T232">oj</ta>
            <ta e="T234" id="Seg_6394" s="T233">togo</ta>
            <ta e="T235" id="Seg_6395" s="T234">satana</ta>
            <ta e="T236" id="Seg_6396" s="T235">kɨːh-a</ta>
            <ta e="T237" id="Seg_6397" s="T236">togo</ta>
            <ta e="T238" id="Seg_6398" s="T237">ü͡örep-pik-kin=ij</ta>
            <ta e="T239" id="Seg_6399" s="T238">bu͡o</ta>
            <ta e="T240" id="Seg_6400" s="T239">dʼi͡e-tten</ta>
            <ta e="T241" id="Seg_6401" s="T240">ahaː-t-a</ta>
            <ta e="T242" id="Seg_6402" s="T241">tünnük-ten</ta>
            <ta e="T243" id="Seg_6403" s="T242">ahaː-t-a</ta>
            <ta e="T245" id="Seg_6404" s="T244">kör</ta>
            <ta e="T246" id="Seg_6405" s="T245">anɨ</ta>
            <ta e="T247" id="Seg_6406" s="T246">barɨ-tɨ-n</ta>
            <ta e="T248" id="Seg_6407" s="T247">u͡or-u͡og-a</ta>
            <ta e="T249" id="Seg_6408" s="T248">diː-r</ta>
            <ta e="T251" id="Seg_6409" s="T250">araː</ta>
            <ta e="T252" id="Seg_6410" s="T251">bɨ͡ar</ta>
            <ta e="T253" id="Seg_6411" s="T252">agaj-bɨn</ta>
            <ta e="T256" id="Seg_6412" s="T255">bar</ta>
            <ta e="T257" id="Seg_6413" s="T256">bar</ta>
            <ta e="T259" id="Seg_6414" s="T258">tu͡ok</ta>
            <ta e="T260" id="Seg_6415" s="T259">ere</ta>
            <ta e="T261" id="Seg_6416" s="T260">trepke-nen</ta>
            <ta e="T265" id="Seg_6417" s="T264">on-ton</ta>
            <ta e="T266" id="Seg_6418" s="T265">bar-bat</ta>
            <ta e="T268" id="Seg_6419" s="T267">de</ta>
            <ta e="T271" id="Seg_6420" s="T270">bar</ta>
            <ta e="T273" id="Seg_6421" s="T272">bi͡er-en</ta>
            <ta e="T274" id="Seg_6422" s="T273">keːh-er</ta>
            <ta e="T275" id="Seg_6423" s="T274">bu͡o</ta>
            <ta e="T277" id="Seg_6424" s="T276">on-tu-ŋ</ta>
            <ta e="T278" id="Seg_6425" s="T277">momuj-a</ta>
            <ta e="T279" id="Seg_6426" s="T278">momuj-a</ta>
            <ta e="T281" id="Seg_6427" s="T280">bar-aːktɨː-r</ta>
            <ta e="T282" id="Seg_6428" s="T281">di͡e-n</ta>
            <ta e="T283" id="Seg_6429" s="T282">hi͡ese</ta>
            <ta e="T284" id="Seg_6430" s="T283">hili͡eb-i</ta>
            <ta e="T285" id="Seg_6431" s="T284">hi͡ese</ta>
            <ta e="T286" id="Seg_6432" s="T285">tot-t-o</ta>
            <ta e="T287" id="Seg_6433" s="T286">bɨhɨlaːk</ta>
            <ta e="T288" id="Seg_6434" s="T287">tu͡ok</ta>
            <ta e="T289" id="Seg_6435" s="T288">toholaːk</ta>
            <ta e="T290" id="Seg_6436" s="T289">tot-u͡o=j</ta>
            <ta e="T291" id="Seg_6437" s="T290">de</ta>
            <ta e="T292" id="Seg_6438" s="T291">ol</ta>
            <ta e="T293" id="Seg_6439" s="T292">kačɨgɨr-ɨ-t-ar</ta>
            <ta e="T296" id="Seg_6440" s="T295">kim-i</ta>
            <ta e="T297" id="Seg_6441" s="T296">bi͡er-bit</ta>
            <ta e="T298" id="Seg_6442" s="T297">kap-pɨt</ta>
            <ta e="T299" id="Seg_6443" s="T298">hili͡eb-i</ta>
            <ta e="T301" id="Seg_6444" s="T300">de</ta>
            <ta e="T302" id="Seg_6445" s="T301">onno</ta>
            <ta e="T303" id="Seg_6446" s="T302">ɨst-ɨː</ta>
            <ta e="T304" id="Seg_6447" s="T303">hataː-t-a</ta>
            <ta e="T305" id="Seg_6448" s="T304">bu͡o</ta>
            <ta e="T307" id="Seg_6449" s="T306">araːle</ta>
            <ta e="T310" id="Seg_6450" s="T309">oj</ta>
            <ta e="T311" id="Seg_6451" s="T310">tiːs-ter-i-n</ta>
            <ta e="T312" id="Seg_6452" s="T311">barɨ-tɨ-n</ta>
            <ta e="T313" id="Seg_6453" s="T312">kim-neː-t-e</ta>
            <ta e="T314" id="Seg_6454" s="T313">bɨhɨlaːk</ta>
            <ta e="T316" id="Seg_6455" s="T315">de</ta>
            <ta e="T317" id="Seg_6456" s="T316">ol</ta>
            <ta e="T318" id="Seg_6457" s="T317">bar-bɨt-a</ta>
            <ta e="T319" id="Seg_6458" s="T318">hi͡ese</ta>
            <ta e="T320" id="Seg_6459" s="T319">dogot-tor-u-n</ta>
            <ta e="T321" id="Seg_6460" s="T320">di͡ek</ta>
            <ta e="T323" id="Seg_6461" s="T322">hɨːr</ta>
            <ta e="T324" id="Seg_6462" s="T323">annɨ-tɨ-gar</ta>
            <ta e="T325" id="Seg_6463" s="T324">tüs-püt-tere</ta>
            <ta e="T326" id="Seg_6464" s="T325">onno</ta>
            <ta e="T327" id="Seg_6465" s="T326">ebe-ge</ta>
            <ta e="T328" id="Seg_6466" s="T327">hɨldʼ-al-lar</ta>
            <ta e="T330" id="Seg_6467" s="T329">tahaːra-bɨt</ta>
            <ta e="T331" id="Seg_6468" s="T330">boru͡or-d-a</ta>
            <ta e="T332" id="Seg_6469" s="T331">karaŋar-ar</ta>
            <ta e="T333" id="Seg_6470" s="T332">karaŋar-an</ta>
            <ta e="T334" id="Seg_6471" s="T333">ih-er</ta>
            <ta e="T336" id="Seg_6472" s="T335">kim</ta>
            <ta e="T337" id="Seg_6473" s="T336">ɨt-taːk</ta>
            <ta e="T338" id="Seg_6474" s="T337">e-ti-bit</ta>
            <ta e="T339" id="Seg_6475" s="T338">bihigi</ta>
            <ta e="T340" id="Seg_6476" s="T339">Djoŋgo</ta>
            <ta e="T341" id="Seg_6477" s="T340">čeːlkeː</ta>
            <ta e="T342" id="Seg_6478" s="T341">bagajɨ</ta>
            <ta e="T343" id="Seg_6479" s="T342">bosku͡oj</ta>
            <ta e="T344" id="Seg_6480" s="T343">bagajɨ</ta>
            <ta e="T345" id="Seg_6481" s="T344">taba-hɨt</ta>
            <ta e="T346" id="Seg_6482" s="T345">bagajɨ</ta>
            <ta e="T347" id="Seg_6483" s="T346">on-tu-ŋ</ta>
            <ta e="T349" id="Seg_6484" s="T348">kim</ta>
            <ta e="T351" id="Seg_6485" s="T350">d-i͡e-ŋ</ta>
            <ta e="T352" id="Seg_6486" s="T351">du͡o</ta>
            <ta e="T353" id="Seg_6487" s="T352">kim-i</ta>
            <ta e="T354" id="Seg_6488" s="T353">ü͡ör-ü</ta>
            <ta e="T355" id="Seg_6489" s="T354">barɨ-tɨ-n</ta>
            <ta e="T356" id="Seg_6490" s="T355">egel-en</ta>
            <ta e="T357" id="Seg_6491" s="T356">kel-i͡e</ta>
            <ta e="T359" id="Seg_6492" s="T358">o</ta>
            <ta e="T360" id="Seg_6493" s="T359">erij-ekt-i͡eg-e</ta>
            <ta e="T361" id="Seg_6494" s="T360">di͡e-n</ta>
            <ta e="T362" id="Seg_6495" s="T361">tögürüččü</ta>
            <ta e="T363" id="Seg_6496" s="T362">biːr</ta>
            <ta e="T364" id="Seg_6497" s="T363">hir-ge</ta>
            <ta e="T365" id="Seg_6498" s="T364">tur-u͡og-a</ta>
            <ta e="T367" id="Seg_6499" s="T366">bu</ta>
            <ta e="T368" id="Seg_6500" s="T367">egel</ta>
            <ta e="T369" id="Seg_6501" s="T368">di͡e-tek-ke</ta>
            <ta e="T370" id="Seg_6502" s="T369">ke</ta>
            <ta e="T372" id="Seg_6503" s="T371">de</ta>
            <ta e="T373" id="Seg_6504" s="T372">on-tu-but</ta>
            <ta e="T374" id="Seg_6505" s="T373">kim-i</ta>
            <ta e="T375" id="Seg_6506" s="T374">kim-neː-čči</ta>
            <ta e="T376" id="Seg_6507" s="T375">e-t-e</ta>
            <ta e="T377" id="Seg_6508" s="T376">maːjdɨn</ta>
            <ta e="T378" id="Seg_6509" s="T377">taba-lar-ɨ-ŋ</ta>
            <ta e="T379" id="Seg_6510" s="T378">baːr</ta>
            <ta e="T380" id="Seg_6511" s="T379">er-dek-terinen</ta>
            <ta e="T381" id="Seg_6512" s="T380">ke</ta>
            <ta e="T382" id="Seg_6513" s="T381">dʼi͡e-ge</ta>
            <ta e="T384" id="Seg_6514" s="T383">tugut</ta>
            <ta e="T385" id="Seg_6515" s="T384">abɨlakaːn</ta>
            <ta e="T386" id="Seg_6516" s="T385">du</ta>
            <ta e="T387" id="Seg_6517" s="T386">tu͡ok</ta>
            <ta e="T388" id="Seg_6518" s="T387">du</ta>
            <ta e="T389" id="Seg_6519" s="T388">abɨlakaːn</ta>
            <ta e="T390" id="Seg_6520" s="T389">tugut</ta>
            <ta e="T391" id="Seg_6521" s="T390">eː</ta>
            <ta e="T392" id="Seg_6522" s="T391">ulakan</ta>
            <ta e="T393" id="Seg_6523" s="T392">tugut-tan</ta>
            <ta e="T394" id="Seg_6524" s="T393">ulakan-kaːn</ta>
            <ta e="T395" id="Seg_6525" s="T394">dʼɨl-ɨ-nan</ta>
            <ta e="T396" id="Seg_6526" s="T395">ere</ta>
            <ta e="T397" id="Seg_6527" s="T396">ulakan</ta>
            <ta e="T399" id="Seg_6528" s="T398">ol</ta>
            <ta e="T400" id="Seg_6529" s="T399">kim-i</ta>
            <ta e="T401" id="Seg_6530" s="T400">de</ta>
            <ta e="T402" id="Seg_6531" s="T401">erij-e</ta>
            <ta e="T403" id="Seg_6532" s="T402">hɨt-ɨ͡ag-a</ta>
            <ta e="T404" id="Seg_6533" s="T403">bu͡o</ta>
            <ta e="T405" id="Seg_6534" s="T404">muŋn-u</ta>
            <ta e="T406" id="Seg_6535" s="T405">hɨt-aːččɨ</ta>
            <ta e="T407" id="Seg_6536" s="T406">iti</ta>
            <ta e="T408" id="Seg_6537" s="T407">tu͡ok</ta>
            <ta e="T409" id="Seg_6538" s="T408">tuguk-kaːn</ta>
            <ta e="T410" id="Seg_6539" s="T409">bagar-aːččɨ</ta>
            <ta e="T411" id="Seg_6540" s="T410">dürü</ta>
            <ta e="T412" id="Seg_6541" s="T411">möŋ-ö</ta>
            <ta e="T413" id="Seg_6542" s="T412">möŋ-ö</ta>
            <ta e="T415" id="Seg_6543" s="T414">hɨt-ɨ͡ag-a</ta>
            <ta e="T417" id="Seg_6544" s="T416">bu͡o</ta>
            <ta e="T418" id="Seg_6545" s="T417">oːnnʼuː-r</ta>
            <ta e="T419" id="Seg_6546" s="T418">bu͡o</ta>
            <ta e="T420" id="Seg_6547" s="T419">gini-ni</ta>
            <ta e="T421" id="Seg_6548" s="T420">gɨtta</ta>
            <ta e="T422" id="Seg_6549" s="T421">ke</ta>
            <ta e="T423" id="Seg_6550" s="T422">on-tu-ŋ</ta>
            <ta e="T424" id="Seg_6551" s="T423">kajdak</ta>
            <ta e="T425" id="Seg_6552" s="T424">da</ta>
            <ta e="T426" id="Seg_6553" s="T425">bu͡ol-l-a</ta>
            <ta e="T427" id="Seg_6554" s="T1150">tab-ɨ-h-a-s</ta>
            <ta e="T428" id="Seg_6555" s="T427">tab-ɨ-s-ar</ta>
            <ta e="T429" id="Seg_6556" s="T428">bu</ta>
            <ta e="T430" id="Seg_6557" s="T429">kurduk</ta>
            <ta e="T431" id="Seg_6558" s="T430">kajdak</ta>
            <ta e="T432" id="Seg_6559" s="T431">gɨn=ɨj</ta>
            <ta e="T433" id="Seg_6560" s="T432">maːma</ta>
            <ta e="T434" id="Seg_6561" s="T433">maːma</ta>
            <ta e="T435" id="Seg_6562" s="T434">d-iː-bin</ta>
            <ta e="T436" id="Seg_6563" s="T435">kör</ta>
            <ta e="T437" id="Seg_6564" s="T436">kör</ta>
            <ta e="T438" id="Seg_6565" s="T437">Djoŋgo-nu</ta>
            <ta e="T440" id="Seg_6566" s="T439">čoko</ta>
            <ta e="T441" id="Seg_6567" s="T440">Čoko</ta>
            <ta e="T442" id="Seg_6568" s="T441">di͡e-čči-bin</ta>
            <ta e="T444" id="Seg_6569" s="T443">Čoko</ta>
            <ta e="T445" id="Seg_6570" s="T444">kör</ta>
            <ta e="T447" id="Seg_6571" s="T446">kim-i</ta>
            <ta e="T448" id="Seg_6572" s="T447">hi͡e-ri</ta>
            <ta e="T449" id="Seg_6573" s="T448">gɨn-n-a</ta>
            <ta e="T450" id="Seg_6574" s="T449">d-iː-bin</ta>
            <ta e="T451" id="Seg_6575" s="T450">tugut-u</ta>
            <ta e="T452" id="Seg_6576" s="T451">hi͡e-ri</ta>
            <ta e="T453" id="Seg_6577" s="T452">gɨn-n-a</ta>
            <ta e="T455" id="Seg_6578" s="T454">o</ta>
            <ta e="T456" id="Seg_6579" s="T455">satana</ta>
            <ta e="T457" id="Seg_6580" s="T456">ɨgɨr</ta>
            <ta e="T458" id="Seg_6581" s="T457">ɨgɨr</ta>
            <ta e="T460" id="Seg_6582" s="T459">hɨla-t-t-a</ta>
            <ta e="T461" id="Seg_6583" s="T460">bɨhɨlaːk</ta>
            <ta e="T462" id="Seg_6584" s="T461">iti</ta>
            <ta e="T463" id="Seg_6585" s="T462">tugut-u</ta>
            <ta e="T464" id="Seg_6586" s="T463">kuttaː-t-a</ta>
            <ta e="T466" id="Seg_6587" s="T465">čoko</ta>
            <ta e="T467" id="Seg_6588" s="T466">Čoko</ta>
            <ta e="T468" id="Seg_6589" s="T467">Čoko</ta>
            <ta e="T469" id="Seg_6590" s="T468">d-iː-bin</ta>
            <ta e="T470" id="Seg_6591" s="T469">tahaːra</ta>
            <ta e="T471" id="Seg_6592" s="T470">taks-an</ta>
            <ta e="T472" id="Seg_6593" s="T471">bar-an</ta>
            <ta e="T473" id="Seg_6594" s="T472">ke</ta>
            <ta e="T475" id="Seg_6595" s="T474">o</ta>
            <ta e="T476" id="Seg_6596" s="T475">de</ta>
            <ta e="T477" id="Seg_6597" s="T476">on-tu-ŋ</ta>
            <ta e="T478" id="Seg_6598" s="T477">kötüt-en</ta>
            <ta e="T479" id="Seg_6599" s="T478">ih-eːktiː-r</ta>
            <ta e="T480" id="Seg_6600" s="T479">di͡e-n</ta>
            <ta e="T482" id="Seg_6601" s="T481">de</ta>
            <ta e="T483" id="Seg_6602" s="T482">ɨstan</ta>
            <ta e="T484" id="Seg_6603" s="T483">de</ta>
            <ta e="T485" id="Seg_6604" s="T484">hɨraj-ba-r</ta>
            <ta e="T486" id="Seg_6605" s="T485">min</ta>
            <ta e="T487" id="Seg_6606" s="T486">tara-s</ta>
            <ta e="T490" id="Seg_6607" s="T489">ittenne</ta>
            <ta e="T491" id="Seg_6608" s="T490">taraj-a</ta>
            <ta e="T492" id="Seg_6609" s="T491">hɨt-aːkt-ɨː-bɨn</ta>
            <ta e="T493" id="Seg_6610" s="T492">di͡e-n</ta>
            <ta e="T495" id="Seg_6611" s="T494">eː</ta>
            <ta e="T496" id="Seg_6612" s="T495">d-iː-bin</ta>
            <ta e="T497" id="Seg_6613" s="T496">maːma</ta>
            <ta e="T498" id="Seg_6614" s="T497">ɨl</ta>
            <ta e="T499" id="Seg_6615" s="T498">dʼa</ta>
            <ta e="T500" id="Seg_6616" s="T499">bu͡o</ta>
            <ta e="T502" id="Seg_6617" s="T501">halɨː-r</ta>
            <ta e="T503" id="Seg_6618" s="T502">kanʼɨː-r</ta>
            <ta e="T504" id="Seg_6619" s="T503">möŋ</ta>
            <ta e="T505" id="Seg_6620" s="T504">möŋ-tör-ör</ta>
            <ta e="T506" id="Seg_6621" s="T505">bu͡o</ta>
            <ta e="T507" id="Seg_6622" s="T506">kihi-ni</ta>
            <ta e="T508" id="Seg_6623" s="T507">ke</ta>
            <ta e="T510" id="Seg_6624" s="T509">möŋ-ü͡ög-ü-n</ta>
            <ta e="T511" id="Seg_6625" s="T510">bagar-ar</ta>
            <ta e="T513" id="Seg_6626" s="T512">minigi-n</ta>
            <ta e="T514" id="Seg_6627" s="T513">hogost-uː</ta>
            <ta e="T515" id="Seg_6628" s="T514">hɨt-aːktɨː-r</ta>
            <ta e="T516" id="Seg_6629" s="T515">di͡e-n</ta>
            <ta e="T518" id="Seg_6630" s="T517">de</ta>
            <ta e="T519" id="Seg_6631" s="T518">ol</ta>
            <ta e="T520" id="Seg_6632" s="T519">maːma-m</ta>
            <ta e="T521" id="Seg_6633" s="T520">tagɨs-t-a</ta>
            <ta e="T523" id="Seg_6634" s="T522">de</ta>
            <ta e="T524" id="Seg_6635" s="T523">tu͡ok</ta>
            <ta e="T525" id="Seg_6636" s="T524">bu͡ol-a-gɨn</ta>
            <ta e="T527" id="Seg_6637" s="T526">kör</ta>
            <ta e="T528" id="Seg_6638" s="T527">d-iː-bin</ta>
            <ta e="T529" id="Seg_6639" s="T528">minigi-n</ta>
            <ta e="T530" id="Seg_6640" s="T529">kɨrbɨː-r</ta>
            <ta e="T531" id="Seg_6641" s="T530">du͡o</ta>
            <ta e="T532" id="Seg_6642" s="T531">d-iː-bin</ta>
            <ta e="T533" id="Seg_6643" s="T532">oːnnʼuː-r</ta>
            <ta e="T534" id="Seg_6644" s="T533">kihi-ni</ta>
            <ta e="T535" id="Seg_6645" s="T534">ke</ta>
            <ta e="T536" id="Seg_6646" s="T535">araː</ta>
            <ta e="T537" id="Seg_6647" s="T536">da</ta>
            <ta e="T539" id="Seg_6648" s="T538">čičigere-n</ta>
            <ta e="T540" id="Seg_6649" s="T539">ɨtaː-n</ta>
            <ta e="T541" id="Seg_6650" s="T540">mörüleːkt-iː-bin</ta>
            <ta e="T542" id="Seg_6651" s="T541">heː</ta>
            <ta e="T544" id="Seg_6652" s="T543">ɨtaː</ta>
            <ta e="T545" id="Seg_6653" s="T544">ɨtaː-ma</ta>
            <ta e="T547" id="Seg_6654" s="T546">beje-ŋ</ta>
            <ta e="T548" id="Seg_6655" s="T547">ɨgɨr-bɨt-ɨ-ŋ</ta>
            <ta e="T549" id="Seg_6656" s="T548">diː-r</ta>
            <ta e="T550" id="Seg_6657" s="T549">kihi</ta>
            <ta e="T551" id="Seg_6658" s="T550">kül-ü͡ök</ta>
            <ta e="T553" id="Seg_6659" s="T552">on-tu-ŋ</ta>
            <ta e="T554" id="Seg_6660" s="T553">emi͡e</ta>
            <ta e="T555" id="Seg_6661" s="T554">bar-an</ta>
            <ta e="T556" id="Seg_6662" s="T555">kaːl-l-a</ta>
            <ta e="T557" id="Seg_6663" s="T556">ol</ta>
            <ta e="T558" id="Seg_6664" s="T557">tuguk-ka</ta>
            <ta e="T560" id="Seg_6665" s="T559">de</ta>
            <ta e="T562" id="Seg_6666" s="T561">kim-neː-beteg-i-m</ta>
            <ta e="T563" id="Seg_6667" s="T562">ol</ta>
            <ta e="T564" id="Seg_6668" s="T563">bi͡ek</ta>
            <ta e="T565" id="Seg_6669" s="T564">kün</ta>
            <ta e="T566" id="Seg_6670" s="T565">eːje</ta>
            <ta e="T567" id="Seg_6671" s="T566">oːnnʼoː-čču</ta>
            <ta e="T568" id="Seg_6672" s="T567">ol</ta>
            <ta e="T569" id="Seg_6673" s="T568">tugut-u</ta>
            <ta e="T570" id="Seg_6674" s="T569">gɨtta</ta>
            <ta e="T571" id="Seg_6675" s="T570">tu͡ok-ka</ta>
            <ta e="T572" id="Seg_6676" s="T571">bagar-bɨt</ta>
            <ta e="T574" id="Seg_6677" s="T573">iti</ta>
            <ta e="T575" id="Seg_6678" s="T574">tugut</ta>
            <ta e="T576" id="Seg_6679" s="T575">bu͡ol-u͡og-u-n</ta>
            <ta e="T577" id="Seg_6680" s="T576">naːda</ta>
            <ta e="T579" id="Seg_6681" s="T578">de</ta>
            <ta e="T580" id="Seg_6682" s="T579">on-ton</ta>
            <ta e="T581" id="Seg_6683" s="T580">tahaːra-bɨt</ta>
            <ta e="T582" id="Seg_6684" s="T581">karaŋar-d-a</ta>
            <ta e="T583" id="Seg_6685" s="T582">bu͡o</ta>
            <ta e="T585" id="Seg_6686" s="T584">bosku͡oj-ɨn</ta>
            <ta e="T586" id="Seg_6687" s="T585">da</ta>
            <ta e="T587" id="Seg_6688" s="T586">kim-mit</ta>
            <ta e="T588" id="Seg_6689" s="T587">ohok-put</ta>
            <ta e="T589" id="Seg_6690" s="T588">ke</ta>
            <ta e="T591" id="Seg_6691" s="T590">maːma-m</ta>
            <ta e="T592" id="Seg_6692" s="T591">delbi</ta>
            <ta e="T593" id="Seg_6693" s="T592">kaːlaː-bɨt</ta>
            <ta e="T598" id="Seg_6694" s="T597">kim</ta>
            <ta e="T599" id="Seg_6695" s="T598">mas</ta>
            <ta e="T600" id="Seg_6696" s="T599">ke</ta>
            <ta e="T601" id="Seg_6697" s="T600">ubaj-ar-a</ta>
            <ta e="T602" id="Seg_6698" s="T601">ke</ta>
            <ta e="T603" id="Seg_6699" s="T602">kɨtar-čɨ</ta>
            <ta e="T604" id="Seg_6700" s="T603">ohop-put</ta>
            <ta e="T606" id="Seg_6701" s="T605">čajnik-tar</ta>
            <ta e="T607" id="Seg_6702" s="T606">köt-ü-t-en</ta>
            <ta e="T609" id="Seg_6703" s="T608">telibireː-ktiː-l-ler</ta>
            <ta e="T610" id="Seg_6704" s="T609">di͡e-n</ta>
            <ta e="T611" id="Seg_6705" s="T610">kappak-tara</ta>
            <ta e="T613" id="Seg_6706" s="T612">kü͡ös-püt</ta>
            <ta e="T614" id="Seg_6707" s="T613">laglɨ͡a</ta>
            <ta e="T615" id="Seg_6708" s="T614">tur-aːktɨː-r</ta>
            <ta e="T616" id="Seg_6709" s="T615">di͡e-An</ta>
            <ta e="T617" id="Seg_6710" s="T616">laglɨ͡a</ta>
            <ta e="T620" id="Seg_6711" s="T619">itij</ta>
            <ta e="T621" id="Seg_6712" s="T620">aːn</ta>
            <ta e="T622" id="Seg_6713" s="T621">tünnüg-ü</ta>
            <ta e="T623" id="Seg_6714" s="T622">arɨj-d-a</ta>
            <ta e="T624" id="Seg_6715" s="T623">ü͡öleh-e</ta>
            <ta e="T625" id="Seg_6716" s="T624">arɨj-d-a</ta>
            <ta e="T627" id="Seg_6717" s="T626">ü͡öles</ta>
            <ta e="T628" id="Seg_6718" s="T627">bu͡ol-aːččɨ</ta>
            <ta e="T629" id="Seg_6719" s="T628">kim</ta>
            <ta e="T630" id="Seg_6720" s="T629">bü͡öleː-čči-ler</ta>
            <ta e="T634" id="Seg_6721" s="T633">tɨ͡al</ta>
            <ta e="T635" id="Seg_6722" s="T634">kiːr-i͡eg-i-n</ta>
            <ta e="T636" id="Seg_6723" s="T635">ke</ta>
            <ta e="T638" id="Seg_6724" s="T637">de</ta>
            <ta e="T639" id="Seg_6725" s="T638">ol</ta>
            <ta e="T640" id="Seg_6726" s="T639">arɨj-d-a</ta>
            <ta e="T641" id="Seg_6727" s="T640">heː</ta>
            <ta e="T643" id="Seg_6728" s="T642">min</ta>
            <ta e="T644" id="Seg_6729" s="T643">du</ta>
            <ta e="T645" id="Seg_6730" s="T644">hürdeːk</ta>
            <ta e="T646" id="Seg_6731" s="T645">bagar-aːččɨ-bɨn</ta>
            <ta e="T647" id="Seg_6732" s="T646">tünnük-ke</ta>
            <ta e="T648" id="Seg_6733" s="T647">tünnük-ke</ta>
            <ta e="T649" id="Seg_6734" s="T648">kör-ü͡öp-pü-n</ta>
            <ta e="T650" id="Seg_6735" s="T649">ke</ta>
            <ta e="T652" id="Seg_6736" s="T651">mɨlaj-a</ta>
            <ta e="T653" id="Seg_6737" s="T652">mɨlaj-a</ta>
            <ta e="T654" id="Seg_6738" s="T653">olor-oːčču-bun</ta>
            <ta e="T656" id="Seg_6739" s="T655">bosku͡oj-in</ta>
            <ta e="T657" id="Seg_6740" s="T656">da</ta>
            <ta e="T658" id="Seg_6741" s="T657">i͡e</ta>
            <ta e="T659" id="Seg_6742" s="T658">kim</ta>
            <ta e="T660" id="Seg_6743" s="T659">ɨj</ta>
            <ta e="T663" id="Seg_6744" s="T662">ɨj</ta>
            <ta e="T664" id="Seg_6745" s="T663">erej-keːn</ta>
            <ta e="T665" id="Seg_6746" s="T664">e-bit-e</ta>
            <ta e="T666" id="Seg_6747" s="T665">eh</ta>
            <ta e="T668" id="Seg_6748" s="T667">ɨj-bɨt</ta>
            <ta e="T669" id="Seg_6749" s="T668">bosku͡oj</ta>
            <ta e="T670" id="Seg_6750" s="T669">bagajɨ</ta>
            <ta e="T671" id="Seg_6751" s="T670">no</ta>
            <ta e="T672" id="Seg_6752" s="T671">kap-karaŋa</ta>
            <ta e="T674" id="Seg_6753" s="T673">tu͡ok-tar</ta>
            <ta e="T675" id="Seg_6754" s="T674">köt-öːččü-ler</ta>
            <ta e="T676" id="Seg_6755" s="T675">e-bit</ta>
            <ta e="T677" id="Seg_6756" s="T676">min</ta>
            <ta e="T679" id="Seg_6757" s="T678">bil-bet</ta>
            <ta e="T680" id="Seg_6758" s="T679">e-ti-m</ta>
            <ta e="T682" id="Seg_6759" s="T681">leŋkej</ta>
            <ta e="T683" id="Seg_6760" s="T682">kördük</ta>
            <ta e="T684" id="Seg_6761" s="T683">leŋkej-dar</ta>
            <ta e="T685" id="Seg_6762" s="T684">di͡e-n</ta>
            <ta e="T686" id="Seg_6763" s="T685">hu͡og-a</ta>
            <ta e="T687" id="Seg_6764" s="T686">küččügüj-dar</ta>
            <ta e="T689" id="Seg_6765" s="T688">mejiː-te</ta>
            <ta e="T690" id="Seg_6766" s="T689">hu͡ok-tar</ta>
            <ta e="T691" id="Seg_6767" s="T690">di͡e-čči-bin</ta>
            <ta e="T692" id="Seg_6768" s="T691">mejiː-te</ta>
            <ta e="T693" id="Seg_6769" s="T692">hu͡ok-tar</ta>
            <ta e="T694" id="Seg_6770" s="T693">hɨldʼ-al-lar</ta>
            <ta e="T695" id="Seg_6771" s="T694">maːma</ta>
            <ta e="T696" id="Seg_6772" s="T695">kör</ta>
            <ta e="T699" id="Seg_6773" s="T698">iti</ta>
            <ta e="T700" id="Seg_6774" s="T699">kim</ta>
            <ta e="T701" id="Seg_6775" s="T700">eh</ta>
            <ta e="T702" id="Seg_6776" s="T701">bu</ta>
            <ta e="T703" id="Seg_6777" s="T702">čok</ta>
            <ta e="T704" id="Seg_6778" s="T703">köt-ör</ta>
            <ta e="T705" id="Seg_6779" s="T704">bu͡o</ta>
            <ta e="T706" id="Seg_6780" s="T705">truba-tan</ta>
            <ta e="T707" id="Seg_6781" s="T706">ke</ta>
            <ta e="T709" id="Seg_6782" s="T708">onu-ga</ta>
            <ta e="T710" id="Seg_6783" s="T709">möŋ-s-öl-lör</ta>
            <ta e="T711" id="Seg_6784" s="T710">e-bit</ta>
            <ta e="T712" id="Seg_6785" s="T711">iti-ler-i-ŋ</ta>
            <ta e="T714" id="Seg_6786" s="T713">bagajɨ</ta>
            <ta e="T716" id="Seg_6787" s="T715">on-ton</ta>
            <ta e="T717" id="Seg_6788" s="T716">töhö</ta>
            <ta e="T718" id="Seg_6789" s="T717">eme</ta>
            <ta e="T719" id="Seg_6790" s="T718">maːma-m</ta>
            <ta e="T720" id="Seg_6791" s="T719">iːsten</ta>
            <ta e="T721" id="Seg_6792" s="T720">olor-or</ta>
            <ta e="T722" id="Seg_6793" s="T721">iːsten</ta>
            <ta e="T723" id="Seg_6794" s="T722">olor-or</ta>
            <ta e="T725" id="Seg_6795" s="T724">orguːjakaːn</ta>
            <ta e="T726" id="Seg_6796" s="T725">orguːjakaːn</ta>
            <ta e="T727" id="Seg_6797" s="T726">diː</ta>
            <ta e="T728" id="Seg_6798" s="T727">diː</ta>
            <ta e="T729" id="Seg_6799" s="T728">ɨllaː-n</ta>
            <ta e="T730" id="Seg_6800" s="T729">olor-or</ta>
            <ta e="T731" id="Seg_6801" s="T730">orguːjak</ta>
            <ta e="T733" id="Seg_6802" s="T732">Djaku-bɨt</ta>
            <ta e="T734" id="Seg_6803" s="T733">hɨt-ar</ta>
            <ta e="T736" id="Seg_6804" s="T735">nʼik</ta>
            <ta e="T737" id="Seg_6805" s="T736">da</ta>
            <ta e="T738" id="Seg_6806" s="T737">gɨm-mat</ta>
            <ta e="T740" id="Seg_6807" s="T739">hɨlaj-bɨt</ta>
            <ta e="T741" id="Seg_6808" s="T740">bɨhɨlaːk</ta>
            <ta e="T742" id="Seg_6809" s="T741">tugut-u-n</ta>
            <ta e="T743" id="Seg_6810" s="T742">gɨtta</ta>
            <ta e="T745" id="Seg_6811" s="T744">on-ton</ta>
            <ta e="T746" id="Seg_6812" s="T745">kim</ta>
            <ta e="T749" id="Seg_6813" s="T748">bar-d-a</ta>
            <ta e="T750" id="Seg_6814" s="T749">bu͡o</ta>
            <ta e="T753" id="Seg_6815" s="T752">ol</ta>
            <ta e="T754" id="Seg_6816" s="T753">tu͡ok</ta>
            <ta e="T755" id="Seg_6817" s="T754">itte</ta>
            <ta e="T756" id="Seg_6818" s="T755">bütte</ta>
            <ta e="T757" id="Seg_6819" s="T756">min</ta>
            <ta e="T759" id="Seg_6820" s="T758">teːte-m</ta>
            <ta e="T760" id="Seg_6821" s="T759">hu͡ok-kaːn-ɨ-nan</ta>
            <ta e="T761" id="Seg_6822" s="T760">radio-na</ta>
            <ta e="T762" id="Seg_6823" s="T761">vklučaj-d-ɨ-n-a-bɨn</ta>
            <ta e="T764" id="Seg_6824" s="T763">heː</ta>
            <ta e="T765" id="Seg_6825" s="T764">gaːgɨnat-aːkt-ɨː-bɨn</ta>
            <ta e="T766" id="Seg_6826" s="T765">de</ta>
            <ta e="T768" id="Seg_6827" s="T767">eː</ta>
            <ta e="T769" id="Seg_6828" s="T768">orguːj</ta>
            <ta e="T770" id="Seg_6829" s="T769">gɨn</ta>
            <ta e="T771" id="Seg_6830" s="T770">beji</ta>
            <ta e="T772" id="Seg_6831" s="T771">talɨgɨraːt-ɨ-ma</ta>
            <ta e="T774" id="Seg_6832" s="T773">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T775" id="Seg_6833" s="T774">ihill-iː</ta>
            <ta e="T776" id="Seg_6834" s="T775">hɨt-ɨ͡a-m</ta>
            <ta e="T777" id="Seg_6835" s="T776">bu͡o</ta>
            <ta e="T780" id="Seg_6836" s="T779">kim-ner-i</ta>
            <ta e="T781" id="Seg_6837" s="T780">di͡e</ta>
            <ta e="T782" id="Seg_6838" s="T781">ihill-iː-bin</ta>
            <ta e="T784" id="Seg_6839" s="T783">Kitajetsar-dar-lar</ta>
            <ta e="T785" id="Seg_6840" s="T784">tu͡ok-tar</ta>
            <ta e="T786" id="Seg_6841" s="T785">du</ta>
            <ta e="T788" id="Seg_6842" s="T787">o-lor-u</ta>
            <ta e="T789" id="Seg_6843" s="T788">ihill-en</ta>
            <ta e="T790" id="Seg_6844" s="T789">bu͡ola</ta>
            <ta e="T791" id="Seg_6845" s="T790">hɨt-aːkt-ɨː-bɨn</ta>
            <ta e="T792" id="Seg_6846" s="T791">di͡e-n</ta>
            <ta e="T793" id="Seg_6847" s="T792">oččogo</ta>
            <ta e="T794" id="Seg_6848" s="T793">beseleː</ta>
            <ta e="T795" id="Seg_6849" s="T794">bagajɨ</ta>
            <ta e="T796" id="Seg_6850" s="T795">bil-e-bin</ta>
            <ta e="T797" id="Seg_6851" s="T796">on-ton</ta>
            <ta e="T798" id="Seg_6852" s="T797">hi͡ese</ta>
            <ta e="T799" id="Seg_6853" s="T798">nʼuːčča-lar</ta>
            <ta e="T800" id="Seg_6854" s="T799">bul-lu-m</ta>
            <ta e="T801" id="Seg_6855" s="T800">on-ton</ta>
            <ta e="T802" id="Seg_6856" s="T801">büt-en</ta>
            <ta e="T803" id="Seg_6857" s="T802">kaːl-bɨt</ta>
            <ta e="T805" id="Seg_6858" s="T804">de</ta>
            <ta e="T806" id="Seg_6859" s="T805">büt-e-r-en</ta>
            <ta e="T807" id="Seg_6860" s="T806">keːh-i͡ek-ke</ta>
            <ta e="T808" id="Seg_6861" s="T807">büt-e-r</ta>
            <ta e="T809" id="Seg_6862" s="T808">teːte-ŋ</ta>
            <ta e="T810" id="Seg_6863" s="T809">kel-i͡e</ta>
            <ta e="T811" id="Seg_6864" s="T810">heː</ta>
            <ta e="T813" id="Seg_6865" s="T812">küm-müt</ta>
            <ta e="T814" id="Seg_6866" s="T813">ɨj-bɨt</ta>
            <ta e="T815" id="Seg_6867" s="T814">ɨraːt-t-a</ta>
            <ta e="T816" id="Seg_6868" s="T815">bu͡o</ta>
            <ta e="T817" id="Seg_6869" s="T816">ü͡öhe</ta>
            <ta e="T818" id="Seg_6870" s="T817">bu͡ol-l-a</ta>
            <ta e="T819" id="Seg_6871" s="T818">ama</ta>
            <ta e="T820" id="Seg_6872" s="T819">bosku͡oj</ta>
            <ta e="T822" id="Seg_6873" s="T821">ama</ta>
            <ta e="T823" id="Seg_6874" s="T822">hɨrdɨk</ta>
            <ta e="T824" id="Seg_6875" s="T823">künüs</ta>
            <ta e="T825" id="Seg_6876" s="T824">kördük</ta>
            <ta e="T827" id="Seg_6877" s="T826">o</ta>
            <ta e="T828" id="Seg_6878" s="T827">taba-lar-bɨt</ta>
            <ta e="T830" id="Seg_6879" s="T829">ebe</ta>
            <ta e="T831" id="Seg_6880" s="T830">üstün</ta>
            <ta e="T832" id="Seg_6881" s="T831">kötüt-e</ta>
            <ta e="T833" id="Seg_6882" s="T832">hɨldʼ-aːktɨː-l-lar</ta>
            <ta e="T834" id="Seg_6883" s="T833">de</ta>
            <ta e="T835" id="Seg_6884" s="T834">ama</ta>
            <ta e="T836" id="Seg_6885" s="T835">bosku͡oj-in</ta>
            <ta e="T837" id="Seg_6886" s="T836">da</ta>
            <ta e="T839" id="Seg_6887" s="T838">kim</ta>
            <ta e="T840" id="Seg_6888" s="T839">buru͡o-lara</ta>
            <ta e="T841" id="Seg_6889" s="T840">aš</ta>
            <ta e="T842" id="Seg_6890" s="T841">köt-ör</ta>
            <ta e="T844" id="Seg_6891" s="T843">oj</ta>
            <ta e="T845" id="Seg_6892" s="T844">ama</ta>
            <ta e="T846" id="Seg_6893" s="T845">bosku͡oj</ta>
            <ta e="T847" id="Seg_6894" s="T846">kim</ta>
            <ta e="T848" id="Seg_6895" s="T847">ke</ta>
            <ta e="T849" id="Seg_6896" s="T848">kaːr-ɨ-ŋ</ta>
            <ta e="T850" id="Seg_6897" s="T849">ke</ta>
            <ta e="T851" id="Seg_6898" s="T850">kim-ŋe</ta>
            <ta e="T852" id="Seg_6899" s="T851">lunaː-tan</ta>
            <ta e="T853" id="Seg_6900" s="T852">ama</ta>
            <ta e="T854" id="Seg_6901" s="T853">kim-niː-r</ta>
            <ta e="T855" id="Seg_6902" s="T854">gɨlba-ŋnɨː-r</ta>
            <ta e="T857" id="Seg_6903" s="T856">on-ton</ta>
            <ta e="T858" id="Seg_6904" s="T857">on-ton</ta>
            <ta e="T859" id="Seg_6905" s="T858">beːbe</ta>
            <ta e="T860" id="Seg_6906" s="T859">ɨp-pɨt</ta>
            <ta e="T863" id="Seg_6907" s="T862">gɨn-ar</ta>
            <ta e="T864" id="Seg_6908" s="T863">bu</ta>
            <ta e="T865" id="Seg_6909" s="T864">kim-niː-r</ta>
            <ta e="T866" id="Seg_6910" s="T865">muŋurguː-r</ta>
            <ta e="T868" id="Seg_6911" s="T867">beː</ta>
            <ta e="T869" id="Seg_6912" s="T868">ču͡oraːn</ta>
            <ta e="T870" id="Seg_6913" s="T869">tɨ͡ahaː-t-a</ta>
            <ta e="T871" id="Seg_6914" s="T870">araj</ta>
            <ta e="T872" id="Seg_6915" s="T871">küpüleːn</ta>
            <ta e="T877" id="Seg_6916" s="T876">teːte-bit</ta>
            <ta e="T878" id="Seg_6917" s="T877">ih-er</ta>
            <ta e="T879" id="Seg_6918" s="T878">teːte-bit</ta>
            <ta e="T880" id="Seg_6919" s="T879">heː</ta>
            <ta e="T882" id="Seg_6920" s="T881">de</ta>
            <ta e="T883" id="Seg_6921" s="T882">ol</ta>
            <ta e="T884" id="Seg_6922" s="T883">kel-l-e</ta>
            <ta e="T886" id="Seg_6923" s="T885">min</ta>
            <ta e="T887" id="Seg_6924" s="T886">du</ta>
            <ta e="T888" id="Seg_6925" s="T887">bil-beteg-i-m</ta>
            <ta e="T889" id="Seg_6926" s="T888">bu͡o</ta>
            <ta e="T890" id="Seg_6927" s="T889">mas-ta-n-a</ta>
            <ta e="T891" id="Seg_6928" s="T890">bar-bɨt</ta>
            <ta e="T892" id="Seg_6929" s="T891">d-iː-bin</ta>
            <ta e="T893" id="Seg_6930" s="T892">bu͡o</ta>
            <ta e="T895" id="Seg_6931" s="T894">gini-ŋ</ta>
            <ta e="T896" id="Seg_6932" s="T895">bu͡o</ta>
            <ta e="T897" id="Seg_6933" s="T896">Dudʼinka</ta>
            <ta e="T898" id="Seg_6934" s="T897">di͡ek</ta>
            <ta e="T899" id="Seg_6935" s="T898">hɨldʼ-a-bɨt</ta>
            <ta e="T900" id="Seg_6936" s="T899">i</ta>
            <ta e="T901" id="Seg_6937" s="T900">Dujunaː</ta>
            <ta e="T902" id="Seg_6938" s="T901">di͡ek</ta>
            <ta e="T904" id="Seg_6939" s="T903">de</ta>
            <ta e="T905" id="Seg_6940" s="T904">ol</ta>
            <ta e="T906" id="Seg_6941" s="T905">kel-l-e</ta>
            <ta e="T909" id="Seg_6942" s="T908">karaŋar-bɨt-ɨ-n</ta>
            <ta e="T910" id="Seg_6943" s="T909">kenne</ta>
            <ta e="T911" id="Seg_6944" s="T910">ke</ta>
            <ta e="T913" id="Seg_6945" s="T912">o</ta>
            <ta e="T914" id="Seg_6946" s="T913">teːte-m</ta>
            <ta e="T915" id="Seg_6947" s="T914">kel-l-e</ta>
            <ta e="T917" id="Seg_6948" s="T916">o</ta>
            <ta e="T918" id="Seg_6949" s="T917">maːma-m</ta>
            <ta e="T919" id="Seg_6950" s="T918">kömölöh-ö</ta>
            <ta e="T920" id="Seg_6951" s="T919">bar-d-a</ta>
            <ta e="T921" id="Seg_6952" s="T920">hü͡ör-d-e</ta>
            <ta e="T922" id="Seg_6953" s="T921">kanʼaː-t-a</ta>
            <ta e="T923" id="Seg_6954" s="T922">küpüleːn-ten</ta>
            <ta e="T924" id="Seg_6955" s="T923">taba-larɨ-n</ta>
            <ta e="T925" id="Seg_6956" s="T924">ɨːt-tɨ-lar</ta>
            <ta e="T926" id="Seg_6957" s="T925">kanʼaː-tɨ-lar</ta>
            <ta e="T928" id="Seg_6958" s="T927">o</ta>
            <ta e="T929" id="Seg_6959" s="T928">teːte-m</ta>
            <ta e="T930" id="Seg_6960" s="T929">mesok-tar-daːk</ta>
            <ta e="T931" id="Seg_6961" s="T930">kel-l-e</ta>
            <ta e="T933" id="Seg_6962" s="T932">küːl-ler-i-n</ta>
            <ta e="T934" id="Seg_6963" s="T933">kiːl-ler-d-e</ta>
            <ta e="T936" id="Seg_6964" s="T935">de</ta>
            <ta e="T937" id="Seg_6965" s="T936">kostoː-t-oloː-t-o</ta>
            <ta e="T938" id="Seg_6966" s="T937">o</ta>
            <ta e="T939" id="Seg_6967" s="T938">ča</ta>
            <ta e="T940" id="Seg_6968" s="T939">kempi͡et</ta>
            <ta e="T941" id="Seg_6969" s="T940">üleger-i-n</ta>
            <ta e="T942" id="Seg_6970" s="T941">egel-bit</ta>
            <ta e="T944" id="Seg_6971" s="T943">on-ton</ta>
            <ta e="T945" id="Seg_6972" s="T944">bosku͡oj</ta>
            <ta e="T946" id="Seg_6973" s="T945">bagajɨ</ta>
            <ta e="T947" id="Seg_6974" s="T946">bi͡ek</ta>
            <ta e="T948" id="Seg_6975" s="T947">öjd-üː-bün</ta>
            <ta e="T950" id="Seg_6976" s="T949">kim</ta>
            <ta e="T951" id="Seg_6977" s="T950">čaːs-tar</ta>
            <ta e="T952" id="Seg_6978" s="T951">du͡o</ta>
            <ta e="T953" id="Seg_6979" s="T952">bosku͡oj</ta>
            <ta e="T954" id="Seg_6980" s="T953">bagajɨ</ta>
            <ta e="T955" id="Seg_6981" s="T954">kim</ta>
            <ta e="T956" id="Seg_6982" s="T955">hirkili-nnen</ta>
            <ta e="T957" id="Seg_6983" s="T956">oŋohull-u-but</ta>
            <ta e="T959" id="Seg_6984" s="T958">on-tu-ŋ</ta>
            <ta e="T961" id="Seg_6985" s="T960">knopka-lar-daːk</ta>
            <ta e="T962" id="Seg_6986" s="T961">tu͡ok</ta>
            <ta e="T963" id="Seg_6987" s="T962">ere</ta>
            <ta e="T964" id="Seg_6988" s="T963">ama</ta>
            <ta e="T965" id="Seg_6989" s="T964">bosku͡oj</ta>
            <ta e="T966" id="Seg_6990" s="T965">oduːrgaː-kt-ɨː-bɨn</ta>
            <ta e="T967" id="Seg_6991" s="T966">ama</ta>
            <ta e="T968" id="Seg_6992" s="T967">bosku͡oj</ta>
            <ta e="T970" id="Seg_6993" s="T969">de</ta>
            <ta e="T971" id="Seg_6994" s="T970">tɨːt-ɨ-ma</ta>
            <ta e="T972" id="Seg_6995" s="T971">höp</ta>
            <ta e="T973" id="Seg_6996" s="T972">bu͡ol-u͡o</ta>
            <ta e="T974" id="Seg_6997" s="T973">diː-l-ler</ta>
            <ta e="T976" id="Seg_6998" s="T975">tɨːt-tar-aːččɨ-ta</ta>
            <ta e="T977" id="Seg_6999" s="T976">hu͡ok-tar</ta>
            <ta e="T978" id="Seg_7000" s="T977">bu͡o</ta>
            <ta e="T980" id="Seg_7001" s="T979">aldʼat-ɨ͡a-ŋ</ta>
            <ta e="T981" id="Seg_7002" s="T980">kaja</ta>
            <ta e="T982" id="Seg_7003" s="T981">heː</ta>
            <ta e="T984" id="Seg_7004" s="T983">de</ta>
            <ta e="T985" id="Seg_7005" s="T984">uːr-du-m</ta>
            <ta e="T986" id="Seg_7006" s="T985">hürdeːk</ta>
            <ta e="T987" id="Seg_7007" s="T986">kör-ü͡öp-pü-n</ta>
            <ta e="T988" id="Seg_7008" s="T987">bagar-a-bɨn</ta>
            <ta e="T989" id="Seg_7009" s="T988">tɨːt-al-ɨ͡ap-pɨ-n</ta>
            <ta e="T990" id="Seg_7010" s="T989">bagar-a-bɨn</ta>
            <ta e="T991" id="Seg_7011" s="T990">bu͡o</ta>
            <ta e="T993" id="Seg_7012" s="T992">on-ton</ta>
            <ta e="T994" id="Seg_7013" s="T993">kim</ta>
            <ta e="T995" id="Seg_7014" s="T994">kim-i</ta>
            <ta e="T996" id="Seg_7015" s="T995">egel-bit-e</ta>
            <ta e="T997" id="Seg_7016" s="T996">mini͡e-ke</ta>
            <ta e="T999" id="Seg_7017" s="T998">kompas-kaːn</ta>
            <ta e="T1000" id="Seg_7018" s="T999">kuččuguj</ta>
            <ta e="T1002" id="Seg_7019" s="T1001">o-nu</ta>
            <ta e="T1003" id="Seg_7020" s="T1002">bi͡ek</ta>
            <ta e="T1004" id="Seg_7021" s="T1003">oːnnʼ-uː</ta>
            <ta e="T1005" id="Seg_7022" s="T1004">hɨt-aːččɨ-bɨn</ta>
            <ta e="T1006" id="Seg_7023" s="T1005">araː</ta>
            <ta e="T1007" id="Seg_7024" s="T1006">bosku͡oj-ɨn</ta>
            <ta e="T1008" id="Seg_7025" s="T1007">du͡o</ta>
            <ta e="T1009" id="Seg_7026" s="T1008">urut</ta>
            <ta e="T1012" id="Seg_7027" s="T1011">egel-bit-e</ta>
            <ta e="T1013" id="Seg_7028" s="T1012">bu</ta>
            <ta e="T1014" id="Seg_7029" s="T1013">korduk</ta>
            <ta e="T1015" id="Seg_7030" s="T1014">oːnnʼuːr-u</ta>
            <ta e="T1016" id="Seg_7031" s="T1015">ke</ta>
            <ta e="T1018" id="Seg_7032" s="T1017">o</ta>
            <ta e="T1019" id="Seg_7033" s="T1018">min</ta>
            <ta e="T1020" id="Seg_7034" s="T1019">muŋ</ta>
            <ta e="T1021" id="Seg_7035" s="T1020">hu͡ok</ta>
            <ta e="T1023" id="Seg_7036" s="T1022">bosku͡oj-ɨn</ta>
            <ta e="T1024" id="Seg_7037" s="T1023">da</ta>
            <ta e="T1026" id="Seg_7038" s="T1025">o</ta>
            <ta e="T1027" id="Seg_7039" s="T1026">eni͡e-ke</ta>
            <ta e="T1029" id="Seg_7040" s="T1028">diː-r</ta>
            <ta e="T1031" id="Seg_7041" s="T1030">de</ta>
            <ta e="T1032" id="Seg_7042" s="T1031">ol</ta>
            <ta e="T1033" id="Seg_7043" s="T1032">kepset-el-ler</ta>
            <ta e="T1034" id="Seg_7044" s="T1033">eni</ta>
            <ta e="T1035" id="Seg_7045" s="T1034">ke</ta>
            <ta e="T1036" id="Seg_7046" s="T1035">min</ta>
            <ta e="T1037" id="Seg_7047" s="T1036">utuj-an</ta>
            <ta e="T1038" id="Seg_7048" s="T1037">kaːl-bɨp-pɨn</ta>
            <ta e="T1040" id="Seg_7049" s="T1039">harsɨ͡arda</ta>
            <ta e="T1041" id="Seg_7050" s="T1040">uže</ta>
            <ta e="T1042" id="Seg_7051" s="T1041">uhukt-a-bɨn</ta>
            <ta e="T1043" id="Seg_7052" s="T1042">o</ta>
            <ta e="T1044" id="Seg_7053" s="T1043">ohok</ta>
            <ta e="T1045" id="Seg_7054" s="T1044">emi͡e</ta>
            <ta e="T1046" id="Seg_7055" s="T1045">köt-ü-t-e</ta>
            <ta e="T1047" id="Seg_7056" s="T1046">tur-ar</ta>
            <ta e="T1049" id="Seg_7057" s="T1048">bosku͡oj-ɨn</ta>
            <ta e="T1050" id="Seg_7058" s="T1049">du͡o</ta>
            <ta e="T1051" id="Seg_7059" s="T1050">kim</ta>
            <ta e="T1052" id="Seg_7060" s="T1051">da</ta>
            <ta e="T1053" id="Seg_7061" s="T1052">hu͡ok</ta>
            <ta e="T1054" id="Seg_7062" s="T1053">dʼi͡e-bit</ta>
            <ta e="T1055" id="Seg_7063" s="T1054">ih-i-ger</ta>
            <ta e="T1057" id="Seg_7064" s="T1056">ol</ta>
            <ta e="T1058" id="Seg_7065" s="T1057">taba-larɨ-n</ta>
            <ta e="T1059" id="Seg_7066" s="T1058">tut-al-lar</ta>
            <ta e="T1060" id="Seg_7067" s="T1059">e-bit</ta>
            <ta e="T1062" id="Seg_7068" s="T1061">tutt-a</ta>
            <ta e="T1063" id="Seg_7069" s="T1062">taks-ɨ-bɨt-tar</ta>
            <ta e="T1065" id="Seg_7070" s="T1064">ɨ͡al-lar-daːk</ta>
            <ta e="T1066" id="Seg_7071" s="T1065">e-ti-bit</ta>
            <ta e="T1067" id="Seg_7072" s="T1066">tak</ta>
            <ta e="T1068" id="Seg_7073" s="T1067">da</ta>
            <ta e="T1069" id="Seg_7074" s="T1068">baːr</ta>
            <ta e="T1070" id="Seg_7075" s="T1069">e-ti-lere</ta>
            <ta e="T1071" id="Seg_7076" s="T1070">ɨ͡al-lar-bɨt</ta>
            <ta e="T1073" id="Seg_7077" s="T1072">de</ta>
            <ta e="T1074" id="Seg_7078" s="T1073">ol</ta>
            <ta e="T1075" id="Seg_7079" s="T1074">komuj-du-lar</ta>
            <ta e="T1076" id="Seg_7080" s="T1075">ü͡ör-deri-n</ta>
            <ta e="T1077" id="Seg_7081" s="T1076">komu-n-al-lar</ta>
            <ta e="T1078" id="Seg_7082" s="T1077">taba</ta>
            <ta e="T1079" id="Seg_7083" s="T1078">tu͡ok</ta>
            <ta e="T1080" id="Seg_7084" s="T1079">köh-ö-büt</ta>
            <ta e="T1081" id="Seg_7085" s="T1080">bɨhɨlaːk</ta>
            <ta e="T1082" id="Seg_7086" s="T1081">öjd-üː-bün</ta>
            <ta e="T1083" id="Seg_7087" s="T1082">kanna</ta>
            <ta e="T1084" id="Seg_7088" s="T1083">ere</ta>
            <ta e="T1086" id="Seg_7089" s="T1085">o</ta>
            <ta e="T1087" id="Seg_7090" s="T1086">de</ta>
            <ta e="T1088" id="Seg_7091" s="T1087">minigi-n</ta>
            <ta e="T1089" id="Seg_7092" s="T1088">taŋɨn-nar-an</ta>
            <ta e="T1090" id="Seg_7093" s="T1089">čičigere-ti-ler</ta>
            <ta e="T1092" id="Seg_7094" s="T1091">hɨrga-ga</ta>
            <ta e="T1093" id="Seg_7095" s="T1092">baːj-an</ta>
            <ta e="T1094" id="Seg_7096" s="T1093">keːs-pit-ter</ta>
            <ta e="T1095" id="Seg_7097" s="T1094">önül-en</ta>
            <ta e="T1096" id="Seg_7098" s="T1095">keːs-pit-ter</ta>
            <ta e="T1098" id="Seg_7099" s="T1097">de</ta>
            <ta e="T1099" id="Seg_7100" s="T1098">kajdi͡ek</ta>
            <ta e="T1100" id="Seg_7101" s="T1099">bar-a-bɨt</ta>
            <ta e="T1102" id="Seg_7102" s="T1101">i</ta>
            <ta e="T1103" id="Seg_7103" s="T1102">kenni-bit-ten</ta>
            <ta e="T1104" id="Seg_7104" s="T1103">du͡o</ta>
            <ta e="T1105" id="Seg_7105" s="T1104">taba-lar-ɨ</ta>
            <ta e="T1106" id="Seg_7106" s="T1105">hi͡et-eːčči-ler</ta>
            <ta e="T1107" id="Seg_7107" s="T1106">kosobu͡oj-ga</ta>
            <ta e="T1109" id="Seg_7108" s="T1108">as-taːk</ta>
            <ta e="T1110" id="Seg_7109" s="T1109">ili</ta>
            <ta e="T1111" id="Seg_7110" s="T1110">mas-taːk</ta>
            <ta e="T1112" id="Seg_7111" s="T1111">kosobu͡oj</ta>
            <ta e="T1113" id="Seg_7112" s="T1112">hi͡et-eːčči</ta>
            <ta e="T1114" id="Seg_7113" s="T1113">e-ti-bit</ta>
            <ta e="T1116" id="Seg_7114" s="T1115">o-lor-ton</ta>
            <ta e="T1117" id="Seg_7115" s="T1116">kuttan-an</ta>
            <ta e="T1118" id="Seg_7116" s="T1117">ɨtaː-n</ta>
            <ta e="T1119" id="Seg_7117" s="T1118">mörüleːkt-iː-bin</ta>
            <ta e="T1120" id="Seg_7118" s="T1119">di͡e-n</ta>
            <ta e="T1122" id="Seg_7119" s="T1121">kuttan-ɨ-ma</ta>
            <ta e="T1123" id="Seg_7120" s="T1122">ɨtɨr-ɨ͡a</ta>
            <ta e="T1124" id="Seg_7121" s="T1123">hu͡og-a</ta>
            <ta e="T1126" id="Seg_7122" s="T1125">bu</ta>
            <ta e="T1127" id="Seg_7123" s="T1126">ke</ta>
            <ta e="T1128" id="Seg_7124" s="T1127">hɨlaj-bɨt-tar</ta>
            <ta e="T1129" id="Seg_7125" s="T1128">ke</ta>
            <ta e="T1130" id="Seg_7126" s="T1129">tɨːn-al-lar</ta>
            <ta e="T1133" id="Seg_7127" s="T1132">hɨraj-ba-r</ta>
            <ta e="T1134" id="Seg_7128" s="T1133">munna</ta>
            <ta e="T1136" id="Seg_7129" s="T1135">de</ta>
            <ta e="T1137" id="Seg_7130" s="T1136">ol</ta>
            <ta e="T1138" id="Seg_7131" s="T1137">iti</ta>
            <ta e="T1139" id="Seg_7132" s="T1138">korduk</ta>
            <ta e="T1140" id="Seg_7133" s="T1139">köh-ö</ta>
            <ta e="T1141" id="Seg_7134" s="T1140">hɨldʼ-aːččɨ</ta>
            <ta e="T1142" id="Seg_7135" s="T1141">e-ti-bit</ta>
            <ta e="T1143" id="Seg_7136" s="T1142">köh-ö</ta>
            <ta e="T1144" id="Seg_7137" s="T1143">hɨldʼ-aːččɨ</ta>
            <ta e="T1145" id="Seg_7138" s="T1144">köh-ü-s-e</ta>
            <ta e="T1146" id="Seg_7139" s="T1145">hɨldʼ-aːččɨ</ta>
            <ta e="T1147" id="Seg_7140" s="T1146">e-ti-m</ta>
            <ta e="T1148" id="Seg_7141" s="T1147">giniler-i</ta>
            <ta e="T1149" id="Seg_7142" s="T1148">gɨtta</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_7143" s="T1">biːrde</ta>
            <ta e="T3" id="Seg_7144" s="T2">öjdöː-A-BIn</ta>
            <ta e="T4" id="Seg_7145" s="T3">küččügüj</ta>
            <ta e="T5" id="Seg_7146" s="T4">er-TAK-BInA</ta>
            <ta e="T6" id="Seg_7147" s="T5">Nahoŋ-GA</ta>
            <ta e="T7" id="Seg_7148" s="T6">točka-BIt</ta>
            <ta e="T8" id="Seg_7149" s="T7">Kires-ttAn</ta>
            <ta e="T9" id="Seg_7150" s="T8">u͡on</ta>
            <ta e="T10" id="Seg_7151" s="T9">ikki</ta>
            <ta e="T11" id="Seg_7152" s="T10">kilometr</ta>
            <ta e="T12" id="Seg_7153" s="T11">onno</ta>
            <ta e="T13" id="Seg_7154" s="T12">töhö-LAːK</ta>
            <ta e="T14" id="Seg_7155" s="T13">kihi</ta>
            <ta e="T15" id="Seg_7156" s="T14">olor-BIT-tA=Ij</ta>
            <ta e="T17" id="Seg_7157" s="T16">kɨrdʼagas-LAr</ta>
            <ta e="T18" id="Seg_7158" s="T17">barɨ-LArA</ta>
            <ta e="T19" id="Seg_7159" s="T18">onno</ta>
            <ta e="T20" id="Seg_7160" s="T19">olor-AːččI</ta>
            <ta e="T21" id="Seg_7161" s="T20">e-TI-LArA</ta>
            <ta e="T23" id="Seg_7162" s="T22">hɨrga</ta>
            <ta e="T24" id="Seg_7163" s="T23">dʼi͡e</ta>
            <ta e="T25" id="Seg_7164" s="T24">toloru</ta>
            <ta e="T26" id="Seg_7165" s="T25">e-TI-tA</ta>
            <ta e="T27" id="Seg_7166" s="T26">onno</ta>
            <ta e="T29" id="Seg_7167" s="T28">taba-LAr</ta>
            <ta e="T30" id="Seg_7168" s="T29">tu͡ok-LAr</ta>
            <ta e="T31" id="Seg_7169" s="T30">baːr</ta>
            <ta e="T32" id="Seg_7170" s="T31">e-TI-LArA</ta>
            <ta e="T34" id="Seg_7171" s="T33">ol-ttAn</ta>
            <ta e="T35" id="Seg_7172" s="T34">biːrde</ta>
            <ta e="T36" id="Seg_7173" s="T35">teːte-m-LAːK</ta>
            <ta e="T37" id="Seg_7174" s="T36">dʼi͡e-LArI-GAr</ta>
            <ta e="T38" id="Seg_7175" s="T37">teːte-m</ta>
            <ta e="T39" id="Seg_7176" s="T38">kɨrdʼagas</ta>
            <ta e="T40" id="Seg_7177" s="T39">ehe-BI-n</ta>
            <ta e="T41" id="Seg_7178" s="T40">di͡e-AːččI-BIn</ta>
            <ta e="T42" id="Seg_7179" s="T41">bu͡o</ta>
            <ta e="T43" id="Seg_7180" s="T42">teːte</ta>
            <ta e="T44" id="Seg_7181" s="T43">di͡e-AːččI-BIn</ta>
            <ta e="T45" id="Seg_7182" s="T44">kɨrdʼagas</ta>
            <ta e="T46" id="Seg_7183" s="T45">maːma-BI-n</ta>
            <ta e="T47" id="Seg_7184" s="T46">maːma</ta>
            <ta e="T48" id="Seg_7185" s="T47">di͡e-AːččI-BIn</ta>
            <ta e="T49" id="Seg_7186" s="T48">kɨrdʼagas</ta>
            <ta e="T50" id="Seg_7187" s="T49">ebe-BI-n</ta>
            <ta e="T51" id="Seg_7188" s="T50">giniler-GA</ta>
            <ta e="T52" id="Seg_7189" s="T51">iːt-I-LIN-I-BIT-I-m</ta>
            <ta e="T54" id="Seg_7190" s="T53">dʼe</ta>
            <ta e="T55" id="Seg_7191" s="T54">ol</ta>
            <ta e="T56" id="Seg_7192" s="T55">teːte-BIt</ta>
            <ta e="T57" id="Seg_7193" s="T56">kajdi͡ek</ta>
            <ta e="T58" id="Seg_7194" s="T57">ere</ta>
            <ta e="T59" id="Seg_7195" s="T58">bar-An</ta>
            <ta e="T60" id="Seg_7196" s="T59">kaːl-TI-tA</ta>
            <ta e="T62" id="Seg_7197" s="T61">mas-LAN-A</ta>
            <ta e="T63" id="Seg_7198" s="T62">bar-TI-tA</ta>
            <ta e="T64" id="Seg_7199" s="T63">eni</ta>
            <ta e="T65" id="Seg_7200" s="T64">dʼe</ta>
            <ta e="T66" id="Seg_7201" s="T65">olor-A-BIt</ta>
            <ta e="T67" id="Seg_7202" s="T66">taba-LAr-BIt</ta>
            <ta e="T69" id="Seg_7203" s="T68">kim-GA</ta>
            <ta e="T70" id="Seg_7204" s="T69">dʼi͡e-BIt</ta>
            <ta e="T71" id="Seg_7205" s="T70">tas-tI-GAr</ta>
            <ta e="T72" id="Seg_7206" s="T71">hɨrɨt-Ar-LAr</ta>
            <ta e="T74" id="Seg_7207" s="T73">tabɨj-A</ta>
            <ta e="T75" id="Seg_7208" s="T74">hɨrɨt-Ar-LAr</ta>
            <ta e="T78" id="Seg_7209" s="T77">aːku-LAːK</ta>
            <ta e="T79" id="Seg_7210" s="T78">e-TI-BIt</ta>
            <ta e="T81" id="Seg_7211" s="T80">bu</ta>
            <ta e="T82" id="Seg_7212" s="T81">iliː-ttAn</ta>
            <ta e="T83" id="Seg_7213" s="T82">ahaː-Ar</ta>
            <ta e="T85" id="Seg_7214" s="T84">keli͡ep-I-msAk</ta>
            <ta e="T86" id="Seg_7215" s="T85">bagajɨ</ta>
            <ta e="T88" id="Seg_7216" s="T87">tünnük-GA</ta>
            <ta e="T89" id="Seg_7217" s="T88">kel-AːččI</ta>
            <ta e="T90" id="Seg_7218" s="T89">keli͡ep</ta>
            <ta e="T91" id="Seg_7219" s="T90">kördön-A</ta>
            <ta e="T93" id="Seg_7220" s="T92">o</ta>
            <ta e="T94" id="Seg_7221" s="T93">munnu-tA</ta>
            <ta e="T95" id="Seg_7222" s="T94">nʼaltaj-An</ta>
            <ta e="T96" id="Seg_7223" s="T95">kel-IAK.[tA]</ta>
            <ta e="T97" id="Seg_7224" s="T96">di͡e-An</ta>
            <ta e="T98" id="Seg_7225" s="T97">o</ta>
            <ta e="T99" id="Seg_7226" s="T98">hohuj-A-BIn</ta>
            <ta e="T100" id="Seg_7227" s="T99">küččügüj</ta>
            <ta e="T101" id="Seg_7228" s="T100">kihi</ta>
            <ta e="T102" id="Seg_7229" s="T101">ka</ta>
            <ta e="T103" id="Seg_7230" s="T102">kuttan-AːččI</ta>
            <ta e="T104" id="Seg_7231" s="T103">e-TI-m</ta>
            <ta e="T105" id="Seg_7232" s="T104">bu͡o</ta>
            <ta e="T108" id="Seg_7233" s="T107">bu</ta>
            <ta e="T109" id="Seg_7234" s="T108">kihi</ta>
            <ta e="T110" id="Seg_7235" s="T109">kisteː-An</ta>
            <ta e="T111" id="Seg_7236" s="T110">arɨj-TI-m</ta>
            <ta e="T112" id="Seg_7237" s="T111">bu͡o</ta>
            <ta e="T114" id="Seg_7238" s="T113">ostu͡ol-ttAn</ta>
            <ta e="T115" id="Seg_7239" s="T114">bu</ta>
            <ta e="T116" id="Seg_7240" s="T115">keli͡ep</ta>
            <ta e="T117" id="Seg_7241" s="T116">lepi͡eske</ta>
            <ta e="T118" id="Seg_7242" s="T117">bagajɨ-LAr</ta>
            <ta e="T120" id="Seg_7243" s="T119">o</ta>
            <ta e="T121" id="Seg_7244" s="T120">ol-tI-m</ta>
            <ta e="T122" id="Seg_7245" s="T121">tülej-BIT</ta>
            <ta e="T123" id="Seg_7246" s="T122">hi͡e-A</ta>
            <ta e="T124" id="Seg_7247" s="T123">hi͡e-AːktAː-Ar</ta>
            <ta e="T125" id="Seg_7248" s="T124">di͡e-An</ta>
            <ta e="T126" id="Seg_7249" s="T125">araː</ta>
            <ta e="T128" id="Seg_7250" s="T127">dʼe</ta>
            <ta e="T129" id="Seg_7251" s="T128">ol</ta>
            <ta e="T130" id="Seg_7252" s="T129">maːma-m</ta>
            <ta e="T131" id="Seg_7253" s="T130">di͡e-r</ta>
            <ta e="T132" id="Seg_7254" s="T131">kɨrdʼagas</ta>
            <ta e="T133" id="Seg_7255" s="T132">maːma-m</ta>
            <ta e="T134" id="Seg_7256" s="T133">ka</ta>
            <ta e="T136" id="Seg_7257" s="T135">o</ta>
            <ta e="T137" id="Seg_7258" s="T136">ü͡öret-I-m</ta>
            <ta e="T138" id="Seg_7259" s="T137">dʼe</ta>
            <ta e="T139" id="Seg_7260" s="T138">en</ta>
            <ta e="T140" id="Seg_7261" s="T139">tünnük-BItI-n</ta>
            <ta e="T141" id="Seg_7262" s="T140">aldʼat-IAK.[tA]</ta>
            <ta e="T142" id="Seg_7263" s="T141">kanʼaː-IAK.[tA]</ta>
            <ta e="T144" id="Seg_7264" s="T143">bar</ta>
            <ta e="T145" id="Seg_7265" s="T144">bar</ta>
            <ta e="T146" id="Seg_7266" s="T145">bar</ta>
            <ta e="T147" id="Seg_7267" s="T146">kel-I-m</ta>
            <ta e="T149" id="Seg_7268" s="T148">di͡e-Ar</ta>
            <ta e="T150" id="Seg_7269" s="T149">tu͡ok</ta>
            <ta e="T151" id="Seg_7270" s="T150">da</ta>
            <ta e="T152" id="Seg_7271" s="T151">hu͡ok</ta>
            <ta e="T154" id="Seg_7272" s="T153">ot-GI-n</ta>
            <ta e="T155" id="Seg_7273" s="T154">hi͡e</ta>
            <ta e="T156" id="Seg_7274" s="T155">bar</ta>
            <ta e="T157" id="Seg_7275" s="T156">di͡e-Ar</ta>
            <ta e="T158" id="Seg_7276" s="T157">eː</ta>
            <ta e="T159" id="Seg_7277" s="T158">hap-An</ta>
            <ta e="T160" id="Seg_7278" s="T159">keːs-TI-tA</ta>
            <ta e="T162" id="Seg_7279" s="T161">ol-ttAn</ta>
            <ta e="T163" id="Seg_7280" s="T162">biːrges</ta>
            <ta e="T164" id="Seg_7281" s="T163">tünnük-I-nAn</ta>
            <ta e="T165" id="Seg_7282" s="T164">kel-Ar</ta>
            <ta e="T166" id="Seg_7283" s="T165">onno</ta>
            <ta e="T168" id="Seg_7284" s="T167">bu͡ol-AːččI</ta>
            <ta e="T169" id="Seg_7285" s="T168">bu͡o</ta>
            <ta e="T171" id="Seg_7286" s="T170">onno</ta>
            <ta e="T172" id="Seg_7287" s="T171">buːs</ta>
            <ta e="T173" id="Seg_7288" s="T172">tu͡ok</ta>
            <ta e="T174" id="Seg_7289" s="T173">uːr-I-n-AːččI-BIt</ta>
            <ta e="T176" id="Seg_7290" s="T175">bu</ta>
            <ta e="T177" id="Seg_7291" s="T176">ka</ta>
            <ta e="T178" id="Seg_7292" s="T177">uː-BIt</ta>
            <ta e="T179" id="Seg_7293" s="T178">bu͡o</ta>
            <ta e="T180" id="Seg_7294" s="T179">buːs-BIt</ta>
            <ta e="T182" id="Seg_7295" s="T181">onno</ta>
            <ta e="T183" id="Seg_7296" s="T182">ɨtɨn-A</ta>
            <ta e="T184" id="Seg_7297" s="T183">ɨtɨn-A</ta>
            <ta e="T185" id="Seg_7298" s="T184">tünnük-BIt</ta>
            <ta e="T186" id="Seg_7299" s="T185">arɨj-LIN-An</ta>
            <ta e="T187" id="Seg_7300" s="T186">tur-Ar</ta>
            <ta e="T188" id="Seg_7301" s="T187">bu͡o</ta>
            <ta e="T189" id="Seg_7302" s="T188">inniki-BIt</ta>
            <ta e="T190" id="Seg_7303" s="T189">ka</ta>
            <ta e="T192" id="Seg_7304" s="T191">onno</ta>
            <ta e="T193" id="Seg_7305" s="T192">moltoj-A</ta>
            <ta e="T194" id="Seg_7306" s="T193">moltoj-A</ta>
            <ta e="T195" id="Seg_7307" s="T194">mu͡os-LAr-LAːK</ta>
            <ta e="T196" id="Seg_7308" s="T195">bu͡o</ta>
            <ta e="T198" id="Seg_7309" s="T197">mu͡os-LAr-tA</ta>
            <ta e="T199" id="Seg_7310" s="T198">bat-BAT-LAr</ta>
            <ta e="T200" id="Seg_7311" s="T199">kim</ta>
            <ta e="T201" id="Seg_7312" s="T200">ču͡oŋaːl-GA</ta>
            <ta e="T202" id="Seg_7313" s="T201">lepi͡eske</ta>
            <ta e="T203" id="Seg_7314" s="T202">tur-Ar</ta>
            <ta e="T204" id="Seg_7315" s="T203">e-TI-tA</ta>
            <ta e="T207" id="Seg_7316" s="T206">kim-LAː-BIT</ta>
            <ta e="T208" id="Seg_7317" s="T207">bɨs-I-LIN-I-BIT-LAr</ta>
            <ta e="T210" id="Seg_7318" s="T209">dʼe</ta>
            <ta e="T211" id="Seg_7319" s="T210">ol-nI</ta>
            <ta e="T212" id="Seg_7320" s="T211">tij-A</ta>
            <ta e="T213" id="Seg_7321" s="T212">hataː-TI-tA</ta>
            <ta e="T214" id="Seg_7322" s="T213">bu͡o</ta>
            <ta e="T215" id="Seg_7323" s="T214">tɨl-tI-n</ta>
            <ta e="T216" id="Seg_7324" s="T215">uːn-A</ta>
            <ta e="T217" id="Seg_7325" s="T216">uːn-A</ta>
            <ta e="T219" id="Seg_7326" s="T218">maːma</ta>
            <ta e="T220" id="Seg_7327" s="T219">kör</ta>
            <ta e="T221" id="Seg_7328" s="T220">kör</ta>
            <ta e="T222" id="Seg_7329" s="T221">kör</ta>
            <ta e="T223" id="Seg_7330" s="T222">di͡e-A-BIn</ta>
            <ta e="T224" id="Seg_7331" s="T223">kim-GI-n</ta>
            <ta e="T225" id="Seg_7332" s="T224">hi͡e-AːrI</ta>
            <ta e="T226" id="Seg_7333" s="T225">gɨn-TI-tA</ta>
            <ta e="T227" id="Seg_7334" s="T226">keli͡ep-LAr-GI-n</ta>
            <ta e="T228" id="Seg_7335" s="T227">hi͡e-AːrI</ta>
            <ta e="T229" id="Seg_7336" s="T228">gɨn-TI-tA</ta>
            <ta e="T230" id="Seg_7337" s="T229">di͡e-A-BIn</ta>
            <ta e="T231" id="Seg_7338" s="T230">keli͡ep-LAr-GI-n</ta>
            <ta e="T233" id="Seg_7339" s="T232">oj</ta>
            <ta e="T234" id="Seg_7340" s="T233">togo</ta>
            <ta e="T235" id="Seg_7341" s="T234">satana</ta>
            <ta e="T236" id="Seg_7342" s="T235">kɨːs-tA</ta>
            <ta e="T237" id="Seg_7343" s="T236">togo</ta>
            <ta e="T238" id="Seg_7344" s="T237">ü͡öret-BIT-GIn=Ij</ta>
            <ta e="T239" id="Seg_7345" s="T238">bu͡o</ta>
            <ta e="T240" id="Seg_7346" s="T239">dʼi͡e-ttAn</ta>
            <ta e="T241" id="Seg_7347" s="T240">ahaː-t-A</ta>
            <ta e="T242" id="Seg_7348" s="T241">tünnük-ttAn</ta>
            <ta e="T243" id="Seg_7349" s="T242">ahaː-t-A</ta>
            <ta e="T245" id="Seg_7350" s="T244">kör</ta>
            <ta e="T246" id="Seg_7351" s="T245">anɨ</ta>
            <ta e="T247" id="Seg_7352" s="T246">barɨ-tI-n</ta>
            <ta e="T248" id="Seg_7353" s="T247">u͡or-IAK-tA</ta>
            <ta e="T249" id="Seg_7354" s="T248">di͡e-Ar</ta>
            <ta e="T251" id="Seg_7355" s="T250">araː</ta>
            <ta e="T252" id="Seg_7356" s="T251">bɨ͡ar</ta>
            <ta e="T253" id="Seg_7357" s="T252">agaj-BIn</ta>
            <ta e="T256" id="Seg_7358" s="T255">bar</ta>
            <ta e="T257" id="Seg_7359" s="T256">bar</ta>
            <ta e="T259" id="Seg_7360" s="T258">tu͡ok</ta>
            <ta e="T260" id="Seg_7361" s="T259">ere</ta>
            <ta e="T261" id="Seg_7362" s="T260">trepke-nAn</ta>
            <ta e="T265" id="Seg_7363" s="T264">ol-ttAn</ta>
            <ta e="T266" id="Seg_7364" s="T265">bar-BAT</ta>
            <ta e="T268" id="Seg_7365" s="T267">dʼe</ta>
            <ta e="T271" id="Seg_7366" s="T270">bar</ta>
            <ta e="T273" id="Seg_7367" s="T272">bi͡er-An</ta>
            <ta e="T274" id="Seg_7368" s="T273">keːs-Ar</ta>
            <ta e="T275" id="Seg_7369" s="T274">bu͡o</ta>
            <ta e="T277" id="Seg_7370" s="T276">ol-tI-ŋ</ta>
            <ta e="T278" id="Seg_7371" s="T277">momuj-A</ta>
            <ta e="T279" id="Seg_7372" s="T278">momuj-A</ta>
            <ta e="T281" id="Seg_7373" s="T280">bar-AːktAː-Ar</ta>
            <ta e="T282" id="Seg_7374" s="T281">di͡e-An</ta>
            <ta e="T283" id="Seg_7375" s="T282">hi͡ese</ta>
            <ta e="T284" id="Seg_7376" s="T283">keli͡ep-nI</ta>
            <ta e="T285" id="Seg_7377" s="T284">hi͡ese</ta>
            <ta e="T286" id="Seg_7378" s="T285">tot-TI-tA</ta>
            <ta e="T287" id="Seg_7379" s="T286">bɨhɨːlaːk</ta>
            <ta e="T288" id="Seg_7380" s="T287">tu͡ok</ta>
            <ta e="T289" id="Seg_7381" s="T288">töhölöːk</ta>
            <ta e="T290" id="Seg_7382" s="T289">tot-IAK.[tA]=Ij</ta>
            <ta e="T291" id="Seg_7383" s="T290">dʼe</ta>
            <ta e="T292" id="Seg_7384" s="T291">ol</ta>
            <ta e="T293" id="Seg_7385" s="T292">kačɨgɨraː-I-t-Ar</ta>
            <ta e="T296" id="Seg_7386" s="T295">kim-nI</ta>
            <ta e="T297" id="Seg_7387" s="T296">bi͡er-BIT</ta>
            <ta e="T298" id="Seg_7388" s="T297">kat-BIT</ta>
            <ta e="T299" id="Seg_7389" s="T298">keli͡ep-nI</ta>
            <ta e="T301" id="Seg_7390" s="T300">dʼe</ta>
            <ta e="T302" id="Seg_7391" s="T301">onno</ta>
            <ta e="T303" id="Seg_7392" s="T302">ɨstaː-A</ta>
            <ta e="T304" id="Seg_7393" s="T303">hataː-TI-tA</ta>
            <ta e="T305" id="Seg_7394" s="T304">bu͡o</ta>
            <ta e="T307" id="Seg_7395" s="T306">araː</ta>
            <ta e="T310" id="Seg_7396" s="T309">oj</ta>
            <ta e="T311" id="Seg_7397" s="T310">tiːs-LAr-tI-n</ta>
            <ta e="T312" id="Seg_7398" s="T311">barɨ-tI-n</ta>
            <ta e="T313" id="Seg_7399" s="T312">kim-LAː-TI-tA</ta>
            <ta e="T314" id="Seg_7400" s="T313">bɨhɨːlaːk</ta>
            <ta e="T316" id="Seg_7401" s="T315">dʼe</ta>
            <ta e="T317" id="Seg_7402" s="T316">ol</ta>
            <ta e="T318" id="Seg_7403" s="T317">bar-BIT-tA</ta>
            <ta e="T319" id="Seg_7404" s="T318">hi͡ese</ta>
            <ta e="T320" id="Seg_7405" s="T319">dogor-LAr-tI-n</ta>
            <ta e="T321" id="Seg_7406" s="T320">dek</ta>
            <ta e="T323" id="Seg_7407" s="T322">hɨːr</ta>
            <ta e="T324" id="Seg_7408" s="T323">alɨn-tI-GAr</ta>
            <ta e="T325" id="Seg_7409" s="T324">tüs-BIT-LArA</ta>
            <ta e="T326" id="Seg_7410" s="T325">onno</ta>
            <ta e="T327" id="Seg_7411" s="T326">ebe-GA</ta>
            <ta e="T328" id="Seg_7412" s="T327">hɨrɨt-Ar-LAr</ta>
            <ta e="T330" id="Seg_7413" s="T329">tahaːra-BIt</ta>
            <ta e="T331" id="Seg_7414" s="T330">boru͡or-TI-tA</ta>
            <ta e="T332" id="Seg_7415" s="T331">karaŋar-Ar</ta>
            <ta e="T333" id="Seg_7416" s="T332">karaŋar-An</ta>
            <ta e="T334" id="Seg_7417" s="T333">is-Ar</ta>
            <ta e="T336" id="Seg_7418" s="T335">kim</ta>
            <ta e="T337" id="Seg_7419" s="T336">ɨt-LAːK</ta>
            <ta e="T338" id="Seg_7420" s="T337">e-TI-BIt</ta>
            <ta e="T339" id="Seg_7421" s="T338">bihigi</ta>
            <ta e="T340" id="Seg_7422" s="T339">Djoŋgo</ta>
            <ta e="T341" id="Seg_7423" s="T340">čeːlkeː</ta>
            <ta e="T342" id="Seg_7424" s="T341">bagajɨ</ta>
            <ta e="T343" id="Seg_7425" s="T342">bosku͡oj</ta>
            <ta e="T344" id="Seg_7426" s="T343">bagajɨ</ta>
            <ta e="T345" id="Seg_7427" s="T344">taba-ČIt</ta>
            <ta e="T346" id="Seg_7428" s="T345">bagajɨ</ta>
            <ta e="T347" id="Seg_7429" s="T346">ol-tI-ŋ</ta>
            <ta e="T349" id="Seg_7430" s="T348">kim</ta>
            <ta e="T351" id="Seg_7431" s="T350">di͡e-IAK-ŋ</ta>
            <ta e="T352" id="Seg_7432" s="T351">du͡o</ta>
            <ta e="T353" id="Seg_7433" s="T352">kim-nI</ta>
            <ta e="T354" id="Seg_7434" s="T353">ü͡ör-nI</ta>
            <ta e="T355" id="Seg_7435" s="T354">barɨ-tI-n</ta>
            <ta e="T356" id="Seg_7436" s="T355">egel-An</ta>
            <ta e="T357" id="Seg_7437" s="T356">kel-IAK.[tA]</ta>
            <ta e="T359" id="Seg_7438" s="T358">o</ta>
            <ta e="T360" id="Seg_7439" s="T359">erij-AːktAː-IAK-tA</ta>
            <ta e="T361" id="Seg_7440" s="T360">di͡e-An</ta>
            <ta e="T362" id="Seg_7441" s="T361">tögürüččü</ta>
            <ta e="T363" id="Seg_7442" s="T362">biːr</ta>
            <ta e="T364" id="Seg_7443" s="T363">hir-GA</ta>
            <ta e="T365" id="Seg_7444" s="T364">tur-IAK-tA</ta>
            <ta e="T367" id="Seg_7445" s="T366">bu</ta>
            <ta e="T368" id="Seg_7446" s="T367">egel</ta>
            <ta e="T369" id="Seg_7447" s="T368">di͡e-TAK-GA</ta>
            <ta e="T370" id="Seg_7448" s="T369">ka</ta>
            <ta e="T372" id="Seg_7449" s="T371">dʼe</ta>
            <ta e="T373" id="Seg_7450" s="T372">ol-tI-BIt</ta>
            <ta e="T374" id="Seg_7451" s="T373">kim-nI</ta>
            <ta e="T375" id="Seg_7452" s="T374">kim-LAː-AːččI</ta>
            <ta e="T376" id="Seg_7453" s="T375">e-TI-tA</ta>
            <ta e="T377" id="Seg_7454" s="T376">maːjdiːn</ta>
            <ta e="T378" id="Seg_7455" s="T377">taba-LAr-I-ŋ</ta>
            <ta e="T379" id="Seg_7456" s="T378">baːr</ta>
            <ta e="T380" id="Seg_7457" s="T379">er-TAK-TArInA</ta>
            <ta e="T381" id="Seg_7458" s="T380">ka</ta>
            <ta e="T382" id="Seg_7459" s="T381">dʼi͡e-GA</ta>
            <ta e="T384" id="Seg_7460" s="T383">tugut</ta>
            <ta e="T385" id="Seg_7461" s="T384">abɨlakaːn</ta>
            <ta e="T386" id="Seg_7462" s="T385">du͡o</ta>
            <ta e="T387" id="Seg_7463" s="T386">tu͡ok</ta>
            <ta e="T388" id="Seg_7464" s="T387">du͡o</ta>
            <ta e="T389" id="Seg_7465" s="T388">abɨlakaːn</ta>
            <ta e="T390" id="Seg_7466" s="T389">tugut</ta>
            <ta e="T391" id="Seg_7467" s="T390">eː</ta>
            <ta e="T392" id="Seg_7468" s="T391">ulakan</ta>
            <ta e="T393" id="Seg_7469" s="T392">tugut-ttAn</ta>
            <ta e="T394" id="Seg_7470" s="T393">ulakan-kAːN</ta>
            <ta e="T395" id="Seg_7471" s="T394">dʼɨl-tI-nAn</ta>
            <ta e="T396" id="Seg_7472" s="T395">ere</ta>
            <ta e="T397" id="Seg_7473" s="T396">ulakan</ta>
            <ta e="T399" id="Seg_7474" s="T398">ol</ta>
            <ta e="T400" id="Seg_7475" s="T399">kim-nI</ta>
            <ta e="T401" id="Seg_7476" s="T400">dʼe</ta>
            <ta e="T402" id="Seg_7477" s="T401">erij-A</ta>
            <ta e="T403" id="Seg_7478" s="T402">hɨt-IAK-tA</ta>
            <ta e="T404" id="Seg_7479" s="T403">bu͡o</ta>
            <ta e="T405" id="Seg_7480" s="T404">muŋnaː-A</ta>
            <ta e="T406" id="Seg_7481" s="T405">hɨt-AːččI</ta>
            <ta e="T407" id="Seg_7482" s="T406">iti</ta>
            <ta e="T408" id="Seg_7483" s="T407">tu͡ok</ta>
            <ta e="T409" id="Seg_7484" s="T408">tugut-kAːN</ta>
            <ta e="T410" id="Seg_7485" s="T409">bagar-AːččI</ta>
            <ta e="T411" id="Seg_7486" s="T410">dʼürü</ta>
            <ta e="T412" id="Seg_7487" s="T411">möŋ-A</ta>
            <ta e="T413" id="Seg_7488" s="T412">möŋ-A</ta>
            <ta e="T415" id="Seg_7489" s="T414">hɨt-IAK-tA</ta>
            <ta e="T417" id="Seg_7490" s="T416">bu͡o</ta>
            <ta e="T418" id="Seg_7491" s="T417">oːnnʼoː-Ar</ta>
            <ta e="T419" id="Seg_7492" s="T418">bu͡o</ta>
            <ta e="T420" id="Seg_7493" s="T419">gini-nI</ta>
            <ta e="T421" id="Seg_7494" s="T420">kɨtta</ta>
            <ta e="T422" id="Seg_7495" s="T421">ka</ta>
            <ta e="T423" id="Seg_7496" s="T422">ol-tI-ŋ</ta>
            <ta e="T424" id="Seg_7497" s="T423">kajdak</ta>
            <ta e="T425" id="Seg_7498" s="T424">da</ta>
            <ta e="T426" id="Seg_7499" s="T425">bu͡ol-TI-tA</ta>
            <ta e="T427" id="Seg_7500" s="T1150">tap-I-s-A-s</ta>
            <ta e="T428" id="Seg_7501" s="T427">tap-I-s-Ar</ta>
            <ta e="T429" id="Seg_7502" s="T428">bu</ta>
            <ta e="T430" id="Seg_7503" s="T429">kurduk</ta>
            <ta e="T431" id="Seg_7504" s="T430">kajdak</ta>
            <ta e="T432" id="Seg_7505" s="T431">gɨn=Ij</ta>
            <ta e="T433" id="Seg_7506" s="T432">maːma</ta>
            <ta e="T434" id="Seg_7507" s="T433">maːma</ta>
            <ta e="T435" id="Seg_7508" s="T434">di͡e-A-BIn</ta>
            <ta e="T436" id="Seg_7509" s="T435">kör</ta>
            <ta e="T437" id="Seg_7510" s="T436">kör</ta>
            <ta e="T438" id="Seg_7511" s="T437">Djoŋgo-nI</ta>
            <ta e="T440" id="Seg_7512" s="T439">Čoko</ta>
            <ta e="T441" id="Seg_7513" s="T440">Čoko</ta>
            <ta e="T442" id="Seg_7514" s="T441">di͡e-AːččI-BIn</ta>
            <ta e="T444" id="Seg_7515" s="T443">Čoko</ta>
            <ta e="T445" id="Seg_7516" s="T444">kör</ta>
            <ta e="T447" id="Seg_7517" s="T446">kim-nI</ta>
            <ta e="T448" id="Seg_7518" s="T447">hi͡e-AːrI</ta>
            <ta e="T449" id="Seg_7519" s="T448">gɨn-TI-tA</ta>
            <ta e="T450" id="Seg_7520" s="T449">di͡e-A-BIn</ta>
            <ta e="T451" id="Seg_7521" s="T450">tugut-nI</ta>
            <ta e="T452" id="Seg_7522" s="T451">hi͡e-AːrI</ta>
            <ta e="T453" id="Seg_7523" s="T452">gɨn-TI-tA</ta>
            <ta e="T455" id="Seg_7524" s="T454">o</ta>
            <ta e="T456" id="Seg_7525" s="T455">satana</ta>
            <ta e="T457" id="Seg_7526" s="T456">ɨgɨr</ta>
            <ta e="T458" id="Seg_7527" s="T457">ɨgɨr</ta>
            <ta e="T460" id="Seg_7528" s="T459">hɨlaj-t-TI-tA</ta>
            <ta e="T461" id="Seg_7529" s="T460">bɨhɨːlaːk</ta>
            <ta e="T462" id="Seg_7530" s="T461">iti</ta>
            <ta e="T463" id="Seg_7531" s="T462">tugut-nI</ta>
            <ta e="T464" id="Seg_7532" s="T463">kuttaː-TI-tA</ta>
            <ta e="T466" id="Seg_7533" s="T465">Čoko</ta>
            <ta e="T467" id="Seg_7534" s="T466">Čoko</ta>
            <ta e="T468" id="Seg_7535" s="T467">Čoko</ta>
            <ta e="T469" id="Seg_7536" s="T468">di͡e-A-BIn</ta>
            <ta e="T470" id="Seg_7537" s="T469">tahaːra</ta>
            <ta e="T471" id="Seg_7538" s="T470">tagɨs-An</ta>
            <ta e="T472" id="Seg_7539" s="T471">bar-An</ta>
            <ta e="T473" id="Seg_7540" s="T472">ka</ta>
            <ta e="T475" id="Seg_7541" s="T474">o</ta>
            <ta e="T476" id="Seg_7542" s="T475">dʼe</ta>
            <ta e="T477" id="Seg_7543" s="T476">ol-tI-ŋ</ta>
            <ta e="T478" id="Seg_7544" s="T477">kötüt-An</ta>
            <ta e="T479" id="Seg_7545" s="T478">is-AːktAː-Ar</ta>
            <ta e="T480" id="Seg_7546" s="T479">di͡e-An</ta>
            <ta e="T482" id="Seg_7547" s="T481">dʼe</ta>
            <ta e="T483" id="Seg_7548" s="T482">ɨstan</ta>
            <ta e="T484" id="Seg_7549" s="T483">dʼe</ta>
            <ta e="T485" id="Seg_7550" s="T484">hɨraj-BA-r</ta>
            <ta e="T486" id="Seg_7551" s="T485">min</ta>
            <ta e="T487" id="Seg_7552" s="T486">taraj-s</ta>
            <ta e="T490" id="Seg_7553" s="T489">ittenne</ta>
            <ta e="T491" id="Seg_7554" s="T490">taraj-A</ta>
            <ta e="T492" id="Seg_7555" s="T491">hɨt-AːktAː-A-BIn</ta>
            <ta e="T493" id="Seg_7556" s="T492">di͡e-An</ta>
            <ta e="T495" id="Seg_7557" s="T494">eː</ta>
            <ta e="T496" id="Seg_7558" s="T495">di͡e-A-BIn</ta>
            <ta e="T497" id="Seg_7559" s="T496">maːma</ta>
            <ta e="T498" id="Seg_7560" s="T497">ɨl</ta>
            <ta e="T499" id="Seg_7561" s="T498">dʼe</ta>
            <ta e="T500" id="Seg_7562" s="T499">bu͡o</ta>
            <ta e="T502" id="Seg_7563" s="T501">halaː-Ar</ta>
            <ta e="T503" id="Seg_7564" s="T502">kanʼaː-Ar</ta>
            <ta e="T504" id="Seg_7565" s="T503">möŋ</ta>
            <ta e="T505" id="Seg_7566" s="T504">möŋ-TAr-Ar</ta>
            <ta e="T506" id="Seg_7567" s="T505">bu͡o</ta>
            <ta e="T507" id="Seg_7568" s="T506">kihi-nI</ta>
            <ta e="T508" id="Seg_7569" s="T507">ka</ta>
            <ta e="T510" id="Seg_7570" s="T509">möŋ-IAK-tI-n</ta>
            <ta e="T511" id="Seg_7571" s="T510">bagar-Ar</ta>
            <ta e="T513" id="Seg_7572" s="T512">min-n</ta>
            <ta e="T514" id="Seg_7573" s="T513">hogostoː-A</ta>
            <ta e="T515" id="Seg_7574" s="T514">hɨt-AːktAː-Ar</ta>
            <ta e="T516" id="Seg_7575" s="T515">di͡e-An</ta>
            <ta e="T518" id="Seg_7576" s="T517">dʼe</ta>
            <ta e="T519" id="Seg_7577" s="T518">ol</ta>
            <ta e="T520" id="Seg_7578" s="T519">maːma-m</ta>
            <ta e="T521" id="Seg_7579" s="T520">tagɨs-TI-tA</ta>
            <ta e="T523" id="Seg_7580" s="T522">dʼe</ta>
            <ta e="T524" id="Seg_7581" s="T523">tu͡ok</ta>
            <ta e="T525" id="Seg_7582" s="T524">bu͡ol-A-GIn</ta>
            <ta e="T527" id="Seg_7583" s="T526">kör</ta>
            <ta e="T528" id="Seg_7584" s="T527">di͡e-A-BIn</ta>
            <ta e="T529" id="Seg_7585" s="T528">min-n</ta>
            <ta e="T530" id="Seg_7586" s="T529">kɨrbaː-Ar</ta>
            <ta e="T531" id="Seg_7587" s="T530">du͡o</ta>
            <ta e="T532" id="Seg_7588" s="T531">di͡e-A-BIn</ta>
            <ta e="T533" id="Seg_7589" s="T532">oːnnʼoː-Ar</ta>
            <ta e="T534" id="Seg_7590" s="T533">kihi-nI</ta>
            <ta e="T535" id="Seg_7591" s="T534">ka</ta>
            <ta e="T536" id="Seg_7592" s="T535">araː</ta>
            <ta e="T537" id="Seg_7593" s="T536">da</ta>
            <ta e="T539" id="Seg_7594" s="T538">čičigere-An</ta>
            <ta e="T540" id="Seg_7595" s="T539">ɨtaː-An</ta>
            <ta e="T541" id="Seg_7596" s="T540">mörüleːkteː-A-BIn</ta>
            <ta e="T542" id="Seg_7597" s="T541">eː</ta>
            <ta e="T544" id="Seg_7598" s="T543">ɨtaː</ta>
            <ta e="T545" id="Seg_7599" s="T544">ɨtaː-BA</ta>
            <ta e="T547" id="Seg_7600" s="T546">beje-ŋ</ta>
            <ta e="T548" id="Seg_7601" s="T547">ɨgɨr-BIT-I-ŋ</ta>
            <ta e="T549" id="Seg_7602" s="T548">di͡e-Ar</ta>
            <ta e="T550" id="Seg_7603" s="T549">kihi</ta>
            <ta e="T551" id="Seg_7604" s="T550">kül-IAK</ta>
            <ta e="T553" id="Seg_7605" s="T552">ol-tI-ŋ</ta>
            <ta e="T554" id="Seg_7606" s="T553">emi͡e</ta>
            <ta e="T555" id="Seg_7607" s="T554">bar-An</ta>
            <ta e="T556" id="Seg_7608" s="T555">kaːl-TI-tA</ta>
            <ta e="T557" id="Seg_7609" s="T556">ol</ta>
            <ta e="T558" id="Seg_7610" s="T557">tugut-GA</ta>
            <ta e="T560" id="Seg_7611" s="T559">dʼe</ta>
            <ta e="T562" id="Seg_7612" s="T561">kim-LAː-BAtAK-I-m</ta>
            <ta e="T563" id="Seg_7613" s="T562">ol</ta>
            <ta e="T564" id="Seg_7614" s="T563">bi͡ek</ta>
            <ta e="T565" id="Seg_7615" s="T564">kün</ta>
            <ta e="T566" id="Seg_7616" s="T565">aːjɨ</ta>
            <ta e="T567" id="Seg_7617" s="T566">oːnnʼoː-AːččI</ta>
            <ta e="T568" id="Seg_7618" s="T567">ol</ta>
            <ta e="T569" id="Seg_7619" s="T568">tugut-nI</ta>
            <ta e="T570" id="Seg_7620" s="T569">kɨtta</ta>
            <ta e="T571" id="Seg_7621" s="T570">tu͡ok-GA</ta>
            <ta e="T572" id="Seg_7622" s="T571">bagar-BIT</ta>
            <ta e="T574" id="Seg_7623" s="T573">iti</ta>
            <ta e="T575" id="Seg_7624" s="T574">tugut</ta>
            <ta e="T576" id="Seg_7625" s="T575">bu͡ol-IAK-tI-n</ta>
            <ta e="T577" id="Seg_7626" s="T576">naːda</ta>
            <ta e="T579" id="Seg_7627" s="T578">dʼe</ta>
            <ta e="T580" id="Seg_7628" s="T579">ol-ttAn</ta>
            <ta e="T581" id="Seg_7629" s="T580">tahaːra-BIt</ta>
            <ta e="T582" id="Seg_7630" s="T581">karaŋar-TI-tA</ta>
            <ta e="T583" id="Seg_7631" s="T582">bu͡o</ta>
            <ta e="T585" id="Seg_7632" s="T584">bosku͡oj-In</ta>
            <ta e="T586" id="Seg_7633" s="T585">da</ta>
            <ta e="T587" id="Seg_7634" s="T586">kim-BIt</ta>
            <ta e="T588" id="Seg_7635" s="T587">ačaːk-BIt</ta>
            <ta e="T589" id="Seg_7636" s="T588">ka</ta>
            <ta e="T591" id="Seg_7637" s="T590">maːma-m</ta>
            <ta e="T592" id="Seg_7638" s="T591">delbi</ta>
            <ta e="T593" id="Seg_7639" s="T592">kaːlaː-BIT</ta>
            <ta e="T598" id="Seg_7640" s="T597">kim</ta>
            <ta e="T599" id="Seg_7641" s="T598">mas</ta>
            <ta e="T600" id="Seg_7642" s="T599">ka</ta>
            <ta e="T601" id="Seg_7643" s="T600">ubaj-Ar-tA</ta>
            <ta e="T602" id="Seg_7644" s="T601">ka</ta>
            <ta e="T603" id="Seg_7645" s="T602">kɨtar-čI</ta>
            <ta e="T604" id="Seg_7646" s="T603">ačaːk-BIt</ta>
            <ta e="T606" id="Seg_7647" s="T605">čajnik-LAr</ta>
            <ta e="T607" id="Seg_7648" s="T606">köt-I-n-An</ta>
            <ta e="T609" id="Seg_7649" s="T608">telibireː-AːktAː-Ar-LAr</ta>
            <ta e="T610" id="Seg_7650" s="T609">di͡e-An</ta>
            <ta e="T611" id="Seg_7651" s="T610">kappak-LArA</ta>
            <ta e="T613" id="Seg_7652" s="T612">kü͡ös-BIt</ta>
            <ta e="T614" id="Seg_7653" s="T613">laglɨ͡a</ta>
            <ta e="T615" id="Seg_7654" s="T614">tur-AːktAː-Ar</ta>
            <ta e="T616" id="Seg_7655" s="T615">di͡e-An</ta>
            <ta e="T617" id="Seg_7656" s="T616">laglɨ͡a</ta>
            <ta e="T620" id="Seg_7657" s="T619">itij</ta>
            <ta e="T621" id="Seg_7658" s="T620">aːn</ta>
            <ta e="T622" id="Seg_7659" s="T621">tünnük-nI</ta>
            <ta e="T623" id="Seg_7660" s="T622">arɨj-TI-tA</ta>
            <ta e="T624" id="Seg_7661" s="T623">ü͡öles-tA</ta>
            <ta e="T625" id="Seg_7662" s="T624">arɨj-TI-tA</ta>
            <ta e="T627" id="Seg_7663" s="T626">ü͡öles</ta>
            <ta e="T628" id="Seg_7664" s="T627">bu͡ol-AːččI</ta>
            <ta e="T629" id="Seg_7665" s="T628">kim</ta>
            <ta e="T630" id="Seg_7666" s="T629">bü͡öleː-AːččI-LAr</ta>
            <ta e="T634" id="Seg_7667" s="T633">tɨ͡al</ta>
            <ta e="T635" id="Seg_7668" s="T634">kiːr-IAK-tI-n</ta>
            <ta e="T636" id="Seg_7669" s="T635">ka</ta>
            <ta e="T638" id="Seg_7670" s="T637">dʼe</ta>
            <ta e="T639" id="Seg_7671" s="T638">ol</ta>
            <ta e="T640" id="Seg_7672" s="T639">arɨj-TI-tA</ta>
            <ta e="T641" id="Seg_7673" s="T640">eː</ta>
            <ta e="T643" id="Seg_7674" s="T642">min</ta>
            <ta e="T644" id="Seg_7675" s="T643">du͡o</ta>
            <ta e="T645" id="Seg_7676" s="T644">hürdeːk</ta>
            <ta e="T646" id="Seg_7677" s="T645">bagar-AːččI-BIn</ta>
            <ta e="T647" id="Seg_7678" s="T646">tünnük-GA</ta>
            <ta e="T648" id="Seg_7679" s="T647">tünnük-GA</ta>
            <ta e="T649" id="Seg_7680" s="T648">kör-IAK-BI-n</ta>
            <ta e="T650" id="Seg_7681" s="T649">ka</ta>
            <ta e="T652" id="Seg_7682" s="T651">mɨlaj-A</ta>
            <ta e="T653" id="Seg_7683" s="T652">mɨlaj-A</ta>
            <ta e="T654" id="Seg_7684" s="T653">olor-AːččI-BIn</ta>
            <ta e="T656" id="Seg_7685" s="T655">bosku͡oj-In</ta>
            <ta e="T657" id="Seg_7686" s="T656">da</ta>
            <ta e="T658" id="Seg_7687" s="T657">eː</ta>
            <ta e="T659" id="Seg_7688" s="T658">kim</ta>
            <ta e="T660" id="Seg_7689" s="T659">ɨj</ta>
            <ta e="T663" id="Seg_7690" s="T662">ɨj</ta>
            <ta e="T664" id="Seg_7691" s="T663">erej-kAːN</ta>
            <ta e="T665" id="Seg_7692" s="T664">e-BIT-tA</ta>
            <ta e="T666" id="Seg_7693" s="T665">eː</ta>
            <ta e="T668" id="Seg_7694" s="T667">ɨj-BIt</ta>
            <ta e="T669" id="Seg_7695" s="T668">bosku͡oj</ta>
            <ta e="T670" id="Seg_7696" s="T669">bagajɨ</ta>
            <ta e="T671" id="Seg_7697" s="T670">no</ta>
            <ta e="T672" id="Seg_7698" s="T671">[C^1][V^1][C^2]-karaŋa</ta>
            <ta e="T674" id="Seg_7699" s="T673">tu͡ok-LAr</ta>
            <ta e="T675" id="Seg_7700" s="T674">köt-AːččI-LAr</ta>
            <ta e="T676" id="Seg_7701" s="T675">e-BIT</ta>
            <ta e="T677" id="Seg_7702" s="T676">min</ta>
            <ta e="T679" id="Seg_7703" s="T678">bil-BAT</ta>
            <ta e="T680" id="Seg_7704" s="T679">e-TI-m</ta>
            <ta e="T682" id="Seg_7705" s="T681">leŋkej</ta>
            <ta e="T683" id="Seg_7706" s="T682">kördük</ta>
            <ta e="T684" id="Seg_7707" s="T683">leŋkej-LAr</ta>
            <ta e="T685" id="Seg_7708" s="T684">di͡e-An</ta>
            <ta e="T686" id="Seg_7709" s="T685">hu͡ok-tA</ta>
            <ta e="T687" id="Seg_7710" s="T686">küččügüj-LAr</ta>
            <ta e="T689" id="Seg_7711" s="T688">menʼiː-tA</ta>
            <ta e="T690" id="Seg_7712" s="T689">hu͡ok-LAr</ta>
            <ta e="T691" id="Seg_7713" s="T690">di͡e-AːččI-BIn</ta>
            <ta e="T692" id="Seg_7714" s="T691">menʼiː-tA</ta>
            <ta e="T693" id="Seg_7715" s="T692">hu͡ok-LAr</ta>
            <ta e="T694" id="Seg_7716" s="T693">hɨrɨt-Ar-LAr</ta>
            <ta e="T695" id="Seg_7717" s="T694">maːma</ta>
            <ta e="T696" id="Seg_7718" s="T695">kör</ta>
            <ta e="T699" id="Seg_7719" s="T698">iti</ta>
            <ta e="T700" id="Seg_7720" s="T699">kim</ta>
            <ta e="T701" id="Seg_7721" s="T700">eː</ta>
            <ta e="T702" id="Seg_7722" s="T701">bu</ta>
            <ta e="T703" id="Seg_7723" s="T702">čok</ta>
            <ta e="T704" id="Seg_7724" s="T703">köt-Ar</ta>
            <ta e="T705" id="Seg_7725" s="T704">bu͡o</ta>
            <ta e="T706" id="Seg_7726" s="T705">truba-ttAn</ta>
            <ta e="T707" id="Seg_7727" s="T706">ka</ta>
            <ta e="T709" id="Seg_7728" s="T708">ol-GA</ta>
            <ta e="T710" id="Seg_7729" s="T709">möŋ-s-Ar-LAr</ta>
            <ta e="T711" id="Seg_7730" s="T710">e-BIT</ta>
            <ta e="T712" id="Seg_7731" s="T711">iti-LAr-I-ŋ</ta>
            <ta e="T714" id="Seg_7732" s="T713">bagajɨ</ta>
            <ta e="T716" id="Seg_7733" s="T715">ol-ttAn</ta>
            <ta e="T717" id="Seg_7734" s="T716">töhö</ta>
            <ta e="T718" id="Seg_7735" s="T717">eme</ta>
            <ta e="T719" id="Seg_7736" s="T718">maːma-m</ta>
            <ta e="T720" id="Seg_7737" s="T719">iːsten</ta>
            <ta e="T721" id="Seg_7738" s="T720">olor-Ar</ta>
            <ta e="T722" id="Seg_7739" s="T721">iːsten</ta>
            <ta e="T723" id="Seg_7740" s="T722">olor-Ar</ta>
            <ta e="T725" id="Seg_7741" s="T724">orgujakaːn</ta>
            <ta e="T726" id="Seg_7742" s="T725">orgujakaːn</ta>
            <ta e="T727" id="Seg_7743" s="T726">diː</ta>
            <ta e="T728" id="Seg_7744" s="T727">diː</ta>
            <ta e="T729" id="Seg_7745" s="T728">ɨllaː-An</ta>
            <ta e="T730" id="Seg_7746" s="T729">olor-Ar</ta>
            <ta e="T731" id="Seg_7747" s="T730">orgujakaːn</ta>
            <ta e="T733" id="Seg_7748" s="T732">Djoŋgo-BIt</ta>
            <ta e="T734" id="Seg_7749" s="T733">hɨt-Ar</ta>
            <ta e="T736" id="Seg_7750" s="T735">nʼik</ta>
            <ta e="T737" id="Seg_7751" s="T736">da</ta>
            <ta e="T738" id="Seg_7752" s="T737">gɨn-BAT</ta>
            <ta e="T740" id="Seg_7753" s="T739">hɨlaj-BIT</ta>
            <ta e="T741" id="Seg_7754" s="T740">bɨhɨːlaːk</ta>
            <ta e="T742" id="Seg_7755" s="T741">tugut-tI-n</ta>
            <ta e="T743" id="Seg_7756" s="T742">kɨtta</ta>
            <ta e="T745" id="Seg_7757" s="T744">ol-ttAn</ta>
            <ta e="T746" id="Seg_7758" s="T745">kim</ta>
            <ta e="T749" id="Seg_7759" s="T748">bar-TI-tA</ta>
            <ta e="T750" id="Seg_7760" s="T749">bu͡o</ta>
            <ta e="T753" id="Seg_7761" s="T752">ol</ta>
            <ta e="T754" id="Seg_7762" s="T753">tu͡ok</ta>
            <ta e="T755" id="Seg_7763" s="T754">itte</ta>
            <ta e="T756" id="Seg_7764" s="T755">bütte</ta>
            <ta e="T757" id="Seg_7765" s="T756">min</ta>
            <ta e="T759" id="Seg_7766" s="T758">teːte-m</ta>
            <ta e="T760" id="Seg_7767" s="T759">hu͡ok-kAːN-tI-nAn</ta>
            <ta e="T761" id="Seg_7768" s="T760">radʼio-nA</ta>
            <ta e="T762" id="Seg_7769" s="T761">vklučaj-LAː-I-n-A-BIn</ta>
            <ta e="T764" id="Seg_7770" s="T763">eː</ta>
            <ta e="T765" id="Seg_7771" s="T764">gaːgɨnat-AːktAː-A-BIn</ta>
            <ta e="T766" id="Seg_7772" s="T765">dʼe</ta>
            <ta e="T768" id="Seg_7773" s="T767">eː</ta>
            <ta e="T769" id="Seg_7774" s="T768">orgujakaːn</ta>
            <ta e="T770" id="Seg_7775" s="T769">gɨn</ta>
            <ta e="T771" id="Seg_7776" s="T770">beje</ta>
            <ta e="T772" id="Seg_7777" s="T771">talɨgɨraːt-I-BA</ta>
            <ta e="T774" id="Seg_7778" s="T773">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T775" id="Seg_7779" s="T774">ihilleː-A</ta>
            <ta e="T776" id="Seg_7780" s="T775">hɨt-IAK-m</ta>
            <ta e="T777" id="Seg_7781" s="T776">bu͡o</ta>
            <ta e="T780" id="Seg_7782" s="T779">kim-LAr-nI</ta>
            <ta e="T781" id="Seg_7783" s="T780">dʼe</ta>
            <ta e="T782" id="Seg_7784" s="T781">ihilleː-A-BIn</ta>
            <ta e="T784" id="Seg_7785" s="T783">kitajɨs-LAr-LAr</ta>
            <ta e="T785" id="Seg_7786" s="T784">tu͡ok-LAr</ta>
            <ta e="T786" id="Seg_7787" s="T785">du͡o</ta>
            <ta e="T788" id="Seg_7788" s="T787">ol-LAr-nI</ta>
            <ta e="T789" id="Seg_7789" s="T788">ihilleː-An</ta>
            <ta e="T790" id="Seg_7790" s="T789">bu͡olla</ta>
            <ta e="T791" id="Seg_7791" s="T790">hɨt-AːktAː-A-BIn</ta>
            <ta e="T792" id="Seg_7792" s="T791">di͡e-An</ta>
            <ta e="T793" id="Seg_7793" s="T792">oččogo</ta>
            <ta e="T794" id="Seg_7794" s="T793">beseleː</ta>
            <ta e="T795" id="Seg_7795" s="T794">bagajɨ</ta>
            <ta e="T796" id="Seg_7796" s="T795">bil-A-BIn</ta>
            <ta e="T797" id="Seg_7797" s="T796">ol-ttAn</ta>
            <ta e="T798" id="Seg_7798" s="T797">hi͡ese</ta>
            <ta e="T799" id="Seg_7799" s="T798">nuːčča-LAr</ta>
            <ta e="T800" id="Seg_7800" s="T799">bul-TI-m</ta>
            <ta e="T801" id="Seg_7801" s="T800">ol-ttAn</ta>
            <ta e="T802" id="Seg_7802" s="T801">büt-An</ta>
            <ta e="T803" id="Seg_7803" s="T802">kaːl-BIT</ta>
            <ta e="T805" id="Seg_7804" s="T804">dʼe</ta>
            <ta e="T806" id="Seg_7805" s="T805">büt-A-r-An</ta>
            <ta e="T807" id="Seg_7806" s="T806">keːs-IAK-GA</ta>
            <ta e="T808" id="Seg_7807" s="T807">büt-A-r</ta>
            <ta e="T809" id="Seg_7808" s="T808">teːte-ŋ</ta>
            <ta e="T810" id="Seg_7809" s="T809">kel-IAK.[tA]</ta>
            <ta e="T811" id="Seg_7810" s="T810">eː</ta>
            <ta e="T813" id="Seg_7811" s="T812">kün-BIt</ta>
            <ta e="T814" id="Seg_7812" s="T813">ɨj-BIt</ta>
            <ta e="T815" id="Seg_7813" s="T814">ɨraːt-TI-tA</ta>
            <ta e="T816" id="Seg_7814" s="T815">bu͡o</ta>
            <ta e="T817" id="Seg_7815" s="T816">ü͡öhe</ta>
            <ta e="T818" id="Seg_7816" s="T817">bu͡ol-TI-tA</ta>
            <ta e="T819" id="Seg_7817" s="T818">ama</ta>
            <ta e="T820" id="Seg_7818" s="T819">bosku͡oj</ta>
            <ta e="T822" id="Seg_7819" s="T821">ama</ta>
            <ta e="T823" id="Seg_7820" s="T822">hɨrdɨk</ta>
            <ta e="T824" id="Seg_7821" s="T823">künüs</ta>
            <ta e="T825" id="Seg_7822" s="T824">kördük</ta>
            <ta e="T827" id="Seg_7823" s="T826">o</ta>
            <ta e="T828" id="Seg_7824" s="T827">taba-LAr-BIt</ta>
            <ta e="T830" id="Seg_7825" s="T829">ebe</ta>
            <ta e="T831" id="Seg_7826" s="T830">üstün</ta>
            <ta e="T832" id="Seg_7827" s="T831">kötüt-A</ta>
            <ta e="T833" id="Seg_7828" s="T832">hɨrɨt-AːktAː-Ar-LAr</ta>
            <ta e="T834" id="Seg_7829" s="T833">dʼe</ta>
            <ta e="T835" id="Seg_7830" s="T834">ama</ta>
            <ta e="T836" id="Seg_7831" s="T835">bosku͡oj-In</ta>
            <ta e="T837" id="Seg_7832" s="T836">da</ta>
            <ta e="T839" id="Seg_7833" s="T838">kim</ta>
            <ta e="T840" id="Seg_7834" s="T839">buru͡o-LArA</ta>
            <ta e="T841" id="Seg_7835" s="T840">aš</ta>
            <ta e="T842" id="Seg_7836" s="T841">köt-Ar</ta>
            <ta e="T844" id="Seg_7837" s="T843">oj</ta>
            <ta e="T845" id="Seg_7838" s="T844">ama</ta>
            <ta e="T846" id="Seg_7839" s="T845">bosku͡oj</ta>
            <ta e="T847" id="Seg_7840" s="T846">kim</ta>
            <ta e="T848" id="Seg_7841" s="T847">ka</ta>
            <ta e="T849" id="Seg_7842" s="T848">kaːr-I-ŋ</ta>
            <ta e="T850" id="Seg_7843" s="T849">ka</ta>
            <ta e="T851" id="Seg_7844" s="T850">kim-GA</ta>
            <ta e="T852" id="Seg_7845" s="T851">lunaː-ttAn</ta>
            <ta e="T853" id="Seg_7846" s="T852">ama</ta>
            <ta e="T854" id="Seg_7847" s="T853">kim-LAː-Ar</ta>
            <ta e="T855" id="Seg_7848" s="T854">gɨlbaj-ŋnAː-Ar</ta>
            <ta e="T857" id="Seg_7849" s="T856">ol-ttAn</ta>
            <ta e="T858" id="Seg_7850" s="T857">ol-ttAn</ta>
            <ta e="T859" id="Seg_7851" s="T858">beːbe</ta>
            <ta e="T860" id="Seg_7852" s="T859">ɨt-BIt</ta>
            <ta e="T863" id="Seg_7853" s="T862">gɨn-Ar</ta>
            <ta e="T864" id="Seg_7854" s="T863">bu</ta>
            <ta e="T865" id="Seg_7855" s="T864">kim-LAː-Ar</ta>
            <ta e="T866" id="Seg_7856" s="T865">muŋurgaː-Ar</ta>
            <ta e="T868" id="Seg_7857" s="T867">beː</ta>
            <ta e="T869" id="Seg_7858" s="T868">ču͡oraːn</ta>
            <ta e="T870" id="Seg_7859" s="T869">tɨ͡ahaː-TI-tA</ta>
            <ta e="T871" id="Seg_7860" s="T870">agaj</ta>
            <ta e="T872" id="Seg_7861" s="T871">küpüleːn</ta>
            <ta e="T877" id="Seg_7862" s="T876">teːte-BIt</ta>
            <ta e="T878" id="Seg_7863" s="T877">is-Ar</ta>
            <ta e="T879" id="Seg_7864" s="T878">teːte-BIt</ta>
            <ta e="T880" id="Seg_7865" s="T879">eː</ta>
            <ta e="T882" id="Seg_7866" s="T881">dʼe</ta>
            <ta e="T883" id="Seg_7867" s="T882">ol</ta>
            <ta e="T884" id="Seg_7868" s="T883">kel-TI-tA</ta>
            <ta e="T886" id="Seg_7869" s="T885">min</ta>
            <ta e="T887" id="Seg_7870" s="T886">du͡o</ta>
            <ta e="T888" id="Seg_7871" s="T887">bil-BAtAK-I-m</ta>
            <ta e="T889" id="Seg_7872" s="T888">bu͡o</ta>
            <ta e="T890" id="Seg_7873" s="T889">mas-LAː-n-A</ta>
            <ta e="T891" id="Seg_7874" s="T890">bar-BIT</ta>
            <ta e="T892" id="Seg_7875" s="T891">di͡e-A-BIn</ta>
            <ta e="T893" id="Seg_7876" s="T892">bu͡o</ta>
            <ta e="T895" id="Seg_7877" s="T894">gini-ŋ</ta>
            <ta e="T896" id="Seg_7878" s="T895">bu͡o</ta>
            <ta e="T897" id="Seg_7879" s="T896">Dudʼinka</ta>
            <ta e="T898" id="Seg_7880" s="T897">dek</ta>
            <ta e="T899" id="Seg_7881" s="T898">hɨrɨt-A-BIT</ta>
            <ta e="T900" id="Seg_7882" s="T899">i</ta>
            <ta e="T901" id="Seg_7883" s="T900">Dudʼinka</ta>
            <ta e="T902" id="Seg_7884" s="T901">dek</ta>
            <ta e="T904" id="Seg_7885" s="T903">dʼe</ta>
            <ta e="T905" id="Seg_7886" s="T904">ol</ta>
            <ta e="T906" id="Seg_7887" s="T905">kel-TI-tA</ta>
            <ta e="T909" id="Seg_7888" s="T908">karaŋar-BIT-tI-n</ta>
            <ta e="T910" id="Seg_7889" s="T909">genne</ta>
            <ta e="T911" id="Seg_7890" s="T910">ka</ta>
            <ta e="T913" id="Seg_7891" s="T912">o</ta>
            <ta e="T914" id="Seg_7892" s="T913">teːte-m</ta>
            <ta e="T915" id="Seg_7893" s="T914">kel-TI-tA</ta>
            <ta e="T917" id="Seg_7894" s="T916">o</ta>
            <ta e="T918" id="Seg_7895" s="T917">maːma-m</ta>
            <ta e="T919" id="Seg_7896" s="T918">kömölös-A</ta>
            <ta e="T920" id="Seg_7897" s="T919">bar-TI-tA</ta>
            <ta e="T921" id="Seg_7898" s="T920">hü͡ör-TI-tA</ta>
            <ta e="T922" id="Seg_7899" s="T921">kanʼaː-TI-tA</ta>
            <ta e="T923" id="Seg_7900" s="T922">küpüleːn-ttAn</ta>
            <ta e="T924" id="Seg_7901" s="T923">taba-LArI-n</ta>
            <ta e="T925" id="Seg_7902" s="T924">ɨːt-TI-LAr</ta>
            <ta e="T926" id="Seg_7903" s="T925">kanʼaː-TI-LAr</ta>
            <ta e="T928" id="Seg_7904" s="T927">o</ta>
            <ta e="T929" id="Seg_7905" s="T928">teːte-m</ta>
            <ta e="T930" id="Seg_7906" s="T929">mesok-LAr-LAːK</ta>
            <ta e="T931" id="Seg_7907" s="T930">kel-TI-tA</ta>
            <ta e="T933" id="Seg_7908" s="T932">küːl-LAr-tI-n</ta>
            <ta e="T934" id="Seg_7909" s="T933">kiːr-TAr-TI-tA</ta>
            <ta e="T936" id="Seg_7910" s="T935">dʼe</ta>
            <ta e="T937" id="Seg_7911" s="T936">kostoː-t-AlAː-TI-tA</ta>
            <ta e="T938" id="Seg_7912" s="T937">o</ta>
            <ta e="T939" id="Seg_7913" s="T938">dʼe</ta>
            <ta e="T940" id="Seg_7914" s="T939">kempi͡et</ta>
            <ta e="T941" id="Seg_7915" s="T940">üleger-tI-n</ta>
            <ta e="T942" id="Seg_7916" s="T941">egel-BIT</ta>
            <ta e="T944" id="Seg_7917" s="T943">ol-ttAn</ta>
            <ta e="T945" id="Seg_7918" s="T944">bosku͡oj</ta>
            <ta e="T946" id="Seg_7919" s="T945">bagajɨ</ta>
            <ta e="T947" id="Seg_7920" s="T946">bi͡ek</ta>
            <ta e="T948" id="Seg_7921" s="T947">öjdöː-A-BIn</ta>
            <ta e="T950" id="Seg_7922" s="T949">kim</ta>
            <ta e="T951" id="Seg_7923" s="T950">čaːs-LAr</ta>
            <ta e="T952" id="Seg_7924" s="T951">du͡o</ta>
            <ta e="T953" id="Seg_7925" s="T952">bosku͡oj</ta>
            <ta e="T954" id="Seg_7926" s="T953">bagajɨ</ta>
            <ta e="T955" id="Seg_7927" s="T954">kim</ta>
            <ta e="T956" id="Seg_7928" s="T955">hi͡erkile-nAn</ta>
            <ta e="T957" id="Seg_7929" s="T956">oŋohulun-I-BIT</ta>
            <ta e="T959" id="Seg_7930" s="T958">ol-tI-ŋ</ta>
            <ta e="T961" id="Seg_7931" s="T960">knopka-LAr-LAːK</ta>
            <ta e="T962" id="Seg_7932" s="T961">tu͡ok</ta>
            <ta e="T963" id="Seg_7933" s="T962">ere</ta>
            <ta e="T964" id="Seg_7934" s="T963">ama</ta>
            <ta e="T965" id="Seg_7935" s="T964">bosku͡oj</ta>
            <ta e="T966" id="Seg_7936" s="T965">oduːrgaː-AːktAː-A-BIn</ta>
            <ta e="T967" id="Seg_7937" s="T966">ama</ta>
            <ta e="T968" id="Seg_7938" s="T967">bosku͡oj</ta>
            <ta e="T970" id="Seg_7939" s="T969">dʼe</ta>
            <ta e="T971" id="Seg_7940" s="T970">tɨːt-I-BA</ta>
            <ta e="T972" id="Seg_7941" s="T971">höp</ta>
            <ta e="T973" id="Seg_7942" s="T972">bu͡ol-IAK.[tA]</ta>
            <ta e="T974" id="Seg_7943" s="T973">di͡e-Ar-LAr</ta>
            <ta e="T976" id="Seg_7944" s="T975">tɨːt-TAr-AːččI-tA</ta>
            <ta e="T977" id="Seg_7945" s="T976">hu͡ok-LAr</ta>
            <ta e="T978" id="Seg_7946" s="T977">bu͡o</ta>
            <ta e="T980" id="Seg_7947" s="T979">aldʼat-IAK-ŋ</ta>
            <ta e="T981" id="Seg_7948" s="T980">kaja</ta>
            <ta e="T982" id="Seg_7949" s="T981">eː</ta>
            <ta e="T984" id="Seg_7950" s="T983">dʼe</ta>
            <ta e="T985" id="Seg_7951" s="T984">uːr-TI-m</ta>
            <ta e="T986" id="Seg_7952" s="T985">hürdeːk</ta>
            <ta e="T987" id="Seg_7953" s="T986">kör-IAK-BI-n</ta>
            <ta e="T988" id="Seg_7954" s="T987">bagar-A-BIn</ta>
            <ta e="T989" id="Seg_7955" s="T988">tɨːt-AlAː-IAK-BI-n</ta>
            <ta e="T990" id="Seg_7956" s="T989">bagar-A-BIn</ta>
            <ta e="T991" id="Seg_7957" s="T990">bu͡o</ta>
            <ta e="T993" id="Seg_7958" s="T992">ol-ttAn</ta>
            <ta e="T994" id="Seg_7959" s="T993">kim</ta>
            <ta e="T995" id="Seg_7960" s="T994">kim-nI</ta>
            <ta e="T996" id="Seg_7961" s="T995">egel-BIT-tA</ta>
            <ta e="T997" id="Seg_7962" s="T996">min-GA</ta>
            <ta e="T999" id="Seg_7963" s="T998">kompas-kAːN</ta>
            <ta e="T1000" id="Seg_7964" s="T999">küččügüj</ta>
            <ta e="T1002" id="Seg_7965" s="T1001">ol-nI</ta>
            <ta e="T1003" id="Seg_7966" s="T1002">bi͡ek</ta>
            <ta e="T1004" id="Seg_7967" s="T1003">oːnnʼoː-A</ta>
            <ta e="T1005" id="Seg_7968" s="T1004">hɨt-AːččI-BIn</ta>
            <ta e="T1006" id="Seg_7969" s="T1005">araː</ta>
            <ta e="T1007" id="Seg_7970" s="T1006">bosku͡oj-In</ta>
            <ta e="T1008" id="Seg_7971" s="T1007">du͡o</ta>
            <ta e="T1009" id="Seg_7972" s="T1008">urut</ta>
            <ta e="T1012" id="Seg_7973" s="T1011">egel-BIT-tA</ta>
            <ta e="T1013" id="Seg_7974" s="T1012">bu</ta>
            <ta e="T1014" id="Seg_7975" s="T1013">kördük</ta>
            <ta e="T1015" id="Seg_7976" s="T1014">oːnnʼuːr-nI</ta>
            <ta e="T1016" id="Seg_7977" s="T1015">ka</ta>
            <ta e="T1018" id="Seg_7978" s="T1017">o</ta>
            <ta e="T1019" id="Seg_7979" s="T1018">min</ta>
            <ta e="T1020" id="Seg_7980" s="T1019">muŋ</ta>
            <ta e="T1021" id="Seg_7981" s="T1020">hu͡ok</ta>
            <ta e="T1023" id="Seg_7982" s="T1022">bosku͡oj-In</ta>
            <ta e="T1024" id="Seg_7983" s="T1023">da</ta>
            <ta e="T1026" id="Seg_7984" s="T1025">o</ta>
            <ta e="T1027" id="Seg_7985" s="T1026">en-GA</ta>
            <ta e="T1029" id="Seg_7986" s="T1028">di͡e-Ar</ta>
            <ta e="T1031" id="Seg_7987" s="T1030">dʼe</ta>
            <ta e="T1032" id="Seg_7988" s="T1031">ol</ta>
            <ta e="T1033" id="Seg_7989" s="T1032">kepset-Ar-LAr</ta>
            <ta e="T1034" id="Seg_7990" s="T1033">eni</ta>
            <ta e="T1035" id="Seg_7991" s="T1034">ka</ta>
            <ta e="T1036" id="Seg_7992" s="T1035">min</ta>
            <ta e="T1037" id="Seg_7993" s="T1036">utuj-An</ta>
            <ta e="T1038" id="Seg_7994" s="T1037">kaːl-BIT-BIn</ta>
            <ta e="T1040" id="Seg_7995" s="T1039">harsi͡erda</ta>
            <ta e="T1041" id="Seg_7996" s="T1040">uže</ta>
            <ta e="T1042" id="Seg_7997" s="T1041">uhugun-A-BIn</ta>
            <ta e="T1043" id="Seg_7998" s="T1042">o</ta>
            <ta e="T1044" id="Seg_7999" s="T1043">ačaːk</ta>
            <ta e="T1045" id="Seg_8000" s="T1044">emi͡e</ta>
            <ta e="T1046" id="Seg_8001" s="T1045">köt-I-t-A</ta>
            <ta e="T1047" id="Seg_8002" s="T1046">tur-Ar</ta>
            <ta e="T1049" id="Seg_8003" s="T1048">bosku͡oj-In</ta>
            <ta e="T1050" id="Seg_8004" s="T1049">du͡o</ta>
            <ta e="T1051" id="Seg_8005" s="T1050">kim</ta>
            <ta e="T1052" id="Seg_8006" s="T1051">da</ta>
            <ta e="T1053" id="Seg_8007" s="T1052">hu͡ok</ta>
            <ta e="T1054" id="Seg_8008" s="T1053">dʼi͡e-BIt</ta>
            <ta e="T1055" id="Seg_8009" s="T1054">is-tI-GAr</ta>
            <ta e="T1057" id="Seg_8010" s="T1056">ol</ta>
            <ta e="T1058" id="Seg_8011" s="T1057">taba-LArI-n</ta>
            <ta e="T1059" id="Seg_8012" s="T1058">tut-Ar-LAr</ta>
            <ta e="T1060" id="Seg_8013" s="T1059">e-BIT</ta>
            <ta e="T1062" id="Seg_8014" s="T1061">tutun-A</ta>
            <ta e="T1063" id="Seg_8015" s="T1062">tagɨs-I-BIT-LAr</ta>
            <ta e="T1065" id="Seg_8016" s="T1064">ɨ͡al-LAr-LAːK</ta>
            <ta e="T1066" id="Seg_8017" s="T1065">e-TI-BIt</ta>
            <ta e="T1067" id="Seg_8018" s="T1066">taːk</ta>
            <ta e="T1068" id="Seg_8019" s="T1067">da</ta>
            <ta e="T1069" id="Seg_8020" s="T1068">baːr</ta>
            <ta e="T1070" id="Seg_8021" s="T1069">e-TI-LArA</ta>
            <ta e="T1071" id="Seg_8022" s="T1070">ɨ͡al-LAr-BIt</ta>
            <ta e="T1073" id="Seg_8023" s="T1072">dʼe</ta>
            <ta e="T1074" id="Seg_8024" s="T1073">ol</ta>
            <ta e="T1075" id="Seg_8025" s="T1074">komuj-TI-LAr</ta>
            <ta e="T1076" id="Seg_8026" s="T1075">ü͡ör-LArI-n</ta>
            <ta e="T1077" id="Seg_8027" s="T1076">komuj-n-Ar-LAr</ta>
            <ta e="T1078" id="Seg_8028" s="T1077">taba</ta>
            <ta e="T1079" id="Seg_8029" s="T1078">tu͡ok</ta>
            <ta e="T1080" id="Seg_8030" s="T1079">kös-A-BIt</ta>
            <ta e="T1081" id="Seg_8031" s="T1080">bɨhɨːlaːk</ta>
            <ta e="T1082" id="Seg_8032" s="T1081">öjdöː-A-BIn</ta>
            <ta e="T1083" id="Seg_8033" s="T1082">kanna</ta>
            <ta e="T1084" id="Seg_8034" s="T1083">ere</ta>
            <ta e="T1086" id="Seg_8035" s="T1085">o</ta>
            <ta e="T1087" id="Seg_8036" s="T1086">dʼe</ta>
            <ta e="T1088" id="Seg_8037" s="T1087">min-n</ta>
            <ta e="T1089" id="Seg_8038" s="T1088">taŋɨn-TAr-An</ta>
            <ta e="T1090" id="Seg_8039" s="T1089">čičigere-TI-LAr</ta>
            <ta e="T1092" id="Seg_8040" s="T1091">hɨrga-GA</ta>
            <ta e="T1093" id="Seg_8041" s="T1092">baːj-An</ta>
            <ta e="T1094" id="Seg_8042" s="T1093">keːs-BIT-LAr</ta>
            <ta e="T1095" id="Seg_8043" s="T1094">önül-An</ta>
            <ta e="T1096" id="Seg_8044" s="T1095">keːs-BIT-LAr</ta>
            <ta e="T1098" id="Seg_8045" s="T1097">dʼe</ta>
            <ta e="T1099" id="Seg_8046" s="T1098">kajdi͡ek</ta>
            <ta e="T1100" id="Seg_8047" s="T1099">bar-A-BIt</ta>
            <ta e="T1102" id="Seg_8048" s="T1101">i</ta>
            <ta e="T1103" id="Seg_8049" s="T1102">kelin-BIt-ttAn</ta>
            <ta e="T1104" id="Seg_8050" s="T1103">du͡o</ta>
            <ta e="T1105" id="Seg_8051" s="T1104">taba-LAr-nI</ta>
            <ta e="T1106" id="Seg_8052" s="T1105">hi͡et-AːččI-LAr</ta>
            <ta e="T1107" id="Seg_8053" s="T1106">kosobu͡oj-GA</ta>
            <ta e="T1109" id="Seg_8054" s="T1108">as-LAːK</ta>
            <ta e="T1110" id="Seg_8055" s="T1109">ili</ta>
            <ta e="T1111" id="Seg_8056" s="T1110">mas-LAːK</ta>
            <ta e="T1112" id="Seg_8057" s="T1111">kosobu͡oj</ta>
            <ta e="T1113" id="Seg_8058" s="T1112">hi͡et-AːččI</ta>
            <ta e="T1114" id="Seg_8059" s="T1113">e-TI-BIt</ta>
            <ta e="T1116" id="Seg_8060" s="T1115">ol-LAr-ttAn</ta>
            <ta e="T1117" id="Seg_8061" s="T1116">kuttan-An</ta>
            <ta e="T1118" id="Seg_8062" s="T1117">ɨtaː-An</ta>
            <ta e="T1119" id="Seg_8063" s="T1118">mörüleːkteː-A-BIn</ta>
            <ta e="T1120" id="Seg_8064" s="T1119">di͡e-An</ta>
            <ta e="T1122" id="Seg_8065" s="T1121">kuttan-I-m</ta>
            <ta e="T1123" id="Seg_8066" s="T1122">ɨtɨr-IAK.[tA]</ta>
            <ta e="T1124" id="Seg_8067" s="T1123">hu͡ok-tA</ta>
            <ta e="T1126" id="Seg_8068" s="T1125">bu</ta>
            <ta e="T1127" id="Seg_8069" s="T1126">ka</ta>
            <ta e="T1128" id="Seg_8070" s="T1127">hɨlaj-BIT-LAr</ta>
            <ta e="T1129" id="Seg_8071" s="T1128">ka</ta>
            <ta e="T1130" id="Seg_8072" s="T1129">tɨːn-Ar-LAr</ta>
            <ta e="T1133" id="Seg_8073" s="T1132">hɨraj-BA-r</ta>
            <ta e="T1134" id="Seg_8074" s="T1133">manna</ta>
            <ta e="T1136" id="Seg_8075" s="T1135">dʼe</ta>
            <ta e="T1137" id="Seg_8076" s="T1136">ol</ta>
            <ta e="T1138" id="Seg_8077" s="T1137">iti</ta>
            <ta e="T1139" id="Seg_8078" s="T1138">kördük</ta>
            <ta e="T1140" id="Seg_8079" s="T1139">kös-A</ta>
            <ta e="T1141" id="Seg_8080" s="T1140">hɨrɨt-AːččI</ta>
            <ta e="T1142" id="Seg_8081" s="T1141">e-TI-BIt</ta>
            <ta e="T1143" id="Seg_8082" s="T1142">kös-A</ta>
            <ta e="T1144" id="Seg_8083" s="T1143">hɨrɨt-AːččI</ta>
            <ta e="T1145" id="Seg_8084" s="T1144">kös-I-s-A</ta>
            <ta e="T1146" id="Seg_8085" s="T1145">hɨrɨt-AːččI</ta>
            <ta e="T1147" id="Seg_8086" s="T1146">e-TI-m</ta>
            <ta e="T1148" id="Seg_8087" s="T1147">giniler-nI</ta>
            <ta e="T1149" id="Seg_8088" s="T1148">kɨtta</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_8089" s="T1">once</ta>
            <ta e="T3" id="Seg_8090" s="T2">remember-PRS-1SG</ta>
            <ta e="T4" id="Seg_8091" s="T3">small.[NOM]</ta>
            <ta e="T5" id="Seg_8092" s="T4">be-TEMP-1SG</ta>
            <ta e="T6" id="Seg_8093" s="T5">Nahong-DAT/LOC</ta>
            <ta e="T7" id="Seg_8094" s="T6">hunting.spot-1PL.[NOM]</ta>
            <ta e="T8" id="Seg_8095" s="T7">Kresty-ABL</ta>
            <ta e="T9" id="Seg_8096" s="T8">ten</ta>
            <ta e="T10" id="Seg_8097" s="T9">two</ta>
            <ta e="T11" id="Seg_8098" s="T10">kilometer.[NOM]</ta>
            <ta e="T12" id="Seg_8099" s="T11">there</ta>
            <ta e="T13" id="Seg_8100" s="T12">how.much-PROPR</ta>
            <ta e="T14" id="Seg_8101" s="T13">human.being.[NOM]</ta>
            <ta e="T15" id="Seg_8102" s="T14">live-PST2-3SG=Q</ta>
            <ta e="T17" id="Seg_8103" s="T16">old-PL.[NOM]</ta>
            <ta e="T18" id="Seg_8104" s="T17">every-3PL.[NOM]</ta>
            <ta e="T19" id="Seg_8105" s="T18">there</ta>
            <ta e="T20" id="Seg_8106" s="T19">live-PTCP.HAB</ta>
            <ta e="T21" id="Seg_8107" s="T20">be-PST1-3PL</ta>
            <ta e="T23" id="Seg_8108" s="T22">sledge.[NOM]</ta>
            <ta e="T24" id="Seg_8109" s="T23">house.[NOM]</ta>
            <ta e="T25" id="Seg_8110" s="T24">full.[NOM]</ta>
            <ta e="T26" id="Seg_8111" s="T25">be-PST1-3SG</ta>
            <ta e="T27" id="Seg_8112" s="T26">there</ta>
            <ta e="T29" id="Seg_8113" s="T28">reindeer-PL.[NOM]</ta>
            <ta e="T30" id="Seg_8114" s="T29">what-PL.[NOM]</ta>
            <ta e="T31" id="Seg_8115" s="T30">there.is</ta>
            <ta e="T32" id="Seg_8116" s="T31">be-PST1-3PL</ta>
            <ta e="T34" id="Seg_8117" s="T33">that-ABL</ta>
            <ta e="T35" id="Seg_8118" s="T34">once</ta>
            <ta e="T36" id="Seg_8119" s="T35">father-1SG-PROPR.[NOM]</ta>
            <ta e="T37" id="Seg_8120" s="T36">house-3PL-DAT/LOC</ta>
            <ta e="T38" id="Seg_8121" s="T37">father-1SG.[NOM]</ta>
            <ta e="T39" id="Seg_8122" s="T38">old</ta>
            <ta e="T40" id="Seg_8123" s="T39">grandfather-1SG-ACC</ta>
            <ta e="T41" id="Seg_8124" s="T40">say-HAB-1SG</ta>
            <ta e="T42" id="Seg_8125" s="T41">EMPH</ta>
            <ta e="T43" id="Seg_8126" s="T42">father</ta>
            <ta e="T44" id="Seg_8127" s="T43">say-HAB-1SG</ta>
            <ta e="T45" id="Seg_8128" s="T44">old</ta>
            <ta e="T46" id="Seg_8129" s="T45">mum-1SG-ACC</ta>
            <ta e="T47" id="Seg_8130" s="T46">mum.[NOM]</ta>
            <ta e="T48" id="Seg_8131" s="T47">say-HAB-1SG</ta>
            <ta e="T49" id="Seg_8132" s="T48">old</ta>
            <ta e="T50" id="Seg_8133" s="T49">grandmother-1SG-ACC</ta>
            <ta e="T51" id="Seg_8134" s="T50">3PL-DAT/LOC</ta>
            <ta e="T52" id="Seg_8135" s="T51">feed-EP-PASS/REFL-EP-PST2-EP-1SG</ta>
            <ta e="T54" id="Seg_8136" s="T53">well</ta>
            <ta e="T55" id="Seg_8137" s="T54">that</ta>
            <ta e="T56" id="Seg_8138" s="T55">father-1PL.[NOM]</ta>
            <ta e="T57" id="Seg_8139" s="T56">whereto</ta>
            <ta e="T58" id="Seg_8140" s="T57">INDEF</ta>
            <ta e="T59" id="Seg_8141" s="T58">go-CVB.SEQ</ta>
            <ta e="T60" id="Seg_8142" s="T59">stay-PST1-3SG</ta>
            <ta e="T62" id="Seg_8143" s="T61">wood-VBZ-CVB.SIM</ta>
            <ta e="T63" id="Seg_8144" s="T62">go-PST1-3SG</ta>
            <ta e="T64" id="Seg_8145" s="T63">apparently</ta>
            <ta e="T65" id="Seg_8146" s="T64">well</ta>
            <ta e="T66" id="Seg_8147" s="T65">sit-PRS-1PL</ta>
            <ta e="T67" id="Seg_8148" s="T66">reindeer-PL-1PL.[NOM]</ta>
            <ta e="T69" id="Seg_8149" s="T68">who-DAT/LOC</ta>
            <ta e="T70" id="Seg_8150" s="T69">house-1PL.[NOM]</ta>
            <ta e="T71" id="Seg_8151" s="T70">edge-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_8152" s="T71">go-PRS-3PL</ta>
            <ta e="T74" id="Seg_8153" s="T73">scratch.with.hoof-CVB.SIM</ta>
            <ta e="T75" id="Seg_8154" s="T74">go-PRS-3PL</ta>
            <ta e="T78" id="Seg_8155" s="T77">tamed.reindeer-PROPR.[NOM]</ta>
            <ta e="T79" id="Seg_8156" s="T78">be-PST1-1PL</ta>
            <ta e="T81" id="Seg_8157" s="T80">this</ta>
            <ta e="T82" id="Seg_8158" s="T81">hand-ABL</ta>
            <ta e="T83" id="Seg_8159" s="T82">eat-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_8160" s="T84">bread-EP-PHIL.[NOM]</ta>
            <ta e="T86" id="Seg_8161" s="T85">very</ta>
            <ta e="T88" id="Seg_8162" s="T87">window-DAT/LOC</ta>
            <ta e="T89" id="Seg_8163" s="T88">come-HAB.[3SG]</ta>
            <ta e="T90" id="Seg_8164" s="T89">bread.[NOM]</ta>
            <ta e="T91" id="Seg_8165" s="T90">search.for-CVB.SIM</ta>
            <ta e="T93" id="Seg_8166" s="T92">oh</ta>
            <ta e="T94" id="Seg_8167" s="T93">nose-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_8168" s="T94">be.close.and.big-CVB.SEQ</ta>
            <ta e="T96" id="Seg_8169" s="T95">come-FUT.[3SG]</ta>
            <ta e="T97" id="Seg_8170" s="T96">say-CVB.SEQ</ta>
            <ta e="T98" id="Seg_8171" s="T97">oh</ta>
            <ta e="T99" id="Seg_8172" s="T98">startle-PRS-1SG</ta>
            <ta e="T100" id="Seg_8173" s="T99">small.[NOM]</ta>
            <ta e="T101" id="Seg_8174" s="T100">human.being.[NOM]</ta>
            <ta e="T102" id="Seg_8175" s="T101">well</ta>
            <ta e="T103" id="Seg_8176" s="T102">be.afraid-PTCP.HAB</ta>
            <ta e="T104" id="Seg_8177" s="T103">be-PST1-1SG</ta>
            <ta e="T105" id="Seg_8178" s="T104">EMPH</ta>
            <ta e="T108" id="Seg_8179" s="T107">this</ta>
            <ta e="T109" id="Seg_8180" s="T108">human.being.[NOM]</ta>
            <ta e="T110" id="Seg_8181" s="T109">hide-CVB.SEQ</ta>
            <ta e="T111" id="Seg_8182" s="T110">open-PST1-1SG</ta>
            <ta e="T112" id="Seg_8183" s="T111">EMPH</ta>
            <ta e="T114" id="Seg_8184" s="T113">table-ABL</ta>
            <ta e="T115" id="Seg_8185" s="T114">this</ta>
            <ta e="T116" id="Seg_8186" s="T115">bread.[NOM]</ta>
            <ta e="T117" id="Seg_8187" s="T116">flat.bread.[NOM]</ta>
            <ta e="T118" id="Seg_8188" s="T117">very-PL</ta>
            <ta e="T120" id="Seg_8189" s="T119">oh</ta>
            <ta e="T121" id="Seg_8190" s="T120">that-3SG-1SG.[NOM]</ta>
            <ta e="T122" id="Seg_8191" s="T121">get.excited-PST2.[3SG]</ta>
            <ta e="T123" id="Seg_8192" s="T122">eat-CVB.SIM</ta>
            <ta e="T124" id="Seg_8193" s="T123">eat-EMOT-PRS.[3SG]</ta>
            <ta e="T125" id="Seg_8194" s="T124">say-CVB.SEQ</ta>
            <ta e="T126" id="Seg_8195" s="T125">oh.dear</ta>
            <ta e="T128" id="Seg_8196" s="T127">well</ta>
            <ta e="T129" id="Seg_8197" s="T128">that</ta>
            <ta e="T130" id="Seg_8198" s="T129">mum-1SG.[NOM]</ta>
            <ta e="T131" id="Seg_8199" s="T130">say-PRS.[3SG]</ta>
            <ta e="T132" id="Seg_8200" s="T131">old</ta>
            <ta e="T133" id="Seg_8201" s="T132">mum-1SG.[NOM]</ta>
            <ta e="T134" id="Seg_8202" s="T133">well</ta>
            <ta e="T136" id="Seg_8203" s="T135">oh</ta>
            <ta e="T137" id="Seg_8204" s="T136">teach-EP-NEG.[IMP.2SG]</ta>
            <ta e="T138" id="Seg_8205" s="T137">well</ta>
            <ta e="T139" id="Seg_8206" s="T138">2SG.[NOM]</ta>
            <ta e="T140" id="Seg_8207" s="T139">window-1PL-ACC</ta>
            <ta e="T141" id="Seg_8208" s="T140">break-FUT.[3SG]</ta>
            <ta e="T142" id="Seg_8209" s="T141">and.so.on-FUT.[3SG]</ta>
            <ta e="T144" id="Seg_8210" s="T143">go.[IMP.2SG]</ta>
            <ta e="T145" id="Seg_8211" s="T144">go.[IMP.2SG]</ta>
            <ta e="T146" id="Seg_8212" s="T145">go.[IMP.2SG]</ta>
            <ta e="T147" id="Seg_8213" s="T146">come-EP-NEG.[IMP.2SG]</ta>
            <ta e="T149" id="Seg_8214" s="T148">say-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_8215" s="T149">what.[NOM]</ta>
            <ta e="T151" id="Seg_8216" s="T150">NEG</ta>
            <ta e="T152" id="Seg_8217" s="T151">NEG.EX</ta>
            <ta e="T154" id="Seg_8218" s="T153">grass-2SG-ACC</ta>
            <ta e="T155" id="Seg_8219" s="T154">eat.[IMP.2SG]</ta>
            <ta e="T156" id="Seg_8220" s="T155">go.[IMP.2SG]</ta>
            <ta e="T157" id="Seg_8221" s="T156">say-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_8222" s="T157">AFFIRM</ta>
            <ta e="T159" id="Seg_8223" s="T158">close-CVB.SEQ</ta>
            <ta e="T160" id="Seg_8224" s="T159">throw-PST1-3SG</ta>
            <ta e="T162" id="Seg_8225" s="T161">that-ABL</ta>
            <ta e="T163" id="Seg_8226" s="T162">other</ta>
            <ta e="T164" id="Seg_8227" s="T163">window-EP-INSTR</ta>
            <ta e="T165" id="Seg_8228" s="T164">come-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_8229" s="T165">there</ta>
            <ta e="T168" id="Seg_8230" s="T167">be-HAB.[3SG]</ta>
            <ta e="T169" id="Seg_8231" s="T168">EMPH</ta>
            <ta e="T171" id="Seg_8232" s="T170">there</ta>
            <ta e="T172" id="Seg_8233" s="T171">ice.[NOM]</ta>
            <ta e="T173" id="Seg_8234" s="T172">what.[NOM]</ta>
            <ta e="T174" id="Seg_8235" s="T173">lay-EP-MED-HAB-1PL</ta>
            <ta e="T176" id="Seg_8236" s="T175">this</ta>
            <ta e="T177" id="Seg_8237" s="T176">well</ta>
            <ta e="T178" id="Seg_8238" s="T177">water-1PL.[NOM]</ta>
            <ta e="T179" id="Seg_8239" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_8240" s="T179">ice-1PL.[NOM]</ta>
            <ta e="T182" id="Seg_8241" s="T181">there</ta>
            <ta e="T183" id="Seg_8242" s="T182">climb-CVB.SIM</ta>
            <ta e="T184" id="Seg_8243" s="T183">climb-CVB.SIM</ta>
            <ta e="T185" id="Seg_8244" s="T184">window-1PL.[NOM]</ta>
            <ta e="T186" id="Seg_8245" s="T185">open-PASS/REFL-CVB.SEQ</ta>
            <ta e="T187" id="Seg_8246" s="T186">stand-PRS.[3SG]</ta>
            <ta e="T188" id="Seg_8247" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_8248" s="T188">front-1PL.[NOM]</ta>
            <ta e="T190" id="Seg_8249" s="T189">well</ta>
            <ta e="T192" id="Seg_8250" s="T191">there</ta>
            <ta e="T193" id="Seg_8251" s="T192">have.long.face-CVB.SIM</ta>
            <ta e="T194" id="Seg_8252" s="T193">have.long.face-CVB.SIM</ta>
            <ta e="T195" id="Seg_8253" s="T194">horn-PL-PROPR.[NOM]</ta>
            <ta e="T196" id="Seg_8254" s="T195">EMPH</ta>
            <ta e="T198" id="Seg_8255" s="T197">horn-PL-3SG.[NOM]</ta>
            <ta e="T199" id="Seg_8256" s="T198">fit-NEG-3PL</ta>
            <ta e="T200" id="Seg_8257" s="T199">who.[NOM]</ta>
            <ta e="T201" id="Seg_8258" s="T200">place.for.crockery-DAT/LOC</ta>
            <ta e="T202" id="Seg_8259" s="T201">flat.bread.[NOM]</ta>
            <ta e="T203" id="Seg_8260" s="T202">stand-PTCP.PRS</ta>
            <ta e="T204" id="Seg_8261" s="T203">be-PST1-3SG</ta>
            <ta e="T207" id="Seg_8262" s="T206">who-VBZ-PTCP.PST</ta>
            <ta e="T208" id="Seg_8263" s="T207">cut-EP-PASS/REFL-EP-PTCP.PST-PL.[NOM]</ta>
            <ta e="T210" id="Seg_8264" s="T209">well</ta>
            <ta e="T211" id="Seg_8265" s="T210">that-ACC</ta>
            <ta e="T212" id="Seg_8266" s="T211">reach-CVB.SIM</ta>
            <ta e="T213" id="Seg_8267" s="T212">try-PST1-3SG</ta>
            <ta e="T214" id="Seg_8268" s="T213">EMPH</ta>
            <ta e="T215" id="Seg_8269" s="T214">tongue-3SG-ACC</ta>
            <ta e="T216" id="Seg_8270" s="T215">stretch.out-CVB.SIM</ta>
            <ta e="T217" id="Seg_8271" s="T216">stretch.out-CVB.SIM</ta>
            <ta e="T219" id="Seg_8272" s="T218">mum.[NOM]</ta>
            <ta e="T220" id="Seg_8273" s="T219">see.[IMP.2SG]</ta>
            <ta e="T221" id="Seg_8274" s="T220">see.[IMP.2SG]</ta>
            <ta e="T222" id="Seg_8275" s="T221">see.[IMP.2SG]</ta>
            <ta e="T223" id="Seg_8276" s="T222">say-PRS-1SG</ta>
            <ta e="T224" id="Seg_8277" s="T223">who-2SG-ACC</ta>
            <ta e="T225" id="Seg_8278" s="T224">eat-CVB.PURP</ta>
            <ta e="T226" id="Seg_8279" s="T225">make-PST1-3SG</ta>
            <ta e="T227" id="Seg_8280" s="T226">bread-PL-2SG-ACC</ta>
            <ta e="T228" id="Seg_8281" s="T227">eat-CVB.PURP</ta>
            <ta e="T229" id="Seg_8282" s="T228">make-PST1-3SG</ta>
            <ta e="T230" id="Seg_8283" s="T229">say-PRS-1SG</ta>
            <ta e="T231" id="Seg_8284" s="T230">bread-PL-2SG-ACC</ta>
            <ta e="T233" id="Seg_8285" s="T232">EXCL</ta>
            <ta e="T234" id="Seg_8286" s="T233">why</ta>
            <ta e="T235" id="Seg_8287" s="T234">satan</ta>
            <ta e="T236" id="Seg_8288" s="T235">daughter-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_8289" s="T236">why</ta>
            <ta e="T238" id="Seg_8290" s="T237">teach-PST2-2SG=Q</ta>
            <ta e="T239" id="Seg_8291" s="T238">EMPH</ta>
            <ta e="T240" id="Seg_8292" s="T239">house-ABL</ta>
            <ta e="T241" id="Seg_8293" s="T240">eat-CAUS-CVB.SIM</ta>
            <ta e="T242" id="Seg_8294" s="T241">window-ABL</ta>
            <ta e="T243" id="Seg_8295" s="T242">eat-CAUS-CVB.SIM</ta>
            <ta e="T245" id="Seg_8296" s="T244">see.[IMP.2SG]</ta>
            <ta e="T246" id="Seg_8297" s="T245">now</ta>
            <ta e="T247" id="Seg_8298" s="T246">every-3SG-ACC</ta>
            <ta e="T248" id="Seg_8299" s="T247">steal-FUT-3SG</ta>
            <ta e="T249" id="Seg_8300" s="T248">say-PRS.[3SG]</ta>
            <ta e="T251" id="Seg_8301" s="T250">oh.dear</ta>
            <ta e="T252" id="Seg_8302" s="T251">belly.[NOM]</ta>
            <ta e="T253" id="Seg_8303" s="T252">only-1SG</ta>
            <ta e="T256" id="Seg_8304" s="T255">go.[IMP.2SG]</ta>
            <ta e="T257" id="Seg_8305" s="T256">go.[IMP.2SG]</ta>
            <ta e="T259" id="Seg_8306" s="T258">what.[NOM]</ta>
            <ta e="T260" id="Seg_8307" s="T259">INDEF</ta>
            <ta e="T261" id="Seg_8308" s="T260">cloth-INSTR</ta>
            <ta e="T265" id="Seg_8309" s="T264">that-ABL</ta>
            <ta e="T266" id="Seg_8310" s="T265">go-NEG.[3SG]</ta>
            <ta e="T268" id="Seg_8311" s="T267">well</ta>
            <ta e="T271" id="Seg_8312" s="T270">go.[IMP.2SG]</ta>
            <ta e="T273" id="Seg_8313" s="T272">give-CVB.SEQ</ta>
            <ta e="T274" id="Seg_8314" s="T273">throw-PRS.[3SG]</ta>
            <ta e="T275" id="Seg_8315" s="T274">EMPH</ta>
            <ta e="T277" id="Seg_8316" s="T276">that-3SG-2SG.[NOM]</ta>
            <ta e="T278" id="Seg_8317" s="T277">chew.with.closed.mouth-CVB.SIM</ta>
            <ta e="T279" id="Seg_8318" s="T278">chew.with.closed.mouth-CVB.SIM</ta>
            <ta e="T281" id="Seg_8319" s="T280">go-FREQ-PRS.[3SG]</ta>
            <ta e="T282" id="Seg_8320" s="T281">say-CVB.SEQ</ta>
            <ta e="T283" id="Seg_8321" s="T282">luckily</ta>
            <ta e="T284" id="Seg_8322" s="T283">bread-ACC</ta>
            <ta e="T285" id="Seg_8323" s="T284">luckily</ta>
            <ta e="T286" id="Seg_8324" s="T285">eat.ones.fill-PST1-3SG</ta>
            <ta e="T287" id="Seg_8325" s="T286">apparently</ta>
            <ta e="T288" id="Seg_8326" s="T287">what.[NOM]</ta>
            <ta e="T289" id="Seg_8327" s="T288">so.much</ta>
            <ta e="T290" id="Seg_8328" s="T289">eat.ones.fill-FUT.[3SG]=Q</ta>
            <ta e="T291" id="Seg_8329" s="T290">well</ta>
            <ta e="T292" id="Seg_8330" s="T291">that</ta>
            <ta e="T293" id="Seg_8331" s="T292">crunch-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T296" id="Seg_8332" s="T295">who-ACC</ta>
            <ta e="T297" id="Seg_8333" s="T296">give-PST2.[3SG]</ta>
            <ta e="T298" id="Seg_8334" s="T297">get.dry-PTCP.PST</ta>
            <ta e="T299" id="Seg_8335" s="T298">bread-ACC</ta>
            <ta e="T301" id="Seg_8336" s="T300">well</ta>
            <ta e="T302" id="Seg_8337" s="T301">there</ta>
            <ta e="T303" id="Seg_8338" s="T302">chew-CVB.SIM</ta>
            <ta e="T304" id="Seg_8339" s="T303">try-PST1-3SG</ta>
            <ta e="T305" id="Seg_8340" s="T304">EMPH</ta>
            <ta e="T307" id="Seg_8341" s="T306">oh.dear</ta>
            <ta e="T310" id="Seg_8342" s="T309">EXCL</ta>
            <ta e="T311" id="Seg_8343" s="T310">tooth-PL-3SG-ACC</ta>
            <ta e="T312" id="Seg_8344" s="T311">whole-3SG-ACC</ta>
            <ta e="T313" id="Seg_8345" s="T312">who-VBZ-PST1-3SG</ta>
            <ta e="T314" id="Seg_8346" s="T313">apparently</ta>
            <ta e="T316" id="Seg_8347" s="T315">well</ta>
            <ta e="T317" id="Seg_8348" s="T316">that</ta>
            <ta e="T318" id="Seg_8349" s="T317">go-PST2-3SG</ta>
            <ta e="T319" id="Seg_8350" s="T318">luckily</ta>
            <ta e="T320" id="Seg_8351" s="T319">friend-PL-3SG-ACC</ta>
            <ta e="T321" id="Seg_8352" s="T320">to</ta>
            <ta e="T323" id="Seg_8353" s="T322">high.shore.[NOM]</ta>
            <ta e="T324" id="Seg_8354" s="T323">lower.part-3SG-DAT/LOC</ta>
            <ta e="T325" id="Seg_8355" s="T324">go.down-PST2-3PL</ta>
            <ta e="T326" id="Seg_8356" s="T325">there</ta>
            <ta e="T327" id="Seg_8357" s="T326">river-DAT/LOC</ta>
            <ta e="T328" id="Seg_8358" s="T327">go-PRS-3PL</ta>
            <ta e="T330" id="Seg_8359" s="T329">yard-1PL.[NOM]</ta>
            <ta e="T331" id="Seg_8360" s="T330">get.dark-PST1-3SG</ta>
            <ta e="T332" id="Seg_8361" s="T331">get.dark-PRS.[3SG]</ta>
            <ta e="T333" id="Seg_8362" s="T332">get.dark-CVB.SEQ</ta>
            <ta e="T334" id="Seg_8363" s="T333">go-PRS.[3SG]</ta>
            <ta e="T336" id="Seg_8364" s="T335">who.[NOM]</ta>
            <ta e="T337" id="Seg_8365" s="T336">dog-PROPR.[NOM]</ta>
            <ta e="T338" id="Seg_8366" s="T337">be-PST1-1PL</ta>
            <ta e="T339" id="Seg_8367" s="T338">1PL.[NOM]</ta>
            <ta e="T340" id="Seg_8368" s="T339">Djongo</ta>
            <ta e="T341" id="Seg_8369" s="T340">white</ta>
            <ta e="T342" id="Seg_8370" s="T341">very</ta>
            <ta e="T343" id="Seg_8371" s="T342">beautiful</ta>
            <ta e="T344" id="Seg_8372" s="T343">very</ta>
            <ta e="T345" id="Seg_8373" s="T344">reindeer-AG.[NOM]</ta>
            <ta e="T346" id="Seg_8374" s="T345">very</ta>
            <ta e="T347" id="Seg_8375" s="T346">that-3SG-2SG.[NOM]</ta>
            <ta e="T349" id="Seg_8376" s="T348">who.[NOM]</ta>
            <ta e="T351" id="Seg_8377" s="T350">say-FUT-2SG</ta>
            <ta e="T352" id="Seg_8378" s="T351">Q</ta>
            <ta e="T353" id="Seg_8379" s="T352">who-ACC</ta>
            <ta e="T354" id="Seg_8380" s="T353">herd-ACC</ta>
            <ta e="T355" id="Seg_8381" s="T354">whole-3SG-ACC</ta>
            <ta e="T356" id="Seg_8382" s="T355">get-CVB.SEQ</ta>
            <ta e="T357" id="Seg_8383" s="T356">come-FUT.[3SG]</ta>
            <ta e="T359" id="Seg_8384" s="T358">oh</ta>
            <ta e="T360" id="Seg_8385" s="T359">turn.around-FREQ-FUT-3SG</ta>
            <ta e="T361" id="Seg_8386" s="T360">say-CVB.SEQ</ta>
            <ta e="T362" id="Seg_8387" s="T361">around</ta>
            <ta e="T363" id="Seg_8388" s="T362">one</ta>
            <ta e="T364" id="Seg_8389" s="T363">place-DAT/LOC</ta>
            <ta e="T365" id="Seg_8390" s="T364">stand-FUT-3SG</ta>
            <ta e="T367" id="Seg_8391" s="T366">this</ta>
            <ta e="T368" id="Seg_8392" s="T367">get.[IMP.2SG]</ta>
            <ta e="T369" id="Seg_8393" s="T368">say-PTCP.COND-DAT/LOC</ta>
            <ta e="T370" id="Seg_8394" s="T369">well</ta>
            <ta e="T372" id="Seg_8395" s="T371">well</ta>
            <ta e="T373" id="Seg_8396" s="T372">that-3SG-1PL.[NOM]</ta>
            <ta e="T374" id="Seg_8397" s="T373">who-ACC</ta>
            <ta e="T375" id="Seg_8398" s="T374">who-VBZ-PTCP.HAB</ta>
            <ta e="T376" id="Seg_8399" s="T375">be-PST1-3SG</ta>
            <ta e="T377" id="Seg_8400" s="T376">not.long.ago</ta>
            <ta e="T378" id="Seg_8401" s="T377">reindeer-PL-EP-2SG.[NOM]</ta>
            <ta e="T379" id="Seg_8402" s="T378">there.is</ta>
            <ta e="T380" id="Seg_8403" s="T379">be-TEMP-3PL</ta>
            <ta e="T381" id="Seg_8404" s="T380">well</ta>
            <ta e="T382" id="Seg_8405" s="T381">house-DAT/LOC</ta>
            <ta e="T384" id="Seg_8406" s="T383">reindeer.calf.[NOM]</ta>
            <ta e="T385" id="Seg_8407" s="T384">one.year.old.reindeer.[NOM]</ta>
            <ta e="T386" id="Seg_8408" s="T385">Q</ta>
            <ta e="T387" id="Seg_8409" s="T386">what.[NOM]</ta>
            <ta e="T388" id="Seg_8410" s="T387">Q</ta>
            <ta e="T389" id="Seg_8411" s="T388">one.year.old.reindeer</ta>
            <ta e="T390" id="Seg_8412" s="T389">reindeer.calf.[NOM]</ta>
            <ta e="T391" id="Seg_8413" s="T390">eh</ta>
            <ta e="T392" id="Seg_8414" s="T391">big</ta>
            <ta e="T393" id="Seg_8415" s="T392">calf-ABL</ta>
            <ta e="T394" id="Seg_8416" s="T393">big-INTNS</ta>
            <ta e="T395" id="Seg_8417" s="T394">year-3SG-INSTR</ta>
            <ta e="T396" id="Seg_8418" s="T395">just</ta>
            <ta e="T397" id="Seg_8419" s="T396">big</ta>
            <ta e="T399" id="Seg_8420" s="T398">that</ta>
            <ta e="T400" id="Seg_8421" s="T399">who-ACC</ta>
            <ta e="T401" id="Seg_8422" s="T400">well</ta>
            <ta e="T402" id="Seg_8423" s="T401">turn.around-CVB.SIM</ta>
            <ta e="T403" id="Seg_8424" s="T402">lie.down-FUT-3SG</ta>
            <ta e="T404" id="Seg_8425" s="T403">EMPH</ta>
            <ta e="T405" id="Seg_8426" s="T404">torture-CVB.SIM</ta>
            <ta e="T406" id="Seg_8427" s="T405">lie.down-HAB.[3SG]</ta>
            <ta e="T407" id="Seg_8428" s="T406">that.[NOM]</ta>
            <ta e="T408" id="Seg_8429" s="T407">what.[NOM]</ta>
            <ta e="T409" id="Seg_8430" s="T408">calf-DIM.[NOM]</ta>
            <ta e="T410" id="Seg_8431" s="T409">want-HAB.[3SG]</ta>
            <ta e="T411" id="Seg_8432" s="T410">MOD</ta>
            <ta e="T412" id="Seg_8433" s="T411">move-CVB.SIM</ta>
            <ta e="T413" id="Seg_8434" s="T412">move-CVB.SIM</ta>
            <ta e="T415" id="Seg_8435" s="T414">lie.down-FUT-3SG</ta>
            <ta e="T417" id="Seg_8436" s="T416">EMPH</ta>
            <ta e="T418" id="Seg_8437" s="T417">play-PRS.[3SG]</ta>
            <ta e="T419" id="Seg_8438" s="T418">EMPH</ta>
            <ta e="T420" id="Seg_8439" s="T419">3SG-ACC</ta>
            <ta e="T421" id="Seg_8440" s="T420">with</ta>
            <ta e="T422" id="Seg_8441" s="T421">well</ta>
            <ta e="T423" id="Seg_8442" s="T422">that-3SG-2SG.[NOM]</ta>
            <ta e="T424" id="Seg_8443" s="T423">how</ta>
            <ta e="T425" id="Seg_8444" s="T424">and</ta>
            <ta e="T426" id="Seg_8445" s="T425">be-PST1-3SG</ta>
            <ta e="T427" id="Seg_8446" s="T1150">strike-EP-RECP/COLL-EP-NMNZ.[NOM]</ta>
            <ta e="T428" id="Seg_8447" s="T427">strike-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T429" id="Seg_8448" s="T428">this</ta>
            <ta e="T430" id="Seg_8449" s="T429">like</ta>
            <ta e="T431" id="Seg_8450" s="T430">how</ta>
            <ta e="T432" id="Seg_8451" s="T431">make=Q</ta>
            <ta e="T433" id="Seg_8452" s="T432">mum.[NOM]</ta>
            <ta e="T434" id="Seg_8453" s="T433">mum.[NOM]</ta>
            <ta e="T435" id="Seg_8454" s="T434">say-PRS-1SG</ta>
            <ta e="T436" id="Seg_8455" s="T435">see.[IMP.2SG]</ta>
            <ta e="T437" id="Seg_8456" s="T436">see.[IMP.2SG]</ta>
            <ta e="T438" id="Seg_8457" s="T437">Djongo-ACC</ta>
            <ta e="T440" id="Seg_8458" s="T439">Choko</ta>
            <ta e="T441" id="Seg_8459" s="T440">Choko</ta>
            <ta e="T442" id="Seg_8460" s="T441">say-HAB-1SG</ta>
            <ta e="T444" id="Seg_8461" s="T443">Choko.[NOM]</ta>
            <ta e="T445" id="Seg_8462" s="T444">see.[IMP.2SG]</ta>
            <ta e="T447" id="Seg_8463" s="T446">who-ACC</ta>
            <ta e="T448" id="Seg_8464" s="T447">eat-CVB.PURP</ta>
            <ta e="T449" id="Seg_8465" s="T448">want-PST1-3SG</ta>
            <ta e="T450" id="Seg_8466" s="T449">say-PRS-1SG</ta>
            <ta e="T451" id="Seg_8467" s="T450">reindeer.calf-ACC</ta>
            <ta e="T452" id="Seg_8468" s="T451">eat-CVB.PURP</ta>
            <ta e="T453" id="Seg_8469" s="T452">make-PST1-3SG</ta>
            <ta e="T455" id="Seg_8470" s="T454">oh</ta>
            <ta e="T456" id="Seg_8471" s="T455">satan</ta>
            <ta e="T457" id="Seg_8472" s="T456">call.[IMP.2SG]</ta>
            <ta e="T458" id="Seg_8473" s="T457">call.[IMP.2SG]</ta>
            <ta e="T460" id="Seg_8474" s="T459">get.tired-CAUS-PST1-3SG</ta>
            <ta e="T461" id="Seg_8475" s="T460">apparently</ta>
            <ta e="T462" id="Seg_8476" s="T461">that.[NOM]</ta>
            <ta e="T463" id="Seg_8477" s="T462">reindeer.calf-ACC</ta>
            <ta e="T464" id="Seg_8478" s="T463">scare-PST1-3SG</ta>
            <ta e="T466" id="Seg_8479" s="T465">Choko</ta>
            <ta e="T467" id="Seg_8480" s="T466">Choko</ta>
            <ta e="T468" id="Seg_8481" s="T467">Choko</ta>
            <ta e="T469" id="Seg_8482" s="T468">say-PRS-1SG</ta>
            <ta e="T470" id="Seg_8483" s="T469">out</ta>
            <ta e="T471" id="Seg_8484" s="T470">go.out-CVB.SEQ</ta>
            <ta e="T472" id="Seg_8485" s="T471">go-CVB.SEQ</ta>
            <ta e="T473" id="Seg_8486" s="T472">well</ta>
            <ta e="T475" id="Seg_8487" s="T474">oh</ta>
            <ta e="T476" id="Seg_8488" s="T475">well</ta>
            <ta e="T477" id="Seg_8489" s="T476">that-3SG-2SG.[NOM]</ta>
            <ta e="T478" id="Seg_8490" s="T477">run-CVB.SEQ</ta>
            <ta e="T479" id="Seg_8491" s="T478">go-EMOT-PRS.[3SG]</ta>
            <ta e="T480" id="Seg_8492" s="T479">say-CVB.SEQ</ta>
            <ta e="T482" id="Seg_8493" s="T481">well</ta>
            <ta e="T483" id="Seg_8494" s="T482">jump</ta>
            <ta e="T484" id="Seg_8495" s="T483">well</ta>
            <ta e="T485" id="Seg_8496" s="T484">face-1SG-DAT/LOC</ta>
            <ta e="T486" id="Seg_8497" s="T485">1SG.[NOM]</ta>
            <ta e="T487" id="Seg_8498" s="T486">lie.on.the.back-NMNZ.[NOM]</ta>
            <ta e="T490" id="Seg_8499" s="T489">on.the.back</ta>
            <ta e="T491" id="Seg_8500" s="T490">lie.on.the.back-CVB.SIM</ta>
            <ta e="T492" id="Seg_8501" s="T491">lie-EMOT-PRS-1SG</ta>
            <ta e="T493" id="Seg_8502" s="T492">say-CVB.SEQ</ta>
            <ta e="T495" id="Seg_8503" s="T494">AFFIRM</ta>
            <ta e="T496" id="Seg_8504" s="T495">say-PRS-1SG</ta>
            <ta e="T497" id="Seg_8505" s="T496">mum.[NOM]</ta>
            <ta e="T498" id="Seg_8506" s="T497">take.[IMP.2SG]</ta>
            <ta e="T499" id="Seg_8507" s="T498">well</ta>
            <ta e="T500" id="Seg_8508" s="T499">EMPH</ta>
            <ta e="T502" id="Seg_8509" s="T501">lick-PRS.[3SG]</ta>
            <ta e="T503" id="Seg_8510" s="T502">and.so.on-PRS.[3SG]</ta>
            <ta e="T504" id="Seg_8511" s="T503">move</ta>
            <ta e="T505" id="Seg_8512" s="T504">move-CAUS-PRS.[3SG]</ta>
            <ta e="T506" id="Seg_8513" s="T505">EMPH</ta>
            <ta e="T507" id="Seg_8514" s="T506">human.being-ACC</ta>
            <ta e="T508" id="Seg_8515" s="T507">well</ta>
            <ta e="T510" id="Seg_8516" s="T509">move-PTCP.FUT-3SG-ACC</ta>
            <ta e="T511" id="Seg_8517" s="T510">want-PRS.[3SG]</ta>
            <ta e="T513" id="Seg_8518" s="T512">1SG-ACC</ta>
            <ta e="T514" id="Seg_8519" s="T513">drag-CVB.SIM</ta>
            <ta e="T515" id="Seg_8520" s="T514">lie.down-FREQ-PRS.[3SG]</ta>
            <ta e="T516" id="Seg_8521" s="T515">say-CVB.SEQ</ta>
            <ta e="T518" id="Seg_8522" s="T517">well</ta>
            <ta e="T519" id="Seg_8523" s="T518">that</ta>
            <ta e="T520" id="Seg_8524" s="T519">mum-1SG.[NOM]</ta>
            <ta e="T521" id="Seg_8525" s="T520">go.out-PST1-3SG</ta>
            <ta e="T523" id="Seg_8526" s="T522">well</ta>
            <ta e="T524" id="Seg_8527" s="T523">what.[NOM]</ta>
            <ta e="T525" id="Seg_8528" s="T524">become-PRS-2SG</ta>
            <ta e="T527" id="Seg_8529" s="T526">see.[IMP.2SG]</ta>
            <ta e="T528" id="Seg_8530" s="T527">say-PRS-1SG</ta>
            <ta e="T529" id="Seg_8531" s="T528">1SG-ACC</ta>
            <ta e="T530" id="Seg_8532" s="T529">thrash-PRS.[3SG]</ta>
            <ta e="T531" id="Seg_8533" s="T530">Q</ta>
            <ta e="T532" id="Seg_8534" s="T531">say-PRS-1SG</ta>
            <ta e="T533" id="Seg_8535" s="T532">play-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_8536" s="T533">human.being-ACC</ta>
            <ta e="T535" id="Seg_8537" s="T534">well</ta>
            <ta e="T536" id="Seg_8538" s="T535">oh.dear</ta>
            <ta e="T537" id="Seg_8539" s="T536">and</ta>
            <ta e="T539" id="Seg_8540" s="T538">%%-CVB.SEQ</ta>
            <ta e="T540" id="Seg_8541" s="T539">cry-CVB.SEQ</ta>
            <ta e="T541" id="Seg_8542" s="T540">cry.loudly-PRS-1SG</ta>
            <ta e="T542" id="Seg_8543" s="T541">AFFIRM</ta>
            <ta e="T544" id="Seg_8544" s="T543">cry.[IMP.2SG]</ta>
            <ta e="T545" id="Seg_8545" s="T544">cry-NEG.[IMP.2SG]</ta>
            <ta e="T547" id="Seg_8546" s="T546">self-2SG.[NOM]</ta>
            <ta e="T548" id="Seg_8547" s="T547">call-PST2-EP-2SG</ta>
            <ta e="T549" id="Seg_8548" s="T548">say-PRS.[3SG]</ta>
            <ta e="T550" id="Seg_8549" s="T549">human.being.[NOM]</ta>
            <ta e="T551" id="Seg_8550" s="T550">laugh-PTCP.FUT.[NOM]</ta>
            <ta e="T553" id="Seg_8551" s="T552">that-3SG-2SG.[NOM]</ta>
            <ta e="T554" id="Seg_8552" s="T553">again</ta>
            <ta e="T555" id="Seg_8553" s="T554">go-CVB.SEQ</ta>
            <ta e="T556" id="Seg_8554" s="T555">stay-PST1-3SG</ta>
            <ta e="T557" id="Seg_8555" s="T556">that</ta>
            <ta e="T558" id="Seg_8556" s="T557">calf-DAT/LOC</ta>
            <ta e="T560" id="Seg_8557" s="T559">well</ta>
            <ta e="T562" id="Seg_8558" s="T561">who-VBZ-PST2.NEG-EP-1SG</ta>
            <ta e="T563" id="Seg_8559" s="T562">that</ta>
            <ta e="T564" id="Seg_8560" s="T563">always</ta>
            <ta e="T565" id="Seg_8561" s="T564">day.[NOM]</ta>
            <ta e="T566" id="Seg_8562" s="T565">every</ta>
            <ta e="T567" id="Seg_8563" s="T566">play-HAB.[3SG]</ta>
            <ta e="T568" id="Seg_8564" s="T567">that</ta>
            <ta e="T569" id="Seg_8565" s="T568">reindeer.calf-ACC</ta>
            <ta e="T570" id="Seg_8566" s="T569">with</ta>
            <ta e="T571" id="Seg_8567" s="T570">what-DAT/LOC</ta>
            <ta e="T572" id="Seg_8568" s="T571">want-PST2.[3SG]</ta>
            <ta e="T574" id="Seg_8569" s="T573">that.[NOM]</ta>
            <ta e="T575" id="Seg_8570" s="T574">reindeer.calf.[NOM]</ta>
            <ta e="T576" id="Seg_8571" s="T575">become-PTCP.FUT-3SG-ACC</ta>
            <ta e="T577" id="Seg_8572" s="T576">need.to</ta>
            <ta e="T579" id="Seg_8573" s="T578">well</ta>
            <ta e="T580" id="Seg_8574" s="T579">that-ABL</ta>
            <ta e="T581" id="Seg_8575" s="T580">yard-1PL.[NOM]</ta>
            <ta e="T582" id="Seg_8576" s="T581">get.dark-PST1-3SG</ta>
            <ta e="T583" id="Seg_8577" s="T582">EMPH</ta>
            <ta e="T585" id="Seg_8578" s="T584">beautiful-ADVZ</ta>
            <ta e="T586" id="Seg_8579" s="T585">and</ta>
            <ta e="T587" id="Seg_8580" s="T586">who-1PL.[NOM]</ta>
            <ta e="T588" id="Seg_8581" s="T587">stove-1PL.[NOM]</ta>
            <ta e="T589" id="Seg_8582" s="T588">well</ta>
            <ta e="T591" id="Seg_8583" s="T590">mum-1SG.[NOM]</ta>
            <ta e="T592" id="Seg_8584" s="T591">enough</ta>
            <ta e="T593" id="Seg_8585" s="T592">lay-PST2.[3SG]</ta>
            <ta e="T598" id="Seg_8586" s="T597">who.[NOM]</ta>
            <ta e="T599" id="Seg_8587" s="T598">wood.[NOM]</ta>
            <ta e="T600" id="Seg_8588" s="T599">well</ta>
            <ta e="T601" id="Seg_8589" s="T600">flame.up-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T602" id="Seg_8590" s="T601">well</ta>
            <ta e="T603" id="Seg_8591" s="T602">get.red-INCH</ta>
            <ta e="T604" id="Seg_8592" s="T603">stove-1PL.[NOM]</ta>
            <ta e="T606" id="Seg_8593" s="T605">teapot-PL.[NOM]</ta>
            <ta e="T607" id="Seg_8594" s="T606">rise-EP-REFL-CVB.SEQ</ta>
            <ta e="T609" id="Seg_8595" s="T608">dither-EMOT-PRS-3PL</ta>
            <ta e="T610" id="Seg_8596" s="T609">say-CVB.SEQ</ta>
            <ta e="T611" id="Seg_8597" s="T610">lid-3PL.[NOM]</ta>
            <ta e="T613" id="Seg_8598" s="T612">meal-1PL.[NOM]</ta>
            <ta e="T614" id="Seg_8599" s="T613">%%</ta>
            <ta e="T615" id="Seg_8600" s="T614">stand.up-FREQ-PRS.[3SG]</ta>
            <ta e="T616" id="Seg_8601" s="T615">say-CVB.SEQ</ta>
            <ta e="T617" id="Seg_8602" s="T616">%%</ta>
            <ta e="T620" id="Seg_8603" s="T619">become.warm</ta>
            <ta e="T621" id="Seg_8604" s="T620">door.[NOM]</ta>
            <ta e="T622" id="Seg_8605" s="T621">window-ACC</ta>
            <ta e="T623" id="Seg_8606" s="T622">open-PST1-3SG</ta>
            <ta e="T624" id="Seg_8607" s="T623">chimney-3SG.[NOM]</ta>
            <ta e="T625" id="Seg_8608" s="T624">open-PST1-3SG</ta>
            <ta e="T627" id="Seg_8609" s="T626">chimney</ta>
            <ta e="T628" id="Seg_8610" s="T627">be-HAB.[3SG]</ta>
            <ta e="T629" id="Seg_8611" s="T628">who.[NOM]</ta>
            <ta e="T630" id="Seg_8612" s="T629">cover-HAB-3PL</ta>
            <ta e="T634" id="Seg_8613" s="T633">wind.[NOM]</ta>
            <ta e="T635" id="Seg_8614" s="T634">go.in-PTCP.FUT-3SG-ACC</ta>
            <ta e="T636" id="Seg_8615" s="T635">well</ta>
            <ta e="T638" id="Seg_8616" s="T637">well</ta>
            <ta e="T639" id="Seg_8617" s="T638">that</ta>
            <ta e="T640" id="Seg_8618" s="T639">open-PST1-3SG</ta>
            <ta e="T641" id="Seg_8619" s="T640">AFFIRM</ta>
            <ta e="T643" id="Seg_8620" s="T642">1SG.[NOM]</ta>
            <ta e="T644" id="Seg_8621" s="T643">Q</ta>
            <ta e="T645" id="Seg_8622" s="T644">very</ta>
            <ta e="T646" id="Seg_8623" s="T645">love-HAB-1SG</ta>
            <ta e="T647" id="Seg_8624" s="T646">window-DAT/LOC</ta>
            <ta e="T648" id="Seg_8625" s="T647">window-DAT/LOC</ta>
            <ta e="T649" id="Seg_8626" s="T648">see-PTCP.FUT-1SG-ACC</ta>
            <ta e="T650" id="Seg_8627" s="T649">well</ta>
            <ta e="T652" id="Seg_8628" s="T651">have.broad.smiley.face-CVB.SIM</ta>
            <ta e="T653" id="Seg_8629" s="T652">have.broad.smiley.face-CVB.SIM</ta>
            <ta e="T654" id="Seg_8630" s="T653">sit-HAB-1SG</ta>
            <ta e="T656" id="Seg_8631" s="T655">beautiful-ADVZ</ta>
            <ta e="T657" id="Seg_8632" s="T656">EMPH</ta>
            <ta e="T658" id="Seg_8633" s="T657">AFFIRM</ta>
            <ta e="T659" id="Seg_8634" s="T658">who.[NOM]</ta>
            <ta e="T660" id="Seg_8635" s="T659">month.[NOM]</ta>
            <ta e="T663" id="Seg_8636" s="T662">month.[NOM]</ta>
            <ta e="T664" id="Seg_8637" s="T663">pain-DIM.[NOM]</ta>
            <ta e="T665" id="Seg_8638" s="T664">be-PST2-3SG</ta>
            <ta e="T666" id="Seg_8639" s="T665">eh</ta>
            <ta e="T668" id="Seg_8640" s="T667">moon-1PL.[NOM]</ta>
            <ta e="T669" id="Seg_8641" s="T668">beautiful.[NOM]</ta>
            <ta e="T670" id="Seg_8642" s="T669">very</ta>
            <ta e="T671" id="Seg_8643" s="T670">but</ta>
            <ta e="T672" id="Seg_8644" s="T671">EMPH-dark.[NOM]</ta>
            <ta e="T674" id="Seg_8645" s="T673">what-PL.[NOM]</ta>
            <ta e="T675" id="Seg_8646" s="T674">fly-HAB-3PL</ta>
            <ta e="T676" id="Seg_8647" s="T675">be-PST2.[3SG]</ta>
            <ta e="T677" id="Seg_8648" s="T676">1SG.[NOM]</ta>
            <ta e="T679" id="Seg_8649" s="T678">know-NEG.PTCP</ta>
            <ta e="T680" id="Seg_8650" s="T679">be-PST1-1SG</ta>
            <ta e="T682" id="Seg_8651" s="T681">snow.owl.[NOM]</ta>
            <ta e="T683" id="Seg_8652" s="T682">similar</ta>
            <ta e="T684" id="Seg_8653" s="T683">snow.owl-PL.[NOM]</ta>
            <ta e="T685" id="Seg_8654" s="T684">say-CVB.SEQ</ta>
            <ta e="T686" id="Seg_8655" s="T685">NEG-3SG</ta>
            <ta e="T687" id="Seg_8656" s="T686">small-3PL</ta>
            <ta e="T689" id="Seg_8657" s="T688">head-POSS</ta>
            <ta e="T690" id="Seg_8658" s="T689">NEG-3PL</ta>
            <ta e="T691" id="Seg_8659" s="T690">say-HAB-1SG</ta>
            <ta e="T692" id="Seg_8660" s="T691">head-POSS</ta>
            <ta e="T693" id="Seg_8661" s="T692">NEG-3PL</ta>
            <ta e="T694" id="Seg_8662" s="T693">go-PRS-3PL</ta>
            <ta e="T695" id="Seg_8663" s="T694">mum.[NOM]</ta>
            <ta e="T696" id="Seg_8664" s="T695">see.[IMP.2SG]</ta>
            <ta e="T699" id="Seg_8665" s="T698">that.[NOM]</ta>
            <ta e="T700" id="Seg_8666" s="T699">who.[NOM]</ta>
            <ta e="T701" id="Seg_8667" s="T700">eh</ta>
            <ta e="T702" id="Seg_8668" s="T701">this</ta>
            <ta e="T703" id="Seg_8669" s="T702">coal.[NOM]</ta>
            <ta e="T704" id="Seg_8670" s="T703">fly-PRS.[3SG]</ta>
            <ta e="T705" id="Seg_8671" s="T704">EMPH</ta>
            <ta e="T706" id="Seg_8672" s="T705">pipe-ABL</ta>
            <ta e="T707" id="Seg_8673" s="T706">well</ta>
            <ta e="T709" id="Seg_8674" s="T708">that-DAT/LOC</ta>
            <ta e="T710" id="Seg_8675" s="T709">move-RECP/COLL-PRS-3PL</ta>
            <ta e="T711" id="Seg_8676" s="T710">be-PST2.[3SG]</ta>
            <ta e="T712" id="Seg_8677" s="T711">that-PL-EP-2SG.[NOM]</ta>
            <ta e="T714" id="Seg_8678" s="T713">very</ta>
            <ta e="T716" id="Seg_8679" s="T715">that-ABL</ta>
            <ta e="T717" id="Seg_8680" s="T716">how.much</ta>
            <ta e="T718" id="Seg_8681" s="T717">INDEF</ta>
            <ta e="T719" id="Seg_8682" s="T718">mum-1SG.[NOM]</ta>
            <ta e="T720" id="Seg_8683" s="T719">sew</ta>
            <ta e="T721" id="Seg_8684" s="T720">sit-PRS.[3SG]</ta>
            <ta e="T722" id="Seg_8685" s="T721">sew</ta>
            <ta e="T723" id="Seg_8686" s="T722">sit-PRS.[3SG]</ta>
            <ta e="T725" id="Seg_8687" s="T724">silently</ta>
            <ta e="T726" id="Seg_8688" s="T725">silently</ta>
            <ta e="T727" id="Seg_8689" s="T726">EMPH</ta>
            <ta e="T728" id="Seg_8690" s="T727">EMPH</ta>
            <ta e="T729" id="Seg_8691" s="T728">sing-CVB.SEQ</ta>
            <ta e="T730" id="Seg_8692" s="T729">sit-PRS.[3SG]</ta>
            <ta e="T731" id="Seg_8693" s="T730">silently</ta>
            <ta e="T733" id="Seg_8694" s="T732">Djongo-1PL.[NOM]</ta>
            <ta e="T734" id="Seg_8695" s="T733">lie-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_8696" s="T735">n'ik</ta>
            <ta e="T737" id="Seg_8697" s="T736">NEG</ta>
            <ta e="T738" id="Seg_8698" s="T737">make-NEG.[3SG]</ta>
            <ta e="T740" id="Seg_8699" s="T739">get.tired-PST2.[3SG]</ta>
            <ta e="T741" id="Seg_8700" s="T740">apparently</ta>
            <ta e="T742" id="Seg_8701" s="T741">reindeer.calf-3SG-ACC</ta>
            <ta e="T743" id="Seg_8702" s="T742">with</ta>
            <ta e="T745" id="Seg_8703" s="T744">that-ABL</ta>
            <ta e="T746" id="Seg_8704" s="T745">who.[NOM]</ta>
            <ta e="T749" id="Seg_8705" s="T748">go-PST1-3SG</ta>
            <ta e="T750" id="Seg_8706" s="T749">EMPH</ta>
            <ta e="T753" id="Seg_8707" s="T752">that</ta>
            <ta e="T754" id="Seg_8708" s="T753">what.[NOM]</ta>
            <ta e="T755" id="Seg_8709" s="T754">different</ta>
            <ta e="T756" id="Seg_8710" s="T755">various</ta>
            <ta e="T757" id="Seg_8711" s="T756">1SG.[NOM]</ta>
            <ta e="T759" id="Seg_8712" s="T758">father-1SG.[NOM]</ta>
            <ta e="T760" id="Seg_8713" s="T759">no-INTNS-3SG-INSTR</ta>
            <ta e="T761" id="Seg_8714" s="T760">radio-PART</ta>
            <ta e="T762" id="Seg_8715" s="T761">switch.on-VBZ-EP-MED-PRS-1SG</ta>
            <ta e="T764" id="Seg_8716" s="T763">AFFIRM</ta>
            <ta e="T765" id="Seg_8717" s="T764">%%-EMOT-PRS-1SG</ta>
            <ta e="T766" id="Seg_8718" s="T765">well</ta>
            <ta e="T768" id="Seg_8719" s="T767">AFFIRM</ta>
            <ta e="T769" id="Seg_8720" s="T768">silently</ta>
            <ta e="T770" id="Seg_8721" s="T769">make.[IMP.2SG]</ta>
            <ta e="T771" id="Seg_8722" s="T770">self</ta>
            <ta e="T772" id="Seg_8723" s="T771">%%-EP-NEG.[IMP.2SG]</ta>
            <ta e="T774" id="Seg_8724" s="T773">song-PL-ACC</ta>
            <ta e="T775" id="Seg_8725" s="T774">listen-CVB.SIM</ta>
            <ta e="T776" id="Seg_8726" s="T775">lie-FUT-1SG</ta>
            <ta e="T777" id="Seg_8727" s="T776">EMPH</ta>
            <ta e="T780" id="Seg_8728" s="T779">who-PL-ACC</ta>
            <ta e="T781" id="Seg_8729" s="T780">well</ta>
            <ta e="T782" id="Seg_8730" s="T781">listen-PRS-1SG</ta>
            <ta e="T784" id="Seg_8731" s="T783">Chinese-PL-PL.[NOM]</ta>
            <ta e="T785" id="Seg_8732" s="T784">what-PL.[NOM]</ta>
            <ta e="T786" id="Seg_8733" s="T785">Q</ta>
            <ta e="T788" id="Seg_8734" s="T787">that-PL-ACC</ta>
            <ta e="T789" id="Seg_8735" s="T788">listen-CVB.SEQ</ta>
            <ta e="T790" id="Seg_8736" s="T789">MOD</ta>
            <ta e="T791" id="Seg_8737" s="T790">lie-FREQ-PRS-1SG</ta>
            <ta e="T792" id="Seg_8738" s="T791">say-CVB.SEQ</ta>
            <ta e="T793" id="Seg_8739" s="T792">then</ta>
            <ta e="T794" id="Seg_8740" s="T793">happily</ta>
            <ta e="T795" id="Seg_8741" s="T794">very</ta>
            <ta e="T796" id="Seg_8742" s="T795">know-PRS-1SG</ta>
            <ta e="T797" id="Seg_8743" s="T796">that-ABL</ta>
            <ta e="T798" id="Seg_8744" s="T797">luckily</ta>
            <ta e="T799" id="Seg_8745" s="T798">Russian-PL.[NOM]</ta>
            <ta e="T800" id="Seg_8746" s="T799">find-PST1-1SG</ta>
            <ta e="T801" id="Seg_8747" s="T800">that-ABL</ta>
            <ta e="T802" id="Seg_8748" s="T801">stop-CVB.SEQ</ta>
            <ta e="T803" id="Seg_8749" s="T802">stay-PST2.[3SG]</ta>
            <ta e="T805" id="Seg_8750" s="T804">well</ta>
            <ta e="T806" id="Seg_8751" s="T805">stop-EP-CAUS-CVB.SEQ</ta>
            <ta e="T807" id="Seg_8752" s="T806">throw-PTCP.FUT-DAT/LOC</ta>
            <ta e="T808" id="Seg_8753" s="T807">stop-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T809" id="Seg_8754" s="T808">father-2SG.[NOM]</ta>
            <ta e="T810" id="Seg_8755" s="T809">come-FUT.[3SG]</ta>
            <ta e="T811" id="Seg_8756" s="T810">AFFIRM</ta>
            <ta e="T813" id="Seg_8757" s="T812">sun-1PL.[NOM]</ta>
            <ta e="T814" id="Seg_8758" s="T813">moon-1PL.[NOM]</ta>
            <ta e="T815" id="Seg_8759" s="T814">move.away-PST1-3SG</ta>
            <ta e="T816" id="Seg_8760" s="T815">EMPH</ta>
            <ta e="T817" id="Seg_8761" s="T816">up</ta>
            <ta e="T818" id="Seg_8762" s="T817">be-PST1-3SG</ta>
            <ta e="T819" id="Seg_8763" s="T818">EMPH</ta>
            <ta e="T820" id="Seg_8764" s="T819">beautiful</ta>
            <ta e="T822" id="Seg_8765" s="T821">EMPH</ta>
            <ta e="T823" id="Seg_8766" s="T822">bright.[NOM]</ta>
            <ta e="T824" id="Seg_8767" s="T823">by.day</ta>
            <ta e="T825" id="Seg_8768" s="T824">similar</ta>
            <ta e="T827" id="Seg_8769" s="T826">oh</ta>
            <ta e="T828" id="Seg_8770" s="T827">reindeer-PL-1PL.[NOM]</ta>
            <ta e="T830" id="Seg_8771" s="T829">river.[NOM]</ta>
            <ta e="T831" id="Seg_8772" s="T830">along</ta>
            <ta e="T832" id="Seg_8773" s="T831">run-CVB.SIM</ta>
            <ta e="T833" id="Seg_8774" s="T832">go-FREQ-PRS-3PL</ta>
            <ta e="T834" id="Seg_8775" s="T833">well</ta>
            <ta e="T835" id="Seg_8776" s="T834">EMPH</ta>
            <ta e="T836" id="Seg_8777" s="T835">beautiful-ADVZ</ta>
            <ta e="T837" id="Seg_8778" s="T836">EMPH</ta>
            <ta e="T839" id="Seg_8779" s="T838">who.[NOM]</ta>
            <ta e="T840" id="Seg_8780" s="T839">smoke-3PL.[NOM]</ta>
            <ta e="T841" id="Seg_8781" s="T840">even</ta>
            <ta e="T842" id="Seg_8782" s="T841">fly-PRS.[3SG]</ta>
            <ta e="T844" id="Seg_8783" s="T843">jump</ta>
            <ta e="T845" id="Seg_8784" s="T844">EMPH</ta>
            <ta e="T846" id="Seg_8785" s="T845">beautiful</ta>
            <ta e="T847" id="Seg_8786" s="T846">who.[NOM]</ta>
            <ta e="T848" id="Seg_8787" s="T847">well</ta>
            <ta e="T849" id="Seg_8788" s="T848">snow-EP-2SG.[NOM]</ta>
            <ta e="T850" id="Seg_8789" s="T849">well</ta>
            <ta e="T851" id="Seg_8790" s="T850">who-DAT/LOC</ta>
            <ta e="T852" id="Seg_8791" s="T851">moon-ABL</ta>
            <ta e="T853" id="Seg_8792" s="T852">EMPH</ta>
            <ta e="T854" id="Seg_8793" s="T853">who-VBZ-PRS.[3SG]</ta>
            <ta e="T855" id="Seg_8794" s="T854">glint-ITER-PRS.[3SG]</ta>
            <ta e="T857" id="Seg_8795" s="T856">that-ABL</ta>
            <ta e="T858" id="Seg_8796" s="T857">that-ABL</ta>
            <ta e="T859" id="Seg_8797" s="T858">wait</ta>
            <ta e="T860" id="Seg_8798" s="T859">dog-1PL.[NOM]</ta>
            <ta e="T863" id="Seg_8799" s="T862">make-PRS.[3SG]</ta>
            <ta e="T864" id="Seg_8800" s="T863">this</ta>
            <ta e="T865" id="Seg_8801" s="T864">who-VBZ-PRS.[3SG]</ta>
            <ta e="T866" id="Seg_8802" s="T865">whine-PRS.[3SG]</ta>
            <ta e="T868" id="Seg_8803" s="T867">INTJ</ta>
            <ta e="T869" id="Seg_8804" s="T868">bell.[NOM]</ta>
            <ta e="T870" id="Seg_8805" s="T869">sound-PST1-3SG</ta>
            <ta e="T871" id="Seg_8806" s="T870">only</ta>
            <ta e="T872" id="Seg_8807" s="T871">neck.bell</ta>
            <ta e="T877" id="Seg_8808" s="T876">father-1PL.[NOM]</ta>
            <ta e="T878" id="Seg_8809" s="T877">go-PRS.[3SG]</ta>
            <ta e="T879" id="Seg_8810" s="T878">father-1PL.[NOM]</ta>
            <ta e="T880" id="Seg_8811" s="T879">AFFIRM</ta>
            <ta e="T882" id="Seg_8812" s="T881">well</ta>
            <ta e="T883" id="Seg_8813" s="T882">that</ta>
            <ta e="T884" id="Seg_8814" s="T883">come-PST1-3SG</ta>
            <ta e="T886" id="Seg_8815" s="T885">1SG.[NOM]</ta>
            <ta e="T887" id="Seg_8816" s="T886">MOD</ta>
            <ta e="T888" id="Seg_8817" s="T887">know-PST2.NEG-EP-1SG</ta>
            <ta e="T889" id="Seg_8818" s="T888">EMPH</ta>
            <ta e="T890" id="Seg_8819" s="T889">wood-VBZ-MED-CVB.SIM</ta>
            <ta e="T891" id="Seg_8820" s="T890">go-PST2.[3SG]</ta>
            <ta e="T892" id="Seg_8821" s="T891">think-PRS-1SG</ta>
            <ta e="T893" id="Seg_8822" s="T892">EMPH</ta>
            <ta e="T895" id="Seg_8823" s="T894">3SG-2SG.[NOM]</ta>
            <ta e="T896" id="Seg_8824" s="T895">EMPH</ta>
            <ta e="T897" id="Seg_8825" s="T896">Dudinka.[NOM]</ta>
            <ta e="T898" id="Seg_8826" s="T897">to</ta>
            <ta e="T899" id="Seg_8827" s="T898">go-EP-PST2.[3SG]</ta>
            <ta e="T900" id="Seg_8828" s="T899">and</ta>
            <ta e="T901" id="Seg_8829" s="T900">Dudinka.[NOM]</ta>
            <ta e="T902" id="Seg_8830" s="T901">to</ta>
            <ta e="T904" id="Seg_8831" s="T903">well</ta>
            <ta e="T905" id="Seg_8832" s="T904">that</ta>
            <ta e="T906" id="Seg_8833" s="T905">come-PST1-3SG</ta>
            <ta e="T909" id="Seg_8834" s="T908">get.dark-PTCP.PST-3SG-ACC</ta>
            <ta e="T910" id="Seg_8835" s="T909">after</ta>
            <ta e="T911" id="Seg_8836" s="T910">well</ta>
            <ta e="T913" id="Seg_8837" s="T912">oh</ta>
            <ta e="T914" id="Seg_8838" s="T913">father-1SG.[NOM]</ta>
            <ta e="T915" id="Seg_8839" s="T914">come-PST1-3SG</ta>
            <ta e="T917" id="Seg_8840" s="T916">oh</ta>
            <ta e="T918" id="Seg_8841" s="T917">mum-1SG.[NOM]</ta>
            <ta e="T919" id="Seg_8842" s="T918">help-CVB.SIM</ta>
            <ta e="T920" id="Seg_8843" s="T919">go-PST1-3SG</ta>
            <ta e="T921" id="Seg_8844" s="T920">untie-PST1-3SG</ta>
            <ta e="T922" id="Seg_8845" s="T921">and.so.on-PST1-3SG</ta>
            <ta e="T923" id="Seg_8846" s="T922">neck.bell-ABL</ta>
            <ta e="T924" id="Seg_8847" s="T923">reindeer-3PL-ACC</ta>
            <ta e="T925" id="Seg_8848" s="T924">release-PST1-3PL</ta>
            <ta e="T926" id="Seg_8849" s="T925">and.so.on-PST1-3PL</ta>
            <ta e="T928" id="Seg_8850" s="T927">oh</ta>
            <ta e="T929" id="Seg_8851" s="T928">father-1SG.[NOM]</ta>
            <ta e="T930" id="Seg_8852" s="T929">bag-PL-PROPR</ta>
            <ta e="T931" id="Seg_8853" s="T930">come-PST1-3SG</ta>
            <ta e="T933" id="Seg_8854" s="T932">sack-PL-3SG-ACC</ta>
            <ta e="T934" id="Seg_8855" s="T933">go.in-CAUS-PST1-3SG</ta>
            <ta e="T936" id="Seg_8856" s="T935">well</ta>
            <ta e="T937" id="Seg_8857" s="T936">take.out-CAUS-FREQ-PST1-3SG</ta>
            <ta e="T938" id="Seg_8858" s="T937">oh</ta>
            <ta e="T939" id="Seg_8859" s="T938">well</ta>
            <ta e="T940" id="Seg_8860" s="T939">sweet.[NOM]</ta>
            <ta e="T941" id="Seg_8861" s="T940">INTJ-3SG-ACC</ta>
            <ta e="T942" id="Seg_8862" s="T941">bring-PST2.[3SG]</ta>
            <ta e="T944" id="Seg_8863" s="T943">that-ABL</ta>
            <ta e="T945" id="Seg_8864" s="T944">beautiful</ta>
            <ta e="T946" id="Seg_8865" s="T945">very</ta>
            <ta e="T947" id="Seg_8866" s="T946">always</ta>
            <ta e="T948" id="Seg_8867" s="T947">remember-PRS-1SG</ta>
            <ta e="T950" id="Seg_8868" s="T949">who.[NOM]</ta>
            <ta e="T951" id="Seg_8869" s="T950">clock-PL.[NOM]</ta>
            <ta e="T952" id="Seg_8870" s="T951">Q</ta>
            <ta e="T953" id="Seg_8871" s="T952">beautiful</ta>
            <ta e="T954" id="Seg_8872" s="T953">very</ta>
            <ta e="T955" id="Seg_8873" s="T954">who.[NOM]</ta>
            <ta e="T956" id="Seg_8874" s="T955">mirror-INSTR</ta>
            <ta e="T957" id="Seg_8875" s="T956">be.made-EP-PTCP.PST</ta>
            <ta e="T959" id="Seg_8876" s="T958">that-3SG-2SG.[NOM]</ta>
            <ta e="T961" id="Seg_8877" s="T960">button-PL-PROPR.[NOM]</ta>
            <ta e="T962" id="Seg_8878" s="T961">what.[NOM]</ta>
            <ta e="T963" id="Seg_8879" s="T962">INDEF</ta>
            <ta e="T964" id="Seg_8880" s="T963">EMPH</ta>
            <ta e="T965" id="Seg_8881" s="T964">beautiful</ta>
            <ta e="T966" id="Seg_8882" s="T965">be.surprised-EMOT-PRS-1SG</ta>
            <ta e="T967" id="Seg_8883" s="T966">EMPH</ta>
            <ta e="T968" id="Seg_8884" s="T967">beautiful</ta>
            <ta e="T970" id="Seg_8885" s="T969">well</ta>
            <ta e="T971" id="Seg_8886" s="T970">touch-EP-NEG.[IMP.2SG]</ta>
            <ta e="T972" id="Seg_8887" s="T971">alright</ta>
            <ta e="T973" id="Seg_8888" s="T972">be-FUT.[3SG]</ta>
            <ta e="T974" id="Seg_8889" s="T973">say-PRS-3PL</ta>
            <ta e="T976" id="Seg_8890" s="T975">touch-CAUS-PTCP.HAB-3SG</ta>
            <ta e="T977" id="Seg_8891" s="T976">no-3PL</ta>
            <ta e="T978" id="Seg_8892" s="T977">EMPH</ta>
            <ta e="T980" id="Seg_8893" s="T979">break-FUT-2SG</ta>
            <ta e="T981" id="Seg_8894" s="T980">well</ta>
            <ta e="T982" id="Seg_8895" s="T981">AFFIRM</ta>
            <ta e="T984" id="Seg_8896" s="T983">well</ta>
            <ta e="T985" id="Seg_8897" s="T984">lay-PST1-1SG</ta>
            <ta e="T986" id="Seg_8898" s="T985">very</ta>
            <ta e="T987" id="Seg_8899" s="T986">see-PTCP.FUT-1SG-ACC</ta>
            <ta e="T988" id="Seg_8900" s="T987">want-PRS-1SG</ta>
            <ta e="T989" id="Seg_8901" s="T988">touch-FREQ-PTCP.FUT-1SG-ACC</ta>
            <ta e="T990" id="Seg_8902" s="T989">want-PRS-1SG</ta>
            <ta e="T991" id="Seg_8903" s="T990">EMPH</ta>
            <ta e="T993" id="Seg_8904" s="T992">that-ABL</ta>
            <ta e="T994" id="Seg_8905" s="T993">who.[NOM]</ta>
            <ta e="T995" id="Seg_8906" s="T994">who-ACC</ta>
            <ta e="T996" id="Seg_8907" s="T995">bring-PST2-3SG</ta>
            <ta e="T997" id="Seg_8908" s="T996">1SG-DAT/LOC</ta>
            <ta e="T999" id="Seg_8909" s="T998">compas-DIM.[NOM]</ta>
            <ta e="T1000" id="Seg_8910" s="T999">small.[NOM]</ta>
            <ta e="T1002" id="Seg_8911" s="T1001">that-ACC</ta>
            <ta e="T1003" id="Seg_8912" s="T1002">always</ta>
            <ta e="T1004" id="Seg_8913" s="T1003">play-CVB.SIM</ta>
            <ta e="T1005" id="Seg_8914" s="T1004">lie-HAB-1SG</ta>
            <ta e="T1006" id="Seg_8915" s="T1005">oh.dear</ta>
            <ta e="T1007" id="Seg_8916" s="T1006">beautiful-ADVZ</ta>
            <ta e="T1008" id="Seg_8917" s="T1007">Q</ta>
            <ta e="T1009" id="Seg_8918" s="T1008">in.the.beginning</ta>
            <ta e="T1012" id="Seg_8919" s="T1011">bring-PST2-3SG</ta>
            <ta e="T1013" id="Seg_8920" s="T1012">this.[NOM]</ta>
            <ta e="T1014" id="Seg_8921" s="T1013">like</ta>
            <ta e="T1015" id="Seg_8922" s="T1014">toys-ACC</ta>
            <ta e="T1016" id="Seg_8923" s="T1015">well</ta>
            <ta e="T1018" id="Seg_8924" s="T1017">oh</ta>
            <ta e="T1019" id="Seg_8925" s="T1018">1SG.[NOM]</ta>
            <ta e="T1020" id="Seg_8926" s="T1019">most</ta>
            <ta e="T1021" id="Seg_8927" s="T1020">NEG</ta>
            <ta e="T1023" id="Seg_8928" s="T1022">beautiful-ADVZ</ta>
            <ta e="T1024" id="Seg_8929" s="T1023">EMPH</ta>
            <ta e="T1026" id="Seg_8930" s="T1025">oh</ta>
            <ta e="T1027" id="Seg_8931" s="T1026">2SG-DAT/LOC</ta>
            <ta e="T1029" id="Seg_8932" s="T1028">say-PRS.[3SG]</ta>
            <ta e="T1031" id="Seg_8933" s="T1030">well</ta>
            <ta e="T1032" id="Seg_8934" s="T1031">that</ta>
            <ta e="T1033" id="Seg_8935" s="T1032">chat-PRS-3PL</ta>
            <ta e="T1034" id="Seg_8936" s="T1033">apparently</ta>
            <ta e="T1035" id="Seg_8937" s="T1034">well</ta>
            <ta e="T1036" id="Seg_8938" s="T1035">1SG.[NOM]</ta>
            <ta e="T1037" id="Seg_8939" s="T1036">sleep-CVB.SEQ</ta>
            <ta e="T1038" id="Seg_8940" s="T1037">stay-PST2-1SG</ta>
            <ta e="T1040" id="Seg_8941" s="T1039">in.the.morning</ta>
            <ta e="T1041" id="Seg_8942" s="T1040">already</ta>
            <ta e="T1042" id="Seg_8943" s="T1041">wake.up-PRS-1SG</ta>
            <ta e="T1043" id="Seg_8944" s="T1042">oh</ta>
            <ta e="T1044" id="Seg_8945" s="T1043">stove.[NOM]</ta>
            <ta e="T1045" id="Seg_8946" s="T1044">again</ta>
            <ta e="T1046" id="Seg_8947" s="T1045">rise-EP-CAUS-CVB.SIM</ta>
            <ta e="T1047" id="Seg_8948" s="T1046">stand-PRS.[3SG]</ta>
            <ta e="T1049" id="Seg_8949" s="T1048">beautiful-ADVZ</ta>
            <ta e="T1050" id="Seg_8950" s="T1049">Q</ta>
            <ta e="T1051" id="Seg_8951" s="T1050">who.[NOM]</ta>
            <ta e="T1052" id="Seg_8952" s="T1051">NEG</ta>
            <ta e="T1053" id="Seg_8953" s="T1052">NEG</ta>
            <ta e="T1054" id="Seg_8954" s="T1053">house-1PL.[NOM]</ta>
            <ta e="T1055" id="Seg_8955" s="T1054">inside-3SG-DAT/LOC</ta>
            <ta e="T1057" id="Seg_8956" s="T1056">that</ta>
            <ta e="T1058" id="Seg_8957" s="T1057">reindeer-3PL-ACC</ta>
            <ta e="T1059" id="Seg_8958" s="T1058">grab-PRS-PL</ta>
            <ta e="T1060" id="Seg_8959" s="T1059">be-PST2.[3SG]</ta>
            <ta e="T1062" id="Seg_8960" s="T1061">catch-CVB.SIM</ta>
            <ta e="T1063" id="Seg_8961" s="T1062">go.out-EP-PST2-3PL</ta>
            <ta e="T1065" id="Seg_8962" s="T1064">neighbour-PL-PROPR.[NOM]</ta>
            <ta e="T1066" id="Seg_8963" s="T1065">be-PST1-1PL</ta>
            <ta e="T1067" id="Seg_8964" s="T1066">so</ta>
            <ta e="T1068" id="Seg_8965" s="T1067">INDEF</ta>
            <ta e="T1069" id="Seg_8966" s="T1068">there.is</ta>
            <ta e="T1070" id="Seg_8967" s="T1069">be-PST1-3PL</ta>
            <ta e="T1071" id="Seg_8968" s="T1070">neighbour-PL-1PL.[NOM]</ta>
            <ta e="T1073" id="Seg_8969" s="T1072">well</ta>
            <ta e="T1074" id="Seg_8970" s="T1073">that</ta>
            <ta e="T1075" id="Seg_8971" s="T1074">gather-PST1-3PL</ta>
            <ta e="T1076" id="Seg_8972" s="T1075">herd-3PL-ACC</ta>
            <ta e="T1077" id="Seg_8973" s="T1076">gather-MED-PRS-3PL</ta>
            <ta e="T1078" id="Seg_8974" s="T1077">reindeer.[NOM]</ta>
            <ta e="T1079" id="Seg_8975" s="T1078">what.[NOM]</ta>
            <ta e="T1080" id="Seg_8976" s="T1079">nomadize-PRS-1PL</ta>
            <ta e="T1081" id="Seg_8977" s="T1080">apparently</ta>
            <ta e="T1082" id="Seg_8978" s="T1081">remember-PRS-1SG</ta>
            <ta e="T1083" id="Seg_8979" s="T1082">whereto</ta>
            <ta e="T1084" id="Seg_8980" s="T1083">INDEF</ta>
            <ta e="T1086" id="Seg_8981" s="T1085">oh</ta>
            <ta e="T1087" id="Seg_8982" s="T1086">well</ta>
            <ta e="T1088" id="Seg_8983" s="T1087">1SG-ACC</ta>
            <ta e="T1089" id="Seg_8984" s="T1088">dress-CAUS-CVB.SEQ</ta>
            <ta e="T1090" id="Seg_8985" s="T1089">%%-PST1-3PL</ta>
            <ta e="T1092" id="Seg_8986" s="T1091">sledge-DAT/LOC</ta>
            <ta e="T1093" id="Seg_8987" s="T1092">tie-CVB.SEQ</ta>
            <ta e="T1094" id="Seg_8988" s="T1093">throw-PST2-3PL</ta>
            <ta e="T1095" id="Seg_8989" s="T1094">%%-CVB.SEQ</ta>
            <ta e="T1096" id="Seg_8990" s="T1095">throw-PST2-3PL</ta>
            <ta e="T1098" id="Seg_8991" s="T1097">well</ta>
            <ta e="T1099" id="Seg_8992" s="T1098">whereto</ta>
            <ta e="T1100" id="Seg_8993" s="T1099">go-PRS-1PL</ta>
            <ta e="T1102" id="Seg_8994" s="T1101">and</ta>
            <ta e="T1103" id="Seg_8995" s="T1102">back-1PL-ABL</ta>
            <ta e="T1104" id="Seg_8996" s="T1103">Q</ta>
            <ta e="T1105" id="Seg_8997" s="T1104">reindeer-PL-ACC</ta>
            <ta e="T1106" id="Seg_8998" s="T1105">lead-HAB-3PL</ta>
            <ta e="T1107" id="Seg_8999" s="T1106">cargo.sleigh-DAT/LOC</ta>
            <ta e="T1109" id="Seg_9000" s="T1108">food-PROPR</ta>
            <ta e="T1110" id="Seg_9001" s="T1109">or</ta>
            <ta e="T1111" id="Seg_9002" s="T1110">wood-PROPR</ta>
            <ta e="T1112" id="Seg_9003" s="T1111">cargo.sleigh.[NOM]</ta>
            <ta e="T1113" id="Seg_9004" s="T1112">lead-PTCP.HAB</ta>
            <ta e="T1114" id="Seg_9005" s="T1113">be-PST1-1PL</ta>
            <ta e="T1116" id="Seg_9006" s="T1115">that-PL-ABL</ta>
            <ta e="T1117" id="Seg_9007" s="T1116">be.afraid-CVB.SEQ</ta>
            <ta e="T1118" id="Seg_9008" s="T1117">cry-CVB.SEQ</ta>
            <ta e="T1119" id="Seg_9009" s="T1118">cry.loudly-PRS-1SG</ta>
            <ta e="T1120" id="Seg_9010" s="T1119">say-CVB.SEQ</ta>
            <ta e="T1122" id="Seg_9011" s="T1121">be.afraid-EP-NEG.[IMP.2SG]</ta>
            <ta e="T1123" id="Seg_9012" s="T1122">bite-FUT.[3SG]</ta>
            <ta e="T1124" id="Seg_9013" s="T1123">NEG-3SG</ta>
            <ta e="T1126" id="Seg_9014" s="T1125">this</ta>
            <ta e="T1127" id="Seg_9015" s="T1126">well</ta>
            <ta e="T1128" id="Seg_9016" s="T1127">get.tired-PST2-3PL</ta>
            <ta e="T1129" id="Seg_9017" s="T1128">well</ta>
            <ta e="T1130" id="Seg_9018" s="T1129">breath-PRS-3PL</ta>
            <ta e="T1133" id="Seg_9019" s="T1132">face-1SG-DAT/LOC</ta>
            <ta e="T1134" id="Seg_9020" s="T1133">here</ta>
            <ta e="T1136" id="Seg_9021" s="T1135">well</ta>
            <ta e="T1137" id="Seg_9022" s="T1136">that</ta>
            <ta e="T1138" id="Seg_9023" s="T1137">that.[NOM]</ta>
            <ta e="T1139" id="Seg_9024" s="T1138">like</ta>
            <ta e="T1140" id="Seg_9025" s="T1139">nomadize-CVB.SIM</ta>
            <ta e="T1141" id="Seg_9026" s="T1140">go-PTCP.HAB</ta>
            <ta e="T1142" id="Seg_9027" s="T1141">be-PST1-1PL</ta>
            <ta e="T1143" id="Seg_9028" s="T1142">nomadize-CVB.SIM</ta>
            <ta e="T1144" id="Seg_9029" s="T1143">go-PTCP.HAB</ta>
            <ta e="T1145" id="Seg_9030" s="T1144">start-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T1146" id="Seg_9031" s="T1145">go-PTCP.HAB</ta>
            <ta e="T1147" id="Seg_9032" s="T1146">be-PST1-1SG</ta>
            <ta e="T1148" id="Seg_9033" s="T1147">3PL-ACC</ta>
            <ta e="T1149" id="Seg_9034" s="T1148">with</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_9035" s="T1">einmal</ta>
            <ta e="T3" id="Seg_9036" s="T2">erinnern-PRS-1SG</ta>
            <ta e="T4" id="Seg_9037" s="T3">klein.[NOM]</ta>
            <ta e="T5" id="Seg_9038" s="T4">sein-TEMP-1SG</ta>
            <ta e="T6" id="Seg_9039" s="T5">Nahong-DAT/LOC</ta>
            <ta e="T7" id="Seg_9040" s="T6">Jagdstelle-1PL.[NOM]</ta>
            <ta e="T8" id="Seg_9041" s="T7">Kresty-ABL</ta>
            <ta e="T9" id="Seg_9042" s="T8">zehn</ta>
            <ta e="T10" id="Seg_9043" s="T9">zwei</ta>
            <ta e="T11" id="Seg_9044" s="T10">Kilometer.[NOM]</ta>
            <ta e="T12" id="Seg_9045" s="T11">dort</ta>
            <ta e="T13" id="Seg_9046" s="T12">wie.viel-PROPR</ta>
            <ta e="T14" id="Seg_9047" s="T13">Mensch.[NOM]</ta>
            <ta e="T15" id="Seg_9048" s="T14">leben-PST2-3SG=Q</ta>
            <ta e="T17" id="Seg_9049" s="T16">alt-PL.[NOM]</ta>
            <ta e="T18" id="Seg_9050" s="T17">jeder-3PL.[NOM]</ta>
            <ta e="T19" id="Seg_9051" s="T18">dort</ta>
            <ta e="T20" id="Seg_9052" s="T19">leben-PTCP.HAB</ta>
            <ta e="T21" id="Seg_9053" s="T20">sein-PST1-3PL</ta>
            <ta e="T23" id="Seg_9054" s="T22">Schlitten.[NOM]</ta>
            <ta e="T24" id="Seg_9055" s="T23">Haus.[NOM]</ta>
            <ta e="T25" id="Seg_9056" s="T24">voll.[NOM]</ta>
            <ta e="T26" id="Seg_9057" s="T25">sein-PST1-3SG</ta>
            <ta e="T27" id="Seg_9058" s="T26">dort</ta>
            <ta e="T29" id="Seg_9059" s="T28">Rentier-PL.[NOM]</ta>
            <ta e="T30" id="Seg_9060" s="T29">was-PL.[NOM]</ta>
            <ta e="T31" id="Seg_9061" s="T30">es.gibt</ta>
            <ta e="T32" id="Seg_9062" s="T31">sein-PST1-3PL</ta>
            <ta e="T34" id="Seg_9063" s="T33">jenes-ABL</ta>
            <ta e="T35" id="Seg_9064" s="T34">einmal</ta>
            <ta e="T36" id="Seg_9065" s="T35">Vater-1SG-PROPR.[NOM]</ta>
            <ta e="T37" id="Seg_9066" s="T36">Haus-3PL-DAT/LOC</ta>
            <ta e="T38" id="Seg_9067" s="T37">Vater-1SG.[NOM]</ta>
            <ta e="T39" id="Seg_9068" s="T38">alt</ta>
            <ta e="T40" id="Seg_9069" s="T39">Großvater-1SG-ACC</ta>
            <ta e="T41" id="Seg_9070" s="T40">sagen-HAB-1SG</ta>
            <ta e="T42" id="Seg_9071" s="T41">EMPH</ta>
            <ta e="T43" id="Seg_9072" s="T42">Vater</ta>
            <ta e="T44" id="Seg_9073" s="T43">sagen-HAB-1SG</ta>
            <ta e="T45" id="Seg_9074" s="T44">alt</ta>
            <ta e="T46" id="Seg_9075" s="T45">Mama-1SG-ACC</ta>
            <ta e="T47" id="Seg_9076" s="T46">Mama.[NOM]</ta>
            <ta e="T48" id="Seg_9077" s="T47">sagen-HAB-1SG</ta>
            <ta e="T49" id="Seg_9078" s="T48">alt</ta>
            <ta e="T50" id="Seg_9079" s="T49">Großmutter-1SG-ACC</ta>
            <ta e="T51" id="Seg_9080" s="T50">3PL-DAT/LOC</ta>
            <ta e="T52" id="Seg_9081" s="T51">füttern-EP-PASS/REFL-EP-PST2-EP-1SG</ta>
            <ta e="T54" id="Seg_9082" s="T53">doch</ta>
            <ta e="T55" id="Seg_9083" s="T54">jenes</ta>
            <ta e="T56" id="Seg_9084" s="T55">Vater-1PL.[NOM]</ta>
            <ta e="T57" id="Seg_9085" s="T56">wohin</ta>
            <ta e="T58" id="Seg_9086" s="T57">INDEF</ta>
            <ta e="T59" id="Seg_9087" s="T58">gehen-CVB.SEQ</ta>
            <ta e="T60" id="Seg_9088" s="T59">bleiben-PST1-3SG</ta>
            <ta e="T62" id="Seg_9089" s="T61">Holz-VBZ-CVB.SIM</ta>
            <ta e="T63" id="Seg_9090" s="T62">gehen-PST1-3SG</ta>
            <ta e="T64" id="Seg_9091" s="T63">offenbar</ta>
            <ta e="T65" id="Seg_9092" s="T64">doch</ta>
            <ta e="T66" id="Seg_9093" s="T65">sitzen-PRS-1PL</ta>
            <ta e="T67" id="Seg_9094" s="T66">Rentier-PL-1PL.[NOM]</ta>
            <ta e="T69" id="Seg_9095" s="T68">wer-DAT/LOC</ta>
            <ta e="T70" id="Seg_9096" s="T69">Haus-1PL.[NOM]</ta>
            <ta e="T71" id="Seg_9097" s="T70">Rand-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_9098" s="T71">gehen-PRS-3PL</ta>
            <ta e="T74" id="Seg_9099" s="T73">scharren.mit.Huf-CVB.SIM</ta>
            <ta e="T75" id="Seg_9100" s="T74">gehen-PRS-3PL</ta>
            <ta e="T78" id="Seg_9101" s="T77">gezähmtes.Rentier-PROPR.[NOM]</ta>
            <ta e="T79" id="Seg_9102" s="T78">sein-PST1-1PL</ta>
            <ta e="T81" id="Seg_9103" s="T80">dieses</ta>
            <ta e="T82" id="Seg_9104" s="T81">Hand-ABL</ta>
            <ta e="T83" id="Seg_9105" s="T82">essen-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_9106" s="T84">Brot-EP-PHIL.[NOM]</ta>
            <ta e="T86" id="Seg_9107" s="T85">sehr</ta>
            <ta e="T88" id="Seg_9108" s="T87">Fenster-DAT/LOC</ta>
            <ta e="T89" id="Seg_9109" s="T88">kommen-HAB.[3SG]</ta>
            <ta e="T90" id="Seg_9110" s="T89">Brot.[NOM]</ta>
            <ta e="T91" id="Seg_9111" s="T90">suchen-CVB.SIM</ta>
            <ta e="T93" id="Seg_9112" s="T92">oh</ta>
            <ta e="T94" id="Seg_9113" s="T93">Nase-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_9114" s="T94">nah.und.gross.sein-CVB.SEQ</ta>
            <ta e="T96" id="Seg_9115" s="T95">kommen-FUT.[3SG]</ta>
            <ta e="T97" id="Seg_9116" s="T96">sagen-CVB.SEQ</ta>
            <ta e="T98" id="Seg_9117" s="T97">oh</ta>
            <ta e="T99" id="Seg_9118" s="T98">erschrecken-PRS-1SG</ta>
            <ta e="T100" id="Seg_9119" s="T99">klein.[NOM]</ta>
            <ta e="T101" id="Seg_9120" s="T100">Mensch.[NOM]</ta>
            <ta e="T102" id="Seg_9121" s="T101">nun</ta>
            <ta e="T103" id="Seg_9122" s="T102">Angst.haben-PTCP.HAB</ta>
            <ta e="T104" id="Seg_9123" s="T103">sein-PST1-1SG</ta>
            <ta e="T105" id="Seg_9124" s="T104">EMPH</ta>
            <ta e="T108" id="Seg_9125" s="T107">dieses</ta>
            <ta e="T109" id="Seg_9126" s="T108">Mensch.[NOM]</ta>
            <ta e="T110" id="Seg_9127" s="T109">verstecken-CVB.SEQ</ta>
            <ta e="T111" id="Seg_9128" s="T110">öffnen-PST1-1SG</ta>
            <ta e="T112" id="Seg_9129" s="T111">EMPH</ta>
            <ta e="T114" id="Seg_9130" s="T113">Tisch-ABL</ta>
            <ta e="T115" id="Seg_9131" s="T114">dieses</ta>
            <ta e="T116" id="Seg_9132" s="T115">Brot.[NOM]</ta>
            <ta e="T117" id="Seg_9133" s="T116">Brotfladen.[NOM]</ta>
            <ta e="T118" id="Seg_9134" s="T117">sehr-PL</ta>
            <ta e="T120" id="Seg_9135" s="T119">oh</ta>
            <ta e="T121" id="Seg_9136" s="T120">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T122" id="Seg_9137" s="T121">aufgeregt.sein-PST2.[3SG]</ta>
            <ta e="T123" id="Seg_9138" s="T122">essen-CVB.SIM</ta>
            <ta e="T124" id="Seg_9139" s="T123">essen-EMOT-PRS.[3SG]</ta>
            <ta e="T125" id="Seg_9140" s="T124">sagen-CVB.SEQ</ta>
            <ta e="T126" id="Seg_9141" s="T125">oh.nein</ta>
            <ta e="T128" id="Seg_9142" s="T127">doch</ta>
            <ta e="T129" id="Seg_9143" s="T128">jenes</ta>
            <ta e="T130" id="Seg_9144" s="T129">Mama-1SG.[NOM]</ta>
            <ta e="T131" id="Seg_9145" s="T130">sagen-PRS.[3SG]</ta>
            <ta e="T132" id="Seg_9146" s="T131">alt</ta>
            <ta e="T133" id="Seg_9147" s="T132">Mama-1SG.[NOM]</ta>
            <ta e="T134" id="Seg_9148" s="T133">nun</ta>
            <ta e="T136" id="Seg_9149" s="T135">oh</ta>
            <ta e="T137" id="Seg_9150" s="T136">lehren-EP-NEG.[IMP.2SG]</ta>
            <ta e="T138" id="Seg_9151" s="T137">doch</ta>
            <ta e="T139" id="Seg_9152" s="T138">2SG.[NOM]</ta>
            <ta e="T140" id="Seg_9153" s="T139">Fenster-1PL-ACC</ta>
            <ta e="T141" id="Seg_9154" s="T140">zerbrechen-FUT.[3SG]</ta>
            <ta e="T142" id="Seg_9155" s="T141">und.so.weiter-FUT.[3SG]</ta>
            <ta e="T144" id="Seg_9156" s="T143">gehen.[IMP.2SG]</ta>
            <ta e="T145" id="Seg_9157" s="T144">gehen.[IMP.2SG]</ta>
            <ta e="T146" id="Seg_9158" s="T145">gehen.[IMP.2SG]</ta>
            <ta e="T147" id="Seg_9159" s="T146">kommen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T149" id="Seg_9160" s="T148">sagen-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_9161" s="T149">was.[NOM]</ta>
            <ta e="T151" id="Seg_9162" s="T150">NEG</ta>
            <ta e="T152" id="Seg_9163" s="T151">NEG.EX</ta>
            <ta e="T154" id="Seg_9164" s="T153">Gras-2SG-ACC</ta>
            <ta e="T155" id="Seg_9165" s="T154">essen.[IMP.2SG]</ta>
            <ta e="T156" id="Seg_9166" s="T155">gehen.[IMP.2SG]</ta>
            <ta e="T157" id="Seg_9167" s="T156">sagen-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_9168" s="T157">AFFIRM</ta>
            <ta e="T159" id="Seg_9169" s="T158">schließen-CVB.SEQ</ta>
            <ta e="T160" id="Seg_9170" s="T159">werfen-PST1-3SG</ta>
            <ta e="T162" id="Seg_9171" s="T161">jenes-ABL</ta>
            <ta e="T163" id="Seg_9172" s="T162">anderer</ta>
            <ta e="T164" id="Seg_9173" s="T163">Fenster-EP-INSTR</ta>
            <ta e="T165" id="Seg_9174" s="T164">kommen-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_9175" s="T165">dort</ta>
            <ta e="T168" id="Seg_9176" s="T167">sein-HAB.[3SG]</ta>
            <ta e="T169" id="Seg_9177" s="T168">EMPH</ta>
            <ta e="T171" id="Seg_9178" s="T170">dort</ta>
            <ta e="T172" id="Seg_9179" s="T171">Eis.[NOM]</ta>
            <ta e="T173" id="Seg_9180" s="T172">was.[NOM]</ta>
            <ta e="T174" id="Seg_9181" s="T173">legen-EP-MED-HAB-1PL</ta>
            <ta e="T176" id="Seg_9182" s="T175">dieses</ta>
            <ta e="T177" id="Seg_9183" s="T176">nun</ta>
            <ta e="T178" id="Seg_9184" s="T177">Wasser-1PL.[NOM]</ta>
            <ta e="T179" id="Seg_9185" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_9186" s="T179">Eis-1PL.[NOM]</ta>
            <ta e="T182" id="Seg_9187" s="T181">dort</ta>
            <ta e="T183" id="Seg_9188" s="T182">klettern-CVB.SIM</ta>
            <ta e="T184" id="Seg_9189" s="T183">klettern-CVB.SIM</ta>
            <ta e="T185" id="Seg_9190" s="T184">Fenster-1PL.[NOM]</ta>
            <ta e="T186" id="Seg_9191" s="T185">öffnen-PASS/REFL-CVB.SEQ</ta>
            <ta e="T187" id="Seg_9192" s="T186">stehen-PRS.[3SG]</ta>
            <ta e="T188" id="Seg_9193" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_9194" s="T188">vorderer-1PL.[NOM]</ta>
            <ta e="T190" id="Seg_9195" s="T189">nun</ta>
            <ta e="T192" id="Seg_9196" s="T191">dort</ta>
            <ta e="T193" id="Seg_9197" s="T192">langes.Gesicht.haben-CVB.SIM</ta>
            <ta e="T194" id="Seg_9198" s="T193">langes.Gesicht.haben-CVB.SIM</ta>
            <ta e="T195" id="Seg_9199" s="T194">Horn-PL-PROPR.[NOM]</ta>
            <ta e="T196" id="Seg_9200" s="T195">EMPH</ta>
            <ta e="T198" id="Seg_9201" s="T197">Horn-PL-3SG.[NOM]</ta>
            <ta e="T199" id="Seg_9202" s="T198">passen-NEG-3PL</ta>
            <ta e="T200" id="Seg_9203" s="T199">wer.[NOM]</ta>
            <ta e="T201" id="Seg_9204" s="T200">Platz.für.Geschirr-DAT/LOC</ta>
            <ta e="T202" id="Seg_9205" s="T201">Brotfladen.[NOM]</ta>
            <ta e="T203" id="Seg_9206" s="T202">stehen-PTCP.PRS</ta>
            <ta e="T204" id="Seg_9207" s="T203">sein-PST1-3SG</ta>
            <ta e="T207" id="Seg_9208" s="T206">wer-VBZ-PTCP.PST</ta>
            <ta e="T208" id="Seg_9209" s="T207">schneiden-EP-PASS/REFL-EP-PTCP.PST-PL.[NOM]</ta>
            <ta e="T210" id="Seg_9210" s="T209">doch</ta>
            <ta e="T211" id="Seg_9211" s="T210">jenes-ACC</ta>
            <ta e="T212" id="Seg_9212" s="T211">ankommen-CVB.SIM</ta>
            <ta e="T213" id="Seg_9213" s="T212">versuchen-PST1-3SG</ta>
            <ta e="T214" id="Seg_9214" s="T213">EMPH</ta>
            <ta e="T215" id="Seg_9215" s="T214">Zunge-3SG-ACC</ta>
            <ta e="T216" id="Seg_9216" s="T215">ausstrecken-CVB.SIM</ta>
            <ta e="T217" id="Seg_9217" s="T216">ausstrecken-CVB.SIM</ta>
            <ta e="T219" id="Seg_9218" s="T218">Mama.[NOM]</ta>
            <ta e="T220" id="Seg_9219" s="T219">sehen.[IMP.2SG]</ta>
            <ta e="T221" id="Seg_9220" s="T220">sehen.[IMP.2SG]</ta>
            <ta e="T222" id="Seg_9221" s="T221">sehen.[IMP.2SG]</ta>
            <ta e="T223" id="Seg_9222" s="T222">sagen-PRS-1SG</ta>
            <ta e="T224" id="Seg_9223" s="T223">wer-2SG-ACC</ta>
            <ta e="T225" id="Seg_9224" s="T224">essen-CVB.PURP</ta>
            <ta e="T226" id="Seg_9225" s="T225">machen-PST1-3SG</ta>
            <ta e="T227" id="Seg_9226" s="T226">Brot-PL-2SG-ACC</ta>
            <ta e="T228" id="Seg_9227" s="T227">essen-CVB.PURP</ta>
            <ta e="T229" id="Seg_9228" s="T228">machen-PST1-3SG</ta>
            <ta e="T230" id="Seg_9229" s="T229">sagen-PRS-1SG</ta>
            <ta e="T231" id="Seg_9230" s="T230">Brot-PL-2SG-ACC</ta>
            <ta e="T233" id="Seg_9231" s="T232">EXCL</ta>
            <ta e="T234" id="Seg_9232" s="T233">warum</ta>
            <ta e="T235" id="Seg_9233" s="T234">Satan</ta>
            <ta e="T236" id="Seg_9234" s="T235">Tochter-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_9235" s="T236">warum</ta>
            <ta e="T238" id="Seg_9236" s="T237">lehren-PST2-2SG=Q</ta>
            <ta e="T239" id="Seg_9237" s="T238">EMPH</ta>
            <ta e="T240" id="Seg_9238" s="T239">Haus-ABL</ta>
            <ta e="T241" id="Seg_9239" s="T240">essen-CAUS-CVB.SIM</ta>
            <ta e="T242" id="Seg_9240" s="T241">Fenster-ABL</ta>
            <ta e="T243" id="Seg_9241" s="T242">essen-CAUS-CVB.SIM</ta>
            <ta e="T245" id="Seg_9242" s="T244">sehen.[IMP.2SG]</ta>
            <ta e="T246" id="Seg_9243" s="T245">jetzt</ta>
            <ta e="T247" id="Seg_9244" s="T246">jeder-3SG-ACC</ta>
            <ta e="T248" id="Seg_9245" s="T247">stehlen-FUT-3SG</ta>
            <ta e="T249" id="Seg_9246" s="T248">sagen-PRS.[3SG]</ta>
            <ta e="T251" id="Seg_9247" s="T250">oh.nein</ta>
            <ta e="T252" id="Seg_9248" s="T251">Bauch.[NOM]</ta>
            <ta e="T253" id="Seg_9249" s="T252">nur-1SG</ta>
            <ta e="T256" id="Seg_9250" s="T255">gehen.[IMP.2SG]</ta>
            <ta e="T257" id="Seg_9251" s="T256">gehen.[IMP.2SG]</ta>
            <ta e="T259" id="Seg_9252" s="T258">was.[NOM]</ta>
            <ta e="T260" id="Seg_9253" s="T259">INDEF</ta>
            <ta e="T261" id="Seg_9254" s="T260">Tuch-INSTR</ta>
            <ta e="T265" id="Seg_9255" s="T264">jenes-ABL</ta>
            <ta e="T266" id="Seg_9256" s="T265">gehen-NEG.[3SG]</ta>
            <ta e="T268" id="Seg_9257" s="T267">doch</ta>
            <ta e="T271" id="Seg_9258" s="T270">gehen.[IMP.2SG]</ta>
            <ta e="T273" id="Seg_9259" s="T272">geben-CVB.SEQ</ta>
            <ta e="T274" id="Seg_9260" s="T273">werfen-PRS.[3SG]</ta>
            <ta e="T275" id="Seg_9261" s="T274">EMPH</ta>
            <ta e="T277" id="Seg_9262" s="T276">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T278" id="Seg_9263" s="T277">mit.geschlossenem.Mund.kauen-CVB.SIM</ta>
            <ta e="T279" id="Seg_9264" s="T278">mit.geschlossenem.Mund.kauen-CVB.SIM</ta>
            <ta e="T281" id="Seg_9265" s="T280">gehen-FREQ-PRS.[3SG]</ta>
            <ta e="T282" id="Seg_9266" s="T281">sagen-CVB.SEQ</ta>
            <ta e="T283" id="Seg_9267" s="T282">zum.Glück</ta>
            <ta e="T284" id="Seg_9268" s="T283">Brot-ACC</ta>
            <ta e="T285" id="Seg_9269" s="T284">zum.Glück</ta>
            <ta e="T286" id="Seg_9270" s="T285">sich.satt.essen-PST1-3SG</ta>
            <ta e="T287" id="Seg_9271" s="T286">offenbar</ta>
            <ta e="T288" id="Seg_9272" s="T287">was.[NOM]</ta>
            <ta e="T289" id="Seg_9273" s="T288">so.viel</ta>
            <ta e="T290" id="Seg_9274" s="T289">sich.satt.essen-FUT.[3SG]=Q</ta>
            <ta e="T291" id="Seg_9275" s="T290">doch</ta>
            <ta e="T292" id="Seg_9276" s="T291">jenes</ta>
            <ta e="T293" id="Seg_9277" s="T292">knirschen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T296" id="Seg_9278" s="T295">wer-ACC</ta>
            <ta e="T297" id="Seg_9279" s="T296">geben-PST2.[3SG]</ta>
            <ta e="T298" id="Seg_9280" s="T297">trocken.werden-PTCP.PST</ta>
            <ta e="T299" id="Seg_9281" s="T298">Brot-ACC</ta>
            <ta e="T301" id="Seg_9282" s="T300">doch</ta>
            <ta e="T302" id="Seg_9283" s="T301">dort</ta>
            <ta e="T303" id="Seg_9284" s="T302">kauen-CVB.SIM</ta>
            <ta e="T304" id="Seg_9285" s="T303">versuchen-PST1-3SG</ta>
            <ta e="T305" id="Seg_9286" s="T304">EMPH</ta>
            <ta e="T307" id="Seg_9287" s="T306">oh.nein</ta>
            <ta e="T310" id="Seg_9288" s="T309">EXCL</ta>
            <ta e="T311" id="Seg_9289" s="T310">Zahn-PL-3SG-ACC</ta>
            <ta e="T312" id="Seg_9290" s="T311">ganz-3SG-ACC</ta>
            <ta e="T313" id="Seg_9291" s="T312">wer-VBZ-PST1-3SG</ta>
            <ta e="T314" id="Seg_9292" s="T313">offenbar</ta>
            <ta e="T316" id="Seg_9293" s="T315">doch</ta>
            <ta e="T317" id="Seg_9294" s="T316">jenes</ta>
            <ta e="T318" id="Seg_9295" s="T317">gehen-PST2-3SG</ta>
            <ta e="T319" id="Seg_9296" s="T318">zum.Glück</ta>
            <ta e="T320" id="Seg_9297" s="T319">Freund-PL-3SG-ACC</ta>
            <ta e="T321" id="Seg_9298" s="T320">zu</ta>
            <ta e="T323" id="Seg_9299" s="T322">hohes.Ufer.[NOM]</ta>
            <ta e="T324" id="Seg_9300" s="T323">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T325" id="Seg_9301" s="T324">hinuntergehen-PST2-3PL</ta>
            <ta e="T326" id="Seg_9302" s="T325">dort</ta>
            <ta e="T327" id="Seg_9303" s="T326">Fluss-DAT/LOC</ta>
            <ta e="T328" id="Seg_9304" s="T327">gehen-PRS-3PL</ta>
            <ta e="T330" id="Seg_9305" s="T329">Hof-1PL.[NOM]</ta>
            <ta e="T331" id="Seg_9306" s="T330">dunkel.werden-PST1-3SG</ta>
            <ta e="T332" id="Seg_9307" s="T331">dunkel.werden-PRS.[3SG]</ta>
            <ta e="T333" id="Seg_9308" s="T332">dunkel.werden-CVB.SEQ</ta>
            <ta e="T334" id="Seg_9309" s="T333">gehen-PRS.[3SG]</ta>
            <ta e="T336" id="Seg_9310" s="T335">wer.[NOM]</ta>
            <ta e="T337" id="Seg_9311" s="T336">Hund-PROPR.[NOM]</ta>
            <ta e="T338" id="Seg_9312" s="T337">sein-PST1-1PL</ta>
            <ta e="T339" id="Seg_9313" s="T338">1PL.[NOM]</ta>
            <ta e="T340" id="Seg_9314" s="T339">Djongo</ta>
            <ta e="T341" id="Seg_9315" s="T340">weiß</ta>
            <ta e="T342" id="Seg_9316" s="T341">sehr</ta>
            <ta e="T343" id="Seg_9317" s="T342">schön</ta>
            <ta e="T344" id="Seg_9318" s="T343">sehr</ta>
            <ta e="T345" id="Seg_9319" s="T344">Rentier-AG.[NOM]</ta>
            <ta e="T346" id="Seg_9320" s="T345">sehr</ta>
            <ta e="T347" id="Seg_9321" s="T346">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T349" id="Seg_9322" s="T348">wer.[NOM]</ta>
            <ta e="T351" id="Seg_9323" s="T350">sagen-FUT-2SG</ta>
            <ta e="T352" id="Seg_9324" s="T351">Q</ta>
            <ta e="T353" id="Seg_9325" s="T352">wer-ACC</ta>
            <ta e="T354" id="Seg_9326" s="T353">Herde-ACC</ta>
            <ta e="T355" id="Seg_9327" s="T354">ganz-3SG-ACC</ta>
            <ta e="T356" id="Seg_9328" s="T355">holen-CVB.SEQ</ta>
            <ta e="T357" id="Seg_9329" s="T356">kommen-FUT.[3SG]</ta>
            <ta e="T359" id="Seg_9330" s="T358">oh</ta>
            <ta e="T360" id="Seg_9331" s="T359">umdrehen-FREQ-FUT-3SG</ta>
            <ta e="T361" id="Seg_9332" s="T360">sagen-CVB.SEQ</ta>
            <ta e="T362" id="Seg_9333" s="T361">um.herum</ta>
            <ta e="T363" id="Seg_9334" s="T362">eins</ta>
            <ta e="T364" id="Seg_9335" s="T363">Ort-DAT/LOC</ta>
            <ta e="T365" id="Seg_9336" s="T364">stehen-FUT-3SG</ta>
            <ta e="T367" id="Seg_9337" s="T366">dieses</ta>
            <ta e="T368" id="Seg_9338" s="T367">holen.[IMP.2SG]</ta>
            <ta e="T369" id="Seg_9339" s="T368">sagen-PTCP.COND-DAT/LOC</ta>
            <ta e="T370" id="Seg_9340" s="T369">nun</ta>
            <ta e="T372" id="Seg_9341" s="T371">doch</ta>
            <ta e="T373" id="Seg_9342" s="T372">jenes-3SG-1PL.[NOM]</ta>
            <ta e="T374" id="Seg_9343" s="T373">wer-ACC</ta>
            <ta e="T375" id="Seg_9344" s="T374">wer-VBZ-PTCP.HAB</ta>
            <ta e="T376" id="Seg_9345" s="T375">sein-PST1-3SG</ta>
            <ta e="T377" id="Seg_9346" s="T376">vor.Kurzem</ta>
            <ta e="T378" id="Seg_9347" s="T377">Rentier-PL-EP-2SG.[NOM]</ta>
            <ta e="T379" id="Seg_9348" s="T378">es.gibt</ta>
            <ta e="T380" id="Seg_9349" s="T379">sein-TEMP-3PL</ta>
            <ta e="T381" id="Seg_9350" s="T380">nun</ta>
            <ta e="T382" id="Seg_9351" s="T381">Haus-DAT/LOC</ta>
            <ta e="T384" id="Seg_9352" s="T383">Rentierkalb.[NOM]</ta>
            <ta e="T385" id="Seg_9353" s="T384">einjähriges.Rentier.[NOM]</ta>
            <ta e="T386" id="Seg_9354" s="T385">Q</ta>
            <ta e="T387" id="Seg_9355" s="T386">was.[NOM]</ta>
            <ta e="T388" id="Seg_9356" s="T387">Q</ta>
            <ta e="T389" id="Seg_9357" s="T388">einjähriges.Rentier</ta>
            <ta e="T390" id="Seg_9358" s="T389">Rentierkalb.[NOM]</ta>
            <ta e="T391" id="Seg_9359" s="T390">äh</ta>
            <ta e="T392" id="Seg_9360" s="T391">groß</ta>
            <ta e="T393" id="Seg_9361" s="T392">Kalb-ABL</ta>
            <ta e="T394" id="Seg_9362" s="T393">groß-INTNS</ta>
            <ta e="T395" id="Seg_9363" s="T394">Jahr-3SG-INSTR</ta>
            <ta e="T396" id="Seg_9364" s="T395">nur</ta>
            <ta e="T397" id="Seg_9365" s="T396">groß</ta>
            <ta e="T399" id="Seg_9366" s="T398">jenes</ta>
            <ta e="T400" id="Seg_9367" s="T399">wer-ACC</ta>
            <ta e="T401" id="Seg_9368" s="T400">doch</ta>
            <ta e="T402" id="Seg_9369" s="T401">umdrehen-CVB.SIM</ta>
            <ta e="T403" id="Seg_9370" s="T402">sich.hinlegen-FUT-3SG</ta>
            <ta e="T404" id="Seg_9371" s="T403">EMPH</ta>
            <ta e="T405" id="Seg_9372" s="T404">quälen-CVB.SIM</ta>
            <ta e="T406" id="Seg_9373" s="T405">sich.hinlegen-HAB.[3SG]</ta>
            <ta e="T407" id="Seg_9374" s="T406">dieses.[NOM]</ta>
            <ta e="T408" id="Seg_9375" s="T407">was.[NOM]</ta>
            <ta e="T409" id="Seg_9376" s="T408">Kalb-DIM.[NOM]</ta>
            <ta e="T410" id="Seg_9377" s="T409">wollen-HAB.[3SG]</ta>
            <ta e="T411" id="Seg_9378" s="T410">MOD</ta>
            <ta e="T412" id="Seg_9379" s="T411">sich.bewegen-CVB.SIM</ta>
            <ta e="T413" id="Seg_9380" s="T412">sich.bewegen-CVB.SIM</ta>
            <ta e="T415" id="Seg_9381" s="T414">sich.hinlegen-FUT-3SG</ta>
            <ta e="T417" id="Seg_9382" s="T416">EMPH</ta>
            <ta e="T418" id="Seg_9383" s="T417">spielen-PRS.[3SG]</ta>
            <ta e="T419" id="Seg_9384" s="T418">EMPH</ta>
            <ta e="T420" id="Seg_9385" s="T419">3SG-ACC</ta>
            <ta e="T421" id="Seg_9386" s="T420">mit</ta>
            <ta e="T422" id="Seg_9387" s="T421">nun</ta>
            <ta e="T423" id="Seg_9388" s="T422">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T424" id="Seg_9389" s="T423">wie</ta>
            <ta e="T425" id="Seg_9390" s="T424">und</ta>
            <ta e="T426" id="Seg_9391" s="T425">sein-PST1-3SG</ta>
            <ta e="T427" id="Seg_9392" s="T1150">treffen-EP-RECP/COLL-EP-NMNZ.[NOM]</ta>
            <ta e="T428" id="Seg_9393" s="T427">treffen-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T429" id="Seg_9394" s="T428">dieses</ta>
            <ta e="T430" id="Seg_9395" s="T429">wie</ta>
            <ta e="T431" id="Seg_9396" s="T430">wie</ta>
            <ta e="T432" id="Seg_9397" s="T431">machen=Q</ta>
            <ta e="T433" id="Seg_9398" s="T432">Mama.[NOM]</ta>
            <ta e="T434" id="Seg_9399" s="T433">Mama.[NOM]</ta>
            <ta e="T435" id="Seg_9400" s="T434">sagen-PRS-1SG</ta>
            <ta e="T436" id="Seg_9401" s="T435">sehen.[IMP.2SG]</ta>
            <ta e="T437" id="Seg_9402" s="T436">sehen.[IMP.2SG]</ta>
            <ta e="T438" id="Seg_9403" s="T437">Djongo-ACC</ta>
            <ta e="T440" id="Seg_9404" s="T439">Choko</ta>
            <ta e="T441" id="Seg_9405" s="T440">Choko</ta>
            <ta e="T442" id="Seg_9406" s="T441">sagen-HAB-1SG</ta>
            <ta e="T444" id="Seg_9407" s="T443">Choko.[NOM]</ta>
            <ta e="T445" id="Seg_9408" s="T444">sehen.[IMP.2SG]</ta>
            <ta e="T447" id="Seg_9409" s="T446">wer-ACC</ta>
            <ta e="T448" id="Seg_9410" s="T447">essen-CVB.PURP</ta>
            <ta e="T449" id="Seg_9411" s="T448">wollen-PST1-3SG</ta>
            <ta e="T450" id="Seg_9412" s="T449">sagen-PRS-1SG</ta>
            <ta e="T451" id="Seg_9413" s="T450">Rentierkalb-ACC</ta>
            <ta e="T452" id="Seg_9414" s="T451">essen-CVB.PURP</ta>
            <ta e="T453" id="Seg_9415" s="T452">machen-PST1-3SG</ta>
            <ta e="T455" id="Seg_9416" s="T454">oh</ta>
            <ta e="T456" id="Seg_9417" s="T455">Satan</ta>
            <ta e="T457" id="Seg_9418" s="T456">rufen.[IMP.2SG]</ta>
            <ta e="T458" id="Seg_9419" s="T457">rufen.[IMP.2SG]</ta>
            <ta e="T460" id="Seg_9420" s="T459">müde.werden-CAUS-PST1-3SG</ta>
            <ta e="T461" id="Seg_9421" s="T460">offenbar</ta>
            <ta e="T462" id="Seg_9422" s="T461">dieses.[NOM]</ta>
            <ta e="T463" id="Seg_9423" s="T462">Rentierkalb-ACC</ta>
            <ta e="T464" id="Seg_9424" s="T463">erschrecken-PST1-3SG</ta>
            <ta e="T466" id="Seg_9425" s="T465">Choko</ta>
            <ta e="T467" id="Seg_9426" s="T466">Choko</ta>
            <ta e="T468" id="Seg_9427" s="T467">Choko</ta>
            <ta e="T469" id="Seg_9428" s="T468">sagen-PRS-1SG</ta>
            <ta e="T470" id="Seg_9429" s="T469">nach.draußen</ta>
            <ta e="T471" id="Seg_9430" s="T470">hinausgehen-CVB.SEQ</ta>
            <ta e="T472" id="Seg_9431" s="T471">gehen-CVB.SEQ</ta>
            <ta e="T473" id="Seg_9432" s="T472">nun</ta>
            <ta e="T475" id="Seg_9433" s="T474">oh</ta>
            <ta e="T476" id="Seg_9434" s="T475">doch</ta>
            <ta e="T477" id="Seg_9435" s="T476">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T478" id="Seg_9436" s="T477">rennen-CVB.SEQ</ta>
            <ta e="T479" id="Seg_9437" s="T478">gehen-EMOT-PRS.[3SG]</ta>
            <ta e="T480" id="Seg_9438" s="T479">sagen-CVB.SEQ</ta>
            <ta e="T482" id="Seg_9439" s="T481">doch</ta>
            <ta e="T483" id="Seg_9440" s="T482">springen</ta>
            <ta e="T484" id="Seg_9441" s="T483">doch</ta>
            <ta e="T485" id="Seg_9442" s="T484">Gesicht-1SG-DAT/LOC</ta>
            <ta e="T486" id="Seg_9443" s="T485">1SG.[NOM]</ta>
            <ta e="T487" id="Seg_9444" s="T486">auf.dem.Rücken.liegen-NMNZ.[NOM]</ta>
            <ta e="T490" id="Seg_9445" s="T489">auf.dem.Rücken</ta>
            <ta e="T491" id="Seg_9446" s="T490">auf.dem.Rücken.liegen-CVB.SIM</ta>
            <ta e="T492" id="Seg_9447" s="T491">liegen-EMOT-PRS-1SG</ta>
            <ta e="T493" id="Seg_9448" s="T492">sagen-CVB.SEQ</ta>
            <ta e="T495" id="Seg_9449" s="T494">AFFIRM</ta>
            <ta e="T496" id="Seg_9450" s="T495">sagen-PRS-1SG</ta>
            <ta e="T497" id="Seg_9451" s="T496">Mama.[NOM]</ta>
            <ta e="T498" id="Seg_9452" s="T497">nehmen.[IMP.2SG]</ta>
            <ta e="T499" id="Seg_9453" s="T498">doch</ta>
            <ta e="T500" id="Seg_9454" s="T499">EMPH</ta>
            <ta e="T502" id="Seg_9455" s="T501">lecken-PRS.[3SG]</ta>
            <ta e="T503" id="Seg_9456" s="T502">und.so.weiter-PRS.[3SG]</ta>
            <ta e="T504" id="Seg_9457" s="T503">sich.bewegen</ta>
            <ta e="T505" id="Seg_9458" s="T504">sich.bewegen-CAUS-PRS.[3SG]</ta>
            <ta e="T506" id="Seg_9459" s="T505">EMPH</ta>
            <ta e="T507" id="Seg_9460" s="T506">Mensch-ACC</ta>
            <ta e="T508" id="Seg_9461" s="T507">nun</ta>
            <ta e="T510" id="Seg_9462" s="T509">sich.bewegen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T511" id="Seg_9463" s="T510">wollen-PRS.[3SG]</ta>
            <ta e="T513" id="Seg_9464" s="T512">1SG-ACC</ta>
            <ta e="T514" id="Seg_9465" s="T513">schleppen-CVB.SIM</ta>
            <ta e="T515" id="Seg_9466" s="T514">sich.hinlegen-FREQ-PRS.[3SG]</ta>
            <ta e="T516" id="Seg_9467" s="T515">sagen-CVB.SEQ</ta>
            <ta e="T518" id="Seg_9468" s="T517">doch</ta>
            <ta e="T519" id="Seg_9469" s="T518">jenes</ta>
            <ta e="T520" id="Seg_9470" s="T519">Mama-1SG.[NOM]</ta>
            <ta e="T521" id="Seg_9471" s="T520">hinausgehen-PST1-3SG</ta>
            <ta e="T523" id="Seg_9472" s="T522">doch</ta>
            <ta e="T524" id="Seg_9473" s="T523">was.[NOM]</ta>
            <ta e="T525" id="Seg_9474" s="T524">werden-PRS-2SG</ta>
            <ta e="T527" id="Seg_9475" s="T526">sehen.[IMP.2SG]</ta>
            <ta e="T528" id="Seg_9476" s="T527">sagen-PRS-1SG</ta>
            <ta e="T529" id="Seg_9477" s="T528">1SG-ACC</ta>
            <ta e="T530" id="Seg_9478" s="T529">prügeln-PRS.[3SG]</ta>
            <ta e="T531" id="Seg_9479" s="T530">Q</ta>
            <ta e="T532" id="Seg_9480" s="T531">sagen-PRS-1SG</ta>
            <ta e="T533" id="Seg_9481" s="T532">spielen-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_9482" s="T533">Mensch-ACC</ta>
            <ta e="T535" id="Seg_9483" s="T534">nun</ta>
            <ta e="T536" id="Seg_9484" s="T535">oh.nein</ta>
            <ta e="T537" id="Seg_9485" s="T536">und</ta>
            <ta e="T539" id="Seg_9486" s="T538">%%-CVB.SEQ</ta>
            <ta e="T540" id="Seg_9487" s="T539">weinen-CVB.SEQ</ta>
            <ta e="T541" id="Seg_9488" s="T540">laut.schreien-PRS-1SG</ta>
            <ta e="T542" id="Seg_9489" s="T541">AFFIRM</ta>
            <ta e="T544" id="Seg_9490" s="T543">weinen.[IMP.2SG]</ta>
            <ta e="T545" id="Seg_9491" s="T544">weinen-NEG.[IMP.2SG]</ta>
            <ta e="T547" id="Seg_9492" s="T546">selbst-2SG.[NOM]</ta>
            <ta e="T548" id="Seg_9493" s="T547">rufen-PST2-EP-2SG</ta>
            <ta e="T549" id="Seg_9494" s="T548">sagen-PRS.[3SG]</ta>
            <ta e="T550" id="Seg_9495" s="T549">Mensch.[NOM]</ta>
            <ta e="T551" id="Seg_9496" s="T550">lachen-PTCP.FUT.[NOM]</ta>
            <ta e="T553" id="Seg_9497" s="T552">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T554" id="Seg_9498" s="T553">wieder</ta>
            <ta e="T555" id="Seg_9499" s="T554">gehen-CVB.SEQ</ta>
            <ta e="T556" id="Seg_9500" s="T555">bleiben-PST1-3SG</ta>
            <ta e="T557" id="Seg_9501" s="T556">jenes</ta>
            <ta e="T558" id="Seg_9502" s="T557">Kalb-DAT/LOC</ta>
            <ta e="T560" id="Seg_9503" s="T559">doch</ta>
            <ta e="T562" id="Seg_9504" s="T561">wer-VBZ-PST2.NEG-EP-1SG</ta>
            <ta e="T563" id="Seg_9505" s="T562">jenes</ta>
            <ta e="T564" id="Seg_9506" s="T563">immer</ta>
            <ta e="T565" id="Seg_9507" s="T564">Tag.[NOM]</ta>
            <ta e="T566" id="Seg_9508" s="T565">jeder</ta>
            <ta e="T567" id="Seg_9509" s="T566">spielen-HAB.[3SG]</ta>
            <ta e="T568" id="Seg_9510" s="T567">jenes</ta>
            <ta e="T569" id="Seg_9511" s="T568">Rentierkalb-ACC</ta>
            <ta e="T570" id="Seg_9512" s="T569">mit</ta>
            <ta e="T571" id="Seg_9513" s="T570">was-DAT/LOC</ta>
            <ta e="T572" id="Seg_9514" s="T571">wollen-PST2.[3SG]</ta>
            <ta e="T574" id="Seg_9515" s="T573">dieses.[NOM]</ta>
            <ta e="T575" id="Seg_9516" s="T574">Rentierkalb.[NOM]</ta>
            <ta e="T576" id="Seg_9517" s="T575">werden-PTCP.FUT-3SG-ACC</ta>
            <ta e="T577" id="Seg_9518" s="T576">man.muss</ta>
            <ta e="T579" id="Seg_9519" s="T578">doch</ta>
            <ta e="T580" id="Seg_9520" s="T579">jenes-ABL</ta>
            <ta e="T581" id="Seg_9521" s="T580">Hof-1PL.[NOM]</ta>
            <ta e="T582" id="Seg_9522" s="T581">dunkel.werden-PST1-3SG</ta>
            <ta e="T583" id="Seg_9523" s="T582">EMPH</ta>
            <ta e="T585" id="Seg_9524" s="T584">schön-ADVZ</ta>
            <ta e="T586" id="Seg_9525" s="T585">und</ta>
            <ta e="T587" id="Seg_9526" s="T586">wer-1PL.[NOM]</ta>
            <ta e="T588" id="Seg_9527" s="T587">Herd-1PL.[NOM]</ta>
            <ta e="T589" id="Seg_9528" s="T588">nun</ta>
            <ta e="T591" id="Seg_9529" s="T590">Mama-1SG.[NOM]</ta>
            <ta e="T592" id="Seg_9530" s="T591">genug</ta>
            <ta e="T593" id="Seg_9531" s="T592">legen-PST2.[3SG]</ta>
            <ta e="T598" id="Seg_9532" s="T597">wer.[NOM]</ta>
            <ta e="T599" id="Seg_9533" s="T598">Holz.[NOM]</ta>
            <ta e="T600" id="Seg_9534" s="T599">nun</ta>
            <ta e="T601" id="Seg_9535" s="T600">aufflammen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T602" id="Seg_9536" s="T601">nun</ta>
            <ta e="T603" id="Seg_9537" s="T602">rot.werden-INCH</ta>
            <ta e="T604" id="Seg_9538" s="T603">Herd-1PL.[NOM]</ta>
            <ta e="T606" id="Seg_9539" s="T605">Teekanne-PL.[NOM]</ta>
            <ta e="T607" id="Seg_9540" s="T606">sich.erheben-EP-REFL-CVB.SEQ</ta>
            <ta e="T609" id="Seg_9541" s="T608">zittern-EMOT-PRS-3PL</ta>
            <ta e="T610" id="Seg_9542" s="T609">sagen-CVB.SEQ</ta>
            <ta e="T611" id="Seg_9543" s="T610">Deckel-3PL.[NOM]</ta>
            <ta e="T613" id="Seg_9544" s="T612">Speise-1PL.[NOM]</ta>
            <ta e="T614" id="Seg_9545" s="T613">%%</ta>
            <ta e="T615" id="Seg_9546" s="T614">aufstehen-FREQ-PRS.[3SG]</ta>
            <ta e="T616" id="Seg_9547" s="T615">sagen-CVB.SEQ</ta>
            <ta e="T617" id="Seg_9548" s="T616">%%</ta>
            <ta e="T620" id="Seg_9549" s="T619">warm.werden</ta>
            <ta e="T621" id="Seg_9550" s="T620">Tür.[NOM]</ta>
            <ta e="T622" id="Seg_9551" s="T621">Fenster-ACC</ta>
            <ta e="T623" id="Seg_9552" s="T622">öffnen-PST1-3SG</ta>
            <ta e="T624" id="Seg_9553" s="T623">Rauchloch-3SG.[NOM]</ta>
            <ta e="T625" id="Seg_9554" s="T624">öffnen-PST1-3SG</ta>
            <ta e="T627" id="Seg_9555" s="T626">Rauchloch</ta>
            <ta e="T628" id="Seg_9556" s="T627">sein-HAB.[3SG]</ta>
            <ta e="T629" id="Seg_9557" s="T628">wer.[NOM]</ta>
            <ta e="T630" id="Seg_9558" s="T629">bedecken-HAB-3PL</ta>
            <ta e="T634" id="Seg_9559" s="T633">Wind.[NOM]</ta>
            <ta e="T635" id="Seg_9560" s="T634">hineingehen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T636" id="Seg_9561" s="T635">nun</ta>
            <ta e="T638" id="Seg_9562" s="T637">doch</ta>
            <ta e="T639" id="Seg_9563" s="T638">jenes</ta>
            <ta e="T640" id="Seg_9564" s="T639">öffnen-PST1-3SG</ta>
            <ta e="T641" id="Seg_9565" s="T640">AFFIRM</ta>
            <ta e="T643" id="Seg_9566" s="T642">1SG.[NOM]</ta>
            <ta e="T644" id="Seg_9567" s="T643">Q</ta>
            <ta e="T645" id="Seg_9568" s="T644">sehr</ta>
            <ta e="T646" id="Seg_9569" s="T645">lieben-HAB-1SG</ta>
            <ta e="T647" id="Seg_9570" s="T646">Fenster-DAT/LOC</ta>
            <ta e="T648" id="Seg_9571" s="T647">Fenster-DAT/LOC</ta>
            <ta e="T649" id="Seg_9572" s="T648">sehen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T650" id="Seg_9573" s="T649">nun</ta>
            <ta e="T652" id="Seg_9574" s="T651">breites.lachendes.Gesicht.haben-CVB.SIM</ta>
            <ta e="T653" id="Seg_9575" s="T652">breites.lachendes.Gesicht.haben-CVB.SIM</ta>
            <ta e="T654" id="Seg_9576" s="T653">sitzen-HAB-1SG</ta>
            <ta e="T656" id="Seg_9577" s="T655">schön-ADVZ</ta>
            <ta e="T657" id="Seg_9578" s="T656">EMPH</ta>
            <ta e="T658" id="Seg_9579" s="T657">AFFIRM</ta>
            <ta e="T659" id="Seg_9580" s="T658">wer.[NOM]</ta>
            <ta e="T660" id="Seg_9581" s="T659">Monat.[NOM]</ta>
            <ta e="T663" id="Seg_9582" s="T662">Monat.[NOM]</ta>
            <ta e="T664" id="Seg_9583" s="T663">Qual-DIM.[NOM]</ta>
            <ta e="T665" id="Seg_9584" s="T664">sein-PST2-3SG</ta>
            <ta e="T666" id="Seg_9585" s="T665">äh</ta>
            <ta e="T668" id="Seg_9586" s="T667">Mond-1PL.[NOM]</ta>
            <ta e="T669" id="Seg_9587" s="T668">schön.[NOM]</ta>
            <ta e="T670" id="Seg_9588" s="T669">sehr</ta>
            <ta e="T671" id="Seg_9589" s="T670">aber</ta>
            <ta e="T672" id="Seg_9590" s="T671">EMPH-dunkel.[NOM]</ta>
            <ta e="T674" id="Seg_9591" s="T673">was-PL.[NOM]</ta>
            <ta e="T675" id="Seg_9592" s="T674">fliegen-HAB-3PL</ta>
            <ta e="T676" id="Seg_9593" s="T675">sein-PST2.[3SG]</ta>
            <ta e="T677" id="Seg_9594" s="T676">1SG.[NOM]</ta>
            <ta e="T679" id="Seg_9595" s="T678">wissen-NEG.PTCP</ta>
            <ta e="T680" id="Seg_9596" s="T679">sein-PST1-1SG</ta>
            <ta e="T682" id="Seg_9597" s="T681">Schneeeule.[NOM]</ta>
            <ta e="T683" id="Seg_9598" s="T682">ähnlich</ta>
            <ta e="T684" id="Seg_9599" s="T683">Schneeeule-PL.[NOM]</ta>
            <ta e="T685" id="Seg_9600" s="T684">sagen-CVB.SEQ</ta>
            <ta e="T686" id="Seg_9601" s="T685">NEG-3SG</ta>
            <ta e="T687" id="Seg_9602" s="T686">klein-3PL</ta>
            <ta e="T689" id="Seg_9603" s="T688">Kopf-POSS</ta>
            <ta e="T690" id="Seg_9604" s="T689">NEG-3PL</ta>
            <ta e="T691" id="Seg_9605" s="T690">sagen-HAB-1SG</ta>
            <ta e="T692" id="Seg_9606" s="T691">Kopf-POSS</ta>
            <ta e="T693" id="Seg_9607" s="T692">NEG-3PL</ta>
            <ta e="T694" id="Seg_9608" s="T693">gehen-PRS-3PL</ta>
            <ta e="T695" id="Seg_9609" s="T694">Mama.[NOM]</ta>
            <ta e="T696" id="Seg_9610" s="T695">sehen.[IMP.2SG]</ta>
            <ta e="T699" id="Seg_9611" s="T698">dieses.[NOM]</ta>
            <ta e="T700" id="Seg_9612" s="T699">wer.[NOM]</ta>
            <ta e="T701" id="Seg_9613" s="T700">äh</ta>
            <ta e="T702" id="Seg_9614" s="T701">dieses</ta>
            <ta e="T703" id="Seg_9615" s="T702">Kohle.[NOM]</ta>
            <ta e="T704" id="Seg_9616" s="T703">fliegen-PRS.[3SG]</ta>
            <ta e="T705" id="Seg_9617" s="T704">EMPH</ta>
            <ta e="T706" id="Seg_9618" s="T705">Rohr-ABL</ta>
            <ta e="T707" id="Seg_9619" s="T706">nun</ta>
            <ta e="T709" id="Seg_9620" s="T708">jenes-DAT/LOC</ta>
            <ta e="T710" id="Seg_9621" s="T709">sich.bewegen-RECP/COLL-PRS-3PL</ta>
            <ta e="T711" id="Seg_9622" s="T710">sein-PST2.[3SG]</ta>
            <ta e="T712" id="Seg_9623" s="T711">dieses-PL-EP-2SG.[NOM]</ta>
            <ta e="T714" id="Seg_9624" s="T713">sehr</ta>
            <ta e="T716" id="Seg_9625" s="T715">jenes-ABL</ta>
            <ta e="T717" id="Seg_9626" s="T716">wie.viel</ta>
            <ta e="T718" id="Seg_9627" s="T717">INDEF</ta>
            <ta e="T719" id="Seg_9628" s="T718">Mama-1SG.[NOM]</ta>
            <ta e="T720" id="Seg_9629" s="T719">nähen</ta>
            <ta e="T721" id="Seg_9630" s="T720">sitzen-PRS.[3SG]</ta>
            <ta e="T722" id="Seg_9631" s="T721">nähen</ta>
            <ta e="T723" id="Seg_9632" s="T722">sitzen-PRS.[3SG]</ta>
            <ta e="T725" id="Seg_9633" s="T724">ganz.leise</ta>
            <ta e="T726" id="Seg_9634" s="T725">ganz.leise</ta>
            <ta e="T727" id="Seg_9635" s="T726">EMPH</ta>
            <ta e="T728" id="Seg_9636" s="T727">EMPH</ta>
            <ta e="T729" id="Seg_9637" s="T728">singen-CVB.SEQ</ta>
            <ta e="T730" id="Seg_9638" s="T729">sitzen-PRS.[3SG]</ta>
            <ta e="T731" id="Seg_9639" s="T730">ganz.leise</ta>
            <ta e="T733" id="Seg_9640" s="T732">Djongo-1PL.[NOM]</ta>
            <ta e="T734" id="Seg_9641" s="T733">liegen-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_9642" s="T735">n'ik</ta>
            <ta e="T737" id="Seg_9643" s="T736">NEG</ta>
            <ta e="T738" id="Seg_9644" s="T737">machen-NEG.[3SG]</ta>
            <ta e="T740" id="Seg_9645" s="T739">müde.werden-PST2.[3SG]</ta>
            <ta e="T741" id="Seg_9646" s="T740">offenbar</ta>
            <ta e="T742" id="Seg_9647" s="T741">Rentierkalb-3SG-ACC</ta>
            <ta e="T743" id="Seg_9648" s="T742">mit</ta>
            <ta e="T745" id="Seg_9649" s="T744">jenes-ABL</ta>
            <ta e="T746" id="Seg_9650" s="T745">wer.[NOM]</ta>
            <ta e="T749" id="Seg_9651" s="T748">gehen-PST1-3SG</ta>
            <ta e="T750" id="Seg_9652" s="T749">EMPH</ta>
            <ta e="T753" id="Seg_9653" s="T752">jenes</ta>
            <ta e="T754" id="Seg_9654" s="T753">was.[NOM]</ta>
            <ta e="T755" id="Seg_9655" s="T754">verschieden</ta>
            <ta e="T756" id="Seg_9656" s="T755">verschieden</ta>
            <ta e="T757" id="Seg_9657" s="T756">1SG.[NOM]</ta>
            <ta e="T759" id="Seg_9658" s="T758">Vater-1SG.[NOM]</ta>
            <ta e="T760" id="Seg_9659" s="T759">nein-INTNS-3SG-INSTR</ta>
            <ta e="T761" id="Seg_9660" s="T760">radio-PART</ta>
            <ta e="T762" id="Seg_9661" s="T761">anschalten-VBZ-EP-MED-PRS-1SG</ta>
            <ta e="T764" id="Seg_9662" s="T763">AFFIRM</ta>
            <ta e="T765" id="Seg_9663" s="T764">%%-EMOT-PRS-1SG</ta>
            <ta e="T766" id="Seg_9664" s="T765">doch</ta>
            <ta e="T768" id="Seg_9665" s="T767">AFFIRM</ta>
            <ta e="T769" id="Seg_9666" s="T768">ganz.leise</ta>
            <ta e="T770" id="Seg_9667" s="T769">machen.[IMP.2SG]</ta>
            <ta e="T771" id="Seg_9668" s="T770">selbst</ta>
            <ta e="T772" id="Seg_9669" s="T771">%%-EP-NEG.[IMP.2SG]</ta>
            <ta e="T774" id="Seg_9670" s="T773">Lied-PL-ACC</ta>
            <ta e="T775" id="Seg_9671" s="T774">zuhören-CVB.SIM</ta>
            <ta e="T776" id="Seg_9672" s="T775">liegen-FUT-1SG</ta>
            <ta e="T777" id="Seg_9673" s="T776">EMPH</ta>
            <ta e="T780" id="Seg_9674" s="T779">wer-PL-ACC</ta>
            <ta e="T781" id="Seg_9675" s="T780">doch</ta>
            <ta e="T782" id="Seg_9676" s="T781">zuhören-PRS-1SG</ta>
            <ta e="T784" id="Seg_9677" s="T783">Chinese-PL-PL.[NOM]</ta>
            <ta e="T785" id="Seg_9678" s="T784">was-PL.[NOM]</ta>
            <ta e="T786" id="Seg_9679" s="T785">Q</ta>
            <ta e="T788" id="Seg_9680" s="T787">jenes-PL-ACC</ta>
            <ta e="T789" id="Seg_9681" s="T788">zuhören-CVB.SEQ</ta>
            <ta e="T790" id="Seg_9682" s="T789">MOD</ta>
            <ta e="T791" id="Seg_9683" s="T790">liegen-FREQ-PRS-1SG</ta>
            <ta e="T792" id="Seg_9684" s="T791">sagen-CVB.SEQ</ta>
            <ta e="T793" id="Seg_9685" s="T792">dann</ta>
            <ta e="T794" id="Seg_9686" s="T793">fröhlich</ta>
            <ta e="T795" id="Seg_9687" s="T794">sehr</ta>
            <ta e="T796" id="Seg_9688" s="T795">wissen-PRS-1SG</ta>
            <ta e="T797" id="Seg_9689" s="T796">jenes-ABL</ta>
            <ta e="T798" id="Seg_9690" s="T797">zum.Glück</ta>
            <ta e="T799" id="Seg_9691" s="T798">Russe-PL.[NOM]</ta>
            <ta e="T800" id="Seg_9692" s="T799">finden-PST1-1SG</ta>
            <ta e="T801" id="Seg_9693" s="T800">jenes-ABL</ta>
            <ta e="T802" id="Seg_9694" s="T801">aufhören-CVB.SEQ</ta>
            <ta e="T803" id="Seg_9695" s="T802">bleiben-PST2.[3SG]</ta>
            <ta e="T805" id="Seg_9696" s="T804">doch</ta>
            <ta e="T806" id="Seg_9697" s="T805">aufhören-EP-CAUS-CVB.SEQ</ta>
            <ta e="T807" id="Seg_9698" s="T806">werfen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T808" id="Seg_9699" s="T807">aufhören-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T809" id="Seg_9700" s="T808">Vater-2SG.[NOM]</ta>
            <ta e="T810" id="Seg_9701" s="T809">kommen-FUT.[3SG]</ta>
            <ta e="T811" id="Seg_9702" s="T810">AFFIRM</ta>
            <ta e="T813" id="Seg_9703" s="T812">Sonne-1PL.[NOM]</ta>
            <ta e="T814" id="Seg_9704" s="T813">Mond-1PL.[NOM]</ta>
            <ta e="T815" id="Seg_9705" s="T814">sich.wegbewegen-PST1-3SG</ta>
            <ta e="T816" id="Seg_9706" s="T815">EMPH</ta>
            <ta e="T817" id="Seg_9707" s="T816">oben</ta>
            <ta e="T818" id="Seg_9708" s="T817">sein-PST1-3SG</ta>
            <ta e="T819" id="Seg_9709" s="T818">EMPH</ta>
            <ta e="T820" id="Seg_9710" s="T819">schön</ta>
            <ta e="T822" id="Seg_9711" s="T821">EMPH</ta>
            <ta e="T823" id="Seg_9712" s="T822">strahlend.[NOM]</ta>
            <ta e="T824" id="Seg_9713" s="T823">am.Tag</ta>
            <ta e="T825" id="Seg_9714" s="T824">ähnlich</ta>
            <ta e="T827" id="Seg_9715" s="T826">oh</ta>
            <ta e="T828" id="Seg_9716" s="T827">Rentier-PL-1PL.[NOM]</ta>
            <ta e="T830" id="Seg_9717" s="T829">Fluss.[NOM]</ta>
            <ta e="T831" id="Seg_9718" s="T830">entlang</ta>
            <ta e="T832" id="Seg_9719" s="T831">rennen-CVB.SIM</ta>
            <ta e="T833" id="Seg_9720" s="T832">gehen-FREQ-PRS-3PL</ta>
            <ta e="T834" id="Seg_9721" s="T833">doch</ta>
            <ta e="T835" id="Seg_9722" s="T834">EMPH</ta>
            <ta e="T836" id="Seg_9723" s="T835">schön-ADVZ</ta>
            <ta e="T837" id="Seg_9724" s="T836">EMPH</ta>
            <ta e="T839" id="Seg_9725" s="T838">wer.[NOM]</ta>
            <ta e="T840" id="Seg_9726" s="T839">Rauch-3PL.[NOM]</ta>
            <ta e="T841" id="Seg_9727" s="T840">sogar</ta>
            <ta e="T842" id="Seg_9728" s="T841">fliegen-PRS.[3SG]</ta>
            <ta e="T844" id="Seg_9729" s="T843">springen</ta>
            <ta e="T845" id="Seg_9730" s="T844">EMPH</ta>
            <ta e="T846" id="Seg_9731" s="T845">schön</ta>
            <ta e="T847" id="Seg_9732" s="T846">wer.[NOM]</ta>
            <ta e="T848" id="Seg_9733" s="T847">nun</ta>
            <ta e="T849" id="Seg_9734" s="T848">Schnee-EP-2SG.[NOM]</ta>
            <ta e="T850" id="Seg_9735" s="T849">nun</ta>
            <ta e="T851" id="Seg_9736" s="T850">wer-DAT/LOC</ta>
            <ta e="T852" id="Seg_9737" s="T851">Mond-ABL</ta>
            <ta e="T853" id="Seg_9738" s="T852">EMPH</ta>
            <ta e="T854" id="Seg_9739" s="T853">wer-VBZ-PRS.[3SG]</ta>
            <ta e="T855" id="Seg_9740" s="T854">glänzen-ITER-PRS.[3SG]</ta>
            <ta e="T857" id="Seg_9741" s="T856">jenes-ABL</ta>
            <ta e="T858" id="Seg_9742" s="T857">jenes-ABL</ta>
            <ta e="T859" id="Seg_9743" s="T858">warte</ta>
            <ta e="T860" id="Seg_9744" s="T859">Hund-1PL.[NOM]</ta>
            <ta e="T863" id="Seg_9745" s="T862">machen-PRS.[3SG]</ta>
            <ta e="T864" id="Seg_9746" s="T863">dieses</ta>
            <ta e="T865" id="Seg_9747" s="T864">wer-VBZ-PRS.[3SG]</ta>
            <ta e="T866" id="Seg_9748" s="T865">heulen-PRS.[3SG]</ta>
            <ta e="T868" id="Seg_9749" s="T867">INTJ</ta>
            <ta e="T869" id="Seg_9750" s="T868">Klingel.[NOM]</ta>
            <ta e="T870" id="Seg_9751" s="T869">klingen-PST1-3SG</ta>
            <ta e="T871" id="Seg_9752" s="T870">nur</ta>
            <ta e="T872" id="Seg_9753" s="T871">Halsglocke</ta>
            <ta e="T877" id="Seg_9754" s="T876">Vater-1PL.[NOM]</ta>
            <ta e="T878" id="Seg_9755" s="T877">gehen-PRS.[3SG]</ta>
            <ta e="T879" id="Seg_9756" s="T878">Vater-1PL.[NOM]</ta>
            <ta e="T880" id="Seg_9757" s="T879">AFFIRM</ta>
            <ta e="T882" id="Seg_9758" s="T881">doch</ta>
            <ta e="T883" id="Seg_9759" s="T882">jenes</ta>
            <ta e="T884" id="Seg_9760" s="T883">kommen-PST1-3SG</ta>
            <ta e="T886" id="Seg_9761" s="T885">1SG.[NOM]</ta>
            <ta e="T887" id="Seg_9762" s="T886">MOD</ta>
            <ta e="T888" id="Seg_9763" s="T887">wissen-PST2.NEG-EP-1SG</ta>
            <ta e="T889" id="Seg_9764" s="T888">EMPH</ta>
            <ta e="T890" id="Seg_9765" s="T889">Holz-VBZ-MED-CVB.SIM</ta>
            <ta e="T891" id="Seg_9766" s="T890">gehen-PST2.[3SG]</ta>
            <ta e="T892" id="Seg_9767" s="T891">denken-PRS-1SG</ta>
            <ta e="T893" id="Seg_9768" s="T892">EMPH</ta>
            <ta e="T895" id="Seg_9769" s="T894">3SG-2SG.[NOM]</ta>
            <ta e="T896" id="Seg_9770" s="T895">EMPH</ta>
            <ta e="T897" id="Seg_9771" s="T896">Dudinka.[NOM]</ta>
            <ta e="T898" id="Seg_9772" s="T897">zu</ta>
            <ta e="T899" id="Seg_9773" s="T898">gehen-EP-PST2.[3SG]</ta>
            <ta e="T900" id="Seg_9774" s="T899">und</ta>
            <ta e="T901" id="Seg_9775" s="T900">Dudinka.[NOM]</ta>
            <ta e="T902" id="Seg_9776" s="T901">zu</ta>
            <ta e="T904" id="Seg_9777" s="T903">doch</ta>
            <ta e="T905" id="Seg_9778" s="T904">jenes</ta>
            <ta e="T906" id="Seg_9779" s="T905">kommen-PST1-3SG</ta>
            <ta e="T909" id="Seg_9780" s="T908">dunkel.werden-PTCP.PST-3SG-ACC</ta>
            <ta e="T910" id="Seg_9781" s="T909">nachdem</ta>
            <ta e="T911" id="Seg_9782" s="T910">nun</ta>
            <ta e="T913" id="Seg_9783" s="T912">oh</ta>
            <ta e="T914" id="Seg_9784" s="T913">Vater-1SG.[NOM]</ta>
            <ta e="T915" id="Seg_9785" s="T914">kommen-PST1-3SG</ta>
            <ta e="T917" id="Seg_9786" s="T916">oh</ta>
            <ta e="T918" id="Seg_9787" s="T917">Mama-1SG.[NOM]</ta>
            <ta e="T919" id="Seg_9788" s="T918">helfen-CVB.SIM</ta>
            <ta e="T920" id="Seg_9789" s="T919">gehen-PST1-3SG</ta>
            <ta e="T921" id="Seg_9790" s="T920">losbinden-PST1-3SG</ta>
            <ta e="T922" id="Seg_9791" s="T921">und.so.weiter-PST1-3SG</ta>
            <ta e="T923" id="Seg_9792" s="T922">Halsglocke-ABL</ta>
            <ta e="T924" id="Seg_9793" s="T923">Rentier-3PL-ACC</ta>
            <ta e="T925" id="Seg_9794" s="T924">lassen-PST1-3PL</ta>
            <ta e="T926" id="Seg_9795" s="T925">und.so.weiter-PST1-3PL</ta>
            <ta e="T928" id="Seg_9796" s="T927">oh</ta>
            <ta e="T929" id="Seg_9797" s="T928">Vater-1SG.[NOM]</ta>
            <ta e="T930" id="Seg_9798" s="T929">Sack-PL-PROPR</ta>
            <ta e="T931" id="Seg_9799" s="T930">kommen-PST1-3SG</ta>
            <ta e="T933" id="Seg_9800" s="T932">Sack-PL-3SG-ACC</ta>
            <ta e="T934" id="Seg_9801" s="T933">hineingehen-CAUS-PST1-3SG</ta>
            <ta e="T936" id="Seg_9802" s="T935">doch</ta>
            <ta e="T937" id="Seg_9803" s="T936">herausnehmen-CAUS-FREQ-PST1-3SG</ta>
            <ta e="T938" id="Seg_9804" s="T937">oh</ta>
            <ta e="T939" id="Seg_9805" s="T938">doch</ta>
            <ta e="T940" id="Seg_9806" s="T939">Süßigkeit.[NOM]</ta>
            <ta e="T941" id="Seg_9807" s="T940">INTJ-3SG-ACC</ta>
            <ta e="T942" id="Seg_9808" s="T941">bringen-PST2.[3SG]</ta>
            <ta e="T944" id="Seg_9809" s="T943">jenes-ABL</ta>
            <ta e="T945" id="Seg_9810" s="T944">schön</ta>
            <ta e="T946" id="Seg_9811" s="T945">sehr</ta>
            <ta e="T947" id="Seg_9812" s="T946">immer</ta>
            <ta e="T948" id="Seg_9813" s="T947">erinnern-PRS-1SG</ta>
            <ta e="T950" id="Seg_9814" s="T949">wer.[NOM]</ta>
            <ta e="T951" id="Seg_9815" s="T950">Uhr-PL.[NOM]</ta>
            <ta e="T952" id="Seg_9816" s="T951">Q</ta>
            <ta e="T953" id="Seg_9817" s="T952">schön</ta>
            <ta e="T954" id="Seg_9818" s="T953">sehr</ta>
            <ta e="T955" id="Seg_9819" s="T954">wer.[NOM]</ta>
            <ta e="T956" id="Seg_9820" s="T955">Spiegel-INSTR</ta>
            <ta e="T957" id="Seg_9821" s="T956">gemacht.werden-EP-PTCP.PST</ta>
            <ta e="T959" id="Seg_9822" s="T958">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T961" id="Seg_9823" s="T960">Knopf-PL-PROPR.[NOM]</ta>
            <ta e="T962" id="Seg_9824" s="T961">was.[NOM]</ta>
            <ta e="T963" id="Seg_9825" s="T962">INDEF</ta>
            <ta e="T964" id="Seg_9826" s="T963">EMPH</ta>
            <ta e="T965" id="Seg_9827" s="T964">schön</ta>
            <ta e="T966" id="Seg_9828" s="T965">sich.wundern-EMOT-PRS-1SG</ta>
            <ta e="T967" id="Seg_9829" s="T966">EMPH</ta>
            <ta e="T968" id="Seg_9830" s="T967">schön</ta>
            <ta e="T970" id="Seg_9831" s="T969">doch</ta>
            <ta e="T971" id="Seg_9832" s="T970">berühren-EP-NEG.[IMP.2SG]</ta>
            <ta e="T972" id="Seg_9833" s="T971">in.Ordnung</ta>
            <ta e="T973" id="Seg_9834" s="T972">sein-FUT.[3SG]</ta>
            <ta e="T974" id="Seg_9835" s="T973">sagen-PRS-3PL</ta>
            <ta e="T976" id="Seg_9836" s="T975">berühren-CAUS-PTCP.HAB-3SG</ta>
            <ta e="T977" id="Seg_9837" s="T976">nein-3PL</ta>
            <ta e="T978" id="Seg_9838" s="T977">EMPH</ta>
            <ta e="T980" id="Seg_9839" s="T979">zerbrechen-FUT-2SG</ta>
            <ta e="T981" id="Seg_9840" s="T980">na</ta>
            <ta e="T982" id="Seg_9841" s="T981">AFFIRM</ta>
            <ta e="T984" id="Seg_9842" s="T983">doch</ta>
            <ta e="T985" id="Seg_9843" s="T984">legen-PST1-1SG</ta>
            <ta e="T986" id="Seg_9844" s="T985">sehr</ta>
            <ta e="T987" id="Seg_9845" s="T986">sehen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T988" id="Seg_9846" s="T987">wollen-PRS-1SG</ta>
            <ta e="T989" id="Seg_9847" s="T988">berühren-FREQ-PTCP.FUT-1SG-ACC</ta>
            <ta e="T990" id="Seg_9848" s="T989">wollen-PRS-1SG</ta>
            <ta e="T991" id="Seg_9849" s="T990">EMPH</ta>
            <ta e="T993" id="Seg_9850" s="T992">jenes-ABL</ta>
            <ta e="T994" id="Seg_9851" s="T993">wer.[NOM]</ta>
            <ta e="T995" id="Seg_9852" s="T994">wer-ACC</ta>
            <ta e="T996" id="Seg_9853" s="T995">bringen-PST2-3SG</ta>
            <ta e="T997" id="Seg_9854" s="T996">1SG-DAT/LOC</ta>
            <ta e="T999" id="Seg_9855" s="T998">Kompass-DIM.[NOM]</ta>
            <ta e="T1000" id="Seg_9856" s="T999">klein.[NOM]</ta>
            <ta e="T1002" id="Seg_9857" s="T1001">jenes-ACC</ta>
            <ta e="T1003" id="Seg_9858" s="T1002">immer</ta>
            <ta e="T1004" id="Seg_9859" s="T1003">spielen-CVB.SIM</ta>
            <ta e="T1005" id="Seg_9860" s="T1004">liegen-HAB-1SG</ta>
            <ta e="T1006" id="Seg_9861" s="T1005">oh.nein</ta>
            <ta e="T1007" id="Seg_9862" s="T1006">schön-ADVZ</ta>
            <ta e="T1008" id="Seg_9863" s="T1007">Q</ta>
            <ta e="T1009" id="Seg_9864" s="T1008">zuerst</ta>
            <ta e="T1012" id="Seg_9865" s="T1011">bringen-PST2-3SG</ta>
            <ta e="T1013" id="Seg_9866" s="T1012">dieses.[NOM]</ta>
            <ta e="T1014" id="Seg_9867" s="T1013">wie</ta>
            <ta e="T1015" id="Seg_9868" s="T1014">Spielzeug-ACC</ta>
            <ta e="T1016" id="Seg_9869" s="T1015">nun</ta>
            <ta e="T1018" id="Seg_9870" s="T1017">oh</ta>
            <ta e="T1019" id="Seg_9871" s="T1018">1SG.[NOM]</ta>
            <ta e="T1020" id="Seg_9872" s="T1019">meist</ta>
            <ta e="T1021" id="Seg_9873" s="T1020">NEG</ta>
            <ta e="T1023" id="Seg_9874" s="T1022">schön-ADVZ</ta>
            <ta e="T1024" id="Seg_9875" s="T1023">EMPH</ta>
            <ta e="T1026" id="Seg_9876" s="T1025">oh</ta>
            <ta e="T1027" id="Seg_9877" s="T1026">2SG-DAT/LOC</ta>
            <ta e="T1029" id="Seg_9878" s="T1028">sagen-PRS.[3SG]</ta>
            <ta e="T1031" id="Seg_9879" s="T1030">doch</ta>
            <ta e="T1032" id="Seg_9880" s="T1031">jenes</ta>
            <ta e="T1033" id="Seg_9881" s="T1032">sich.unterhalten-PRS-3PL</ta>
            <ta e="T1034" id="Seg_9882" s="T1033">offenbar</ta>
            <ta e="T1035" id="Seg_9883" s="T1034">nun</ta>
            <ta e="T1036" id="Seg_9884" s="T1035">1SG.[NOM]</ta>
            <ta e="T1037" id="Seg_9885" s="T1036">schlafen-CVB.SEQ</ta>
            <ta e="T1038" id="Seg_9886" s="T1037">bleiben-PST2-1SG</ta>
            <ta e="T1040" id="Seg_9887" s="T1039">am.Morgen</ta>
            <ta e="T1041" id="Seg_9888" s="T1040">schon</ta>
            <ta e="T1042" id="Seg_9889" s="T1041">aufwachen-PRS-1SG</ta>
            <ta e="T1043" id="Seg_9890" s="T1042">oh</ta>
            <ta e="T1044" id="Seg_9891" s="T1043">Herd.[NOM]</ta>
            <ta e="T1045" id="Seg_9892" s="T1044">wieder</ta>
            <ta e="T1046" id="Seg_9893" s="T1045">sich.erheben-EP-CAUS-CVB.SIM</ta>
            <ta e="T1047" id="Seg_9894" s="T1046">stehen-PRS.[3SG]</ta>
            <ta e="T1049" id="Seg_9895" s="T1048">schön-ADVZ</ta>
            <ta e="T1050" id="Seg_9896" s="T1049">Q</ta>
            <ta e="T1051" id="Seg_9897" s="T1050">wer.[NOM]</ta>
            <ta e="T1052" id="Seg_9898" s="T1051">NEG</ta>
            <ta e="T1053" id="Seg_9899" s="T1052">NEG</ta>
            <ta e="T1054" id="Seg_9900" s="T1053">Haus-1PL.[NOM]</ta>
            <ta e="T1055" id="Seg_9901" s="T1054">Inneres-3SG-DAT/LOC</ta>
            <ta e="T1057" id="Seg_9902" s="T1056">jenes</ta>
            <ta e="T1058" id="Seg_9903" s="T1057">Rentier-3PL-ACC</ta>
            <ta e="T1059" id="Seg_9904" s="T1058">greifen-PRS-PL</ta>
            <ta e="T1060" id="Seg_9905" s="T1059">sein-PST2.[3SG]</ta>
            <ta e="T1062" id="Seg_9906" s="T1061">ergreifen-CVB.SIM</ta>
            <ta e="T1063" id="Seg_9907" s="T1062">hinausgehen-EP-PST2-3PL</ta>
            <ta e="T1065" id="Seg_9908" s="T1064">Nachbar-PL-PROPR.[NOM]</ta>
            <ta e="T1066" id="Seg_9909" s="T1065">sein-PST1-1PL</ta>
            <ta e="T1067" id="Seg_9910" s="T1066">so</ta>
            <ta e="T1068" id="Seg_9911" s="T1067">INDEF</ta>
            <ta e="T1069" id="Seg_9912" s="T1068">es.gibt</ta>
            <ta e="T1070" id="Seg_9913" s="T1069">sein-PST1-3PL</ta>
            <ta e="T1071" id="Seg_9914" s="T1070">Nachbar-PL-1PL.[NOM]</ta>
            <ta e="T1073" id="Seg_9915" s="T1072">doch</ta>
            <ta e="T1074" id="Seg_9916" s="T1073">jenes</ta>
            <ta e="T1075" id="Seg_9917" s="T1074">sammeln-PST1-3PL</ta>
            <ta e="T1076" id="Seg_9918" s="T1075">Herde-3PL-ACC</ta>
            <ta e="T1077" id="Seg_9919" s="T1076">sammeln-MED-PRS-3PL</ta>
            <ta e="T1078" id="Seg_9920" s="T1077">Rentier.[NOM]</ta>
            <ta e="T1079" id="Seg_9921" s="T1078">was.[NOM]</ta>
            <ta e="T1080" id="Seg_9922" s="T1079">nomadisieren-PRS-1PL</ta>
            <ta e="T1081" id="Seg_9923" s="T1080">offenbar</ta>
            <ta e="T1082" id="Seg_9924" s="T1081">erinnern-PRS-1SG</ta>
            <ta e="T1083" id="Seg_9925" s="T1082">wohin</ta>
            <ta e="T1084" id="Seg_9926" s="T1083">INDEF</ta>
            <ta e="T1086" id="Seg_9927" s="T1085">oh</ta>
            <ta e="T1087" id="Seg_9928" s="T1086">doch</ta>
            <ta e="T1088" id="Seg_9929" s="T1087">1SG-ACC</ta>
            <ta e="T1089" id="Seg_9930" s="T1088">sich.anziehen-CAUS-CVB.SEQ</ta>
            <ta e="T1090" id="Seg_9931" s="T1089">%%-PST1-3PL</ta>
            <ta e="T1092" id="Seg_9932" s="T1091">Schlitten-DAT/LOC</ta>
            <ta e="T1093" id="Seg_9933" s="T1092">binden-CVB.SEQ</ta>
            <ta e="T1094" id="Seg_9934" s="T1093">werfen-PST2-3PL</ta>
            <ta e="T1095" id="Seg_9935" s="T1094">%%-CVB.SEQ</ta>
            <ta e="T1096" id="Seg_9936" s="T1095">werfen-PST2-3PL</ta>
            <ta e="T1098" id="Seg_9937" s="T1097">doch</ta>
            <ta e="T1099" id="Seg_9938" s="T1098">wohin</ta>
            <ta e="T1100" id="Seg_9939" s="T1099">gehen-PRS-1PL</ta>
            <ta e="T1102" id="Seg_9940" s="T1101">und</ta>
            <ta e="T1103" id="Seg_9941" s="T1102">Hinterteil-1PL-ABL</ta>
            <ta e="T1104" id="Seg_9942" s="T1103">Q</ta>
            <ta e="T1105" id="Seg_9943" s="T1104">Rentier-PL-ACC</ta>
            <ta e="T1106" id="Seg_9944" s="T1105">führen-HAB-3PL</ta>
            <ta e="T1107" id="Seg_9945" s="T1106">Frachtschlitten-DAT/LOC</ta>
            <ta e="T1109" id="Seg_9946" s="T1108">Nahrung-PROPR</ta>
            <ta e="T1110" id="Seg_9947" s="T1109">oder</ta>
            <ta e="T1111" id="Seg_9948" s="T1110">Holz-PROPR</ta>
            <ta e="T1112" id="Seg_9949" s="T1111">Frachtschlitten.[NOM]</ta>
            <ta e="T1113" id="Seg_9950" s="T1112">führen-PTCP.HAB</ta>
            <ta e="T1114" id="Seg_9951" s="T1113">sein-PST1-1PL</ta>
            <ta e="T1116" id="Seg_9952" s="T1115">jenes-PL-ABL</ta>
            <ta e="T1117" id="Seg_9953" s="T1116">Angst.haben-CVB.SEQ</ta>
            <ta e="T1118" id="Seg_9954" s="T1117">weinen-CVB.SEQ</ta>
            <ta e="T1119" id="Seg_9955" s="T1118">laut.schreien-PRS-1SG</ta>
            <ta e="T1120" id="Seg_9956" s="T1119">sagen-CVB.SEQ</ta>
            <ta e="T1122" id="Seg_9957" s="T1121">Angst.haben-EP-NEG.[IMP.2SG]</ta>
            <ta e="T1123" id="Seg_9958" s="T1122">beissen-FUT.[3SG]</ta>
            <ta e="T1124" id="Seg_9959" s="T1123">NEG-3SG</ta>
            <ta e="T1126" id="Seg_9960" s="T1125">dieses</ta>
            <ta e="T1127" id="Seg_9961" s="T1126">nun</ta>
            <ta e="T1128" id="Seg_9962" s="T1127">müde.werden-PST2-3PL</ta>
            <ta e="T1129" id="Seg_9963" s="T1128">nun</ta>
            <ta e="T1130" id="Seg_9964" s="T1129">atmen-PRS-3PL</ta>
            <ta e="T1133" id="Seg_9965" s="T1132">Gesicht-1SG-DAT/LOC</ta>
            <ta e="T1134" id="Seg_9966" s="T1133">hier</ta>
            <ta e="T1136" id="Seg_9967" s="T1135">doch</ta>
            <ta e="T1137" id="Seg_9968" s="T1136">jenes</ta>
            <ta e="T1138" id="Seg_9969" s="T1137">dieses.[NOM]</ta>
            <ta e="T1139" id="Seg_9970" s="T1138">wie</ta>
            <ta e="T1140" id="Seg_9971" s="T1139">nomadisieren-CVB.SIM</ta>
            <ta e="T1141" id="Seg_9972" s="T1140">gehen-PTCP.HAB</ta>
            <ta e="T1142" id="Seg_9973" s="T1141">sein-PST1-1PL</ta>
            <ta e="T1143" id="Seg_9974" s="T1142">nomadisieren-CVB.SIM</ta>
            <ta e="T1144" id="Seg_9975" s="T1143">gehen-PTCP.HAB</ta>
            <ta e="T1145" id="Seg_9976" s="T1144">wegfahren-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T1146" id="Seg_9977" s="T1145">gehen-PTCP.HAB</ta>
            <ta e="T1147" id="Seg_9978" s="T1146">sein-PST1-1SG</ta>
            <ta e="T1148" id="Seg_9979" s="T1147">3PL-ACC</ta>
            <ta e="T1149" id="Seg_9980" s="T1148">mit</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_9981" s="T1">однажды</ta>
            <ta e="T3" id="Seg_9982" s="T2">помнить-PRS-1SG</ta>
            <ta e="T4" id="Seg_9983" s="T3">маленький.[NOM]</ta>
            <ta e="T5" id="Seg_9984" s="T4">быть-TEMP-1SG</ta>
            <ta e="T6" id="Seg_9985" s="T5">Наһоӈ-DAT/LOC</ta>
            <ta e="T7" id="Seg_9986" s="T6">точка-1PL.[NOM]</ta>
            <ta e="T8" id="Seg_9987" s="T7">Кресты-ABL</ta>
            <ta e="T9" id="Seg_9988" s="T8">десять</ta>
            <ta e="T10" id="Seg_9989" s="T9">два</ta>
            <ta e="T11" id="Seg_9990" s="T10">километр.[NOM]</ta>
            <ta e="T12" id="Seg_9991" s="T11">там</ta>
            <ta e="T13" id="Seg_9992" s="T12">сколько-PROPR</ta>
            <ta e="T14" id="Seg_9993" s="T13">человек.[NOM]</ta>
            <ta e="T15" id="Seg_9994" s="T14">жить-PST2-3SG=Q</ta>
            <ta e="T17" id="Seg_9995" s="T16">старый-PL.[NOM]</ta>
            <ta e="T18" id="Seg_9996" s="T17">каждый-3PL.[NOM]</ta>
            <ta e="T19" id="Seg_9997" s="T18">там</ta>
            <ta e="T20" id="Seg_9998" s="T19">жить-PTCP.HAB</ta>
            <ta e="T21" id="Seg_9999" s="T20">быть-PST1-3PL</ta>
            <ta e="T23" id="Seg_10000" s="T22">сани.[NOM]</ta>
            <ta e="T24" id="Seg_10001" s="T23">дом.[NOM]</ta>
            <ta e="T25" id="Seg_10002" s="T24">полный.[NOM]</ta>
            <ta e="T26" id="Seg_10003" s="T25">быть-PST1-3SG</ta>
            <ta e="T27" id="Seg_10004" s="T26">там</ta>
            <ta e="T29" id="Seg_10005" s="T28">олень-PL.[NOM]</ta>
            <ta e="T30" id="Seg_10006" s="T29">что-PL.[NOM]</ta>
            <ta e="T31" id="Seg_10007" s="T30">есть</ta>
            <ta e="T32" id="Seg_10008" s="T31">быть-PST1-3PL</ta>
            <ta e="T34" id="Seg_10009" s="T33">тот-ABL</ta>
            <ta e="T35" id="Seg_10010" s="T34">однажды</ta>
            <ta e="T36" id="Seg_10011" s="T35">отец-1SG-PROPR.[NOM]</ta>
            <ta e="T37" id="Seg_10012" s="T36">дом-3PL-DAT/LOC</ta>
            <ta e="T38" id="Seg_10013" s="T37">отец-1SG.[NOM]</ta>
            <ta e="T39" id="Seg_10014" s="T38">старый</ta>
            <ta e="T40" id="Seg_10015" s="T39">дедушка-1SG-ACC</ta>
            <ta e="T41" id="Seg_10016" s="T40">говорить-HAB-1SG</ta>
            <ta e="T42" id="Seg_10017" s="T41">EMPH</ta>
            <ta e="T43" id="Seg_10018" s="T42">отец</ta>
            <ta e="T44" id="Seg_10019" s="T43">говорить-HAB-1SG</ta>
            <ta e="T45" id="Seg_10020" s="T44">старый</ta>
            <ta e="T46" id="Seg_10021" s="T45">мама-1SG-ACC</ta>
            <ta e="T47" id="Seg_10022" s="T46">мама.[NOM]</ta>
            <ta e="T48" id="Seg_10023" s="T47">говорить-HAB-1SG</ta>
            <ta e="T49" id="Seg_10024" s="T48">старый</ta>
            <ta e="T50" id="Seg_10025" s="T49">бабушка-1SG-ACC</ta>
            <ta e="T51" id="Seg_10026" s="T50">3PL-DAT/LOC</ta>
            <ta e="T52" id="Seg_10027" s="T51">кормить-EP-PASS/REFL-EP-PST2-EP-1SG</ta>
            <ta e="T54" id="Seg_10028" s="T53">вот</ta>
            <ta e="T55" id="Seg_10029" s="T54">тот</ta>
            <ta e="T56" id="Seg_10030" s="T55">отец-1PL.[NOM]</ta>
            <ta e="T57" id="Seg_10031" s="T56">куда</ta>
            <ta e="T58" id="Seg_10032" s="T57">INDEF</ta>
            <ta e="T59" id="Seg_10033" s="T58">идти-CVB.SEQ</ta>
            <ta e="T60" id="Seg_10034" s="T59">оставаться-PST1-3SG</ta>
            <ta e="T62" id="Seg_10035" s="T61">дерево-VBZ-CVB.SIM</ta>
            <ta e="T63" id="Seg_10036" s="T62">идти-PST1-3SG</ta>
            <ta e="T64" id="Seg_10037" s="T63">очевидно</ta>
            <ta e="T65" id="Seg_10038" s="T64">вот</ta>
            <ta e="T66" id="Seg_10039" s="T65">сидеть-PRS-1PL</ta>
            <ta e="T67" id="Seg_10040" s="T66">олень-PL-1PL.[NOM]</ta>
            <ta e="T69" id="Seg_10041" s="T68">кто-DAT/LOC</ta>
            <ta e="T70" id="Seg_10042" s="T69">дом-1PL.[NOM]</ta>
            <ta e="T71" id="Seg_10043" s="T70">край-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_10044" s="T71">идти-PRS-3PL</ta>
            <ta e="T74" id="Seg_10045" s="T73">скрести.копытами-CVB.SIM</ta>
            <ta e="T75" id="Seg_10046" s="T74">идти-PRS-3PL</ta>
            <ta e="T78" id="Seg_10047" s="T77">ручной.олень-PROPR.[NOM]</ta>
            <ta e="T79" id="Seg_10048" s="T78">быть-PST1-1PL</ta>
            <ta e="T81" id="Seg_10049" s="T80">этот</ta>
            <ta e="T82" id="Seg_10050" s="T81">рука-ABL</ta>
            <ta e="T83" id="Seg_10051" s="T82">есть-PRS.[3SG]</ta>
            <ta e="T85" id="Seg_10052" s="T84">хлеб-EP-PHIL.[NOM]</ta>
            <ta e="T86" id="Seg_10053" s="T85">очень</ta>
            <ta e="T88" id="Seg_10054" s="T87">окно-DAT/LOC</ta>
            <ta e="T89" id="Seg_10055" s="T88">приходить-HAB.[3SG]</ta>
            <ta e="T90" id="Seg_10056" s="T89">хлеб.[NOM]</ta>
            <ta e="T91" id="Seg_10057" s="T90">искать-CVB.SIM</ta>
            <ta e="T93" id="Seg_10058" s="T92">о</ta>
            <ta e="T94" id="Seg_10059" s="T93">нос-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_10060" s="T94">быть.близким.и.большим-CVB.SEQ</ta>
            <ta e="T96" id="Seg_10061" s="T95">приходить-FUT.[3SG]</ta>
            <ta e="T97" id="Seg_10062" s="T96">говорить-CVB.SEQ</ta>
            <ta e="T98" id="Seg_10063" s="T97">о</ta>
            <ta e="T99" id="Seg_10064" s="T98">испугаться-PRS-1SG</ta>
            <ta e="T100" id="Seg_10065" s="T99">маленький.[NOM]</ta>
            <ta e="T101" id="Seg_10066" s="T100">человек.[NOM]</ta>
            <ta e="T102" id="Seg_10067" s="T101">вот</ta>
            <ta e="T103" id="Seg_10068" s="T102">бояться-PTCP.HAB</ta>
            <ta e="T104" id="Seg_10069" s="T103">быть-PST1-1SG</ta>
            <ta e="T105" id="Seg_10070" s="T104">EMPH</ta>
            <ta e="T108" id="Seg_10071" s="T107">этот</ta>
            <ta e="T109" id="Seg_10072" s="T108">человек.[NOM]</ta>
            <ta e="T110" id="Seg_10073" s="T109">скрывать-CVB.SEQ</ta>
            <ta e="T111" id="Seg_10074" s="T110">открывать-PST1-1SG</ta>
            <ta e="T112" id="Seg_10075" s="T111">EMPH</ta>
            <ta e="T114" id="Seg_10076" s="T113">стол-ABL</ta>
            <ta e="T115" id="Seg_10077" s="T114">этот</ta>
            <ta e="T116" id="Seg_10078" s="T115">хлеб.[NOM]</ta>
            <ta e="T117" id="Seg_10079" s="T116">лепешка.[NOM]</ta>
            <ta e="T118" id="Seg_10080" s="T117">очень-PL</ta>
            <ta e="T120" id="Seg_10081" s="T119">о</ta>
            <ta e="T121" id="Seg_10082" s="T120">тот-3SG-1SG.[NOM]</ta>
            <ta e="T122" id="Seg_10083" s="T121">возбуждаться-PST2.[3SG]</ta>
            <ta e="T123" id="Seg_10084" s="T122">есть-CVB.SIM</ta>
            <ta e="T124" id="Seg_10085" s="T123">есть-EMOT-PRS.[3SG]</ta>
            <ta e="T125" id="Seg_10086" s="T124">говорить-CVB.SEQ</ta>
            <ta e="T126" id="Seg_10087" s="T125">вот.беда</ta>
            <ta e="T128" id="Seg_10088" s="T127">вот</ta>
            <ta e="T129" id="Seg_10089" s="T128">тот</ta>
            <ta e="T130" id="Seg_10090" s="T129">мама-1SG.[NOM]</ta>
            <ta e="T131" id="Seg_10091" s="T130">говорить-PRS.[3SG]</ta>
            <ta e="T132" id="Seg_10092" s="T131">старый</ta>
            <ta e="T133" id="Seg_10093" s="T132">мама-1SG.[NOM]</ta>
            <ta e="T134" id="Seg_10094" s="T133">вот</ta>
            <ta e="T136" id="Seg_10095" s="T135">о</ta>
            <ta e="T137" id="Seg_10096" s="T136">учить-EP-NEG.[IMP.2SG]</ta>
            <ta e="T138" id="Seg_10097" s="T137">вот</ta>
            <ta e="T139" id="Seg_10098" s="T138">2SG.[NOM]</ta>
            <ta e="T140" id="Seg_10099" s="T139">окно-1PL-ACC</ta>
            <ta e="T141" id="Seg_10100" s="T140">разбивать-FUT.[3SG]</ta>
            <ta e="T142" id="Seg_10101" s="T141">и.так.далее-FUT.[3SG]</ta>
            <ta e="T144" id="Seg_10102" s="T143">идти.[IMP.2SG]</ta>
            <ta e="T145" id="Seg_10103" s="T144">идти.[IMP.2SG]</ta>
            <ta e="T146" id="Seg_10104" s="T145">идти.[IMP.2SG]</ta>
            <ta e="T147" id="Seg_10105" s="T146">приходить-EP-NEG.[IMP.2SG]</ta>
            <ta e="T149" id="Seg_10106" s="T148">говорить-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_10107" s="T149">что.[NOM]</ta>
            <ta e="T151" id="Seg_10108" s="T150">NEG</ta>
            <ta e="T152" id="Seg_10109" s="T151">NEG.EX</ta>
            <ta e="T154" id="Seg_10110" s="T153">трава-2SG-ACC</ta>
            <ta e="T155" id="Seg_10111" s="T154">есть.[IMP.2SG]</ta>
            <ta e="T156" id="Seg_10112" s="T155">идти.[IMP.2SG]</ta>
            <ta e="T157" id="Seg_10113" s="T156">говорить-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_10114" s="T157">AFFIRM</ta>
            <ta e="T159" id="Seg_10115" s="T158">закрывать-CVB.SEQ</ta>
            <ta e="T160" id="Seg_10116" s="T159">бросать-PST1-3SG</ta>
            <ta e="T162" id="Seg_10117" s="T161">тот-ABL</ta>
            <ta e="T163" id="Seg_10118" s="T162">другой</ta>
            <ta e="T164" id="Seg_10119" s="T163">окно-EP-INSTR</ta>
            <ta e="T165" id="Seg_10120" s="T164">приходить-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_10121" s="T165">там</ta>
            <ta e="T168" id="Seg_10122" s="T167">быть-HAB.[3SG]</ta>
            <ta e="T169" id="Seg_10123" s="T168">EMPH</ta>
            <ta e="T171" id="Seg_10124" s="T170">там</ta>
            <ta e="T172" id="Seg_10125" s="T171">лед.[NOM]</ta>
            <ta e="T173" id="Seg_10126" s="T172">что.[NOM]</ta>
            <ta e="T174" id="Seg_10127" s="T173">класть-EP-MED-HAB-1PL</ta>
            <ta e="T176" id="Seg_10128" s="T175">этот</ta>
            <ta e="T177" id="Seg_10129" s="T176">вот</ta>
            <ta e="T178" id="Seg_10130" s="T177">вода-1PL.[NOM]</ta>
            <ta e="T179" id="Seg_10131" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_10132" s="T179">лед-1PL.[NOM]</ta>
            <ta e="T182" id="Seg_10133" s="T181">там</ta>
            <ta e="T183" id="Seg_10134" s="T182">лезть-CVB.SIM</ta>
            <ta e="T184" id="Seg_10135" s="T183">лезть-CVB.SIM</ta>
            <ta e="T185" id="Seg_10136" s="T184">окно-1PL.[NOM]</ta>
            <ta e="T186" id="Seg_10137" s="T185">открывать-PASS/REFL-CVB.SEQ</ta>
            <ta e="T187" id="Seg_10138" s="T186">стоять-PRS.[3SG]</ta>
            <ta e="T188" id="Seg_10139" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_10140" s="T188">передний-1PL.[NOM]</ta>
            <ta e="T190" id="Seg_10141" s="T189">вот</ta>
            <ta e="T192" id="Seg_10142" s="T191">там</ta>
            <ta e="T193" id="Seg_10143" s="T192">иметь.длинное.лицо-CVB.SIM</ta>
            <ta e="T194" id="Seg_10144" s="T193">иметь.длинное.лицо-CVB.SIM</ta>
            <ta e="T195" id="Seg_10145" s="T194">рог-PL-PROPR.[NOM]</ta>
            <ta e="T196" id="Seg_10146" s="T195">EMPH</ta>
            <ta e="T198" id="Seg_10147" s="T197">рог-PL-3SG.[NOM]</ta>
            <ta e="T199" id="Seg_10148" s="T198">подходить-NEG-3PL</ta>
            <ta e="T200" id="Seg_10149" s="T199">кто.[NOM]</ta>
            <ta e="T201" id="Seg_10150" s="T200">место.для.посуды-DAT/LOC</ta>
            <ta e="T202" id="Seg_10151" s="T201">лепешка.[NOM]</ta>
            <ta e="T203" id="Seg_10152" s="T202">стоять-PTCP.PRS</ta>
            <ta e="T204" id="Seg_10153" s="T203">быть-PST1-3SG</ta>
            <ta e="T207" id="Seg_10154" s="T206">кто-VBZ-PTCP.PST</ta>
            <ta e="T208" id="Seg_10155" s="T207">резать-EP-PASS/REFL-EP-PTCP.PST-PL.[NOM]</ta>
            <ta e="T210" id="Seg_10156" s="T209">вот</ta>
            <ta e="T211" id="Seg_10157" s="T210">тот-ACC</ta>
            <ta e="T212" id="Seg_10158" s="T211">доезжать-CVB.SIM</ta>
            <ta e="T213" id="Seg_10159" s="T212">пытаться-PST1-3SG</ta>
            <ta e="T214" id="Seg_10160" s="T213">EMPH</ta>
            <ta e="T215" id="Seg_10161" s="T214">язык-3SG-ACC</ta>
            <ta e="T216" id="Seg_10162" s="T215">протягивать-CVB.SIM</ta>
            <ta e="T217" id="Seg_10163" s="T216">протягивать-CVB.SIM</ta>
            <ta e="T219" id="Seg_10164" s="T218">мама.[NOM]</ta>
            <ta e="T220" id="Seg_10165" s="T219">видеть.[IMP.2SG]</ta>
            <ta e="T221" id="Seg_10166" s="T220">видеть.[IMP.2SG]</ta>
            <ta e="T222" id="Seg_10167" s="T221">видеть.[IMP.2SG]</ta>
            <ta e="T223" id="Seg_10168" s="T222">говорить-PRS-1SG</ta>
            <ta e="T224" id="Seg_10169" s="T223">кто-2SG-ACC</ta>
            <ta e="T225" id="Seg_10170" s="T224">есть-CVB.PURP</ta>
            <ta e="T226" id="Seg_10171" s="T225">делать-PST1-3SG</ta>
            <ta e="T227" id="Seg_10172" s="T226">хлеб-PL-2SG-ACC</ta>
            <ta e="T228" id="Seg_10173" s="T227">есть-CVB.PURP</ta>
            <ta e="T229" id="Seg_10174" s="T228">делать-PST1-3SG</ta>
            <ta e="T230" id="Seg_10175" s="T229">говорить-PRS-1SG</ta>
            <ta e="T231" id="Seg_10176" s="T230">хлеб-PL-2SG-ACC</ta>
            <ta e="T233" id="Seg_10177" s="T232">EXCL</ta>
            <ta e="T234" id="Seg_10178" s="T233">почему</ta>
            <ta e="T235" id="Seg_10179" s="T234">сатана</ta>
            <ta e="T236" id="Seg_10180" s="T235">дочь-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_10181" s="T236">почему</ta>
            <ta e="T238" id="Seg_10182" s="T237">учить-PST2-2SG=Q</ta>
            <ta e="T239" id="Seg_10183" s="T238">EMPH</ta>
            <ta e="T240" id="Seg_10184" s="T239">дом-ABL</ta>
            <ta e="T241" id="Seg_10185" s="T240">есть-CAUS-CVB.SIM</ta>
            <ta e="T242" id="Seg_10186" s="T241">окно-ABL</ta>
            <ta e="T243" id="Seg_10187" s="T242">есть-CAUS-CVB.SIM</ta>
            <ta e="T245" id="Seg_10188" s="T244">видеть.[IMP.2SG]</ta>
            <ta e="T246" id="Seg_10189" s="T245">теперь</ta>
            <ta e="T247" id="Seg_10190" s="T246">каждый-3SG-ACC</ta>
            <ta e="T248" id="Seg_10191" s="T247">красть-FUT-3SG</ta>
            <ta e="T249" id="Seg_10192" s="T248">говорить-PRS.[3SG]</ta>
            <ta e="T251" id="Seg_10193" s="T250">вот.беда</ta>
            <ta e="T252" id="Seg_10194" s="T251">живот.[NOM]</ta>
            <ta e="T253" id="Seg_10195" s="T252">только-1SG</ta>
            <ta e="T256" id="Seg_10196" s="T255">идти.[IMP.2SG]</ta>
            <ta e="T257" id="Seg_10197" s="T256">идти.[IMP.2SG]</ta>
            <ta e="T259" id="Seg_10198" s="T258">что.[NOM]</ta>
            <ta e="T260" id="Seg_10199" s="T259">INDEF</ta>
            <ta e="T261" id="Seg_10200" s="T260">тряпка-INSTR</ta>
            <ta e="T265" id="Seg_10201" s="T264">тот-ABL</ta>
            <ta e="T266" id="Seg_10202" s="T265">идти-NEG.[3SG]</ta>
            <ta e="T268" id="Seg_10203" s="T267">вот</ta>
            <ta e="T271" id="Seg_10204" s="T270">идти.[IMP.2SG]</ta>
            <ta e="T273" id="Seg_10205" s="T272">давать-CVB.SEQ</ta>
            <ta e="T274" id="Seg_10206" s="T273">бросать-PRS.[3SG]</ta>
            <ta e="T275" id="Seg_10207" s="T274">EMPH</ta>
            <ta e="T277" id="Seg_10208" s="T276">тот-3SG-2SG.[NOM]</ta>
            <ta e="T278" id="Seg_10209" s="T277">жевать.закрытым.ртом-CVB.SIM</ta>
            <ta e="T279" id="Seg_10210" s="T278">жевать.закрытым.ртом-CVB.SIM</ta>
            <ta e="T281" id="Seg_10211" s="T280">идти-FREQ-PRS.[3SG]</ta>
            <ta e="T282" id="Seg_10212" s="T281">говорить-CVB.SEQ</ta>
            <ta e="T283" id="Seg_10213" s="T282">к.счастью</ta>
            <ta e="T284" id="Seg_10214" s="T283">хлеб-ACC</ta>
            <ta e="T285" id="Seg_10215" s="T284">к.счастью</ta>
            <ta e="T286" id="Seg_10216" s="T285">наесться-PST1-3SG</ta>
            <ta e="T287" id="Seg_10217" s="T286">наверное</ta>
            <ta e="T288" id="Seg_10218" s="T287">что.[NOM]</ta>
            <ta e="T289" id="Seg_10219" s="T288">столько</ta>
            <ta e="T290" id="Seg_10220" s="T289">наесться-FUT.[3SG]=Q</ta>
            <ta e="T291" id="Seg_10221" s="T290">вот</ta>
            <ta e="T292" id="Seg_10222" s="T291">тот</ta>
            <ta e="T293" id="Seg_10223" s="T292">скрежетать-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T296" id="Seg_10224" s="T295">кто-ACC</ta>
            <ta e="T297" id="Seg_10225" s="T296">давать-PST2.[3SG]</ta>
            <ta e="T298" id="Seg_10226" s="T297">высыхать-PTCP.PST</ta>
            <ta e="T299" id="Seg_10227" s="T298">хлеб-ACC</ta>
            <ta e="T301" id="Seg_10228" s="T300">вот</ta>
            <ta e="T302" id="Seg_10229" s="T301">там</ta>
            <ta e="T303" id="Seg_10230" s="T302">жевать-CVB.SIM</ta>
            <ta e="T304" id="Seg_10231" s="T303">пытаться-PST1-3SG</ta>
            <ta e="T305" id="Seg_10232" s="T304">EMPH</ta>
            <ta e="T307" id="Seg_10233" s="T306">вот.беда</ta>
            <ta e="T310" id="Seg_10234" s="T309">EXCL</ta>
            <ta e="T311" id="Seg_10235" s="T310">зуб-PL-3SG-ACC</ta>
            <ta e="T312" id="Seg_10236" s="T311">целый-3SG-ACC</ta>
            <ta e="T313" id="Seg_10237" s="T312">кто-VBZ-PST1-3SG</ta>
            <ta e="T314" id="Seg_10238" s="T313">наверное</ta>
            <ta e="T316" id="Seg_10239" s="T315">вот</ta>
            <ta e="T317" id="Seg_10240" s="T316">тот</ta>
            <ta e="T318" id="Seg_10241" s="T317">идти-PST2-3SG</ta>
            <ta e="T319" id="Seg_10242" s="T318">к.счастью</ta>
            <ta e="T320" id="Seg_10243" s="T319">друг-PL-3SG-ACC</ta>
            <ta e="T321" id="Seg_10244" s="T320">к</ta>
            <ta e="T323" id="Seg_10245" s="T322">высокий.берег.[NOM]</ta>
            <ta e="T324" id="Seg_10246" s="T323">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T325" id="Seg_10247" s="T324">сходить-PST2-3PL</ta>
            <ta e="T326" id="Seg_10248" s="T325">там</ta>
            <ta e="T327" id="Seg_10249" s="T326">река-DAT/LOC</ta>
            <ta e="T328" id="Seg_10250" s="T327">идти-PRS-3PL</ta>
            <ta e="T330" id="Seg_10251" s="T329">двор-1PL.[NOM]</ta>
            <ta e="T331" id="Seg_10252" s="T330">темнеть-PST1-3SG</ta>
            <ta e="T332" id="Seg_10253" s="T331">темнеть-PRS.[3SG]</ta>
            <ta e="T333" id="Seg_10254" s="T332">темнеть-CVB.SEQ</ta>
            <ta e="T334" id="Seg_10255" s="T333">идти-PRS.[3SG]</ta>
            <ta e="T336" id="Seg_10256" s="T335">кто.[NOM]</ta>
            <ta e="T337" id="Seg_10257" s="T336">собака-PROPR.[NOM]</ta>
            <ta e="T338" id="Seg_10258" s="T337">быть-PST1-1PL</ta>
            <ta e="T339" id="Seg_10259" s="T338">1PL.[NOM]</ta>
            <ta e="T340" id="Seg_10260" s="T339">Дьонго</ta>
            <ta e="T341" id="Seg_10261" s="T340">белый</ta>
            <ta e="T342" id="Seg_10262" s="T341">очень</ta>
            <ta e="T343" id="Seg_10263" s="T342">красивый</ta>
            <ta e="T344" id="Seg_10264" s="T343">очень</ta>
            <ta e="T345" id="Seg_10265" s="T344">олень-AG.[NOM]</ta>
            <ta e="T346" id="Seg_10266" s="T345">очень</ta>
            <ta e="T347" id="Seg_10267" s="T346">тот-3SG-2SG.[NOM]</ta>
            <ta e="T349" id="Seg_10268" s="T348">кто.[NOM]</ta>
            <ta e="T351" id="Seg_10269" s="T350">говорить-FUT-2SG</ta>
            <ta e="T352" id="Seg_10270" s="T351">Q</ta>
            <ta e="T353" id="Seg_10271" s="T352">кто-ACC</ta>
            <ta e="T354" id="Seg_10272" s="T353">стадо-ACC</ta>
            <ta e="T355" id="Seg_10273" s="T354">целый-3SG-ACC</ta>
            <ta e="T356" id="Seg_10274" s="T355">приносить-CVB.SEQ</ta>
            <ta e="T357" id="Seg_10275" s="T356">приходить-FUT.[3SG]</ta>
            <ta e="T359" id="Seg_10276" s="T358">о</ta>
            <ta e="T360" id="Seg_10277" s="T359">повернуть-FREQ-FUT-3SG</ta>
            <ta e="T361" id="Seg_10278" s="T360">говорить-CVB.SEQ</ta>
            <ta e="T362" id="Seg_10279" s="T361">вокруг</ta>
            <ta e="T363" id="Seg_10280" s="T362">один</ta>
            <ta e="T364" id="Seg_10281" s="T363">место-DAT/LOC</ta>
            <ta e="T365" id="Seg_10282" s="T364">стоять-FUT-3SG</ta>
            <ta e="T367" id="Seg_10283" s="T366">этот</ta>
            <ta e="T368" id="Seg_10284" s="T367">приносить.[IMP.2SG]</ta>
            <ta e="T369" id="Seg_10285" s="T368">говорить-PTCP.COND-DAT/LOC</ta>
            <ta e="T370" id="Seg_10286" s="T369">вот</ta>
            <ta e="T372" id="Seg_10287" s="T371">вот</ta>
            <ta e="T373" id="Seg_10288" s="T372">тот-3SG-1PL.[NOM]</ta>
            <ta e="T374" id="Seg_10289" s="T373">кто-ACC</ta>
            <ta e="T375" id="Seg_10290" s="T374">кто-VBZ-PTCP.HAB</ta>
            <ta e="T376" id="Seg_10291" s="T375">быть-PST1-3SG</ta>
            <ta e="T377" id="Seg_10292" s="T376">недавно</ta>
            <ta e="T378" id="Seg_10293" s="T377">олень-PL-EP-2SG.[NOM]</ta>
            <ta e="T379" id="Seg_10294" s="T378">есть</ta>
            <ta e="T380" id="Seg_10295" s="T379">быть-TEMP-3PL</ta>
            <ta e="T381" id="Seg_10296" s="T380">вот</ta>
            <ta e="T382" id="Seg_10297" s="T381">дом-DAT/LOC</ta>
            <ta e="T384" id="Seg_10298" s="T383">олененок.[NOM]</ta>
            <ta e="T385" id="Seg_10299" s="T384">годовалый.олень.[NOM]</ta>
            <ta e="T386" id="Seg_10300" s="T385">Q</ta>
            <ta e="T387" id="Seg_10301" s="T386">что.[NOM]</ta>
            <ta e="T388" id="Seg_10302" s="T387">Q</ta>
            <ta e="T389" id="Seg_10303" s="T388">годовалый.олень</ta>
            <ta e="T390" id="Seg_10304" s="T389">олененок.[NOM]</ta>
            <ta e="T391" id="Seg_10305" s="T390">ээ</ta>
            <ta e="T392" id="Seg_10306" s="T391">большой</ta>
            <ta e="T393" id="Seg_10307" s="T392">теленок-ABL</ta>
            <ta e="T394" id="Seg_10308" s="T393">большой-INTNS</ta>
            <ta e="T395" id="Seg_10309" s="T394">год-3SG-INSTR</ta>
            <ta e="T396" id="Seg_10310" s="T395">только</ta>
            <ta e="T397" id="Seg_10311" s="T396">большой</ta>
            <ta e="T399" id="Seg_10312" s="T398">тот</ta>
            <ta e="T400" id="Seg_10313" s="T399">кто-ACC</ta>
            <ta e="T401" id="Seg_10314" s="T400">вот</ta>
            <ta e="T402" id="Seg_10315" s="T401">повернуть-CVB.SIM</ta>
            <ta e="T403" id="Seg_10316" s="T402">ложиться-FUT-3SG</ta>
            <ta e="T404" id="Seg_10317" s="T403">EMPH</ta>
            <ta e="T405" id="Seg_10318" s="T404">мучить-CVB.SIM</ta>
            <ta e="T406" id="Seg_10319" s="T405">ложиться-HAB.[3SG]</ta>
            <ta e="T407" id="Seg_10320" s="T406">тот.[NOM]</ta>
            <ta e="T408" id="Seg_10321" s="T407">что.[NOM]</ta>
            <ta e="T409" id="Seg_10322" s="T408">теленок-DIM.[NOM]</ta>
            <ta e="T410" id="Seg_10323" s="T409">хотеть-HAB.[3SG]</ta>
            <ta e="T411" id="Seg_10324" s="T410">MOD</ta>
            <ta e="T412" id="Seg_10325" s="T411">двигаться-CVB.SIM</ta>
            <ta e="T413" id="Seg_10326" s="T412">двигаться-CVB.SIM</ta>
            <ta e="T415" id="Seg_10327" s="T414">ложиться-FUT-3SG</ta>
            <ta e="T417" id="Seg_10328" s="T416">EMPH</ta>
            <ta e="T418" id="Seg_10329" s="T417">играть-PRS.[3SG]</ta>
            <ta e="T419" id="Seg_10330" s="T418">EMPH</ta>
            <ta e="T420" id="Seg_10331" s="T419">3SG-ACC</ta>
            <ta e="T421" id="Seg_10332" s="T420">с</ta>
            <ta e="T422" id="Seg_10333" s="T421">вот</ta>
            <ta e="T423" id="Seg_10334" s="T422">тот-3SG-2SG.[NOM]</ta>
            <ta e="T424" id="Seg_10335" s="T423">как</ta>
            <ta e="T425" id="Seg_10336" s="T424">да</ta>
            <ta e="T426" id="Seg_10337" s="T425">быть-PST1-3SG</ta>
            <ta e="T427" id="Seg_10338" s="T1150">попадать.в-EP-RECP/COLL-EP-NMNZ.[NOM]</ta>
            <ta e="T428" id="Seg_10339" s="T427">попадать.в-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T429" id="Seg_10340" s="T428">этот</ta>
            <ta e="T430" id="Seg_10341" s="T429">как</ta>
            <ta e="T431" id="Seg_10342" s="T430">как</ta>
            <ta e="T432" id="Seg_10343" s="T431">делать=Q</ta>
            <ta e="T433" id="Seg_10344" s="T432">мама.[NOM]</ta>
            <ta e="T434" id="Seg_10345" s="T433">мама.[NOM]</ta>
            <ta e="T435" id="Seg_10346" s="T434">говорить-PRS-1SG</ta>
            <ta e="T436" id="Seg_10347" s="T435">видеть.[IMP.2SG]</ta>
            <ta e="T437" id="Seg_10348" s="T436">видеть.[IMP.2SG]</ta>
            <ta e="T438" id="Seg_10349" s="T437">Дьонго-ACC</ta>
            <ta e="T440" id="Seg_10350" s="T439">Чоко</ta>
            <ta e="T441" id="Seg_10351" s="T440">Чоко</ta>
            <ta e="T442" id="Seg_10352" s="T441">говорить-HAB-1SG</ta>
            <ta e="T444" id="Seg_10353" s="T443">Чоко.[NOM]</ta>
            <ta e="T445" id="Seg_10354" s="T444">видеть.[IMP.2SG]</ta>
            <ta e="T447" id="Seg_10355" s="T446">кто-ACC</ta>
            <ta e="T448" id="Seg_10356" s="T447">есть-CVB.PURP</ta>
            <ta e="T449" id="Seg_10357" s="T448">хотеть-PST1-3SG</ta>
            <ta e="T450" id="Seg_10358" s="T449">говорить-PRS-1SG</ta>
            <ta e="T451" id="Seg_10359" s="T450">олененок-ACC</ta>
            <ta e="T452" id="Seg_10360" s="T451">есть-CVB.PURP</ta>
            <ta e="T453" id="Seg_10361" s="T452">делать-PST1-3SG</ta>
            <ta e="T455" id="Seg_10362" s="T454">о</ta>
            <ta e="T456" id="Seg_10363" s="T455">сатана</ta>
            <ta e="T457" id="Seg_10364" s="T456">звать.[IMP.2SG]</ta>
            <ta e="T458" id="Seg_10365" s="T457">звать.[IMP.2SG]</ta>
            <ta e="T460" id="Seg_10366" s="T459">устать-CAUS-PST1-3SG</ta>
            <ta e="T461" id="Seg_10367" s="T460">наверное</ta>
            <ta e="T462" id="Seg_10368" s="T461">тот.[NOM]</ta>
            <ta e="T463" id="Seg_10369" s="T462">олененок-ACC</ta>
            <ta e="T464" id="Seg_10370" s="T463">путать-PST1-3SG</ta>
            <ta e="T466" id="Seg_10371" s="T465">Чоко</ta>
            <ta e="T467" id="Seg_10372" s="T466">Чоко</ta>
            <ta e="T468" id="Seg_10373" s="T467">Чоко</ta>
            <ta e="T469" id="Seg_10374" s="T468">говорить-PRS-1SG</ta>
            <ta e="T470" id="Seg_10375" s="T469">на.улицу</ta>
            <ta e="T471" id="Seg_10376" s="T470">выйти-CVB.SEQ</ta>
            <ta e="T472" id="Seg_10377" s="T471">идти-CVB.SEQ</ta>
            <ta e="T473" id="Seg_10378" s="T472">вот</ta>
            <ta e="T475" id="Seg_10379" s="T474">о</ta>
            <ta e="T476" id="Seg_10380" s="T475">вот</ta>
            <ta e="T477" id="Seg_10381" s="T476">тот-3SG-2SG.[NOM]</ta>
            <ta e="T478" id="Seg_10382" s="T477">мчаться-CVB.SEQ</ta>
            <ta e="T479" id="Seg_10383" s="T478">идти-EMOT-PRS.[3SG]</ta>
            <ta e="T480" id="Seg_10384" s="T479">говорить-CVB.SEQ</ta>
            <ta e="T482" id="Seg_10385" s="T481">вот</ta>
            <ta e="T483" id="Seg_10386" s="T482">прыгать</ta>
            <ta e="T484" id="Seg_10387" s="T483">вот</ta>
            <ta e="T485" id="Seg_10388" s="T484">лицо-1SG-DAT/LOC</ta>
            <ta e="T486" id="Seg_10389" s="T485">1SG.[NOM]</ta>
            <ta e="T487" id="Seg_10390" s="T486">лежать.навзничь-NMNZ.[NOM]</ta>
            <ta e="T490" id="Seg_10391" s="T489">навзничь</ta>
            <ta e="T491" id="Seg_10392" s="T490">лежать.навзничь-CVB.SIM</ta>
            <ta e="T492" id="Seg_10393" s="T491">лежать-EMOT-PRS-1SG</ta>
            <ta e="T493" id="Seg_10394" s="T492">говорить-CVB.SEQ</ta>
            <ta e="T495" id="Seg_10395" s="T494">AFFIRM</ta>
            <ta e="T496" id="Seg_10396" s="T495">говорить-PRS-1SG</ta>
            <ta e="T497" id="Seg_10397" s="T496">мама.[NOM]</ta>
            <ta e="T498" id="Seg_10398" s="T497">взять.[IMP.2SG]</ta>
            <ta e="T499" id="Seg_10399" s="T498">вот</ta>
            <ta e="T500" id="Seg_10400" s="T499">EMPH</ta>
            <ta e="T502" id="Seg_10401" s="T501">лизать-PRS.[3SG]</ta>
            <ta e="T503" id="Seg_10402" s="T502">и.так.далее-PRS.[3SG]</ta>
            <ta e="T504" id="Seg_10403" s="T503">двигаться</ta>
            <ta e="T505" id="Seg_10404" s="T504">двигаться-CAUS-PRS.[3SG]</ta>
            <ta e="T506" id="Seg_10405" s="T505">EMPH</ta>
            <ta e="T507" id="Seg_10406" s="T506">человек-ACC</ta>
            <ta e="T508" id="Seg_10407" s="T507">вот</ta>
            <ta e="T510" id="Seg_10408" s="T509">двигаться-PTCP.FUT-3SG-ACC</ta>
            <ta e="T511" id="Seg_10409" s="T510">хотеть-PRS.[3SG]</ta>
            <ta e="T513" id="Seg_10410" s="T512">1SG-ACC</ta>
            <ta e="T514" id="Seg_10411" s="T513">таскать-CVB.SIM</ta>
            <ta e="T515" id="Seg_10412" s="T514">ложиться-FREQ-PRS.[3SG]</ta>
            <ta e="T516" id="Seg_10413" s="T515">говорить-CVB.SEQ</ta>
            <ta e="T518" id="Seg_10414" s="T517">вот</ta>
            <ta e="T519" id="Seg_10415" s="T518">тот</ta>
            <ta e="T520" id="Seg_10416" s="T519">мама-1SG.[NOM]</ta>
            <ta e="T521" id="Seg_10417" s="T520">выйти-PST1-3SG</ta>
            <ta e="T523" id="Seg_10418" s="T522">вот</ta>
            <ta e="T524" id="Seg_10419" s="T523">что.[NOM]</ta>
            <ta e="T525" id="Seg_10420" s="T524">становиться-PRS-2SG</ta>
            <ta e="T527" id="Seg_10421" s="T526">видеть.[IMP.2SG]</ta>
            <ta e="T528" id="Seg_10422" s="T527">говорить-PRS-1SG</ta>
            <ta e="T529" id="Seg_10423" s="T528">1SG-ACC</ta>
            <ta e="T530" id="Seg_10424" s="T529">бить-PRS.[3SG]</ta>
            <ta e="T531" id="Seg_10425" s="T530">Q</ta>
            <ta e="T532" id="Seg_10426" s="T531">говорить-PRS-1SG</ta>
            <ta e="T533" id="Seg_10427" s="T532">играть-PRS.[3SG]</ta>
            <ta e="T534" id="Seg_10428" s="T533">человек-ACC</ta>
            <ta e="T535" id="Seg_10429" s="T534">вот</ta>
            <ta e="T536" id="Seg_10430" s="T535">вот.беда</ta>
            <ta e="T537" id="Seg_10431" s="T536">да</ta>
            <ta e="T539" id="Seg_10432" s="T538">%%-CVB.SEQ</ta>
            <ta e="T540" id="Seg_10433" s="T539">плакать-CVB.SEQ</ta>
            <ta e="T541" id="Seg_10434" s="T540">громко.кричать-PRS-1SG</ta>
            <ta e="T542" id="Seg_10435" s="T541">AFFIRM</ta>
            <ta e="T544" id="Seg_10436" s="T543">плакать.[IMP.2SG]</ta>
            <ta e="T545" id="Seg_10437" s="T544">плакать-NEG.[IMP.2SG]</ta>
            <ta e="T547" id="Seg_10438" s="T546">сам-2SG.[NOM]</ta>
            <ta e="T548" id="Seg_10439" s="T547">звать-PST2-EP-2SG</ta>
            <ta e="T549" id="Seg_10440" s="T548">говорить-PRS.[3SG]</ta>
            <ta e="T550" id="Seg_10441" s="T549">человек.[NOM]</ta>
            <ta e="T551" id="Seg_10442" s="T550">смеяться-PTCP.FUT.[NOM]</ta>
            <ta e="T553" id="Seg_10443" s="T552">тот-3SG-2SG.[NOM]</ta>
            <ta e="T554" id="Seg_10444" s="T553">опять</ta>
            <ta e="T555" id="Seg_10445" s="T554">идти-CVB.SEQ</ta>
            <ta e="T556" id="Seg_10446" s="T555">оставаться-PST1-3SG</ta>
            <ta e="T557" id="Seg_10447" s="T556">тот</ta>
            <ta e="T558" id="Seg_10448" s="T557">теленок-DAT/LOC</ta>
            <ta e="T560" id="Seg_10449" s="T559">вот</ta>
            <ta e="T562" id="Seg_10450" s="T561">кто-VBZ-PST2.NEG-EP-1SG</ta>
            <ta e="T563" id="Seg_10451" s="T562">тот</ta>
            <ta e="T564" id="Seg_10452" s="T563">всегда</ta>
            <ta e="T565" id="Seg_10453" s="T564">день.[NOM]</ta>
            <ta e="T566" id="Seg_10454" s="T565">каждый</ta>
            <ta e="T567" id="Seg_10455" s="T566">играть-HAB.[3SG]</ta>
            <ta e="T568" id="Seg_10456" s="T567">тот</ta>
            <ta e="T569" id="Seg_10457" s="T568">олененок-ACC</ta>
            <ta e="T570" id="Seg_10458" s="T569">с</ta>
            <ta e="T571" id="Seg_10459" s="T570">что-DAT/LOC</ta>
            <ta e="T572" id="Seg_10460" s="T571">хотеть-PST2.[3SG]</ta>
            <ta e="T574" id="Seg_10461" s="T573">тот.[NOM]</ta>
            <ta e="T575" id="Seg_10462" s="T574">олененок.[NOM]</ta>
            <ta e="T576" id="Seg_10463" s="T575">становиться-PTCP.FUT-3SG-ACC</ta>
            <ta e="T577" id="Seg_10464" s="T576">надо</ta>
            <ta e="T579" id="Seg_10465" s="T578">вот</ta>
            <ta e="T580" id="Seg_10466" s="T579">тот-ABL</ta>
            <ta e="T581" id="Seg_10467" s="T580">двор-1PL.[NOM]</ta>
            <ta e="T582" id="Seg_10468" s="T581">темнеть-PST1-3SG</ta>
            <ta e="T583" id="Seg_10469" s="T582">EMPH</ta>
            <ta e="T585" id="Seg_10470" s="T584">красивый-ADVZ</ta>
            <ta e="T586" id="Seg_10471" s="T585">да</ta>
            <ta e="T587" id="Seg_10472" s="T586">кто-1PL.[NOM]</ta>
            <ta e="T588" id="Seg_10473" s="T587">очаг-1PL.[NOM]</ta>
            <ta e="T589" id="Seg_10474" s="T588">вот</ta>
            <ta e="T591" id="Seg_10475" s="T590">мама-1SG.[NOM]</ta>
            <ta e="T592" id="Seg_10476" s="T591">вдоволь</ta>
            <ta e="T593" id="Seg_10477" s="T592">класть-PST2.[3SG]</ta>
            <ta e="T598" id="Seg_10478" s="T597">кто.[NOM]</ta>
            <ta e="T599" id="Seg_10479" s="T598">дерево.[NOM]</ta>
            <ta e="T600" id="Seg_10480" s="T599">вот</ta>
            <ta e="T601" id="Seg_10481" s="T600">загораться-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T602" id="Seg_10482" s="T601">вот</ta>
            <ta e="T603" id="Seg_10483" s="T602">краснеть-INCH</ta>
            <ta e="T604" id="Seg_10484" s="T603">очаг-1PL.[NOM]</ta>
            <ta e="T606" id="Seg_10485" s="T605">чайник-PL.[NOM]</ta>
            <ta e="T607" id="Seg_10486" s="T606">подниматься-EP-REFL-CVB.SEQ</ta>
            <ta e="T609" id="Seg_10487" s="T608">дрожать-EMOT-PRS-3PL</ta>
            <ta e="T610" id="Seg_10488" s="T609">говорить-CVB.SEQ</ta>
            <ta e="T611" id="Seg_10489" s="T610">крышка-3PL.[NOM]</ta>
            <ta e="T613" id="Seg_10490" s="T612">пища-1PL.[NOM]</ta>
            <ta e="T614" id="Seg_10491" s="T613">%%</ta>
            <ta e="T615" id="Seg_10492" s="T614">вставать-FREQ-PRS.[3SG]</ta>
            <ta e="T616" id="Seg_10493" s="T615">говорить-CVB.SEQ</ta>
            <ta e="T617" id="Seg_10494" s="T616">%%</ta>
            <ta e="T620" id="Seg_10495" s="T619">согреться</ta>
            <ta e="T621" id="Seg_10496" s="T620">дверь.[NOM]</ta>
            <ta e="T622" id="Seg_10497" s="T621">окно-ACC</ta>
            <ta e="T623" id="Seg_10498" s="T622">открывать-PST1-3SG</ta>
            <ta e="T624" id="Seg_10499" s="T623">дымоход-3SG.[NOM]</ta>
            <ta e="T625" id="Seg_10500" s="T624">открывать-PST1-3SG</ta>
            <ta e="T627" id="Seg_10501" s="T626">дымоход</ta>
            <ta e="T628" id="Seg_10502" s="T627">быть-HAB.[3SG]</ta>
            <ta e="T629" id="Seg_10503" s="T628">кто.[NOM]</ta>
            <ta e="T630" id="Seg_10504" s="T629">закрывать-HAB-3PL</ta>
            <ta e="T634" id="Seg_10505" s="T633">ветер.[NOM]</ta>
            <ta e="T635" id="Seg_10506" s="T634">входить-PTCP.FUT-3SG-ACC</ta>
            <ta e="T636" id="Seg_10507" s="T635">вот</ta>
            <ta e="T638" id="Seg_10508" s="T637">вот</ta>
            <ta e="T639" id="Seg_10509" s="T638">тот</ta>
            <ta e="T640" id="Seg_10510" s="T639">открывать-PST1-3SG</ta>
            <ta e="T641" id="Seg_10511" s="T640">AFFIRM</ta>
            <ta e="T643" id="Seg_10512" s="T642">1SG.[NOM]</ta>
            <ta e="T644" id="Seg_10513" s="T643">Q</ta>
            <ta e="T645" id="Seg_10514" s="T644">очень</ta>
            <ta e="T646" id="Seg_10515" s="T645">любить-HAB-1SG</ta>
            <ta e="T647" id="Seg_10516" s="T646">окно-DAT/LOC</ta>
            <ta e="T648" id="Seg_10517" s="T647">окно-DAT/LOC</ta>
            <ta e="T649" id="Seg_10518" s="T648">видеть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T650" id="Seg_10519" s="T649">вот</ta>
            <ta e="T652" id="Seg_10520" s="T651">иметь.широкое.лицо-CVB.SIM</ta>
            <ta e="T653" id="Seg_10521" s="T652">иметь.широкое.лицо-CVB.SIM</ta>
            <ta e="T654" id="Seg_10522" s="T653">сидеть-HAB-1SG</ta>
            <ta e="T656" id="Seg_10523" s="T655">красивый-ADVZ</ta>
            <ta e="T657" id="Seg_10524" s="T656">EMPH</ta>
            <ta e="T658" id="Seg_10525" s="T657">AFFIRM</ta>
            <ta e="T659" id="Seg_10526" s="T658">кто.[NOM]</ta>
            <ta e="T660" id="Seg_10527" s="T659">месяц.[NOM]</ta>
            <ta e="T663" id="Seg_10528" s="T662">месяц.[NOM]</ta>
            <ta e="T664" id="Seg_10529" s="T663">мука-DIM.[NOM]</ta>
            <ta e="T665" id="Seg_10530" s="T664">быть-PST2-3SG</ta>
            <ta e="T666" id="Seg_10531" s="T665">ээ</ta>
            <ta e="T668" id="Seg_10532" s="T667">луна-1PL.[NOM]</ta>
            <ta e="T669" id="Seg_10533" s="T668">красивый.[NOM]</ta>
            <ta e="T670" id="Seg_10534" s="T669">очень</ta>
            <ta e="T671" id="Seg_10535" s="T670">но</ta>
            <ta e="T672" id="Seg_10536" s="T671">EMPH-темный.[NOM]</ta>
            <ta e="T674" id="Seg_10537" s="T673">что-PL.[NOM]</ta>
            <ta e="T675" id="Seg_10538" s="T674">летать-HAB-3PL</ta>
            <ta e="T676" id="Seg_10539" s="T675">быть-PST2.[3SG]</ta>
            <ta e="T677" id="Seg_10540" s="T676">1SG.[NOM]</ta>
            <ta e="T679" id="Seg_10541" s="T678">знать-NEG.PTCP</ta>
            <ta e="T680" id="Seg_10542" s="T679">быть-PST1-1SG</ta>
            <ta e="T682" id="Seg_10543" s="T681">полярная.сова.[NOM]</ta>
            <ta e="T683" id="Seg_10544" s="T682">подобно</ta>
            <ta e="T684" id="Seg_10545" s="T683">полярная.сова-PL.[NOM]</ta>
            <ta e="T685" id="Seg_10546" s="T684">говорить-CVB.SEQ</ta>
            <ta e="T686" id="Seg_10547" s="T685">NEG-3SG</ta>
            <ta e="T687" id="Seg_10548" s="T686">маленький-3PL</ta>
            <ta e="T689" id="Seg_10549" s="T688">голова-POSS</ta>
            <ta e="T690" id="Seg_10550" s="T689">NEG-3PL</ta>
            <ta e="T691" id="Seg_10551" s="T690">говорить-HAB-1SG</ta>
            <ta e="T692" id="Seg_10552" s="T691">голова-POSS</ta>
            <ta e="T693" id="Seg_10553" s="T692">NEG-3PL</ta>
            <ta e="T694" id="Seg_10554" s="T693">идти-PRS-3PL</ta>
            <ta e="T695" id="Seg_10555" s="T694">мама.[NOM]</ta>
            <ta e="T696" id="Seg_10556" s="T695">видеть.[IMP.2SG]</ta>
            <ta e="T699" id="Seg_10557" s="T698">тот.[NOM]</ta>
            <ta e="T700" id="Seg_10558" s="T699">кто.[NOM]</ta>
            <ta e="T701" id="Seg_10559" s="T700">ээ</ta>
            <ta e="T702" id="Seg_10560" s="T701">этот</ta>
            <ta e="T703" id="Seg_10561" s="T702">уголь.[NOM]</ta>
            <ta e="T704" id="Seg_10562" s="T703">летать-PRS.[3SG]</ta>
            <ta e="T705" id="Seg_10563" s="T704">EMPH</ta>
            <ta e="T706" id="Seg_10564" s="T705">труба-ABL</ta>
            <ta e="T707" id="Seg_10565" s="T706">вот</ta>
            <ta e="T709" id="Seg_10566" s="T708">тот-DAT/LOC</ta>
            <ta e="T710" id="Seg_10567" s="T709">двигаться-RECP/COLL-PRS-3PL</ta>
            <ta e="T711" id="Seg_10568" s="T710">быть-PST2.[3SG]</ta>
            <ta e="T712" id="Seg_10569" s="T711">тот-PL-EP-2SG.[NOM]</ta>
            <ta e="T714" id="Seg_10570" s="T713">очень</ta>
            <ta e="T716" id="Seg_10571" s="T715">тот-ABL</ta>
            <ta e="T717" id="Seg_10572" s="T716">сколько</ta>
            <ta e="T718" id="Seg_10573" s="T717">INDEF</ta>
            <ta e="T719" id="Seg_10574" s="T718">мама-1SG.[NOM]</ta>
            <ta e="T720" id="Seg_10575" s="T719">шить</ta>
            <ta e="T721" id="Seg_10576" s="T720">сидеть-PRS.[3SG]</ta>
            <ta e="T722" id="Seg_10577" s="T721">шить</ta>
            <ta e="T723" id="Seg_10578" s="T722">сидеть-PRS.[3SG]</ta>
            <ta e="T725" id="Seg_10579" s="T724">потихоньку</ta>
            <ta e="T726" id="Seg_10580" s="T725">потихоньку</ta>
            <ta e="T727" id="Seg_10581" s="T726">EMPH</ta>
            <ta e="T728" id="Seg_10582" s="T727">EMPH</ta>
            <ta e="T729" id="Seg_10583" s="T728">петь-CVB.SEQ</ta>
            <ta e="T730" id="Seg_10584" s="T729">сидеть-PRS.[3SG]</ta>
            <ta e="T731" id="Seg_10585" s="T730">потихоньку</ta>
            <ta e="T733" id="Seg_10586" s="T732">Дьонго-1PL.[NOM]</ta>
            <ta e="T734" id="Seg_10587" s="T733">лежать-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_10588" s="T735">ник</ta>
            <ta e="T737" id="Seg_10589" s="T736">NEG</ta>
            <ta e="T738" id="Seg_10590" s="T737">делать-NEG.[3SG]</ta>
            <ta e="T740" id="Seg_10591" s="T739">устать-PST2.[3SG]</ta>
            <ta e="T741" id="Seg_10592" s="T740">наверное</ta>
            <ta e="T742" id="Seg_10593" s="T741">олененок-3SG-ACC</ta>
            <ta e="T743" id="Seg_10594" s="T742">с</ta>
            <ta e="T745" id="Seg_10595" s="T744">тот-ABL</ta>
            <ta e="T746" id="Seg_10596" s="T745">кто.[NOM]</ta>
            <ta e="T749" id="Seg_10597" s="T748">идти-PST1-3SG</ta>
            <ta e="T750" id="Seg_10598" s="T749">EMPH</ta>
            <ta e="T753" id="Seg_10599" s="T752">тот</ta>
            <ta e="T754" id="Seg_10600" s="T753">что.[NOM]</ta>
            <ta e="T755" id="Seg_10601" s="T754">разный</ta>
            <ta e="T756" id="Seg_10602" s="T755">разный</ta>
            <ta e="T757" id="Seg_10603" s="T756">1SG.[NOM]</ta>
            <ta e="T759" id="Seg_10604" s="T758">отец-1SG.[NOM]</ta>
            <ta e="T760" id="Seg_10605" s="T759">нет-INTNS-3SG-INSTR</ta>
            <ta e="T761" id="Seg_10606" s="T760">радио-PART</ta>
            <ta e="T762" id="Seg_10607" s="T761">включ-VBZ-EP-MED-PRS-1SG</ta>
            <ta e="T764" id="Seg_10608" s="T763">AFFIRM</ta>
            <ta e="T765" id="Seg_10609" s="T764">%%-EMOT-PRS-1SG</ta>
            <ta e="T766" id="Seg_10610" s="T765">вот</ta>
            <ta e="T768" id="Seg_10611" s="T767">AFFIRM</ta>
            <ta e="T769" id="Seg_10612" s="T768">потихоньку</ta>
            <ta e="T770" id="Seg_10613" s="T769">делать.[IMP.2SG]</ta>
            <ta e="T771" id="Seg_10614" s="T770">сам</ta>
            <ta e="T772" id="Seg_10615" s="T771">%%-EP-NEG.[IMP.2SG]</ta>
            <ta e="T774" id="Seg_10616" s="T773">песня-PL-ACC</ta>
            <ta e="T775" id="Seg_10617" s="T774">слушать-CVB.SIM</ta>
            <ta e="T776" id="Seg_10618" s="T775">лежать-FUT-1SG</ta>
            <ta e="T777" id="Seg_10619" s="T776">EMPH</ta>
            <ta e="T780" id="Seg_10620" s="T779">кто-PL-ACC</ta>
            <ta e="T781" id="Seg_10621" s="T780">вот</ta>
            <ta e="T782" id="Seg_10622" s="T781">слушать-PRS-1SG</ta>
            <ta e="T784" id="Seg_10623" s="T783">китаец-PL-PL.[NOM]</ta>
            <ta e="T785" id="Seg_10624" s="T784">что-PL.[NOM]</ta>
            <ta e="T786" id="Seg_10625" s="T785">Q</ta>
            <ta e="T788" id="Seg_10626" s="T787">тот-PL-ACC</ta>
            <ta e="T789" id="Seg_10627" s="T788">слушать-CVB.SEQ</ta>
            <ta e="T790" id="Seg_10628" s="T789">MOD</ta>
            <ta e="T791" id="Seg_10629" s="T790">лежать-FREQ-PRS-1SG</ta>
            <ta e="T792" id="Seg_10630" s="T791">говорить-CVB.SEQ</ta>
            <ta e="T793" id="Seg_10631" s="T792">тогда</ta>
            <ta e="T794" id="Seg_10632" s="T793">весело</ta>
            <ta e="T795" id="Seg_10633" s="T794">очень</ta>
            <ta e="T796" id="Seg_10634" s="T795">знать-PRS-1SG</ta>
            <ta e="T797" id="Seg_10635" s="T796">тот-ABL</ta>
            <ta e="T798" id="Seg_10636" s="T797">к.счастью</ta>
            <ta e="T799" id="Seg_10637" s="T798">русский-PL.[NOM]</ta>
            <ta e="T800" id="Seg_10638" s="T799">найти-PST1-1SG</ta>
            <ta e="T801" id="Seg_10639" s="T800">тот-ABL</ta>
            <ta e="T802" id="Seg_10640" s="T801">кончать-CVB.SEQ</ta>
            <ta e="T803" id="Seg_10641" s="T802">оставаться-PST2.[3SG]</ta>
            <ta e="T805" id="Seg_10642" s="T804">вот</ta>
            <ta e="T806" id="Seg_10643" s="T805">кончать-EP-CAUS-CVB.SEQ</ta>
            <ta e="T807" id="Seg_10644" s="T806">бросать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T808" id="Seg_10645" s="T807">кончать-EP-CAUS.[IMP.2SG]</ta>
            <ta e="T809" id="Seg_10646" s="T808">отец-2SG.[NOM]</ta>
            <ta e="T810" id="Seg_10647" s="T809">приходить-FUT.[3SG]</ta>
            <ta e="T811" id="Seg_10648" s="T810">AFFIRM</ta>
            <ta e="T813" id="Seg_10649" s="T812">солнце-1PL.[NOM]</ta>
            <ta e="T814" id="Seg_10650" s="T813">луна-1PL.[NOM]</ta>
            <ta e="T815" id="Seg_10651" s="T814">удаляться-PST1-3SG</ta>
            <ta e="T816" id="Seg_10652" s="T815">EMPH</ta>
            <ta e="T817" id="Seg_10653" s="T816">наверху</ta>
            <ta e="T818" id="Seg_10654" s="T817">быть-PST1-3SG</ta>
            <ta e="T819" id="Seg_10655" s="T818">EMPH</ta>
            <ta e="T820" id="Seg_10656" s="T819">красивый</ta>
            <ta e="T822" id="Seg_10657" s="T821">EMPH</ta>
            <ta e="T823" id="Seg_10658" s="T822">яркий.[NOM]</ta>
            <ta e="T824" id="Seg_10659" s="T823">днем</ta>
            <ta e="T825" id="Seg_10660" s="T824">подобно</ta>
            <ta e="T827" id="Seg_10661" s="T826">о</ta>
            <ta e="T828" id="Seg_10662" s="T827">олень-PL-1PL.[NOM]</ta>
            <ta e="T830" id="Seg_10663" s="T829">река.[NOM]</ta>
            <ta e="T831" id="Seg_10664" s="T830">вдоль</ta>
            <ta e="T832" id="Seg_10665" s="T831">мчаться-CVB.SIM</ta>
            <ta e="T833" id="Seg_10666" s="T832">идти-FREQ-PRS-3PL</ta>
            <ta e="T834" id="Seg_10667" s="T833">вот</ta>
            <ta e="T835" id="Seg_10668" s="T834">EMPH</ta>
            <ta e="T836" id="Seg_10669" s="T835">красивый-ADVZ</ta>
            <ta e="T837" id="Seg_10670" s="T836">EMPH</ta>
            <ta e="T839" id="Seg_10671" s="T838">кто.[NOM]</ta>
            <ta e="T840" id="Seg_10672" s="T839">дым-3PL.[NOM]</ta>
            <ta e="T841" id="Seg_10673" s="T840">аж</ta>
            <ta e="T842" id="Seg_10674" s="T841">летать-PRS.[3SG]</ta>
            <ta e="T844" id="Seg_10675" s="T843">прыгать</ta>
            <ta e="T845" id="Seg_10676" s="T844">EMPH</ta>
            <ta e="T846" id="Seg_10677" s="T845">красивый</ta>
            <ta e="T847" id="Seg_10678" s="T846">кто.[NOM]</ta>
            <ta e="T848" id="Seg_10679" s="T847">вот</ta>
            <ta e="T849" id="Seg_10680" s="T848">снег-EP-2SG.[NOM]</ta>
            <ta e="T850" id="Seg_10681" s="T849">вот</ta>
            <ta e="T851" id="Seg_10682" s="T850">кто-DAT/LOC</ta>
            <ta e="T852" id="Seg_10683" s="T851">луна-ABL</ta>
            <ta e="T853" id="Seg_10684" s="T852">EMPH</ta>
            <ta e="T854" id="Seg_10685" s="T853">кто-VBZ-PRS.[3SG]</ta>
            <ta e="T855" id="Seg_10686" s="T854">блестеть-ITER-PRS.[3SG]</ta>
            <ta e="T857" id="Seg_10687" s="T856">тот-ABL</ta>
            <ta e="T858" id="Seg_10688" s="T857">тот-ABL</ta>
            <ta e="T859" id="Seg_10689" s="T858">подожди</ta>
            <ta e="T860" id="Seg_10690" s="T859">собака-1PL.[NOM]</ta>
            <ta e="T863" id="Seg_10691" s="T862">делать-PRS.[3SG]</ta>
            <ta e="T864" id="Seg_10692" s="T863">этот</ta>
            <ta e="T865" id="Seg_10693" s="T864">кто-VBZ-PRS.[3SG]</ta>
            <ta e="T866" id="Seg_10694" s="T865">выть-PRS.[3SG]</ta>
            <ta e="T868" id="Seg_10695" s="T867">INTJ</ta>
            <ta e="T869" id="Seg_10696" s="T868">бубенчик.[NOM]</ta>
            <ta e="T870" id="Seg_10697" s="T869">звучать-PST1-3SG</ta>
            <ta e="T871" id="Seg_10698" s="T870">только</ta>
            <ta e="T872" id="Seg_10699" s="T871">ботало</ta>
            <ta e="T877" id="Seg_10700" s="T876">отец-1PL.[NOM]</ta>
            <ta e="T878" id="Seg_10701" s="T877">идти-PRS.[3SG]</ta>
            <ta e="T879" id="Seg_10702" s="T878">отец-1PL.[NOM]</ta>
            <ta e="T880" id="Seg_10703" s="T879">AFFIRM</ta>
            <ta e="T882" id="Seg_10704" s="T881">вот</ta>
            <ta e="T883" id="Seg_10705" s="T882">тот</ta>
            <ta e="T884" id="Seg_10706" s="T883">приходить-PST1-3SG</ta>
            <ta e="T886" id="Seg_10707" s="T885">1SG.[NOM]</ta>
            <ta e="T887" id="Seg_10708" s="T886">MOD</ta>
            <ta e="T888" id="Seg_10709" s="T887">знать-PST2.NEG-EP-1SG</ta>
            <ta e="T889" id="Seg_10710" s="T888">EMPH</ta>
            <ta e="T890" id="Seg_10711" s="T889">дерево-VBZ-MED-CVB.SIM</ta>
            <ta e="T891" id="Seg_10712" s="T890">идти-PST2.[3SG]</ta>
            <ta e="T892" id="Seg_10713" s="T891">думать-PRS-1SG</ta>
            <ta e="T893" id="Seg_10714" s="T892">EMPH</ta>
            <ta e="T895" id="Seg_10715" s="T894">3SG-2SG.[NOM]</ta>
            <ta e="T896" id="Seg_10716" s="T895">EMPH</ta>
            <ta e="T897" id="Seg_10717" s="T896">Дудинка.[NOM]</ta>
            <ta e="T898" id="Seg_10718" s="T897">к</ta>
            <ta e="T899" id="Seg_10719" s="T898">идти-EP-PST2.[3SG]</ta>
            <ta e="T900" id="Seg_10720" s="T899">и</ta>
            <ta e="T901" id="Seg_10721" s="T900">Дудинка.[NOM]</ta>
            <ta e="T902" id="Seg_10722" s="T901">к</ta>
            <ta e="T904" id="Seg_10723" s="T903">вот</ta>
            <ta e="T905" id="Seg_10724" s="T904">тот</ta>
            <ta e="T906" id="Seg_10725" s="T905">приходить-PST1-3SG</ta>
            <ta e="T909" id="Seg_10726" s="T908">темнеть-PTCP.PST-3SG-ACC</ta>
            <ta e="T910" id="Seg_10727" s="T909">после.того</ta>
            <ta e="T911" id="Seg_10728" s="T910">вот</ta>
            <ta e="T913" id="Seg_10729" s="T912">о</ta>
            <ta e="T914" id="Seg_10730" s="T913">отец-1SG.[NOM]</ta>
            <ta e="T915" id="Seg_10731" s="T914">приходить-PST1-3SG</ta>
            <ta e="T917" id="Seg_10732" s="T916">о</ta>
            <ta e="T918" id="Seg_10733" s="T917">мама-1SG.[NOM]</ta>
            <ta e="T919" id="Seg_10734" s="T918">помогать-CVB.SIM</ta>
            <ta e="T920" id="Seg_10735" s="T919">идти-PST1-3SG</ta>
            <ta e="T921" id="Seg_10736" s="T920">развязать-PST1-3SG</ta>
            <ta e="T922" id="Seg_10737" s="T921">и.так.далее-PST1-3SG</ta>
            <ta e="T923" id="Seg_10738" s="T922">ботало-ABL</ta>
            <ta e="T924" id="Seg_10739" s="T923">олень-3PL-ACC</ta>
            <ta e="T925" id="Seg_10740" s="T924">пустить-PST1-3PL</ta>
            <ta e="T926" id="Seg_10741" s="T925">и.так.далее-PST1-3PL</ta>
            <ta e="T928" id="Seg_10742" s="T927">о</ta>
            <ta e="T929" id="Seg_10743" s="T928">отец-1SG.[NOM]</ta>
            <ta e="T930" id="Seg_10744" s="T929">мешок-PL-PROPR</ta>
            <ta e="T931" id="Seg_10745" s="T930">приходить-PST1-3SG</ta>
            <ta e="T933" id="Seg_10746" s="T932">мешок-PL-3SG-ACC</ta>
            <ta e="T934" id="Seg_10747" s="T933">входить-CAUS-PST1-3SG</ta>
            <ta e="T936" id="Seg_10748" s="T935">вот</ta>
            <ta e="T937" id="Seg_10749" s="T936">вынимать-CAUS-FREQ-PST1-3SG</ta>
            <ta e="T938" id="Seg_10750" s="T937">о</ta>
            <ta e="T939" id="Seg_10751" s="T938">вот</ta>
            <ta e="T940" id="Seg_10752" s="T939">конфета.[NOM]</ta>
            <ta e="T941" id="Seg_10753" s="T940">INTJ-3SG-ACC</ta>
            <ta e="T942" id="Seg_10754" s="T941">принести-PST2.[3SG]</ta>
            <ta e="T944" id="Seg_10755" s="T943">тот-ABL</ta>
            <ta e="T945" id="Seg_10756" s="T944">красивый</ta>
            <ta e="T946" id="Seg_10757" s="T945">очень</ta>
            <ta e="T947" id="Seg_10758" s="T946">всегда</ta>
            <ta e="T948" id="Seg_10759" s="T947">помнить-PRS-1SG</ta>
            <ta e="T950" id="Seg_10760" s="T949">кто.[NOM]</ta>
            <ta e="T951" id="Seg_10761" s="T950">часы-PL.[NOM]</ta>
            <ta e="T952" id="Seg_10762" s="T951">Q</ta>
            <ta e="T953" id="Seg_10763" s="T952">красивый</ta>
            <ta e="T954" id="Seg_10764" s="T953">очень</ta>
            <ta e="T955" id="Seg_10765" s="T954">кто.[NOM]</ta>
            <ta e="T956" id="Seg_10766" s="T955">зеркало-INSTR</ta>
            <ta e="T957" id="Seg_10767" s="T956">делаться-EP-PTCP.PST</ta>
            <ta e="T959" id="Seg_10768" s="T958">тот-3SG-2SG.[NOM]</ta>
            <ta e="T961" id="Seg_10769" s="T960">кнопка-PL-PROPR.[NOM]</ta>
            <ta e="T962" id="Seg_10770" s="T961">что.[NOM]</ta>
            <ta e="T963" id="Seg_10771" s="T962">INDEF</ta>
            <ta e="T964" id="Seg_10772" s="T963">EMPH</ta>
            <ta e="T965" id="Seg_10773" s="T964">красивый</ta>
            <ta e="T966" id="Seg_10774" s="T965">удивляться-EMOT-PRS-1SG</ta>
            <ta e="T967" id="Seg_10775" s="T966">EMPH</ta>
            <ta e="T968" id="Seg_10776" s="T967">красивый</ta>
            <ta e="T970" id="Seg_10777" s="T969">вот</ta>
            <ta e="T971" id="Seg_10778" s="T970">трогать-EP-NEG.[IMP.2SG]</ta>
            <ta e="T972" id="Seg_10779" s="T971">ладно</ta>
            <ta e="T973" id="Seg_10780" s="T972">быть-FUT.[3SG]</ta>
            <ta e="T974" id="Seg_10781" s="T973">говорить-PRS-3PL</ta>
            <ta e="T976" id="Seg_10782" s="T975">трогать-CAUS-PTCP.HAB-3SG</ta>
            <ta e="T977" id="Seg_10783" s="T976">нет-3PL</ta>
            <ta e="T978" id="Seg_10784" s="T977">EMPH</ta>
            <ta e="T980" id="Seg_10785" s="T979">разбивать-FUT-2SG</ta>
            <ta e="T981" id="Seg_10786" s="T980">эй</ta>
            <ta e="T982" id="Seg_10787" s="T981">AFFIRM</ta>
            <ta e="T984" id="Seg_10788" s="T983">вот</ta>
            <ta e="T985" id="Seg_10789" s="T984">класть-PST1-1SG</ta>
            <ta e="T986" id="Seg_10790" s="T985">очень</ta>
            <ta e="T987" id="Seg_10791" s="T986">видеть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T988" id="Seg_10792" s="T987">хотеть-PRS-1SG</ta>
            <ta e="T989" id="Seg_10793" s="T988">трогать-FREQ-PTCP.FUT-1SG-ACC</ta>
            <ta e="T990" id="Seg_10794" s="T989">хотеть-PRS-1SG</ta>
            <ta e="T991" id="Seg_10795" s="T990">EMPH</ta>
            <ta e="T993" id="Seg_10796" s="T992">тот-ABL</ta>
            <ta e="T994" id="Seg_10797" s="T993">кто.[NOM]</ta>
            <ta e="T995" id="Seg_10798" s="T994">кто-ACC</ta>
            <ta e="T996" id="Seg_10799" s="T995">принести-PST2-3SG</ta>
            <ta e="T997" id="Seg_10800" s="T996">1SG-DAT/LOC</ta>
            <ta e="T999" id="Seg_10801" s="T998">компас-DIM.[NOM]</ta>
            <ta e="T1000" id="Seg_10802" s="T999">маленький.[NOM]</ta>
            <ta e="T1002" id="Seg_10803" s="T1001">тот-ACC</ta>
            <ta e="T1003" id="Seg_10804" s="T1002">всегда</ta>
            <ta e="T1004" id="Seg_10805" s="T1003">играть-CVB.SIM</ta>
            <ta e="T1005" id="Seg_10806" s="T1004">лежать-HAB-1SG</ta>
            <ta e="T1006" id="Seg_10807" s="T1005">вот.беда</ta>
            <ta e="T1007" id="Seg_10808" s="T1006">красивый-ADVZ</ta>
            <ta e="T1008" id="Seg_10809" s="T1007">Q</ta>
            <ta e="T1009" id="Seg_10810" s="T1008">сначала</ta>
            <ta e="T1012" id="Seg_10811" s="T1011">принести-PST2-3SG</ta>
            <ta e="T1013" id="Seg_10812" s="T1012">этот.[NOM]</ta>
            <ta e="T1014" id="Seg_10813" s="T1013">как</ta>
            <ta e="T1015" id="Seg_10814" s="T1014">игрушки-ACC</ta>
            <ta e="T1016" id="Seg_10815" s="T1015">вот</ta>
            <ta e="T1018" id="Seg_10816" s="T1017">о</ta>
            <ta e="T1019" id="Seg_10817" s="T1018">1SG.[NOM]</ta>
            <ta e="T1020" id="Seg_10818" s="T1019">самый</ta>
            <ta e="T1021" id="Seg_10819" s="T1020">NEG</ta>
            <ta e="T1023" id="Seg_10820" s="T1022">красивый-ADVZ</ta>
            <ta e="T1024" id="Seg_10821" s="T1023">EMPH</ta>
            <ta e="T1026" id="Seg_10822" s="T1025">о</ta>
            <ta e="T1027" id="Seg_10823" s="T1026">2SG-DAT/LOC</ta>
            <ta e="T1029" id="Seg_10824" s="T1028">говорить-PRS.[3SG]</ta>
            <ta e="T1031" id="Seg_10825" s="T1030">вот</ta>
            <ta e="T1032" id="Seg_10826" s="T1031">тот</ta>
            <ta e="T1033" id="Seg_10827" s="T1032">разговаривать-PRS-3PL</ta>
            <ta e="T1034" id="Seg_10828" s="T1033">очевидно</ta>
            <ta e="T1035" id="Seg_10829" s="T1034">вот</ta>
            <ta e="T1036" id="Seg_10830" s="T1035">1SG.[NOM]</ta>
            <ta e="T1037" id="Seg_10831" s="T1036">спать-CVB.SEQ</ta>
            <ta e="T1038" id="Seg_10832" s="T1037">оставаться-PST2-1SG</ta>
            <ta e="T1040" id="Seg_10833" s="T1039">утром</ta>
            <ta e="T1041" id="Seg_10834" s="T1040">уже</ta>
            <ta e="T1042" id="Seg_10835" s="T1041">просыпаться-PRS-1SG</ta>
            <ta e="T1043" id="Seg_10836" s="T1042">о</ta>
            <ta e="T1044" id="Seg_10837" s="T1043">очаг.[NOM]</ta>
            <ta e="T1045" id="Seg_10838" s="T1044">опять</ta>
            <ta e="T1046" id="Seg_10839" s="T1045">подниматься-EP-CAUS-CVB.SIM</ta>
            <ta e="T1047" id="Seg_10840" s="T1046">стоять-PRS.[3SG]</ta>
            <ta e="T1049" id="Seg_10841" s="T1048">красивый-ADVZ</ta>
            <ta e="T1050" id="Seg_10842" s="T1049">Q</ta>
            <ta e="T1051" id="Seg_10843" s="T1050">кто.[NOM]</ta>
            <ta e="T1052" id="Seg_10844" s="T1051">NEG</ta>
            <ta e="T1053" id="Seg_10845" s="T1052">NEG</ta>
            <ta e="T1054" id="Seg_10846" s="T1053">дом-1PL.[NOM]</ta>
            <ta e="T1055" id="Seg_10847" s="T1054">нутро-3SG-DAT/LOC</ta>
            <ta e="T1057" id="Seg_10848" s="T1056">тот</ta>
            <ta e="T1058" id="Seg_10849" s="T1057">олень-3PL-ACC</ta>
            <ta e="T1059" id="Seg_10850" s="T1058">хватать-PRS-PL</ta>
            <ta e="T1060" id="Seg_10851" s="T1059">быть-PST2.[3SG]</ta>
            <ta e="T1062" id="Seg_10852" s="T1061">поймать-CVB.SIM</ta>
            <ta e="T1063" id="Seg_10853" s="T1062">выйти-EP-PST2-3PL</ta>
            <ta e="T1065" id="Seg_10854" s="T1064">сосед-PL-PROPR.[NOM]</ta>
            <ta e="T1066" id="Seg_10855" s="T1065">быть-PST1-1PL</ta>
            <ta e="T1067" id="Seg_10856" s="T1066">так</ta>
            <ta e="T1068" id="Seg_10857" s="T1067">INDEF</ta>
            <ta e="T1069" id="Seg_10858" s="T1068">есть</ta>
            <ta e="T1070" id="Seg_10859" s="T1069">быть-PST1-3PL</ta>
            <ta e="T1071" id="Seg_10860" s="T1070">сосед-PL-1PL.[NOM]</ta>
            <ta e="T1073" id="Seg_10861" s="T1072">вот</ta>
            <ta e="T1074" id="Seg_10862" s="T1073">тот</ta>
            <ta e="T1075" id="Seg_10863" s="T1074">собирать-PST1-3PL</ta>
            <ta e="T1076" id="Seg_10864" s="T1075">стадо-3PL-ACC</ta>
            <ta e="T1077" id="Seg_10865" s="T1076">собирать-MED-PRS-3PL</ta>
            <ta e="T1078" id="Seg_10866" s="T1077">олень.[NOM]</ta>
            <ta e="T1079" id="Seg_10867" s="T1078">что.[NOM]</ta>
            <ta e="T1080" id="Seg_10868" s="T1079">кочевать-PRS-1PL</ta>
            <ta e="T1081" id="Seg_10869" s="T1080">наверное</ta>
            <ta e="T1082" id="Seg_10870" s="T1081">помнить-PRS-1SG</ta>
            <ta e="T1083" id="Seg_10871" s="T1082">куда</ta>
            <ta e="T1084" id="Seg_10872" s="T1083">INDEF</ta>
            <ta e="T1086" id="Seg_10873" s="T1085">о</ta>
            <ta e="T1087" id="Seg_10874" s="T1086">вот</ta>
            <ta e="T1088" id="Seg_10875" s="T1087">1SG-ACC</ta>
            <ta e="T1089" id="Seg_10876" s="T1088">одеваться-CAUS-CVB.SEQ</ta>
            <ta e="T1090" id="Seg_10877" s="T1089">%%-PST1-3PL</ta>
            <ta e="T1092" id="Seg_10878" s="T1091">сани-DAT/LOC</ta>
            <ta e="T1093" id="Seg_10879" s="T1092">связывать-CVB.SEQ</ta>
            <ta e="T1094" id="Seg_10880" s="T1093">бросать-PST2-3PL</ta>
            <ta e="T1095" id="Seg_10881" s="T1094">%%-CVB.SEQ</ta>
            <ta e="T1096" id="Seg_10882" s="T1095">бросать-PST2-3PL</ta>
            <ta e="T1098" id="Seg_10883" s="T1097">вот</ta>
            <ta e="T1099" id="Seg_10884" s="T1098">куда</ta>
            <ta e="T1100" id="Seg_10885" s="T1099">идти-PRS-1PL</ta>
            <ta e="T1102" id="Seg_10886" s="T1101">и</ta>
            <ta e="T1103" id="Seg_10887" s="T1102">задняя.часть-1PL-ABL</ta>
            <ta e="T1104" id="Seg_10888" s="T1103">Q</ta>
            <ta e="T1105" id="Seg_10889" s="T1104">олень-PL-ACC</ta>
            <ta e="T1106" id="Seg_10890" s="T1105">водить-HAB-3PL</ta>
            <ta e="T1107" id="Seg_10891" s="T1106">кочевой-DAT/LOC</ta>
            <ta e="T1109" id="Seg_10892" s="T1108">пища-PROPR</ta>
            <ta e="T1110" id="Seg_10893" s="T1109">или</ta>
            <ta e="T1111" id="Seg_10894" s="T1110">дерево-PROPR</ta>
            <ta e="T1112" id="Seg_10895" s="T1111">кочевой.[NOM]</ta>
            <ta e="T1113" id="Seg_10896" s="T1112">водить-PTCP.HAB</ta>
            <ta e="T1114" id="Seg_10897" s="T1113">быть-PST1-1PL</ta>
            <ta e="T1116" id="Seg_10898" s="T1115">тот-PL-ABL</ta>
            <ta e="T1117" id="Seg_10899" s="T1116">бояться-CVB.SEQ</ta>
            <ta e="T1118" id="Seg_10900" s="T1117">плакать-CVB.SEQ</ta>
            <ta e="T1119" id="Seg_10901" s="T1118">громко.кричать-PRS-1SG</ta>
            <ta e="T1120" id="Seg_10902" s="T1119">говорить-CVB.SEQ</ta>
            <ta e="T1122" id="Seg_10903" s="T1121">бояться-EP-NEG.[IMP.2SG]</ta>
            <ta e="T1123" id="Seg_10904" s="T1122">кусать-FUT.[3SG]</ta>
            <ta e="T1124" id="Seg_10905" s="T1123">NEG-3SG</ta>
            <ta e="T1126" id="Seg_10906" s="T1125">этот</ta>
            <ta e="T1127" id="Seg_10907" s="T1126">вот</ta>
            <ta e="T1128" id="Seg_10908" s="T1127">устать-PST2-3PL</ta>
            <ta e="T1129" id="Seg_10909" s="T1128">вот</ta>
            <ta e="T1130" id="Seg_10910" s="T1129">дышать-PRS-3PL</ta>
            <ta e="T1133" id="Seg_10911" s="T1132">лицо-1SG-DAT/LOC</ta>
            <ta e="T1134" id="Seg_10912" s="T1133">здесь</ta>
            <ta e="T1136" id="Seg_10913" s="T1135">вот</ta>
            <ta e="T1137" id="Seg_10914" s="T1136">тот</ta>
            <ta e="T1138" id="Seg_10915" s="T1137">тот.[NOM]</ta>
            <ta e="T1139" id="Seg_10916" s="T1138">как</ta>
            <ta e="T1140" id="Seg_10917" s="T1139">кочевать-CVB.SIM</ta>
            <ta e="T1141" id="Seg_10918" s="T1140">идти-PTCP.HAB</ta>
            <ta e="T1142" id="Seg_10919" s="T1141">быть-PST1-1PL</ta>
            <ta e="T1143" id="Seg_10920" s="T1142">кочевать-CVB.SIM</ta>
            <ta e="T1144" id="Seg_10921" s="T1143">идти-PTCP.HAB</ta>
            <ta e="T1145" id="Seg_10922" s="T1144">уезжать-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T1146" id="Seg_10923" s="T1145">идти-PTCP.HAB</ta>
            <ta e="T1147" id="Seg_10924" s="T1146">быть-PST1-1SG</ta>
            <ta e="T1148" id="Seg_10925" s="T1147">3PL-ACC</ta>
            <ta e="T1149" id="Seg_10926" s="T1148">с</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_10927" s="T1">adv</ta>
            <ta e="T3" id="Seg_10928" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_10929" s="T3">adj.[n:case]</ta>
            <ta e="T5" id="Seg_10930" s="T4">v-v:mood-v:temp.pn</ta>
            <ta e="T6" id="Seg_10931" s="T5">propr-n:case</ta>
            <ta e="T7" id="Seg_10932" s="T6">n-n:(poss).[n:case]</ta>
            <ta e="T8" id="Seg_10933" s="T7">propr-n:case</ta>
            <ta e="T9" id="Seg_10934" s="T8">cardnum</ta>
            <ta e="T10" id="Seg_10935" s="T9">cardnum</ta>
            <ta e="T11" id="Seg_10936" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_10937" s="T11">adv</ta>
            <ta e="T13" id="Seg_10938" s="T12">que-que&gt;adj</ta>
            <ta e="T14" id="Seg_10939" s="T13">n.[n:case]</ta>
            <ta e="T15" id="Seg_10940" s="T14">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T17" id="Seg_10941" s="T16">adj-n:(num).[n:case]</ta>
            <ta e="T18" id="Seg_10942" s="T17">adj-n:(poss).[n:case]</ta>
            <ta e="T19" id="Seg_10943" s="T18">adv</ta>
            <ta e="T20" id="Seg_10944" s="T19">v-v:ptcp</ta>
            <ta e="T21" id="Seg_10945" s="T20">v-v:tense-v:poss.pn</ta>
            <ta e="T23" id="Seg_10946" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_10947" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_10948" s="T24">adj.[n:case]</ta>
            <ta e="T26" id="Seg_10949" s="T25">v-v:tense-v:poss.pn</ta>
            <ta e="T27" id="Seg_10950" s="T26">adv</ta>
            <ta e="T29" id="Seg_10951" s="T28">n-n:(num).[n:case]</ta>
            <ta e="T30" id="Seg_10952" s="T29">que-pro:(num).[pro:case]</ta>
            <ta e="T31" id="Seg_10953" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_10954" s="T31">v-v:tense-v:poss.pn</ta>
            <ta e="T34" id="Seg_10955" s="T33">dempro-pro:case</ta>
            <ta e="T35" id="Seg_10956" s="T34">adv</ta>
            <ta e="T36" id="Seg_10957" s="T35">n-n:(poss)-n&gt;adj.[n:case]</ta>
            <ta e="T37" id="Seg_10958" s="T36">n-n:poss-n:case</ta>
            <ta e="T38" id="Seg_10959" s="T37">n-n:(poss).[n:case]</ta>
            <ta e="T39" id="Seg_10960" s="T38">adj</ta>
            <ta e="T40" id="Seg_10961" s="T39">n-n:poss-n:case</ta>
            <ta e="T41" id="Seg_10962" s="T40">v-v:mood-v:pred.pn</ta>
            <ta e="T42" id="Seg_10963" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_10964" s="T42">n</ta>
            <ta e="T44" id="Seg_10965" s="T43">v-v:mood-v:pred.pn</ta>
            <ta e="T45" id="Seg_10966" s="T44">adj</ta>
            <ta e="T46" id="Seg_10967" s="T45">n-n:poss-n:case</ta>
            <ta e="T47" id="Seg_10968" s="T46">n.[n:case]</ta>
            <ta e="T48" id="Seg_10969" s="T47">v-v:mood-v:pred.pn</ta>
            <ta e="T49" id="Seg_10970" s="T48">adj</ta>
            <ta e="T50" id="Seg_10971" s="T49">n-n:poss-n:case</ta>
            <ta e="T51" id="Seg_10972" s="T50">pers-pro:case</ta>
            <ta e="T52" id="Seg_10973" s="T51">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T54" id="Seg_10974" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_10975" s="T54">dempro</ta>
            <ta e="T56" id="Seg_10976" s="T55">n-n:(poss).[n:case]</ta>
            <ta e="T57" id="Seg_10977" s="T56">que</ta>
            <ta e="T58" id="Seg_10978" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_10979" s="T58">v-v:cvb</ta>
            <ta e="T60" id="Seg_10980" s="T59">v-v:tense-n:(poss)</ta>
            <ta e="T62" id="Seg_10981" s="T61">n-n&gt;v-v:cvb</ta>
            <ta e="T63" id="Seg_10982" s="T62">v-v:tense-v:poss.pn</ta>
            <ta e="T64" id="Seg_10983" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_10984" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_10985" s="T65">v-v:tense-v:pred.pn</ta>
            <ta e="T67" id="Seg_10986" s="T66">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T69" id="Seg_10987" s="T68">que-pro:case</ta>
            <ta e="T70" id="Seg_10988" s="T69">n-n:(poss).[n:case]</ta>
            <ta e="T71" id="Seg_10989" s="T70">n-n:poss-n:case</ta>
            <ta e="T72" id="Seg_10990" s="T71">v-v:tense-v:pred.pn</ta>
            <ta e="T74" id="Seg_10991" s="T73">v-v:cvb</ta>
            <ta e="T75" id="Seg_10992" s="T74">v-v:tense-v:pred.pn</ta>
            <ta e="T78" id="Seg_10993" s="T77">n-n&gt;adj.[n:case]</ta>
            <ta e="T79" id="Seg_10994" s="T78">v-v:tense-v:poss.pn</ta>
            <ta e="T81" id="Seg_10995" s="T80">dempro</ta>
            <ta e="T82" id="Seg_10996" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_10997" s="T82">v-v:tense.[v:pred.pn]</ta>
            <ta e="T85" id="Seg_10998" s="T84">n-n:(ins)-n&gt;n.[n:case]</ta>
            <ta e="T86" id="Seg_10999" s="T85">ptcl</ta>
            <ta e="T88" id="Seg_11000" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_11001" s="T88">v-v:mood.[v:pred.pn]</ta>
            <ta e="T90" id="Seg_11002" s="T89">n.[n:case]</ta>
            <ta e="T91" id="Seg_11003" s="T90">v-v:cvb</ta>
            <ta e="T93" id="Seg_11004" s="T92">interj</ta>
            <ta e="T94" id="Seg_11005" s="T93">n-n:(poss).[n:case]</ta>
            <ta e="T95" id="Seg_11006" s="T94">v-v:cvb</ta>
            <ta e="T96" id="Seg_11007" s="T95">v-v:tense.[v:poss.pn]</ta>
            <ta e="T97" id="Seg_11008" s="T96">v-v:cvb</ta>
            <ta e="T98" id="Seg_11009" s="T97">interj</ta>
            <ta e="T99" id="Seg_11010" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_11011" s="T99">adj.[n:case]</ta>
            <ta e="T101" id="Seg_11012" s="T100">n.[n:case]</ta>
            <ta e="T102" id="Seg_11013" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_11014" s="T102">v-v:ptcp</ta>
            <ta e="T104" id="Seg_11015" s="T103">v-v:tense-v:poss.pn</ta>
            <ta e="T105" id="Seg_11016" s="T104">ptcl</ta>
            <ta e="T108" id="Seg_11017" s="T107">dempro</ta>
            <ta e="T109" id="Seg_11018" s="T108">n.[n:case]</ta>
            <ta e="T110" id="Seg_11019" s="T109">v-v:cvb</ta>
            <ta e="T111" id="Seg_11020" s="T110">v-v:tense-v:poss.pn</ta>
            <ta e="T112" id="Seg_11021" s="T111">ptcl</ta>
            <ta e="T114" id="Seg_11022" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_11023" s="T114">dempro</ta>
            <ta e="T116" id="Seg_11024" s="T115">n.[n:case]</ta>
            <ta e="T117" id="Seg_11025" s="T116">n.[n:case]</ta>
            <ta e="T118" id="Seg_11026" s="T117">ptcl-n:(num)</ta>
            <ta e="T120" id="Seg_11027" s="T119">interj</ta>
            <ta e="T121" id="Seg_11028" s="T120">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T122" id="Seg_11029" s="T121">v-v:tense.[v:pred.pn]</ta>
            <ta e="T123" id="Seg_11030" s="T122">v-v:cvb</ta>
            <ta e="T124" id="Seg_11031" s="T123">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T125" id="Seg_11032" s="T124">v-v:cvb</ta>
            <ta e="T126" id="Seg_11033" s="T125">interj</ta>
            <ta e="T128" id="Seg_11034" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_11035" s="T128">dempro</ta>
            <ta e="T130" id="Seg_11036" s="T129">n-n:(poss).[n:case]</ta>
            <ta e="T131" id="Seg_11037" s="T130">v-v:tense.[v:pred.pn]</ta>
            <ta e="T132" id="Seg_11038" s="T131">adj</ta>
            <ta e="T133" id="Seg_11039" s="T132">n-n:(poss).[n:case]</ta>
            <ta e="T134" id="Seg_11040" s="T133">ptcl</ta>
            <ta e="T136" id="Seg_11041" s="T135">interj</ta>
            <ta e="T137" id="Seg_11042" s="T136">v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T138" id="Seg_11043" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_11044" s="T138">pers.[pro:case]</ta>
            <ta e="T140" id="Seg_11045" s="T139">n-n:poss-n:case</ta>
            <ta e="T141" id="Seg_11046" s="T140">v-v:tense.[v:poss.pn]</ta>
            <ta e="T142" id="Seg_11047" s="T141">v-v:tense.[v:poss.pn]</ta>
            <ta e="T144" id="Seg_11048" s="T143">v.[v:mood.pn]</ta>
            <ta e="T145" id="Seg_11049" s="T144">v.[v:mood.pn]</ta>
            <ta e="T146" id="Seg_11050" s="T145">v.[v:mood.pn]</ta>
            <ta e="T147" id="Seg_11051" s="T146">v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T149" id="Seg_11052" s="T148">v-v:tense.[v:pred.pn]</ta>
            <ta e="T150" id="Seg_11053" s="T149">que.[pro:case]</ta>
            <ta e="T151" id="Seg_11054" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_11055" s="T151">ptcl</ta>
            <ta e="T154" id="Seg_11056" s="T153">n-n:poss-n:case</ta>
            <ta e="T155" id="Seg_11057" s="T154">v.[v:mood.pn]</ta>
            <ta e="T156" id="Seg_11058" s="T155">v.[v:mood.pn]</ta>
            <ta e="T157" id="Seg_11059" s="T156">v-v:tense.[v:pred.pn]</ta>
            <ta e="T158" id="Seg_11060" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_11061" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_11062" s="T159">v-v:tense-v:poss.pn</ta>
            <ta e="T162" id="Seg_11063" s="T161">dempro-pro:case</ta>
            <ta e="T163" id="Seg_11064" s="T162">adj</ta>
            <ta e="T164" id="Seg_11065" s="T163">n-n:(ins)-n:case</ta>
            <ta e="T165" id="Seg_11066" s="T164">v-v:tense.[v:pred.pn]</ta>
            <ta e="T166" id="Seg_11067" s="T165">adv</ta>
            <ta e="T168" id="Seg_11068" s="T167">v-v:mood.[v:pred.pn]</ta>
            <ta e="T169" id="Seg_11069" s="T168">ptcl</ta>
            <ta e="T171" id="Seg_11070" s="T170">adv</ta>
            <ta e="T172" id="Seg_11071" s="T171">n.[n:case]</ta>
            <ta e="T173" id="Seg_11072" s="T172">que.[pro:case]</ta>
            <ta e="T174" id="Seg_11073" s="T173">v-v:(ins)-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T176" id="Seg_11074" s="T175">dempro</ta>
            <ta e="T177" id="Seg_11075" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_11076" s="T177">n-n:(poss).[n:case]</ta>
            <ta e="T179" id="Seg_11077" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_11078" s="T179">n-n:(poss).[n:case]</ta>
            <ta e="T182" id="Seg_11079" s="T181">adv</ta>
            <ta e="T183" id="Seg_11080" s="T182">v-v:cvb</ta>
            <ta e="T184" id="Seg_11081" s="T183">v-v:cvb</ta>
            <ta e="T185" id="Seg_11082" s="T184">n-n:(poss).[n:case]</ta>
            <ta e="T186" id="Seg_11083" s="T185">v-v&gt;v-v:cvb</ta>
            <ta e="T187" id="Seg_11084" s="T186">v-v:tense.[v:pred.pn]</ta>
            <ta e="T188" id="Seg_11085" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_11086" s="T188">adj-n:(poss).[n:case]</ta>
            <ta e="T190" id="Seg_11087" s="T189">ptcl</ta>
            <ta e="T192" id="Seg_11088" s="T191">adv</ta>
            <ta e="T193" id="Seg_11089" s="T192">v-v:cvb</ta>
            <ta e="T194" id="Seg_11090" s="T193">v-v:cvb</ta>
            <ta e="T195" id="Seg_11091" s="T194">n-n:(num)-n&gt;adj.[n:case]</ta>
            <ta e="T196" id="Seg_11092" s="T195">ptcl</ta>
            <ta e="T198" id="Seg_11093" s="T197">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T199" id="Seg_11094" s="T198">v-v:(neg)-v:pred.pn</ta>
            <ta e="T200" id="Seg_11095" s="T199">que.[pro:case]</ta>
            <ta e="T201" id="Seg_11096" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_11097" s="T201">n.[n:case]</ta>
            <ta e="T203" id="Seg_11098" s="T202">v-v:ptcp</ta>
            <ta e="T204" id="Seg_11099" s="T203">v-v:tense-v:poss.pn</ta>
            <ta e="T207" id="Seg_11100" s="T206">que-que&gt;v-v:ptcp</ta>
            <ta e="T208" id="Seg_11101" s="T207">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp-v:(num).[v:(case)]</ta>
            <ta e="T210" id="Seg_11102" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_11103" s="T210">dempro-pro:case</ta>
            <ta e="T212" id="Seg_11104" s="T211">v-v:cvb</ta>
            <ta e="T213" id="Seg_11105" s="T212">v-v:tense-v:poss.pn</ta>
            <ta e="T214" id="Seg_11106" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_11107" s="T214">n-n:poss-n:case</ta>
            <ta e="T216" id="Seg_11108" s="T215">v-v:cvb</ta>
            <ta e="T217" id="Seg_11109" s="T216">v-v:cvb</ta>
            <ta e="T219" id="Seg_11110" s="T218">n.[n:case]</ta>
            <ta e="T220" id="Seg_11111" s="T219">v.[v:mood.pn]</ta>
            <ta e="T221" id="Seg_11112" s="T220">v.[v:mood.pn]</ta>
            <ta e="T222" id="Seg_11113" s="T221">v.[v:mood.pn]</ta>
            <ta e="T223" id="Seg_11114" s="T222">v-v:tense-v:pred.pn</ta>
            <ta e="T224" id="Seg_11115" s="T223">que-pro:(poss)-pro:case</ta>
            <ta e="T225" id="Seg_11116" s="T224">v-v:cvb</ta>
            <ta e="T226" id="Seg_11117" s="T225">v-v:tense-v:poss.pn</ta>
            <ta e="T227" id="Seg_11118" s="T226">n-n:(num)-n:poss-n:case</ta>
            <ta e="T228" id="Seg_11119" s="T227">v-v:cvb</ta>
            <ta e="T229" id="Seg_11120" s="T228">v-v:tense-v:poss.pn</ta>
            <ta e="T230" id="Seg_11121" s="T229">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_11122" s="T230">n-n:(num)-n:poss-n:case</ta>
            <ta e="T233" id="Seg_11123" s="T232">interj</ta>
            <ta e="T234" id="Seg_11124" s="T233">que</ta>
            <ta e="T235" id="Seg_11125" s="T234">n</ta>
            <ta e="T236" id="Seg_11126" s="T235">n-n:(poss).[n:case]</ta>
            <ta e="T237" id="Seg_11127" s="T236">que</ta>
            <ta e="T238" id="Seg_11128" s="T237">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T239" id="Seg_11129" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_11130" s="T239">n-n:case</ta>
            <ta e="T241" id="Seg_11131" s="T240">v-v&gt;v-v:cvb</ta>
            <ta e="T242" id="Seg_11132" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_11133" s="T242">v-v&gt;v-v:cvb</ta>
            <ta e="T245" id="Seg_11134" s="T244">v.[v:mood.pn]</ta>
            <ta e="T246" id="Seg_11135" s="T245">adv</ta>
            <ta e="T247" id="Seg_11136" s="T246">adj-n:poss-n:case</ta>
            <ta e="T248" id="Seg_11137" s="T247">v-v:tense-v:poss.pn</ta>
            <ta e="T249" id="Seg_11138" s="T248">v-v:tense.[v:pred.pn]</ta>
            <ta e="T251" id="Seg_11139" s="T250">interj</ta>
            <ta e="T252" id="Seg_11140" s="T251">n.[n:case]</ta>
            <ta e="T253" id="Seg_11141" s="T252">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T256" id="Seg_11142" s="T255">v.[v:mood.pn]</ta>
            <ta e="T257" id="Seg_11143" s="T256">v.[v:mood.pn]</ta>
            <ta e="T259" id="Seg_11144" s="T258">que.[pro:case]</ta>
            <ta e="T260" id="Seg_11145" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_11146" s="T260">n-n:case</ta>
            <ta e="T265" id="Seg_11147" s="T264">dempro-pro:case</ta>
            <ta e="T266" id="Seg_11148" s="T265">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T268" id="Seg_11149" s="T267">ptcl</ta>
            <ta e="T271" id="Seg_11150" s="T270">v.[v:mood.pn]</ta>
            <ta e="T273" id="Seg_11151" s="T272">v-v:cvb</ta>
            <ta e="T274" id="Seg_11152" s="T273">v-v:tense.[v:pred.pn]</ta>
            <ta e="T275" id="Seg_11153" s="T274">ptcl</ta>
            <ta e="T277" id="Seg_11154" s="T276">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T278" id="Seg_11155" s="T277">v-v:cvb</ta>
            <ta e="T279" id="Seg_11156" s="T278">v-v:cvb</ta>
            <ta e="T281" id="Seg_11157" s="T280">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T282" id="Seg_11158" s="T281">v-v:cvb</ta>
            <ta e="T283" id="Seg_11159" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_11160" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_11161" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_11162" s="T285">v-v:tense-v:poss.pn</ta>
            <ta e="T287" id="Seg_11163" s="T286">adv</ta>
            <ta e="T288" id="Seg_11164" s="T287">que.[pro:case]</ta>
            <ta e="T289" id="Seg_11165" s="T288">quant</ta>
            <ta e="T290" id="Seg_11166" s="T289">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T291" id="Seg_11167" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_11168" s="T291">dempro</ta>
            <ta e="T293" id="Seg_11169" s="T292">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T296" id="Seg_11170" s="T295">que-pro:case</ta>
            <ta e="T297" id="Seg_11171" s="T296">v-v:tense.[v:pred.pn]</ta>
            <ta e="T298" id="Seg_11172" s="T297">v-v:ptcp</ta>
            <ta e="T299" id="Seg_11173" s="T298">n-n:case</ta>
            <ta e="T301" id="Seg_11174" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_11175" s="T301">adv</ta>
            <ta e="T303" id="Seg_11176" s="T302">v-v:cvb</ta>
            <ta e="T304" id="Seg_11177" s="T303">v-v:tense-v:poss.pn</ta>
            <ta e="T305" id="Seg_11178" s="T304">ptcl</ta>
            <ta e="T307" id="Seg_11179" s="T306">interj</ta>
            <ta e="T310" id="Seg_11180" s="T309">interj</ta>
            <ta e="T311" id="Seg_11181" s="T310">n-n:(num)-n:poss-n:case</ta>
            <ta e="T312" id="Seg_11182" s="T311">adj-n:poss-n:case</ta>
            <ta e="T313" id="Seg_11183" s="T312">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T314" id="Seg_11184" s="T313">adv</ta>
            <ta e="T316" id="Seg_11185" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_11186" s="T316">dempro</ta>
            <ta e="T318" id="Seg_11187" s="T317">v-v:tense-v:poss.pn</ta>
            <ta e="T319" id="Seg_11188" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_11189" s="T319">n-n:(num)-n:poss-n:case</ta>
            <ta e="T321" id="Seg_11190" s="T320">post</ta>
            <ta e="T323" id="Seg_11191" s="T322">n.[n:case]</ta>
            <ta e="T324" id="Seg_11192" s="T323">n-n:poss-n:case</ta>
            <ta e="T325" id="Seg_11193" s="T324">v-v:tense-v:poss.pn</ta>
            <ta e="T326" id="Seg_11194" s="T325">adv</ta>
            <ta e="T327" id="Seg_11195" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_11196" s="T327">v-v:tense-v:pred.pn</ta>
            <ta e="T330" id="Seg_11197" s="T329">n-n:(poss).[n:case]</ta>
            <ta e="T331" id="Seg_11198" s="T330">v-v:tense-v:poss.pn</ta>
            <ta e="T332" id="Seg_11199" s="T331">v-v:tense.[v:pred.pn]</ta>
            <ta e="T333" id="Seg_11200" s="T332">v-v:cvb</ta>
            <ta e="T334" id="Seg_11201" s="T333">v-v:tense.[v:pred.pn]</ta>
            <ta e="T336" id="Seg_11202" s="T335">que.[pro:case]</ta>
            <ta e="T337" id="Seg_11203" s="T336">n-n&gt;adj.[n:case]</ta>
            <ta e="T338" id="Seg_11204" s="T337">v-v:tense-v:poss.pn</ta>
            <ta e="T339" id="Seg_11205" s="T338">pers.[pro:case]</ta>
            <ta e="T340" id="Seg_11206" s="T339">propr</ta>
            <ta e="T341" id="Seg_11207" s="T340">adj</ta>
            <ta e="T342" id="Seg_11208" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_11209" s="T342">adj</ta>
            <ta e="T344" id="Seg_11210" s="T343">ptcl</ta>
            <ta e="T345" id="Seg_11211" s="T344">n-n&gt;n.[n:case]</ta>
            <ta e="T346" id="Seg_11212" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_11213" s="T346">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T349" id="Seg_11214" s="T348">que.[pro:case]</ta>
            <ta e="T351" id="Seg_11215" s="T350">v-v:tense-v:poss.pn</ta>
            <ta e="T352" id="Seg_11216" s="T351">ptcl</ta>
            <ta e="T353" id="Seg_11217" s="T352">que-pro:case</ta>
            <ta e="T354" id="Seg_11218" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_11219" s="T354">adj-n:poss-n:case</ta>
            <ta e="T356" id="Seg_11220" s="T355">v-v:cvb</ta>
            <ta e="T357" id="Seg_11221" s="T356">v-v:tense.[v:poss.pn]</ta>
            <ta e="T359" id="Seg_11222" s="T358">interj</ta>
            <ta e="T360" id="Seg_11223" s="T359">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T361" id="Seg_11224" s="T360">v-v:cvb</ta>
            <ta e="T362" id="Seg_11225" s="T361">post</ta>
            <ta e="T363" id="Seg_11226" s="T362">cardnum</ta>
            <ta e="T364" id="Seg_11227" s="T363">n-n:case</ta>
            <ta e="T365" id="Seg_11228" s="T364">v-v:tense-n:(poss)</ta>
            <ta e="T367" id="Seg_11229" s="T366">dempro</ta>
            <ta e="T368" id="Seg_11230" s="T367">v.[v:mood.pn]</ta>
            <ta e="T369" id="Seg_11231" s="T368">v-v:ptcp-v:(case)</ta>
            <ta e="T370" id="Seg_11232" s="T369">ptcl</ta>
            <ta e="T372" id="Seg_11233" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_11234" s="T372">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T374" id="Seg_11235" s="T373">que-pro:case</ta>
            <ta e="T375" id="Seg_11236" s="T374">que-que&gt;v-v:ptcp</ta>
            <ta e="T376" id="Seg_11237" s="T375">v-v:tense-v:poss.pn</ta>
            <ta e="T377" id="Seg_11238" s="T376">adv</ta>
            <ta e="T378" id="Seg_11239" s="T377">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T379" id="Seg_11240" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_11241" s="T379">v-v:mood-v:temp.pn</ta>
            <ta e="T381" id="Seg_11242" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_11243" s="T381">n-n:case</ta>
            <ta e="T384" id="Seg_11244" s="T383">n.[n:case]</ta>
            <ta e="T385" id="Seg_11245" s="T384">n.[n:case]</ta>
            <ta e="T386" id="Seg_11246" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_11247" s="T386">que.[pro:case]</ta>
            <ta e="T388" id="Seg_11248" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_11249" s="T388">n</ta>
            <ta e="T390" id="Seg_11250" s="T389">n.[n:case]</ta>
            <ta e="T391" id="Seg_11251" s="T390">interj</ta>
            <ta e="T392" id="Seg_11252" s="T391">adj</ta>
            <ta e="T393" id="Seg_11253" s="T392">n-n:case</ta>
            <ta e="T394" id="Seg_11254" s="T393">adj-adj&gt;adj</ta>
            <ta e="T395" id="Seg_11255" s="T394">n-n:poss-n:case</ta>
            <ta e="T396" id="Seg_11256" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_11257" s="T396">adj</ta>
            <ta e="T399" id="Seg_11258" s="T398">dempro</ta>
            <ta e="T400" id="Seg_11259" s="T399">que-pro:case</ta>
            <ta e="T401" id="Seg_11260" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_11261" s="T401">v-v:cvb</ta>
            <ta e="T403" id="Seg_11262" s="T402">v-v:tense-v:poss.pn</ta>
            <ta e="T404" id="Seg_11263" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_11264" s="T404">v-v:cvb</ta>
            <ta e="T406" id="Seg_11265" s="T405">v-v:mood.[v:pred.pn]</ta>
            <ta e="T407" id="Seg_11266" s="T406">dempro.[pro:case]</ta>
            <ta e="T408" id="Seg_11267" s="T407">que.[pro:case]</ta>
            <ta e="T409" id="Seg_11268" s="T408">n-n&gt;n.[n:case]</ta>
            <ta e="T410" id="Seg_11269" s="T409">v-v:mood.[v:pred.pn]</ta>
            <ta e="T411" id="Seg_11270" s="T410">ptcl</ta>
            <ta e="T412" id="Seg_11271" s="T411">v-v:cvb</ta>
            <ta e="T413" id="Seg_11272" s="T412">v-v:cvb</ta>
            <ta e="T415" id="Seg_11273" s="T414">v-v:tense-v:poss.pn</ta>
            <ta e="T417" id="Seg_11274" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_11275" s="T417">v-v:tense.[v:pred.pn]</ta>
            <ta e="T419" id="Seg_11276" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_11277" s="T419">pers-pro:case</ta>
            <ta e="T421" id="Seg_11278" s="T420">post</ta>
            <ta e="T422" id="Seg_11279" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_11280" s="T422">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T424" id="Seg_11281" s="T423">que</ta>
            <ta e="T425" id="Seg_11282" s="T424">conj</ta>
            <ta e="T426" id="Seg_11283" s="T425">v-v:tense-v:poss.pn</ta>
            <ta e="T427" id="Seg_11284" s="T1150">v-v:(ins)-v&gt;v-v:(ins)-v&gt;n.[n:case]</ta>
            <ta e="T428" id="Seg_11285" s="T427">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T429" id="Seg_11286" s="T428">dempro</ta>
            <ta e="T430" id="Seg_11287" s="T429">post</ta>
            <ta e="T431" id="Seg_11288" s="T430">que</ta>
            <ta e="T432" id="Seg_11289" s="T431">v=ptcl</ta>
            <ta e="T433" id="Seg_11290" s="T432">n.[n:case]</ta>
            <ta e="T434" id="Seg_11291" s="T433">n.[n:case]</ta>
            <ta e="T435" id="Seg_11292" s="T434">v-v:tense-v:pred.pn</ta>
            <ta e="T436" id="Seg_11293" s="T435">v.[v:mood.pn]</ta>
            <ta e="T437" id="Seg_11294" s="T436">v.[v:mood.pn]</ta>
            <ta e="T438" id="Seg_11295" s="T437">propr-n:case</ta>
            <ta e="T440" id="Seg_11296" s="T439">propr</ta>
            <ta e="T441" id="Seg_11297" s="T440">propr</ta>
            <ta e="T442" id="Seg_11298" s="T441">v-v:mood-v:pred.pn</ta>
            <ta e="T444" id="Seg_11299" s="T443">propr.[n:case]</ta>
            <ta e="T445" id="Seg_11300" s="T444">v.[v:mood.pn]</ta>
            <ta e="T447" id="Seg_11301" s="T446">que-pro:case</ta>
            <ta e="T448" id="Seg_11302" s="T447">v-v:cvb</ta>
            <ta e="T449" id="Seg_11303" s="T448">v-v:tense-v:poss.pn</ta>
            <ta e="T450" id="Seg_11304" s="T449">v-v:tense-v:pred.pn</ta>
            <ta e="T451" id="Seg_11305" s="T450">n-n:case</ta>
            <ta e="T452" id="Seg_11306" s="T451">v-v:cvb</ta>
            <ta e="T453" id="Seg_11307" s="T452">v-v:tense-v:poss.pn</ta>
            <ta e="T455" id="Seg_11308" s="T454">interj</ta>
            <ta e="T456" id="Seg_11309" s="T455">n</ta>
            <ta e="T457" id="Seg_11310" s="T456">v.[v:mood.pn]</ta>
            <ta e="T458" id="Seg_11311" s="T457">v.[v:mood.pn]</ta>
            <ta e="T460" id="Seg_11312" s="T459">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T461" id="Seg_11313" s="T460">adv</ta>
            <ta e="T462" id="Seg_11314" s="T461">dempro.[pro:case]</ta>
            <ta e="T463" id="Seg_11315" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_11316" s="T463">v-v:tense-v:poss.pn</ta>
            <ta e="T466" id="Seg_11317" s="T465">propr</ta>
            <ta e="T467" id="Seg_11318" s="T466">propr</ta>
            <ta e="T468" id="Seg_11319" s="T467">propr</ta>
            <ta e="T469" id="Seg_11320" s="T468">v-v:tense-v:pred.pn</ta>
            <ta e="T470" id="Seg_11321" s="T469">adv</ta>
            <ta e="T471" id="Seg_11322" s="T470">v-v:cvb</ta>
            <ta e="T472" id="Seg_11323" s="T471">v-v:cvb</ta>
            <ta e="T473" id="Seg_11324" s="T472">ptcl</ta>
            <ta e="T475" id="Seg_11325" s="T474">interj</ta>
            <ta e="T476" id="Seg_11326" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_11327" s="T476">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T478" id="Seg_11328" s="T477">v-v:cvb</ta>
            <ta e="T479" id="Seg_11329" s="T478">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T480" id="Seg_11330" s="T479">v-v:cvb</ta>
            <ta e="T482" id="Seg_11331" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_11332" s="T482">v</ta>
            <ta e="T484" id="Seg_11333" s="T483">ptcl</ta>
            <ta e="T485" id="Seg_11334" s="T484">n-n:poss-n:case</ta>
            <ta e="T486" id="Seg_11335" s="T485">pers.[pro:case]</ta>
            <ta e="T487" id="Seg_11336" s="T486">v-v&gt;n.[n:case]</ta>
            <ta e="T490" id="Seg_11337" s="T489">adv</ta>
            <ta e="T491" id="Seg_11338" s="T490">v-v:cvb</ta>
            <ta e="T492" id="Seg_11339" s="T491">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T493" id="Seg_11340" s="T492">v-v:cvb</ta>
            <ta e="T495" id="Seg_11341" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_11342" s="T495">v-v:tense-v:pred.pn</ta>
            <ta e="T497" id="Seg_11343" s="T496">n.[n:case]</ta>
            <ta e="T498" id="Seg_11344" s="T497">v.[v:mood.pn]</ta>
            <ta e="T499" id="Seg_11345" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_11346" s="T499">ptcl</ta>
            <ta e="T502" id="Seg_11347" s="T501">v-v:tense.[v:pred.pn]</ta>
            <ta e="T503" id="Seg_11348" s="T502">v-v:tense.[v:pred.pn]</ta>
            <ta e="T504" id="Seg_11349" s="T503">v</ta>
            <ta e="T505" id="Seg_11350" s="T504">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T506" id="Seg_11351" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_11352" s="T506">n-n:case</ta>
            <ta e="T508" id="Seg_11353" s="T507">ptcl</ta>
            <ta e="T510" id="Seg_11354" s="T509">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T511" id="Seg_11355" s="T510">v-v:tense.[v:pred.pn]</ta>
            <ta e="T513" id="Seg_11356" s="T512">pers-pro:case</ta>
            <ta e="T514" id="Seg_11357" s="T513">v-v:cvb</ta>
            <ta e="T515" id="Seg_11358" s="T514">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T516" id="Seg_11359" s="T515">v-v:cvb</ta>
            <ta e="T518" id="Seg_11360" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_11361" s="T518">dempro</ta>
            <ta e="T520" id="Seg_11362" s="T519">n-n:(poss).[n:case]</ta>
            <ta e="T521" id="Seg_11363" s="T520">v-v:tense-v:poss.pn</ta>
            <ta e="T523" id="Seg_11364" s="T522">ptcl</ta>
            <ta e="T524" id="Seg_11365" s="T523">que.[pro:case]</ta>
            <ta e="T525" id="Seg_11366" s="T524">v-v:tense-v:pred.pn</ta>
            <ta e="T527" id="Seg_11367" s="T526">v.[v:mood.pn]</ta>
            <ta e="T528" id="Seg_11368" s="T527">v-v:tense-v:pred.pn</ta>
            <ta e="T529" id="Seg_11369" s="T528">pers-pro:case</ta>
            <ta e="T530" id="Seg_11370" s="T529">v-v:tense.[v:pred.pn]</ta>
            <ta e="T531" id="Seg_11371" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_11372" s="T531">v-v:tense-v:pred.pn</ta>
            <ta e="T533" id="Seg_11373" s="T532">v-v:tense.[v:pred.pn]</ta>
            <ta e="T534" id="Seg_11374" s="T533">n-n:case</ta>
            <ta e="T535" id="Seg_11375" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_11376" s="T535">interj</ta>
            <ta e="T537" id="Seg_11377" s="T536">conj</ta>
            <ta e="T539" id="Seg_11378" s="T538">v-v:cvb</ta>
            <ta e="T540" id="Seg_11379" s="T539">v-v:cvb</ta>
            <ta e="T541" id="Seg_11380" s="T540">v-v:tense-v:pred.pn</ta>
            <ta e="T542" id="Seg_11381" s="T541">ptcl</ta>
            <ta e="T544" id="Seg_11382" s="T543">v.[v:mood.pn]</ta>
            <ta e="T545" id="Seg_11383" s="T544">v-v:(neg).[v:mood.pn]</ta>
            <ta e="T547" id="Seg_11384" s="T546">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T548" id="Seg_11385" s="T547">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T549" id="Seg_11386" s="T548">v-v:tense.[v:pred.pn]</ta>
            <ta e="T550" id="Seg_11387" s="T549">n.[n:case]</ta>
            <ta e="T551" id="Seg_11388" s="T550">v-v:ptcp.[v:(case)]</ta>
            <ta e="T553" id="Seg_11389" s="T552">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T554" id="Seg_11390" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_11391" s="T554">v-v:cvb</ta>
            <ta e="T556" id="Seg_11392" s="T555">v-v:tense-v:poss.pn</ta>
            <ta e="T557" id="Seg_11393" s="T556">dempro</ta>
            <ta e="T558" id="Seg_11394" s="T557">n-n:case</ta>
            <ta e="T560" id="Seg_11395" s="T559">ptcl</ta>
            <ta e="T562" id="Seg_11396" s="T561">que-que&gt;v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T563" id="Seg_11397" s="T562">dempro</ta>
            <ta e="T564" id="Seg_11398" s="T563">adv</ta>
            <ta e="T565" id="Seg_11399" s="T564">n.[n:case]</ta>
            <ta e="T566" id="Seg_11400" s="T565">adj</ta>
            <ta e="T567" id="Seg_11401" s="T566">v-v:mood.[v:pred.pn]</ta>
            <ta e="T568" id="Seg_11402" s="T567">dempro</ta>
            <ta e="T569" id="Seg_11403" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_11404" s="T569">post</ta>
            <ta e="T571" id="Seg_11405" s="T570">que-pro:case</ta>
            <ta e="T572" id="Seg_11406" s="T571">v-v:tense.[v:pred.pn]</ta>
            <ta e="T574" id="Seg_11407" s="T573">dempro.[pro:case]</ta>
            <ta e="T575" id="Seg_11408" s="T574">n.[n:case]</ta>
            <ta e="T576" id="Seg_11409" s="T575">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T577" id="Seg_11410" s="T576">ptcl</ta>
            <ta e="T579" id="Seg_11411" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_11412" s="T579">dempro-pro:case</ta>
            <ta e="T581" id="Seg_11413" s="T580">n-n:(poss).[n:case]</ta>
            <ta e="T582" id="Seg_11414" s="T581">v-v:tense-v:poss.pn</ta>
            <ta e="T583" id="Seg_11415" s="T582">ptcl</ta>
            <ta e="T585" id="Seg_11416" s="T584">adj-adj&gt;adv</ta>
            <ta e="T586" id="Seg_11417" s="T585">conj</ta>
            <ta e="T587" id="Seg_11418" s="T586">que-pro:(poss).[pro:case]</ta>
            <ta e="T588" id="Seg_11419" s="T587">n-n:(poss).[n:case]</ta>
            <ta e="T589" id="Seg_11420" s="T588">ptcl</ta>
            <ta e="T591" id="Seg_11421" s="T590">n-n:(poss).[n:case]</ta>
            <ta e="T592" id="Seg_11422" s="T591">adv</ta>
            <ta e="T593" id="Seg_11423" s="T592">v-v:tense.[v:pred.pn]</ta>
            <ta e="T598" id="Seg_11424" s="T597">que.[pro:case]</ta>
            <ta e="T599" id="Seg_11425" s="T598">n.[n:case]</ta>
            <ta e="T600" id="Seg_11426" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_11427" s="T600">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T602" id="Seg_11428" s="T601">ptcl</ta>
            <ta e="T603" id="Seg_11429" s="T602">v-v&gt;adv</ta>
            <ta e="T604" id="Seg_11430" s="T603">n-n:(poss).[n:case]</ta>
            <ta e="T606" id="Seg_11431" s="T605">n-n:(num).[n:case]</ta>
            <ta e="T607" id="Seg_11432" s="T606">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T609" id="Seg_11433" s="T608">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T610" id="Seg_11434" s="T609">v-v:cvb</ta>
            <ta e="T611" id="Seg_11435" s="T610">n-n:(poss).[n:case]</ta>
            <ta e="T613" id="Seg_11436" s="T612">n-n:(poss).[n:case]</ta>
            <ta e="T614" id="Seg_11437" s="T613">%%</ta>
            <ta e="T615" id="Seg_11438" s="T614">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T616" id="Seg_11439" s="T615">v-v:cvb</ta>
            <ta e="T617" id="Seg_11440" s="T616">%%</ta>
            <ta e="T620" id="Seg_11441" s="T619">v</ta>
            <ta e="T621" id="Seg_11442" s="T620">n.[n:case]</ta>
            <ta e="T622" id="Seg_11443" s="T621">n-n:case</ta>
            <ta e="T623" id="Seg_11444" s="T622">v-v:tense-v:poss.pn</ta>
            <ta e="T624" id="Seg_11445" s="T623">n-n:(poss).[n:case]</ta>
            <ta e="T625" id="Seg_11446" s="T624">v-v:tense-v:poss.pn</ta>
            <ta e="T627" id="Seg_11447" s="T626">n</ta>
            <ta e="T628" id="Seg_11448" s="T627">v-v:mood.[v:pred.pn]</ta>
            <ta e="T629" id="Seg_11449" s="T628">que.[pro:case]</ta>
            <ta e="T630" id="Seg_11450" s="T629">v-v:mood-v:pred.pn</ta>
            <ta e="T634" id="Seg_11451" s="T633">n.[n:case]</ta>
            <ta e="T635" id="Seg_11452" s="T634">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T636" id="Seg_11453" s="T635">ptcl</ta>
            <ta e="T638" id="Seg_11454" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_11455" s="T638">dempro</ta>
            <ta e="T640" id="Seg_11456" s="T639">v-v:tense-v:poss.pn</ta>
            <ta e="T641" id="Seg_11457" s="T640">ptcl</ta>
            <ta e="T643" id="Seg_11458" s="T642">pers.[pro:case]</ta>
            <ta e="T644" id="Seg_11459" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_11460" s="T644">adv</ta>
            <ta e="T646" id="Seg_11461" s="T645">v-v:mood-v:pred.pn</ta>
            <ta e="T647" id="Seg_11462" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_11463" s="T647">n-n:case</ta>
            <ta e="T649" id="Seg_11464" s="T648">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T650" id="Seg_11465" s="T649">ptcl</ta>
            <ta e="T652" id="Seg_11466" s="T651">v-v:cvb</ta>
            <ta e="T653" id="Seg_11467" s="T652">v-v:cvb</ta>
            <ta e="T654" id="Seg_11468" s="T653">v-v:mood-v:pred.pn</ta>
            <ta e="T656" id="Seg_11469" s="T655">adj-adj&gt;adv</ta>
            <ta e="T657" id="Seg_11470" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_11471" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_11472" s="T658">que.[pro:case]</ta>
            <ta e="T660" id="Seg_11473" s="T659">n.[n:case]</ta>
            <ta e="T663" id="Seg_11474" s="T662">n.[n:case]</ta>
            <ta e="T664" id="Seg_11475" s="T663">n-n&gt;n.[n:case]</ta>
            <ta e="T665" id="Seg_11476" s="T664">v-v:tense-v:poss.pn</ta>
            <ta e="T666" id="Seg_11477" s="T665">interj</ta>
            <ta e="T668" id="Seg_11478" s="T667">n-n:(poss).[n:case]</ta>
            <ta e="T669" id="Seg_11479" s="T668">adj.[n:case]</ta>
            <ta e="T670" id="Seg_11480" s="T669">ptcl</ta>
            <ta e="T671" id="Seg_11481" s="T670">conj</ta>
            <ta e="T672" id="Seg_11482" s="T671">adj&gt;adj-adj.[n:case]</ta>
            <ta e="T674" id="Seg_11483" s="T673">que-pro:(num).[pro:case]</ta>
            <ta e="T675" id="Seg_11484" s="T674">v-v:mood-v:pred.pn</ta>
            <ta e="T676" id="Seg_11485" s="T675">v-v:tense.[v:pred.pn]</ta>
            <ta e="T677" id="Seg_11486" s="T676">pers.[pro:case]</ta>
            <ta e="T679" id="Seg_11487" s="T678">v-v:ptcp</ta>
            <ta e="T680" id="Seg_11488" s="T679">v-v:tense-v:poss.pn</ta>
            <ta e="T682" id="Seg_11489" s="T681">n.[n:case]</ta>
            <ta e="T683" id="Seg_11490" s="T682">post</ta>
            <ta e="T684" id="Seg_11491" s="T683">n-n:(num).[n:case]</ta>
            <ta e="T685" id="Seg_11492" s="T684">v-v:cvb</ta>
            <ta e="T686" id="Seg_11493" s="T685">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T687" id="Seg_11494" s="T686">adj-n:(pred.pn)</ta>
            <ta e="T689" id="Seg_11495" s="T688">n-n:(poss)</ta>
            <ta e="T690" id="Seg_11496" s="T689">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T691" id="Seg_11497" s="T690">v-v:mood-v:pred.pn</ta>
            <ta e="T692" id="Seg_11498" s="T691">n-n:(poss)</ta>
            <ta e="T693" id="Seg_11499" s="T692">ptcl-n:(pred.pn)</ta>
            <ta e="T694" id="Seg_11500" s="T693">v-v:tense-v:pred.pn</ta>
            <ta e="T695" id="Seg_11501" s="T694">n.[n:case]</ta>
            <ta e="T696" id="Seg_11502" s="T695">v.[v:mood.pn]</ta>
            <ta e="T699" id="Seg_11503" s="T698">dempro.[pro:case]</ta>
            <ta e="T700" id="Seg_11504" s="T699">que.[pro:case]</ta>
            <ta e="T701" id="Seg_11505" s="T700">interj</ta>
            <ta e="T702" id="Seg_11506" s="T701">dempro</ta>
            <ta e="T703" id="Seg_11507" s="T702">n.[n:case]</ta>
            <ta e="T704" id="Seg_11508" s="T703">v-v:tense.[v:pred.pn]</ta>
            <ta e="T705" id="Seg_11509" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_11510" s="T705">n-n:case</ta>
            <ta e="T707" id="Seg_11511" s="T706">ptcl</ta>
            <ta e="T709" id="Seg_11512" s="T708">dempro-pro:case</ta>
            <ta e="T710" id="Seg_11513" s="T709">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T711" id="Seg_11514" s="T710">v-v:tense.[v:pred.pn]</ta>
            <ta e="T712" id="Seg_11515" s="T711">dempro-pro:(num)-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T714" id="Seg_11516" s="T713">ptcl</ta>
            <ta e="T716" id="Seg_11517" s="T715">dempro-pro:case</ta>
            <ta e="T717" id="Seg_11518" s="T716">que</ta>
            <ta e="T718" id="Seg_11519" s="T717">ptcl</ta>
            <ta e="T719" id="Seg_11520" s="T718">n-n:(poss).[n:case]</ta>
            <ta e="T720" id="Seg_11521" s="T719">v</ta>
            <ta e="T721" id="Seg_11522" s="T720">v-v:tense.[v:pred.pn]</ta>
            <ta e="T722" id="Seg_11523" s="T721">v</ta>
            <ta e="T723" id="Seg_11524" s="T722">v-v:tense.[v:pred.pn]</ta>
            <ta e="T725" id="Seg_11525" s="T724">adv</ta>
            <ta e="T726" id="Seg_11526" s="T725">adv</ta>
            <ta e="T727" id="Seg_11527" s="T726">ptcl</ta>
            <ta e="T728" id="Seg_11528" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_11529" s="T728">v-v:cvb</ta>
            <ta e="T730" id="Seg_11530" s="T729">v-v:tense.[v:pred.pn]</ta>
            <ta e="T731" id="Seg_11531" s="T730">adv</ta>
            <ta e="T733" id="Seg_11532" s="T732">propr-n:(poss).[n:case]</ta>
            <ta e="T734" id="Seg_11533" s="T733">v-v:tense.[v:pred.pn]</ta>
            <ta e="T736" id="Seg_11534" s="T735">interj</ta>
            <ta e="T737" id="Seg_11535" s="T736">ptcl</ta>
            <ta e="T738" id="Seg_11536" s="T737">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T740" id="Seg_11537" s="T739">v-v:tense.[v:pred.pn]</ta>
            <ta e="T741" id="Seg_11538" s="T740">adv</ta>
            <ta e="T742" id="Seg_11539" s="T741">n-n:poss-n:case</ta>
            <ta e="T743" id="Seg_11540" s="T742">post</ta>
            <ta e="T745" id="Seg_11541" s="T744">dempro-pro:case</ta>
            <ta e="T746" id="Seg_11542" s="T745">que.[pro:case]</ta>
            <ta e="T749" id="Seg_11543" s="T748">v-v:tense-v:poss.pn</ta>
            <ta e="T750" id="Seg_11544" s="T749">ptcl</ta>
            <ta e="T753" id="Seg_11545" s="T752">dempro</ta>
            <ta e="T754" id="Seg_11546" s="T753">que.[pro:case]</ta>
            <ta e="T755" id="Seg_11547" s="T754">adj</ta>
            <ta e="T756" id="Seg_11548" s="T755">adj</ta>
            <ta e="T757" id="Seg_11549" s="T756">pers.[pro:case]</ta>
            <ta e="T759" id="Seg_11550" s="T758">n-n:(poss).[n:case]</ta>
            <ta e="T760" id="Seg_11551" s="T759">ptcl-ptcl&gt;ptcl-n:poss-n:case</ta>
            <ta e="T761" id="Seg_11552" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_11553" s="T761">v-v&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T764" id="Seg_11554" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_11555" s="T764">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T766" id="Seg_11556" s="T765">ptcl</ta>
            <ta e="T768" id="Seg_11557" s="T767">ptcl</ta>
            <ta e="T769" id="Seg_11558" s="T768">adv</ta>
            <ta e="T770" id="Seg_11559" s="T769">v.[v:mood.pn]</ta>
            <ta e="T771" id="Seg_11560" s="T770">emphpro</ta>
            <ta e="T772" id="Seg_11561" s="T771">v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T774" id="Seg_11562" s="T773">n-n:(num)-n:case</ta>
            <ta e="T775" id="Seg_11563" s="T774">v-v:cvb</ta>
            <ta e="T776" id="Seg_11564" s="T775">v-v:tense-v:poss.pn</ta>
            <ta e="T777" id="Seg_11565" s="T776">ptcl</ta>
            <ta e="T780" id="Seg_11566" s="T779">que-pro:(num)-pro:case</ta>
            <ta e="T781" id="Seg_11567" s="T780">ptcl</ta>
            <ta e="T782" id="Seg_11568" s="T781">v-v:tense-v:pred.pn</ta>
            <ta e="T784" id="Seg_11569" s="T783">n-n:(num)-n:(num).[n:case]</ta>
            <ta e="T785" id="Seg_11570" s="T784">que-pro:(num).[pro:case]</ta>
            <ta e="T786" id="Seg_11571" s="T785">ptcl</ta>
            <ta e="T788" id="Seg_11572" s="T787">dempro-pro:(num)-pro:case</ta>
            <ta e="T789" id="Seg_11573" s="T788">v-v:cvb</ta>
            <ta e="T790" id="Seg_11574" s="T789">ptcl</ta>
            <ta e="T791" id="Seg_11575" s="T790">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T792" id="Seg_11576" s="T791">v-v:cvb</ta>
            <ta e="T793" id="Seg_11577" s="T792">adv</ta>
            <ta e="T794" id="Seg_11578" s="T793">adv</ta>
            <ta e="T795" id="Seg_11579" s="T794">ptcl</ta>
            <ta e="T796" id="Seg_11580" s="T795">v-v:tense-v:pred.pn</ta>
            <ta e="T797" id="Seg_11581" s="T796">dempro-pro:case</ta>
            <ta e="T798" id="Seg_11582" s="T797">ptcl</ta>
            <ta e="T799" id="Seg_11583" s="T798">n-n:(num).[n:case]</ta>
            <ta e="T800" id="Seg_11584" s="T799">v-v:tense-v:poss.pn</ta>
            <ta e="T801" id="Seg_11585" s="T800">dempro-pro:case</ta>
            <ta e="T802" id="Seg_11586" s="T801">v-v:cvb</ta>
            <ta e="T803" id="Seg_11587" s="T802">v-v:tense.[v:pred.pn]</ta>
            <ta e="T805" id="Seg_11588" s="T804">ptcl</ta>
            <ta e="T806" id="Seg_11589" s="T805">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T807" id="Seg_11590" s="T806">v-v:ptcp-v:(case)</ta>
            <ta e="T808" id="Seg_11591" s="T807">v-v:(ins)-v&gt;v.[v:mood.pn]</ta>
            <ta e="T809" id="Seg_11592" s="T808">n-n:(poss).[n:case]</ta>
            <ta e="T810" id="Seg_11593" s="T809">v-v:tense.[v:poss.pn]</ta>
            <ta e="T811" id="Seg_11594" s="T810">ptcl</ta>
            <ta e="T813" id="Seg_11595" s="T812">n-n:(poss).[n:case]</ta>
            <ta e="T814" id="Seg_11596" s="T813">n-n:(poss).[n:case]</ta>
            <ta e="T815" id="Seg_11597" s="T814">v-v:tense-v:poss.pn</ta>
            <ta e="T816" id="Seg_11598" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_11599" s="T816">adv</ta>
            <ta e="T818" id="Seg_11600" s="T817">v-v:tense-v:poss.pn</ta>
            <ta e="T819" id="Seg_11601" s="T818">ptcl</ta>
            <ta e="T820" id="Seg_11602" s="T819">adj</ta>
            <ta e="T822" id="Seg_11603" s="T821">ptcl</ta>
            <ta e="T823" id="Seg_11604" s="T822">adj.[n:case]</ta>
            <ta e="T824" id="Seg_11605" s="T823">adv</ta>
            <ta e="T825" id="Seg_11606" s="T824">post</ta>
            <ta e="T827" id="Seg_11607" s="T826">interj</ta>
            <ta e="T828" id="Seg_11608" s="T827">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T830" id="Seg_11609" s="T829">n.[n:case]</ta>
            <ta e="T831" id="Seg_11610" s="T830">post</ta>
            <ta e="T832" id="Seg_11611" s="T831">v-v:cvb</ta>
            <ta e="T833" id="Seg_11612" s="T832">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T834" id="Seg_11613" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_11614" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_11615" s="T835">adj-adj&gt;adv</ta>
            <ta e="T837" id="Seg_11616" s="T836">ptcl</ta>
            <ta e="T839" id="Seg_11617" s="T838">que.[pro:case]</ta>
            <ta e="T840" id="Seg_11618" s="T839">n-n:(poss).[n:case]</ta>
            <ta e="T841" id="Seg_11619" s="T840">ptcl</ta>
            <ta e="T842" id="Seg_11620" s="T841">v-v:tense.[v:pred.pn]</ta>
            <ta e="T844" id="Seg_11621" s="T843">v</ta>
            <ta e="T845" id="Seg_11622" s="T844">ptcl</ta>
            <ta e="T846" id="Seg_11623" s="T845">adj</ta>
            <ta e="T847" id="Seg_11624" s="T846">que.[pro:case]</ta>
            <ta e="T848" id="Seg_11625" s="T847">ptcl</ta>
            <ta e="T849" id="Seg_11626" s="T848">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T850" id="Seg_11627" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_11628" s="T850">que-pro:case</ta>
            <ta e="T852" id="Seg_11629" s="T851">n-n:case</ta>
            <ta e="T853" id="Seg_11630" s="T852">ptcl</ta>
            <ta e="T854" id="Seg_11631" s="T853">que-que&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T855" id="Seg_11632" s="T854">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T857" id="Seg_11633" s="T856">dempro-pro:case</ta>
            <ta e="T858" id="Seg_11634" s="T857">dempro-pro:case</ta>
            <ta e="T859" id="Seg_11635" s="T858">interj</ta>
            <ta e="T860" id="Seg_11636" s="T859">n-n:(poss).[n:case]</ta>
            <ta e="T863" id="Seg_11637" s="T862">v-v:tense.[v:pred.pn]</ta>
            <ta e="T864" id="Seg_11638" s="T863">dempro</ta>
            <ta e="T865" id="Seg_11639" s="T864">que-que&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T866" id="Seg_11640" s="T865">v-v:tense.[v:pred.pn]</ta>
            <ta e="T868" id="Seg_11641" s="T867">interj</ta>
            <ta e="T869" id="Seg_11642" s="T868">n.[n:case]</ta>
            <ta e="T870" id="Seg_11643" s="T869">v-v:tense-v:poss.pn</ta>
            <ta e="T871" id="Seg_11644" s="T870">ptcl</ta>
            <ta e="T872" id="Seg_11645" s="T871">n</ta>
            <ta e="T877" id="Seg_11646" s="T876">n-n:(poss).[n:case]</ta>
            <ta e="T878" id="Seg_11647" s="T877">v-v:tense.[v:pred.pn]</ta>
            <ta e="T879" id="Seg_11648" s="T878">n-n:(poss).[n:case]</ta>
            <ta e="T880" id="Seg_11649" s="T879">ptcl</ta>
            <ta e="T882" id="Seg_11650" s="T881">ptcl</ta>
            <ta e="T883" id="Seg_11651" s="T882">dempro</ta>
            <ta e="T884" id="Seg_11652" s="T883">v-v:tense-v:poss.pn</ta>
            <ta e="T886" id="Seg_11653" s="T885">pers.[pro:case]</ta>
            <ta e="T887" id="Seg_11654" s="T886">ptcl</ta>
            <ta e="T888" id="Seg_11655" s="T887">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T889" id="Seg_11656" s="T888">ptcl</ta>
            <ta e="T890" id="Seg_11657" s="T889">n-n&gt;v-v&gt;v-v:cvb</ta>
            <ta e="T891" id="Seg_11658" s="T890">v-v:tense.[v:pred.pn]</ta>
            <ta e="T892" id="Seg_11659" s="T891">v-v:tense-v:pred.pn</ta>
            <ta e="T893" id="Seg_11660" s="T892">ptcl</ta>
            <ta e="T895" id="Seg_11661" s="T894">pers-pro:(poss).[pro:case]</ta>
            <ta e="T896" id="Seg_11662" s="T895">ptcl</ta>
            <ta e="T897" id="Seg_11663" s="T896">propr.[n:case]</ta>
            <ta e="T898" id="Seg_11664" s="T897">post</ta>
            <ta e="T899" id="Seg_11665" s="T898">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T900" id="Seg_11666" s="T899">conj</ta>
            <ta e="T901" id="Seg_11667" s="T900">propr.[n:case]</ta>
            <ta e="T902" id="Seg_11668" s="T901">post</ta>
            <ta e="T904" id="Seg_11669" s="T903">ptcl</ta>
            <ta e="T905" id="Seg_11670" s="T904">dempro</ta>
            <ta e="T906" id="Seg_11671" s="T905">v-v:tense-v:poss.pn</ta>
            <ta e="T909" id="Seg_11672" s="T908">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T910" id="Seg_11673" s="T909">post</ta>
            <ta e="T911" id="Seg_11674" s="T910">ptcl</ta>
            <ta e="T913" id="Seg_11675" s="T912">interj</ta>
            <ta e="T914" id="Seg_11676" s="T913">n-n:(poss).[n:case]</ta>
            <ta e="T915" id="Seg_11677" s="T914">v-v:tense-v:poss.pn</ta>
            <ta e="T917" id="Seg_11678" s="T916">interj</ta>
            <ta e="T918" id="Seg_11679" s="T917">n-n:(poss).[n:case]</ta>
            <ta e="T919" id="Seg_11680" s="T918">v-v:cvb</ta>
            <ta e="T920" id="Seg_11681" s="T919">v-v:tense-v:poss.pn</ta>
            <ta e="T921" id="Seg_11682" s="T920">v-v:tense-v:poss.pn</ta>
            <ta e="T922" id="Seg_11683" s="T921">v-v:tense-v:poss.pn</ta>
            <ta e="T923" id="Seg_11684" s="T922">n-n:case</ta>
            <ta e="T924" id="Seg_11685" s="T923">n-n:poss-n:case</ta>
            <ta e="T925" id="Seg_11686" s="T924">v-v:tense-v:pred.pn</ta>
            <ta e="T926" id="Seg_11687" s="T925">v-v:tense-v:pred.pn</ta>
            <ta e="T928" id="Seg_11688" s="T927">interj</ta>
            <ta e="T929" id="Seg_11689" s="T928">n-n:(poss).[n:case]</ta>
            <ta e="T930" id="Seg_11690" s="T929">n-n:(num)-n&gt;adj</ta>
            <ta e="T931" id="Seg_11691" s="T930">v-v:tense-v:poss.pn</ta>
            <ta e="T933" id="Seg_11692" s="T932">n-n:(num)-n:poss-n:case</ta>
            <ta e="T934" id="Seg_11693" s="T933">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T936" id="Seg_11694" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_11695" s="T936">v-v&gt;v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T938" id="Seg_11696" s="T937">interj</ta>
            <ta e="T939" id="Seg_11697" s="T938">ptcl</ta>
            <ta e="T940" id="Seg_11698" s="T939">n.[n:case]</ta>
            <ta e="T941" id="Seg_11699" s="T940">interj-n:poss-n:case</ta>
            <ta e="T942" id="Seg_11700" s="T941">v-v:tense.[v:pred.pn]</ta>
            <ta e="T944" id="Seg_11701" s="T943">dempro-pro:case</ta>
            <ta e="T945" id="Seg_11702" s="T944">adj</ta>
            <ta e="T946" id="Seg_11703" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_11704" s="T946">adv</ta>
            <ta e="T948" id="Seg_11705" s="T947">v-v:tense-v:pred.pn</ta>
            <ta e="T950" id="Seg_11706" s="T949">que.[pro:case]</ta>
            <ta e="T951" id="Seg_11707" s="T950">n-n:(num).[n:case]</ta>
            <ta e="T952" id="Seg_11708" s="T951">ptcl</ta>
            <ta e="T953" id="Seg_11709" s="T952">adj</ta>
            <ta e="T954" id="Seg_11710" s="T953">ptcl</ta>
            <ta e="T955" id="Seg_11711" s="T954">que.[pro:case]</ta>
            <ta e="T956" id="Seg_11712" s="T955">n-n:case</ta>
            <ta e="T957" id="Seg_11713" s="T956">v-v:(ins)-v:ptcp</ta>
            <ta e="T959" id="Seg_11714" s="T958">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T961" id="Seg_11715" s="T960">n-n:(num)-n&gt;adj.[n:case]</ta>
            <ta e="T962" id="Seg_11716" s="T961">que.[pro:case]</ta>
            <ta e="T963" id="Seg_11717" s="T962">ptcl</ta>
            <ta e="T964" id="Seg_11718" s="T963">ptcl</ta>
            <ta e="T965" id="Seg_11719" s="T964">adj</ta>
            <ta e="T966" id="Seg_11720" s="T965">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T967" id="Seg_11721" s="T966">ptcl</ta>
            <ta e="T968" id="Seg_11722" s="T967">adj</ta>
            <ta e="T970" id="Seg_11723" s="T969">ptcl</ta>
            <ta e="T971" id="Seg_11724" s="T970">v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T972" id="Seg_11725" s="T971">interj</ta>
            <ta e="T973" id="Seg_11726" s="T972">v-v:tense.[v:poss.pn]</ta>
            <ta e="T974" id="Seg_11727" s="T973">v-v:tense-v:pred.pn</ta>
            <ta e="T976" id="Seg_11728" s="T975">v-v&gt;v-v:ptcp-v:(poss)</ta>
            <ta e="T977" id="Seg_11729" s="T976">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T978" id="Seg_11730" s="T977">ptcl</ta>
            <ta e="T980" id="Seg_11731" s="T979">v-v:tense-v:poss.pn</ta>
            <ta e="T981" id="Seg_11732" s="T980">interj</ta>
            <ta e="T982" id="Seg_11733" s="T981">ptcl</ta>
            <ta e="T984" id="Seg_11734" s="T983">ptcl</ta>
            <ta e="T985" id="Seg_11735" s="T984">v-v:tense-v:poss.pn</ta>
            <ta e="T986" id="Seg_11736" s="T985">adv</ta>
            <ta e="T987" id="Seg_11737" s="T986">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T988" id="Seg_11738" s="T987">v-v:tense-v:pred.pn</ta>
            <ta e="T989" id="Seg_11739" s="T988">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T990" id="Seg_11740" s="T989">v-v:tense-v:pred.pn</ta>
            <ta e="T991" id="Seg_11741" s="T990">ptcl</ta>
            <ta e="T993" id="Seg_11742" s="T992">dempro-pro:case</ta>
            <ta e="T994" id="Seg_11743" s="T993">que.[pro:case]</ta>
            <ta e="T995" id="Seg_11744" s="T994">que-pro:case</ta>
            <ta e="T996" id="Seg_11745" s="T995">v-v:tense-v:poss.pn</ta>
            <ta e="T997" id="Seg_11746" s="T996">pers-pro:case</ta>
            <ta e="T999" id="Seg_11747" s="T998">n-n&gt;n.[n:case]</ta>
            <ta e="T1000" id="Seg_11748" s="T999">adj.[n:case]</ta>
            <ta e="T1002" id="Seg_11749" s="T1001">dempro-pro:case</ta>
            <ta e="T1003" id="Seg_11750" s="T1002">adv</ta>
            <ta e="T1004" id="Seg_11751" s="T1003">v-v:cvb</ta>
            <ta e="T1005" id="Seg_11752" s="T1004">v-v:mood-v:pred.pn</ta>
            <ta e="T1006" id="Seg_11753" s="T1005">interj</ta>
            <ta e="T1007" id="Seg_11754" s="T1006">adj-adj&gt;adv</ta>
            <ta e="T1008" id="Seg_11755" s="T1007">ptcl</ta>
            <ta e="T1009" id="Seg_11756" s="T1008">adv</ta>
            <ta e="T1012" id="Seg_11757" s="T1011">v-v:tense-v:poss.pn</ta>
            <ta e="T1013" id="Seg_11758" s="T1012">dempro.[pro:case]</ta>
            <ta e="T1014" id="Seg_11759" s="T1013">post</ta>
            <ta e="T1015" id="Seg_11760" s="T1014">n-n:case</ta>
            <ta e="T1016" id="Seg_11761" s="T1015">ptcl</ta>
            <ta e="T1018" id="Seg_11762" s="T1017">interj</ta>
            <ta e="T1019" id="Seg_11763" s="T1018">pers.[pro:case]</ta>
            <ta e="T1020" id="Seg_11764" s="T1019">ptcl</ta>
            <ta e="T1021" id="Seg_11765" s="T1020">ptcl</ta>
            <ta e="T1023" id="Seg_11766" s="T1022">adj-adj&gt;adv</ta>
            <ta e="T1024" id="Seg_11767" s="T1023">ptcl</ta>
            <ta e="T1026" id="Seg_11768" s="T1025">interj</ta>
            <ta e="T1027" id="Seg_11769" s="T1026">pers-pro:case</ta>
            <ta e="T1029" id="Seg_11770" s="T1028">v-v:tense.[v:pred.pn]</ta>
            <ta e="T1031" id="Seg_11771" s="T1030">ptcl</ta>
            <ta e="T1032" id="Seg_11772" s="T1031">dempro</ta>
            <ta e="T1033" id="Seg_11773" s="T1032">v-v:tense-v:pred.pn</ta>
            <ta e="T1034" id="Seg_11774" s="T1033">ptcl</ta>
            <ta e="T1035" id="Seg_11775" s="T1034">ptcl</ta>
            <ta e="T1036" id="Seg_11776" s="T1035">pers.[pro:case]</ta>
            <ta e="T1037" id="Seg_11777" s="T1036">v-v:cvb</ta>
            <ta e="T1038" id="Seg_11778" s="T1037">v-v:tense-v:pred.pn</ta>
            <ta e="T1040" id="Seg_11779" s="T1039">adv</ta>
            <ta e="T1041" id="Seg_11780" s="T1040">ptcl</ta>
            <ta e="T1042" id="Seg_11781" s="T1041">v-v:tense-v:pred.pn</ta>
            <ta e="T1043" id="Seg_11782" s="T1042">interj</ta>
            <ta e="T1044" id="Seg_11783" s="T1043">n.[n:case]</ta>
            <ta e="T1045" id="Seg_11784" s="T1044">ptcl</ta>
            <ta e="T1046" id="Seg_11785" s="T1045">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T1047" id="Seg_11786" s="T1046">v-v:tense.[v:pred.pn]</ta>
            <ta e="T1049" id="Seg_11787" s="T1048">adj-adj&gt;adv</ta>
            <ta e="T1050" id="Seg_11788" s="T1049">ptcl</ta>
            <ta e="T1051" id="Seg_11789" s="T1050">que.[pro:case]</ta>
            <ta e="T1052" id="Seg_11790" s="T1051">ptcl</ta>
            <ta e="T1053" id="Seg_11791" s="T1052">ptcl</ta>
            <ta e="T1054" id="Seg_11792" s="T1053">n-n:(poss).[n:case]</ta>
            <ta e="T1055" id="Seg_11793" s="T1054">n-n:poss-n:case</ta>
            <ta e="T1057" id="Seg_11794" s="T1056">dempro</ta>
            <ta e="T1058" id="Seg_11795" s="T1057">n-n:poss-n:case</ta>
            <ta e="T1059" id="Seg_11796" s="T1058">v-v:tense-v:(num)</ta>
            <ta e="T1060" id="Seg_11797" s="T1059">v-v:tense.[v:pred.pn]</ta>
            <ta e="T1062" id="Seg_11798" s="T1061">v-v:cvb</ta>
            <ta e="T1063" id="Seg_11799" s="T1062">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T1065" id="Seg_11800" s="T1064">n-n:(num)-n&gt;adj.[n:case]</ta>
            <ta e="T1066" id="Seg_11801" s="T1065">v-v:tense-v:poss.pn</ta>
            <ta e="T1067" id="Seg_11802" s="T1066">ptcl</ta>
            <ta e="T1068" id="Seg_11803" s="T1067">ptcl</ta>
            <ta e="T1069" id="Seg_11804" s="T1068">ptcl</ta>
            <ta e="T1070" id="Seg_11805" s="T1069">v-v:tense-v:poss.pn</ta>
            <ta e="T1071" id="Seg_11806" s="T1070">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T1073" id="Seg_11807" s="T1072">ptcl</ta>
            <ta e="T1074" id="Seg_11808" s="T1073">dempro</ta>
            <ta e="T1075" id="Seg_11809" s="T1074">v-v:tense-v:pred.pn</ta>
            <ta e="T1076" id="Seg_11810" s="T1075">n-n:poss-n:case</ta>
            <ta e="T1077" id="Seg_11811" s="T1076">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T1078" id="Seg_11812" s="T1077">n.[n:case]</ta>
            <ta e="T1079" id="Seg_11813" s="T1078">que.[pro:case]</ta>
            <ta e="T1080" id="Seg_11814" s="T1079">v-v:tense-v:pred.pn</ta>
            <ta e="T1081" id="Seg_11815" s="T1080">adv</ta>
            <ta e="T1082" id="Seg_11816" s="T1081">v-v:tense-v:pred.pn</ta>
            <ta e="T1083" id="Seg_11817" s="T1082">que</ta>
            <ta e="T1084" id="Seg_11818" s="T1083">ptcl</ta>
            <ta e="T1086" id="Seg_11819" s="T1085">interj</ta>
            <ta e="T1087" id="Seg_11820" s="T1086">ptcl</ta>
            <ta e="T1088" id="Seg_11821" s="T1087">pers-pro:case</ta>
            <ta e="T1089" id="Seg_11822" s="T1088">v-v&gt;v-v:cvb</ta>
            <ta e="T1090" id="Seg_11823" s="T1089">v-v:tense-v:pred.pn</ta>
            <ta e="T1092" id="Seg_11824" s="T1091">n-n:case</ta>
            <ta e="T1093" id="Seg_11825" s="T1092">v-v:cvb</ta>
            <ta e="T1094" id="Seg_11826" s="T1093">v-v:tense-v:pred.pn</ta>
            <ta e="T1095" id="Seg_11827" s="T1094">v-v:cvb</ta>
            <ta e="T1096" id="Seg_11828" s="T1095">v-v:tense-v:pred.pn</ta>
            <ta e="T1098" id="Seg_11829" s="T1097">ptcl</ta>
            <ta e="T1099" id="Seg_11830" s="T1098">que</ta>
            <ta e="T1100" id="Seg_11831" s="T1099">v-v:tense-v:pred.pn</ta>
            <ta e="T1102" id="Seg_11832" s="T1101">conj</ta>
            <ta e="T1103" id="Seg_11833" s="T1102">n-pro:(poss)-n:case</ta>
            <ta e="T1104" id="Seg_11834" s="T1103">ptcl</ta>
            <ta e="T1105" id="Seg_11835" s="T1104">n-n:(num)-n:case</ta>
            <ta e="T1106" id="Seg_11836" s="T1105">v-v:mood-v:pred.pn</ta>
            <ta e="T1107" id="Seg_11837" s="T1106">n-n:case</ta>
            <ta e="T1109" id="Seg_11838" s="T1108">n-n&gt;adj</ta>
            <ta e="T1110" id="Seg_11839" s="T1109">conj</ta>
            <ta e="T1111" id="Seg_11840" s="T1110">n-n&gt;adj</ta>
            <ta e="T1112" id="Seg_11841" s="T1111">n.[n:case]</ta>
            <ta e="T1113" id="Seg_11842" s="T1112">v-v:ptcp</ta>
            <ta e="T1114" id="Seg_11843" s="T1113">v-v:tense-v:poss.pn</ta>
            <ta e="T1116" id="Seg_11844" s="T1115">dempro-pro:(num)-pro:case</ta>
            <ta e="T1117" id="Seg_11845" s="T1116">v-v:cvb</ta>
            <ta e="T1118" id="Seg_11846" s="T1117">v-v:cvb</ta>
            <ta e="T1119" id="Seg_11847" s="T1118">v-v:tense-v:pred.pn</ta>
            <ta e="T1120" id="Seg_11848" s="T1119">v-v:cvb</ta>
            <ta e="T1122" id="Seg_11849" s="T1121">v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T1123" id="Seg_11850" s="T1122">v-v:tense.[v:poss.pn]</ta>
            <ta e="T1124" id="Seg_11851" s="T1123">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T1126" id="Seg_11852" s="T1125">dempro</ta>
            <ta e="T1127" id="Seg_11853" s="T1126">ptcl</ta>
            <ta e="T1128" id="Seg_11854" s="T1127">v-v:tense-v:pred.pn</ta>
            <ta e="T1129" id="Seg_11855" s="T1128">ptcl</ta>
            <ta e="T1130" id="Seg_11856" s="T1129">v-v:tense-v:pred.pn</ta>
            <ta e="T1133" id="Seg_11857" s="T1132">n-n:poss-n:case</ta>
            <ta e="T1134" id="Seg_11858" s="T1133">adv</ta>
            <ta e="T1136" id="Seg_11859" s="T1135">ptcl</ta>
            <ta e="T1137" id="Seg_11860" s="T1136">dempro</ta>
            <ta e="T1138" id="Seg_11861" s="T1137">dempro.[pro:case]</ta>
            <ta e="T1139" id="Seg_11862" s="T1138">post</ta>
            <ta e="T1140" id="Seg_11863" s="T1139">v-v:cvb</ta>
            <ta e="T1141" id="Seg_11864" s="T1140">v-v:ptcp</ta>
            <ta e="T1142" id="Seg_11865" s="T1141">v-v:tense-v:poss.pn</ta>
            <ta e="T1143" id="Seg_11866" s="T1142">v-v:cvb</ta>
            <ta e="T1144" id="Seg_11867" s="T1143">v-v:ptcp</ta>
            <ta e="T1145" id="Seg_11868" s="T1144">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T1146" id="Seg_11869" s="T1145">v-v:ptcp</ta>
            <ta e="T1147" id="Seg_11870" s="T1146">v-v:tense-v:poss.pn</ta>
            <ta e="T1148" id="Seg_11871" s="T1147">pers-pro:case</ta>
            <ta e="T1149" id="Seg_11872" s="T1148">post</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_11873" s="T1">adv</ta>
            <ta e="T3" id="Seg_11874" s="T2">v</ta>
            <ta e="T4" id="Seg_11875" s="T3">adj</ta>
            <ta e="T5" id="Seg_11876" s="T4">cop</ta>
            <ta e="T6" id="Seg_11877" s="T5">propr</ta>
            <ta e="T7" id="Seg_11878" s="T6">n</ta>
            <ta e="T8" id="Seg_11879" s="T7">propr</ta>
            <ta e="T9" id="Seg_11880" s="T8">cardnum</ta>
            <ta e="T10" id="Seg_11881" s="T9">cardnum</ta>
            <ta e="T11" id="Seg_11882" s="T10">n</ta>
            <ta e="T12" id="Seg_11883" s="T11">adv</ta>
            <ta e="T13" id="Seg_11884" s="T12">adj</ta>
            <ta e="T14" id="Seg_11885" s="T13">n</ta>
            <ta e="T15" id="Seg_11886" s="T14">v</ta>
            <ta e="T17" id="Seg_11887" s="T16">n</ta>
            <ta e="T18" id="Seg_11888" s="T17">adj</ta>
            <ta e="T19" id="Seg_11889" s="T18">adv</ta>
            <ta e="T20" id="Seg_11890" s="T19">v</ta>
            <ta e="T21" id="Seg_11891" s="T20">aux</ta>
            <ta e="T23" id="Seg_11892" s="T22">n</ta>
            <ta e="T24" id="Seg_11893" s="T23">n</ta>
            <ta e="T25" id="Seg_11894" s="T24">adj</ta>
            <ta e="T26" id="Seg_11895" s="T25">cop</ta>
            <ta e="T27" id="Seg_11896" s="T26">adv</ta>
            <ta e="T29" id="Seg_11897" s="T28">n</ta>
            <ta e="T30" id="Seg_11898" s="T29">que</ta>
            <ta e="T31" id="Seg_11899" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_11900" s="T31">cop</ta>
            <ta e="T34" id="Seg_11901" s="T33">dempro</ta>
            <ta e="T35" id="Seg_11902" s="T34">adv</ta>
            <ta e="T36" id="Seg_11903" s="T35">n</ta>
            <ta e="T37" id="Seg_11904" s="T36">n</ta>
            <ta e="T38" id="Seg_11905" s="T37">n</ta>
            <ta e="T39" id="Seg_11906" s="T38">adj</ta>
            <ta e="T40" id="Seg_11907" s="T39">n</ta>
            <ta e="T41" id="Seg_11908" s="T40">v</ta>
            <ta e="T42" id="Seg_11909" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_11910" s="T42">n</ta>
            <ta e="T44" id="Seg_11911" s="T43">v</ta>
            <ta e="T45" id="Seg_11912" s="T44">adj</ta>
            <ta e="T46" id="Seg_11913" s="T45">n</ta>
            <ta e="T47" id="Seg_11914" s="T46">n</ta>
            <ta e="T48" id="Seg_11915" s="T47">v</ta>
            <ta e="T49" id="Seg_11916" s="T48">adj</ta>
            <ta e="T50" id="Seg_11917" s="T49">n</ta>
            <ta e="T51" id="Seg_11918" s="T50">pers</ta>
            <ta e="T52" id="Seg_11919" s="T51">v</ta>
            <ta e="T54" id="Seg_11920" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_11921" s="T54">dempro</ta>
            <ta e="T56" id="Seg_11922" s="T55">n</ta>
            <ta e="T57" id="Seg_11923" s="T56">que</ta>
            <ta e="T58" id="Seg_11924" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_11925" s="T58">v</ta>
            <ta e="T60" id="Seg_11926" s="T59">aux</ta>
            <ta e="T62" id="Seg_11927" s="T61">v</ta>
            <ta e="T63" id="Seg_11928" s="T62">v</ta>
            <ta e="T64" id="Seg_11929" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_11930" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_11931" s="T65">v</ta>
            <ta e="T67" id="Seg_11932" s="T66">n</ta>
            <ta e="T69" id="Seg_11933" s="T68">que</ta>
            <ta e="T70" id="Seg_11934" s="T69">n</ta>
            <ta e="T71" id="Seg_11935" s="T70">n</ta>
            <ta e="T72" id="Seg_11936" s="T71">v</ta>
            <ta e="T74" id="Seg_11937" s="T73">v</ta>
            <ta e="T75" id="Seg_11938" s="T74">v</ta>
            <ta e="T78" id="Seg_11939" s="T77">adj</ta>
            <ta e="T79" id="Seg_11940" s="T78">cop</ta>
            <ta e="T81" id="Seg_11941" s="T80">dempro</ta>
            <ta e="T82" id="Seg_11942" s="T81">n</ta>
            <ta e="T83" id="Seg_11943" s="T82">v</ta>
            <ta e="T85" id="Seg_11944" s="T84">n</ta>
            <ta e="T86" id="Seg_11945" s="T85">ptcl</ta>
            <ta e="T88" id="Seg_11946" s="T87">n</ta>
            <ta e="T89" id="Seg_11947" s="T88">v</ta>
            <ta e="T90" id="Seg_11948" s="T89">n</ta>
            <ta e="T91" id="Seg_11949" s="T90">v</ta>
            <ta e="T93" id="Seg_11950" s="T92">interj</ta>
            <ta e="T94" id="Seg_11951" s="T93">n</ta>
            <ta e="T95" id="Seg_11952" s="T94">v</ta>
            <ta e="T96" id="Seg_11953" s="T95">v</ta>
            <ta e="T97" id="Seg_11954" s="T96">v</ta>
            <ta e="T98" id="Seg_11955" s="T97">interj</ta>
            <ta e="T99" id="Seg_11956" s="T98">v</ta>
            <ta e="T100" id="Seg_11957" s="T99">adj</ta>
            <ta e="T101" id="Seg_11958" s="T100">n</ta>
            <ta e="T102" id="Seg_11959" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_11960" s="T102">v</ta>
            <ta e="T104" id="Seg_11961" s="T103">aux</ta>
            <ta e="T105" id="Seg_11962" s="T104">ptcl</ta>
            <ta e="T108" id="Seg_11963" s="T107">dempro</ta>
            <ta e="T109" id="Seg_11964" s="T108">n</ta>
            <ta e="T110" id="Seg_11965" s="T109">v</ta>
            <ta e="T111" id="Seg_11966" s="T110">v</ta>
            <ta e="T112" id="Seg_11967" s="T111">ptcl</ta>
            <ta e="T114" id="Seg_11968" s="T113">n</ta>
            <ta e="T115" id="Seg_11969" s="T114">dempro</ta>
            <ta e="T116" id="Seg_11970" s="T115">n</ta>
            <ta e="T117" id="Seg_11971" s="T116">n</ta>
            <ta e="T118" id="Seg_11972" s="T117">ptcl</ta>
            <ta e="T120" id="Seg_11973" s="T119">interj</ta>
            <ta e="T121" id="Seg_11974" s="T120">dempro</ta>
            <ta e="T122" id="Seg_11975" s="T121">v</ta>
            <ta e="T123" id="Seg_11976" s="T122">v</ta>
            <ta e="T124" id="Seg_11977" s="T123">v</ta>
            <ta e="T125" id="Seg_11978" s="T124">v</ta>
            <ta e="T126" id="Seg_11979" s="T125">interj</ta>
            <ta e="T128" id="Seg_11980" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_11981" s="T128">dempro</ta>
            <ta e="T130" id="Seg_11982" s="T129">n</ta>
            <ta e="T131" id="Seg_11983" s="T130">v</ta>
            <ta e="T132" id="Seg_11984" s="T131">adj</ta>
            <ta e="T133" id="Seg_11985" s="T132">n</ta>
            <ta e="T134" id="Seg_11986" s="T133">ptcl</ta>
            <ta e="T136" id="Seg_11987" s="T135">interj</ta>
            <ta e="T137" id="Seg_11988" s="T136">v</ta>
            <ta e="T138" id="Seg_11989" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_11990" s="T138">pers</ta>
            <ta e="T140" id="Seg_11991" s="T139">n</ta>
            <ta e="T141" id="Seg_11992" s="T140">v</ta>
            <ta e="T142" id="Seg_11993" s="T141">v</ta>
            <ta e="T144" id="Seg_11994" s="T143">v</ta>
            <ta e="T145" id="Seg_11995" s="T144">v</ta>
            <ta e="T146" id="Seg_11996" s="T145">v</ta>
            <ta e="T147" id="Seg_11997" s="T146">v</ta>
            <ta e="T149" id="Seg_11998" s="T148">v</ta>
            <ta e="T150" id="Seg_11999" s="T149">que</ta>
            <ta e="T151" id="Seg_12000" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_12001" s="T151">ptcl</ta>
            <ta e="T154" id="Seg_12002" s="T153">n</ta>
            <ta e="T155" id="Seg_12003" s="T154">v</ta>
            <ta e="T156" id="Seg_12004" s="T155">v</ta>
            <ta e="T157" id="Seg_12005" s="T156">v</ta>
            <ta e="T158" id="Seg_12006" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_12007" s="T158">v</ta>
            <ta e="T160" id="Seg_12008" s="T159">aux</ta>
            <ta e="T162" id="Seg_12009" s="T161">dempro</ta>
            <ta e="T163" id="Seg_12010" s="T162">adj</ta>
            <ta e="T164" id="Seg_12011" s="T163">n</ta>
            <ta e="T165" id="Seg_12012" s="T164">v</ta>
            <ta e="T166" id="Seg_12013" s="T165">adv</ta>
            <ta e="T168" id="Seg_12014" s="T167">cop</ta>
            <ta e="T169" id="Seg_12015" s="T168">ptcl</ta>
            <ta e="T171" id="Seg_12016" s="T170">adv</ta>
            <ta e="T172" id="Seg_12017" s="T171">n</ta>
            <ta e="T173" id="Seg_12018" s="T172">que</ta>
            <ta e="T174" id="Seg_12019" s="T173">v</ta>
            <ta e="T176" id="Seg_12020" s="T175">dempro</ta>
            <ta e="T177" id="Seg_12021" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_12022" s="T177">n</ta>
            <ta e="T179" id="Seg_12023" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_12024" s="T179">n</ta>
            <ta e="T182" id="Seg_12025" s="T181">adv</ta>
            <ta e="T183" id="Seg_12026" s="T182">v</ta>
            <ta e="T184" id="Seg_12027" s="T183">v</ta>
            <ta e="T185" id="Seg_12028" s="T184">n</ta>
            <ta e="T186" id="Seg_12029" s="T185">v</ta>
            <ta e="T187" id="Seg_12030" s="T186">v</ta>
            <ta e="T188" id="Seg_12031" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_12032" s="T188">n</ta>
            <ta e="T190" id="Seg_12033" s="T189">ptcl</ta>
            <ta e="T192" id="Seg_12034" s="T191">adv</ta>
            <ta e="T193" id="Seg_12035" s="T192">v</ta>
            <ta e="T194" id="Seg_12036" s="T193">v</ta>
            <ta e="T195" id="Seg_12037" s="T194">adj</ta>
            <ta e="T196" id="Seg_12038" s="T195">ptcl</ta>
            <ta e="T198" id="Seg_12039" s="T197">n</ta>
            <ta e="T199" id="Seg_12040" s="T198">v</ta>
            <ta e="T200" id="Seg_12041" s="T199">que</ta>
            <ta e="T201" id="Seg_12042" s="T200">n</ta>
            <ta e="T202" id="Seg_12043" s="T201">n</ta>
            <ta e="T203" id="Seg_12044" s="T202">v</ta>
            <ta e="T204" id="Seg_12045" s="T203">aux</ta>
            <ta e="T207" id="Seg_12046" s="T206">v</ta>
            <ta e="T208" id="Seg_12047" s="T207">v</ta>
            <ta e="T210" id="Seg_12048" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_12049" s="T210">dempro</ta>
            <ta e="T212" id="Seg_12050" s="T211">v</ta>
            <ta e="T213" id="Seg_12051" s="T212">v</ta>
            <ta e="T214" id="Seg_12052" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_12053" s="T214">n</ta>
            <ta e="T216" id="Seg_12054" s="T215">v</ta>
            <ta e="T217" id="Seg_12055" s="T216">v</ta>
            <ta e="T219" id="Seg_12056" s="T218">n</ta>
            <ta e="T220" id="Seg_12057" s="T219">v</ta>
            <ta e="T221" id="Seg_12058" s="T220">v</ta>
            <ta e="T222" id="Seg_12059" s="T221">v</ta>
            <ta e="T223" id="Seg_12060" s="T222">v</ta>
            <ta e="T224" id="Seg_12061" s="T223">que</ta>
            <ta e="T225" id="Seg_12062" s="T224">v</ta>
            <ta e="T226" id="Seg_12063" s="T225">aux</ta>
            <ta e="T227" id="Seg_12064" s="T226">n</ta>
            <ta e="T228" id="Seg_12065" s="T227">v</ta>
            <ta e="T229" id="Seg_12066" s="T228">aux</ta>
            <ta e="T230" id="Seg_12067" s="T229">v</ta>
            <ta e="T231" id="Seg_12068" s="T230">n</ta>
            <ta e="T233" id="Seg_12069" s="T232">interj</ta>
            <ta e="T234" id="Seg_12070" s="T233">que</ta>
            <ta e="T235" id="Seg_12071" s="T234">n</ta>
            <ta e="T236" id="Seg_12072" s="T235">n</ta>
            <ta e="T237" id="Seg_12073" s="T236">que</ta>
            <ta e="T238" id="Seg_12074" s="T237">v</ta>
            <ta e="T239" id="Seg_12075" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_12076" s="T239">n</ta>
            <ta e="T241" id="Seg_12077" s="T240">v</ta>
            <ta e="T242" id="Seg_12078" s="T241">n</ta>
            <ta e="T243" id="Seg_12079" s="T242">v</ta>
            <ta e="T245" id="Seg_12080" s="T244">v</ta>
            <ta e="T246" id="Seg_12081" s="T245">adv</ta>
            <ta e="T247" id="Seg_12082" s="T246">adj</ta>
            <ta e="T248" id="Seg_12083" s="T247">v</ta>
            <ta e="T249" id="Seg_12084" s="T248">v</ta>
            <ta e="T251" id="Seg_12085" s="T250">interj</ta>
            <ta e="T252" id="Seg_12086" s="T251">n</ta>
            <ta e="T253" id="Seg_12087" s="T252">ptcl</ta>
            <ta e="T256" id="Seg_12088" s="T255">v</ta>
            <ta e="T257" id="Seg_12089" s="T256">v</ta>
            <ta e="T259" id="Seg_12090" s="T258">que</ta>
            <ta e="T260" id="Seg_12091" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_12092" s="T260">n</ta>
            <ta e="T265" id="Seg_12093" s="T264">dempro</ta>
            <ta e="T266" id="Seg_12094" s="T265">v</ta>
            <ta e="T268" id="Seg_12095" s="T267">ptcl</ta>
            <ta e="T271" id="Seg_12096" s="T270">v</ta>
            <ta e="T273" id="Seg_12097" s="T272">v</ta>
            <ta e="T274" id="Seg_12098" s="T273">aux</ta>
            <ta e="T275" id="Seg_12099" s="T274">ptcl</ta>
            <ta e="T277" id="Seg_12100" s="T276">dempro</ta>
            <ta e="T278" id="Seg_12101" s="T277">v</ta>
            <ta e="T279" id="Seg_12102" s="T278">v</ta>
            <ta e="T281" id="Seg_12103" s="T280">v</ta>
            <ta e="T282" id="Seg_12104" s="T281">v</ta>
            <ta e="T283" id="Seg_12105" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_12106" s="T283">n</ta>
            <ta e="T285" id="Seg_12107" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_12108" s="T285">v</ta>
            <ta e="T287" id="Seg_12109" s="T286">adv</ta>
            <ta e="T288" id="Seg_12110" s="T287">que</ta>
            <ta e="T289" id="Seg_12111" s="T288">quant</ta>
            <ta e="T290" id="Seg_12112" s="T289">v</ta>
            <ta e="T291" id="Seg_12113" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_12114" s="T291">dempro</ta>
            <ta e="T293" id="Seg_12115" s="T292">v</ta>
            <ta e="T296" id="Seg_12116" s="T295">que</ta>
            <ta e="T297" id="Seg_12117" s="T296">v</ta>
            <ta e="T298" id="Seg_12118" s="T297">v</ta>
            <ta e="T299" id="Seg_12119" s="T298">n</ta>
            <ta e="T301" id="Seg_12120" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_12121" s="T301">adv</ta>
            <ta e="T303" id="Seg_12122" s="T302">v</ta>
            <ta e="T304" id="Seg_12123" s="T303">v</ta>
            <ta e="T305" id="Seg_12124" s="T304">ptcl</ta>
            <ta e="T307" id="Seg_12125" s="T306">interj</ta>
            <ta e="T310" id="Seg_12126" s="T309">interj</ta>
            <ta e="T311" id="Seg_12127" s="T310">n</ta>
            <ta e="T312" id="Seg_12128" s="T311">adj</ta>
            <ta e="T313" id="Seg_12129" s="T312">v</ta>
            <ta e="T314" id="Seg_12130" s="T313">adv</ta>
            <ta e="T316" id="Seg_12131" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_12132" s="T316">dempro</ta>
            <ta e="T318" id="Seg_12133" s="T317">v</ta>
            <ta e="T319" id="Seg_12134" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_12135" s="T319">n</ta>
            <ta e="T321" id="Seg_12136" s="T320">post</ta>
            <ta e="T323" id="Seg_12137" s="T322">n</ta>
            <ta e="T324" id="Seg_12138" s="T323">n</ta>
            <ta e="T325" id="Seg_12139" s="T324">v</ta>
            <ta e="T326" id="Seg_12140" s="T325">adv</ta>
            <ta e="T327" id="Seg_12141" s="T326">n</ta>
            <ta e="T328" id="Seg_12142" s="T327">v</ta>
            <ta e="T330" id="Seg_12143" s="T329">n</ta>
            <ta e="T331" id="Seg_12144" s="T330">v</ta>
            <ta e="T332" id="Seg_12145" s="T331">v</ta>
            <ta e="T333" id="Seg_12146" s="T332">v</ta>
            <ta e="T334" id="Seg_12147" s="T333">aux</ta>
            <ta e="T336" id="Seg_12148" s="T335">que</ta>
            <ta e="T337" id="Seg_12149" s="T336">adj</ta>
            <ta e="T338" id="Seg_12150" s="T337">cop</ta>
            <ta e="T339" id="Seg_12151" s="T338">pers</ta>
            <ta e="T340" id="Seg_12152" s="T339">propr</ta>
            <ta e="T341" id="Seg_12153" s="T340">adj</ta>
            <ta e="T342" id="Seg_12154" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_12155" s="T342">adj</ta>
            <ta e="T344" id="Seg_12156" s="T343">ptcl</ta>
            <ta e="T345" id="Seg_12157" s="T344">n</ta>
            <ta e="T346" id="Seg_12158" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_12159" s="T346">dempro</ta>
            <ta e="T349" id="Seg_12160" s="T348">que</ta>
            <ta e="T351" id="Seg_12161" s="T350">v</ta>
            <ta e="T352" id="Seg_12162" s="T351">ptcl</ta>
            <ta e="T353" id="Seg_12163" s="T352">que</ta>
            <ta e="T354" id="Seg_12164" s="T353">n</ta>
            <ta e="T355" id="Seg_12165" s="T354">adj</ta>
            <ta e="T356" id="Seg_12166" s="T355">v</ta>
            <ta e="T357" id="Seg_12167" s="T356">aux</ta>
            <ta e="T359" id="Seg_12168" s="T358">interj</ta>
            <ta e="T360" id="Seg_12169" s="T359">v</ta>
            <ta e="T361" id="Seg_12170" s="T360">v</ta>
            <ta e="T362" id="Seg_12171" s="T361">post</ta>
            <ta e="T363" id="Seg_12172" s="T362">cardnum</ta>
            <ta e="T364" id="Seg_12173" s="T363">n</ta>
            <ta e="T365" id="Seg_12174" s="T364">v</ta>
            <ta e="T367" id="Seg_12175" s="T366">dempro</ta>
            <ta e="T368" id="Seg_12176" s="T367">v</ta>
            <ta e="T369" id="Seg_12177" s="T368">v</ta>
            <ta e="T370" id="Seg_12178" s="T369">ptcl</ta>
            <ta e="T372" id="Seg_12179" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_12180" s="T372">dempro</ta>
            <ta e="T374" id="Seg_12181" s="T373">que</ta>
            <ta e="T375" id="Seg_12182" s="T374">v</ta>
            <ta e="T376" id="Seg_12183" s="T375">aux</ta>
            <ta e="T377" id="Seg_12184" s="T376">adv</ta>
            <ta e="T378" id="Seg_12185" s="T377">n</ta>
            <ta e="T379" id="Seg_12186" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_12187" s="T379">v</ta>
            <ta e="T381" id="Seg_12188" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_12189" s="T381">n</ta>
            <ta e="T384" id="Seg_12190" s="T383">n</ta>
            <ta e="T385" id="Seg_12191" s="T384">n</ta>
            <ta e="T386" id="Seg_12192" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_12193" s="T386">que</ta>
            <ta e="T388" id="Seg_12194" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_12195" s="T388">n</ta>
            <ta e="T390" id="Seg_12196" s="T389">n</ta>
            <ta e="T391" id="Seg_12197" s="T390">interj</ta>
            <ta e="T392" id="Seg_12198" s="T391">adj</ta>
            <ta e="T393" id="Seg_12199" s="T392">n</ta>
            <ta e="T394" id="Seg_12200" s="T393">adj</ta>
            <ta e="T395" id="Seg_12201" s="T394">n</ta>
            <ta e="T396" id="Seg_12202" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_12203" s="T396">adj</ta>
            <ta e="T399" id="Seg_12204" s="T398">dempro</ta>
            <ta e="T400" id="Seg_12205" s="T399">que</ta>
            <ta e="T401" id="Seg_12206" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_12207" s="T401">v</ta>
            <ta e="T403" id="Seg_12208" s="T402">aux</ta>
            <ta e="T404" id="Seg_12209" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_12210" s="T404">v</ta>
            <ta e="T406" id="Seg_12211" s="T405">aux</ta>
            <ta e="T407" id="Seg_12212" s="T406">dempro</ta>
            <ta e="T408" id="Seg_12213" s="T407">que</ta>
            <ta e="T409" id="Seg_12214" s="T408">n</ta>
            <ta e="T410" id="Seg_12215" s="T409">v</ta>
            <ta e="T411" id="Seg_12216" s="T410">ptcl</ta>
            <ta e="T412" id="Seg_12217" s="T411">v</ta>
            <ta e="T413" id="Seg_12218" s="T412">v</ta>
            <ta e="T415" id="Seg_12219" s="T414">v</ta>
            <ta e="T417" id="Seg_12220" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_12221" s="T417">v</ta>
            <ta e="T419" id="Seg_12222" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_12223" s="T419">pers</ta>
            <ta e="T421" id="Seg_12224" s="T420">post</ta>
            <ta e="T422" id="Seg_12225" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_12226" s="T422">dempro</ta>
            <ta e="T424" id="Seg_12227" s="T423">que</ta>
            <ta e="T425" id="Seg_12228" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_12229" s="T425">cop</ta>
            <ta e="T427" id="Seg_12230" s="T1150">n</ta>
            <ta e="T428" id="Seg_12231" s="T427">v</ta>
            <ta e="T429" id="Seg_12232" s="T428">dempro</ta>
            <ta e="T430" id="Seg_12233" s="T429">post</ta>
            <ta e="T431" id="Seg_12234" s="T430">que</ta>
            <ta e="T432" id="Seg_12235" s="T431">v</ta>
            <ta e="T433" id="Seg_12236" s="T432">n</ta>
            <ta e="T434" id="Seg_12237" s="T433">n</ta>
            <ta e="T435" id="Seg_12238" s="T434">v</ta>
            <ta e="T436" id="Seg_12239" s="T435">v</ta>
            <ta e="T437" id="Seg_12240" s="T436">v</ta>
            <ta e="T438" id="Seg_12241" s="T437">propr</ta>
            <ta e="T440" id="Seg_12242" s="T439">propr</ta>
            <ta e="T441" id="Seg_12243" s="T440">propr</ta>
            <ta e="T442" id="Seg_12244" s="T441">v</ta>
            <ta e="T444" id="Seg_12245" s="T443">propr</ta>
            <ta e="T445" id="Seg_12246" s="T444">v</ta>
            <ta e="T447" id="Seg_12247" s="T446">que</ta>
            <ta e="T448" id="Seg_12248" s="T447">v</ta>
            <ta e="T449" id="Seg_12249" s="T448">v</ta>
            <ta e="T450" id="Seg_12250" s="T449">v</ta>
            <ta e="T451" id="Seg_12251" s="T450">n</ta>
            <ta e="T452" id="Seg_12252" s="T451">v</ta>
            <ta e="T453" id="Seg_12253" s="T452">v</ta>
            <ta e="T455" id="Seg_12254" s="T454">interj</ta>
            <ta e="T456" id="Seg_12255" s="T455">n</ta>
            <ta e="T457" id="Seg_12256" s="T456">v</ta>
            <ta e="T458" id="Seg_12257" s="T457">v</ta>
            <ta e="T460" id="Seg_12258" s="T459">v</ta>
            <ta e="T461" id="Seg_12259" s="T460">adv</ta>
            <ta e="T462" id="Seg_12260" s="T461">dempro</ta>
            <ta e="T463" id="Seg_12261" s="T462">n</ta>
            <ta e="T464" id="Seg_12262" s="T463">v</ta>
            <ta e="T466" id="Seg_12263" s="T465">propr</ta>
            <ta e="T467" id="Seg_12264" s="T466">propr</ta>
            <ta e="T468" id="Seg_12265" s="T467">propr</ta>
            <ta e="T469" id="Seg_12266" s="T468">v</ta>
            <ta e="T470" id="Seg_12267" s="T469">adv</ta>
            <ta e="T471" id="Seg_12268" s="T470">v</ta>
            <ta e="T472" id="Seg_12269" s="T471">v</ta>
            <ta e="T473" id="Seg_12270" s="T472">ptcl</ta>
            <ta e="T475" id="Seg_12271" s="T474">interj</ta>
            <ta e="T476" id="Seg_12272" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_12273" s="T476">dempro</ta>
            <ta e="T478" id="Seg_12274" s="T477">v</ta>
            <ta e="T479" id="Seg_12275" s="T478">aux</ta>
            <ta e="T480" id="Seg_12276" s="T479">v</ta>
            <ta e="T482" id="Seg_12277" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_12278" s="T482">v</ta>
            <ta e="T484" id="Seg_12279" s="T483">ptcl</ta>
            <ta e="T485" id="Seg_12280" s="T484">n</ta>
            <ta e="T486" id="Seg_12281" s="T485">pers</ta>
            <ta e="T487" id="Seg_12282" s="T486">n</ta>
            <ta e="T490" id="Seg_12283" s="T489">adv</ta>
            <ta e="T491" id="Seg_12284" s="T490">v</ta>
            <ta e="T492" id="Seg_12285" s="T491">v</ta>
            <ta e="T493" id="Seg_12286" s="T492">v</ta>
            <ta e="T495" id="Seg_12287" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_12288" s="T495">v</ta>
            <ta e="T497" id="Seg_12289" s="T496">n</ta>
            <ta e="T498" id="Seg_12290" s="T497">v</ta>
            <ta e="T499" id="Seg_12291" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_12292" s="T499">ptcl</ta>
            <ta e="T502" id="Seg_12293" s="T501">v</ta>
            <ta e="T503" id="Seg_12294" s="T502">v</ta>
            <ta e="T504" id="Seg_12295" s="T503">v</ta>
            <ta e="T505" id="Seg_12296" s="T504">v</ta>
            <ta e="T506" id="Seg_12297" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_12298" s="T506">n</ta>
            <ta e="T508" id="Seg_12299" s="T507">ptcl</ta>
            <ta e="T510" id="Seg_12300" s="T509">v</ta>
            <ta e="T511" id="Seg_12301" s="T510">v</ta>
            <ta e="T513" id="Seg_12302" s="T512">pers</ta>
            <ta e="T514" id="Seg_12303" s="T513">v</ta>
            <ta e="T515" id="Seg_12304" s="T514">aux</ta>
            <ta e="T516" id="Seg_12305" s="T515">v</ta>
            <ta e="T518" id="Seg_12306" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_12307" s="T518">dempro</ta>
            <ta e="T520" id="Seg_12308" s="T519">n</ta>
            <ta e="T521" id="Seg_12309" s="T520">v</ta>
            <ta e="T523" id="Seg_12310" s="T522">ptcl</ta>
            <ta e="T524" id="Seg_12311" s="T523">que</ta>
            <ta e="T525" id="Seg_12312" s="T524">cop</ta>
            <ta e="T527" id="Seg_12313" s="T526">v</ta>
            <ta e="T528" id="Seg_12314" s="T527">v</ta>
            <ta e="T529" id="Seg_12315" s="T528">pers</ta>
            <ta e="T530" id="Seg_12316" s="T529">v</ta>
            <ta e="T531" id="Seg_12317" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_12318" s="T531">v</ta>
            <ta e="T533" id="Seg_12319" s="T532">v</ta>
            <ta e="T534" id="Seg_12320" s="T533">n</ta>
            <ta e="T535" id="Seg_12321" s="T534">ptcl</ta>
            <ta e="T536" id="Seg_12322" s="T535">interj</ta>
            <ta e="T537" id="Seg_12323" s="T536">conj</ta>
            <ta e="T539" id="Seg_12324" s="T538">v</ta>
            <ta e="T540" id="Seg_12325" s="T539">v</ta>
            <ta e="T541" id="Seg_12326" s="T540">v</ta>
            <ta e="T542" id="Seg_12327" s="T541">ptcl</ta>
            <ta e="T544" id="Seg_12328" s="T543">v</ta>
            <ta e="T545" id="Seg_12329" s="T544">v</ta>
            <ta e="T547" id="Seg_12330" s="T546">emphpro</ta>
            <ta e="T548" id="Seg_12331" s="T547">v</ta>
            <ta e="T549" id="Seg_12332" s="T548">v</ta>
            <ta e="T550" id="Seg_12333" s="T549">n</ta>
            <ta e="T551" id="Seg_12334" s="T550">adj</ta>
            <ta e="T553" id="Seg_12335" s="T552">dempro</ta>
            <ta e="T554" id="Seg_12336" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_12337" s="T554">v</ta>
            <ta e="T556" id="Seg_12338" s="T555">aux</ta>
            <ta e="T557" id="Seg_12339" s="T556">dempro</ta>
            <ta e="T558" id="Seg_12340" s="T557">n</ta>
            <ta e="T560" id="Seg_12341" s="T559">ptcl</ta>
            <ta e="T562" id="Seg_12342" s="T561">v</ta>
            <ta e="T563" id="Seg_12343" s="T562">dempro</ta>
            <ta e="T564" id="Seg_12344" s="T563">adv</ta>
            <ta e="T565" id="Seg_12345" s="T564">n</ta>
            <ta e="T566" id="Seg_12346" s="T565">adj</ta>
            <ta e="T567" id="Seg_12347" s="T566">v</ta>
            <ta e="T568" id="Seg_12348" s="T567">dempro</ta>
            <ta e="T569" id="Seg_12349" s="T568">n</ta>
            <ta e="T570" id="Seg_12350" s="T569">post</ta>
            <ta e="T571" id="Seg_12351" s="T570">que</ta>
            <ta e="T572" id="Seg_12352" s="T571">v</ta>
            <ta e="T574" id="Seg_12353" s="T573">dempro</ta>
            <ta e="T575" id="Seg_12354" s="T574">n</ta>
            <ta e="T576" id="Seg_12355" s="T575">cop</ta>
            <ta e="T577" id="Seg_12356" s="T576">ptcl</ta>
            <ta e="T579" id="Seg_12357" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_12358" s="T579">dempro</ta>
            <ta e="T581" id="Seg_12359" s="T580">n</ta>
            <ta e="T582" id="Seg_12360" s="T581">v</ta>
            <ta e="T583" id="Seg_12361" s="T582">ptcl</ta>
            <ta e="T585" id="Seg_12362" s="T584">adv</ta>
            <ta e="T586" id="Seg_12363" s="T585">conj</ta>
            <ta e="T587" id="Seg_12364" s="T586">que</ta>
            <ta e="T588" id="Seg_12365" s="T587">n</ta>
            <ta e="T589" id="Seg_12366" s="T588">ptcl</ta>
            <ta e="T591" id="Seg_12367" s="T590">n</ta>
            <ta e="T592" id="Seg_12368" s="T591">adv</ta>
            <ta e="T593" id="Seg_12369" s="T592">v</ta>
            <ta e="T598" id="Seg_12370" s="T597">que</ta>
            <ta e="T599" id="Seg_12371" s="T598">n</ta>
            <ta e="T600" id="Seg_12372" s="T599">ptcl</ta>
            <ta e="T601" id="Seg_12373" s="T600">v</ta>
            <ta e="T602" id="Seg_12374" s="T601">ptcl</ta>
            <ta e="T603" id="Seg_12375" s="T602">adv</ta>
            <ta e="T604" id="Seg_12376" s="T603">n</ta>
            <ta e="T606" id="Seg_12377" s="T605">n</ta>
            <ta e="T607" id="Seg_12378" s="T606">v</ta>
            <ta e="T609" id="Seg_12379" s="T608">v</ta>
            <ta e="T610" id="Seg_12380" s="T609">v</ta>
            <ta e="T611" id="Seg_12381" s="T610">n</ta>
            <ta e="T613" id="Seg_12382" s="T612">n</ta>
            <ta e="T614" id="Seg_12383" s="T613">%%</ta>
            <ta e="T615" id="Seg_12384" s="T614">v</ta>
            <ta e="T616" id="Seg_12385" s="T615">v</ta>
            <ta e="T617" id="Seg_12386" s="T616">%%</ta>
            <ta e="T620" id="Seg_12387" s="T619">v</ta>
            <ta e="T621" id="Seg_12388" s="T620">n</ta>
            <ta e="T622" id="Seg_12389" s="T621">n</ta>
            <ta e="T623" id="Seg_12390" s="T622">v</ta>
            <ta e="T624" id="Seg_12391" s="T623">n</ta>
            <ta e="T625" id="Seg_12392" s="T624">v</ta>
            <ta e="T627" id="Seg_12393" s="T626">n</ta>
            <ta e="T628" id="Seg_12394" s="T627">cop</ta>
            <ta e="T629" id="Seg_12395" s="T628">que</ta>
            <ta e="T630" id="Seg_12396" s="T629">v</ta>
            <ta e="T634" id="Seg_12397" s="T633">n</ta>
            <ta e="T635" id="Seg_12398" s="T634">v</ta>
            <ta e="T636" id="Seg_12399" s="T635">ptcl</ta>
            <ta e="T638" id="Seg_12400" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_12401" s="T638">dempro</ta>
            <ta e="T640" id="Seg_12402" s="T639">v</ta>
            <ta e="T641" id="Seg_12403" s="T640">ptcl</ta>
            <ta e="T643" id="Seg_12404" s="T642">pers</ta>
            <ta e="T644" id="Seg_12405" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_12406" s="T644">adv</ta>
            <ta e="T646" id="Seg_12407" s="T645">v</ta>
            <ta e="T647" id="Seg_12408" s="T646">n</ta>
            <ta e="T648" id="Seg_12409" s="T647">n</ta>
            <ta e="T649" id="Seg_12410" s="T648">v</ta>
            <ta e="T650" id="Seg_12411" s="T649">ptcl</ta>
            <ta e="T652" id="Seg_12412" s="T651">v</ta>
            <ta e="T653" id="Seg_12413" s="T652">v</ta>
            <ta e="T654" id="Seg_12414" s="T653">v</ta>
            <ta e="T656" id="Seg_12415" s="T655">adv</ta>
            <ta e="T657" id="Seg_12416" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_12417" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_12418" s="T658">que</ta>
            <ta e="T660" id="Seg_12419" s="T659">n</ta>
            <ta e="T663" id="Seg_12420" s="T662">n</ta>
            <ta e="T664" id="Seg_12421" s="T663">n</ta>
            <ta e="T665" id="Seg_12422" s="T664">aux</ta>
            <ta e="T666" id="Seg_12423" s="T665">interj</ta>
            <ta e="T668" id="Seg_12424" s="T667">n</ta>
            <ta e="T669" id="Seg_12425" s="T668">adj</ta>
            <ta e="T670" id="Seg_12426" s="T669">ptcl</ta>
            <ta e="T671" id="Seg_12427" s="T670">conj</ta>
            <ta e="T672" id="Seg_12428" s="T671">adj</ta>
            <ta e="T674" id="Seg_12429" s="T673">que</ta>
            <ta e="T675" id="Seg_12430" s="T674">v</ta>
            <ta e="T676" id="Seg_12431" s="T675">aux</ta>
            <ta e="T677" id="Seg_12432" s="T676">pers</ta>
            <ta e="T679" id="Seg_12433" s="T678">v</ta>
            <ta e="T680" id="Seg_12434" s="T679">aux</ta>
            <ta e="T682" id="Seg_12435" s="T681">n</ta>
            <ta e="T683" id="Seg_12436" s="T682">post</ta>
            <ta e="T684" id="Seg_12437" s="T683">n</ta>
            <ta e="T685" id="Seg_12438" s="T684">v</ta>
            <ta e="T686" id="Seg_12439" s="T685">ptcl</ta>
            <ta e="T687" id="Seg_12440" s="T686">adj</ta>
            <ta e="T689" id="Seg_12441" s="T688">n</ta>
            <ta e="T690" id="Seg_12442" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_12443" s="T690">v</ta>
            <ta e="T692" id="Seg_12444" s="T691">n</ta>
            <ta e="T693" id="Seg_12445" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_12446" s="T693">v</ta>
            <ta e="T695" id="Seg_12447" s="T694">n</ta>
            <ta e="T696" id="Seg_12448" s="T695">v</ta>
            <ta e="T699" id="Seg_12449" s="T698">dempro</ta>
            <ta e="T700" id="Seg_12450" s="T699">que</ta>
            <ta e="T701" id="Seg_12451" s="T700">interj</ta>
            <ta e="T702" id="Seg_12452" s="T701">dempro</ta>
            <ta e="T703" id="Seg_12453" s="T702">n</ta>
            <ta e="T704" id="Seg_12454" s="T703">v</ta>
            <ta e="T705" id="Seg_12455" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_12456" s="T705">n</ta>
            <ta e="T707" id="Seg_12457" s="T706">ptcl</ta>
            <ta e="T709" id="Seg_12458" s="T708">dempro</ta>
            <ta e="T710" id="Seg_12459" s="T709">v</ta>
            <ta e="T711" id="Seg_12460" s="T710">aux</ta>
            <ta e="T712" id="Seg_12461" s="T711">dempro</ta>
            <ta e="T714" id="Seg_12462" s="T713">ptcl</ta>
            <ta e="T716" id="Seg_12463" s="T715">dempro</ta>
            <ta e="T717" id="Seg_12464" s="T716">que</ta>
            <ta e="T718" id="Seg_12465" s="T717">ptcl</ta>
            <ta e="T719" id="Seg_12466" s="T718">n</ta>
            <ta e="T720" id="Seg_12467" s="T719">v</ta>
            <ta e="T721" id="Seg_12468" s="T720">v</ta>
            <ta e="T722" id="Seg_12469" s="T721">v</ta>
            <ta e="T723" id="Seg_12470" s="T722">v</ta>
            <ta e="T725" id="Seg_12471" s="T724">adv</ta>
            <ta e="T726" id="Seg_12472" s="T725">adv</ta>
            <ta e="T727" id="Seg_12473" s="T726">ptcl</ta>
            <ta e="T728" id="Seg_12474" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_12475" s="T728">v</ta>
            <ta e="T730" id="Seg_12476" s="T729">v</ta>
            <ta e="T731" id="Seg_12477" s="T730">adv</ta>
            <ta e="T733" id="Seg_12478" s="T732">propr</ta>
            <ta e="T734" id="Seg_12479" s="T733">v</ta>
            <ta e="T736" id="Seg_12480" s="T735">interj</ta>
            <ta e="T737" id="Seg_12481" s="T736">ptcl</ta>
            <ta e="T738" id="Seg_12482" s="T737">v</ta>
            <ta e="T740" id="Seg_12483" s="T739">v</ta>
            <ta e="T741" id="Seg_12484" s="T740">adv</ta>
            <ta e="T742" id="Seg_12485" s="T741">n</ta>
            <ta e="T743" id="Seg_12486" s="T742">post</ta>
            <ta e="T745" id="Seg_12487" s="T744">dempro</ta>
            <ta e="T746" id="Seg_12488" s="T745">que</ta>
            <ta e="T749" id="Seg_12489" s="T748">v</ta>
            <ta e="T750" id="Seg_12490" s="T749">ptcl</ta>
            <ta e="T753" id="Seg_12491" s="T752">dempro</ta>
            <ta e="T754" id="Seg_12492" s="T753">que</ta>
            <ta e="T755" id="Seg_12493" s="T754">adj</ta>
            <ta e="T756" id="Seg_12494" s="T755">adj</ta>
            <ta e="T757" id="Seg_12495" s="T756">pers</ta>
            <ta e="T759" id="Seg_12496" s="T758">n</ta>
            <ta e="T760" id="Seg_12497" s="T759">ptcl</ta>
            <ta e="T761" id="Seg_12498" s="T760">n</ta>
            <ta e="T762" id="Seg_12499" s="T761">v</ta>
            <ta e="T764" id="Seg_12500" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_12501" s="T764">v</ta>
            <ta e="T766" id="Seg_12502" s="T765">ptcl</ta>
            <ta e="T768" id="Seg_12503" s="T767">ptcl</ta>
            <ta e="T769" id="Seg_12504" s="T768">adv</ta>
            <ta e="T770" id="Seg_12505" s="T769">v</ta>
            <ta e="T771" id="Seg_12506" s="T770">emphpro</ta>
            <ta e="T772" id="Seg_12507" s="T771">v</ta>
            <ta e="T774" id="Seg_12508" s="T773">n</ta>
            <ta e="T775" id="Seg_12509" s="T774">v</ta>
            <ta e="T776" id="Seg_12510" s="T775">v</ta>
            <ta e="T777" id="Seg_12511" s="T776">ptcl</ta>
            <ta e="T780" id="Seg_12512" s="T779">que</ta>
            <ta e="T781" id="Seg_12513" s="T780">ptcl</ta>
            <ta e="T782" id="Seg_12514" s="T781">v</ta>
            <ta e="T784" id="Seg_12515" s="T783">n</ta>
            <ta e="T785" id="Seg_12516" s="T784">que</ta>
            <ta e="T786" id="Seg_12517" s="T785">ptcl</ta>
            <ta e="T788" id="Seg_12518" s="T787">dempro</ta>
            <ta e="T789" id="Seg_12519" s="T788">v</ta>
            <ta e="T790" id="Seg_12520" s="T789">cop</ta>
            <ta e="T791" id="Seg_12521" s="T790">aux</ta>
            <ta e="T792" id="Seg_12522" s="T791">v</ta>
            <ta e="T793" id="Seg_12523" s="T792">adv</ta>
            <ta e="T794" id="Seg_12524" s="T793">adv</ta>
            <ta e="T795" id="Seg_12525" s="T794">ptcl</ta>
            <ta e="T796" id="Seg_12526" s="T795">v</ta>
            <ta e="T797" id="Seg_12527" s="T796">dempro</ta>
            <ta e="T798" id="Seg_12528" s="T797">ptcl</ta>
            <ta e="T799" id="Seg_12529" s="T798">n</ta>
            <ta e="T800" id="Seg_12530" s="T799">v</ta>
            <ta e="T801" id="Seg_12531" s="T800">dempro</ta>
            <ta e="T802" id="Seg_12532" s="T801">v</ta>
            <ta e="T803" id="Seg_12533" s="T802">aux</ta>
            <ta e="T805" id="Seg_12534" s="T804">ptcl</ta>
            <ta e="T806" id="Seg_12535" s="T805">v</ta>
            <ta e="T807" id="Seg_12536" s="T806">v</ta>
            <ta e="T808" id="Seg_12537" s="T807">v</ta>
            <ta e="T809" id="Seg_12538" s="T808">n</ta>
            <ta e="T810" id="Seg_12539" s="T809">v</ta>
            <ta e="T811" id="Seg_12540" s="T810">ptcl</ta>
            <ta e="T813" id="Seg_12541" s="T812">n</ta>
            <ta e="T814" id="Seg_12542" s="T813">n</ta>
            <ta e="T815" id="Seg_12543" s="T814">v</ta>
            <ta e="T816" id="Seg_12544" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_12545" s="T816">adv</ta>
            <ta e="T818" id="Seg_12546" s="T817">cop</ta>
            <ta e="T819" id="Seg_12547" s="T818">ptcl</ta>
            <ta e="T820" id="Seg_12548" s="T819">adj</ta>
            <ta e="T822" id="Seg_12549" s="T821">ptcl</ta>
            <ta e="T823" id="Seg_12550" s="T822">adj</ta>
            <ta e="T824" id="Seg_12551" s="T823">adv</ta>
            <ta e="T825" id="Seg_12552" s="T824">post</ta>
            <ta e="T827" id="Seg_12553" s="T826">interj</ta>
            <ta e="T828" id="Seg_12554" s="T827">n</ta>
            <ta e="T830" id="Seg_12555" s="T829">n</ta>
            <ta e="T831" id="Seg_12556" s="T830">post</ta>
            <ta e="T832" id="Seg_12557" s="T831">v</ta>
            <ta e="T833" id="Seg_12558" s="T832">v</ta>
            <ta e="T834" id="Seg_12559" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_12560" s="T834">ptcl</ta>
            <ta e="T836" id="Seg_12561" s="T835">adv</ta>
            <ta e="T837" id="Seg_12562" s="T836">ptcl</ta>
            <ta e="T839" id="Seg_12563" s="T838">que</ta>
            <ta e="T840" id="Seg_12564" s="T839">n</ta>
            <ta e="T841" id="Seg_12565" s="T840">ptcl</ta>
            <ta e="T842" id="Seg_12566" s="T841">n</ta>
            <ta e="T844" id="Seg_12567" s="T843">v</ta>
            <ta e="T845" id="Seg_12568" s="T844">ptcl</ta>
            <ta e="T846" id="Seg_12569" s="T845">adj</ta>
            <ta e="T847" id="Seg_12570" s="T846">que</ta>
            <ta e="T848" id="Seg_12571" s="T847">ptcl</ta>
            <ta e="T849" id="Seg_12572" s="T848">n</ta>
            <ta e="T850" id="Seg_12573" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_12574" s="T850">que</ta>
            <ta e="T852" id="Seg_12575" s="T851">n</ta>
            <ta e="T853" id="Seg_12576" s="T852">ptcl</ta>
            <ta e="T854" id="Seg_12577" s="T853">v</ta>
            <ta e="T855" id="Seg_12578" s="T854">v</ta>
            <ta e="T857" id="Seg_12579" s="T856">dempro</ta>
            <ta e="T858" id="Seg_12580" s="T857">dempro</ta>
            <ta e="T859" id="Seg_12581" s="T858">interj</ta>
            <ta e="T860" id="Seg_12582" s="T859">n</ta>
            <ta e="T863" id="Seg_12583" s="T862">v</ta>
            <ta e="T864" id="Seg_12584" s="T863">dempro</ta>
            <ta e="T865" id="Seg_12585" s="T864">v</ta>
            <ta e="T866" id="Seg_12586" s="T865">v</ta>
            <ta e="T868" id="Seg_12587" s="T867">interj</ta>
            <ta e="T869" id="Seg_12588" s="T868">n</ta>
            <ta e="T870" id="Seg_12589" s="T869">v</ta>
            <ta e="T871" id="Seg_12590" s="T870">ptcl</ta>
            <ta e="T872" id="Seg_12591" s="T871">n</ta>
            <ta e="T877" id="Seg_12592" s="T876">n</ta>
            <ta e="T878" id="Seg_12593" s="T877">v</ta>
            <ta e="T879" id="Seg_12594" s="T878">n</ta>
            <ta e="T880" id="Seg_12595" s="T879">ptcl</ta>
            <ta e="T882" id="Seg_12596" s="T881">ptcl</ta>
            <ta e="T883" id="Seg_12597" s="T882">dempro</ta>
            <ta e="T884" id="Seg_12598" s="T883">v</ta>
            <ta e="T886" id="Seg_12599" s="T885">pers</ta>
            <ta e="T887" id="Seg_12600" s="T886">ptcl</ta>
            <ta e="T888" id="Seg_12601" s="T887">v</ta>
            <ta e="T889" id="Seg_12602" s="T888">ptcl</ta>
            <ta e="T890" id="Seg_12603" s="T889">v</ta>
            <ta e="T891" id="Seg_12604" s="T890">v</ta>
            <ta e="T892" id="Seg_12605" s="T891">v</ta>
            <ta e="T893" id="Seg_12606" s="T892">ptcl</ta>
            <ta e="T895" id="Seg_12607" s="T894">pers</ta>
            <ta e="T896" id="Seg_12608" s="T895">ptcl</ta>
            <ta e="T897" id="Seg_12609" s="T896">propr</ta>
            <ta e="T898" id="Seg_12610" s="T897">post</ta>
            <ta e="T899" id="Seg_12611" s="T898">v</ta>
            <ta e="T900" id="Seg_12612" s="T899">conj</ta>
            <ta e="T901" id="Seg_12613" s="T900">propr</ta>
            <ta e="T902" id="Seg_12614" s="T901">post</ta>
            <ta e="T904" id="Seg_12615" s="T903">ptcl</ta>
            <ta e="T905" id="Seg_12616" s="T904">dempro</ta>
            <ta e="T906" id="Seg_12617" s="T905">v</ta>
            <ta e="T909" id="Seg_12618" s="T908">v</ta>
            <ta e="T910" id="Seg_12619" s="T909">post</ta>
            <ta e="T911" id="Seg_12620" s="T910">ptcl</ta>
            <ta e="T913" id="Seg_12621" s="T912">interj</ta>
            <ta e="T914" id="Seg_12622" s="T913">n</ta>
            <ta e="T915" id="Seg_12623" s="T914">v</ta>
            <ta e="T917" id="Seg_12624" s="T916">interj</ta>
            <ta e="T918" id="Seg_12625" s="T917">n</ta>
            <ta e="T919" id="Seg_12626" s="T918">v</ta>
            <ta e="T920" id="Seg_12627" s="T919">v</ta>
            <ta e="T921" id="Seg_12628" s="T920">v</ta>
            <ta e="T922" id="Seg_12629" s="T921">v</ta>
            <ta e="T923" id="Seg_12630" s="T922">n</ta>
            <ta e="T924" id="Seg_12631" s="T923">n</ta>
            <ta e="T925" id="Seg_12632" s="T924">v</ta>
            <ta e="T926" id="Seg_12633" s="T925">v</ta>
            <ta e="T928" id="Seg_12634" s="T927">interj</ta>
            <ta e="T929" id="Seg_12635" s="T928">n</ta>
            <ta e="T930" id="Seg_12636" s="T929">adj</ta>
            <ta e="T931" id="Seg_12637" s="T930">v</ta>
            <ta e="T933" id="Seg_12638" s="T932">n</ta>
            <ta e="T934" id="Seg_12639" s="T933">v</ta>
            <ta e="T936" id="Seg_12640" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_12641" s="T936">v</ta>
            <ta e="T938" id="Seg_12642" s="T937">interj</ta>
            <ta e="T939" id="Seg_12643" s="T938">ptcl</ta>
            <ta e="T940" id="Seg_12644" s="T939">n</ta>
            <ta e="T941" id="Seg_12645" s="T940">interj</ta>
            <ta e="T942" id="Seg_12646" s="T941">v</ta>
            <ta e="T944" id="Seg_12647" s="T943">dempro</ta>
            <ta e="T945" id="Seg_12648" s="T944">adj</ta>
            <ta e="T946" id="Seg_12649" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_12650" s="T946">adv</ta>
            <ta e="T948" id="Seg_12651" s="T947">v</ta>
            <ta e="T950" id="Seg_12652" s="T949">que</ta>
            <ta e="T951" id="Seg_12653" s="T950">n</ta>
            <ta e="T952" id="Seg_12654" s="T951">ptcl</ta>
            <ta e="T953" id="Seg_12655" s="T952">adj</ta>
            <ta e="T954" id="Seg_12656" s="T953">ptcl</ta>
            <ta e="T955" id="Seg_12657" s="T954">que</ta>
            <ta e="T956" id="Seg_12658" s="T955">n</ta>
            <ta e="T957" id="Seg_12659" s="T956">v</ta>
            <ta e="T959" id="Seg_12660" s="T958">dempro</ta>
            <ta e="T961" id="Seg_12661" s="T960">adj</ta>
            <ta e="T962" id="Seg_12662" s="T961">que</ta>
            <ta e="T963" id="Seg_12663" s="T962">ptcl</ta>
            <ta e="T964" id="Seg_12664" s="T963">ptcl</ta>
            <ta e="T965" id="Seg_12665" s="T964">adj</ta>
            <ta e="T966" id="Seg_12666" s="T965">v</ta>
            <ta e="T967" id="Seg_12667" s="T966">ptcl</ta>
            <ta e="T968" id="Seg_12668" s="T967">adj</ta>
            <ta e="T970" id="Seg_12669" s="T969">ptcl</ta>
            <ta e="T971" id="Seg_12670" s="T970">v</ta>
            <ta e="T972" id="Seg_12671" s="T971">interj</ta>
            <ta e="T973" id="Seg_12672" s="T972">cop</ta>
            <ta e="T974" id="Seg_12673" s="T973">v</ta>
            <ta e="T976" id="Seg_12674" s="T975">v</ta>
            <ta e="T977" id="Seg_12675" s="T976">ptcl</ta>
            <ta e="T978" id="Seg_12676" s="T977">ptcl</ta>
            <ta e="T980" id="Seg_12677" s="T979">v</ta>
            <ta e="T981" id="Seg_12678" s="T980">interj</ta>
            <ta e="T982" id="Seg_12679" s="T981">ptcl</ta>
            <ta e="T984" id="Seg_12680" s="T983">ptcl</ta>
            <ta e="T985" id="Seg_12681" s="T984">v</ta>
            <ta e="T986" id="Seg_12682" s="T985">adv</ta>
            <ta e="T987" id="Seg_12683" s="T986">v</ta>
            <ta e="T988" id="Seg_12684" s="T987">v</ta>
            <ta e="T989" id="Seg_12685" s="T988">v</ta>
            <ta e="T990" id="Seg_12686" s="T989">v</ta>
            <ta e="T991" id="Seg_12687" s="T990">ptcl</ta>
            <ta e="T993" id="Seg_12688" s="T992">dempro</ta>
            <ta e="T994" id="Seg_12689" s="T993">que</ta>
            <ta e="T995" id="Seg_12690" s="T994">que</ta>
            <ta e="T996" id="Seg_12691" s="T995">v</ta>
            <ta e="T997" id="Seg_12692" s="T996">pers</ta>
            <ta e="T999" id="Seg_12693" s="T998">n</ta>
            <ta e="T1000" id="Seg_12694" s="T999">adj</ta>
            <ta e="T1002" id="Seg_12695" s="T1001">dempro</ta>
            <ta e="T1003" id="Seg_12696" s="T1002">adv</ta>
            <ta e="T1004" id="Seg_12697" s="T1003">v</ta>
            <ta e="T1005" id="Seg_12698" s="T1004">aux</ta>
            <ta e="T1006" id="Seg_12699" s="T1005">interj</ta>
            <ta e="T1007" id="Seg_12700" s="T1006">adv</ta>
            <ta e="T1008" id="Seg_12701" s="T1007">ptcl</ta>
            <ta e="T1009" id="Seg_12702" s="T1008">adv</ta>
            <ta e="T1012" id="Seg_12703" s="T1011">v</ta>
            <ta e="T1013" id="Seg_12704" s="T1012">dempro</ta>
            <ta e="T1014" id="Seg_12705" s="T1013">post</ta>
            <ta e="T1015" id="Seg_12706" s="T1014">n</ta>
            <ta e="T1016" id="Seg_12707" s="T1015">ptcl</ta>
            <ta e="T1018" id="Seg_12708" s="T1017">interj</ta>
            <ta e="T1019" id="Seg_12709" s="T1018">pers</ta>
            <ta e="T1020" id="Seg_12710" s="T1019">ptcl</ta>
            <ta e="T1021" id="Seg_12711" s="T1020">ptcl</ta>
            <ta e="T1023" id="Seg_12712" s="T1022">adv</ta>
            <ta e="T1024" id="Seg_12713" s="T1023">ptcl</ta>
            <ta e="T1026" id="Seg_12714" s="T1025">interj</ta>
            <ta e="T1027" id="Seg_12715" s="T1026">pers</ta>
            <ta e="T1029" id="Seg_12716" s="T1028">v</ta>
            <ta e="T1031" id="Seg_12717" s="T1030">ptcl</ta>
            <ta e="T1032" id="Seg_12718" s="T1031">dempro</ta>
            <ta e="T1033" id="Seg_12719" s="T1032">v</ta>
            <ta e="T1034" id="Seg_12720" s="T1033">ptcl</ta>
            <ta e="T1035" id="Seg_12721" s="T1034">ptcl</ta>
            <ta e="T1036" id="Seg_12722" s="T1035">pers</ta>
            <ta e="T1037" id="Seg_12723" s="T1036">v</ta>
            <ta e="T1038" id="Seg_12724" s="T1037">aux</ta>
            <ta e="T1040" id="Seg_12725" s="T1039">adv</ta>
            <ta e="T1041" id="Seg_12726" s="T1040">ptcl</ta>
            <ta e="T1042" id="Seg_12727" s="T1041">v</ta>
            <ta e="T1043" id="Seg_12728" s="T1042">interj</ta>
            <ta e="T1044" id="Seg_12729" s="T1043">n</ta>
            <ta e="T1045" id="Seg_12730" s="T1044">ptcl</ta>
            <ta e="T1046" id="Seg_12731" s="T1045">v</ta>
            <ta e="T1047" id="Seg_12732" s="T1046">aux</ta>
            <ta e="T1049" id="Seg_12733" s="T1048">adv</ta>
            <ta e="T1050" id="Seg_12734" s="T1049">ptcl</ta>
            <ta e="T1051" id="Seg_12735" s="T1050">que</ta>
            <ta e="T1052" id="Seg_12736" s="T1051">ptcl</ta>
            <ta e="T1053" id="Seg_12737" s="T1052">ptcl</ta>
            <ta e="T1054" id="Seg_12738" s="T1053">n</ta>
            <ta e="T1055" id="Seg_12739" s="T1054">n</ta>
            <ta e="T1057" id="Seg_12740" s="T1056">dempro</ta>
            <ta e="T1058" id="Seg_12741" s="T1057">n</ta>
            <ta e="T1059" id="Seg_12742" s="T1058">v</ta>
            <ta e="T1060" id="Seg_12743" s="T1059">aux</ta>
            <ta e="T1062" id="Seg_12744" s="T1061">v</ta>
            <ta e="T1063" id="Seg_12745" s="T1062">v</ta>
            <ta e="T1065" id="Seg_12746" s="T1064">adj</ta>
            <ta e="T1066" id="Seg_12747" s="T1065">cop</ta>
            <ta e="T1067" id="Seg_12748" s="T1066">ptcl</ta>
            <ta e="T1068" id="Seg_12749" s="T1067">conj</ta>
            <ta e="T1069" id="Seg_12750" s="T1068">ptcl</ta>
            <ta e="T1070" id="Seg_12751" s="T1069">cop</ta>
            <ta e="T1071" id="Seg_12752" s="T1070">n</ta>
            <ta e="T1073" id="Seg_12753" s="T1072">ptcl</ta>
            <ta e="T1074" id="Seg_12754" s="T1073">dempro</ta>
            <ta e="T1075" id="Seg_12755" s="T1074">v</ta>
            <ta e="T1076" id="Seg_12756" s="T1075">n</ta>
            <ta e="T1077" id="Seg_12757" s="T1076">v</ta>
            <ta e="T1078" id="Seg_12758" s="T1077">n</ta>
            <ta e="T1079" id="Seg_12759" s="T1078">que</ta>
            <ta e="T1080" id="Seg_12760" s="T1079">v</ta>
            <ta e="T1081" id="Seg_12761" s="T1080">adv</ta>
            <ta e="T1082" id="Seg_12762" s="T1081">v</ta>
            <ta e="T1083" id="Seg_12763" s="T1082">que</ta>
            <ta e="T1084" id="Seg_12764" s="T1083">ptcl</ta>
            <ta e="T1086" id="Seg_12765" s="T1085">interj</ta>
            <ta e="T1087" id="Seg_12766" s="T1086">ptcl</ta>
            <ta e="T1088" id="Seg_12767" s="T1087">pers</ta>
            <ta e="T1089" id="Seg_12768" s="T1088">v</ta>
            <ta e="T1090" id="Seg_12769" s="T1089">v</ta>
            <ta e="T1092" id="Seg_12770" s="T1091">n</ta>
            <ta e="T1093" id="Seg_12771" s="T1092">v</ta>
            <ta e="T1094" id="Seg_12772" s="T1093">aux</ta>
            <ta e="T1095" id="Seg_12773" s="T1094">v</ta>
            <ta e="T1096" id="Seg_12774" s="T1095">aux</ta>
            <ta e="T1098" id="Seg_12775" s="T1097">ptcl</ta>
            <ta e="T1099" id="Seg_12776" s="T1098">que</ta>
            <ta e="T1100" id="Seg_12777" s="T1099">v</ta>
            <ta e="T1102" id="Seg_12778" s="T1101">conj</ta>
            <ta e="T1103" id="Seg_12779" s="T1102">n</ta>
            <ta e="T1104" id="Seg_12780" s="T1103">ptcl</ta>
            <ta e="T1105" id="Seg_12781" s="T1104">n</ta>
            <ta e="T1106" id="Seg_12782" s="T1105">v</ta>
            <ta e="T1107" id="Seg_12783" s="T1106">n</ta>
            <ta e="T1109" id="Seg_12784" s="T1108">adj</ta>
            <ta e="T1110" id="Seg_12785" s="T1109">conj</ta>
            <ta e="T1111" id="Seg_12786" s="T1110">adj</ta>
            <ta e="T1112" id="Seg_12787" s="T1111">n</ta>
            <ta e="T1113" id="Seg_12788" s="T1112">v</ta>
            <ta e="T1114" id="Seg_12789" s="T1113">aux</ta>
            <ta e="T1116" id="Seg_12790" s="T1115">dempro</ta>
            <ta e="T1117" id="Seg_12791" s="T1116">v</ta>
            <ta e="T1118" id="Seg_12792" s="T1117">v</ta>
            <ta e="T1119" id="Seg_12793" s="T1118">v</ta>
            <ta e="T1120" id="Seg_12794" s="T1119">v</ta>
            <ta e="T1122" id="Seg_12795" s="T1121">v</ta>
            <ta e="T1123" id="Seg_12796" s="T1122">v</ta>
            <ta e="T1124" id="Seg_12797" s="T1123">ptcl</ta>
            <ta e="T1126" id="Seg_12798" s="T1125">dempro</ta>
            <ta e="T1127" id="Seg_12799" s="T1126">ptcl</ta>
            <ta e="T1128" id="Seg_12800" s="T1127">v</ta>
            <ta e="T1129" id="Seg_12801" s="T1128">ptcl</ta>
            <ta e="T1130" id="Seg_12802" s="T1129">v</ta>
            <ta e="T1133" id="Seg_12803" s="T1132">n</ta>
            <ta e="T1134" id="Seg_12804" s="T1133">adv</ta>
            <ta e="T1136" id="Seg_12805" s="T1135">ptcl</ta>
            <ta e="T1137" id="Seg_12806" s="T1136">dempro</ta>
            <ta e="T1138" id="Seg_12807" s="T1137">dempro</ta>
            <ta e="T1139" id="Seg_12808" s="T1138">post</ta>
            <ta e="T1140" id="Seg_12809" s="T1139">v</ta>
            <ta e="T1141" id="Seg_12810" s="T1140">v</ta>
            <ta e="T1142" id="Seg_12811" s="T1141">aux</ta>
            <ta e="T1143" id="Seg_12812" s="T1142">v</ta>
            <ta e="T1144" id="Seg_12813" s="T1143">v</ta>
            <ta e="T1145" id="Seg_12814" s="T1144">v</ta>
            <ta e="T1146" id="Seg_12815" s="T1145">v</ta>
            <ta e="T1147" id="Seg_12816" s="T1146">aux</ta>
            <ta e="T1148" id="Seg_12817" s="T1147">pers</ta>
            <ta e="T1149" id="Seg_12818" s="T1148">post</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_12819" s="T6">RUS:cult</ta>
            <ta e="T8" id="Seg_12820" s="T7">RUS:cult</ta>
            <ta e="T11" id="Seg_12821" s="T10">RUS:cult</ta>
            <ta e="T46" id="Seg_12822" s="T45">RUS:cult</ta>
            <ta e="T47" id="Seg_12823" s="T46">RUS:cult</ta>
            <ta e="T78" id="Seg_12824" s="T77">EV:cult</ta>
            <ta e="T85" id="Seg_12825" s="T84">RUS:cult</ta>
            <ta e="T90" id="Seg_12826" s="T89">RUS:cult</ta>
            <ta e="T114" id="Seg_12827" s="T113">RUS:cult</ta>
            <ta e="T116" id="Seg_12828" s="T115">RUS:cult</ta>
            <ta e="T117" id="Seg_12829" s="T116">RUS:cult</ta>
            <ta e="T130" id="Seg_12830" s="T129">RUS:cult</ta>
            <ta e="T133" id="Seg_12831" s="T132">RUS:cult</ta>
            <ta e="T201" id="Seg_12832" s="T200">EV:cult</ta>
            <ta e="T202" id="Seg_12833" s="T201">RUS:cult</ta>
            <ta e="T219" id="Seg_12834" s="T218">RUS:cult</ta>
            <ta e="T227" id="Seg_12835" s="T226">RUS:cult</ta>
            <ta e="T231" id="Seg_12836" s="T230">RUS:cult</ta>
            <ta e="T235" id="Seg_12837" s="T234">RUS:cult</ta>
            <ta e="T261" id="Seg_12838" s="T260">RUS:cult</ta>
            <ta e="T284" id="Seg_12839" s="T283">RUS:cult</ta>
            <ta e="T299" id="Seg_12840" s="T298">RUS:cult</ta>
            <ta e="T320" id="Seg_12841" s="T319">EV:core</ta>
            <ta e="T341" id="Seg_12842" s="T340">EV:core</ta>
            <ta e="T343" id="Seg_12843" s="T342">RUS:core</ta>
            <ta e="T394" id="Seg_12844" s="T393">EV:gram (DIM)</ta>
            <ta e="T409" id="Seg_12845" s="T408">EV:gram (DIM)</ta>
            <ta e="T425" id="Seg_12846" s="T424">RUS:gram</ta>
            <ta e="T433" id="Seg_12847" s="T432">RUS:cult</ta>
            <ta e="T434" id="Seg_12848" s="T433">RUS:cult</ta>
            <ta e="T456" id="Seg_12849" s="T455">RUS:cult</ta>
            <ta e="T497" id="Seg_12850" s="T496">RUS:cult</ta>
            <ta e="T520" id="Seg_12851" s="T519">RUS:cult</ta>
            <ta e="T537" id="Seg_12852" s="T536">RUS:gram</ta>
            <ta e="T564" id="Seg_12853" s="T563">RUS:core</ta>
            <ta e="T577" id="Seg_12854" s="T576">RUS:mod</ta>
            <ta e="T585" id="Seg_12855" s="T584">RUS:core</ta>
            <ta e="T586" id="Seg_12856" s="T585">RUS:gram</ta>
            <ta e="T588" id="Seg_12857" s="T587">RUS:cult</ta>
            <ta e="T591" id="Seg_12858" s="T590">RUS:cult</ta>
            <ta e="T604" id="Seg_12859" s="T603">RUS:cult</ta>
            <ta e="T606" id="Seg_12860" s="T605">RUS:cult</ta>
            <ta e="T656" id="Seg_12861" s="T655">RUS:core</ta>
            <ta e="T664" id="Seg_12862" s="T663">EV:gram (DIM)</ta>
            <ta e="T669" id="Seg_12863" s="T668">RUS:core</ta>
            <ta e="T671" id="Seg_12864" s="T670">RUS:gram</ta>
            <ta e="T695" id="Seg_12865" s="T694">RUS:cult</ta>
            <ta e="T706" id="Seg_12866" s="T705">RUS:cult</ta>
            <ta e="T719" id="Seg_12867" s="T718">RUS:cult</ta>
            <ta e="T760" id="Seg_12868" s="T759">EV:gram (DIM)</ta>
            <ta e="T761" id="Seg_12869" s="T760">RUS:cult</ta>
            <ta e="T784" id="Seg_12870" s="T783">RUS:cult</ta>
            <ta e="T794" id="Seg_12871" s="T793">RUS:core</ta>
            <ta e="T820" id="Seg_12872" s="T819">RUS:core</ta>
            <ta e="T836" id="Seg_12873" s="T835">RUS:core</ta>
            <ta e="T841" id="Seg_12874" s="T840">RUS:mod</ta>
            <ta e="T846" id="Seg_12875" s="T845">RUS:core</ta>
            <ta e="T852" id="Seg_12876" s="T851">RUS:core</ta>
            <ta e="T897" id="Seg_12877" s="T896">RUS:cult</ta>
            <ta e="T900" id="Seg_12878" s="T899">RUS:gram</ta>
            <ta e="T901" id="Seg_12879" s="T900">RUS:cult</ta>
            <ta e="T918" id="Seg_12880" s="T917">RUS:cult</ta>
            <ta e="T930" id="Seg_12881" s="T929">RUS:cult</ta>
            <ta e="T940" id="Seg_12882" s="T939">RUS:cult</ta>
            <ta e="T945" id="Seg_12883" s="T944">RUS:core</ta>
            <ta e="T947" id="Seg_12884" s="T946">RUS:core</ta>
            <ta e="T951" id="Seg_12885" s="T950">RUS:cult</ta>
            <ta e="T953" id="Seg_12886" s="T952">RUS:core</ta>
            <ta e="T956" id="Seg_12887" s="T955">RUS:cult</ta>
            <ta e="T961" id="Seg_12888" s="T960">RUS:cult</ta>
            <ta e="T965" id="Seg_12889" s="T964">RUS:core</ta>
            <ta e="T968" id="Seg_12890" s="T967">RUS:core</ta>
            <ta e="T999" id="Seg_12891" s="T998">RUS:cultEV:gram (DIM)</ta>
            <ta e="T1003" id="Seg_12892" s="T1002">RUS:core</ta>
            <ta e="T1007" id="Seg_12893" s="T1006">RUS:core</ta>
            <ta e="T1023" id="Seg_12894" s="T1022">RUS:core</ta>
            <ta e="T1041" id="Seg_12895" s="T1040">RUS:mod</ta>
            <ta e="T1044" id="Seg_12896" s="T1043">RUS:cult</ta>
            <ta e="T1049" id="Seg_12897" s="T1048">RUS:core</ta>
            <ta e="T1067" id="Seg_12898" s="T1066">RUS:disc</ta>
            <ta e="T1102" id="Seg_12899" s="T1101">RUS:gram</ta>
            <ta e="T1107" id="Seg_12900" s="T1106">RUS:cult</ta>
            <ta e="T1110" id="Seg_12901" s="T1109">RUS:gram</ta>
            <ta e="T1112" id="Seg_12902" s="T1111">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T11" id="Seg_12903" s="T1">Once I remember, when I was little, at Nahong, our hunting hunting spot twelve kilometres from Kresty.</ta>
            <ta e="T15" id="Seg_12904" s="T11">There lived many people.</ta>
            <ta e="T21" id="Seg_12905" s="T16">Old people, everybody lived there.</ta>
            <ta e="T27" id="Seg_12906" s="T22">There were many baloks there.</ta>
            <ta e="T32" id="Seg_12907" s="T28">There were many reindeer and so on.</ta>
            <ta e="T52" id="Seg_12908" s="T33">Then once my father… At our house I called my grandfather father, I said father, and I called my grandmother mother, I was raised at theirs.</ta>
            <ta e="T60" id="Seg_12909" s="T53">Well our father had gone somewhere.</ta>
            <ta e="T72" id="Seg_12910" s="T61">He probably went to get wood, we sit [at home] and the reindeer walk around right near the house.</ta>
            <ta e="T76" id="Seg_12911" s="T73">They walk around scratching with their hooves.</ta>
            <ta e="T79" id="Seg_12912" s="T77">We had a tame reindeer.</ta>
            <ta e="T83" id="Seg_12913" s="T80">That one eats out of the hand.</ta>
            <ta e="T86" id="Seg_12914" s="T84">It likes bread very much.</ta>
            <ta e="T91" id="Seg_12915" s="T87">It comes to the window looking for bread. </ta>
            <ta e="T105" id="Seg_12916" s="T92">Oh, it came bringing its nose close, oh, I am afraid, I was little and was always afraid.</ta>
            <ta e="T112" id="Seg_12917" s="T106">Uuh I secretly opened it.</ta>
            <ta e="T118" id="Seg_12918" s="T113">From the table it (took) bread, flat bread and so on.</ta>
            <ta e="T126" id="Seg_12919" s="T119">Oh, it got excited, it gobbles up (the food), oh dear!</ta>
            <ta e="T134" id="Seg_12920" s="T127">Well mother says, my old mother: </ta>
            <ta e="T142" id="Seg_12921" s="T135">"Oh, don't teach him, it will break out window and so on!</ta>
            <ta e="T152" id="Seg_12922" s="T143">Go, go, go, don't come here anymore, there is nothing!</ta>
            <ta e="T160" id="Seg_12923" s="T153">Go eat your grass", she said, and closed [the window].</ta>
            <ta e="T169" id="Seg_12924" s="T161">Then it comes via the other window, there was the detachable front [of the balok].</ta>
            <ta e="T174" id="Seg_12925" s="T170">There we always put the ice.</ta>
            <ta e="T180" id="Seg_12926" s="T175">Well, our water, our ice.</ta>
            <ta e="T190" id="Seg_12927" s="T181">It climbed up there, our window stands open, the front window.</ta>
            <ta e="T196" id="Seg_12928" s="T191">There it [stands] with its long curious face and its antlers.</ta>
            <ta e="T208" id="Seg_12929" s="T197">The antlers don't fit, in the place where we keep our crockery was flat bread, flat breads that were cut.</ta>
            <ta e="T217" id="Seg_12930" s="T209">It tried to get it, stretching out its tongue.</ta>
            <ta e="T231" id="Seg_12931" s="T218">"Mother look, look look", I said, "it is going to eat whatever, it is going to eat the breads, the breads!"</ta>
            <ta e="T243" id="Seg_12932" s="T232">"Oh, why, bad girl, did you teach it, to feed from the house, to feed from the window.</ta>
            <ta e="T249" id="Seg_12933" s="T244">Look, soon it wil steal everything", she says.</ta>
            <ta e="T253" id="Seg_12934" s="T250">Oh dear, [its so funny] my stomach hurts.</ta>
            <ta e="T257" id="Seg_12935" s="T254">Uuuh, go, go.</ta>
            <ta e="T263" id="Seg_12936" s="T258">Somehow with a cloth - lash lash.</ta>
            <ta e="T266" id="Seg_12937" s="T264">And then it doesn't go away.</ta>
            <ta e="T271" id="Seg_12938" s="T267">Well, that's enough, go.</ta>
            <ta e="T275" id="Seg_12939" s="T272">And eventually gives him [bread].</ta>
            <ta e="T293" id="Seg_12940" s="T276">Then it luckily goes away munching the bread, it probably had enough, how can it have enough? It crunches it.</ta>
            <ta e="T299" id="Seg_12941" s="T294">It turns out she gave it what, dried bread.</ta>
            <ta e="T305" id="Seg_12942" s="T300">And it tries to chew it.</ta>
            <ta e="T308" id="Seg_12943" s="T306">Oh dear, it's so funny.</ta>
            <ta e="T314" id="Seg_12944" s="T309">Oh, it probably whatchammacallit its teeth.</ta>
            <ta e="T321" id="Seg_12945" s="T315">Well then it went to its friends, luckily.</ta>
            <ta e="T328" id="Seg_12946" s="T322">They came down the high shore and they go to the river.</ta>
            <ta e="T334" id="Seg_12947" s="T329">Outside it was getting dusky, it was getting dark.</ta>
            <ta e="T347" id="Seg_12948" s="T335">We had a dog, Djongo, a very white, very beautiful, very good herding dog it was.</ta>
            <ta e="T357" id="Seg_12949" s="T348">You just say hush and it will bring the whole herd.</ta>
            <ta e="T365" id="Seg_12950" s="T358">Oh it will run around, then it will stand still on one spot.</ta>
            <ta e="T370" id="Seg_12951" s="T366">When one says "bring here!"</ta>
            <ta e="T382" id="Seg_12952" s="T371">It did something, when the reindeer were home recently.</ta>
            <ta e="T397" id="Seg_12953" s="T383">A reindeer calf, a one year old reindeer or something, a one year old reindeer, ehm, one year older than a calf.</ta>
            <ta e="T413" id="Seg_12954" s="T398">It begins to run around, begins to irritate the reindeer calf, moving and moving around.</ta>
            <ta e="T422" id="Seg_12955" s="T414">Lying down it plays with it.</ta>
            <ta e="T432" id="Seg_12956" s="T422">And then how did it go? It hits it [with its hoof], it does like that.</ta>
            <ta e="T438" id="Seg_12957" s="T432">"Mommy, mommy", I say, "look, look at Djongo!"</ta>
            <ta e="T442" id="Seg_12958" s="T439">We tended to call it Choko.</ta>
            <ta e="T445" id="Seg_12959" s="T443">"Look at Choko!</ta>
            <ta e="T453" id="Seg_12960" s="T446">It wants to eat the calf", I say.</ta>
            <ta e="T458" id="Seg_12961" s="T454">"Oh devil, call it, call it!"</ta>
            <ta e="T464" id="Seg_12962" s="T459">It had exhausted the reindeer calf probably, it had scared it.</ta>
            <ta e="T473" id="Seg_12963" s="T465">"Choko, Choko, Choko", I say, after having gone outside.</ta>
            <ta e="T480" id="Seg_12964" s="T474">Oh, it came dashing (towards me).</ta>
            <ta e="T488" id="Seg_12965" s="T481">It jumped at my face and I [fell] on my back.</ta>
            <ta e="T493" id="Seg_12966" s="T489">I lie on my back.</ta>
            <ta e="T500" id="Seg_12967" s="T494">"Yeah", I say, "mommy take him!"</ta>
            <ta e="T508" id="Seg_12968" s="T501">It licks me and so on, and plays with me.</ta>
            <ta e="T511" id="Seg_12969" s="T509">It wants to play.</ta>
            <ta e="T516" id="Seg_12970" s="T512">It begins to drag me everywhere.</ta>
            <ta e="T521" id="Seg_12971" s="T517">My mother came outside.</ta>
            <ta e="T525" id="Seg_12972" s="T522">What are you doing?</ta>
            <ta e="T537" id="Seg_12973" s="T526">"Look", I say, "it beats me (?)", I say, "it plays with me, oh yes!"</ta>
            <ta e="T542" id="Seg_12974" s="T538">I am crying loudly.</ta>
            <ta e="T545" id="Seg_12975" s="T543">"Don't cry.</ta>
            <ta e="T551" id="Seg_12976" s="T546">You called it yourself", she said, "it was funny."</ta>
            <ta e="T558" id="Seg_12977" s="T552">Then it went again to that calf.</ta>
            <ta e="T577" id="Seg_12978" s="T559">Otherwise I didn't do anything, it played every day with that calf, why it wanted especially that calf, why did it have to be that calf?</ta>
            <ta e="T583" id="Seg_12979" s="T578">Well than it became dark outside.</ta>
            <ta e="T589" id="Seg_12980" s="T584">Our stove was very beautiful.</ta>
            <ta e="T593" id="Seg_12981" s="T590">My mother had heated well.</ta>
            <ta e="T604" id="Seg_12982" s="T594">Ps, ps, ps the wood flames up, our stove becomes red.</ta>
            <ta e="T611" id="Seg_12983" s="T605">The tea pots began to boil, the lids began to rattle.</ta>
            <ta e="T618" id="Seg_12984" s="T612">Our food began [to cook].</ta>
            <ta e="T625" id="Seg_12985" s="T619">It became warm, she opened the door, the window, she opened the smoke hole.</ta>
            <ta e="T636" id="Seg_12986" s="T626">There is a smoke hole, so the wind goes in.</ta>
            <ta e="T641" id="Seg_12987" s="T637">She opened it.</ta>
            <ta e="T650" id="Seg_12988" s="T642">I very much loved looking at the window.</ta>
            <ta e="T654" id="Seg_12989" s="T651">I used to sit with my face [close to the window].</ta>
            <ta e="T661" id="Seg_12990" s="T655">Beautiful is the whatchamacallit, the moon hahaha.</ta>
            <ta e="T666" id="Seg_12991" s="T662">The bloody moon!</ta>
            <ta e="T672" id="Seg_12992" s="T667">The moon is very beautiful, but pitch black.</ta>
            <ta e="T680" id="Seg_12993" s="T673">Something was flying, I didn't know what.</ta>
            <ta e="T687" id="Seg_12994" s="T681">Something like a snow owl, you wouldn't say snow owls, they are smaller.</ta>
            <ta e="T696" id="Seg_12995" s="T688">They don't have a head, I would say, they fly without a head, mother, look!</ta>
            <ta e="T707" id="Seg_12996" s="T697">It turns out cole is flying from the chimney.</ta>
            <ta e="T714" id="Seg_12997" s="T708">There they are moving around, very interesting.</ta>
            <ta e="T723" id="Seg_12998" s="T715">Then after some time, my mother was sewing, she was sewing.</ta>
            <ta e="T731" id="Seg_12999" s="T724">Quietly, quietly, she sits singing quietly.</ta>
            <ta e="T734" id="Seg_13000" s="T732">Our Djaku is lying.</ta>
            <ta e="T738" id="Seg_13001" s="T735">It doesn't make the slightest sound. </ta>
            <ta e="T743" id="Seg_13002" s="T739">It probably got tired with the calf.</ta>
            <ta e="T750" id="Seg_13003" s="T744">Then time passed.</ta>
            <ta e="T762" id="Seg_13004" s="T751">As lons as my father isn't there, I switch on the radio.</ta>
            <ta e="T766" id="Seg_13005" s="T763">Yeah, I switched it on full power.</ta>
            <ta e="T772" id="Seg_13006" s="T767">"Do quiet, don't make noise."</ta>
            <ta e="T777" id="Seg_13007" s="T773">I lay there listening to songs.</ta>
            <ta e="T786" id="Seg_13008" s="T778">Heeehoo, I listen to Chinese or something, it turns out.</ta>
            <ta e="T796" id="Seg_13009" s="T787">I listened to them, it was much fun, I know.</ta>
            <ta e="T803" id="Seg_13010" s="T796">Then I luckily found Russians and then it ended.</ta>
            <ta e="T811" id="Seg_13011" s="T804">"Well make it end, end it, your father is coming!"</ta>
            <ta e="T820" id="Seg_13012" s="T812">The sun, the moon had moved high, very beautiful.</ta>
            <ta e="T825" id="Seg_13013" s="T821">Very light like in the day.</ta>
            <ta e="T837" id="Seg_13014" s="T826">Oh our reindeer galop along the river, very beautiful.</ta>
            <ta e="T842" id="Seg_13015" s="T838">Even smoke comes from them.</ta>
            <ta e="T855" id="Seg_13016" s="T843">Oh very beautiful, that snow in the whatever, shines from the moonlight.</ta>
            <ta e="T866" id="Seg_13017" s="T856">Then, wait, our dog does "hm hm", it whines.</ta>
            <ta e="T872" id="Seg_13018" s="T867">Wait, a bell was tinckling, a neck bell.</ta>
            <ta e="T880" id="Seg_13019" s="T873">Ting, ting, ting, our father is coming, our father, yeah.</ta>
            <ta e="T884" id="Seg_13020" s="T881">Well he came.</ta>
            <ta e="T902" id="Seg_13021" s="T885">Well I didn't know, I think he went to get wood, but it appears he went to Dudinka.</ta>
            <ta e="T906" id="Seg_13022" s="T903">Well he came.</ta>
            <ta e="T911" id="Seg_13023" s="T907">Already after it had gotten dark.</ta>
            <ta e="T915" id="Seg_13024" s="T912">Oooh my father has arrived!</ta>
            <ta e="T923" id="Seg_13025" s="T916">Oooh my mother went to help him untie from the neck bells and so on.</ta>
            <ta e="T926" id="Seg_13026" s="T923">They let the reindeer go and so on.</ta>
            <ta e="T931" id="Seg_13027" s="T927">Oooh my father came with bags.</ta>
            <ta e="T934" id="Seg_13028" s="T932">He brought in the bags.</ta>
            <ta e="T937" id="Seg_13029" s="T935">He started to unpack them.</ta>
            <ta e="T942" id="Seg_13030" s="T937">Oh he brought so many sweets.</ta>
            <ta e="T948" id="Seg_13031" s="T943">Then something beautiful I always remember.</ta>
            <ta e="T957" id="Seg_13032" s="T949">Was it a clock or something, very beautiful it was made of a mirror.</ta>
            <ta e="T968" id="Seg_13033" s="T958">It had even buttons, so beautiful, I was surprised, so beautiful.</ta>
            <ta e="T974" id="Seg_13034" s="T969">"Well don't touch, it's enough", they say.</ta>
            <ta e="T978" id="Seg_13035" s="T975">They didn't let me touch it.</ta>
            <ta e="T982" id="Seg_13036" s="T979">"You might break it!"</ta>
            <ta e="T991" id="Seg_13037" s="T983">I put it up, I very much wanted to look at it, wanted to touch it.</ta>
            <ta e="T1000" id="Seg_13038" s="T992">Then he brought me what, a present, a little compas.</ta>
            <ta e="T1016" id="Seg_13039" s="T1001">I always played with it, very beautiful, long ago he brought me for the first time such a toy.</ta>
            <ta e="T1024" id="Seg_13040" s="T1017">Oh I [found] it so beautiful.</ta>
            <ta e="T1029" id="Seg_13041" s="T1025">"Oh here is a present for you", he says.</ta>
            <ta e="T1038" id="Seg_13042" s="T1030">Well they chatted and I fell asleep.</ta>
            <ta e="T1047" id="Seg_13043" s="T1039">In the morning I wake up, oh, the fire is already burning vigorously.</ta>
            <ta e="T1055" id="Seg_13044" s="T1048">Very beautiful and in the house there is nobody.</ta>
            <ta e="T1060" id="Seg_13045" s="T1056">They were catching the reindeer.</ta>
            <ta e="T1063" id="Seg_13046" s="T1061">They went out to catch them.</ta>
            <ta e="T1071" id="Seg_13047" s="T1064">We had sometimes neighbours, our neighbours were there.</ta>
            <ta e="T1084" id="Seg_13048" s="T1072">They gather the herd, gather the reindeer, we migrate probably somewhere, I remember.</ta>
            <ta e="T1090" id="Seg_13049" s="T1085">Oh they hurried to dress me quickly.</ta>
            <ta e="T1096" id="Seg_13050" s="T1091">They tied me to the sleigh.</ta>
            <ta e="T1100" id="Seg_13051" s="T1097">Well, where are we going?</ta>
            <ta e="T1107" id="Seg_13052" s="T1101">And on the back side they tied the reindeer to the cargo sleigh.</ta>
            <ta e="T1114" id="Seg_13053" s="T1108">We would transport a cargo sleigh with food or with wood.</ta>
            <ta e="T1120" id="Seg_13054" s="T1115">Frightened by them I began to cry loudly.</ta>
            <ta e="T1124" id="Seg_13055" s="T1121">"Don't be afraind, it won't bite!"</ta>
            <ta e="T1134" id="Seg_13056" s="T1125">Having gotten tired, they breathe right in the face, here.</ta>
            <ta e="T1149" id="Seg_13057" s="T1135">That's how we used to migrate, how I used to migrate with them.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T11" id="Seg_13058" s="T1">Ich erinnere einmal, als ich klein war, in Nahong, unsere Jagdstelle zwölf Kilometer von Kresty.</ta>
            <ta e="T15" id="Seg_13059" s="T11">Dort lebten viele Menschen.</ta>
            <ta e="T21" id="Seg_13060" s="T16">Alte Leute, alle lebten dort.</ta>
            <ta e="T27" id="Seg_13061" s="T22">Es war voll von Baloks dort.</ta>
            <ta e="T32" id="Seg_13062" s="T28">Rentiere und so weiter waren [da].</ta>
            <ta e="T52" id="Seg_13063" s="T33">Dann einmal mein Vater… Zuhause nannte ich meinen Großvater "Vater", ich sagte "Vater", und ich nannte meine Großmutter "Mutter", ich wurde bei ihnen großgezogen.</ta>
            <ta e="T60" id="Seg_13064" s="T53">Nun, unser Vater ging irgendwohin.</ta>
            <ta e="T72" id="Seg_13065" s="T61">Er ging wahrscheinlich Holz holen, wir sitzen [zuhause] und die Rentiere laufen direkt um unser Haus herum.</ta>
            <ta e="T76" id="Seg_13066" s="T73">Sie laufen und scharren mit ihren Hufen.</ta>
            <ta e="T79" id="Seg_13067" s="T77">Wir hatten ein gezähmtes Rentier.</ta>
            <ta e="T83" id="Seg_13068" s="T80">Das frisst aus der Hand.</ta>
            <ta e="T86" id="Seg_13069" s="T84">Es mag Brot sehr gerne.</ta>
            <ta e="T91" id="Seg_13070" s="T87">Es kommt ans Fenster, um nach Brot zu suchen.</ta>
            <ta e="T105" id="Seg_13071" s="T92">Oh, seine Nase kam und war groß und nahe, oh, ich erschrak, ich war klein und hatte immer Angst.</ta>
            <ta e="T112" id="Seg_13072" s="T106">Uh, ich öffnete es heimlich.</ta>
            <ta e="T118" id="Seg_13073" s="T113">Vom Tisch nahm es Brot und Brotfladen.</ta>
            <ta e="T126" id="Seg_13074" s="T119">Oh, es war aufgeregt, es schlingt es hinunter, oh man!</ta>
            <ta e="T134" id="Seg_13075" s="T127">Nun, meine Mutter sagt, meine alte Mutter:</ta>
            <ta e="T142" id="Seg_13076" s="T135">"Oh, bring ihm das nicht bei, es macht unser Fenster kaputt und so!</ta>
            <ta e="T152" id="Seg_13077" s="T143">Geh, geh, geh, komm nicht mehr, sagt sie, [da ist] nichts!</ta>
            <ta e="T160" id="Seg_13078" s="T153">Geh und friss dein Gras", sagte sie und schloss [das Fenster].</ta>
            <ta e="T169" id="Seg_13079" s="T161">Dann kommt es durch ein anderes Fenster, da war so eine abtrennbare Front [am Balok].</ta>
            <ta e="T174" id="Seg_13080" s="T170">Dort legen wir immer Eis hin.</ta>
            <ta e="T180" id="Seg_13081" s="T175">Nun, unser Wasser, unser Eis.</ta>
            <ta e="T190" id="Seg_13082" s="T181">Es kletterte da hoch, unser Fenster steht offen, unser Vorderes.</ta>
            <ta e="T196" id="Seg_13083" s="T191">Dort [steht] es mit langem neugierigem Gesicht und seinem Geweih.</ta>
            <ta e="T208" id="Seg_13084" s="T197">Das Geweih passt nicht hinein, am Platz für Geschirr waren Brotfladen, solche die geschnitten waren.</ta>
            <ta e="T217" id="Seg_13085" s="T209">Es versuchte sie zu erreichen, indem es seine Zunge ausstreckte.</ta>
            <ta e="T231" id="Seg_13086" s="T218">"Mama, schau, schau, schau", sage ich, "es frisst das Dings, das Brot, es frisst das Brot."</ta>
            <ta e="T243" id="Seg_13087" s="T232">"Oh, warum, verfluchtes Mädchen, hast du ihm beigebracht, dass es am Haus gefüttert wird, dass es am Fenster gefüttert wird.</ta>
            <ta e="T249" id="Seg_13088" s="T244">Schau, jetzt wird es alles klauen", sagt sie.</ta>
            <ta e="T253" id="Seg_13089" s="T250">Oh man, [es ist so lustig] dass mein Bauch wehtut.</ta>
            <ta e="T257" id="Seg_13090" s="T254">Uh, geh, geh.</ta>
            <ta e="T263" id="Seg_13091" s="T258">Mit irgendeinem Tuch, ksch-ksch.</ta>
            <ta e="T266" id="Seg_13092" s="T264">Und dann geht es nicht.</ta>
            <ta e="T271" id="Seg_13093" s="T267">Nun, das reicht, geh.</ta>
            <ta e="T275" id="Seg_13094" s="T272">Und schließlich gibt sie ihm [Brot].</ta>
            <ta e="T293" id="Seg_13095" s="T276">Dann geht es zum Glück weg und kaut das Brot, es war wahrscheinlich satt, wie kann es satt sein, es zermalmt es.</ta>
            <ta e="T299" id="Seg_13096" s="T294">Offenbar hatte sie ihm Dings, getrocknetes Brot gegeben.</ta>
            <ta e="T305" id="Seg_13097" s="T300">Und es versuchte es zu kauen.</ta>
            <ta e="T308" id="Seg_13098" s="T306">Oh nein, so lustig.</ta>
            <ta e="T314" id="Seg_13099" s="T309">Oh, es dingste wahrscheinlich seine ganzen Zähne.</ta>
            <ta e="T321" id="Seg_13100" s="T315">Nun, dann ging es zum Glück zu seinen Freunden.</ta>
            <ta e="T328" id="Seg_13101" s="T322">Sie kamen das hohe Ufer hinunter und sie gehen zum Fluss.</ta>
            <ta e="T334" id="Seg_13102" s="T329">Draußen wurde es dunkel, es wurde dunkel.</ta>
            <ta e="T347" id="Seg_13103" s="T335">Wir hatten einen Hund, Djongo, er war ganz weiß, sehr schön und ein sehr gute Rentierhund.</ta>
            <ta e="T357" id="Seg_13104" s="T348">Du sagt nur "husch" und er bringt die ganze Herde.</ta>
            <ta e="T365" id="Seg_13105" s="T358">Oh, er dreht sich um, dann steht er an einem Ort.</ta>
            <ta e="T370" id="Seg_13106" s="T366">Wenn man sagt "bring sie her!"</ta>
            <ta e="T382" id="Seg_13107" s="T371">Der hatte etwas gemacht, als die Rentiere zuhause waren.</ta>
            <ta e="T397" id="Seg_13108" s="T383">Ein Rentierkalb, ein einjähriges Rentier oder so, ein einjähriges Rentier, äh, ein Jahr älter als ein Rentierkalb.</ta>
            <ta e="T413" id="Seg_13109" s="T398">Er fing an sich umzudrehen, fing an das Rentierkalb zu irritieren und bewegte sich und bewegte sich.</ta>
            <ta e="T422" id="Seg_13110" s="T414">Er legt sich hin und spielt mit ihm.</ta>
            <ta e="T432" id="Seg_13111" s="T422">Und was passierte dann, es trat ihn [mit seinem Huf], so macht es.</ta>
            <ta e="T438" id="Seg_13112" s="T432">"Mama, Mama", sage ich, "guck, guck, Djongo!"</ta>
            <ta e="T442" id="Seg_13113" s="T439">Wir nennen ihn Tschoko.</ta>
            <ta e="T445" id="Seg_13114" s="T443">"Schau Tschoko an!</ta>
            <ta e="T453" id="Seg_13115" s="T446">Er will das Dings fressen", sage ich, "er will das Kalb fressen."</ta>
            <ta e="T458" id="Seg_13116" s="T454">"Oh Mist, ruf ihn, ruf ihn!"</ta>
            <ta e="T464" id="Seg_13117" s="T459">Er hatte das Rentierkalb offenbar ermüdet, es hatte es erschreckt.</ta>
            <ta e="T473" id="Seg_13118" s="T465">"Tschoko, Tschoko, Tschoko", sage ich, nachdem ich rausgegangen bin.</ta>
            <ta e="T480" id="Seg_13119" s="T474">Oh, er kommt angerannt.</ta>
            <ta e="T488" id="Seg_13120" s="T481">Er sprang mir ins Gesicht und ich [fiel] auf den Rücken.</ta>
            <ta e="T493" id="Seg_13121" s="T489">Ich liege so auf dem Rücken.</ta>
            <ta e="T500" id="Seg_13122" s="T494">"Hej", sage ich, "Mama, nimm ihn!"</ta>
            <ta e="T508" id="Seg_13123" s="T501">Er leckt mich ab und so, er spielt mit mir.</ta>
            <ta e="T511" id="Seg_13124" s="T509">Er möchte spielen.</ta>
            <ta e="T516" id="Seg_13125" s="T512">Er fängt an mich umherzuziehen.</ta>
            <ta e="T521" id="Seg_13126" s="T517">Nun, meine Mama kam heraus.</ta>
            <ta e="T525" id="Seg_13127" s="T522">Was ist mit dir?</ta>
            <ta e="T537" id="Seg_13128" s="T526">"Schau", sage ich, "er schlägt mich (?)", sage ich, "er spielt mit mir, ja!"</ta>
            <ta e="T542" id="Seg_13129" s="T538">Ich schreie laut.</ta>
            <ta e="T545" id="Seg_13130" s="T543">"Wein nicht.</ta>
            <ta e="T551" id="Seg_13131" s="T546">Du hast ihn selbst gerufen", sagt sie, "es ist lustig."</ta>
            <ta e="T558" id="Seg_13132" s="T552">Dann ging er wieder zum Kalb.</ta>
            <ta e="T577" id="Seg_13133" s="T559">Sonst habe ich nichts gemacht, er spielte jedem Tag mit diesem Kalb, warum wollte er, dass es genau dieses Kalb ist?</ta>
            <ta e="T583" id="Seg_13134" s="T578">Nun, dann wurde es dunkel draußen.</ta>
            <ta e="T589" id="Seg_13135" s="T584">Unser Dings, unser Ofen war sehr schön.</ta>
            <ta e="T593" id="Seg_13136" s="T590">Meine Mutter hatte gut geheizt.</ta>
            <ta e="T604" id="Seg_13137" s="T594">Ps, ps, ps das Holz flammt auf, unser Ofen wird rot.</ta>
            <ta e="T611" id="Seg_13138" s="T605">Die Teekannen fingen an sich zu erheben, die Deckel fingen an zu klappern.</ta>
            <ta e="T618" id="Seg_13139" s="T612">Unser Essen fing an [zu kochen].</ta>
            <ta e="T625" id="Seg_13140" s="T619">Es wurde warm, sie öffnete Tür und Fenster, sie öffnete das Rauchloch.</ta>
            <ta e="T636" id="Seg_13141" s="T626">Es gibt ein Rauchloch, das bedeckt Dings, damit der Wind hineinkommt.</ta>
            <ta e="T641" id="Seg_13142" s="T637">Das öffnete sie, ja.</ta>
            <ta e="T650" id="Seg_13143" s="T642">Ich mochte es sehr aus dem Fenster zu schauen.</ta>
            <ta e="T654" id="Seg_13144" s="T651">Ich saß immer mit meinem breiten Gesicht [da].</ta>
            <ta e="T661" id="Seg_13145" s="T655">Sehr schön ist Dings, der Mond, hahaha.</ta>
            <ta e="T666" id="Seg_13146" s="T662">Das verdammt Mond!</ta>
            <ta e="T672" id="Seg_13147" s="T667">Der Mond ist sehr schön, aber ganz schwarz.</ta>
            <ta e="T680" id="Seg_13148" s="T673">Irgendetwas flog, ich kannte es nicht.</ta>
            <ta e="T687" id="Seg_13149" s="T681">Wie eine Schneeeule, du sagst nicht Schneeeule, die sind kleiner.</ta>
            <ta e="T696" id="Seg_13150" s="T688">Die haben keinen Kopf, sage ich, die fliegen ohne Kopf, Mama, schau!</ta>
            <ta e="T707" id="Seg_13151" s="T697">Es stellt sich heraus, dass Kohle aus dem Schornstein fliegt.</ta>
            <ta e="T714" id="Seg_13152" s="T708">Dort bewegen sie sich offenbar, sehr interessant.</ta>
            <ta e="T723" id="Seg_13153" s="T715">Dann nach einiger Zeit sitzt meine Mutter und näht.</ta>
            <ta e="T731" id="Seg_13154" s="T724">Ganz leise, sie sitzt und singt ganz leise.</ta>
            <ta e="T734" id="Seg_13155" s="T732">Unser Djaku liegt da.</ta>
            <ta e="T738" id="Seg_13156" s="T735">Er macht kein Geräusch. </ta>
            <ta e="T743" id="Seg_13157" s="T739">Er war offenbar mit dem Kalb müde geworden.</ta>
            <ta e="T750" id="Seg_13158" s="T744">Dann Dings, die Zeit verging.</ta>
            <ta e="T762" id="Seg_13159" s="T751">Solange mein Vater noch nicht da ist, mache ich das Radio an.</ta>
            <ta e="T766" id="Seg_13160" s="T763">Ja, ich drehe es ganz auf.</ta>
            <ta e="T772" id="Seg_13161" s="T767">"Mach es leise, mach keinen Krach."</ta>
            <ta e="T777" id="Seg_13162" s="T773">Ich liege und höre den Liedern zu.</ta>
            <ta e="T786" id="Seg_13163" s="T778">Heyho, es stellt sich heraus, dass ich Chinesen oder so zuhöre.</ta>
            <ta e="T796" id="Seg_13164" s="T787">Ich hörte ihnen zu, es war sehr lustig, ich weiß.</ta>
            <ta e="T803" id="Seg_13165" s="T796">Dann fand ich zum Glück Russen und dann hörte es auf.</ta>
            <ta e="T811" id="Seg_13166" s="T804">"Nun, man muss es stoppen, hör auf, dein Vater kommt!"</ta>
            <ta e="T820" id="Seg_13167" s="T812">Die Sonne und der Mond standen hoch, sehr schön.</ta>
            <ta e="T825" id="Seg_13168" s="T821">Sehr hell, wie am Tag.</ta>
            <ta e="T837" id="Seg_13169" s="T826">Oh, unsere Rentiere rennen den Fluss entlang, sehr schön.</ta>
            <ta e="T842" id="Seg_13170" s="T838">Sogar Rauch kommt.</ta>
            <ta e="T855" id="Seg_13171" s="T843">Oh sehr schön, der Schnee im Dings, vom Mondschein dingst, glitzert er.</ta>
            <ta e="T866" id="Seg_13172" s="T856">Dann, warte, unser Hund macht "hm hm", er heult.</ta>
            <ta e="T872" id="Seg_13173" s="T867">Warte, eine Klingel klingelte, eine Halsglocke.</ta>
            <ta e="T880" id="Seg_13174" s="T873">Kling, kling, kling, unser Vater kommt, unser Vater, ja.</ta>
            <ta e="T884" id="Seg_13175" s="T881">Nun, er kam.</ta>
            <ta e="T902" id="Seg_13176" s="T885">Nun ich wusste nicht, ich denke er ging Holz holen, aber es stellte sich heraus, dass er nach Dudinka gefahren war.</ta>
            <ta e="T906" id="Seg_13177" s="T903">Nun, er kam.</ta>
            <ta e="T911" id="Seg_13178" s="T907">Erst nachdem es dunkel geworden war.</ta>
            <ta e="T915" id="Seg_13179" s="T912">Oh, mein Vater ist gekommen!</ta>
            <ta e="T923" id="Seg_13180" s="T916">Oh, meine Mutter ging ihm helfen, sie band [ihn?] von den Halsglocken los und so weiter.</ta>
            <ta e="T926" id="Seg_13181" s="T923">Sie lassen die Rentiere los und so.</ta>
            <ta e="T931" id="Seg_13182" s="T927">Oh, mein Vater kam mit Säcken.</ta>
            <ta e="T934" id="Seg_13183" s="T932">Er brachte die Säcke herein.</ta>
            <ta e="T937" id="Seg_13184" s="T935">Er packte sie aus.</ta>
            <ta e="T942" id="Seg_13185" s="T937">Oh, er bracht ganz viel Süßigkeiten.</ta>
            <ta e="T948" id="Seg_13186" s="T943">Dann etwas sehr Schönes, ich erinnere es immer.</ta>
            <ta e="T957" id="Seg_13187" s="T949">Eine Uhr oder so, es war sehr schön, aus einem Spiegel gemacht.</ta>
            <ta e="T968" id="Seg_13188" s="T958">Es hatte sogar Knöpfe, so schon, ich bin erstaunt, so schön.</ta>
            <ta e="T974" id="Seg_13189" s="T969">"Nun fass es nicht an, es ist genug", sagen sie.</ta>
            <ta e="T978" id="Seg_13190" s="T975">Sie ließen es mich nicht anfassen.</ta>
            <ta e="T982" id="Seg_13191" s="T979">"Du machst es noch kaputt!"</ta>
            <ta e="T991" id="Seg_13192" s="T983">Ich legte es hin, ich wollte es gerne sehen, ich wollte es anfassen.</ta>
            <ta e="T1000" id="Seg_13193" s="T992">Dann brachte er mir ein Dings, ein Geschenk, ein kleinen Kompass.</ta>
            <ta e="T1016" id="Seg_13194" s="T1001">Ich spielte immer damit, sehr schön, zum ersten Mal brachte er mir so ein Spielzeug.</ta>
            <ta e="T1024" id="Seg_13195" s="T1017">Oh, ich [fand] es so schön.</ta>
            <ta e="T1029" id="Seg_13196" s="T1025">"Oh, ein Geschenk für dich", sagt er.</ta>
            <ta e="T1038" id="Seg_13197" s="T1030">Nun sie unterhielten sich und ich schlief ein.</ta>
            <ta e="T1047" id="Seg_13198" s="T1039">Am Morgen wache ich auf, oh, das Feuer brennt schon kräftig.</ta>
            <ta e="T1055" id="Seg_13199" s="T1048">Sehr schön und niemand ist in unserem Haus.</ta>
            <ta e="T1060" id="Seg_13200" s="T1056">Sie fingen wohl die Rentiere.</ta>
            <ta e="T1063" id="Seg_13201" s="T1061">Sie gingen raus um sie zu fangen.</ta>
            <ta e="T1071" id="Seg_13202" s="T1064">Wir hatten Nachbarn, unsere Nachbarn waren da.</ta>
            <ta e="T1084" id="Seg_13203" s="T1072">Sie sammelten die Herde, sie sammeln die Rentiere, wir nomadisieren offenbar irgendwohin, erinnere ich mich.</ta>
            <ta e="T1090" id="Seg_13204" s="T1085">Oh, sie drängten mich mich anzuziehen.</ta>
            <ta e="T1096" id="Seg_13205" s="T1091">Sie banden mich auf den Schlitten.</ta>
            <ta e="T1100" id="Seg_13206" s="T1097">Nun, wohin fahren wir?</ta>
            <ta e="T1107" id="Seg_13207" s="T1101">Und hinter uns banden sie Rentiere an den Frachtschlitten.</ta>
            <ta e="T1114" id="Seg_13208" s="T1108">Wir transportierten einen Frachtschlitten mit Essen oder Holz.</ta>
            <ta e="T1120" id="Seg_13209" s="T1115">Ich hatte Angst vor ihnen und schrie laut.</ta>
            <ta e="T1124" id="Seg_13210" s="T1121">"Hab keine Angst, es beißt nicht!"</ta>
            <ta e="T1134" id="Seg_13211" s="T1125">Sie wurden müde, sie atmen direkt in mein Gesicht.</ta>
            <ta e="T1149" id="Seg_13212" s="T1135">So nomadisierten wir, so nomadisierte ich mit ihnen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T11" id="Seg_13213" s="T1">Однажды я помню, когда я была маленькой, в Нахонге, наша точка в двенадцати километрах от Крестов.</ta>
            <ta e="T15" id="Seg_13214" s="T11">Там жило много людей.</ta>
            <ta e="T21" id="Seg_13215" s="T16">Старики, все там жили.</ta>
            <ta e="T27" id="Seg_13216" s="T22">Там было много балков.</ta>
            <ta e="T32" id="Seg_13217" s="T28">Было много оленей и так далее.</ta>
            <ta e="T52" id="Seg_13218" s="T33">Потом однажды отец… Дома я дедушку называла "отец", а бабушку называла "мама", я у них воспитывалась.</ta>
            <ta e="T60" id="Seg_13219" s="T53">Вот, отец ушёл куда-то.</ta>
            <ta e="T72" id="Seg_13220" s="T61">Он, наверное, отправился за дровами, мы сидим [дома], а олени прямо вокруг дома ходят.</ta>
            <ta e="T76" id="Seg_13221" s="T73">Они ходят и скребут копытами.</ta>
            <ta e="T79" id="Seg_13222" s="T77">У нас был ручной олень.</ta>
            <ta e="T83" id="Seg_13223" s="T80">Он ест из рук.</ta>
            <ta e="T86" id="Seg_13224" s="T84">Очень любит хлеб.</ta>
            <ta e="T91" id="Seg_13225" s="T87">Подходит к окну, хлеб ищет.</ta>
            <ta e="T105" id="Seg_13226" s="T92">Ой, очень близко подходит со своим носом, ой, я испугалась, я маленькая была, всё время боялась.</ta>
            <ta e="T112" id="Seg_13227" s="T106">Мм, я потихоньку открыла.</ta>
            <ta e="T118" id="Seg_13228" s="T113">Со стола [он взял] хлеб, лепёшку и так далее.</ta>
            <ta e="T126" id="Seg_13229" s="T119">Ох, он возбудился, ест и ест, ужас!</ta>
            <ta e="T134" id="Seg_13230" s="T127">Ну, мама говорит, моя старая мама:</ta>
            <ta e="T142" id="Seg_13231" s="T135">"Ой, не приучай его, а то окно нам разобьёт и всё такое!</ta>
            <ta e="T152" id="Seg_13232" s="T143">Иди, иди, иди, не приходи больше, тут ничего нет!</ta>
            <ta e="T160" id="Seg_13233" s="T153">Иди ешь свою траву," — сказала она и закрыла [окно].</ta>
            <ta e="T169" id="Seg_13234" s="T161">Тогда он подошёл к другому окну, там, где передок.</ta>
            <ta e="T174" id="Seg_13235" s="T170">Мы туда клали лёд.</ta>
            <ta e="T180" id="Seg_13236" s="T175">Ну, воду, лёд.</ta>
            <ta e="T190" id="Seg_13237" s="T181">Он залез туда, наше окно открыто, переднее.</ta>
            <ta e="T196" id="Seg_13238" s="T191">[Стоит] там со своей вытянутой мордой, с рогами.</ta>
            <ta e="T208" id="Seg_13239" s="T197">Рога не пролезают, туда, где посуда, лепёшки, которые нарезают.</ta>
            <ta e="T217" id="Seg_13240" s="T209">Он пытался дотянуться, язык вытягивал, вытягивал.</ta>
            <ta e="T231" id="Seg_13241" s="T218">"Мама, гляди, гляди," говорю, "он сейчас съест это, хлеб съест," говорю, "хлеб!"</ta>
            <ta e="T243" id="Seg_13242" s="T232">"Ох, зачем же ты, дрянная девчонка, научила его из дома есть, из окна есть!</ta>
            <ta e="T249" id="Seg_13243" s="T244">Смотри, теперь он всё будет таскать," говорит она.</ta>
            <ta e="T253" id="Seg_13244" s="T250">Ай-ай-ай, [так смешно, что] живот болит.</ta>
            <ta e="T257" id="Seg_13245" s="T254">У-у, уходи, уходи.</ta>
            <ta e="T263" id="Seg_13246" s="T258">Как-то тряпкой его хлоп, хлоп.</ta>
            <ta e="T266" id="Seg_13247" s="T264">А он не уходит.</ta>
            <ta e="T271" id="Seg_13248" s="T267">Ну хватит, уходи.</ta>
            <ta e="T275" id="Seg_13249" s="T272">И всё-таки бросает ему [хлеба].</ta>
            <ta e="T293" id="Seg_13250" s="T276">Потом, к счастью, он ушёл, жуя хлеб, наверное, наелся, хотя как он может наесться? Грызёт его.</ta>
            <ta e="T299" id="Seg_13251" s="T294">Оказывается, она дала ему это, сухой хлеб.</ta>
            <ta e="T305" id="Seg_13252" s="T300">А он пытается его разжевать.</ta>
            <ta e="T308" id="Seg_13253" s="T306">Ой-ой-ой, так весело.</ta>
            <ta e="T314" id="Seg_13254" s="T309">Ой, он, наверное, свои зубы все это.</ta>
            <ta e="T321" id="Seg_13255" s="T315">Потом он, к счастью, пошёл к своим друзьям.</ta>
            <ta e="T328" id="Seg_13256" s="T322">Они спустились с высокого берега и пошли к реке.</ta>
            <ta e="T334" id="Seg_13257" s="T329">Снаружи уже смеркалось, уже темнело.</ta>
            <ta e="T347" id="Seg_13258" s="T335">У нас была собака, Дьонго, это была совсем белая, очень красивая пастушья собака.</ta>
            <ta e="T357" id="Seg_13259" s="T348">Только скажи "хуш," и она пригонит всё стадо.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T1150" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T1141" />
            <conversion-tli id="T1142" />
            <conversion-tli id="T1143" />
            <conversion-tli id="T1144" />
            <conversion-tli id="T1145" />
            <conversion-tli id="T1146" />
            <conversion-tli id="T1147" />
            <conversion-tli id="T1148" />
            <conversion-tli id="T1149" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
