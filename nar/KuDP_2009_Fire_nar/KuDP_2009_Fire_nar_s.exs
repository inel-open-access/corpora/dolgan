<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8F7D89AD-9DB8-8F38-25FE-E1B78CA8A980">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KuDP_2009_Fire_nar</transcription-name>
         <referenced-file url="KuDP_2009_Fire_nar.wav" />
         <referenced-file url="KuDP_2009_Fire_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KuDP_2009_Fire_nar\KuDP_2009_Fire_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">204</ud-information>
            <ud-information attribute-name="# HIAT:w">143</ud-information>
            <ud-information attribute-name="# e">146</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">3</ud-information>
            <ud-information attribute-name="# HIAT:u">17</ud-information>
            <ud-information attribute-name="# sc">4</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KuDP">
            <abbreviation>KuDP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="1.695" type="appl" />
         <tli id="T1" time="2.084" type="appl" />
         <tli id="T2" time="2.472" type="appl" />
         <tli id="T3" time="2.861" type="appl" />
         <tli id="T4" time="3.25" type="appl" />
         <tli id="T5" time="3.638" type="appl" />
         <tli id="T6" time="4.027" type="appl" />
         <tli id="T7" time="4.416" type="appl" />
         <tli id="T8" time="4.804" type="appl" />
         <tli id="T9" time="5.119668375746021" />
         <tli id="T10" time="5.508" type="appl" />
         <tli id="T11" time="5.824" type="appl" />
         <tli id="T12" time="6.139" type="appl" />
         <tli id="T13" time="6.454" type="appl" />
         <tli id="T14" time="6.769" type="appl" />
         <tli id="T15" time="7.085" type="appl" />
         <tli id="T16" time="7.4" type="appl" />
         <tli id="T17" time="7.715" type="appl" />
         <tli id="T18" time="8.03" type="appl" />
         <tli id="T19" time="8.346" type="appl" />
         <tli id="T20" time="8.747665117912799" />
         <tli id="T21" time="8.937" type="appl" />
         <tli id="T22" time="9.214" type="appl" />
         <tli id="T23" time="9.49" type="appl" />
         <tli id="T24" time="9.767" type="appl" />
         <tli id="T25" time="10.043" type="appl" />
         <tli id="T26" time="10.32" type="appl" />
         <tli id="T27" time="10.86266092651691" />
         <tli id="T28" time="10.923" type="appl" />
         <tli id="T29" time="11.249" type="appl" />
         <tli id="T30" time="11.576" type="appl" />
         <tli id="T31" time="11.903" type="appl" />
         <tli id="T32" time="12.229" type="appl" />
         <tli id="T33" time="12.51600067887931" />
         <tli id="T34" time="12.944" type="appl" />
         <tli id="T35" time="13.332" type="appl" />
         <tli id="T36" time="13.721" type="appl" />
         <tli id="T37" time="13.942337598640583" />
         <tli id="T38" time="14.47" type="appl" />
         <tli id="T39" time="14.832" type="appl" />
         <tli id="T40" time="15.193" type="appl" />
         <tli id="T147" time="15.343833333333333" type="intp" />
         <tli id="T41" time="15.555" type="appl" />
         <tli id="T42" time="15.916" type="appl" />
         <tli id="T43" time="16.278" type="appl" />
         <tli id="T44" time="16.639" type="appl" />
         <tli id="T45" time="17.001" type="appl" />
         <tli id="T46" time="17.362" type="appl" />
         <tli id="T47" time="17.791" type="appl" />
         <tli id="T48" time="18.219" type="appl" />
         <tli id="T49" time="18.648" type="appl" />
         <tli id="T50" time="19.076" type="appl" />
         <tli id="T51" time="19.505" type="appl" />
         <tli id="T52" time="19.933" type="appl" />
         <tli id="T53" time="20.362" type="appl" />
         <tli id="T54" time="20.79" type="appl" />
         <tli id="T55" time="21.219" type="appl" />
         <tli id="T56" time="21.647" type="appl" />
         <tli id="T57" time="22.076" type="appl" />
         <tli id="T58" time="22.504" type="appl" />
         <tli id="T59" time="22.933" type="appl" />
         <tli id="T60" time="23.361" type="appl" />
         <tli id="T61" time="23.79" type="appl" />
         <tli id="T62" time="24.437" type="appl" />
         <tli id="T63" time="25.084" type="appl" />
         <tli id="T64" time="25.731" type="appl" />
         <tli id="T65" time="26.377" type="appl" />
         <tli id="T66" time="27.024" type="appl" />
         <tli id="T67" time="27.671" type="appl" />
         <tli id="T68" time="28.7060575066313" />
         <tli id="T69" time="28.82" type="appl" />
         <tli id="T70" time="29.322" type="appl" />
         <tli id="T71" time="29.825" type="appl" />
         <tli id="T72" time="30.327" type="appl" />
         <tli id="T73" time="30.829" type="appl" />
         <tli id="T74" time="31.331" type="appl" />
         <tli id="T75" time="31.833" type="appl" />
         <tli id="T76" time="32.336" type="appl" />
         <tli id="T77" time="32.838" type="appl" />
         <tli id="T78" time="33.35333193551061" />
         <tli id="T79" time="33.784" type="appl" />
         <tli id="T80" time="34.229" type="appl" />
         <tli id="T81" time="34.674" type="appl" />
         <tli id="T82" time="35.118" type="appl" />
         <tli id="T83" time="35.562" type="appl" />
         <tli id="T84" time="36.007" type="appl" />
         <tli id="T85" time="36.452" type="appl" />
         <tli id="T86" time="36.896" type="appl" />
         <tli id="T87" time="37.34" type="appl" />
         <tli id="T88" time="37.785" type="appl" />
         <tli id="T89" time="38.23" type="appl" />
         <tli id="T90" time="38.60733502569629" />
         <tli id="T91" time="38.954" type="appl" />
         <tli id="T92" time="39.234" type="appl" />
         <tli id="T93" time="39.514" type="appl" />
         <tli id="T94" time="39.794" type="appl" />
         <tli id="T95" time="40.075" type="appl" />
         <tli id="T96" time="40.355" type="appl" />
         <tli id="T97" time="40.635" type="appl" />
         <tli id="T98" time="40.915" type="appl" />
         <tli id="T150" time="41.81244604774536" />
         <tli id="T99" time="42.00577527851459" />
         <tli id="T100" time="42.15910535809019" />
         <tli id="T101" time="42.54576381962865" />
         <tli id="T102" time="42.712" type="appl" />
         <tli id="T103" time="43.217" type="appl" />
         <tli id="T104" time="43.723" type="appl" />
         <tli id="T105" time="44.394663386936344" />
         <tli id="T106" time="44.876" type="appl" />
         <tli id="T107" time="45.523" type="appl" />
         <tli id="T108" time="46.17" type="appl" />
         <tli id="T109" time="46.818" type="appl" />
         <tli id="T110" time="47.133" type="appl" />
         <tli id="T111" time="47.448" type="appl" />
         <tli id="T112" time="47.763" type="appl" />
         <tli id="T113" time="48.078" type="appl" />
         <tli id="T114" time="48.392" type="appl" />
         <tli id="T115" time="48.707" type="appl" />
         <tli id="T116" time="49.022" type="appl" />
         <tli id="T117" time="49.337" type="appl" />
         <tli id="T118" time="49.652" type="appl" />
         <tli id="T119" time="49.90033431366047" />
         <tli id="T120" time="50.33" type="appl" />
         <tli id="T121" time="50.693" type="appl" />
         <tli id="T122" time="51.056" type="appl" />
         <tli id="T123" time="51.419" type="appl" />
         <tli id="T124" time="51.782" type="appl" />
         <tli id="T125" time="52.145" type="appl" />
         <tli id="T126" time="52.508" type="appl" />
         <tli id="T127" time="52.872" type="appl" />
         <tli id="T128" time="53.235" type="appl" />
         <tli id="T129" time="53.598" type="appl" />
         <tli id="T130" time="53.961" type="appl" />
         <tli id="T131" time="54.324" type="appl" />
         <tli id="T132" time="54.687" type="appl" />
         <tli id="T133" time="55.05" type="appl" />
         <tli id="T149" time="55.271497790948274" type="intp" />
         <tli id="T134" time="55.49299558189655" />
         <tli id="T135" time="55.938" type="appl" />
         <tli id="T136" time="56.463" type="appl" />
         <tli id="T137" time="56.989" type="appl" />
         <tli id="T138" time="57.514" type="appl" />
         <tli id="T139" time="58.039" type="appl" />
         <tli id="T140" time="58.564" type="appl" />
         <tli id="T141" time="59.089" type="appl" />
         <tli id="T142" time="59.614" type="appl" />
         <tli id="T143" time="60.14" type="appl" />
         <tli id="T148" time="60.4025" type="intp" />
         <tli id="T144" time="60.665" type="appl" />
         <tli id="T145" time="61.19" type="appl" />
         <tli id="T146" time="62.832" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KuDP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T149" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <ts e="T1" id="Seg_5" n="HIAT:w" s="T0">Biːr-</ts>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_9" n="HIAT:w" s="T1">biːrde</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_12" n="HIAT:w" s="T2">kim</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">Riːtanɨ</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">gɨtta</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">že</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">bihi͡eke</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">kelbit</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">etibit</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_34" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">Onton</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">onno</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">iti</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">ke</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">dʼi͡e</ts>
                  <nts id="Seg_49" n="HIAT:ip">,</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">dʼi͡e</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">min</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">že</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">dʼi͡em</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">tɨmnɨː</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">ete</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_71" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">Onton</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">Riːta</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">onno</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">dʼendʼinnan</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">oːnnʼuː</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">oloror</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">ete</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_95" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">Onton</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">min</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_103" n="HIAT:w" s="T29">di͡ebitim</ts>
                  <nts id="Seg_104" n="HIAT:ip">:</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_107" n="HIAT:u" s="T30">
                  <nts id="Seg_108" n="HIAT:ip">"</nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">U͡otu</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">ottu͡om</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">du͡o</ts>
                  <nts id="Seg_117" n="HIAT:ip">?</nts>
                  <nts id="Seg_118" n="HIAT:ip">"</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_121" n="HIAT:u" s="T33">
                  <nts id="Seg_122" n="HIAT:ip">"</nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">Otun</ts>
                  <nts id="Seg_125" n="HIAT:ip">,</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_128" n="HIAT:w" s="T34">otun</ts>
                  <nts id="Seg_129" n="HIAT:ip">"</nts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">di͡ebite</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">Riːta</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_140" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">Onton</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">min</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_148" n="HIAT:w" s="T39">barbɨtɨm</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_151" n="HIAT:ip">(</nts>
                  <nts id="Seg_152" n="HIAT:ip">(</nts>
                  <ats e="T147" id="Seg_153" n="HIAT:non-pho" s="T40">…</ats>
                  <nts id="Seg_154" n="HIAT:ip">)</nts>
                  <nts id="Seg_155" n="HIAT:ip">)</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T147">ulakan</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">bagajɨ</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">čok</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">hɨtar</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_170" n="HIAT:w" s="T44">ete</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_174" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">Onton</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_179" n="HIAT:w" s="T47">onu</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">kimi</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_185" n="HIAT:w" s="T49">čogunan</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">ɨspɨtɨm</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_191" n="HIAT:w" s="T51">kimi</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">eː</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">kim</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">eː</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">čokko</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">ɨspɨtɨm</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_209" n="HIAT:w" s="T57">salʼarkanɨ</ts>
                  <nts id="Seg_210" n="HIAT:ip">,</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">eː</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_216" n="HIAT:w" s="T59">salʼarka</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_219" n="HIAT:w" s="T60">bu͡oltak</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_223" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_225" n="HIAT:w" s="T61">Kim</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_228" n="HIAT:w" s="T62">etej</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_232" n="HIAT:w" s="T63">eː</ts>
                  <nts id="Seg_233" n="HIAT:ip">,</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">kʼerasinɨ</ts>
                  <nts id="Seg_237" n="HIAT:ip">,</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_240" n="HIAT:w" s="T65">onton</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_243" n="HIAT:w" s="T66">hɨrajɨm</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">ubajbɨta</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_250" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_252" n="HIAT:w" s="T68">Onton</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_255" n="HIAT:w" s="T69">hojut</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">kim</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_261" n="HIAT:w" s="T71">dʼi͡ebitiger</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_264" n="HIAT:w" s="T72">tʼotʼa</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_267" n="HIAT:w" s="T73">Stʼošalaːk</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_270" n="HIAT:w" s="T74">hüːren</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_273" n="HIAT:w" s="T75">barbɨtɨm</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_276" n="HIAT:w" s="T76">Riːtanɨ</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_279" n="HIAT:w" s="T77">gɨtta</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_283" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_285" n="HIAT:w" s="T78">Onton</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_288" n="HIAT:w" s="T79">onno</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_291" n="HIAT:w" s="T80">kimi͡eke</ts>
                  <nts id="Seg_292" n="HIAT:ip">,</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_295" n="HIAT:w" s="T81">kim</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_298" n="HIAT:w" s="T82">etej</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_301" n="HIAT:w" s="T83">teːtemeːk</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_304" n="HIAT:w" s="T84">onno</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_307" n="HIAT:w" s="T85">tɨː</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_310" n="HIAT:w" s="T86">oŋoro</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_313" n="HIAT:w" s="T87">hɨtaːččɨ</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_316" n="HIAT:w" s="T88">etilere</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_319" n="HIAT:w" s="T89">bi͡ekten</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_323" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_325" n="HIAT:w" s="T90">Onton</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_328" n="HIAT:w" s="T91">onno</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_331" n="HIAT:w" s="T92">tʼotʼa</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_334" n="HIAT:w" s="T93">Stʼoša</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_337" n="HIAT:w" s="T94">di͡ebite</ts>
                  <nts id="Seg_338" n="HIAT:ip">,</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_341" n="HIAT:w" s="T95">eː</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_344" n="HIAT:w" s="T96">Roska</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_347" n="HIAT:w" s="T97">di͡ebit</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_350" n="HIAT:w" s="T98">ete</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_354" n="HIAT:u" s="T150">
                  <nts id="Seg_355" n="HIAT:ip">(</nts>
                  <nts id="Seg_356" n="HIAT:ip">(</nts>
                  <ats e="T99" id="Seg_357" n="HIAT:non-pho" s="T150">NOISE</ats>
                  <nts id="Seg_358" n="HIAT:ip">)</nts>
                  <nts id="Seg_359" n="HIAT:ip">)</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_361" n="HIAT:ip">"</nts>
                  <ts e="T100" id="Seg_363" n="HIAT:w" s="T99">Tu͡ok</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_366" n="HIAT:w" s="T100">bu͡olbut</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_369" n="HIAT:w" s="T101">araj</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_372" n="HIAT:w" s="T102">brovʼilarɨŋ</ts>
                  <nts id="Seg_373" n="HIAT:ip">"</nts>
                  <nts id="Seg_374" n="HIAT:ip">,</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_377" n="HIAT:w" s="T103">di͡ebit</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_380" n="HIAT:w" s="T104">ete</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_384" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_386" n="HIAT:w" s="T105">Zamʼečʼajdɨː</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_389" n="HIAT:w" s="T106">ilik</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_392" n="HIAT:w" s="T107">ete</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_395" n="HIAT:w" s="T108">brovʼilarbɨn</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_399" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_401" n="HIAT:w" s="T109">Onton</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_404" n="HIAT:w" s="T110">min</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_407" n="HIAT:w" s="T111">di͡ebitim</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_410" n="HIAT:w" s="T112">tu͡ok</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_413" n="HIAT:w" s="T113">da</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_415" n="HIAT:ip">(</nts>
                  <ts e="T115" id="Seg_417" n="HIAT:w" s="T114">di͡e-</ts>
                  <nts id="Seg_418" n="HIAT:ip">)</nts>
                  <nts id="Seg_419" n="HIAT:ip">,</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_422" n="HIAT:w" s="T115">eː</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_425" n="HIAT:w" s="T116">tu͡ok</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_428" n="HIAT:w" s="T117">da</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_431" n="HIAT:w" s="T118">kepseːbetegim</ts>
                  <nts id="Seg_432" n="HIAT:ip">.</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_435" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_437" n="HIAT:w" s="T119">Onton</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_440" n="HIAT:w" s="T120">tʼotʼa</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_443" n="HIAT:w" s="T121">Stʼoša</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_446" n="HIAT:w" s="T122">ɨgɨrbɨta</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_449" n="HIAT:w" s="T123">teːtebin</ts>
                  <nts id="Seg_450" n="HIAT:ip">,</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_453" n="HIAT:w" s="T124">onton</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_456" n="HIAT:w" s="T125">teːtem</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_459" n="HIAT:w" s="T126">tu͡ok</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_462" n="HIAT:w" s="T127">da</ts>
                  <nts id="Seg_463" n="HIAT:ip">,</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_466" n="HIAT:w" s="T128">eː</ts>
                  <nts id="Seg_467" n="HIAT:ip">,</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_470" n="HIAT:w" s="T129">emi͡e</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_473" n="HIAT:w" s="T130">ɨgɨrbɨta</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_476" n="HIAT:w" s="T131">teːtebin</ts>
                  <nts id="Seg_477" n="HIAT:ip">,</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_480" n="HIAT:w" s="T132">onton</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_483" n="HIAT:w" s="T133">barbɨta</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T145" id="Seg_486" n="sc" s="T134">
               <ts e="T145" id="Seg_488" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_490" n="HIAT:w" s="T134">Tʼotʼa</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_493" n="HIAT:w" s="T135">Lʼenalaːkka</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_496" n="HIAT:w" s="T136">ɨgɨrbɨta</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_499" n="HIAT:w" s="T137">kimi</ts>
                  <nts id="Seg_500" n="HIAT:ip">,</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_503" n="HIAT:w" s="T138">tʼotʼa</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_506" n="HIAT:w" s="T139">Lʼena</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_509" n="HIAT:w" s="T140">doːktor</ts>
                  <nts id="Seg_510" n="HIAT:ip">,</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_513" n="HIAT:w" s="T141">Lʼenanɨ</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_516" n="HIAT:w" s="T142">onton</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_518" n="HIAT:ip">(</nts>
                  <nts id="Seg_519" n="HIAT:ip">(</nts>
                  <ats e="T148" id="Seg_520" n="HIAT:non-pho" s="T143">…</ats>
                  <nts id="Seg_521" n="HIAT:ip">)</nts>
                  <nts id="Seg_522" n="HIAT:ip">)</nts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_526" n="HIAT:w" s="T148">elete</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T149" id="Seg_529" n="sc" s="T0">
               <ts e="T1" id="Seg_531" n="e" s="T0">(Biːr-) </ts>
               <ts e="T2" id="Seg_533" n="e" s="T1">biːrde </ts>
               <ts e="T3" id="Seg_535" n="e" s="T2">kim </ts>
               <ts e="T4" id="Seg_537" n="e" s="T3">Riːtanɨ </ts>
               <ts e="T5" id="Seg_539" n="e" s="T4">gɨtta </ts>
               <ts e="T6" id="Seg_541" n="e" s="T5">že </ts>
               <ts e="T7" id="Seg_543" n="e" s="T6">bihi͡eke </ts>
               <ts e="T8" id="Seg_545" n="e" s="T7">kelbit </ts>
               <ts e="T9" id="Seg_547" n="e" s="T8">etibit. </ts>
               <ts e="T10" id="Seg_549" n="e" s="T9">Onton </ts>
               <ts e="T11" id="Seg_551" n="e" s="T10">onno </ts>
               <ts e="T12" id="Seg_553" n="e" s="T11">iti </ts>
               <ts e="T13" id="Seg_555" n="e" s="T12">ke </ts>
               <ts e="T14" id="Seg_557" n="e" s="T13">dʼi͡e, </ts>
               <ts e="T15" id="Seg_559" n="e" s="T14">dʼi͡e </ts>
               <ts e="T16" id="Seg_561" n="e" s="T15">min </ts>
               <ts e="T17" id="Seg_563" n="e" s="T16">že </ts>
               <ts e="T18" id="Seg_565" n="e" s="T17">dʼi͡em </ts>
               <ts e="T19" id="Seg_567" n="e" s="T18">tɨmnɨː </ts>
               <ts e="T20" id="Seg_569" n="e" s="T19">ete. </ts>
               <ts e="T21" id="Seg_571" n="e" s="T20">Onton </ts>
               <ts e="T22" id="Seg_573" n="e" s="T21">Riːta </ts>
               <ts e="T23" id="Seg_575" n="e" s="T22">onno </ts>
               <ts e="T24" id="Seg_577" n="e" s="T23">dʼendʼinnan </ts>
               <ts e="T25" id="Seg_579" n="e" s="T24">oːnnʼuː </ts>
               <ts e="T26" id="Seg_581" n="e" s="T25">oloror </ts>
               <ts e="T27" id="Seg_583" n="e" s="T26">ete. </ts>
               <ts e="T28" id="Seg_585" n="e" s="T27">Onton </ts>
               <ts e="T29" id="Seg_587" n="e" s="T28">min </ts>
               <ts e="T30" id="Seg_589" n="e" s="T29">di͡ebitim: </ts>
               <ts e="T31" id="Seg_591" n="e" s="T30">"U͡otu </ts>
               <ts e="T32" id="Seg_593" n="e" s="T31">ottu͡om </ts>
               <ts e="T33" id="Seg_595" n="e" s="T32">du͡o?" </ts>
               <ts e="T34" id="Seg_597" n="e" s="T33">"Otun, </ts>
               <ts e="T35" id="Seg_599" n="e" s="T34">otun", </ts>
               <ts e="T36" id="Seg_601" n="e" s="T35">di͡ebite </ts>
               <ts e="T37" id="Seg_603" n="e" s="T36">Riːta. </ts>
               <ts e="T38" id="Seg_605" n="e" s="T37">Onton </ts>
               <ts e="T39" id="Seg_607" n="e" s="T38">min </ts>
               <ts e="T40" id="Seg_609" n="e" s="T39">barbɨtɨm, </ts>
               <ts e="T147" id="Seg_611" n="e" s="T40">((…)) </ts>
               <ts e="T41" id="Seg_613" n="e" s="T147">ulakan </ts>
               <ts e="T42" id="Seg_615" n="e" s="T41">bagajɨ </ts>
               <ts e="T43" id="Seg_617" n="e" s="T42">čok </ts>
               <ts e="T44" id="Seg_619" n="e" s="T43">hɨtar </ts>
               <ts e="T46" id="Seg_621" n="e" s="T44">ete. </ts>
               <ts e="T47" id="Seg_623" n="e" s="T46">Onton </ts>
               <ts e="T48" id="Seg_625" n="e" s="T47">onu </ts>
               <ts e="T49" id="Seg_627" n="e" s="T48">kimi </ts>
               <ts e="T50" id="Seg_629" n="e" s="T49">čogunan </ts>
               <ts e="T51" id="Seg_631" n="e" s="T50">ɨspɨtɨm </ts>
               <ts e="T52" id="Seg_633" n="e" s="T51">kimi </ts>
               <ts e="T53" id="Seg_635" n="e" s="T52">eː </ts>
               <ts e="T54" id="Seg_637" n="e" s="T53">kim </ts>
               <ts e="T55" id="Seg_639" n="e" s="T54">eː </ts>
               <ts e="T56" id="Seg_641" n="e" s="T55">čokko </ts>
               <ts e="T57" id="Seg_643" n="e" s="T56">ɨspɨtɨm </ts>
               <ts e="T58" id="Seg_645" n="e" s="T57">salʼarkanɨ, </ts>
               <ts e="T59" id="Seg_647" n="e" s="T58">eː </ts>
               <ts e="T60" id="Seg_649" n="e" s="T59">salʼarka </ts>
               <ts e="T61" id="Seg_651" n="e" s="T60">bu͡oltak. </ts>
               <ts e="T62" id="Seg_653" n="e" s="T61">Kim </ts>
               <ts e="T63" id="Seg_655" n="e" s="T62">etej, </ts>
               <ts e="T64" id="Seg_657" n="e" s="T63">eː, </ts>
               <ts e="T65" id="Seg_659" n="e" s="T64">kʼerasinɨ, </ts>
               <ts e="T66" id="Seg_661" n="e" s="T65">onton </ts>
               <ts e="T67" id="Seg_663" n="e" s="T66">hɨrajɨm </ts>
               <ts e="T68" id="Seg_665" n="e" s="T67">ubajbɨta. </ts>
               <ts e="T69" id="Seg_667" n="e" s="T68">Onton </ts>
               <ts e="T70" id="Seg_669" n="e" s="T69">hojut </ts>
               <ts e="T71" id="Seg_671" n="e" s="T70">kim </ts>
               <ts e="T72" id="Seg_673" n="e" s="T71">dʼi͡ebitiger </ts>
               <ts e="T73" id="Seg_675" n="e" s="T72">tʼotʼa </ts>
               <ts e="T74" id="Seg_677" n="e" s="T73">Stʼošalaːk </ts>
               <ts e="T75" id="Seg_679" n="e" s="T74">hüːren </ts>
               <ts e="T76" id="Seg_681" n="e" s="T75">barbɨtɨm </ts>
               <ts e="T77" id="Seg_683" n="e" s="T76">Riːtanɨ </ts>
               <ts e="T78" id="Seg_685" n="e" s="T77">gɨtta. </ts>
               <ts e="T79" id="Seg_687" n="e" s="T78">Onton </ts>
               <ts e="T80" id="Seg_689" n="e" s="T79">onno </ts>
               <ts e="T81" id="Seg_691" n="e" s="T80">kimi͡eke, </ts>
               <ts e="T82" id="Seg_693" n="e" s="T81">kim </ts>
               <ts e="T83" id="Seg_695" n="e" s="T82">etej </ts>
               <ts e="T84" id="Seg_697" n="e" s="T83">teːtemeːk </ts>
               <ts e="T85" id="Seg_699" n="e" s="T84">onno </ts>
               <ts e="T86" id="Seg_701" n="e" s="T85">tɨː </ts>
               <ts e="T87" id="Seg_703" n="e" s="T86">oŋoro </ts>
               <ts e="T88" id="Seg_705" n="e" s="T87">hɨtaːččɨ </ts>
               <ts e="T89" id="Seg_707" n="e" s="T88">etilere </ts>
               <ts e="T90" id="Seg_709" n="e" s="T89">bi͡ekten. </ts>
               <ts e="T91" id="Seg_711" n="e" s="T90">Onton </ts>
               <ts e="T92" id="Seg_713" n="e" s="T91">onno </ts>
               <ts e="T93" id="Seg_715" n="e" s="T92">tʼotʼa </ts>
               <ts e="T94" id="Seg_717" n="e" s="T93">Stʼoša </ts>
               <ts e="T95" id="Seg_719" n="e" s="T94">di͡ebite, </ts>
               <ts e="T96" id="Seg_721" n="e" s="T95">eː </ts>
               <ts e="T97" id="Seg_723" n="e" s="T96">Roska </ts>
               <ts e="T98" id="Seg_725" n="e" s="T97">di͡ebit </ts>
               <ts e="T150" id="Seg_727" n="e" s="T98">ete. </ts>
               <ts e="T99" id="Seg_729" n="e" s="T150">((NOISE)) </ts>
               <ts e="T100" id="Seg_731" n="e" s="T99">"Tu͡ok </ts>
               <ts e="T101" id="Seg_733" n="e" s="T100">bu͡olbut </ts>
               <ts e="T102" id="Seg_735" n="e" s="T101">araj </ts>
               <ts e="T103" id="Seg_737" n="e" s="T102">brovʼilarɨŋ", </ts>
               <ts e="T104" id="Seg_739" n="e" s="T103">di͡ebit </ts>
               <ts e="T105" id="Seg_741" n="e" s="T104">ete. </ts>
               <ts e="T106" id="Seg_743" n="e" s="T105">Zamʼečʼajdɨː </ts>
               <ts e="T107" id="Seg_745" n="e" s="T106">ilik </ts>
               <ts e="T108" id="Seg_747" n="e" s="T107">ete </ts>
               <ts e="T109" id="Seg_749" n="e" s="T108">brovʼilarbɨn. </ts>
               <ts e="T110" id="Seg_751" n="e" s="T109">Onton </ts>
               <ts e="T111" id="Seg_753" n="e" s="T110">min </ts>
               <ts e="T112" id="Seg_755" n="e" s="T111">di͡ebitim </ts>
               <ts e="T113" id="Seg_757" n="e" s="T112">tu͡ok </ts>
               <ts e="T114" id="Seg_759" n="e" s="T113">da </ts>
               <ts e="T115" id="Seg_761" n="e" s="T114">(di͡e-), </ts>
               <ts e="T116" id="Seg_763" n="e" s="T115">eː </ts>
               <ts e="T117" id="Seg_765" n="e" s="T116">tu͡ok </ts>
               <ts e="T118" id="Seg_767" n="e" s="T117">da </ts>
               <ts e="T119" id="Seg_769" n="e" s="T118">kepseːbetegim. </ts>
               <ts e="T120" id="Seg_771" n="e" s="T119">Onton </ts>
               <ts e="T121" id="Seg_773" n="e" s="T120">tʼotʼa </ts>
               <ts e="T122" id="Seg_775" n="e" s="T121">Stʼoša </ts>
               <ts e="T123" id="Seg_777" n="e" s="T122">ɨgɨrbɨta </ts>
               <ts e="T124" id="Seg_779" n="e" s="T123">teːtebin, </ts>
               <ts e="T125" id="Seg_781" n="e" s="T124">onton </ts>
               <ts e="T126" id="Seg_783" n="e" s="T125">teːtem </ts>
               <ts e="T127" id="Seg_785" n="e" s="T126">tu͡ok </ts>
               <ts e="T128" id="Seg_787" n="e" s="T127">da, </ts>
               <ts e="T129" id="Seg_789" n="e" s="T128">eː, </ts>
               <ts e="T130" id="Seg_791" n="e" s="T129">emi͡e </ts>
               <ts e="T131" id="Seg_793" n="e" s="T130">ɨgɨrbɨta </ts>
               <ts e="T132" id="Seg_795" n="e" s="T131">teːtebin, </ts>
               <ts e="T133" id="Seg_797" n="e" s="T132">onton </ts>
               <ts e="T149" id="Seg_799" n="e" s="T133">barbɨta. </ts>
            </ts>
            <ts e="T145" id="Seg_800" n="sc" s="T134">
               <ts e="T135" id="Seg_802" n="e" s="T134">Tʼotʼa </ts>
               <ts e="T136" id="Seg_804" n="e" s="T135">Lʼenalaːkka </ts>
               <ts e="T137" id="Seg_806" n="e" s="T136">ɨgɨrbɨta </ts>
               <ts e="T138" id="Seg_808" n="e" s="T137">kimi, </ts>
               <ts e="T139" id="Seg_810" n="e" s="T138">tʼotʼa </ts>
               <ts e="T140" id="Seg_812" n="e" s="T139">Lʼena </ts>
               <ts e="T141" id="Seg_814" n="e" s="T140">doːktor, </ts>
               <ts e="T142" id="Seg_816" n="e" s="T141">Lʼenanɨ </ts>
               <ts e="T143" id="Seg_818" n="e" s="T142">onton </ts>
               <ts e="T148" id="Seg_820" n="e" s="T143">((…)), </ts>
               <ts e="T145" id="Seg_822" n="e" s="T148">elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_823" s="T0">KuDP_2009_Fire_nar.001 (001.001)</ta>
            <ta e="T20" id="Seg_824" s="T9">KuDP_2009_Fire_nar.002 (001.002)</ta>
            <ta e="T27" id="Seg_825" s="T20">KuDP_2009_Fire_nar.003 (001.003)</ta>
            <ta e="T30" id="Seg_826" s="T27">KuDP_2009_Fire_nar.004 (001.004)</ta>
            <ta e="T33" id="Seg_827" s="T30">KuDP_2009_Fire_nar.005 (001.004)</ta>
            <ta e="T37" id="Seg_828" s="T33">KuDP_2009_Fire_nar.006 (001.005)</ta>
            <ta e="T46" id="Seg_829" s="T37">KuDP_2009_Fire_nar.007 (001.006)</ta>
            <ta e="T61" id="Seg_830" s="T46">KuDP_2009_Fire_nar.008 (001.007)</ta>
            <ta e="T68" id="Seg_831" s="T61">KuDP_2009_Fire_nar.009 (001.008)</ta>
            <ta e="T78" id="Seg_832" s="T68">KuDP_2009_Fire_nar.010 (001.009)</ta>
            <ta e="T90" id="Seg_833" s="T78">KuDP_2009_Fire_nar.011 (001.010)</ta>
            <ta e="T150" id="Seg_834" s="T90">KuDP_2009_Fire_nar.012 (001.011)</ta>
            <ta e="T105" id="Seg_835" s="T150">KuDP_2009_Fire_nar.013 (001.012)</ta>
            <ta e="T109" id="Seg_836" s="T105">KuDP_2009_Fire_nar.014 (001.013)</ta>
            <ta e="T119" id="Seg_837" s="T109">KuDP_2009_Fire_nar.015 (001.014)</ta>
            <ta e="T149" id="Seg_838" s="T119">KuDP_2009_Fire_nar.016 (001.015)</ta>
            <ta e="T145" id="Seg_839" s="T134">KuDP_2009_Fire_nar.017 (001.016)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_840" s="T0">(Biːr-) biːrde kim Riːtanɨ gɨtta že bihi͡eke kelbit etibit. </ta>
            <ta e="T20" id="Seg_841" s="T9">Onton onno iti ke dʼi͡e, dʼi͡e min že dʼi͡em tɨmnɨː ete. </ta>
            <ta e="T27" id="Seg_842" s="T20">Onton Riːta onno dʼendʼinnan oːnnʼuː oloror ete. </ta>
            <ta e="T30" id="Seg_843" s="T27">Onton min di͡ebitim: </ta>
            <ta e="T33" id="Seg_844" s="T30">"U͡otu ottu͡om du͡o?" </ta>
            <ta e="T37" id="Seg_845" s="T33">"Otun, otun", di͡ebite Riːta. </ta>
            <ta e="T46" id="Seg_846" s="T37">Onton min barbɨtɨm, ((…)) ulakan bagajɨ čok hɨtar ete. </ta>
            <ta e="T61" id="Seg_847" s="T46">Onton onu kimi čogunan ɨspɨtɨm kimi eː kim eː čokko ɨspɨtɨm salʼarkanɨ, eː salʼarka bu͡oltak. </ta>
            <ta e="T68" id="Seg_848" s="T61">Kim etej, eː, kʼerasinɨ, onton hɨrajɨm ubajbɨta. </ta>
            <ta e="T78" id="Seg_849" s="T68">Onton hojut kim dʼi͡ebitiger tʼotʼa Stʼošalaːk hüːren barbɨtɨm Riːtanɨ gɨtta. </ta>
            <ta e="T90" id="Seg_850" s="T78">Onton onno kimi͡eke, kim etej teːtemeːk onno tɨː oŋoro hɨtaːččɨ etilere bi͡ekten. </ta>
            <ta e="T150" id="Seg_851" s="T90">Onton onno tʼotʼa Stʼoša di͡ebite, eː Roska di͡ebit ete. </ta>
            <ta e="T105" id="Seg_852" s="T150">((NOISE)) "Tu͡ok bu͡olbut araj brovʼilarɨŋ", di͡ebit ete. </ta>
            <ta e="T109" id="Seg_853" s="T105">Zamʼečʼajdɨː ilik ete brovʼilarbɨn. </ta>
            <ta e="T119" id="Seg_854" s="T109">Onton min di͡ebitim tu͡ok da (di͡e-), eː tu͡ok da kepseːbetegim. </ta>
            <ta e="T149" id="Seg_855" s="T119">Onton tʼotʼa Stʼoša ɨgɨrbɨta teːtebin, onton teːtem tu͡ok da, eː, emi͡e ɨgɨrbɨta teːtebin, onton barbɨta. </ta>
            <ta e="T145" id="Seg_856" s="T134">Tʼotʼa Lʼenalaːkka ɨgɨrbɨta kimi, tʼotʼa Lʼena doːktor, Lʼenanɨ onton ((…)), elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_857" s="T0">biːr</ta>
            <ta e="T2" id="Seg_858" s="T1">biːrde</ta>
            <ta e="T3" id="Seg_859" s="T2">kim</ta>
            <ta e="T4" id="Seg_860" s="T3">Riːta-nɨ</ta>
            <ta e="T5" id="Seg_861" s="T4">gɨtta</ta>
            <ta e="T6" id="Seg_862" s="T5">že</ta>
            <ta e="T7" id="Seg_863" s="T6">bihi͡e-ke</ta>
            <ta e="T8" id="Seg_864" s="T7">kel-bit</ta>
            <ta e="T9" id="Seg_865" s="T8">e-ti-bit</ta>
            <ta e="T10" id="Seg_866" s="T9">onton</ta>
            <ta e="T11" id="Seg_867" s="T10">onno</ta>
            <ta e="T12" id="Seg_868" s="T11">iti</ta>
            <ta e="T13" id="Seg_869" s="T12">ke</ta>
            <ta e="T14" id="Seg_870" s="T13">dʼi͡e</ta>
            <ta e="T15" id="Seg_871" s="T14">dʼi͡e</ta>
            <ta e="T16" id="Seg_872" s="T15">min</ta>
            <ta e="T17" id="Seg_873" s="T16">že</ta>
            <ta e="T18" id="Seg_874" s="T17">dʼi͡e-m</ta>
            <ta e="T19" id="Seg_875" s="T18">tɨmnɨː</ta>
            <ta e="T20" id="Seg_876" s="T19">e-t-e</ta>
            <ta e="T21" id="Seg_877" s="T20">onton</ta>
            <ta e="T22" id="Seg_878" s="T21">Riːta</ta>
            <ta e="T23" id="Seg_879" s="T22">onno</ta>
            <ta e="T24" id="Seg_880" s="T23">dʼendʼi-nnan</ta>
            <ta e="T25" id="Seg_881" s="T24">oːnnʼ-uː</ta>
            <ta e="T26" id="Seg_882" s="T25">olor-or</ta>
            <ta e="T27" id="Seg_883" s="T26">e-t-e</ta>
            <ta e="T28" id="Seg_884" s="T27">onton</ta>
            <ta e="T29" id="Seg_885" s="T28">min</ta>
            <ta e="T30" id="Seg_886" s="T29">di͡e-bit-i-m</ta>
            <ta e="T31" id="Seg_887" s="T30">u͡ot-u</ta>
            <ta e="T32" id="Seg_888" s="T31">ott-u͡o-m</ta>
            <ta e="T33" id="Seg_889" s="T32">du͡o</ta>
            <ta e="T34" id="Seg_890" s="T33">otun</ta>
            <ta e="T35" id="Seg_891" s="T34">otun</ta>
            <ta e="T36" id="Seg_892" s="T35">di͡e-bit-e</ta>
            <ta e="T37" id="Seg_893" s="T36">Riːta</ta>
            <ta e="T38" id="Seg_894" s="T37">onton</ta>
            <ta e="T39" id="Seg_895" s="T38">min</ta>
            <ta e="T40" id="Seg_896" s="T39">bar-bɨt-ɨ-m</ta>
            <ta e="T41" id="Seg_897" s="T147">ulakan</ta>
            <ta e="T42" id="Seg_898" s="T41">bagajɨ</ta>
            <ta e="T43" id="Seg_899" s="T42">čok</ta>
            <ta e="T44" id="Seg_900" s="T43">hɨt-ar</ta>
            <ta e="T46" id="Seg_901" s="T44">e-t-e</ta>
            <ta e="T47" id="Seg_902" s="T46">onton</ta>
            <ta e="T48" id="Seg_903" s="T47">o-nu</ta>
            <ta e="T49" id="Seg_904" s="T48">kim-i</ta>
            <ta e="T50" id="Seg_905" s="T49">čog-u-nan</ta>
            <ta e="T51" id="Seg_906" s="T50">ɨs-pɨt-ɨ-m</ta>
            <ta e="T52" id="Seg_907" s="T51">kim-i</ta>
            <ta e="T53" id="Seg_908" s="T52">eː</ta>
            <ta e="T54" id="Seg_909" s="T53">kim</ta>
            <ta e="T55" id="Seg_910" s="T54">eː</ta>
            <ta e="T56" id="Seg_911" s="T55">čok-ko</ta>
            <ta e="T57" id="Seg_912" s="T56">ɨs-pɨt-ɨ-m</ta>
            <ta e="T58" id="Seg_913" s="T57">salʼarka-nɨ</ta>
            <ta e="T59" id="Seg_914" s="T58">eː</ta>
            <ta e="T60" id="Seg_915" s="T59">salʼarka</ta>
            <ta e="T61" id="Seg_916" s="T60">bu͡ol-tak</ta>
            <ta e="T62" id="Seg_917" s="T61">kim</ta>
            <ta e="T63" id="Seg_918" s="T62">e-t-e=j</ta>
            <ta e="T64" id="Seg_919" s="T63">eː</ta>
            <ta e="T65" id="Seg_920" s="T64">kʼerasin-ɨ</ta>
            <ta e="T66" id="Seg_921" s="T65">onton</ta>
            <ta e="T67" id="Seg_922" s="T66">hɨraj-ɨ-m</ta>
            <ta e="T68" id="Seg_923" s="T67">ubaj-bɨt-a</ta>
            <ta e="T69" id="Seg_924" s="T68">onton</ta>
            <ta e="T70" id="Seg_925" s="T69">hojut</ta>
            <ta e="T71" id="Seg_926" s="T70">kim</ta>
            <ta e="T72" id="Seg_927" s="T71">dʼi͡e-biti-ger</ta>
            <ta e="T73" id="Seg_928" s="T72">tʼotʼa</ta>
            <ta e="T74" id="Seg_929" s="T73">Stʼoša-laːk</ta>
            <ta e="T75" id="Seg_930" s="T74">hüːr-en</ta>
            <ta e="T76" id="Seg_931" s="T75">bar-bɨt-ɨ-m</ta>
            <ta e="T77" id="Seg_932" s="T76">Riːta-nɨ</ta>
            <ta e="T78" id="Seg_933" s="T77">gɨtta</ta>
            <ta e="T79" id="Seg_934" s="T78">onton</ta>
            <ta e="T80" id="Seg_935" s="T79">onno</ta>
            <ta e="T81" id="Seg_936" s="T80">kimi͡e-ke</ta>
            <ta e="T82" id="Seg_937" s="T81">kim</ta>
            <ta e="T83" id="Seg_938" s="T82">e-t-e=j</ta>
            <ta e="T84" id="Seg_939" s="T83">teːte-m-eːk</ta>
            <ta e="T85" id="Seg_940" s="T84">onno</ta>
            <ta e="T86" id="Seg_941" s="T85">tɨː</ta>
            <ta e="T87" id="Seg_942" s="T86">oŋor-o</ta>
            <ta e="T88" id="Seg_943" s="T87">hɨt-aːččɨ</ta>
            <ta e="T89" id="Seg_944" s="T88">e-ti-lere</ta>
            <ta e="T90" id="Seg_945" s="T89">bi͡ek-ten</ta>
            <ta e="T91" id="Seg_946" s="T90">onton</ta>
            <ta e="T92" id="Seg_947" s="T91">onno</ta>
            <ta e="T93" id="Seg_948" s="T92">tʼotʼa</ta>
            <ta e="T94" id="Seg_949" s="T93">Stʼoša</ta>
            <ta e="T95" id="Seg_950" s="T94">di͡e-bit-e</ta>
            <ta e="T96" id="Seg_951" s="T95">eː</ta>
            <ta e="T97" id="Seg_952" s="T96">Roska</ta>
            <ta e="T98" id="Seg_953" s="T97">di͡e-bit</ta>
            <ta e="T150" id="Seg_954" s="T98">e-t-e</ta>
            <ta e="T100" id="Seg_955" s="T99">tu͡ok</ta>
            <ta e="T101" id="Seg_956" s="T100">bu͡ol-but</ta>
            <ta e="T102" id="Seg_957" s="T101">araj</ta>
            <ta e="T103" id="Seg_958" s="T102">brovʼi-lar-ɨ-ŋ</ta>
            <ta e="T104" id="Seg_959" s="T103">di͡e-bit</ta>
            <ta e="T105" id="Seg_960" s="T104">e-t-e</ta>
            <ta e="T106" id="Seg_961" s="T105">zamʼečʼaj-d-ɨː</ta>
            <ta e="T107" id="Seg_962" s="T106">ilik</ta>
            <ta e="T108" id="Seg_963" s="T107">e-t-e</ta>
            <ta e="T109" id="Seg_964" s="T108">brovʼi-lar-bɨ-n</ta>
            <ta e="T110" id="Seg_965" s="T109">onton</ta>
            <ta e="T111" id="Seg_966" s="T110">min</ta>
            <ta e="T112" id="Seg_967" s="T111">di͡e-bit-i-m</ta>
            <ta e="T113" id="Seg_968" s="T112">tu͡ok</ta>
            <ta e="T114" id="Seg_969" s="T113">da</ta>
            <ta e="T115" id="Seg_970" s="T114">di͡e</ta>
            <ta e="T116" id="Seg_971" s="T115">eː</ta>
            <ta e="T117" id="Seg_972" s="T116">tu͡ok</ta>
            <ta e="T118" id="Seg_973" s="T117">da</ta>
            <ta e="T119" id="Seg_974" s="T118">kepseː-beteg-i-m</ta>
            <ta e="T120" id="Seg_975" s="T119">onton</ta>
            <ta e="T121" id="Seg_976" s="T120">tʼotʼa</ta>
            <ta e="T122" id="Seg_977" s="T121">Stʼoša</ta>
            <ta e="T123" id="Seg_978" s="T122">ɨgɨr-bɨt-a</ta>
            <ta e="T124" id="Seg_979" s="T123">teːte-bi-n</ta>
            <ta e="T125" id="Seg_980" s="T124">onton</ta>
            <ta e="T126" id="Seg_981" s="T125">teːte-m</ta>
            <ta e="T127" id="Seg_982" s="T126">tu͡ok</ta>
            <ta e="T128" id="Seg_983" s="T127">da</ta>
            <ta e="T129" id="Seg_984" s="T128">eː</ta>
            <ta e="T130" id="Seg_985" s="T129">emi͡e</ta>
            <ta e="T131" id="Seg_986" s="T130">ɨgɨr-bɨt-a</ta>
            <ta e="T132" id="Seg_987" s="T131">teːte-bi-n</ta>
            <ta e="T133" id="Seg_988" s="T132">onton</ta>
            <ta e="T149" id="Seg_989" s="T133">bar-bɨt-a</ta>
            <ta e="T135" id="Seg_990" s="T134">tʼotʼa</ta>
            <ta e="T136" id="Seg_991" s="T135">Lʼena-laːk-ka</ta>
            <ta e="T137" id="Seg_992" s="T136">ɨgɨr-bɨt-a</ta>
            <ta e="T138" id="Seg_993" s="T137">kim-i</ta>
            <ta e="T139" id="Seg_994" s="T138">tʼotʼa</ta>
            <ta e="T140" id="Seg_995" s="T139">Lʼena</ta>
            <ta e="T141" id="Seg_996" s="T140">doːktor</ta>
            <ta e="T142" id="Seg_997" s="T141">Lʼena-nɨ</ta>
            <ta e="T143" id="Seg_998" s="T142">onton</ta>
            <ta e="T145" id="Seg_999" s="T148">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1000" s="T0">biːr</ta>
            <ta e="T2" id="Seg_1001" s="T1">biːrde</ta>
            <ta e="T3" id="Seg_1002" s="T2">kim</ta>
            <ta e="T4" id="Seg_1003" s="T3">Rita-nI</ta>
            <ta e="T5" id="Seg_1004" s="T4">kɨtta</ta>
            <ta e="T6" id="Seg_1005" s="T5">že</ta>
            <ta e="T7" id="Seg_1006" s="T6">bihigi-GA</ta>
            <ta e="T8" id="Seg_1007" s="T7">kel-BIT</ta>
            <ta e="T9" id="Seg_1008" s="T8">e-TI-BIt</ta>
            <ta e="T10" id="Seg_1009" s="T9">onton</ta>
            <ta e="T11" id="Seg_1010" s="T10">onno</ta>
            <ta e="T12" id="Seg_1011" s="T11">iti</ta>
            <ta e="T13" id="Seg_1012" s="T12">ka</ta>
            <ta e="T14" id="Seg_1013" s="T13">dʼi͡e</ta>
            <ta e="T15" id="Seg_1014" s="T14">dʼi͡e</ta>
            <ta e="T16" id="Seg_1015" s="T15">min</ta>
            <ta e="T17" id="Seg_1016" s="T16">že</ta>
            <ta e="T18" id="Seg_1017" s="T17">dʼi͡e-m</ta>
            <ta e="T19" id="Seg_1018" s="T18">tɨmnɨː</ta>
            <ta e="T20" id="Seg_1019" s="T19">e-TI-tA</ta>
            <ta e="T21" id="Seg_1020" s="T20">onton</ta>
            <ta e="T22" id="Seg_1021" s="T21">Rita</ta>
            <ta e="T23" id="Seg_1022" s="T22">onno</ta>
            <ta e="T24" id="Seg_1023" s="T23">dʼendʼi-nAn</ta>
            <ta e="T25" id="Seg_1024" s="T24">oːnnʼoː-A</ta>
            <ta e="T26" id="Seg_1025" s="T25">olor-Ar</ta>
            <ta e="T27" id="Seg_1026" s="T26">e-TI-tA</ta>
            <ta e="T28" id="Seg_1027" s="T27">onton</ta>
            <ta e="T29" id="Seg_1028" s="T28">min</ta>
            <ta e="T30" id="Seg_1029" s="T29">di͡e-BIT-I-m</ta>
            <ta e="T31" id="Seg_1030" s="T30">u͡ot-nI</ta>
            <ta e="T32" id="Seg_1031" s="T31">otut-IAK-m</ta>
            <ta e="T33" id="Seg_1032" s="T32">du͡o</ta>
            <ta e="T34" id="Seg_1033" s="T33">otun</ta>
            <ta e="T35" id="Seg_1034" s="T34">otun</ta>
            <ta e="T36" id="Seg_1035" s="T35">di͡e-BIT-tA</ta>
            <ta e="T37" id="Seg_1036" s="T36">Rita</ta>
            <ta e="T38" id="Seg_1037" s="T37">onton</ta>
            <ta e="T39" id="Seg_1038" s="T38">min</ta>
            <ta e="T40" id="Seg_1039" s="T39">bar-BIT-I-m</ta>
            <ta e="T41" id="Seg_1040" s="T147">ulakan</ta>
            <ta e="T42" id="Seg_1041" s="T41">bagajɨ</ta>
            <ta e="T43" id="Seg_1042" s="T42">čok</ta>
            <ta e="T44" id="Seg_1043" s="T43">hɨt-Ar</ta>
            <ta e="T46" id="Seg_1044" s="T44">e-TI-tA</ta>
            <ta e="T47" id="Seg_1045" s="T46">onton</ta>
            <ta e="T48" id="Seg_1046" s="T47">ol-nI</ta>
            <ta e="T49" id="Seg_1047" s="T48">kim-nI</ta>
            <ta e="T50" id="Seg_1048" s="T49">čok-tI-nAn</ta>
            <ta e="T51" id="Seg_1049" s="T50">ɨs-BIT-I-m</ta>
            <ta e="T52" id="Seg_1050" s="T51">kim-nI</ta>
            <ta e="T53" id="Seg_1051" s="T52">eː</ta>
            <ta e="T54" id="Seg_1052" s="T53">kim</ta>
            <ta e="T55" id="Seg_1053" s="T54">eː</ta>
            <ta e="T56" id="Seg_1054" s="T55">čok-GA</ta>
            <ta e="T57" id="Seg_1055" s="T56">ɨs-BIT-I-m</ta>
            <ta e="T58" id="Seg_1056" s="T57">salʼarka-nI</ta>
            <ta e="T59" id="Seg_1057" s="T58">eː</ta>
            <ta e="T60" id="Seg_1058" s="T59">salʼarka</ta>
            <ta e="T61" id="Seg_1059" s="T60">bu͡ol-BAtAK</ta>
            <ta e="T62" id="Seg_1060" s="T61">kim</ta>
            <ta e="T63" id="Seg_1061" s="T62">e-TI-tA=Ij</ta>
            <ta e="T64" id="Seg_1062" s="T63">eː</ta>
            <ta e="T65" id="Seg_1063" s="T64">kʼerasin-nI</ta>
            <ta e="T66" id="Seg_1064" s="T65">onton</ta>
            <ta e="T67" id="Seg_1065" s="T66">hɨraj-I-m</ta>
            <ta e="T68" id="Seg_1066" s="T67">ubaj-BIT-tA</ta>
            <ta e="T69" id="Seg_1067" s="T68">onton</ta>
            <ta e="T70" id="Seg_1068" s="T69">kojut</ta>
            <ta e="T71" id="Seg_1069" s="T70">kim</ta>
            <ta e="T72" id="Seg_1070" s="T71">dʼi͡e-BItI-GAr</ta>
            <ta e="T73" id="Seg_1071" s="T72">tʼotʼa</ta>
            <ta e="T74" id="Seg_1072" s="T73">Stʼoša-LAːK</ta>
            <ta e="T75" id="Seg_1073" s="T74">hüːr-An</ta>
            <ta e="T76" id="Seg_1074" s="T75">bar-BIT-I-m</ta>
            <ta e="T77" id="Seg_1075" s="T76">Rita-nI</ta>
            <ta e="T78" id="Seg_1076" s="T77">kɨtta</ta>
            <ta e="T79" id="Seg_1077" s="T78">onton</ta>
            <ta e="T80" id="Seg_1078" s="T79">onno</ta>
            <ta e="T81" id="Seg_1079" s="T80">kim-GA</ta>
            <ta e="T82" id="Seg_1080" s="T81">kim</ta>
            <ta e="T83" id="Seg_1081" s="T82">e-TI-tA=Ij</ta>
            <ta e="T84" id="Seg_1082" s="T83">teːte-m-LAːK</ta>
            <ta e="T85" id="Seg_1083" s="T84">onno</ta>
            <ta e="T86" id="Seg_1084" s="T85">tɨː</ta>
            <ta e="T87" id="Seg_1085" s="T86">oŋor-A</ta>
            <ta e="T88" id="Seg_1086" s="T87">hɨt-AːččI</ta>
            <ta e="T89" id="Seg_1087" s="T88">e-TI-LArA</ta>
            <ta e="T90" id="Seg_1088" s="T89">bi͡ek-ttAn</ta>
            <ta e="T91" id="Seg_1089" s="T90">onton</ta>
            <ta e="T92" id="Seg_1090" s="T91">onno</ta>
            <ta e="T93" id="Seg_1091" s="T92">tʼotʼa</ta>
            <ta e="T94" id="Seg_1092" s="T93">Stʼoša</ta>
            <ta e="T95" id="Seg_1093" s="T94">di͡e-BIT-tA</ta>
            <ta e="T96" id="Seg_1094" s="T95">eː</ta>
            <ta e="T97" id="Seg_1095" s="T96">Roska</ta>
            <ta e="T98" id="Seg_1096" s="T97">di͡e-BIT</ta>
            <ta e="T150" id="Seg_1097" s="T98">e-TI-tA</ta>
            <ta e="T100" id="Seg_1098" s="T99">tu͡ok</ta>
            <ta e="T101" id="Seg_1099" s="T100">bu͡ol-BIT</ta>
            <ta e="T102" id="Seg_1100" s="T101">agaj</ta>
            <ta e="T103" id="Seg_1101" s="T102">brovʼi-LAr-I-ŋ</ta>
            <ta e="T104" id="Seg_1102" s="T103">di͡e-BIT</ta>
            <ta e="T105" id="Seg_1103" s="T104">e-TI-tA</ta>
            <ta e="T106" id="Seg_1104" s="T105">zamečaj-LAː-A</ta>
            <ta e="T107" id="Seg_1105" s="T106">ilik</ta>
            <ta e="T108" id="Seg_1106" s="T107">e-TI-tA</ta>
            <ta e="T109" id="Seg_1107" s="T108">brovʼi-LAr-BI-n</ta>
            <ta e="T110" id="Seg_1108" s="T109">onton</ta>
            <ta e="T111" id="Seg_1109" s="T110">min</ta>
            <ta e="T112" id="Seg_1110" s="T111">di͡e-BIT-I-m</ta>
            <ta e="T113" id="Seg_1111" s="T112">tu͡ok</ta>
            <ta e="T114" id="Seg_1112" s="T113">da</ta>
            <ta e="T115" id="Seg_1113" s="T114">di͡e</ta>
            <ta e="T116" id="Seg_1114" s="T115">eː</ta>
            <ta e="T117" id="Seg_1115" s="T116">tu͡ok</ta>
            <ta e="T118" id="Seg_1116" s="T117">da</ta>
            <ta e="T119" id="Seg_1117" s="T118">kepseː-BAtAK-I-m</ta>
            <ta e="T120" id="Seg_1118" s="T119">onton</ta>
            <ta e="T121" id="Seg_1119" s="T120">tʼotʼa</ta>
            <ta e="T122" id="Seg_1120" s="T121">Stʼoša</ta>
            <ta e="T123" id="Seg_1121" s="T122">ɨgɨr-BIT-tA</ta>
            <ta e="T124" id="Seg_1122" s="T123">teːte-BI-n</ta>
            <ta e="T125" id="Seg_1123" s="T124">onton</ta>
            <ta e="T126" id="Seg_1124" s="T125">teːte-m</ta>
            <ta e="T127" id="Seg_1125" s="T126">tu͡ok</ta>
            <ta e="T128" id="Seg_1126" s="T127">da</ta>
            <ta e="T129" id="Seg_1127" s="T128">eː</ta>
            <ta e="T130" id="Seg_1128" s="T129">emi͡e</ta>
            <ta e="T131" id="Seg_1129" s="T130">ɨgɨr-BIT-tA</ta>
            <ta e="T132" id="Seg_1130" s="T131">teːte-BI-n</ta>
            <ta e="T133" id="Seg_1131" s="T132">onton</ta>
            <ta e="T149" id="Seg_1132" s="T133">bar-BIT-tA</ta>
            <ta e="T135" id="Seg_1133" s="T134">tʼotʼa</ta>
            <ta e="T136" id="Seg_1134" s="T135">Lʼena-LAːK-GA</ta>
            <ta e="T137" id="Seg_1135" s="T136">ɨgɨr-BIT-tA</ta>
            <ta e="T138" id="Seg_1136" s="T137">kim-nI</ta>
            <ta e="T139" id="Seg_1137" s="T138">tʼotʼa</ta>
            <ta e="T140" id="Seg_1138" s="T139">Lʼena</ta>
            <ta e="T141" id="Seg_1139" s="T140">du͡oktuːr</ta>
            <ta e="T142" id="Seg_1140" s="T141">Lʼena-nI</ta>
            <ta e="T143" id="Seg_1141" s="T142">onton</ta>
            <ta e="T145" id="Seg_1142" s="T148">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1143" s="T0">one</ta>
            <ta e="T2" id="Seg_1144" s="T1">once</ta>
            <ta e="T3" id="Seg_1145" s="T2">who.[NOM]</ta>
            <ta e="T4" id="Seg_1146" s="T3">Rita-ACC</ta>
            <ta e="T5" id="Seg_1147" s="T4">with</ta>
            <ta e="T6" id="Seg_1148" s="T5">EMPH</ta>
            <ta e="T7" id="Seg_1149" s="T6">1PL-DAT/LOC</ta>
            <ta e="T8" id="Seg_1150" s="T7">come-PTCP.PST</ta>
            <ta e="T9" id="Seg_1151" s="T8">be-PST1-1PL</ta>
            <ta e="T10" id="Seg_1152" s="T9">then</ta>
            <ta e="T11" id="Seg_1153" s="T10">there</ta>
            <ta e="T12" id="Seg_1154" s="T11">that.[NOM]</ta>
            <ta e="T13" id="Seg_1155" s="T12">well</ta>
            <ta e="T14" id="Seg_1156" s="T13">house.[NOM]</ta>
            <ta e="T15" id="Seg_1157" s="T14">house.[NOM]</ta>
            <ta e="T16" id="Seg_1158" s="T15">1SG.[NOM]</ta>
            <ta e="T17" id="Seg_1159" s="T16">EMPH</ta>
            <ta e="T18" id="Seg_1160" s="T17">house-1SG.[NOM]</ta>
            <ta e="T19" id="Seg_1161" s="T18">cold.[NOM]</ta>
            <ta e="T20" id="Seg_1162" s="T19">be-PST1-3SG</ta>
            <ta e="T21" id="Seg_1163" s="T20">then</ta>
            <ta e="T22" id="Seg_1164" s="T21">Rita.[NOM]</ta>
            <ta e="T23" id="Seg_1165" s="T22">there</ta>
            <ta e="T24" id="Seg_1166" s="T23">Dendy-INSTR</ta>
            <ta e="T25" id="Seg_1167" s="T24">play-CVB.SIM</ta>
            <ta e="T26" id="Seg_1168" s="T25">sit-PTCP.PRS</ta>
            <ta e="T27" id="Seg_1169" s="T26">be-PST1-3SG</ta>
            <ta e="T28" id="Seg_1170" s="T27">then</ta>
            <ta e="T29" id="Seg_1171" s="T28">1SG.[NOM]</ta>
            <ta e="T30" id="Seg_1172" s="T29">say-PST2-EP-1SG</ta>
            <ta e="T31" id="Seg_1173" s="T30">fire-ACC</ta>
            <ta e="T32" id="Seg_1174" s="T31">light-FUT-1SG</ta>
            <ta e="T33" id="Seg_1175" s="T32">Q</ta>
            <ta e="T34" id="Seg_1176" s="T33">heat.[IMP.2SG]</ta>
            <ta e="T35" id="Seg_1177" s="T34">heat.[IMP.2SG]</ta>
            <ta e="T36" id="Seg_1178" s="T35">say-PST2-3SG</ta>
            <ta e="T37" id="Seg_1179" s="T36">Rita.[NOM]</ta>
            <ta e="T38" id="Seg_1180" s="T37">then</ta>
            <ta e="T39" id="Seg_1181" s="T38">1SG.[NOM]</ta>
            <ta e="T40" id="Seg_1182" s="T39">go-PST2-EP-1SG</ta>
            <ta e="T41" id="Seg_1183" s="T147">big</ta>
            <ta e="T42" id="Seg_1184" s="T41">very</ta>
            <ta e="T43" id="Seg_1185" s="T42">coal.[NOM]</ta>
            <ta e="T44" id="Seg_1186" s="T43">lie-PTCP.PRS</ta>
            <ta e="T46" id="Seg_1187" s="T44">be-PST1-3SG</ta>
            <ta e="T47" id="Seg_1188" s="T46">then</ta>
            <ta e="T48" id="Seg_1189" s="T47">that-ACC</ta>
            <ta e="T49" id="Seg_1190" s="T48">who-ACC</ta>
            <ta e="T50" id="Seg_1191" s="T49">coal-3SG-INSTR</ta>
            <ta e="T51" id="Seg_1192" s="T50">splash-PST2-EP-1SG</ta>
            <ta e="T52" id="Seg_1193" s="T51">who-ACC</ta>
            <ta e="T53" id="Seg_1194" s="T52">eh</ta>
            <ta e="T54" id="Seg_1195" s="T53">who.[NOM]</ta>
            <ta e="T55" id="Seg_1196" s="T54">eh</ta>
            <ta e="T56" id="Seg_1197" s="T55">coal-DAT/LOC</ta>
            <ta e="T57" id="Seg_1198" s="T56">splash-PST2-EP-1SG</ta>
            <ta e="T58" id="Seg_1199" s="T57">solar.oil-ACC</ta>
            <ta e="T59" id="Seg_1200" s="T58">eh</ta>
            <ta e="T60" id="Seg_1201" s="T59">solar.oil.[NOM]</ta>
            <ta e="T61" id="Seg_1202" s="T60">be-PST2.NEG.[3SG]</ta>
            <ta e="T62" id="Seg_1203" s="T61">who.[NOM]</ta>
            <ta e="T63" id="Seg_1204" s="T62">be-PST1-3SG=Q</ta>
            <ta e="T64" id="Seg_1205" s="T63">eh</ta>
            <ta e="T65" id="Seg_1206" s="T64">kerosine-ACC</ta>
            <ta e="T66" id="Seg_1207" s="T65">then</ta>
            <ta e="T67" id="Seg_1208" s="T66">face-EP-1SG.[NOM]</ta>
            <ta e="T68" id="Seg_1209" s="T67">flame.up-PST2-3SG</ta>
            <ta e="T69" id="Seg_1210" s="T68">then</ta>
            <ta e="T70" id="Seg_1211" s="T69">late</ta>
            <ta e="T71" id="Seg_1212" s="T70">who.[NOM]</ta>
            <ta e="T72" id="Seg_1213" s="T71">house-1PL-DAT/LOC</ta>
            <ta e="T73" id="Seg_1214" s="T72">aunt</ta>
            <ta e="T74" id="Seg_1215" s="T73">Styosha-PROPR</ta>
            <ta e="T75" id="Seg_1216" s="T74">run-CVB.SEQ</ta>
            <ta e="T76" id="Seg_1217" s="T75">go-PST2-EP-1SG</ta>
            <ta e="T77" id="Seg_1218" s="T76">Rita-ACC</ta>
            <ta e="T78" id="Seg_1219" s="T77">with</ta>
            <ta e="T79" id="Seg_1220" s="T78">then</ta>
            <ta e="T80" id="Seg_1221" s="T79">there</ta>
            <ta e="T81" id="Seg_1222" s="T80">who-DAT/LOC</ta>
            <ta e="T82" id="Seg_1223" s="T81">who.[NOM]</ta>
            <ta e="T83" id="Seg_1224" s="T82">be-PST1-3SG=Q</ta>
            <ta e="T84" id="Seg_1225" s="T83">father-1SG-PROPR.[NOM]</ta>
            <ta e="T85" id="Seg_1226" s="T84">there</ta>
            <ta e="T86" id="Seg_1227" s="T85">small.boat.[NOM]</ta>
            <ta e="T87" id="Seg_1228" s="T86">make-CVB.SIM</ta>
            <ta e="T88" id="Seg_1229" s="T87">lie-PTCP.HAB</ta>
            <ta e="T89" id="Seg_1230" s="T88">be-PST1-3PL</ta>
            <ta e="T90" id="Seg_1231" s="T89">always-ABL</ta>
            <ta e="T91" id="Seg_1232" s="T90">then</ta>
            <ta e="T92" id="Seg_1233" s="T91">there</ta>
            <ta e="T93" id="Seg_1234" s="T92">aunt</ta>
            <ta e="T94" id="Seg_1235" s="T93">Styosha.[NOM]</ta>
            <ta e="T95" id="Seg_1236" s="T94">say-PST2-3SG</ta>
            <ta e="T96" id="Seg_1237" s="T95">eh</ta>
            <ta e="T97" id="Seg_1238" s="T96">Roska.[NOM]</ta>
            <ta e="T98" id="Seg_1239" s="T97">say-PTCP.PST</ta>
            <ta e="T150" id="Seg_1240" s="T98">be-PST1-3SG</ta>
            <ta e="T100" id="Seg_1241" s="T99">what.[NOM]</ta>
            <ta e="T101" id="Seg_1242" s="T100">become-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_1243" s="T101">only</ta>
            <ta e="T103" id="Seg_1244" s="T102">eyebrow.PL-PL-EP-2SG.[NOM]</ta>
            <ta e="T104" id="Seg_1245" s="T103">say-PTCP.PST</ta>
            <ta e="T105" id="Seg_1246" s="T104">be-PST1-3SG</ta>
            <ta e="T106" id="Seg_1247" s="T105">notice-VBZ-CVB.SIM</ta>
            <ta e="T107" id="Seg_1248" s="T106">not.yet</ta>
            <ta e="T108" id="Seg_1249" s="T107">be-PST1-3SG</ta>
            <ta e="T109" id="Seg_1250" s="T108">eyebrow.PL-PL-1SG-ACC</ta>
            <ta e="T110" id="Seg_1251" s="T109">then</ta>
            <ta e="T111" id="Seg_1252" s="T110">1SG.[NOM]</ta>
            <ta e="T112" id="Seg_1253" s="T111">say-PST2-EP-1SG</ta>
            <ta e="T113" id="Seg_1254" s="T112">what.[NOM]</ta>
            <ta e="T114" id="Seg_1255" s="T113">NEG</ta>
            <ta e="T115" id="Seg_1256" s="T114">say</ta>
            <ta e="T116" id="Seg_1257" s="T115">eh</ta>
            <ta e="T117" id="Seg_1258" s="T116">what.[NOM]</ta>
            <ta e="T118" id="Seg_1259" s="T117">NEG</ta>
            <ta e="T119" id="Seg_1260" s="T118">tell-PST2.NEG-EP-1SG</ta>
            <ta e="T120" id="Seg_1261" s="T119">then</ta>
            <ta e="T121" id="Seg_1262" s="T120">aunt</ta>
            <ta e="T122" id="Seg_1263" s="T121">Styosha.[NOM]</ta>
            <ta e="T123" id="Seg_1264" s="T122">call-PST2-3SG</ta>
            <ta e="T124" id="Seg_1265" s="T123">father-1SG-ACC</ta>
            <ta e="T125" id="Seg_1266" s="T124">then</ta>
            <ta e="T126" id="Seg_1267" s="T125">father-1SG.[NOM]</ta>
            <ta e="T127" id="Seg_1268" s="T126">what.[NOM]</ta>
            <ta e="T128" id="Seg_1269" s="T127">NEG</ta>
            <ta e="T129" id="Seg_1270" s="T128">eh</ta>
            <ta e="T130" id="Seg_1271" s="T129">again</ta>
            <ta e="T131" id="Seg_1272" s="T130">call-PST2-3SG</ta>
            <ta e="T132" id="Seg_1273" s="T131">father-1SG-ACC</ta>
            <ta e="T133" id="Seg_1274" s="T132">then</ta>
            <ta e="T149" id="Seg_1275" s="T133">go-PST2-3SG</ta>
            <ta e="T135" id="Seg_1276" s="T134">aunt</ta>
            <ta e="T136" id="Seg_1277" s="T135">Lena-PROPR-DAT/LOC</ta>
            <ta e="T137" id="Seg_1278" s="T136">call-PST2-3SG</ta>
            <ta e="T138" id="Seg_1279" s="T137">who-ACC</ta>
            <ta e="T139" id="Seg_1280" s="T138">aunt</ta>
            <ta e="T140" id="Seg_1281" s="T139">Lena.[NOM]</ta>
            <ta e="T141" id="Seg_1282" s="T140">doctor.[NOM]</ta>
            <ta e="T142" id="Seg_1283" s="T141">Lena-ACC</ta>
            <ta e="T143" id="Seg_1284" s="T142">then</ta>
            <ta e="T145" id="Seg_1285" s="T148">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_1286" s="T0">eins</ta>
            <ta e="T2" id="Seg_1287" s="T1">einmal</ta>
            <ta e="T3" id="Seg_1288" s="T2">wer.[NOM]</ta>
            <ta e="T4" id="Seg_1289" s="T3">Rita-ACC</ta>
            <ta e="T5" id="Seg_1290" s="T4">mit</ta>
            <ta e="T6" id="Seg_1291" s="T5">EMPH</ta>
            <ta e="T7" id="Seg_1292" s="T6">1PL-DAT/LOC</ta>
            <ta e="T8" id="Seg_1293" s="T7">kommen-PTCP.PST</ta>
            <ta e="T9" id="Seg_1294" s="T8">sein-PST1-1PL</ta>
            <ta e="T10" id="Seg_1295" s="T9">dann</ta>
            <ta e="T11" id="Seg_1296" s="T10">dort</ta>
            <ta e="T12" id="Seg_1297" s="T11">dieses.[NOM]</ta>
            <ta e="T13" id="Seg_1298" s="T12">nun</ta>
            <ta e="T14" id="Seg_1299" s="T13">Haus.[NOM]</ta>
            <ta e="T15" id="Seg_1300" s="T14">Haus.[NOM]</ta>
            <ta e="T16" id="Seg_1301" s="T15">1SG.[NOM]</ta>
            <ta e="T17" id="Seg_1302" s="T16">EMPH</ta>
            <ta e="T18" id="Seg_1303" s="T17">Haus-1SG.[NOM]</ta>
            <ta e="T19" id="Seg_1304" s="T18">kalt.[NOM]</ta>
            <ta e="T20" id="Seg_1305" s="T19">sein-PST1-3SG</ta>
            <ta e="T21" id="Seg_1306" s="T20">dann</ta>
            <ta e="T22" id="Seg_1307" s="T21">Rita.[NOM]</ta>
            <ta e="T23" id="Seg_1308" s="T22">dort</ta>
            <ta e="T24" id="Seg_1309" s="T23">Dendy-INSTR</ta>
            <ta e="T25" id="Seg_1310" s="T24">spielen-CVB.SIM</ta>
            <ta e="T26" id="Seg_1311" s="T25">sitzen-PTCP.PRS</ta>
            <ta e="T27" id="Seg_1312" s="T26">sein-PST1-3SG</ta>
            <ta e="T28" id="Seg_1313" s="T27">dann</ta>
            <ta e="T29" id="Seg_1314" s="T28">1SG.[NOM]</ta>
            <ta e="T30" id="Seg_1315" s="T29">sagen-PST2-EP-1SG</ta>
            <ta e="T31" id="Seg_1316" s="T30">Feuer-ACC</ta>
            <ta e="T32" id="Seg_1317" s="T31">anzünden-FUT-1SG</ta>
            <ta e="T33" id="Seg_1318" s="T32">Q</ta>
            <ta e="T34" id="Seg_1319" s="T33">heizen.[IMP.2SG]</ta>
            <ta e="T35" id="Seg_1320" s="T34">heizen.[IMP.2SG]</ta>
            <ta e="T36" id="Seg_1321" s="T35">sagen-PST2-3SG</ta>
            <ta e="T37" id="Seg_1322" s="T36">Rita.[NOM]</ta>
            <ta e="T38" id="Seg_1323" s="T37">dann</ta>
            <ta e="T39" id="Seg_1324" s="T38">1SG.[NOM]</ta>
            <ta e="T40" id="Seg_1325" s="T39">gehen-PST2-EP-1SG</ta>
            <ta e="T41" id="Seg_1326" s="T147">groß</ta>
            <ta e="T42" id="Seg_1327" s="T41">sehr</ta>
            <ta e="T43" id="Seg_1328" s="T42">Kohle.[NOM]</ta>
            <ta e="T44" id="Seg_1329" s="T43">liegen-PTCP.PRS</ta>
            <ta e="T46" id="Seg_1330" s="T44">sein-PST1-3SG</ta>
            <ta e="T47" id="Seg_1331" s="T46">dann</ta>
            <ta e="T48" id="Seg_1332" s="T47">jenes-ACC</ta>
            <ta e="T49" id="Seg_1333" s="T48">wer-ACC</ta>
            <ta e="T50" id="Seg_1334" s="T49">Kohle-3SG-INSTR</ta>
            <ta e="T51" id="Seg_1335" s="T50">spritzen-PST2-EP-1SG</ta>
            <ta e="T52" id="Seg_1336" s="T51">wer-ACC</ta>
            <ta e="T53" id="Seg_1337" s="T52">äh</ta>
            <ta e="T54" id="Seg_1338" s="T53">wer.[NOM]</ta>
            <ta e="T55" id="Seg_1339" s="T54">äh</ta>
            <ta e="T56" id="Seg_1340" s="T55">Kohle-DAT/LOC</ta>
            <ta e="T57" id="Seg_1341" s="T56">spritzen-PST2-EP-1SG</ta>
            <ta e="T58" id="Seg_1342" s="T57">Solaröl-ACC</ta>
            <ta e="T59" id="Seg_1343" s="T58">äh</ta>
            <ta e="T60" id="Seg_1344" s="T59">Solaröl.[NOM]</ta>
            <ta e="T61" id="Seg_1345" s="T60">sein-PST2.NEG.[3SG]</ta>
            <ta e="T62" id="Seg_1346" s="T61">wer.[NOM]</ta>
            <ta e="T63" id="Seg_1347" s="T62">sein-PST1-3SG=Q</ta>
            <ta e="T64" id="Seg_1348" s="T63">äh</ta>
            <ta e="T65" id="Seg_1349" s="T64">Kerosin-ACC</ta>
            <ta e="T66" id="Seg_1350" s="T65">dann</ta>
            <ta e="T67" id="Seg_1351" s="T66">Gesicht-EP-1SG.[NOM]</ta>
            <ta e="T68" id="Seg_1352" s="T67">aufflammen-PST2-3SG</ta>
            <ta e="T69" id="Seg_1353" s="T68">dann</ta>
            <ta e="T70" id="Seg_1354" s="T69">spät</ta>
            <ta e="T71" id="Seg_1355" s="T70">wer.[NOM]</ta>
            <ta e="T72" id="Seg_1356" s="T71">Haus-1PL-DAT/LOC</ta>
            <ta e="T73" id="Seg_1357" s="T72">Tante</ta>
            <ta e="T74" id="Seg_1358" s="T73">Stjoscha-PROPR</ta>
            <ta e="T75" id="Seg_1359" s="T74">laufen-CVB.SEQ</ta>
            <ta e="T76" id="Seg_1360" s="T75">gehen-PST2-EP-1SG</ta>
            <ta e="T77" id="Seg_1361" s="T76">Rita-ACC</ta>
            <ta e="T78" id="Seg_1362" s="T77">mit</ta>
            <ta e="T79" id="Seg_1363" s="T78">dann</ta>
            <ta e="T80" id="Seg_1364" s="T79">dort</ta>
            <ta e="T81" id="Seg_1365" s="T80">wer-DAT/LOC</ta>
            <ta e="T82" id="Seg_1366" s="T81">wer.[NOM]</ta>
            <ta e="T83" id="Seg_1367" s="T82">sein-PST1-3SG=Q</ta>
            <ta e="T84" id="Seg_1368" s="T83">Vater-1SG-PROPR.[NOM]</ta>
            <ta e="T85" id="Seg_1369" s="T84">dort</ta>
            <ta e="T86" id="Seg_1370" s="T85">kleines.Boot.[NOM]</ta>
            <ta e="T87" id="Seg_1371" s="T86">machen-CVB.SIM</ta>
            <ta e="T88" id="Seg_1372" s="T87">liegen-PTCP.HAB</ta>
            <ta e="T89" id="Seg_1373" s="T88">sein-PST1-3PL</ta>
            <ta e="T90" id="Seg_1374" s="T89">immer-ABL</ta>
            <ta e="T91" id="Seg_1375" s="T90">dann</ta>
            <ta e="T92" id="Seg_1376" s="T91">dort</ta>
            <ta e="T93" id="Seg_1377" s="T92">Tante</ta>
            <ta e="T94" id="Seg_1378" s="T93">Stjoscha.[NOM]</ta>
            <ta e="T95" id="Seg_1379" s="T94">sagen-PST2-3SG</ta>
            <ta e="T96" id="Seg_1380" s="T95">äh</ta>
            <ta e="T97" id="Seg_1381" s="T96">Roska.[NOM]</ta>
            <ta e="T98" id="Seg_1382" s="T97">sagen-PTCP.PST</ta>
            <ta e="T150" id="Seg_1383" s="T98">sein-PST1-3SG</ta>
            <ta e="T100" id="Seg_1384" s="T99">was.[NOM]</ta>
            <ta e="T101" id="Seg_1385" s="T100">werden-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_1386" s="T101">nur</ta>
            <ta e="T103" id="Seg_1387" s="T102">Augenbraue.PL-PL-EP-2SG.[NOM]</ta>
            <ta e="T104" id="Seg_1388" s="T103">sagen-PTCP.PST</ta>
            <ta e="T105" id="Seg_1389" s="T104">sein-PST1-3SG</ta>
            <ta e="T106" id="Seg_1390" s="T105">bemerken-VBZ-CVB.SIM</ta>
            <ta e="T107" id="Seg_1391" s="T106">noch.nicht</ta>
            <ta e="T108" id="Seg_1392" s="T107">sein-PST1-3SG</ta>
            <ta e="T109" id="Seg_1393" s="T108">Augenbraue.PL-PL-1SG-ACC</ta>
            <ta e="T110" id="Seg_1394" s="T109">dann</ta>
            <ta e="T111" id="Seg_1395" s="T110">1SG.[NOM]</ta>
            <ta e="T112" id="Seg_1396" s="T111">sagen-PST2-EP-1SG</ta>
            <ta e="T113" id="Seg_1397" s="T112">was.[NOM]</ta>
            <ta e="T114" id="Seg_1398" s="T113">NEG</ta>
            <ta e="T115" id="Seg_1399" s="T114">sagen</ta>
            <ta e="T116" id="Seg_1400" s="T115">äh</ta>
            <ta e="T117" id="Seg_1401" s="T116">was.[NOM]</ta>
            <ta e="T118" id="Seg_1402" s="T117">NEG</ta>
            <ta e="T119" id="Seg_1403" s="T118">erzählen-PST2.NEG-EP-1SG</ta>
            <ta e="T120" id="Seg_1404" s="T119">dann</ta>
            <ta e="T121" id="Seg_1405" s="T120">Tante</ta>
            <ta e="T122" id="Seg_1406" s="T121">Stjoscha.[NOM]</ta>
            <ta e="T123" id="Seg_1407" s="T122">rufen-PST2-3SG</ta>
            <ta e="T124" id="Seg_1408" s="T123">Vater-1SG-ACC</ta>
            <ta e="T125" id="Seg_1409" s="T124">dann</ta>
            <ta e="T126" id="Seg_1410" s="T125">Vater-1SG.[NOM]</ta>
            <ta e="T127" id="Seg_1411" s="T126">was.[NOM]</ta>
            <ta e="T128" id="Seg_1412" s="T127">NEG</ta>
            <ta e="T129" id="Seg_1413" s="T128">äh</ta>
            <ta e="T130" id="Seg_1414" s="T129">wieder</ta>
            <ta e="T131" id="Seg_1415" s="T130">rufen-PST2-3SG</ta>
            <ta e="T132" id="Seg_1416" s="T131">Vater-1SG-ACC</ta>
            <ta e="T133" id="Seg_1417" s="T132">dann</ta>
            <ta e="T149" id="Seg_1418" s="T133">gehen-PST2-3SG</ta>
            <ta e="T135" id="Seg_1419" s="T134">Tante</ta>
            <ta e="T136" id="Seg_1420" s="T135">Lena-PROPR-DAT/LOC</ta>
            <ta e="T137" id="Seg_1421" s="T136">rufen-PST2-3SG</ta>
            <ta e="T138" id="Seg_1422" s="T137">wer-ACC</ta>
            <ta e="T139" id="Seg_1423" s="T138">Tante</ta>
            <ta e="T140" id="Seg_1424" s="T139">Lena.[NOM]</ta>
            <ta e="T141" id="Seg_1425" s="T140">Doktor.[NOM]</ta>
            <ta e="T142" id="Seg_1426" s="T141">Lena-ACC</ta>
            <ta e="T143" id="Seg_1427" s="T142">dann</ta>
            <ta e="T145" id="Seg_1428" s="T148">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1429" s="T0">один</ta>
            <ta e="T2" id="Seg_1430" s="T1">однажды</ta>
            <ta e="T3" id="Seg_1431" s="T2">кто.[NOM]</ta>
            <ta e="T4" id="Seg_1432" s="T3">Рита-ACC</ta>
            <ta e="T5" id="Seg_1433" s="T4">с</ta>
            <ta e="T6" id="Seg_1434" s="T5">EMPH</ta>
            <ta e="T7" id="Seg_1435" s="T6">1PL-DAT/LOC</ta>
            <ta e="T8" id="Seg_1436" s="T7">приходить-PTCP.PST</ta>
            <ta e="T9" id="Seg_1437" s="T8">быть-PST1-1PL</ta>
            <ta e="T10" id="Seg_1438" s="T9">потом</ta>
            <ta e="T11" id="Seg_1439" s="T10">там</ta>
            <ta e="T12" id="Seg_1440" s="T11">тот.[NOM]</ta>
            <ta e="T13" id="Seg_1441" s="T12">вот</ta>
            <ta e="T14" id="Seg_1442" s="T13">дом.[NOM]</ta>
            <ta e="T15" id="Seg_1443" s="T14">дом.[NOM]</ta>
            <ta e="T16" id="Seg_1444" s="T15">1SG.[NOM]</ta>
            <ta e="T17" id="Seg_1445" s="T16">EMPH</ta>
            <ta e="T18" id="Seg_1446" s="T17">дом-1SG.[NOM]</ta>
            <ta e="T19" id="Seg_1447" s="T18">холодний.[NOM]</ta>
            <ta e="T20" id="Seg_1448" s="T19">быть-PST1-3SG</ta>
            <ta e="T21" id="Seg_1449" s="T20">потом</ta>
            <ta e="T22" id="Seg_1450" s="T21">Рита.[NOM]</ta>
            <ta e="T23" id="Seg_1451" s="T22">там</ta>
            <ta e="T24" id="Seg_1452" s="T23">Денды-INSTR</ta>
            <ta e="T25" id="Seg_1453" s="T24">играть-CVB.SIM</ta>
            <ta e="T26" id="Seg_1454" s="T25">сидеть-PTCP.PRS</ta>
            <ta e="T27" id="Seg_1455" s="T26">быть-PST1-3SG</ta>
            <ta e="T28" id="Seg_1456" s="T27">потом</ta>
            <ta e="T29" id="Seg_1457" s="T28">1SG.[NOM]</ta>
            <ta e="T30" id="Seg_1458" s="T29">говорить-PST2-EP-1SG</ta>
            <ta e="T31" id="Seg_1459" s="T30">огонь-ACC</ta>
            <ta e="T32" id="Seg_1460" s="T31">зажигать-FUT-1SG</ta>
            <ta e="T33" id="Seg_1461" s="T32">Q</ta>
            <ta e="T34" id="Seg_1462" s="T33">топить.[IMP.2SG]</ta>
            <ta e="T35" id="Seg_1463" s="T34">топить.[IMP.2SG]</ta>
            <ta e="T36" id="Seg_1464" s="T35">говорить-PST2-3SG</ta>
            <ta e="T37" id="Seg_1465" s="T36">Рита.[NOM]</ta>
            <ta e="T38" id="Seg_1466" s="T37">потом</ta>
            <ta e="T39" id="Seg_1467" s="T38">1SG.[NOM]</ta>
            <ta e="T40" id="Seg_1468" s="T39">идти-PST2-EP-1SG</ta>
            <ta e="T41" id="Seg_1469" s="T147">большой</ta>
            <ta e="T42" id="Seg_1470" s="T41">очень</ta>
            <ta e="T43" id="Seg_1471" s="T42">уголь.[NOM]</ta>
            <ta e="T44" id="Seg_1472" s="T43">лежать-PTCP.PRS</ta>
            <ta e="T46" id="Seg_1473" s="T44">быть-PST1-3SG</ta>
            <ta e="T47" id="Seg_1474" s="T46">потом</ta>
            <ta e="T48" id="Seg_1475" s="T47">тот-ACC</ta>
            <ta e="T49" id="Seg_1476" s="T48">кто-ACC</ta>
            <ta e="T50" id="Seg_1477" s="T49">уголь-3SG-INSTR</ta>
            <ta e="T51" id="Seg_1478" s="T50">брызгать-PST2-EP-1SG</ta>
            <ta e="T52" id="Seg_1479" s="T51">кто-ACC</ta>
            <ta e="T53" id="Seg_1480" s="T52">ээ</ta>
            <ta e="T54" id="Seg_1481" s="T53">кто.[NOM]</ta>
            <ta e="T55" id="Seg_1482" s="T54">ээ</ta>
            <ta e="T56" id="Seg_1483" s="T55">уголь-DAT/LOC</ta>
            <ta e="T57" id="Seg_1484" s="T56">брызгать-PST2-EP-1SG</ta>
            <ta e="T58" id="Seg_1485" s="T57">солярка-ACC</ta>
            <ta e="T59" id="Seg_1486" s="T58">ээ</ta>
            <ta e="T60" id="Seg_1487" s="T59">солярка.[NOM]</ta>
            <ta e="T61" id="Seg_1488" s="T60">быть-PST2.NEG.[3SG]</ta>
            <ta e="T62" id="Seg_1489" s="T61">кто.[NOM]</ta>
            <ta e="T63" id="Seg_1490" s="T62">быть-PST1-3SG=Q</ta>
            <ta e="T64" id="Seg_1491" s="T63">ээ</ta>
            <ta e="T65" id="Seg_1492" s="T64">керосин-ACC</ta>
            <ta e="T66" id="Seg_1493" s="T65">потом</ta>
            <ta e="T67" id="Seg_1494" s="T66">лицо-EP-1SG.[NOM]</ta>
            <ta e="T68" id="Seg_1495" s="T67">загораться-PST2-3SG</ta>
            <ta e="T69" id="Seg_1496" s="T68">потом</ta>
            <ta e="T70" id="Seg_1497" s="T69">поздний</ta>
            <ta e="T71" id="Seg_1498" s="T70">кто.[NOM]</ta>
            <ta e="T72" id="Seg_1499" s="T71">дом-1PL-DAT/LOC</ta>
            <ta e="T73" id="Seg_1500" s="T72">тетя</ta>
            <ta e="T74" id="Seg_1501" s="T73">Стеша-PROPR</ta>
            <ta e="T75" id="Seg_1502" s="T74">бегать-CVB.SEQ</ta>
            <ta e="T76" id="Seg_1503" s="T75">идти-PST2-EP-1SG</ta>
            <ta e="T77" id="Seg_1504" s="T76">Рита-ACC</ta>
            <ta e="T78" id="Seg_1505" s="T77">с</ta>
            <ta e="T79" id="Seg_1506" s="T78">потом</ta>
            <ta e="T80" id="Seg_1507" s="T79">там</ta>
            <ta e="T81" id="Seg_1508" s="T80">кто-DAT/LOC</ta>
            <ta e="T82" id="Seg_1509" s="T81">кто.[NOM]</ta>
            <ta e="T83" id="Seg_1510" s="T82">быть-PST1-3SG=Q</ta>
            <ta e="T84" id="Seg_1511" s="T83">отец-1SG-PROPR.[NOM]</ta>
            <ta e="T85" id="Seg_1512" s="T84">там</ta>
            <ta e="T86" id="Seg_1513" s="T85">лодочка.[NOM]</ta>
            <ta e="T87" id="Seg_1514" s="T86">делать-CVB.SIM</ta>
            <ta e="T88" id="Seg_1515" s="T87">лежать-PTCP.HAB</ta>
            <ta e="T89" id="Seg_1516" s="T88">быть-PST1-3PL</ta>
            <ta e="T90" id="Seg_1517" s="T89">всегда-ABL</ta>
            <ta e="T91" id="Seg_1518" s="T90">потом</ta>
            <ta e="T92" id="Seg_1519" s="T91">там</ta>
            <ta e="T93" id="Seg_1520" s="T92">тетя</ta>
            <ta e="T94" id="Seg_1521" s="T93">Стеша.[NOM]</ta>
            <ta e="T95" id="Seg_1522" s="T94">говорить-PST2-3SG</ta>
            <ta e="T96" id="Seg_1523" s="T95">ээ</ta>
            <ta e="T97" id="Seg_1524" s="T96">Роска.[NOM]</ta>
            <ta e="T98" id="Seg_1525" s="T97">говорить-PTCP.PST</ta>
            <ta e="T150" id="Seg_1526" s="T98">быть-PST1-3SG</ta>
            <ta e="T100" id="Seg_1527" s="T99">что.[NOM]</ta>
            <ta e="T101" id="Seg_1528" s="T100">становиться-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_1529" s="T101">только</ta>
            <ta e="T103" id="Seg_1530" s="T102">бровь.PL-PL-EP-2SG.[NOM]</ta>
            <ta e="T104" id="Seg_1531" s="T103">говорить-PTCP.PST</ta>
            <ta e="T105" id="Seg_1532" s="T104">быть-PST1-3SG</ta>
            <ta e="T106" id="Seg_1533" s="T105">замечать-VBZ-CVB.SIM</ta>
            <ta e="T107" id="Seg_1534" s="T106">еще.не</ta>
            <ta e="T108" id="Seg_1535" s="T107">быть-PST1-3SG</ta>
            <ta e="T109" id="Seg_1536" s="T108">бровь.PL-PL-1SG-ACC</ta>
            <ta e="T110" id="Seg_1537" s="T109">потом</ta>
            <ta e="T111" id="Seg_1538" s="T110">1SG.[NOM]</ta>
            <ta e="T112" id="Seg_1539" s="T111">говорить-PST2-EP-1SG</ta>
            <ta e="T113" id="Seg_1540" s="T112">что.[NOM]</ta>
            <ta e="T114" id="Seg_1541" s="T113">NEG</ta>
            <ta e="T115" id="Seg_1542" s="T114">говорить</ta>
            <ta e="T116" id="Seg_1543" s="T115">ээ</ta>
            <ta e="T117" id="Seg_1544" s="T116">что.[NOM]</ta>
            <ta e="T118" id="Seg_1545" s="T117">NEG</ta>
            <ta e="T119" id="Seg_1546" s="T118">рассказывать-PST2.NEG-EP-1SG</ta>
            <ta e="T120" id="Seg_1547" s="T119">потом</ta>
            <ta e="T121" id="Seg_1548" s="T120">тетя</ta>
            <ta e="T122" id="Seg_1549" s="T121">Стеша.[NOM]</ta>
            <ta e="T123" id="Seg_1550" s="T122">звать-PST2-3SG</ta>
            <ta e="T124" id="Seg_1551" s="T123">отец-1SG-ACC</ta>
            <ta e="T125" id="Seg_1552" s="T124">потом</ta>
            <ta e="T126" id="Seg_1553" s="T125">отец-1SG.[NOM]</ta>
            <ta e="T127" id="Seg_1554" s="T126">что.[NOM]</ta>
            <ta e="T128" id="Seg_1555" s="T127">NEG</ta>
            <ta e="T129" id="Seg_1556" s="T128">ээ</ta>
            <ta e="T130" id="Seg_1557" s="T129">опять</ta>
            <ta e="T131" id="Seg_1558" s="T130">звать-PST2-3SG</ta>
            <ta e="T132" id="Seg_1559" s="T131">отец-1SG-ACC</ta>
            <ta e="T133" id="Seg_1560" s="T132">потом</ta>
            <ta e="T149" id="Seg_1561" s="T133">идти-PST2-3SG</ta>
            <ta e="T135" id="Seg_1562" s="T134">тетя</ta>
            <ta e="T136" id="Seg_1563" s="T135">Лена-PROPR-DAT/LOC</ta>
            <ta e="T137" id="Seg_1564" s="T136">звать-PST2-3SG</ta>
            <ta e="T138" id="Seg_1565" s="T137">кто-ACC</ta>
            <ta e="T139" id="Seg_1566" s="T138">тетя</ta>
            <ta e="T140" id="Seg_1567" s="T139">Лена.[NOM]</ta>
            <ta e="T141" id="Seg_1568" s="T140">доктор.[NOM]</ta>
            <ta e="T142" id="Seg_1569" s="T141">Лена-ACC</ta>
            <ta e="T143" id="Seg_1570" s="T142">потом</ta>
            <ta e="T145" id="Seg_1571" s="T148">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1572" s="T0">cardnum</ta>
            <ta e="T2" id="Seg_1573" s="T1">adv</ta>
            <ta e="T3" id="Seg_1574" s="T2">que-pro:case</ta>
            <ta e="T4" id="Seg_1575" s="T3">propr-n:case</ta>
            <ta e="T5" id="Seg_1576" s="T4">post</ta>
            <ta e="T6" id="Seg_1577" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_1578" s="T6">pers-pro:case</ta>
            <ta e="T8" id="Seg_1579" s="T7">v-v:ptcp</ta>
            <ta e="T9" id="Seg_1580" s="T8">v-v:tense-v:poss.pn</ta>
            <ta e="T10" id="Seg_1581" s="T9">adv</ta>
            <ta e="T11" id="Seg_1582" s="T10">adv</ta>
            <ta e="T12" id="Seg_1583" s="T11">dempro-pro:case</ta>
            <ta e="T13" id="Seg_1584" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_1585" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1586" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_1587" s="T15">pers-pro:case</ta>
            <ta e="T17" id="Seg_1588" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_1589" s="T17">n-n:(poss)-n:case</ta>
            <ta e="T19" id="Seg_1590" s="T18">adj-n:case</ta>
            <ta e="T20" id="Seg_1591" s="T19">v-v:tense-v:poss.pn</ta>
            <ta e="T21" id="Seg_1592" s="T20">adv</ta>
            <ta e="T22" id="Seg_1593" s="T21">propr-n:case</ta>
            <ta e="T23" id="Seg_1594" s="T22">adv</ta>
            <ta e="T24" id="Seg_1595" s="T23">propr-n:case</ta>
            <ta e="T25" id="Seg_1596" s="T24">v-v:cvb</ta>
            <ta e="T26" id="Seg_1597" s="T25">v-v:ptcp</ta>
            <ta e="T27" id="Seg_1598" s="T26">v-v:tense-v:poss.pn</ta>
            <ta e="T28" id="Seg_1599" s="T27">adv</ta>
            <ta e="T29" id="Seg_1600" s="T28">pers-pro:case</ta>
            <ta e="T30" id="Seg_1601" s="T29">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T31" id="Seg_1602" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_1603" s="T31">v-v:tense-v:poss.pn</ta>
            <ta e="T33" id="Seg_1604" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_1605" s="T33">v-v:mood.pn</ta>
            <ta e="T35" id="Seg_1606" s="T34">v-v:mood.pn</ta>
            <ta e="T36" id="Seg_1607" s="T35">v-v:tense-v:poss.pn</ta>
            <ta e="T37" id="Seg_1608" s="T36">propr-n:case</ta>
            <ta e="T38" id="Seg_1609" s="T37">adv</ta>
            <ta e="T39" id="Seg_1610" s="T38">pers-pro:case</ta>
            <ta e="T40" id="Seg_1611" s="T39">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T41" id="Seg_1612" s="T147">adj</ta>
            <ta e="T42" id="Seg_1613" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1614" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_1615" s="T43">v-v:ptcp</ta>
            <ta e="T46" id="Seg_1616" s="T44">v-v:tense-v:poss.pn</ta>
            <ta e="T47" id="Seg_1617" s="T46">adv</ta>
            <ta e="T48" id="Seg_1618" s="T47">dempro-pro:case</ta>
            <ta e="T49" id="Seg_1619" s="T48">que-pro:case</ta>
            <ta e="T50" id="Seg_1620" s="T49">n-n:poss-n:case</ta>
            <ta e="T51" id="Seg_1621" s="T50">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T52" id="Seg_1622" s="T51">que-pro:case</ta>
            <ta e="T53" id="Seg_1623" s="T52">interj</ta>
            <ta e="T54" id="Seg_1624" s="T53">que-pro:case</ta>
            <ta e="T55" id="Seg_1625" s="T54">interj</ta>
            <ta e="T56" id="Seg_1626" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_1627" s="T56">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T58" id="Seg_1628" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_1629" s="T58">interj</ta>
            <ta e="T60" id="Seg_1630" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_1631" s="T60">v-v:neg-v:pred.pn</ta>
            <ta e="T62" id="Seg_1632" s="T61">que-pro:case</ta>
            <ta e="T63" id="Seg_1633" s="T62">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T64" id="Seg_1634" s="T63">interj</ta>
            <ta e="T65" id="Seg_1635" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_1636" s="T65">adv</ta>
            <ta e="T67" id="Seg_1637" s="T66">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T68" id="Seg_1638" s="T67">v-v:tense-v:poss.pn</ta>
            <ta e="T69" id="Seg_1639" s="T68">adv</ta>
            <ta e="T70" id="Seg_1640" s="T69">adj</ta>
            <ta e="T71" id="Seg_1641" s="T70">que-pro:case</ta>
            <ta e="T72" id="Seg_1642" s="T71">n-n:poss-n:case</ta>
            <ta e="T73" id="Seg_1643" s="T72">n</ta>
            <ta e="T74" id="Seg_1644" s="T73">propr-propr&gt;adj</ta>
            <ta e="T75" id="Seg_1645" s="T74">v-v:cvb</ta>
            <ta e="T76" id="Seg_1646" s="T75">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T77" id="Seg_1647" s="T76">propr-n:case</ta>
            <ta e="T78" id="Seg_1648" s="T77">post</ta>
            <ta e="T79" id="Seg_1649" s="T78">adv</ta>
            <ta e="T80" id="Seg_1650" s="T79">adv</ta>
            <ta e="T81" id="Seg_1651" s="T80">que-pro:case</ta>
            <ta e="T82" id="Seg_1652" s="T81">que-pro:case</ta>
            <ta e="T83" id="Seg_1653" s="T82">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T84" id="Seg_1654" s="T83">n-n:(poss)-n&gt;adj-n:case</ta>
            <ta e="T85" id="Seg_1655" s="T84">adv</ta>
            <ta e="T86" id="Seg_1656" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1657" s="T86">v-v:cvb</ta>
            <ta e="T88" id="Seg_1658" s="T87">v-v:ptcp</ta>
            <ta e="T89" id="Seg_1659" s="T88">v-v:tense-v:poss.pn</ta>
            <ta e="T90" id="Seg_1660" s="T89">adv-n:case</ta>
            <ta e="T91" id="Seg_1661" s="T90">adv</ta>
            <ta e="T92" id="Seg_1662" s="T91">adv</ta>
            <ta e="T93" id="Seg_1663" s="T92">n</ta>
            <ta e="T94" id="Seg_1664" s="T93">propr-n:case</ta>
            <ta e="T95" id="Seg_1665" s="T94">v-v:tense-v:poss.pn</ta>
            <ta e="T96" id="Seg_1666" s="T95">interj</ta>
            <ta e="T97" id="Seg_1667" s="T96">propr-n:case</ta>
            <ta e="T98" id="Seg_1668" s="T97">v-v:ptcp</ta>
            <ta e="T150" id="Seg_1669" s="T98">v-v:tense-v:poss.pn</ta>
            <ta e="T100" id="Seg_1670" s="T99">que-pro:case</ta>
            <ta e="T101" id="Seg_1671" s="T100">v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_1672" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1673" s="T102">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T104" id="Seg_1674" s="T103">v-v:ptcp</ta>
            <ta e="T105" id="Seg_1675" s="T104">v-v:tense-v:poss.pn</ta>
            <ta e="T106" id="Seg_1676" s="T105">v-v&gt;v-v:cvb</ta>
            <ta e="T107" id="Seg_1677" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_1678" s="T107">v-v:tense-v:poss.pn</ta>
            <ta e="T109" id="Seg_1679" s="T108">n-n:(num)-n:poss-n:case</ta>
            <ta e="T110" id="Seg_1680" s="T109">adv</ta>
            <ta e="T111" id="Seg_1681" s="T110">pers-pro:case</ta>
            <ta e="T112" id="Seg_1682" s="T111">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T113" id="Seg_1683" s="T112">que-pro:case</ta>
            <ta e="T114" id="Seg_1684" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_1685" s="T114">v</ta>
            <ta e="T116" id="Seg_1686" s="T115">interj</ta>
            <ta e="T117" id="Seg_1687" s="T116">que-pro:case</ta>
            <ta e="T118" id="Seg_1688" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_1689" s="T118">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T120" id="Seg_1690" s="T119">adv</ta>
            <ta e="T121" id="Seg_1691" s="T120">n</ta>
            <ta e="T122" id="Seg_1692" s="T121">propr-n:case</ta>
            <ta e="T123" id="Seg_1693" s="T122">v-v:tense-v:poss.pn</ta>
            <ta e="T124" id="Seg_1694" s="T123">n-n:poss-n:case</ta>
            <ta e="T125" id="Seg_1695" s="T124">adv</ta>
            <ta e="T126" id="Seg_1696" s="T125">n-n:(poss)-n:case</ta>
            <ta e="T127" id="Seg_1697" s="T126">que-pro:case</ta>
            <ta e="T128" id="Seg_1698" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_1699" s="T128">interj</ta>
            <ta e="T130" id="Seg_1700" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_1701" s="T130">v-v:tense-v:poss.pn</ta>
            <ta e="T132" id="Seg_1702" s="T131">n-n:poss-n:case</ta>
            <ta e="T133" id="Seg_1703" s="T132">adv</ta>
            <ta e="T149" id="Seg_1704" s="T133">v-v:tense-v:poss.pn</ta>
            <ta e="T135" id="Seg_1705" s="T134">n</ta>
            <ta e="T136" id="Seg_1706" s="T135">propr-propr&gt;adj-n:case</ta>
            <ta e="T137" id="Seg_1707" s="T136">v-v:tense-v:poss.pn</ta>
            <ta e="T138" id="Seg_1708" s="T137">que-pro:case</ta>
            <ta e="T139" id="Seg_1709" s="T138">n</ta>
            <ta e="T140" id="Seg_1710" s="T139">propr-n:case</ta>
            <ta e="T141" id="Seg_1711" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_1712" s="T141">propr-n:case</ta>
            <ta e="T143" id="Seg_1713" s="T142">adv</ta>
            <ta e="T145" id="Seg_1714" s="T148">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1715" s="T0">cardnum</ta>
            <ta e="T2" id="Seg_1716" s="T1">adv</ta>
            <ta e="T3" id="Seg_1717" s="T2">que</ta>
            <ta e="T4" id="Seg_1718" s="T3">propr</ta>
            <ta e="T5" id="Seg_1719" s="T4">post</ta>
            <ta e="T6" id="Seg_1720" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_1721" s="T6">pers</ta>
            <ta e="T8" id="Seg_1722" s="T7">v</ta>
            <ta e="T9" id="Seg_1723" s="T8">aux</ta>
            <ta e="T10" id="Seg_1724" s="T9">adv</ta>
            <ta e="T11" id="Seg_1725" s="T10">adv</ta>
            <ta e="T12" id="Seg_1726" s="T11">dempro</ta>
            <ta e="T13" id="Seg_1727" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_1728" s="T13">n</ta>
            <ta e="T15" id="Seg_1729" s="T14">n</ta>
            <ta e="T16" id="Seg_1730" s="T15">pers</ta>
            <ta e="T17" id="Seg_1731" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_1732" s="T17">n</ta>
            <ta e="T19" id="Seg_1733" s="T18">adj</ta>
            <ta e="T20" id="Seg_1734" s="T19">cop</ta>
            <ta e="T21" id="Seg_1735" s="T20">adv</ta>
            <ta e="T22" id="Seg_1736" s="T21">propr</ta>
            <ta e="T23" id="Seg_1737" s="T22">adv</ta>
            <ta e="T24" id="Seg_1738" s="T23">propr</ta>
            <ta e="T25" id="Seg_1739" s="T24">v</ta>
            <ta e="T26" id="Seg_1740" s="T25">aux</ta>
            <ta e="T27" id="Seg_1741" s="T26">aux</ta>
            <ta e="T28" id="Seg_1742" s="T27">adv</ta>
            <ta e="T29" id="Seg_1743" s="T28">pers</ta>
            <ta e="T30" id="Seg_1744" s="T29">v</ta>
            <ta e="T31" id="Seg_1745" s="T30">n</ta>
            <ta e="T32" id="Seg_1746" s="T31">v</ta>
            <ta e="T33" id="Seg_1747" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_1748" s="T33">v</ta>
            <ta e="T35" id="Seg_1749" s="T34">v</ta>
            <ta e="T36" id="Seg_1750" s="T35">v</ta>
            <ta e="T37" id="Seg_1751" s="T36">propr</ta>
            <ta e="T38" id="Seg_1752" s="T37">adv</ta>
            <ta e="T39" id="Seg_1753" s="T38">pers</ta>
            <ta e="T40" id="Seg_1754" s="T39">v</ta>
            <ta e="T41" id="Seg_1755" s="T147">adj</ta>
            <ta e="T42" id="Seg_1756" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1757" s="T42">n</ta>
            <ta e="T44" id="Seg_1758" s="T43">v</ta>
            <ta e="T46" id="Seg_1759" s="T44">aux</ta>
            <ta e="T47" id="Seg_1760" s="T46">adv</ta>
            <ta e="T48" id="Seg_1761" s="T47">dempro</ta>
            <ta e="T49" id="Seg_1762" s="T48">que</ta>
            <ta e="T50" id="Seg_1763" s="T49">n</ta>
            <ta e="T51" id="Seg_1764" s="T50">v</ta>
            <ta e="T52" id="Seg_1765" s="T51">que</ta>
            <ta e="T53" id="Seg_1766" s="T52">interj</ta>
            <ta e="T54" id="Seg_1767" s="T53">que</ta>
            <ta e="T55" id="Seg_1768" s="T54">interj</ta>
            <ta e="T56" id="Seg_1769" s="T55">n</ta>
            <ta e="T57" id="Seg_1770" s="T56">v</ta>
            <ta e="T58" id="Seg_1771" s="T57">n</ta>
            <ta e="T59" id="Seg_1772" s="T58">interj</ta>
            <ta e="T60" id="Seg_1773" s="T59">n</ta>
            <ta e="T61" id="Seg_1774" s="T60">cop</ta>
            <ta e="T62" id="Seg_1775" s="T61">que</ta>
            <ta e="T63" id="Seg_1776" s="T62">cop</ta>
            <ta e="T64" id="Seg_1777" s="T63">interj</ta>
            <ta e="T65" id="Seg_1778" s="T64">n</ta>
            <ta e="T66" id="Seg_1779" s="T65">adv</ta>
            <ta e="T67" id="Seg_1780" s="T66">n</ta>
            <ta e="T68" id="Seg_1781" s="T67">v</ta>
            <ta e="T69" id="Seg_1782" s="T68">adv</ta>
            <ta e="T70" id="Seg_1783" s="T69">adj</ta>
            <ta e="T71" id="Seg_1784" s="T70">que</ta>
            <ta e="T72" id="Seg_1785" s="T71">n</ta>
            <ta e="T73" id="Seg_1786" s="T72">n</ta>
            <ta e="T74" id="Seg_1787" s="T73">adj</ta>
            <ta e="T75" id="Seg_1788" s="T74">v</ta>
            <ta e="T76" id="Seg_1789" s="T75">v</ta>
            <ta e="T77" id="Seg_1790" s="T76">propr</ta>
            <ta e="T78" id="Seg_1791" s="T77">post</ta>
            <ta e="T79" id="Seg_1792" s="T78">adv</ta>
            <ta e="T80" id="Seg_1793" s="T79">adv</ta>
            <ta e="T81" id="Seg_1794" s="T80">que</ta>
            <ta e="T82" id="Seg_1795" s="T81">que</ta>
            <ta e="T83" id="Seg_1796" s="T82">cop</ta>
            <ta e="T84" id="Seg_1797" s="T83">n</ta>
            <ta e="T85" id="Seg_1798" s="T84">adv</ta>
            <ta e="T86" id="Seg_1799" s="T85">n</ta>
            <ta e="T87" id="Seg_1800" s="T86">v</ta>
            <ta e="T88" id="Seg_1801" s="T87">aux</ta>
            <ta e="T89" id="Seg_1802" s="T88">aux</ta>
            <ta e="T90" id="Seg_1803" s="T89">adv</ta>
            <ta e="T91" id="Seg_1804" s="T90">adv</ta>
            <ta e="T92" id="Seg_1805" s="T91">adv</ta>
            <ta e="T93" id="Seg_1806" s="T92">n</ta>
            <ta e="T94" id="Seg_1807" s="T93">propr</ta>
            <ta e="T95" id="Seg_1808" s="T94">v</ta>
            <ta e="T96" id="Seg_1809" s="T95">interj</ta>
            <ta e="T97" id="Seg_1810" s="T96">propr</ta>
            <ta e="T98" id="Seg_1811" s="T97">v</ta>
            <ta e="T150" id="Seg_1812" s="T98">aux</ta>
            <ta e="T100" id="Seg_1813" s="T99">que</ta>
            <ta e="T101" id="Seg_1814" s="T100">cop</ta>
            <ta e="T102" id="Seg_1815" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1816" s="T102">n</ta>
            <ta e="T104" id="Seg_1817" s="T103">v</ta>
            <ta e="T105" id="Seg_1818" s="T104">aux</ta>
            <ta e="T106" id="Seg_1819" s="T105">v</ta>
            <ta e="T107" id="Seg_1820" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_1821" s="T107">aux</ta>
            <ta e="T109" id="Seg_1822" s="T108">n</ta>
            <ta e="T110" id="Seg_1823" s="T109">adv</ta>
            <ta e="T111" id="Seg_1824" s="T110">pers</ta>
            <ta e="T112" id="Seg_1825" s="T111">v</ta>
            <ta e="T113" id="Seg_1826" s="T112">que</ta>
            <ta e="T114" id="Seg_1827" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_1828" s="T114">v</ta>
            <ta e="T116" id="Seg_1829" s="T115">interj</ta>
            <ta e="T117" id="Seg_1830" s="T116">que</ta>
            <ta e="T118" id="Seg_1831" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_1832" s="T118">v</ta>
            <ta e="T120" id="Seg_1833" s="T119">adv</ta>
            <ta e="T121" id="Seg_1834" s="T120">n</ta>
            <ta e="T122" id="Seg_1835" s="T121">propr</ta>
            <ta e="T123" id="Seg_1836" s="T122">v</ta>
            <ta e="T124" id="Seg_1837" s="T123">n</ta>
            <ta e="T125" id="Seg_1838" s="T124">adv</ta>
            <ta e="T126" id="Seg_1839" s="T125">n</ta>
            <ta e="T127" id="Seg_1840" s="T126">que</ta>
            <ta e="T128" id="Seg_1841" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_1842" s="T128">interj</ta>
            <ta e="T130" id="Seg_1843" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_1844" s="T130">v</ta>
            <ta e="T132" id="Seg_1845" s="T131">n</ta>
            <ta e="T133" id="Seg_1846" s="T132">adv</ta>
            <ta e="T149" id="Seg_1847" s="T133">v</ta>
            <ta e="T135" id="Seg_1848" s="T134">n</ta>
            <ta e="T136" id="Seg_1849" s="T135">adj</ta>
            <ta e="T137" id="Seg_1850" s="T136">v</ta>
            <ta e="T138" id="Seg_1851" s="T137">que</ta>
            <ta e="T139" id="Seg_1852" s="T138">n</ta>
            <ta e="T140" id="Seg_1853" s="T139">propr</ta>
            <ta e="T141" id="Seg_1854" s="T140">n</ta>
            <ta e="T142" id="Seg_1855" s="T141">propr</ta>
            <ta e="T143" id="Seg_1856" s="T142">adv</ta>
            <ta e="T145" id="Seg_1857" s="T148">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1858" s="T1">adv:Time</ta>
            <ta e="T5" id="Seg_1859" s="T3">pp:Com</ta>
            <ta e="T7" id="Seg_1860" s="T6">pro:G</ta>
            <ta e="T9" id="Seg_1861" s="T7">0.1.h:A</ta>
            <ta e="T16" id="Seg_1862" s="T15">pro.h:Poss</ta>
            <ta e="T18" id="Seg_1863" s="T17">np:Th</ta>
            <ta e="T22" id="Seg_1864" s="T21">np.h:A</ta>
            <ta e="T29" id="Seg_1865" s="T28">pro.h:A</ta>
            <ta e="T31" id="Seg_1866" s="T30">np:P</ta>
            <ta e="T32" id="Seg_1867" s="T31">0.1.h:A</ta>
            <ta e="T34" id="Seg_1868" s="T33">0.2.h:A</ta>
            <ta e="T35" id="Seg_1869" s="T34">0.2.h:A</ta>
            <ta e="T37" id="Seg_1870" s="T36">np.h:A</ta>
            <ta e="T39" id="Seg_1871" s="T38">pro.h:A</ta>
            <ta e="T43" id="Seg_1872" s="T42">np:Th</ta>
            <ta e="T51" id="Seg_1873" s="T50">0.1.h:A</ta>
            <ta e="T52" id="Seg_1874" s="T51">pro:Th</ta>
            <ta e="T56" id="Seg_1875" s="T55">np:G</ta>
            <ta e="T57" id="Seg_1876" s="T56">0.1.h:A</ta>
            <ta e="T58" id="Seg_1877" s="T57">np:Th</ta>
            <ta e="T67" id="Seg_1878" s="T66">0.1.h:Poss np:P</ta>
            <ta e="T84" id="Seg_1879" s="T83">np.h:A</ta>
            <ta e="T85" id="Seg_1880" s="T84">adv:L</ta>
            <ta e="T86" id="Seg_1881" s="T85">np:P</ta>
            <ta e="T90" id="Seg_1882" s="T89">adv:Time</ta>
            <ta e="T94" id="Seg_1883" s="T93">np.h:A</ta>
            <ta e="T97" id="Seg_1884" s="T96">np.h:A</ta>
            <ta e="T103" id="Seg_1885" s="T102">0.2.h:Poss np:Th</ta>
            <ta e="T105" id="Seg_1886" s="T103">0.3.h:A</ta>
            <ta e="T108" id="Seg_1887" s="T105">0.3.h:E</ta>
            <ta e="T109" id="Seg_1888" s="T108">0.1.h:Poss np:St</ta>
            <ta e="T110" id="Seg_1889" s="T109">adv:Time</ta>
            <ta e="T111" id="Seg_1890" s="T110">pro.h:A</ta>
            <ta e="T117" id="Seg_1891" s="T116">pro:Th</ta>
            <ta e="T119" id="Seg_1892" s="T118">0.1.h:A</ta>
            <ta e="T122" id="Seg_1893" s="T121">np.h:A</ta>
            <ta e="T124" id="Seg_1894" s="T123">0.1.h:Poss np.h:Th</ta>
            <ta e="T126" id="Seg_1895" s="T125">0.1.h:Poss</ta>
            <ta e="T131" id="Seg_1896" s="T130">0.3.h:A</ta>
            <ta e="T132" id="Seg_1897" s="T131">0.1.h:Poss np.h:Th</ta>
            <ta e="T133" id="Seg_1898" s="T132">adv:Time</ta>
            <ta e="T149" id="Seg_1899" s="T133">0.3.h:A</ta>
            <ta e="T137" id="Seg_1900" s="T136">0.3.h:A</ta>
            <ta e="T140" id="Seg_1901" s="T139">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T9" id="Seg_1902" s="T7">0.1.h:S v:pred</ta>
            <ta e="T18" id="Seg_1903" s="T17">np:S</ta>
            <ta e="T19" id="Seg_1904" s="T18">adj:pred</ta>
            <ta e="T20" id="Seg_1905" s="T19">cop</ta>
            <ta e="T22" id="Seg_1906" s="T21">np.h:S</ta>
            <ta e="T27" id="Seg_1907" s="T24">v:pred</ta>
            <ta e="T29" id="Seg_1908" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_1909" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_1910" s="T30">np:O</ta>
            <ta e="T32" id="Seg_1911" s="T31">0.1.h:S v:pred</ta>
            <ta e="T34" id="Seg_1912" s="T33">0.2.h:S v:pred</ta>
            <ta e="T35" id="Seg_1913" s="T34">0.2.h:S v:pred</ta>
            <ta e="T36" id="Seg_1914" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_1915" s="T36">np.h:S</ta>
            <ta e="T39" id="Seg_1916" s="T38">pro.h:S</ta>
            <ta e="T40" id="Seg_1917" s="T39">v:pred</ta>
            <ta e="T43" id="Seg_1918" s="T42">np:S</ta>
            <ta e="T46" id="Seg_1919" s="T43">v:pred</ta>
            <ta e="T51" id="Seg_1920" s="T50">0.1.h:S v:pred</ta>
            <ta e="T52" id="Seg_1921" s="T51">pro:O</ta>
            <ta e="T57" id="Seg_1922" s="T56">0.1.h:S v:pred</ta>
            <ta e="T58" id="Seg_1923" s="T57">np:O</ta>
            <ta e="T60" id="Seg_1924" s="T59">np:S</ta>
            <ta e="T61" id="Seg_1925" s="T60">v:pred</ta>
            <ta e="T67" id="Seg_1926" s="T66">np:S</ta>
            <ta e="T68" id="Seg_1927" s="T67">v:pred</ta>
            <ta e="T84" id="Seg_1928" s="T83">np.h:S</ta>
            <ta e="T86" id="Seg_1929" s="T85">np:O</ta>
            <ta e="T89" id="Seg_1930" s="T86">v:pred</ta>
            <ta e="T94" id="Seg_1931" s="T93">np.h:S</ta>
            <ta e="T95" id="Seg_1932" s="T94">v:pred</ta>
            <ta e="T97" id="Seg_1933" s="T96">np.h:S</ta>
            <ta e="T150" id="Seg_1934" s="T97">v:pred</ta>
            <ta e="T100" id="Seg_1935" s="T99">pro:pred</ta>
            <ta e="T101" id="Seg_1936" s="T100">cop</ta>
            <ta e="T103" id="Seg_1937" s="T102">np:S</ta>
            <ta e="T105" id="Seg_1938" s="T103">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_1939" s="T105">0.3.h:S v:pred</ta>
            <ta e="T109" id="Seg_1940" s="T108">np:O</ta>
            <ta e="T111" id="Seg_1941" s="T110">pro.h:S</ta>
            <ta e="T112" id="Seg_1942" s="T111">v:pred</ta>
            <ta e="T117" id="Seg_1943" s="T116">pro:O</ta>
            <ta e="T119" id="Seg_1944" s="T118">0.1.h:S v:pred</ta>
            <ta e="T122" id="Seg_1945" s="T121">np.h:S</ta>
            <ta e="T123" id="Seg_1946" s="T122">v:pred</ta>
            <ta e="T124" id="Seg_1947" s="T123">np.h:O</ta>
            <ta e="T131" id="Seg_1948" s="T130">0.3.h:S v:pred</ta>
            <ta e="T132" id="Seg_1949" s="T131">np.h:O</ta>
            <ta e="T149" id="Seg_1950" s="T133">0.3.h:S v:pred</ta>
            <ta e="T137" id="Seg_1951" s="T136">0.3.h:S v:pred</ta>
            <ta e="T140" id="Seg_1952" s="T139">np.h:S</ta>
            <ta e="T141" id="Seg_1953" s="T140">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T4" id="Seg_1954" s="T3">new</ta>
            <ta e="T7" id="Seg_1955" s="T6">accs-sit</ta>
            <ta e="T9" id="Seg_1956" s="T7">0.accs-aggr</ta>
            <ta e="T16" id="Seg_1957" s="T15">giv-active</ta>
            <ta e="T18" id="Seg_1958" s="T17">accs-inf</ta>
            <ta e="T22" id="Seg_1959" s="T21">giv-inactive</ta>
            <ta e="T24" id="Seg_1960" s="T23">new</ta>
            <ta e="T29" id="Seg_1961" s="T28">giv-inactive</ta>
            <ta e="T30" id="Seg_1962" s="T29">quot-sp</ta>
            <ta e="T31" id="Seg_1963" s="T30">new-Q</ta>
            <ta e="T32" id="Seg_1964" s="T31">0.giv-active-Q</ta>
            <ta e="T34" id="Seg_1965" s="T33">0.giv-active-Q</ta>
            <ta e="T35" id="Seg_1966" s="T34">0.giv-active-Q</ta>
            <ta e="T36" id="Seg_1967" s="T35">quot-sp</ta>
            <ta e="T37" id="Seg_1968" s="T36">giv-inactive</ta>
            <ta e="T39" id="Seg_1969" s="T38">giv-active</ta>
            <ta e="T43" id="Seg_1970" s="T42">new</ta>
            <ta e="T51" id="Seg_1971" s="T50">0.giv-active</ta>
            <ta e="T56" id="Seg_1972" s="T55">giv-active</ta>
            <ta e="T57" id="Seg_1973" s="T56">0.giv-active</ta>
            <ta e="T58" id="Seg_1974" s="T57">new</ta>
            <ta e="T60" id="Seg_1975" s="T59">giv-active</ta>
            <ta e="T65" id="Seg_1976" s="T64">new</ta>
            <ta e="T67" id="Seg_1977" s="T66">accs-inf</ta>
            <ta e="T72" id="Seg_1978" s="T71">giv-inactive</ta>
            <ta e="T74" id="Seg_1979" s="T73">new</ta>
            <ta e="T77" id="Seg_1980" s="T76">giv-inactive</ta>
            <ta e="T84" id="Seg_1981" s="T83">accs-inf</ta>
            <ta e="T86" id="Seg_1982" s="T85">new</ta>
            <ta e="T94" id="Seg_1983" s="T93">giv-inactive</ta>
            <ta e="T97" id="Seg_1984" s="T96">new</ta>
            <ta e="T103" id="Seg_1985" s="T102">accs-sit-Q</ta>
            <ta e="T105" id="Seg_1986" s="T103">0.giv-active 0.quot-sp</ta>
            <ta e="T108" id="Seg_1987" s="T105">0.giv-active</ta>
            <ta e="T109" id="Seg_1988" s="T108">giv-active</ta>
            <ta e="T111" id="Seg_1989" s="T110">giv-inactive</ta>
            <ta e="T119" id="Seg_1990" s="T118">0.giv-active</ta>
            <ta e="T122" id="Seg_1991" s="T121">giv-inactive</ta>
            <ta e="T124" id="Seg_1992" s="T123">giv-inactive</ta>
            <ta e="T126" id="Seg_1993" s="T125">giv-active</ta>
            <ta e="T131" id="Seg_1994" s="T130">0.giv-active</ta>
            <ta e="T132" id="Seg_1995" s="T131">giv-active</ta>
            <ta e="T149" id="Seg_1996" s="T133">0.giv-active</ta>
            <ta e="T136" id="Seg_1997" s="T135">new</ta>
            <ta e="T137" id="Seg_1998" s="T136">0.giv-active</ta>
            <ta e="T140" id="Seg_1999" s="T139">giv-active</ta>
            <ta e="T142" id="Seg_2000" s="T141">giv-active</ta>
         </annotation>
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_2001" s="T3">RUS:cult</ta>
            <ta e="T6" id="Seg_2002" s="T5">RUS:mod</ta>
            <ta e="T17" id="Seg_2003" s="T16">RUS:mod</ta>
            <ta e="T22" id="Seg_2004" s="T21">RUS:cult</ta>
            <ta e="T24" id="Seg_2005" s="T23">RUS:cult</ta>
            <ta e="T37" id="Seg_2006" s="T36">RUS:cult</ta>
            <ta e="T58" id="Seg_2007" s="T57">RUS:cult</ta>
            <ta e="T60" id="Seg_2008" s="T59">RUS:cult</ta>
            <ta e="T65" id="Seg_2009" s="T64">RUS:cult</ta>
            <ta e="T73" id="Seg_2010" s="T72">RUS:core</ta>
            <ta e="T74" id="Seg_2011" s="T73">RUS:cult</ta>
            <ta e="T77" id="Seg_2012" s="T76">RUS:cult</ta>
            <ta e="T90" id="Seg_2013" s="T89">RUS:core</ta>
            <ta e="T93" id="Seg_2014" s="T92">RUS:core</ta>
            <ta e="T94" id="Seg_2015" s="T93">RUS:cult</ta>
            <ta e="T97" id="Seg_2016" s="T96">RUS:cult</ta>
            <ta e="T103" id="Seg_2017" s="T102">RUS:core</ta>
            <ta e="T106" id="Seg_2018" s="T105">RUS:core</ta>
            <ta e="T109" id="Seg_2019" s="T108">RUS:core</ta>
            <ta e="T121" id="Seg_2020" s="T120">RUS:core</ta>
            <ta e="T122" id="Seg_2021" s="T121">RUS:cult</ta>
            <ta e="T135" id="Seg_2022" s="T134">RUS:core</ta>
            <ta e="T136" id="Seg_2023" s="T135">RUS:cult</ta>
            <ta e="T139" id="Seg_2024" s="T138">RUS:core</ta>
            <ta e="T140" id="Seg_2025" s="T139">RUS:cult</ta>
            <ta e="T141" id="Seg_2026" s="T140">RUS:cult</ta>
            <ta e="T142" id="Seg_2027" s="T141">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T90" id="Seg_2028" s="T89">fortition Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T4" id="Seg_2029" s="T3">dir:infl</ta>
            <ta e="T6" id="Seg_2030" s="T5">dir:bare</ta>
            <ta e="T17" id="Seg_2031" s="T16">dir:bare</ta>
            <ta e="T22" id="Seg_2032" s="T21">dir:bare</ta>
            <ta e="T24" id="Seg_2033" s="T23">dir:infl</ta>
            <ta e="T37" id="Seg_2034" s="T36">dir:bare</ta>
            <ta e="T58" id="Seg_2035" s="T57">dir:infl</ta>
            <ta e="T60" id="Seg_2036" s="T59">dir:infl</ta>
            <ta e="T65" id="Seg_2037" s="T64">dir:infl</ta>
            <ta e="T73" id="Seg_2038" s="T72">dir:bare</ta>
            <ta e="T74" id="Seg_2039" s="T73">dir:infl</ta>
            <ta e="T77" id="Seg_2040" s="T76">dir:infl</ta>
            <ta e="T90" id="Seg_2041" s="T89">dir:infl</ta>
            <ta e="T93" id="Seg_2042" s="T92">dir:bare</ta>
            <ta e="T94" id="Seg_2043" s="T93">dir:bare</ta>
            <ta e="T97" id="Seg_2044" s="T96">dir:bare</ta>
            <ta e="T103" id="Seg_2045" s="T102">parad:infl</ta>
            <ta e="T106" id="Seg_2046" s="T105">indir:infl</ta>
            <ta e="T109" id="Seg_2047" s="T108">parad:infl</ta>
            <ta e="T135" id="Seg_2048" s="T134">dir:bare</ta>
            <ta e="T136" id="Seg_2049" s="T135">dir:infl</ta>
            <ta e="T139" id="Seg_2050" s="T138">dir:bare</ta>
            <ta e="T140" id="Seg_2051" s="T139">dir:bare</ta>
            <ta e="T141" id="Seg_2052" s="T140">dir:bare</ta>
            <ta e="T142" id="Seg_2053" s="T141">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_2054" s="T0">Once Rita and me came to our place.</ta>
            <ta e="T20" id="Seg_2055" s="T9">Then there, the house, the house, my house was cold.</ta>
            <ta e="T27" id="Seg_2056" s="T20">Rita played Dendy.</ta>
            <ta e="T30" id="Seg_2057" s="T27">Then I said: </ta>
            <ta e="T33" id="Seg_2058" s="T30">"Shall I light a fire?"</ta>
            <ta e="T37" id="Seg_2059" s="T33">"Heat, heat", Rita said.</ta>
            <ta e="T46" id="Seg_2060" s="T37">Then I went, (…) a big piece of coal was lying there.</ta>
            <ta e="T61" id="Seg_2061" s="T46">Then that, with the coal, I splashed that whatchamacallit, eh, I splashed solar oil onto the coal, eh, it wasn't solar oil.</ta>
            <ta e="T68" id="Seg_2062" s="T61">What was it, eh, kerosine, then my face burned.</ta>
            <ta e="T78" id="Seg_2063" s="T68">Then, later, Rita and aunt Styosha went into our house.</ta>
            <ta e="T90" id="Seg_2064" s="T78">Then there, who was it, my father and his people always were making a boat there.</ta>
            <ta e="T150" id="Seg_2065" s="T90">Then aunt Styosha said there, eh, Roska has said it.</ta>
            <ta e="T105" id="Seg_2066" s="T150">"What has happened to your eyebrows then", she said.</ta>
            <ta e="T109" id="Seg_2067" s="T105">She hadn't noticed my eyebrows.</ta>
            <ta e="T119" id="Seg_2068" s="T109">Then I said nothing, eh, I didn't tell anything.</ta>
            <ta e="T149" id="Seg_2069" s="T119">Then aunt Styosha called my father, then my father nothing, eh, then she called again, then he went.</ta>
            <ta e="T145" id="Seg_2070" s="T134">He/she called aunt Lena, aunt Lena is a doctor, Lena then (…) that's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_2071" s="T0">Einmal sind ich Rita und ich zu uns gekommen.</ta>
            <ta e="T20" id="Seg_2072" s="T9">Dann dort, das Haus, das Haus, mein Haus war kalt.</ta>
            <ta e="T27" id="Seg_2073" s="T20">Rita spielte Dendy.</ta>
            <ta e="T30" id="Seg_2074" s="T27">Dann sagte ich: </ta>
            <ta e="T33" id="Seg_2075" s="T30">"Soll ich Feuer machen?"</ta>
            <ta e="T37" id="Seg_2076" s="T33">"Heize, heize", sagte Rita.</ta>
            <ta e="T46" id="Seg_2077" s="T37">Dann ging ich, (…) ein großes Kohlestück lag da.</ta>
            <ta e="T61" id="Seg_2078" s="T46">Dann dieses, mit der Kohle, ich spritze das Dings, äh, auf die Kohle spritzte ich Solaröl, äh, das war kein Solaröl.</ta>
            <ta e="T68" id="Seg_2079" s="T61">Was war es, äh, Kerosin, dann verbrannte mein Gesicht.</ta>
            <ta e="T78" id="Seg_2080" s="T68">Dann später kamen Rita und Tante Stjoscha in unser Haus gelaufen.</ta>
            <ta e="T90" id="Seg_2081" s="T78">Dann dort, wer war es, mein Vater und seine Leute haben dort immer ein Boot gemacht.</ta>
            <ta e="T150" id="Seg_2082" s="T90">Dann sagte Tante Stjoscha da, äh, Roska hatte das gesagt.</ta>
            <ta e="T105" id="Seg_2083" s="T150">"Was ist bloß mit deinen Augenbrauen passiert", sagte sie.</ta>
            <ta e="T109" id="Seg_2084" s="T105">Sie hatte meine Augenbrauen noch nicht bemerkt.</ta>
            <ta e="T119" id="Seg_2085" s="T109">Dann sagte ich nichts, äh, ich habe nichts erzählt.</ta>
            <ta e="T149" id="Seg_2086" s="T119">Dann rief Tante Styosha meinen Vater [an], dann mein Vater nichts, äh, dann rief sie wieder [an], dann ging er.</ta>
            <ta e="T145" id="Seg_2087" s="T134">Er/sie rief Tante Lena [an], Tante Lena ist Doktor, Lena dann (…) das ist alles.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_2088" s="T0">Однажды мы с Ритой пришли к нам.</ta>
            <ta e="T20" id="Seg_2089" s="T9">А там дома, дома, у меня дома холодно было.</ta>
            <ta e="T27" id="Seg_2090" s="T20">А Рита сидела играла в Денди.</ta>
            <ta e="T30" id="Seg_2091" s="T27">Потом я сказала: </ta>
            <ta e="T33" id="Seg_2092" s="T30">"Я растоплю печку?"</ta>
            <ta e="T37" id="Seg_2093" s="T33">"Растопи, растопи", – сказала Рита.</ta>
            <ta e="T46" id="Seg_2094" s="T37">Потом я пошла, (…) а там лежал большой уголёк.</ta>
            <ta e="T61" id="Seg_2095" s="T46">Потом это, этим угольком я взбрызнула, ээ, на этот уголёк я бырзнула соляркой, ээ, это была не солярка.</ta>
            <ta e="T68" id="Seg_2096" s="T61">Как это, керосин, и я себе лицо обожгла.</ta>
            <ta e="T78" id="Seg_2097" s="T68">Потом уже, в дом прибежала тётя Стёша, с Ритой.</ta>
            <ta e="T90" id="Seg_2098" s="T78">Потом там, кто это был, мой отец с людьми мастерил маленькую лодку.</ta>
            <ta e="T150" id="Seg_2099" s="T90">А потом тётя Стёша сказала, ээ, это Роска сказала.</ta>
            <ta e="T105" id="Seg_2100" s="T150">"Что случилось с твоими бровями?" – сказала она.</ta>
            <ta e="T109" id="Seg_2101" s="T105">Она сначала не заметила мои брови.</ta>
            <ta e="T119" id="Seg_2102" s="T109">А я ничего не сказала, ничего не рассказала.</ta>
            <ta e="T149" id="Seg_2103" s="T119">Тогда тётя Стёша позвала моего отца, потом мой отец ничего, ээ, тогда она снова позвала, тогда он пошёл.</ta>
            <ta e="T145" id="Seg_2104" s="T134">Позвали тётю Лену, тётя Лена – врач, Лена тогда (…), всё.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_2105" s="T0">odnazhdi mɨ s ritoj k nam prishli</ta>
            <ta e="T20" id="Seg_2106" s="T9">potom tam v moem dome holodno bɨlo</ta>
            <ta e="T27" id="Seg_2107" s="T20">Rita igrala v dandi</ta>
            <ta e="T30" id="Seg_2108" s="T27">potom ja skazala </ta>
            <ta e="T33" id="Seg_2109" s="T30">rastopit pechku,</ta>
            <ta e="T37" id="Seg_2110" s="T33">rastopi rastopi skazala Ritta</ta>
            <ta e="T46" id="Seg_2111" s="T37">potom ja podoshla, tam lezhal bolshoj ugoljek</ta>
            <ta e="T61" id="Seg_2112" s="T46">potom etot ugoljok brɨzgnula brɨzgnula solarkaj, a eto bɨla ne soljarka, </ta>
            <ta e="T68" id="Seg_2113" s="T61">kak etot nazɨvaetsja, kerosin potom litso obozhgla</ta>
            <ta e="T78" id="Seg_2114" s="T68">potom po pozzhe pobezhala tjetja stesha vmeste s riitoj </ta>
            <ta e="T90" id="Seg_2115" s="T78">potom tam otets s kem to tam lodku delali vsegda</ta>
            <ta e="T150" id="Seg_2116" s="T90">potom tetje stesha aaa roska skazala </ta>
            <ta e="T105" id="Seg_2117" s="T150">chto sluchilos s brovjami sprosila ona</ta>
            <ta e="T109" id="Seg_2118" s="T105">ona ne zametlia snachala moi brovi</ta>
            <ta e="T119" id="Seg_2119" s="T109">potom ja skazala nichego ne rasskazala</ta>
            <ta e="T149" id="Seg_2120" s="T119">potom tetja stesha pozvala otsa potm otets nichego ee opjat pozvala otsa potom otets ushel</ta>
            <ta e="T145" id="Seg_2121" s="T134">tetja lena pozvala tjetja lena doktoru lenu</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T27" id="Seg_2122" s="T20">[DCh]: Dendy is a clone of the Japanese Nintendo console which was designed especially for the Russian market.</ta>
            <ta e="T78" id="Seg_2123" s="T68">[DCh]: Not clear why 1st person singular endings at the verb.</ta>
            <ta e="T145" id="Seg_2124" s="T134">[DCh]: Not really clear who called whom.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T147" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T150" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T149" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T148" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
