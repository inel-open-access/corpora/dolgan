<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1C314A36-376E-B541-A96B-A1351DE5CBD7">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnIM_2009_Pear_nar</transcription-name>
         <referenced-file url="AnIM_2009_Pear_nar.wav" />
         <referenced-file url="AnIM_2009_Pear_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\AnIM_2009_Pear_nar\AnIM_2009_Pear_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">294</ud-information>
            <ud-information attribute-name="# HIAT:w">242</ud-information>
            <ud-information attribute-name="# e">242</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">6</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="6a772c28-b741-4448-bb26-198e9eec3296">
            <abbreviation>AnIM</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="12.256" type="appl" />
         <tli id="T2" time="13.491142857142858" type="appl" />
         <tli id="T3" time="14.726285714285716" type="appl" />
         <tli id="T4" time="15.961428571428572" type="appl" />
         <tli id="T5" time="17.19657142857143" type="appl" />
         <tli id="T6" time="18.431714285714285" type="appl" />
         <tli id="T7" time="19.666857142857143" type="appl" />
         <tli id="T8" time="20.902" type="appl" />
         <tli id="T9" time="21.4799" type="appl" />
         <tli id="T10" time="22.0578" type="appl" />
         <tli id="T11" time="22.6357" type="appl" />
         <tli id="T12" time="23.2136" type="appl" />
         <tli id="T13" time="23.7915" type="appl" />
         <tli id="T14" time="24.3694" type="appl" />
         <tli id="T15" time="24.947300000000002" type="appl" />
         <tli id="T16" time="25.5252" type="appl" />
         <tli id="T17" time="26.1031" type="appl" />
         <tli id="T18" time="26.681" type="appl" />
         <tli id="T19" time="27.513333333333335" type="appl" />
         <tli id="T20" time="28.345666666666666" type="appl" />
         <tli id="T21" time="29.178" type="appl" />
         <tli id="T22" time="29.686083333333332" type="appl" />
         <tli id="T23" time="30.194166666666668" type="appl" />
         <tli id="T24" time="30.70225" type="appl" />
         <tli id="T25" time="31.210333333333335" type="appl" />
         <tli id="T26" time="31.718416666666666" type="appl" />
         <tli id="T27" time="32.2265" type="appl" />
         <tli id="T28" time="32.73458333333333" type="appl" />
         <tli id="T29" time="33.242666666666665" type="appl" />
         <tli id="T30" time="33.75075" type="appl" />
         <tli id="T31" time="34.25883333333333" type="appl" />
         <tli id="T32" time="34.76691666666667" type="appl" />
         <tli id="T33" time="35.275" type="appl" />
         <tli id="T34" time="35.777499999999996" type="appl" />
         <tli id="T35" time="36.28" type="appl" />
         <tli id="T36" time="36.7825" type="appl" />
         <tli id="T37" time="37.285" type="appl" />
         <tli id="T38" time="37.787499999999994" type="appl" />
         <tli id="T39" time="38.29" type="appl" />
         <tli id="T40" time="38.7925" type="appl" />
         <tli id="T41" time="39.294999999999995" type="appl" />
         <tli id="T42" time="39.7975" type="appl" />
         <tli id="T43" time="40.3" type="appl" />
         <tli id="T44" time="40.40239130434782" type="appl" />
         <tli id="T45" time="40.50478260869565" type="appl" />
         <tli id="T46" time="40.607173913043475" type="appl" />
         <tli id="T47" time="40.7095652173913" type="appl" />
         <tli id="T48" time="40.81195652173913" type="appl" />
         <tli id="T49" time="40.91434782608695" type="appl" />
         <tli id="T50" time="41.01673913043478" type="appl" />
         <tli id="T51" time="41.119130434782605" type="appl" />
         <tli id="T52" time="41.22152173913043" type="appl" />
         <tli id="T53" time="41.32391304347826" type="appl" />
         <tli id="T54" time="41.42630434782608" type="appl" />
         <tli id="T55" time="41.52869565217391" type="appl" />
         <tli id="T56" time="41.63108695652174" type="appl" />
         <tli id="T57" time="41.73347826086957" type="appl" />
         <tli id="T58" time="41.83586956521739" type="appl" />
         <tli id="T59" time="41.93826086956522" type="appl" />
         <tli id="T60" time="42.040652173913045" type="appl" />
         <tli id="T61" time="42.14304347826087" type="appl" />
         <tli id="T62" time="42.2454347826087" type="appl" />
         <tli id="T63" time="42.34782608695652" type="appl" />
         <tli id="T64" time="42.45021739130435" type="appl" />
         <tli id="T65" time="42.552608695652175" type="appl" />
         <tli id="T66" time="51.4386324604546" />
         <tli id="T67" time="51.61" type="appl" />
         <tli id="T68" time="51.940666666666665" type="appl" />
         <tli id="T69" time="52.27133333333333" type="appl" />
         <tli id="T70" time="52.602" type="appl" />
         <tli id="T71" time="52.93266666666666" type="appl" />
         <tli id="T72" time="53.263333333333335" type="appl" />
         <tli id="T73" time="53.594" type="appl" />
         <tli id="T74" time="53.92466666666667" type="appl" />
         <tli id="T75" time="54.25533333333333" type="appl" />
         <tli id="T76" time="57.25847773494617" />
         <tli id="T77" time="57.423" type="appl" />
         <tli id="T78" time="57.901" type="appl" />
         <tli id="T79" time="58.379000000000005" type="appl" />
         <tli id="T80" time="58.857" type="appl" />
         <tli id="T81" time="59.335" type="appl" />
         <tli id="T82" time="59.9599375" type="appl" />
         <tli id="T83" time="60.584875000000004" type="appl" />
         <tli id="T84" time="61.2098125" type="appl" />
         <tli id="T85" time="61.83475" type="appl" />
         <tli id="T86" time="62.4596875" type="appl" />
         <tli id="T87" time="63.084625" type="appl" />
         <tli id="T88" time="63.709562500000004" type="appl" />
         <tli id="T89" time="64.3345" type="appl" />
         <tli id="T90" time="64.9594375" type="appl" />
         <tli id="T91" time="65.58437500000001" type="appl" />
         <tli id="T92" time="66.20931250000001" type="appl" />
         <tli id="T93" time="66.83425" type="appl" />
         <tli id="T94" time="67.4591875" type="appl" />
         <tli id="T95" time="68.084125" type="appl" />
         <tli id="T96" time="68.7090625" type="appl" />
         <tli id="T97" time="69.334" type="appl" />
         <tli id="T98" time="69.97283333333334" type="appl" />
         <tli id="T99" time="70.61166666666666" type="appl" />
         <tli id="T100" time="71.2505" type="appl" />
         <tli id="T101" time="71.88933333333334" type="appl" />
         <tli id="T102" time="72.52816666666666" type="appl" />
         <tli id="T103" time="73.167" type="appl" />
         <tli id="T104" time="73.80583333333334" type="appl" />
         <tli id="T105" time="74.44466666666666" type="appl" />
         <tli id="T106" time="75.0835" type="appl" />
         <tli id="T107" time="75.72233333333334" type="appl" />
         <tli id="T108" time="76.36116666666666" type="appl" />
         <tli id="T109" />
         <tli id="T110" time="77.5" type="appl" />
         <tli id="T111" time="78.0" type="appl" />
         <tli id="T112" time="78.5" type="appl" />
         <tli id="T113" time="79.0" type="appl" />
         <tli id="T114" time="79.5" type="appl" />
         <tli id="T115" time="80.0" type="appl" />
         <tli id="T116" time="80.5" type="appl" />
         <tli id="T117" time="81.0" type="appl" />
         <tli id="T118" time="81.5" type="appl" />
         <tli id="T119" time="82.0" type="appl" />
         <tli id="T120" time="82.5" type="appl" />
         <tli id="T121" time="83.0" type="appl" />
         <tli id="T122" time="83.5" type="appl" />
         <tli id="T123" time="83.68444184500864" />
         <tli id="T124" time="84.48254545454546" type="appl" />
         <tli id="T125" time="84.9650909090909" type="appl" />
         <tli id="T126" time="85.44763636363636" type="appl" />
         <tli id="T127" time="85.93018181818182" type="appl" />
         <tli id="T128" time="86.41272727272728" type="appl" />
         <tli id="T129" time="86.89527272727273" type="appl" />
         <tli id="T130" time="87.37781818181818" type="appl" />
         <tli id="T131" time="87.86036363636364" type="appl" />
         <tli id="T132" time="88.3429090909091" type="appl" />
         <tli id="T133" time="88.82545454545455" type="appl" />
         <tli id="T134" time="89.308" type="appl" />
         <tli id="T135" time="89.7731" type="appl" />
         <tli id="T136" time="90.2382" type="appl" />
         <tli id="T137" time="90.70330000000001" type="appl" />
         <tli id="T138" time="91.1684" type="appl" />
         <tli id="T139" time="91.6335" type="appl" />
         <tli id="T140" time="92.0986" type="appl" />
         <tli id="T141" time="92.56370000000001" type="appl" />
         <tli id="T142" time="93.0288" type="appl" />
         <tli id="T143" time="93.4939" type="appl" />
         <tli id="T144" time="93.959" type="appl" />
         <tli id="T145" time="94.33966666666667" type="appl" />
         <tli id="T146" time="94.72033333333333" type="appl" />
         <tli id="T147" time="95.101" type="appl" />
         <tli id="T148" time="95.48166666666667" type="appl" />
         <tli id="T149" time="95.86233333333334" type="appl" />
         <tli id="T150" time="96.243" type="appl" />
         <tli id="T151" time="96.62366666666667" type="appl" />
         <tli id="T152" time="97.00433333333334" type="appl" />
         <tli id="T153" time="97.385" type="appl" />
         <tli id="T154" time="97.76566666666666" type="appl" />
         <tli id="T155" time="98.14633333333333" type="appl" />
         <tli id="T156" time="98.527" type="appl" />
         <tli id="T157" time="98.90766666666667" type="appl" />
         <tli id="T158" time="99.28833333333333" type="appl" />
         <tli id="T159" time="99.669" type="appl" />
         <tli id="T160" time="100.20266666666666" type="appl" />
         <tli id="T161" time="100.73633333333333" type="appl" />
         <tli id="T162" time="101.27" type="appl" />
         <tli id="T163" time="101.80366666666666" type="appl" />
         <tli id="T164" time="102.33733333333333" type="appl" />
         <tli id="T165" time="102.871" type="appl" />
         <tli id="T166" time="103.40466666666666" type="appl" />
         <tli id="T167" time="103.93833333333333" type="appl" />
         <tli id="T168" time="104.472" type="appl" />
         <tli id="T169" time="104.95174999999999" type="appl" />
         <tli id="T170" time="105.4315" type="appl" />
         <tli id="T171" time="105.91125" type="appl" />
         <tli id="T172" time="106.39099999999999" type="appl" />
         <tli id="T173" time="106.87075" type="appl" />
         <tli id="T174" time="107.3505" type="appl" />
         <tli id="T175" time="107.83024999999999" type="appl" />
         <tli id="T176" time="108.31" type="appl" />
         <tli id="T177" time="108.78975" type="appl" />
         <tli id="T178" time="109.2695" type="appl" />
         <tli id="T179" time="109.74924999999999" type="appl" />
         <tli id="T180" time="110.229" type="appl" />
         <tli id="T181" time="110.70875" type="appl" />
         <tli id="T182" time="111.18849999999999" type="appl" />
         <tli id="T183" time="111.66825" type="appl" />
         <tli id="T184" time="112.148" type="appl" />
         <tli id="T185" time="112.85265" type="appl" />
         <tli id="T186" time="113.5573" type="appl" />
         <tli id="T187" time="114.26195" type="appl" />
         <tli id="T188" time="114.9666" type="appl" />
         <tli id="T189" time="115.67125" type="appl" />
         <tli id="T190" time="116.3759" type="appl" />
         <tli id="T191" time="117.08055" type="appl" />
         <tli id="T192" time="117.7852" type="appl" />
         <tli id="T193" time="118.48985" type="appl" />
         <tli id="T194" time="119.1945" type="appl" />
         <tli id="T195" time="119.89914999999999" type="appl" />
         <tli id="T196" time="120.6038" type="appl" />
         <tli id="T197" time="121.30845" type="appl" />
         <tli id="T198" time="122.0131" type="appl" />
         <tli id="T199" time="122.71775" type="appl" />
         <tli id="T200" time="123.4224" type="appl" />
         <tli id="T201" time="124.12705" type="appl" />
         <tli id="T202" time="124.8317" type="appl" />
         <tli id="T203" time="125.53635" type="appl" />
         <tli id="T204" time="126.241" type="appl" />
         <tli id="T205" time="126.89607692307692" type="appl" />
         <tli id="T206" time="127.55115384615385" type="appl" />
         <tli id="T207" time="128.20623076923076" type="appl" />
         <tli id="T208" time="128.8613076923077" type="appl" />
         <tli id="T209" time="129.51638461538462" type="appl" />
         <tli id="T210" time="130.17146153846153" type="appl" />
         <tli id="T211" time="130.82653846153846" type="appl" />
         <tli id="T212" time="131.4816153846154" type="appl" />
         <tli id="T213" time="132.1366923076923" type="appl" />
         <tli id="T214" time="132.79176923076923" type="appl" />
         <tli id="T215" time="133.44684615384617" type="appl" />
         <tli id="T216" time="134.10192307692307" type="appl" />
         <tli id="T217" time="134.757" type="appl" />
         <tli id="T218" time="135.247" type="appl" />
         <tli id="T219" time="135.737" type="appl" />
         <tli id="T220" time="136.227" type="appl" />
         <tli id="T221" time="136.717" type="appl" />
         <tli id="T222" time="137.207" type="appl" />
         <tli id="T223" time="137.697" type="appl" />
         <tli id="T224" time="138.187" type="appl" />
         <tli id="T225" time="138.677" type="appl" />
         <tli id="T226" time="139.167" type="appl" />
         <tli id="T227" time="139.657" type="appl" />
         <tli id="T228" time="140.147" type="appl" />
         <tli id="T229" time="140.637" type="appl" />
         <tli id="T230" time="141.127" type="appl" />
         <tli id="T231" time="141.617" type="appl" />
         <tli id="T232" time="142.107" type="appl" />
         <tli id="T233" time="142.597" type="appl" />
         <tli id="T234" time="143.087" type="appl" />
         <tli id="T235" time="143.577" type="appl" />
         <tli id="T236" time="144.2649" type="appl" />
         <tli id="T237" time="144.9528" type="appl" />
         <tli id="T238" time="145.64069999999998" type="appl" />
         <tli id="T239" time="146.3286" type="appl" />
         <tli id="T240" time="147.0165" type="appl" />
         <tli id="T241" time="147.7044" type="appl" />
         <tli id="T242" time="148.39229999999998" type="appl" />
         <tli id="T243" time="149.0802" type="appl" />
         <tli id="T244" time="149.7681" type="appl" />
         <tli id="T245" time="150.456" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="6a772c28-b741-4448-bb26-198e9eec3296"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T66" id="Seg_0" n="sc" s="T1">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <ts e="T2" id="Seg_5" n="HIAT:w" s="T1">Sad-</ts>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">eː</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_11" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">sad-</ts>
                  <nts id="Seg_14" n="HIAT:ip">)</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">sadovnik</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">sadovnik</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">grušalarɨ</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">komujar</ts>
                  <nts id="Seg_27" n="HIAT:ip">.</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_30" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">Biːr</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">karzina</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">grušanɨ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">komujbut</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">ol</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">komuja</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">hɨttagɨna</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">bu͡o</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">ɨksatɨnan</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">bu͡ollagɨna</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_63" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">Kazaːlaːk</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">kihi</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">aːspɨta</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_75" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">Oːnton</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_79" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">ešč-</ts>
                  <nts id="Seg_82" n="HIAT:ip">)</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">ešo</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">da</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_90" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">ɨttɨ-</ts>
                  <nts id="Seg_93" n="HIAT:ip">)</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_95" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">ɨttɨbɨ-</ts>
                  <nts id="Seg_98" n="HIAT:ip">)</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">maska</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">ɨttɨbɨt</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">ešo</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">da</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">gruša</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">komujaːrɨ</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_120" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">Ol</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">hɨttagɨna</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">bu͡o</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">ol</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">di͡egitten</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">bilisipi͡etteːk</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">u͡ol</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">ogo</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_146" n="HIAT:w" s="T41">kelbit</ts>
                  <nts id="Seg_147" n="HIAT:ip">,</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_150" n="HIAT:w" s="T42">iher</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_154" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">Onton</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">itini</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">gini͡eke</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">kelen</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">bu͡ollagɨna</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">grušalarɨ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_174" n="HIAT:w" s="T49">körön</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_177" n="HIAT:w" s="T50">biːr</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_180" n="HIAT:w" s="T51">grušanɨ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_183" n="HIAT:w" s="T52">ɨlaːrɨ</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_186" n="HIAT:w" s="T53">gɨmmɨta</ts>
                  <nts id="Seg_187" n="HIAT:ip">,</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_190" n="HIAT:w" s="T54">ontubut</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_193" n="HIAT:w" s="T55">herener</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_196" n="HIAT:w" s="T56">du</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_199" n="HIAT:w" s="T57">tu͡ok</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_202" n="HIAT:w" s="T58">du</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_205" n="HIAT:w" s="T59">innʼe</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_208" n="HIAT:w" s="T60">ikkis</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_211" n="HIAT:w" s="T61">e</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_214" n="HIAT:w" s="T62">ɨlla</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_217" n="HIAT:w" s="T63">daganɨ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_220" n="HIAT:w" s="T64">bütün</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_223" n="HIAT:w" s="T65">karzinanɨ</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T76" id="Seg_226" n="sc" s="T67">
               <ts e="T76" id="Seg_228" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_230" n="HIAT:w" s="T67">Bilisipi͡ediger</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_233" n="HIAT:w" s="T68">tiːjen</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_236" n="HIAT:w" s="T69">baran</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_239" n="HIAT:w" s="T70">orok</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_242" n="HIAT:w" s="T71">üstün</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_245" n="HIAT:w" s="T72">bara</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_248" n="HIAT:w" s="T73">turar</ts>
                  <nts id="Seg_249" n="HIAT:ip">,</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_252" n="HIAT:w" s="T74">baran</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_255" n="HIAT:w" s="T75">ihen-ihen</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T245" id="Seg_258" n="sc" s="T77">
               <ts e="T81" id="Seg_260" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_262" n="HIAT:w" s="T77">Utarɨ</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_265" n="HIAT:w" s="T78">kɨːh</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_268" n="HIAT:w" s="T79">ogonu</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_271" n="HIAT:w" s="T80">körsübüt</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_275" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_277" n="HIAT:w" s="T81">Eː</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_280" n="HIAT:w" s="T82">körbüt</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_283" n="HIAT:w" s="T83">onu</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_286" n="HIAT:w" s="T84">kördö</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_289" n="HIAT:w" s="T85">körbütünen</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_292" n="HIAT:w" s="T86">uhuguttan</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_295" n="HIAT:w" s="T87">taːska</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_298" n="HIAT:w" s="T88">keben</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_300" n="HIAT:ip">(</nts>
                  <ts e="T90" id="Seg_302" n="HIAT:w" s="T89">biːlisipi͡etitten</ts>
                  <nts id="Seg_303" n="HIAT:ip">)</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_306" n="HIAT:w" s="T90">eː</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_309" n="HIAT:w" s="T91">huːlan</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_312" n="HIAT:w" s="T92">tüspüt</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_315" n="HIAT:w" s="T93">grušalara</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_318" n="HIAT:w" s="T94">hirge</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_321" n="HIAT:w" s="T95">čekenijen</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_324" n="HIAT:w" s="T96">tüspütter</ts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_328" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_330" n="HIAT:w" s="T97">Ol</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_333" n="HIAT:w" s="T98">tebene</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_336" n="HIAT:w" s="T99">hɨttagɨna</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_339" n="HIAT:w" s="T100">üs</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_342" n="HIAT:w" s="T101">u͡ol</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_345" n="HIAT:w" s="T102">ogo</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_348" n="HIAT:w" s="T103">üs</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_351" n="HIAT:w" s="T104">u͡ol</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_354" n="HIAT:w" s="T105">ogo</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_357" n="HIAT:w" s="T106">körön</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_360" n="HIAT:w" s="T107">turbuttara</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_363" n="HIAT:w" s="T108">onu</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_367" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_369" n="HIAT:w" s="T109">Üs</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_372" n="HIAT:w" s="T110">u͡ol</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_375" n="HIAT:w" s="T111">ogo</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_377" n="HIAT:ip">(</nts>
                  <ts e="T113" id="Seg_379" n="HIAT:w" s="T112">kele-</ts>
                  <nts id="Seg_380" n="HIAT:ip">)</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_383" n="HIAT:w" s="T113">onuga</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_386" n="HIAT:w" s="T114">kelenner</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_389" n="HIAT:w" s="T115">grušalarɨn</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_392" n="HIAT:w" s="T116">komujbuttar</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_395" n="HIAT:w" s="T117">bejetin</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_398" n="HIAT:w" s="T118">kumaktan</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_401" n="HIAT:w" s="T119">tebeːbitter</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_404" n="HIAT:w" s="T120">innʼe</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_407" n="HIAT:w" s="T121">gɨnan</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_410" n="HIAT:w" s="T122">baran</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_414" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_416" n="HIAT:w" s="T123">Dalʼse</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_419" n="HIAT:w" s="T124">gini</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_422" n="HIAT:w" s="T125">orogun</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_425" n="HIAT:w" s="T126">üstün</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_428" n="HIAT:w" s="T127">kaːman</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_431" n="HIAT:w" s="T128">ispitter</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_434" n="HIAT:w" s="T129">ol</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_437" n="HIAT:w" s="T130">kaːman</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_439" n="HIAT:ip">(</nts>
                  <ts e="T132" id="Seg_441" n="HIAT:w" s="T131">is-</ts>
                  <nts id="Seg_442" n="HIAT:ip">)</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_445" n="HIAT:w" s="T132">ihenner</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_448" n="HIAT:w" s="T133">bu͡o</ts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_452" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_454" n="HIAT:w" s="T134">Eː</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_457" n="HIAT:w" s="T135">kenniki</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_460" n="HIAT:w" s="T136">ogoloro</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_463" n="HIAT:w" s="T137">ke</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_466" n="HIAT:w" s="T138">iti</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_469" n="HIAT:w" s="T139">u͡ol</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_472" n="HIAT:w" s="T140">ogo</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_475" n="HIAT:w" s="T141">šljapatɨn</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_478" n="HIAT:w" s="T142">šljapalaːk</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_481" n="HIAT:w" s="T143">ete</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_485" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_487" n="HIAT:w" s="T144">Ol</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_490" n="HIAT:w" s="T145">šljapatɨn</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_493" n="HIAT:w" s="T146">bu͡o</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_496" n="HIAT:w" s="T147">kɨːhɨ</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_499" n="HIAT:w" s="T148">köröbün</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_502" n="HIAT:w" s="T149">di͡en</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_505" n="HIAT:w" s="T150">tüherbit</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_508" n="HIAT:w" s="T151">ete</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_511" n="HIAT:w" s="T152">ol</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_514" n="HIAT:w" s="T153">šljapagɨn</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_517" n="HIAT:w" s="T154">bulan</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_520" n="HIAT:w" s="T155">bu͡o</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_523" n="HIAT:w" s="T156">ol</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_526" n="HIAT:w" s="T157">u͡ol</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_529" n="HIAT:w" s="T158">ogogo</ts>
                  <nts id="Seg_530" n="HIAT:ip">.</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_533" n="HIAT:u" s="T159">
                  <nts id="Seg_534" n="HIAT:ip">(</nts>
                  <ts e="T160" id="Seg_536" n="HIAT:w" s="T159">Il-</ts>
                  <nts id="Seg_537" n="HIAT:ip">)</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_539" n="HIAT:ip">(</nts>
                  <ts e="T161" id="Seg_541" n="HIAT:w" s="T160">ilpit-</ts>
                  <nts id="Seg_542" n="HIAT:ip">)</nts>
                  <nts id="Seg_543" n="HIAT:ip">,</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_546" n="HIAT:w" s="T161">ihiːrbitter</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_548" n="HIAT:ip">(</nts>
                  <ts e="T163" id="Seg_550" n="HIAT:w" s="T162">mekt-</ts>
                  <nts id="Seg_551" n="HIAT:ip">)</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_554" n="HIAT:w" s="T163">maŋnaj</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_557" n="HIAT:w" s="T164">onton</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_560" n="HIAT:w" s="T165">u͡ol</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_563" n="HIAT:w" s="T166">ogoŋ</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_566" n="HIAT:w" s="T167">toktoːbut</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_570" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_572" n="HIAT:w" s="T168">Otton</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_575" n="HIAT:w" s="T169">bu͡ollagɨna</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_578" n="HIAT:w" s="T170">ol</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_581" n="HIAT:w" s="T171">bu͡o</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_584" n="HIAT:w" s="T172">kenniki</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_587" n="HIAT:w" s="T173">ogoŋ</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_590" n="HIAT:w" s="T174">bu͡olla</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_593" n="HIAT:w" s="T175">ol</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_596" n="HIAT:w" s="T176">kimin</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_599" n="HIAT:w" s="T177">šljapatɨn</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_602" n="HIAT:w" s="T178">bi͡ere</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_605" n="HIAT:w" s="T179">barbɨt</ts>
                  <nts id="Seg_606" n="HIAT:ip">,</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_609" n="HIAT:w" s="T180">innʼe</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_612" n="HIAT:w" s="T181">bi͡eren</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_615" n="HIAT:w" s="T182">baran</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_618" n="HIAT:w" s="T183">bu͡o</ts>
                  <nts id="Seg_619" n="HIAT:ip">.</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_622" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_624" n="HIAT:w" s="T184">Üs</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_627" n="HIAT:w" s="T185">üs</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_630" n="HIAT:w" s="T186">grušanɨ</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_633" n="HIAT:w" s="T187">ɨlbɨt</ts>
                  <nts id="Seg_634" n="HIAT:ip">,</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_637" n="HIAT:w" s="T188">i</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_640" n="HIAT:w" s="T189">buntuŋ</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_643" n="HIAT:w" s="T190">dalʼše</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_646" n="HIAT:w" s="T191">barbɨt</ts>
                  <nts id="Seg_647" n="HIAT:ip">,</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_650" n="HIAT:w" s="T192">onton</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_653" n="HIAT:w" s="T193">bular</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_656" n="HIAT:w" s="T194">bara</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_659" n="HIAT:w" s="T195">turdaktarɨna</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_662" n="HIAT:w" s="T196">bu͡o</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_665" n="HIAT:w" s="T197">sadovnik</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_668" n="HIAT:w" s="T198">ol</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_671" n="HIAT:w" s="T199">üktelitten</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_674" n="HIAT:w" s="T200">tühen</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_677" n="HIAT:w" s="T201">baran</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_680" n="HIAT:w" s="T202">grušalarɨn</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_683" n="HIAT:w" s="T203">nöŋü͡ö</ts>
                  <nts id="Seg_684" n="HIAT:ip">.</nts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_687" n="HIAT:u" s="T204">
                  <nts id="Seg_688" n="HIAT:ip">(</nts>
                  <ts e="T205" id="Seg_690" n="HIAT:w" s="T204">Ka-</ts>
                  <nts id="Seg_691" n="HIAT:ip">)</nts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_694" n="HIAT:w" s="T205">iččitek</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_697" n="HIAT:w" s="T206">karzinaga</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_700" n="HIAT:w" s="T207">grušalarɨn</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_703" n="HIAT:w" s="T208">ugaːrɨ</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_706" n="HIAT:w" s="T209">körbüte</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_709" n="HIAT:w" s="T210">biːr</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_712" n="HIAT:w" s="T211">karzinata</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_715" n="HIAT:w" s="T212">hu͡ok</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_718" n="HIAT:w" s="T213">aːkpɨta</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_721" n="HIAT:w" s="T214">biːr</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_724" n="HIAT:w" s="T215">karzinata</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_727" n="HIAT:w" s="T216">hu͡ok</ts>
                  <nts id="Seg_728" n="HIAT:ip">.</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_731" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_733" n="HIAT:w" s="T217">Ontuŋ</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_736" n="HIAT:w" s="T218">dumajdɨːr</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_739" n="HIAT:w" s="T219">horogor</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_742" n="HIAT:w" s="T220">kajdi͡egɨnan</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_745" n="HIAT:w" s="T221">kajdi͡ek</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_748" n="HIAT:w" s="T222">bartaj</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_751" n="HIAT:w" s="T223">diːr</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_754" n="HIAT:w" s="T224">ol</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_757" n="HIAT:w" s="T225">dumajdɨː</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_760" n="HIAT:w" s="T226">turdagɨna</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_763" n="HIAT:w" s="T227">bu͡o</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_766" n="HIAT:w" s="T228">baːjdiːn</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_769" n="HIAT:w" s="T229">üs</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_772" n="HIAT:w" s="T230">u͡ol</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_775" n="HIAT:w" s="T231">ogo</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_778" n="HIAT:w" s="T232">ɨksatɨnan</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_781" n="HIAT:w" s="T233">aːhan</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_784" n="HIAT:w" s="T234">aːspɨttar</ts>
                  <nts id="Seg_785" n="HIAT:ip">.</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_788" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_790" n="HIAT:w" s="T235">Gini</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_793" n="HIAT:w" s="T236">ɨksatɨnan</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_796" n="HIAT:w" s="T237">ke</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_799" n="HIAT:w" s="T238">oloru</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_802" n="HIAT:w" s="T239">kördö</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_805" n="HIAT:w" s="T240">körbütünen</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_808" n="HIAT:w" s="T241">honon</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_811" n="HIAT:w" s="T242">kaːlbɨt</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_814" n="HIAT:w" s="T243">sadovnik</ts>
                  <nts id="Seg_815" n="HIAT:ip">,</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_818" n="HIAT:w" s="T244">elete</ts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T66" id="Seg_821" n="sc" s="T1">
               <ts e="T2" id="Seg_823" n="e" s="T1">(Sad-) </ts>
               <ts e="T3" id="Seg_825" n="e" s="T2">eː </ts>
               <ts e="T4" id="Seg_827" n="e" s="T3">(sad-) </ts>
               <ts e="T5" id="Seg_829" n="e" s="T4">sadovnik </ts>
               <ts e="T6" id="Seg_831" n="e" s="T5">sadovnik </ts>
               <ts e="T7" id="Seg_833" n="e" s="T6">grušalarɨ </ts>
               <ts e="T8" id="Seg_835" n="e" s="T7">komujar. </ts>
               <ts e="T9" id="Seg_837" n="e" s="T8">Biːr </ts>
               <ts e="T10" id="Seg_839" n="e" s="T9">karzina </ts>
               <ts e="T11" id="Seg_841" n="e" s="T10">grušanɨ </ts>
               <ts e="T12" id="Seg_843" n="e" s="T11">komujbut </ts>
               <ts e="T13" id="Seg_845" n="e" s="T12">ol </ts>
               <ts e="T14" id="Seg_847" n="e" s="T13">komuja </ts>
               <ts e="T15" id="Seg_849" n="e" s="T14">hɨttagɨna </ts>
               <ts e="T16" id="Seg_851" n="e" s="T15">bu͡o </ts>
               <ts e="T17" id="Seg_853" n="e" s="T16">ɨksatɨnan </ts>
               <ts e="T18" id="Seg_855" n="e" s="T17">bu͡ollagɨna. </ts>
               <ts e="T19" id="Seg_857" n="e" s="T18">Kazaːlaːk </ts>
               <ts e="T20" id="Seg_859" n="e" s="T19">kihi </ts>
               <ts e="T21" id="Seg_861" n="e" s="T20">aːspɨta. </ts>
               <ts e="T22" id="Seg_863" n="e" s="T21">Oːnton </ts>
               <ts e="T23" id="Seg_865" n="e" s="T22">(ešč-) </ts>
               <ts e="T24" id="Seg_867" n="e" s="T23">ešo </ts>
               <ts e="T25" id="Seg_869" n="e" s="T24">da </ts>
               <ts e="T26" id="Seg_871" n="e" s="T25">(ɨttɨ-) </ts>
               <ts e="T27" id="Seg_873" n="e" s="T26">(ɨttɨbɨ-) </ts>
               <ts e="T28" id="Seg_875" n="e" s="T27">maska </ts>
               <ts e="T29" id="Seg_877" n="e" s="T28">ɨttɨbɨt </ts>
               <ts e="T30" id="Seg_879" n="e" s="T29">ešo </ts>
               <ts e="T31" id="Seg_881" n="e" s="T30">da </ts>
               <ts e="T32" id="Seg_883" n="e" s="T31">gruša </ts>
               <ts e="T33" id="Seg_885" n="e" s="T32">komujaːrɨ. </ts>
               <ts e="T34" id="Seg_887" n="e" s="T33">Ol </ts>
               <ts e="T35" id="Seg_889" n="e" s="T34">hɨttagɨna </ts>
               <ts e="T36" id="Seg_891" n="e" s="T35">bu͡o </ts>
               <ts e="T37" id="Seg_893" n="e" s="T36">ol </ts>
               <ts e="T38" id="Seg_895" n="e" s="T37">di͡egitten </ts>
               <ts e="T39" id="Seg_897" n="e" s="T38">bilisipi͡etteːk </ts>
               <ts e="T40" id="Seg_899" n="e" s="T39">u͡ol </ts>
               <ts e="T41" id="Seg_901" n="e" s="T40">ogo </ts>
               <ts e="T42" id="Seg_903" n="e" s="T41">kelbit, </ts>
               <ts e="T43" id="Seg_905" n="e" s="T42">iher. </ts>
               <ts e="T44" id="Seg_907" n="e" s="T43">Onton </ts>
               <ts e="T45" id="Seg_909" n="e" s="T44">itini </ts>
               <ts e="T46" id="Seg_911" n="e" s="T45">gini͡eke </ts>
               <ts e="T47" id="Seg_913" n="e" s="T46">kelen </ts>
               <ts e="T48" id="Seg_915" n="e" s="T47">bu͡ollagɨna </ts>
               <ts e="T49" id="Seg_917" n="e" s="T48">grušalarɨ </ts>
               <ts e="T50" id="Seg_919" n="e" s="T49">körön </ts>
               <ts e="T51" id="Seg_921" n="e" s="T50">biːr </ts>
               <ts e="T52" id="Seg_923" n="e" s="T51">grušanɨ </ts>
               <ts e="T53" id="Seg_925" n="e" s="T52">ɨlaːrɨ </ts>
               <ts e="T54" id="Seg_927" n="e" s="T53">gɨmmɨta, </ts>
               <ts e="T55" id="Seg_929" n="e" s="T54">ontubut </ts>
               <ts e="T56" id="Seg_931" n="e" s="T55">herener </ts>
               <ts e="T57" id="Seg_933" n="e" s="T56">du </ts>
               <ts e="T58" id="Seg_935" n="e" s="T57">tu͡ok </ts>
               <ts e="T59" id="Seg_937" n="e" s="T58">du </ts>
               <ts e="T60" id="Seg_939" n="e" s="T59">innʼe </ts>
               <ts e="T61" id="Seg_941" n="e" s="T60">ikkis </ts>
               <ts e="T62" id="Seg_943" n="e" s="T61">e </ts>
               <ts e="T63" id="Seg_945" n="e" s="T62">ɨlla </ts>
               <ts e="T64" id="Seg_947" n="e" s="T63">daganɨ </ts>
               <ts e="T65" id="Seg_949" n="e" s="T64">bütün </ts>
               <ts e="T66" id="Seg_951" n="e" s="T65">karzinanɨ. </ts>
            </ts>
            <ts e="T76" id="Seg_952" n="sc" s="T67">
               <ts e="T68" id="Seg_954" n="e" s="T67">Bilisipi͡ediger </ts>
               <ts e="T69" id="Seg_956" n="e" s="T68">tiːjen </ts>
               <ts e="T70" id="Seg_958" n="e" s="T69">baran </ts>
               <ts e="T71" id="Seg_960" n="e" s="T70">orok </ts>
               <ts e="T72" id="Seg_962" n="e" s="T71">üstün </ts>
               <ts e="T73" id="Seg_964" n="e" s="T72">bara </ts>
               <ts e="T74" id="Seg_966" n="e" s="T73">turar, </ts>
               <ts e="T75" id="Seg_968" n="e" s="T74">baran </ts>
               <ts e="T76" id="Seg_970" n="e" s="T75">ihen-ihen. </ts>
            </ts>
            <ts e="T245" id="Seg_971" n="sc" s="T77">
               <ts e="T78" id="Seg_973" n="e" s="T77">Utarɨ </ts>
               <ts e="T79" id="Seg_975" n="e" s="T78">kɨːh </ts>
               <ts e="T80" id="Seg_977" n="e" s="T79">ogonu </ts>
               <ts e="T81" id="Seg_979" n="e" s="T80">körsübüt. </ts>
               <ts e="T82" id="Seg_981" n="e" s="T81">Eː </ts>
               <ts e="T83" id="Seg_983" n="e" s="T82">körbüt </ts>
               <ts e="T84" id="Seg_985" n="e" s="T83">onu </ts>
               <ts e="T85" id="Seg_987" n="e" s="T84">kördö </ts>
               <ts e="T86" id="Seg_989" n="e" s="T85">körbütünen </ts>
               <ts e="T87" id="Seg_991" n="e" s="T86">uhuguttan </ts>
               <ts e="T88" id="Seg_993" n="e" s="T87">taːska </ts>
               <ts e="T89" id="Seg_995" n="e" s="T88">keben </ts>
               <ts e="T90" id="Seg_997" n="e" s="T89">(biːlisipi͡etitten) </ts>
               <ts e="T91" id="Seg_999" n="e" s="T90">eː </ts>
               <ts e="T92" id="Seg_1001" n="e" s="T91">huːlan </ts>
               <ts e="T93" id="Seg_1003" n="e" s="T92">tüspüt </ts>
               <ts e="T94" id="Seg_1005" n="e" s="T93">grušalara </ts>
               <ts e="T95" id="Seg_1007" n="e" s="T94">hirge </ts>
               <ts e="T96" id="Seg_1009" n="e" s="T95">čekenijen </ts>
               <ts e="T97" id="Seg_1011" n="e" s="T96">tüspütter. </ts>
               <ts e="T98" id="Seg_1013" n="e" s="T97">Ol </ts>
               <ts e="T99" id="Seg_1015" n="e" s="T98">tebene </ts>
               <ts e="T100" id="Seg_1017" n="e" s="T99">hɨttagɨna </ts>
               <ts e="T101" id="Seg_1019" n="e" s="T100">üs </ts>
               <ts e="T102" id="Seg_1021" n="e" s="T101">u͡ol </ts>
               <ts e="T103" id="Seg_1023" n="e" s="T102">ogo </ts>
               <ts e="T104" id="Seg_1025" n="e" s="T103">üs </ts>
               <ts e="T105" id="Seg_1027" n="e" s="T104">u͡ol </ts>
               <ts e="T106" id="Seg_1029" n="e" s="T105">ogo </ts>
               <ts e="T107" id="Seg_1031" n="e" s="T106">körön </ts>
               <ts e="T108" id="Seg_1033" n="e" s="T107">turbuttara </ts>
               <ts e="T109" id="Seg_1035" n="e" s="T108">onu. </ts>
               <ts e="T110" id="Seg_1037" n="e" s="T109">Üs </ts>
               <ts e="T111" id="Seg_1039" n="e" s="T110">u͡ol </ts>
               <ts e="T112" id="Seg_1041" n="e" s="T111">ogo </ts>
               <ts e="T113" id="Seg_1043" n="e" s="T112">(kele-) </ts>
               <ts e="T114" id="Seg_1045" n="e" s="T113">onuga </ts>
               <ts e="T115" id="Seg_1047" n="e" s="T114">kelenner </ts>
               <ts e="T116" id="Seg_1049" n="e" s="T115">grušalarɨn </ts>
               <ts e="T117" id="Seg_1051" n="e" s="T116">komujbuttar </ts>
               <ts e="T118" id="Seg_1053" n="e" s="T117">bejetin </ts>
               <ts e="T119" id="Seg_1055" n="e" s="T118">kumaktan </ts>
               <ts e="T120" id="Seg_1057" n="e" s="T119">tebeːbitter </ts>
               <ts e="T121" id="Seg_1059" n="e" s="T120">innʼe </ts>
               <ts e="T122" id="Seg_1061" n="e" s="T121">gɨnan </ts>
               <ts e="T123" id="Seg_1063" n="e" s="T122">baran. </ts>
               <ts e="T124" id="Seg_1065" n="e" s="T123">Dalʼse </ts>
               <ts e="T125" id="Seg_1067" n="e" s="T124">gini </ts>
               <ts e="T126" id="Seg_1069" n="e" s="T125">orogun </ts>
               <ts e="T127" id="Seg_1071" n="e" s="T126">üstün </ts>
               <ts e="T128" id="Seg_1073" n="e" s="T127">kaːman </ts>
               <ts e="T129" id="Seg_1075" n="e" s="T128">ispitter </ts>
               <ts e="T130" id="Seg_1077" n="e" s="T129">ol </ts>
               <ts e="T131" id="Seg_1079" n="e" s="T130">kaːman </ts>
               <ts e="T132" id="Seg_1081" n="e" s="T131">(is-) </ts>
               <ts e="T133" id="Seg_1083" n="e" s="T132">ihenner </ts>
               <ts e="T134" id="Seg_1085" n="e" s="T133">bu͡o. </ts>
               <ts e="T135" id="Seg_1087" n="e" s="T134">Eː </ts>
               <ts e="T136" id="Seg_1089" n="e" s="T135">kenniki </ts>
               <ts e="T137" id="Seg_1091" n="e" s="T136">ogoloro </ts>
               <ts e="T138" id="Seg_1093" n="e" s="T137">ke </ts>
               <ts e="T139" id="Seg_1095" n="e" s="T138">iti </ts>
               <ts e="T140" id="Seg_1097" n="e" s="T139">u͡ol </ts>
               <ts e="T141" id="Seg_1099" n="e" s="T140">ogo </ts>
               <ts e="T142" id="Seg_1101" n="e" s="T141">šljapatɨn </ts>
               <ts e="T143" id="Seg_1103" n="e" s="T142">šljapalaːk </ts>
               <ts e="T144" id="Seg_1105" n="e" s="T143">ete. </ts>
               <ts e="T145" id="Seg_1107" n="e" s="T144">Ol </ts>
               <ts e="T146" id="Seg_1109" n="e" s="T145">šljapatɨn </ts>
               <ts e="T147" id="Seg_1111" n="e" s="T146">bu͡o </ts>
               <ts e="T148" id="Seg_1113" n="e" s="T147">kɨːhɨ </ts>
               <ts e="T149" id="Seg_1115" n="e" s="T148">köröbün </ts>
               <ts e="T150" id="Seg_1117" n="e" s="T149">di͡en </ts>
               <ts e="T151" id="Seg_1119" n="e" s="T150">tüherbit </ts>
               <ts e="T152" id="Seg_1121" n="e" s="T151">ete </ts>
               <ts e="T153" id="Seg_1123" n="e" s="T152">ol </ts>
               <ts e="T154" id="Seg_1125" n="e" s="T153">šljapagɨn </ts>
               <ts e="T155" id="Seg_1127" n="e" s="T154">bulan </ts>
               <ts e="T156" id="Seg_1129" n="e" s="T155">bu͡o </ts>
               <ts e="T157" id="Seg_1131" n="e" s="T156">ol </ts>
               <ts e="T158" id="Seg_1133" n="e" s="T157">u͡ol </ts>
               <ts e="T159" id="Seg_1135" n="e" s="T158">ogogo. </ts>
               <ts e="T160" id="Seg_1137" n="e" s="T159">(Il-) </ts>
               <ts e="T161" id="Seg_1139" n="e" s="T160">(ilpit-), </ts>
               <ts e="T162" id="Seg_1141" n="e" s="T161">ihiːrbitter </ts>
               <ts e="T163" id="Seg_1143" n="e" s="T162">(mekt-) </ts>
               <ts e="T164" id="Seg_1145" n="e" s="T163">maŋnaj </ts>
               <ts e="T165" id="Seg_1147" n="e" s="T164">onton </ts>
               <ts e="T166" id="Seg_1149" n="e" s="T165">u͡ol </ts>
               <ts e="T167" id="Seg_1151" n="e" s="T166">ogoŋ </ts>
               <ts e="T168" id="Seg_1153" n="e" s="T167">toktoːbut. </ts>
               <ts e="T169" id="Seg_1155" n="e" s="T168">Otton </ts>
               <ts e="T170" id="Seg_1157" n="e" s="T169">bu͡ollagɨna </ts>
               <ts e="T171" id="Seg_1159" n="e" s="T170">ol </ts>
               <ts e="T172" id="Seg_1161" n="e" s="T171">bu͡o </ts>
               <ts e="T173" id="Seg_1163" n="e" s="T172">kenniki </ts>
               <ts e="T174" id="Seg_1165" n="e" s="T173">ogoŋ </ts>
               <ts e="T175" id="Seg_1167" n="e" s="T174">bu͡olla </ts>
               <ts e="T176" id="Seg_1169" n="e" s="T175">ol </ts>
               <ts e="T177" id="Seg_1171" n="e" s="T176">kimin </ts>
               <ts e="T178" id="Seg_1173" n="e" s="T177">šljapatɨn </ts>
               <ts e="T179" id="Seg_1175" n="e" s="T178">bi͡ere </ts>
               <ts e="T180" id="Seg_1177" n="e" s="T179">barbɨt, </ts>
               <ts e="T181" id="Seg_1179" n="e" s="T180">innʼe </ts>
               <ts e="T182" id="Seg_1181" n="e" s="T181">bi͡eren </ts>
               <ts e="T183" id="Seg_1183" n="e" s="T182">baran </ts>
               <ts e="T184" id="Seg_1185" n="e" s="T183">bu͡o. </ts>
               <ts e="T185" id="Seg_1187" n="e" s="T184">Üs </ts>
               <ts e="T186" id="Seg_1189" n="e" s="T185">üs </ts>
               <ts e="T187" id="Seg_1191" n="e" s="T186">grušanɨ </ts>
               <ts e="T188" id="Seg_1193" n="e" s="T187">ɨlbɨt, </ts>
               <ts e="T189" id="Seg_1195" n="e" s="T188">i </ts>
               <ts e="T190" id="Seg_1197" n="e" s="T189">buntuŋ </ts>
               <ts e="T191" id="Seg_1199" n="e" s="T190">dalʼše </ts>
               <ts e="T192" id="Seg_1201" n="e" s="T191">barbɨt, </ts>
               <ts e="T193" id="Seg_1203" n="e" s="T192">onton </ts>
               <ts e="T194" id="Seg_1205" n="e" s="T193">bular </ts>
               <ts e="T195" id="Seg_1207" n="e" s="T194">bara </ts>
               <ts e="T196" id="Seg_1209" n="e" s="T195">turdaktarɨna </ts>
               <ts e="T197" id="Seg_1211" n="e" s="T196">bu͡o </ts>
               <ts e="T198" id="Seg_1213" n="e" s="T197">sadovnik </ts>
               <ts e="T199" id="Seg_1215" n="e" s="T198">ol </ts>
               <ts e="T200" id="Seg_1217" n="e" s="T199">üktelitten </ts>
               <ts e="T201" id="Seg_1219" n="e" s="T200">tühen </ts>
               <ts e="T202" id="Seg_1221" n="e" s="T201">baran </ts>
               <ts e="T203" id="Seg_1223" n="e" s="T202">grušalarɨn </ts>
               <ts e="T204" id="Seg_1225" n="e" s="T203">nöŋü͡ö. </ts>
               <ts e="T205" id="Seg_1227" n="e" s="T204">(Ka-) </ts>
               <ts e="T206" id="Seg_1229" n="e" s="T205">iččitek </ts>
               <ts e="T207" id="Seg_1231" n="e" s="T206">karzinaga </ts>
               <ts e="T208" id="Seg_1233" n="e" s="T207">grušalarɨn </ts>
               <ts e="T209" id="Seg_1235" n="e" s="T208">ugaːrɨ </ts>
               <ts e="T210" id="Seg_1237" n="e" s="T209">körbüte </ts>
               <ts e="T211" id="Seg_1239" n="e" s="T210">biːr </ts>
               <ts e="T212" id="Seg_1241" n="e" s="T211">karzinata </ts>
               <ts e="T213" id="Seg_1243" n="e" s="T212">hu͡ok </ts>
               <ts e="T214" id="Seg_1245" n="e" s="T213">aːkpɨta </ts>
               <ts e="T215" id="Seg_1247" n="e" s="T214">biːr </ts>
               <ts e="T216" id="Seg_1249" n="e" s="T215">karzinata </ts>
               <ts e="T217" id="Seg_1251" n="e" s="T216">hu͡ok. </ts>
               <ts e="T218" id="Seg_1253" n="e" s="T217">Ontuŋ </ts>
               <ts e="T219" id="Seg_1255" n="e" s="T218">dumajdɨːr </ts>
               <ts e="T220" id="Seg_1257" n="e" s="T219">horogor </ts>
               <ts e="T221" id="Seg_1259" n="e" s="T220">kajdi͡egɨnan </ts>
               <ts e="T222" id="Seg_1261" n="e" s="T221">kajdi͡ek </ts>
               <ts e="T223" id="Seg_1263" n="e" s="T222">bartaj </ts>
               <ts e="T224" id="Seg_1265" n="e" s="T223">diːr </ts>
               <ts e="T225" id="Seg_1267" n="e" s="T224">ol </ts>
               <ts e="T226" id="Seg_1269" n="e" s="T225">dumajdɨː </ts>
               <ts e="T227" id="Seg_1271" n="e" s="T226">turdagɨna </ts>
               <ts e="T228" id="Seg_1273" n="e" s="T227">bu͡o </ts>
               <ts e="T229" id="Seg_1275" n="e" s="T228">baːjdiːn </ts>
               <ts e="T230" id="Seg_1277" n="e" s="T229">üs </ts>
               <ts e="T231" id="Seg_1279" n="e" s="T230">u͡ol </ts>
               <ts e="T232" id="Seg_1281" n="e" s="T231">ogo </ts>
               <ts e="T233" id="Seg_1283" n="e" s="T232">ɨksatɨnan </ts>
               <ts e="T234" id="Seg_1285" n="e" s="T233">aːhan </ts>
               <ts e="T235" id="Seg_1287" n="e" s="T234">aːspɨttar. </ts>
               <ts e="T236" id="Seg_1289" n="e" s="T235">Gini </ts>
               <ts e="T237" id="Seg_1291" n="e" s="T236">ɨksatɨnan </ts>
               <ts e="T238" id="Seg_1293" n="e" s="T237">ke </ts>
               <ts e="T239" id="Seg_1295" n="e" s="T238">oloru </ts>
               <ts e="T240" id="Seg_1297" n="e" s="T239">kördö </ts>
               <ts e="T241" id="Seg_1299" n="e" s="T240">körbütünen </ts>
               <ts e="T242" id="Seg_1301" n="e" s="T241">honon </ts>
               <ts e="T243" id="Seg_1303" n="e" s="T242">kaːlbɨt </ts>
               <ts e="T244" id="Seg_1305" n="e" s="T243">sadovnik, </ts>
               <ts e="T245" id="Seg_1307" n="e" s="T244">elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_1308" s="T1">AnIM_2009_Pear_nar.001 (001)</ta>
            <ta e="T18" id="Seg_1309" s="T8">AnIM_2009_Pear_nar.002 (002)</ta>
            <ta e="T21" id="Seg_1310" s="T18">AnIM_2009_Pear_nar.003 (003)</ta>
            <ta e="T33" id="Seg_1311" s="T21">AnIM_2009_Pear_nar.004 (004)</ta>
            <ta e="T43" id="Seg_1312" s="T33">AnIM_2009_Pear_nar.005 (005)</ta>
            <ta e="T66" id="Seg_1313" s="T43">AnIM_2009_Pear_nar.006 (006)</ta>
            <ta e="T76" id="Seg_1314" s="T67">AnIM_2009_Pear_nar.007 (007)</ta>
            <ta e="T81" id="Seg_1315" s="T77">AnIM_2009_Pear_nar.008 (008)</ta>
            <ta e="T97" id="Seg_1316" s="T81">AnIM_2009_Pear_nar.009 (009)</ta>
            <ta e="T109" id="Seg_1317" s="T97">AnIM_2009_Pear_nar.010 (010)</ta>
            <ta e="T123" id="Seg_1318" s="T109">AnIM_2009_Pear_nar.011 (011)</ta>
            <ta e="T134" id="Seg_1319" s="T123">AnIM_2009_Pear_nar.012 (012)</ta>
            <ta e="T144" id="Seg_1320" s="T134">AnIM_2009_Pear_nar.013 (013)</ta>
            <ta e="T159" id="Seg_1321" s="T144">AnIM_2009_Pear_nar.014 (014)</ta>
            <ta e="T168" id="Seg_1322" s="T159">AnIM_2009_Pear_nar.015 (015)</ta>
            <ta e="T184" id="Seg_1323" s="T168">AnIM_2009_Pear_nar.016 (016)</ta>
            <ta e="T204" id="Seg_1324" s="T184">AnIM_2009_Pear_nar.017 (017)</ta>
            <ta e="T217" id="Seg_1325" s="T204">AnIM_2009_Pear_nar.018 (018)</ta>
            <ta e="T235" id="Seg_1326" s="T217">AnIM_2009_Pear_nar.019 (019)</ta>
            <ta e="T245" id="Seg_1327" s="T235">AnIM_2009_Pear_nar.020 (020)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T8" id="Seg_1328" s="T1">Сад ээ сад садовник садовник грушалары комуйар</ta>
            <ta e="T18" id="Seg_1329" s="T8">Биир карзина грушаны комуйбут ол комуйа һыттагына буо ыксатына буллагына</ta>
            <ta e="T21" id="Seg_1330" s="T18">Казаалак киһи ааспыта</ta>
            <ta e="T33" id="Seg_1331" s="T21">о-о-онтон ещ.. ещё да ытты.. ыттыбы маска ыттыбыт ещё да груша комуйары</ta>
            <ta e="T43" id="Seg_1332" s="T33">ол һыттагына буо ол диэгиттэн билисипиэттэк уол ого кэлбит, иһэр. </ta>
            <ta e="T66" id="Seg_1333" s="T43">отон итини гиниэкэ кэлэн буоллагына грушалары көрөн биир грушаны ылаары гыммыта онтубут һэрэнэр ду туок ду иннэ иккис э ылла даганы бүтүн карзинаны</ta>
            <ta e="T76" id="Seg_1334" s="T67">билисипиэдигэр тийэн баран орок үстүн бара турар, баран иһэн-иһэн </ta>
            <ta e="T81" id="Seg_1335" s="T77">утары кыыһ огону көрсүбүт</ta>
            <ta e="T97" id="Seg_1336" s="T81">ээ көрбүт ону көрдө-көрбүтүнэн уһугуттан тааска кэбэн биилисипиэтит… ээ һуулан түспүт грушалара һиргэ чэкэнийэн түспүттэр</ta>
            <ta e="T109" id="Seg_1337" s="T97">ол тэбэнэ һыттагына үс уол ого.. үс уол ого көрөн турбуттара ону</ta>
            <ta e="T123" id="Seg_1338" s="T109">үс уол ого кэлэ.. онуга кэлэннэр грушаларын комуйбуттар бэйэтин кумактан тэбээбиттэр иннэ гынан баран</ta>
            <ta e="T134" id="Seg_1339" s="T123">дальсе гини орогун үстүн кааман испиттэр ол кааман ис.. иһэннэр буо</ta>
            <ta e="T144" id="Seg_1340" s="T134">ээ кэнники оголоро кэ ити уол ого шляпатын шляпалак этэ</ta>
            <ta e="T159" id="Seg_1341" s="T144">ол шляпатын буо кыһы көрөбүн диэн түһэрбит этэ ол шляпагын булан буо ол уол огого</ta>
            <ta e="T168" id="Seg_1342" s="T159">ил.. илпит, иһирбиттэр мэкт.. маӈнай онтон уол огоӈ токтообут</ta>
            <ta e="T184" id="Seg_1343" s="T168">оттон буоллагына ол буо кэнники огоӈ буолла ол кимин шляпатын биэрэ барбыт, инэ биэрэн баран буо</ta>
            <ta e="T204" id="Seg_1344" s="T184">үс үс грушаны ылбыт, и бунтуӈ дальше барбыт, отон булар бара турдактарына буо садовник ол үктэлиттэн түһэн баран грушалын ноӈүө</ta>
            <ta e="T217" id="Seg_1345" s="T204">ка.. иччитэк карзинага грушаларын угаары көрбүтэ биир карзината һуок акпыта биир карзината һуок </ta>
            <ta e="T235" id="Seg_1346" s="T217">онтуӈ думайдыр һорогор кайдиэгынан кайдиэк бартай диир ол думайдыы турдагына буо байди үс уол ого ыксатынан ааһан ааспыттар</ta>
            <ta e="T245" id="Seg_1347" s="T235">гини ыксатынан кэ олору көрдө көрбүтүнэн һонон каалбыт садовник,элэтэ</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_1348" s="T1">(Sad-) eː (sad-) sadovnik sadovnik grušalarɨ komujar. </ta>
            <ta e="T18" id="Seg_1349" s="T8">Biːr karzina grušanɨ komujbut ol komuja hɨttagɨna bu͡o ɨksatɨnan bu͡ollagɨna. </ta>
            <ta e="T21" id="Seg_1350" s="T18">Kazaːlaːk kihi aːspɨta. </ta>
            <ta e="T33" id="Seg_1351" s="T21">Oːnton (ešč-) ešo da (ɨttɨ-) (ɨttɨbɨ-) maska ɨttɨbɨt ešo da gruša komujaːrɨ. </ta>
            <ta e="T43" id="Seg_1352" s="T33">Ol hɨttagɨna bu͡o ol di͡egitten bilisipi͡etteːk u͡ol ogo kelbit, iher. </ta>
            <ta e="T66" id="Seg_1353" s="T43">Onton itini gini͡eke kelen bu͡ollagɨna grušalarɨ körön biːr grušanɨ ɨlaːrɨ gɨmmɨta, ontubut herener du tu͡ok du innʼe ikkis e ɨlla daganɨ bütün karzinanɨ. </ta>
            <ta e="T76" id="Seg_1354" s="T67">Bilisipi͡ediger tiːjen baran orok üstün bara turar, baran ihen-ihen. </ta>
            <ta e="T81" id="Seg_1355" s="T77">Utarɨ kɨːh ogonu körsübüt. </ta>
            <ta e="T97" id="Seg_1356" s="T81">Eː körbüt onu kördö körbütünen uhuguttan taːska keben (biːlisipi͡etitten) eː huːlan tüspüt grušalara hirge čekenijen tüspütter. </ta>
            <ta e="T109" id="Seg_1357" s="T97">Ol tebene hɨttagɨna üs u͡ol ogo üs u͡ol ogo körön turbuttara onu. </ta>
            <ta e="T123" id="Seg_1358" s="T109">Üs u͡ol ogo (kele-) onuga kelenner grušalarɨn komujbuttar bejetin kumaktan tebeːbitter innʼe gɨnan baran. </ta>
            <ta e="T134" id="Seg_1359" s="T123">Dalʼse gini orogun üstün kaːman ispitter ol kaːman (is-) ihenner bu͡o. </ta>
            <ta e="T144" id="Seg_1360" s="T134">Eː kenniki ogoloro ke iti u͡ol ogo šljapatɨn šljapalaːk ete. </ta>
            <ta e="T159" id="Seg_1361" s="T144">Ol šljapatɨn bu͡o kɨːhɨ köröbün di͡en tüherbit ete ol šljapagɨn bulan bu͡o ol u͡ol ogogo. </ta>
            <ta e="T168" id="Seg_1362" s="T159">(Il-) (ilpit-), ihiːrbitter (mekt-) maŋnaj onton u͡ol ogoŋ toktoːbut. </ta>
            <ta e="T184" id="Seg_1363" s="T168">Otton bu͡ollagɨna ol bu͡o kenniki ogoŋ bu͡olla ol kimin šljapatɨn bi͡ere barbɨt, innʼe bi͡eren baran bu͡o. </ta>
            <ta e="T204" id="Seg_1364" s="T184">Üs üs grušanɨ ɨlbɨt, i buntuŋ dalʼše barbɨt, onton bular bara turdaktarɨna bu͡o sadovnik ol üktelitten tühen baran grušalarɨn nöŋü͡ö. </ta>
            <ta e="T217" id="Seg_1365" s="T204">(Ka-) iččitek karzinaga grušalarɨn ugaːrɨ körbüte biːr karzinata hu͡ok aːkpɨta biːr karzinata hu͡ok. </ta>
            <ta e="T235" id="Seg_1366" s="T217">Ontuŋ dumajdɨːr horogor kajdi͡egɨnan kajdi͡ek bartaj diːr ol dumajdɨː turdagɨna bu͡o baːjdiːn üs u͡ol ogo ɨksatɨnan aːhan aːspɨttar. </ta>
            <ta e="T245" id="Seg_1367" s="T235">Gini ɨksatɨnan ke oloru kördö körbütünen honon kaːlbɨt sadovnik, elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T3" id="Seg_1368" s="T2">eː</ta>
            <ta e="T5" id="Seg_1369" s="T4">sadovnik</ta>
            <ta e="T6" id="Seg_1370" s="T5">sadovnik</ta>
            <ta e="T7" id="Seg_1371" s="T6">gruša-lar-ɨ</ta>
            <ta e="T8" id="Seg_1372" s="T7">komuj-ar</ta>
            <ta e="T9" id="Seg_1373" s="T8">biːr</ta>
            <ta e="T10" id="Seg_1374" s="T9">karzina</ta>
            <ta e="T11" id="Seg_1375" s="T10">gruša-nɨ</ta>
            <ta e="T12" id="Seg_1376" s="T11">komuj-but</ta>
            <ta e="T13" id="Seg_1377" s="T12">ol</ta>
            <ta e="T14" id="Seg_1378" s="T13">komuj-a</ta>
            <ta e="T15" id="Seg_1379" s="T14">hɨt-tag-ɨna</ta>
            <ta e="T16" id="Seg_1380" s="T15">bu͡o</ta>
            <ta e="T17" id="Seg_1381" s="T16">ɨksa-tɨ-nan</ta>
            <ta e="T18" id="Seg_1382" s="T17">bu͡ollagɨna</ta>
            <ta e="T19" id="Seg_1383" s="T18">kazaː-laːk</ta>
            <ta e="T20" id="Seg_1384" s="T19">kihi</ta>
            <ta e="T21" id="Seg_1385" s="T20">aːs-pɨt-a</ta>
            <ta e="T22" id="Seg_1386" s="T21">oːnton</ta>
            <ta e="T24" id="Seg_1387" s="T23">ešo</ta>
            <ta e="T25" id="Seg_1388" s="T24">da</ta>
            <ta e="T28" id="Seg_1389" s="T27">mas-ka</ta>
            <ta e="T29" id="Seg_1390" s="T28">ɨtt-ɨ-bɨt</ta>
            <ta e="T30" id="Seg_1391" s="T29">ešo</ta>
            <ta e="T31" id="Seg_1392" s="T30">da</ta>
            <ta e="T32" id="Seg_1393" s="T31">gruša</ta>
            <ta e="T33" id="Seg_1394" s="T32">komuj-aːrɨ</ta>
            <ta e="T34" id="Seg_1395" s="T33">ol</ta>
            <ta e="T35" id="Seg_1396" s="T34">hɨt-tag-ɨna</ta>
            <ta e="T36" id="Seg_1397" s="T35">bu͡o</ta>
            <ta e="T37" id="Seg_1398" s="T36">ol</ta>
            <ta e="T38" id="Seg_1399" s="T37">di͡egi-tten</ta>
            <ta e="T39" id="Seg_1400" s="T38">bilisipi͡et-teːk</ta>
            <ta e="T40" id="Seg_1401" s="T39">u͡ol</ta>
            <ta e="T41" id="Seg_1402" s="T40">ogo</ta>
            <ta e="T42" id="Seg_1403" s="T41">kel-bit</ta>
            <ta e="T43" id="Seg_1404" s="T42">ih-er</ta>
            <ta e="T44" id="Seg_1405" s="T43">onton</ta>
            <ta e="T45" id="Seg_1406" s="T44">iti-ni</ta>
            <ta e="T46" id="Seg_1407" s="T45">gini͡e-ke</ta>
            <ta e="T47" id="Seg_1408" s="T46">kel-en</ta>
            <ta e="T48" id="Seg_1409" s="T47">bu͡ollagɨna</ta>
            <ta e="T49" id="Seg_1410" s="T48">gruša-lar-ɨ</ta>
            <ta e="T50" id="Seg_1411" s="T49">kör-ön</ta>
            <ta e="T51" id="Seg_1412" s="T50">biːr</ta>
            <ta e="T52" id="Seg_1413" s="T51">gruša-nɨ</ta>
            <ta e="T53" id="Seg_1414" s="T52">ɨl-aːrɨ</ta>
            <ta e="T54" id="Seg_1415" s="T53">gɨm-mɨt-a</ta>
            <ta e="T55" id="Seg_1416" s="T54">on-tu-but</ta>
            <ta e="T56" id="Seg_1417" s="T55">heren-er</ta>
            <ta e="T57" id="Seg_1418" s="T56">du</ta>
            <ta e="T58" id="Seg_1419" s="T57">tu͡ok</ta>
            <ta e="T59" id="Seg_1420" s="T58">du</ta>
            <ta e="T60" id="Seg_1421" s="T59">innʼe</ta>
            <ta e="T61" id="Seg_1422" s="T60">ikki-s</ta>
            <ta e="T62" id="Seg_1423" s="T61">e</ta>
            <ta e="T63" id="Seg_1424" s="T62">ɨl-l-a</ta>
            <ta e="T64" id="Seg_1425" s="T63">daganɨ</ta>
            <ta e="T65" id="Seg_1426" s="T64">bütün</ta>
            <ta e="T66" id="Seg_1427" s="T65">karzina-nɨ</ta>
            <ta e="T68" id="Seg_1428" s="T67">bilisipi͡ed-i-ger</ta>
            <ta e="T69" id="Seg_1429" s="T68">tiːj-en</ta>
            <ta e="T70" id="Seg_1430" s="T69">bar-an</ta>
            <ta e="T71" id="Seg_1431" s="T70">orok</ta>
            <ta e="T72" id="Seg_1432" s="T71">üstün</ta>
            <ta e="T73" id="Seg_1433" s="T72">bar-a</ta>
            <ta e="T74" id="Seg_1434" s="T73">tur-ar</ta>
            <ta e="T75" id="Seg_1435" s="T74">bar-an</ta>
            <ta e="T76" id="Seg_1436" s="T75">ih-en-ih-en</ta>
            <ta e="T78" id="Seg_1437" s="T77">utarɨ</ta>
            <ta e="T79" id="Seg_1438" s="T78">kɨːh</ta>
            <ta e="T80" id="Seg_1439" s="T79">ogo-nu</ta>
            <ta e="T81" id="Seg_1440" s="T80">körs-ü-büt</ta>
            <ta e="T82" id="Seg_1441" s="T81">eː</ta>
            <ta e="T83" id="Seg_1442" s="T82">kör-büt</ta>
            <ta e="T84" id="Seg_1443" s="T83">o-nu</ta>
            <ta e="T85" id="Seg_1444" s="T84">kör-d-ö</ta>
            <ta e="T86" id="Seg_1445" s="T85">kör-büt-ü-nen</ta>
            <ta e="T87" id="Seg_1446" s="T86">uhug-u-ttan</ta>
            <ta e="T88" id="Seg_1447" s="T87">taːs-ka</ta>
            <ta e="T89" id="Seg_1448" s="T88">keb-en</ta>
            <ta e="T90" id="Seg_1449" s="T89">biːlisipi͡et-i-tten</ta>
            <ta e="T91" id="Seg_1450" s="T90">eː</ta>
            <ta e="T92" id="Seg_1451" s="T91">huːl-an</ta>
            <ta e="T93" id="Seg_1452" s="T92">tüs-püt</ta>
            <ta e="T94" id="Seg_1453" s="T93">gruša-lar-a</ta>
            <ta e="T95" id="Seg_1454" s="T94">hir-ge</ta>
            <ta e="T96" id="Seg_1455" s="T95">čekenij-en</ta>
            <ta e="T97" id="Seg_1456" s="T96">tüs-püt-ter</ta>
            <ta e="T98" id="Seg_1457" s="T97">ol</ta>
            <ta e="T99" id="Seg_1458" s="T98">teb-e-n-e</ta>
            <ta e="T100" id="Seg_1459" s="T99">hɨt-tag-ɨna</ta>
            <ta e="T101" id="Seg_1460" s="T100">üs</ta>
            <ta e="T102" id="Seg_1461" s="T101">u͡ol</ta>
            <ta e="T103" id="Seg_1462" s="T102">ogo</ta>
            <ta e="T104" id="Seg_1463" s="T103">üs</ta>
            <ta e="T105" id="Seg_1464" s="T104">u͡ol</ta>
            <ta e="T106" id="Seg_1465" s="T105">ogo</ta>
            <ta e="T107" id="Seg_1466" s="T106">kör-ön</ta>
            <ta e="T108" id="Seg_1467" s="T107">tur-but-tara</ta>
            <ta e="T109" id="Seg_1468" s="T108">o-nu</ta>
            <ta e="T110" id="Seg_1469" s="T109">üs</ta>
            <ta e="T111" id="Seg_1470" s="T110">u͡ol</ta>
            <ta e="T112" id="Seg_1471" s="T111">ogo</ta>
            <ta e="T113" id="Seg_1472" s="T112">kel-e</ta>
            <ta e="T114" id="Seg_1473" s="T113">onu-ga</ta>
            <ta e="T115" id="Seg_1474" s="T114">kel-en-ner</ta>
            <ta e="T116" id="Seg_1475" s="T115">gruša-lar-ɨ-n</ta>
            <ta e="T117" id="Seg_1476" s="T116">komuj-but-tar</ta>
            <ta e="T118" id="Seg_1477" s="T117">beje-ti-n</ta>
            <ta e="T119" id="Seg_1478" s="T118">kumak-tan</ta>
            <ta e="T120" id="Seg_1479" s="T119">tebeː-bit-ter</ta>
            <ta e="T121" id="Seg_1480" s="T120">innʼe</ta>
            <ta e="T122" id="Seg_1481" s="T121">gɨn-an</ta>
            <ta e="T123" id="Seg_1482" s="T122">baran</ta>
            <ta e="T124" id="Seg_1483" s="T123">dalʼse</ta>
            <ta e="T125" id="Seg_1484" s="T124">gini</ta>
            <ta e="T126" id="Seg_1485" s="T125">orog-u-n</ta>
            <ta e="T127" id="Seg_1486" s="T126">üstün</ta>
            <ta e="T128" id="Seg_1487" s="T127">kaːm-an</ta>
            <ta e="T129" id="Seg_1488" s="T128">is-pit-ter</ta>
            <ta e="T130" id="Seg_1489" s="T129">ol</ta>
            <ta e="T131" id="Seg_1490" s="T130">kaːm-an</ta>
            <ta e="T133" id="Seg_1491" s="T132">ih-en-ner</ta>
            <ta e="T134" id="Seg_1492" s="T133">bu͡o</ta>
            <ta e="T135" id="Seg_1493" s="T134">eː</ta>
            <ta e="T136" id="Seg_1494" s="T135">kenn-i-ki</ta>
            <ta e="T137" id="Seg_1495" s="T136">ogo-loro</ta>
            <ta e="T138" id="Seg_1496" s="T137">ke</ta>
            <ta e="T139" id="Seg_1497" s="T138">iti</ta>
            <ta e="T140" id="Seg_1498" s="T139">u͡ol</ta>
            <ta e="T141" id="Seg_1499" s="T140">ogo</ta>
            <ta e="T142" id="Seg_1500" s="T141">šljapa-tɨ-n</ta>
            <ta e="T143" id="Seg_1501" s="T142">šljapa-laːk</ta>
            <ta e="T144" id="Seg_1502" s="T143">e-t-e</ta>
            <ta e="T145" id="Seg_1503" s="T144">ol</ta>
            <ta e="T146" id="Seg_1504" s="T145">šljapa-tɨ-n</ta>
            <ta e="T147" id="Seg_1505" s="T146">bu͡o</ta>
            <ta e="T148" id="Seg_1506" s="T147">kɨːh-ɨ</ta>
            <ta e="T149" id="Seg_1507" s="T148">kör-ö-bün</ta>
            <ta e="T150" id="Seg_1508" s="T149">di͡e-n</ta>
            <ta e="T151" id="Seg_1509" s="T150">tüher-bit</ta>
            <ta e="T152" id="Seg_1510" s="T151">e-t-e</ta>
            <ta e="T153" id="Seg_1511" s="T152">ol</ta>
            <ta e="T154" id="Seg_1512" s="T153">šljapa-gɨ-n</ta>
            <ta e="T155" id="Seg_1513" s="T154">bul-an</ta>
            <ta e="T156" id="Seg_1514" s="T155">bu͡o</ta>
            <ta e="T157" id="Seg_1515" s="T156">ol</ta>
            <ta e="T158" id="Seg_1516" s="T157">u͡ol</ta>
            <ta e="T159" id="Seg_1517" s="T158">ogo-go</ta>
            <ta e="T160" id="Seg_1518" s="T159">il</ta>
            <ta e="T161" id="Seg_1519" s="T160">il-pit</ta>
            <ta e="T162" id="Seg_1520" s="T161">ihiːr-bit-ter</ta>
            <ta e="T164" id="Seg_1521" s="T163">maŋnaj</ta>
            <ta e="T165" id="Seg_1522" s="T164">onton</ta>
            <ta e="T166" id="Seg_1523" s="T165">u͡ol</ta>
            <ta e="T167" id="Seg_1524" s="T166">ogo-ŋ</ta>
            <ta e="T168" id="Seg_1525" s="T167">toktoː-but</ta>
            <ta e="T169" id="Seg_1526" s="T168">otton</ta>
            <ta e="T170" id="Seg_1527" s="T169">bu͡ollagɨna</ta>
            <ta e="T171" id="Seg_1528" s="T170">ol</ta>
            <ta e="T172" id="Seg_1529" s="T171">bu͡o</ta>
            <ta e="T173" id="Seg_1530" s="T172">kenn-i-ki</ta>
            <ta e="T174" id="Seg_1531" s="T173">ogo-ŋ</ta>
            <ta e="T175" id="Seg_1532" s="T174">bu͡olla</ta>
            <ta e="T176" id="Seg_1533" s="T175">ol</ta>
            <ta e="T177" id="Seg_1534" s="T176">kim-i-n</ta>
            <ta e="T178" id="Seg_1535" s="T177">šljapa-tɨ-n</ta>
            <ta e="T179" id="Seg_1536" s="T178">bi͡er-e</ta>
            <ta e="T180" id="Seg_1537" s="T179">bar-bɨt</ta>
            <ta e="T181" id="Seg_1538" s="T180">innʼe</ta>
            <ta e="T182" id="Seg_1539" s="T181">bi͡er-en</ta>
            <ta e="T183" id="Seg_1540" s="T182">bar-an</ta>
            <ta e="T184" id="Seg_1541" s="T183">bu͡o</ta>
            <ta e="T185" id="Seg_1542" s="T184">üs</ta>
            <ta e="T186" id="Seg_1543" s="T185">üs</ta>
            <ta e="T187" id="Seg_1544" s="T186">gruša-nɨ</ta>
            <ta e="T188" id="Seg_1545" s="T187">ɨl-bɨt</ta>
            <ta e="T189" id="Seg_1546" s="T188">i</ta>
            <ta e="T190" id="Seg_1547" s="T189">bun-tu-ŋ</ta>
            <ta e="T191" id="Seg_1548" s="T190">dalʼše</ta>
            <ta e="T192" id="Seg_1549" s="T191">bar-bɨt</ta>
            <ta e="T193" id="Seg_1550" s="T192">onton</ta>
            <ta e="T194" id="Seg_1551" s="T193">bu-lar</ta>
            <ta e="T195" id="Seg_1552" s="T194">bar-a</ta>
            <ta e="T196" id="Seg_1553" s="T195">tur-dak-tarɨna</ta>
            <ta e="T197" id="Seg_1554" s="T196">bu͡o</ta>
            <ta e="T198" id="Seg_1555" s="T197">sadovnik</ta>
            <ta e="T199" id="Seg_1556" s="T198">ol</ta>
            <ta e="T200" id="Seg_1557" s="T199">üktel-i-tten</ta>
            <ta e="T201" id="Seg_1558" s="T200">tüh-en</ta>
            <ta e="T202" id="Seg_1559" s="T201">bar-an</ta>
            <ta e="T203" id="Seg_1560" s="T202">gruša-lar-ɨ-n</ta>
            <ta e="T204" id="Seg_1561" s="T203">nöŋü͡ö</ta>
            <ta e="T206" id="Seg_1562" s="T205">iččitek</ta>
            <ta e="T207" id="Seg_1563" s="T206">karzina-ga</ta>
            <ta e="T208" id="Seg_1564" s="T207">gruša-lar-ɨ-n</ta>
            <ta e="T209" id="Seg_1565" s="T208">ug-aːrɨ</ta>
            <ta e="T210" id="Seg_1566" s="T209">kör-büt-e</ta>
            <ta e="T211" id="Seg_1567" s="T210">biːr</ta>
            <ta e="T212" id="Seg_1568" s="T211">karzina-ta</ta>
            <ta e="T213" id="Seg_1569" s="T212">hu͡ok</ta>
            <ta e="T214" id="Seg_1570" s="T213">aːk-pɨt-a</ta>
            <ta e="T215" id="Seg_1571" s="T214">biːr</ta>
            <ta e="T216" id="Seg_1572" s="T215">karzina-ta</ta>
            <ta e="T217" id="Seg_1573" s="T216">hu͡ok</ta>
            <ta e="T218" id="Seg_1574" s="T217">on-tu-ŋ</ta>
            <ta e="T219" id="Seg_1575" s="T218">dumaj-dɨː-r</ta>
            <ta e="T220" id="Seg_1576" s="T219">horogor</ta>
            <ta e="T221" id="Seg_1577" s="T220">kajdi͡eg-ɨ-nan</ta>
            <ta e="T222" id="Seg_1578" s="T221">kajdi͡ek</ta>
            <ta e="T223" id="Seg_1579" s="T222">bar-t-a=j</ta>
            <ta e="T224" id="Seg_1580" s="T223">diː-r</ta>
            <ta e="T225" id="Seg_1581" s="T224">ol</ta>
            <ta e="T226" id="Seg_1582" s="T225">dumaj-d-ɨː</ta>
            <ta e="T227" id="Seg_1583" s="T226">tur-dag-ɨna</ta>
            <ta e="T228" id="Seg_1584" s="T227">bu͡o</ta>
            <ta e="T229" id="Seg_1585" s="T228">baːjdiːn</ta>
            <ta e="T230" id="Seg_1586" s="T229">üs</ta>
            <ta e="T231" id="Seg_1587" s="T230">u͡ol</ta>
            <ta e="T232" id="Seg_1588" s="T231">ogo</ta>
            <ta e="T233" id="Seg_1589" s="T232">ɨksa-tɨ-nan</ta>
            <ta e="T234" id="Seg_1590" s="T233">aːh-an</ta>
            <ta e="T235" id="Seg_1591" s="T234">aːs-pɨt-tar</ta>
            <ta e="T236" id="Seg_1592" s="T235">gini</ta>
            <ta e="T237" id="Seg_1593" s="T236">ɨksa-tɨ-nan</ta>
            <ta e="T238" id="Seg_1594" s="T237">ke</ta>
            <ta e="T239" id="Seg_1595" s="T238">o-lor-u</ta>
            <ta e="T240" id="Seg_1596" s="T239">kör-d-ö</ta>
            <ta e="T241" id="Seg_1597" s="T240">kör-büt-ü-nen</ta>
            <ta e="T242" id="Seg_1598" s="T241">h-onon</ta>
            <ta e="T243" id="Seg_1599" s="T242">kaːl-bɨt</ta>
            <ta e="T244" id="Seg_1600" s="T243">sadovnik</ta>
            <ta e="T245" id="Seg_1601" s="T244">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T3" id="Seg_1602" s="T2">eː</ta>
            <ta e="T5" id="Seg_1603" s="T4">sadovnik</ta>
            <ta e="T6" id="Seg_1604" s="T5">sadovnik</ta>
            <ta e="T7" id="Seg_1605" s="T6">gruša-LAr-nI</ta>
            <ta e="T8" id="Seg_1606" s="T7">komuj-Ar</ta>
            <ta e="T9" id="Seg_1607" s="T8">biːr</ta>
            <ta e="T10" id="Seg_1608" s="T9">karzina</ta>
            <ta e="T11" id="Seg_1609" s="T10">gruša-nI</ta>
            <ta e="T12" id="Seg_1610" s="T11">komuj-BIT</ta>
            <ta e="T13" id="Seg_1611" s="T12">ol</ta>
            <ta e="T14" id="Seg_1612" s="T13">komuj-A</ta>
            <ta e="T15" id="Seg_1613" s="T14">hɨt-TAK-InA</ta>
            <ta e="T16" id="Seg_1614" s="T15">bu͡o</ta>
            <ta e="T17" id="Seg_1615" s="T16">ɨksa-tI-nAn</ta>
            <ta e="T18" id="Seg_1616" s="T17">bu͡ollagɨna</ta>
            <ta e="T19" id="Seg_1617" s="T18">kazaː-LAːK</ta>
            <ta e="T20" id="Seg_1618" s="T19">kihi</ta>
            <ta e="T21" id="Seg_1619" s="T20">aːs-BIT-tA</ta>
            <ta e="T22" id="Seg_1620" s="T21">onton</ta>
            <ta e="T24" id="Seg_1621" s="T23">össü͡ö</ta>
            <ta e="T25" id="Seg_1622" s="T24">da</ta>
            <ta e="T28" id="Seg_1623" s="T27">mas-GA</ta>
            <ta e="T29" id="Seg_1624" s="T28">ɨtɨn-I-BIT</ta>
            <ta e="T30" id="Seg_1625" s="T29">össü͡ö</ta>
            <ta e="T31" id="Seg_1626" s="T30">da</ta>
            <ta e="T32" id="Seg_1627" s="T31">gruša</ta>
            <ta e="T33" id="Seg_1628" s="T32">komuj-AːrI</ta>
            <ta e="T34" id="Seg_1629" s="T33">ol</ta>
            <ta e="T35" id="Seg_1630" s="T34">hɨt-TAK-InA</ta>
            <ta e="T36" id="Seg_1631" s="T35">bu͡o</ta>
            <ta e="T37" id="Seg_1632" s="T36">ol</ta>
            <ta e="T38" id="Seg_1633" s="T37">di͡egi-ttAn</ta>
            <ta e="T39" id="Seg_1634" s="T38">bilisipi͡et-LAːK</ta>
            <ta e="T40" id="Seg_1635" s="T39">u͡ol</ta>
            <ta e="T41" id="Seg_1636" s="T40">ogo</ta>
            <ta e="T42" id="Seg_1637" s="T41">kel-BIT</ta>
            <ta e="T43" id="Seg_1638" s="T42">is-Ar</ta>
            <ta e="T44" id="Seg_1639" s="T43">onton</ta>
            <ta e="T45" id="Seg_1640" s="T44">iti-nI</ta>
            <ta e="T46" id="Seg_1641" s="T45">gini-GA</ta>
            <ta e="T47" id="Seg_1642" s="T46">kel-An</ta>
            <ta e="T48" id="Seg_1643" s="T47">bu͡ollagɨna</ta>
            <ta e="T49" id="Seg_1644" s="T48">gruša-LAr-nI</ta>
            <ta e="T50" id="Seg_1645" s="T49">kör-An</ta>
            <ta e="T51" id="Seg_1646" s="T50">biːr</ta>
            <ta e="T52" id="Seg_1647" s="T51">gruša-nI</ta>
            <ta e="T53" id="Seg_1648" s="T52">ɨl-AːrI</ta>
            <ta e="T54" id="Seg_1649" s="T53">gɨn-BIT-tA</ta>
            <ta e="T55" id="Seg_1650" s="T54">ol-tI-BIt</ta>
            <ta e="T56" id="Seg_1651" s="T55">heren-Ar</ta>
            <ta e="T57" id="Seg_1652" s="T56">du͡o</ta>
            <ta e="T58" id="Seg_1653" s="T57">tu͡ok</ta>
            <ta e="T59" id="Seg_1654" s="T58">du͡o</ta>
            <ta e="T60" id="Seg_1655" s="T59">innʼe</ta>
            <ta e="T61" id="Seg_1656" s="T60">ikki-Is</ta>
            <ta e="T62" id="Seg_1657" s="T61">e</ta>
            <ta e="T63" id="Seg_1658" s="T62">ɨl-TI-tA</ta>
            <ta e="T64" id="Seg_1659" s="T63">daːganɨ</ta>
            <ta e="T65" id="Seg_1660" s="T64">bütün</ta>
            <ta e="T66" id="Seg_1661" s="T65">karzina-nI</ta>
            <ta e="T68" id="Seg_1662" s="T67">bilisipi͡et-tI-GAr</ta>
            <ta e="T69" id="Seg_1663" s="T68">tij-An</ta>
            <ta e="T70" id="Seg_1664" s="T69">bar-An</ta>
            <ta e="T71" id="Seg_1665" s="T70">orok</ta>
            <ta e="T72" id="Seg_1666" s="T71">üstün</ta>
            <ta e="T73" id="Seg_1667" s="T72">bar-A</ta>
            <ta e="T74" id="Seg_1668" s="T73">tur-Ar</ta>
            <ta e="T75" id="Seg_1669" s="T74">bar-An</ta>
            <ta e="T76" id="Seg_1670" s="T75">is-An-is-An</ta>
            <ta e="T78" id="Seg_1671" s="T77">utarɨ</ta>
            <ta e="T79" id="Seg_1672" s="T78">kɨːs</ta>
            <ta e="T80" id="Seg_1673" s="T79">ogo-nI</ta>
            <ta e="T81" id="Seg_1674" s="T80">körüs-I-BIT</ta>
            <ta e="T82" id="Seg_1675" s="T81">eː</ta>
            <ta e="T83" id="Seg_1676" s="T82">kör-BIT</ta>
            <ta e="T84" id="Seg_1677" s="T83">ol-nI</ta>
            <ta e="T85" id="Seg_1678" s="T84">kör-TI-tA</ta>
            <ta e="T86" id="Seg_1679" s="T85">kör-BIT-tI-nAn</ta>
            <ta e="T87" id="Seg_1680" s="T86">uhuk-I-ttAn</ta>
            <ta e="T88" id="Seg_1681" s="T87">taːs-GA</ta>
            <ta e="T89" id="Seg_1682" s="T88">kep-An</ta>
            <ta e="T90" id="Seg_1683" s="T89">bilisipi͡et-tI-ttAn</ta>
            <ta e="T91" id="Seg_1684" s="T90">eː</ta>
            <ta e="T92" id="Seg_1685" s="T91">huːl-An</ta>
            <ta e="T93" id="Seg_1686" s="T92">tüs-BIT</ta>
            <ta e="T94" id="Seg_1687" s="T93">gruša-LAr-tA</ta>
            <ta e="T95" id="Seg_1688" s="T94">hir-GA</ta>
            <ta e="T96" id="Seg_1689" s="T95">čekenij-An</ta>
            <ta e="T97" id="Seg_1690" s="T96">tüs-BIT-LAr</ta>
            <ta e="T98" id="Seg_1691" s="T97">ol</ta>
            <ta e="T99" id="Seg_1692" s="T98">tep-A-n-A</ta>
            <ta e="T100" id="Seg_1693" s="T99">hɨt-TAK-InA</ta>
            <ta e="T101" id="Seg_1694" s="T100">üs</ta>
            <ta e="T102" id="Seg_1695" s="T101">u͡ol</ta>
            <ta e="T103" id="Seg_1696" s="T102">ogo</ta>
            <ta e="T104" id="Seg_1697" s="T103">üs</ta>
            <ta e="T105" id="Seg_1698" s="T104">u͡ol</ta>
            <ta e="T106" id="Seg_1699" s="T105">ogo</ta>
            <ta e="T107" id="Seg_1700" s="T106">kör-An</ta>
            <ta e="T108" id="Seg_1701" s="T107">tur-BIT-LArA</ta>
            <ta e="T109" id="Seg_1702" s="T108">ol-nI</ta>
            <ta e="T110" id="Seg_1703" s="T109">üs</ta>
            <ta e="T111" id="Seg_1704" s="T110">u͡ol</ta>
            <ta e="T112" id="Seg_1705" s="T111">ogo</ta>
            <ta e="T113" id="Seg_1706" s="T112">kel-A</ta>
            <ta e="T114" id="Seg_1707" s="T113">ol-GA</ta>
            <ta e="T115" id="Seg_1708" s="T114">kel-An-LAr</ta>
            <ta e="T116" id="Seg_1709" s="T115">gruša-LAr-tI-n</ta>
            <ta e="T117" id="Seg_1710" s="T116">komuj-BIT-LAr</ta>
            <ta e="T118" id="Seg_1711" s="T117">beje-tI-n</ta>
            <ta e="T119" id="Seg_1712" s="T118">kumak-ttAn</ta>
            <ta e="T120" id="Seg_1713" s="T119">tebeː-BIT-LAr</ta>
            <ta e="T121" id="Seg_1714" s="T120">innʼe</ta>
            <ta e="T122" id="Seg_1715" s="T121">gɨn-An</ta>
            <ta e="T123" id="Seg_1716" s="T122">baran</ta>
            <ta e="T124" id="Seg_1717" s="T123">dalʼše</ta>
            <ta e="T125" id="Seg_1718" s="T124">gini</ta>
            <ta e="T126" id="Seg_1719" s="T125">orok-tI-n</ta>
            <ta e="T127" id="Seg_1720" s="T126">üstün</ta>
            <ta e="T128" id="Seg_1721" s="T127">kaːm-An</ta>
            <ta e="T129" id="Seg_1722" s="T128">is-BIT-LAr</ta>
            <ta e="T130" id="Seg_1723" s="T129">ol</ta>
            <ta e="T131" id="Seg_1724" s="T130">kaːm-An</ta>
            <ta e="T133" id="Seg_1725" s="T132">is-An-LAr</ta>
            <ta e="T134" id="Seg_1726" s="T133">bu͡o</ta>
            <ta e="T135" id="Seg_1727" s="T134">eː</ta>
            <ta e="T136" id="Seg_1728" s="T135">kelin-I-GI</ta>
            <ta e="T137" id="Seg_1729" s="T136">ogo-LArA</ta>
            <ta e="T138" id="Seg_1730" s="T137">ka</ta>
            <ta e="T139" id="Seg_1731" s="T138">iti</ta>
            <ta e="T140" id="Seg_1732" s="T139">u͡ol</ta>
            <ta e="T141" id="Seg_1733" s="T140">ogo</ta>
            <ta e="T142" id="Seg_1734" s="T141">šljapa-tI-n</ta>
            <ta e="T143" id="Seg_1735" s="T142">šljapa-LAːK</ta>
            <ta e="T144" id="Seg_1736" s="T143">e-TI-tA</ta>
            <ta e="T145" id="Seg_1737" s="T144">ol</ta>
            <ta e="T146" id="Seg_1738" s="T145">šljapa-tI-n</ta>
            <ta e="T147" id="Seg_1739" s="T146">bu͡o</ta>
            <ta e="T148" id="Seg_1740" s="T147">kɨːs-nI</ta>
            <ta e="T149" id="Seg_1741" s="T148">kör-A-BIn</ta>
            <ta e="T150" id="Seg_1742" s="T149">di͡e-An</ta>
            <ta e="T151" id="Seg_1743" s="T150">tüher-BIT</ta>
            <ta e="T152" id="Seg_1744" s="T151">e-TI-tA</ta>
            <ta e="T153" id="Seg_1745" s="T152">ol</ta>
            <ta e="T154" id="Seg_1746" s="T153">šljapa-GI-n</ta>
            <ta e="T155" id="Seg_1747" s="T154">bul-An</ta>
            <ta e="T156" id="Seg_1748" s="T155">bu͡o</ta>
            <ta e="T157" id="Seg_1749" s="T156">ol</ta>
            <ta e="T158" id="Seg_1750" s="T157">u͡ol</ta>
            <ta e="T159" id="Seg_1751" s="T158">ogo-GA</ta>
            <ta e="T160" id="Seg_1752" s="T159">ilt</ta>
            <ta e="T161" id="Seg_1753" s="T160">ilt-BIT</ta>
            <ta e="T162" id="Seg_1754" s="T161">ihiːr-BIT-LAr</ta>
            <ta e="T164" id="Seg_1755" s="T163">maŋnaj</ta>
            <ta e="T165" id="Seg_1756" s="T164">onton</ta>
            <ta e="T166" id="Seg_1757" s="T165">u͡ol</ta>
            <ta e="T167" id="Seg_1758" s="T166">ogo-ŋ</ta>
            <ta e="T168" id="Seg_1759" s="T167">toktoː-BIT</ta>
            <ta e="T169" id="Seg_1760" s="T168">onton</ta>
            <ta e="T170" id="Seg_1761" s="T169">bu͡ollagɨna</ta>
            <ta e="T171" id="Seg_1762" s="T170">ol</ta>
            <ta e="T172" id="Seg_1763" s="T171">bu͡o</ta>
            <ta e="T173" id="Seg_1764" s="T172">kelin-I-GI</ta>
            <ta e="T174" id="Seg_1765" s="T173">ogo-ŋ</ta>
            <ta e="T175" id="Seg_1766" s="T174">bu͡olla</ta>
            <ta e="T176" id="Seg_1767" s="T175">ol</ta>
            <ta e="T177" id="Seg_1768" s="T176">kim-tI-n</ta>
            <ta e="T178" id="Seg_1769" s="T177">šljapa-tI-n</ta>
            <ta e="T179" id="Seg_1770" s="T178">bi͡er-A</ta>
            <ta e="T180" id="Seg_1771" s="T179">bar-BIT</ta>
            <ta e="T181" id="Seg_1772" s="T180">innʼe</ta>
            <ta e="T182" id="Seg_1773" s="T181">bi͡er-An</ta>
            <ta e="T183" id="Seg_1774" s="T182">bar-An</ta>
            <ta e="T184" id="Seg_1775" s="T183">bu͡o</ta>
            <ta e="T185" id="Seg_1776" s="T184">üs</ta>
            <ta e="T186" id="Seg_1777" s="T185">üs</ta>
            <ta e="T187" id="Seg_1778" s="T186">gruša-nI</ta>
            <ta e="T188" id="Seg_1779" s="T187">ɨl-BIT</ta>
            <ta e="T189" id="Seg_1780" s="T188">i</ta>
            <ta e="T190" id="Seg_1781" s="T189">bu-tI-ŋ</ta>
            <ta e="T191" id="Seg_1782" s="T190">dalʼše</ta>
            <ta e="T192" id="Seg_1783" s="T191">bar-BIT</ta>
            <ta e="T193" id="Seg_1784" s="T192">onton</ta>
            <ta e="T194" id="Seg_1785" s="T193">bu-LAr</ta>
            <ta e="T195" id="Seg_1786" s="T194">bar-A</ta>
            <ta e="T196" id="Seg_1787" s="T195">tur-TAK-TArInA</ta>
            <ta e="T197" id="Seg_1788" s="T196">bu͡o</ta>
            <ta e="T198" id="Seg_1789" s="T197">sadovnik</ta>
            <ta e="T199" id="Seg_1790" s="T198">ol</ta>
            <ta e="T200" id="Seg_1791" s="T199">üktel-tI-ttAn</ta>
            <ta e="T201" id="Seg_1792" s="T200">tüs-An</ta>
            <ta e="T202" id="Seg_1793" s="T201">bar-An</ta>
            <ta e="T203" id="Seg_1794" s="T202">gruša-LAr-tI-n</ta>
            <ta e="T204" id="Seg_1795" s="T203">nöŋü͡ö</ta>
            <ta e="T206" id="Seg_1796" s="T205">iččitek</ta>
            <ta e="T207" id="Seg_1797" s="T206">karzina-GA</ta>
            <ta e="T208" id="Seg_1798" s="T207">gruša-LAr-tI-n</ta>
            <ta e="T209" id="Seg_1799" s="T208">uk-AːrI</ta>
            <ta e="T210" id="Seg_1800" s="T209">kör-BIT-tA</ta>
            <ta e="T211" id="Seg_1801" s="T210">biːr</ta>
            <ta e="T212" id="Seg_1802" s="T211">karzina-tA</ta>
            <ta e="T213" id="Seg_1803" s="T212">hu͡ok</ta>
            <ta e="T214" id="Seg_1804" s="T213">aːk-BIT-tA</ta>
            <ta e="T215" id="Seg_1805" s="T214">biːr</ta>
            <ta e="T216" id="Seg_1806" s="T215">karzina-tA</ta>
            <ta e="T217" id="Seg_1807" s="T216">hu͡ok</ta>
            <ta e="T218" id="Seg_1808" s="T217">ol-tI-ŋ</ta>
            <ta e="T219" id="Seg_1809" s="T218">dumaj-LAː-Ar</ta>
            <ta e="T220" id="Seg_1810" s="T219">horogor</ta>
            <ta e="T221" id="Seg_1811" s="T220">kajdi͡ek-I-nAn</ta>
            <ta e="T222" id="Seg_1812" s="T221">kajdi͡ek</ta>
            <ta e="T223" id="Seg_1813" s="T222">bar-BIT-tA=Ij</ta>
            <ta e="T224" id="Seg_1814" s="T223">di͡e-Ar</ta>
            <ta e="T225" id="Seg_1815" s="T224">ol</ta>
            <ta e="T226" id="Seg_1816" s="T225">dumaj-LAː-A</ta>
            <ta e="T227" id="Seg_1817" s="T226">tur-TAK-InA</ta>
            <ta e="T228" id="Seg_1818" s="T227">bu͡o</ta>
            <ta e="T229" id="Seg_1819" s="T228">maːjdiːn</ta>
            <ta e="T230" id="Seg_1820" s="T229">üs</ta>
            <ta e="T231" id="Seg_1821" s="T230">u͡ol</ta>
            <ta e="T232" id="Seg_1822" s="T231">ogo</ta>
            <ta e="T233" id="Seg_1823" s="T232">ɨksa-tI-nAn</ta>
            <ta e="T234" id="Seg_1824" s="T233">aːs-An</ta>
            <ta e="T235" id="Seg_1825" s="T234">aːs-BIT-LAr</ta>
            <ta e="T236" id="Seg_1826" s="T235">gini</ta>
            <ta e="T237" id="Seg_1827" s="T236">ɨksa-tI-nAn</ta>
            <ta e="T238" id="Seg_1828" s="T237">ka</ta>
            <ta e="T239" id="Seg_1829" s="T238">ol-LAr-nI</ta>
            <ta e="T240" id="Seg_1830" s="T239">kör-TI-tA</ta>
            <ta e="T241" id="Seg_1831" s="T240">kör-BIT-tI-nAn</ta>
            <ta e="T242" id="Seg_1832" s="T241">h-onon</ta>
            <ta e="T243" id="Seg_1833" s="T242">kaːl-BIT</ta>
            <ta e="T244" id="Seg_1834" s="T243">sadovnik</ta>
            <ta e="T245" id="Seg_1835" s="T244">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T3" id="Seg_1836" s="T2">eh</ta>
            <ta e="T5" id="Seg_1837" s="T4">gardener.[NOM]</ta>
            <ta e="T6" id="Seg_1838" s="T5">gardener.[NOM]</ta>
            <ta e="T7" id="Seg_1839" s="T6">pear-PL-ACC</ta>
            <ta e="T8" id="Seg_1840" s="T7">gather-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_1841" s="T8">one</ta>
            <ta e="T10" id="Seg_1842" s="T9">basket.[NOM]</ta>
            <ta e="T11" id="Seg_1843" s="T10">pear-ACC</ta>
            <ta e="T12" id="Seg_1844" s="T11">gather-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_1845" s="T12">that</ta>
            <ta e="T14" id="Seg_1846" s="T13">gather-CVB.SIM</ta>
            <ta e="T15" id="Seg_1847" s="T14">lie-TEMP-3SG</ta>
            <ta e="T16" id="Seg_1848" s="T15">EMPH</ta>
            <ta e="T17" id="Seg_1849" s="T16">proximity-3SG-INSTR</ta>
            <ta e="T18" id="Seg_1850" s="T17">though</ta>
            <ta e="T19" id="Seg_1851" s="T18">goat-PROPR</ta>
            <ta e="T20" id="Seg_1852" s="T19">human.being.[NOM]</ta>
            <ta e="T21" id="Seg_1853" s="T20">pass.by-PST2-3SG</ta>
            <ta e="T22" id="Seg_1854" s="T21">then</ta>
            <ta e="T24" id="Seg_1855" s="T23">still</ta>
            <ta e="T25" id="Seg_1856" s="T24">and</ta>
            <ta e="T28" id="Seg_1857" s="T27">tree-DAT/LOC</ta>
            <ta e="T29" id="Seg_1858" s="T28">climb-EP-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_1859" s="T29">still</ta>
            <ta e="T31" id="Seg_1860" s="T30">and</ta>
            <ta e="T32" id="Seg_1861" s="T31">pear.[NOM]</ta>
            <ta e="T33" id="Seg_1862" s="T32">gather-CVB.PURP</ta>
            <ta e="T34" id="Seg_1863" s="T33">that</ta>
            <ta e="T35" id="Seg_1864" s="T34">lie-TEMP-3SG</ta>
            <ta e="T36" id="Seg_1865" s="T35">EMPH</ta>
            <ta e="T37" id="Seg_1866" s="T36">that</ta>
            <ta e="T38" id="Seg_1867" s="T37">side-ABL</ta>
            <ta e="T39" id="Seg_1868" s="T38">bicycle-PROPR</ta>
            <ta e="T40" id="Seg_1869" s="T39">boy.[NOM]</ta>
            <ta e="T41" id="Seg_1870" s="T40">child.[NOM]</ta>
            <ta e="T42" id="Seg_1871" s="T41">come-PST2.[3SG]</ta>
            <ta e="T43" id="Seg_1872" s="T42">go-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_1873" s="T43">then</ta>
            <ta e="T45" id="Seg_1874" s="T44">that-ACC</ta>
            <ta e="T46" id="Seg_1875" s="T45">3SG-DAT/LOC</ta>
            <ta e="T47" id="Seg_1876" s="T46">come-CVB.SEQ</ta>
            <ta e="T48" id="Seg_1877" s="T47">though</ta>
            <ta e="T49" id="Seg_1878" s="T48">pear-PL-ACC</ta>
            <ta e="T50" id="Seg_1879" s="T49">see-CVB.SEQ</ta>
            <ta e="T51" id="Seg_1880" s="T50">one</ta>
            <ta e="T52" id="Seg_1881" s="T51">pear-ACC</ta>
            <ta e="T53" id="Seg_1882" s="T52">take-CVB.PURP</ta>
            <ta e="T54" id="Seg_1883" s="T53">want-PST2-3SG</ta>
            <ta e="T55" id="Seg_1884" s="T54">that-3SG-1PL.[NOM]</ta>
            <ta e="T56" id="Seg_1885" s="T55">be.careful-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_1886" s="T56">Q</ta>
            <ta e="T58" id="Seg_1887" s="T57">what.[NOM]</ta>
            <ta e="T59" id="Seg_1888" s="T58">Q</ta>
            <ta e="T60" id="Seg_1889" s="T59">so</ta>
            <ta e="T61" id="Seg_1890" s="T60">two-ORD</ta>
            <ta e="T62" id="Seg_1891" s="T61">eh</ta>
            <ta e="T63" id="Seg_1892" s="T62">take-PST1-3SG</ta>
            <ta e="T64" id="Seg_1893" s="T63">even.if</ta>
            <ta e="T65" id="Seg_1894" s="T64">complete.[NOM]</ta>
            <ta e="T66" id="Seg_1895" s="T65">basket-ACC</ta>
            <ta e="T68" id="Seg_1896" s="T67">bicycle-3SG-DAT/LOC</ta>
            <ta e="T69" id="Seg_1897" s="T68">reach-CVB.SEQ</ta>
            <ta e="T70" id="Seg_1898" s="T69">go-CVB.SEQ</ta>
            <ta e="T71" id="Seg_1899" s="T70">path.[NOM]</ta>
            <ta e="T72" id="Seg_1900" s="T71">along</ta>
            <ta e="T73" id="Seg_1901" s="T72">go-CVB.SIM</ta>
            <ta e="T74" id="Seg_1902" s="T73">stand-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_1903" s="T74">go-CVB.SEQ</ta>
            <ta e="T76" id="Seg_1904" s="T75">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T78" id="Seg_1905" s="T77">towards</ta>
            <ta e="T79" id="Seg_1906" s="T78">girl.[NOM]</ta>
            <ta e="T80" id="Seg_1907" s="T79">child-ACC</ta>
            <ta e="T81" id="Seg_1908" s="T80">meet-EP-PST2.[3SG]</ta>
            <ta e="T82" id="Seg_1909" s="T81">eh</ta>
            <ta e="T83" id="Seg_1910" s="T82">see-PST2.[3SG]</ta>
            <ta e="T84" id="Seg_1911" s="T83">that-ACC</ta>
            <ta e="T85" id="Seg_1912" s="T84">see-PST1-3SG</ta>
            <ta e="T86" id="Seg_1913" s="T85">see-PTCP.PST-3SG-INSTR</ta>
            <ta e="T87" id="Seg_1914" s="T86">end-EP-ABL</ta>
            <ta e="T88" id="Seg_1915" s="T87">stone-DAT/LOC</ta>
            <ta e="T89" id="Seg_1916" s="T88">push-CVB.SEQ</ta>
            <ta e="T90" id="Seg_1917" s="T89">bicycle-3SG-ABL</ta>
            <ta e="T91" id="Seg_1918" s="T90">hey</ta>
            <ta e="T92" id="Seg_1919" s="T91">crash-CVB.SEQ</ta>
            <ta e="T93" id="Seg_1920" s="T92">fall-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_1921" s="T93">pear-PL-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_1922" s="T94">earth-DAT/LOC</ta>
            <ta e="T96" id="Seg_1923" s="T95">roll-CVB.SEQ</ta>
            <ta e="T97" id="Seg_1924" s="T96">fall-PST2-3PL</ta>
            <ta e="T98" id="Seg_1925" s="T97">that</ta>
            <ta e="T99" id="Seg_1926" s="T98">kick-EP-REFL-CVB.SIM</ta>
            <ta e="T100" id="Seg_1927" s="T99">lie-TEMP-3SG</ta>
            <ta e="T101" id="Seg_1928" s="T100">three</ta>
            <ta e="T102" id="Seg_1929" s="T101">boy.[NOM]</ta>
            <ta e="T103" id="Seg_1930" s="T102">child.[NOM]</ta>
            <ta e="T104" id="Seg_1931" s="T103">three</ta>
            <ta e="T105" id="Seg_1932" s="T104">boy.[NOM]</ta>
            <ta e="T106" id="Seg_1933" s="T105">child.[NOM]</ta>
            <ta e="T107" id="Seg_1934" s="T106">see-CVB.SEQ</ta>
            <ta e="T108" id="Seg_1935" s="T107">stand-PST2-3PL</ta>
            <ta e="T109" id="Seg_1936" s="T108">that-ACC</ta>
            <ta e="T110" id="Seg_1937" s="T109">three</ta>
            <ta e="T111" id="Seg_1938" s="T110">boy.[NOM]</ta>
            <ta e="T112" id="Seg_1939" s="T111">child.[NOM]</ta>
            <ta e="T113" id="Seg_1940" s="T112">come-EP</ta>
            <ta e="T114" id="Seg_1941" s="T113">that-DAT/LOC</ta>
            <ta e="T115" id="Seg_1942" s="T114">come-CVB.SEQ-3PL</ta>
            <ta e="T116" id="Seg_1943" s="T115">pear-PL-3SG-ACC</ta>
            <ta e="T117" id="Seg_1944" s="T116">gather-PST2-3PL</ta>
            <ta e="T118" id="Seg_1945" s="T117">self-3SG-ACC</ta>
            <ta e="T119" id="Seg_1946" s="T118">sand-ABL</ta>
            <ta e="T120" id="Seg_1947" s="T119">shake.off-PST2-3PL</ta>
            <ta e="T121" id="Seg_1948" s="T120">so</ta>
            <ta e="T122" id="Seg_1949" s="T121">make-CVB.SEQ</ta>
            <ta e="T123" id="Seg_1950" s="T122">after</ta>
            <ta e="T124" id="Seg_1951" s="T123">further</ta>
            <ta e="T125" id="Seg_1952" s="T124">3SG.[NOM]</ta>
            <ta e="T126" id="Seg_1953" s="T125">path-3SG-ACC</ta>
            <ta e="T127" id="Seg_1954" s="T126">along</ta>
            <ta e="T128" id="Seg_1955" s="T127">walk-CVB.SEQ</ta>
            <ta e="T129" id="Seg_1956" s="T128">go-PST2-3PL</ta>
            <ta e="T130" id="Seg_1957" s="T129">that</ta>
            <ta e="T131" id="Seg_1958" s="T130">walk-CVB.SEQ</ta>
            <ta e="T133" id="Seg_1959" s="T132">go-CVB.SEQ-3PL</ta>
            <ta e="T134" id="Seg_1960" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_1961" s="T134">eh</ta>
            <ta e="T136" id="Seg_1962" s="T135">back-EP-ADJZ</ta>
            <ta e="T137" id="Seg_1963" s="T136">child-3PL.[NOM]</ta>
            <ta e="T138" id="Seg_1964" s="T137">well</ta>
            <ta e="T139" id="Seg_1965" s="T138">that.[NOM]</ta>
            <ta e="T140" id="Seg_1966" s="T139">boy.[NOM]</ta>
            <ta e="T141" id="Seg_1967" s="T140">child.[NOM]</ta>
            <ta e="T142" id="Seg_1968" s="T141">hat-3SG-ACC</ta>
            <ta e="T143" id="Seg_1969" s="T142">hat-PROPR.[NOM]</ta>
            <ta e="T144" id="Seg_1970" s="T143">be-PST1-3SG</ta>
            <ta e="T145" id="Seg_1971" s="T144">that</ta>
            <ta e="T146" id="Seg_1972" s="T145">hat-3SG-ACC</ta>
            <ta e="T147" id="Seg_1973" s="T146">EMPH</ta>
            <ta e="T148" id="Seg_1974" s="T147">girl-ACC</ta>
            <ta e="T149" id="Seg_1975" s="T148">see-PRS-1SG</ta>
            <ta e="T150" id="Seg_1976" s="T149">say-CVB.SEQ</ta>
            <ta e="T151" id="Seg_1977" s="T150">drop-PTCP.PST</ta>
            <ta e="T152" id="Seg_1978" s="T151">be-PST1-3SG</ta>
            <ta e="T153" id="Seg_1979" s="T152">that</ta>
            <ta e="T154" id="Seg_1980" s="T153">hat-2SG-ACC</ta>
            <ta e="T155" id="Seg_1981" s="T154">find-CVB.SEQ</ta>
            <ta e="T156" id="Seg_1982" s="T155">EMPH</ta>
            <ta e="T157" id="Seg_1983" s="T156">that</ta>
            <ta e="T158" id="Seg_1984" s="T157">boy.[NOM]</ta>
            <ta e="T159" id="Seg_1985" s="T158">child-DAT/LOC</ta>
            <ta e="T160" id="Seg_1986" s="T159">bring</ta>
            <ta e="T161" id="Seg_1987" s="T160">bring-PST2</ta>
            <ta e="T162" id="Seg_1988" s="T161">whistle-PST2-3PL</ta>
            <ta e="T164" id="Seg_1989" s="T163">at.first</ta>
            <ta e="T165" id="Seg_1990" s="T164">then</ta>
            <ta e="T166" id="Seg_1991" s="T165">boy.[NOM]</ta>
            <ta e="T167" id="Seg_1992" s="T166">child-2SG.[NOM]</ta>
            <ta e="T168" id="Seg_1993" s="T167">stop-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_1994" s="T168">then</ta>
            <ta e="T170" id="Seg_1995" s="T169">though</ta>
            <ta e="T171" id="Seg_1996" s="T170">that</ta>
            <ta e="T172" id="Seg_1997" s="T171">EMPH</ta>
            <ta e="T173" id="Seg_1998" s="T172">back-EP-ADJZ</ta>
            <ta e="T174" id="Seg_1999" s="T173">child-2SG.[NOM]</ta>
            <ta e="T175" id="Seg_2000" s="T174">MOD</ta>
            <ta e="T176" id="Seg_2001" s="T175">that</ta>
            <ta e="T177" id="Seg_2002" s="T176">who-3SG-ACC</ta>
            <ta e="T178" id="Seg_2003" s="T177">hat-3SG-ACC</ta>
            <ta e="T179" id="Seg_2004" s="T178">give-CVB.SIM</ta>
            <ta e="T180" id="Seg_2005" s="T179">go-PST2.[3SG]</ta>
            <ta e="T181" id="Seg_2006" s="T180">so</ta>
            <ta e="T182" id="Seg_2007" s="T181">give-CVB.SEQ</ta>
            <ta e="T183" id="Seg_2008" s="T182">go-CVB.SEQ</ta>
            <ta e="T184" id="Seg_2009" s="T183">EMPH</ta>
            <ta e="T185" id="Seg_2010" s="T184">three</ta>
            <ta e="T186" id="Seg_2011" s="T185">three</ta>
            <ta e="T187" id="Seg_2012" s="T186">pear-ACC</ta>
            <ta e="T188" id="Seg_2013" s="T187">get-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_2014" s="T188">and</ta>
            <ta e="T190" id="Seg_2015" s="T189">this-3SG-2SG.[NOM]</ta>
            <ta e="T191" id="Seg_2016" s="T190">further</ta>
            <ta e="T192" id="Seg_2017" s="T191">go-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_2018" s="T192">then</ta>
            <ta e="T194" id="Seg_2019" s="T193">this-PL.[NOM]</ta>
            <ta e="T195" id="Seg_2020" s="T194">go-CVB.SIM</ta>
            <ta e="T196" id="Seg_2021" s="T195">stand-TEMP-3PL</ta>
            <ta e="T197" id="Seg_2022" s="T196">EMPH</ta>
            <ta e="T198" id="Seg_2023" s="T197">gardener.[NOM]</ta>
            <ta e="T199" id="Seg_2024" s="T198">that</ta>
            <ta e="T200" id="Seg_2025" s="T199">step-3SG-ABL</ta>
            <ta e="T201" id="Seg_2026" s="T200">fall-CVB.SEQ</ta>
            <ta e="T202" id="Seg_2027" s="T201">go-CVB.SEQ</ta>
            <ta e="T203" id="Seg_2028" s="T202">pear-PL-3SG-ACC</ta>
            <ta e="T204" id="Seg_2029" s="T203">next</ta>
            <ta e="T206" id="Seg_2030" s="T205">empty</ta>
            <ta e="T207" id="Seg_2031" s="T206">basket-DAT/LOC</ta>
            <ta e="T208" id="Seg_2032" s="T207">pear-PL-3SG-ACC</ta>
            <ta e="T209" id="Seg_2033" s="T208">stick-CVB.PURP</ta>
            <ta e="T210" id="Seg_2034" s="T209">see-PST2-3SG</ta>
            <ta e="T211" id="Seg_2035" s="T210">one</ta>
            <ta e="T212" id="Seg_2036" s="T211">basket-3SG.[NOM]</ta>
            <ta e="T213" id="Seg_2037" s="T212">NEG.EX</ta>
            <ta e="T214" id="Seg_2038" s="T213">count-PST2-3SG</ta>
            <ta e="T215" id="Seg_2039" s="T214">one</ta>
            <ta e="T216" id="Seg_2040" s="T215">basket-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_2041" s="T216">NEG.EX</ta>
            <ta e="T218" id="Seg_2042" s="T217">that-3SG-2SG.[NOM]</ta>
            <ta e="T219" id="Seg_2043" s="T218">think-VBZ-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_2044" s="T219">sometimes</ta>
            <ta e="T221" id="Seg_2045" s="T220">whereto-EP-INSTR</ta>
            <ta e="T222" id="Seg_2046" s="T221">whereto</ta>
            <ta e="T223" id="Seg_2047" s="T222">go-PST2-3SG=Q</ta>
            <ta e="T224" id="Seg_2048" s="T223">think-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_2049" s="T224">that</ta>
            <ta e="T226" id="Seg_2050" s="T225">think-VBZ-CVB.SIM</ta>
            <ta e="T227" id="Seg_2051" s="T226">stand-TEMP-3SG</ta>
            <ta e="T228" id="Seg_2052" s="T227">EMPH</ta>
            <ta e="T229" id="Seg_2053" s="T228">recent</ta>
            <ta e="T230" id="Seg_2054" s="T229">three</ta>
            <ta e="T231" id="Seg_2055" s="T230">boy.[NOM]</ta>
            <ta e="T232" id="Seg_2056" s="T231">child.[NOM]</ta>
            <ta e="T233" id="Seg_2057" s="T232">proximity-3SG-INSTR</ta>
            <ta e="T234" id="Seg_2058" s="T233">pass.by-CVB.SEQ</ta>
            <ta e="T235" id="Seg_2059" s="T234">pass.by-PST2-3PL</ta>
            <ta e="T236" id="Seg_2060" s="T235">3SG.[NOM]</ta>
            <ta e="T237" id="Seg_2061" s="T236">proximity-3SG-INSTR</ta>
            <ta e="T238" id="Seg_2062" s="T237">well</ta>
            <ta e="T239" id="Seg_2063" s="T238">that-PL-ACC</ta>
            <ta e="T240" id="Seg_2064" s="T239">see-PST1-3SG</ta>
            <ta e="T241" id="Seg_2065" s="T240">see-PTCP.PST-3SG-INSTR</ta>
            <ta e="T242" id="Seg_2066" s="T241">EMPH-then</ta>
            <ta e="T243" id="Seg_2067" s="T242">stay-PST2.[3SG]</ta>
            <ta e="T244" id="Seg_2068" s="T243">gardener.[NOM]</ta>
            <ta e="T245" id="Seg_2069" s="T244">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T3" id="Seg_2070" s="T2">äh</ta>
            <ta e="T5" id="Seg_2071" s="T4">Gärtner.[NOM]</ta>
            <ta e="T6" id="Seg_2072" s="T5">Gärtner.[NOM]</ta>
            <ta e="T7" id="Seg_2073" s="T6">Birne-PL-ACC</ta>
            <ta e="T8" id="Seg_2074" s="T7">sammeln-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_2075" s="T8">eins</ta>
            <ta e="T10" id="Seg_2076" s="T9">Korb.[NOM]</ta>
            <ta e="T11" id="Seg_2077" s="T10">Birne-ACC</ta>
            <ta e="T12" id="Seg_2078" s="T11">sammeln-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_2079" s="T12">jenes</ta>
            <ta e="T14" id="Seg_2080" s="T13">sammeln-CVB.SIM</ta>
            <ta e="T15" id="Seg_2081" s="T14">liegen-TEMP-3SG</ta>
            <ta e="T16" id="Seg_2082" s="T15">EMPH</ta>
            <ta e="T17" id="Seg_2083" s="T16">Nähe-3SG-INSTR</ta>
            <ta e="T18" id="Seg_2084" s="T17">aber</ta>
            <ta e="T19" id="Seg_2085" s="T18">Ziege-PROPR</ta>
            <ta e="T20" id="Seg_2086" s="T19">Mensch.[NOM]</ta>
            <ta e="T21" id="Seg_2087" s="T20">vorbeigehen-PST2-3SG</ta>
            <ta e="T22" id="Seg_2088" s="T21">dann</ta>
            <ta e="T24" id="Seg_2089" s="T23">noch</ta>
            <ta e="T25" id="Seg_2090" s="T24">und</ta>
            <ta e="T28" id="Seg_2091" s="T27">Baum-DAT/LOC</ta>
            <ta e="T29" id="Seg_2092" s="T28">klettern-EP-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_2093" s="T29">noch</ta>
            <ta e="T31" id="Seg_2094" s="T30">und</ta>
            <ta e="T32" id="Seg_2095" s="T31">Birne.[NOM]</ta>
            <ta e="T33" id="Seg_2096" s="T32">sammeln-CVB.PURP</ta>
            <ta e="T34" id="Seg_2097" s="T33">jenes</ta>
            <ta e="T35" id="Seg_2098" s="T34">liegen-TEMP-3SG</ta>
            <ta e="T36" id="Seg_2099" s="T35">EMPH</ta>
            <ta e="T37" id="Seg_2100" s="T36">jenes</ta>
            <ta e="T38" id="Seg_2101" s="T37">Seite-ABL</ta>
            <ta e="T39" id="Seg_2102" s="T38">Fahrrad-PROPR</ta>
            <ta e="T40" id="Seg_2103" s="T39">Junge.[NOM]</ta>
            <ta e="T41" id="Seg_2104" s="T40">Kind.[NOM]</ta>
            <ta e="T42" id="Seg_2105" s="T41">kommen-PST2.[3SG]</ta>
            <ta e="T43" id="Seg_2106" s="T42">gehen-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_2107" s="T43">dann</ta>
            <ta e="T45" id="Seg_2108" s="T44">dieses-ACC</ta>
            <ta e="T46" id="Seg_2109" s="T45">3SG-DAT/LOC</ta>
            <ta e="T47" id="Seg_2110" s="T46">kommen-CVB.SEQ</ta>
            <ta e="T48" id="Seg_2111" s="T47">aber</ta>
            <ta e="T49" id="Seg_2112" s="T48">Birne-PL-ACC</ta>
            <ta e="T50" id="Seg_2113" s="T49">sehen-CVB.SEQ</ta>
            <ta e="T51" id="Seg_2114" s="T50">eins</ta>
            <ta e="T52" id="Seg_2115" s="T51">Birne-ACC</ta>
            <ta e="T53" id="Seg_2116" s="T52">nehmen-CVB.PURP</ta>
            <ta e="T54" id="Seg_2117" s="T53">wollen-PST2-3SG</ta>
            <ta e="T55" id="Seg_2118" s="T54">jenes-3SG-1PL.[NOM]</ta>
            <ta e="T56" id="Seg_2119" s="T55">vorsichtig.sein-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_2120" s="T56">Q</ta>
            <ta e="T58" id="Seg_2121" s="T57">was.[NOM]</ta>
            <ta e="T59" id="Seg_2122" s="T58">Q</ta>
            <ta e="T60" id="Seg_2123" s="T59">so</ta>
            <ta e="T61" id="Seg_2124" s="T60">zwei-ORD</ta>
            <ta e="T62" id="Seg_2125" s="T61">äh</ta>
            <ta e="T63" id="Seg_2126" s="T62">nehmen-PST1-3SG</ta>
            <ta e="T64" id="Seg_2127" s="T63">wenn.auch</ta>
            <ta e="T65" id="Seg_2128" s="T64">vollständig.[NOM]</ta>
            <ta e="T66" id="Seg_2129" s="T65">Korb-ACC</ta>
            <ta e="T68" id="Seg_2130" s="T67">Fahrrad-3SG-DAT/LOC</ta>
            <ta e="T69" id="Seg_2131" s="T68">ankommen-CVB.SEQ</ta>
            <ta e="T70" id="Seg_2132" s="T69">gehen-CVB.SEQ</ta>
            <ta e="T71" id="Seg_2133" s="T70">Pfad.[NOM]</ta>
            <ta e="T72" id="Seg_2134" s="T71">entlang</ta>
            <ta e="T73" id="Seg_2135" s="T72">gehen-CVB.SIM</ta>
            <ta e="T74" id="Seg_2136" s="T73">stehen-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_2137" s="T74">gehen-CVB.SEQ</ta>
            <ta e="T76" id="Seg_2138" s="T75">gehen-CVB.SEQ-gehen-CVB.SEQ</ta>
            <ta e="T78" id="Seg_2139" s="T77">entgegen</ta>
            <ta e="T79" id="Seg_2140" s="T78">Mädchen.[NOM]</ta>
            <ta e="T80" id="Seg_2141" s="T79">Kind-ACC</ta>
            <ta e="T81" id="Seg_2142" s="T80">treffen-EP-PST2.[3SG]</ta>
            <ta e="T82" id="Seg_2143" s="T81">äh</ta>
            <ta e="T83" id="Seg_2144" s="T82">sehen-PST2.[3SG]</ta>
            <ta e="T84" id="Seg_2145" s="T83">jenes-ACC</ta>
            <ta e="T85" id="Seg_2146" s="T84">sehen-PST1-3SG</ta>
            <ta e="T86" id="Seg_2147" s="T85">sehen-PTCP.PST-3SG-INSTR</ta>
            <ta e="T87" id="Seg_2148" s="T86">Ende-EP-ABL</ta>
            <ta e="T88" id="Seg_2149" s="T87">Stein-DAT/LOC</ta>
            <ta e="T89" id="Seg_2150" s="T88">stoßen-CVB.SEQ</ta>
            <ta e="T90" id="Seg_2151" s="T89">Fahrrad-3SG-ABL</ta>
            <ta e="T91" id="Seg_2152" s="T90">hey</ta>
            <ta e="T92" id="Seg_2153" s="T91">krachen-CVB.SEQ</ta>
            <ta e="T93" id="Seg_2154" s="T92">fallen-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_2155" s="T93">Birne-PL-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_2156" s="T94">Erde-DAT/LOC</ta>
            <ta e="T96" id="Seg_2157" s="T95">rollen-CVB.SEQ</ta>
            <ta e="T97" id="Seg_2158" s="T96">fallen-PST2-3PL</ta>
            <ta e="T98" id="Seg_2159" s="T97">jenes</ta>
            <ta e="T99" id="Seg_2160" s="T98">treten-EP-REFL-CVB.SIM</ta>
            <ta e="T100" id="Seg_2161" s="T99">liegen-TEMP-3SG</ta>
            <ta e="T101" id="Seg_2162" s="T100">drei</ta>
            <ta e="T102" id="Seg_2163" s="T101">Junge.[NOM]</ta>
            <ta e="T103" id="Seg_2164" s="T102">Kind.[NOM]</ta>
            <ta e="T104" id="Seg_2165" s="T103">drei</ta>
            <ta e="T105" id="Seg_2166" s="T104">Junge.[NOM]</ta>
            <ta e="T106" id="Seg_2167" s="T105">Kind.[NOM]</ta>
            <ta e="T107" id="Seg_2168" s="T106">sehen-CVB.SEQ</ta>
            <ta e="T108" id="Seg_2169" s="T107">stehen-PST2-3PL</ta>
            <ta e="T109" id="Seg_2170" s="T108">jenes-ACC</ta>
            <ta e="T110" id="Seg_2171" s="T109">drei</ta>
            <ta e="T111" id="Seg_2172" s="T110">Junge.[NOM]</ta>
            <ta e="T112" id="Seg_2173" s="T111">Kind.[NOM]</ta>
            <ta e="T113" id="Seg_2174" s="T112">kommen-EP</ta>
            <ta e="T114" id="Seg_2175" s="T113">jenes-DAT/LOC</ta>
            <ta e="T115" id="Seg_2176" s="T114">kommen-CVB.SEQ-3PL</ta>
            <ta e="T116" id="Seg_2177" s="T115">Birne-PL-3SG-ACC</ta>
            <ta e="T117" id="Seg_2178" s="T116">sammeln-PST2-3PL</ta>
            <ta e="T118" id="Seg_2179" s="T117">selbst-3SG-ACC</ta>
            <ta e="T119" id="Seg_2180" s="T118">Sand-ABL</ta>
            <ta e="T120" id="Seg_2181" s="T119">abschütteln-PST2-3PL</ta>
            <ta e="T121" id="Seg_2182" s="T120">so</ta>
            <ta e="T122" id="Seg_2183" s="T121">machen-CVB.SEQ</ta>
            <ta e="T123" id="Seg_2184" s="T122">nachdem</ta>
            <ta e="T124" id="Seg_2185" s="T123">weiter</ta>
            <ta e="T125" id="Seg_2186" s="T124">3SG.[NOM]</ta>
            <ta e="T126" id="Seg_2187" s="T125">Pfad-3SG-ACC</ta>
            <ta e="T127" id="Seg_2188" s="T126">entlang</ta>
            <ta e="T128" id="Seg_2189" s="T127">go-CVB.SEQ</ta>
            <ta e="T129" id="Seg_2190" s="T128">gehen-PST2-3PL</ta>
            <ta e="T130" id="Seg_2191" s="T129">jenes</ta>
            <ta e="T131" id="Seg_2192" s="T130">go-CVB.SEQ</ta>
            <ta e="T133" id="Seg_2193" s="T132">gehen-CVB.SEQ-3PL</ta>
            <ta e="T134" id="Seg_2194" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_2195" s="T134">äh</ta>
            <ta e="T136" id="Seg_2196" s="T135">Hinterteil-EP-ADJZ</ta>
            <ta e="T137" id="Seg_2197" s="T136">Kind-3PL.[NOM]</ta>
            <ta e="T138" id="Seg_2198" s="T137">nun</ta>
            <ta e="T139" id="Seg_2199" s="T138">dieses.[NOM]</ta>
            <ta e="T140" id="Seg_2200" s="T139">Junge.[NOM]</ta>
            <ta e="T141" id="Seg_2201" s="T140">Kind.[NOM]</ta>
            <ta e="T142" id="Seg_2202" s="T141">Hut-3SG-ACC</ta>
            <ta e="T143" id="Seg_2203" s="T142">Hut-PROPR.[NOM]</ta>
            <ta e="T144" id="Seg_2204" s="T143">sein-PST1-3SG</ta>
            <ta e="T145" id="Seg_2205" s="T144">jenes</ta>
            <ta e="T146" id="Seg_2206" s="T145">Hut-3SG-ACC</ta>
            <ta e="T147" id="Seg_2207" s="T146">EMPH</ta>
            <ta e="T148" id="Seg_2208" s="T147">Mädchen-ACC</ta>
            <ta e="T149" id="Seg_2209" s="T148">sehen-PRS-1SG</ta>
            <ta e="T150" id="Seg_2210" s="T149">sagen-CVB.SEQ</ta>
            <ta e="T151" id="Seg_2211" s="T150">fallen.lassen-PTCP.PST</ta>
            <ta e="T152" id="Seg_2212" s="T151">sein-PST1-3SG</ta>
            <ta e="T153" id="Seg_2213" s="T152">jenes</ta>
            <ta e="T154" id="Seg_2214" s="T153">Hut-2SG-ACC</ta>
            <ta e="T155" id="Seg_2215" s="T154">finden-CVB.SEQ</ta>
            <ta e="T156" id="Seg_2216" s="T155">EMPH</ta>
            <ta e="T157" id="Seg_2217" s="T156">jenes</ta>
            <ta e="T158" id="Seg_2218" s="T157">Junge.[NOM]</ta>
            <ta e="T159" id="Seg_2219" s="T158">Kind-DAT/LOC</ta>
            <ta e="T160" id="Seg_2220" s="T159">bringen</ta>
            <ta e="T161" id="Seg_2221" s="T160">bringen-PST2</ta>
            <ta e="T162" id="Seg_2222" s="T161">pfeifen-PST2-3PL</ta>
            <ta e="T164" id="Seg_2223" s="T163">zuerst</ta>
            <ta e="T165" id="Seg_2224" s="T164">dann</ta>
            <ta e="T166" id="Seg_2225" s="T165">Junge.[NOM]</ta>
            <ta e="T167" id="Seg_2226" s="T166">Kind-2SG.[NOM]</ta>
            <ta e="T168" id="Seg_2227" s="T167">halten-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_2228" s="T168">dann</ta>
            <ta e="T170" id="Seg_2229" s="T169">aber</ta>
            <ta e="T171" id="Seg_2230" s="T170">jenes</ta>
            <ta e="T172" id="Seg_2231" s="T171">EMPH</ta>
            <ta e="T173" id="Seg_2232" s="T172">Hinterteil-EP-ADJZ</ta>
            <ta e="T174" id="Seg_2233" s="T173">Kind-2SG.[NOM]</ta>
            <ta e="T175" id="Seg_2234" s="T174">MOD</ta>
            <ta e="T176" id="Seg_2235" s="T175">jenes</ta>
            <ta e="T177" id="Seg_2236" s="T176">wer-3SG-ACC</ta>
            <ta e="T178" id="Seg_2237" s="T177">Hut-3SG-ACC</ta>
            <ta e="T179" id="Seg_2238" s="T178">geben-CVB.SIM</ta>
            <ta e="T180" id="Seg_2239" s="T179">gehen-PST2.[3SG]</ta>
            <ta e="T181" id="Seg_2240" s="T180">so</ta>
            <ta e="T182" id="Seg_2241" s="T181">geben-CVB.SEQ</ta>
            <ta e="T183" id="Seg_2242" s="T182">gehen-CVB.SEQ</ta>
            <ta e="T184" id="Seg_2243" s="T183">EMPH</ta>
            <ta e="T185" id="Seg_2244" s="T184">drei</ta>
            <ta e="T186" id="Seg_2245" s="T185">drei</ta>
            <ta e="T187" id="Seg_2246" s="T186">Birne-ACC</ta>
            <ta e="T188" id="Seg_2247" s="T187">bekommen-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_2248" s="T188">und</ta>
            <ta e="T190" id="Seg_2249" s="T189">dieses-3SG-2SG.[NOM]</ta>
            <ta e="T191" id="Seg_2250" s="T190">weiter</ta>
            <ta e="T192" id="Seg_2251" s="T191">gehen-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_2252" s="T192">dann</ta>
            <ta e="T194" id="Seg_2253" s="T193">dieses-PL.[NOM]</ta>
            <ta e="T195" id="Seg_2254" s="T194">gehen-CVB.SIM</ta>
            <ta e="T196" id="Seg_2255" s="T195">stehen-TEMP-3PL</ta>
            <ta e="T197" id="Seg_2256" s="T196">EMPH</ta>
            <ta e="T198" id="Seg_2257" s="T197">Gärtner.[NOM]</ta>
            <ta e="T199" id="Seg_2258" s="T198">jenes</ta>
            <ta e="T200" id="Seg_2259" s="T199">Stufe-3SG-ABL</ta>
            <ta e="T201" id="Seg_2260" s="T200">fallen-CVB.SEQ</ta>
            <ta e="T202" id="Seg_2261" s="T201">gehen-CVB.SEQ</ta>
            <ta e="T203" id="Seg_2262" s="T202">Birne-PL-3SG-ACC</ta>
            <ta e="T204" id="Seg_2263" s="T203">nächster</ta>
            <ta e="T206" id="Seg_2264" s="T205">leer</ta>
            <ta e="T207" id="Seg_2265" s="T206">Korb-DAT/LOC</ta>
            <ta e="T208" id="Seg_2266" s="T207">Birne-PL-3SG-ACC</ta>
            <ta e="T209" id="Seg_2267" s="T208">stecken-CVB.PURP</ta>
            <ta e="T210" id="Seg_2268" s="T209">sehen-PST2-3SG</ta>
            <ta e="T211" id="Seg_2269" s="T210">eins</ta>
            <ta e="T212" id="Seg_2270" s="T211">Korb-3SG.[NOM]</ta>
            <ta e="T213" id="Seg_2271" s="T212">NEG.EX</ta>
            <ta e="T214" id="Seg_2272" s="T213">zählen-PST2-3SG</ta>
            <ta e="T215" id="Seg_2273" s="T214">eins</ta>
            <ta e="T216" id="Seg_2274" s="T215">Korb-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_2275" s="T216">NEG.EX</ta>
            <ta e="T218" id="Seg_2276" s="T217">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T219" id="Seg_2277" s="T218">denken-VBZ-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_2278" s="T219">manchmal</ta>
            <ta e="T221" id="Seg_2279" s="T220">wohin-EP-INSTR</ta>
            <ta e="T222" id="Seg_2280" s="T221">wohin</ta>
            <ta e="T223" id="Seg_2281" s="T222">gehen-PST2-3SG=Q</ta>
            <ta e="T224" id="Seg_2282" s="T223">denken-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_2283" s="T224">jenes</ta>
            <ta e="T226" id="Seg_2284" s="T225">denken-VBZ-CVB.SIM</ta>
            <ta e="T227" id="Seg_2285" s="T226">stehen-TEMP-3SG</ta>
            <ta e="T228" id="Seg_2286" s="T227">EMPH</ta>
            <ta e="T229" id="Seg_2287" s="T228">kürzlich</ta>
            <ta e="T230" id="Seg_2288" s="T229">drei</ta>
            <ta e="T231" id="Seg_2289" s="T230">Junge.[NOM]</ta>
            <ta e="T232" id="Seg_2290" s="T231">Kind.[NOM]</ta>
            <ta e="T233" id="Seg_2291" s="T232">Nähe-3SG-INSTR</ta>
            <ta e="T234" id="Seg_2292" s="T233">vorbeigehen-CVB.SEQ</ta>
            <ta e="T235" id="Seg_2293" s="T234">vorbeigehen-PST2-3PL</ta>
            <ta e="T236" id="Seg_2294" s="T235">3SG.[NOM]</ta>
            <ta e="T237" id="Seg_2295" s="T236">Nähe-3SG-INSTR</ta>
            <ta e="T238" id="Seg_2296" s="T237">nun</ta>
            <ta e="T239" id="Seg_2297" s="T238">jenes-PL-ACC</ta>
            <ta e="T240" id="Seg_2298" s="T239">sehen-PST1-3SG</ta>
            <ta e="T241" id="Seg_2299" s="T240">sehen-PTCP.PST-3SG-INSTR</ta>
            <ta e="T242" id="Seg_2300" s="T241">EMPH-dann</ta>
            <ta e="T243" id="Seg_2301" s="T242">bleiben-PST2.[3SG]</ta>
            <ta e="T244" id="Seg_2302" s="T243">Gärtner.[NOM]</ta>
            <ta e="T245" id="Seg_2303" s="T244">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T3" id="Seg_2304" s="T2">ээ</ta>
            <ta e="T5" id="Seg_2305" s="T4">садовник.[NOM]</ta>
            <ta e="T6" id="Seg_2306" s="T5">садовник.[NOM]</ta>
            <ta e="T7" id="Seg_2307" s="T6">груша-PL-ACC</ta>
            <ta e="T8" id="Seg_2308" s="T7">собирать-PRS.[3SG]</ta>
            <ta e="T9" id="Seg_2309" s="T8">один</ta>
            <ta e="T10" id="Seg_2310" s="T9">корзина.[NOM]</ta>
            <ta e="T11" id="Seg_2311" s="T10">груша-ACC</ta>
            <ta e="T12" id="Seg_2312" s="T11">собирать-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_2313" s="T12">тот</ta>
            <ta e="T14" id="Seg_2314" s="T13">собирать-CVB.SIM</ta>
            <ta e="T15" id="Seg_2315" s="T14">лежать-TEMP-3SG</ta>
            <ta e="T16" id="Seg_2316" s="T15">EMPH</ta>
            <ta e="T17" id="Seg_2317" s="T16">близость-3SG-INSTR</ta>
            <ta e="T18" id="Seg_2318" s="T17">однако</ta>
            <ta e="T19" id="Seg_2319" s="T18">коза-PROPR</ta>
            <ta e="T20" id="Seg_2320" s="T19">человек.[NOM]</ta>
            <ta e="T21" id="Seg_2321" s="T20">проехать-PST2-3SG</ta>
            <ta e="T22" id="Seg_2322" s="T21">потом</ta>
            <ta e="T24" id="Seg_2323" s="T23">еще</ta>
            <ta e="T25" id="Seg_2324" s="T24">да</ta>
            <ta e="T28" id="Seg_2325" s="T27">дерево-DAT/LOC</ta>
            <ta e="T29" id="Seg_2326" s="T28">лезть-EP-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_2327" s="T29">еще</ta>
            <ta e="T31" id="Seg_2328" s="T30">да</ta>
            <ta e="T32" id="Seg_2329" s="T31">груша.[NOM]</ta>
            <ta e="T33" id="Seg_2330" s="T32">собирать-CVB.PURP</ta>
            <ta e="T34" id="Seg_2331" s="T33">тот</ta>
            <ta e="T35" id="Seg_2332" s="T34">лежать-TEMP-3SG</ta>
            <ta e="T36" id="Seg_2333" s="T35">EMPH</ta>
            <ta e="T37" id="Seg_2334" s="T36">тот</ta>
            <ta e="T38" id="Seg_2335" s="T37">сторона-ABL</ta>
            <ta e="T39" id="Seg_2336" s="T38">велосипед-PROPR</ta>
            <ta e="T40" id="Seg_2337" s="T39">мальчик.[NOM]</ta>
            <ta e="T41" id="Seg_2338" s="T40">ребенок.[NOM]</ta>
            <ta e="T42" id="Seg_2339" s="T41">приходить-PST2.[3SG]</ta>
            <ta e="T43" id="Seg_2340" s="T42">идти-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_2341" s="T43">потом</ta>
            <ta e="T45" id="Seg_2342" s="T44">тот-ACC</ta>
            <ta e="T46" id="Seg_2343" s="T45">3SG-DAT/LOC</ta>
            <ta e="T47" id="Seg_2344" s="T46">приходить-CVB.SEQ</ta>
            <ta e="T48" id="Seg_2345" s="T47">однако</ta>
            <ta e="T49" id="Seg_2346" s="T48">груша-PL-ACC</ta>
            <ta e="T50" id="Seg_2347" s="T49">видеть-CVB.SEQ</ta>
            <ta e="T51" id="Seg_2348" s="T50">один</ta>
            <ta e="T52" id="Seg_2349" s="T51">груша-ACC</ta>
            <ta e="T53" id="Seg_2350" s="T52">взять-CVB.PURP</ta>
            <ta e="T54" id="Seg_2351" s="T53">хотеть-PST2-3SG</ta>
            <ta e="T55" id="Seg_2352" s="T54">тот-3SG-1PL.[NOM]</ta>
            <ta e="T56" id="Seg_2353" s="T55">беречься-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_2354" s="T56">Q</ta>
            <ta e="T58" id="Seg_2355" s="T57">что.[NOM]</ta>
            <ta e="T59" id="Seg_2356" s="T58">Q</ta>
            <ta e="T60" id="Seg_2357" s="T59">так</ta>
            <ta e="T61" id="Seg_2358" s="T60">два-ORD</ta>
            <ta e="T62" id="Seg_2359" s="T61">э</ta>
            <ta e="T63" id="Seg_2360" s="T62">взять-PST1-3SG</ta>
            <ta e="T64" id="Seg_2361" s="T63">хоть</ta>
            <ta e="T65" id="Seg_2362" s="T64">полный.[NOM]</ta>
            <ta e="T66" id="Seg_2363" s="T65">корзина-ACC</ta>
            <ta e="T68" id="Seg_2364" s="T67">велосипед-3SG-DAT/LOC</ta>
            <ta e="T69" id="Seg_2365" s="T68">доезжать-CVB.SEQ</ta>
            <ta e="T70" id="Seg_2366" s="T69">идти-CVB.SEQ</ta>
            <ta e="T71" id="Seg_2367" s="T70">тропка.[NOM]</ta>
            <ta e="T72" id="Seg_2368" s="T71">вдоль</ta>
            <ta e="T73" id="Seg_2369" s="T72">идти-CVB.SIM</ta>
            <ta e="T74" id="Seg_2370" s="T73">стоять-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_2371" s="T74">идти-CVB.SEQ</ta>
            <ta e="T76" id="Seg_2372" s="T75">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T78" id="Seg_2373" s="T77">навстречу</ta>
            <ta e="T79" id="Seg_2374" s="T78">девушка.[NOM]</ta>
            <ta e="T80" id="Seg_2375" s="T79">ребенок-ACC</ta>
            <ta e="T81" id="Seg_2376" s="T80">встречать-EP-PST2.[3SG]</ta>
            <ta e="T82" id="Seg_2377" s="T81">ээ</ta>
            <ta e="T83" id="Seg_2378" s="T82">видеть-PST2.[3SG]</ta>
            <ta e="T84" id="Seg_2379" s="T83">тот-ACC</ta>
            <ta e="T85" id="Seg_2380" s="T84">видеть-PST1-3SG</ta>
            <ta e="T86" id="Seg_2381" s="T85">видеть-PTCP.PST-3SG-INSTR</ta>
            <ta e="T87" id="Seg_2382" s="T86">конец-EP-ABL</ta>
            <ta e="T88" id="Seg_2383" s="T87">камень-DAT/LOC</ta>
            <ta e="T89" id="Seg_2384" s="T88">толкать-CVB.SEQ</ta>
            <ta e="T90" id="Seg_2385" s="T89">велосипед-3SG-ABL</ta>
            <ta e="T91" id="Seg_2386" s="T90">ээ</ta>
            <ta e="T92" id="Seg_2387" s="T91">рухнуть-CVB.SEQ</ta>
            <ta e="T93" id="Seg_2388" s="T92">падать-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_2389" s="T93">груша-PL-3SG.[NOM]</ta>
            <ta e="T95" id="Seg_2390" s="T94">земля-DAT/LOC</ta>
            <ta e="T96" id="Seg_2391" s="T95">кататься-CVB.SEQ</ta>
            <ta e="T97" id="Seg_2392" s="T96">падать-PST2-3PL</ta>
            <ta e="T98" id="Seg_2393" s="T97">тот</ta>
            <ta e="T99" id="Seg_2394" s="T98">пнуть-EP-REFL-CVB.SIM</ta>
            <ta e="T100" id="Seg_2395" s="T99">лежать-TEMP-3SG</ta>
            <ta e="T101" id="Seg_2396" s="T100">три</ta>
            <ta e="T102" id="Seg_2397" s="T101">мальчик.[NOM]</ta>
            <ta e="T103" id="Seg_2398" s="T102">ребенок.[NOM]</ta>
            <ta e="T104" id="Seg_2399" s="T103">три</ta>
            <ta e="T105" id="Seg_2400" s="T104">мальчик.[NOM]</ta>
            <ta e="T106" id="Seg_2401" s="T105">ребенок.[NOM]</ta>
            <ta e="T107" id="Seg_2402" s="T106">видеть-CVB.SEQ</ta>
            <ta e="T108" id="Seg_2403" s="T107">стоять-PST2-3PL</ta>
            <ta e="T109" id="Seg_2404" s="T108">тот-ACC</ta>
            <ta e="T110" id="Seg_2405" s="T109">три</ta>
            <ta e="T111" id="Seg_2406" s="T110">мальчик.[NOM]</ta>
            <ta e="T112" id="Seg_2407" s="T111">ребенок.[NOM]</ta>
            <ta e="T113" id="Seg_2408" s="T112">приходить-EP</ta>
            <ta e="T114" id="Seg_2409" s="T113">тот-DAT/LOC</ta>
            <ta e="T115" id="Seg_2410" s="T114">приходить-CVB.SEQ-3PL</ta>
            <ta e="T116" id="Seg_2411" s="T115">груша-PL-3SG-ACC</ta>
            <ta e="T117" id="Seg_2412" s="T116">собирать-PST2-3PL</ta>
            <ta e="T118" id="Seg_2413" s="T117">сам-3SG-ACC</ta>
            <ta e="T119" id="Seg_2414" s="T118">песок-ABL</ta>
            <ta e="T120" id="Seg_2415" s="T119">отряхивать-PST2-3PL</ta>
            <ta e="T121" id="Seg_2416" s="T120">так</ta>
            <ta e="T122" id="Seg_2417" s="T121">делать-CVB.SEQ</ta>
            <ta e="T123" id="Seg_2418" s="T122">после</ta>
            <ta e="T124" id="Seg_2419" s="T123">дальше</ta>
            <ta e="T125" id="Seg_2420" s="T124">3SG.[NOM]</ta>
            <ta e="T126" id="Seg_2421" s="T125">тропка-3SG-ACC</ta>
            <ta e="T127" id="Seg_2422" s="T126">вдоль</ta>
            <ta e="T128" id="Seg_2423" s="T127">идти-CVB.SEQ</ta>
            <ta e="T129" id="Seg_2424" s="T128">идти-PST2-3PL</ta>
            <ta e="T130" id="Seg_2425" s="T129">тот</ta>
            <ta e="T131" id="Seg_2426" s="T130">идти-CVB.SEQ</ta>
            <ta e="T133" id="Seg_2427" s="T132">идти-CVB.SEQ-3PL</ta>
            <ta e="T134" id="Seg_2428" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_2429" s="T134">ээ</ta>
            <ta e="T136" id="Seg_2430" s="T135">задняя.часть-EP-ADJZ</ta>
            <ta e="T137" id="Seg_2431" s="T136">ребенок-3PL.[NOM]</ta>
            <ta e="T138" id="Seg_2432" s="T137">вот</ta>
            <ta e="T139" id="Seg_2433" s="T138">тот.[NOM]</ta>
            <ta e="T140" id="Seg_2434" s="T139">мальчик.[NOM]</ta>
            <ta e="T141" id="Seg_2435" s="T140">ребенок.[NOM]</ta>
            <ta e="T142" id="Seg_2436" s="T141">шляпа-3SG-ACC</ta>
            <ta e="T143" id="Seg_2437" s="T142">шляпа-PROPR.[NOM]</ta>
            <ta e="T144" id="Seg_2438" s="T143">быть-PST1-3SG</ta>
            <ta e="T145" id="Seg_2439" s="T144">тот</ta>
            <ta e="T146" id="Seg_2440" s="T145">шляпа-3SG-ACC</ta>
            <ta e="T147" id="Seg_2441" s="T146">EMPH</ta>
            <ta e="T148" id="Seg_2442" s="T147">девушка-ACC</ta>
            <ta e="T149" id="Seg_2443" s="T148">видеть-PRS-1SG</ta>
            <ta e="T150" id="Seg_2444" s="T149">говорить-CVB.SEQ</ta>
            <ta e="T151" id="Seg_2445" s="T150">спустить-PTCP.PST</ta>
            <ta e="T152" id="Seg_2446" s="T151">быть-PST1-3SG</ta>
            <ta e="T153" id="Seg_2447" s="T152">тот</ta>
            <ta e="T154" id="Seg_2448" s="T153">шляпа-2SG-ACC</ta>
            <ta e="T155" id="Seg_2449" s="T154">найти-CVB.SEQ</ta>
            <ta e="T156" id="Seg_2450" s="T155">EMPH</ta>
            <ta e="T157" id="Seg_2451" s="T156">тот</ta>
            <ta e="T158" id="Seg_2452" s="T157">мальчик.[NOM]</ta>
            <ta e="T159" id="Seg_2453" s="T158">ребенок-DAT/LOC</ta>
            <ta e="T160" id="Seg_2454" s="T159">приносить</ta>
            <ta e="T161" id="Seg_2455" s="T160">приносить-PST2</ta>
            <ta e="T162" id="Seg_2456" s="T161">свистеть-PST2-3PL</ta>
            <ta e="T164" id="Seg_2457" s="T163">сначала</ta>
            <ta e="T165" id="Seg_2458" s="T164">потом</ta>
            <ta e="T166" id="Seg_2459" s="T165">мальчик.[NOM]</ta>
            <ta e="T167" id="Seg_2460" s="T166">ребенок-2SG.[NOM]</ta>
            <ta e="T168" id="Seg_2461" s="T167">останавливаться-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_2462" s="T168">потом</ta>
            <ta e="T170" id="Seg_2463" s="T169">однако</ta>
            <ta e="T171" id="Seg_2464" s="T170">тот</ta>
            <ta e="T172" id="Seg_2465" s="T171">EMPH</ta>
            <ta e="T173" id="Seg_2466" s="T172">задняя.часть-EP-ADJZ</ta>
            <ta e="T174" id="Seg_2467" s="T173">ребенок-2SG.[NOM]</ta>
            <ta e="T175" id="Seg_2468" s="T174">MOD</ta>
            <ta e="T176" id="Seg_2469" s="T175">тот</ta>
            <ta e="T177" id="Seg_2470" s="T176">кто-3SG-ACC</ta>
            <ta e="T178" id="Seg_2471" s="T177">шляпа-3SG-ACC</ta>
            <ta e="T179" id="Seg_2472" s="T178">давать-CVB.SIM</ta>
            <ta e="T180" id="Seg_2473" s="T179">идти-PST2.[3SG]</ta>
            <ta e="T181" id="Seg_2474" s="T180">так</ta>
            <ta e="T182" id="Seg_2475" s="T181">давать-CVB.SEQ</ta>
            <ta e="T183" id="Seg_2476" s="T182">идти-CVB.SEQ</ta>
            <ta e="T184" id="Seg_2477" s="T183">EMPH</ta>
            <ta e="T185" id="Seg_2478" s="T184">три</ta>
            <ta e="T186" id="Seg_2479" s="T185">три</ta>
            <ta e="T187" id="Seg_2480" s="T186">груша-ACC</ta>
            <ta e="T188" id="Seg_2481" s="T187">получить-PST2.[3SG]</ta>
            <ta e="T189" id="Seg_2482" s="T188">и</ta>
            <ta e="T190" id="Seg_2483" s="T189">этот-3SG-2SG.[NOM]</ta>
            <ta e="T191" id="Seg_2484" s="T190">дальше</ta>
            <ta e="T192" id="Seg_2485" s="T191">идти-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_2486" s="T192">потом</ta>
            <ta e="T194" id="Seg_2487" s="T193">этот-PL.[NOM]</ta>
            <ta e="T195" id="Seg_2488" s="T194">идти-CVB.SIM</ta>
            <ta e="T196" id="Seg_2489" s="T195">стоять-TEMP-3PL</ta>
            <ta e="T197" id="Seg_2490" s="T196">EMPH</ta>
            <ta e="T198" id="Seg_2491" s="T197">садовник.[NOM]</ta>
            <ta e="T199" id="Seg_2492" s="T198">тот</ta>
            <ta e="T200" id="Seg_2493" s="T199">ступень-3SG-ABL</ta>
            <ta e="T201" id="Seg_2494" s="T200">падать-CVB.SEQ</ta>
            <ta e="T202" id="Seg_2495" s="T201">идти-CVB.SEQ</ta>
            <ta e="T203" id="Seg_2496" s="T202">груша-PL-3SG-ACC</ta>
            <ta e="T204" id="Seg_2497" s="T203">следующий</ta>
            <ta e="T206" id="Seg_2498" s="T205">пустой</ta>
            <ta e="T207" id="Seg_2499" s="T206">корзина-DAT/LOC</ta>
            <ta e="T208" id="Seg_2500" s="T207">груша-PL-3SG-ACC</ta>
            <ta e="T209" id="Seg_2501" s="T208">вставлять-CVB.PURP</ta>
            <ta e="T210" id="Seg_2502" s="T209">видеть-PST2-3SG</ta>
            <ta e="T211" id="Seg_2503" s="T210">один</ta>
            <ta e="T212" id="Seg_2504" s="T211">корзина-3SG.[NOM]</ta>
            <ta e="T213" id="Seg_2505" s="T212">NEG.EX</ta>
            <ta e="T214" id="Seg_2506" s="T213">считать-PST2-3SG</ta>
            <ta e="T215" id="Seg_2507" s="T214">один</ta>
            <ta e="T216" id="Seg_2508" s="T215">корзина-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_2509" s="T216">NEG.EX</ta>
            <ta e="T218" id="Seg_2510" s="T217">тот-3SG-2SG.[NOM]</ta>
            <ta e="T219" id="Seg_2511" s="T218">думать-VBZ-PRS.[3SG]</ta>
            <ta e="T220" id="Seg_2512" s="T219">иногда</ta>
            <ta e="T221" id="Seg_2513" s="T220">куда-EP-INSTR</ta>
            <ta e="T222" id="Seg_2514" s="T221">куда</ta>
            <ta e="T223" id="Seg_2515" s="T222">идти-PST2-3SG=Q</ta>
            <ta e="T224" id="Seg_2516" s="T223">думать-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_2517" s="T224">тот</ta>
            <ta e="T226" id="Seg_2518" s="T225">думать-VBZ-CVB.SIM</ta>
            <ta e="T227" id="Seg_2519" s="T226">стоять-TEMP-3SG</ta>
            <ta e="T228" id="Seg_2520" s="T227">EMPH</ta>
            <ta e="T229" id="Seg_2521" s="T228">давешний</ta>
            <ta e="T230" id="Seg_2522" s="T229">три</ta>
            <ta e="T231" id="Seg_2523" s="T230">мальчик.[NOM]</ta>
            <ta e="T232" id="Seg_2524" s="T231">ребенок.[NOM]</ta>
            <ta e="T233" id="Seg_2525" s="T232">близость-3SG-INSTR</ta>
            <ta e="T234" id="Seg_2526" s="T233">проехать-CVB.SEQ</ta>
            <ta e="T235" id="Seg_2527" s="T234">проехать-PST2-3PL</ta>
            <ta e="T236" id="Seg_2528" s="T235">3SG.[NOM]</ta>
            <ta e="T237" id="Seg_2529" s="T236">близость-3SG-INSTR</ta>
            <ta e="T238" id="Seg_2530" s="T237">вот</ta>
            <ta e="T239" id="Seg_2531" s="T238">тот-PL-ACC</ta>
            <ta e="T240" id="Seg_2532" s="T239">видеть-PST1-3SG</ta>
            <ta e="T241" id="Seg_2533" s="T240">видеть-PTCP.PST-3SG-INSTR</ta>
            <ta e="T242" id="Seg_2534" s="T241">EMPH-вот</ta>
            <ta e="T243" id="Seg_2535" s="T242">оставаться-PST2.[3SG]</ta>
            <ta e="T244" id="Seg_2536" s="T243">садовник.[NOM]</ta>
            <ta e="T245" id="Seg_2537" s="T244">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T3" id="Seg_2538" s="T2">interj</ta>
            <ta e="T5" id="Seg_2539" s="T4">n.[n:case]</ta>
            <ta e="T6" id="Seg_2540" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_2541" s="T6">n-n:(num)-n:case</ta>
            <ta e="T8" id="Seg_2542" s="T7">v-v:tense.[v:pred.pn]</ta>
            <ta e="T9" id="Seg_2543" s="T8">cardnum</ta>
            <ta e="T10" id="Seg_2544" s="T9">n.[n:case]</ta>
            <ta e="T11" id="Seg_2545" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_2546" s="T11">v-v:tense.[v:pred.pn]</ta>
            <ta e="T13" id="Seg_2547" s="T12">dempro</ta>
            <ta e="T14" id="Seg_2548" s="T13">v-v:cvb</ta>
            <ta e="T15" id="Seg_2549" s="T14">v-v:mood-v:temp.pn</ta>
            <ta e="T16" id="Seg_2550" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_2551" s="T16">n-n:poss-n:case</ta>
            <ta e="T18" id="Seg_2552" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_2553" s="T18">n-n&gt;adj</ta>
            <ta e="T20" id="Seg_2554" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_2555" s="T20">v-v:tense-v:poss.pn</ta>
            <ta e="T22" id="Seg_2556" s="T21">adv</ta>
            <ta e="T24" id="Seg_2557" s="T23">adv</ta>
            <ta e="T25" id="Seg_2558" s="T24">conj</ta>
            <ta e="T28" id="Seg_2559" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_2560" s="T28">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T30" id="Seg_2561" s="T29">adv</ta>
            <ta e="T31" id="Seg_2562" s="T30">conj</ta>
            <ta e="T32" id="Seg_2563" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_2564" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_2565" s="T33">dempro</ta>
            <ta e="T35" id="Seg_2566" s="T34">v-v:mood-v:temp.pn</ta>
            <ta e="T36" id="Seg_2567" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2568" s="T36">dempro</ta>
            <ta e="T38" id="Seg_2569" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_2570" s="T38">n-n&gt;adj</ta>
            <ta e="T40" id="Seg_2571" s="T39">n.[n:case]</ta>
            <ta e="T41" id="Seg_2572" s="T40">n.[n:case]</ta>
            <ta e="T42" id="Seg_2573" s="T41">v-v:tense.[v:pred.pn]</ta>
            <ta e="T43" id="Seg_2574" s="T42">v-v:tense.[v:pred.pn]</ta>
            <ta e="T44" id="Seg_2575" s="T43">adv</ta>
            <ta e="T45" id="Seg_2576" s="T44">dempro-pro:case</ta>
            <ta e="T46" id="Seg_2577" s="T45">pers-pro:case</ta>
            <ta e="T47" id="Seg_2578" s="T46">v-v:cvb</ta>
            <ta e="T48" id="Seg_2579" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_2580" s="T48">n-n:(num)-n:case</ta>
            <ta e="T50" id="Seg_2581" s="T49">v-v:cvb</ta>
            <ta e="T51" id="Seg_2582" s="T50">cardnum</ta>
            <ta e="T52" id="Seg_2583" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_2584" s="T52">v-v:cvb</ta>
            <ta e="T54" id="Seg_2585" s="T53">v-v:tense-v:poss.pn</ta>
            <ta e="T55" id="Seg_2586" s="T54">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T56" id="Seg_2587" s="T55">v-v:tense.[v:pred.pn]</ta>
            <ta e="T57" id="Seg_2588" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_2589" s="T57">que.[pro:case]</ta>
            <ta e="T59" id="Seg_2590" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_2591" s="T59">adv</ta>
            <ta e="T61" id="Seg_2592" s="T60">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T62" id="Seg_2593" s="T61">interj</ta>
            <ta e="T63" id="Seg_2594" s="T62">v-v:tense-v:poss.pn</ta>
            <ta e="T64" id="Seg_2595" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_2596" s="T64">adj.[n:case]</ta>
            <ta e="T66" id="Seg_2597" s="T65">n-n:case</ta>
            <ta e="T68" id="Seg_2598" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_2599" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_2600" s="T69">v-v:cvb</ta>
            <ta e="T71" id="Seg_2601" s="T70">n.[n:case]</ta>
            <ta e="T72" id="Seg_2602" s="T71">post</ta>
            <ta e="T73" id="Seg_2603" s="T72">v-v:cvb</ta>
            <ta e="T74" id="Seg_2604" s="T73">v-v:tense.[v:pred.pn]</ta>
            <ta e="T75" id="Seg_2605" s="T74">v-v:cvb</ta>
            <ta e="T76" id="Seg_2606" s="T75">v-v:cvb-v-v:cvb</ta>
            <ta e="T78" id="Seg_2607" s="T77">adv</ta>
            <ta e="T79" id="Seg_2608" s="T78">n.[n:case]</ta>
            <ta e="T80" id="Seg_2609" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_2610" s="T80">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T82" id="Seg_2611" s="T81">interj</ta>
            <ta e="T83" id="Seg_2612" s="T82">v-v:tense.[v:pred.pn]</ta>
            <ta e="T84" id="Seg_2613" s="T83">dempro-pro:case</ta>
            <ta e="T85" id="Seg_2614" s="T84">v-v:tense-v:poss.pn</ta>
            <ta e="T86" id="Seg_2615" s="T85">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T87" id="Seg_2616" s="T86">n-n:(ins)-n:case</ta>
            <ta e="T88" id="Seg_2617" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_2618" s="T88">v-v:cvb</ta>
            <ta e="T90" id="Seg_2619" s="T89">n-n:poss-n:case</ta>
            <ta e="T91" id="Seg_2620" s="T90">interj</ta>
            <ta e="T92" id="Seg_2621" s="T91">v-v:cvb</ta>
            <ta e="T93" id="Seg_2622" s="T92">v-v:tense.[v:pred.pn]</ta>
            <ta e="T94" id="Seg_2623" s="T93">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T95" id="Seg_2624" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_2625" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_2626" s="T96">v-v:tense-v:pred.pn</ta>
            <ta e="T98" id="Seg_2627" s="T97">dempro</ta>
            <ta e="T99" id="Seg_2628" s="T98">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T100" id="Seg_2629" s="T99">v-v:mood-v:temp.pn</ta>
            <ta e="T101" id="Seg_2630" s="T100">cardnum</ta>
            <ta e="T102" id="Seg_2631" s="T101">n.[n:case]</ta>
            <ta e="T103" id="Seg_2632" s="T102">n.[n:case]</ta>
            <ta e="T104" id="Seg_2633" s="T103">cardnum</ta>
            <ta e="T105" id="Seg_2634" s="T104">n.[n:case]</ta>
            <ta e="T106" id="Seg_2635" s="T105">n.[n:case]</ta>
            <ta e="T107" id="Seg_2636" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_2637" s="T107">v-v:tense-v:poss.pn</ta>
            <ta e="T109" id="Seg_2638" s="T108">dempro-pro:case</ta>
            <ta e="T110" id="Seg_2639" s="T109">cardnum</ta>
            <ta e="T111" id="Seg_2640" s="T110">n.[n:case]</ta>
            <ta e="T112" id="Seg_2641" s="T111">n.[n:case]</ta>
            <ta e="T113" id="Seg_2642" s="T112">v-v:(ins)</ta>
            <ta e="T114" id="Seg_2643" s="T113">dempro-pro:case</ta>
            <ta e="T115" id="Seg_2644" s="T114">v-v:cvb-v:pred.pn</ta>
            <ta e="T116" id="Seg_2645" s="T115">n-n:(num)-n:poss-n:case</ta>
            <ta e="T117" id="Seg_2646" s="T116">v-v:tense-v:pred.pn</ta>
            <ta e="T118" id="Seg_2647" s="T117">emphpro-n:poss-n:case</ta>
            <ta e="T119" id="Seg_2648" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_2649" s="T119">v-v:tense-v:pred.pn</ta>
            <ta e="T121" id="Seg_2650" s="T120">adv</ta>
            <ta e="T122" id="Seg_2651" s="T121">v-v:cvb</ta>
            <ta e="T123" id="Seg_2652" s="T122">post</ta>
            <ta e="T124" id="Seg_2653" s="T123">adv</ta>
            <ta e="T125" id="Seg_2654" s="T124">pers.[pro:case]</ta>
            <ta e="T126" id="Seg_2655" s="T125">n-n:poss-n:case</ta>
            <ta e="T127" id="Seg_2656" s="T126">post</ta>
            <ta e="T128" id="Seg_2657" s="T127">v-v:cvb</ta>
            <ta e="T129" id="Seg_2658" s="T128">v-v:tense-v:pred.pn</ta>
            <ta e="T130" id="Seg_2659" s="T129">dempro</ta>
            <ta e="T131" id="Seg_2660" s="T130">v-v:cvb</ta>
            <ta e="T133" id="Seg_2661" s="T132">v-v:cvb-v:pred.pn</ta>
            <ta e="T134" id="Seg_2662" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_2663" s="T134">interj</ta>
            <ta e="T136" id="Seg_2664" s="T135">n-n:(ins)-n&gt;adj</ta>
            <ta e="T137" id="Seg_2665" s="T136">n-n:(poss).[n:case]</ta>
            <ta e="T138" id="Seg_2666" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_2667" s="T138">dempro.[pro:case]</ta>
            <ta e="T140" id="Seg_2668" s="T139">n.[n:case]</ta>
            <ta e="T141" id="Seg_2669" s="T140">n.[n:case]</ta>
            <ta e="T142" id="Seg_2670" s="T141">n-n:poss-n:case</ta>
            <ta e="T143" id="Seg_2671" s="T142">n-n&gt;adj.[n:case]</ta>
            <ta e="T144" id="Seg_2672" s="T143">v-v:tense-v:poss.pn</ta>
            <ta e="T145" id="Seg_2673" s="T144">dempro</ta>
            <ta e="T146" id="Seg_2674" s="T145">n-n:poss-n:case</ta>
            <ta e="T147" id="Seg_2675" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_2676" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_2677" s="T148">v-v:tense-v:pred.pn</ta>
            <ta e="T150" id="Seg_2678" s="T149">v-v:cvb</ta>
            <ta e="T151" id="Seg_2679" s="T150">v-v:ptcp</ta>
            <ta e="T152" id="Seg_2680" s="T151">v-v:tense-v:poss.pn</ta>
            <ta e="T153" id="Seg_2681" s="T152">dempro</ta>
            <ta e="T154" id="Seg_2682" s="T153">n-n:poss-n:case</ta>
            <ta e="T155" id="Seg_2683" s="T154">v-v:cvb</ta>
            <ta e="T156" id="Seg_2684" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_2685" s="T156">dempro</ta>
            <ta e="T158" id="Seg_2686" s="T157">n.[n:case]</ta>
            <ta e="T159" id="Seg_2687" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_2688" s="T159">v</ta>
            <ta e="T161" id="Seg_2689" s="T160">v-v:tense</ta>
            <ta e="T162" id="Seg_2690" s="T161">v-v:tense-v:pred.pn</ta>
            <ta e="T164" id="Seg_2691" s="T163">adv</ta>
            <ta e="T165" id="Seg_2692" s="T164">adv</ta>
            <ta e="T166" id="Seg_2693" s="T165">n.[n:case]</ta>
            <ta e="T167" id="Seg_2694" s="T166">n-n:(poss).[n:case]</ta>
            <ta e="T168" id="Seg_2695" s="T167">v-v:tense.[v:pred.pn]</ta>
            <ta e="T169" id="Seg_2696" s="T168">adv</ta>
            <ta e="T170" id="Seg_2697" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_2698" s="T170">dempro</ta>
            <ta e="T172" id="Seg_2699" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_2700" s="T172">n-n:(ins)-n&gt;adj</ta>
            <ta e="T174" id="Seg_2701" s="T173">n-n:(poss).[n:case]</ta>
            <ta e="T175" id="Seg_2702" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_2703" s="T175">dempro</ta>
            <ta e="T177" id="Seg_2704" s="T176">que-pro:(poss)-pro:case</ta>
            <ta e="T178" id="Seg_2705" s="T177">n-n:poss-n:case</ta>
            <ta e="T179" id="Seg_2706" s="T178">v-v:cvb</ta>
            <ta e="T180" id="Seg_2707" s="T179">v-v:tense.[v:pred.pn]</ta>
            <ta e="T181" id="Seg_2708" s="T180">adv</ta>
            <ta e="T182" id="Seg_2709" s="T181">v-v:cvb</ta>
            <ta e="T183" id="Seg_2710" s="T182">v-v:cvb</ta>
            <ta e="T184" id="Seg_2711" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_2712" s="T184">cardnum</ta>
            <ta e="T186" id="Seg_2713" s="T185">cardnum</ta>
            <ta e="T187" id="Seg_2714" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_2715" s="T187">v-v:tense.[v:pred.pn]</ta>
            <ta e="T189" id="Seg_2716" s="T188">conj</ta>
            <ta e="T190" id="Seg_2717" s="T189">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T191" id="Seg_2718" s="T190">adv</ta>
            <ta e="T192" id="Seg_2719" s="T191">v-v:tense.[v:pred.pn]</ta>
            <ta e="T193" id="Seg_2720" s="T192">adv</ta>
            <ta e="T194" id="Seg_2721" s="T193">dempro-pro:(num).[pro:case]</ta>
            <ta e="T195" id="Seg_2722" s="T194">v-v:cvb</ta>
            <ta e="T196" id="Seg_2723" s="T195">v-v:mood-v:temp.pn</ta>
            <ta e="T197" id="Seg_2724" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_2725" s="T197">n.[n:case]</ta>
            <ta e="T199" id="Seg_2726" s="T198">dempro</ta>
            <ta e="T200" id="Seg_2727" s="T199">n-n:poss-n:case</ta>
            <ta e="T201" id="Seg_2728" s="T200">v-v:cvb</ta>
            <ta e="T202" id="Seg_2729" s="T201">v-v:cvb</ta>
            <ta e="T203" id="Seg_2730" s="T202">n-n:(num)-n:poss-n:case</ta>
            <ta e="T204" id="Seg_2731" s="T203">adj</ta>
            <ta e="T206" id="Seg_2732" s="T205">adj</ta>
            <ta e="T207" id="Seg_2733" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_2734" s="T207">n-n:(num)-n:poss-n:case</ta>
            <ta e="T209" id="Seg_2735" s="T208">v-v:cvb</ta>
            <ta e="T210" id="Seg_2736" s="T209">v-v:tense-v:poss.pn</ta>
            <ta e="T211" id="Seg_2737" s="T210">cardnum</ta>
            <ta e="T212" id="Seg_2738" s="T211">n-n:(poss).[n:case]</ta>
            <ta e="T213" id="Seg_2739" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_2740" s="T213">v-v:tense-v:poss.pn</ta>
            <ta e="T215" id="Seg_2741" s="T214">cardnum</ta>
            <ta e="T216" id="Seg_2742" s="T215">n-n:(poss).[n:case]</ta>
            <ta e="T217" id="Seg_2743" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_2744" s="T217">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T219" id="Seg_2745" s="T218">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T220" id="Seg_2746" s="T219">adv</ta>
            <ta e="T221" id="Seg_2747" s="T220">que-pro:(ins)-pro:case</ta>
            <ta e="T222" id="Seg_2748" s="T221">que</ta>
            <ta e="T223" id="Seg_2749" s="T222">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T224" id="Seg_2750" s="T223">v-v:tense.[v:pred.pn]</ta>
            <ta e="T225" id="Seg_2751" s="T224">dempro</ta>
            <ta e="T226" id="Seg_2752" s="T225">v-v&gt;v-v:cvb</ta>
            <ta e="T227" id="Seg_2753" s="T226">v-v:mood-v:temp.pn</ta>
            <ta e="T228" id="Seg_2754" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_2755" s="T228">adj</ta>
            <ta e="T230" id="Seg_2756" s="T229">cardnum</ta>
            <ta e="T231" id="Seg_2757" s="T230">n.[n:case]</ta>
            <ta e="T232" id="Seg_2758" s="T231">n.[n:case]</ta>
            <ta e="T233" id="Seg_2759" s="T232">n-n:poss-n:case</ta>
            <ta e="T234" id="Seg_2760" s="T233">v-v:cvb</ta>
            <ta e="T235" id="Seg_2761" s="T234">v-v:tense-v:pred.pn</ta>
            <ta e="T236" id="Seg_2762" s="T235">pers.[pro:case]</ta>
            <ta e="T237" id="Seg_2763" s="T236">n-n:poss-n:case</ta>
            <ta e="T238" id="Seg_2764" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_2765" s="T238">dempro-pro:(num)-pro:case</ta>
            <ta e="T240" id="Seg_2766" s="T239">v-v:tense-v:poss.pn</ta>
            <ta e="T241" id="Seg_2767" s="T240">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T242" id="Seg_2768" s="T241">adv&gt;adv-adv</ta>
            <ta e="T243" id="Seg_2769" s="T242">v-v:tense.[v:pred.pn]</ta>
            <ta e="T244" id="Seg_2770" s="T243">n.[n:case]</ta>
            <ta e="T245" id="Seg_2771" s="T244">adj-n:(poss).[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T3" id="Seg_2772" s="T2">interj</ta>
            <ta e="T5" id="Seg_2773" s="T4">n</ta>
            <ta e="T6" id="Seg_2774" s="T5">n</ta>
            <ta e="T7" id="Seg_2775" s="T6">n</ta>
            <ta e="T8" id="Seg_2776" s="T7">v</ta>
            <ta e="T9" id="Seg_2777" s="T8">cardnum</ta>
            <ta e="T10" id="Seg_2778" s="T9">n</ta>
            <ta e="T11" id="Seg_2779" s="T10">n</ta>
            <ta e="T12" id="Seg_2780" s="T11">v</ta>
            <ta e="T13" id="Seg_2781" s="T12">dempro</ta>
            <ta e="T14" id="Seg_2782" s="T13">v</ta>
            <ta e="T15" id="Seg_2783" s="T14">aux</ta>
            <ta e="T16" id="Seg_2784" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_2785" s="T16">n</ta>
            <ta e="T18" id="Seg_2786" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_2787" s="T18">adj</ta>
            <ta e="T20" id="Seg_2788" s="T19">n</ta>
            <ta e="T21" id="Seg_2789" s="T20">v</ta>
            <ta e="T22" id="Seg_2790" s="T21">adv</ta>
            <ta e="T24" id="Seg_2791" s="T23">adv</ta>
            <ta e="T25" id="Seg_2792" s="T24">conj</ta>
            <ta e="T28" id="Seg_2793" s="T27">n</ta>
            <ta e="T29" id="Seg_2794" s="T28">v</ta>
            <ta e="T30" id="Seg_2795" s="T29">adv</ta>
            <ta e="T31" id="Seg_2796" s="T30">conj</ta>
            <ta e="T32" id="Seg_2797" s="T31">n</ta>
            <ta e="T33" id="Seg_2798" s="T32">v</ta>
            <ta e="T34" id="Seg_2799" s="T33">dempro</ta>
            <ta e="T35" id="Seg_2800" s="T34">v</ta>
            <ta e="T36" id="Seg_2801" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2802" s="T36">dempro</ta>
            <ta e="T38" id="Seg_2803" s="T37">n</ta>
            <ta e="T39" id="Seg_2804" s="T38">adj</ta>
            <ta e="T40" id="Seg_2805" s="T39">n</ta>
            <ta e="T41" id="Seg_2806" s="T40">n</ta>
            <ta e="T42" id="Seg_2807" s="T41">v</ta>
            <ta e="T43" id="Seg_2808" s="T42">v</ta>
            <ta e="T44" id="Seg_2809" s="T43">adv</ta>
            <ta e="T45" id="Seg_2810" s="T44">dempro</ta>
            <ta e="T46" id="Seg_2811" s="T45">pers</ta>
            <ta e="T47" id="Seg_2812" s="T46">v</ta>
            <ta e="T48" id="Seg_2813" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_2814" s="T48">n</ta>
            <ta e="T50" id="Seg_2815" s="T49">v</ta>
            <ta e="T51" id="Seg_2816" s="T50">cardnum</ta>
            <ta e="T52" id="Seg_2817" s="T51">n</ta>
            <ta e="T53" id="Seg_2818" s="T52">v</ta>
            <ta e="T54" id="Seg_2819" s="T53">v</ta>
            <ta e="T55" id="Seg_2820" s="T54">dempro</ta>
            <ta e="T56" id="Seg_2821" s="T55">v</ta>
            <ta e="T57" id="Seg_2822" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_2823" s="T57">que</ta>
            <ta e="T59" id="Seg_2824" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_2825" s="T59">adv</ta>
            <ta e="T61" id="Seg_2826" s="T60">ordnum</ta>
            <ta e="T62" id="Seg_2827" s="T61">interj</ta>
            <ta e="T63" id="Seg_2828" s="T62">v</ta>
            <ta e="T64" id="Seg_2829" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_2830" s="T64">adj</ta>
            <ta e="T66" id="Seg_2831" s="T65">n</ta>
            <ta e="T68" id="Seg_2832" s="T67">n</ta>
            <ta e="T69" id="Seg_2833" s="T68">v</ta>
            <ta e="T70" id="Seg_2834" s="T69">aux</ta>
            <ta e="T71" id="Seg_2835" s="T70">n</ta>
            <ta e="T72" id="Seg_2836" s="T71">post</ta>
            <ta e="T73" id="Seg_2837" s="T72">v</ta>
            <ta e="T74" id="Seg_2838" s="T73">aux</ta>
            <ta e="T75" id="Seg_2839" s="T74">v</ta>
            <ta e="T76" id="Seg_2840" s="T75">v</ta>
            <ta e="T78" id="Seg_2841" s="T77">adv</ta>
            <ta e="T79" id="Seg_2842" s="T78">n</ta>
            <ta e="T80" id="Seg_2843" s="T79">n</ta>
            <ta e="T81" id="Seg_2844" s="T80">v</ta>
            <ta e="T82" id="Seg_2845" s="T81">interj</ta>
            <ta e="T83" id="Seg_2846" s="T82">v</ta>
            <ta e="T84" id="Seg_2847" s="T83">dempro</ta>
            <ta e="T85" id="Seg_2848" s="T84">v</ta>
            <ta e="T86" id="Seg_2849" s="T85">v</ta>
            <ta e="T87" id="Seg_2850" s="T86">n</ta>
            <ta e="T88" id="Seg_2851" s="T87">n</ta>
            <ta e="T89" id="Seg_2852" s="T88">v</ta>
            <ta e="T90" id="Seg_2853" s="T89">n</ta>
            <ta e="T91" id="Seg_2854" s="T90">interj</ta>
            <ta e="T92" id="Seg_2855" s="T91">v</ta>
            <ta e="T93" id="Seg_2856" s="T92">aux</ta>
            <ta e="T94" id="Seg_2857" s="T93">n</ta>
            <ta e="T95" id="Seg_2858" s="T94">n</ta>
            <ta e="T96" id="Seg_2859" s="T95">v</ta>
            <ta e="T97" id="Seg_2860" s="T96">aux</ta>
            <ta e="T98" id="Seg_2861" s="T97">dempro</ta>
            <ta e="T99" id="Seg_2862" s="T98">v</ta>
            <ta e="T100" id="Seg_2863" s="T99">aux</ta>
            <ta e="T101" id="Seg_2864" s="T100">cardnum</ta>
            <ta e="T102" id="Seg_2865" s="T101">n</ta>
            <ta e="T103" id="Seg_2866" s="T102">n</ta>
            <ta e="T104" id="Seg_2867" s="T103">cardnum</ta>
            <ta e="T105" id="Seg_2868" s="T104">n</ta>
            <ta e="T106" id="Seg_2869" s="T105">n</ta>
            <ta e="T107" id="Seg_2870" s="T106">v</ta>
            <ta e="T108" id="Seg_2871" s="T107">v</ta>
            <ta e="T109" id="Seg_2872" s="T108">dempro</ta>
            <ta e="T110" id="Seg_2873" s="T109">cardnum</ta>
            <ta e="T111" id="Seg_2874" s="T110">n</ta>
            <ta e="T112" id="Seg_2875" s="T111">n</ta>
            <ta e="T113" id="Seg_2876" s="T112">v</ta>
            <ta e="T114" id="Seg_2877" s="T113">dempro</ta>
            <ta e="T115" id="Seg_2878" s="T114">v</ta>
            <ta e="T116" id="Seg_2879" s="T115">n</ta>
            <ta e="T117" id="Seg_2880" s="T116">v</ta>
            <ta e="T118" id="Seg_2881" s="T117">emphpro</ta>
            <ta e="T119" id="Seg_2882" s="T118">n</ta>
            <ta e="T120" id="Seg_2883" s="T119">v</ta>
            <ta e="T121" id="Seg_2884" s="T120">adv</ta>
            <ta e="T122" id="Seg_2885" s="T121">v</ta>
            <ta e="T123" id="Seg_2886" s="T122">post</ta>
            <ta e="T124" id="Seg_2887" s="T123">adv</ta>
            <ta e="T125" id="Seg_2888" s="T124">pers</ta>
            <ta e="T126" id="Seg_2889" s="T125">n</ta>
            <ta e="T127" id="Seg_2890" s="T126">post</ta>
            <ta e="T128" id="Seg_2891" s="T127">v</ta>
            <ta e="T129" id="Seg_2892" s="T128">aux</ta>
            <ta e="T130" id="Seg_2893" s="T129">dempro</ta>
            <ta e="T131" id="Seg_2894" s="T130">v</ta>
            <ta e="T133" id="Seg_2895" s="T132">aux</ta>
            <ta e="T134" id="Seg_2896" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_2897" s="T134">interj</ta>
            <ta e="T136" id="Seg_2898" s="T135">adj</ta>
            <ta e="T137" id="Seg_2899" s="T136">n</ta>
            <ta e="T138" id="Seg_2900" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_2901" s="T138">dempro</ta>
            <ta e="T140" id="Seg_2902" s="T139">n</ta>
            <ta e="T141" id="Seg_2903" s="T140">n</ta>
            <ta e="T142" id="Seg_2904" s="T141">n</ta>
            <ta e="T143" id="Seg_2905" s="T142">adj</ta>
            <ta e="T144" id="Seg_2906" s="T143">cop</ta>
            <ta e="T145" id="Seg_2907" s="T144">dempro</ta>
            <ta e="T146" id="Seg_2908" s="T145">n</ta>
            <ta e="T147" id="Seg_2909" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_2910" s="T147">n</ta>
            <ta e="T149" id="Seg_2911" s="T148">v</ta>
            <ta e="T150" id="Seg_2912" s="T149">v</ta>
            <ta e="T151" id="Seg_2913" s="T150">v</ta>
            <ta e="T152" id="Seg_2914" s="T151">aux</ta>
            <ta e="T153" id="Seg_2915" s="T152">dempro</ta>
            <ta e="T154" id="Seg_2916" s="T153">n</ta>
            <ta e="T155" id="Seg_2917" s="T154">v</ta>
            <ta e="T156" id="Seg_2918" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_2919" s="T156">dempro</ta>
            <ta e="T158" id="Seg_2920" s="T157">n</ta>
            <ta e="T159" id="Seg_2921" s="T158">n</ta>
            <ta e="T160" id="Seg_2922" s="T159">v</ta>
            <ta e="T161" id="Seg_2923" s="T160">v</ta>
            <ta e="T162" id="Seg_2924" s="T161">v</ta>
            <ta e="T164" id="Seg_2925" s="T163">adv</ta>
            <ta e="T165" id="Seg_2926" s="T164">adv</ta>
            <ta e="T166" id="Seg_2927" s="T165">n</ta>
            <ta e="T167" id="Seg_2928" s="T166">n</ta>
            <ta e="T168" id="Seg_2929" s="T167">v</ta>
            <ta e="T169" id="Seg_2930" s="T168">adv</ta>
            <ta e="T170" id="Seg_2931" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_2932" s="T170">dempro</ta>
            <ta e="T172" id="Seg_2933" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_2934" s="T172">adj</ta>
            <ta e="T174" id="Seg_2935" s="T173">n</ta>
            <ta e="T175" id="Seg_2936" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_2937" s="T175">dempro</ta>
            <ta e="T177" id="Seg_2938" s="T176">que</ta>
            <ta e="T178" id="Seg_2939" s="T177">n</ta>
            <ta e="T179" id="Seg_2940" s="T178">v</ta>
            <ta e="T180" id="Seg_2941" s="T179">v</ta>
            <ta e="T181" id="Seg_2942" s="T180">adv</ta>
            <ta e="T182" id="Seg_2943" s="T181">v</ta>
            <ta e="T183" id="Seg_2944" s="T182">aux</ta>
            <ta e="T184" id="Seg_2945" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_2946" s="T184">cardnum</ta>
            <ta e="T186" id="Seg_2947" s="T185">cardnum</ta>
            <ta e="T187" id="Seg_2948" s="T186">n</ta>
            <ta e="T188" id="Seg_2949" s="T187">v</ta>
            <ta e="T189" id="Seg_2950" s="T188">conj</ta>
            <ta e="T190" id="Seg_2951" s="T189">dempro</ta>
            <ta e="T191" id="Seg_2952" s="T190">adv</ta>
            <ta e="T192" id="Seg_2953" s="T191">v</ta>
            <ta e="T193" id="Seg_2954" s="T192">adv</ta>
            <ta e="T194" id="Seg_2955" s="T193">dempro</ta>
            <ta e="T195" id="Seg_2956" s="T194">v</ta>
            <ta e="T196" id="Seg_2957" s="T195">aux</ta>
            <ta e="T197" id="Seg_2958" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_2959" s="T197">n</ta>
            <ta e="T199" id="Seg_2960" s="T198">dempro</ta>
            <ta e="T200" id="Seg_2961" s="T199">n</ta>
            <ta e="T201" id="Seg_2962" s="T200">v</ta>
            <ta e="T202" id="Seg_2963" s="T201">aux</ta>
            <ta e="T203" id="Seg_2964" s="T202">n</ta>
            <ta e="T204" id="Seg_2965" s="T203">adj</ta>
            <ta e="T206" id="Seg_2966" s="T205">adj</ta>
            <ta e="T207" id="Seg_2967" s="T206">n</ta>
            <ta e="T208" id="Seg_2968" s="T207">n</ta>
            <ta e="T209" id="Seg_2969" s="T208">v</ta>
            <ta e="T210" id="Seg_2970" s="T209">v</ta>
            <ta e="T211" id="Seg_2971" s="T210">cardnum</ta>
            <ta e="T212" id="Seg_2972" s="T211">n</ta>
            <ta e="T213" id="Seg_2973" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_2974" s="T213">v</ta>
            <ta e="T215" id="Seg_2975" s="T214">cardnum</ta>
            <ta e="T216" id="Seg_2976" s="T215">n</ta>
            <ta e="T217" id="Seg_2977" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_2978" s="T217">dempro</ta>
            <ta e="T219" id="Seg_2979" s="T218">v</ta>
            <ta e="T220" id="Seg_2980" s="T219">adv</ta>
            <ta e="T221" id="Seg_2981" s="T220">que</ta>
            <ta e="T222" id="Seg_2982" s="T221">que</ta>
            <ta e="T223" id="Seg_2983" s="T222">v</ta>
            <ta e="T224" id="Seg_2984" s="T223">v</ta>
            <ta e="T225" id="Seg_2985" s="T224">dempro</ta>
            <ta e="T226" id="Seg_2986" s="T225">v</ta>
            <ta e="T227" id="Seg_2987" s="T226">aux</ta>
            <ta e="T228" id="Seg_2988" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_2989" s="T228">adj</ta>
            <ta e="T230" id="Seg_2990" s="T229">cardnum</ta>
            <ta e="T231" id="Seg_2991" s="T230">n</ta>
            <ta e="T232" id="Seg_2992" s="T231">n</ta>
            <ta e="T233" id="Seg_2993" s="T232">n</ta>
            <ta e="T234" id="Seg_2994" s="T233">v</ta>
            <ta e="T235" id="Seg_2995" s="T234">v</ta>
            <ta e="T236" id="Seg_2996" s="T235">pers</ta>
            <ta e="T237" id="Seg_2997" s="T236">n</ta>
            <ta e="T238" id="Seg_2998" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_2999" s="T238">dempro</ta>
            <ta e="T240" id="Seg_3000" s="T239">v</ta>
            <ta e="T241" id="Seg_3001" s="T240">v</ta>
            <ta e="T242" id="Seg_3002" s="T241">adv</ta>
            <ta e="T243" id="Seg_3003" s="T242">v</ta>
            <ta e="T244" id="Seg_3004" s="T243">n</ta>
            <ta e="T245" id="Seg_3005" s="T244">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_3006" s="T4">RUS:cult</ta>
            <ta e="T6" id="Seg_3007" s="T5">RUS:cult</ta>
            <ta e="T7" id="Seg_3008" s="T6">RUS:cult</ta>
            <ta e="T10" id="Seg_3009" s="T9">RUS:cult</ta>
            <ta e="T11" id="Seg_3010" s="T10">RUS:cult</ta>
            <ta e="T19" id="Seg_3011" s="T18">RUS:cult</ta>
            <ta e="T24" id="Seg_3012" s="T23">RUS:mod</ta>
            <ta e="T25" id="Seg_3013" s="T24">RUS:gram</ta>
            <ta e="T30" id="Seg_3014" s="T29">RUS:mod</ta>
            <ta e="T31" id="Seg_3015" s="T30">RUS:gram</ta>
            <ta e="T32" id="Seg_3016" s="T31">RUS:cult</ta>
            <ta e="T39" id="Seg_3017" s="T38">RUS:cult</ta>
            <ta e="T49" id="Seg_3018" s="T48">RUS:cult</ta>
            <ta e="T52" id="Seg_3019" s="T51">RUS:cult</ta>
            <ta e="T66" id="Seg_3020" s="T65">RUS:cult</ta>
            <ta e="T68" id="Seg_3021" s="T67">RUS:cult</ta>
            <ta e="T90" id="Seg_3022" s="T89">RUS:cult</ta>
            <ta e="T94" id="Seg_3023" s="T93">RUS:cult</ta>
            <ta e="T116" id="Seg_3024" s="T115">RUS:cult</ta>
            <ta e="T124" id="Seg_3025" s="T123">RUS:mod</ta>
            <ta e="T142" id="Seg_3026" s="T141">RUS:cult</ta>
            <ta e="T143" id="Seg_3027" s="T142">RUS:cult</ta>
            <ta e="T146" id="Seg_3028" s="T145">RUS:cult</ta>
            <ta e="T154" id="Seg_3029" s="T153">RUS:cult</ta>
            <ta e="T178" id="Seg_3030" s="T177">RUS:cult</ta>
            <ta e="T187" id="Seg_3031" s="T186">RUS:cult</ta>
            <ta e="T189" id="Seg_3032" s="T188">RUS:gram</ta>
            <ta e="T191" id="Seg_3033" s="T190">RUS:mod</ta>
            <ta e="T198" id="Seg_3034" s="T197">RUS:cult</ta>
            <ta e="T203" id="Seg_3035" s="T202">RUS:cult</ta>
            <ta e="T207" id="Seg_3036" s="T206">RUS:cult</ta>
            <ta e="T208" id="Seg_3037" s="T207">RUS:cult</ta>
            <ta e="T212" id="Seg_3038" s="T211">RUS:cult</ta>
            <ta e="T216" id="Seg_3039" s="T215">RUS:cult</ta>
            <ta e="T219" id="Seg_3040" s="T218">RUS:core</ta>
            <ta e="T226" id="Seg_3041" s="T225">RUS:core</ta>
            <ta e="T244" id="Seg_3042" s="T243">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T39" id="Seg_3043" s="T38">fortition Vsub Vsub Vsub</ta>
            <ta e="T68" id="Seg_3044" s="T67">fortition Vsub Vsub Vsub</ta>
            <ta e="T90" id="Seg_3045" s="T89">fortition Vsub Vsub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T5" id="Seg_3046" s="T4">dir:bare</ta>
            <ta e="T6" id="Seg_3047" s="T5">dir:bare</ta>
            <ta e="T7" id="Seg_3048" s="T6">dir:infl</ta>
            <ta e="T10" id="Seg_3049" s="T9">dir:bare</ta>
            <ta e="T11" id="Seg_3050" s="T10">dir:infl</ta>
            <ta e="T19" id="Seg_3051" s="T18">dir:infl</ta>
            <ta e="T24" id="Seg_3052" s="T23">dir:bare</ta>
            <ta e="T25" id="Seg_3053" s="T24">dir:bare</ta>
            <ta e="T30" id="Seg_3054" s="T29">dir:bare</ta>
            <ta e="T31" id="Seg_3055" s="T30">dir:bare</ta>
            <ta e="T32" id="Seg_3056" s="T31">dir:bare</ta>
            <ta e="T39" id="Seg_3057" s="T38">dir:infl</ta>
            <ta e="T49" id="Seg_3058" s="T48">dir:infl</ta>
            <ta e="T52" id="Seg_3059" s="T51">dir:infl</ta>
            <ta e="T66" id="Seg_3060" s="T65">dir:infl</ta>
            <ta e="T68" id="Seg_3061" s="T67">dir:infl</ta>
            <ta e="T90" id="Seg_3062" s="T89">dir:infl</ta>
            <ta e="T94" id="Seg_3063" s="T93">dir:infl</ta>
            <ta e="T116" id="Seg_3064" s="T115">dir:infl</ta>
            <ta e="T124" id="Seg_3065" s="T123">dir:bare</ta>
            <ta e="T142" id="Seg_3066" s="T141">dir:infl</ta>
            <ta e="T143" id="Seg_3067" s="T142">dir:infl</ta>
            <ta e="T146" id="Seg_3068" s="T145">dir:infl</ta>
            <ta e="T154" id="Seg_3069" s="T153">dir:infl</ta>
            <ta e="T178" id="Seg_3070" s="T177">dir:infl</ta>
            <ta e="T187" id="Seg_3071" s="T186">dir:infl</ta>
            <ta e="T189" id="Seg_3072" s="T188">dir:bare</ta>
            <ta e="T191" id="Seg_3073" s="T190">dir:bare</ta>
            <ta e="T198" id="Seg_3074" s="T197">dir:bare</ta>
            <ta e="T203" id="Seg_3075" s="T202">dir:infl</ta>
            <ta e="T207" id="Seg_3076" s="T206">dir:infl</ta>
            <ta e="T208" id="Seg_3077" s="T207">dir:infl</ta>
            <ta e="T212" id="Seg_3078" s="T211">dir:infl</ta>
            <ta e="T216" id="Seg_3079" s="T215">dir:infl</ta>
            <ta e="T219" id="Seg_3080" s="T218">indir:infl</ta>
            <ta e="T226" id="Seg_3081" s="T225">indir:infl</ta>
            <ta e="T244" id="Seg_3082" s="T243">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_3083" s="T1">A gardener is collecting pears.</ta>
            <ta e="T18" id="Seg_3084" s="T8">He collected one basket of pears, when he was collecting past him…</ta>
            <ta e="T21" id="Seg_3085" s="T18">A person with a goat passed by.</ta>
            <ta e="T33" id="Seg_3086" s="T21">Then he climbed onto a tree to gather even more pears.</ta>
            <ta e="T43" id="Seg_3087" s="T33">When he was there, a boy on a bicycle came from there, he rides.</ta>
            <ta e="T66" id="Seg_3088" s="T43">Then, however, when he came to him, he saw the pears and wanted to take a pear, he is careful or so, the second (?), he took the whole basket.</ta>
            <ta e="T76" id="Seg_3089" s="T67">He comes to the bicycle, he rides along the path, he is ridng and riding.</ta>
            <ta e="T81" id="Seg_3090" s="T77">On the opposite side(?) he met a girl.</ta>
            <ta e="T97" id="Seg_3091" s="T81">Eh, he saw it, when he saw it, he stumbled over a stone and fell down from the bicycle, the pears scattered on the earth.</ta>
            <ta e="T109" id="Seg_3092" s="T97">When he was shaking off, three boys were standing there, seeing it.</ta>
            <ta e="T123" id="Seg_3093" s="T109">The three boys came to him, collected the pears and shook off the sand from him.</ta>
            <ta e="T134" id="Seg_3094" s="T123">They went further along his way, when going so…</ta>
            <ta e="T144" id="Seg_3095" s="T134">The back child this boy's hat, he had a hat.</ta>
            <ta e="T159" id="Seg_3096" s="T144">The hat, saying "I see a girl" he dropped it, [they] found the hat for him.</ta>
            <ta e="T168" id="Seg_3097" s="T159">They brought, they whistled, then the boy stopped.</ta>
            <ta e="T184" id="Seg_3098" s="T168">Then the back boy went, though, to give [him] the whatchamacallit, the hat, giving it…</ta>
            <ta e="T204" id="Seg_3099" s="T184">He got three pears, and that one went on, when these were going, when the gardener came down from the step [= the ladder?], [to put] the next pears…</ta>
            <ta e="T217" id="Seg_3100" s="T204">To put the pears into an empty basket, he saw, one basket is missing, he counted, one basket is not there.</ta>
            <ta e="T235" id="Seg_3101" s="T217">Then he is thinking, "where could the pears be", thinking so, the three boys passed by him. </ta>
            <ta e="T245" id="Seg_3102" s="T235">Past him, and he saw them, seeing them he stayed so, the gardener, that's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_3103" s="T1">Ein Gärtner, ein Gärtner sammelt Birnen.</ta>
            <ta e="T18" id="Seg_3104" s="T8">Er sammelte einen Korb Birnen, als er sammelte, an ihm vorbei…</ta>
            <ta e="T21" id="Seg_3105" s="T18">Ein Mensch mit einer Ziege ging vorbei.</ta>
            <ta e="T33" id="Seg_3106" s="T21">Dann kletterte er auf einen Baum, um noch mehr Birnen zu sammeln.</ta>
            <ta e="T43" id="Seg_3107" s="T33">Als er dort war, kam von dort ein Junge auf einem Fahrrad, er fährt.</ta>
            <ta e="T66" id="Seg_3108" s="T43">Dann aber, als er zu ihm kam, sah er die Birnen und wollte eine Birne nehmen, er ist vorsichtig oder so, der zweite (?), er nahm den ganzen Korb.</ta>
            <ta e="T76" id="Seg_3109" s="T67">Er kommt zum Fahrrad und fährt den Weg entlang, er fährt und fährt.</ta>
            <ta e="T81" id="Seg_3110" s="T77">Gegenüber(?) traf er ein Mädchen.</ta>
            <ta e="T97" id="Seg_3111" s="T81">Äh, er sah es, als er es sah, stolperte er über einen Stein und krachte vom Fahrrad, die Birnen rollten auf die Erde.</ta>
            <ta e="T109" id="Seg_3112" s="T97">Als er sich schüttelte, standen drei Jungen da und sahen das.</ta>
            <ta e="T123" id="Seg_3113" s="T109">Die drei Jungen kamen zu ihm, sammelten die Birnen ein und klopften ihm den Sand ab.</ta>
            <ta e="T134" id="Seg_3114" s="T123">Sie gingen weiter entlang seines Weges, als sie gingen…</ta>
            <ta e="T144" id="Seg_3115" s="T134">Das hintere Kind den Hut dieses Jungen, er hatte einen Hut.</ta>
            <ta e="T159" id="Seg_3116" s="T144">Den Hut, "ich sehe ein Mädchen", sagte er und ließ ihn fallen, [sie] fanden den Hut für diesen Jungen.</ta>
            <ta e="T168" id="Seg_3117" s="T159">Sie brachten, sie pfiffen, dann blieb der Junge stehen.</ta>
            <ta e="T184" id="Seg_3118" s="T168">Dann aber ging der hintere Junge, um [ihm] das Dings, den Hut zu geben, als er ihn gab…</ta>
            <ta e="T204" id="Seg_3119" s="T184">Er bekam drei Birnen, und jener ging weiter, als diese gingen, als der Gärtner von der Stufe [= der Leiter?] stieg, [um] die nächsten Birnen…</ta>
            <ta e="T217" id="Seg_3120" s="T204">Um die Birnen in einen leeren Korb zu legen, sah er, dass ein Korb fehlt, er zählte, ein Korb ist nicht da.</ta>
            <ta e="T235" id="Seg_3121" s="T217">Dann denkt er, "wo können die Birnen hin sein", als er dachte, gingen die drei Jungen von eben an ihm vorbei.</ta>
            <ta e="T245" id="Seg_3122" s="T235">An ihm vorbei, und er sah sie, und als er sie sah, blieb er so, der Gärtner, Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T8" id="Seg_3123" s="T1">Садовник груши собрает</ta>
            <ta e="T18" id="Seg_3124" s="T8">Одну корзину груш собрал вон собирал лежал …. рядом будет</ta>
            <ta e="T21" id="Seg_3125" s="T18">с козой человек прошел</ta>
            <ta e="T33" id="Seg_3126" s="T21">потом ещё поднялся (скарабкался) на дерево поднялся (скарабкался) ещё груш собрал</ta>
            <ta e="T43" id="Seg_3127" s="T33">там лежал оттуда на велосипеде мальчик приехал, едет</ta>
            <ta e="T66" id="Seg_3128" s="T43">потом к нему приехал груши увидел одну грушу забрать хотел тот боиться что ли затем взял целую корзину</ta>
            <ta e="T76" id="Seg_3129" s="T67">на велосипед загрузил по дороге едет, едет едет </ta>
            <ta e="T81" id="Seg_3130" s="T77">на встречу (на против)девочку встретил (увидел)</ta>
            <ta e="T97" id="Seg_3131" s="T81">увидел туда смотрел-смотрел в итоге об камень стукнулся на велосипеде свалился упал груши на землю покатились упали </ta>
            <ta e="T109" id="Seg_3132" s="T97">когда он встряхивался трое мальчиков смотрели стояли на это</ta>
            <ta e="T123" id="Seg_3133" s="T109">трое мальчиков туда пришли груши собрали с него песок встряхнули после этого</ta>
            <ta e="T134" id="Seg_3134" s="T123">дальше по его дороге шли когда шли</ta>
            <ta e="T144" id="Seg_3135" s="T134">поелдний ребенок этот мальчик в шляпе был</ta>
            <ta e="T159" id="Seg_3136" s="T144">ту шляпу девушку смотрел уронил эту шляпу нашли этому мальчику</ta>
            <ta e="T168" id="Seg_3137" s="T159">отнесли, свистели даже потом мальчик остановился</ta>
            <ta e="T184" id="Seg_3138" s="T168">потом тот последний ребенок это шляпу отдать пошел, как отдал</ta>
            <ta e="T204" id="Seg_3139" s="T184">три три груши забрал, и этот дальше пошел, потом когда эти шли садовник спустился с опоры</ta>
            <ta e="T217" id="Seg_3140" s="T204">в пустую корзину грушы положить (засунуть) увидел одной корзины нет посчитал одной корзины нет</ta>
            <ta e="T235" id="Seg_3141" s="T217">потом думает иногда куда делись говорит вот думал стоял те три парня рядом прошли</ta>
            <ta e="T245" id="Seg_3142" s="T235">с ним рядом на них смотрел смотрел так и остался садовник, все</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
