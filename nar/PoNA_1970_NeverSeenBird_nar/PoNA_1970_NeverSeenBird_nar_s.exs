<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID9F35EECF-112C-683F-BDE2-0EE2400EB844">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_1970_NeverSeenBird_nar</transcription-name>
         <referenced-file url="PoNA_1970_NeverSeenBird_nar.wav" />
         <referenced-file url="PoNA_1970_NeverSeenBird_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\PoNA_1970_NeverSeenBird_nar\PoNA_1970_NeverSeenBird_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">710</ud-information>
            <ud-information attribute-name="# HIAT:w">515</ud-information>
            <ud-information attribute-name="# e">516</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">71</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.628" type="appl" />
         <tli id="T2" time="1.255" type="appl" />
         <tli id="T3" time="1.883" type="appl" />
         <tli id="T4" time="2.511" type="appl" />
         <tli id="T5" time="3.139" type="appl" />
         <tli id="T6" time="3.766" type="appl" />
         <tli id="T7" time="4.394" type="appl" />
         <tli id="T8" time="5.022" type="appl" />
         <tli id="T9" time="5.649" type="appl" />
         <tli id="T10" time="6.277" type="appl" />
         <tli id="T11" time="6.905" type="appl" />
         <tli id="T12" time="7.533" type="appl" />
         <tli id="T13" time="8.16" type="appl" />
         <tli id="T14" time="9.519954153597912" />
         <tli id="T15" time="9.582" type="appl" />
         <tli id="T16" time="10.377" type="appl" />
         <tli id="T17" time="11.171" type="appl" />
         <tli id="T18" time="11.966" type="appl" />
         <tli id="T19" time="12.446668574688003" />
         <tli id="T20" time="13.31" type="appl" />
         <tli id="T21" time="13.86" type="appl" />
         <tli id="T22" time="14.411" type="appl" />
         <tli id="T23" time="15.339926125650416" />
         <tli id="T24" time="15.612" type="appl" />
         <tli id="T25" time="16.262" type="appl" />
         <tli id="T26" time="16.912" type="appl" />
         <tli id="T27" time="17.563" type="appl" />
         <tli id="T28" time="18.214" type="appl" />
         <tli id="T29" time="18.864" type="appl" />
         <tli id="T30" time="19.514" type="appl" />
         <tli id="T31" time="20.165" type="appl" />
         <tli id="T32" time="20.816" type="appl" />
         <tli id="T33" time="21.466" type="appl" />
         <tli id="T34" time="22.117" type="appl" />
         <tli id="T35" time="22.767" type="appl" />
         <tli id="T36" time="23.418" type="appl" />
         <tli id="T37" time="24.068" type="appl" />
         <tli id="T38" time="24.718" type="appl" />
         <tli id="T39" time="25.369" type="appl" />
         <tli id="T40" time="26.02" type="appl" />
         <tli id="T41" time="27.37320150887466" />
         <tli id="T42" time="27.434" type="appl" />
         <tli id="T43" time="28.198" type="appl" />
         <tli id="T44" time="28.961" type="appl" />
         <tli id="T45" time="29.725" type="appl" />
         <tli id="T46" time="30.489" type="appl" />
         <tli id="T47" time="31.253" type="appl" />
         <tli id="T48" time="32.016" type="appl" />
         <tli id="T49" time="32.78" type="appl" />
         <tli id="T50" time="34.04650270477909" />
         <tli id="T51" time="34.444" type="appl" />
         <tli id="T52" time="35.344" type="appl" />
         <tli id="T53" time="36.244" type="appl" />
         <tli id="T54" time="37.144" type="appl" />
         <tli id="T55" time="38.43314824614283" />
         <tli id="T56" time="38.664" type="appl" />
         <tli id="T57" time="39.283" type="appl" />
         <tli id="T58" time="39.903" type="appl" />
         <tli id="T59" time="40.523" type="appl" />
         <tli id="T60" time="41.142" type="appl" />
         <tli id="T61" time="42.15313033137227" />
         <tli id="T62" time="42.502" type="appl" />
         <tli id="T63" time="43.241" type="appl" />
         <tli id="T64" time="43.981" type="appl" />
         <tli id="T65" time="44.721" type="appl" />
         <tli id="T66" time="45.461" type="appl" />
         <tli id="T67" time="46.2" type="appl" />
         <tli id="T68" time="46.94" type="appl" />
         <tli id="T69" time="47.68" type="appl" />
         <tli id="T70" time="48.42" type="appl" />
         <tli id="T71" time="49.159" type="appl" />
         <tli id="T72" time="50.11975863217724" />
         <tli id="T73" time="50.589" type="appl" />
         <tli id="T74" time="51.28" type="appl" />
         <tli id="T75" time="51.97" type="appl" />
         <tli id="T76" time="52.386668288061124" />
         <tli id="T77" time="53.338" type="appl" />
         <tli id="T78" time="54.016" type="appl" />
         <tli id="T79" time="54.693" type="appl" />
         <tli id="T80" time="55.371" type="appl" />
         <tli id="T81" time="56.049" type="appl" />
         <tli id="T82" time="56.727" type="appl" />
         <tli id="T83" time="57.405" type="appl" />
         <tli id="T84" time="58.082" type="appl" />
         <tli id="T85" time="58.76" type="appl" />
         <tli id="T86" time="59.58466747794687" />
         <tli id="T87" time="60.23" type="appl" />
         <tli id="T88" time="61.021" type="appl" />
         <tli id="T89" time="61.813" type="appl" />
         <tli id="T90" time="62.605" type="appl" />
         <tli id="T91" time="63.396" type="appl" />
         <tli id="T92" time="64.53302255380098" />
         <tli id="T93" time="64.953" type="appl" />
         <tli id="T94" time="65.718" type="appl" />
         <tli id="T95" time="66.484" type="appl" />
         <tli id="T96" time="67.249" type="appl" />
         <tli id="T97" time="68.014" type="appl" />
         <tli id="T98" time="68.78" type="appl" />
         <tli id="T99" time="69.545" type="appl" />
         <tli id="T100" time="70.24333359562763" />
         <tli id="T101" time="70.873" type="appl" />
         <tli id="T102" time="71.436" type="appl" />
         <tli id="T103" time="72.0" type="appl" />
         <tli id="T104" time="72.563" type="appl" />
         <tli id="T105" time="73.39266738733086" />
         <tli id="T106" time="74.105" type="appl" />
         <tli id="T107" time="75.084" type="appl" />
         <tli id="T108" time="76.063" type="appl" />
         <tli id="T109" time="77.043" type="appl" />
         <tli id="T110" time="78.022" type="appl" />
         <tli id="T111" time="79.001" type="appl" />
         <tli id="T112" time="80.53294550102436" />
         <tli id="T113" time="80.691" type="appl" />
         <tli id="T114" time="81.402" type="appl" />
         <tli id="T115" time="82.113" type="appl" />
         <tli id="T116" time="82.824" type="appl" />
         <tli id="T117" time="83.534" type="appl" />
         <tli id="T118" time="84.245" type="appl" />
         <tli id="T119" time="84.956" type="appl" />
         <tli id="T120" time="85.85366857528722" />
         <tli id="T121" time="86.495" type="appl" />
         <tli id="T122" time="87.322" type="appl" />
         <tli id="T123" time="88.15" type="appl" />
         <tli id="T124" time="88.837" type="appl" />
         <tli id="T125" time="89.523" type="appl" />
         <tli id="T126" time="90.15000205157776" />
         <tli id="T127" time="90.963" type="appl" />
         <tli id="T128" time="91.717" type="appl" />
         <tli id="T129" time="92.47" type="appl" />
         <tli id="T130" time="93.224" type="appl" />
         <tli id="T131" time="93.977" type="appl" />
         <tli id="T132" time="94.538" type="appl" />
         <tli id="T133" time="95.099" type="appl" />
         <tli id="T134" time="95.66" type="appl" />
         <tli id="T135" time="96.222" type="appl" />
         <tli id="T136" time="96.783" type="appl" />
         <tli id="T137" time="97.93952834069111" />
         <tli id="T138" time="98.038" type="appl" />
         <tli id="T139" time="98.732" type="appl" />
         <tli id="T140" time="99.426" type="appl" />
         <tli id="T141" time="100.12" type="appl" />
         <tli id="T142" time="100.814" type="appl" />
         <tli id="T143" time="101.508" type="appl" />
         <tli id="T144" time="102.202" type="appl" />
         <tli id="T145" time="102.896" type="appl" />
         <tli id="T146" time="103.59" type="appl" />
         <tli id="T147" time="104.285" type="appl" />
         <tli id="T148" time="104.979" type="appl" />
         <tli id="T149" time="105.673" type="appl" />
         <tli id="T150" time="106.367" type="appl" />
         <tli id="T151" time="107.061" type="appl" />
         <tli id="T152" time="107.755" type="appl" />
         <tli id="T153" time="108.449" type="appl" />
         <tli id="T154" time="109.143" type="appl" />
         <tli id="T155" time="109.837" type="appl" />
         <tli id="T156" time="110.568" type="appl" />
         <tli id="T520" time="110.96672727272727" type="intp" />
         <tli id="T157" time="111.299" type="appl" />
         <tli id="T158" time="112.03" type="appl" />
         <tli id="T159" time="112.762" type="appl" />
         <tli id="T160" time="113.493" type="appl" />
         <tli id="T161" time="114.224" type="appl" />
         <tli id="T162" time="114.955" type="appl" />
         <tli id="T163" time="115.686" type="appl" />
         <tli id="T164" time="116.417" type="appl" />
         <tli id="T165" time="117.148" type="appl" />
         <tli id="T166" time="117.879" type="appl" />
         <tli id="T167" time="118.611" type="appl" />
         <tli id="T168" time="119.342" type="appl" />
         <tli id="T170" time="120.804" type="appl" />
         <tli id="T171" time="121.393" type="appl" />
         <tli id="T172" time="121.982" type="appl" />
         <tli id="T173" time="122.571" type="appl" />
         <tli id="T174" time="123.16" type="appl" />
         <tli id="T175" time="123.749" type="appl" />
         <tli id="T176" time="124.338" type="appl" />
         <tli id="T177" time="124.927" type="appl" />
         <tli id="T178" time="125.516" type="appl" />
         <tli id="T179" time="126.49272416692352" />
         <tli id="T180" time="127.1" type="appl" />
         <tli id="T181" time="128.095" type="appl" />
         <tli id="T182" time="129.09" type="appl" />
         <tli id="T183" time="130.085" type="appl" />
         <tli id="T184" time="131.08" type="appl" />
         <tli id="T185" time="132.082" type="appl" />
         <tli id="T186" time="133.083" type="appl" />
         <tli id="T187" time="134.085" type="appl" />
         <tli id="T188" time="135.086" type="appl" />
         <tli id="T189" time="135.96800405649805" />
         <tli id="T190" time="136.73" type="appl" />
         <tli id="T191" time="137.372" type="appl" />
         <tli id="T192" time="138.013" type="appl" />
         <tli id="T193" time="138.655" type="appl" />
         <tli id="T194" time="139.6369968039497" />
         <tli id="T195" time="140.024" type="appl" />
         <tli id="T196" time="140.752" type="appl" />
         <tli id="T197" time="141.479" type="appl" />
         <tli id="T198" time="142.207" type="appl" />
         <tli id="T199" time="143.053998577445" />
         <tli id="T200" time="143.438" type="appl" />
         <tli id="T201" time="143.942" type="appl" />
         <tli id="T202" time="144.446" type="appl" />
         <tli id="T203" time="145.03000468635184" />
         <tli id="T204" time="145.47" type="appl" />
         <tli id="T205" time="145.99" type="appl" />
         <tli id="T206" time="146.51" type="appl" />
         <tli id="T207" time="147.03" type="appl" />
         <tli id="T208" time="147.55" type="appl" />
         <tli id="T209" time="148.07" type="appl" />
         <tli id="T210" time="148.59" type="appl" />
         <tli id="T211" time="149.11" type="appl" />
         <tli id="T212" time="150.02594416846458" />
         <tli id="T213" time="150.34" type="appl" />
         <tli id="T214" time="151.05" type="appl" />
         <tli id="T215" time="151.76" type="appl" />
         <tli id="T216" time="152.47" type="appl" />
         <tli id="T217" time="153.6192601970285" />
         <tli id="T218" time="153.835" type="appl" />
         <tli id="T219" time="154.49" type="appl" />
         <tli id="T220" time="155.145" type="appl" />
         <tli id="T221" time="155.8" type="appl" />
         <tli id="T222" time="156.454" type="appl" />
         <tli id="T223" time="157.109" type="appl" />
         <tli id="T224" time="157.764" type="appl" />
         <tli id="T225" time="158.69923573277188" />
         <tli id="T226" time="159.008" type="appl" />
         <tli id="T227" time="159.598" type="appl" />
         <tli id="T228" time="160.187" type="appl" />
         <tli id="T229" time="160.776" type="appl" />
         <tli id="T230" time="161.366" type="appl" />
         <tli id="T231" time="161.955" type="appl" />
         <tli id="T232" time="162.544" type="appl" />
         <tli id="T233" time="163.134" type="appl" />
         <tli id="T234" time="163.96299944592425" />
         <tli id="T235" time="164.443" type="appl" />
         <tli id="T236" time="165.163" type="appl" />
         <tli id="T237" time="165.883" type="appl" />
         <tli id="T238" time="166.603" type="appl" />
         <tli id="T239" time="167.323" type="appl" />
         <tli id="T240" time="168.043" type="appl" />
         <tli id="T241" time="168.763" type="appl" />
         <tli id="T242" time="169.8258488156535" />
         <tli id="T243" time="170.334" type="appl" />
         <tli id="T244" time="171.185" type="appl" />
         <tli id="T245" time="172.036" type="appl" />
         <tli id="T246" time="172.888" type="appl" />
         <tli id="T247" time="173.739" type="appl" />
         <tli id="T248" time="174.59" type="appl" />
         <tli id="T249" time="175.54" type="appl" />
         <tli id="T250" time="176.67099814230127" />
         <tli id="T251" time="177.326" type="appl" />
         <tli id="T252" time="178.16" type="appl" />
         <tli id="T253" time="178.995" type="appl" />
         <tli id="T254" time="179.696673675354" />
         <tli id="T255" time="180.533" type="appl" />
         <tli id="T256" time="181.236" type="appl" />
         <tli id="T257" time="181.939" type="appl" />
         <tli id="T258" time="182.642" type="appl" />
         <tli id="T259" time="183.345" type="appl" />
         <tli id="T260" time="184.048" type="appl" />
         <tli id="T261" time="184.752" type="appl" />
         <tli id="T262" time="185.455" type="appl" />
         <tli id="T263" time="186.158" type="appl" />
         <tli id="T264" time="186.861" type="appl" />
         <tli id="T265" time="187.564" type="appl" />
         <tli id="T266" time="188.267" type="appl" />
         <tli id="T267" time="188.97" type="appl" />
         <tli id="T268" time="190.21241730424063" />
         <tli id="T269" time="190.279" type="appl" />
         <tli id="T270" time="190.885" type="appl" />
         <tli id="T271" time="191.492" type="appl" />
         <tli id="T272" time="192.098" type="appl" />
         <tli id="T273" time="192.704" type="appl" />
         <tli id="T274" time="193.31" type="appl" />
         <tli id="T275" time="193.916" type="appl" />
         <tli id="T276" time="194.522" type="appl" />
         <tli id="T277" time="195.128" type="appl" />
         <tli id="T278" time="195.735" type="appl" />
         <tli id="T279" time="196.341" type="appl" />
         <tli id="T280" time="197.47238234154324" />
         <tli id="T281" time="197.643" type="appl" />
         <tli id="T282" time="198.339" type="appl" />
         <tli id="T283" time="199.035" type="appl" />
         <tli id="T284" time="199.731" type="appl" />
         <tli id="T285" time="200.427" type="appl" />
         <tli id="T286" time="201.123" type="appl" />
         <tli id="T287" time="201.818" type="appl" />
         <tli id="T288" time="202.514" type="appl" />
         <tli id="T289" time="203.21" type="appl" />
         <tli id="T290" time="203.906" type="appl" />
         <tli id="T291" time="204.602" type="appl" />
         <tli id="T292" time="205.298" type="appl" />
         <tli id="T293" time="206.44732609898074" />
         <tli id="T294" time="206.92" type="appl" />
         <tli id="T295" time="207.846" type="appl" />
         <tli id="T296" time="208.772" type="appl" />
         <tli id="T297" time="209.697" type="appl" />
         <tli id="T298" time="210.623" type="appl" />
         <tli id="T299" time="211.549" type="appl" />
         <tli id="T300" time="212.92564125473646" />
         <tli id="T301" time="213.174" type="appl" />
         <tli id="T302" time="213.873" type="appl" />
         <tli id="T303" time="214.572" type="appl" />
         <tli id="T304" time="215.27" type="appl" />
         <tli id="T305" time="215.969" type="appl" />
         <tli id="T306" time="216.668" type="appl" />
         <tli id="T307" time="217.367" type="appl" />
         <tli id="T308" time="218.0993272766869" />
         <tli id="T309" time="218.847" type="appl" />
         <tli id="T310" time="219.627" type="appl" />
         <tli id="T311" time="220.408" type="appl" />
         <tli id="T312" time="221.189" type="appl" />
         <tli id="T313" time="221.969" type="appl" />
         <tli id="T314" time="222.75" type="appl" />
         <tli id="T315" time="223.53" type="appl" />
         <tli id="T316" time="224.311" type="appl" />
         <tli id="T317" time="225.092" type="appl" />
         <tli id="T318" time="225.872" type="appl" />
         <tli id="T319" time="227.37223834916693" />
         <tli id="T320" time="227.651" type="appl" />
         <tli id="T321" time="228.649" type="appl" />
         <tli id="T322" time="229.647" type="appl" />
         <tli id="T323" time="230.645" type="appl" />
         <tli id="T324" time="231.643" type="appl" />
         <tli id="T325" time="232.641" type="appl" />
         <tli id="T326" time="233.91220685384445" />
         <tli id="T327" time="234.202" type="appl" />
         <tli id="T328" time="234.765" type="appl" />
         <tli id="T330" time="235.89" type="appl" />
         <tli id="T331" time="236.453" type="appl" />
         <tli id="T332" time="236.96266091340155" />
         <tli id="T333" time="237.464" type="appl" />
         <tli id="T334" time="237.911" type="appl" />
         <tli id="T335" time="238.359" type="appl" />
         <tli id="T336" time="238.807" type="appl" />
         <tli id="T337" time="239.255" type="appl" />
         <tli id="T338" time="239.702" type="appl" />
         <tli id="T339" time="240.15" type="appl" />
         <tli id="T340" time="240.89" type="appl" />
         <tli id="T341" time="241.63" type="appl" />
         <tli id="T342" time="242.37" type="appl" />
         <tli id="T343" time="243.03666812039893" />
         <tli id="T344" time="243.64" type="appl" />
         <tli id="T345" time="244.17" type="appl" />
         <tli id="T346" time="244.7" type="appl" />
         <tli id="T347" time="245.04999175792454" />
         <tli id="T348" time="246.01" type="appl" />
         <tli id="T349" time="246.79" type="appl" />
         <tli id="T350" time="248.0454721238918" />
         <tli id="T351" time="248.237" type="appl" />
         <tli id="T352" time="248.904" type="appl" />
         <tli id="T353" time="249.57" type="appl" />
         <tli id="T355" time="250.904" type="appl" />
         <tli id="T356" time="251.57" type="appl" />
         <tli id="T357" time="252.237" type="appl" />
         <tli id="T358" time="253.37877977296625" />
         <tli id="T359" time="253.772" type="appl" />
         <tli id="T360" time="254.64" type="appl" />
         <tli id="T361" time="255.509" type="appl" />
         <tli id="T362" time="256.377" type="appl" />
         <tli id="T363" time="257.245" type="appl" />
         <tli id="T364" time="258.114" type="appl" />
         <tli id="T365" time="258.982" type="appl" />
         <tli id="T366" time="260.2120801983429" />
         <tli id="T367" time="260.784" type="appl" />
         <tli id="T368" time="261.719" type="appl" />
         <tli id="T369" time="262.653" type="appl" />
         <tli id="T370" time="263.363" type="appl" />
         <tli id="T371" time="264.072" type="appl" />
         <tli id="T372" time="264.782" type="appl" />
         <tli id="T373" time="265.77205342250306" />
         <tli id="T374" time="266.302" type="appl" />
         <tli id="T375" time="267.111" type="appl" />
         <tli id="T376" time="267.921" type="appl" />
         <tli id="T377" time="268.73" type="appl" />
         <tli id="T378" time="269.7520342556249" />
         <tli id="T379" time="270.33" type="appl" />
         <tli id="T380" time="271.12" type="appl" />
         <tli id="T381" time="271.7566600183712" />
         <tli id="T382" time="272.67" type="appl" />
         <tli id="T383" time="273.43" type="appl" />
         <tli id="T384" time="274.19" type="appl" />
         <tli id="T385" time="274.95" type="appl" />
         <tli id="T386" time="275.536667856194" />
         <tli id="T387" time="276.422" type="appl" />
         <tli id="T388" time="277.135" type="appl" />
         <tli id="T389" time="277.847" type="appl" />
         <tli id="T390" time="278.766678342706" />
         <tli id="T391" time="279.67" type="appl" />
         <tli id="T392" time="280.78" type="appl" />
         <tli id="T393" time="281.66333105960956" />
         <tli id="T394" time="282.495" type="appl" />
         <tli id="T395" time="283.01999119284284" />
         <tli id="T396" time="283.597" type="appl" />
         <tli id="T397" time="284.094" type="appl" />
         <tli id="T398" time="284.592" type="appl" />
         <tli id="T399" time="285.089" type="appl" />
         <tli id="T400" time="285.586" type="appl" />
         <tli id="T401" time="286.6586195031909" />
         <tli id="T402" time="286.842" type="appl" />
         <tli id="T403" time="287.6" type="appl" />
         <tli id="T404" time="288.359" type="appl" />
         <tli id="T405" time="289.118" type="appl" />
         <tli id="T406" time="289.876" type="appl" />
         <tli id="T407" time="290.8816720823892" />
         <tli id="T408" time="291.222" type="appl" />
         <tli id="T409" time="291.809" type="appl" />
         <tli id="T410" time="292.396" type="appl" />
         <tli id="T411" time="292.983" type="appl" />
         <tli id="T412" time="293.57" type="appl" />
         <tli id="T413" time="294.156" type="appl" />
         <tli id="T414" time="294.743" type="appl" />
         <tli id="T415" time="295.33" type="appl" />
         <tli id="T416" time="295.917" type="appl" />
         <tli id="T417" time="296.504" type="appl" />
         <tli id="T418" time="297.47190076168937" />
         <tli id="T419" time="298.307" type="appl" />
         <tli id="T420" time="299.524" type="appl" />
         <tli id="T421" time="300.74" type="appl" />
         <tli id="T422" time="301.312" type="appl" />
         <tli id="T423" time="301.885" type="appl" />
         <tli id="T424" time="302.458" type="appl" />
         <tli id="T425" time="302.78333351610945" />
         <tli id="T426" time="303.968" type="appl" />
         <tli id="T427" time="304.906" type="appl" />
         <tli id="T428" time="305.844" type="appl" />
         <tli id="T429" time="306.782" type="appl" />
         <tli id="T430" time="307.72" type="appl" />
         <tli id="T431" time="308.658" type="appl" />
         <tli id="T432" time="309.60267567537323" />
         <tli id="T433" time="309.89" type="appl" />
         <tli id="T434" time="310.184" type="appl" />
         <tli id="T435" time="310.479" type="appl" />
         <tli id="T436" time="310.773" type="appl" />
         <tli id="T437" time="311.067" type="appl" />
         <tli id="T438" time="311.362" type="appl" />
         <tli id="T439" time="311.656" type="appl" />
         <tli id="T440" time="315.458480808193" />
         <tli id="T441" time="315.694" type="appl" />
         <tli id="T442" time="316.512" type="appl" />
         <tli id="T443" time="317.329" type="appl" />
         <tli id="T444" time="318.147" type="appl" />
         <tli id="T445" time="318.964" type="appl" />
         <tli id="T446" time="319.782" type="appl" />
         <tli id="T447" time="320.93900128998337" />
         <tli id="T448" time="321.402" type="appl" />
         <tli id="T449" time="322.204" type="appl" />
         <tli id="T450" time="323.007" type="appl" />
         <tli id="T451" time="323.809" type="appl" />
         <tli id="T452" time="324.612" type="appl" />
         <tli id="T453" time="325.415" type="appl" />
         <tli id="T454" time="326.217" type="appl" />
         <tli id="T455" time="326.99332109179187" />
         <tli id="T456" time="327.67" type="appl" />
         <tli id="T457" time="328.32" type="appl" />
         <tli id="T458" time="328.969" type="appl" />
         <tli id="T459" time="329.619" type="appl" />
         <tli id="T460" time="330.6756731499074" />
         <tli id="T461" time="330.828" type="appl" />
         <tli id="T462" time="331.387" type="appl" />
         <tli id="T463" time="331.946" type="appl" />
         <tli id="T464" time="332.505" type="appl" />
         <tli id="T465" time="333.063" type="appl" />
         <tli id="T466" time="333.622" type="appl" />
         <tli id="T467" time="334.181" type="appl" />
         <tli id="T468" time="334.74" type="appl" />
         <tli id="T469" time="335.89234073569963" />
         <tli id="T470" time="336.002" type="appl" />
         <tli id="T471" time="336.704" type="appl" />
         <tli id="T472" time="337.407" type="appl" />
         <tli id="T473" time="337.78334725394933" />
         <tli id="T474" time="338.879" type="appl" />
         <tli id="T475" time="339.647" type="appl" />
         <tli id="T476" time="340.416" type="appl" />
         <tli id="T477" time="341.184" type="appl" />
         <tli id="T478" time="341.953" type="appl" />
         <tli id="T479" time="342.721" type="appl" />
         <tli id="T480" time="343.50334574912296" />
         <tli id="T481" time="344.114" type="appl" />
         <tli id="T482" time="344.738" type="appl" />
         <tli id="T483" time="345.362" type="appl" />
         <tli id="T484" time="345.987" type="appl" />
         <tli id="T485" time="346.611" type="appl" />
         <tli id="T486" time="347.235" type="appl" />
         <tli id="T487" time="347.868" type="appl" />
         <tli id="T488" time="348.5" type="appl" />
         <tli id="T489" time="349.133" type="appl" />
         <tli id="T490" time="349.765" type="appl" />
         <tli id="T491" time="350.398" type="appl" />
         <tli id="T492" time="351.03" type="appl" />
         <tli id="T493" time="352.2916367596135" />
         <tli id="T494" time="352.356" type="appl" />
         <tli id="T495" time="353.05" type="appl" />
         <tli id="T496" time="353.743" type="appl" />
         <tli id="T497" time="354.437" type="appl" />
         <tli id="T498" time="354.8366765864887" />
         <tli id="T499" time="355.639" type="appl" />
         <tli id="T500" time="356.149" type="appl" />
         <tli id="T501" time="356.658" type="appl" />
         <tli id="T502" time="357.168" type="appl" />
         <tli id="T503" time="357.677" type="appl" />
         <tli id="T504" time="358.186" type="appl" />
         <tli id="T505" time="358.696" type="appl" />
         <tli id="T506" time="359.58498705280533" />
         <tli id="T507" time="359.86" type="appl" />
         <tli id="T508" time="360.516" type="appl" />
         <tli id="T509" time="361.171" type="appl" />
         <tli id="T510" time="361.827" type="appl" />
         <tli id="T511" time="362.482" type="appl" />
         <tli id="T512" time="363.138" type="appl" />
         <tli id="T513" time="363.793" type="appl" />
         <tli id="T514" time="364.449" type="appl" />
         <tli id="T515" time="365.104" type="appl" />
         <tli id="T516" time="365.76" type="appl" />
         <tli id="T517" time="366.415" type="appl" />
         <tli id="T518" time="367.071" type="appl" />
         <tli id="T519" time="367.726" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T519" id="Seg_0" n="sc" s="T0">
               <ts e="T14" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Savʼettar</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ɨjaːktara</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">haŋardɨː</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">keleriger</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">Kurʼja</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">di͡en</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">olokko</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">hajɨn</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">aːjɨ</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">balɨktɨː</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">oloroːčču</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_38" n="HIAT:w" s="T11">etilere</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_41" n="HIAT:w" s="T12">haka</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">dʼono</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_48" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">Tu͡ora</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">karaktaːk</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_56" n="HIAT:w" s="T16">čubu-čubu</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">hɨldʼɨbat</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_62" n="HIAT:w" s="T18">ologo</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_66" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">Onnuk</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">ɨraːk</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">balɨktɨːr</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_77" n="HIAT:w" s="T22">hirdere</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_81" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_83" n="HIAT:w" s="T23">Kahɨ͡an</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_86" n="HIAT:w" s="T24">ire</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">kim</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">eme</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_95" n="HIAT:w" s="T27">keli͡e</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T28">hajɨn</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_101" n="HIAT:w" s="T29">čugastaːgɨ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_104" n="HIAT:w" s="T30">hɨtɨnnʼaktarga</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_107" n="HIAT:w" s="T31">ös</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_110" n="HIAT:w" s="T32">kepsiː</ts>
                  <nts id="Seg_111" n="HIAT:ip">,</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_114" n="HIAT:w" s="T33">bejetin</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_117" n="HIAT:w" s="T34">ologun</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_120" n="HIAT:w" s="T35">tuhunan</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_124" n="HIAT:w" s="T36">bileːri</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_127" n="HIAT:w" s="T37">kajdak</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_130" n="HIAT:w" s="T38">balɨktɨːllarɨn</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_133" n="HIAT:w" s="T39">Kurʼja</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_136" n="HIAT:w" s="T40">dʼono</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_140" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_142" n="HIAT:w" s="T41">Biːr</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_145" n="HIAT:w" s="T42">hajɨn</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_148" n="HIAT:w" s="T43">ogonnʼottor</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_151" n="HIAT:w" s="T44">erde</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_154" n="HIAT:w" s="T45">harsi͡erda</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_157" n="HIAT:w" s="T46">erdinen</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_160" n="HIAT:w" s="T47">kaːlbɨttara</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_163" n="HIAT:w" s="T48">biːskelerin</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_166" n="HIAT:w" s="T49">körüne</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_170" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_172" n="HIAT:w" s="T50">Dʼaktar</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_176" n="HIAT:w" s="T51">ogo</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_179" n="HIAT:w" s="T52">aːjmak</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_182" n="HIAT:w" s="T53">dʼi͡eleriger</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_185" n="HIAT:w" s="T54">olorbuttara</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_189" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_191" n="HIAT:w" s="T55">Kim</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_194" n="HIAT:w" s="T56">balɨk</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_197" n="HIAT:w" s="T57">ɨraːstanar</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_201" n="HIAT:w" s="T58">kim</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_204" n="HIAT:w" s="T59">dʼukula</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_207" n="HIAT:w" s="T60">kuːrdunar</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_211" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_213" n="HIAT:w" s="T61">Olorton</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_216" n="HIAT:w" s="T62">ikki</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_219" n="HIAT:w" s="T63">emeːksin</ts>
                  <nts id="Seg_220" n="HIAT:ip">,</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_223" n="HIAT:w" s="T64">ogonnʼottor</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_226" n="HIAT:w" s="T65">keli͡ekteriger</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_229" n="HIAT:w" s="T66">di͡eri</ts>
                  <nts id="Seg_230" n="HIAT:ip">,</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_233" n="HIAT:w" s="T67">hɨrga</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_236" n="HIAT:w" s="T68">dʼi͡elerin</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_239" n="HIAT:w" s="T69">tiriːlerin</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_242" n="HIAT:w" s="T70">abɨraktɨːllar</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_245" n="HIAT:w" s="T71">ebit</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_249" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_251" n="HIAT:w" s="T72">Kunʼaːs</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_254" n="HIAT:w" s="T73">bagaj</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_257" n="HIAT:w" s="T74">kün</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_260" n="HIAT:w" s="T75">turbuta</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_264" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_266" n="HIAT:w" s="T76">Dudupta</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_269" n="HIAT:w" s="T77">aːn</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_272" n="HIAT:w" s="T78">ebelerin</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_275" n="HIAT:w" s="T79">uːta</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_278" n="HIAT:w" s="T80">čumputugar</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_281" n="HIAT:w" s="T81">ürüŋ</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_284" n="HIAT:w" s="T82">etiŋ</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_287" n="HIAT:w" s="T83">bɨlɨttara</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_290" n="HIAT:w" s="T84">bɨdʼɨrahan</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_293" n="HIAT:w" s="T85">köstöllör</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_297" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_299" n="HIAT:w" s="T86">Kumaːr</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_302" n="HIAT:w" s="T87">kotoku</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_305" n="HIAT:w" s="T88">kɨnata</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_308" n="HIAT:w" s="T89">katar</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_311" n="HIAT:w" s="T90">kunʼaha</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_314" n="HIAT:w" s="T91">turbuta</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_318" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_320" n="HIAT:w" s="T92">Emeːksittere</ts>
                  <nts id="Seg_321" n="HIAT:ip">,</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_324" n="HIAT:w" s="T93">ol</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_327" n="HIAT:w" s="T94">daː</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_330" n="HIAT:w" s="T95">bu͡ollar</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_333" n="HIAT:w" s="T96">iliːlere</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_336" n="HIAT:w" s="T97">holoto</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_339" n="HIAT:w" s="T98">hu͡ok</ts>
                  <nts id="Seg_340" n="HIAT:ip">,</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_343" n="HIAT:w" s="T99">ilbineller</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_347" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_349" n="HIAT:w" s="T100">Ol</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_352" n="HIAT:w" s="T101">bɨːhɨgar</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_355" n="HIAT:w" s="T102">huptu</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_358" n="HIAT:w" s="T103">tiktiː</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_361" n="HIAT:w" s="T104">bu͡olbuttara</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_365" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_367" n="HIAT:w" s="T105">Innelerin</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_370" n="HIAT:w" s="T106">tɨ͡aha</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_373" n="HIAT:w" s="T107">öŋnöːk</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_376" n="HIAT:w" s="T108">tiriːge</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_379" n="HIAT:w" s="T109">kurdurguːra</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_382" n="HIAT:w" s="T110">ɨraːktan</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_385" n="HIAT:w" s="T111">ihiller</ts>
                  <nts id="Seg_386" n="HIAT:ip">.</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_389" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_391" n="HIAT:w" s="T112">Ol</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_394" n="HIAT:w" s="T113">ülelene</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_397" n="HIAT:w" s="T114">olordoktoruna</ts>
                  <nts id="Seg_398" n="HIAT:ip">,</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_401" n="HIAT:w" s="T115">hirdere</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_404" n="HIAT:w" s="T116">togo</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_407" n="HIAT:w" s="T117">ere</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_410" n="HIAT:w" s="T118">nʼirgijer</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_413" n="HIAT:w" s="T119">ebit</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_417" n="HIAT:u" s="T120">
                  <nts id="Seg_418" n="HIAT:ip">"</nts>
                  <ts e="T121" id="Seg_420" n="HIAT:w" s="T120">Be</ts>
                  <nts id="Seg_421" n="HIAT:ip">"</nts>
                  <nts id="Seg_422" n="HIAT:ip">,</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_425" n="HIAT:w" s="T121">di͡ebit</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_428" n="HIAT:w" s="T122">biːrgehe</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_432" n="HIAT:u" s="T123">
                  <nts id="Seg_433" n="HIAT:ip">"</nts>
                  <ts e="T124" id="Seg_435" n="HIAT:w" s="T123">Togo</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_438" n="HIAT:w" s="T124">hirbit</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_441" n="HIAT:w" s="T125">nʼirgijer</ts>
                  <nts id="Seg_442" n="HIAT:ip">?</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_445" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_447" n="HIAT:w" s="T126">Togo</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_450" n="HIAT:w" s="T127">etiŋ</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_453" n="HIAT:w" s="T128">tɨ͡aha</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_456" n="HIAT:w" s="T129">itigirdik</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_459" n="HIAT:w" s="T130">bu͡olu͡oj</ts>
                  <nts id="Seg_460" n="HIAT:ip">?</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_463" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_465" n="HIAT:w" s="T131">Tu͡ok</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_468" n="HIAT:w" s="T132">ere</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_471" n="HIAT:w" s="T133">diktite</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_474" n="HIAT:w" s="T134">bu͡olla</ts>
                  <nts id="Seg_475" n="HIAT:ip">,</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_478" n="HIAT:w" s="T135">ihilleː</ts>
                  <nts id="Seg_479" n="HIAT:ip">,</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_482" n="HIAT:w" s="T136">emeːksin</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip">"</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_487" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_489" n="HIAT:w" s="T137">Krʼistʼin</ts>
                  <nts id="Seg_490" n="HIAT:ip">,</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_493" n="HIAT:w" s="T138">kɨrdʼagas</ts>
                  <nts id="Seg_494" n="HIAT:ip">,</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_497" n="HIAT:w" s="T139">aha</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_500" n="HIAT:w" s="T140">bɨldʼaːččɨ</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_503" n="HIAT:w" s="T141">gɨlbajbɨt</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_506" n="HIAT:w" s="T142">emeːksin</ts>
                  <nts id="Seg_507" n="HIAT:ip">,</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_510" n="HIAT:w" s="T143">u͡ohugar</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_513" n="HIAT:w" s="T144">biːr</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_516" n="HIAT:w" s="T145">da</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_519" n="HIAT:w" s="T146">tiːhe</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_522" n="HIAT:w" s="T147">hu͡ok</ts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_526" n="HIAT:w" s="T148">ös</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_529" n="HIAT:w" s="T149">kiːrbet</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_532" n="HIAT:w" s="T150">pɨlaːtɨn</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_535" n="HIAT:w" s="T151">menʼiːtitten</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_538" n="HIAT:w" s="T152">hulbu</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_541" n="HIAT:w" s="T153">tardaːt</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_544" n="HIAT:w" s="T154">ihilleːbit</ts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_548" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_550" n="HIAT:w" s="T155">Ol</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_552" n="HIAT:ip">(</nts>
                  <nts id="Seg_553" n="HIAT:ip">(</nts>
                  <ats e="T520" id="Seg_554" n="HIAT:non-pho" s="T156">…</ats>
                  <nts id="Seg_555" n="HIAT:ip">)</nts>
                  <nts id="Seg_556" n="HIAT:ip">)</nts>
                  <nts id="Seg_557" n="HIAT:ip">,</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_560" n="HIAT:w" s="T520">ikki</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_563" n="HIAT:w" s="T157">emeːksin</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_566" n="HIAT:w" s="T158">kallaːn</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_569" n="HIAT:w" s="T159">di͡ek</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_572" n="HIAT:w" s="T160">nʼɨlajan</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_575" n="HIAT:w" s="T161">baraːn</ts>
                  <nts id="Seg_576" n="HIAT:ip">,</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_579" n="HIAT:w" s="T162">üːrpüt</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_582" n="HIAT:w" s="T163">taba</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_585" n="HIAT:w" s="T164">kördük</ts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_589" n="HIAT:w" s="T165">nʼirgijer</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_592" n="HIAT:w" s="T166">tɨ͡ahɨ</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_595" n="HIAT:w" s="T167">ihilleːn</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_598" n="HIAT:w" s="T168">bu͡olbuttar</ts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_602" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_604" n="HIAT:w" s="T170">Kumaːr</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_607" n="HIAT:w" s="T171">daː</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_610" n="HIAT:w" s="T172">hiːrin</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_613" n="HIAT:w" s="T173">bilbekke</ts>
                  <nts id="Seg_614" n="HIAT:ip">,</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_617" n="HIAT:w" s="T174">örköːn</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_620" n="HIAT:w" s="T175">olorbuttar</ts>
                  <nts id="Seg_621" n="HIAT:ip">,</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_624" n="HIAT:w" s="T176">anʼaktarɨn</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_627" n="HIAT:w" s="T177">atan</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_630" n="HIAT:w" s="T178">baraːn</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_634" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_636" n="HIAT:w" s="T179">Attɨlarɨgar</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_639" n="HIAT:w" s="T180">tiriːler</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_642" n="HIAT:w" s="T181">loskutu͡oktara</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_645" n="HIAT:w" s="T182">onno-manna</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_648" n="HIAT:w" s="T183">hɨtallar</ts>
                  <nts id="Seg_649" n="HIAT:ip">.</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_652" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_654" n="HIAT:w" s="T184">Bɨ͡annan</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_657" n="HIAT:w" s="T185">baːjɨːlaːk</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_660" n="HIAT:w" s="T186">mordʼoːloro</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_663" n="HIAT:w" s="T187">iliːleriger</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_666" n="HIAT:w" s="T188">buru͡olahallar</ts>
                  <nts id="Seg_667" n="HIAT:ip">.</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_670" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_672" n="HIAT:w" s="T189">Küːppüt</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_675" n="HIAT:w" s="T190">kördük</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_678" n="HIAT:w" s="T191">sɨp-sɨp</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_681" n="HIAT:w" s="T192">kahan</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_684" n="HIAT:w" s="T193">oloru͡oktarɨn</ts>
                  <nts id="Seg_685" n="HIAT:ip">.</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_688" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_690" n="HIAT:w" s="T194">Kirdik</ts>
                  <nts id="Seg_691" n="HIAT:ip">,</nts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_694" n="HIAT:w" s="T195">ginner</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_697" n="HIAT:w" s="T196">bilbetek</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_700" n="HIAT:w" s="T197">tɨ͡astara</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_703" n="HIAT:w" s="T198">bu͡olbut</ts>
                  <nts id="Seg_704" n="HIAT:ip">.</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_707" n="HIAT:u" s="T199">
                  <nts id="Seg_708" n="HIAT:ip">"</nts>
                  <ts e="T200" id="Seg_710" n="HIAT:w" s="T199">Kaja</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_713" n="HIAT:w" s="T200">da</ts>
                  <nts id="Seg_714" n="HIAT:ip">,</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_717" n="HIAT:w" s="T201">dʼe</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_720" n="HIAT:w" s="T202">öllübüt</ts>
                  <nts id="Seg_721" n="HIAT:ip">"</nts>
                  <nts id="Seg_722" n="HIAT:ip">,</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_725" n="HIAT:w" s="T203">diː</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_728" n="HIAT:w" s="T204">tüheːt</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_731" n="HIAT:w" s="T205">biːrges</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_734" n="HIAT:w" s="T206">emeːksin</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_737" n="HIAT:w" s="T207">tura</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_740" n="HIAT:w" s="T208">ekkireːbit</ts>
                  <nts id="Seg_741" n="HIAT:ip">,</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_744" n="HIAT:w" s="T209">mas</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_747" n="HIAT:w" s="T210">di͡ek</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_750" n="HIAT:w" s="T211">körüleːbit</ts>
                  <nts id="Seg_751" n="HIAT:ip">.</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_754" n="HIAT:u" s="T212">
                  <nts id="Seg_755" n="HIAT:ip">"</nts>
                  <ts e="T213" id="Seg_757" n="HIAT:w" s="T212">Dʼe</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_761" n="HIAT:w" s="T213">kötön</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_764" n="HIAT:w" s="T214">iher</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_767" n="HIAT:w" s="T215">uluː</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_770" n="HIAT:w" s="T216">kötörö</ts>
                  <nts id="Seg_771" n="HIAT:ip">.</nts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_774" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_776" n="HIAT:w" s="T217">Kör</ts>
                  <nts id="Seg_777" n="HIAT:ip">,</nts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_780" n="HIAT:w" s="T218">ol</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_783" n="HIAT:w" s="T219">kɨtaran</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_786" n="HIAT:w" s="T220">iher</ts>
                  <nts id="Seg_787" n="HIAT:ip">,</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_790" n="HIAT:w" s="T221">gini</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_793" n="HIAT:w" s="T222">sihin</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_796" n="HIAT:w" s="T223">tɨ͡aha</ts>
                  <nts id="Seg_797" n="HIAT:ip">,</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_800" n="HIAT:w" s="T224">ebit</ts>
                  <nts id="Seg_801" n="HIAT:ip">.</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_804" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_806" n="HIAT:w" s="T225">Xoːspodi</ts>
                  <nts id="Seg_807" n="HIAT:ip">,</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_810" n="HIAT:w" s="T226">min</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_813" n="HIAT:w" s="T227">haŋarbatagɨm</ts>
                  <nts id="Seg_814" n="HIAT:ip">,</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_817" n="HIAT:w" s="T228">körbötögüm</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_820" n="HIAT:w" s="T229">bu͡ollun</ts>
                  <nts id="Seg_821" n="HIAT:ip">"</nts>
                  <nts id="Seg_822" n="HIAT:ip">,</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_825" n="HIAT:w" s="T230">di͡en</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_828" n="HIAT:w" s="T231">emeːksin</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_831" n="HIAT:w" s="T232">labɨjan</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_834" n="HIAT:w" s="T233">barbɨt</ts>
                  <nts id="Seg_835" n="HIAT:ip">.</nts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_838" n="HIAT:u" s="T234">
                  <nts id="Seg_839" n="HIAT:ip">"</nts>
                  <ts e="T235" id="Seg_841" n="HIAT:w" s="T234">O</ts>
                  <nts id="Seg_842" n="HIAT:ip">,</nts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_845" n="HIAT:w" s="T235">tu͡ok</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_848" n="HIAT:w" s="T236">kötörün</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_851" n="HIAT:w" s="T237">gɨnagɨn</ts>
                  <nts id="Seg_852" n="HIAT:ip">"</nts>
                  <nts id="Seg_853" n="HIAT:ip">,</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_856" n="HIAT:w" s="T238">diː-diː</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_859" n="HIAT:w" s="T239">dogoro</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_862" n="HIAT:w" s="T240">emi͡e</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_865" n="HIAT:w" s="T241">körüleːbit</ts>
                  <nts id="Seg_866" n="HIAT:ip">.</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_869" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_871" n="HIAT:w" s="T242">Ataktarɨn</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_874" n="HIAT:w" s="T243">annɨlarɨgar</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_877" n="HIAT:w" s="T244">hirdere</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_880" n="HIAT:w" s="T245">möŋör</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_883" n="HIAT:w" s="T246">kördük</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_886" n="HIAT:w" s="T247">bu͡olbut</ts>
                  <nts id="Seg_887" n="HIAT:ip">.</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_890" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_892" n="HIAT:w" s="T248">Urahalar</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_895" n="HIAT:w" s="T249">titireːbitter</ts>
                  <nts id="Seg_896" n="HIAT:ip">.</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_899" n="HIAT:u" s="T250">
                  <nts id="Seg_900" n="HIAT:ip">"</nts>
                  <ts e="T251" id="Seg_902" n="HIAT:w" s="T250">Türgennik</ts>
                  <nts id="Seg_903" n="HIAT:ip">,</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_906" n="HIAT:w" s="T251">hɨrga</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_909" n="HIAT:w" s="T252">di͡ek</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_912" n="HIAT:w" s="T253">annɨtɨgar</ts>
                  <nts id="Seg_913" n="HIAT:ip">"</nts>
                  <nts id="Seg_914" n="HIAT:ip">,</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_917" n="HIAT:w" s="T254">diː</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_920" n="HIAT:w" s="T255">tüheːt</ts>
                  <nts id="Seg_921" n="HIAT:ip">,</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_924" n="HIAT:w" s="T256">biːrgehe</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_927" n="HIAT:w" s="T257">čip</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_930" n="HIAT:w" s="T258">gɨmmɨt</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_933" n="HIAT:w" s="T259">katɨːlardaːk</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_936" n="HIAT:w" s="T260">mu͡osta</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_939" n="HIAT:w" s="T261">annɨgar</ts>
                  <nts id="Seg_940" n="HIAT:ip">,</nts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_943" n="HIAT:w" s="T262">dogoro</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_946" n="HIAT:w" s="T263">ginini</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_949" n="HIAT:w" s="T264">köŋüjen</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_952" n="HIAT:w" s="T265">kiːrbit</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_955" n="HIAT:w" s="T266">bu͡olagɨn</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_958" n="HIAT:w" s="T267">annɨgar</ts>
                  <nts id="Seg_959" n="HIAT:ip">.</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_962" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_964" n="HIAT:w" s="T268">Ol</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_967" n="HIAT:w" s="T269">kem</ts>
                  <nts id="Seg_968" n="HIAT:ip">,</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_971" n="HIAT:w" s="T270">lü͡ötčik</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_974" n="HIAT:w" s="T271">haŋardɨː</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_977" n="HIAT:w" s="T272">uraha</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_980" n="HIAT:w" s="T273">dʼi͡eleri</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_983" n="HIAT:w" s="T274">körö</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_986" n="HIAT:w" s="T275">hotoru</ts>
                  <nts id="Seg_987" n="HIAT:ip">,</nts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_990" n="HIAT:w" s="T276">nʼamčɨgas</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_993" n="HIAT:w" s="T277">bagajɨ</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_996" n="HIAT:w" s="T278">kötön</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_999" n="HIAT:w" s="T279">aːspɨt</ts>
                  <nts id="Seg_1000" n="HIAT:ip">.</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1003" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1005" n="HIAT:w" s="T280">Hɨrga</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1008" n="HIAT:w" s="T281">dʼi͡eni</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1011" n="HIAT:w" s="T282">gɨtta</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1014" n="HIAT:w" s="T283">bu͡olak</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1017" n="HIAT:w" s="T284">annɨlarɨttan</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1019" n="HIAT:ip">(</nts>
                  <ts e="T287" id="Seg_1021" n="HIAT:w" s="T285">hiːp-</ts>
                  <nts id="Seg_1022" n="HIAT:ip">)</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1025" n="HIAT:w" s="T287">emeːksitter</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1028" n="HIAT:w" s="T288">harɨː</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1031" n="HIAT:w" s="T289">ataktara</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1034" n="HIAT:w" s="T290">mömpökkö</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1037" n="HIAT:w" s="T291">čoroho</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1040" n="HIAT:w" s="T292">hɨppɨttar</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1044" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1046" n="HIAT:w" s="T293">Kɨtɨlga</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1049" n="HIAT:w" s="T294">ogonnʼottor</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1052" n="HIAT:w" s="T295">ekkiriː-ekkiriː</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1055" n="HIAT:w" s="T296">bergehelerin</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1058" n="HIAT:w" s="T297">samalʼokka</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1061" n="HIAT:w" s="T298">dajbɨː</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1064" n="HIAT:w" s="T299">turbuttar</ts>
                  <nts id="Seg_1065" n="HIAT:ip">.</nts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1068" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1070" n="HIAT:w" s="T300">Ginner</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1073" n="HIAT:w" s="T301">ü͡örüːlere</ts>
                  <nts id="Seg_1074" n="HIAT:ip">,</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1077" n="HIAT:w" s="T302">haŋardɨː</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1080" n="HIAT:w" s="T303">samalʼot</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1083" n="HIAT:w" s="T304">keleriger</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1086" n="HIAT:w" s="T305">mu͡oraga</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1089" n="HIAT:w" s="T306">bert</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1092" n="HIAT:w" s="T307">ebit</ts>
                  <nts id="Seg_1093" n="HIAT:ip">.</nts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1096" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1098" n="HIAT:w" s="T308">Bileller</ts>
                  <nts id="Seg_1099" n="HIAT:ip">,</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1102" n="HIAT:w" s="T309">savʼetskʼij</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1105" n="HIAT:w" s="T310">vlastʼ</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1108" n="HIAT:w" s="T311">bilser</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1111" n="HIAT:w" s="T312">bu͡ola</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1114" n="HIAT:w" s="T313">hɨldʼɨ͡aga</ts>
                  <nts id="Seg_1115" n="HIAT:ip">,</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1118" n="HIAT:w" s="T314">kajtak</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1121" n="HIAT:w" s="T315">tabahɨttar</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1125" n="HIAT:w" s="T316">balɨkčɨttar</ts>
                  <nts id="Seg_1126" n="HIAT:ip">,</nts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1129" n="HIAT:w" s="T317">olorollorun</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1132" n="HIAT:w" s="T318">köröːrü</ts>
                  <nts id="Seg_1133" n="HIAT:ip">.</nts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1136" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1138" n="HIAT:w" s="T319">Hɨːr</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1141" n="HIAT:w" s="T320">ürdüger</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1144" n="HIAT:w" s="T321">taksaːt</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1147" n="HIAT:w" s="T322">oduːlammɨttar</ts>
                  <nts id="Seg_1148" n="HIAT:ip">–</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1151" n="HIAT:w" s="T323">uraha</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1154" n="HIAT:w" s="T324">dʼi͡elere</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1157" n="HIAT:w" s="T325">buru͡olaːbattar</ts>
                  <nts id="Seg_1158" n="HIAT:ip">.</nts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1161" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1163" n="HIAT:w" s="T326">Ti͡ergeːn</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1166" n="HIAT:w" s="T327">ürdünen</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1168" n="HIAT:ip">(</nts>
                  <ts e="T330" id="Seg_1170" n="HIAT:w" s="T328">kim-</ts>
                  <nts id="Seg_1171" n="HIAT:ip">)</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1174" n="HIAT:w" s="T330">im</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1177" n="HIAT:w" s="T331">dʼɨlɨččɨ</ts>
                  <nts id="Seg_1178" n="HIAT:ip">.</nts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1181" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1183" n="HIAT:w" s="T332">ɨt</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1186" n="HIAT:w" s="T333">da</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1189" n="HIAT:w" s="T334">ürbet</ts>
                  <nts id="Seg_1190" n="HIAT:ip">,</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1193" n="HIAT:w" s="T335">kihi</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1196" n="HIAT:w" s="T336">da</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1199" n="HIAT:w" s="T337">haŋata</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1202" n="HIAT:w" s="T338">ihillibet</ts>
                  <nts id="Seg_1203" n="HIAT:ip">.</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1206" n="HIAT:u" s="T339">
                  <nts id="Seg_1207" n="HIAT:ip">"</nts>
                  <ts e="T340" id="Seg_1209" n="HIAT:w" s="T339">Tu͡ok</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1212" n="HIAT:w" s="T340">dʼonor</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1215" n="HIAT:w" s="T341">bu</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1218" n="HIAT:w" s="T342">emeːksitter</ts>
                  <nts id="Seg_1219" n="HIAT:ip">?</nts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1222" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1224" n="HIAT:w" s="T343">Bilbetter</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1227" n="HIAT:w" s="T344">du͡o</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1230" n="HIAT:w" s="T345">bihigi</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1233" n="HIAT:w" s="T346">keli͡ekpitin</ts>
                  <nts id="Seg_1234" n="HIAT:ip">"</nts>
                  <nts id="Seg_1235" n="HIAT:ip">,</nts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1238" n="HIAT:w" s="T347">di͡ebit</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1241" n="HIAT:w" s="T348">Onoː</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1244" n="HIAT:w" s="T349">ogonnʼor</ts>
                  <nts id="Seg_1245" n="HIAT:ip">.</nts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1248" n="HIAT:u" s="T350">
                  <nts id="Seg_1249" n="HIAT:ip">"</nts>
                  <ts e="T351" id="Seg_1251" n="HIAT:w" s="T350">Kanna</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1254" n="HIAT:w" s="T351">baːrgɨtɨːj</ts>
                  <nts id="Seg_1255" n="HIAT:ip">,</nts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1258" n="HIAT:w" s="T352">dʼi͡eleːkter</ts>
                  <nts id="Seg_1259" n="HIAT:ip">"</nts>
                  <nts id="Seg_1260" n="HIAT:ip">,</nts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1262" n="HIAT:ip">(</nts>
                  <ts e="T355" id="Seg_1264" n="HIAT:w" s="T353">ü͡ög-</ts>
                  <nts id="Seg_1265" n="HIAT:ip">)</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1268" n="HIAT:w" s="T355">ü͡ögüleːbit</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1271" n="HIAT:w" s="T356">ɨ͡al</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1274" n="HIAT:w" s="T357">kihite</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1278" n="HIAT:u" s="T358">
                  <nts id="Seg_1279" n="HIAT:ip">"</nts>
                  <ts e="T359" id="Seg_1281" n="HIAT:w" s="T358">Ara</ts>
                  <nts id="Seg_1282" n="HIAT:ip">,</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1285" n="HIAT:w" s="T359">abɨraːŋ</ts>
                  <nts id="Seg_1286" n="HIAT:ip">,</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1289" n="HIAT:w" s="T360">araːrɨŋ</ts>
                  <nts id="Seg_1290" n="HIAT:ip">,</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1293" n="HIAT:w" s="T361">öllübüt</ts>
                  <nts id="Seg_1294" n="HIAT:ip">"</nts>
                  <nts id="Seg_1295" n="HIAT:ip">,</nts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1298" n="HIAT:w" s="T362">hir</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1301" n="HIAT:w" s="T363">ihitten</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1304" n="HIAT:w" s="T364">ü͡ögüː</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1307" n="HIAT:w" s="T365">ihillibit</ts>
                  <nts id="Seg_1308" n="HIAT:ip">.</nts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1311" n="HIAT:u" s="T366">
                  <nts id="Seg_1312" n="HIAT:ip">"</nts>
                  <ts e="T367" id="Seg_1314" n="HIAT:w" s="T366">Tu͡oktarɨj</ts>
                  <nts id="Seg_1315" n="HIAT:ip">,</nts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1318" n="HIAT:w" s="T367">iːrbitter</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1321" n="HIAT:w" s="T368">du͡o</ts>
                  <nts id="Seg_1322" n="HIAT:ip">"</nts>
                  <nts id="Seg_1323" n="HIAT:ip">,</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1326" n="HIAT:w" s="T369">diː</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1329" n="HIAT:w" s="T370">tüheːt</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1332" n="HIAT:w" s="T371">Onoː</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1335" n="HIAT:w" s="T372">körüleːbit</ts>
                  <nts id="Seg_1336" n="HIAT:ip">.</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T378" id="Seg_1339" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1341" n="HIAT:w" s="T373">Gini</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1344" n="HIAT:w" s="T374">hɨrga</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1347" n="HIAT:w" s="T375">dʼi͡eler</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1350" n="HIAT:w" s="T376">annɨlarɨgar</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1353" n="HIAT:w" s="T377">körüleːbit</ts>
                  <nts id="Seg_1354" n="HIAT:ip">.</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1357" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1359" n="HIAT:w" s="T378">Tu͡ok</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1362" n="HIAT:w" s="T379">diktite</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1365" n="HIAT:w" s="T380">bu͡olla</ts>
                  <nts id="Seg_1366" n="HIAT:ip">?</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1369" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1371" n="HIAT:w" s="T381">Emeːksitterin</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1374" n="HIAT:w" s="T382">ataktara</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1377" n="HIAT:w" s="T383">onno</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1380" n="HIAT:w" s="T384">halɨbɨraha</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1383" n="HIAT:w" s="T385">hɨppɨttar</ts>
                  <nts id="Seg_1384" n="HIAT:ip">.</nts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1387" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1389" n="HIAT:w" s="T386">Kördögüne</ts>
                  <nts id="Seg_1390" n="HIAT:ip">,</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1393" n="HIAT:w" s="T387">taksa</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1396" n="HIAT:w" s="T388">hatɨːllar</ts>
                  <nts id="Seg_1397" n="HIAT:ip">,</nts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1400" n="HIAT:w" s="T389">bɨhɨlaːk</ts>
                  <nts id="Seg_1401" n="HIAT:ip">.</nts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1404" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1406" n="HIAT:w" s="T390">Katɨːlar</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1409" n="HIAT:w" s="T391">taŋastarɨttan</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1412" n="HIAT:w" s="T392">iːlbitter</ts>
                  <nts id="Seg_1413" n="HIAT:ip">.</nts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1416" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1418" n="HIAT:w" s="T393">Togo</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1421" n="HIAT:w" s="T394">ɨːtɨ͡aktaraj</ts>
                  <nts id="Seg_1422" n="HIAT:ip">?</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1425" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1427" n="HIAT:w" s="T395">Iske</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1430" n="HIAT:w" s="T396">da</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1433" n="HIAT:w" s="T397">kiːllerbetter</ts>
                  <nts id="Seg_1434" n="HIAT:ip">,</nts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1437" n="HIAT:w" s="T398">taska</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1440" n="HIAT:w" s="T399">da</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1443" n="HIAT:w" s="T400">tahaːrbattar</ts>
                  <nts id="Seg_1444" n="HIAT:ip">.</nts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1447" n="HIAT:u" s="T401">
                  <nts id="Seg_1448" n="HIAT:ip">"</nts>
                  <ts e="T402" id="Seg_1450" n="HIAT:w" s="T401">Ara</ts>
                  <nts id="Seg_1451" n="HIAT:ip">"</nts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1454" n="HIAT:w" s="T402">dehiː</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1457" n="HIAT:w" s="T403">elbeːn</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1460" n="HIAT:w" s="T404">ispit</ts>
                  <nts id="Seg_1461" n="HIAT:ip">,</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1464" n="HIAT:w" s="T405">ɨnčɨktɨːr</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1467" n="HIAT:w" s="T406">kojdubut</ts>
                  <nts id="Seg_1468" n="HIAT:ip">.</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T418" id="Seg_1471" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1473" n="HIAT:w" s="T407">Ogonnʼottor</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1476" n="HIAT:w" s="T408">tu͡ok</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1479" n="HIAT:w" s="T409">da</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1482" n="HIAT:w" s="T410">di͡ek</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1485" n="HIAT:w" s="T411">berditten</ts>
                  <nts id="Seg_1486" n="HIAT:ip">,</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1489" n="HIAT:w" s="T412">sögö</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1492" n="HIAT:w" s="T413">hataːn</ts>
                  <nts id="Seg_1493" n="HIAT:ip">,</nts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1496" n="HIAT:w" s="T414">tugu</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1499" n="HIAT:w" s="T415">da</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1502" n="HIAT:w" s="T416">haŋarbat</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1505" n="HIAT:w" s="T417">bu͡olbuttar</ts>
                  <nts id="Seg_1506" n="HIAT:ip">.</nts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1509" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1511" n="HIAT:w" s="T418">Emeːksitteri</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1514" n="HIAT:w" s="T419">biːrdiː</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1517" n="HIAT:w" s="T420">boskoloːbuttar</ts>
                  <nts id="Seg_1518" n="HIAT:ip">.</nts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1521" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1523" n="HIAT:w" s="T421">Ginnerge</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1526" n="HIAT:w" s="T422">hɨraj</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1529" n="HIAT:w" s="T423">keli͡e</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1532" n="HIAT:w" s="T424">du͡o</ts>
                  <nts id="Seg_1533" n="HIAT:ip">.</nts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1536" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1538" n="HIAT:w" s="T425">Astara</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1541" n="HIAT:w" s="T426">karaktarɨn</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1544" n="HIAT:w" s="T427">happɨt</ts>
                  <nts id="Seg_1545" n="HIAT:ip">,</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1548" n="HIAT:w" s="T428">harɨː</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1551" n="HIAT:w" s="T429">ataktara</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1554" n="HIAT:w" s="T430">nʼɨpkaččɨ</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1557" n="HIAT:w" s="T431">tüspütter</ts>
                  <nts id="Seg_1558" n="HIAT:ip">.</nts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1561" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1563" n="HIAT:w" s="T432">Dʼühünnere</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1566" n="HIAT:w" s="T433">ularɨjbɨt</ts>
                  <nts id="Seg_1567" n="HIAT:ip">,</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1570" n="HIAT:w" s="T434">bu͡ortan</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1573" n="HIAT:w" s="T435">taksɨbɨt</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1576" n="HIAT:w" s="T436">kɨrsa</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1579" n="HIAT:w" s="T437">ogolorun</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1582" n="HIAT:w" s="T438">kördük</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1585" n="HIAT:w" s="T439">bu͡olbuttar</ts>
                  <nts id="Seg_1586" n="HIAT:ip">.</nts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1589" n="HIAT:u" s="T440">
                  <nts id="Seg_1590" n="HIAT:ip">"</nts>
                  <ts e="T441" id="Seg_1592" n="HIAT:w" s="T440">Kaja</ts>
                  <nts id="Seg_1593" n="HIAT:ip">,</nts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1596" n="HIAT:w" s="T441">tu͡ok</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1599" n="HIAT:w" s="T442">bu͡olbukkutuj</ts>
                  <nts id="Seg_1600" n="HIAT:ip">"</nts>
                  <nts id="Seg_1601" n="HIAT:ip">,</nts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1604" n="HIAT:w" s="T443">di͡en</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1607" n="HIAT:w" s="T444">ɨjɨppɨttar</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1610" n="HIAT:w" s="T445">biːrdiː</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1613" n="HIAT:w" s="T446">duruk</ts>
                  <nts id="Seg_1614" n="HIAT:ip">.</nts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1617" n="HIAT:u" s="T447">
                  <nts id="Seg_1618" n="HIAT:ip">"</nts>
                  <ts e="T448" id="Seg_1620" n="HIAT:w" s="T447">Učum</ts>
                  <nts id="Seg_1621" n="HIAT:ip">,</nts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1624" n="HIAT:w" s="T448">bilbetek</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1627" n="HIAT:w" s="T449">kötörbüt</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1630" n="HIAT:w" s="T450">aːsta</ts>
                  <nts id="Seg_1631" n="HIAT:ip">"</nts>
                  <nts id="Seg_1632" n="HIAT:ip">,</nts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1635" n="HIAT:w" s="T451">di͡en</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1638" n="HIAT:w" s="T452">nʼɨŋɨnaspɨttar</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1641" n="HIAT:w" s="T453">ikki</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1644" n="HIAT:w" s="T454">emeːksin</ts>
                  <nts id="Seg_1645" n="HIAT:ip">.</nts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1648" n="HIAT:u" s="T455">
                  <nts id="Seg_1649" n="HIAT:ip">"</nts>
                  <ts e="T456" id="Seg_1651" n="HIAT:w" s="T455">Oː</ts>
                  <nts id="Seg_1652" n="HIAT:ip">"</nts>
                  <nts id="Seg_1653" n="HIAT:ip">,</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1656" n="HIAT:w" s="T456">bilbetek</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1659" n="HIAT:w" s="T457">bu͡olan</ts>
                  <nts id="Seg_1660" n="HIAT:ip">,</nts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1663" n="HIAT:w" s="T458">Onoː</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1666" n="HIAT:w" s="T459">heŋeːrbit</ts>
                  <nts id="Seg_1667" n="HIAT:ip">.</nts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1670" n="HIAT:u" s="T460">
                  <nts id="Seg_1671" n="HIAT:ip">"</nts>
                  <ts e="T461" id="Seg_1673" n="HIAT:w" s="T460">Dʼe</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1676" n="HIAT:w" s="T461">kepseːŋ</ts>
                  <nts id="Seg_1677" n="HIAT:ip">,</nts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1680" n="HIAT:w" s="T462">tu͡ok</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1683" n="HIAT:w" s="T463">tüːlün</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1686" n="HIAT:w" s="T464">tüheːtigit</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1689" n="HIAT:w" s="T465">hɨrga</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1692" n="HIAT:w" s="T466">dʼi͡e</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1695" n="HIAT:w" s="T467">annɨgar</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1698" n="HIAT:w" s="T468">hɨtaŋŋɨt</ts>
                  <nts id="Seg_1699" n="HIAT:ip">?</nts>
                  <nts id="Seg_1700" n="HIAT:ip">"</nts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1703" n="HIAT:u" s="T469">
                  <nts id="Seg_1704" n="HIAT:ip">"</nts>
                  <ts e="T470" id="Seg_1706" n="HIAT:w" s="T469">Bu</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1709" n="HIAT:w" s="T470">tu͡ok</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1712" n="HIAT:w" s="T471">bu͡olar</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1715" n="HIAT:w" s="T472">ogonnʼoruj</ts>
                  <nts id="Seg_1716" n="HIAT:ip">?</nts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1719" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_1721" n="HIAT:w" s="T473">Kɨrdʼan</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1724" n="HIAT:w" s="T474">öjö</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1727" n="HIAT:w" s="T475">kɨlgɨːr</ts>
                  <nts id="Seg_1728" n="HIAT:ip">,</nts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1731" n="HIAT:w" s="T476">bɨhɨlaːk</ts>
                  <nts id="Seg_1732" n="HIAT:ip">"</nts>
                  <nts id="Seg_1733" n="HIAT:ip">,</nts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1736" n="HIAT:w" s="T477">agdaspɨttar</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1739" n="HIAT:w" s="T478">ikki</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1742" n="HIAT:w" s="T479">kuttannʼaktar</ts>
                  <nts id="Seg_1743" n="HIAT:ip">.</nts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T486" id="Seg_1746" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1748" n="HIAT:w" s="T480">Hi</ts>
                  <nts id="Seg_1749" n="HIAT:ip">,</nts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1752" n="HIAT:w" s="T481">uskaːn</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1755" n="HIAT:w" s="T482">hürektere</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1758" n="HIAT:w" s="T483">össü͡ö</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1761" n="HIAT:w" s="T484">haŋalaːk</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1764" n="HIAT:w" s="T485">bu͡olbuttar</ts>
                  <nts id="Seg_1765" n="HIAT:ip">.</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1768" n="HIAT:u" s="T486">
                  <nts id="Seg_1769" n="HIAT:ip">"</nts>
                  <ts e="T487" id="Seg_1771" n="HIAT:w" s="T486">Bilbekkit</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1774" n="HIAT:w" s="T487">du͡o</ts>
                  <nts id="Seg_1775" n="HIAT:ip">,</nts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1778" n="HIAT:w" s="T488">üje</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1781" n="HIAT:w" s="T489">ularɨjbɨtɨn</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1784" n="HIAT:w" s="T490">savʼettar</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1787" n="HIAT:w" s="T491">čɨːčaːktara</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1790" n="HIAT:w" s="T492">kelerin</ts>
                  <nts id="Seg_1791" n="HIAT:ip">?</nts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1794" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1796" n="HIAT:w" s="T493">Intigit</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1799" n="HIAT:w" s="T494">ehigini</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1802" n="HIAT:w" s="T495">abɨrɨː</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1805" n="HIAT:w" s="T496">kötö</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1808" n="HIAT:w" s="T497">hɨldʼar</ts>
                  <nts id="Seg_1809" n="HIAT:ip">.</nts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_1812" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_1814" n="HIAT:w" s="T498">Tɨ͡a</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1817" n="HIAT:w" s="T499">dʼono</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1820" n="HIAT:w" s="T500">tot</ts>
                  <nts id="Seg_1821" n="HIAT:ip">,</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1824" n="HIAT:w" s="T501">baːj</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1827" n="HIAT:w" s="T502">oloru͡oktarɨn</ts>
                  <nts id="Seg_1828" n="HIAT:ip">"</nts>
                  <nts id="Seg_1829" n="HIAT:ip">,</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1832" n="HIAT:w" s="T503">despitter</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1835" n="HIAT:w" s="T504">ikki</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1838" n="HIAT:w" s="T505">kihi</ts>
                  <nts id="Seg_1839" n="HIAT:ip">.</nts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_1842" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_1844" n="HIAT:w" s="T506">Emeːksitter</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1847" n="HIAT:w" s="T507">tu͡ok</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1850" n="HIAT:w" s="T508">daː</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1853" n="HIAT:w" s="T509">di͡ekterin</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1856" n="HIAT:w" s="T510">berditten</ts>
                  <nts id="Seg_1857" n="HIAT:ip">,</nts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1860" n="HIAT:w" s="T511">haŋata</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1863" n="HIAT:w" s="T512">hu͡ok</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1866" n="HIAT:w" s="T513">haːpput</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1869" n="HIAT:w" s="T514">kördük</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1872" n="HIAT:w" s="T515">dʼi͡elerin</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1875" n="HIAT:w" s="T516">di͡ek</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1878" n="HIAT:w" s="T517">ököhö</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1881" n="HIAT:w" s="T518">turbuttar</ts>
                  <nts id="Seg_1882" n="HIAT:ip">.</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T519" id="Seg_1884" n="sc" s="T0">
               <ts e="T1" id="Seg_1886" n="e" s="T0">Savʼettar </ts>
               <ts e="T2" id="Seg_1888" n="e" s="T1">ɨjaːktara </ts>
               <ts e="T3" id="Seg_1890" n="e" s="T2">haŋardɨː </ts>
               <ts e="T4" id="Seg_1892" n="e" s="T3">keleriger, </ts>
               <ts e="T5" id="Seg_1894" n="e" s="T4">Kurʼja </ts>
               <ts e="T6" id="Seg_1896" n="e" s="T5">di͡en </ts>
               <ts e="T7" id="Seg_1898" n="e" s="T6">olokko </ts>
               <ts e="T8" id="Seg_1900" n="e" s="T7">hajɨn </ts>
               <ts e="T9" id="Seg_1902" n="e" s="T8">aːjɨ </ts>
               <ts e="T10" id="Seg_1904" n="e" s="T9">balɨktɨː </ts>
               <ts e="T11" id="Seg_1906" n="e" s="T10">oloroːčču </ts>
               <ts e="T12" id="Seg_1908" n="e" s="T11">etilere </ts>
               <ts e="T13" id="Seg_1910" n="e" s="T12">haka </ts>
               <ts e="T14" id="Seg_1912" n="e" s="T13">dʼono. </ts>
               <ts e="T15" id="Seg_1914" n="e" s="T14">Tu͡ora </ts>
               <ts e="T16" id="Seg_1916" n="e" s="T15">karaktaːk </ts>
               <ts e="T17" id="Seg_1918" n="e" s="T16">čubu-čubu </ts>
               <ts e="T18" id="Seg_1920" n="e" s="T17">hɨldʼɨbat </ts>
               <ts e="T19" id="Seg_1922" n="e" s="T18">ologo. </ts>
               <ts e="T20" id="Seg_1924" n="e" s="T19">Onnuk </ts>
               <ts e="T21" id="Seg_1926" n="e" s="T20">ɨraːk </ts>
               <ts e="T22" id="Seg_1928" n="e" s="T21">balɨktɨːr </ts>
               <ts e="T23" id="Seg_1930" n="e" s="T22">hirdere. </ts>
               <ts e="T24" id="Seg_1932" n="e" s="T23">Kahɨ͡an </ts>
               <ts e="T25" id="Seg_1934" n="e" s="T24">ire </ts>
               <ts e="T26" id="Seg_1936" n="e" s="T25">kim </ts>
               <ts e="T27" id="Seg_1938" n="e" s="T26">eme </ts>
               <ts e="T28" id="Seg_1940" n="e" s="T27">keli͡e </ts>
               <ts e="T29" id="Seg_1942" n="e" s="T28">hajɨn </ts>
               <ts e="T30" id="Seg_1944" n="e" s="T29">čugastaːgɨ </ts>
               <ts e="T31" id="Seg_1946" n="e" s="T30">hɨtɨnnʼaktarga </ts>
               <ts e="T32" id="Seg_1948" n="e" s="T31">ös </ts>
               <ts e="T33" id="Seg_1950" n="e" s="T32">kepsiː, </ts>
               <ts e="T34" id="Seg_1952" n="e" s="T33">bejetin </ts>
               <ts e="T35" id="Seg_1954" n="e" s="T34">ologun </ts>
               <ts e="T36" id="Seg_1956" n="e" s="T35">tuhunan, </ts>
               <ts e="T37" id="Seg_1958" n="e" s="T36">bileːri </ts>
               <ts e="T38" id="Seg_1960" n="e" s="T37">kajdak </ts>
               <ts e="T39" id="Seg_1962" n="e" s="T38">balɨktɨːllarɨn </ts>
               <ts e="T40" id="Seg_1964" n="e" s="T39">Kurʼja </ts>
               <ts e="T41" id="Seg_1966" n="e" s="T40">dʼono. </ts>
               <ts e="T42" id="Seg_1968" n="e" s="T41">Biːr </ts>
               <ts e="T43" id="Seg_1970" n="e" s="T42">hajɨn </ts>
               <ts e="T44" id="Seg_1972" n="e" s="T43">ogonnʼottor </ts>
               <ts e="T45" id="Seg_1974" n="e" s="T44">erde </ts>
               <ts e="T46" id="Seg_1976" n="e" s="T45">harsi͡erda </ts>
               <ts e="T47" id="Seg_1978" n="e" s="T46">erdinen </ts>
               <ts e="T48" id="Seg_1980" n="e" s="T47">kaːlbɨttara </ts>
               <ts e="T49" id="Seg_1982" n="e" s="T48">biːskelerin </ts>
               <ts e="T50" id="Seg_1984" n="e" s="T49">körüne. </ts>
               <ts e="T51" id="Seg_1986" n="e" s="T50">Dʼaktar, </ts>
               <ts e="T52" id="Seg_1988" n="e" s="T51">ogo </ts>
               <ts e="T53" id="Seg_1990" n="e" s="T52">aːjmak </ts>
               <ts e="T54" id="Seg_1992" n="e" s="T53">dʼi͡eleriger </ts>
               <ts e="T55" id="Seg_1994" n="e" s="T54">olorbuttara. </ts>
               <ts e="T56" id="Seg_1996" n="e" s="T55">Kim </ts>
               <ts e="T57" id="Seg_1998" n="e" s="T56">balɨk </ts>
               <ts e="T58" id="Seg_2000" n="e" s="T57">ɨraːstanar, </ts>
               <ts e="T59" id="Seg_2002" n="e" s="T58">kim </ts>
               <ts e="T60" id="Seg_2004" n="e" s="T59">dʼukula </ts>
               <ts e="T61" id="Seg_2006" n="e" s="T60">kuːrdunar. </ts>
               <ts e="T62" id="Seg_2008" n="e" s="T61">Olorton </ts>
               <ts e="T63" id="Seg_2010" n="e" s="T62">ikki </ts>
               <ts e="T64" id="Seg_2012" n="e" s="T63">emeːksin, </ts>
               <ts e="T65" id="Seg_2014" n="e" s="T64">ogonnʼottor </ts>
               <ts e="T66" id="Seg_2016" n="e" s="T65">keli͡ekteriger </ts>
               <ts e="T67" id="Seg_2018" n="e" s="T66">di͡eri, </ts>
               <ts e="T68" id="Seg_2020" n="e" s="T67">hɨrga </ts>
               <ts e="T69" id="Seg_2022" n="e" s="T68">dʼi͡elerin </ts>
               <ts e="T70" id="Seg_2024" n="e" s="T69">tiriːlerin </ts>
               <ts e="T71" id="Seg_2026" n="e" s="T70">abɨraktɨːllar </ts>
               <ts e="T72" id="Seg_2028" n="e" s="T71">ebit. </ts>
               <ts e="T73" id="Seg_2030" n="e" s="T72">Kunʼaːs </ts>
               <ts e="T74" id="Seg_2032" n="e" s="T73">bagaj </ts>
               <ts e="T75" id="Seg_2034" n="e" s="T74">kün </ts>
               <ts e="T76" id="Seg_2036" n="e" s="T75">turbuta. </ts>
               <ts e="T77" id="Seg_2038" n="e" s="T76">Dudupta </ts>
               <ts e="T78" id="Seg_2040" n="e" s="T77">aːn </ts>
               <ts e="T79" id="Seg_2042" n="e" s="T78">ebelerin </ts>
               <ts e="T80" id="Seg_2044" n="e" s="T79">uːta </ts>
               <ts e="T81" id="Seg_2046" n="e" s="T80">čumputugar </ts>
               <ts e="T82" id="Seg_2048" n="e" s="T81">ürüŋ </ts>
               <ts e="T83" id="Seg_2050" n="e" s="T82">etiŋ </ts>
               <ts e="T84" id="Seg_2052" n="e" s="T83">bɨlɨttara </ts>
               <ts e="T85" id="Seg_2054" n="e" s="T84">bɨdʼɨrahan </ts>
               <ts e="T86" id="Seg_2056" n="e" s="T85">köstöllör. </ts>
               <ts e="T87" id="Seg_2058" n="e" s="T86">Kumaːr </ts>
               <ts e="T88" id="Seg_2060" n="e" s="T87">kotoku </ts>
               <ts e="T89" id="Seg_2062" n="e" s="T88">kɨnata </ts>
               <ts e="T90" id="Seg_2064" n="e" s="T89">katar </ts>
               <ts e="T91" id="Seg_2066" n="e" s="T90">kunʼaha </ts>
               <ts e="T92" id="Seg_2068" n="e" s="T91">turbuta. </ts>
               <ts e="T93" id="Seg_2070" n="e" s="T92">Emeːksittere, </ts>
               <ts e="T94" id="Seg_2072" n="e" s="T93">ol </ts>
               <ts e="T95" id="Seg_2074" n="e" s="T94">daː </ts>
               <ts e="T96" id="Seg_2076" n="e" s="T95">bu͡ollar </ts>
               <ts e="T97" id="Seg_2078" n="e" s="T96">iliːlere </ts>
               <ts e="T98" id="Seg_2080" n="e" s="T97">holoto </ts>
               <ts e="T99" id="Seg_2082" n="e" s="T98">hu͡ok, </ts>
               <ts e="T100" id="Seg_2084" n="e" s="T99">ilbineller. </ts>
               <ts e="T101" id="Seg_2086" n="e" s="T100">Ol </ts>
               <ts e="T102" id="Seg_2088" n="e" s="T101">bɨːhɨgar </ts>
               <ts e="T103" id="Seg_2090" n="e" s="T102">huptu </ts>
               <ts e="T104" id="Seg_2092" n="e" s="T103">tiktiː </ts>
               <ts e="T105" id="Seg_2094" n="e" s="T104">bu͡olbuttara. </ts>
               <ts e="T106" id="Seg_2096" n="e" s="T105">Innelerin </ts>
               <ts e="T107" id="Seg_2098" n="e" s="T106">tɨ͡aha </ts>
               <ts e="T108" id="Seg_2100" n="e" s="T107">öŋnöːk </ts>
               <ts e="T109" id="Seg_2102" n="e" s="T108">tiriːge </ts>
               <ts e="T110" id="Seg_2104" n="e" s="T109">kurdurguːra </ts>
               <ts e="T111" id="Seg_2106" n="e" s="T110">ɨraːktan </ts>
               <ts e="T112" id="Seg_2108" n="e" s="T111">ihiller. </ts>
               <ts e="T113" id="Seg_2110" n="e" s="T112">Ol </ts>
               <ts e="T114" id="Seg_2112" n="e" s="T113">ülelene </ts>
               <ts e="T115" id="Seg_2114" n="e" s="T114">olordoktoruna, </ts>
               <ts e="T116" id="Seg_2116" n="e" s="T115">hirdere </ts>
               <ts e="T117" id="Seg_2118" n="e" s="T116">togo </ts>
               <ts e="T118" id="Seg_2120" n="e" s="T117">ere </ts>
               <ts e="T119" id="Seg_2122" n="e" s="T118">nʼirgijer </ts>
               <ts e="T120" id="Seg_2124" n="e" s="T119">ebit. </ts>
               <ts e="T121" id="Seg_2126" n="e" s="T120">"Be", </ts>
               <ts e="T122" id="Seg_2128" n="e" s="T121">di͡ebit </ts>
               <ts e="T123" id="Seg_2130" n="e" s="T122">biːrgehe. </ts>
               <ts e="T124" id="Seg_2132" n="e" s="T123">"Togo </ts>
               <ts e="T125" id="Seg_2134" n="e" s="T124">hirbit </ts>
               <ts e="T126" id="Seg_2136" n="e" s="T125">nʼirgijer? </ts>
               <ts e="T127" id="Seg_2138" n="e" s="T126">Togo </ts>
               <ts e="T128" id="Seg_2140" n="e" s="T127">etiŋ </ts>
               <ts e="T129" id="Seg_2142" n="e" s="T128">tɨ͡aha </ts>
               <ts e="T130" id="Seg_2144" n="e" s="T129">itigirdik </ts>
               <ts e="T131" id="Seg_2146" n="e" s="T130">bu͡olu͡oj? </ts>
               <ts e="T132" id="Seg_2148" n="e" s="T131">Tu͡ok </ts>
               <ts e="T133" id="Seg_2150" n="e" s="T132">ere </ts>
               <ts e="T134" id="Seg_2152" n="e" s="T133">diktite </ts>
               <ts e="T135" id="Seg_2154" n="e" s="T134">bu͡olla, </ts>
               <ts e="T136" id="Seg_2156" n="e" s="T135">ihilleː, </ts>
               <ts e="T137" id="Seg_2158" n="e" s="T136">emeːksin." </ts>
               <ts e="T138" id="Seg_2160" n="e" s="T137">Krʼistʼin, </ts>
               <ts e="T139" id="Seg_2162" n="e" s="T138">kɨrdʼagas, </ts>
               <ts e="T140" id="Seg_2164" n="e" s="T139">aha </ts>
               <ts e="T141" id="Seg_2166" n="e" s="T140">bɨldʼaːččɨ </ts>
               <ts e="T142" id="Seg_2168" n="e" s="T141">gɨlbajbɨt </ts>
               <ts e="T143" id="Seg_2170" n="e" s="T142">emeːksin, </ts>
               <ts e="T144" id="Seg_2172" n="e" s="T143">u͡ohugar </ts>
               <ts e="T145" id="Seg_2174" n="e" s="T144">biːr </ts>
               <ts e="T146" id="Seg_2176" n="e" s="T145">da </ts>
               <ts e="T147" id="Seg_2178" n="e" s="T146">tiːhe </ts>
               <ts e="T148" id="Seg_2180" n="e" s="T147">hu͡ok, </ts>
               <ts e="T149" id="Seg_2182" n="e" s="T148">ös </ts>
               <ts e="T150" id="Seg_2184" n="e" s="T149">kiːrbet </ts>
               <ts e="T151" id="Seg_2186" n="e" s="T150">pɨlaːtɨn </ts>
               <ts e="T152" id="Seg_2188" n="e" s="T151">menʼiːtitten </ts>
               <ts e="T153" id="Seg_2190" n="e" s="T152">hulbu </ts>
               <ts e="T154" id="Seg_2192" n="e" s="T153">tardaːt </ts>
               <ts e="T155" id="Seg_2194" n="e" s="T154">ihilleːbit. </ts>
               <ts e="T156" id="Seg_2196" n="e" s="T155">Ol </ts>
               <ts e="T520" id="Seg_2198" n="e" s="T156">((…)), </ts>
               <ts e="T157" id="Seg_2200" n="e" s="T520">ikki </ts>
               <ts e="T158" id="Seg_2202" n="e" s="T157">emeːksin </ts>
               <ts e="T159" id="Seg_2204" n="e" s="T158">kallaːn </ts>
               <ts e="T160" id="Seg_2206" n="e" s="T159">di͡ek </ts>
               <ts e="T161" id="Seg_2208" n="e" s="T160">nʼɨlajan </ts>
               <ts e="T162" id="Seg_2210" n="e" s="T161">baraːn, </ts>
               <ts e="T163" id="Seg_2212" n="e" s="T162">üːrpüt </ts>
               <ts e="T164" id="Seg_2214" n="e" s="T163">taba </ts>
               <ts e="T165" id="Seg_2216" n="e" s="T164">kördük, </ts>
               <ts e="T166" id="Seg_2218" n="e" s="T165">nʼirgijer </ts>
               <ts e="T167" id="Seg_2220" n="e" s="T166">tɨ͡ahɨ </ts>
               <ts e="T168" id="Seg_2222" n="e" s="T167">ihilleːn </ts>
               <ts e="T170" id="Seg_2224" n="e" s="T168">bu͡olbuttar. </ts>
               <ts e="T171" id="Seg_2226" n="e" s="T170">Kumaːr </ts>
               <ts e="T172" id="Seg_2228" n="e" s="T171">daː </ts>
               <ts e="T173" id="Seg_2230" n="e" s="T172">hiːrin </ts>
               <ts e="T174" id="Seg_2232" n="e" s="T173">bilbekke, </ts>
               <ts e="T175" id="Seg_2234" n="e" s="T174">örköːn </ts>
               <ts e="T176" id="Seg_2236" n="e" s="T175">olorbuttar, </ts>
               <ts e="T177" id="Seg_2238" n="e" s="T176">anʼaktarɨn </ts>
               <ts e="T178" id="Seg_2240" n="e" s="T177">atan </ts>
               <ts e="T179" id="Seg_2242" n="e" s="T178">baraːn. </ts>
               <ts e="T180" id="Seg_2244" n="e" s="T179">Attɨlarɨgar </ts>
               <ts e="T181" id="Seg_2246" n="e" s="T180">tiriːler </ts>
               <ts e="T182" id="Seg_2248" n="e" s="T181">loskutu͡oktara </ts>
               <ts e="T183" id="Seg_2250" n="e" s="T182">onno-manna </ts>
               <ts e="T184" id="Seg_2252" n="e" s="T183">hɨtallar. </ts>
               <ts e="T185" id="Seg_2254" n="e" s="T184">Bɨ͡annan </ts>
               <ts e="T186" id="Seg_2256" n="e" s="T185">baːjɨːlaːk </ts>
               <ts e="T187" id="Seg_2258" n="e" s="T186">mordʼoːloro </ts>
               <ts e="T188" id="Seg_2260" n="e" s="T187">iliːleriger </ts>
               <ts e="T189" id="Seg_2262" n="e" s="T188">buru͡olahallar. </ts>
               <ts e="T190" id="Seg_2264" n="e" s="T189">Küːppüt </ts>
               <ts e="T191" id="Seg_2266" n="e" s="T190">kördük </ts>
               <ts e="T192" id="Seg_2268" n="e" s="T191">sɨp-sɨp </ts>
               <ts e="T193" id="Seg_2270" n="e" s="T192">kahan </ts>
               <ts e="T194" id="Seg_2272" n="e" s="T193">oloru͡oktarɨn. </ts>
               <ts e="T195" id="Seg_2274" n="e" s="T194">Kirdik, </ts>
               <ts e="T196" id="Seg_2276" n="e" s="T195">ginner </ts>
               <ts e="T197" id="Seg_2278" n="e" s="T196">bilbetek </ts>
               <ts e="T198" id="Seg_2280" n="e" s="T197">tɨ͡astara </ts>
               <ts e="T199" id="Seg_2282" n="e" s="T198">bu͡olbut. </ts>
               <ts e="T200" id="Seg_2284" n="e" s="T199">"Kaja </ts>
               <ts e="T201" id="Seg_2286" n="e" s="T200">da, </ts>
               <ts e="T202" id="Seg_2288" n="e" s="T201">dʼe </ts>
               <ts e="T203" id="Seg_2290" n="e" s="T202">öllübüt", </ts>
               <ts e="T204" id="Seg_2292" n="e" s="T203">diː </ts>
               <ts e="T205" id="Seg_2294" n="e" s="T204">tüheːt </ts>
               <ts e="T206" id="Seg_2296" n="e" s="T205">biːrges </ts>
               <ts e="T207" id="Seg_2298" n="e" s="T206">emeːksin </ts>
               <ts e="T208" id="Seg_2300" n="e" s="T207">tura </ts>
               <ts e="T209" id="Seg_2302" n="e" s="T208">ekkireːbit, </ts>
               <ts e="T210" id="Seg_2304" n="e" s="T209">mas </ts>
               <ts e="T211" id="Seg_2306" n="e" s="T210">di͡ek </ts>
               <ts e="T212" id="Seg_2308" n="e" s="T211">körüleːbit. </ts>
               <ts e="T213" id="Seg_2310" n="e" s="T212">"Dʼe, </ts>
               <ts e="T214" id="Seg_2312" n="e" s="T213">kötön </ts>
               <ts e="T215" id="Seg_2314" n="e" s="T214">iher </ts>
               <ts e="T216" id="Seg_2316" n="e" s="T215">uluː </ts>
               <ts e="T217" id="Seg_2318" n="e" s="T216">kötörö. </ts>
               <ts e="T218" id="Seg_2320" n="e" s="T217">Kör, </ts>
               <ts e="T219" id="Seg_2322" n="e" s="T218">ol </ts>
               <ts e="T220" id="Seg_2324" n="e" s="T219">kɨtaran </ts>
               <ts e="T221" id="Seg_2326" n="e" s="T220">iher, </ts>
               <ts e="T222" id="Seg_2328" n="e" s="T221">gini </ts>
               <ts e="T223" id="Seg_2330" n="e" s="T222">sihin </ts>
               <ts e="T224" id="Seg_2332" n="e" s="T223">tɨ͡aha, </ts>
               <ts e="T225" id="Seg_2334" n="e" s="T224">ebit. </ts>
               <ts e="T226" id="Seg_2336" n="e" s="T225">Xoːspodi, </ts>
               <ts e="T227" id="Seg_2338" n="e" s="T226">min </ts>
               <ts e="T228" id="Seg_2340" n="e" s="T227">haŋarbatagɨm, </ts>
               <ts e="T229" id="Seg_2342" n="e" s="T228">körbötögüm </ts>
               <ts e="T230" id="Seg_2344" n="e" s="T229">bu͡ollun", </ts>
               <ts e="T231" id="Seg_2346" n="e" s="T230">di͡en </ts>
               <ts e="T232" id="Seg_2348" n="e" s="T231">emeːksin </ts>
               <ts e="T233" id="Seg_2350" n="e" s="T232">labɨjan </ts>
               <ts e="T234" id="Seg_2352" n="e" s="T233">barbɨt. </ts>
               <ts e="T235" id="Seg_2354" n="e" s="T234">"O, </ts>
               <ts e="T236" id="Seg_2356" n="e" s="T235">tu͡ok </ts>
               <ts e="T237" id="Seg_2358" n="e" s="T236">kötörün </ts>
               <ts e="T238" id="Seg_2360" n="e" s="T237">gɨnagɨn", </ts>
               <ts e="T239" id="Seg_2362" n="e" s="T238">diː-diː </ts>
               <ts e="T240" id="Seg_2364" n="e" s="T239">dogoro </ts>
               <ts e="T241" id="Seg_2366" n="e" s="T240">emi͡e </ts>
               <ts e="T242" id="Seg_2368" n="e" s="T241">körüleːbit. </ts>
               <ts e="T243" id="Seg_2370" n="e" s="T242">Ataktarɨn </ts>
               <ts e="T244" id="Seg_2372" n="e" s="T243">annɨlarɨgar </ts>
               <ts e="T245" id="Seg_2374" n="e" s="T244">hirdere </ts>
               <ts e="T246" id="Seg_2376" n="e" s="T245">möŋör </ts>
               <ts e="T247" id="Seg_2378" n="e" s="T246">kördük </ts>
               <ts e="T248" id="Seg_2380" n="e" s="T247">bu͡olbut. </ts>
               <ts e="T249" id="Seg_2382" n="e" s="T248">Urahalar </ts>
               <ts e="T250" id="Seg_2384" n="e" s="T249">titireːbitter. </ts>
               <ts e="T251" id="Seg_2386" n="e" s="T250">"Türgennik, </ts>
               <ts e="T252" id="Seg_2388" n="e" s="T251">hɨrga </ts>
               <ts e="T253" id="Seg_2390" n="e" s="T252">di͡ek </ts>
               <ts e="T254" id="Seg_2392" n="e" s="T253">annɨtɨgar", </ts>
               <ts e="T255" id="Seg_2394" n="e" s="T254">diː </ts>
               <ts e="T256" id="Seg_2396" n="e" s="T255">tüheːt, </ts>
               <ts e="T257" id="Seg_2398" n="e" s="T256">biːrgehe </ts>
               <ts e="T258" id="Seg_2400" n="e" s="T257">čip </ts>
               <ts e="T259" id="Seg_2402" n="e" s="T258">gɨmmɨt </ts>
               <ts e="T260" id="Seg_2404" n="e" s="T259">katɨːlardaːk </ts>
               <ts e="T261" id="Seg_2406" n="e" s="T260">mu͡osta </ts>
               <ts e="T262" id="Seg_2408" n="e" s="T261">annɨgar, </ts>
               <ts e="T263" id="Seg_2410" n="e" s="T262">dogoro </ts>
               <ts e="T264" id="Seg_2412" n="e" s="T263">ginini </ts>
               <ts e="T265" id="Seg_2414" n="e" s="T264">köŋüjen </ts>
               <ts e="T266" id="Seg_2416" n="e" s="T265">kiːrbit </ts>
               <ts e="T267" id="Seg_2418" n="e" s="T266">bu͡olagɨn </ts>
               <ts e="T268" id="Seg_2420" n="e" s="T267">annɨgar. </ts>
               <ts e="T269" id="Seg_2422" n="e" s="T268">Ol </ts>
               <ts e="T270" id="Seg_2424" n="e" s="T269">kem, </ts>
               <ts e="T271" id="Seg_2426" n="e" s="T270">lü͡ötčik </ts>
               <ts e="T272" id="Seg_2428" n="e" s="T271">haŋardɨː </ts>
               <ts e="T273" id="Seg_2430" n="e" s="T272">uraha </ts>
               <ts e="T274" id="Seg_2432" n="e" s="T273">dʼi͡eleri </ts>
               <ts e="T275" id="Seg_2434" n="e" s="T274">körö </ts>
               <ts e="T276" id="Seg_2436" n="e" s="T275">hotoru, </ts>
               <ts e="T277" id="Seg_2438" n="e" s="T276">nʼamčɨgas </ts>
               <ts e="T278" id="Seg_2440" n="e" s="T277">bagajɨ </ts>
               <ts e="T279" id="Seg_2442" n="e" s="T278">kötön </ts>
               <ts e="T280" id="Seg_2444" n="e" s="T279">aːspɨt. </ts>
               <ts e="T281" id="Seg_2446" n="e" s="T280">Hɨrga </ts>
               <ts e="T282" id="Seg_2448" n="e" s="T281">dʼi͡eni </ts>
               <ts e="T283" id="Seg_2450" n="e" s="T282">gɨtta </ts>
               <ts e="T284" id="Seg_2452" n="e" s="T283">bu͡olak </ts>
               <ts e="T285" id="Seg_2454" n="e" s="T284">annɨlarɨttan </ts>
               <ts e="T287" id="Seg_2456" n="e" s="T285">(hiːp-) </ts>
               <ts e="T288" id="Seg_2458" n="e" s="T287">emeːksitter </ts>
               <ts e="T289" id="Seg_2460" n="e" s="T288">harɨː </ts>
               <ts e="T290" id="Seg_2462" n="e" s="T289">ataktara </ts>
               <ts e="T291" id="Seg_2464" n="e" s="T290">mömpökkö </ts>
               <ts e="T292" id="Seg_2466" n="e" s="T291">čoroho </ts>
               <ts e="T293" id="Seg_2468" n="e" s="T292">hɨppɨttar. </ts>
               <ts e="T294" id="Seg_2470" n="e" s="T293">Kɨtɨlga </ts>
               <ts e="T295" id="Seg_2472" n="e" s="T294">ogonnʼottor </ts>
               <ts e="T296" id="Seg_2474" n="e" s="T295">ekkiriː-ekkiriː </ts>
               <ts e="T297" id="Seg_2476" n="e" s="T296">bergehelerin </ts>
               <ts e="T298" id="Seg_2478" n="e" s="T297">samalʼokka </ts>
               <ts e="T299" id="Seg_2480" n="e" s="T298">dajbɨː </ts>
               <ts e="T300" id="Seg_2482" n="e" s="T299">turbuttar. </ts>
               <ts e="T301" id="Seg_2484" n="e" s="T300">Ginner </ts>
               <ts e="T302" id="Seg_2486" n="e" s="T301">ü͡örüːlere, </ts>
               <ts e="T303" id="Seg_2488" n="e" s="T302">haŋardɨː </ts>
               <ts e="T304" id="Seg_2490" n="e" s="T303">samalʼot </ts>
               <ts e="T305" id="Seg_2492" n="e" s="T304">keleriger </ts>
               <ts e="T306" id="Seg_2494" n="e" s="T305">mu͡oraga </ts>
               <ts e="T307" id="Seg_2496" n="e" s="T306">bert </ts>
               <ts e="T308" id="Seg_2498" n="e" s="T307">ebit. </ts>
               <ts e="T309" id="Seg_2500" n="e" s="T308">Bileller, </ts>
               <ts e="T310" id="Seg_2502" n="e" s="T309">savʼetskʼij </ts>
               <ts e="T311" id="Seg_2504" n="e" s="T310">vlastʼ </ts>
               <ts e="T312" id="Seg_2506" n="e" s="T311">bilser </ts>
               <ts e="T313" id="Seg_2508" n="e" s="T312">bu͡ola </ts>
               <ts e="T314" id="Seg_2510" n="e" s="T313">hɨldʼɨ͡aga, </ts>
               <ts e="T315" id="Seg_2512" n="e" s="T314">kajtak </ts>
               <ts e="T316" id="Seg_2514" n="e" s="T315">tabahɨttar, </ts>
               <ts e="T317" id="Seg_2516" n="e" s="T316">balɨkčɨttar, </ts>
               <ts e="T318" id="Seg_2518" n="e" s="T317">olorollorun </ts>
               <ts e="T319" id="Seg_2520" n="e" s="T318">köröːrü. </ts>
               <ts e="T320" id="Seg_2522" n="e" s="T319">Hɨːr </ts>
               <ts e="T321" id="Seg_2524" n="e" s="T320">ürdüger </ts>
               <ts e="T322" id="Seg_2526" n="e" s="T321">taksaːt </ts>
               <ts e="T323" id="Seg_2528" n="e" s="T322">oduːlammɨttar– </ts>
               <ts e="T324" id="Seg_2530" n="e" s="T323">uraha </ts>
               <ts e="T325" id="Seg_2532" n="e" s="T324">dʼi͡elere </ts>
               <ts e="T326" id="Seg_2534" n="e" s="T325">buru͡olaːbattar. </ts>
               <ts e="T327" id="Seg_2536" n="e" s="T326">Ti͡ergeːn </ts>
               <ts e="T328" id="Seg_2538" n="e" s="T327">ürdünen </ts>
               <ts e="T330" id="Seg_2540" n="e" s="T328">(kim-) </ts>
               <ts e="T331" id="Seg_2542" n="e" s="T330">im </ts>
               <ts e="T332" id="Seg_2544" n="e" s="T331">dʼɨlɨččɨ. </ts>
               <ts e="T333" id="Seg_2546" n="e" s="T332">ɨt </ts>
               <ts e="T334" id="Seg_2548" n="e" s="T333">da </ts>
               <ts e="T335" id="Seg_2550" n="e" s="T334">ürbet, </ts>
               <ts e="T336" id="Seg_2552" n="e" s="T335">kihi </ts>
               <ts e="T337" id="Seg_2554" n="e" s="T336">da </ts>
               <ts e="T338" id="Seg_2556" n="e" s="T337">haŋata </ts>
               <ts e="T339" id="Seg_2558" n="e" s="T338">ihillibet. </ts>
               <ts e="T340" id="Seg_2560" n="e" s="T339">"Tu͡ok </ts>
               <ts e="T341" id="Seg_2562" n="e" s="T340">dʼonor </ts>
               <ts e="T342" id="Seg_2564" n="e" s="T341">bu </ts>
               <ts e="T343" id="Seg_2566" n="e" s="T342">emeːksitter? </ts>
               <ts e="T344" id="Seg_2568" n="e" s="T343">Bilbetter </ts>
               <ts e="T345" id="Seg_2570" n="e" s="T344">du͡o </ts>
               <ts e="T346" id="Seg_2572" n="e" s="T345">bihigi </ts>
               <ts e="T347" id="Seg_2574" n="e" s="T346">keli͡ekpitin", </ts>
               <ts e="T348" id="Seg_2576" n="e" s="T347">di͡ebit </ts>
               <ts e="T349" id="Seg_2578" n="e" s="T348">Onoː </ts>
               <ts e="T350" id="Seg_2580" n="e" s="T349">ogonnʼor. </ts>
               <ts e="T351" id="Seg_2582" n="e" s="T350">"Kanna </ts>
               <ts e="T352" id="Seg_2584" n="e" s="T351">baːrgɨtɨːj, </ts>
               <ts e="T353" id="Seg_2586" n="e" s="T352">dʼi͡eleːkter", </ts>
               <ts e="T355" id="Seg_2588" n="e" s="T353">(ü͡ög-) </ts>
               <ts e="T356" id="Seg_2590" n="e" s="T355">ü͡ögüleːbit </ts>
               <ts e="T357" id="Seg_2592" n="e" s="T356">ɨ͡al </ts>
               <ts e="T358" id="Seg_2594" n="e" s="T357">kihite. </ts>
               <ts e="T359" id="Seg_2596" n="e" s="T358">"Ara, </ts>
               <ts e="T360" id="Seg_2598" n="e" s="T359">abɨraːŋ, </ts>
               <ts e="T361" id="Seg_2600" n="e" s="T360">araːrɨŋ, </ts>
               <ts e="T362" id="Seg_2602" n="e" s="T361">öllübüt", </ts>
               <ts e="T363" id="Seg_2604" n="e" s="T362">hir </ts>
               <ts e="T364" id="Seg_2606" n="e" s="T363">ihitten </ts>
               <ts e="T365" id="Seg_2608" n="e" s="T364">ü͡ögüː </ts>
               <ts e="T366" id="Seg_2610" n="e" s="T365">ihillibit. </ts>
               <ts e="T367" id="Seg_2612" n="e" s="T366">"Tu͡oktarɨj, </ts>
               <ts e="T368" id="Seg_2614" n="e" s="T367">iːrbitter </ts>
               <ts e="T369" id="Seg_2616" n="e" s="T368">du͡o", </ts>
               <ts e="T370" id="Seg_2618" n="e" s="T369">diː </ts>
               <ts e="T371" id="Seg_2620" n="e" s="T370">tüheːt </ts>
               <ts e="T372" id="Seg_2622" n="e" s="T371">Onoː </ts>
               <ts e="T373" id="Seg_2624" n="e" s="T372">körüleːbit. </ts>
               <ts e="T374" id="Seg_2626" n="e" s="T373">Gini </ts>
               <ts e="T375" id="Seg_2628" n="e" s="T374">hɨrga </ts>
               <ts e="T376" id="Seg_2630" n="e" s="T375">dʼi͡eler </ts>
               <ts e="T377" id="Seg_2632" n="e" s="T376">annɨlarɨgar </ts>
               <ts e="T378" id="Seg_2634" n="e" s="T377">körüleːbit. </ts>
               <ts e="T379" id="Seg_2636" n="e" s="T378">Tu͡ok </ts>
               <ts e="T380" id="Seg_2638" n="e" s="T379">diktite </ts>
               <ts e="T381" id="Seg_2640" n="e" s="T380">bu͡olla? </ts>
               <ts e="T382" id="Seg_2642" n="e" s="T381">Emeːksitterin </ts>
               <ts e="T383" id="Seg_2644" n="e" s="T382">ataktara </ts>
               <ts e="T384" id="Seg_2646" n="e" s="T383">onno </ts>
               <ts e="T385" id="Seg_2648" n="e" s="T384">halɨbɨraha </ts>
               <ts e="T386" id="Seg_2650" n="e" s="T385">hɨppɨttar. </ts>
               <ts e="T387" id="Seg_2652" n="e" s="T386">Kördögüne, </ts>
               <ts e="T388" id="Seg_2654" n="e" s="T387">taksa </ts>
               <ts e="T389" id="Seg_2656" n="e" s="T388">hatɨːllar, </ts>
               <ts e="T390" id="Seg_2658" n="e" s="T389">bɨhɨlaːk. </ts>
               <ts e="T391" id="Seg_2660" n="e" s="T390">Katɨːlar </ts>
               <ts e="T392" id="Seg_2662" n="e" s="T391">taŋastarɨttan </ts>
               <ts e="T393" id="Seg_2664" n="e" s="T392">iːlbitter. </ts>
               <ts e="T394" id="Seg_2666" n="e" s="T393">Togo </ts>
               <ts e="T395" id="Seg_2668" n="e" s="T394">ɨːtɨ͡aktaraj? </ts>
               <ts e="T396" id="Seg_2670" n="e" s="T395">Iske </ts>
               <ts e="T397" id="Seg_2672" n="e" s="T396">da </ts>
               <ts e="T398" id="Seg_2674" n="e" s="T397">kiːllerbetter, </ts>
               <ts e="T399" id="Seg_2676" n="e" s="T398">taska </ts>
               <ts e="T400" id="Seg_2678" n="e" s="T399">da </ts>
               <ts e="T401" id="Seg_2680" n="e" s="T400">tahaːrbattar. </ts>
               <ts e="T402" id="Seg_2682" n="e" s="T401">"Ara" </ts>
               <ts e="T403" id="Seg_2684" n="e" s="T402">dehiː </ts>
               <ts e="T404" id="Seg_2686" n="e" s="T403">elbeːn </ts>
               <ts e="T405" id="Seg_2688" n="e" s="T404">ispit, </ts>
               <ts e="T406" id="Seg_2690" n="e" s="T405">ɨnčɨktɨːr </ts>
               <ts e="T407" id="Seg_2692" n="e" s="T406">kojdubut. </ts>
               <ts e="T408" id="Seg_2694" n="e" s="T407">Ogonnʼottor </ts>
               <ts e="T409" id="Seg_2696" n="e" s="T408">tu͡ok </ts>
               <ts e="T410" id="Seg_2698" n="e" s="T409">da </ts>
               <ts e="T411" id="Seg_2700" n="e" s="T410">di͡ek </ts>
               <ts e="T412" id="Seg_2702" n="e" s="T411">berditten, </ts>
               <ts e="T413" id="Seg_2704" n="e" s="T412">sögö </ts>
               <ts e="T414" id="Seg_2706" n="e" s="T413">hataːn, </ts>
               <ts e="T415" id="Seg_2708" n="e" s="T414">tugu </ts>
               <ts e="T416" id="Seg_2710" n="e" s="T415">da </ts>
               <ts e="T417" id="Seg_2712" n="e" s="T416">haŋarbat </ts>
               <ts e="T418" id="Seg_2714" n="e" s="T417">bu͡olbuttar. </ts>
               <ts e="T419" id="Seg_2716" n="e" s="T418">Emeːksitteri </ts>
               <ts e="T420" id="Seg_2718" n="e" s="T419">biːrdiː </ts>
               <ts e="T421" id="Seg_2720" n="e" s="T420">boskoloːbuttar. </ts>
               <ts e="T422" id="Seg_2722" n="e" s="T421">Ginnerge </ts>
               <ts e="T423" id="Seg_2724" n="e" s="T422">hɨraj </ts>
               <ts e="T424" id="Seg_2726" n="e" s="T423">keli͡e </ts>
               <ts e="T425" id="Seg_2728" n="e" s="T424">du͡o. </ts>
               <ts e="T426" id="Seg_2730" n="e" s="T425">Astara </ts>
               <ts e="T427" id="Seg_2732" n="e" s="T426">karaktarɨn </ts>
               <ts e="T428" id="Seg_2734" n="e" s="T427">happɨt, </ts>
               <ts e="T429" id="Seg_2736" n="e" s="T428">harɨː </ts>
               <ts e="T430" id="Seg_2738" n="e" s="T429">ataktara </ts>
               <ts e="T431" id="Seg_2740" n="e" s="T430">nʼɨpkaččɨ </ts>
               <ts e="T432" id="Seg_2742" n="e" s="T431">tüspütter. </ts>
               <ts e="T433" id="Seg_2744" n="e" s="T432">Dʼühünnere </ts>
               <ts e="T434" id="Seg_2746" n="e" s="T433">ularɨjbɨt, </ts>
               <ts e="T435" id="Seg_2748" n="e" s="T434">bu͡ortan </ts>
               <ts e="T436" id="Seg_2750" n="e" s="T435">taksɨbɨt </ts>
               <ts e="T437" id="Seg_2752" n="e" s="T436">kɨrsa </ts>
               <ts e="T438" id="Seg_2754" n="e" s="T437">ogolorun </ts>
               <ts e="T439" id="Seg_2756" n="e" s="T438">kördük </ts>
               <ts e="T440" id="Seg_2758" n="e" s="T439">bu͡olbuttar. </ts>
               <ts e="T441" id="Seg_2760" n="e" s="T440">"Kaja, </ts>
               <ts e="T442" id="Seg_2762" n="e" s="T441">tu͡ok </ts>
               <ts e="T443" id="Seg_2764" n="e" s="T442">bu͡olbukkutuj", </ts>
               <ts e="T444" id="Seg_2766" n="e" s="T443">di͡en </ts>
               <ts e="T445" id="Seg_2768" n="e" s="T444">ɨjɨppɨttar </ts>
               <ts e="T446" id="Seg_2770" n="e" s="T445">biːrdiː </ts>
               <ts e="T447" id="Seg_2772" n="e" s="T446">duruk. </ts>
               <ts e="T448" id="Seg_2774" n="e" s="T447">"Učum, </ts>
               <ts e="T449" id="Seg_2776" n="e" s="T448">bilbetek </ts>
               <ts e="T450" id="Seg_2778" n="e" s="T449">kötörbüt </ts>
               <ts e="T451" id="Seg_2780" n="e" s="T450">aːsta", </ts>
               <ts e="T452" id="Seg_2782" n="e" s="T451">di͡en </ts>
               <ts e="T453" id="Seg_2784" n="e" s="T452">nʼɨŋɨnaspɨttar </ts>
               <ts e="T454" id="Seg_2786" n="e" s="T453">ikki </ts>
               <ts e="T455" id="Seg_2788" n="e" s="T454">emeːksin. </ts>
               <ts e="T456" id="Seg_2790" n="e" s="T455">"Oː", </ts>
               <ts e="T457" id="Seg_2792" n="e" s="T456">bilbetek </ts>
               <ts e="T458" id="Seg_2794" n="e" s="T457">bu͡olan, </ts>
               <ts e="T459" id="Seg_2796" n="e" s="T458">Onoː </ts>
               <ts e="T460" id="Seg_2798" n="e" s="T459">heŋeːrbit. </ts>
               <ts e="T461" id="Seg_2800" n="e" s="T460">"Dʼe </ts>
               <ts e="T462" id="Seg_2802" n="e" s="T461">kepseːŋ, </ts>
               <ts e="T463" id="Seg_2804" n="e" s="T462">tu͡ok </ts>
               <ts e="T464" id="Seg_2806" n="e" s="T463">tüːlün </ts>
               <ts e="T465" id="Seg_2808" n="e" s="T464">tüheːtigit </ts>
               <ts e="T466" id="Seg_2810" n="e" s="T465">hɨrga </ts>
               <ts e="T467" id="Seg_2812" n="e" s="T466">dʼi͡e </ts>
               <ts e="T468" id="Seg_2814" n="e" s="T467">annɨgar </ts>
               <ts e="T469" id="Seg_2816" n="e" s="T468">hɨtaŋŋɨt?" </ts>
               <ts e="T470" id="Seg_2818" n="e" s="T469">"Bu </ts>
               <ts e="T471" id="Seg_2820" n="e" s="T470">tu͡ok </ts>
               <ts e="T472" id="Seg_2822" n="e" s="T471">bu͡olar </ts>
               <ts e="T473" id="Seg_2824" n="e" s="T472">ogonnʼoruj? </ts>
               <ts e="T474" id="Seg_2826" n="e" s="T473">Kɨrdʼan </ts>
               <ts e="T475" id="Seg_2828" n="e" s="T474">öjö </ts>
               <ts e="T476" id="Seg_2830" n="e" s="T475">kɨlgɨːr, </ts>
               <ts e="T477" id="Seg_2832" n="e" s="T476">bɨhɨlaːk", </ts>
               <ts e="T478" id="Seg_2834" n="e" s="T477">agdaspɨttar </ts>
               <ts e="T479" id="Seg_2836" n="e" s="T478">ikki </ts>
               <ts e="T480" id="Seg_2838" n="e" s="T479">kuttannʼaktar. </ts>
               <ts e="T481" id="Seg_2840" n="e" s="T480">Hi, </ts>
               <ts e="T482" id="Seg_2842" n="e" s="T481">uskaːn </ts>
               <ts e="T483" id="Seg_2844" n="e" s="T482">hürektere </ts>
               <ts e="T484" id="Seg_2846" n="e" s="T483">össü͡ö </ts>
               <ts e="T485" id="Seg_2848" n="e" s="T484">haŋalaːk </ts>
               <ts e="T486" id="Seg_2850" n="e" s="T485">bu͡olbuttar. </ts>
               <ts e="T487" id="Seg_2852" n="e" s="T486">"Bilbekkit </ts>
               <ts e="T488" id="Seg_2854" n="e" s="T487">du͡o, </ts>
               <ts e="T489" id="Seg_2856" n="e" s="T488">üje </ts>
               <ts e="T490" id="Seg_2858" n="e" s="T489">ularɨjbɨtɨn </ts>
               <ts e="T491" id="Seg_2860" n="e" s="T490">savʼettar </ts>
               <ts e="T492" id="Seg_2862" n="e" s="T491">čɨːčaːktara </ts>
               <ts e="T493" id="Seg_2864" n="e" s="T492">kelerin? </ts>
               <ts e="T494" id="Seg_2866" n="e" s="T493">Intigit </ts>
               <ts e="T495" id="Seg_2868" n="e" s="T494">ehigini </ts>
               <ts e="T496" id="Seg_2870" n="e" s="T495">abɨrɨː </ts>
               <ts e="T497" id="Seg_2872" n="e" s="T496">kötö </ts>
               <ts e="T498" id="Seg_2874" n="e" s="T497">hɨldʼar. </ts>
               <ts e="T499" id="Seg_2876" n="e" s="T498">Tɨ͡a </ts>
               <ts e="T500" id="Seg_2878" n="e" s="T499">dʼono </ts>
               <ts e="T501" id="Seg_2880" n="e" s="T500">tot, </ts>
               <ts e="T502" id="Seg_2882" n="e" s="T501">baːj </ts>
               <ts e="T503" id="Seg_2884" n="e" s="T502">oloru͡oktarɨn", </ts>
               <ts e="T504" id="Seg_2886" n="e" s="T503">despitter </ts>
               <ts e="T505" id="Seg_2888" n="e" s="T504">ikki </ts>
               <ts e="T506" id="Seg_2890" n="e" s="T505">kihi. </ts>
               <ts e="T507" id="Seg_2892" n="e" s="T506">Emeːksitter </ts>
               <ts e="T508" id="Seg_2894" n="e" s="T507">tu͡ok </ts>
               <ts e="T509" id="Seg_2896" n="e" s="T508">daː </ts>
               <ts e="T510" id="Seg_2898" n="e" s="T509">di͡ekterin </ts>
               <ts e="T511" id="Seg_2900" n="e" s="T510">berditten, </ts>
               <ts e="T512" id="Seg_2902" n="e" s="T511">haŋata </ts>
               <ts e="T513" id="Seg_2904" n="e" s="T512">hu͡ok </ts>
               <ts e="T514" id="Seg_2906" n="e" s="T513">haːpput </ts>
               <ts e="T515" id="Seg_2908" n="e" s="T514">kördük </ts>
               <ts e="T516" id="Seg_2910" n="e" s="T515">dʼi͡elerin </ts>
               <ts e="T517" id="Seg_2912" n="e" s="T516">di͡ek </ts>
               <ts e="T518" id="Seg_2914" n="e" s="T517">ököhö </ts>
               <ts e="T519" id="Seg_2916" n="e" s="T518">turbuttar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T14" id="Seg_2917" s="T0">PoNA_1970_NeverSeenBird_nar.001 (001.001)</ta>
            <ta e="T19" id="Seg_2918" s="T14">PoNA_1970_NeverSeenBird_nar.002 (001.002)</ta>
            <ta e="T23" id="Seg_2919" s="T19">PoNA_1970_NeverSeenBird_nar.003 (001.003)</ta>
            <ta e="T41" id="Seg_2920" s="T23">PoNA_1970_NeverSeenBird_nar.004 (001.004)</ta>
            <ta e="T50" id="Seg_2921" s="T41">PoNA_1970_NeverSeenBird_nar.005 (001.005)</ta>
            <ta e="T55" id="Seg_2922" s="T50">PoNA_1970_NeverSeenBird_nar.006 (001.006)</ta>
            <ta e="T61" id="Seg_2923" s="T55">PoNA_1970_NeverSeenBird_nar.007 (001.007)</ta>
            <ta e="T72" id="Seg_2924" s="T61">PoNA_1970_NeverSeenBird_nar.008 (001.008)</ta>
            <ta e="T76" id="Seg_2925" s="T72">PoNA_1970_NeverSeenBird_nar.009 (001.009)</ta>
            <ta e="T86" id="Seg_2926" s="T76">PoNA_1970_NeverSeenBird_nar.010 (001.010)</ta>
            <ta e="T92" id="Seg_2927" s="T86">PoNA_1970_NeverSeenBird_nar.011 (001.011)</ta>
            <ta e="T100" id="Seg_2928" s="T92">PoNA_1970_NeverSeenBird_nar.012 (001.012)</ta>
            <ta e="T105" id="Seg_2929" s="T100">PoNA_1970_NeverSeenBird_nar.013 (001.013)</ta>
            <ta e="T112" id="Seg_2930" s="T105">PoNA_1970_NeverSeenBird_nar.014 (001.014)</ta>
            <ta e="T120" id="Seg_2931" s="T112">PoNA_1970_NeverSeenBird_nar.015 (001.015)</ta>
            <ta e="T123" id="Seg_2932" s="T120">PoNA_1970_NeverSeenBird_nar.016 (001.016)</ta>
            <ta e="T126" id="Seg_2933" s="T123">PoNA_1970_NeverSeenBird_nar.017 (001.017)</ta>
            <ta e="T131" id="Seg_2934" s="T126">PoNA_1970_NeverSeenBird_nar.018 (001.018)</ta>
            <ta e="T137" id="Seg_2935" s="T131">PoNA_1970_NeverSeenBird_nar.019 (001.019)</ta>
            <ta e="T155" id="Seg_2936" s="T137">PoNA_1970_NeverSeenBird_nar.020 (001.020)</ta>
            <ta e="T170" id="Seg_2937" s="T155">PoNA_1970_NeverSeenBird_nar.021 (001.021)</ta>
            <ta e="T179" id="Seg_2938" s="T170">PoNA_1970_NeverSeenBird_nar.022 (001.022)</ta>
            <ta e="T184" id="Seg_2939" s="T179">PoNA_1970_NeverSeenBird_nar.023 (001.023)</ta>
            <ta e="T189" id="Seg_2940" s="T184">PoNA_1970_NeverSeenBird_nar.024 (001.024)</ta>
            <ta e="T194" id="Seg_2941" s="T189">PoNA_1970_NeverSeenBird_nar.025 (001.025)</ta>
            <ta e="T199" id="Seg_2942" s="T194">PoNA_1970_NeverSeenBird_nar.026 (001.026)</ta>
            <ta e="T212" id="Seg_2943" s="T199">PoNA_1970_NeverSeenBird_nar.027 (001.027)</ta>
            <ta e="T217" id="Seg_2944" s="T212">PoNA_1970_NeverSeenBird_nar.028 (001.029)</ta>
            <ta e="T225" id="Seg_2945" s="T217">PoNA_1970_NeverSeenBird_nar.029 (001.030)</ta>
            <ta e="T234" id="Seg_2946" s="T225">PoNA_1970_NeverSeenBird_nar.030 (001.031)</ta>
            <ta e="T242" id="Seg_2947" s="T234">PoNA_1970_NeverSeenBird_nar.031 (001.032)</ta>
            <ta e="T248" id="Seg_2948" s="T242">PoNA_1970_NeverSeenBird_nar.032 (001.033)</ta>
            <ta e="T250" id="Seg_2949" s="T248">PoNA_1970_NeverSeenBird_nar.033 (001.034)</ta>
            <ta e="T268" id="Seg_2950" s="T250">PoNA_1970_NeverSeenBird_nar.034 (001.035)</ta>
            <ta e="T280" id="Seg_2951" s="T268">PoNA_1970_NeverSeenBird_nar.035 (001.037)</ta>
            <ta e="T293" id="Seg_2952" s="T280">PoNA_1970_NeverSeenBird_nar.036 (001.038)</ta>
            <ta e="T300" id="Seg_2953" s="T293">PoNA_1970_NeverSeenBird_nar.037 (001.039)</ta>
            <ta e="T308" id="Seg_2954" s="T300">PoNA_1970_NeverSeenBird_nar.038 (001.040)</ta>
            <ta e="T319" id="Seg_2955" s="T308">PoNA_1970_NeverSeenBird_nar.039 (001.041)</ta>
            <ta e="T326" id="Seg_2956" s="T319">PoNA_1970_NeverSeenBird_nar.040 (001.042)</ta>
            <ta e="T332" id="Seg_2957" s="T326">PoNA_1970_NeverSeenBird_nar.041 (001.043)</ta>
            <ta e="T339" id="Seg_2958" s="T332">PoNA_1970_NeverSeenBird_nar.042 (001.044)</ta>
            <ta e="T343" id="Seg_2959" s="T339">PoNA_1970_NeverSeenBird_nar.043 (001.045)</ta>
            <ta e="T350" id="Seg_2960" s="T343">PoNA_1970_NeverSeenBird_nar.044 (001.046)</ta>
            <ta e="T358" id="Seg_2961" s="T350">PoNA_1970_NeverSeenBird_nar.045 (001.048)</ta>
            <ta e="T366" id="Seg_2962" s="T358">PoNA_1970_NeverSeenBird_nar.046 (001.049)</ta>
            <ta e="T373" id="Seg_2963" s="T366">PoNA_1970_NeverSeenBird_nar.047 (001.050)</ta>
            <ta e="T378" id="Seg_2964" s="T373">PoNA_1970_NeverSeenBird_nar.048 (001.052)</ta>
            <ta e="T381" id="Seg_2965" s="T378">PoNA_1970_NeverSeenBird_nar.049 (001.053)</ta>
            <ta e="T386" id="Seg_2966" s="T381">PoNA_1970_NeverSeenBird_nar.050 (001.054)</ta>
            <ta e="T390" id="Seg_2967" s="T386">PoNA_1970_NeverSeenBird_nar.051 (001.055)</ta>
            <ta e="T393" id="Seg_2968" s="T390">PoNA_1970_NeverSeenBird_nar.052 (001.056)</ta>
            <ta e="T395" id="Seg_2969" s="T393">PoNA_1970_NeverSeenBird_nar.053 (001.057)</ta>
            <ta e="T401" id="Seg_2970" s="T395">PoNA_1970_NeverSeenBird_nar.054 (001.058)</ta>
            <ta e="T407" id="Seg_2971" s="T401">PoNA_1970_NeverSeenBird_nar.055 (001.059)</ta>
            <ta e="T418" id="Seg_2972" s="T407">PoNA_1970_NeverSeenBird_nar.056 (001.060)</ta>
            <ta e="T421" id="Seg_2973" s="T418">PoNA_1970_NeverSeenBird_nar.057 (001.061)</ta>
            <ta e="T425" id="Seg_2974" s="T421">PoNA_1970_NeverSeenBird_nar.058 (001.062)</ta>
            <ta e="T432" id="Seg_2975" s="T425">PoNA_1970_NeverSeenBird_nar.059 (001.063)</ta>
            <ta e="T440" id="Seg_2976" s="T432">PoNA_1970_NeverSeenBird_nar.060 (001.064)</ta>
            <ta e="T447" id="Seg_2977" s="T440">PoNA_1970_NeverSeenBird_nar.061 (001.065)</ta>
            <ta e="T455" id="Seg_2978" s="T447">PoNA_1970_NeverSeenBird_nar.062 (001.066)</ta>
            <ta e="T460" id="Seg_2979" s="T455">PoNA_1970_NeverSeenBird_nar.063 (001.067)</ta>
            <ta e="T469" id="Seg_2980" s="T460">PoNA_1970_NeverSeenBird_nar.064 (001.068)</ta>
            <ta e="T473" id="Seg_2981" s="T469">PoNA_1970_NeverSeenBird_nar.065 (001.069)</ta>
            <ta e="T480" id="Seg_2982" s="T473">PoNA_1970_NeverSeenBird_nar.066 (001.070)</ta>
            <ta e="T486" id="Seg_2983" s="T480">PoNA_1970_NeverSeenBird_nar.067 (001.071)</ta>
            <ta e="T493" id="Seg_2984" s="T486">PoNA_1970_NeverSeenBird_nar.068 (001.072)</ta>
            <ta e="T498" id="Seg_2985" s="T493">PoNA_1970_NeverSeenBird_nar.069 (001.073)</ta>
            <ta e="T506" id="Seg_2986" s="T498">PoNA_1970_NeverSeenBird_nar.070 (001.074)</ta>
            <ta e="T519" id="Seg_2987" s="T506">PoNA_1970_NeverSeenBird_nar.071 (001.075)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T14" id="Seg_2988" s="T0">Советтар ыйаактара һаӈардыы кэлэригэр, Курйа диэн олокко һайын аайы балыктыы олороoччу этилэрэ һака дьоно.</ta>
            <ta e="T19" id="Seg_2989" s="T14">Туора карактаак чубу-чубу һылдьыбат олого.</ta>
            <ta e="T23" id="Seg_2990" s="T19">Оннук ыраак балыктыыр һирдэрэ.</ta>
            <ta e="T41" id="Seg_2991" s="T23">Каһыан (каһан) ирэ ким эмэ кэлиэ һайын чугастаагы һытынньактарга өс кэпсии, бэйэтин ологун туһунан, билээри кайдак балыктылларын Курйа дьоно.</ta>
            <ta e="T50" id="Seg_2992" s="T41">Биир һайын огонньоттор эрдэ һарсиэрда эрдинэн каалбыттара биискэлэрин көрүнэ.</ta>
            <ta e="T55" id="Seg_2993" s="T50">Дьактар, ого ааймак дьиэлэригэр олорбуттара.</ta>
            <ta e="T61" id="Seg_2994" s="T55">Ким балык ыраастанар, ким дьукула куурдунар.</ta>
            <ta e="T72" id="Seg_2995" s="T61">Олортон икки эмээксин, огонньоттор кэлиэктэригэр диэри, һырга дьиэлэрин тириилэрин абырактыыллар эбит.</ta>
            <ta e="T76" id="Seg_2996" s="T72">Куньаас багай күн турбута.</ta>
            <ta e="T86" id="Seg_2997" s="T76">Дудупта аан эбэлэрин уута чумпутугар үрүӈ этиӈ былыттара быдьыраһан көстөллөр.</ta>
            <ta e="T92" id="Seg_2998" s="T86">Кумаар котоку кыната катар куньаһа турбута.</ta>
            <ta e="T100" id="Seg_2999" s="T92">Эмээкситтэрэ, ол даа буоллар илиилэрэ һолото һуок, илбинэллэр.</ta>
            <ta e="T105" id="Seg_3000" s="T100">Ол быыһыгар һупту тиктии буолбуттара.</ta>
            <ta e="T112" id="Seg_3001" s="T105">Иннэлэрин тыаһа өӈнөөк тириигэ курдургуура ыраактан иһиллэр.</ta>
            <ta e="T120" id="Seg_3002" s="T112">Ол үлэлэнэ олордокторуна, һирдэрэ того эрэ ньиргийэр эбит.</ta>
            <ta e="T123" id="Seg_3003" s="T120">"Бэ – диэбит бииргэһэ.</ta>
            <ta e="T126" id="Seg_3004" s="T123">– Того һирбит ньиргийэр?</ta>
            <ta e="T131" id="Seg_3005" s="T126">Того этиӈ тыаһа итигирдиик буолуой?</ta>
            <ta e="T137" id="Seg_3006" s="T131">Туок эрэ диктитэ буолла, иһиллэ, эмээксин."</ta>
            <ta e="T155" id="Seg_3007" s="T137">Кристин, кырдьагас, аһа былдьаччы гылбайбыт эмээксин, уоһугар биир да тииһэ һуок, өс киирбэк пылаатын мэньиититтэн һулбу тардаат иһиллээбит.</ta>
            <ta e="T170" id="Seg_3008" s="T155">Ол оһур, икки эмээксин каллаан диэк ньылайан бараан, үүрпүт таба көрдүк, ньиргийэр тыаһы иһиллээн буолбуттар.</ta>
            <ta e="T179" id="Seg_3009" s="T170">Кумаар даа һиирин билбэккэ, өркөөн олорбуттар, аньактарын атан бараан.</ta>
            <ta e="T184" id="Seg_3010" s="T179">Аттыларыгар тириилэр лоскутуоктара онно-манна һыталлар.</ta>
            <ta e="T189" id="Seg_3011" s="T184">Быаннан байыылаак мордьоолоро илиилэригэр буруолаһаллар.</ta>
            <ta e="T194" id="Seg_3012" s="T189">Күүппүт көрдүк сып-сып каһан олоруоктарын.</ta>
            <ta e="T199" id="Seg_3013" s="T194">Кирдик, гиннэр билбэтэк тыастара буолбут.</ta>
            <ta e="T212" id="Seg_3014" s="T199">"Кайа да, дьэ өллүбүт!" – дии түһээт бииргэс эмээксин тура эккирээбит, мас диэк көрүлээбит.</ta>
            <ta e="T217" id="Seg_3015" s="T212">"Дьэ, көтөн иһэр улуу көтөрө.</ta>
            <ta e="T225" id="Seg_3016" s="T217">Көр, ол кытаран иһэр, гини сиһин (һиһин) тыаһа, эбит.</ta>
            <ta e="T234" id="Seg_3017" s="T225">Хоосподи, мин һаӈарбатагым, көрбөтөгүм буоллун," – диэн эмээксин лабыйан барбыт.</ta>
            <ta e="T242" id="Seg_3018" s="T234">"О, туок көтөрүн гынагын" – дии-дии догоро эмиэ көрүлээбит. </ta>
            <ta e="T248" id="Seg_3019" s="T242">Атактарын анныларыгар һирдэрэ мөӈөр көрдүк буолбут.</ta>
            <ta e="T250" id="Seg_3020" s="T248">Ураһалар титирээбиттэр.</ta>
            <ta e="T268" id="Seg_3021" s="T250">"Түргэнник, һырга диэк аннытыгар!" – дии түһээт, бииргэһэ чип гыммыт катыылардаак муоста анныгар, догоро гинини көӈүйэн (когүйэн) киирбит буолагын анныгар.</ta>
            <ta e="T280" id="Seg_3022" s="T268">Ол кэм, лүөтчик һаӈардыы ураһа дьиэлэри көрө һотору, ньамчыгас багайы көтөн ааспыт.</ta>
            <ta e="T293" id="Seg_3023" s="T280">Һырга дьиэни гытта буолак анныларыттан һиипкэ эмээкситтэр һарыы атактара мөмпөккө чороһо һыппыттар. </ta>
            <ta e="T300" id="Seg_3024" s="T293">Кытылга огонньоттор эккирии-эккирии бэргэһэлэрин самолёкка даайбы турбуттар.</ta>
            <ta e="T308" id="Seg_3025" s="T300">Гиннэр (гинилэр) үөрүүлэрэ, һаӈардыы самолёт кэлэригэр муорага бэрт эбит.</ta>
            <ta e="T319" id="Seg_3026" s="T308">Билэллэр, советский власть билсэр буола һылдьыага, кайтак (кайдак) табаһыттар, балыкчыттар, олороллорун көрөөрү.</ta>
            <ta e="T326" id="Seg_3027" s="T319">Һыыр үрдүгэр таксаат одууламмыттар – ураһа дьиэлэрэ буруолаабаттар.</ta>
            <ta e="T332" id="Seg_3028" s="T326">Тиэргээн үрдүнэн (ким..) им дьылыччы.</ta>
            <ta e="T339" id="Seg_3029" s="T332">Ыт да үрбэт, киһи да һаӈата иһиллибэт.</ta>
            <ta e="T343" id="Seg_3030" s="T339">"Туок дьонор (дьоной) бу эмээкситтэр?</ta>
            <ta e="T350" id="Seg_3031" s="T343">Билбэттэр дуо биһиги кэлиэкпитин?" – диэбит Оноо огонньор.</ta>
            <ta e="T358" id="Seg_3032" s="T350">"Канна баргытыый (бааргытый), дьиэлээктэр", – (үөг..) үөгүлээбит ыал киһитэ.</ta>
            <ta e="T366" id="Seg_3033" s="T358">"Ара, абырааӈ, араарыӈ, өллүбүт", – һир иһиттэн үөгүү иһиллибит.</ta>
            <ta e="T373" id="Seg_3034" s="T366">"Туоктарый, иирбиттэр дуо?" – дии түһээт Оноо көрүлээбит.</ta>
            <ta e="T378" id="Seg_3035" s="T373">Гини һырга дьиэлэр анныларыгар көрүлээбит.</ta>
            <ta e="T381" id="Seg_3036" s="T378">Туок диктитэ буолла?</ta>
            <ta e="T386" id="Seg_3037" s="T381">Эмээкситтэрин атактара онно һалыбыраһа һыппыттар.</ta>
            <ta e="T390" id="Seg_3038" s="T386">Көрдөгүнэ, такса һатыыллар, быһылаак.</ta>
            <ta e="T393" id="Seg_3039" s="T390">Катыылар таӈастарыттан иилбиттэр.</ta>
            <ta e="T395" id="Seg_3040" s="T393">Того ыытыактарай?</ta>
            <ta e="T401" id="Seg_3041" s="T395">Искэ да кииллэрбэттэр, таска да таһаарбаттар.</ta>
            <ta e="T407" id="Seg_3042" s="T401">"Ара" дэһии элбээн испит, ынчыктыыр койдубут.</ta>
            <ta e="T418" id="Seg_3043" s="T407">Огонньоттор туок да диэк бэрдиттэн, сөгө(һөгө) һатаан, тугу да һаӈарбат буолбуттар.</ta>
            <ta e="T421" id="Seg_3044" s="T418">Эмээкситтэри биирди босколообуттар.</ta>
            <ta e="T425" id="Seg_3045" s="T421">Гиннэргэ (гинилэргэ) һырай кэлиэ дуо.</ta>
            <ta e="T432" id="Seg_3046" s="T425">Астара карактарын һаппыт, һарыы атактара ньыпкаччы түспүттэр.</ta>
            <ta e="T440" id="Seg_3047" s="T432">Дьүһүннэрэ уларыйбыт: буортан таксыбыт кырса оголорун көрдүк буолбуттар.</ta>
            <ta e="T447" id="Seg_3048" s="T440">"Кайа, туок буолбуккутуй" – диэн ыйыппыттар биирди дурук.</ta>
            <ta e="T455" id="Seg_3049" s="T447">"Учум, билбэтэк көтөрбүт ааста," – диэн ньыӈынаспыттар икки эмээксин.</ta>
            <ta e="T460" id="Seg_3050" s="T455">"О-o" – билбэтэк буолан, Оноо һэӈээрбит.</ta>
            <ta e="T469" id="Seg_3051" s="T460">"Дьэ кэпсээӈ, туок түүлүн түһээтигит һырга дьиэ анныгар һытаӈӈыт?"</ta>
            <ta e="T473" id="Seg_3052" s="T469">"Бу туок буолар огонньоруй?</ta>
            <ta e="T480" id="Seg_3053" s="T473">Кырдьан өйө кылгыыр, быһылаак" – агдаспыттар икки куттанньактар.</ta>
            <ta e="T486" id="Seg_3054" s="T480">Һи, ускаан һүрэктэрэ өссүө һаӈалаак буолбуттар.</ta>
            <ta e="T493" id="Seg_3055" s="T486">"Билбэккит дуо, үйэ уларыйбытын советтар чыычаактара кэлэрин?</ta>
            <ta e="T498" id="Seg_3056" s="T493">Интигит эһигини абырыы көтө һылдьар.</ta>
            <ta e="T506" id="Seg_3057" s="T498">Тыа дьоно тот, баай олоруоктарын," – дэспиттэр икки киһи.</ta>
            <ta e="T519" id="Seg_3058" s="T506">Эмээкситтэр туок даа диэктэрин бэрдиттэн, һаӈата һуок һааппут (һааппыт) көрдүк дьиэлэрин диэк өкөһө турбуттар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T14" id="Seg_3059" s="T0">Savʼettar ɨjaːktara haŋardɨː keleriger, Kurʼja di͡en olokko hajɨn aːjɨ balɨktɨː oloroːčču etilere haka dʼono. </ta>
            <ta e="T19" id="Seg_3060" s="T14">Tu͡ora karaktaːk čubu-čubu hɨldʼɨbat ologo. </ta>
            <ta e="T23" id="Seg_3061" s="T19">Onnuk ɨraːk balɨktɨːr hirdere. </ta>
            <ta e="T41" id="Seg_3062" s="T23">Kahɨ͡an ire kim eme keli͡e hajɨn čugastaːgɨ hɨtɨnnʼaktarga ös kepsiː, bejetin ologun tuhunan, bileːri kajdak balɨktɨːllarɨn Kurʼja dʼono. </ta>
            <ta e="T50" id="Seg_3063" s="T41">Biːr hajɨn ogonnʼottor erde harsi͡erda erdinen kaːlbɨttara biːskelerin körüne. </ta>
            <ta e="T55" id="Seg_3064" s="T50">Dʼaktar, ogo aːjmak dʼi͡eleriger olorbuttara. </ta>
            <ta e="T61" id="Seg_3065" s="T55">Kim balɨk ɨraːstanar, kim dʼukula kuːrdunar. </ta>
            <ta e="T72" id="Seg_3066" s="T61">Olorton ikki emeːksin, ogonnʼottor keli͡ekteriger di͡eri, hɨrga dʼi͡elerin tiriːlerin abɨraktɨːllar ebit. </ta>
            <ta e="T76" id="Seg_3067" s="T72">Kunʼaːs bagaj kün turbuta. </ta>
            <ta e="T86" id="Seg_3068" s="T76">Dudupta aːn ebelerin uːta čumputugar ürüŋ etiŋ bɨlɨttara bɨdʼɨrahan köstöllör. </ta>
            <ta e="T92" id="Seg_3069" s="T86">Kumaːr kotoku kɨnata katar kunʼaha turbuta. </ta>
            <ta e="T100" id="Seg_3070" s="T92">Emeːksittere, ol daː bu͡ollar iliːlere holoto hu͡ok, ilbineller. </ta>
            <ta e="T105" id="Seg_3071" s="T100">Ol bɨːhɨgar huptu tiktiː bu͡olbuttara. </ta>
            <ta e="T112" id="Seg_3072" s="T105">Innelerin tɨ͡aha öŋnöːk tiriːge kurdurguːra ɨraːktan ihiller. </ta>
            <ta e="T120" id="Seg_3073" s="T112">Ol ülelene olordoktoruna, hirdere togo ere nʼirgijer ebit. </ta>
            <ta e="T123" id="Seg_3074" s="T120">"Be", di͡ebit biːrgehe. </ta>
            <ta e="T126" id="Seg_3075" s="T123">"Togo hirbit nʼirgijer? </ta>
            <ta e="T131" id="Seg_3076" s="T126">Togo etiŋ tɨ͡aha itigirdik bu͡olu͡oj? </ta>
            <ta e="T137" id="Seg_3077" s="T131">Tu͡ok ere diktite bu͡olla, ihilleː, emeːksin." </ta>
            <ta e="T155" id="Seg_3078" s="T137">Krʼistʼin, kɨrdʼagas, aha bɨldʼaːččɨ gɨlbajbɨt emeːksin, u͡ohugar biːr da tiːhe hu͡ok, ös kiːrbet pɨlaːtɨn menʼiːtitten hulbu tardaːt ihilleːbit. </ta>
            <ta e="T170" id="Seg_3079" s="T155">Ol ((…)), ikki emeːksin kallaːn di͡ek nʼɨlajan baraːn, üːrpüt taba kördük, nʼirgijer tɨ͡ahɨ ihilleːn bu͡olbuttar. </ta>
            <ta e="T179" id="Seg_3080" s="T170">Kumaːr daː hiːrin bilbekke, örköːn olorbuttar, anʼaktarɨn atan baraːn. </ta>
            <ta e="T184" id="Seg_3081" s="T179">Attɨlarɨgar tiriːler loskutu͡oktara onno-manna hɨtallar. </ta>
            <ta e="T189" id="Seg_3082" s="T184">Bɨ͡annan baːjɨːlaːk mordʼoːloro iliːleriger buru͡olahallar. </ta>
            <ta e="T194" id="Seg_3083" s="T189">Küːppüt kördük sɨp-sɨp kahan oloru͡oktarɨn. </ta>
            <ta e="T199" id="Seg_3084" s="T194">Kirdik, ginner bilbetek tɨ͡astara bu͡olbut. </ta>
            <ta e="T212" id="Seg_3085" s="T199">"Kaja da, dʼe öllübüt", diː tüheːt biːrges emeːksin tura ekkireːbit, mas di͡ek körüleːbit. </ta>
            <ta e="T217" id="Seg_3086" s="T212">"Dʼe, kötön iher uluː kötörö. </ta>
            <ta e="T225" id="Seg_3087" s="T217">Kör, ol kɨtaran iher, gini sihin tɨ͡aha, ebit. </ta>
            <ta e="T234" id="Seg_3088" s="T225">Xoːspodi, min haŋarbatagɨm, körbötögüm bu͡ollun", di͡en emeːksin labɨjan barbɨt. </ta>
            <ta e="T242" id="Seg_3089" s="T234">"O, tu͡ok kötörün gɨnagɨn", diː-diː dogoro emi͡e körüleːbit. </ta>
            <ta e="T248" id="Seg_3090" s="T242">Ataktarɨn annɨlarɨgar hirdere möŋör kördük bu͡olbut. </ta>
            <ta e="T250" id="Seg_3091" s="T248">Urahalar titireːbitter. </ta>
            <ta e="T268" id="Seg_3092" s="T250">"Türgennik, hɨrga di͡ek annɨtɨgar", diː tüheːt, biːrgehe čip gɨmmɨt katɨːlardaːk mu͡osta annɨgar, dogoro ginini köŋüjen kiːrbit bu͡olagɨn annɨgar. </ta>
            <ta e="T280" id="Seg_3093" s="T268">Ol kem, lü͡ötčik haŋardɨː uraha dʼi͡eleri körö hotoru, nʼamčɨgas bagajɨ kötön aːspɨt. </ta>
            <ta e="T293" id="Seg_3094" s="T280">Hɨrga dʼi͡eni gɨtta bu͡olak annɨlarɨttan (hiːp-) emeːksitter harɨː ataktara mömpökkö čoroho hɨppɨttar. </ta>
            <ta e="T300" id="Seg_3095" s="T293">Kɨtɨlga ogonnʼottor ekkiriː-ekkiriː bergehelerin samalʼokka dajbɨː turbuttar. </ta>
            <ta e="T308" id="Seg_3096" s="T300">Ginner ü͡örüːlere, haŋardɨː samalʼot keleriger mu͡oraga bert ebit. </ta>
            <ta e="T319" id="Seg_3097" s="T308">Bileller, savʼetskʼij vlastʼ bilser bu͡ola hɨldʼɨ͡aga, kajtak tabahɨttar, balɨkčɨttar, olorollorun köröːrü. </ta>
            <ta e="T326" id="Seg_3098" s="T319">Hɨːr ürdüger taksaːt oduːlammɨttar– uraha dʼi͡elere buru͡olaːbattar. </ta>
            <ta e="T332" id="Seg_3099" s="T326">Ti͡ergeːn ürdünen (kim-) im dʼɨlɨččɨ. </ta>
            <ta e="T339" id="Seg_3100" s="T332">ɨt da ürbet, kihi da haŋata ihillibet. </ta>
            <ta e="T343" id="Seg_3101" s="T339">"Tu͡ok dʼonor bu emeːksitter? </ta>
            <ta e="T350" id="Seg_3102" s="T343">Bilbetter du͡o bihigi keli͡ekpitin", di͡ebit Onoː ogonnʼor. </ta>
            <ta e="T358" id="Seg_3103" s="T350">"Kanna baːrgɨtɨːj, dʼi͡eleːkter", (ü͡ög-) ü͡ögüleːbit ɨ͡al kihite. </ta>
            <ta e="T366" id="Seg_3104" s="T358">"Ara, abɨraːŋ, araːrɨŋ, öllübüt", hir ihitten ü͡ögüː ihillibit. </ta>
            <ta e="T373" id="Seg_3105" s="T366">"Tu͡oktarɨj, iːrbitter du͡o", diː tüheːt Onoː körüleːbit. </ta>
            <ta e="T378" id="Seg_3106" s="T373">Gini hɨrga dʼi͡eler annɨlarɨgar körüleːbit. </ta>
            <ta e="T381" id="Seg_3107" s="T378">Tu͡ok diktite bu͡olla? </ta>
            <ta e="T386" id="Seg_3108" s="T381">Emeːksitterin ataktara onno halɨbɨraha hɨppɨttar. </ta>
            <ta e="T390" id="Seg_3109" s="T386">Kördögüne, taksa hatɨːllar, bɨhɨlaːk. </ta>
            <ta e="T393" id="Seg_3110" s="T390">Katɨːlar taŋastarɨttan iːlbitter. </ta>
            <ta e="T395" id="Seg_3111" s="T393">Togo ɨːtɨ͡aktaraj? </ta>
            <ta e="T401" id="Seg_3112" s="T395">Iske da kiːllerbetter, taska da tahaːrbattar. </ta>
            <ta e="T407" id="Seg_3113" s="T401">"Ara" dehiː elbeːn ispit, ɨnčɨktɨːr kojdubut. </ta>
            <ta e="T418" id="Seg_3114" s="T407">Ogonnʼottor tu͡ok da di͡ek berditten, sögö hataːn, tugu da haŋarbat bu͡olbuttar. </ta>
            <ta e="T421" id="Seg_3115" s="T418">Emeːksitteri biːrdiː boskoloːbuttar. </ta>
            <ta e="T425" id="Seg_3116" s="T421">Ginnerge hɨraj keli͡e du͡o. </ta>
            <ta e="T432" id="Seg_3117" s="T425">Astara karaktarɨn happɨt, harɨː ataktara nʼɨpkaččɨ tüspütter. </ta>
            <ta e="T440" id="Seg_3118" s="T432">Dʼühünnere ularɨjbɨt, bu͡ortan taksɨbɨt kɨrsa ogolorun kördük bu͡olbuttar. </ta>
            <ta e="T447" id="Seg_3119" s="T440">"Kaja, tu͡ok bu͡olbukkutuj", di͡en ɨjɨppɨttar biːrdiː duruk. </ta>
            <ta e="T455" id="Seg_3120" s="T447">"Učum, bilbetek kötörbüt aːsta", di͡en nʼɨŋɨnaspɨttar ikki emeːksin. </ta>
            <ta e="T460" id="Seg_3121" s="T455">"Oː", bilbetek bu͡olan, Onoː heŋeːrbit. </ta>
            <ta e="T469" id="Seg_3122" s="T460">"Dʼe kepseːŋ, tu͡ok tüːlün tüheːtigit hɨrga dʼi͡e annɨgar hɨtaŋŋɨt?" </ta>
            <ta e="T473" id="Seg_3123" s="T469">"Bu tu͡ok bu͡olar ogonnʼoruj? </ta>
            <ta e="T480" id="Seg_3124" s="T473">Kɨrdʼan öjö kɨlgɨːr, bɨhɨlaːk", agdaspɨttar ikki kuttannʼaktar. </ta>
            <ta e="T486" id="Seg_3125" s="T480">Hi, uskaːn hürektere össü͡ö haŋalaːk bu͡olbuttar. </ta>
            <ta e="T493" id="Seg_3126" s="T486">"Bilbekkit du͡o, üje ularɨjbɨtɨn savʼettar čɨːčaːktara kelerin? </ta>
            <ta e="T498" id="Seg_3127" s="T493">Intigit ehigini abɨrɨː kötö hɨldʼar. </ta>
            <ta e="T506" id="Seg_3128" s="T498">Tɨ͡a dʼono tot, baːj oloru͡oktarɨn", despitter ikki kihi. </ta>
            <ta e="T519" id="Seg_3129" s="T506">Emeːksitter tu͡ok daː di͡ekterin berditten, haŋata hu͡ok haːpput kördük dʼi͡elerin di͡ek ököhö turbuttar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3130" s="T0">savʼet-tar</ta>
            <ta e="T2" id="Seg_3131" s="T1">ɨjaːk-tara</ta>
            <ta e="T3" id="Seg_3132" s="T2">haŋardɨː</ta>
            <ta e="T4" id="Seg_3133" s="T3">kel-er-i-ger</ta>
            <ta e="T5" id="Seg_3134" s="T4">Kurʼja</ta>
            <ta e="T6" id="Seg_3135" s="T5">di͡e-n</ta>
            <ta e="T7" id="Seg_3136" s="T6">olok-ko</ta>
            <ta e="T8" id="Seg_3137" s="T7">hajɨn</ta>
            <ta e="T9" id="Seg_3138" s="T8">aːjɨ</ta>
            <ta e="T10" id="Seg_3139" s="T9">balɨkt-ɨː</ta>
            <ta e="T11" id="Seg_3140" s="T10">olor-oːčču</ta>
            <ta e="T12" id="Seg_3141" s="T11">e-ti-lere</ta>
            <ta e="T13" id="Seg_3142" s="T12">haka</ta>
            <ta e="T14" id="Seg_3143" s="T13">dʼon-o</ta>
            <ta e="T15" id="Seg_3144" s="T14">tu͡ora</ta>
            <ta e="T16" id="Seg_3145" s="T15">karak-taːk</ta>
            <ta e="T17" id="Seg_3146" s="T16">čubu-čubu</ta>
            <ta e="T18" id="Seg_3147" s="T17">hɨldʼ-ɨ-bat</ta>
            <ta e="T19" id="Seg_3148" s="T18">olog-o</ta>
            <ta e="T20" id="Seg_3149" s="T19">onnuk</ta>
            <ta e="T21" id="Seg_3150" s="T20">ɨraːk</ta>
            <ta e="T22" id="Seg_3151" s="T21">balɨktɨː-r</ta>
            <ta e="T23" id="Seg_3152" s="T22">hir-dere</ta>
            <ta e="T24" id="Seg_3153" s="T23">kahɨ͡an</ta>
            <ta e="T25" id="Seg_3154" s="T24">ire</ta>
            <ta e="T26" id="Seg_3155" s="T25">kim</ta>
            <ta e="T27" id="Seg_3156" s="T26">eme</ta>
            <ta e="T28" id="Seg_3157" s="T27">kel-i͡e</ta>
            <ta e="T29" id="Seg_3158" s="T28">hajɨn</ta>
            <ta e="T30" id="Seg_3159" s="T29">čugas-taːgɨ</ta>
            <ta e="T31" id="Seg_3160" s="T30">hɨtɨnnʼak-tar-ga</ta>
            <ta e="T32" id="Seg_3161" s="T31">ös</ta>
            <ta e="T33" id="Seg_3162" s="T32">keps-iː</ta>
            <ta e="T34" id="Seg_3163" s="T33">beje-ti-n</ta>
            <ta e="T35" id="Seg_3164" s="T34">olog-u-n</ta>
            <ta e="T36" id="Seg_3165" s="T35">tuh-u-nan</ta>
            <ta e="T37" id="Seg_3166" s="T36">bil-eːri</ta>
            <ta e="T38" id="Seg_3167" s="T37">kajdak</ta>
            <ta e="T39" id="Seg_3168" s="T38">balɨktɨː-l-larɨ-n</ta>
            <ta e="T40" id="Seg_3169" s="T39">Kurʼja</ta>
            <ta e="T41" id="Seg_3170" s="T40">dʼon-o</ta>
            <ta e="T42" id="Seg_3171" s="T41">biːr</ta>
            <ta e="T43" id="Seg_3172" s="T42">hajɨn</ta>
            <ta e="T44" id="Seg_3173" s="T43">ogonnʼot-tor</ta>
            <ta e="T45" id="Seg_3174" s="T44">erde</ta>
            <ta e="T46" id="Seg_3175" s="T45">harsi͡erda</ta>
            <ta e="T47" id="Seg_3176" s="T46">erd-i-n-en</ta>
            <ta e="T48" id="Seg_3177" s="T47">kaːl-bɨt-tara</ta>
            <ta e="T49" id="Seg_3178" s="T48">biːske-ler-i-n</ta>
            <ta e="T50" id="Seg_3179" s="T49">kör-ü-n-e</ta>
            <ta e="T51" id="Seg_3180" s="T50">dʼaktar</ta>
            <ta e="T52" id="Seg_3181" s="T51">ogo</ta>
            <ta e="T53" id="Seg_3182" s="T52">aːjmak</ta>
            <ta e="T54" id="Seg_3183" s="T53">dʼi͡e-leri-ger</ta>
            <ta e="T55" id="Seg_3184" s="T54">olor-but-tara</ta>
            <ta e="T56" id="Seg_3185" s="T55">kim</ta>
            <ta e="T57" id="Seg_3186" s="T56">balɨk</ta>
            <ta e="T58" id="Seg_3187" s="T57">ɨraːst-a-n-ar</ta>
            <ta e="T59" id="Seg_3188" s="T58">kim</ta>
            <ta e="T60" id="Seg_3189" s="T59">dʼukula</ta>
            <ta e="T61" id="Seg_3190" s="T60">kuːrd-u-n-ar</ta>
            <ta e="T62" id="Seg_3191" s="T61">o-lor-ton</ta>
            <ta e="T63" id="Seg_3192" s="T62">ikki</ta>
            <ta e="T64" id="Seg_3193" s="T63">emeːksin</ta>
            <ta e="T65" id="Seg_3194" s="T64">ogonnʼot-tor</ta>
            <ta e="T66" id="Seg_3195" s="T65">kel-i͡ek-teri-ger</ta>
            <ta e="T67" id="Seg_3196" s="T66">di͡eri</ta>
            <ta e="T68" id="Seg_3197" s="T67">hɨrga</ta>
            <ta e="T69" id="Seg_3198" s="T68">dʼi͡e-leri-n</ta>
            <ta e="T70" id="Seg_3199" s="T69">tiriː-leri-n</ta>
            <ta e="T71" id="Seg_3200" s="T70">abɨraktɨː-l-lar</ta>
            <ta e="T72" id="Seg_3201" s="T71">e-bit</ta>
            <ta e="T73" id="Seg_3202" s="T72">kunʼaːs</ta>
            <ta e="T74" id="Seg_3203" s="T73">bagaj</ta>
            <ta e="T75" id="Seg_3204" s="T74">kün</ta>
            <ta e="T76" id="Seg_3205" s="T75">tur-but-a</ta>
            <ta e="T77" id="Seg_3206" s="T76">Dudupta</ta>
            <ta e="T78" id="Seg_3207" s="T77">aːn</ta>
            <ta e="T79" id="Seg_3208" s="T78">ebe-leri-n</ta>
            <ta e="T80" id="Seg_3209" s="T79">uː-ta</ta>
            <ta e="T81" id="Seg_3210" s="T80">čumpu-tu-gar</ta>
            <ta e="T82" id="Seg_3211" s="T81">ürüŋ</ta>
            <ta e="T83" id="Seg_3212" s="T82">etiŋ</ta>
            <ta e="T84" id="Seg_3213" s="T83">bɨlɨt-tar-a</ta>
            <ta e="T85" id="Seg_3214" s="T84">bɨdʼɨrah-an</ta>
            <ta e="T86" id="Seg_3215" s="T85">köst-öl-lör</ta>
            <ta e="T87" id="Seg_3216" s="T86">kumaːr</ta>
            <ta e="T88" id="Seg_3217" s="T87">kotoku</ta>
            <ta e="T89" id="Seg_3218" s="T88">kɨnat-a</ta>
            <ta e="T90" id="Seg_3219" s="T89">kat-ar</ta>
            <ta e="T91" id="Seg_3220" s="T90">kunʼah-a</ta>
            <ta e="T92" id="Seg_3221" s="T91">tur-but-a</ta>
            <ta e="T93" id="Seg_3222" s="T92">emeːksit-ter-e</ta>
            <ta e="T94" id="Seg_3223" s="T93">ol</ta>
            <ta e="T95" id="Seg_3224" s="T94">daː</ta>
            <ta e="T96" id="Seg_3225" s="T95">bu͡ol-lar</ta>
            <ta e="T97" id="Seg_3226" s="T96">iliː-lere</ta>
            <ta e="T98" id="Seg_3227" s="T97">holo-to</ta>
            <ta e="T99" id="Seg_3228" s="T98">hu͡ok</ta>
            <ta e="T100" id="Seg_3229" s="T99">ilbin-el-ler</ta>
            <ta e="T101" id="Seg_3230" s="T100">ol</ta>
            <ta e="T102" id="Seg_3231" s="T101">bɨːh-ɨ-gar</ta>
            <ta e="T103" id="Seg_3232" s="T102">huptu</ta>
            <ta e="T104" id="Seg_3233" s="T103">tik-t-iː</ta>
            <ta e="T105" id="Seg_3234" s="T104">bu͡ol-but-tara</ta>
            <ta e="T106" id="Seg_3235" s="T105">inne-leri-n</ta>
            <ta e="T107" id="Seg_3236" s="T106">tɨ͡ah-a</ta>
            <ta e="T108" id="Seg_3237" s="T107">öŋnöːk</ta>
            <ta e="T109" id="Seg_3238" s="T108">tiriː-ge</ta>
            <ta e="T110" id="Seg_3239" s="T109">kurdurguː-r-a</ta>
            <ta e="T111" id="Seg_3240" s="T110">ɨraːk-tan</ta>
            <ta e="T112" id="Seg_3241" s="T111">ihill-er</ta>
            <ta e="T113" id="Seg_3242" s="T112">ol</ta>
            <ta e="T114" id="Seg_3243" s="T113">üle-len-e</ta>
            <ta e="T115" id="Seg_3244" s="T114">olor-dok-toruna</ta>
            <ta e="T116" id="Seg_3245" s="T115">hir-dere</ta>
            <ta e="T117" id="Seg_3246" s="T116">togo</ta>
            <ta e="T118" id="Seg_3247" s="T117">ere</ta>
            <ta e="T119" id="Seg_3248" s="T118">nʼirgij-er</ta>
            <ta e="T120" id="Seg_3249" s="T119">e-bit</ta>
            <ta e="T121" id="Seg_3250" s="T120">be</ta>
            <ta e="T122" id="Seg_3251" s="T121">di͡e-bit</ta>
            <ta e="T123" id="Seg_3252" s="T122">biːrgeh-e</ta>
            <ta e="T124" id="Seg_3253" s="T123">togo</ta>
            <ta e="T125" id="Seg_3254" s="T124">hir-bit</ta>
            <ta e="T126" id="Seg_3255" s="T125">nʼirgij-er</ta>
            <ta e="T127" id="Seg_3256" s="T126">togo</ta>
            <ta e="T128" id="Seg_3257" s="T127">etiŋ</ta>
            <ta e="T129" id="Seg_3258" s="T128">tɨ͡ah-a</ta>
            <ta e="T130" id="Seg_3259" s="T129">itigirdik</ta>
            <ta e="T131" id="Seg_3260" s="T130">bu͡ol-u͡o=j</ta>
            <ta e="T132" id="Seg_3261" s="T131">tu͡ok</ta>
            <ta e="T133" id="Seg_3262" s="T132">ere</ta>
            <ta e="T134" id="Seg_3263" s="T133">dikti-te</ta>
            <ta e="T135" id="Seg_3264" s="T134">bu͡ol-l-a</ta>
            <ta e="T136" id="Seg_3265" s="T135">ihilleː</ta>
            <ta e="T137" id="Seg_3266" s="T136">emeːksin</ta>
            <ta e="T138" id="Seg_3267" s="T137">Krʼistʼin</ta>
            <ta e="T139" id="Seg_3268" s="T138">kɨrdʼagas</ta>
            <ta e="T140" id="Seg_3269" s="T139">ah-a</ta>
            <ta e="T141" id="Seg_3270" s="T140">bɨldʼaː-ččɨ</ta>
            <ta e="T142" id="Seg_3271" s="T141">gɨlbaj-bɨt</ta>
            <ta e="T143" id="Seg_3272" s="T142">emeːksin</ta>
            <ta e="T144" id="Seg_3273" s="T143">u͡oh-u-gar</ta>
            <ta e="T145" id="Seg_3274" s="T144">biːr</ta>
            <ta e="T146" id="Seg_3275" s="T145">da</ta>
            <ta e="T147" id="Seg_3276" s="T146">tiːh-e</ta>
            <ta e="T148" id="Seg_3277" s="T147">hu͡ok</ta>
            <ta e="T149" id="Seg_3278" s="T148">ös</ta>
            <ta e="T150" id="Seg_3279" s="T149">kiːr-bet</ta>
            <ta e="T151" id="Seg_3280" s="T150">pɨlaːt-ɨ-n</ta>
            <ta e="T152" id="Seg_3281" s="T151">menʼiː-ti-tten</ta>
            <ta e="T153" id="Seg_3282" s="T152">hulbu</ta>
            <ta e="T154" id="Seg_3283" s="T153">tard-aːt</ta>
            <ta e="T155" id="Seg_3284" s="T154">ihilleː-bit</ta>
            <ta e="T156" id="Seg_3285" s="T155">ol</ta>
            <ta e="T157" id="Seg_3286" s="T520">ikki</ta>
            <ta e="T158" id="Seg_3287" s="T157">emeːksin</ta>
            <ta e="T159" id="Seg_3288" s="T158">kallaːn</ta>
            <ta e="T160" id="Seg_3289" s="T159">di͡ek</ta>
            <ta e="T161" id="Seg_3290" s="T160">nʼɨlaj-an</ta>
            <ta e="T162" id="Seg_3291" s="T161">baraːn</ta>
            <ta e="T163" id="Seg_3292" s="T162">üːr-püt</ta>
            <ta e="T164" id="Seg_3293" s="T163">taba</ta>
            <ta e="T165" id="Seg_3294" s="T164">kördük</ta>
            <ta e="T166" id="Seg_3295" s="T165">nʼirgij-er</ta>
            <ta e="T167" id="Seg_3296" s="T166">tɨ͡ah-ɨ</ta>
            <ta e="T168" id="Seg_3297" s="T167">ihilleː-n</ta>
            <ta e="T170" id="Seg_3298" s="T168">bu͡ol-but-tar</ta>
            <ta e="T171" id="Seg_3299" s="T170">kumaːr</ta>
            <ta e="T172" id="Seg_3300" s="T171">daː</ta>
            <ta e="T173" id="Seg_3301" s="T172">hiː-r-i-n</ta>
            <ta e="T174" id="Seg_3302" s="T173">bil-bekke</ta>
            <ta e="T175" id="Seg_3303" s="T174">ör-köːn</ta>
            <ta e="T176" id="Seg_3304" s="T175">olor-but-tar</ta>
            <ta e="T177" id="Seg_3305" s="T176">anʼak-tarɨ-n</ta>
            <ta e="T178" id="Seg_3306" s="T177">at-an</ta>
            <ta e="T179" id="Seg_3307" s="T178">baraːn</ta>
            <ta e="T180" id="Seg_3308" s="T179">attɨ-larɨ-gar</ta>
            <ta e="T181" id="Seg_3309" s="T180">tiriː-ler</ta>
            <ta e="T182" id="Seg_3310" s="T181">loskutu͡ok-tara</ta>
            <ta e="T183" id="Seg_3311" s="T182">onno-manna</ta>
            <ta e="T184" id="Seg_3312" s="T183">hɨt-al-lar</ta>
            <ta e="T185" id="Seg_3313" s="T184">bɨ͡a-nnan</ta>
            <ta e="T186" id="Seg_3314" s="T185">baːj-ɨː-laːk</ta>
            <ta e="T187" id="Seg_3315" s="T186">mordʼoː-loro</ta>
            <ta e="T188" id="Seg_3316" s="T187">iliː-leri-ger</ta>
            <ta e="T189" id="Seg_3317" s="T188">buru͡olah-al-lar</ta>
            <ta e="T190" id="Seg_3318" s="T189">küːp-püt</ta>
            <ta e="T191" id="Seg_3319" s="T190">kördük</ta>
            <ta e="T192" id="Seg_3320" s="T191">sɨp-sɨp</ta>
            <ta e="T193" id="Seg_3321" s="T192">kahan</ta>
            <ta e="T194" id="Seg_3322" s="T193">olor-u͡ok-tarɨ-n</ta>
            <ta e="T195" id="Seg_3323" s="T194">kirdik</ta>
            <ta e="T196" id="Seg_3324" s="T195">ginner</ta>
            <ta e="T197" id="Seg_3325" s="T196">bil-betek</ta>
            <ta e="T198" id="Seg_3326" s="T197">tɨ͡as-tara</ta>
            <ta e="T199" id="Seg_3327" s="T198">bu͡ol-but</ta>
            <ta e="T200" id="Seg_3328" s="T199">kaja</ta>
            <ta e="T201" id="Seg_3329" s="T200">da</ta>
            <ta e="T202" id="Seg_3330" s="T201">dʼe</ta>
            <ta e="T203" id="Seg_3331" s="T202">öl-lü-büt</ta>
            <ta e="T204" id="Seg_3332" s="T203">d-iː</ta>
            <ta e="T205" id="Seg_3333" s="T204">tüh-eːt</ta>
            <ta e="T206" id="Seg_3334" s="T205">biːrges</ta>
            <ta e="T207" id="Seg_3335" s="T206">emeːksin</ta>
            <ta e="T208" id="Seg_3336" s="T207">tur-a</ta>
            <ta e="T209" id="Seg_3337" s="T208">ekkireː-bit</ta>
            <ta e="T210" id="Seg_3338" s="T209">mas</ta>
            <ta e="T211" id="Seg_3339" s="T210">di͡ek</ta>
            <ta e="T212" id="Seg_3340" s="T211">körüleː-bit</ta>
            <ta e="T213" id="Seg_3341" s="T212">dʼe</ta>
            <ta e="T214" id="Seg_3342" s="T213">köt-ön</ta>
            <ta e="T215" id="Seg_3343" s="T214">ih-er</ta>
            <ta e="T216" id="Seg_3344" s="T215">uluː</ta>
            <ta e="T217" id="Seg_3345" s="T216">kötör-ö</ta>
            <ta e="T218" id="Seg_3346" s="T217">kör</ta>
            <ta e="T219" id="Seg_3347" s="T218">ol</ta>
            <ta e="T220" id="Seg_3348" s="T219">kɨtar-an</ta>
            <ta e="T221" id="Seg_3349" s="T220">ih-er</ta>
            <ta e="T222" id="Seg_3350" s="T221">gini</ta>
            <ta e="T223" id="Seg_3351" s="T222">sih-i-n</ta>
            <ta e="T224" id="Seg_3352" s="T223">tɨ͡ah-a</ta>
            <ta e="T225" id="Seg_3353" s="T224">e-bit</ta>
            <ta e="T226" id="Seg_3354" s="T225">xoːspodi</ta>
            <ta e="T227" id="Seg_3355" s="T226">min</ta>
            <ta e="T228" id="Seg_3356" s="T227">haŋar-batag-ɨ-m</ta>
            <ta e="T229" id="Seg_3357" s="T228">kör-bötög-ü-m</ta>
            <ta e="T230" id="Seg_3358" s="T229">bu͡ollun</ta>
            <ta e="T231" id="Seg_3359" s="T230">di͡e-n</ta>
            <ta e="T232" id="Seg_3360" s="T231">emeːksin</ta>
            <ta e="T233" id="Seg_3361" s="T232">labɨj-an</ta>
            <ta e="T234" id="Seg_3362" s="T233">bar-bɨt</ta>
            <ta e="T235" id="Seg_3363" s="T234">o</ta>
            <ta e="T236" id="Seg_3364" s="T235">tu͡ok</ta>
            <ta e="T237" id="Seg_3365" s="T236">kötör-ü-n</ta>
            <ta e="T238" id="Seg_3366" s="T237">gɨn-a-gɨn</ta>
            <ta e="T239" id="Seg_3367" s="T238">d-iː-d-iː</ta>
            <ta e="T240" id="Seg_3368" s="T239">dogor-o</ta>
            <ta e="T241" id="Seg_3369" s="T240">emi͡e</ta>
            <ta e="T242" id="Seg_3370" s="T241">körüleː-bit</ta>
            <ta e="T243" id="Seg_3371" s="T242">atak-tarɨ-n</ta>
            <ta e="T244" id="Seg_3372" s="T243">annɨ-larɨ-gar</ta>
            <ta e="T245" id="Seg_3373" s="T244">hir-dere</ta>
            <ta e="T246" id="Seg_3374" s="T245">möŋ-ör</ta>
            <ta e="T247" id="Seg_3375" s="T246">kördük</ta>
            <ta e="T248" id="Seg_3376" s="T247">bu͡ol-but</ta>
            <ta e="T249" id="Seg_3377" s="T248">uraha-lar</ta>
            <ta e="T250" id="Seg_3378" s="T249">titireː-bit-ter</ta>
            <ta e="T251" id="Seg_3379" s="T250">türgen-nik</ta>
            <ta e="T252" id="Seg_3380" s="T251">hɨrga</ta>
            <ta e="T253" id="Seg_3381" s="T252">di͡ek</ta>
            <ta e="T254" id="Seg_3382" s="T253">annɨ-tɨ-gar</ta>
            <ta e="T255" id="Seg_3383" s="T254">d-iː</ta>
            <ta e="T256" id="Seg_3384" s="T255">tüh-eːt</ta>
            <ta e="T257" id="Seg_3385" s="T256">biːrgeh-e</ta>
            <ta e="T258" id="Seg_3386" s="T257">čip</ta>
            <ta e="T259" id="Seg_3387" s="T258">gɨm-mɨt</ta>
            <ta e="T260" id="Seg_3388" s="T259">katɨː-lar-daːk</ta>
            <ta e="T261" id="Seg_3389" s="T260">mu͡osta</ta>
            <ta e="T262" id="Seg_3390" s="T261">ann-ɨ-gar</ta>
            <ta e="T263" id="Seg_3391" s="T262">dogor-o</ta>
            <ta e="T264" id="Seg_3392" s="T263">gini-ni</ta>
            <ta e="T265" id="Seg_3393" s="T264">köŋüj-en</ta>
            <ta e="T266" id="Seg_3394" s="T265">kiːr-bit</ta>
            <ta e="T267" id="Seg_3395" s="T266">bu͡olag-ɨ-n</ta>
            <ta e="T268" id="Seg_3396" s="T267">ann-ɨ-gar</ta>
            <ta e="T269" id="Seg_3397" s="T268">ol</ta>
            <ta e="T270" id="Seg_3398" s="T269">kem</ta>
            <ta e="T271" id="Seg_3399" s="T270">lü͡ötčik</ta>
            <ta e="T272" id="Seg_3400" s="T271">haŋardɨː</ta>
            <ta e="T273" id="Seg_3401" s="T272">uraha</ta>
            <ta e="T274" id="Seg_3402" s="T273">dʼi͡e-ler-i</ta>
            <ta e="T275" id="Seg_3403" s="T274">kör-ö</ta>
            <ta e="T276" id="Seg_3404" s="T275">hotoru</ta>
            <ta e="T277" id="Seg_3405" s="T276">nʼamčɨgas</ta>
            <ta e="T278" id="Seg_3406" s="T277">bagajɨ</ta>
            <ta e="T279" id="Seg_3407" s="T278">köt-ön</ta>
            <ta e="T280" id="Seg_3408" s="T279">aːs-pɨt</ta>
            <ta e="T281" id="Seg_3409" s="T280">hɨrga</ta>
            <ta e="T282" id="Seg_3410" s="T281">dʼi͡e-ni</ta>
            <ta e="T283" id="Seg_3411" s="T282">gɨtta</ta>
            <ta e="T284" id="Seg_3412" s="T283">bu͡olak</ta>
            <ta e="T285" id="Seg_3413" s="T284">annɨ-larɨ-ttan</ta>
            <ta e="T288" id="Seg_3414" s="T287">emeːksit-ter</ta>
            <ta e="T289" id="Seg_3415" s="T288">harɨː</ta>
            <ta e="T290" id="Seg_3416" s="T289">atak-tara</ta>
            <ta e="T291" id="Seg_3417" s="T290">möm-pökkö</ta>
            <ta e="T292" id="Seg_3418" s="T291">čoroh-o</ta>
            <ta e="T293" id="Seg_3419" s="T292">hɨp-pɨt-tar</ta>
            <ta e="T294" id="Seg_3420" s="T293">kɨtɨl-ga</ta>
            <ta e="T295" id="Seg_3421" s="T294">ogonnʼot-tor</ta>
            <ta e="T296" id="Seg_3422" s="T295">ekkir-iː-ekkir-iː</ta>
            <ta e="T297" id="Seg_3423" s="T296">bergehe-leri-n</ta>
            <ta e="T298" id="Seg_3424" s="T297">samalʼok-ka</ta>
            <ta e="T299" id="Seg_3425" s="T298">dajb-ɨː</ta>
            <ta e="T300" id="Seg_3426" s="T299">tur-but-tar</ta>
            <ta e="T301" id="Seg_3427" s="T300">ginner</ta>
            <ta e="T302" id="Seg_3428" s="T301">ü͡ör-üː-lere</ta>
            <ta e="T303" id="Seg_3429" s="T302">haŋardɨː</ta>
            <ta e="T304" id="Seg_3430" s="T303">samalʼot</ta>
            <ta e="T305" id="Seg_3431" s="T304">kel-er-i-ger</ta>
            <ta e="T306" id="Seg_3432" s="T305">mu͡ora-ga</ta>
            <ta e="T307" id="Seg_3433" s="T306">bert</ta>
            <ta e="T308" id="Seg_3434" s="T307">e-bit</ta>
            <ta e="T309" id="Seg_3435" s="T308">bil-el-ler</ta>
            <ta e="T312" id="Seg_3436" s="T311">bil-s-er</ta>
            <ta e="T313" id="Seg_3437" s="T312">bu͡ol-a</ta>
            <ta e="T314" id="Seg_3438" s="T313">hɨldʼ-ɨ͡ag-a</ta>
            <ta e="T315" id="Seg_3439" s="T314">kajtak</ta>
            <ta e="T316" id="Seg_3440" s="T315">taba-hɨt-tar</ta>
            <ta e="T317" id="Seg_3441" s="T316">balɨk-čɨt-tar</ta>
            <ta e="T318" id="Seg_3442" s="T317">olor-ol-loru-n</ta>
            <ta e="T319" id="Seg_3443" s="T318">kör-öːrü</ta>
            <ta e="T320" id="Seg_3444" s="T319">hɨːr</ta>
            <ta e="T321" id="Seg_3445" s="T320">ürd-ü-ger</ta>
            <ta e="T322" id="Seg_3446" s="T321">taks-aːt</ta>
            <ta e="T323" id="Seg_3447" s="T322">oduːl-a-m-mɨt-tar</ta>
            <ta e="T324" id="Seg_3448" s="T323">uraha</ta>
            <ta e="T325" id="Seg_3449" s="T324">dʼi͡e-lere</ta>
            <ta e="T326" id="Seg_3450" s="T325">buru͡olaː-bat-tar</ta>
            <ta e="T327" id="Seg_3451" s="T326">ti͡ergeːn</ta>
            <ta e="T328" id="Seg_3452" s="T327">ürd-ü-nen</ta>
            <ta e="T331" id="Seg_3453" s="T330">im</ta>
            <ta e="T332" id="Seg_3454" s="T331">dʼɨlɨ-ččɨ</ta>
            <ta e="T333" id="Seg_3455" s="T332">ɨt</ta>
            <ta e="T334" id="Seg_3456" s="T333">da</ta>
            <ta e="T335" id="Seg_3457" s="T334">ür-bet</ta>
            <ta e="T336" id="Seg_3458" s="T335">kihi</ta>
            <ta e="T337" id="Seg_3459" s="T336">da</ta>
            <ta e="T338" id="Seg_3460" s="T337">haŋa-ta</ta>
            <ta e="T339" id="Seg_3461" s="T338">ihill-i-bet</ta>
            <ta e="T340" id="Seg_3462" s="T339">tu͡ok</ta>
            <ta e="T341" id="Seg_3463" s="T340">dʼo-nor</ta>
            <ta e="T342" id="Seg_3464" s="T341">bu</ta>
            <ta e="T343" id="Seg_3465" s="T342">emeːksit-ter</ta>
            <ta e="T344" id="Seg_3466" s="T343">bil-bet-ter</ta>
            <ta e="T345" id="Seg_3467" s="T344">du͡o</ta>
            <ta e="T346" id="Seg_3468" s="T345">bihigi</ta>
            <ta e="T347" id="Seg_3469" s="T346">kel-i͡ek-piti-n</ta>
            <ta e="T348" id="Seg_3470" s="T347">di͡e-bit</ta>
            <ta e="T349" id="Seg_3471" s="T348">Onoː</ta>
            <ta e="T350" id="Seg_3472" s="T349">ogonnʼor</ta>
            <ta e="T351" id="Seg_3473" s="T350">kanna</ta>
            <ta e="T352" id="Seg_3474" s="T351">baːr-gɨt=ɨːj</ta>
            <ta e="T353" id="Seg_3475" s="T352">dʼi͡e-leːk-ter</ta>
            <ta e="T356" id="Seg_3476" s="T355">ü͡ögüleː-bit</ta>
            <ta e="T357" id="Seg_3477" s="T356">ɨ͡al</ta>
            <ta e="T358" id="Seg_3478" s="T357">kihi-te</ta>
            <ta e="T359" id="Seg_3479" s="T358">ara</ta>
            <ta e="T360" id="Seg_3480" s="T359">abɨraː-ŋ</ta>
            <ta e="T361" id="Seg_3481" s="T360">araːr-ɨ-ŋ</ta>
            <ta e="T362" id="Seg_3482" s="T361">öl-lü-büt</ta>
            <ta e="T363" id="Seg_3483" s="T362">hir</ta>
            <ta e="T364" id="Seg_3484" s="T363">ih-i-tten</ta>
            <ta e="T365" id="Seg_3485" s="T364">ü͡ögüː</ta>
            <ta e="T366" id="Seg_3486" s="T365">ihill-i-bit</ta>
            <ta e="T367" id="Seg_3487" s="T366">tu͡ok-tar=ɨj</ta>
            <ta e="T368" id="Seg_3488" s="T367">iːr-bit-ter</ta>
            <ta e="T369" id="Seg_3489" s="T368">du͡o</ta>
            <ta e="T370" id="Seg_3490" s="T369">d-iː</ta>
            <ta e="T371" id="Seg_3491" s="T370">tüh-eːt</ta>
            <ta e="T372" id="Seg_3492" s="T371">Onoː</ta>
            <ta e="T373" id="Seg_3493" s="T372">körüleː-bit</ta>
            <ta e="T374" id="Seg_3494" s="T373">gini</ta>
            <ta e="T375" id="Seg_3495" s="T374">hɨrga</ta>
            <ta e="T376" id="Seg_3496" s="T375">dʼi͡e-ler</ta>
            <ta e="T377" id="Seg_3497" s="T376">annɨ-larɨ-gar</ta>
            <ta e="T378" id="Seg_3498" s="T377">körüleː-bit</ta>
            <ta e="T379" id="Seg_3499" s="T378">tu͡ok</ta>
            <ta e="T380" id="Seg_3500" s="T379">dikti-te</ta>
            <ta e="T381" id="Seg_3501" s="T380">bu͡ol-l-a</ta>
            <ta e="T382" id="Seg_3502" s="T381">emeːksit-teri-n</ta>
            <ta e="T383" id="Seg_3503" s="T382">atak-tara</ta>
            <ta e="T384" id="Seg_3504" s="T383">onno</ta>
            <ta e="T385" id="Seg_3505" s="T384">halɨbɨr-a-h-a</ta>
            <ta e="T386" id="Seg_3506" s="T385">hɨp-pɨt-tar</ta>
            <ta e="T387" id="Seg_3507" s="T386">kör-dög-üne</ta>
            <ta e="T388" id="Seg_3508" s="T387">taks-a</ta>
            <ta e="T389" id="Seg_3509" s="T388">hatɨː-l-lar</ta>
            <ta e="T390" id="Seg_3510" s="T389">bɨhɨlaːk</ta>
            <ta e="T391" id="Seg_3511" s="T390">katɨː-lar</ta>
            <ta e="T392" id="Seg_3512" s="T391">taŋas-tarɨ-ttan</ta>
            <ta e="T393" id="Seg_3513" s="T392">iːl-bit-ter</ta>
            <ta e="T394" id="Seg_3514" s="T393">togo</ta>
            <ta e="T395" id="Seg_3515" s="T394">ɨːt-ɨ͡ak-tara=j</ta>
            <ta e="T396" id="Seg_3516" s="T395">is-ke</ta>
            <ta e="T397" id="Seg_3517" s="T396">da</ta>
            <ta e="T398" id="Seg_3518" s="T397">kiːl-ler-bet-ter</ta>
            <ta e="T399" id="Seg_3519" s="T398">tas-ka</ta>
            <ta e="T400" id="Seg_3520" s="T399">da</ta>
            <ta e="T401" id="Seg_3521" s="T400">tahaːr-bat-tar</ta>
            <ta e="T402" id="Seg_3522" s="T401">ara</ta>
            <ta e="T403" id="Seg_3523" s="T402">d-e-h-iː</ta>
            <ta e="T404" id="Seg_3524" s="T403">elbeː-n</ta>
            <ta e="T405" id="Seg_3525" s="T404">is-pit</ta>
            <ta e="T406" id="Seg_3526" s="T405">ɨnčɨktɨː-r</ta>
            <ta e="T407" id="Seg_3527" s="T406">kojd-u-but</ta>
            <ta e="T408" id="Seg_3528" s="T407">ogonnʼot-tor</ta>
            <ta e="T409" id="Seg_3529" s="T408">tu͡ok</ta>
            <ta e="T410" id="Seg_3530" s="T409">da</ta>
            <ta e="T411" id="Seg_3531" s="T410">di͡ek</ta>
            <ta e="T412" id="Seg_3532" s="T411">berd-i-tten</ta>
            <ta e="T413" id="Seg_3533" s="T412">sög-ö</ta>
            <ta e="T414" id="Seg_3534" s="T413">hataː-n</ta>
            <ta e="T415" id="Seg_3535" s="T414">tug-u</ta>
            <ta e="T416" id="Seg_3536" s="T415">da</ta>
            <ta e="T417" id="Seg_3537" s="T416">haŋar-bat</ta>
            <ta e="T418" id="Seg_3538" s="T417">bu͡ol-but-tar</ta>
            <ta e="T419" id="Seg_3539" s="T418">emeːksit-ter-i</ta>
            <ta e="T420" id="Seg_3540" s="T419">biːr-diː</ta>
            <ta e="T421" id="Seg_3541" s="T420">boskoloː-but-tar</ta>
            <ta e="T422" id="Seg_3542" s="T421">ginner-ge</ta>
            <ta e="T423" id="Seg_3543" s="T422">hɨraj</ta>
            <ta e="T424" id="Seg_3544" s="T423">kel-i͡e</ta>
            <ta e="T425" id="Seg_3545" s="T424">du͡o</ta>
            <ta e="T426" id="Seg_3546" s="T425">as-tara</ta>
            <ta e="T427" id="Seg_3547" s="T426">karak-tarɨ-n</ta>
            <ta e="T428" id="Seg_3548" s="T427">hap-pɨt</ta>
            <ta e="T429" id="Seg_3549" s="T428">harɨː</ta>
            <ta e="T430" id="Seg_3550" s="T429">atak-tara</ta>
            <ta e="T431" id="Seg_3551" s="T430">nʼɨpka-ččɨ</ta>
            <ta e="T432" id="Seg_3552" s="T431">tüs-püt-ter</ta>
            <ta e="T433" id="Seg_3553" s="T432">dʼühün-nere</ta>
            <ta e="T434" id="Seg_3554" s="T433">ularɨj-bɨt</ta>
            <ta e="T435" id="Seg_3555" s="T434">bu͡or-tan</ta>
            <ta e="T436" id="Seg_3556" s="T435">taks-ɨ-bɨt</ta>
            <ta e="T437" id="Seg_3557" s="T436">kɨrsa</ta>
            <ta e="T438" id="Seg_3558" s="T437">ogo-lor-u-n</ta>
            <ta e="T439" id="Seg_3559" s="T438">kördük</ta>
            <ta e="T440" id="Seg_3560" s="T439">bu͡ol-but-tar</ta>
            <ta e="T441" id="Seg_3561" s="T440">kaja</ta>
            <ta e="T442" id="Seg_3562" s="T441">tu͡ok</ta>
            <ta e="T443" id="Seg_3563" s="T442">bu͡ol-buk-kut=uj</ta>
            <ta e="T444" id="Seg_3564" s="T443">di͡e-n</ta>
            <ta e="T445" id="Seg_3565" s="T444">ɨjɨp-pɨt-tar</ta>
            <ta e="T446" id="Seg_3566" s="T445">biːr-diː</ta>
            <ta e="T447" id="Seg_3567" s="T446">duruk</ta>
            <ta e="T448" id="Seg_3568" s="T447">učum</ta>
            <ta e="T449" id="Seg_3569" s="T448">bil-betek</ta>
            <ta e="T450" id="Seg_3570" s="T449">kötör-büt</ta>
            <ta e="T451" id="Seg_3571" s="T450">aːs-t-a</ta>
            <ta e="T452" id="Seg_3572" s="T451">di͡e-n</ta>
            <ta e="T453" id="Seg_3573" s="T452">nʼɨŋɨnas-pɨt-tar</ta>
            <ta e="T454" id="Seg_3574" s="T453">ikki</ta>
            <ta e="T455" id="Seg_3575" s="T454">emeːksin</ta>
            <ta e="T456" id="Seg_3576" s="T455">oː</ta>
            <ta e="T457" id="Seg_3577" s="T456">bil-betek</ta>
            <ta e="T458" id="Seg_3578" s="T457">bu͡ol-an</ta>
            <ta e="T459" id="Seg_3579" s="T458">Onoː</ta>
            <ta e="T460" id="Seg_3580" s="T459">heŋeːr-bit</ta>
            <ta e="T461" id="Seg_3581" s="T460">dʼe</ta>
            <ta e="T462" id="Seg_3582" s="T461">kepseː-ŋ</ta>
            <ta e="T463" id="Seg_3583" s="T462">tu͡ok</ta>
            <ta e="T464" id="Seg_3584" s="T463">tüːl-ü-n</ta>
            <ta e="T465" id="Seg_3585" s="T464">tüheː-ti-git</ta>
            <ta e="T466" id="Seg_3586" s="T465">hɨrga</ta>
            <ta e="T467" id="Seg_3587" s="T466">dʼi͡e</ta>
            <ta e="T468" id="Seg_3588" s="T467">ann-ɨ-gar</ta>
            <ta e="T469" id="Seg_3589" s="T468">hɨt-aŋ-ŋɨt</ta>
            <ta e="T470" id="Seg_3590" s="T469">bu</ta>
            <ta e="T471" id="Seg_3591" s="T470">tu͡ok</ta>
            <ta e="T472" id="Seg_3592" s="T471">bu͡ol-ar</ta>
            <ta e="T473" id="Seg_3593" s="T472">ogonnʼor=uj</ta>
            <ta e="T474" id="Seg_3594" s="T473">kɨrdʼ-an</ta>
            <ta e="T475" id="Seg_3595" s="T474">öj-ö</ta>
            <ta e="T476" id="Seg_3596" s="T475">kɨlgɨː-r</ta>
            <ta e="T477" id="Seg_3597" s="T476">bɨhɨlaːk</ta>
            <ta e="T478" id="Seg_3598" s="T477">agdas-pɨt-tar</ta>
            <ta e="T479" id="Seg_3599" s="T478">ikki</ta>
            <ta e="T480" id="Seg_3600" s="T479">kuttannʼak-tar</ta>
            <ta e="T481" id="Seg_3601" s="T480">hi</ta>
            <ta e="T482" id="Seg_3602" s="T481">uskaːn</ta>
            <ta e="T483" id="Seg_3603" s="T482">hürek-tere</ta>
            <ta e="T484" id="Seg_3604" s="T483">össü͡ö</ta>
            <ta e="T485" id="Seg_3605" s="T484">haŋa-laːk</ta>
            <ta e="T486" id="Seg_3606" s="T485">bu͡ol-but-tar</ta>
            <ta e="T487" id="Seg_3607" s="T486">bil-bek-kit</ta>
            <ta e="T488" id="Seg_3608" s="T487">du͡o</ta>
            <ta e="T489" id="Seg_3609" s="T488">üje</ta>
            <ta e="T490" id="Seg_3610" s="T489">ularɨj-bɨt-ɨ-n</ta>
            <ta e="T491" id="Seg_3611" s="T490">savʼet-tar</ta>
            <ta e="T492" id="Seg_3612" s="T491">čiːčaːk-tara</ta>
            <ta e="T493" id="Seg_3613" s="T492">kel-er-i-n</ta>
            <ta e="T494" id="Seg_3614" s="T493">in-ti-git</ta>
            <ta e="T495" id="Seg_3615" s="T494">ehigi-ni</ta>
            <ta e="T496" id="Seg_3616" s="T495">abɨr-ɨː</ta>
            <ta e="T497" id="Seg_3617" s="T496">köt-ö</ta>
            <ta e="T498" id="Seg_3618" s="T497">hɨldʼ-ar</ta>
            <ta e="T499" id="Seg_3619" s="T498">tɨ͡a</ta>
            <ta e="T500" id="Seg_3620" s="T499">dʼon-o</ta>
            <ta e="T501" id="Seg_3621" s="T500">tot</ta>
            <ta e="T502" id="Seg_3622" s="T501">baːj</ta>
            <ta e="T503" id="Seg_3623" s="T502">olor-u͡ok-tar-ɨ-n</ta>
            <ta e="T504" id="Seg_3624" s="T503">d-e-s-pit-ter</ta>
            <ta e="T505" id="Seg_3625" s="T504">ikki</ta>
            <ta e="T506" id="Seg_3626" s="T505">kihi</ta>
            <ta e="T507" id="Seg_3627" s="T506">emeːksit-ter</ta>
            <ta e="T508" id="Seg_3628" s="T507">tu͡ok</ta>
            <ta e="T509" id="Seg_3629" s="T508">daː</ta>
            <ta e="T510" id="Seg_3630" s="T509">d-i͡ek-teri-n</ta>
            <ta e="T511" id="Seg_3631" s="T510">berd-i-tten</ta>
            <ta e="T512" id="Seg_3632" s="T511">haŋa-ta</ta>
            <ta e="T513" id="Seg_3633" s="T512">hu͡ok</ta>
            <ta e="T514" id="Seg_3634" s="T513">haːp-put</ta>
            <ta e="T515" id="Seg_3635" s="T514">kördük</ta>
            <ta e="T516" id="Seg_3636" s="T515">dʼi͡e-leri-n</ta>
            <ta e="T517" id="Seg_3637" s="T516">di͡ek</ta>
            <ta e="T518" id="Seg_3638" s="T517">ökö-h-ö</ta>
            <ta e="T519" id="Seg_3639" s="T518">tur-but-tar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3640" s="T0">savʼet-LAr</ta>
            <ta e="T2" id="Seg_3641" s="T1">ɨjaːk-LArA</ta>
            <ta e="T3" id="Seg_3642" s="T2">haŋardɨː</ta>
            <ta e="T4" id="Seg_3643" s="T3">kel-Ar-tI-GAr</ta>
            <ta e="T5" id="Seg_3644" s="T4">Kurja</ta>
            <ta e="T6" id="Seg_3645" s="T5">di͡e-An</ta>
            <ta e="T7" id="Seg_3646" s="T6">olok-GA</ta>
            <ta e="T8" id="Seg_3647" s="T7">hajɨn</ta>
            <ta e="T9" id="Seg_3648" s="T8">aːjɨ</ta>
            <ta e="T10" id="Seg_3649" s="T9">balɨktaː-A</ta>
            <ta e="T11" id="Seg_3650" s="T10">olor-AːččI</ta>
            <ta e="T12" id="Seg_3651" s="T11">e-TI-LArA</ta>
            <ta e="T13" id="Seg_3652" s="T12">haka</ta>
            <ta e="T14" id="Seg_3653" s="T13">dʼon-tA</ta>
            <ta e="T15" id="Seg_3654" s="T14">tu͡ora</ta>
            <ta e="T16" id="Seg_3655" s="T15">karak-LAːK</ta>
            <ta e="T17" id="Seg_3656" s="T16">čubu-čubu</ta>
            <ta e="T18" id="Seg_3657" s="T17">hɨrɨt-I-BAT</ta>
            <ta e="T19" id="Seg_3658" s="T18">olok-tA</ta>
            <ta e="T20" id="Seg_3659" s="T19">onnuk</ta>
            <ta e="T21" id="Seg_3660" s="T20">ɨraːk</ta>
            <ta e="T22" id="Seg_3661" s="T21">balɨktaː-Ar</ta>
            <ta e="T23" id="Seg_3662" s="T22">hir-LArA</ta>
            <ta e="T24" id="Seg_3663" s="T23">kahan</ta>
            <ta e="T25" id="Seg_3664" s="T24">ere</ta>
            <ta e="T26" id="Seg_3665" s="T25">kim</ta>
            <ta e="T27" id="Seg_3666" s="T26">eme</ta>
            <ta e="T28" id="Seg_3667" s="T27">kel-IAK.[tA]</ta>
            <ta e="T29" id="Seg_3668" s="T28">hajɨn</ta>
            <ta e="T30" id="Seg_3669" s="T29">hugas-LAːgI</ta>
            <ta e="T31" id="Seg_3670" s="T30">hɨtɨnnʼak-LAr-GA</ta>
            <ta e="T32" id="Seg_3671" s="T31">ös</ta>
            <ta e="T33" id="Seg_3672" s="T32">kepseː-A</ta>
            <ta e="T34" id="Seg_3673" s="T33">beje-tI-n</ta>
            <ta e="T35" id="Seg_3674" s="T34">olok-tI-n</ta>
            <ta e="T36" id="Seg_3675" s="T35">tus-tI-nAn</ta>
            <ta e="T37" id="Seg_3676" s="T36">bil-AːrI</ta>
            <ta e="T38" id="Seg_3677" s="T37">kajdak</ta>
            <ta e="T39" id="Seg_3678" s="T38">balɨktaː-Ar-LArI-n</ta>
            <ta e="T40" id="Seg_3679" s="T39">Kurja</ta>
            <ta e="T41" id="Seg_3680" s="T40">dʼon-tA</ta>
            <ta e="T42" id="Seg_3681" s="T41">biːr</ta>
            <ta e="T43" id="Seg_3682" s="T42">hajɨn</ta>
            <ta e="T44" id="Seg_3683" s="T43">ogonnʼor-LAr</ta>
            <ta e="T45" id="Seg_3684" s="T44">erde</ta>
            <ta e="T46" id="Seg_3685" s="T45">harsi͡erda</ta>
            <ta e="T47" id="Seg_3686" s="T46">ert-I-n-An</ta>
            <ta e="T48" id="Seg_3687" s="T47">kaːl-BIT-LArA</ta>
            <ta e="T49" id="Seg_3688" s="T48">biːske-LAr-tI-n</ta>
            <ta e="T50" id="Seg_3689" s="T49">kör-I-n-A</ta>
            <ta e="T51" id="Seg_3690" s="T50">dʼaktar</ta>
            <ta e="T52" id="Seg_3691" s="T51">ogo</ta>
            <ta e="T53" id="Seg_3692" s="T52">ajmak</ta>
            <ta e="T54" id="Seg_3693" s="T53">dʼi͡e-LArI-GAr</ta>
            <ta e="T55" id="Seg_3694" s="T54">olor-BIT-LArA</ta>
            <ta e="T56" id="Seg_3695" s="T55">kim</ta>
            <ta e="T57" id="Seg_3696" s="T56">balɨk</ta>
            <ta e="T58" id="Seg_3697" s="T57">ɨraːstaː-A-n-Ar</ta>
            <ta e="T59" id="Seg_3698" s="T58">kim</ta>
            <ta e="T60" id="Seg_3699" s="T59">dʼukula</ta>
            <ta e="T61" id="Seg_3700" s="T60">kuːrt-I-n-Ar</ta>
            <ta e="T62" id="Seg_3701" s="T61">ol-LAr-ttAn</ta>
            <ta e="T63" id="Seg_3702" s="T62">ikki</ta>
            <ta e="T64" id="Seg_3703" s="T63">emeːksin</ta>
            <ta e="T65" id="Seg_3704" s="T64">ogonnʼor-LAr</ta>
            <ta e="T66" id="Seg_3705" s="T65">kel-IAK-LArI-GAr</ta>
            <ta e="T67" id="Seg_3706" s="T66">di͡eri</ta>
            <ta e="T68" id="Seg_3707" s="T67">hɨrga</ta>
            <ta e="T69" id="Seg_3708" s="T68">dʼi͡e-LArI-n</ta>
            <ta e="T70" id="Seg_3709" s="T69">tiriː-LArI-n</ta>
            <ta e="T71" id="Seg_3710" s="T70">abɨraktaː-Ar-LAr</ta>
            <ta e="T72" id="Seg_3711" s="T71">e-BIT</ta>
            <ta e="T73" id="Seg_3712" s="T72">kunʼaːs</ta>
            <ta e="T74" id="Seg_3713" s="T73">bagajɨ</ta>
            <ta e="T75" id="Seg_3714" s="T74">kün</ta>
            <ta e="T76" id="Seg_3715" s="T75">tur-BIT-tA</ta>
            <ta e="T77" id="Seg_3716" s="T76">Dudupta</ta>
            <ta e="T78" id="Seg_3717" s="T77">aːn</ta>
            <ta e="T79" id="Seg_3718" s="T78">ebe-LArI-n</ta>
            <ta e="T80" id="Seg_3719" s="T79">uː-tA</ta>
            <ta e="T81" id="Seg_3720" s="T80">čumpu-tI-GAr</ta>
            <ta e="T82" id="Seg_3721" s="T81">ürüŋ</ta>
            <ta e="T83" id="Seg_3722" s="T82">etiŋ</ta>
            <ta e="T84" id="Seg_3723" s="T83">bɨlɨt-LAr-tA</ta>
            <ta e="T85" id="Seg_3724" s="T84">bɨdʼɨras-An</ta>
            <ta e="T86" id="Seg_3725" s="T85">köhün-Ar-LAr</ta>
            <ta e="T87" id="Seg_3726" s="T86">kumaːr</ta>
            <ta e="T88" id="Seg_3727" s="T87">kotoku</ta>
            <ta e="T89" id="Seg_3728" s="T88">kɨnat-tA</ta>
            <ta e="T90" id="Seg_3729" s="T89">kat-Ar</ta>
            <ta e="T91" id="Seg_3730" s="T90">kunʼaːs-tA</ta>
            <ta e="T92" id="Seg_3731" s="T91">tur-BIT-tA</ta>
            <ta e="T93" id="Seg_3732" s="T92">emeːksin-LAr-tA</ta>
            <ta e="T94" id="Seg_3733" s="T93">ol</ta>
            <ta e="T95" id="Seg_3734" s="T94">da</ta>
            <ta e="T96" id="Seg_3735" s="T95">bu͡ol-TAR</ta>
            <ta e="T97" id="Seg_3736" s="T96">iliː-LArA</ta>
            <ta e="T98" id="Seg_3737" s="T97">holo-tA</ta>
            <ta e="T99" id="Seg_3738" s="T98">hu͡ok</ta>
            <ta e="T100" id="Seg_3739" s="T99">ilbin-Ar-LAr</ta>
            <ta e="T101" id="Seg_3740" s="T100">ol</ta>
            <ta e="T102" id="Seg_3741" s="T101">bɨːs-tI-GAr</ta>
            <ta e="T103" id="Seg_3742" s="T102">huptu</ta>
            <ta e="T104" id="Seg_3743" s="T103">tik-n-A</ta>
            <ta e="T105" id="Seg_3744" s="T104">bu͡ol-BIT-LArA</ta>
            <ta e="T106" id="Seg_3745" s="T105">iŋne-LArI-n</ta>
            <ta e="T107" id="Seg_3746" s="T106">tɨ͡as-tA</ta>
            <ta e="T108" id="Seg_3747" s="T107">öŋnöːk</ta>
            <ta e="T109" id="Seg_3748" s="T108">tiriː-GA</ta>
            <ta e="T110" id="Seg_3749" s="T109">kurdurgaː-Ar-tA</ta>
            <ta e="T111" id="Seg_3750" s="T110">ɨraːk-ttAn</ta>
            <ta e="T112" id="Seg_3751" s="T111">ihilin-Ar</ta>
            <ta e="T113" id="Seg_3752" s="T112">ol</ta>
            <ta e="T114" id="Seg_3753" s="T113">üle-LAN-A</ta>
            <ta e="T115" id="Seg_3754" s="T114">olor-TAK-TArInA</ta>
            <ta e="T116" id="Seg_3755" s="T115">hir-LArA</ta>
            <ta e="T117" id="Seg_3756" s="T116">togo</ta>
            <ta e="T118" id="Seg_3757" s="T117">ere</ta>
            <ta e="T119" id="Seg_3758" s="T118">nʼirgij-Ar</ta>
            <ta e="T120" id="Seg_3759" s="T119">e-BIT</ta>
            <ta e="T121" id="Seg_3760" s="T120">be</ta>
            <ta e="T122" id="Seg_3761" s="T121">di͡e-BIT</ta>
            <ta e="T123" id="Seg_3762" s="T122">biːrges-tA</ta>
            <ta e="T124" id="Seg_3763" s="T123">togo</ta>
            <ta e="T125" id="Seg_3764" s="T124">hir-BIt</ta>
            <ta e="T126" id="Seg_3765" s="T125">nʼirgij-Ar</ta>
            <ta e="T127" id="Seg_3766" s="T126">togo</ta>
            <ta e="T128" id="Seg_3767" s="T127">etiŋ</ta>
            <ta e="T129" id="Seg_3768" s="T128">tɨ͡as-tA</ta>
            <ta e="T130" id="Seg_3769" s="T129">itigirdik</ta>
            <ta e="T131" id="Seg_3770" s="T130">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T132" id="Seg_3771" s="T131">tu͡ok</ta>
            <ta e="T133" id="Seg_3772" s="T132">ere</ta>
            <ta e="T134" id="Seg_3773" s="T133">dʼikti-tA</ta>
            <ta e="T135" id="Seg_3774" s="T134">bu͡ol-TI-tA</ta>
            <ta e="T136" id="Seg_3775" s="T135">ihilleː</ta>
            <ta e="T137" id="Seg_3776" s="T136">emeːksin</ta>
            <ta e="T138" id="Seg_3777" s="T137">Krʼistʼin</ta>
            <ta e="T139" id="Seg_3778" s="T138">kɨrdʼagas</ta>
            <ta e="T140" id="Seg_3779" s="T139">as-tA</ta>
            <ta e="T141" id="Seg_3780" s="T140">bɨldʼaː-AːččI</ta>
            <ta e="T142" id="Seg_3781" s="T141">gɨlbaj-BIT</ta>
            <ta e="T143" id="Seg_3782" s="T142">emeːksin</ta>
            <ta e="T144" id="Seg_3783" s="T143">u͡os-tI-GAr</ta>
            <ta e="T145" id="Seg_3784" s="T144">biːr</ta>
            <ta e="T146" id="Seg_3785" s="T145">da</ta>
            <ta e="T147" id="Seg_3786" s="T146">tiːs-tA</ta>
            <ta e="T148" id="Seg_3787" s="T147">hu͡ok</ta>
            <ta e="T149" id="Seg_3788" s="T148">ös</ta>
            <ta e="T150" id="Seg_3789" s="T149">kiːr-BAT</ta>
            <ta e="T151" id="Seg_3790" s="T150">pɨlaːt-tI-n</ta>
            <ta e="T152" id="Seg_3791" s="T151">menʼiː-tI-ttAn</ta>
            <ta e="T153" id="Seg_3792" s="T152">hulbu</ta>
            <ta e="T154" id="Seg_3793" s="T153">tart-AːT</ta>
            <ta e="T155" id="Seg_3794" s="T154">ihilleː-BIT</ta>
            <ta e="T156" id="Seg_3795" s="T155">ol</ta>
            <ta e="T157" id="Seg_3796" s="T520">ikki</ta>
            <ta e="T158" id="Seg_3797" s="T157">emeːksin</ta>
            <ta e="T159" id="Seg_3798" s="T158">kallaːn</ta>
            <ta e="T160" id="Seg_3799" s="T159">dek</ta>
            <ta e="T161" id="Seg_3800" s="T160">nʼɨlaj-An</ta>
            <ta e="T162" id="Seg_3801" s="T161">baran</ta>
            <ta e="T163" id="Seg_3802" s="T162">üːr-BIT</ta>
            <ta e="T164" id="Seg_3803" s="T163">taba</ta>
            <ta e="T165" id="Seg_3804" s="T164">kördük</ta>
            <ta e="T166" id="Seg_3805" s="T165">nʼirgij-Ar</ta>
            <ta e="T167" id="Seg_3806" s="T166">tɨ͡as-nI</ta>
            <ta e="T168" id="Seg_3807" s="T167">ihilleː-An</ta>
            <ta e="T170" id="Seg_3808" s="T168">bu͡ol-BIT-LAr</ta>
            <ta e="T171" id="Seg_3809" s="T170">kumaːr</ta>
            <ta e="T172" id="Seg_3810" s="T171">da</ta>
            <ta e="T173" id="Seg_3811" s="T172">hi͡e-Ar-tI-n</ta>
            <ta e="T174" id="Seg_3812" s="T173">bil-BAkkA</ta>
            <ta e="T175" id="Seg_3813" s="T174">ör-kAːN</ta>
            <ta e="T176" id="Seg_3814" s="T175">olor-BIT-LAr</ta>
            <ta e="T177" id="Seg_3815" s="T176">anʼak-LArI-n</ta>
            <ta e="T178" id="Seg_3816" s="T177">at-An</ta>
            <ta e="T179" id="Seg_3817" s="T178">baran</ta>
            <ta e="T180" id="Seg_3818" s="T179">attɨ-LArI-GAr</ta>
            <ta e="T181" id="Seg_3819" s="T180">tiriː-LAr</ta>
            <ta e="T182" id="Seg_3820" s="T181">loskutu͡ok-LArA</ta>
            <ta e="T183" id="Seg_3821" s="T182">onno-manna</ta>
            <ta e="T184" id="Seg_3822" s="T183">hɨt-Ar-LAr</ta>
            <ta e="T185" id="Seg_3823" s="T184">bɨ͡a-nAn</ta>
            <ta e="T186" id="Seg_3824" s="T185">baːj-Iː-LAːK</ta>
            <ta e="T187" id="Seg_3825" s="T186">mordʼoː-LArA</ta>
            <ta e="T188" id="Seg_3826" s="T187">iliː-LArI-GAr</ta>
            <ta e="T189" id="Seg_3827" s="T188">buru͡olas-Ar-LAr</ta>
            <ta e="T190" id="Seg_3828" s="T189">köhüt-BIT</ta>
            <ta e="T191" id="Seg_3829" s="T190">kördük</ta>
            <ta e="T192" id="Seg_3830" s="T191">sɨp-sɨp</ta>
            <ta e="T193" id="Seg_3831" s="T192">kahan</ta>
            <ta e="T194" id="Seg_3832" s="T193">olor-IAK-LArI-n</ta>
            <ta e="T195" id="Seg_3833" s="T194">kirdik</ta>
            <ta e="T196" id="Seg_3834" s="T195">giniler</ta>
            <ta e="T197" id="Seg_3835" s="T196">bil-BAtAK</ta>
            <ta e="T198" id="Seg_3836" s="T197">tɨ͡as-LArA</ta>
            <ta e="T199" id="Seg_3837" s="T198">bu͡ol-BIT</ta>
            <ta e="T200" id="Seg_3838" s="T199">kaja</ta>
            <ta e="T201" id="Seg_3839" s="T200">da</ta>
            <ta e="T202" id="Seg_3840" s="T201">dʼe</ta>
            <ta e="T203" id="Seg_3841" s="T202">öl-TI-BIt</ta>
            <ta e="T204" id="Seg_3842" s="T203">di͡e-A</ta>
            <ta e="T205" id="Seg_3843" s="T204">tüs-AːT</ta>
            <ta e="T206" id="Seg_3844" s="T205">biːrges</ta>
            <ta e="T207" id="Seg_3845" s="T206">emeːksin</ta>
            <ta e="T208" id="Seg_3846" s="T207">tur-A</ta>
            <ta e="T209" id="Seg_3847" s="T208">ekkireː-BIT</ta>
            <ta e="T210" id="Seg_3848" s="T209">mas</ta>
            <ta e="T211" id="Seg_3849" s="T210">dek</ta>
            <ta e="T212" id="Seg_3850" s="T211">körüleː-BIT</ta>
            <ta e="T213" id="Seg_3851" s="T212">dʼe</ta>
            <ta e="T214" id="Seg_3852" s="T213">köt-An</ta>
            <ta e="T215" id="Seg_3853" s="T214">is-Ar</ta>
            <ta e="T216" id="Seg_3854" s="T215">uluː</ta>
            <ta e="T217" id="Seg_3855" s="T216">kötör-tA</ta>
            <ta e="T218" id="Seg_3856" s="T217">kör</ta>
            <ta e="T219" id="Seg_3857" s="T218">ol</ta>
            <ta e="T220" id="Seg_3858" s="T219">kɨtar-An</ta>
            <ta e="T221" id="Seg_3859" s="T220">is-Ar</ta>
            <ta e="T222" id="Seg_3860" s="T221">gini</ta>
            <ta e="T223" id="Seg_3861" s="T222">his-tI-n</ta>
            <ta e="T224" id="Seg_3862" s="T223">tɨ͡as-tA</ta>
            <ta e="T225" id="Seg_3863" s="T224">e-BIT</ta>
            <ta e="T226" id="Seg_3864" s="T225">koːspodi</ta>
            <ta e="T227" id="Seg_3865" s="T226">min</ta>
            <ta e="T228" id="Seg_3866" s="T227">haŋar-BAtAK-I-m</ta>
            <ta e="T229" id="Seg_3867" s="T228">kör-BAtAK-I-m</ta>
            <ta e="T230" id="Seg_3868" s="T229">bu͡ollun</ta>
            <ta e="T231" id="Seg_3869" s="T230">di͡e-An</ta>
            <ta e="T232" id="Seg_3870" s="T231">emeːksin</ta>
            <ta e="T233" id="Seg_3871" s="T232">labɨj-An</ta>
            <ta e="T234" id="Seg_3872" s="T233">bar-BIT</ta>
            <ta e="T235" id="Seg_3873" s="T234">o</ta>
            <ta e="T236" id="Seg_3874" s="T235">tu͡ok</ta>
            <ta e="T237" id="Seg_3875" s="T236">kötör-tI-n</ta>
            <ta e="T238" id="Seg_3876" s="T237">gɨn-A-GIn</ta>
            <ta e="T239" id="Seg_3877" s="T238">di͡e-A-di͡e-A</ta>
            <ta e="T240" id="Seg_3878" s="T239">dogor-tA</ta>
            <ta e="T241" id="Seg_3879" s="T240">emi͡e</ta>
            <ta e="T242" id="Seg_3880" s="T241">körüleː-BIT</ta>
            <ta e="T243" id="Seg_3881" s="T242">atak-LArI-n</ta>
            <ta e="T244" id="Seg_3882" s="T243">alɨn-LArI-GAr</ta>
            <ta e="T245" id="Seg_3883" s="T244">hir-LArA</ta>
            <ta e="T246" id="Seg_3884" s="T245">möŋ-Ar</ta>
            <ta e="T247" id="Seg_3885" s="T246">kördük</ta>
            <ta e="T248" id="Seg_3886" s="T247">bu͡ol-BIT</ta>
            <ta e="T249" id="Seg_3887" s="T248">uraha-LAr</ta>
            <ta e="T250" id="Seg_3888" s="T249">titireː-BIT-LAr</ta>
            <ta e="T251" id="Seg_3889" s="T250">türgen-LIk</ta>
            <ta e="T252" id="Seg_3890" s="T251">hɨrga</ta>
            <ta e="T253" id="Seg_3891" s="T252">dek</ta>
            <ta e="T254" id="Seg_3892" s="T253">alɨn-tI-GAr</ta>
            <ta e="T255" id="Seg_3893" s="T254">di͡e-A</ta>
            <ta e="T256" id="Seg_3894" s="T255">tüs-AːT</ta>
            <ta e="T257" id="Seg_3895" s="T256">biːrges-tA</ta>
            <ta e="T258" id="Seg_3896" s="T257">čip</ta>
            <ta e="T259" id="Seg_3897" s="T258">gɨn-BIT</ta>
            <ta e="T260" id="Seg_3898" s="T259">katɨː-LAr-LAːK</ta>
            <ta e="T261" id="Seg_3899" s="T260">mu͡osta</ta>
            <ta e="T262" id="Seg_3900" s="T261">alɨn-tI-GAr</ta>
            <ta e="T263" id="Seg_3901" s="T262">dogor-tA</ta>
            <ta e="T264" id="Seg_3902" s="T263">gini-nI</ta>
            <ta e="T265" id="Seg_3903" s="T264">köŋüj-An</ta>
            <ta e="T266" id="Seg_3904" s="T265">kiːr-BIT</ta>
            <ta e="T267" id="Seg_3905" s="T266">balok-tI-n</ta>
            <ta e="T268" id="Seg_3906" s="T267">alɨn-tI-GAr</ta>
            <ta e="T269" id="Seg_3907" s="T268">ol</ta>
            <ta e="T270" id="Seg_3908" s="T269">kem</ta>
            <ta e="T271" id="Seg_3909" s="T270">lü͡ötčik</ta>
            <ta e="T272" id="Seg_3910" s="T271">haŋardɨː</ta>
            <ta e="T273" id="Seg_3911" s="T272">uraha</ta>
            <ta e="T274" id="Seg_3912" s="T273">dʼi͡e-LAr-nI</ta>
            <ta e="T275" id="Seg_3913" s="T274">kör-A</ta>
            <ta e="T276" id="Seg_3914" s="T275">hotoru</ta>
            <ta e="T277" id="Seg_3915" s="T276">nʼamčɨgas</ta>
            <ta e="T278" id="Seg_3916" s="T277">bagajɨ</ta>
            <ta e="T279" id="Seg_3917" s="T278">köt-An</ta>
            <ta e="T280" id="Seg_3918" s="T279">aːs-BIT</ta>
            <ta e="T281" id="Seg_3919" s="T280">hɨrga</ta>
            <ta e="T282" id="Seg_3920" s="T281">dʼi͡e-nI</ta>
            <ta e="T283" id="Seg_3921" s="T282">kɨtta</ta>
            <ta e="T284" id="Seg_3922" s="T283">balok</ta>
            <ta e="T285" id="Seg_3923" s="T284">alɨn-LArI-ttAn</ta>
            <ta e="T288" id="Seg_3924" s="T287">emeːksin-LAr</ta>
            <ta e="T289" id="Seg_3925" s="T288">harɨː</ta>
            <ta e="T290" id="Seg_3926" s="T289">atak-LArA</ta>
            <ta e="T291" id="Seg_3927" s="T290">möŋ-BAkkA</ta>
            <ta e="T292" id="Seg_3928" s="T291">čoros-A</ta>
            <ta e="T293" id="Seg_3929" s="T292">hɨt-BIT-LAr</ta>
            <ta e="T294" id="Seg_3930" s="T293">kɨtɨl-GA</ta>
            <ta e="T295" id="Seg_3931" s="T294">ogonnʼor-LAr</ta>
            <ta e="T296" id="Seg_3932" s="T295">ekkireː-A-ekkireː-A</ta>
            <ta e="T297" id="Seg_3933" s="T296">bergehe-LArI-n</ta>
            <ta e="T298" id="Seg_3934" s="T297">samalʼot-GA</ta>
            <ta e="T299" id="Seg_3935" s="T298">dajbaː-A</ta>
            <ta e="T300" id="Seg_3936" s="T299">tur-BIT-LAr</ta>
            <ta e="T301" id="Seg_3937" s="T300">giniler</ta>
            <ta e="T302" id="Seg_3938" s="T301">ü͡ör-Iː-LArA</ta>
            <ta e="T303" id="Seg_3939" s="T302">haŋardɨː</ta>
            <ta e="T304" id="Seg_3940" s="T303">samalʼot</ta>
            <ta e="T305" id="Seg_3941" s="T304">kel-Ar-tI-GAr</ta>
            <ta e="T306" id="Seg_3942" s="T305">mu͡ora-GA</ta>
            <ta e="T307" id="Seg_3943" s="T306">bert</ta>
            <ta e="T308" id="Seg_3944" s="T307">e-BIT</ta>
            <ta e="T309" id="Seg_3945" s="T308">bil-Ar-LAr</ta>
            <ta e="T312" id="Seg_3946" s="T311">bil-s-Ar</ta>
            <ta e="T313" id="Seg_3947" s="T312">bu͡ol-A</ta>
            <ta e="T314" id="Seg_3948" s="T313">hɨrɨt-IAK-tA</ta>
            <ta e="T315" id="Seg_3949" s="T314">kajdak</ta>
            <ta e="T316" id="Seg_3950" s="T315">taba-ČIt-LAr</ta>
            <ta e="T317" id="Seg_3951" s="T316">balɨk-ČIt-LAr</ta>
            <ta e="T318" id="Seg_3952" s="T317">olor-Ar-LArI-n</ta>
            <ta e="T319" id="Seg_3953" s="T318">kör-AːrI</ta>
            <ta e="T320" id="Seg_3954" s="T319">hɨːr</ta>
            <ta e="T321" id="Seg_3955" s="T320">ürüt-tI-GAr</ta>
            <ta e="T322" id="Seg_3956" s="T321">tagɨs-AːT</ta>
            <ta e="T323" id="Seg_3957" s="T322">oduːlaː-A-n-BIT-LAr</ta>
            <ta e="T324" id="Seg_3958" s="T323">uraha</ta>
            <ta e="T325" id="Seg_3959" s="T324">dʼi͡e-LArA</ta>
            <ta e="T326" id="Seg_3960" s="T325">buru͡olaː-BAT-LAr</ta>
            <ta e="T327" id="Seg_3961" s="T326">ti͡ergen</ta>
            <ta e="T328" id="Seg_3962" s="T327">ürüt-tI-nAn</ta>
            <ta e="T331" id="Seg_3963" s="T330">im</ta>
            <ta e="T332" id="Seg_3964" s="T331">dʼɨlɨj-ččI</ta>
            <ta e="T333" id="Seg_3965" s="T332">ɨt</ta>
            <ta e="T334" id="Seg_3966" s="T333">da</ta>
            <ta e="T335" id="Seg_3967" s="T334">ür-BAT</ta>
            <ta e="T336" id="Seg_3968" s="T335">kihi</ta>
            <ta e="T337" id="Seg_3969" s="T336">da</ta>
            <ta e="T338" id="Seg_3970" s="T337">haŋa-tA</ta>
            <ta e="T339" id="Seg_3971" s="T338">ihilin-I-BAT</ta>
            <ta e="T340" id="Seg_3972" s="T339">tu͡ok</ta>
            <ta e="T341" id="Seg_3973" s="T340">dʼon-LAr</ta>
            <ta e="T342" id="Seg_3974" s="T341">bu</ta>
            <ta e="T343" id="Seg_3975" s="T342">emeːksin-LAr</ta>
            <ta e="T344" id="Seg_3976" s="T343">bil-BAT-LAr</ta>
            <ta e="T345" id="Seg_3977" s="T344">du͡o</ta>
            <ta e="T346" id="Seg_3978" s="T345">bihigi</ta>
            <ta e="T347" id="Seg_3979" s="T346">kel-IAK-BItI-n</ta>
            <ta e="T348" id="Seg_3980" s="T347">di͡e-BIT</ta>
            <ta e="T349" id="Seg_3981" s="T348">Onoː</ta>
            <ta e="T350" id="Seg_3982" s="T349">ogonnʼor</ta>
            <ta e="T351" id="Seg_3983" s="T350">kanna</ta>
            <ta e="T352" id="Seg_3984" s="T351">baːr-GIt=Ij</ta>
            <ta e="T353" id="Seg_3985" s="T352">dʼi͡e-LAːK-LAr</ta>
            <ta e="T356" id="Seg_3986" s="T355">ü͡ögüleː-BIT</ta>
            <ta e="T357" id="Seg_3987" s="T356">ɨ͡al</ta>
            <ta e="T358" id="Seg_3988" s="T357">kihi-tA</ta>
            <ta e="T359" id="Seg_3989" s="T358">araː</ta>
            <ta e="T360" id="Seg_3990" s="T359">abɨraː-ŋ</ta>
            <ta e="T361" id="Seg_3991" s="T360">araːr-I-ŋ</ta>
            <ta e="T362" id="Seg_3992" s="T361">öl-TI-BIt</ta>
            <ta e="T363" id="Seg_3993" s="T362">hir</ta>
            <ta e="T364" id="Seg_3994" s="T363">is-tI-ttAn</ta>
            <ta e="T365" id="Seg_3995" s="T364">ü͡ögüː</ta>
            <ta e="T366" id="Seg_3996" s="T365">ihilin-I-BIT</ta>
            <ta e="T367" id="Seg_3997" s="T366">tu͡ok-LAr=Ij</ta>
            <ta e="T368" id="Seg_3998" s="T367">iːr-BIT-LAr</ta>
            <ta e="T369" id="Seg_3999" s="T368">du͡o</ta>
            <ta e="T370" id="Seg_4000" s="T369">di͡e-A</ta>
            <ta e="T371" id="Seg_4001" s="T370">tüs-AːT</ta>
            <ta e="T372" id="Seg_4002" s="T371">Onoː</ta>
            <ta e="T373" id="Seg_4003" s="T372">körüleː-BIT</ta>
            <ta e="T374" id="Seg_4004" s="T373">gini</ta>
            <ta e="T375" id="Seg_4005" s="T374">hɨrga</ta>
            <ta e="T376" id="Seg_4006" s="T375">dʼi͡e-LAr</ta>
            <ta e="T377" id="Seg_4007" s="T376">alɨn-LArI-GAr</ta>
            <ta e="T378" id="Seg_4008" s="T377">körüleː-BIT</ta>
            <ta e="T379" id="Seg_4009" s="T378">tu͡ok</ta>
            <ta e="T380" id="Seg_4010" s="T379">dʼikti-tA</ta>
            <ta e="T381" id="Seg_4011" s="T380">bu͡ol-TI-tA</ta>
            <ta e="T382" id="Seg_4012" s="T381">emeːksin-LArI-n</ta>
            <ta e="T383" id="Seg_4013" s="T382">atak-LArA</ta>
            <ta e="T384" id="Seg_4014" s="T383">onno</ta>
            <ta e="T385" id="Seg_4015" s="T384">halɨbɨraː-A-s-A</ta>
            <ta e="T386" id="Seg_4016" s="T385">hɨt-BIT-LAr</ta>
            <ta e="T387" id="Seg_4017" s="T386">kör-TAK-InA</ta>
            <ta e="T388" id="Seg_4018" s="T387">tagɨs-A</ta>
            <ta e="T389" id="Seg_4019" s="T388">hataː-Ar-LAr</ta>
            <ta e="T390" id="Seg_4020" s="T389">bɨhɨːlaːk</ta>
            <ta e="T391" id="Seg_4021" s="T390">katɨː-LAr</ta>
            <ta e="T392" id="Seg_4022" s="T391">taŋas-LArI-ttAn</ta>
            <ta e="T393" id="Seg_4023" s="T392">iːl-BIT-LAr</ta>
            <ta e="T394" id="Seg_4024" s="T393">togo</ta>
            <ta e="T395" id="Seg_4025" s="T394">ɨːt-IAK-LArA=Ij</ta>
            <ta e="T396" id="Seg_4026" s="T395">is-GA</ta>
            <ta e="T397" id="Seg_4027" s="T396">da</ta>
            <ta e="T398" id="Seg_4028" s="T397">kiːr-TAr-BAT-LAr</ta>
            <ta e="T399" id="Seg_4029" s="T398">tas-GA</ta>
            <ta e="T400" id="Seg_4030" s="T399">da</ta>
            <ta e="T401" id="Seg_4031" s="T400">tahaːr-BAT-LAr</ta>
            <ta e="T402" id="Seg_4032" s="T401">araː</ta>
            <ta e="T403" id="Seg_4033" s="T402">di͡e-A-s-Iː</ta>
            <ta e="T404" id="Seg_4034" s="T403">elbeː-An</ta>
            <ta e="T405" id="Seg_4035" s="T404">is-BIT</ta>
            <ta e="T406" id="Seg_4036" s="T405">ɨnčɨktaː-Ar</ta>
            <ta e="T407" id="Seg_4037" s="T406">kojut-I-BIT</ta>
            <ta e="T408" id="Seg_4038" s="T407">ogonnʼor-LAr</ta>
            <ta e="T409" id="Seg_4039" s="T408">tu͡ok</ta>
            <ta e="T410" id="Seg_4040" s="T409">da</ta>
            <ta e="T411" id="Seg_4041" s="T410">dek</ta>
            <ta e="T412" id="Seg_4042" s="T411">bert-tI-ttAn</ta>
            <ta e="T413" id="Seg_4043" s="T412">hök-A</ta>
            <ta e="T414" id="Seg_4044" s="T413">hataː-An</ta>
            <ta e="T415" id="Seg_4045" s="T414">tu͡ok-nI</ta>
            <ta e="T416" id="Seg_4046" s="T415">da</ta>
            <ta e="T417" id="Seg_4047" s="T416">haŋar-BAT</ta>
            <ta e="T418" id="Seg_4048" s="T417">bu͡ol-BIT-LAr</ta>
            <ta e="T419" id="Seg_4049" s="T418">emeːksin-LAr-nI</ta>
            <ta e="T420" id="Seg_4050" s="T419">biːr-LIː</ta>
            <ta e="T421" id="Seg_4051" s="T420">boskoloː-BIT-LAr</ta>
            <ta e="T422" id="Seg_4052" s="T421">giniler-GA</ta>
            <ta e="T423" id="Seg_4053" s="T422">hɨraj</ta>
            <ta e="T424" id="Seg_4054" s="T423">kel-IAK.[tA]</ta>
            <ta e="T425" id="Seg_4055" s="T424">du͡o</ta>
            <ta e="T426" id="Seg_4056" s="T425">as-LArA</ta>
            <ta e="T427" id="Seg_4057" s="T426">karak-LArI-n</ta>
            <ta e="T428" id="Seg_4058" s="T427">hap-BIT</ta>
            <ta e="T429" id="Seg_4059" s="T428">harɨː</ta>
            <ta e="T430" id="Seg_4060" s="T429">atak-LArA</ta>
            <ta e="T431" id="Seg_4061" s="T430">nʼɨpkaj-ččI</ta>
            <ta e="T432" id="Seg_4062" s="T431">tüs-BIT-LAr</ta>
            <ta e="T433" id="Seg_4063" s="T432">dʼühün-LArA</ta>
            <ta e="T434" id="Seg_4064" s="T433">ularɨj-BIT</ta>
            <ta e="T435" id="Seg_4065" s="T434">bu͡or-ttAn</ta>
            <ta e="T436" id="Seg_4066" s="T435">tagɨs-I-BIT</ta>
            <ta e="T437" id="Seg_4067" s="T436">kɨrsa</ta>
            <ta e="T438" id="Seg_4068" s="T437">ogo-LAr-tI-n</ta>
            <ta e="T439" id="Seg_4069" s="T438">kördük</ta>
            <ta e="T440" id="Seg_4070" s="T439">bu͡ol-BIT-LAr</ta>
            <ta e="T441" id="Seg_4071" s="T440">kaja</ta>
            <ta e="T442" id="Seg_4072" s="T441">tu͡ok</ta>
            <ta e="T443" id="Seg_4073" s="T442">bu͡ol-BIT-GIt=Ij</ta>
            <ta e="T444" id="Seg_4074" s="T443">di͡e-An</ta>
            <ta e="T445" id="Seg_4075" s="T444">ɨjɨt-BIT-LAr</ta>
            <ta e="T446" id="Seg_4076" s="T445">biːr-LIː</ta>
            <ta e="T447" id="Seg_4077" s="T446">duruk</ta>
            <ta e="T448" id="Seg_4078" s="T447">učum</ta>
            <ta e="T449" id="Seg_4079" s="T448">bil-BAtAK</ta>
            <ta e="T450" id="Seg_4080" s="T449">kötör-BIt</ta>
            <ta e="T451" id="Seg_4081" s="T450">aːs-TI-tA</ta>
            <ta e="T452" id="Seg_4082" s="T451">di͡e-An</ta>
            <ta e="T453" id="Seg_4083" s="T452">nʼɨŋɨnas-BIT-LAr</ta>
            <ta e="T454" id="Seg_4084" s="T453">ikki</ta>
            <ta e="T455" id="Seg_4085" s="T454">emeːksin</ta>
            <ta e="T456" id="Seg_4086" s="T455">oː</ta>
            <ta e="T457" id="Seg_4087" s="T456">bil-BAtAK</ta>
            <ta e="T458" id="Seg_4088" s="T457">bu͡ol-An</ta>
            <ta e="T459" id="Seg_4089" s="T458">Onoː</ta>
            <ta e="T460" id="Seg_4090" s="T459">heŋeːr-BIT</ta>
            <ta e="T461" id="Seg_4091" s="T460">dʼe</ta>
            <ta e="T462" id="Seg_4092" s="T461">kepseː-ŋ</ta>
            <ta e="T463" id="Seg_4093" s="T462">tu͡ok</ta>
            <ta e="T464" id="Seg_4094" s="T463">tüːl-tI-n</ta>
            <ta e="T465" id="Seg_4095" s="T464">tüheː-TI-GIt</ta>
            <ta e="T466" id="Seg_4096" s="T465">hɨrga</ta>
            <ta e="T467" id="Seg_4097" s="T466">dʼi͡e</ta>
            <ta e="T468" id="Seg_4098" s="T467">alɨn-tI-GAr</ta>
            <ta e="T469" id="Seg_4099" s="T468">hɨt-An-GIt</ta>
            <ta e="T470" id="Seg_4100" s="T469">bu</ta>
            <ta e="T471" id="Seg_4101" s="T470">tu͡ok</ta>
            <ta e="T472" id="Seg_4102" s="T471">bu͡ol-Ar</ta>
            <ta e="T473" id="Seg_4103" s="T472">ogonnʼor=Ij</ta>
            <ta e="T474" id="Seg_4104" s="T473">kɨrɨj-An</ta>
            <ta e="T475" id="Seg_4105" s="T474">öj-tA</ta>
            <ta e="T476" id="Seg_4106" s="T475">kɨlgaː-Ar</ta>
            <ta e="T477" id="Seg_4107" s="T476">bɨhɨːlaːk</ta>
            <ta e="T478" id="Seg_4108" s="T477">agdas-BIT-LAr</ta>
            <ta e="T479" id="Seg_4109" s="T478">ikki</ta>
            <ta e="T480" id="Seg_4110" s="T479">kuttannʼak-LAr</ta>
            <ta e="T481" id="Seg_4111" s="T480">hin</ta>
            <ta e="T482" id="Seg_4112" s="T481">uskaːn</ta>
            <ta e="T483" id="Seg_4113" s="T482">hürek-LArA</ta>
            <ta e="T484" id="Seg_4114" s="T483">össü͡ö</ta>
            <ta e="T485" id="Seg_4115" s="T484">haŋa-LAːK</ta>
            <ta e="T486" id="Seg_4116" s="T485">bu͡ol-BIT-LAr</ta>
            <ta e="T487" id="Seg_4117" s="T486">bil-BAT-GIt</ta>
            <ta e="T488" id="Seg_4118" s="T487">du͡o</ta>
            <ta e="T489" id="Seg_4119" s="T488">üje</ta>
            <ta e="T490" id="Seg_4120" s="T489">ularɨj-BIT-tI-n</ta>
            <ta e="T491" id="Seg_4121" s="T490">savʼet-LAr</ta>
            <ta e="T492" id="Seg_4122" s="T491">čɨːčaːk-LArA</ta>
            <ta e="T493" id="Seg_4123" s="T492">kel-Ar-tI-n</ta>
            <ta e="T494" id="Seg_4124" s="T493">iti-tI-GIt</ta>
            <ta e="T495" id="Seg_4125" s="T494">ehigi-nI</ta>
            <ta e="T496" id="Seg_4126" s="T495">abɨraː-A</ta>
            <ta e="T497" id="Seg_4127" s="T496">köt-A</ta>
            <ta e="T498" id="Seg_4128" s="T497">hɨrɨt-Ar</ta>
            <ta e="T499" id="Seg_4129" s="T498">tɨ͡a</ta>
            <ta e="T500" id="Seg_4130" s="T499">dʼon-tA</ta>
            <ta e="T501" id="Seg_4131" s="T500">tot</ta>
            <ta e="T502" id="Seg_4132" s="T501">baːj</ta>
            <ta e="T503" id="Seg_4133" s="T502">olor-IAK-LAr-tI-n</ta>
            <ta e="T504" id="Seg_4134" s="T503">di͡e-A-s-BIT-LAr</ta>
            <ta e="T505" id="Seg_4135" s="T504">ikki</ta>
            <ta e="T506" id="Seg_4136" s="T505">kihi</ta>
            <ta e="T507" id="Seg_4137" s="T506">emeːksin-LAr</ta>
            <ta e="T508" id="Seg_4138" s="T507">tu͡ok</ta>
            <ta e="T509" id="Seg_4139" s="T508">da</ta>
            <ta e="T510" id="Seg_4140" s="T509">di͡e-IAK-LArI-n</ta>
            <ta e="T511" id="Seg_4141" s="T510">bert-tI-ttAn</ta>
            <ta e="T512" id="Seg_4142" s="T511">haŋa-tA</ta>
            <ta e="T513" id="Seg_4143" s="T512">hu͡ok</ta>
            <ta e="T514" id="Seg_4144" s="T513">haːt-BIT</ta>
            <ta e="T515" id="Seg_4145" s="T514">kördük</ta>
            <ta e="T516" id="Seg_4146" s="T515">dʼi͡e-LArI-n</ta>
            <ta e="T517" id="Seg_4147" s="T516">dek</ta>
            <ta e="T518" id="Seg_4148" s="T517">ököj-s-A</ta>
            <ta e="T519" id="Seg_4149" s="T518">tur-BIT-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_4150" s="T0">Soviet-PL.[NOM]</ta>
            <ta e="T2" id="Seg_4151" s="T1">law-3PL.[NOM]</ta>
            <ta e="T3" id="Seg_4152" s="T2">just</ta>
            <ta e="T4" id="Seg_4153" s="T3">come-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T5" id="Seg_4154" s="T4">Kurya</ta>
            <ta e="T6" id="Seg_4155" s="T5">say-CVB.SEQ</ta>
            <ta e="T7" id="Seg_4156" s="T6">settlement-DAT/LOC</ta>
            <ta e="T8" id="Seg_4157" s="T7">summer.[NOM]</ta>
            <ta e="T9" id="Seg_4158" s="T8">every</ta>
            <ta e="T10" id="Seg_4159" s="T9">fish-CVB.SIM</ta>
            <ta e="T11" id="Seg_4160" s="T10">sit-PTCP.HAB</ta>
            <ta e="T12" id="Seg_4161" s="T11">be-PST1-3PL</ta>
            <ta e="T13" id="Seg_4162" s="T12">Dolgan</ta>
            <ta e="T14" id="Seg_4163" s="T13">people-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_4164" s="T14">across</ta>
            <ta e="T16" id="Seg_4165" s="T15">eye-PROPR.[NOM]</ta>
            <ta e="T17" id="Seg_4166" s="T16">often-often</ta>
            <ta e="T18" id="Seg_4167" s="T17">go-EP-NEG.PTCP</ta>
            <ta e="T19" id="Seg_4168" s="T18">settlement-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_4169" s="T19">such</ta>
            <ta e="T21" id="Seg_4170" s="T20">distant.[NOM]</ta>
            <ta e="T22" id="Seg_4171" s="T21">fish-PTCP.PRS</ta>
            <ta e="T23" id="Seg_4172" s="T22">place-3PL.[NOM]</ta>
            <ta e="T24" id="Seg_4173" s="T23">when</ta>
            <ta e="T25" id="Seg_4174" s="T24">INDEF</ta>
            <ta e="T26" id="Seg_4175" s="T25">who.[NOM]</ta>
            <ta e="T27" id="Seg_4176" s="T26">INDEF</ta>
            <ta e="T28" id="Seg_4177" s="T27">come-FUT.[3SG]</ta>
            <ta e="T29" id="Seg_4178" s="T28">in.summer</ta>
            <ta e="T30" id="Seg_4179" s="T29">close-ADJZ</ta>
            <ta e="T31" id="Seg_4180" s="T30">rot-PL-DAT/LOC</ta>
            <ta e="T32" id="Seg_4181" s="T31">news.[NOM]</ta>
            <ta e="T33" id="Seg_4182" s="T32">tell-CVB.SIM</ta>
            <ta e="T34" id="Seg_4183" s="T33">self-3SG-GEN</ta>
            <ta e="T35" id="Seg_4184" s="T34">life-3SG-GEN</ta>
            <ta e="T36" id="Seg_4185" s="T35">side-3SG-INSTR</ta>
            <ta e="T37" id="Seg_4186" s="T36">get.to.know-CVB.PURP</ta>
            <ta e="T38" id="Seg_4187" s="T37">how</ta>
            <ta e="T39" id="Seg_4188" s="T38">fish-PTCP.PRS-3PL-ACC</ta>
            <ta e="T40" id="Seg_4189" s="T39">Kurya.[NOM]</ta>
            <ta e="T41" id="Seg_4190" s="T40">people-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_4191" s="T41">one</ta>
            <ta e="T43" id="Seg_4192" s="T42">summer.[NOM]</ta>
            <ta e="T44" id="Seg_4193" s="T43">old.man-PL.[NOM]</ta>
            <ta e="T45" id="Seg_4194" s="T44">early</ta>
            <ta e="T46" id="Seg_4195" s="T45">in.the.morning</ta>
            <ta e="T47" id="Seg_4196" s="T46">row-EP-MED-CVB.SEQ</ta>
            <ta e="T48" id="Seg_4197" s="T47">stay-PST2-3PL</ta>
            <ta e="T49" id="Seg_4198" s="T48">tributary-PL-3SG-ACC</ta>
            <ta e="T50" id="Seg_4199" s="T49">see-EP-MED-CVB.SIM</ta>
            <ta e="T51" id="Seg_4200" s="T50">woman.[NOM]</ta>
            <ta e="T52" id="Seg_4201" s="T51">child.[NOM]</ta>
            <ta e="T53" id="Seg_4202" s="T52">relative.[NOM]</ta>
            <ta e="T54" id="Seg_4203" s="T53">house-3PL-DAT/LOC</ta>
            <ta e="T55" id="Seg_4204" s="T54">sit-PST2-3PL</ta>
            <ta e="T56" id="Seg_4205" s="T55">who.[NOM]</ta>
            <ta e="T57" id="Seg_4206" s="T56">fish.[NOM]</ta>
            <ta e="T58" id="Seg_4207" s="T57">clean-EP-MED-PRS.[3SG]</ta>
            <ta e="T59" id="Seg_4208" s="T58">who.[NOM]</ta>
            <ta e="T60" id="Seg_4209" s="T59">dried.fish.[NOM]</ta>
            <ta e="T61" id="Seg_4210" s="T60">dry-EP-MED-PRS.[3SG]</ta>
            <ta e="T62" id="Seg_4211" s="T61">that-PL-ABL</ta>
            <ta e="T63" id="Seg_4212" s="T62">two</ta>
            <ta e="T64" id="Seg_4213" s="T63">old.woman.[NOM]</ta>
            <ta e="T65" id="Seg_4214" s="T64">old.man-PL.[NOM]</ta>
            <ta e="T66" id="Seg_4215" s="T65">come-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T67" id="Seg_4216" s="T66">until</ta>
            <ta e="T68" id="Seg_4217" s="T67">sledge.[NOM]</ta>
            <ta e="T69" id="Seg_4218" s="T68">house-3PL-GEN</ta>
            <ta e="T70" id="Seg_4219" s="T69">skin-3PL-ACC</ta>
            <ta e="T71" id="Seg_4220" s="T70">patch-PRS-3PL</ta>
            <ta e="T72" id="Seg_4221" s="T71">be-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_4222" s="T72">hot</ta>
            <ta e="T74" id="Seg_4223" s="T73">very</ta>
            <ta e="T75" id="Seg_4224" s="T74">day.[NOM]</ta>
            <ta e="T76" id="Seg_4225" s="T75">stand-PST2-3SG</ta>
            <ta e="T77" id="Seg_4226" s="T76">Dudupta.[NOM]</ta>
            <ta e="T78" id="Seg_4227" s="T77">holy</ta>
            <ta e="T79" id="Seg_4228" s="T78">river-3PL-GEN</ta>
            <ta e="T80" id="Seg_4229" s="T79">water-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_4230" s="T80">windless-3SG-DAT/LOC</ta>
            <ta e="T82" id="Seg_4231" s="T81">white</ta>
            <ta e="T83" id="Seg_4232" s="T82">thunder.[NOM]</ta>
            <ta e="T84" id="Seg_4233" s="T83">cloud-PL-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_4234" s="T84">line.up-CVB.SEQ</ta>
            <ta e="T86" id="Seg_4235" s="T85">to.be.on.view-PRS-3PL</ta>
            <ta e="T87" id="Seg_4236" s="T86">mosquito.[NOM]</ta>
            <ta e="T88" id="Seg_4237" s="T87">poor.one.[NOM]</ta>
            <ta e="T89" id="Seg_4238" s="T88">wing-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_4239" s="T89">get.dry-PTCP.PRS</ta>
            <ta e="T91" id="Seg_4240" s="T90">heat-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_4241" s="T91">stand-PST2-3SG</ta>
            <ta e="T93" id="Seg_4242" s="T92">old.woman-PL-3SG.[NOM]</ta>
            <ta e="T94" id="Seg_4243" s="T93">that</ta>
            <ta e="T95" id="Seg_4244" s="T94">and</ta>
            <ta e="T96" id="Seg_4245" s="T95">be-COND.[3SG]</ta>
            <ta e="T97" id="Seg_4246" s="T96">hand-3PL.[NOM]</ta>
            <ta e="T98" id="Seg_4247" s="T97">free.time-POSS</ta>
            <ta e="T99" id="Seg_4248" s="T98">NEG</ta>
            <ta e="T100" id="Seg_4249" s="T99">chase.away-PRS-3PL</ta>
            <ta e="T101" id="Seg_4250" s="T100">that.[NOM]</ta>
            <ta e="T102" id="Seg_4251" s="T101">place.between-3SG-DAT/LOC</ta>
            <ta e="T103" id="Seg_4252" s="T102">straight</ta>
            <ta e="T104" id="Seg_4253" s="T103">sew-MED-CVB.SIM</ta>
            <ta e="T105" id="Seg_4254" s="T104">be-PST2-3PL</ta>
            <ta e="T106" id="Seg_4255" s="T105">needle-3PL-GEN</ta>
            <ta e="T107" id="Seg_4256" s="T106">sound-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_4257" s="T107">colourful</ta>
            <ta e="T109" id="Seg_4258" s="T108">skin-DAT/LOC</ta>
            <ta e="T110" id="Seg_4259" s="T109">rustle-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T111" id="Seg_4260" s="T110">distant-ABL</ta>
            <ta e="T112" id="Seg_4261" s="T111">be.heard-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_4262" s="T112">that</ta>
            <ta e="T114" id="Seg_4263" s="T113">work-VBZ-CVB.SIM</ta>
            <ta e="T115" id="Seg_4264" s="T114">sit-TEMP-3PL</ta>
            <ta e="T116" id="Seg_4265" s="T115">earth-3PL.[NOM]</ta>
            <ta e="T117" id="Seg_4266" s="T116">why</ta>
            <ta e="T118" id="Seg_4267" s="T117">INDEF</ta>
            <ta e="T119" id="Seg_4268" s="T118">roar-PTCP.PRS</ta>
            <ta e="T120" id="Seg_4269" s="T119">be-PST2.[3SG]</ta>
            <ta e="T121" id="Seg_4270" s="T120">INTJ</ta>
            <ta e="T122" id="Seg_4271" s="T121">say-PST2.[3SG]</ta>
            <ta e="T123" id="Seg_4272" s="T122">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T124" id="Seg_4273" s="T123">why</ta>
            <ta e="T125" id="Seg_4274" s="T124">earth-1PL.[NOM]</ta>
            <ta e="T126" id="Seg_4275" s="T125">roar-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_4276" s="T126">why</ta>
            <ta e="T128" id="Seg_4277" s="T127">thunder.[NOM]</ta>
            <ta e="T129" id="Seg_4278" s="T128">sound-3SG.[NOM]</ta>
            <ta e="T130" id="Seg_4279" s="T129">such.[NOM]</ta>
            <ta e="T131" id="Seg_4280" s="T130">be-FUT.[3SG]=Q</ta>
            <ta e="T132" id="Seg_4281" s="T131">what.[NOM]</ta>
            <ta e="T133" id="Seg_4282" s="T132">INDEF</ta>
            <ta e="T134" id="Seg_4283" s="T133">miracle-3SG.[NOM]</ta>
            <ta e="T135" id="Seg_4284" s="T134">be-PST1-3SG</ta>
            <ta e="T136" id="Seg_4285" s="T135">listen.[IMP.2SG]</ta>
            <ta e="T137" id="Seg_4286" s="T136">old.woman.[NOM]</ta>
            <ta e="T138" id="Seg_4287" s="T137">Kristin.[NOM]</ta>
            <ta e="T139" id="Seg_4288" s="T138">old</ta>
            <ta e="T140" id="Seg_4289" s="T139">hair-3SG.[NOM]</ta>
            <ta e="T141" id="Seg_4290" s="T140">take.away-PTCP.HAB</ta>
            <ta e="T142" id="Seg_4291" s="T141">glint.white-PTCP.PST</ta>
            <ta e="T143" id="Seg_4292" s="T142">old.woman.[NOM]</ta>
            <ta e="T144" id="Seg_4293" s="T143">mouth-3SG-DAT/LOC</ta>
            <ta e="T145" id="Seg_4294" s="T144">one</ta>
            <ta e="T146" id="Seg_4295" s="T145">NEG</ta>
            <ta e="T147" id="Seg_4296" s="T146">tooth-POSS</ta>
            <ta e="T148" id="Seg_4297" s="T147">NEG.[3SG]</ta>
            <ta e="T149" id="Seg_4298" s="T148">word.[NOM]</ta>
            <ta e="T150" id="Seg_4299" s="T149">go.in-NEG.PTCP</ta>
            <ta e="T151" id="Seg_4300" s="T150">kerchief-3SG-ACC</ta>
            <ta e="T152" id="Seg_4301" s="T151">head-3SG-ABL</ta>
            <ta e="T153" id="Seg_4302" s="T152">PFV</ta>
            <ta e="T154" id="Seg_4303" s="T153">pull-CVB.ANT</ta>
            <ta e="T155" id="Seg_4304" s="T154">listen-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_4305" s="T155">that.[NOM]</ta>
            <ta e="T157" id="Seg_4306" s="T520">two</ta>
            <ta e="T158" id="Seg_4307" s="T157">old.woman.[NOM]</ta>
            <ta e="T159" id="Seg_4308" s="T158">sky.[NOM]</ta>
            <ta e="T160" id="Seg_4309" s="T159">to</ta>
            <ta e="T161" id="Seg_4310" s="T160">crawl-CVB.SEQ</ta>
            <ta e="T162" id="Seg_4311" s="T161">after</ta>
            <ta e="T163" id="Seg_4312" s="T162">hunt-PTCP.PST</ta>
            <ta e="T164" id="Seg_4313" s="T163">reindeer.[NOM]</ta>
            <ta e="T165" id="Seg_4314" s="T164">similar</ta>
            <ta e="T166" id="Seg_4315" s="T165">roar-PTCP.PRS</ta>
            <ta e="T167" id="Seg_4316" s="T166">noise-ACC</ta>
            <ta e="T168" id="Seg_4317" s="T167">listen-CVB.SEQ</ta>
            <ta e="T170" id="Seg_4318" s="T168">become-PST2-3PL</ta>
            <ta e="T171" id="Seg_4319" s="T170">mosquito.[NOM]</ta>
            <ta e="T172" id="Seg_4320" s="T171">and</ta>
            <ta e="T173" id="Seg_4321" s="T172">eat-PTCP.PRS-3SG-ACC</ta>
            <ta e="T174" id="Seg_4322" s="T173">notice-NEG.CVB.SIM</ta>
            <ta e="T175" id="Seg_4323" s="T174">long-INTNS</ta>
            <ta e="T176" id="Seg_4324" s="T175">sit-PST2-3PL</ta>
            <ta e="T177" id="Seg_4325" s="T176">mouth-3PL-ACC</ta>
            <ta e="T178" id="Seg_4326" s="T177">open-CVB.SEQ</ta>
            <ta e="T179" id="Seg_4327" s="T178">after</ta>
            <ta e="T180" id="Seg_4328" s="T179">place.beneath-3PL-DAT/LOC</ta>
            <ta e="T181" id="Seg_4329" s="T180">fur-PL.[NOM]</ta>
            <ta e="T182" id="Seg_4330" s="T181">rag-3PL.[NOM]</ta>
            <ta e="T183" id="Seg_4331" s="T182">there-here</ta>
            <ta e="T184" id="Seg_4332" s="T183">lie-PRS-3PL</ta>
            <ta e="T185" id="Seg_4333" s="T184">string-INSTR</ta>
            <ta e="T186" id="Seg_4334" s="T185">tie-NMNZ-PROPR</ta>
            <ta e="T187" id="Seg_4335" s="T186">tobacco.pipe-3PL.[NOM]</ta>
            <ta e="T188" id="Seg_4336" s="T187">hand-3PL-DAT/LOC</ta>
            <ta e="T189" id="Seg_4337" s="T188">smoke-PRS-3PL</ta>
            <ta e="T190" id="Seg_4338" s="T189">wait-PTCP.PST.[NOM]</ta>
            <ta e="T191" id="Seg_4339" s="T190">similar</ta>
            <ta e="T192" id="Seg_4340" s="T191">ONOM-ONOM</ta>
            <ta e="T193" id="Seg_4341" s="T192">when</ta>
            <ta e="T194" id="Seg_4342" s="T193">sit-PTCP.FUT-3PL-ACC</ta>
            <ta e="T195" id="Seg_4343" s="T194">truth.[NOM]</ta>
            <ta e="T196" id="Seg_4344" s="T195">3PL.[NOM]</ta>
            <ta e="T197" id="Seg_4345" s="T196">know-NEG.PTCP.PST</ta>
            <ta e="T198" id="Seg_4346" s="T197">sound-3PL.[NOM]</ta>
            <ta e="T199" id="Seg_4347" s="T198">become-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_4348" s="T199">well</ta>
            <ta e="T201" id="Seg_4349" s="T200">EMPH</ta>
            <ta e="T202" id="Seg_4350" s="T201">well</ta>
            <ta e="T203" id="Seg_4351" s="T202">die-PST1-1PL</ta>
            <ta e="T204" id="Seg_4352" s="T203">say-CVB.SIM</ta>
            <ta e="T205" id="Seg_4353" s="T204">fall-CVB.ANT</ta>
            <ta e="T206" id="Seg_4354" s="T205">one.out.of.two</ta>
            <ta e="T207" id="Seg_4355" s="T206">old.woman.[NOM]</ta>
            <ta e="T208" id="Seg_4356" s="T207">stand.up-CVB.SIM</ta>
            <ta e="T209" id="Seg_4357" s="T208">jump-PST2.[3SG]</ta>
            <ta e="T210" id="Seg_4358" s="T209">forest.[NOM]</ta>
            <ta e="T211" id="Seg_4359" s="T210">to</ta>
            <ta e="T212" id="Seg_4360" s="T211">look.at-PST2.[3SG]</ta>
            <ta e="T213" id="Seg_4361" s="T212">well</ta>
            <ta e="T214" id="Seg_4362" s="T213">fly-CVB.SEQ</ta>
            <ta e="T215" id="Seg_4363" s="T214">go-PRS.[3SG]</ta>
            <ta e="T216" id="Seg_4364" s="T215">big</ta>
            <ta e="T217" id="Seg_4365" s="T216">bird-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_4366" s="T217">see.[IMP.2SG]</ta>
            <ta e="T219" id="Seg_4367" s="T218">that</ta>
            <ta e="T220" id="Seg_4368" s="T219">get.red-CVB.SEQ</ta>
            <ta e="T221" id="Seg_4369" s="T220">go-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_4370" s="T221">3SG.[NOM]</ta>
            <ta e="T223" id="Seg_4371" s="T222">back-3SG-GEN</ta>
            <ta e="T224" id="Seg_4372" s="T223">sound-3SG.[NOM]</ta>
            <ta e="T225" id="Seg_4373" s="T224">be-PST2.[3SG]</ta>
            <ta e="T226" id="Seg_4374" s="T225">my.god</ta>
            <ta e="T227" id="Seg_4375" s="T226">1SG.[NOM]</ta>
            <ta e="T228" id="Seg_4376" s="T227">speak-PST2.NEG-EP-1SG</ta>
            <ta e="T229" id="Seg_4377" s="T228">see-PST2.NEG-EP-1SG</ta>
            <ta e="T230" id="Seg_4378" s="T229">EMPH</ta>
            <ta e="T231" id="Seg_4379" s="T230">say-CVB.SEQ</ta>
            <ta e="T232" id="Seg_4380" s="T231">old.woman.[NOM]</ta>
            <ta e="T233" id="Seg_4381" s="T232">patter-CVB.SEQ</ta>
            <ta e="T234" id="Seg_4382" s="T233">go-PST2.[3SG]</ta>
            <ta e="T235" id="Seg_4383" s="T234">oh</ta>
            <ta e="T236" id="Seg_4384" s="T235">what.[NOM]</ta>
            <ta e="T237" id="Seg_4385" s="T236">bird-3SG-ACC</ta>
            <ta e="T238" id="Seg_4386" s="T237">make-PRS-2SG</ta>
            <ta e="T239" id="Seg_4387" s="T238">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T240" id="Seg_4388" s="T239">friend-3SG.[NOM]</ta>
            <ta e="T241" id="Seg_4389" s="T240">also</ta>
            <ta e="T242" id="Seg_4390" s="T241">look.at-PST2.[3SG]</ta>
            <ta e="T243" id="Seg_4391" s="T242">foot-3PL-GEN</ta>
            <ta e="T244" id="Seg_4392" s="T243">lower.part-3PL-DAT/LOC</ta>
            <ta e="T245" id="Seg_4393" s="T244">earth-3PL.[NOM]</ta>
            <ta e="T246" id="Seg_4394" s="T245">move-PTCP.PRS.[NOM]</ta>
            <ta e="T247" id="Seg_4395" s="T246">similar</ta>
            <ta e="T248" id="Seg_4396" s="T247">become-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_4397" s="T248">pole.tent-PL.[NOM]</ta>
            <ta e="T250" id="Seg_4398" s="T249">shiver-PST2-3PL</ta>
            <ta e="T251" id="Seg_4399" s="T250">fast-ADVZ</ta>
            <ta e="T252" id="Seg_4400" s="T251">sledge.[NOM]</ta>
            <ta e="T253" id="Seg_4401" s="T252">to</ta>
            <ta e="T254" id="Seg_4402" s="T253">lower.part-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_4403" s="T254">say-CVB.SIM</ta>
            <ta e="T256" id="Seg_4404" s="T255">fall-CVB.ANT</ta>
            <ta e="T257" id="Seg_4405" s="T256">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T258" id="Seg_4406" s="T257">INTJ</ta>
            <ta e="T259" id="Seg_4407" s="T258">make-PST2.[3SG]</ta>
            <ta e="T260" id="Seg_4408" s="T259">small.nail-PL-PROPR</ta>
            <ta e="T261" id="Seg_4409" s="T260">floor.[NOM]</ta>
            <ta e="T262" id="Seg_4410" s="T261">lower.part-3SG-DAT/LOC</ta>
            <ta e="T263" id="Seg_4411" s="T262">friend-3SG.[NOM]</ta>
            <ta e="T264" id="Seg_4412" s="T263">3SG-ACC</ta>
            <ta e="T265" id="Seg_4413" s="T264">repeat-CVB.SEQ</ta>
            <ta e="T266" id="Seg_4414" s="T265">go.in-PST2.[3SG]</ta>
            <ta e="T267" id="Seg_4415" s="T266">balok-3SG-GEN</ta>
            <ta e="T268" id="Seg_4416" s="T267">lower.part-3SG-DAT/LOC</ta>
            <ta e="T269" id="Seg_4417" s="T268">that</ta>
            <ta e="T270" id="Seg_4418" s="T269">time.[NOM]</ta>
            <ta e="T271" id="Seg_4419" s="T270">airman.[NOM]</ta>
            <ta e="T272" id="Seg_4420" s="T271">just</ta>
            <ta e="T273" id="Seg_4421" s="T272">pole.[NOM]</ta>
            <ta e="T274" id="Seg_4422" s="T273">tent-PL-ACC</ta>
            <ta e="T275" id="Seg_4423" s="T274">see-CVB.SIM</ta>
            <ta e="T276" id="Seg_4424" s="T275">soon</ta>
            <ta e="T277" id="Seg_4425" s="T276">low</ta>
            <ta e="T278" id="Seg_4426" s="T277">very</ta>
            <ta e="T279" id="Seg_4427" s="T278">fly-CVB.SEQ</ta>
            <ta e="T280" id="Seg_4428" s="T279">pass.by-PST2.[3SG]</ta>
            <ta e="T281" id="Seg_4429" s="T280">sledge.[NOM]</ta>
            <ta e="T282" id="Seg_4430" s="T281">house-ACC</ta>
            <ta e="T283" id="Seg_4431" s="T282">with</ta>
            <ta e="T284" id="Seg_4432" s="T283">balok.[NOM]</ta>
            <ta e="T285" id="Seg_4433" s="T284">lower.part-3PL-ABL</ta>
            <ta e="T288" id="Seg_4434" s="T287">old.woman-PL.[NOM]</ta>
            <ta e="T289" id="Seg_4435" s="T288">thin.layer.of.leather.[NOM]</ta>
            <ta e="T290" id="Seg_4436" s="T289">shoes-3PL.[NOM]</ta>
            <ta e="T291" id="Seg_4437" s="T290">move-NEG.CVB.SIM</ta>
            <ta e="T292" id="Seg_4438" s="T291">pull.out-CVB.SIM</ta>
            <ta e="T293" id="Seg_4439" s="T292">lie-PST2-3PL</ta>
            <ta e="T294" id="Seg_4440" s="T293">shore-DAT/LOC</ta>
            <ta e="T295" id="Seg_4441" s="T294">old.man-PL.[NOM]</ta>
            <ta e="T296" id="Seg_4442" s="T295">jump-CVB.SIM-jump-CVB.SIM</ta>
            <ta e="T297" id="Seg_4443" s="T296">cap-3PL-ACC</ta>
            <ta e="T298" id="Seg_4444" s="T297">airplane-DAT/LOC</ta>
            <ta e="T299" id="Seg_4445" s="T298">wave-CVB.SIM</ta>
            <ta e="T300" id="Seg_4446" s="T299">stand-PST2-3PL</ta>
            <ta e="T301" id="Seg_4447" s="T300">3PL.[NOM]</ta>
            <ta e="T302" id="Seg_4448" s="T301">be.happy-NMNZ-3PL.[NOM]</ta>
            <ta e="T303" id="Seg_4449" s="T302">for.the.first.time</ta>
            <ta e="T304" id="Seg_4450" s="T303">airplane.[NOM]</ta>
            <ta e="T305" id="Seg_4451" s="T304">come-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T306" id="Seg_4452" s="T305">tundra-DAT/LOC</ta>
            <ta e="T307" id="Seg_4453" s="T306">well</ta>
            <ta e="T308" id="Seg_4454" s="T307">be-PST2.[3SG]</ta>
            <ta e="T309" id="Seg_4455" s="T308">know-PRS-3PL</ta>
            <ta e="T312" id="Seg_4456" s="T311">know-RECP/COLL-PTCP.PRS</ta>
            <ta e="T313" id="Seg_4457" s="T312">be-CVB.SIM</ta>
            <ta e="T314" id="Seg_4458" s="T313">go-FUT-3SG</ta>
            <ta e="T315" id="Seg_4459" s="T314">how</ta>
            <ta e="T316" id="Seg_4460" s="T315">reindeer-AG-PL.[NOM]</ta>
            <ta e="T317" id="Seg_4461" s="T316">fish-AG-PL.[NOM]</ta>
            <ta e="T318" id="Seg_4462" s="T317">live-PTCP.PRS-3PL-ACC</ta>
            <ta e="T319" id="Seg_4463" s="T318">see-CVB.PURP</ta>
            <ta e="T320" id="Seg_4464" s="T319">mountain.[NOM]</ta>
            <ta e="T321" id="Seg_4465" s="T320">upper.part-3SG-DAT/LOC</ta>
            <ta e="T322" id="Seg_4466" s="T321">go.out-CVB.ANT</ta>
            <ta e="T323" id="Seg_4467" s="T322">wonder-EP-MED-PST2-3PL</ta>
            <ta e="T324" id="Seg_4468" s="T323">pole.[NOM]</ta>
            <ta e="T325" id="Seg_4469" s="T324">tent-3PL.[NOM]</ta>
            <ta e="T326" id="Seg_4470" s="T325">smoke-NEG-3PL</ta>
            <ta e="T327" id="Seg_4471" s="T326">yard.[NOM]</ta>
            <ta e="T328" id="Seg_4472" s="T327">upper.part-3SG-INSTR</ta>
            <ta e="T331" id="Seg_4473" s="T330">silence.[NOM]</ta>
            <ta e="T332" id="Seg_4474" s="T331">fade.away-ADVZ</ta>
            <ta e="T333" id="Seg_4475" s="T332">dog.[NOM]</ta>
            <ta e="T334" id="Seg_4476" s="T333">NEG</ta>
            <ta e="T335" id="Seg_4477" s="T334">bark-NEG.[3SG]</ta>
            <ta e="T336" id="Seg_4478" s="T335">human.being.[NOM]</ta>
            <ta e="T337" id="Seg_4479" s="T336">NEG</ta>
            <ta e="T338" id="Seg_4480" s="T337">voice-3SG.[NOM]</ta>
            <ta e="T339" id="Seg_4481" s="T338">be.heard-EP-NEG.[3SG]</ta>
            <ta e="T340" id="Seg_4482" s="T339">what</ta>
            <ta e="T341" id="Seg_4483" s="T340">people-PL.[NOM]</ta>
            <ta e="T342" id="Seg_4484" s="T341">this</ta>
            <ta e="T343" id="Seg_4485" s="T342">old.woman-PL.[NOM]</ta>
            <ta e="T344" id="Seg_4486" s="T343">know-NEG-3PL</ta>
            <ta e="T345" id="Seg_4487" s="T344">Q</ta>
            <ta e="T346" id="Seg_4488" s="T345">1PL.[NOM]</ta>
            <ta e="T347" id="Seg_4489" s="T346">come-PTCP.FUT-1PL-ACC</ta>
            <ta e="T348" id="Seg_4490" s="T347">say-PST2.[3SG]</ta>
            <ta e="T349" id="Seg_4491" s="T348">Onoo.[NOM]</ta>
            <ta e="T350" id="Seg_4492" s="T349">old.man.[NOM]</ta>
            <ta e="T351" id="Seg_4493" s="T350">where</ta>
            <ta e="T352" id="Seg_4494" s="T351">there.is-2PL=Q</ta>
            <ta e="T353" id="Seg_4495" s="T352">house-PROPR-PL.[NOM]</ta>
            <ta e="T356" id="Seg_4496" s="T355">shout-PST2.[3SG]</ta>
            <ta e="T357" id="Seg_4497" s="T356">inhabitant.[NOM]</ta>
            <ta e="T358" id="Seg_4498" s="T357">human.being-3SG.[NOM]</ta>
            <ta e="T359" id="Seg_4499" s="T358">oh.dear</ta>
            <ta e="T360" id="Seg_4500" s="T359">come.to.aid-IMP.2PL</ta>
            <ta e="T361" id="Seg_4501" s="T360">separate-EP-IMP.2PL</ta>
            <ta e="T362" id="Seg_4502" s="T361">die-PST1-1PL</ta>
            <ta e="T363" id="Seg_4503" s="T362">earth.[NOM]</ta>
            <ta e="T364" id="Seg_4504" s="T363">inside-3SG-ABL</ta>
            <ta e="T365" id="Seg_4505" s="T364">shout.[NOM]</ta>
            <ta e="T366" id="Seg_4506" s="T365">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T367" id="Seg_4507" s="T366">what-PL.[NOM]=Q</ta>
            <ta e="T368" id="Seg_4508" s="T367">get.crazy-PST2-3PL</ta>
            <ta e="T369" id="Seg_4509" s="T368">Q</ta>
            <ta e="T370" id="Seg_4510" s="T369">say-CVB.SIM</ta>
            <ta e="T371" id="Seg_4511" s="T370">fall-CVB.ANT</ta>
            <ta e="T372" id="Seg_4512" s="T371">Onoo.[NOM]</ta>
            <ta e="T373" id="Seg_4513" s="T372">look.at-PST2.[3SG]</ta>
            <ta e="T374" id="Seg_4514" s="T373">3SG.[NOM]</ta>
            <ta e="T375" id="Seg_4515" s="T374">sledge.[NOM]</ta>
            <ta e="T376" id="Seg_4516" s="T375">house-PL.[NOM]</ta>
            <ta e="T377" id="Seg_4517" s="T376">lower.part-3PL-DAT/LOC</ta>
            <ta e="T378" id="Seg_4518" s="T377">look.at-PST2.[3SG]</ta>
            <ta e="T379" id="Seg_4519" s="T378">what.[NOM]</ta>
            <ta e="T380" id="Seg_4520" s="T379">miracle-3SG.[NOM]</ta>
            <ta e="T381" id="Seg_4521" s="T380">be-PST1-3SG</ta>
            <ta e="T382" id="Seg_4522" s="T381">old.woman-3PL-GEN</ta>
            <ta e="T383" id="Seg_4523" s="T382">leg-3PL.[NOM]</ta>
            <ta e="T384" id="Seg_4524" s="T383">there</ta>
            <ta e="T385" id="Seg_4525" s="T384">shiver-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T386" id="Seg_4526" s="T385">lie-PST2-3PL</ta>
            <ta e="T387" id="Seg_4527" s="T386">see-TEMP-3SG</ta>
            <ta e="T388" id="Seg_4528" s="T387">go.out-CVB.SIM</ta>
            <ta e="T389" id="Seg_4529" s="T388">try-PRS-3PL</ta>
            <ta e="T390" id="Seg_4530" s="T389">apparently</ta>
            <ta e="T391" id="Seg_4531" s="T390">small.nail-PL.[NOM]</ta>
            <ta e="T392" id="Seg_4532" s="T391">clothes-3PL-ABL</ta>
            <ta e="T393" id="Seg_4533" s="T392">fasten-PST2-3PL</ta>
            <ta e="T394" id="Seg_4534" s="T393">why</ta>
            <ta e="T395" id="Seg_4535" s="T394">release-FUT-3PL=Q</ta>
            <ta e="T396" id="Seg_4536" s="T395">inside-DAT/LOC</ta>
            <ta e="T397" id="Seg_4537" s="T396">NEG</ta>
            <ta e="T398" id="Seg_4538" s="T397">go.in-CAUS-NEG-3PL</ta>
            <ta e="T399" id="Seg_4539" s="T398">outer-DAT/LOC</ta>
            <ta e="T400" id="Seg_4540" s="T399">NEG</ta>
            <ta e="T401" id="Seg_4541" s="T400">take.out-NEG-3PL</ta>
            <ta e="T402" id="Seg_4542" s="T401">oh.dear</ta>
            <ta e="T403" id="Seg_4543" s="T402">say-EP-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T404" id="Seg_4544" s="T403">become.more-CVB.SEQ</ta>
            <ta e="T405" id="Seg_4545" s="T404">go-PST2.[3SG]</ta>
            <ta e="T406" id="Seg_4546" s="T405">moan-PTCP.PRS.[NOM]</ta>
            <ta e="T407" id="Seg_4547" s="T406">add-EP-PST2.[3SG]</ta>
            <ta e="T408" id="Seg_4548" s="T407">old.man-PL.[NOM]</ta>
            <ta e="T409" id="Seg_4549" s="T408">what.[NOM]</ta>
            <ta e="T410" id="Seg_4550" s="T409">NEG</ta>
            <ta e="T411" id="Seg_4551" s="T410">to</ta>
            <ta e="T412" id="Seg_4552" s="T411">power-3SG-ABL</ta>
            <ta e="T413" id="Seg_4553" s="T412">wonder-CVB.SIM</ta>
            <ta e="T414" id="Seg_4554" s="T413">not.manage-CVB.SEQ</ta>
            <ta e="T415" id="Seg_4555" s="T414">what-ACC</ta>
            <ta e="T416" id="Seg_4556" s="T415">NEG</ta>
            <ta e="T417" id="Seg_4557" s="T416">speak-NEG.PTCP</ta>
            <ta e="T418" id="Seg_4558" s="T417">be-PST2-3PL</ta>
            <ta e="T419" id="Seg_4559" s="T418">old.woman-PL-ACC</ta>
            <ta e="T420" id="Seg_4560" s="T419">one-DISTR</ta>
            <ta e="T421" id="Seg_4561" s="T420">free-PST2-3PL</ta>
            <ta e="T422" id="Seg_4562" s="T421">3PL-DAT/LOC</ta>
            <ta e="T423" id="Seg_4563" s="T422">face.[NOM]</ta>
            <ta e="T424" id="Seg_4564" s="T423">come-FUT.[3SG]</ta>
            <ta e="T425" id="Seg_4565" s="T424">MOD</ta>
            <ta e="T426" id="Seg_4566" s="T425">hair-3PL.[NOM]</ta>
            <ta e="T427" id="Seg_4567" s="T426">eye-3PL-ACC</ta>
            <ta e="T428" id="Seg_4568" s="T427">cover-PST2.[3SG]</ta>
            <ta e="T429" id="Seg_4569" s="T428">thin.layer.of.leather.[NOM]</ta>
            <ta e="T430" id="Seg_4570" s="T429">shoes-3PL.[NOM]</ta>
            <ta e="T431" id="Seg_4571" s="T430">be.clinched-ADVZ</ta>
            <ta e="T432" id="Seg_4572" s="T431">fall-PST2-3PL</ta>
            <ta e="T433" id="Seg_4573" s="T432">shape-3PL.[NOM]</ta>
            <ta e="T434" id="Seg_4574" s="T433">change-PST2.[3SG]</ta>
            <ta e="T435" id="Seg_4575" s="T434">earth-ABL</ta>
            <ta e="T436" id="Seg_4576" s="T435">go.out-EP-PTCP.PST</ta>
            <ta e="T437" id="Seg_4577" s="T436">polar.fox.[NOM]</ta>
            <ta e="T438" id="Seg_4578" s="T437">child-PL-3SG-ACC</ta>
            <ta e="T439" id="Seg_4579" s="T438">similar</ta>
            <ta e="T440" id="Seg_4580" s="T439">become-PST2-3PL</ta>
            <ta e="T441" id="Seg_4581" s="T440">well</ta>
            <ta e="T442" id="Seg_4582" s="T441">what.[NOM]</ta>
            <ta e="T443" id="Seg_4583" s="T442">be-PST2-2PL=Q</ta>
            <ta e="T444" id="Seg_4584" s="T443">say-CVB.SEQ</ta>
            <ta e="T445" id="Seg_4585" s="T444">ask-PST2-3PL</ta>
            <ta e="T446" id="Seg_4586" s="T445">one-DISTR</ta>
            <ta e="T447" id="Seg_4587" s="T446">moment.[NOM]</ta>
            <ta e="T448" id="Seg_4588" s="T447">quiet</ta>
            <ta e="T449" id="Seg_4589" s="T448">know-NEG.PTCP.PST</ta>
            <ta e="T450" id="Seg_4590" s="T449">bird-1PL.[NOM]</ta>
            <ta e="T451" id="Seg_4591" s="T450">pass.by-PST1-3SG</ta>
            <ta e="T452" id="Seg_4592" s="T451">say-CVB.SEQ</ta>
            <ta e="T453" id="Seg_4593" s="T452">whisper-PST2-3PL</ta>
            <ta e="T454" id="Seg_4594" s="T453">two</ta>
            <ta e="T455" id="Seg_4595" s="T454">old.woman.[NOM]</ta>
            <ta e="T456" id="Seg_4596" s="T455">oh</ta>
            <ta e="T457" id="Seg_4597" s="T456">know-NEG.PTCP.PST</ta>
            <ta e="T458" id="Seg_4598" s="T457">be-CVB.SEQ</ta>
            <ta e="T459" id="Seg_4599" s="T458">Onoo.[NOM]</ta>
            <ta e="T460" id="Seg_4600" s="T459">agree-PST2.[3SG]</ta>
            <ta e="T461" id="Seg_4601" s="T460">well</ta>
            <ta e="T462" id="Seg_4602" s="T461">tell-IMP.2PL</ta>
            <ta e="T463" id="Seg_4603" s="T462">what.[NOM]</ta>
            <ta e="T464" id="Seg_4604" s="T463">dream-3SG-ACC</ta>
            <ta e="T465" id="Seg_4605" s="T464">dream-PST1-2PL</ta>
            <ta e="T466" id="Seg_4606" s="T465">sledge.[NOM]</ta>
            <ta e="T467" id="Seg_4607" s="T466">house.[NOM]</ta>
            <ta e="T468" id="Seg_4608" s="T467">lower.part-3SG-DAT/LOC</ta>
            <ta e="T469" id="Seg_4609" s="T468">lie-CVB.SEQ-2PL</ta>
            <ta e="T470" id="Seg_4610" s="T469">this</ta>
            <ta e="T471" id="Seg_4611" s="T470">what.[NOM]</ta>
            <ta e="T472" id="Seg_4612" s="T471">be-PRS.[3SG]</ta>
            <ta e="T473" id="Seg_4613" s="T472">old.man.[NOM]=Q</ta>
            <ta e="T474" id="Seg_4614" s="T473">age-CVB.SEQ</ta>
            <ta e="T475" id="Seg_4615" s="T474">mind-3SG.[NOM]</ta>
            <ta e="T476" id="Seg_4616" s="T475">shorten-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_4617" s="T476">apparently</ta>
            <ta e="T478" id="Seg_4618" s="T477">yell-PST2-3PL</ta>
            <ta e="T479" id="Seg_4619" s="T478">two</ta>
            <ta e="T480" id="Seg_4620" s="T479">scary-PL.[NOM]</ta>
            <ta e="T481" id="Seg_4621" s="T480">however</ta>
            <ta e="T482" id="Seg_4622" s="T481">hare.[NOM]</ta>
            <ta e="T483" id="Seg_4623" s="T482">heart-3PL.[NOM]</ta>
            <ta e="T484" id="Seg_4624" s="T483">still</ta>
            <ta e="T485" id="Seg_4625" s="T484">language-PROPR.[NOM]</ta>
            <ta e="T486" id="Seg_4626" s="T485">become-PST2-3PL</ta>
            <ta e="T487" id="Seg_4627" s="T486">know-NEG-2PL</ta>
            <ta e="T488" id="Seg_4628" s="T487">Q</ta>
            <ta e="T489" id="Seg_4629" s="T488">time.[NOM]</ta>
            <ta e="T490" id="Seg_4630" s="T489">change-PST2-3SG-ACC</ta>
            <ta e="T491" id="Seg_4631" s="T490">Soviet-PL.[NOM]</ta>
            <ta e="T492" id="Seg_4632" s="T491">small.bird-3PL.[NOM]</ta>
            <ta e="T493" id="Seg_4633" s="T492">come-PTCP.PRS-3SG-ACC</ta>
            <ta e="T494" id="Seg_4634" s="T493">that-3SG-2PL.[NOM]</ta>
            <ta e="T495" id="Seg_4635" s="T494">2PL-ACC</ta>
            <ta e="T496" id="Seg_4636" s="T495">come.to.aid-CVB.SIM</ta>
            <ta e="T497" id="Seg_4637" s="T496">fly-CVB.SIM</ta>
            <ta e="T498" id="Seg_4638" s="T497">go-PRS.[3SG]</ta>
            <ta e="T499" id="Seg_4639" s="T498">Dolgan</ta>
            <ta e="T500" id="Seg_4640" s="T499">people-3SG.[NOM]</ta>
            <ta e="T501" id="Seg_4641" s="T500">sated.[NOM]</ta>
            <ta e="T502" id="Seg_4642" s="T501">rich.[NOM]</ta>
            <ta e="T503" id="Seg_4643" s="T502">live-PTCP.FUT-PL-3SG-ACC</ta>
            <ta e="T504" id="Seg_4644" s="T503">say-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T505" id="Seg_4645" s="T504">two</ta>
            <ta e="T506" id="Seg_4646" s="T505">human.being.[NOM]</ta>
            <ta e="T507" id="Seg_4647" s="T506">old.woman-PL.[NOM]</ta>
            <ta e="T508" id="Seg_4648" s="T507">what.[NOM]</ta>
            <ta e="T509" id="Seg_4649" s="T508">and</ta>
            <ta e="T510" id="Seg_4650" s="T509">think-PTCP.FUT-3PL-ACC</ta>
            <ta e="T511" id="Seg_4651" s="T510">power-3SG-ABL</ta>
            <ta e="T512" id="Seg_4652" s="T511">voice-POSS</ta>
            <ta e="T513" id="Seg_4653" s="T512">NEG</ta>
            <ta e="T514" id="Seg_4654" s="T513">feel.ashamed-PTCP.PST.[NOM]</ta>
            <ta e="T515" id="Seg_4655" s="T514">similar</ta>
            <ta e="T516" id="Seg_4656" s="T515">house-3PL-ACC</ta>
            <ta e="T517" id="Seg_4657" s="T516">to</ta>
            <ta e="T518" id="Seg_4658" s="T517">slouch-RECP/COLL-CVB.SIM</ta>
            <ta e="T519" id="Seg_4659" s="T518">stand-PST2-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_4660" s="T0">Sowjet-PL.[NOM]</ta>
            <ta e="T2" id="Seg_4661" s="T1">Gesetz-3PL.[NOM]</ta>
            <ta e="T3" id="Seg_4662" s="T2">soeben</ta>
            <ta e="T4" id="Seg_4663" s="T3">kommen-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T5" id="Seg_4664" s="T4">Kurja</ta>
            <ta e="T6" id="Seg_4665" s="T5">sagen-CVB.SEQ</ta>
            <ta e="T7" id="Seg_4666" s="T6">Siedlung-DAT/LOC</ta>
            <ta e="T8" id="Seg_4667" s="T7">Sommer.[NOM]</ta>
            <ta e="T9" id="Seg_4668" s="T8">jeder</ta>
            <ta e="T10" id="Seg_4669" s="T9">fischen-CVB.SIM</ta>
            <ta e="T11" id="Seg_4670" s="T10">sitzen-PTCP.HAB</ta>
            <ta e="T12" id="Seg_4671" s="T11">sein-PST1-3PL</ta>
            <ta e="T13" id="Seg_4672" s="T12">dolganisch</ta>
            <ta e="T14" id="Seg_4673" s="T13">Volk-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_4674" s="T14">quer.über</ta>
            <ta e="T16" id="Seg_4675" s="T15">Auge-PROPR.[NOM]</ta>
            <ta e="T17" id="Seg_4676" s="T16">oft-oft</ta>
            <ta e="T18" id="Seg_4677" s="T17">gehen-EP-NEG.PTCP</ta>
            <ta e="T19" id="Seg_4678" s="T18">Siedlung-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_4679" s="T19">solch</ta>
            <ta e="T21" id="Seg_4680" s="T20">fern.[NOM]</ta>
            <ta e="T22" id="Seg_4681" s="T21">fischen-PTCP.PRS</ta>
            <ta e="T23" id="Seg_4682" s="T22">Ort-3PL.[NOM]</ta>
            <ta e="T24" id="Seg_4683" s="T23">wann</ta>
            <ta e="T25" id="Seg_4684" s="T24">INDEF</ta>
            <ta e="T26" id="Seg_4685" s="T25">wer.[NOM]</ta>
            <ta e="T27" id="Seg_4686" s="T26">INDEF</ta>
            <ta e="T28" id="Seg_4687" s="T27">kommen-FUT.[3SG]</ta>
            <ta e="T29" id="Seg_4688" s="T28">im.Sommer</ta>
            <ta e="T30" id="Seg_4689" s="T29">nah-ADJZ</ta>
            <ta e="T31" id="Seg_4690" s="T30">Fäule-PL-DAT/LOC</ta>
            <ta e="T32" id="Seg_4691" s="T31">Neuigkeit.[NOM]</ta>
            <ta e="T33" id="Seg_4692" s="T32">erzählen-CVB.SIM</ta>
            <ta e="T34" id="Seg_4693" s="T33">selbst-3SG-GEN</ta>
            <ta e="T35" id="Seg_4694" s="T34">Leben-3SG-GEN</ta>
            <ta e="T36" id="Seg_4695" s="T35">Seite-3SG-INSTR</ta>
            <ta e="T37" id="Seg_4696" s="T36">erfahren-CVB.PURP</ta>
            <ta e="T38" id="Seg_4697" s="T37">wie</ta>
            <ta e="T39" id="Seg_4698" s="T38">fischen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T40" id="Seg_4699" s="T39">Kurja.[NOM]</ta>
            <ta e="T41" id="Seg_4700" s="T40">Volk-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_4701" s="T41">eins</ta>
            <ta e="T43" id="Seg_4702" s="T42">Sommer.[NOM]</ta>
            <ta e="T44" id="Seg_4703" s="T43">alter.Mann-PL.[NOM]</ta>
            <ta e="T45" id="Seg_4704" s="T44">früh</ta>
            <ta e="T46" id="Seg_4705" s="T45">am.Morgen</ta>
            <ta e="T47" id="Seg_4706" s="T46">rudern-EP-MED-CVB.SEQ</ta>
            <ta e="T48" id="Seg_4707" s="T47">bleiben-PST2-3PL</ta>
            <ta e="T49" id="Seg_4708" s="T48">Zufluss-PL-3SG-ACC</ta>
            <ta e="T50" id="Seg_4709" s="T49">sehen-EP-MED-CVB.SIM</ta>
            <ta e="T51" id="Seg_4710" s="T50">Frau.[NOM]</ta>
            <ta e="T52" id="Seg_4711" s="T51">Kind.[NOM]</ta>
            <ta e="T53" id="Seg_4712" s="T52">Verwandter.[NOM]</ta>
            <ta e="T54" id="Seg_4713" s="T53">Haus-3PL-DAT/LOC</ta>
            <ta e="T55" id="Seg_4714" s="T54">sitzen-PST2-3PL</ta>
            <ta e="T56" id="Seg_4715" s="T55">wer.[NOM]</ta>
            <ta e="T57" id="Seg_4716" s="T56">Fisch.[NOM]</ta>
            <ta e="T58" id="Seg_4717" s="T57">säubern-EP-MED-PRS.[3SG]</ta>
            <ta e="T59" id="Seg_4718" s="T58">wer.[NOM]</ta>
            <ta e="T60" id="Seg_4719" s="T59">getrockneter.Fisch.[NOM]</ta>
            <ta e="T61" id="Seg_4720" s="T60">trocknen-EP-MED-PRS.[3SG]</ta>
            <ta e="T62" id="Seg_4721" s="T61">jenes-PL-ABL</ta>
            <ta e="T63" id="Seg_4722" s="T62">zwei</ta>
            <ta e="T64" id="Seg_4723" s="T63">Alte.[NOM]</ta>
            <ta e="T65" id="Seg_4724" s="T64">alter.Mann-PL.[NOM]</ta>
            <ta e="T66" id="Seg_4725" s="T65">kommen-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T67" id="Seg_4726" s="T66">bis.zu</ta>
            <ta e="T68" id="Seg_4727" s="T67">Schlitten.[NOM]</ta>
            <ta e="T69" id="Seg_4728" s="T68">Haus-3PL-GEN</ta>
            <ta e="T70" id="Seg_4729" s="T69">Haut-3PL-ACC</ta>
            <ta e="T71" id="Seg_4730" s="T70">flicken-PRS-3PL</ta>
            <ta e="T72" id="Seg_4731" s="T71">sein-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_4732" s="T72">heiß</ta>
            <ta e="T74" id="Seg_4733" s="T73">sehr</ta>
            <ta e="T75" id="Seg_4734" s="T74">Tag.[NOM]</ta>
            <ta e="T76" id="Seg_4735" s="T75">stehen-PST2-3SG</ta>
            <ta e="T77" id="Seg_4736" s="T76">Dudupta.[NOM]</ta>
            <ta e="T78" id="Seg_4737" s="T77">heilig</ta>
            <ta e="T79" id="Seg_4738" s="T78">Fluss-3PL-GEN</ta>
            <ta e="T80" id="Seg_4739" s="T79">Wasser-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_4740" s="T80">windstill-3SG-DAT/LOC</ta>
            <ta e="T82" id="Seg_4741" s="T81">weiß</ta>
            <ta e="T83" id="Seg_4742" s="T82">Donner.[NOM]</ta>
            <ta e="T84" id="Seg_4743" s="T83">Wolke-PL-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_4744" s="T84">aufreihen-CVB.SEQ</ta>
            <ta e="T86" id="Seg_4745" s="T85">zu.sehen.sein-PRS-3PL</ta>
            <ta e="T87" id="Seg_4746" s="T86">Mücke.[NOM]</ta>
            <ta e="T88" id="Seg_4747" s="T87">Armer.[NOM]</ta>
            <ta e="T89" id="Seg_4748" s="T88">Flügel-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_4749" s="T89">trocken.werden-PTCP.PRS</ta>
            <ta e="T91" id="Seg_4750" s="T90">Hitze-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_4751" s="T91">stehen-PST2-3SG</ta>
            <ta e="T93" id="Seg_4752" s="T92">Alte-PL-3SG.[NOM]</ta>
            <ta e="T94" id="Seg_4753" s="T93">jenes</ta>
            <ta e="T95" id="Seg_4754" s="T94">und</ta>
            <ta e="T96" id="Seg_4755" s="T95">sein-COND.[3SG]</ta>
            <ta e="T97" id="Seg_4756" s="T96">Hand-3PL.[NOM]</ta>
            <ta e="T98" id="Seg_4757" s="T97">Freizeit-POSS</ta>
            <ta e="T99" id="Seg_4758" s="T98">NEG</ta>
            <ta e="T100" id="Seg_4759" s="T99">verjagen-PRS-3PL</ta>
            <ta e="T101" id="Seg_4760" s="T100">jenes.[NOM]</ta>
            <ta e="T102" id="Seg_4761" s="T101">Zwischenraum-3SG-DAT/LOC</ta>
            <ta e="T103" id="Seg_4762" s="T102">direkt</ta>
            <ta e="T104" id="Seg_4763" s="T103">nähen-MED-CVB.SIM</ta>
            <ta e="T105" id="Seg_4764" s="T104">sein-PST2-3PL</ta>
            <ta e="T106" id="Seg_4765" s="T105">Nadel-3PL-GEN</ta>
            <ta e="T107" id="Seg_4766" s="T106">Geräusch-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_4767" s="T107">farbig</ta>
            <ta e="T109" id="Seg_4768" s="T108">Haut-DAT/LOC</ta>
            <ta e="T110" id="Seg_4769" s="T109">knistern-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T111" id="Seg_4770" s="T110">fern-ABL</ta>
            <ta e="T112" id="Seg_4771" s="T111">gehört.werden-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_4772" s="T112">jenes</ta>
            <ta e="T114" id="Seg_4773" s="T113">Arbeit-VBZ-CVB.SIM</ta>
            <ta e="T115" id="Seg_4774" s="T114">sitzen-TEMP-3PL</ta>
            <ta e="T116" id="Seg_4775" s="T115">Erde-3PL.[NOM]</ta>
            <ta e="T117" id="Seg_4776" s="T116">warum</ta>
            <ta e="T118" id="Seg_4777" s="T117">INDEF</ta>
            <ta e="T119" id="Seg_4778" s="T118">dröhnen-PTCP.PRS</ta>
            <ta e="T120" id="Seg_4779" s="T119">sein-PST2.[3SG]</ta>
            <ta e="T121" id="Seg_4780" s="T120">INTJ</ta>
            <ta e="T122" id="Seg_4781" s="T121">sagen-PST2.[3SG]</ta>
            <ta e="T123" id="Seg_4782" s="T122">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T124" id="Seg_4783" s="T123">warum</ta>
            <ta e="T125" id="Seg_4784" s="T124">Erde-1PL.[NOM]</ta>
            <ta e="T126" id="Seg_4785" s="T125">dröhnen-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_4786" s="T126">warum</ta>
            <ta e="T128" id="Seg_4787" s="T127">Donner.[NOM]</ta>
            <ta e="T129" id="Seg_4788" s="T128">Geräusch-3SG.[NOM]</ta>
            <ta e="T130" id="Seg_4789" s="T129">solch.[NOM]</ta>
            <ta e="T131" id="Seg_4790" s="T130">sein-FUT.[3SG]=Q</ta>
            <ta e="T132" id="Seg_4791" s="T131">was.[NOM]</ta>
            <ta e="T133" id="Seg_4792" s="T132">INDEF</ta>
            <ta e="T134" id="Seg_4793" s="T133">Wunder-3SG.[NOM]</ta>
            <ta e="T135" id="Seg_4794" s="T134">sein-PST1-3SG</ta>
            <ta e="T136" id="Seg_4795" s="T135">zuhören.[IMP.2SG]</ta>
            <ta e="T137" id="Seg_4796" s="T136">Alte.[NOM]</ta>
            <ta e="T138" id="Seg_4797" s="T137">Kristin.[NOM]</ta>
            <ta e="T139" id="Seg_4798" s="T138">alt</ta>
            <ta e="T140" id="Seg_4799" s="T139">Haar-3SG.[NOM]</ta>
            <ta e="T141" id="Seg_4800" s="T140">wegnehmen-PTCP.HAB</ta>
            <ta e="T142" id="Seg_4801" s="T141">weiß.schimmern-PTCP.PST</ta>
            <ta e="T143" id="Seg_4802" s="T142">Alte.[NOM]</ta>
            <ta e="T144" id="Seg_4803" s="T143">Mund-3SG-DAT/LOC</ta>
            <ta e="T145" id="Seg_4804" s="T144">eins</ta>
            <ta e="T146" id="Seg_4805" s="T145">NEG</ta>
            <ta e="T147" id="Seg_4806" s="T146">Zahn-POSS</ta>
            <ta e="T148" id="Seg_4807" s="T147">NEG.[3SG]</ta>
            <ta e="T149" id="Seg_4808" s="T148">Wort.[NOM]</ta>
            <ta e="T150" id="Seg_4809" s="T149">hineingehen-NEG.PTCP</ta>
            <ta e="T151" id="Seg_4810" s="T150">Tuch-3SG-ACC</ta>
            <ta e="T152" id="Seg_4811" s="T151">Kopf-3SG-ABL</ta>
            <ta e="T153" id="Seg_4812" s="T152">PFV</ta>
            <ta e="T154" id="Seg_4813" s="T153">ziehen-CVB.ANT</ta>
            <ta e="T155" id="Seg_4814" s="T154">zuhören-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_4815" s="T155">jenes.[NOM]</ta>
            <ta e="T157" id="Seg_4816" s="T520">zwei</ta>
            <ta e="T158" id="Seg_4817" s="T157">Alte.[NOM]</ta>
            <ta e="T159" id="Seg_4818" s="T158">Himmel.[NOM]</ta>
            <ta e="T160" id="Seg_4819" s="T159">zu</ta>
            <ta e="T161" id="Seg_4820" s="T160">kriechen-CVB.SEQ</ta>
            <ta e="T162" id="Seg_4821" s="T161">nachdem</ta>
            <ta e="T163" id="Seg_4822" s="T162">jagen-PTCP.PST</ta>
            <ta e="T164" id="Seg_4823" s="T163">Rentier.[NOM]</ta>
            <ta e="T165" id="Seg_4824" s="T164">ähnlich</ta>
            <ta e="T166" id="Seg_4825" s="T165">dröhnen-PTCP.PRS</ta>
            <ta e="T167" id="Seg_4826" s="T166">Lärm-ACC</ta>
            <ta e="T168" id="Seg_4827" s="T167">zuhören-CVB.SEQ</ta>
            <ta e="T170" id="Seg_4828" s="T168">werden-PST2-3PL</ta>
            <ta e="T171" id="Seg_4829" s="T170">Mücke.[NOM]</ta>
            <ta e="T172" id="Seg_4830" s="T171">und</ta>
            <ta e="T173" id="Seg_4831" s="T172">essen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T174" id="Seg_4832" s="T173">bemerken-NEG.CVB.SIM</ta>
            <ta e="T175" id="Seg_4833" s="T174">lange-INTNS</ta>
            <ta e="T176" id="Seg_4834" s="T175">sitzen-PST2-3PL</ta>
            <ta e="T177" id="Seg_4835" s="T176">Mund-3PL-ACC</ta>
            <ta e="T178" id="Seg_4836" s="T177">öffnen-CVB.SEQ</ta>
            <ta e="T179" id="Seg_4837" s="T178">nachdem</ta>
            <ta e="T180" id="Seg_4838" s="T179">Platz.neben-3PL-DAT/LOC</ta>
            <ta e="T181" id="Seg_4839" s="T180">Fell-PL.[NOM]</ta>
            <ta e="T182" id="Seg_4840" s="T181">Fetzen-3PL.[NOM]</ta>
            <ta e="T183" id="Seg_4841" s="T182">dort-hier</ta>
            <ta e="T184" id="Seg_4842" s="T183">liegen-PRS-3PL</ta>
            <ta e="T185" id="Seg_4843" s="T184">Schnur-INSTR</ta>
            <ta e="T186" id="Seg_4844" s="T185">binden-NMNZ-PROPR</ta>
            <ta e="T187" id="Seg_4845" s="T186">Tabakspfeife-3PL.[NOM]</ta>
            <ta e="T188" id="Seg_4846" s="T187">Hand-3PL-DAT/LOC</ta>
            <ta e="T189" id="Seg_4847" s="T188">rauchen-PRS-3PL</ta>
            <ta e="T190" id="Seg_4848" s="T189">warten-PTCP.PST.[NOM]</ta>
            <ta e="T191" id="Seg_4849" s="T190">ähnlich</ta>
            <ta e="T192" id="Seg_4850" s="T191">ONOM-ONOM</ta>
            <ta e="T193" id="Seg_4851" s="T192">wann</ta>
            <ta e="T194" id="Seg_4852" s="T193">sitzen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T195" id="Seg_4853" s="T194">Wahrheit.[NOM]</ta>
            <ta e="T196" id="Seg_4854" s="T195">3PL.[NOM]</ta>
            <ta e="T197" id="Seg_4855" s="T196">wissen-NEG.PTCP.PST</ta>
            <ta e="T198" id="Seg_4856" s="T197">Geräusch-3PL.[NOM]</ta>
            <ta e="T199" id="Seg_4857" s="T198">werden-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_4858" s="T199">na</ta>
            <ta e="T201" id="Seg_4859" s="T200">EMPH</ta>
            <ta e="T202" id="Seg_4860" s="T201">doch</ta>
            <ta e="T203" id="Seg_4861" s="T202">sterben-PST1-1PL</ta>
            <ta e="T204" id="Seg_4862" s="T203">sagen-CVB.SIM</ta>
            <ta e="T205" id="Seg_4863" s="T204">fallen-CVB.ANT</ta>
            <ta e="T206" id="Seg_4864" s="T205">einer.von.zwei</ta>
            <ta e="T207" id="Seg_4865" s="T206">Alte.[NOM]</ta>
            <ta e="T208" id="Seg_4866" s="T207">aufstehen-CVB.SIM</ta>
            <ta e="T209" id="Seg_4867" s="T208">springen-PST2.[3SG]</ta>
            <ta e="T210" id="Seg_4868" s="T209">Wald.[NOM]</ta>
            <ta e="T211" id="Seg_4869" s="T210">zu</ta>
            <ta e="T212" id="Seg_4870" s="T211">schauen.auf-PST2.[3SG]</ta>
            <ta e="T213" id="Seg_4871" s="T212">doch</ta>
            <ta e="T214" id="Seg_4872" s="T213">fliegen-CVB.SEQ</ta>
            <ta e="T215" id="Seg_4873" s="T214">gehen-PRS.[3SG]</ta>
            <ta e="T216" id="Seg_4874" s="T215">groß</ta>
            <ta e="T217" id="Seg_4875" s="T216">Vogel-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_4876" s="T217">sehen.[IMP.2SG]</ta>
            <ta e="T219" id="Seg_4877" s="T218">jenes</ta>
            <ta e="T220" id="Seg_4878" s="T219">rot.werden-CVB.SEQ</ta>
            <ta e="T221" id="Seg_4879" s="T220">gehen-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_4880" s="T221">3SG.[NOM]</ta>
            <ta e="T223" id="Seg_4881" s="T222">Rücken-3SG-GEN</ta>
            <ta e="T224" id="Seg_4882" s="T223">Geräusch-3SG.[NOM]</ta>
            <ta e="T225" id="Seg_4883" s="T224">sein-PST2.[3SG]</ta>
            <ta e="T226" id="Seg_4884" s="T225">mein.Gott</ta>
            <ta e="T227" id="Seg_4885" s="T226">1SG.[NOM]</ta>
            <ta e="T228" id="Seg_4886" s="T227">sprechen-PST2.NEG-EP-1SG</ta>
            <ta e="T229" id="Seg_4887" s="T228">sehen-PST2.NEG-EP-1SG</ta>
            <ta e="T230" id="Seg_4888" s="T229">EMPH</ta>
            <ta e="T231" id="Seg_4889" s="T230">sagen-CVB.SEQ</ta>
            <ta e="T232" id="Seg_4890" s="T231">Alte.[NOM]</ta>
            <ta e="T233" id="Seg_4891" s="T232">trappeln-CVB.SEQ</ta>
            <ta e="T234" id="Seg_4892" s="T233">gehen-PST2.[3SG]</ta>
            <ta e="T235" id="Seg_4893" s="T234">oh</ta>
            <ta e="T236" id="Seg_4894" s="T235">was.[NOM]</ta>
            <ta e="T237" id="Seg_4895" s="T236">Vogel-3SG-ACC</ta>
            <ta e="T238" id="Seg_4896" s="T237">machen-PRS-2SG</ta>
            <ta e="T239" id="Seg_4897" s="T238">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T240" id="Seg_4898" s="T239">Freund-3SG.[NOM]</ta>
            <ta e="T241" id="Seg_4899" s="T240">auch</ta>
            <ta e="T242" id="Seg_4900" s="T241">schauen.auf-PST2.[3SG]</ta>
            <ta e="T243" id="Seg_4901" s="T242">Fuß-3PL-GEN</ta>
            <ta e="T244" id="Seg_4902" s="T243">Unterteil-3PL-DAT/LOC</ta>
            <ta e="T245" id="Seg_4903" s="T244">Erde-3PL.[NOM]</ta>
            <ta e="T246" id="Seg_4904" s="T245">sich.bewegen-PTCP.PRS.[NOM]</ta>
            <ta e="T247" id="Seg_4905" s="T246">ähnlich</ta>
            <ta e="T248" id="Seg_4906" s="T247">werden-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_4907" s="T248">Stangenzelt-PL.[NOM]</ta>
            <ta e="T250" id="Seg_4908" s="T249">zittern-PST2-3PL</ta>
            <ta e="T251" id="Seg_4909" s="T250">schnell-ADVZ</ta>
            <ta e="T252" id="Seg_4910" s="T251">Schlitten.[NOM]</ta>
            <ta e="T253" id="Seg_4911" s="T252">zu</ta>
            <ta e="T254" id="Seg_4912" s="T253">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_4913" s="T254">sagen-CVB.SIM</ta>
            <ta e="T256" id="Seg_4914" s="T255">fallen-CVB.ANT</ta>
            <ta e="T257" id="Seg_4915" s="T256">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T258" id="Seg_4916" s="T257">INTJ</ta>
            <ta e="T259" id="Seg_4917" s="T258">machen-PST2.[3SG]</ta>
            <ta e="T260" id="Seg_4918" s="T259">kleiner.Nagel-PL-PROPR</ta>
            <ta e="T261" id="Seg_4919" s="T260">Boden.[NOM]</ta>
            <ta e="T262" id="Seg_4920" s="T261">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T263" id="Seg_4921" s="T262">Freund-3SG.[NOM]</ta>
            <ta e="T264" id="Seg_4922" s="T263">3SG-ACC</ta>
            <ta e="T265" id="Seg_4923" s="T264">wiederholen-CVB.SEQ</ta>
            <ta e="T266" id="Seg_4924" s="T265">hineingehen-PST2.[3SG]</ta>
            <ta e="T267" id="Seg_4925" s="T266">Balok-3SG-GEN</ta>
            <ta e="T268" id="Seg_4926" s="T267">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T269" id="Seg_4927" s="T268">jenes</ta>
            <ta e="T270" id="Seg_4928" s="T269">Zeit.[NOM]</ta>
            <ta e="T271" id="Seg_4929" s="T270">Flieger.[NOM]</ta>
            <ta e="T272" id="Seg_4930" s="T271">soeben</ta>
            <ta e="T273" id="Seg_4931" s="T272">Stange.[NOM]</ta>
            <ta e="T274" id="Seg_4932" s="T273">Zelt-PL-ACC</ta>
            <ta e="T275" id="Seg_4933" s="T274">sehen-CVB.SIM</ta>
            <ta e="T276" id="Seg_4934" s="T275">bald</ta>
            <ta e="T277" id="Seg_4935" s="T276">niedrig</ta>
            <ta e="T278" id="Seg_4936" s="T277">sehr</ta>
            <ta e="T279" id="Seg_4937" s="T278">fliegen-CVB.SEQ</ta>
            <ta e="T280" id="Seg_4938" s="T279">vorbeigehen-PST2.[3SG]</ta>
            <ta e="T281" id="Seg_4939" s="T280">Schlitten.[NOM]</ta>
            <ta e="T282" id="Seg_4940" s="T281">Haus-ACC</ta>
            <ta e="T283" id="Seg_4941" s="T282">mit</ta>
            <ta e="T284" id="Seg_4942" s="T283">Balok.[NOM]</ta>
            <ta e="T285" id="Seg_4943" s="T284">Unterteil-3PL-ABL</ta>
            <ta e="T288" id="Seg_4944" s="T287">Alte-PL.[NOM]</ta>
            <ta e="T289" id="Seg_4945" s="T288">dünne.Lederschicht.[NOM]</ta>
            <ta e="T290" id="Seg_4946" s="T289">Schuhe-3PL.[NOM]</ta>
            <ta e="T291" id="Seg_4947" s="T290">sich.bewegen-NEG.CVB.SIM</ta>
            <ta e="T292" id="Seg_4948" s="T291">herausziehen-CVB.SIM</ta>
            <ta e="T293" id="Seg_4949" s="T292">liegen-PST2-3PL</ta>
            <ta e="T294" id="Seg_4950" s="T293">Ufer-DAT/LOC</ta>
            <ta e="T295" id="Seg_4951" s="T294">alter.Mann-PL.[NOM]</ta>
            <ta e="T296" id="Seg_4952" s="T295">springen-CVB.SIM-springen-CVB.SIM</ta>
            <ta e="T297" id="Seg_4953" s="T296">Mütze-3PL-ACC</ta>
            <ta e="T298" id="Seg_4954" s="T297">Flugzeug-DAT/LOC</ta>
            <ta e="T299" id="Seg_4955" s="T298">schwenken-CVB.SIM</ta>
            <ta e="T300" id="Seg_4956" s="T299">stehen-PST2-3PL</ta>
            <ta e="T301" id="Seg_4957" s="T300">3PL.[NOM]</ta>
            <ta e="T302" id="Seg_4958" s="T301">sich.freuen-NMNZ-3PL.[NOM]</ta>
            <ta e="T303" id="Seg_4959" s="T302">erstmals</ta>
            <ta e="T304" id="Seg_4960" s="T303">Flugzeug.[NOM]</ta>
            <ta e="T305" id="Seg_4961" s="T304">kommen-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T306" id="Seg_4962" s="T305">Tundra-DAT/LOC</ta>
            <ta e="T307" id="Seg_4963" s="T306">gut</ta>
            <ta e="T308" id="Seg_4964" s="T307">sein-PST2.[3SG]</ta>
            <ta e="T309" id="Seg_4965" s="T308">wissen-PRS-3PL</ta>
            <ta e="T312" id="Seg_4966" s="T311">wissen-RECP/COLL-PTCP.PRS</ta>
            <ta e="T313" id="Seg_4967" s="T312">sein-CVB.SIM</ta>
            <ta e="T314" id="Seg_4968" s="T313">gehen-FUT-3SG</ta>
            <ta e="T315" id="Seg_4969" s="T314">wie</ta>
            <ta e="T316" id="Seg_4970" s="T315">Rentier-AG-PL.[NOM]</ta>
            <ta e="T317" id="Seg_4971" s="T316">Fisch-AG-PL.[NOM]</ta>
            <ta e="T318" id="Seg_4972" s="T317">leben-PTCP.PRS-3PL-ACC</ta>
            <ta e="T319" id="Seg_4973" s="T318">sehen-CVB.PURP</ta>
            <ta e="T320" id="Seg_4974" s="T319">Berg.[NOM]</ta>
            <ta e="T321" id="Seg_4975" s="T320">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T322" id="Seg_4976" s="T321">hinausgehen-CVB.ANT</ta>
            <ta e="T323" id="Seg_4977" s="T322">sich.wundern-EP-MED-PST2-3PL</ta>
            <ta e="T324" id="Seg_4978" s="T323">Stange.[NOM]</ta>
            <ta e="T325" id="Seg_4979" s="T324">Zelt-3PL.[NOM]</ta>
            <ta e="T326" id="Seg_4980" s="T325">rauchen-NEG-3PL</ta>
            <ta e="T327" id="Seg_4981" s="T326">Hof.[NOM]</ta>
            <ta e="T328" id="Seg_4982" s="T327">oberer.Teil-3SG-INSTR</ta>
            <ta e="T331" id="Seg_4983" s="T330">Stille.[NOM]</ta>
            <ta e="T332" id="Seg_4984" s="T331">abklingen-ADVZ</ta>
            <ta e="T333" id="Seg_4985" s="T332">Hund.[NOM]</ta>
            <ta e="T334" id="Seg_4986" s="T333">NEG</ta>
            <ta e="T335" id="Seg_4987" s="T334">bellen-NEG.[3SG]</ta>
            <ta e="T336" id="Seg_4988" s="T335">Mensch.[NOM]</ta>
            <ta e="T337" id="Seg_4989" s="T336">NEG</ta>
            <ta e="T338" id="Seg_4990" s="T337">Stimme-3SG.[NOM]</ta>
            <ta e="T339" id="Seg_4991" s="T338">gehört.werden-EP-NEG.[3SG]</ta>
            <ta e="T340" id="Seg_4992" s="T339">was</ta>
            <ta e="T341" id="Seg_4993" s="T340">Volk-PL.[NOM]</ta>
            <ta e="T342" id="Seg_4994" s="T341">dieses</ta>
            <ta e="T343" id="Seg_4995" s="T342">Alte-PL.[NOM]</ta>
            <ta e="T344" id="Seg_4996" s="T343">wissen-NEG-3PL</ta>
            <ta e="T345" id="Seg_4997" s="T344">Q</ta>
            <ta e="T346" id="Seg_4998" s="T345">1PL.[NOM]</ta>
            <ta e="T347" id="Seg_4999" s="T346">kommen-PTCP.FUT-1PL-ACC</ta>
            <ta e="T348" id="Seg_5000" s="T347">sagen-PST2.[3SG]</ta>
            <ta e="T349" id="Seg_5001" s="T348">Onoo.[NOM]</ta>
            <ta e="T350" id="Seg_5002" s="T349">alter.Mann.[NOM]</ta>
            <ta e="T351" id="Seg_5003" s="T350">wo</ta>
            <ta e="T352" id="Seg_5004" s="T351">es.gibt-2PL=Q</ta>
            <ta e="T353" id="Seg_5005" s="T352">Haus-PROPR-PL.[NOM]</ta>
            <ta e="T356" id="Seg_5006" s="T355">schreien-PST2.[3SG]</ta>
            <ta e="T357" id="Seg_5007" s="T356">Bewohner.[NOM]</ta>
            <ta e="T358" id="Seg_5008" s="T357">Mensch-3SG.[NOM]</ta>
            <ta e="T359" id="Seg_5009" s="T358">oh.nein</ta>
            <ta e="T360" id="Seg_5010" s="T359">zu.Hilfe.kommen-IMP.2PL</ta>
            <ta e="T361" id="Seg_5011" s="T360">trennen-EP-IMP.2PL</ta>
            <ta e="T362" id="Seg_5012" s="T361">sterben-PST1-1PL</ta>
            <ta e="T363" id="Seg_5013" s="T362">Erde.[NOM]</ta>
            <ta e="T364" id="Seg_5014" s="T363">Inneres-3SG-ABL</ta>
            <ta e="T365" id="Seg_5015" s="T364">Schrei.[NOM]</ta>
            <ta e="T366" id="Seg_5016" s="T365">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T367" id="Seg_5017" s="T366">was-PL.[NOM]=Q</ta>
            <ta e="T368" id="Seg_5018" s="T367">verrückt.werden-PST2-3PL</ta>
            <ta e="T369" id="Seg_5019" s="T368">Q</ta>
            <ta e="T370" id="Seg_5020" s="T369">sagen-CVB.SIM</ta>
            <ta e="T371" id="Seg_5021" s="T370">fallen-CVB.ANT</ta>
            <ta e="T372" id="Seg_5022" s="T371">Onoo.[NOM]</ta>
            <ta e="T373" id="Seg_5023" s="T372">schauen.auf-PST2.[3SG]</ta>
            <ta e="T374" id="Seg_5024" s="T373">3SG.[NOM]</ta>
            <ta e="T375" id="Seg_5025" s="T374">Schlitten.[NOM]</ta>
            <ta e="T376" id="Seg_5026" s="T375">Haus-PL.[NOM]</ta>
            <ta e="T377" id="Seg_5027" s="T376">Unterteil-3PL-DAT/LOC</ta>
            <ta e="T378" id="Seg_5028" s="T377">schauen.auf-PST2.[3SG]</ta>
            <ta e="T379" id="Seg_5029" s="T378">was.[NOM]</ta>
            <ta e="T380" id="Seg_5030" s="T379">Wunder-3SG.[NOM]</ta>
            <ta e="T381" id="Seg_5031" s="T380">sein-PST1-3SG</ta>
            <ta e="T382" id="Seg_5032" s="T381">Alte-3PL-GEN</ta>
            <ta e="T383" id="Seg_5033" s="T382">Bein-3PL.[NOM]</ta>
            <ta e="T384" id="Seg_5034" s="T383">dort</ta>
            <ta e="T385" id="Seg_5035" s="T384">zittern-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T386" id="Seg_5036" s="T385">liegen-PST2-3PL</ta>
            <ta e="T387" id="Seg_5037" s="T386">sehen-TEMP-3SG</ta>
            <ta e="T388" id="Seg_5038" s="T387">hinausgehen-CVB.SIM</ta>
            <ta e="T389" id="Seg_5039" s="T388">versuchen-PRS-3PL</ta>
            <ta e="T390" id="Seg_5040" s="T389">offenbar</ta>
            <ta e="T391" id="Seg_5041" s="T390">kleiner.Nagel-PL.[NOM]</ta>
            <ta e="T392" id="Seg_5042" s="T391">Kleidung-3PL-ABL</ta>
            <ta e="T393" id="Seg_5043" s="T392">festmachen-PST2-3PL</ta>
            <ta e="T394" id="Seg_5044" s="T393">warum</ta>
            <ta e="T395" id="Seg_5045" s="T394">lassen-FUT-3PL=Q</ta>
            <ta e="T396" id="Seg_5046" s="T395">Inneres-DAT/LOC</ta>
            <ta e="T397" id="Seg_5047" s="T396">NEG</ta>
            <ta e="T398" id="Seg_5048" s="T397">hineingehen-CAUS-NEG-3PL</ta>
            <ta e="T399" id="Seg_5049" s="T398">äußerer-DAT/LOC</ta>
            <ta e="T400" id="Seg_5050" s="T399">NEG</ta>
            <ta e="T401" id="Seg_5051" s="T400">herausnehmen-NEG-3PL</ta>
            <ta e="T402" id="Seg_5052" s="T401">oh.nein</ta>
            <ta e="T403" id="Seg_5053" s="T402">sagen-EP-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T404" id="Seg_5054" s="T403">mehr.werden-CVB.SEQ</ta>
            <ta e="T405" id="Seg_5055" s="T404">gehen-PST2.[3SG]</ta>
            <ta e="T406" id="Seg_5056" s="T405">stöhnen-PTCP.PRS.[NOM]</ta>
            <ta e="T407" id="Seg_5057" s="T406">hinzukommen-EP-PST2.[3SG]</ta>
            <ta e="T408" id="Seg_5058" s="T407">alter.Mann-PL.[NOM]</ta>
            <ta e="T409" id="Seg_5059" s="T408">was.[NOM]</ta>
            <ta e="T410" id="Seg_5060" s="T409">NEG</ta>
            <ta e="T411" id="Seg_5061" s="T410">zu</ta>
            <ta e="T412" id="Seg_5062" s="T411">Kraft-3SG-ABL</ta>
            <ta e="T413" id="Seg_5063" s="T412">sich.wundern-CVB.SIM</ta>
            <ta e="T414" id="Seg_5064" s="T413">nicht.schaffen-CVB.SEQ</ta>
            <ta e="T415" id="Seg_5065" s="T414">was-ACC</ta>
            <ta e="T416" id="Seg_5066" s="T415">NEG</ta>
            <ta e="T417" id="Seg_5067" s="T416">sprechen-NEG.PTCP</ta>
            <ta e="T418" id="Seg_5068" s="T417">sein-PST2-3PL</ta>
            <ta e="T419" id="Seg_5069" s="T418">Alte-PL-ACC</ta>
            <ta e="T420" id="Seg_5070" s="T419">eins-DISTR</ta>
            <ta e="T421" id="Seg_5071" s="T420">befreien-PST2-3PL</ta>
            <ta e="T422" id="Seg_5072" s="T421">3PL-DAT/LOC</ta>
            <ta e="T423" id="Seg_5073" s="T422">Gesicht.[NOM]</ta>
            <ta e="T424" id="Seg_5074" s="T423">kommen-FUT.[3SG]</ta>
            <ta e="T425" id="Seg_5075" s="T424">MOD</ta>
            <ta e="T426" id="Seg_5076" s="T425">Haar-3PL.[NOM]</ta>
            <ta e="T427" id="Seg_5077" s="T426">Auge-3PL-ACC</ta>
            <ta e="T428" id="Seg_5078" s="T427">bedecken-PST2.[3SG]</ta>
            <ta e="T429" id="Seg_5079" s="T428">dünne.Lederschicht.[NOM]</ta>
            <ta e="T430" id="Seg_5080" s="T429">Schuhe-3PL.[NOM]</ta>
            <ta e="T431" id="Seg_5081" s="T430">gestaucht.werden-ADVZ</ta>
            <ta e="T432" id="Seg_5082" s="T431">fallen-PST2-3PL</ta>
            <ta e="T433" id="Seg_5083" s="T432">Gestalt-3PL.[NOM]</ta>
            <ta e="T434" id="Seg_5084" s="T433">sich.ändern-PST2.[3SG]</ta>
            <ta e="T435" id="Seg_5085" s="T434">Boden-ABL</ta>
            <ta e="T436" id="Seg_5086" s="T435">hinausgehen-EP-PTCP.PST</ta>
            <ta e="T437" id="Seg_5087" s="T436">Polarfuchs.[NOM]</ta>
            <ta e="T438" id="Seg_5088" s="T437">Kind-PL-3SG-ACC</ta>
            <ta e="T439" id="Seg_5089" s="T438">ähnlich</ta>
            <ta e="T440" id="Seg_5090" s="T439">werden-PST2-3PL</ta>
            <ta e="T441" id="Seg_5091" s="T440">na</ta>
            <ta e="T442" id="Seg_5092" s="T441">was.[NOM]</ta>
            <ta e="T443" id="Seg_5093" s="T442">sein-PST2-2PL=Q</ta>
            <ta e="T444" id="Seg_5094" s="T443">sagen-CVB.SEQ</ta>
            <ta e="T445" id="Seg_5095" s="T444">fragen-PST2-3PL</ta>
            <ta e="T446" id="Seg_5096" s="T445">eins-DISTR</ta>
            <ta e="T447" id="Seg_5097" s="T446">Augenblick.[NOM]</ta>
            <ta e="T448" id="Seg_5098" s="T447">Ruhe</ta>
            <ta e="T449" id="Seg_5099" s="T448">wissen-NEG.PTCP.PST</ta>
            <ta e="T450" id="Seg_5100" s="T449">Vogel-1PL.[NOM]</ta>
            <ta e="T451" id="Seg_5101" s="T450">vorbeigehen-PST1-3SG</ta>
            <ta e="T452" id="Seg_5102" s="T451">sagen-CVB.SEQ</ta>
            <ta e="T453" id="Seg_5103" s="T452">flüstern-PST2-3PL</ta>
            <ta e="T454" id="Seg_5104" s="T453">zwei</ta>
            <ta e="T455" id="Seg_5105" s="T454">Alte.[NOM]</ta>
            <ta e="T456" id="Seg_5106" s="T455">oh</ta>
            <ta e="T457" id="Seg_5107" s="T456">wissen-NEG.PTCP.PST</ta>
            <ta e="T458" id="Seg_5108" s="T457">sein-CVB.SEQ</ta>
            <ta e="T459" id="Seg_5109" s="T458">Onoo.[NOM]</ta>
            <ta e="T460" id="Seg_5110" s="T459">zustimmen-PST2.[3SG]</ta>
            <ta e="T461" id="Seg_5111" s="T460">doch</ta>
            <ta e="T462" id="Seg_5112" s="T461">erzählen-IMP.2PL</ta>
            <ta e="T463" id="Seg_5113" s="T462">was.[NOM]</ta>
            <ta e="T464" id="Seg_5114" s="T463">Traum-3SG-ACC</ta>
            <ta e="T465" id="Seg_5115" s="T464">träumen-PST1-2PL</ta>
            <ta e="T466" id="Seg_5116" s="T465">Schlitten.[NOM]</ta>
            <ta e="T467" id="Seg_5117" s="T466">Haus.[NOM]</ta>
            <ta e="T468" id="Seg_5118" s="T467">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T469" id="Seg_5119" s="T468">liegen-CVB.SEQ-2PL</ta>
            <ta e="T470" id="Seg_5120" s="T469">dieses</ta>
            <ta e="T471" id="Seg_5121" s="T470">was.[NOM]</ta>
            <ta e="T472" id="Seg_5122" s="T471">sein-PRS.[3SG]</ta>
            <ta e="T473" id="Seg_5123" s="T472">alter.Mann.[NOM]=Q</ta>
            <ta e="T474" id="Seg_5124" s="T473">altern-CVB.SEQ</ta>
            <ta e="T475" id="Seg_5125" s="T474">Verstand-3SG.[NOM]</ta>
            <ta e="T476" id="Seg_5126" s="T475">kürzer.werden-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_5127" s="T476">offenbar</ta>
            <ta e="T478" id="Seg_5128" s="T477">schreien-PST2-3PL</ta>
            <ta e="T479" id="Seg_5129" s="T478">zwei</ta>
            <ta e="T480" id="Seg_5130" s="T479">ängstlich-PL.[NOM]</ta>
            <ta e="T481" id="Seg_5131" s="T480">doch</ta>
            <ta e="T482" id="Seg_5132" s="T481">Hase.[NOM]</ta>
            <ta e="T483" id="Seg_5133" s="T482">Herz-3PL.[NOM]</ta>
            <ta e="T484" id="Seg_5134" s="T483">noch</ta>
            <ta e="T485" id="Seg_5135" s="T484">Sprache-PROPR.[NOM]</ta>
            <ta e="T486" id="Seg_5136" s="T485">werden-PST2-3PL</ta>
            <ta e="T487" id="Seg_5137" s="T486">wissen-NEG-2PL</ta>
            <ta e="T488" id="Seg_5138" s="T487">Q</ta>
            <ta e="T489" id="Seg_5139" s="T488">Zeit.[NOM]</ta>
            <ta e="T490" id="Seg_5140" s="T489">sich.ändern-PST2-3SG-ACC</ta>
            <ta e="T491" id="Seg_5141" s="T490">Sowjet-PL.[NOM]</ta>
            <ta e="T492" id="Seg_5142" s="T491">Vögelchen-3PL.[NOM]</ta>
            <ta e="T493" id="Seg_5143" s="T492">kommen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T494" id="Seg_5144" s="T493">dieses-3SG-2PL.[NOM]</ta>
            <ta e="T495" id="Seg_5145" s="T494">2PL-ACC</ta>
            <ta e="T496" id="Seg_5146" s="T495">zu.Hilfe.kommen-CVB.SIM</ta>
            <ta e="T497" id="Seg_5147" s="T496">fliegen-CVB.SIM</ta>
            <ta e="T498" id="Seg_5148" s="T497">gehen-PRS.[3SG]</ta>
            <ta e="T499" id="Seg_5149" s="T498">dolganisch</ta>
            <ta e="T500" id="Seg_5150" s="T499">Volk-3SG.[NOM]</ta>
            <ta e="T501" id="Seg_5151" s="T500">satt.[NOM]</ta>
            <ta e="T502" id="Seg_5152" s="T501">reich.[NOM]</ta>
            <ta e="T503" id="Seg_5153" s="T502">leben-PTCP.FUT-PL-3SG-ACC</ta>
            <ta e="T504" id="Seg_5154" s="T503">sagen-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T505" id="Seg_5155" s="T504">zwei</ta>
            <ta e="T506" id="Seg_5156" s="T505">Mensch.[NOM]</ta>
            <ta e="T507" id="Seg_5157" s="T506">Alte-PL.[NOM]</ta>
            <ta e="T508" id="Seg_5158" s="T507">was.[NOM]</ta>
            <ta e="T509" id="Seg_5159" s="T508">und</ta>
            <ta e="T510" id="Seg_5160" s="T509">denken-PTCP.FUT-3PL-ACC</ta>
            <ta e="T511" id="Seg_5161" s="T510">Kraft-3SG-ABL</ta>
            <ta e="T512" id="Seg_5162" s="T511">Stimme-POSS</ta>
            <ta e="T513" id="Seg_5163" s="T512">NEG</ta>
            <ta e="T514" id="Seg_5164" s="T513">sich.schämen-PTCP.PST.[NOM]</ta>
            <ta e="T515" id="Seg_5165" s="T514">ähnlich</ta>
            <ta e="T516" id="Seg_5166" s="T515">Haus-3PL-ACC</ta>
            <ta e="T517" id="Seg_5167" s="T516">zu</ta>
            <ta e="T518" id="Seg_5168" s="T517">latschen-RECP/COLL-CVB.SIM</ta>
            <ta e="T519" id="Seg_5169" s="T518">stehen-PST2-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_5170" s="T0">совет-PL.[NOM]</ta>
            <ta e="T2" id="Seg_5171" s="T1">закон-3PL.[NOM]</ta>
            <ta e="T3" id="Seg_5172" s="T2">только.что</ta>
            <ta e="T4" id="Seg_5173" s="T3">приходить-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T5" id="Seg_5174" s="T4">Курья</ta>
            <ta e="T6" id="Seg_5175" s="T5">говорить-CVB.SEQ</ta>
            <ta e="T7" id="Seg_5176" s="T6">стойбище-DAT/LOC</ta>
            <ta e="T8" id="Seg_5177" s="T7">лето.[NOM]</ta>
            <ta e="T9" id="Seg_5178" s="T8">каждый</ta>
            <ta e="T10" id="Seg_5179" s="T9">рыбачить-CVB.SIM</ta>
            <ta e="T11" id="Seg_5180" s="T10">сидеть-PTCP.HAB</ta>
            <ta e="T12" id="Seg_5181" s="T11">быть-PST1-3PL</ta>
            <ta e="T13" id="Seg_5182" s="T12">долганский</ta>
            <ta e="T14" id="Seg_5183" s="T13">народ-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_5184" s="T14">поперек</ta>
            <ta e="T16" id="Seg_5185" s="T15">глаз-PROPR.[NOM]</ta>
            <ta e="T17" id="Seg_5186" s="T16">часто-часто</ta>
            <ta e="T18" id="Seg_5187" s="T17">идти-EP-NEG.PTCP</ta>
            <ta e="T19" id="Seg_5188" s="T18">стойбище-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_5189" s="T19">такой</ta>
            <ta e="T21" id="Seg_5190" s="T20">далекий.[NOM]</ta>
            <ta e="T22" id="Seg_5191" s="T21">рыбачить-PTCP.PRS</ta>
            <ta e="T23" id="Seg_5192" s="T22">место-3PL.[NOM]</ta>
            <ta e="T24" id="Seg_5193" s="T23">когда</ta>
            <ta e="T25" id="Seg_5194" s="T24">INDEF</ta>
            <ta e="T26" id="Seg_5195" s="T25">кто.[NOM]</ta>
            <ta e="T27" id="Seg_5196" s="T26">INDEF</ta>
            <ta e="T28" id="Seg_5197" s="T27">приходить-FUT.[3SG]</ta>
            <ta e="T29" id="Seg_5198" s="T28">летом</ta>
            <ta e="T30" id="Seg_5199" s="T29">близкий-ADJZ</ta>
            <ta e="T31" id="Seg_5200" s="T30">гниль-PL-DAT/LOC</ta>
            <ta e="T32" id="Seg_5201" s="T31">новость.[NOM]</ta>
            <ta e="T33" id="Seg_5202" s="T32">рассказывать-CVB.SIM</ta>
            <ta e="T34" id="Seg_5203" s="T33">сам-3SG-GEN</ta>
            <ta e="T35" id="Seg_5204" s="T34">жизнь-3SG-GEN</ta>
            <ta e="T36" id="Seg_5205" s="T35">сторона-3SG-INSTR</ta>
            <ta e="T37" id="Seg_5206" s="T36">узнавать-CVB.PURP</ta>
            <ta e="T38" id="Seg_5207" s="T37">как</ta>
            <ta e="T39" id="Seg_5208" s="T38">рыбачить-PTCP.PRS-3PL-ACC</ta>
            <ta e="T40" id="Seg_5209" s="T39">Курья.[NOM]</ta>
            <ta e="T41" id="Seg_5210" s="T40">народ-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_5211" s="T41">один</ta>
            <ta e="T43" id="Seg_5212" s="T42">лето.[NOM]</ta>
            <ta e="T44" id="Seg_5213" s="T43">старик-PL.[NOM]</ta>
            <ta e="T45" id="Seg_5214" s="T44">рано</ta>
            <ta e="T46" id="Seg_5215" s="T45">утром</ta>
            <ta e="T47" id="Seg_5216" s="T46">грести-EP-MED-CVB.SEQ</ta>
            <ta e="T48" id="Seg_5217" s="T47">оставаться-PST2-3PL</ta>
            <ta e="T49" id="Seg_5218" s="T48">проток-PL-3SG-ACC</ta>
            <ta e="T50" id="Seg_5219" s="T49">видеть-EP-MED-CVB.SIM</ta>
            <ta e="T51" id="Seg_5220" s="T50">жена.[NOM]</ta>
            <ta e="T52" id="Seg_5221" s="T51">ребенок.[NOM]</ta>
            <ta e="T53" id="Seg_5222" s="T52">родственник.[NOM]</ta>
            <ta e="T54" id="Seg_5223" s="T53">дом-3PL-DAT/LOC</ta>
            <ta e="T55" id="Seg_5224" s="T54">сидеть-PST2-3PL</ta>
            <ta e="T56" id="Seg_5225" s="T55">кто.[NOM]</ta>
            <ta e="T57" id="Seg_5226" s="T56">рыба.[NOM]</ta>
            <ta e="T58" id="Seg_5227" s="T57">очистить-EP-MED-PRS.[3SG]</ta>
            <ta e="T59" id="Seg_5228" s="T58">кто.[NOM]</ta>
            <ta e="T60" id="Seg_5229" s="T59">юкола.[NOM]</ta>
            <ta e="T61" id="Seg_5230" s="T60">сушить-EP-MED-PRS.[3SG]</ta>
            <ta e="T62" id="Seg_5231" s="T61">тот-PL-ABL</ta>
            <ta e="T63" id="Seg_5232" s="T62">два</ta>
            <ta e="T64" id="Seg_5233" s="T63">старуха.[NOM]</ta>
            <ta e="T65" id="Seg_5234" s="T64">старик-PL.[NOM]</ta>
            <ta e="T66" id="Seg_5235" s="T65">приходить-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T67" id="Seg_5236" s="T66">пока</ta>
            <ta e="T68" id="Seg_5237" s="T67">сани.[NOM]</ta>
            <ta e="T69" id="Seg_5238" s="T68">дом-3PL-GEN</ta>
            <ta e="T70" id="Seg_5239" s="T69">кожа-3PL-ACC</ta>
            <ta e="T71" id="Seg_5240" s="T70">латать-PRS-3PL</ta>
            <ta e="T72" id="Seg_5241" s="T71">быть-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_5242" s="T72">жаркий</ta>
            <ta e="T74" id="Seg_5243" s="T73">очень</ta>
            <ta e="T75" id="Seg_5244" s="T74">день.[NOM]</ta>
            <ta e="T76" id="Seg_5245" s="T75">стоять-PST2-3SG</ta>
            <ta e="T77" id="Seg_5246" s="T76">Дудыпта.[NOM]</ta>
            <ta e="T78" id="Seg_5247" s="T77">священный</ta>
            <ta e="T79" id="Seg_5248" s="T78">река-3PL-GEN</ta>
            <ta e="T80" id="Seg_5249" s="T79">вода-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_5250" s="T80">штилевой-3SG-DAT/LOC</ta>
            <ta e="T82" id="Seg_5251" s="T81">белый</ta>
            <ta e="T83" id="Seg_5252" s="T82">гром.[NOM]</ta>
            <ta e="T84" id="Seg_5253" s="T83">туча-PL-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_5254" s="T84">нанизать-CVB.SEQ</ta>
            <ta e="T86" id="Seg_5255" s="T85">быть.видно-PRS-3PL</ta>
            <ta e="T87" id="Seg_5256" s="T86">комар.[NOM]</ta>
            <ta e="T88" id="Seg_5257" s="T87">бедненький.[NOM]</ta>
            <ta e="T89" id="Seg_5258" s="T88">крыло-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_5259" s="T89">высыхать-PTCP.PRS</ta>
            <ta e="T91" id="Seg_5260" s="T90">жара-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_5261" s="T91">стоять-PST2-3SG</ta>
            <ta e="T93" id="Seg_5262" s="T92">старуха-PL-3SG.[NOM]</ta>
            <ta e="T94" id="Seg_5263" s="T93">тот</ta>
            <ta e="T95" id="Seg_5264" s="T94">да</ta>
            <ta e="T96" id="Seg_5265" s="T95">быть-COND.[3SG]</ta>
            <ta e="T97" id="Seg_5266" s="T96">рука-3PL.[NOM]</ta>
            <ta e="T98" id="Seg_5267" s="T97">досуг-POSS</ta>
            <ta e="T99" id="Seg_5268" s="T98">NEG</ta>
            <ta e="T100" id="Seg_5269" s="T99">отмахиваться-PRS-3PL</ta>
            <ta e="T101" id="Seg_5270" s="T100">тот.[NOM]</ta>
            <ta e="T102" id="Seg_5271" s="T101">промежуток-3SG-DAT/LOC</ta>
            <ta e="T103" id="Seg_5272" s="T102">прямо</ta>
            <ta e="T104" id="Seg_5273" s="T103">шить-MED-CVB.SIM</ta>
            <ta e="T105" id="Seg_5274" s="T104">быть-PST2-3PL</ta>
            <ta e="T106" id="Seg_5275" s="T105">пгла-3PL-GEN</ta>
            <ta e="T107" id="Seg_5276" s="T106">звук-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_5277" s="T107">цветной</ta>
            <ta e="T109" id="Seg_5278" s="T108">кожа-DAT/LOC</ta>
            <ta e="T110" id="Seg_5279" s="T109">трещать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T111" id="Seg_5280" s="T110">далекий-ABL</ta>
            <ta e="T112" id="Seg_5281" s="T111">слышаться-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_5282" s="T112">тот</ta>
            <ta e="T114" id="Seg_5283" s="T113">работа-VBZ-CVB.SIM</ta>
            <ta e="T115" id="Seg_5284" s="T114">сидеть-TEMP-3PL</ta>
            <ta e="T116" id="Seg_5285" s="T115">земля-3PL.[NOM]</ta>
            <ta e="T117" id="Seg_5286" s="T116">почему</ta>
            <ta e="T118" id="Seg_5287" s="T117">INDEF</ta>
            <ta e="T119" id="Seg_5288" s="T118">рычать-PTCP.PRS</ta>
            <ta e="T120" id="Seg_5289" s="T119">быть-PST2.[3SG]</ta>
            <ta e="T121" id="Seg_5290" s="T120">INTJ</ta>
            <ta e="T122" id="Seg_5291" s="T121">говорить-PST2.[3SG]</ta>
            <ta e="T123" id="Seg_5292" s="T122">один.из.двух-3SG.[NOM]</ta>
            <ta e="T124" id="Seg_5293" s="T123">почему</ta>
            <ta e="T125" id="Seg_5294" s="T124">земля-1PL.[NOM]</ta>
            <ta e="T126" id="Seg_5295" s="T125">рычать-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_5296" s="T126">почему</ta>
            <ta e="T128" id="Seg_5297" s="T127">гром.[NOM]</ta>
            <ta e="T129" id="Seg_5298" s="T128">звук-3SG.[NOM]</ta>
            <ta e="T130" id="Seg_5299" s="T129">такой.[NOM]</ta>
            <ta e="T131" id="Seg_5300" s="T130">быть-FUT.[3SG]=Q</ta>
            <ta e="T132" id="Seg_5301" s="T131">что.[NOM]</ta>
            <ta e="T133" id="Seg_5302" s="T132">INDEF</ta>
            <ta e="T134" id="Seg_5303" s="T133">чудо-3SG.[NOM]</ta>
            <ta e="T135" id="Seg_5304" s="T134">быть-PST1-3SG</ta>
            <ta e="T136" id="Seg_5305" s="T135">слушать.[IMP.2SG]</ta>
            <ta e="T137" id="Seg_5306" s="T136">старуха.[NOM]</ta>
            <ta e="T138" id="Seg_5307" s="T137">Кристина.[NOM]</ta>
            <ta e="T139" id="Seg_5308" s="T138">старый</ta>
            <ta e="T140" id="Seg_5309" s="T139">волос-3SG.[NOM]</ta>
            <ta e="T141" id="Seg_5310" s="T140">отнимать-PTCP.HAB</ta>
            <ta e="T142" id="Seg_5311" s="T141">белеть-PTCP.PST</ta>
            <ta e="T143" id="Seg_5312" s="T142">старуха.[NOM]</ta>
            <ta e="T144" id="Seg_5313" s="T143">рот-3SG-DAT/LOC</ta>
            <ta e="T145" id="Seg_5314" s="T144">один</ta>
            <ta e="T146" id="Seg_5315" s="T145">NEG</ta>
            <ta e="T147" id="Seg_5316" s="T146">зуб-POSS</ta>
            <ta e="T148" id="Seg_5317" s="T147">NEG.[3SG]</ta>
            <ta e="T149" id="Seg_5318" s="T148">слово.[NOM]</ta>
            <ta e="T150" id="Seg_5319" s="T149">входить-NEG.PTCP</ta>
            <ta e="T151" id="Seg_5320" s="T150">платок-3SG-ACC</ta>
            <ta e="T152" id="Seg_5321" s="T151">голова-3SG-ABL</ta>
            <ta e="T153" id="Seg_5322" s="T152">PFV</ta>
            <ta e="T154" id="Seg_5323" s="T153">тянуть-CVB.ANT</ta>
            <ta e="T155" id="Seg_5324" s="T154">слушать-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_5325" s="T155">тот.[NOM]</ta>
            <ta e="T157" id="Seg_5326" s="T520">два</ta>
            <ta e="T158" id="Seg_5327" s="T157">старуха.[NOM]</ta>
            <ta e="T159" id="Seg_5328" s="T158">небо.[NOM]</ta>
            <ta e="T160" id="Seg_5329" s="T159">к</ta>
            <ta e="T161" id="Seg_5330" s="T160">ползать-CVB.SEQ</ta>
            <ta e="T162" id="Seg_5331" s="T161">после</ta>
            <ta e="T163" id="Seg_5332" s="T162">гнать-PTCP.PST</ta>
            <ta e="T164" id="Seg_5333" s="T163">олень.[NOM]</ta>
            <ta e="T165" id="Seg_5334" s="T164">подобно</ta>
            <ta e="T166" id="Seg_5335" s="T165">рычать-PTCP.PRS</ta>
            <ta e="T167" id="Seg_5336" s="T166">шум-ACC</ta>
            <ta e="T168" id="Seg_5337" s="T167">слушать-CVB.SEQ</ta>
            <ta e="T170" id="Seg_5338" s="T168">становиться-PST2-3PL</ta>
            <ta e="T171" id="Seg_5339" s="T170">комар.[NOM]</ta>
            <ta e="T172" id="Seg_5340" s="T171">да</ta>
            <ta e="T173" id="Seg_5341" s="T172">есть-PTCP.PRS-3SG-ACC</ta>
            <ta e="T174" id="Seg_5342" s="T173">замечать-NEG.CVB.SIM</ta>
            <ta e="T175" id="Seg_5343" s="T174">долго-INTNS</ta>
            <ta e="T176" id="Seg_5344" s="T175">сидеть-PST2-3PL</ta>
            <ta e="T177" id="Seg_5345" s="T176">рот-3PL-ACC</ta>
            <ta e="T178" id="Seg_5346" s="T177">открывать-CVB.SEQ</ta>
            <ta e="T179" id="Seg_5347" s="T178">после</ta>
            <ta e="T180" id="Seg_5348" s="T179">место.около-3PL-DAT/LOC</ta>
            <ta e="T181" id="Seg_5349" s="T180">шкура-PL.[NOM]</ta>
            <ta e="T182" id="Seg_5350" s="T181">лоскуток-3PL.[NOM]</ta>
            <ta e="T183" id="Seg_5351" s="T182">там-здесь</ta>
            <ta e="T184" id="Seg_5352" s="T183">лежать-PRS-3PL</ta>
            <ta e="T185" id="Seg_5353" s="T184">веревка-INSTR</ta>
            <ta e="T186" id="Seg_5354" s="T185">связывать-NMNZ-PROPR</ta>
            <ta e="T187" id="Seg_5355" s="T186">курительная.трубка-3PL.[NOM]</ta>
            <ta e="T188" id="Seg_5356" s="T187">рука-3PL-DAT/LOC</ta>
            <ta e="T189" id="Seg_5357" s="T188">дымиться-PRS-3PL</ta>
            <ta e="T190" id="Seg_5358" s="T189">ждать-PTCP.PST.[NOM]</ta>
            <ta e="T191" id="Seg_5359" s="T190">подобно</ta>
            <ta e="T192" id="Seg_5360" s="T191">ONOM-ONOM</ta>
            <ta e="T193" id="Seg_5361" s="T192">когда</ta>
            <ta e="T194" id="Seg_5362" s="T193">сидеть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T195" id="Seg_5363" s="T194">правда.[NOM]</ta>
            <ta e="T196" id="Seg_5364" s="T195">3PL.[NOM]</ta>
            <ta e="T197" id="Seg_5365" s="T196">знать-NEG.PTCP.PST</ta>
            <ta e="T198" id="Seg_5366" s="T197">звук-3PL.[NOM]</ta>
            <ta e="T199" id="Seg_5367" s="T198">становиться-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_5368" s="T199">эй</ta>
            <ta e="T201" id="Seg_5369" s="T200">EMPH</ta>
            <ta e="T202" id="Seg_5370" s="T201">вот</ta>
            <ta e="T203" id="Seg_5371" s="T202">умирать-PST1-1PL</ta>
            <ta e="T204" id="Seg_5372" s="T203">говорить-CVB.SIM</ta>
            <ta e="T205" id="Seg_5373" s="T204">падать-CVB.ANT</ta>
            <ta e="T206" id="Seg_5374" s="T205">один.из.двух</ta>
            <ta e="T207" id="Seg_5375" s="T206">старуха.[NOM]</ta>
            <ta e="T208" id="Seg_5376" s="T207">вставать-CVB.SIM</ta>
            <ta e="T209" id="Seg_5377" s="T208">прыгать-PST2.[3SG]</ta>
            <ta e="T210" id="Seg_5378" s="T209">лес.[NOM]</ta>
            <ta e="T211" id="Seg_5379" s="T210">к</ta>
            <ta e="T212" id="Seg_5380" s="T211">заглянуть-PST2.[3SG]</ta>
            <ta e="T213" id="Seg_5381" s="T212">вот</ta>
            <ta e="T214" id="Seg_5382" s="T213">летать-CVB.SEQ</ta>
            <ta e="T215" id="Seg_5383" s="T214">идти-PRS.[3SG]</ta>
            <ta e="T216" id="Seg_5384" s="T215">большой</ta>
            <ta e="T217" id="Seg_5385" s="T216">птица-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_5386" s="T217">видеть.[IMP.2SG]</ta>
            <ta e="T219" id="Seg_5387" s="T218">тот</ta>
            <ta e="T220" id="Seg_5388" s="T219">краснеть-CVB.SEQ</ta>
            <ta e="T221" id="Seg_5389" s="T220">идти-PRS.[3SG]</ta>
            <ta e="T222" id="Seg_5390" s="T221">3SG.[NOM]</ta>
            <ta e="T223" id="Seg_5391" s="T222">спина-3SG-GEN</ta>
            <ta e="T224" id="Seg_5392" s="T223">звук-3SG.[NOM]</ta>
            <ta e="T225" id="Seg_5393" s="T224">быть-PST2.[3SG]</ta>
            <ta e="T226" id="Seg_5394" s="T225">господи</ta>
            <ta e="T227" id="Seg_5395" s="T226">1SG.[NOM]</ta>
            <ta e="T228" id="Seg_5396" s="T227">говорить-PST2.NEG-EP-1SG</ta>
            <ta e="T229" id="Seg_5397" s="T228">видеть-PST2.NEG-EP-1SG</ta>
            <ta e="T230" id="Seg_5398" s="T229">EMPH</ta>
            <ta e="T231" id="Seg_5399" s="T230">говорить-CVB.SEQ</ta>
            <ta e="T232" id="Seg_5400" s="T231">старуха.[NOM]</ta>
            <ta e="T233" id="Seg_5401" s="T232">шлёпать-CVB.SEQ</ta>
            <ta e="T234" id="Seg_5402" s="T233">идти-PST2.[3SG]</ta>
            <ta e="T235" id="Seg_5403" s="T234">о</ta>
            <ta e="T236" id="Seg_5404" s="T235">что.[NOM]</ta>
            <ta e="T237" id="Seg_5405" s="T236">птица-3SG-ACC</ta>
            <ta e="T238" id="Seg_5406" s="T237">делать-PRS-2SG</ta>
            <ta e="T239" id="Seg_5407" s="T238">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T240" id="Seg_5408" s="T239">друг-3SG.[NOM]</ta>
            <ta e="T241" id="Seg_5409" s="T240">тоже</ta>
            <ta e="T242" id="Seg_5410" s="T241">заглянуть-PST2.[3SG]</ta>
            <ta e="T243" id="Seg_5411" s="T242">нога-3PL-GEN</ta>
            <ta e="T244" id="Seg_5412" s="T243">нижняя.часть-3PL-DAT/LOC</ta>
            <ta e="T245" id="Seg_5413" s="T244">земля-3PL.[NOM]</ta>
            <ta e="T246" id="Seg_5414" s="T245">двигаться-PTCP.PRS.[NOM]</ta>
            <ta e="T247" id="Seg_5415" s="T246">подобно</ta>
            <ta e="T248" id="Seg_5416" s="T247">становиться-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_5417" s="T248">чум-PL.[NOM]</ta>
            <ta e="T250" id="Seg_5418" s="T249">дрожать-PST2-3PL</ta>
            <ta e="T251" id="Seg_5419" s="T250">быстрый-ADVZ</ta>
            <ta e="T252" id="Seg_5420" s="T251">сани.[NOM]</ta>
            <ta e="T253" id="Seg_5421" s="T252">к</ta>
            <ta e="T254" id="Seg_5422" s="T253">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T255" id="Seg_5423" s="T254">говорить-CVB.SIM</ta>
            <ta e="T256" id="Seg_5424" s="T255">падать-CVB.ANT</ta>
            <ta e="T257" id="Seg_5425" s="T256">один.из.двух-3SG.[NOM]</ta>
            <ta e="T258" id="Seg_5426" s="T257">INTJ</ta>
            <ta e="T259" id="Seg_5427" s="T258">делать-PST2.[3SG]</ta>
            <ta e="T260" id="Seg_5428" s="T259">маленький.гвоздик-PL-PROPR</ta>
            <ta e="T261" id="Seg_5429" s="T260">пол.[NOM]</ta>
            <ta e="T262" id="Seg_5430" s="T261">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T263" id="Seg_5431" s="T262">друг-3SG.[NOM]</ta>
            <ta e="T264" id="Seg_5432" s="T263">3SG-ACC</ta>
            <ta e="T265" id="Seg_5433" s="T264">повторять-CVB.SEQ</ta>
            <ta e="T266" id="Seg_5434" s="T265">входить-PST2.[3SG]</ta>
            <ta e="T267" id="Seg_5435" s="T266">балок-3SG-GEN</ta>
            <ta e="T268" id="Seg_5436" s="T267">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T269" id="Seg_5437" s="T268">тот</ta>
            <ta e="T270" id="Seg_5438" s="T269">час.[NOM]</ta>
            <ta e="T271" id="Seg_5439" s="T270">лётчик.[NOM]</ta>
            <ta e="T272" id="Seg_5440" s="T271">только.что</ta>
            <ta e="T273" id="Seg_5441" s="T272">шест.[NOM]</ta>
            <ta e="T274" id="Seg_5442" s="T273">чум-PL-ACC</ta>
            <ta e="T275" id="Seg_5443" s="T274">видеть-CVB.SIM</ta>
            <ta e="T276" id="Seg_5444" s="T275">скоро</ta>
            <ta e="T277" id="Seg_5445" s="T276">низкий</ta>
            <ta e="T278" id="Seg_5446" s="T277">очень</ta>
            <ta e="T279" id="Seg_5447" s="T278">летать-CVB.SEQ</ta>
            <ta e="T280" id="Seg_5448" s="T279">проехать-PST2.[3SG]</ta>
            <ta e="T281" id="Seg_5449" s="T280">сани.[NOM]</ta>
            <ta e="T282" id="Seg_5450" s="T281">дом-ACC</ta>
            <ta e="T283" id="Seg_5451" s="T282">с</ta>
            <ta e="T284" id="Seg_5452" s="T283">балок.[NOM]</ta>
            <ta e="T285" id="Seg_5453" s="T284">нижняя.часть-3PL-ABL</ta>
            <ta e="T288" id="Seg_5454" s="T287">старуха-PL.[NOM]</ta>
            <ta e="T289" id="Seg_5455" s="T288">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T290" id="Seg_5456" s="T289">обувь-3PL.[NOM]</ta>
            <ta e="T291" id="Seg_5457" s="T290">двигаться-NEG.CVB.SIM</ta>
            <ta e="T292" id="Seg_5458" s="T291">вытянуть-CVB.SIM</ta>
            <ta e="T293" id="Seg_5459" s="T292">лежать-PST2-3PL</ta>
            <ta e="T294" id="Seg_5460" s="T293">берег-DAT/LOC</ta>
            <ta e="T295" id="Seg_5461" s="T294">старик-PL.[NOM]</ta>
            <ta e="T296" id="Seg_5462" s="T295">прыгать-CVB.SIM-прыгать-CVB.SIM</ta>
            <ta e="T297" id="Seg_5463" s="T296">шапка-3PL-ACC</ta>
            <ta e="T298" id="Seg_5464" s="T297">самолет-DAT/LOC</ta>
            <ta e="T299" id="Seg_5465" s="T298">махнуть-CVB.SIM</ta>
            <ta e="T300" id="Seg_5466" s="T299">стоять-PST2-3PL</ta>
            <ta e="T301" id="Seg_5467" s="T300">3PL.[NOM]</ta>
            <ta e="T302" id="Seg_5468" s="T301">радоваться-NMNZ-3PL.[NOM]</ta>
            <ta e="T303" id="Seg_5469" s="T302">впервые</ta>
            <ta e="T304" id="Seg_5470" s="T303">самолет.[NOM]</ta>
            <ta e="T305" id="Seg_5471" s="T304">приходить-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T306" id="Seg_5472" s="T305">тундра-DAT/LOC</ta>
            <ta e="T307" id="Seg_5473" s="T306">хорошо</ta>
            <ta e="T308" id="Seg_5474" s="T307">быть-PST2.[3SG]</ta>
            <ta e="T309" id="Seg_5475" s="T308">знать-PRS-3PL</ta>
            <ta e="T312" id="Seg_5476" s="T311">знать-RECP/COLL-PTCP.PRS</ta>
            <ta e="T313" id="Seg_5477" s="T312">быть-CVB.SIM</ta>
            <ta e="T314" id="Seg_5478" s="T313">идти-FUT-3SG</ta>
            <ta e="T315" id="Seg_5479" s="T314">как</ta>
            <ta e="T316" id="Seg_5480" s="T315">олень-AG-PL.[NOM]</ta>
            <ta e="T317" id="Seg_5481" s="T316">рыба-AG-PL.[NOM]</ta>
            <ta e="T318" id="Seg_5482" s="T317">жить-PTCP.PRS-3PL-ACC</ta>
            <ta e="T319" id="Seg_5483" s="T318">видеть-CVB.PURP</ta>
            <ta e="T320" id="Seg_5484" s="T319">гора.[NOM]</ta>
            <ta e="T321" id="Seg_5485" s="T320">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T322" id="Seg_5486" s="T321">выйти-CVB.ANT</ta>
            <ta e="T323" id="Seg_5487" s="T322">удивляться-EP-MED-PST2-3PL</ta>
            <ta e="T324" id="Seg_5488" s="T323">шест.[NOM]</ta>
            <ta e="T325" id="Seg_5489" s="T324">чум-3PL.[NOM]</ta>
            <ta e="T326" id="Seg_5490" s="T325">дымиться-NEG-3PL</ta>
            <ta e="T327" id="Seg_5491" s="T326">двор.[NOM]</ta>
            <ta e="T328" id="Seg_5492" s="T327">верхняя.часть-3SG-INSTR</ta>
            <ta e="T331" id="Seg_5493" s="T330">тишина.[NOM]</ta>
            <ta e="T332" id="Seg_5494" s="T331">затихать-ADVZ</ta>
            <ta e="T333" id="Seg_5495" s="T332">собака.[NOM]</ta>
            <ta e="T334" id="Seg_5496" s="T333">NEG</ta>
            <ta e="T335" id="Seg_5497" s="T334">лаять-NEG.[3SG]</ta>
            <ta e="T336" id="Seg_5498" s="T335">человек.[NOM]</ta>
            <ta e="T337" id="Seg_5499" s="T336">NEG</ta>
            <ta e="T338" id="Seg_5500" s="T337">голос-3SG.[NOM]</ta>
            <ta e="T339" id="Seg_5501" s="T338">слышаться-EP-NEG.[3SG]</ta>
            <ta e="T340" id="Seg_5502" s="T339">что</ta>
            <ta e="T341" id="Seg_5503" s="T340">народ-PL.[NOM]</ta>
            <ta e="T342" id="Seg_5504" s="T341">этот</ta>
            <ta e="T343" id="Seg_5505" s="T342">старуха-PL.[NOM]</ta>
            <ta e="T344" id="Seg_5506" s="T343">знать-NEG-3PL</ta>
            <ta e="T345" id="Seg_5507" s="T344">Q</ta>
            <ta e="T346" id="Seg_5508" s="T345">1PL.[NOM]</ta>
            <ta e="T347" id="Seg_5509" s="T346">приходить-PTCP.FUT-1PL-ACC</ta>
            <ta e="T348" id="Seg_5510" s="T347">говорить-PST2.[3SG]</ta>
            <ta e="T349" id="Seg_5511" s="T348">Ануфрий.[NOM]</ta>
            <ta e="T350" id="Seg_5512" s="T349">старик.[NOM]</ta>
            <ta e="T351" id="Seg_5513" s="T350">где</ta>
            <ta e="T352" id="Seg_5514" s="T351">есть-2PL=Q</ta>
            <ta e="T353" id="Seg_5515" s="T352">дом-PROPR-PL.[NOM]</ta>
            <ta e="T356" id="Seg_5516" s="T355">кричать-PST2.[3SG]</ta>
            <ta e="T357" id="Seg_5517" s="T356">житель.[NOM]</ta>
            <ta e="T358" id="Seg_5518" s="T357">человек-3SG.[NOM]</ta>
            <ta e="T359" id="Seg_5519" s="T358">вот.беда</ta>
            <ta e="T360" id="Seg_5520" s="T359">выручать-IMP.2PL</ta>
            <ta e="T361" id="Seg_5521" s="T360">разделять-EP-IMP.2PL</ta>
            <ta e="T362" id="Seg_5522" s="T361">умирать-PST1-1PL</ta>
            <ta e="T363" id="Seg_5523" s="T362">земля.[NOM]</ta>
            <ta e="T364" id="Seg_5524" s="T363">нутро-3SG-ABL</ta>
            <ta e="T365" id="Seg_5525" s="T364">крик.[NOM]</ta>
            <ta e="T366" id="Seg_5526" s="T365">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T367" id="Seg_5527" s="T366">что-PL.[NOM]=Q</ta>
            <ta e="T368" id="Seg_5528" s="T367">сходить.с.ума-PST2-3PL</ta>
            <ta e="T369" id="Seg_5529" s="T368">Q</ta>
            <ta e="T370" id="Seg_5530" s="T369">говорить-CVB.SIM</ta>
            <ta e="T371" id="Seg_5531" s="T370">падать-CVB.ANT</ta>
            <ta e="T372" id="Seg_5532" s="T371">Ануфрий.[NOM]</ta>
            <ta e="T373" id="Seg_5533" s="T372">заглянуть-PST2.[3SG]</ta>
            <ta e="T374" id="Seg_5534" s="T373">3SG.[NOM]</ta>
            <ta e="T375" id="Seg_5535" s="T374">сани.[NOM]</ta>
            <ta e="T376" id="Seg_5536" s="T375">дом-PL.[NOM]</ta>
            <ta e="T377" id="Seg_5537" s="T376">нижняя.часть-3PL-DAT/LOC</ta>
            <ta e="T378" id="Seg_5538" s="T377">заглянуть-PST2.[3SG]</ta>
            <ta e="T379" id="Seg_5539" s="T378">что.[NOM]</ta>
            <ta e="T380" id="Seg_5540" s="T379">чудо-3SG.[NOM]</ta>
            <ta e="T381" id="Seg_5541" s="T380">быть-PST1-3SG</ta>
            <ta e="T382" id="Seg_5542" s="T381">старуха-3PL-GEN</ta>
            <ta e="T383" id="Seg_5543" s="T382">нога-3PL.[NOM]</ta>
            <ta e="T384" id="Seg_5544" s="T383">там</ta>
            <ta e="T385" id="Seg_5545" s="T384">дрожать-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T386" id="Seg_5546" s="T385">лежать-PST2-3PL</ta>
            <ta e="T387" id="Seg_5547" s="T386">видеть-TEMP-3SG</ta>
            <ta e="T388" id="Seg_5548" s="T387">выйти-CVB.SIM</ta>
            <ta e="T389" id="Seg_5549" s="T388">пытаться-PRS-3PL</ta>
            <ta e="T390" id="Seg_5550" s="T389">наверное</ta>
            <ta e="T391" id="Seg_5551" s="T390">маленький.гвоздик-PL.[NOM]</ta>
            <ta e="T392" id="Seg_5552" s="T391">одежда-3PL-ABL</ta>
            <ta e="T393" id="Seg_5553" s="T392">нацеплять-PST2-3PL</ta>
            <ta e="T394" id="Seg_5554" s="T393">почему</ta>
            <ta e="T395" id="Seg_5555" s="T394">пустить-FUT-3PL=Q</ta>
            <ta e="T396" id="Seg_5556" s="T395">нутро-DAT/LOC</ta>
            <ta e="T397" id="Seg_5557" s="T396">NEG</ta>
            <ta e="T398" id="Seg_5558" s="T397">входить-CAUS-NEG-3PL</ta>
            <ta e="T399" id="Seg_5559" s="T398">внешний-DAT/LOC</ta>
            <ta e="T400" id="Seg_5560" s="T399">NEG</ta>
            <ta e="T401" id="Seg_5561" s="T400">вынимать-NEG-3PL</ta>
            <ta e="T402" id="Seg_5562" s="T401">вот.беда</ta>
            <ta e="T403" id="Seg_5563" s="T402">говорить-EP-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T404" id="Seg_5564" s="T403">умножаться-CVB.SEQ</ta>
            <ta e="T405" id="Seg_5565" s="T404">идти-PST2.[3SG]</ta>
            <ta e="T406" id="Seg_5566" s="T405">бредить-PTCP.PRS.[NOM]</ta>
            <ta e="T407" id="Seg_5567" s="T406">прибавляься-EP-PST2.[3SG]</ta>
            <ta e="T408" id="Seg_5568" s="T407">старик-PL.[NOM]</ta>
            <ta e="T409" id="Seg_5569" s="T408">что.[NOM]</ta>
            <ta e="T410" id="Seg_5570" s="T409">NEG</ta>
            <ta e="T411" id="Seg_5571" s="T410">к</ta>
            <ta e="T412" id="Seg_5572" s="T411">сила-3SG-ABL</ta>
            <ta e="T413" id="Seg_5573" s="T412">удивляться-CVB.SIM</ta>
            <ta e="T414" id="Seg_5574" s="T413">не.справиться-CVB.SEQ</ta>
            <ta e="T415" id="Seg_5575" s="T414">что-ACC</ta>
            <ta e="T416" id="Seg_5576" s="T415">NEG</ta>
            <ta e="T417" id="Seg_5577" s="T416">говорить-NEG.PTCP</ta>
            <ta e="T418" id="Seg_5578" s="T417">быть-PST2-3PL</ta>
            <ta e="T419" id="Seg_5579" s="T418">старуха-PL-ACC</ta>
            <ta e="T420" id="Seg_5580" s="T419">один-DISTR</ta>
            <ta e="T421" id="Seg_5581" s="T420">освободить-PST2-3PL</ta>
            <ta e="T422" id="Seg_5582" s="T421">3PL-DAT/LOC</ta>
            <ta e="T423" id="Seg_5583" s="T422">лицо.[NOM]</ta>
            <ta e="T424" id="Seg_5584" s="T423">приходить-FUT.[3SG]</ta>
            <ta e="T425" id="Seg_5585" s="T424">MOD</ta>
            <ta e="T426" id="Seg_5586" s="T425">волос-3PL.[NOM]</ta>
            <ta e="T427" id="Seg_5587" s="T426">глаз-3PL-ACC</ta>
            <ta e="T428" id="Seg_5588" s="T427">покрывать-PST2.[3SG]</ta>
            <ta e="T429" id="Seg_5589" s="T428">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T430" id="Seg_5590" s="T429">обувь-3PL.[NOM]</ta>
            <ta e="T431" id="Seg_5591" s="T430">сдавливаться-ADVZ</ta>
            <ta e="T432" id="Seg_5592" s="T431">падать-PST2-3PL</ta>
            <ta e="T433" id="Seg_5593" s="T432">образ-3PL.[NOM]</ta>
            <ta e="T434" id="Seg_5594" s="T433">измениться-PST2.[3SG]</ta>
            <ta e="T435" id="Seg_5595" s="T434">земля-ABL</ta>
            <ta e="T436" id="Seg_5596" s="T435">выйти-EP-PTCP.PST</ta>
            <ta e="T437" id="Seg_5597" s="T436">песец.[NOM]</ta>
            <ta e="T438" id="Seg_5598" s="T437">ребенок-PL-3SG-ACC</ta>
            <ta e="T439" id="Seg_5599" s="T438">подобно</ta>
            <ta e="T440" id="Seg_5600" s="T439">становиться-PST2-3PL</ta>
            <ta e="T441" id="Seg_5601" s="T440">эй</ta>
            <ta e="T442" id="Seg_5602" s="T441">что.[NOM]</ta>
            <ta e="T443" id="Seg_5603" s="T442">быть-PST2-2PL=Q</ta>
            <ta e="T444" id="Seg_5604" s="T443">говорить-CVB.SEQ</ta>
            <ta e="T445" id="Seg_5605" s="T444">спрашивать-PST2-3PL</ta>
            <ta e="T446" id="Seg_5606" s="T445">один-DISTR</ta>
            <ta e="T447" id="Seg_5607" s="T446">миг.[NOM]</ta>
            <ta e="T448" id="Seg_5608" s="T447">тихо</ta>
            <ta e="T449" id="Seg_5609" s="T448">знать-NEG.PTCP.PST</ta>
            <ta e="T450" id="Seg_5610" s="T449">птица-1PL.[NOM]</ta>
            <ta e="T451" id="Seg_5611" s="T450">проехать-PST1-3SG</ta>
            <ta e="T452" id="Seg_5612" s="T451">говорить-CVB.SEQ</ta>
            <ta e="T453" id="Seg_5613" s="T452">шептать-PST2-3PL</ta>
            <ta e="T454" id="Seg_5614" s="T453">два</ta>
            <ta e="T455" id="Seg_5615" s="T454">старуха.[NOM]</ta>
            <ta e="T456" id="Seg_5616" s="T455">о</ta>
            <ta e="T457" id="Seg_5617" s="T456">знать-NEG.PTCP.PST</ta>
            <ta e="T458" id="Seg_5618" s="T457">быть-CVB.SEQ</ta>
            <ta e="T459" id="Seg_5619" s="T458">Ануфрий.[NOM]</ta>
            <ta e="T460" id="Seg_5620" s="T459">согласиться-PST2.[3SG]</ta>
            <ta e="T461" id="Seg_5621" s="T460">вот</ta>
            <ta e="T462" id="Seg_5622" s="T461">рассказывать-IMP.2PL</ta>
            <ta e="T463" id="Seg_5623" s="T462">что.[NOM]</ta>
            <ta e="T464" id="Seg_5624" s="T463">сон-3SG-ACC</ta>
            <ta e="T465" id="Seg_5625" s="T464">сниться-PST1-2PL</ta>
            <ta e="T466" id="Seg_5626" s="T465">сани.[NOM]</ta>
            <ta e="T467" id="Seg_5627" s="T466">дом.[NOM]</ta>
            <ta e="T468" id="Seg_5628" s="T467">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T469" id="Seg_5629" s="T468">лежать-CVB.SEQ-2PL</ta>
            <ta e="T470" id="Seg_5630" s="T469">этот</ta>
            <ta e="T471" id="Seg_5631" s="T470">что.[NOM]</ta>
            <ta e="T472" id="Seg_5632" s="T471">быть-PRS.[3SG]</ta>
            <ta e="T473" id="Seg_5633" s="T472">старик.[NOM]=Q</ta>
            <ta e="T474" id="Seg_5634" s="T473">постареть-CVB.SEQ</ta>
            <ta e="T475" id="Seg_5635" s="T474">ум-3SG.[NOM]</ta>
            <ta e="T476" id="Seg_5636" s="T475">сокращаться-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_5637" s="T476">наверное</ta>
            <ta e="T478" id="Seg_5638" s="T477">прокричать-PST2-3PL</ta>
            <ta e="T479" id="Seg_5639" s="T478">два</ta>
            <ta e="T480" id="Seg_5640" s="T479">боязливый-PL.[NOM]</ta>
            <ta e="T481" id="Seg_5641" s="T480">ведь</ta>
            <ta e="T482" id="Seg_5642" s="T481">заяц.[NOM]</ta>
            <ta e="T483" id="Seg_5643" s="T482">сердце-3PL.[NOM]</ta>
            <ta e="T484" id="Seg_5644" s="T483">еще</ta>
            <ta e="T485" id="Seg_5645" s="T484">язык-PROPR.[NOM]</ta>
            <ta e="T486" id="Seg_5646" s="T485">становиться-PST2-3PL</ta>
            <ta e="T487" id="Seg_5647" s="T486">знать-NEG-2PL</ta>
            <ta e="T488" id="Seg_5648" s="T487">Q</ta>
            <ta e="T489" id="Seg_5649" s="T488">время.[NOM]</ta>
            <ta e="T490" id="Seg_5650" s="T489">измениться-PST2-3SG-ACC</ta>
            <ta e="T491" id="Seg_5651" s="T490">совет-PL.[NOM]</ta>
            <ta e="T492" id="Seg_5652" s="T491">птичка-3PL.[NOM]</ta>
            <ta e="T493" id="Seg_5653" s="T492">приходить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T494" id="Seg_5654" s="T493">тот-3SG-2PL.[NOM]</ta>
            <ta e="T495" id="Seg_5655" s="T494">2PL-ACC</ta>
            <ta e="T496" id="Seg_5656" s="T495">выручать-CVB.SIM</ta>
            <ta e="T497" id="Seg_5657" s="T496">летать-CVB.SIM</ta>
            <ta e="T498" id="Seg_5658" s="T497">идти-PRS.[3SG]</ta>
            <ta e="T499" id="Seg_5659" s="T498">долганский</ta>
            <ta e="T500" id="Seg_5660" s="T499">народ-3SG.[NOM]</ta>
            <ta e="T501" id="Seg_5661" s="T500">сытый.[NOM]</ta>
            <ta e="T502" id="Seg_5662" s="T501">богатый.[NOM]</ta>
            <ta e="T503" id="Seg_5663" s="T502">жить-PTCP.FUT-PL-3SG-ACC</ta>
            <ta e="T504" id="Seg_5664" s="T503">говорить-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T505" id="Seg_5665" s="T504">два</ta>
            <ta e="T506" id="Seg_5666" s="T505">человек.[NOM]</ta>
            <ta e="T507" id="Seg_5667" s="T506">старуха-PL.[NOM]</ta>
            <ta e="T508" id="Seg_5668" s="T507">что.[NOM]</ta>
            <ta e="T509" id="Seg_5669" s="T508">да</ta>
            <ta e="T510" id="Seg_5670" s="T509">думать-PTCP.FUT-3PL-ACC</ta>
            <ta e="T511" id="Seg_5671" s="T510">сила-3SG-ABL</ta>
            <ta e="T512" id="Seg_5672" s="T511">голос-POSS</ta>
            <ta e="T513" id="Seg_5673" s="T512">NEG</ta>
            <ta e="T514" id="Seg_5674" s="T513">стесняться-PTCP.PST.[NOM]</ta>
            <ta e="T515" id="Seg_5675" s="T514">подобно</ta>
            <ta e="T516" id="Seg_5676" s="T515">дом-3PL-ACC</ta>
            <ta e="T517" id="Seg_5677" s="T516">к</ta>
            <ta e="T518" id="Seg_5678" s="T517">сутулиться-RECP/COLL-CVB.SIM</ta>
            <ta e="T519" id="Seg_5679" s="T518">стоять-PST2-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_5680" s="T0">n-n:(num)-n:case</ta>
            <ta e="T2" id="Seg_5681" s="T1">n-n:(poss)-n:case</ta>
            <ta e="T3" id="Seg_5682" s="T2">adv</ta>
            <ta e="T4" id="Seg_5683" s="T3">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T5" id="Seg_5684" s="T4">propr</ta>
            <ta e="T6" id="Seg_5685" s="T5">v-v:cvb</ta>
            <ta e="T7" id="Seg_5686" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_5687" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_5688" s="T8">adj</ta>
            <ta e="T10" id="Seg_5689" s="T9">v-v:cvb</ta>
            <ta e="T11" id="Seg_5690" s="T10">v-v:ptcp</ta>
            <ta e="T12" id="Seg_5691" s="T11">v-v:tense-v:poss.pn</ta>
            <ta e="T13" id="Seg_5692" s="T12">adj</ta>
            <ta e="T14" id="Seg_5693" s="T13">n-n:(poss)-n:case</ta>
            <ta e="T15" id="Seg_5694" s="T14">post</ta>
            <ta e="T16" id="Seg_5695" s="T15">n-n&gt;adj-n:case</ta>
            <ta e="T17" id="Seg_5696" s="T16">adv-adv</ta>
            <ta e="T18" id="Seg_5697" s="T17">v-v:(ins)-v:ptcp</ta>
            <ta e="T19" id="Seg_5698" s="T18">n-n:(poss)-n:case</ta>
            <ta e="T20" id="Seg_5699" s="T19">dempro</ta>
            <ta e="T21" id="Seg_5700" s="T20">adj-n:case</ta>
            <ta e="T22" id="Seg_5701" s="T21">v-v:ptcp</ta>
            <ta e="T23" id="Seg_5702" s="T22">n-n:(poss)-n:case</ta>
            <ta e="T24" id="Seg_5703" s="T23">que</ta>
            <ta e="T25" id="Seg_5704" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_5705" s="T25">que-pro:case</ta>
            <ta e="T27" id="Seg_5706" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_5707" s="T27">v-v:tense-v:poss.pn</ta>
            <ta e="T29" id="Seg_5708" s="T28">adv</ta>
            <ta e="T30" id="Seg_5709" s="T29">adj-adj&gt;adj</ta>
            <ta e="T31" id="Seg_5710" s="T30">n-n:(num)-n:case</ta>
            <ta e="T32" id="Seg_5711" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_5712" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_5713" s="T33">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T35" id="Seg_5714" s="T34">n-n:poss-n:case</ta>
            <ta e="T36" id="Seg_5715" s="T35">n-n:poss-n:case</ta>
            <ta e="T37" id="Seg_5716" s="T36">v-v:cvb</ta>
            <ta e="T38" id="Seg_5717" s="T37">que</ta>
            <ta e="T39" id="Seg_5718" s="T38">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T40" id="Seg_5719" s="T39">propr-n:case</ta>
            <ta e="T41" id="Seg_5720" s="T40">n-n:(poss)-n:case</ta>
            <ta e="T42" id="Seg_5721" s="T41">cardnum</ta>
            <ta e="T43" id="Seg_5722" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_5723" s="T43">n-n:(num)-n:case</ta>
            <ta e="T45" id="Seg_5724" s="T44">adv</ta>
            <ta e="T46" id="Seg_5725" s="T45">adv</ta>
            <ta e="T47" id="Seg_5726" s="T46">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T48" id="Seg_5727" s="T47">v-v:tense-v:poss.pn</ta>
            <ta e="T49" id="Seg_5728" s="T48">n-n:(num)-n:poss-n:case</ta>
            <ta e="T50" id="Seg_5729" s="T49">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T51" id="Seg_5730" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_5731" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_5732" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_5733" s="T53">n-n:poss-n:case</ta>
            <ta e="T55" id="Seg_5734" s="T54">v-v:tense-v:poss.pn</ta>
            <ta e="T56" id="Seg_5735" s="T55">que-pro:case</ta>
            <ta e="T57" id="Seg_5736" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_5737" s="T57">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_5738" s="T58">que-pro:case</ta>
            <ta e="T60" id="Seg_5739" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_5740" s="T60">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_5741" s="T61">dempro-pro:(num)-pro:case</ta>
            <ta e="T63" id="Seg_5742" s="T62">cardnum</ta>
            <ta e="T64" id="Seg_5743" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_5744" s="T64">n-n:(num)-n:case</ta>
            <ta e="T66" id="Seg_5745" s="T65">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T67" id="Seg_5746" s="T66">post</ta>
            <ta e="T68" id="Seg_5747" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_5748" s="T68">n-n:poss-n:case</ta>
            <ta e="T70" id="Seg_5749" s="T69">n-n:poss-n:case</ta>
            <ta e="T71" id="Seg_5750" s="T70">v-v:tense-v:pred.pn</ta>
            <ta e="T72" id="Seg_5751" s="T71">v-v:tense-v:pred.pn</ta>
            <ta e="T73" id="Seg_5752" s="T72">adj</ta>
            <ta e="T74" id="Seg_5753" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_5754" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_5755" s="T75">v-v:tense-v:poss.pn</ta>
            <ta e="T77" id="Seg_5756" s="T76">propr-n:case</ta>
            <ta e="T78" id="Seg_5757" s="T77">adj</ta>
            <ta e="T79" id="Seg_5758" s="T78">n-n:poss-n:case</ta>
            <ta e="T80" id="Seg_5759" s="T79">n-n:(poss)-n:case</ta>
            <ta e="T81" id="Seg_5760" s="T80">adj-n:poss-n:case</ta>
            <ta e="T82" id="Seg_5761" s="T81">adj</ta>
            <ta e="T83" id="Seg_5762" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_5763" s="T83">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T85" id="Seg_5764" s="T84">v-v:cvb</ta>
            <ta e="T86" id="Seg_5765" s="T85">v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_5766" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_5767" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_5768" s="T88">n-n:(poss)-n:case</ta>
            <ta e="T90" id="Seg_5769" s="T89">v-v:ptcp</ta>
            <ta e="T91" id="Seg_5770" s="T90">n-n:(poss)-n:case</ta>
            <ta e="T92" id="Seg_5771" s="T91">v-v:tense-v:poss.pn</ta>
            <ta e="T93" id="Seg_5772" s="T92">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T94" id="Seg_5773" s="T93">dempro</ta>
            <ta e="T95" id="Seg_5774" s="T94">conj</ta>
            <ta e="T96" id="Seg_5775" s="T95">v-v:mood-v:pred.pn</ta>
            <ta e="T97" id="Seg_5776" s="T96">n-n:(poss)-n:case</ta>
            <ta e="T98" id="Seg_5777" s="T97">n-n:(poss)</ta>
            <ta e="T99" id="Seg_5778" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_5779" s="T99">v-v:tense-v:pred.pn</ta>
            <ta e="T101" id="Seg_5780" s="T100">dempro-pro:case</ta>
            <ta e="T102" id="Seg_5781" s="T101">n-n:poss-n:case</ta>
            <ta e="T103" id="Seg_5782" s="T102">adv</ta>
            <ta e="T104" id="Seg_5783" s="T103">v-v&gt;v-v:cvb</ta>
            <ta e="T105" id="Seg_5784" s="T104">v-v:tense-v:poss.pn</ta>
            <ta e="T106" id="Seg_5785" s="T105">n-n:poss-n:case</ta>
            <ta e="T107" id="Seg_5786" s="T106">n-n:(poss)-n:case</ta>
            <ta e="T108" id="Seg_5787" s="T107">adj</ta>
            <ta e="T109" id="Seg_5788" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_5789" s="T109">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T111" id="Seg_5790" s="T110">adj-n:case</ta>
            <ta e="T112" id="Seg_5791" s="T111">v-v:tense-v:pred.pn</ta>
            <ta e="T113" id="Seg_5792" s="T112">dempro</ta>
            <ta e="T114" id="Seg_5793" s="T113">n-n&gt;v-v:cvb</ta>
            <ta e="T115" id="Seg_5794" s="T114">v-v:mood-v:temp.pn</ta>
            <ta e="T116" id="Seg_5795" s="T115">n-n:(poss)-n:case</ta>
            <ta e="T117" id="Seg_5796" s="T116">que</ta>
            <ta e="T118" id="Seg_5797" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_5798" s="T118">v-v:ptcp</ta>
            <ta e="T120" id="Seg_5799" s="T119">v-v:tense-v:pred.pn</ta>
            <ta e="T121" id="Seg_5800" s="T120">interj</ta>
            <ta e="T122" id="Seg_5801" s="T121">v-v:tense-v:pred.pn</ta>
            <ta e="T123" id="Seg_5802" s="T122">adj-n:(poss)-n:case</ta>
            <ta e="T124" id="Seg_5803" s="T123">que</ta>
            <ta e="T125" id="Seg_5804" s="T124">n-n:(poss)-n:case</ta>
            <ta e="T126" id="Seg_5805" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_5806" s="T126">que</ta>
            <ta e="T128" id="Seg_5807" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_5808" s="T128">n-n:(poss)-n:case</ta>
            <ta e="T130" id="Seg_5809" s="T129">dempro-pro:case</ta>
            <ta e="T131" id="Seg_5810" s="T130">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T132" id="Seg_5811" s="T131">que-pro:case</ta>
            <ta e="T133" id="Seg_5812" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_5813" s="T133">n-n:(poss)-n:case</ta>
            <ta e="T135" id="Seg_5814" s="T134">v-v:tense-v:poss.pn</ta>
            <ta e="T136" id="Seg_5815" s="T135">v-v:mood.pn</ta>
            <ta e="T137" id="Seg_5816" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_5817" s="T137">propr-n:case</ta>
            <ta e="T139" id="Seg_5818" s="T138">adj</ta>
            <ta e="T140" id="Seg_5819" s="T139">n-n:(poss)-n:case</ta>
            <ta e="T141" id="Seg_5820" s="T140">v-v:ptcp</ta>
            <ta e="T142" id="Seg_5821" s="T141">v-v:ptcp</ta>
            <ta e="T143" id="Seg_5822" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_5823" s="T143">n-n:poss-n:case</ta>
            <ta e="T145" id="Seg_5824" s="T144">cardnum</ta>
            <ta e="T146" id="Seg_5825" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_5826" s="T146">n-n:(poss)</ta>
            <ta e="T148" id="Seg_5827" s="T147">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T149" id="Seg_5828" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_5829" s="T149">v-v:ptcp</ta>
            <ta e="T151" id="Seg_5830" s="T150">n-n:poss-n:case</ta>
            <ta e="T152" id="Seg_5831" s="T151">n-n:poss-n:case</ta>
            <ta e="T153" id="Seg_5832" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_5833" s="T153">v-v:cvb</ta>
            <ta e="T155" id="Seg_5834" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_5835" s="T155">dempro-n:case</ta>
            <ta e="T157" id="Seg_5836" s="T520">cardnum</ta>
            <ta e="T158" id="Seg_5837" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_5838" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_5839" s="T159">post</ta>
            <ta e="T161" id="Seg_5840" s="T160">v-v:cvb</ta>
            <ta e="T162" id="Seg_5841" s="T161">post</ta>
            <ta e="T163" id="Seg_5842" s="T162">v-v:ptcp</ta>
            <ta e="T164" id="Seg_5843" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_5844" s="T164">post</ta>
            <ta e="T166" id="Seg_5845" s="T165">v-v:ptcp</ta>
            <ta e="T167" id="Seg_5846" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_5847" s="T167">v-v:cvb</ta>
            <ta e="T170" id="Seg_5848" s="T168">v-v:tense-v:pred.pn</ta>
            <ta e="T171" id="Seg_5849" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_5850" s="T171">conj</ta>
            <ta e="T173" id="Seg_5851" s="T172">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T174" id="Seg_5852" s="T173">v-v:cvb</ta>
            <ta e="T175" id="Seg_5853" s="T174">adv-adv&gt;adv</ta>
            <ta e="T176" id="Seg_5854" s="T175">v-v:tense-v:pred.pn</ta>
            <ta e="T177" id="Seg_5855" s="T176">n-n:poss-n:case</ta>
            <ta e="T178" id="Seg_5856" s="T177">v-v:cvb</ta>
            <ta e="T179" id="Seg_5857" s="T178">post</ta>
            <ta e="T180" id="Seg_5858" s="T179">n-n:poss-n:case</ta>
            <ta e="T181" id="Seg_5859" s="T180">n-n:(num)-n:case</ta>
            <ta e="T182" id="Seg_5860" s="T181">n-n:(poss)-n:case</ta>
            <ta e="T183" id="Seg_5861" s="T182">adv-adv</ta>
            <ta e="T184" id="Seg_5862" s="T183">v-v:tense-v:pred.pn</ta>
            <ta e="T185" id="Seg_5863" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_5864" s="T185">v-v&gt;n-n&gt;adj</ta>
            <ta e="T187" id="Seg_5865" s="T186">n-n:(poss)-n:case</ta>
            <ta e="T188" id="Seg_5866" s="T187">n-n:poss-n:case</ta>
            <ta e="T189" id="Seg_5867" s="T188">v-v:tense-v:pred.pn</ta>
            <ta e="T190" id="Seg_5868" s="T189">v-v:ptcp-v:(case)</ta>
            <ta e="T191" id="Seg_5869" s="T190">post</ta>
            <ta e="T192" id="Seg_5870" s="T191">ptcl-ptcl</ta>
            <ta e="T193" id="Seg_5871" s="T192">que</ta>
            <ta e="T194" id="Seg_5872" s="T193">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T195" id="Seg_5873" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_5874" s="T195">pers-pro:case</ta>
            <ta e="T197" id="Seg_5875" s="T196">v-v:ptcp</ta>
            <ta e="T198" id="Seg_5876" s="T197">n-n:(poss)-n:case</ta>
            <ta e="T199" id="Seg_5877" s="T198">v-v:tense-v:pred.pn</ta>
            <ta e="T200" id="Seg_5878" s="T199">interj</ta>
            <ta e="T201" id="Seg_5879" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_5880" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_5881" s="T202">v-v:tense-v:poss.pn</ta>
            <ta e="T204" id="Seg_5882" s="T203">v-v:cvb</ta>
            <ta e="T205" id="Seg_5883" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_5884" s="T205">adj</ta>
            <ta e="T207" id="Seg_5885" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_5886" s="T207">v-v:cvb</ta>
            <ta e="T209" id="Seg_5887" s="T208">v-v:tense-v:pred.pn</ta>
            <ta e="T210" id="Seg_5888" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_5889" s="T210">post</ta>
            <ta e="T212" id="Seg_5890" s="T211">v-v:tense-v:pred.pn</ta>
            <ta e="T213" id="Seg_5891" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_5892" s="T213">v-v:cvb</ta>
            <ta e="T215" id="Seg_5893" s="T214">v-v:tense-v:pred.pn</ta>
            <ta e="T216" id="Seg_5894" s="T215">adj</ta>
            <ta e="T217" id="Seg_5895" s="T216">n-n:(poss)-n:case</ta>
            <ta e="T218" id="Seg_5896" s="T217">v-v:mood.pn</ta>
            <ta e="T219" id="Seg_5897" s="T218">dempro</ta>
            <ta e="T220" id="Seg_5898" s="T219">v-v:cvb</ta>
            <ta e="T221" id="Seg_5899" s="T220">v-v:tense-v:pred.pn</ta>
            <ta e="T222" id="Seg_5900" s="T221">pers-pro:case</ta>
            <ta e="T223" id="Seg_5901" s="T222">n-n:poss-n:case</ta>
            <ta e="T224" id="Seg_5902" s="T223">n-n:(poss)-n:case</ta>
            <ta e="T225" id="Seg_5903" s="T224">v-v:tense-v:pred.pn</ta>
            <ta e="T226" id="Seg_5904" s="T225">interj</ta>
            <ta e="T227" id="Seg_5905" s="T226">pers-pro:case</ta>
            <ta e="T228" id="Seg_5906" s="T227">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T229" id="Seg_5907" s="T228">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T230" id="Seg_5908" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_5909" s="T230">v-v:cvb</ta>
            <ta e="T232" id="Seg_5910" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_5911" s="T232">v-v:cvb</ta>
            <ta e="T234" id="Seg_5912" s="T233">v-v:tense-v:pred.pn</ta>
            <ta e="T235" id="Seg_5913" s="T234">interj</ta>
            <ta e="T236" id="Seg_5914" s="T235">que-pro:case</ta>
            <ta e="T237" id="Seg_5915" s="T236">n-n:poss-n:case</ta>
            <ta e="T238" id="Seg_5916" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_5917" s="T238">v-v:cvb-v-v:cvb</ta>
            <ta e="T240" id="Seg_5918" s="T239">n-n:(poss)-n:case</ta>
            <ta e="T241" id="Seg_5919" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_5920" s="T241">v-v:tense-v:pred.pn</ta>
            <ta e="T243" id="Seg_5921" s="T242">n-n:poss-n:case</ta>
            <ta e="T244" id="Seg_5922" s="T243">n-n:poss-n:case</ta>
            <ta e="T245" id="Seg_5923" s="T244">n-n:(poss)-n:case</ta>
            <ta e="T246" id="Seg_5924" s="T245">v-v:ptcp-v:(case)</ta>
            <ta e="T247" id="Seg_5925" s="T246">post</ta>
            <ta e="T248" id="Seg_5926" s="T247">v-v:tense-v:pred.pn</ta>
            <ta e="T249" id="Seg_5927" s="T248">n-n:(num)-n:case</ta>
            <ta e="T250" id="Seg_5928" s="T249">v-v:tense-v:pred.pn</ta>
            <ta e="T251" id="Seg_5929" s="T250">adj-adj&gt;adv</ta>
            <ta e="T252" id="Seg_5930" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_5931" s="T252">post</ta>
            <ta e="T254" id="Seg_5932" s="T253">n-n:poss-n:case</ta>
            <ta e="T255" id="Seg_5933" s="T254">v-v:cvb</ta>
            <ta e="T256" id="Seg_5934" s="T255">v-v:cvb</ta>
            <ta e="T257" id="Seg_5935" s="T256">adj-n:(poss)-n:case</ta>
            <ta e="T258" id="Seg_5936" s="T257">interj</ta>
            <ta e="T259" id="Seg_5937" s="T258">v-v:tense-v:pred.pn</ta>
            <ta e="T260" id="Seg_5938" s="T259">n-n:(num)-n&gt;adj</ta>
            <ta e="T261" id="Seg_5939" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_5940" s="T261">n-n:poss-n:case</ta>
            <ta e="T263" id="Seg_5941" s="T262">n-n:(poss)-n:case</ta>
            <ta e="T264" id="Seg_5942" s="T263">pers-pro:case</ta>
            <ta e="T265" id="Seg_5943" s="T264">v-v:cvb</ta>
            <ta e="T266" id="Seg_5944" s="T265">v-v:tense-v:pred.pn</ta>
            <ta e="T267" id="Seg_5945" s="T266">n-n:poss-n:case</ta>
            <ta e="T268" id="Seg_5946" s="T267">n-n:poss-n:case</ta>
            <ta e="T269" id="Seg_5947" s="T268">dempro</ta>
            <ta e="T270" id="Seg_5948" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_5949" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_5950" s="T271">adv</ta>
            <ta e="T273" id="Seg_5951" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_5952" s="T273">n-n:(num)-n:case</ta>
            <ta e="T275" id="Seg_5953" s="T274">v-v:cvb</ta>
            <ta e="T276" id="Seg_5954" s="T275">adv</ta>
            <ta e="T277" id="Seg_5955" s="T276">adj</ta>
            <ta e="T278" id="Seg_5956" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_5957" s="T278">v-v:cvb</ta>
            <ta e="T280" id="Seg_5958" s="T279">v-v:tense-v:pred.pn</ta>
            <ta e="T281" id="Seg_5959" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_5960" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_5961" s="T282">post</ta>
            <ta e="T284" id="Seg_5962" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_5963" s="T284">n-n:poss-n:case</ta>
            <ta e="T288" id="Seg_5964" s="T287">n-n:(num)-n:case</ta>
            <ta e="T289" id="Seg_5965" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_5966" s="T289">n-n:(poss)-n:case</ta>
            <ta e="T291" id="Seg_5967" s="T290">v-v:cvb</ta>
            <ta e="T292" id="Seg_5968" s="T291">v-v:cvb</ta>
            <ta e="T293" id="Seg_5969" s="T292">v-v:tense-v:pred.pn</ta>
            <ta e="T294" id="Seg_5970" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_5971" s="T294">n-n:(num)-n:case</ta>
            <ta e="T296" id="Seg_5972" s="T295">v-v:cvb-v-v:cvb</ta>
            <ta e="T297" id="Seg_5973" s="T296">n-n:poss-n:case</ta>
            <ta e="T298" id="Seg_5974" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_5975" s="T298">v-v:cvb</ta>
            <ta e="T300" id="Seg_5976" s="T299">v-v:tense-v:pred.pn</ta>
            <ta e="T301" id="Seg_5977" s="T300">pers-pro:case</ta>
            <ta e="T302" id="Seg_5978" s="T301">v-v&gt;n-n:(poss)-n:case</ta>
            <ta e="T303" id="Seg_5979" s="T302">adv</ta>
            <ta e="T304" id="Seg_5980" s="T303">n-n:case</ta>
            <ta e="T305" id="Seg_5981" s="T304">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T306" id="Seg_5982" s="T305">n-n:case</ta>
            <ta e="T307" id="Seg_5983" s="T306">adv</ta>
            <ta e="T308" id="Seg_5984" s="T307">v-v:tense-v:pred.pn</ta>
            <ta e="T309" id="Seg_5985" s="T308">v-v:tense-v:pred.pn</ta>
            <ta e="T312" id="Seg_5986" s="T311">v-v&gt;v-v:ptcp</ta>
            <ta e="T313" id="Seg_5987" s="T312">v-v:cvb</ta>
            <ta e="T314" id="Seg_5988" s="T313">v-v:tense-v:poss.pn</ta>
            <ta e="T315" id="Seg_5989" s="T314">que</ta>
            <ta e="T316" id="Seg_5990" s="T315">n-n&gt;n-n:(num)-n:case</ta>
            <ta e="T317" id="Seg_5991" s="T316">n-n&gt;n-n:(num)-n:case</ta>
            <ta e="T318" id="Seg_5992" s="T317">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T319" id="Seg_5993" s="T318">v-v:cvb</ta>
            <ta e="T320" id="Seg_5994" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_5995" s="T320">n-n:poss-n:case</ta>
            <ta e="T322" id="Seg_5996" s="T321">v-v:cvb</ta>
            <ta e="T323" id="Seg_5997" s="T322">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T324" id="Seg_5998" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_5999" s="T324">n-n:(poss)-n:case</ta>
            <ta e="T326" id="Seg_6000" s="T325">v-v:(neg)-v:pred.pn</ta>
            <ta e="T327" id="Seg_6001" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_6002" s="T327">n-n:poss-n:case</ta>
            <ta e="T331" id="Seg_6003" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_6004" s="T331">v-v&gt;adv</ta>
            <ta e="T333" id="Seg_6005" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_6006" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_6007" s="T334">v-v:(neg)-v:pred.pn</ta>
            <ta e="T336" id="Seg_6008" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_6009" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_6010" s="T337">n-n:(poss)-n:case</ta>
            <ta e="T339" id="Seg_6011" s="T338">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T340" id="Seg_6012" s="T339">que</ta>
            <ta e="T341" id="Seg_6013" s="T340">n-n:(num)-n:case</ta>
            <ta e="T342" id="Seg_6014" s="T341">dempro</ta>
            <ta e="T343" id="Seg_6015" s="T342">n-n:(num)-n:case</ta>
            <ta e="T344" id="Seg_6016" s="T343">v-v:(neg)-v:pred.pn</ta>
            <ta e="T345" id="Seg_6017" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_6018" s="T345">pers-pro:case</ta>
            <ta e="T347" id="Seg_6019" s="T346">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T348" id="Seg_6020" s="T347">v-v:tense-v:pred.pn</ta>
            <ta e="T349" id="Seg_6021" s="T348">propr-n:case</ta>
            <ta e="T350" id="Seg_6022" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_6023" s="T350">que</ta>
            <ta e="T352" id="Seg_6024" s="T351">ptcl-ptcl:(pred.pn)-ptcl</ta>
            <ta e="T353" id="Seg_6025" s="T352">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T356" id="Seg_6026" s="T355">v-v:tense-v:pred.pn</ta>
            <ta e="T357" id="Seg_6027" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_6028" s="T357">n-n:(poss)-n:case</ta>
            <ta e="T359" id="Seg_6029" s="T358">interj</ta>
            <ta e="T360" id="Seg_6030" s="T359">v-v:mood.pn</ta>
            <ta e="T361" id="Seg_6031" s="T360">v-v:(ins)-v:mood.pn</ta>
            <ta e="T362" id="Seg_6032" s="T361">v-v:tense-v:poss.pn</ta>
            <ta e="T363" id="Seg_6033" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_6034" s="T363">n-n:poss-n:case</ta>
            <ta e="T365" id="Seg_6035" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_6036" s="T365">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T367" id="Seg_6037" s="T366">que-pro:(num)-pro:case-ptcl</ta>
            <ta e="T368" id="Seg_6038" s="T367">v-v:tense-v:pred.pn</ta>
            <ta e="T369" id="Seg_6039" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_6040" s="T369">v-v:cvb</ta>
            <ta e="T371" id="Seg_6041" s="T370">v-v:cvb</ta>
            <ta e="T372" id="Seg_6042" s="T371">propr-n:case</ta>
            <ta e="T373" id="Seg_6043" s="T372">v-v:tense-v:pred.pn</ta>
            <ta e="T374" id="Seg_6044" s="T373">pers-pro:case</ta>
            <ta e="T375" id="Seg_6045" s="T374">n-n:case</ta>
            <ta e="T376" id="Seg_6046" s="T375">n-n:(num)-n:case</ta>
            <ta e="T377" id="Seg_6047" s="T376">n-n:poss-n:case</ta>
            <ta e="T378" id="Seg_6048" s="T377">v-v:tense-v:pred.pn</ta>
            <ta e="T379" id="Seg_6049" s="T378">que-pro:case</ta>
            <ta e="T380" id="Seg_6050" s="T379">n-n:(poss)-n:case</ta>
            <ta e="T381" id="Seg_6051" s="T380">v-v:tense-v:poss.pn</ta>
            <ta e="T382" id="Seg_6052" s="T381">n-n:poss-n:case</ta>
            <ta e="T383" id="Seg_6053" s="T382">n-n:(poss)-n:case</ta>
            <ta e="T384" id="Seg_6054" s="T383">adv</ta>
            <ta e="T385" id="Seg_6055" s="T384">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T386" id="Seg_6056" s="T385">v-v:tense-v:pred.pn</ta>
            <ta e="T387" id="Seg_6057" s="T386">v-v:mood-v:temp.pn</ta>
            <ta e="T388" id="Seg_6058" s="T387">v-v:cvb</ta>
            <ta e="T389" id="Seg_6059" s="T388">v-v:tense-v:pred.pn</ta>
            <ta e="T390" id="Seg_6060" s="T389">adv</ta>
            <ta e="T391" id="Seg_6061" s="T390">n-n:(num)-n:case</ta>
            <ta e="T392" id="Seg_6062" s="T391">n-n:poss-n:case</ta>
            <ta e="T393" id="Seg_6063" s="T392">v-v:tense-v:pred.pn</ta>
            <ta e="T394" id="Seg_6064" s="T393">que</ta>
            <ta e="T395" id="Seg_6065" s="T394">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T396" id="Seg_6066" s="T395">n-n:case</ta>
            <ta e="T397" id="Seg_6067" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_6068" s="T397">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T399" id="Seg_6069" s="T398">adj-n:case</ta>
            <ta e="T400" id="Seg_6070" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_6071" s="T400">v-v:(neg)-v:pred.pn</ta>
            <ta e="T402" id="Seg_6072" s="T401">interj</ta>
            <ta e="T403" id="Seg_6073" s="T402">v-v:(ins)-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T404" id="Seg_6074" s="T403">v-v:cvb</ta>
            <ta e="T405" id="Seg_6075" s="T404">v-v:tense-v:pred.pn</ta>
            <ta e="T406" id="Seg_6076" s="T405">v-v:ptcp-v:(case)</ta>
            <ta e="T407" id="Seg_6077" s="T406">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T408" id="Seg_6078" s="T407">n-n:(num)-n:case</ta>
            <ta e="T409" id="Seg_6079" s="T408">que-pro:case</ta>
            <ta e="T410" id="Seg_6080" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_6081" s="T410">post</ta>
            <ta e="T412" id="Seg_6082" s="T411">n-n:poss-n:case</ta>
            <ta e="T413" id="Seg_6083" s="T412">v-v:cvb</ta>
            <ta e="T414" id="Seg_6084" s="T413">v-v:cvb</ta>
            <ta e="T415" id="Seg_6085" s="T414">que-pro:case</ta>
            <ta e="T416" id="Seg_6086" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_6087" s="T416">v-v:ptcp</ta>
            <ta e="T418" id="Seg_6088" s="T417">v-v:tense-v:pred.pn</ta>
            <ta e="T419" id="Seg_6089" s="T418">n-n:(num)-n:case</ta>
            <ta e="T420" id="Seg_6090" s="T419">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T421" id="Seg_6091" s="T420">v-v:tense-v:pred.pn</ta>
            <ta e="T422" id="Seg_6092" s="T421">pers-pro:case</ta>
            <ta e="T423" id="Seg_6093" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_6094" s="T423">v-v:tense-v:poss.pn</ta>
            <ta e="T425" id="Seg_6095" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_6096" s="T425">n-n:(poss)-n:case</ta>
            <ta e="T427" id="Seg_6097" s="T426">n-n:poss-n:case</ta>
            <ta e="T428" id="Seg_6098" s="T427">v-v:tense-v:pred.pn</ta>
            <ta e="T429" id="Seg_6099" s="T428">n-n:case</ta>
            <ta e="T430" id="Seg_6100" s="T429">n-n:(poss)-n:case</ta>
            <ta e="T431" id="Seg_6101" s="T430">v-v&gt;adv</ta>
            <ta e="T432" id="Seg_6102" s="T431">v-v:tense-v:pred.pn</ta>
            <ta e="T433" id="Seg_6103" s="T432">n-n:(poss)-n:case</ta>
            <ta e="T434" id="Seg_6104" s="T433">v-v:tense-v:pred.pn</ta>
            <ta e="T435" id="Seg_6105" s="T434">n-n:case</ta>
            <ta e="T436" id="Seg_6106" s="T435">v-v:(ins)-v:ptcp</ta>
            <ta e="T437" id="Seg_6107" s="T436">n-n:case</ta>
            <ta e="T438" id="Seg_6108" s="T437">n-n:(num)-n:poss-n:case</ta>
            <ta e="T439" id="Seg_6109" s="T438">post</ta>
            <ta e="T440" id="Seg_6110" s="T439">v-v:tense-v:pred.pn</ta>
            <ta e="T441" id="Seg_6111" s="T440">interj</ta>
            <ta e="T442" id="Seg_6112" s="T441">que-pro:case</ta>
            <ta e="T443" id="Seg_6113" s="T442">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T444" id="Seg_6114" s="T443">v-v:cvb</ta>
            <ta e="T445" id="Seg_6115" s="T444">v-v:tense-v:pred.pn</ta>
            <ta e="T446" id="Seg_6116" s="T445">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T447" id="Seg_6117" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_6118" s="T447">interj</ta>
            <ta e="T449" id="Seg_6119" s="T448">v-v:ptcp</ta>
            <ta e="T450" id="Seg_6120" s="T449">n-n:(poss)-n:case</ta>
            <ta e="T451" id="Seg_6121" s="T450">v-v:tense-v:poss.pn</ta>
            <ta e="T452" id="Seg_6122" s="T451">v-v:cvb</ta>
            <ta e="T453" id="Seg_6123" s="T452">v-v:tense-v:pred.pn</ta>
            <ta e="T454" id="Seg_6124" s="T453">cardnum</ta>
            <ta e="T455" id="Seg_6125" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_6126" s="T455">interj</ta>
            <ta e="T457" id="Seg_6127" s="T456">v-v:ptcp</ta>
            <ta e="T458" id="Seg_6128" s="T457">v-v:cvb</ta>
            <ta e="T459" id="Seg_6129" s="T458">propr-n:case</ta>
            <ta e="T460" id="Seg_6130" s="T459">v-v:tense-v:pred.pn</ta>
            <ta e="T461" id="Seg_6131" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_6132" s="T461">v-v:mood.pn</ta>
            <ta e="T463" id="Seg_6133" s="T462">que-pro:case</ta>
            <ta e="T464" id="Seg_6134" s="T463">n-n:poss-n:case</ta>
            <ta e="T465" id="Seg_6135" s="T464">v-v:tense-v:poss.pn</ta>
            <ta e="T466" id="Seg_6136" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_6137" s="T466">n-n:case</ta>
            <ta e="T468" id="Seg_6138" s="T467">n-n:poss-n:case</ta>
            <ta e="T469" id="Seg_6139" s="T468">v-v:cvb-v:pred.pn</ta>
            <ta e="T470" id="Seg_6140" s="T469">dempro</ta>
            <ta e="T471" id="Seg_6141" s="T470">que-pro:case</ta>
            <ta e="T472" id="Seg_6142" s="T471">v-v:tense-v:pred.pn</ta>
            <ta e="T473" id="Seg_6143" s="T472">n-n:case-ptcl</ta>
            <ta e="T474" id="Seg_6144" s="T473">v-v:cvb</ta>
            <ta e="T475" id="Seg_6145" s="T474">n-n:(poss)-n:case</ta>
            <ta e="T476" id="Seg_6146" s="T475">v-v:tense-v:pred.pn</ta>
            <ta e="T477" id="Seg_6147" s="T476">adv</ta>
            <ta e="T478" id="Seg_6148" s="T477">v-v:tense-v:pred.pn</ta>
            <ta e="T479" id="Seg_6149" s="T478">cardnum</ta>
            <ta e="T480" id="Seg_6150" s="T479">adj-n:(num)-n:case</ta>
            <ta e="T481" id="Seg_6151" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_6152" s="T481">n-n:case</ta>
            <ta e="T483" id="Seg_6153" s="T482">n-n:(poss)-n:case</ta>
            <ta e="T484" id="Seg_6154" s="T483">adv</ta>
            <ta e="T485" id="Seg_6155" s="T484">n-n&gt;adj-n:case</ta>
            <ta e="T486" id="Seg_6156" s="T485">v-v:tense-v:pred.pn</ta>
            <ta e="T487" id="Seg_6157" s="T486">v-v:(neg)-v:pred.pn</ta>
            <ta e="T488" id="Seg_6158" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_6159" s="T488">n-n:case</ta>
            <ta e="T490" id="Seg_6160" s="T489">v-v:tense-v:(poss)-v:(case)</ta>
            <ta e="T491" id="Seg_6161" s="T490">n-n:(num)-n:case</ta>
            <ta e="T492" id="Seg_6162" s="T491">n-n:(poss)-n:case</ta>
            <ta e="T493" id="Seg_6163" s="T492">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T494" id="Seg_6164" s="T493">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T495" id="Seg_6165" s="T494">pers-pro:case</ta>
            <ta e="T496" id="Seg_6166" s="T495">v-v:cvb</ta>
            <ta e="T497" id="Seg_6167" s="T496">v-v:cvb</ta>
            <ta e="T498" id="Seg_6168" s="T497">v-v:tense-v:pred.pn</ta>
            <ta e="T499" id="Seg_6169" s="T498">adj</ta>
            <ta e="T500" id="Seg_6170" s="T499">n-n:(poss)-n:case</ta>
            <ta e="T501" id="Seg_6171" s="T500">adj-n:case</ta>
            <ta e="T502" id="Seg_6172" s="T501">adj-n:case</ta>
            <ta e="T503" id="Seg_6173" s="T502">v-v:ptcp-n:(num)-n:poss-n:case</ta>
            <ta e="T504" id="Seg_6174" s="T503">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T505" id="Seg_6175" s="T504">cardnum</ta>
            <ta e="T506" id="Seg_6176" s="T505">n-n:case</ta>
            <ta e="T507" id="Seg_6177" s="T506">n-n:(num)-n:case</ta>
            <ta e="T508" id="Seg_6178" s="T507">que-pro:case</ta>
            <ta e="T509" id="Seg_6179" s="T508">conj</ta>
            <ta e="T510" id="Seg_6180" s="T509">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T511" id="Seg_6181" s="T510">n-n:poss-n:case</ta>
            <ta e="T512" id="Seg_6182" s="T511">n-n:(poss)</ta>
            <ta e="T513" id="Seg_6183" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_6184" s="T513">v-v:ptcp-v:(case)</ta>
            <ta e="T515" id="Seg_6185" s="T514">post</ta>
            <ta e="T516" id="Seg_6186" s="T515">n-n:poss-n:case</ta>
            <ta e="T517" id="Seg_6187" s="T516">post</ta>
            <ta e="T518" id="Seg_6188" s="T517">v-v&gt;v-v:cvb</ta>
            <ta e="T519" id="Seg_6189" s="T518">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_6190" s="T0">n</ta>
            <ta e="T2" id="Seg_6191" s="T1">n</ta>
            <ta e="T3" id="Seg_6192" s="T2">adv</ta>
            <ta e="T4" id="Seg_6193" s="T3">v</ta>
            <ta e="T5" id="Seg_6194" s="T4">propr</ta>
            <ta e="T6" id="Seg_6195" s="T5">v</ta>
            <ta e="T7" id="Seg_6196" s="T6">n</ta>
            <ta e="T8" id="Seg_6197" s="T7">n</ta>
            <ta e="T9" id="Seg_6198" s="T8">adj</ta>
            <ta e="T10" id="Seg_6199" s="T9">v</ta>
            <ta e="T11" id="Seg_6200" s="T10">aux</ta>
            <ta e="T12" id="Seg_6201" s="T11">aux</ta>
            <ta e="T13" id="Seg_6202" s="T12">adj</ta>
            <ta e="T14" id="Seg_6203" s="T13">n</ta>
            <ta e="T15" id="Seg_6204" s="T14">post</ta>
            <ta e="T16" id="Seg_6205" s="T15">n</ta>
            <ta e="T17" id="Seg_6206" s="T16">adv</ta>
            <ta e="T18" id="Seg_6207" s="T17">v</ta>
            <ta e="T19" id="Seg_6208" s="T18">n</ta>
            <ta e="T20" id="Seg_6209" s="T19">dempro</ta>
            <ta e="T21" id="Seg_6210" s="T20">adj</ta>
            <ta e="T22" id="Seg_6211" s="T21">v</ta>
            <ta e="T23" id="Seg_6212" s="T22">n</ta>
            <ta e="T24" id="Seg_6213" s="T23">que</ta>
            <ta e="T25" id="Seg_6214" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_6215" s="T25">que</ta>
            <ta e="T27" id="Seg_6216" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_6217" s="T27">v</ta>
            <ta e="T29" id="Seg_6218" s="T28">adv</ta>
            <ta e="T30" id="Seg_6219" s="T29">adj</ta>
            <ta e="T31" id="Seg_6220" s="T30">n</ta>
            <ta e="T32" id="Seg_6221" s="T31">n</ta>
            <ta e="T33" id="Seg_6222" s="T32">v</ta>
            <ta e="T34" id="Seg_6223" s="T33">emphpro</ta>
            <ta e="T35" id="Seg_6224" s="T34">n</ta>
            <ta e="T36" id="Seg_6225" s="T35">n</ta>
            <ta e="T37" id="Seg_6226" s="T36">v</ta>
            <ta e="T38" id="Seg_6227" s="T37">que</ta>
            <ta e="T39" id="Seg_6228" s="T38">v</ta>
            <ta e="T40" id="Seg_6229" s="T39">propr</ta>
            <ta e="T41" id="Seg_6230" s="T40">n</ta>
            <ta e="T42" id="Seg_6231" s="T41">cardnum</ta>
            <ta e="T43" id="Seg_6232" s="T42">n</ta>
            <ta e="T44" id="Seg_6233" s="T43">n</ta>
            <ta e="T45" id="Seg_6234" s="T44">adv</ta>
            <ta e="T46" id="Seg_6235" s="T45">adv</ta>
            <ta e="T47" id="Seg_6236" s="T46">v</ta>
            <ta e="T48" id="Seg_6237" s="T47">aux</ta>
            <ta e="T49" id="Seg_6238" s="T48">n</ta>
            <ta e="T50" id="Seg_6239" s="T49">v</ta>
            <ta e="T51" id="Seg_6240" s="T50">n</ta>
            <ta e="T52" id="Seg_6241" s="T51">n</ta>
            <ta e="T53" id="Seg_6242" s="T52">n</ta>
            <ta e="T54" id="Seg_6243" s="T53">n</ta>
            <ta e="T55" id="Seg_6244" s="T54">v</ta>
            <ta e="T56" id="Seg_6245" s="T55">que</ta>
            <ta e="T57" id="Seg_6246" s="T56">n</ta>
            <ta e="T58" id="Seg_6247" s="T57">v</ta>
            <ta e="T59" id="Seg_6248" s="T58">que</ta>
            <ta e="T60" id="Seg_6249" s="T59">n</ta>
            <ta e="T61" id="Seg_6250" s="T60">v</ta>
            <ta e="T62" id="Seg_6251" s="T61">dempro</ta>
            <ta e="T63" id="Seg_6252" s="T62">cardnum</ta>
            <ta e="T64" id="Seg_6253" s="T63">n</ta>
            <ta e="T65" id="Seg_6254" s="T64">n</ta>
            <ta e="T66" id="Seg_6255" s="T65">v</ta>
            <ta e="T67" id="Seg_6256" s="T66">post</ta>
            <ta e="T68" id="Seg_6257" s="T67">n</ta>
            <ta e="T69" id="Seg_6258" s="T68">n</ta>
            <ta e="T70" id="Seg_6259" s="T69">n</ta>
            <ta e="T71" id="Seg_6260" s="T70">v</ta>
            <ta e="T72" id="Seg_6261" s="T71">aux</ta>
            <ta e="T73" id="Seg_6262" s="T72">adj</ta>
            <ta e="T74" id="Seg_6263" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_6264" s="T74">n</ta>
            <ta e="T76" id="Seg_6265" s="T75">v</ta>
            <ta e="T77" id="Seg_6266" s="T76">propr</ta>
            <ta e="T78" id="Seg_6267" s="T77">adj</ta>
            <ta e="T79" id="Seg_6268" s="T78">n</ta>
            <ta e="T80" id="Seg_6269" s="T79">n</ta>
            <ta e="T81" id="Seg_6270" s="T80">n</ta>
            <ta e="T82" id="Seg_6271" s="T81">adj</ta>
            <ta e="T83" id="Seg_6272" s="T82">n</ta>
            <ta e="T84" id="Seg_6273" s="T83">n</ta>
            <ta e="T85" id="Seg_6274" s="T84">v</ta>
            <ta e="T86" id="Seg_6275" s="T85">v</ta>
            <ta e="T87" id="Seg_6276" s="T86">n</ta>
            <ta e="T88" id="Seg_6277" s="T87">n</ta>
            <ta e="T89" id="Seg_6278" s="T88">n</ta>
            <ta e="T90" id="Seg_6279" s="T89">v</ta>
            <ta e="T91" id="Seg_6280" s="T90">n</ta>
            <ta e="T92" id="Seg_6281" s="T91">v</ta>
            <ta e="T93" id="Seg_6282" s="T92">n</ta>
            <ta e="T94" id="Seg_6283" s="T93">dempro</ta>
            <ta e="T95" id="Seg_6284" s="T94">conj</ta>
            <ta e="T96" id="Seg_6285" s="T95">cop</ta>
            <ta e="T97" id="Seg_6286" s="T96">n</ta>
            <ta e="T98" id="Seg_6287" s="T97">n</ta>
            <ta e="T99" id="Seg_6288" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_6289" s="T99">v</ta>
            <ta e="T101" id="Seg_6290" s="T100">dempro</ta>
            <ta e="T102" id="Seg_6291" s="T101">n</ta>
            <ta e="T103" id="Seg_6292" s="T102">adv</ta>
            <ta e="T104" id="Seg_6293" s="T103">v</ta>
            <ta e="T105" id="Seg_6294" s="T104">aux</ta>
            <ta e="T106" id="Seg_6295" s="T105">n</ta>
            <ta e="T107" id="Seg_6296" s="T106">n</ta>
            <ta e="T108" id="Seg_6297" s="T107">adj</ta>
            <ta e="T109" id="Seg_6298" s="T108">n</ta>
            <ta e="T110" id="Seg_6299" s="T109">v</ta>
            <ta e="T111" id="Seg_6300" s="T110">adj</ta>
            <ta e="T112" id="Seg_6301" s="T111">v</ta>
            <ta e="T113" id="Seg_6302" s="T112">dempro</ta>
            <ta e="T114" id="Seg_6303" s="T113">v</ta>
            <ta e="T115" id="Seg_6304" s="T114">v</ta>
            <ta e="T116" id="Seg_6305" s="T115">n</ta>
            <ta e="T117" id="Seg_6306" s="T116">que</ta>
            <ta e="T118" id="Seg_6307" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_6308" s="T118">v</ta>
            <ta e="T120" id="Seg_6309" s="T119">aux</ta>
            <ta e="T121" id="Seg_6310" s="T120">interj</ta>
            <ta e="T122" id="Seg_6311" s="T121">v</ta>
            <ta e="T123" id="Seg_6312" s="T122">adj</ta>
            <ta e="T124" id="Seg_6313" s="T123">que</ta>
            <ta e="T125" id="Seg_6314" s="T124">n</ta>
            <ta e="T126" id="Seg_6315" s="T125">v</ta>
            <ta e="T127" id="Seg_6316" s="T126">que</ta>
            <ta e="T128" id="Seg_6317" s="T127">n</ta>
            <ta e="T129" id="Seg_6318" s="T128">n</ta>
            <ta e="T130" id="Seg_6319" s="T129">dempro</ta>
            <ta e="T131" id="Seg_6320" s="T130">cop</ta>
            <ta e="T132" id="Seg_6321" s="T131">que</ta>
            <ta e="T133" id="Seg_6322" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_6323" s="T133">n</ta>
            <ta e="T135" id="Seg_6324" s="T134">cop</ta>
            <ta e="T136" id="Seg_6325" s="T135">v</ta>
            <ta e="T137" id="Seg_6326" s="T136">n</ta>
            <ta e="T138" id="Seg_6327" s="T137">propr</ta>
            <ta e="T139" id="Seg_6328" s="T138">adj</ta>
            <ta e="T140" id="Seg_6329" s="T139">n</ta>
            <ta e="T141" id="Seg_6330" s="T140">v</ta>
            <ta e="T142" id="Seg_6331" s="T141">v</ta>
            <ta e="T143" id="Seg_6332" s="T142">n</ta>
            <ta e="T144" id="Seg_6333" s="T143">n</ta>
            <ta e="T145" id="Seg_6334" s="T144">cardnum</ta>
            <ta e="T146" id="Seg_6335" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_6336" s="T146">n</ta>
            <ta e="T148" id="Seg_6337" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_6338" s="T148">n</ta>
            <ta e="T150" id="Seg_6339" s="T149">v</ta>
            <ta e="T151" id="Seg_6340" s="T150">n</ta>
            <ta e="T152" id="Seg_6341" s="T151">n</ta>
            <ta e="T153" id="Seg_6342" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_6343" s="T153">v</ta>
            <ta e="T155" id="Seg_6344" s="T154">v</ta>
            <ta e="T156" id="Seg_6345" s="T155">dempro</ta>
            <ta e="T157" id="Seg_6346" s="T520">cardnum</ta>
            <ta e="T158" id="Seg_6347" s="T157">n</ta>
            <ta e="T159" id="Seg_6348" s="T158">n</ta>
            <ta e="T160" id="Seg_6349" s="T159">post</ta>
            <ta e="T161" id="Seg_6350" s="T160">v</ta>
            <ta e="T162" id="Seg_6351" s="T161">post</ta>
            <ta e="T163" id="Seg_6352" s="T162">v</ta>
            <ta e="T164" id="Seg_6353" s="T163">n</ta>
            <ta e="T165" id="Seg_6354" s="T164">post</ta>
            <ta e="T166" id="Seg_6355" s="T165">v</ta>
            <ta e="T167" id="Seg_6356" s="T166">n</ta>
            <ta e="T168" id="Seg_6357" s="T167">v</ta>
            <ta e="T170" id="Seg_6358" s="T168">aux</ta>
            <ta e="T171" id="Seg_6359" s="T170">n</ta>
            <ta e="T172" id="Seg_6360" s="T171">conj</ta>
            <ta e="T173" id="Seg_6361" s="T172">v</ta>
            <ta e="T174" id="Seg_6362" s="T173">v</ta>
            <ta e="T175" id="Seg_6363" s="T174">adv</ta>
            <ta e="T176" id="Seg_6364" s="T175">v</ta>
            <ta e="T177" id="Seg_6365" s="T176">n</ta>
            <ta e="T178" id="Seg_6366" s="T177">v</ta>
            <ta e="T179" id="Seg_6367" s="T178">post</ta>
            <ta e="T180" id="Seg_6368" s="T179">n</ta>
            <ta e="T181" id="Seg_6369" s="T180">n</ta>
            <ta e="T182" id="Seg_6370" s="T181">n</ta>
            <ta e="T183" id="Seg_6371" s="T182">adv</ta>
            <ta e="T184" id="Seg_6372" s="T183">v</ta>
            <ta e="T185" id="Seg_6373" s="T184">n</ta>
            <ta e="T186" id="Seg_6374" s="T185">adj</ta>
            <ta e="T187" id="Seg_6375" s="T186">n</ta>
            <ta e="T188" id="Seg_6376" s="T187">n</ta>
            <ta e="T189" id="Seg_6377" s="T188">v</ta>
            <ta e="T190" id="Seg_6378" s="T189">v</ta>
            <ta e="T191" id="Seg_6379" s="T190">post</ta>
            <ta e="T192" id="Seg_6380" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_6381" s="T192">que</ta>
            <ta e="T194" id="Seg_6382" s="T193">v</ta>
            <ta e="T195" id="Seg_6383" s="T194">n</ta>
            <ta e="T196" id="Seg_6384" s="T195">pers</ta>
            <ta e="T197" id="Seg_6385" s="T196">v</ta>
            <ta e="T198" id="Seg_6386" s="T197">n</ta>
            <ta e="T199" id="Seg_6387" s="T198">cop</ta>
            <ta e="T200" id="Seg_6388" s="T199">interj</ta>
            <ta e="T201" id="Seg_6389" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_6390" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_6391" s="T202">v</ta>
            <ta e="T204" id="Seg_6392" s="T203">v</ta>
            <ta e="T205" id="Seg_6393" s="T204">aux</ta>
            <ta e="T206" id="Seg_6394" s="T205">adj</ta>
            <ta e="T207" id="Seg_6395" s="T206">n</ta>
            <ta e="T208" id="Seg_6396" s="T207">v</ta>
            <ta e="T209" id="Seg_6397" s="T208">v</ta>
            <ta e="T210" id="Seg_6398" s="T209">n</ta>
            <ta e="T211" id="Seg_6399" s="T210">post</ta>
            <ta e="T212" id="Seg_6400" s="T211">v</ta>
            <ta e="T213" id="Seg_6401" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_6402" s="T213">v</ta>
            <ta e="T215" id="Seg_6403" s="T214">aux</ta>
            <ta e="T216" id="Seg_6404" s="T215">adj</ta>
            <ta e="T217" id="Seg_6405" s="T216">n</ta>
            <ta e="T218" id="Seg_6406" s="T217">v</ta>
            <ta e="T219" id="Seg_6407" s="T218">dempro</ta>
            <ta e="T220" id="Seg_6408" s="T219">v</ta>
            <ta e="T221" id="Seg_6409" s="T220">aux</ta>
            <ta e="T222" id="Seg_6410" s="T221">pers</ta>
            <ta e="T223" id="Seg_6411" s="T222">n</ta>
            <ta e="T224" id="Seg_6412" s="T223">n</ta>
            <ta e="T225" id="Seg_6413" s="T224">cop</ta>
            <ta e="T226" id="Seg_6414" s="T225">interj</ta>
            <ta e="T227" id="Seg_6415" s="T226">pers</ta>
            <ta e="T228" id="Seg_6416" s="T227">v</ta>
            <ta e="T229" id="Seg_6417" s="T228">v</ta>
            <ta e="T230" id="Seg_6418" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_6419" s="T230">v</ta>
            <ta e="T232" id="Seg_6420" s="T231">n</ta>
            <ta e="T233" id="Seg_6421" s="T232">v</ta>
            <ta e="T234" id="Seg_6422" s="T233">v</ta>
            <ta e="T235" id="Seg_6423" s="T234">interj</ta>
            <ta e="T236" id="Seg_6424" s="T235">que</ta>
            <ta e="T237" id="Seg_6425" s="T236">n</ta>
            <ta e="T238" id="Seg_6426" s="T237">v</ta>
            <ta e="T239" id="Seg_6427" s="T238">v</ta>
            <ta e="T240" id="Seg_6428" s="T239">n</ta>
            <ta e="T241" id="Seg_6429" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_6430" s="T241">v</ta>
            <ta e="T243" id="Seg_6431" s="T242">n</ta>
            <ta e="T244" id="Seg_6432" s="T243">n</ta>
            <ta e="T245" id="Seg_6433" s="T244">n</ta>
            <ta e="T246" id="Seg_6434" s="T245">v</ta>
            <ta e="T247" id="Seg_6435" s="T246">post</ta>
            <ta e="T248" id="Seg_6436" s="T247">cop</ta>
            <ta e="T249" id="Seg_6437" s="T248">n</ta>
            <ta e="T250" id="Seg_6438" s="T249">v</ta>
            <ta e="T251" id="Seg_6439" s="T250">adv</ta>
            <ta e="T252" id="Seg_6440" s="T251">n</ta>
            <ta e="T253" id="Seg_6441" s="T252">post</ta>
            <ta e="T254" id="Seg_6442" s="T253">n</ta>
            <ta e="T255" id="Seg_6443" s="T254">v</ta>
            <ta e="T256" id="Seg_6444" s="T255">aux</ta>
            <ta e="T257" id="Seg_6445" s="T256">adj</ta>
            <ta e="T258" id="Seg_6446" s="T257">interj</ta>
            <ta e="T259" id="Seg_6447" s="T258">v</ta>
            <ta e="T260" id="Seg_6448" s="T259">adj</ta>
            <ta e="T261" id="Seg_6449" s="T260">n</ta>
            <ta e="T262" id="Seg_6450" s="T261">n</ta>
            <ta e="T263" id="Seg_6451" s="T262">n</ta>
            <ta e="T264" id="Seg_6452" s="T263">pers</ta>
            <ta e="T265" id="Seg_6453" s="T264">v</ta>
            <ta e="T266" id="Seg_6454" s="T265">v</ta>
            <ta e="T267" id="Seg_6455" s="T266">n</ta>
            <ta e="T268" id="Seg_6456" s="T267">n</ta>
            <ta e="T269" id="Seg_6457" s="T268">dempro</ta>
            <ta e="T270" id="Seg_6458" s="T269">n</ta>
            <ta e="T271" id="Seg_6459" s="T270">n</ta>
            <ta e="T272" id="Seg_6460" s="T271">adv</ta>
            <ta e="T273" id="Seg_6461" s="T272">n</ta>
            <ta e="T274" id="Seg_6462" s="T273">n</ta>
            <ta e="T275" id="Seg_6463" s="T274">v</ta>
            <ta e="T276" id="Seg_6464" s="T275">adv</ta>
            <ta e="T277" id="Seg_6465" s="T276">adj</ta>
            <ta e="T278" id="Seg_6466" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_6467" s="T278">v</ta>
            <ta e="T280" id="Seg_6468" s="T279">v</ta>
            <ta e="T281" id="Seg_6469" s="T280">n</ta>
            <ta e="T282" id="Seg_6470" s="T281">n</ta>
            <ta e="T283" id="Seg_6471" s="T282">post</ta>
            <ta e="T284" id="Seg_6472" s="T283">n</ta>
            <ta e="T285" id="Seg_6473" s="T284">n</ta>
            <ta e="T288" id="Seg_6474" s="T287">n</ta>
            <ta e="T289" id="Seg_6475" s="T288">n</ta>
            <ta e="T290" id="Seg_6476" s="T289">n</ta>
            <ta e="T291" id="Seg_6477" s="T290">v</ta>
            <ta e="T292" id="Seg_6478" s="T291">v</ta>
            <ta e="T293" id="Seg_6479" s="T292">v</ta>
            <ta e="T294" id="Seg_6480" s="T293">n</ta>
            <ta e="T295" id="Seg_6481" s="T294">n</ta>
            <ta e="T296" id="Seg_6482" s="T295">v</ta>
            <ta e="T297" id="Seg_6483" s="T296">n</ta>
            <ta e="T298" id="Seg_6484" s="T297">n</ta>
            <ta e="T299" id="Seg_6485" s="T298">v</ta>
            <ta e="T300" id="Seg_6486" s="T299">aux</ta>
            <ta e="T301" id="Seg_6487" s="T300">pers</ta>
            <ta e="T302" id="Seg_6488" s="T301">n</ta>
            <ta e="T303" id="Seg_6489" s="T302">adv</ta>
            <ta e="T304" id="Seg_6490" s="T303">n</ta>
            <ta e="T305" id="Seg_6491" s="T304">v</ta>
            <ta e="T306" id="Seg_6492" s="T305">n</ta>
            <ta e="T307" id="Seg_6493" s="T306">adv</ta>
            <ta e="T308" id="Seg_6494" s="T307">cop</ta>
            <ta e="T309" id="Seg_6495" s="T308">v</ta>
            <ta e="T312" id="Seg_6496" s="T311">v</ta>
            <ta e="T313" id="Seg_6497" s="T312">aux</ta>
            <ta e="T314" id="Seg_6498" s="T313">aux</ta>
            <ta e="T315" id="Seg_6499" s="T314">que</ta>
            <ta e="T316" id="Seg_6500" s="T315">n</ta>
            <ta e="T317" id="Seg_6501" s="T316">n</ta>
            <ta e="T318" id="Seg_6502" s="T317">v</ta>
            <ta e="T319" id="Seg_6503" s="T318">v</ta>
            <ta e="T320" id="Seg_6504" s="T319">n</ta>
            <ta e="T321" id="Seg_6505" s="T320">n</ta>
            <ta e="T322" id="Seg_6506" s="T321">v</ta>
            <ta e="T323" id="Seg_6507" s="T322">v</ta>
            <ta e="T324" id="Seg_6508" s="T323">n</ta>
            <ta e="T325" id="Seg_6509" s="T324">n</ta>
            <ta e="T326" id="Seg_6510" s="T325">v</ta>
            <ta e="T327" id="Seg_6511" s="T326">n</ta>
            <ta e="T328" id="Seg_6512" s="T327">n</ta>
            <ta e="T331" id="Seg_6513" s="T330">n</ta>
            <ta e="T332" id="Seg_6514" s="T331">adv</ta>
            <ta e="T333" id="Seg_6515" s="T332">n</ta>
            <ta e="T334" id="Seg_6516" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_6517" s="T334">v</ta>
            <ta e="T336" id="Seg_6518" s="T335">n</ta>
            <ta e="T337" id="Seg_6519" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_6520" s="T337">n</ta>
            <ta e="T339" id="Seg_6521" s="T338">v</ta>
            <ta e="T340" id="Seg_6522" s="T339">que</ta>
            <ta e="T341" id="Seg_6523" s="T340">n</ta>
            <ta e="T342" id="Seg_6524" s="T341">dempro</ta>
            <ta e="T343" id="Seg_6525" s="T342">n</ta>
            <ta e="T344" id="Seg_6526" s="T343">v</ta>
            <ta e="T345" id="Seg_6527" s="T344">ptcl</ta>
            <ta e="T346" id="Seg_6528" s="T345">pers</ta>
            <ta e="T347" id="Seg_6529" s="T346">v</ta>
            <ta e="T348" id="Seg_6530" s="T347">v</ta>
            <ta e="T349" id="Seg_6531" s="T348">propr</ta>
            <ta e="T350" id="Seg_6532" s="T349">n</ta>
            <ta e="T351" id="Seg_6533" s="T350">que</ta>
            <ta e="T352" id="Seg_6534" s="T351">ptcl</ta>
            <ta e="T353" id="Seg_6535" s="T352">n</ta>
            <ta e="T356" id="Seg_6536" s="T355">v</ta>
            <ta e="T357" id="Seg_6537" s="T356">n</ta>
            <ta e="T358" id="Seg_6538" s="T357">n</ta>
            <ta e="T359" id="Seg_6539" s="T358">interj</ta>
            <ta e="T360" id="Seg_6540" s="T359">v</ta>
            <ta e="T361" id="Seg_6541" s="T360">v</ta>
            <ta e="T362" id="Seg_6542" s="T361">v</ta>
            <ta e="T363" id="Seg_6543" s="T362">n</ta>
            <ta e="T364" id="Seg_6544" s="T363">n</ta>
            <ta e="T365" id="Seg_6545" s="T364">n</ta>
            <ta e="T366" id="Seg_6546" s="T365">v</ta>
            <ta e="T367" id="Seg_6547" s="T366">que</ta>
            <ta e="T368" id="Seg_6548" s="T367">v</ta>
            <ta e="T369" id="Seg_6549" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_6550" s="T369">v</ta>
            <ta e="T371" id="Seg_6551" s="T370">aux</ta>
            <ta e="T372" id="Seg_6552" s="T371">propr</ta>
            <ta e="T373" id="Seg_6553" s="T372">v</ta>
            <ta e="T374" id="Seg_6554" s="T373">pers</ta>
            <ta e="T375" id="Seg_6555" s="T374">n</ta>
            <ta e="T376" id="Seg_6556" s="T375">n</ta>
            <ta e="T377" id="Seg_6557" s="T376">n</ta>
            <ta e="T378" id="Seg_6558" s="T377">v</ta>
            <ta e="T379" id="Seg_6559" s="T378">que</ta>
            <ta e="T380" id="Seg_6560" s="T379">n</ta>
            <ta e="T381" id="Seg_6561" s="T380">cop</ta>
            <ta e="T382" id="Seg_6562" s="T381">n</ta>
            <ta e="T383" id="Seg_6563" s="T382">n</ta>
            <ta e="T384" id="Seg_6564" s="T383">adv</ta>
            <ta e="T385" id="Seg_6565" s="T384">v</ta>
            <ta e="T386" id="Seg_6566" s="T385">v</ta>
            <ta e="T387" id="Seg_6567" s="T386">v</ta>
            <ta e="T388" id="Seg_6568" s="T387">v</ta>
            <ta e="T389" id="Seg_6569" s="T388">v</ta>
            <ta e="T390" id="Seg_6570" s="T389">adv</ta>
            <ta e="T391" id="Seg_6571" s="T390">n</ta>
            <ta e="T392" id="Seg_6572" s="T391">n</ta>
            <ta e="T393" id="Seg_6573" s="T392">v</ta>
            <ta e="T394" id="Seg_6574" s="T393">que</ta>
            <ta e="T395" id="Seg_6575" s="T394">v</ta>
            <ta e="T396" id="Seg_6576" s="T395">n</ta>
            <ta e="T397" id="Seg_6577" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_6578" s="T397">v</ta>
            <ta e="T399" id="Seg_6579" s="T398">n</ta>
            <ta e="T400" id="Seg_6580" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_6581" s="T400">v</ta>
            <ta e="T402" id="Seg_6582" s="T401">interj</ta>
            <ta e="T403" id="Seg_6583" s="T402">n</ta>
            <ta e="T404" id="Seg_6584" s="T403">v</ta>
            <ta e="T405" id="Seg_6585" s="T404">aux</ta>
            <ta e="T406" id="Seg_6586" s="T405">v</ta>
            <ta e="T407" id="Seg_6587" s="T406">v</ta>
            <ta e="T408" id="Seg_6588" s="T407">n</ta>
            <ta e="T409" id="Seg_6589" s="T408">que</ta>
            <ta e="T410" id="Seg_6590" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_6591" s="T410">post</ta>
            <ta e="T412" id="Seg_6592" s="T411">n</ta>
            <ta e="T413" id="Seg_6593" s="T412">v</ta>
            <ta e="T414" id="Seg_6594" s="T413">v</ta>
            <ta e="T415" id="Seg_6595" s="T414">que</ta>
            <ta e="T416" id="Seg_6596" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_6597" s="T416">v</ta>
            <ta e="T418" id="Seg_6598" s="T417">aux</ta>
            <ta e="T419" id="Seg_6599" s="T418">n</ta>
            <ta e="T420" id="Seg_6600" s="T419">distrnum</ta>
            <ta e="T421" id="Seg_6601" s="T420">v</ta>
            <ta e="T422" id="Seg_6602" s="T421">pers</ta>
            <ta e="T423" id="Seg_6603" s="T422">n</ta>
            <ta e="T424" id="Seg_6604" s="T423">v</ta>
            <ta e="T425" id="Seg_6605" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_6606" s="T425">n</ta>
            <ta e="T427" id="Seg_6607" s="T426">n</ta>
            <ta e="T428" id="Seg_6608" s="T427">v</ta>
            <ta e="T429" id="Seg_6609" s="T428">n</ta>
            <ta e="T430" id="Seg_6610" s="T429">n</ta>
            <ta e="T431" id="Seg_6611" s="T430">adv</ta>
            <ta e="T432" id="Seg_6612" s="T431">v</ta>
            <ta e="T433" id="Seg_6613" s="T432">n</ta>
            <ta e="T434" id="Seg_6614" s="T433">v</ta>
            <ta e="T435" id="Seg_6615" s="T434">n</ta>
            <ta e="T436" id="Seg_6616" s="T435">v</ta>
            <ta e="T437" id="Seg_6617" s="T436">n</ta>
            <ta e="T438" id="Seg_6618" s="T437">n</ta>
            <ta e="T439" id="Seg_6619" s="T438">post</ta>
            <ta e="T440" id="Seg_6620" s="T439">cop</ta>
            <ta e="T441" id="Seg_6621" s="T440">interj</ta>
            <ta e="T442" id="Seg_6622" s="T441">que</ta>
            <ta e="T443" id="Seg_6623" s="T442">cop</ta>
            <ta e="T444" id="Seg_6624" s="T443">v</ta>
            <ta e="T445" id="Seg_6625" s="T444">v</ta>
            <ta e="T446" id="Seg_6626" s="T445">distrnum</ta>
            <ta e="T447" id="Seg_6627" s="T446">n</ta>
            <ta e="T448" id="Seg_6628" s="T447">interj</ta>
            <ta e="T449" id="Seg_6629" s="T448">v</ta>
            <ta e="T450" id="Seg_6630" s="T449">n</ta>
            <ta e="T451" id="Seg_6631" s="T450">v</ta>
            <ta e="T452" id="Seg_6632" s="T451">v</ta>
            <ta e="T453" id="Seg_6633" s="T452">v</ta>
            <ta e="T454" id="Seg_6634" s="T453">cardnum</ta>
            <ta e="T455" id="Seg_6635" s="T454">n</ta>
            <ta e="T456" id="Seg_6636" s="T455">interj</ta>
            <ta e="T457" id="Seg_6637" s="T456">v</ta>
            <ta e="T458" id="Seg_6638" s="T457">aux</ta>
            <ta e="T459" id="Seg_6639" s="T458">propr</ta>
            <ta e="T460" id="Seg_6640" s="T459">v</ta>
            <ta e="T461" id="Seg_6641" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_6642" s="T461">v</ta>
            <ta e="T463" id="Seg_6643" s="T462">que</ta>
            <ta e="T464" id="Seg_6644" s="T463">n</ta>
            <ta e="T465" id="Seg_6645" s="T464">v</ta>
            <ta e="T466" id="Seg_6646" s="T465">n</ta>
            <ta e="T467" id="Seg_6647" s="T466">n</ta>
            <ta e="T468" id="Seg_6648" s="T467">n</ta>
            <ta e="T469" id="Seg_6649" s="T468">v</ta>
            <ta e="T470" id="Seg_6650" s="T469">dempro</ta>
            <ta e="T471" id="Seg_6651" s="T470">que</ta>
            <ta e="T472" id="Seg_6652" s="T471">cop</ta>
            <ta e="T473" id="Seg_6653" s="T472">n</ta>
            <ta e="T474" id="Seg_6654" s="T473">v</ta>
            <ta e="T475" id="Seg_6655" s="T474">n</ta>
            <ta e="T476" id="Seg_6656" s="T475">v</ta>
            <ta e="T477" id="Seg_6657" s="T476">adv</ta>
            <ta e="T478" id="Seg_6658" s="T477">v</ta>
            <ta e="T479" id="Seg_6659" s="T478">cardnum</ta>
            <ta e="T480" id="Seg_6660" s="T479">n</ta>
            <ta e="T481" id="Seg_6661" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_6662" s="T481">n</ta>
            <ta e="T483" id="Seg_6663" s="T482">n</ta>
            <ta e="T484" id="Seg_6664" s="T483">adv</ta>
            <ta e="T485" id="Seg_6665" s="T484">adj</ta>
            <ta e="T486" id="Seg_6666" s="T485">cop</ta>
            <ta e="T487" id="Seg_6667" s="T486">v</ta>
            <ta e="T488" id="Seg_6668" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_6669" s="T488">n</ta>
            <ta e="T490" id="Seg_6670" s="T489">v</ta>
            <ta e="T491" id="Seg_6671" s="T490">n</ta>
            <ta e="T492" id="Seg_6672" s="T491">n</ta>
            <ta e="T493" id="Seg_6673" s="T492">v</ta>
            <ta e="T494" id="Seg_6674" s="T493">dempro</ta>
            <ta e="T495" id="Seg_6675" s="T494">pers</ta>
            <ta e="T496" id="Seg_6676" s="T495">v</ta>
            <ta e="T497" id="Seg_6677" s="T496">v</ta>
            <ta e="T498" id="Seg_6678" s="T497">aux</ta>
            <ta e="T499" id="Seg_6679" s="T498">adj</ta>
            <ta e="T500" id="Seg_6680" s="T499">n</ta>
            <ta e="T501" id="Seg_6681" s="T500">adj</ta>
            <ta e="T502" id="Seg_6682" s="T501">adj</ta>
            <ta e="T503" id="Seg_6683" s="T502">v</ta>
            <ta e="T504" id="Seg_6684" s="T503">v</ta>
            <ta e="T505" id="Seg_6685" s="T504">cardnum</ta>
            <ta e="T506" id="Seg_6686" s="T505">n</ta>
            <ta e="T507" id="Seg_6687" s="T506">n</ta>
            <ta e="T508" id="Seg_6688" s="T507">que</ta>
            <ta e="T509" id="Seg_6689" s="T508">conj</ta>
            <ta e="T510" id="Seg_6690" s="T509">v</ta>
            <ta e="T511" id="Seg_6691" s="T510">n</ta>
            <ta e="T512" id="Seg_6692" s="T511">n</ta>
            <ta e="T513" id="Seg_6693" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_6694" s="T513">v</ta>
            <ta e="T515" id="Seg_6695" s="T514">post</ta>
            <ta e="T516" id="Seg_6696" s="T515">n</ta>
            <ta e="T517" id="Seg_6697" s="T516">post</ta>
            <ta e="T518" id="Seg_6698" s="T517">v</ta>
            <ta e="T519" id="Seg_6699" s="T518">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_6700" s="T0">RUS:cult</ta>
            <ta e="T5" id="Seg_6701" s="T4">RUS:cult</ta>
            <ta e="T40" id="Seg_6702" s="T39">RUS:cult</ta>
            <ta e="T60" id="Seg_6703" s="T59">RUS:cult</ta>
            <ta e="T77" id="Seg_6704" s="T76">NGAN:cult</ta>
            <ta e="T87" id="Seg_6705" s="T86">RUS:cult</ta>
            <ta e="T95" id="Seg_6706" s="T94">RUS:gram</ta>
            <ta e="T138" id="Seg_6707" s="T137">RUS:cult</ta>
            <ta e="T151" id="Seg_6708" s="T150">RUS:cult</ta>
            <ta e="T171" id="Seg_6709" s="T170">RUS:cult</ta>
            <ta e="T172" id="Seg_6710" s="T171">RUS:gram</ta>
            <ta e="T175" id="Seg_6711" s="T174">EV:gram (INTNS)</ta>
            <ta e="T182" id="Seg_6712" s="T181">RUS:cult</ta>
            <ta e="T226" id="Seg_6713" s="T225">RUS:disc</ta>
            <ta e="T240" id="Seg_6714" s="T239">EV:core</ta>
            <ta e="T263" id="Seg_6715" s="T262">EV:core</ta>
            <ta e="T267" id="Seg_6716" s="T266">RUS:cult</ta>
            <ta e="T271" id="Seg_6717" s="T270">RUS:cult</ta>
            <ta e="T284" id="Seg_6718" s="T283">RUS:cult</ta>
            <ta e="T298" id="Seg_6719" s="T297">RUS:cult</ta>
            <ta e="T304" id="Seg_6720" s="T303">RUS:cult</ta>
            <ta e="T484" id="Seg_6721" s="T483">RUS:mod</ta>
            <ta e="T491" id="Seg_6722" s="T490">RUS:cult</ta>
            <ta e="T509" id="Seg_6723" s="T508">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T14" id="Seg_6724" s="T0">When the Soviet time had just come, the Dolgan people in the village of Kurya went fishing every summer.</ta>
            <ta e="T19" id="Seg_6725" s="T14">A village, to which cross-eyed [people] don't come often.</ta>
            <ta e="T23" id="Seg_6726" s="T19">So far away the fishing grounds are. </ta>
            <ta e="T41" id="Seg_6727" s="T23">Sometimes somebody comes to these close and rotten (?) place, in order to tell news about his life, in order to get to know how the people from Kurya are fishing.</ta>
            <ta e="T50" id="Seg_6728" s="T41">Once in summer the old men had rowed away early in the morging to check the tributaries.</ta>
            <ta e="T55" id="Seg_6729" s="T50">The women and the children of the clan sat at home.</ta>
            <ta e="T61" id="Seg_6730" s="T55">Some of them are cleaning fish, some are drying fish.</ta>
            <ta e="T72" id="Seg_6731" s="T61">Two old women among them, until the old men will come back, are patching, apparently, the covers of the sledge houses. </ta>
            <ta e="T76" id="Seg_6732" s="T72">It was a very hot day.</ta>
            <ta e="T86" id="Seg_6733" s="T76">In the lull of the holy river Dudupta a row of white thunder clouds is visible.</ta>
            <ta e="T92" id="Seg_6734" s="T86">It was such a heat that the wings of the poor mosquitos dried out.</ta>
            <ta e="T100" id="Seg_6735" s="T92">The women chase them away, although their hands are occupied.</ta>
            <ta e="T105" id="Seg_6736" s="T100">Thereby they were sewing the whole time.</ta>
            <ta e="T112" id="Seg_6737" s="T105">The sound of their needles, when they scratch on the colourful skins, can be heard from far away.</ta>
            <ta e="T120" id="Seg_6738" s="T112">When they were sitting and working, for some reason the earth trembled.</ta>
            <ta e="T123" id="Seg_6739" s="T120">"Eh", one of them said.</ta>
            <ta e="T126" id="Seg_6740" s="T123">"Why does our earth tremble?</ta>
            <ta e="T131" id="Seg_6741" s="T126">Why the rolling thunder is such?</ta>
            <ta e="T137" id="Seg_6742" s="T131">Something strange has happened, listen, old woman."</ta>
            <ta e="T155" id="Seg_6743" s="T137">Kristin, an old woman with sparse and whitely shimmering hair, in her mouth she has not a single tooth, she took off her kerchief, which doesn't let a sound through, and listened.</ta>
            <ta e="T170" id="Seg_6744" s="T155">That (…), both women, looking(?) up to the sky, like chased reindeer, listen to the roaring sound.</ta>
            <ta e="T179" id="Seg_6745" s="T170">Not even noticing the mosquito stings they sat for a long time there, having opened their mouths.</ta>
            <ta e="T184" id="Seg_6746" s="T179">Beneath them here and there fur rags are lying around.</ta>
            <ta e="T189" id="Seg_6747" s="T184">Her tobacco pipes, tied up with a rope, smoke in their hands.</ta>
            <ta e="T194" id="Seg_6748" s="T189">Like waiting for something they are sitting there silently.</ta>
            <ta e="T199" id="Seg_6749" s="T194">And indeed, a never heard sound came up.</ta>
            <ta e="T212" id="Seg_6750" s="T199">"Oh, we will die!", one of the old women said, jumped up and looked in the direction of the forest.</ta>
            <ta e="T217" id="Seg_6751" s="T212">"Look, there is a big bird flying.</ta>
            <ta e="T225" id="Seg_6752" s="T217">Look, it is read, that's apparently the sound of his back.</ta>
            <ta e="T234" id="Seg_6753" s="T225">My God, I didn't say anything, didn't see anything", the old woman said and pattered away.</ta>
            <ta e="T242" id="Seg_6754" s="T234">"Oh, what a bird is it, you say", her friend said and looked, too.</ta>
            <ta e="T248" id="Seg_6755" s="T242">Under their feet the earth seemed to move.</ta>
            <ta e="T250" id="Seg_6756" s="T248">The tents shivered.</ta>
            <ta e="T268" id="Seg_6757" s="T250">"Quick, under the sledge!", one of them said, made "chip" and crawled under the floor, which was covered with small nails, her friend followed suit and crawled under the balok.</ta>
            <ta e="T280" id="Seg_6758" s="T268">At that time the airman was just flying very low in order to watch the pole tents.</ta>
            <ta e="T293" id="Seg_6759" s="T280">From under the balok (…) the old women's leather shoes were visible without being moved.</ta>
            <ta e="T300" id="Seg_6760" s="T293">On the shore the old men jumped and waved with their caps to the airplane.</ta>
            <ta e="T308" id="Seg_6761" s="T300">They are apparently happy that the airplane has come well to the tundra for the first time.</ta>
            <ta e="T319" id="Seg_6762" s="T308">They know that the Soviet power will come to look how the reindeer herders and the fishermen live.</ta>
            <ta e="T326" id="Seg_6763" s="T319">They went up to the hill are were wondering - the pole tents didn't smoke.</ta>
            <ta e="T332" id="Seg_6764" s="T326">Over the yard there is silence.</ta>
            <ta e="T339" id="Seg_6765" s="T332">Neither dogs are barking, neither human voices are heard.</ta>
            <ta e="T343" id="Seg_6766" s="T339">"What people are these old women?</ta>
            <ta e="T350" id="Seg_6767" s="T343">Don't they know that we come?", the old Onoo said.</ta>
            <ta e="T358" id="Seg_6768" s="T350">"Where are you, housemates", one inhabitant shouted.</ta>
            <ta e="T366" id="Seg_6769" s="T358">"Oh, help, untie us, we will die", under the earth a scream was heard.</ta>
            <ta e="T373" id="Seg_6770" s="T366">"What's that, are they gone crazy?", Onoo said and looked around.</ta>
            <ta e="T378" id="Seg_6771" s="T373">He looked under the sledge houses.</ta>
            <ta e="T381" id="Seg_6772" s="T378">What's that strange stuff?</ta>
            <ta e="T386" id="Seg_6773" s="T381">The old women's legs were lying there shivering.</ta>
            <ta e="T390" id="Seg_6774" s="T386">When he looks, they apparently try to get out.</ta>
            <ta e="T393" id="Seg_6775" s="T390">Small nails bottle their clothes up.</ta>
            <ta e="T395" id="Seg_6776" s="T393">Why do they release [them]?</ta>
            <ta e="T401" id="Seg_6777" s="T395">They neither let them go in nor out.</ta>
            <ta e="T407" id="Seg_6778" s="T401">The screams "oh no" are increasing, moaning is added.</ta>
            <ta e="T418" id="Seg_6779" s="T407">The old men couldn't effort anything out of wonder, they stayed dumb.</ta>
            <ta e="T421" id="Seg_6780" s="T418">They freed the old women separately.</ta>
            <ta e="T425" id="Seg_6781" s="T421">They get again a face.</ta>
            <ta e="T432" id="Seg_6782" s="T425">Their hair covered their eyes, their leather shoes were clinched.</ta>
            <ta e="T440" id="Seg_6783" s="T432">Their look changed: they were like young polar foxes, who had just come out of the earth.</ta>
            <ta e="T447" id="Seg_6784" s="T440">"Eh, what's happened", each of them asked.</ta>
            <ta e="T455" id="Seg_6785" s="T447">"Quiet, an unknown bird has passed by", the two old women whispered.</ta>
            <ta e="T460" id="Seg_6786" s="T455">"Oh", as if he wouldn't know, Onoo agreed.</ta>
            <ta e="T469" id="Seg_6787" s="T460">"Tell, though, what a dream did you dream, when lying under the sledge house?"</ta>
            <ta e="T473" id="Seg_6788" s="T469">"What is with the old man?</ta>
            <ta e="T480" id="Seg_6789" s="T473">Getting old, his mind weakens apparently", the two scaredy-cats shouted.</ta>
            <ta e="T486" id="Seg_6790" s="T480">Well, the scaredy-cats started to tell.</ta>
            <ta e="T493" id="Seg_6791" s="T486">"Don't you know then that the times have changed, that a bird of the Soviets is coming?</ta>
            <ta e="T498" id="Seg_6792" s="T493">It comes flying in order to help you.</ta>
            <ta e="T506" id="Seg_6793" s="T498">For that the Dolgans live sated and richly", both men said.</ta>
            <ta e="T519" id="Seg_6794" s="T506">The old women couldn't think anymore and didn't say anything, they slouched home, as if they were ashamed.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T14" id="Seg_6795" s="T0">Als die Sowjetzeit gerade angebrochen war, ging das dolganische Volk im Dorf Kurja jeden Sommer fischen.</ta>
            <ta e="T19" id="Seg_6796" s="T14">Ein Dorf, wo Queräugige nicht oft hinkommen.</ta>
            <ta e="T23" id="Seg_6797" s="T19">So weit sind die Fischgründe.</ta>
            <ta e="T41" id="Seg_6798" s="T23">Irgendwann kommt irgendjemand zu diesen nahen verfaulten (?) Orten, um Neuigkeiten über sein Leben zu erzählen, um zu erfahren, wie die Leute aus Kurja fischen.</ta>
            <ta e="T50" id="Seg_6799" s="T41">Einmal im Sommer waren die alten Männer früh am Morgen losgerudert, um die Zuflüsse zu überprüfen.</ta>
            <ta e="T55" id="Seg_6800" s="T50">Die Frauen und die Kinder der Verwandtschaft saßen zu Hause.</ta>
            <ta e="T61" id="Seg_6801" s="T55">Einige säubern Fisch, einige trocknen Fisch.</ta>
            <ta e="T72" id="Seg_6802" s="T61">Zwei alte Frauen von ihnen, bis die alten Männer kommen, flicken offenbar die Decken von den Schlittenhäusern.</ta>
            <ta e="T76" id="Seg_6803" s="T72">Es war ein sehr heißer Tag.</ta>
            <ta e="T86" id="Seg_6804" s="T76">In der Windstille des heiligen Duduptaflusses sind weiße Donnerwolken aufgereiht zu sehen.</ta>
            <ta e="T92" id="Seg_6805" s="T86">Es war so eine Hitze, in der die Flügel der Mücken, der Armen, austrocknen.</ta>
            <ta e="T100" id="Seg_6806" s="T92">Die alten Frauen, obwohl ihre Hände beschäftigt sind, verjagen sie.</ta>
            <ta e="T105" id="Seg_6807" s="T100">Dabei waren sie immer dabei zu nähen.</ta>
            <ta e="T112" id="Seg_6808" s="T105">Das Geräusch ihrer Nadeln, wenn sie über die bunten Häute kratzen, ist von weitem zu hören.</ta>
            <ta e="T120" id="Seg_6809" s="T112">Als sie saßen und arbeiteten, dröhnte aus irgendeinem Grund die Erde.</ta>
            <ta e="T123" id="Seg_6810" s="T120">"Hä", sagte eine von ihnen.</ta>
            <ta e="T126" id="Seg_6811" s="T123">"Warum dröhnt unsere Erde?</ta>
            <ta e="T131" id="Seg_6812" s="T126">Warum ist das Donnergrollen so?</ta>
            <ta e="T137" id="Seg_6813" s="T131">Irgendetwas Merkwürdiges ist passiert, hör hin, Alte."</ta>
            <ta e="T155" id="Seg_6814" s="T137">Kristin, eine alte Frau mit lichtem, weiß schimmernden Haar, in ihrem Mund hat sie nicht einen Zahn, nahm ihr Tuch, durch das kein Laut dringt, ab und lauschte.</ta>
            <ta e="T170" id="Seg_6815" s="T155">Das (…), die beiden Frauen, ihren Blick(?) in Richtung Himmel gerichtet, wie gejagte Rentiere, lauschten dem dröhnenden Geräusch.</ta>
            <ta e="T179" id="Seg_6816" s="T170">Nicht einmal die Mückenstiche bemerkend saßen sie lange da, ihre Münder aufgerissen.</ta>
            <ta e="T184" id="Seg_6817" s="T179">Neben ihnen liegen hier und dort Fellfetzen herum.</ta>
            <ta e="T189" id="Seg_6818" s="T184">Ihre mit einer Schnur festgebundenen Tabakspfeifen rauchen in ihren Händen.</ta>
            <ta e="T194" id="Seg_6819" s="T189">Wie in Erwartung saßen sie leise da.</ta>
            <ta e="T199" id="Seg_6820" s="T194">Und tatsächlich, ein noch nicht gekanntes Geräusch ertönte.</ta>
            <ta e="T212" id="Seg_6821" s="T199">"Oh, wir sterben!", sagte eine der beiden alten Frauen, sprang auf und schaute in Richtung des Waldes.</ta>
            <ta e="T217" id="Seg_6822" s="T212">"Guck, da fliegt ein großer Vogel.</ta>
            <ta e="T225" id="Seg_6823" s="T217">Schau, er ist rot, das ist offenbar das Geräusch seines Rückens.</ta>
            <ta e="T234" id="Seg_6824" s="T225">Mein Gott, ich habe nichts gesagt, nichts gesehen", sagte die alte Frau und trappelte davon.</ta>
            <ta e="T242" id="Seg_6825" s="T234">"Oh, was ist das für ein Vogel, sagst du", sagte ihre Freundin und schaute auch.</ta>
            <ta e="T248" id="Seg_6826" s="T242">Unter ihren Füßen schien sich die Erde zu bewegen.</ta>
            <ta e="T250" id="Seg_6827" s="T248">Die Zelte zitterten.</ta>
            <ta e="T268" id="Seg_6828" s="T250">"Schnell, unter den Schlitten!", sagte eine von beiden, machte "tschip" und kroch unter den mit kleinen Nägeln versehenen Boden, ihre Freunden machte es ihr nach und kroch unter den Balok.</ta>
            <ta e="T280" id="Seg_6829" s="T268">Zu der Zeit flog der Flieger gerade sehr niederig, um die Stangenzelte anzusehen.</ta>
            <ta e="T293" id="Seg_6830" s="T280">Von unter dem Balok schauten (…) die Lederschuhe der alten Frauen hervor, ohne sich zu bewegen.</ta>
            <ta e="T300" id="Seg_6831" s="T293">Am Ufer sprangen die alten Männer und winkten dem Flugzeug mit ihren Mützen.</ta>
            <ta e="T308" id="Seg_6832" s="T300">Sie freuen sich offenbar, dass das Flugzeug das erste Mal gut in die Tundra gekommen ist.</ta>
            <ta e="T319" id="Seg_6833" s="T308">Sie wissen, dass die Sowjetmacht kommt, um zu schauen, wie die Rentierhirten und Fischer leben.</ta>
            <ta e="T326" id="Seg_6834" s="T319">Sie gingen auf den Hügel und wunderten sich - die Stangenzelte rauchen nicht.</ta>
            <ta e="T332" id="Seg_6835" s="T326">Über dem Platz liegt Stille.</ta>
            <ta e="T339" id="Seg_6836" s="T332">Weder bellen Hunde, noch sind menschliche Stimmen zu hören.</ta>
            <ta e="T343" id="Seg_6837" s="T339">"Was für Leute sind diese alten Frauen?</ta>
            <ta e="T350" id="Seg_6838" s="T343">Wissen sie nicht, dass wir kommen?", sagte der alte Onoo.</ta>
            <ta e="T358" id="Seg_6839" s="T350">"Wo seid ihr, Mitbewohner", schrie ein Bewohner.</ta>
            <ta e="T366" id="Seg_6840" s="T358">"Oh, helft, macht uns los, wir sterben", erklang unter der Erde ein Schrei.</ta>
            <ta e="T373" id="Seg_6841" s="T366">"Was ist das, sind sie verrückt geworden?", sagte Onoo und schaute sich um.</ta>
            <ta e="T378" id="Seg_6842" s="T373">Er schaute unter die Schlittenhäuser.</ta>
            <ta e="T381" id="Seg_6843" s="T378">Was ist das denn Merkwürdiges?</ta>
            <ta e="T386" id="Seg_6844" s="T381">Die Beine der alten Frauen lagen zitternd dort.</ta>
            <ta e="T390" id="Seg_6845" s="T386">Als er schaut, versuchen sie offenbar, hinauszukommen.</ta>
            <ta e="T393" id="Seg_6846" s="T390">Kleine Nägel halten ihre Kleidung fest.</ta>
            <ta e="T395" id="Seg_6847" s="T393">Warum lassen sie [sie] los?</ta>
            <ta e="T401" id="Seg_6848" s="T395">Sie lassen sie weder hinein- noch hinausgehen.</ta>
            <ta e="T407" id="Seg_6849" s="T401">Die Schreie "oh nein" werden mehr, es kommt Stöhnen hinzu.</ta>
            <ta e="T418" id="Seg_6850" s="T407">Die alten Männer waren vor Verwunderung zu nichts in der Lage, sie blieben stumm.</ta>
            <ta e="T421" id="Seg_6851" s="T418">Sie befreiten die alten Frauen einzeln.</ta>
            <ta e="T425" id="Seg_6852" s="T421">Sie bekommen wieder ein Gesicht.</ta>
            <ta e="T432" id="Seg_6853" s="T425">Ihre Haare bedeckten ihre Augen, ihre Lederschuhe wurden zerquetscht.</ta>
            <ta e="T440" id="Seg_6854" s="T432">Ihr Aussehen änderte sich: Sie waren wie aus der Erde gekommene Polarfuchsjunge.</ta>
            <ta e="T447" id="Seg_6855" s="T440">"Hä, was ist passiert", fragten sie einzelnd. </ta>
            <ta e="T455" id="Seg_6856" s="T447">"Ruhe, ein unbekannter Vogel ist vorbeigekommen", flüsterten die beiden alten Frauen.</ta>
            <ta e="T460" id="Seg_6857" s="T455">"Oh", als ob er es nicht wüsste, stimmte Onoo zu.</ta>
            <ta e="T469" id="Seg_6858" s="T460">"Erzählt doch, was für einen Traum träumtet ihr, als ihr unter dem Schlittenhaus lagt?"</ta>
            <ta e="T473" id="Seg_6859" s="T469">"Was ist mit dem alten Mann?</ta>
            <ta e="T480" id="Seg_6860" s="T473">Im Alter lässt sein Verstand wohl nach", schrieen die beiden Angsthasen.</ta>
            <ta e="T486" id="Seg_6861" s="T480">Nun, die Angsthasen fingen doch an zu erzählen.</ta>
            <ta e="T493" id="Seg_6862" s="T486">"Wisst ihr denn nicht, dass sie die Zeiten geändert haben, dass ein Vögelchen der Sowjets kommt?</ta>
            <ta e="T498" id="Seg_6863" s="T493">Das kommt geflogen, um euch zu helfen.</ta>
            <ta e="T506" id="Seg_6864" s="T498">Damit die Dolganen satt und reich leben", sagten die beiden Menschen.</ta>
            <ta e="T519" id="Seg_6865" s="T506">Die alten Frauen konnten nicht mehr denken und sagten nichts, als ob sie sich schämten, latschten sie nach Hause.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T14" id="Seg_6866" s="T0">Советские времена когда впервые наступали, в Курья называемом посёлке каждое лето рыбачили долганский народ (долганы).</ta>
            <ta e="T19" id="Seg_6867" s="T14">С вертикально расположенными глазами (не знающий человек) часто не будет доходить до этого посёлка.</ta>
            <ta e="T23" id="Seg_6868" s="T19">Так далеко, где рыбачат земля (место).</ta>
            <ta e="T41" id="Seg_6869" s="T23">Когда-нибудь кто-нибудь приедет летом на ближайшие (прогнившие места) новости рассказать о своей жизни, чтобы узнать, как рыбачат Курьи жители.</ta>
            <ta e="T50" id="Seg_6870" s="T41">Однажды летом старики рано утром поплыли биски посмотреть.</ta>
            <ta e="T55" id="Seg_6871" s="T50">Женщины, детей гурьба дома сидели.</ta>
            <ta e="T61" id="Seg_6872" s="T55">Кто рыбу чистит, кто юколу сушит.</ta>
            <ta e="T72" id="Seg_6873" s="T61">Из них две старушки, мужья пока не пришли, от нартяных домов нюки латают, оказывается.</ta>
            <ta e="T76" id="Seg_6874" s="T72">Жаркий (очень тёплый) день стоял.</ta>
            <ta e="T86" id="Seg_6875" s="T76">У Дудупты реки на воде штилевой светлого грома облака в ряд смотрятся.</ta>
            <ta e="T92" id="Seg_6876" s="T86">У комара бедненького крылышек подсыхать жара стояла.</ta>
            <ta e="T100" id="Seg_6877" s="T92">Старушки те, несмотря на это, что руки заняты, отмахиваются.</ta>
            <ta e="T105" id="Seg_6878" s="T100">Между делом постоянно шитьём заняты.</ta>
            <ta e="T112" id="Seg_6879" s="T105">Иголок их звон об жёсткие шкуры как трещат, из дали слышно. </ta>
            <ta e="T120" id="Seg_6880" s="T112">Вот, когда работали сидели, земля почему-то загудела, оказывается.</ta>
            <ta e="T123" id="Seg_6881" s="T120">"Погоди – сказала одна.</ta>
            <ta e="T126" id="Seg_6882" s="T123">– Почему земля наша гудит?</ta>
            <ta e="T131" id="Seg_6883" s="T126">Почему грома звук такой может быть?</ta>
            <ta e="T137" id="Seg_6884" s="T131">Что-то удивительное случилось, послушай старуха".</ta>
            <ta e="T155" id="Seg_6885" s="T137">Кристина, старая, волосы с сединой старуха, во рту ни одного зуба нет, слыхом не слыхала платок свой с головы сорвав, послушала.</ta>
            <ta e="T170" id="Seg_6886" s="T155">Тут же, две бабки в небо подняв взор, как испуганный олень, странный звук слушая, стояли. </ta>
            <ta e="T179" id="Seg_6887" s="T170">Комариные укусы даже не замечая, долго просидели, рты разинув.</ta>
            <ta e="T184" id="Seg_6888" s="T179">Рядом шкурок лоскутки то там, то тут лежат.</ta>
            <ta e="T189" id="Seg_6889" s="T184">Привязанные их трубки в руках дымятся.</ta>
            <ta e="T194" id="Seg_6890" s="T189">Словно в ожидании (тихо) просидели.</ta>
            <ta e="T199" id="Seg_6891" s="T194">И правда, не разу не слыханный шум прогудил.</ta>
            <ta e="T212" id="Seg_6892" s="T199">"Ой, ой умираем!" – проговаривая, одна бабка подскочила, в сторону леса посмотрела.</ta>
            <ta e="T217" id="Seg_6893" s="T212">"Вот, летит великая птица.</ta>
            <ta e="T225" id="Seg_6894" s="T217">Смотри, вон краснеет, его спины звук, оказывается.</ta>
            <ta e="T234" id="Seg_6895" s="T225">Господи, я не говорила и не видела пусть будет," – проговаривая, бабка ушла.</ta>
            <ta e="T242" id="Seg_6896" s="T234">"О, что за птица, говоришь" – проговаривая, подружка тоже посмотрела.</ta>
            <ta e="T248" id="Seg_6897" s="T242">Под ногами земля будто зашевелилась.</ta>
            <ta e="T250" id="Seg_6898" s="T248">Шесты (чумов) задрожали.</ta>
            <ta e="T268" id="Seg_6899" s="T250">"Быстрее, под нарту (ляжем)!" – промолвив, другая прошмыгнула с торчащими гвоздями под пол, подруга за ней повторя, полезла под балок. </ta>
            <ta e="T280" id="Seg_6900" s="T268">В это время, лётчик впервые чумы чтоб посмотреть, низко очень пролетел.</ta>
            <ta e="T293" id="Seg_6901" s="T280">Из-под нартяного дома и (балка ?) притихших старушек замшевые сапоги, не шевелясь, вытянуто лежали.</ta>
            <ta e="T300" id="Seg_6902" s="T293">На берегу старики, подпрыгивая, шапками самолёту помахивая стояли.</ta>
            <ta e="T308" id="Seg_6903" s="T300">К их радости самолёта первый прилёт в тундре хорошо, оказывается.</ta>
            <ta e="T319" id="Seg_6904" s="T308">Знают, советская власть узнавать будет, как оленеводы, рыбаки поживают чтобы смотреть.</ta>
            <ta e="T326" id="Seg_6905" s="T319">На горы вершину забравшись, удивились – чумы не дымятся.</ta>
            <ta e="T332" id="Seg_6906" s="T326">Над местностью -- безмолная тишина.</ta>
            <ta e="T339" id="Seg_6907" s="T332">Собака даже не лает, человека даже голос не слышно.</ta>
            <ta e="T343" id="Seg_6908" s="T339">"Что за народ эти старушки.</ta>
            <ta e="T350" id="Seg_6909" s="T343">Не знают, что ли о нашем приходе?" – сказал Ануфрий старик.</ta>
            <ta e="T358" id="Seg_6910" s="T350">"Где вы есть, домашние", – крикнул гость.</ta>
            <ta e="T366" id="Seg_6911" s="T358">"Ой, помогите, разъедините, умираем", – из-под земли крик донёсся.</ta>
            <ta e="T373" id="Seg_6912" s="T366">"Что это, с ума сошли что-ли?" – проговорив, Ануфрий осмотрелся.</ta>
            <ta e="T378" id="Seg_6913" s="T373">Он под нартяными домами (балками) посмотрел.</ta>
            <ta e="T381" id="Seg_6914" s="T378">Что за чертовщина?</ta>
            <ta e="T386" id="Seg_6915" s="T381">Старушечьи ноги там, болтаясь, лежали.</ta>
            <ta e="T390" id="Seg_6916" s="T386">Как посмотрит, выйти пытаются, кажется.</ta>
            <ta e="T393" id="Seg_6917" s="T390">Гвозди за одежду зацепились.</ta>
            <ta e="T395" id="Seg_6918" s="T393">Почему отпустят?</ta>
            <ta e="T401" id="Seg_6919" s="T395">И вовнутрь не пускают, и наружу не выпускают.</ta>
            <ta e="T407" id="Seg_6920" s="T401">"Ой" крики увеличивались, стоны прибавлялись.</ta>
            <ta e="T418" id="Seg_6921" s="T407">Старики что бы подумать, удивляясь, без слов остались. </ta>
            <ta e="T421" id="Seg_6922" s="T418">Старушек по одной освободили.</ta>
            <ta e="T425" id="Seg_6923" s="T421">К ним лицо придёт разве (на них лиц не было).</ta>
            <ta e="T432" id="Seg_6924" s="T425">Волосы на глаза налезли, кожаные сапоги в гармошку скатились.</ta>
            <ta e="T440" id="Seg_6925" s="T432">Внешний вид изменился: как из норы вышедшые на песца дитёнышей похожи стали.</ta>
            <ta e="T447" id="Seg_6926" s="T440">"Ну, что случилось" – разом спросили вместе.</ta>
            <ta e="T455" id="Seg_6927" s="T447">"Тихо, незнакомая птица пролетела," – пропищали обе бабки.</ta>
            <ta e="T460" id="Seg_6928" s="T455">"О-o" – не зная будто, Ануфрий соглашался.</ta>
            <ta e="T469" id="Seg_6929" s="T460">"Ну рассказывайте, что за сон приснился под нартяным домом лёжа?" </ta>
            <ta e="T473" id="Seg_6930" s="T469">"Это что происходит со стариком?</ta>
            <ta e="T480" id="Seg_6931" s="T473">От старости ум у него укорачивается, кажется" – прокричали две трусихи.</ta>
            <ta e="T486" id="Seg_6932" s="T480">Хи, заячьи сердца ещё разговорчивые стали.</ta>
            <ta e="T493" id="Seg_6933" s="T486">"Не знаете разве, что времена изменились, что советов птица прилетает?</ta>
            <ta e="T498" id="Seg_6934" s="T493">Этот, вам чтобы помочь, летает.</ta>
            <ta e="T506" id="Seg_6935" s="T498">Тундры жители чтобы сыто и богато жили," – промолвили два человека.</ta>
            <ta e="T519" id="Seg_6936" s="T506">Старушки ничего не придумав, без слов, постыдившись будто, в сторону дома, сутулясь, пошли. </ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T50" id="Seg_6937" s="T41">[DCh]: A common fishing method among the Dolgans is to place nets across small tributaries, therefore the old men went checking the tributaries.</ta>
            <ta e="T194" id="Seg_6938" s="T189">[DCh]: The morphosyntax of the sentence is not clear.</ta>
            <ta e="T395" id="Seg_6939" s="T393">[DCh]: Negation is somehow missing in the sentence.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T520" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
