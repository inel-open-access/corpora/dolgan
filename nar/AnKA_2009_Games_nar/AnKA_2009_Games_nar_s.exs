<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8E9260C7-716A-F21F-60D1-77966D548290">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnKA_2009_Games_nar</transcription-name>
         <referenced-file url="AnKA_2009_Games_nar.wav" />
         <referenced-file url="AnKA_2009_Games_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\AnKA_2009_Games_nar\AnKA_2009_Games_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">123</ud-information>
            <ud-information attribute-name="# HIAT:w">106</ud-information>
            <ud-information attribute-name="# e">106</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnKA">
            <abbreviation>AnKA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.4965555555555556" type="appl" />
         <tli id="T3" time="0.9931111111111112" type="appl" />
         <tli id="T4" time="1.4896666666666667" type="appl" />
         <tli id="T5" time="1.9862222222222223" type="appl" />
         <tli id="T6" time="2.4827777777777778" type="appl" />
         <tli id="T7" time="2.9793333333333334" type="appl" />
         <tli id="T8" time="3.475888888888889" type="appl" />
         <tli id="T9" time="3.9724444444444447" type="appl" />
         <tli id="T10" time="4.469" type="appl" />
         <tli id="T11" time="4.898086956521739" type="appl" />
         <tli id="T12" time="5.327173913043478" type="appl" />
         <tli id="T13" time="5.756260869565217" type="appl" />
         <tli id="T14" time="6.185347826086957" type="appl" />
         <tli id="T15" time="6.614434782608695" type="appl" />
         <tli id="T16" time="7.043521739130435" type="appl" />
         <tli id="T17" time="7.472608695652174" type="appl" />
         <tli id="T18" time="7.901695652173913" type="appl" />
         <tli id="T19" time="8.330782608695653" type="appl" />
         <tli id="T20" time="8.759869565217391" type="appl" />
         <tli id="T21" time="9.18895652173913" type="appl" />
         <tli id="T22" time="9.61804347826087" type="appl" />
         <tli id="T23" time="10.04713043478261" type="appl" />
         <tli id="T24" time="10.476217391304349" type="appl" />
         <tli id="T25" time="10.905304347826087" type="appl" />
         <tli id="T26" time="11.334391304347825" type="appl" />
         <tli id="T27" time="11.763478260869565" type="appl" />
         <tli id="T28" time="12.192565217391305" type="appl" />
         <tli id="T29" time="12.621652173913045" type="appl" />
         <tli id="T30" time="13.050739130434781" type="appl" />
         <tli id="T31" time="13.479826086956521" type="appl" />
         <tli id="T32" time="13.908913043478261" type="appl" />
         <tli id="T35" time="14.339" type="appl" />
         <tli id="T36" time="14.8389" type="appl" />
         <tli id="T37" time="15.3388" type="appl" />
         <tli id="T38" time="15.8387" type="appl" />
         <tli id="T39" time="16.3386" type="appl" />
         <tli id="T40" time="16.8385" type="appl" />
         <tli id="T41" time="17.3384" type="appl" />
         <tli id="T42" time="17.8383" type="appl" />
         <tli id="T43" time="18.3382" type="appl" />
         <tli id="T44" time="18.8381" type="appl" />
         <tli id="T45" time="19.338" type="appl" />
         <tli id="T46" time="19.8379" type="appl" />
         <tli id="T47" time="20.3378" type="appl" />
         <tli id="T48" time="20.837699999999998" type="appl" />
         <tli id="T49" time="21.337600000000002" type="appl" />
         <tli id="T50" time="21.8375" type="appl" />
         <tli id="T51" time="22.3374" type="appl" />
         <tli id="T52" time="22.8373" type="appl" />
         <tli id="T53" time="23.3372" type="appl" />
         <tli id="T54" time="23.8371" type="appl" />
         <tli id="T55" time="24.337" type="appl" />
         <tli id="T56" time="25.20875" type="appl" />
         <tli id="T57" time="26.0805" type="appl" />
         <tli id="T58" time="26.95225" type="appl" />
         <tli id="T59" time="27.823999999999998" type="appl" />
         <tli id="T60" time="28.69575" type="appl" />
         <tli id="T61" time="29.5675" type="appl" />
         <tli id="T62" time="30.43925" type="appl" />
         <tli id="T63" time="31.311" type="appl" />
         <tli id="T64" time="31.907222222222224" type="appl" />
         <tli id="T65" time="32.50344444444445" type="appl" />
         <tli id="T66" time="33.099666666666664" type="appl" />
         <tli id="T67" time="33.69588888888889" type="appl" />
         <tli id="T68" time="34.29211111111111" type="appl" />
         <tli id="T69" time="34.888333333333335" type="appl" />
         <tli id="T70" time="35.48455555555556" type="appl" />
         <tli id="T71" time="36.080777777777776" type="appl" />
         <tli id="T72" time="36.677" type="appl" />
         <tli id="T73" time="38.30566666666667" type="appl" />
         <tli id="T74" time="39.934333333333335" type="appl" />
         <tli id="T75" time="41.563" type="appl" />
         <tli id="T76" time="42.150875" type="appl" />
         <tli id="T77" time="42.73875" type="appl" />
         <tli id="T78" time="43.326625" type="appl" />
         <tli id="T79" time="43.914500000000004" type="appl" />
         <tli id="T80" time="44.502375" type="appl" />
         <tli id="T81" time="45.090250000000005" type="appl" />
         <tli id="T82" time="45.678125" type="appl" />
         <tli id="T83" time="46.266000000000005" type="appl" />
         <tli id="T84" time="46.853875" type="appl" />
         <tli id="T85" time="47.44175" type="appl" />
         <tli id="T86" time="48.029625" type="appl" />
         <tli id="T87" time="48.6175" type="appl" />
         <tli id="T88" time="49.205375000000004" type="appl" />
         <tli id="T89" time="49.79325" type="appl" />
         <tli id="T90" time="50.381125" type="appl" />
         <tli id="T91" time="50.969" type="appl" />
         <tli id="T92" time="51.59463636363636" type="appl" />
         <tli id="T93" time="52.22027272727273" type="appl" />
         <tli id="T94" time="52.84590909090909" type="appl" />
         <tli id="T95" time="53.471545454545456" type="appl" />
         <tli id="T96" time="54.09718181818182" type="appl" />
         <tli id="T97" time="54.722818181818184" type="appl" />
         <tli id="T0" time="55.080324675324675" type="intp" />
         <tli id="T98" time="55.348454545454544" type="appl" />
         <tli id="T99" time="55.97409090909091" type="appl" />
         <tli id="T100" time="56.59972727272727" type="appl" />
         <tli id="T101" time="57.22536363636364" type="appl" />
         <tli id="T102" time="57.851" type="appl" />
         <tli id="T103" time="59.3145" type="appl" />
         <tli id="T104" time="60.778" type="appl" />
         <tli id="T105" time="62.2415" type="appl" />
         <tli id="T106" time="63.705000000000005" type="appl" />
         <tli id="T107" time="65.16850000000001" type="appl" />
         <tli id="T108" time="66.632" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AnKA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T108" id="Seg_0" n="sc" s="T1">
               <ts e="T10" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Dʼe</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ke</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">begeheː</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">bihigi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">kimŋe</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">barbɨt</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">etibit</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T8">bu͡o</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_28" n="HIAT:w" s="T9">poxottɨː</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_32" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">Motorɨnan</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">barbɨt</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">etibit</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">barbɨt</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_47" n="HIAT:w" s="T14">etibit</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_50" n="HIAT:w" s="T15">ogolor</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_53" n="HIAT:w" s="T16">onton</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_56" n="HIAT:w" s="T17">maːmam</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_58" n="HIAT:ip">(</nts>
                  <ts e="T19" id="Seg_60" n="HIAT:w" s="T18">Vik-</ts>
                  <nts id="Seg_61" n="HIAT:ip">)</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_64" n="HIAT:w" s="T19">Vikanɨ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_67" n="HIAT:w" s="T20">kɨtta</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_70" n="HIAT:w" s="T21">barbɨt</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_73" n="HIAT:w" s="T22">samɨj</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_76" n="HIAT:w" s="T23">mladšij</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_79" n="HIAT:w" s="T24">ogo</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_82" n="HIAT:w" s="T25">baltɨbɨt</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_85" n="HIAT:w" s="T26">ke</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_88" n="HIAT:w" s="T27">emi͡e</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_91" n="HIAT:w" s="T28">bihigini</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_94" n="HIAT:w" s="T29">gɨtta</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_97" n="HIAT:w" s="T30">motorɨnan</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_100" n="HIAT:w" s="T31">barsɨbɨt</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_103" n="HIAT:w" s="T32">ete</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_107" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_109" n="HIAT:w" s="T35">A</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_112" n="HIAT:w" s="T36">tetja</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_115" n="HIAT:w" s="T37">Rozalaːk</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_118" n="HIAT:w" s="T38">tu͡oktaːk</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_121" n="HIAT:w" s="T39">ulakan</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_124" n="HIAT:w" s="T40">kihiler</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_127" n="HIAT:w" s="T41">po</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_130" n="HIAT:w" s="T42">beregu</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_133" n="HIAT:w" s="T43">bartɨlara</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_136" n="HIAT:w" s="T44">hatɨː</ts>
                  <nts id="Seg_137" n="HIAT:ip">,</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_140" n="HIAT:w" s="T45">onton</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_143" n="HIAT:w" s="T46">bihigi</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_146" n="HIAT:w" s="T47">onno</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_149" n="HIAT:w" s="T48">onnʼuːr</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_152" n="HIAT:w" s="T49">etibit</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_156" n="HIAT:w" s="T50">palatka</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_159" n="HIAT:w" s="T51">tu͡ok</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_162" n="HIAT:w" s="T52">uːrummupput</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_166" n="HIAT:w" s="T53">onnʼuːr</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_169" n="HIAT:w" s="T54">etibit</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_173" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_175" n="HIAT:w" s="T55">Kim</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_178" n="HIAT:w" s="T56">skalaːlar</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_181" n="HIAT:w" s="T57">ürdüger</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_184" n="HIAT:w" s="T58">turar</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_187" n="HIAT:w" s="T59">etibit</ts>
                  <nts id="Seg_188" n="HIAT:ip">,</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_191" n="HIAT:w" s="T60">gribɨ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_194" n="HIAT:w" s="T61">komunar</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_197" n="HIAT:w" s="T62">etibit</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_201" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_203" n="HIAT:w" s="T63">Kim</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_206" n="HIAT:w" s="T64">laptu</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_209" n="HIAT:w" s="T65">onnʼuːr</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_212" n="HIAT:w" s="T66">etibit</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_215" n="HIAT:w" s="T67">ulakan</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_218" n="HIAT:w" s="T68">kihileri</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_221" n="HIAT:w" s="T69">kɨtta</ts>
                  <nts id="Seg_222" n="HIAT:ip">,</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_225" n="HIAT:w" s="T70">onton</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_228" n="HIAT:w" s="T71">ke</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_232" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_234" n="HIAT:w" s="T72">Lodkannan</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_237" n="HIAT:w" s="T73">katatsalɨːr</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_240" n="HIAT:w" s="T74">etibit</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_244" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_246" n="HIAT:w" s="T75">Onton</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_249" n="HIAT:w" s="T76">ke</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_252" n="HIAT:w" s="T77">laptu</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_255" n="HIAT:w" s="T78">onnʼuːr</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_258" n="HIAT:w" s="T79">etibit</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_261" n="HIAT:w" s="T80">otto</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_264" n="HIAT:w" s="T81">ahaːbɨppɨt</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_267" n="HIAT:w" s="T82">emi͡e</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_270" n="HIAT:w" s="T83">onnʼoːbupput</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_273" n="HIAT:w" s="T84">otto</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_276" n="HIAT:w" s="T85">palatka</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_279" n="HIAT:w" s="T86">ihiger</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_282" n="HIAT:w" s="T87">ogonu</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_285" n="HIAT:w" s="T88">kɨtta</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_288" n="HIAT:w" s="T89">onnʼuːr</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_291" n="HIAT:w" s="T90">etibit</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_295" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_297" n="HIAT:w" s="T91">Dʼi͡e</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_300" n="HIAT:w" s="T92">onnʼuːr</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_303" n="HIAT:w" s="T93">etibit</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_306" n="HIAT:w" s="T94">otto</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_309" n="HIAT:w" s="T95">ogoloru</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_312" n="HIAT:w" s="T96">kɨtta</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_315" n="HIAT:w" s="T97">otdelʼno</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_318" n="HIAT:w" s="T0">laptu</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_321" n="HIAT:w" s="T98">onnʼuːr</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_324" n="HIAT:w" s="T99">etibit</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_327" n="HIAT:w" s="T100">onton</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_330" n="HIAT:w" s="T101">onton</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_334" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_336" n="HIAT:w" s="T102">Laptu</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_339" n="HIAT:w" s="T103">onnʼuːr</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_342" n="HIAT:w" s="T104">etibit</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_345" n="HIAT:w" s="T105">batɨ͡alaha</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_348" n="HIAT:w" s="T106">onnʼuːr</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_351" n="HIAT:w" s="T107">etibit</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T108" id="Seg_354" n="sc" s="T1">
               <ts e="T2" id="Seg_356" n="e" s="T1">Dʼe </ts>
               <ts e="T3" id="Seg_358" n="e" s="T2">ke </ts>
               <ts e="T4" id="Seg_360" n="e" s="T3">begeheː </ts>
               <ts e="T5" id="Seg_362" n="e" s="T4">bihigi </ts>
               <ts e="T6" id="Seg_364" n="e" s="T5">kimŋe </ts>
               <ts e="T7" id="Seg_366" n="e" s="T6">barbɨt </ts>
               <ts e="T8" id="Seg_368" n="e" s="T7">etibit </ts>
               <ts e="T9" id="Seg_370" n="e" s="T8">bu͡o </ts>
               <ts e="T10" id="Seg_372" n="e" s="T9">poxottɨː. </ts>
               <ts e="T11" id="Seg_374" n="e" s="T10">Motorɨnan </ts>
               <ts e="T12" id="Seg_376" n="e" s="T11">barbɨt </ts>
               <ts e="T13" id="Seg_378" n="e" s="T12">etibit, </ts>
               <ts e="T14" id="Seg_380" n="e" s="T13">barbɨt </ts>
               <ts e="T15" id="Seg_382" n="e" s="T14">etibit </ts>
               <ts e="T16" id="Seg_384" n="e" s="T15">ogolor </ts>
               <ts e="T17" id="Seg_386" n="e" s="T16">onton </ts>
               <ts e="T18" id="Seg_388" n="e" s="T17">maːmam </ts>
               <ts e="T19" id="Seg_390" n="e" s="T18">(Vik-) </ts>
               <ts e="T20" id="Seg_392" n="e" s="T19">Vikanɨ </ts>
               <ts e="T21" id="Seg_394" n="e" s="T20">kɨtta </ts>
               <ts e="T22" id="Seg_396" n="e" s="T21">barbɨt </ts>
               <ts e="T23" id="Seg_398" n="e" s="T22">samɨj </ts>
               <ts e="T24" id="Seg_400" n="e" s="T23">mladšij </ts>
               <ts e="T25" id="Seg_402" n="e" s="T24">ogo </ts>
               <ts e="T26" id="Seg_404" n="e" s="T25">baltɨbɨt </ts>
               <ts e="T27" id="Seg_406" n="e" s="T26">ke </ts>
               <ts e="T28" id="Seg_408" n="e" s="T27">emi͡e </ts>
               <ts e="T29" id="Seg_410" n="e" s="T28">bihigini </ts>
               <ts e="T30" id="Seg_412" n="e" s="T29">gɨtta </ts>
               <ts e="T31" id="Seg_414" n="e" s="T30">motorɨnan </ts>
               <ts e="T32" id="Seg_416" n="e" s="T31">barsɨbɨt </ts>
               <ts e="T35" id="Seg_418" n="e" s="T32">ete. </ts>
               <ts e="T36" id="Seg_420" n="e" s="T35">A </ts>
               <ts e="T37" id="Seg_422" n="e" s="T36">tetja </ts>
               <ts e="T38" id="Seg_424" n="e" s="T37">Rozalaːk </ts>
               <ts e="T39" id="Seg_426" n="e" s="T38">tu͡oktaːk </ts>
               <ts e="T40" id="Seg_428" n="e" s="T39">ulakan </ts>
               <ts e="T41" id="Seg_430" n="e" s="T40">kihiler </ts>
               <ts e="T42" id="Seg_432" n="e" s="T41">po </ts>
               <ts e="T43" id="Seg_434" n="e" s="T42">beregu </ts>
               <ts e="T44" id="Seg_436" n="e" s="T43">bartɨlara </ts>
               <ts e="T45" id="Seg_438" n="e" s="T44">hatɨː, </ts>
               <ts e="T46" id="Seg_440" n="e" s="T45">onton </ts>
               <ts e="T47" id="Seg_442" n="e" s="T46">bihigi </ts>
               <ts e="T48" id="Seg_444" n="e" s="T47">onno </ts>
               <ts e="T49" id="Seg_446" n="e" s="T48">onnʼuːr </ts>
               <ts e="T50" id="Seg_448" n="e" s="T49">etibit, </ts>
               <ts e="T51" id="Seg_450" n="e" s="T50">palatka </ts>
               <ts e="T52" id="Seg_452" n="e" s="T51">tu͡ok </ts>
               <ts e="T53" id="Seg_454" n="e" s="T52">uːrummupput, </ts>
               <ts e="T54" id="Seg_456" n="e" s="T53">onnʼuːr </ts>
               <ts e="T55" id="Seg_458" n="e" s="T54">etibit. </ts>
               <ts e="T56" id="Seg_460" n="e" s="T55">Kim </ts>
               <ts e="T57" id="Seg_462" n="e" s="T56">skalaːlar </ts>
               <ts e="T58" id="Seg_464" n="e" s="T57">ürdüger </ts>
               <ts e="T59" id="Seg_466" n="e" s="T58">turar </ts>
               <ts e="T60" id="Seg_468" n="e" s="T59">etibit, </ts>
               <ts e="T61" id="Seg_470" n="e" s="T60">gribɨ </ts>
               <ts e="T62" id="Seg_472" n="e" s="T61">komunar </ts>
               <ts e="T63" id="Seg_474" n="e" s="T62">etibit. </ts>
               <ts e="T64" id="Seg_476" n="e" s="T63">Kim </ts>
               <ts e="T65" id="Seg_478" n="e" s="T64">laptu </ts>
               <ts e="T66" id="Seg_480" n="e" s="T65">onnʼuːr </ts>
               <ts e="T67" id="Seg_482" n="e" s="T66">etibit </ts>
               <ts e="T68" id="Seg_484" n="e" s="T67">ulakan </ts>
               <ts e="T69" id="Seg_486" n="e" s="T68">kihileri </ts>
               <ts e="T70" id="Seg_488" n="e" s="T69">kɨtta, </ts>
               <ts e="T71" id="Seg_490" n="e" s="T70">onton </ts>
               <ts e="T72" id="Seg_492" n="e" s="T71">ke. </ts>
               <ts e="T73" id="Seg_494" n="e" s="T72">Lodkannan </ts>
               <ts e="T74" id="Seg_496" n="e" s="T73">katatsalɨːr </ts>
               <ts e="T75" id="Seg_498" n="e" s="T74">etibit. </ts>
               <ts e="T76" id="Seg_500" n="e" s="T75">Onton </ts>
               <ts e="T77" id="Seg_502" n="e" s="T76">ke </ts>
               <ts e="T78" id="Seg_504" n="e" s="T77">laptu </ts>
               <ts e="T79" id="Seg_506" n="e" s="T78">onnʼuːr </ts>
               <ts e="T80" id="Seg_508" n="e" s="T79">etibit </ts>
               <ts e="T81" id="Seg_510" n="e" s="T80">otto </ts>
               <ts e="T82" id="Seg_512" n="e" s="T81">ahaːbɨppɨt </ts>
               <ts e="T83" id="Seg_514" n="e" s="T82">emi͡e </ts>
               <ts e="T84" id="Seg_516" n="e" s="T83">onnʼoːbupput </ts>
               <ts e="T85" id="Seg_518" n="e" s="T84">otto </ts>
               <ts e="T86" id="Seg_520" n="e" s="T85">palatka </ts>
               <ts e="T87" id="Seg_522" n="e" s="T86">ihiger </ts>
               <ts e="T88" id="Seg_524" n="e" s="T87">ogonu </ts>
               <ts e="T89" id="Seg_526" n="e" s="T88">kɨtta </ts>
               <ts e="T90" id="Seg_528" n="e" s="T89">onnʼuːr </ts>
               <ts e="T91" id="Seg_530" n="e" s="T90">etibit. </ts>
               <ts e="T92" id="Seg_532" n="e" s="T91">Dʼi͡e </ts>
               <ts e="T93" id="Seg_534" n="e" s="T92">onnʼuːr </ts>
               <ts e="T94" id="Seg_536" n="e" s="T93">etibit </ts>
               <ts e="T95" id="Seg_538" n="e" s="T94">otto </ts>
               <ts e="T96" id="Seg_540" n="e" s="T95">ogoloru </ts>
               <ts e="T97" id="Seg_542" n="e" s="T96">kɨtta </ts>
               <ts e="T0" id="Seg_544" n="e" s="T97">otdelʼno </ts>
               <ts e="T98" id="Seg_546" n="e" s="T0">laptu </ts>
               <ts e="T99" id="Seg_548" n="e" s="T98">onnʼuːr </ts>
               <ts e="T100" id="Seg_550" n="e" s="T99">etibit </ts>
               <ts e="T101" id="Seg_552" n="e" s="T100">onton </ts>
               <ts e="T102" id="Seg_554" n="e" s="T101">onton. </ts>
               <ts e="T103" id="Seg_556" n="e" s="T102">Laptu </ts>
               <ts e="T104" id="Seg_558" n="e" s="T103">onnʼuːr </ts>
               <ts e="T105" id="Seg_560" n="e" s="T104">etibit </ts>
               <ts e="T106" id="Seg_562" n="e" s="T105">batɨ͡alaha </ts>
               <ts e="T107" id="Seg_564" n="e" s="T106">onnʼuːr </ts>
               <ts e="T108" id="Seg_566" n="e" s="T107">etibit. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T10" id="Seg_567" s="T1">AnKA_2009_Games_nar.001 (001)</ta>
            <ta e="T35" id="Seg_568" s="T10">AnKA_2009_Games_nar.002 (002)</ta>
            <ta e="T55" id="Seg_569" s="T35">AnKA_2009_Games_nar.003 (003)</ta>
            <ta e="T63" id="Seg_570" s="T55">AnKA_2009_Games_nar.004 (004)</ta>
            <ta e="T72" id="Seg_571" s="T63">AnKA_2009_Games_nar.005 (005)</ta>
            <ta e="T75" id="Seg_572" s="T72">AnKA_2009_Games_nar.006 (006)</ta>
            <ta e="T91" id="Seg_573" s="T75">AnKA_2009_Games_nar.007 (007)</ta>
            <ta e="T102" id="Seg_574" s="T91">AnKA_2009_Games_nar.008 (008)</ta>
            <ta e="T108" id="Seg_575" s="T102">AnKA_2009_Games_nar.009 (009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T10" id="Seg_576" s="T1">дьэ кэ бэгэһэ биһиги кимӈэ барбыт этибит буо похотты.</ta>
            <ta e="T35" id="Seg_577" s="T10">моторынан барбыт этибит, барбыт этибит оголор онтон маамам Вик- Виканы кытта барбыт самый младший ого.. балтыбыт кэ эмиэ биһитта моторынан барсыбыт этэ.</ta>
            <ta e="T55" id="Seg_578" s="T35">а тетя Розалак туоктаак улакан киһилэр по берегу бартылара һаты, онтон биһиги онно онньуур этибит, палатка туок уруммуппут, онньуур этибит.</ta>
            <ta e="T63" id="Seg_579" s="T55">Ким скалаалар үрдүгэр турар этибит, грибы комунар этибит. </ta>
            <ta e="T72" id="Seg_580" s="T63">Ким лапту онньур этибит улакан киһилэри кытта, онтон кэ.</ta>
            <ta e="T75" id="Seg_581" s="T72">лодканнан катассалыр этибит. </ta>
            <ta e="T91" id="Seg_582" s="T75">онтон кэ лапту онньур этибит отто аһаабыппыт эмиэ онньообуппут отто палатка иһигэр огону кытта онньуур этибит. </ta>
            <ta e="T102" id="Seg_583" s="T91">дьиэ онньуур этибит отто оголору кытта отдельно лапту онньуур этибит онтон онтон. </ta>
            <ta e="T108" id="Seg_584" s="T102">лапту онньуур этибит батыэлаһа онньуур этибит. </ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T10" id="Seg_585" s="T1">Dʼe ke begeheː bihigi kimŋe barbɨt etibit bu͡o poxottɨː. </ta>
            <ta e="T35" id="Seg_586" s="T10">Motorɨnan barbɨt etibit, barbɨt etibit ogolor onton maːmam (Vik-) Vikanɨ kɨtta barbɨt samɨj mladšij ogo baltɨbɨt ke emi͡e bihigini gɨtta motorɨnan barsɨbɨt ete. </ta>
            <ta e="T55" id="Seg_587" s="T35">A tetja Rozalaːk tu͡oktaːk ulakan kihiler po beregu bartɨlara hatɨː, onton bihigi onno onnʼuːr etibit, palatka tu͡ok uːrummupput, onnʼuːr etibit. </ta>
            <ta e="T63" id="Seg_588" s="T55">Kim skalaːlar ürdüger turar etibit, gribɨ komunar etibit. </ta>
            <ta e="T72" id="Seg_589" s="T63">Kim laptu onnʼuːr etibit ulakan kihileri kɨtta, onton ke. </ta>
            <ta e="T75" id="Seg_590" s="T72">Lodkannan katatsalɨːr etibit. </ta>
            <ta e="T91" id="Seg_591" s="T75">Onton ke laptu onnʼuːr etibit otto ahaːbɨppɨt emi͡e onnʼoːbupput otto palatka ihiger ogonu kɨtta onnʼuːr etibit. </ta>
            <ta e="T102" id="Seg_592" s="T91">Dʼi͡e onnʼuːr etibit otto ogoloru kɨtta otdelʼnolaptu onnʼuːr etibit onton onton. </ta>
            <ta e="T108" id="Seg_593" s="T102">Laptu onnʼuːr etibit batɨ͡alaha onnʼuːr etibit. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_594" s="T1">dʼe</ta>
            <ta e="T3" id="Seg_595" s="T2">ke</ta>
            <ta e="T4" id="Seg_596" s="T3">begeheː</ta>
            <ta e="T5" id="Seg_597" s="T4">bihigi</ta>
            <ta e="T6" id="Seg_598" s="T5">kim-ŋe</ta>
            <ta e="T7" id="Seg_599" s="T6">bar-bɨt</ta>
            <ta e="T8" id="Seg_600" s="T7">e-ti-bit</ta>
            <ta e="T9" id="Seg_601" s="T8">bu͡o</ta>
            <ta e="T10" id="Seg_602" s="T9">poxot-t-ɨː</ta>
            <ta e="T11" id="Seg_603" s="T10">motor-ɨ-nan</ta>
            <ta e="T12" id="Seg_604" s="T11">bar-bɨt</ta>
            <ta e="T13" id="Seg_605" s="T12">e-ti-bit</ta>
            <ta e="T14" id="Seg_606" s="T13">bar-bɨt</ta>
            <ta e="T15" id="Seg_607" s="T14">e-ti-bit</ta>
            <ta e="T16" id="Seg_608" s="T15">ogo-lor</ta>
            <ta e="T17" id="Seg_609" s="T16">on-ton</ta>
            <ta e="T18" id="Seg_610" s="T17">maːma-m</ta>
            <ta e="T20" id="Seg_611" s="T19">Vika-nɨ</ta>
            <ta e="T21" id="Seg_612" s="T20">kɨtta</ta>
            <ta e="T22" id="Seg_613" s="T21">bar-bɨt</ta>
            <ta e="T25" id="Seg_614" s="T24">ogo</ta>
            <ta e="T26" id="Seg_615" s="T25">balt-ɨ-bɨt</ta>
            <ta e="T27" id="Seg_616" s="T26">ke</ta>
            <ta e="T28" id="Seg_617" s="T27">emi͡e</ta>
            <ta e="T29" id="Seg_618" s="T28">bihigi-ni</ta>
            <ta e="T30" id="Seg_619" s="T29">gɨtta</ta>
            <ta e="T31" id="Seg_620" s="T30">motor-ɨ-nan</ta>
            <ta e="T32" id="Seg_621" s="T31">bars-ɨ-bɨt</ta>
            <ta e="T35" id="Seg_622" s="T32">e-t-e</ta>
            <ta e="T38" id="Seg_623" s="T37">Roza-laːk</ta>
            <ta e="T39" id="Seg_624" s="T38">tu͡ok-taːk</ta>
            <ta e="T40" id="Seg_625" s="T39">ulakan</ta>
            <ta e="T41" id="Seg_626" s="T40">kihi-ler</ta>
            <ta e="T44" id="Seg_627" s="T43">bar-t-ɨ-lara</ta>
            <ta e="T45" id="Seg_628" s="T44">hatɨː</ta>
            <ta e="T46" id="Seg_629" s="T45">on-ton</ta>
            <ta e="T47" id="Seg_630" s="T46">bihigi</ta>
            <ta e="T48" id="Seg_631" s="T47">onno</ta>
            <ta e="T49" id="Seg_632" s="T48">onnʼuː-r</ta>
            <ta e="T50" id="Seg_633" s="T49">e-ti-bit</ta>
            <ta e="T52" id="Seg_634" s="T51">tu͡ok</ta>
            <ta e="T53" id="Seg_635" s="T52">uːr-u-m-mup-put</ta>
            <ta e="T54" id="Seg_636" s="T53">onnʼuː-r</ta>
            <ta e="T55" id="Seg_637" s="T54">e-ti-bit</ta>
            <ta e="T56" id="Seg_638" s="T55">kim</ta>
            <ta e="T57" id="Seg_639" s="T56">skalaː-lar</ta>
            <ta e="T58" id="Seg_640" s="T57">ürd-ü-ger</ta>
            <ta e="T59" id="Seg_641" s="T58">tur-Ar</ta>
            <ta e="T60" id="Seg_642" s="T59">e-ti-bit</ta>
            <ta e="T62" id="Seg_643" s="T61">komu-n-ar</ta>
            <ta e="T63" id="Seg_644" s="T62">e-ti-bit</ta>
            <ta e="T64" id="Seg_645" s="T63">kim</ta>
            <ta e="T66" id="Seg_646" s="T65">onnʼuː-r</ta>
            <ta e="T67" id="Seg_647" s="T66">e-ti-bit</ta>
            <ta e="T68" id="Seg_648" s="T67">ulakan</ta>
            <ta e="T69" id="Seg_649" s="T68">kihi-ler-i</ta>
            <ta e="T70" id="Seg_650" s="T69">kɨtta</ta>
            <ta e="T71" id="Seg_651" s="T70">on-ton</ta>
            <ta e="T72" id="Seg_652" s="T71">ke</ta>
            <ta e="T73" id="Seg_653" s="T72">lodka-nnan</ta>
            <ta e="T74" id="Seg_654" s="T73">katatsa-lɨː-r</ta>
            <ta e="T75" id="Seg_655" s="T74">e-ti-bit</ta>
            <ta e="T76" id="Seg_656" s="T75">on-ton</ta>
            <ta e="T77" id="Seg_657" s="T76">ke</ta>
            <ta e="T79" id="Seg_658" s="T78">onnʼuː-r</ta>
            <ta e="T80" id="Seg_659" s="T79">e-ti-bit</ta>
            <ta e="T81" id="Seg_660" s="T80">otto</ta>
            <ta e="T82" id="Seg_661" s="T81">ahaː-bɨp-pɨt</ta>
            <ta e="T83" id="Seg_662" s="T82">emi͡e</ta>
            <ta e="T84" id="Seg_663" s="T83">onnʼoː-bup-put</ta>
            <ta e="T85" id="Seg_664" s="T84">otto</ta>
            <ta e="T87" id="Seg_665" s="T86">ih-i-ger</ta>
            <ta e="T88" id="Seg_666" s="T87">ogo-nu</ta>
            <ta e="T89" id="Seg_667" s="T88">kɨtta</ta>
            <ta e="T90" id="Seg_668" s="T89">onnʼuː-r</ta>
            <ta e="T91" id="Seg_669" s="T90">e-ti-bit</ta>
            <ta e="T92" id="Seg_670" s="T91">dʼi͡e</ta>
            <ta e="T93" id="Seg_671" s="T92">onnʼuː-r</ta>
            <ta e="T94" id="Seg_672" s="T93">e-ti-bit</ta>
            <ta e="T95" id="Seg_673" s="T94">otto</ta>
            <ta e="T96" id="Seg_674" s="T95">ogo-lor-u</ta>
            <ta e="T97" id="Seg_675" s="T96">kɨtta</ta>
            <ta e="T99" id="Seg_676" s="T98">onnʼuː-r</ta>
            <ta e="T100" id="Seg_677" s="T99">e-ti-bit</ta>
            <ta e="T101" id="Seg_678" s="T100">on-ton</ta>
            <ta e="T102" id="Seg_679" s="T101">on-ton</ta>
            <ta e="T104" id="Seg_680" s="T103">onnʼuː-r</ta>
            <ta e="T105" id="Seg_681" s="T104">e-ti-bit</ta>
            <ta e="T106" id="Seg_682" s="T105">bat-ɨ͡al-a-h-a</ta>
            <ta e="T107" id="Seg_683" s="T106">onnʼuː-r</ta>
            <ta e="T108" id="Seg_684" s="T107">e-ti-bit</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_685" s="T1">dʼe</ta>
            <ta e="T3" id="Seg_686" s="T2">ka</ta>
            <ta e="T4" id="Seg_687" s="T3">begeheː</ta>
            <ta e="T5" id="Seg_688" s="T4">bihigi</ta>
            <ta e="T6" id="Seg_689" s="T5">kim-GA</ta>
            <ta e="T7" id="Seg_690" s="T6">bar-BIT</ta>
            <ta e="T8" id="Seg_691" s="T7">e-TI-BIt</ta>
            <ta e="T9" id="Seg_692" s="T8">bu͡o</ta>
            <ta e="T10" id="Seg_693" s="T9">poxot-LAː-A</ta>
            <ta e="T11" id="Seg_694" s="T10">motor-I-nAn</ta>
            <ta e="T12" id="Seg_695" s="T11">bar-BIT</ta>
            <ta e="T13" id="Seg_696" s="T12">e-TI-BIt</ta>
            <ta e="T14" id="Seg_697" s="T13">bar-BIT</ta>
            <ta e="T15" id="Seg_698" s="T14">e-TI-BIt</ta>
            <ta e="T16" id="Seg_699" s="T15">ogo-LAr</ta>
            <ta e="T17" id="Seg_700" s="T16">ol-ttAn</ta>
            <ta e="T18" id="Seg_701" s="T17">maːma-m</ta>
            <ta e="T20" id="Seg_702" s="T19">Vika-nI</ta>
            <ta e="T21" id="Seg_703" s="T20">kɨtta</ta>
            <ta e="T22" id="Seg_704" s="T21">bar-BIT</ta>
            <ta e="T25" id="Seg_705" s="T24">ogo</ta>
            <ta e="T26" id="Seg_706" s="T25">balɨs-I-BIt</ta>
            <ta e="T27" id="Seg_707" s="T26">ka</ta>
            <ta e="T28" id="Seg_708" s="T27">emi͡e</ta>
            <ta e="T29" id="Seg_709" s="T28">bihigi-nI</ta>
            <ta e="T30" id="Seg_710" s="T29">kɨtta</ta>
            <ta e="T31" id="Seg_711" s="T30">motor-I-nAn</ta>
            <ta e="T32" id="Seg_712" s="T31">barɨs-I-BIT</ta>
            <ta e="T35" id="Seg_713" s="T32">e-TI-tA</ta>
            <ta e="T38" id="Seg_714" s="T37">Roza-LAːK</ta>
            <ta e="T39" id="Seg_715" s="T38">tu͡ok-LAːK</ta>
            <ta e="T40" id="Seg_716" s="T39">ulakan</ta>
            <ta e="T41" id="Seg_717" s="T40">kihi-LAr</ta>
            <ta e="T44" id="Seg_718" s="T43">bar-BIT-I-LArA</ta>
            <ta e="T45" id="Seg_719" s="T44">hatɨː</ta>
            <ta e="T46" id="Seg_720" s="T45">ol-ttAn</ta>
            <ta e="T47" id="Seg_721" s="T46">bihigi</ta>
            <ta e="T48" id="Seg_722" s="T47">onno</ta>
            <ta e="T49" id="Seg_723" s="T48">oːnnʼoː-Ar</ta>
            <ta e="T50" id="Seg_724" s="T49">e-TI-BIt</ta>
            <ta e="T52" id="Seg_725" s="T51">tu͡ok</ta>
            <ta e="T53" id="Seg_726" s="T52">uːr-I-n-BIT-BIt</ta>
            <ta e="T54" id="Seg_727" s="T53">oːnnʼoː-Ar</ta>
            <ta e="T55" id="Seg_728" s="T54">e-TI-BIt</ta>
            <ta e="T56" id="Seg_729" s="T55">kim</ta>
            <ta e="T57" id="Seg_730" s="T56">skalaː-LAr</ta>
            <ta e="T58" id="Seg_731" s="T57">ürüt-tI-GAr</ta>
            <ta e="T59" id="Seg_732" s="T58">tur-Ar</ta>
            <ta e="T60" id="Seg_733" s="T59">e-TI-BIt</ta>
            <ta e="T62" id="Seg_734" s="T61">komuj-n-Ar</ta>
            <ta e="T63" id="Seg_735" s="T62">e-TI-BIt</ta>
            <ta e="T64" id="Seg_736" s="T63">kim</ta>
            <ta e="T66" id="Seg_737" s="T65">oːnnʼoː-Ar</ta>
            <ta e="T67" id="Seg_738" s="T66">e-TI-BIt</ta>
            <ta e="T68" id="Seg_739" s="T67">ulakan</ta>
            <ta e="T69" id="Seg_740" s="T68">kihi-LAr-nI</ta>
            <ta e="T70" id="Seg_741" s="T69">kɨtta</ta>
            <ta e="T71" id="Seg_742" s="T70">ol-ttAn</ta>
            <ta e="T72" id="Seg_743" s="T71">ka</ta>
            <ta e="T73" id="Seg_744" s="T72">lu͡otka-nAn</ta>
            <ta e="T74" id="Seg_745" s="T73">katatsa-LAː-Ar</ta>
            <ta e="T75" id="Seg_746" s="T74">e-TI-BIt</ta>
            <ta e="T76" id="Seg_747" s="T75">ol-ttAn</ta>
            <ta e="T77" id="Seg_748" s="T76">ka</ta>
            <ta e="T79" id="Seg_749" s="T78">oːnnʼoː-Ar</ta>
            <ta e="T80" id="Seg_750" s="T79">e-TI-BIt</ta>
            <ta e="T81" id="Seg_751" s="T80">otto</ta>
            <ta e="T82" id="Seg_752" s="T81">ahaː-BIT-BIt</ta>
            <ta e="T83" id="Seg_753" s="T82">emi͡e</ta>
            <ta e="T84" id="Seg_754" s="T83">oːnnʼoː-BIT-BIt</ta>
            <ta e="T85" id="Seg_755" s="T84">otto</ta>
            <ta e="T87" id="Seg_756" s="T86">is-tI-GAr</ta>
            <ta e="T88" id="Seg_757" s="T87">ogo-nI</ta>
            <ta e="T89" id="Seg_758" s="T88">kɨtta</ta>
            <ta e="T90" id="Seg_759" s="T89">oːnnʼoː-Ar</ta>
            <ta e="T91" id="Seg_760" s="T90">e-TI-BIt</ta>
            <ta e="T92" id="Seg_761" s="T91">dʼi͡e</ta>
            <ta e="T93" id="Seg_762" s="T92">oːnnʼoː-Ar</ta>
            <ta e="T94" id="Seg_763" s="T93">e-TI-BIt</ta>
            <ta e="T95" id="Seg_764" s="T94">otto</ta>
            <ta e="T96" id="Seg_765" s="T95">ogo-LAr-nI</ta>
            <ta e="T97" id="Seg_766" s="T96">kɨtta</ta>
            <ta e="T99" id="Seg_767" s="T98">oːnnʼoː-Ar</ta>
            <ta e="T100" id="Seg_768" s="T99">e-TI-BIt</ta>
            <ta e="T101" id="Seg_769" s="T100">ol-ttAn</ta>
            <ta e="T102" id="Seg_770" s="T101">ol-ttAn</ta>
            <ta e="T104" id="Seg_771" s="T103">oːnnʼoː-Ar</ta>
            <ta e="T105" id="Seg_772" s="T104">e-TI-BIt</ta>
            <ta e="T106" id="Seg_773" s="T105">bat-IAlAː-A-s-A</ta>
            <ta e="T107" id="Seg_774" s="T106">oːnnʼoː-Ar</ta>
            <ta e="T108" id="Seg_775" s="T107">e-TI-BIt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_776" s="T1">well</ta>
            <ta e="T3" id="Seg_777" s="T2">well</ta>
            <ta e="T4" id="Seg_778" s="T3">yesterday</ta>
            <ta e="T5" id="Seg_779" s="T4">1PL.[NOM]</ta>
            <ta e="T6" id="Seg_780" s="T5">who-DAT/LOC</ta>
            <ta e="T7" id="Seg_781" s="T6">go-PTCP.PST</ta>
            <ta e="T8" id="Seg_782" s="T7">be-PST1-1PL</ta>
            <ta e="T9" id="Seg_783" s="T8">EMPH</ta>
            <ta e="T10" id="Seg_784" s="T9">hike-VBZ-CVB.SIM</ta>
            <ta e="T11" id="Seg_785" s="T10">motor.boat-EP-INSTR</ta>
            <ta e="T12" id="Seg_786" s="T11">go-PTCP.PST</ta>
            <ta e="T13" id="Seg_787" s="T12">be-PST1-1PL</ta>
            <ta e="T14" id="Seg_788" s="T13">go-PTCP.PST</ta>
            <ta e="T15" id="Seg_789" s="T14">be-PST1-1PL</ta>
            <ta e="T16" id="Seg_790" s="T15">child-PL.[NOM]</ta>
            <ta e="T17" id="Seg_791" s="T16">that -ABL</ta>
            <ta e="T18" id="Seg_792" s="T17">mum-1SG.[NOM]</ta>
            <ta e="T20" id="Seg_793" s="T19">Vika-ACC</ta>
            <ta e="T21" id="Seg_794" s="T20">with</ta>
            <ta e="T22" id="Seg_795" s="T21">go-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_796" s="T24">child.[NOM]</ta>
            <ta e="T26" id="Seg_797" s="T25">younger.sister-EP-1PL.[NOM]</ta>
            <ta e="T27" id="Seg_798" s="T26">well</ta>
            <ta e="T28" id="Seg_799" s="T27">again</ta>
            <ta e="T29" id="Seg_800" s="T28">1PL-ACC</ta>
            <ta e="T30" id="Seg_801" s="T29">with</ta>
            <ta e="T31" id="Seg_802" s="T30">motor.boat-EP-INSTR</ta>
            <ta e="T32" id="Seg_803" s="T31">come.along-EP-PTCP.PST</ta>
            <ta e="T35" id="Seg_804" s="T32">be-PST1-3SG</ta>
            <ta e="T38" id="Seg_805" s="T37">Rose-PROPR</ta>
            <ta e="T39" id="Seg_806" s="T38">what-PROPR</ta>
            <ta e="T40" id="Seg_807" s="T39">big</ta>
            <ta e="T41" id="Seg_808" s="T40">human.being-PL.[NOM]</ta>
            <ta e="T44" id="Seg_809" s="T43">go-PST2-EP-3PL</ta>
            <ta e="T45" id="Seg_810" s="T44">by.foot</ta>
            <ta e="T46" id="Seg_811" s="T45">that -ABL</ta>
            <ta e="T47" id="Seg_812" s="T46">1PL.[NOM]</ta>
            <ta e="T48" id="Seg_813" s="T47">there</ta>
            <ta e="T49" id="Seg_814" s="T48">play-PTCP.PRS</ta>
            <ta e="T50" id="Seg_815" s="T49">be-PST1-1PL</ta>
            <ta e="T52" id="Seg_816" s="T51">what.[NOM]</ta>
            <ta e="T53" id="Seg_817" s="T52">lay-EP-REFL-PST2-1PL</ta>
            <ta e="T54" id="Seg_818" s="T53">play-PTCP.PRS</ta>
            <ta e="T55" id="Seg_819" s="T54">be-PST1-1PL</ta>
            <ta e="T56" id="Seg_820" s="T55">who.[NOM]</ta>
            <ta e="T57" id="Seg_821" s="T56">rock-PL.[NOM]</ta>
            <ta e="T58" id="Seg_822" s="T57">upper.part-3SG-DAT/LOC</ta>
            <ta e="T59" id="Seg_823" s="T58">stand-PTCP.PRS</ta>
            <ta e="T60" id="Seg_824" s="T59">be-PST1-1PL</ta>
            <ta e="T62" id="Seg_825" s="T61">gather-REFL-PTCP.PRS</ta>
            <ta e="T63" id="Seg_826" s="T62">be-PST1-1PL</ta>
            <ta e="T64" id="Seg_827" s="T63">who.[NOM]</ta>
            <ta e="T66" id="Seg_828" s="T65">play-PTCP.PRS</ta>
            <ta e="T67" id="Seg_829" s="T66">be-PST1-1PL</ta>
            <ta e="T68" id="Seg_830" s="T67">big</ta>
            <ta e="T69" id="Seg_831" s="T68">human.being-PL-ACC</ta>
            <ta e="T70" id="Seg_832" s="T69">with</ta>
            <ta e="T71" id="Seg_833" s="T70">that -ABL</ta>
            <ta e="T72" id="Seg_834" s="T71">well</ta>
            <ta e="T73" id="Seg_835" s="T72">boat-INSTR</ta>
            <ta e="T74" id="Seg_836" s="T73">ride-VBZ-PTCP.PRS</ta>
            <ta e="T75" id="Seg_837" s="T74">be-PST1-1PL</ta>
            <ta e="T76" id="Seg_838" s="T75">that -ABL</ta>
            <ta e="T77" id="Seg_839" s="T76">well</ta>
            <ta e="T79" id="Seg_840" s="T78">play-PTCP.PRS</ta>
            <ta e="T80" id="Seg_841" s="T79">be-PST1-1PL</ta>
            <ta e="T81" id="Seg_842" s="T80">EMPH</ta>
            <ta e="T82" id="Seg_843" s="T81">eat-PST2-1PL</ta>
            <ta e="T83" id="Seg_844" s="T82">again</ta>
            <ta e="T84" id="Seg_845" s="T83">play-PST2-1PL</ta>
            <ta e="T85" id="Seg_846" s="T84">EMPH</ta>
            <ta e="T87" id="Seg_847" s="T86">inside-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_848" s="T87">child-ACC</ta>
            <ta e="T89" id="Seg_849" s="T88">with</ta>
            <ta e="T90" id="Seg_850" s="T89">play-PTCP.PRS</ta>
            <ta e="T91" id="Seg_851" s="T90">be-PST1-1PL</ta>
            <ta e="T92" id="Seg_852" s="T91">house.[NOM]</ta>
            <ta e="T93" id="Seg_853" s="T92">play-PTCP.PRS</ta>
            <ta e="T94" id="Seg_854" s="T93">be-PST1-1PL</ta>
            <ta e="T95" id="Seg_855" s="T94">EMPH</ta>
            <ta e="T96" id="Seg_856" s="T95">child-PL-ACC</ta>
            <ta e="T97" id="Seg_857" s="T96">with</ta>
            <ta e="T99" id="Seg_858" s="T98">play-PTCP.PRS</ta>
            <ta e="T100" id="Seg_859" s="T99">be-PST1-1PL</ta>
            <ta e="T101" id="Seg_860" s="T100">that -ABL</ta>
            <ta e="T102" id="Seg_861" s="T101">that -ABL</ta>
            <ta e="T104" id="Seg_862" s="T103">play-PTCP.PRS</ta>
            <ta e="T105" id="Seg_863" s="T104">be-PST1-1PL</ta>
            <ta e="T106" id="Seg_864" s="T105">follow-FREQ-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T107" id="Seg_865" s="T106">play-PTCP.PRS</ta>
            <ta e="T108" id="Seg_866" s="T107">be-PST1-1PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_867" s="T1">doch</ta>
            <ta e="T3" id="Seg_868" s="T2">nun</ta>
            <ta e="T4" id="Seg_869" s="T3">gesteren</ta>
            <ta e="T5" id="Seg_870" s="T4">1PL.[NOM]</ta>
            <ta e="T6" id="Seg_871" s="T5">wer-DAT/LOC</ta>
            <ta e="T7" id="Seg_872" s="T6">gehen-PTCP.PST</ta>
            <ta e="T8" id="Seg_873" s="T7">sein-PST1-1PL</ta>
            <ta e="T9" id="Seg_874" s="T8">EMPH</ta>
            <ta e="T10" id="Seg_875" s="T9">Wanderung-VBZ-CVB.SIM</ta>
            <ta e="T11" id="Seg_876" s="T10">Motorboot-EP-INSTR</ta>
            <ta e="T12" id="Seg_877" s="T11">gehen-PTCP.PST</ta>
            <ta e="T13" id="Seg_878" s="T12">sein-PST1-1PL</ta>
            <ta e="T14" id="Seg_879" s="T13">gehen-PTCP.PST</ta>
            <ta e="T15" id="Seg_880" s="T14">sein-PST1-1PL</ta>
            <ta e="T16" id="Seg_881" s="T15">Kind-PL.[NOM]</ta>
            <ta e="T17" id="Seg_882" s="T16">jenes-ABL</ta>
            <ta e="T18" id="Seg_883" s="T17">Mama-1SG.[NOM]</ta>
            <ta e="T20" id="Seg_884" s="T19">Vika-ACC</ta>
            <ta e="T21" id="Seg_885" s="T20">mit</ta>
            <ta e="T22" id="Seg_886" s="T21">gehen-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_887" s="T24">Kind.[NOM]</ta>
            <ta e="T26" id="Seg_888" s="T25">jüngere.Schwester-EP-1PL.[NOM]</ta>
            <ta e="T27" id="Seg_889" s="T26">nun</ta>
            <ta e="T28" id="Seg_890" s="T27">wieder</ta>
            <ta e="T29" id="Seg_891" s="T28">1PL-ACC</ta>
            <ta e="T30" id="Seg_892" s="T29">mit</ta>
            <ta e="T31" id="Seg_893" s="T30">Motorboot-EP-INSTR</ta>
            <ta e="T32" id="Seg_894" s="T31">mitkommen-EP-PTCP.PST</ta>
            <ta e="T35" id="Seg_895" s="T32">sein-PST1-3SG</ta>
            <ta e="T38" id="Seg_896" s="T37">Rosa-PROPR</ta>
            <ta e="T39" id="Seg_897" s="T38">was-PROPR</ta>
            <ta e="T40" id="Seg_898" s="T39">groß</ta>
            <ta e="T41" id="Seg_899" s="T40">Mensch-PL.[NOM]</ta>
            <ta e="T44" id="Seg_900" s="T43">gehen-PST2-EP-3PL</ta>
            <ta e="T45" id="Seg_901" s="T44">zu.Fuß</ta>
            <ta e="T46" id="Seg_902" s="T45">jenes-ABL</ta>
            <ta e="T47" id="Seg_903" s="T46">1PL.[NOM]</ta>
            <ta e="T48" id="Seg_904" s="T47">dort</ta>
            <ta e="T49" id="Seg_905" s="T48">spielen-PTCP.PRS</ta>
            <ta e="T50" id="Seg_906" s="T49">sein-PST1-1PL</ta>
            <ta e="T52" id="Seg_907" s="T51">was.[NOM]</ta>
            <ta e="T53" id="Seg_908" s="T52">legen-EP-REFL-PST2-1PL</ta>
            <ta e="T54" id="Seg_909" s="T53">spielen-PTCP.PRS</ta>
            <ta e="T55" id="Seg_910" s="T54">sein-PST1-1PL</ta>
            <ta e="T56" id="Seg_911" s="T55">wer.[NOM]</ta>
            <ta e="T57" id="Seg_912" s="T56">Felsen-PL.[NOM]</ta>
            <ta e="T58" id="Seg_913" s="T57">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T59" id="Seg_914" s="T58">stehen-PTCP.PRS</ta>
            <ta e="T60" id="Seg_915" s="T59">sein-PST1-1PL</ta>
            <ta e="T62" id="Seg_916" s="T61">sammeln-REFL-PTCP.PRS</ta>
            <ta e="T63" id="Seg_917" s="T62">sein-PST1-1PL</ta>
            <ta e="T64" id="Seg_918" s="T63">wer.[NOM]</ta>
            <ta e="T66" id="Seg_919" s="T65">spielen-PTCP.PRS</ta>
            <ta e="T67" id="Seg_920" s="T66">sein-PST1-1PL</ta>
            <ta e="T68" id="Seg_921" s="T67">groß</ta>
            <ta e="T69" id="Seg_922" s="T68">Mensch-PL-ACC</ta>
            <ta e="T70" id="Seg_923" s="T69">mit</ta>
            <ta e="T71" id="Seg_924" s="T70">jenes-ABL</ta>
            <ta e="T72" id="Seg_925" s="T71">nun</ta>
            <ta e="T73" id="Seg_926" s="T72">Boot-INSTR</ta>
            <ta e="T74" id="Seg_927" s="T73">fahren-VBZ-PTCP.PRS</ta>
            <ta e="T75" id="Seg_928" s="T74">sein-PST1-1PL</ta>
            <ta e="T76" id="Seg_929" s="T75">jenes-ABL</ta>
            <ta e="T77" id="Seg_930" s="T76">nun</ta>
            <ta e="T79" id="Seg_931" s="T78">spielen-PTCP.PRS</ta>
            <ta e="T80" id="Seg_932" s="T79">sein-PST1-1PL</ta>
            <ta e="T81" id="Seg_933" s="T80">EMPH</ta>
            <ta e="T82" id="Seg_934" s="T81">essen-PST2-1PL</ta>
            <ta e="T83" id="Seg_935" s="T82">wieder</ta>
            <ta e="T84" id="Seg_936" s="T83">spielen-PST2-1PL</ta>
            <ta e="T85" id="Seg_937" s="T84">EMPH</ta>
            <ta e="T87" id="Seg_938" s="T86">Inneres-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_939" s="T87">Kind-ACC</ta>
            <ta e="T89" id="Seg_940" s="T88">mit</ta>
            <ta e="T90" id="Seg_941" s="T89">spielen-PTCP.PRS</ta>
            <ta e="T91" id="Seg_942" s="T90">sein-PST1-1PL</ta>
            <ta e="T92" id="Seg_943" s="T91">Haus.[NOM]</ta>
            <ta e="T93" id="Seg_944" s="T92">spielen-PTCP.PRS</ta>
            <ta e="T94" id="Seg_945" s="T93">sein-PST1-1PL</ta>
            <ta e="T95" id="Seg_946" s="T94">EMPH</ta>
            <ta e="T96" id="Seg_947" s="T95">Kind-PL-ACC</ta>
            <ta e="T97" id="Seg_948" s="T96">mit</ta>
            <ta e="T99" id="Seg_949" s="T98">spielen-PTCP.PRS</ta>
            <ta e="T100" id="Seg_950" s="T99">sein-PST1-1PL</ta>
            <ta e="T101" id="Seg_951" s="T100">jenes-ABL</ta>
            <ta e="T102" id="Seg_952" s="T101">jenes-ABL</ta>
            <ta e="T104" id="Seg_953" s="T103">spielen-PTCP.PRS</ta>
            <ta e="T105" id="Seg_954" s="T104">sein-PST1-1PL</ta>
            <ta e="T106" id="Seg_955" s="T105">folgen-FREQ-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T107" id="Seg_956" s="T106">spielen-PTCP.PRS</ta>
            <ta e="T108" id="Seg_957" s="T107">sein-PST1-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_958" s="T1">вот</ta>
            <ta e="T3" id="Seg_959" s="T2">вот</ta>
            <ta e="T4" id="Seg_960" s="T3">вчера</ta>
            <ta e="T5" id="Seg_961" s="T4">1PL.[NOM]</ta>
            <ta e="T6" id="Seg_962" s="T5">кто-DAT/LOC</ta>
            <ta e="T7" id="Seg_963" s="T6">идти-PTCP.PST</ta>
            <ta e="T8" id="Seg_964" s="T7">быть-PST1-1PL</ta>
            <ta e="T9" id="Seg_965" s="T8">EMPH</ta>
            <ta e="T10" id="Seg_966" s="T9">поход-VBZ-CVB.SIM</ta>
            <ta e="T11" id="Seg_967" s="T10">моторная.лодка-EP-INSTR</ta>
            <ta e="T12" id="Seg_968" s="T11">идти-PTCP.PST</ta>
            <ta e="T13" id="Seg_969" s="T12">быть-PST1-1PL</ta>
            <ta e="T14" id="Seg_970" s="T13">идти-PTCP.PST</ta>
            <ta e="T15" id="Seg_971" s="T14">быть-PST1-1PL</ta>
            <ta e="T16" id="Seg_972" s="T15">ребенок-PL.[NOM]</ta>
            <ta e="T17" id="Seg_973" s="T16">тот -ABL</ta>
            <ta e="T18" id="Seg_974" s="T17">мама-1SG.[NOM]</ta>
            <ta e="T20" id="Seg_975" s="T19">Вика-ACC</ta>
            <ta e="T21" id="Seg_976" s="T20">с</ta>
            <ta e="T22" id="Seg_977" s="T21">идти-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_978" s="T24">ребенок.[NOM]</ta>
            <ta e="T26" id="Seg_979" s="T25">младшая.сестра-EP-1PL.[NOM]</ta>
            <ta e="T27" id="Seg_980" s="T26">вот</ta>
            <ta e="T28" id="Seg_981" s="T27">опять</ta>
            <ta e="T29" id="Seg_982" s="T28">1PL-ACC</ta>
            <ta e="T30" id="Seg_983" s="T29">с</ta>
            <ta e="T31" id="Seg_984" s="T30">моторная.лодка-EP-INSTR</ta>
            <ta e="T32" id="Seg_985" s="T31">сопровождать-EP-PTCP.PST</ta>
            <ta e="T35" id="Seg_986" s="T32">быть-PST1-3SG</ta>
            <ta e="T38" id="Seg_987" s="T37">Роза-PROPR</ta>
            <ta e="T39" id="Seg_988" s="T38">что-PROPR</ta>
            <ta e="T40" id="Seg_989" s="T39">большой</ta>
            <ta e="T41" id="Seg_990" s="T40">человек-PL.[NOM]</ta>
            <ta e="T44" id="Seg_991" s="T43">идти-PST2-EP-3PL</ta>
            <ta e="T45" id="Seg_992" s="T44">пешком</ta>
            <ta e="T46" id="Seg_993" s="T45">тот -ABL</ta>
            <ta e="T47" id="Seg_994" s="T46">1PL.[NOM]</ta>
            <ta e="T48" id="Seg_995" s="T47">там</ta>
            <ta e="T49" id="Seg_996" s="T48">играть-PTCP.PRS</ta>
            <ta e="T50" id="Seg_997" s="T49">быть-PST1-1PL</ta>
            <ta e="T52" id="Seg_998" s="T51">что.[NOM]</ta>
            <ta e="T53" id="Seg_999" s="T52">класть-EP-REFL-PST2-1PL</ta>
            <ta e="T54" id="Seg_1000" s="T53">играть-PTCP.PRS</ta>
            <ta e="T55" id="Seg_1001" s="T54">быть-PST1-1PL</ta>
            <ta e="T56" id="Seg_1002" s="T55">кто.[NOM]</ta>
            <ta e="T57" id="Seg_1003" s="T56">скала-PL.[NOM]</ta>
            <ta e="T58" id="Seg_1004" s="T57">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T59" id="Seg_1005" s="T58">стоять-PTCP.PRS</ta>
            <ta e="T60" id="Seg_1006" s="T59">быть-PST1-1PL</ta>
            <ta e="T62" id="Seg_1007" s="T61">собирать-REFL-PTCP.PRS</ta>
            <ta e="T63" id="Seg_1008" s="T62">быть-PST1-1PL</ta>
            <ta e="T64" id="Seg_1009" s="T63">кто.[NOM]</ta>
            <ta e="T66" id="Seg_1010" s="T65">играть-PTCP.PRS</ta>
            <ta e="T67" id="Seg_1011" s="T66">быть-PST1-1PL</ta>
            <ta e="T68" id="Seg_1012" s="T67">большой</ta>
            <ta e="T69" id="Seg_1013" s="T68">человек-PL-ACC</ta>
            <ta e="T70" id="Seg_1014" s="T69">с</ta>
            <ta e="T71" id="Seg_1015" s="T70">тот -ABL</ta>
            <ta e="T72" id="Seg_1016" s="T71">вот</ta>
            <ta e="T73" id="Seg_1017" s="T72">лодка-INSTR</ta>
            <ta e="T74" id="Seg_1018" s="T73">кататься-VBZ-PTCP.PRS</ta>
            <ta e="T75" id="Seg_1019" s="T74">быть-PST1-1PL</ta>
            <ta e="T76" id="Seg_1020" s="T75">тот -ABL</ta>
            <ta e="T77" id="Seg_1021" s="T76">вот</ta>
            <ta e="T79" id="Seg_1022" s="T78">играть-PTCP.PRS</ta>
            <ta e="T80" id="Seg_1023" s="T79">быть-PST1-1PL</ta>
            <ta e="T81" id="Seg_1024" s="T80">EMPH</ta>
            <ta e="T82" id="Seg_1025" s="T81">есть-PST2-1PL</ta>
            <ta e="T83" id="Seg_1026" s="T82">опять</ta>
            <ta e="T84" id="Seg_1027" s="T83">играть-PST2-1PL</ta>
            <ta e="T85" id="Seg_1028" s="T84">EMPH</ta>
            <ta e="T87" id="Seg_1029" s="T86">нутро-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_1030" s="T87">ребенок-ACC</ta>
            <ta e="T89" id="Seg_1031" s="T88">с</ta>
            <ta e="T90" id="Seg_1032" s="T89">играть-PTCP.PRS</ta>
            <ta e="T91" id="Seg_1033" s="T90">быть-PST1-1PL</ta>
            <ta e="T92" id="Seg_1034" s="T91">дом.[NOM]</ta>
            <ta e="T93" id="Seg_1035" s="T92">играть-PTCP.PRS</ta>
            <ta e="T94" id="Seg_1036" s="T93">быть-PST1-1PL</ta>
            <ta e="T95" id="Seg_1037" s="T94">EMPH</ta>
            <ta e="T96" id="Seg_1038" s="T95">ребенок-PL-ACC</ta>
            <ta e="T97" id="Seg_1039" s="T96">с</ta>
            <ta e="T99" id="Seg_1040" s="T98">играть-PTCP.PRS</ta>
            <ta e="T100" id="Seg_1041" s="T99">быть-PST1-1PL</ta>
            <ta e="T101" id="Seg_1042" s="T100">тот -ABL</ta>
            <ta e="T102" id="Seg_1043" s="T101">тот -ABL</ta>
            <ta e="T104" id="Seg_1044" s="T103">играть-PTCP.PRS</ta>
            <ta e="T105" id="Seg_1045" s="T104">быть-PST1-1PL</ta>
            <ta e="T106" id="Seg_1046" s="T105">следовать-FREQ-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T107" id="Seg_1047" s="T106">играть-PTCP.PRS</ta>
            <ta e="T108" id="Seg_1048" s="T107">быть-PST1-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1049" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_1050" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_1051" s="T3">adv</ta>
            <ta e="T5" id="Seg_1052" s="T4">pers.[pro:case]</ta>
            <ta e="T6" id="Seg_1053" s="T5">que-pro:case</ta>
            <ta e="T7" id="Seg_1054" s="T6">v-v:ptcp</ta>
            <ta e="T8" id="Seg_1055" s="T7">v-v:tense-v:pred.pn</ta>
            <ta e="T9" id="Seg_1056" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_1057" s="T9">n-n&gt;v-v:cvb</ta>
            <ta e="T11" id="Seg_1058" s="T10">n-n:(ins)-n:case</ta>
            <ta e="T12" id="Seg_1059" s="T11">v-v:ptcp</ta>
            <ta e="T13" id="Seg_1060" s="T12">v-v:tense-v:pred.pn</ta>
            <ta e="T14" id="Seg_1061" s="T13">v-v:ptcp</ta>
            <ta e="T15" id="Seg_1062" s="T14">v-v:tense-v:pred.pn</ta>
            <ta e="T16" id="Seg_1063" s="T15">n-n:(num).[n:case]</ta>
            <ta e="T17" id="Seg_1064" s="T16">dempro-pro:case</ta>
            <ta e="T18" id="Seg_1065" s="T17">n-n:(poss).[n:case]</ta>
            <ta e="T20" id="Seg_1066" s="T19">propr-n:case</ta>
            <ta e="T21" id="Seg_1067" s="T20">post</ta>
            <ta e="T22" id="Seg_1068" s="T21">v-v:tense.[v:pred.pn]</ta>
            <ta e="T25" id="Seg_1069" s="T24">n.[n:case]</ta>
            <ta e="T26" id="Seg_1070" s="T25">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T27" id="Seg_1071" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_1072" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1073" s="T28">pers-pro:case</ta>
            <ta e="T30" id="Seg_1074" s="T29">post</ta>
            <ta e="T31" id="Seg_1075" s="T30">n-n:(ins)-n:case</ta>
            <ta e="T32" id="Seg_1076" s="T31">v-v:(ins)-v:ptcp</ta>
            <ta e="T35" id="Seg_1077" s="T32">v-v:tense-v:poss.pn</ta>
            <ta e="T38" id="Seg_1078" s="T37">propr-propr&gt;adj</ta>
            <ta e="T39" id="Seg_1079" s="T38">que-que&gt;adj</ta>
            <ta e="T40" id="Seg_1080" s="T39">adj</ta>
            <ta e="T41" id="Seg_1081" s="T40">n-n:(num).[n:case]</ta>
            <ta e="T44" id="Seg_1082" s="T43">v-v:tense-v:(ins)-n:(poss)</ta>
            <ta e="T45" id="Seg_1083" s="T44">adv</ta>
            <ta e="T46" id="Seg_1084" s="T45">dempro-pro:case</ta>
            <ta e="T47" id="Seg_1085" s="T46">pers.[pro:case]</ta>
            <ta e="T48" id="Seg_1086" s="T47">adv</ta>
            <ta e="T49" id="Seg_1087" s="T48">v-v:ptcp</ta>
            <ta e="T50" id="Seg_1088" s="T49">v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_1089" s="T51">que.[pro:case]</ta>
            <ta e="T53" id="Seg_1090" s="T52">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_1091" s="T53">v-v:ptcp</ta>
            <ta e="T55" id="Seg_1092" s="T54">v-v:tense-v:pred.pn</ta>
            <ta e="T56" id="Seg_1093" s="T55">que.[pro:case]</ta>
            <ta e="T57" id="Seg_1094" s="T56">n-n:(num).[n:case]</ta>
            <ta e="T58" id="Seg_1095" s="T57">n-n:poss-n:case</ta>
            <ta e="T59" id="Seg_1096" s="T58">v-v:ptcp</ta>
            <ta e="T60" id="Seg_1097" s="T59">v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_1098" s="T61">v-v&gt;v-v:ptcp</ta>
            <ta e="T63" id="Seg_1099" s="T62">v-v:tense-v:pred.pn</ta>
            <ta e="T64" id="Seg_1100" s="T63">que.[pro:case]</ta>
            <ta e="T66" id="Seg_1101" s="T65">v-v:ptcp</ta>
            <ta e="T67" id="Seg_1102" s="T66">v-v:tense-v:poss.pn</ta>
            <ta e="T68" id="Seg_1103" s="T67">adj</ta>
            <ta e="T69" id="Seg_1104" s="T68">n-n:(num)-n:case</ta>
            <ta e="T70" id="Seg_1105" s="T69">post</ta>
            <ta e="T71" id="Seg_1106" s="T70">dempro-pro:case</ta>
            <ta e="T72" id="Seg_1107" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1108" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_1109" s="T73">v-v&gt;v-v:ptcp</ta>
            <ta e="T75" id="Seg_1110" s="T74">v-v:tense-v:poss.pn</ta>
            <ta e="T76" id="Seg_1111" s="T75">dempro-pro:case</ta>
            <ta e="T77" id="Seg_1112" s="T76">ptcl</ta>
            <ta e="T79" id="Seg_1113" s="T78">v-v:ptcp</ta>
            <ta e="T80" id="Seg_1114" s="T79">v-v:tense-v:poss.pn</ta>
            <ta e="T81" id="Seg_1115" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1116" s="T81">v-v:tense-v:pred.pn</ta>
            <ta e="T83" id="Seg_1117" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_1118" s="T83">v-v:tense-v:pred.pn</ta>
            <ta e="T85" id="Seg_1119" s="T84">ptcl</ta>
            <ta e="T87" id="Seg_1120" s="T86">n-n:poss-n:case</ta>
            <ta e="T88" id="Seg_1121" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_1122" s="T88">post</ta>
            <ta e="T90" id="Seg_1123" s="T89">v-v:ptcp</ta>
            <ta e="T91" id="Seg_1124" s="T90">v-v:tense-v:poss.pn</ta>
            <ta e="T92" id="Seg_1125" s="T91">n.[n:case]</ta>
            <ta e="T93" id="Seg_1126" s="T92">v-v:ptcp</ta>
            <ta e="T94" id="Seg_1127" s="T93">v-v:tense-v:poss.pn</ta>
            <ta e="T95" id="Seg_1128" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_1129" s="T95">n-n:(num)-n:case</ta>
            <ta e="T97" id="Seg_1130" s="T96">post</ta>
            <ta e="T99" id="Seg_1131" s="T98">v-v:ptcp</ta>
            <ta e="T100" id="Seg_1132" s="T99">v-v:tense-v:poss.pn</ta>
            <ta e="T101" id="Seg_1133" s="T100">dempro-pro:case</ta>
            <ta e="T102" id="Seg_1134" s="T101">dempro-pro:case</ta>
            <ta e="T104" id="Seg_1135" s="T103">v-v:ptcp</ta>
            <ta e="T105" id="Seg_1136" s="T104">v-v:tense-v:poss.pn</ta>
            <ta e="T106" id="Seg_1137" s="T105">v-v&gt;v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T107" id="Seg_1138" s="T106">v-v:ptcp</ta>
            <ta e="T108" id="Seg_1139" s="T107">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1140" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_1141" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_1142" s="T3">adv</ta>
            <ta e="T5" id="Seg_1143" s="T4">pers</ta>
            <ta e="T6" id="Seg_1144" s="T5">que</ta>
            <ta e="T7" id="Seg_1145" s="T6">v</ta>
            <ta e="T8" id="Seg_1146" s="T7">aux</ta>
            <ta e="T9" id="Seg_1147" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_1148" s="T9">v</ta>
            <ta e="T11" id="Seg_1149" s="T10">n</ta>
            <ta e="T12" id="Seg_1150" s="T11">v</ta>
            <ta e="T13" id="Seg_1151" s="T12">aux</ta>
            <ta e="T14" id="Seg_1152" s="T13">v</ta>
            <ta e="T15" id="Seg_1153" s="T14">aux</ta>
            <ta e="T16" id="Seg_1154" s="T15">n</ta>
            <ta e="T17" id="Seg_1155" s="T16">dempro</ta>
            <ta e="T18" id="Seg_1156" s="T17">n</ta>
            <ta e="T20" id="Seg_1157" s="T19">propr</ta>
            <ta e="T21" id="Seg_1158" s="T20">post</ta>
            <ta e="T22" id="Seg_1159" s="T21">v</ta>
            <ta e="T25" id="Seg_1160" s="T24">n</ta>
            <ta e="T26" id="Seg_1161" s="T25">n</ta>
            <ta e="T27" id="Seg_1162" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_1163" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1164" s="T28">pers</ta>
            <ta e="T30" id="Seg_1165" s="T29">post</ta>
            <ta e="T31" id="Seg_1166" s="T30">n</ta>
            <ta e="T32" id="Seg_1167" s="T31">v</ta>
            <ta e="T35" id="Seg_1168" s="T32">aux</ta>
            <ta e="T38" id="Seg_1169" s="T37">adj</ta>
            <ta e="T39" id="Seg_1170" s="T38">adj</ta>
            <ta e="T40" id="Seg_1171" s="T39">adj</ta>
            <ta e="T41" id="Seg_1172" s="T40">n</ta>
            <ta e="T44" id="Seg_1173" s="T43">v</ta>
            <ta e="T45" id="Seg_1174" s="T44">adv</ta>
            <ta e="T46" id="Seg_1175" s="T45">dempro</ta>
            <ta e="T47" id="Seg_1176" s="T46">pers</ta>
            <ta e="T48" id="Seg_1177" s="T47">adv</ta>
            <ta e="T49" id="Seg_1178" s="T48">v</ta>
            <ta e="T50" id="Seg_1179" s="T49">aux</ta>
            <ta e="T52" id="Seg_1180" s="T51">que</ta>
            <ta e="T53" id="Seg_1181" s="T52">v</ta>
            <ta e="T54" id="Seg_1182" s="T53">v</ta>
            <ta e="T55" id="Seg_1183" s="T54">aux</ta>
            <ta e="T56" id="Seg_1184" s="T55">que</ta>
            <ta e="T57" id="Seg_1185" s="T56">n</ta>
            <ta e="T58" id="Seg_1186" s="T57">n</ta>
            <ta e="T59" id="Seg_1187" s="T58">v</ta>
            <ta e="T60" id="Seg_1188" s="T59">aux</ta>
            <ta e="T62" id="Seg_1189" s="T61">v</ta>
            <ta e="T63" id="Seg_1190" s="T62">aux</ta>
            <ta e="T64" id="Seg_1191" s="T63">que</ta>
            <ta e="T66" id="Seg_1192" s="T65">v</ta>
            <ta e="T67" id="Seg_1193" s="T66">aux</ta>
            <ta e="T68" id="Seg_1194" s="T67">adj</ta>
            <ta e="T69" id="Seg_1195" s="T68">n</ta>
            <ta e="T70" id="Seg_1196" s="T69">post</ta>
            <ta e="T71" id="Seg_1197" s="T70">dempro</ta>
            <ta e="T72" id="Seg_1198" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1199" s="T72">n</ta>
            <ta e="T74" id="Seg_1200" s="T73">v</ta>
            <ta e="T75" id="Seg_1201" s="T74">aux</ta>
            <ta e="T76" id="Seg_1202" s="T75">dempro</ta>
            <ta e="T77" id="Seg_1203" s="T76">ptcl</ta>
            <ta e="T79" id="Seg_1204" s="T78">v</ta>
            <ta e="T80" id="Seg_1205" s="T79">aux</ta>
            <ta e="T81" id="Seg_1206" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1207" s="T81">v</ta>
            <ta e="T83" id="Seg_1208" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_1209" s="T83">v</ta>
            <ta e="T85" id="Seg_1210" s="T84">v</ta>
            <ta e="T87" id="Seg_1211" s="T86">n</ta>
            <ta e="T88" id="Seg_1212" s="T87">n</ta>
            <ta e="T89" id="Seg_1213" s="T88">post</ta>
            <ta e="T90" id="Seg_1214" s="T89">v</ta>
            <ta e="T91" id="Seg_1215" s="T90">aux</ta>
            <ta e="T92" id="Seg_1216" s="T91">n</ta>
            <ta e="T93" id="Seg_1217" s="T92">v</ta>
            <ta e="T94" id="Seg_1218" s="T93">aux</ta>
            <ta e="T95" id="Seg_1219" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_1220" s="T95">n</ta>
            <ta e="T97" id="Seg_1221" s="T96">post</ta>
            <ta e="T99" id="Seg_1222" s="T98">v</ta>
            <ta e="T100" id="Seg_1223" s="T99">aux</ta>
            <ta e="T101" id="Seg_1224" s="T100">dempro</ta>
            <ta e="T102" id="Seg_1225" s="T101">dempro</ta>
            <ta e="T104" id="Seg_1226" s="T103">v</ta>
            <ta e="T105" id="Seg_1227" s="T104">aux</ta>
            <ta e="T106" id="Seg_1228" s="T105">v</ta>
            <ta e="T107" id="Seg_1229" s="T106">v</ta>
            <ta e="T108" id="Seg_1230" s="T107">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_1231" s="T9">RUS:cult</ta>
            <ta e="T11" id="Seg_1232" s="T10">RUS:cult</ta>
            <ta e="T18" id="Seg_1233" s="T17">RUS:cult</ta>
            <ta e="T20" id="Seg_1234" s="T19">RUS:cult</ta>
            <ta e="T31" id="Seg_1235" s="T30">RUS:cult</ta>
            <ta e="T38" id="Seg_1236" s="T37">RUS:cult</ta>
            <ta e="T57" id="Seg_1237" s="T56">RUS:core</ta>
            <ta e="T73" id="Seg_1238" s="T72">RUS:cult</ta>
            <ta e="T74" id="Seg_1239" s="T73">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T10" id="Seg_1240" s="T1">Well, yesterday we went to the whatever, on a hike.</ta>
            <ta e="T35" id="Seg_1241" s="T10">We went by motorboat, the children went, and mommy with Vika came along and the youngest sister also came with us with the motorboat.</ta>
            <ta e="T55" id="Seg_1242" s="T35">And auntie Roza and her people, some grown up people went along the shore by foot, then we played, we set up a tent or something, we played.</ta>
            <ta e="T63" id="Seg_1243" s="T55">Well we stood on top of the rocks, we collected mushrooms.</ta>
            <ta e="T72" id="Seg_1244" s="T63">We played cricket with the grown ups then.</ta>
            <ta e="T75" id="Seg_1245" s="T72">We rode a boat.</ta>
            <ta e="T91" id="Seg_1246" s="T75">Then we played cricket, then we ate, then we played again in the tent, we played with the child.</ta>
            <ta e="T102" id="Seg_1247" s="T91">We played 'house', then we played cricked with the children separately, then, then…</ta>
            <ta e="T108" id="Seg_1248" s="T102">We played cricket and then we played tag.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T10" id="Seg_1249" s="T1">Nun, gestern gingen wir nach Dings, wandern. </ta>
            <ta e="T35" id="Seg_1250" s="T10">Wir fuhren mit dem Motorboot, die Kinder fuhren, und meine Mama kam mit Vika mit und unsere jüngste Schwester kam auch mit uns im Motorboot mit.</ta>
            <ta e="T55" id="Seg_1251" s="T35">Und Tante Roza und ihre Leute, einige Erwachsene gingen zu Fuß am Ufer, dann spielten wir, wir stellten ein Zelt und so auf, wir spielten.</ta>
            <ta e="T63" id="Seg_1252" s="T55">Wir standen auf den Dings, auf den Felsen, wir sammelten Pilze.</ta>
            <ta e="T72" id="Seg_1253" s="T63">Wir spielten Kricket mit den Erwachsenen dann.</ta>
            <ta e="T75" id="Seg_1254" s="T72">Wir fuhren mit einem Boot.</ta>
            <ta e="T91" id="Seg_1255" s="T75">Dann spielten wir Kricket, dann aßen wir, dann spielten wir wieder im Zelt, wir spielten mit dem Kind.</ta>
            <ta e="T102" id="Seg_1256" s="T91">Wir spielten "Haus", dann spielten wir alleine mit den Kindern Kricket, dann, dann...</ta>
            <ta e="T108" id="Seg_1257" s="T102">Wir spielten Kricket und dann spielten wir Ticken.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T10" id="Seg_1258" s="T1">вчера мы туда сходили в поход</ta>
            <ta e="T35" id="Seg_1259" s="T10">на моторе ездили, ездтлт дети потом мама с Викой ездила с самой младшей сестренкой тоже с нами на моторе ездила</ta>
            <ta e="T55" id="Seg_1260" s="T35">а тетя Роза и некоторые взрослые люди по берегу пошли пешом, потом мы там играли, палатку поставили, играли</ta>
            <ta e="T63" id="Seg_1261" s="T55">Эта на скалах верху стояли,грибы собирали</ta>
            <ta e="T72" id="Seg_1262" s="T63">эта в лапту играли со взрослыми людьми, потом</ta>
            <ta e="T75" id="Seg_1263" s="T72">на лодке катались</ta>
            <ta e="T91" id="Seg_1264" s="T75">потом в лапту играли потом поели нова поиграли потом в палатке с ребенком поиграли</ta>
            <ta e="T102" id="Seg_1265" s="T91">в дом играли потом с детьми отдельно в лапту играли потом потом</ta>
            <ta e="T108" id="Seg_1266" s="T102">в лапту играли в догонялки играли</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T0" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
