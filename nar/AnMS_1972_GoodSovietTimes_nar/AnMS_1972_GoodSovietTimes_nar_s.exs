<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDFC265541-F976-4C9B-82C0-D17EE2009F87">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnMS_1972_GoodSovietTimes_nar</transcription-name>
         <referenced-file url="AnMS_1972_GoodSovietTimes_nar.wav" />
         <referenced-file url="AnMS_1972_GoodSovietTimes_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\AnMS_1972_GoodSovietTimes_nar\AnMS_1972_GoodSovietTimes_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">772</ud-information>
            <ud-information attribute-name="# HIAT:w">612</ud-information>
            <ud-information attribute-name="# e">610</ud-information>
            <ud-information attribute-name="# HIAT:u">74</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnMS">
            <abbreviation>AnMS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="2.17" type="appl" />
         <tli id="T1" time="2.901" type="appl" />
         <tli id="T2" time="3.631" type="appl" />
         <tli id="T3" time="4.362" type="appl" />
         <tli id="T4" time="5.093" type="appl" />
         <tli id="T5" time="5.824" type="appl" />
         <tli id="T6" time="6.554" type="appl" />
         <tli id="T7" time="7.285" type="appl" />
         <tli id="T8" time="7.831" type="appl" />
         <tli id="T9" time="8.377" type="appl" />
         <tli id="T10" time="8.922" type="appl" />
         <tli id="T11" time="9.468" type="appl" />
         <tli id="T12" time="10.014" type="appl" />
         <tli id="T13" time="10.886506950168435" />
         <tli id="T14" time="11.023" type="appl" />
         <tli id="T15" time="11.486" type="appl" />
         <tli id="T16" time="11.95" type="appl" />
         <tli id="T17" time="12.413" type="appl" />
         <tli id="T18" time="12.876" type="appl" />
         <tli id="T19" time="14.593119236937357" />
         <tli id="T20" time="14.618746157958238" type="intp" />
         <tli id="T21" time="14.644373078979118" type="intp" />
         <tli id="T22" time="14.67" type="appl" />
         <tli id="T23" time="15.114" type="appl" />
         <tli id="T24" time="15.558" type="appl" />
         <tli id="T25" time="16.002" type="appl" />
         <tli id="T26" time="16.446" type="appl" />
         <tli id="T27" time="16.89" type="appl" />
         <tli id="T28" time="17.333" type="appl" />
         <tli id="T29" time="17.777" type="appl" />
         <tli id="T30" time="18.221" type="appl" />
         <tli id="T31" time="18.665" type="appl" />
         <tli id="T32" time="19.109" type="appl" />
         <tli id="T33" time="19.553" type="appl" />
         <tli id="T34" time="19.996" type="appl" />
         <tli id="T35" time="20.44" type="appl" />
         <tli id="T36" time="21.946344690725347" />
         <tli id="T37" time="21.999172345362673" type="intp" />
         <tli id="T38" time="22.052" type="appl" />
         <tli id="T39" time="22.636" type="appl" />
         <tli id="T40" time="23.22" type="appl" />
         <tli id="T41" time="23.804" type="appl" />
         <tli id="T42" time="24.388" type="appl" />
         <tli id="T43" time="24.972" type="appl" />
         <tli id="T44" time="25.556" type="appl" />
         <tli id="T45" time="26.14" type="appl" />
         <tli id="T46" time="26.724" type="appl" />
         <tli id="T47" time="27.308" type="appl" />
         <tli id="T48" time="27.892" type="appl" />
         <tli id="T49" time="28.476" type="appl" />
         <tli id="T50" time="29.06" type="appl" />
         <tli id="T51" time="29.644" type="appl" />
         <tli id="T52" time="30.228" type="appl" />
         <tli id="T53" time="31.9728642578125" />
         <tli id="T54" time="31.98943212890625" type="intp" />
         <tli id="T55" time="32.006" type="appl" />
         <tli id="T56" time="32.603" type="appl" />
         <tli id="T57" time="33.2" type="appl" />
         <tli id="T58" time="33.797" type="appl" />
         <tli id="T59" time="34.394" type="appl" />
         <tli id="T60" time="34.992" type="appl" />
         <tli id="T61" time="35.589" type="appl" />
         <tli id="T62" time="36.186" type="appl" />
         <tli id="T63" time="36.783" type="appl" />
         <tli id="T64" time="37.38" type="appl" />
         <tli id="T65" time="38.57276743029673" />
         <tli id="T66" time="38.72" type="appl" />
         <tli id="T67" time="39.464" type="appl" />
         <tli id="T68" time="40.207" type="appl" />
         <tli id="T69" time="40.951" type="appl" />
         <tli id="T70" time="41.694" type="appl" />
         <tli id="T71" time="42.438" type="appl" />
         <tli id="T72" time="43.182" type="appl" />
         <tli id="T73" time="43.925" type="appl" />
         <tli id="T74" time="44.444" type="appl" />
         <tli id="T75" time="44.963" type="appl" />
         <tli id="T76" time="45.482" type="appl" />
         <tli id="T77" time="46.001" type="appl" />
         <tli id="T78" time="46.40000285694358" />
         <tli id="T79" time="47.253" type="appl" />
         <tli id="T80" time="47.986" type="appl" />
         <tli id="T81" time="48.719" type="appl" />
         <tli id="T82" time="49.452" type="appl" />
         <tli id="T83" time="50.185" type="appl" />
         <tli id="T84" time="51.319247092710725" />
         <tli id="T85" time="51.605" type="appl" />
         <tli id="T86" time="52.291" type="appl" />
         <tli id="T87" time="52.978" type="appl" />
         <tli id="T88" time="53.664" type="appl" />
         <tli id="T89" time="54.351" type="appl" />
         <tli id="T90" time="55.037" type="appl" />
         <tli id="T91" time="55.724" type="appl" />
         <tli id="T92" time="56.41" type="appl" />
         <tli id="T93" time="57.097" type="appl" />
         <tli id="T94" time="57.783" type="appl" />
         <tli id="T95" time="58.410002438877704" />
         <tli id="T96" time="59.924" type="appl" />
         <tli id="T97" time="61.372335280424956" />
         <tli id="T98" time="62.189" type="appl" />
         <tli id="T99" time="62.999" type="appl" />
         <tli id="T100" time="63.808" type="appl" />
         <tli id="T101" time="64.618" type="appl" />
         <tli id="T102" time="65.428" type="appl" />
         <tli id="T103" time="66.54569037145212" />
         <tli id="T104" time="66.829" type="appl" />
         <tli id="T105" time="67.42" type="appl" />
         <tli id="T106" time="68.011" type="appl" />
         <tli id="T107" time="68.602" type="appl" />
         <tli id="T108" time="69.194" type="appl" />
         <tli id="T109" time="69.785" type="appl" />
         <tli id="T110" time="70.376" type="appl" />
         <tli id="T111" time="72.03227654413347" />
         <tli id="T112" time="72.07113827206673" type="intp" />
         <tli id="T113" time="72.11" type="appl" />
         <tli id="T114" time="72.682" type="appl" />
         <tli id="T115" time="73.253" type="appl" />
         <tli id="T116" time="73.824" type="appl" />
         <tli id="T117" time="74.396" type="appl" />
         <tli id="T118" time="74.968" type="appl" />
         <tli id="T119" time="75.539" type="appl" />
         <tli id="T120" time="76.11" type="appl" />
         <tli id="T121" time="76.682" type="appl" />
         <tli id="T122" time="77.254" type="appl" />
         <tli id="T123" time="77.825" type="appl" />
         <tli id="T124" time="78.413" type="appl" />
         <tli id="T125" time="79.0" type="appl" />
         <tli id="T126" time="79.588" type="appl" />
         <tli id="T127" time="80.176" type="appl" />
         <tli id="T128" time="80.763" type="appl" />
         <tli id="T129" time="81.351" type="appl" />
         <tli id="T130" time="81.939" type="appl" />
         <tli id="T131" time="82.526" type="appl" />
         <tli id="T132" time="83.114" type="appl" />
         <tli id="T133" time="83.702" type="appl" />
         <tli id="T134" time="84.29" type="appl" />
         <tli id="T135" time="84.877" type="appl" />
         <tli id="T136" time="85.465" type="appl" />
         <tli id="T137" time="86.053" type="appl" />
         <tli id="T138" time="86.64" type="appl" />
         <tli id="T139" time="88.4520356490109" />
         <tli id="T140" time="88.67451782450544" type="intp" />
         <tli id="T141" time="88.897" type="appl" />
         <tli id="T142" time="89.731" type="appl" />
         <tli id="T143" time="90.566" type="appl" />
         <tli id="T144" time="91.33199339700401" />
         <tli id="T145" time="92.204" type="appl" />
         <tli id="T146" time="93.008" type="appl" />
         <tli id="T147" time="93.812" type="appl" />
         <tli id="T148" time="94.617" type="appl" />
         <tli id="T149" time="95.421" type="appl" />
         <tli id="T150" time="96.6319156415747" />
         <tli id="T151" time="96.889" type="appl" />
         <tli id="T152" time="97.553" type="appl" />
         <tli id="T153" time="98.218" type="appl" />
         <tli id="T154" time="98.882" type="appl" />
         <tli id="T155" time="99.546" type="appl" />
         <tli id="T156" time="100.232" type="appl" />
         <tli id="T157" time="100.917" type="appl" />
         <tli id="T158" time="101.603" type="appl" />
         <tli id="T159" time="102.289" type="appl" />
         <tli id="T160" time="102.974" type="appl" />
         <tli id="T161" time="103.66" type="appl" />
         <tli id="T162" time="103.989" type="appl" />
         <tli id="T163" time="104.318" type="appl" />
         <tli id="T164" time="104.647" type="appl" />
         <tli id="T165" time="104.977" type="appl" />
         <tli id="T166" time="105.306" type="appl" />
         <tli id="T167" time="105.635" type="appl" />
         <tli id="T168" time="105.964" type="appl" />
         <tli id="T169" time="106.657" type="appl" />
         <tli id="T170" time="107.35" type="appl" />
         <tli id="T171" time="108.043" type="appl" />
         <tli id="T172" time="108.737" type="appl" />
         <tli id="T173" time="109.43" type="appl" />
         <tli id="T174" time="110.123" type="appl" />
         <tli id="T175" time="110.62267392411363" />
         <tli id="T176" time="111.323" type="appl" />
         <tli id="T177" time="111.83" type="appl" />
         <tli id="T178" time="112.336" type="appl" />
         <tli id="T179" time="112.843" type="appl" />
         <tli id="T180" time="113.96499468042215" />
         <tli id="T181" time="114.67165097969824" type="intp" />
         <tli id="T182" time="115.37830727897433" type="intp" />
         <tli id="T183" time="116.08496357825042" type="intp" />
         <tli id="T184" time="116.79161987752651" type="intp" />
         <tli id="T185" time="117.4982761768026" type="intp" />
         <tli id="T186" time="118.2049324760787" type="intp" />
         <tli id="T187" time="118.9115887753548" type="intp" />
         <tli id="T188" time="119.61824507463089" />
         <tli id="T189" time="120.114" type="appl" />
         <tli id="T190" time="120.895" type="appl" />
         <tli id="T191" time="121.677" type="appl" />
         <tli id="T192" time="122.459" type="appl" />
         <tli id="T193" time="123.24" type="appl" />
         <tli id="T194" time="123.99532929570522" />
         <tli id="T195" time="124.527" type="appl" />
         <tli id="T196" time="125.032" type="appl" />
         <tli id="T197" time="125.536" type="appl" />
         <tli id="T198" time="126.40481217522579" />
         <tli id="T199" time="126.627" type="appl" />
         <tli id="T200" time="127.214" type="appl" />
         <tli id="T201" time="127.8" type="appl" />
         <tli id="T202" time="128.386" type="appl" />
         <tli id="T203" time="129.056" type="appl" />
         <tli id="T204" time="129.727" type="appl" />
         <tli id="T205" time="130.35700679654374" />
         <tli id="T206" time="131.026" type="appl" />
         <tli id="T207" time="131.655" type="appl" />
         <tli id="T208" time="132.284" type="appl" />
         <tli id="T209" time="132.913" type="appl" />
         <tli id="T210" time="134.17136489840166" />
         <tli id="T211" time="134.624" type="appl" />
         <tli id="T212" time="135.706" type="appl" />
         <tli id="T213" time="136.788" type="appl" />
         <tli id="T214" time="137.871" type="appl" />
         <tli id="T215" time="138.953" type="appl" />
         <tli id="T216" time="139.78461587944383" />
         <tli id="T217" time="140.69" type="appl" />
         <tli id="T218" time="141.344" type="appl" />
         <tli id="T219" time="141.999" type="appl" />
         <tli id="T220" time="142.654" type="appl" />
         <tli id="T221" time="143.309" type="appl" />
         <tli id="T222" time="143.963" type="appl" />
         <tli id="T223" time="144.618" type="appl" />
         <tli id="T224" time="145.273" type="appl" />
         <tli id="T225" time="145.928" type="appl" />
         <tli id="T226" time="146.583" type="appl" />
         <tli id="T227" time="147.238" type="appl" />
         <tli id="T228" time="147.893" type="appl" />
         <tli id="T229" time="148.548" type="appl" />
         <tli id="T230" time="149.203" type="appl" />
         <tli id="T231" time="149.858" type="appl" />
         <tli id="T232" time="150.8396620236318" />
         <tli id="T233" time="151.239" type="appl" />
         <tli id="T234" time="151.964" type="appl" />
         <tli id="T235" time="152.69" type="appl" />
         <tli id="T236" time="153.416" type="appl" />
         <tli id="T237" time="154.142" type="appl" />
         <tli id="T238" time="154.867" type="appl" />
         <tli id="T239" time="156.75770019979214" />
         <tli id="T240" time="156.76785009989607" type="intp" />
         <tli id="T241" time="156.778" type="appl" />
         <tli id="T242" time="157.371" type="appl" />
         <tli id="T243" time="157.964" type="appl" />
         <tli id="T244" time="158.557" type="appl" />
         <tli id="T245" time="159.149" type="appl" />
         <tli id="T246" time="159.742" type="appl" />
         <tli id="T247" time="160.232" type="appl" />
         <tli id="T248" time="160.723" type="appl" />
         <tli id="T249" time="161.213" type="appl" />
         <tli id="T250" time="161.704" type="appl" />
         <tli id="T251" time="162.194" type="appl" />
         <tli id="T252" time="162.684" type="appl" />
         <tli id="T253" time="163.175" type="appl" />
         <tli id="T254" time="163.665" type="appl" />
         <tli id="T255" time="164.156" type="appl" />
         <tli id="T256" time="165.91756581493692" />
         <tli id="T257" time="165.93678290746846" type="intp" />
         <tli id="T258" time="165.956" type="appl" />
         <tli id="T259" time="166.611" type="appl" />
         <tli id="T260" time="167.266" type="appl" />
         <tli id="T261" time="167.921" type="appl" />
         <tli id="T262" time="168.577" type="appl" />
         <tli id="T263" time="169.232" type="appl" />
         <tli id="T264" time="169.887" type="appl" />
         <tli id="T265" time="170.542" type="appl" />
         <tli id="T266" time="171.197" type="appl" />
         <tli id="T267" time="171.6320002073454" />
         <tli id="T268" time="172.273" type="appl" />
         <tli id="T269" time="172.694" type="appl" />
         <tli id="T270" time="173.115" type="appl" />
         <tli id="T271" time="173.536" type="appl" />
         <tli id="T272" time="173.957" type="appl" />
         <tli id="T273" time="174.525" type="appl" />
         <tli id="T274" time="175.092" type="appl" />
         <tli id="T275" time="175.66" type="appl" />
         <tli id="T276" time="176.228" type="appl" />
         <tli id="T277" time="176.795" type="appl" />
         <tli id="T278" time="177.363" type="appl" />
         <tli id="T279" time="177.931" type="appl" />
         <tli id="T280" time="178.498" type="appl" />
         <tli id="T281" time="179.066" type="appl" />
         <tli id="T282" time="179.634" type="appl" />
         <tli id="T283" time="180.201" type="appl" />
         <tli id="T284" time="180.769" type="appl" />
         <tli id="T285" time="181.336" type="appl" />
         <tli id="T286" time="181.904" type="appl" />
         <tli id="T287" time="182.472" type="appl" />
         <tli id="T288" time="183.039" type="appl" />
         <tli id="T289" time="183.607" type="appl" />
         <tli id="T290" time="184.175" type="appl" />
         <tli id="T291" time="184.742" type="appl" />
         <tli id="T292" time="185.4500006077574" />
         <tli id="T293" time="186.176" type="appl" />
         <tli id="T294" time="187.042" type="appl" />
         <tli id="T295" time="187.907" type="appl" />
         <tli id="T296" time="188.773" type="appl" />
         <tli id="T297" time="189.639" type="appl" />
         <tli id="T298" time="190.505" type="appl" />
         <tli id="T299" time="191.37" type="appl" />
         <tli id="T300" time="192.236" type="appl" />
         <tli id="T301" time="193.20199625283425" />
         <tli id="T302" time="193.638" type="appl" />
         <tli id="T303" time="194.173" type="appl" />
         <tli id="T304" time="194.709" type="appl" />
         <tli id="T305" time="195.245" type="appl" />
         <tli id="T306" time="195.78" type="appl" />
         <tli id="T307" time="197.15710749802895" />
         <tli id="T308" time="197.1765537490145" type="intp" />
         <tli id="T309" time="197.196" type="appl" />
         <tli id="T310" time="197.636" type="appl" />
         <tli id="T311" time="198.076" type="appl" />
         <tli id="T312" time="198.516" type="appl" />
         <tli id="T313" time="198.956" type="appl" />
         <tli id="T314" time="199.395" type="appl" />
         <tli id="T315" time="199.835" type="appl" />
         <tli id="T316" time="200.275" type="appl" />
         <tli id="T317" time="200.715" type="appl" />
         <tli id="T318" time="201.155" type="appl" />
         <tli id="T319" time="202.7570253413489" />
         <tli id="T320" time="202.85001267067446" type="intp" />
         <tli id="T321" time="202.943" type="appl" />
         <tli id="T322" time="203.617" type="appl" />
         <tli id="T323" time="204.291" type="appl" />
         <tli id="T324" time="204.966" type="appl" />
         <tli id="T325" time="205.64" type="appl" />
         <tli id="T326" time="206.314" type="appl" />
         <tli id="T327" time="207.5169555081709" />
         <tli id="T328" time="208.01480508446545" type="intp" />
         <tli id="T329" time="208.51265466076" type="intp" />
         <tli id="T330" time="209.01050423705456" type="intp" />
         <tli id="T331" time="209.50835381334912" type="intp" />
         <tli id="T332" time="210.00620338964367" type="intp" />
         <tli id="T333" time="210.50405296593823" type="intp" />
         <tli id="T334" time="211.00190254223276" type="intp" />
         <tli id="T335" time="211.49975211852728" type="intp" />
         <tli id="T336" time="211.99760169482184" type="intp" />
         <tli id="T337" time="212.4954512711164" type="intp" />
         <tli id="T338" time="212.99330084741092" type="intp" />
         <tli id="T339" time="213.49115042370545" type="intp" />
         <tli id="T340" time="213.989" type="appl" />
         <tli id="T341" time="214.49" type="appl" />
         <tli id="T342" time="214.99" type="appl" />
         <tli id="T343" time="215.491" type="appl" />
         <tli id="T344" time="215.992" type="appl" />
         <tli id="T345" time="216.713" type="appl" />
         <tli id="T346" time="217.434" type="appl" />
         <tli id="T347" time="218.155" type="appl" />
         <tli id="T348" time="218.875" type="appl" />
         <tli id="T349" time="219.596" type="appl" />
         <tli id="T350" time="220.317" type="appl" />
         <tli id="T351" time="221.038" type="appl" />
         <tli id="T352" time="221.485" type="appl" />
         <tli id="T353" time="221.931" type="appl" />
         <tli id="T354" time="222.378" type="appl" />
         <tli id="T355" time="222.824" type="appl" />
         <tli id="T356" time="223.271" type="appl" />
         <tli id="T357" time="223.717" type="appl" />
         <tli id="T358" time="224.164" type="appl" />
         <tli id="T359" time="224.83667536801156" />
         <tli id="T619" time="224.84" type="intp" />
         <tli id="T360" time="225.07" type="appl" />
         <tli id="T361" time="225.529" type="appl" />
         <tli id="T362" time="225.989" type="appl" />
         <tli id="T363" time="226.448" type="appl" />
         <tli id="T364" time="226.908" type="appl" />
         <tli id="T365" time="227.367" type="appl" />
         <tli id="T366" time="227.827" type="appl" />
         <tli id="T367" time="228.286" type="appl" />
         <tli id="T368" time="228.746" type="appl" />
         <tli id="T369" time="229.205" type="appl" />
         <tli id="T370" time="229.665" type="appl" />
         <tli id="T371" time="230.124" type="appl" />
         <tli id="T372" time="230.75067714505676" />
         <tli id="T373" time="231.252" type="appl" />
         <tli id="T374" time="231.921" type="appl" />
         <tli id="T375" time="232.589" type="appl" />
         <tli id="T376" time="233.258" type="appl" />
         <tli id="T377" time="233.93932828086014" />
         <tli id="T378" time="234.307" type="appl" />
         <tli id="T379" time="234.688" type="appl" />
         <tli id="T380" time="235.07" type="appl" />
         <tli id="T381" time="235.451" type="appl" />
         <tli id="T382" time="235.832" type="appl" />
         <tli id="T383" time="236.25965882250586" />
         <tli id="T384" time="236.763" type="appl" />
         <tli id="T385" time="237.313" type="appl" />
         <tli id="T386" time="237.863" type="appl" />
         <tli id="T387" time="238.413" type="appl" />
         <tli id="T388" time="238.963" type="appl" />
         <tli id="T389" time="239.513" type="appl" />
         <tli id="T390" time="240.063" type="appl" />
         <tli id="T391" time="240.613" type="appl" />
         <tli id="T392" time="241.163" type="appl" />
         <tli id="T393" time="241.713" type="appl" />
         <tli id="T394" time="242.372" type="appl" />
         <tli id="T395" time="243.032" type="appl" />
         <tli id="T396" time="243.691" type="appl" />
         <tli id="T397" time="244.35" type="appl" />
         <tli id="T398" time="245.009" type="appl" />
         <tli id="T399" time="245.669" type="appl" />
         <tli id="T400" time="246.328" type="appl" />
         <tli id="T401" time="246.987" type="appl" />
         <tli id="T402" time="247.646" type="appl" />
         <tli id="T403" time="248.306" type="appl" />
         <tli id="T404" time="249.644983279062" />
         <tli id="T405" time="249.648" type="appl" />
         <tli id="T406" time="250.331" type="appl" />
         <tli id="T407" time="251.014" type="appl" />
         <tli id="T408" time="251.697" type="appl" />
         <tli id="T409" time="252.38" type="appl" />
         <tli id="T410" time="253.56961320061637" />
         <tli id="T411" time="254.069" type="appl" />
         <tli id="T412" time="255.075" type="appl" />
         <tli id="T413" time="256.7695662539421" />
         <tli id="T414" time="256.829" type="appl" />
         <tli id="T415" time="257.578" type="appl" />
         <tli id="T416" time="258.326" type="appl" />
         <tli id="T417" time="259.074" type="appl" />
         <tli id="T418" time="259.823" type="appl" />
         <tli id="T419" time="260.571" type="appl" />
         <tli id="T420" time="261.32" type="appl" />
         <tli id="T421" time="262.068" type="appl" />
         <tli id="T422" time="262.816" type="appl" />
         <tli id="T423" time="263.565" type="appl" />
         <tli id="T424" time="264.25966469965937" />
         <tli id="T425" time="264.613" type="appl" />
         <tli id="T426" time="264.913" type="appl" />
         <tli id="T427" time="265.213" type="appl" />
         <tli id="T428" time="265.513" type="appl" />
         <tli id="T429" time="265.812" type="appl" />
         <tli id="T430" time="266.112" type="appl" />
         <tli id="T431" time="266.412" type="appl" />
         <tli id="T432" time="266.712" type="appl" />
         <tli id="T433" time="267.012" type="appl" />
         <tli id="T434" time="267.312" type="appl" />
         <tli id="T435" time="267.612" type="appl" />
         <tli id="T436" time="267.912" type="appl" />
         <tli id="T437" time="268.211" type="appl" />
         <tli id="T438" time="268.511" type="appl" />
         <tli id="T439" time="268.811" type="appl" />
         <tli id="T440" time="269.111" type="appl" />
         <tli id="T441" time="270.276034766521" />
         <tli id="T442" time="270.323" type="appl" />
         <tli id="T443" time="271.235" type="appl" />
         <tli id="T444" time="272.147" type="appl" />
         <tli id="T445" time="273.059" type="appl" />
         <tli id="T446" time="273.971" type="appl" />
         <tli id="T447" time="274.883" type="appl" />
         <tli id="T448" time="275.666" type="appl" />
         <tli id="T449" time="276.448" type="appl" />
         <tli id="T450" time="277.231" type="appl" />
         <tli id="T451" time="278.013" type="appl" />
         <tli id="T452" time="278.796" type="appl" />
         <tli id="T453" time="279.579" type="appl" />
         <tli id="T454" time="280.361" type="appl" />
         <tli id="T455" time="281.144" type="appl" />
         <tli id="T456" time="281.926" type="appl" />
         <tli id="T457" time="282.709" type="appl" />
         <tli id="T458" time="283.199" type="appl" />
         <tli id="T459" time="283.69" type="appl" />
         <tli id="T460" time="284.18" type="appl" />
         <tli id="T461" time="284.671" type="appl" />
         <tli id="T462" time="284.99433445859614" />
         <tli id="T463" time="285.77" type="appl" />
         <tli id="T464" time="286.378" type="appl" />
         <tli id="T465" time="286.987" type="appl" />
         <tli id="T466" time="287.596" type="appl" />
         <tli id="T467" time="288.205" type="appl" />
         <tli id="T468" time="288.813" type="appl" />
         <tli id="T469" time="289.422" type="appl" />
         <tli id="T470" time="290.031" type="appl" />
         <tli id="T471" time="290.639" type="appl" />
         <tli id="T472" time="291.248" type="appl" />
         <tli id="T473" time="291.857" type="appl" />
         <tli id="T474" time="292.466" type="appl" />
         <tli id="T475" time="293.074" type="appl" />
         <tli id="T476" time="293.683" type="appl" />
         <tli id="T477" time="294.356" type="appl" />
         <tli id="T478" time="295.03" type="appl" />
         <tli id="T479" time="295.703" type="appl" />
         <tli id="T480" time="296.376" type="appl" />
         <tli id="T481" time="297.05" type="appl" />
         <tli id="T482" time="297.723" type="appl" />
         <tli id="T483" time="298.479" type="appl" />
         <tli id="T484" time="299.236" type="appl" />
         <tli id="T485" time="299.89867308795124" />
         <tli id="T486" time="300.417" type="appl" />
         <tli id="T487" time="300.843" type="appl" />
         <tli id="T488" time="301.268" type="appl" />
         <tli id="T489" time="301.694" type="appl" />
         <tli id="T490" time="302.119" type="appl" />
         <tli id="T491" time="302.544" type="appl" />
         <tli id="T492" time="302.97" type="appl" />
         <tli id="T493" time="303.395" type="appl" />
         <tli id="T494" time="303.82" type="appl" />
         <tli id="T495" time="304.246" type="appl" />
         <tli id="T496" time="304.671" type="appl" />
         <tli id="T497" time="305.097" type="appl" />
         <tli id="T498" time="305.388670668917" />
         <tli id="T499" time="306.073" type="appl" />
         <tli id="T500" time="306.625" type="appl" />
         <tli id="T501" time="307.176" type="appl" />
         <tli id="T502" time="307.655" type="appl" />
         <tli id="T503" time="308.134" type="appl" />
         <tli id="T504" time="308.613" type="appl" />
         <tli id="T505" time="309.092" type="appl" />
         <tli id="T506" time="309.571" type="appl" />
         <tli id="T507" time="310.05" type="appl" />
         <tli id="T508" time="310.53" type="appl" />
         <tli id="T509" time="311.009" type="appl" />
         <tli id="T510" time="311.488" type="appl" />
         <tli id="T511" time="311.967" type="appl" />
         <tli id="T512" time="312.446" type="appl" />
         <tli id="T513" time="312.925" type="appl" />
         <tli id="T514" time="313.754" type="appl" />
         <tli id="T515" time="314.583" type="appl" />
         <tli id="T516" time="315.411" type="appl" />
         <tli id="T517" time="316.24" type="appl" />
         <tli id="T518" time="317.069" type="appl" />
         <tli id="T519" time="317.898" type="appl" />
         <tli id="T520" time="318.726" type="appl" />
         <tli id="T521" time="319.555" type="appl" />
         <tli id="T522" time="320.384" type="appl" />
         <tli id="T523" time="321.033" type="appl" />
         <tli id="T524" time="321.683" type="appl" />
         <tli id="T525" time="322.332" type="appl" />
         <tli id="T526" time="322.981" type="appl" />
         <tli id="T527" time="323.631" type="appl" />
         <tli id="T528" time="324.28" type="appl" />
         <tli id="T529" time="324.929" type="appl" />
         <tli id="T530" time="325.578" type="appl" />
         <tli id="T531" time="326.228" type="appl" />
         <tli id="T532" time="326.877" type="appl" />
         <tli id="T533" time="327.526" type="appl" />
         <tli id="T534" time="328.176" type="appl" />
         <tli id="T535" time="328.825" type="appl" />
         <tli id="T536" time="329.545" type="appl" />
         <tli id="T537" time="330.264" type="appl" />
         <tli id="T538" time="330.984" type="appl" />
         <tli id="T539" time="331.704" type="appl" />
         <tli id="T540" time="332.424" type="appl" />
         <tli id="T541" time="333.143" type="appl" />
         <tli id="T542" time="333.863" type="appl" />
         <tli id="T543" time="334.583" type="appl" />
         <tli id="T544" time="335.302" type="appl" />
         <tli id="T545" time="336.022" type="appl" />
         <tli id="T546" time="336.406" type="appl" />
         <tli id="T547" time="336.79" type="appl" />
         <tli id="T548" time="337.174" type="appl" />
         <tli id="T549" time="337.557" type="appl" />
         <tli id="T550" time="337.941" type="appl" />
         <tli id="T551" time="338.325" type="appl" />
         <tli id="T552" time="338.729" type="appl" />
         <tli id="T553" time="339.132" type="appl" />
         <tli id="T554" time="339.536" type="appl" />
         <tli id="T555" time="339.939" type="appl" />
         <tli id="T556" time="340.343" type="appl" />
         <tli id="T557" time="340.746" type="appl" />
         <tli id="T558" time="341.15" type="appl" />
         <tli id="T559" time="341.554" type="appl" />
         <tli id="T560" time="341.957" type="appl" />
         <tli id="T561" time="342.361" type="appl" />
         <tli id="T562" time="342.764" type="appl" />
         <tli id="T563" time="343.168" type="appl" />
         <tli id="T564" time="343.571" type="appl" />
         <tli id="T565" time="343.975" type="appl" />
         <tli id="T566" time="344.45" type="appl" />
         <tli id="T567" time="344.926" type="appl" />
         <tli id="T568" time="345.401" type="appl" />
         <tli id="T569" time="345.877" type="appl" />
         <tli id="T570" time="346.352" type="appl" />
         <tli id="T571" time="346.828" type="appl" />
         <tli id="T572" time="347.303" type="appl" />
         <tli id="T573" time="347.779" type="appl" />
         <tli id="T574" time="348.254" type="appl" />
         <tli id="T575" time="348.758" type="appl" />
         <tli id="T576" time="349.261" type="appl" />
         <tli id="T577" time="349.765" type="appl" />
         <tli id="T578" time="350.268" type="appl" />
         <tli id="T579" time="350.772" type="appl" />
         <tli id="T580" time="351.275" type="appl" />
         <tli id="T581" time="351.779" type="appl" />
         <tli id="T582" time="352.282" type="appl" />
         <tli id="T583" time="352.48149539268564" />
         <tli id="T584" time="353.257" type="appl" />
         <tli id="T585" time="353.729" type="appl" />
         <tli id="T586" time="354.74146223659693" />
         <tli id="T587" time="354.767" type="appl" />
         <tli id="T588" time="355.334" type="appl" />
         <tli id="T589" time="355.901" type="appl" />
         <tli id="T590" time="356.468" type="appl" />
         <tli id="T591" time="357.036" type="appl" />
         <tli id="T592" time="357.603" type="appl" />
         <tli id="T593" time="358.17" type="appl" />
         <tli id="T594" time="358.737" type="appl" />
         <tli id="T595" time="359.31733260376956" />
         <tli id="T596" time="359.765" type="appl" />
         <tli id="T597" time="360.226" type="appl" />
         <tli id="T598" time="360.687" type="appl" />
         <tli id="T599" time="361.148" type="appl" />
         <tli id="T600" time="361.608" type="appl" />
         <tli id="T601" time="362.069" type="appl" />
         <tli id="T602" time="362.53" type="appl" />
         <tli id="T603" time="362.991" type="appl" />
         <tli id="T604" time="363.452" type="appl" />
         <tli id="T605" time="363.85966179635983" />
         <tli id="T606" time="364.54" type="appl" />
         <tli id="T607" time="365.167" type="appl" />
         <tli id="T608" time="365.794" type="appl" />
         <tli id="T609" time="366.42" type="appl" />
         <tli id="T610" time="367.047" type="appl" />
         <tli id="T611" time="367.674" type="appl" />
         <tli id="T612" time="368.301" type="appl" />
         <tli id="T613" time="368.928" type="appl" />
         <tli id="T614" time="369.555" type="appl" />
         <tli id="T615" time="370.181" type="appl" />
         <tli id="T616" time="370.808" type="appl" />
         <tli id="T617" time="371.435" type="appl" />
         <tli id="T618" time="372.062" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AnMS"
                      type="t">
         <timeline-fork end="T25" start="T23">
            <tli id="T23.tx.1" />
         </timeline-fork>
         <timeline-fork end="T29" start="T27">
            <tli id="T27.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T618" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Habi͡eskaj</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">bɨlaːs</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">bu͡olarɨn</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">bagas</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">össü͡ö</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">daːganɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">öjdöːböppün</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Oččogo</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">min</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">daːganɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">dolu͡oj</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">ogo</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">etim</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_47" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">Kannɨk</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">da</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">vlaːska</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">üleleːbet</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">ogo</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">etim</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_68" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">Habi͡eskaj</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">bɨlaːs</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">bu͡olar</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">di͡en</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23.tx.1" id="Seg_82" n="HIAT:w" s="T23">tak</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T23.tx.1">i</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">ɨjaːk</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">ergillere</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27.tx.1" id="Seg_95" n="HIAT:w" s="T27">tak</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T27.tx.1">i</ts>
                  <nts id="Seg_99" n="HIAT:ip">,</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_102" n="HIAT:w" s="T29">hin</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_105" n="HIAT:w" s="T30">daːganɨ</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_108" n="HIAT:w" s="T31">tu͡olkuluːr</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_111" n="HIAT:w" s="T32">kihi</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_114" n="HIAT:w" s="T33">tu͡olkuluːr</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_117" n="HIAT:w" s="T34">kördük</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_120" n="HIAT:w" s="T35">ete</ts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_124" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_126" n="HIAT:w" s="T36">ɨjaːk</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_129" n="HIAT:w" s="T37">ergilliːtin</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_132" n="HIAT:w" s="T38">hagɨna</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_135" n="HIAT:w" s="T39">maŋnaj</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_138" n="HIAT:w" s="T40">kihini</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_141" n="HIAT:w" s="T41">urut</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_144" n="HIAT:w" s="T42">baːj</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_147" n="HIAT:w" s="T43">kihi</ts>
                  <nts id="Seg_148" n="HIAT:ip">,</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_151" n="HIAT:w" s="T44">kulaːk</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_154" n="HIAT:w" s="T45">kihi</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_157" n="HIAT:w" s="T46">battɨːr</ts>
                  <nts id="Seg_158" n="HIAT:ip">,</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_161" n="HIAT:w" s="T47">kɨlgahɨ</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_164" n="HIAT:w" s="T48">kɨ͡aragahɨ</ts>
                  <nts id="Seg_165" n="HIAT:ip">,</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_168" n="HIAT:w" s="T49">dʼadagɨnɨ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_171" n="HIAT:w" s="T50">di͡en</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_174" n="HIAT:w" s="T51">aːttɨːr</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_177" n="HIAT:w" s="T52">etilere</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_181" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_183" n="HIAT:w" s="T53">Oččogo</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_186" n="HIAT:w" s="T54">du͡o</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_189" n="HIAT:w" s="T55">min</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_192" n="HIAT:w" s="T56">bu</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_195" n="HIAT:w" s="T57">baːj</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_198" n="HIAT:w" s="T58">kihilerge</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_201" n="HIAT:w" s="T59">maččɨkkaːn</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_204" n="HIAT:w" s="T60">etim</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_207" n="HIAT:w" s="T61">vsʼo</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_210" n="HIAT:w" s="T62">takʼi</ts>
                  <nts id="Seg_211" n="HIAT:ip">,</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_214" n="HIAT:w" s="T63">tabahɨt</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_217" n="HIAT:w" s="T64">etim</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_221" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_223" n="HIAT:w" s="T65">Ol</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_226" n="HIAT:w" s="T66">betereː</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_229" n="HIAT:w" s="T67">öttüger</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_232" n="HIAT:w" s="T68">onton</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_235" n="HIAT:w" s="T69">tu͡olkulaːn</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_238" n="HIAT:w" s="T70">istim</ts>
                  <nts id="Seg_239" n="HIAT:ip">,</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_242" n="HIAT:w" s="T71">bilen</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_245" n="HIAT:w" s="T72">istim</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_249" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_251" n="HIAT:w" s="T73">Kanʼešna</ts>
                  <nts id="Seg_252" n="HIAT:ip">,</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_255" n="HIAT:w" s="T74">kirdik</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_259" n="HIAT:w" s="T75">ɨjaːk</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_262" n="HIAT:w" s="T76">ularɨjar</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_265" n="HIAT:w" s="T77">ebit</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_269" n="HIAT:u" s="T78">
                  <nts id="Seg_270" n="HIAT:ip">"</nts>
                  <ts e="T79" id="Seg_272" n="HIAT:w" s="T78">Urukku</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_275" n="HIAT:w" s="T79">sarskaj</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_278" n="HIAT:w" s="T80">ɨjaːk</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_281" n="HIAT:w" s="T81">dʼe</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_284" n="HIAT:w" s="T82">bütte</ts>
                  <nts id="Seg_285" n="HIAT:ip">"</nts>
                  <nts id="Seg_286" n="HIAT:ip">,</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_289" n="HIAT:w" s="T83">di͡ebittere</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_293" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_295" n="HIAT:w" s="T84">Onton</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_298" n="HIAT:w" s="T85">bettek</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_301" n="HIAT:w" s="T86">uhuguttan</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_304" n="HIAT:w" s="T87">usku͡olaga</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_307" n="HIAT:w" s="T88">ü͡öreni͡eger</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_310" n="HIAT:w" s="T89">di͡en</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_313" n="HIAT:w" s="T90">tavarʼišʼ</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_316" n="HIAT:w" s="T91">Lʼenʼin</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_319" n="HIAT:w" s="T92">ehe</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_322" n="HIAT:w" s="T93">di͡en</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_325" n="HIAT:w" s="T94">ete</ts>
                  <nts id="Seg_326" n="HIAT:ip">.</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_329" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_331" n="HIAT:w" s="T95">Lʼenʼin</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_334" n="HIAT:w" s="T96">zakona</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_338" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_340" n="HIAT:w" s="T97">Dʼe</ts>
                  <nts id="Seg_341" n="HIAT:ip">,</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_344" n="HIAT:w" s="T98">ularɨjan-ularɨjan</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_347" n="HIAT:w" s="T99">ol</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_350" n="HIAT:w" s="T100">ularɨjan</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_353" n="HIAT:w" s="T101">iste</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_356" n="HIAT:w" s="T102">uhuguttan</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_360" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_362" n="HIAT:w" s="T103">Kihi</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_365" n="HIAT:w" s="T104">barɨta</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_368" n="HIAT:w" s="T105">kɨlgas</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_371" n="HIAT:w" s="T106">kɨ͡aragas</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_374" n="HIAT:w" s="T107">ogoto</ts>
                  <nts id="Seg_375" n="HIAT:ip">,</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_378" n="HIAT:w" s="T108">kaja</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_381" n="HIAT:w" s="T109">da</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_384" n="HIAT:w" s="T110">jon</ts>
                  <nts id="Seg_385" n="HIAT:ip">.</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_388" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_390" n="HIAT:w" s="T111">Kannɨk</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_393" n="HIAT:w" s="T112">daːganɨ</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_396" n="HIAT:w" s="T113">kɨlgas</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_399" n="HIAT:w" s="T114">kɨ͡aragas</ts>
                  <nts id="Seg_400" n="HIAT:ip">,</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_403" n="HIAT:w" s="T115">dʼadaŋɨ</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_406" n="HIAT:w" s="T116">kihi</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_409" n="HIAT:w" s="T117">ogoto</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_412" n="HIAT:w" s="T118">barɨta</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_415" n="HIAT:w" s="T119">ü͡örekke</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_418" n="HIAT:w" s="T120">da</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_421" n="HIAT:w" s="T121">dʼe</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_424" n="HIAT:w" s="T122">načʼinajdaːta</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_428" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_430" n="HIAT:w" s="T123">O</ts>
                  <nts id="Seg_431" n="HIAT:ip">,</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_434" n="HIAT:w" s="T124">hol</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_437" n="HIAT:w" s="T125">ereːri</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_441" n="HIAT:w" s="T126">baːjtan</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_444" n="HIAT:w" s="T127">ahɨː</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_447" n="HIAT:w" s="T128">ü͡öremmitter</ts>
                  <nts id="Seg_448" n="HIAT:ip">,</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_451" n="HIAT:w" s="T129">kulaːktan</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_454" n="HIAT:w" s="T130">abɨrana</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_457" n="HIAT:w" s="T131">ü͡öremmitter</ts>
                  <nts id="Seg_458" n="HIAT:ip">,</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_461" n="HIAT:w" s="T132">sin</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_464" n="HIAT:w" s="T133">daːganɨ</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_467" n="HIAT:w" s="T134">usku͡olaga</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_470" n="HIAT:w" s="T135">bi͡eri͡ekpitin</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_473" n="HIAT:w" s="T136">di͡ebet</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_476" n="HIAT:w" s="T137">etilere</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_479" n="HIAT:w" s="T138">ogonu</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_483" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_485" n="HIAT:w" s="T139">Onton</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_488" n="HIAT:w" s="T140">bu͡olbuta</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_491" n="HIAT:w" s="T141">kolkoz</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_494" n="HIAT:w" s="T142">bu͡olu͡okka</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_497" n="HIAT:w" s="T143">di͡en</ts>
                  <nts id="Seg_498" n="HIAT:ip">.</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_501" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_503" n="HIAT:w" s="T144">Obšʼij</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_506" n="HIAT:w" s="T145">xazʼajstvannan</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_509" n="HIAT:w" s="T146">dʼon</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_512" n="HIAT:w" s="T147">ologun</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_515" n="HIAT:w" s="T148">tuttu͡ogun</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_518" n="HIAT:w" s="T149">di͡enner</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_522" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_524" n="HIAT:w" s="T150">Hu͡ok</ts>
                  <nts id="Seg_525" n="HIAT:ip">,</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_528" n="HIAT:w" s="T151">onuga</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_531" n="HIAT:w" s="T152">emi͡e</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_534" n="HIAT:w" s="T153">bu͡ola</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_537" n="HIAT:w" s="T154">tarpataktara</ts>
                  <nts id="Seg_538" n="HIAT:ip">.</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_541" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_543" n="HIAT:w" s="T155">Kas</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_546" n="HIAT:w" s="T156">da</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_549" n="HIAT:w" s="T157">dʼɨlɨnan</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_551" n="HIAT:ip">(</nts>
                  <ts e="T159" id="Seg_553" n="HIAT:w" s="T158">bu͡ol-</ts>
                  <nts id="Seg_554" n="HIAT:ip">)</nts>
                  <nts id="Seg_555" n="HIAT:ip">,</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_558" n="HIAT:w" s="T159">kolkoz</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_561" n="HIAT:w" s="T160">bu͡olbataktara</ts>
                  <nts id="Seg_562" n="HIAT:ip">.</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_565" n="HIAT:u" s="T161">
                  <nts id="Seg_566" n="HIAT:ip">"</nts>
                  <ts e="T162" id="Seg_568" n="HIAT:w" s="T161">Kolkoz</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_571" n="HIAT:w" s="T162">bu͡olammɨt</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_574" n="HIAT:w" s="T163">bihigi</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_577" n="HIAT:w" s="T164">hatanɨ͡akpɨt</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_580" n="HIAT:w" s="T165">hu͡o</ts>
                  <nts id="Seg_581" n="HIAT:ip">"</nts>
                  <nts id="Seg_582" n="HIAT:ip">,</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_585" n="HIAT:w" s="T166">diːr</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_588" n="HIAT:w" s="T167">etilere</ts>
                  <nts id="Seg_589" n="HIAT:ip">.</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_592" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_594" n="HIAT:w" s="T168">Urut</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_597" n="HIAT:w" s="T169">sarskaj</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_600" n="HIAT:w" s="T170">zakoŋŋa</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_603" n="HIAT:w" s="T171">ü͡öskeːbit</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_606" n="HIAT:w" s="T172">dʼon</ts>
                  <nts id="Seg_607" n="HIAT:ip">,</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_610" n="HIAT:w" s="T173">ü͡öremmit</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_613" n="HIAT:w" s="T174">dʼon</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_617" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_619" n="HIAT:w" s="T175">Onton</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_622" n="HIAT:w" s="T176">kajdak</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_625" n="HIAT:w" s="T177">ere</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_628" n="HIAT:w" s="T178">gɨnan</ts>
                  <nts id="Seg_629" n="HIAT:ip">…</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_632" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_634" n="HIAT:w" s="T180">Kepseːn</ts>
                  <nts id="Seg_635" n="HIAT:ip">,</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_638" n="HIAT:w" s="T181">kepseːnner</ts>
                  <nts id="Seg_639" n="HIAT:ip">,</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_642" n="HIAT:w" s="T182">hoku͡on</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_645" n="HIAT:w" s="T183">kelen-kelen</ts>
                  <nts id="Seg_646" n="HIAT:ip">,</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_649" n="HIAT:w" s="T184">hubu</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_652" n="HIAT:w" s="T185">dojdularga</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_655" n="HIAT:w" s="T186">kelen</ts>
                  <nts id="Seg_656" n="HIAT:ip">,</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_659" n="HIAT:w" s="T187">kelen</ts>
                  <nts id="Seg_660" n="HIAT:ip">.</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_663" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_665" n="HIAT:w" s="T188">Kolkoz</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_668" n="HIAT:w" s="T189">bu͡ollubut</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_671" n="HIAT:w" s="T190">otut</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_674" n="HIAT:w" s="T191">settisteːgitten</ts>
                  <nts id="Seg_675" n="HIAT:ip">,</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_678" n="HIAT:w" s="T192">otut</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_681" n="HIAT:w" s="T193">agɨstaːkka</ts>
                  <nts id="Seg_682" n="HIAT:ip">.</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_685" n="HIAT:u" s="T194">
                  <ts e="T196" id="Seg_687" n="HIAT:w" s="T194">Kаnʼec-takɨ</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_690" n="HIAT:w" s="T196">kirdik</ts>
                  <nts id="Seg_691" n="HIAT:ip">.</nts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_694" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_696" n="HIAT:w" s="T198">Dʼe</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_699" n="HIAT:w" s="T199">kolkoz</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_702" n="HIAT:w" s="T200">bu͡ol</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_705" n="HIAT:w" s="T201">načʼinajdaːn</ts>
                  <nts id="Seg_706" n="HIAT:ip">.</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_709" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_711" n="HIAT:w" s="T202">Ustrojnaːn</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_713" n="HIAT:ip">(</nts>
                  <ts e="T204" id="Seg_715" n="HIAT:w" s="T203">kaːl-</ts>
                  <nts id="Seg_716" n="HIAT:ip">)</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_719" n="HIAT:w" s="T204">uhuguttan</ts>
                  <nts id="Seg_720" n="HIAT:ip">.</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_723" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_725" n="HIAT:w" s="T205">Oččogo</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_728" n="HIAT:w" s="T206">kolkoːhu</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_731" n="HIAT:w" s="T207">törömmüppüt</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_734" n="HIAT:w" s="T208">ete</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_737" n="HIAT:w" s="T209">bihigi</ts>
                  <nts id="Seg_738" n="HIAT:ip">.</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_741" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_743" n="HIAT:w" s="T210">Kanna</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_746" n="HIAT:w" s="T211">orduk</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_749" n="HIAT:w" s="T212">tabalaːktan</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_752" n="HIAT:w" s="T213">onuga</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_755" n="HIAT:w" s="T214">kolkozka</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_758" n="HIAT:w" s="T215">tuppupput</ts>
                  <nts id="Seg_759" n="HIAT:ip">.</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_762" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_764" n="HIAT:w" s="T216">Kanna</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_767" n="HIAT:w" s="T217">paːstaːkpɨtɨn</ts>
                  <nts id="Seg_768" n="HIAT:ip">,</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_771" n="HIAT:w" s="T218">honon</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_774" n="HIAT:w" s="T219">bejebit</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_777" n="HIAT:w" s="T220">paːspɨtɨnan</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_780" n="HIAT:w" s="T221">bejebit</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_783" n="HIAT:w" s="T222">kapkaːmmɨtɨnan</ts>
                  <nts id="Seg_784" n="HIAT:ip">.</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_787" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_789" n="HIAT:w" s="T223">Uhuguttan</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_792" n="HIAT:w" s="T224">kolku͡o</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_795" n="HIAT:w" s="T225">ologun</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_798" n="HIAT:w" s="T226">tutammɨt</ts>
                  <nts id="Seg_799" n="HIAT:ip">,</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_802" n="HIAT:w" s="T227">anɨ</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_805" n="HIAT:w" s="T228">bu</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_808" n="HIAT:w" s="T229">kemnerge</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_811" n="HIAT:w" s="T230">tijdibit</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_814" n="HIAT:w" s="T231">dʼe</ts>
                  <nts id="Seg_815" n="HIAT:ip">.</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_818" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_820" n="HIAT:w" s="T232">Dʼe</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_823" n="HIAT:w" s="T233">onton</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_826" n="HIAT:w" s="T234">kolkoz</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_829" n="HIAT:w" s="T235">ologo</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_832" n="HIAT:w" s="T236">üčügej</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_835" n="HIAT:w" s="T237">duː</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_838" n="HIAT:w" s="T238">di͡en</ts>
                  <nts id="Seg_839" n="HIAT:ip">.</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_842" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_844" n="HIAT:w" s="T239">Ol</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_847" n="HIAT:w" s="T240">kördük</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_850" n="HIAT:w" s="T241">bu͡olla</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_853" n="HIAT:w" s="T242">bu͡olbutunan</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_856" n="HIAT:w" s="T243">dʼe</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_859" n="HIAT:w" s="T244">kolku͡os</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_862" n="HIAT:w" s="T245">bu͡olbupput</ts>
                  <nts id="Seg_863" n="HIAT:ip">.</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_866" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_868" n="HIAT:w" s="T246">Ol</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_871" n="HIAT:w" s="T247">betereː</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_874" n="HIAT:w" s="T248">öttüger</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_877" n="HIAT:w" s="T249">emi͡e</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_880" n="HIAT:w" s="T250">daːganɨ</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_883" n="HIAT:w" s="T251">erejdeːk</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_886" n="HIAT:w" s="T252">kɨhalgalaːk</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_889" n="HIAT:w" s="T253">üjeler</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_892" n="HIAT:w" s="T254">bu͡olar</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_895" n="HIAT:w" s="T255">etilere</ts>
                  <nts id="Seg_896" n="HIAT:ip">.</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_899" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_901" n="HIAT:w" s="T256">Baː</ts>
                  <nts id="Seg_902" n="HIAT:ip">,</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_905" n="HIAT:w" s="T257">kulaːkka</ts>
                  <nts id="Seg_906" n="HIAT:ip">,</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_909" n="HIAT:w" s="T258">urukku</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_912" n="HIAT:w" s="T259">sarskajga</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_915" n="HIAT:w" s="T260">bastarɨn</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_918" n="HIAT:w" s="T261">ɨstarbɨt</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_921" n="HIAT:w" s="T262">kihiler</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_924" n="HIAT:w" s="T263">tam</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_927" n="HIAT:w" s="T264">baldiːttɨːr</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_930" n="HIAT:w" s="T265">da</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_933" n="HIAT:w" s="T266">etilere</ts>
                  <nts id="Seg_934" n="HIAT:ip">.</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_937" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_939" n="HIAT:w" s="T267">Olor</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_942" n="HIAT:w" s="T268">sin</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_945" n="HIAT:w" s="T269">ereji</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_948" n="HIAT:w" s="T270">oŋoroːčču</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_951" n="HIAT:w" s="T271">etiler</ts>
                  <nts id="Seg_952" n="HIAT:ip">.</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_955" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_957" n="HIAT:w" s="T272">Ol</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_960" n="HIAT:w" s="T273">baldʼiːtɨ</ts>
                  <nts id="Seg_961" n="HIAT:ip">,</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_964" n="HIAT:w" s="T274">hu͡ok</ts>
                  <nts id="Seg_965" n="HIAT:ip">,</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_968" n="HIAT:w" s="T275">ol</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_971" n="HIAT:w" s="T276">aːtɨ</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_974" n="HIAT:w" s="T277">savʼeskaj</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_977" n="HIAT:w" s="T278">bɨlaːs</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_980" n="HIAT:w" s="T279">inni</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_983" n="HIAT:w" s="T280">di͡ek</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_986" n="HIAT:w" s="T281">kaːman</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_989" n="HIAT:w" s="T282">ihen</ts>
                  <nts id="Seg_990" n="HIAT:ip">,</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_993" n="HIAT:w" s="T283">ol</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_996" n="HIAT:w" s="T284">baldʼiːtɨ</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_999" n="HIAT:w" s="T285">hin</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1002" n="HIAT:w" s="T286">daːganɨ</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1005" n="HIAT:w" s="T287">ɨraːstaːččɨ</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1008" n="HIAT:w" s="T288">ete</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1011" n="HIAT:w" s="T289">dʼahajaːččɨ</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1014" n="HIAT:w" s="T290">da</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1017" n="HIAT:w" s="T291">ete</ts>
                  <nts id="Seg_1018" n="HIAT:ip">.</nts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1021" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1023" n="HIAT:w" s="T292">Tʼe</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1026" n="HIAT:w" s="T293">ol</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1029" n="HIAT:w" s="T294">büppütün</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1032" n="HIAT:w" s="T295">kenne</ts>
                  <nts id="Seg_1033" n="HIAT:ip">,</nts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1036" n="HIAT:w" s="T296">uhuguttan</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1039" n="HIAT:w" s="T297">bu</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1042" n="HIAT:w" s="T298">Gʼermanʼija</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1045" n="HIAT:w" s="T299">heriːte</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1048" n="HIAT:w" s="T300">bu͡olbuta</ts>
                  <nts id="Seg_1049" n="HIAT:ip">.</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1052" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1054" n="HIAT:w" s="T301">Dʼe</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1057" n="HIAT:w" s="T302">itinige</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1060" n="HIAT:w" s="T303">kɨhalla</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1063" n="HIAT:w" s="T304">hɨspɨppɨt</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1066" n="HIAT:w" s="T305">bihigi</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1069" n="HIAT:w" s="T306">hürdeːk</ts>
                  <nts id="Seg_1070" n="HIAT:ip">.</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1073" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1075" n="HIAT:w" s="T307">Ol</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1078" n="HIAT:w" s="T308">taŋas</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1081" n="HIAT:w" s="T309">daː</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1084" n="HIAT:w" s="T310">terijiːte</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1087" n="HIAT:w" s="T311">bu͡olar</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1090" n="HIAT:w" s="T312">ete</ts>
                  <nts id="Seg_1091" n="HIAT:ip">,</nts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1094" n="HIAT:w" s="T313">ol</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1097" n="HIAT:w" s="T314">karčɨnnan</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1100" n="HIAT:w" s="T315">daː</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1103" n="HIAT:w" s="T316">nalog</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1106" n="HIAT:w" s="T317">bu͡olar</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1109" n="HIAT:w" s="T318">ete</ts>
                  <nts id="Seg_1110" n="HIAT:ip">.</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1113" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1115" n="HIAT:w" s="T319">Bihigi</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1118" n="HIAT:w" s="T320">ol</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1121" n="HIAT:w" s="T321">di͡ek</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1125" n="HIAT:w" s="T322">gu͡orat</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1128" n="HIAT:w" s="T323">hirderge</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1131" n="HIAT:w" s="T324">armijaga</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1134" n="HIAT:w" s="T325">kömölöhömmüt</ts>
                  <nts id="Seg_1135" n="HIAT:ip">…</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1138" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1140" n="HIAT:w" s="T327">Onton</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1143" n="HIAT:w" s="T328">hɨl</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1146" n="HIAT:w" s="T329">büten</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1149" n="HIAT:w" s="T330">dʼe</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1152" n="HIAT:w" s="T331">anɨ</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1155" n="HIAT:w" s="T332">tu͡ok</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1158" n="HIAT:w" s="T333">da</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1161" n="HIAT:w" s="T334">bu͡olu͡okput</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1164" n="HIAT:w" s="T335">di͡ebeppin</ts>
                  <nts id="Seg_1165" n="HIAT:ip">,</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1168" n="HIAT:w" s="T336">bügüŋŋü</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1171" n="HIAT:w" s="T337">hanaːm</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1174" n="HIAT:w" s="T338">üčügej</ts>
                  <nts id="Seg_1175" n="HIAT:ip">.</nts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1178" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1180" n="HIAT:w" s="T339">Anɨ</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1183" n="HIAT:w" s="T340">olok</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1186" n="HIAT:w" s="T341">könnö</ts>
                  <nts id="Seg_1187" n="HIAT:ip">,</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1190" n="HIAT:w" s="T342">olok</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1193" n="HIAT:w" s="T343">ulaːtta</ts>
                  <nts id="Seg_1194" n="HIAT:ip">.</nts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1197" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1199" n="HIAT:w" s="T344">Anɨ</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1202" n="HIAT:w" s="T345">bütün</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1205" n="HIAT:w" s="T346">kolku͡ostar</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1208" n="HIAT:w" s="T347">uhuguttan</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1211" n="HIAT:w" s="T348">argijaːsija</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1214" n="HIAT:w" s="T349">kördük</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1217" n="HIAT:w" s="T350">bu͡ollular</ts>
                  <nts id="Seg_1218" n="HIAT:ip">.</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1221" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1223" n="HIAT:w" s="T351">Anɨ</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1226" n="HIAT:w" s="T352">mantan</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1229" n="HIAT:w" s="T353">inni</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1232" n="HIAT:w" s="T354">di͡egitin</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1235" n="HIAT:w" s="T355">bilbeppin</ts>
                  <nts id="Seg_1236" n="HIAT:ip">,</nts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1239" n="HIAT:w" s="T356">bejem</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1242" n="HIAT:w" s="T357">kɨrdʼan</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1245" n="HIAT:w" s="T358">kaːllɨm</ts>
                  <nts id="Seg_1246" n="HIAT:ip">.</nts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1249" n="HIAT:u" s="T359">
                  <ts e="T619" id="Seg_1251" n="HIAT:w" s="T359">Tak</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1254" n="HIAT:w" s="T619">i</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1257" n="HIAT:w" s="T360">tak</ts>
                  <nts id="Seg_1258" n="HIAT:ip">,</nts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1261" n="HIAT:w" s="T361">min</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1264" n="HIAT:w" s="T362">hanaːbar</ts>
                  <nts id="Seg_1265" n="HIAT:ip">,</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1268" n="HIAT:w" s="T363">üčügej</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1271" n="HIAT:w" s="T364">olokko</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1274" n="HIAT:w" s="T365">anɨ</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1277" n="HIAT:w" s="T366">tijdibit</ts>
                  <nts id="Seg_1278" n="HIAT:ip">,</nts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1281" n="HIAT:w" s="T367">diːbin</ts>
                  <nts id="Seg_1282" n="HIAT:ip">,</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1285" n="HIAT:w" s="T368">hubu</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1288" n="HIAT:w" s="T369">kɨrdʼar</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1291" n="HIAT:w" s="T370">haːspar</ts>
                  <nts id="Seg_1292" n="HIAT:ip">.</nts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1295" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1297" n="HIAT:w" s="T372">Anɨ</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1300" n="HIAT:w" s="T373">tot</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1303" n="HIAT:w" s="T374">bagajbɨt</ts>
                  <nts id="Seg_1304" n="HIAT:ip">,</nts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1307" n="HIAT:w" s="T375">ičiges</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1310" n="HIAT:w" s="T376">bagajbɨt</ts>
                  <nts id="Seg_1311" n="HIAT:ip">.</nts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1314" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1316" n="HIAT:w" s="T377">Taŋahɨ</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1319" n="HIAT:w" s="T378">daː</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1322" n="HIAT:w" s="T379">taŋnabɨt</ts>
                  <nts id="Seg_1323" n="HIAT:ip">,</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1326" n="HIAT:w" s="T380">ahɨ</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1329" n="HIAT:w" s="T381">daː</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1332" n="HIAT:w" s="T382">ahɨːbɨt</ts>
                  <nts id="Seg_1333" n="HIAT:ip">.</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1336" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1338" n="HIAT:w" s="T383">Bu</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1341" n="HIAT:w" s="T384">ka</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1344" n="HIAT:w" s="T385">maŋnaj</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1347" n="HIAT:w" s="T386">mini͡ene</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1350" n="HIAT:w" s="T387">bu</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1353" n="HIAT:w" s="T388">ulakan</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1356" n="HIAT:w" s="T389">ogom</ts>
                  <nts id="Seg_1357" n="HIAT:ip">,</nts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1360" n="HIAT:w" s="T390">bu</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1363" n="HIAT:w" s="T391">Bahiːlaj</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1366" n="HIAT:w" s="T392">baːr</ts>
                  <nts id="Seg_1367" n="HIAT:ip">.</nts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1370" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_1372" n="HIAT:w" s="T393">Bu</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1375" n="HIAT:w" s="T394">ogom</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1378" n="HIAT:w" s="T395">ulaːtta</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1381" n="HIAT:w" s="T396">da</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1384" n="HIAT:w" s="T397">ü͡öremmite</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1387" n="HIAT:w" s="T398">giniː</ts>
                  <nts id="Seg_1388" n="HIAT:ip">,</nts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1391" n="HIAT:w" s="T399">usku͡olatɨn</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1394" n="HIAT:w" s="T400">büterde</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1397" n="HIAT:w" s="T401">daːganɨ</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1400" n="HIAT:w" s="T402">učiːtalga</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1403" n="HIAT:w" s="T403">ü͡öremmite</ts>
                  <nts id="Seg_1404" n="HIAT:ip">.</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1407" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1409" n="HIAT:w" s="T404">Onon</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1412" n="HIAT:w" s="T405">anɨ</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1415" n="HIAT:w" s="T406">kün</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1418" n="HIAT:w" s="T407">bügün</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1421" n="HIAT:w" s="T408">učitallɨː</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1424" n="HIAT:w" s="T409">oloror</ts>
                  <nts id="Seg_1425" n="HIAT:ip">.</nts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_1428" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1430" n="HIAT:w" s="T410">Onton</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1433" n="HIAT:w" s="T411">biːr</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1436" n="HIAT:w" s="T412">kɨːstaːkpɨn</ts>
                  <nts id="Seg_1437" n="HIAT:ip">.</nts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1440" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_1442" n="HIAT:w" s="T413">Ol</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1445" n="HIAT:w" s="T414">emi͡e</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1448" n="HIAT:w" s="T415">ü͡öregin</ts>
                  <nts id="Seg_1449" n="HIAT:ip">,</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1452" n="HIAT:w" s="T416">usku͡olatɨn</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1455" n="HIAT:w" s="T417">büteren</ts>
                  <nts id="Seg_1456" n="HIAT:ip">,</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1459" n="HIAT:w" s="T418">emi͡e</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1462" n="HIAT:w" s="T419">bɨssaj</ts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1465" n="HIAT:w" s="T420">ü͡örenen</ts>
                  <nts id="Seg_1466" n="HIAT:ip">,</nts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1469" n="HIAT:w" s="T421">du͡okturdaːn</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1472" n="HIAT:w" s="T422">oloror</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1475" n="HIAT:w" s="T423">anɨ</ts>
                  <nts id="Seg_1476" n="HIAT:ip">.</nts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1479" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1481" n="HIAT:w" s="T424">Ol</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1484" n="HIAT:w" s="T425">bejetin</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1487" n="HIAT:w" s="T426">inniger</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1490" n="HIAT:w" s="T427">üčügej</ts>
                  <nts id="Seg_1491" n="HIAT:ip">,</nts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1494" n="HIAT:w" s="T428">ol</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1497" n="HIAT:w" s="T429">kenne</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1500" n="HIAT:w" s="T430">min</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1503" n="HIAT:w" s="T431">daː</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1506" n="HIAT:w" s="T432">hanaːbar</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1509" n="HIAT:w" s="T433">üčügej</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1512" n="HIAT:w" s="T434">anɨ</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1514" n="HIAT:ip">–</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1517" n="HIAT:w" s="T435">bu</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1520" n="HIAT:w" s="T436">olokko</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1523" n="HIAT:w" s="T437">oloruːlara</ts>
                  <nts id="Seg_1524" n="HIAT:ip">,</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1527" n="HIAT:w" s="T438">anɨ</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1530" n="HIAT:w" s="T439">bu</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1533" n="HIAT:w" s="T440">ogolorum</ts>
                  <nts id="Seg_1534" n="HIAT:ip">.</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1537" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1539" n="HIAT:w" s="T441">Onuga</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1542" n="HIAT:w" s="T442">ühüs</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1545" n="HIAT:w" s="T443">emi͡e</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1548" n="HIAT:w" s="T444">u͡olum</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1551" n="HIAT:w" s="T445">avijporka</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1554" n="HIAT:w" s="T446">üleliːr</ts>
                  <nts id="Seg_1555" n="HIAT:ip">.</nts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1558" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1560" n="HIAT:w" s="T447">Onton</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1563" n="HIAT:w" s="T448">muŋ</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1566" n="HIAT:w" s="T449">kuččuguj</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1569" n="HIAT:w" s="T450">u͡ollaːppɨn</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1572" n="HIAT:w" s="T451">anɨ</ts>
                  <nts id="Seg_1573" n="HIAT:ip">,</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1576" n="HIAT:w" s="T452">Ujbaːn</ts>
                  <nts id="Seg_1577" n="HIAT:ip">,</nts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1580" n="HIAT:w" s="T453">Antonаv</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1583" n="HIAT:w" s="T454">Ujbaːn</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1585" n="HIAT:ip">(</nts>
                  <ts e="T457" id="Seg_1587" n="HIAT:w" s="T455">Mʼixʼeis-</ts>
                  <nts id="Seg_1588" n="HIAT:ip">)</nts>
                  <nts id="Seg_1589" n="HIAT:ip">.</nts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1592" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1594" n="HIAT:w" s="T457">Ol</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1597" n="HIAT:w" s="T458">du͡o</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1600" n="HIAT:w" s="T459">anɨ</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1603" n="HIAT:w" s="T460">kolku͡oska</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1606" n="HIAT:w" s="T461">üleliːr</ts>
                  <nts id="Seg_1607" n="HIAT:ip">.</nts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T476" id="Seg_1610" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1612" n="HIAT:w" s="T462">Kolku͡os</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1615" n="HIAT:w" s="T463">daːganɨ</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1618" n="HIAT:w" s="T464">üleliːre</ts>
                  <nts id="Seg_1619" n="HIAT:ip">,</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1622" n="HIAT:w" s="T465">ülete</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1625" n="HIAT:w" s="T466">ületin</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1628" n="HIAT:w" s="T467">daː</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1631" n="HIAT:w" s="T468">üleleːk</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1634" n="HIAT:w" s="T469">kolku͡os</ts>
                  <nts id="Seg_1635" n="HIAT:ip">,</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1638" n="HIAT:w" s="T470">hin</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1641" n="HIAT:w" s="T471">daːganɨ</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1644" n="HIAT:w" s="T472">bejetin</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1647" n="HIAT:w" s="T473">kolku͡os</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1650" n="HIAT:w" s="T474">ihiger</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1653" n="HIAT:w" s="T475">hin</ts>
                  <nts id="Seg_1654" n="HIAT:ip">.</nts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1657" n="HIAT:u" s="T476">
                  <ts e="T477" id="Seg_1659" n="HIAT:w" s="T476">Üčügejdik</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1662" n="HIAT:w" s="T477">totoron</ts>
                  <nts id="Seg_1663" n="HIAT:ip">,</nts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1666" n="HIAT:w" s="T478">taŋɨnnaran</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1669" n="HIAT:w" s="T479">hin</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1672" n="HIAT:w" s="T480">da</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1675" n="HIAT:w" s="T481">üleleter</ts>
                  <nts id="Seg_1676" n="HIAT:ip">.</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_1679" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1681" n="HIAT:w" s="T482">Hin</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1684" n="HIAT:w" s="T483">daː</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1687" n="HIAT:w" s="T484">harpalaːttaːk</ts>
                  <nts id="Seg_1688" n="HIAT:ip">.</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_1691" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1693" n="HIAT:w" s="T485">Kihi</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1696" n="HIAT:w" s="T486">kanna</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1699" n="HIAT:w" s="T487">učugu͡oduk</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1702" n="HIAT:w" s="T488">üleliːre</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1705" n="HIAT:w" s="T489">ol</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1708" n="HIAT:w" s="T490">orduk</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1711" n="HIAT:w" s="T491">bu͡olar</ts>
                  <nts id="Seg_1712" n="HIAT:ip">,</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1715" n="HIAT:w" s="T492">kanna</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1718" n="HIAT:w" s="T493">kuhagannɨk</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1721" n="HIAT:w" s="T494">üleliːre</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1724" n="HIAT:w" s="T495">ol</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1727" n="HIAT:w" s="T496">niːze</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1730" n="HIAT:w" s="T497">bu͡olar</ts>
                  <nts id="Seg_1731" n="HIAT:ip">.</nts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_1734" n="HIAT:u" s="T498">
                  <ts e="T499" id="Seg_1736" n="HIAT:w" s="T498">Min</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1739" n="HIAT:w" s="T499">hanaːbar</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1742" n="HIAT:w" s="T500">hitigirdik</ts>
                  <nts id="Seg_1743" n="HIAT:ip">.</nts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_1746" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1748" n="HIAT:w" s="T501">Bu</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1751" n="HIAT:w" s="T502">ke</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1754" n="HIAT:w" s="T503">maŋnaj</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1757" n="HIAT:w" s="T504">bejem</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1760" n="HIAT:w" s="T505">hin</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1763" n="HIAT:w" s="T506">daːganɨ</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1766" n="HIAT:w" s="T507">bu</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1769" n="HIAT:w" s="T508">sʼelʼsavʼetɨnnan</ts>
                  <nts id="Seg_1770" n="HIAT:ip">,</nts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1773" n="HIAT:w" s="T509">tugunan</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1776" n="HIAT:w" s="T510">rukavostvavalaːn</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1779" n="HIAT:w" s="T511">oloror</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1782" n="HIAT:w" s="T512">etim</ts>
                  <nts id="Seg_1783" n="HIAT:ip">.</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T522" id="Seg_1786" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_1788" n="HIAT:w" s="T513">Onton</ts>
                  <nts id="Seg_1789" n="HIAT:ip">,</nts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1792" n="HIAT:w" s="T514">anɨ</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1795" n="HIAT:w" s="T515">ogo</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1798" n="HIAT:w" s="T516">kihiler</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1801" n="HIAT:w" s="T517">ü͡örekke</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1804" n="HIAT:w" s="T518">ü͡örenenner</ts>
                  <nts id="Seg_1805" n="HIAT:ip">,</nts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1808" n="HIAT:w" s="T519">miniginneːger</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1811" n="HIAT:w" s="T520">luːčče</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1814" n="HIAT:w" s="T521">üleliːhiler</ts>
                  <nts id="Seg_1815" n="HIAT:ip">.</nts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T535" id="Seg_1818" n="HIAT:u" s="T522">
                  <ts e="T523" id="Seg_1820" n="HIAT:w" s="T522">Oččogo</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1823" n="HIAT:w" s="T523">du͡o</ts>
                  <nts id="Seg_1824" n="HIAT:ip">,</nts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1827" n="HIAT:w" s="T524">kimi͡eke</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1830" n="HIAT:w" s="T525">daːganɨ</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1833" n="HIAT:w" s="T526">buruj</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1836" n="HIAT:w" s="T527">haŋata</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1839" n="HIAT:w" s="T528">hu͡ok</ts>
                  <nts id="Seg_1840" n="HIAT:ip">,</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1842" n="HIAT:ip">"</nts>
                  <ts e="T530" id="Seg_1844" n="HIAT:w" s="T529">anɨ</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1847" n="HIAT:w" s="T530">hɨnnʼanɨ͡am</ts>
                  <nts id="Seg_1848" n="HIAT:ip">"</nts>
                  <nts id="Seg_1849" n="HIAT:ip">,</nts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1852" n="HIAT:w" s="T531">di͡emmin</ts>
                  <nts id="Seg_1853" n="HIAT:ip">,</nts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1856" n="HIAT:w" s="T532">selsavʼettaːn</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1859" n="HIAT:w" s="T533">büppütüm</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1862" n="HIAT:w" s="T534">ete</ts>
                  <nts id="Seg_1863" n="HIAT:ip">.</nts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_1866" n="HIAT:u" s="T535">
                  <ts e="T536" id="Seg_1868" n="HIAT:w" s="T535">Anɨ</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1871" n="HIAT:w" s="T536">bu</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1874" n="HIAT:w" s="T537">ogolorum</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1877" n="HIAT:w" s="T538">ü͡örekke</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1880" n="HIAT:w" s="T539">ü͡örenenner</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1883" n="HIAT:w" s="T540">üčügejdik</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1885" n="HIAT:ip">(</nts>
                  <ts e="T543" id="Seg_1887" n="HIAT:w" s="T541">olo-</ts>
                  <nts id="Seg_1888" n="HIAT:ip">)</nts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1891" n="HIAT:w" s="T543">olorollor</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1894" n="HIAT:w" s="T544">onton</ts>
                  <nts id="Seg_1895" n="HIAT:ip">.</nts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_1898" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_1900" n="HIAT:w" s="T545">Anɨ</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1903" n="HIAT:w" s="T546">ü͡örene</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1906" n="HIAT:w" s="T547">olorobun</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1909" n="HIAT:w" s="T548">bu</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1912" n="HIAT:w" s="T549">kɨrdʼan</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1915" n="HIAT:w" s="T550">baraːmmɨn</ts>
                  <nts id="Seg_1916" n="HIAT:ip">.</nts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_1919" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_1921" n="HIAT:w" s="T551">Bejem</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1924" n="HIAT:w" s="T552">ü͡örege</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1927" n="HIAT:w" s="T553">hu͡ok</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1930" n="HIAT:w" s="T554">bagaj</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1933" n="HIAT:w" s="T555">etim</ts>
                  <nts id="Seg_1934" n="HIAT:ip">,</nts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1937" n="HIAT:w" s="T556">ol</ts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1940" n="HIAT:w" s="T557">kenne</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1943" n="HIAT:w" s="T558">daže</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1946" n="HIAT:w" s="T559">min</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1949" n="HIAT:w" s="T560">tɨlɨ</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1952" n="HIAT:w" s="T561">da</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1955" n="HIAT:w" s="T562">kɨ͡ajan</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1958" n="HIAT:w" s="T563">istibeppin</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1960" n="HIAT:ip">–</nts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1963" n="HIAT:w" s="T564">etim</ts>
                  <nts id="Seg_1964" n="HIAT:ip">.</nts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_1967" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_1969" n="HIAT:w" s="T565">Kaččaga</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1972" n="HIAT:w" s="T566">ol</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1975" n="HIAT:w" s="T567">min</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1978" n="HIAT:w" s="T568">sʼelʼsavʼet</ts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1981" n="HIAT:w" s="T569">erdekpinen</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1984" n="HIAT:w" s="T570">oː</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1987" n="HIAT:w" s="T571">tu͡ok</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1990" n="HIAT:w" s="T572">eme</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1993" n="HIAT:w" s="T573">pʼerʼevoːččigunan</ts>
                  <nts id="Seg_1994" n="HIAT:ip">.</nts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_1997" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_1999" n="HIAT:w" s="T574">Biːr</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2002" n="HIAT:w" s="T575">eme</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2005" n="HIAT:w" s="T576">tɨlkaːnɨ</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2008" n="HIAT:w" s="T577">kohu͡on</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2011" n="HIAT:w" s="T578">kihi</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2014" n="HIAT:w" s="T579">haŋatɨnan</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2017" n="HIAT:w" s="T580">ergeteːčči</ts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2020" n="HIAT:w" s="T581">etibit</ts>
                  <nts id="Seg_2021" n="HIAT:ip">,</nts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2024" n="HIAT:w" s="T582">pʼerʼevoːččigunan</ts>
                  <nts id="Seg_2025" n="HIAT:ip">.</nts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T586" id="Seg_2028" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_2030" n="HIAT:w" s="T583">Ol</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2033" n="HIAT:w" s="T584">erejdeːk</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2036" n="HIAT:w" s="T585">ete</ts>
                  <nts id="Seg_2037" n="HIAT:ip">.</nts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2040" n="HIAT:u" s="T586">
                  <ts e="T587" id="Seg_2042" n="HIAT:w" s="T586">Anɨ</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2045" n="HIAT:w" s="T587">ereje</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2048" n="HIAT:w" s="T588">hu͡ok</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2051" n="HIAT:w" s="T589">bejelere</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2054" n="HIAT:w" s="T590">sɨraj</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2057" n="HIAT:w" s="T591">kepseteller</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2060" n="HIAT:w" s="T592">kimi</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2063" n="HIAT:w" s="T593">da</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2066" n="HIAT:w" s="T594">gɨtta</ts>
                  <nts id="Seg_2067" n="HIAT:ip">.</nts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_2070" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_2072" n="HIAT:w" s="T595">Savʼeskaj</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2075" n="HIAT:w" s="T596">bɨlaːs</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2078" n="HIAT:w" s="T597">učugu͡oj</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2081" n="HIAT:w" s="T598">bert</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2084" n="HIAT:w" s="T599">olokko</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2087" n="HIAT:w" s="T600">tiri͡erde</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2090" n="HIAT:w" s="T601">anɨ</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2093" n="HIAT:w" s="T602">habi͡eskaj</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2096" n="HIAT:w" s="T603">bɨlaːs</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2099" n="HIAT:w" s="T604">bu͡olan</ts>
                  <nts id="Seg_2100" n="HIAT:ip">.</nts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T618" id="Seg_2103" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_2105" n="HIAT:w" s="T605">Ogolorbor</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2108" n="HIAT:w" s="T606">daːganɨ</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2111" n="HIAT:w" s="T607">min</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2114" n="HIAT:w" s="T608">daːganɨ</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2117" n="HIAT:w" s="T609">baččabar</ts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2120" n="HIAT:w" s="T610">tijdim</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2123" n="HIAT:w" s="T611">anɨ</ts>
                  <nts id="Seg_2124" n="HIAT:ip">,</nts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2127" n="HIAT:w" s="T612">bert</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2130" n="HIAT:w" s="T613">učugu͡ojduk</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2133" n="HIAT:w" s="T614">bačča</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2136" n="HIAT:w" s="T615">bačča</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2139" n="HIAT:w" s="T616">kɨrdʼɨ͡akpar</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2142" n="HIAT:w" s="T617">tijdim</ts>
                  <nts id="Seg_2143" n="HIAT:ip">.</nts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T618" id="Seg_2145" n="sc" s="T0">
               <ts e="T1" id="Seg_2147" n="e" s="T0">Habi͡eskaj </ts>
               <ts e="T2" id="Seg_2149" n="e" s="T1">bɨlaːs </ts>
               <ts e="T3" id="Seg_2151" n="e" s="T2">bu͡olarɨn </ts>
               <ts e="T4" id="Seg_2153" n="e" s="T3">bagas </ts>
               <ts e="T5" id="Seg_2155" n="e" s="T4">össü͡ö </ts>
               <ts e="T6" id="Seg_2157" n="e" s="T5">daːganɨ </ts>
               <ts e="T7" id="Seg_2159" n="e" s="T6">öjdöːböppün. </ts>
               <ts e="T8" id="Seg_2161" n="e" s="T7">Oččogo </ts>
               <ts e="T9" id="Seg_2163" n="e" s="T8">min </ts>
               <ts e="T10" id="Seg_2165" n="e" s="T9">daːganɨ </ts>
               <ts e="T11" id="Seg_2167" n="e" s="T10">dolu͡oj </ts>
               <ts e="T12" id="Seg_2169" n="e" s="T11">ogo </ts>
               <ts e="T13" id="Seg_2171" n="e" s="T12">etim. </ts>
               <ts e="T14" id="Seg_2173" n="e" s="T13">Kannɨk </ts>
               <ts e="T15" id="Seg_2175" n="e" s="T14">da </ts>
               <ts e="T16" id="Seg_2177" n="e" s="T15">vlaːska </ts>
               <ts e="T17" id="Seg_2179" n="e" s="T16">üleleːbet </ts>
               <ts e="T18" id="Seg_2181" n="e" s="T17">ogo </ts>
               <ts e="T19" id="Seg_2183" n="e" s="T18">etim. </ts>
               <ts e="T20" id="Seg_2185" n="e" s="T19">Habi͡eskaj </ts>
               <ts e="T21" id="Seg_2187" n="e" s="T20">bɨlaːs </ts>
               <ts e="T22" id="Seg_2189" n="e" s="T21">bu͡olar </ts>
               <ts e="T23" id="Seg_2191" n="e" s="T22">di͡en </ts>
               <ts e="T25" id="Seg_2193" n="e" s="T23">tak i, </ts>
               <ts e="T26" id="Seg_2195" n="e" s="T25">ɨjaːk </ts>
               <ts e="T27" id="Seg_2197" n="e" s="T26">ergillere </ts>
               <ts e="T29" id="Seg_2199" n="e" s="T27">tak i, </ts>
               <ts e="T30" id="Seg_2201" n="e" s="T29">hin </ts>
               <ts e="T31" id="Seg_2203" n="e" s="T30">daːganɨ </ts>
               <ts e="T32" id="Seg_2205" n="e" s="T31">tu͡olkuluːr </ts>
               <ts e="T33" id="Seg_2207" n="e" s="T32">kihi </ts>
               <ts e="T34" id="Seg_2209" n="e" s="T33">tu͡olkuluːr </ts>
               <ts e="T35" id="Seg_2211" n="e" s="T34">kördük </ts>
               <ts e="T36" id="Seg_2213" n="e" s="T35">ete. </ts>
               <ts e="T37" id="Seg_2215" n="e" s="T36">ɨjaːk </ts>
               <ts e="T38" id="Seg_2217" n="e" s="T37">ergilliːtin </ts>
               <ts e="T39" id="Seg_2219" n="e" s="T38">hagɨna </ts>
               <ts e="T40" id="Seg_2221" n="e" s="T39">maŋnaj </ts>
               <ts e="T41" id="Seg_2223" n="e" s="T40">kihini </ts>
               <ts e="T42" id="Seg_2225" n="e" s="T41">urut </ts>
               <ts e="T43" id="Seg_2227" n="e" s="T42">baːj </ts>
               <ts e="T44" id="Seg_2229" n="e" s="T43">kihi, </ts>
               <ts e="T45" id="Seg_2231" n="e" s="T44">kulaːk </ts>
               <ts e="T46" id="Seg_2233" n="e" s="T45">kihi </ts>
               <ts e="T47" id="Seg_2235" n="e" s="T46">battɨːr, </ts>
               <ts e="T48" id="Seg_2237" n="e" s="T47">kɨlgahɨ </ts>
               <ts e="T49" id="Seg_2239" n="e" s="T48">kɨ͡aragahɨ, </ts>
               <ts e="T50" id="Seg_2241" n="e" s="T49">dʼadagɨnɨ </ts>
               <ts e="T51" id="Seg_2243" n="e" s="T50">di͡en </ts>
               <ts e="T52" id="Seg_2245" n="e" s="T51">aːttɨːr </ts>
               <ts e="T53" id="Seg_2247" n="e" s="T52">etilere. </ts>
               <ts e="T54" id="Seg_2249" n="e" s="T53">Oččogo </ts>
               <ts e="T55" id="Seg_2251" n="e" s="T54">du͡o </ts>
               <ts e="T56" id="Seg_2253" n="e" s="T55">min </ts>
               <ts e="T57" id="Seg_2255" n="e" s="T56">bu </ts>
               <ts e="T58" id="Seg_2257" n="e" s="T57">baːj </ts>
               <ts e="T59" id="Seg_2259" n="e" s="T58">kihilerge </ts>
               <ts e="T60" id="Seg_2261" n="e" s="T59">maččɨkkaːn </ts>
               <ts e="T61" id="Seg_2263" n="e" s="T60">etim </ts>
               <ts e="T62" id="Seg_2265" n="e" s="T61">vsʼo </ts>
               <ts e="T63" id="Seg_2267" n="e" s="T62">takʼi, </ts>
               <ts e="T64" id="Seg_2269" n="e" s="T63">tabahɨt </ts>
               <ts e="T65" id="Seg_2271" n="e" s="T64">etim. </ts>
               <ts e="T66" id="Seg_2273" n="e" s="T65">Ol </ts>
               <ts e="T67" id="Seg_2275" n="e" s="T66">betereː </ts>
               <ts e="T68" id="Seg_2277" n="e" s="T67">öttüger </ts>
               <ts e="T69" id="Seg_2279" n="e" s="T68">onton </ts>
               <ts e="T70" id="Seg_2281" n="e" s="T69">tu͡olkulaːn </ts>
               <ts e="T71" id="Seg_2283" n="e" s="T70">istim, </ts>
               <ts e="T72" id="Seg_2285" n="e" s="T71">bilen </ts>
               <ts e="T73" id="Seg_2287" n="e" s="T72">istim. </ts>
               <ts e="T74" id="Seg_2289" n="e" s="T73">Kanʼešna, </ts>
               <ts e="T75" id="Seg_2291" n="e" s="T74">kirdik, </ts>
               <ts e="T76" id="Seg_2293" n="e" s="T75">ɨjaːk </ts>
               <ts e="T77" id="Seg_2295" n="e" s="T76">ularɨjar </ts>
               <ts e="T78" id="Seg_2297" n="e" s="T77">ebit. </ts>
               <ts e="T79" id="Seg_2299" n="e" s="T78">"Urukku </ts>
               <ts e="T80" id="Seg_2301" n="e" s="T79">sarskaj </ts>
               <ts e="T81" id="Seg_2303" n="e" s="T80">ɨjaːk </ts>
               <ts e="T82" id="Seg_2305" n="e" s="T81">dʼe </ts>
               <ts e="T83" id="Seg_2307" n="e" s="T82">bütte", </ts>
               <ts e="T84" id="Seg_2309" n="e" s="T83">di͡ebittere. </ts>
               <ts e="T85" id="Seg_2311" n="e" s="T84">Onton </ts>
               <ts e="T86" id="Seg_2313" n="e" s="T85">bettek </ts>
               <ts e="T87" id="Seg_2315" n="e" s="T86">uhuguttan </ts>
               <ts e="T88" id="Seg_2317" n="e" s="T87">usku͡olaga </ts>
               <ts e="T89" id="Seg_2319" n="e" s="T88">ü͡öreni͡eger </ts>
               <ts e="T90" id="Seg_2321" n="e" s="T89">di͡en </ts>
               <ts e="T91" id="Seg_2323" n="e" s="T90">tavarʼišʼ </ts>
               <ts e="T92" id="Seg_2325" n="e" s="T91">Lʼenʼin </ts>
               <ts e="T93" id="Seg_2327" n="e" s="T92">ehe </ts>
               <ts e="T94" id="Seg_2329" n="e" s="T93">di͡en </ts>
               <ts e="T95" id="Seg_2331" n="e" s="T94">ete. </ts>
               <ts e="T96" id="Seg_2333" n="e" s="T95">Lʼenʼin </ts>
               <ts e="T97" id="Seg_2335" n="e" s="T96">zakona. </ts>
               <ts e="T98" id="Seg_2337" n="e" s="T97">Dʼe, </ts>
               <ts e="T99" id="Seg_2339" n="e" s="T98">ularɨjan-ularɨjan </ts>
               <ts e="T100" id="Seg_2341" n="e" s="T99">ol </ts>
               <ts e="T101" id="Seg_2343" n="e" s="T100">ularɨjan </ts>
               <ts e="T102" id="Seg_2345" n="e" s="T101">iste </ts>
               <ts e="T103" id="Seg_2347" n="e" s="T102">uhuguttan. </ts>
               <ts e="T104" id="Seg_2349" n="e" s="T103">Kihi </ts>
               <ts e="T105" id="Seg_2351" n="e" s="T104">barɨta </ts>
               <ts e="T106" id="Seg_2353" n="e" s="T105">kɨlgas </ts>
               <ts e="T107" id="Seg_2355" n="e" s="T106">kɨ͡aragas </ts>
               <ts e="T108" id="Seg_2357" n="e" s="T107">ogoto, </ts>
               <ts e="T109" id="Seg_2359" n="e" s="T108">kaja </ts>
               <ts e="T110" id="Seg_2361" n="e" s="T109">da </ts>
               <ts e="T111" id="Seg_2363" n="e" s="T110">jon. </ts>
               <ts e="T112" id="Seg_2365" n="e" s="T111">Kannɨk </ts>
               <ts e="T113" id="Seg_2367" n="e" s="T112">daːganɨ </ts>
               <ts e="T114" id="Seg_2369" n="e" s="T113">kɨlgas </ts>
               <ts e="T115" id="Seg_2371" n="e" s="T114">kɨ͡aragas, </ts>
               <ts e="T116" id="Seg_2373" n="e" s="T115">dʼadaŋɨ </ts>
               <ts e="T117" id="Seg_2375" n="e" s="T116">kihi </ts>
               <ts e="T118" id="Seg_2377" n="e" s="T117">ogoto </ts>
               <ts e="T119" id="Seg_2379" n="e" s="T118">barɨta </ts>
               <ts e="T120" id="Seg_2381" n="e" s="T119">ü͡örekke </ts>
               <ts e="T121" id="Seg_2383" n="e" s="T120">da </ts>
               <ts e="T122" id="Seg_2385" n="e" s="T121">dʼe </ts>
               <ts e="T123" id="Seg_2387" n="e" s="T122">načʼinajdaːta. </ts>
               <ts e="T124" id="Seg_2389" n="e" s="T123">O, </ts>
               <ts e="T125" id="Seg_2391" n="e" s="T124">hol </ts>
               <ts e="T126" id="Seg_2393" n="e" s="T125">ereːri, </ts>
               <ts e="T127" id="Seg_2395" n="e" s="T126">baːjtan </ts>
               <ts e="T128" id="Seg_2397" n="e" s="T127">ahɨː </ts>
               <ts e="T129" id="Seg_2399" n="e" s="T128">ü͡öremmitter, </ts>
               <ts e="T130" id="Seg_2401" n="e" s="T129">kulaːktan </ts>
               <ts e="T131" id="Seg_2403" n="e" s="T130">abɨrana </ts>
               <ts e="T132" id="Seg_2405" n="e" s="T131">ü͡öremmitter, </ts>
               <ts e="T133" id="Seg_2407" n="e" s="T132">sin </ts>
               <ts e="T134" id="Seg_2409" n="e" s="T133">daːganɨ </ts>
               <ts e="T135" id="Seg_2411" n="e" s="T134">usku͡olaga </ts>
               <ts e="T136" id="Seg_2413" n="e" s="T135">bi͡eri͡ekpitin </ts>
               <ts e="T137" id="Seg_2415" n="e" s="T136">di͡ebet </ts>
               <ts e="T138" id="Seg_2417" n="e" s="T137">etilere </ts>
               <ts e="T139" id="Seg_2419" n="e" s="T138">ogonu. </ts>
               <ts e="T140" id="Seg_2421" n="e" s="T139">Onton </ts>
               <ts e="T141" id="Seg_2423" n="e" s="T140">bu͡olbuta </ts>
               <ts e="T142" id="Seg_2425" n="e" s="T141">kolkoz </ts>
               <ts e="T143" id="Seg_2427" n="e" s="T142">bu͡olu͡okka </ts>
               <ts e="T144" id="Seg_2429" n="e" s="T143">di͡en. </ts>
               <ts e="T145" id="Seg_2431" n="e" s="T144">Obšʼij </ts>
               <ts e="T146" id="Seg_2433" n="e" s="T145">xazʼajstvannan </ts>
               <ts e="T147" id="Seg_2435" n="e" s="T146">dʼon </ts>
               <ts e="T148" id="Seg_2437" n="e" s="T147">ologun </ts>
               <ts e="T149" id="Seg_2439" n="e" s="T148">tuttu͡ogun </ts>
               <ts e="T150" id="Seg_2441" n="e" s="T149">di͡enner. </ts>
               <ts e="T151" id="Seg_2443" n="e" s="T150">Hu͡ok, </ts>
               <ts e="T152" id="Seg_2445" n="e" s="T151">onuga </ts>
               <ts e="T153" id="Seg_2447" n="e" s="T152">emi͡e </ts>
               <ts e="T154" id="Seg_2449" n="e" s="T153">bu͡ola </ts>
               <ts e="T155" id="Seg_2451" n="e" s="T154">tarpataktara. </ts>
               <ts e="T156" id="Seg_2453" n="e" s="T155">Kas </ts>
               <ts e="T157" id="Seg_2455" n="e" s="T156">da </ts>
               <ts e="T158" id="Seg_2457" n="e" s="T157">dʼɨlɨnan </ts>
               <ts e="T159" id="Seg_2459" n="e" s="T158">(bu͡ol-), </ts>
               <ts e="T160" id="Seg_2461" n="e" s="T159">kolkoz </ts>
               <ts e="T161" id="Seg_2463" n="e" s="T160">bu͡olbataktara. </ts>
               <ts e="T162" id="Seg_2465" n="e" s="T161">"Kolkoz </ts>
               <ts e="T163" id="Seg_2467" n="e" s="T162">bu͡olammɨt </ts>
               <ts e="T164" id="Seg_2469" n="e" s="T163">bihigi </ts>
               <ts e="T165" id="Seg_2471" n="e" s="T164">hatanɨ͡akpɨt </ts>
               <ts e="T166" id="Seg_2473" n="e" s="T165">hu͡o", </ts>
               <ts e="T167" id="Seg_2475" n="e" s="T166">diːr </ts>
               <ts e="T168" id="Seg_2477" n="e" s="T167">etilere. </ts>
               <ts e="T169" id="Seg_2479" n="e" s="T168">Urut </ts>
               <ts e="T170" id="Seg_2481" n="e" s="T169">sarskaj </ts>
               <ts e="T171" id="Seg_2483" n="e" s="T170">zakoŋŋa </ts>
               <ts e="T172" id="Seg_2485" n="e" s="T171">ü͡öskeːbit </ts>
               <ts e="T173" id="Seg_2487" n="e" s="T172">dʼon, </ts>
               <ts e="T174" id="Seg_2489" n="e" s="T173">ü͡öremmit </ts>
               <ts e="T175" id="Seg_2491" n="e" s="T174">dʼon. </ts>
               <ts e="T176" id="Seg_2493" n="e" s="T175">Onton </ts>
               <ts e="T177" id="Seg_2495" n="e" s="T176">kajdak </ts>
               <ts e="T178" id="Seg_2497" n="e" s="T177">ere </ts>
               <ts e="T180" id="Seg_2499" n="e" s="T178">gɨnan… </ts>
               <ts e="T181" id="Seg_2501" n="e" s="T180">Kepseːn, </ts>
               <ts e="T182" id="Seg_2503" n="e" s="T181">kepseːnner, </ts>
               <ts e="T183" id="Seg_2505" n="e" s="T182">hoku͡on </ts>
               <ts e="T184" id="Seg_2507" n="e" s="T183">kelen-kelen, </ts>
               <ts e="T185" id="Seg_2509" n="e" s="T184">hubu </ts>
               <ts e="T186" id="Seg_2511" n="e" s="T185">dojdularga </ts>
               <ts e="T187" id="Seg_2513" n="e" s="T186">kelen, </ts>
               <ts e="T188" id="Seg_2515" n="e" s="T187">kelen. </ts>
               <ts e="T189" id="Seg_2517" n="e" s="T188">Kolkoz </ts>
               <ts e="T190" id="Seg_2519" n="e" s="T189">bu͡ollubut </ts>
               <ts e="T191" id="Seg_2521" n="e" s="T190">otut </ts>
               <ts e="T192" id="Seg_2523" n="e" s="T191">settisteːgitten, </ts>
               <ts e="T193" id="Seg_2525" n="e" s="T192">otut </ts>
               <ts e="T194" id="Seg_2527" n="e" s="T193">agɨstaːkka. </ts>
               <ts e="T196" id="Seg_2529" n="e" s="T194">Kаnʼec-takɨ </ts>
               <ts e="T198" id="Seg_2531" n="e" s="T196">kirdik. </ts>
               <ts e="T199" id="Seg_2533" n="e" s="T198">Dʼe </ts>
               <ts e="T200" id="Seg_2535" n="e" s="T199">kolkoz </ts>
               <ts e="T201" id="Seg_2537" n="e" s="T200">bu͡ol </ts>
               <ts e="T202" id="Seg_2539" n="e" s="T201">načʼinajdaːn. </ts>
               <ts e="T203" id="Seg_2541" n="e" s="T202">Ustrojnaːn </ts>
               <ts e="T204" id="Seg_2543" n="e" s="T203">(kaːl-) </ts>
               <ts e="T205" id="Seg_2545" n="e" s="T204">uhuguttan. </ts>
               <ts e="T206" id="Seg_2547" n="e" s="T205">Oččogo </ts>
               <ts e="T207" id="Seg_2549" n="e" s="T206">kolkoːhu </ts>
               <ts e="T208" id="Seg_2551" n="e" s="T207">törömmüppüt </ts>
               <ts e="T209" id="Seg_2553" n="e" s="T208">ete </ts>
               <ts e="T210" id="Seg_2555" n="e" s="T209">bihigi. </ts>
               <ts e="T211" id="Seg_2557" n="e" s="T210">Kanna </ts>
               <ts e="T212" id="Seg_2559" n="e" s="T211">orduk </ts>
               <ts e="T213" id="Seg_2561" n="e" s="T212">tabalaːktan </ts>
               <ts e="T214" id="Seg_2563" n="e" s="T213">onuga </ts>
               <ts e="T215" id="Seg_2565" n="e" s="T214">kolkozka </ts>
               <ts e="T216" id="Seg_2567" n="e" s="T215">tuppupput. </ts>
               <ts e="T217" id="Seg_2569" n="e" s="T216">Kanna </ts>
               <ts e="T218" id="Seg_2571" n="e" s="T217">paːstaːkpɨtɨn, </ts>
               <ts e="T219" id="Seg_2573" n="e" s="T218">honon </ts>
               <ts e="T220" id="Seg_2575" n="e" s="T219">bejebit </ts>
               <ts e="T221" id="Seg_2577" n="e" s="T220">paːspɨtɨnan </ts>
               <ts e="T222" id="Seg_2579" n="e" s="T221">bejebit </ts>
               <ts e="T223" id="Seg_2581" n="e" s="T222">kapkaːmmɨtɨnan. </ts>
               <ts e="T224" id="Seg_2583" n="e" s="T223">Uhuguttan </ts>
               <ts e="T225" id="Seg_2585" n="e" s="T224">kolku͡o </ts>
               <ts e="T226" id="Seg_2587" n="e" s="T225">ologun </ts>
               <ts e="T227" id="Seg_2589" n="e" s="T226">tutammɨt, </ts>
               <ts e="T228" id="Seg_2591" n="e" s="T227">anɨ </ts>
               <ts e="T229" id="Seg_2593" n="e" s="T228">bu </ts>
               <ts e="T230" id="Seg_2595" n="e" s="T229">kemnerge </ts>
               <ts e="T231" id="Seg_2597" n="e" s="T230">tijdibit </ts>
               <ts e="T232" id="Seg_2599" n="e" s="T231">dʼe. </ts>
               <ts e="T233" id="Seg_2601" n="e" s="T232">Dʼe </ts>
               <ts e="T234" id="Seg_2603" n="e" s="T233">onton </ts>
               <ts e="T235" id="Seg_2605" n="e" s="T234">kolkoz </ts>
               <ts e="T236" id="Seg_2607" n="e" s="T235">ologo </ts>
               <ts e="T237" id="Seg_2609" n="e" s="T236">üčügej </ts>
               <ts e="T238" id="Seg_2611" n="e" s="T237">duː </ts>
               <ts e="T239" id="Seg_2613" n="e" s="T238">di͡en. </ts>
               <ts e="T240" id="Seg_2615" n="e" s="T239">Ol </ts>
               <ts e="T241" id="Seg_2617" n="e" s="T240">kördük </ts>
               <ts e="T242" id="Seg_2619" n="e" s="T241">bu͡olla </ts>
               <ts e="T243" id="Seg_2621" n="e" s="T242">bu͡olbutunan </ts>
               <ts e="T244" id="Seg_2623" n="e" s="T243">dʼe </ts>
               <ts e="T245" id="Seg_2625" n="e" s="T244">kolku͡os </ts>
               <ts e="T246" id="Seg_2627" n="e" s="T245">bu͡olbupput. </ts>
               <ts e="T247" id="Seg_2629" n="e" s="T246">Ol </ts>
               <ts e="T248" id="Seg_2631" n="e" s="T247">betereː </ts>
               <ts e="T249" id="Seg_2633" n="e" s="T248">öttüger </ts>
               <ts e="T250" id="Seg_2635" n="e" s="T249">emi͡e </ts>
               <ts e="T251" id="Seg_2637" n="e" s="T250">daːganɨ </ts>
               <ts e="T252" id="Seg_2639" n="e" s="T251">erejdeːk </ts>
               <ts e="T253" id="Seg_2641" n="e" s="T252">kɨhalgalaːk </ts>
               <ts e="T254" id="Seg_2643" n="e" s="T253">üjeler </ts>
               <ts e="T255" id="Seg_2645" n="e" s="T254">bu͡olar </ts>
               <ts e="T256" id="Seg_2647" n="e" s="T255">etilere. </ts>
               <ts e="T257" id="Seg_2649" n="e" s="T256">Baː, </ts>
               <ts e="T258" id="Seg_2651" n="e" s="T257">kulaːkka, </ts>
               <ts e="T259" id="Seg_2653" n="e" s="T258">urukku </ts>
               <ts e="T260" id="Seg_2655" n="e" s="T259">sarskajga </ts>
               <ts e="T261" id="Seg_2657" n="e" s="T260">bastarɨn </ts>
               <ts e="T262" id="Seg_2659" n="e" s="T261">ɨstarbɨt </ts>
               <ts e="T263" id="Seg_2661" n="e" s="T262">kihiler </ts>
               <ts e="T264" id="Seg_2663" n="e" s="T263">tam </ts>
               <ts e="T265" id="Seg_2665" n="e" s="T264">baldiːttɨːr </ts>
               <ts e="T266" id="Seg_2667" n="e" s="T265">da </ts>
               <ts e="T267" id="Seg_2669" n="e" s="T266">etilere. </ts>
               <ts e="T268" id="Seg_2671" n="e" s="T267">Olor </ts>
               <ts e="T269" id="Seg_2673" n="e" s="T268">sin </ts>
               <ts e="T270" id="Seg_2675" n="e" s="T269">ereji </ts>
               <ts e="T271" id="Seg_2677" n="e" s="T270">oŋoroːčču </ts>
               <ts e="T272" id="Seg_2679" n="e" s="T271">etiler. </ts>
               <ts e="T273" id="Seg_2681" n="e" s="T272">Ol </ts>
               <ts e="T274" id="Seg_2683" n="e" s="T273">baldʼiːtɨ, </ts>
               <ts e="T275" id="Seg_2685" n="e" s="T274">hu͡ok, </ts>
               <ts e="T276" id="Seg_2687" n="e" s="T275">ol </ts>
               <ts e="T277" id="Seg_2689" n="e" s="T276">aːtɨ </ts>
               <ts e="T278" id="Seg_2691" n="e" s="T277">savʼeskaj </ts>
               <ts e="T279" id="Seg_2693" n="e" s="T278">bɨlaːs </ts>
               <ts e="T280" id="Seg_2695" n="e" s="T279">inni </ts>
               <ts e="T281" id="Seg_2697" n="e" s="T280">di͡ek </ts>
               <ts e="T282" id="Seg_2699" n="e" s="T281">kaːman </ts>
               <ts e="T283" id="Seg_2701" n="e" s="T282">ihen, </ts>
               <ts e="T284" id="Seg_2703" n="e" s="T283">ol </ts>
               <ts e="T285" id="Seg_2705" n="e" s="T284">baldʼiːtɨ </ts>
               <ts e="T286" id="Seg_2707" n="e" s="T285">hin </ts>
               <ts e="T287" id="Seg_2709" n="e" s="T286">daːganɨ </ts>
               <ts e="T288" id="Seg_2711" n="e" s="T287">ɨraːstaːččɨ </ts>
               <ts e="T289" id="Seg_2713" n="e" s="T288">ete </ts>
               <ts e="T290" id="Seg_2715" n="e" s="T289">dʼahajaːččɨ </ts>
               <ts e="T291" id="Seg_2717" n="e" s="T290">da </ts>
               <ts e="T292" id="Seg_2719" n="e" s="T291">ete. </ts>
               <ts e="T293" id="Seg_2721" n="e" s="T292">Tʼe </ts>
               <ts e="T294" id="Seg_2723" n="e" s="T293">ol </ts>
               <ts e="T295" id="Seg_2725" n="e" s="T294">büppütün </ts>
               <ts e="T296" id="Seg_2727" n="e" s="T295">kenne, </ts>
               <ts e="T297" id="Seg_2729" n="e" s="T296">uhuguttan </ts>
               <ts e="T298" id="Seg_2731" n="e" s="T297">bu </ts>
               <ts e="T299" id="Seg_2733" n="e" s="T298">Gʼermanʼija </ts>
               <ts e="T300" id="Seg_2735" n="e" s="T299">heriːte </ts>
               <ts e="T301" id="Seg_2737" n="e" s="T300">bu͡olbuta. </ts>
               <ts e="T302" id="Seg_2739" n="e" s="T301">Dʼe </ts>
               <ts e="T303" id="Seg_2741" n="e" s="T302">itinige </ts>
               <ts e="T304" id="Seg_2743" n="e" s="T303">kɨhalla </ts>
               <ts e="T305" id="Seg_2745" n="e" s="T304">hɨspɨppɨt </ts>
               <ts e="T306" id="Seg_2747" n="e" s="T305">bihigi </ts>
               <ts e="T307" id="Seg_2749" n="e" s="T306">hürdeːk. </ts>
               <ts e="T308" id="Seg_2751" n="e" s="T307">Ol </ts>
               <ts e="T309" id="Seg_2753" n="e" s="T308">taŋas </ts>
               <ts e="T310" id="Seg_2755" n="e" s="T309">daː </ts>
               <ts e="T311" id="Seg_2757" n="e" s="T310">terijiːte </ts>
               <ts e="T312" id="Seg_2759" n="e" s="T311">bu͡olar </ts>
               <ts e="T313" id="Seg_2761" n="e" s="T312">ete, </ts>
               <ts e="T314" id="Seg_2763" n="e" s="T313">ol </ts>
               <ts e="T315" id="Seg_2765" n="e" s="T314">karčɨnnan </ts>
               <ts e="T316" id="Seg_2767" n="e" s="T315">daː </ts>
               <ts e="T317" id="Seg_2769" n="e" s="T316">nalog </ts>
               <ts e="T318" id="Seg_2771" n="e" s="T317">bu͡olar </ts>
               <ts e="T319" id="Seg_2773" n="e" s="T318">ete. </ts>
               <ts e="T320" id="Seg_2775" n="e" s="T319">Bihigi </ts>
               <ts e="T321" id="Seg_2777" n="e" s="T320">ol </ts>
               <ts e="T322" id="Seg_2779" n="e" s="T321">di͡ek, </ts>
               <ts e="T323" id="Seg_2781" n="e" s="T322">gu͡orat </ts>
               <ts e="T324" id="Seg_2783" n="e" s="T323">hirderge </ts>
               <ts e="T325" id="Seg_2785" n="e" s="T324">armijaga </ts>
               <ts e="T327" id="Seg_2787" n="e" s="T325">kömölöhömmüt… </ts>
               <ts e="T328" id="Seg_2789" n="e" s="T327">Onton </ts>
               <ts e="T329" id="Seg_2791" n="e" s="T328">hɨl </ts>
               <ts e="T330" id="Seg_2793" n="e" s="T329">büten </ts>
               <ts e="T331" id="Seg_2795" n="e" s="T330">dʼe </ts>
               <ts e="T332" id="Seg_2797" n="e" s="T331">anɨ </ts>
               <ts e="T333" id="Seg_2799" n="e" s="T332">tu͡ok </ts>
               <ts e="T334" id="Seg_2801" n="e" s="T333">da </ts>
               <ts e="T335" id="Seg_2803" n="e" s="T334">bu͡olu͡okput </ts>
               <ts e="T336" id="Seg_2805" n="e" s="T335">di͡ebeppin, </ts>
               <ts e="T337" id="Seg_2807" n="e" s="T336">bügüŋŋü </ts>
               <ts e="T338" id="Seg_2809" n="e" s="T337">hanaːm </ts>
               <ts e="T339" id="Seg_2811" n="e" s="T338">üčügej. </ts>
               <ts e="T340" id="Seg_2813" n="e" s="T339">Anɨ </ts>
               <ts e="T341" id="Seg_2815" n="e" s="T340">olok </ts>
               <ts e="T342" id="Seg_2817" n="e" s="T341">könnö, </ts>
               <ts e="T343" id="Seg_2819" n="e" s="T342">olok </ts>
               <ts e="T344" id="Seg_2821" n="e" s="T343">ulaːtta. </ts>
               <ts e="T345" id="Seg_2823" n="e" s="T344">Anɨ </ts>
               <ts e="T346" id="Seg_2825" n="e" s="T345">bütün </ts>
               <ts e="T347" id="Seg_2827" n="e" s="T346">kolku͡ostar </ts>
               <ts e="T348" id="Seg_2829" n="e" s="T347">uhuguttan </ts>
               <ts e="T349" id="Seg_2831" n="e" s="T348">argijaːsija </ts>
               <ts e="T350" id="Seg_2833" n="e" s="T349">kördük </ts>
               <ts e="T351" id="Seg_2835" n="e" s="T350">bu͡ollular. </ts>
               <ts e="T352" id="Seg_2837" n="e" s="T351">Anɨ </ts>
               <ts e="T353" id="Seg_2839" n="e" s="T352">mantan </ts>
               <ts e="T354" id="Seg_2841" n="e" s="T353">inni </ts>
               <ts e="T355" id="Seg_2843" n="e" s="T354">di͡egitin </ts>
               <ts e="T356" id="Seg_2845" n="e" s="T355">bilbeppin, </ts>
               <ts e="T357" id="Seg_2847" n="e" s="T356">bejem </ts>
               <ts e="T358" id="Seg_2849" n="e" s="T357">kɨrdʼan </ts>
               <ts e="T359" id="Seg_2851" n="e" s="T358">kaːllɨm. </ts>
               <ts e="T619" id="Seg_2853" n="e" s="T359">Tak </ts>
               <ts e="T360" id="Seg_2855" n="e" s="T619">i </ts>
               <ts e="T361" id="Seg_2857" n="e" s="T360">tak, </ts>
               <ts e="T362" id="Seg_2859" n="e" s="T361">min </ts>
               <ts e="T363" id="Seg_2861" n="e" s="T362">hanaːbar, </ts>
               <ts e="T364" id="Seg_2863" n="e" s="T363">üčügej </ts>
               <ts e="T365" id="Seg_2865" n="e" s="T364">olokko </ts>
               <ts e="T366" id="Seg_2867" n="e" s="T365">anɨ </ts>
               <ts e="T367" id="Seg_2869" n="e" s="T366">tijdibit, </ts>
               <ts e="T368" id="Seg_2871" n="e" s="T367">diːbin, </ts>
               <ts e="T369" id="Seg_2873" n="e" s="T368">hubu </ts>
               <ts e="T370" id="Seg_2875" n="e" s="T369">kɨrdʼar </ts>
               <ts e="T372" id="Seg_2877" n="e" s="T370">haːspar. </ts>
               <ts e="T373" id="Seg_2879" n="e" s="T372">Anɨ </ts>
               <ts e="T374" id="Seg_2881" n="e" s="T373">tot </ts>
               <ts e="T375" id="Seg_2883" n="e" s="T374">bagajbɨt, </ts>
               <ts e="T376" id="Seg_2885" n="e" s="T375">ičiges </ts>
               <ts e="T377" id="Seg_2887" n="e" s="T376">bagajbɨt. </ts>
               <ts e="T378" id="Seg_2889" n="e" s="T377">Taŋahɨ </ts>
               <ts e="T379" id="Seg_2891" n="e" s="T378">daː </ts>
               <ts e="T380" id="Seg_2893" n="e" s="T379">taŋnabɨt, </ts>
               <ts e="T381" id="Seg_2895" n="e" s="T380">ahɨ </ts>
               <ts e="T382" id="Seg_2897" n="e" s="T381">daː </ts>
               <ts e="T383" id="Seg_2899" n="e" s="T382">ahɨːbɨt. </ts>
               <ts e="T384" id="Seg_2901" n="e" s="T383">Bu </ts>
               <ts e="T385" id="Seg_2903" n="e" s="T384">ka </ts>
               <ts e="T386" id="Seg_2905" n="e" s="T385">maŋnaj </ts>
               <ts e="T387" id="Seg_2907" n="e" s="T386">mini͡ene </ts>
               <ts e="T388" id="Seg_2909" n="e" s="T387">bu </ts>
               <ts e="T389" id="Seg_2911" n="e" s="T388">ulakan </ts>
               <ts e="T390" id="Seg_2913" n="e" s="T389">ogom, </ts>
               <ts e="T391" id="Seg_2915" n="e" s="T390">bu </ts>
               <ts e="T392" id="Seg_2917" n="e" s="T391">Bahiːlaj </ts>
               <ts e="T393" id="Seg_2919" n="e" s="T392">baːr. </ts>
               <ts e="T394" id="Seg_2921" n="e" s="T393">Bu </ts>
               <ts e="T395" id="Seg_2923" n="e" s="T394">ogom </ts>
               <ts e="T396" id="Seg_2925" n="e" s="T395">ulaːtta </ts>
               <ts e="T397" id="Seg_2927" n="e" s="T396">da </ts>
               <ts e="T398" id="Seg_2929" n="e" s="T397">ü͡öremmite </ts>
               <ts e="T399" id="Seg_2931" n="e" s="T398">giniː, </ts>
               <ts e="T400" id="Seg_2933" n="e" s="T399">usku͡olatɨn </ts>
               <ts e="T401" id="Seg_2935" n="e" s="T400">büterde </ts>
               <ts e="T402" id="Seg_2937" n="e" s="T401">daːganɨ </ts>
               <ts e="T403" id="Seg_2939" n="e" s="T402">učiːtalga </ts>
               <ts e="T404" id="Seg_2941" n="e" s="T403">ü͡öremmite. </ts>
               <ts e="T405" id="Seg_2943" n="e" s="T404">Onon </ts>
               <ts e="T406" id="Seg_2945" n="e" s="T405">anɨ </ts>
               <ts e="T407" id="Seg_2947" n="e" s="T406">kün </ts>
               <ts e="T408" id="Seg_2949" n="e" s="T407">bügün </ts>
               <ts e="T409" id="Seg_2951" n="e" s="T408">učitallɨː </ts>
               <ts e="T410" id="Seg_2953" n="e" s="T409">oloror. </ts>
               <ts e="T411" id="Seg_2955" n="e" s="T410">Onton </ts>
               <ts e="T412" id="Seg_2957" n="e" s="T411">biːr </ts>
               <ts e="T413" id="Seg_2959" n="e" s="T412">kɨːstaːkpɨn. </ts>
               <ts e="T414" id="Seg_2961" n="e" s="T413">Ol </ts>
               <ts e="T415" id="Seg_2963" n="e" s="T414">emi͡e </ts>
               <ts e="T416" id="Seg_2965" n="e" s="T415">ü͡öregin, </ts>
               <ts e="T417" id="Seg_2967" n="e" s="T416">usku͡olatɨn </ts>
               <ts e="T418" id="Seg_2969" n="e" s="T417">büteren, </ts>
               <ts e="T419" id="Seg_2971" n="e" s="T418">emi͡e </ts>
               <ts e="T420" id="Seg_2973" n="e" s="T419">bɨssaj </ts>
               <ts e="T421" id="Seg_2975" n="e" s="T420">ü͡örenen, </ts>
               <ts e="T422" id="Seg_2977" n="e" s="T421">du͡okturdaːn </ts>
               <ts e="T423" id="Seg_2979" n="e" s="T422">oloror </ts>
               <ts e="T424" id="Seg_2981" n="e" s="T423">anɨ. </ts>
               <ts e="T425" id="Seg_2983" n="e" s="T424">Ol </ts>
               <ts e="T426" id="Seg_2985" n="e" s="T425">bejetin </ts>
               <ts e="T427" id="Seg_2987" n="e" s="T426">inniger </ts>
               <ts e="T428" id="Seg_2989" n="e" s="T427">üčügej, </ts>
               <ts e="T429" id="Seg_2991" n="e" s="T428">ol </ts>
               <ts e="T430" id="Seg_2993" n="e" s="T429">kenne </ts>
               <ts e="T431" id="Seg_2995" n="e" s="T430">min </ts>
               <ts e="T432" id="Seg_2997" n="e" s="T431">daː </ts>
               <ts e="T433" id="Seg_2999" n="e" s="T432">hanaːbar </ts>
               <ts e="T434" id="Seg_3001" n="e" s="T433">üčügej </ts>
               <ts e="T435" id="Seg_3003" n="e" s="T434">anɨ – </ts>
               <ts e="T436" id="Seg_3005" n="e" s="T435">bu </ts>
               <ts e="T437" id="Seg_3007" n="e" s="T436">olokko </ts>
               <ts e="T438" id="Seg_3009" n="e" s="T437">oloruːlara, </ts>
               <ts e="T439" id="Seg_3011" n="e" s="T438">anɨ </ts>
               <ts e="T440" id="Seg_3013" n="e" s="T439">bu </ts>
               <ts e="T441" id="Seg_3015" n="e" s="T440">ogolorum. </ts>
               <ts e="T442" id="Seg_3017" n="e" s="T441">Onuga </ts>
               <ts e="T443" id="Seg_3019" n="e" s="T442">ühüs </ts>
               <ts e="T444" id="Seg_3021" n="e" s="T443">emi͡e </ts>
               <ts e="T445" id="Seg_3023" n="e" s="T444">u͡olum </ts>
               <ts e="T446" id="Seg_3025" n="e" s="T445">avijporka </ts>
               <ts e="T447" id="Seg_3027" n="e" s="T446">üleliːr. </ts>
               <ts e="T448" id="Seg_3029" n="e" s="T447">Onton </ts>
               <ts e="T449" id="Seg_3031" n="e" s="T448">muŋ </ts>
               <ts e="T450" id="Seg_3033" n="e" s="T449">kuččuguj </ts>
               <ts e="T451" id="Seg_3035" n="e" s="T450">u͡ollaːppɨn </ts>
               <ts e="T452" id="Seg_3037" n="e" s="T451">anɨ, </ts>
               <ts e="T453" id="Seg_3039" n="e" s="T452">Ujbaːn, </ts>
               <ts e="T454" id="Seg_3041" n="e" s="T453">Antonаv </ts>
               <ts e="T455" id="Seg_3043" n="e" s="T454">Ujbaːn </ts>
               <ts e="T457" id="Seg_3045" n="e" s="T455">(Mʼixʼeis-). </ts>
               <ts e="T458" id="Seg_3047" n="e" s="T457">Ol </ts>
               <ts e="T459" id="Seg_3049" n="e" s="T458">du͡o </ts>
               <ts e="T460" id="Seg_3051" n="e" s="T459">anɨ </ts>
               <ts e="T461" id="Seg_3053" n="e" s="T460">kolku͡oska </ts>
               <ts e="T462" id="Seg_3055" n="e" s="T461">üleliːr. </ts>
               <ts e="T463" id="Seg_3057" n="e" s="T462">Kolku͡os </ts>
               <ts e="T464" id="Seg_3059" n="e" s="T463">daːganɨ </ts>
               <ts e="T465" id="Seg_3061" n="e" s="T464">üleliːre, </ts>
               <ts e="T466" id="Seg_3063" n="e" s="T465">ülete </ts>
               <ts e="T467" id="Seg_3065" n="e" s="T466">ületin </ts>
               <ts e="T468" id="Seg_3067" n="e" s="T467">daː </ts>
               <ts e="T469" id="Seg_3069" n="e" s="T468">üleleːk </ts>
               <ts e="T470" id="Seg_3071" n="e" s="T469">kolku͡os, </ts>
               <ts e="T471" id="Seg_3073" n="e" s="T470">hin </ts>
               <ts e="T472" id="Seg_3075" n="e" s="T471">daːganɨ </ts>
               <ts e="T473" id="Seg_3077" n="e" s="T472">bejetin </ts>
               <ts e="T474" id="Seg_3079" n="e" s="T473">kolku͡os </ts>
               <ts e="T475" id="Seg_3081" n="e" s="T474">ihiger </ts>
               <ts e="T476" id="Seg_3083" n="e" s="T475">hin. </ts>
               <ts e="T477" id="Seg_3085" n="e" s="T476">Üčügejdik </ts>
               <ts e="T478" id="Seg_3087" n="e" s="T477">totoron, </ts>
               <ts e="T479" id="Seg_3089" n="e" s="T478">taŋɨnnaran </ts>
               <ts e="T480" id="Seg_3091" n="e" s="T479">hin </ts>
               <ts e="T481" id="Seg_3093" n="e" s="T480">da </ts>
               <ts e="T482" id="Seg_3095" n="e" s="T481">üleleter. </ts>
               <ts e="T483" id="Seg_3097" n="e" s="T482">Hin </ts>
               <ts e="T484" id="Seg_3099" n="e" s="T483">daː </ts>
               <ts e="T485" id="Seg_3101" n="e" s="T484">harpalaːttaːk. </ts>
               <ts e="T486" id="Seg_3103" n="e" s="T485">Kihi </ts>
               <ts e="T487" id="Seg_3105" n="e" s="T486">kanna </ts>
               <ts e="T488" id="Seg_3107" n="e" s="T487">učugu͡oduk </ts>
               <ts e="T489" id="Seg_3109" n="e" s="T488">üleliːre </ts>
               <ts e="T490" id="Seg_3111" n="e" s="T489">ol </ts>
               <ts e="T491" id="Seg_3113" n="e" s="T490">orduk </ts>
               <ts e="T492" id="Seg_3115" n="e" s="T491">bu͡olar, </ts>
               <ts e="T493" id="Seg_3117" n="e" s="T492">kanna </ts>
               <ts e="T494" id="Seg_3119" n="e" s="T493">kuhagannɨk </ts>
               <ts e="T495" id="Seg_3121" n="e" s="T494">üleliːre </ts>
               <ts e="T496" id="Seg_3123" n="e" s="T495">ol </ts>
               <ts e="T497" id="Seg_3125" n="e" s="T496">niːze </ts>
               <ts e="T498" id="Seg_3127" n="e" s="T497">bu͡olar. </ts>
               <ts e="T499" id="Seg_3129" n="e" s="T498">Min </ts>
               <ts e="T500" id="Seg_3131" n="e" s="T499">hanaːbar </ts>
               <ts e="T501" id="Seg_3133" n="e" s="T500">hitigirdik. </ts>
               <ts e="T502" id="Seg_3135" n="e" s="T501">Bu </ts>
               <ts e="T503" id="Seg_3137" n="e" s="T502">ke </ts>
               <ts e="T504" id="Seg_3139" n="e" s="T503">maŋnaj </ts>
               <ts e="T505" id="Seg_3141" n="e" s="T504">bejem </ts>
               <ts e="T506" id="Seg_3143" n="e" s="T505">hin </ts>
               <ts e="T507" id="Seg_3145" n="e" s="T506">daːganɨ </ts>
               <ts e="T508" id="Seg_3147" n="e" s="T507">bu </ts>
               <ts e="T509" id="Seg_3149" n="e" s="T508">sʼelʼsavʼetɨnnan, </ts>
               <ts e="T510" id="Seg_3151" n="e" s="T509">tugunan </ts>
               <ts e="T511" id="Seg_3153" n="e" s="T510">rukavostvavalaːn </ts>
               <ts e="T512" id="Seg_3155" n="e" s="T511">oloror </ts>
               <ts e="T513" id="Seg_3157" n="e" s="T512">etim. </ts>
               <ts e="T514" id="Seg_3159" n="e" s="T513">Onton, </ts>
               <ts e="T515" id="Seg_3161" n="e" s="T514">anɨ </ts>
               <ts e="T516" id="Seg_3163" n="e" s="T515">ogo </ts>
               <ts e="T517" id="Seg_3165" n="e" s="T516">kihiler </ts>
               <ts e="T518" id="Seg_3167" n="e" s="T517">ü͡örekke </ts>
               <ts e="T519" id="Seg_3169" n="e" s="T518">ü͡örenenner, </ts>
               <ts e="T520" id="Seg_3171" n="e" s="T519">miniginneːger </ts>
               <ts e="T521" id="Seg_3173" n="e" s="T520">luːčče </ts>
               <ts e="T522" id="Seg_3175" n="e" s="T521">üleliːhiler. </ts>
               <ts e="T523" id="Seg_3177" n="e" s="T522">Oččogo </ts>
               <ts e="T524" id="Seg_3179" n="e" s="T523">du͡o, </ts>
               <ts e="T525" id="Seg_3181" n="e" s="T524">kimi͡eke </ts>
               <ts e="T526" id="Seg_3183" n="e" s="T525">daːganɨ </ts>
               <ts e="T527" id="Seg_3185" n="e" s="T526">buruj </ts>
               <ts e="T528" id="Seg_3187" n="e" s="T527">haŋata </ts>
               <ts e="T529" id="Seg_3189" n="e" s="T528">hu͡ok, </ts>
               <ts e="T530" id="Seg_3191" n="e" s="T529">"anɨ </ts>
               <ts e="T531" id="Seg_3193" n="e" s="T530">hɨnnʼanɨ͡am", </ts>
               <ts e="T532" id="Seg_3195" n="e" s="T531">di͡emmin, </ts>
               <ts e="T533" id="Seg_3197" n="e" s="T532">selsavʼettaːn </ts>
               <ts e="T534" id="Seg_3199" n="e" s="T533">büppütüm </ts>
               <ts e="T535" id="Seg_3201" n="e" s="T534">ete. </ts>
               <ts e="T536" id="Seg_3203" n="e" s="T535">Anɨ </ts>
               <ts e="T537" id="Seg_3205" n="e" s="T536">bu </ts>
               <ts e="T538" id="Seg_3207" n="e" s="T537">ogolorum </ts>
               <ts e="T539" id="Seg_3209" n="e" s="T538">ü͡örekke </ts>
               <ts e="T540" id="Seg_3211" n="e" s="T539">ü͡örenenner </ts>
               <ts e="T541" id="Seg_3213" n="e" s="T540">üčügejdik </ts>
               <ts e="T543" id="Seg_3215" n="e" s="T541">(olo-) </ts>
               <ts e="T544" id="Seg_3217" n="e" s="T543">olorollor </ts>
               <ts e="T545" id="Seg_3219" n="e" s="T544">onton. </ts>
               <ts e="T546" id="Seg_3221" n="e" s="T545">Anɨ </ts>
               <ts e="T547" id="Seg_3223" n="e" s="T546">ü͡örene </ts>
               <ts e="T548" id="Seg_3225" n="e" s="T547">olorobun </ts>
               <ts e="T549" id="Seg_3227" n="e" s="T548">bu </ts>
               <ts e="T550" id="Seg_3229" n="e" s="T549">kɨrdʼan </ts>
               <ts e="T551" id="Seg_3231" n="e" s="T550">baraːmmɨn. </ts>
               <ts e="T552" id="Seg_3233" n="e" s="T551">Bejem </ts>
               <ts e="T553" id="Seg_3235" n="e" s="T552">ü͡örege </ts>
               <ts e="T554" id="Seg_3237" n="e" s="T553">hu͡ok </ts>
               <ts e="T555" id="Seg_3239" n="e" s="T554">bagaj </ts>
               <ts e="T556" id="Seg_3241" n="e" s="T555">etim, </ts>
               <ts e="T557" id="Seg_3243" n="e" s="T556">ol </ts>
               <ts e="T558" id="Seg_3245" n="e" s="T557">kenne </ts>
               <ts e="T559" id="Seg_3247" n="e" s="T558">daže </ts>
               <ts e="T560" id="Seg_3249" n="e" s="T559">min </ts>
               <ts e="T561" id="Seg_3251" n="e" s="T560">tɨlɨ </ts>
               <ts e="T562" id="Seg_3253" n="e" s="T561">da </ts>
               <ts e="T563" id="Seg_3255" n="e" s="T562">kɨ͡ajan </ts>
               <ts e="T564" id="Seg_3257" n="e" s="T563">istibeppin – </ts>
               <ts e="T565" id="Seg_3259" n="e" s="T564">etim. </ts>
               <ts e="T566" id="Seg_3261" n="e" s="T565">Kaččaga </ts>
               <ts e="T567" id="Seg_3263" n="e" s="T566">ol </ts>
               <ts e="T568" id="Seg_3265" n="e" s="T567">min </ts>
               <ts e="T569" id="Seg_3267" n="e" s="T568">sʼelʼsavʼet </ts>
               <ts e="T570" id="Seg_3269" n="e" s="T569">erdekpinen </ts>
               <ts e="T571" id="Seg_3271" n="e" s="T570">oː </ts>
               <ts e="T572" id="Seg_3273" n="e" s="T571">tu͡ok </ts>
               <ts e="T573" id="Seg_3275" n="e" s="T572">eme </ts>
               <ts e="T574" id="Seg_3277" n="e" s="T573">pʼerʼevoːččigunan. </ts>
               <ts e="T575" id="Seg_3279" n="e" s="T574">Biːr </ts>
               <ts e="T576" id="Seg_3281" n="e" s="T575">eme </ts>
               <ts e="T577" id="Seg_3283" n="e" s="T576">tɨlkaːnɨ </ts>
               <ts e="T578" id="Seg_3285" n="e" s="T577">kohu͡on </ts>
               <ts e="T579" id="Seg_3287" n="e" s="T578">kihi </ts>
               <ts e="T580" id="Seg_3289" n="e" s="T579">haŋatɨnan </ts>
               <ts e="T581" id="Seg_3291" n="e" s="T580">ergeteːčči </ts>
               <ts e="T582" id="Seg_3293" n="e" s="T581">etibit, </ts>
               <ts e="T583" id="Seg_3295" n="e" s="T582">pʼerʼevoːččigunan. </ts>
               <ts e="T584" id="Seg_3297" n="e" s="T583">Ol </ts>
               <ts e="T585" id="Seg_3299" n="e" s="T584">erejdeːk </ts>
               <ts e="T586" id="Seg_3301" n="e" s="T585">ete. </ts>
               <ts e="T587" id="Seg_3303" n="e" s="T586">Anɨ </ts>
               <ts e="T588" id="Seg_3305" n="e" s="T587">ereje </ts>
               <ts e="T589" id="Seg_3307" n="e" s="T588">hu͡ok </ts>
               <ts e="T590" id="Seg_3309" n="e" s="T589">bejelere </ts>
               <ts e="T591" id="Seg_3311" n="e" s="T590">sɨraj </ts>
               <ts e="T592" id="Seg_3313" n="e" s="T591">kepseteller </ts>
               <ts e="T593" id="Seg_3315" n="e" s="T592">kimi </ts>
               <ts e="T594" id="Seg_3317" n="e" s="T593">da </ts>
               <ts e="T595" id="Seg_3319" n="e" s="T594">gɨtta. </ts>
               <ts e="T596" id="Seg_3321" n="e" s="T595">Savʼeskaj </ts>
               <ts e="T597" id="Seg_3323" n="e" s="T596">bɨlaːs </ts>
               <ts e="T598" id="Seg_3325" n="e" s="T597">učugu͡oj </ts>
               <ts e="T599" id="Seg_3327" n="e" s="T598">bert </ts>
               <ts e="T600" id="Seg_3329" n="e" s="T599">olokko </ts>
               <ts e="T601" id="Seg_3331" n="e" s="T600">tiri͡erde </ts>
               <ts e="T602" id="Seg_3333" n="e" s="T601">anɨ </ts>
               <ts e="T603" id="Seg_3335" n="e" s="T602">habi͡eskaj </ts>
               <ts e="T604" id="Seg_3337" n="e" s="T603">bɨlaːs </ts>
               <ts e="T605" id="Seg_3339" n="e" s="T604">bu͡olan. </ts>
               <ts e="T606" id="Seg_3341" n="e" s="T605">Ogolorbor </ts>
               <ts e="T607" id="Seg_3343" n="e" s="T606">daːganɨ </ts>
               <ts e="T608" id="Seg_3345" n="e" s="T607">min </ts>
               <ts e="T609" id="Seg_3347" n="e" s="T608">daːganɨ </ts>
               <ts e="T610" id="Seg_3349" n="e" s="T609">baččabar </ts>
               <ts e="T611" id="Seg_3351" n="e" s="T610">tijdim </ts>
               <ts e="T612" id="Seg_3353" n="e" s="T611">anɨ, </ts>
               <ts e="T613" id="Seg_3355" n="e" s="T612">bert </ts>
               <ts e="T614" id="Seg_3357" n="e" s="T613">učugu͡ojduk </ts>
               <ts e="T615" id="Seg_3359" n="e" s="T614">bačča </ts>
               <ts e="T616" id="Seg_3361" n="e" s="T615">bačča </ts>
               <ts e="T617" id="Seg_3363" n="e" s="T616">kɨrdʼɨ͡akpar </ts>
               <ts e="T618" id="Seg_3365" n="e" s="T617">tijdim. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_3366" s="T0">AnMS_1972_GoodSovietTimes_nar.001 (001.001)</ta>
            <ta e="T13" id="Seg_3367" s="T7">AnMS_1972_GoodSovietTimes_nar.002 (001.002)</ta>
            <ta e="T19" id="Seg_3368" s="T13">AnMS_1972_GoodSovietTimes_nar.003 (001.003)</ta>
            <ta e="T36" id="Seg_3369" s="T19">AnMS_1972_GoodSovietTimes_nar.004 (001.004)</ta>
            <ta e="T53" id="Seg_3370" s="T36">AnMS_1972_GoodSovietTimes_nar.005 (001.005)</ta>
            <ta e="T65" id="Seg_3371" s="T53">AnMS_1972_GoodSovietTimes_nar.006 (001.006)</ta>
            <ta e="T73" id="Seg_3372" s="T65">AnMS_1972_GoodSovietTimes_nar.007 (001.007)</ta>
            <ta e="T78" id="Seg_3373" s="T73">AnMS_1972_GoodSovietTimes_nar.008 (001.008)</ta>
            <ta e="T84" id="Seg_3374" s="T78">AnMS_1972_GoodSovietTimes_nar.009 (001.009)</ta>
            <ta e="T95" id="Seg_3375" s="T84">AnMS_1972_GoodSovietTimes_nar.010 (001.010)</ta>
            <ta e="T97" id="Seg_3376" s="T95">AnMS_1972_GoodSovietTimes_nar.011 (001.011)</ta>
            <ta e="T103" id="Seg_3377" s="T97">AnMS_1972_GoodSovietTimes_nar.012 (001.012)</ta>
            <ta e="T111" id="Seg_3378" s="T103">AnMS_1972_GoodSovietTimes_nar.013 (001.013)</ta>
            <ta e="T123" id="Seg_3379" s="T111">AnMS_1972_GoodSovietTimes_nar.014 (001.014)</ta>
            <ta e="T139" id="Seg_3380" s="T123">AnMS_1972_GoodSovietTimes_nar.015 (001.015)</ta>
            <ta e="T144" id="Seg_3381" s="T139">AnMS_1972_GoodSovietTimes_nar.016 (001.016)</ta>
            <ta e="T150" id="Seg_3382" s="T144">AnMS_1972_GoodSovietTimes_nar.017 (001.017)</ta>
            <ta e="T155" id="Seg_3383" s="T150">AnMS_1972_GoodSovietTimes_nar.018 (001.018)</ta>
            <ta e="T161" id="Seg_3384" s="T155">AnMS_1972_GoodSovietTimes_nar.019 (001.019)</ta>
            <ta e="T168" id="Seg_3385" s="T161">AnMS_1972_GoodSovietTimes_nar.020 (001.020)</ta>
            <ta e="T175" id="Seg_3386" s="T168">AnMS_1972_GoodSovietTimes_nar.021 (001.021)</ta>
            <ta e="T180" id="Seg_3387" s="T175">AnMS_1972_GoodSovietTimes_nar.022 (001.022)</ta>
            <ta e="T188" id="Seg_3388" s="T180">AnMS_1972_GoodSovietTimes_nar.023 (001.023)</ta>
            <ta e="T194" id="Seg_3389" s="T188">AnMS_1972_GoodSovietTimes_nar.024 (001.024)</ta>
            <ta e="T198" id="Seg_3390" s="T194">AnMS_1972_GoodSovietTimes_nar.025 (001.025)</ta>
            <ta e="T202" id="Seg_3391" s="T198">AnMS_1972_GoodSovietTimes_nar.026 (001.026)</ta>
            <ta e="T205" id="Seg_3392" s="T202">AnMS_1972_GoodSovietTimes_nar.027 (001.027)</ta>
            <ta e="T210" id="Seg_3393" s="T205">AnMS_1972_GoodSovietTimes_nar.028 (001.028)</ta>
            <ta e="T216" id="Seg_3394" s="T210">AnMS_1972_GoodSovietTimes_nar.029 (001.029)</ta>
            <ta e="T223" id="Seg_3395" s="T216">AnMS_1972_GoodSovietTimes_nar.030 (001.030)</ta>
            <ta e="T232" id="Seg_3396" s="T223">AnMS_1972_GoodSovietTimes_nar.031 (001.031)</ta>
            <ta e="T239" id="Seg_3397" s="T232">AnMS_1972_GoodSovietTimes_nar.032 (001.032)</ta>
            <ta e="T246" id="Seg_3398" s="T239">AnMS_1972_GoodSovietTimes_nar.033 (001.033)</ta>
            <ta e="T256" id="Seg_3399" s="T246">AnMS_1972_GoodSovietTimes_nar.034 (001.034)</ta>
            <ta e="T267" id="Seg_3400" s="T256">AnMS_1972_GoodSovietTimes_nar.035 (001.035)</ta>
            <ta e="T272" id="Seg_3401" s="T267">AnMS_1972_GoodSovietTimes_nar.036 (001.036)</ta>
            <ta e="T292" id="Seg_3402" s="T272">AnMS_1972_GoodSovietTimes_nar.037 (001.037)</ta>
            <ta e="T301" id="Seg_3403" s="T292">AnMS_1972_GoodSovietTimes_nar.038 (001.038)</ta>
            <ta e="T307" id="Seg_3404" s="T301">AnMS_1972_GoodSovietTimes_nar.039 (001.039)</ta>
            <ta e="T319" id="Seg_3405" s="T307">AnMS_1972_GoodSovietTimes_nar.040 (001.040)</ta>
            <ta e="T327" id="Seg_3406" s="T319">AnMS_1972_GoodSovietTimes_nar.041 (001.041)</ta>
            <ta e="T339" id="Seg_3407" s="T327">AnMS_1972_GoodSovietTimes_nar.042 (001.042)</ta>
            <ta e="T344" id="Seg_3408" s="T339">AnMS_1972_GoodSovietTimes_nar.043 (001.043)</ta>
            <ta e="T351" id="Seg_3409" s="T344">AnMS_1972_GoodSovietTimes_nar.044 (001.044)</ta>
            <ta e="T359" id="Seg_3410" s="T351">AnMS_1972_GoodSovietTimes_nar.045 (001.045)</ta>
            <ta e="T372" id="Seg_3411" s="T359">AnMS_1972_GoodSovietTimes_nar.046 (001.046)</ta>
            <ta e="T377" id="Seg_3412" s="T372">AnMS_1972_GoodSovietTimes_nar.047 (001.047)</ta>
            <ta e="T383" id="Seg_3413" s="T377">AnMS_1972_GoodSovietTimes_nar.048 (001.048)</ta>
            <ta e="T393" id="Seg_3414" s="T383">AnMS_1972_GoodSovietTimes_nar.049 (001.049)</ta>
            <ta e="T404" id="Seg_3415" s="T393">AnMS_1972_GoodSovietTimes_nar.050 (001.050)</ta>
            <ta e="T410" id="Seg_3416" s="T404">AnMS_1972_GoodSovietTimes_nar.051 (001.051)</ta>
            <ta e="T413" id="Seg_3417" s="T410">AnMS_1972_GoodSovietTimes_nar.052 (001.052)</ta>
            <ta e="T424" id="Seg_3418" s="T413">AnMS_1972_GoodSovietTimes_nar.053 (001.053)</ta>
            <ta e="T441" id="Seg_3419" s="T424">AnMS_1972_GoodSovietTimes_nar.054 (001.054)</ta>
            <ta e="T447" id="Seg_3420" s="T441">AnMS_1972_GoodSovietTimes_nar.055 (001.055)</ta>
            <ta e="T457" id="Seg_3421" s="T447">AnMS_1972_GoodSovietTimes_nar.056 (001.056)</ta>
            <ta e="T462" id="Seg_3422" s="T457">AnMS_1972_GoodSovietTimes_nar.057 (001.057)</ta>
            <ta e="T476" id="Seg_3423" s="T462">AnMS_1972_GoodSovietTimes_nar.058 (001.058)</ta>
            <ta e="T482" id="Seg_3424" s="T476">AnMS_1972_GoodSovietTimes_nar.059 (001.059)</ta>
            <ta e="T485" id="Seg_3425" s="T482">AnMS_1972_GoodSovietTimes_nar.060 (001.060)</ta>
            <ta e="T498" id="Seg_3426" s="T485">AnMS_1972_GoodSovietTimes_nar.061 (001.061)</ta>
            <ta e="T501" id="Seg_3427" s="T498">AnMS_1972_GoodSovietTimes_nar.062 (001.062)</ta>
            <ta e="T513" id="Seg_3428" s="T501">AnMS_1972_GoodSovietTimes_nar.063 (001.063)</ta>
            <ta e="T522" id="Seg_3429" s="T513">AnMS_1972_GoodSovietTimes_nar.064 (001.064)</ta>
            <ta e="T535" id="Seg_3430" s="T522">AnMS_1972_GoodSovietTimes_nar.065 (001.065)</ta>
            <ta e="T545" id="Seg_3431" s="T535">AnMS_1972_GoodSovietTimes_nar.066 (001.066)</ta>
            <ta e="T551" id="Seg_3432" s="T545">AnMS_1972_GoodSovietTimes_nar.067 (001.067)</ta>
            <ta e="T565" id="Seg_3433" s="T551">AnMS_1972_GoodSovietTimes_nar.068 (001.068)</ta>
            <ta e="T574" id="Seg_3434" s="T565">AnMS_1972_GoodSovietTimes_nar.069 (001.069)</ta>
            <ta e="T583" id="Seg_3435" s="T574">AnMS_1972_GoodSovietTimes_nar.070 (001.070)</ta>
            <ta e="T586" id="Seg_3436" s="T583">AnMS_1972_GoodSovietTimes_nar.071 (001.071)</ta>
            <ta e="T595" id="Seg_3437" s="T586">AnMS_1972_GoodSovietTimes_nar.072 (001.072)</ta>
            <ta e="T605" id="Seg_3438" s="T595">AnMS_1972_GoodSovietTimes_nar.073 (001.073)</ta>
            <ta e="T618" id="Seg_3439" s="T605">AnMS_1972_GoodSovietTimes_nar.074 (001.074)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_3440" s="T0">Һабиэскай былаас буоларын багас өссүө дааганы өйдөөбөппүн.</ta>
            <ta e="T13" id="Seg_3441" s="T7">Оччого мин дааганы долуой ого этим.</ta>
            <ta e="T19" id="Seg_3442" s="T13">Каннык да влааска үлэлээбэт ого этим.</ta>
            <ta e="T36" id="Seg_3443" s="T19">Һабиэскай былаас буолар диэн так и, ыйаак эргиллэрэ так и һин дааганы туолкулуур киһи толкулуур көрдүк этэ.</ta>
            <ta e="T53" id="Seg_3444" s="T36">Ыйаак эргиллиитин һагына маӈнай киһини урут баай киһи, кулаак киһи баттыыр, кылгаһы кыарагаһы, дьадагыны диэн ааттыыр этилэрэ.</ta>
            <ta e="T65" id="Seg_3445" s="T53">Оччого дуо мин бу баай киһилэргэ маччыккаан этим всё таки, табаһыт этим.</ta>
            <ta e="T73" id="Seg_3446" s="T65">Ол бэтэрээ өттүгэр онтон туолкулаан истим, билэн истим.</ta>
            <ta e="T78" id="Seg_3447" s="T73">Конешно, кирдик, ыйаак уларыйар эбит.</ta>
            <ta e="T84" id="Seg_3448" s="T78">Урукку царскай ыйаак дьэ бүттэ диэбиттэрэ.</ta>
            <ta e="T95" id="Seg_3449" s="T84">Онтон бэттэк уһугуттан ускуолага үөрэниэгэр диэн товарищ Ленин эһэ диэн этэ.</ta>
            <ta e="T97" id="Seg_3450" s="T95">Ленин закона.</ta>
            <ta e="T103" id="Seg_3451" s="T97">Дьэ, уларыйан-уларыйан ол уларыйан истэ уһугуттан.</ta>
            <ta e="T111" id="Seg_3452" s="T103">Киһи барыта кылгас кыарагас огото, кайа да йон.</ta>
            <ta e="T123" id="Seg_3453" s="T111">Каннык дааганы кылгас кыарагас, дьадаӈы киһи огото барыта үөрэккэ да дьэ начинайдаата.</ta>
            <ta e="T139" id="Seg_3454" s="T123">О, һол эрээри, баайтан аһыы үөрэммиттэр, кулаактан абырана үөрэммиттэр, син (һин) дааганы ускуолага биэриэкпитин диэбэт этилэрэ огону.</ta>
            <ta e="T144" id="Seg_3455" s="T139">Онтон буолбута колхоз буолуокка диэн.</ta>
            <ta e="T150" id="Seg_3456" s="T144">Общий хозяйстваннан дьон ологун туттуогун диэннэр.</ta>
            <ta e="T155" id="Seg_3457" s="T150">Һуок, онуга эмиэ буола тарпатактара.</ta>
            <ta e="T161" id="Seg_3458" s="T155">Кас да дьылынан (буол-), колкоз буолбатактара.</ta>
            <ta e="T168" id="Seg_3459" s="T161">Колхоз буоламмыт биһиги һатаныакпыт һуо, диир этилэрэ.</ta>
            <ta e="T175" id="Seg_3460" s="T168">Урут царскай законӈа үөскээбит дьон, үөрэммит дьон.</ta>
            <ta e="T180" id="Seg_3461" s="T175">Онтон кайдак эрэ гынан… </ta>
            <ta e="T188" id="Seg_3462" s="T180">Кэпсээн, кэпсээннэр, һокуон кэлэн-кэлэн, һубу дойдуларга кэлэн, кэлэн.</ta>
            <ta e="T194" id="Seg_3463" s="T188">Колхоз буоллубут отут сэттистээгиттэн (һэттистээгиттэн), отут агыстаакка.</ta>
            <ta e="T198" id="Seg_3464" s="T194">Конец-таки кирдик.</ta>
            <ta e="T202" id="Seg_3465" s="T198">Дьэ колкоз буол начинайдаан.</ta>
            <ta e="T205" id="Seg_3466" s="T202">Устроинаан каалыӈ уһугуттан.</ta>
            <ta e="T210" id="Seg_3467" s="T205">Оччого колкооһу төрөммүппүт этэ биһиги.</ta>
            <ta e="T216" id="Seg_3468" s="T210">Канна ордук табалаактан онуга колкозка туппуппут.</ta>
            <ta e="T223" id="Seg_3469" s="T216">Канна паастаакпытын, һонон бэйэбит пааспытынан бэйэбит капкааммытынан.</ta>
            <ta e="T232" id="Seg_3470" s="T223">Уһугуттан колхуо ологун тутаммыт, аны бу кэмнэргэ тийдибит дьэ.</ta>
            <ta e="T239" id="Seg_3471" s="T232">Дьэ онтон колкоз олого үчүгэй дуу диэн.</ta>
            <ta e="T246" id="Seg_3472" s="T239">Ол көрдүк буолла буолбутунан дьэ колкуос буолбуппут.</ta>
            <ta e="T256" id="Seg_3473" s="T246">Ол бэтэрээ өттүгэр эмиэ дааганы эрэйдээк кыһалгалар үйэлэр буолар этилэрэ.</ta>
            <ta e="T267" id="Seg_3474" s="T256">Ба-а, кулаакка, урукку царскайга бастарын ыстарбыт киһилэр там балдииттыыр да этилэрэ.</ta>
            <ta e="T272" id="Seg_3475" s="T267">Олор син (һин) эрэйи оӈорооччу этилэр. </ta>
            <ta e="T292" id="Seg_3476" s="T272">Ол балдииты (бандииты), һуок, ол ааты Советскай былаас инни диэк кааман иһэн, ол балдииты (бандииты) һин дааганы ыраастааччы этэ дьаһайааччы да этэ.</ta>
            <ta e="T301" id="Seg_3477" s="T292">Те (дьэ) ол бүппүтүн кэннэ, уһугуттан бу Германия һэриитэ буолбута.</ta>
            <ta e="T307" id="Seg_3478" s="T301">Дьэ итинигэ кыһалла hыспыппыт биһиги һүрдээк.</ta>
            <ta e="T319" id="Seg_3479" s="T307">Ол таӈас даа тэрийиитэ буолар этэ, ол карчыннан даа налог буолар этэ.</ta>
            <ta e="T327" id="Seg_3480" s="T319">Биһиги ол диэк, гуорат һирдэргэ армияга көмөлөһөммүт… </ta>
            <ta e="T339" id="Seg_3481" s="T327">Онтон һыл бүтэн (?) дьэ аны туок да буолуокпут диэбэппин, бүгүӈӈү һанаам учугуой (үчүгэй).</ta>
            <ta e="T344" id="Seg_3482" s="T339">Аны олок көннө, олок улаатта.</ta>
            <ta e="T351" id="Seg_3483" s="T344">Аны бүтүн колкуостар уһугуттан аргизация (организация) көрдүк буоллулар.</ta>
            <ta e="T359" id="Seg_3484" s="T351">Аны мантан инни диэгитин билбэппин, бэйэм кырдьан кааллым.</ta>
            <ta e="T372" id="Seg_3485" s="T359">Так и так, мин һанаабар, үчүгэй олокко аны тийдибит, диибин, һубу кырдьар һааспар.</ta>
            <ta e="T377" id="Seg_3486" s="T372">Аны тот багайбыт, ичигэс багайбыт.</ta>
            <ta e="T383" id="Seg_3487" s="T377">Таӈаһы даа таӈнабыт, аһы даа аһыыбыт.</ta>
            <ta e="T393" id="Seg_3488" s="T383">Бу кa маӈнай миниэнэ бу улакан огом, бу Баһиилай баар.</ta>
            <ta e="T404" id="Seg_3489" s="T393">Бу огом улаатта да үөрэммитэ гинии (гини), ускуолатын бүтэрдэ дааганы учииталга үөрэммитэ.</ta>
            <ta e="T410" id="Seg_3490" s="T404">Онон аны күн бүгүн учиталлыы олорор.</ta>
            <ta e="T413" id="Seg_3491" s="T410">Онтон биир кыыстаакпын.</ta>
            <ta e="T424" id="Seg_3492" s="T413">Ол эмиэ үөрэгин, ускуолатын бүтэрэн, эмиэ быссай (высшай) үөрэнэн, дуоктурдаан олорор аны.</ta>
            <ta e="T441" id="Seg_3493" s="T424">Ол бэйэтин иннигэр учугуой (үчүгэй), ол кэннэ мин даа һанаабар үчүгэй аны – бу олокко олоруулара, аны бу оголорум. </ta>
            <ta e="T447" id="Seg_3494" s="T441">Онуга үһүс эмиэ уолум авийпортка үлэлиир.</ta>
            <ta e="T457" id="Seg_3495" s="T447">Онтон муӈ куччугуй уоллааппын аны, Уйбаан, Антонов Уйбаан (Михеис-).</ta>
            <ta e="T462" id="Seg_3496" s="T457">Ол дуо аны колкуоска үлэлиир.</ta>
            <ta e="T476" id="Seg_3497" s="T462">Колкуос дааганы үлэлиирэ, үлэтэ үлэтин даа үлэлээк колкуос, һин дааганы бэйэтин колкуос иһигэр һин.</ta>
            <ta e="T482" id="Seg_3498" s="T476">Учугуойдык (үчүгэйдик) тоторон, таӈыннаран һин да үлэлэтэр.</ta>
            <ta e="T485" id="Seg_3499" s="T482">Һин даа һарплаттаак.</ta>
            <ta e="T498" id="Seg_3500" s="T485">Киһи канна учугуодук үлэлиирэ ол ордук буолар, канна куһаганнык үлэлиирэ ол ниизэ буолар.</ta>
            <ta e="T501" id="Seg_3501" s="T498">Мин һанаабар һитигирдик.</ta>
            <ta e="T513" id="Seg_3502" s="T501">Бу кэ маӈнай бэйэм һин дааганы бу сельсоветыннан, тугунан руководствовалаан олорор этим.</ta>
            <ta e="T522" id="Seg_3503" s="T513">Онтон, аны ого киһилэр үөрэккэ үөрэнэннэр, минигиннээгэр лучче (лучше) үлэлииһилэр.</ta>
            <ta e="T535" id="Seg_3504" s="T522">Оччого дуо, кимиэкэ дааганы буруй һаӈата һуок, аны һынньаныам, диэммин, сельсоветтаан бүппүтүм этэ.</ta>
            <ta e="T545" id="Seg_3505" s="T535">Аны бу оголорум үөрэккэ үөрэнэннэн учугуойдык олороллор онтон.</ta>
            <ta e="T551" id="Seg_3506" s="T545">Аны үөрэнэ олоробун бу кырдьан барааммын.</ta>
            <ta e="T565" id="Seg_3507" s="T551">Бэйэм үөрэгэ һуок багай этим, ол кэннэ даже мин тылы да кыайан истибэппин – этим.</ta>
            <ta e="T574" id="Seg_3508" s="T565">Каччага ол мин сельсовет эрдэкпинэн о-о туок эмэ перевоодчигунан.</ta>
            <ta e="T583" id="Seg_3509" s="T574">Биир эмэ тылкааны коһуон киһи һаӈатынан эргэтээччи этибит перевоодчигунан.</ta>
            <ta e="T586" id="Seg_3510" s="T583">Ол эрэйдээк этэ.</ta>
            <ta e="T595" id="Seg_3511" s="T586">Аны эрэйэ һуок бэйэлэрэ сырай (һырай) кэпсэтэллэр кими да гытта.</ta>
            <ta e="T605" id="Seg_3512" s="T595">Советскай былаас учугуой бэрт олокко тириэрдэ аны Советскай былаас буолан.</ta>
            <ta e="T618" id="Seg_3513" s="T605">Оголорбор дааганы мин дааганы баччабар тийдим аны, бэрт учугуойдук бачча (бачча) кырдьыакпар тийдим.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_3514" s="T0">Habi͡eskaj bɨlaːs bu͡olarɨn bagas össü͡ö daːganɨ öjdöːböppün. </ta>
            <ta e="T13" id="Seg_3515" s="T7">Oččogo min daːganɨ dolu͡oj ogo etim. </ta>
            <ta e="T19" id="Seg_3516" s="T13">Kannɨk da vlaːska üleleːbet ogo etim. </ta>
            <ta e="T36" id="Seg_3517" s="T19">Habi͡eskaj bɨlaːs bu͡olar di͡en tak i, ɨjaːk ergillere tak i, hin daːganɨ tu͡olkuluːr kihi tu͡olkuluːr kördük ete. </ta>
            <ta e="T53" id="Seg_3518" s="T36">ɨjaːk ergilliːtin hagɨna maŋnaj kihini urut baːj kihi, kulaːk kihi battɨːr, kɨlgahɨ kɨ͡aragahɨ, dʼadagɨnɨ di͡en aːttɨːr etilere. </ta>
            <ta e="T65" id="Seg_3519" s="T53">Oččogo du͡o min bu baːj kihilerge maččɨkkaːn etim vsʼo takʼi, tabahɨt etim. </ta>
            <ta e="T73" id="Seg_3520" s="T65">Ol betereː öttüger onton tu͡olkulaːn istim, bilen istim. </ta>
            <ta e="T78" id="Seg_3521" s="T73">Kanʼešna, kirdik, ɨjaːk ularɨjar ebit. </ta>
            <ta e="T84" id="Seg_3522" s="T78">"Urukku sarskaj ɨjaːk dʼe bütte", di͡ebittere. </ta>
            <ta e="T95" id="Seg_3523" s="T84">Onton bettek uhuguttan usku͡olaga ü͡öreni͡eger di͡en tavarʼišʼ Lʼenʼin ehe di͡en ete. </ta>
            <ta e="T97" id="Seg_3524" s="T95">Lʼenʼin zakona. </ta>
            <ta e="T103" id="Seg_3525" s="T97">Dʼe, ularɨjan-ularɨjan ol ularɨjan iste uhuguttan. </ta>
            <ta e="T111" id="Seg_3526" s="T103">Kihi barɨta kɨlgas kɨ͡aragas ogoto, kaja da jon. </ta>
            <ta e="T123" id="Seg_3527" s="T111">Kannɨk daːganɨ kɨlgas kɨ͡aragas, dʼadaŋɨ kihi ogoto barɨta ü͡örekke da dʼe načʼinajdaːta. </ta>
            <ta e="T139" id="Seg_3528" s="T123">O, hol ereːri, baːjtan ahɨː ü͡öremmitter, kulaːktan abɨrana ü͡öremmitter, sin daːganɨ usku͡olaga bi͡eri͡ekpitin di͡ebet etilere ogonu. </ta>
            <ta e="T144" id="Seg_3529" s="T139">Onton bu͡olbuta kolkoz bu͡olu͡okka di͡en. </ta>
            <ta e="T150" id="Seg_3530" s="T144">Obšʼij xazʼajstvannan dʼon ologun tuttu͡ogun di͡enner. </ta>
            <ta e="T155" id="Seg_3531" s="T150">Hu͡ok, onuga emi͡e bu͡ola tarpataktara. </ta>
            <ta e="T161" id="Seg_3532" s="T155">Kas da dʼɨlɨnan (bu͡ol-), kolkoz bu͡olbataktara. </ta>
            <ta e="T168" id="Seg_3533" s="T161">"Kolkoz bu͡olammɨt bihigi hatanɨ͡akpɨt hu͡o", diːr etilere. </ta>
            <ta e="T175" id="Seg_3534" s="T168">Urut sarskaj zakoŋŋa ü͡öskeːbit dʼon, ü͡öremmit dʼon. </ta>
            <ta e="T180" id="Seg_3535" s="T175">Onton kajdak ere gɨnan…</ta>
            <ta e="T188" id="Seg_3536" s="T180">Kepseːn, kepseːnner, hoku͡on kelen-kelen, hubu dojdularga kelen, kelen. </ta>
            <ta e="T194" id="Seg_3537" s="T188">Kolkoz bu͡ollubut otut settisteːgitten, otut agɨstaːkka. </ta>
            <ta e="T198" id="Seg_3538" s="T194">Kаnʼec-takɨ kirdik. </ta>
            <ta e="T202" id="Seg_3539" s="T198">Dʼe kolkoz bu͡ol načʼinajdaːn. </ta>
            <ta e="T205" id="Seg_3540" s="T202">Ustrojnaːn (kaːl-) uhuguttan. </ta>
            <ta e="T210" id="Seg_3541" s="T205">Oččogo kolkoːhu törömmüppüt ete bihigi. </ta>
            <ta e="T216" id="Seg_3542" s="T210">Kanna orduk tabalaːktan onuga kolkozka tuppupput. </ta>
            <ta e="T223" id="Seg_3543" s="T216">Kanna paːstaːkpɨtɨn, honon bejebit paːspɨtɨnan bejebit kapkaːmmɨtɨnan. </ta>
            <ta e="T232" id="Seg_3544" s="T223">Uhuguttan kolku͡o ologun tutammɨt, anɨ bu kemnerge tijdibit dʼe. </ta>
            <ta e="T239" id="Seg_3545" s="T232">Dʼe onton kolkoz ologo üčügej duː di͡en. </ta>
            <ta e="T246" id="Seg_3546" s="T239">Ol kördük bu͡olla bu͡olbutunan dʼe kolku͡os bu͡olbupput. </ta>
            <ta e="T256" id="Seg_3547" s="T246">Ol betereː öttüger emi͡e daːganɨ erejdeːk kɨhalgalaːk üjeler bu͡olar etilere. </ta>
            <ta e="T267" id="Seg_3548" s="T256">Baː, kulaːkka, urukku sarskajga bastarɨn ɨstarbɨt kihiler tam baldiːttɨːr da etilere. </ta>
            <ta e="T272" id="Seg_3549" s="T267">Olor sin ereji oŋoroːčču etiler. </ta>
            <ta e="T292" id="Seg_3550" s="T272">Ol baldʼiːtɨ, hu͡ok, ol aːtɨ savʼeskaj bɨlaːs inni di͡ek kaːman ihen, ol baldʼiːtɨ hin daːganɨ ɨraːstaːččɨ ete dʼahajaːččɨ da ete. </ta>
            <ta e="T301" id="Seg_3551" s="T292">Tʼe ol büppütün kenne, uhuguttan bu Gʼermanʼija heriːte bu͡olbuta. </ta>
            <ta e="T307" id="Seg_3552" s="T301">Dʼe itinige kɨhalla hɨspɨppɨt bihigi hürdeːk. </ta>
            <ta e="T319" id="Seg_3553" s="T307">Ol taŋas daː terijiːte bu͡olar ete, ol karčɨnnan daː nalog bu͡olar ete. </ta>
            <ta e="T327" id="Seg_3554" s="T319">Bihigi ol di͡ek, gu͡orat hirderge armijaga kömölöhömmüt…</ta>
            <ta e="T339" id="Seg_3555" s="T327">Onton hɨl büten dʼe anɨ tu͡ok da bu͡olu͡okput di͡ebeppin, bügüŋŋü hanaːm üčügej. </ta>
            <ta e="T344" id="Seg_3556" s="T339">Anɨ olok könnö, olok ulaːtta. </ta>
            <ta e="T351" id="Seg_3557" s="T344">Anɨ bütün kolku͡ostar uhuguttan argijaːsija kördük bu͡ollular. </ta>
            <ta e="T359" id="Seg_3558" s="T351">Anɨ mantan inni di͡egitin bilbeppin, bejem kɨrdʼan kaːllɨm. </ta>
            <ta e="T372" id="Seg_3559" s="T359">Tak i tak, min hanaːbar, üčügej olokko anɨ tijdibit, diːbin, hubu kɨrdʼar haːspar. </ta>
            <ta e="T377" id="Seg_3560" s="T372">Anɨ tot bagajbɨt, ičiges bagajbɨt. </ta>
            <ta e="T383" id="Seg_3561" s="T377">Taŋahɨ daː taŋnabɨt, ahɨ daː ahɨːbɨt. </ta>
            <ta e="T393" id="Seg_3562" s="T383">Bu ka maŋnaj mini͡ene bu ulakan ogom, bu Bahiːlaj baːr. </ta>
            <ta e="T404" id="Seg_3563" s="T393">Bu ogom ulaːtta da ü͡öremmite giniː, usku͡olatɨn büterde daːganɨ učiːtalga ü͡öremmite. </ta>
            <ta e="T410" id="Seg_3564" s="T404">Onon anɨ kün bügün učitallɨː oloror. </ta>
            <ta e="T413" id="Seg_3565" s="T410">Onton biːr kɨːstaːkpɨn. </ta>
            <ta e="T424" id="Seg_3566" s="T413">Ol emi͡e ü͡öregin, usku͡olatɨn büteren, emi͡e bɨssaj ü͡örenen, du͡okturdaːn oloror anɨ. </ta>
            <ta e="T441" id="Seg_3567" s="T424">Ol bejetin inniger üčügej, ol kenne min daː hanaːbar üčügej anɨ – bu olokko oloruːlara, anɨ bu ogolorum. </ta>
            <ta e="T447" id="Seg_3568" s="T441">Onuga ühüs emi͡e u͡olum avijporka üleliːr. </ta>
            <ta e="T457" id="Seg_3569" s="T447">Onton muŋ kuččuguj u͡ollaːppɨn anɨ, Ujbaːn, Antonаv Ujbaːn (Mʼixʼeis-). </ta>
            <ta e="T462" id="Seg_3570" s="T457">Ol du͡o anɨ kolku͡oska üleliːr. </ta>
            <ta e="T476" id="Seg_3571" s="T462">Kolku͡os daːganɨ üleliːre, ülete ületin daː üleleːk kolku͡os, hin daːganɨ bejetin kolku͡os ihiger hin. </ta>
            <ta e="T482" id="Seg_3572" s="T476">Üčügejdik totoron, taŋɨnnaran hin da üleleter. </ta>
            <ta e="T485" id="Seg_3573" s="T482">Hin daː harpalaːttaːk. </ta>
            <ta e="T498" id="Seg_3574" s="T485">Kihi kanna učugu͡oduk üleliːre ol orduk bu͡olar, kanna kuhagannɨk üleliːre ol niːze bu͡olar. </ta>
            <ta e="T501" id="Seg_3575" s="T498">Min hanaːbar hitigirdik. </ta>
            <ta e="T513" id="Seg_3576" s="T501">Bu ke maŋnaj bejem hin daːganɨ bu sʼelʼsavʼetɨnnan, tugunan rukavostvavalaːn oloror etim. </ta>
            <ta e="T522" id="Seg_3577" s="T513">Onton, anɨ ogo kihiler ü͡örekke ü͡örenenner, miniginneːger luːčče üleliːhiler. </ta>
            <ta e="T535" id="Seg_3578" s="T522">Oččogo du͡o, kimi͡eke daːganɨ buruj haŋata hu͡ok, "anɨ hɨnnʼanɨ͡am", di͡emmin, selsavʼettaːn büppütüm ete. </ta>
            <ta e="T545" id="Seg_3579" s="T535">Anɨ bu ogolorum ü͡örekke ü͡örenenner üčügejdik (olo-) olorollor onton. </ta>
            <ta e="T551" id="Seg_3580" s="T545">Anɨ ü͡örene olorobun bu kɨrdʼan baraːmmɨn. </ta>
            <ta e="T565" id="Seg_3581" s="T551">Bejem ü͡örege hu͡ok bagaj etim, ol kenne daže min tɨlɨ da kɨ͡ajan istibeppin – etim. </ta>
            <ta e="T574" id="Seg_3582" s="T565">Kaččaga ol min sʼelʼsavʼet erdekpinen oː tu͡ok eme pʼerʼevoːččigunan. </ta>
            <ta e="T583" id="Seg_3583" s="T574">Biːr eme tɨlkaːnɨ kohu͡on kihi haŋatɨnan ergeteːčči etibit, pʼerʼevoːččigunan. </ta>
            <ta e="T586" id="Seg_3584" s="T583">Ol erejdeːk ete. </ta>
            <ta e="T595" id="Seg_3585" s="T586">Anɨ ereje hu͡ok bejelere sɨraj kepseteller kimi da gɨtta. </ta>
            <ta e="T605" id="Seg_3586" s="T595">Savʼeskaj bɨlaːs učugu͡oj bert olokko tiri͡erde anɨ habi͡eskaj bɨlaːs bu͡olan. </ta>
            <ta e="T618" id="Seg_3587" s="T605">Ogolorbor daːganɨ min daːganɨ baččabar tijdim anɨ, bert učugu͡ojduk bačča bačča kɨrdʼɨ͡akpar tijdim. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3588" s="T0">habi͡eskaj</ta>
            <ta e="T2" id="Seg_3589" s="T1">bɨlaːs</ta>
            <ta e="T3" id="Seg_3590" s="T2">bu͡ol-ar-ɨ-n</ta>
            <ta e="T4" id="Seg_3591" s="T3">bagas</ta>
            <ta e="T5" id="Seg_3592" s="T4">össü͡ö</ta>
            <ta e="T6" id="Seg_3593" s="T5">daːganɨ</ta>
            <ta e="T7" id="Seg_3594" s="T6">öjdöː-böp-pün</ta>
            <ta e="T8" id="Seg_3595" s="T7">oččogo</ta>
            <ta e="T9" id="Seg_3596" s="T8">min</ta>
            <ta e="T10" id="Seg_3597" s="T9">daːganɨ</ta>
            <ta e="T11" id="Seg_3598" s="T10">dolu͡oj</ta>
            <ta e="T12" id="Seg_3599" s="T11">ogo</ta>
            <ta e="T13" id="Seg_3600" s="T12">e-ti-m</ta>
            <ta e="T14" id="Seg_3601" s="T13">kannɨk</ta>
            <ta e="T15" id="Seg_3602" s="T14">da</ta>
            <ta e="T16" id="Seg_3603" s="T15">vlaːs-ka</ta>
            <ta e="T17" id="Seg_3604" s="T16">üleleː-bet</ta>
            <ta e="T18" id="Seg_3605" s="T17">ogo</ta>
            <ta e="T19" id="Seg_3606" s="T18">e-ti-m</ta>
            <ta e="T20" id="Seg_3607" s="T19">habi͡eskaj</ta>
            <ta e="T21" id="Seg_3608" s="T20">bɨlaːs</ta>
            <ta e="T22" id="Seg_3609" s="T21">bu͡ol-ar</ta>
            <ta e="T23" id="Seg_3610" s="T22">di͡e-n</ta>
            <ta e="T26" id="Seg_3611" s="T25">ɨjaːk</ta>
            <ta e="T27" id="Seg_3612" s="T26">ergill-er-e</ta>
            <ta e="T30" id="Seg_3613" s="T29">hin</ta>
            <ta e="T31" id="Seg_3614" s="T30">daːganɨ</ta>
            <ta e="T32" id="Seg_3615" s="T31">tu͡olkuluː-r</ta>
            <ta e="T33" id="Seg_3616" s="T32">kihi</ta>
            <ta e="T34" id="Seg_3617" s="T33">tu͡olkuluː-r</ta>
            <ta e="T35" id="Seg_3618" s="T34">kördük</ta>
            <ta e="T36" id="Seg_3619" s="T35">e-t-e</ta>
            <ta e="T37" id="Seg_3620" s="T36">ɨjaːk</ta>
            <ta e="T38" id="Seg_3621" s="T37">ergill-iː-ti-n</ta>
            <ta e="T39" id="Seg_3622" s="T38">hagɨna</ta>
            <ta e="T40" id="Seg_3623" s="T39">maŋnaj</ta>
            <ta e="T41" id="Seg_3624" s="T40">kihi-ni</ta>
            <ta e="T42" id="Seg_3625" s="T41">urut</ta>
            <ta e="T43" id="Seg_3626" s="T42">baːj</ta>
            <ta e="T44" id="Seg_3627" s="T43">kihi</ta>
            <ta e="T45" id="Seg_3628" s="T44">kulaːk</ta>
            <ta e="T46" id="Seg_3629" s="T45">kihi</ta>
            <ta e="T47" id="Seg_3630" s="T46">bat-tɨː-r</ta>
            <ta e="T48" id="Seg_3631" s="T47">kɨlgah-ɨ</ta>
            <ta e="T49" id="Seg_3632" s="T48">kɨ͡aragah-ɨ</ta>
            <ta e="T50" id="Seg_3633" s="T49">dʼadagɨ-nɨ</ta>
            <ta e="T51" id="Seg_3634" s="T50">di͡e-n</ta>
            <ta e="T52" id="Seg_3635" s="T51">aːt-tɨː-r</ta>
            <ta e="T53" id="Seg_3636" s="T52">e-ti-lere</ta>
            <ta e="T54" id="Seg_3637" s="T53">oččogo</ta>
            <ta e="T55" id="Seg_3638" s="T54">du͡o</ta>
            <ta e="T56" id="Seg_3639" s="T55">min</ta>
            <ta e="T57" id="Seg_3640" s="T56">bu</ta>
            <ta e="T58" id="Seg_3641" s="T57">baːj</ta>
            <ta e="T59" id="Seg_3642" s="T58">kihi-ler-ge</ta>
            <ta e="T60" id="Seg_3643" s="T59">mač-čɨk-kaːn</ta>
            <ta e="T61" id="Seg_3644" s="T60">e-ti-m</ta>
            <ta e="T64" id="Seg_3645" s="T63">taba-hɨt</ta>
            <ta e="T65" id="Seg_3646" s="T64">e-ti-m</ta>
            <ta e="T66" id="Seg_3647" s="T65">ol</ta>
            <ta e="T67" id="Seg_3648" s="T66">betereː</ta>
            <ta e="T68" id="Seg_3649" s="T67">ött-ü-ger</ta>
            <ta e="T69" id="Seg_3650" s="T68">onton</ta>
            <ta e="T70" id="Seg_3651" s="T69">tu͡olkulaː-n</ta>
            <ta e="T71" id="Seg_3652" s="T70">is-ti-m</ta>
            <ta e="T72" id="Seg_3653" s="T71">bil-en</ta>
            <ta e="T73" id="Seg_3654" s="T72">is-ti-m</ta>
            <ta e="T74" id="Seg_3655" s="T73">kanʼešna</ta>
            <ta e="T75" id="Seg_3656" s="T74">kirdik</ta>
            <ta e="T76" id="Seg_3657" s="T75">ɨjaːk</ta>
            <ta e="T77" id="Seg_3658" s="T76">ularɨj-ar</ta>
            <ta e="T78" id="Seg_3659" s="T77">e-bit</ta>
            <ta e="T79" id="Seg_3660" s="T78">urukku</ta>
            <ta e="T80" id="Seg_3661" s="T79">sarskaj</ta>
            <ta e="T81" id="Seg_3662" s="T80">ɨjaːk</ta>
            <ta e="T82" id="Seg_3663" s="T81">dʼe</ta>
            <ta e="T83" id="Seg_3664" s="T82">büt-t-e</ta>
            <ta e="T84" id="Seg_3665" s="T83">di͡e-bit-tere</ta>
            <ta e="T85" id="Seg_3666" s="T84">onton</ta>
            <ta e="T86" id="Seg_3667" s="T85">bettek</ta>
            <ta e="T87" id="Seg_3668" s="T86">uhug-u-ttan</ta>
            <ta e="T88" id="Seg_3669" s="T87">usku͡ola-ga</ta>
            <ta e="T89" id="Seg_3670" s="T88">ü͡ören-i͡e-ger</ta>
            <ta e="T90" id="Seg_3671" s="T89">di͡e-n</ta>
            <ta e="T91" id="Seg_3672" s="T90">tavarʼišʼ</ta>
            <ta e="T92" id="Seg_3673" s="T91">Lʼenʼin</ta>
            <ta e="T93" id="Seg_3674" s="T92">ehe</ta>
            <ta e="T94" id="Seg_3675" s="T93">di͡e-n</ta>
            <ta e="T95" id="Seg_3676" s="T94">e-t-e</ta>
            <ta e="T96" id="Seg_3677" s="T95">Lʼenʼin</ta>
            <ta e="T97" id="Seg_3678" s="T96">zakon-a</ta>
            <ta e="T98" id="Seg_3679" s="T97">dʼe</ta>
            <ta e="T99" id="Seg_3680" s="T98">ularɨj-an-ularɨj-an</ta>
            <ta e="T100" id="Seg_3681" s="T99">ol</ta>
            <ta e="T101" id="Seg_3682" s="T100">ularɨj-an</ta>
            <ta e="T102" id="Seg_3683" s="T101">is-t-e</ta>
            <ta e="T103" id="Seg_3684" s="T102">uhug-u-ttan</ta>
            <ta e="T104" id="Seg_3685" s="T103">kihi</ta>
            <ta e="T105" id="Seg_3686" s="T104">barɨ-ta</ta>
            <ta e="T106" id="Seg_3687" s="T105">kɨlgas</ta>
            <ta e="T107" id="Seg_3688" s="T106">kɨ͡aragas</ta>
            <ta e="T108" id="Seg_3689" s="T107">ogo-to</ta>
            <ta e="T109" id="Seg_3690" s="T108">kaja</ta>
            <ta e="T110" id="Seg_3691" s="T109">da</ta>
            <ta e="T111" id="Seg_3692" s="T110">jon</ta>
            <ta e="T112" id="Seg_3693" s="T111">kannɨk</ta>
            <ta e="T113" id="Seg_3694" s="T112">daːganɨ</ta>
            <ta e="T114" id="Seg_3695" s="T113">kɨlgas</ta>
            <ta e="T115" id="Seg_3696" s="T114">kɨ͡aragas</ta>
            <ta e="T116" id="Seg_3697" s="T115">dʼadaŋɨ</ta>
            <ta e="T117" id="Seg_3698" s="T116">kihi</ta>
            <ta e="T118" id="Seg_3699" s="T117">ogo-to</ta>
            <ta e="T119" id="Seg_3700" s="T118">barɨ-ta</ta>
            <ta e="T120" id="Seg_3701" s="T119">ü͡örek-ke</ta>
            <ta e="T121" id="Seg_3702" s="T120">da</ta>
            <ta e="T122" id="Seg_3703" s="T121">dʼe</ta>
            <ta e="T123" id="Seg_3704" s="T122">načʼinaj-daː-t-a</ta>
            <ta e="T124" id="Seg_3705" s="T123">o</ta>
            <ta e="T125" id="Seg_3706" s="T124">hol</ta>
            <ta e="T126" id="Seg_3707" s="T125">ereːri</ta>
            <ta e="T127" id="Seg_3708" s="T126">baːj-tan</ta>
            <ta e="T128" id="Seg_3709" s="T127">ah-ɨː</ta>
            <ta e="T129" id="Seg_3710" s="T128">ü͡örem-mit-ter</ta>
            <ta e="T130" id="Seg_3711" s="T129">kulaːk-tan</ta>
            <ta e="T131" id="Seg_3712" s="T130">abɨr-a-n-a</ta>
            <ta e="T132" id="Seg_3713" s="T131">ü͡örem-mit-ter</ta>
            <ta e="T133" id="Seg_3714" s="T132">sin</ta>
            <ta e="T134" id="Seg_3715" s="T133">daːganɨ</ta>
            <ta e="T135" id="Seg_3716" s="T134">usku͡ola-ga</ta>
            <ta e="T136" id="Seg_3717" s="T135">bi͡er-i͡ek-piti-n</ta>
            <ta e="T137" id="Seg_3718" s="T136">di͡e-bet</ta>
            <ta e="T138" id="Seg_3719" s="T137">e-ti-lere</ta>
            <ta e="T139" id="Seg_3720" s="T138">ogo-nu</ta>
            <ta e="T140" id="Seg_3721" s="T139">onton</ta>
            <ta e="T141" id="Seg_3722" s="T140">bu͡ol-but-a</ta>
            <ta e="T142" id="Seg_3723" s="T141">kolkoz</ta>
            <ta e="T143" id="Seg_3724" s="T142">bu͡ol-u͡ok-ka</ta>
            <ta e="T144" id="Seg_3725" s="T143">di͡e-n</ta>
            <ta e="T145" id="Seg_3726" s="T144">obšʼij</ta>
            <ta e="T146" id="Seg_3727" s="T145">xazʼajstva-nnan</ta>
            <ta e="T147" id="Seg_3728" s="T146">dʼon</ta>
            <ta e="T148" id="Seg_3729" s="T147">olog-u-n</ta>
            <ta e="T149" id="Seg_3730" s="T148">tutt-u͡og-u-n</ta>
            <ta e="T150" id="Seg_3731" s="T149">di͡e-n-ner</ta>
            <ta e="T151" id="Seg_3732" s="T150">hu͡ok</ta>
            <ta e="T152" id="Seg_3733" s="T151">onu-ga</ta>
            <ta e="T153" id="Seg_3734" s="T152">emi͡e</ta>
            <ta e="T154" id="Seg_3735" s="T153">bu͡ol-a</ta>
            <ta e="T155" id="Seg_3736" s="T154">tar-patak-tara</ta>
            <ta e="T156" id="Seg_3737" s="T155">kas</ta>
            <ta e="T157" id="Seg_3738" s="T156">da</ta>
            <ta e="T158" id="Seg_3739" s="T157">dʼɨl-ɨ-nan</ta>
            <ta e="T159" id="Seg_3740" s="T158">bu͡ol</ta>
            <ta e="T160" id="Seg_3741" s="T159">kolkoz</ta>
            <ta e="T161" id="Seg_3742" s="T160">bu͡ol-batak-tara</ta>
            <ta e="T162" id="Seg_3743" s="T161">kolkoz</ta>
            <ta e="T163" id="Seg_3744" s="T162">bu͡ol-am-mɨt</ta>
            <ta e="T164" id="Seg_3745" s="T163">bihigi</ta>
            <ta e="T165" id="Seg_3746" s="T164">hatan-ɨ͡ak-pɨt</ta>
            <ta e="T166" id="Seg_3747" s="T165">hu͡o</ta>
            <ta e="T167" id="Seg_3748" s="T166">diː-r</ta>
            <ta e="T168" id="Seg_3749" s="T167">e-ti-lere</ta>
            <ta e="T169" id="Seg_3750" s="T168">urut</ta>
            <ta e="T170" id="Seg_3751" s="T169">sarskaj</ta>
            <ta e="T171" id="Seg_3752" s="T170">zakoŋ-ŋa</ta>
            <ta e="T172" id="Seg_3753" s="T171">ü͡öskeː-bit</ta>
            <ta e="T173" id="Seg_3754" s="T172">dʼon</ta>
            <ta e="T174" id="Seg_3755" s="T173">ü͡örem-mit</ta>
            <ta e="T175" id="Seg_3756" s="T174">dʼon</ta>
            <ta e="T176" id="Seg_3757" s="T175">onton</ta>
            <ta e="T177" id="Seg_3758" s="T176">kajdak</ta>
            <ta e="T178" id="Seg_3759" s="T177">ere</ta>
            <ta e="T180" id="Seg_3760" s="T178">gɨn-an</ta>
            <ta e="T181" id="Seg_3761" s="T180">kepseː-n</ta>
            <ta e="T182" id="Seg_3762" s="T181">kepseː-n-ner</ta>
            <ta e="T183" id="Seg_3763" s="T182">hoku͡on</ta>
            <ta e="T184" id="Seg_3764" s="T183">kel-en-kel-en</ta>
            <ta e="T185" id="Seg_3765" s="T184">hubu</ta>
            <ta e="T186" id="Seg_3766" s="T185">dojdu-lar-ga</ta>
            <ta e="T187" id="Seg_3767" s="T186">kel-en</ta>
            <ta e="T188" id="Seg_3768" s="T187">kel-en</ta>
            <ta e="T189" id="Seg_3769" s="T188">kolkoz</ta>
            <ta e="T190" id="Seg_3770" s="T189">bu͡ol-lu-but</ta>
            <ta e="T191" id="Seg_3771" s="T190">otut</ta>
            <ta e="T192" id="Seg_3772" s="T191">sett-is-teːg-i-tten</ta>
            <ta e="T193" id="Seg_3773" s="T192">otut</ta>
            <ta e="T194" id="Seg_3774" s="T193">agɨs-taːk-ka</ta>
            <ta e="T198" id="Seg_3775" s="T196">kirdik</ta>
            <ta e="T199" id="Seg_3776" s="T198">dʼe</ta>
            <ta e="T200" id="Seg_3777" s="T199">kolkoz</ta>
            <ta e="T201" id="Seg_3778" s="T200">bu͡ol</ta>
            <ta e="T202" id="Seg_3779" s="T201">načʼinaj-daː-n</ta>
            <ta e="T203" id="Seg_3780" s="T202">ustroj-naː-n</ta>
            <ta e="T204" id="Seg_3781" s="T203">kaːl</ta>
            <ta e="T205" id="Seg_3782" s="T204">uhug-u-ttan</ta>
            <ta e="T206" id="Seg_3783" s="T205">oččogo</ta>
            <ta e="T207" id="Seg_3784" s="T206">kolkoːh-u</ta>
            <ta e="T208" id="Seg_3785" s="T207">tör-ö-m-müp-püt</ta>
            <ta e="T209" id="Seg_3786" s="T208">e-t-e</ta>
            <ta e="T210" id="Seg_3787" s="T209">bihigi</ta>
            <ta e="T211" id="Seg_3788" s="T210">kanna</ta>
            <ta e="T212" id="Seg_3789" s="T211">orduk</ta>
            <ta e="T213" id="Seg_3790" s="T212">taba-laːk-tan</ta>
            <ta e="T214" id="Seg_3791" s="T213">onu-ga</ta>
            <ta e="T215" id="Seg_3792" s="T214">kolkoz-ka</ta>
            <ta e="T216" id="Seg_3793" s="T215">tup-pup-put</ta>
            <ta e="T217" id="Seg_3794" s="T216">kanna</ta>
            <ta e="T218" id="Seg_3795" s="T217">paːs-taːk-pɨtɨ-n</ta>
            <ta e="T219" id="Seg_3796" s="T218">h-onon</ta>
            <ta e="T220" id="Seg_3797" s="T219">beje-bit</ta>
            <ta e="T221" id="Seg_3798" s="T220">paːs-pɨtɨ-nan</ta>
            <ta e="T222" id="Seg_3799" s="T221">beje-bit</ta>
            <ta e="T223" id="Seg_3800" s="T222">kapkaːm-mɨtɨ-nan</ta>
            <ta e="T224" id="Seg_3801" s="T223">uhug-u-ttan</ta>
            <ta e="T225" id="Seg_3802" s="T224">kolku͡o</ta>
            <ta e="T226" id="Seg_3803" s="T225">olog-u-n</ta>
            <ta e="T227" id="Seg_3804" s="T226">tut-am-mɨt</ta>
            <ta e="T228" id="Seg_3805" s="T227">anɨ</ta>
            <ta e="T229" id="Seg_3806" s="T228">bu</ta>
            <ta e="T230" id="Seg_3807" s="T229">kem-ner-ge</ta>
            <ta e="T231" id="Seg_3808" s="T230">tij-di-bit</ta>
            <ta e="T232" id="Seg_3809" s="T231">dʼe</ta>
            <ta e="T233" id="Seg_3810" s="T232">dʼe</ta>
            <ta e="T234" id="Seg_3811" s="T233">onton</ta>
            <ta e="T235" id="Seg_3812" s="T234">kolkoz</ta>
            <ta e="T236" id="Seg_3813" s="T235">olog-o</ta>
            <ta e="T237" id="Seg_3814" s="T236">üčügej</ta>
            <ta e="T238" id="Seg_3815" s="T237">duː</ta>
            <ta e="T239" id="Seg_3816" s="T238">di͡e-n</ta>
            <ta e="T240" id="Seg_3817" s="T239">ol</ta>
            <ta e="T241" id="Seg_3818" s="T240">kördük</ta>
            <ta e="T242" id="Seg_3819" s="T241">bu͡olla</ta>
            <ta e="T243" id="Seg_3820" s="T242">bu͡ol-but-u-nan</ta>
            <ta e="T244" id="Seg_3821" s="T243">dʼe</ta>
            <ta e="T245" id="Seg_3822" s="T244">kolku͡os</ta>
            <ta e="T246" id="Seg_3823" s="T245">bu͡ol-bup-put</ta>
            <ta e="T247" id="Seg_3824" s="T246">ol</ta>
            <ta e="T248" id="Seg_3825" s="T247">betereː</ta>
            <ta e="T249" id="Seg_3826" s="T248">ött-ü-ger</ta>
            <ta e="T250" id="Seg_3827" s="T249">emi͡e</ta>
            <ta e="T251" id="Seg_3828" s="T250">daːganɨ</ta>
            <ta e="T252" id="Seg_3829" s="T251">erej-deːk</ta>
            <ta e="T253" id="Seg_3830" s="T252">kɨhalga-laːk</ta>
            <ta e="T254" id="Seg_3831" s="T253">üje-ler</ta>
            <ta e="T255" id="Seg_3832" s="T254">bu͡ol-ar</ta>
            <ta e="T256" id="Seg_3833" s="T255">e-ti-lere</ta>
            <ta e="T257" id="Seg_3834" s="T256">baː</ta>
            <ta e="T258" id="Seg_3835" s="T257">kulaːk-ka</ta>
            <ta e="T259" id="Seg_3836" s="T258">urukku</ta>
            <ta e="T260" id="Seg_3837" s="T259">sarskaj-ga</ta>
            <ta e="T261" id="Seg_3838" s="T260">bas-tar-ɨ-n</ta>
            <ta e="T262" id="Seg_3839" s="T261">ɨs-tar-bɨt</ta>
            <ta e="T263" id="Seg_3840" s="T262">kihi-ler</ta>
            <ta e="T265" id="Seg_3841" s="T264">baldiːt-tɨː-r</ta>
            <ta e="T266" id="Seg_3842" s="T265">da</ta>
            <ta e="T267" id="Seg_3843" s="T266">e-ti-lere</ta>
            <ta e="T268" id="Seg_3844" s="T267">o-lor</ta>
            <ta e="T269" id="Seg_3845" s="T268">sin</ta>
            <ta e="T270" id="Seg_3846" s="T269">erej-i</ta>
            <ta e="T271" id="Seg_3847" s="T270">oŋor-oːčču</ta>
            <ta e="T272" id="Seg_3848" s="T271">e-ti-ler</ta>
            <ta e="T273" id="Seg_3849" s="T272">ol</ta>
            <ta e="T274" id="Seg_3850" s="T273">baldʼiːt-ɨ</ta>
            <ta e="T275" id="Seg_3851" s="T274">hu͡ok</ta>
            <ta e="T276" id="Seg_3852" s="T275">ol</ta>
            <ta e="T277" id="Seg_3853" s="T276">aːt-ɨ</ta>
            <ta e="T278" id="Seg_3854" s="T277">savʼeskaj</ta>
            <ta e="T279" id="Seg_3855" s="T278">bɨlaːs</ta>
            <ta e="T280" id="Seg_3856" s="T279">inni</ta>
            <ta e="T281" id="Seg_3857" s="T280">di͡ek</ta>
            <ta e="T282" id="Seg_3858" s="T281">kaːm-an</ta>
            <ta e="T283" id="Seg_3859" s="T282">ih-en</ta>
            <ta e="T284" id="Seg_3860" s="T283">ol</ta>
            <ta e="T285" id="Seg_3861" s="T284">baldʼiːt-ɨ</ta>
            <ta e="T286" id="Seg_3862" s="T285">hin</ta>
            <ta e="T287" id="Seg_3863" s="T286">daːganɨ</ta>
            <ta e="T288" id="Seg_3864" s="T287">ɨraːstaː-ččɨ</ta>
            <ta e="T289" id="Seg_3865" s="T288">e-t-e</ta>
            <ta e="T290" id="Seg_3866" s="T289">dʼahaj-aːččɨ</ta>
            <ta e="T291" id="Seg_3867" s="T290">da</ta>
            <ta e="T292" id="Seg_3868" s="T291">e-t-e</ta>
            <ta e="T293" id="Seg_3869" s="T292">tʼe</ta>
            <ta e="T294" id="Seg_3870" s="T293">ol</ta>
            <ta e="T295" id="Seg_3871" s="T294">büp-püt-ü-n</ta>
            <ta e="T296" id="Seg_3872" s="T295">kenne</ta>
            <ta e="T297" id="Seg_3873" s="T296">uhug-u-ttan</ta>
            <ta e="T298" id="Seg_3874" s="T297">bu</ta>
            <ta e="T299" id="Seg_3875" s="T298">Gʼermanʼija</ta>
            <ta e="T300" id="Seg_3876" s="T299">heriː-te</ta>
            <ta e="T301" id="Seg_3877" s="T300">bu͡ol-but-a</ta>
            <ta e="T302" id="Seg_3878" s="T301">dʼe</ta>
            <ta e="T303" id="Seg_3879" s="T302">itini-ge</ta>
            <ta e="T304" id="Seg_3880" s="T303">kɨhall-a</ta>
            <ta e="T305" id="Seg_3881" s="T304">hɨs-pɨp-pɨt</ta>
            <ta e="T306" id="Seg_3882" s="T305">bihigi</ta>
            <ta e="T307" id="Seg_3883" s="T306">hürdeːk</ta>
            <ta e="T308" id="Seg_3884" s="T307">ol</ta>
            <ta e="T309" id="Seg_3885" s="T308">taŋas</ta>
            <ta e="T310" id="Seg_3886" s="T309">daː</ta>
            <ta e="T311" id="Seg_3887" s="T310">terij-iː-te</ta>
            <ta e="T312" id="Seg_3888" s="T311">bu͡ol-ar</ta>
            <ta e="T313" id="Seg_3889" s="T312">e-t-e</ta>
            <ta e="T314" id="Seg_3890" s="T313">ol</ta>
            <ta e="T315" id="Seg_3891" s="T314">karčɨ-nnan</ta>
            <ta e="T316" id="Seg_3892" s="T315">daː</ta>
            <ta e="T317" id="Seg_3893" s="T316">nalog</ta>
            <ta e="T318" id="Seg_3894" s="T317">bu͡ol-ar</ta>
            <ta e="T319" id="Seg_3895" s="T318">e-t-e</ta>
            <ta e="T320" id="Seg_3896" s="T319">bihigi</ta>
            <ta e="T321" id="Seg_3897" s="T320">ol</ta>
            <ta e="T322" id="Seg_3898" s="T321">di͡ek</ta>
            <ta e="T323" id="Seg_3899" s="T322">gu͡orat</ta>
            <ta e="T324" id="Seg_3900" s="T323">hir-der-ge</ta>
            <ta e="T325" id="Seg_3901" s="T324">armʼija-ga</ta>
            <ta e="T327" id="Seg_3902" s="T325">kömölöh-öm-müt</ta>
            <ta e="T328" id="Seg_3903" s="T327">onton</ta>
            <ta e="T329" id="Seg_3904" s="T328">hɨl</ta>
            <ta e="T330" id="Seg_3905" s="T329">büt-en</ta>
            <ta e="T331" id="Seg_3906" s="T330">dʼe</ta>
            <ta e="T332" id="Seg_3907" s="T331">anɨ</ta>
            <ta e="T333" id="Seg_3908" s="T332">tu͡ok</ta>
            <ta e="T334" id="Seg_3909" s="T333">da</ta>
            <ta e="T335" id="Seg_3910" s="T334">bu͡ol-u͡ok-put</ta>
            <ta e="T336" id="Seg_3911" s="T335">di͡e-bep-pin</ta>
            <ta e="T337" id="Seg_3912" s="T336">bügüŋŋü</ta>
            <ta e="T338" id="Seg_3913" s="T337">hanaː-m</ta>
            <ta e="T339" id="Seg_3914" s="T338">üčügej</ta>
            <ta e="T340" id="Seg_3915" s="T339">anɨ</ta>
            <ta e="T341" id="Seg_3916" s="T340">olok</ta>
            <ta e="T342" id="Seg_3917" s="T341">kön-n-ö</ta>
            <ta e="T343" id="Seg_3918" s="T342">olok</ta>
            <ta e="T344" id="Seg_3919" s="T343">ulaːt-t-a</ta>
            <ta e="T345" id="Seg_3920" s="T344">anɨ</ta>
            <ta e="T346" id="Seg_3921" s="T345">bütün</ta>
            <ta e="T347" id="Seg_3922" s="T346">kolku͡os-tar</ta>
            <ta e="T348" id="Seg_3923" s="T347">uhug-u-ttan</ta>
            <ta e="T349" id="Seg_3924" s="T348">argijaːsija</ta>
            <ta e="T350" id="Seg_3925" s="T349">kördük</ta>
            <ta e="T351" id="Seg_3926" s="T350">bu͡ol-lu-lar</ta>
            <ta e="T352" id="Seg_3927" s="T351">anɨ</ta>
            <ta e="T353" id="Seg_3928" s="T352">mantan</ta>
            <ta e="T354" id="Seg_3929" s="T353">inni</ta>
            <ta e="T355" id="Seg_3930" s="T354">di͡egi-ti-n</ta>
            <ta e="T356" id="Seg_3931" s="T355">bil-bep-pin</ta>
            <ta e="T357" id="Seg_3932" s="T356">beje-m</ta>
            <ta e="T358" id="Seg_3933" s="T357">kɨrdʼ-an</ta>
            <ta e="T359" id="Seg_3934" s="T358">kaːl-lɨ-m</ta>
            <ta e="T362" id="Seg_3935" s="T361">min</ta>
            <ta e="T363" id="Seg_3936" s="T362">hanaː-ba-r</ta>
            <ta e="T364" id="Seg_3937" s="T363">üčügej</ta>
            <ta e="T365" id="Seg_3938" s="T364">olok-ko</ta>
            <ta e="T366" id="Seg_3939" s="T365">anɨ</ta>
            <ta e="T367" id="Seg_3940" s="T366">tij-di-bit</ta>
            <ta e="T368" id="Seg_3941" s="T367">d-iː-bin</ta>
            <ta e="T369" id="Seg_3942" s="T368">hubu</ta>
            <ta e="T370" id="Seg_3943" s="T369">kɨrdʼ-ar</ta>
            <ta e="T372" id="Seg_3944" s="T370">haːs-pa-r</ta>
            <ta e="T373" id="Seg_3945" s="T372">anɨ</ta>
            <ta e="T374" id="Seg_3946" s="T373">tot</ta>
            <ta e="T375" id="Seg_3947" s="T374">bagaj-bɨt</ta>
            <ta e="T376" id="Seg_3948" s="T375">ičiges</ta>
            <ta e="T377" id="Seg_3949" s="T376">bagaj-bɨt</ta>
            <ta e="T378" id="Seg_3950" s="T377">taŋah-ɨ</ta>
            <ta e="T379" id="Seg_3951" s="T378">daː</ta>
            <ta e="T380" id="Seg_3952" s="T379">taŋn-a-bɨt</ta>
            <ta e="T381" id="Seg_3953" s="T380">ah-ɨ</ta>
            <ta e="T382" id="Seg_3954" s="T381">daː</ta>
            <ta e="T383" id="Seg_3955" s="T382">ah-ɨː-bɨt</ta>
            <ta e="T384" id="Seg_3956" s="T383">bu</ta>
            <ta e="T385" id="Seg_3957" s="T384">ka</ta>
            <ta e="T386" id="Seg_3958" s="T385">maŋnaj</ta>
            <ta e="T387" id="Seg_3959" s="T386">mini͡ene</ta>
            <ta e="T388" id="Seg_3960" s="T387">bu</ta>
            <ta e="T389" id="Seg_3961" s="T388">ulakan</ta>
            <ta e="T390" id="Seg_3962" s="T389">ogo-m</ta>
            <ta e="T391" id="Seg_3963" s="T390">bu</ta>
            <ta e="T392" id="Seg_3964" s="T391">Bahiːlaj</ta>
            <ta e="T393" id="Seg_3965" s="T392">baːr</ta>
            <ta e="T394" id="Seg_3966" s="T393">bu</ta>
            <ta e="T395" id="Seg_3967" s="T394">ogo-m</ta>
            <ta e="T396" id="Seg_3968" s="T395">ulaːt-t-a</ta>
            <ta e="T397" id="Seg_3969" s="T396">da</ta>
            <ta e="T398" id="Seg_3970" s="T397">ü͡örem-mit-e</ta>
            <ta e="T399" id="Seg_3971" s="T398">giniː</ta>
            <ta e="T400" id="Seg_3972" s="T399">usku͡ola-tɨ-n</ta>
            <ta e="T401" id="Seg_3973" s="T400">büt-e-r-d-e</ta>
            <ta e="T402" id="Seg_3974" s="T401">daːganɨ</ta>
            <ta e="T403" id="Seg_3975" s="T402">učiːtal-ga</ta>
            <ta e="T404" id="Seg_3976" s="T403">ü͡örem-mit-e</ta>
            <ta e="T405" id="Seg_3977" s="T404">onon</ta>
            <ta e="T406" id="Seg_3978" s="T405">anɨ</ta>
            <ta e="T407" id="Seg_3979" s="T406">kün</ta>
            <ta e="T408" id="Seg_3980" s="T407">bügün</ta>
            <ta e="T409" id="Seg_3981" s="T408">učital-l-ɨː</ta>
            <ta e="T410" id="Seg_3982" s="T409">olor-or</ta>
            <ta e="T411" id="Seg_3983" s="T410">onton</ta>
            <ta e="T412" id="Seg_3984" s="T411">biːr</ta>
            <ta e="T413" id="Seg_3985" s="T412">kɨːs-taːk-pɨn</ta>
            <ta e="T414" id="Seg_3986" s="T413">ol</ta>
            <ta e="T415" id="Seg_3987" s="T414">emi͡e</ta>
            <ta e="T416" id="Seg_3988" s="T415">ü͡öreg-i-n</ta>
            <ta e="T417" id="Seg_3989" s="T416">usku͡ola-tɨ-n</ta>
            <ta e="T418" id="Seg_3990" s="T417">büt-e-r-en</ta>
            <ta e="T419" id="Seg_3991" s="T418">emi͡e</ta>
            <ta e="T420" id="Seg_3992" s="T419">bɨssaj</ta>
            <ta e="T421" id="Seg_3993" s="T420">ü͡ören-en</ta>
            <ta e="T422" id="Seg_3994" s="T421">du͡oktur-daː-n</ta>
            <ta e="T423" id="Seg_3995" s="T422">olor-or</ta>
            <ta e="T424" id="Seg_3996" s="T423">anɨ</ta>
            <ta e="T425" id="Seg_3997" s="T424">ol</ta>
            <ta e="T426" id="Seg_3998" s="T425">beje-ti-n</ta>
            <ta e="T427" id="Seg_3999" s="T426">inn-i-ger</ta>
            <ta e="T428" id="Seg_4000" s="T427">üčügej</ta>
            <ta e="T429" id="Seg_4001" s="T428">ol</ta>
            <ta e="T430" id="Seg_4002" s="T429">kenne</ta>
            <ta e="T431" id="Seg_4003" s="T430">min</ta>
            <ta e="T432" id="Seg_4004" s="T431">daː</ta>
            <ta e="T433" id="Seg_4005" s="T432">hanaː-ba-r</ta>
            <ta e="T434" id="Seg_4006" s="T433">üčügej</ta>
            <ta e="T435" id="Seg_4007" s="T434">anɨ</ta>
            <ta e="T436" id="Seg_4008" s="T435">bu</ta>
            <ta e="T437" id="Seg_4009" s="T436">olok-ko</ta>
            <ta e="T438" id="Seg_4010" s="T437">olor-uː-lara</ta>
            <ta e="T439" id="Seg_4011" s="T438">anɨ</ta>
            <ta e="T440" id="Seg_4012" s="T439">bu</ta>
            <ta e="T441" id="Seg_4013" s="T440">ogo-lor-u-m</ta>
            <ta e="T442" id="Seg_4014" s="T441">onu-ga</ta>
            <ta e="T443" id="Seg_4015" s="T442">üh-üs</ta>
            <ta e="T444" id="Seg_4016" s="T443">emi͡e</ta>
            <ta e="T445" id="Seg_4017" s="T444">u͡ol-u-m</ta>
            <ta e="T446" id="Seg_4018" s="T445">avijpor-ka</ta>
            <ta e="T447" id="Seg_4019" s="T446">üleliː-r</ta>
            <ta e="T448" id="Seg_4020" s="T447">onton</ta>
            <ta e="T449" id="Seg_4021" s="T448">muŋ</ta>
            <ta e="T450" id="Seg_4022" s="T449">kuččuguj</ta>
            <ta e="T451" id="Seg_4023" s="T450">u͡ol-laːp-pɨn</ta>
            <ta e="T452" id="Seg_4024" s="T451">anɨ</ta>
            <ta e="T453" id="Seg_4025" s="T452">Ujbaːn</ta>
            <ta e="T454" id="Seg_4026" s="T453">Antonаv</ta>
            <ta e="T455" id="Seg_4027" s="T454">Ujbaːn</ta>
            <ta e="T458" id="Seg_4028" s="T457">ol</ta>
            <ta e="T459" id="Seg_4029" s="T458">du͡o</ta>
            <ta e="T460" id="Seg_4030" s="T459">anɨ</ta>
            <ta e="T461" id="Seg_4031" s="T460">kolku͡os-ka</ta>
            <ta e="T462" id="Seg_4032" s="T461">üleliː-r</ta>
            <ta e="T463" id="Seg_4033" s="T462">kolku͡os</ta>
            <ta e="T464" id="Seg_4034" s="T463">daːganɨ</ta>
            <ta e="T465" id="Seg_4035" s="T464">üleliː-r-e</ta>
            <ta e="T466" id="Seg_4036" s="T465">üle-te</ta>
            <ta e="T467" id="Seg_4037" s="T466">üle-ti-n</ta>
            <ta e="T468" id="Seg_4038" s="T467">daː</ta>
            <ta e="T469" id="Seg_4039" s="T468">üle-leːk</ta>
            <ta e="T470" id="Seg_4040" s="T469">kolku͡os</ta>
            <ta e="T471" id="Seg_4041" s="T470">hin</ta>
            <ta e="T472" id="Seg_4042" s="T471">daːganɨ</ta>
            <ta e="T473" id="Seg_4043" s="T472">beje-ti-n</ta>
            <ta e="T474" id="Seg_4044" s="T473">kolku͡os</ta>
            <ta e="T475" id="Seg_4045" s="T474">ih-i-ger</ta>
            <ta e="T476" id="Seg_4046" s="T475">hin</ta>
            <ta e="T477" id="Seg_4047" s="T476">üčügej-dik</ta>
            <ta e="T478" id="Seg_4048" s="T477">tot-o-r-on</ta>
            <ta e="T479" id="Seg_4049" s="T478">taŋɨn-nar-an</ta>
            <ta e="T480" id="Seg_4050" s="T479">hin</ta>
            <ta e="T481" id="Seg_4051" s="T480">da</ta>
            <ta e="T482" id="Seg_4052" s="T481">ülel-e-t-er</ta>
            <ta e="T483" id="Seg_4053" s="T482">hin</ta>
            <ta e="T484" id="Seg_4054" s="T483">daː</ta>
            <ta e="T485" id="Seg_4055" s="T484">harpalaːt-taːk</ta>
            <ta e="T486" id="Seg_4056" s="T485">kihi</ta>
            <ta e="T487" id="Seg_4057" s="T486">kanna</ta>
            <ta e="T488" id="Seg_4058" s="T487">učugu͡o-duk</ta>
            <ta e="T489" id="Seg_4059" s="T488">üleliː-r-e</ta>
            <ta e="T490" id="Seg_4060" s="T489">ol</ta>
            <ta e="T491" id="Seg_4061" s="T490">orduk</ta>
            <ta e="T492" id="Seg_4062" s="T491">bu͡ol-ar</ta>
            <ta e="T493" id="Seg_4063" s="T492">kanna</ta>
            <ta e="T494" id="Seg_4064" s="T493">kuhagan-nɨk</ta>
            <ta e="T495" id="Seg_4065" s="T494">üleliː-r-e</ta>
            <ta e="T496" id="Seg_4066" s="T495">ol</ta>
            <ta e="T497" id="Seg_4067" s="T496">niːze</ta>
            <ta e="T498" id="Seg_4068" s="T497">bu͡ol-ar</ta>
            <ta e="T499" id="Seg_4069" s="T498">min</ta>
            <ta e="T500" id="Seg_4070" s="T499">hanaː-ba-r</ta>
            <ta e="T501" id="Seg_4071" s="T500">h-itigirdik</ta>
            <ta e="T502" id="Seg_4072" s="T501">bu</ta>
            <ta e="T503" id="Seg_4073" s="T502">ke</ta>
            <ta e="T504" id="Seg_4074" s="T503">maŋnaj</ta>
            <ta e="T505" id="Seg_4075" s="T504">beje-m</ta>
            <ta e="T506" id="Seg_4076" s="T505">hin</ta>
            <ta e="T507" id="Seg_4077" s="T506">daːganɨ</ta>
            <ta e="T508" id="Seg_4078" s="T507">bu</ta>
            <ta e="T509" id="Seg_4079" s="T508">sʼelʼsavʼet-ɨ-nnan</ta>
            <ta e="T510" id="Seg_4080" s="T509">tug-u-nan</ta>
            <ta e="T511" id="Seg_4081" s="T510">rukavostvava-laː-n</ta>
            <ta e="T512" id="Seg_4082" s="T511">olor-or</ta>
            <ta e="T513" id="Seg_4083" s="T512">e-ti-m</ta>
            <ta e="T514" id="Seg_4084" s="T513">onton</ta>
            <ta e="T515" id="Seg_4085" s="T514">anɨ</ta>
            <ta e="T516" id="Seg_4086" s="T515">ogo</ta>
            <ta e="T517" id="Seg_4087" s="T516">kihi-ler</ta>
            <ta e="T518" id="Seg_4088" s="T517">ü͡örek-ke</ta>
            <ta e="T519" id="Seg_4089" s="T518">ü͡ören-en-ner</ta>
            <ta e="T520" id="Seg_4090" s="T519">minigi-nneːger</ta>
            <ta e="T521" id="Seg_4091" s="T520">luːčče</ta>
            <ta e="T522" id="Seg_4092" s="T521">ülel-iːhi-ler</ta>
            <ta e="T523" id="Seg_4093" s="T522">oččogo</ta>
            <ta e="T524" id="Seg_4094" s="T523">du͡o</ta>
            <ta e="T525" id="Seg_4095" s="T524">kimi͡e-ke</ta>
            <ta e="T526" id="Seg_4096" s="T525">daːganɨ</ta>
            <ta e="T527" id="Seg_4097" s="T526">buruj</ta>
            <ta e="T528" id="Seg_4098" s="T527">haŋa-ta</ta>
            <ta e="T529" id="Seg_4099" s="T528">hu͡ok</ta>
            <ta e="T530" id="Seg_4100" s="T529">anɨ</ta>
            <ta e="T531" id="Seg_4101" s="T530">hɨnnʼan-ɨ͡a-m</ta>
            <ta e="T532" id="Seg_4102" s="T531">di͡e-m-min</ta>
            <ta e="T533" id="Seg_4103" s="T532">selsavʼet-taː-n</ta>
            <ta e="T534" id="Seg_4104" s="T533">büp-püt-ü-m</ta>
            <ta e="T535" id="Seg_4105" s="T534">e-t-e</ta>
            <ta e="T536" id="Seg_4106" s="T535">anɨ</ta>
            <ta e="T537" id="Seg_4107" s="T536">bu</ta>
            <ta e="T538" id="Seg_4108" s="T537">ogo-lor-u-m</ta>
            <ta e="T539" id="Seg_4109" s="T538">ü͡örek-ke</ta>
            <ta e="T540" id="Seg_4110" s="T539">ü͡ören-en-ner</ta>
            <ta e="T541" id="Seg_4111" s="T540">üčügej-dik</ta>
            <ta e="T544" id="Seg_4112" s="T543">olor-ol-lor</ta>
            <ta e="T545" id="Seg_4113" s="T544">onton</ta>
            <ta e="T546" id="Seg_4114" s="T545">anɨ</ta>
            <ta e="T547" id="Seg_4115" s="T546">ü͡ör-e-n-e</ta>
            <ta e="T548" id="Seg_4116" s="T547">olor-o-bun</ta>
            <ta e="T549" id="Seg_4117" s="T548">bu</ta>
            <ta e="T550" id="Seg_4118" s="T549">kɨrdʼ-an</ta>
            <ta e="T551" id="Seg_4119" s="T550">bar-aːm-mɨn</ta>
            <ta e="T552" id="Seg_4120" s="T551">beje-m</ta>
            <ta e="T553" id="Seg_4121" s="T552">ü͡öreg-e</ta>
            <ta e="T554" id="Seg_4122" s="T553">hu͡ok</ta>
            <ta e="T555" id="Seg_4123" s="T554">bagaj</ta>
            <ta e="T556" id="Seg_4124" s="T555">e-ti-m</ta>
            <ta e="T557" id="Seg_4125" s="T556">ol</ta>
            <ta e="T558" id="Seg_4126" s="T557">kenne</ta>
            <ta e="T559" id="Seg_4127" s="T558">daže</ta>
            <ta e="T560" id="Seg_4128" s="T559">min</ta>
            <ta e="T561" id="Seg_4129" s="T560">tɨl-ɨ</ta>
            <ta e="T562" id="Seg_4130" s="T561">da</ta>
            <ta e="T563" id="Seg_4131" s="T562">kɨ͡aj-an</ta>
            <ta e="T564" id="Seg_4132" s="T563">ist-i-bep-pin</ta>
            <ta e="T565" id="Seg_4133" s="T564">e-ti-m</ta>
            <ta e="T566" id="Seg_4134" s="T565">kaččaga</ta>
            <ta e="T567" id="Seg_4135" s="T566">ol</ta>
            <ta e="T568" id="Seg_4136" s="T567">min</ta>
            <ta e="T569" id="Seg_4137" s="T568">sʼelʼsavʼet</ta>
            <ta e="T570" id="Seg_4138" s="T569">er-dek-pinen</ta>
            <ta e="T571" id="Seg_4139" s="T570">oː</ta>
            <ta e="T572" id="Seg_4140" s="T571">tu͡ok</ta>
            <ta e="T573" id="Seg_4141" s="T572">eme</ta>
            <ta e="T574" id="Seg_4142" s="T573">pʼerʼevoːččig-u-nan</ta>
            <ta e="T575" id="Seg_4143" s="T574">biːr</ta>
            <ta e="T576" id="Seg_4144" s="T575">eme</ta>
            <ta e="T577" id="Seg_4145" s="T576">tɨl-kaːn-ɨ</ta>
            <ta e="T578" id="Seg_4146" s="T577">kohu͡on</ta>
            <ta e="T579" id="Seg_4147" s="T578">kihi</ta>
            <ta e="T580" id="Seg_4148" s="T579">haŋa-tɨ-nan</ta>
            <ta e="T581" id="Seg_4149" s="T580">erget-eːčči</ta>
            <ta e="T582" id="Seg_4150" s="T581">e-ti-bit</ta>
            <ta e="T583" id="Seg_4151" s="T582">pʼerʼevoːččig-u-nan</ta>
            <ta e="T584" id="Seg_4152" s="T583">ol</ta>
            <ta e="T585" id="Seg_4153" s="T584">erej-deːk</ta>
            <ta e="T586" id="Seg_4154" s="T585">e-t-e</ta>
            <ta e="T587" id="Seg_4155" s="T586">anɨ</ta>
            <ta e="T588" id="Seg_4156" s="T587">erej-e</ta>
            <ta e="T589" id="Seg_4157" s="T588">hu͡ok</ta>
            <ta e="T590" id="Seg_4158" s="T589">beje-lere</ta>
            <ta e="T591" id="Seg_4159" s="T590">sɨraj</ta>
            <ta e="T592" id="Seg_4160" s="T591">kepset-el-ler</ta>
            <ta e="T593" id="Seg_4161" s="T592">kim-i</ta>
            <ta e="T594" id="Seg_4162" s="T593">da</ta>
            <ta e="T595" id="Seg_4163" s="T594">gɨtta</ta>
            <ta e="T596" id="Seg_4164" s="T595">savʼeskaj</ta>
            <ta e="T597" id="Seg_4165" s="T596">bɨlaːs</ta>
            <ta e="T598" id="Seg_4166" s="T597">učugu͡oj</ta>
            <ta e="T599" id="Seg_4167" s="T598">bert</ta>
            <ta e="T600" id="Seg_4168" s="T599">olok-ko</ta>
            <ta e="T601" id="Seg_4169" s="T600">tiri͡er-d-e</ta>
            <ta e="T602" id="Seg_4170" s="T601">anɨ</ta>
            <ta e="T603" id="Seg_4171" s="T602">habi͡eskaj</ta>
            <ta e="T604" id="Seg_4172" s="T603">bɨlaːs</ta>
            <ta e="T605" id="Seg_4173" s="T604">bu͡ol-an</ta>
            <ta e="T606" id="Seg_4174" s="T605">ogo-lor-bo-r</ta>
            <ta e="T607" id="Seg_4175" s="T606">daːganɨ</ta>
            <ta e="T608" id="Seg_4176" s="T607">min</ta>
            <ta e="T609" id="Seg_4177" s="T608">daːganɨ</ta>
            <ta e="T610" id="Seg_4178" s="T609">bačča-ba-r</ta>
            <ta e="T611" id="Seg_4179" s="T610">tij-di-m</ta>
            <ta e="T612" id="Seg_4180" s="T611">anɨ</ta>
            <ta e="T613" id="Seg_4181" s="T612">bert</ta>
            <ta e="T614" id="Seg_4182" s="T613">učugu͡oj-duk</ta>
            <ta e="T615" id="Seg_4183" s="T614">bačča</ta>
            <ta e="T616" id="Seg_4184" s="T615">bačča</ta>
            <ta e="T617" id="Seg_4185" s="T616">kɨrdʼ-ɨ͡ak-pa-r</ta>
            <ta e="T618" id="Seg_4186" s="T617">tij-di-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_4187" s="T0">habi͡eskaj</ta>
            <ta e="T2" id="Seg_4188" s="T1">bɨlaːs</ta>
            <ta e="T3" id="Seg_4189" s="T2">bu͡ol-Ar-tI-n</ta>
            <ta e="T4" id="Seg_4190" s="T3">bagas</ta>
            <ta e="T5" id="Seg_4191" s="T4">össü͡ö</ta>
            <ta e="T6" id="Seg_4192" s="T5">daːganɨ</ta>
            <ta e="T7" id="Seg_4193" s="T6">öjdöː-BAT-BIn</ta>
            <ta e="T8" id="Seg_4194" s="T7">oččogo</ta>
            <ta e="T9" id="Seg_4195" s="T8">min</ta>
            <ta e="T10" id="Seg_4196" s="T9">daːganɨ</ta>
            <ta e="T11" id="Seg_4197" s="T10">dolu͡oj</ta>
            <ta e="T12" id="Seg_4198" s="T11">ogo</ta>
            <ta e="T13" id="Seg_4199" s="T12">e-TI-m</ta>
            <ta e="T14" id="Seg_4200" s="T13">kannɨk</ta>
            <ta e="T15" id="Seg_4201" s="T14">da</ta>
            <ta e="T16" id="Seg_4202" s="T15">bɨlaːs-GA</ta>
            <ta e="T17" id="Seg_4203" s="T16">üleleː-BAT</ta>
            <ta e="T18" id="Seg_4204" s="T17">ogo</ta>
            <ta e="T19" id="Seg_4205" s="T18">e-TI-m</ta>
            <ta e="T20" id="Seg_4206" s="T19">habi͡eskaj</ta>
            <ta e="T21" id="Seg_4207" s="T20">bɨlaːs</ta>
            <ta e="T22" id="Seg_4208" s="T21">bu͡ol-Ar</ta>
            <ta e="T23" id="Seg_4209" s="T22">di͡e-An</ta>
            <ta e="T26" id="Seg_4210" s="T25">ɨjaːk</ta>
            <ta e="T27" id="Seg_4211" s="T26">ergilin-Ar-tA</ta>
            <ta e="T30" id="Seg_4212" s="T29">hin</ta>
            <ta e="T31" id="Seg_4213" s="T30">daːganɨ</ta>
            <ta e="T32" id="Seg_4214" s="T31">tu͡olkulaː-Ar</ta>
            <ta e="T33" id="Seg_4215" s="T32">kihi</ta>
            <ta e="T34" id="Seg_4216" s="T33">tu͡olkulaː-Ar</ta>
            <ta e="T35" id="Seg_4217" s="T34">kördük</ta>
            <ta e="T36" id="Seg_4218" s="T35">e-TI-tA</ta>
            <ta e="T37" id="Seg_4219" s="T36">ɨjaːk</ta>
            <ta e="T38" id="Seg_4220" s="T37">ergilin-Iː-tI-n</ta>
            <ta e="T39" id="Seg_4221" s="T38">hagɨna</ta>
            <ta e="T40" id="Seg_4222" s="T39">maŋnaj</ta>
            <ta e="T41" id="Seg_4223" s="T40">kihi-nI</ta>
            <ta e="T42" id="Seg_4224" s="T41">urut</ta>
            <ta e="T43" id="Seg_4225" s="T42">baːj</ta>
            <ta e="T44" id="Seg_4226" s="T43">kihi</ta>
            <ta e="T45" id="Seg_4227" s="T44">kulaːk</ta>
            <ta e="T46" id="Seg_4228" s="T45">kihi</ta>
            <ta e="T47" id="Seg_4229" s="T46">bat-TAː-Ar</ta>
            <ta e="T48" id="Seg_4230" s="T47">kɨlgas-nI</ta>
            <ta e="T49" id="Seg_4231" s="T48">kɨ͡aragas-nI</ta>
            <ta e="T50" id="Seg_4232" s="T49">dʼadaŋɨ-nI</ta>
            <ta e="T51" id="Seg_4233" s="T50">di͡e-An</ta>
            <ta e="T52" id="Seg_4234" s="T51">aːt-LAː-Ar</ta>
            <ta e="T53" id="Seg_4235" s="T52">e-TI-LArA</ta>
            <ta e="T54" id="Seg_4236" s="T53">oččogo</ta>
            <ta e="T55" id="Seg_4237" s="T54">du͡o</ta>
            <ta e="T56" id="Seg_4238" s="T55">min</ta>
            <ta e="T57" id="Seg_4239" s="T56">bu</ta>
            <ta e="T58" id="Seg_4240" s="T57">baːj</ta>
            <ta e="T59" id="Seg_4241" s="T58">kihi-LAr-GA</ta>
            <ta e="T60" id="Seg_4242" s="T59">mas-ČIt-kAːN</ta>
            <ta e="T61" id="Seg_4243" s="T60">e-TI-m</ta>
            <ta e="T64" id="Seg_4244" s="T63">taba-ČIt</ta>
            <ta e="T65" id="Seg_4245" s="T64">e-TI-m</ta>
            <ta e="T66" id="Seg_4246" s="T65">ol</ta>
            <ta e="T67" id="Seg_4247" s="T66">betereː</ta>
            <ta e="T68" id="Seg_4248" s="T67">örüt-tI-GAr</ta>
            <ta e="T69" id="Seg_4249" s="T68">onton</ta>
            <ta e="T70" id="Seg_4250" s="T69">tu͡olkulaː-An</ta>
            <ta e="T71" id="Seg_4251" s="T70">is-TI-m</ta>
            <ta e="T72" id="Seg_4252" s="T71">bil-An</ta>
            <ta e="T73" id="Seg_4253" s="T72">is-TI-m</ta>
            <ta e="T74" id="Seg_4254" s="T73">kanʼešna</ta>
            <ta e="T75" id="Seg_4255" s="T74">kirdik</ta>
            <ta e="T76" id="Seg_4256" s="T75">ɨjaːk</ta>
            <ta e="T77" id="Seg_4257" s="T76">ularɨj-Ar</ta>
            <ta e="T78" id="Seg_4258" s="T77">e-BIT</ta>
            <ta e="T79" id="Seg_4259" s="T78">urukku</ta>
            <ta e="T80" id="Seg_4260" s="T79">carskaj</ta>
            <ta e="T81" id="Seg_4261" s="T80">ɨjaːk</ta>
            <ta e="T82" id="Seg_4262" s="T81">dʼe</ta>
            <ta e="T83" id="Seg_4263" s="T82">büt-TI-tA</ta>
            <ta e="T84" id="Seg_4264" s="T83">di͡e-BIT-LArA</ta>
            <ta e="T85" id="Seg_4265" s="T84">onton</ta>
            <ta e="T86" id="Seg_4266" s="T85">bettek</ta>
            <ta e="T87" id="Seg_4267" s="T86">uhuk-I-ttAn</ta>
            <ta e="T88" id="Seg_4268" s="T87">usku͡ola-GA</ta>
            <ta e="T89" id="Seg_4269" s="T88">ü͡ören-IAK-GAr</ta>
            <ta e="T90" id="Seg_4270" s="T89">di͡e-An</ta>
            <ta e="T91" id="Seg_4271" s="T90">tavarʼišʼ</ta>
            <ta e="T92" id="Seg_4272" s="T91">Lʼenʼin</ta>
            <ta e="T93" id="Seg_4273" s="T92">ehe</ta>
            <ta e="T94" id="Seg_4274" s="T93">di͡e-An</ta>
            <ta e="T95" id="Seg_4275" s="T94">e-TI-tA</ta>
            <ta e="T96" id="Seg_4276" s="T95">Lʼenʼin</ta>
            <ta e="T97" id="Seg_4277" s="T96">hoku͡on-tA</ta>
            <ta e="T98" id="Seg_4278" s="T97">dʼe</ta>
            <ta e="T99" id="Seg_4279" s="T98">ularɨj-An-ularɨj-An</ta>
            <ta e="T100" id="Seg_4280" s="T99">ol</ta>
            <ta e="T101" id="Seg_4281" s="T100">ularɨj-An</ta>
            <ta e="T102" id="Seg_4282" s="T101">is-TI-tA</ta>
            <ta e="T103" id="Seg_4283" s="T102">uhuk-I-ttAn</ta>
            <ta e="T104" id="Seg_4284" s="T103">kihi</ta>
            <ta e="T105" id="Seg_4285" s="T104">barɨ-tA</ta>
            <ta e="T106" id="Seg_4286" s="T105">kɨlgas</ta>
            <ta e="T107" id="Seg_4287" s="T106">kɨ͡aragas</ta>
            <ta e="T108" id="Seg_4288" s="T107">ogo-tA</ta>
            <ta e="T109" id="Seg_4289" s="T108">kaja</ta>
            <ta e="T110" id="Seg_4290" s="T109">da</ta>
            <ta e="T111" id="Seg_4291" s="T110">dʼon</ta>
            <ta e="T112" id="Seg_4292" s="T111">kannɨk</ta>
            <ta e="T113" id="Seg_4293" s="T112">daːganɨ</ta>
            <ta e="T114" id="Seg_4294" s="T113">kɨlgas</ta>
            <ta e="T115" id="Seg_4295" s="T114">kɨ͡aragas</ta>
            <ta e="T116" id="Seg_4296" s="T115">dʼadaŋɨ</ta>
            <ta e="T117" id="Seg_4297" s="T116">kihi</ta>
            <ta e="T118" id="Seg_4298" s="T117">ogo-tA</ta>
            <ta e="T119" id="Seg_4299" s="T118">barɨ-tA</ta>
            <ta e="T120" id="Seg_4300" s="T119">ü͡örek-GA</ta>
            <ta e="T121" id="Seg_4301" s="T120">da</ta>
            <ta e="T122" id="Seg_4302" s="T121">dʼe</ta>
            <ta e="T123" id="Seg_4303" s="T122">načʼinaj-LAː-TI-tA</ta>
            <ta e="T124" id="Seg_4304" s="T123">o</ta>
            <ta e="T125" id="Seg_4305" s="T124">hol</ta>
            <ta e="T126" id="Seg_4306" s="T125">ereːri</ta>
            <ta e="T127" id="Seg_4307" s="T126">baːj-ttAn</ta>
            <ta e="T128" id="Seg_4308" s="T127">ahaː-A</ta>
            <ta e="T129" id="Seg_4309" s="T128">ü͡ören-BIT-LAr</ta>
            <ta e="T130" id="Seg_4310" s="T129">kulaːk-ttAn</ta>
            <ta e="T131" id="Seg_4311" s="T130">abɨraː-A-n-A</ta>
            <ta e="T132" id="Seg_4312" s="T131">ü͡ören-BIT-LAr</ta>
            <ta e="T133" id="Seg_4313" s="T132">hin</ta>
            <ta e="T134" id="Seg_4314" s="T133">daːganɨ</ta>
            <ta e="T135" id="Seg_4315" s="T134">usku͡ola-GA</ta>
            <ta e="T136" id="Seg_4316" s="T135">bi͡er-IAK-BItI-n</ta>
            <ta e="T137" id="Seg_4317" s="T136">di͡e-BAT</ta>
            <ta e="T138" id="Seg_4318" s="T137">e-TI-LArA</ta>
            <ta e="T139" id="Seg_4319" s="T138">ogo-nI</ta>
            <ta e="T140" id="Seg_4320" s="T139">onton</ta>
            <ta e="T141" id="Seg_4321" s="T140">bu͡ol-BIT-tA</ta>
            <ta e="T142" id="Seg_4322" s="T141">kolxoz</ta>
            <ta e="T143" id="Seg_4323" s="T142">bu͡ol-IAK-GA</ta>
            <ta e="T144" id="Seg_4324" s="T143">di͡e-An</ta>
            <ta e="T145" id="Seg_4325" s="T144">obšʼij</ta>
            <ta e="T146" id="Seg_4326" s="T145">xazʼajstva-nAn</ta>
            <ta e="T147" id="Seg_4327" s="T146">dʼon</ta>
            <ta e="T148" id="Seg_4328" s="T147">olok-tI-n</ta>
            <ta e="T149" id="Seg_4329" s="T148">tutun-IAK-tI-n</ta>
            <ta e="T150" id="Seg_4330" s="T149">di͡e-An-LAr</ta>
            <ta e="T151" id="Seg_4331" s="T150">hu͡ok</ta>
            <ta e="T152" id="Seg_4332" s="T151">ol-GA</ta>
            <ta e="T153" id="Seg_4333" s="T152">emi͡e</ta>
            <ta e="T154" id="Seg_4334" s="T153">bu͡ol-A</ta>
            <ta e="T155" id="Seg_4335" s="T154">tart-BAtAK-LArA</ta>
            <ta e="T156" id="Seg_4336" s="T155">kas</ta>
            <ta e="T157" id="Seg_4337" s="T156">da</ta>
            <ta e="T158" id="Seg_4338" s="T157">dʼɨl-I-nAn</ta>
            <ta e="T159" id="Seg_4339" s="T158">bu͡ol</ta>
            <ta e="T160" id="Seg_4340" s="T159">kolxoz</ta>
            <ta e="T161" id="Seg_4341" s="T160">bu͡ol-BAtAK-LArA</ta>
            <ta e="T162" id="Seg_4342" s="T161">kolxoz</ta>
            <ta e="T163" id="Seg_4343" s="T162">bu͡ol-An-BIt</ta>
            <ta e="T164" id="Seg_4344" s="T163">bihigi</ta>
            <ta e="T165" id="Seg_4345" s="T164">hatan-IAK-BIt</ta>
            <ta e="T166" id="Seg_4346" s="T165">hu͡ok</ta>
            <ta e="T167" id="Seg_4347" s="T166">di͡e-Ar</ta>
            <ta e="T168" id="Seg_4348" s="T167">e-TI-LArA</ta>
            <ta e="T169" id="Seg_4349" s="T168">urut</ta>
            <ta e="T170" id="Seg_4350" s="T169">carskaj</ta>
            <ta e="T171" id="Seg_4351" s="T170">hoku͡on-GA</ta>
            <ta e="T172" id="Seg_4352" s="T171">ü͡öskeː-BIT</ta>
            <ta e="T173" id="Seg_4353" s="T172">dʼon</ta>
            <ta e="T174" id="Seg_4354" s="T173">ü͡ören-BIT</ta>
            <ta e="T175" id="Seg_4355" s="T174">dʼon</ta>
            <ta e="T176" id="Seg_4356" s="T175">onton</ta>
            <ta e="T177" id="Seg_4357" s="T176">kajdak</ta>
            <ta e="T178" id="Seg_4358" s="T177">ere</ta>
            <ta e="T180" id="Seg_4359" s="T178">gɨn-An</ta>
            <ta e="T181" id="Seg_4360" s="T180">kepseː-An</ta>
            <ta e="T182" id="Seg_4361" s="T181">kepseː-An-LAr</ta>
            <ta e="T183" id="Seg_4362" s="T182">hoku͡on</ta>
            <ta e="T184" id="Seg_4363" s="T183">kel-An-kel-An</ta>
            <ta e="T185" id="Seg_4364" s="T184">hubu</ta>
            <ta e="T186" id="Seg_4365" s="T185">dojdu-LAr-GA</ta>
            <ta e="T187" id="Seg_4366" s="T186">kel-An</ta>
            <ta e="T188" id="Seg_4367" s="T187">kel-An</ta>
            <ta e="T189" id="Seg_4368" s="T188">kolxoz</ta>
            <ta e="T190" id="Seg_4369" s="T189">bu͡ol-TI-BIt</ta>
            <ta e="T191" id="Seg_4370" s="T190">otut</ta>
            <ta e="T192" id="Seg_4371" s="T191">hette-Is-LAːK-I-ttAn</ta>
            <ta e="T193" id="Seg_4372" s="T192">otut</ta>
            <ta e="T194" id="Seg_4373" s="T193">agɨs-LAːK-GA</ta>
            <ta e="T198" id="Seg_4374" s="T196">kirdik</ta>
            <ta e="T199" id="Seg_4375" s="T198">dʼe</ta>
            <ta e="T200" id="Seg_4376" s="T199">kolxoz</ta>
            <ta e="T201" id="Seg_4377" s="T200">bu͡ol</ta>
            <ta e="T202" id="Seg_4378" s="T201">načʼinaj-LAː-An</ta>
            <ta e="T203" id="Seg_4379" s="T202">ustroj-LAː-An</ta>
            <ta e="T204" id="Seg_4380" s="T203">kaːl</ta>
            <ta e="T205" id="Seg_4381" s="T204">uhuk-I-ttAn</ta>
            <ta e="T206" id="Seg_4382" s="T205">oččogo</ta>
            <ta e="T207" id="Seg_4383" s="T206">kolxoz-nI</ta>
            <ta e="T208" id="Seg_4384" s="T207">töröː-A-n-BIT-BIt</ta>
            <ta e="T209" id="Seg_4385" s="T208">e-TI-tA</ta>
            <ta e="T210" id="Seg_4386" s="T209">bihigi</ta>
            <ta e="T211" id="Seg_4387" s="T210">kanna</ta>
            <ta e="T212" id="Seg_4388" s="T211">orduk</ta>
            <ta e="T213" id="Seg_4389" s="T212">taba-LAːK-ttAn</ta>
            <ta e="T214" id="Seg_4390" s="T213">ol-GA</ta>
            <ta e="T215" id="Seg_4391" s="T214">kolxoz-GA</ta>
            <ta e="T216" id="Seg_4392" s="T215">tut-BIT-BIt</ta>
            <ta e="T217" id="Seg_4393" s="T216">kanna</ta>
            <ta e="T218" id="Seg_4394" s="T217">paːs-LAːK-BItI-n</ta>
            <ta e="T219" id="Seg_4395" s="T218">h-onon</ta>
            <ta e="T220" id="Seg_4396" s="T219">beje-BIt</ta>
            <ta e="T221" id="Seg_4397" s="T220">paːs-BItI-nAn</ta>
            <ta e="T222" id="Seg_4398" s="T221">beje-BIt</ta>
            <ta e="T223" id="Seg_4399" s="T222">kapkaːn-BItI-nAn</ta>
            <ta e="T224" id="Seg_4400" s="T223">uhuk-I-ttAn</ta>
            <ta e="T225" id="Seg_4401" s="T224">kolxoz</ta>
            <ta e="T226" id="Seg_4402" s="T225">olok-tI-n</ta>
            <ta e="T227" id="Seg_4403" s="T226">tut-An-BIt</ta>
            <ta e="T228" id="Seg_4404" s="T227">anɨ</ta>
            <ta e="T229" id="Seg_4405" s="T228">bu</ta>
            <ta e="T230" id="Seg_4406" s="T229">kem-LAr-GA</ta>
            <ta e="T231" id="Seg_4407" s="T230">tij-TI-BIt</ta>
            <ta e="T232" id="Seg_4408" s="T231">dʼe</ta>
            <ta e="T233" id="Seg_4409" s="T232">dʼe</ta>
            <ta e="T234" id="Seg_4410" s="T233">onton</ta>
            <ta e="T235" id="Seg_4411" s="T234">kolxoz</ta>
            <ta e="T236" id="Seg_4412" s="T235">olok-tA</ta>
            <ta e="T237" id="Seg_4413" s="T236">üčügej</ta>
            <ta e="T238" id="Seg_4414" s="T237">du͡o</ta>
            <ta e="T239" id="Seg_4415" s="T238">di͡e-An</ta>
            <ta e="T240" id="Seg_4416" s="T239">ol</ta>
            <ta e="T241" id="Seg_4417" s="T240">kördük</ta>
            <ta e="T242" id="Seg_4418" s="T241">bu͡olla</ta>
            <ta e="T243" id="Seg_4419" s="T242">bu͡ol-BIT-I-nAn</ta>
            <ta e="T244" id="Seg_4420" s="T243">dʼe</ta>
            <ta e="T245" id="Seg_4421" s="T244">kolxoz</ta>
            <ta e="T246" id="Seg_4422" s="T245">bu͡ol-BIT-BIt</ta>
            <ta e="T247" id="Seg_4423" s="T246">ol</ta>
            <ta e="T248" id="Seg_4424" s="T247">betereː</ta>
            <ta e="T249" id="Seg_4425" s="T248">örüt-tI-GAr</ta>
            <ta e="T250" id="Seg_4426" s="T249">emi͡e</ta>
            <ta e="T251" id="Seg_4427" s="T250">daːganɨ</ta>
            <ta e="T252" id="Seg_4428" s="T251">erej-LAːK</ta>
            <ta e="T253" id="Seg_4429" s="T252">kɨhalga-LAːK</ta>
            <ta e="T254" id="Seg_4430" s="T253">üje-LAr</ta>
            <ta e="T255" id="Seg_4431" s="T254">bu͡ol-Ar</ta>
            <ta e="T256" id="Seg_4432" s="T255">e-TI-LArA</ta>
            <ta e="T257" id="Seg_4433" s="T256">bu</ta>
            <ta e="T258" id="Seg_4434" s="T257">kulaːk-GA</ta>
            <ta e="T259" id="Seg_4435" s="T258">urukku</ta>
            <ta e="T260" id="Seg_4436" s="T259">carskaj-GA</ta>
            <ta e="T261" id="Seg_4437" s="T260">bas-LAr-tI-n</ta>
            <ta e="T262" id="Seg_4438" s="T261">ɨs-TAr-BIT</ta>
            <ta e="T263" id="Seg_4439" s="T262">kihi-LAr</ta>
            <ta e="T265" id="Seg_4440" s="T264">bandiːt-LAː-Ar</ta>
            <ta e="T266" id="Seg_4441" s="T265">da</ta>
            <ta e="T267" id="Seg_4442" s="T266">e-TI-LArA</ta>
            <ta e="T268" id="Seg_4443" s="T267">ol-LAr</ta>
            <ta e="T269" id="Seg_4444" s="T268">hin</ta>
            <ta e="T270" id="Seg_4445" s="T269">erej-nI</ta>
            <ta e="T271" id="Seg_4446" s="T270">oŋor-AːččI</ta>
            <ta e="T272" id="Seg_4447" s="T271">e-TI-LAr</ta>
            <ta e="T273" id="Seg_4448" s="T272">ol</ta>
            <ta e="T274" id="Seg_4449" s="T273">bandiːt-nI</ta>
            <ta e="T275" id="Seg_4450" s="T274">hu͡ok</ta>
            <ta e="T276" id="Seg_4451" s="T275">ol</ta>
            <ta e="T277" id="Seg_4452" s="T276">aːt-nI</ta>
            <ta e="T278" id="Seg_4453" s="T277">habi͡eskaj</ta>
            <ta e="T279" id="Seg_4454" s="T278">bɨlaːs</ta>
            <ta e="T280" id="Seg_4455" s="T279">ilin</ta>
            <ta e="T281" id="Seg_4456" s="T280">dek</ta>
            <ta e="T282" id="Seg_4457" s="T281">kaːm-An</ta>
            <ta e="T283" id="Seg_4458" s="T282">is-An</ta>
            <ta e="T284" id="Seg_4459" s="T283">ol</ta>
            <ta e="T285" id="Seg_4460" s="T284">bandiːt-nI</ta>
            <ta e="T286" id="Seg_4461" s="T285">hin</ta>
            <ta e="T287" id="Seg_4462" s="T286">daːganɨ</ta>
            <ta e="T288" id="Seg_4463" s="T287">ɨraːstaː-AːččI</ta>
            <ta e="T289" id="Seg_4464" s="T288">e-TI-tA</ta>
            <ta e="T290" id="Seg_4465" s="T289">dʼahaj-AːččI</ta>
            <ta e="T291" id="Seg_4466" s="T290">da</ta>
            <ta e="T292" id="Seg_4467" s="T291">e-TI-tA</ta>
            <ta e="T293" id="Seg_4468" s="T292">dʼe</ta>
            <ta e="T294" id="Seg_4469" s="T293">ol</ta>
            <ta e="T295" id="Seg_4470" s="T294">büt-BIT-tI-n</ta>
            <ta e="T296" id="Seg_4471" s="T295">genne</ta>
            <ta e="T297" id="Seg_4472" s="T296">uhuk-I-ttAn</ta>
            <ta e="T298" id="Seg_4473" s="T297">bu</ta>
            <ta e="T299" id="Seg_4474" s="T298">Gʼermanʼija</ta>
            <ta e="T300" id="Seg_4475" s="T299">heriː-tA</ta>
            <ta e="T301" id="Seg_4476" s="T300">bu͡ol-BIT-tA</ta>
            <ta e="T302" id="Seg_4477" s="T301">dʼe</ta>
            <ta e="T303" id="Seg_4478" s="T302">iti-GA</ta>
            <ta e="T304" id="Seg_4479" s="T303">kɨhalɨn-A</ta>
            <ta e="T305" id="Seg_4480" s="T304">hɨs-BIT-BIt</ta>
            <ta e="T306" id="Seg_4481" s="T305">bihigi</ta>
            <ta e="T307" id="Seg_4482" s="T306">hürdeːk</ta>
            <ta e="T308" id="Seg_4483" s="T307">ol</ta>
            <ta e="T309" id="Seg_4484" s="T308">taŋas</ta>
            <ta e="T310" id="Seg_4485" s="T309">da</ta>
            <ta e="T311" id="Seg_4486" s="T310">terij-Iː-tA</ta>
            <ta e="T312" id="Seg_4487" s="T311">bu͡ol-Ar</ta>
            <ta e="T313" id="Seg_4488" s="T312">e-TI-tA</ta>
            <ta e="T314" id="Seg_4489" s="T313">ol</ta>
            <ta e="T315" id="Seg_4490" s="T314">karčɨ-nAn</ta>
            <ta e="T316" id="Seg_4491" s="T315">da</ta>
            <ta e="T317" id="Seg_4492" s="T316">nolu͡og</ta>
            <ta e="T318" id="Seg_4493" s="T317">bu͡ol-Ar</ta>
            <ta e="T319" id="Seg_4494" s="T318">e-TI-tA</ta>
            <ta e="T320" id="Seg_4495" s="T319">bihigi</ta>
            <ta e="T321" id="Seg_4496" s="T320">ol</ta>
            <ta e="T322" id="Seg_4497" s="T321">dek</ta>
            <ta e="T323" id="Seg_4498" s="T322">gu͡orat</ta>
            <ta e="T324" id="Seg_4499" s="T323">hir-LAr-GA</ta>
            <ta e="T325" id="Seg_4500" s="T324">armʼija-GA</ta>
            <ta e="T327" id="Seg_4501" s="T325">kömölös-An-BIt</ta>
            <ta e="T328" id="Seg_4502" s="T327">onton</ta>
            <ta e="T329" id="Seg_4503" s="T328">dʼɨl</ta>
            <ta e="T330" id="Seg_4504" s="T329">büt-An</ta>
            <ta e="T331" id="Seg_4505" s="T330">dʼe</ta>
            <ta e="T332" id="Seg_4506" s="T331">anɨ</ta>
            <ta e="T333" id="Seg_4507" s="T332">tu͡ok</ta>
            <ta e="T334" id="Seg_4508" s="T333">da</ta>
            <ta e="T335" id="Seg_4509" s="T334">bu͡ol-IAK-BIt</ta>
            <ta e="T336" id="Seg_4510" s="T335">di͡e-BAT-BIn</ta>
            <ta e="T337" id="Seg_4511" s="T336">bügüŋŋü</ta>
            <ta e="T338" id="Seg_4512" s="T337">hanaː-m</ta>
            <ta e="T339" id="Seg_4513" s="T338">üčügej</ta>
            <ta e="T340" id="Seg_4514" s="T339">anɨ</ta>
            <ta e="T341" id="Seg_4515" s="T340">olok</ta>
            <ta e="T342" id="Seg_4516" s="T341">kön-TI-tA</ta>
            <ta e="T343" id="Seg_4517" s="T342">olok</ta>
            <ta e="T344" id="Seg_4518" s="T343">ulaːt-TI-tA</ta>
            <ta e="T345" id="Seg_4519" s="T344">anɨ</ta>
            <ta e="T346" id="Seg_4520" s="T345">bütün</ta>
            <ta e="T347" id="Seg_4521" s="T346">kolxoz-LAr</ta>
            <ta e="T348" id="Seg_4522" s="T347">uhuk-I-ttAn</ta>
            <ta e="T349" id="Seg_4523" s="T348">argijaːsija</ta>
            <ta e="T350" id="Seg_4524" s="T349">kördük</ta>
            <ta e="T351" id="Seg_4525" s="T350">bu͡ol-TI-LAr</ta>
            <ta e="T352" id="Seg_4526" s="T351">anɨ</ta>
            <ta e="T353" id="Seg_4527" s="T352">mantan</ta>
            <ta e="T354" id="Seg_4528" s="T353">ilin</ta>
            <ta e="T355" id="Seg_4529" s="T354">di͡egi-tI-n</ta>
            <ta e="T356" id="Seg_4530" s="T355">bil-BAT-BIn</ta>
            <ta e="T357" id="Seg_4531" s="T356">beje-m</ta>
            <ta e="T358" id="Seg_4532" s="T357">kɨrɨj-An</ta>
            <ta e="T359" id="Seg_4533" s="T358">kaːl-TI-m</ta>
            <ta e="T362" id="Seg_4534" s="T361">min</ta>
            <ta e="T363" id="Seg_4535" s="T362">hanaː-BA-r</ta>
            <ta e="T364" id="Seg_4536" s="T363">üčügej</ta>
            <ta e="T365" id="Seg_4537" s="T364">olok-GA</ta>
            <ta e="T366" id="Seg_4538" s="T365">anɨ</ta>
            <ta e="T367" id="Seg_4539" s="T366">tij-TI-BIt</ta>
            <ta e="T368" id="Seg_4540" s="T367">di͡e-A-BIn</ta>
            <ta e="T369" id="Seg_4541" s="T368">hubu</ta>
            <ta e="T370" id="Seg_4542" s="T369">kɨrɨj-Ar</ta>
            <ta e="T372" id="Seg_4543" s="T370">haːs-BA-r</ta>
            <ta e="T373" id="Seg_4544" s="T372">anɨ</ta>
            <ta e="T374" id="Seg_4545" s="T373">tot</ta>
            <ta e="T375" id="Seg_4546" s="T374">bagajɨ-BIt</ta>
            <ta e="T376" id="Seg_4547" s="T375">ičiges</ta>
            <ta e="T377" id="Seg_4548" s="T376">bagajɨ-BIt</ta>
            <ta e="T378" id="Seg_4549" s="T377">taŋas-nI</ta>
            <ta e="T379" id="Seg_4550" s="T378">da</ta>
            <ta e="T380" id="Seg_4551" s="T379">taŋɨn-A-BIt</ta>
            <ta e="T381" id="Seg_4552" s="T380">as-nI</ta>
            <ta e="T382" id="Seg_4553" s="T381">da</ta>
            <ta e="T383" id="Seg_4554" s="T382">ahaː-A-BIt</ta>
            <ta e="T384" id="Seg_4555" s="T383">bu</ta>
            <ta e="T385" id="Seg_4556" s="T384">ka</ta>
            <ta e="T386" id="Seg_4557" s="T385">maŋnaj</ta>
            <ta e="T387" id="Seg_4558" s="T386">mini͡ene</ta>
            <ta e="T388" id="Seg_4559" s="T387">bu</ta>
            <ta e="T389" id="Seg_4560" s="T388">ulakan</ta>
            <ta e="T390" id="Seg_4561" s="T389">ogo-m</ta>
            <ta e="T391" id="Seg_4562" s="T390">bu</ta>
            <ta e="T392" id="Seg_4563" s="T391">Bahiːlaj</ta>
            <ta e="T393" id="Seg_4564" s="T392">baːr</ta>
            <ta e="T394" id="Seg_4565" s="T393">bu</ta>
            <ta e="T395" id="Seg_4566" s="T394">ogo-m</ta>
            <ta e="T396" id="Seg_4567" s="T395">ulaːt-TI-tA</ta>
            <ta e="T397" id="Seg_4568" s="T396">da</ta>
            <ta e="T398" id="Seg_4569" s="T397">ü͡ören-BIT-tA</ta>
            <ta e="T399" id="Seg_4570" s="T398">gini</ta>
            <ta e="T400" id="Seg_4571" s="T399">usku͡ola-tI-n</ta>
            <ta e="T401" id="Seg_4572" s="T400">büt-A-r-TI-tA</ta>
            <ta e="T402" id="Seg_4573" s="T401">daːganɨ</ta>
            <ta e="T403" id="Seg_4574" s="T402">učuːtal-GA</ta>
            <ta e="T404" id="Seg_4575" s="T403">ü͡ören-BIT-tA</ta>
            <ta e="T405" id="Seg_4576" s="T404">onon</ta>
            <ta e="T406" id="Seg_4577" s="T405">anɨ</ta>
            <ta e="T407" id="Seg_4578" s="T406">kün</ta>
            <ta e="T408" id="Seg_4579" s="T407">bügün</ta>
            <ta e="T409" id="Seg_4580" s="T408">učuːtal-LAː-A</ta>
            <ta e="T410" id="Seg_4581" s="T409">olor-Ar</ta>
            <ta e="T411" id="Seg_4582" s="T410">onton</ta>
            <ta e="T412" id="Seg_4583" s="T411">biːr</ta>
            <ta e="T413" id="Seg_4584" s="T412">kɨːs-LAːK-BIn</ta>
            <ta e="T414" id="Seg_4585" s="T413">ol</ta>
            <ta e="T415" id="Seg_4586" s="T414">emi͡e</ta>
            <ta e="T416" id="Seg_4587" s="T415">ü͡örek-tI-n</ta>
            <ta e="T417" id="Seg_4588" s="T416">usku͡ola-tI-n</ta>
            <ta e="T418" id="Seg_4589" s="T417">büt-A-r-An</ta>
            <ta e="T419" id="Seg_4590" s="T418">emi͡e</ta>
            <ta e="T420" id="Seg_4591" s="T419">bɨssaj</ta>
            <ta e="T421" id="Seg_4592" s="T420">ü͡ören-An</ta>
            <ta e="T422" id="Seg_4593" s="T421">du͡oktuːr-LAː-An</ta>
            <ta e="T423" id="Seg_4594" s="T422">olor-Ar</ta>
            <ta e="T424" id="Seg_4595" s="T423">anɨ</ta>
            <ta e="T425" id="Seg_4596" s="T424">ol</ta>
            <ta e="T426" id="Seg_4597" s="T425">beje-tI-n</ta>
            <ta e="T427" id="Seg_4598" s="T426">ilin-tI-GAr</ta>
            <ta e="T428" id="Seg_4599" s="T427">üčügej</ta>
            <ta e="T429" id="Seg_4600" s="T428">ol</ta>
            <ta e="T430" id="Seg_4601" s="T429">genne</ta>
            <ta e="T431" id="Seg_4602" s="T430">min</ta>
            <ta e="T432" id="Seg_4603" s="T431">da</ta>
            <ta e="T433" id="Seg_4604" s="T432">hanaː-BA-r</ta>
            <ta e="T434" id="Seg_4605" s="T433">üčügej</ta>
            <ta e="T435" id="Seg_4606" s="T434">anɨ</ta>
            <ta e="T436" id="Seg_4607" s="T435">bu</ta>
            <ta e="T437" id="Seg_4608" s="T436">olok-GA</ta>
            <ta e="T438" id="Seg_4609" s="T437">olor-Iː-LArA</ta>
            <ta e="T439" id="Seg_4610" s="T438">anɨ</ta>
            <ta e="T440" id="Seg_4611" s="T439">bu</ta>
            <ta e="T441" id="Seg_4612" s="T440">ogo-LAr-I-m</ta>
            <ta e="T442" id="Seg_4613" s="T441">ol-GA</ta>
            <ta e="T443" id="Seg_4614" s="T442">üs-Is</ta>
            <ta e="T444" id="Seg_4615" s="T443">emi͡e</ta>
            <ta e="T445" id="Seg_4616" s="T444">u͡ol-I-m</ta>
            <ta e="T446" id="Seg_4617" s="T445">avʼijport-GA</ta>
            <ta e="T447" id="Seg_4618" s="T446">üleleː-Ar</ta>
            <ta e="T448" id="Seg_4619" s="T447">onton</ta>
            <ta e="T449" id="Seg_4620" s="T448">muŋ</ta>
            <ta e="T450" id="Seg_4621" s="T449">küččügüj</ta>
            <ta e="T451" id="Seg_4622" s="T450">u͡ol-LAːK-BIn</ta>
            <ta e="T452" id="Seg_4623" s="T451">anɨ</ta>
            <ta e="T453" id="Seg_4624" s="T452">Ujbaːn</ta>
            <ta e="T454" id="Seg_4625" s="T453">Antonаv</ta>
            <ta e="T455" id="Seg_4626" s="T454">Ujbaːn</ta>
            <ta e="T458" id="Seg_4627" s="T457">ol</ta>
            <ta e="T459" id="Seg_4628" s="T458">du͡o</ta>
            <ta e="T460" id="Seg_4629" s="T459">anɨ</ta>
            <ta e="T461" id="Seg_4630" s="T460">kolxoz-GA</ta>
            <ta e="T462" id="Seg_4631" s="T461">üleleː-Ar</ta>
            <ta e="T463" id="Seg_4632" s="T462">kolxoz</ta>
            <ta e="T464" id="Seg_4633" s="T463">daːganɨ</ta>
            <ta e="T465" id="Seg_4634" s="T464">üleleː-Ar-tA</ta>
            <ta e="T466" id="Seg_4635" s="T465">üle-tA</ta>
            <ta e="T467" id="Seg_4636" s="T466">üle-tI-n</ta>
            <ta e="T468" id="Seg_4637" s="T467">da</ta>
            <ta e="T469" id="Seg_4638" s="T468">üle-LAːK</ta>
            <ta e="T470" id="Seg_4639" s="T469">kolxoz</ta>
            <ta e="T471" id="Seg_4640" s="T470">hin</ta>
            <ta e="T472" id="Seg_4641" s="T471">daːganɨ</ta>
            <ta e="T473" id="Seg_4642" s="T472">beje-tI-n</ta>
            <ta e="T474" id="Seg_4643" s="T473">kolxoz</ta>
            <ta e="T475" id="Seg_4644" s="T474">is-tI-GAr</ta>
            <ta e="T476" id="Seg_4645" s="T475">hin</ta>
            <ta e="T477" id="Seg_4646" s="T476">üčügej-LIk</ta>
            <ta e="T478" id="Seg_4647" s="T477">tot-A-r-An</ta>
            <ta e="T479" id="Seg_4648" s="T478">taŋɨn-TAr-An</ta>
            <ta e="T480" id="Seg_4649" s="T479">hin</ta>
            <ta e="T481" id="Seg_4650" s="T480">da</ta>
            <ta e="T482" id="Seg_4651" s="T481">üleleː-A-t-Ar</ta>
            <ta e="T483" id="Seg_4652" s="T482">hin</ta>
            <ta e="T484" id="Seg_4653" s="T483">da</ta>
            <ta e="T485" id="Seg_4654" s="T484">harpalaːt-LAːK</ta>
            <ta e="T486" id="Seg_4655" s="T485">kihi</ta>
            <ta e="T487" id="Seg_4656" s="T486">kanna</ta>
            <ta e="T488" id="Seg_4657" s="T487">üčügej-LIk</ta>
            <ta e="T489" id="Seg_4658" s="T488">üleleː-Ar-tA</ta>
            <ta e="T490" id="Seg_4659" s="T489">ol</ta>
            <ta e="T491" id="Seg_4660" s="T490">orduk</ta>
            <ta e="T492" id="Seg_4661" s="T491">bu͡ol-Ar</ta>
            <ta e="T493" id="Seg_4662" s="T492">kanna</ta>
            <ta e="T494" id="Seg_4663" s="T493">kuhagan-LIk</ta>
            <ta e="T495" id="Seg_4664" s="T494">üleleː-Ar-tA</ta>
            <ta e="T496" id="Seg_4665" s="T495">ol</ta>
            <ta e="T497" id="Seg_4666" s="T496">niːze</ta>
            <ta e="T498" id="Seg_4667" s="T497">bu͡ol-Ar</ta>
            <ta e="T499" id="Seg_4668" s="T498">min</ta>
            <ta e="T500" id="Seg_4669" s="T499">hanaː-BA-r</ta>
            <ta e="T501" id="Seg_4670" s="T500">h-itigirdik</ta>
            <ta e="T502" id="Seg_4671" s="T501">bu</ta>
            <ta e="T503" id="Seg_4672" s="T502">ka</ta>
            <ta e="T504" id="Seg_4673" s="T503">maŋnaj</ta>
            <ta e="T505" id="Seg_4674" s="T504">beje-m</ta>
            <ta e="T506" id="Seg_4675" s="T505">hin</ta>
            <ta e="T507" id="Seg_4676" s="T506">daːganɨ</ta>
            <ta e="T508" id="Seg_4677" s="T507">bu</ta>
            <ta e="T509" id="Seg_4678" s="T508">sʼelʼsavʼet-I-nAn</ta>
            <ta e="T510" id="Seg_4679" s="T509">tu͡ok-I-nAn</ta>
            <ta e="T511" id="Seg_4680" s="T510">rukavostvava-LAː-An</ta>
            <ta e="T512" id="Seg_4681" s="T511">olor-Ar</ta>
            <ta e="T513" id="Seg_4682" s="T512">e-TI-m</ta>
            <ta e="T514" id="Seg_4683" s="T513">onton</ta>
            <ta e="T515" id="Seg_4684" s="T514">anɨ</ta>
            <ta e="T516" id="Seg_4685" s="T515">ogo</ta>
            <ta e="T517" id="Seg_4686" s="T516">kihi-LAr</ta>
            <ta e="T518" id="Seg_4687" s="T517">ü͡örek-GA</ta>
            <ta e="T519" id="Seg_4688" s="T518">ü͡ören-An-LAr</ta>
            <ta e="T520" id="Seg_4689" s="T519">min-TAːgAr</ta>
            <ta e="T521" id="Seg_4690" s="T520">luːčča</ta>
            <ta e="T522" id="Seg_4691" s="T521">üleleː-IːhI-LAr</ta>
            <ta e="T523" id="Seg_4692" s="T522">oččogo</ta>
            <ta e="T524" id="Seg_4693" s="T523">du͡o</ta>
            <ta e="T525" id="Seg_4694" s="T524">kim-GA</ta>
            <ta e="T526" id="Seg_4695" s="T525">daːganɨ</ta>
            <ta e="T527" id="Seg_4696" s="T526">buruj</ta>
            <ta e="T528" id="Seg_4697" s="T527">haŋa-tA</ta>
            <ta e="T529" id="Seg_4698" s="T528">hu͡ok</ta>
            <ta e="T530" id="Seg_4699" s="T529">anɨ</ta>
            <ta e="T531" id="Seg_4700" s="T530">hɨnnʼan-IAK-m</ta>
            <ta e="T532" id="Seg_4701" s="T531">di͡e-An-BIn</ta>
            <ta e="T533" id="Seg_4702" s="T532">sʼelʼsavʼet-LAː-An</ta>
            <ta e="T534" id="Seg_4703" s="T533">büt-BIT-I-m</ta>
            <ta e="T535" id="Seg_4704" s="T534">e-TI-tA</ta>
            <ta e="T536" id="Seg_4705" s="T535">anɨ</ta>
            <ta e="T537" id="Seg_4706" s="T536">bu</ta>
            <ta e="T538" id="Seg_4707" s="T537">ogo-LAr-I-m</ta>
            <ta e="T539" id="Seg_4708" s="T538">ü͡örek-GA</ta>
            <ta e="T540" id="Seg_4709" s="T539">ü͡ören-An-LAr</ta>
            <ta e="T541" id="Seg_4710" s="T540">üčügej-LIk</ta>
            <ta e="T544" id="Seg_4711" s="T543">olor-Ar-LAr</ta>
            <ta e="T545" id="Seg_4712" s="T544">onton</ta>
            <ta e="T546" id="Seg_4713" s="T545">anɨ</ta>
            <ta e="T547" id="Seg_4714" s="T546">ü͡ör-A-n-A</ta>
            <ta e="T548" id="Seg_4715" s="T547">olor-A-BIn</ta>
            <ta e="T549" id="Seg_4716" s="T548">bu</ta>
            <ta e="T550" id="Seg_4717" s="T549">kɨrɨj-An</ta>
            <ta e="T551" id="Seg_4718" s="T550">bar-An-BIn</ta>
            <ta e="T552" id="Seg_4719" s="T551">beje-m</ta>
            <ta e="T553" id="Seg_4720" s="T552">ü͡örek-tA</ta>
            <ta e="T554" id="Seg_4721" s="T553">hu͡ok</ta>
            <ta e="T555" id="Seg_4722" s="T554">bagajɨ</ta>
            <ta e="T556" id="Seg_4723" s="T555">e-TI-m</ta>
            <ta e="T557" id="Seg_4724" s="T556">ol</ta>
            <ta e="T558" id="Seg_4725" s="T557">genne</ta>
            <ta e="T559" id="Seg_4726" s="T558">daːse</ta>
            <ta e="T560" id="Seg_4727" s="T559">min</ta>
            <ta e="T561" id="Seg_4728" s="T560">tɨl-nI</ta>
            <ta e="T562" id="Seg_4729" s="T561">da</ta>
            <ta e="T563" id="Seg_4730" s="T562">kɨ͡aj-An</ta>
            <ta e="T564" id="Seg_4731" s="T563">ihit-I-BAT-BIn</ta>
            <ta e="T565" id="Seg_4732" s="T564">e-TI-m</ta>
            <ta e="T566" id="Seg_4733" s="T565">kaččaga</ta>
            <ta e="T567" id="Seg_4734" s="T566">ol</ta>
            <ta e="T568" id="Seg_4735" s="T567">min</ta>
            <ta e="T569" id="Seg_4736" s="T568">sʼelʼsavʼet</ta>
            <ta e="T570" id="Seg_4737" s="T569">er-TAK-BInA</ta>
            <ta e="T571" id="Seg_4738" s="T570">oː</ta>
            <ta e="T572" id="Seg_4739" s="T571">tu͡ok</ta>
            <ta e="T573" id="Seg_4740" s="T572">eme</ta>
            <ta e="T574" id="Seg_4741" s="T573">pʼerʼevoːččik-I-nAn</ta>
            <ta e="T575" id="Seg_4742" s="T574">biːr</ta>
            <ta e="T576" id="Seg_4743" s="T575">eme</ta>
            <ta e="T577" id="Seg_4744" s="T576">tɨl-kAːN-nI</ta>
            <ta e="T578" id="Seg_4745" s="T577">kahu͡on</ta>
            <ta e="T579" id="Seg_4746" s="T578">kihi</ta>
            <ta e="T580" id="Seg_4747" s="T579">haŋa-tI-nAn</ta>
            <ta e="T581" id="Seg_4748" s="T580">ergit-AːččI</ta>
            <ta e="T582" id="Seg_4749" s="T581">e-TI-BIt</ta>
            <ta e="T583" id="Seg_4750" s="T582">pʼerʼevoːččik-I-nAn</ta>
            <ta e="T584" id="Seg_4751" s="T583">ol</ta>
            <ta e="T585" id="Seg_4752" s="T584">erej-LAːK</ta>
            <ta e="T586" id="Seg_4753" s="T585">e-TI-tA</ta>
            <ta e="T587" id="Seg_4754" s="T586">anɨ</ta>
            <ta e="T588" id="Seg_4755" s="T587">erej-tA</ta>
            <ta e="T589" id="Seg_4756" s="T588">hu͡ok</ta>
            <ta e="T590" id="Seg_4757" s="T589">beje-LArA</ta>
            <ta e="T591" id="Seg_4758" s="T590">hɨraj</ta>
            <ta e="T592" id="Seg_4759" s="T591">kepset-Ar-LAr</ta>
            <ta e="T593" id="Seg_4760" s="T592">kim-nI</ta>
            <ta e="T594" id="Seg_4761" s="T593">da</ta>
            <ta e="T595" id="Seg_4762" s="T594">kɨtta</ta>
            <ta e="T596" id="Seg_4763" s="T595">habi͡eskaj</ta>
            <ta e="T597" id="Seg_4764" s="T596">bɨlaːs</ta>
            <ta e="T598" id="Seg_4765" s="T597">üčügej</ta>
            <ta e="T599" id="Seg_4766" s="T598">bert</ta>
            <ta e="T600" id="Seg_4767" s="T599">olok-GA</ta>
            <ta e="T601" id="Seg_4768" s="T600">tiri͡er-TI-tA</ta>
            <ta e="T602" id="Seg_4769" s="T601">anɨ</ta>
            <ta e="T603" id="Seg_4770" s="T602">habi͡eskaj</ta>
            <ta e="T604" id="Seg_4771" s="T603">bɨlaːs</ta>
            <ta e="T605" id="Seg_4772" s="T604">bu͡ol-An</ta>
            <ta e="T606" id="Seg_4773" s="T605">ogo-LAr-BA-r</ta>
            <ta e="T607" id="Seg_4774" s="T606">daːganɨ</ta>
            <ta e="T608" id="Seg_4775" s="T607">min</ta>
            <ta e="T609" id="Seg_4776" s="T608">daːganɨ</ta>
            <ta e="T610" id="Seg_4777" s="T609">bačča-BA-r</ta>
            <ta e="T611" id="Seg_4778" s="T610">tij-TI-m</ta>
            <ta e="T612" id="Seg_4779" s="T611">anɨ</ta>
            <ta e="T613" id="Seg_4780" s="T612">bert</ta>
            <ta e="T614" id="Seg_4781" s="T613">üčügej-LIk</ta>
            <ta e="T615" id="Seg_4782" s="T614">bačča</ta>
            <ta e="T616" id="Seg_4783" s="T615">bačča</ta>
            <ta e="T617" id="Seg_4784" s="T616">kɨrɨj-IAK-BA-r</ta>
            <ta e="T618" id="Seg_4785" s="T617">tij-TI-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_4786" s="T0">Soviet</ta>
            <ta e="T2" id="Seg_4787" s="T1">power.[NOM]</ta>
            <ta e="T3" id="Seg_4788" s="T2">become-PTCP.PRS-3SG-ACC</ta>
            <ta e="T4" id="Seg_4789" s="T3">EMPH</ta>
            <ta e="T5" id="Seg_4790" s="T4">still</ta>
            <ta e="T6" id="Seg_4791" s="T5">EMPH</ta>
            <ta e="T7" id="Seg_4792" s="T6">remember-NEG-1SG</ta>
            <ta e="T8" id="Seg_4793" s="T7">then</ta>
            <ta e="T9" id="Seg_4794" s="T8">1SG.[NOM]</ta>
            <ta e="T10" id="Seg_4795" s="T9">EMPH</ta>
            <ta e="T11" id="Seg_4796" s="T10">completely</ta>
            <ta e="T12" id="Seg_4797" s="T11">child.[NOM]</ta>
            <ta e="T13" id="Seg_4798" s="T12">be-PST1-1SG</ta>
            <ta e="T14" id="Seg_4799" s="T13">what.kind.of</ta>
            <ta e="T15" id="Seg_4800" s="T14">NEG</ta>
            <ta e="T16" id="Seg_4801" s="T15">power-DAT/LOC</ta>
            <ta e="T17" id="Seg_4802" s="T16">work-NEG.PTCP</ta>
            <ta e="T18" id="Seg_4803" s="T17">child.[NOM]</ta>
            <ta e="T19" id="Seg_4804" s="T18">be-PST1-1SG</ta>
            <ta e="T20" id="Seg_4805" s="T19">Soviet</ta>
            <ta e="T21" id="Seg_4806" s="T20">power.[NOM]</ta>
            <ta e="T22" id="Seg_4807" s="T21">become-PRS.[3SG]</ta>
            <ta e="T23" id="Seg_4808" s="T22">say-CVB.SEQ</ta>
            <ta e="T26" id="Seg_4809" s="T25">law.[NOM]</ta>
            <ta e="T27" id="Seg_4810" s="T26">turn-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T30" id="Seg_4811" s="T29">however</ta>
            <ta e="T31" id="Seg_4812" s="T30">EMPH</ta>
            <ta e="T32" id="Seg_4813" s="T31">understand-PTCP.PRS</ta>
            <ta e="T33" id="Seg_4814" s="T32">human.being.[NOM]</ta>
            <ta e="T34" id="Seg_4815" s="T33">understand-PTCP.PRS.[NOM]</ta>
            <ta e="T35" id="Seg_4816" s="T34">similar</ta>
            <ta e="T36" id="Seg_4817" s="T35">be-PST1-3SG</ta>
            <ta e="T37" id="Seg_4818" s="T36">law.[NOM]</ta>
            <ta e="T38" id="Seg_4819" s="T37">turn-NMNZ-3SG-ACC</ta>
            <ta e="T39" id="Seg_4820" s="T38">when</ta>
            <ta e="T40" id="Seg_4821" s="T39">at.first</ta>
            <ta e="T41" id="Seg_4822" s="T40">human.being-ACC</ta>
            <ta e="T42" id="Seg_4823" s="T41">before</ta>
            <ta e="T43" id="Seg_4824" s="T42">rich</ta>
            <ta e="T44" id="Seg_4825" s="T43">human.being.[NOM]</ta>
            <ta e="T45" id="Seg_4826" s="T44">kulak.[NOM]</ta>
            <ta e="T46" id="Seg_4827" s="T45">human.being.[NOM]</ta>
            <ta e="T47" id="Seg_4828" s="T46">follow-ITER-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_4829" s="T47">short-ACC</ta>
            <ta e="T49" id="Seg_4830" s="T48">narrow-ACC</ta>
            <ta e="T50" id="Seg_4831" s="T49">poor-ACC</ta>
            <ta e="T51" id="Seg_4832" s="T50">say-CVB.SEQ</ta>
            <ta e="T52" id="Seg_4833" s="T51">name-VBZ-PTCP.PRS</ta>
            <ta e="T53" id="Seg_4834" s="T52">be-PST1-3PL</ta>
            <ta e="T54" id="Seg_4835" s="T53">then</ta>
            <ta e="T55" id="Seg_4836" s="T54">MOD</ta>
            <ta e="T56" id="Seg_4837" s="T55">1SG.[NOM]</ta>
            <ta e="T57" id="Seg_4838" s="T56">this</ta>
            <ta e="T58" id="Seg_4839" s="T57">rich</ta>
            <ta e="T59" id="Seg_4840" s="T58">human.being-PL-DAT/LOC</ta>
            <ta e="T60" id="Seg_4841" s="T59">wood-AG-DIM.[NOM]</ta>
            <ta e="T61" id="Seg_4842" s="T60">be-PST1-1SG</ta>
            <ta e="T64" id="Seg_4843" s="T63">reindeer-AG.[NOM]</ta>
            <ta e="T65" id="Seg_4844" s="T64">be-PST1-1SG</ta>
            <ta e="T66" id="Seg_4845" s="T65">that</ta>
            <ta e="T67" id="Seg_4846" s="T66">that.side.[NOM]</ta>
            <ta e="T68" id="Seg_4847" s="T67">side-3SG-DAT/LOC</ta>
            <ta e="T69" id="Seg_4848" s="T68">then</ta>
            <ta e="T70" id="Seg_4849" s="T69">understand-CVB.SEQ</ta>
            <ta e="T71" id="Seg_4850" s="T70">go-PST1-1SG</ta>
            <ta e="T72" id="Seg_4851" s="T71">get.to.know-CVB.SEQ</ta>
            <ta e="T73" id="Seg_4852" s="T72">go-PST1-1SG</ta>
            <ta e="T74" id="Seg_4853" s="T73">of.course</ta>
            <ta e="T75" id="Seg_4854" s="T74">truth.[NOM]</ta>
            <ta e="T76" id="Seg_4855" s="T75">law.[NOM]</ta>
            <ta e="T77" id="Seg_4856" s="T76">change-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_4857" s="T77">be-PST2.[3SG]</ta>
            <ta e="T79" id="Seg_4858" s="T78">former</ta>
            <ta e="T80" id="Seg_4859" s="T79">Czars</ta>
            <ta e="T81" id="Seg_4860" s="T80">law.[NOM]</ta>
            <ta e="T82" id="Seg_4861" s="T81">well</ta>
            <ta e="T83" id="Seg_4862" s="T82">stop-PST1-3SG</ta>
            <ta e="T84" id="Seg_4863" s="T83">say-PST2-3PL</ta>
            <ta e="T85" id="Seg_4864" s="T84">then</ta>
            <ta e="T86" id="Seg_4865" s="T85">later</ta>
            <ta e="T87" id="Seg_4866" s="T86">end-EP-ABL</ta>
            <ta e="T88" id="Seg_4867" s="T87">school-DAT/LOC</ta>
            <ta e="T89" id="Seg_4868" s="T88">learn-PTCP.FUT-DAT/LOC</ta>
            <ta e="T90" id="Seg_4869" s="T89">say-CVB.SEQ</ta>
            <ta e="T91" id="Seg_4870" s="T90">comrade.[NOM]</ta>
            <ta e="T92" id="Seg_4871" s="T91">Lenin.[NOM]</ta>
            <ta e="T93" id="Seg_4872" s="T92">grandfather.[NOM]</ta>
            <ta e="T94" id="Seg_4873" s="T93">say-CVB.SEQ</ta>
            <ta e="T95" id="Seg_4874" s="T94">be-PST1-3SG</ta>
            <ta e="T96" id="Seg_4875" s="T95">Lenin.[NOM]</ta>
            <ta e="T97" id="Seg_4876" s="T96">law-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_4877" s="T97">well</ta>
            <ta e="T99" id="Seg_4878" s="T98">change-CVB.SEQ-change-CVB.SEQ</ta>
            <ta e="T100" id="Seg_4879" s="T99">that</ta>
            <ta e="T101" id="Seg_4880" s="T100">change-CVB.SEQ</ta>
            <ta e="T102" id="Seg_4881" s="T101">go-PST1-3SG</ta>
            <ta e="T103" id="Seg_4882" s="T102">end-EP-ABL</ta>
            <ta e="T104" id="Seg_4883" s="T103">human.being.[NOM]</ta>
            <ta e="T105" id="Seg_4884" s="T104">every-3SG.[NOM]</ta>
            <ta e="T106" id="Seg_4885" s="T105">short</ta>
            <ta e="T107" id="Seg_4886" s="T106">narrow</ta>
            <ta e="T108" id="Seg_4887" s="T107">child-3SG.[NOM]</ta>
            <ta e="T109" id="Seg_4888" s="T108">what.kind.of</ta>
            <ta e="T110" id="Seg_4889" s="T109">INDEF</ta>
            <ta e="T111" id="Seg_4890" s="T110">people.[NOM]</ta>
            <ta e="T112" id="Seg_4891" s="T111">what.kind.of</ta>
            <ta e="T113" id="Seg_4892" s="T112">EMPH</ta>
            <ta e="T114" id="Seg_4893" s="T113">short</ta>
            <ta e="T115" id="Seg_4894" s="T114">narrow</ta>
            <ta e="T116" id="Seg_4895" s="T115">poor</ta>
            <ta e="T117" id="Seg_4896" s="T116">human.being.[NOM]</ta>
            <ta e="T118" id="Seg_4897" s="T117">child-3SG.[NOM]</ta>
            <ta e="T119" id="Seg_4898" s="T118">every-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_4899" s="T119">education-DAT/LOC</ta>
            <ta e="T121" id="Seg_4900" s="T120">and</ta>
            <ta e="T122" id="Seg_4901" s="T121">well</ta>
            <ta e="T123" id="Seg_4902" s="T122">begin-VBZ-PST1-3SG</ta>
            <ta e="T124" id="Seg_4903" s="T123">oh</ta>
            <ta e="T125" id="Seg_4904" s="T124">that.EMPH.[NOM]</ta>
            <ta e="T126" id="Seg_4905" s="T125">despite</ta>
            <ta e="T127" id="Seg_4906" s="T126">rich-ABL</ta>
            <ta e="T128" id="Seg_4907" s="T127">eat-CVB.SIM</ta>
            <ta e="T129" id="Seg_4908" s="T128">learn-PTCP.PST-PL.[NOM]</ta>
            <ta e="T130" id="Seg_4909" s="T129">kulak-ABL</ta>
            <ta e="T131" id="Seg_4910" s="T130">come.to.aid-EP-REFL-CVB.SIM</ta>
            <ta e="T132" id="Seg_4911" s="T131">learn-PTCP.PST-PL.[NOM]</ta>
            <ta e="T133" id="Seg_4912" s="T132">however</ta>
            <ta e="T134" id="Seg_4913" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_4914" s="T134">school-DAT/LOC</ta>
            <ta e="T136" id="Seg_4915" s="T135">give-PTCP.FUT-1PL-ACC</ta>
            <ta e="T137" id="Seg_4916" s="T136">say-NEG.PTCP</ta>
            <ta e="T138" id="Seg_4917" s="T137">be-PST1-3PL</ta>
            <ta e="T139" id="Seg_4918" s="T138">child-ACC</ta>
            <ta e="T140" id="Seg_4919" s="T139">then</ta>
            <ta e="T141" id="Seg_4920" s="T140">become-PST2-3SG</ta>
            <ta e="T142" id="Seg_4921" s="T141">kolkhoz.[NOM]</ta>
            <ta e="T143" id="Seg_4922" s="T142">be-PTCP.FUT-DAT/LOC</ta>
            <ta e="T144" id="Seg_4923" s="T143">think-CVB.SEQ</ta>
            <ta e="T145" id="Seg_4924" s="T144">general</ta>
            <ta e="T146" id="Seg_4925" s="T145">economy-INSTR</ta>
            <ta e="T147" id="Seg_4926" s="T146">people.[NOM]</ta>
            <ta e="T148" id="Seg_4927" s="T147">life-3SG-ACC</ta>
            <ta e="T149" id="Seg_4928" s="T148">build.up-PTCP.FUT-3SG-ACC</ta>
            <ta e="T150" id="Seg_4929" s="T149">say-CVB.SEQ-3PL</ta>
            <ta e="T151" id="Seg_4930" s="T150">no</ta>
            <ta e="T152" id="Seg_4931" s="T151">that-DAT/LOC</ta>
            <ta e="T153" id="Seg_4932" s="T152">also</ta>
            <ta e="T154" id="Seg_4933" s="T153">be-CVB.SIM</ta>
            <ta e="T155" id="Seg_4934" s="T154">pull-PST2.NEG-3PL</ta>
            <ta e="T156" id="Seg_4935" s="T155">how.much</ta>
            <ta e="T157" id="Seg_4936" s="T156">INDEF</ta>
            <ta e="T158" id="Seg_4937" s="T157">year-EP-INSTR</ta>
            <ta e="T159" id="Seg_4938" s="T158">be</ta>
            <ta e="T160" id="Seg_4939" s="T159">kolkhoz.[NOM]</ta>
            <ta e="T161" id="Seg_4940" s="T160">become-PST2.NEG-3PL</ta>
            <ta e="T162" id="Seg_4941" s="T161">kolkhoz.[NOM]</ta>
            <ta e="T163" id="Seg_4942" s="T162">be-CVB.SEQ-1PL</ta>
            <ta e="T164" id="Seg_4943" s="T163">1PL.[NOM]</ta>
            <ta e="T165" id="Seg_4944" s="T164">be.able-FUT-1PL</ta>
            <ta e="T166" id="Seg_4945" s="T165">NEG.[3SG]</ta>
            <ta e="T167" id="Seg_4946" s="T166">say-PTCP.PRS</ta>
            <ta e="T168" id="Seg_4947" s="T167">be-PST1-3PL</ta>
            <ta e="T169" id="Seg_4948" s="T168">before</ta>
            <ta e="T170" id="Seg_4949" s="T169">Czars</ta>
            <ta e="T171" id="Seg_4950" s="T170">law-DAT/LOC</ta>
            <ta e="T172" id="Seg_4951" s="T171">be.born-PTCP.PST</ta>
            <ta e="T173" id="Seg_4952" s="T172">people.[NOM]</ta>
            <ta e="T174" id="Seg_4953" s="T173">learn-PTCP.PST</ta>
            <ta e="T175" id="Seg_4954" s="T174">people.[NOM]</ta>
            <ta e="T176" id="Seg_4955" s="T175">then</ta>
            <ta e="T177" id="Seg_4956" s="T176">how</ta>
            <ta e="T178" id="Seg_4957" s="T177">INDEF</ta>
            <ta e="T180" id="Seg_4958" s="T178">make-CVB.SEQ</ta>
            <ta e="T181" id="Seg_4959" s="T180">tell-CVB.SEQ</ta>
            <ta e="T182" id="Seg_4960" s="T181">tell-CVB.SEQ-3PL</ta>
            <ta e="T183" id="Seg_4961" s="T182">law.[NOM]</ta>
            <ta e="T184" id="Seg_4962" s="T183">come-CVB.SEQ-come-CVB.SEQ</ta>
            <ta e="T185" id="Seg_4963" s="T184">this.EMPH</ta>
            <ta e="T186" id="Seg_4964" s="T185">country-PL-DAT/LOC</ta>
            <ta e="T187" id="Seg_4965" s="T186">come-CVB.SEQ</ta>
            <ta e="T188" id="Seg_4966" s="T187">come-CVB.SEQ</ta>
            <ta e="T189" id="Seg_4967" s="T188">kolkhoz.[NOM]</ta>
            <ta e="T190" id="Seg_4968" s="T189">be-PST1-1PL</ta>
            <ta e="T191" id="Seg_4969" s="T190">thirty</ta>
            <ta e="T192" id="Seg_4970" s="T191">seven-ORD-PROPR-EP-ABL</ta>
            <ta e="T193" id="Seg_4971" s="T192">thirty</ta>
            <ta e="T194" id="Seg_4972" s="T193">eight-PROPR-DAT/LOC</ta>
            <ta e="T198" id="Seg_4973" s="T196">truth.[NOM]</ta>
            <ta e="T199" id="Seg_4974" s="T198">well</ta>
            <ta e="T200" id="Seg_4975" s="T199">kolkhoz.[NOM]</ta>
            <ta e="T201" id="Seg_4976" s="T200">EMPH</ta>
            <ta e="T202" id="Seg_4977" s="T201">begin-VBZ-CVB.SEQ</ta>
            <ta e="T203" id="Seg_4978" s="T202">establish-VBZ-CVB.SEQ</ta>
            <ta e="T204" id="Seg_4979" s="T203">stay</ta>
            <ta e="T205" id="Seg_4980" s="T204">end-EP-ABL</ta>
            <ta e="T206" id="Seg_4981" s="T205">then</ta>
            <ta e="T207" id="Seg_4982" s="T206">kolkhoz-ACC</ta>
            <ta e="T208" id="Seg_4983" s="T207">give.birth-EP-MED-PST2-1PL</ta>
            <ta e="T209" id="Seg_4984" s="T208">be-PST1-3SG</ta>
            <ta e="T210" id="Seg_4985" s="T209">1PL.[NOM]</ta>
            <ta e="T211" id="Seg_4986" s="T210">where</ta>
            <ta e="T212" id="Seg_4987" s="T211">more</ta>
            <ta e="T213" id="Seg_4988" s="T212">reindeer-PROPR-ABL</ta>
            <ta e="T214" id="Seg_4989" s="T213">that-DAT/LOC</ta>
            <ta e="T215" id="Seg_4990" s="T214">kolkhoz-DAT/LOC</ta>
            <ta e="T216" id="Seg_4991" s="T215">hold-PST2-1PL</ta>
            <ta e="T217" id="Seg_4992" s="T216">where</ta>
            <ta e="T218" id="Seg_4993" s="T217">deadfall-PROPR-1PL-ACC</ta>
            <ta e="T219" id="Seg_4994" s="T218">EMPH-then</ta>
            <ta e="T220" id="Seg_4995" s="T219">self-1PL.[NOM]</ta>
            <ta e="T221" id="Seg_4996" s="T220">deadfall-1PL-INSTR</ta>
            <ta e="T222" id="Seg_4997" s="T221">self-1PL.[NOM]</ta>
            <ta e="T223" id="Seg_4998" s="T222">gin.trap-1PL-INSTR</ta>
            <ta e="T224" id="Seg_4999" s="T223">end-EP-ABL</ta>
            <ta e="T225" id="Seg_5000" s="T224">kolkhoz.[NOM]</ta>
            <ta e="T226" id="Seg_5001" s="T225">life-3SG-ACC</ta>
            <ta e="T227" id="Seg_5002" s="T226">build-CVB.SEQ-1PL</ta>
            <ta e="T228" id="Seg_5003" s="T227">now</ta>
            <ta e="T229" id="Seg_5004" s="T228">this</ta>
            <ta e="T230" id="Seg_5005" s="T229">time-PL-DAT/LOC</ta>
            <ta e="T231" id="Seg_5006" s="T230">reach-PST1-1PL</ta>
            <ta e="T232" id="Seg_5007" s="T231">well</ta>
            <ta e="T233" id="Seg_5008" s="T232">well</ta>
            <ta e="T234" id="Seg_5009" s="T233">then</ta>
            <ta e="T235" id="Seg_5010" s="T234">kolkhoz.[NOM]</ta>
            <ta e="T236" id="Seg_5011" s="T235">life-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_5012" s="T236">good.[NOM]</ta>
            <ta e="T238" id="Seg_5013" s="T237">MOD</ta>
            <ta e="T239" id="Seg_5014" s="T238">say-CVB.SEQ</ta>
            <ta e="T240" id="Seg_5015" s="T239">that.[NOM]</ta>
            <ta e="T241" id="Seg_5016" s="T240">similar</ta>
            <ta e="T242" id="Seg_5017" s="T241">MOD</ta>
            <ta e="T243" id="Seg_5018" s="T242">be-PTCP.PST-EP-INSTR</ta>
            <ta e="T244" id="Seg_5019" s="T243">well</ta>
            <ta e="T245" id="Seg_5020" s="T244">kolkhoz.[NOM]</ta>
            <ta e="T246" id="Seg_5021" s="T245">be-PST2-1PL</ta>
            <ta e="T247" id="Seg_5022" s="T246">that</ta>
            <ta e="T248" id="Seg_5023" s="T247">that.side.[NOM]</ta>
            <ta e="T249" id="Seg_5024" s="T248">side-3SG-DAT/LOC</ta>
            <ta e="T250" id="Seg_5025" s="T249">again</ta>
            <ta e="T251" id="Seg_5026" s="T250">EMPH</ta>
            <ta e="T252" id="Seg_5027" s="T251">pain-PROPR</ta>
            <ta e="T253" id="Seg_5028" s="T252">need-PROPR</ta>
            <ta e="T254" id="Seg_5029" s="T253">time-PL.[NOM]</ta>
            <ta e="T255" id="Seg_5030" s="T254">be-PTCP.PRS</ta>
            <ta e="T256" id="Seg_5031" s="T255">be-PST1-3PL</ta>
            <ta e="T257" id="Seg_5032" s="T256">this</ta>
            <ta e="T258" id="Seg_5033" s="T257">kulak-DAT/LOC</ta>
            <ta e="T259" id="Seg_5034" s="T258">former</ta>
            <ta e="T260" id="Seg_5035" s="T259">Czars-DAT/LOC</ta>
            <ta e="T261" id="Seg_5036" s="T260">head-PL-3SG-ACC</ta>
            <ta e="T262" id="Seg_5037" s="T261">scatter-CAUS-PTCP.PST</ta>
            <ta e="T263" id="Seg_5038" s="T262">human.being-PL.[NOM]</ta>
            <ta e="T265" id="Seg_5039" s="T264">bandite-VBZ-PTCP.PRS</ta>
            <ta e="T266" id="Seg_5040" s="T265">and</ta>
            <ta e="T267" id="Seg_5041" s="T266">be-PST1-3PL</ta>
            <ta e="T268" id="Seg_5042" s="T267">that-PL.[NOM]</ta>
            <ta e="T269" id="Seg_5043" s="T268">however</ta>
            <ta e="T270" id="Seg_5044" s="T269">pain-ACC</ta>
            <ta e="T271" id="Seg_5045" s="T270">make-PTCP.HAB</ta>
            <ta e="T272" id="Seg_5046" s="T271">be-PST1-3PL</ta>
            <ta e="T273" id="Seg_5047" s="T272">that</ta>
            <ta e="T274" id="Seg_5048" s="T273">bandite-ACC</ta>
            <ta e="T275" id="Seg_5049" s="T274">no</ta>
            <ta e="T276" id="Seg_5050" s="T275">that</ta>
            <ta e="T277" id="Seg_5051" s="T276">rank-ACC</ta>
            <ta e="T278" id="Seg_5052" s="T277">Soviet</ta>
            <ta e="T279" id="Seg_5053" s="T278">power.[NOM]</ta>
            <ta e="T280" id="Seg_5054" s="T279">front.[NOM]</ta>
            <ta e="T281" id="Seg_5055" s="T280">to</ta>
            <ta e="T282" id="Seg_5056" s="T281">stride-CVB.SEQ</ta>
            <ta e="T283" id="Seg_5057" s="T282">go-CVB.SEQ</ta>
            <ta e="T284" id="Seg_5058" s="T283">that</ta>
            <ta e="T285" id="Seg_5059" s="T284">bandite-ACC</ta>
            <ta e="T286" id="Seg_5060" s="T285">however</ta>
            <ta e="T287" id="Seg_5061" s="T286">EMPH</ta>
            <ta e="T288" id="Seg_5062" s="T287">clean-PTCP.HAB</ta>
            <ta e="T289" id="Seg_5063" s="T288">be-PST1-3SG</ta>
            <ta e="T290" id="Seg_5064" s="T289">put.in.order-PTCP.HAB</ta>
            <ta e="T291" id="Seg_5065" s="T290">and</ta>
            <ta e="T292" id="Seg_5066" s="T291">be-PST1-3SG</ta>
            <ta e="T293" id="Seg_5067" s="T292">well</ta>
            <ta e="T294" id="Seg_5068" s="T293">that</ta>
            <ta e="T295" id="Seg_5069" s="T294">stop-PTCP.PST-3SG-ACC</ta>
            <ta e="T296" id="Seg_5070" s="T295">after</ta>
            <ta e="T297" id="Seg_5071" s="T296">end-EP-ABL</ta>
            <ta e="T298" id="Seg_5072" s="T297">this</ta>
            <ta e="T299" id="Seg_5073" s="T298">Germany.[NOM]</ta>
            <ta e="T300" id="Seg_5074" s="T299">war-3SG.[NOM]</ta>
            <ta e="T301" id="Seg_5075" s="T300">become-PST2-3SG</ta>
            <ta e="T302" id="Seg_5076" s="T301">well</ta>
            <ta e="T303" id="Seg_5077" s="T302">that-DAT/LOC</ta>
            <ta e="T304" id="Seg_5078" s="T303">worry-CVB.SIM</ta>
            <ta e="T305" id="Seg_5079" s="T304">beat-PST2-1PL</ta>
            <ta e="T306" id="Seg_5080" s="T305">1PL.[NOM]</ta>
            <ta e="T307" id="Seg_5081" s="T306">very</ta>
            <ta e="T308" id="Seg_5082" s="T307">that</ta>
            <ta e="T309" id="Seg_5083" s="T308">clothes.[NOM]</ta>
            <ta e="T310" id="Seg_5084" s="T309">and</ta>
            <ta e="T311" id="Seg_5085" s="T310">gather.oneself-NMNZ-3SG.[NOM]</ta>
            <ta e="T312" id="Seg_5086" s="T311">be-PTCP.PRS</ta>
            <ta e="T313" id="Seg_5087" s="T312">be-PST1-3SG</ta>
            <ta e="T314" id="Seg_5088" s="T313">that</ta>
            <ta e="T315" id="Seg_5089" s="T314">money-INSTR</ta>
            <ta e="T316" id="Seg_5090" s="T315">and</ta>
            <ta e="T317" id="Seg_5091" s="T316">tax.[NOM]</ta>
            <ta e="T318" id="Seg_5092" s="T317">be-PTCP.PRS</ta>
            <ta e="T319" id="Seg_5093" s="T318">be-PST1-3SG</ta>
            <ta e="T320" id="Seg_5094" s="T319">1PL.[NOM]</ta>
            <ta e="T321" id="Seg_5095" s="T320">that.[NOM]</ta>
            <ta e="T322" id="Seg_5096" s="T321">to</ta>
            <ta e="T323" id="Seg_5097" s="T322">city.[NOM]</ta>
            <ta e="T324" id="Seg_5098" s="T323">place-PL-DAT/LOC</ta>
            <ta e="T325" id="Seg_5099" s="T324">army-DAT/LOC</ta>
            <ta e="T327" id="Seg_5100" s="T325">help-CVB.SEQ-1PL</ta>
            <ta e="T328" id="Seg_5101" s="T327">then</ta>
            <ta e="T329" id="Seg_5102" s="T328">year.[NOM]</ta>
            <ta e="T330" id="Seg_5103" s="T329">stop-CVB.SEQ</ta>
            <ta e="T331" id="Seg_5104" s="T330">well</ta>
            <ta e="T332" id="Seg_5105" s="T331">now</ta>
            <ta e="T333" id="Seg_5106" s="T332">what.[NOM]</ta>
            <ta e="T334" id="Seg_5107" s="T333">NEG</ta>
            <ta e="T335" id="Seg_5108" s="T334">be-FUT-1PL</ta>
            <ta e="T336" id="Seg_5109" s="T335">think-NEG-1SG</ta>
            <ta e="T337" id="Seg_5110" s="T336">todays</ta>
            <ta e="T338" id="Seg_5111" s="T337">soul-1SG.[NOM]</ta>
            <ta e="T339" id="Seg_5112" s="T338">good.[NOM]</ta>
            <ta e="T340" id="Seg_5113" s="T339">now</ta>
            <ta e="T341" id="Seg_5114" s="T340">life.[NOM]</ta>
            <ta e="T342" id="Seg_5115" s="T341">get.better-PST1-3SG</ta>
            <ta e="T343" id="Seg_5116" s="T342">life.[NOM]</ta>
            <ta e="T344" id="Seg_5117" s="T343">grow-PST1-3SG</ta>
            <ta e="T345" id="Seg_5118" s="T344">now</ta>
            <ta e="T346" id="Seg_5119" s="T345">intact</ta>
            <ta e="T347" id="Seg_5120" s="T346">kolkhoz-PL.[NOM]</ta>
            <ta e="T348" id="Seg_5121" s="T347">end-EP-ABL</ta>
            <ta e="T349" id="Seg_5122" s="T348">organisation.[NOM]</ta>
            <ta e="T350" id="Seg_5123" s="T349">similar</ta>
            <ta e="T351" id="Seg_5124" s="T350">become-PST1-3PL</ta>
            <ta e="T352" id="Seg_5125" s="T351">now</ta>
            <ta e="T353" id="Seg_5126" s="T352">from.here</ta>
            <ta e="T354" id="Seg_5127" s="T353">front.[NOM]</ta>
            <ta e="T355" id="Seg_5128" s="T354">side-3SG-ACC</ta>
            <ta e="T356" id="Seg_5129" s="T355">know-NEG-1SG</ta>
            <ta e="T357" id="Seg_5130" s="T356">self-1SG.[NOM]</ta>
            <ta e="T358" id="Seg_5131" s="T357">age-CVB.SEQ</ta>
            <ta e="T359" id="Seg_5132" s="T358">stay-PST1-1SG</ta>
            <ta e="T362" id="Seg_5133" s="T361">1SG.[NOM]</ta>
            <ta e="T363" id="Seg_5134" s="T362">thought-1SG-DAT/LOC</ta>
            <ta e="T364" id="Seg_5135" s="T363">good</ta>
            <ta e="T365" id="Seg_5136" s="T364">life-DAT/LOC</ta>
            <ta e="T366" id="Seg_5137" s="T365">now</ta>
            <ta e="T367" id="Seg_5138" s="T366">reach-PST1-1PL</ta>
            <ta e="T368" id="Seg_5139" s="T367">say-PRS-1SG</ta>
            <ta e="T369" id="Seg_5140" s="T368">this.EMPH</ta>
            <ta e="T370" id="Seg_5141" s="T369">age-PTCP.PRS</ta>
            <ta e="T372" id="Seg_5142" s="T370">age-1SG-DAT/LOC</ta>
            <ta e="T373" id="Seg_5143" s="T372">now</ta>
            <ta e="T374" id="Seg_5144" s="T373">sated.[NOM]</ta>
            <ta e="T375" id="Seg_5145" s="T374">very-1PL</ta>
            <ta e="T376" id="Seg_5146" s="T375">warm.[NOM]</ta>
            <ta e="T377" id="Seg_5147" s="T376">very-1PL</ta>
            <ta e="T378" id="Seg_5148" s="T377">clothes-ACC</ta>
            <ta e="T379" id="Seg_5149" s="T378">and</ta>
            <ta e="T380" id="Seg_5150" s="T379">dress-PRS-1PL</ta>
            <ta e="T381" id="Seg_5151" s="T380">food-ACC</ta>
            <ta e="T382" id="Seg_5152" s="T381">and</ta>
            <ta e="T383" id="Seg_5153" s="T382">eat-PRS-1PL</ta>
            <ta e="T384" id="Seg_5154" s="T383">this</ta>
            <ta e="T385" id="Seg_5155" s="T384">well</ta>
            <ta e="T386" id="Seg_5156" s="T385">at.first</ta>
            <ta e="T387" id="Seg_5157" s="T386">my</ta>
            <ta e="T388" id="Seg_5158" s="T387">this</ta>
            <ta e="T389" id="Seg_5159" s="T388">big</ta>
            <ta e="T390" id="Seg_5160" s="T389">child-1SG.[NOM]</ta>
            <ta e="T391" id="Seg_5161" s="T390">this</ta>
            <ta e="T392" id="Seg_5162" s="T391">Vasiliy.[NOM]</ta>
            <ta e="T393" id="Seg_5163" s="T392">there.is</ta>
            <ta e="T394" id="Seg_5164" s="T393">this</ta>
            <ta e="T395" id="Seg_5165" s="T394">child-1SG.[NOM]</ta>
            <ta e="T396" id="Seg_5166" s="T395">grow-PST1-3SG</ta>
            <ta e="T397" id="Seg_5167" s="T396">and</ta>
            <ta e="T398" id="Seg_5168" s="T397">learn-PST2-3SG</ta>
            <ta e="T399" id="Seg_5169" s="T398">3SG.[NOM]</ta>
            <ta e="T400" id="Seg_5170" s="T399">school-3SG-ACC</ta>
            <ta e="T401" id="Seg_5171" s="T400">stop-EP-CAUS-PST1-3SG</ta>
            <ta e="T402" id="Seg_5172" s="T401">EMPH</ta>
            <ta e="T403" id="Seg_5173" s="T402">teacher-DAT/LOC</ta>
            <ta e="T404" id="Seg_5174" s="T403">learn-PST2-3SG</ta>
            <ta e="T405" id="Seg_5175" s="T404">so</ta>
            <ta e="T406" id="Seg_5176" s="T405">now</ta>
            <ta e="T407" id="Seg_5177" s="T406">day.[NOM]</ta>
            <ta e="T408" id="Seg_5178" s="T407">today</ta>
            <ta e="T409" id="Seg_5179" s="T408">teacher-VBZ-CVB.SIM</ta>
            <ta e="T410" id="Seg_5180" s="T409">sit-PRS.[3SG]</ta>
            <ta e="T411" id="Seg_5181" s="T410">then</ta>
            <ta e="T412" id="Seg_5182" s="T411">one</ta>
            <ta e="T413" id="Seg_5183" s="T412">daughter-PROPR-1SG</ta>
            <ta e="T414" id="Seg_5184" s="T413">that.[NOM]</ta>
            <ta e="T415" id="Seg_5185" s="T414">also</ta>
            <ta e="T416" id="Seg_5186" s="T415">education-3SG-ACC</ta>
            <ta e="T417" id="Seg_5187" s="T416">school-3SG-ACC</ta>
            <ta e="T418" id="Seg_5188" s="T417">stop-EP-CAUS-CVB.SEQ</ta>
            <ta e="T419" id="Seg_5189" s="T418">also</ta>
            <ta e="T420" id="Seg_5190" s="T419">higher.[NOM]</ta>
            <ta e="T421" id="Seg_5191" s="T420">learn-CVB.SEQ</ta>
            <ta e="T422" id="Seg_5192" s="T421">doctor-VBZ-CVB.SEQ</ta>
            <ta e="T423" id="Seg_5193" s="T422">sit-PRS.[3SG]</ta>
            <ta e="T424" id="Seg_5194" s="T423">now</ta>
            <ta e="T425" id="Seg_5195" s="T424">that</ta>
            <ta e="T426" id="Seg_5196" s="T425">self-3SG-GEN</ta>
            <ta e="T427" id="Seg_5197" s="T426">front-3SG-DAT/LOC</ta>
            <ta e="T428" id="Seg_5198" s="T427">good.[NOM]</ta>
            <ta e="T429" id="Seg_5199" s="T428">that.[NOM]</ta>
            <ta e="T430" id="Seg_5200" s="T429">after</ta>
            <ta e="T431" id="Seg_5201" s="T430">1SG.[NOM]</ta>
            <ta e="T432" id="Seg_5202" s="T431">and</ta>
            <ta e="T433" id="Seg_5203" s="T432">soul-1SG-DAT/LOC</ta>
            <ta e="T434" id="Seg_5204" s="T433">good.[NOM]</ta>
            <ta e="T435" id="Seg_5205" s="T434">now</ta>
            <ta e="T436" id="Seg_5206" s="T435">this</ta>
            <ta e="T437" id="Seg_5207" s="T436">life-DAT/LOC</ta>
            <ta e="T438" id="Seg_5208" s="T437">live-NMNZ-3PL.[NOM]</ta>
            <ta e="T439" id="Seg_5209" s="T438">now</ta>
            <ta e="T440" id="Seg_5210" s="T439">this</ta>
            <ta e="T441" id="Seg_5211" s="T440">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T442" id="Seg_5212" s="T441">that-DAT/LOC</ta>
            <ta e="T443" id="Seg_5213" s="T442">three-ORD</ta>
            <ta e="T444" id="Seg_5214" s="T443">again</ta>
            <ta e="T445" id="Seg_5215" s="T444">boy-EP-1SG.[NOM]</ta>
            <ta e="T446" id="Seg_5216" s="T445">airport-DAT/LOC</ta>
            <ta e="T447" id="Seg_5217" s="T446">work-PRS.[3SG]</ta>
            <ta e="T448" id="Seg_5218" s="T447">then</ta>
            <ta e="T449" id="Seg_5219" s="T448">most</ta>
            <ta e="T450" id="Seg_5220" s="T449">small.[NOM]</ta>
            <ta e="T451" id="Seg_5221" s="T450">son-PROPR-1SG</ta>
            <ta e="T452" id="Seg_5222" s="T451">now</ta>
            <ta e="T453" id="Seg_5223" s="T452">Ivan.[NOM]</ta>
            <ta e="T454" id="Seg_5224" s="T453">Antonov</ta>
            <ta e="T455" id="Seg_5225" s="T454">Ivan.[NOM]</ta>
            <ta e="T458" id="Seg_5226" s="T457">that</ta>
            <ta e="T459" id="Seg_5227" s="T458">MOD</ta>
            <ta e="T460" id="Seg_5228" s="T459">now</ta>
            <ta e="T461" id="Seg_5229" s="T460">kolkhoz-DAT/LOC</ta>
            <ta e="T462" id="Seg_5230" s="T461">work-PRS.[3SG]</ta>
            <ta e="T463" id="Seg_5231" s="T462">kolkhoz.[NOM]</ta>
            <ta e="T464" id="Seg_5232" s="T463">EMPH</ta>
            <ta e="T465" id="Seg_5233" s="T464">work-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T466" id="Seg_5234" s="T465">work-3SG.[NOM]</ta>
            <ta e="T467" id="Seg_5235" s="T466">work-3SG-ACC</ta>
            <ta e="T468" id="Seg_5236" s="T467">and</ta>
            <ta e="T469" id="Seg_5237" s="T468">work-PROPR</ta>
            <ta e="T470" id="Seg_5238" s="T469">kolkhoz.[NOM]</ta>
            <ta e="T471" id="Seg_5239" s="T470">however</ta>
            <ta e="T472" id="Seg_5240" s="T471">EMPH</ta>
            <ta e="T473" id="Seg_5241" s="T472">self-3SG-GEN</ta>
            <ta e="T474" id="Seg_5242" s="T473">kolkhoz.[NOM]</ta>
            <ta e="T475" id="Seg_5243" s="T474">inside-3SG-DAT/LOC</ta>
            <ta e="T476" id="Seg_5244" s="T475">however</ta>
            <ta e="T477" id="Seg_5245" s="T476">good-ADVZ</ta>
            <ta e="T478" id="Seg_5246" s="T477">eat.ones.fill-EP-CAUS-CVB.SEQ</ta>
            <ta e="T479" id="Seg_5247" s="T478">dress-CAUS-CVB.SEQ</ta>
            <ta e="T480" id="Seg_5248" s="T479">however</ta>
            <ta e="T481" id="Seg_5249" s="T480">and</ta>
            <ta e="T482" id="Seg_5250" s="T481">work-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_5251" s="T482">however</ta>
            <ta e="T484" id="Seg_5252" s="T483">and</ta>
            <ta e="T485" id="Seg_5253" s="T484">salary-PROPR.[NOM]</ta>
            <ta e="T486" id="Seg_5254" s="T485">human.being.[NOM]</ta>
            <ta e="T487" id="Seg_5255" s="T486">where</ta>
            <ta e="T488" id="Seg_5256" s="T487">good-ADVZ</ta>
            <ta e="T489" id="Seg_5257" s="T488">work-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T490" id="Seg_5258" s="T489">that</ta>
            <ta e="T491" id="Seg_5259" s="T490">better</ta>
            <ta e="T492" id="Seg_5260" s="T491">become-PRS.[3SG]</ta>
            <ta e="T493" id="Seg_5261" s="T492">where</ta>
            <ta e="T494" id="Seg_5262" s="T493">bad-ADVZ</ta>
            <ta e="T495" id="Seg_5263" s="T494">work-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T496" id="Seg_5264" s="T495">that</ta>
            <ta e="T497" id="Seg_5265" s="T496">down</ta>
            <ta e="T498" id="Seg_5266" s="T497">become-PRS.[3SG]</ta>
            <ta e="T499" id="Seg_5267" s="T498">1SG.[NOM]</ta>
            <ta e="T500" id="Seg_5268" s="T499">thought-1SG-DAT/LOC</ta>
            <ta e="T501" id="Seg_5269" s="T500">EMPH-like.that</ta>
            <ta e="T502" id="Seg_5270" s="T501">this</ta>
            <ta e="T503" id="Seg_5271" s="T502">well</ta>
            <ta e="T504" id="Seg_5272" s="T503">at.first</ta>
            <ta e="T505" id="Seg_5273" s="T504">self-1SG.[NOM]</ta>
            <ta e="T506" id="Seg_5274" s="T505">however</ta>
            <ta e="T507" id="Seg_5275" s="T506">EMPH</ta>
            <ta e="T508" id="Seg_5276" s="T507">this</ta>
            <ta e="T509" id="Seg_5277" s="T508">selsoviet-EP-INSTR</ta>
            <ta e="T510" id="Seg_5278" s="T509">what-EP-INSTR</ta>
            <ta e="T511" id="Seg_5279" s="T510">lead-VBZ-CVB.SEQ</ta>
            <ta e="T512" id="Seg_5280" s="T511">sit-PTCP.PRS</ta>
            <ta e="T513" id="Seg_5281" s="T512">be-PST1-1SG</ta>
            <ta e="T514" id="Seg_5282" s="T513">then</ta>
            <ta e="T515" id="Seg_5283" s="T514">now</ta>
            <ta e="T516" id="Seg_5284" s="T515">young</ta>
            <ta e="T517" id="Seg_5285" s="T516">human.being-PL.[NOM]</ta>
            <ta e="T518" id="Seg_5286" s="T517">education-DAT/LOC</ta>
            <ta e="T519" id="Seg_5287" s="T518">learn-CVB.SEQ-3PL</ta>
            <ta e="T520" id="Seg_5288" s="T519">1SG-COMP</ta>
            <ta e="T521" id="Seg_5289" s="T520">better</ta>
            <ta e="T522" id="Seg_5290" s="T521">work-CAP-3PL</ta>
            <ta e="T523" id="Seg_5291" s="T522">then</ta>
            <ta e="T524" id="Seg_5292" s="T523">MOD</ta>
            <ta e="T525" id="Seg_5293" s="T524">who-DAT/LOC</ta>
            <ta e="T526" id="Seg_5294" s="T525">EMPH</ta>
            <ta e="T527" id="Seg_5295" s="T526">guilt.[NOM]</ta>
            <ta e="T528" id="Seg_5296" s="T527">word-POSS</ta>
            <ta e="T529" id="Seg_5297" s="T528">NEG</ta>
            <ta e="T530" id="Seg_5298" s="T529">now</ta>
            <ta e="T531" id="Seg_5299" s="T530">relax-FUT-1SG</ta>
            <ta e="T532" id="Seg_5300" s="T531">say-CVB.SEQ-1SG</ta>
            <ta e="T533" id="Seg_5301" s="T532">selsoviet-VBZ-CVB.SEQ</ta>
            <ta e="T534" id="Seg_5302" s="T533">stop-PST2-EP-1SG</ta>
            <ta e="T535" id="Seg_5303" s="T534">be-PST1-3SG</ta>
            <ta e="T536" id="Seg_5304" s="T535">now</ta>
            <ta e="T537" id="Seg_5305" s="T536">this</ta>
            <ta e="T538" id="Seg_5306" s="T537">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T539" id="Seg_5307" s="T538">education-DAT/LOC</ta>
            <ta e="T540" id="Seg_5308" s="T539">learn-CVB.SEQ-3PL</ta>
            <ta e="T541" id="Seg_5309" s="T540">good-ADVZ</ta>
            <ta e="T544" id="Seg_5310" s="T543">live-PRS-3PL</ta>
            <ta e="T545" id="Seg_5311" s="T544">then</ta>
            <ta e="T546" id="Seg_5312" s="T545">now</ta>
            <ta e="T547" id="Seg_5313" s="T546">be.happy-EP-REFL-CVB.SIM</ta>
            <ta e="T548" id="Seg_5314" s="T547">sit-PRS-1SG</ta>
            <ta e="T549" id="Seg_5315" s="T548">this</ta>
            <ta e="T550" id="Seg_5316" s="T549">age-CVB.SEQ</ta>
            <ta e="T551" id="Seg_5317" s="T550">go-CVB.SEQ-1SG</ta>
            <ta e="T552" id="Seg_5318" s="T551">self-1SG.[NOM]</ta>
            <ta e="T553" id="Seg_5319" s="T552">education-POSS</ta>
            <ta e="T554" id="Seg_5320" s="T553">NEG</ta>
            <ta e="T555" id="Seg_5321" s="T554">very</ta>
            <ta e="T556" id="Seg_5322" s="T555">be-PST1-1SG</ta>
            <ta e="T557" id="Seg_5323" s="T556">that.[NOM]</ta>
            <ta e="T558" id="Seg_5324" s="T557">as</ta>
            <ta e="T559" id="Seg_5325" s="T558">even</ta>
            <ta e="T560" id="Seg_5326" s="T559">1SG.[NOM]</ta>
            <ta e="T561" id="Seg_5327" s="T560">language-ACC</ta>
            <ta e="T562" id="Seg_5328" s="T561">NEG</ta>
            <ta e="T563" id="Seg_5329" s="T562">can-CVB.SEQ</ta>
            <ta e="T564" id="Seg_5330" s="T563">hear-EP-NEG-1SG</ta>
            <ta e="T565" id="Seg_5331" s="T564">be-PST1-1SG</ta>
            <ta e="T566" id="Seg_5332" s="T565">when</ta>
            <ta e="T567" id="Seg_5333" s="T566">that</ta>
            <ta e="T568" id="Seg_5334" s="T567">1SG.[NOM]</ta>
            <ta e="T569" id="Seg_5335" s="T568">selsoviet.[NOM]</ta>
            <ta e="T570" id="Seg_5336" s="T569">be-TEMP-1SG</ta>
            <ta e="T571" id="Seg_5337" s="T570">oh</ta>
            <ta e="T572" id="Seg_5338" s="T571">what.[NOM]</ta>
            <ta e="T573" id="Seg_5339" s="T572">INDEF</ta>
            <ta e="T574" id="Seg_5340" s="T573">interpreter-EP-INSTR</ta>
            <ta e="T575" id="Seg_5341" s="T574">one</ta>
            <ta e="T576" id="Seg_5342" s="T575">INDEF</ta>
            <ta e="T577" id="Seg_5343" s="T576">word-DIM-ACC</ta>
            <ta e="T578" id="Seg_5344" s="T577">some</ta>
            <ta e="T579" id="Seg_5345" s="T578">human.being.[NOM]</ta>
            <ta e="T580" id="Seg_5346" s="T579">word-3SG-INSTR</ta>
            <ta e="T581" id="Seg_5347" s="T580">change-PTCP.HAB</ta>
            <ta e="T582" id="Seg_5348" s="T581">be-PST1-1PL</ta>
            <ta e="T583" id="Seg_5349" s="T582">interpreter-EP-INSTR</ta>
            <ta e="T584" id="Seg_5350" s="T583">that</ta>
            <ta e="T585" id="Seg_5351" s="T584">pain-PROPR.[NOM]</ta>
            <ta e="T586" id="Seg_5352" s="T585">be-PST1-3SG</ta>
            <ta e="T587" id="Seg_5353" s="T586">now</ta>
            <ta e="T588" id="Seg_5354" s="T587">pain-POSS</ta>
            <ta e="T589" id="Seg_5355" s="T588">NEG</ta>
            <ta e="T590" id="Seg_5356" s="T589">self-3PL.[NOM]</ta>
            <ta e="T591" id="Seg_5357" s="T590">face.[NOM]</ta>
            <ta e="T592" id="Seg_5358" s="T591">chat-PRS-3PL</ta>
            <ta e="T593" id="Seg_5359" s="T592">who-ACC</ta>
            <ta e="T594" id="Seg_5360" s="T593">INDEF</ta>
            <ta e="T595" id="Seg_5361" s="T594">with</ta>
            <ta e="T596" id="Seg_5362" s="T595">Soviet</ta>
            <ta e="T597" id="Seg_5363" s="T596">power.[NOM]</ta>
            <ta e="T598" id="Seg_5364" s="T597">good</ta>
            <ta e="T599" id="Seg_5365" s="T598">very</ta>
            <ta e="T600" id="Seg_5366" s="T599">life-DAT/LOC</ta>
            <ta e="T601" id="Seg_5367" s="T600">bring-PST1-3SG</ta>
            <ta e="T602" id="Seg_5368" s="T601">now</ta>
            <ta e="T603" id="Seg_5369" s="T602">Soviet</ta>
            <ta e="T604" id="Seg_5370" s="T603">power.[NOM]</ta>
            <ta e="T605" id="Seg_5371" s="T604">become-CVB.SEQ</ta>
            <ta e="T606" id="Seg_5372" s="T605">child-PL-1SG-DAT/LOC</ta>
            <ta e="T607" id="Seg_5373" s="T606">EMPH</ta>
            <ta e="T608" id="Seg_5374" s="T607">1SG.[NOM]</ta>
            <ta e="T609" id="Seg_5375" s="T608">EMPH</ta>
            <ta e="T610" id="Seg_5376" s="T609">so.much-1SG-DAT/LOC</ta>
            <ta e="T611" id="Seg_5377" s="T610">reach-PST1-1SG</ta>
            <ta e="T612" id="Seg_5378" s="T611">now</ta>
            <ta e="T613" id="Seg_5379" s="T612">very</ta>
            <ta e="T614" id="Seg_5380" s="T613">good-ADVZ</ta>
            <ta e="T615" id="Seg_5381" s="T614">so.much</ta>
            <ta e="T616" id="Seg_5382" s="T615">so.much</ta>
            <ta e="T617" id="Seg_5383" s="T616">age-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T618" id="Seg_5384" s="T617">reach-PST1-1SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_5385" s="T0">sowjetisch</ta>
            <ta e="T2" id="Seg_5386" s="T1">Macht.[NOM]</ta>
            <ta e="T3" id="Seg_5387" s="T2">werden-PTCP.PRS-3SG-ACC</ta>
            <ta e="T4" id="Seg_5388" s="T3">EMPH</ta>
            <ta e="T5" id="Seg_5389" s="T4">noch</ta>
            <ta e="T6" id="Seg_5390" s="T5">EMPH</ta>
            <ta e="T7" id="Seg_5391" s="T6">erinnern-NEG-1SG</ta>
            <ta e="T8" id="Seg_5392" s="T7">dann</ta>
            <ta e="T9" id="Seg_5393" s="T8">1SG.[NOM]</ta>
            <ta e="T10" id="Seg_5394" s="T9">EMPH</ta>
            <ta e="T11" id="Seg_5395" s="T10">ganz.und.gar</ta>
            <ta e="T12" id="Seg_5396" s="T11">Kind.[NOM]</ta>
            <ta e="T13" id="Seg_5397" s="T12">sein-PST1-1SG</ta>
            <ta e="T14" id="Seg_5398" s="T13">was.für.ein</ta>
            <ta e="T15" id="Seg_5399" s="T14">NEG</ta>
            <ta e="T16" id="Seg_5400" s="T15">Macht-DAT/LOC</ta>
            <ta e="T17" id="Seg_5401" s="T16">arbeiten-NEG.PTCP</ta>
            <ta e="T18" id="Seg_5402" s="T17">Kind.[NOM]</ta>
            <ta e="T19" id="Seg_5403" s="T18">sein-PST1-1SG</ta>
            <ta e="T20" id="Seg_5404" s="T19">sowjetisch</ta>
            <ta e="T21" id="Seg_5405" s="T20">Macht.[NOM]</ta>
            <ta e="T22" id="Seg_5406" s="T21">werden-PRS.[3SG]</ta>
            <ta e="T23" id="Seg_5407" s="T22">sagen-CVB.SEQ</ta>
            <ta e="T26" id="Seg_5408" s="T25">Gesetz.[NOM]</ta>
            <ta e="T27" id="Seg_5409" s="T26">sich.drehen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T30" id="Seg_5410" s="T29">doch</ta>
            <ta e="T31" id="Seg_5411" s="T30">EMPH</ta>
            <ta e="T32" id="Seg_5412" s="T31">verstehen-PTCP.PRS</ta>
            <ta e="T33" id="Seg_5413" s="T32">Mensch.[NOM]</ta>
            <ta e="T34" id="Seg_5414" s="T33">verstehen-PTCP.PRS.[NOM]</ta>
            <ta e="T35" id="Seg_5415" s="T34">ähnlich</ta>
            <ta e="T36" id="Seg_5416" s="T35">sein-PST1-3SG</ta>
            <ta e="T37" id="Seg_5417" s="T36">Gesetz.[NOM]</ta>
            <ta e="T38" id="Seg_5418" s="T37">sich.drehen-NMNZ-3SG-ACC</ta>
            <ta e="T39" id="Seg_5419" s="T38">als</ta>
            <ta e="T40" id="Seg_5420" s="T39">zuerst</ta>
            <ta e="T41" id="Seg_5421" s="T40">Mensch-ACC</ta>
            <ta e="T42" id="Seg_5422" s="T41">früher</ta>
            <ta e="T43" id="Seg_5423" s="T42">reich</ta>
            <ta e="T44" id="Seg_5424" s="T43">Mensch.[NOM]</ta>
            <ta e="T45" id="Seg_5425" s="T44">Kulake.[NOM]</ta>
            <ta e="T46" id="Seg_5426" s="T45">Mensch.[NOM]</ta>
            <ta e="T47" id="Seg_5427" s="T46">folgen-ITER-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_5428" s="T47">kurz-ACC</ta>
            <ta e="T49" id="Seg_5429" s="T48">schmal-ACC</ta>
            <ta e="T50" id="Seg_5430" s="T49">arm-ACC</ta>
            <ta e="T51" id="Seg_5431" s="T50">sagen-CVB.SEQ</ta>
            <ta e="T52" id="Seg_5432" s="T51">Name-VBZ-PTCP.PRS</ta>
            <ta e="T53" id="Seg_5433" s="T52">sein-PST1-3PL</ta>
            <ta e="T54" id="Seg_5434" s="T53">dann</ta>
            <ta e="T55" id="Seg_5435" s="T54">MOD</ta>
            <ta e="T56" id="Seg_5436" s="T55">1SG.[NOM]</ta>
            <ta e="T57" id="Seg_5437" s="T56">dieses</ta>
            <ta e="T58" id="Seg_5438" s="T57">reich</ta>
            <ta e="T59" id="Seg_5439" s="T58">Mensch-PL-DAT/LOC</ta>
            <ta e="T60" id="Seg_5440" s="T59">Holz-AG-DIM.[NOM]</ta>
            <ta e="T61" id="Seg_5441" s="T60">sein-PST1-1SG</ta>
            <ta e="T64" id="Seg_5442" s="T63">Rentier-AG.[NOM]</ta>
            <ta e="T65" id="Seg_5443" s="T64">sein-PST1-1SG</ta>
            <ta e="T66" id="Seg_5444" s="T65">jenes</ta>
            <ta e="T67" id="Seg_5445" s="T66">jene.Seite.[NOM]</ta>
            <ta e="T68" id="Seg_5446" s="T67">Seite-3SG-DAT/LOC</ta>
            <ta e="T69" id="Seg_5447" s="T68">dann</ta>
            <ta e="T70" id="Seg_5448" s="T69">verstehen-CVB.SEQ</ta>
            <ta e="T71" id="Seg_5449" s="T70">gehen-PST1-1SG</ta>
            <ta e="T72" id="Seg_5450" s="T71">erfahren-CVB.SEQ</ta>
            <ta e="T73" id="Seg_5451" s="T72">gehen-PST1-1SG</ta>
            <ta e="T74" id="Seg_5452" s="T73">natürlich</ta>
            <ta e="T75" id="Seg_5453" s="T74">Wahrheit.[NOM]</ta>
            <ta e="T76" id="Seg_5454" s="T75">Gesetz.[NOM]</ta>
            <ta e="T77" id="Seg_5455" s="T76">sich.ändern-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_5456" s="T77">sein-PST2.[3SG]</ta>
            <ta e="T79" id="Seg_5457" s="T78">früher</ta>
            <ta e="T80" id="Seg_5458" s="T79">zaristisch</ta>
            <ta e="T81" id="Seg_5459" s="T80">Gesetz.[NOM]</ta>
            <ta e="T82" id="Seg_5460" s="T81">doch</ta>
            <ta e="T83" id="Seg_5461" s="T82">aufhören-PST1-3SG</ta>
            <ta e="T84" id="Seg_5462" s="T83">sagen-PST2-3PL</ta>
            <ta e="T85" id="Seg_5463" s="T84">dann</ta>
            <ta e="T86" id="Seg_5464" s="T85">später</ta>
            <ta e="T87" id="Seg_5465" s="T86">Ende-EP-ABL</ta>
            <ta e="T88" id="Seg_5466" s="T87">Schule-DAT/LOC</ta>
            <ta e="T89" id="Seg_5467" s="T88">lernen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T90" id="Seg_5468" s="T89">sagen-CVB.SEQ</ta>
            <ta e="T91" id="Seg_5469" s="T90">Genosse.[NOM]</ta>
            <ta e="T92" id="Seg_5470" s="T91">Lenin.[NOM]</ta>
            <ta e="T93" id="Seg_5471" s="T92">Großvater.[NOM]</ta>
            <ta e="T94" id="Seg_5472" s="T93">sagen-CVB.SEQ</ta>
            <ta e="T95" id="Seg_5473" s="T94">sein-PST1-3SG</ta>
            <ta e="T96" id="Seg_5474" s="T95">Lenin.[NOM]</ta>
            <ta e="T97" id="Seg_5475" s="T96">Gesetz-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_5476" s="T97">doch</ta>
            <ta e="T99" id="Seg_5477" s="T98">sich.ändern-CVB.SEQ-sich.ändern-CVB.SEQ</ta>
            <ta e="T100" id="Seg_5478" s="T99">jenes</ta>
            <ta e="T101" id="Seg_5479" s="T100">sich.ändern-CVB.SEQ</ta>
            <ta e="T102" id="Seg_5480" s="T101">gehen-PST1-3SG</ta>
            <ta e="T103" id="Seg_5481" s="T102">Ende-EP-ABL</ta>
            <ta e="T104" id="Seg_5482" s="T103">Mensch.[NOM]</ta>
            <ta e="T105" id="Seg_5483" s="T104">jeder-3SG.[NOM]</ta>
            <ta e="T106" id="Seg_5484" s="T105">kurz</ta>
            <ta e="T107" id="Seg_5485" s="T106">schmal</ta>
            <ta e="T108" id="Seg_5486" s="T107">Kind-3SG.[NOM]</ta>
            <ta e="T109" id="Seg_5487" s="T108">was.für.ein</ta>
            <ta e="T110" id="Seg_5488" s="T109">INDEF</ta>
            <ta e="T111" id="Seg_5489" s="T110">Volk.[NOM]</ta>
            <ta e="T112" id="Seg_5490" s="T111">was.für.ein</ta>
            <ta e="T113" id="Seg_5491" s="T112">EMPH</ta>
            <ta e="T114" id="Seg_5492" s="T113">kurz</ta>
            <ta e="T115" id="Seg_5493" s="T114">schmal</ta>
            <ta e="T116" id="Seg_5494" s="T115">arm</ta>
            <ta e="T117" id="Seg_5495" s="T116">Mensch.[NOM]</ta>
            <ta e="T118" id="Seg_5496" s="T117">Kind-3SG.[NOM]</ta>
            <ta e="T119" id="Seg_5497" s="T118">jeder-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_5498" s="T119">Ausbildung-DAT/LOC</ta>
            <ta e="T121" id="Seg_5499" s="T120">und</ta>
            <ta e="T122" id="Seg_5500" s="T121">doch</ta>
            <ta e="T123" id="Seg_5501" s="T122">anfangen-VBZ-PST1-3SG</ta>
            <ta e="T124" id="Seg_5502" s="T123">oh</ta>
            <ta e="T125" id="Seg_5503" s="T124">jenes.EMPH.[NOM]</ta>
            <ta e="T126" id="Seg_5504" s="T125">trotz</ta>
            <ta e="T127" id="Seg_5505" s="T126">reich-ABL</ta>
            <ta e="T128" id="Seg_5506" s="T127">essen-CVB.SIM</ta>
            <ta e="T129" id="Seg_5507" s="T128">lernen-PTCP.PST-PL.[NOM]</ta>
            <ta e="T130" id="Seg_5508" s="T129">Kulake-ABL</ta>
            <ta e="T131" id="Seg_5509" s="T130">zu.Hilfe.kommen-EP-REFL-CVB.SIM</ta>
            <ta e="T132" id="Seg_5510" s="T131">lernen-PTCP.PST-PL.[NOM]</ta>
            <ta e="T133" id="Seg_5511" s="T132">doch</ta>
            <ta e="T134" id="Seg_5512" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_5513" s="T134">Schule-DAT/LOC</ta>
            <ta e="T136" id="Seg_5514" s="T135">geben-PTCP.FUT-1PL-ACC</ta>
            <ta e="T137" id="Seg_5515" s="T136">sagen-NEG.PTCP</ta>
            <ta e="T138" id="Seg_5516" s="T137">sein-PST1-3PL</ta>
            <ta e="T139" id="Seg_5517" s="T138">Kind-ACC</ta>
            <ta e="T140" id="Seg_5518" s="T139">dann</ta>
            <ta e="T141" id="Seg_5519" s="T140">werden-PST2-3SG</ta>
            <ta e="T142" id="Seg_5520" s="T141">Kolchose.[NOM]</ta>
            <ta e="T143" id="Seg_5521" s="T142">sein-PTCP.FUT-DAT/LOC</ta>
            <ta e="T144" id="Seg_5522" s="T143">denken-CVB.SEQ</ta>
            <ta e="T145" id="Seg_5523" s="T144">allgemein</ta>
            <ta e="T146" id="Seg_5524" s="T145">Wirtschaft-INSTR</ta>
            <ta e="T147" id="Seg_5525" s="T146">Leute.[NOM]</ta>
            <ta e="T148" id="Seg_5526" s="T147">Leben-3SG-ACC</ta>
            <ta e="T149" id="Seg_5527" s="T148">aufbauen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T150" id="Seg_5528" s="T149">sagen-CVB.SEQ-3PL</ta>
            <ta e="T151" id="Seg_5529" s="T150">nein</ta>
            <ta e="T152" id="Seg_5530" s="T151">jenes-DAT/LOC</ta>
            <ta e="T153" id="Seg_5531" s="T152">auch</ta>
            <ta e="T154" id="Seg_5532" s="T153">sein-CVB.SIM</ta>
            <ta e="T155" id="Seg_5533" s="T154">ziehen-PST2.NEG-3PL</ta>
            <ta e="T156" id="Seg_5534" s="T155">wie.viel</ta>
            <ta e="T157" id="Seg_5535" s="T156">INDEF</ta>
            <ta e="T158" id="Seg_5536" s="T157">Jahr-EP-INSTR</ta>
            <ta e="T159" id="Seg_5537" s="T158">sein</ta>
            <ta e="T160" id="Seg_5538" s="T159">Kolchose.[NOM]</ta>
            <ta e="T161" id="Seg_5539" s="T160">werden-PST2.NEG-3PL</ta>
            <ta e="T162" id="Seg_5540" s="T161">Kolchose.[NOM]</ta>
            <ta e="T163" id="Seg_5541" s="T162">sein-CVB.SEQ-1PL</ta>
            <ta e="T164" id="Seg_5542" s="T163">1PL.[NOM]</ta>
            <ta e="T165" id="Seg_5543" s="T164">vermögen-FUT-1PL</ta>
            <ta e="T166" id="Seg_5544" s="T165">NEG.[3SG]</ta>
            <ta e="T167" id="Seg_5545" s="T166">sagen-PTCP.PRS</ta>
            <ta e="T168" id="Seg_5546" s="T167">sein-PST1-3PL</ta>
            <ta e="T169" id="Seg_5547" s="T168">früher</ta>
            <ta e="T170" id="Seg_5548" s="T169">zaristisch</ta>
            <ta e="T171" id="Seg_5549" s="T170">Gesetz-DAT/LOC</ta>
            <ta e="T172" id="Seg_5550" s="T171">geboren.werden-PTCP.PST</ta>
            <ta e="T173" id="Seg_5551" s="T172">Leute.[NOM]</ta>
            <ta e="T174" id="Seg_5552" s="T173">lernen-PTCP.PST</ta>
            <ta e="T175" id="Seg_5553" s="T174">Leute.[NOM]</ta>
            <ta e="T176" id="Seg_5554" s="T175">dann</ta>
            <ta e="T177" id="Seg_5555" s="T176">wie</ta>
            <ta e="T178" id="Seg_5556" s="T177">INDEF</ta>
            <ta e="T180" id="Seg_5557" s="T178">machen-CVB.SEQ</ta>
            <ta e="T181" id="Seg_5558" s="T180">erzählen-CVB.SEQ</ta>
            <ta e="T182" id="Seg_5559" s="T181">erzählen-CVB.SEQ-3PL</ta>
            <ta e="T183" id="Seg_5560" s="T182">Gesetz.[NOM]</ta>
            <ta e="T184" id="Seg_5561" s="T183">kommen-CVB.SEQ-kommen-CVB.SEQ</ta>
            <ta e="T185" id="Seg_5562" s="T184">dieses.EMPH</ta>
            <ta e="T186" id="Seg_5563" s="T185">Land-PL-DAT/LOC</ta>
            <ta e="T187" id="Seg_5564" s="T186">kommen-CVB.SEQ</ta>
            <ta e="T188" id="Seg_5565" s="T187">kommen-CVB.SEQ</ta>
            <ta e="T189" id="Seg_5566" s="T188">Kolchose.[NOM]</ta>
            <ta e="T190" id="Seg_5567" s="T189">sein-PST1-1PL</ta>
            <ta e="T191" id="Seg_5568" s="T190">dreißig</ta>
            <ta e="T192" id="Seg_5569" s="T191">sieben-ORD-PROPR-EP-ABL</ta>
            <ta e="T193" id="Seg_5570" s="T192">dreißig</ta>
            <ta e="T194" id="Seg_5571" s="T193">acht-PROPR-DAT/LOC</ta>
            <ta e="T198" id="Seg_5572" s="T196">Wahrheit.[NOM]</ta>
            <ta e="T199" id="Seg_5573" s="T198">doch</ta>
            <ta e="T200" id="Seg_5574" s="T199">Kolchose.[NOM]</ta>
            <ta e="T201" id="Seg_5575" s="T200">EMPH</ta>
            <ta e="T202" id="Seg_5576" s="T201">anfangen-VBZ-CVB.SEQ</ta>
            <ta e="T203" id="Seg_5577" s="T202">einrichten-VBZ-CVB.SEQ</ta>
            <ta e="T204" id="Seg_5578" s="T203">bleiben</ta>
            <ta e="T205" id="Seg_5579" s="T204">Ende-EP-ABL</ta>
            <ta e="T206" id="Seg_5580" s="T205">dann</ta>
            <ta e="T207" id="Seg_5581" s="T206">Kolchose-ACC</ta>
            <ta e="T208" id="Seg_5582" s="T207">gebären-EP-MED-PST2-1PL</ta>
            <ta e="T209" id="Seg_5583" s="T208">sein-PST1-3SG</ta>
            <ta e="T210" id="Seg_5584" s="T209">1PL.[NOM]</ta>
            <ta e="T211" id="Seg_5585" s="T210">wo</ta>
            <ta e="T212" id="Seg_5586" s="T211">mehr</ta>
            <ta e="T213" id="Seg_5587" s="T212">Rentier-PROPR-ABL</ta>
            <ta e="T214" id="Seg_5588" s="T213">jenes-DAT/LOC</ta>
            <ta e="T215" id="Seg_5589" s="T214">Kolchose-DAT/LOC</ta>
            <ta e="T216" id="Seg_5590" s="T215">halten-PST2-1PL</ta>
            <ta e="T217" id="Seg_5591" s="T216">wo</ta>
            <ta e="T218" id="Seg_5592" s="T217">Totfalle-PROPR-1PL-ACC</ta>
            <ta e="T219" id="Seg_5593" s="T218">EMPH-dann</ta>
            <ta e="T220" id="Seg_5594" s="T219">selbst-1PL.[NOM]</ta>
            <ta e="T221" id="Seg_5595" s="T220">Totfalle-1PL-INSTR</ta>
            <ta e="T222" id="Seg_5596" s="T221">selbst-1PL.[NOM]</ta>
            <ta e="T223" id="Seg_5597" s="T222">Fangeisen-1PL-INSTR</ta>
            <ta e="T224" id="Seg_5598" s="T223">Ende-EP-ABL</ta>
            <ta e="T225" id="Seg_5599" s="T224">Kolchose.[NOM]</ta>
            <ta e="T226" id="Seg_5600" s="T225">Leben-3SG-ACC</ta>
            <ta e="T227" id="Seg_5601" s="T226">bauen-CVB.SEQ-1PL</ta>
            <ta e="T228" id="Seg_5602" s="T227">jetzt</ta>
            <ta e="T229" id="Seg_5603" s="T228">dieses</ta>
            <ta e="T230" id="Seg_5604" s="T229">Zeit-PL-DAT/LOC</ta>
            <ta e="T231" id="Seg_5605" s="T230">ankommen-PST1-1PL</ta>
            <ta e="T232" id="Seg_5606" s="T231">doch</ta>
            <ta e="T233" id="Seg_5607" s="T232">doch</ta>
            <ta e="T234" id="Seg_5608" s="T233">dann</ta>
            <ta e="T235" id="Seg_5609" s="T234">Kolchose.[NOM]</ta>
            <ta e="T236" id="Seg_5610" s="T235">Leben-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_5611" s="T236">gut.[NOM]</ta>
            <ta e="T238" id="Seg_5612" s="T237">MOD</ta>
            <ta e="T239" id="Seg_5613" s="T238">sagen-CVB.SEQ</ta>
            <ta e="T240" id="Seg_5614" s="T239">jenes.[NOM]</ta>
            <ta e="T241" id="Seg_5615" s="T240">ähnlich</ta>
            <ta e="T242" id="Seg_5616" s="T241">MOD</ta>
            <ta e="T243" id="Seg_5617" s="T242">sein-PTCP.PST-EP-INSTR</ta>
            <ta e="T244" id="Seg_5618" s="T243">doch</ta>
            <ta e="T245" id="Seg_5619" s="T244">Kolchose.[NOM]</ta>
            <ta e="T246" id="Seg_5620" s="T245">sein-PST2-1PL</ta>
            <ta e="T247" id="Seg_5621" s="T246">jenes</ta>
            <ta e="T248" id="Seg_5622" s="T247">jene.Seite.[NOM]</ta>
            <ta e="T249" id="Seg_5623" s="T248">Seite-3SG-DAT/LOC</ta>
            <ta e="T250" id="Seg_5624" s="T249">wieder</ta>
            <ta e="T251" id="Seg_5625" s="T250">EMPH</ta>
            <ta e="T252" id="Seg_5626" s="T251">Qual-PROPR</ta>
            <ta e="T253" id="Seg_5627" s="T252">Bedarf-PROPR</ta>
            <ta e="T254" id="Seg_5628" s="T253">Zeit-PL.[NOM]</ta>
            <ta e="T255" id="Seg_5629" s="T254">sein-PTCP.PRS</ta>
            <ta e="T256" id="Seg_5630" s="T255">sein-PST1-3PL</ta>
            <ta e="T257" id="Seg_5631" s="T256">dieses</ta>
            <ta e="T258" id="Seg_5632" s="T257">Kulake-DAT/LOC</ta>
            <ta e="T259" id="Seg_5633" s="T258">früher</ta>
            <ta e="T260" id="Seg_5634" s="T259">zaristisch-DAT/LOC</ta>
            <ta e="T261" id="Seg_5635" s="T260">Kopf-PL-3SG-ACC</ta>
            <ta e="T262" id="Seg_5636" s="T261">verstreuen-CAUS-PTCP.PST</ta>
            <ta e="T263" id="Seg_5637" s="T262">Mensch-PL.[NOM]</ta>
            <ta e="T265" id="Seg_5638" s="T264">Bandit-VBZ-PTCP.PRS</ta>
            <ta e="T266" id="Seg_5639" s="T265">und</ta>
            <ta e="T267" id="Seg_5640" s="T266">sein-PST1-3PL</ta>
            <ta e="T268" id="Seg_5641" s="T267">jenes-PL.[NOM]</ta>
            <ta e="T269" id="Seg_5642" s="T268">doch</ta>
            <ta e="T270" id="Seg_5643" s="T269">Qual-ACC</ta>
            <ta e="T271" id="Seg_5644" s="T270">machen-PTCP.HAB</ta>
            <ta e="T272" id="Seg_5645" s="T271">sein-PST1-3PL</ta>
            <ta e="T273" id="Seg_5646" s="T272">jenes</ta>
            <ta e="T274" id="Seg_5647" s="T273">Bandit-ACC</ta>
            <ta e="T275" id="Seg_5648" s="T274">nein</ta>
            <ta e="T276" id="Seg_5649" s="T275">jenes</ta>
            <ta e="T277" id="Seg_5650" s="T276">Rang-ACC</ta>
            <ta e="T278" id="Seg_5651" s="T277">sowjetisch</ta>
            <ta e="T279" id="Seg_5652" s="T278">Macht.[NOM]</ta>
            <ta e="T280" id="Seg_5653" s="T279">Vorderteil.[NOM]</ta>
            <ta e="T281" id="Seg_5654" s="T280">zu</ta>
            <ta e="T282" id="Seg_5655" s="T281">schreiten-CVB.SEQ</ta>
            <ta e="T283" id="Seg_5656" s="T282">gehen-CVB.SEQ</ta>
            <ta e="T284" id="Seg_5657" s="T283">jenes</ta>
            <ta e="T285" id="Seg_5658" s="T284">Bandit-ACC</ta>
            <ta e="T286" id="Seg_5659" s="T285">doch</ta>
            <ta e="T287" id="Seg_5660" s="T286">EMPH</ta>
            <ta e="T288" id="Seg_5661" s="T287">säubern-PTCP.HAB</ta>
            <ta e="T289" id="Seg_5662" s="T288">sein-PST1-3SG</ta>
            <ta e="T290" id="Seg_5663" s="T289">in.Ordnung.bringen-PTCP.HAB</ta>
            <ta e="T291" id="Seg_5664" s="T290">und</ta>
            <ta e="T292" id="Seg_5665" s="T291">sein-PST1-3SG</ta>
            <ta e="T293" id="Seg_5666" s="T292">doch</ta>
            <ta e="T294" id="Seg_5667" s="T293">jenes</ta>
            <ta e="T295" id="Seg_5668" s="T294">aufhören-PTCP.PST-3SG-ACC</ta>
            <ta e="T296" id="Seg_5669" s="T295">nachdem</ta>
            <ta e="T297" id="Seg_5670" s="T296">Ende-EP-ABL</ta>
            <ta e="T298" id="Seg_5671" s="T297">dieses</ta>
            <ta e="T299" id="Seg_5672" s="T298">Deutschland.[NOM]</ta>
            <ta e="T300" id="Seg_5673" s="T299">Krieg-3SG.[NOM]</ta>
            <ta e="T301" id="Seg_5674" s="T300">werden-PST2-3SG</ta>
            <ta e="T302" id="Seg_5675" s="T301">doch</ta>
            <ta e="T303" id="Seg_5676" s="T302">dieses-DAT/LOC</ta>
            <ta e="T304" id="Seg_5677" s="T303">beunruhigen-CVB.SIM</ta>
            <ta e="T305" id="Seg_5678" s="T304">schlagen-PST2-1PL</ta>
            <ta e="T306" id="Seg_5679" s="T305">1PL.[NOM]</ta>
            <ta e="T307" id="Seg_5680" s="T306">sehr</ta>
            <ta e="T308" id="Seg_5681" s="T307">jenes</ta>
            <ta e="T309" id="Seg_5682" s="T308">Kleidung.[NOM]</ta>
            <ta e="T310" id="Seg_5683" s="T309">und</ta>
            <ta e="T311" id="Seg_5684" s="T310">sich.sammeln-NMNZ-3SG.[NOM]</ta>
            <ta e="T312" id="Seg_5685" s="T311">sein-PTCP.PRS</ta>
            <ta e="T313" id="Seg_5686" s="T312">sein-PST1-3SG</ta>
            <ta e="T314" id="Seg_5687" s="T313">jenes</ta>
            <ta e="T315" id="Seg_5688" s="T314">Geld-INSTR</ta>
            <ta e="T316" id="Seg_5689" s="T315">und</ta>
            <ta e="T317" id="Seg_5690" s="T316">Steuer.[NOM]</ta>
            <ta e="T318" id="Seg_5691" s="T317">sein-PTCP.PRS</ta>
            <ta e="T319" id="Seg_5692" s="T318">sein-PST1-3SG</ta>
            <ta e="T320" id="Seg_5693" s="T319">1PL.[NOM]</ta>
            <ta e="T321" id="Seg_5694" s="T320">jenes.[NOM]</ta>
            <ta e="T322" id="Seg_5695" s="T321">zu</ta>
            <ta e="T323" id="Seg_5696" s="T322">Stadt.[NOM]</ta>
            <ta e="T324" id="Seg_5697" s="T323">Ort-PL-DAT/LOC</ta>
            <ta e="T325" id="Seg_5698" s="T324">Armee-DAT/LOC</ta>
            <ta e="T327" id="Seg_5699" s="T325">helfen-CVB.SEQ-1PL</ta>
            <ta e="T328" id="Seg_5700" s="T327">dann</ta>
            <ta e="T329" id="Seg_5701" s="T328">Jahr.[NOM]</ta>
            <ta e="T330" id="Seg_5702" s="T329">aufhören-CVB.SEQ</ta>
            <ta e="T331" id="Seg_5703" s="T330">doch</ta>
            <ta e="T332" id="Seg_5704" s="T331">jetzt</ta>
            <ta e="T333" id="Seg_5705" s="T332">was.[NOM]</ta>
            <ta e="T334" id="Seg_5706" s="T333">NEG</ta>
            <ta e="T335" id="Seg_5707" s="T334">sein-FUT-1PL</ta>
            <ta e="T336" id="Seg_5708" s="T335">denken-NEG-1SG</ta>
            <ta e="T337" id="Seg_5709" s="T336">heutig</ta>
            <ta e="T338" id="Seg_5710" s="T337">Seele-1SG.[NOM]</ta>
            <ta e="T339" id="Seg_5711" s="T338">gut.[NOM]</ta>
            <ta e="T340" id="Seg_5712" s="T339">jetzt</ta>
            <ta e="T341" id="Seg_5713" s="T340">Leben.[NOM]</ta>
            <ta e="T342" id="Seg_5714" s="T341">besser.werden-PST1-3SG</ta>
            <ta e="T343" id="Seg_5715" s="T342">Leben.[NOM]</ta>
            <ta e="T344" id="Seg_5716" s="T343">wachsen-PST1-3SG</ta>
            <ta e="T345" id="Seg_5717" s="T344">jetzt</ta>
            <ta e="T346" id="Seg_5718" s="T345">ganz</ta>
            <ta e="T347" id="Seg_5719" s="T346">Kolchose-PL.[NOM]</ta>
            <ta e="T348" id="Seg_5720" s="T347">Ende-EP-ABL</ta>
            <ta e="T349" id="Seg_5721" s="T348">Organisation.[NOM]</ta>
            <ta e="T350" id="Seg_5722" s="T349">ähnlich</ta>
            <ta e="T351" id="Seg_5723" s="T350">werden-PST1-3PL</ta>
            <ta e="T352" id="Seg_5724" s="T351">jetzt</ta>
            <ta e="T353" id="Seg_5725" s="T352">von.hier</ta>
            <ta e="T354" id="Seg_5726" s="T353">Vorderteil.[NOM]</ta>
            <ta e="T355" id="Seg_5727" s="T354">Seite-3SG-ACC</ta>
            <ta e="T356" id="Seg_5728" s="T355">wissen-NEG-1SG</ta>
            <ta e="T357" id="Seg_5729" s="T356">selbst-1SG.[NOM]</ta>
            <ta e="T358" id="Seg_5730" s="T357">altern-CVB.SEQ</ta>
            <ta e="T359" id="Seg_5731" s="T358">bleiben-PST1-1SG</ta>
            <ta e="T362" id="Seg_5732" s="T361">1SG.[NOM]</ta>
            <ta e="T363" id="Seg_5733" s="T362">Gedanke-1SG-DAT/LOC</ta>
            <ta e="T364" id="Seg_5734" s="T363">gut</ta>
            <ta e="T365" id="Seg_5735" s="T364">Leben-DAT/LOC</ta>
            <ta e="T366" id="Seg_5736" s="T365">jetzt</ta>
            <ta e="T367" id="Seg_5737" s="T366">ankommen-PST1-1PL</ta>
            <ta e="T368" id="Seg_5738" s="T367">sagen-PRS-1SG</ta>
            <ta e="T369" id="Seg_5739" s="T368">dieses.EMPH</ta>
            <ta e="T370" id="Seg_5740" s="T369">altern-PTCP.PRS</ta>
            <ta e="T372" id="Seg_5741" s="T370">Alter-1SG-DAT/LOC</ta>
            <ta e="T373" id="Seg_5742" s="T372">jetzt</ta>
            <ta e="T374" id="Seg_5743" s="T373">satt.[NOM]</ta>
            <ta e="T375" id="Seg_5744" s="T374">sehr-1PL</ta>
            <ta e="T376" id="Seg_5745" s="T375">warm.[NOM]</ta>
            <ta e="T377" id="Seg_5746" s="T376">sehr-1PL</ta>
            <ta e="T378" id="Seg_5747" s="T377">Kleidung-ACC</ta>
            <ta e="T379" id="Seg_5748" s="T378">und</ta>
            <ta e="T380" id="Seg_5749" s="T379">sich.anziehen-PRS-1PL</ta>
            <ta e="T381" id="Seg_5750" s="T380">Nahrung-ACC</ta>
            <ta e="T382" id="Seg_5751" s="T381">und</ta>
            <ta e="T383" id="Seg_5752" s="T382">essen-PRS-1PL</ta>
            <ta e="T384" id="Seg_5753" s="T383">dieses</ta>
            <ta e="T385" id="Seg_5754" s="T384">nun</ta>
            <ta e="T386" id="Seg_5755" s="T385">zuerst</ta>
            <ta e="T387" id="Seg_5756" s="T386">mein</ta>
            <ta e="T388" id="Seg_5757" s="T387">dieses</ta>
            <ta e="T389" id="Seg_5758" s="T388">groß</ta>
            <ta e="T390" id="Seg_5759" s="T389">Kind-1SG.[NOM]</ta>
            <ta e="T391" id="Seg_5760" s="T390">dieses</ta>
            <ta e="T392" id="Seg_5761" s="T391">Vasilij.[NOM]</ta>
            <ta e="T393" id="Seg_5762" s="T392">es.gibt</ta>
            <ta e="T394" id="Seg_5763" s="T393">dieses</ta>
            <ta e="T395" id="Seg_5764" s="T394">Kind-1SG.[NOM]</ta>
            <ta e="T396" id="Seg_5765" s="T395">wachsen-PST1-3SG</ta>
            <ta e="T397" id="Seg_5766" s="T396">und</ta>
            <ta e="T398" id="Seg_5767" s="T397">lernen-PST2-3SG</ta>
            <ta e="T399" id="Seg_5768" s="T398">3SG.[NOM]</ta>
            <ta e="T400" id="Seg_5769" s="T399">Schule-3SG-ACC</ta>
            <ta e="T401" id="Seg_5770" s="T400">aufhören-EP-CAUS-PST1-3SG</ta>
            <ta e="T402" id="Seg_5771" s="T401">EMPH</ta>
            <ta e="T403" id="Seg_5772" s="T402">Lehrer-DAT/LOC</ta>
            <ta e="T404" id="Seg_5773" s="T403">lernen-PST2-3SG</ta>
            <ta e="T405" id="Seg_5774" s="T404">so</ta>
            <ta e="T406" id="Seg_5775" s="T405">jetzt</ta>
            <ta e="T407" id="Seg_5776" s="T406">Tag.[NOM]</ta>
            <ta e="T408" id="Seg_5777" s="T407">heute</ta>
            <ta e="T409" id="Seg_5778" s="T408">Lehrer-VBZ-CVB.SIM</ta>
            <ta e="T410" id="Seg_5779" s="T409">sitzen-PRS.[3SG]</ta>
            <ta e="T411" id="Seg_5780" s="T410">dann</ta>
            <ta e="T412" id="Seg_5781" s="T411">eins</ta>
            <ta e="T413" id="Seg_5782" s="T412">Tochter-PROPR-1SG</ta>
            <ta e="T414" id="Seg_5783" s="T413">jenes.[NOM]</ta>
            <ta e="T415" id="Seg_5784" s="T414">auch</ta>
            <ta e="T416" id="Seg_5785" s="T415">Ausbildung-3SG-ACC</ta>
            <ta e="T417" id="Seg_5786" s="T416">Schule-3SG-ACC</ta>
            <ta e="T418" id="Seg_5787" s="T417">aufhören-EP-CAUS-CVB.SEQ</ta>
            <ta e="T419" id="Seg_5788" s="T418">auch</ta>
            <ta e="T420" id="Seg_5789" s="T419">höher.[NOM]</ta>
            <ta e="T421" id="Seg_5790" s="T420">lernen-CVB.SEQ</ta>
            <ta e="T422" id="Seg_5791" s="T421">Doktor-VBZ-CVB.SEQ</ta>
            <ta e="T423" id="Seg_5792" s="T422">sitzen-PRS.[3SG]</ta>
            <ta e="T424" id="Seg_5793" s="T423">jetzt</ta>
            <ta e="T425" id="Seg_5794" s="T424">jenes</ta>
            <ta e="T426" id="Seg_5795" s="T425">selbst-3SG-GEN</ta>
            <ta e="T427" id="Seg_5796" s="T426">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T428" id="Seg_5797" s="T427">gut.[NOM]</ta>
            <ta e="T429" id="Seg_5798" s="T428">jenes.[NOM]</ta>
            <ta e="T430" id="Seg_5799" s="T429">nachdem</ta>
            <ta e="T431" id="Seg_5800" s="T430">1SG.[NOM]</ta>
            <ta e="T432" id="Seg_5801" s="T431">und</ta>
            <ta e="T433" id="Seg_5802" s="T432">Seele-1SG-DAT/LOC</ta>
            <ta e="T434" id="Seg_5803" s="T433">gut.[NOM]</ta>
            <ta e="T435" id="Seg_5804" s="T434">jetzt</ta>
            <ta e="T436" id="Seg_5805" s="T435">dieses</ta>
            <ta e="T437" id="Seg_5806" s="T436">Leben-DAT/LOC</ta>
            <ta e="T438" id="Seg_5807" s="T437">leben-NMNZ-3PL.[NOM]</ta>
            <ta e="T439" id="Seg_5808" s="T438">jetzt</ta>
            <ta e="T440" id="Seg_5809" s="T439">dieses</ta>
            <ta e="T441" id="Seg_5810" s="T440">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T442" id="Seg_5811" s="T441">jenes-DAT/LOC</ta>
            <ta e="T443" id="Seg_5812" s="T442">drei-ORD</ta>
            <ta e="T444" id="Seg_5813" s="T443">wieder</ta>
            <ta e="T445" id="Seg_5814" s="T444">Junge-EP-1SG.[NOM]</ta>
            <ta e="T446" id="Seg_5815" s="T445">Flughafen-DAT/LOC</ta>
            <ta e="T447" id="Seg_5816" s="T446">arbeiten-PRS.[3SG]</ta>
            <ta e="T448" id="Seg_5817" s="T447">dann</ta>
            <ta e="T449" id="Seg_5818" s="T448">meist</ta>
            <ta e="T450" id="Seg_5819" s="T449">klein.[NOM]</ta>
            <ta e="T451" id="Seg_5820" s="T450">Sohn-PROPR-1SG</ta>
            <ta e="T452" id="Seg_5821" s="T451">jetzt</ta>
            <ta e="T453" id="Seg_5822" s="T452">Ivan.[NOM]</ta>
            <ta e="T454" id="Seg_5823" s="T453">Antonov</ta>
            <ta e="T455" id="Seg_5824" s="T454">Ivan.[NOM]</ta>
            <ta e="T458" id="Seg_5825" s="T457">jenes</ta>
            <ta e="T459" id="Seg_5826" s="T458">MOD</ta>
            <ta e="T460" id="Seg_5827" s="T459">jetzt</ta>
            <ta e="T461" id="Seg_5828" s="T460">Kolchose-DAT/LOC</ta>
            <ta e="T462" id="Seg_5829" s="T461">arbeiten-PRS.[3SG]</ta>
            <ta e="T463" id="Seg_5830" s="T462">Kolchose.[NOM]</ta>
            <ta e="T464" id="Seg_5831" s="T463">EMPH</ta>
            <ta e="T465" id="Seg_5832" s="T464">arbeiten-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T466" id="Seg_5833" s="T465">Arbeit-3SG.[NOM]</ta>
            <ta e="T467" id="Seg_5834" s="T466">Arbeit-3SG-ACC</ta>
            <ta e="T468" id="Seg_5835" s="T467">und</ta>
            <ta e="T469" id="Seg_5836" s="T468">Arbeit-PROPR</ta>
            <ta e="T470" id="Seg_5837" s="T469">Kolchose.[NOM]</ta>
            <ta e="T471" id="Seg_5838" s="T470">doch</ta>
            <ta e="T472" id="Seg_5839" s="T471">EMPH</ta>
            <ta e="T473" id="Seg_5840" s="T472">selbst-3SG-GEN</ta>
            <ta e="T474" id="Seg_5841" s="T473">Kolchose.[NOM]</ta>
            <ta e="T475" id="Seg_5842" s="T474">Inneres-3SG-DAT/LOC</ta>
            <ta e="T476" id="Seg_5843" s="T475">doch</ta>
            <ta e="T477" id="Seg_5844" s="T476">gut-ADVZ</ta>
            <ta e="T478" id="Seg_5845" s="T477">sich.satt.essen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T479" id="Seg_5846" s="T478">sich.anziehen-CAUS-CVB.SEQ</ta>
            <ta e="T480" id="Seg_5847" s="T479">doch</ta>
            <ta e="T481" id="Seg_5848" s="T480">und</ta>
            <ta e="T482" id="Seg_5849" s="T481">arbeiten-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_5850" s="T482">doch</ta>
            <ta e="T484" id="Seg_5851" s="T483">und</ta>
            <ta e="T485" id="Seg_5852" s="T484">Lohn-PROPR.[NOM]</ta>
            <ta e="T486" id="Seg_5853" s="T485">Mensch.[NOM]</ta>
            <ta e="T487" id="Seg_5854" s="T486">wo</ta>
            <ta e="T488" id="Seg_5855" s="T487">gut-ADVZ</ta>
            <ta e="T489" id="Seg_5856" s="T488">arbeiten-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T490" id="Seg_5857" s="T489">jenes</ta>
            <ta e="T491" id="Seg_5858" s="T490">besser</ta>
            <ta e="T492" id="Seg_5859" s="T491">werden-PRS.[3SG]</ta>
            <ta e="T493" id="Seg_5860" s="T492">wo</ta>
            <ta e="T494" id="Seg_5861" s="T493">schlecht-ADVZ</ta>
            <ta e="T495" id="Seg_5862" s="T494">arbeiten-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T496" id="Seg_5863" s="T495">jenes</ta>
            <ta e="T497" id="Seg_5864" s="T496">niedriger</ta>
            <ta e="T498" id="Seg_5865" s="T497">werden-PRS.[3SG]</ta>
            <ta e="T499" id="Seg_5866" s="T498">1SG.[NOM]</ta>
            <ta e="T500" id="Seg_5867" s="T499">Gedanke-1SG-DAT/LOC</ta>
            <ta e="T501" id="Seg_5868" s="T500">EMPH-so</ta>
            <ta e="T502" id="Seg_5869" s="T501">dieses</ta>
            <ta e="T503" id="Seg_5870" s="T502">nun</ta>
            <ta e="T504" id="Seg_5871" s="T503">zuerst</ta>
            <ta e="T505" id="Seg_5872" s="T504">selbst-1SG.[NOM]</ta>
            <ta e="T506" id="Seg_5873" s="T505">doch</ta>
            <ta e="T507" id="Seg_5874" s="T506">EMPH</ta>
            <ta e="T508" id="Seg_5875" s="T507">dieses</ta>
            <ta e="T509" id="Seg_5876" s="T508">Selsowjet-EP-INSTR</ta>
            <ta e="T510" id="Seg_5877" s="T509">was-EP-INSTR</ta>
            <ta e="T511" id="Seg_5878" s="T510">leiten-VBZ-CVB.SEQ</ta>
            <ta e="T512" id="Seg_5879" s="T511">sitzen-PTCP.PRS</ta>
            <ta e="T513" id="Seg_5880" s="T512">sein-PST1-1SG</ta>
            <ta e="T514" id="Seg_5881" s="T513">dann</ta>
            <ta e="T515" id="Seg_5882" s="T514">jetzt</ta>
            <ta e="T516" id="Seg_5883" s="T515">jung</ta>
            <ta e="T517" id="Seg_5884" s="T516">Mensch-PL.[NOM]</ta>
            <ta e="T518" id="Seg_5885" s="T517">Ausbildung-DAT/LOC</ta>
            <ta e="T519" id="Seg_5886" s="T518">lernen-CVB.SEQ-3PL</ta>
            <ta e="T520" id="Seg_5887" s="T519">1SG-COMP</ta>
            <ta e="T521" id="Seg_5888" s="T520">besser</ta>
            <ta e="T522" id="Seg_5889" s="T521">arbeiten-CAP-3PL</ta>
            <ta e="T523" id="Seg_5890" s="T522">dann</ta>
            <ta e="T524" id="Seg_5891" s="T523">MOD</ta>
            <ta e="T525" id="Seg_5892" s="T524">wer-DAT/LOC</ta>
            <ta e="T526" id="Seg_5893" s="T525">EMPH</ta>
            <ta e="T527" id="Seg_5894" s="T526">Schuld.[NOM]</ta>
            <ta e="T528" id="Seg_5895" s="T527">Wort-POSS</ta>
            <ta e="T529" id="Seg_5896" s="T528">NEG</ta>
            <ta e="T530" id="Seg_5897" s="T529">jetzt</ta>
            <ta e="T531" id="Seg_5898" s="T530">sich.ausruhen-FUT-1SG</ta>
            <ta e="T532" id="Seg_5899" s="T531">sagen-CVB.SEQ-1SG</ta>
            <ta e="T533" id="Seg_5900" s="T532">Selsowjet-VBZ-CVB.SEQ</ta>
            <ta e="T534" id="Seg_5901" s="T533">aufhören-PST2-EP-1SG</ta>
            <ta e="T535" id="Seg_5902" s="T534">sein-PST1-3SG</ta>
            <ta e="T536" id="Seg_5903" s="T535">jetzt</ta>
            <ta e="T537" id="Seg_5904" s="T536">dieses</ta>
            <ta e="T538" id="Seg_5905" s="T537">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T539" id="Seg_5906" s="T538">Ausbildung-DAT/LOC</ta>
            <ta e="T540" id="Seg_5907" s="T539">lernen-CVB.SEQ-3PL</ta>
            <ta e="T541" id="Seg_5908" s="T540">gut-ADVZ</ta>
            <ta e="T544" id="Seg_5909" s="T543">leben-PRS-3PL</ta>
            <ta e="T545" id="Seg_5910" s="T544">dann</ta>
            <ta e="T546" id="Seg_5911" s="T545">jetzt</ta>
            <ta e="T547" id="Seg_5912" s="T546">sich.freuen-EP-REFL-CVB.SIM</ta>
            <ta e="T548" id="Seg_5913" s="T547">sitzen-PRS-1SG</ta>
            <ta e="T549" id="Seg_5914" s="T548">dieses</ta>
            <ta e="T550" id="Seg_5915" s="T549">altern-CVB.SEQ</ta>
            <ta e="T551" id="Seg_5916" s="T550">gehen-CVB.SEQ-1SG</ta>
            <ta e="T552" id="Seg_5917" s="T551">selbst-1SG.[NOM]</ta>
            <ta e="T553" id="Seg_5918" s="T552">Ausbildung-POSS</ta>
            <ta e="T554" id="Seg_5919" s="T553">NEG</ta>
            <ta e="T555" id="Seg_5920" s="T554">sehr</ta>
            <ta e="T556" id="Seg_5921" s="T555">sein-PST1-1SG</ta>
            <ta e="T557" id="Seg_5922" s="T556">jenes.[NOM]</ta>
            <ta e="T558" id="Seg_5923" s="T557">als</ta>
            <ta e="T559" id="Seg_5924" s="T558">sogar</ta>
            <ta e="T560" id="Seg_5925" s="T559">1SG.[NOM]</ta>
            <ta e="T561" id="Seg_5926" s="T560">Sprache-ACC</ta>
            <ta e="T562" id="Seg_5927" s="T561">NEG</ta>
            <ta e="T563" id="Seg_5928" s="T562">können-CVB.SEQ</ta>
            <ta e="T564" id="Seg_5929" s="T563">hören-EP-NEG-1SG</ta>
            <ta e="T565" id="Seg_5930" s="T564">sein-PST1-1SG</ta>
            <ta e="T566" id="Seg_5931" s="T565">wann</ta>
            <ta e="T567" id="Seg_5932" s="T566">jenes</ta>
            <ta e="T568" id="Seg_5933" s="T567">1SG.[NOM]</ta>
            <ta e="T569" id="Seg_5934" s="T568">Selsowjet.[NOM]</ta>
            <ta e="T570" id="Seg_5935" s="T569">sein-TEMP-1SG</ta>
            <ta e="T571" id="Seg_5936" s="T570">oh</ta>
            <ta e="T572" id="Seg_5937" s="T571">was.[NOM]</ta>
            <ta e="T573" id="Seg_5938" s="T572">INDEF</ta>
            <ta e="T574" id="Seg_5939" s="T573">Dolmetscher-EP-INSTR</ta>
            <ta e="T575" id="Seg_5940" s="T574">eins</ta>
            <ta e="T576" id="Seg_5941" s="T575">INDEF</ta>
            <ta e="T577" id="Seg_5942" s="T576">Wort-DIM-ACC</ta>
            <ta e="T578" id="Seg_5943" s="T577">einige</ta>
            <ta e="T579" id="Seg_5944" s="T578">Mensch.[NOM]</ta>
            <ta e="T580" id="Seg_5945" s="T579">Wort-3SG-INSTR</ta>
            <ta e="T581" id="Seg_5946" s="T580">tauschen-PTCP.HAB</ta>
            <ta e="T582" id="Seg_5947" s="T581">sein-PST1-1PL</ta>
            <ta e="T583" id="Seg_5948" s="T582">Dolmetscher-EP-INSTR</ta>
            <ta e="T584" id="Seg_5949" s="T583">jenes</ta>
            <ta e="T585" id="Seg_5950" s="T584">Qual-PROPR.[NOM]</ta>
            <ta e="T586" id="Seg_5951" s="T585">sein-PST1-3SG</ta>
            <ta e="T587" id="Seg_5952" s="T586">jetzt</ta>
            <ta e="T588" id="Seg_5953" s="T587">Qual-POSS</ta>
            <ta e="T589" id="Seg_5954" s="T588">NEG</ta>
            <ta e="T590" id="Seg_5955" s="T589">selbst-3PL.[NOM]</ta>
            <ta e="T591" id="Seg_5956" s="T590">Gesicht.[NOM]</ta>
            <ta e="T592" id="Seg_5957" s="T591">sich.unterhalten-PRS-3PL</ta>
            <ta e="T593" id="Seg_5958" s="T592">wer-ACC</ta>
            <ta e="T594" id="Seg_5959" s="T593">INDEF</ta>
            <ta e="T595" id="Seg_5960" s="T594">mit</ta>
            <ta e="T596" id="Seg_5961" s="T595">sowjetisch</ta>
            <ta e="T597" id="Seg_5962" s="T596">Macht.[NOM]</ta>
            <ta e="T598" id="Seg_5963" s="T597">gut</ta>
            <ta e="T599" id="Seg_5964" s="T598">sehr</ta>
            <ta e="T600" id="Seg_5965" s="T599">Leben-DAT/LOC</ta>
            <ta e="T601" id="Seg_5966" s="T600">bringen-PST1-3SG</ta>
            <ta e="T602" id="Seg_5967" s="T601">jetzt</ta>
            <ta e="T603" id="Seg_5968" s="T602">sowjetisch</ta>
            <ta e="T604" id="Seg_5969" s="T603">Macht.[NOM]</ta>
            <ta e="T605" id="Seg_5970" s="T604">werden-CVB.SEQ</ta>
            <ta e="T606" id="Seg_5971" s="T605">Kind-PL-1SG-DAT/LOC</ta>
            <ta e="T607" id="Seg_5972" s="T606">EMPH</ta>
            <ta e="T608" id="Seg_5973" s="T607">1SG.[NOM]</ta>
            <ta e="T609" id="Seg_5974" s="T608">EMPH</ta>
            <ta e="T610" id="Seg_5975" s="T609">so.viel-1SG-DAT/LOC</ta>
            <ta e="T611" id="Seg_5976" s="T610">ankommen-PST1-1SG</ta>
            <ta e="T612" id="Seg_5977" s="T611">jetzt</ta>
            <ta e="T613" id="Seg_5978" s="T612">sehr</ta>
            <ta e="T614" id="Seg_5979" s="T613">gut-ADVZ</ta>
            <ta e="T615" id="Seg_5980" s="T614">so.viel</ta>
            <ta e="T616" id="Seg_5981" s="T615">so.viel</ta>
            <ta e="T617" id="Seg_5982" s="T616">altern-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T618" id="Seg_5983" s="T617">ankommen-PST1-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_5984" s="T0">советский</ta>
            <ta e="T2" id="Seg_5985" s="T1">власть.[NOM]</ta>
            <ta e="T3" id="Seg_5986" s="T2">становиться-PTCP.PRS-3SG-ACC</ta>
            <ta e="T4" id="Seg_5987" s="T3">EMPH</ta>
            <ta e="T5" id="Seg_5988" s="T4">еще</ta>
            <ta e="T6" id="Seg_5989" s="T5">EMPH</ta>
            <ta e="T7" id="Seg_5990" s="T6">помнить-NEG-1SG</ta>
            <ta e="T8" id="Seg_5991" s="T7">тогда</ta>
            <ta e="T9" id="Seg_5992" s="T8">1SG.[NOM]</ta>
            <ta e="T10" id="Seg_5993" s="T9">EMPH</ta>
            <ta e="T11" id="Seg_5994" s="T10">совсем</ta>
            <ta e="T12" id="Seg_5995" s="T11">ребенок.[NOM]</ta>
            <ta e="T13" id="Seg_5996" s="T12">быть-PST1-1SG</ta>
            <ta e="T14" id="Seg_5997" s="T13">какой</ta>
            <ta e="T15" id="Seg_5998" s="T14">NEG</ta>
            <ta e="T16" id="Seg_5999" s="T15">власть-DAT/LOC</ta>
            <ta e="T17" id="Seg_6000" s="T16">работать-NEG.PTCP</ta>
            <ta e="T18" id="Seg_6001" s="T17">ребенок.[NOM]</ta>
            <ta e="T19" id="Seg_6002" s="T18">быть-PST1-1SG</ta>
            <ta e="T20" id="Seg_6003" s="T19">советский</ta>
            <ta e="T21" id="Seg_6004" s="T20">власть.[NOM]</ta>
            <ta e="T22" id="Seg_6005" s="T21">становиться-PRS.[3SG]</ta>
            <ta e="T23" id="Seg_6006" s="T22">говорить-CVB.SEQ</ta>
            <ta e="T26" id="Seg_6007" s="T25">закон.[NOM]</ta>
            <ta e="T27" id="Seg_6008" s="T26">вертеться-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T30" id="Seg_6009" s="T29">ведь</ta>
            <ta e="T31" id="Seg_6010" s="T30">EMPH</ta>
            <ta e="T32" id="Seg_6011" s="T31">понимать-PTCP.PRS</ta>
            <ta e="T33" id="Seg_6012" s="T32">человек.[NOM]</ta>
            <ta e="T34" id="Seg_6013" s="T33">понимать-PTCP.PRS.[NOM]</ta>
            <ta e="T35" id="Seg_6014" s="T34">подобно</ta>
            <ta e="T36" id="Seg_6015" s="T35">быть-PST1-3SG</ta>
            <ta e="T37" id="Seg_6016" s="T36">закон.[NOM]</ta>
            <ta e="T38" id="Seg_6017" s="T37">вертеться-NMNZ-3SG-ACC</ta>
            <ta e="T39" id="Seg_6018" s="T38">когда</ta>
            <ta e="T40" id="Seg_6019" s="T39">сначала</ta>
            <ta e="T41" id="Seg_6020" s="T40">человек-ACC</ta>
            <ta e="T42" id="Seg_6021" s="T41">раньше</ta>
            <ta e="T43" id="Seg_6022" s="T42">богатый</ta>
            <ta e="T44" id="Seg_6023" s="T43">человек.[NOM]</ta>
            <ta e="T45" id="Seg_6024" s="T44">кулак.[NOM]</ta>
            <ta e="T46" id="Seg_6025" s="T45">человек.[NOM]</ta>
            <ta e="T47" id="Seg_6026" s="T46">следовать-ITER-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_6027" s="T47">короткий-ACC</ta>
            <ta e="T49" id="Seg_6028" s="T48">узкий-ACC</ta>
            <ta e="T50" id="Seg_6029" s="T49">бедный-ACC</ta>
            <ta e="T51" id="Seg_6030" s="T50">говорить-CVB.SEQ</ta>
            <ta e="T52" id="Seg_6031" s="T51">имя-VBZ-PTCP.PRS</ta>
            <ta e="T53" id="Seg_6032" s="T52">быть-PST1-3PL</ta>
            <ta e="T54" id="Seg_6033" s="T53">тогда</ta>
            <ta e="T55" id="Seg_6034" s="T54">MOD</ta>
            <ta e="T56" id="Seg_6035" s="T55">1SG.[NOM]</ta>
            <ta e="T57" id="Seg_6036" s="T56">этот</ta>
            <ta e="T58" id="Seg_6037" s="T57">богатый</ta>
            <ta e="T59" id="Seg_6038" s="T58">человек-PL-DAT/LOC</ta>
            <ta e="T60" id="Seg_6039" s="T59">дерево-AG-DIM.[NOM]</ta>
            <ta e="T61" id="Seg_6040" s="T60">быть-PST1-1SG</ta>
            <ta e="T64" id="Seg_6041" s="T63">олень-AG.[NOM]</ta>
            <ta e="T65" id="Seg_6042" s="T64">быть-PST1-1SG</ta>
            <ta e="T66" id="Seg_6043" s="T65">тот</ta>
            <ta e="T67" id="Seg_6044" s="T66">та.сторона.[NOM]</ta>
            <ta e="T68" id="Seg_6045" s="T67">сторона-3SG-DAT/LOC</ta>
            <ta e="T69" id="Seg_6046" s="T68">потом</ta>
            <ta e="T70" id="Seg_6047" s="T69">понимать-CVB.SEQ</ta>
            <ta e="T71" id="Seg_6048" s="T70">идти-PST1-1SG</ta>
            <ta e="T72" id="Seg_6049" s="T71">узнавать-CVB.SEQ</ta>
            <ta e="T73" id="Seg_6050" s="T72">идти-PST1-1SG</ta>
            <ta e="T74" id="Seg_6051" s="T73">конечно</ta>
            <ta e="T75" id="Seg_6052" s="T74">правда.[NOM]</ta>
            <ta e="T76" id="Seg_6053" s="T75">закон.[NOM]</ta>
            <ta e="T77" id="Seg_6054" s="T76">измениться-PRS.[3SG]</ta>
            <ta e="T78" id="Seg_6055" s="T77">быть-PST2.[3SG]</ta>
            <ta e="T79" id="Seg_6056" s="T78">прежний</ta>
            <ta e="T80" id="Seg_6057" s="T79">царский</ta>
            <ta e="T81" id="Seg_6058" s="T80">закон.[NOM]</ta>
            <ta e="T82" id="Seg_6059" s="T81">вот</ta>
            <ta e="T83" id="Seg_6060" s="T82">кончать-PST1-3SG</ta>
            <ta e="T84" id="Seg_6061" s="T83">говорить-PST2-3PL</ta>
            <ta e="T85" id="Seg_6062" s="T84">потом</ta>
            <ta e="T86" id="Seg_6063" s="T85">позже</ta>
            <ta e="T87" id="Seg_6064" s="T86">конец-EP-ABL</ta>
            <ta e="T88" id="Seg_6065" s="T87">школа-DAT/LOC</ta>
            <ta e="T89" id="Seg_6066" s="T88">учиться-PTCP.FUT-DAT/LOC</ta>
            <ta e="T90" id="Seg_6067" s="T89">говорить-CVB.SEQ</ta>
            <ta e="T91" id="Seg_6068" s="T90">товарищ.[NOM]</ta>
            <ta e="T92" id="Seg_6069" s="T91">Ленин.[NOM]</ta>
            <ta e="T93" id="Seg_6070" s="T92">дедушка.[NOM]</ta>
            <ta e="T94" id="Seg_6071" s="T93">говорить-CVB.SEQ</ta>
            <ta e="T95" id="Seg_6072" s="T94">быть-PST1-3SG</ta>
            <ta e="T96" id="Seg_6073" s="T95">Ленин.[NOM]</ta>
            <ta e="T97" id="Seg_6074" s="T96">закон-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_6075" s="T97">вот</ta>
            <ta e="T99" id="Seg_6076" s="T98">измениться-CVB.SEQ-измениться-CVB.SEQ</ta>
            <ta e="T100" id="Seg_6077" s="T99">тот</ta>
            <ta e="T101" id="Seg_6078" s="T100">измениться-CVB.SEQ</ta>
            <ta e="T102" id="Seg_6079" s="T101">идти-PST1-3SG</ta>
            <ta e="T103" id="Seg_6080" s="T102">конец-EP-ABL</ta>
            <ta e="T104" id="Seg_6081" s="T103">человек.[NOM]</ta>
            <ta e="T105" id="Seg_6082" s="T104">каждый-3SG.[NOM]</ta>
            <ta e="T106" id="Seg_6083" s="T105">короткий</ta>
            <ta e="T107" id="Seg_6084" s="T106">узкий</ta>
            <ta e="T108" id="Seg_6085" s="T107">ребенок-3SG.[NOM]</ta>
            <ta e="T109" id="Seg_6086" s="T108">какой</ta>
            <ta e="T110" id="Seg_6087" s="T109">INDEF</ta>
            <ta e="T111" id="Seg_6088" s="T110">народ.[NOM]</ta>
            <ta e="T112" id="Seg_6089" s="T111">какой</ta>
            <ta e="T113" id="Seg_6090" s="T112">EMPH</ta>
            <ta e="T114" id="Seg_6091" s="T113">короткий</ta>
            <ta e="T115" id="Seg_6092" s="T114">узкий</ta>
            <ta e="T116" id="Seg_6093" s="T115">бедный</ta>
            <ta e="T117" id="Seg_6094" s="T116">человек.[NOM]</ta>
            <ta e="T118" id="Seg_6095" s="T117">ребенок-3SG.[NOM]</ta>
            <ta e="T119" id="Seg_6096" s="T118">каждый-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_6097" s="T119">учеба-DAT/LOC</ta>
            <ta e="T121" id="Seg_6098" s="T120">да</ta>
            <ta e="T122" id="Seg_6099" s="T121">вот</ta>
            <ta e="T123" id="Seg_6100" s="T122">начинать-VBZ-PST1-3SG</ta>
            <ta e="T124" id="Seg_6101" s="T123">о</ta>
            <ta e="T125" id="Seg_6102" s="T124">тот.EMPH.[NOM]</ta>
            <ta e="T126" id="Seg_6103" s="T125">вопреки</ta>
            <ta e="T127" id="Seg_6104" s="T126">богатый-ABL</ta>
            <ta e="T128" id="Seg_6105" s="T127">есть-CVB.SIM</ta>
            <ta e="T129" id="Seg_6106" s="T128">учиться-PTCP.PST-PL.[NOM]</ta>
            <ta e="T130" id="Seg_6107" s="T129">кулак-ABL</ta>
            <ta e="T131" id="Seg_6108" s="T130">выручать-EP-REFL-CVB.SIM</ta>
            <ta e="T132" id="Seg_6109" s="T131">учиться-PTCP.PST-PL.[NOM]</ta>
            <ta e="T133" id="Seg_6110" s="T132">ведь</ta>
            <ta e="T134" id="Seg_6111" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_6112" s="T134">школа-DAT/LOC</ta>
            <ta e="T136" id="Seg_6113" s="T135">давать-PTCP.FUT-1PL-ACC</ta>
            <ta e="T137" id="Seg_6114" s="T136">говорить-NEG.PTCP</ta>
            <ta e="T138" id="Seg_6115" s="T137">быть-PST1-3PL</ta>
            <ta e="T139" id="Seg_6116" s="T138">ребенок-ACC</ta>
            <ta e="T140" id="Seg_6117" s="T139">потом</ta>
            <ta e="T141" id="Seg_6118" s="T140">становиться-PST2-3SG</ta>
            <ta e="T142" id="Seg_6119" s="T141">колхоз.[NOM]</ta>
            <ta e="T143" id="Seg_6120" s="T142">быть-PTCP.FUT-DAT/LOC</ta>
            <ta e="T144" id="Seg_6121" s="T143">думать-CVB.SEQ</ta>
            <ta e="T145" id="Seg_6122" s="T144">общий</ta>
            <ta e="T146" id="Seg_6123" s="T145">хозяйство-INSTR</ta>
            <ta e="T147" id="Seg_6124" s="T146">люди.[NOM]</ta>
            <ta e="T148" id="Seg_6125" s="T147">жизнь-3SG-ACC</ta>
            <ta e="T149" id="Seg_6126" s="T148">построить-PTCP.FUT-3SG-ACC</ta>
            <ta e="T150" id="Seg_6127" s="T149">говорить-CVB.SEQ-3PL</ta>
            <ta e="T151" id="Seg_6128" s="T150">нет</ta>
            <ta e="T152" id="Seg_6129" s="T151">тот-DAT/LOC</ta>
            <ta e="T153" id="Seg_6130" s="T152">тоже</ta>
            <ta e="T154" id="Seg_6131" s="T153">быть-CVB.SIM</ta>
            <ta e="T155" id="Seg_6132" s="T154">тянуть-PST2.NEG-3PL</ta>
            <ta e="T156" id="Seg_6133" s="T155">сколько</ta>
            <ta e="T157" id="Seg_6134" s="T156">INDEF</ta>
            <ta e="T158" id="Seg_6135" s="T157">год-EP-INSTR</ta>
            <ta e="T159" id="Seg_6136" s="T158">быть</ta>
            <ta e="T160" id="Seg_6137" s="T159">колхоз.[NOM]</ta>
            <ta e="T161" id="Seg_6138" s="T160">становиться-PST2.NEG-3PL</ta>
            <ta e="T162" id="Seg_6139" s="T161">колхоз.[NOM]</ta>
            <ta e="T163" id="Seg_6140" s="T162">быть-CVB.SEQ-1PL</ta>
            <ta e="T164" id="Seg_6141" s="T163">1PL.[NOM]</ta>
            <ta e="T165" id="Seg_6142" s="T164">уметь-FUT-1PL</ta>
            <ta e="T166" id="Seg_6143" s="T165">NEG.[3SG]</ta>
            <ta e="T167" id="Seg_6144" s="T166">говорить-PTCP.PRS</ta>
            <ta e="T168" id="Seg_6145" s="T167">быть-PST1-3PL</ta>
            <ta e="T169" id="Seg_6146" s="T168">раньше</ta>
            <ta e="T170" id="Seg_6147" s="T169">царский</ta>
            <ta e="T171" id="Seg_6148" s="T170">закон-DAT/LOC</ta>
            <ta e="T172" id="Seg_6149" s="T171">рождаться-PTCP.PST</ta>
            <ta e="T173" id="Seg_6150" s="T172">люди.[NOM]</ta>
            <ta e="T174" id="Seg_6151" s="T173">учиться-PTCP.PST</ta>
            <ta e="T175" id="Seg_6152" s="T174">люди.[NOM]</ta>
            <ta e="T176" id="Seg_6153" s="T175">потом</ta>
            <ta e="T177" id="Seg_6154" s="T176">как</ta>
            <ta e="T178" id="Seg_6155" s="T177">INDEF</ta>
            <ta e="T180" id="Seg_6156" s="T178">делать-CVB.SEQ</ta>
            <ta e="T181" id="Seg_6157" s="T180">рассказывать-CVB.SEQ</ta>
            <ta e="T182" id="Seg_6158" s="T181">рассказывать-CVB.SEQ-3PL</ta>
            <ta e="T183" id="Seg_6159" s="T182">закон.[NOM]</ta>
            <ta e="T184" id="Seg_6160" s="T183">приходить-CVB.SEQ-приходить-CVB.SEQ</ta>
            <ta e="T185" id="Seg_6161" s="T184">этот.EMPH</ta>
            <ta e="T186" id="Seg_6162" s="T185">страна-PL-DAT/LOC</ta>
            <ta e="T187" id="Seg_6163" s="T186">приходить-CVB.SEQ</ta>
            <ta e="T188" id="Seg_6164" s="T187">приходить-CVB.SEQ</ta>
            <ta e="T189" id="Seg_6165" s="T188">колхоз.[NOM]</ta>
            <ta e="T190" id="Seg_6166" s="T189">быть-PST1-1PL</ta>
            <ta e="T191" id="Seg_6167" s="T190">тридцать</ta>
            <ta e="T192" id="Seg_6168" s="T191">семь-ORD-PROPR-EP-ABL</ta>
            <ta e="T193" id="Seg_6169" s="T192">тридцать</ta>
            <ta e="T194" id="Seg_6170" s="T193">восемь-PROPR-DAT/LOC</ta>
            <ta e="T198" id="Seg_6171" s="T196">правда.[NOM]</ta>
            <ta e="T199" id="Seg_6172" s="T198">вот</ta>
            <ta e="T200" id="Seg_6173" s="T199">колхоз.[NOM]</ta>
            <ta e="T201" id="Seg_6174" s="T200">EMPH</ta>
            <ta e="T202" id="Seg_6175" s="T201">начинать-VBZ-CVB.SEQ</ta>
            <ta e="T203" id="Seg_6176" s="T202">усторить-VBZ-CVB.SEQ</ta>
            <ta e="T204" id="Seg_6177" s="T203">оставаться</ta>
            <ta e="T205" id="Seg_6178" s="T204">конец-EP-ABL</ta>
            <ta e="T206" id="Seg_6179" s="T205">тогда</ta>
            <ta e="T207" id="Seg_6180" s="T206">колхоз-ACC</ta>
            <ta e="T208" id="Seg_6181" s="T207">родить-EP-MED-PST2-1PL</ta>
            <ta e="T209" id="Seg_6182" s="T208">быть-PST1-3SG</ta>
            <ta e="T210" id="Seg_6183" s="T209">1PL.[NOM]</ta>
            <ta e="T211" id="Seg_6184" s="T210">где</ta>
            <ta e="T212" id="Seg_6185" s="T211">больше</ta>
            <ta e="T213" id="Seg_6186" s="T212">олень-PROPR-ABL</ta>
            <ta e="T214" id="Seg_6187" s="T213">тот-DAT/LOC</ta>
            <ta e="T215" id="Seg_6188" s="T214">колхоз-DAT/LOC</ta>
            <ta e="T216" id="Seg_6189" s="T215">держать-PST2-1PL</ta>
            <ta e="T217" id="Seg_6190" s="T216">где</ta>
            <ta e="T218" id="Seg_6191" s="T217">пасть-PROPR-1PL-ACC</ta>
            <ta e="T219" id="Seg_6192" s="T218">EMPH-вот</ta>
            <ta e="T220" id="Seg_6193" s="T219">сам-1PL.[NOM]</ta>
            <ta e="T221" id="Seg_6194" s="T220">пасть-1PL-INSTR</ta>
            <ta e="T222" id="Seg_6195" s="T221">сам-1PL.[NOM]</ta>
            <ta e="T223" id="Seg_6196" s="T222">капкан-1PL-INSTR</ta>
            <ta e="T224" id="Seg_6197" s="T223">конец-EP-ABL</ta>
            <ta e="T225" id="Seg_6198" s="T224">колхоз.[NOM]</ta>
            <ta e="T226" id="Seg_6199" s="T225">жизнь-3SG-ACC</ta>
            <ta e="T227" id="Seg_6200" s="T226">строить-CVB.SEQ-1PL</ta>
            <ta e="T228" id="Seg_6201" s="T227">теперь</ta>
            <ta e="T229" id="Seg_6202" s="T228">этот</ta>
            <ta e="T230" id="Seg_6203" s="T229">час-PL-DAT/LOC</ta>
            <ta e="T231" id="Seg_6204" s="T230">доезжать-PST1-1PL</ta>
            <ta e="T232" id="Seg_6205" s="T231">вот</ta>
            <ta e="T233" id="Seg_6206" s="T232">вот</ta>
            <ta e="T234" id="Seg_6207" s="T233">потом</ta>
            <ta e="T235" id="Seg_6208" s="T234">колхоз.[NOM]</ta>
            <ta e="T236" id="Seg_6209" s="T235">жизнь-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_6210" s="T236">хороший.[NOM]</ta>
            <ta e="T238" id="Seg_6211" s="T237">MOD</ta>
            <ta e="T239" id="Seg_6212" s="T238">говорить-CVB.SEQ</ta>
            <ta e="T240" id="Seg_6213" s="T239">тот.[NOM]</ta>
            <ta e="T241" id="Seg_6214" s="T240">подобно</ta>
            <ta e="T242" id="Seg_6215" s="T241">MOD</ta>
            <ta e="T243" id="Seg_6216" s="T242">быть-PTCP.PST-EP-INSTR</ta>
            <ta e="T244" id="Seg_6217" s="T243">вот</ta>
            <ta e="T245" id="Seg_6218" s="T244">колхоз.[NOM]</ta>
            <ta e="T246" id="Seg_6219" s="T245">быть-PST2-1PL</ta>
            <ta e="T247" id="Seg_6220" s="T246">тот</ta>
            <ta e="T248" id="Seg_6221" s="T247">та.сторона.[NOM]</ta>
            <ta e="T249" id="Seg_6222" s="T248">сторона-3SG-DAT/LOC</ta>
            <ta e="T250" id="Seg_6223" s="T249">опять</ta>
            <ta e="T251" id="Seg_6224" s="T250">EMPH</ta>
            <ta e="T252" id="Seg_6225" s="T251">мука-PROPR</ta>
            <ta e="T253" id="Seg_6226" s="T252">нужда-PROPR</ta>
            <ta e="T254" id="Seg_6227" s="T253">время-PL.[NOM]</ta>
            <ta e="T255" id="Seg_6228" s="T254">быть-PTCP.PRS</ta>
            <ta e="T256" id="Seg_6229" s="T255">быть-PST1-3PL</ta>
            <ta e="T257" id="Seg_6230" s="T256">этот</ta>
            <ta e="T258" id="Seg_6231" s="T257">кулак-DAT/LOC</ta>
            <ta e="T259" id="Seg_6232" s="T258">прежний</ta>
            <ta e="T260" id="Seg_6233" s="T259">царский-DAT/LOC</ta>
            <ta e="T261" id="Seg_6234" s="T260">голова-PL-3SG-ACC</ta>
            <ta e="T262" id="Seg_6235" s="T261">разбрасывать-CAUS-PTCP.PST</ta>
            <ta e="T263" id="Seg_6236" s="T262">человек-PL.[NOM]</ta>
            <ta e="T265" id="Seg_6237" s="T264">бандит-VBZ-PTCP.PRS</ta>
            <ta e="T266" id="Seg_6238" s="T265">да</ta>
            <ta e="T267" id="Seg_6239" s="T266">быть-PST1-3PL</ta>
            <ta e="T268" id="Seg_6240" s="T267">тот-PL.[NOM]</ta>
            <ta e="T269" id="Seg_6241" s="T268">ведь</ta>
            <ta e="T270" id="Seg_6242" s="T269">мука-ACC</ta>
            <ta e="T271" id="Seg_6243" s="T270">делать-PTCP.HAB</ta>
            <ta e="T272" id="Seg_6244" s="T271">быть-PST1-3PL</ta>
            <ta e="T273" id="Seg_6245" s="T272">тот</ta>
            <ta e="T274" id="Seg_6246" s="T273">бандит-ACC</ta>
            <ta e="T275" id="Seg_6247" s="T274">нет</ta>
            <ta e="T276" id="Seg_6248" s="T275">тот</ta>
            <ta e="T277" id="Seg_6249" s="T276">чин-ACC</ta>
            <ta e="T278" id="Seg_6250" s="T277">советский</ta>
            <ta e="T279" id="Seg_6251" s="T278">власть.[NOM]</ta>
            <ta e="T280" id="Seg_6252" s="T279">передняя.часть.[NOM]</ta>
            <ta e="T281" id="Seg_6253" s="T280">к</ta>
            <ta e="T282" id="Seg_6254" s="T281">шагать-CVB.SEQ</ta>
            <ta e="T283" id="Seg_6255" s="T282">идти-CVB.SEQ</ta>
            <ta e="T284" id="Seg_6256" s="T283">тот</ta>
            <ta e="T285" id="Seg_6257" s="T284">бандит-ACC</ta>
            <ta e="T286" id="Seg_6258" s="T285">ведь</ta>
            <ta e="T287" id="Seg_6259" s="T286">EMPH</ta>
            <ta e="T288" id="Seg_6260" s="T287">очистить-PTCP.HAB</ta>
            <ta e="T289" id="Seg_6261" s="T288">быть-PST1-3SG</ta>
            <ta e="T290" id="Seg_6262" s="T289">проводить.в.порядок-PTCP.HAB</ta>
            <ta e="T291" id="Seg_6263" s="T290">да</ta>
            <ta e="T292" id="Seg_6264" s="T291">быть-PST1-3SG</ta>
            <ta e="T293" id="Seg_6265" s="T292">вот</ta>
            <ta e="T294" id="Seg_6266" s="T293">тот</ta>
            <ta e="T295" id="Seg_6267" s="T294">кончать-PTCP.PST-3SG-ACC</ta>
            <ta e="T296" id="Seg_6268" s="T295">после.того</ta>
            <ta e="T297" id="Seg_6269" s="T296">конец-EP-ABL</ta>
            <ta e="T298" id="Seg_6270" s="T297">этот</ta>
            <ta e="T299" id="Seg_6271" s="T298">Германия.[NOM]</ta>
            <ta e="T300" id="Seg_6272" s="T299">война-3SG.[NOM]</ta>
            <ta e="T301" id="Seg_6273" s="T300">становиться-PST2-3SG</ta>
            <ta e="T302" id="Seg_6274" s="T301">вот</ta>
            <ta e="T303" id="Seg_6275" s="T302">тот-DAT/LOC</ta>
            <ta e="T304" id="Seg_6276" s="T303">заботить-CVB.SIM</ta>
            <ta e="T305" id="Seg_6277" s="T304">бить-PST2-1PL</ta>
            <ta e="T306" id="Seg_6278" s="T305">1PL.[NOM]</ta>
            <ta e="T307" id="Seg_6279" s="T306">очень</ta>
            <ta e="T308" id="Seg_6280" s="T307">тот</ta>
            <ta e="T309" id="Seg_6281" s="T308">одежда.[NOM]</ta>
            <ta e="T310" id="Seg_6282" s="T309">да</ta>
            <ta e="T311" id="Seg_6283" s="T310">собираться-NMNZ-3SG.[NOM]</ta>
            <ta e="T312" id="Seg_6284" s="T311">быть-PTCP.PRS</ta>
            <ta e="T313" id="Seg_6285" s="T312">быть-PST1-3SG</ta>
            <ta e="T314" id="Seg_6286" s="T313">тот</ta>
            <ta e="T315" id="Seg_6287" s="T314">деньги-INSTR</ta>
            <ta e="T316" id="Seg_6288" s="T315">да</ta>
            <ta e="T317" id="Seg_6289" s="T316">налог.[NOM]</ta>
            <ta e="T318" id="Seg_6290" s="T317">быть-PTCP.PRS</ta>
            <ta e="T319" id="Seg_6291" s="T318">быть-PST1-3SG</ta>
            <ta e="T320" id="Seg_6292" s="T319">1PL.[NOM]</ta>
            <ta e="T321" id="Seg_6293" s="T320">тот.[NOM]</ta>
            <ta e="T322" id="Seg_6294" s="T321">к</ta>
            <ta e="T323" id="Seg_6295" s="T322">город.[NOM]</ta>
            <ta e="T324" id="Seg_6296" s="T323">место-PL-DAT/LOC</ta>
            <ta e="T325" id="Seg_6297" s="T324">армия-DAT/LOC</ta>
            <ta e="T327" id="Seg_6298" s="T325">помогать-CVB.SEQ-1PL</ta>
            <ta e="T328" id="Seg_6299" s="T327">потом</ta>
            <ta e="T329" id="Seg_6300" s="T328">год.[NOM]</ta>
            <ta e="T330" id="Seg_6301" s="T329">кончать-CVB.SEQ</ta>
            <ta e="T331" id="Seg_6302" s="T330">вот</ta>
            <ta e="T332" id="Seg_6303" s="T331">теперь</ta>
            <ta e="T333" id="Seg_6304" s="T332">что.[NOM]</ta>
            <ta e="T334" id="Seg_6305" s="T333">NEG</ta>
            <ta e="T335" id="Seg_6306" s="T334">быть-FUT-1PL</ta>
            <ta e="T336" id="Seg_6307" s="T335">думать-NEG-1SG</ta>
            <ta e="T337" id="Seg_6308" s="T336">сегодняшний</ta>
            <ta e="T338" id="Seg_6309" s="T337">душа-1SG.[NOM]</ta>
            <ta e="T339" id="Seg_6310" s="T338">хороший.[NOM]</ta>
            <ta e="T340" id="Seg_6311" s="T339">теперь</ta>
            <ta e="T341" id="Seg_6312" s="T340">жизнь.[NOM]</ta>
            <ta e="T342" id="Seg_6313" s="T341">становиться.лучше-PST1-3SG</ta>
            <ta e="T343" id="Seg_6314" s="T342">жизнь.[NOM]</ta>
            <ta e="T344" id="Seg_6315" s="T343">расти-PST1-3SG</ta>
            <ta e="T345" id="Seg_6316" s="T344">теперь</ta>
            <ta e="T346" id="Seg_6317" s="T345">целый</ta>
            <ta e="T347" id="Seg_6318" s="T346">колхоз-PL.[NOM]</ta>
            <ta e="T348" id="Seg_6319" s="T347">конец-EP-ABL</ta>
            <ta e="T349" id="Seg_6320" s="T348">организация.[NOM]</ta>
            <ta e="T350" id="Seg_6321" s="T349">подобно</ta>
            <ta e="T351" id="Seg_6322" s="T350">становиться-PST1-3PL</ta>
            <ta e="T352" id="Seg_6323" s="T351">теперь</ta>
            <ta e="T353" id="Seg_6324" s="T352">отсюда</ta>
            <ta e="T354" id="Seg_6325" s="T353">передняя.часть.[NOM]</ta>
            <ta e="T355" id="Seg_6326" s="T354">сторона-3SG-ACC</ta>
            <ta e="T356" id="Seg_6327" s="T355">знать-NEG-1SG</ta>
            <ta e="T357" id="Seg_6328" s="T356">сам-1SG.[NOM]</ta>
            <ta e="T358" id="Seg_6329" s="T357">постареть-CVB.SEQ</ta>
            <ta e="T359" id="Seg_6330" s="T358">оставаться-PST1-1SG</ta>
            <ta e="T362" id="Seg_6331" s="T361">1SG.[NOM]</ta>
            <ta e="T363" id="Seg_6332" s="T362">мысль-1SG-DAT/LOC</ta>
            <ta e="T364" id="Seg_6333" s="T363">хороший</ta>
            <ta e="T365" id="Seg_6334" s="T364">жизнь-DAT/LOC</ta>
            <ta e="T366" id="Seg_6335" s="T365">теперь</ta>
            <ta e="T367" id="Seg_6336" s="T366">доезжать-PST1-1PL</ta>
            <ta e="T368" id="Seg_6337" s="T367">говорить-PRS-1SG</ta>
            <ta e="T369" id="Seg_6338" s="T368">этот.EMPH</ta>
            <ta e="T370" id="Seg_6339" s="T369">постареть-PTCP.PRS</ta>
            <ta e="T372" id="Seg_6340" s="T370">возраст-1SG-DAT/LOC</ta>
            <ta e="T373" id="Seg_6341" s="T372">теперь</ta>
            <ta e="T374" id="Seg_6342" s="T373">сытый.[NOM]</ta>
            <ta e="T375" id="Seg_6343" s="T374">очень-1PL</ta>
            <ta e="T376" id="Seg_6344" s="T375">теплый.[NOM]</ta>
            <ta e="T377" id="Seg_6345" s="T376">очень-1PL</ta>
            <ta e="T378" id="Seg_6346" s="T377">одежда-ACC</ta>
            <ta e="T379" id="Seg_6347" s="T378">да</ta>
            <ta e="T380" id="Seg_6348" s="T379">одеваться-PRS-1PL</ta>
            <ta e="T381" id="Seg_6349" s="T380">пища-ACC</ta>
            <ta e="T382" id="Seg_6350" s="T381">да</ta>
            <ta e="T383" id="Seg_6351" s="T382">есть-PRS-1PL</ta>
            <ta e="T384" id="Seg_6352" s="T383">этот</ta>
            <ta e="T385" id="Seg_6353" s="T384">вот</ta>
            <ta e="T386" id="Seg_6354" s="T385">сначала</ta>
            <ta e="T387" id="Seg_6355" s="T386">мой</ta>
            <ta e="T388" id="Seg_6356" s="T387">этот</ta>
            <ta e="T389" id="Seg_6357" s="T388">большой</ta>
            <ta e="T390" id="Seg_6358" s="T389">ребенок-1SG.[NOM]</ta>
            <ta e="T391" id="Seg_6359" s="T390">этот</ta>
            <ta e="T392" id="Seg_6360" s="T391">Василий.[NOM]</ta>
            <ta e="T393" id="Seg_6361" s="T392">есть</ta>
            <ta e="T394" id="Seg_6362" s="T393">этот</ta>
            <ta e="T395" id="Seg_6363" s="T394">ребенок-1SG.[NOM]</ta>
            <ta e="T396" id="Seg_6364" s="T395">расти-PST1-3SG</ta>
            <ta e="T397" id="Seg_6365" s="T396">да</ta>
            <ta e="T398" id="Seg_6366" s="T397">учиться-PST2-3SG</ta>
            <ta e="T399" id="Seg_6367" s="T398">3SG.[NOM]</ta>
            <ta e="T400" id="Seg_6368" s="T399">школа-3SG-ACC</ta>
            <ta e="T401" id="Seg_6369" s="T400">кончать-EP-CAUS-PST1-3SG</ta>
            <ta e="T402" id="Seg_6370" s="T401">EMPH</ta>
            <ta e="T403" id="Seg_6371" s="T402">учитель-DAT/LOC</ta>
            <ta e="T404" id="Seg_6372" s="T403">учиться-PST2-3SG</ta>
            <ta e="T405" id="Seg_6373" s="T404">так</ta>
            <ta e="T406" id="Seg_6374" s="T405">теперь</ta>
            <ta e="T407" id="Seg_6375" s="T406">день.[NOM]</ta>
            <ta e="T408" id="Seg_6376" s="T407">сегодня</ta>
            <ta e="T409" id="Seg_6377" s="T408">учитель-VBZ-CVB.SIM</ta>
            <ta e="T410" id="Seg_6378" s="T409">сидеть-PRS.[3SG]</ta>
            <ta e="T411" id="Seg_6379" s="T410">потом</ta>
            <ta e="T412" id="Seg_6380" s="T411">один</ta>
            <ta e="T413" id="Seg_6381" s="T412">дочь-PROPR-1SG</ta>
            <ta e="T414" id="Seg_6382" s="T413">тот.[NOM]</ta>
            <ta e="T415" id="Seg_6383" s="T414">тоже</ta>
            <ta e="T416" id="Seg_6384" s="T415">учеба-3SG-ACC</ta>
            <ta e="T417" id="Seg_6385" s="T416">школа-3SG-ACC</ta>
            <ta e="T418" id="Seg_6386" s="T417">кончать-EP-CAUS-CVB.SEQ</ta>
            <ta e="T419" id="Seg_6387" s="T418">тоже</ta>
            <ta e="T420" id="Seg_6388" s="T419">высший.[NOM]</ta>
            <ta e="T421" id="Seg_6389" s="T420">учиться-CVB.SEQ</ta>
            <ta e="T422" id="Seg_6390" s="T421">доктор-VBZ-CVB.SEQ</ta>
            <ta e="T423" id="Seg_6391" s="T422">сидеть-PRS.[3SG]</ta>
            <ta e="T424" id="Seg_6392" s="T423">теперь</ta>
            <ta e="T425" id="Seg_6393" s="T424">тот</ta>
            <ta e="T426" id="Seg_6394" s="T425">сам-3SG-GEN</ta>
            <ta e="T427" id="Seg_6395" s="T426">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T428" id="Seg_6396" s="T427">хороший.[NOM]</ta>
            <ta e="T429" id="Seg_6397" s="T428">тот.[NOM]</ta>
            <ta e="T430" id="Seg_6398" s="T429">после.того</ta>
            <ta e="T431" id="Seg_6399" s="T430">1SG.[NOM]</ta>
            <ta e="T432" id="Seg_6400" s="T431">да</ta>
            <ta e="T433" id="Seg_6401" s="T432">душа-1SG-DAT/LOC</ta>
            <ta e="T434" id="Seg_6402" s="T433">хороший.[NOM]</ta>
            <ta e="T435" id="Seg_6403" s="T434">теперь</ta>
            <ta e="T436" id="Seg_6404" s="T435">этот</ta>
            <ta e="T437" id="Seg_6405" s="T436">жизнь-DAT/LOC</ta>
            <ta e="T438" id="Seg_6406" s="T437">жить-NMNZ-3PL.[NOM]</ta>
            <ta e="T439" id="Seg_6407" s="T438">теперь</ta>
            <ta e="T440" id="Seg_6408" s="T439">этот</ta>
            <ta e="T441" id="Seg_6409" s="T440">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T442" id="Seg_6410" s="T441">тот-DAT/LOC</ta>
            <ta e="T443" id="Seg_6411" s="T442">три-ORD</ta>
            <ta e="T444" id="Seg_6412" s="T443">опять</ta>
            <ta e="T445" id="Seg_6413" s="T444">мальчик-EP-1SG.[NOM]</ta>
            <ta e="T446" id="Seg_6414" s="T445">аэропорт-DAT/LOC</ta>
            <ta e="T447" id="Seg_6415" s="T446">работать-PRS.[3SG]</ta>
            <ta e="T448" id="Seg_6416" s="T447">потом</ta>
            <ta e="T449" id="Seg_6417" s="T448">самый</ta>
            <ta e="T450" id="Seg_6418" s="T449">маленький.[NOM]</ta>
            <ta e="T451" id="Seg_6419" s="T450">сын-PROPR-1SG</ta>
            <ta e="T452" id="Seg_6420" s="T451">теперь</ta>
            <ta e="T453" id="Seg_6421" s="T452">Иван.[NOM]</ta>
            <ta e="T454" id="Seg_6422" s="T453">Антонов</ta>
            <ta e="T455" id="Seg_6423" s="T454">Иван.[NOM]</ta>
            <ta e="T458" id="Seg_6424" s="T457">тот</ta>
            <ta e="T459" id="Seg_6425" s="T458">MOD</ta>
            <ta e="T460" id="Seg_6426" s="T459">теперь</ta>
            <ta e="T461" id="Seg_6427" s="T460">колхоз-DAT/LOC</ta>
            <ta e="T462" id="Seg_6428" s="T461">работать-PRS.[3SG]</ta>
            <ta e="T463" id="Seg_6429" s="T462">колхоз.[NOM]</ta>
            <ta e="T464" id="Seg_6430" s="T463">EMPH</ta>
            <ta e="T465" id="Seg_6431" s="T464">работать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T466" id="Seg_6432" s="T465">работа-3SG.[NOM]</ta>
            <ta e="T467" id="Seg_6433" s="T466">работа-3SG-ACC</ta>
            <ta e="T468" id="Seg_6434" s="T467">да</ta>
            <ta e="T469" id="Seg_6435" s="T468">работа-PROPR</ta>
            <ta e="T470" id="Seg_6436" s="T469">колхоз.[NOM]</ta>
            <ta e="T471" id="Seg_6437" s="T470">ведь</ta>
            <ta e="T472" id="Seg_6438" s="T471">EMPH</ta>
            <ta e="T473" id="Seg_6439" s="T472">сам-3SG-GEN</ta>
            <ta e="T474" id="Seg_6440" s="T473">колхоз.[NOM]</ta>
            <ta e="T475" id="Seg_6441" s="T474">нутро-3SG-DAT/LOC</ta>
            <ta e="T476" id="Seg_6442" s="T475">ведь</ta>
            <ta e="T477" id="Seg_6443" s="T476">хороший-ADVZ</ta>
            <ta e="T478" id="Seg_6444" s="T477">наесться-EP-CAUS-CVB.SEQ</ta>
            <ta e="T479" id="Seg_6445" s="T478">одеваться-CAUS-CVB.SEQ</ta>
            <ta e="T480" id="Seg_6446" s="T479">ведь</ta>
            <ta e="T481" id="Seg_6447" s="T480">да</ta>
            <ta e="T482" id="Seg_6448" s="T481">работать-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_6449" s="T482">ведь</ta>
            <ta e="T484" id="Seg_6450" s="T483">да</ta>
            <ta e="T485" id="Seg_6451" s="T484">зарплата-PROPR.[NOM]</ta>
            <ta e="T486" id="Seg_6452" s="T485">человек.[NOM]</ta>
            <ta e="T487" id="Seg_6453" s="T486">где</ta>
            <ta e="T488" id="Seg_6454" s="T487">хороший-ADVZ</ta>
            <ta e="T489" id="Seg_6455" s="T488">работать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T490" id="Seg_6456" s="T489">тот</ta>
            <ta e="T491" id="Seg_6457" s="T490">лучше</ta>
            <ta e="T492" id="Seg_6458" s="T491">становиться-PRS.[3SG]</ta>
            <ta e="T493" id="Seg_6459" s="T492">где</ta>
            <ta e="T494" id="Seg_6460" s="T493">плохой-ADVZ</ta>
            <ta e="T495" id="Seg_6461" s="T494">работать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T496" id="Seg_6462" s="T495">тот</ta>
            <ta e="T497" id="Seg_6463" s="T496">ниже</ta>
            <ta e="T498" id="Seg_6464" s="T497">становиться-PRS.[3SG]</ta>
            <ta e="T499" id="Seg_6465" s="T498">1SG.[NOM]</ta>
            <ta e="T500" id="Seg_6466" s="T499">мысль-1SG-DAT/LOC</ta>
            <ta e="T501" id="Seg_6467" s="T500">EMPH-так</ta>
            <ta e="T502" id="Seg_6468" s="T501">этот</ta>
            <ta e="T503" id="Seg_6469" s="T502">вот</ta>
            <ta e="T504" id="Seg_6470" s="T503">сначала</ta>
            <ta e="T505" id="Seg_6471" s="T504">сам-1SG.[NOM]</ta>
            <ta e="T506" id="Seg_6472" s="T505">ведь</ta>
            <ta e="T507" id="Seg_6473" s="T506">EMPH</ta>
            <ta e="T508" id="Seg_6474" s="T507">этот</ta>
            <ta e="T509" id="Seg_6475" s="T508">сельсовет-EP-INSTR</ta>
            <ta e="T510" id="Seg_6476" s="T509">что-EP-INSTR</ta>
            <ta e="T511" id="Seg_6477" s="T510">руководствовать-VBZ-CVB.SEQ</ta>
            <ta e="T512" id="Seg_6478" s="T511">сидеть-PTCP.PRS</ta>
            <ta e="T513" id="Seg_6479" s="T512">быть-PST1-1SG</ta>
            <ta e="T514" id="Seg_6480" s="T513">потом</ta>
            <ta e="T515" id="Seg_6481" s="T514">теперь</ta>
            <ta e="T516" id="Seg_6482" s="T515">молодой</ta>
            <ta e="T517" id="Seg_6483" s="T516">человек-PL.[NOM]</ta>
            <ta e="T518" id="Seg_6484" s="T517">учеба-DAT/LOC</ta>
            <ta e="T519" id="Seg_6485" s="T518">учиться-CVB.SEQ-3PL</ta>
            <ta e="T520" id="Seg_6486" s="T519">1SG-COMP</ta>
            <ta e="T521" id="Seg_6487" s="T520">лучше</ta>
            <ta e="T522" id="Seg_6488" s="T521">работать-CAP-3PL</ta>
            <ta e="T523" id="Seg_6489" s="T522">тогда</ta>
            <ta e="T524" id="Seg_6490" s="T523">MOD</ta>
            <ta e="T525" id="Seg_6491" s="T524">кто-DAT/LOC</ta>
            <ta e="T526" id="Seg_6492" s="T525">EMPH</ta>
            <ta e="T527" id="Seg_6493" s="T526">вина.[NOM]</ta>
            <ta e="T528" id="Seg_6494" s="T527">слово-POSS</ta>
            <ta e="T529" id="Seg_6495" s="T528">NEG</ta>
            <ta e="T530" id="Seg_6496" s="T529">теперь</ta>
            <ta e="T531" id="Seg_6497" s="T530">отдыхать-FUT-1SG</ta>
            <ta e="T532" id="Seg_6498" s="T531">говорить-CVB.SEQ-1SG</ta>
            <ta e="T533" id="Seg_6499" s="T532">сельсовет-VBZ-CVB.SEQ</ta>
            <ta e="T534" id="Seg_6500" s="T533">кончать-PST2-EP-1SG</ta>
            <ta e="T535" id="Seg_6501" s="T534">быть-PST1-3SG</ta>
            <ta e="T536" id="Seg_6502" s="T535">теперь</ta>
            <ta e="T537" id="Seg_6503" s="T536">этот</ta>
            <ta e="T538" id="Seg_6504" s="T537">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T539" id="Seg_6505" s="T538">учеба-DAT/LOC</ta>
            <ta e="T540" id="Seg_6506" s="T539">учиться-CVB.SEQ-3PL</ta>
            <ta e="T541" id="Seg_6507" s="T540">хороший-ADVZ</ta>
            <ta e="T544" id="Seg_6508" s="T543">жить-PRS-3PL</ta>
            <ta e="T545" id="Seg_6509" s="T544">потом</ta>
            <ta e="T546" id="Seg_6510" s="T545">теперь</ta>
            <ta e="T547" id="Seg_6511" s="T546">радоваться-EP-REFL-CVB.SIM</ta>
            <ta e="T548" id="Seg_6512" s="T547">сидеть-PRS-1SG</ta>
            <ta e="T549" id="Seg_6513" s="T548">этот</ta>
            <ta e="T550" id="Seg_6514" s="T549">постареть-CVB.SEQ</ta>
            <ta e="T551" id="Seg_6515" s="T550">идти-CVB.SEQ-1SG</ta>
            <ta e="T552" id="Seg_6516" s="T551">сам-1SG.[NOM]</ta>
            <ta e="T553" id="Seg_6517" s="T552">учеба-POSS</ta>
            <ta e="T554" id="Seg_6518" s="T553">NEG</ta>
            <ta e="T555" id="Seg_6519" s="T554">очень</ta>
            <ta e="T556" id="Seg_6520" s="T555">быть-PST1-1SG</ta>
            <ta e="T557" id="Seg_6521" s="T556">тот.[NOM]</ta>
            <ta e="T558" id="Seg_6522" s="T557">когда</ta>
            <ta e="T559" id="Seg_6523" s="T558">даже</ta>
            <ta e="T560" id="Seg_6524" s="T559">1SG.[NOM]</ta>
            <ta e="T561" id="Seg_6525" s="T560">язык-ACC</ta>
            <ta e="T562" id="Seg_6526" s="T561">NEG</ta>
            <ta e="T563" id="Seg_6527" s="T562">мочь-CVB.SEQ</ta>
            <ta e="T564" id="Seg_6528" s="T563">слышать-EP-NEG-1SG</ta>
            <ta e="T565" id="Seg_6529" s="T564">быть-PST1-1SG</ta>
            <ta e="T566" id="Seg_6530" s="T565">когда</ta>
            <ta e="T567" id="Seg_6531" s="T566">тот</ta>
            <ta e="T568" id="Seg_6532" s="T567">1SG.[NOM]</ta>
            <ta e="T569" id="Seg_6533" s="T568">сельсовет.[NOM]</ta>
            <ta e="T570" id="Seg_6534" s="T569">быть-TEMP-1SG</ta>
            <ta e="T571" id="Seg_6535" s="T570">о</ta>
            <ta e="T572" id="Seg_6536" s="T571">что.[NOM]</ta>
            <ta e="T573" id="Seg_6537" s="T572">INDEF</ta>
            <ta e="T574" id="Seg_6538" s="T573">переводчик-EP-INSTR</ta>
            <ta e="T575" id="Seg_6539" s="T574">один</ta>
            <ta e="T576" id="Seg_6540" s="T575">INDEF</ta>
            <ta e="T577" id="Seg_6541" s="T576">слово-DIM-ACC</ta>
            <ta e="T578" id="Seg_6542" s="T577">несколько</ta>
            <ta e="T579" id="Seg_6543" s="T578">человек.[NOM]</ta>
            <ta e="T580" id="Seg_6544" s="T579">слово-3SG-INSTR</ta>
            <ta e="T581" id="Seg_6545" s="T580">обменять-PTCP.HAB</ta>
            <ta e="T582" id="Seg_6546" s="T581">быть-PST1-1PL</ta>
            <ta e="T583" id="Seg_6547" s="T582">переводчик-EP-INSTR</ta>
            <ta e="T584" id="Seg_6548" s="T583">тот</ta>
            <ta e="T585" id="Seg_6549" s="T584">мука-PROPR.[NOM]</ta>
            <ta e="T586" id="Seg_6550" s="T585">быть-PST1-3SG</ta>
            <ta e="T587" id="Seg_6551" s="T586">теперь</ta>
            <ta e="T588" id="Seg_6552" s="T587">мука-POSS</ta>
            <ta e="T589" id="Seg_6553" s="T588">NEG</ta>
            <ta e="T590" id="Seg_6554" s="T589">сам-3PL.[NOM]</ta>
            <ta e="T591" id="Seg_6555" s="T590">лицо.[NOM]</ta>
            <ta e="T592" id="Seg_6556" s="T591">разговаривать-PRS-3PL</ta>
            <ta e="T593" id="Seg_6557" s="T592">кто-ACC</ta>
            <ta e="T594" id="Seg_6558" s="T593">INDEF</ta>
            <ta e="T595" id="Seg_6559" s="T594">с</ta>
            <ta e="T596" id="Seg_6560" s="T595">советский</ta>
            <ta e="T597" id="Seg_6561" s="T596">власть.[NOM]</ta>
            <ta e="T598" id="Seg_6562" s="T597">хороший</ta>
            <ta e="T599" id="Seg_6563" s="T598">очень</ta>
            <ta e="T600" id="Seg_6564" s="T599">жизнь-DAT/LOC</ta>
            <ta e="T601" id="Seg_6565" s="T600">доводить-PST1-3SG</ta>
            <ta e="T602" id="Seg_6566" s="T601">теперь</ta>
            <ta e="T603" id="Seg_6567" s="T602">советский</ta>
            <ta e="T604" id="Seg_6568" s="T603">власть.[NOM]</ta>
            <ta e="T605" id="Seg_6569" s="T604">становиться-CVB.SEQ</ta>
            <ta e="T606" id="Seg_6570" s="T605">ребенок-PL-1SG-DAT/LOC</ta>
            <ta e="T607" id="Seg_6571" s="T606">EMPH</ta>
            <ta e="T608" id="Seg_6572" s="T607">1SG.[NOM]</ta>
            <ta e="T609" id="Seg_6573" s="T608">EMPH</ta>
            <ta e="T610" id="Seg_6574" s="T609">столько-1SG-DAT/LOC</ta>
            <ta e="T611" id="Seg_6575" s="T610">доезжать-PST1-1SG</ta>
            <ta e="T612" id="Seg_6576" s="T611">теперь</ta>
            <ta e="T613" id="Seg_6577" s="T612">очень</ta>
            <ta e="T614" id="Seg_6578" s="T613">хороший-ADVZ</ta>
            <ta e="T615" id="Seg_6579" s="T614">столько</ta>
            <ta e="T616" id="Seg_6580" s="T615">столько</ta>
            <ta e="T617" id="Seg_6581" s="T616">постареть-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T618" id="Seg_6582" s="T617">доезжать-PST1-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_6583" s="T0">adj</ta>
            <ta e="T2" id="Seg_6584" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_6585" s="T2">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T4" id="Seg_6586" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_6587" s="T4">adv</ta>
            <ta e="T6" id="Seg_6588" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_6589" s="T6">v-v:(neg)-v:pred.pn</ta>
            <ta e="T8" id="Seg_6590" s="T7">adv</ta>
            <ta e="T9" id="Seg_6591" s="T8">pers-pro:case</ta>
            <ta e="T10" id="Seg_6592" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_6593" s="T10">adv</ta>
            <ta e="T12" id="Seg_6594" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_6595" s="T12">v-v:tense-v:poss.pn</ta>
            <ta e="T14" id="Seg_6596" s="T13">que</ta>
            <ta e="T15" id="Seg_6597" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_6598" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_6599" s="T16">v-v:ptcp</ta>
            <ta e="T18" id="Seg_6600" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_6601" s="T18">v-v:tense-v:poss.pn</ta>
            <ta e="T20" id="Seg_6602" s="T19">adj</ta>
            <ta e="T21" id="Seg_6603" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_6604" s="T21">v-v:tense-v:pred.pn</ta>
            <ta e="T23" id="Seg_6605" s="T22">v-v:cvb</ta>
            <ta e="T26" id="Seg_6606" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_6607" s="T26">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T30" id="Seg_6608" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_6609" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_6610" s="T31">v-v:ptcp</ta>
            <ta e="T33" id="Seg_6611" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_6612" s="T33">v-v:ptcp-v:(case)</ta>
            <ta e="T35" id="Seg_6613" s="T34">post</ta>
            <ta e="T36" id="Seg_6614" s="T35">v-v:tense-v:poss.pn</ta>
            <ta e="T37" id="Seg_6615" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_6616" s="T37">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T39" id="Seg_6617" s="T38">post</ta>
            <ta e="T40" id="Seg_6618" s="T39">adv</ta>
            <ta e="T41" id="Seg_6619" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_6620" s="T41">adv</ta>
            <ta e="T43" id="Seg_6621" s="T42">adj</ta>
            <ta e="T44" id="Seg_6622" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_6623" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_6624" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_6625" s="T46">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T48" id="Seg_6626" s="T47">adj-n:case</ta>
            <ta e="T49" id="Seg_6627" s="T48">adj-n:case</ta>
            <ta e="T50" id="Seg_6628" s="T49">adj-n:case</ta>
            <ta e="T51" id="Seg_6629" s="T50">v-v:cvb</ta>
            <ta e="T52" id="Seg_6630" s="T51">n-n&gt;v-v:ptcp</ta>
            <ta e="T53" id="Seg_6631" s="T52">v-v:tense-v:poss.pn</ta>
            <ta e="T54" id="Seg_6632" s="T53">adv</ta>
            <ta e="T55" id="Seg_6633" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_6634" s="T55">pers-pro:case</ta>
            <ta e="T57" id="Seg_6635" s="T56">dempro</ta>
            <ta e="T58" id="Seg_6636" s="T57">adj</ta>
            <ta e="T59" id="Seg_6637" s="T58">n-n:(num)-n:case</ta>
            <ta e="T60" id="Seg_6638" s="T59">n-n&gt;n-n&gt;n-n:case</ta>
            <ta e="T61" id="Seg_6639" s="T60">v-v:tense-v:poss.pn</ta>
            <ta e="T64" id="Seg_6640" s="T63">n-n&gt;n-n:case</ta>
            <ta e="T65" id="Seg_6641" s="T64">v-v:tense-v:poss.pn</ta>
            <ta e="T66" id="Seg_6642" s="T65">dempro</ta>
            <ta e="T67" id="Seg_6643" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_6644" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_6645" s="T68">adv</ta>
            <ta e="T70" id="Seg_6646" s="T69">v-v:cvb</ta>
            <ta e="T71" id="Seg_6647" s="T70">v-v:tense-v:poss.pn</ta>
            <ta e="T72" id="Seg_6648" s="T71">v-v:cvb</ta>
            <ta e="T73" id="Seg_6649" s="T72">v-v:tense-v:poss.pn</ta>
            <ta e="T74" id="Seg_6650" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_6651" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_6652" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_6653" s="T76">v-v:tense.[v:pred.pn]</ta>
            <ta e="T78" id="Seg_6654" s="T77">v-v:tense-v:pred.pn</ta>
            <ta e="T79" id="Seg_6655" s="T78">adj</ta>
            <ta e="T80" id="Seg_6656" s="T79">adj</ta>
            <ta e="T81" id="Seg_6657" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_6658" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_6659" s="T82">v-v:tense-v:poss.pn</ta>
            <ta e="T84" id="Seg_6660" s="T83">v-v:tense-v:poss.pn</ta>
            <ta e="T85" id="Seg_6661" s="T84">adv</ta>
            <ta e="T86" id="Seg_6662" s="T85">adv</ta>
            <ta e="T87" id="Seg_6663" s="T86">n-n:(ins)-n:case</ta>
            <ta e="T88" id="Seg_6664" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_6665" s="T88">v-v:ptcp-v:(case)</ta>
            <ta e="T90" id="Seg_6666" s="T89">v-v:cvb</ta>
            <ta e="T91" id="Seg_6667" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_6668" s="T91">propr-n:case</ta>
            <ta e="T93" id="Seg_6669" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_6670" s="T93">v-v:cvb</ta>
            <ta e="T95" id="Seg_6671" s="T94">v-v:tense-v:poss.pn</ta>
            <ta e="T96" id="Seg_6672" s="T95">propr-n:case</ta>
            <ta e="T97" id="Seg_6673" s="T96">n-n:(poss)-n:case</ta>
            <ta e="T98" id="Seg_6674" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_6675" s="T98">v-v:cvb-v-v:cvb</ta>
            <ta e="T100" id="Seg_6676" s="T99">dempro</ta>
            <ta e="T101" id="Seg_6677" s="T100">v-v:cvb</ta>
            <ta e="T102" id="Seg_6678" s="T101">v-v:tense-v:poss.pn</ta>
            <ta e="T103" id="Seg_6679" s="T102">n-n:(ins)-n:case</ta>
            <ta e="T104" id="Seg_6680" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_6681" s="T104">adj-n:(poss).[n:case]</ta>
            <ta e="T106" id="Seg_6682" s="T105">adj</ta>
            <ta e="T107" id="Seg_6683" s="T106">adj</ta>
            <ta e="T108" id="Seg_6684" s="T107">n-n:(poss)-n:case</ta>
            <ta e="T109" id="Seg_6685" s="T108">que</ta>
            <ta e="T110" id="Seg_6686" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_6687" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_6688" s="T111">que</ta>
            <ta e="T113" id="Seg_6689" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_6690" s="T113">adj</ta>
            <ta e="T115" id="Seg_6691" s="T114">adj</ta>
            <ta e="T116" id="Seg_6692" s="T115">adj</ta>
            <ta e="T117" id="Seg_6693" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_6694" s="T117">n-n:(poss)-n:case</ta>
            <ta e="T119" id="Seg_6695" s="T118">adj-n:(poss).[n:case]</ta>
            <ta e="T120" id="Seg_6696" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_6697" s="T120">conj</ta>
            <ta e="T122" id="Seg_6698" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_6699" s="T122">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T124" id="Seg_6700" s="T123">interj</ta>
            <ta e="T125" id="Seg_6701" s="T124">dempro-pro:case</ta>
            <ta e="T126" id="Seg_6702" s="T125">post</ta>
            <ta e="T127" id="Seg_6703" s="T126">adj-n:case</ta>
            <ta e="T128" id="Seg_6704" s="T127">v-v:cvb</ta>
            <ta e="T129" id="Seg_6705" s="T128">v-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T130" id="Seg_6706" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_6707" s="T130">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T132" id="Seg_6708" s="T131">v-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T133" id="Seg_6709" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_6710" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_6711" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_6712" s="T135">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T137" id="Seg_6713" s="T136">v-v:ptcp</ta>
            <ta e="T138" id="Seg_6714" s="T137">v-v:tense-v:poss.pn</ta>
            <ta e="T139" id="Seg_6715" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_6716" s="T139">adv</ta>
            <ta e="T141" id="Seg_6717" s="T140">v-v:tense-v:poss.pn</ta>
            <ta e="T142" id="Seg_6718" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_6719" s="T142">v-v:ptcp-v:(case)</ta>
            <ta e="T144" id="Seg_6720" s="T143">v-v:cvb</ta>
            <ta e="T145" id="Seg_6721" s="T144">adj</ta>
            <ta e="T146" id="Seg_6722" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_6723" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_6724" s="T147">n-n:poss-n:case</ta>
            <ta e="T149" id="Seg_6725" s="T148">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T150" id="Seg_6726" s="T149">v-v:cvb-v:pred.pn</ta>
            <ta e="T151" id="Seg_6727" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_6728" s="T151">dempro-pro:case</ta>
            <ta e="T153" id="Seg_6729" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_6730" s="T153">v-v:cvb</ta>
            <ta e="T155" id="Seg_6731" s="T154">v-v:neg-v:poss.pn</ta>
            <ta e="T156" id="Seg_6732" s="T155">que</ta>
            <ta e="T157" id="Seg_6733" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_6734" s="T157">n-n:(ins)-n:case</ta>
            <ta e="T159" id="Seg_6735" s="T158">v</ta>
            <ta e="T160" id="Seg_6736" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_6737" s="T160">v-v:neg-v:poss.pn</ta>
            <ta e="T162" id="Seg_6738" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_6739" s="T162">v-v:cvb-v:pred.pn</ta>
            <ta e="T164" id="Seg_6740" s="T163">pers-pro:case</ta>
            <ta e="T165" id="Seg_6741" s="T164">v-v:tense-v:poss.pn</ta>
            <ta e="T166" id="Seg_6742" s="T165">ptcl.[ptcl:(pred.pn)]</ta>
            <ta e="T167" id="Seg_6743" s="T166">v-v:ptcp</ta>
            <ta e="T168" id="Seg_6744" s="T167">v-v:tense-v:poss.pn</ta>
            <ta e="T169" id="Seg_6745" s="T168">adv</ta>
            <ta e="T170" id="Seg_6746" s="T169">adj</ta>
            <ta e="T171" id="Seg_6747" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_6748" s="T171">v-v:ptcp</ta>
            <ta e="T173" id="Seg_6749" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_6750" s="T173">v-v:ptcp</ta>
            <ta e="T175" id="Seg_6751" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_6752" s="T175">adv</ta>
            <ta e="T177" id="Seg_6753" s="T176">que</ta>
            <ta e="T178" id="Seg_6754" s="T177">ptcl</ta>
            <ta e="T180" id="Seg_6755" s="T178">v-v:cvb</ta>
            <ta e="T181" id="Seg_6756" s="T180">v-v:cvb</ta>
            <ta e="T182" id="Seg_6757" s="T181">v-v:cvb-v:pred.pn</ta>
            <ta e="T183" id="Seg_6758" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_6759" s="T183">v-v:cvb-v-v:cvb</ta>
            <ta e="T185" id="Seg_6760" s="T184">dempro</ta>
            <ta e="T186" id="Seg_6761" s="T185">n-n:(num)-n:case</ta>
            <ta e="T187" id="Seg_6762" s="T186">v-v:cvb</ta>
            <ta e="T188" id="Seg_6763" s="T187">v-v:cvb</ta>
            <ta e="T189" id="Seg_6764" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_6765" s="T189">v-v:tense-v:poss.pn</ta>
            <ta e="T191" id="Seg_6766" s="T190">cardnum</ta>
            <ta e="T192" id="Seg_6767" s="T191">cardnum-cardnum&gt;ordnum-ordnum&gt;adj-n:(ins)-n:case</ta>
            <ta e="T193" id="Seg_6768" s="T192">cardnum</ta>
            <ta e="T194" id="Seg_6769" s="T193">cardnum-cardnum&gt;adj-n:case</ta>
            <ta e="T198" id="Seg_6770" s="T196">n-n:case</ta>
            <ta e="T199" id="Seg_6771" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_6772" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_6773" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_6774" s="T201">v-v&gt;v-v:cvb</ta>
            <ta e="T203" id="Seg_6775" s="T202">v-v&gt;v-v:cvb</ta>
            <ta e="T204" id="Seg_6776" s="T203">v</ta>
            <ta e="T205" id="Seg_6777" s="T204">n-n:(ins)-n:case</ta>
            <ta e="T206" id="Seg_6778" s="T205">adv</ta>
            <ta e="T207" id="Seg_6779" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_6780" s="T207">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_6781" s="T208">v-v:tense-v:poss.pn</ta>
            <ta e="T210" id="Seg_6782" s="T209">pers-pro:case</ta>
            <ta e="T211" id="Seg_6783" s="T210">que</ta>
            <ta e="T212" id="Seg_6784" s="T211">adv</ta>
            <ta e="T213" id="Seg_6785" s="T212">n-n&gt;adj-n:case</ta>
            <ta e="T214" id="Seg_6786" s="T213">dempro-pro:case</ta>
            <ta e="T215" id="Seg_6787" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_6788" s="T215">v-v:tense-v:pred.pn</ta>
            <ta e="T217" id="Seg_6789" s="T216">que</ta>
            <ta e="T218" id="Seg_6790" s="T217">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T219" id="Seg_6791" s="T218">adv&gt;adv-adv</ta>
            <ta e="T220" id="Seg_6792" s="T219">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T221" id="Seg_6793" s="T220">n-n:poss-n:case</ta>
            <ta e="T222" id="Seg_6794" s="T221">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T223" id="Seg_6795" s="T222">n-n:poss-n:case</ta>
            <ta e="T224" id="Seg_6796" s="T223">n-n:(ins)-n:case</ta>
            <ta e="T225" id="Seg_6797" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_6798" s="T225">n-n:poss-n:case</ta>
            <ta e="T227" id="Seg_6799" s="T226">v-v:cvb-v:pred.pn</ta>
            <ta e="T228" id="Seg_6800" s="T227">adv</ta>
            <ta e="T229" id="Seg_6801" s="T228">dempro</ta>
            <ta e="T230" id="Seg_6802" s="T229">n-n:(num)-n:case</ta>
            <ta e="T231" id="Seg_6803" s="T230">v-v:tense-v:poss.pn</ta>
            <ta e="T232" id="Seg_6804" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_6805" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_6806" s="T233">adv</ta>
            <ta e="T235" id="Seg_6807" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_6808" s="T235">n-n:(poss)-n:case</ta>
            <ta e="T237" id="Seg_6809" s="T236">adj-n:case</ta>
            <ta e="T238" id="Seg_6810" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_6811" s="T238">v-v:cvb</ta>
            <ta e="T240" id="Seg_6812" s="T239">dempro-pro:case</ta>
            <ta e="T241" id="Seg_6813" s="T240">post</ta>
            <ta e="T242" id="Seg_6814" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_6815" s="T242">v-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T244" id="Seg_6816" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_6817" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_6818" s="T245">v-v:tense-v:pred.pn</ta>
            <ta e="T247" id="Seg_6819" s="T246">dempro</ta>
            <ta e="T248" id="Seg_6820" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_6821" s="T248">n-n:poss-n:case</ta>
            <ta e="T250" id="Seg_6822" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_6823" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_6824" s="T251">n-n&gt;adj</ta>
            <ta e="T253" id="Seg_6825" s="T252">n-n&gt;adj</ta>
            <ta e="T254" id="Seg_6826" s="T253">n-n:(num)-n:case</ta>
            <ta e="T255" id="Seg_6827" s="T254">v-v:ptcp</ta>
            <ta e="T256" id="Seg_6828" s="T255">v-v:tense-v:poss.pn</ta>
            <ta e="T257" id="Seg_6829" s="T256">dempro</ta>
            <ta e="T258" id="Seg_6830" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_6831" s="T258">adj</ta>
            <ta e="T260" id="Seg_6832" s="T259">adj-n:case</ta>
            <ta e="T261" id="Seg_6833" s="T260">n-n:(num)-n:poss-n:case</ta>
            <ta e="T262" id="Seg_6834" s="T261">v-v&gt;v-v:ptcp</ta>
            <ta e="T263" id="Seg_6835" s="T262">n-n:(num)-n:case</ta>
            <ta e="T265" id="Seg_6836" s="T264">n-n&gt;v-v:ptcp</ta>
            <ta e="T266" id="Seg_6837" s="T265">conj</ta>
            <ta e="T267" id="Seg_6838" s="T266">v-v:tense-v:poss.pn</ta>
            <ta e="T268" id="Seg_6839" s="T267">dempro-pro:(num)-pro:case</ta>
            <ta e="T269" id="Seg_6840" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_6841" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_6842" s="T270">v-v:ptcp</ta>
            <ta e="T272" id="Seg_6843" s="T271">v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_6844" s="T272">dempro</ta>
            <ta e="T274" id="Seg_6845" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_6846" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_6847" s="T275">dempro</ta>
            <ta e="T277" id="Seg_6848" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_6849" s="T277">adj</ta>
            <ta e="T279" id="Seg_6850" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_6851" s="T279">n-n:case</ta>
            <ta e="T281" id="Seg_6852" s="T280">post</ta>
            <ta e="T282" id="Seg_6853" s="T281">v-v:cvb</ta>
            <ta e="T283" id="Seg_6854" s="T282">v-v:cvb</ta>
            <ta e="T284" id="Seg_6855" s="T283">dempro</ta>
            <ta e="T285" id="Seg_6856" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_6857" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_6858" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_6859" s="T287">v-v:ptcp</ta>
            <ta e="T289" id="Seg_6860" s="T288">v-v:tense-v:poss.pn</ta>
            <ta e="T290" id="Seg_6861" s="T289">v-v:ptcp</ta>
            <ta e="T291" id="Seg_6862" s="T290">conj</ta>
            <ta e="T292" id="Seg_6863" s="T291">v-v:tense-v:poss.pn</ta>
            <ta e="T293" id="Seg_6864" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_6865" s="T293">dempro</ta>
            <ta e="T295" id="Seg_6866" s="T294">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T296" id="Seg_6867" s="T295">post</ta>
            <ta e="T297" id="Seg_6868" s="T296">n-n:(ins)-n:case</ta>
            <ta e="T298" id="Seg_6869" s="T297">dempro</ta>
            <ta e="T299" id="Seg_6870" s="T298">propr-n:case</ta>
            <ta e="T300" id="Seg_6871" s="T299">n-n:(poss)-n:case</ta>
            <ta e="T301" id="Seg_6872" s="T300">v-v:tense-v:poss.pn</ta>
            <ta e="T302" id="Seg_6873" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_6874" s="T302">dempro-pro:case</ta>
            <ta e="T304" id="Seg_6875" s="T303">v-v:cvb</ta>
            <ta e="T305" id="Seg_6876" s="T304">v-v:tense-v:pred.pn</ta>
            <ta e="T306" id="Seg_6877" s="T305">pers-pro:case</ta>
            <ta e="T307" id="Seg_6878" s="T306">adv</ta>
            <ta e="T308" id="Seg_6879" s="T307">dempro</ta>
            <ta e="T309" id="Seg_6880" s="T308">n-n:case</ta>
            <ta e="T310" id="Seg_6881" s="T309">conj</ta>
            <ta e="T311" id="Seg_6882" s="T310">v-v&gt;n-n:(poss)-n:case</ta>
            <ta e="T312" id="Seg_6883" s="T311">v-v:ptcp</ta>
            <ta e="T313" id="Seg_6884" s="T312">v-v:tense-v:poss.pn</ta>
            <ta e="T314" id="Seg_6885" s="T313">dempro</ta>
            <ta e="T315" id="Seg_6886" s="T314">n-n:case</ta>
            <ta e="T316" id="Seg_6887" s="T315">conj</ta>
            <ta e="T317" id="Seg_6888" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_6889" s="T317">v-v:ptcp</ta>
            <ta e="T319" id="Seg_6890" s="T318">v-v:tense-v:poss.pn</ta>
            <ta e="T320" id="Seg_6891" s="T319">pers-pro:case</ta>
            <ta e="T321" id="Seg_6892" s="T320">dempro-pro:case</ta>
            <ta e="T322" id="Seg_6893" s="T321">post</ta>
            <ta e="T323" id="Seg_6894" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_6895" s="T323">n-n:(num)-n:case</ta>
            <ta e="T325" id="Seg_6896" s="T324">n-n:case</ta>
            <ta e="T327" id="Seg_6897" s="T325">v-v:cvb-v:pred.pn</ta>
            <ta e="T328" id="Seg_6898" s="T327">adv</ta>
            <ta e="T329" id="Seg_6899" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_6900" s="T329">v-v:cvb</ta>
            <ta e="T331" id="Seg_6901" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_6902" s="T331">adv</ta>
            <ta e="T333" id="Seg_6903" s="T332">que-pro:case</ta>
            <ta e="T334" id="Seg_6904" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_6905" s="T334">v-v:tense-v:poss.pn</ta>
            <ta e="T336" id="Seg_6906" s="T335">v-v:(neg)-v:pred.pn</ta>
            <ta e="T337" id="Seg_6907" s="T336">adj</ta>
            <ta e="T338" id="Seg_6908" s="T337">n-n:(poss)-n:case</ta>
            <ta e="T339" id="Seg_6909" s="T338">adj-n:case</ta>
            <ta e="T340" id="Seg_6910" s="T339">adv</ta>
            <ta e="T341" id="Seg_6911" s="T340">n-n:case</ta>
            <ta e="T342" id="Seg_6912" s="T341">v-v:tense-v:poss.pn</ta>
            <ta e="T343" id="Seg_6913" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_6914" s="T343">v-v:tense-v:poss.pn</ta>
            <ta e="T345" id="Seg_6915" s="T344">adv</ta>
            <ta e="T346" id="Seg_6916" s="T345">adj</ta>
            <ta e="T347" id="Seg_6917" s="T346">n-n:(num)-n:case</ta>
            <ta e="T348" id="Seg_6918" s="T347">n-n:(ins)-n:case</ta>
            <ta e="T349" id="Seg_6919" s="T348">n-n:case</ta>
            <ta e="T350" id="Seg_6920" s="T349">post</ta>
            <ta e="T351" id="Seg_6921" s="T350">v-v:tense-v:pred.pn</ta>
            <ta e="T352" id="Seg_6922" s="T351">adv</ta>
            <ta e="T353" id="Seg_6923" s="T352">adv</ta>
            <ta e="T354" id="Seg_6924" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_6925" s="T354">n-n:poss-n:case</ta>
            <ta e="T356" id="Seg_6926" s="T355">v-v:(neg)-v:pred.pn</ta>
            <ta e="T357" id="Seg_6927" s="T356">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T358" id="Seg_6928" s="T357">v-v:cvb</ta>
            <ta e="T359" id="Seg_6929" s="T358">v-v:tense-v:poss.pn</ta>
            <ta e="T362" id="Seg_6930" s="T361">pers-pro:case</ta>
            <ta e="T363" id="Seg_6931" s="T362">n-n:poss-n:case</ta>
            <ta e="T364" id="Seg_6932" s="T363">adj</ta>
            <ta e="T365" id="Seg_6933" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_6934" s="T365">adv</ta>
            <ta e="T367" id="Seg_6935" s="T366">v-v:tense-v:poss.pn</ta>
            <ta e="T368" id="Seg_6936" s="T367">v-v:tense-v:pred.pn</ta>
            <ta e="T369" id="Seg_6937" s="T368">dempro</ta>
            <ta e="T370" id="Seg_6938" s="T369">v-v:ptcp</ta>
            <ta e="T372" id="Seg_6939" s="T370">n-n:poss-n:case</ta>
            <ta e="T373" id="Seg_6940" s="T372">adv</ta>
            <ta e="T374" id="Seg_6941" s="T373">adj-n:case</ta>
            <ta e="T375" id="Seg_6942" s="T374">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T376" id="Seg_6943" s="T375">adj-n:case</ta>
            <ta e="T377" id="Seg_6944" s="T376">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T378" id="Seg_6945" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_6946" s="T378">conj</ta>
            <ta e="T380" id="Seg_6947" s="T379">v-v:tense-v:pred.pn</ta>
            <ta e="T381" id="Seg_6948" s="T380">n-n:case</ta>
            <ta e="T382" id="Seg_6949" s="T381">conj</ta>
            <ta e="T383" id="Seg_6950" s="T382">v-v:tense-v:pred.pn</ta>
            <ta e="T384" id="Seg_6951" s="T383">dempro</ta>
            <ta e="T385" id="Seg_6952" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_6953" s="T385">adv</ta>
            <ta e="T387" id="Seg_6954" s="T386">posspr</ta>
            <ta e="T388" id="Seg_6955" s="T387">dempro</ta>
            <ta e="T389" id="Seg_6956" s="T388">adj</ta>
            <ta e="T390" id="Seg_6957" s="T389">n-n:(poss)-n:case</ta>
            <ta e="T391" id="Seg_6958" s="T390">dempro</ta>
            <ta e="T392" id="Seg_6959" s="T391">propr-n:case</ta>
            <ta e="T393" id="Seg_6960" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_6961" s="T393">dempro</ta>
            <ta e="T395" id="Seg_6962" s="T394">n-n:(poss)-n:case</ta>
            <ta e="T396" id="Seg_6963" s="T395">v-v:tense-v:poss.pn</ta>
            <ta e="T397" id="Seg_6964" s="T396">conj</ta>
            <ta e="T398" id="Seg_6965" s="T397">v-v:tense-v:poss.pn</ta>
            <ta e="T399" id="Seg_6966" s="T398">pers-pro:case</ta>
            <ta e="T400" id="Seg_6967" s="T399">n-n:poss-n:case</ta>
            <ta e="T401" id="Seg_6968" s="T400">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T402" id="Seg_6969" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_6970" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_6971" s="T403">v-v:tense-v:poss.pn</ta>
            <ta e="T405" id="Seg_6972" s="T404">adv</ta>
            <ta e="T406" id="Seg_6973" s="T405">adv</ta>
            <ta e="T407" id="Seg_6974" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_6975" s="T407">adv</ta>
            <ta e="T409" id="Seg_6976" s="T408">n-n&gt;v-v:cvb</ta>
            <ta e="T410" id="Seg_6977" s="T409">v-v:tense-v:pred.pn</ta>
            <ta e="T411" id="Seg_6978" s="T410">adv</ta>
            <ta e="T412" id="Seg_6979" s="T411">cardnum</ta>
            <ta e="T413" id="Seg_6980" s="T412">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T414" id="Seg_6981" s="T413">dempro-pro:case</ta>
            <ta e="T415" id="Seg_6982" s="T414">ptcl</ta>
            <ta e="T416" id="Seg_6983" s="T415">n-n:poss-n:case</ta>
            <ta e="T417" id="Seg_6984" s="T416">n-n:poss-n:case</ta>
            <ta e="T418" id="Seg_6985" s="T417">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T419" id="Seg_6986" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_6987" s="T419">adj-n:case</ta>
            <ta e="T421" id="Seg_6988" s="T420">v-v:cvb</ta>
            <ta e="T422" id="Seg_6989" s="T421">n-n&gt;v-v:cvb</ta>
            <ta e="T423" id="Seg_6990" s="T422">v-v:tense-v:pred.pn</ta>
            <ta e="T424" id="Seg_6991" s="T423">adv</ta>
            <ta e="T425" id="Seg_6992" s="T424">dempro</ta>
            <ta e="T426" id="Seg_6993" s="T425">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T427" id="Seg_6994" s="T426">n-n:poss-n:case</ta>
            <ta e="T428" id="Seg_6995" s="T427">adj-n:case</ta>
            <ta e="T429" id="Seg_6996" s="T428">dempro-pro:case</ta>
            <ta e="T430" id="Seg_6997" s="T429">post</ta>
            <ta e="T431" id="Seg_6998" s="T430">pers-pro:case</ta>
            <ta e="T432" id="Seg_6999" s="T431">conj</ta>
            <ta e="T433" id="Seg_7000" s="T432">n-n:poss-n:case</ta>
            <ta e="T434" id="Seg_7001" s="T433">adj-n:case</ta>
            <ta e="T435" id="Seg_7002" s="T434">adv</ta>
            <ta e="T436" id="Seg_7003" s="T435">dempro</ta>
            <ta e="T437" id="Seg_7004" s="T436">n-n:case</ta>
            <ta e="T438" id="Seg_7005" s="T437">v-v&gt;n-n:(poss)-n:case</ta>
            <ta e="T439" id="Seg_7006" s="T438">adv</ta>
            <ta e="T440" id="Seg_7007" s="T439">dempro</ta>
            <ta e="T441" id="Seg_7008" s="T440">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T442" id="Seg_7009" s="T441">dempro-pro:case</ta>
            <ta e="T443" id="Seg_7010" s="T442">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T444" id="Seg_7011" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_7012" s="T444">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T446" id="Seg_7013" s="T445">n-n:case</ta>
            <ta e="T447" id="Seg_7014" s="T446">v-v:tense-v:pred.pn</ta>
            <ta e="T448" id="Seg_7015" s="T447">adv</ta>
            <ta e="T449" id="Seg_7016" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_7017" s="T449">adj-n:case</ta>
            <ta e="T451" id="Seg_7018" s="T450">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T452" id="Seg_7019" s="T451">adv</ta>
            <ta e="T453" id="Seg_7020" s="T452">propr-n:case</ta>
            <ta e="T454" id="Seg_7021" s="T453">propr</ta>
            <ta e="T455" id="Seg_7022" s="T454">propr-n:case</ta>
            <ta e="T458" id="Seg_7023" s="T457">dempro</ta>
            <ta e="T459" id="Seg_7024" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_7025" s="T459">adv</ta>
            <ta e="T461" id="Seg_7026" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_7027" s="T461">v-v:tense-v:pred.pn</ta>
            <ta e="T463" id="Seg_7028" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_7029" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_7030" s="T464">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T466" id="Seg_7031" s="T465">n-n:(poss)-n:case</ta>
            <ta e="T467" id="Seg_7032" s="T466">n-n:poss-n:case</ta>
            <ta e="T468" id="Seg_7033" s="T467">conj</ta>
            <ta e="T469" id="Seg_7034" s="T468">n-n&gt;adj</ta>
            <ta e="T470" id="Seg_7035" s="T469">n-n:case</ta>
            <ta e="T471" id="Seg_7036" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_7037" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_7038" s="T472">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T474" id="Seg_7039" s="T473">n-n:case</ta>
            <ta e="T475" id="Seg_7040" s="T474">n-n:poss-n:case</ta>
            <ta e="T476" id="Seg_7041" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_7042" s="T476">adj-adj&gt;adv</ta>
            <ta e="T478" id="Seg_7043" s="T477">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T479" id="Seg_7044" s="T478">v-v&gt;v-v:cvb</ta>
            <ta e="T480" id="Seg_7045" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_7046" s="T480">conj</ta>
            <ta e="T482" id="Seg_7047" s="T481">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T483" id="Seg_7048" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_7049" s="T483">conj</ta>
            <ta e="T485" id="Seg_7050" s="T484">n-n&gt;adj-n:case</ta>
            <ta e="T486" id="Seg_7051" s="T485">n-n:case</ta>
            <ta e="T487" id="Seg_7052" s="T486">que</ta>
            <ta e="T488" id="Seg_7053" s="T487">adj-adj&gt;adv</ta>
            <ta e="T489" id="Seg_7054" s="T488">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T490" id="Seg_7055" s="T489">dempro</ta>
            <ta e="T491" id="Seg_7056" s="T490">adv</ta>
            <ta e="T492" id="Seg_7057" s="T491">v-v:tense-v:pred.pn</ta>
            <ta e="T493" id="Seg_7058" s="T492">que</ta>
            <ta e="T494" id="Seg_7059" s="T493">adj-adj&gt;adv</ta>
            <ta e="T495" id="Seg_7060" s="T494">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T496" id="Seg_7061" s="T495">dempro</ta>
            <ta e="T497" id="Seg_7062" s="T496">adv</ta>
            <ta e="T498" id="Seg_7063" s="T497">v-v:tense-v:pred.pn</ta>
            <ta e="T499" id="Seg_7064" s="T498">pers-pro:case</ta>
            <ta e="T500" id="Seg_7065" s="T499">n-n:poss-n:case</ta>
            <ta e="T501" id="Seg_7066" s="T500">adv&gt;adv-adv</ta>
            <ta e="T502" id="Seg_7067" s="T501">dempro</ta>
            <ta e="T503" id="Seg_7068" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_7069" s="T503">adv</ta>
            <ta e="T505" id="Seg_7070" s="T504">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T506" id="Seg_7071" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_7072" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_7073" s="T507">dempro</ta>
            <ta e="T509" id="Seg_7074" s="T508">n-n:(ins)-n:case</ta>
            <ta e="T510" id="Seg_7075" s="T509">que-pro:(ins)-pro:case</ta>
            <ta e="T511" id="Seg_7076" s="T510">v-v&gt;v-v:cvb</ta>
            <ta e="T512" id="Seg_7077" s="T511">v-v:ptcp</ta>
            <ta e="T513" id="Seg_7078" s="T512">v-v:tense-v:poss.pn</ta>
            <ta e="T514" id="Seg_7079" s="T513">adv</ta>
            <ta e="T515" id="Seg_7080" s="T514">adv</ta>
            <ta e="T516" id="Seg_7081" s="T515">adj</ta>
            <ta e="T517" id="Seg_7082" s="T516">n-n:(num)-n:case</ta>
            <ta e="T518" id="Seg_7083" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_7084" s="T518">v-v:cvb-v:pred.pn</ta>
            <ta e="T520" id="Seg_7085" s="T519">pers-pro:case</ta>
            <ta e="T521" id="Seg_7086" s="T520">adv</ta>
            <ta e="T522" id="Seg_7087" s="T521">v-v:mood-v:pred.pn</ta>
            <ta e="T523" id="Seg_7088" s="T522">adv</ta>
            <ta e="T524" id="Seg_7089" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_7090" s="T524">que-pro:case</ta>
            <ta e="T526" id="Seg_7091" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_7092" s="T526">n-n:case</ta>
            <ta e="T528" id="Seg_7093" s="T527">n-n:(poss)</ta>
            <ta e="T529" id="Seg_7094" s="T528">ptcl</ta>
            <ta e="T530" id="Seg_7095" s="T529">adv</ta>
            <ta e="T531" id="Seg_7096" s="T530">v-v:tense-v:poss.pn</ta>
            <ta e="T532" id="Seg_7097" s="T531">v-v:cvb-v:pred.pn</ta>
            <ta e="T533" id="Seg_7098" s="T532">n-n&gt;v-v:cvb</ta>
            <ta e="T534" id="Seg_7099" s="T533">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T535" id="Seg_7100" s="T534">v-v:tense-v:poss.pn</ta>
            <ta e="T536" id="Seg_7101" s="T535">adv</ta>
            <ta e="T537" id="Seg_7102" s="T536">dempro</ta>
            <ta e="T538" id="Seg_7103" s="T537">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T539" id="Seg_7104" s="T538">n-n:case</ta>
            <ta e="T540" id="Seg_7105" s="T539">v-v:cvb-v:pred.pn</ta>
            <ta e="T541" id="Seg_7106" s="T540">adj-adj&gt;adv</ta>
            <ta e="T544" id="Seg_7107" s="T543">v-v:tense-v:pred.pn</ta>
            <ta e="T545" id="Seg_7108" s="T544">adv</ta>
            <ta e="T546" id="Seg_7109" s="T545">adv</ta>
            <ta e="T547" id="Seg_7110" s="T546">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T548" id="Seg_7111" s="T547">v-v:tense-v:pred.pn</ta>
            <ta e="T549" id="Seg_7112" s="T548">dempro</ta>
            <ta e="T550" id="Seg_7113" s="T549">v-v:cvb</ta>
            <ta e="T551" id="Seg_7114" s="T550">v-v:cvb-v:pred.pn</ta>
            <ta e="T552" id="Seg_7115" s="T551">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T553" id="Seg_7116" s="T552">n-n:(poss)</ta>
            <ta e="T554" id="Seg_7117" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_7118" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_7119" s="T555">v-v:tense-v:poss.pn</ta>
            <ta e="T557" id="Seg_7120" s="T556">dempro-pro:case</ta>
            <ta e="T558" id="Seg_7121" s="T557">post</ta>
            <ta e="T559" id="Seg_7122" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_7123" s="T559">pers-pro:case</ta>
            <ta e="T561" id="Seg_7124" s="T560">n-n:case</ta>
            <ta e="T562" id="Seg_7125" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_7126" s="T562">v-v:cvb</ta>
            <ta e="T564" id="Seg_7127" s="T563">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T565" id="Seg_7128" s="T564">v-v:tense-v:poss.pn</ta>
            <ta e="T566" id="Seg_7129" s="T565">que</ta>
            <ta e="T567" id="Seg_7130" s="T566">dempro</ta>
            <ta e="T568" id="Seg_7131" s="T567">pers-pro:case</ta>
            <ta e="T569" id="Seg_7132" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_7133" s="T569">v-v:mood-v:temp.pn</ta>
            <ta e="T571" id="Seg_7134" s="T570">interj</ta>
            <ta e="T572" id="Seg_7135" s="T571">que-pro:case</ta>
            <ta e="T573" id="Seg_7136" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_7137" s="T573">n-n:(ins)-n:case</ta>
            <ta e="T575" id="Seg_7138" s="T574">cardnum</ta>
            <ta e="T576" id="Seg_7139" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_7140" s="T576">n-n&gt;n-n:case</ta>
            <ta e="T578" id="Seg_7141" s="T577">quant</ta>
            <ta e="T579" id="Seg_7142" s="T578">n-n:case</ta>
            <ta e="T580" id="Seg_7143" s="T579">n-n:poss-n:case</ta>
            <ta e="T581" id="Seg_7144" s="T580">v-v:ptcp</ta>
            <ta e="T582" id="Seg_7145" s="T581">v-v:tense-v:poss.pn</ta>
            <ta e="T583" id="Seg_7146" s="T582">n-n:(ins)-n:case</ta>
            <ta e="T584" id="Seg_7147" s="T583">dempro</ta>
            <ta e="T585" id="Seg_7148" s="T584">n-n&gt;adj-n:case</ta>
            <ta e="T586" id="Seg_7149" s="T585">v-v:tense-v:poss.pn</ta>
            <ta e="T587" id="Seg_7150" s="T586">adv</ta>
            <ta e="T588" id="Seg_7151" s="T587">n-n:(poss)</ta>
            <ta e="T589" id="Seg_7152" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_7153" s="T589">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T591" id="Seg_7154" s="T590">n-n:case</ta>
            <ta e="T592" id="Seg_7155" s="T591">v-v:tense-v:pred.pn</ta>
            <ta e="T593" id="Seg_7156" s="T592">que-pro:case</ta>
            <ta e="T594" id="Seg_7157" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_7158" s="T594">post</ta>
            <ta e="T596" id="Seg_7159" s="T595">adj</ta>
            <ta e="T597" id="Seg_7160" s="T596">n-n:case</ta>
            <ta e="T598" id="Seg_7161" s="T597">adj</ta>
            <ta e="T599" id="Seg_7162" s="T598">adv</ta>
            <ta e="T600" id="Seg_7163" s="T599">n-n:case</ta>
            <ta e="T601" id="Seg_7164" s="T600">v-v:tense-v:poss.pn</ta>
            <ta e="T602" id="Seg_7165" s="T601">adv</ta>
            <ta e="T603" id="Seg_7166" s="T602">adj</ta>
            <ta e="T604" id="Seg_7167" s="T603">n-n:case</ta>
            <ta e="T605" id="Seg_7168" s="T604">v-v:cvb</ta>
            <ta e="T606" id="Seg_7169" s="T605">n-n:(num)-n:poss-n:case</ta>
            <ta e="T607" id="Seg_7170" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_7171" s="T607">pers-pro:case</ta>
            <ta e="T609" id="Seg_7172" s="T608">ptcl</ta>
            <ta e="T610" id="Seg_7173" s="T609">adv-n:poss-n:case</ta>
            <ta e="T611" id="Seg_7174" s="T610">v-v:tense-v:poss.pn</ta>
            <ta e="T612" id="Seg_7175" s="T611">adv</ta>
            <ta e="T613" id="Seg_7176" s="T612">adv</ta>
            <ta e="T614" id="Seg_7177" s="T613">adj-adj&gt;adv</ta>
            <ta e="T615" id="Seg_7178" s="T614">adv</ta>
            <ta e="T616" id="Seg_7179" s="T615">adv</ta>
            <ta e="T617" id="Seg_7180" s="T616">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T618" id="Seg_7181" s="T617">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_7182" s="T0">adj</ta>
            <ta e="T2" id="Seg_7183" s="T1">n</ta>
            <ta e="T3" id="Seg_7184" s="T2">cop</ta>
            <ta e="T4" id="Seg_7185" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_7186" s="T4">adv</ta>
            <ta e="T6" id="Seg_7187" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_7188" s="T6">v</ta>
            <ta e="T8" id="Seg_7189" s="T7">adv</ta>
            <ta e="T9" id="Seg_7190" s="T8">pers</ta>
            <ta e="T10" id="Seg_7191" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_7192" s="T10">adv</ta>
            <ta e="T12" id="Seg_7193" s="T11">n</ta>
            <ta e="T13" id="Seg_7194" s="T12">cop</ta>
            <ta e="T14" id="Seg_7195" s="T13">que</ta>
            <ta e="T15" id="Seg_7196" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_7197" s="T15">n</ta>
            <ta e="T17" id="Seg_7198" s="T16">v</ta>
            <ta e="T18" id="Seg_7199" s="T17">n</ta>
            <ta e="T19" id="Seg_7200" s="T18">cop</ta>
            <ta e="T20" id="Seg_7201" s="T19">adj</ta>
            <ta e="T21" id="Seg_7202" s="T20">n</ta>
            <ta e="T22" id="Seg_7203" s="T21">cop</ta>
            <ta e="T23" id="Seg_7204" s="T22">v</ta>
            <ta e="T26" id="Seg_7205" s="T25">n</ta>
            <ta e="T27" id="Seg_7206" s="T26">v</ta>
            <ta e="T30" id="Seg_7207" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_7208" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_7209" s="T31">v</ta>
            <ta e="T33" id="Seg_7210" s="T32">n</ta>
            <ta e="T34" id="Seg_7211" s="T33">v</ta>
            <ta e="T35" id="Seg_7212" s="T34">post</ta>
            <ta e="T36" id="Seg_7213" s="T35">cop</ta>
            <ta e="T37" id="Seg_7214" s="T36">n</ta>
            <ta e="T38" id="Seg_7215" s="T37">n</ta>
            <ta e="T39" id="Seg_7216" s="T38">post</ta>
            <ta e="T40" id="Seg_7217" s="T39">adv</ta>
            <ta e="T41" id="Seg_7218" s="T40">n</ta>
            <ta e="T42" id="Seg_7219" s="T41">adv</ta>
            <ta e="T43" id="Seg_7220" s="T42">adj</ta>
            <ta e="T44" id="Seg_7221" s="T43">n</ta>
            <ta e="T45" id="Seg_7222" s="T44">n</ta>
            <ta e="T46" id="Seg_7223" s="T45">n</ta>
            <ta e="T47" id="Seg_7224" s="T46">v</ta>
            <ta e="T48" id="Seg_7225" s="T47">n</ta>
            <ta e="T49" id="Seg_7226" s="T48">n</ta>
            <ta e="T50" id="Seg_7227" s="T49">n</ta>
            <ta e="T51" id="Seg_7228" s="T50">v</ta>
            <ta e="T52" id="Seg_7229" s="T51">v</ta>
            <ta e="T53" id="Seg_7230" s="T52">aux</ta>
            <ta e="T54" id="Seg_7231" s="T53">adv</ta>
            <ta e="T55" id="Seg_7232" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_7233" s="T55">pers</ta>
            <ta e="T57" id="Seg_7234" s="T56">dempro</ta>
            <ta e="T58" id="Seg_7235" s="T57">adj</ta>
            <ta e="T59" id="Seg_7236" s="T58">n</ta>
            <ta e="T60" id="Seg_7237" s="T59">n</ta>
            <ta e="T61" id="Seg_7238" s="T60">cop</ta>
            <ta e="T64" id="Seg_7239" s="T63">n</ta>
            <ta e="T65" id="Seg_7240" s="T64">cop</ta>
            <ta e="T66" id="Seg_7241" s="T65">dempro</ta>
            <ta e="T67" id="Seg_7242" s="T66">n</ta>
            <ta e="T68" id="Seg_7243" s="T67">n</ta>
            <ta e="T69" id="Seg_7244" s="T68">adv</ta>
            <ta e="T70" id="Seg_7245" s="T69">v</ta>
            <ta e="T71" id="Seg_7246" s="T70">aux</ta>
            <ta e="T72" id="Seg_7247" s="T71">v</ta>
            <ta e="T73" id="Seg_7248" s="T72">aux</ta>
            <ta e="T74" id="Seg_7249" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_7250" s="T74">n</ta>
            <ta e="T76" id="Seg_7251" s="T75">n</ta>
            <ta e="T77" id="Seg_7252" s="T76">v</ta>
            <ta e="T78" id="Seg_7253" s="T77">aux</ta>
            <ta e="T79" id="Seg_7254" s="T78">adj</ta>
            <ta e="T80" id="Seg_7255" s="T79">adj</ta>
            <ta e="T81" id="Seg_7256" s="T80">n</ta>
            <ta e="T82" id="Seg_7257" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_7258" s="T82">v</ta>
            <ta e="T84" id="Seg_7259" s="T83">v</ta>
            <ta e="T85" id="Seg_7260" s="T84">adv</ta>
            <ta e="T86" id="Seg_7261" s="T85">adv</ta>
            <ta e="T87" id="Seg_7262" s="T86">n</ta>
            <ta e="T88" id="Seg_7263" s="T87">n</ta>
            <ta e="T89" id="Seg_7264" s="T88">v</ta>
            <ta e="T90" id="Seg_7265" s="T89">v</ta>
            <ta e="T91" id="Seg_7266" s="T90">n</ta>
            <ta e="T92" id="Seg_7267" s="T91">propr</ta>
            <ta e="T93" id="Seg_7268" s="T92">n</ta>
            <ta e="T94" id="Seg_7269" s="T93">v</ta>
            <ta e="T95" id="Seg_7270" s="T94">aux</ta>
            <ta e="T96" id="Seg_7271" s="T95">propr</ta>
            <ta e="T97" id="Seg_7272" s="T96">n</ta>
            <ta e="T98" id="Seg_7273" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_7274" s="T98">v</ta>
            <ta e="T100" id="Seg_7275" s="T99">dempro</ta>
            <ta e="T101" id="Seg_7276" s="T100">v</ta>
            <ta e="T102" id="Seg_7277" s="T101">aux</ta>
            <ta e="T103" id="Seg_7278" s="T102">n</ta>
            <ta e="T104" id="Seg_7279" s="T103">n</ta>
            <ta e="T105" id="Seg_7280" s="T104">adj</ta>
            <ta e="T106" id="Seg_7281" s="T105">adj</ta>
            <ta e="T107" id="Seg_7282" s="T106">adj</ta>
            <ta e="T108" id="Seg_7283" s="T107">n</ta>
            <ta e="T109" id="Seg_7284" s="T108">que</ta>
            <ta e="T110" id="Seg_7285" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_7286" s="T110">n</ta>
            <ta e="T112" id="Seg_7287" s="T111">que</ta>
            <ta e="T113" id="Seg_7288" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_7289" s="T113">adj</ta>
            <ta e="T115" id="Seg_7290" s="T114">adj</ta>
            <ta e="T116" id="Seg_7291" s="T115">adj</ta>
            <ta e="T117" id="Seg_7292" s="T116">n</ta>
            <ta e="T118" id="Seg_7293" s="T117">n</ta>
            <ta e="T119" id="Seg_7294" s="T118">adj</ta>
            <ta e="T120" id="Seg_7295" s="T119">n</ta>
            <ta e="T121" id="Seg_7296" s="T120">conj</ta>
            <ta e="T122" id="Seg_7297" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_7298" s="T122">v</ta>
            <ta e="T124" id="Seg_7299" s="T123">interj</ta>
            <ta e="T125" id="Seg_7300" s="T124">dempro</ta>
            <ta e="T126" id="Seg_7301" s="T125">post</ta>
            <ta e="T127" id="Seg_7302" s="T126">n</ta>
            <ta e="T128" id="Seg_7303" s="T127">v</ta>
            <ta e="T129" id="Seg_7304" s="T128">v</ta>
            <ta e="T130" id="Seg_7305" s="T129">n</ta>
            <ta e="T131" id="Seg_7306" s="T130">v</ta>
            <ta e="T132" id="Seg_7307" s="T131">v</ta>
            <ta e="T133" id="Seg_7308" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_7309" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_7310" s="T134">n</ta>
            <ta e="T136" id="Seg_7311" s="T135">v</ta>
            <ta e="T137" id="Seg_7312" s="T136">v</ta>
            <ta e="T138" id="Seg_7313" s="T137">aux</ta>
            <ta e="T139" id="Seg_7314" s="T138">n</ta>
            <ta e="T140" id="Seg_7315" s="T139">adv</ta>
            <ta e="T141" id="Seg_7316" s="T140">cop</ta>
            <ta e="T142" id="Seg_7317" s="T141">n</ta>
            <ta e="T143" id="Seg_7318" s="T142">v</ta>
            <ta e="T144" id="Seg_7319" s="T143">v</ta>
            <ta e="T145" id="Seg_7320" s="T144">adj</ta>
            <ta e="T146" id="Seg_7321" s="T145">n</ta>
            <ta e="T147" id="Seg_7322" s="T146">n</ta>
            <ta e="T148" id="Seg_7323" s="T147">n</ta>
            <ta e="T149" id="Seg_7324" s="T148">v</ta>
            <ta e="T150" id="Seg_7325" s="T149">v</ta>
            <ta e="T151" id="Seg_7326" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_7327" s="T151">dempro</ta>
            <ta e="T153" id="Seg_7328" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_7329" s="T153">cop</ta>
            <ta e="T155" id="Seg_7330" s="T154">v</ta>
            <ta e="T156" id="Seg_7331" s="T155">que</ta>
            <ta e="T157" id="Seg_7332" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_7333" s="T157">n</ta>
            <ta e="T159" id="Seg_7334" s="T158">cop</ta>
            <ta e="T160" id="Seg_7335" s="T159">n</ta>
            <ta e="T161" id="Seg_7336" s="T160">cop</ta>
            <ta e="T162" id="Seg_7337" s="T161">n</ta>
            <ta e="T163" id="Seg_7338" s="T162">cop</ta>
            <ta e="T164" id="Seg_7339" s="T163">pers</ta>
            <ta e="T165" id="Seg_7340" s="T164">v</ta>
            <ta e="T166" id="Seg_7341" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_7342" s="T166">v</ta>
            <ta e="T168" id="Seg_7343" s="T167">aux</ta>
            <ta e="T169" id="Seg_7344" s="T168">adv</ta>
            <ta e="T170" id="Seg_7345" s="T169">adj</ta>
            <ta e="T171" id="Seg_7346" s="T170">n</ta>
            <ta e="T172" id="Seg_7347" s="T171">v</ta>
            <ta e="T173" id="Seg_7348" s="T172">n</ta>
            <ta e="T174" id="Seg_7349" s="T173">v</ta>
            <ta e="T175" id="Seg_7350" s="T174">n</ta>
            <ta e="T176" id="Seg_7351" s="T175">adv</ta>
            <ta e="T177" id="Seg_7352" s="T176">que</ta>
            <ta e="T178" id="Seg_7353" s="T177">ptcl</ta>
            <ta e="T180" id="Seg_7354" s="T178">v</ta>
            <ta e="T181" id="Seg_7355" s="T180">v</ta>
            <ta e="T182" id="Seg_7356" s="T181">v</ta>
            <ta e="T183" id="Seg_7357" s="T182">n</ta>
            <ta e="T184" id="Seg_7358" s="T183">v</ta>
            <ta e="T185" id="Seg_7359" s="T184">dempro</ta>
            <ta e="T186" id="Seg_7360" s="T185">n</ta>
            <ta e="T187" id="Seg_7361" s="T186">v</ta>
            <ta e="T188" id="Seg_7362" s="T187">v</ta>
            <ta e="T189" id="Seg_7363" s="T188">n</ta>
            <ta e="T190" id="Seg_7364" s="T189">cop</ta>
            <ta e="T191" id="Seg_7365" s="T190">cardnum</ta>
            <ta e="T192" id="Seg_7366" s="T191">adj</ta>
            <ta e="T193" id="Seg_7367" s="T192">cardnum</ta>
            <ta e="T194" id="Seg_7368" s="T193">adj</ta>
            <ta e="T198" id="Seg_7369" s="T196">n</ta>
            <ta e="T199" id="Seg_7370" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_7371" s="T199">n</ta>
            <ta e="T201" id="Seg_7372" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_7373" s="T201">v</ta>
            <ta e="T203" id="Seg_7374" s="T202">v</ta>
            <ta e="T204" id="Seg_7375" s="T203">aux</ta>
            <ta e="T205" id="Seg_7376" s="T204">n</ta>
            <ta e="T206" id="Seg_7377" s="T205">adv</ta>
            <ta e="T207" id="Seg_7378" s="T206">n</ta>
            <ta e="T208" id="Seg_7379" s="T207">v</ta>
            <ta e="T209" id="Seg_7380" s="T208">aux</ta>
            <ta e="T210" id="Seg_7381" s="T209">pers</ta>
            <ta e="T211" id="Seg_7382" s="T210">que</ta>
            <ta e="T212" id="Seg_7383" s="T211">adv</ta>
            <ta e="T213" id="Seg_7384" s="T212">adj</ta>
            <ta e="T214" id="Seg_7385" s="T213">dempro</ta>
            <ta e="T215" id="Seg_7386" s="T214">n</ta>
            <ta e="T216" id="Seg_7387" s="T215">v</ta>
            <ta e="T217" id="Seg_7388" s="T216">que</ta>
            <ta e="T218" id="Seg_7389" s="T217">n</ta>
            <ta e="T219" id="Seg_7390" s="T218">adv</ta>
            <ta e="T220" id="Seg_7391" s="T219">emphpro</ta>
            <ta e="T221" id="Seg_7392" s="T220">n</ta>
            <ta e="T222" id="Seg_7393" s="T221">emphpro</ta>
            <ta e="T223" id="Seg_7394" s="T222">n</ta>
            <ta e="T224" id="Seg_7395" s="T223">n</ta>
            <ta e="T225" id="Seg_7396" s="T224">n</ta>
            <ta e="T226" id="Seg_7397" s="T225">n</ta>
            <ta e="T227" id="Seg_7398" s="T226">v</ta>
            <ta e="T228" id="Seg_7399" s="T227">adv</ta>
            <ta e="T229" id="Seg_7400" s="T228">dempro</ta>
            <ta e="T230" id="Seg_7401" s="T229">n</ta>
            <ta e="T231" id="Seg_7402" s="T230">v</ta>
            <ta e="T232" id="Seg_7403" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_7404" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_7405" s="T233">adv</ta>
            <ta e="T235" id="Seg_7406" s="T234">n</ta>
            <ta e="T236" id="Seg_7407" s="T235">n</ta>
            <ta e="T237" id="Seg_7408" s="T236">adj</ta>
            <ta e="T238" id="Seg_7409" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_7410" s="T238">v</ta>
            <ta e="T240" id="Seg_7411" s="T239">dempro</ta>
            <ta e="T241" id="Seg_7412" s="T240">post</ta>
            <ta e="T242" id="Seg_7413" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_7414" s="T242">cop</ta>
            <ta e="T244" id="Seg_7415" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_7416" s="T244">n</ta>
            <ta e="T246" id="Seg_7417" s="T245">cop</ta>
            <ta e="T247" id="Seg_7418" s="T246">dempro</ta>
            <ta e="T248" id="Seg_7419" s="T247">n</ta>
            <ta e="T249" id="Seg_7420" s="T248">n</ta>
            <ta e="T250" id="Seg_7421" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_7422" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_7423" s="T251">adj</ta>
            <ta e="T253" id="Seg_7424" s="T252">adj</ta>
            <ta e="T254" id="Seg_7425" s="T253">n</ta>
            <ta e="T255" id="Seg_7426" s="T254">cop</ta>
            <ta e="T256" id="Seg_7427" s="T255">aux</ta>
            <ta e="T257" id="Seg_7428" s="T256">dempro</ta>
            <ta e="T258" id="Seg_7429" s="T257">n</ta>
            <ta e="T259" id="Seg_7430" s="T258">adj</ta>
            <ta e="T260" id="Seg_7431" s="T259">n</ta>
            <ta e="T261" id="Seg_7432" s="T260">n</ta>
            <ta e="T262" id="Seg_7433" s="T261">v</ta>
            <ta e="T263" id="Seg_7434" s="T262">n</ta>
            <ta e="T265" id="Seg_7435" s="T264">v</ta>
            <ta e="T266" id="Seg_7436" s="T265">conj</ta>
            <ta e="T267" id="Seg_7437" s="T266">aux</ta>
            <ta e="T268" id="Seg_7438" s="T267">dempro</ta>
            <ta e="T269" id="Seg_7439" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_7440" s="T269">n</ta>
            <ta e="T271" id="Seg_7441" s="T270">v</ta>
            <ta e="T272" id="Seg_7442" s="T271">aux</ta>
            <ta e="T273" id="Seg_7443" s="T272">dempro</ta>
            <ta e="T274" id="Seg_7444" s="T273">n</ta>
            <ta e="T275" id="Seg_7445" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_7446" s="T275">dempro</ta>
            <ta e="T277" id="Seg_7447" s="T276">n</ta>
            <ta e="T278" id="Seg_7448" s="T277">adj</ta>
            <ta e="T279" id="Seg_7449" s="T278">n</ta>
            <ta e="T280" id="Seg_7450" s="T279">n</ta>
            <ta e="T281" id="Seg_7451" s="T280">post</ta>
            <ta e="T282" id="Seg_7452" s="T281">v</ta>
            <ta e="T283" id="Seg_7453" s="T282">aux</ta>
            <ta e="T284" id="Seg_7454" s="T283">dempro</ta>
            <ta e="T285" id="Seg_7455" s="T284">n</ta>
            <ta e="T286" id="Seg_7456" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_7457" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_7458" s="T287">v</ta>
            <ta e="T289" id="Seg_7459" s="T288">aux</ta>
            <ta e="T290" id="Seg_7460" s="T289">v</ta>
            <ta e="T291" id="Seg_7461" s="T290">conj</ta>
            <ta e="T292" id="Seg_7462" s="T291">aux</ta>
            <ta e="T293" id="Seg_7463" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_7464" s="T293">dempro</ta>
            <ta e="T295" id="Seg_7465" s="T294">v</ta>
            <ta e="T296" id="Seg_7466" s="T295">post</ta>
            <ta e="T297" id="Seg_7467" s="T296">n</ta>
            <ta e="T298" id="Seg_7468" s="T297">dempro</ta>
            <ta e="T299" id="Seg_7469" s="T298">propr</ta>
            <ta e="T300" id="Seg_7470" s="T299">n</ta>
            <ta e="T301" id="Seg_7471" s="T300">cop</ta>
            <ta e="T302" id="Seg_7472" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_7473" s="T302">dempro</ta>
            <ta e="T304" id="Seg_7474" s="T303">v</ta>
            <ta e="T305" id="Seg_7475" s="T304">aux</ta>
            <ta e="T306" id="Seg_7476" s="T305">pers</ta>
            <ta e="T307" id="Seg_7477" s="T306">adv</ta>
            <ta e="T308" id="Seg_7478" s="T307">dempro</ta>
            <ta e="T309" id="Seg_7479" s="T308">n</ta>
            <ta e="T310" id="Seg_7480" s="T309">conj</ta>
            <ta e="T311" id="Seg_7481" s="T310">n</ta>
            <ta e="T312" id="Seg_7482" s="T311">cop</ta>
            <ta e="T313" id="Seg_7483" s="T312">aux</ta>
            <ta e="T314" id="Seg_7484" s="T313">dempro</ta>
            <ta e="T315" id="Seg_7485" s="T314">n</ta>
            <ta e="T316" id="Seg_7486" s="T315">conj</ta>
            <ta e="T317" id="Seg_7487" s="T316">n</ta>
            <ta e="T318" id="Seg_7488" s="T317">cop</ta>
            <ta e="T319" id="Seg_7489" s="T318">aux</ta>
            <ta e="T320" id="Seg_7490" s="T319">pers</ta>
            <ta e="T321" id="Seg_7491" s="T320">dempro</ta>
            <ta e="T322" id="Seg_7492" s="T321">post</ta>
            <ta e="T323" id="Seg_7493" s="T322">n</ta>
            <ta e="T324" id="Seg_7494" s="T323">n</ta>
            <ta e="T325" id="Seg_7495" s="T324">n</ta>
            <ta e="T327" id="Seg_7496" s="T325">v</ta>
            <ta e="T328" id="Seg_7497" s="T327">adv</ta>
            <ta e="T329" id="Seg_7498" s="T328">n</ta>
            <ta e="T330" id="Seg_7499" s="T329">v</ta>
            <ta e="T331" id="Seg_7500" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_7501" s="T331">adv</ta>
            <ta e="T333" id="Seg_7502" s="T332">que</ta>
            <ta e="T334" id="Seg_7503" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_7504" s="T334">cop</ta>
            <ta e="T336" id="Seg_7505" s="T335">v</ta>
            <ta e="T337" id="Seg_7506" s="T336">adj</ta>
            <ta e="T338" id="Seg_7507" s="T337">n</ta>
            <ta e="T339" id="Seg_7508" s="T338">adj</ta>
            <ta e="T340" id="Seg_7509" s="T339">adv</ta>
            <ta e="T341" id="Seg_7510" s="T340">n</ta>
            <ta e="T342" id="Seg_7511" s="T341">v</ta>
            <ta e="T343" id="Seg_7512" s="T342">n</ta>
            <ta e="T344" id="Seg_7513" s="T343">v</ta>
            <ta e="T345" id="Seg_7514" s="T344">adv</ta>
            <ta e="T346" id="Seg_7515" s="T345">adj</ta>
            <ta e="T347" id="Seg_7516" s="T346">n</ta>
            <ta e="T348" id="Seg_7517" s="T347">n</ta>
            <ta e="T349" id="Seg_7518" s="T348">n</ta>
            <ta e="T350" id="Seg_7519" s="T349">post</ta>
            <ta e="T351" id="Seg_7520" s="T350">cop</ta>
            <ta e="T352" id="Seg_7521" s="T351">adv</ta>
            <ta e="T353" id="Seg_7522" s="T352">adv</ta>
            <ta e="T354" id="Seg_7523" s="T353">n</ta>
            <ta e="T355" id="Seg_7524" s="T354">n</ta>
            <ta e="T356" id="Seg_7525" s="T355">v</ta>
            <ta e="T357" id="Seg_7526" s="T356">emphpro</ta>
            <ta e="T358" id="Seg_7527" s="T357">v</ta>
            <ta e="T359" id="Seg_7528" s="T358">aux</ta>
            <ta e="T362" id="Seg_7529" s="T361">pers</ta>
            <ta e="T363" id="Seg_7530" s="T362">n</ta>
            <ta e="T364" id="Seg_7531" s="T363">adj</ta>
            <ta e="T365" id="Seg_7532" s="T364">n</ta>
            <ta e="T366" id="Seg_7533" s="T365">adv</ta>
            <ta e="T367" id="Seg_7534" s="T366">v</ta>
            <ta e="T368" id="Seg_7535" s="T367">v</ta>
            <ta e="T369" id="Seg_7536" s="T368">dempro</ta>
            <ta e="T370" id="Seg_7537" s="T369">v</ta>
            <ta e="T372" id="Seg_7538" s="T370">n</ta>
            <ta e="T373" id="Seg_7539" s="T372">adv</ta>
            <ta e="T374" id="Seg_7540" s="T373">adj</ta>
            <ta e="T375" id="Seg_7541" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_7542" s="T375">adj</ta>
            <ta e="T377" id="Seg_7543" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_7544" s="T377">n</ta>
            <ta e="T379" id="Seg_7545" s="T378">conj</ta>
            <ta e="T380" id="Seg_7546" s="T379">v</ta>
            <ta e="T381" id="Seg_7547" s="T380">n</ta>
            <ta e="T382" id="Seg_7548" s="T381">conj</ta>
            <ta e="T383" id="Seg_7549" s="T382">v</ta>
            <ta e="T384" id="Seg_7550" s="T383">dempro</ta>
            <ta e="T385" id="Seg_7551" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_7552" s="T385">adv</ta>
            <ta e="T387" id="Seg_7553" s="T386">posspr</ta>
            <ta e="T388" id="Seg_7554" s="T387">dempro</ta>
            <ta e="T389" id="Seg_7555" s="T388">adj</ta>
            <ta e="T390" id="Seg_7556" s="T389">n</ta>
            <ta e="T391" id="Seg_7557" s="T390">dempro</ta>
            <ta e="T392" id="Seg_7558" s="T391">propr</ta>
            <ta e="T393" id="Seg_7559" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_7560" s="T393">dempro</ta>
            <ta e="T395" id="Seg_7561" s="T394">n</ta>
            <ta e="T396" id="Seg_7562" s="T395">v</ta>
            <ta e="T397" id="Seg_7563" s="T396">conj</ta>
            <ta e="T398" id="Seg_7564" s="T397">v</ta>
            <ta e="T399" id="Seg_7565" s="T398">pers</ta>
            <ta e="T400" id="Seg_7566" s="T399">n</ta>
            <ta e="T401" id="Seg_7567" s="T400">v</ta>
            <ta e="T402" id="Seg_7568" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_7569" s="T402">n</ta>
            <ta e="T404" id="Seg_7570" s="T403">v</ta>
            <ta e="T405" id="Seg_7571" s="T404">adv</ta>
            <ta e="T406" id="Seg_7572" s="T405">adv</ta>
            <ta e="T407" id="Seg_7573" s="T406">n</ta>
            <ta e="T408" id="Seg_7574" s="T407">adv</ta>
            <ta e="T409" id="Seg_7575" s="T408">v</ta>
            <ta e="T410" id="Seg_7576" s="T409">aux</ta>
            <ta e="T411" id="Seg_7577" s="T410">adv</ta>
            <ta e="T412" id="Seg_7578" s="T411">cardnum</ta>
            <ta e="T413" id="Seg_7579" s="T412">adj</ta>
            <ta e="T414" id="Seg_7580" s="T413">dempro</ta>
            <ta e="T415" id="Seg_7581" s="T414">ptcl</ta>
            <ta e="T416" id="Seg_7582" s="T415">n</ta>
            <ta e="T417" id="Seg_7583" s="T416">n</ta>
            <ta e="T418" id="Seg_7584" s="T417">v</ta>
            <ta e="T419" id="Seg_7585" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_7586" s="T419">adj</ta>
            <ta e="T421" id="Seg_7587" s="T420">v</ta>
            <ta e="T422" id="Seg_7588" s="T421">v</ta>
            <ta e="T423" id="Seg_7589" s="T422">aux</ta>
            <ta e="T424" id="Seg_7590" s="T423">adv</ta>
            <ta e="T425" id="Seg_7591" s="T424">dempro</ta>
            <ta e="T426" id="Seg_7592" s="T425">emphpro</ta>
            <ta e="T427" id="Seg_7593" s="T426">n</ta>
            <ta e="T428" id="Seg_7594" s="T427">adj</ta>
            <ta e="T429" id="Seg_7595" s="T428">dempro</ta>
            <ta e="T430" id="Seg_7596" s="T429">post</ta>
            <ta e="T431" id="Seg_7597" s="T430">pers</ta>
            <ta e="T432" id="Seg_7598" s="T431">conj</ta>
            <ta e="T433" id="Seg_7599" s="T432">n</ta>
            <ta e="T434" id="Seg_7600" s="T433">adj</ta>
            <ta e="T435" id="Seg_7601" s="T434">adv</ta>
            <ta e="T436" id="Seg_7602" s="T435">dempro</ta>
            <ta e="T437" id="Seg_7603" s="T436">n</ta>
            <ta e="T438" id="Seg_7604" s="T437">n</ta>
            <ta e="T439" id="Seg_7605" s="T438">adv</ta>
            <ta e="T440" id="Seg_7606" s="T439">dempro</ta>
            <ta e="T441" id="Seg_7607" s="T440">n</ta>
            <ta e="T442" id="Seg_7608" s="T441">dempro</ta>
            <ta e="T443" id="Seg_7609" s="T442">ordnum</ta>
            <ta e="T444" id="Seg_7610" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_7611" s="T444">n</ta>
            <ta e="T446" id="Seg_7612" s="T445">n</ta>
            <ta e="T447" id="Seg_7613" s="T446">v</ta>
            <ta e="T448" id="Seg_7614" s="T447">adv</ta>
            <ta e="T449" id="Seg_7615" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_7616" s="T449">adj</ta>
            <ta e="T451" id="Seg_7617" s="T450">adj</ta>
            <ta e="T452" id="Seg_7618" s="T451">adv</ta>
            <ta e="T453" id="Seg_7619" s="T452">propr</ta>
            <ta e="T454" id="Seg_7620" s="T453">propr</ta>
            <ta e="T455" id="Seg_7621" s="T454">propr</ta>
            <ta e="T458" id="Seg_7622" s="T457">dempro</ta>
            <ta e="T459" id="Seg_7623" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_7624" s="T459">adv</ta>
            <ta e="T461" id="Seg_7625" s="T460">n</ta>
            <ta e="T462" id="Seg_7626" s="T461">v</ta>
            <ta e="T463" id="Seg_7627" s="T462">n</ta>
            <ta e="T464" id="Seg_7628" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_7629" s="T464">v</ta>
            <ta e="T466" id="Seg_7630" s="T465">n</ta>
            <ta e="T467" id="Seg_7631" s="T466">n</ta>
            <ta e="T468" id="Seg_7632" s="T467">conj</ta>
            <ta e="T469" id="Seg_7633" s="T468">adj</ta>
            <ta e="T470" id="Seg_7634" s="T469">n</ta>
            <ta e="T471" id="Seg_7635" s="T470">ptcl</ta>
            <ta e="T472" id="Seg_7636" s="T471">ptcl</ta>
            <ta e="T473" id="Seg_7637" s="T472">emphpro</ta>
            <ta e="T474" id="Seg_7638" s="T473">n</ta>
            <ta e="T475" id="Seg_7639" s="T474">n</ta>
            <ta e="T476" id="Seg_7640" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_7641" s="T476">adv</ta>
            <ta e="T478" id="Seg_7642" s="T477">v</ta>
            <ta e="T479" id="Seg_7643" s="T478">v</ta>
            <ta e="T480" id="Seg_7644" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_7645" s="T480">conj</ta>
            <ta e="T482" id="Seg_7646" s="T481">v</ta>
            <ta e="T483" id="Seg_7647" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_7648" s="T483">conj</ta>
            <ta e="T485" id="Seg_7649" s="T484">adj</ta>
            <ta e="T486" id="Seg_7650" s="T485">n</ta>
            <ta e="T487" id="Seg_7651" s="T486">que</ta>
            <ta e="T488" id="Seg_7652" s="T487">adv</ta>
            <ta e="T489" id="Seg_7653" s="T488">v</ta>
            <ta e="T490" id="Seg_7654" s="T489">dempro</ta>
            <ta e="T491" id="Seg_7655" s="T490">adv</ta>
            <ta e="T492" id="Seg_7656" s="T491">cop</ta>
            <ta e="T493" id="Seg_7657" s="T492">que</ta>
            <ta e="T494" id="Seg_7658" s="T493">adv</ta>
            <ta e="T495" id="Seg_7659" s="T494">v</ta>
            <ta e="T496" id="Seg_7660" s="T495">dempro</ta>
            <ta e="T497" id="Seg_7661" s="T496">adv</ta>
            <ta e="T498" id="Seg_7662" s="T497">cop</ta>
            <ta e="T499" id="Seg_7663" s="T498">pers</ta>
            <ta e="T500" id="Seg_7664" s="T499">n</ta>
            <ta e="T501" id="Seg_7665" s="T500">adv</ta>
            <ta e="T502" id="Seg_7666" s="T501">dempro</ta>
            <ta e="T503" id="Seg_7667" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_7668" s="T503">adv</ta>
            <ta e="T505" id="Seg_7669" s="T504">emphpro</ta>
            <ta e="T506" id="Seg_7670" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_7671" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_7672" s="T507">dempro</ta>
            <ta e="T509" id="Seg_7673" s="T508">n</ta>
            <ta e="T510" id="Seg_7674" s="T509">que</ta>
            <ta e="T511" id="Seg_7675" s="T510">v</ta>
            <ta e="T512" id="Seg_7676" s="T511">aux</ta>
            <ta e="T513" id="Seg_7677" s="T512">aux</ta>
            <ta e="T514" id="Seg_7678" s="T513">adv</ta>
            <ta e="T515" id="Seg_7679" s="T514">adv</ta>
            <ta e="T516" id="Seg_7680" s="T515">adj</ta>
            <ta e="T517" id="Seg_7681" s="T516">n</ta>
            <ta e="T518" id="Seg_7682" s="T517">n</ta>
            <ta e="T519" id="Seg_7683" s="T518">v</ta>
            <ta e="T520" id="Seg_7684" s="T519">pers</ta>
            <ta e="T521" id="Seg_7685" s="T520">adv</ta>
            <ta e="T522" id="Seg_7686" s="T521">v</ta>
            <ta e="T523" id="Seg_7687" s="T522">adv</ta>
            <ta e="T524" id="Seg_7688" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_7689" s="T524">que</ta>
            <ta e="T526" id="Seg_7690" s="T525">ptcl</ta>
            <ta e="T527" id="Seg_7691" s="T526">n</ta>
            <ta e="T528" id="Seg_7692" s="T527">n</ta>
            <ta e="T529" id="Seg_7693" s="T528">ptcl</ta>
            <ta e="T530" id="Seg_7694" s="T529">adv</ta>
            <ta e="T531" id="Seg_7695" s="T530">v</ta>
            <ta e="T532" id="Seg_7696" s="T531">v</ta>
            <ta e="T533" id="Seg_7697" s="T532">v</ta>
            <ta e="T534" id="Seg_7698" s="T533">v</ta>
            <ta e="T535" id="Seg_7699" s="T534">aux</ta>
            <ta e="T536" id="Seg_7700" s="T535">adv</ta>
            <ta e="T537" id="Seg_7701" s="T536">dempro</ta>
            <ta e="T538" id="Seg_7702" s="T537">n</ta>
            <ta e="T539" id="Seg_7703" s="T538">n</ta>
            <ta e="T540" id="Seg_7704" s="T539">v</ta>
            <ta e="T541" id="Seg_7705" s="T540">adv</ta>
            <ta e="T544" id="Seg_7706" s="T543">v</ta>
            <ta e="T545" id="Seg_7707" s="T544">adv</ta>
            <ta e="T546" id="Seg_7708" s="T545">adv</ta>
            <ta e="T547" id="Seg_7709" s="T546">v</ta>
            <ta e="T548" id="Seg_7710" s="T547">aux</ta>
            <ta e="T549" id="Seg_7711" s="T548">dempro</ta>
            <ta e="T550" id="Seg_7712" s="T549">v</ta>
            <ta e="T551" id="Seg_7713" s="T550">aux</ta>
            <ta e="T552" id="Seg_7714" s="T551">emphpro</ta>
            <ta e="T553" id="Seg_7715" s="T552">n</ta>
            <ta e="T554" id="Seg_7716" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_7717" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_7718" s="T555">cop</ta>
            <ta e="T557" id="Seg_7719" s="T556">dempro</ta>
            <ta e="T558" id="Seg_7720" s="T557">post</ta>
            <ta e="T559" id="Seg_7721" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_7722" s="T559">pers</ta>
            <ta e="T561" id="Seg_7723" s="T560">n</ta>
            <ta e="T562" id="Seg_7724" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_7725" s="T562">v</ta>
            <ta e="T564" id="Seg_7726" s="T563">v</ta>
            <ta e="T565" id="Seg_7727" s="T564">cop</ta>
            <ta e="T566" id="Seg_7728" s="T565">que</ta>
            <ta e="T567" id="Seg_7729" s="T566">dempro</ta>
            <ta e="T568" id="Seg_7730" s="T567">pers</ta>
            <ta e="T569" id="Seg_7731" s="T568">n</ta>
            <ta e="T570" id="Seg_7732" s="T569">cop</ta>
            <ta e="T571" id="Seg_7733" s="T570">interj</ta>
            <ta e="T572" id="Seg_7734" s="T571">que</ta>
            <ta e="T573" id="Seg_7735" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_7736" s="T573">n</ta>
            <ta e="T575" id="Seg_7737" s="T574">cardnum</ta>
            <ta e="T576" id="Seg_7738" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_7739" s="T576">n</ta>
            <ta e="T578" id="Seg_7740" s="T577">quant</ta>
            <ta e="T579" id="Seg_7741" s="T578">n</ta>
            <ta e="T580" id="Seg_7742" s="T579">n</ta>
            <ta e="T581" id="Seg_7743" s="T580">v</ta>
            <ta e="T582" id="Seg_7744" s="T581">aux</ta>
            <ta e="T583" id="Seg_7745" s="T582">n</ta>
            <ta e="T584" id="Seg_7746" s="T583">dempro</ta>
            <ta e="T585" id="Seg_7747" s="T584">adj</ta>
            <ta e="T586" id="Seg_7748" s="T585">cop</ta>
            <ta e="T587" id="Seg_7749" s="T586">adv</ta>
            <ta e="T588" id="Seg_7750" s="T587">n</ta>
            <ta e="T589" id="Seg_7751" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_7752" s="T589">emphpro</ta>
            <ta e="T591" id="Seg_7753" s="T590">n</ta>
            <ta e="T592" id="Seg_7754" s="T591">v</ta>
            <ta e="T593" id="Seg_7755" s="T592">que</ta>
            <ta e="T594" id="Seg_7756" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_7757" s="T594">post</ta>
            <ta e="T596" id="Seg_7758" s="T595">adj</ta>
            <ta e="T597" id="Seg_7759" s="T596">n</ta>
            <ta e="T598" id="Seg_7760" s="T597">adj</ta>
            <ta e="T599" id="Seg_7761" s="T598">adv</ta>
            <ta e="T600" id="Seg_7762" s="T599">n</ta>
            <ta e="T601" id="Seg_7763" s="T600">v</ta>
            <ta e="T602" id="Seg_7764" s="T601">adv</ta>
            <ta e="T603" id="Seg_7765" s="T602">adj</ta>
            <ta e="T604" id="Seg_7766" s="T603">n</ta>
            <ta e="T605" id="Seg_7767" s="T604">cop</ta>
            <ta e="T606" id="Seg_7768" s="T605">n</ta>
            <ta e="T607" id="Seg_7769" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_7770" s="T607">pers</ta>
            <ta e="T609" id="Seg_7771" s="T608">ptcl</ta>
            <ta e="T610" id="Seg_7772" s="T609">adv</ta>
            <ta e="T611" id="Seg_7773" s="T610">v</ta>
            <ta e="T612" id="Seg_7774" s="T611">adv</ta>
            <ta e="T613" id="Seg_7775" s="T612">adv</ta>
            <ta e="T614" id="Seg_7776" s="T613">adv</ta>
            <ta e="T615" id="Seg_7777" s="T614">adv</ta>
            <ta e="T616" id="Seg_7778" s="T615">adv</ta>
            <ta e="T617" id="Seg_7779" s="T616">v</ta>
            <ta e="T618" id="Seg_7780" s="T617">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_7781" s="T1">np:Th</ta>
            <ta e="T7" id="Seg_7782" s="T6">0.1.h:E</ta>
            <ta e="T8" id="Seg_7783" s="T7">adv:Time</ta>
            <ta e="T9" id="Seg_7784" s="T8">pro.h:Th</ta>
            <ta e="T19" id="Seg_7785" s="T18">0.1.h:Th</ta>
            <ta e="T21" id="Seg_7786" s="T20">np:Th</ta>
            <ta e="T26" id="Seg_7787" s="T25">np:Th</ta>
            <ta e="T33" id="Seg_7788" s="T32">np.h:E</ta>
            <ta e="T37" id="Seg_7789" s="T36">np:Th</ta>
            <ta e="T41" id="Seg_7790" s="T40">np.h:Th</ta>
            <ta e="T44" id="Seg_7791" s="T43">np.h:A</ta>
            <ta e="T46" id="Seg_7792" s="T45">np.h:A</ta>
            <ta e="T54" id="Seg_7793" s="T53">adv:Time</ta>
            <ta e="T56" id="Seg_7794" s="T55">pro.h:Th</ta>
            <ta e="T59" id="Seg_7795" s="T58">np:L</ta>
            <ta e="T65" id="Seg_7796" s="T64">0.1.h:Th</ta>
            <ta e="T68" id="Seg_7797" s="T67">n:Time</ta>
            <ta e="T71" id="Seg_7798" s="T70">0.1.h:E</ta>
            <ta e="T73" id="Seg_7799" s="T72">0.1.h:E</ta>
            <ta e="T76" id="Seg_7800" s="T75">np:Th</ta>
            <ta e="T81" id="Seg_7801" s="T80">np:Th</ta>
            <ta e="T84" id="Seg_7802" s="T83">0.3.h:A</ta>
            <ta e="T86" id="Seg_7803" s="T85">adv:Time</ta>
            <ta e="T88" id="Seg_7804" s="T87">np:L</ta>
            <ta e="T93" id="Seg_7805" s="T92">np.h:A</ta>
            <ta e="T96" id="Seg_7806" s="T95">np.h:Poss</ta>
            <ta e="T102" id="Seg_7807" s="T101">0.3:Th</ta>
            <ta e="T117" id="Seg_7808" s="T116">np.h:Poss</ta>
            <ta e="T118" id="Seg_7809" s="T117">np.h:A</ta>
            <ta e="T135" id="Seg_7810" s="T134">np:G</ta>
            <ta e="T136" id="Seg_7811" s="T135">0.1.h:A</ta>
            <ta e="T139" id="Seg_7812" s="T138">np.h:R</ta>
            <ta e="T150" id="Seg_7813" s="T149">0.3.h:A</ta>
            <ta e="T160" id="Seg_7814" s="T159">np:Th</ta>
            <ta e="T163" id="Seg_7815" s="T162">0.1.h:Th</ta>
            <ta e="T164" id="Seg_7816" s="T163">pro.h:E</ta>
            <ta e="T168" id="Seg_7817" s="T167">0.3.h:A</ta>
            <ta e="T182" id="Seg_7818" s="T181">0.3.h:A</ta>
            <ta e="T183" id="Seg_7819" s="T182">np:Th</ta>
            <ta e="T186" id="Seg_7820" s="T185">np:G</ta>
            <ta e="T190" id="Seg_7821" s="T189">0.1.h:Th</ta>
            <ta e="T192" id="Seg_7822" s="T191">n:Time</ta>
            <ta e="T194" id="Seg_7823" s="T193">n:Time</ta>
            <ta e="T200" id="Seg_7824" s="T199">np:Th</ta>
            <ta e="T206" id="Seg_7825" s="T205">adv:Time</ta>
            <ta e="T207" id="Seg_7826" s="T206">np:P</ta>
            <ta e="T210" id="Seg_7827" s="T209">pro.h:A</ta>
            <ta e="T215" id="Seg_7828" s="T214">np:L</ta>
            <ta e="T216" id="Seg_7829" s="T215">0.1.h:A</ta>
            <ta e="T225" id="Seg_7830" s="T224">np:Poss</ta>
            <ta e="T226" id="Seg_7831" s="T225">np:P</ta>
            <ta e="T227" id="Seg_7832" s="T226">0.1.h:A</ta>
            <ta e="T230" id="Seg_7833" s="T229">n:Time</ta>
            <ta e="T231" id="Seg_7834" s="T230">0.1.h:Th</ta>
            <ta e="T235" id="Seg_7835" s="T234">np:Poss</ta>
            <ta e="T236" id="Seg_7836" s="T235">np:Th</ta>
            <ta e="T246" id="Seg_7837" s="T245">0.1.h:Th</ta>
            <ta e="T249" id="Seg_7838" s="T248">n:Time</ta>
            <ta e="T254" id="Seg_7839" s="T253">np:Th</ta>
            <ta e="T263" id="Seg_7840" s="T262">np.h:A</ta>
            <ta e="T268" id="Seg_7841" s="T267">pro.h:A</ta>
            <ta e="T270" id="Seg_7842" s="T269">np:Th</ta>
            <ta e="T279" id="Seg_7843" s="T278">np:A</ta>
            <ta e="T285" id="Seg_7844" s="T284">np.h:P</ta>
            <ta e="T292" id="Seg_7845" s="T291">0.3.h:A</ta>
            <ta e="T294" id="Seg_7846" s="T293">pro.h:Th</ta>
            <ta e="T300" id="Seg_7847" s="T299">np:Th</ta>
            <ta e="T306" id="Seg_7848" s="T305">pro.h:Th</ta>
            <ta e="T311" id="Seg_7849" s="T310">np:Th</ta>
            <ta e="T317" id="Seg_7850" s="T316">np:Th</ta>
            <ta e="T320" id="Seg_7851" s="T319">pro.h:A</ta>
            <ta e="T324" id="Seg_7852" s="T323">np:L</ta>
            <ta e="T325" id="Seg_7853" s="T324">np.h:B</ta>
            <ta e="T333" id="Seg_7854" s="T332">pro:Th</ta>
            <ta e="T336" id="Seg_7855" s="T335">0.1.h:E</ta>
            <ta e="T338" id="Seg_7856" s="T337">np:Th</ta>
            <ta e="T340" id="Seg_7857" s="T339">adv:Time</ta>
            <ta e="T341" id="Seg_7858" s="T340">np:Th</ta>
            <ta e="T343" id="Seg_7859" s="T342">np:Th</ta>
            <ta e="T345" id="Seg_7860" s="T344">adv:Time</ta>
            <ta e="T347" id="Seg_7861" s="T346">np:Th</ta>
            <ta e="T355" id="Seg_7862" s="T354">np:Th</ta>
            <ta e="T356" id="Seg_7863" s="T355">0.1.h:E</ta>
            <ta e="T357" id="Seg_7864" s="T356">pro.h:Th</ta>
            <ta e="T365" id="Seg_7865" s="T364">np:G</ta>
            <ta e="T367" id="Seg_7866" s="T366">0.1.h:Th</ta>
            <ta e="T373" id="Seg_7867" s="T372">adv:Time</ta>
            <ta e="T375" id="Seg_7868" s="T374">0.1.h:Th</ta>
            <ta e="T377" id="Seg_7869" s="T376">0.1.h:Th</ta>
            <ta e="T378" id="Seg_7870" s="T377">np:Th</ta>
            <ta e="T380" id="Seg_7871" s="T379">0.1.h:A</ta>
            <ta e="T381" id="Seg_7872" s="T380">np:P</ta>
            <ta e="T383" id="Seg_7873" s="T382">0.1.h:A</ta>
            <ta e="T390" id="Seg_7874" s="T389">0.1.h:Poss </ta>
            <ta e="T392" id="Seg_7875" s="T391">np.h:Th</ta>
            <ta e="T395" id="Seg_7876" s="T394">0.1.h:Poss np.h:Th</ta>
            <ta e="T399" id="Seg_7877" s="T398">pro.h:A</ta>
            <ta e="T400" id="Seg_7878" s="T399">np:Th</ta>
            <ta e="T401" id="Seg_7879" s="T400">0.3.h:A</ta>
            <ta e="T404" id="Seg_7880" s="T403">0.3.h:A</ta>
            <ta e="T408" id="Seg_7881" s="T407">adv:Time</ta>
            <ta e="T410" id="Seg_7882" s="T409">0.3.h:A</ta>
            <ta e="T413" id="Seg_7883" s="T412">0.1.h:Poss np.h:Th</ta>
            <ta e="T414" id="Seg_7884" s="T413">pro.h:A</ta>
            <ta e="T416" id="Seg_7885" s="T415">np:Th</ta>
            <ta e="T417" id="Seg_7886" s="T416">np:Th</ta>
            <ta e="T420" id="Seg_7887" s="T419">np:Th</ta>
            <ta e="T431" id="Seg_7888" s="T430">np.h:Poss</ta>
            <ta e="T433" id="Seg_7889" s="T432">np:L</ta>
            <ta e="T445" id="Seg_7890" s="T444">0.1.h:Poss np.h:A</ta>
            <ta e="T446" id="Seg_7891" s="T445">np:L</ta>
            <ta e="T451" id="Seg_7892" s="T450">0.1.h:Poss np.h:Th</ta>
            <ta e="T458" id="Seg_7893" s="T457">pro.h:A</ta>
            <ta e="T460" id="Seg_7894" s="T459">adv:Time</ta>
            <ta e="T461" id="Seg_7895" s="T460">np:L</ta>
            <ta e="T482" id="Seg_7896" s="T481">0.3:Th</ta>
            <ta e="T485" id="Seg_7897" s="T484">0.3:Poss np:Th</ta>
            <ta e="T486" id="Seg_7898" s="T485">np.h:Th</ta>
            <ta e="T490" id="Seg_7899" s="T489">pro.h:Th</ta>
            <ta e="T496" id="Seg_7900" s="T495">pro.h:Th</ta>
            <ta e="T505" id="Seg_7901" s="T504">pro.h:A</ta>
            <ta e="T515" id="Seg_7902" s="T514">adv:Time</ta>
            <ta e="T517" id="Seg_7903" s="T516">np.h:A</ta>
            <ta e="T519" id="Seg_7904" s="T518">0.3.h:A</ta>
            <ta e="T523" id="Seg_7905" s="T522">adv:Time</ta>
            <ta e="T525" id="Seg_7906" s="T524">pro.h:R</ta>
            <ta e="T528" id="Seg_7907" s="T527">np:Th</ta>
            <ta e="T530" id="Seg_7908" s="T529">adv:Time</ta>
            <ta e="T531" id="Seg_7909" s="T530">0.1.h:A</ta>
            <ta e="T532" id="Seg_7910" s="T531">0.1.h:A</ta>
            <ta e="T534" id="Seg_7911" s="T533">0.1.h:A</ta>
            <ta e="T538" id="Seg_7912" s="T537">0.1.h:Poss np.h:Th</ta>
            <ta e="T540" id="Seg_7913" s="T539">0.3.h:A</ta>
            <ta e="T548" id="Seg_7914" s="T547">0.1.h:E</ta>
            <ta e="T551" id="Seg_7915" s="T550">0.1.h:Th</ta>
            <ta e="T552" id="Seg_7916" s="T551">pro.h:Poss</ta>
            <ta e="T553" id="Seg_7917" s="T552">np:Th</ta>
            <ta e="T560" id="Seg_7918" s="T559">pro.h:E</ta>
            <ta e="T561" id="Seg_7919" s="T560">np:Th</ta>
            <ta e="T568" id="Seg_7920" s="T567">pro.h:Th</ta>
            <ta e="T574" id="Seg_7921" s="T573">np:Com</ta>
            <ta e="T577" id="Seg_7922" s="T576">np:Th</ta>
            <ta e="T582" id="Seg_7923" s="T581">0.1.h:A</ta>
            <ta e="T583" id="Seg_7924" s="T582">np:Ins</ta>
            <ta e="T584" id="Seg_7925" s="T583">pro:Th</ta>
            <ta e="T590" id="Seg_7926" s="T589">pro.h:A</ta>
            <ta e="T595" id="Seg_7927" s="T592">pp:Com</ta>
            <ta e="T597" id="Seg_7928" s="T596">np:A</ta>
            <ta e="T600" id="Seg_7929" s="T599">np:G</ta>
            <ta e="T604" id="Seg_7930" s="T603">np:Th</ta>
            <ta e="T606" id="Seg_7931" s="T605">0.1.h:Poss np.h:R</ta>
            <ta e="T608" id="Seg_7932" s="T607">pro.h:Th</ta>
            <ta e="T618" id="Seg_7933" s="T617">0.1.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_7934" s="T0">s:comp</ta>
            <ta e="T7" id="Seg_7935" s="T6">0.1.h:S v:pred</ta>
            <ta e="T9" id="Seg_7936" s="T8">pro.h:S</ta>
            <ta e="T12" id="Seg_7937" s="T11">n:pred</ta>
            <ta e="T13" id="Seg_7938" s="T12">cop</ta>
            <ta e="T17" id="Seg_7939" s="T13">s:rel</ta>
            <ta e="T18" id="Seg_7940" s="T17">n:pred</ta>
            <ta e="T19" id="Seg_7941" s="T18">0.1.h:S cop</ta>
            <ta e="T21" id="Seg_7942" s="T20">np:S</ta>
            <ta e="T22" id="Seg_7943" s="T21">cop</ta>
            <ta e="T27" id="Seg_7944" s="T25">s:comp</ta>
            <ta e="T33" id="Seg_7945" s="T32">np.h:S</ta>
            <ta e="T36" id="Seg_7946" s="T35">v:pred</ta>
            <ta e="T39" id="Seg_7947" s="T36">s:temp</ta>
            <ta e="T41" id="Seg_7948" s="T40">np.h:O</ta>
            <ta e="T44" id="Seg_7949" s="T43">np.h:S</ta>
            <ta e="T46" id="Seg_7950" s="T45">np.h:S</ta>
            <ta e="T47" id="Seg_7951" s="T46">v:pred</ta>
            <ta e="T51" id="Seg_7952" s="T47">s:adv</ta>
            <ta e="T53" id="Seg_7953" s="T52">v:pred</ta>
            <ta e="T56" id="Seg_7954" s="T55">pro.h:S</ta>
            <ta e="T60" id="Seg_7955" s="T59">n:pred</ta>
            <ta e="T61" id="Seg_7956" s="T60">cop</ta>
            <ta e="T64" id="Seg_7957" s="T63">n:pred</ta>
            <ta e="T65" id="Seg_7958" s="T64">0.1.h:S cop</ta>
            <ta e="T71" id="Seg_7959" s="T70">0.1.h:S v:pred</ta>
            <ta e="T73" id="Seg_7960" s="T72">0.1.h:S v:pred</ta>
            <ta e="T76" id="Seg_7961" s="T75">np:S</ta>
            <ta e="T78" id="Seg_7962" s="T77">v:pred</ta>
            <ta e="T81" id="Seg_7963" s="T80">np:S</ta>
            <ta e="T83" id="Seg_7964" s="T82">v:pred</ta>
            <ta e="T84" id="Seg_7965" s="T83">0.3.h:S v:pred</ta>
            <ta e="T89" id="Seg_7966" s="T87">s:comp</ta>
            <ta e="T93" id="Seg_7967" s="T92">np.h:S</ta>
            <ta e="T95" id="Seg_7968" s="T94">v:pred</ta>
            <ta e="T99" id="Seg_7969" s="T98">s:adv</ta>
            <ta e="T102" id="Seg_7970" s="T101">0.3:S v:pred</ta>
            <ta e="T118" id="Seg_7971" s="T117">np.h:S</ta>
            <ta e="T123" id="Seg_7972" s="T122">v:pred</ta>
            <ta e="T129" id="Seg_7973" s="T128">np.h:S</ta>
            <ta e="T132" id="Seg_7974" s="T131">np.h:S</ta>
            <ta e="T136" id="Seg_7975" s="T134">s:comp</ta>
            <ta e="T138" id="Seg_7976" s="T137">v:pred</ta>
            <ta e="T139" id="Seg_7977" s="T138">np.h:O</ta>
            <ta e="T149" id="Seg_7978" s="T144">s:purp</ta>
            <ta e="T150" id="Seg_7979" s="T149">0.3.h:S v:pred</ta>
            <ta e="T155" id="Seg_7980" s="T154">v:pred</ta>
            <ta e="T160" id="Seg_7981" s="T159">np:S</ta>
            <ta e="T161" id="Seg_7982" s="T160">cop</ta>
            <ta e="T163" id="Seg_7983" s="T161">s:temp</ta>
            <ta e="T164" id="Seg_7984" s="T163">pro.h:S</ta>
            <ta e="T165" id="Seg_7985" s="T164">v:pred</ta>
            <ta e="T168" id="Seg_7986" s="T167">0.3.h:S v:pred</ta>
            <ta e="T182" id="Seg_7987" s="T181">0.3.h:S v:pred</ta>
            <ta e="T184" id="Seg_7988" s="T183">v:pred</ta>
            <ta e="T187" id="Seg_7989" s="T186">v:pred</ta>
            <ta e="T188" id="Seg_7990" s="T187">v:pred</ta>
            <ta e="T189" id="Seg_7991" s="T188">n:pred</ta>
            <ta e="T190" id="Seg_7992" s="T189">cop</ta>
            <ta e="T200" id="Seg_7993" s="T199">np:S</ta>
            <ta e="T202" id="Seg_7994" s="T201">v:pred</ta>
            <ta e="T207" id="Seg_7995" s="T206">np:O</ta>
            <ta e="T208" id="Seg_7996" s="T207">v:pred</ta>
            <ta e="T210" id="Seg_7997" s="T209">pro.h:S</ta>
            <ta e="T216" id="Seg_7998" s="T215">0.1.h:S v:pred</ta>
            <ta e="T226" id="Seg_7999" s="T225">np:O</ta>
            <ta e="T227" id="Seg_8000" s="T226">0.1.h:S v:pred</ta>
            <ta e="T231" id="Seg_8001" s="T230">0.1.h:S v:pred</ta>
            <ta e="T236" id="Seg_8002" s="T235">np:S</ta>
            <ta e="T237" id="Seg_8003" s="T236">adj:pred</ta>
            <ta e="T245" id="Seg_8004" s="T244">n:pred</ta>
            <ta e="T246" id="Seg_8005" s="T245">0.1.h:S cop</ta>
            <ta e="T254" id="Seg_8006" s="T253">np:S</ta>
            <ta e="T256" id="Seg_8007" s="T255">cop</ta>
            <ta e="T262" id="Seg_8008" s="T257">s:rel</ta>
            <ta e="T263" id="Seg_8009" s="T262">np.h:S</ta>
            <ta e="T267" id="Seg_8010" s="T266">v:pred</ta>
            <ta e="T268" id="Seg_8011" s="T267">pro.h:S</ta>
            <ta e="T270" id="Seg_8012" s="T269">np:O</ta>
            <ta e="T272" id="Seg_8013" s="T271">v:pred</ta>
            <ta e="T279" id="Seg_8014" s="T278">np:S</ta>
            <ta e="T283" id="Seg_8015" s="T279">s:temp</ta>
            <ta e="T285" id="Seg_8016" s="T284">np.h:O</ta>
            <ta e="T289" id="Seg_8017" s="T288">v:pred</ta>
            <ta e="T292" id="Seg_8018" s="T291">0.3.h:S v:pred</ta>
            <ta e="T296" id="Seg_8019" s="T293">s:temp</ta>
            <ta e="T300" id="Seg_8020" s="T299">np:S</ta>
            <ta e="T301" id="Seg_8021" s="T300">cop</ta>
            <ta e="T305" id="Seg_8022" s="T304">v:pred</ta>
            <ta e="T306" id="Seg_8023" s="T305">pro.h:S</ta>
            <ta e="T311" id="Seg_8024" s="T310">np:S</ta>
            <ta e="T313" id="Seg_8025" s="T312">cop</ta>
            <ta e="T317" id="Seg_8026" s="T316">np:S</ta>
            <ta e="T319" id="Seg_8027" s="T318">cop</ta>
            <ta e="T320" id="Seg_8028" s="T319">pro.h:S</ta>
            <ta e="T327" id="Seg_8029" s="T325">v:pred</ta>
            <ta e="T330" id="Seg_8030" s="T327">s:temp</ta>
            <ta e="T333" id="Seg_8031" s="T332">pro:S</ta>
            <ta e="T335" id="Seg_8032" s="T334">cop</ta>
            <ta e="T336" id="Seg_8033" s="T335">0.1.h:S v:pred</ta>
            <ta e="T338" id="Seg_8034" s="T337">np:S</ta>
            <ta e="T339" id="Seg_8035" s="T338">adj:pred</ta>
            <ta e="T341" id="Seg_8036" s="T340">np:S</ta>
            <ta e="T342" id="Seg_8037" s="T341">v:pred</ta>
            <ta e="T343" id="Seg_8038" s="T342">np:S</ta>
            <ta e="T344" id="Seg_8039" s="T343">v:pred</ta>
            <ta e="T347" id="Seg_8040" s="T346">np:S</ta>
            <ta e="T351" id="Seg_8041" s="T350">cop</ta>
            <ta e="T355" id="Seg_8042" s="T354">np:O</ta>
            <ta e="T356" id="Seg_8043" s="T355">0.1.h:S v:pred</ta>
            <ta e="T357" id="Seg_8044" s="T356">pro.h:S</ta>
            <ta e="T359" id="Seg_8045" s="T358">v:pred</ta>
            <ta e="T367" id="Seg_8046" s="T366">0.1.h:S v:pred</ta>
            <ta e="T375" id="Seg_8047" s="T374">0.1.h:S ptcl:pred</ta>
            <ta e="T377" id="Seg_8048" s="T376">0.1.h:S ptcl:pred</ta>
            <ta e="T378" id="Seg_8049" s="T377">np:O</ta>
            <ta e="T380" id="Seg_8050" s="T379">0.1.h:S v:pred</ta>
            <ta e="T381" id="Seg_8051" s="T380">np:O</ta>
            <ta e="T383" id="Seg_8052" s="T382">0.1.h:S v:pred</ta>
            <ta e="T392" id="Seg_8053" s="T391">np.h:S</ta>
            <ta e="T393" id="Seg_8054" s="T392">ptcl:pred</ta>
            <ta e="T395" id="Seg_8055" s="T394">np.h:S</ta>
            <ta e="T396" id="Seg_8056" s="T395">v:pred</ta>
            <ta e="T398" id="Seg_8057" s="T397">v:pred</ta>
            <ta e="T399" id="Seg_8058" s="T398">pro.h:S</ta>
            <ta e="T400" id="Seg_8059" s="T399">np:O</ta>
            <ta e="T401" id="Seg_8060" s="T400">0.3.h:S v:pred</ta>
            <ta e="T404" id="Seg_8061" s="T403">0.3.h:S v:pred</ta>
            <ta e="T410" id="Seg_8062" s="T409">0.3.h:S v:pred</ta>
            <ta e="T413" id="Seg_8063" s="T412">0.1.h:S adj:pred</ta>
            <ta e="T414" id="Seg_8064" s="T413">pro.h:S</ta>
            <ta e="T418" id="Seg_8065" s="T414">s:adv</ta>
            <ta e="T421" id="Seg_8066" s="T418">s:adv</ta>
            <ta e="T423" id="Seg_8067" s="T422">v:pred</ta>
            <ta e="T428" id="Seg_8068" s="T427">adj:pred</ta>
            <ta e="T434" id="Seg_8069" s="T433">adj:pred</ta>
            <ta e="T445" id="Seg_8070" s="T444">np.h:S</ta>
            <ta e="T447" id="Seg_8071" s="T446">v:pred</ta>
            <ta e="T451" id="Seg_8072" s="T450">0.1.h:S adj:pred</ta>
            <ta e="T458" id="Seg_8073" s="T457">pro.h:S</ta>
            <ta e="T462" id="Seg_8074" s="T461">v:pred</ta>
            <ta e="T478" id="Seg_8075" s="T476">s:adv</ta>
            <ta e="T479" id="Seg_8076" s="T478">s:adv</ta>
            <ta e="T482" id="Seg_8077" s="T481">0.3:S v:pred</ta>
            <ta e="T485" id="Seg_8078" s="T484">0.3:S adj:pred</ta>
            <ta e="T486" id="Seg_8079" s="T485">np.h:S</ta>
            <ta e="T489" id="Seg_8080" s="T486">s:rel</ta>
            <ta e="T490" id="Seg_8081" s="T489">pro.h:S</ta>
            <ta e="T491" id="Seg_8082" s="T490">adj:pred</ta>
            <ta e="T492" id="Seg_8083" s="T491">cop</ta>
            <ta e="T495" id="Seg_8084" s="T492">s:rel</ta>
            <ta e="T496" id="Seg_8085" s="T495">pro.h:S</ta>
            <ta e="T497" id="Seg_8086" s="T496">adj:pred</ta>
            <ta e="T498" id="Seg_8087" s="T497">cop</ta>
            <ta e="T505" id="Seg_8088" s="T504">pro.h:S</ta>
            <ta e="T513" id="Seg_8089" s="T512">v:pred</ta>
            <ta e="T517" id="Seg_8090" s="T516">np.h:S</ta>
            <ta e="T519" id="Seg_8091" s="T517">s:adv</ta>
            <ta e="T522" id="Seg_8092" s="T521">v:pred</ta>
            <ta e="T531" id="Seg_8093" s="T530">0.1.h:S v:pred</ta>
            <ta e="T532" id="Seg_8094" s="T531">0.1.h:S v:pred</ta>
            <ta e="T534" id="Seg_8095" s="T533">0.1.h:S v:pred</ta>
            <ta e="T538" id="Seg_8096" s="T537">np.h:S</ta>
            <ta e="T540" id="Seg_8097" s="T538">s:adv</ta>
            <ta e="T544" id="Seg_8098" s="T543">v:pred</ta>
            <ta e="T548" id="Seg_8099" s="T547">0.1.h:S v:pred</ta>
            <ta e="T551" id="Seg_8100" s="T548">s:adv</ta>
            <ta e="T552" id="Seg_8101" s="T551">pro.h:S</ta>
            <ta e="T554" id="Seg_8102" s="T553">ptcl:pred</ta>
            <ta e="T556" id="Seg_8103" s="T555">cop</ta>
            <ta e="T560" id="Seg_8104" s="T559">pro.h:S</ta>
            <ta e="T561" id="Seg_8105" s="T560">np:O</ta>
            <ta e="T564" id="Seg_8106" s="T563">v:pred</ta>
            <ta e="T570" id="Seg_8107" s="T565">s:temp</ta>
            <ta e="T577" id="Seg_8108" s="T576">np:O</ta>
            <ta e="T582" id="Seg_8109" s="T581">0.1.h:S v:pred</ta>
            <ta e="T584" id="Seg_8110" s="T583">pro:S</ta>
            <ta e="T585" id="Seg_8111" s="T584">adj:pred</ta>
            <ta e="T586" id="Seg_8112" s="T585">cop</ta>
            <ta e="T590" id="Seg_8113" s="T589">pro.h:S</ta>
            <ta e="T592" id="Seg_8114" s="T591">v:pred</ta>
            <ta e="T597" id="Seg_8115" s="T596">np.h:S</ta>
            <ta e="T601" id="Seg_8116" s="T600">v:pred</ta>
            <ta e="T605" id="Seg_8117" s="T601">s:adv</ta>
            <ta e="T608" id="Seg_8118" s="T607">pro.h:S</ta>
            <ta e="T611" id="Seg_8119" s="T610">v:pred</ta>
            <ta e="T618" id="Seg_8120" s="T617">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_8121" s="T1">accs-gen</ta>
            <ta e="T7" id="Seg_8122" s="T6">0.accs-sit</ta>
            <ta e="T9" id="Seg_8123" s="T8">giv-active</ta>
            <ta e="T19" id="Seg_8124" s="T18">0.giv-active</ta>
            <ta e="T21" id="Seg_8125" s="T20">giv-inactive</ta>
            <ta e="T26" id="Seg_8126" s="T25">accs-inf</ta>
            <ta e="T37" id="Seg_8127" s="T36">giv-active</ta>
            <ta e="T41" id="Seg_8128" s="T40">new</ta>
            <ta e="T44" id="Seg_8129" s="T43">new</ta>
            <ta e="T46" id="Seg_8130" s="T45">new</ta>
            <ta e="T56" id="Seg_8131" s="T55">giv-inactive</ta>
            <ta e="T59" id="Seg_8132" s="T58">accs-inf</ta>
            <ta e="T65" id="Seg_8133" s="T64">0.giv-active</ta>
            <ta e="T71" id="Seg_8134" s="T70">0.giv-active</ta>
            <ta e="T73" id="Seg_8135" s="T72">0.giv-active</ta>
            <ta e="T76" id="Seg_8136" s="T75">giv-inactive</ta>
            <ta e="T81" id="Seg_8137" s="T80">giv-active-Q</ta>
            <ta e="T84" id="Seg_8138" s="T83">0.quot-sp</ta>
            <ta e="T88" id="Seg_8139" s="T87">accs-gen</ta>
            <ta e="T93" id="Seg_8140" s="T92">accs-gen</ta>
            <ta e="T96" id="Seg_8141" s="T95">giv-active</ta>
            <ta e="T97" id="Seg_8142" s="T96">accs-inf</ta>
            <ta e="T102" id="Seg_8143" s="T101">0.giv-inactive</ta>
            <ta e="T118" id="Seg_8144" s="T117">accs-inf</ta>
            <ta e="T127" id="Seg_8145" s="T126">giv-inactive</ta>
            <ta e="T129" id="Seg_8146" s="T128">new</ta>
            <ta e="T130" id="Seg_8147" s="T129">giv-inactive</ta>
            <ta e="T132" id="Seg_8148" s="T131">giv-active</ta>
            <ta e="T135" id="Seg_8149" s="T134">giv-inactive</ta>
            <ta e="T139" id="Seg_8150" s="T138">accs-inf</ta>
            <ta e="T142" id="Seg_8151" s="T141">new</ta>
            <ta e="T160" id="Seg_8152" s="T159">giv-inactive</ta>
            <ta e="T162" id="Seg_8153" s="T161">giv-active-Q</ta>
            <ta e="T163" id="Seg_8154" s="T162">0.accs-sit-Q</ta>
            <ta e="T164" id="Seg_8155" s="T163">giv-active-Q</ta>
            <ta e="T168" id="Seg_8156" s="T167">0.quot-sp</ta>
            <ta e="T171" id="Seg_8157" s="T170">giv-inactive</ta>
            <ta e="T173" id="Seg_8158" s="T172">accs-inf</ta>
            <ta e="T175" id="Seg_8159" s="T174">giv-active</ta>
            <ta e="T183" id="Seg_8160" s="T182">accs-inf</ta>
            <ta e="T186" id="Seg_8161" s="T185">accs-sit</ta>
            <ta e="T189" id="Seg_8162" s="T188">giv-inactive</ta>
            <ta e="T190" id="Seg_8163" s="T189">0.giv-inactive</ta>
            <ta e="T200" id="Seg_8164" s="T199">giv-active</ta>
            <ta e="T207" id="Seg_8165" s="T206">giv-active</ta>
            <ta e="T210" id="Seg_8166" s="T209">giv-inactive</ta>
            <ta e="T215" id="Seg_8167" s="T214">giv-active</ta>
            <ta e="T216" id="Seg_8168" s="T215">0.giv-active</ta>
            <ta e="T220" id="Seg_8169" s="T219">giv-active</ta>
            <ta e="T222" id="Seg_8170" s="T221">giv-active</ta>
            <ta e="T225" id="Seg_8171" s="T224">giv-inactive</ta>
            <ta e="T226" id="Seg_8172" s="T225">accs-inf</ta>
            <ta e="T227" id="Seg_8173" s="T226">0.giv-active</ta>
            <ta e="T231" id="Seg_8174" s="T230">0.giv-active</ta>
            <ta e="T235" id="Seg_8175" s="T234">giv-active</ta>
            <ta e="T236" id="Seg_8176" s="T235">giv-active</ta>
            <ta e="T245" id="Seg_8177" s="T244">giv-active</ta>
            <ta e="T246" id="Seg_8178" s="T245">0.giv-inactive</ta>
            <ta e="T254" id="Seg_8179" s="T253">new</ta>
            <ta e="T258" id="Seg_8180" s="T257">giv-inactive</ta>
            <ta e="T260" id="Seg_8181" s="T259">giv-inactive</ta>
            <ta e="T261" id="Seg_8182" s="T260">accs-inf</ta>
            <ta e="T263" id="Seg_8183" s="T262">accs-inf</ta>
            <ta e="T268" id="Seg_8184" s="T267">giv-active</ta>
            <ta e="T274" id="Seg_8185" s="T273">giv-active</ta>
            <ta e="T279" id="Seg_8186" s="T278">giv-inactive</ta>
            <ta e="T285" id="Seg_8187" s="T284">giv-active</ta>
            <ta e="T292" id="Seg_8188" s="T291">0.giv-active</ta>
            <ta e="T300" id="Seg_8189" s="T299">accs-gen</ta>
            <ta e="T306" id="Seg_8190" s="T305">giv-inactive</ta>
            <ta e="T311" id="Seg_8191" s="T310">new</ta>
            <ta e="T317" id="Seg_8192" s="T316">new</ta>
            <ta e="T320" id="Seg_8193" s="T319">giv-inactive</ta>
            <ta e="T324" id="Seg_8194" s="T323">accs-inf</ta>
            <ta e="T325" id="Seg_8195" s="T324">accs-inf</ta>
            <ta e="T336" id="Seg_8196" s="T335">0.giv-inactive</ta>
            <ta e="T341" id="Seg_8197" s="T340">giv-inactive</ta>
            <ta e="T343" id="Seg_8198" s="T342">giv-active</ta>
            <ta e="T347" id="Seg_8199" s="T346">accs-inf</ta>
            <ta e="T356" id="Seg_8200" s="T355">0.giv-inactive</ta>
            <ta e="T357" id="Seg_8201" s="T356">giv-active</ta>
            <ta e="T362" id="Seg_8202" s="T361">giv-active</ta>
            <ta e="T365" id="Seg_8203" s="T364">giv-inactive</ta>
            <ta e="T367" id="Seg_8204" s="T366">0.giv-inactive</ta>
            <ta e="T372" id="Seg_8205" s="T370">accs-inf</ta>
            <ta e="T375" id="Seg_8206" s="T374">0.giv-active</ta>
            <ta e="T377" id="Seg_8207" s="T376">0.giv-active</ta>
            <ta e="T380" id="Seg_8208" s="T379">0.giv-active</ta>
            <ta e="T383" id="Seg_8209" s="T382">0.giv-active</ta>
            <ta e="T387" id="Seg_8210" s="T386">giv-inactive</ta>
            <ta e="T392" id="Seg_8211" s="T391">new</ta>
            <ta e="T395" id="Seg_8212" s="T394">giv-active</ta>
            <ta e="T399" id="Seg_8213" s="T398">giv-active</ta>
            <ta e="T400" id="Seg_8214" s="T399">giv-inactive</ta>
            <ta e="T401" id="Seg_8215" s="T400">0.giv-active</ta>
            <ta e="T404" id="Seg_8216" s="T403">0.giv-active</ta>
            <ta e="T410" id="Seg_8217" s="T409">0.giv-active</ta>
            <ta e="T413" id="Seg_8218" s="T412">0.giv-inactive new</ta>
            <ta e="T414" id="Seg_8219" s="T413">giv-active</ta>
            <ta e="T416" id="Seg_8220" s="T415">accs-inf</ta>
            <ta e="T417" id="Seg_8221" s="T416">giv-inactive</ta>
            <ta e="T420" id="Seg_8222" s="T419">accs-inf</ta>
            <ta e="T425" id="Seg_8223" s="T424">giv-active</ta>
            <ta e="T431" id="Seg_8224" s="T430">giv-inactive</ta>
            <ta e="T441" id="Seg_8225" s="T440">accs-aggr</ta>
            <ta e="T445" id="Seg_8226" s="T444">new</ta>
            <ta e="T446" id="Seg_8227" s="T445">accs-gen</ta>
            <ta e="T451" id="Seg_8228" s="T450">0.giv-inactive new</ta>
            <ta e="T458" id="Seg_8229" s="T457">giv-active</ta>
            <ta e="T461" id="Seg_8230" s="T460">giv-inactive</ta>
            <ta e="T463" id="Seg_8231" s="T462">giv-active</ta>
            <ta e="T470" id="Seg_8232" s="T469">giv-active</ta>
            <ta e="T474" id="Seg_8233" s="T473">giv-active</ta>
            <ta e="T482" id="Seg_8234" s="T481">0.giv-active</ta>
            <ta e="T485" id="Seg_8235" s="T484">0.giv-active accs-inf</ta>
            <ta e="T499" id="Seg_8236" s="T498">giv-inactive</ta>
            <ta e="T505" id="Seg_8237" s="T504">giv-active</ta>
            <ta e="T509" id="Seg_8238" s="T508">accs-inf</ta>
            <ta e="T517" id="Seg_8239" s="T516">accs-inf</ta>
            <ta e="T520" id="Seg_8240" s="T519">giv-active</ta>
            <ta e="T528" id="Seg_8241" s="T527">new</ta>
            <ta e="T531" id="Seg_8242" s="T530">0.giv-active-Q</ta>
            <ta e="T532" id="Seg_8243" s="T531">0.giv-active 0.quot-sp</ta>
            <ta e="T533" id="Seg_8244" s="T532">giv-inactive</ta>
            <ta e="T534" id="Seg_8245" s="T533">0.giv-active</ta>
            <ta e="T538" id="Seg_8246" s="T537">giv-inactive</ta>
            <ta e="T548" id="Seg_8247" s="T547">0.giv-inactive</ta>
            <ta e="T551" id="Seg_8248" s="T550">0.giv-active</ta>
            <ta e="T552" id="Seg_8249" s="T551">giv-active</ta>
            <ta e="T560" id="Seg_8250" s="T559">giv-active</ta>
            <ta e="T561" id="Seg_8251" s="T560">accs-gen</ta>
            <ta e="T568" id="Seg_8252" s="T567">giv-active</ta>
            <ta e="T569" id="Seg_8253" s="T568">giv-inactive</ta>
            <ta e="T574" id="Seg_8254" s="T573">new</ta>
            <ta e="T577" id="Seg_8255" s="T576">accs-inf</ta>
            <ta e="T582" id="Seg_8256" s="T581">0.accs-aggr</ta>
            <ta e="T583" id="Seg_8257" s="T582">giv-active</ta>
            <ta e="T597" id="Seg_8258" s="T596">giv-inactive</ta>
            <ta e="T600" id="Seg_8259" s="T599">giv-inactive</ta>
            <ta e="T604" id="Seg_8260" s="T603">giv-active</ta>
            <ta e="T606" id="Seg_8261" s="T605">giv-inactive</ta>
            <ta e="T608" id="Seg_8262" s="T607">giv-inactive</ta>
            <ta e="T618" id="Seg_8263" s="T617">0.giv-active</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T3" id="Seg_8264" s="T0">top.int.concr</ta>
            <ta e="T8" id="Seg_8265" s="T7">top.int.concr</ta>
            <ta e="T19" id="Seg_8266" s="T18">0.top.int.concr</ta>
            <ta e="T22" id="Seg_8267" s="T21">0.top.int.abstr</ta>
            <ta e="T27" id="Seg_8268" s="T25">top.int.concr</ta>
            <ta e="T39" id="Seg_8269" s="T36">top.int.concr</ta>
            <ta e="T54" id="Seg_8270" s="T53">top.int.concr</ta>
            <ta e="T65" id="Seg_8271" s="T64">0.top.int.concr</ta>
            <ta e="T68" id="Seg_8272" s="T65">top.int.concr</ta>
            <ta e="T76" id="Seg_8273" s="T75">top.int.concr</ta>
            <ta e="T81" id="Seg_8274" s="T78">top.int.concr</ta>
            <ta e="T86" id="Seg_8275" s="T84">top.int.concr</ta>
            <ta e="T118" id="Seg_8276" s="T111">top.int.concr</ta>
            <ta e="T129" id="Seg_8277" s="T126">top.int.concr</ta>
            <ta e="T132" id="Seg_8278" s="T129">top.int.concr</ta>
            <ta e="T163" id="Seg_8279" s="T161">top.int.concr</ta>
            <ta e="T183" id="Seg_8280" s="T182">top.int.concr</ta>
            <ta e="T187" id="Seg_8281" s="T186">0.top.int.concr</ta>
            <ta e="T190" id="Seg_8282" s="T189">0.top.int.concr</ta>
            <ta e="T206" id="Seg_8283" s="T205">top.int.concr</ta>
            <ta e="T227" id="Seg_8284" s="T226">0.top.int.concr</ta>
            <ta e="T231" id="Seg_8285" s="T230">0.top.int.concr</ta>
            <ta e="T236" id="Seg_8286" s="T234">top.int.concr</ta>
            <ta e="T246" id="Seg_8287" s="T245">0.top.int.concr</ta>
            <ta e="T249" id="Seg_8288" s="T246">top.int.concr</ta>
            <ta e="T263" id="Seg_8289" s="T256">top.int.concr</ta>
            <ta e="T268" id="Seg_8290" s="T267">top.int.concr</ta>
            <ta e="T274" id="Seg_8291" s="T272">top.int.concr</ta>
            <ta e="T296" id="Seg_8292" s="T293">top.int.concr</ta>
            <ta e="T303" id="Seg_8293" s="T302">top.int.concr</ta>
            <ta e="T313" id="Seg_8294" s="T312">0.top.int.abstr</ta>
            <ta e="T319" id="Seg_8295" s="T318">0.top.int.abstr</ta>
            <ta e="T320" id="Seg_8296" s="T319">top.int.concr</ta>
            <ta e="T330" id="Seg_8297" s="T327">top.int.concr</ta>
            <ta e="T338" id="Seg_8298" s="T336">top.int.concr</ta>
            <ta e="T340" id="Seg_8299" s="T339">top.int.concr</ta>
            <ta e="T343" id="Seg_8300" s="T342">top.int.concr</ta>
            <ta e="T345" id="Seg_8301" s="T344">top.int.concr</ta>
            <ta e="T356" id="Seg_8302" s="T355">0.top.int.concr</ta>
            <ta e="T357" id="Seg_8303" s="T356">top.int.concr</ta>
            <ta e="T367" id="Seg_8304" s="T366">0.top.int.concr</ta>
            <ta e="T375" id="Seg_8305" s="T374">0.top.int.concr</ta>
            <ta e="T377" id="Seg_8306" s="T376">0.top.int.concr</ta>
            <ta e="T380" id="Seg_8307" s="T379">0.top.int.concr</ta>
            <ta e="T383" id="Seg_8308" s="T382">0.top.int.concr</ta>
            <ta e="T393" id="Seg_8309" s="T392">0.top.int.abstr</ta>
            <ta e="T395" id="Seg_8310" s="T393">top.int.concr</ta>
            <ta e="T399" id="Seg_8311" s="T398">top.int.concr</ta>
            <ta e="T401" id="Seg_8312" s="T400">0.top.int.concr</ta>
            <ta e="T404" id="Seg_8313" s="T403">0.top.int.concr</ta>
            <ta e="T410" id="Seg_8314" s="T409">0.top.int.concr</ta>
            <ta e="T413" id="Seg_8315" s="T412">0.top.int.concr</ta>
            <ta e="T414" id="Seg_8316" s="T413">top.int.concr</ta>
            <ta e="T445" id="Seg_8317" s="T442">top.int.concr</ta>
            <ta e="T451" id="Seg_8318" s="T450">0.top.int.concr</ta>
            <ta e="T458" id="Seg_8319" s="T457">top.int.concr</ta>
            <ta e="T482" id="Seg_8320" s="T481">0.top.int.concr</ta>
            <ta e="T485" id="Seg_8321" s="T484">0.top.int.concr</ta>
            <ta e="T489" id="Seg_8322" s="T485">top.int.concr</ta>
            <ta e="T495" id="Seg_8323" s="T492">top.int.concr.contr</ta>
            <ta e="T500" id="Seg_8324" s="T498">top.int.concr</ta>
            <ta e="T505" id="Seg_8325" s="T504">top.int.concr</ta>
            <ta e="T515" id="Seg_8326" s="T514">top.int.concr</ta>
            <ta e="T523" id="Seg_8327" s="T522">top.int.concr</ta>
            <ta e="T536" id="Seg_8328" s="T535">top.int.concr</ta>
            <ta e="T548" id="Seg_8329" s="T547">0.top.int.concr</ta>
            <ta e="T552" id="Seg_8330" s="T551">top.int.concr</ta>
            <ta e="T560" id="Seg_8331" s="T559">top.int.concr</ta>
            <ta e="T570" id="Seg_8332" s="T565">top.int.concr</ta>
            <ta e="T577" id="Seg_8333" s="T574">top.int.concr</ta>
            <ta e="T584" id="Seg_8334" s="T583">top.int.concr</ta>
            <ta e="T587" id="Seg_8335" s="T586">top.int.concr</ta>
            <ta e="T597" id="Seg_8336" s="T595">top.int.concr</ta>
            <ta e="T608" id="Seg_8337" s="T607">top.int.concr</ta>
            <ta e="T618" id="Seg_8338" s="T617">0.top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T7" id="Seg_8339" s="T6">foc.int</ta>
            <ta e="T12" id="Seg_8340" s="T10">foc.nar</ta>
            <ta e="T17" id="Seg_8341" s="T13">foc.nar</ta>
            <ta e="T22" id="Seg_8342" s="T19">foc.wid</ta>
            <ta e="T36" id="Seg_8343" s="T31">foc.wid</ta>
            <ta e="T50" id="Seg_8344" s="T47">foc.nar</ta>
            <ta e="T60" id="Seg_8345" s="T59">foc.nar</ta>
            <ta e="T64" id="Seg_8346" s="T63">foc.nar</ta>
            <ta e="T71" id="Seg_8347" s="T69">foc.int</ta>
            <ta e="T73" id="Seg_8348" s="T71">foc.int</ta>
            <ta e="T78" id="Seg_8349" s="T77">foc.ver</ta>
            <ta e="T83" id="Seg_8350" s="T82">foc.int</ta>
            <ta e="T95" id="Seg_8351" s="T87">foc.wid</ta>
            <ta e="T123" id="Seg_8352" s="T119">foc.int</ta>
            <ta e="T139" id="Seg_8353" s="T134">foc.int</ta>
            <ta e="T161" id="Seg_8354" s="T159">foc.wid</ta>
            <ta e="T166" id="Seg_8355" s="T164">foc.nar</ta>
            <ta e="T184" id="Seg_8356" s="T183">foc.int</ta>
            <ta e="T186" id="Seg_8357" s="T184">foc.nar</ta>
            <ta e="T192" id="Seg_8358" s="T190">foc.nar</ta>
            <ta e="T194" id="Seg_8359" s="T192">foc.nar</ta>
            <ta e="T209" id="Seg_8360" s="T206">foc.int</ta>
            <ta e="T227" id="Seg_8361" s="T223">foc.int</ta>
            <ta e="T231" id="Seg_8362" s="T227">foc.int</ta>
            <ta e="T237" id="Seg_8363" s="T236">foc.nar</ta>
            <ta e="T243" id="Seg_8364" s="T239">foc.nar</ta>
            <ta e="T256" id="Seg_8365" s="T249">foc.wid</ta>
            <ta e="T267" id="Seg_8366" s="T264">foc.int</ta>
            <ta e="T270" id="Seg_8367" s="T269">foc.nar</ta>
            <ta e="T289" id="Seg_8368" s="T287">foc.nar</ta>
            <ta e="T292" id="Seg_8369" s="T289">foc.nar</ta>
            <ta e="T301" id="Seg_8370" s="T297">foc.wid</ta>
            <ta e="T305" id="Seg_8371" s="T303">foc.nar</ta>
            <ta e="T313" id="Seg_8372" s="T307">foc.wid</ta>
            <ta e="T319" id="Seg_8373" s="T313">foc.wid</ta>
            <ta e="T327" id="Seg_8374" s="T320">foc.int</ta>
            <ta e="T334" id="Seg_8375" s="T332">foc.nar</ta>
            <ta e="T339" id="Seg_8376" s="T338">foc.nar</ta>
            <ta e="T342" id="Seg_8377" s="T340">foc.wid</ta>
            <ta e="T344" id="Seg_8378" s="T343">foc.int</ta>
            <ta e="T351" id="Seg_8379" s="T345">foc.wid</ta>
            <ta e="T355" id="Seg_8380" s="T352">foc.nar</ta>
            <ta e="T359" id="Seg_8381" s="T357">foc.nar</ta>
            <ta e="T367" id="Seg_8382" s="T363">foc.int</ta>
            <ta e="T375" id="Seg_8383" s="T372">foc.int</ta>
            <ta e="T377" id="Seg_8384" s="T375">foc.int</ta>
            <ta e="T380" id="Seg_8385" s="T377">foc.int</ta>
            <ta e="T383" id="Seg_8386" s="T380">foc.int</ta>
            <ta e="T393" id="Seg_8387" s="T383">foc.wid</ta>
            <ta e="T396" id="Seg_8388" s="T395">foc.int</ta>
            <ta e="T398" id="Seg_8389" s="T397">foc.nar</ta>
            <ta e="T401" id="Seg_8390" s="T399">foc.int</ta>
            <ta e="T404" id="Seg_8391" s="T402">foc.int</ta>
            <ta e="T410" id="Seg_8392" s="T408">foc.nar</ta>
            <ta e="T413" id="Seg_8393" s="T412">foc.nar</ta>
            <ta e="T424" id="Seg_8394" s="T421">foc.int</ta>
            <ta e="T446" id="Seg_8395" s="T445">foc.nar</ta>
            <ta e="T451" id="Seg_8396" s="T448">foc.nar</ta>
            <ta e="T461" id="Seg_8397" s="T460">foc.nar</ta>
            <ta e="T482" id="Seg_8398" s="T476">foc.int</ta>
            <ta e="T485" id="Seg_8399" s="T484">foc.nar</ta>
            <ta e="T491" id="Seg_8400" s="T490">foc.nar</ta>
            <ta e="T497" id="Seg_8401" s="T496">foc.contr</ta>
            <ta e="T501" id="Seg_8402" s="T500">foc.nar</ta>
            <ta e="T513" id="Seg_8403" s="T507">foc.int</ta>
            <ta e="T521" id="Seg_8404" s="T519">foc.nar</ta>
            <ta e="T535" id="Seg_8405" s="T524">foc.int</ta>
            <ta e="T541" id="Seg_8406" s="T540">foc.nar</ta>
            <ta e="T548" id="Seg_8407" s="T545">foc.int</ta>
            <ta e="T556" id="Seg_8408" s="T552">foc.int</ta>
            <ta e="T562" id="Seg_8409" s="T560">foc.nar</ta>
            <ta e="T574" id="Seg_8410" s="T571">foc.nar</ta>
            <ta e="T583" id="Seg_8411" s="T582">foc.nar</ta>
            <ta e="T585" id="Seg_8412" s="T584">foc.nar</ta>
            <ta e="T595" id="Seg_8413" s="T592">foc.nar</ta>
            <ta e="T601" id="Seg_8414" s="T597">foc.int</ta>
            <ta e="T606" id="Seg_8415" s="T605">foc.nar</ta>
            <ta e="T610" id="Seg_8416" s="T609">foc.nar</ta>
            <ta e="T618" id="Seg_8417" s="T612">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_8418" s="T0">RUS:cult</ta>
            <ta e="T2" id="Seg_8419" s="T1">RUS:cult</ta>
            <ta e="T5" id="Seg_8420" s="T4">RUS:mod</ta>
            <ta e="T16" id="Seg_8421" s="T15">RUS:cult</ta>
            <ta e="T20" id="Seg_8422" s="T19">RUS:cult</ta>
            <ta e="T21" id="Seg_8423" s="T20">RUS:cult</ta>
            <ta e="T25" id="Seg_8424" s="T23">RUS:gram</ta>
            <ta e="T29" id="Seg_8425" s="T27">RUS:gram</ta>
            <ta e="T32" id="Seg_8426" s="T31">RUS:cult</ta>
            <ta e="T34" id="Seg_8427" s="T33">RUS:cult</ta>
            <ta e="T45" id="Seg_8428" s="T44">RUS:cult</ta>
            <ta e="T70" id="Seg_8429" s="T69">RUS:cult</ta>
            <ta e="T74" id="Seg_8430" s="T73">RUS:disc</ta>
            <ta e="T80" id="Seg_8431" s="T79">RUS:cult</ta>
            <ta e="T88" id="Seg_8432" s="T87">RUS:cult</ta>
            <ta e="T91" id="Seg_8433" s="T90">RUS:cult</ta>
            <ta e="T92" id="Seg_8434" s="T91">RUS:cult</ta>
            <ta e="T96" id="Seg_8435" s="T95">RUS:cult</ta>
            <ta e="T97" id="Seg_8436" s="T96">RUS:cult</ta>
            <ta e="T121" id="Seg_8437" s="T120">RUS:gram</ta>
            <ta e="T123" id="Seg_8438" s="T122">RUS:core</ta>
            <ta e="T130" id="Seg_8439" s="T129">RUS:cult</ta>
            <ta e="T135" id="Seg_8440" s="T134">RUS:cult</ta>
            <ta e="T142" id="Seg_8441" s="T141">RUS:cult</ta>
            <ta e="T145" id="Seg_8442" s="T144">RUS:cult</ta>
            <ta e="T146" id="Seg_8443" s="T145">RUS:cult</ta>
            <ta e="T160" id="Seg_8444" s="T159">RUS:cult</ta>
            <ta e="T162" id="Seg_8445" s="T161">RUS:cult</ta>
            <ta e="T170" id="Seg_8446" s="T169">RUS:cult</ta>
            <ta e="T171" id="Seg_8447" s="T170">RUS:cult</ta>
            <ta e="T183" id="Seg_8448" s="T182">RUS:cult</ta>
            <ta e="T189" id="Seg_8449" s="T188">RUS:cult</ta>
            <ta e="T200" id="Seg_8450" s="T199">RUS:cult</ta>
            <ta e="T202" id="Seg_8451" s="T201">RUS:core</ta>
            <ta e="T203" id="Seg_8452" s="T202">RUS:cult</ta>
            <ta e="T207" id="Seg_8453" s="T206">RUS:cult</ta>
            <ta e="T215" id="Seg_8454" s="T214">RUS:cult</ta>
            <ta e="T218" id="Seg_8455" s="T217">RUS:cult</ta>
            <ta e="T221" id="Seg_8456" s="T220">RUS:cult</ta>
            <ta e="T223" id="Seg_8457" s="T222">RUS:cult</ta>
            <ta e="T225" id="Seg_8458" s="T224">RUS:cult</ta>
            <ta e="T235" id="Seg_8459" s="T234">RUS:cult</ta>
            <ta e="T245" id="Seg_8460" s="T244">RUS:cult</ta>
            <ta e="T258" id="Seg_8461" s="T257">RUS:cult</ta>
            <ta e="T260" id="Seg_8462" s="T259">RUS:cult</ta>
            <ta e="T265" id="Seg_8463" s="T264">RUS:cult</ta>
            <ta e="T266" id="Seg_8464" s="T265">RUS:gram</ta>
            <ta e="T274" id="Seg_8465" s="T273">RUS:cult</ta>
            <ta e="T278" id="Seg_8466" s="T277">RUS:cult</ta>
            <ta e="T279" id="Seg_8467" s="T278">RUS:cult</ta>
            <ta e="T285" id="Seg_8468" s="T284">RUS:cult</ta>
            <ta e="T291" id="Seg_8469" s="T290">RUS:gram</ta>
            <ta e="T299" id="Seg_8470" s="T298">RUS:cult</ta>
            <ta e="T310" id="Seg_8471" s="T309">RUS:gram</ta>
            <ta e="T316" id="Seg_8472" s="T315">RUS:gram</ta>
            <ta e="T317" id="Seg_8473" s="T316">RUS:cult</ta>
            <ta e="T323" id="Seg_8474" s="T322">RUS:cult</ta>
            <ta e="T325" id="Seg_8475" s="T324">RUS:cult</ta>
            <ta e="T347" id="Seg_8476" s="T346">RUS:cult</ta>
            <ta e="T349" id="Seg_8477" s="T348">RUS:cult</ta>
            <ta e="T379" id="Seg_8478" s="T378">RUS:gram</ta>
            <ta e="T382" id="Seg_8479" s="T381">RUS:gram</ta>
            <ta e="T392" id="Seg_8480" s="T391">RUS:cult</ta>
            <ta e="T397" id="Seg_8481" s="T396">RUS:gram</ta>
            <ta e="T400" id="Seg_8482" s="T399">RUS:cult</ta>
            <ta e="T403" id="Seg_8483" s="T402">RUS:cult</ta>
            <ta e="T409" id="Seg_8484" s="T408">RUS:cult</ta>
            <ta e="T417" id="Seg_8485" s="T416">RUS:cult</ta>
            <ta e="T420" id="Seg_8486" s="T419">RUS:cult</ta>
            <ta e="T422" id="Seg_8487" s="T421">RUS:cult</ta>
            <ta e="T432" id="Seg_8488" s="T431">RUS:gram</ta>
            <ta e="T446" id="Seg_8489" s="T445">RUS:cult</ta>
            <ta e="T453" id="Seg_8490" s="T452">RUS:cult</ta>
            <ta e="T454" id="Seg_8491" s="T453">RUS:cult</ta>
            <ta e="T455" id="Seg_8492" s="T454">RUS:cult</ta>
            <ta e="T461" id="Seg_8493" s="T460">RUS:cult</ta>
            <ta e="T463" id="Seg_8494" s="T462">RUS:cult</ta>
            <ta e="T468" id="Seg_8495" s="T467">RUS:gram</ta>
            <ta e="T470" id="Seg_8496" s="T469">RUS:cult</ta>
            <ta e="T474" id="Seg_8497" s="T473">RUS:cult</ta>
            <ta e="T481" id="Seg_8498" s="T480">RUS:gram</ta>
            <ta e="T484" id="Seg_8499" s="T483">RUS:gram</ta>
            <ta e="T485" id="Seg_8500" s="T484">RUS:cult</ta>
            <ta e="T497" id="Seg_8501" s="T496">RUS:core</ta>
            <ta e="T509" id="Seg_8502" s="T508">RUS:cult</ta>
            <ta e="T511" id="Seg_8503" s="T510">RUS:cult</ta>
            <ta e="T521" id="Seg_8504" s="T520">RUS:mod</ta>
            <ta e="T533" id="Seg_8505" s="T532">RUS:cult</ta>
            <ta e="T559" id="Seg_8506" s="T558">RUS:mod</ta>
            <ta e="T569" id="Seg_8507" s="T568">RUS:cult</ta>
            <ta e="T574" id="Seg_8508" s="T573">RUS:cult</ta>
            <ta e="T583" id="Seg_8509" s="T582">RUS:cult</ta>
            <ta e="T596" id="Seg_8510" s="T595">RUS:cult</ta>
            <ta e="T597" id="Seg_8511" s="T596">RUS:cult</ta>
            <ta e="T603" id="Seg_8512" s="T602">RUS:cult</ta>
            <ta e="T604" id="Seg_8513" s="T603">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T1" id="Seg_8514" s="T0">Csub fortition Vsub medCdel</ta>
            <ta e="T2" id="Seg_8515" s="T1">fortition medVins Vsub finCdel</ta>
            <ta e="T5" id="Seg_8516" s="T4">Vsub Csub Vsub</ta>
            <ta e="T16" id="Seg_8517" s="T15">Vsub medCdel</ta>
            <ta e="T20" id="Seg_8518" s="T19">Csub fortition Vsub medCdel</ta>
            <ta e="T21" id="Seg_8519" s="T20">fortition medVins Vsub finCdel</ta>
            <ta e="T32" id="Seg_8520" s="T31">Vsub Vsub</ta>
            <ta e="T34" id="Seg_8521" s="T33">Vsub Vsub</ta>
            <ta e="T45" id="Seg_8522" s="T44">Vsub</ta>
            <ta e="T70" id="Seg_8523" s="T69">Vsub Vsub</ta>
            <ta e="T80" id="Seg_8524" s="T79">lenition</ta>
            <ta e="T88" id="Seg_8525" s="T87">inVins Csub Vsub</ta>
            <ta e="T130" id="Seg_8526" s="T129">Vsub</ta>
            <ta e="T135" id="Seg_8527" s="T134">inVins Csub Vsub</ta>
            <ta e="T142" id="Seg_8528" s="T141">Csub fortition</ta>
            <ta e="T160" id="Seg_8529" s="T159">Vsub fortition</ta>
            <ta e="T162" id="Seg_8530" s="T161">Vsub fortition</ta>
            <ta e="T170" id="Seg_8531" s="T169">lenition</ta>
            <ta e="T183" id="Seg_8532" s="T182">Csub Vsub Vsub</ta>
            <ta e="T189" id="Seg_8533" s="T188">Vsub fortition</ta>
            <ta e="T200" id="Seg_8534" s="T199">Vsub fortition</ta>
            <ta e="T207" id="Seg_8535" s="T206">Vsub fortition Vsub</ta>
            <ta e="T215" id="Seg_8536" s="T214">Vsub fortition</ta>
            <ta e="T218" id="Seg_8537" s="T217">Vsub medCdel</ta>
            <ta e="T221" id="Seg_8538" s="T220">Vsub medCdel</ta>
            <ta e="T223" id="Seg_8539" s="T222">Vsub</ta>
            <ta e="T225" id="Seg_8540" s="T224">Vsub fortition Vsub finCdel</ta>
            <ta e="T235" id="Seg_8541" s="T234">Vsub fortition</ta>
            <ta e="T245" id="Seg_8542" s="T244">Vsub fortition Vsub</ta>
            <ta e="T258" id="Seg_8543" s="T257">Vsub</ta>
            <ta e="T260" id="Seg_8544" s="T259">lenition</ta>
            <ta e="T265" id="Seg_8545" s="T264">Csub Vsub</ta>
            <ta e="T274" id="Seg_8546" s="T273">Csub Vsub</ta>
            <ta e="T278" id="Seg_8547" s="T277">medCdel</ta>
            <ta e="T279" id="Seg_8548" s="T278">fortition medVins Vsub finCdel</ta>
            <ta e="T285" id="Seg_8549" s="T284">Csub Vsub</ta>
            <ta e="T323" id="Seg_8550" s="T322">Vsub fortition</ta>
            <ta e="T347" id="Seg_8551" s="T346">Vsub fortition Vsub fortition</ta>
            <ta e="T349" id="Seg_8552" s="T348">Csub Vsub Csub lenition</ta>
            <ta e="T392" id="Seg_8553" s="T391">fortition Csub Vsub</ta>
            <ta e="T400" id="Seg_8554" s="T399">inVins Csub Vsub</ta>
            <ta e="T403" id="Seg_8555" s="T402">Vsub</ta>
            <ta e="T409" id="Seg_8556" s="T408">Vsub</ta>
            <ta e="T417" id="Seg_8557" s="T416">inVins Csub Vsub</ta>
            <ta e="T420" id="Seg_8558" s="T419">fortition Csub</ta>
            <ta e="T422" id="Seg_8559" s="T421">Vsub Vsub</ta>
            <ta e="T446" id="Seg_8560" s="T445">medCdel</ta>
            <ta e="T453" id="Seg_8561" s="T452">Vsub fortition</ta>
            <ta e="T455" id="Seg_8562" s="T454">Vsub fortition</ta>
            <ta e="T461" id="Seg_8563" s="T460">Vsub fortition Vsub fortition</ta>
            <ta e="T463" id="Seg_8564" s="T462">Vsub fortition Vsub fortition</ta>
            <ta e="T470" id="Seg_8565" s="T469">Vsub fortition Vsub fortition</ta>
            <ta e="T474" id="Seg_8566" s="T473">Vsub fortition Vsub fortition</ta>
            <ta e="T485" id="Seg_8567" s="T484">Csub medVins medVdel</ta>
            <ta e="T497" id="Seg_8568" s="T496">Csub</ta>
            <ta e="T511" id="Seg_8569" s="T510">medCdel</ta>
            <ta e="T596" id="Seg_8570" s="T595">medCdel</ta>
            <ta e="T597" id="Seg_8571" s="T596">fortition medVins Vsub finCdel</ta>
            <ta e="T603" id="Seg_8572" s="T602">Csub fortition Vsub medCdel</ta>
            <ta e="T604" id="Seg_8573" s="T603">fortition medVins Vsub finCdel</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T1" id="Seg_8574" s="T0">indir:bare</ta>
            <ta e="T2" id="Seg_8575" s="T1">dir:bare</ta>
            <ta e="T5" id="Seg_8576" s="T4">dir:bare</ta>
            <ta e="T16" id="Seg_8577" s="T15">dir:infl</ta>
            <ta e="T20" id="Seg_8578" s="T19">indir:bare</ta>
            <ta e="T21" id="Seg_8579" s="T20">dir:bare</ta>
            <ta e="T32" id="Seg_8580" s="T31">indir:infl</ta>
            <ta e="T34" id="Seg_8581" s="T33">indir:infl</ta>
            <ta e="T45" id="Seg_8582" s="T44">dir:bare</ta>
            <ta e="T70" id="Seg_8583" s="T69">indir:infl</ta>
            <ta e="T74" id="Seg_8584" s="T73">dir:bare</ta>
            <ta e="T80" id="Seg_8585" s="T79">indir:bare</ta>
            <ta e="T88" id="Seg_8586" s="T87">dir:infl</ta>
            <ta e="T91" id="Seg_8587" s="T90">dir:bare</ta>
            <ta e="T92" id="Seg_8588" s="T91">dir:bare</ta>
            <ta e="T96" id="Seg_8589" s="T95">dir:bare</ta>
            <ta e="T97" id="Seg_8590" s="T96">dir:infl</ta>
            <ta e="T121" id="Seg_8591" s="T120">dir:bare</ta>
            <ta e="T123" id="Seg_8592" s="T122">indir:infl</ta>
            <ta e="T130" id="Seg_8593" s="T129">dir:infl</ta>
            <ta e="T135" id="Seg_8594" s="T134">dir:infl</ta>
            <ta e="T142" id="Seg_8595" s="T141">dir:bare</ta>
            <ta e="T145" id="Seg_8596" s="T144">dir:bare</ta>
            <ta e="T146" id="Seg_8597" s="T145">dir:infl</ta>
            <ta e="T160" id="Seg_8598" s="T159">dir:bare</ta>
            <ta e="T162" id="Seg_8599" s="T161">dir:bare</ta>
            <ta e="T170" id="Seg_8600" s="T169">indir:bare</ta>
            <ta e="T171" id="Seg_8601" s="T170">dir:infl</ta>
            <ta e="T183" id="Seg_8602" s="T182">dir:bare</ta>
            <ta e="T189" id="Seg_8603" s="T188">dir:bare</ta>
            <ta e="T200" id="Seg_8604" s="T199">dir:bare</ta>
            <ta e="T202" id="Seg_8605" s="T201">indir:infl</ta>
            <ta e="T203" id="Seg_8606" s="T202">indir:infl</ta>
            <ta e="T207" id="Seg_8607" s="T206">dir:infl</ta>
            <ta e="T215" id="Seg_8608" s="T214">dir:infl</ta>
            <ta e="T218" id="Seg_8609" s="T217">dir:infl</ta>
            <ta e="T221" id="Seg_8610" s="T220">dir:infl</ta>
            <ta e="T223" id="Seg_8611" s="T222">dir:infl</ta>
            <ta e="T225" id="Seg_8612" s="T224">dir:bare</ta>
            <ta e="T235" id="Seg_8613" s="T234">dir:bare</ta>
            <ta e="T245" id="Seg_8614" s="T244">dir:bare</ta>
            <ta e="T258" id="Seg_8615" s="T257">dir:infl</ta>
            <ta e="T260" id="Seg_8616" s="T259">indir:infl</ta>
            <ta e="T265" id="Seg_8617" s="T264">dir:infl</ta>
            <ta e="T266" id="Seg_8618" s="T265">dir:bare</ta>
            <ta e="T274" id="Seg_8619" s="T273">dir:infl</ta>
            <ta e="T278" id="Seg_8620" s="T277">indir:bare</ta>
            <ta e="T279" id="Seg_8621" s="T278">dir:bare</ta>
            <ta e="T285" id="Seg_8622" s="T284">dir:infl</ta>
            <ta e="T291" id="Seg_8623" s="T290">dir:bare</ta>
            <ta e="T299" id="Seg_8624" s="T298">dir:bare</ta>
            <ta e="T310" id="Seg_8625" s="T309">dir:bare</ta>
            <ta e="T316" id="Seg_8626" s="T315">dir:bare</ta>
            <ta e="T317" id="Seg_8627" s="T316">dir:bare</ta>
            <ta e="T323" id="Seg_8628" s="T322">dir:bare</ta>
            <ta e="T325" id="Seg_8629" s="T324">dir:infl</ta>
            <ta e="T347" id="Seg_8630" s="T346">dir:infl</ta>
            <ta e="T349" id="Seg_8631" s="T348">dir:bare</ta>
            <ta e="T379" id="Seg_8632" s="T378">dir:bare</ta>
            <ta e="T382" id="Seg_8633" s="T381">dir:bare</ta>
            <ta e="T392" id="Seg_8634" s="T391">dir:bare</ta>
            <ta e="T397" id="Seg_8635" s="T396">dir:bare</ta>
            <ta e="T400" id="Seg_8636" s="T399">dir:infl</ta>
            <ta e="T403" id="Seg_8637" s="T402">dir:infl</ta>
            <ta e="T409" id="Seg_8638" s="T408">dir:infl</ta>
            <ta e="T417" id="Seg_8639" s="T416">dir:infl</ta>
            <ta e="T420" id="Seg_8640" s="T419">indir:bare</ta>
            <ta e="T422" id="Seg_8641" s="T421">dir:infl</ta>
            <ta e="T432" id="Seg_8642" s="T431">dir:bare</ta>
            <ta e="T446" id="Seg_8643" s="T445">dir:infl</ta>
            <ta e="T453" id="Seg_8644" s="T452">dir:bare</ta>
            <ta e="T454" id="Seg_8645" s="T453">dir:bare</ta>
            <ta e="T455" id="Seg_8646" s="T454">dir:bare</ta>
            <ta e="T461" id="Seg_8647" s="T460">dir:infl</ta>
            <ta e="T463" id="Seg_8648" s="T462">dir:bare</ta>
            <ta e="T468" id="Seg_8649" s="T467">dir:bare</ta>
            <ta e="T470" id="Seg_8650" s="T469">dir:bare</ta>
            <ta e="T474" id="Seg_8651" s="T473">dir:bare</ta>
            <ta e="T481" id="Seg_8652" s="T480">dir:bare</ta>
            <ta e="T484" id="Seg_8653" s="T483">dir:bare</ta>
            <ta e="T485" id="Seg_8654" s="T484">dir:infl</ta>
            <ta e="T497" id="Seg_8655" s="T496">dir:bare</ta>
            <ta e="T509" id="Seg_8656" s="T508">dir:infl</ta>
            <ta e="T511" id="Seg_8657" s="T510">indir:infl</ta>
            <ta e="T521" id="Seg_8658" s="T520">dir:bare</ta>
            <ta e="T533" id="Seg_8659" s="T532">dir:infl</ta>
            <ta e="T559" id="Seg_8660" s="T558">dir:bare</ta>
            <ta e="T569" id="Seg_8661" s="T568">dir:bare</ta>
            <ta e="T574" id="Seg_8662" s="T573">dir:infl</ta>
            <ta e="T583" id="Seg_8663" s="T582">dir:infl</ta>
            <ta e="T596" id="Seg_8664" s="T595">indir:bare</ta>
            <ta e="T597" id="Seg_8665" s="T596">dir:bare</ta>
            <ta e="T603" id="Seg_8666" s="T602">indir:bare</ta>
            <ta e="T604" id="Seg_8667" s="T603">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T196" id="Seg_8668" s="T194">RUS:int.ins</ta>
            <ta e="T361" id="Seg_8669" s="T359">RUS:int.ins</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_8670" s="T0">How the Soviet era began, I hardly remember.</ta>
            <ta e="T13" id="Seg_8671" s="T7">At that time I was a child at all.</ta>
            <ta e="T19" id="Seg_8672" s="T13">I was a child which does not work under any power.</ta>
            <ta e="T36" id="Seg_8673" s="T19">It was said that the Soviet power was coming, and a knowing man notices how the times are changing.</ta>
            <ta e="T53" id="Seg_8674" s="T36">When the power was changing, the people, which were earlier chased by the rich people, by the kulaks, were called now short, narrow, poor [people].</ta>
            <ta e="T65" id="Seg_8675" s="T53">At that time I was nevertheless a woodcutter at rich people, I was a reindeer herder.</ta>
            <ta e="T73" id="Seg_8676" s="T65">And then later I began to understand, to perceive.</ta>
            <ta e="T78" id="Seg_8677" s="T73">Of course, the power is changing indeed.</ta>
            <ta e="T84" id="Seg_8678" s="T78">"The Czarist power is over", they said.</ta>
            <ta e="T95" id="Seg_8679" s="T84">Then later, comrade grandfather Lenin said that one should learn at school.</ta>
            <ta e="T97" id="Seg_8680" s="T95">The law of Lenin.</ta>
            <ta e="T103" id="Seg_8681" s="T97">Well, changing and changing, it finally changed.</ta>
            <ta e="T111" id="Seg_8682" s="T103">All people, poor, penniless children and the whole people.</ta>
            <ta e="T123" id="Seg_8683" s="T111">The children of all kind of people, of poor people, all began to learn.</ta>
            <ta e="T139" id="Seg_8684" s="T123">Oh, nevertheless, those, who were used to get food from rich people, to get help from the kulaks, were not willing to give their children to school.</ta>
            <ta e="T144" id="Seg_8685" s="T139">Then it became, one was thinking that one has to build up a kolkhoz.</ta>
            <ta e="T150" id="Seg_8686" s="T144">For that the people live in a collective economy, they said.</ta>
            <ta e="T155" id="Seg_8687" s="T150">No, to that they didn't come at once.</ta>
            <ta e="T161" id="Seg_8688" s="T155">Some years passed and a kolkhoz wasn't yet built up.</ta>
            <ta e="T168" id="Seg_8689" s="T161">"We won't succeed in becoming a kolkhoz", they said.</ta>
            <ta e="T175" id="Seg_8690" s="T168">The people, born earlier under the Czarist law and used to it. </ta>
            <ta e="T180" id="Seg_8691" s="T175">Then at some point making…</ta>
            <ta e="T188" id="Seg_8692" s="T180">They were propagating and propagating, the law came, it came to these regions.</ta>
            <ta e="T194" id="Seg_8693" s="T188">We became a kolkhoz in 1937, 1938.</ta>
            <ta e="T198" id="Seg_8694" s="T194">Finally really.</ta>
            <ta e="T202" id="Seg_8695" s="T198">And the kolkhoz came into being.</ta>
            <ta e="T205" id="Seg_8696" s="T202">Finally she was built up.</ta>
            <ta e="T210" id="Seg_8697" s="T205">At that time we created the kolkhoz.</ta>
            <ta e="T216" id="Seg_8698" s="T210">Where there were reindeers, we kept them also in the kolkhoz.</ta>
            <ta e="T223" id="Seg_8699" s="T216">Where we had deadfall traps, so with our own deadfall traps, with our own gin traps.</ta>
            <ta e="T232" id="Seg_8700" s="T223">Finally we built up the kolkhoz life, we reached today's time. </ta>
            <ta e="T239" id="Seg_8701" s="T232">And then the kolkhoz life was good, though.</ta>
            <ta e="T246" id="Seg_8702" s="T239">And so we became a kolkhoz like that.</ta>
            <ta e="T256" id="Seg_8703" s="T246">And then hard times came again.</ta>
            <ta e="T267" id="Seg_8704" s="T256">Those people, whose heads were turned at the kulaks, under the czar, they started to be bandits there.</ta>
            <ta e="T272" id="Seg_8705" s="T267">They also brought us harm.</ta>
            <ta e="T292" id="Seg_8706" s="T272">Those bandits, no, those ranks (?), when the Soviet power was going forward, they cleaned those bandits and put them in order.</ta>
            <ta e="T301" id="Seg_8707" s="T292">When that had stopped, the war with Germany started.</ta>
            <ta e="T307" id="Seg_8708" s="T301">And also there we suffered a lot.</ta>
            <ta e="T319" id="Seg_8709" s="T307">There were collections of clothes, are there were also taxes in the form of money.</ta>
            <ta e="T327" id="Seg_8710" s="T319">We helping the army there in the cities…</ta>
            <ta e="T339" id="Seg_8711" s="T327">Then after those years were over, nothing will happen, I think, today my soul is calm.</ta>
            <ta e="T344" id="Seg_8712" s="T339">Today life got better, the level of living is higher.</ta>
            <ta e="T351" id="Seg_8713" s="T344">Now whole kolkhozes became like organisations.</ta>
            <ta e="T359" id="Seg_8714" s="T351">I don't know what is waiting for us, I myself have turned old.</ta>
            <ta e="T372" id="Seg_8715" s="T359">However, in my opinion we have reached a better life, I say, in my old age.</ta>
            <ta e="T377" id="Seg_8716" s="T372">Now we are very sated and very warm.</ta>
            <ta e="T383" id="Seg_8717" s="T377">We have enough clothes and enough food.</ta>
            <ta e="T393" id="Seg_8718" s="T383">And then there is my big child, Vasiliy.</ta>
            <ta e="T404" id="Seg_8719" s="T393">This child, he has grown up and finished studying, he finished the school and studied to become a teacher.</ta>
            <ta e="T410" id="Seg_8720" s="T404">And so he works today as a teacher.</ta>
            <ta e="T413" id="Seg_8721" s="T410">Then I have a daughter.</ta>
            <ta e="T424" id="Seg_8722" s="T413">She also has finished her education, her school and has also studied, she works as a doctor.</ta>
            <ta e="T441" id="Seg_8723" s="T424">She is fine, and so I am fine, too, they live now in this life, my children now.</ta>
            <ta e="T447" id="Seg_8724" s="T441">And my third one, again a boy, works at the airport.</ta>
            <ta e="T457" id="Seg_8725" s="T447">And then I have my youngest son, Ivan, Ivan Antonov.</ta>
            <ta e="T462" id="Seg_8726" s="T457">He works now at the kolkhoz.</ta>
            <ta e="T476" id="Seg_8727" s="T462">And also working at the kolkhoz, the kolkhoz has its own work, nevertheless at the own kolkhoz.</ta>
            <ta e="T482" id="Seg_8728" s="T476">It gives you work, so that you can feed yourself and dress yourself, though.</ta>
            <ta e="T485" id="Seg_8729" s="T482">And there is salary.</ta>
            <ta e="T498" id="Seg_8730" s="T485">A human, who works well, is better, who works badly, is dropped down.</ta>
            <ta e="T501" id="Seg_8731" s="T498">In my opinion it is like that.</ta>
            <ta e="T513" id="Seg_8732" s="T501">And so I was working as a Selsoviet in a leading position first.</ta>
            <ta e="T522" id="Seg_8733" s="T513">But now young people, who just have finished learning, are working better than I.</ta>
            <ta e="T535" id="Seg_8734" s="T522">And then, without saying a bad word to anyone, I said "I will relax now", and stopped [working] in the Selsoviet.</ta>
            <ta e="T545" id="Seg_8735" s="T535">Now my children, who have finished learning, are living well.</ta>
            <ta e="T551" id="Seg_8736" s="T545">And now I am enjoying my old age.</ta>
            <ta e="T565" id="Seg_8737" s="T551">I myself did not have a good education, therefore I sometimes even don't understand the language, I was.</ta>
            <ta e="T574" id="Seg_8738" s="T565">When I was a Selsoviet, with some interpreter.</ta>
            <ta e="T583" id="Seg_8739" s="T574">One single word, we used to translate via several people, with an interpreter.</ta>
            <ta e="T586" id="Seg_8740" s="T583">That was painful.</ta>
            <ta e="T595" id="Seg_8741" s="T586">Now it is no problem, one talks face to face.</ta>
            <ta e="T605" id="Seg_8742" s="T595">The Soviet power has led us to a very good life, when the Soviet power came.</ta>
            <ta e="T618" id="Seg_8743" s="T605">And also to my children, and I even live until now, I reached a high age so well.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_8744" s="T0">Wie die Sowjetzeit kam, erinnere ich kaum noch.</ta>
            <ta e="T13" id="Seg_8745" s="T7">Damals war ich noch ganz ein Kind.</ta>
            <ta e="T19" id="Seg_8746" s="T13">Ich war ein Kind, das unter keiner Macht arbeitet.</ta>
            <ta e="T36" id="Seg_8747" s="T19">Es wurde gesagt, dass die Sowjetmacht kommt, und wie sich die Ordnung verändert, bemerkt ein verständiger Mensch doch auch.</ta>
            <ta e="T53" id="Seg_8748" s="T36">Als sich die Ordnung änderte, nannte man die Leute, die früher von den reichen Leuten, von den Kulaken gejagt wurden, kurze, enge, arme [Leute].</ta>
            <ta e="T65" id="Seg_8749" s="T53">Damals war ich dennoch bei reichen Menschen Holzfäller, ich war Rentierhirte.</ta>
            <ta e="T73" id="Seg_8750" s="T65">Und dann weiter fing ich an zu verstehen, wahrzunehmen.</ta>
            <ta e="T78" id="Seg_8751" s="T73">Natürlich, die Ordnung ändert sich wirklich.</ta>
            <ta e="T84" id="Seg_8752" s="T78">"Die zaristische Ordnung ist vorbei", sagten sie.</ta>
            <ta e="T95" id="Seg_8753" s="T84">Dann später, dass man in der Schule lernen sollte, sagte der Genosse Großvater Lenin.</ta>
            <ta e="T97" id="Seg_8754" s="T95">Das Gesetz von Lenin.</ta>
            <ta e="T103" id="Seg_8755" s="T97">Nun, sich ändernd, sich ändernd, veränderte es sich schließlich.</ta>
            <ta e="T111" id="Seg_8756" s="T103">Alle Leute, arme, mittellose Kinder und das ganze Volk.</ta>
            <ta e="T123" id="Seg_8757" s="T111">Von allen möglichen, von Armen die Kinder, alle fingen an zu lernen.</ta>
            <ta e="T139" id="Seg_8758" s="T123">Oh, dennoch, diejenigen, die gewöhnt waren, von den Reichen Essen zu bekommen, von den Kulaken Hilfe zu bekommen, waren nicht bereit, ihre Kinder in die Schule zu geben.</ta>
            <ta e="T144" id="Seg_8759" s="T139">Dann kam, man dachte, dass man eine Kolchose braucht.</ta>
            <ta e="T150" id="Seg_8760" s="T144">Damit die Leute in der Kollektivwirtschaft leben, sagten sie.</ta>
            <ta e="T155" id="Seg_8761" s="T150">Nein, dazu kam es auch nicht sofort.</ta>
            <ta e="T161" id="Seg_8762" s="T155">Einige Jahre vergingen und man richtete noch keine Kolchose ein.</ta>
            <ta e="T168" id="Seg_8763" s="T161">"Es wird uns nicht glücken, eine Kolchose zu werden", sagte man.</ta>
            <ta e="T175" id="Seg_8764" s="T168">Die früher unter zaristischen Gesetzen geborenen und daran gewöhnten Leute.</ta>
            <ta e="T180" id="Seg_8765" s="T175">Dann irgendwie machten… </ta>
            <ta e="T188" id="Seg_8766" s="T180">Sie propagierten und propagierten, das Gesetz kam, es kam in diese Regionen.</ta>
            <ta e="T194" id="Seg_8767" s="T188">Eine Kolchose wurden wir 1937, 1938.</ta>
            <ta e="T198" id="Seg_8768" s="T194">Schließlich wirklich.</ta>
            <ta e="T202" id="Seg_8769" s="T198">Und die Kolchose fing auch an.</ta>
            <ta e="T205" id="Seg_8770" s="T202">Sie wurde schließlich eingerichtet.</ta>
            <ta e="T210" id="Seg_8771" s="T205">Damals schufen wir die Kolchose.</ta>
            <ta e="T216" id="Seg_8772" s="T210">Wo es Rentiere gab, die hielten wir auch in der Kolchose.</ta>
            <ta e="T223" id="Seg_8773" s="T216">Wo wir Totfallen hatten, so mit unseren eigenen Totfallen, mit unseren eigenen Fangeisen.</ta>
            <ta e="T232" id="Seg_8774" s="T223">Schließlich haben wir das Kolchosleben aufgebaut, bis in diese Zeit sind wir gekommen.</ta>
            <ta e="T239" id="Seg_8775" s="T232">Und dann war das Kolchosleben doch gut.</ta>
            <ta e="T246" id="Seg_8776" s="T239">Und so auf diese Weise sind wir eine Kolchose geworden.</ta>
            <ta e="T256" id="Seg_8777" s="T246">Und danach kamen wieder schwere Zeiten.</ta>
            <ta e="T267" id="Seg_8778" s="T256">Diejenigen Leute, denen früher bei den Kulaken, unter dem Zaren der Kopf verdreht wurde, fingen an als Banditen ihr Unwesen zu treiben.</ta>
            <ta e="T272" id="Seg_8779" s="T267">Die brachten doch auch Unglück.</ta>
            <ta e="T292" id="Seg_8780" s="T272">Diese Banditen, nein, diese Ränge (?), als die Sowjetmacht voranschritt, säuberte sie diese Banditen und brachte sie in Ordnung.</ta>
            <ta e="T301" id="Seg_8781" s="T292">Als das aufgehört hatte, da fing der Krieg mit Deutschland an.</ta>
            <ta e="T307" id="Seg_8782" s="T301">Und auch dort litten wir sehr.</ta>
            <ta e="T319" id="Seg_8783" s="T307">Es gab Kleidersammlungen, und auch Steuern in Form von Geld gab es.</ta>
            <ta e="T327" id="Seg_8784" s="T319">Wir haben dort in den Städten der Armee geholfen… </ta>
            <ta e="T339" id="Seg_8785" s="T327">Dann nachdem diese Jahre vorbei waren, wird nichts passieren, denke ich, heute ist die Seele ruhig.</ta>
            <ta e="T344" id="Seg_8786" s="T339">Heute ist das Leben besser geworden, der Lebensstandard gestiegen.</ta>
            <ta e="T351" id="Seg_8787" s="T344">Jetzt sind schließlich ganze Kolchosen wie Organisationen geworden.</ta>
            <ta e="T359" id="Seg_8788" s="T351">Ich weiß nicht, was uns erwartet, ich selbst bin alt geworden.</ta>
            <ta e="T372" id="Seg_8789" s="T359">So oder so, meiner Meinung nach, haben wir ein besseres Leben erreicht, sage ich, auf meine alten Tage.</ta>
            <ta e="T377" id="Seg_8790" s="T372">Jetzt sind wir sehr satt und haben es sehr warm.</ta>
            <ta e="T383" id="Seg_8791" s="T377">Wir haben genug anzuziehen und zu essen.</ta>
            <ta e="T393" id="Seg_8792" s="T383">Und nun zuerst ist da mein großes Kind, Vasilij.</ta>
            <ta e="T404" id="Seg_8793" s="T393">Dieses Kind, er ist gewachsen und hat ausgelernt, er hat die Schule beendet und hat auf Lehramt studiert.</ta>
            <ta e="T410" id="Seg_8794" s="T404">Und so arbeitet er heute als Lehrer.</ta>
            <ta e="T413" id="Seg_8795" s="T410">Dann habe ich eine Tochter.</ta>
            <ta e="T424" id="Seg_8796" s="T413">Sie hat auch ihre Ausbildung, die Schule beendet und auch studiert, sie arbeitet als Ärztin.</ta>
            <ta e="T441" id="Seg_8797" s="T424">Ihr geht es gut, und so geht es auch mir gut, sie leben jetzt in diesem Leben, meine Kinder jetzt.</ta>
            <ta e="T447" id="Seg_8798" s="T441">Und mein Dritter, wieder ein Junge, arbeitet am Flughafen.</ta>
            <ta e="T457" id="Seg_8799" s="T447">Und dann habe ich meinen jüngsten Sohn, Ivan, Ivan Antonov.</ta>
            <ta e="T462" id="Seg_8800" s="T457">Der arbeitet jetzt in der Kolchose.</ta>
            <ta e="T476" id="Seg_8801" s="T462">Und auch in der Kolchose arbeitend, und die Kolchose hat ihre eigene Arbeit, dennoch in der eigenen Kolchose.</ta>
            <ta e="T482" id="Seg_8802" s="T476">Sie lässt doch so arbeiten, dass man sich ernähren und anziehen kann.</ta>
            <ta e="T485" id="Seg_8803" s="T482">Und es gibt Lohn.</ta>
            <ta e="T498" id="Seg_8804" s="T485">Ein Mensch, der gut arbeitet, dem geht es besser, wer schlecht arbeitet, der wird fallen gelassen.</ta>
            <ta e="T501" id="Seg_8805" s="T498">Meiner Meinung nach ist das so.</ta>
            <ta e="T513" id="Seg_8806" s="T501">Und so habe ich zuerst doch auch als Selsowjet in leitender Position gearbeitet.</ta>
            <ta e="T522" id="Seg_8807" s="T513">Dann jetzt aber arbeiten junge Leute, die gerade ausgelernt haben, besser als ich.</ta>
            <ta e="T535" id="Seg_8808" s="T522">Und dann, ohne ein böses Wort an jemanden zu richten, sagte ich, "ich ruhe mich jetzt aus", und hörte im Selsowjet auf.</ta>
            <ta e="T545" id="Seg_8809" s="T535">Jetzt meine Kinder, die ausgelernt haben, leben dann gut.</ta>
            <ta e="T551" id="Seg_8810" s="T545">Und jetzt freue ich mich in meinem hohen Alter.</ta>
            <ta e="T565" id="Seg_8811" s="T551">Ich selber habe keine gute Ausbildung gehabt, daher verstehe ich manchmal nicht einmal die Sprache, war ich.</ta>
            <ta e="T574" id="Seg_8812" s="T565">Als ich Selsowjet war, mit irgendeinem Dolmetscher.</ta>
            <ta e="T583" id="Seg_8813" s="T574">Ein einziges Wort haben wir über mehrere Menschen übersetzt, mit einem Dolmetscher.</ta>
            <ta e="T586" id="Seg_8814" s="T583">Das war mühsam.</ta>
            <ta e="T595" id="Seg_8815" s="T586">Jetzt ist das kein Problem, man unterhält sich von Angesicht zu Angesicht.</ta>
            <ta e="T605" id="Seg_8816" s="T595">Die Sowjetmacht hat uns zu einem sehr guten Leben geführt, als die Sowjetmacht kam.</ta>
            <ta e="T618" id="Seg_8817" s="T605">Und auch meinen Kindern, und dass ich sogar bis jetzt lebe, dass ich so gut so ein gesegnetes Alter erreicht habe.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_8818" s="T0">Советская власть как наступала, ещё как-то не помню.</ta>
            <ta e="T13" id="Seg_8819" s="T7">Тогда я тоже совсем ребёнком был.</ta>
            <ta e="T19" id="Seg_8820" s="T13">Ни при какой власти не работающим ребёнком был.</ta>
            <ta e="T36" id="Seg_8821" s="T19">О том, что советская власть наступает, слух так и шёл, (порядки?) всё время менялись, но всё равно понятливый человек осознавал [что происходит].</ta>
            <ta e="T53" id="Seg_8822" s="T36">До смены власти сначала человека, которого богатый человек, кулак, угнетал, малоимущим, бедным называли.</ta>
            <ta e="T65" id="Seg_8823" s="T53">Тогда вот я у богачей дровосеком был всё-таки, оленеводом был.</ta>
            <ta e="T73" id="Seg_8824" s="T65">Потом поближе понимать начал, узнавать начал.</ta>
            <ta e="T78" id="Seg_8825" s="T73">Конечно, и вправду, власть меняется.</ta>
            <ta e="T84" id="Seg_8826" s="T78">«Старое царское время наконец-то завершилось», сказали.</ta>
            <ta e="T95" id="Seg_8827" s="T84">Потом позже товарищ Ленин-дедушка сказал, чтобы в школе начали учиться.</ta>
            <ta e="T97" id="Seg_8828" s="T95">Закон Ленина.</ta>
            <ta e="T103" id="Seg_8829" s="T97">Ну вот так менялось-менялось и поменялось.</ta>
            <ta e="T111" id="Seg_8830" s="T103">Все люди: дети бедняков, неимущих, всякий народ.</ta>
            <ta e="T123" id="Seg_8831" s="T111">Разные: и бедняков, и неимущих дети, все учиться начали.</ta>
            <ta e="T139" id="Seg_8832" s="T123">О, и даже тогда те, кто привык у богачей кормиться, кто привык у кулака помощь получать, всё-таки детей не собирались в школу отдавать.</ta>
            <ta e="T144" id="Seg_8833" s="T139">Потом стало нужно, стали думать колхоз образовать. </ta>
            <ta e="T150" id="Seg_8834" s="T144">Советовали, чтобы народ общим хозяйством жизнь свою наладил.</ta>
            <ta e="T155" id="Seg_8835" s="T150">Нет, к этому тоже не сразу пришли.</ta>
            <ta e="T161" id="Seg_8836" s="T155">Несколько лет тянули, в колхоз не объединялись.</ta>
            <ta e="T168" id="Seg_8837" s="T161">Говорили, если в колхоз перейдём, нам не сдобровать.</ta>
            <ta e="T175" id="Seg_8838" s="T168">Народ, родившийся раньше, при царском законе, привыкший народ.</ta>
            <ta e="T180" id="Seg_8839" s="T175">Затем каким-то образом… </ta>
            <ta e="T188" id="Seg_8840" s="T180">Агитировали, агитировали, закон утверждался, утверждался, доходил до этих мест.</ta>
            <ta e="T194" id="Seg_8841" s="T188">Колхозом стали с тридцать седьмого, в тридцать восьмом.</ta>
            <ta e="T198" id="Seg_8842" s="T194">Наконец-то, и вправду.</ta>
            <ta e="T202" id="Seg_8843" s="T198">Вот тут колхоз и возник.</ta>
            <ta e="T205" id="Seg_8844" s="T202">Устроились всё-таки.</ta>
            <ta e="T210" id="Seg_8845" s="T205">Тогда мы создали колхоз.</ta>
            <ta e="T216" id="Seg_8846" s="T210">Где больше оленей было, там и в колхозе держать стали.</ta>
            <ta e="T223" id="Seg_8847" s="T216">Где пасти были, там и со своими пастями, со своими капканами [охотились].</ta>
            <ta e="T232" id="Seg_8848" s="T223">Таким образом, колхозную жизнь поднимая, дошли вот сейчас до нынешних времён.</ta>
            <ta e="T239" id="Seg_8849" s="T232">И вот потом думали, что колхозная жизнь хороша.</ta>
            <ta e="T246" id="Seg_8850" s="T239">Таким образом вот колхозом стали.</ta>
            <ta e="T256" id="Seg_8851" s="T246">И после этого времени опять тяжёлые времена бывали. </ta>
            <ta e="T267" id="Seg_8852" s="T256">Те люди, с головой, замутнённой у кулаков, в давние царские времена, там и бандитствовали.</ta>
            <ta e="T272" id="Seg_8853" s="T267">Они тоже горе приносили.</ta>
            <ta e="T292" id="Seg_8854" s="T272">Этих бандиты, нет, те чины (?), и опять-таки Советская власть, шагая вперёд, этих бандитов всё-таки вычищала и пресекала. </ta>
            <ta e="T301" id="Seg_8855" s="T292">И после этого, ни с того ни с сего вот германская война началась.</ta>
            <ta e="T307" id="Seg_8856" s="T301">И вот в этом мы сильно поучаствовали.</ta>
            <ta e="T319" id="Seg_8857" s="T307">И одежду собирали, и деньгами налог собирали.</ta>
            <ta e="T327" id="Seg_8858" s="T319">Мы там, помогая армии в городах… </ta>
            <ta e="T339" id="Seg_8859" s="T327">И вот, когда она прекратилась, сейчас не думаю, что что-то случится, на сегодня душа спокойна.</ta>
            <ta e="T344" id="Seg_8860" s="T339">Сейчас жизнь выровнялась, [качество] жизни возросло.</ta>
            <ta e="T351" id="Seg_8861" s="T344">Сейчас целые колхозы стали как организации.</ta>
            <ta e="T359" id="Seg_8862" s="T351">Теперь не знаю, что впереди ждёт, сам состарился.</ta>
            <ta e="T372" id="Seg_8863" s="T359">Так и так, по моему мнению, сейчас до лучшей жизни дошли, говорю, к моей старости.</ta>
            <ta e="T377" id="Seg_8864" s="T372">Сейчас мы сыты, в тепле.</ta>
            <ta e="T383" id="Seg_8865" s="T377">Одеваемся как хотим, едим что хотим.</ta>
            <ta e="T393" id="Seg_8866" s="T383">Вот сначала мой старший сын, этот Василий.</ta>
            <ta e="T404" id="Seg_8867" s="T393">Этот сын, как подрос, отучился он, школу как закончил, на учителя выучился.</ta>
            <ta e="T410" id="Seg_8868" s="T404">Сейчас, на сегодняшний день учителем работает.</ta>
            <ta e="T413" id="Seg_8869" s="T410">Ещё одна дочь есть.</ta>
            <ta e="T424" id="Seg_8870" s="T413">Она тоже, как школу закончила, тоже на высшем обучилась, врачом работает.</ta>
            <ta e="T441" id="Seg_8871" s="T424">Ей самой хорошо, и мне сейчас хорошо — как в этой жизни живут сейчас эти мои дети.</ta>
            <ta e="T447" id="Seg_8872" s="T441">И ещё третий сын тоже в аэропорту работает.</ta>
            <ta e="T457" id="Seg_8873" s="T447">Потом самый младший сын есть сейчас, Иван, Антонов Иван (Михеич?).</ta>
            <ta e="T462" id="Seg_8874" s="T457">Тот вот сейчас в колзозе работает.</ta>
            <ta e="T476" id="Seg_8875" s="T462">И в колхозе тоже работа, и колхоз своя работа есть, всё равно ведь внутри самого колхоза.</ta>
            <ta e="T482" id="Seg_8876" s="T476">Хорошо кормит, одевает, всё-таки даёт поработать.</ta>
            <ta e="T485" id="Seg_8877" s="T482">И зарплата всё-таки есть.</ta>
            <ta e="T498" id="Seg_8878" s="T485">Человек, где хорошо работает, там лучше становится, где плохо работает, там и опускается.</ta>
            <ta e="T501" id="Seg_8879" s="T498">По моему мнению так.</ta>
            <ta e="T513" id="Seg_8880" s="T501">И я сам же сначала опять-таки тут в сельсовете руководителем работал.</ta>
            <ta e="T522" id="Seg_8881" s="T513">Потом сейчас молодые люди, отучившись, лучше меня работают.</ta>
            <ta e="T535" id="Seg_8882" s="T522">И тогда, ни на кого не обижаясь, решил, сейчас отдохну, ушёл из сельсовета.</ta>
            <ta e="T545" id="Seg_8883" s="T535">Сейчас вот мои дети, после того как обучились, свою жизнь хорошо устроили.</ta>
            <ta e="T551" id="Seg_8884" s="T545">Сейчас, в преклонном возрасте сижу радуюсь. </ta>
            <ta e="T565" id="Seg_8885" s="T551">Я сам неграмотным был, иногда даже языка не понимал.</ta>
            <ta e="T574" id="Seg_8886" s="T565">Когда я в сельсовете был, то с каким-нибудь переводчиком.</ta>
            <ta e="T583" id="Seg_8887" s="T574">Даже одно слово через нескольких людей переводили.</ta>
            <ta e="T586" id="Seg_8888" s="T583">Это мучение было.</ta>
            <ta e="T595" id="Seg_8889" s="T586">Сейчас без проблем, сами лично хоть с кем разговаривают.</ta>
            <ta e="T605" id="Seg_8890" s="T595">Советская власть сейчас к хорошей, спокойной жизни привела, когда Советская власть наступила.</ta>
            <ta e="T618" id="Seg_8891" s="T605">И детям тоже, и я даже до сих пор дожил сейчас, очень хорошо до такого преклонного возраста дожил.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_8892" s="T0">Советская власть как наступала ещё как-то не помню.</ta>
            <ta e="T13" id="Seg_8893" s="T7">Тогда я тоже ребёнком был.</ta>
            <ta e="T19" id="Seg_8894" s="T13">Ни при какой власти не работающим ребёнком был.</ta>
            <ta e="T36" id="Seg_8895" s="T19">Советская власть наступает, слух всё-так и шёл, как время меняется всё равно понятливый человек осозновал.</ta>
            <ta e="T53" id="Seg_8896" s="T36">До смены власти сначала человека раньше богатый человек, кулак угнетал, малоимущих, бедных так и называли.</ta>
            <ta e="T65" id="Seg_8897" s="T53">Тогда вот я у богачей дровосеком был всё-таки, оленеводом был.</ta>
            <ta e="T73" id="Seg_8898" s="T65">И поближе, потом понимать начал, узнавать начал.</ta>
            <ta e="T78" id="Seg_8899" s="T73">Конечно, и в правду, власть меняется.</ta>
            <ta e="T84" id="Seg_8900" s="T78">Старое царское время наконец-то завершилось, сказали.</ta>
            <ta e="T95" id="Seg_8901" s="T84">Потом позже всё-таки, чтобы в школе начали учиться, думая – товарищ Ленине-дедушка, речь шла.</ta>
            <ta e="T97" id="Seg_8902" s="T95">Ленина закон.</ta>
            <ta e="T103" id="Seg_8903" s="T97">Ну, меняясь, и вот поменялось так и.</ta>
            <ta e="T111" id="Seg_8904" s="T103">Все люди: бедняков, неимущих дети и весь народ.</ta>
            <ta e="T123" id="Seg_8905" s="T111">Разные: и бедняков, и неимущих дети, все учиться начали.</ta>
            <ta e="T139" id="Seg_8906" s="T123">О, и даже тогда, у богачей кушать привыкшие, у кулака получить помощь привыкшие, всё-таки в школу отдавать не собирались детей.</ta>
            <ta e="T144" id="Seg_8907" s="T139">Потом стало, колхоз образовать надо, подумывали. </ta>
            <ta e="T150" id="Seg_8908" s="T144">Общим хозяйством народ, чтобы жизнь свою наладил, посоветовали.</ta>
            <ta e="T155" id="Seg_8909" s="T150">Нет, к этому тоже сразу не пришли.</ta>
            <ta e="T161" id="Seg_8910" s="T155">Несколько лет продолжая, в колхоз не объединялись.</ta>
            <ta e="T168" id="Seg_8911" s="T161">В колхоз если перейдем, нам не сдобровать, говорили.</ta>
            <ta e="T175" id="Seg_8912" s="T168">Раньше в царском законе родивший народ, привыкший народ.</ta>
            <ta e="T180" id="Seg_8913" s="T175">Затем каким-то образом… </ta>
            <ta e="T188" id="Seg_8914" s="T180">Агитировали, агитировали, закон утверждался, утверждался, к этим местам дойдя, дойдя.</ta>
            <ta e="T194" id="Seg_8915" s="T188">Колхозом стали с тридцать седьмого, в тридцать восьмом.</ta>
            <ta e="T198" id="Seg_8916" s="T194">Наконец-то, и вправду.</ta>
            <ta e="T202" id="Seg_8917" s="T198">Вот тут колхоз и начался.</ta>
            <ta e="T205" id="Seg_8918" s="T202">Устроились все-таки.</ta>
            <ta e="T210" id="Seg_8919" s="T205">Тогда колхоз создали мы.</ta>
            <ta e="T216" id="Seg_8920" s="T210">Где больше оленей было, там и в колхозе держать стали.</ta>
            <ta e="T223" id="Seg_8921" s="T216">Где пасти есть, так и своим пастем, своим капканом.</ta>
            <ta e="T232" id="Seg_8922" s="T223">Таким образом, колхозную жизнь поднимая, сейчас до нынешних времён дошли вот.</ta>
            <ta e="T239" id="Seg_8923" s="T232">И вот потом колхозная жизнь хороша что ли, думая.</ta>
            <ta e="T246" id="Seg_8924" s="T239">Вот таким образом, вот колхозом стали.</ta>
            <ta e="T256" id="Seg_8925" s="T246">И после этого времени опять таки тяжёлые времена бывали. </ta>
            <ta e="T267" id="Seg_8926" s="T256">Те, у кулаков, в давние царские времена замутненные головой люди там и бандитствовали.</ta>
            <ta e="T272" id="Seg_8927" s="T267">Они тоже горе приносили.</ta>
            <ta e="T292" id="Seg_8928" s="T272">Этих бандитов, нет, и опять таки Советская власть вперёд шагая, этого бандита всё-таки чистила и пресекала. </ta>
            <ta e="T301" id="Seg_8929" s="T292">И после этого, не с того ни с сего вот Германская война началась.</ta>
            <ta e="T307" id="Seg_8930" s="T301">И вот в это вмешались мы очень.</ta>
            <ta e="T319" id="Seg_8931" s="T307">И одежду собирали, и деньгами налог делать начали.</ta>
            <ta e="T327" id="Seg_8932" s="T319">Мы там, в городах армии помогая… </ta>
            <ta e="T339" id="Seg_8933" s="T327">И вот, как она прекратилась сейчас что-то случится не думаю, на сегодня душа спокойна.</ta>
            <ta e="T344" id="Seg_8934" s="T339">Сейчас жизнь выпрямилась, жизнь повысилась.</ta>
            <ta e="T351" id="Seg_8935" s="T344">Сейчас целые колхозы стали как организации.</ta>
            <ta e="T359" id="Seg_8936" s="T351">Сейчас что впереди ждёт не знаю, сам состарился.</ta>
            <ta e="T372" id="Seg_8937" s="T359">Так и так, по моему мнению, к лучшей жизни сейчас дошли, говорю, к моей старости.</ta>
            <ta e="T377" id="Seg_8938" s="T372">Сейчас сыты, в тепле.</ta>
            <ta e="T383" id="Seg_8939" s="T377">Одеваемся как можем, и кушаем что хотим.</ta>
            <ta e="T393" id="Seg_8940" s="T383">Вот же сначала мой старший сын, этот Василий есть.</ta>
            <ta e="T404" id="Seg_8941" s="T393">Этот сын, как подрос, отучился он, школу как закончил, на учителя выучился.</ta>
            <ta e="T410" id="Seg_8942" s="T404">Сейчас, на сегодняшний день учителем работает.</ta>
            <ta e="T413" id="Seg_8943" s="T410">Ещё одна дочь есть.</ta>
            <ta e="T424" id="Seg_8944" s="T413">Она тоже, как школу закончила, тоже на высшем обучилась, опять врачём работает.</ta>
            <ta e="T441" id="Seg_8945" s="T424">Ей самой хорошо, и так же и мне сейчас хорошо – в этой жизни как живут, сейчас эти дети мои.</ta>
            <ta e="T447" id="Seg_8946" s="T441">И ещё третий опять сын в авиапорту работает.</ta>
            <ta e="T457" id="Seg_8947" s="T447">И потом самый младший сын есть сейчас, Иван, Антонов Иван (Михеич ?).</ta>
            <ta e="T462" id="Seg_8948" s="T457">Тот вот сейчас в колзозе работает.</ta>
            <ta e="T476" id="Seg_8949" s="T462">И колхоз тоже работая, и труд свой имеет колхоз, всё равно внутри самого колхоза таки.</ta>
            <ta e="T482" id="Seg_8950" s="T476">Хорошо кормит, одевает всё таки даёт поработать.</ta>
            <ta e="T485" id="Seg_8951" s="T482">И всё таки зарплата есть.</ta>
            <ta e="T498" id="Seg_8952" s="T485">Человек, где хорошо работает, там лучше становится, где плохо работает там и опускается.</ta>
            <ta e="T501" id="Seg_8953" s="T498">По моему мнению так.</ta>
            <ta e="T513" id="Seg_8954" s="T501">И тут же в начале сам опять таки тут в сельсовете руководителем проработал.</ta>
            <ta e="T522" id="Seg_8955" s="T513">Потом сейчас молодые люди, отучившись, чем я лучше работают.</ta>
            <ta e="T535" id="Seg_8956" s="T522">И тогда, ни на кого не обижаясь, сейчас отдохну, подумав, с сельсовет ушёл было.</ta>
            <ta e="T545" id="Seg_8957" s="T535">Сейчас вот мои дети, после как обучились, хорошую жизнь свою устроили.</ta>
            <ta e="T551" id="Seg_8958" s="T545">Сейчас радуюсь сижу в преклонном возрасте. </ta>
            <ta e="T565" id="Seg_8959" s="T551">Сам не грамотным был, иногда даже я языка не понимал.</ta>
            <ta e="T574" id="Seg_8960" s="T565">Когда я в сельсовете был с каким-нибудь переводчиком.</ta>
            <ta e="T583" id="Seg_8961" s="T574">Хоть одно слово через нескольких людей переводили.</ta>
            <ta e="T586" id="Seg_8962" s="T583">Это мучение было.</ta>
            <ta e="T595" id="Seg_8963" s="T586">Сейчас без проблем, с глазу на глаз разговаривают хоть с кем.</ta>
            <ta e="T605" id="Seg_8964" s="T595">Советская власть к хорошей, спокойной жизни привела сейчас, как Советская власть наступила.</ta>
            <ta e="T618" id="Seg_8965" s="T605">И детям тоже, и я даже до сих пор дожил сейчас, очень хорошо до такого преклонного возраста дожил.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T36" id="Seg_8966" s="T19">[AAV] check glosses for ɨjaːk and tu͡olkuluːr (%)</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T619" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
