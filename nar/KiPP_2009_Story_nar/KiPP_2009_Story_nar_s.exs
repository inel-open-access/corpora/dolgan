<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDAAB8F230-A069-F26B-B2EA-9F90A08ABE13">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KiPP_2009_Story_nar</transcription-name>
         <referenced-file url="KiPP_2009_Story_nar.wav" />
         <referenced-file url="KiPP_2009_Story_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KiPP_2009_Story_nar\KiPP_2009_Story_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">557</ud-information>
            <ud-information attribute-name="# HIAT:w">389</ud-information>
            <ud-information attribute-name="# e">386</ud-information>
            <ud-information attribute-name="# HIAT:u">72</ud-information>
            <ud-information attribute-name="# sc">34</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KiPP">
            <abbreviation>KiPP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="SE">
            <abbreviation>SE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.016" type="appl" />
         <tli id="T2" time="0.4406666666666667" type="appl" />
         <tli id="T3" time="0.8653333333333334" type="appl" />
         <tli id="T4" time="1.29" type="appl" />
         <tli id="T5" time="1.7379090909090908" type="appl" />
         <tli id="T6" time="2.1858181818181817" type="appl" />
         <tli id="T7" time="2.6337272727272727" type="appl" />
         <tli id="T8" time="3.0816363636363633" type="appl" />
         <tli id="T9" time="3.5295454545454543" type="appl" />
         <tli id="T10" time="3.9774545454545454" type="appl" />
         <tli id="T11" time="4.425363636363636" type="appl" />
         <tli id="T12" time="4.873272727272727" type="appl" />
         <tli id="T13" time="5.321181818181818" type="appl" />
         <tli id="T14" time="5.769090909090909" type="appl" />
         <tli id="T15" time="6.217" type="appl" />
         <tli id="T16" time="6.710666666666667" type="appl" />
         <tli id="T17" time="7.2043333333333335" type="appl" />
         <tli id="T18" time="7.698" type="appl" />
         <tli id="T19" time="8.191666666666666" type="appl" />
         <tli id="T20" time="8.685333333333332" type="appl" />
         <tli id="T21" time="9.232332430847073" />
         <tli id="T22" time="9.594875" type="appl" />
         <tli id="T23" time="10.01075" type="appl" />
         <tli id="T24" time="10.426625" type="appl" />
         <tli id="T25" time="10.842500000000001" type="appl" />
         <tli id="T26" time="11.258375000000001" type="appl" />
         <tli id="T27" time="11.67425" type="appl" />
         <tli id="T28" time="12.090125" type="appl" />
         <tli id="T29" time="12.539332821407845" />
         <tli id="T30" time="13.445" type="appl" />
         <tli id="T31" time="14.317335338882062" />
         <tli id="T32" time="16.803" type="appl" />
         <tli id="T33" time="17.183555555555557" type="appl" />
         <tli id="T34" time="17.564111111111114" type="appl" />
         <tli id="T35" time="17.944666666666667" type="appl" />
         <tli id="T36" time="18.325222222222223" type="appl" />
         <tli id="T37" time="18.70577777777778" type="appl" />
         <tli id="T38" time="19.086333333333336" type="appl" />
         <tli id="T39" time="19.46688888888889" type="appl" />
         <tli id="T40" time="19.847444444444445" type="appl" />
         <tli id="T41" time="21.11291290787627" />
         <tli id="T42" time="21.371956453938136" type="intp" />
         <tli id="T43" time="21.631" type="appl" />
         <tli id="T44" time="22.332500000000003" type="appl" />
         <tli id="T45" time="23.034000000000002" type="appl" />
         <tli id="T46" time="23.735500000000002" type="appl" />
         <tli id="T47" time="24.437" type="appl" />
         <tli id="T48" time="25.1385" type="appl" />
         <tli id="T49" time="25.84" type="appl" />
         <tli id="T50" time="26.5415" type="appl" />
         <tli id="T51" time="27.243000000000002" type="appl" />
         <tli id="T52" time="27.9445" type="appl" />
         <tli id="T53" time="28.646" type="appl" />
         <tli id="T54" time="29.3475" type="appl" />
         <tli id="T55" time="30.80605321985988" />
         <tli id="T56" time="30.9832" type="appl" />
         <tli id="T57" time="31.9174" type="appl" />
         <tli id="T58" time="32.8516" type="appl" />
         <tli id="T59" time="33.7858" type="appl" />
         <tli id="T60" time="34.4793134068633" />
         <tli id="T61" time="35.177" type="appl" />
         <tli id="T62" time="35.634" type="appl" />
         <tli id="T63" time="36.091" type="appl" />
         <tli id="T64" time="36.548" type="appl" />
         <tli id="T65" time="37.005" type="appl" />
         <tli id="T66" time="37.66591661809313" />
         <tli id="T67" time="37.9624" type="appl" />
         <tli id="T68" time="38.4628" type="appl" />
         <tli id="T69" time="38.9632" type="appl" />
         <tli id="T70" time="39.4636" type="appl" />
         <tli id="T71" time="40.09732978944108" />
         <tli id="T72" time="40.673" type="appl" />
         <tli id="T73" time="41.49533254970412" />
         <tli id="T74" time="42.126999999999995" type="appl" />
         <tli id="T75" time="42.872" type="appl" />
         <tli id="T76" time="43.617000000000004" type="appl" />
         <tli id="T77" time="44.4086664630142" />
         <tli id="T78" time="45.96575134190303" />
         <tli id="T79" time="53.324" type="appl" />
         <tli id="T80" time="54.20171428571428" type="appl" />
         <tli id="T81" time="55.07942857142857" type="appl" />
         <tli id="T82" time="55.957142857142856" type="appl" />
         <tli id="T83" time="56.83485714285714" type="appl" />
         <tli id="T84" time="57.71257142857142" type="appl" />
         <tli id="T85" time="58.59028571428571" type="appl" />
         <tli id="T86" time="59.467999999999996" type="appl" />
         <tli id="T87" time="60.34571428571428" type="appl" />
         <tli id="T88" time="61.22342857142857" type="appl" />
         <tli id="T89" time="62.101142857142854" type="appl" />
         <tli id="T90" time="62.97885714285714" type="appl" />
         <tli id="T91" time="63.85657142857143" type="appl" />
         <tli id="T92" time="64.306" type="appl" />
         <tli id="T93" time="64.6325" type="appl" />
         <tli id="T94" time="64.7342857142857" type="appl" />
         <tli id="T95" time="64.959" type="appl" />
         <tli id="T96" time="65.2855" type="appl" />
         <tli id="T97" time="65.612" type="appl" />
         <tli id="T98" time="66.085" type="appl" />
         <tli id="T99" time="66.558" type="appl" />
         <tli id="T100" time="67.028" type="appl" />
         <tli id="T101" time="67.32432993070846" />
         <tli id="T102" time="67.42883333333334" type="appl" />
         <tli id="T103" time="67.82966666666667" type="appl" />
         <tli id="T104" time="68.2305" type="appl" />
         <tli id="T105" time="68.63133333333333" type="appl" />
         <tli id="T106" time="69.03216666666667" type="appl" />
         <tli id="T107" time="69.43299999999999" type="appl" />
         <tli id="T108" time="69.83383333333333" type="appl" />
         <tli id="T109" time="70.23466666666667" type="appl" />
         <tli id="T110" time="70.6355" type="appl" />
         <tli id="T111" time="71.03633333333333" type="appl" />
         <tli id="T112" time="71.43716666666666" type="appl" />
         <tli id="T113" time="71.838" type="appl" />
         <tli id="T114" time="72.5165" type="appl" />
         <tli id="T115" time="73.195" type="appl" />
         <tli id="T116" time="73.8735" type="appl" />
         <tli id="T117" time="74.552" type="appl" />
         <tli id="T118" time="75.2305" type="appl" />
         <tli id="T119" time="76.13181730595106" />
         <tli id="T120" time="76.45687500000001" type="appl" />
         <tli id="T121" time="77.00475" type="appl" />
         <tli id="T122" time="77.552625" type="appl" />
         <tli id="T123" time="78.10050000000001" type="appl" />
         <tli id="T124" time="78.648375" type="appl" />
         <tli id="T125" time="79.19625" type="appl" />
         <tli id="T126" time="79.744125" type="appl" />
         <tli id="T127" time="80.40533246704616" />
         <tli id="T128" time="81.295" type="appl" />
         <tli id="T129" time="82.298" type="appl" />
         <tli id="T130" time="83.57432535187044" />
         <tli id="T131" time="83.93536363636363" type="appl" />
         <tli id="T132" time="84.56972727272728" type="appl" />
         <tli id="T133" time="85.20409090909091" type="appl" />
         <tli id="T134" time="85.83845454545454" type="appl" />
         <tli id="T135" time="86.47281818181818" type="appl" />
         <tli id="T136" time="87.10718181818181" type="appl" />
         <tli id="T137" time="87.74154545454546" type="appl" />
         <tli id="T138" time="88.37590909090909" type="appl" />
         <tli id="T139" time="89.01027272727272" type="appl" />
         <tli id="T140" time="89.64463636363637" type="appl" />
         <tli id="T141" time="89.71821342412342" />
         <tli id="T142" time="91.04599999999999" type="appl" />
         <tli id="T143" time="91.813" type="appl" />
         <tli id="T144" time="92.58" type="appl" />
         <tli id="T145" time="93.5185" type="appl" />
         <tli id="T146" time="94.457" type="appl" />
         <tli id="T147" time="95.3955" type="appl" />
         <tli id="T148" time="96.68732594894132" />
         <tli id="T149" time="98.2685" type="appl" />
         <tli id="T150" time="100.60965669698854" />
         <tli id="T151" time="100.71216666666666" type="appl" />
         <tli id="T152" time="101.22133333333333" type="appl" />
         <tli id="T153" time="101.7305" type="appl" />
         <tli id="T154" time="102.23966666666666" type="appl" />
         <tli id="T155" time="102.74883333333332" type="appl" />
         <tli id="T156" time="103.14461272834279" />
         <tli id="T157" time="105.0895" type="appl" />
         <tli id="T158" time="106.921" type="appl" />
         <tli id="T159" time="107.7295" type="appl" />
         <tli id="T160" time="108.53800000000001" type="appl" />
         <tli id="T161" time="109.3465" type="appl" />
         <tli id="T162" time="110.155" type="appl" />
         <tli id="T163" time="110.96350000000001" type="appl" />
         <tli id="T164" time="111.772" type="appl" />
         <tli id="T165" time="112.7335" type="appl" />
         <tli id="T166" time="113.695" type="appl" />
         <tli id="T167" time="114.6565" type="appl" />
         <tli id="T168" time="115.618" type="appl" />
         <tli id="T169" time="116.3818" type="appl" />
         <tli id="T170" time="117.1456" type="appl" />
         <tli id="T171" time="117.90939999999999" type="appl" />
         <tli id="T172" time="118.6732" type="appl" />
         <tli id="T173" time="121.17092043370985" />
         <tli id="T174" time="121.41312688352158" type="intp" />
         <tli id="T175" time="121.65533333333333" type="appl" />
         <tli id="T176" time="122.7645" type="appl" />
         <tli id="T177" time="123.87366666666667" type="appl" />
         <tli id="T178" time="124.98283333333333" type="appl" />
         <tli id="T179" time="125.93199750047516" />
         <tli id="T180" time="126.87757142857143" type="appl" />
         <tli id="T181" time="127.66314285714286" type="appl" />
         <tli id="T182" time="128.4487142857143" type="appl" />
         <tli id="T183" time="129.2342857142857" type="appl" />
         <tli id="T184" time="130.01985714285715" type="appl" />
         <tli id="T185" time="130.8054285714286" type="appl" />
         <tli id="T186" time="131.591" type="appl" />
         <tli id="T187" time="132.651" type="appl" />
         <tli id="T188" time="133.71099999999998" type="appl" />
         <tli id="T189" time="137.91058709353848" />
         <tli id="T190" time="138.02251582867694" type="intp" />
         <tli id="T191" time="138.1344445638154" type="intp" />
         <tli id="T192" time="138.24637329895387" type="intp" />
         <tli id="T193" time="138.35830203409233" type="intp" />
         <tli id="T194" time="138.47023076923077" type="appl" />
         <tli id="T195" time="139.2100769230769" type="appl" />
         <tli id="T196" time="139.94992307692308" type="appl" />
         <tli id="T197" time="140.68976923076923" type="appl" />
         <tli id="T198" time="141.42961538461537" type="appl" />
         <tli id="T199" time="142.16946153846155" type="appl" />
         <tli id="T200" time="142.9093076923077" type="appl" />
         <tli id="T201" time="143.64915384615387" type="appl" />
         <tli id="T202" time="144.389" type="appl" />
         <tli id="T203" time="144.99460000000002" type="appl" />
         <tli id="T204" time="145.6002" type="appl" />
         <tli id="T205" time="146.2058" type="appl" />
         <tli id="T206" time="146.8114" type="appl" />
         <tli id="T207" time="147.417" type="appl" />
         <tli id="T208" time="148.64666666666668" type="appl" />
         <tli id="T209" time="149.87633333333332" type="appl" />
         <tli id="T210" time="151.57698161868734" />
         <tli id="T211" time="151.583" type="appl" />
         <tli id="T212" time="152.06" type="appl" />
         <tli id="T213" time="152.537" type="appl" />
         <tli id="T214" time="153.014" type="appl" />
         <tli id="T215" time="153.49099999999999" type="appl" />
         <tli id="T216" time="153.968" type="appl" />
         <tli id="T217" time="154.445" type="appl" />
         <tli id="T218" time="154.922" type="appl" />
         <tli id="T219" time="155.399" type="appl" />
         <tli id="T220" time="156.1102246806879" />
         <tli id="T221" time="156.59266666666667" type="appl" />
         <tli id="T222" time="157.30933333333334" type="appl" />
         <tli id="T223" time="158.026" type="appl" />
         <tli id="T224" time="158.74266666666665" type="appl" />
         <tli id="T225" time="159.45933333333332" type="appl" />
         <tli id="T226" time="160.176" type="appl" />
         <tli id="T227" time="160.72416666666666" type="appl" />
         <tli id="T228" time="161.27233333333334" type="appl" />
         <tli id="T229" time="161.82049999999998" type="appl" />
         <tli id="T230" time="162.36866666666666" type="appl" />
         <tli id="T231" time="162.91683333333333" type="appl" />
         <tli id="T232" time="163.465" type="appl" />
         <tli id="T233" time="163.93116666666668" type="appl" />
         <tli id="T234" time="164.39733333333334" type="appl" />
         <tli id="T235" time="164.8635" type="appl" />
         <tli id="T236" time="165.32966666666667" type="appl" />
         <tli id="T237" time="165.79583333333335" type="appl" />
         <tli id="T238" time="166.262" type="appl" />
         <tli id="T239" time="166.72816666666665" type="appl" />
         <tli id="T240" time="167.19433333333333" type="appl" />
         <tli id="T241" time="167.6605" type="appl" />
         <tli id="T242" time="168.12666666666667" type="appl" />
         <tli id="T243" time="168.59283333333332" type="appl" />
         <tli id="T244" time="172.3965670343165" />
         <tli id="T245" time="172.58764468954433" type="intp" />
         <tli id="T246" time="172.77872234477218" type="intp" />
         <tli id="T247" time="172.9698" type="appl" />
         <tli id="T248" time="174.2734" type="appl" />
         <tli id="T249" time="175.577" type="appl" />
         <tli id="T250" time="176.8155" type="appl" />
         <tli id="T251" time="178.054" type="appl" />
         <tli id="T252" time="178.51966666666667" type="appl" />
         <tli id="T253" time="178.98533333333333" type="appl" />
         <tli id="T254" time="179.152" type="appl" />
         <tli id="T255" time="179.451" type="appl" />
         <tli id="T256" time="179.499" type="appl" />
         <tli id="T257" time="179.846" type="appl" />
         <tli id="T258" time="180.3945" type="appl" />
         <tli id="T259" time="180.943" type="appl" />
         <tli id="T260" time="181.4915" type="appl" />
         <tli id="T261" time="182.04000000000002" type="appl" />
         <tli id="T262" time="182.5885" type="appl" />
         <tli id="T263" time="183.137" type="appl" />
         <tli id="T264" time="183.68550000000002" type="appl" />
         <tli id="T265" time="184.45399620326566" />
         <tli id="T266" time="185.30700000000002" type="appl" />
         <tli id="T267" time="186.38" type="appl" />
         <tli id="T268" time="187.453" type="appl" />
         <tli id="T269" time="188.52599999999998" type="appl" />
         <tli id="T270" time="189.599" type="appl" />
         <tli id="T271" time="190.59575" type="appl" />
         <tli id="T272" time="191.5925" type="appl" />
         <tli id="T273" time="192.58925" type="appl" />
         <tli id="T274" time="193.69266639853262" />
         <tli id="T275" time="194.40290000000002" type="appl" />
         <tli id="T276" time="195.21980000000002" type="appl" />
         <tli id="T277" time="196.0367" type="appl" />
         <tli id="T278" time="196.8536" type="appl" />
         <tli id="T279" time="197.6705" type="appl" />
         <tli id="T280" time="198.4874" type="appl" />
         <tli id="T281" time="199.3043" type="appl" />
         <tli id="T282" time="200.1212" type="appl" />
         <tli id="T283" time="200.9381" type="appl" />
         <tli id="T284" time="203.46928161076465" />
         <tli id="T285" time="204.0445" type="appl" />
         <tli id="T286" time="206.32922466017385" />
         <tli id="T287" time="206.335" type="appl" />
         <tli id="T288" time="206.7715" type="appl" />
         <tli id="T289" time="207.208" type="appl" />
         <tli id="T290" time="207.6445" type="appl" />
         <tli id="T291" time="208.08100000000002" type="appl" />
         <tli id="T292" time="208.5175" type="appl" />
         <tli id="T293" time="208.954" type="appl" />
         <tli id="T294" time="209.3905" type="appl" />
         <tli id="T295" time="209.822" type="appl" />
         <tli id="T296" time="209.84699315184633" />
         <tli id="T297" time="210.43966666666668" type="appl" />
         <tli id="T298" time="211.05733333333333" type="appl" />
         <tli id="T299" time="211.641670955752" />
         <tli id="T300" time="212.8904" type="appl" />
         <tli id="T301" time="214.10580000000002" type="appl" />
         <tli id="T302" time="215.3212" type="appl" />
         <tli id="T303" time="216.53660000000002" type="appl" />
         <tli id="T304" time="217.7253388730137" />
         <tli id="T305" time="218.81075" type="appl" />
         <tli id="T306" time="219.86950000000002" type="appl" />
         <tli id="T307" time="220.92825" type="appl" />
         <tli id="T308" time="221.987" type="appl" />
         <tli id="T309" time="222.701" type="appl" />
         <tli id="T310" time="223.415" type="appl" />
         <tli id="T311" time="224.129" type="appl" />
         <tli id="T312" time="224.843" type="appl" />
         <tli id="T313" time="225.68365696000947" />
         <tli id="T314" time="226.459" type="appl" />
         <tli id="T315" time="227.361" type="appl" />
         <tli id="T316" time="228.26299999999998" type="appl" />
         <tli id="T317" time="229.165" type="appl" />
         <tli id="T318" time="229.58322222222222" type="appl" />
         <tli id="T319" time="230.00144444444445" type="appl" />
         <tli id="T320" time="230.41966666666667" type="appl" />
         <tli id="T321" time="230.8378888888889" type="appl" />
         <tli id="T322" time="231.2561111111111" type="appl" />
         <tli id="T323" time="231.67433333333332" type="appl" />
         <tli id="T324" time="232.09255555555555" type="appl" />
         <tli id="T325" time="232.51077777777778" type="appl" />
         <tli id="T326" time="232.929" type="appl" />
         <tli id="T327" time="233.62571428571428" type="appl" />
         <tli id="T328" time="234.3224285714286" type="appl" />
         <tli id="T329" time="235.01914285714287" type="appl" />
         <tli id="T330" time="235.71585714285715" type="appl" />
         <tli id="T331" time="236.41257142857143" type="appl" />
         <tli id="T332" time="237.10928571428573" type="appl" />
         <tli id="T333" time="239.87522331897821" />
         <tli id="T334" time="241.483" type="appl" />
         <tli id="T335" time="241.493" type="appl" />
         <tli id="T336" time="242.21075" type="appl" />
         <tli id="T337" time="242.92849999999999" type="appl" />
         <tli id="T338" time="243.64625" type="appl" />
         <tli id="T339" time="244.354" type="appl" />
         <tli id="T340" time="244.364" type="appl" />
         <tli id="T341" time="244.79633333333334" type="appl" />
         <tli id="T342" time="245.2386666666667" type="appl" />
         <tli id="T343" time="245.681" type="appl" />
         <tli id="T344" time="246.3814" type="appl" />
         <tli id="T345" time="247.08180000000002" type="appl" />
         <tli id="T346" time="247.7822" type="appl" />
         <tli id="T347" time="248.4826" type="appl" />
         <tli id="T348" time="250.08835327630894" />
         <tli id="T349" time="250.923" type="appl" />
         <tli id="T350" time="252.66299999999998" type="appl" />
         <tli id="T351" time="254.2830093306571" />
         <tli id="T352" time="255.05357142857142" type="appl" />
         <tli id="T353" time="255.70414285714284" type="appl" />
         <tli id="T354" time="256.3547142857143" type="appl" />
         <tli id="T355" time="257.0052857142857" type="appl" />
         <tli id="T356" time="257.65585714285714" type="appl" />
         <tli id="T357" time="258.30642857142857" type="appl" />
         <tli id="T358" time="259.04150832376007" />
         <tli id="T359" time="259.5175" type="appl" />
         <tli id="T360" time="260.078" type="appl" />
         <tli id="T361" time="260.63849999999996" type="appl" />
         <tli id="T362" time="261.19899999999996" type="appl" />
         <tli id="T363" time="261.7595" type="appl" />
         <tli id="T364" time="262.32" type="appl" />
         <tli id="T365" time="262.8805" type="appl" />
         <tli id="T366" time="264.2880705146343" />
         <tli id="T367" time="266.5746916473787" />
         <tli id="T368" time="266.590875" type="appl" />
         <tli id="T369" time="267.18575" type="appl" />
         <tli id="T370" time="267.780625" type="appl" />
         <tli id="T371" time="268.3755" type="appl" />
         <tli id="T372" time="268.970375" type="appl" />
         <tli id="T373" time="269.56525" type="appl" />
         <tli id="T374" time="270.160125" type="appl" />
         <tli id="T375" time="270.755" type="appl" />
         <tli id="T376" time="275.709" type="appl" />
         <tli id="T377" time="276.37625" type="appl" />
         <tli id="T378" time="277.0435" type="appl" />
         <tli id="T379" time="277.71074999999996" type="appl" />
         <tli id="T380" time="279.16110767834505" />
         <tli id="T381" time="279.70966666666664" type="appl" />
         <tli id="T382" time="281.04133333333334" type="appl" />
         <tli id="T383" time="282.81436826360436" />
         <tli id="T384" time="283.62666666666667" type="appl" />
         <tli id="T385" time="284.88033333333334" type="appl" />
         <tli id="T386" time="286.134" type="appl" />
         <tli id="T387" time="286.7436666666667" type="appl" />
         <tli id="T388" time="287.35333333333335" type="appl" />
         <tli id="T389" time="287.963" type="appl" />
         <tli id="T390" time="292.78083646609093" />
         <tli id="T391" time="292.81409097739396" type="intp" />
         <tli id="T392" time="292.847345488697" type="intp" />
         <tli id="T393" time="292.88059999999996" type="appl" />
         <tli id="T394" time="293.66179999999997" type="appl" />
         <tli id="T395" time="294.443" type="appl" />
         <tli id="T0" time="294.507" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KiPP"
                      id="tx-KiPP"
                      speaker="KiPP"
                      type="t">
         <timeline-fork end="T135" start="T134">
            <tli id="T134.tx-KiPP.1" />
         </timeline-fork>
         <timeline-fork end="T157" start="T156">
            <tli id="T156.tx-KiPP.1" />
         </timeline-fork>
         <timeline-fork end="T351" start="T350">
            <tli id="T350.tx-KiPP.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KiPP">
            <ts e="T78" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Büten</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">erer</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">bu͡olla</ts>
                  <nts id="Seg_13" n="HIAT:ip">.</nts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_16" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">Bihigi</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">baːrbɨt</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">turkarɨ</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">baːr</ts>
                  <nts id="Seg_28" n="HIAT:ip">,</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">anɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">bihigi</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">büttekpitine</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">emi͡e</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">atɨn</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">bu͡olu͡oktara</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">giniler</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_53" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">Bihigi</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">anɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">onuga</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">oloro</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">hatɨːbɨt</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_70" n="HIAT:w" s="T20">daːgɨnɨ</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_74" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">Anɨ</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">maladʼostarɨŋ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">isti͡ektere</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">bu͡o</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_89" n="HIAT:w" s="T25">atɨn</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_92" n="HIAT:w" s="T26">hirge</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_95" n="HIAT:w" s="T27">baran</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T28">iheller</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_102" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_104" n="HIAT:w" s="T29">Tuspa</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_107" n="HIAT:w" s="T30">bu͡olallar</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_111" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">Abɨčajdar</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_117" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">Tak</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">da</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">bihigi</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">ol</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">starajdar</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">abɨčajdarɨnan</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">oloror</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">etibit</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">munna</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_147" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">kelerin</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_155" n="HIAT:w" s="T43">tohujarga</ts>
                  <nts id="Seg_156" n="HIAT:ip">,</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">ɨraːktan</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">ɨ͡aldʼɨt</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">kelerger</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">ütü͡ö</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">asta</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_174" n="HIAT:w" s="T49">ahatɨ͡akka</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_178" n="HIAT:w" s="T50">üčügej</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_181" n="HIAT:w" s="T51">ahɨ</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_184" n="HIAT:w" s="T52">ahatɨ͡akka</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_187" n="HIAT:w" s="T53">di͡ečči</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_190" n="HIAT:w" s="T54">etibit</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_194" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_196" n="HIAT:w" s="T55">Onton</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_199" n="HIAT:w" s="T56">ɨ͡aldʼɨttar</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_202" n="HIAT:w" s="T57">kellekterine</ts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_206" n="HIAT:w" s="T58">aragiːnnan</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_209" n="HIAT:w" s="T59">iherdebit</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_213" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_215" n="HIAT:w" s="T60">Agɨjaktɨk</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_218" n="HIAT:w" s="T61">giniler</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_221" n="HIAT:w" s="T62">kördük</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_224" n="HIAT:w" s="T63">iti</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_227" n="HIAT:w" s="T64">ispeppit</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_230" n="HIAT:w" s="T65">bihigi</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_234" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_236" n="HIAT:w" s="T66">Üs</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_239" n="HIAT:w" s="T67">ürümkeni</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_242" n="HIAT:w" s="T68">iherdi͡eppit</ts>
                  <nts id="Seg_243" n="HIAT:ip">,</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_246" n="HIAT:w" s="T69">büterebit</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_249" n="HIAT:w" s="T70">bu͡o</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_253" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_255" n="HIAT:w" s="T71">Urukku</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_258" n="HIAT:w" s="T72">kihi</ts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_262" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_264" n="HIAT:w" s="T73">Itirikke</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_267" n="HIAT:w" s="T74">di͡eri</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_270" n="HIAT:w" s="T75">nʼelzʼa</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_273" n="HIAT:w" s="T76">tak</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_277" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_279" n="HIAT:w" s="T77">Kuhagan</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T97" id="Seg_282" n="sc" s="T92">
               <ts e="T97" id="Seg_284" n="HIAT:u" s="T92">
                  <nts id="Seg_285" n="HIAT:ip">–</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_288" n="HIAT:w" s="T92">Kün</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_291" n="HIAT:w" s="T93">aːjɨ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_294" n="HIAT:w" s="T95">iheller</ts>
                  <nts id="Seg_295" n="HIAT:ip">,</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_298" n="HIAT:w" s="T96">heː</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T150" id="Seg_301" n="sc" s="T100">
               <ts e="T113" id="Seg_303" n="HIAT:u" s="T100">
                  <nts id="Seg_304" n="HIAT:ip">–</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_307" n="HIAT:w" s="T100">Kün</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_310" n="HIAT:w" s="T102">aːjɨ</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_313" n="HIAT:w" s="T103">iheller</ts>
                  <nts id="Seg_314" n="HIAT:ip">,</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_317" n="HIAT:w" s="T104">atɨn</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_320" n="HIAT:w" s="T105">hirten</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_323" n="HIAT:w" s="T106">aragiːnɨ</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_326" n="HIAT:w" s="T107">egeline-egeline</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_329" n="HIAT:w" s="T108">bihi͡eke</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_332" n="HIAT:w" s="T109">hu͡ok</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_335" n="HIAT:w" s="T110">bu</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_338" n="HIAT:w" s="T111">dojduga</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_341" n="HIAT:w" s="T112">aragiː</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_345" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_347" n="HIAT:w" s="T113">Bihigi</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_350" n="HIAT:w" s="T114">kɨrdʼagastar</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_353" n="HIAT:w" s="T115">baːrbɨtɨna</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_356" n="HIAT:w" s="T116">aragiːnɨ</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_359" n="HIAT:w" s="T117">egelterbeppit</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_362" n="HIAT:w" s="T118">bu͡o</ts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_366" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_368" n="HIAT:w" s="T119">Ogolor</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_371" n="HIAT:w" s="T120">ajɨlɨː</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_374" n="HIAT:w" s="T121">ahaːbattar</ts>
                  <nts id="Seg_375" n="HIAT:ip">,</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_378" n="HIAT:w" s="T122">üčügej</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_381" n="HIAT:w" s="T123">džon</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_384" n="HIAT:w" s="T124">bu͡olu͡oktarɨn</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_387" n="HIAT:w" s="T125">kihi</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_390" n="HIAT:w" s="T126">bu͡olu͡oktarɨn</ts>
                  <nts id="Seg_391" n="HIAT:ip">.</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_394" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_396" n="HIAT:w" s="T127">Praːznʼikka</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_399" n="HIAT:w" s="T128">ere</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_402" n="HIAT:w" s="T129">egeltereːččibit</ts>
                  <nts id="Seg_403" n="HIAT:ip">.</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_406" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_408" n="HIAT:w" s="T130">Anɨ</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_411" n="HIAT:w" s="T131">iti</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_414" n="HIAT:w" s="T132">onton-mantan</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_417" n="HIAT:w" s="T133">egeleller</ts>
                  <nts id="Seg_418" n="HIAT:ip">,</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134.tx-KiPP.1" id="Seg_421" n="HIAT:w" s="T134">Ürüŋ</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_424" n="HIAT:w" s="T134.tx-KiPP.1">Kajattan</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_427" n="HIAT:w" s="T135">egeleller</ts>
                  <nts id="Seg_428" n="HIAT:ip">,</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_431" n="HIAT:w" s="T136">ɨrɨbnajtan</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_434" n="HIAT:w" s="T137">egeleller</ts>
                  <nts id="Seg_435" n="HIAT:ip">,</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_438" n="HIAT:w" s="T138">Nosku͡otan</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_441" n="HIAT:w" s="T139">egelen</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_444" n="HIAT:w" s="T140">keleller</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_448" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_450" n="HIAT:w" s="T141">Onuga</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_453" n="HIAT:w" s="T142">itireller</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_456" n="HIAT:w" s="T143">iti</ts>
                  <nts id="Seg_457" n="HIAT:ip">.</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_460" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_462" n="HIAT:w" s="T144">Bu</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_465" n="HIAT:w" s="T145">dojduga</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_468" n="HIAT:w" s="T146">aragiː</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_471" n="HIAT:w" s="T147">hu͡ok</ts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_475" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_477" n="HIAT:w" s="T148">Iherpeppit</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_480" n="HIAT:w" s="T149">aragini</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T173" id="Seg_483" n="sc" s="T156">
               <ts e="T158" id="Seg_485" n="HIAT:u" s="T156">
                  <nts id="Seg_486" n="HIAT:ip">–</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156.tx-KiPP.1" id="Seg_489" n="HIAT:w" s="T156">Vsʼo</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_492" n="HIAT:w" s="T156.tx-KiPP.1">ravno</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_495" n="HIAT:w" s="T157">barɨ͡aktara</ts>
                  <nts id="Seg_496" n="HIAT:ip">.</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_499" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_501" n="HIAT:w" s="T158">Nʼemnožko</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_504" n="HIAT:w" s="T159">koločuk</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_507" n="HIAT:w" s="T160">bu͡olu͡oktara</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_510" n="HIAT:w" s="T161">onnoːgor</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_513" n="HIAT:w" s="T162">smʼelajdɨk</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_516" n="HIAT:w" s="T163">barɨ͡aktara</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_520" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_522" n="HIAT:w" s="T164">Aragiː</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_525" n="HIAT:w" s="T165">hu͡ok</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_528" n="HIAT:w" s="T166">bu͡o</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_531" n="HIAT:w" s="T167">barɨ͡aktarɨn</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_535" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_537" n="HIAT:w" s="T168">Kata</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_540" n="HIAT:w" s="T169">bu͡o</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_543" n="HIAT:w" s="T170">kallaːmmɨt</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_546" n="HIAT:w" s="T171">kuhagan</ts>
                  <nts id="Seg_547" n="HIAT:ip">,</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_550" n="HIAT:w" s="T172">mʼenʼajdemmit</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T255" id="Seg_553" n="sc" s="T179">
               <ts e="T186" id="Seg_555" n="HIAT:u" s="T179">
                  <nts id="Seg_556" n="HIAT:ip">–</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_559" n="HIAT:w" s="T179">Aragiːnan</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_562" n="HIAT:w" s="T180">tabanɨ</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_565" n="HIAT:w" s="T181">ahappattar</ts>
                  <nts id="Seg_566" n="HIAT:ip">,</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_569" n="HIAT:w" s="T182">hirten</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_572" n="HIAT:w" s="T183">ere</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_575" n="HIAT:w" s="T184">ahɨːr</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_578" n="HIAT:w" s="T185">tabaŋ</ts>
                  <nts id="Seg_579" n="HIAT:ip">.</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_582" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_584" n="HIAT:w" s="T186">Hirten</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_587" n="HIAT:w" s="T187">ahɨːr</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_590" n="HIAT:w" s="T188">taba</ts>
                  <nts id="Seg_591" n="HIAT:ip">.</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_594" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_596" n="HIAT:w" s="T189">Hɨrsɨːlarga</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_599" n="HIAT:w" s="T190">bagas</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_602" n="HIAT:w" s="T191">üčügej</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_605" n="HIAT:w" s="T192">bu͡olu͡oktara</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_608" n="HIAT:w" s="T193">bu͡o</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_611" n="HIAT:w" s="T194">tak</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_614" n="HIAT:w" s="T195">da</ts>
                  <nts id="Seg_615" n="HIAT:ip">,</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_618" n="HIAT:w" s="T196">anɨ</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_621" n="HIAT:w" s="T197">tu͡ok</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_624" n="HIAT:w" s="T198">aragiːtɨn</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_627" n="HIAT:w" s="T199">bu͡olu͡oktaraj</ts>
                  <nts id="Seg_628" n="HIAT:ip">,</nts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_631" n="HIAT:w" s="T200">aragiːlara</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_634" n="HIAT:w" s="T201">bütü͡öge</ts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_638" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_640" n="HIAT:w" s="T202">Ölörtörbüt</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_643" n="HIAT:w" s="T203">kihiler</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_646" n="HIAT:w" s="T204">kɨ͡ajan</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_649" n="HIAT:w" s="T205">barɨ͡aktara</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_652" n="HIAT:w" s="T206">hu͡oga</ts>
                  <nts id="Seg_653" n="HIAT:ip">.</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_656" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_658" n="HIAT:w" s="T207">Kɨ͡ajan</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_661" n="HIAT:w" s="T208">oːnnʼu͡oktara</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_664" n="HIAT:w" s="T209">hu͡ok</ts>
                  <nts id="Seg_665" n="HIAT:ip">.</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_668" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_670" n="HIAT:w" s="T210">Tak</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_673" n="HIAT:w" s="T211">da</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_676" n="HIAT:w" s="T212">aŋar</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_679" n="HIAT:w" s="T213">kihiŋ</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_682" n="HIAT:w" s="T214">anɨ</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_684" n="HIAT:ip">(</nts>
                  <ts e="T216" id="Seg_686" n="HIAT:w" s="T215">is-</ts>
                  <nts id="Seg_687" n="HIAT:ip">)</nts>
                  <nts id="Seg_688" n="HIAT:ip">,</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_691" n="HIAT:w" s="T216">hɨrsar</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_694" n="HIAT:w" s="T217">kihileriŋ</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_697" n="HIAT:w" s="T218">ispetter</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_700" n="HIAT:w" s="T219">bu͡o</ts>
                  <nts id="Seg_701" n="HIAT:ip">.</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_704" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_706" n="HIAT:w" s="T220">Taba</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_709" n="HIAT:w" s="T221">kiːllesti͡ektere</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_712" n="HIAT:w" s="T222">anɨ</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_715" n="HIAT:w" s="T223">hɨrsɨːga</ts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_719" n="HIAT:w" s="T224">hɨrsar</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_722" n="HIAT:w" s="T225">taba</ts>
                  <nts id="Seg_723" n="HIAT:ip">.</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_726" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_728" n="HIAT:w" s="T226">Innʼe</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_731" n="HIAT:w" s="T227">egelenner</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_734" n="HIAT:w" s="T228">munna</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_737" n="HIAT:w" s="T229">tabalɨ͡aktara</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_740" n="HIAT:w" s="T230">ol</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_743" n="HIAT:w" s="T231">tabanɨ</ts>
                  <nts id="Seg_744" n="HIAT:ip">.</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_747" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_749" n="HIAT:w" s="T232">Ol</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_752" n="HIAT:w" s="T233">ke</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_755" n="HIAT:w" s="T234">bu</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_758" n="HIAT:w" s="T235">ɨttara</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_761" n="HIAT:w" s="T236">ügühe</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_764" n="HIAT:w" s="T237">bert</ts>
                  <nts id="Seg_765" n="HIAT:ip">,</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_768" n="HIAT:w" s="T238">bu</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_771" n="HIAT:w" s="T239">ɨttara</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_774" n="HIAT:w" s="T240">hatannarbattar</ts>
                  <nts id="Seg_775" n="HIAT:ip">,</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_778" n="HIAT:w" s="T241">kɨ͡ajan</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_781" n="HIAT:w" s="T242">tabalatɨ͡aktara</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_784" n="HIAT:w" s="T243">hu͡oga</ts>
                  <nts id="Seg_785" n="HIAT:ip">.</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_788" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_790" n="HIAT:w" s="T244">Oːl</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_793" n="HIAT:w" s="T245">Gubaːga</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_796" n="HIAT:w" s="T246">hɨrsaːččɨlar</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_799" n="HIAT:w" s="T247">iti</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_802" n="HIAT:w" s="T248">di͡ek</ts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_806" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_808" n="HIAT:w" s="T249">Baraːččɨlar</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_811" n="HIAT:w" s="T250">oːnnʼuː</ts>
                  <nts id="Seg_812" n="HIAT:ip">.</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_815" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_817" n="HIAT:w" s="T251">Anɨ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_820" n="HIAT:w" s="T252">onno</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_823" n="HIAT:w" s="T253">dʼi͡e</ts>
                  <nts id="Seg_824" n="HIAT:ip">…</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T286" id="Seg_826" n="sc" s="T257">
               <ts e="T265" id="Seg_828" n="HIAT:u" s="T257">
                  <nts id="Seg_829" n="HIAT:ip">–</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_832" n="HIAT:w" s="T257">Eː</ts>
                  <nts id="Seg_833" n="HIAT:ip">,</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_836" n="HIAT:w" s="T258">anɨ</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_839" n="HIAT:w" s="T259">dʼi͡e</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_842" n="HIAT:w" s="T260">tutu͡oktara</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_845" n="HIAT:w" s="T261">onno</ts>
                  <nts id="Seg_846" n="HIAT:ip">,</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_849" n="HIAT:w" s="T262">boloktarɨ</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_852" n="HIAT:w" s="T263">illeller</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_855" n="HIAT:w" s="T264">bügün</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_859" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_861" n="HIAT:w" s="T265">Mi͡eteleri</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_864" n="HIAT:w" s="T266">oŋottullar</ts>
                  <nts id="Seg_865" n="HIAT:ip">,</nts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_868" n="HIAT:w" s="T267">bügün</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_871" n="HIAT:w" s="T268">belemnener</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_874" n="HIAT:w" s="T269">künnere</ts>
                  <nts id="Seg_875" n="HIAT:ip">.</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_878" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_880" n="HIAT:w" s="T270">Sɨ͡ana</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_883" n="HIAT:w" s="T271">oŋorollor</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_886" n="HIAT:w" s="T272">onno</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_889" n="HIAT:w" s="T273">bu͡očkularɨnan</ts>
                  <nts id="Seg_890" n="HIAT:ip">.</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_893" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_895" n="HIAT:w" s="T274">Ol</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_898" n="HIAT:w" s="T275">dojduga</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_901" n="HIAT:w" s="T276">onno</ts>
                  <nts id="Seg_902" n="HIAT:ip">,</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_905" n="HIAT:w" s="T277">onno</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_908" n="HIAT:w" s="T278">ös</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_911" n="HIAT:w" s="T279">kepsi͡ektere</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_914" n="HIAT:w" s="T280">ɨllɨ͡aktara</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_917" n="HIAT:w" s="T281">daːgɨnɨ</ts>
                  <nts id="Seg_918" n="HIAT:ip">,</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_921" n="HIAT:w" s="T282">kim</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_924" n="HIAT:w" s="T283">ɨllɨːr</ts>
                  <nts id="Seg_925" n="HIAT:ip">.</nts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_928" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_930" n="HIAT:w" s="T284">Onnuktar</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_933" n="HIAT:w" s="T285">baːllar</ts>
                  <nts id="Seg_934" n="HIAT:ip">.</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T334" id="Seg_936" n="sc" s="T295">
               <ts e="T299" id="Seg_938" n="HIAT:u" s="T295">
                  <nts id="Seg_939" n="HIAT:ip">–</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_942" n="HIAT:w" s="T295">Ja</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_945" n="HIAT:w" s="T297">nʼe</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_948" n="HIAT:w" s="T298">pajedu</ts>
                  <nts id="Seg_949" n="HIAT:ip">.</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_952" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_954" n="HIAT:w" s="T299">Ustal</ts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_958" n="HIAT:w" s="T300">tak</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_961" n="HIAT:w" s="T301">da</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_964" n="HIAT:w" s="T302">pakojnʼikka</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_967" n="HIAT:w" s="T303">barɨ͡am</ts>
                  <nts id="Seg_968" n="HIAT:ip">.</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_971" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_973" n="HIAT:w" s="T304">Harsɨn</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_976" n="HIAT:w" s="T305">tahaːrɨ͡aktara</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_979" n="HIAT:w" s="T306">bu͡o</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_982" n="HIAT:w" s="T307">emeːksini</ts>
                  <nts id="Seg_983" n="HIAT:ip">.</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_986" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_988" n="HIAT:w" s="T308">Hanɨ</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_991" n="HIAT:w" s="T309">hataːn</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_994" n="HIAT:w" s="T310">hɨrsɨbatɨlar</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_997" n="HIAT:w" s="T311">ol</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1000" n="HIAT:w" s="T312">u͡olattarɨm</ts>
                  <nts id="Seg_1001" n="HIAT:ip">.</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1004" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1006" n="HIAT:w" s="T313">Pakojnʼikka</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1009" n="HIAT:w" s="T314">bardaktarɨna</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1012" n="HIAT:w" s="T315">aragiːlɨ͡aktara</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1015" n="HIAT:w" s="T316">bu͡o</ts>
                  <nts id="Seg_1016" n="HIAT:ip">.</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1019" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1021" n="HIAT:w" s="T317">Tak</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1024" n="HIAT:w" s="T318">da</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1027" n="HIAT:w" s="T319">elbegi</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1030" n="HIAT:w" s="T320">iherpetter</ts>
                  <nts id="Seg_1031" n="HIAT:ip">,</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1034" n="HIAT:w" s="T321">üs</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1037" n="HIAT:w" s="T322">ere</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1040" n="HIAT:w" s="T323">ürümkeni</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1043" n="HIAT:w" s="T324">iherdeːččibit</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1046" n="HIAT:w" s="T325">bihigi</ts>
                  <nts id="Seg_1047" n="HIAT:ip">.</nts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1050" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1052" n="HIAT:w" s="T326">Pakojnʼikka</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1055" n="HIAT:w" s="T327">elete</ts>
                  <nts id="Seg_1056" n="HIAT:ip">,</nts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1059" n="HIAT:w" s="T328">bolʼše</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1062" n="HIAT:w" s="T329">aragiː</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1065" n="HIAT:w" s="T330">bi͡erbetter</ts>
                  <nts id="Seg_1066" n="HIAT:ip">,</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1069" n="HIAT:w" s="T331">büteller</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1072" n="HIAT:w" s="T332">onon</ts>
                  <nts id="Seg_1073" n="HIAT:ip">.</nts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1076" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1078" n="HIAT:w" s="T333">Ataːrɨːga</ts>
                  <nts id="Seg_1079" n="HIAT:ip">.</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T376" id="Seg_1081" n="sc" s="T339">
               <ts e="T343" id="Seg_1083" n="HIAT:u" s="T339">
                  <nts id="Seg_1084" n="HIAT:ip">–</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1087" n="HIAT:w" s="T339">Ol</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1090" n="HIAT:w" s="T341">emeːksini</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1093" n="HIAT:w" s="T342">du͡o</ts>
                  <nts id="Seg_1094" n="HIAT:ip">?</nts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1097" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_1099" n="HIAT:w" s="T343">Bilebin</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1102" n="HIAT:w" s="T344">min</ts>
                  <nts id="Seg_1103" n="HIAT:ip">,</nts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1106" n="HIAT:w" s="T345">ogonnʼorum</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1109" n="HIAT:w" s="T346">ubajɨn</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1112" n="HIAT:w" s="T347">dʼaktara</ts>
                  <nts id="Seg_1113" n="HIAT:ip">.</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1116" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1118" n="HIAT:w" s="T348">Buraːtɨn</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1121" n="HIAT:w" s="T349">dʼaktara</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350.tx-KiPP.1" id="Seg_1125" n="HIAT:w" s="T350">dvojurodnaj</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1128" n="HIAT:w" s="T350.tx-KiPP.1">bɨraːtɨn</ts>
                  <nts id="Seg_1129" n="HIAT:ip">.</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1132" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1134" n="HIAT:w" s="T351">Bɨraːta</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1137" n="HIAT:w" s="T352">ete</ts>
                  <nts id="Seg_1138" n="HIAT:ip">,</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1141" n="HIAT:w" s="T353">bert</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1144" n="HIAT:w" s="T354">bilebin</ts>
                  <nts id="Seg_1145" n="HIAT:ip">,</nts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1148" n="HIAT:w" s="T355">biːrge</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1151" n="HIAT:w" s="T356">hɨldʼaːččɨ</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1154" n="HIAT:w" s="T357">etibit</ts>
                  <nts id="Seg_1155" n="HIAT:ip">.</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1158" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1160" n="HIAT:w" s="T358">Anɨ</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1163" n="HIAT:w" s="T359">bu͡o</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1166" n="HIAT:w" s="T360">kɨ͡ajan</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1169" n="HIAT:w" s="T361">barbappɨn</ts>
                  <nts id="Seg_1170" n="HIAT:ip">,</nts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1173" n="HIAT:w" s="T362">hɨtabɨn</ts>
                  <nts id="Seg_1174" n="HIAT:ip">,</nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1177" n="HIAT:w" s="T363">onno</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1180" n="HIAT:w" s="T364">hɨtɨ͡ak</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1183" n="HIAT:w" s="T365">etim</ts>
                  <nts id="Seg_1184" n="HIAT:ip">.</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1187" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1189" n="HIAT:w" s="T366">Gini͡eke</ts>
                  <nts id="Seg_1190" n="HIAT:ip">.</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1193" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1195" n="HIAT:w" s="T367">Ogom</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1198" n="HIAT:w" s="T368">itte</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1201" n="HIAT:w" s="T369">kelen</ts>
                  <nts id="Seg_1202" n="HIAT:ip">,</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1205" n="HIAT:w" s="T370">ogom</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1208" n="HIAT:w" s="T371">kelbetege</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1211" n="HIAT:w" s="T372">bu͡o</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1214" n="HIAT:w" s="T373">barɨ͡ak</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1217" n="HIAT:w" s="T374">etim</ts>
                  <nts id="Seg_1218" n="HIAT:ip">.</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1221" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1223" n="HIAT:w" s="T375">Ügüs</ts>
                  <nts id="Seg_1224" n="HIAT:ip">.</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T389" id="Seg_1226" n="sc" s="T380">
               <ts e="T383" id="Seg_1228" n="HIAT:u" s="T380">
                  <nts id="Seg_1229" n="HIAT:ip">–</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1232" n="HIAT:w" s="T380">Eː</ts>
                  <nts id="Seg_1233" n="HIAT:ip">,</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1236" n="HIAT:w" s="T381">heː</ts>
                  <nts id="Seg_1237" n="HIAT:ip">,</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1240" n="HIAT:w" s="T382">begehe</ts>
                  <nts id="Seg_1241" n="HIAT:ip">.</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1244" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1246" n="HIAT:w" s="T383">Bügün</ts>
                  <nts id="Seg_1247" n="HIAT:ip">,</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1250" n="HIAT:w" s="T384">harsɨn</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1253" n="HIAT:w" s="T385">taksar</ts>
                  <nts id="Seg_1254" n="HIAT:ip">.</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1257" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1259" n="HIAT:w" s="T386">Ühüs</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1262" n="HIAT:w" s="T387">künüger</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1265" n="HIAT:w" s="T388">taksaːččɨ</ts>
                  <nts id="Seg_1266" n="HIAT:ip">.</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T395" id="Seg_1268" n="sc" s="T390">
               <ts e="T395" id="Seg_1270" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1272" n="HIAT:w" s="T390">Dʼe</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1275" n="HIAT:w" s="T391">onton</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1278" n="HIAT:w" s="T392">elete</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1281" n="HIAT:w" s="T393">du͡o</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1284" n="HIAT:w" s="T394">ɨjɨtar</ts>
                  <nts id="Seg_1285" n="HIAT:ip">.</nts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KiPP">
            <ts e="T78" id="Seg_1287" n="sc" s="T1">
               <ts e="T2" id="Seg_1289" n="e" s="T1">– Büten </ts>
               <ts e="T3" id="Seg_1291" n="e" s="T2">erer </ts>
               <ts e="T4" id="Seg_1293" n="e" s="T3">bu͡olla. </ts>
               <ts e="T5" id="Seg_1295" n="e" s="T4">Bihigi </ts>
               <ts e="T6" id="Seg_1297" n="e" s="T5">baːrbɨt </ts>
               <ts e="T7" id="Seg_1299" n="e" s="T6">turkarɨ </ts>
               <ts e="T8" id="Seg_1301" n="e" s="T7">baːr, </ts>
               <ts e="T9" id="Seg_1303" n="e" s="T8">anɨ </ts>
               <ts e="T10" id="Seg_1305" n="e" s="T9">bihigi </ts>
               <ts e="T11" id="Seg_1307" n="e" s="T10">büttekpitine </ts>
               <ts e="T12" id="Seg_1309" n="e" s="T11">emi͡e </ts>
               <ts e="T13" id="Seg_1311" n="e" s="T12">atɨn </ts>
               <ts e="T14" id="Seg_1313" n="e" s="T13">bu͡olu͡oktara </ts>
               <ts e="T15" id="Seg_1315" n="e" s="T14">giniler. </ts>
               <ts e="T16" id="Seg_1317" n="e" s="T15">Bihigi </ts>
               <ts e="T17" id="Seg_1319" n="e" s="T16">anɨ </ts>
               <ts e="T18" id="Seg_1321" n="e" s="T17">onuga </ts>
               <ts e="T19" id="Seg_1323" n="e" s="T18">oloro </ts>
               <ts e="T20" id="Seg_1325" n="e" s="T19">hatɨːbɨt </ts>
               <ts e="T21" id="Seg_1327" n="e" s="T20">daːgɨnɨ. </ts>
               <ts e="T22" id="Seg_1329" n="e" s="T21">Anɨ </ts>
               <ts e="T23" id="Seg_1331" n="e" s="T22">maladʼostarɨŋ </ts>
               <ts e="T24" id="Seg_1333" n="e" s="T23">isti͡ektere </ts>
               <ts e="T25" id="Seg_1335" n="e" s="T24">bu͡o, </ts>
               <ts e="T26" id="Seg_1337" n="e" s="T25">atɨn </ts>
               <ts e="T27" id="Seg_1339" n="e" s="T26">hirge </ts>
               <ts e="T28" id="Seg_1341" n="e" s="T27">baran </ts>
               <ts e="T29" id="Seg_1343" n="e" s="T28">iheller. </ts>
               <ts e="T30" id="Seg_1345" n="e" s="T29">Tuspa </ts>
               <ts e="T31" id="Seg_1347" n="e" s="T30">bu͡olallar. </ts>
               <ts e="T32" id="Seg_1349" n="e" s="T31">Abɨčajdar. </ts>
               <ts e="T33" id="Seg_1351" n="e" s="T32">Tak </ts>
               <ts e="T34" id="Seg_1353" n="e" s="T33">da </ts>
               <ts e="T35" id="Seg_1355" n="e" s="T34">bihigi </ts>
               <ts e="T36" id="Seg_1357" n="e" s="T35">ol </ts>
               <ts e="T37" id="Seg_1359" n="e" s="T36">starajdar </ts>
               <ts e="T38" id="Seg_1361" n="e" s="T37">abɨčajdarɨnan </ts>
               <ts e="T39" id="Seg_1363" n="e" s="T38">oloror </ts>
               <ts e="T40" id="Seg_1365" n="e" s="T39">etibit </ts>
               <ts e="T41" id="Seg_1367" n="e" s="T40">munna. </ts>
               <ts e="T42" id="Seg_1369" n="e" s="T41">ɨ͡aldʼɨt </ts>
               <ts e="T43" id="Seg_1371" n="e" s="T42">kelerin </ts>
               <ts e="T44" id="Seg_1373" n="e" s="T43">tohujarga, </ts>
               <ts e="T45" id="Seg_1375" n="e" s="T44">ɨraːktan </ts>
               <ts e="T46" id="Seg_1377" n="e" s="T45">ɨ͡aldʼɨt </ts>
               <ts e="T47" id="Seg_1379" n="e" s="T46">kelerger </ts>
               <ts e="T48" id="Seg_1381" n="e" s="T47">ütü͡ö </ts>
               <ts e="T49" id="Seg_1383" n="e" s="T48">asta </ts>
               <ts e="T50" id="Seg_1385" n="e" s="T49">ahatɨ͡akka, </ts>
               <ts e="T51" id="Seg_1387" n="e" s="T50">üčügej </ts>
               <ts e="T52" id="Seg_1389" n="e" s="T51">ahɨ </ts>
               <ts e="T53" id="Seg_1391" n="e" s="T52">ahatɨ͡akka </ts>
               <ts e="T54" id="Seg_1393" n="e" s="T53">di͡ečči </ts>
               <ts e="T55" id="Seg_1395" n="e" s="T54">etibit. </ts>
               <ts e="T56" id="Seg_1397" n="e" s="T55">Onton </ts>
               <ts e="T57" id="Seg_1399" n="e" s="T56">ɨ͡aldʼɨttar </ts>
               <ts e="T58" id="Seg_1401" n="e" s="T57">kellekterine, </ts>
               <ts e="T59" id="Seg_1403" n="e" s="T58">aragiːnnan </ts>
               <ts e="T60" id="Seg_1405" n="e" s="T59">iherdebit. </ts>
               <ts e="T61" id="Seg_1407" n="e" s="T60">Agɨjaktɨk </ts>
               <ts e="T62" id="Seg_1409" n="e" s="T61">giniler </ts>
               <ts e="T63" id="Seg_1411" n="e" s="T62">kördük </ts>
               <ts e="T64" id="Seg_1413" n="e" s="T63">iti </ts>
               <ts e="T65" id="Seg_1415" n="e" s="T64">ispeppit </ts>
               <ts e="T66" id="Seg_1417" n="e" s="T65">bihigi. </ts>
               <ts e="T67" id="Seg_1419" n="e" s="T66">Üs </ts>
               <ts e="T68" id="Seg_1421" n="e" s="T67">ürümkeni </ts>
               <ts e="T69" id="Seg_1423" n="e" s="T68">iherdi͡eppit, </ts>
               <ts e="T70" id="Seg_1425" n="e" s="T69">büterebit </ts>
               <ts e="T71" id="Seg_1427" n="e" s="T70">bu͡o. </ts>
               <ts e="T72" id="Seg_1429" n="e" s="T71">Urukku </ts>
               <ts e="T73" id="Seg_1431" n="e" s="T72">kihi. </ts>
               <ts e="T74" id="Seg_1433" n="e" s="T73">Itirikke </ts>
               <ts e="T75" id="Seg_1435" n="e" s="T74">di͡eri </ts>
               <ts e="T76" id="Seg_1437" n="e" s="T75">nʼelzʼa </ts>
               <ts e="T77" id="Seg_1439" n="e" s="T76">tak. </ts>
               <ts e="T78" id="Seg_1441" n="e" s="T77">Kuhagan. </ts>
            </ts>
            <ts e="T97" id="Seg_1442" n="sc" s="T92">
               <ts e="T93" id="Seg_1444" n="e" s="T92">– Kün </ts>
               <ts e="T95" id="Seg_1446" n="e" s="T93">aːjɨ </ts>
               <ts e="T96" id="Seg_1448" n="e" s="T95">iheller, </ts>
               <ts e="T97" id="Seg_1450" n="e" s="T96">heː. </ts>
            </ts>
            <ts e="T150" id="Seg_1451" n="sc" s="T100">
               <ts e="T102" id="Seg_1453" n="e" s="T100">– Kün </ts>
               <ts e="T103" id="Seg_1455" n="e" s="T102">aːjɨ </ts>
               <ts e="T104" id="Seg_1457" n="e" s="T103">iheller, </ts>
               <ts e="T105" id="Seg_1459" n="e" s="T104">atɨn </ts>
               <ts e="T106" id="Seg_1461" n="e" s="T105">hirten </ts>
               <ts e="T107" id="Seg_1463" n="e" s="T106">aragiːnɨ </ts>
               <ts e="T108" id="Seg_1465" n="e" s="T107">egeline-egeline </ts>
               <ts e="T109" id="Seg_1467" n="e" s="T108">bihi͡eke </ts>
               <ts e="T110" id="Seg_1469" n="e" s="T109">hu͡ok </ts>
               <ts e="T111" id="Seg_1471" n="e" s="T110">bu </ts>
               <ts e="T112" id="Seg_1473" n="e" s="T111">dojduga </ts>
               <ts e="T113" id="Seg_1475" n="e" s="T112">aragiː. </ts>
               <ts e="T114" id="Seg_1477" n="e" s="T113">Bihigi </ts>
               <ts e="T115" id="Seg_1479" n="e" s="T114">kɨrdʼagastar </ts>
               <ts e="T116" id="Seg_1481" n="e" s="T115">baːrbɨtɨna </ts>
               <ts e="T117" id="Seg_1483" n="e" s="T116">aragiːnɨ </ts>
               <ts e="T118" id="Seg_1485" n="e" s="T117">egelterbeppit </ts>
               <ts e="T119" id="Seg_1487" n="e" s="T118">bu͡o. </ts>
               <ts e="T120" id="Seg_1489" n="e" s="T119">Ogolor </ts>
               <ts e="T121" id="Seg_1491" n="e" s="T120">ajɨlɨː </ts>
               <ts e="T122" id="Seg_1493" n="e" s="T121">ahaːbattar, </ts>
               <ts e="T123" id="Seg_1495" n="e" s="T122">üčügej </ts>
               <ts e="T124" id="Seg_1497" n="e" s="T123">džon </ts>
               <ts e="T125" id="Seg_1499" n="e" s="T124">bu͡olu͡oktarɨn </ts>
               <ts e="T126" id="Seg_1501" n="e" s="T125">kihi </ts>
               <ts e="T127" id="Seg_1503" n="e" s="T126">bu͡olu͡oktarɨn. </ts>
               <ts e="T128" id="Seg_1505" n="e" s="T127">Praːznʼikka </ts>
               <ts e="T129" id="Seg_1507" n="e" s="T128">ere </ts>
               <ts e="T130" id="Seg_1509" n="e" s="T129">egeltereːččibit. </ts>
               <ts e="T131" id="Seg_1511" n="e" s="T130">Anɨ </ts>
               <ts e="T132" id="Seg_1513" n="e" s="T131">iti </ts>
               <ts e="T133" id="Seg_1515" n="e" s="T132">onton-mantan </ts>
               <ts e="T134" id="Seg_1517" n="e" s="T133">egeleller, </ts>
               <ts e="T135" id="Seg_1519" n="e" s="T134">Ürüŋ Kajattan </ts>
               <ts e="T136" id="Seg_1521" n="e" s="T135">egeleller, </ts>
               <ts e="T137" id="Seg_1523" n="e" s="T136">ɨrɨbnajtan </ts>
               <ts e="T138" id="Seg_1525" n="e" s="T137">egeleller, </ts>
               <ts e="T139" id="Seg_1527" n="e" s="T138">Nosku͡otan </ts>
               <ts e="T140" id="Seg_1529" n="e" s="T139">egelen </ts>
               <ts e="T141" id="Seg_1531" n="e" s="T140">keleller. </ts>
               <ts e="T142" id="Seg_1533" n="e" s="T141">Onuga </ts>
               <ts e="T143" id="Seg_1535" n="e" s="T142">itireller </ts>
               <ts e="T144" id="Seg_1537" n="e" s="T143">iti. </ts>
               <ts e="T145" id="Seg_1539" n="e" s="T144">Bu </ts>
               <ts e="T146" id="Seg_1541" n="e" s="T145">dojduga </ts>
               <ts e="T147" id="Seg_1543" n="e" s="T146">aragiː </ts>
               <ts e="T148" id="Seg_1545" n="e" s="T147">hu͡ok. </ts>
               <ts e="T149" id="Seg_1547" n="e" s="T148">Iherpeppit </ts>
               <ts e="T150" id="Seg_1549" n="e" s="T149">aragini. </ts>
            </ts>
            <ts e="T173" id="Seg_1550" n="sc" s="T156">
               <ts e="T157" id="Seg_1552" n="e" s="T156">– Vsʼo ravno </ts>
               <ts e="T158" id="Seg_1554" n="e" s="T157">barɨ͡aktara. </ts>
               <ts e="T159" id="Seg_1556" n="e" s="T158">Nʼemnožko </ts>
               <ts e="T160" id="Seg_1558" n="e" s="T159">koločuk </ts>
               <ts e="T161" id="Seg_1560" n="e" s="T160">bu͡olu͡oktara </ts>
               <ts e="T162" id="Seg_1562" n="e" s="T161">onnoːgor </ts>
               <ts e="T163" id="Seg_1564" n="e" s="T162">smʼelajdɨk </ts>
               <ts e="T164" id="Seg_1566" n="e" s="T163">barɨ͡aktara. </ts>
               <ts e="T165" id="Seg_1568" n="e" s="T164">Aragiː </ts>
               <ts e="T166" id="Seg_1570" n="e" s="T165">hu͡ok </ts>
               <ts e="T167" id="Seg_1572" n="e" s="T166">bu͡o </ts>
               <ts e="T168" id="Seg_1574" n="e" s="T167">barɨ͡aktarɨn. </ts>
               <ts e="T169" id="Seg_1576" n="e" s="T168">Kata </ts>
               <ts e="T170" id="Seg_1578" n="e" s="T169">bu͡o </ts>
               <ts e="T171" id="Seg_1580" n="e" s="T170">kallaːmmɨt </ts>
               <ts e="T172" id="Seg_1582" n="e" s="T171">kuhagan, </ts>
               <ts e="T173" id="Seg_1584" n="e" s="T172">mʼenʼajdemmit. </ts>
            </ts>
            <ts e="T255" id="Seg_1585" n="sc" s="T179">
               <ts e="T180" id="Seg_1587" n="e" s="T179">– Aragiːnan </ts>
               <ts e="T181" id="Seg_1589" n="e" s="T180">tabanɨ </ts>
               <ts e="T182" id="Seg_1591" n="e" s="T181">ahappattar, </ts>
               <ts e="T183" id="Seg_1593" n="e" s="T182">hirten </ts>
               <ts e="T184" id="Seg_1595" n="e" s="T183">ere </ts>
               <ts e="T185" id="Seg_1597" n="e" s="T184">ahɨːr </ts>
               <ts e="T186" id="Seg_1599" n="e" s="T185">tabaŋ. </ts>
               <ts e="T187" id="Seg_1601" n="e" s="T186">Hirten </ts>
               <ts e="T188" id="Seg_1603" n="e" s="T187">ahɨːr </ts>
               <ts e="T189" id="Seg_1605" n="e" s="T188">taba. </ts>
               <ts e="T190" id="Seg_1607" n="e" s="T189">Hɨrsɨːlarga </ts>
               <ts e="T191" id="Seg_1609" n="e" s="T190">bagas </ts>
               <ts e="T192" id="Seg_1611" n="e" s="T191">üčügej </ts>
               <ts e="T193" id="Seg_1613" n="e" s="T192">bu͡olu͡oktara </ts>
               <ts e="T194" id="Seg_1615" n="e" s="T193">bu͡o </ts>
               <ts e="T195" id="Seg_1617" n="e" s="T194">tak </ts>
               <ts e="T196" id="Seg_1619" n="e" s="T195">da, </ts>
               <ts e="T197" id="Seg_1621" n="e" s="T196">anɨ </ts>
               <ts e="T198" id="Seg_1623" n="e" s="T197">tu͡ok </ts>
               <ts e="T199" id="Seg_1625" n="e" s="T198">aragiːtɨn </ts>
               <ts e="T200" id="Seg_1627" n="e" s="T199">bu͡olu͡oktaraj, </ts>
               <ts e="T201" id="Seg_1629" n="e" s="T200">aragiːlara </ts>
               <ts e="T202" id="Seg_1631" n="e" s="T201">bütü͡öge. </ts>
               <ts e="T203" id="Seg_1633" n="e" s="T202">Ölörtörbüt </ts>
               <ts e="T204" id="Seg_1635" n="e" s="T203">kihiler </ts>
               <ts e="T205" id="Seg_1637" n="e" s="T204">kɨ͡ajan </ts>
               <ts e="T206" id="Seg_1639" n="e" s="T205">barɨ͡aktara </ts>
               <ts e="T207" id="Seg_1641" n="e" s="T206">hu͡oga. </ts>
               <ts e="T208" id="Seg_1643" n="e" s="T207">Kɨ͡ajan </ts>
               <ts e="T209" id="Seg_1645" n="e" s="T208">oːnnʼu͡oktara </ts>
               <ts e="T210" id="Seg_1647" n="e" s="T209">hu͡ok. </ts>
               <ts e="T211" id="Seg_1649" n="e" s="T210">Tak </ts>
               <ts e="T212" id="Seg_1651" n="e" s="T211">da </ts>
               <ts e="T213" id="Seg_1653" n="e" s="T212">aŋar </ts>
               <ts e="T214" id="Seg_1655" n="e" s="T213">kihiŋ </ts>
               <ts e="T215" id="Seg_1657" n="e" s="T214">anɨ </ts>
               <ts e="T216" id="Seg_1659" n="e" s="T215">(is-), </ts>
               <ts e="T217" id="Seg_1661" n="e" s="T216">hɨrsar </ts>
               <ts e="T218" id="Seg_1663" n="e" s="T217">kihileriŋ </ts>
               <ts e="T219" id="Seg_1665" n="e" s="T218">ispetter </ts>
               <ts e="T220" id="Seg_1667" n="e" s="T219">bu͡o. </ts>
               <ts e="T221" id="Seg_1669" n="e" s="T220">Taba </ts>
               <ts e="T222" id="Seg_1671" n="e" s="T221">kiːllesti͡ektere </ts>
               <ts e="T223" id="Seg_1673" n="e" s="T222">anɨ </ts>
               <ts e="T224" id="Seg_1675" n="e" s="T223">hɨrsɨːga, </ts>
               <ts e="T225" id="Seg_1677" n="e" s="T224">hɨrsar </ts>
               <ts e="T226" id="Seg_1679" n="e" s="T225">taba. </ts>
               <ts e="T227" id="Seg_1681" n="e" s="T226">Innʼe </ts>
               <ts e="T228" id="Seg_1683" n="e" s="T227">egelenner </ts>
               <ts e="T229" id="Seg_1685" n="e" s="T228">munna </ts>
               <ts e="T230" id="Seg_1687" n="e" s="T229">tabalɨ͡aktara </ts>
               <ts e="T231" id="Seg_1689" n="e" s="T230">ol </ts>
               <ts e="T232" id="Seg_1691" n="e" s="T231">tabanɨ. </ts>
               <ts e="T233" id="Seg_1693" n="e" s="T232">Ol </ts>
               <ts e="T234" id="Seg_1695" n="e" s="T233">ke </ts>
               <ts e="T235" id="Seg_1697" n="e" s="T234">bu </ts>
               <ts e="T236" id="Seg_1699" n="e" s="T235">ɨttara </ts>
               <ts e="T237" id="Seg_1701" n="e" s="T236">ügühe </ts>
               <ts e="T238" id="Seg_1703" n="e" s="T237">bert, </ts>
               <ts e="T239" id="Seg_1705" n="e" s="T238">bu </ts>
               <ts e="T240" id="Seg_1707" n="e" s="T239">ɨttara </ts>
               <ts e="T241" id="Seg_1709" n="e" s="T240">hatannarbattar, </ts>
               <ts e="T242" id="Seg_1711" n="e" s="T241">kɨ͡ajan </ts>
               <ts e="T243" id="Seg_1713" n="e" s="T242">tabalatɨ͡aktara </ts>
               <ts e="T244" id="Seg_1715" n="e" s="T243">hu͡oga. </ts>
               <ts e="T245" id="Seg_1717" n="e" s="T244">Oːl </ts>
               <ts e="T246" id="Seg_1719" n="e" s="T245">Gubaːga </ts>
               <ts e="T247" id="Seg_1721" n="e" s="T246">hɨrsaːččɨlar </ts>
               <ts e="T248" id="Seg_1723" n="e" s="T247">iti </ts>
               <ts e="T249" id="Seg_1725" n="e" s="T248">di͡ek. </ts>
               <ts e="T250" id="Seg_1727" n="e" s="T249">Baraːččɨlar </ts>
               <ts e="T251" id="Seg_1729" n="e" s="T250">oːnnʼuː. </ts>
               <ts e="T252" id="Seg_1731" n="e" s="T251">Anɨ </ts>
               <ts e="T253" id="Seg_1733" n="e" s="T252">onno </ts>
               <ts e="T255" id="Seg_1735" n="e" s="T253">dʼi͡e… </ts>
            </ts>
            <ts e="T286" id="Seg_1736" n="sc" s="T257">
               <ts e="T258" id="Seg_1738" n="e" s="T257">– Eː, </ts>
               <ts e="T259" id="Seg_1740" n="e" s="T258">anɨ </ts>
               <ts e="T260" id="Seg_1742" n="e" s="T259">dʼi͡e </ts>
               <ts e="T261" id="Seg_1744" n="e" s="T260">tutu͡oktara </ts>
               <ts e="T262" id="Seg_1746" n="e" s="T261">onno, </ts>
               <ts e="T263" id="Seg_1748" n="e" s="T262">boloktarɨ </ts>
               <ts e="T264" id="Seg_1750" n="e" s="T263">illeller </ts>
               <ts e="T265" id="Seg_1752" n="e" s="T264">bügün. </ts>
               <ts e="T266" id="Seg_1754" n="e" s="T265">Mi͡eteleri </ts>
               <ts e="T267" id="Seg_1756" n="e" s="T266">oŋottullar, </ts>
               <ts e="T268" id="Seg_1758" n="e" s="T267">bügün </ts>
               <ts e="T269" id="Seg_1760" n="e" s="T268">belemnener </ts>
               <ts e="T270" id="Seg_1762" n="e" s="T269">künnere. </ts>
               <ts e="T271" id="Seg_1764" n="e" s="T270">Sɨ͡ana </ts>
               <ts e="T272" id="Seg_1766" n="e" s="T271">oŋorollor </ts>
               <ts e="T273" id="Seg_1768" n="e" s="T272">onno </ts>
               <ts e="T274" id="Seg_1770" n="e" s="T273">bu͡očkularɨnan. </ts>
               <ts e="T275" id="Seg_1772" n="e" s="T274">Ol </ts>
               <ts e="T276" id="Seg_1774" n="e" s="T275">dojduga </ts>
               <ts e="T277" id="Seg_1776" n="e" s="T276">onno, </ts>
               <ts e="T278" id="Seg_1778" n="e" s="T277">onno </ts>
               <ts e="T279" id="Seg_1780" n="e" s="T278">ös </ts>
               <ts e="T280" id="Seg_1782" n="e" s="T279">kepsi͡ektere </ts>
               <ts e="T281" id="Seg_1784" n="e" s="T280">ɨllɨ͡aktara </ts>
               <ts e="T282" id="Seg_1786" n="e" s="T281">daːgɨnɨ, </ts>
               <ts e="T283" id="Seg_1788" n="e" s="T282">kim </ts>
               <ts e="T284" id="Seg_1790" n="e" s="T283">ɨllɨːr. </ts>
               <ts e="T285" id="Seg_1792" n="e" s="T284">Onnuktar </ts>
               <ts e="T286" id="Seg_1794" n="e" s="T285">baːllar. </ts>
            </ts>
            <ts e="T334" id="Seg_1795" n="sc" s="T295">
               <ts e="T297" id="Seg_1797" n="e" s="T295">– Ja </ts>
               <ts e="T298" id="Seg_1799" n="e" s="T297">nʼe </ts>
               <ts e="T299" id="Seg_1801" n="e" s="T298">pajedu. </ts>
               <ts e="T300" id="Seg_1803" n="e" s="T299">Ustal, </ts>
               <ts e="T301" id="Seg_1805" n="e" s="T300">tak </ts>
               <ts e="T302" id="Seg_1807" n="e" s="T301">da </ts>
               <ts e="T303" id="Seg_1809" n="e" s="T302">pakojnʼikka </ts>
               <ts e="T304" id="Seg_1811" n="e" s="T303">barɨ͡am. </ts>
               <ts e="T305" id="Seg_1813" n="e" s="T304">Harsɨn </ts>
               <ts e="T306" id="Seg_1815" n="e" s="T305">tahaːrɨ͡aktara </ts>
               <ts e="T307" id="Seg_1817" n="e" s="T306">bu͡o </ts>
               <ts e="T308" id="Seg_1819" n="e" s="T307">emeːksini. </ts>
               <ts e="T309" id="Seg_1821" n="e" s="T308">Hanɨ </ts>
               <ts e="T310" id="Seg_1823" n="e" s="T309">hataːn </ts>
               <ts e="T311" id="Seg_1825" n="e" s="T310">hɨrsɨbatɨlar </ts>
               <ts e="T312" id="Seg_1827" n="e" s="T311">ol </ts>
               <ts e="T313" id="Seg_1829" n="e" s="T312">u͡olattarɨm. </ts>
               <ts e="T314" id="Seg_1831" n="e" s="T313">Pakojnʼikka </ts>
               <ts e="T315" id="Seg_1833" n="e" s="T314">bardaktarɨna </ts>
               <ts e="T316" id="Seg_1835" n="e" s="T315">aragiːlɨ͡aktara </ts>
               <ts e="T317" id="Seg_1837" n="e" s="T316">bu͡o. </ts>
               <ts e="T318" id="Seg_1839" n="e" s="T317">Tak </ts>
               <ts e="T319" id="Seg_1841" n="e" s="T318">da </ts>
               <ts e="T320" id="Seg_1843" n="e" s="T319">elbegi </ts>
               <ts e="T321" id="Seg_1845" n="e" s="T320">iherpetter, </ts>
               <ts e="T322" id="Seg_1847" n="e" s="T321">üs </ts>
               <ts e="T323" id="Seg_1849" n="e" s="T322">ere </ts>
               <ts e="T324" id="Seg_1851" n="e" s="T323">ürümkeni </ts>
               <ts e="T325" id="Seg_1853" n="e" s="T324">iherdeːččibit </ts>
               <ts e="T326" id="Seg_1855" n="e" s="T325">bihigi. </ts>
               <ts e="T327" id="Seg_1857" n="e" s="T326">Pakojnʼikka </ts>
               <ts e="T328" id="Seg_1859" n="e" s="T327">elete, </ts>
               <ts e="T329" id="Seg_1861" n="e" s="T328">bolʼše </ts>
               <ts e="T330" id="Seg_1863" n="e" s="T329">aragiː </ts>
               <ts e="T331" id="Seg_1865" n="e" s="T330">bi͡erbetter, </ts>
               <ts e="T332" id="Seg_1867" n="e" s="T331">büteller </ts>
               <ts e="T333" id="Seg_1869" n="e" s="T332">onon. </ts>
               <ts e="T334" id="Seg_1871" n="e" s="T333">Ataːrɨːga. </ts>
            </ts>
            <ts e="T376" id="Seg_1872" n="sc" s="T339">
               <ts e="T341" id="Seg_1874" n="e" s="T339">– Ol </ts>
               <ts e="T342" id="Seg_1876" n="e" s="T341">emeːksini </ts>
               <ts e="T343" id="Seg_1878" n="e" s="T342">du͡o? </ts>
               <ts e="T344" id="Seg_1880" n="e" s="T343">Bilebin </ts>
               <ts e="T345" id="Seg_1882" n="e" s="T344">min, </ts>
               <ts e="T346" id="Seg_1884" n="e" s="T345">ogonnʼorum </ts>
               <ts e="T347" id="Seg_1886" n="e" s="T346">ubajɨn </ts>
               <ts e="T348" id="Seg_1888" n="e" s="T347">dʼaktara. </ts>
               <ts e="T349" id="Seg_1890" n="e" s="T348">Buraːtɨn </ts>
               <ts e="T350" id="Seg_1892" n="e" s="T349">dʼaktara, </ts>
               <ts e="T351" id="Seg_1894" n="e" s="T350">dvojurodnaj bɨraːtɨn. </ts>
               <ts e="T352" id="Seg_1896" n="e" s="T351">Bɨraːta </ts>
               <ts e="T353" id="Seg_1898" n="e" s="T352">ete, </ts>
               <ts e="T354" id="Seg_1900" n="e" s="T353">bert </ts>
               <ts e="T355" id="Seg_1902" n="e" s="T354">bilebin, </ts>
               <ts e="T356" id="Seg_1904" n="e" s="T355">biːrge </ts>
               <ts e="T357" id="Seg_1906" n="e" s="T356">hɨldʼaːččɨ </ts>
               <ts e="T358" id="Seg_1908" n="e" s="T357">etibit. </ts>
               <ts e="T359" id="Seg_1910" n="e" s="T358">Anɨ </ts>
               <ts e="T360" id="Seg_1912" n="e" s="T359">bu͡o </ts>
               <ts e="T361" id="Seg_1914" n="e" s="T360">kɨ͡ajan </ts>
               <ts e="T362" id="Seg_1916" n="e" s="T361">barbappɨn, </ts>
               <ts e="T363" id="Seg_1918" n="e" s="T362">hɨtabɨn, </ts>
               <ts e="T364" id="Seg_1920" n="e" s="T363">onno </ts>
               <ts e="T365" id="Seg_1922" n="e" s="T364">hɨtɨ͡ak </ts>
               <ts e="T366" id="Seg_1924" n="e" s="T365">etim. </ts>
               <ts e="T367" id="Seg_1926" n="e" s="T366">Gini͡eke. </ts>
               <ts e="T368" id="Seg_1928" n="e" s="T367">Ogom </ts>
               <ts e="T369" id="Seg_1930" n="e" s="T368">itte </ts>
               <ts e="T370" id="Seg_1932" n="e" s="T369">kelen, </ts>
               <ts e="T371" id="Seg_1934" n="e" s="T370">ogom </ts>
               <ts e="T372" id="Seg_1936" n="e" s="T371">kelbetege </ts>
               <ts e="T373" id="Seg_1938" n="e" s="T372">bu͡o </ts>
               <ts e="T374" id="Seg_1940" n="e" s="T373">barɨ͡ak </ts>
               <ts e="T375" id="Seg_1942" n="e" s="T374">etim. </ts>
               <ts e="T376" id="Seg_1944" n="e" s="T375">Ügüs. </ts>
            </ts>
            <ts e="T389" id="Seg_1945" n="sc" s="T380">
               <ts e="T381" id="Seg_1947" n="e" s="T380">– Eː, </ts>
               <ts e="T382" id="Seg_1949" n="e" s="T381">heː, </ts>
               <ts e="T383" id="Seg_1951" n="e" s="T382">begehe. </ts>
               <ts e="T384" id="Seg_1953" n="e" s="T383">Bügün, </ts>
               <ts e="T385" id="Seg_1955" n="e" s="T384">harsɨn </ts>
               <ts e="T386" id="Seg_1957" n="e" s="T385">taksar. </ts>
               <ts e="T387" id="Seg_1959" n="e" s="T386">Ühüs </ts>
               <ts e="T388" id="Seg_1961" n="e" s="T387">künüger </ts>
               <ts e="T389" id="Seg_1963" n="e" s="T388">taksaːččɨ. </ts>
            </ts>
            <ts e="T395" id="Seg_1964" n="sc" s="T390">
               <ts e="T391" id="Seg_1966" n="e" s="T390">Dʼe </ts>
               <ts e="T392" id="Seg_1968" n="e" s="T391">onton </ts>
               <ts e="T393" id="Seg_1970" n="e" s="T392">elete </ts>
               <ts e="T394" id="Seg_1972" n="e" s="T393">du͡o </ts>
               <ts e="T395" id="Seg_1974" n="e" s="T394">ɨjɨtar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KiPP">
            <ta e="T4" id="Seg_1975" s="T1">KiPP_2009_Story_nar.KiPP.001 (001.001)</ta>
            <ta e="T15" id="Seg_1976" s="T4">KiPP_2009_Story_nar.KiPP.002 (001.002)</ta>
            <ta e="T21" id="Seg_1977" s="T15">KiPP_2009_Story_nar.KiPP.003 (001.003)</ta>
            <ta e="T29" id="Seg_1978" s="T21">KiPP_2009_Story_nar.KiPP.004 (001.004)</ta>
            <ta e="T31" id="Seg_1979" s="T29">KiPP_2009_Story_nar.KiPP.005 (001.005)</ta>
            <ta e="T32" id="Seg_1980" s="T31">KiPP_2009_Story_nar.KiPP.006 (001.006)</ta>
            <ta e="T41" id="Seg_1981" s="T32">KiPP_2009_Story_nar.KiPP.007 (001.007)</ta>
            <ta e="T55" id="Seg_1982" s="T41">KiPP_2009_Story_nar.KiPP.008 (001.008)</ta>
            <ta e="T60" id="Seg_1983" s="T55">KiPP_2009_Story_nar.KiPP.009 (001.009)</ta>
            <ta e="T66" id="Seg_1984" s="T60">KiPP_2009_Story_nar.KiPP.010 (001.010)</ta>
            <ta e="T71" id="Seg_1985" s="T66">KiPP_2009_Story_nar.KiPP.011 (001.011)</ta>
            <ta e="T73" id="Seg_1986" s="T71">KiPP_2009_Story_nar.KiPP.012 (001.012)</ta>
            <ta e="T77" id="Seg_1987" s="T73">KiPP_2009_Story_nar.KiPP.013 (001.013)</ta>
            <ta e="T78" id="Seg_1988" s="T77">KiPP_2009_Story_nar.KiPP.014 (001.014)</ta>
            <ta e="T97" id="Seg_1989" s="T92">KiPP_2009_Story_nar.KiPP.015 (001.016)</ta>
            <ta e="T113" id="Seg_1990" s="T100">KiPP_2009_Story_nar.KiPP.016 (001.018)</ta>
            <ta e="T119" id="Seg_1991" s="T113">KiPP_2009_Story_nar.KiPP.017 (001.019)</ta>
            <ta e="T127" id="Seg_1992" s="T119">KiPP_2009_Story_nar.KiPP.018 (001.020)</ta>
            <ta e="T130" id="Seg_1993" s="T127">KiPP_2009_Story_nar.KiPP.019 (001.021)</ta>
            <ta e="T141" id="Seg_1994" s="T130">KiPP_2009_Story_nar.KiPP.020 (001.022)</ta>
            <ta e="T144" id="Seg_1995" s="T141">KiPP_2009_Story_nar.KiPP.021 (001.023)</ta>
            <ta e="T148" id="Seg_1996" s="T144">KiPP_2009_Story_nar.KiPP.022 (001.024)</ta>
            <ta e="T150" id="Seg_1997" s="T148">KiPP_2009_Story_nar.KiPP.023 (001.025)</ta>
            <ta e="T158" id="Seg_1998" s="T156">KiPP_2009_Story_nar.KiPP.024 (001.027)</ta>
            <ta e="T164" id="Seg_1999" s="T158">KiPP_2009_Story_nar.KiPP.025 (001.028)</ta>
            <ta e="T168" id="Seg_2000" s="T164">KiPP_2009_Story_nar.KiPP.026 (001.029)</ta>
            <ta e="T173" id="Seg_2001" s="T168">KiPP_2009_Story_nar.KiPP.027 (001.030)</ta>
            <ta e="T186" id="Seg_2002" s="T179">KiPP_2009_Story_nar.KiPP.028 (001.032)</ta>
            <ta e="T189" id="Seg_2003" s="T186">KiPP_2009_Story_nar.KiPP.029 (001.033)</ta>
            <ta e="T202" id="Seg_2004" s="T189">KiPP_2009_Story_nar.KiPP.030 (001.034)</ta>
            <ta e="T207" id="Seg_2005" s="T202">KiPP_2009_Story_nar.KiPP.031 (001.035)</ta>
            <ta e="T210" id="Seg_2006" s="T207">KiPP_2009_Story_nar.KiPP.032 (001.036)</ta>
            <ta e="T220" id="Seg_2007" s="T210">KiPP_2009_Story_nar.KiPP.033 (001.037)</ta>
            <ta e="T226" id="Seg_2008" s="T220">KiPP_2009_Story_nar.KiPP.034 (001.038)</ta>
            <ta e="T232" id="Seg_2009" s="T226">KiPP_2009_Story_nar.KiPP.035 (001.039)</ta>
            <ta e="T244" id="Seg_2010" s="T232">KiPP_2009_Story_nar.KiPP.036 (001.040)</ta>
            <ta e="T249" id="Seg_2011" s="T244">KiPP_2009_Story_nar.KiPP.037 (001.041)</ta>
            <ta e="T251" id="Seg_2012" s="T249">KiPP_2009_Story_nar.KiPP.038 (001.042)</ta>
            <ta e="T255" id="Seg_2013" s="T251">KiPP_2009_Story_nar.KiPP.039 (001.043)</ta>
            <ta e="T265" id="Seg_2014" s="T257">KiPP_2009_Story_nar.KiPP.040 (001.045)</ta>
            <ta e="T270" id="Seg_2015" s="T265">KiPP_2009_Story_nar.KiPP.041 (001.046)</ta>
            <ta e="T274" id="Seg_2016" s="T270">KiPP_2009_Story_nar.KiPP.042 (001.047)</ta>
            <ta e="T284" id="Seg_2017" s="T274">KiPP_2009_Story_nar.KiPP.043 (001.048)</ta>
            <ta e="T286" id="Seg_2018" s="T284">KiPP_2009_Story_nar.KiPP.044 (001.049)</ta>
            <ta e="T299" id="Seg_2019" s="T295">KiPP_2009_Story_nar.KiPP.045 (001.051)</ta>
            <ta e="T304" id="Seg_2020" s="T299">KiPP_2009_Story_nar.KiPP.046 (001.052)</ta>
            <ta e="T308" id="Seg_2021" s="T304">KiPP_2009_Story_nar.KiPP.047 (001.053)</ta>
            <ta e="T313" id="Seg_2022" s="T308">KiPP_2009_Story_nar.KiPP.048 (001.054)</ta>
            <ta e="T317" id="Seg_2023" s="T313">KiPP_2009_Story_nar.KiPP.049 (001.055)</ta>
            <ta e="T326" id="Seg_2024" s="T317">KiPP_2009_Story_nar.KiPP.050 (001.056)</ta>
            <ta e="T333" id="Seg_2025" s="T326">KiPP_2009_Story_nar.KiPP.051 (001.057)</ta>
            <ta e="T334" id="Seg_2026" s="T333">KiPP_2009_Story_nar.KiPP.052 (001.058)</ta>
            <ta e="T343" id="Seg_2027" s="T339">KiPP_2009_Story_nar.KiPP.053 (001.060)</ta>
            <ta e="T348" id="Seg_2028" s="T343">KiPP_2009_Story_nar.KiPP.054 (001.061)</ta>
            <ta e="T351" id="Seg_2029" s="T348">KiPP_2009_Story_nar.KiPP.055 (001.062)</ta>
            <ta e="T358" id="Seg_2030" s="T351">KiPP_2009_Story_nar.KiPP.056 (001.063)</ta>
            <ta e="T366" id="Seg_2031" s="T358">KiPP_2009_Story_nar.KiPP.057 (001.064)</ta>
            <ta e="T367" id="Seg_2032" s="T366">KiPP_2009_Story_nar.KiPP.058 (001.065)</ta>
            <ta e="T375" id="Seg_2033" s="T367">KiPP_2009_Story_nar.KiPP.059 (001.066)</ta>
            <ta e="T376" id="Seg_2034" s="T375">KiPP_2009_Story_nar.KiPP.060 (001.067)</ta>
            <ta e="T383" id="Seg_2035" s="T380">KiPP_2009_Story_nar.KiPP.061 (001.069)</ta>
            <ta e="T386" id="Seg_2036" s="T383">KiPP_2009_Story_nar.KiPP.062 (001.070)</ta>
            <ta e="T389" id="Seg_2037" s="T386">KiPP_2009_Story_nar.KiPP.063 (001.071)</ta>
            <ta e="T395" id="Seg_2038" s="T390">KiPP_2009_Story_nar.KiPP.064 (001.072)</ta>
         </annotation>
         <annotation name="st" tierref="st-KiPP" />
         <annotation name="ts" tierref="ts-KiPP">
            <ta e="T4" id="Seg_2039" s="T1">– Büten erer bu͡olla. </ta>
            <ta e="T15" id="Seg_2040" s="T4">Bihigi baːrbɨt turkarɨ baːr, anɨ bihigi büttekpitine emi͡e atɨn bu͡olu͡oktara giniler. </ta>
            <ta e="T21" id="Seg_2041" s="T15">Bihigi anɨ onuga oloro hatɨːbɨt daːgɨnɨ. </ta>
            <ta e="T29" id="Seg_2042" s="T21">Anɨ maladʼostarɨŋ isti͡ektere bu͡o, atɨn hirge baran iheller. </ta>
            <ta e="T31" id="Seg_2043" s="T29">Tuspa bu͡olallar. </ta>
            <ta e="T32" id="Seg_2044" s="T31">Abɨčajdar. </ta>
            <ta e="T41" id="Seg_2045" s="T32">Tak da bihigi ol starajdar abɨčajdarɨnan oloror etibit munna. </ta>
            <ta e="T55" id="Seg_2046" s="T41">ɨ͡aldʼɨt kelerin tohujarga, ɨraːktan ɨ͡aldʼɨt kelerger ütü͡ö asta ahatɨ͡akka, üčügej ahɨ ahatɨ͡akka di͡ečči etibit. </ta>
            <ta e="T60" id="Seg_2047" s="T55">Onton ɨ͡aldʼɨttar kellekterine, aragiːnnan iherdebit. </ta>
            <ta e="T66" id="Seg_2048" s="T60">Agɨjaktɨk giniler kördük iti ispeppit bihigi. </ta>
            <ta e="T71" id="Seg_2049" s="T66">Üs ürümkeni iherdi͡eppit, büterebit bu͡o. </ta>
            <ta e="T73" id="Seg_2050" s="T71">Urukku kihi. </ta>
            <ta e="T77" id="Seg_2051" s="T73">Itirikke di͡eri nʼelzʼa tak. </ta>
            <ta e="T78" id="Seg_2052" s="T77">Kuhagan. </ta>
            <ta e="T97" id="Seg_2053" s="T92">– Kün aːjɨ iheller, heː. </ta>
            <ta e="T113" id="Seg_2054" s="T100">– Kün aːjɨ iheller, atɨn hirten aragiːnɨ egeline-egeline bihi͡eke hu͡ok bu dojduga aragiː. </ta>
            <ta e="T119" id="Seg_2055" s="T113">Bihigi kɨrdʼagastar baːrbɨtɨna aragiːnɨ egelterbeppit bu͡o. </ta>
            <ta e="T127" id="Seg_2056" s="T119">Ogolor ajɨlɨː ahaːbattar, üčügej džon bu͡olu͡oktarɨn kihi bu͡olu͡oktarɨn. </ta>
            <ta e="T130" id="Seg_2057" s="T127">Praːznʼikka ere egeltereːččibit. </ta>
            <ta e="T141" id="Seg_2058" s="T130">Anɨ iti onton-mantan egeleller, Ürüŋ Kajattan egeleller, ɨrɨbnajtan egeleller, Nosku͡otan egelen keleller. </ta>
            <ta e="T144" id="Seg_2059" s="T141">Onuga itireller iti. </ta>
            <ta e="T148" id="Seg_2060" s="T144">Bu dojduga aragiː hu͡ok. </ta>
            <ta e="T150" id="Seg_2061" s="T148">Iherpeppit aragini. </ta>
            <ta e="T158" id="Seg_2062" s="T156">– Vsʼo ravno barɨ͡aktara. </ta>
            <ta e="T164" id="Seg_2063" s="T158">Nʼemnožko koločuk bu͡olu͡oktara onnoːgor smʼelajdɨk barɨ͡aktara. </ta>
            <ta e="T168" id="Seg_2064" s="T164">Aragiː hu͡ok bu͡o barɨ͡aktarɨn. </ta>
            <ta e="T173" id="Seg_2065" s="T168">Kata bu͡o kallaːmmɨt kuhagan, mʼenʼajdemmit. </ta>
            <ta e="T186" id="Seg_2066" s="T179">– Aragiːnan tabanɨ ahappattar, hirten ere ahɨːr tabaŋ. </ta>
            <ta e="T189" id="Seg_2067" s="T186">Hirten ahɨːr taba. </ta>
            <ta e="T202" id="Seg_2068" s="T189">Hɨrsɨːlarga bagas üčügej bu͡olu͡oktara bu͡o tak da, anɨ tu͡ok aragiːtɨn bu͡olu͡oktaraj, aragiːlara bütü͡öge. </ta>
            <ta e="T207" id="Seg_2069" s="T202">Ölörtörbüt kihiler kɨ͡ajan barɨ͡aktara hu͡oga. </ta>
            <ta e="T210" id="Seg_2070" s="T207">Kɨ͡ajan oːnnʼu͡oktara hu͡ok. </ta>
            <ta e="T220" id="Seg_2071" s="T210">Tak da aŋar kihiŋ anɨ (is-), hɨrsar kihileriŋ ispetter bu͡o. </ta>
            <ta e="T226" id="Seg_2072" s="T220">Taba kiːllesti͡ektere anɨ hɨrsɨːga, hɨrsar taba. </ta>
            <ta e="T232" id="Seg_2073" s="T226">Innʼe egelenner munna tabalɨ͡aktara ol tabanɨ. </ta>
            <ta e="T244" id="Seg_2074" s="T232">Ol ke bu ɨttara ügühe bert, bu ɨttara hatannarbattar, kɨ͡ajan tabalatɨ͡aktara hu͡oga. </ta>
            <ta e="T249" id="Seg_2075" s="T244">Oːl Gubaːga hɨrsaːččɨlar iti di͡ek. </ta>
            <ta e="T251" id="Seg_2076" s="T249">Baraːččɨlar oːnnʼuː. </ta>
            <ta e="T255" id="Seg_2077" s="T251">Anɨ onno dʼi͡e… </ta>
            <ta e="T265" id="Seg_2078" s="T257">– Eː, anɨ dʼi͡e tutu͡oktara onno, boloktarɨ illeller bügün. </ta>
            <ta e="T270" id="Seg_2079" s="T265">Mi͡eteleri oŋottullar, bügün belemnener künnere. </ta>
            <ta e="T274" id="Seg_2080" s="T270">Sɨ͡ana oŋorollor onno bu͡očkularɨnan. </ta>
            <ta e="T284" id="Seg_2081" s="T274">Ol dojduga onno, onno ös kepsi͡ektere ɨllɨ͡aktara daːgɨnɨ, kim ɨllɨːr. </ta>
            <ta e="T286" id="Seg_2082" s="T284">Onnuktar baːllar. </ta>
            <ta e="T299" id="Seg_2083" s="T295">– Ja nʼe pajedu. </ta>
            <ta e="T304" id="Seg_2084" s="T299">Ustal, tak da pakojnʼikka barɨ͡am. </ta>
            <ta e="T308" id="Seg_2085" s="T304">Harsɨn tahaːrɨ͡aktara bu͡o emeːksini. </ta>
            <ta e="T313" id="Seg_2086" s="T308">Hanɨ hataːn hɨrsɨbatɨlar ol u͡olattarɨm. </ta>
            <ta e="T317" id="Seg_2087" s="T313">Pakojnʼikka bardaktarɨna aragiːlɨ͡aktara bu͡o. </ta>
            <ta e="T326" id="Seg_2088" s="T317">Tak da elbegi iherpetter, üs ere ürümkeni iherdeːččibit bihigi. </ta>
            <ta e="T333" id="Seg_2089" s="T326">Pakojnʼikka elete, bolʼše aragiː bi͡erbetter, büteller onon. </ta>
            <ta e="T334" id="Seg_2090" s="T333">Ataːrɨːga. </ta>
            <ta e="T343" id="Seg_2091" s="T339">– Ol emeːksini du͡o? </ta>
            <ta e="T348" id="Seg_2092" s="T343">Bilebin min, ogonnʼorum ubajɨn dʼaktara. </ta>
            <ta e="T351" id="Seg_2093" s="T348">Buraːtɨn dʼaktara, dvojurodnaj bɨraːtɨn. </ta>
            <ta e="T358" id="Seg_2094" s="T351">Bɨraːta ete, bert bilebin, biːrge hɨldʼaːččɨ etibit. </ta>
            <ta e="T366" id="Seg_2095" s="T358">Anɨ bu͡o kɨ͡ajan barbappɨn, hɨtabɨn, onno hɨtɨ͡ak etim. </ta>
            <ta e="T367" id="Seg_2096" s="T366">Gini͡eke. </ta>
            <ta e="T375" id="Seg_2097" s="T367">Ogom itte kelen, ogom kelbetege bu͡o barɨ͡ak etim. </ta>
            <ta e="T376" id="Seg_2098" s="T375">Ügüs. </ta>
            <ta e="T383" id="Seg_2099" s="T380">– Eː, heː, begehe. </ta>
            <ta e="T386" id="Seg_2100" s="T383">Bügün, harsɨn taksar. </ta>
            <ta e="T389" id="Seg_2101" s="T386">Ühüs künüger taksaːččɨ. </ta>
            <ta e="T395" id="Seg_2102" s="T390">Dʼe onton elete du͡o ɨjɨtar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KiPP">
            <ta e="T2" id="Seg_2103" s="T1">büt-en</ta>
            <ta e="T3" id="Seg_2104" s="T2">er-er</ta>
            <ta e="T4" id="Seg_2105" s="T3">bu͡olla</ta>
            <ta e="T5" id="Seg_2106" s="T4">bihigi</ta>
            <ta e="T6" id="Seg_2107" s="T5">baːr-bɨt</ta>
            <ta e="T7" id="Seg_2108" s="T6">turkarɨ</ta>
            <ta e="T8" id="Seg_2109" s="T7">baːr</ta>
            <ta e="T9" id="Seg_2110" s="T8">anɨ</ta>
            <ta e="T10" id="Seg_2111" s="T9">bihigi</ta>
            <ta e="T11" id="Seg_2112" s="T10">büt-tek-pitine</ta>
            <ta e="T12" id="Seg_2113" s="T11">emi͡e</ta>
            <ta e="T13" id="Seg_2114" s="T12">atɨn</ta>
            <ta e="T14" id="Seg_2115" s="T13">bu͡ol-u͡ok-tara</ta>
            <ta e="T15" id="Seg_2116" s="T14">giniler</ta>
            <ta e="T16" id="Seg_2117" s="T15">bihigi</ta>
            <ta e="T17" id="Seg_2118" s="T16">anɨ</ta>
            <ta e="T18" id="Seg_2119" s="T17">onu-ga</ta>
            <ta e="T19" id="Seg_2120" s="T18">olor-o</ta>
            <ta e="T20" id="Seg_2121" s="T19">hat-ɨː-bɨt</ta>
            <ta e="T21" id="Seg_2122" s="T20">daːgɨnɨ</ta>
            <ta e="T22" id="Seg_2123" s="T21">anɨ</ta>
            <ta e="T23" id="Seg_2124" s="T22">maladʼos-tar-ɨ-ŋ</ta>
            <ta e="T24" id="Seg_2125" s="T23">ist-i͡ek-tere</ta>
            <ta e="T25" id="Seg_2126" s="T24">bu͡o</ta>
            <ta e="T26" id="Seg_2127" s="T25">atɨn</ta>
            <ta e="T27" id="Seg_2128" s="T26">hir-ge</ta>
            <ta e="T28" id="Seg_2129" s="T27">bar-an</ta>
            <ta e="T29" id="Seg_2130" s="T28">ih-el-ler</ta>
            <ta e="T30" id="Seg_2131" s="T29">tuspa</ta>
            <ta e="T31" id="Seg_2132" s="T30">bu͡ol-al-lar</ta>
            <ta e="T32" id="Seg_2133" s="T31">abɨčaj-dar</ta>
            <ta e="T33" id="Seg_2134" s="T32">tak</ta>
            <ta e="T34" id="Seg_2135" s="T33">da</ta>
            <ta e="T35" id="Seg_2136" s="T34">bihigi</ta>
            <ta e="T36" id="Seg_2137" s="T35">ol</ta>
            <ta e="T37" id="Seg_2138" s="T36">staraj-dar</ta>
            <ta e="T38" id="Seg_2139" s="T37">abɨčaj-darɨ-nan</ta>
            <ta e="T39" id="Seg_2140" s="T38">olor-or</ta>
            <ta e="T40" id="Seg_2141" s="T39">e-ti-bit</ta>
            <ta e="T41" id="Seg_2142" s="T40">munna</ta>
            <ta e="T42" id="Seg_2143" s="T41">ɨ͡aldʼɨt</ta>
            <ta e="T43" id="Seg_2144" s="T42">kel-er-i-n</ta>
            <ta e="T44" id="Seg_2145" s="T43">tohuj-ar-ga</ta>
            <ta e="T45" id="Seg_2146" s="T44">ɨraːk-tan</ta>
            <ta e="T46" id="Seg_2147" s="T45">ɨ͡aldʼɨt</ta>
            <ta e="T47" id="Seg_2148" s="T46">kel-er-ger</ta>
            <ta e="T48" id="Seg_2149" s="T47">ütü͡ö</ta>
            <ta e="T49" id="Seg_2150" s="T48">as-ta</ta>
            <ta e="T50" id="Seg_2151" s="T49">ah-a-t-ɨ͡ak-ka</ta>
            <ta e="T51" id="Seg_2152" s="T50">üčügej</ta>
            <ta e="T52" id="Seg_2153" s="T51">ah-ɨ</ta>
            <ta e="T53" id="Seg_2154" s="T52">ah-a-t-ɨ͡ak-ka</ta>
            <ta e="T54" id="Seg_2155" s="T53">di͡e-čči</ta>
            <ta e="T55" id="Seg_2156" s="T54">e-ti-bit</ta>
            <ta e="T56" id="Seg_2157" s="T55">onton</ta>
            <ta e="T57" id="Seg_2158" s="T56">ɨ͡aldʼɨt-tar</ta>
            <ta e="T58" id="Seg_2159" s="T57">kel-lek-terine</ta>
            <ta e="T59" id="Seg_2160" s="T58">aragiː-nnan</ta>
            <ta e="T60" id="Seg_2161" s="T59">ih-e-r-d-e-bit</ta>
            <ta e="T61" id="Seg_2162" s="T60">agɨjak-tɨk</ta>
            <ta e="T62" id="Seg_2163" s="T61">giniler</ta>
            <ta e="T63" id="Seg_2164" s="T62">kördük</ta>
            <ta e="T64" id="Seg_2165" s="T63">iti</ta>
            <ta e="T65" id="Seg_2166" s="T64">is-pep-pit</ta>
            <ta e="T66" id="Seg_2167" s="T65">bihigi</ta>
            <ta e="T67" id="Seg_2168" s="T66">üs</ta>
            <ta e="T68" id="Seg_2169" s="T67">ürümke-ni</ta>
            <ta e="T69" id="Seg_2170" s="T68">ih-e-r-d-i͡ep-pit</ta>
            <ta e="T70" id="Seg_2171" s="T69">büt-e-r-e-bit</ta>
            <ta e="T71" id="Seg_2172" s="T70">bu͡o</ta>
            <ta e="T72" id="Seg_2173" s="T71">urukku</ta>
            <ta e="T73" id="Seg_2174" s="T72">kihi</ta>
            <ta e="T74" id="Seg_2175" s="T73">itirik-ke</ta>
            <ta e="T75" id="Seg_2176" s="T74">di͡eri</ta>
            <ta e="T78" id="Seg_2177" s="T77">kuhagan</ta>
            <ta e="T93" id="Seg_2178" s="T92">kün</ta>
            <ta e="T95" id="Seg_2179" s="T93">aːjɨ</ta>
            <ta e="T96" id="Seg_2180" s="T95">ih-el-ler</ta>
            <ta e="T97" id="Seg_2181" s="T96">heː</ta>
            <ta e="T102" id="Seg_2182" s="T100">kün</ta>
            <ta e="T103" id="Seg_2183" s="T102">aːjɨ</ta>
            <ta e="T104" id="Seg_2184" s="T103">ih-el-ler</ta>
            <ta e="T105" id="Seg_2185" s="T104">atɨn</ta>
            <ta e="T106" id="Seg_2186" s="T105">hir-ten</ta>
            <ta e="T107" id="Seg_2187" s="T106">aragiː-nɨ</ta>
            <ta e="T108" id="Seg_2188" s="T107">egel-i-n-e-egel-i-n-e</ta>
            <ta e="T109" id="Seg_2189" s="T108">bihi͡e-ke</ta>
            <ta e="T110" id="Seg_2190" s="T109">hu͡ok</ta>
            <ta e="T111" id="Seg_2191" s="T110">bu</ta>
            <ta e="T112" id="Seg_2192" s="T111">dojdu-ga</ta>
            <ta e="T113" id="Seg_2193" s="T112">aragiː</ta>
            <ta e="T114" id="Seg_2194" s="T113">bihigi</ta>
            <ta e="T115" id="Seg_2195" s="T114">kɨrdʼagas-tar</ta>
            <ta e="T116" id="Seg_2196" s="T115">baːr-bɨtɨna</ta>
            <ta e="T117" id="Seg_2197" s="T116">aragiː-nɨ</ta>
            <ta e="T118" id="Seg_2198" s="T117">egel-ter-bep-pit</ta>
            <ta e="T119" id="Seg_2199" s="T118">bu͡o</ta>
            <ta e="T120" id="Seg_2200" s="T119">ogo-lor</ta>
            <ta e="T121" id="Seg_2201" s="T120">ajɨ-lɨː</ta>
            <ta e="T122" id="Seg_2202" s="T121">ahaː-bat-tar</ta>
            <ta e="T123" id="Seg_2203" s="T122">üčügej</ta>
            <ta e="T124" id="Seg_2204" s="T123">džon</ta>
            <ta e="T125" id="Seg_2205" s="T124">bu͡ol-u͡ok-tarɨ-n</ta>
            <ta e="T126" id="Seg_2206" s="T125">kihi</ta>
            <ta e="T127" id="Seg_2207" s="T126">bu͡ol-u͡ok-tarɨ-n</ta>
            <ta e="T128" id="Seg_2208" s="T127">praːznʼik-ka</ta>
            <ta e="T129" id="Seg_2209" s="T128">ere</ta>
            <ta e="T130" id="Seg_2210" s="T129">egel-ter-eːčči-bit</ta>
            <ta e="T131" id="Seg_2211" s="T130">anɨ</ta>
            <ta e="T132" id="Seg_2212" s="T131">iti</ta>
            <ta e="T133" id="Seg_2213" s="T132">onton-mantan</ta>
            <ta e="T134" id="Seg_2214" s="T133">egel-el-ler</ta>
            <ta e="T135" id="Seg_2215" s="T134">Ürüŋ Kaja-ttan</ta>
            <ta e="T136" id="Seg_2216" s="T135">egel-el-ler</ta>
            <ta e="T137" id="Seg_2217" s="T136">ɨrɨbnaj-tan</ta>
            <ta e="T138" id="Seg_2218" s="T137">egel-el-ler</ta>
            <ta e="T139" id="Seg_2219" s="T138">Nosku͡o-tan</ta>
            <ta e="T140" id="Seg_2220" s="T139">egel-en</ta>
            <ta e="T141" id="Seg_2221" s="T140">kel-el-ler</ta>
            <ta e="T142" id="Seg_2222" s="T141">onu-ga</ta>
            <ta e="T143" id="Seg_2223" s="T142">itir-el-ler</ta>
            <ta e="T144" id="Seg_2224" s="T143">iti</ta>
            <ta e="T145" id="Seg_2225" s="T144">bu</ta>
            <ta e="T146" id="Seg_2226" s="T145">dojdu-ga</ta>
            <ta e="T147" id="Seg_2227" s="T146">aragiː</ta>
            <ta e="T148" id="Seg_2228" s="T147">hu͡ok</ta>
            <ta e="T149" id="Seg_2229" s="T148">ih-e-r-pep-pit</ta>
            <ta e="T150" id="Seg_2230" s="T149">aragi-ni</ta>
            <ta e="T157" id="Seg_2231" s="T156">vsʼo ravno</ta>
            <ta e="T158" id="Seg_2232" s="T157">bar-ɨ͡ak-tara</ta>
            <ta e="T159" id="Seg_2233" s="T158">nʼemnožko</ta>
            <ta e="T160" id="Seg_2234" s="T159">koločuk</ta>
            <ta e="T161" id="Seg_2235" s="T160">bu͡ol-u͡ok-tara</ta>
            <ta e="T162" id="Seg_2236" s="T161">onnoːgor</ta>
            <ta e="T163" id="Seg_2237" s="T162">smʼelaj-dɨk</ta>
            <ta e="T164" id="Seg_2238" s="T163">bar-ɨ͡ak-tara</ta>
            <ta e="T165" id="Seg_2239" s="T164">aragiː</ta>
            <ta e="T166" id="Seg_2240" s="T165">hu͡ok</ta>
            <ta e="T167" id="Seg_2241" s="T166">bu͡o</ta>
            <ta e="T168" id="Seg_2242" s="T167">bar-ɨ͡ak-tarɨ-n</ta>
            <ta e="T169" id="Seg_2243" s="T168">kata</ta>
            <ta e="T170" id="Seg_2244" s="T169">bu͡o</ta>
            <ta e="T171" id="Seg_2245" s="T170">kallaːm-mɨt</ta>
            <ta e="T172" id="Seg_2246" s="T171">kuhagan</ta>
            <ta e="T173" id="Seg_2247" s="T172">mʼenʼaj-dem-mit</ta>
            <ta e="T180" id="Seg_2248" s="T179">aragiː-nan</ta>
            <ta e="T181" id="Seg_2249" s="T180">taba-nɨ</ta>
            <ta e="T182" id="Seg_2250" s="T181">ah-a-p-pat-tar</ta>
            <ta e="T183" id="Seg_2251" s="T182">hir-ten</ta>
            <ta e="T184" id="Seg_2252" s="T183">ere</ta>
            <ta e="T185" id="Seg_2253" s="T184">ahɨː-r</ta>
            <ta e="T186" id="Seg_2254" s="T185">taba-ŋ</ta>
            <ta e="T187" id="Seg_2255" s="T186">hir-ten</ta>
            <ta e="T188" id="Seg_2256" s="T187">ahɨː-r</ta>
            <ta e="T189" id="Seg_2257" s="T188">taba</ta>
            <ta e="T190" id="Seg_2258" s="T189">hɨrs-ɨː-lar-ga</ta>
            <ta e="T191" id="Seg_2259" s="T190">bagas</ta>
            <ta e="T192" id="Seg_2260" s="T191">üčügej</ta>
            <ta e="T193" id="Seg_2261" s="T192">bu͡ol-u͡ok-tara</ta>
            <ta e="T194" id="Seg_2262" s="T193">bu͡o</ta>
            <ta e="T195" id="Seg_2263" s="T194">tak</ta>
            <ta e="T196" id="Seg_2264" s="T195">da</ta>
            <ta e="T197" id="Seg_2265" s="T196">anɨ</ta>
            <ta e="T198" id="Seg_2266" s="T197">tu͡ok</ta>
            <ta e="T199" id="Seg_2267" s="T198">aragiː-tɨ-n</ta>
            <ta e="T200" id="Seg_2268" s="T199">bu͡ol-u͡ok-tara=j</ta>
            <ta e="T201" id="Seg_2269" s="T200">aragiː-lara</ta>
            <ta e="T202" id="Seg_2270" s="T201">büt-ü͡ög-e</ta>
            <ta e="T203" id="Seg_2271" s="T202">ölör-tör-büt</ta>
            <ta e="T204" id="Seg_2272" s="T203">kihi-ler</ta>
            <ta e="T205" id="Seg_2273" s="T204">kɨ͡aj-an</ta>
            <ta e="T206" id="Seg_2274" s="T205">bar-ɨ͡ak-tara</ta>
            <ta e="T207" id="Seg_2275" s="T206">hu͡og-a</ta>
            <ta e="T208" id="Seg_2276" s="T207">kɨ͡aj-an</ta>
            <ta e="T209" id="Seg_2277" s="T208">oːnnʼ-u͡ok-tara</ta>
            <ta e="T210" id="Seg_2278" s="T209">hu͡ok</ta>
            <ta e="T211" id="Seg_2279" s="T210">tak</ta>
            <ta e="T212" id="Seg_2280" s="T211">da</ta>
            <ta e="T213" id="Seg_2281" s="T212">aŋar</ta>
            <ta e="T214" id="Seg_2282" s="T213">kihi-ŋ</ta>
            <ta e="T215" id="Seg_2283" s="T214">anɨ</ta>
            <ta e="T216" id="Seg_2284" s="T215">is</ta>
            <ta e="T217" id="Seg_2285" s="T216">hɨrs-ar</ta>
            <ta e="T218" id="Seg_2286" s="T217">kihi-ler-i-ŋ</ta>
            <ta e="T219" id="Seg_2287" s="T218">is-pet-ter</ta>
            <ta e="T220" id="Seg_2288" s="T219">bu͡o</ta>
            <ta e="T221" id="Seg_2289" s="T220">taba</ta>
            <ta e="T222" id="Seg_2290" s="T221">kiːl-l-e-s-t-i͡ek-tere</ta>
            <ta e="T223" id="Seg_2291" s="T222">anɨ</ta>
            <ta e="T224" id="Seg_2292" s="T223">hɨrs-ɨː-ga</ta>
            <ta e="T225" id="Seg_2293" s="T224">hɨrs-ar</ta>
            <ta e="T226" id="Seg_2294" s="T225">taba</ta>
            <ta e="T227" id="Seg_2295" s="T226">innʼe</ta>
            <ta e="T228" id="Seg_2296" s="T227">egel-en-ner</ta>
            <ta e="T229" id="Seg_2297" s="T228">munna</ta>
            <ta e="T230" id="Seg_2298" s="T229">taba-l-ɨ͡ak-tara</ta>
            <ta e="T231" id="Seg_2299" s="T230">ol</ta>
            <ta e="T232" id="Seg_2300" s="T231">taba-nɨ</ta>
            <ta e="T233" id="Seg_2301" s="T232">ol</ta>
            <ta e="T234" id="Seg_2302" s="T233">ke</ta>
            <ta e="T235" id="Seg_2303" s="T234">bu</ta>
            <ta e="T236" id="Seg_2304" s="T235">ɨt-tara</ta>
            <ta e="T237" id="Seg_2305" s="T236">ügüh-e</ta>
            <ta e="T238" id="Seg_2306" s="T237">bert</ta>
            <ta e="T239" id="Seg_2307" s="T238">bu</ta>
            <ta e="T240" id="Seg_2308" s="T239">ɨt-tara</ta>
            <ta e="T241" id="Seg_2309" s="T240">hatan-nar-bat-tar</ta>
            <ta e="T242" id="Seg_2310" s="T241">kɨ͡aj-an</ta>
            <ta e="T243" id="Seg_2311" s="T242">taba-l-a-t-ɨ͡ak-tara</ta>
            <ta e="T244" id="Seg_2312" s="T243">hu͡og-a</ta>
            <ta e="T245" id="Seg_2313" s="T244">oːl</ta>
            <ta e="T246" id="Seg_2314" s="T245">Gubaː-ga</ta>
            <ta e="T247" id="Seg_2315" s="T246">hɨrs-aːččɨ-lar</ta>
            <ta e="T248" id="Seg_2316" s="T247">iti</ta>
            <ta e="T249" id="Seg_2317" s="T248">di͡ek</ta>
            <ta e="T250" id="Seg_2318" s="T249">bar-aːččɨ-lar</ta>
            <ta e="T251" id="Seg_2319" s="T250">oːnnʼ-uː</ta>
            <ta e="T252" id="Seg_2320" s="T251">anɨ</ta>
            <ta e="T253" id="Seg_2321" s="T252">onno</ta>
            <ta e="T255" id="Seg_2322" s="T253">dʼi͡e</ta>
            <ta e="T258" id="Seg_2323" s="T257">eː</ta>
            <ta e="T259" id="Seg_2324" s="T258">anɨ</ta>
            <ta e="T260" id="Seg_2325" s="T259">dʼi͡e</ta>
            <ta e="T261" id="Seg_2326" s="T260">tut-u͡ok-tara</ta>
            <ta e="T262" id="Seg_2327" s="T261">onno</ta>
            <ta e="T263" id="Seg_2328" s="T262">bolok-tar-ɨ</ta>
            <ta e="T264" id="Seg_2329" s="T263">ill-el-ler</ta>
            <ta e="T265" id="Seg_2330" s="T264">bügün</ta>
            <ta e="T266" id="Seg_2331" s="T265">mi͡ete-ler-i</ta>
            <ta e="T267" id="Seg_2332" s="T266">oŋott-u-l-lar</ta>
            <ta e="T268" id="Seg_2333" s="T267">bügün</ta>
            <ta e="T269" id="Seg_2334" s="T268">belemn-e-n-er</ta>
            <ta e="T270" id="Seg_2335" s="T269">kün-nere</ta>
            <ta e="T271" id="Seg_2336" s="T270">sɨ͡ana</ta>
            <ta e="T272" id="Seg_2337" s="T271">oŋor-ol-lor</ta>
            <ta e="T273" id="Seg_2338" s="T272">onno</ta>
            <ta e="T274" id="Seg_2339" s="T273">bu͡očku-lar-ɨ-nan</ta>
            <ta e="T275" id="Seg_2340" s="T274">ol</ta>
            <ta e="T276" id="Seg_2341" s="T275">dojdu-ga</ta>
            <ta e="T277" id="Seg_2342" s="T276">onno</ta>
            <ta e="T278" id="Seg_2343" s="T277">onno</ta>
            <ta e="T279" id="Seg_2344" s="T278">ös</ta>
            <ta e="T280" id="Seg_2345" s="T279">keps-i͡ek-tere</ta>
            <ta e="T281" id="Seg_2346" s="T280">ɨll-ɨ͡ak-tara</ta>
            <ta e="T282" id="Seg_2347" s="T281">daːgɨnɨ</ta>
            <ta e="T283" id="Seg_2348" s="T282">kim</ta>
            <ta e="T284" id="Seg_2349" s="T283">ɨllɨː-r</ta>
            <ta e="T285" id="Seg_2350" s="T284">onnuk-tar</ta>
            <ta e="T286" id="Seg_2351" s="T285">baːl-lar</ta>
            <ta e="T303" id="Seg_2352" s="T302">pakojnʼik-ka</ta>
            <ta e="T304" id="Seg_2353" s="T303">bar-ɨ͡a-m</ta>
            <ta e="T305" id="Seg_2354" s="T304">harsɨn</ta>
            <ta e="T306" id="Seg_2355" s="T305">tahaːr-ɨ͡ak-tara</ta>
            <ta e="T307" id="Seg_2356" s="T306">bu͡o</ta>
            <ta e="T308" id="Seg_2357" s="T307">emeːksin-i</ta>
            <ta e="T309" id="Seg_2358" s="T308">hanɨ</ta>
            <ta e="T310" id="Seg_2359" s="T309">hataː-n</ta>
            <ta e="T311" id="Seg_2360" s="T310">hɨrs-ɨ-ba-tɨ-lar</ta>
            <ta e="T312" id="Seg_2361" s="T311">ol</ta>
            <ta e="T313" id="Seg_2362" s="T312">u͡olat-tar-ɨ-m</ta>
            <ta e="T314" id="Seg_2363" s="T313">pakojnʼik-ka</ta>
            <ta e="T315" id="Seg_2364" s="T314">bar-dak-tarɨna</ta>
            <ta e="T316" id="Seg_2365" s="T315">aragiː-l-ɨ͡ak-tara</ta>
            <ta e="T317" id="Seg_2366" s="T316">bu͡o</ta>
            <ta e="T318" id="Seg_2367" s="T317">tak</ta>
            <ta e="T319" id="Seg_2368" s="T318">da</ta>
            <ta e="T320" id="Seg_2369" s="T319">elbeg-i</ta>
            <ta e="T321" id="Seg_2370" s="T320">ih-e-r-pet-ter</ta>
            <ta e="T322" id="Seg_2371" s="T321">üs</ta>
            <ta e="T323" id="Seg_2372" s="T322">ere</ta>
            <ta e="T324" id="Seg_2373" s="T323">ürümke-ni</ta>
            <ta e="T325" id="Seg_2374" s="T324">ih-e-r-d-eːčči-bit</ta>
            <ta e="T326" id="Seg_2375" s="T325">bihigi</ta>
            <ta e="T327" id="Seg_2376" s="T326">pakojnʼik-ka</ta>
            <ta e="T328" id="Seg_2377" s="T327">ele-te</ta>
            <ta e="T329" id="Seg_2378" s="T328">bolʼše</ta>
            <ta e="T330" id="Seg_2379" s="T329">aragiː</ta>
            <ta e="T331" id="Seg_2380" s="T330">bi͡er-bet-ter</ta>
            <ta e="T332" id="Seg_2381" s="T331">büt-el-ler</ta>
            <ta e="T333" id="Seg_2382" s="T332">onon</ta>
            <ta e="T334" id="Seg_2383" s="T333">ataːr-ɨː-ga</ta>
            <ta e="T341" id="Seg_2384" s="T339">ol</ta>
            <ta e="T342" id="Seg_2385" s="T341">emeːksin-i</ta>
            <ta e="T343" id="Seg_2386" s="T342">du͡o</ta>
            <ta e="T344" id="Seg_2387" s="T343">bil-e-bin</ta>
            <ta e="T345" id="Seg_2388" s="T344">min</ta>
            <ta e="T346" id="Seg_2389" s="T345">ogonnʼor-u-m</ta>
            <ta e="T347" id="Seg_2390" s="T346">ubaj-ɨ-n</ta>
            <ta e="T348" id="Seg_2391" s="T347">dʼaktar-a</ta>
            <ta e="T349" id="Seg_2392" s="T348">buraːt-ɨ-n</ta>
            <ta e="T350" id="Seg_2393" s="T349">dʼaktar-a</ta>
            <ta e="T351" id="Seg_2394" s="T350">dvojurodnaj bɨraːt-ɨ-n</ta>
            <ta e="T352" id="Seg_2395" s="T351">bɨraːt-a</ta>
            <ta e="T353" id="Seg_2396" s="T352">e-t-e</ta>
            <ta e="T354" id="Seg_2397" s="T353">bert</ta>
            <ta e="T355" id="Seg_2398" s="T354">bil-e-bin</ta>
            <ta e="T356" id="Seg_2399" s="T355">biːrge</ta>
            <ta e="T357" id="Seg_2400" s="T356">hɨldʼ-aːččɨ</ta>
            <ta e="T358" id="Seg_2401" s="T357">e-ti-bit</ta>
            <ta e="T359" id="Seg_2402" s="T358">anɨ</ta>
            <ta e="T360" id="Seg_2403" s="T359">bu͡o</ta>
            <ta e="T361" id="Seg_2404" s="T360">kɨ͡aj-an</ta>
            <ta e="T362" id="Seg_2405" s="T361">bar-bap-pɨn</ta>
            <ta e="T363" id="Seg_2406" s="T362">hɨt-a-bɨn</ta>
            <ta e="T364" id="Seg_2407" s="T363">onno</ta>
            <ta e="T365" id="Seg_2408" s="T364">hɨt-ɨ͡ak</ta>
            <ta e="T366" id="Seg_2409" s="T365">e-ti-m</ta>
            <ta e="T367" id="Seg_2410" s="T366">gini͡e-ke</ta>
            <ta e="T368" id="Seg_2411" s="T367">ogo-m</ta>
            <ta e="T369" id="Seg_2412" s="T368">itte</ta>
            <ta e="T370" id="Seg_2413" s="T369">kel-en</ta>
            <ta e="T371" id="Seg_2414" s="T370">ogo-m</ta>
            <ta e="T372" id="Seg_2415" s="T371">kel-be-teg-e</ta>
            <ta e="T373" id="Seg_2416" s="T372">bu͡o</ta>
            <ta e="T374" id="Seg_2417" s="T373">bar-ɨ͡ak</ta>
            <ta e="T375" id="Seg_2418" s="T374">e-ti-m</ta>
            <ta e="T376" id="Seg_2419" s="T375">ügüs</ta>
            <ta e="T381" id="Seg_2420" s="T380">eː</ta>
            <ta e="T382" id="Seg_2421" s="T381">heː</ta>
            <ta e="T383" id="Seg_2422" s="T382">begehe</ta>
            <ta e="T384" id="Seg_2423" s="T383">bügün</ta>
            <ta e="T385" id="Seg_2424" s="T384">harsɨn</ta>
            <ta e="T386" id="Seg_2425" s="T385">taks-ar</ta>
            <ta e="T387" id="Seg_2426" s="T386">üh-üs</ta>
            <ta e="T388" id="Seg_2427" s="T387">kün-ü-ger</ta>
            <ta e="T389" id="Seg_2428" s="T388">taks-aːččɨ</ta>
            <ta e="T391" id="Seg_2429" s="T390">dʼe</ta>
            <ta e="T392" id="Seg_2430" s="T391">onton</ta>
            <ta e="T393" id="Seg_2431" s="T392">ele-te</ta>
            <ta e="T394" id="Seg_2432" s="T393">du͡o</ta>
            <ta e="T395" id="Seg_2433" s="T394">ɨjɨt-ar</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KiPP">
            <ta e="T2" id="Seg_2434" s="T1">büt-An</ta>
            <ta e="T3" id="Seg_2435" s="T2">er-Ar</ta>
            <ta e="T4" id="Seg_2436" s="T3">bu͡olla</ta>
            <ta e="T5" id="Seg_2437" s="T4">bihigi</ta>
            <ta e="T6" id="Seg_2438" s="T5">baːr-BIt</ta>
            <ta e="T7" id="Seg_2439" s="T6">turkarɨ</ta>
            <ta e="T8" id="Seg_2440" s="T7">baːr</ta>
            <ta e="T9" id="Seg_2441" s="T8">anɨ</ta>
            <ta e="T10" id="Seg_2442" s="T9">bihigi</ta>
            <ta e="T11" id="Seg_2443" s="T10">büt-TAK-BItInA</ta>
            <ta e="T12" id="Seg_2444" s="T11">emi͡e</ta>
            <ta e="T13" id="Seg_2445" s="T12">atɨn</ta>
            <ta e="T14" id="Seg_2446" s="T13">bu͡ol-IAK-LArA</ta>
            <ta e="T15" id="Seg_2447" s="T14">giniler</ta>
            <ta e="T16" id="Seg_2448" s="T15">bihigi</ta>
            <ta e="T17" id="Seg_2449" s="T16">anɨ</ta>
            <ta e="T18" id="Seg_2450" s="T17">ol-GA</ta>
            <ta e="T19" id="Seg_2451" s="T18">olor-A</ta>
            <ta e="T20" id="Seg_2452" s="T19">hataː-A-BIt</ta>
            <ta e="T21" id="Seg_2453" s="T20">daːganɨ</ta>
            <ta e="T22" id="Seg_2454" s="T21">anɨ</ta>
            <ta e="T23" id="Seg_2455" s="T22">maladʼož-LAr-I-ŋ</ta>
            <ta e="T24" id="Seg_2456" s="T23">ihit-IAK-LArA</ta>
            <ta e="T25" id="Seg_2457" s="T24">bu͡o</ta>
            <ta e="T26" id="Seg_2458" s="T25">atɨn</ta>
            <ta e="T27" id="Seg_2459" s="T26">hir-GA</ta>
            <ta e="T28" id="Seg_2460" s="T27">bar-An</ta>
            <ta e="T29" id="Seg_2461" s="T28">is-Ar-LAr</ta>
            <ta e="T30" id="Seg_2462" s="T29">tuspa</ta>
            <ta e="T31" id="Seg_2463" s="T30">bu͡ol-Ar-LAr</ta>
            <ta e="T32" id="Seg_2464" s="T31">obɨčaj-LAr</ta>
            <ta e="T33" id="Seg_2465" s="T32">taːk</ta>
            <ta e="T34" id="Seg_2466" s="T33">da</ta>
            <ta e="T35" id="Seg_2467" s="T34">bihigi</ta>
            <ta e="T36" id="Seg_2468" s="T35">ol</ta>
            <ta e="T37" id="Seg_2469" s="T36">staraj-LAr</ta>
            <ta e="T38" id="Seg_2470" s="T37">obɨčaj-LArI-nAn</ta>
            <ta e="T39" id="Seg_2471" s="T38">olor-Ar</ta>
            <ta e="T40" id="Seg_2472" s="T39">e-TI-BIt</ta>
            <ta e="T41" id="Seg_2473" s="T40">manna</ta>
            <ta e="T42" id="Seg_2474" s="T41">ɨ͡aldʼɨt</ta>
            <ta e="T43" id="Seg_2475" s="T42">kel-Ar-tI-n</ta>
            <ta e="T44" id="Seg_2476" s="T43">tohuj-Ar-GA</ta>
            <ta e="T45" id="Seg_2477" s="T44">ɨraːk-ttAn</ta>
            <ta e="T46" id="Seg_2478" s="T45">ɨ͡aldʼɨt</ta>
            <ta e="T47" id="Seg_2479" s="T46">kel-Ar-GAr</ta>
            <ta e="T48" id="Seg_2480" s="T47">ötü͡ö</ta>
            <ta e="T49" id="Seg_2481" s="T48">as-TA</ta>
            <ta e="T50" id="Seg_2482" s="T49">ahaː-A-t-IAK-GA</ta>
            <ta e="T51" id="Seg_2483" s="T50">üčügej</ta>
            <ta e="T52" id="Seg_2484" s="T51">as-nI</ta>
            <ta e="T53" id="Seg_2485" s="T52">ahaː-A-t-IAK-GA</ta>
            <ta e="T54" id="Seg_2486" s="T53">di͡e-AːččI</ta>
            <ta e="T55" id="Seg_2487" s="T54">e-TI-BIt</ta>
            <ta e="T56" id="Seg_2488" s="T55">onton</ta>
            <ta e="T57" id="Seg_2489" s="T56">ɨ͡aldʼɨt-LAr</ta>
            <ta e="T58" id="Seg_2490" s="T57">kel-TAK-TArInA</ta>
            <ta e="T59" id="Seg_2491" s="T58">aragiː-nAn</ta>
            <ta e="T60" id="Seg_2492" s="T59">is-A-r-t-A-BIt</ta>
            <ta e="T61" id="Seg_2493" s="T60">agɨjak-LIk</ta>
            <ta e="T62" id="Seg_2494" s="T61">giniler</ta>
            <ta e="T63" id="Seg_2495" s="T62">kördük</ta>
            <ta e="T64" id="Seg_2496" s="T63">iti</ta>
            <ta e="T65" id="Seg_2497" s="T64">is-BAT-BIt</ta>
            <ta e="T66" id="Seg_2498" s="T65">bihigi</ta>
            <ta e="T67" id="Seg_2499" s="T66">üs</ta>
            <ta e="T68" id="Seg_2500" s="T67">ürümka-nI</ta>
            <ta e="T69" id="Seg_2501" s="T68">is-A-r-t-IAK-BIt</ta>
            <ta e="T70" id="Seg_2502" s="T69">büt-A-r-A-BIt</ta>
            <ta e="T71" id="Seg_2503" s="T70">bu͡o</ta>
            <ta e="T72" id="Seg_2504" s="T71">urukku</ta>
            <ta e="T73" id="Seg_2505" s="T72">kihi</ta>
            <ta e="T74" id="Seg_2506" s="T73">itirik-GA</ta>
            <ta e="T75" id="Seg_2507" s="T74">di͡eri</ta>
            <ta e="T78" id="Seg_2508" s="T77">kuhagan</ta>
            <ta e="T93" id="Seg_2509" s="T92">kün</ta>
            <ta e="T95" id="Seg_2510" s="T93">aːjɨ</ta>
            <ta e="T96" id="Seg_2511" s="T95">is-Ar-LAr</ta>
            <ta e="T97" id="Seg_2512" s="T96">eː</ta>
            <ta e="T102" id="Seg_2513" s="T100">kün</ta>
            <ta e="T103" id="Seg_2514" s="T102">aːjɨ</ta>
            <ta e="T104" id="Seg_2515" s="T103">is-Ar-LAr</ta>
            <ta e="T105" id="Seg_2516" s="T104">atɨn</ta>
            <ta e="T106" id="Seg_2517" s="T105">hir-ttAn</ta>
            <ta e="T107" id="Seg_2518" s="T106">aragiː-nI</ta>
            <ta e="T108" id="Seg_2519" s="T107">egel-I-n-A-egel-I-n-A</ta>
            <ta e="T109" id="Seg_2520" s="T108">bihigi-GA</ta>
            <ta e="T110" id="Seg_2521" s="T109">hu͡ok</ta>
            <ta e="T111" id="Seg_2522" s="T110">bu</ta>
            <ta e="T112" id="Seg_2523" s="T111">dojdu-GA</ta>
            <ta e="T113" id="Seg_2524" s="T112">aragiː</ta>
            <ta e="T114" id="Seg_2525" s="T113">bihigi</ta>
            <ta e="T115" id="Seg_2526" s="T114">kɨrdʼagas-LAr</ta>
            <ta e="T116" id="Seg_2527" s="T115">baːr-BItInA</ta>
            <ta e="T117" id="Seg_2528" s="T116">aragiː-nI</ta>
            <ta e="T118" id="Seg_2529" s="T117">egel-TAr-BAT-BIt</ta>
            <ta e="T119" id="Seg_2530" s="T118">bu͡o</ta>
            <ta e="T120" id="Seg_2531" s="T119">ogo-LAr</ta>
            <ta e="T121" id="Seg_2532" s="T120">ajɨː-LIː</ta>
            <ta e="T122" id="Seg_2533" s="T121">ahaː-BAT-LAr</ta>
            <ta e="T123" id="Seg_2534" s="T122">üčügej</ta>
            <ta e="T124" id="Seg_2535" s="T123">dʼon</ta>
            <ta e="T125" id="Seg_2536" s="T124">bu͡ol-IAK-LArI-n</ta>
            <ta e="T126" id="Seg_2537" s="T125">kihi</ta>
            <ta e="T127" id="Seg_2538" s="T126">bu͡ol-IAK-LArI-n</ta>
            <ta e="T128" id="Seg_2539" s="T127">prazdnʼik-GA</ta>
            <ta e="T129" id="Seg_2540" s="T128">ere</ta>
            <ta e="T130" id="Seg_2541" s="T129">egel-TAr-AːččI-BIt</ta>
            <ta e="T131" id="Seg_2542" s="T130">anɨ</ta>
            <ta e="T132" id="Seg_2543" s="T131">iti</ta>
            <ta e="T133" id="Seg_2544" s="T132">onton-mantan</ta>
            <ta e="T134" id="Seg_2545" s="T133">egel-Ar-LAr</ta>
            <ta e="T135" id="Seg_2546" s="T134">Ürüŋ Kaja-ttAn</ta>
            <ta e="T136" id="Seg_2547" s="T135">egel-Ar-LAr</ta>
            <ta e="T137" id="Seg_2548" s="T136">Rɨmnaj-ttAn</ta>
            <ta e="T138" id="Seg_2549" s="T137">egel-Ar-LAr</ta>
            <ta e="T139" id="Seg_2550" s="T138">Nosku͡o-ttAn</ta>
            <ta e="T140" id="Seg_2551" s="T139">egel-An</ta>
            <ta e="T141" id="Seg_2552" s="T140">kel-Ar-LAr</ta>
            <ta e="T142" id="Seg_2553" s="T141">ol-GA</ta>
            <ta e="T143" id="Seg_2554" s="T142">itir-Ar-LAr</ta>
            <ta e="T144" id="Seg_2555" s="T143">iti</ta>
            <ta e="T145" id="Seg_2556" s="T144">bu</ta>
            <ta e="T146" id="Seg_2557" s="T145">dojdu-GA</ta>
            <ta e="T147" id="Seg_2558" s="T146">aragiː</ta>
            <ta e="T148" id="Seg_2559" s="T147">hu͡ok</ta>
            <ta e="T149" id="Seg_2560" s="T148">is-A-r.[t]-BAT-BIt</ta>
            <ta e="T150" id="Seg_2561" s="T149">aragiː-nI</ta>
            <ta e="T157" id="Seg_2562" s="T156">vsʼo ravno</ta>
            <ta e="T158" id="Seg_2563" s="T157">bar-IAK-LArA</ta>
            <ta e="T159" id="Seg_2564" s="T158">nemnožko</ta>
            <ta e="T160" id="Seg_2565" s="T159">koločuk</ta>
            <ta e="T161" id="Seg_2566" s="T160">bu͡ol-IAK-LArA</ta>
            <ta e="T162" id="Seg_2567" s="T161">onnoːgor</ta>
            <ta e="T163" id="Seg_2568" s="T162">smʼelaj-LIk</ta>
            <ta e="T164" id="Seg_2569" s="T163">bar-IAK-LArA</ta>
            <ta e="T165" id="Seg_2570" s="T164">aragiː</ta>
            <ta e="T166" id="Seg_2571" s="T165">hu͡ok</ta>
            <ta e="T167" id="Seg_2572" s="T166">bu͡o</ta>
            <ta e="T168" id="Seg_2573" s="T167">bar-IAK-LArI-n</ta>
            <ta e="T169" id="Seg_2574" s="T168">kata</ta>
            <ta e="T170" id="Seg_2575" s="T169">bu͡o</ta>
            <ta e="T171" id="Seg_2576" s="T170">kallaːn-BIt</ta>
            <ta e="T172" id="Seg_2577" s="T171">kuhagan</ta>
            <ta e="T173" id="Seg_2578" s="T172">menjaj-LAN-BIT</ta>
            <ta e="T180" id="Seg_2579" s="T179">aragiː-nAn</ta>
            <ta e="T181" id="Seg_2580" s="T180">taba-nI</ta>
            <ta e="T182" id="Seg_2581" s="T181">ahaː-A-t-BAT-LAr</ta>
            <ta e="T183" id="Seg_2582" s="T182">hir-ttAn</ta>
            <ta e="T184" id="Seg_2583" s="T183">ere</ta>
            <ta e="T185" id="Seg_2584" s="T184">ahaː-Ar</ta>
            <ta e="T186" id="Seg_2585" s="T185">taba-ŋ</ta>
            <ta e="T187" id="Seg_2586" s="T186">hir-ttAn</ta>
            <ta e="T188" id="Seg_2587" s="T187">ahaː-Ar</ta>
            <ta e="T189" id="Seg_2588" s="T188">taba</ta>
            <ta e="T190" id="Seg_2589" s="T189">hɨrɨs-Iː-LAr-GA</ta>
            <ta e="T191" id="Seg_2590" s="T190">bagas</ta>
            <ta e="T192" id="Seg_2591" s="T191">üčügej</ta>
            <ta e="T193" id="Seg_2592" s="T192">bu͡ol-IAK-LArA</ta>
            <ta e="T194" id="Seg_2593" s="T193">bu͡o</ta>
            <ta e="T195" id="Seg_2594" s="T194">taːk</ta>
            <ta e="T196" id="Seg_2595" s="T195">da</ta>
            <ta e="T197" id="Seg_2596" s="T196">anɨ</ta>
            <ta e="T198" id="Seg_2597" s="T197">tu͡ok</ta>
            <ta e="T199" id="Seg_2598" s="T198">aragiː-tI-n</ta>
            <ta e="T200" id="Seg_2599" s="T199">bu͡ol-IAK-LArA=Ij</ta>
            <ta e="T201" id="Seg_2600" s="T200">aragiː-LArA</ta>
            <ta e="T202" id="Seg_2601" s="T201">büt-IAK-tA</ta>
            <ta e="T203" id="Seg_2602" s="T202">ölör-TAr-BIT</ta>
            <ta e="T204" id="Seg_2603" s="T203">kihi-LAr</ta>
            <ta e="T205" id="Seg_2604" s="T204">kɨ͡aj-An</ta>
            <ta e="T206" id="Seg_2605" s="T205">bar-IAK-LArA</ta>
            <ta e="T207" id="Seg_2606" s="T206">hu͡ok-tA</ta>
            <ta e="T208" id="Seg_2607" s="T207">kɨ͡aj-An</ta>
            <ta e="T209" id="Seg_2608" s="T208">oːnnʼoː-IAK-LArA</ta>
            <ta e="T210" id="Seg_2609" s="T209">hu͡ok</ta>
            <ta e="T211" id="Seg_2610" s="T210">taːk</ta>
            <ta e="T212" id="Seg_2611" s="T211">da</ta>
            <ta e="T213" id="Seg_2612" s="T212">aŋar</ta>
            <ta e="T214" id="Seg_2613" s="T213">kihi-ŋ</ta>
            <ta e="T215" id="Seg_2614" s="T214">anɨ</ta>
            <ta e="T216" id="Seg_2615" s="T215">is</ta>
            <ta e="T217" id="Seg_2616" s="T216">hɨrɨs-Ar</ta>
            <ta e="T218" id="Seg_2617" s="T217">kihi-LAr-I-ŋ</ta>
            <ta e="T219" id="Seg_2618" s="T218">is-BAT-LAr</ta>
            <ta e="T220" id="Seg_2619" s="T219">bu͡o</ta>
            <ta e="T221" id="Seg_2620" s="T220">taba</ta>
            <ta e="T222" id="Seg_2621" s="T221">kiːr-TAː-A-s-t-IAK-LArA</ta>
            <ta e="T223" id="Seg_2622" s="T222">anɨ</ta>
            <ta e="T224" id="Seg_2623" s="T223">hɨrɨs-Iː-GA</ta>
            <ta e="T225" id="Seg_2624" s="T224">hɨrɨs-Ar</ta>
            <ta e="T226" id="Seg_2625" s="T225">taba</ta>
            <ta e="T227" id="Seg_2626" s="T226">innʼe</ta>
            <ta e="T228" id="Seg_2627" s="T227">egel-An-LAr</ta>
            <ta e="T229" id="Seg_2628" s="T228">manna</ta>
            <ta e="T230" id="Seg_2629" s="T229">taba-LAː-IAK-LArA</ta>
            <ta e="T231" id="Seg_2630" s="T230">ol</ta>
            <ta e="T232" id="Seg_2631" s="T231">taba-nI</ta>
            <ta e="T233" id="Seg_2632" s="T232">ol</ta>
            <ta e="T234" id="Seg_2633" s="T233">ka</ta>
            <ta e="T235" id="Seg_2634" s="T234">bu</ta>
            <ta e="T236" id="Seg_2635" s="T235">ɨt-LArA</ta>
            <ta e="T237" id="Seg_2636" s="T236">ügüs-tA</ta>
            <ta e="T238" id="Seg_2637" s="T237">bert</ta>
            <ta e="T239" id="Seg_2638" s="T238">bu</ta>
            <ta e="T240" id="Seg_2639" s="T239">ɨt-LArA</ta>
            <ta e="T241" id="Seg_2640" s="T240">hatan-TAr-BAT-LAr</ta>
            <ta e="T242" id="Seg_2641" s="T241">kɨ͡aj-An</ta>
            <ta e="T243" id="Seg_2642" s="T242">taba-LAː-A-t-IAK-LArA</ta>
            <ta e="T244" id="Seg_2643" s="T243">hu͡ok-tA</ta>
            <ta e="T245" id="Seg_2644" s="T244">ol</ta>
            <ta e="T246" id="Seg_2645" s="T245">Gubaː-GA</ta>
            <ta e="T247" id="Seg_2646" s="T246">hɨrɨs-AːččI-LAr</ta>
            <ta e="T248" id="Seg_2647" s="T247">iti</ta>
            <ta e="T249" id="Seg_2648" s="T248">dek</ta>
            <ta e="T250" id="Seg_2649" s="T249">bar-AːččI-LAr</ta>
            <ta e="T251" id="Seg_2650" s="T250">oːnnʼoː-A</ta>
            <ta e="T252" id="Seg_2651" s="T251">anɨ</ta>
            <ta e="T253" id="Seg_2652" s="T252">onno</ta>
            <ta e="T255" id="Seg_2653" s="T253">dʼi͡e</ta>
            <ta e="T258" id="Seg_2654" s="T257">eː</ta>
            <ta e="T259" id="Seg_2655" s="T258">anɨ</ta>
            <ta e="T260" id="Seg_2656" s="T259">dʼi͡e</ta>
            <ta e="T261" id="Seg_2657" s="T260">tut-IAK-LArA</ta>
            <ta e="T262" id="Seg_2658" s="T261">onno</ta>
            <ta e="T263" id="Seg_2659" s="T262">balok-LAr-nI</ta>
            <ta e="T264" id="Seg_2660" s="T263">ilin-Ar-LAr</ta>
            <ta e="T265" id="Seg_2661" s="T264">bügün</ta>
            <ta e="T266" id="Seg_2662" s="T265">mi͡ete-LAr-nI</ta>
            <ta e="T267" id="Seg_2663" s="T266">oŋohun-I-Ar-LAr</ta>
            <ta e="T268" id="Seg_2664" s="T267">bügün</ta>
            <ta e="T269" id="Seg_2665" s="T268">belemneː-A-n-Ar</ta>
            <ta e="T270" id="Seg_2666" s="T269">kün-LArA</ta>
            <ta e="T271" id="Seg_2667" s="T270">sɨ͡ana</ta>
            <ta e="T272" id="Seg_2668" s="T271">oŋor-Ar-LAr</ta>
            <ta e="T273" id="Seg_2669" s="T272">onno</ta>
            <ta e="T274" id="Seg_2670" s="T273">bu͡očuku-LAr-I-nAn</ta>
            <ta e="T275" id="Seg_2671" s="T274">ol</ta>
            <ta e="T276" id="Seg_2672" s="T275">dojdu-GA</ta>
            <ta e="T277" id="Seg_2673" s="T276">onno</ta>
            <ta e="T278" id="Seg_2674" s="T277">onno</ta>
            <ta e="T279" id="Seg_2675" s="T278">ös</ta>
            <ta e="T280" id="Seg_2676" s="T279">kepseː-IAK-LArA</ta>
            <ta e="T281" id="Seg_2677" s="T280">ɨllaː-IAK-LArA</ta>
            <ta e="T282" id="Seg_2678" s="T281">daːganɨ</ta>
            <ta e="T283" id="Seg_2679" s="T282">kim</ta>
            <ta e="T284" id="Seg_2680" s="T283">ɨllaː-Ar</ta>
            <ta e="T285" id="Seg_2681" s="T284">onnuk-LAr</ta>
            <ta e="T286" id="Seg_2682" s="T285">baːr-LAr</ta>
            <ta e="T303" id="Seg_2683" s="T302">pakojnʼik-GA</ta>
            <ta e="T304" id="Seg_2684" s="T303">bar-IAK-m</ta>
            <ta e="T305" id="Seg_2685" s="T304">harsɨn</ta>
            <ta e="T306" id="Seg_2686" s="T305">tahaːr-IAK-LArA</ta>
            <ta e="T307" id="Seg_2687" s="T306">bu͡o</ta>
            <ta e="T308" id="Seg_2688" s="T307">emeːksin-nI</ta>
            <ta e="T309" id="Seg_2689" s="T308">hannɨ</ta>
            <ta e="T310" id="Seg_2690" s="T309">hataː-An</ta>
            <ta e="T311" id="Seg_2691" s="T310">hɨrɨs-I-BA-TI-LAr</ta>
            <ta e="T312" id="Seg_2692" s="T311">ol</ta>
            <ta e="T313" id="Seg_2693" s="T312">u͡ol-LAr-I-m</ta>
            <ta e="T314" id="Seg_2694" s="T313">pakojnʼik-GA</ta>
            <ta e="T315" id="Seg_2695" s="T314">bar-TAK-TArInA</ta>
            <ta e="T316" id="Seg_2696" s="T315">aragiː-LAː-IAK-LArA</ta>
            <ta e="T317" id="Seg_2697" s="T316">bu͡o</ta>
            <ta e="T318" id="Seg_2698" s="T317">taːk</ta>
            <ta e="T319" id="Seg_2699" s="T318">da</ta>
            <ta e="T320" id="Seg_2700" s="T319">elbek-nI</ta>
            <ta e="T321" id="Seg_2701" s="T320">is-A-r.[t]-BAT-LAr</ta>
            <ta e="T322" id="Seg_2702" s="T321">üs</ta>
            <ta e="T323" id="Seg_2703" s="T322">ere</ta>
            <ta e="T324" id="Seg_2704" s="T323">ürümka-nI</ta>
            <ta e="T325" id="Seg_2705" s="T324">is-A-r-t-AːččI-BIt</ta>
            <ta e="T326" id="Seg_2706" s="T325">bihigi</ta>
            <ta e="T327" id="Seg_2707" s="T326">pakojnʼik-GA</ta>
            <ta e="T328" id="Seg_2708" s="T327">ele-tA</ta>
            <ta e="T329" id="Seg_2709" s="T328">bolʼše</ta>
            <ta e="T330" id="Seg_2710" s="T329">aragiː</ta>
            <ta e="T331" id="Seg_2711" s="T330">bi͡er-BAT-LAr</ta>
            <ta e="T332" id="Seg_2712" s="T331">büt-Ar-LAr</ta>
            <ta e="T333" id="Seg_2713" s="T332">onon</ta>
            <ta e="T334" id="Seg_2714" s="T333">ataːr-Iː-GA</ta>
            <ta e="T341" id="Seg_2715" s="T339">ol</ta>
            <ta e="T342" id="Seg_2716" s="T341">emeːksin-nI</ta>
            <ta e="T343" id="Seg_2717" s="T342">du͡o</ta>
            <ta e="T344" id="Seg_2718" s="T343">bil-A-BIn</ta>
            <ta e="T345" id="Seg_2719" s="T344">min</ta>
            <ta e="T346" id="Seg_2720" s="T345">ogonnʼor-I-m</ta>
            <ta e="T347" id="Seg_2721" s="T346">ubaj-tI-n</ta>
            <ta e="T348" id="Seg_2722" s="T347">dʼaktar-tA</ta>
            <ta e="T349" id="Seg_2723" s="T348">bɨraːt-tI-n</ta>
            <ta e="T350" id="Seg_2724" s="T349">dʼaktar-tA</ta>
            <ta e="T351" id="Seg_2725" s="T350">dvojurodnaj bɨraːt-tI-n</ta>
            <ta e="T352" id="Seg_2726" s="T351">bɨraːt-tA</ta>
            <ta e="T353" id="Seg_2727" s="T352">e-TI-tA</ta>
            <ta e="T354" id="Seg_2728" s="T353">bert</ta>
            <ta e="T355" id="Seg_2729" s="T354">bil-A-BIn</ta>
            <ta e="T356" id="Seg_2730" s="T355">biːrge</ta>
            <ta e="T357" id="Seg_2731" s="T356">hɨrɨt-AːččI</ta>
            <ta e="T358" id="Seg_2732" s="T357">e-TI-BIt</ta>
            <ta e="T359" id="Seg_2733" s="T358">anɨ</ta>
            <ta e="T360" id="Seg_2734" s="T359">bu͡o</ta>
            <ta e="T361" id="Seg_2735" s="T360">kɨ͡aj-An</ta>
            <ta e="T362" id="Seg_2736" s="T361">bar-BAT-BIn</ta>
            <ta e="T363" id="Seg_2737" s="T362">hɨt-A-BIn</ta>
            <ta e="T364" id="Seg_2738" s="T363">onno</ta>
            <ta e="T365" id="Seg_2739" s="T364">hɨt-IAK</ta>
            <ta e="T366" id="Seg_2740" s="T365">e-TI-m</ta>
            <ta e="T367" id="Seg_2741" s="T366">gini-GA</ta>
            <ta e="T368" id="Seg_2742" s="T367">ogo-m</ta>
            <ta e="T369" id="Seg_2743" s="T368">itte</ta>
            <ta e="T370" id="Seg_2744" s="T369">kel-An</ta>
            <ta e="T371" id="Seg_2745" s="T370">ogo-m</ta>
            <ta e="T372" id="Seg_2746" s="T371">kel-BA-TAK-tA</ta>
            <ta e="T373" id="Seg_2747" s="T372">bu͡o</ta>
            <ta e="T374" id="Seg_2748" s="T373">bar-IAK</ta>
            <ta e="T375" id="Seg_2749" s="T374">e-TI-m</ta>
            <ta e="T376" id="Seg_2750" s="T375">ügüs</ta>
            <ta e="T381" id="Seg_2751" s="T380">eː</ta>
            <ta e="T382" id="Seg_2752" s="T381">eː</ta>
            <ta e="T383" id="Seg_2753" s="T382">begeheː</ta>
            <ta e="T384" id="Seg_2754" s="T383">bügün</ta>
            <ta e="T385" id="Seg_2755" s="T384">harsɨn</ta>
            <ta e="T386" id="Seg_2756" s="T385">tagɨs-Ar</ta>
            <ta e="T387" id="Seg_2757" s="T386">üs-Is</ta>
            <ta e="T388" id="Seg_2758" s="T387">kün-tI-GAr</ta>
            <ta e="T389" id="Seg_2759" s="T388">tagɨs-AːččI</ta>
            <ta e="T391" id="Seg_2760" s="T390">dʼe</ta>
            <ta e="T392" id="Seg_2761" s="T391">onton</ta>
            <ta e="T393" id="Seg_2762" s="T392">ele-tA</ta>
            <ta e="T394" id="Seg_2763" s="T393">du͡o</ta>
            <ta e="T395" id="Seg_2764" s="T394">ɨjɨt-Ar</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KiPP">
            <ta e="T2" id="Seg_2765" s="T1">stop-CVB.SEQ</ta>
            <ta e="T3" id="Seg_2766" s="T2">be-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_2767" s="T3">MOD</ta>
            <ta e="T5" id="Seg_2768" s="T4">1PL.[NOM]</ta>
            <ta e="T6" id="Seg_2769" s="T5">there.is-1PL</ta>
            <ta e="T7" id="Seg_2770" s="T6">as.long.as</ta>
            <ta e="T8" id="Seg_2771" s="T7">there.is</ta>
            <ta e="T9" id="Seg_2772" s="T8">now</ta>
            <ta e="T10" id="Seg_2773" s="T9">1PL.[NOM]</ta>
            <ta e="T11" id="Seg_2774" s="T10">stop-TEMP-1PL</ta>
            <ta e="T12" id="Seg_2775" s="T11">also</ta>
            <ta e="T13" id="Seg_2776" s="T12">different.[NOM]</ta>
            <ta e="T14" id="Seg_2777" s="T13">be-FUT-3PL</ta>
            <ta e="T15" id="Seg_2778" s="T14">3PL.[NOM]</ta>
            <ta e="T16" id="Seg_2779" s="T15">1PL.[NOM]</ta>
            <ta e="T17" id="Seg_2780" s="T16">now</ta>
            <ta e="T18" id="Seg_2781" s="T17">that-DAT/LOC</ta>
            <ta e="T19" id="Seg_2782" s="T18">live-CVB.SIM</ta>
            <ta e="T20" id="Seg_2783" s="T19">try-PRS-1PL</ta>
            <ta e="T21" id="Seg_2784" s="T20">EMPH</ta>
            <ta e="T22" id="Seg_2785" s="T21">now</ta>
            <ta e="T23" id="Seg_2786" s="T22">youth-PL-EP-2SG.[NOM]</ta>
            <ta e="T24" id="Seg_2787" s="T23">hear-FUT-3PL</ta>
            <ta e="T25" id="Seg_2788" s="T24">EMPH</ta>
            <ta e="T26" id="Seg_2789" s="T25">different</ta>
            <ta e="T27" id="Seg_2790" s="T26">place-DAT/LOC</ta>
            <ta e="T28" id="Seg_2791" s="T27">go-CVB.SEQ</ta>
            <ta e="T29" id="Seg_2792" s="T28">go-PRS-3PL</ta>
            <ta e="T30" id="Seg_2793" s="T29">different.[NOM]</ta>
            <ta e="T31" id="Seg_2794" s="T30">be-PRS-3PL</ta>
            <ta e="T32" id="Seg_2795" s="T31">custom-PL.[NOM]</ta>
            <ta e="T33" id="Seg_2796" s="T32">so</ta>
            <ta e="T34" id="Seg_2797" s="T33">EMPH</ta>
            <ta e="T35" id="Seg_2798" s="T34">1PL.[NOM]</ta>
            <ta e="T36" id="Seg_2799" s="T35">that</ta>
            <ta e="T37" id="Seg_2800" s="T36">old-PL.[NOM]</ta>
            <ta e="T38" id="Seg_2801" s="T37">custom-3PL-INSTR</ta>
            <ta e="T39" id="Seg_2802" s="T38">live-PTCP.PRS</ta>
            <ta e="T40" id="Seg_2803" s="T39">be-PST1-1PL</ta>
            <ta e="T41" id="Seg_2804" s="T40">here</ta>
            <ta e="T42" id="Seg_2805" s="T41">guest.[NOM]</ta>
            <ta e="T43" id="Seg_2806" s="T42">come-PTCP.PRS-3SG-ACC</ta>
            <ta e="T44" id="Seg_2807" s="T43">receive-PTCP.PRS-DAT/LOC</ta>
            <ta e="T45" id="Seg_2808" s="T44">distant-ABL</ta>
            <ta e="T46" id="Seg_2809" s="T45">guest.[NOM]</ta>
            <ta e="T47" id="Seg_2810" s="T46">come-PTCP.PRS-DAT/LOC</ta>
            <ta e="T48" id="Seg_2811" s="T47">good</ta>
            <ta e="T49" id="Seg_2812" s="T48">food-PART</ta>
            <ta e="T50" id="Seg_2813" s="T49">eat-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T51" id="Seg_2814" s="T50">good.[NOM]</ta>
            <ta e="T52" id="Seg_2815" s="T51">food-ACC</ta>
            <ta e="T53" id="Seg_2816" s="T52">eat-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T54" id="Seg_2817" s="T53">say-PTCP.HAB</ta>
            <ta e="T55" id="Seg_2818" s="T54">be-PST1-1PL</ta>
            <ta e="T56" id="Seg_2819" s="T55">then</ta>
            <ta e="T57" id="Seg_2820" s="T56">guest-PL.[NOM]</ta>
            <ta e="T58" id="Seg_2821" s="T57">come-TEMP-3PL</ta>
            <ta e="T59" id="Seg_2822" s="T58">spirit-INSTR</ta>
            <ta e="T60" id="Seg_2823" s="T59">go-EP-CAUS-CAUS-PRS-1PL</ta>
            <ta e="T61" id="Seg_2824" s="T60">little-ADVZ</ta>
            <ta e="T62" id="Seg_2825" s="T61">3PL.[NOM]</ta>
            <ta e="T63" id="Seg_2826" s="T62">similar</ta>
            <ta e="T64" id="Seg_2827" s="T63">that.[NOM]</ta>
            <ta e="T65" id="Seg_2828" s="T64">drink-NEG-1PL</ta>
            <ta e="T66" id="Seg_2829" s="T65">1PL.[NOM]</ta>
            <ta e="T67" id="Seg_2830" s="T66">three</ta>
            <ta e="T68" id="Seg_2831" s="T67">shot.glass-ACC</ta>
            <ta e="T69" id="Seg_2832" s="T68">go-EP-CAUS-CAUS-FUT-1PL</ta>
            <ta e="T70" id="Seg_2833" s="T69">stop-EP-CAUS-PRS-1PL</ta>
            <ta e="T71" id="Seg_2834" s="T70">EMPH</ta>
            <ta e="T72" id="Seg_2835" s="T71">former</ta>
            <ta e="T73" id="Seg_2836" s="T72">human.being.[NOM]</ta>
            <ta e="T74" id="Seg_2837" s="T73">drunk-DAT/LOC</ta>
            <ta e="T75" id="Seg_2838" s="T74">until</ta>
            <ta e="T78" id="Seg_2839" s="T77">bad.[NOM]</ta>
            <ta e="T93" id="Seg_2840" s="T92">day.[NOM]</ta>
            <ta e="T95" id="Seg_2841" s="T93">every</ta>
            <ta e="T96" id="Seg_2842" s="T95">drink-PRS-3PL</ta>
            <ta e="T97" id="Seg_2843" s="T96">AFFIRM</ta>
            <ta e="T102" id="Seg_2844" s="T100">day.[NOM]</ta>
            <ta e="T103" id="Seg_2845" s="T102">every</ta>
            <ta e="T104" id="Seg_2846" s="T103">drink-PRS-3PL</ta>
            <ta e="T105" id="Seg_2847" s="T104">different</ta>
            <ta e="T106" id="Seg_2848" s="T105">place-ABL</ta>
            <ta e="T107" id="Seg_2849" s="T106">spirit-ACC</ta>
            <ta e="T108" id="Seg_2850" s="T107">bring-EP-MED-CVB.SIM-bring-EP-MED-CVB.SIM</ta>
            <ta e="T109" id="Seg_2851" s="T108">1PL-DAT/LOC</ta>
            <ta e="T110" id="Seg_2852" s="T109">NEG.EX</ta>
            <ta e="T111" id="Seg_2853" s="T110">this</ta>
            <ta e="T112" id="Seg_2854" s="T111">place-DAT/LOC</ta>
            <ta e="T113" id="Seg_2855" s="T112">spirit.[NOM]</ta>
            <ta e="T114" id="Seg_2856" s="T113">1PL.[NOM]</ta>
            <ta e="T115" id="Seg_2857" s="T114">old-PL.[NOM]</ta>
            <ta e="T116" id="Seg_2858" s="T115">there.is-1PL</ta>
            <ta e="T117" id="Seg_2859" s="T116">spirit-ACC</ta>
            <ta e="T118" id="Seg_2860" s="T117">bring-CAUS-NEG-1PL</ta>
            <ta e="T119" id="Seg_2861" s="T118">EMPH</ta>
            <ta e="T120" id="Seg_2862" s="T119">child-PL.[NOM]</ta>
            <ta e="T121" id="Seg_2863" s="T120">good.spirit-SIM</ta>
            <ta e="T122" id="Seg_2864" s="T121">eat-NEG-3PL</ta>
            <ta e="T123" id="Seg_2865" s="T122">good.[NOM]</ta>
            <ta e="T124" id="Seg_2866" s="T123">people.[NOM]</ta>
            <ta e="T125" id="Seg_2867" s="T124">be-PTCP.FUT-3PL-ACC</ta>
            <ta e="T126" id="Seg_2868" s="T125">human.being.[NOM]</ta>
            <ta e="T127" id="Seg_2869" s="T126">be-PTCP.FUT-3PL-ACC</ta>
            <ta e="T128" id="Seg_2870" s="T127">holiday-DAT/LOC</ta>
            <ta e="T129" id="Seg_2871" s="T128">just</ta>
            <ta e="T130" id="Seg_2872" s="T129">bring-CAUS-HAB-1PL</ta>
            <ta e="T131" id="Seg_2873" s="T130">now</ta>
            <ta e="T132" id="Seg_2874" s="T131">that.[NOM]</ta>
            <ta e="T133" id="Seg_2875" s="T132">from.there-from.here</ta>
            <ta e="T134" id="Seg_2876" s="T133">bring-PRS-3PL</ta>
            <ta e="T135" id="Seg_2877" s="T134">Urung.Xaya-ABL</ta>
            <ta e="T136" id="Seg_2878" s="T135">bring-PRS-3PL</ta>
            <ta e="T137" id="Seg_2879" s="T136">Novorybnoe-ABL</ta>
            <ta e="T138" id="Seg_2880" s="T137">bring-PRS-3PL</ta>
            <ta e="T139" id="Seg_2881" s="T138">Khatanga-ABL</ta>
            <ta e="T140" id="Seg_2882" s="T139">get-CVB.SEQ</ta>
            <ta e="T141" id="Seg_2883" s="T140">come-PRS-3PL</ta>
            <ta e="T142" id="Seg_2884" s="T141">that-DAT/LOC</ta>
            <ta e="T143" id="Seg_2885" s="T142">get.drunk-PRS-3PL</ta>
            <ta e="T144" id="Seg_2886" s="T143">that.[NOM]</ta>
            <ta e="T145" id="Seg_2887" s="T144">this</ta>
            <ta e="T146" id="Seg_2888" s="T145">country-DAT/LOC</ta>
            <ta e="T147" id="Seg_2889" s="T146">spirit.[NOM]</ta>
            <ta e="T148" id="Seg_2890" s="T147">NEG.EX</ta>
            <ta e="T149" id="Seg_2891" s="T148">drink-EP-CAUS.[CAUS]-NEG-1PL</ta>
            <ta e="T150" id="Seg_2892" s="T149">spirit-ACC</ta>
            <ta e="T157" id="Seg_2893" s="T156">however</ta>
            <ta e="T158" id="Seg_2894" s="T157">go-FUT-3PL</ta>
            <ta e="T159" id="Seg_2895" s="T158">a.little.bit</ta>
            <ta e="T160" id="Seg_2896" s="T159">half_drunk.[NOM]</ta>
            <ta e="T161" id="Seg_2897" s="T160">be-FUT-3PL</ta>
            <ta e="T162" id="Seg_2898" s="T161">even</ta>
            <ta e="T163" id="Seg_2899" s="T162">brave-ADVZ</ta>
            <ta e="T164" id="Seg_2900" s="T163">go-FUT-3PL</ta>
            <ta e="T165" id="Seg_2901" s="T164">spirit.[NOM]</ta>
            <ta e="T166" id="Seg_2902" s="T165">NEG.EX</ta>
            <ta e="T167" id="Seg_2903" s="T166">EMPH</ta>
            <ta e="T168" id="Seg_2904" s="T167">go-PTCP.FUT-3PL-ACC</ta>
            <ta e="T169" id="Seg_2905" s="T168">just</ta>
            <ta e="T170" id="Seg_2906" s="T169">EMPH</ta>
            <ta e="T171" id="Seg_2907" s="T170">weather-1PL.[NOM]</ta>
            <ta e="T172" id="Seg_2908" s="T171">bad.[NOM]</ta>
            <ta e="T173" id="Seg_2909" s="T172">change-VBZ-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_2910" s="T179">spirit-INSTR</ta>
            <ta e="T181" id="Seg_2911" s="T180">reindeer-ACC</ta>
            <ta e="T182" id="Seg_2912" s="T181">eat-EP-CAUS-NEG-3PL</ta>
            <ta e="T183" id="Seg_2913" s="T182">earth-ABL</ta>
            <ta e="T184" id="Seg_2914" s="T183">just</ta>
            <ta e="T185" id="Seg_2915" s="T184">eat-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_2916" s="T185">reindeer-2SG.[NOM]</ta>
            <ta e="T187" id="Seg_2917" s="T186">earth-ABL</ta>
            <ta e="T188" id="Seg_2918" s="T187">eat-PRS.[3SG]</ta>
            <ta e="T189" id="Seg_2919" s="T188">reindeer.[NOM]</ta>
            <ta e="T190" id="Seg_2920" s="T189">run.a.race-NMNZ-PL-DAT/LOC</ta>
            <ta e="T191" id="Seg_2921" s="T190">EMPH</ta>
            <ta e="T192" id="Seg_2922" s="T191">good.[NOM]</ta>
            <ta e="T193" id="Seg_2923" s="T192">be-FUT-3PL</ta>
            <ta e="T194" id="Seg_2924" s="T193">EMPH</ta>
            <ta e="T195" id="Seg_2925" s="T194">so</ta>
            <ta e="T196" id="Seg_2926" s="T195">EMPH</ta>
            <ta e="T197" id="Seg_2927" s="T196">now</ta>
            <ta e="T198" id="Seg_2928" s="T197">what.[NOM]</ta>
            <ta e="T199" id="Seg_2929" s="T198">spirit-3SG-ACC</ta>
            <ta e="T200" id="Seg_2930" s="T199">be-FUT-3PL=Q</ta>
            <ta e="T201" id="Seg_2931" s="T200">spirit-3PL.[NOM]</ta>
            <ta e="T202" id="Seg_2932" s="T201">stop-FUT-3SG</ta>
            <ta e="T203" id="Seg_2933" s="T202">injure-PASS-PTCP.PST</ta>
            <ta e="T204" id="Seg_2934" s="T203">human.being-PL.[NOM]</ta>
            <ta e="T205" id="Seg_2935" s="T204">can-CVB.SEQ</ta>
            <ta e="T206" id="Seg_2936" s="T205">go-FUT-3PL</ta>
            <ta e="T207" id="Seg_2937" s="T206">NEG-3SG</ta>
            <ta e="T208" id="Seg_2938" s="T207">can-CVB.SEQ</ta>
            <ta e="T209" id="Seg_2939" s="T208">play-FUT-3PL</ta>
            <ta e="T210" id="Seg_2940" s="T209">NEG</ta>
            <ta e="T211" id="Seg_2941" s="T210">so</ta>
            <ta e="T212" id="Seg_2942" s="T211">EMPH</ta>
            <ta e="T213" id="Seg_2943" s="T212">other.of.two</ta>
            <ta e="T214" id="Seg_2944" s="T213">human.being-2SG.[NOM]</ta>
            <ta e="T215" id="Seg_2945" s="T214">now</ta>
            <ta e="T216" id="Seg_2946" s="T215">drink</ta>
            <ta e="T217" id="Seg_2947" s="T216">run.a.race-PTCP.PRS</ta>
            <ta e="T218" id="Seg_2948" s="T217">human.being-PL-EP-2SG.[NOM]</ta>
            <ta e="T219" id="Seg_2949" s="T218">go-NEG-3PL</ta>
            <ta e="T220" id="Seg_2950" s="T219">EMPH</ta>
            <ta e="T221" id="Seg_2951" s="T220">reindeer.[NOM]</ta>
            <ta e="T222" id="Seg_2952" s="T221">go.in-ITER-EP-RECP/COLL-CAUS-FUT-3PL</ta>
            <ta e="T223" id="Seg_2953" s="T222">now</ta>
            <ta e="T224" id="Seg_2954" s="T223">run.a.race-NMNZ-DAT/LOC</ta>
            <ta e="T225" id="Seg_2955" s="T224">run.a.race-PTCP.PRS</ta>
            <ta e="T226" id="Seg_2956" s="T225">reindeer.[NOM]</ta>
            <ta e="T227" id="Seg_2957" s="T226">so</ta>
            <ta e="T228" id="Seg_2958" s="T227">bring-CVB.SEQ-3PL</ta>
            <ta e="T229" id="Seg_2959" s="T228">hither</ta>
            <ta e="T230" id="Seg_2960" s="T229">reindeer-VBZ-FUT-3PL</ta>
            <ta e="T231" id="Seg_2961" s="T230">that</ta>
            <ta e="T232" id="Seg_2962" s="T231">reindeer-ACC</ta>
            <ta e="T233" id="Seg_2963" s="T232">that</ta>
            <ta e="T234" id="Seg_2964" s="T233">well</ta>
            <ta e="T235" id="Seg_2965" s="T234">this</ta>
            <ta e="T236" id="Seg_2966" s="T235">dog-3PL.[NOM]</ta>
            <ta e="T237" id="Seg_2967" s="T236">many-3SG</ta>
            <ta e="T238" id="Seg_2968" s="T237">very</ta>
            <ta e="T239" id="Seg_2969" s="T238">this</ta>
            <ta e="T240" id="Seg_2970" s="T239">dog-3PL.[NOM]</ta>
            <ta e="T241" id="Seg_2971" s="T240">be.able-CAUS-NEG-3PL</ta>
            <ta e="T242" id="Seg_2972" s="T241">can-CVB.SEQ</ta>
            <ta e="T243" id="Seg_2973" s="T242">reindeer-VBZ-EP-CAUS-FUT-3PL</ta>
            <ta e="T244" id="Seg_2974" s="T243">NEG-3SG</ta>
            <ta e="T245" id="Seg_2975" s="T244">that</ta>
            <ta e="T246" id="Seg_2976" s="T245">Guba-DAT/LOC</ta>
            <ta e="T247" id="Seg_2977" s="T246">run.a.race-HAB-3PL</ta>
            <ta e="T248" id="Seg_2978" s="T247">that.[NOM]</ta>
            <ta e="T249" id="Seg_2979" s="T248">to</ta>
            <ta e="T250" id="Seg_2980" s="T249">go-HAB-3PL</ta>
            <ta e="T251" id="Seg_2981" s="T250">play-CVB.SIM</ta>
            <ta e="T252" id="Seg_2982" s="T251">now</ta>
            <ta e="T253" id="Seg_2983" s="T252">there</ta>
            <ta e="T255" id="Seg_2984" s="T253">tent.[NOM]</ta>
            <ta e="T258" id="Seg_2985" s="T257">AFFIRM</ta>
            <ta e="T259" id="Seg_2986" s="T258">now</ta>
            <ta e="T260" id="Seg_2987" s="T259">tent.[NOM]</ta>
            <ta e="T261" id="Seg_2988" s="T260">build-FUT-3PL</ta>
            <ta e="T262" id="Seg_2989" s="T261">there</ta>
            <ta e="T263" id="Seg_2990" s="T262">balok-PL-ACC</ta>
            <ta e="T264" id="Seg_2991" s="T263">take.away-PRS-3PL</ta>
            <ta e="T265" id="Seg_2992" s="T264">today</ta>
            <ta e="T266" id="Seg_2993" s="T265">sign-PL-ACC</ta>
            <ta e="T267" id="Seg_2994" s="T266">make-EP-PRS-3PL</ta>
            <ta e="T268" id="Seg_2995" s="T267">today</ta>
            <ta e="T269" id="Seg_2996" s="T268">prepare-EP-REFL-PTCP.PRS</ta>
            <ta e="T270" id="Seg_2997" s="T269">day-3PL.[NOM]</ta>
            <ta e="T271" id="Seg_2998" s="T270">stage.[NOM]</ta>
            <ta e="T272" id="Seg_2999" s="T271">make-PRS-3PL</ta>
            <ta e="T273" id="Seg_3000" s="T272">there</ta>
            <ta e="T274" id="Seg_3001" s="T273">barrel-PL-EP-INSTR</ta>
            <ta e="T275" id="Seg_3002" s="T274">that</ta>
            <ta e="T276" id="Seg_3003" s="T275">place-DAT/LOC</ta>
            <ta e="T277" id="Seg_3004" s="T276">there</ta>
            <ta e="T278" id="Seg_3005" s="T277">there</ta>
            <ta e="T279" id="Seg_3006" s="T278">news.[NOM]</ta>
            <ta e="T280" id="Seg_3007" s="T279">tell-FUT-3PL</ta>
            <ta e="T281" id="Seg_3008" s="T280">sing-FUT-3PL</ta>
            <ta e="T282" id="Seg_3009" s="T281">EMPH</ta>
            <ta e="T283" id="Seg_3010" s="T282">who.[NOM]</ta>
            <ta e="T284" id="Seg_3011" s="T283">sing-PRS.[3SG]</ta>
            <ta e="T285" id="Seg_3012" s="T284">such-PL.[NOM]</ta>
            <ta e="T286" id="Seg_3013" s="T285">there.is-3PL</ta>
            <ta e="T303" id="Seg_3014" s="T302">deceased-DAT/LOC</ta>
            <ta e="T304" id="Seg_3015" s="T303">go-FUT-1SG</ta>
            <ta e="T305" id="Seg_3016" s="T304">tomorrow</ta>
            <ta e="T306" id="Seg_3017" s="T305">take.out-FUT-3PL</ta>
            <ta e="T307" id="Seg_3018" s="T306">EMPH</ta>
            <ta e="T308" id="Seg_3019" s="T307">old.woman-ACC</ta>
            <ta e="T309" id="Seg_3020" s="T308">recently</ta>
            <ta e="T310" id="Seg_3021" s="T309">can-CVB.SEQ</ta>
            <ta e="T311" id="Seg_3022" s="T310">run.a.race-EP-NEG-PST1-3PL</ta>
            <ta e="T312" id="Seg_3023" s="T311">that</ta>
            <ta e="T313" id="Seg_3024" s="T312">boy-PL-EP-1SG.[NOM]</ta>
            <ta e="T314" id="Seg_3025" s="T313">deceased-DAT/LOC</ta>
            <ta e="T315" id="Seg_3026" s="T314">go-TEMP-3PL</ta>
            <ta e="T316" id="Seg_3027" s="T315">spirit-VBZ-FUT-3PL</ta>
            <ta e="T317" id="Seg_3028" s="T316">EMPH</ta>
            <ta e="T318" id="Seg_3029" s="T317">so</ta>
            <ta e="T319" id="Seg_3030" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_3031" s="T319">many-ACC</ta>
            <ta e="T321" id="Seg_3032" s="T320">drink-EP-CAUS.[CAUS]-NEG-3PL</ta>
            <ta e="T322" id="Seg_3033" s="T321">three</ta>
            <ta e="T323" id="Seg_3034" s="T322">just</ta>
            <ta e="T324" id="Seg_3035" s="T323">shot.glass-ACC</ta>
            <ta e="T325" id="Seg_3036" s="T324">drink-EP-CAUS-CAUS-HAB-1PL</ta>
            <ta e="T326" id="Seg_3037" s="T325">1PL.[NOM]</ta>
            <ta e="T327" id="Seg_3038" s="T326">deceased-DAT/LOC</ta>
            <ta e="T328" id="Seg_3039" s="T327">last-3SG.[NOM]</ta>
            <ta e="T329" id="Seg_3040" s="T328">more</ta>
            <ta e="T330" id="Seg_3041" s="T329">spirit.[NOM]</ta>
            <ta e="T331" id="Seg_3042" s="T330">give-NEG-3PL</ta>
            <ta e="T332" id="Seg_3043" s="T331">stop-PRS-3PL</ta>
            <ta e="T333" id="Seg_3044" s="T332">so</ta>
            <ta e="T334" id="Seg_3045" s="T333">accompany-NMNZ-DAT/LOC</ta>
            <ta e="T341" id="Seg_3046" s="T339">that</ta>
            <ta e="T342" id="Seg_3047" s="T341">old.woman-ACC</ta>
            <ta e="T343" id="Seg_3048" s="T342">Q</ta>
            <ta e="T344" id="Seg_3049" s="T343">know-PRS-1SG</ta>
            <ta e="T345" id="Seg_3050" s="T344">1SG.[NOM]</ta>
            <ta e="T346" id="Seg_3051" s="T345">old.man-EP-1SG.[NOM]</ta>
            <ta e="T347" id="Seg_3052" s="T346">elder.brother-3SG-GEN</ta>
            <ta e="T348" id="Seg_3053" s="T347">woman-3SG.[NOM]</ta>
            <ta e="T349" id="Seg_3054" s="T348">brother-3SG-GEN</ta>
            <ta e="T350" id="Seg_3055" s="T349">woman-3SG.[NOM]</ta>
            <ta e="T351" id="Seg_3056" s="T350">cousin-3SG-GEN</ta>
            <ta e="T352" id="Seg_3057" s="T351">brother-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_3058" s="T352">be-PST1-3SG</ta>
            <ta e="T354" id="Seg_3059" s="T353">very</ta>
            <ta e="T355" id="Seg_3060" s="T354">know-PRS-1SG</ta>
            <ta e="T356" id="Seg_3061" s="T355">together</ta>
            <ta e="T357" id="Seg_3062" s="T356">go-PTCP.HAB</ta>
            <ta e="T358" id="Seg_3063" s="T357">be-PST1-1PL</ta>
            <ta e="T359" id="Seg_3064" s="T358">now</ta>
            <ta e="T360" id="Seg_3065" s="T359">EMPH</ta>
            <ta e="T361" id="Seg_3066" s="T360">can-CVB.SEQ</ta>
            <ta e="T362" id="Seg_3067" s="T361">go-NEG-1SG</ta>
            <ta e="T363" id="Seg_3068" s="T362">lie-PRS-1SG</ta>
            <ta e="T364" id="Seg_3069" s="T363">there</ta>
            <ta e="T365" id="Seg_3070" s="T364">lie-PTCP.FUT</ta>
            <ta e="T366" id="Seg_3071" s="T365">be-PST1-1SG</ta>
            <ta e="T367" id="Seg_3072" s="T366">3SG-DAT/LOC</ta>
            <ta e="T368" id="Seg_3073" s="T367">child-1SG.[NOM]</ta>
            <ta e="T369" id="Seg_3074" s="T368">EMPH</ta>
            <ta e="T370" id="Seg_3075" s="T369">come-CVB.SEQ</ta>
            <ta e="T371" id="Seg_3076" s="T370">child-1SG.[NOM]</ta>
            <ta e="T372" id="Seg_3077" s="T371">come-NEG-TEMP-3SG</ta>
            <ta e="T373" id="Seg_3078" s="T372">EMPH</ta>
            <ta e="T374" id="Seg_3079" s="T373">go-PTCP.FUT</ta>
            <ta e="T375" id="Seg_3080" s="T374">be-PST1-1SG</ta>
            <ta e="T376" id="Seg_3081" s="T375">many</ta>
            <ta e="T381" id="Seg_3082" s="T380">AFFIRM</ta>
            <ta e="T382" id="Seg_3083" s="T381">AFFIRM</ta>
            <ta e="T383" id="Seg_3084" s="T382">yesterday</ta>
            <ta e="T384" id="Seg_3085" s="T383">today</ta>
            <ta e="T385" id="Seg_3086" s="T384">tomorrow</ta>
            <ta e="T386" id="Seg_3087" s="T385">go.out-PRS.[3SG]</ta>
            <ta e="T387" id="Seg_3088" s="T386">three-ORD</ta>
            <ta e="T388" id="Seg_3089" s="T387">day-3SG-DAT/LOC</ta>
            <ta e="T389" id="Seg_3090" s="T388">go.out-HAB.[3SG]</ta>
            <ta e="T391" id="Seg_3091" s="T390">well</ta>
            <ta e="T392" id="Seg_3092" s="T391">then</ta>
            <ta e="T393" id="Seg_3093" s="T392">last-3SG.[NOM]</ta>
            <ta e="T394" id="Seg_3094" s="T393">MOD</ta>
            <ta e="T395" id="Seg_3095" s="T394">ask-PTCP.PRS.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KiPP">
            <ta e="T2" id="Seg_3096" s="T1">aufhören-CVB.SEQ</ta>
            <ta e="T3" id="Seg_3097" s="T2">sein-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_3098" s="T3">MOD</ta>
            <ta e="T5" id="Seg_3099" s="T4">1PL.[NOM]</ta>
            <ta e="T6" id="Seg_3100" s="T5">es.gibt-1PL</ta>
            <ta e="T7" id="Seg_3101" s="T6">solange</ta>
            <ta e="T8" id="Seg_3102" s="T7">es.gibt</ta>
            <ta e="T9" id="Seg_3103" s="T8">jetzt</ta>
            <ta e="T10" id="Seg_3104" s="T9">1PL.[NOM]</ta>
            <ta e="T11" id="Seg_3105" s="T10">aufhören-TEMP-1PL</ta>
            <ta e="T12" id="Seg_3106" s="T11">auch</ta>
            <ta e="T13" id="Seg_3107" s="T12">anders.[NOM]</ta>
            <ta e="T14" id="Seg_3108" s="T13">sein-FUT-3PL</ta>
            <ta e="T15" id="Seg_3109" s="T14">3PL.[NOM]</ta>
            <ta e="T16" id="Seg_3110" s="T15">1PL.[NOM]</ta>
            <ta e="T17" id="Seg_3111" s="T16">jetzt</ta>
            <ta e="T18" id="Seg_3112" s="T17">jenes-DAT/LOC</ta>
            <ta e="T19" id="Seg_3113" s="T18">leben-CVB.SIM</ta>
            <ta e="T20" id="Seg_3114" s="T19">versuchen-PRS-1PL</ta>
            <ta e="T21" id="Seg_3115" s="T20">EMPH</ta>
            <ta e="T22" id="Seg_3116" s="T21">jetzt</ta>
            <ta e="T23" id="Seg_3117" s="T22">Jugend-PL-EP-2SG.[NOM]</ta>
            <ta e="T24" id="Seg_3118" s="T23">hören-FUT-3PL</ta>
            <ta e="T25" id="Seg_3119" s="T24">EMPH</ta>
            <ta e="T26" id="Seg_3120" s="T25">anders</ta>
            <ta e="T27" id="Seg_3121" s="T26">Ort-DAT/LOC</ta>
            <ta e="T28" id="Seg_3122" s="T27">gehen-CVB.SEQ</ta>
            <ta e="T29" id="Seg_3123" s="T28">gehen-PRS-3PL</ta>
            <ta e="T30" id="Seg_3124" s="T29">verschieden.[NOM]</ta>
            <ta e="T31" id="Seg_3125" s="T30">sein-PRS-3PL</ta>
            <ta e="T32" id="Seg_3126" s="T31">Gebrauch-PL.[NOM]</ta>
            <ta e="T33" id="Seg_3127" s="T32">so</ta>
            <ta e="T34" id="Seg_3128" s="T33">EMPH</ta>
            <ta e="T35" id="Seg_3129" s="T34">1PL.[NOM]</ta>
            <ta e="T36" id="Seg_3130" s="T35">jenes</ta>
            <ta e="T37" id="Seg_3131" s="T36">alt-PL.[NOM]</ta>
            <ta e="T38" id="Seg_3132" s="T37">Gebrauch-3PL-INSTR</ta>
            <ta e="T39" id="Seg_3133" s="T38">leben-PTCP.PRS</ta>
            <ta e="T40" id="Seg_3134" s="T39">sein-PST1-1PL</ta>
            <ta e="T41" id="Seg_3135" s="T40">hier</ta>
            <ta e="T42" id="Seg_3136" s="T41">Gast.[NOM]</ta>
            <ta e="T43" id="Seg_3137" s="T42">kommen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T44" id="Seg_3138" s="T43">empfangen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T45" id="Seg_3139" s="T44">fern-ABL</ta>
            <ta e="T46" id="Seg_3140" s="T45">Gast.[NOM]</ta>
            <ta e="T47" id="Seg_3141" s="T46">kommen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T48" id="Seg_3142" s="T47">gut</ta>
            <ta e="T49" id="Seg_3143" s="T48">Nahrung-PART</ta>
            <ta e="T50" id="Seg_3144" s="T49">essen-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T51" id="Seg_3145" s="T50">gut.[NOM]</ta>
            <ta e="T52" id="Seg_3146" s="T51">Nahrung-ACC</ta>
            <ta e="T53" id="Seg_3147" s="T52">essen-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T54" id="Seg_3148" s="T53">sagen-PTCP.HAB</ta>
            <ta e="T55" id="Seg_3149" s="T54">sein-PST1-1PL</ta>
            <ta e="T56" id="Seg_3150" s="T55">dann</ta>
            <ta e="T57" id="Seg_3151" s="T56">Gast-PL.[NOM]</ta>
            <ta e="T58" id="Seg_3152" s="T57">kommen-TEMP-3PL</ta>
            <ta e="T59" id="Seg_3153" s="T58">Schnaps-INSTR</ta>
            <ta e="T60" id="Seg_3154" s="T59">gehen-EP-CAUS-CAUS-PRS-1PL</ta>
            <ta e="T61" id="Seg_3155" s="T60">wenig-ADVZ</ta>
            <ta e="T62" id="Seg_3156" s="T61">3PL.[NOM]</ta>
            <ta e="T63" id="Seg_3157" s="T62">ähnlich</ta>
            <ta e="T64" id="Seg_3158" s="T63">dieses.[NOM]</ta>
            <ta e="T65" id="Seg_3159" s="T64">trinken-NEG-1PL</ta>
            <ta e="T66" id="Seg_3160" s="T65">1PL.[NOM]</ta>
            <ta e="T67" id="Seg_3161" s="T66">drei</ta>
            <ta e="T68" id="Seg_3162" s="T67">Schnapsglas-ACC</ta>
            <ta e="T69" id="Seg_3163" s="T68">gehen-EP-CAUS-CAUS-FUT-1PL</ta>
            <ta e="T70" id="Seg_3164" s="T69">aufhören-EP-CAUS-PRS-1PL</ta>
            <ta e="T71" id="Seg_3165" s="T70">EMPH</ta>
            <ta e="T72" id="Seg_3166" s="T71">früher</ta>
            <ta e="T73" id="Seg_3167" s="T72">Mensch.[NOM]</ta>
            <ta e="T74" id="Seg_3168" s="T73">betrunken-DAT/LOC</ta>
            <ta e="T75" id="Seg_3169" s="T74">bis.zu</ta>
            <ta e="T78" id="Seg_3170" s="T77">schlecht.[NOM]</ta>
            <ta e="T93" id="Seg_3171" s="T92">Tag.[NOM]</ta>
            <ta e="T95" id="Seg_3172" s="T93">jeder</ta>
            <ta e="T96" id="Seg_3173" s="T95">trinken-PRS-3PL</ta>
            <ta e="T97" id="Seg_3174" s="T96">AFFIRM</ta>
            <ta e="T102" id="Seg_3175" s="T100">Tag.[NOM]</ta>
            <ta e="T103" id="Seg_3176" s="T102">jeder</ta>
            <ta e="T104" id="Seg_3177" s="T103">trinken-PRS-3PL</ta>
            <ta e="T105" id="Seg_3178" s="T104">anders</ta>
            <ta e="T106" id="Seg_3179" s="T105">Ort-ABL</ta>
            <ta e="T107" id="Seg_3180" s="T106">Schnaps-ACC</ta>
            <ta e="T108" id="Seg_3181" s="T107">bringen-EP-MED-CVB.SIM-bringen-EP-MED-CVB.SIM</ta>
            <ta e="T109" id="Seg_3182" s="T108">1PL-DAT/LOC</ta>
            <ta e="T110" id="Seg_3183" s="T109">NEG.EX</ta>
            <ta e="T111" id="Seg_3184" s="T110">dieses</ta>
            <ta e="T112" id="Seg_3185" s="T111">Ort-DAT/LOC</ta>
            <ta e="T113" id="Seg_3186" s="T112">Schnaps.[NOM]</ta>
            <ta e="T114" id="Seg_3187" s="T113">1PL.[NOM]</ta>
            <ta e="T115" id="Seg_3188" s="T114">alt-PL.[NOM]</ta>
            <ta e="T116" id="Seg_3189" s="T115">es.gibt-1PL</ta>
            <ta e="T117" id="Seg_3190" s="T116">Schnaps-ACC</ta>
            <ta e="T118" id="Seg_3191" s="T117">bringen-CAUS-NEG-1PL</ta>
            <ta e="T119" id="Seg_3192" s="T118">EMPH</ta>
            <ta e="T120" id="Seg_3193" s="T119">Kind-PL.[NOM]</ta>
            <ta e="T121" id="Seg_3194" s="T120">guter.Geist-SIM</ta>
            <ta e="T122" id="Seg_3195" s="T121">essen-NEG-3PL</ta>
            <ta e="T123" id="Seg_3196" s="T122">gut.[NOM]</ta>
            <ta e="T124" id="Seg_3197" s="T123">Volk.[NOM]</ta>
            <ta e="T125" id="Seg_3198" s="T124">sein-PTCP.FUT-3PL-ACC</ta>
            <ta e="T126" id="Seg_3199" s="T125">Mensch.[NOM]</ta>
            <ta e="T127" id="Seg_3200" s="T126">sein-PTCP.FUT-3PL-ACC</ta>
            <ta e="T128" id="Seg_3201" s="T127">Feiertag-DAT/LOC</ta>
            <ta e="T129" id="Seg_3202" s="T128">nur</ta>
            <ta e="T130" id="Seg_3203" s="T129">bringen-CAUS-HAB-1PL</ta>
            <ta e="T131" id="Seg_3204" s="T130">jetzt</ta>
            <ta e="T132" id="Seg_3205" s="T131">dieses.[NOM]</ta>
            <ta e="T133" id="Seg_3206" s="T132">von.dort-von.hier</ta>
            <ta e="T134" id="Seg_3207" s="T133">bringen-PRS-3PL</ta>
            <ta e="T135" id="Seg_3208" s="T134">Ürüng.Xaja-ABL</ta>
            <ta e="T136" id="Seg_3209" s="T135">bringen-PRS-3PL</ta>
            <ta e="T137" id="Seg_3210" s="T136">Novorybnoe-ABL</ta>
            <ta e="T138" id="Seg_3211" s="T137">bringen-PRS-3PL</ta>
            <ta e="T139" id="Seg_3212" s="T138">Chatanga-ABL</ta>
            <ta e="T140" id="Seg_3213" s="T139">holen-CVB.SEQ</ta>
            <ta e="T141" id="Seg_3214" s="T140">kommen-PRS-3PL</ta>
            <ta e="T142" id="Seg_3215" s="T141">jenes-DAT/LOC</ta>
            <ta e="T143" id="Seg_3216" s="T142">sich.betrinken-PRS-3PL</ta>
            <ta e="T144" id="Seg_3217" s="T143">dieses.[NOM]</ta>
            <ta e="T145" id="Seg_3218" s="T144">dieses</ta>
            <ta e="T146" id="Seg_3219" s="T145">Land-DAT/LOC</ta>
            <ta e="T147" id="Seg_3220" s="T146">Schnaps.[NOM]</ta>
            <ta e="T148" id="Seg_3221" s="T147">NEG.EX</ta>
            <ta e="T149" id="Seg_3222" s="T148">trinken-EP-CAUS.[CAUS]-NEG-1PL</ta>
            <ta e="T150" id="Seg_3223" s="T149">Schnaps-ACC</ta>
            <ta e="T157" id="Seg_3224" s="T156">gleichwohl</ta>
            <ta e="T158" id="Seg_3225" s="T157">gehen-FUT-3PL</ta>
            <ta e="T159" id="Seg_3226" s="T158">ein.kleines.Bisschen</ta>
            <ta e="T160" id="Seg_3227" s="T159">angetrunken.[NOM]</ta>
            <ta e="T161" id="Seg_3228" s="T160">sein-FUT-3PL</ta>
            <ta e="T162" id="Seg_3229" s="T161">sogar</ta>
            <ta e="T163" id="Seg_3230" s="T162">mutig-ADVZ</ta>
            <ta e="T164" id="Seg_3231" s="T163">gehen-FUT-3PL</ta>
            <ta e="T165" id="Seg_3232" s="T164">Schnaps.[NOM]</ta>
            <ta e="T166" id="Seg_3233" s="T165">NEG.EX</ta>
            <ta e="T167" id="Seg_3234" s="T166">EMPH</ta>
            <ta e="T168" id="Seg_3235" s="T167">gehen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T169" id="Seg_3236" s="T168">gerade</ta>
            <ta e="T170" id="Seg_3237" s="T169">EMPH</ta>
            <ta e="T171" id="Seg_3238" s="T170">Wetter-1PL.[NOM]</ta>
            <ta e="T172" id="Seg_3239" s="T171">schlecht.[NOM]</ta>
            <ta e="T173" id="Seg_3240" s="T172">wechseln-VBZ-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_3241" s="T179">Schnaps-INSTR</ta>
            <ta e="T181" id="Seg_3242" s="T180">Rentier-ACC</ta>
            <ta e="T182" id="Seg_3243" s="T181">essen-EP-CAUS-NEG-3PL</ta>
            <ta e="T183" id="Seg_3244" s="T182">Erde-ABL</ta>
            <ta e="T184" id="Seg_3245" s="T183">nur</ta>
            <ta e="T185" id="Seg_3246" s="T184">essen-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_3247" s="T185">Rentier-2SG.[NOM]</ta>
            <ta e="T187" id="Seg_3248" s="T186">Erde-ABL</ta>
            <ta e="T188" id="Seg_3249" s="T187">essen-PRS.[3SG]</ta>
            <ta e="T189" id="Seg_3250" s="T188">Rentier.[NOM]</ta>
            <ta e="T190" id="Seg_3251" s="T189">um.die.Wette.laufen-NMNZ-PL-DAT/LOC</ta>
            <ta e="T191" id="Seg_3252" s="T190">EMPH</ta>
            <ta e="T192" id="Seg_3253" s="T191">gut.[NOM]</ta>
            <ta e="T193" id="Seg_3254" s="T192">sein-FUT-3PL</ta>
            <ta e="T194" id="Seg_3255" s="T193">EMPH</ta>
            <ta e="T195" id="Seg_3256" s="T194">so</ta>
            <ta e="T196" id="Seg_3257" s="T195">EMPH</ta>
            <ta e="T197" id="Seg_3258" s="T196">jetzt</ta>
            <ta e="T198" id="Seg_3259" s="T197">was.[NOM]</ta>
            <ta e="T199" id="Seg_3260" s="T198">Schnaps-3SG-ACC</ta>
            <ta e="T200" id="Seg_3261" s="T199">sein-FUT-3PL=Q</ta>
            <ta e="T201" id="Seg_3262" s="T200">Schnaps-3PL.[NOM]</ta>
            <ta e="T202" id="Seg_3263" s="T201">aufhören-FUT-3SG</ta>
            <ta e="T203" id="Seg_3264" s="T202">verletzen-PASS-PTCP.PST</ta>
            <ta e="T204" id="Seg_3265" s="T203">Mensch-PL.[NOM]</ta>
            <ta e="T205" id="Seg_3266" s="T204">können-CVB.SEQ</ta>
            <ta e="T206" id="Seg_3267" s="T205">gehen-FUT-3PL</ta>
            <ta e="T207" id="Seg_3268" s="T206">NEG-3SG</ta>
            <ta e="T208" id="Seg_3269" s="T207">können-CVB.SEQ</ta>
            <ta e="T209" id="Seg_3270" s="T208">spielen-FUT-3PL</ta>
            <ta e="T210" id="Seg_3271" s="T209">NEG</ta>
            <ta e="T211" id="Seg_3272" s="T210">so</ta>
            <ta e="T212" id="Seg_3273" s="T211">EMPH</ta>
            <ta e="T213" id="Seg_3274" s="T212">anderer.von.zwei</ta>
            <ta e="T214" id="Seg_3275" s="T213">Mensch-2SG.[NOM]</ta>
            <ta e="T215" id="Seg_3276" s="T214">jetzt</ta>
            <ta e="T216" id="Seg_3277" s="T215">trinken</ta>
            <ta e="T217" id="Seg_3278" s="T216">um.die.Wette.laufen-PTCP.PRS</ta>
            <ta e="T218" id="Seg_3279" s="T217">Mensch-PL-EP-2SG.[NOM]</ta>
            <ta e="T219" id="Seg_3280" s="T218">gehen-NEG-3PL</ta>
            <ta e="T220" id="Seg_3281" s="T219">EMPH</ta>
            <ta e="T221" id="Seg_3282" s="T220">Rentier.[NOM]</ta>
            <ta e="T222" id="Seg_3283" s="T221">hineingehen-ITER-EP-RECP/COLL-CAUS-FUT-3PL</ta>
            <ta e="T223" id="Seg_3284" s="T222">jetzt</ta>
            <ta e="T224" id="Seg_3285" s="T223">um.die.Wette.laufen-NMNZ-DAT/LOC</ta>
            <ta e="T225" id="Seg_3286" s="T224">um.die.Wette.laufen-PTCP.PRS</ta>
            <ta e="T226" id="Seg_3287" s="T225">Rentier.[NOM]</ta>
            <ta e="T227" id="Seg_3288" s="T226">so</ta>
            <ta e="T228" id="Seg_3289" s="T227">bringen-CVB.SEQ-3PL</ta>
            <ta e="T229" id="Seg_3290" s="T228">hierher</ta>
            <ta e="T230" id="Seg_3291" s="T229">Rentier-VBZ-FUT-3PL</ta>
            <ta e="T231" id="Seg_3292" s="T230">jenes</ta>
            <ta e="T232" id="Seg_3293" s="T231">Rentier-ACC</ta>
            <ta e="T233" id="Seg_3294" s="T232">jenes</ta>
            <ta e="T234" id="Seg_3295" s="T233">nun</ta>
            <ta e="T235" id="Seg_3296" s="T234">dieses</ta>
            <ta e="T236" id="Seg_3297" s="T235">Hund-3PL.[NOM]</ta>
            <ta e="T237" id="Seg_3298" s="T236">viel-3SG</ta>
            <ta e="T238" id="Seg_3299" s="T237">sehr</ta>
            <ta e="T239" id="Seg_3300" s="T238">dieses</ta>
            <ta e="T240" id="Seg_3301" s="T239">Hund-3PL.[NOM]</ta>
            <ta e="T241" id="Seg_3302" s="T240">vermögen-CAUS-NEG-3PL</ta>
            <ta e="T242" id="Seg_3303" s="T241">können-CVB.SEQ</ta>
            <ta e="T243" id="Seg_3304" s="T242">Rentier-VBZ-EP-CAUS-FUT-3PL</ta>
            <ta e="T244" id="Seg_3305" s="T243">NEG-3SG</ta>
            <ta e="T245" id="Seg_3306" s="T244">jenes</ta>
            <ta e="T246" id="Seg_3307" s="T245">Guba-DAT/LOC</ta>
            <ta e="T247" id="Seg_3308" s="T246">um.die.Wette.laufen-HAB-3PL</ta>
            <ta e="T248" id="Seg_3309" s="T247">dieses.[NOM]</ta>
            <ta e="T249" id="Seg_3310" s="T248">zu</ta>
            <ta e="T250" id="Seg_3311" s="T249">gehen-HAB-3PL</ta>
            <ta e="T251" id="Seg_3312" s="T250">spielen-CVB.SIM</ta>
            <ta e="T252" id="Seg_3313" s="T251">jetzt</ta>
            <ta e="T253" id="Seg_3314" s="T252">dort</ta>
            <ta e="T255" id="Seg_3315" s="T253">Zelt.[NOM]</ta>
            <ta e="T258" id="Seg_3316" s="T257">AFFIRM</ta>
            <ta e="T259" id="Seg_3317" s="T258">jetzt</ta>
            <ta e="T260" id="Seg_3318" s="T259">Zelt.[NOM]</ta>
            <ta e="T261" id="Seg_3319" s="T260">bauen-FUT-3PL</ta>
            <ta e="T262" id="Seg_3320" s="T261">dort</ta>
            <ta e="T263" id="Seg_3321" s="T262">Balok-PL-ACC</ta>
            <ta e="T264" id="Seg_3322" s="T263">mitnehmen-PRS-3PL</ta>
            <ta e="T265" id="Seg_3323" s="T264">heute</ta>
            <ta e="T266" id="Seg_3324" s="T265">Zeichen-PL-ACC</ta>
            <ta e="T267" id="Seg_3325" s="T266">machen-EP-PRS-3PL</ta>
            <ta e="T268" id="Seg_3326" s="T267">heute</ta>
            <ta e="T269" id="Seg_3327" s="T268">vorbereiten-EP-REFL-PTCP.PRS</ta>
            <ta e="T270" id="Seg_3328" s="T269">Tag-3PL.[NOM]</ta>
            <ta e="T271" id="Seg_3329" s="T270">Bühne.[NOM]</ta>
            <ta e="T272" id="Seg_3330" s="T271">machen-PRS-3PL</ta>
            <ta e="T273" id="Seg_3331" s="T272">dort</ta>
            <ta e="T274" id="Seg_3332" s="T273">Fass-PL-EP-INSTR</ta>
            <ta e="T275" id="Seg_3333" s="T274">jenes</ta>
            <ta e="T276" id="Seg_3334" s="T275">Ort-DAT/LOC</ta>
            <ta e="T277" id="Seg_3335" s="T276">dort</ta>
            <ta e="T278" id="Seg_3336" s="T277">dort</ta>
            <ta e="T279" id="Seg_3337" s="T278">Neuigkeit.[NOM]</ta>
            <ta e="T280" id="Seg_3338" s="T279">erzählen-FUT-3PL</ta>
            <ta e="T281" id="Seg_3339" s="T280">singen-FUT-3PL</ta>
            <ta e="T282" id="Seg_3340" s="T281">EMPH</ta>
            <ta e="T283" id="Seg_3341" s="T282">wer.[NOM]</ta>
            <ta e="T284" id="Seg_3342" s="T283">singen-PRS.[3SG]</ta>
            <ta e="T285" id="Seg_3343" s="T284">solch-PL.[NOM]</ta>
            <ta e="T286" id="Seg_3344" s="T285">es.gibt-3PL</ta>
            <ta e="T303" id="Seg_3345" s="T302">Toter-DAT/LOC</ta>
            <ta e="T304" id="Seg_3346" s="T303">gehen-FUT-1SG</ta>
            <ta e="T305" id="Seg_3347" s="T304">morgen</ta>
            <ta e="T306" id="Seg_3348" s="T305">herausnehmen-FUT-3PL</ta>
            <ta e="T307" id="Seg_3349" s="T306">EMPH</ta>
            <ta e="T308" id="Seg_3350" s="T307">Alte-ACC</ta>
            <ta e="T309" id="Seg_3351" s="T308">vor.Kurzem</ta>
            <ta e="T310" id="Seg_3352" s="T309">können-CVB.SEQ</ta>
            <ta e="T311" id="Seg_3353" s="T310">um.die.Wette.laufen-EP-NEG-PST1-3PL</ta>
            <ta e="T312" id="Seg_3354" s="T311">jenes</ta>
            <ta e="T313" id="Seg_3355" s="T312">Junge-PL-EP-1SG.[NOM]</ta>
            <ta e="T314" id="Seg_3356" s="T313">Toter-DAT/LOC</ta>
            <ta e="T315" id="Seg_3357" s="T314">gehen-TEMP-3PL</ta>
            <ta e="T316" id="Seg_3358" s="T315">Schnaps-VBZ-FUT-3PL</ta>
            <ta e="T317" id="Seg_3359" s="T316">EMPH</ta>
            <ta e="T318" id="Seg_3360" s="T317">so</ta>
            <ta e="T319" id="Seg_3361" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_3362" s="T319">viel-ACC</ta>
            <ta e="T321" id="Seg_3363" s="T320">trinken-EP-CAUS.[CAUS]-NEG-3PL</ta>
            <ta e="T322" id="Seg_3364" s="T321">drei</ta>
            <ta e="T323" id="Seg_3365" s="T322">nur</ta>
            <ta e="T324" id="Seg_3366" s="T323">Schnapsglas-ACC</ta>
            <ta e="T325" id="Seg_3367" s="T324">trinken-EP-CAUS-CAUS-HAB-1PL</ta>
            <ta e="T326" id="Seg_3368" s="T325">1PL.[NOM]</ta>
            <ta e="T327" id="Seg_3369" s="T326">Toter-DAT/LOC</ta>
            <ta e="T328" id="Seg_3370" s="T327">letzter-3SG.[NOM]</ta>
            <ta e="T329" id="Seg_3371" s="T328">mehr</ta>
            <ta e="T330" id="Seg_3372" s="T329">Schnaps.[NOM]</ta>
            <ta e="T331" id="Seg_3373" s="T330">geben-NEG-3PL</ta>
            <ta e="T332" id="Seg_3374" s="T331">aufhören-PRS-3PL</ta>
            <ta e="T333" id="Seg_3375" s="T332">so</ta>
            <ta e="T334" id="Seg_3376" s="T333">begleiten-NMNZ-DAT/LOC</ta>
            <ta e="T341" id="Seg_3377" s="T339">jenes</ta>
            <ta e="T342" id="Seg_3378" s="T341">Alte-ACC</ta>
            <ta e="T343" id="Seg_3379" s="T342">Q</ta>
            <ta e="T344" id="Seg_3380" s="T343">wissen-PRS-1SG</ta>
            <ta e="T345" id="Seg_3381" s="T344">1SG.[NOM]</ta>
            <ta e="T346" id="Seg_3382" s="T345">alter.Mann-EP-1SG.[NOM]</ta>
            <ta e="T347" id="Seg_3383" s="T346">älterer.Bruder-3SG-GEN</ta>
            <ta e="T348" id="Seg_3384" s="T347">Frau-3SG.[NOM]</ta>
            <ta e="T349" id="Seg_3385" s="T348">Bruder-3SG-GEN</ta>
            <ta e="T350" id="Seg_3386" s="T349">Frau-3SG.[NOM]</ta>
            <ta e="T351" id="Seg_3387" s="T350">Cousin-3SG-GEN</ta>
            <ta e="T352" id="Seg_3388" s="T351">Bruder-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_3389" s="T352">sein-PST1-3SG</ta>
            <ta e="T354" id="Seg_3390" s="T353">sehr</ta>
            <ta e="T355" id="Seg_3391" s="T354">wissen-PRS-1SG</ta>
            <ta e="T356" id="Seg_3392" s="T355">zusammen</ta>
            <ta e="T357" id="Seg_3393" s="T356">gehen-PTCP.HAB</ta>
            <ta e="T358" id="Seg_3394" s="T357">sein-PST1-1PL</ta>
            <ta e="T359" id="Seg_3395" s="T358">jetzt</ta>
            <ta e="T360" id="Seg_3396" s="T359">EMPH</ta>
            <ta e="T361" id="Seg_3397" s="T360">können-CVB.SEQ</ta>
            <ta e="T362" id="Seg_3398" s="T361">gehen-NEG-1SG</ta>
            <ta e="T363" id="Seg_3399" s="T362">liegen-PRS-1SG</ta>
            <ta e="T364" id="Seg_3400" s="T363">dort</ta>
            <ta e="T365" id="Seg_3401" s="T364">liegen-PTCP.FUT</ta>
            <ta e="T366" id="Seg_3402" s="T365">sein-PST1-1SG</ta>
            <ta e="T367" id="Seg_3403" s="T366">3SG-DAT/LOC</ta>
            <ta e="T368" id="Seg_3404" s="T367">Kind-1SG.[NOM]</ta>
            <ta e="T369" id="Seg_3405" s="T368">EMPH</ta>
            <ta e="T370" id="Seg_3406" s="T369">kommen-CVB.SEQ</ta>
            <ta e="T371" id="Seg_3407" s="T370">Kind-1SG.[NOM]</ta>
            <ta e="T372" id="Seg_3408" s="T371">kommen-NEG-TEMP-3SG</ta>
            <ta e="T373" id="Seg_3409" s="T372">EMPH</ta>
            <ta e="T374" id="Seg_3410" s="T373">gehen-PTCP.FUT</ta>
            <ta e="T375" id="Seg_3411" s="T374">sein-PST1-1SG</ta>
            <ta e="T376" id="Seg_3412" s="T375">viel</ta>
            <ta e="T381" id="Seg_3413" s="T380">AFFIRM</ta>
            <ta e="T382" id="Seg_3414" s="T381">AFFIRM</ta>
            <ta e="T383" id="Seg_3415" s="T382">gesteren</ta>
            <ta e="T384" id="Seg_3416" s="T383">heute</ta>
            <ta e="T385" id="Seg_3417" s="T384">morgen</ta>
            <ta e="T386" id="Seg_3418" s="T385">hinausgehen-PRS.[3SG]</ta>
            <ta e="T387" id="Seg_3419" s="T386">drei-ORD</ta>
            <ta e="T388" id="Seg_3420" s="T387">Tag-3SG-DAT/LOC</ta>
            <ta e="T389" id="Seg_3421" s="T388">hinausgehen-HAB.[3SG]</ta>
            <ta e="T391" id="Seg_3422" s="T390">doch</ta>
            <ta e="T392" id="Seg_3423" s="T391">dann</ta>
            <ta e="T393" id="Seg_3424" s="T392">letzter-3SG.[NOM]</ta>
            <ta e="T394" id="Seg_3425" s="T393">MOD</ta>
            <ta e="T395" id="Seg_3426" s="T394">fragen-PTCP.PRS.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KiPP">
            <ta e="T2" id="Seg_3427" s="T1">кончать-CVB.SEQ</ta>
            <ta e="T3" id="Seg_3428" s="T2">быть-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_3429" s="T3">MOD</ta>
            <ta e="T5" id="Seg_3430" s="T4">1PL.[NOM]</ta>
            <ta e="T6" id="Seg_3431" s="T5">есть-1PL</ta>
            <ta e="T7" id="Seg_3432" s="T6">так.долго</ta>
            <ta e="T8" id="Seg_3433" s="T7">есть</ta>
            <ta e="T9" id="Seg_3434" s="T8">теперь</ta>
            <ta e="T10" id="Seg_3435" s="T9">1PL.[NOM]</ta>
            <ta e="T11" id="Seg_3436" s="T10">кончать-TEMP-1PL</ta>
            <ta e="T12" id="Seg_3437" s="T11">тоже</ta>
            <ta e="T13" id="Seg_3438" s="T12">другой.[NOM]</ta>
            <ta e="T14" id="Seg_3439" s="T13">быть-FUT-3PL</ta>
            <ta e="T15" id="Seg_3440" s="T14">3PL.[NOM]</ta>
            <ta e="T16" id="Seg_3441" s="T15">1PL.[NOM]</ta>
            <ta e="T17" id="Seg_3442" s="T16">теперь</ta>
            <ta e="T18" id="Seg_3443" s="T17">тот-DAT/LOC</ta>
            <ta e="T19" id="Seg_3444" s="T18">жить-CVB.SIM</ta>
            <ta e="T20" id="Seg_3445" s="T19">пытаться-PRS-1PL</ta>
            <ta e="T21" id="Seg_3446" s="T20">EMPH</ta>
            <ta e="T22" id="Seg_3447" s="T21">теперь</ta>
            <ta e="T23" id="Seg_3448" s="T22">молодеж-PL-EP-2SG.[NOM]</ta>
            <ta e="T24" id="Seg_3449" s="T23">слышать-FUT-3PL</ta>
            <ta e="T25" id="Seg_3450" s="T24">EMPH</ta>
            <ta e="T26" id="Seg_3451" s="T25">другой</ta>
            <ta e="T27" id="Seg_3452" s="T26">место-DAT/LOC</ta>
            <ta e="T28" id="Seg_3453" s="T27">идти-CVB.SEQ</ta>
            <ta e="T29" id="Seg_3454" s="T28">идти-PRS-3PL</ta>
            <ta e="T30" id="Seg_3455" s="T29">различный.[NOM]</ta>
            <ta e="T31" id="Seg_3456" s="T30">быть-PRS-3PL</ta>
            <ta e="T32" id="Seg_3457" s="T31">обычай-PL.[NOM]</ta>
            <ta e="T33" id="Seg_3458" s="T32">так</ta>
            <ta e="T34" id="Seg_3459" s="T33">EMPH</ta>
            <ta e="T35" id="Seg_3460" s="T34">1PL.[NOM]</ta>
            <ta e="T36" id="Seg_3461" s="T35">тот</ta>
            <ta e="T37" id="Seg_3462" s="T36">старый-PL.[NOM]</ta>
            <ta e="T38" id="Seg_3463" s="T37">обычай-3PL-INSTR</ta>
            <ta e="T39" id="Seg_3464" s="T38">жить-PTCP.PRS</ta>
            <ta e="T40" id="Seg_3465" s="T39">быть-PST1-1PL</ta>
            <ta e="T41" id="Seg_3466" s="T40">здесь</ta>
            <ta e="T42" id="Seg_3467" s="T41">гость.[NOM]</ta>
            <ta e="T43" id="Seg_3468" s="T42">приходить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T44" id="Seg_3469" s="T43">принимать-PTCP.PRS-DAT/LOC</ta>
            <ta e="T45" id="Seg_3470" s="T44">далекий-ABL</ta>
            <ta e="T46" id="Seg_3471" s="T45">гость.[NOM]</ta>
            <ta e="T47" id="Seg_3472" s="T46">приходить-PTCP.PRS-DAT/LOC</ta>
            <ta e="T48" id="Seg_3473" s="T47">добрый</ta>
            <ta e="T49" id="Seg_3474" s="T48">пища-PART</ta>
            <ta e="T50" id="Seg_3475" s="T49">есть-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T51" id="Seg_3476" s="T50">хороший.[NOM]</ta>
            <ta e="T52" id="Seg_3477" s="T51">пища-ACC</ta>
            <ta e="T53" id="Seg_3478" s="T52">есть-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T54" id="Seg_3479" s="T53">говорить-PTCP.HAB</ta>
            <ta e="T55" id="Seg_3480" s="T54">быть-PST1-1PL</ta>
            <ta e="T56" id="Seg_3481" s="T55">потом</ta>
            <ta e="T57" id="Seg_3482" s="T56">гость-PL.[NOM]</ta>
            <ta e="T58" id="Seg_3483" s="T57">приходить-TEMP-3PL</ta>
            <ta e="T59" id="Seg_3484" s="T58">спиртное-INSTR</ta>
            <ta e="T60" id="Seg_3485" s="T59">идти-EP-CAUS-CAUS-PRS-1PL</ta>
            <ta e="T61" id="Seg_3486" s="T60">мало-ADVZ</ta>
            <ta e="T62" id="Seg_3487" s="T61">3PL.[NOM]</ta>
            <ta e="T63" id="Seg_3488" s="T62">подобно</ta>
            <ta e="T64" id="Seg_3489" s="T63">тот.[NOM]</ta>
            <ta e="T65" id="Seg_3490" s="T64">пить-NEG-1PL</ta>
            <ta e="T66" id="Seg_3491" s="T65">1PL.[NOM]</ta>
            <ta e="T67" id="Seg_3492" s="T66">три</ta>
            <ta e="T68" id="Seg_3493" s="T67">рюмка-ACC</ta>
            <ta e="T69" id="Seg_3494" s="T68">идти-EP-CAUS-CAUS-FUT-1PL</ta>
            <ta e="T70" id="Seg_3495" s="T69">кончать-EP-CAUS-PRS-1PL</ta>
            <ta e="T71" id="Seg_3496" s="T70">EMPH</ta>
            <ta e="T72" id="Seg_3497" s="T71">прежний</ta>
            <ta e="T73" id="Seg_3498" s="T72">человек.[NOM]</ta>
            <ta e="T74" id="Seg_3499" s="T73">пьяный-DAT/LOC</ta>
            <ta e="T75" id="Seg_3500" s="T74">пока</ta>
            <ta e="T78" id="Seg_3501" s="T77">плохой.[NOM]</ta>
            <ta e="T93" id="Seg_3502" s="T92">день.[NOM]</ta>
            <ta e="T95" id="Seg_3503" s="T93">каждый</ta>
            <ta e="T96" id="Seg_3504" s="T95">пить-PRS-3PL</ta>
            <ta e="T97" id="Seg_3505" s="T96">AFFIRM</ta>
            <ta e="T102" id="Seg_3506" s="T100">день.[NOM]</ta>
            <ta e="T103" id="Seg_3507" s="T102">каждый</ta>
            <ta e="T104" id="Seg_3508" s="T103">пить-PRS-3PL</ta>
            <ta e="T105" id="Seg_3509" s="T104">другой</ta>
            <ta e="T106" id="Seg_3510" s="T105">место-ABL</ta>
            <ta e="T107" id="Seg_3511" s="T106">спиртное-ACC</ta>
            <ta e="T108" id="Seg_3512" s="T107">принести-EP-MED-CVB.SIM-принести-EP-MED-CVB.SIM</ta>
            <ta e="T109" id="Seg_3513" s="T108">1PL-DAT/LOC</ta>
            <ta e="T110" id="Seg_3514" s="T109">NEG.EX</ta>
            <ta e="T111" id="Seg_3515" s="T110">этот</ta>
            <ta e="T112" id="Seg_3516" s="T111">место-DAT/LOC</ta>
            <ta e="T113" id="Seg_3517" s="T112">спиртное.[NOM]</ta>
            <ta e="T114" id="Seg_3518" s="T113">1PL.[NOM]</ta>
            <ta e="T115" id="Seg_3519" s="T114">старый-PL.[NOM]</ta>
            <ta e="T116" id="Seg_3520" s="T115">есть-1PL</ta>
            <ta e="T117" id="Seg_3521" s="T116">спиртное-ACC</ta>
            <ta e="T118" id="Seg_3522" s="T117">принести-CAUS-NEG-1PL</ta>
            <ta e="T119" id="Seg_3523" s="T118">EMPH</ta>
            <ta e="T120" id="Seg_3524" s="T119">ребенок-PL.[NOM]</ta>
            <ta e="T121" id="Seg_3525" s="T120">хороший.дух-SIM</ta>
            <ta e="T122" id="Seg_3526" s="T121">есть-NEG-3PL</ta>
            <ta e="T123" id="Seg_3527" s="T122">хороший.[NOM]</ta>
            <ta e="T124" id="Seg_3528" s="T123">народ.[NOM]</ta>
            <ta e="T125" id="Seg_3529" s="T124">быть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T126" id="Seg_3530" s="T125">человек.[NOM]</ta>
            <ta e="T127" id="Seg_3531" s="T126">быть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T128" id="Seg_3532" s="T127">праздник-DAT/LOC</ta>
            <ta e="T129" id="Seg_3533" s="T128">только</ta>
            <ta e="T130" id="Seg_3534" s="T129">принести-CAUS-HAB-1PL</ta>
            <ta e="T131" id="Seg_3535" s="T130">теперь</ta>
            <ta e="T132" id="Seg_3536" s="T131">тот.[NOM]</ta>
            <ta e="T133" id="Seg_3537" s="T132">оттуда-отсюда</ta>
            <ta e="T134" id="Seg_3538" s="T133">принести-PRS-3PL</ta>
            <ta e="T135" id="Seg_3539" s="T134">Юрюнг.Хая-ABL</ta>
            <ta e="T136" id="Seg_3540" s="T135">принести-PRS-3PL</ta>
            <ta e="T137" id="Seg_3541" s="T136">Новорыбное-ABL</ta>
            <ta e="T138" id="Seg_3542" s="T137">принести-PRS-3PL</ta>
            <ta e="T139" id="Seg_3543" s="T138">Хатанга-ABL</ta>
            <ta e="T140" id="Seg_3544" s="T139">приносить-CVB.SEQ</ta>
            <ta e="T141" id="Seg_3545" s="T140">приходить-PRS-3PL</ta>
            <ta e="T142" id="Seg_3546" s="T141">тот-DAT/LOC</ta>
            <ta e="T143" id="Seg_3547" s="T142">напиваться-PRS-3PL</ta>
            <ta e="T144" id="Seg_3548" s="T143">тот.[NOM]</ta>
            <ta e="T145" id="Seg_3549" s="T144">этот</ta>
            <ta e="T146" id="Seg_3550" s="T145">страна-DAT/LOC</ta>
            <ta e="T147" id="Seg_3551" s="T146">спиртное.[NOM]</ta>
            <ta e="T148" id="Seg_3552" s="T147">NEG.EX</ta>
            <ta e="T149" id="Seg_3553" s="T148">пить-EP-CAUS.[CAUS]-NEG-1PL</ta>
            <ta e="T150" id="Seg_3554" s="T149">спиртное-ACC</ta>
            <ta e="T157" id="Seg_3555" s="T156">все.равно</ta>
            <ta e="T158" id="Seg_3556" s="T157">идти-FUT-3PL</ta>
            <ta e="T159" id="Seg_3557" s="T158">немножко</ta>
            <ta e="T160" id="Seg_3558" s="T159">подвыпивший.[NOM]</ta>
            <ta e="T161" id="Seg_3559" s="T160">быть-FUT-3PL</ta>
            <ta e="T162" id="Seg_3560" s="T161">даже</ta>
            <ta e="T163" id="Seg_3561" s="T162">смелый-ADVZ</ta>
            <ta e="T164" id="Seg_3562" s="T163">идти-FUT-3PL</ta>
            <ta e="T165" id="Seg_3563" s="T164">спиртное.[NOM]</ta>
            <ta e="T166" id="Seg_3564" s="T165">NEG.EX</ta>
            <ta e="T167" id="Seg_3565" s="T166">EMPH</ta>
            <ta e="T168" id="Seg_3566" s="T167">идти-PTCP.FUT-3PL-ACC</ta>
            <ta e="T169" id="Seg_3567" s="T168">как.раз</ta>
            <ta e="T170" id="Seg_3568" s="T169">EMPH</ta>
            <ta e="T171" id="Seg_3569" s="T170">погода-1PL.[NOM]</ta>
            <ta e="T172" id="Seg_3570" s="T171">плохой.[NOM]</ta>
            <ta e="T173" id="Seg_3571" s="T172">менять-VBZ-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_3572" s="T179">спиртное-INSTR</ta>
            <ta e="T181" id="Seg_3573" s="T180">олень-ACC</ta>
            <ta e="T182" id="Seg_3574" s="T181">есть-EP-CAUS-NEG-3PL</ta>
            <ta e="T183" id="Seg_3575" s="T182">земля-ABL</ta>
            <ta e="T184" id="Seg_3576" s="T183">только</ta>
            <ta e="T185" id="Seg_3577" s="T184">есть-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_3578" s="T185">олень-2SG.[NOM]</ta>
            <ta e="T187" id="Seg_3579" s="T186">земля-ABL</ta>
            <ta e="T188" id="Seg_3580" s="T187">есть-PRS.[3SG]</ta>
            <ta e="T189" id="Seg_3581" s="T188">олень.[NOM]</ta>
            <ta e="T190" id="Seg_3582" s="T189">бежить.наперегонки-NMNZ-PL-DAT/LOC</ta>
            <ta e="T191" id="Seg_3583" s="T190">EMPH</ta>
            <ta e="T192" id="Seg_3584" s="T191">хороший.[NOM]</ta>
            <ta e="T193" id="Seg_3585" s="T192">быть-FUT-3PL</ta>
            <ta e="T194" id="Seg_3586" s="T193">EMPH</ta>
            <ta e="T195" id="Seg_3587" s="T194">так</ta>
            <ta e="T196" id="Seg_3588" s="T195">EMPH</ta>
            <ta e="T197" id="Seg_3589" s="T196">теперь</ta>
            <ta e="T198" id="Seg_3590" s="T197">что.[NOM]</ta>
            <ta e="T199" id="Seg_3591" s="T198">спиртное-3SG-ACC</ta>
            <ta e="T200" id="Seg_3592" s="T199">быть-FUT-3PL=Q</ta>
            <ta e="T201" id="Seg_3593" s="T200">спиртное-3PL.[NOM]</ta>
            <ta e="T202" id="Seg_3594" s="T201">кончать-FUT-3SG</ta>
            <ta e="T203" id="Seg_3595" s="T202">повредить-PASS-PTCP.PST</ta>
            <ta e="T204" id="Seg_3596" s="T203">человек-PL.[NOM]</ta>
            <ta e="T205" id="Seg_3597" s="T204">мочь-CVB.SEQ</ta>
            <ta e="T206" id="Seg_3598" s="T205">идти-FUT-3PL</ta>
            <ta e="T207" id="Seg_3599" s="T206">NEG-3SG</ta>
            <ta e="T208" id="Seg_3600" s="T207">мочь-CVB.SEQ</ta>
            <ta e="T209" id="Seg_3601" s="T208">играть-FUT-3PL</ta>
            <ta e="T210" id="Seg_3602" s="T209">NEG</ta>
            <ta e="T211" id="Seg_3603" s="T210">так</ta>
            <ta e="T212" id="Seg_3604" s="T211">EMPH</ta>
            <ta e="T213" id="Seg_3605" s="T212">другой.из.двух</ta>
            <ta e="T214" id="Seg_3606" s="T213">человек-2SG.[NOM]</ta>
            <ta e="T215" id="Seg_3607" s="T214">теперь</ta>
            <ta e="T216" id="Seg_3608" s="T215">пить</ta>
            <ta e="T217" id="Seg_3609" s="T216">бежить.наперегонки-PTCP.PRS</ta>
            <ta e="T218" id="Seg_3610" s="T217">человек-PL-EP-2SG.[NOM]</ta>
            <ta e="T219" id="Seg_3611" s="T218">идти-NEG-3PL</ta>
            <ta e="T220" id="Seg_3612" s="T219">EMPH</ta>
            <ta e="T221" id="Seg_3613" s="T220">олень.[NOM]</ta>
            <ta e="T222" id="Seg_3614" s="T221">входить-ITER-EP-RECP/COLL-CAUS-FUT-3PL</ta>
            <ta e="T223" id="Seg_3615" s="T222">теперь</ta>
            <ta e="T224" id="Seg_3616" s="T223">бежить.наперегонки-NMNZ-DAT/LOC</ta>
            <ta e="T225" id="Seg_3617" s="T224">бежить.наперегонки-PTCP.PRS</ta>
            <ta e="T226" id="Seg_3618" s="T225">олень.[NOM]</ta>
            <ta e="T227" id="Seg_3619" s="T226">так</ta>
            <ta e="T228" id="Seg_3620" s="T227">принести-CVB.SEQ-3PL</ta>
            <ta e="T229" id="Seg_3621" s="T228">сюда</ta>
            <ta e="T230" id="Seg_3622" s="T229">олень-VBZ-FUT-3PL</ta>
            <ta e="T231" id="Seg_3623" s="T230">тот</ta>
            <ta e="T232" id="Seg_3624" s="T231">олень-ACC</ta>
            <ta e="T233" id="Seg_3625" s="T232">тот</ta>
            <ta e="T234" id="Seg_3626" s="T233">вот</ta>
            <ta e="T235" id="Seg_3627" s="T234">этот</ta>
            <ta e="T236" id="Seg_3628" s="T235">собака-3PL.[NOM]</ta>
            <ta e="T237" id="Seg_3629" s="T236">много-3SG</ta>
            <ta e="T238" id="Seg_3630" s="T237">очень</ta>
            <ta e="T239" id="Seg_3631" s="T238">этот</ta>
            <ta e="T240" id="Seg_3632" s="T239">собака-3PL.[NOM]</ta>
            <ta e="T241" id="Seg_3633" s="T240">уметь-CAUS-NEG-3PL</ta>
            <ta e="T242" id="Seg_3634" s="T241">мочь-CVB.SEQ</ta>
            <ta e="T243" id="Seg_3635" s="T242">олень-VBZ-EP-CAUS-FUT-3PL</ta>
            <ta e="T244" id="Seg_3636" s="T243">NEG-3SG</ta>
            <ta e="T245" id="Seg_3637" s="T244">тот</ta>
            <ta e="T246" id="Seg_3638" s="T245">Губа-DAT/LOC</ta>
            <ta e="T247" id="Seg_3639" s="T246">бежить.наперегонки-HAB-3PL</ta>
            <ta e="T248" id="Seg_3640" s="T247">тот.[NOM]</ta>
            <ta e="T249" id="Seg_3641" s="T248">к</ta>
            <ta e="T250" id="Seg_3642" s="T249">идти-HAB-3PL</ta>
            <ta e="T251" id="Seg_3643" s="T250">играть-CVB.SIM</ta>
            <ta e="T252" id="Seg_3644" s="T251">теперь</ta>
            <ta e="T253" id="Seg_3645" s="T252">там</ta>
            <ta e="T255" id="Seg_3646" s="T253">чум.[NOM]</ta>
            <ta e="T258" id="Seg_3647" s="T257">AFFIRM</ta>
            <ta e="T259" id="Seg_3648" s="T258">теперь</ta>
            <ta e="T260" id="Seg_3649" s="T259">чум.[NOM]</ta>
            <ta e="T261" id="Seg_3650" s="T260">строить-FUT-3PL</ta>
            <ta e="T262" id="Seg_3651" s="T261">там</ta>
            <ta e="T263" id="Seg_3652" s="T262">балок-PL-ACC</ta>
            <ta e="T264" id="Seg_3653" s="T263">уносить-PRS-3PL</ta>
            <ta e="T265" id="Seg_3654" s="T264">сегодня</ta>
            <ta e="T266" id="Seg_3655" s="T265">отметка-PL-ACC</ta>
            <ta e="T267" id="Seg_3656" s="T266">делать-EP-PRS-3PL</ta>
            <ta e="T268" id="Seg_3657" s="T267">сегодня</ta>
            <ta e="T269" id="Seg_3658" s="T268">готовить-EP-REFL-PTCP.PRS</ta>
            <ta e="T270" id="Seg_3659" s="T269">день-3PL.[NOM]</ta>
            <ta e="T271" id="Seg_3660" s="T270">сцена.[NOM]</ta>
            <ta e="T272" id="Seg_3661" s="T271">делать-PRS-3PL</ta>
            <ta e="T273" id="Seg_3662" s="T272">там</ta>
            <ta e="T274" id="Seg_3663" s="T273">бочка-PL-EP-INSTR</ta>
            <ta e="T275" id="Seg_3664" s="T274">тот</ta>
            <ta e="T276" id="Seg_3665" s="T275">место-DAT/LOC</ta>
            <ta e="T277" id="Seg_3666" s="T276">там</ta>
            <ta e="T278" id="Seg_3667" s="T277">там</ta>
            <ta e="T279" id="Seg_3668" s="T278">новость.[NOM]</ta>
            <ta e="T280" id="Seg_3669" s="T279">рассказывать-FUT-3PL</ta>
            <ta e="T281" id="Seg_3670" s="T280">петь-FUT-3PL</ta>
            <ta e="T282" id="Seg_3671" s="T281">EMPH</ta>
            <ta e="T283" id="Seg_3672" s="T282">кто.[NOM]</ta>
            <ta e="T284" id="Seg_3673" s="T283">петь-PRS.[3SG]</ta>
            <ta e="T285" id="Seg_3674" s="T284">такой-PL.[NOM]</ta>
            <ta e="T286" id="Seg_3675" s="T285">есть-3PL</ta>
            <ta e="T303" id="Seg_3676" s="T302">покойник-DAT/LOC</ta>
            <ta e="T304" id="Seg_3677" s="T303">идти-FUT-1SG</ta>
            <ta e="T305" id="Seg_3678" s="T304">завтра</ta>
            <ta e="T306" id="Seg_3679" s="T305">вынимать-FUT-3PL</ta>
            <ta e="T307" id="Seg_3680" s="T306">EMPH</ta>
            <ta e="T308" id="Seg_3681" s="T307">старуха-ACC</ta>
            <ta e="T309" id="Seg_3682" s="T308">недавно</ta>
            <ta e="T310" id="Seg_3683" s="T309">мочь-CVB.SEQ</ta>
            <ta e="T311" id="Seg_3684" s="T310">бежить.наперегонки-EP-NEG-PST1-3PL</ta>
            <ta e="T312" id="Seg_3685" s="T311">тот</ta>
            <ta e="T313" id="Seg_3686" s="T312">мальчик-PL-EP-1SG.[NOM]</ta>
            <ta e="T314" id="Seg_3687" s="T313">покойник-DAT/LOC</ta>
            <ta e="T315" id="Seg_3688" s="T314">идти-TEMP-3PL</ta>
            <ta e="T316" id="Seg_3689" s="T315">спиртное-VBZ-FUT-3PL</ta>
            <ta e="T317" id="Seg_3690" s="T316">EMPH</ta>
            <ta e="T318" id="Seg_3691" s="T317">так</ta>
            <ta e="T319" id="Seg_3692" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_3693" s="T319">много-ACC</ta>
            <ta e="T321" id="Seg_3694" s="T320">пить-EP-CAUS.[CAUS]-NEG-3PL</ta>
            <ta e="T322" id="Seg_3695" s="T321">три</ta>
            <ta e="T323" id="Seg_3696" s="T322">только</ta>
            <ta e="T324" id="Seg_3697" s="T323">рюмка-ACC</ta>
            <ta e="T325" id="Seg_3698" s="T324">пить-EP-CAUS-CAUS-HAB-1PL</ta>
            <ta e="T326" id="Seg_3699" s="T325">1PL.[NOM]</ta>
            <ta e="T327" id="Seg_3700" s="T326">покойник-DAT/LOC</ta>
            <ta e="T328" id="Seg_3701" s="T327">последний-3SG.[NOM]</ta>
            <ta e="T329" id="Seg_3702" s="T328">больше</ta>
            <ta e="T330" id="Seg_3703" s="T329">спиртное.[NOM]</ta>
            <ta e="T331" id="Seg_3704" s="T330">давать-NEG-3PL</ta>
            <ta e="T332" id="Seg_3705" s="T331">кончать-PRS-3PL</ta>
            <ta e="T333" id="Seg_3706" s="T332">так</ta>
            <ta e="T334" id="Seg_3707" s="T333">провожать-NMNZ-DAT/LOC</ta>
            <ta e="T341" id="Seg_3708" s="T339">тот</ta>
            <ta e="T342" id="Seg_3709" s="T341">старуха-ACC</ta>
            <ta e="T343" id="Seg_3710" s="T342">Q</ta>
            <ta e="T344" id="Seg_3711" s="T343">знать-PRS-1SG</ta>
            <ta e="T345" id="Seg_3712" s="T344">1SG.[NOM]</ta>
            <ta e="T346" id="Seg_3713" s="T345">старик-EP-1SG.[NOM]</ta>
            <ta e="T347" id="Seg_3714" s="T346">старший.брат-3SG-GEN</ta>
            <ta e="T348" id="Seg_3715" s="T347">жена-3SG.[NOM]</ta>
            <ta e="T349" id="Seg_3716" s="T348">брат-3SG-GEN</ta>
            <ta e="T350" id="Seg_3717" s="T349">жена-3SG.[NOM]</ta>
            <ta e="T351" id="Seg_3718" s="T350">двоюродный.брат-3SG-GEN</ta>
            <ta e="T352" id="Seg_3719" s="T351">брат-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_3720" s="T352">быть-PST1-3SG</ta>
            <ta e="T354" id="Seg_3721" s="T353">очень</ta>
            <ta e="T355" id="Seg_3722" s="T354">знать-PRS-1SG</ta>
            <ta e="T356" id="Seg_3723" s="T355">вместе</ta>
            <ta e="T357" id="Seg_3724" s="T356">идти-PTCP.HAB</ta>
            <ta e="T358" id="Seg_3725" s="T357">быть-PST1-1PL</ta>
            <ta e="T359" id="Seg_3726" s="T358">теперь</ta>
            <ta e="T360" id="Seg_3727" s="T359">EMPH</ta>
            <ta e="T361" id="Seg_3728" s="T360">мочь-CVB.SEQ</ta>
            <ta e="T362" id="Seg_3729" s="T361">идти-NEG-1SG</ta>
            <ta e="T363" id="Seg_3730" s="T362">лежать-PRS-1SG</ta>
            <ta e="T364" id="Seg_3731" s="T363">там</ta>
            <ta e="T365" id="Seg_3732" s="T364">лежать-PTCP.FUT</ta>
            <ta e="T366" id="Seg_3733" s="T365">быть-PST1-1SG</ta>
            <ta e="T367" id="Seg_3734" s="T366">3SG-DAT/LOC</ta>
            <ta e="T368" id="Seg_3735" s="T367">ребенок-1SG.[NOM]</ta>
            <ta e="T369" id="Seg_3736" s="T368">EMPH</ta>
            <ta e="T370" id="Seg_3737" s="T369">приходить-CVB.SEQ</ta>
            <ta e="T371" id="Seg_3738" s="T370">ребенок-1SG.[NOM]</ta>
            <ta e="T372" id="Seg_3739" s="T371">приходить-NEG-TEMP-3SG</ta>
            <ta e="T373" id="Seg_3740" s="T372">EMPH</ta>
            <ta e="T374" id="Seg_3741" s="T373">идти-PTCP.FUT</ta>
            <ta e="T375" id="Seg_3742" s="T374">быть-PST1-1SG</ta>
            <ta e="T376" id="Seg_3743" s="T375">много</ta>
            <ta e="T381" id="Seg_3744" s="T380">AFFIRM</ta>
            <ta e="T382" id="Seg_3745" s="T381">AFFIRM</ta>
            <ta e="T383" id="Seg_3746" s="T382">вчера</ta>
            <ta e="T384" id="Seg_3747" s="T383">сегодня</ta>
            <ta e="T385" id="Seg_3748" s="T384">завтра</ta>
            <ta e="T386" id="Seg_3749" s="T385">выйти-PRS.[3SG]</ta>
            <ta e="T387" id="Seg_3750" s="T386">три-ORD</ta>
            <ta e="T388" id="Seg_3751" s="T387">день-3SG-DAT/LOC</ta>
            <ta e="T389" id="Seg_3752" s="T388">выйти-HAB.[3SG]</ta>
            <ta e="T391" id="Seg_3753" s="T390">вот</ta>
            <ta e="T392" id="Seg_3754" s="T391">потом</ta>
            <ta e="T393" id="Seg_3755" s="T392">последний-3SG.[NOM]</ta>
            <ta e="T394" id="Seg_3756" s="T393">MOD</ta>
            <ta e="T395" id="Seg_3757" s="T394">спрашивать-PTCP.PRS.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KiPP">
            <ta e="T2" id="Seg_3758" s="T1">v-v:cvb</ta>
            <ta e="T3" id="Seg_3759" s="T2">v-v:tense.[v:pred.pn]</ta>
            <ta e="T4" id="Seg_3760" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_3761" s="T4">pers.[pro:case]</ta>
            <ta e="T6" id="Seg_3762" s="T5">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T7" id="Seg_3763" s="T6">post</ta>
            <ta e="T8" id="Seg_3764" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_3765" s="T8">adv</ta>
            <ta e="T10" id="Seg_3766" s="T9">pers.[pro:case]</ta>
            <ta e="T11" id="Seg_3767" s="T10">v-v:mood-v:temp.pn</ta>
            <ta e="T12" id="Seg_3768" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_3769" s="T12">adj.[n:case]</ta>
            <ta e="T14" id="Seg_3770" s="T13">v-v:tense-v:poss.pn</ta>
            <ta e="T15" id="Seg_3771" s="T14">pers.[pro:case]</ta>
            <ta e="T16" id="Seg_3772" s="T15">pers.[pro:case]</ta>
            <ta e="T17" id="Seg_3773" s="T16">adv</ta>
            <ta e="T18" id="Seg_3774" s="T17">dempro-pro:case</ta>
            <ta e="T19" id="Seg_3775" s="T18">v-v:cvb</ta>
            <ta e="T20" id="Seg_3776" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_3777" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_3778" s="T21">adv</ta>
            <ta e="T23" id="Seg_3779" s="T22">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T24" id="Seg_3780" s="T23">v-v:tense-v:poss.pn</ta>
            <ta e="T25" id="Seg_3781" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_3782" s="T25">adj</ta>
            <ta e="T27" id="Seg_3783" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_3784" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_3785" s="T28">v-v:tense-v:pred.pn</ta>
            <ta e="T30" id="Seg_3786" s="T29">adj.[n:case]</ta>
            <ta e="T31" id="Seg_3787" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_3788" s="T31">n-n:(num).[n:case]</ta>
            <ta e="T33" id="Seg_3789" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_3790" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_3791" s="T34">pers.[pro:case]</ta>
            <ta e="T36" id="Seg_3792" s="T35">dempro</ta>
            <ta e="T37" id="Seg_3793" s="T36">adj-n:(num).[n:case]</ta>
            <ta e="T38" id="Seg_3794" s="T37">n-n:poss-n:case</ta>
            <ta e="T39" id="Seg_3795" s="T38">v-v:ptcp</ta>
            <ta e="T40" id="Seg_3796" s="T39">v-v:tense-v:poss.pn</ta>
            <ta e="T41" id="Seg_3797" s="T40">adv</ta>
            <ta e="T42" id="Seg_3798" s="T41">n.[n:case]</ta>
            <ta e="T43" id="Seg_3799" s="T42">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T44" id="Seg_3800" s="T43">v-v:ptcp-v:(case)</ta>
            <ta e="T45" id="Seg_3801" s="T44">adj-n:case</ta>
            <ta e="T46" id="Seg_3802" s="T45">n.[n:case]</ta>
            <ta e="T47" id="Seg_3803" s="T46">v-v:ptcp-v:(case)</ta>
            <ta e="T48" id="Seg_3804" s="T47">adj</ta>
            <ta e="T49" id="Seg_3805" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_3806" s="T49">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T51" id="Seg_3807" s="T50">adj.[n:case]</ta>
            <ta e="T52" id="Seg_3808" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_3809" s="T52">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T54" id="Seg_3810" s="T53">v-v:ptcp</ta>
            <ta e="T55" id="Seg_3811" s="T54">v-v:tense-v:poss.pn</ta>
            <ta e="T56" id="Seg_3812" s="T55">adv</ta>
            <ta e="T57" id="Seg_3813" s="T56">n-n:(num).[n:case]</ta>
            <ta e="T58" id="Seg_3814" s="T57">v-v:mood-v:temp.pn</ta>
            <ta e="T59" id="Seg_3815" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_3816" s="T59">v-v:(ins)-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T61" id="Seg_3817" s="T60">quant-quant&gt;adv</ta>
            <ta e="T62" id="Seg_3818" s="T61">pers.[pro:case]</ta>
            <ta e="T63" id="Seg_3819" s="T62">post</ta>
            <ta e="T64" id="Seg_3820" s="T63">dempro.[pro:case]</ta>
            <ta e="T65" id="Seg_3821" s="T64">v-v:(neg)-v:pred.pn</ta>
            <ta e="T66" id="Seg_3822" s="T65">pers.[pro:case]</ta>
            <ta e="T67" id="Seg_3823" s="T66">cardnum</ta>
            <ta e="T68" id="Seg_3824" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_3825" s="T68">v-v:(ins)-v&gt;v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T70" id="Seg_3826" s="T69">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_3827" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_3828" s="T71">adj</ta>
            <ta e="T73" id="Seg_3829" s="T72">n.[n:case]</ta>
            <ta e="T74" id="Seg_3830" s="T73">adj-n:case</ta>
            <ta e="T75" id="Seg_3831" s="T74">post</ta>
            <ta e="T78" id="Seg_3832" s="T77">adj.[n:case]</ta>
            <ta e="T93" id="Seg_3833" s="T92">n.[n:case]</ta>
            <ta e="T95" id="Seg_3834" s="T93">adj</ta>
            <ta e="T96" id="Seg_3835" s="T95">v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_3836" s="T96">ptcl</ta>
            <ta e="T102" id="Seg_3837" s="T100">n.[n:case]</ta>
            <ta e="T103" id="Seg_3838" s="T102">adj</ta>
            <ta e="T104" id="Seg_3839" s="T103">v-v:tense-v:pred.pn</ta>
            <ta e="T105" id="Seg_3840" s="T104">adj</ta>
            <ta e="T106" id="Seg_3841" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_3842" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_3843" s="T107">v-v:(ins)-v&gt;v-v:cvb-v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T109" id="Seg_3844" s="T108">pers-pro:case</ta>
            <ta e="T110" id="Seg_3845" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_3846" s="T110">dempro</ta>
            <ta e="T112" id="Seg_3847" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_3848" s="T112">n.[n:case]</ta>
            <ta e="T114" id="Seg_3849" s="T113">pers.[pro:case]</ta>
            <ta e="T115" id="Seg_3850" s="T114">adj-n:(num).[n:case]</ta>
            <ta e="T116" id="Seg_3851" s="T115">ptcl-ptcl:(temp.pn)</ta>
            <ta e="T117" id="Seg_3852" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_3853" s="T117">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T119" id="Seg_3854" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_3855" s="T119">n-n:(num).[n:case]</ta>
            <ta e="T121" id="Seg_3856" s="T120">n-n&gt;adv</ta>
            <ta e="T122" id="Seg_3857" s="T121">v-v:(neg)-v:pred.pn</ta>
            <ta e="T123" id="Seg_3858" s="T122">adj.[n:case]</ta>
            <ta e="T124" id="Seg_3859" s="T123">n.[n:case]</ta>
            <ta e="T125" id="Seg_3860" s="T124">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T126" id="Seg_3861" s="T125">n.[n:case]</ta>
            <ta e="T127" id="Seg_3862" s="T126">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T128" id="Seg_3863" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_3864" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_3865" s="T129">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T131" id="Seg_3866" s="T130">adv</ta>
            <ta e="T132" id="Seg_3867" s="T131">dempro.[pro:case]</ta>
            <ta e="T133" id="Seg_3868" s="T132">adv-adv</ta>
            <ta e="T134" id="Seg_3869" s="T133">v-v:tense-v:pred.pn</ta>
            <ta e="T135" id="Seg_3870" s="T134">propr-n:case</ta>
            <ta e="T136" id="Seg_3871" s="T135">v-v:tense-v:pred.pn</ta>
            <ta e="T137" id="Seg_3872" s="T136">propr-n:case</ta>
            <ta e="T138" id="Seg_3873" s="T137">v-v:tense-v:pred.pn</ta>
            <ta e="T139" id="Seg_3874" s="T138">propr-n:case</ta>
            <ta e="T140" id="Seg_3875" s="T139">v-v:cvb</ta>
            <ta e="T141" id="Seg_3876" s="T140">v-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_3877" s="T141">dempro-pro:case</ta>
            <ta e="T143" id="Seg_3878" s="T142">v-v:tense-v:pred.pn</ta>
            <ta e="T144" id="Seg_3879" s="T143">dempro.[pro:case]</ta>
            <ta e="T145" id="Seg_3880" s="T144">dempro</ta>
            <ta e="T146" id="Seg_3881" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_3882" s="T146">n.[n:case]</ta>
            <ta e="T148" id="Seg_3883" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_3884" s="T148">v-v:(ins)-v&gt;v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T150" id="Seg_3885" s="T149">n-n:case</ta>
            <ta e="T157" id="Seg_3886" s="T156">adv</ta>
            <ta e="T158" id="Seg_3887" s="T157">v-v:tense-v:poss.pn</ta>
            <ta e="T159" id="Seg_3888" s="T158">adv</ta>
            <ta e="T160" id="Seg_3889" s="T159">adj.[n:case]</ta>
            <ta e="T161" id="Seg_3890" s="T160">v-v:tense-v:poss.pn</ta>
            <ta e="T162" id="Seg_3891" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_3892" s="T162">adj-adj&gt;adv</ta>
            <ta e="T164" id="Seg_3893" s="T163">v-v:tense-v:poss.pn</ta>
            <ta e="T165" id="Seg_3894" s="T164">n.[n:case]</ta>
            <ta e="T166" id="Seg_3895" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_3896" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_3897" s="T167">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T169" id="Seg_3898" s="T168">adv</ta>
            <ta e="T170" id="Seg_3899" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_3900" s="T170">n-n:(poss).[n:case]</ta>
            <ta e="T172" id="Seg_3901" s="T171">adj.[n:case]</ta>
            <ta e="T173" id="Seg_3902" s="T172">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T180" id="Seg_3903" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_3904" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_3905" s="T181">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T183" id="Seg_3906" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_3907" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_3908" s="T184">v-v:tense.[v:pred.pn]</ta>
            <ta e="T186" id="Seg_3909" s="T185">n-n:(poss).[n:case]</ta>
            <ta e="T187" id="Seg_3910" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_3911" s="T187">v-v:tense.[v:pred.pn]</ta>
            <ta e="T189" id="Seg_3912" s="T188">n.[n:case]</ta>
            <ta e="T190" id="Seg_3913" s="T189">v-v&gt;n-n:(num)-n:case</ta>
            <ta e="T191" id="Seg_3914" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_3915" s="T191">adj.[n:case]</ta>
            <ta e="T193" id="Seg_3916" s="T192">v-v:tense-v:poss.pn</ta>
            <ta e="T194" id="Seg_3917" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_3918" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_3919" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_3920" s="T196">adv</ta>
            <ta e="T198" id="Seg_3921" s="T197">que.[pro:case]</ta>
            <ta e="T199" id="Seg_3922" s="T198">n-n:poss-n:case</ta>
            <ta e="T200" id="Seg_3923" s="T199">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T201" id="Seg_3924" s="T200">n-n:(poss).[n:case]</ta>
            <ta e="T202" id="Seg_3925" s="T201">v-v:tense-v:poss.pn</ta>
            <ta e="T203" id="Seg_3926" s="T202">v-v&gt;v-v:ptcp</ta>
            <ta e="T204" id="Seg_3927" s="T203">n-n:(num).[n:case]</ta>
            <ta e="T205" id="Seg_3928" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_3929" s="T205">v-v:tense-v:poss.pn</ta>
            <ta e="T207" id="Seg_3930" s="T206">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T208" id="Seg_3931" s="T207">v-v:cvb</ta>
            <ta e="T209" id="Seg_3932" s="T208">v-v:tense-v:poss.pn</ta>
            <ta e="T210" id="Seg_3933" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_3934" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_3935" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_3936" s="T212">adj</ta>
            <ta e="T214" id="Seg_3937" s="T213">n-n:(poss).[n:case]</ta>
            <ta e="T215" id="Seg_3938" s="T214">adv</ta>
            <ta e="T216" id="Seg_3939" s="T215">v</ta>
            <ta e="T217" id="Seg_3940" s="T216">v-v:ptcp</ta>
            <ta e="T218" id="Seg_3941" s="T217">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T219" id="Seg_3942" s="T218">v-v:(neg)-v:pred.pn</ta>
            <ta e="T220" id="Seg_3943" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_3944" s="T220">n.[n:case]</ta>
            <ta e="T222" id="Seg_3945" s="T221">v-v&gt;v-v:(ins)-v&gt;v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T223" id="Seg_3946" s="T222">adv</ta>
            <ta e="T224" id="Seg_3947" s="T223">v-v&gt;n-n:case</ta>
            <ta e="T225" id="Seg_3948" s="T224">v-v:ptcp</ta>
            <ta e="T226" id="Seg_3949" s="T225">n.[n:case]</ta>
            <ta e="T227" id="Seg_3950" s="T226">adv</ta>
            <ta e="T228" id="Seg_3951" s="T227">v-v:cvb-v:pred.pn</ta>
            <ta e="T229" id="Seg_3952" s="T228">adv</ta>
            <ta e="T230" id="Seg_3953" s="T229">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T231" id="Seg_3954" s="T230">dempro</ta>
            <ta e="T232" id="Seg_3955" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_3956" s="T232">dempro</ta>
            <ta e="T234" id="Seg_3957" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_3958" s="T234">dempro</ta>
            <ta e="T236" id="Seg_3959" s="T235">n-n:(poss).[n:case]</ta>
            <ta e="T237" id="Seg_3960" s="T236">quant-n:(poss)</ta>
            <ta e="T238" id="Seg_3961" s="T237">adv</ta>
            <ta e="T239" id="Seg_3962" s="T238">dempro</ta>
            <ta e="T240" id="Seg_3963" s="T239">n-n:(poss).[n:case]</ta>
            <ta e="T241" id="Seg_3964" s="T240">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T242" id="Seg_3965" s="T241">v-v:cvb</ta>
            <ta e="T243" id="Seg_3966" s="T242">n-n&gt;v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T244" id="Seg_3967" s="T243">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T245" id="Seg_3968" s="T244">dempro</ta>
            <ta e="T246" id="Seg_3969" s="T245">propr-n:case</ta>
            <ta e="T247" id="Seg_3970" s="T246">v-v:mood-v:pred.pn</ta>
            <ta e="T248" id="Seg_3971" s="T247">dempro.[pro:case]</ta>
            <ta e="T249" id="Seg_3972" s="T248">post</ta>
            <ta e="T250" id="Seg_3973" s="T249">v-v:mood-v:pred.pn</ta>
            <ta e="T251" id="Seg_3974" s="T250">v-v:cvb</ta>
            <ta e="T252" id="Seg_3975" s="T251">adv</ta>
            <ta e="T253" id="Seg_3976" s="T252">adv</ta>
            <ta e="T255" id="Seg_3977" s="T253">n.[n:case]</ta>
            <ta e="T258" id="Seg_3978" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_3979" s="T258">adv</ta>
            <ta e="T260" id="Seg_3980" s="T259">n.[n:case]</ta>
            <ta e="T261" id="Seg_3981" s="T260">v-v:tense-v:poss.pn</ta>
            <ta e="T262" id="Seg_3982" s="T261">adv</ta>
            <ta e="T263" id="Seg_3983" s="T262">n-n:(num)-n:case</ta>
            <ta e="T264" id="Seg_3984" s="T263">v-v:tense-v:pred.pn</ta>
            <ta e="T265" id="Seg_3985" s="T264">adv</ta>
            <ta e="T266" id="Seg_3986" s="T265">n-n:(num)-n:case</ta>
            <ta e="T267" id="Seg_3987" s="T266">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T268" id="Seg_3988" s="T267">adv</ta>
            <ta e="T269" id="Seg_3989" s="T268">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T270" id="Seg_3990" s="T269">n-n:(poss).[n:case]</ta>
            <ta e="T271" id="Seg_3991" s="T270">n.[n:case]</ta>
            <ta e="T272" id="Seg_3992" s="T271">v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_3993" s="T272">adv</ta>
            <ta e="T274" id="Seg_3994" s="T273">n-n:(num)-n:(ins)-n:case</ta>
            <ta e="T275" id="Seg_3995" s="T274">dempro</ta>
            <ta e="T276" id="Seg_3996" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_3997" s="T276">adv</ta>
            <ta e="T278" id="Seg_3998" s="T277">adv</ta>
            <ta e="T279" id="Seg_3999" s="T278">n.[n:case]</ta>
            <ta e="T280" id="Seg_4000" s="T279">v-v:tense-v:poss.pn</ta>
            <ta e="T281" id="Seg_4001" s="T280">v-v:tense-v:poss.pn</ta>
            <ta e="T282" id="Seg_4002" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_4003" s="T282">que.[pro:case]</ta>
            <ta e="T284" id="Seg_4004" s="T283">v-v:tense.[v:pred.pn]</ta>
            <ta e="T285" id="Seg_4005" s="T284">dempro-pro:(num).[pro:case]</ta>
            <ta e="T286" id="Seg_4006" s="T285">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T303" id="Seg_4007" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_4008" s="T303">v-v:tense-v:poss.pn</ta>
            <ta e="T305" id="Seg_4009" s="T304">adv</ta>
            <ta e="T306" id="Seg_4010" s="T305">v-v:tense-v:poss.pn</ta>
            <ta e="T307" id="Seg_4011" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_4012" s="T307">n-n:case</ta>
            <ta e="T309" id="Seg_4013" s="T308">adv</ta>
            <ta e="T310" id="Seg_4014" s="T309">v-v:cvb</ta>
            <ta e="T311" id="Seg_4015" s="T310">v-v:(ins)-v:(neg)-v:tense-v:pred.pn</ta>
            <ta e="T312" id="Seg_4016" s="T311">dempro</ta>
            <ta e="T313" id="Seg_4017" s="T312">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T314" id="Seg_4018" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_4019" s="T314">v-v:mood-v:temp.pn</ta>
            <ta e="T316" id="Seg_4020" s="T315">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T317" id="Seg_4021" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_4022" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_4023" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_4024" s="T319">quant-n:case</ta>
            <ta e="T321" id="Seg_4025" s="T320">v-v:(ins)-v&gt;v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T322" id="Seg_4026" s="T321">cardnum</ta>
            <ta e="T323" id="Seg_4027" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_4028" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_4029" s="T324">v-v:(ins)-v&gt;v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T326" id="Seg_4030" s="T325">pers.[pro:case]</ta>
            <ta e="T327" id="Seg_4031" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_4032" s="T327">adj-n:(poss).[n:case]</ta>
            <ta e="T329" id="Seg_4033" s="T328">adv</ta>
            <ta e="T330" id="Seg_4034" s="T329">n.[n:case]</ta>
            <ta e="T331" id="Seg_4035" s="T330">v-v:(neg)-v:pred.pn</ta>
            <ta e="T332" id="Seg_4036" s="T331">v-v:tense-v:pred.pn</ta>
            <ta e="T333" id="Seg_4037" s="T332">adv</ta>
            <ta e="T334" id="Seg_4038" s="T333">v-v&gt;n-n:case</ta>
            <ta e="T341" id="Seg_4039" s="T339">dempro</ta>
            <ta e="T342" id="Seg_4040" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_4041" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_4042" s="T343">v-v:tense-v:pred.pn</ta>
            <ta e="T345" id="Seg_4043" s="T344">pers.[pro:case]</ta>
            <ta e="T346" id="Seg_4044" s="T345">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T347" id="Seg_4045" s="T346">n-n:poss-n:case</ta>
            <ta e="T348" id="Seg_4046" s="T347">n-n:(poss).[n:case]</ta>
            <ta e="T349" id="Seg_4047" s="T348">n-n:poss-n:case</ta>
            <ta e="T350" id="Seg_4048" s="T349">n-n:(poss).[n:case]</ta>
            <ta e="T351" id="Seg_4049" s="T350">n-n:poss-n:case</ta>
            <ta e="T352" id="Seg_4050" s="T351">n-n:(poss).[n:case]</ta>
            <ta e="T353" id="Seg_4051" s="T352">v-v:tense-v:poss.pn</ta>
            <ta e="T354" id="Seg_4052" s="T353">adv</ta>
            <ta e="T355" id="Seg_4053" s="T354">v-v:tense-v:pred.pn</ta>
            <ta e="T356" id="Seg_4054" s="T355">adv</ta>
            <ta e="T357" id="Seg_4055" s="T356">v-v:ptcp</ta>
            <ta e="T358" id="Seg_4056" s="T357">v-v:tense-v:pred.pn</ta>
            <ta e="T359" id="Seg_4057" s="T358">adv</ta>
            <ta e="T360" id="Seg_4058" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_4059" s="T360">v-v:cvb</ta>
            <ta e="T362" id="Seg_4060" s="T361">v-v:(neg)-v:pred.pn</ta>
            <ta e="T363" id="Seg_4061" s="T362">v-v:tense-v:pred.pn</ta>
            <ta e="T364" id="Seg_4062" s="T363">adv</ta>
            <ta e="T365" id="Seg_4063" s="T364">v-v:ptcp</ta>
            <ta e="T366" id="Seg_4064" s="T365">v-v:tense-v:poss.pn</ta>
            <ta e="T367" id="Seg_4065" s="T366">pers-pro:case</ta>
            <ta e="T368" id="Seg_4066" s="T367">n-n:(poss).[n:case]</ta>
            <ta e="T369" id="Seg_4067" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_4068" s="T369">v-v:cvb</ta>
            <ta e="T371" id="Seg_4069" s="T370">n-n:(poss).[n:case]</ta>
            <ta e="T372" id="Seg_4070" s="T371">v-v:(neg)-v:mood-v:poss.pn</ta>
            <ta e="T373" id="Seg_4071" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_4072" s="T373">v-v:ptcp</ta>
            <ta e="T375" id="Seg_4073" s="T374">v-v:tense-v:poss.pn</ta>
            <ta e="T376" id="Seg_4074" s="T375">quant</ta>
            <ta e="T381" id="Seg_4075" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_4076" s="T381">ptcl</ta>
            <ta e="T383" id="Seg_4077" s="T382">adv</ta>
            <ta e="T384" id="Seg_4078" s="T383">adv</ta>
            <ta e="T385" id="Seg_4079" s="T384">adv</ta>
            <ta e="T386" id="Seg_4080" s="T385">v-v:tense.[v:pred.pn]</ta>
            <ta e="T387" id="Seg_4081" s="T386">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T388" id="Seg_4082" s="T387">n-n:poss-n:case</ta>
            <ta e="T389" id="Seg_4083" s="T388">v-v:mood.[v:pred.pn]</ta>
            <ta e="T391" id="Seg_4084" s="T390">ptcl</ta>
            <ta e="T392" id="Seg_4085" s="T391">adv</ta>
            <ta e="T393" id="Seg_4086" s="T392">adj-n:(poss).[n:case]</ta>
            <ta e="T394" id="Seg_4087" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_4088" s="T394">v-v:ptcp.[v:(case)]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KiPP">
            <ta e="T2" id="Seg_4089" s="T1">v</ta>
            <ta e="T3" id="Seg_4090" s="T2">aux</ta>
            <ta e="T4" id="Seg_4091" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_4092" s="T4">pers</ta>
            <ta e="T6" id="Seg_4093" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_4094" s="T6">post</ta>
            <ta e="T8" id="Seg_4095" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_4096" s="T8">adv</ta>
            <ta e="T10" id="Seg_4097" s="T9">pers</ta>
            <ta e="T11" id="Seg_4098" s="T10">v</ta>
            <ta e="T12" id="Seg_4099" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_4100" s="T12">adj</ta>
            <ta e="T14" id="Seg_4101" s="T13">cop</ta>
            <ta e="T15" id="Seg_4102" s="T14">pers</ta>
            <ta e="T16" id="Seg_4103" s="T15">pers</ta>
            <ta e="T17" id="Seg_4104" s="T16">adv</ta>
            <ta e="T18" id="Seg_4105" s="T17">dempro</ta>
            <ta e="T19" id="Seg_4106" s="T18">v</ta>
            <ta e="T20" id="Seg_4107" s="T19">v</ta>
            <ta e="T21" id="Seg_4108" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_4109" s="T21">adv</ta>
            <ta e="T23" id="Seg_4110" s="T22">n</ta>
            <ta e="T24" id="Seg_4111" s="T23">v</ta>
            <ta e="T25" id="Seg_4112" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_4113" s="T25">adj</ta>
            <ta e="T27" id="Seg_4114" s="T26">n</ta>
            <ta e="T28" id="Seg_4115" s="T27">v</ta>
            <ta e="T29" id="Seg_4116" s="T28">aux</ta>
            <ta e="T30" id="Seg_4117" s="T29">adj</ta>
            <ta e="T31" id="Seg_4118" s="T30">cop</ta>
            <ta e="T32" id="Seg_4119" s="T31">n</ta>
            <ta e="T33" id="Seg_4120" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_4121" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_4122" s="T34">pers</ta>
            <ta e="T36" id="Seg_4123" s="T35">dempro</ta>
            <ta e="T37" id="Seg_4124" s="T36">n</ta>
            <ta e="T38" id="Seg_4125" s="T37">n</ta>
            <ta e="T39" id="Seg_4126" s="T38">v</ta>
            <ta e="T40" id="Seg_4127" s="T39">aux</ta>
            <ta e="T41" id="Seg_4128" s="T40">adv</ta>
            <ta e="T42" id="Seg_4129" s="T41">n</ta>
            <ta e="T43" id="Seg_4130" s="T42">v</ta>
            <ta e="T44" id="Seg_4131" s="T43">v</ta>
            <ta e="T45" id="Seg_4132" s="T44">adj</ta>
            <ta e="T46" id="Seg_4133" s="T45">n</ta>
            <ta e="T47" id="Seg_4134" s="T46">v</ta>
            <ta e="T48" id="Seg_4135" s="T47">adj</ta>
            <ta e="T49" id="Seg_4136" s="T48">n</ta>
            <ta e="T50" id="Seg_4137" s="T49">v</ta>
            <ta e="T51" id="Seg_4138" s="T50">adj</ta>
            <ta e="T52" id="Seg_4139" s="T51">n</ta>
            <ta e="T53" id="Seg_4140" s="T52">v</ta>
            <ta e="T54" id="Seg_4141" s="T53">v</ta>
            <ta e="T55" id="Seg_4142" s="T54">aux</ta>
            <ta e="T56" id="Seg_4143" s="T55">adv</ta>
            <ta e="T57" id="Seg_4144" s="T56">n</ta>
            <ta e="T58" id="Seg_4145" s="T57">v</ta>
            <ta e="T59" id="Seg_4146" s="T58">n</ta>
            <ta e="T60" id="Seg_4147" s="T59">v</ta>
            <ta e="T61" id="Seg_4148" s="T60">adv</ta>
            <ta e="T62" id="Seg_4149" s="T61">pers</ta>
            <ta e="T63" id="Seg_4150" s="T62">post</ta>
            <ta e="T64" id="Seg_4151" s="T63">dempro</ta>
            <ta e="T65" id="Seg_4152" s="T64">v</ta>
            <ta e="T66" id="Seg_4153" s="T65">pers</ta>
            <ta e="T67" id="Seg_4154" s="T66">cardnum</ta>
            <ta e="T68" id="Seg_4155" s="T67">n</ta>
            <ta e="T69" id="Seg_4156" s="T68">v</ta>
            <ta e="T70" id="Seg_4157" s="T69">v</ta>
            <ta e="T71" id="Seg_4158" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_4159" s="T71">adj</ta>
            <ta e="T73" id="Seg_4160" s="T72">n</ta>
            <ta e="T74" id="Seg_4161" s="T73">adj</ta>
            <ta e="T75" id="Seg_4162" s="T74">post</ta>
            <ta e="T78" id="Seg_4163" s="T77">adj</ta>
            <ta e="T93" id="Seg_4164" s="T92">n</ta>
            <ta e="T95" id="Seg_4165" s="T93">adj</ta>
            <ta e="T96" id="Seg_4166" s="T95">v</ta>
            <ta e="T97" id="Seg_4167" s="T96">ptcl</ta>
            <ta e="T102" id="Seg_4168" s="T100">n</ta>
            <ta e="T103" id="Seg_4169" s="T102">adj</ta>
            <ta e="T104" id="Seg_4170" s="T103">v</ta>
            <ta e="T105" id="Seg_4171" s="T104">adj</ta>
            <ta e="T106" id="Seg_4172" s="T105">n</ta>
            <ta e="T107" id="Seg_4173" s="T106">n</ta>
            <ta e="T108" id="Seg_4174" s="T107">v</ta>
            <ta e="T109" id="Seg_4175" s="T108">pers</ta>
            <ta e="T110" id="Seg_4176" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_4177" s="T110">dempro</ta>
            <ta e="T112" id="Seg_4178" s="T111">n</ta>
            <ta e="T113" id="Seg_4179" s="T112">n</ta>
            <ta e="T114" id="Seg_4180" s="T113">pers</ta>
            <ta e="T115" id="Seg_4181" s="T114">n</ta>
            <ta e="T116" id="Seg_4182" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_4183" s="T116">n</ta>
            <ta e="T118" id="Seg_4184" s="T117">v</ta>
            <ta e="T119" id="Seg_4185" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_4186" s="T119">n</ta>
            <ta e="T121" id="Seg_4187" s="T120">adv</ta>
            <ta e="T122" id="Seg_4188" s="T121">v</ta>
            <ta e="T123" id="Seg_4189" s="T122">adj</ta>
            <ta e="T124" id="Seg_4190" s="T123">n</ta>
            <ta e="T125" id="Seg_4191" s="T124">cop</ta>
            <ta e="T126" id="Seg_4192" s="T125">n</ta>
            <ta e="T127" id="Seg_4193" s="T126">cop</ta>
            <ta e="T128" id="Seg_4194" s="T127">n</ta>
            <ta e="T129" id="Seg_4195" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_4196" s="T129">v</ta>
            <ta e="T131" id="Seg_4197" s="T130">adv</ta>
            <ta e="T132" id="Seg_4198" s="T131">dempro</ta>
            <ta e="T133" id="Seg_4199" s="T132">adv</ta>
            <ta e="T134" id="Seg_4200" s="T133">v</ta>
            <ta e="T135" id="Seg_4201" s="T134">propr</ta>
            <ta e="T136" id="Seg_4202" s="T135">v</ta>
            <ta e="T137" id="Seg_4203" s="T136">propr</ta>
            <ta e="T138" id="Seg_4204" s="T137">v</ta>
            <ta e="T139" id="Seg_4205" s="T138">propr</ta>
            <ta e="T140" id="Seg_4206" s="T139">v</ta>
            <ta e="T141" id="Seg_4207" s="T140">aux</ta>
            <ta e="T142" id="Seg_4208" s="T141">dempro</ta>
            <ta e="T143" id="Seg_4209" s="T142">v</ta>
            <ta e="T144" id="Seg_4210" s="T143">dempro</ta>
            <ta e="T145" id="Seg_4211" s="T144">dempro</ta>
            <ta e="T146" id="Seg_4212" s="T145">n</ta>
            <ta e="T147" id="Seg_4213" s="T146">n</ta>
            <ta e="T148" id="Seg_4214" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_4215" s="T148">v</ta>
            <ta e="T150" id="Seg_4216" s="T149">n</ta>
            <ta e="T157" id="Seg_4217" s="T156">adv</ta>
            <ta e="T158" id="Seg_4218" s="T157">v</ta>
            <ta e="T159" id="Seg_4219" s="T158">adv</ta>
            <ta e="T160" id="Seg_4220" s="T159">adj</ta>
            <ta e="T161" id="Seg_4221" s="T160">cop</ta>
            <ta e="T162" id="Seg_4222" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_4223" s="T162">adv</ta>
            <ta e="T164" id="Seg_4224" s="T163">v</ta>
            <ta e="T165" id="Seg_4225" s="T164">n</ta>
            <ta e="T166" id="Seg_4226" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_4227" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_4228" s="T167">v</ta>
            <ta e="T169" id="Seg_4229" s="T168">adv</ta>
            <ta e="T170" id="Seg_4230" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_4231" s="T170">n</ta>
            <ta e="T172" id="Seg_4232" s="T171">adj</ta>
            <ta e="T173" id="Seg_4233" s="T172">v</ta>
            <ta e="T180" id="Seg_4234" s="T179">n</ta>
            <ta e="T181" id="Seg_4235" s="T180">n</ta>
            <ta e="T182" id="Seg_4236" s="T181">v</ta>
            <ta e="T183" id="Seg_4237" s="T182">n</ta>
            <ta e="T184" id="Seg_4238" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_4239" s="T184">v</ta>
            <ta e="T186" id="Seg_4240" s="T185">n</ta>
            <ta e="T187" id="Seg_4241" s="T186">n</ta>
            <ta e="T188" id="Seg_4242" s="T187">v</ta>
            <ta e="T189" id="Seg_4243" s="T188">n</ta>
            <ta e="T190" id="Seg_4244" s="T189">n</ta>
            <ta e="T191" id="Seg_4245" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_4246" s="T191">adj</ta>
            <ta e="T193" id="Seg_4247" s="T192">cop</ta>
            <ta e="T194" id="Seg_4248" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_4249" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_4250" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_4251" s="T196">adv</ta>
            <ta e="T198" id="Seg_4252" s="T197">que</ta>
            <ta e="T199" id="Seg_4253" s="T198">n</ta>
            <ta e="T200" id="Seg_4254" s="T199">cop</ta>
            <ta e="T201" id="Seg_4255" s="T200">n</ta>
            <ta e="T202" id="Seg_4256" s="T201">v</ta>
            <ta e="T203" id="Seg_4257" s="T202">v</ta>
            <ta e="T204" id="Seg_4258" s="T203">n</ta>
            <ta e="T205" id="Seg_4259" s="T204">v</ta>
            <ta e="T206" id="Seg_4260" s="T205">v</ta>
            <ta e="T207" id="Seg_4261" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_4262" s="T207">v</ta>
            <ta e="T209" id="Seg_4263" s="T208">v</ta>
            <ta e="T210" id="Seg_4264" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_4265" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_4266" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_4267" s="T212">adj</ta>
            <ta e="T214" id="Seg_4268" s="T213">n</ta>
            <ta e="T215" id="Seg_4269" s="T214">adv</ta>
            <ta e="T216" id="Seg_4270" s="T215">v</ta>
            <ta e="T217" id="Seg_4271" s="T216">v</ta>
            <ta e="T218" id="Seg_4272" s="T217">n</ta>
            <ta e="T219" id="Seg_4273" s="T218">v</ta>
            <ta e="T220" id="Seg_4274" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_4275" s="T220">n</ta>
            <ta e="T222" id="Seg_4276" s="T221">v</ta>
            <ta e="T223" id="Seg_4277" s="T222">adv</ta>
            <ta e="T224" id="Seg_4278" s="T223">n</ta>
            <ta e="T225" id="Seg_4279" s="T224">v</ta>
            <ta e="T226" id="Seg_4280" s="T225">n</ta>
            <ta e="T227" id="Seg_4281" s="T226">adv</ta>
            <ta e="T228" id="Seg_4282" s="T227">v</ta>
            <ta e="T229" id="Seg_4283" s="T228">adv</ta>
            <ta e="T230" id="Seg_4284" s="T229">v</ta>
            <ta e="T231" id="Seg_4285" s="T230">dempro</ta>
            <ta e="T232" id="Seg_4286" s="T231">n</ta>
            <ta e="T233" id="Seg_4287" s="T232">dempro</ta>
            <ta e="T234" id="Seg_4288" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_4289" s="T234">dempro</ta>
            <ta e="T236" id="Seg_4290" s="T235">n</ta>
            <ta e="T237" id="Seg_4291" s="T236">quant</ta>
            <ta e="T238" id="Seg_4292" s="T237">adv</ta>
            <ta e="T239" id="Seg_4293" s="T238">dempro</ta>
            <ta e="T240" id="Seg_4294" s="T239">n</ta>
            <ta e="T241" id="Seg_4295" s="T240">v</ta>
            <ta e="T242" id="Seg_4296" s="T241">v</ta>
            <ta e="T243" id="Seg_4297" s="T242">v</ta>
            <ta e="T244" id="Seg_4298" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_4299" s="T244">dempro</ta>
            <ta e="T246" id="Seg_4300" s="T245">propr</ta>
            <ta e="T247" id="Seg_4301" s="T246">v</ta>
            <ta e="T248" id="Seg_4302" s="T247">dempro</ta>
            <ta e="T249" id="Seg_4303" s="T248">post</ta>
            <ta e="T250" id="Seg_4304" s="T249">v</ta>
            <ta e="T251" id="Seg_4305" s="T250">v</ta>
            <ta e="T252" id="Seg_4306" s="T251">adv</ta>
            <ta e="T253" id="Seg_4307" s="T252">adv</ta>
            <ta e="T255" id="Seg_4308" s="T253">n</ta>
            <ta e="T258" id="Seg_4309" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_4310" s="T258">adv</ta>
            <ta e="T260" id="Seg_4311" s="T259">n</ta>
            <ta e="T261" id="Seg_4312" s="T260">v</ta>
            <ta e="T262" id="Seg_4313" s="T261">adv</ta>
            <ta e="T263" id="Seg_4314" s="T262">n</ta>
            <ta e="T264" id="Seg_4315" s="T263">v</ta>
            <ta e="T265" id="Seg_4316" s="T264">adv</ta>
            <ta e="T266" id="Seg_4317" s="T265">n</ta>
            <ta e="T267" id="Seg_4318" s="T266">v</ta>
            <ta e="T268" id="Seg_4319" s="T267">adv</ta>
            <ta e="T269" id="Seg_4320" s="T268">v</ta>
            <ta e="T270" id="Seg_4321" s="T269">n</ta>
            <ta e="T271" id="Seg_4322" s="T270">n</ta>
            <ta e="T272" id="Seg_4323" s="T271">v</ta>
            <ta e="T273" id="Seg_4324" s="T272">adv</ta>
            <ta e="T274" id="Seg_4325" s="T273">n</ta>
            <ta e="T275" id="Seg_4326" s="T274">dempro</ta>
            <ta e="T276" id="Seg_4327" s="T275">n</ta>
            <ta e="T277" id="Seg_4328" s="T276">adv</ta>
            <ta e="T278" id="Seg_4329" s="T277">adv</ta>
            <ta e="T279" id="Seg_4330" s="T278">n</ta>
            <ta e="T280" id="Seg_4331" s="T279">v</ta>
            <ta e="T281" id="Seg_4332" s="T280">v</ta>
            <ta e="T282" id="Seg_4333" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_4334" s="T282">que</ta>
            <ta e="T284" id="Seg_4335" s="T283">v</ta>
            <ta e="T285" id="Seg_4336" s="T284">dempro</ta>
            <ta e="T286" id="Seg_4337" s="T285">ptcl</ta>
            <ta e="T303" id="Seg_4338" s="T302">n</ta>
            <ta e="T304" id="Seg_4339" s="T303">v</ta>
            <ta e="T305" id="Seg_4340" s="T304">adv</ta>
            <ta e="T306" id="Seg_4341" s="T305">v</ta>
            <ta e="T307" id="Seg_4342" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_4343" s="T307">n</ta>
            <ta e="T309" id="Seg_4344" s="T308">adv</ta>
            <ta e="T310" id="Seg_4345" s="T309">v</ta>
            <ta e="T311" id="Seg_4346" s="T310">v</ta>
            <ta e="T312" id="Seg_4347" s="T311">dempro</ta>
            <ta e="T313" id="Seg_4348" s="T312">n</ta>
            <ta e="T314" id="Seg_4349" s="T313">n</ta>
            <ta e="T315" id="Seg_4350" s="T314">v</ta>
            <ta e="T316" id="Seg_4351" s="T315">v</ta>
            <ta e="T317" id="Seg_4352" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_4353" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_4354" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_4355" s="T319">quant</ta>
            <ta e="T321" id="Seg_4356" s="T320">v</ta>
            <ta e="T322" id="Seg_4357" s="T321">cardnum</ta>
            <ta e="T323" id="Seg_4358" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_4359" s="T323">n</ta>
            <ta e="T325" id="Seg_4360" s="T324">v</ta>
            <ta e="T326" id="Seg_4361" s="T325">pers</ta>
            <ta e="T327" id="Seg_4362" s="T326">n</ta>
            <ta e="T328" id="Seg_4363" s="T327">adj</ta>
            <ta e="T329" id="Seg_4364" s="T328">adv</ta>
            <ta e="T330" id="Seg_4365" s="T329">n</ta>
            <ta e="T331" id="Seg_4366" s="T330">v</ta>
            <ta e="T332" id="Seg_4367" s="T331">v</ta>
            <ta e="T333" id="Seg_4368" s="T332">adv</ta>
            <ta e="T334" id="Seg_4369" s="T333">n</ta>
            <ta e="T341" id="Seg_4370" s="T339">dempro</ta>
            <ta e="T342" id="Seg_4371" s="T341">n</ta>
            <ta e="T343" id="Seg_4372" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_4373" s="T343">v</ta>
            <ta e="T345" id="Seg_4374" s="T344">pers</ta>
            <ta e="T346" id="Seg_4375" s="T345">n</ta>
            <ta e="T347" id="Seg_4376" s="T346">n</ta>
            <ta e="T348" id="Seg_4377" s="T347">n</ta>
            <ta e="T349" id="Seg_4378" s="T348">n</ta>
            <ta e="T350" id="Seg_4379" s="T349">n</ta>
            <ta e="T351" id="Seg_4380" s="T350">n</ta>
            <ta e="T352" id="Seg_4381" s="T351">n</ta>
            <ta e="T353" id="Seg_4382" s="T352">cop</ta>
            <ta e="T354" id="Seg_4383" s="T353">adv</ta>
            <ta e="T355" id="Seg_4384" s="T354">v</ta>
            <ta e="T356" id="Seg_4385" s="T355">adv</ta>
            <ta e="T357" id="Seg_4386" s="T356">v</ta>
            <ta e="T358" id="Seg_4387" s="T357">aux</ta>
            <ta e="T359" id="Seg_4388" s="T358">adv</ta>
            <ta e="T360" id="Seg_4389" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_4390" s="T360">v</ta>
            <ta e="T362" id="Seg_4391" s="T361">v</ta>
            <ta e="T363" id="Seg_4392" s="T362">v</ta>
            <ta e="T364" id="Seg_4393" s="T363">adv</ta>
            <ta e="T365" id="Seg_4394" s="T364">v</ta>
            <ta e="T366" id="Seg_4395" s="T365">aux</ta>
            <ta e="T367" id="Seg_4396" s="T366">pers</ta>
            <ta e="T368" id="Seg_4397" s="T367">n</ta>
            <ta e="T369" id="Seg_4398" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_4399" s="T369">v</ta>
            <ta e="T371" id="Seg_4400" s="T370">n</ta>
            <ta e="T372" id="Seg_4401" s="T371">v</ta>
            <ta e="T373" id="Seg_4402" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_4403" s="T373">v</ta>
            <ta e="T375" id="Seg_4404" s="T374">aux</ta>
            <ta e="T376" id="Seg_4405" s="T375">quant</ta>
            <ta e="T381" id="Seg_4406" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_4407" s="T381">ptcl</ta>
            <ta e="T383" id="Seg_4408" s="T382">adv</ta>
            <ta e="T384" id="Seg_4409" s="T383">adv</ta>
            <ta e="T385" id="Seg_4410" s="T384">adv</ta>
            <ta e="T386" id="Seg_4411" s="T385">v</ta>
            <ta e="T387" id="Seg_4412" s="T386">ordnum</ta>
            <ta e="T388" id="Seg_4413" s="T387">n</ta>
            <ta e="T389" id="Seg_4414" s="T388">v</ta>
            <ta e="T391" id="Seg_4415" s="T390">ptcl</ta>
            <ta e="T392" id="Seg_4416" s="T391">adv</ta>
            <ta e="T393" id="Seg_4417" s="T392">adj</ta>
            <ta e="T394" id="Seg_4418" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_4419" s="T394">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KiPP" />
         <annotation name="SyF" tierref="SyF-KiPP" />
         <annotation name="IST" tierref="IST-KiPP" />
         <annotation name="Top" tierref="Top-KiPP" />
         <annotation name="Foc" tierref="Foc-KiPP" />
         <annotation name="BOR" tierref="BOR-KiPP">
            <ta e="T23" id="Seg_4420" s="T22">RUS:cult</ta>
            <ta e="T32" id="Seg_4421" s="T31">RUS:cult</ta>
            <ta e="T33" id="Seg_4422" s="T32">RUS:disc</ta>
            <ta e="T37" id="Seg_4423" s="T36">RUS:core</ta>
            <ta e="T38" id="Seg_4424" s="T37">RUS:cult</ta>
            <ta e="T68" id="Seg_4425" s="T67">RUS:cult</ta>
            <ta e="T128" id="Seg_4426" s="T127">RUS:cult</ta>
            <ta e="T135" id="Seg_4427" s="T134">YAK:cult</ta>
            <ta e="T137" id="Seg_4428" s="T136">RUS:cult</ta>
            <ta e="T157" id="Seg_4429" s="T156">RUS:mod</ta>
            <ta e="T159" id="Seg_4430" s="T158">RUS:mod</ta>
            <ta e="T163" id="Seg_4431" s="T162">RUS:core</ta>
            <ta e="T173" id="Seg_4432" s="T172">RUS:core</ta>
            <ta e="T195" id="Seg_4433" s="T194">RUS:disc</ta>
            <ta e="T211" id="Seg_4434" s="T210">RUS:disc</ta>
            <ta e="T263" id="Seg_4435" s="T262">RUS:cult</ta>
            <ta e="T266" id="Seg_4436" s="T265">RUS:cult</ta>
            <ta e="T271" id="Seg_4437" s="T270">RUS:cult</ta>
            <ta e="T274" id="Seg_4438" s="T273">RUS:cult</ta>
            <ta e="T303" id="Seg_4439" s="T302">RUS:cult</ta>
            <ta e="T314" id="Seg_4440" s="T313">RUS:cult</ta>
            <ta e="T318" id="Seg_4441" s="T317">RUS:disc</ta>
            <ta e="T324" id="Seg_4442" s="T323">RUS:cult</ta>
            <ta e="T327" id="Seg_4443" s="T326">RUS:cult</ta>
            <ta e="T329" id="Seg_4444" s="T328">RUS:mod</ta>
            <ta e="T349" id="Seg_4445" s="T348">RUS:core</ta>
            <ta e="T351" id="Seg_4446" s="T350">RUS:cult</ta>
            <ta e="T352" id="Seg_4447" s="T351">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KiPP" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KiPP" />
         <annotation name="CS" tierref="CS-KiPP" />
         <annotation name="fe" tierref="fe-KiPP">
            <ta e="T4" id="Seg_4448" s="T1">– It stops (?).</ta>
            <ta e="T15" id="Seg_4449" s="T4">As long as we are there, they are there, when we stop being now, then they'll be different, too (?).</ta>
            <ta e="T21" id="Seg_4450" s="T15">Well, we are trying to live now.</ta>
            <ta e="T29" id="Seg_4451" s="T21">The young people will hear [it] (?), they go to a different place.</ta>
            <ta e="T31" id="Seg_4452" s="T29">They are different.</ta>
            <ta e="T32" id="Seg_4453" s="T31">The customs.</ta>
            <ta e="T41" id="Seg_4454" s="T32">We, the old people, lived here according to the customs.</ta>
            <ta e="T55" id="Seg_4455" s="T41">To receive a guest, when a guest is coming from far away, one has to feed him with good food, we used to say.</ta>
            <ta e="T60" id="Seg_4456" s="T55">Then, when guests are coming, we give them spirit to drink.</ta>
            <ta e="T66" id="Seg_4457" s="T60">Little, we don't drink like them.</ta>
            <ta e="T71" id="Seg_4458" s="T66">We give three shot glasses to drink, we stop.</ta>
            <ta e="T73" id="Seg_4459" s="T71">The former people.</ta>
            <ta e="T77" id="Seg_4460" s="T73">Until drunken one must not.</ta>
            <ta e="T78" id="Seg_4461" s="T77">[That's] bad.</ta>
            <ta e="T97" id="Seg_4462" s="T92">– They drink every day, yes.</ta>
            <ta e="T113" id="Seg_4463" s="T100">– They drink every day, they bring spirit from somewhere else, in our land there is no spirit.</ta>
            <ta e="T119" id="Seg_4464" s="T113">As long as we old people are there, we don't let them bring spirit.</ta>
            <ta e="T127" id="Seg_4465" s="T119">Children do not eat well, they should be good men. [?]</ta>
            <ta e="T130" id="Seg_4466" s="T127">Only on holidays we let bring [some].</ta>
            <ta e="T141" id="Seg_4467" s="T130">Now they bring [it] from everywhere, they bring form Urung Xaya, they bring from Novorybnoe, they bring from Khatanga.</ta>
            <ta e="T144" id="Seg_4468" s="T141">With that they get drunk.</ta>
            <ta e="T148" id="Seg_4469" s="T144">In this land there is no spirit.</ta>
            <ta e="T150" id="Seg_4470" s="T148">We don't let [them] drink spirit.</ta>
            <ta e="T158" id="Seg_4471" s="T156">– They'll drive, howver.</ta>
            <ta e="T164" id="Seg_4472" s="T158">They'll be half-drunk, they'll drive even bravelier.</ta>
            <ta e="T168" id="Seg_4473" s="T164">There is no spirit, for that they'll drive.</ta>
            <ta e="T173" id="Seg_4474" s="T168">Just now the weather is bad, it changed.</ta>
            <ta e="T186" id="Seg_4475" s="T179">– One doesn't feed the reindeer with spirit, the reindeer eat only from the ground.</ta>
            <ta e="T189" id="Seg_4476" s="T186">The reindeer eats from the ground.</ta>
            <ta e="T202" id="Seg_4477" s="T189">In the races everything will be fine, the spirit they find now, the spirit will come to an end.</ta>
            <ta e="T207" id="Seg_4478" s="T202">Drunken people won't be able to drive.</ta>
            <ta e="T210" id="Seg_4479" s="T207">They won't be able to play.</ta>
            <ta e="T220" id="Seg_4480" s="T210">And so some people, the people taking part in the race don't drink.</ta>
            <ta e="T226" id="Seg_4481" s="T220">They are now bringing the reindeer to the race, the reindeer which are running.</ta>
            <ta e="T232" id="Seg_4482" s="T226">They are bringing them here, they'll go after those reindeer.</ta>
            <ta e="T244" id="Seg_4483" s="T232">There are very many dogs, the dogs don't leave any peace, they can't follow the reindeer (?).</ta>
            <ta e="T249" id="Seg_4484" s="T244">On the Guba they'll compete, there.</ta>
            <ta e="T251" id="Seg_4485" s="T249">They go to play.</ta>
            <ta e="T255" id="Seg_4486" s="T251">Now there a tent…</ta>
            <ta e="T265" id="Seg_4487" s="T257">– Yes, now they are building up a tent, they take away the baloks today.</ta>
            <ta e="T270" id="Seg_4488" s="T265">They make signs, today is the day where they prepare theirselves.</ta>
            <ta e="T274" id="Seg_4489" s="T270">They make a stage there out of barrels.</ta>
            <ta e="T284" id="Seg_4490" s="T274">There at that place they'll tell stories and sing, somebody sings.</ta>
            <ta e="T286" id="Seg_4491" s="T284">Things like that will be.</ta>
            <ta e="T299" id="Seg_4492" s="T295">– I won't go.</ta>
            <ta e="T304" id="Seg_4493" s="T299">I am tired, I'll go to a deceased.</ta>
            <ta e="T308" id="Seg_4494" s="T304">Tomorrow the old woman is taken out.</ta>
            <ta e="T313" id="Seg_4495" s="T308">Now the young men are not able to compete.</ta>
            <ta e="T317" id="Seg_4496" s="T313">Going to a deceased, they'll drink spirit.</ta>
            <ta e="T326" id="Seg_4497" s="T317">They don't give much to drink, only three shots we use to give to drink.</ta>
            <ta e="T333" id="Seg_4498" s="T326">At a deceases that's it, they don't give more spirit, they stop with this.</ta>
            <ta e="T334" id="Seg_4499" s="T333">As a company.</ta>
            <ta e="T343" id="Seg_4500" s="T339">– That old woman?</ta>
            <ta e="T348" id="Seg_4501" s="T343">I know her, the wife of the elder brother of my husband.</ta>
            <ta e="T351" id="Seg_4502" s="T348">His brother's wife, of his cousin.</ta>
            <ta e="T358" id="Seg_4503" s="T351">It was the brother, I know him well, we were nomadizing together.</ta>
            <ta e="T366" id="Seg_4504" s="T358">Now I cannot go, I am lying, I would lie there.</ta>
            <ta e="T367" id="Seg_4505" s="T366">Beneath her.</ta>
            <ta e="T375" id="Seg_4506" s="T367">My child has come, if my child hadn't come, I would have gone.</ta>
            <ta e="T376" id="Seg_4507" s="T375">Many.</ta>
            <ta e="T383" id="Seg_4508" s="T380">– Yes, yes, yesterday.</ta>
            <ta e="T386" id="Seg_4509" s="T383">Today, tomorrow she goes out [=she is brought out].</ta>
            <ta e="T389" id="Seg_4510" s="T386">On the third day one goes out.</ta>
            <ta e="T395" id="Seg_4511" s="T390">Well, that's all with questions.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KiPP">
            <ta e="T4" id="Seg_4512" s="T1">– Es hört auf (?).</ta>
            <ta e="T15" id="Seg_4513" s="T4">Solange es uns gibt, gibt es das, wenn wir jetzt zuende gehen, dann werden sie auch anders (?).</ta>
            <ta e="T21" id="Seg_4514" s="T15">Nun, so versuchen wir jetzt zu leben.</ta>
            <ta e="T29" id="Seg_4515" s="T21">Die jungen Leute werden [es] hören (?), sie gehen an einen anderen Ort.</ta>
            <ta e="T31" id="Seg_4516" s="T29">Sie sind anders.</ta>
            <ta e="T32" id="Seg_4517" s="T31">Die Bräuche.</ta>
            <ta e="T41" id="Seg_4518" s="T32">Wir Alten lebten gemäß der Bräuche hier.</ta>
            <ta e="T55" id="Seg_4519" s="T41">Einen Gast zu empfangen, wenn ein Gast von weit her kommt, dann muss man ihn mit gutem Essen verköstigen, sagten wir.</ta>
            <ta e="T60" id="Seg_4520" s="T55">Dann, wenn Gäste kommen, geben wir ihnen Schnaps zu trinken.</ta>
            <ta e="T66" id="Seg_4521" s="T60">Wenig, wie sie trinken wir nicht.</ta>
            <ta e="T71" id="Seg_4522" s="T66">Drei Schnapsgläser geben wir zu trinken, wir hören auf.</ta>
            <ta e="T73" id="Seg_4523" s="T71">Die alten Leute.</ta>
            <ta e="T77" id="Seg_4524" s="T73">Bis man betrunken ist, darf man nicht.</ta>
            <ta e="T78" id="Seg_4525" s="T77">[Das ist] schlecht.</ta>
            <ta e="T97" id="Seg_4526" s="T92">– Jeden Tag trinken sie, ja.</ta>
            <ta e="T113" id="Seg_4527" s="T100">– Jeden Tag trinken sie, sie bringen Schnaps von woanders, bei uns, in unserem Land gibt es keinen Schnaps.</ta>
            <ta e="T119" id="Seg_4528" s="T113">Solange wir alten Leute da sind, lassen wir sie keinen Schnaps bringen.</ta>
            <ta e="T127" id="Seg_4529" s="T119">Die Kinder essen nicht gut, man muss ein guter Mensch sein. [?]</ta>
            <ta e="T130" id="Seg_4530" s="T127">Nur am Feiertag lassen wir [welchen] bringen.</ta>
            <ta e="T141" id="Seg_4531" s="T130">Jetzt bringen sie [ihn] von überall, aus Ürüng Xaja bringen sie, aus Novorybnoe bringen sie, aus Chatanga bringen sie.</ta>
            <ta e="T144" id="Seg_4532" s="T141">Damit betrinken sie sich.</ta>
            <ta e="T148" id="Seg_4533" s="T144">In diesem Land gibt es keinen Schnaps.</ta>
            <ta e="T150" id="Seg_4534" s="T148">Wir lassen [sie] keinen Schnaps trinken.</ta>
            <ta e="T158" id="Seg_4535" s="T156">– Sie werden jedenfalls fahren.</ta>
            <ta e="T164" id="Seg_4536" s="T158">Sie werden etwas angetrunken sein, sie werden noch mutiger fahren.</ta>
            <ta e="T168" id="Seg_4537" s="T164">Es gibt keinen Schnaps, damit sie fahren.</ta>
            <ta e="T173" id="Seg_4538" s="T168">Gerade ist das Wetter schlecht, es ist umgeschlagen.</ta>
            <ta e="T186" id="Seg_4539" s="T179">– Mit Schnaps verköstigt man die Rentiere nicht, Rentiere fressen nur vom Boden.</ta>
            <ta e="T189" id="Seg_4540" s="T186">Das Rentier frisst vom Boden.</ta>
            <ta e="T202" id="Seg_4541" s="T189">In den Rennen wird alles gut sein, was sie jetzt an Schnaps finden, der Schnaps wird zur Neige gehen.</ta>
            <ta e="T207" id="Seg_4542" s="T202">Betrunkene Leute können nicht fahren.</ta>
            <ta e="T210" id="Seg_4543" s="T207">Sie können nicht spielen.</ta>
            <ta e="T220" id="Seg_4544" s="T210">Und so die einen Leute, die Leute, die am Rennen teilnehmen, trinken nicht.</ta>
            <ta e="T226" id="Seg_4545" s="T220">Sie bringen jetzt die Rentiere zum Wettlauf, die Rentiere, die laufen.</ta>
            <ta e="T232" id="Seg_4546" s="T226">Sie bringen sie hierher, sie werden diesen Rentieren folgen.</ta>
            <ta e="T244" id="Seg_4547" s="T232">Es gibt sehr viele Hunde, die Hunde geben keine Ruhe, sie können die Rentiere nicht verfolgen (?).</ta>
            <ta e="T249" id="Seg_4548" s="T244">Auf dem Guba werden sie um die Wette fahren, dort.</ta>
            <ta e="T251" id="Seg_4549" s="T249">Sie gehen, um zu spielen.</ta>
            <ta e="T255" id="Seg_4550" s="T251">Jetzt dort ein Zelt…</ta>
            <ta e="T265" id="Seg_4551" s="T257">– Ja, jetzt bauen sie dort ein Zelt auf, die Baloks nehmen sie heute weg.</ta>
            <ta e="T270" id="Seg_4552" s="T265">Sie machen Zeichen, heute ist der Tag, an dem sie sich vorbereiten.</ta>
            <ta e="T274" id="Seg_4553" s="T270">Sie machen dort eine Bühne aus Fässern.</ta>
            <ta e="T284" id="Seg_4554" s="T274">An dem Platz dort werden sie erzählen und singen, irgendwer singt.</ta>
            <ta e="T286" id="Seg_4555" s="T284">Das gibt es.</ta>
            <ta e="T299" id="Seg_4556" s="T295">– Ich gehe nicht.</ta>
            <ta e="T304" id="Seg_4557" s="T299">Ich bin müde, ich gehe zu einem Toten.</ta>
            <ta e="T308" id="Seg_4558" s="T304">Morgen wird die alte Frau hinausgebracht.</ta>
            <ta e="T313" id="Seg_4559" s="T308">Jetzt können die Jungs nicht um die Wette fahren.</ta>
            <ta e="T317" id="Seg_4560" s="T313">Wenn sie zu einem Toten gehen, trinken sie Schnaps.</ta>
            <ta e="T326" id="Seg_4561" s="T317">Viel geben sie nicht zu trinken, nur drei Schnapsgläser geben wir zu trinken.</ta>
            <ta e="T333" id="Seg_4562" s="T326">Beim Toten ist das Alles, mehr Schnaps gibt man nicht, damit hört man auf.</ta>
            <ta e="T334" id="Seg_4563" s="T333">Zur Begleitung.</ta>
            <ta e="T343" id="Seg_4564" s="T339">– Die alte Frau?</ta>
            <ta e="T348" id="Seg_4565" s="T343">Die kenne ich, die Frau des älteren Bruders meines Mannes.</ta>
            <ta e="T351" id="Seg_4566" s="T348">Die Frau seines Bruders, seines Cousins.</ta>
            <ta e="T358" id="Seg_4567" s="T351">Es war der Bruder, ich kenne ihn gut, wir sind zusammen nomadisiert.</ta>
            <ta e="T366" id="Seg_4568" s="T358">Jetzt kann ich nicht gehen, ich liege, dort würde ich liegen.</ta>
            <ta e="T367" id="Seg_4569" s="T366">Bei ihr.</ta>
            <ta e="T375" id="Seg_4570" s="T367">Mein Kind ist gekommen, wenn mein Kind nicht gekommen wäre, wäre ich gegangen.</ta>
            <ta e="T376" id="Seg_4571" s="T375">Viele.</ta>
            <ta e="T383" id="Seg_4572" s="T380">– Ja, ja, gestern.</ta>
            <ta e="T386" id="Seg_4573" s="T383">Heute, morgen geht sie hinaus [=wird sie hinausgebracht].</ta>
            <ta e="T389" id="Seg_4574" s="T386">Am dritten Tag geht man hinaus.</ta>
            <ta e="T395" id="Seg_4575" s="T390">Nun, dann ist Schluss mit Fragen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KiPP">
            <ta e="T4" id="Seg_4576" s="T1">– Прекращается (?).</ta>
            <ta e="T15" id="Seg_4577" s="T4">Пока мы есть, это есть, когда мы закончимся, они тоже станут другими (?).</ta>
            <ta e="T21" id="Seg_4578" s="T15">Вот так мы стараемся теперь жить.</ta>
            <ta e="T29" id="Seg_4579" s="T21">Молодежь услышит, уедет в другое место [в город], будет уезжать.</ta>
            <ta e="T31" id="Seg_4580" s="T29">Они другие (/отдельно будут жить).</ta>
            <ta e="T32" id="Seg_4581" s="T31">Обычаи.</ta>
            <ta e="T41" id="Seg_4582" s="T32">Так мы жили по обычаям стариков.</ta>
            <ta e="T55" id="Seg_4583" s="T41">Чтобы встречать приехавшего гостя, когда гость издалека приезжает, нужно кормить его хорошей едой, так мы говорили.</ta>
            <ta e="T60" id="Seg_4584" s="T55">Потом, когда гости приезжают, мы их спиртным поим.</ta>
            <ta e="T66" id="Seg_4585" s="T60">Понемножку, как они, мы не пьем.</ta>
            <ta e="T71" id="Seg_4586" s="T66">Три рюмки даем выпить, и на этом всё.</ta>
            <ta e="T73" id="Seg_4587" s="T71">Старые люди.</ta>
            <ta e="T77" id="Seg_4588" s="T73">Допьяна нельзя так.</ta>
            <ta e="T78" id="Seg_4589" s="T77">Плохо.</ta>
            <ta e="T97" id="Seg_4590" s="T92">– Каждый день пьют, да.</ta>
            <ta e="T113" id="Seg_4591" s="T100">– Каждый день выпивают, привозят спирт из других мест, у нас нет спиртного.</ta>
            <ta e="T119" id="Seg_4592" s="T113">Пока мы, старики, тут, спиртное не разрешаем привозить.</ta>
            <ta e="T127" id="Seg_4593" s="T119">Дети хорошо не кушают (пьют), нужно быть хорошим человеком. [?]</ta>
            <ta e="T130" id="Seg_4594" s="T127">На праздник только мы просим привезти.</ta>
            <ta e="T141" id="Seg_4595" s="T130">Сейчас отовсюду привозят, из Юрюнг Хая привозят, из Новорыбного привозят, из Хатанги привозят.</ta>
            <ta e="T144" id="Seg_4596" s="T141">И напиваются этим.</ta>
            <ta e="T148" id="Seg_4597" s="T144">В нашей земле спиртного нету.</ta>
            <ta e="T150" id="Seg_4598" s="T148">Не разрешаем пить.</ta>
            <ta e="T158" id="Seg_4599" s="T156">– Все равно поедут.</ta>
            <ta e="T164" id="Seg_4600" s="T158">Немножко подвыпивши будут, еще смелее поедут.</ta>
            <ta e="T168" id="Seg_4601" s="T164">Спиртного нету, чтобы поехать.</ta>
            <ta e="T173" id="Seg_4602" s="T168">Теперь погода плохая, поменялась.</ta>
            <ta e="T186" id="Seg_4603" s="T179">– Спиртным оленей не поят, с земли только ест олень.</ta>
            <ta e="T189" id="Seg_4604" s="T186">С земли ест олень.</ta>
            <ta e="T202" id="Seg_4605" s="T189">В гонке все хорошо будет, сейчас какое спиртное найдут, спиртное закончится.</ta>
            <ta e="T207" id="Seg_4606" s="T202">С похмелья люди не смогут поехать.</ta>
            <ta e="T210" id="Seg_4607" s="T207">Не смогут играть.</ta>
            <ta e="T220" id="Seg_4608" s="T210">А так некоторые люди котоыре участвуют в гонках, не пьют.</ta>
            <ta e="T226" id="Seg_4609" s="T220">Они заводят оленей для гонки, гоночных оленей.</ta>
            <ta e="T232" id="Seg_4610" s="T226">Они пригонят их сюда, здесь будут следить за этими оленями.</ta>
            <ta e="T244" id="Seg_4611" s="T232">Собак много, собаки покоя не дадут, они не смогут уследить за оленями (?).</ta>
            <ta e="T249" id="Seg_4612" s="T244">На Губе они будут соревноваться, вон там.</ta>
            <ta e="T251" id="Seg_4613" s="T249">Идут играть.</ta>
            <ta e="T255" id="Seg_4614" s="T251">Сейчас там чум…</ta>
            <ta e="T265" id="Seg_4615" s="T257">– Да, сейчас там чум поставят, балки увезут сегодня.</ta>
            <ta e="T270" id="Seg_4616" s="T265">Отметки делают, сегодня они готовятся.</ta>
            <ta e="T274" id="Seg_4617" s="T270">Они делают там сцену из бочек.</ta>
            <ta e="T284" id="Seg_4618" s="T274">Там будут рассказывать и петь, кто поет.</ta>
            <ta e="T286" id="Seg_4619" s="T284">Такие есть.</ta>
            <ta e="T299" id="Seg_4620" s="T295">– Я не поеду.</ta>
            <ta e="T304" id="Seg_4621" s="T299">Я устала, к покойникам пойду.</ta>
            <ta e="T308" id="Seg_4622" s="T304">Завтра хоронят бабушку.</ta>
            <ta e="T313" id="Seg_4623" s="T308">Сейчас не смогут соревноваться те парни.</ta>
            <ta e="T317" id="Seg_4624" s="T313">Они пойдут к поконику, выпьют.</ta>
            <ta e="T326" id="Seg_4625" s="T317">Так да много не дают выпить, три рюмки только мы даем выпить.</ta>
            <ta e="T333" id="Seg_4626" s="T326">У покойника всё, больше спиртного не дают, заканчивают на этом.</ta>
            <ta e="T334" id="Seg_4627" s="T333">Чтобы проводить.</ta>
            <ta e="T343" id="Seg_4628" s="T339">– Бабушку что ли?</ta>
            <ta e="T348" id="Seg_4629" s="T343">Я знаю ее, жена старшего брата моего мужа.</ta>
            <ta e="T351" id="Seg_4630" s="T348">Жена его брата, двоюрдного брата.</ta>
            <ta e="T358" id="Seg_4631" s="T351">Брат был, я его хорошо знаю, мы аргишили вместе.</ta>
            <ta e="T366" id="Seg_4632" s="T358">Сейчас не могу идти, лежу здесь, там бы лежала.</ta>
            <ta e="T367" id="Seg_4633" s="T366">Возле нее.</ta>
            <ta e="T375" id="Seg_4634" s="T367">Мой ребенок пришел, если бы ребенок не пришел, я бы пошла.</ta>
            <ta e="T376" id="Seg_4635" s="T375">Много.</ta>
            <ta e="T383" id="Seg_4636" s="T380">– Да, да, вчера.</ta>
            <ta e="T386" id="Seg_4637" s="T383">Сегодня-завтра выйдет [выносят].</ta>
            <ta e="T389" id="Seg_4638" s="T386">На третий день выходят.</ta>
            <ta e="T395" id="Seg_4639" s="T390">Ну что, закончили спрашивать.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-KiPP">
            <ta e="T4" id="Seg_4640" s="T1">poka mɨ est est, </ta>
            <ta e="T15" id="Seg_4641" s="T4">kogda mɨ zakonchimsja tozhe oni budut drugie. </ta>
            <ta e="T21" id="Seg_4642" s="T15">vot mɨ takim obrazom staraemsja zhit </ta>
            <ta e="T29" id="Seg_4643" s="T21">molodezh slushatsja budet budet uezhaet na drugoe mesto (goroda) budut uezhat</ta>
            <ta e="T31" id="Seg_4644" s="T29">otdelno budut (zhit)</ta>
            <ta e="T32" id="Seg_4645" s="T31">obɨchie</ta>
            <ta e="T41" id="Seg_4646" s="T32">tak to mɨ zhili po obɨchajam starikov</ta>
            <ta e="T55" id="Seg_4647" s="T41">priehavshih gostej vstrechat, gostej iz daleka nado kormit horoshej edoj, horoshej edoj kormit nado mɨ govorili</ta>
            <ta e="T60" id="Seg_4648" s="T55">potom kogda gosti priezajut spirtnoe napoim (ugoshaem)</ta>
            <ta e="T66" id="Seg_4649" s="T60">malenko mɨ ne spaivaem</ta>
            <ta e="T71" id="Seg_4650" s="T66">tri rjumki daem pit, i na etom vse</ta>
            <ta e="T73" id="Seg_4651" s="T71">starɨe ljudi</ta>
            <ta e="T77" id="Seg_4652" s="T73">dopjano nelzja tak</ta>
            <ta e="T78" id="Seg_4653" s="T77">ploho</ta>
            <ta e="T113" id="Seg_4654" s="T100">kazhdɨj den pjut privozjat s drugih mest, u nas net, na etom meste spirtnogo</ta>
            <ta e="T119" id="Seg_4655" s="T113">mɨ poka est starɨe ljudi spirtnoe ne razreshaem privozit</ta>
            <ta e="T127" id="Seg_4656" s="T119">deti horosho ne kushajut (pjut), horoshij chelovek nado bɨt (naado)</ta>
            <ta e="T130" id="Seg_4657" s="T127">na prazdnike prosim privesti</ta>
            <ta e="T141" id="Seg_4658" s="T130">seichas oto vsjudu privozjat iz Urung Kaja privozjat iz novorɨbnogo privizjat iz khatanga</ta>
            <ta e="T144" id="Seg_4659" s="T141">ot etogo pjenejut </ta>
            <ta e="T148" id="Seg_4660" s="T144">zdes spirtnogo netu</ta>
            <ta e="T150" id="Seg_4661" s="T148">ne razreshaem (daem) pit</ta>
            <ta e="T158" id="Seg_4662" s="T156">vse ravno poedut</ta>
            <ta e="T164" id="Seg_4663" s="T158">nemnozhke podvɨpivshij budut esho smelee poedut</ta>
            <ta e="T168" id="Seg_4664" s="T164">sportnogo netu chtobɨ poehali</ta>
            <ta e="T173" id="Seg_4665" s="T168">seichas pogoda plohaja pomenjalas</ta>
            <ta e="T186" id="Seg_4666" s="T179">spirtnɨm olenej ne kormjat, s zemli tolko kushaet olen</ta>
            <ta e="T189" id="Seg_4667" s="T186">s zemli kushaet olen</ta>
            <ta e="T202" id="Seg_4668" s="T189">gonkam vse normalno budet tak da, seichas kakoe spirtnoe naidut spirtnoe zakonchitsja</ta>
            <ta e="T207" id="Seg_4669" s="T202">s pohmeljem ljudi ne smogut poehat </ta>
            <ta e="T210" id="Seg_4670" s="T207">ne smogut igrat</ta>
            <ta e="T220" id="Seg_4671" s="T210">tak da nekkotorɨe ljudi, ljudi kotorɨe sorevnujutsa (uchastvujut v gonkah) oni ne pjut</ta>
            <ta e="T226" id="Seg_4672" s="T220">olenej (kiilerer - zavodjat) progonjat, olenej dlja gonok</ta>
            <ta e="T232" id="Seg_4673" s="T226">prigonjat zdes budut sledit za etimi olenjami </ta>
            <ta e="T244" id="Seg_4674" s="T232">sobak mnogo, sobaki pokoja ne dadut oni ne smogut usledit</ta>
            <ta e="T249" id="Seg_4675" s="T244">von tak na Gube sorevnujutsja von tam</ta>
            <ta e="T251" id="Seg_4676" s="T249">idut igrat</ta>
            <ta e="T255" id="Seg_4677" s="T251">seichas tam… chum postrojat tam balki uvezut segodnja</ta>
            <ta e="T265" id="Seg_4678" s="T257">seichas tam… chum postrojat tam balki uvezut segodnja</ta>
            <ta e="T270" id="Seg_4679" s="T265">otmetki delajut, segodnja oni govotjatsja (prigotovjatsja)</ta>
            <ta e="T274" id="Seg_4680" s="T270">szenu delajut tam iz bockek (bochkami)</ta>
            <ta e="T284" id="Seg_4681" s="T274">tam budut rasskazɨvat budut pet, kto poet</ta>
            <ta e="T286" id="Seg_4682" s="T284">takie est</ta>
            <ta e="T304" id="Seg_4683" s="T299">k pokojniku poidu</ta>
            <ta e="T308" id="Seg_4684" s="T304">zavtra pohoronjat babushku</ta>
            <ta e="T313" id="Seg_4685" s="T308">seichas ne smogut sorevnovatsja te parni</ta>
            <ta e="T317" id="Seg_4686" s="T313">poidut k pokojniku vɨpjut</ta>
            <ta e="T326" id="Seg_4687" s="T317">tak da mnogo ne dajut pit, tri rjumki tolko mɨ daem</ta>
            <ta e="T333" id="Seg_4688" s="T326">u pokojnika i vse, bolshe spirtnoe ne dajut, zakanchivajut na etom</ta>
            <ta e="T334" id="Seg_4689" s="T333">provozhat,</ta>
            <ta e="T343" id="Seg_4690" s="T339">babushku chto li</ta>
            <ta e="T348" id="Seg_4691" s="T343">moego muzha brata zhena</ta>
            <ta e="T351" id="Seg_4692" s="T348">zhena brata, dvojurodnogo brata</ta>
            <ta e="T358" id="Seg_4693" s="T351">brat bɨl horosho znaju, vmeste argishili (ezdili)</ta>
            <ta e="T366" id="Seg_4694" s="T358">seichas tut ne mogu idti, sizhu zdes, tam bɨ sidela</ta>
            <ta e="T367" id="Seg_4695" s="T366">u nee</ta>
            <ta e="T375" id="Seg_4696" s="T367">rebenok prishel, esli bɨ rebenok ne prishel ja bɨ poshla</ta>
            <ta e="T386" id="Seg_4697" s="T383">segodnja zavtra vɨnosjat</ta>
            <ta e="T395" id="Seg_4698" s="T390">nu chto zakonchilis sprashivat</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KiPP" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-SE"
                      id="tx-SE"
                      speaker="SE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-SE">
            <ts e="T101" id="Seg_4699" n="sc" s="T79">
               <ts e="T97" id="Seg_4701" n="HIAT:u" s="T79">
                  <nts id="Seg_4702" n="HIAT:ip">–</nts>
                  <nts id="Seg_4703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_4705" n="HIAT:w" s="T79">Anɨ</ts>
                  <nts id="Seg_4706" n="HIAT:ip">,</nts>
                  <nts id="Seg_4707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_4709" n="HIAT:w" s="T80">anɨ</ts>
                  <nts id="Seg_4710" n="HIAT:ip">,</nts>
                  <nts id="Seg_4711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_4713" n="HIAT:w" s="T81">anɨ</ts>
                  <nts id="Seg_4714" n="HIAT:ip">,</nts>
                  <nts id="Seg_4715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_4717" n="HIAT:w" s="T82">kün</ts>
                  <nts id="Seg_4718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_4720" n="HIAT:w" s="T83">aːjɨ</ts>
                  <nts id="Seg_4721" n="HIAT:ip">,</nts>
                  <nts id="Seg_4722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_4724" n="HIAT:w" s="T84">eː</ts>
                  <nts id="Seg_4725" n="HIAT:ip">,</nts>
                  <nts id="Seg_4726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4727" n="HIAT:ip">(</nts>
                  <ts e="T86" id="Seg_4729" n="HIAT:w" s="T85">ispe-</ts>
                  <nts id="Seg_4730" n="HIAT:ip">)</nts>
                  <nts id="Seg_4731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4732" n="HIAT:ip">(</nts>
                  <ts e="T87" id="Seg_4734" n="HIAT:w" s="T86">is-</ts>
                  <nts id="Seg_4735" n="HIAT:ip">)</nts>
                  <nts id="Seg_4736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_4738" n="HIAT:w" s="T87">ispite</ts>
                  <nts id="Seg_4739" n="HIAT:ip">,</nts>
                  <nts id="Seg_4740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_4742" n="HIAT:w" s="T88">vot</ts>
                  <nts id="Seg_4743" n="HIAT:ip">,</nts>
                  <nts id="Seg_4744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_4746" n="HIAT:w" s="T89">pʼjut</ts>
                  <nts id="Seg_4747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_4749" n="HIAT:w" s="T90">šʼas</ts>
                  <nts id="Seg_4750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_4752" n="HIAT:w" s="T91">každɨj</ts>
                  <nts id="Seg_4753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_4755" n="HIAT:w" s="T94">dʼenʼ</ts>
                  <nts id="Seg_4756" n="HIAT:ip">.</nts>
                  <nts id="Seg_4757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_4759" n="HIAT:u" s="T97">
                  <nts id="Seg_4760" n="HIAT:ip">–</nts>
                  <nts id="Seg_4761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_4763" n="HIAT:w" s="T97">Aː</ts>
                  <nts id="Seg_4764" n="HIAT:ip">,</nts>
                  <nts id="Seg_4765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_4767" n="HIAT:w" s="T98">iheller</ts>
                  <nts id="Seg_4768" n="HIAT:ip">,</nts>
                  <nts id="Seg_4769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_4771" n="HIAT:w" s="T99">da</ts>
                  <nts id="Seg_4772" n="HIAT:ip">.</nts>
                  <nts id="Seg_4773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T156" id="Seg_4774" n="sc" s="T150">
               <ts e="T156" id="Seg_4776" n="HIAT:u" s="T150">
                  <nts id="Seg_4777" n="HIAT:ip">–</nts>
                  <nts id="Seg_4778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_4780" n="HIAT:w" s="T150">Kak</ts>
                  <nts id="Seg_4781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_4783" n="HIAT:w" s="T151">anʼi</ts>
                  <nts id="Seg_4784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_4786" n="HIAT:w" s="T152">zavtra</ts>
                  <nts id="Seg_4787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_4789" n="HIAT:w" s="T153">sarʼevnavatʼ</ts>
                  <nts id="Seg_4790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_4792" n="HIAT:w" s="T154">budut</ts>
                  <nts id="Seg_4793" n="HIAT:ip">,</nts>
                  <nts id="Seg_4794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_4796" n="HIAT:w" s="T155">e</ts>
                  <nts id="Seg_4797" n="HIAT:ip">?</nts>
                  <nts id="Seg_4798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T179" id="Seg_4799" n="sc" s="T173">
               <ts e="T179" id="Seg_4801" n="HIAT:u" s="T173">
                  <nts id="Seg_4802" n="HIAT:ip">–</nts>
                  <nts id="Seg_4803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_4805" n="HIAT:w" s="T173">I</ts>
                  <nts id="Seg_4806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_4808" n="HIAT:w" s="T174">ješʼo</ts>
                  <nts id="Seg_4809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_4811" n="HIAT:w" s="T175">olʼenʼa</ts>
                  <nts id="Seg_4812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_4814" n="HIAT:w" s="T176">vkormʼat</ts>
                  <nts id="Seg_4815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_4817" n="HIAT:w" s="T177">tože</ts>
                  <nts id="Seg_4818" n="HIAT:ip">,</nts>
                  <nts id="Seg_4819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_4821" n="HIAT:w" s="T178">aragiːjem</ts>
                  <nts id="Seg_4822" n="HIAT:ip">.</nts>
                  <nts id="Seg_4823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T257" id="Seg_4824" n="sc" s="T254">
               <ts e="T257" id="Seg_4826" n="HIAT:u" s="T254">
                  <nts id="Seg_4827" n="HIAT:ip">–</nts>
                  <nts id="Seg_4828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_4830" n="HIAT:w" s="T254">Harsɨn</ts>
                  <nts id="Seg_4831" n="HIAT:ip">,</nts>
                  <nts id="Seg_4832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_4834" n="HIAT:w" s="T256">da</ts>
                  <nts id="Seg_4835" n="HIAT:ip">?</nts>
                  <nts id="Seg_4836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T296" id="Seg_4837" n="sc" s="T287">
               <ts e="T296" id="Seg_4839" n="HIAT:u" s="T287">
                  <nts id="Seg_4840" n="HIAT:ip">–</nts>
                  <nts id="Seg_4841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_4843" n="HIAT:w" s="T287">Vɨ</ts>
                  <nts id="Seg_4844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_4846" n="HIAT:w" s="T288">na</ts>
                  <nts id="Seg_4847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_4849" n="HIAT:w" s="T289">sankax</ts>
                  <nts id="Seg_4850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_4852" n="HIAT:w" s="T290">nʼe</ts>
                  <nts id="Seg_4853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_4855" n="HIAT:w" s="T291">pajedʼete</ts>
                  <nts id="Seg_4856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_4858" n="HIAT:w" s="T292">tuda</ts>
                  <nts id="Seg_4859" n="HIAT:ip">,</nts>
                  <nts id="Seg_4860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_4862" n="HIAT:w" s="T293">da</ts>
                  <nts id="Seg_4863" n="HIAT:ip">,</nts>
                  <nts id="Seg_4864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_4866" n="HIAT:w" s="T294">zavtra</ts>
                  <nts id="Seg_4867" n="HIAT:ip">?</nts>
                  <nts id="Seg_4868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T340" id="Seg_4869" n="sc" s="T335">
               <ts e="T340" id="Seg_4871" n="HIAT:u" s="T335">
                  <nts id="Seg_4872" n="HIAT:ip">–</nts>
                  <nts id="Seg_4873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_4875" n="HIAT:w" s="T335">Ginini</ts>
                  <nts id="Seg_4876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_4878" n="HIAT:w" s="T336">bilegit</ts>
                  <nts id="Seg_4879" n="HIAT:ip">,</nts>
                  <nts id="Seg_4880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_4882" n="HIAT:w" s="T337">bilegin</ts>
                  <nts id="Seg_4883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_4885" n="HIAT:w" s="T338">da</ts>
                  <nts id="Seg_4886" n="HIAT:ip">?</nts>
                  <nts id="Seg_4887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T380" id="Seg_4888" n="sc" s="T376">
               <ts e="T380" id="Seg_4890" n="HIAT:u" s="T376">
                  <nts id="Seg_4891" n="HIAT:ip">–</nts>
                  <nts id="Seg_4892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_4894" n="HIAT:w" s="T376">Ana</ts>
                  <nts id="Seg_4895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_4897" n="HIAT:w" s="T377">včʼera</ts>
                  <nts id="Seg_4898" n="HIAT:ip">,</nts>
                  <nts id="Seg_4899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_4901" n="HIAT:w" s="T378">da</ts>
                  <nts id="Seg_4902" n="HIAT:ip">,</nts>
                  <nts id="Seg_4903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_4905" n="HIAT:w" s="T379">umʼerla</ts>
                  <nts id="Seg_4906" n="HIAT:ip">?</nts>
                  <nts id="Seg_4907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-SE">
            <ts e="T101" id="Seg_4908" n="sc" s="T79">
               <ts e="T80" id="Seg_4910" n="e" s="T79">– Anɨ, </ts>
               <ts e="T81" id="Seg_4912" n="e" s="T80">anɨ, </ts>
               <ts e="T82" id="Seg_4914" n="e" s="T81">anɨ, </ts>
               <ts e="T83" id="Seg_4916" n="e" s="T82">kün </ts>
               <ts e="T84" id="Seg_4918" n="e" s="T83">aːjɨ, </ts>
               <ts e="T85" id="Seg_4920" n="e" s="T84">eː, </ts>
               <ts e="T86" id="Seg_4922" n="e" s="T85">(ispe-) </ts>
               <ts e="T87" id="Seg_4924" n="e" s="T86">(is-) </ts>
               <ts e="T88" id="Seg_4926" n="e" s="T87">ispite, </ts>
               <ts e="T89" id="Seg_4928" n="e" s="T88">vot, </ts>
               <ts e="T90" id="Seg_4930" n="e" s="T89">pʼjut </ts>
               <ts e="T91" id="Seg_4932" n="e" s="T90">šʼas </ts>
               <ts e="T94" id="Seg_4934" n="e" s="T91">každɨj </ts>
               <ts e="T97" id="Seg_4936" n="e" s="T94">dʼenʼ. </ts>
               <ts e="T98" id="Seg_4938" n="e" s="T97">– Aː, </ts>
               <ts e="T99" id="Seg_4940" n="e" s="T98">iheller, </ts>
               <ts e="T101" id="Seg_4942" n="e" s="T99">da. </ts>
            </ts>
            <ts e="T156" id="Seg_4943" n="sc" s="T150">
               <ts e="T151" id="Seg_4945" n="e" s="T150">– Kak </ts>
               <ts e="T152" id="Seg_4947" n="e" s="T151">anʼi </ts>
               <ts e="T153" id="Seg_4949" n="e" s="T152">zavtra </ts>
               <ts e="T154" id="Seg_4951" n="e" s="T153">sarʼevnavatʼ </ts>
               <ts e="T155" id="Seg_4953" n="e" s="T154">budut, </ts>
               <ts e="T156" id="Seg_4955" n="e" s="T155">e? </ts>
            </ts>
            <ts e="T179" id="Seg_4956" n="sc" s="T173">
               <ts e="T174" id="Seg_4958" n="e" s="T173">– I </ts>
               <ts e="T175" id="Seg_4960" n="e" s="T174">ješʼo </ts>
               <ts e="T176" id="Seg_4962" n="e" s="T175">olʼenʼa </ts>
               <ts e="T177" id="Seg_4964" n="e" s="T176">vkormʼat </ts>
               <ts e="T178" id="Seg_4966" n="e" s="T177">tože, </ts>
               <ts e="T179" id="Seg_4968" n="e" s="T178">aragiːjem. </ts>
            </ts>
            <ts e="T257" id="Seg_4969" n="sc" s="T254">
               <ts e="T256" id="Seg_4971" n="e" s="T254">– Harsɨn, </ts>
               <ts e="T257" id="Seg_4973" n="e" s="T256">da? </ts>
            </ts>
            <ts e="T296" id="Seg_4974" n="sc" s="T287">
               <ts e="T288" id="Seg_4976" n="e" s="T287">– Vɨ </ts>
               <ts e="T289" id="Seg_4978" n="e" s="T288">na </ts>
               <ts e="T290" id="Seg_4980" n="e" s="T289">sankax </ts>
               <ts e="T291" id="Seg_4982" n="e" s="T290">nʼe </ts>
               <ts e="T292" id="Seg_4984" n="e" s="T291">pajedʼete </ts>
               <ts e="T293" id="Seg_4986" n="e" s="T292">tuda, </ts>
               <ts e="T294" id="Seg_4988" n="e" s="T293">da, </ts>
               <ts e="T296" id="Seg_4990" n="e" s="T294">zavtra? </ts>
            </ts>
            <ts e="T340" id="Seg_4991" n="sc" s="T335">
               <ts e="T336" id="Seg_4993" n="e" s="T335">– Ginini </ts>
               <ts e="T337" id="Seg_4995" n="e" s="T336">bilegit, </ts>
               <ts e="T338" id="Seg_4997" n="e" s="T337">bilegin </ts>
               <ts e="T340" id="Seg_4999" n="e" s="T338">da? </ts>
            </ts>
            <ts e="T380" id="Seg_5000" n="sc" s="T376">
               <ts e="T377" id="Seg_5002" n="e" s="T376">– Ana </ts>
               <ts e="T378" id="Seg_5004" n="e" s="T377">včʼera, </ts>
               <ts e="T379" id="Seg_5006" n="e" s="T378">da, </ts>
               <ts e="T380" id="Seg_5008" n="e" s="T379">umʼerla? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-SE">
            <ta e="T97" id="Seg_5009" s="T79">KiPP_2009_Story_nar.ES.001 (001.015)</ta>
            <ta e="T101" id="Seg_5010" s="T97">KiPP_2009_Story_nar.ES.002 (001.017)</ta>
            <ta e="T156" id="Seg_5011" s="T150">KiPP_2009_Story_nar.ES.003 (001.026)</ta>
            <ta e="T179" id="Seg_5012" s="T173">KiPP_2009_Story_nar.ES.004 (001.031)</ta>
            <ta e="T257" id="Seg_5013" s="T254">KiPP_2009_Story_nar.ES.005 (001.044)</ta>
            <ta e="T296" id="Seg_5014" s="T287">KiPP_2009_Story_nar.ES.006 (001.050)</ta>
            <ta e="T340" id="Seg_5015" s="T335">KiPP_2009_Story_nar.ES.007 (001.059)</ta>
            <ta e="T380" id="Seg_5016" s="T376">KiPP_2009_Story_nar.ES.008 (001.068)</ta>
         </annotation>
         <annotation name="st" tierref="st-SE" />
         <annotation name="ts" tierref="ts-SE">
            <ta e="T97" id="Seg_5017" s="T79">– Anɨ, anɨ, anɨ, kün aːjɨ, eː, (ispe-) (is-) ispite, vot, pʼjut šʼas každɨj dʼenʼ. </ta>
            <ta e="T101" id="Seg_5018" s="T97">– Aː, iheller, da. </ta>
            <ta e="T156" id="Seg_5019" s="T150">– Kak anʼi zavtra sarʼevnavatʼ budut, e? </ta>
            <ta e="T179" id="Seg_5020" s="T173">– I ješʼo olʼenʼa vkormʼat tože, aragiːjem. </ta>
            <ta e="T257" id="Seg_5021" s="T254">– Harsɨn, da? </ta>
            <ta e="T296" id="Seg_5022" s="T287">– Vɨ na sankax nʼe pajedʼete tuda, da, zavtra? </ta>
            <ta e="T340" id="Seg_5023" s="T335">– Ginini bilegit, bilegin da? </ta>
            <ta e="T380" id="Seg_5024" s="T376">– Ana včʼera, da, umʼerla? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-SE">
            <ta e="T80" id="Seg_5025" s="T79">anɨ</ta>
            <ta e="T81" id="Seg_5026" s="T80">anɨ</ta>
            <ta e="T82" id="Seg_5027" s="T81">anɨ</ta>
            <ta e="T83" id="Seg_5028" s="T82">kün</ta>
            <ta e="T84" id="Seg_5029" s="T83">aːjɨ</ta>
            <ta e="T85" id="Seg_5030" s="T84">eː</ta>
            <ta e="T87" id="Seg_5031" s="T86">is</ta>
            <ta e="T88" id="Seg_5032" s="T87">is-pit-e</ta>
            <ta e="T98" id="Seg_5033" s="T97">aː</ta>
            <ta e="T99" id="Seg_5034" s="T98">ih-el-ler</ta>
            <ta e="T101" id="Seg_5035" s="T99">da</ta>
            <ta e="T256" id="Seg_5036" s="T254">harsɨn</ta>
            <ta e="T257" id="Seg_5037" s="T256">da</ta>
            <ta e="T336" id="Seg_5038" s="T335">gini-ni</ta>
            <ta e="T337" id="Seg_5039" s="T336">bil-e-git</ta>
            <ta e="T338" id="Seg_5040" s="T337">bil-e-gin</ta>
            <ta e="T340" id="Seg_5041" s="T338">da</ta>
         </annotation>
         <annotation name="mp" tierref="mp-SE">
            <ta e="T80" id="Seg_5042" s="T79">anɨ</ta>
            <ta e="T81" id="Seg_5043" s="T80">anɨ</ta>
            <ta e="T82" id="Seg_5044" s="T81">anɨ</ta>
            <ta e="T83" id="Seg_5045" s="T82">kün</ta>
            <ta e="T84" id="Seg_5046" s="T83">aːjɨ</ta>
            <ta e="T85" id="Seg_5047" s="T84">eː</ta>
            <ta e="T87" id="Seg_5048" s="T86">is</ta>
            <ta e="T88" id="Seg_5049" s="T87">is-BIT-tA</ta>
            <ta e="T98" id="Seg_5050" s="T97">aː</ta>
            <ta e="T99" id="Seg_5051" s="T98">is-Ar-LAr</ta>
            <ta e="T101" id="Seg_5052" s="T99">da</ta>
            <ta e="T256" id="Seg_5053" s="T254">harsɨn</ta>
            <ta e="T257" id="Seg_5054" s="T256">da</ta>
            <ta e="T336" id="Seg_5055" s="T335">gini-nI</ta>
            <ta e="T337" id="Seg_5056" s="T336">bil-A-GIt</ta>
            <ta e="T338" id="Seg_5057" s="T337">bil-A-GIn</ta>
            <ta e="T340" id="Seg_5058" s="T338">da</ta>
         </annotation>
         <annotation name="ge" tierref="ge-SE">
            <ta e="T80" id="Seg_5059" s="T79">now</ta>
            <ta e="T81" id="Seg_5060" s="T80">now</ta>
            <ta e="T82" id="Seg_5061" s="T81">now</ta>
            <ta e="T83" id="Seg_5062" s="T82">day.[NOM]</ta>
            <ta e="T84" id="Seg_5063" s="T83">every</ta>
            <ta e="T85" id="Seg_5064" s="T84">eh</ta>
            <ta e="T87" id="Seg_5065" s="T86">drink</ta>
            <ta e="T88" id="Seg_5066" s="T87">drink-PST2-3SG</ta>
            <ta e="T98" id="Seg_5067" s="T97">ah</ta>
            <ta e="T99" id="Seg_5068" s="T98">drink-PRS-3PL</ta>
            <ta e="T101" id="Seg_5069" s="T99">yes</ta>
            <ta e="T256" id="Seg_5070" s="T254">tomorrow</ta>
            <ta e="T257" id="Seg_5071" s="T256">yes</ta>
            <ta e="T336" id="Seg_5072" s="T335">3SG-ACC</ta>
            <ta e="T337" id="Seg_5073" s="T336">know-PRS-2PL</ta>
            <ta e="T338" id="Seg_5074" s="T337">know-PRS-2SG</ta>
            <ta e="T340" id="Seg_5075" s="T338">yes</ta>
         </annotation>
         <annotation name="gg" tierref="gg-SE">
            <ta e="T80" id="Seg_5076" s="T79">jetzt</ta>
            <ta e="T81" id="Seg_5077" s="T80">jetzt</ta>
            <ta e="T82" id="Seg_5078" s="T81">jetzt</ta>
            <ta e="T83" id="Seg_5079" s="T82">Tag.[NOM]</ta>
            <ta e="T84" id="Seg_5080" s="T83">jeder</ta>
            <ta e="T85" id="Seg_5081" s="T84">äh</ta>
            <ta e="T87" id="Seg_5082" s="T86">trinken</ta>
            <ta e="T88" id="Seg_5083" s="T87">trinken-PST2-3SG</ta>
            <ta e="T98" id="Seg_5084" s="T97">ah</ta>
            <ta e="T99" id="Seg_5085" s="T98">trinken-PRS-3PL</ta>
            <ta e="T101" id="Seg_5086" s="T99">ja</ta>
            <ta e="T256" id="Seg_5087" s="T254">morgen</ta>
            <ta e="T257" id="Seg_5088" s="T256">ja</ta>
            <ta e="T336" id="Seg_5089" s="T335">3SG-ACC</ta>
            <ta e="T337" id="Seg_5090" s="T336">wissen-PRS-2PL</ta>
            <ta e="T338" id="Seg_5091" s="T337">wissen-PRS-2SG</ta>
            <ta e="T340" id="Seg_5092" s="T338">ja</ta>
         </annotation>
         <annotation name="gr" tierref="gr-SE">
            <ta e="T80" id="Seg_5093" s="T79">теперь</ta>
            <ta e="T81" id="Seg_5094" s="T80">теперь</ta>
            <ta e="T82" id="Seg_5095" s="T81">теперь</ta>
            <ta e="T83" id="Seg_5096" s="T82">день.[NOM]</ta>
            <ta e="T84" id="Seg_5097" s="T83">каждый</ta>
            <ta e="T85" id="Seg_5098" s="T84">ээ</ta>
            <ta e="T87" id="Seg_5099" s="T86">пить</ta>
            <ta e="T88" id="Seg_5100" s="T87">пить-PST2-3SG</ta>
            <ta e="T98" id="Seg_5101" s="T97">ах</ta>
            <ta e="T99" id="Seg_5102" s="T98">пить-PRS-3PL</ta>
            <ta e="T101" id="Seg_5103" s="T99">да</ta>
            <ta e="T256" id="Seg_5104" s="T254">завтра</ta>
            <ta e="T257" id="Seg_5105" s="T256">да</ta>
            <ta e="T336" id="Seg_5106" s="T335">3SG-ACC</ta>
            <ta e="T337" id="Seg_5107" s="T336">знать-PRS-2PL</ta>
            <ta e="T338" id="Seg_5108" s="T337">знать-PRS-2SG</ta>
            <ta e="T340" id="Seg_5109" s="T338">да</ta>
         </annotation>
         <annotation name="mc" tierref="mc-SE">
            <ta e="T80" id="Seg_5110" s="T79">adv</ta>
            <ta e="T81" id="Seg_5111" s="T80">adv</ta>
            <ta e="T82" id="Seg_5112" s="T81">adv</ta>
            <ta e="T83" id="Seg_5113" s="T82">n.[n:case]</ta>
            <ta e="T84" id="Seg_5114" s="T83">adj</ta>
            <ta e="T85" id="Seg_5115" s="T84">interj</ta>
            <ta e="T87" id="Seg_5116" s="T86">v</ta>
            <ta e="T88" id="Seg_5117" s="T87">v-v:tense-v:poss.pn</ta>
            <ta e="T98" id="Seg_5118" s="T97">interj</ta>
            <ta e="T99" id="Seg_5119" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T101" id="Seg_5120" s="T99">ptcl</ta>
            <ta e="T256" id="Seg_5121" s="T254">adv</ta>
            <ta e="T257" id="Seg_5122" s="T256">ptcl</ta>
            <ta e="T336" id="Seg_5123" s="T335">pers-pro:case</ta>
            <ta e="T337" id="Seg_5124" s="T336">v-v:tense-v:pred.pn</ta>
            <ta e="T338" id="Seg_5125" s="T337">v-v:tense-v:pred.pn</ta>
            <ta e="T340" id="Seg_5126" s="T338">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-SE">
            <ta e="T80" id="Seg_5127" s="T79">adv</ta>
            <ta e="T81" id="Seg_5128" s="T80">adv</ta>
            <ta e="T82" id="Seg_5129" s="T81">adv</ta>
            <ta e="T83" id="Seg_5130" s="T82">n</ta>
            <ta e="T84" id="Seg_5131" s="T83">adj</ta>
            <ta e="T85" id="Seg_5132" s="T84">interj</ta>
            <ta e="T87" id="Seg_5133" s="T86">v</ta>
            <ta e="T88" id="Seg_5134" s="T87">v</ta>
            <ta e="T98" id="Seg_5135" s="T97">interj</ta>
            <ta e="T99" id="Seg_5136" s="T98">v</ta>
            <ta e="T101" id="Seg_5137" s="T99">ptcl</ta>
            <ta e="T256" id="Seg_5138" s="T254">adv</ta>
            <ta e="T257" id="Seg_5139" s="T256">ptcl</ta>
            <ta e="T336" id="Seg_5140" s="T335">pers</ta>
            <ta e="T337" id="Seg_5141" s="T336">v</ta>
            <ta e="T338" id="Seg_5142" s="T337">v</ta>
            <ta e="T340" id="Seg_5143" s="T338">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-SE" />
         <annotation name="SyF" tierref="SyF-SE" />
         <annotation name="IST" tierref="IST-SE" />
         <annotation name="Top" tierref="Top-SE" />
         <annotation name="Foc" tierref="Foc-SE" />
         <annotation name="BOR" tierref="BOR-SE">
            <ta e="T101" id="Seg_5144" s="T99">RUS:disc</ta>
            <ta e="T257" id="Seg_5145" s="T256">RUS:disc</ta>
            <ta e="T340" id="Seg_5146" s="T338">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-SE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-SE" />
         <annotation name="CS" tierref="CS-SE" />
         <annotation name="fe" tierref="fe-SE">
            <ta e="T97" id="Seg_5147" s="T79">– Now, now, now, every day, eh, one drinks, they drink every day.</ta>
            <ta e="T101" id="Seg_5148" s="T97">– Ah, they drink, yes.</ta>
            <ta e="T156" id="Seg_5149" s="T150">– How will they compete tomorrow?</ta>
            <ta e="T179" id="Seg_5150" s="T173">– And also the reindeer are fed, with spirit.</ta>
            <ta e="T257" id="Seg_5151" s="T254">– Tomorrow, right?</ta>
            <ta e="T296" id="Seg_5152" s="T287">– You won't go there by sledge, tomorrow?</ta>
            <ta e="T340" id="Seg_5153" s="T335">– Did You know her, did you know, yes?</ta>
            <ta e="T380" id="Seg_5154" s="T376">– She died yesterday, right?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-SE">
            <ta e="T97" id="Seg_5155" s="T79">– Jetzt, jetzt, jetzt, jeden Tag, äh, trinkt man, nun, jeden Tag trinken sie.</ta>
            <ta e="T101" id="Seg_5156" s="T97">– Ah, sie trinken, ja.</ta>
            <ta e="T156" id="Seg_5157" s="T150">– Wie werden sie morgen den Wettkampf bestreiten?</ta>
            <ta e="T179" id="Seg_5158" s="T173">– Und auch die Rentiere verköstigen sie, mit Schnaps.</ta>
            <ta e="T257" id="Seg_5159" s="T254">– Morgen, ja?</ta>
            <ta e="T296" id="Seg_5160" s="T287">– Sie fahren morgen nicht mit dem Schlitten dorthin, morgen?</ta>
            <ta e="T340" id="Seg_5161" s="T335">– Kannten Sie sie, kanntest du sie, ja?</ta>
            <ta e="T380" id="Seg_5162" s="T376">– Sie ist gestern gestorben, ja?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-SE">
            <ta e="T97" id="Seg_5163" s="T79">– Сейчас, сейчас, сейчас каждый день, ээ, выпивают, вот, пьют сейчас каждый день.</ta>
            <ta e="T101" id="Seg_5164" s="T97">– Эх, пьют, да.</ta>
            <ta e="T156" id="Seg_5165" s="T150">– Как они завтра соревноваться будут, э?</ta>
            <ta e="T179" id="Seg_5166" s="T173">– И оленей они угощают тоже, спиртным.</ta>
            <ta e="T257" id="Seg_5167" s="T254">– Завтра, да?</ta>
            <ta e="T296" id="Seg_5168" s="T287">– Вы на нартах не поедете туда, да, завтра?</ta>
            <ta e="T340" id="Seg_5169" s="T335">– Вы ее знали, ты ее знал, да?</ta>
            <ta e="T380" id="Seg_5170" s="T376">– Она вчера умерла, да?</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-SE" />
         <annotation name="nt" tierref="nt-SE" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KiPP"
                          name="ref"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KiPP"
                          name="st"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KiPP"
                          name="ts"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KiPP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KiPP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KiPP"
                          name="mb"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KiPP"
                          name="mp"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KiPP"
                          name="ge"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KiPP"
                          name="gg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KiPP"
                          name="gr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KiPP"
                          name="mc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KiPP"
                          name="ps"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KiPP"
                          name="SeR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KiPP"
                          name="SyF"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KiPP"
                          name="IST"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KiPP"
                          name="Top"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KiPP"
                          name="Foc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KiPP"
                          name="BOR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KiPP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KiPP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KiPP"
                          name="CS"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KiPP"
                          name="fe"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KiPP"
                          name="fg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KiPP"
                          name="fr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KiPP"
                          name="ltr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KiPP"
                          name="nt"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-SE"
                          name="ref"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-SE"
                          name="st"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-SE"
                          name="ts"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-SE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-SE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-SE"
                          name="mb"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-SE"
                          name="mp"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-SE"
                          name="ge"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-SE"
                          name="gg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-SE"
                          name="gr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-SE"
                          name="mc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-SE"
                          name="ps"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-SE"
                          name="SeR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-SE"
                          name="SyF"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-SE"
                          name="IST"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-SE"
                          name="Top"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-SE"
                          name="Foc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-SE"
                          name="BOR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-SE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-SE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-SE"
                          name="CS"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-SE"
                          name="fe"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-SE"
                          name="fg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-SE"
                          name="fr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-SE"
                          name="ltr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-SE"
                          name="nt"
                          segmented-tier-id="tx-SE"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
