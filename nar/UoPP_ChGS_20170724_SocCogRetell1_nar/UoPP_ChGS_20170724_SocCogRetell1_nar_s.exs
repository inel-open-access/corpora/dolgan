<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID54BE33F4-FAB6-D334-137B-8CCB6B3016F8">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>UoPP_ChGS_20170724_SocCogRetelling1_nar</transcription-name>
         <referenced-file url="UoPP_ChGS_20170724_SocCogRetell1_nar.wav" />
         <referenced-file url="UoPP_ChGS_20170724_SocCogRetell1_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\UoPP_ChGS_20170724_SocCogRetell1_nar\UoPP_ChGS_20170724_SocCogRetell1_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">573</ud-information>
            <ud-information attribute-name="# HIAT:w">318</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# e">320</ud-information>
            <ud-information attribute-name="# HIAT:u">66</ud-information>
            <ud-information attribute-name="# sc">114</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="DCh">
            <abbreviation>DCh</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="UoPP">
            <abbreviation>UoPP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="ChGS">
            <abbreviation>ChGS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.031" type="appl" />
         <tli id="T2" time="0.8974285714285715" type="appl" />
         <tli id="T3" time="1.7638571428571428" type="appl" />
         <tli id="T4" time="2.6302857142857143" type="appl" />
         <tli id="T5" time="3.496714285714286" type="appl" />
         <tli id="T6" time="4.363142857142857" type="appl" />
         <tli id="T7" time="5.229571428571428" type="appl" />
         <tli id="T8" time="6.096" type="appl" />
         <tli id="T9" time="6.116" type="appl" />
         <tli id="T10" time="6.846" type="appl" />
         <tli id="T11" time="7.576" type="appl" />
         <tli id="T12" time="8.306" type="appl" />
         <tli id="T13" time="9.036" type="appl" />
         <tli id="T14" time="9.766" type="appl" />
         <tli id="T15" time="10.495999999999999" type="appl" />
         <tli id="T16" time="11.225999999999999" type="appl" />
         <tli id="T17" time="11.956" type="appl" />
         <tli id="T18" time="11.961" type="appl" />
         <tli id="T19" time="12.60375" type="appl" />
         <tli id="T20" time="13.246500000000001" type="appl" />
         <tli id="T21" time="13.88925" type="appl" />
         <tli id="T22" time="14.456" type="appl" />
         <tli id="T23" time="14.532" type="appl" />
         <tli id="T24" time="14.875" type="appl" />
         <tli id="T25" time="15.293999999999999" type="appl" />
         <tli id="T26" time="15.713" type="appl" />
         <tli id="T27" time="15.72" type="appl" />
         <tli id="T28" time="16.221600000000002" type="appl" />
         <tli id="T29" time="16.723200000000002" type="appl" />
         <tli id="T30" time="17.224800000000002" type="appl" />
         <tli id="T31" time="17.269" type="appl" />
         <tli id="T32" time="17.28" type="appl" />
         <tli id="T33" time="17.7264" type="appl" />
         <tli id="T34" time="17.945500000000003" type="appl" />
         <tli id="T35" time="18.14633333333333" type="appl" />
         <tli id="T36" time="18.228" type="appl" />
         <tli id="T37" time="18.611" type="appl" />
         <tli id="T38" time="19.023666666666667" type="appl" />
         <tli id="T39" time="19.2765" type="appl" />
         <tli id="T40" time="19.901" type="appl" />
         <tli id="T41" time="19.902" type="appl" />
         <tli id="T42" time="19.942" type="appl" />
         <tli id="T43" time="20.44425" type="appl" />
         <tli id="T44" time="20.6075" type="appl" />
         <tli id="T45" time="20.9865" type="appl" />
         <tli id="T46" time="21.273" type="appl" />
         <tli id="T47" time="21.528750000000002" type="appl" />
         <tli id="T48" time="21.938499999999998" type="appl" />
         <tli id="T49" time="22.071" type="appl" />
         <tli id="T50" time="22.604" type="appl" />
         <tli id="T51" time="22.609999785038337" />
         <tli id="T52" time="23.1" type="appl" />
         <tli id="T53" time="24.005" type="appl" />
         <tli id="T54" time="24.562583333333333" type="appl" />
         <tli id="T55" time="25.120166666666666" type="appl" />
         <tli id="T56" time="25.67775" type="appl" />
         <tli id="T57" time="26.235333333333333" type="appl" />
         <tli id="T58" time="26.792916666666667" type="appl" />
         <tli id="T59" time="27.3505" type="appl" />
         <tli id="T60" time="27.908083333333334" type="appl" />
         <tli id="T61" time="28.465666666666667" type="appl" />
         <tli id="T62" time="29.02325" type="appl" />
         <tli id="T63" time="29.580833333333334" type="appl" />
         <tli id="T64" time="30.138416666666668" type="appl" />
         <tli id="T65" time="30.696" type="appl" />
         <tli id="T66" time="31.327285714285715" type="appl" />
         <tli id="T67" time="31.95857142857143" type="appl" />
         <tli id="T68" time="32.58985714285714" type="appl" />
         <tli id="T69" time="33.22114285714286" type="appl" />
         <tli id="T70" time="33.852428571428575" type="appl" />
         <tli id="T71" time="34.483714285714285" type="appl" />
         <tli id="T72" time="35.04833430338603" />
         <tli id="T73" time="35.06766969369414" />
         <tli id="T74" time="35.590500000000006" type="appl" />
         <tli id="T75" time="36.12564431028519" />
         <tli id="T76" time="36.5295" type="appl" />
         <tli id="T77" time="36.999" type="appl" />
         <tli id="T78" time="37.468500000000006" type="appl" />
         <tli id="T79" time="38.03132931165717" />
         <tli id="T80" time="38.03866308848615" />
         <tli id="T81" time="38.525" type="appl" />
         <tli id="T82" time="39.098" type="appl" />
         <tli id="T83" time="39.671" type="appl" />
         <tli id="T84" time="40.244" type="appl" />
         <tli id="T85" time="40.896996911129406" />
         <tli id="T86" time="41.8255" type="appl" />
         <tli id="T87" time="42.834" type="appl" />
         <tli id="T88" time="43.8425" type="appl" />
         <tli id="T89" time="44.851" type="appl" />
         <tli id="T90" time="46.3994" type="appl" />
         <tli id="T91" time="47.542" type="appl" />
         <tli id="T92" time="47.9478" type="appl" />
         <tli id="T93" time="48.12533333333334" type="appl" />
         <tli id="T94" time="48.708666666666666" type="appl" />
         <tli id="T95" time="49.292" type="appl" />
         <tli id="T96" time="49.4962" type="appl" />
         <tli id="T97" time="51.0446" type="appl" />
         <tli id="T98" time="51.296" type="appl" />
         <tli id="T99" time="51.7474" type="appl" />
         <tli id="T100" time="52.1988" type="appl" />
         <tli id="T101" time="52.593" type="appl" />
         <tli id="T102" time="52.6502" type="appl" />
         <tli id="T103" time="53.1016" type="appl" />
         <tli id="T104" time="53.49966759907305" />
         <tli id="T105" time="53.501334218574364" />
         <tli id="T106" time="54.1164" type="appl" />
         <tli id="T107" time="54.6648" type="appl" />
         <tli id="T108" time="55.2132" type="appl" />
         <tli id="T109" time="55.7616" type="appl" />
         <tli id="T110" time="56.31" type="appl" />
         <tli id="T111" time="56.325" type="appl" />
         <tli id="T112" time="56.81766666666667" type="appl" />
         <tli id="T113" time="57.31033333333333" type="appl" />
         <tli id="T114" time="57.896330282815306" />
         <tli id="T115" time="57.899995543671686" />
         <tli id="T116" time="58.71628571428571" type="appl" />
         <tli id="T117" time="59.61257142857143" type="appl" />
         <tli id="T118" time="60.50885714285714" type="appl" />
         <tli id="T119" time="61.405142857142856" type="appl" />
         <tli id="T120" time="62.301428571428566" type="appl" />
         <tli id="T121" time="63.197714285714284" type="appl" />
         <tli id="T122" time="64.094" type="appl" />
         <tli id="T123" time="64.866" type="appl" />
         <tli id="T124" time="65.63799999999999" type="appl" />
         <tli id="T125" time="66.21667424401541" />
         <tli id="T126" time="66.31700343594748" />
         <tli id="T127" time="67.14233333333333" type="appl" />
         <tli id="T128" time="67.84766666666667" type="appl" />
         <tli id="T129" time="68.553" type="appl" />
         <tli id="T130" time="69.25833333333333" type="appl" />
         <tli id="T131" time="69.96366666666667" type="appl" />
         <tli id="T132" time="70.669" type="appl" />
         <tli id="T133" time="71.37433333333333" type="appl" />
         <tli id="T134" time="72.07966666666667" type="appl" />
         <tli id="T135" time="72.67166734884485" />
         <tli id="T136" time="72.69966785851325" />
         <tli id="T137" time="73.3952" type="appl" />
         <tli id="T138" time="73.9974" type="appl" />
         <tli id="T139" time="74.59960000000001" type="appl" />
         <tli id="T140" time="75.2018" type="appl" />
         <tli id="T141" time="75.804" type="appl" />
         <tli id="T142" time="76.844" type="appl" />
         <tli id="T143" time="77.884" type="appl" />
         <tli id="T144" time="78.924" type="appl" />
         <tli id="T145" time="79.524416124265" />
         <tli id="T146" time="82.19100732635286" />
         <tli id="T147" time="82.864" type="appl" />
         <tli id="T148" time="83.636" type="appl" />
         <tli id="T149" time="84.06134502076259" />
         <tli id="T150" time="84.40833389888778" />
         <tli id="T151" time="84.934" type="appl" />
         <tli id="T152" time="85.04528571428573" type="appl" />
         <tli id="T153" time="85.54014285714285" type="appl" />
         <tli id="T154" time="85.67557142857143" type="appl" />
         <tli id="T155" time="86.14628571428571" type="appl" />
         <tli id="T156" time="86.30585714285715" type="appl" />
         <tli id="T157" time="86.75242857142857" type="appl" />
         <tli id="T158" time="86.93614285714287" type="appl" />
         <tli id="T159" time="87.35857142857144" type="appl" />
         <tli id="T160" time="87.56642857142857" type="appl" />
         <tli id="T161" time="87.9647142857143" type="appl" />
         <tli id="T162" time="88.1967142857143" type="appl" />
         <tli id="T163" time="88.57085714285715" type="appl" />
         <tli id="T164" time="88.827" type="appl" />
         <tli id="T165" time="89.14214285714286" type="intp" />
         <tli id="T166" time="89.45728571428572" type="appl" />
         <tli id="T167" time="90.08757142857144" type="appl" />
         <tli id="T168" time="90.71785714285714" type="appl" />
         <tli id="T169" time="91.34814285714286" type="appl" />
         <tli id="T170" time="91.97842857142858" type="appl" />
         <tli id="T171" time="92.60871428571429" type="appl" />
         <tli id="T172" time="92.93070339276169" />
         <tli id="T173" time="93.39069037512185" />
         <tli id="T174" time="93.7758" type="appl" />
         <tli id="T175" time="94.3096" type="appl" />
         <tli id="T176" time="94.8434" type="appl" />
         <tli id="T177" time="95.3772" type="appl" />
         <tli id="T178" time="95.46396503474516" />
         <tli id="T179" time="96.01728270917839" />
         <tli id="T180" time="96.5695" type="appl" />
         <tli id="T181" time="97.219" type="appl" />
         <tli id="T182" time="97.8685" type="appl" />
         <tli id="T183" time="98.518" type="appl" />
         <tli id="T184" time="99.27699514675797" />
         <tli id="T185" time="99.54699757337899" type="intp" />
         <tli id="T186" time="99.81700000000001" type="appl" />
         <tli id="T187" time="100.412" type="appl" />
         <tli id="T188" time="100.4665" type="appl" />
         <tli id="T189" time="100.92848199069269" type="intp" />
         <tli id="T190" time="101.3904639813854" />
         <tli id="T191" time="102.309" type="appl" />
         <tli id="T192" time="103.20374599880515" />
         <tli id="T193" time="104.789" type="appl" />
         <tli id="T194" time="105.48227272727273" type="appl" />
         <tli id="T195" time="106.17554545454546" type="appl" />
         <tli id="T196" time="106.86881818181818" type="appl" />
         <tli id="T197" time="107.56209090909091" type="appl" />
         <tli id="T198" time="108.25536363636364" type="appl" />
         <tli id="T199" time="108.94863636363637" type="appl" />
         <tli id="T200" time="109.6419090909091" type="appl" />
         <tli id="T201" time="110.33518181818182" type="appl" />
         <tli id="T202" time="111.02845454545455" type="appl" />
         <tli id="T203" time="111.72172727272728" type="appl" />
         <tli id="T204" time="112.17682539383078" />
         <tli id="T205" time="114.17010231739143" />
         <tli id="T206" time="115.21977777777778" type="appl" />
         <tli id="T207" time="115.96655555555556" type="appl" />
         <tli id="T208" time="116.71333333333334" type="appl" />
         <tli id="T209" time="117.46011111111112" type="appl" />
         <tli id="T210" time="118.20688888888888" type="appl" />
         <tli id="T211" time="118.95366666666666" type="appl" />
         <tli id="T212" time="119.70044444444444" type="appl" />
         <tli id="T213" time="120.44722222222222" type="appl" />
         <tli id="T214" time="120.58325415841274" />
         <tli id="T215" time="122.04987931956103" />
         <tli id="T216" time="122.83652372417696" />
         <tli id="T217" time="123.99649089708517" />
         <tli id="T218" time="124.28642857142857" type="appl" />
         <tli id="T219" time="124.89257142857143" type="appl" />
         <tli id="T220" time="125.0650023248593" />
         <tli id="T221" time="125.49871428571429" type="appl" />
         <tli id="T222" time="125.68325" type="appl" />
         <tli id="T223" time="126.10485714285714" type="appl" />
         <tli id="T224" time="126.3615" type="appl" />
         <tli id="T225" time="126.711" type="appl" />
         <tli id="T226" time="127.03975" type="appl" />
         <tli id="T227" time="127.718" type="appl" />
         <tli id="T228" time="131.151" type="appl" />
         <tli id="T229" time="133.042" type="appl" />
         <tli id="T230" time="133.335" type="appl" />
         <tli id="T231" time="133.66816666666668" type="appl" />
         <tli id="T232" time="134.29433333333333" type="appl" />
         <tli id="T233" time="134.902" type="appl" />
         <tli id="T234" time="134.9205" type="appl" />
         <tli id="T235" time="135.54666666666668" type="appl" />
         <tli id="T236" time="135.8395" type="appl" />
         <tli id="T237" time="136.049" type="appl" />
         <tli id="T238" time="136.17283333333333" type="appl" />
         <tli id="T239" time="136.777" type="appl" />
         <tli id="T240" time="136.787" type="appl" />
         <tli id="T241" time="136.799" type="appl" />
         <tli id="T242" time="136.88" type="appl" />
         <tli id="T243" time="137.3345" type="appl" />
         <tli id="T244" time="137.882" type="appl" />
         <tli id="T245" time="138.42950000000002" type="appl" />
         <tli id="T246" time="138.977" type="appl" />
         <tli id="T247" time="139.5245" type="appl" />
         <tli id="T248" time="140.072" type="appl" />
         <tli id="T249" time="140.61950000000002" type="appl" />
         <tli id="T250" time="141.167" type="appl" />
         <tli id="T251" time="141.7145" type="appl" />
         <tli id="T252" time="142.262" type="appl" />
         <tli id="T253" time="142.8095" type="appl" />
         <tli id="T254" time="143.357" type="appl" />
         <tli id="T255" time="144.12175" type="appl" />
         <tli id="T256" time="144.489" type="appl" />
         <tli id="T257" time="144.8865" type="appl" />
         <tli id="T258" time="145.07828571428573" type="appl" />
         <tli id="T259" time="145.65125" type="appl" />
         <tli id="T260" time="145.66757142857142" type="appl" />
         <tli id="T261" time="146.25685714285714" type="appl" />
         <tli id="T262" time="146.416" type="appl" />
         <tli id="T263" time="146.84614285714287" type="appl" />
         <tli id="T264" time="147.4354285714286" type="appl" />
         <tli id="T265" time="148.02471428571428" type="appl" />
         <tli id="T266" time="148.054" type="appl" />
         <tli id="T267" time="148.614" type="appl" />
         <tli id="T268" time="148.70233333333334" type="appl" />
         <tli id="T269" time="149.238" type="appl" />
         <tli id="T270" time="149.35066666666665" type="appl" />
         <tli id="T271" time="149.999" type="appl" />
         <tli id="T272" time="150.216" type="appl" />
         <tli id="T273" time="150.64733333333334" type="appl" />
         <tli id="T274" time="151.29566666666665" type="appl" />
         <tli id="T275" time="151.944" type="appl" />
         <tli id="T276" time="151.95" type="appl" />
         <tli id="T277" time="152.53099999999998" type="appl" />
         <tli id="T278" time="153.112" type="appl" />
         <tli id="T279" time="153.69299999999998" type="appl" />
         <tli id="T280" time="154.274" type="appl" />
         <tli id="T281" time="154.855" type="appl" />
         <tli id="T282" time="155.436" type="appl" />
         <tli id="T283" time="156.017" type="appl" />
         <tli id="T284" time="156.598" type="appl" />
         <tli id="T285" time="156.88567732001997" />
         <tli id="T286" time="157.13833944051072" />
         <tli id="T287" time="157.989" type="appl" />
         <tli id="T288" time="158.793" type="appl" />
         <tli id="T289" time="158.98367784240088" />
         <tli id="T290" time="159.4976737129752" />
         <tli id="T291" time="160.5236" type="appl" />
         <tli id="T292" time="161.43619999999999" type="appl" />
         <tli id="T293" time="162.3488" type="appl" />
         <tli id="T294" time="163.2614" type="appl" />
         <tli id="T295" time="164.168" type="appl" />
         <tli id="T296" time="164.174" type="appl" />
         <tli id="T297" time="164.672" type="appl" />
         <tli id="T298" time="165.50325" type="appl" />
         <tli id="T299" time="165.558" type="appl" />
         <tli id="T300" time="166.064" type="appl" />
         <tli id="T301" time="166.3345" type="appl" />
         <tli id="T302" time="167.16575" type="appl" />
         <tli id="T303" time="167.557" type="appl" />
         <tli id="T304" time="167.997" type="appl" />
         <tli id="T305" time="168.82825" type="appl" />
         <tli id="T306" time="169.6595" type="appl" />
         <tli id="T307" time="170.49075" type="appl" />
         <tli id="T308" time="171.1553385925168" />
         <tli id="T309" time="171.65514215640033" />
         <tli id="T310" time="173.32532926507525" />
         <tli id="T311" time="173.38666867500217" />
         <tli id="T312" time="173.983" type="appl" />
         <tli id="T313" time="174.061" type="appl" />
         <tli id="T314" time="174.586" type="appl" />
         <tli id="T315" time="174.7942857142857" type="appl" />
         <tli id="T316" time="175.52757142857143" type="appl" />
         <tli id="T317" time="176.26085714285713" type="appl" />
         <tli id="T318" time="176.99414285714286" type="appl" />
         <tli id="T319" time="177.72742857142856" type="appl" />
         <tli id="T320" time="178.4607142857143" type="appl" />
         <tli id="T321" time="179.00734294315296" />
         <tli id="T322" time="180.59488916139986" />
         <tli id="T323" time="180.77534458069994" type="intp" />
         <tli id="T324" time="180.9558" type="appl" />
         <tli id="T325" time="181.8322" type="appl" />
         <tli id="T326" time="182.70860000000002" type="appl" />
         <tli id="T327" time="183.585" type="appl" />
         <tli id="T328" time="184.0317" type="appl" />
         <tli id="T329" time="184.4784" type="appl" />
         <tli id="T330" time="184.92510000000001" type="appl" />
         <tli id="T331" time="185.3718" type="appl" />
         <tli id="T332" time="185.8185" type="appl" />
         <tli id="T333" time="186.2652" type="appl" />
         <tli id="T334" time="186.71189999999999" type="appl" />
         <tli id="T335" time="187.1586" type="appl" />
         <tli id="T336" time="187.6053" type="appl" />
         <tli id="T337" time="187.95199866340243" />
         <tli id="T338" time="188.46866666666665" type="appl" />
         <tli id="T339" time="188.88533333333334" type="appl" />
         <tli id="T340" time="189.302" type="appl" />
         <tli id="T341" time="190.43200660413325" />
         <tli id="T342" time="191.04466666666667" type="appl" />
         <tli id="T343" time="191.53733333333332" type="appl" />
         <tli id="T344" time="192.03" type="appl" />
         <tli id="T345" time="192.52266666666668" type="appl" />
         <tli id="T346" time="193.01533333333333" type="appl" />
         <tli id="T347" time="193.40800311254537" />
         <tli id="T348" time="193.81965812936772" />
         <tli id="T349" time="194.15266666666668" type="appl" />
         <tli id="T350" time="194.79233333333335" type="appl" />
         <tli id="T351" time="195.43200000000002" type="appl" />
         <tli id="T352" time="196.07166666666666" type="appl" />
         <tli id="T353" time="196.71133333333333" type="appl" />
         <tli id="T354" time="197.351" type="appl" />
         <tli id="T355" time="198.2885" type="appl" />
         <tli id="T356" time="199.027" type="appl" />
         <tli id="T357" time="199.23266900356296" />
         <tli id="T358" time="199.697" type="appl" />
         <tli id="T359" time="200.54765783102224" />
         <tli id="T360" time="200.76533333333333" type="appl" />
         <tli id="T361" time="201.82666666666668" type="appl" />
         <tli id="T362" time="202.40760519447852" />
         <tli id="T363" time="203.04299085888616" />
         <tli id="T364" time="203.506" type="appl" />
         <tli id="T365" time="203.92900745126738" />
         <tli id="T366" time="205.86084080118226" />
         <tli id="T367" time="206.56748746973557" />
         <tli id="T368" time="206.6895" type="appl" />
         <tli id="T369" time="207.97075" type="appl" />
         <tli id="T370" time="209.252" type="appl" />
         <tli id="T371" time="210.11599999999999" type="appl" />
         <tli id="T372" time="210.98" type="appl" />
         <tli id="T373" time="210.988" type="appl" />
         <tli id="T374" time="211.98" type="appl" />
         <tli id="T0" time="212.014" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-DCh"
                      id="tx-DCh"
                      speaker="DCh"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-DCh">
            <ts e="T8" id="Seg_0" n="sc" s="T1">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Eta</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">bɨla</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">ftaraja</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">zadačʼa</ts>
                  <nts id="Seg_16" n="HIAT:ip">,</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">sejčʼas</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">trʼetʼja</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">zadačʼa</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T17" id="Seg_28" n="sc" s="T9">
               <ts e="T17" id="Seg_30" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">Požalujsta</ts>
                  <nts id="Seg_33" n="HIAT:ip">,</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">odna</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">iz</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">Vas</ts>
                  <nts id="Seg_43" n="HIAT:ip">,</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">eː</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">rasskažite</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">što</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">praizašlo</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T23" id="Seg_60" n="sc" s="T18">
               <ts e="T23" id="Seg_62" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">Na</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">dalganskʼi</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">na</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_74" n="HIAT:w" s="T21">dalganskom</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T36" id="Seg_77" n="sc" s="T27">
               <ts e="T36" id="Seg_79" n="HIAT:u" s="T27">
                  <nts id="Seg_80" n="HIAT:ip">–</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_83" n="HIAT:w" s="T27">Vɨ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_86" n="HIAT:w" s="T28">ili</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_89" n="HIAT:w" s="T29">Vɨ</ts>
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_93" n="HIAT:w" s="T30">nʼe</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_96" n="HIAT:w" s="T33">važna</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T230" id="Seg_99" n="sc" s="T228">
               <ts e="T230" id="Seg_101" n="HIAT:u" s="T228">
                  <nts id="Seg_102" n="HIAT:ip">–</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_105" n="HIAT:w" s="T228">Mʼečʼtajdaːbɨt</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T242" id="Seg_108" n="sc" s="T237">
               <ts e="T242" id="Seg_110" n="HIAT:u" s="T237">
                  <nts id="Seg_111" n="HIAT:ip">–</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_114" n="HIAT:w" s="T237">Dalʼše</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T358" id="Seg_117" n="sc" s="T356">
               <ts e="T358" id="Seg_119" n="HIAT:u" s="T356">
                  <nts id="Seg_120" n="HIAT:ip">–</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_123" n="HIAT:w" s="T356">Fsʼo</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T372" id="Seg_126" n="sc" s="T370">
               <ts e="T372" id="Seg_128" n="HIAT:u" s="T370">
                  <nts id="Seg_129" n="HIAT:ip">–</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_132" n="HIAT:w" s="T370">Nʼičʼevo</ts>
                  <nts id="Seg_133" n="HIAT:ip">,</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_136" n="HIAT:w" s="T371">klassna</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-DCh">
            <ts e="T8" id="Seg_139" n="sc" s="T1">
               <ts e="T2" id="Seg_141" n="e" s="T1">– Eta </ts>
               <ts e="T3" id="Seg_143" n="e" s="T2">bɨla </ts>
               <ts e="T4" id="Seg_145" n="e" s="T3">ftaraja </ts>
               <ts e="T5" id="Seg_147" n="e" s="T4">zadačʼa, </ts>
               <ts e="T6" id="Seg_149" n="e" s="T5">sejčʼas </ts>
               <ts e="T7" id="Seg_151" n="e" s="T6">trʼetʼja </ts>
               <ts e="T8" id="Seg_153" n="e" s="T7">zadačʼa. </ts>
            </ts>
            <ts e="T17" id="Seg_154" n="sc" s="T9">
               <ts e="T10" id="Seg_156" n="e" s="T9">Požalujsta, </ts>
               <ts e="T11" id="Seg_158" n="e" s="T10">odna </ts>
               <ts e="T12" id="Seg_160" n="e" s="T11">iz </ts>
               <ts e="T13" id="Seg_162" n="e" s="T12">Vas, </ts>
               <ts e="T14" id="Seg_164" n="e" s="T13">eː, </ts>
               <ts e="T15" id="Seg_166" n="e" s="T14">rasskažite, </ts>
               <ts e="T16" id="Seg_168" n="e" s="T15">što </ts>
               <ts e="T17" id="Seg_170" n="e" s="T16">praizašlo. </ts>
            </ts>
            <ts e="T23" id="Seg_171" n="sc" s="T18">
               <ts e="T19" id="Seg_173" n="e" s="T18">Na </ts>
               <ts e="T20" id="Seg_175" n="e" s="T19">dalganskʼi, </ts>
               <ts e="T21" id="Seg_177" n="e" s="T20">na </ts>
               <ts e="T23" id="Seg_179" n="e" s="T21">dalganskom. </ts>
            </ts>
            <ts e="T36" id="Seg_180" n="sc" s="T27">
               <ts e="T28" id="Seg_182" n="e" s="T27">– Vɨ </ts>
               <ts e="T29" id="Seg_184" n="e" s="T28">ili </ts>
               <ts e="T30" id="Seg_186" n="e" s="T29">Vɨ, </ts>
               <ts e="T33" id="Seg_188" n="e" s="T30">nʼe </ts>
               <ts e="T36" id="Seg_190" n="e" s="T33">važna. </ts>
            </ts>
            <ts e="T230" id="Seg_191" n="sc" s="T228">
               <ts e="T230" id="Seg_193" n="e" s="T228">– Mʼečʼtajdaːbɨt. </ts>
            </ts>
            <ts e="T242" id="Seg_194" n="sc" s="T237">
               <ts e="T242" id="Seg_196" n="e" s="T237">– Dalʼše. </ts>
            </ts>
            <ts e="T358" id="Seg_197" n="sc" s="T356">
               <ts e="T358" id="Seg_199" n="e" s="T356">– Fsʼo. </ts>
            </ts>
            <ts e="T372" id="Seg_200" n="sc" s="T370">
               <ts e="T371" id="Seg_202" n="e" s="T370">– Nʼičʼevo, </ts>
               <ts e="T372" id="Seg_204" n="e" s="T371">klassna. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-DCh">
            <ta e="T8" id="Seg_205" s="T1">UoPP_ChGS_20170724_SocCogRetell1_nar.DCh.001 (001.001)</ta>
            <ta e="T17" id="Seg_206" s="T9">UoPP_ChGS_20170724_SocCogRetell1_nar.DCh.002 (001.002)</ta>
            <ta e="T23" id="Seg_207" s="T18">UoPP_ChGS_20170724_SocCogRetell1_nar.DCh.003 (001.003)</ta>
            <ta e="T36" id="Seg_208" s="T27">UoPP_ChGS_20170724_SocCogRetell1_nar.DCh.004 (001.005)</ta>
            <ta e="T230" id="Seg_209" s="T228">UoPP_ChGS_20170724_SocCogRetell1_nar.DCh.005 (001.036)</ta>
            <ta e="T242" id="Seg_210" s="T237">UoPP_ChGS_20170724_SocCogRetell1_nar.DCh.006 (001.039)</ta>
            <ta e="T358" id="Seg_211" s="T356">UoPP_ChGS_20170724_SocCogRetell1_nar.DCh.007 (001.060)</ta>
            <ta e="T372" id="Seg_212" s="T370">UoPP_ChGS_20170724_SocCogRetell1_nar.DCh.008 (001.064)</ta>
         </annotation>
         <annotation name="st" tierref="st-DCh">
            <ta e="T8" id="Seg_213" s="T1">Это была вторая задача, сейчас третья задача.</ta>
            <ta e="T17" id="Seg_214" s="T9">Пожалуйста, одна из Вас, расскажите, что произошло.</ta>
            <ta e="T23" id="Seg_215" s="T18">На долгански, на долганском.</ta>
            <ta e="T36" id="Seg_216" s="T27">Вы или Вы, не важно.</ta>
            <ta e="T230" id="Seg_217" s="T228">Мечтайдаабыт.</ta>
            <ta e="T242" id="Seg_218" s="T237">Дальше.</ta>
            <ta e="T358" id="Seg_219" s="T356">Все.</ta>
            <ta e="T372" id="Seg_220" s="T370">Ничего, классно.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-DCh">
            <ta e="T8" id="Seg_221" s="T1">– Eta bɨla ftaraja zadačʼa, sejčʼas trʼetʼja zadačʼa. </ta>
            <ta e="T17" id="Seg_222" s="T9">Požalujsta, odna iz Vas, eː, rasskažite, što praizašlo. </ta>
            <ta e="T23" id="Seg_223" s="T18">Na dalganskʼi, na dalganskom. </ta>
            <ta e="T36" id="Seg_224" s="T27">– Vɨ ili Vɨ, nʼe važna. </ta>
            <ta e="T230" id="Seg_225" s="T228">– Mʼečʼtajdaːbɨt. </ta>
            <ta e="T242" id="Seg_226" s="T237">– Dalʼše. </ta>
            <ta e="T358" id="Seg_227" s="T356">– Fsʼo. </ta>
            <ta e="T372" id="Seg_228" s="T370">– Nʼičʼevo, klassna. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-DCh">
            <ta e="T230" id="Seg_229" s="T228">mʼečʼtaj-daː-bɨt</ta>
         </annotation>
         <annotation name="mp" tierref="mp-DCh">
            <ta e="T230" id="Seg_230" s="T228">mʼečʼtaj-LAː-BIT</ta>
         </annotation>
         <annotation name="ge" tierref="ge-DCh">
            <ta e="T230" id="Seg_231" s="T228">dream-VBZ-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-DCh">
            <ta e="T230" id="Seg_232" s="T228">träumen-VBZ-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-DCh">
            <ta e="T230" id="Seg_233" s="T228">мечтать-VBZ-PST2.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-DCh">
            <ta e="T230" id="Seg_234" s="T228">v-v&gt;v-v:tense.[v:pred.pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-DCh">
            <ta e="T230" id="Seg_235" s="T228">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-DCh" />
         <annotation name="SyF" tierref="SyF-DCh" />
         <annotation name="IST" tierref="IST-DCh" />
         <annotation name="Top" tierref="Top-DCh" />
         <annotation name="Foc" tierref="Foc-DCh" />
         <annotation name="BOR" tierref="BOR-DCh">
            <ta e="T230" id="Seg_236" s="T228">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-DCh" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-DCh">
            <ta e="T230" id="Seg_237" s="T228">indir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS-DCh">
            <ta e="T8" id="Seg_238" s="T1">RUS:ext</ta>
            <ta e="T17" id="Seg_239" s="T9">RUS:ext</ta>
            <ta e="T23" id="Seg_240" s="T18">RUS:ext</ta>
            <ta e="T36" id="Seg_241" s="T27">RUS:ext</ta>
            <ta e="T358" id="Seg_242" s="T356">RUS:ext</ta>
            <ta e="T372" id="Seg_243" s="T370">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe-DCh">
            <ta e="T36" id="Seg_244" s="T27">– You or You, not important.</ta>
            <ta e="T230" id="Seg_245" s="T228">– He was dreaming.</ta>
            <ta e="T242" id="Seg_246" s="T237">– Go on.</ta>
            <ta e="T358" id="Seg_247" s="T356">– That's all.</ta>
            <ta e="T372" id="Seg_248" s="T370">– Everything fine, great.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-DCh">
            <ta e="T8" id="Seg_249" s="T1">– Das war die zweite Aufgabe, jetzt die dritte Aufgabe.</ta>
            <ta e="T17" id="Seg_250" s="T9">Bitte, eine von Ihnen, erzählen Sie, was passiert ist.</ta>
            <ta e="T23" id="Seg_251" s="T18">Auf Dolganisch.</ta>
            <ta e="T36" id="Seg_252" s="T27">– Sie oder Sie, ist unwichtig.</ta>
            <ta e="T230" id="Seg_253" s="T228">– Er träumte.</ta>
            <ta e="T242" id="Seg_254" s="T237">– Weiter.</ta>
            <ta e="T358" id="Seg_255" s="T356">– Alles.</ta>
            <ta e="T372" id="Seg_256" s="T370">– Alles gut, klasse.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-DCh" />
         <annotation name="ltr" tierref="ltr-DCh" />
         <annotation name="nt" tierref="nt-DCh" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-UoPP"
                      id="tx-UoPP"
                      speaker="UoPP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-UoPP">
            <ts e="T26" id="Seg_257" n="sc" s="T22">
               <ts e="T26" id="Seg_259" n="HIAT:u" s="T22">
                  <nts id="Seg_260" n="HIAT:ip">–</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_263" n="HIAT:w" s="T22">Kepseː</ts>
                  <nts id="Seg_264" n="HIAT:ip">,</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_267" n="HIAT:w" s="T24">dogo</ts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_271" n="HIAT:w" s="T25">en</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T50" id="Seg_274" n="sc" s="T32">
               <ts e="T50" id="Seg_276" n="HIAT:u" s="T32">
                  <nts id="Seg_277" n="HIAT:ip">–</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_280" n="HIAT:w" s="T32">Eː</ts>
                  <nts id="Seg_281" n="HIAT:ip">,</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_284" n="HIAT:w" s="T34">vaabšʼe</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_287" n="HIAT:w" s="T37">biːr</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_290" n="HIAT:w" s="T39">biːr</ts>
                  <nts id="Seg_291" n="HIAT:ip">,</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_294" n="HIAT:w" s="T42">eː</ts>
                  <nts id="Seg_295" n="HIAT:ip">,</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_297" n="HIAT:ip">"</nts>
                  <ts e="T46" id="Seg_299" n="HIAT:w" s="T44">sʼemʼja</ts>
                  <nts id="Seg_300" n="HIAT:ip">"</nts>
                  <nts id="Seg_301" n="HIAT:ip">,</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_304" n="HIAT:w" s="T46">kajtak</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_307" n="HIAT:w" s="T48">etej</ts>
                  <nts id="Seg_308" n="HIAT:ip">?</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T72" id="Seg_310" n="sc" s="T53">
               <ts e="T65" id="Seg_312" n="HIAT:u" s="T53">
                  <nts id="Seg_313" n="HIAT:ip">–</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_316" n="HIAT:w" s="T53">Biːr</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_319" n="HIAT:w" s="T54">ɨ͡allar</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_322" n="HIAT:w" s="T55">bu͡ollar</ts>
                  <nts id="Seg_323" n="HIAT:ip">,</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_326" n="HIAT:w" s="T56">eː</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_330" n="HIAT:w" s="T57">üčügej</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_333" n="HIAT:w" s="T58">bagajdɨk</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_336" n="HIAT:w" s="T59">oloror</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_339" n="HIAT:w" s="T60">etiler</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_342" n="HIAT:w" s="T61">ebit</ts>
                  <nts id="Seg_343" n="HIAT:ip">,</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_346" n="HIAT:w" s="T62">üleliːller</ts>
                  <nts id="Seg_347" n="HIAT:ip">,</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_350" n="HIAT:w" s="T63">üleliːr</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_353" n="HIAT:w" s="T64">etiler</ts>
                  <nts id="Seg_354" n="HIAT:ip">.</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_357" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_359" n="HIAT:w" s="T65">Onton</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_362" n="HIAT:w" s="T66">bu͡o</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_365" n="HIAT:w" s="T67">dogottorun</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_368" n="HIAT:w" s="T68">körsön</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_371" n="HIAT:w" s="T69">aragiː</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_374" n="HIAT:w" s="T70">ihe</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_377" n="HIAT:w" s="T71">barbɨt</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T79" id="Seg_380" n="sc" s="T73">
               <ts e="T75" id="Seg_382" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_384" n="HIAT:w" s="T73">Onno</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_387" n="HIAT:w" s="T74">kepsiːller</ts>
                  <nts id="Seg_388" n="HIAT:ip">:</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_391" n="HIAT:u" s="T75">
                  <nts id="Seg_392" n="HIAT:ip">"</nts>
                  <ts e="T76" id="Seg_394" n="HIAT:w" s="T75">En</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_397" n="HIAT:w" s="T76">dʼaktarɨŋ</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_400" n="HIAT:w" s="T77">ol</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_403" n="HIAT:w" s="T78">kördük</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T101" id="Seg_406" n="sc" s="T80">
               <ts e="T85" id="Seg_408" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_410" n="HIAT:w" s="T80">Bu</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_413" n="HIAT:w" s="T81">kördük</ts>
                  <nts id="Seg_414" n="HIAT:ip">,</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_417" n="HIAT:w" s="T82">ol</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_420" n="HIAT:w" s="T83">kihini</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_423" n="HIAT:w" s="T84">gɨtta</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_427" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_429" n="HIAT:w" s="T85">Kepseppite</ts>
                  <nts id="Seg_430" n="HIAT:ip">,</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_433" n="HIAT:w" s="T86">eː</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_437" n="HIAT:w" s="T87">kimniːr</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_440" n="HIAT:w" s="T88">ete</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip">"</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_445" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_447" n="HIAT:w" s="T89">Kajdak</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_450" n="HIAT:w" s="T90">di͡eččiler</ts>
                  <nts id="Seg_451" n="HIAT:ip">,</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_454" n="HIAT:w" s="T92">karoče</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_457" n="HIAT:w" s="T96">zaigrɨvala</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_459" n="HIAT:ip">(</nts>
                  <nts id="Seg_460" n="HIAT:ip">(</nts>
                  <ats e="T101" id="Seg_461" n="HIAT:non-pho" s="T97">…</ats>
                  <nts id="Seg_462" n="HIAT:ip">)</nts>
                  <nts id="Seg_463" n="HIAT:ip">)</nts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T110" id="Seg_466" n="sc" s="T105">
               <ts e="T110" id="Seg_468" n="HIAT:u" s="T105">
                  <nts id="Seg_469" n="HIAT:ip">"</nts>
                  <ts e="T106" id="Seg_471" n="HIAT:w" s="T105">Eː</ts>
                  <nts id="Seg_472" n="HIAT:ip">,</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_475" n="HIAT:w" s="T106">dʼaktarɨŋ</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_478" n="HIAT:w" s="T107">ol</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_481" n="HIAT:w" s="T108">kördük</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_484" n="HIAT:w" s="T109">körsüleher</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip">"</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T114" id="Seg_488" n="sc" s="T111">
               <ts e="T114" id="Seg_490" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_492" n="HIAT:w" s="T111">Bu</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_495" n="HIAT:w" s="T112">kördük</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_498" n="HIAT:w" s="T113">diːller</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T125" id="Seg_501" n="sc" s="T115">
               <ts e="T122" id="Seg_503" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_505" n="HIAT:w" s="T115">Onton</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_508" n="HIAT:w" s="T116">baran</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_511" n="HIAT:w" s="T117">dʼaktarɨttan</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_514" n="HIAT:w" s="T118">ɨjɨtar</ts>
                  <nts id="Seg_515" n="HIAT:ip">,</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_518" n="HIAT:w" s="T119">kimniːr</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_521" n="HIAT:w" s="T120">eː</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_524" n="HIAT:w" s="T121">ü͡ögülüːr</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_528" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_530" n="HIAT:w" s="T122">Onton</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_533" n="HIAT:w" s="T123">oksubut</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_536" n="HIAT:w" s="T124">dʼaktarɨn</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T135" id="Seg_539" n="sc" s="T126">
               <ts e="T135" id="Seg_541" n="HIAT:u" s="T126">
                  <nts id="Seg_542" n="HIAT:ip">(</nts>
                  <ts e="T127" id="Seg_544" n="HIAT:w" s="T126">Dʼak-</ts>
                  <nts id="Seg_545" n="HIAT:ip">)</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_548" n="HIAT:w" s="T127">dʼaktar</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_551" n="HIAT:w" s="T128">ogoloːk</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_554" n="HIAT:w" s="T129">ogoloːk</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_557" n="HIAT:w" s="T130">iliːtiger</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_560" n="HIAT:w" s="T131">ogoloːk</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_563" n="HIAT:w" s="T132">hɨldʼar</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_566" n="HIAT:w" s="T133">dʼaktarɨ</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_569" n="HIAT:w" s="T134">kɨrbɨːr</ts>
                  <nts id="Seg_570" n="HIAT:ip">.</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T145" id="Seg_572" n="sc" s="T136">
               <ts e="T141" id="Seg_574" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_576" n="HIAT:w" s="T136">Mʼilʼisʼijeler</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_579" n="HIAT:w" s="T137">kelbitter</ts>
                  <nts id="Seg_580" n="HIAT:ip">,</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_583" n="HIAT:w" s="T138">kim</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_586" n="HIAT:w" s="T139">ere</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_589" n="HIAT:w" s="T140">ɨgɨrbɨt</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_593" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_595" n="HIAT:w" s="T141">Onton</ts>
                  <nts id="Seg_596" n="HIAT:ip">,</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_599" n="HIAT:w" s="T142">mm</ts>
                  <nts id="Seg_600" n="HIAT:ip">,</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_603" n="HIAT:w" s="T143">mʼilʼisʼijeler</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_606" n="HIAT:w" s="T144">ilpitter</ts>
                  <nts id="Seg_607" n="HIAT:ip">.</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T149" id="Seg_609" n="sc" s="T146">
               <ts e="T149" id="Seg_611" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_613" n="HIAT:w" s="T146">Barɨlarɨn</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_616" n="HIAT:w" s="T147">ilpitter</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_619" n="HIAT:w" s="T148">onno</ts>
                  <nts id="Seg_620" n="HIAT:ip">.</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_622" n="sc" s="T150">
               <ts e="T172" id="Seg_624" n="HIAT:u" s="T150">
                  <ts e="T152" id="Seg_626" n="HIAT:w" s="T150">Daprosdaːbɨttar</ts>
                  <nts id="Seg_627" n="HIAT:ip">,</nts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_630" n="HIAT:w" s="T152">daprosdaːbɨttar</ts>
                  <nts id="Seg_631" n="HIAT:ip">,</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_634" n="HIAT:w" s="T154">ɨjɨtalaːbɨttar</ts>
                  <nts id="Seg_635" n="HIAT:ip">,</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_637" n="HIAT:ip">"</nts>
                  <ts e="T158" id="Seg_639" n="HIAT:w" s="T156">kajtak</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_642" n="HIAT:w" s="T158">tu͡ok</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_645" n="HIAT:w" s="T160">bu͡olbukkutuj</ts>
                  <nts id="Seg_646" n="HIAT:ip">,</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_649" n="HIAT:w" s="T162">kajtak</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_652" n="HIAT:w" s="T164">tu͡ok</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_655" n="HIAT:w" s="T166">bu͡olu͡on</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_658" n="HIAT:w" s="T167">ölörsübükkütüj</ts>
                  <nts id="Seg_659" n="HIAT:ip">"</nts>
                  <nts id="Seg_660" n="HIAT:ip">,</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_663" n="HIAT:w" s="T168">diː</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_666" n="HIAT:w" s="T169">kepsiːr</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_669" n="HIAT:w" s="T170">onno</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_672" n="HIAT:w" s="T171">dʼaktara</ts>
                  <nts id="Seg_673" n="HIAT:ip">.</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T178" id="Seg_675" n="sc" s="T173">
               <ts e="T178" id="Seg_677" n="HIAT:u" s="T173">
                  <nts id="Seg_678" n="HIAT:ip">"</nts>
                  <ts e="T174" id="Seg_680" n="HIAT:w" s="T173">Ol</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_683" n="HIAT:w" s="T174">kördük</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_686" n="HIAT:w" s="T175">kɨrbaːta</ts>
                  <nts id="Seg_687" n="HIAT:ip">,</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_690" n="HIAT:w" s="T176">bu</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_693" n="HIAT:w" s="T177">kördük</ts>
                  <nts id="Seg_694" n="HIAT:ip">.</nts>
                  <nts id="Seg_695" n="HIAT:ip">"</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T189" id="Seg_697" n="sc" s="T179">
               <ts e="T189" id="Seg_699" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_701" n="HIAT:w" s="T179">Onton</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_704" n="HIAT:w" s="T180">türmege</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_707" n="HIAT:w" s="T181">olorputtar</ts>
                  <nts id="Seg_708" n="HIAT:ip">,</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_711" n="HIAT:w" s="T182">eː</ts>
                  <nts id="Seg_712" n="HIAT:ip">,</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_715" n="HIAT:w" s="T183">türme</ts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_719" n="HIAT:w" s="T185">kaːjɨːga</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_722" n="HIAT:w" s="T186">olorputtar</ts>
                  <nts id="Seg_723" n="HIAT:ip">,</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_726" n="HIAT:w" s="T188">aha</ts>
                  <nts id="Seg_727" n="HIAT:ip">.</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T192" id="Seg_729" n="sc" s="T190">
               <ts e="T192" id="Seg_731" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_733" n="HIAT:w" s="T190">Kaːjɨːga</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_736" n="HIAT:w" s="T191">olorputtar</ts>
                  <nts id="Seg_737" n="HIAT:ip">.</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T204" id="Seg_739" n="sc" s="T193">
               <ts e="T204" id="Seg_741" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_743" n="HIAT:w" s="T193">Onton</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_746" n="HIAT:w" s="T194">bu</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_748" n="HIAT:ip">(</nts>
                  <ts e="T196" id="Seg_750" n="HIAT:w" s="T195">on-</ts>
                  <nts id="Seg_751" n="HIAT:ip">)</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_754" n="HIAT:w" s="T196">öjdüː</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_757" n="HIAT:w" s="T197">oloror</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_761" n="HIAT:w" s="T198">ogotun</ts>
                  <nts id="Seg_762" n="HIAT:ip">,</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_765" n="HIAT:w" s="T199">dʼaktarɨn</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_768" n="HIAT:w" s="T200">öjdüː</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_771" n="HIAT:w" s="T201">oloror</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_774" n="HIAT:w" s="T202">ol</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_777" n="HIAT:w" s="T203">kördük</ts>
                  <nts id="Seg_778" n="HIAT:ip">.</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T214" id="Seg_780" n="sc" s="T205">
               <ts e="T214" id="Seg_782" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_784" n="HIAT:w" s="T205">Mm</ts>
                  <nts id="Seg_785" n="HIAT:ip">,</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_788" n="HIAT:w" s="T206">taksarɨn</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_791" n="HIAT:w" s="T207">türmeːtten</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_794" n="HIAT:w" s="T208">taksarɨn</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_796" n="HIAT:ip">(</nts>
                  <ts e="T210" id="Seg_798" n="HIAT:w" s="T209">köhüt-</ts>
                  <nts id="Seg_799" n="HIAT:ip">)</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_802" n="HIAT:w" s="T210">köhüten</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_804" n="HIAT:ip">(</nts>
                  <ts e="T212" id="Seg_806" n="HIAT:w" s="T211">mʼečʼt-</ts>
                  <nts id="Seg_807" n="HIAT:ip">)</nts>
                  <nts id="Seg_808" n="HIAT:ip">,</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_811" n="HIAT:w" s="T212">eː</ts>
                  <nts id="Seg_812" n="HIAT:ip">,</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_815" n="HIAT:w" s="T213">mʼečʼtajet</ts>
                  <nts id="Seg_816" n="HIAT:ip">.</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_818" n="sc" s="T216">
               <ts e="T227" id="Seg_820" n="HIAT:u" s="T216">
                  <nts id="Seg_821" n="HIAT:ip">(</nts>
                  <nts id="Seg_822" n="HIAT:ip">(</nts>
                  <ats e="T220" id="Seg_823" n="HIAT:non-pho" s="T216">LAUGH</ats>
                  <nts id="Seg_824" n="HIAT:ip">)</nts>
                  <nts id="Seg_825" n="HIAT:ip">)</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_827" n="HIAT:ip">"</nts>
                  <ts e="T222" id="Seg_829" n="HIAT:w" s="T220">Kimniːbin</ts>
                  <nts id="Seg_830" n="HIAT:ip">"</nts>
                  <nts id="Seg_831" n="HIAT:ip">,</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_834" n="HIAT:w" s="T222">diːr</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_837" n="HIAT:w" s="T224">eː</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_840" n="HIAT:w" s="T226">mm</ts>
                  <nts id="Seg_841" n="HIAT:ip">.</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T239" id="Seg_843" n="sc" s="T233">
               <ts e="T239" id="Seg_845" n="HIAT:u" s="T233">
                  <nts id="Seg_846" n="HIAT:ip">–</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_849" n="HIAT:w" s="T233">Nʼet</ts>
                  <nts id="Seg_850" n="HIAT:ip">,</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_853" n="HIAT:w" s="T236">hu͡ok</ts>
                  <nts id="Seg_854" n="HIAT:ip">.</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T254" id="Seg_856" n="sc" s="T240">
               <ts e="T254" id="Seg_858" n="HIAT:u" s="T240">
                  <ts e="T243" id="Seg_860" n="HIAT:w" s="T240">Kimniː</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_863" n="HIAT:w" s="T243">oloror</ts>
                  <nts id="Seg_864" n="HIAT:ip">,</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_867" n="HIAT:w" s="T244">eː</ts>
                  <nts id="Seg_868" n="HIAT:ip">,</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_870" n="HIAT:ip">(</nts>
                  <ts e="T246" id="Seg_872" n="HIAT:w" s="T245">kepsi-</ts>
                  <nts id="Seg_873" n="HIAT:ip">)</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_876" n="HIAT:w" s="T246">kimniː</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_879" n="HIAT:w" s="T247">oloror</ts>
                  <nts id="Seg_880" n="HIAT:ip">,</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_883" n="HIAT:w" s="T248">öjdüː</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_886" n="HIAT:w" s="T249">oloror</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_889" n="HIAT:w" s="T250">dʼi͡eleːkterin</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_891" n="HIAT:ip">"</nts>
                  <ts e="T252" id="Seg_893" n="HIAT:w" s="T251">ol</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_896" n="HIAT:w" s="T252">kördük</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_899" n="HIAT:w" s="T253">tagɨstappɨna</ts>
                  <nts id="Seg_900" n="HIAT:ip">"</nts>
                  <nts id="Seg_901" n="HIAT:ip">.</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T267" id="Seg_903" n="sc" s="T256">
               <ts e="T267" id="Seg_905" n="HIAT:u" s="T256">
                  <nts id="Seg_906" n="HIAT:ip">–</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_909" n="HIAT:w" s="T256">Eː</ts>
                  <nts id="Seg_910" n="HIAT:ip">,</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_913" n="HIAT:w" s="T258">hanɨː</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_916" n="HIAT:w" s="T260">oloror</ts>
                  <nts id="Seg_917" n="HIAT:ip">,</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_920" n="HIAT:w" s="T261">öjdüː</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_923" n="HIAT:w" s="T263">oloror</ts>
                  <nts id="Seg_924" n="HIAT:ip">,</nts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_927" n="HIAT:w" s="T264">hanɨːr</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_930" n="HIAT:w" s="T265">onton</ts>
                  <nts id="Seg_931" n="HIAT:ip">.</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T272" id="Seg_933" n="sc" s="T269">
               <ts e="T272" id="Seg_935" n="HIAT:u" s="T269">
                  <nts id="Seg_936" n="HIAT:ip">–</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_939" n="HIAT:w" s="T269">Aː</ts>
                  <nts id="Seg_940" n="HIAT:ip">.</nts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T285" id="Seg_942" n="sc" s="T276">
               <ts e="T285" id="Seg_944" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_946" n="HIAT:w" s="T276">Eː</ts>
                  <nts id="Seg_947" n="HIAT:ip">,</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_949" n="HIAT:ip">"</nts>
                  <ts e="T278" id="Seg_951" n="HIAT:w" s="T277">ü͡örü͡öktere</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_954" n="HIAT:w" s="T278">min</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_957" n="HIAT:w" s="T279">tagɨstappɨna</ts>
                  <nts id="Seg_958" n="HIAT:ip">,</nts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_961" n="HIAT:w" s="T280">aragiː</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_964" n="HIAT:w" s="T281">ihi͡em</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_967" n="HIAT:w" s="T282">hu͡oga</ts>
                  <nts id="Seg_968" n="HIAT:ip">"</nts>
                  <nts id="Seg_969" n="HIAT:ip">,</nts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_972" n="HIAT:w" s="T283">diːr</ts>
                  <nts id="Seg_973" n="HIAT:ip">,</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_975" n="HIAT:ip">"</nts>
                  <ts e="T285" id="Seg_977" n="HIAT:w" s="T284">bolʼše</ts>
                  <nts id="Seg_978" n="HIAT:ip">.</nts>
                  <nts id="Seg_979" n="HIAT:ip">"</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T289" id="Seg_981" n="sc" s="T286">
               <ts e="T289" id="Seg_983" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_985" n="HIAT:w" s="T286">Taksɨbɨt</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_988" n="HIAT:w" s="T287">onton</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_991" n="HIAT:w" s="T288">türmeːtten</ts>
                  <nts id="Seg_992" n="HIAT:ip">.</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T295" id="Seg_994" n="sc" s="T290">
               <ts e="T295" id="Seg_996" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_998" n="HIAT:w" s="T290">Ü͡örer</ts>
                  <nts id="Seg_999" n="HIAT:ip">,</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1002" n="HIAT:w" s="T291">ü͡örer</ts>
                  <nts id="Seg_1003" n="HIAT:ip">,</nts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1006" n="HIAT:w" s="T292">kün</ts>
                  <nts id="Seg_1007" n="HIAT:ip">,</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1010" n="HIAT:w" s="T293">kün</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1013" n="HIAT:w" s="T294">tɨgar</ts>
                  <nts id="Seg_1014" n="HIAT:ip">.</nts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T308" id="Seg_1016" n="sc" s="T297">
               <ts e="T308" id="Seg_1018" n="HIAT:u" s="T297">
                  <nts id="Seg_1019" n="HIAT:ip">–</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1022" n="HIAT:w" s="T297">Eː</ts>
                  <nts id="Seg_1023" n="HIAT:ip">,</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1026" n="HIAT:w" s="T298">hürdeːk</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1029" n="HIAT:w" s="T301">üčügej</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1032" n="HIAT:w" s="T302">bagajɨ</ts>
                  <nts id="Seg_1033" n="HIAT:ip">,</nts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1036" n="HIAT:w" s="T304">onton</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1039" n="HIAT:w" s="T305">ü͡öre-ü͡öre</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1042" n="HIAT:w" s="T306">dʼi͡etiger</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1045" n="HIAT:w" s="T307">kelbit</ts>
                  <nts id="Seg_1046" n="HIAT:ip">.</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T310" id="Seg_1048" n="sc" s="T309">
               <ts e="T310" id="Seg_1050" n="HIAT:u" s="T309">
                  <nts id="Seg_1051" n="HIAT:ip">–</nts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1054" n="HIAT:w" s="T309">Ontulara</ts>
                  <nts id="Seg_1055" n="HIAT:ip">.</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T321" id="Seg_1057" n="sc" s="T313">
               <ts e="T321" id="Seg_1059" n="HIAT:u" s="T313">
                  <ts e="T315" id="Seg_1061" n="HIAT:w" s="T313">Eː</ts>
                  <nts id="Seg_1062" n="HIAT:ip">,</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1065" n="HIAT:w" s="T315">ontulara</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1068" n="HIAT:w" s="T316">bu͡olla</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1071" n="HIAT:w" s="T317">du͡o</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1074" n="HIAT:w" s="T318">köröllör</ts>
                  <nts id="Seg_1075" n="HIAT:ip">,</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1078" n="HIAT:w" s="T319">kuttanallar</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1081" n="HIAT:w" s="T320">bɨhɨlaːk</ts>
                  <nts id="Seg_1082" n="HIAT:ip">.</nts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T340" id="Seg_1084" n="sc" s="T322">
               <ts e="T327" id="Seg_1086" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1088" n="HIAT:w" s="T322">Onton</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1091" n="HIAT:w" s="T323">ogotun</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1094" n="HIAT:w" s="T324">kɨtta</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1097" n="HIAT:w" s="T325">hɨldʼa</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1100" n="HIAT:w" s="T326">barbɨt</ts>
                  <nts id="Seg_1101" n="HIAT:ip">.</nts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1104" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1106" n="HIAT:w" s="T327">Dogottorun</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1109" n="HIAT:w" s="T328">körsübüt</ts>
                  <nts id="Seg_1110" n="HIAT:ip">,</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1112" n="HIAT:ip">"</nts>
                  <ts e="T330" id="Seg_1114" n="HIAT:w" s="T329">kel</ts>
                  <nts id="Seg_1115" n="HIAT:ip">,</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1118" n="HIAT:w" s="T330">aragiːta</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1121" n="HIAT:w" s="T331">is</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1124" n="HIAT:w" s="T332">bihigini</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1127" n="HIAT:w" s="T333">gɨtta</ts>
                  <nts id="Seg_1128" n="HIAT:ip">"</nts>
                  <nts id="Seg_1129" n="HIAT:ip">,</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1131" n="HIAT:ip">"</nts>
                  <ts e="T335" id="Seg_1133" n="HIAT:w" s="T334">hu͡ok</ts>
                  <nts id="Seg_1134" n="HIAT:ip">,</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1137" n="HIAT:w" s="T335">hu͡ok</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1140" n="HIAT:w" s="T336">ispeppin</ts>
                  <nts id="Seg_1141" n="HIAT:ip">"</nts>
                  <nts id="Seg_1142" n="HIAT:ip">.</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1145" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1147" n="HIAT:w" s="T337">Kak</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1150" n="HIAT:w" s="T338">skazka</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1153" n="HIAT:w" s="T339">že</ts>
                  <nts id="Seg_1154" n="HIAT:ip">.</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T347" id="Seg_1156" n="sc" s="T341">
               <ts e="T347" id="Seg_1158" n="HIAT:u" s="T341">
                  <nts id="Seg_1159" n="HIAT:ip">"</nts>
                  <ts e="T342" id="Seg_1161" n="HIAT:w" s="T341">Ispeppin</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1164" n="HIAT:w" s="T342">bolʼše</ts>
                  <nts id="Seg_1165" n="HIAT:ip">,</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1168" n="HIAT:w" s="T343">eː</ts>
                  <nts id="Seg_1169" n="HIAT:ip">,</nts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1172" n="HIAT:w" s="T344">bolʼše</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1175" n="HIAT:w" s="T345">bu͡o</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1178" n="HIAT:w" s="T346">ispeppin</ts>
                  <nts id="Seg_1179" n="HIAT:ip">.</nts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T357" id="Seg_1181" n="sc" s="T348">
               <ts e="T354" id="Seg_1183" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1185" n="HIAT:w" s="T348">Ihi͡em</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1188" n="HIAT:w" s="T349">hu͡oga</ts>
                  <nts id="Seg_1189" n="HIAT:ip">,</nts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1192" n="HIAT:w" s="T350">aragiːnɨ</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1195" n="HIAT:w" s="T351">bɨrakpɨtɨm</ts>
                  <nts id="Seg_1196" n="HIAT:ip">"</nts>
                  <nts id="Seg_1197" n="HIAT:ip">,</nts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1200" n="HIAT:w" s="T352">di͡ebit</ts>
                  <nts id="Seg_1201" n="HIAT:ip">,</nts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1204" n="HIAT:w" s="T353">karočʼe</ts>
                  <nts id="Seg_1205" n="HIAT:ip">.</nts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1208" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1210" n="HIAT:w" s="T354">He</ts>
                  <nts id="Seg_1211" n="HIAT:ip">,</nts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1214" n="HIAT:w" s="T355">fsʼo</ts>
                  <nts id="Seg_1215" n="HIAT:ip">.</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T362" id="Seg_1217" n="sc" s="T359">
               <ts e="T362" id="Seg_1219" n="HIAT:u" s="T359">
                  <nts id="Seg_1220" n="HIAT:ip">–</nts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1223" n="HIAT:w" s="T359">Bajan-toton</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1226" n="HIAT:w" s="T360">olorbuttar</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1229" n="HIAT:w" s="T361">onton</ts>
                  <nts id="Seg_1230" n="HIAT:ip">.</nts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T365" id="Seg_1232" n="sc" s="T363">
               <ts e="T365" id="Seg_1234" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1236" n="HIAT:w" s="T363">Üčügej</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1239" n="HIAT:w" s="T364">bagajdɨk</ts>
                  <nts id="Seg_1240" n="HIAT:ip">.</nts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T370" id="Seg_1242" n="sc" s="T366">
               <ts e="T370" id="Seg_1244" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1246" n="HIAT:w" s="T366">Fsʼo</ts>
                  <nts id="Seg_1247" n="HIAT:ip">,</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1250" n="HIAT:w" s="T367">navʼerna</ts>
                  <nts id="Seg_1251" n="HIAT:ip">,</nts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1254" n="HIAT:w" s="T368">mala</ts>
                  <nts id="Seg_1255" n="HIAT:ip">,</nts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1258" n="HIAT:w" s="T369">da</ts>
                  <nts id="Seg_1259" n="HIAT:ip">?</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-UoPP">
            <ts e="T26" id="Seg_1261" n="sc" s="T22">
               <ts e="T24" id="Seg_1263" n="e" s="T22">– Kepseː, </ts>
               <ts e="T25" id="Seg_1265" n="e" s="T24">dogo, </ts>
               <ts e="T26" id="Seg_1267" n="e" s="T25">en. </ts>
            </ts>
            <ts e="T50" id="Seg_1268" n="sc" s="T32">
               <ts e="T34" id="Seg_1270" n="e" s="T32">– Eː, </ts>
               <ts e="T37" id="Seg_1272" n="e" s="T34">vaabšʼe </ts>
               <ts e="T39" id="Seg_1274" n="e" s="T37">biːr </ts>
               <ts e="T42" id="Seg_1276" n="e" s="T39">biːr, </ts>
               <ts e="T44" id="Seg_1278" n="e" s="T42">eː, </ts>
               <ts e="T46" id="Seg_1280" n="e" s="T44">"sʼemʼja", </ts>
               <ts e="T48" id="Seg_1282" n="e" s="T46">kajtak </ts>
               <ts e="T50" id="Seg_1284" n="e" s="T48">etej? </ts>
            </ts>
            <ts e="T72" id="Seg_1285" n="sc" s="T53">
               <ts e="T54" id="Seg_1287" n="e" s="T53">– Biːr </ts>
               <ts e="T55" id="Seg_1289" n="e" s="T54">ɨ͡allar </ts>
               <ts e="T56" id="Seg_1291" n="e" s="T55">bu͡ollar, </ts>
               <ts e="T57" id="Seg_1293" n="e" s="T56">eː, </ts>
               <ts e="T58" id="Seg_1295" n="e" s="T57">üčügej </ts>
               <ts e="T59" id="Seg_1297" n="e" s="T58">bagajdɨk </ts>
               <ts e="T60" id="Seg_1299" n="e" s="T59">oloror </ts>
               <ts e="T61" id="Seg_1301" n="e" s="T60">etiler </ts>
               <ts e="T62" id="Seg_1303" n="e" s="T61">ebit, </ts>
               <ts e="T63" id="Seg_1305" n="e" s="T62">üleliːller, </ts>
               <ts e="T64" id="Seg_1307" n="e" s="T63">üleliːr </ts>
               <ts e="T65" id="Seg_1309" n="e" s="T64">etiler. </ts>
               <ts e="T66" id="Seg_1311" n="e" s="T65">Onton </ts>
               <ts e="T67" id="Seg_1313" n="e" s="T66">bu͡o </ts>
               <ts e="T68" id="Seg_1315" n="e" s="T67">dogottorun </ts>
               <ts e="T69" id="Seg_1317" n="e" s="T68">körsön </ts>
               <ts e="T70" id="Seg_1319" n="e" s="T69">aragiː </ts>
               <ts e="T71" id="Seg_1321" n="e" s="T70">ihe </ts>
               <ts e="T72" id="Seg_1323" n="e" s="T71">barbɨt. </ts>
            </ts>
            <ts e="T79" id="Seg_1324" n="sc" s="T73">
               <ts e="T74" id="Seg_1326" n="e" s="T73">Onno </ts>
               <ts e="T75" id="Seg_1328" n="e" s="T74">kepsiːller: </ts>
               <ts e="T76" id="Seg_1330" n="e" s="T75">"En </ts>
               <ts e="T77" id="Seg_1332" n="e" s="T76">dʼaktarɨŋ </ts>
               <ts e="T78" id="Seg_1334" n="e" s="T77">ol </ts>
               <ts e="T79" id="Seg_1336" n="e" s="T78">kördük. </ts>
            </ts>
            <ts e="T101" id="Seg_1337" n="sc" s="T80">
               <ts e="T81" id="Seg_1339" n="e" s="T80">Bu </ts>
               <ts e="T82" id="Seg_1341" n="e" s="T81">kördük, </ts>
               <ts e="T83" id="Seg_1343" n="e" s="T82">ol </ts>
               <ts e="T84" id="Seg_1345" n="e" s="T83">kihini </ts>
               <ts e="T85" id="Seg_1347" n="e" s="T84">gɨtta. </ts>
               <ts e="T86" id="Seg_1349" n="e" s="T85">Kepseppite, </ts>
               <ts e="T87" id="Seg_1351" n="e" s="T86">eː, </ts>
               <ts e="T88" id="Seg_1353" n="e" s="T87">kimniːr </ts>
               <ts e="T89" id="Seg_1355" n="e" s="T88">ete." </ts>
               <ts e="T90" id="Seg_1357" n="e" s="T89">Kajdak </ts>
               <ts e="T92" id="Seg_1359" n="e" s="T90">di͡eččiler, </ts>
               <ts e="T96" id="Seg_1361" n="e" s="T92">karoče </ts>
               <ts e="T97" id="Seg_1363" n="e" s="T96">zaigrɨvala </ts>
               <ts e="T101" id="Seg_1365" n="e" s="T97">((…)). </ts>
            </ts>
            <ts e="T110" id="Seg_1366" n="sc" s="T105">
               <ts e="T106" id="Seg_1368" n="e" s="T105">"Eː, </ts>
               <ts e="T107" id="Seg_1370" n="e" s="T106">dʼaktarɨŋ </ts>
               <ts e="T108" id="Seg_1372" n="e" s="T107">ol </ts>
               <ts e="T109" id="Seg_1374" n="e" s="T108">kördük </ts>
               <ts e="T110" id="Seg_1376" n="e" s="T109">körsüleher." </ts>
            </ts>
            <ts e="T114" id="Seg_1377" n="sc" s="T111">
               <ts e="T112" id="Seg_1379" n="e" s="T111">Bu </ts>
               <ts e="T113" id="Seg_1381" n="e" s="T112">kördük </ts>
               <ts e="T114" id="Seg_1383" n="e" s="T113">diːller. </ts>
            </ts>
            <ts e="T125" id="Seg_1384" n="sc" s="T115">
               <ts e="T116" id="Seg_1386" n="e" s="T115">Onton </ts>
               <ts e="T117" id="Seg_1388" n="e" s="T116">baran </ts>
               <ts e="T118" id="Seg_1390" n="e" s="T117">dʼaktarɨttan </ts>
               <ts e="T119" id="Seg_1392" n="e" s="T118">ɨjɨtar, </ts>
               <ts e="T120" id="Seg_1394" n="e" s="T119">kimniːr </ts>
               <ts e="T121" id="Seg_1396" n="e" s="T120">eː </ts>
               <ts e="T122" id="Seg_1398" n="e" s="T121">ü͡ögülüːr. </ts>
               <ts e="T123" id="Seg_1400" n="e" s="T122">Onton </ts>
               <ts e="T124" id="Seg_1402" n="e" s="T123">oksubut </ts>
               <ts e="T125" id="Seg_1404" n="e" s="T124">dʼaktarɨn. </ts>
            </ts>
            <ts e="T135" id="Seg_1405" n="sc" s="T126">
               <ts e="T127" id="Seg_1407" n="e" s="T126">(Dʼak-) </ts>
               <ts e="T128" id="Seg_1409" n="e" s="T127">dʼaktar </ts>
               <ts e="T129" id="Seg_1411" n="e" s="T128">ogoloːk </ts>
               <ts e="T130" id="Seg_1413" n="e" s="T129">ogoloːk </ts>
               <ts e="T131" id="Seg_1415" n="e" s="T130">iliːtiger </ts>
               <ts e="T132" id="Seg_1417" n="e" s="T131">ogoloːk </ts>
               <ts e="T133" id="Seg_1419" n="e" s="T132">hɨldʼar </ts>
               <ts e="T134" id="Seg_1421" n="e" s="T133">dʼaktarɨ </ts>
               <ts e="T135" id="Seg_1423" n="e" s="T134">kɨrbɨːr. </ts>
            </ts>
            <ts e="T145" id="Seg_1424" n="sc" s="T136">
               <ts e="T137" id="Seg_1426" n="e" s="T136">Mʼilʼisʼijeler </ts>
               <ts e="T138" id="Seg_1428" n="e" s="T137">kelbitter, </ts>
               <ts e="T139" id="Seg_1430" n="e" s="T138">kim </ts>
               <ts e="T140" id="Seg_1432" n="e" s="T139">ere </ts>
               <ts e="T141" id="Seg_1434" n="e" s="T140">ɨgɨrbɨt. </ts>
               <ts e="T142" id="Seg_1436" n="e" s="T141">Onton, </ts>
               <ts e="T143" id="Seg_1438" n="e" s="T142">mm, </ts>
               <ts e="T144" id="Seg_1440" n="e" s="T143">mʼilʼisʼijeler </ts>
               <ts e="T145" id="Seg_1442" n="e" s="T144">ilpitter. </ts>
            </ts>
            <ts e="T149" id="Seg_1443" n="sc" s="T146">
               <ts e="T147" id="Seg_1445" n="e" s="T146">Barɨlarɨn </ts>
               <ts e="T148" id="Seg_1447" n="e" s="T147">ilpitter </ts>
               <ts e="T149" id="Seg_1449" n="e" s="T148">onno. </ts>
            </ts>
            <ts e="T172" id="Seg_1450" n="sc" s="T150">
               <ts e="T152" id="Seg_1452" n="e" s="T150">Daprosdaːbɨttar, </ts>
               <ts e="T154" id="Seg_1454" n="e" s="T152">daprosdaːbɨttar, </ts>
               <ts e="T156" id="Seg_1456" n="e" s="T154">ɨjɨtalaːbɨttar, </ts>
               <ts e="T158" id="Seg_1458" n="e" s="T156">"kajtak </ts>
               <ts e="T160" id="Seg_1460" n="e" s="T158">tu͡ok </ts>
               <ts e="T162" id="Seg_1462" n="e" s="T160">bu͡olbukkutuj, </ts>
               <ts e="T164" id="Seg_1464" n="e" s="T162">kajtak </ts>
               <ts e="T166" id="Seg_1466" n="e" s="T164">tu͡ok </ts>
               <ts e="T167" id="Seg_1468" n="e" s="T166">bu͡olu͡on </ts>
               <ts e="T168" id="Seg_1470" n="e" s="T167">ölörsübükkütüj", </ts>
               <ts e="T169" id="Seg_1472" n="e" s="T168">diː </ts>
               <ts e="T170" id="Seg_1474" n="e" s="T169">kepsiːr </ts>
               <ts e="T171" id="Seg_1476" n="e" s="T170">onno </ts>
               <ts e="T172" id="Seg_1478" n="e" s="T171">dʼaktara. </ts>
            </ts>
            <ts e="T178" id="Seg_1479" n="sc" s="T173">
               <ts e="T174" id="Seg_1481" n="e" s="T173">"Ol </ts>
               <ts e="T175" id="Seg_1483" n="e" s="T174">kördük </ts>
               <ts e="T176" id="Seg_1485" n="e" s="T175">kɨrbaːta, </ts>
               <ts e="T177" id="Seg_1487" n="e" s="T176">bu </ts>
               <ts e="T178" id="Seg_1489" n="e" s="T177">kördük." </ts>
            </ts>
            <ts e="T189" id="Seg_1490" n="sc" s="T179">
               <ts e="T180" id="Seg_1492" n="e" s="T179">Onton </ts>
               <ts e="T181" id="Seg_1494" n="e" s="T180">türmege </ts>
               <ts e="T182" id="Seg_1496" n="e" s="T181">olorputtar, </ts>
               <ts e="T183" id="Seg_1498" n="e" s="T182">eː, </ts>
               <ts e="T185" id="Seg_1500" n="e" s="T183">türme, </ts>
               <ts e="T186" id="Seg_1502" n="e" s="T185">kaːjɨːga </ts>
               <ts e="T188" id="Seg_1504" n="e" s="T186">olorputtar, </ts>
               <ts e="T189" id="Seg_1506" n="e" s="T188">aha. </ts>
            </ts>
            <ts e="T192" id="Seg_1507" n="sc" s="T190">
               <ts e="T191" id="Seg_1509" n="e" s="T190">Kaːjɨːga </ts>
               <ts e="T192" id="Seg_1511" n="e" s="T191">olorputtar. </ts>
            </ts>
            <ts e="T204" id="Seg_1512" n="sc" s="T193">
               <ts e="T194" id="Seg_1514" n="e" s="T193">Onton </ts>
               <ts e="T195" id="Seg_1516" n="e" s="T194">bu </ts>
               <ts e="T196" id="Seg_1518" n="e" s="T195">(on-) </ts>
               <ts e="T197" id="Seg_1520" n="e" s="T196">öjdüː </ts>
               <ts e="T198" id="Seg_1522" n="e" s="T197">oloror, </ts>
               <ts e="T199" id="Seg_1524" n="e" s="T198">ogotun, </ts>
               <ts e="T200" id="Seg_1526" n="e" s="T199">dʼaktarɨn </ts>
               <ts e="T201" id="Seg_1528" n="e" s="T200">öjdüː </ts>
               <ts e="T202" id="Seg_1530" n="e" s="T201">oloror </ts>
               <ts e="T203" id="Seg_1532" n="e" s="T202">ol </ts>
               <ts e="T204" id="Seg_1534" n="e" s="T203">kördük. </ts>
            </ts>
            <ts e="T214" id="Seg_1535" n="sc" s="T205">
               <ts e="T206" id="Seg_1537" n="e" s="T205">Mm, </ts>
               <ts e="T207" id="Seg_1539" n="e" s="T206">taksarɨn </ts>
               <ts e="T208" id="Seg_1541" n="e" s="T207">türmeːtten </ts>
               <ts e="T209" id="Seg_1543" n="e" s="T208">taksarɨn </ts>
               <ts e="T210" id="Seg_1545" n="e" s="T209">(köhüt-) </ts>
               <ts e="T211" id="Seg_1547" n="e" s="T210">köhüten </ts>
               <ts e="T212" id="Seg_1549" n="e" s="T211">(mʼečʼt-), </ts>
               <ts e="T213" id="Seg_1551" n="e" s="T212">eː, </ts>
               <ts e="T214" id="Seg_1553" n="e" s="T213">mʼečʼtajet. </ts>
            </ts>
            <ts e="T227" id="Seg_1554" n="sc" s="T216">
               <ts e="T220" id="Seg_1556" n="e" s="T216">((LAUGH)) </ts>
               <ts e="T222" id="Seg_1558" n="e" s="T220">"Kimniːbin", </ts>
               <ts e="T224" id="Seg_1560" n="e" s="T222">diːr </ts>
               <ts e="T226" id="Seg_1562" n="e" s="T224">eː </ts>
               <ts e="T227" id="Seg_1564" n="e" s="T226">mm. </ts>
            </ts>
            <ts e="T239" id="Seg_1565" n="sc" s="T233">
               <ts e="T236" id="Seg_1567" n="e" s="T233">– Nʼet, </ts>
               <ts e="T239" id="Seg_1569" n="e" s="T236">hu͡ok. </ts>
            </ts>
            <ts e="T254" id="Seg_1570" n="sc" s="T240">
               <ts e="T243" id="Seg_1572" n="e" s="T240">Kimniː </ts>
               <ts e="T244" id="Seg_1574" n="e" s="T243">oloror, </ts>
               <ts e="T245" id="Seg_1576" n="e" s="T244">eː, </ts>
               <ts e="T246" id="Seg_1578" n="e" s="T245">(kepsi-) </ts>
               <ts e="T247" id="Seg_1580" n="e" s="T246">kimniː </ts>
               <ts e="T248" id="Seg_1582" n="e" s="T247">oloror, </ts>
               <ts e="T249" id="Seg_1584" n="e" s="T248">öjdüː </ts>
               <ts e="T250" id="Seg_1586" n="e" s="T249">oloror </ts>
               <ts e="T251" id="Seg_1588" n="e" s="T250">dʼi͡eleːkterin </ts>
               <ts e="T252" id="Seg_1590" n="e" s="T251">"ol </ts>
               <ts e="T253" id="Seg_1592" n="e" s="T252">kördük </ts>
               <ts e="T254" id="Seg_1594" n="e" s="T253">tagɨstappɨna". </ts>
            </ts>
            <ts e="T267" id="Seg_1595" n="sc" s="T256">
               <ts e="T258" id="Seg_1597" n="e" s="T256">– Eː, </ts>
               <ts e="T260" id="Seg_1599" n="e" s="T258">hanɨː </ts>
               <ts e="T261" id="Seg_1601" n="e" s="T260">oloror, </ts>
               <ts e="T263" id="Seg_1603" n="e" s="T261">öjdüː </ts>
               <ts e="T264" id="Seg_1605" n="e" s="T263">oloror, </ts>
               <ts e="T265" id="Seg_1607" n="e" s="T264">hanɨːr </ts>
               <ts e="T267" id="Seg_1609" n="e" s="T265">onton. </ts>
            </ts>
            <ts e="T272" id="Seg_1610" n="sc" s="T269">
               <ts e="T272" id="Seg_1612" n="e" s="T269">– Aː. </ts>
            </ts>
            <ts e="T285" id="Seg_1613" n="sc" s="T276">
               <ts e="T277" id="Seg_1615" n="e" s="T276">Eː, </ts>
               <ts e="T278" id="Seg_1617" n="e" s="T277">"ü͡örü͡öktere </ts>
               <ts e="T279" id="Seg_1619" n="e" s="T278">min </ts>
               <ts e="T280" id="Seg_1621" n="e" s="T279">tagɨstappɨna, </ts>
               <ts e="T281" id="Seg_1623" n="e" s="T280">aragiː </ts>
               <ts e="T282" id="Seg_1625" n="e" s="T281">ihi͡em </ts>
               <ts e="T283" id="Seg_1627" n="e" s="T282">hu͡oga", </ts>
               <ts e="T284" id="Seg_1629" n="e" s="T283">diːr, </ts>
               <ts e="T285" id="Seg_1631" n="e" s="T284">"bolʼše." </ts>
            </ts>
            <ts e="T289" id="Seg_1632" n="sc" s="T286">
               <ts e="T287" id="Seg_1634" n="e" s="T286">Taksɨbɨt </ts>
               <ts e="T288" id="Seg_1636" n="e" s="T287">onton </ts>
               <ts e="T289" id="Seg_1638" n="e" s="T288">türmeːtten. </ts>
            </ts>
            <ts e="T295" id="Seg_1639" n="sc" s="T290">
               <ts e="T291" id="Seg_1641" n="e" s="T290">Ü͡örer, </ts>
               <ts e="T292" id="Seg_1643" n="e" s="T291">ü͡örer, </ts>
               <ts e="T293" id="Seg_1645" n="e" s="T292">kün, </ts>
               <ts e="T294" id="Seg_1647" n="e" s="T293">kün </ts>
               <ts e="T295" id="Seg_1649" n="e" s="T294">tɨgar. </ts>
            </ts>
            <ts e="T308" id="Seg_1650" n="sc" s="T297">
               <ts e="T298" id="Seg_1652" n="e" s="T297">– Eː, </ts>
               <ts e="T301" id="Seg_1654" n="e" s="T298">hürdeːk </ts>
               <ts e="T302" id="Seg_1656" n="e" s="T301">üčügej </ts>
               <ts e="T304" id="Seg_1658" n="e" s="T302">bagajɨ, </ts>
               <ts e="T305" id="Seg_1660" n="e" s="T304">onton </ts>
               <ts e="T306" id="Seg_1662" n="e" s="T305">ü͡öre-ü͡öre </ts>
               <ts e="T307" id="Seg_1664" n="e" s="T306">dʼi͡etiger </ts>
               <ts e="T308" id="Seg_1666" n="e" s="T307">kelbit. </ts>
            </ts>
            <ts e="T310" id="Seg_1667" n="sc" s="T309">
               <ts e="T310" id="Seg_1669" n="e" s="T309">– Ontulara. </ts>
            </ts>
            <ts e="T321" id="Seg_1670" n="sc" s="T313">
               <ts e="T315" id="Seg_1672" n="e" s="T313">Eː, </ts>
               <ts e="T316" id="Seg_1674" n="e" s="T315">ontulara </ts>
               <ts e="T317" id="Seg_1676" n="e" s="T316">bu͡olla </ts>
               <ts e="T318" id="Seg_1678" n="e" s="T317">du͡o </ts>
               <ts e="T319" id="Seg_1680" n="e" s="T318">köröllör, </ts>
               <ts e="T320" id="Seg_1682" n="e" s="T319">kuttanallar </ts>
               <ts e="T321" id="Seg_1684" n="e" s="T320">bɨhɨlaːk. </ts>
            </ts>
            <ts e="T340" id="Seg_1685" n="sc" s="T322">
               <ts e="T323" id="Seg_1687" n="e" s="T322">Onton </ts>
               <ts e="T324" id="Seg_1689" n="e" s="T323">ogotun </ts>
               <ts e="T325" id="Seg_1691" n="e" s="T324">kɨtta </ts>
               <ts e="T326" id="Seg_1693" n="e" s="T325">hɨldʼa </ts>
               <ts e="T327" id="Seg_1695" n="e" s="T326">barbɨt. </ts>
               <ts e="T328" id="Seg_1697" n="e" s="T327">Dogottorun </ts>
               <ts e="T329" id="Seg_1699" n="e" s="T328">körsübüt, </ts>
               <ts e="T330" id="Seg_1701" n="e" s="T329">"kel, </ts>
               <ts e="T331" id="Seg_1703" n="e" s="T330">aragiːta </ts>
               <ts e="T332" id="Seg_1705" n="e" s="T331">is </ts>
               <ts e="T333" id="Seg_1707" n="e" s="T332">bihigini </ts>
               <ts e="T334" id="Seg_1709" n="e" s="T333">gɨtta", </ts>
               <ts e="T335" id="Seg_1711" n="e" s="T334">"hu͡ok, </ts>
               <ts e="T336" id="Seg_1713" n="e" s="T335">hu͡ok </ts>
               <ts e="T337" id="Seg_1715" n="e" s="T336">ispeppin". </ts>
               <ts e="T338" id="Seg_1717" n="e" s="T337">Kak </ts>
               <ts e="T339" id="Seg_1719" n="e" s="T338">skazka </ts>
               <ts e="T340" id="Seg_1721" n="e" s="T339">že. </ts>
            </ts>
            <ts e="T347" id="Seg_1722" n="sc" s="T341">
               <ts e="T342" id="Seg_1724" n="e" s="T341">"Ispeppin </ts>
               <ts e="T343" id="Seg_1726" n="e" s="T342">bolʼše, </ts>
               <ts e="T344" id="Seg_1728" n="e" s="T343">eː, </ts>
               <ts e="T345" id="Seg_1730" n="e" s="T344">bolʼše </ts>
               <ts e="T346" id="Seg_1732" n="e" s="T345">bu͡o </ts>
               <ts e="T347" id="Seg_1734" n="e" s="T346">ispeppin. </ts>
            </ts>
            <ts e="T357" id="Seg_1735" n="sc" s="T348">
               <ts e="T349" id="Seg_1737" n="e" s="T348">Ihi͡em </ts>
               <ts e="T350" id="Seg_1739" n="e" s="T349">hu͡oga, </ts>
               <ts e="T351" id="Seg_1741" n="e" s="T350">aragiːnɨ </ts>
               <ts e="T352" id="Seg_1743" n="e" s="T351">bɨrakpɨtɨm", </ts>
               <ts e="T353" id="Seg_1745" n="e" s="T352">di͡ebit, </ts>
               <ts e="T354" id="Seg_1747" n="e" s="T353">karočʼe. </ts>
               <ts e="T355" id="Seg_1749" n="e" s="T354">He, </ts>
               <ts e="T357" id="Seg_1751" n="e" s="T355">fsʼo. </ts>
            </ts>
            <ts e="T362" id="Seg_1752" n="sc" s="T359">
               <ts e="T360" id="Seg_1754" n="e" s="T359">– Bajan-toton </ts>
               <ts e="T361" id="Seg_1756" n="e" s="T360">olorbuttar </ts>
               <ts e="T362" id="Seg_1758" n="e" s="T361">onton. </ts>
            </ts>
            <ts e="T365" id="Seg_1759" n="sc" s="T363">
               <ts e="T364" id="Seg_1761" n="e" s="T363">Üčügej </ts>
               <ts e="T365" id="Seg_1763" n="e" s="T364">bagajdɨk. </ts>
            </ts>
            <ts e="T370" id="Seg_1764" n="sc" s="T366">
               <ts e="T367" id="Seg_1766" n="e" s="T366">Fsʼo, </ts>
               <ts e="T368" id="Seg_1768" n="e" s="T367">navʼerna, </ts>
               <ts e="T369" id="Seg_1770" n="e" s="T368">mala, </ts>
               <ts e="T370" id="Seg_1772" n="e" s="T369">da? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-UoPP">
            <ta e="T26" id="Seg_1773" s="T22">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.001 (001.004)</ta>
            <ta e="T50" id="Seg_1774" s="T32">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.002 (001.007)</ta>
            <ta e="T65" id="Seg_1775" s="T53">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.003 (001.010)</ta>
            <ta e="T72" id="Seg_1776" s="T65">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.004 (001.011)</ta>
            <ta e="T75" id="Seg_1777" s="T73">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.005 (001.012)</ta>
            <ta e="T79" id="Seg_1778" s="T75">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.006 (001.012)</ta>
            <ta e="T85" id="Seg_1779" s="T80">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.007 (001.013)</ta>
            <ta e="T89" id="Seg_1780" s="T85">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.008 (001.014)</ta>
            <ta e="T101" id="Seg_1781" s="T89">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.009 (001.015)</ta>
            <ta e="T110" id="Seg_1782" s="T105">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.010 (001.018)</ta>
            <ta e="T114" id="Seg_1783" s="T111">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.011 (001.019)</ta>
            <ta e="T122" id="Seg_1784" s="T115">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.012 (001.020)</ta>
            <ta e="T125" id="Seg_1785" s="T122">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.013 (001.021)</ta>
            <ta e="T135" id="Seg_1786" s="T126">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.014 (001.022)</ta>
            <ta e="T141" id="Seg_1787" s="T136">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.015 (001.023)</ta>
            <ta e="T145" id="Seg_1788" s="T141">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.016 (001.024)</ta>
            <ta e="T149" id="Seg_1789" s="T146">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.017 (001.025)</ta>
            <ta e="T172" id="Seg_1790" s="T150">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.018 (001.026)</ta>
            <ta e="T178" id="Seg_1791" s="T173">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.019 (001.028)</ta>
            <ta e="T189" id="Seg_1792" s="T179">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.020 (001.029)</ta>
            <ta e="T192" id="Seg_1793" s="T190">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.021 (001.031)</ta>
            <ta e="T204" id="Seg_1794" s="T193">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.022 (001.032)</ta>
            <ta e="T214" id="Seg_1795" s="T205">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.023 (001.033)</ta>
            <ta e="T227" id="Seg_1796" s="T216">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.024 (001.035)</ta>
            <ta e="T239" id="Seg_1797" s="T233">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.025 (001.038)</ta>
            <ta e="T254" id="Seg_1798" s="T240">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.026 (001.040)</ta>
            <ta e="T267" id="Seg_1799" s="T256">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.027 (001.042)</ta>
            <ta e="T272" id="Seg_1800" s="T269">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.028 (001.044)</ta>
            <ta e="T285" id="Seg_1801" s="T276">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.029 (001.045)</ta>
            <ta e="T289" id="Seg_1802" s="T286">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.030 (001.046)</ta>
            <ta e="T295" id="Seg_1803" s="T290">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.031 (001.047)</ta>
            <ta e="T308" id="Seg_1804" s="T297">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.032 (001.049)</ta>
            <ta e="T310" id="Seg_1805" s="T309">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.033 (001.051)</ta>
            <ta e="T321" id="Seg_1806" s="T313">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.034 (001.053)</ta>
            <ta e="T327" id="Seg_1807" s="T322">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.035 (001.054)</ta>
            <ta e="T337" id="Seg_1808" s="T327">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.036 (001.055)</ta>
            <ta e="T340" id="Seg_1809" s="T337">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.037 (001.056)</ta>
            <ta e="T347" id="Seg_1810" s="T341">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.038 (001.057)</ta>
            <ta e="T354" id="Seg_1811" s="T348">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.039 (001.058)</ta>
            <ta e="T357" id="Seg_1812" s="T354">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.040 (001.059)</ta>
            <ta e="T362" id="Seg_1813" s="T359">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.041 (001.061)</ta>
            <ta e="T365" id="Seg_1814" s="T363">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.042 (001.062)</ta>
            <ta e="T370" id="Seg_1815" s="T366">UoPP_ChGS_20170724_SocCogRetell1_nar.UoPP.043 (001.063)</ta>
         </annotation>
         <annotation name="st" tierref="st-UoPP">
            <ta e="T26" id="Seg_1816" s="T22">Кэпсээ, дого, эн.</ta>
            <ta e="T50" id="Seg_1817" s="T32">Ээ вообще биир семья ээ кайтак этэй?</ta>
            <ta e="T65" id="Seg_1818" s="T53">Биир ыаллар буолар үчүгэй багайыдык олорор этилер эбит үлэлииллэр, үлэлиир этилэр.</ta>
            <ta e="T72" id="Seg_1819" s="T65">Онтон буо доготторун көрсөн араги иһэ барбыт.</ta>
            <ta e="T75" id="Seg_1820" s="T73">Онно кэпсииллэр </ta>
            <ta e="T79" id="Seg_1821" s="T75">"эн дьактарын ол көрдүк".</ta>
            <ta e="T85" id="Seg_1822" s="T80">"Бу көрдүк, ол киһини кытта."</ta>
            <ta e="T89" id="Seg_1823" s="T85">"Кэпсэппитэ ээ кимниир этэ."</ta>
            <ta e="T101" id="Seg_1824" s="T89">Кайдак дьиэччилэрий? Короче заигрывала.</ta>
            <ta e="T110" id="Seg_1825" s="T105">Ээ, дьактарыӈ ол көрдүк көрсүлэһэр.</ta>
            <ta e="T114" id="Seg_1826" s="T111">Бу көрдүк дииллэр.</ta>
            <ta e="T122" id="Seg_1827" s="T115">Онтон баран дьактарыттан ыйытар, кимниир ээ үгүлүүр.</ta>
            <ta e="T125" id="Seg_1828" s="T122">Онтон оксубут дьактарын.</ta>
            <ta e="T135" id="Seg_1829" s="T126">Дьактара оголоок оголоок илиитигэр оголоок һылдьар дьактары кырбыыр.</ta>
            <ta e="T141" id="Seg_1830" s="T136">Милициялар кэлбиттэр ким эрэ ыгырбыт.</ta>
            <ta e="T145" id="Seg_1831" s="T141">Онтон, эээ, милициялар илпиттэр.</ta>
            <ta e="T149" id="Seg_1832" s="T146">Барыларын илпиттэр онно.</ta>
            <ta e="T172" id="Seg_1833" s="T150">Допросдаабыттар, допросдаабыттар аата тугуй? Ыйыталлаабыттар, кайдак туок буолбуккутуй, кайдак туок буолуон өлөрсүбүккүтүй дии кэпсиир онно дьактара.</ta>
            <ta e="T178" id="Seg_1834" s="T173">Ол көрдүк кырбаата, бу көрдүк.</ta>
            <ta e="T189" id="Seg_1835" s="T179">Онтон тюрмэгэ олорпуттар, ээ, тюрмэгэ, кайыыга олорпуттар, аһа.</ta>
            <ta e="T192" id="Seg_1836" s="T190">Кайыыга олорпуттар.</ta>
            <ta e="T204" id="Seg_1837" s="T193">Онтон бу (он-) өйдүү олорор, оготун, дьактарын өйдүү олорор ол көрдүк.</ta>
            <ta e="T214" id="Seg_1838" s="T205">Мм, таксарын тюрмэттэн таксарын көһүтэр (мечт-) ээ мечтает.</ta>
            <ta e="T227" id="Seg_1839" s="T216">Кимниибин диир ээ мм.</ta>
            <ta e="T239" id="Seg_1840" s="T233">Нет, һуок.</ta>
            <ta e="T254" id="Seg_1841" s="T240">Кимнии олорор, ээ, (кэпси-) кимнии олорор, өйдүү олорор диэклээктэрин ол көрдүк тагыстакпына.</ta>
            <ta e="T267" id="Seg_1842" s="T256">Ээ, һаныы олорор, өйдүү олорор, һаныыр онтон.</ta>
            <ta e="T272" id="Seg_1843" s="T269">Аа.</ta>
            <ta e="T285" id="Seg_1844" s="T276">Үөрүөктэрэ мин тагыстаппына "араги иһиэм һуога" диир "больше".</ta>
            <ta e="T289" id="Seg_1845" s="T286">Таксыбыт онтон тюрмээттэн.</ta>
            <ta e="T295" id="Seg_1846" s="T290">Үөрэр үөрэр күн тыгар.</ta>
            <ta e="T308" id="Seg_1847" s="T297">Ээ һүрдээк үчүгэй багайы онтон үөрэ-үөрэ диэтигэр кэлбит.</ta>
            <ta e="T310" id="Seg_1848" s="T309">Онтулара.</ta>
            <ta e="T321" id="Seg_1849" s="T313">Ээ, онтулара буолла дуо көрөллөр куттаналлар быһылаак.</ta>
            <ta e="T327" id="Seg_1850" s="T322">Онтон оготун кытта һылдьа барбыт.</ta>
            <ta e="T337" id="Seg_1851" s="T327">Доготторун көрсүбүт, "кэл, арагита ис биһигини кытта" "һуок, һуок, испэппин".</ta>
            <ta e="T340" id="Seg_1852" s="T337">Как сказка же.</ta>
            <ta e="T347" id="Seg_1853" s="T341">"Испэппин больше, ээ, больше буолтак испэппин".</ta>
            <ta e="T354" id="Seg_1854" s="T348">"Иһиэм һуога арагины быракпытым" диэбит короче.</ta>
            <ta e="T357" id="Seg_1855" s="T354">Һэ, все.</ta>
            <ta e="T362" id="Seg_1856" s="T359">Байан-тотон олорбуттар онтон.</ta>
            <ta e="T365" id="Seg_1857" s="T363">Үчүгэй багайытык.</ta>
            <ta e="T370" id="Seg_1858" s="T366">Все, да, наверно, мало, да?</ta>
         </annotation>
         <annotation name="ts" tierref="ts-UoPP">
            <ta e="T26" id="Seg_1859" s="T22">– Kepseː, dogo, en. </ta>
            <ta e="T50" id="Seg_1860" s="T32">– Eː, vaabšʼe biːr biːr, eː, "sʼemʼja", kajtak etej? </ta>
            <ta e="T65" id="Seg_1861" s="T53">– Biːr ɨ͡allar bu͡ollar, eː, üčügej bagajdɨk oloror etiler ebit, üleliːller, üleliːr etiler. </ta>
            <ta e="T72" id="Seg_1862" s="T65">Onton bu͡o dogottorun körsön aragiː ihe barbɨt. </ta>
            <ta e="T75" id="Seg_1863" s="T73">Onno kepsiːller: </ta>
            <ta e="T79" id="Seg_1864" s="T75">"En dʼaktarɨŋ ol kördük. </ta>
            <ta e="T85" id="Seg_1865" s="T80">Bu kördük, ol kihini gɨtta. </ta>
            <ta e="T89" id="Seg_1866" s="T85">Kepseppite, eː, kimniːr ete." </ta>
            <ta e="T101" id="Seg_1867" s="T89">Kajdak di͡eččiler, karoče zaigrɨvala ((…)). </ta>
            <ta e="T110" id="Seg_1868" s="T105">"Eː, dʼaktarɨŋ ol kördük körsüleher." </ta>
            <ta e="T114" id="Seg_1869" s="T111">Bu kördük diːller. </ta>
            <ta e="T122" id="Seg_1870" s="T115">Onton baran dʼaktarɨttan ɨjɨtar, kimniːr eː ü͡ögülüːr. </ta>
            <ta e="T125" id="Seg_1871" s="T122">Onton oksubut dʼaktarɨn. </ta>
            <ta e="T135" id="Seg_1872" s="T126">(Dʼak-) dʼaktar ogoloːk ogoloːk iliːtiger ogoloːk hɨldʼar dʼaktarɨ kɨrbɨːr. </ta>
            <ta e="T141" id="Seg_1873" s="T136">Mʼilʼisʼijeler kelbitter, kim ere ɨgɨrbɨt. </ta>
            <ta e="T145" id="Seg_1874" s="T141">Onton, mm, mʼilʼisʼijeler ilpitter. </ta>
            <ta e="T149" id="Seg_1875" s="T146">Barɨlarɨn ilpitter onno. </ta>
            <ta e="T172" id="Seg_1876" s="T150">Daprosdaːbɨttar, daprosdaːbɨttar, ɨjɨtalaːbɨttar, "kajtak tu͡ok bu͡olbukkutuj, kajtak tu͡ok bu͡olu͡on ölörsübükkütüj", diː kepsiːr onno dʼaktara. </ta>
            <ta e="T178" id="Seg_1877" s="T173">"Ol kördük kɨrbaːta, bu kördük." </ta>
            <ta e="T189" id="Seg_1878" s="T179">Onton türmege olorputtar, eː, türme, kaːjɨːga olorputtar, aha. </ta>
            <ta e="T192" id="Seg_1879" s="T190">Kaːjɨːga olorputtar. </ta>
            <ta e="T204" id="Seg_1880" s="T193">Onton bu (on-) öjdüː oloror, ogotun, dʼaktarɨn öjdüː oloror ol kördük. </ta>
            <ta e="T214" id="Seg_1881" s="T205">Mm, taksarɨn türmeːtten taksarɨn (köhüt-) köhüten (mʼečʼt-), eː, mʼečʼtajet. </ta>
            <ta e="T227" id="Seg_1882" s="T216">((LAUGH)) "Kimniːbin", diːr eː mm. </ta>
            <ta e="T239" id="Seg_1883" s="T233">– Nʼet, hu͡ok. </ta>
            <ta e="T254" id="Seg_1884" s="T240">Kimniː oloror, eː, (kepsi-) kimniː oloror, öjdüː oloror dʼi͡eleːkterin "ol kördük tagɨstappɨna". </ta>
            <ta e="T267" id="Seg_1885" s="T256">– Eː, hanɨː oloror, öjdüː oloror, hanɨːr onton. </ta>
            <ta e="T272" id="Seg_1886" s="T269">– Aː. </ta>
            <ta e="T285" id="Seg_1887" s="T276">Eː, "ü͡örü͡öktere min tagɨstappɨna, aragiː ihi͡em hu͡oga", diːr, "bolʼše." </ta>
            <ta e="T289" id="Seg_1888" s="T286">Taksɨbɨt onton türmeːtten. </ta>
            <ta e="T295" id="Seg_1889" s="T290">Ü͡örer, ü͡örer, kün, kün tɨgar. </ta>
            <ta e="T308" id="Seg_1890" s="T297">– Eː, hürdeːk üčügej bagajɨ, onton ü͡öre-ü͡öre dʼi͡etiger kelbit. </ta>
            <ta e="T310" id="Seg_1891" s="T309">– Ontulara. </ta>
            <ta e="T321" id="Seg_1892" s="T313">Eː, ontulara bu͡olla du͡o köröllör, kuttanallar bɨhɨlaːk. </ta>
            <ta e="T327" id="Seg_1893" s="T322">Onton ogotun kɨtta hɨldʼa barbɨt. </ta>
            <ta e="T337" id="Seg_1894" s="T327">Dogottorun körsübüt, "kel, aragiːta is bihigini gɨtta", "hu͡ok, hu͡ok ispeppin". </ta>
            <ta e="T340" id="Seg_1895" s="T337">Kak skazka že. </ta>
            <ta e="T347" id="Seg_1896" s="T341">"Ispeppin bolʼše, eː, bolʼše bu͡o ispeppin. </ta>
            <ta e="T354" id="Seg_1897" s="T348">Ihi͡em hu͡oga, aragiːnɨ bɨrakpɨtɨm", di͡ebit, karočʼe. </ta>
            <ta e="T357" id="Seg_1898" s="T354">He, fsʼo. </ta>
            <ta e="T362" id="Seg_1899" s="T359">– Bajan-toton olorbuttar onton. </ta>
            <ta e="T365" id="Seg_1900" s="T363">Üčügej bagajdɨk. </ta>
            <ta e="T370" id="Seg_1901" s="T366">Fsʼo, navʼerna, mala, da? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-UoPP">
            <ta e="T24" id="Seg_1902" s="T22">kepseː</ta>
            <ta e="T25" id="Seg_1903" s="T24">dogo</ta>
            <ta e="T26" id="Seg_1904" s="T25">en</ta>
            <ta e="T34" id="Seg_1905" s="T32">eː</ta>
            <ta e="T37" id="Seg_1906" s="T34">vaabšʼe</ta>
            <ta e="T39" id="Seg_1907" s="T37">biːr</ta>
            <ta e="T42" id="Seg_1908" s="T39">biːr</ta>
            <ta e="T44" id="Seg_1909" s="T42">eː</ta>
            <ta e="T48" id="Seg_1910" s="T46">kajtak</ta>
            <ta e="T50" id="Seg_1911" s="T48">e-t-e=j</ta>
            <ta e="T54" id="Seg_1912" s="T53">biːr</ta>
            <ta e="T55" id="Seg_1913" s="T54">ɨ͡al-lar</ta>
            <ta e="T56" id="Seg_1914" s="T55">bu͡ol-lar</ta>
            <ta e="T57" id="Seg_1915" s="T56">eː</ta>
            <ta e="T58" id="Seg_1916" s="T57">üčügej</ta>
            <ta e="T59" id="Seg_1917" s="T58">bagaj-dɨk</ta>
            <ta e="T60" id="Seg_1918" s="T59">olor-or</ta>
            <ta e="T61" id="Seg_1919" s="T60">e-ti-ler</ta>
            <ta e="T62" id="Seg_1920" s="T61">e-bit</ta>
            <ta e="T63" id="Seg_1921" s="T62">üleliː-l-ler</ta>
            <ta e="T64" id="Seg_1922" s="T63">üleliː-r</ta>
            <ta e="T65" id="Seg_1923" s="T64">e-ti-ler</ta>
            <ta e="T66" id="Seg_1924" s="T65">onton</ta>
            <ta e="T67" id="Seg_1925" s="T66">bu͡o</ta>
            <ta e="T68" id="Seg_1926" s="T67">dogot-tor-u-n</ta>
            <ta e="T69" id="Seg_1927" s="T68">körs-ön</ta>
            <ta e="T70" id="Seg_1928" s="T69">aragiː</ta>
            <ta e="T71" id="Seg_1929" s="T70">ih-e</ta>
            <ta e="T72" id="Seg_1930" s="T71">bar-bɨt</ta>
            <ta e="T74" id="Seg_1931" s="T73">onno</ta>
            <ta e="T75" id="Seg_1932" s="T74">kepsiː-l-ler</ta>
            <ta e="T76" id="Seg_1933" s="T75">en</ta>
            <ta e="T77" id="Seg_1934" s="T76">dʼaktar-ɨ-ŋ</ta>
            <ta e="T78" id="Seg_1935" s="T77">ol</ta>
            <ta e="T79" id="Seg_1936" s="T78">kördük</ta>
            <ta e="T81" id="Seg_1937" s="T80">bu</ta>
            <ta e="T82" id="Seg_1938" s="T81">kördük</ta>
            <ta e="T83" id="Seg_1939" s="T82">ol</ta>
            <ta e="T84" id="Seg_1940" s="T83">kihi-ni</ta>
            <ta e="T85" id="Seg_1941" s="T84">gɨtta</ta>
            <ta e="T86" id="Seg_1942" s="T85">kepsep-pit-e</ta>
            <ta e="T87" id="Seg_1943" s="T86">eː</ta>
            <ta e="T88" id="Seg_1944" s="T87">kim-niː-r</ta>
            <ta e="T89" id="Seg_1945" s="T88">e-t-e</ta>
            <ta e="T90" id="Seg_1946" s="T89">kajdak</ta>
            <ta e="T92" id="Seg_1947" s="T90">di͡e-čči-ler</ta>
            <ta e="T106" id="Seg_1948" s="T105">eː</ta>
            <ta e="T107" id="Seg_1949" s="T106">dʼaktar-ɨ-ŋ</ta>
            <ta e="T108" id="Seg_1950" s="T107">ol</ta>
            <ta e="T109" id="Seg_1951" s="T108">kördük</ta>
            <ta e="T110" id="Seg_1952" s="T109">körs-ü-l-e-h-er</ta>
            <ta e="T112" id="Seg_1953" s="T111">bu</ta>
            <ta e="T113" id="Seg_1954" s="T112">kördük</ta>
            <ta e="T114" id="Seg_1955" s="T113">diː-l-ler</ta>
            <ta e="T116" id="Seg_1956" s="T115">onton</ta>
            <ta e="T117" id="Seg_1957" s="T116">bar-an</ta>
            <ta e="T118" id="Seg_1958" s="T117">dʼaktar-ɨ-ttan</ta>
            <ta e="T119" id="Seg_1959" s="T118">ɨjɨt-ar</ta>
            <ta e="T120" id="Seg_1960" s="T119">kim-niː-r</ta>
            <ta e="T121" id="Seg_1961" s="T120">eː</ta>
            <ta e="T122" id="Seg_1962" s="T121">ü͡ögülüː-r</ta>
            <ta e="T123" id="Seg_1963" s="T122">onton</ta>
            <ta e="T124" id="Seg_1964" s="T123">oks-u-but</ta>
            <ta e="T125" id="Seg_1965" s="T124">dʼaktar-ɨ-n</ta>
            <ta e="T128" id="Seg_1966" s="T127">dʼaktar</ta>
            <ta e="T129" id="Seg_1967" s="T128">ogo-loːk</ta>
            <ta e="T130" id="Seg_1968" s="T129">ogo-loːk</ta>
            <ta e="T131" id="Seg_1969" s="T130">iliː-ti-ger</ta>
            <ta e="T132" id="Seg_1970" s="T131">ogo-loːk</ta>
            <ta e="T133" id="Seg_1971" s="T132">hɨldʼ-ar</ta>
            <ta e="T134" id="Seg_1972" s="T133">dʼaktar-ɨ</ta>
            <ta e="T135" id="Seg_1973" s="T134">kɨrbɨː-r</ta>
            <ta e="T137" id="Seg_1974" s="T136">mʼilʼisʼije-ler</ta>
            <ta e="T138" id="Seg_1975" s="T137">kel-bit-ter</ta>
            <ta e="T139" id="Seg_1976" s="T138">kim</ta>
            <ta e="T140" id="Seg_1977" s="T139">ere</ta>
            <ta e="T141" id="Seg_1978" s="T140">ɨgɨr-bɨt</ta>
            <ta e="T142" id="Seg_1979" s="T141">onton</ta>
            <ta e="T143" id="Seg_1980" s="T142">mm</ta>
            <ta e="T144" id="Seg_1981" s="T143">mʼilʼisʼije-ler</ta>
            <ta e="T145" id="Seg_1982" s="T144">il-pit-ter</ta>
            <ta e="T147" id="Seg_1983" s="T146">barɨ-larɨ-n</ta>
            <ta e="T148" id="Seg_1984" s="T147">il-pit-ter</ta>
            <ta e="T149" id="Seg_1985" s="T148">onno</ta>
            <ta e="T152" id="Seg_1986" s="T150">dapros-daː-bɨt-tar</ta>
            <ta e="T154" id="Seg_1987" s="T152">dapros-daː-bɨt-tar</ta>
            <ta e="T156" id="Seg_1988" s="T154">ɨjɨt-alaː-bɨt-tar</ta>
            <ta e="T158" id="Seg_1989" s="T156">kajtak</ta>
            <ta e="T160" id="Seg_1990" s="T158">tu͡ok</ta>
            <ta e="T162" id="Seg_1991" s="T160">bu͡ol-buk-kut=uj</ta>
            <ta e="T164" id="Seg_1992" s="T162">kajtak</ta>
            <ta e="T166" id="Seg_1993" s="T164">tu͡ok</ta>
            <ta e="T167" id="Seg_1994" s="T166">bu͡ol-u͡o-n</ta>
            <ta e="T168" id="Seg_1995" s="T167">ölörs-ü-bük-küt=üj</ta>
            <ta e="T169" id="Seg_1996" s="T168">d-iː</ta>
            <ta e="T170" id="Seg_1997" s="T169">kepsiː-r</ta>
            <ta e="T171" id="Seg_1998" s="T170">onno</ta>
            <ta e="T172" id="Seg_1999" s="T171">dʼaktar-a</ta>
            <ta e="T174" id="Seg_2000" s="T173">ol</ta>
            <ta e="T175" id="Seg_2001" s="T174">kördük</ta>
            <ta e="T176" id="Seg_2002" s="T175">kɨrbaː-t-a</ta>
            <ta e="T177" id="Seg_2003" s="T176">bu</ta>
            <ta e="T178" id="Seg_2004" s="T177">kördük</ta>
            <ta e="T180" id="Seg_2005" s="T179">onton</ta>
            <ta e="T181" id="Seg_2006" s="T180">türme-ge</ta>
            <ta e="T182" id="Seg_2007" s="T181">olor-put-tar</ta>
            <ta e="T183" id="Seg_2008" s="T182">eː</ta>
            <ta e="T185" id="Seg_2009" s="T183">türme</ta>
            <ta e="T186" id="Seg_2010" s="T185">kaːjɨː-ga</ta>
            <ta e="T188" id="Seg_2011" s="T186">olor-put-tar</ta>
            <ta e="T189" id="Seg_2012" s="T188">aha</ta>
            <ta e="T191" id="Seg_2013" s="T190">kaːjɨː-ga</ta>
            <ta e="T192" id="Seg_2014" s="T191">olor-put-tar</ta>
            <ta e="T194" id="Seg_2015" s="T193">onton</ta>
            <ta e="T195" id="Seg_2016" s="T194">bu</ta>
            <ta e="T197" id="Seg_2017" s="T196">öjd-üː</ta>
            <ta e="T198" id="Seg_2018" s="T197">olor-or</ta>
            <ta e="T199" id="Seg_2019" s="T198">ogo-tu-n</ta>
            <ta e="T200" id="Seg_2020" s="T199">dʼaktar-ɨ-n</ta>
            <ta e="T201" id="Seg_2021" s="T200">öjd-üː</ta>
            <ta e="T202" id="Seg_2022" s="T201">olor-or</ta>
            <ta e="T203" id="Seg_2023" s="T202">ol</ta>
            <ta e="T204" id="Seg_2024" s="T203">kördük</ta>
            <ta e="T206" id="Seg_2025" s="T205">mm</ta>
            <ta e="T207" id="Seg_2026" s="T206">taks-ar-ɨ-n</ta>
            <ta e="T208" id="Seg_2027" s="T207">türmeː-tten</ta>
            <ta e="T209" id="Seg_2028" s="T208">taks-ar-ɨ-n</ta>
            <ta e="T210" id="Seg_2029" s="T209">köhüt</ta>
            <ta e="T211" id="Seg_2030" s="T210">köhüt-en</ta>
            <ta e="T222" id="Seg_2031" s="T220">kim-n-iː-bin</ta>
            <ta e="T224" id="Seg_2032" s="T222">diː-r</ta>
            <ta e="T226" id="Seg_2033" s="T224">eː</ta>
            <ta e="T227" id="Seg_2034" s="T226">mm</ta>
            <ta e="T239" id="Seg_2035" s="T236">hu͡ok</ta>
            <ta e="T243" id="Seg_2036" s="T240">kim-n-iː</ta>
            <ta e="T244" id="Seg_2037" s="T243">olor-or</ta>
            <ta e="T245" id="Seg_2038" s="T244">eː</ta>
            <ta e="T247" id="Seg_2039" s="T246">kim-n-iː</ta>
            <ta e="T248" id="Seg_2040" s="T247">olor-or</ta>
            <ta e="T249" id="Seg_2041" s="T248">öjd-üː</ta>
            <ta e="T250" id="Seg_2042" s="T249">olor-or</ta>
            <ta e="T251" id="Seg_2043" s="T250">dʼi͡e-leːk-ter-i-n</ta>
            <ta e="T252" id="Seg_2044" s="T251">ol</ta>
            <ta e="T253" id="Seg_2045" s="T252">kördük</ta>
            <ta e="T254" id="Seg_2046" s="T253">tagɨs-tap-pɨna</ta>
            <ta e="T258" id="Seg_2047" s="T256">eː</ta>
            <ta e="T260" id="Seg_2048" s="T258">han-ɨː</ta>
            <ta e="T261" id="Seg_2049" s="T260">olor-or</ta>
            <ta e="T263" id="Seg_2050" s="T261">öjd-üː</ta>
            <ta e="T264" id="Seg_2051" s="T263">olor-or</ta>
            <ta e="T265" id="Seg_2052" s="T264">hanɨː-r</ta>
            <ta e="T267" id="Seg_2053" s="T265">onton</ta>
            <ta e="T272" id="Seg_2054" s="T269">aː</ta>
            <ta e="T277" id="Seg_2055" s="T276">eː</ta>
            <ta e="T278" id="Seg_2056" s="T277">ü͡ör-ü͡ök-tere</ta>
            <ta e="T279" id="Seg_2057" s="T278">min</ta>
            <ta e="T280" id="Seg_2058" s="T279">tagɨs-tap-pɨna</ta>
            <ta e="T281" id="Seg_2059" s="T280">aragiː</ta>
            <ta e="T282" id="Seg_2060" s="T281">ih-i͡e-m</ta>
            <ta e="T283" id="Seg_2061" s="T282">hu͡og-a</ta>
            <ta e="T284" id="Seg_2062" s="T283">diː-r</ta>
            <ta e="T285" id="Seg_2063" s="T284">bolʼše</ta>
            <ta e="T287" id="Seg_2064" s="T286">taks-ɨ-bɨt</ta>
            <ta e="T288" id="Seg_2065" s="T287">onton</ta>
            <ta e="T289" id="Seg_2066" s="T288">türmeː-tten</ta>
            <ta e="T291" id="Seg_2067" s="T290">ü͡ör-er</ta>
            <ta e="T292" id="Seg_2068" s="T291">ü͡ör-er</ta>
            <ta e="T293" id="Seg_2069" s="T292">kün</ta>
            <ta e="T294" id="Seg_2070" s="T293">kün</ta>
            <ta e="T295" id="Seg_2071" s="T294">tɨg-ar</ta>
            <ta e="T298" id="Seg_2072" s="T297">eː</ta>
            <ta e="T301" id="Seg_2073" s="T298">hürdeːk</ta>
            <ta e="T302" id="Seg_2074" s="T301">üčügej</ta>
            <ta e="T304" id="Seg_2075" s="T302">bagajɨ</ta>
            <ta e="T305" id="Seg_2076" s="T304">onton</ta>
            <ta e="T306" id="Seg_2077" s="T305">ü͡ör-e-ü͡ör-e</ta>
            <ta e="T307" id="Seg_2078" s="T306">dʼi͡e-ti-ger</ta>
            <ta e="T308" id="Seg_2079" s="T307">kel-bit</ta>
            <ta e="T310" id="Seg_2080" s="T309">on-tu-lara</ta>
            <ta e="T315" id="Seg_2081" s="T313">eː</ta>
            <ta e="T316" id="Seg_2082" s="T315">on-tu-lara</ta>
            <ta e="T317" id="Seg_2083" s="T316">bu͡olla</ta>
            <ta e="T318" id="Seg_2084" s="T317">du͡o</ta>
            <ta e="T319" id="Seg_2085" s="T318">kör-öl-lör</ta>
            <ta e="T320" id="Seg_2086" s="T319">kuttan-al-lar</ta>
            <ta e="T321" id="Seg_2087" s="T320">bɨhɨlaːk</ta>
            <ta e="T323" id="Seg_2088" s="T322">onton</ta>
            <ta e="T324" id="Seg_2089" s="T323">ogo-tu-n</ta>
            <ta e="T325" id="Seg_2090" s="T324">kɨtta</ta>
            <ta e="T326" id="Seg_2091" s="T325">hɨldʼ-a</ta>
            <ta e="T327" id="Seg_2092" s="T326">bar-bɨt</ta>
            <ta e="T328" id="Seg_2093" s="T327">dogot-tor-u-n</ta>
            <ta e="T329" id="Seg_2094" s="T328">körs-ü-büt</ta>
            <ta e="T330" id="Seg_2095" s="T329">kel</ta>
            <ta e="T331" id="Seg_2096" s="T330">aragiː-ta</ta>
            <ta e="T332" id="Seg_2097" s="T331">is</ta>
            <ta e="T333" id="Seg_2098" s="T332">bihigi-ni</ta>
            <ta e="T334" id="Seg_2099" s="T333">gɨtta</ta>
            <ta e="T335" id="Seg_2100" s="T334">hu͡ok</ta>
            <ta e="T336" id="Seg_2101" s="T335">hu͡ok</ta>
            <ta e="T337" id="Seg_2102" s="T336">is-pep-pin</ta>
            <ta e="T342" id="Seg_2103" s="T341">is-pep-pin</ta>
            <ta e="T343" id="Seg_2104" s="T342">bolʼše</ta>
            <ta e="T344" id="Seg_2105" s="T343">eː</ta>
            <ta e="T345" id="Seg_2106" s="T344">bolʼše</ta>
            <ta e="T346" id="Seg_2107" s="T345">bu͡o</ta>
            <ta e="T347" id="Seg_2108" s="T346">is-pep-pin</ta>
            <ta e="T349" id="Seg_2109" s="T348">ih-i͡e-m</ta>
            <ta e="T350" id="Seg_2110" s="T349">hu͡og-a</ta>
            <ta e="T351" id="Seg_2111" s="T350">aragiː-nɨ</ta>
            <ta e="T352" id="Seg_2112" s="T351">bɨrak-pɨt-ɨ-m</ta>
            <ta e="T353" id="Seg_2113" s="T352">di͡e-bit</ta>
            <ta e="T354" id="Seg_2114" s="T353">karočʼe</ta>
            <ta e="T360" id="Seg_2115" s="T359">baj-an-tot-on</ta>
            <ta e="T361" id="Seg_2116" s="T360">olor-but-tar</ta>
            <ta e="T362" id="Seg_2117" s="T361">onton</ta>
            <ta e="T364" id="Seg_2118" s="T363">üčügej</ta>
            <ta e="T365" id="Seg_2119" s="T364">bagaj-dɨk</ta>
         </annotation>
         <annotation name="mp" tierref="mp-UoPP">
            <ta e="T24" id="Seg_2120" s="T22">kepseː</ta>
            <ta e="T25" id="Seg_2121" s="T24">dogor</ta>
            <ta e="T26" id="Seg_2122" s="T25">en</ta>
            <ta e="T34" id="Seg_2123" s="T32">eː</ta>
            <ta e="T37" id="Seg_2124" s="T34">vaabšʼe</ta>
            <ta e="T39" id="Seg_2125" s="T37">biːr</ta>
            <ta e="T42" id="Seg_2126" s="T39">biːr</ta>
            <ta e="T44" id="Seg_2127" s="T42">eː</ta>
            <ta e="T48" id="Seg_2128" s="T46">kajdak</ta>
            <ta e="T50" id="Seg_2129" s="T48">e-TI-tA=Ij</ta>
            <ta e="T54" id="Seg_2130" s="T53">biːr</ta>
            <ta e="T55" id="Seg_2131" s="T54">ɨ͡al-LAr</ta>
            <ta e="T56" id="Seg_2132" s="T55">bu͡ol-TAR</ta>
            <ta e="T57" id="Seg_2133" s="T56">eː</ta>
            <ta e="T58" id="Seg_2134" s="T57">üčügej</ta>
            <ta e="T59" id="Seg_2135" s="T58">bagajɨ-LIk</ta>
            <ta e="T60" id="Seg_2136" s="T59">olor-Ar</ta>
            <ta e="T61" id="Seg_2137" s="T60">e-TI-LAr</ta>
            <ta e="T62" id="Seg_2138" s="T61">e-BIT</ta>
            <ta e="T63" id="Seg_2139" s="T62">üleleː-Ar-LAr</ta>
            <ta e="T64" id="Seg_2140" s="T63">üleleː-Ar</ta>
            <ta e="T65" id="Seg_2141" s="T64">e-TI-LAr</ta>
            <ta e="T66" id="Seg_2142" s="T65">onton</ta>
            <ta e="T67" id="Seg_2143" s="T66">bu͡o</ta>
            <ta e="T68" id="Seg_2144" s="T67">dogor-LAr-tI-n</ta>
            <ta e="T69" id="Seg_2145" s="T68">körüs-An</ta>
            <ta e="T70" id="Seg_2146" s="T69">aragiː</ta>
            <ta e="T71" id="Seg_2147" s="T70">is-A</ta>
            <ta e="T72" id="Seg_2148" s="T71">bar-BIT</ta>
            <ta e="T74" id="Seg_2149" s="T73">onno</ta>
            <ta e="T75" id="Seg_2150" s="T74">kepseː-Ar-LAr</ta>
            <ta e="T76" id="Seg_2151" s="T75">en</ta>
            <ta e="T77" id="Seg_2152" s="T76">dʼaktar-I-ŋ</ta>
            <ta e="T78" id="Seg_2153" s="T77">ol</ta>
            <ta e="T79" id="Seg_2154" s="T78">kördük</ta>
            <ta e="T81" id="Seg_2155" s="T80">bu</ta>
            <ta e="T82" id="Seg_2156" s="T81">kördük</ta>
            <ta e="T83" id="Seg_2157" s="T82">ol</ta>
            <ta e="T84" id="Seg_2158" s="T83">kihi-nI</ta>
            <ta e="T85" id="Seg_2159" s="T84">kɨtta</ta>
            <ta e="T86" id="Seg_2160" s="T85">kepset-BIT-tA</ta>
            <ta e="T87" id="Seg_2161" s="T86">eː</ta>
            <ta e="T88" id="Seg_2162" s="T87">kim-LAː-Ar</ta>
            <ta e="T89" id="Seg_2163" s="T88">e-TI-tA</ta>
            <ta e="T90" id="Seg_2164" s="T89">kajdak</ta>
            <ta e="T92" id="Seg_2165" s="T90">di͡e-AːččI-LAr</ta>
            <ta e="T106" id="Seg_2166" s="T105">eː</ta>
            <ta e="T107" id="Seg_2167" s="T106">dʼaktar-I-ŋ</ta>
            <ta e="T108" id="Seg_2168" s="T107">ol</ta>
            <ta e="T109" id="Seg_2169" s="T108">kördük</ta>
            <ta e="T110" id="Seg_2170" s="T109">körüs-I-TAː-A-s-Ar</ta>
            <ta e="T112" id="Seg_2171" s="T111">bu</ta>
            <ta e="T113" id="Seg_2172" s="T112">kördük</ta>
            <ta e="T114" id="Seg_2173" s="T113">di͡e-Ar-LAr</ta>
            <ta e="T116" id="Seg_2174" s="T115">onton</ta>
            <ta e="T117" id="Seg_2175" s="T116">bar-An</ta>
            <ta e="T118" id="Seg_2176" s="T117">dʼaktar-tI-ttAn</ta>
            <ta e="T119" id="Seg_2177" s="T118">ɨjɨt-Ar</ta>
            <ta e="T120" id="Seg_2178" s="T119">kim-LAː-Ar</ta>
            <ta e="T121" id="Seg_2179" s="T120">eː</ta>
            <ta e="T122" id="Seg_2180" s="T121">ü͡ögüleː-Ar</ta>
            <ta e="T123" id="Seg_2181" s="T122">onton</ta>
            <ta e="T124" id="Seg_2182" s="T123">ogus-I-BIT</ta>
            <ta e="T125" id="Seg_2183" s="T124">dʼaktar-tI-n</ta>
            <ta e="T128" id="Seg_2184" s="T127">dʼaktar</ta>
            <ta e="T129" id="Seg_2185" s="T128">ogo-LAːK</ta>
            <ta e="T130" id="Seg_2186" s="T129">ogo-LAːK</ta>
            <ta e="T131" id="Seg_2187" s="T130">iliː-tI-GAr</ta>
            <ta e="T132" id="Seg_2188" s="T131">ogo-LAːK</ta>
            <ta e="T133" id="Seg_2189" s="T132">hɨrɨt-Ar</ta>
            <ta e="T134" id="Seg_2190" s="T133">dʼaktar-nI</ta>
            <ta e="T135" id="Seg_2191" s="T134">kɨrbaː-Ar</ta>
            <ta e="T137" id="Seg_2192" s="T136">mʼilʼisʼija-LAr</ta>
            <ta e="T138" id="Seg_2193" s="T137">kel-BIT-LAr</ta>
            <ta e="T139" id="Seg_2194" s="T138">kim</ta>
            <ta e="T140" id="Seg_2195" s="T139">ere</ta>
            <ta e="T141" id="Seg_2196" s="T140">ɨgɨr-BIT</ta>
            <ta e="T142" id="Seg_2197" s="T141">onton</ta>
            <ta e="T143" id="Seg_2198" s="T142">mm</ta>
            <ta e="T144" id="Seg_2199" s="T143">mʼilʼisʼija-LAr</ta>
            <ta e="T145" id="Seg_2200" s="T144">ilt-BIT-LAr</ta>
            <ta e="T147" id="Seg_2201" s="T146">barɨ-LArI-n</ta>
            <ta e="T148" id="Seg_2202" s="T147">ilt-BIT-LAr</ta>
            <ta e="T149" id="Seg_2203" s="T148">onno</ta>
            <ta e="T152" id="Seg_2204" s="T150">dapros-LAː-BIT-LAr</ta>
            <ta e="T154" id="Seg_2205" s="T152">dapros-LAː-BIT-LAr</ta>
            <ta e="T156" id="Seg_2206" s="T154">ɨjɨt-AlAː-BIT-LAr</ta>
            <ta e="T158" id="Seg_2207" s="T156">kajdak</ta>
            <ta e="T160" id="Seg_2208" s="T158">tu͡ok</ta>
            <ta e="T162" id="Seg_2209" s="T160">bu͡ol-BIT-GIt=Ij</ta>
            <ta e="T164" id="Seg_2210" s="T162">kajdak</ta>
            <ta e="T166" id="Seg_2211" s="T164">tu͡ok</ta>
            <ta e="T167" id="Seg_2212" s="T166">bu͡ol-IAK.[tI]-n</ta>
            <ta e="T168" id="Seg_2213" s="T167">ölörös-I-BIT-GIt=Ij</ta>
            <ta e="T169" id="Seg_2214" s="T168">di͡e-A</ta>
            <ta e="T170" id="Seg_2215" s="T169">kepseː-Ar</ta>
            <ta e="T171" id="Seg_2216" s="T170">onno</ta>
            <ta e="T172" id="Seg_2217" s="T171">dʼaktar-tA</ta>
            <ta e="T174" id="Seg_2218" s="T173">ol</ta>
            <ta e="T175" id="Seg_2219" s="T174">kördük</ta>
            <ta e="T176" id="Seg_2220" s="T175">kɨrbaː-TI-tA</ta>
            <ta e="T177" id="Seg_2221" s="T176">bu</ta>
            <ta e="T178" id="Seg_2222" s="T177">kördük</ta>
            <ta e="T180" id="Seg_2223" s="T179">onton</ta>
            <ta e="T181" id="Seg_2224" s="T180">türme-GA</ta>
            <ta e="T182" id="Seg_2225" s="T181">olort-BIT-LAr</ta>
            <ta e="T183" id="Seg_2226" s="T182">eː</ta>
            <ta e="T185" id="Seg_2227" s="T183">türme</ta>
            <ta e="T186" id="Seg_2228" s="T185">kaːjɨː-GA</ta>
            <ta e="T188" id="Seg_2229" s="T186">olort-BIT-LAr</ta>
            <ta e="T189" id="Seg_2230" s="T188">aga</ta>
            <ta e="T191" id="Seg_2231" s="T190">kaːjɨː-GA</ta>
            <ta e="T192" id="Seg_2232" s="T191">olort-BIT-LAr</ta>
            <ta e="T194" id="Seg_2233" s="T193">onton</ta>
            <ta e="T195" id="Seg_2234" s="T194">bu</ta>
            <ta e="T197" id="Seg_2235" s="T196">öjdöː-A</ta>
            <ta e="T198" id="Seg_2236" s="T197">olor-Ar</ta>
            <ta e="T199" id="Seg_2237" s="T198">ogo-tI-n</ta>
            <ta e="T200" id="Seg_2238" s="T199">dʼaktar-tI-n</ta>
            <ta e="T201" id="Seg_2239" s="T200">öjdöː-A</ta>
            <ta e="T202" id="Seg_2240" s="T201">olor-Ar</ta>
            <ta e="T203" id="Seg_2241" s="T202">ol</ta>
            <ta e="T204" id="Seg_2242" s="T203">kördük</ta>
            <ta e="T206" id="Seg_2243" s="T205">mm</ta>
            <ta e="T207" id="Seg_2244" s="T206">tagɨs-Ar-tI-n</ta>
            <ta e="T208" id="Seg_2245" s="T207">türme-ttAn</ta>
            <ta e="T209" id="Seg_2246" s="T208">tagɨs-Ar-tI-n</ta>
            <ta e="T210" id="Seg_2247" s="T209">köhüt</ta>
            <ta e="T211" id="Seg_2248" s="T210">köhüt-An</ta>
            <ta e="T222" id="Seg_2249" s="T220">kim-LAː-A-BIn</ta>
            <ta e="T224" id="Seg_2250" s="T222">di͡e-Ar</ta>
            <ta e="T226" id="Seg_2251" s="T224">eː</ta>
            <ta e="T227" id="Seg_2252" s="T226">mm</ta>
            <ta e="T239" id="Seg_2253" s="T236">hu͡ok</ta>
            <ta e="T243" id="Seg_2254" s="T240">kim-LAː-A</ta>
            <ta e="T244" id="Seg_2255" s="T243">olor-Ar</ta>
            <ta e="T245" id="Seg_2256" s="T244">eː</ta>
            <ta e="T247" id="Seg_2257" s="T246">kim-LAː-A</ta>
            <ta e="T248" id="Seg_2258" s="T247">olor-Ar</ta>
            <ta e="T249" id="Seg_2259" s="T248">öjdöː-A</ta>
            <ta e="T250" id="Seg_2260" s="T249">olor-Ar</ta>
            <ta e="T251" id="Seg_2261" s="T250">dʼi͡e-LAːK-LAr-tI-n</ta>
            <ta e="T252" id="Seg_2262" s="T251">ol</ta>
            <ta e="T253" id="Seg_2263" s="T252">kördük</ta>
            <ta e="T254" id="Seg_2264" s="T253">tagɨs-TAK-BInA</ta>
            <ta e="T258" id="Seg_2265" s="T256">eː</ta>
            <ta e="T260" id="Seg_2266" s="T258">hanaː-A</ta>
            <ta e="T261" id="Seg_2267" s="T260">olor-Ar</ta>
            <ta e="T263" id="Seg_2268" s="T261">öjdöː-A</ta>
            <ta e="T264" id="Seg_2269" s="T263">olor-Ar</ta>
            <ta e="T265" id="Seg_2270" s="T264">hanaː-Ar</ta>
            <ta e="T267" id="Seg_2271" s="T265">onton</ta>
            <ta e="T272" id="Seg_2272" s="T269">aː</ta>
            <ta e="T277" id="Seg_2273" s="T276">eː</ta>
            <ta e="T278" id="Seg_2274" s="T277">ü͡ör-IAK-LArA</ta>
            <ta e="T279" id="Seg_2275" s="T278">min</ta>
            <ta e="T280" id="Seg_2276" s="T279">tagɨs-TAK-BInA</ta>
            <ta e="T281" id="Seg_2277" s="T280">aragiː</ta>
            <ta e="T282" id="Seg_2278" s="T281">is-IAK-m</ta>
            <ta e="T283" id="Seg_2279" s="T282">hu͡ok-tA</ta>
            <ta e="T284" id="Seg_2280" s="T283">di͡e-Ar</ta>
            <ta e="T285" id="Seg_2281" s="T284">bolʼše</ta>
            <ta e="T287" id="Seg_2282" s="T286">tagɨs-I-BIT</ta>
            <ta e="T288" id="Seg_2283" s="T287">onton</ta>
            <ta e="T289" id="Seg_2284" s="T288">türme-ttAn</ta>
            <ta e="T291" id="Seg_2285" s="T290">ü͡ör-Ar</ta>
            <ta e="T292" id="Seg_2286" s="T291">ü͡ör-Ar</ta>
            <ta e="T293" id="Seg_2287" s="T292">kün</ta>
            <ta e="T294" id="Seg_2288" s="T293">kün</ta>
            <ta e="T295" id="Seg_2289" s="T294">tɨk-Ar</ta>
            <ta e="T298" id="Seg_2290" s="T297">eː</ta>
            <ta e="T301" id="Seg_2291" s="T298">hürdeːk</ta>
            <ta e="T302" id="Seg_2292" s="T301">üčügej</ta>
            <ta e="T304" id="Seg_2293" s="T302">bagajɨ</ta>
            <ta e="T305" id="Seg_2294" s="T304">onton</ta>
            <ta e="T306" id="Seg_2295" s="T305">ü͡ör-A-ü͡ör-A</ta>
            <ta e="T307" id="Seg_2296" s="T306">dʼi͡e-tI-GAr</ta>
            <ta e="T308" id="Seg_2297" s="T307">kel-BIT</ta>
            <ta e="T310" id="Seg_2298" s="T309">ol-tI-LArA</ta>
            <ta e="T315" id="Seg_2299" s="T313">eː</ta>
            <ta e="T316" id="Seg_2300" s="T315">ol-tI-LArA</ta>
            <ta e="T317" id="Seg_2301" s="T316">bu͡olla</ta>
            <ta e="T318" id="Seg_2302" s="T317">du͡o</ta>
            <ta e="T319" id="Seg_2303" s="T318">kör-Ar-LAr</ta>
            <ta e="T320" id="Seg_2304" s="T319">kuttan-Ar-LAr</ta>
            <ta e="T321" id="Seg_2305" s="T320">bɨhɨːlaːk</ta>
            <ta e="T323" id="Seg_2306" s="T322">onton</ta>
            <ta e="T324" id="Seg_2307" s="T323">ogo-tI-n</ta>
            <ta e="T325" id="Seg_2308" s="T324">kɨtta</ta>
            <ta e="T326" id="Seg_2309" s="T325">hɨrɨt-A</ta>
            <ta e="T327" id="Seg_2310" s="T326">bar-BIT</ta>
            <ta e="T328" id="Seg_2311" s="T327">dogor-LAr-tI-n</ta>
            <ta e="T329" id="Seg_2312" s="T328">körüs-I-BIT</ta>
            <ta e="T330" id="Seg_2313" s="T329">kel</ta>
            <ta e="T331" id="Seg_2314" s="T330">aragiː-TA</ta>
            <ta e="T332" id="Seg_2315" s="T331">is</ta>
            <ta e="T333" id="Seg_2316" s="T332">bihigi-nI</ta>
            <ta e="T334" id="Seg_2317" s="T333">kɨtta</ta>
            <ta e="T335" id="Seg_2318" s="T334">hu͡ok</ta>
            <ta e="T336" id="Seg_2319" s="T335">hu͡ok</ta>
            <ta e="T337" id="Seg_2320" s="T336">is-BAT-BIn</ta>
            <ta e="T342" id="Seg_2321" s="T341">is-BAT-BIn</ta>
            <ta e="T343" id="Seg_2322" s="T342">bolʼše</ta>
            <ta e="T344" id="Seg_2323" s="T343">eː</ta>
            <ta e="T345" id="Seg_2324" s="T344">bolʼše</ta>
            <ta e="T346" id="Seg_2325" s="T345">bu͡o</ta>
            <ta e="T347" id="Seg_2326" s="T346">is-BAT-BIn</ta>
            <ta e="T349" id="Seg_2327" s="T348">is-IAK-m</ta>
            <ta e="T350" id="Seg_2328" s="T349">hu͡ok-tA</ta>
            <ta e="T351" id="Seg_2329" s="T350">aragiː-nI</ta>
            <ta e="T352" id="Seg_2330" s="T351">bɨrak-BIT-I-m</ta>
            <ta e="T353" id="Seg_2331" s="T352">di͡e-BIT</ta>
            <ta e="T354" id="Seg_2332" s="T353">karočʼe</ta>
            <ta e="T360" id="Seg_2333" s="T359">baːj-An-tot-An</ta>
            <ta e="T361" id="Seg_2334" s="T360">olor-BIT-LAr</ta>
            <ta e="T362" id="Seg_2335" s="T361">onton</ta>
            <ta e="T364" id="Seg_2336" s="T363">üčügej</ta>
            <ta e="T365" id="Seg_2337" s="T364">bagajɨ-LIk</ta>
         </annotation>
         <annotation name="ge" tierref="ge-UoPP">
            <ta e="T24" id="Seg_2338" s="T22">tell.[IMP.2SG]</ta>
            <ta e="T25" id="Seg_2339" s="T24">friend</ta>
            <ta e="T26" id="Seg_2340" s="T25">2SG.[NOM]</ta>
            <ta e="T34" id="Seg_2341" s="T32">eh</ta>
            <ta e="T37" id="Seg_2342" s="T34">at.all</ta>
            <ta e="T39" id="Seg_2343" s="T37">one</ta>
            <ta e="T42" id="Seg_2344" s="T39">one</ta>
            <ta e="T44" id="Seg_2345" s="T42">eh</ta>
            <ta e="T48" id="Seg_2346" s="T46">how</ta>
            <ta e="T50" id="Seg_2347" s="T48">be-PST1-3SG=Q</ta>
            <ta e="T54" id="Seg_2348" s="T53">one</ta>
            <ta e="T55" id="Seg_2349" s="T54">family-PL.[NOM]</ta>
            <ta e="T56" id="Seg_2350" s="T55">be-COND.[3SG]</ta>
            <ta e="T57" id="Seg_2351" s="T56">eh</ta>
            <ta e="T58" id="Seg_2352" s="T57">good.[NOM]</ta>
            <ta e="T59" id="Seg_2353" s="T58">very-ADVZ</ta>
            <ta e="T60" id="Seg_2354" s="T59">live-PTCP.PRS</ta>
            <ta e="T61" id="Seg_2355" s="T60">be-PST1-3PL</ta>
            <ta e="T62" id="Seg_2356" s="T61">be-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_2357" s="T62">work-PRS-3PL</ta>
            <ta e="T64" id="Seg_2358" s="T63">work-PTCP.PRS</ta>
            <ta e="T65" id="Seg_2359" s="T64">be-PST1-3PL</ta>
            <ta e="T66" id="Seg_2360" s="T65">then</ta>
            <ta e="T67" id="Seg_2361" s="T66">EMPH</ta>
            <ta e="T68" id="Seg_2362" s="T67">friend-PL-3SG-ACC</ta>
            <ta e="T69" id="Seg_2363" s="T68">meet-CVB.SEQ</ta>
            <ta e="T70" id="Seg_2364" s="T69">spirit.[NOM]</ta>
            <ta e="T71" id="Seg_2365" s="T70">drink-CVB.SIM</ta>
            <ta e="T72" id="Seg_2366" s="T71">go-PST2.[3SG]</ta>
            <ta e="T74" id="Seg_2367" s="T73">there</ta>
            <ta e="T75" id="Seg_2368" s="T74">tell-PRS-3PL</ta>
            <ta e="T76" id="Seg_2369" s="T75">2SG.[NOM]</ta>
            <ta e="T77" id="Seg_2370" s="T76">woman-EP-2SG.[NOM]</ta>
            <ta e="T78" id="Seg_2371" s="T77">that.[NOM]</ta>
            <ta e="T79" id="Seg_2372" s="T78">similar</ta>
            <ta e="T81" id="Seg_2373" s="T80">this.[NOM]</ta>
            <ta e="T82" id="Seg_2374" s="T81">similar</ta>
            <ta e="T83" id="Seg_2375" s="T82">that</ta>
            <ta e="T84" id="Seg_2376" s="T83">human.being-ACC</ta>
            <ta e="T85" id="Seg_2377" s="T84">with</ta>
            <ta e="T86" id="Seg_2378" s="T85">chat-PST2-3SG</ta>
            <ta e="T87" id="Seg_2379" s="T86">eh</ta>
            <ta e="T88" id="Seg_2380" s="T87">who-VBZ-PTCP.PRS</ta>
            <ta e="T89" id="Seg_2381" s="T88">be-PST1-3SG</ta>
            <ta e="T90" id="Seg_2382" s="T89">how</ta>
            <ta e="T92" id="Seg_2383" s="T90">say-HAB-3PL</ta>
            <ta e="T106" id="Seg_2384" s="T105">eh</ta>
            <ta e="T107" id="Seg_2385" s="T106">woman-EP-2SG.[NOM]</ta>
            <ta e="T108" id="Seg_2386" s="T107">that.[NOM]</ta>
            <ta e="T109" id="Seg_2387" s="T108">similar</ta>
            <ta e="T110" id="Seg_2388" s="T109">meet-EP-ITER-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T112" id="Seg_2389" s="T111">this.[NOM]</ta>
            <ta e="T113" id="Seg_2390" s="T112">similar</ta>
            <ta e="T114" id="Seg_2391" s="T113">say-PRS-3PL</ta>
            <ta e="T116" id="Seg_2392" s="T115">then</ta>
            <ta e="T117" id="Seg_2393" s="T116">go-CVB.SEQ</ta>
            <ta e="T118" id="Seg_2394" s="T117">woman-3SG-ABL</ta>
            <ta e="T119" id="Seg_2395" s="T118">ask-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_2396" s="T119">who-VBZ-PRS.[3SG]</ta>
            <ta e="T121" id="Seg_2397" s="T120">eh</ta>
            <ta e="T122" id="Seg_2398" s="T121">shout-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_2399" s="T122">then</ta>
            <ta e="T124" id="Seg_2400" s="T123">beat-EP-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_2401" s="T124">woman-3SG-ACC</ta>
            <ta e="T128" id="Seg_2402" s="T127">woman.[NOM]</ta>
            <ta e="T129" id="Seg_2403" s="T128">child-PROPR.[NOM]</ta>
            <ta e="T130" id="Seg_2404" s="T129">child-PROPR.[NOM]</ta>
            <ta e="T131" id="Seg_2405" s="T130">arm-3SG-DAT/LOC</ta>
            <ta e="T132" id="Seg_2406" s="T131">child-PROPR.[NOM]</ta>
            <ta e="T133" id="Seg_2407" s="T132">go-PTCP.PRS</ta>
            <ta e="T134" id="Seg_2408" s="T133">woman-ACC</ta>
            <ta e="T135" id="Seg_2409" s="T134">thrash-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_2410" s="T136">police-PL.[NOM]</ta>
            <ta e="T138" id="Seg_2411" s="T137">come-PST2-3PL</ta>
            <ta e="T139" id="Seg_2412" s="T138">who.[NOM]</ta>
            <ta e="T140" id="Seg_2413" s="T139">INDEF</ta>
            <ta e="T141" id="Seg_2414" s="T140">call-PST2.[3SG]</ta>
            <ta e="T142" id="Seg_2415" s="T141">then</ta>
            <ta e="T143" id="Seg_2416" s="T142">mm</ta>
            <ta e="T144" id="Seg_2417" s="T143">police-PL.[NOM]</ta>
            <ta e="T145" id="Seg_2418" s="T144">bring-PST2-3PL</ta>
            <ta e="T147" id="Seg_2419" s="T146">every-3PL-ACC</ta>
            <ta e="T148" id="Seg_2420" s="T147">bring-PST2-3PL</ta>
            <ta e="T149" id="Seg_2421" s="T148">thither</ta>
            <ta e="T152" id="Seg_2422" s="T150">interrogation-VBZ-PST2-3PL</ta>
            <ta e="T154" id="Seg_2423" s="T152">interrogation-VBZ-PST2-3PL</ta>
            <ta e="T156" id="Seg_2424" s="T154">ask-FREQ-PST2-3PL</ta>
            <ta e="T158" id="Seg_2425" s="T156">how</ta>
            <ta e="T160" id="Seg_2426" s="T158">what.[NOM]</ta>
            <ta e="T162" id="Seg_2427" s="T160">be-PST2-2PL=Q</ta>
            <ta e="T164" id="Seg_2428" s="T162">how</ta>
            <ta e="T166" id="Seg_2429" s="T164">what.[NOM]</ta>
            <ta e="T167" id="Seg_2430" s="T166">be-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T168" id="Seg_2431" s="T167">fight-EP-PST2-2PL=Q</ta>
            <ta e="T169" id="Seg_2432" s="T168">say-CVB.SIM</ta>
            <ta e="T170" id="Seg_2433" s="T169">tell-PRS.[3SG]</ta>
            <ta e="T171" id="Seg_2434" s="T170">there</ta>
            <ta e="T172" id="Seg_2435" s="T171">woman-3SG.[NOM]</ta>
            <ta e="T174" id="Seg_2436" s="T173">that.[NOM]</ta>
            <ta e="T175" id="Seg_2437" s="T174">similar</ta>
            <ta e="T176" id="Seg_2438" s="T175">thrash-PST1-3SG</ta>
            <ta e="T177" id="Seg_2439" s="T176">this.[NOM]</ta>
            <ta e="T178" id="Seg_2440" s="T177">similar</ta>
            <ta e="T180" id="Seg_2441" s="T179">then</ta>
            <ta e="T181" id="Seg_2442" s="T180">prison-DAT/LOC</ta>
            <ta e="T182" id="Seg_2443" s="T181">seat-PST2-3PL</ta>
            <ta e="T183" id="Seg_2444" s="T182">AFFIRM</ta>
            <ta e="T185" id="Seg_2445" s="T183">prison</ta>
            <ta e="T186" id="Seg_2446" s="T185">prison-DAT/LOC</ta>
            <ta e="T188" id="Seg_2447" s="T186">seat-PST2-3PL</ta>
            <ta e="T189" id="Seg_2448" s="T188">mhm</ta>
            <ta e="T191" id="Seg_2449" s="T190">prison-DAT/LOC</ta>
            <ta e="T192" id="Seg_2450" s="T191">seat-PST2-3PL</ta>
            <ta e="T194" id="Seg_2451" s="T193">then</ta>
            <ta e="T195" id="Seg_2452" s="T194">this</ta>
            <ta e="T197" id="Seg_2453" s="T196">remember-CVB.SIM</ta>
            <ta e="T198" id="Seg_2454" s="T197">sit-PRS.[3SG]</ta>
            <ta e="T199" id="Seg_2455" s="T198">child-3SG-ACC</ta>
            <ta e="T200" id="Seg_2456" s="T199">woman-3SG-ACC</ta>
            <ta e="T201" id="Seg_2457" s="T200">remember-CVB.SIM</ta>
            <ta e="T202" id="Seg_2458" s="T201">sit-PRS.[3SG]</ta>
            <ta e="T203" id="Seg_2459" s="T202">that.[NOM]</ta>
            <ta e="T204" id="Seg_2460" s="T203">similar</ta>
            <ta e="T206" id="Seg_2461" s="T205">mm</ta>
            <ta e="T207" id="Seg_2462" s="T206">go.out-PTCP.PRS-3SG-ACC</ta>
            <ta e="T208" id="Seg_2463" s="T207">prison-ABL</ta>
            <ta e="T209" id="Seg_2464" s="T208">go.out-PTCP.PRS-3SG-ACC</ta>
            <ta e="T210" id="Seg_2465" s="T209">wait</ta>
            <ta e="T211" id="Seg_2466" s="T210">wait-CVB.SEQ</ta>
            <ta e="T222" id="Seg_2467" s="T220">who-VBZ-PRS-1SG</ta>
            <ta e="T224" id="Seg_2468" s="T222">say-PRS.[3SG]</ta>
            <ta e="T226" id="Seg_2469" s="T224">eh</ta>
            <ta e="T227" id="Seg_2470" s="T226">mm</ta>
            <ta e="T239" id="Seg_2471" s="T236">no</ta>
            <ta e="T243" id="Seg_2472" s="T240">who-VBZ-CVB.SIM</ta>
            <ta e="T244" id="Seg_2473" s="T243">sit-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_2474" s="T244">eh</ta>
            <ta e="T247" id="Seg_2475" s="T246">who-VBZ-CVB.SIM</ta>
            <ta e="T248" id="Seg_2476" s="T247">sit-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_2477" s="T248">remember-CVB.SIM</ta>
            <ta e="T250" id="Seg_2478" s="T249">sit-PRS.[3SG]</ta>
            <ta e="T251" id="Seg_2479" s="T250">house-PROPR-PL-3SG-ACC</ta>
            <ta e="T252" id="Seg_2480" s="T251">that.[NOM]</ta>
            <ta e="T253" id="Seg_2481" s="T252">similar</ta>
            <ta e="T254" id="Seg_2482" s="T253">go.out-TEMP-1SG</ta>
            <ta e="T258" id="Seg_2483" s="T256">AFFIRM</ta>
            <ta e="T260" id="Seg_2484" s="T258">wish-CVB.SIM</ta>
            <ta e="T261" id="Seg_2485" s="T260">sit-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_2486" s="T261">remember-CVB.SIM</ta>
            <ta e="T264" id="Seg_2487" s="T263">sit-PRS.[3SG]</ta>
            <ta e="T265" id="Seg_2488" s="T264">wish-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_2489" s="T265">then</ta>
            <ta e="T272" id="Seg_2490" s="T269">ah</ta>
            <ta e="T277" id="Seg_2491" s="T276">eh</ta>
            <ta e="T278" id="Seg_2492" s="T277">be.happy-FUT-3PL</ta>
            <ta e="T279" id="Seg_2493" s="T278">1SG.[NOM]</ta>
            <ta e="T280" id="Seg_2494" s="T279">go.out-TEMP-1SG</ta>
            <ta e="T281" id="Seg_2495" s="T280">spirit.[NOM]</ta>
            <ta e="T282" id="Seg_2496" s="T281">drink-FUT-1SG</ta>
            <ta e="T283" id="Seg_2497" s="T282">NEG-3SG</ta>
            <ta e="T284" id="Seg_2498" s="T283">think-PRS.[3SG]</ta>
            <ta e="T285" id="Seg_2499" s="T284">more</ta>
            <ta e="T287" id="Seg_2500" s="T286">go.out-EP-PST2.[3SG]</ta>
            <ta e="T288" id="Seg_2501" s="T287">then</ta>
            <ta e="T289" id="Seg_2502" s="T288">prison-ABL</ta>
            <ta e="T291" id="Seg_2503" s="T290">be.happy-PRS.[3SG]</ta>
            <ta e="T292" id="Seg_2504" s="T291">be.happy-PRS.[3SG]</ta>
            <ta e="T293" id="Seg_2505" s="T292">sun.[NOM]</ta>
            <ta e="T294" id="Seg_2506" s="T293">sun.[NOM]</ta>
            <ta e="T295" id="Seg_2507" s="T294">shine-PRS.[3SG]</ta>
            <ta e="T298" id="Seg_2508" s="T297">AFFIRM</ta>
            <ta e="T301" id="Seg_2509" s="T298">very</ta>
            <ta e="T302" id="Seg_2510" s="T301">good.[NOM]</ta>
            <ta e="T304" id="Seg_2511" s="T302">very</ta>
            <ta e="T305" id="Seg_2512" s="T304">then</ta>
            <ta e="T306" id="Seg_2513" s="T305">be.happy-CVB.SIM-be.happy-CVB.SIM</ta>
            <ta e="T307" id="Seg_2514" s="T306">house-3SG-DAT/LOC</ta>
            <ta e="T308" id="Seg_2515" s="T307">come-PST2.[3SG]</ta>
            <ta e="T310" id="Seg_2516" s="T309">that-3SG-3PL.[NOM]</ta>
            <ta e="T315" id="Seg_2517" s="T313">EMPH</ta>
            <ta e="T316" id="Seg_2518" s="T315">that-3SG-3PL.[NOM]</ta>
            <ta e="T317" id="Seg_2519" s="T316">MOD</ta>
            <ta e="T318" id="Seg_2520" s="T317">Q</ta>
            <ta e="T319" id="Seg_2521" s="T318">see-PRS-3PL</ta>
            <ta e="T320" id="Seg_2522" s="T319">be.afraid-PRS-3PL</ta>
            <ta e="T321" id="Seg_2523" s="T320">apparently</ta>
            <ta e="T323" id="Seg_2524" s="T322">then</ta>
            <ta e="T324" id="Seg_2525" s="T323">child-3SG-ACC</ta>
            <ta e="T325" id="Seg_2526" s="T324">with</ta>
            <ta e="T326" id="Seg_2527" s="T325">go-CVB.SIM</ta>
            <ta e="T327" id="Seg_2528" s="T326">go-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_2529" s="T327">friend-PL-3SG-ACC</ta>
            <ta e="T329" id="Seg_2530" s="T328">meet-EP-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_2531" s="T329">come.[IMP.2SG]</ta>
            <ta e="T331" id="Seg_2532" s="T330">spirit-PART</ta>
            <ta e="T332" id="Seg_2533" s="T331">drink.[IMP.2SG]</ta>
            <ta e="T333" id="Seg_2534" s="T332">1PL-ACC</ta>
            <ta e="T334" id="Seg_2535" s="T333">with</ta>
            <ta e="T335" id="Seg_2536" s="T334">no</ta>
            <ta e="T336" id="Seg_2537" s="T335">no</ta>
            <ta e="T337" id="Seg_2538" s="T336">drink-NEG-1SG</ta>
            <ta e="T342" id="Seg_2539" s="T341">drink-NEG-1SG</ta>
            <ta e="T343" id="Seg_2540" s="T342">more</ta>
            <ta e="T344" id="Seg_2541" s="T343">eh</ta>
            <ta e="T345" id="Seg_2542" s="T344">more</ta>
            <ta e="T346" id="Seg_2543" s="T345">EMPH</ta>
            <ta e="T347" id="Seg_2544" s="T346">drink-NEG-1SG</ta>
            <ta e="T349" id="Seg_2545" s="T348">drink-FUT-1SG</ta>
            <ta e="T350" id="Seg_2546" s="T349">NEG-3SG</ta>
            <ta e="T351" id="Seg_2547" s="T350">spirit-ACC</ta>
            <ta e="T352" id="Seg_2548" s="T351">throw-PST2-EP-1SG</ta>
            <ta e="T353" id="Seg_2549" s="T352">say-PST2.[3SG]</ta>
            <ta e="T354" id="Seg_2550" s="T353">shortly</ta>
            <ta e="T360" id="Seg_2551" s="T359">be.rich-CVB.SEQ-eat.ones.fill-CVB.SEQ</ta>
            <ta e="T361" id="Seg_2552" s="T360">live-PST2-3PL</ta>
            <ta e="T362" id="Seg_2553" s="T361">then</ta>
            <ta e="T364" id="Seg_2554" s="T363">good.[NOM]</ta>
            <ta e="T365" id="Seg_2555" s="T364">very-ADVZ</ta>
         </annotation>
         <annotation name="gg" tierref="gg-UoPP">
            <ta e="T24" id="Seg_2556" s="T22">erzählen.[IMP.2SG]</ta>
            <ta e="T25" id="Seg_2557" s="T24">Freund</ta>
            <ta e="T26" id="Seg_2558" s="T25">2SG.[NOM]</ta>
            <ta e="T34" id="Seg_2559" s="T32">äh</ta>
            <ta e="T37" id="Seg_2560" s="T34">überhaupt</ta>
            <ta e="T39" id="Seg_2561" s="T37">eins</ta>
            <ta e="T42" id="Seg_2562" s="T39">eins</ta>
            <ta e="T44" id="Seg_2563" s="T42">äh</ta>
            <ta e="T48" id="Seg_2564" s="T46">wie</ta>
            <ta e="T50" id="Seg_2565" s="T48">sein-PST1-3SG=Q</ta>
            <ta e="T54" id="Seg_2566" s="T53">eins</ta>
            <ta e="T55" id="Seg_2567" s="T54">Familie-PL.[NOM]</ta>
            <ta e="T56" id="Seg_2568" s="T55">sein-COND.[3SG]</ta>
            <ta e="T57" id="Seg_2569" s="T56">äh</ta>
            <ta e="T58" id="Seg_2570" s="T57">gut.[NOM]</ta>
            <ta e="T59" id="Seg_2571" s="T58">sehr-ADVZ</ta>
            <ta e="T60" id="Seg_2572" s="T59">leben-PTCP.PRS</ta>
            <ta e="T61" id="Seg_2573" s="T60">sein-PST1-3PL</ta>
            <ta e="T62" id="Seg_2574" s="T61">sein-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_2575" s="T62">arbeiten-PRS-3PL</ta>
            <ta e="T64" id="Seg_2576" s="T63">arbeiten-PTCP.PRS</ta>
            <ta e="T65" id="Seg_2577" s="T64">sein-PST1-3PL</ta>
            <ta e="T66" id="Seg_2578" s="T65">dann</ta>
            <ta e="T67" id="Seg_2579" s="T66">EMPH</ta>
            <ta e="T68" id="Seg_2580" s="T67">Freund-PL-3SG-ACC</ta>
            <ta e="T69" id="Seg_2581" s="T68">treffen-CVB.SEQ</ta>
            <ta e="T70" id="Seg_2582" s="T69">Schnaps.[NOM]</ta>
            <ta e="T71" id="Seg_2583" s="T70">trinken-CVB.SIM</ta>
            <ta e="T72" id="Seg_2584" s="T71">gehen-PST2.[3SG]</ta>
            <ta e="T74" id="Seg_2585" s="T73">dort</ta>
            <ta e="T75" id="Seg_2586" s="T74">erzählen-PRS-3PL</ta>
            <ta e="T76" id="Seg_2587" s="T75">2SG.[NOM]</ta>
            <ta e="T77" id="Seg_2588" s="T76">Frau-EP-2SG.[NOM]</ta>
            <ta e="T78" id="Seg_2589" s="T77">jenes.[NOM]</ta>
            <ta e="T79" id="Seg_2590" s="T78">ähnlich</ta>
            <ta e="T81" id="Seg_2591" s="T80">dieses.[NOM]</ta>
            <ta e="T82" id="Seg_2592" s="T81">ähnlich</ta>
            <ta e="T83" id="Seg_2593" s="T82">jenes</ta>
            <ta e="T84" id="Seg_2594" s="T83">Mensch-ACC</ta>
            <ta e="T85" id="Seg_2595" s="T84">mit</ta>
            <ta e="T86" id="Seg_2596" s="T85">sich.unterhalten-PST2-3SG</ta>
            <ta e="T87" id="Seg_2597" s="T86">äh</ta>
            <ta e="T88" id="Seg_2598" s="T87">wer-VBZ-PTCP.PRS</ta>
            <ta e="T89" id="Seg_2599" s="T88">sein-PST1-3SG</ta>
            <ta e="T90" id="Seg_2600" s="T89">wie</ta>
            <ta e="T92" id="Seg_2601" s="T90">sagen-HAB-3PL</ta>
            <ta e="T106" id="Seg_2602" s="T105">äh</ta>
            <ta e="T107" id="Seg_2603" s="T106">Frau-EP-2SG.[NOM]</ta>
            <ta e="T108" id="Seg_2604" s="T107">jenes.[NOM]</ta>
            <ta e="T109" id="Seg_2605" s="T108">ähnlich</ta>
            <ta e="T110" id="Seg_2606" s="T109">treffen-EP-ITER-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T112" id="Seg_2607" s="T111">dieses.[NOM]</ta>
            <ta e="T113" id="Seg_2608" s="T112">ähnlich</ta>
            <ta e="T114" id="Seg_2609" s="T113">sagen-PRS-3PL</ta>
            <ta e="T116" id="Seg_2610" s="T115">dann</ta>
            <ta e="T117" id="Seg_2611" s="T116">gehen-CVB.SEQ</ta>
            <ta e="T118" id="Seg_2612" s="T117">Frau-3SG-ABL</ta>
            <ta e="T119" id="Seg_2613" s="T118">fragen-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_2614" s="T119">wer-VBZ-PRS.[3SG]</ta>
            <ta e="T121" id="Seg_2615" s="T120">äh</ta>
            <ta e="T122" id="Seg_2616" s="T121">schreien-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_2617" s="T122">dann</ta>
            <ta e="T124" id="Seg_2618" s="T123">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_2619" s="T124">Frau-3SG-ACC</ta>
            <ta e="T128" id="Seg_2620" s="T127">Frau.[NOM]</ta>
            <ta e="T129" id="Seg_2621" s="T128">Kind-PROPR.[NOM]</ta>
            <ta e="T130" id="Seg_2622" s="T129">Kind-PROPR.[NOM]</ta>
            <ta e="T131" id="Seg_2623" s="T130">Arm-3SG-DAT/LOC</ta>
            <ta e="T132" id="Seg_2624" s="T131">Kind-PROPR.[NOM]</ta>
            <ta e="T133" id="Seg_2625" s="T132">gehen-PTCP.PRS</ta>
            <ta e="T134" id="Seg_2626" s="T133">Frau-ACC</ta>
            <ta e="T135" id="Seg_2627" s="T134">prügeln-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_2628" s="T136">Polizei-PL.[NOM]</ta>
            <ta e="T138" id="Seg_2629" s="T137">kommen-PST2-3PL</ta>
            <ta e="T139" id="Seg_2630" s="T138">wer.[NOM]</ta>
            <ta e="T140" id="Seg_2631" s="T139">INDEF</ta>
            <ta e="T141" id="Seg_2632" s="T140">rufen-PST2.[3SG]</ta>
            <ta e="T142" id="Seg_2633" s="T141">dann</ta>
            <ta e="T143" id="Seg_2634" s="T142">mm</ta>
            <ta e="T144" id="Seg_2635" s="T143">Polizei-PL.[NOM]</ta>
            <ta e="T145" id="Seg_2636" s="T144">bringen-PST2-3PL</ta>
            <ta e="T147" id="Seg_2637" s="T146">jeder-3PL-ACC</ta>
            <ta e="T148" id="Seg_2638" s="T147">bringen-PST2-3PL</ta>
            <ta e="T149" id="Seg_2639" s="T148">dorthin</ta>
            <ta e="T152" id="Seg_2640" s="T150">Verhör-VBZ-PST2-3PL</ta>
            <ta e="T154" id="Seg_2641" s="T152">Verhör-VBZ-PST2-3PL</ta>
            <ta e="T156" id="Seg_2642" s="T154">fragen-FREQ-PST2-3PL</ta>
            <ta e="T158" id="Seg_2643" s="T156">wie</ta>
            <ta e="T160" id="Seg_2644" s="T158">was.[NOM]</ta>
            <ta e="T162" id="Seg_2645" s="T160">sein-PST2-2PL=Q</ta>
            <ta e="T164" id="Seg_2646" s="T162">wie</ta>
            <ta e="T166" id="Seg_2647" s="T164">was.[NOM]</ta>
            <ta e="T167" id="Seg_2648" s="T166">sein-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T168" id="Seg_2649" s="T167">kämpfen-EP-PST2-2PL=Q</ta>
            <ta e="T169" id="Seg_2650" s="T168">sagen-CVB.SIM</ta>
            <ta e="T170" id="Seg_2651" s="T169">erzählen-PRS.[3SG]</ta>
            <ta e="T171" id="Seg_2652" s="T170">dort</ta>
            <ta e="T172" id="Seg_2653" s="T171">Frau-3SG.[NOM]</ta>
            <ta e="T174" id="Seg_2654" s="T173">jenes.[NOM]</ta>
            <ta e="T175" id="Seg_2655" s="T174">ähnlich</ta>
            <ta e="T176" id="Seg_2656" s="T175">prügeln-PST1-3SG</ta>
            <ta e="T177" id="Seg_2657" s="T176">dieses.[NOM]</ta>
            <ta e="T178" id="Seg_2658" s="T177">ähnlich</ta>
            <ta e="T180" id="Seg_2659" s="T179">dann</ta>
            <ta e="T181" id="Seg_2660" s="T180">Gefängnis-DAT/LOC</ta>
            <ta e="T182" id="Seg_2661" s="T181">setzen-PST2-3PL</ta>
            <ta e="T183" id="Seg_2662" s="T182">AFFIRM</ta>
            <ta e="T185" id="Seg_2663" s="T183">Gefängnis</ta>
            <ta e="T186" id="Seg_2664" s="T185">Gefängnis-DAT/LOC</ta>
            <ta e="T188" id="Seg_2665" s="T186">setzen-PST2-3PL</ta>
            <ta e="T189" id="Seg_2666" s="T188">aha</ta>
            <ta e="T191" id="Seg_2667" s="T190">Gefängnis-DAT/LOC</ta>
            <ta e="T192" id="Seg_2668" s="T191">setzen-PST2-3PL</ta>
            <ta e="T194" id="Seg_2669" s="T193">dann</ta>
            <ta e="T195" id="Seg_2670" s="T194">dieses</ta>
            <ta e="T197" id="Seg_2671" s="T196">erinnern-CVB.SIM</ta>
            <ta e="T198" id="Seg_2672" s="T197">sitzen-PRS.[3SG]</ta>
            <ta e="T199" id="Seg_2673" s="T198">Kind-3SG-ACC</ta>
            <ta e="T200" id="Seg_2674" s="T199">Frau-3SG-ACC</ta>
            <ta e="T201" id="Seg_2675" s="T200">erinnern-CVB.SIM</ta>
            <ta e="T202" id="Seg_2676" s="T201">sitzen-PRS.[3SG]</ta>
            <ta e="T203" id="Seg_2677" s="T202">jenes.[NOM]</ta>
            <ta e="T204" id="Seg_2678" s="T203">ähnlich</ta>
            <ta e="T206" id="Seg_2679" s="T205">mm</ta>
            <ta e="T207" id="Seg_2680" s="T206">hinausgehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T208" id="Seg_2681" s="T207">Gefängnis-ABL</ta>
            <ta e="T209" id="Seg_2682" s="T208">hinausgehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T210" id="Seg_2683" s="T209">warten</ta>
            <ta e="T211" id="Seg_2684" s="T210">warten-CVB.SEQ</ta>
            <ta e="T222" id="Seg_2685" s="T220">wer-VBZ-PRS-1SG</ta>
            <ta e="T224" id="Seg_2686" s="T222">sagen-PRS.[3SG]</ta>
            <ta e="T226" id="Seg_2687" s="T224">äh</ta>
            <ta e="T227" id="Seg_2688" s="T226">mm</ta>
            <ta e="T239" id="Seg_2689" s="T236">nein</ta>
            <ta e="T243" id="Seg_2690" s="T240">wer-VBZ-CVB.SIM</ta>
            <ta e="T244" id="Seg_2691" s="T243">sitzen-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_2692" s="T244">äh</ta>
            <ta e="T247" id="Seg_2693" s="T246">wer-VBZ-CVB.SIM</ta>
            <ta e="T248" id="Seg_2694" s="T247">sitzen-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_2695" s="T248">erinnern-CVB.SIM</ta>
            <ta e="T250" id="Seg_2696" s="T249">sitzen-PRS.[3SG]</ta>
            <ta e="T251" id="Seg_2697" s="T250">Haus-PROPR-PL-3SG-ACC</ta>
            <ta e="T252" id="Seg_2698" s="T251">jenes.[NOM]</ta>
            <ta e="T253" id="Seg_2699" s="T252">ähnlich</ta>
            <ta e="T254" id="Seg_2700" s="T253">hinausgehen-TEMP-1SG</ta>
            <ta e="T258" id="Seg_2701" s="T256">AFFIRM</ta>
            <ta e="T260" id="Seg_2702" s="T258">wünschen-CVB.SIM</ta>
            <ta e="T261" id="Seg_2703" s="T260">sitzen-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_2704" s="T261">erinnern-CVB.SIM</ta>
            <ta e="T264" id="Seg_2705" s="T263">sitzen-PRS.[3SG]</ta>
            <ta e="T265" id="Seg_2706" s="T264">wünschen-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_2707" s="T265">dann</ta>
            <ta e="T272" id="Seg_2708" s="T269">ah</ta>
            <ta e="T277" id="Seg_2709" s="T276">äh</ta>
            <ta e="T278" id="Seg_2710" s="T277">sich.freuen-FUT-3PL</ta>
            <ta e="T279" id="Seg_2711" s="T278">1SG.[NOM]</ta>
            <ta e="T280" id="Seg_2712" s="T279">hinausgehen-TEMP-1SG</ta>
            <ta e="T281" id="Seg_2713" s="T280">Schnaps.[NOM]</ta>
            <ta e="T282" id="Seg_2714" s="T281">trinken-FUT-1SG</ta>
            <ta e="T283" id="Seg_2715" s="T282">NEG-3SG</ta>
            <ta e="T284" id="Seg_2716" s="T283">denken-PRS.[3SG]</ta>
            <ta e="T285" id="Seg_2717" s="T284">mehr</ta>
            <ta e="T287" id="Seg_2718" s="T286">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T288" id="Seg_2719" s="T287">dann</ta>
            <ta e="T289" id="Seg_2720" s="T288">Gefängnis-ABL</ta>
            <ta e="T291" id="Seg_2721" s="T290">sich.freuen-PRS.[3SG]</ta>
            <ta e="T292" id="Seg_2722" s="T291">sich.freuen-PRS.[3SG]</ta>
            <ta e="T293" id="Seg_2723" s="T292">Sonne.[NOM]</ta>
            <ta e="T294" id="Seg_2724" s="T293">Sonne.[NOM]</ta>
            <ta e="T295" id="Seg_2725" s="T294">scheinen-PRS.[3SG]</ta>
            <ta e="T298" id="Seg_2726" s="T297">AFFIRM</ta>
            <ta e="T301" id="Seg_2727" s="T298">sehr</ta>
            <ta e="T302" id="Seg_2728" s="T301">gut.[NOM]</ta>
            <ta e="T304" id="Seg_2729" s="T302">sehr</ta>
            <ta e="T305" id="Seg_2730" s="T304">dann</ta>
            <ta e="T306" id="Seg_2731" s="T305">sich.freuen-CVB.SIM-sich.freuen-CVB.SIM</ta>
            <ta e="T307" id="Seg_2732" s="T306">Haus-3SG-DAT/LOC</ta>
            <ta e="T308" id="Seg_2733" s="T307">kommen-PST2.[3SG]</ta>
            <ta e="T310" id="Seg_2734" s="T309">jenes-3SG-3PL.[NOM]</ta>
            <ta e="T315" id="Seg_2735" s="T313">EMPH</ta>
            <ta e="T316" id="Seg_2736" s="T315">jenes-3SG-3PL.[NOM]</ta>
            <ta e="T317" id="Seg_2737" s="T316">MOD</ta>
            <ta e="T318" id="Seg_2738" s="T317">Q</ta>
            <ta e="T319" id="Seg_2739" s="T318">sehen-PRS-3PL</ta>
            <ta e="T320" id="Seg_2740" s="T319">Angst.haben-PRS-3PL</ta>
            <ta e="T321" id="Seg_2741" s="T320">offenbar</ta>
            <ta e="T323" id="Seg_2742" s="T322">dann</ta>
            <ta e="T324" id="Seg_2743" s="T323">Kind-3SG-ACC</ta>
            <ta e="T325" id="Seg_2744" s="T324">mit</ta>
            <ta e="T326" id="Seg_2745" s="T325">gehen-CVB.SIM</ta>
            <ta e="T327" id="Seg_2746" s="T326">gehen-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_2747" s="T327">Freund-PL-3SG-ACC</ta>
            <ta e="T329" id="Seg_2748" s="T328">treffen-EP-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_2749" s="T329">kommen.[IMP.2SG]</ta>
            <ta e="T331" id="Seg_2750" s="T330">Schnaps-PART</ta>
            <ta e="T332" id="Seg_2751" s="T331">trinken.[IMP.2SG]</ta>
            <ta e="T333" id="Seg_2752" s="T332">1PL-ACC</ta>
            <ta e="T334" id="Seg_2753" s="T333">mit</ta>
            <ta e="T335" id="Seg_2754" s="T334">nein</ta>
            <ta e="T336" id="Seg_2755" s="T335">nein</ta>
            <ta e="T337" id="Seg_2756" s="T336">trinken-NEG-1SG</ta>
            <ta e="T342" id="Seg_2757" s="T341">trinken-NEG-1SG</ta>
            <ta e="T343" id="Seg_2758" s="T342">mehr</ta>
            <ta e="T344" id="Seg_2759" s="T343">äh</ta>
            <ta e="T345" id="Seg_2760" s="T344">mehr</ta>
            <ta e="T346" id="Seg_2761" s="T345">EMPH</ta>
            <ta e="T347" id="Seg_2762" s="T346">trinken-NEG-1SG</ta>
            <ta e="T349" id="Seg_2763" s="T348">trinken-FUT-1SG</ta>
            <ta e="T350" id="Seg_2764" s="T349">NEG-3SG</ta>
            <ta e="T351" id="Seg_2765" s="T350">Schnaps-ACC</ta>
            <ta e="T352" id="Seg_2766" s="T351">werfen-PST2-EP-1SG</ta>
            <ta e="T353" id="Seg_2767" s="T352">sagen-PST2.[3SG]</ta>
            <ta e="T354" id="Seg_2768" s="T353">kurz</ta>
            <ta e="T360" id="Seg_2769" s="T359">reich.sein-CVB.SEQ-sich.satt.essen-CVB.SEQ</ta>
            <ta e="T361" id="Seg_2770" s="T360">leben-PST2-3PL</ta>
            <ta e="T362" id="Seg_2771" s="T361">dann</ta>
            <ta e="T364" id="Seg_2772" s="T363">gut.[NOM]</ta>
            <ta e="T365" id="Seg_2773" s="T364">sehr-ADVZ</ta>
         </annotation>
         <annotation name="gr" tierref="gr-UoPP">
            <ta e="T24" id="Seg_2774" s="T22">рассказывать.[IMP.2SG]</ta>
            <ta e="T25" id="Seg_2775" s="T24">друг</ta>
            <ta e="T26" id="Seg_2776" s="T25">2SG.[NOM]</ta>
            <ta e="T34" id="Seg_2777" s="T32">ээ</ta>
            <ta e="T37" id="Seg_2778" s="T34">вообще</ta>
            <ta e="T39" id="Seg_2779" s="T37">один</ta>
            <ta e="T42" id="Seg_2780" s="T39">один</ta>
            <ta e="T44" id="Seg_2781" s="T42">ээ</ta>
            <ta e="T48" id="Seg_2782" s="T46">как</ta>
            <ta e="T50" id="Seg_2783" s="T48">быть-PST1-3SG=Q</ta>
            <ta e="T54" id="Seg_2784" s="T53">один</ta>
            <ta e="T55" id="Seg_2785" s="T54">семья-PL.[NOM]</ta>
            <ta e="T56" id="Seg_2786" s="T55">быть-COND.[3SG]</ta>
            <ta e="T57" id="Seg_2787" s="T56">ээ</ta>
            <ta e="T58" id="Seg_2788" s="T57">хороший.[NOM]</ta>
            <ta e="T59" id="Seg_2789" s="T58">очень-ADVZ</ta>
            <ta e="T60" id="Seg_2790" s="T59">жить-PTCP.PRS</ta>
            <ta e="T61" id="Seg_2791" s="T60">быть-PST1-3PL</ta>
            <ta e="T62" id="Seg_2792" s="T61">быть-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_2793" s="T62">работать-PRS-3PL</ta>
            <ta e="T64" id="Seg_2794" s="T63">работать-PTCP.PRS</ta>
            <ta e="T65" id="Seg_2795" s="T64">быть-PST1-3PL</ta>
            <ta e="T66" id="Seg_2796" s="T65">потом</ta>
            <ta e="T67" id="Seg_2797" s="T66">EMPH</ta>
            <ta e="T68" id="Seg_2798" s="T67">друг-PL-3SG-ACC</ta>
            <ta e="T69" id="Seg_2799" s="T68">встречать-CVB.SEQ</ta>
            <ta e="T70" id="Seg_2800" s="T69">спиртное.[NOM]</ta>
            <ta e="T71" id="Seg_2801" s="T70">пить-CVB.SIM</ta>
            <ta e="T72" id="Seg_2802" s="T71">идти-PST2.[3SG]</ta>
            <ta e="T74" id="Seg_2803" s="T73">там</ta>
            <ta e="T75" id="Seg_2804" s="T74">рассказывать-PRS-3PL</ta>
            <ta e="T76" id="Seg_2805" s="T75">2SG.[NOM]</ta>
            <ta e="T77" id="Seg_2806" s="T76">жена-EP-2SG.[NOM]</ta>
            <ta e="T78" id="Seg_2807" s="T77">тот.[NOM]</ta>
            <ta e="T79" id="Seg_2808" s="T78">подобно</ta>
            <ta e="T81" id="Seg_2809" s="T80">этот.[NOM]</ta>
            <ta e="T82" id="Seg_2810" s="T81">подобно</ta>
            <ta e="T83" id="Seg_2811" s="T82">тот</ta>
            <ta e="T84" id="Seg_2812" s="T83">человек-ACC</ta>
            <ta e="T85" id="Seg_2813" s="T84">с</ta>
            <ta e="T86" id="Seg_2814" s="T85">разговаривать-PST2-3SG</ta>
            <ta e="T87" id="Seg_2815" s="T86">ээ</ta>
            <ta e="T88" id="Seg_2816" s="T87">кто-VBZ-PTCP.PRS</ta>
            <ta e="T89" id="Seg_2817" s="T88">быть-PST1-3SG</ta>
            <ta e="T90" id="Seg_2818" s="T89">как</ta>
            <ta e="T92" id="Seg_2819" s="T90">говорить-HAB-3PL</ta>
            <ta e="T106" id="Seg_2820" s="T105">ээ</ta>
            <ta e="T107" id="Seg_2821" s="T106">жена-EP-2SG.[NOM]</ta>
            <ta e="T108" id="Seg_2822" s="T107">тот.[NOM]</ta>
            <ta e="T109" id="Seg_2823" s="T108">подобно</ta>
            <ta e="T110" id="Seg_2824" s="T109">встречать-EP-ITER-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T112" id="Seg_2825" s="T111">этот.[NOM]</ta>
            <ta e="T113" id="Seg_2826" s="T112">подобно</ta>
            <ta e="T114" id="Seg_2827" s="T113">говорить-PRS-3PL</ta>
            <ta e="T116" id="Seg_2828" s="T115">потом</ta>
            <ta e="T117" id="Seg_2829" s="T116">идти-CVB.SEQ</ta>
            <ta e="T118" id="Seg_2830" s="T117">жена-3SG-ABL</ta>
            <ta e="T119" id="Seg_2831" s="T118">спрашивать-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_2832" s="T119">кто-VBZ-PRS.[3SG]</ta>
            <ta e="T121" id="Seg_2833" s="T120">ээ</ta>
            <ta e="T122" id="Seg_2834" s="T121">кричать-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_2835" s="T122">потом</ta>
            <ta e="T124" id="Seg_2836" s="T123">бить-EP-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_2837" s="T124">жена-3SG-ACC</ta>
            <ta e="T128" id="Seg_2838" s="T127">жена.[NOM]</ta>
            <ta e="T129" id="Seg_2839" s="T128">ребенок-PROPR.[NOM]</ta>
            <ta e="T130" id="Seg_2840" s="T129">ребенок-PROPR.[NOM]</ta>
            <ta e="T131" id="Seg_2841" s="T130">рука-3SG-DAT/LOC</ta>
            <ta e="T132" id="Seg_2842" s="T131">ребенок-PROPR.[NOM]</ta>
            <ta e="T133" id="Seg_2843" s="T132">идти-PTCP.PRS</ta>
            <ta e="T134" id="Seg_2844" s="T133">жена-ACC</ta>
            <ta e="T135" id="Seg_2845" s="T134">бить-PRS.[3SG]</ta>
            <ta e="T137" id="Seg_2846" s="T136">милиция-PL.[NOM]</ta>
            <ta e="T138" id="Seg_2847" s="T137">приходить-PST2-3PL</ta>
            <ta e="T139" id="Seg_2848" s="T138">кто.[NOM]</ta>
            <ta e="T140" id="Seg_2849" s="T139">INDEF</ta>
            <ta e="T141" id="Seg_2850" s="T140">звать-PST2.[3SG]</ta>
            <ta e="T142" id="Seg_2851" s="T141">потом</ta>
            <ta e="T143" id="Seg_2852" s="T142">мм</ta>
            <ta e="T144" id="Seg_2853" s="T143">милиция-PL.[NOM]</ta>
            <ta e="T145" id="Seg_2854" s="T144">приносить-PST2-3PL</ta>
            <ta e="T147" id="Seg_2855" s="T146">каждый-3PL-ACC</ta>
            <ta e="T148" id="Seg_2856" s="T147">приносить-PST2-3PL</ta>
            <ta e="T149" id="Seg_2857" s="T148">туда</ta>
            <ta e="T152" id="Seg_2858" s="T150">допрос-VBZ-PST2-3PL</ta>
            <ta e="T154" id="Seg_2859" s="T152">допрос-VBZ-PST2-3PL</ta>
            <ta e="T156" id="Seg_2860" s="T154">спрашивать-FREQ-PST2-3PL</ta>
            <ta e="T158" id="Seg_2861" s="T156">как</ta>
            <ta e="T160" id="Seg_2862" s="T158">что.[NOM]</ta>
            <ta e="T162" id="Seg_2863" s="T160">быть-PST2-2PL=Q</ta>
            <ta e="T164" id="Seg_2864" s="T162">как</ta>
            <ta e="T166" id="Seg_2865" s="T164">что.[NOM]</ta>
            <ta e="T167" id="Seg_2866" s="T166">быть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T168" id="Seg_2867" s="T167">побить-EP-PST2-2PL=Q</ta>
            <ta e="T169" id="Seg_2868" s="T168">говорить-CVB.SIM</ta>
            <ta e="T170" id="Seg_2869" s="T169">рассказывать-PRS.[3SG]</ta>
            <ta e="T171" id="Seg_2870" s="T170">там</ta>
            <ta e="T172" id="Seg_2871" s="T171">жена-3SG.[NOM]</ta>
            <ta e="T174" id="Seg_2872" s="T173">тот.[NOM]</ta>
            <ta e="T175" id="Seg_2873" s="T174">подобно</ta>
            <ta e="T176" id="Seg_2874" s="T175">бить-PST1-3SG</ta>
            <ta e="T177" id="Seg_2875" s="T176">этот.[NOM]</ta>
            <ta e="T178" id="Seg_2876" s="T177">подобно</ta>
            <ta e="T180" id="Seg_2877" s="T179">потом</ta>
            <ta e="T181" id="Seg_2878" s="T180">тюрьма-DAT/LOC</ta>
            <ta e="T182" id="Seg_2879" s="T181">посадить-PST2-3PL</ta>
            <ta e="T183" id="Seg_2880" s="T182">AFFIRM</ta>
            <ta e="T185" id="Seg_2881" s="T183">тюрьма</ta>
            <ta e="T186" id="Seg_2882" s="T185">тюрьма-DAT/LOC</ta>
            <ta e="T188" id="Seg_2883" s="T186">посадить-PST2-3PL</ta>
            <ta e="T189" id="Seg_2884" s="T188">ага</ta>
            <ta e="T191" id="Seg_2885" s="T190">тюрьма-DAT/LOC</ta>
            <ta e="T192" id="Seg_2886" s="T191">посадить-PST2-3PL</ta>
            <ta e="T194" id="Seg_2887" s="T193">потом</ta>
            <ta e="T195" id="Seg_2888" s="T194">этот</ta>
            <ta e="T197" id="Seg_2889" s="T196">помнить-CVB.SIM</ta>
            <ta e="T198" id="Seg_2890" s="T197">сидеть-PRS.[3SG]</ta>
            <ta e="T199" id="Seg_2891" s="T198">ребенок-3SG-ACC</ta>
            <ta e="T200" id="Seg_2892" s="T199">жена-3SG-ACC</ta>
            <ta e="T201" id="Seg_2893" s="T200">помнить-CVB.SIM</ta>
            <ta e="T202" id="Seg_2894" s="T201">сидеть-PRS.[3SG]</ta>
            <ta e="T203" id="Seg_2895" s="T202">тот.[NOM]</ta>
            <ta e="T204" id="Seg_2896" s="T203">подобно</ta>
            <ta e="T206" id="Seg_2897" s="T205">мм</ta>
            <ta e="T207" id="Seg_2898" s="T206">выйти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T208" id="Seg_2899" s="T207">тюрьма-ABL</ta>
            <ta e="T209" id="Seg_2900" s="T208">выйти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T210" id="Seg_2901" s="T209">ждать</ta>
            <ta e="T211" id="Seg_2902" s="T210">ждать-CVB.SEQ</ta>
            <ta e="T222" id="Seg_2903" s="T220">кто-VBZ-PRS-1SG</ta>
            <ta e="T224" id="Seg_2904" s="T222">говорить-PRS.[3SG]</ta>
            <ta e="T226" id="Seg_2905" s="T224">ээ</ta>
            <ta e="T227" id="Seg_2906" s="T226">мм</ta>
            <ta e="T239" id="Seg_2907" s="T236">нет</ta>
            <ta e="T243" id="Seg_2908" s="T240">кто-VBZ-CVB.SIM</ta>
            <ta e="T244" id="Seg_2909" s="T243">сидеть-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_2910" s="T244">ээ</ta>
            <ta e="T247" id="Seg_2911" s="T246">кто-VBZ-CVB.SIM</ta>
            <ta e="T248" id="Seg_2912" s="T247">сидеть-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_2913" s="T248">помнить-CVB.SIM</ta>
            <ta e="T250" id="Seg_2914" s="T249">сидеть-PRS.[3SG]</ta>
            <ta e="T251" id="Seg_2915" s="T250">дом-PROPR-PL-3SG-ACC</ta>
            <ta e="T252" id="Seg_2916" s="T251">тот.[NOM]</ta>
            <ta e="T253" id="Seg_2917" s="T252">подобно</ta>
            <ta e="T254" id="Seg_2918" s="T253">выйти-TEMP-1SG</ta>
            <ta e="T258" id="Seg_2919" s="T256">AFFIRM</ta>
            <ta e="T260" id="Seg_2920" s="T258">желать-CVB.SIM</ta>
            <ta e="T261" id="Seg_2921" s="T260">сидеть-PRS.[3SG]</ta>
            <ta e="T263" id="Seg_2922" s="T261">помнить-CVB.SIM</ta>
            <ta e="T264" id="Seg_2923" s="T263">сидеть-PRS.[3SG]</ta>
            <ta e="T265" id="Seg_2924" s="T264">желать-PRS.[3SG]</ta>
            <ta e="T267" id="Seg_2925" s="T265">потом</ta>
            <ta e="T272" id="Seg_2926" s="T269">ах</ta>
            <ta e="T277" id="Seg_2927" s="T276">ээ</ta>
            <ta e="T278" id="Seg_2928" s="T277">радоваться-FUT-3PL</ta>
            <ta e="T279" id="Seg_2929" s="T278">1SG.[NOM]</ta>
            <ta e="T280" id="Seg_2930" s="T279">выйти-TEMP-1SG</ta>
            <ta e="T281" id="Seg_2931" s="T280">спиртное.[NOM]</ta>
            <ta e="T282" id="Seg_2932" s="T281">пить-FUT-1SG</ta>
            <ta e="T283" id="Seg_2933" s="T282">NEG-3SG</ta>
            <ta e="T284" id="Seg_2934" s="T283">думать-PRS.[3SG]</ta>
            <ta e="T285" id="Seg_2935" s="T284">больше</ta>
            <ta e="T287" id="Seg_2936" s="T286">выйти-EP-PST2.[3SG]</ta>
            <ta e="T288" id="Seg_2937" s="T287">потом</ta>
            <ta e="T289" id="Seg_2938" s="T288">тюрьма-ABL</ta>
            <ta e="T291" id="Seg_2939" s="T290">радоваться-PRS.[3SG]</ta>
            <ta e="T292" id="Seg_2940" s="T291">радоваться-PRS.[3SG]</ta>
            <ta e="T293" id="Seg_2941" s="T292">солнце.[NOM]</ta>
            <ta e="T294" id="Seg_2942" s="T293">солнце.[NOM]</ta>
            <ta e="T295" id="Seg_2943" s="T294">светить-PRS.[3SG]</ta>
            <ta e="T298" id="Seg_2944" s="T297">AFFIRM</ta>
            <ta e="T301" id="Seg_2945" s="T298">очень</ta>
            <ta e="T302" id="Seg_2946" s="T301">хороший.[NOM]</ta>
            <ta e="T304" id="Seg_2947" s="T302">очень</ta>
            <ta e="T305" id="Seg_2948" s="T304">потом</ta>
            <ta e="T306" id="Seg_2949" s="T305">радоваться-CVB.SIM-радоваться-CVB.SIM</ta>
            <ta e="T307" id="Seg_2950" s="T306">дом-3SG-DAT/LOC</ta>
            <ta e="T308" id="Seg_2951" s="T307">приходить-PST2.[3SG]</ta>
            <ta e="T310" id="Seg_2952" s="T309">тот-3SG-3PL.[NOM]</ta>
            <ta e="T315" id="Seg_2953" s="T313">EMPH</ta>
            <ta e="T316" id="Seg_2954" s="T315">тот-3SG-3PL.[NOM]</ta>
            <ta e="T317" id="Seg_2955" s="T316">MOD</ta>
            <ta e="T318" id="Seg_2956" s="T317">Q</ta>
            <ta e="T319" id="Seg_2957" s="T318">видеть-PRS-3PL</ta>
            <ta e="T320" id="Seg_2958" s="T319">бояться-PRS-3PL</ta>
            <ta e="T321" id="Seg_2959" s="T320">наверное</ta>
            <ta e="T323" id="Seg_2960" s="T322">потом</ta>
            <ta e="T324" id="Seg_2961" s="T323">ребенок-3SG-ACC</ta>
            <ta e="T325" id="Seg_2962" s="T324">с</ta>
            <ta e="T326" id="Seg_2963" s="T325">идти-CVB.SIM</ta>
            <ta e="T327" id="Seg_2964" s="T326">идти-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_2965" s="T327">друг-PL-3SG-ACC</ta>
            <ta e="T329" id="Seg_2966" s="T328">встречать-EP-PST2.[3SG]</ta>
            <ta e="T330" id="Seg_2967" s="T329">приходить.[IMP.2SG]</ta>
            <ta e="T331" id="Seg_2968" s="T330">спиртное-PART</ta>
            <ta e="T332" id="Seg_2969" s="T331">пить.[IMP.2SG]</ta>
            <ta e="T333" id="Seg_2970" s="T332">1PL-ACC</ta>
            <ta e="T334" id="Seg_2971" s="T333">с</ta>
            <ta e="T335" id="Seg_2972" s="T334">нет</ta>
            <ta e="T336" id="Seg_2973" s="T335">нет</ta>
            <ta e="T337" id="Seg_2974" s="T336">пить-NEG-1SG</ta>
            <ta e="T342" id="Seg_2975" s="T341">пить-NEG-1SG</ta>
            <ta e="T343" id="Seg_2976" s="T342">больше</ta>
            <ta e="T344" id="Seg_2977" s="T343">ээ</ta>
            <ta e="T345" id="Seg_2978" s="T344">больше</ta>
            <ta e="T346" id="Seg_2979" s="T345">EMPH</ta>
            <ta e="T347" id="Seg_2980" s="T346">пить-NEG-1SG</ta>
            <ta e="T349" id="Seg_2981" s="T348">пить-FUT-1SG</ta>
            <ta e="T350" id="Seg_2982" s="T349">NEG-3SG</ta>
            <ta e="T351" id="Seg_2983" s="T350">спиртное-ACC</ta>
            <ta e="T352" id="Seg_2984" s="T351">бросать-PST2-EP-1SG</ta>
            <ta e="T353" id="Seg_2985" s="T352">говорить-PST2.[3SG]</ta>
            <ta e="T354" id="Seg_2986" s="T353">короче</ta>
            <ta e="T360" id="Seg_2987" s="T359">быть.богатым-CVB.SEQ-наесться-CVB.SEQ</ta>
            <ta e="T361" id="Seg_2988" s="T360">жить-PST2-3PL</ta>
            <ta e="T362" id="Seg_2989" s="T361">потом</ta>
            <ta e="T364" id="Seg_2990" s="T363">хороший.[NOM]</ta>
            <ta e="T365" id="Seg_2991" s="T364">очень-ADVZ</ta>
         </annotation>
         <annotation name="mc" tierref="mc-UoPP">
            <ta e="T24" id="Seg_2992" s="T22">v.[v:mood.pn]</ta>
            <ta e="T25" id="Seg_2993" s="T24">n</ta>
            <ta e="T26" id="Seg_2994" s="T25">pers.[pro:case]</ta>
            <ta e="T34" id="Seg_2995" s="T32">interj</ta>
            <ta e="T37" id="Seg_2996" s="T34">adv</ta>
            <ta e="T39" id="Seg_2997" s="T37">cardnum</ta>
            <ta e="T42" id="Seg_2998" s="T39">cardnum</ta>
            <ta e="T44" id="Seg_2999" s="T42">interj</ta>
            <ta e="T48" id="Seg_3000" s="T46">que</ta>
            <ta e="T50" id="Seg_3001" s="T48">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T54" id="Seg_3002" s="T53">cardnum</ta>
            <ta e="T55" id="Seg_3003" s="T54">n-n:(num).[n:case]</ta>
            <ta e="T56" id="Seg_3004" s="T55">v-v:mood.[v:pred.pn]</ta>
            <ta e="T57" id="Seg_3005" s="T56">interj</ta>
            <ta e="T58" id="Seg_3006" s="T57">adj.[n:case]</ta>
            <ta e="T59" id="Seg_3007" s="T58">ptcl-ptcl&gt;adv</ta>
            <ta e="T60" id="Seg_3008" s="T59">v-v:ptcp</ta>
            <ta e="T61" id="Seg_3009" s="T60">v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_3010" s="T61">v-v:tense.[v:pred.pn]</ta>
            <ta e="T63" id="Seg_3011" s="T62">v-v:tense-v:pred.pn</ta>
            <ta e="T64" id="Seg_3012" s="T63">v-v:ptcp</ta>
            <ta e="T65" id="Seg_3013" s="T64">v-v:tense-v:pred.pn</ta>
            <ta e="T66" id="Seg_3014" s="T65">adv</ta>
            <ta e="T67" id="Seg_3015" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_3016" s="T67">n-n:(num)-n:poss-n:case</ta>
            <ta e="T69" id="Seg_3017" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_3018" s="T69">n.[n:case]</ta>
            <ta e="T71" id="Seg_3019" s="T70">v-v:cvb</ta>
            <ta e="T72" id="Seg_3020" s="T71">v-v:tense.[v:pred.pn]</ta>
            <ta e="T74" id="Seg_3021" s="T73">adv</ta>
            <ta e="T75" id="Seg_3022" s="T74">v-v:tense-v:pred.pn</ta>
            <ta e="T76" id="Seg_3023" s="T75">pers.[pro:case]</ta>
            <ta e="T77" id="Seg_3024" s="T76">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T78" id="Seg_3025" s="T77">dempro.[pro:case]</ta>
            <ta e="T79" id="Seg_3026" s="T78">post</ta>
            <ta e="T81" id="Seg_3027" s="T80">dempro.[pro:case]</ta>
            <ta e="T82" id="Seg_3028" s="T81">post</ta>
            <ta e="T83" id="Seg_3029" s="T82">dempro</ta>
            <ta e="T84" id="Seg_3030" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_3031" s="T84">post</ta>
            <ta e="T86" id="Seg_3032" s="T85">v-v:tense-v:poss.pn</ta>
            <ta e="T87" id="Seg_3033" s="T86">interj</ta>
            <ta e="T88" id="Seg_3034" s="T87">que-que&gt;v-v:ptcp</ta>
            <ta e="T89" id="Seg_3035" s="T88">v-v:tense-v:poss.pn</ta>
            <ta e="T90" id="Seg_3036" s="T89">que</ta>
            <ta e="T92" id="Seg_3037" s="T90">v-v:mood-v:pred.pn</ta>
            <ta e="T106" id="Seg_3038" s="T105">interj</ta>
            <ta e="T107" id="Seg_3039" s="T106">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T108" id="Seg_3040" s="T107">dempro.[pro:case]</ta>
            <ta e="T109" id="Seg_3041" s="T108">post</ta>
            <ta e="T110" id="Seg_3042" s="T109">v-v:(ins)-v&gt;v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T112" id="Seg_3043" s="T111">dempro.[pro:case]</ta>
            <ta e="T113" id="Seg_3044" s="T112">post</ta>
            <ta e="T114" id="Seg_3045" s="T113">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_3046" s="T115">adv</ta>
            <ta e="T117" id="Seg_3047" s="T116">v-v:cvb</ta>
            <ta e="T118" id="Seg_3048" s="T117">n-n:poss-n:case</ta>
            <ta e="T119" id="Seg_3049" s="T118">v-v:tense.[v:pred.pn]</ta>
            <ta e="T120" id="Seg_3050" s="T119">que-que&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T121" id="Seg_3051" s="T120">interj</ta>
            <ta e="T122" id="Seg_3052" s="T121">v-v:tense.[v:pred.pn]</ta>
            <ta e="T123" id="Seg_3053" s="T122">adv</ta>
            <ta e="T124" id="Seg_3054" s="T123">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T125" id="Seg_3055" s="T124">n-n:poss-n:case</ta>
            <ta e="T128" id="Seg_3056" s="T127">n.[n:case]</ta>
            <ta e="T129" id="Seg_3057" s="T128">n-n&gt;adj.[n:case]</ta>
            <ta e="T130" id="Seg_3058" s="T129">n-n&gt;adj.[n:case]</ta>
            <ta e="T131" id="Seg_3059" s="T130">n-n:poss-n:case</ta>
            <ta e="T132" id="Seg_3060" s="T131">n-n&gt;adj.[n:case]</ta>
            <ta e="T133" id="Seg_3061" s="T132">v-v:ptcp</ta>
            <ta e="T134" id="Seg_3062" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_3063" s="T134">v-v:tense.[v:pred.pn]</ta>
            <ta e="T137" id="Seg_3064" s="T136">n-n:(num).[n:case]</ta>
            <ta e="T138" id="Seg_3065" s="T137">v-v:tense-v:pred.pn</ta>
            <ta e="T139" id="Seg_3066" s="T138">que.[pro:case]</ta>
            <ta e="T140" id="Seg_3067" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_3068" s="T140">v-v:tense.[v:pred.pn]</ta>
            <ta e="T142" id="Seg_3069" s="T141">adv</ta>
            <ta e="T143" id="Seg_3070" s="T142">interj</ta>
            <ta e="T144" id="Seg_3071" s="T143">n-n:(num).[n:case]</ta>
            <ta e="T145" id="Seg_3072" s="T144">v-v:tense-v:pred.pn</ta>
            <ta e="T147" id="Seg_3073" s="T146">adj-n:poss-n:case</ta>
            <ta e="T148" id="Seg_3074" s="T147">v-v:tense-v:pred.pn</ta>
            <ta e="T149" id="Seg_3075" s="T148">adv</ta>
            <ta e="T152" id="Seg_3076" s="T150">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T154" id="Seg_3077" s="T152">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_3078" s="T154">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T158" id="Seg_3079" s="T156">que</ta>
            <ta e="T160" id="Seg_3080" s="T158">que.[pro:case]</ta>
            <ta e="T162" id="Seg_3081" s="T160">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T164" id="Seg_3082" s="T162">que</ta>
            <ta e="T166" id="Seg_3083" s="T164">que.[pro:case]</ta>
            <ta e="T167" id="Seg_3084" s="T166">v-v:ptcp.[v:poss.pn]-v:(case)</ta>
            <ta e="T168" id="Seg_3085" s="T167">v-v:(ins)-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T169" id="Seg_3086" s="T168">v-v:cvb</ta>
            <ta e="T170" id="Seg_3087" s="T169">v-v:tense.[v:pred.pn]</ta>
            <ta e="T171" id="Seg_3088" s="T170">adv</ta>
            <ta e="T172" id="Seg_3089" s="T171">n-n:(poss).[n:case]</ta>
            <ta e="T174" id="Seg_3090" s="T173">dempro.[pro:case]</ta>
            <ta e="T175" id="Seg_3091" s="T174">post</ta>
            <ta e="T176" id="Seg_3092" s="T175">v-v:tense-v:poss.pn</ta>
            <ta e="T177" id="Seg_3093" s="T176">dempro.[pro:case]</ta>
            <ta e="T178" id="Seg_3094" s="T177">post</ta>
            <ta e="T180" id="Seg_3095" s="T179">adv</ta>
            <ta e="T181" id="Seg_3096" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_3097" s="T181">v-v:tense-v:pred.pn</ta>
            <ta e="T183" id="Seg_3098" s="T182">ptcl</ta>
            <ta e="T185" id="Seg_3099" s="T183">n</ta>
            <ta e="T186" id="Seg_3100" s="T185">n-n:case</ta>
            <ta e="T188" id="Seg_3101" s="T186">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_3102" s="T188">interj</ta>
            <ta e="T191" id="Seg_3103" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_3104" s="T191">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_3105" s="T193">adv</ta>
            <ta e="T195" id="Seg_3106" s="T194">dempro</ta>
            <ta e="T197" id="Seg_3107" s="T196">v-v:cvb</ta>
            <ta e="T198" id="Seg_3108" s="T197">v-v:tense.[v:pred.pn]</ta>
            <ta e="T199" id="Seg_3109" s="T198">n-n:poss-n:case</ta>
            <ta e="T200" id="Seg_3110" s="T199">n-n:poss-n:case</ta>
            <ta e="T201" id="Seg_3111" s="T200">v-v:cvb</ta>
            <ta e="T202" id="Seg_3112" s="T201">v-v:tense.[v:pred.pn]</ta>
            <ta e="T203" id="Seg_3113" s="T202">dempro.[pro:case]</ta>
            <ta e="T204" id="Seg_3114" s="T203">post</ta>
            <ta e="T206" id="Seg_3115" s="T205">interj</ta>
            <ta e="T207" id="Seg_3116" s="T206">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T208" id="Seg_3117" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_3118" s="T208">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T210" id="Seg_3119" s="T209">v</ta>
            <ta e="T211" id="Seg_3120" s="T210">v-v:cvb</ta>
            <ta e="T222" id="Seg_3121" s="T220">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T224" id="Seg_3122" s="T222">v-v:tense.[v:pred.pn]</ta>
            <ta e="T226" id="Seg_3123" s="T224">interj</ta>
            <ta e="T227" id="Seg_3124" s="T226">interj</ta>
            <ta e="T239" id="Seg_3125" s="T236">ptcl</ta>
            <ta e="T243" id="Seg_3126" s="T240">que-que&gt;v-v:cvb</ta>
            <ta e="T244" id="Seg_3127" s="T243">v-v:tense.[v:pred.pn]</ta>
            <ta e="T245" id="Seg_3128" s="T244">interj</ta>
            <ta e="T247" id="Seg_3129" s="T246">que-que&gt;v-v:cvb</ta>
            <ta e="T248" id="Seg_3130" s="T247">v-v:tense.[v:pred.pn]</ta>
            <ta e="T249" id="Seg_3131" s="T248">v-v:cvb</ta>
            <ta e="T250" id="Seg_3132" s="T249">v-v:tense.[v:pred.pn]</ta>
            <ta e="T251" id="Seg_3133" s="T250">n-n&gt;adj-n:(num)-n:poss-n:case</ta>
            <ta e="T252" id="Seg_3134" s="T251">dempro.[pro:case]</ta>
            <ta e="T253" id="Seg_3135" s="T252">post</ta>
            <ta e="T254" id="Seg_3136" s="T253">v-v:mood-v:temp.pn</ta>
            <ta e="T258" id="Seg_3137" s="T256">ptcl</ta>
            <ta e="T260" id="Seg_3138" s="T258">v-v:cvb</ta>
            <ta e="T261" id="Seg_3139" s="T260">v-v:tense.[v:pred.pn]</ta>
            <ta e="T263" id="Seg_3140" s="T261">v-v:cvb</ta>
            <ta e="T264" id="Seg_3141" s="T263">v-v:tense.[v:pred.pn]</ta>
            <ta e="T265" id="Seg_3142" s="T264">v-v:tense.[v:pred.pn]</ta>
            <ta e="T267" id="Seg_3143" s="T265">adv</ta>
            <ta e="T272" id="Seg_3144" s="T269">interj</ta>
            <ta e="T277" id="Seg_3145" s="T276">interj</ta>
            <ta e="T278" id="Seg_3146" s="T277">v-v:tense-v:poss.pn</ta>
            <ta e="T279" id="Seg_3147" s="T278">pers.[pro:case]</ta>
            <ta e="T280" id="Seg_3148" s="T279">v-v:mood-v:temp.pn</ta>
            <ta e="T281" id="Seg_3149" s="T280">n.[n:case]</ta>
            <ta e="T282" id="Seg_3150" s="T281">v-v:tense-v:poss.pn</ta>
            <ta e="T283" id="Seg_3151" s="T282">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T284" id="Seg_3152" s="T283">v-v:tense.[v:pred.pn]</ta>
            <ta e="T285" id="Seg_3153" s="T284">adv</ta>
            <ta e="T287" id="Seg_3154" s="T286">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T288" id="Seg_3155" s="T287">adv</ta>
            <ta e="T289" id="Seg_3156" s="T288">n-n:case</ta>
            <ta e="T291" id="Seg_3157" s="T290">v-v:tense.[v:pred.pn]</ta>
            <ta e="T292" id="Seg_3158" s="T291">v-v:tense.[v:pred.pn]</ta>
            <ta e="T293" id="Seg_3159" s="T292">n.[n:case]</ta>
            <ta e="T294" id="Seg_3160" s="T293">n.[n:case]</ta>
            <ta e="T295" id="Seg_3161" s="T294">v-v:tense.[v:pred.pn]</ta>
            <ta e="T298" id="Seg_3162" s="T297">ptcl</ta>
            <ta e="T301" id="Seg_3163" s="T298">adv</ta>
            <ta e="T302" id="Seg_3164" s="T301">adj.[n:case]</ta>
            <ta e="T304" id="Seg_3165" s="T302">ptcl</ta>
            <ta e="T305" id="Seg_3166" s="T304">adv</ta>
            <ta e="T306" id="Seg_3167" s="T305">v-v:cvb-v-v:cvb</ta>
            <ta e="T307" id="Seg_3168" s="T306">n-n:poss-n:case</ta>
            <ta e="T308" id="Seg_3169" s="T307">v-v:tense.[v:pred.pn]</ta>
            <ta e="T310" id="Seg_3170" s="T309">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T315" id="Seg_3171" s="T313">ptcl</ta>
            <ta e="T316" id="Seg_3172" s="T315">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T317" id="Seg_3173" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_3174" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_3175" s="T318">v-v:tense-v:pred.pn</ta>
            <ta e="T320" id="Seg_3176" s="T319">v-v:tense-v:pred.pn</ta>
            <ta e="T321" id="Seg_3177" s="T320">adv</ta>
            <ta e="T323" id="Seg_3178" s="T322">adv</ta>
            <ta e="T324" id="Seg_3179" s="T323">n-n:poss-n:case</ta>
            <ta e="T325" id="Seg_3180" s="T324">post</ta>
            <ta e="T326" id="Seg_3181" s="T325">v-v:cvb</ta>
            <ta e="T327" id="Seg_3182" s="T326">v-v:tense.[v:pred.pn]</ta>
            <ta e="T328" id="Seg_3183" s="T327">n-n:(num)-n:poss-n:case</ta>
            <ta e="T329" id="Seg_3184" s="T328">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T330" id="Seg_3185" s="T329">v.[v:mood.pn]</ta>
            <ta e="T331" id="Seg_3186" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_3187" s="T331">v.[v:mood.pn]</ta>
            <ta e="T333" id="Seg_3188" s="T332">pers-pro:case</ta>
            <ta e="T334" id="Seg_3189" s="T333">post</ta>
            <ta e="T335" id="Seg_3190" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_3191" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_3192" s="T336">v-v:(neg)-v:pred.pn</ta>
            <ta e="T342" id="Seg_3193" s="T341">v-v:(neg)-v:pred.pn</ta>
            <ta e="T343" id="Seg_3194" s="T342">adv</ta>
            <ta e="T344" id="Seg_3195" s="T343">interj</ta>
            <ta e="T345" id="Seg_3196" s="T344">adv</ta>
            <ta e="T346" id="Seg_3197" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_3198" s="T346">v-v:(neg)-v:pred.pn</ta>
            <ta e="T349" id="Seg_3199" s="T348">v-v:tense-v:poss.pn</ta>
            <ta e="T350" id="Seg_3200" s="T349">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T351" id="Seg_3201" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_3202" s="T351">v-v:tense-v(ins)-v:poss.pn</ta>
            <ta e="T353" id="Seg_3203" s="T352">v-v:tense.[v:pred.pn]</ta>
            <ta e="T354" id="Seg_3204" s="T353">ptcl</ta>
            <ta e="T360" id="Seg_3205" s="T359">v-v:cvb-v-v:cvb</ta>
            <ta e="T361" id="Seg_3206" s="T360">v-v:tense-v:pred.pn</ta>
            <ta e="T362" id="Seg_3207" s="T361">adv</ta>
            <ta e="T364" id="Seg_3208" s="T363">adj.[n:case]</ta>
            <ta e="T365" id="Seg_3209" s="T364">ptcl-ptcl&gt;adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps-UoPP">
            <ta e="T24" id="Seg_3210" s="T22">v</ta>
            <ta e="T25" id="Seg_3211" s="T24">n</ta>
            <ta e="T26" id="Seg_3212" s="T25">pers</ta>
            <ta e="T34" id="Seg_3213" s="T32">interj</ta>
            <ta e="T37" id="Seg_3214" s="T34">adv</ta>
            <ta e="T39" id="Seg_3215" s="T37">cardnum</ta>
            <ta e="T42" id="Seg_3216" s="T39">cardnum</ta>
            <ta e="T44" id="Seg_3217" s="T42">interj</ta>
            <ta e="T48" id="Seg_3218" s="T46">que</ta>
            <ta e="T50" id="Seg_3219" s="T48">cop</ta>
            <ta e="T54" id="Seg_3220" s="T53">cardnum</ta>
            <ta e="T55" id="Seg_3221" s="T54">n</ta>
            <ta e="T56" id="Seg_3222" s="T55">v</ta>
            <ta e="T57" id="Seg_3223" s="T56">interj</ta>
            <ta e="T58" id="Seg_3224" s="T57">adj</ta>
            <ta e="T59" id="Seg_3225" s="T58">adv</ta>
            <ta e="T60" id="Seg_3226" s="T59">v</ta>
            <ta e="T61" id="Seg_3227" s="T60">aux</ta>
            <ta e="T62" id="Seg_3228" s="T61">aux</ta>
            <ta e="T63" id="Seg_3229" s="T62">v</ta>
            <ta e="T64" id="Seg_3230" s="T63">v</ta>
            <ta e="T65" id="Seg_3231" s="T64">aux</ta>
            <ta e="T66" id="Seg_3232" s="T65">adv</ta>
            <ta e="T67" id="Seg_3233" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_3234" s="T67">n</ta>
            <ta e="T69" id="Seg_3235" s="T68">v</ta>
            <ta e="T70" id="Seg_3236" s="T69">n</ta>
            <ta e="T71" id="Seg_3237" s="T70">v</ta>
            <ta e="T72" id="Seg_3238" s="T71">v</ta>
            <ta e="T74" id="Seg_3239" s="T73">adv</ta>
            <ta e="T75" id="Seg_3240" s="T74">v</ta>
            <ta e="T76" id="Seg_3241" s="T75">pers</ta>
            <ta e="T77" id="Seg_3242" s="T76">n</ta>
            <ta e="T78" id="Seg_3243" s="T77">dempro</ta>
            <ta e="T79" id="Seg_3244" s="T78">post</ta>
            <ta e="T81" id="Seg_3245" s="T80">dempro</ta>
            <ta e="T82" id="Seg_3246" s="T81">post</ta>
            <ta e="T83" id="Seg_3247" s="T82">dempro</ta>
            <ta e="T84" id="Seg_3248" s="T83">n</ta>
            <ta e="T85" id="Seg_3249" s="T84">post</ta>
            <ta e="T86" id="Seg_3250" s="T85">v</ta>
            <ta e="T87" id="Seg_3251" s="T86">interj</ta>
            <ta e="T88" id="Seg_3252" s="T87">v</ta>
            <ta e="T89" id="Seg_3253" s="T88">aux</ta>
            <ta e="T90" id="Seg_3254" s="T89">que</ta>
            <ta e="T92" id="Seg_3255" s="T90">v</ta>
            <ta e="T106" id="Seg_3256" s="T105">interj</ta>
            <ta e="T107" id="Seg_3257" s="T106">n</ta>
            <ta e="T108" id="Seg_3258" s="T107">dempro</ta>
            <ta e="T109" id="Seg_3259" s="T108">post</ta>
            <ta e="T110" id="Seg_3260" s="T109">v</ta>
            <ta e="T112" id="Seg_3261" s="T111">dempro</ta>
            <ta e="T113" id="Seg_3262" s="T112">post</ta>
            <ta e="T114" id="Seg_3263" s="T113">v</ta>
            <ta e="T116" id="Seg_3264" s="T115">adv</ta>
            <ta e="T117" id="Seg_3265" s="T116">v</ta>
            <ta e="T118" id="Seg_3266" s="T117">n</ta>
            <ta e="T119" id="Seg_3267" s="T118">v</ta>
            <ta e="T120" id="Seg_3268" s="T119">v</ta>
            <ta e="T121" id="Seg_3269" s="T120">interj</ta>
            <ta e="T122" id="Seg_3270" s="T121">v</ta>
            <ta e="T123" id="Seg_3271" s="T122">adv</ta>
            <ta e="T124" id="Seg_3272" s="T123">v</ta>
            <ta e="T125" id="Seg_3273" s="T124">n</ta>
            <ta e="T128" id="Seg_3274" s="T127">n</ta>
            <ta e="T129" id="Seg_3275" s="T128">adj</ta>
            <ta e="T130" id="Seg_3276" s="T129">adj</ta>
            <ta e="T131" id="Seg_3277" s="T130">n</ta>
            <ta e="T132" id="Seg_3278" s="T131">adj</ta>
            <ta e="T133" id="Seg_3279" s="T132">v</ta>
            <ta e="T134" id="Seg_3280" s="T133">n</ta>
            <ta e="T135" id="Seg_3281" s="T134">v</ta>
            <ta e="T137" id="Seg_3282" s="T136">n</ta>
            <ta e="T138" id="Seg_3283" s="T137">v</ta>
            <ta e="T139" id="Seg_3284" s="T138">que</ta>
            <ta e="T140" id="Seg_3285" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_3286" s="T140">v</ta>
            <ta e="T142" id="Seg_3287" s="T141">adv</ta>
            <ta e="T143" id="Seg_3288" s="T142">interj</ta>
            <ta e="T144" id="Seg_3289" s="T143">n</ta>
            <ta e="T145" id="Seg_3290" s="T144">v</ta>
            <ta e="T147" id="Seg_3291" s="T146">adj</ta>
            <ta e="T148" id="Seg_3292" s="T147">v</ta>
            <ta e="T149" id="Seg_3293" s="T148">adv</ta>
            <ta e="T152" id="Seg_3294" s="T150">v</ta>
            <ta e="T154" id="Seg_3295" s="T152">v</ta>
            <ta e="T156" id="Seg_3296" s="T154">v</ta>
            <ta e="T158" id="Seg_3297" s="T156">que</ta>
            <ta e="T160" id="Seg_3298" s="T158">que</ta>
            <ta e="T162" id="Seg_3299" s="T160">cop</ta>
            <ta e="T164" id="Seg_3300" s="T162">que</ta>
            <ta e="T166" id="Seg_3301" s="T164">que</ta>
            <ta e="T167" id="Seg_3302" s="T166">cop</ta>
            <ta e="T168" id="Seg_3303" s="T167">v</ta>
            <ta e="T169" id="Seg_3304" s="T168">v</ta>
            <ta e="T170" id="Seg_3305" s="T169">v</ta>
            <ta e="T171" id="Seg_3306" s="T170">adv</ta>
            <ta e="T172" id="Seg_3307" s="T171">n</ta>
            <ta e="T174" id="Seg_3308" s="T173">dempro</ta>
            <ta e="T175" id="Seg_3309" s="T174">post</ta>
            <ta e="T176" id="Seg_3310" s="T175">v</ta>
            <ta e="T177" id="Seg_3311" s="T176">dempro</ta>
            <ta e="T178" id="Seg_3312" s="T177">post</ta>
            <ta e="T180" id="Seg_3313" s="T179">adv</ta>
            <ta e="T181" id="Seg_3314" s="T180">n</ta>
            <ta e="T182" id="Seg_3315" s="T181">v</ta>
            <ta e="T183" id="Seg_3316" s="T182">ptcl</ta>
            <ta e="T185" id="Seg_3317" s="T183">n</ta>
            <ta e="T186" id="Seg_3318" s="T185">n</ta>
            <ta e="T188" id="Seg_3319" s="T186">v</ta>
            <ta e="T189" id="Seg_3320" s="T188">interj</ta>
            <ta e="T191" id="Seg_3321" s="T190">n</ta>
            <ta e="T192" id="Seg_3322" s="T191">v</ta>
            <ta e="T194" id="Seg_3323" s="T193">adv</ta>
            <ta e="T195" id="Seg_3324" s="T194">dempro</ta>
            <ta e="T197" id="Seg_3325" s="T196">v</ta>
            <ta e="T198" id="Seg_3326" s="T197">v</ta>
            <ta e="T199" id="Seg_3327" s="T198">n</ta>
            <ta e="T200" id="Seg_3328" s="T199">n</ta>
            <ta e="T201" id="Seg_3329" s="T200">v</ta>
            <ta e="T202" id="Seg_3330" s="T201">aux</ta>
            <ta e="T203" id="Seg_3331" s="T202">dempro</ta>
            <ta e="T204" id="Seg_3332" s="T203">post</ta>
            <ta e="T206" id="Seg_3333" s="T205">interj</ta>
            <ta e="T207" id="Seg_3334" s="T206">v</ta>
            <ta e="T208" id="Seg_3335" s="T207">n</ta>
            <ta e="T209" id="Seg_3336" s="T208">v</ta>
            <ta e="T210" id="Seg_3337" s="T209">v</ta>
            <ta e="T211" id="Seg_3338" s="T210">v</ta>
            <ta e="T222" id="Seg_3339" s="T220">v</ta>
            <ta e="T224" id="Seg_3340" s="T222">v</ta>
            <ta e="T226" id="Seg_3341" s="T224">interj</ta>
            <ta e="T227" id="Seg_3342" s="T226">interj</ta>
            <ta e="T239" id="Seg_3343" s="T236">ptcl</ta>
            <ta e="T243" id="Seg_3344" s="T240">v</ta>
            <ta e="T244" id="Seg_3345" s="T243">aux</ta>
            <ta e="T245" id="Seg_3346" s="T244">interj</ta>
            <ta e="T247" id="Seg_3347" s="T246">v</ta>
            <ta e="T248" id="Seg_3348" s="T247">aux</ta>
            <ta e="T249" id="Seg_3349" s="T248">v</ta>
            <ta e="T250" id="Seg_3350" s="T249">aux</ta>
            <ta e="T251" id="Seg_3351" s="T250">n</ta>
            <ta e="T252" id="Seg_3352" s="T251">dempro</ta>
            <ta e="T253" id="Seg_3353" s="T252">post</ta>
            <ta e="T254" id="Seg_3354" s="T253">v</ta>
            <ta e="T258" id="Seg_3355" s="T256">ptcl</ta>
            <ta e="T260" id="Seg_3356" s="T258">v</ta>
            <ta e="T261" id="Seg_3357" s="T260">aux</ta>
            <ta e="T263" id="Seg_3358" s="T261">v</ta>
            <ta e="T264" id="Seg_3359" s="T263">aux</ta>
            <ta e="T265" id="Seg_3360" s="T264">v</ta>
            <ta e="T267" id="Seg_3361" s="T265">adv</ta>
            <ta e="T272" id="Seg_3362" s="T269">interj</ta>
            <ta e="T277" id="Seg_3363" s="T276">interj</ta>
            <ta e="T278" id="Seg_3364" s="T277">v</ta>
            <ta e="T279" id="Seg_3365" s="T278">pers</ta>
            <ta e="T280" id="Seg_3366" s="T279">v</ta>
            <ta e="T281" id="Seg_3367" s="T280">n</ta>
            <ta e="T282" id="Seg_3368" s="T281">v</ta>
            <ta e="T283" id="Seg_3369" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_3370" s="T283">v</ta>
            <ta e="T285" id="Seg_3371" s="T284">adv</ta>
            <ta e="T287" id="Seg_3372" s="T286">v</ta>
            <ta e="T288" id="Seg_3373" s="T287">adv</ta>
            <ta e="T289" id="Seg_3374" s="T288">n</ta>
            <ta e="T291" id="Seg_3375" s="T290">v</ta>
            <ta e="T292" id="Seg_3376" s="T291">v</ta>
            <ta e="T293" id="Seg_3377" s="T292">n</ta>
            <ta e="T294" id="Seg_3378" s="T293">n</ta>
            <ta e="T295" id="Seg_3379" s="T294">v</ta>
            <ta e="T298" id="Seg_3380" s="T297">ptcl</ta>
            <ta e="T301" id="Seg_3381" s="T298">adv</ta>
            <ta e="T302" id="Seg_3382" s="T301">adj</ta>
            <ta e="T304" id="Seg_3383" s="T302">ptcl</ta>
            <ta e="T305" id="Seg_3384" s="T304">adv</ta>
            <ta e="T306" id="Seg_3385" s="T305">v</ta>
            <ta e="T307" id="Seg_3386" s="T306">n</ta>
            <ta e="T308" id="Seg_3387" s="T307">v</ta>
            <ta e="T310" id="Seg_3388" s="T309">dempro</ta>
            <ta e="T315" id="Seg_3389" s="T313">ptcl</ta>
            <ta e="T316" id="Seg_3390" s="T315">dempro</ta>
            <ta e="T317" id="Seg_3391" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_3392" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_3393" s="T318">v</ta>
            <ta e="T320" id="Seg_3394" s="T319">v</ta>
            <ta e="T321" id="Seg_3395" s="T320">adv</ta>
            <ta e="T323" id="Seg_3396" s="T322">adv</ta>
            <ta e="T324" id="Seg_3397" s="T323">n</ta>
            <ta e="T325" id="Seg_3398" s="T324">post</ta>
            <ta e="T326" id="Seg_3399" s="T325">v</ta>
            <ta e="T327" id="Seg_3400" s="T326">v</ta>
            <ta e="T328" id="Seg_3401" s="T327">n</ta>
            <ta e="T329" id="Seg_3402" s="T328">v</ta>
            <ta e="T330" id="Seg_3403" s="T329">v</ta>
            <ta e="T331" id="Seg_3404" s="T330">n</ta>
            <ta e="T332" id="Seg_3405" s="T331">v</ta>
            <ta e="T333" id="Seg_3406" s="T332">pers</ta>
            <ta e="T334" id="Seg_3407" s="T333">post</ta>
            <ta e="T335" id="Seg_3408" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_3409" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_3410" s="T336">v</ta>
            <ta e="T342" id="Seg_3411" s="T341">v</ta>
            <ta e="T343" id="Seg_3412" s="T342">adv</ta>
            <ta e="T344" id="Seg_3413" s="T343">interj</ta>
            <ta e="T345" id="Seg_3414" s="T344">adv</ta>
            <ta e="T346" id="Seg_3415" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_3416" s="T346">v</ta>
            <ta e="T349" id="Seg_3417" s="T348">v</ta>
            <ta e="T350" id="Seg_3418" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_3419" s="T350">n</ta>
            <ta e="T352" id="Seg_3420" s="T351">v</ta>
            <ta e="T353" id="Seg_3421" s="T352">v</ta>
            <ta e="T354" id="Seg_3422" s="T353">ptcl</ta>
            <ta e="T360" id="Seg_3423" s="T359">v</ta>
            <ta e="T361" id="Seg_3424" s="T360">v</ta>
            <ta e="T362" id="Seg_3425" s="T361">adv</ta>
            <ta e="T364" id="Seg_3426" s="T363">adj</ta>
            <ta e="T365" id="Seg_3427" s="T364">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-UoPP" />
         <annotation name="SyF" tierref="SyF-UoPP" />
         <annotation name="IST" tierref="IST-UoPP" />
         <annotation name="Top" tierref="Top-UoPP" />
         <annotation name="Foc" tierref="Foc-UoPP" />
         <annotation name="BOR" tierref="BOR-UoPP">
            <ta e="T25" id="Seg_3428" s="T24">EV:core</ta>
            <ta e="T37" id="Seg_3429" s="T34">RUS:mod</ta>
            <ta e="T68" id="Seg_3430" s="T67">EV:core</ta>
            <ta e="T137" id="Seg_3431" s="T136">RUS:cult</ta>
            <ta e="T144" id="Seg_3432" s="T143">RUS:cult</ta>
            <ta e="T152" id="Seg_3433" s="T150">RUS:cult</ta>
            <ta e="T154" id="Seg_3434" s="T152">RUS:cult</ta>
            <ta e="T181" id="Seg_3435" s="T180">RUS:cult</ta>
            <ta e="T185" id="Seg_3436" s="T183">RUS:cult</ta>
            <ta e="T208" id="Seg_3437" s="T207">RUS:cult</ta>
            <ta e="T285" id="Seg_3438" s="T284">RUS:mod</ta>
            <ta e="T289" id="Seg_3439" s="T288">RUS:cult</ta>
            <ta e="T328" id="Seg_3440" s="T327">EV:core</ta>
            <ta e="T343" id="Seg_3441" s="T342">RUS:mod</ta>
            <ta e="T345" id="Seg_3442" s="T344">RUS:mod</ta>
            <ta e="T354" id="Seg_3443" s="T353">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-UoPP">
            <ta e="T137" id="Seg_3444" s="T136">Vsub</ta>
            <ta e="T144" id="Seg_3445" s="T143">Vsub</ta>
            <ta e="T181" id="Seg_3446" s="T180">Vsub Vsub</ta>
            <ta e="T185" id="Seg_3447" s="T183">Vsub Vsub</ta>
            <ta e="T208" id="Seg_3448" s="T207">Vsub Vsub</ta>
            <ta e="T289" id="Seg_3449" s="T288">Vsub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-UoPP">
            <ta e="T25" id="Seg_3450" s="T24">dir:bare</ta>
            <ta e="T37" id="Seg_3451" s="T34">dir:bare</ta>
            <ta e="T68" id="Seg_3452" s="T67">dir:infl</ta>
            <ta e="T137" id="Seg_3453" s="T136">dir:infl</ta>
            <ta e="T144" id="Seg_3454" s="T143">dir:infl</ta>
            <ta e="T152" id="Seg_3455" s="T150">indir:infl</ta>
            <ta e="T154" id="Seg_3456" s="T152">indir:infl</ta>
            <ta e="T181" id="Seg_3457" s="T180">dir:infl</ta>
            <ta e="T185" id="Seg_3458" s="T183">dir:bare</ta>
            <ta e="T208" id="Seg_3459" s="T207">dir:infl</ta>
            <ta e="T285" id="Seg_3460" s="T284">dir:bare</ta>
            <ta e="T289" id="Seg_3461" s="T288">dir:infl</ta>
            <ta e="T328" id="Seg_3462" s="T327">dir:infl</ta>
            <ta e="T343" id="Seg_3463" s="T342">dir:bare</ta>
            <ta e="T345" id="Seg_3464" s="T344">dir:bare</ta>
            <ta e="T354" id="Seg_3465" s="T353">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-UoPP">
            <ta e="T46" id="Seg_3466" s="T44">RUS:int.alt</ta>
            <ta e="T101" id="Seg_3467" s="T92">RUS:int.ins</ta>
            <ta e="T214" id="Seg_3468" s="T211">RUS:int.alt</ta>
            <ta e="T236" id="Seg_3469" s="T233">RUS:int.alt</ta>
            <ta e="T340" id="Seg_3470" s="T337">RUS:ext</ta>
            <ta e="T357" id="Seg_3471" s="T354">RUS:ext</ta>
            <ta e="T370" id="Seg_3472" s="T366">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe-UoPP">
            <ta e="T26" id="Seg_3473" s="T22">– You tell.</ta>
            <ta e="T50" id="Seg_3474" s="T32">– Eh, at all, one, one, eh, how was "family"?</ta>
            <ta e="T65" id="Seg_3475" s="T53">– If it is a family, eh, they were living very well apparently, they are working, they were working.</ta>
            <ta e="T72" id="Seg_3476" s="T65">Then he met his friends and went to drink alcohol.</ta>
            <ta e="T75" id="Seg_3477" s="T73">There it is told: </ta>
            <ta e="T79" id="Seg_3478" s="T75">"Your wife, like that.</ta>
            <ta e="T85" id="Seg_3479" s="T80">Like this, with his man.</ta>
            <ta e="T89" id="Seg_3480" s="T85">She was chatting, eh, and so on."</ta>
            <ta e="T101" id="Seg_3481" s="T89">How does one say, in brief, she was flirting ((…)).</ta>
            <ta e="T110" id="Seg_3482" s="T105">"Eh, your wife is betraying [you] like that."</ta>
            <ta e="T114" id="Seg_3483" s="T111">Like this they say.</ta>
            <ta e="T122" id="Seg_3484" s="T115">Then he goes and asks his wife, he whatchamacallit, eh, is shouting.</ta>
            <ta e="T125" id="Seg_3485" s="T122">Then he beat his wife.</ta>
            <ta e="T135" id="Seg_3486" s="T126">The woman with child, he is beating the woman who is holding the child in her arms.</ta>
            <ta e="T141" id="Seg_3487" s="T136">Policemen came, somebody had called [them].</ta>
            <ta e="T145" id="Seg_3488" s="T141">Then, mhm, the policemen took [him?] away.</ta>
            <ta e="T149" id="Seg_3489" s="T146">They brought all of them there.</ta>
            <ta e="T172" id="Seg_3490" s="T150">They were interrogating, interrogating, they were questioning then, "how was what, why did you quarrel", then the woman is telling.</ta>
            <ta e="T178" id="Seg_3491" s="T173">"Like that he thrashed, like this."</ta>
            <ta e="T189" id="Seg_3492" s="T179">Then they put him in prison, ah, prison, they put him in prison, mhm.</ta>
            <ta e="T192" id="Seg_3493" s="T190">They pur him in prison.</ta>
            <ta e="T204" id="Seg_3494" s="T193">Then he is sitting and remembering, he is remembering his child and his wife, like that.</ta>
            <ta e="T214" id="Seg_3495" s="T205">Mhm, waiting for that he comes out, that he comes out from prison, eh, he is dreaming. </ta>
            <ta e="T227" id="Seg_3496" s="T216">"I whatchamacallit", he says, mhm.</ta>
            <ta e="T239" id="Seg_3497" s="T233">– No, no.</ta>
            <ta e="T254" id="Seg_3498" s="T240">He whatchamacallit, eh, whatchamacallit, he is remembering his family, "like that when I come out".</ta>
            <ta e="T267" id="Seg_3499" s="T256">– Yes, he wishes, he is remembering, he wishes then. </ta>
            <ta e="T272" id="Seg_3500" s="T269">– Ah.</ta>
            <ta e="T285" id="Seg_3501" s="T276">Eh, "they'll be happy, when I'll come out, I won't drink alcohol anymore", he thinks.</ta>
            <ta e="T289" id="Seg_3502" s="T286">He came out from prison then.</ta>
            <ta e="T295" id="Seg_3503" s="T290">He is happy, he is happy, the sun is shining.</ta>
            <ta e="T308" id="Seg_3504" s="T297">Yes, very good, they he came home being happy.</ta>
            <ta e="T310" id="Seg_3505" s="T309">– Those ones.</ta>
            <ta e="T321" id="Seg_3506" s="T313">Eh, those ones are looking, they are apparently afraid.</ta>
            <ta e="T327" id="Seg_3507" s="T322">Then he was going for a walk with his child.</ta>
            <ta e="T337" id="Seg_3508" s="T327">He met his friend, "come, drink alcohol with us", "no, no, I don't drink".</ta>
            <ta e="T340" id="Seg_3509" s="T337">Like a tale.</ta>
            <ta e="T347" id="Seg_3510" s="T341">"I don't drink anymore, eh, I don't drink anymore.</ta>
            <ta e="T354" id="Seg_3511" s="T348">I won't drink, I left the alcohol", he said, in brief.</ta>
            <ta e="T357" id="Seg_3512" s="T354">Yeah, that's all.</ta>
            <ta e="T362" id="Seg_3513" s="T359">They were living rich and wealthy then.</ta>
            <ta e="T365" id="Seg_3514" s="T363">Very well.</ta>
            <ta e="T370" id="Seg_3515" s="T366">That's probably all, not much, is it?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-UoPP">
            <ta e="T26" id="Seg_3516" s="T22">– Erzähl du.</ta>
            <ta e="T50" id="Seg_3517" s="T32">– Äh, überhaupt, eine, eine, äh, wie war "Familie"?</ta>
            <ta e="T65" id="Seg_3518" s="T53">– Wenn es eine Familie ist, äh, sie haben wohl sehr gut gelebt, sie arbeiten, haben gearbeitet.</ta>
            <ta e="T72" id="Seg_3519" s="T65">Dann traf er seine Freunde und ging Schnaps trinken.</ta>
            <ta e="T75" id="Seg_3520" s="T73">Dort wird erzählt: </ta>
            <ta e="T79" id="Seg_3521" s="T75">"Deine Frau, so und so.</ta>
            <ta e="T85" id="Seg_3522" s="T80">So und so, mit diesem Mann.</ta>
            <ta e="T89" id="Seg_3523" s="T85">Sie unterhielt sich und so weiter."</ta>
            <ta e="T101" id="Seg_3524" s="T89">Wie sagt man, kurz gesagt, sie hat geflirtet ((…)).</ta>
            <ta e="T110" id="Seg_3525" s="T105">"Äh, deine Frau betrügt [dich] so und so."</ta>
            <ta e="T114" id="Seg_3526" s="T111">So wird geredet.</ta>
            <ta e="T122" id="Seg_3527" s="T115">Dann geht er und fragt seine Frau, dingst, äh, schreit.</ta>
            <ta e="T125" id="Seg_3528" s="T122">Dann schlug er seine Frau.</ta>
            <ta e="T135" id="Seg_3529" s="T126">Die Frau mit Kind, er schlägt die Frau, die das Kind in den Armen hält.</ta>
            <ta e="T141" id="Seg_3530" s="T136">Polizisten kamen, irgendwer hatte [sie] gerufen.</ta>
            <ta e="T145" id="Seg_3531" s="T141">Dann, mhm, nahmen die Polizisten [ihn?] mit.</ta>
            <ta e="T149" id="Seg_3532" s="T146">Sie brachten alle dorthin.</ta>
            <ta e="T172" id="Seg_3533" s="T150">Sie verhörten, sie verhörten, sie befragten sie, "wie war was, weswegen habt ihr euch gestritten", die Frau erzählt da.</ta>
            <ta e="T178" id="Seg_3534" s="T173">"So hat er geprügelt, so."</ta>
            <ta e="T189" id="Seg_3535" s="T179">Dann steckten sie ihn ins Gefängnis, ah, Gefängnis, sie steckten ihn ins Gefängnis, mhm.</ta>
            <ta e="T192" id="Seg_3536" s="T190">Sie steckten ihn ins Gefängnis.</ta>
            <ta e="T204" id="Seg_3537" s="T193">Dann sitzt er und erinnert sich, sein Kind, seine Frau erinnert er, so.</ta>
            <ta e="T214" id="Seg_3538" s="T205">Mhm, darauf, dass er herauskommt, dass er aus dem Gefängnis herauskommt, wartend, äh, er träumt.</ta>
            <ta e="T227" id="Seg_3539" s="T216">"Ich dingse", sagt er, mhm.</ta>
            <ta e="T239" id="Seg_3540" s="T233">– Nein, nein.</ta>
            <ta e="T254" id="Seg_3541" s="T240">Er dingst, äh, dingst, er erinnert sich an seine Familie, "so, wenn ich herauskomme".</ta>
            <ta e="T267" id="Seg_3542" s="T256">– Ja, er wünscht es sich, er erinnert sich, er wünscht es sich dann.</ta>
            <ta e="T272" id="Seg_3543" s="T269">– Ah.</ta>
            <ta e="T285" id="Seg_3544" s="T276">Äh, "sie freuen sich, wenn ich herauskomme, ich werden keinen Schnaps mehr trinken", denkt er.</ta>
            <ta e="T289" id="Seg_3545" s="T286">Er kam dann aus dem Gefängnis heraus.</ta>
            <ta e="T295" id="Seg_3546" s="T290">Er freut sich, er freut sich, die Sonne scheint.</ta>
            <ta e="T308" id="Seg_3547" s="T297">– Ja, sehr gut, dann kam er sich freuend nach Hause.</ta>
            <ta e="T310" id="Seg_3548" s="T309">– Jene.</ta>
            <ta e="T321" id="Seg_3549" s="T313">Ach, jene schauen, sie haben offenbar Angst.</ta>
            <ta e="T327" id="Seg_3550" s="T322">Dann ging er mit dem Kind spazieren.</ta>
            <ta e="T337" id="Seg_3551" s="T327">Er traf seine Freunde, "komm, trink Schnaps mit uns", "nein, nein, ich trinke nicht".</ta>
            <ta e="T340" id="Seg_3552" s="T337">Wie ein Märchen.</ta>
            <ta e="T347" id="Seg_3553" s="T341">"Ich trinke nicht mehr, äh, ich trinke nicht mehr.</ta>
            <ta e="T354" id="Seg_3554" s="T348">Ich werde nicht trinken, ich habe den Schnaps sein gelassen", sagte er, kurz.</ta>
            <ta e="T357" id="Seg_3555" s="T354">Ja, alles.</ta>
            <ta e="T362" id="Seg_3556" s="T359">Sie lebten glücklich und zufrieden dann.</ta>
            <ta e="T365" id="Seg_3557" s="T363">Sehr gut.</ta>
            <ta e="T370" id="Seg_3558" s="T366">Das ist wohl alles, wenig, oder?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-UoPP" />
         <annotation name="ltr" tierref="ltr-UoPP">
            <ta e="T26" id="Seg_3559" s="T22">Рассказывай, ты.</ta>
            <ta e="T50" id="Seg_3560" s="T32">Ээ вообще одна семья как это?</ta>
            <ta e="T65" id="Seg_3561" s="T53">Одна семья жила очень хорошо, оказывается, работают, работали.</ta>
            <ta e="T72" id="Seg_3562" s="T65">Потом встретил друзей, пошел с ними пить спиртное.</ta>
            <ta e="T75" id="Seg_3563" s="T73">Они рассказывают </ta>
            <ta e="T79" id="Seg_3564" s="T75">"твоя жена так и так".</ta>
            <ta e="T85" id="Seg_3565" s="T80">"Так и так, с таким человеком."</ta>
            <ta e="T89" id="Seg_3566" s="T85">Разговаривала и так далее.</ta>
            <ta e="T101" id="Seg_3567" s="T89">Как говорят? Короче заиргывала.</ta>
            <ta e="T110" id="Seg_3568" s="T105">Аа, жена твоя вот так изменяет.</ta>
            <ta e="T114" id="Seg_3569" s="T111">Вот так говорят.</ta>
            <ta e="T122" id="Seg_3570" s="T115">Потом пошел к жене спрашивает, делает ээ кричит.</ta>
            <ta e="T125" id="Seg_3571" s="T122">Потом ударил жену.</ta>
            <ta e="T135" id="Seg_3572" s="T126">Жена с ребенком с ребенком на руках женщину с ребенком на руках бьет.</ta>
            <ta e="T141" id="Seg_3573" s="T136">Милиционеры пришли, кто-то вызвал.</ta>
            <ta e="T145" id="Seg_3574" s="T141">Потом милиционеры увели.</ta>
            <ta e="T149" id="Seg_3575" s="T146">Всех увели туда.</ta>
            <ta e="T172" id="Seg_3576" s="T150">Допрашивают, допрашивают как это? Допрашивали, как что случилось, как вы подрались, тогда жена рассказывает.</ta>
            <ta e="T178" id="Seg_3577" s="T173">Вот так избил, вот так.</ta>
            <ta e="T189" id="Seg_3578" s="T179">Потом в тюрьму посадили, аа, в тюрьму, в тюрьму посадили, аһа.</ta>
            <ta e="T192" id="Seg_3579" s="T190">В тюрьму посадили.</ta>
            <ta e="T204" id="Seg_3580" s="T193">Потом сидит вспоминает, ребенка, жену, вспоминает сидит, вот так.</ta>
            <ta e="T214" id="Seg_3581" s="T205">Мм, когда его выпустят из тюрьмы, мечтает.</ta>
            <ta e="T227" id="Seg_3582" s="T216">Так делаю говорит.</ta>
            <ta e="T239" id="Seg_3583" s="T233">Нет, нет.</ta>
            <ta e="T254" id="Seg_3584" s="T240">Сидит, ээ, (рассказ-) делает сидит, вспоминает сидит семью вот так когда выйду.</ta>
            <ta e="T267" id="Seg_3585" s="T256">Ээ, мечтает сидит, вспоминает сидит, мечтает потом.</ta>
            <ta e="T285" id="Seg_3586" s="T276">Обрадуется когда я выйду "спиртного пить не буду" говорит "больше".</ta>
            <ta e="T289" id="Seg_3587" s="T286">Вышел потом из тюрьмы.</ta>
            <ta e="T295" id="Seg_3588" s="T290">Радуется радуется солнце светит.</ta>
            <ta e="T308" id="Seg_3589" s="T297">Ээ, очень хорошо потом радостный домой пришел.</ta>
            <ta e="T310" id="Seg_3590" s="T309">А те.</ta>
            <ta e="T321" id="Seg_3591" s="T313">Ээ, а те смотрят, боятся, наверное.</ta>
            <ta e="T327" id="Seg_3592" s="T322">Потом с ребенком пошел гулять.</ta>
            <ta e="T337" id="Seg_3593" s="T327">Друзей встретил "иди, вместе с нами выйпий" "нет, нет, не буду пить".</ta>
            <ta e="T347" id="Seg_3594" s="T341">"Пить не буду больше, ээ, не больше буду пить".</ta>
            <ta e="T354" id="Seg_3595" s="T348">"Пить не буду, спиртное бросил", сказал, короче.</ta>
            <ta e="T362" id="Seg_3596" s="T359">Богато-сытно жили потом.</ta>
            <ta e="T365" id="Seg_3597" s="T363">Очень хорошо.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-UoPP">
            <ta e="T337" id="Seg_3598" s="T327">[AAV:] Mobile phone starts ringing in the background.</ta>
            <ta e="T370" id="Seg_3599" s="T366">[AAV:] Mobile phone is switched off.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-ChGS"
                      id="tx-ChGS"
                      speaker="ChGS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-ChGS">
            <ts e="T40" id="Seg_3600" n="sc" s="T31">
               <ts e="T40" id="Seg_3602" n="HIAT:u" s="T31">
                  <nts id="Seg_3603" n="HIAT:ip">–</nts>
                  <nts id="Seg_3604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_3606" n="HIAT:w" s="T31">Eː</ts>
                  <nts id="Seg_3607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_3609" n="HIAT:w" s="T35">ikki͡emmit</ts>
                  <nts id="Seg_3610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_3612" n="HIAT:w" s="T38">paprobujduːbut</ts>
                  <nts id="Seg_3613" n="HIAT:ip">.</nts>
                  <nts id="Seg_3614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T49" id="Seg_3615" n="sc" s="T41">
               <ts e="T49" id="Seg_3617" n="HIAT:u" s="T41">
                  <nts id="Seg_3618" n="HIAT:ip">–</nts>
                  <nts id="Seg_3619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_3621" n="HIAT:w" s="T41">En</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_3624" n="HIAT:w" s="T43">kepseː</ts>
                  <nts id="Seg_3625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_3627" n="HIAT:w" s="T45">oččogo</ts>
                  <nts id="Seg_3628" n="HIAT:ip">,</nts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_3631" n="HIAT:w" s="T47">kepseː</ts>
                  <nts id="Seg_3632" n="HIAT:ip">.</nts>
                  <nts id="Seg_3633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T52" id="Seg_3634" n="sc" s="T51">
               <ts e="T52" id="Seg_3636" n="HIAT:u" s="T51">
                  <nts id="Seg_3637" n="HIAT:ip">–</nts>
                  <nts id="Seg_3638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_3640" n="HIAT:w" s="T51">Kergen</ts>
                  <nts id="Seg_3641" n="HIAT:ip">.</nts>
                  <nts id="Seg_3642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T95" id="Seg_3643" n="sc" s="T91">
               <ts e="T95" id="Seg_3645" n="HIAT:u" s="T91">
                  <nts id="Seg_3646" n="HIAT:ip">–</nts>
                  <nts id="Seg_3647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_3649" n="HIAT:w" s="T91">Körsülespit</ts>
                  <nts id="Seg_3650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_3652" n="HIAT:w" s="T93">du͡o</ts>
                  <nts id="Seg_3653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_3655" n="HIAT:w" s="T94">du</ts>
                  <nts id="Seg_3656" n="HIAT:ip">?</nts>
                  <nts id="Seg_3657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T104" id="Seg_3658" n="sc" s="T98">
               <ts e="T104" id="Seg_3660" n="HIAT:u" s="T98">
                  <nts id="Seg_3661" n="HIAT:ip">–</nts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_3664" n="HIAT:w" s="T98">Körsüleher</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_3667" n="HIAT:w" s="T99">du͡o</ts>
                  <nts id="Seg_3668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_3670" n="HIAT:w" s="T100">tu͡ok</ts>
                  <nts id="Seg_3671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_3673" n="HIAT:w" s="T102">du</ts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_3676" n="HIAT:w" s="T103">bu͡olbut</ts>
                  <nts id="Seg_3677" n="HIAT:ip">?</nts>
                  <nts id="Seg_3678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T165" id="Seg_3679" n="sc" s="T151">
               <ts e="T165" id="Seg_3681" n="HIAT:u" s="T151">
                  <nts id="Seg_3682" n="HIAT:ip">–</nts>
                  <nts id="Seg_3683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_3685" n="HIAT:w" s="T151">Huːt</ts>
                  <nts id="Seg_3686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_3688" n="HIAT:w" s="T153">bu͡olla</ts>
                  <nts id="Seg_3689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_3691" n="HIAT:w" s="T155">eni</ts>
                  <nts id="Seg_3692" n="HIAT:ip">,</nts>
                  <nts id="Seg_3693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_3695" n="HIAT:w" s="T157">huːt</ts>
                  <nts id="Seg_3696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_3698" n="HIAT:w" s="T159">bu͡olla</ts>
                  <nts id="Seg_3699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_3701" n="HIAT:w" s="T161">eni</ts>
                  <nts id="Seg_3702" n="HIAT:ip">,</nts>
                  <nts id="Seg_3703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_3705" n="HIAT:w" s="T163">mm</ts>
                  <nts id="Seg_3706" n="HIAT:ip">.</nts>
                  <nts id="Seg_3707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T187" id="Seg_3708" n="sc" s="T184">
               <ts e="T187" id="Seg_3710" n="HIAT:u" s="T184">
                  <nts id="Seg_3711" n="HIAT:ip">–</nts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_3714" n="HIAT:w" s="T184">Kaːjɨːga</ts>
                  <nts id="Seg_3715" n="HIAT:ip">.</nts>
                  <nts id="Seg_3716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T225" id="Seg_3717" n="sc" s="T215">
               <ts e="T225" id="Seg_3719" n="HIAT:u" s="T215">
                  <nts id="Seg_3720" n="HIAT:ip">–</nts>
                  <nts id="Seg_3721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_3723" n="HIAT:w" s="T215">Mʼečʼtajet</ts>
                  <nts id="Seg_3724" n="HIAT:ip">,</nts>
                  <nts id="Seg_3725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_3727" n="HIAT:w" s="T216">mʼečʼtajet</ts>
                  <nts id="Seg_3728" n="HIAT:ip">,</nts>
                  <nts id="Seg_3729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_3731" n="HIAT:w" s="T217">hu͡ok</ts>
                  <nts id="Seg_3732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_3734" n="HIAT:w" s="T218">eni</ts>
                  <nts id="Seg_3735" n="HIAT:ip">,</nts>
                  <nts id="Seg_3736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_3738" n="HIAT:w" s="T219">hu͡ok</ts>
                  <nts id="Seg_3739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_3741" n="HIAT:w" s="T221">eni</ts>
                  <nts id="Seg_3742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_3744" n="HIAT:w" s="T223">hakalɨː</ts>
                  <nts id="Seg_3745" n="HIAT:ip">.</nts>
                  <nts id="Seg_3746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T241" id="Seg_3747" n="sc" s="T229">
               <ts e="T241" id="Seg_3749" n="HIAT:u" s="T229">
                  <nts id="Seg_3750" n="HIAT:ip">–</nts>
                  <nts id="Seg_3751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_3753" n="HIAT:w" s="T229">Mʼečʼtajdaːbɨt</ts>
                  <nts id="Seg_3754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_3756" n="HIAT:w" s="T231">možna</ts>
                  <nts id="Seg_3757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_3759" n="HIAT:w" s="T232">da</ts>
                  <nts id="Seg_3760" n="HIAT:ip">,</nts>
                  <nts id="Seg_3761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_3763" n="HIAT:w" s="T234">iti</ts>
                  <nts id="Seg_3764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_3766" n="HIAT:w" s="T235">kördük</ts>
                  <nts id="Seg_3767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_3769" n="HIAT:w" s="T238">di͡ekke</ts>
                  <nts id="Seg_3770" n="HIAT:ip">.</nts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T262" id="Seg_3772" n="sc" s="T254">
               <ts e="T262" id="Seg_3774" n="HIAT:u" s="T254">
                  <nts id="Seg_3775" n="HIAT:ip">–</nts>
                  <nts id="Seg_3776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3777" n="HIAT:ip">"</nts>
                  <ts e="T255" id="Seg_3779" n="HIAT:w" s="T254">Hanɨːr</ts>
                  <nts id="Seg_3780" n="HIAT:ip">"</nts>
                  <nts id="Seg_3781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_3783" n="HIAT:w" s="T255">navʼerna</ts>
                  <nts id="Seg_3784" n="HIAT:ip">,</nts>
                  <nts id="Seg_3785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_3787" n="HIAT:w" s="T257">hanɨːr</ts>
                  <nts id="Seg_3788" n="HIAT:ip">,</nts>
                  <nts id="Seg_3789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_3791" n="HIAT:w" s="T259">hanɨːr</ts>
                  <nts id="Seg_3792" n="HIAT:ip">.</nts>
                  <nts id="Seg_3793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T275" id="Seg_3794" n="sc" s="T266">
               <ts e="T275" id="Seg_3796" n="HIAT:u" s="T266">
                  <nts id="Seg_3797" n="HIAT:ip">–</nts>
                  <nts id="Seg_3798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_3800" n="HIAT:w" s="T266">Hanɨːr</ts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_3803" n="HIAT:w" s="T268">tagɨstagɨna</ts>
                  <nts id="Seg_3804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_3806" n="HIAT:w" s="T270">ogoto</ts>
                  <nts id="Seg_3807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_3809" n="HIAT:w" s="T271">dʼaktara</ts>
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_3812" n="HIAT:w" s="T273">kergettere</ts>
                  <nts id="Seg_3813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_3815" n="HIAT:w" s="T274">ü͡örellerin</ts>
                  <nts id="Seg_3816" n="HIAT:ip">.</nts>
                  <nts id="Seg_3817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T299" id="Seg_3818" n="sc" s="T295">
               <ts e="T299" id="Seg_3820" n="HIAT:u" s="T295">
                  <nts id="Seg_3821" n="HIAT:ip">–</nts>
                  <nts id="Seg_3822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_3824" n="HIAT:w" s="T295">Svaboːdattan</ts>
                  <nts id="Seg_3825" n="HIAT:ip">.</nts>
                  <nts id="Seg_3826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T303" id="Seg_3827" n="sc" s="T300">
               <ts e="T303" id="Seg_3829" n="HIAT:u" s="T300">
                  <nts id="Seg_3830" n="HIAT:ip">–</nts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_3833" n="HIAT:w" s="T300">Svabodnɨj</ts>
                  <nts id="Seg_3834" n="HIAT:ip">.</nts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T314" id="Seg_3836" n="sc" s="T311">
               <ts e="T314" id="Seg_3838" n="HIAT:u" s="T311">
                  <nts id="Seg_3839" n="HIAT:ip">–</nts>
                  <nts id="Seg_3840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_3842" n="HIAT:w" s="T311">Olus</ts>
                  <nts id="Seg_3843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_3845" n="HIAT:w" s="T312">ü͡örbetter</ts>
                  <nts id="Seg_3846" n="HIAT:ip">.</nts>
                  <nts id="Seg_3847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T374" id="Seg_3848" n="sc" s="T373">
               <ts e="T374" id="Seg_3850" n="HIAT:u" s="T373">
                  <nts id="Seg_3851" n="HIAT:ip">–</nts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_3854" n="HIAT:w" s="T373">Narmalʼna</ts>
                  <nts id="Seg_3855" n="HIAT:ip">.</nts>
                  <nts id="Seg_3856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-ChGS">
            <ts e="T40" id="Seg_3857" n="sc" s="T31">
               <ts e="T35" id="Seg_3859" n="e" s="T31">– Eː </ts>
               <ts e="T38" id="Seg_3861" n="e" s="T35">ikki͡emmit </ts>
               <ts e="T40" id="Seg_3863" n="e" s="T38">paprobujduːbut. </ts>
            </ts>
            <ts e="T49" id="Seg_3864" n="sc" s="T41">
               <ts e="T43" id="Seg_3866" n="e" s="T41">– En </ts>
               <ts e="T45" id="Seg_3868" n="e" s="T43">kepseː </ts>
               <ts e="T47" id="Seg_3870" n="e" s="T45">oččogo, </ts>
               <ts e="T49" id="Seg_3872" n="e" s="T47">kepseː. </ts>
            </ts>
            <ts e="T52" id="Seg_3873" n="sc" s="T51">
               <ts e="T52" id="Seg_3875" n="e" s="T51">– Kergen. </ts>
            </ts>
            <ts e="T95" id="Seg_3876" n="sc" s="T91">
               <ts e="T93" id="Seg_3878" n="e" s="T91">– Körsülespit </ts>
               <ts e="T94" id="Seg_3880" n="e" s="T93">du͡o </ts>
               <ts e="T95" id="Seg_3882" n="e" s="T94">du? </ts>
            </ts>
            <ts e="T104" id="Seg_3883" n="sc" s="T98">
               <ts e="T99" id="Seg_3885" n="e" s="T98">– Körsüleher </ts>
               <ts e="T100" id="Seg_3887" n="e" s="T99">du͡o </ts>
               <ts e="T102" id="Seg_3889" n="e" s="T100">tu͡ok </ts>
               <ts e="T103" id="Seg_3891" n="e" s="T102">du </ts>
               <ts e="T104" id="Seg_3893" n="e" s="T103">bu͡olbut? </ts>
            </ts>
            <ts e="T165" id="Seg_3894" n="sc" s="T151">
               <ts e="T153" id="Seg_3896" n="e" s="T151">– Huːt </ts>
               <ts e="T155" id="Seg_3898" n="e" s="T153">bu͡olla </ts>
               <ts e="T157" id="Seg_3900" n="e" s="T155">eni, </ts>
               <ts e="T159" id="Seg_3902" n="e" s="T157">huːt </ts>
               <ts e="T161" id="Seg_3904" n="e" s="T159">bu͡olla </ts>
               <ts e="T163" id="Seg_3906" n="e" s="T161">eni, </ts>
               <ts e="T165" id="Seg_3908" n="e" s="T163">mm. </ts>
            </ts>
            <ts e="T187" id="Seg_3909" n="sc" s="T184">
               <ts e="T187" id="Seg_3911" n="e" s="T184">– Kaːjɨːga. </ts>
            </ts>
            <ts e="T225" id="Seg_3912" n="sc" s="T215">
               <ts e="T216" id="Seg_3914" n="e" s="T215">– Mʼečʼtajet, </ts>
               <ts e="T217" id="Seg_3916" n="e" s="T216">mʼečʼtajet, </ts>
               <ts e="T218" id="Seg_3918" n="e" s="T217">hu͡ok </ts>
               <ts e="T219" id="Seg_3920" n="e" s="T218">eni, </ts>
               <ts e="T221" id="Seg_3922" n="e" s="T219">hu͡ok </ts>
               <ts e="T223" id="Seg_3924" n="e" s="T221">eni </ts>
               <ts e="T225" id="Seg_3926" n="e" s="T223">hakalɨː. </ts>
            </ts>
            <ts e="T241" id="Seg_3927" n="sc" s="T229">
               <ts e="T231" id="Seg_3929" n="e" s="T229">– Mʼečʼtajdaːbɨt </ts>
               <ts e="T232" id="Seg_3931" n="e" s="T231">možna </ts>
               <ts e="T234" id="Seg_3933" n="e" s="T232">da, </ts>
               <ts e="T235" id="Seg_3935" n="e" s="T234">iti </ts>
               <ts e="T238" id="Seg_3937" n="e" s="T235">kördük </ts>
               <ts e="T241" id="Seg_3939" n="e" s="T238">di͡ekke. </ts>
            </ts>
            <ts e="T262" id="Seg_3940" n="sc" s="T254">
               <ts e="T255" id="Seg_3942" n="e" s="T254">– "Hanɨːr" </ts>
               <ts e="T257" id="Seg_3944" n="e" s="T255">navʼerna, </ts>
               <ts e="T259" id="Seg_3946" n="e" s="T257">hanɨːr, </ts>
               <ts e="T262" id="Seg_3948" n="e" s="T259">hanɨːr. </ts>
            </ts>
            <ts e="T275" id="Seg_3949" n="sc" s="T266">
               <ts e="T268" id="Seg_3951" n="e" s="T266">– Hanɨːr </ts>
               <ts e="T270" id="Seg_3953" n="e" s="T268">tagɨstagɨna </ts>
               <ts e="T271" id="Seg_3955" n="e" s="T270">ogoto </ts>
               <ts e="T273" id="Seg_3957" n="e" s="T271">dʼaktara </ts>
               <ts e="T274" id="Seg_3959" n="e" s="T273">kergettere </ts>
               <ts e="T275" id="Seg_3961" n="e" s="T274">ü͡örellerin. </ts>
            </ts>
            <ts e="T299" id="Seg_3962" n="sc" s="T295">
               <ts e="T299" id="Seg_3964" n="e" s="T295">– Svaboːdattan. </ts>
            </ts>
            <ts e="T303" id="Seg_3965" n="sc" s="T300">
               <ts e="T303" id="Seg_3967" n="e" s="T300">– Svabodnɨj. </ts>
            </ts>
            <ts e="T314" id="Seg_3968" n="sc" s="T311">
               <ts e="T312" id="Seg_3970" n="e" s="T311">– Olus </ts>
               <ts e="T314" id="Seg_3972" n="e" s="T312">ü͡örbetter. </ts>
            </ts>
            <ts e="T374" id="Seg_3973" n="sc" s="T373">
               <ts e="T374" id="Seg_3975" n="e" s="T373">– Narmalʼna. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-ChGS">
            <ta e="T40" id="Seg_3976" s="T31">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.001 (001.006)</ta>
            <ta e="T49" id="Seg_3977" s="T41">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.002 (001.008)</ta>
            <ta e="T52" id="Seg_3978" s="T51">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.003 (001.009)</ta>
            <ta e="T95" id="Seg_3979" s="T91">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.004 (001.016)</ta>
            <ta e="T104" id="Seg_3980" s="T98">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.005 (001.017)</ta>
            <ta e="T165" id="Seg_3981" s="T151">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.006 (001.027)</ta>
            <ta e="T187" id="Seg_3982" s="T184">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.007 (001.030)</ta>
            <ta e="T225" id="Seg_3983" s="T215">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.008 (001.034)</ta>
            <ta e="T241" id="Seg_3984" s="T229">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.009 (001.037)</ta>
            <ta e="T262" id="Seg_3985" s="T254">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.010 (001.041)</ta>
            <ta e="T275" id="Seg_3986" s="T266">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.011 (001.043)</ta>
            <ta e="T299" id="Seg_3987" s="T295">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.012 (001.048)</ta>
            <ta e="T303" id="Seg_3988" s="T300">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.013 (001.050)</ta>
            <ta e="T314" id="Seg_3989" s="T311">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.014 (001.052)</ta>
            <ta e="T374" id="Seg_3990" s="T373">UoPP_ChGS_20170724_SocCogRetell1_nar.ChGS.015 (001.065)</ta>
         </annotation>
         <annotation name="st" tierref="st-ChGS">
            <ta e="T40" id="Seg_3991" s="T31">Ээ иккиэммит попробовайдиэкпит.</ta>
            <ta e="T49" id="Seg_3992" s="T41">Эн кэпсээ оччого.</ta>
            <ta e="T52" id="Seg_3993" s="T51">Кэргэн.</ta>
            <ta e="T95" id="Seg_3994" s="T91">Көрсүлэспит дуо ду?</ta>
            <ta e="T104" id="Seg_3995" s="T98">Көрсүлэһэр дуо туок ду буолбут?</ta>
            <ta e="T165" id="Seg_3996" s="T151">Һуут буолла эни, һуут буолла эни… мм.</ta>
            <ta e="T187" id="Seg_3997" s="T184">Кайыыга.</ta>
            <ta e="T225" id="Seg_3998" s="T215">Мечтает, мечтает, һуок эни һакалыы.</ta>
            <ta e="T241" id="Seg_3999" s="T229">Мечтайдаабыт можно да, ити көрдүк диэккэ.</ta>
            <ta e="T262" id="Seg_4000" s="T254">Һаныыр, наверно, һаныыр, һаныыр.</ta>
            <ta e="T275" id="Seg_4001" s="T266">Һаныыр тагыстагына огото дьактара кэргэттэрэ үөрэллэрин.</ta>
            <ta e="T299" id="Seg_4002" s="T295">Свободаттан.</ta>
            <ta e="T303" id="Seg_4003" s="T300">Свободный(?).</ta>
            <ta e="T314" id="Seg_4004" s="T311">Олус үөрбэттэр.</ta>
            <ta e="T374" id="Seg_4005" s="T373">Нормально.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-ChGS">
            <ta e="T40" id="Seg_4006" s="T31">– Eː ikki͡emmit paprobujduːbut. </ta>
            <ta e="T49" id="Seg_4007" s="T41">– En kepseː oččogo, kepseː. </ta>
            <ta e="T52" id="Seg_4008" s="T51">– Kergen. </ta>
            <ta e="T95" id="Seg_4009" s="T91">– Körsülespit du͡o du? </ta>
            <ta e="T104" id="Seg_4010" s="T98">– Körsüleher du͡o tu͡ok du bu͡olbut? </ta>
            <ta e="T165" id="Seg_4011" s="T151">– Huːt bu͡olla eni, huːt bu͡olla eni, mm. </ta>
            <ta e="T187" id="Seg_4012" s="T184">– Kaːjɨːga. </ta>
            <ta e="T225" id="Seg_4013" s="T215">– Mʼečʼtajet, mʼečʼtajet, hu͡ok eni, hu͡ok eni hakalɨː. </ta>
            <ta e="T241" id="Seg_4014" s="T229">– Mʼečʼtajdaːbɨt možna da, iti kördük di͡ekke. </ta>
            <ta e="T262" id="Seg_4015" s="T254">– "Hanɨːr" navʼerna, hanɨːr, hanɨːr. </ta>
            <ta e="T275" id="Seg_4016" s="T266">– Hanɨːr tagɨstagɨna ogoto dʼaktara kergettere ü͡örellerin. </ta>
            <ta e="T299" id="Seg_4017" s="T295">– Svaboːdattan. </ta>
            <ta e="T303" id="Seg_4018" s="T300">– Svabodnɨj. </ta>
            <ta e="T314" id="Seg_4019" s="T311">– Olus ü͡örbetter. </ta>
            <ta e="T374" id="Seg_4020" s="T373">– Narmalʼna. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-ChGS">
            <ta e="T35" id="Seg_4021" s="T31">eː</ta>
            <ta e="T38" id="Seg_4022" s="T35">ikk-i͡em-mit</ta>
            <ta e="T40" id="Seg_4023" s="T38">paprobuj-d-uː-but</ta>
            <ta e="T43" id="Seg_4024" s="T41">en</ta>
            <ta e="T45" id="Seg_4025" s="T43">kepseː</ta>
            <ta e="T47" id="Seg_4026" s="T45">oččogo</ta>
            <ta e="T49" id="Seg_4027" s="T47">kepseː</ta>
            <ta e="T52" id="Seg_4028" s="T51">kergen</ta>
            <ta e="T93" id="Seg_4029" s="T91">körs-ü-l-e-s-pit</ta>
            <ta e="T94" id="Seg_4030" s="T93">du͡o</ta>
            <ta e="T95" id="Seg_4031" s="T94">du</ta>
            <ta e="T99" id="Seg_4032" s="T98">körs-ü-l-e-h-er</ta>
            <ta e="T100" id="Seg_4033" s="T99">du͡o</ta>
            <ta e="T102" id="Seg_4034" s="T100">tu͡ok</ta>
            <ta e="T103" id="Seg_4035" s="T102">du</ta>
            <ta e="T104" id="Seg_4036" s="T103">bu͡ol-but</ta>
            <ta e="T153" id="Seg_4037" s="T151">huːt</ta>
            <ta e="T155" id="Seg_4038" s="T153">bu͡ol-l-a</ta>
            <ta e="T157" id="Seg_4039" s="T155">eni</ta>
            <ta e="T159" id="Seg_4040" s="T157">huːt</ta>
            <ta e="T161" id="Seg_4041" s="T159">bu͡ol-l-a</ta>
            <ta e="T163" id="Seg_4042" s="T161">eni</ta>
            <ta e="T165" id="Seg_4043" s="T163">mm</ta>
            <ta e="T187" id="Seg_4044" s="T184">kaːjɨː-ga</ta>
            <ta e="T218" id="Seg_4045" s="T217">hu͡ok</ta>
            <ta e="T219" id="Seg_4046" s="T218">eni</ta>
            <ta e="T221" id="Seg_4047" s="T219">hu͡ok</ta>
            <ta e="T223" id="Seg_4048" s="T221">eni</ta>
            <ta e="T225" id="Seg_4049" s="T223">haka-lɨː</ta>
            <ta e="T231" id="Seg_4050" s="T229">mʼečʼtaj-daː-bɨt</ta>
            <ta e="T232" id="Seg_4051" s="T231">možna</ta>
            <ta e="T234" id="Seg_4052" s="T232">da</ta>
            <ta e="T235" id="Seg_4053" s="T234">iti</ta>
            <ta e="T238" id="Seg_4054" s="T235">kördük</ta>
            <ta e="T241" id="Seg_4055" s="T238">d-i͡ek-ke</ta>
            <ta e="T255" id="Seg_4056" s="T254">hanɨː-r</ta>
            <ta e="T257" id="Seg_4057" s="T255">navʼerna</ta>
            <ta e="T259" id="Seg_4058" s="T257">hanɨː-r</ta>
            <ta e="T262" id="Seg_4059" s="T259">hanɨː-r</ta>
            <ta e="T268" id="Seg_4060" s="T266">hanɨː-r</ta>
            <ta e="T270" id="Seg_4061" s="T268">tagɨs-tag-ɨna</ta>
            <ta e="T271" id="Seg_4062" s="T270">ogo-to</ta>
            <ta e="T273" id="Seg_4063" s="T271">dʼaktar-a</ta>
            <ta e="T274" id="Seg_4064" s="T273">kerget-ter-e</ta>
            <ta e="T275" id="Seg_4065" s="T274">ü͡ör-el-leri-n</ta>
            <ta e="T299" id="Seg_4066" s="T295">svaboːda-ttan</ta>
            <ta e="T312" id="Seg_4067" s="T311">olus</ta>
            <ta e="T314" id="Seg_4068" s="T312">ü͡ör-bet-ter</ta>
         </annotation>
         <annotation name="mp" tierref="mp-ChGS">
            <ta e="T35" id="Seg_4069" s="T31">eː</ta>
            <ta e="T38" id="Seg_4070" s="T35">ikki-IAn-BIt</ta>
            <ta e="T40" id="Seg_4071" s="T38">paprobuj-LAː-A-BIt</ta>
            <ta e="T43" id="Seg_4072" s="T41">en</ta>
            <ta e="T45" id="Seg_4073" s="T43">kepseː</ta>
            <ta e="T47" id="Seg_4074" s="T45">oččogo</ta>
            <ta e="T49" id="Seg_4075" s="T47">kepseː</ta>
            <ta e="T52" id="Seg_4076" s="T51">kergen</ta>
            <ta e="T93" id="Seg_4077" s="T91">körüs-I-TAː-A-s-BIT</ta>
            <ta e="T94" id="Seg_4078" s="T93">du͡o</ta>
            <ta e="T95" id="Seg_4079" s="T94">du͡o</ta>
            <ta e="T99" id="Seg_4080" s="T98">körüs-I-TAː-A-s-Ar</ta>
            <ta e="T100" id="Seg_4081" s="T99">du͡o</ta>
            <ta e="T102" id="Seg_4082" s="T100">tu͡ok</ta>
            <ta e="T103" id="Seg_4083" s="T102">du͡o</ta>
            <ta e="T104" id="Seg_4084" s="T103">bu͡ol-BIT</ta>
            <ta e="T153" id="Seg_4085" s="T151">huːt</ta>
            <ta e="T155" id="Seg_4086" s="T153">bu͡ol-TI-tA</ta>
            <ta e="T157" id="Seg_4087" s="T155">eni</ta>
            <ta e="T159" id="Seg_4088" s="T157">huːt</ta>
            <ta e="T161" id="Seg_4089" s="T159">bu͡ol-TI-tA</ta>
            <ta e="T163" id="Seg_4090" s="T161">eni</ta>
            <ta e="T165" id="Seg_4091" s="T163">mm</ta>
            <ta e="T187" id="Seg_4092" s="T184">kaːjɨː-GA</ta>
            <ta e="T218" id="Seg_4093" s="T217">hu͡ok</ta>
            <ta e="T219" id="Seg_4094" s="T218">eni</ta>
            <ta e="T221" id="Seg_4095" s="T219">hu͡ok</ta>
            <ta e="T223" id="Seg_4096" s="T221">eni</ta>
            <ta e="T225" id="Seg_4097" s="T223">haka-LIː</ta>
            <ta e="T231" id="Seg_4098" s="T229">mʼečʼtaj-LAː-BIT</ta>
            <ta e="T232" id="Seg_4099" s="T231">možno</ta>
            <ta e="T234" id="Seg_4100" s="T232">da</ta>
            <ta e="T235" id="Seg_4101" s="T234">iti</ta>
            <ta e="T238" id="Seg_4102" s="T235">kördük</ta>
            <ta e="T241" id="Seg_4103" s="T238">di͡e-IAK-GA</ta>
            <ta e="T255" id="Seg_4104" s="T254">hanaː-Ar</ta>
            <ta e="T257" id="Seg_4105" s="T255">navʼernaje</ta>
            <ta e="T259" id="Seg_4106" s="T257">hanaː-Ar</ta>
            <ta e="T262" id="Seg_4107" s="T259">hanaː-Ar</ta>
            <ta e="T268" id="Seg_4108" s="T266">hanaː-Ar</ta>
            <ta e="T270" id="Seg_4109" s="T268">tagɨs-TAK-InA</ta>
            <ta e="T271" id="Seg_4110" s="T270">ogo-tA</ta>
            <ta e="T273" id="Seg_4111" s="T271">dʼaktar-tA</ta>
            <ta e="T274" id="Seg_4112" s="T273">kergen-LAr-tA</ta>
            <ta e="T275" id="Seg_4113" s="T274">ü͡ör-Ar-LArI-n</ta>
            <ta e="T299" id="Seg_4114" s="T295">svaboːda-ttAn</ta>
            <ta e="T312" id="Seg_4115" s="T311">olus</ta>
            <ta e="T314" id="Seg_4116" s="T312">ü͡ör-BAT-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge-ChGS">
            <ta e="T35" id="Seg_4117" s="T31">AFFIRM</ta>
            <ta e="T38" id="Seg_4118" s="T35">two-COLL-1PL.[NOM]</ta>
            <ta e="T40" id="Seg_4119" s="T38">try-VBZ-PRS-1PL</ta>
            <ta e="T43" id="Seg_4120" s="T41">2SG.[NOM]</ta>
            <ta e="T45" id="Seg_4121" s="T43">tell.[IMP.2SG]</ta>
            <ta e="T47" id="Seg_4122" s="T45">then</ta>
            <ta e="T49" id="Seg_4123" s="T47">tell.[IMP.2SG]</ta>
            <ta e="T52" id="Seg_4124" s="T51">family</ta>
            <ta e="T93" id="Seg_4125" s="T91">meet-EP-ITER-EP-RECP/COLL-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_4126" s="T93">Q</ta>
            <ta e="T95" id="Seg_4127" s="T94">Q</ta>
            <ta e="T99" id="Seg_4128" s="T98">meet-EP-ITER-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_4129" s="T99">Q</ta>
            <ta e="T102" id="Seg_4130" s="T100">what.[NOM]</ta>
            <ta e="T103" id="Seg_4131" s="T102">Q</ta>
            <ta e="T104" id="Seg_4132" s="T103">become-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_4133" s="T151">court.[NOM]</ta>
            <ta e="T155" id="Seg_4134" s="T153">be-PST1-3SG</ta>
            <ta e="T157" id="Seg_4135" s="T155">apparently</ta>
            <ta e="T159" id="Seg_4136" s="T157">court.[NOM]</ta>
            <ta e="T161" id="Seg_4137" s="T159">be-PST1-3SG</ta>
            <ta e="T163" id="Seg_4138" s="T161">apparently</ta>
            <ta e="T165" id="Seg_4139" s="T163">mm</ta>
            <ta e="T187" id="Seg_4140" s="T184">prison-DAT/LOC</ta>
            <ta e="T218" id="Seg_4141" s="T217">NEG.EX</ta>
            <ta e="T219" id="Seg_4142" s="T218">apparently</ta>
            <ta e="T221" id="Seg_4143" s="T219">NEG.EX</ta>
            <ta e="T223" id="Seg_4144" s="T221">apparently</ta>
            <ta e="T225" id="Seg_4145" s="T223">Dolgan-SIM</ta>
            <ta e="T231" id="Seg_4146" s="T229">dream-VBZ-PST2.[3SG]</ta>
            <ta e="T232" id="Seg_4147" s="T231">it.is.possible</ta>
            <ta e="T234" id="Seg_4148" s="T232">EMPH</ta>
            <ta e="T235" id="Seg_4149" s="T234">that.[NOM]</ta>
            <ta e="T238" id="Seg_4150" s="T235">similar</ta>
            <ta e="T241" id="Seg_4151" s="T238">say-PTCP.FUT-DAT/LOC</ta>
            <ta e="T255" id="Seg_4152" s="T254">wish-PRS.[3SG]</ta>
            <ta e="T257" id="Seg_4153" s="T255">probably</ta>
            <ta e="T259" id="Seg_4154" s="T257">wish-PRS.[3SG]</ta>
            <ta e="T262" id="Seg_4155" s="T259">wish-PRS.[3SG]</ta>
            <ta e="T268" id="Seg_4156" s="T266">wish-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_4157" s="T268">go.out-TEMP-3SG</ta>
            <ta e="T271" id="Seg_4158" s="T270">child-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_4159" s="T271">woman-3SG.[NOM]</ta>
            <ta e="T274" id="Seg_4160" s="T273">family-PL-3SG.[NOM]</ta>
            <ta e="T275" id="Seg_4161" s="T274">be.happy-PTCP.PRS-3PL-ACC</ta>
            <ta e="T299" id="Seg_4162" s="T295">freedom-ABL</ta>
            <ta e="T312" id="Seg_4163" s="T311">very</ta>
            <ta e="T314" id="Seg_4164" s="T312">be.happy-NEG-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg-ChGS">
            <ta e="T35" id="Seg_4165" s="T31">AFFIRM</ta>
            <ta e="T38" id="Seg_4166" s="T35">zwei-COLL-1PL.[NOM]</ta>
            <ta e="T40" id="Seg_4167" s="T38">versuchen-VBZ-PRS-1PL</ta>
            <ta e="T43" id="Seg_4168" s="T41">2SG.[NOM]</ta>
            <ta e="T45" id="Seg_4169" s="T43">erzählen.[IMP.2SG]</ta>
            <ta e="T47" id="Seg_4170" s="T45">dann</ta>
            <ta e="T49" id="Seg_4171" s="T47">erzählen.[IMP.2SG]</ta>
            <ta e="T52" id="Seg_4172" s="T51">Familie</ta>
            <ta e="T93" id="Seg_4173" s="T91">treffen-EP-ITER-EP-RECP/COLL-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_4174" s="T93">Q</ta>
            <ta e="T95" id="Seg_4175" s="T94">Q</ta>
            <ta e="T99" id="Seg_4176" s="T98">treffen-EP-ITER-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_4177" s="T99">Q</ta>
            <ta e="T102" id="Seg_4178" s="T100">was.[NOM]</ta>
            <ta e="T103" id="Seg_4179" s="T102">Q</ta>
            <ta e="T104" id="Seg_4180" s="T103">werden-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_4181" s="T151">Gericht.[NOM]</ta>
            <ta e="T155" id="Seg_4182" s="T153">sein-PST1-3SG</ta>
            <ta e="T157" id="Seg_4183" s="T155">offenbar</ta>
            <ta e="T159" id="Seg_4184" s="T157">Gericht.[NOM]</ta>
            <ta e="T161" id="Seg_4185" s="T159">sein-PST1-3SG</ta>
            <ta e="T163" id="Seg_4186" s="T161">offenbar</ta>
            <ta e="T165" id="Seg_4187" s="T163">mm</ta>
            <ta e="T187" id="Seg_4188" s="T184">Gefängnis-DAT/LOC</ta>
            <ta e="T218" id="Seg_4189" s="T217">NEG.EX</ta>
            <ta e="T219" id="Seg_4190" s="T218">offenbar</ta>
            <ta e="T221" id="Seg_4191" s="T219">NEG.EX</ta>
            <ta e="T223" id="Seg_4192" s="T221">offenbar</ta>
            <ta e="T225" id="Seg_4193" s="T223">dolganisch-SIM</ta>
            <ta e="T231" id="Seg_4194" s="T229">träumen-VBZ-PST2.[3SG]</ta>
            <ta e="T232" id="Seg_4195" s="T231">es.ist.möglich</ta>
            <ta e="T234" id="Seg_4196" s="T232">EMPH</ta>
            <ta e="T235" id="Seg_4197" s="T234">dieses.[NOM]</ta>
            <ta e="T238" id="Seg_4198" s="T235">ähnlich</ta>
            <ta e="T241" id="Seg_4199" s="T238">sagen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T255" id="Seg_4200" s="T254">wünschen-PRS.[3SG]</ta>
            <ta e="T257" id="Seg_4201" s="T255">wahrscheinlich</ta>
            <ta e="T259" id="Seg_4202" s="T257">wünschen-PRS.[3SG]</ta>
            <ta e="T262" id="Seg_4203" s="T259">wünschen-PRS.[3SG]</ta>
            <ta e="T268" id="Seg_4204" s="T266">wünschen-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_4205" s="T268">hinausgehen-TEMP-3SG</ta>
            <ta e="T271" id="Seg_4206" s="T270">Kind-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_4207" s="T271">Frau-3SG.[NOM]</ta>
            <ta e="T274" id="Seg_4208" s="T273">Familie-PL-3SG.[NOM]</ta>
            <ta e="T275" id="Seg_4209" s="T274">sich.freuen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T299" id="Seg_4210" s="T295">Freiheit-ABL</ta>
            <ta e="T312" id="Seg_4211" s="T311">sehr</ta>
            <ta e="T314" id="Seg_4212" s="T312">sich.freuen-NEG-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr-ChGS">
            <ta e="T35" id="Seg_4213" s="T31">AFFIRM</ta>
            <ta e="T38" id="Seg_4214" s="T35">два-COLL-1PL.[NOM]</ta>
            <ta e="T40" id="Seg_4215" s="T38">попробовать-VBZ-PRS-1PL</ta>
            <ta e="T43" id="Seg_4216" s="T41">2SG.[NOM]</ta>
            <ta e="T45" id="Seg_4217" s="T43">рассказывать.[IMP.2SG]</ta>
            <ta e="T47" id="Seg_4218" s="T45">тогда</ta>
            <ta e="T49" id="Seg_4219" s="T47">рассказывать.[IMP.2SG]</ta>
            <ta e="T52" id="Seg_4220" s="T51">семья</ta>
            <ta e="T93" id="Seg_4221" s="T91">встречать-EP-ITER-EP-RECP/COLL-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_4222" s="T93">Q</ta>
            <ta e="T95" id="Seg_4223" s="T94">Q</ta>
            <ta e="T99" id="Seg_4224" s="T98">встречать-EP-ITER-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_4225" s="T99">Q</ta>
            <ta e="T102" id="Seg_4226" s="T100">что.[NOM]</ta>
            <ta e="T103" id="Seg_4227" s="T102">Q</ta>
            <ta e="T104" id="Seg_4228" s="T103">становиться-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_4229" s="T151">суд.[NOM]</ta>
            <ta e="T155" id="Seg_4230" s="T153">быть-PST1-3SG</ta>
            <ta e="T157" id="Seg_4231" s="T155">очевидно</ta>
            <ta e="T159" id="Seg_4232" s="T157">суд.[NOM]</ta>
            <ta e="T161" id="Seg_4233" s="T159">быть-PST1-3SG</ta>
            <ta e="T163" id="Seg_4234" s="T161">очевидно</ta>
            <ta e="T165" id="Seg_4235" s="T163">мм</ta>
            <ta e="T187" id="Seg_4236" s="T184">тюрьма-DAT/LOC</ta>
            <ta e="T218" id="Seg_4237" s="T217">NEG.EX</ta>
            <ta e="T219" id="Seg_4238" s="T218">очевидно</ta>
            <ta e="T221" id="Seg_4239" s="T219">NEG.EX</ta>
            <ta e="T223" id="Seg_4240" s="T221">очевидно</ta>
            <ta e="T225" id="Seg_4241" s="T223">долганский-SIM</ta>
            <ta e="T231" id="Seg_4242" s="T229">мечтать-VBZ-PST2.[3SG]</ta>
            <ta e="T232" id="Seg_4243" s="T231">можно</ta>
            <ta e="T234" id="Seg_4244" s="T232">EMPH</ta>
            <ta e="T235" id="Seg_4245" s="T234">тот.[NOM]</ta>
            <ta e="T238" id="Seg_4246" s="T235">подобно</ta>
            <ta e="T241" id="Seg_4247" s="T238">говорить-PTCP.FUT-DAT/LOC</ta>
            <ta e="T255" id="Seg_4248" s="T254">желать-PRS.[3SG]</ta>
            <ta e="T257" id="Seg_4249" s="T255">наверное</ta>
            <ta e="T259" id="Seg_4250" s="T257">желать-PRS.[3SG]</ta>
            <ta e="T262" id="Seg_4251" s="T259">желать-PRS.[3SG]</ta>
            <ta e="T268" id="Seg_4252" s="T266">желать-PRS.[3SG]</ta>
            <ta e="T270" id="Seg_4253" s="T268">выйти-TEMP-3SG</ta>
            <ta e="T271" id="Seg_4254" s="T270">ребенок-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_4255" s="T271">жена-3SG.[NOM]</ta>
            <ta e="T274" id="Seg_4256" s="T273">семья-PL-3SG.[NOM]</ta>
            <ta e="T275" id="Seg_4257" s="T274">радоваться-PTCP.PRS-3PL-ACC</ta>
            <ta e="T299" id="Seg_4258" s="T295">свобода-ABL</ta>
            <ta e="T312" id="Seg_4259" s="T311">очень</ta>
            <ta e="T314" id="Seg_4260" s="T312">радоваться-NEG-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc-ChGS">
            <ta e="T35" id="Seg_4261" s="T31">ptcl</ta>
            <ta e="T38" id="Seg_4262" s="T35">cardnum-cardnum&gt;collnum-n:(poss).[n:case]</ta>
            <ta e="T40" id="Seg_4263" s="T38">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T43" id="Seg_4264" s="T41">pers.[pro:case]</ta>
            <ta e="T45" id="Seg_4265" s="T43">v.[v:mood.pn]</ta>
            <ta e="T47" id="Seg_4266" s="T45">adv</ta>
            <ta e="T49" id="Seg_4267" s="T47">v.[v:mood.pn]</ta>
            <ta e="T52" id="Seg_4268" s="T51">n</ta>
            <ta e="T93" id="Seg_4269" s="T91">v-v:(ins)-v&gt;v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T94" id="Seg_4270" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_4271" s="T94">ptcl</ta>
            <ta e="T99" id="Seg_4272" s="T98">v-v:(ins)-v&gt;v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T100" id="Seg_4273" s="T99">ptcl</ta>
            <ta e="T102" id="Seg_4274" s="T100">que.[pro:case]</ta>
            <ta e="T103" id="Seg_4275" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_4276" s="T103">v-v:tense.[v:pred.pn]</ta>
            <ta e="T153" id="Seg_4277" s="T151">n.[n:case]</ta>
            <ta e="T155" id="Seg_4278" s="T153">v-v:tense-v:poss.pn</ta>
            <ta e="T157" id="Seg_4279" s="T155">ptcl</ta>
            <ta e="T159" id="Seg_4280" s="T157">n.[n:case]</ta>
            <ta e="T161" id="Seg_4281" s="T159">v-v:tense-v:poss.pn</ta>
            <ta e="T163" id="Seg_4282" s="T161">ptcl</ta>
            <ta e="T165" id="Seg_4283" s="T163">interj</ta>
            <ta e="T187" id="Seg_4284" s="T184">n-n:case</ta>
            <ta e="T218" id="Seg_4285" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_4286" s="T218">ptcl</ta>
            <ta e="T221" id="Seg_4287" s="T219">ptcl</ta>
            <ta e="T223" id="Seg_4288" s="T221">ptcl</ta>
            <ta e="T225" id="Seg_4289" s="T223">adj-adj&gt;adv</ta>
            <ta e="T231" id="Seg_4290" s="T229">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T232" id="Seg_4291" s="T231">ptcl</ta>
            <ta e="T234" id="Seg_4292" s="T232">ptcl</ta>
            <ta e="T235" id="Seg_4293" s="T234">dempro.[pro:case]</ta>
            <ta e="T238" id="Seg_4294" s="T235">post</ta>
            <ta e="T241" id="Seg_4295" s="T238">v-v:ptcp-v:(case)</ta>
            <ta e="T255" id="Seg_4296" s="T254">v-v:tense.[v:pred.pn]</ta>
            <ta e="T257" id="Seg_4297" s="T255">adv</ta>
            <ta e="T259" id="Seg_4298" s="T257">v-v:tense.[v:pred.pn]</ta>
            <ta e="T262" id="Seg_4299" s="T259">v-v:tense.[v:pred.pn]</ta>
            <ta e="T268" id="Seg_4300" s="T266">v-v:tense.[v:pred.pn]</ta>
            <ta e="T270" id="Seg_4301" s="T268">v-v:mood-v:temp.pn</ta>
            <ta e="T271" id="Seg_4302" s="T270">n-n:(poss).[n:case]</ta>
            <ta e="T273" id="Seg_4303" s="T271">n-n:(poss).[n:case]</ta>
            <ta e="T274" id="Seg_4304" s="T273">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T275" id="Seg_4305" s="T274">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T299" id="Seg_4306" s="T295">n-n:case</ta>
            <ta e="T312" id="Seg_4307" s="T311">adv</ta>
            <ta e="T314" id="Seg_4308" s="T312">v-v:(neg)-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-ChGS">
            <ta e="T35" id="Seg_4309" s="T31">ptcl</ta>
            <ta e="T38" id="Seg_4310" s="T35">collnum</ta>
            <ta e="T40" id="Seg_4311" s="T38">v</ta>
            <ta e="T43" id="Seg_4312" s="T41">pers</ta>
            <ta e="T45" id="Seg_4313" s="T43">v</ta>
            <ta e="T47" id="Seg_4314" s="T45">adv</ta>
            <ta e="T49" id="Seg_4315" s="T47">v</ta>
            <ta e="T52" id="Seg_4316" s="T51">n</ta>
            <ta e="T93" id="Seg_4317" s="T91">v</ta>
            <ta e="T94" id="Seg_4318" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_4319" s="T94">ptcl</ta>
            <ta e="T99" id="Seg_4320" s="T98">v</ta>
            <ta e="T100" id="Seg_4321" s="T99">ptcl</ta>
            <ta e="T102" id="Seg_4322" s="T100">que</ta>
            <ta e="T103" id="Seg_4323" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_4324" s="T103">cop</ta>
            <ta e="T153" id="Seg_4325" s="T151">n</ta>
            <ta e="T155" id="Seg_4326" s="T153">cop</ta>
            <ta e="T157" id="Seg_4327" s="T155">ptcl</ta>
            <ta e="T159" id="Seg_4328" s="T157">n</ta>
            <ta e="T161" id="Seg_4329" s="T159">cop</ta>
            <ta e="T163" id="Seg_4330" s="T161">ptcl</ta>
            <ta e="T165" id="Seg_4331" s="T163">interj</ta>
            <ta e="T187" id="Seg_4332" s="T184">n</ta>
            <ta e="T218" id="Seg_4333" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_4334" s="T218">ptcl</ta>
            <ta e="T221" id="Seg_4335" s="T219">ptcl</ta>
            <ta e="T223" id="Seg_4336" s="T221">ptcl</ta>
            <ta e="T225" id="Seg_4337" s="T223">adv</ta>
            <ta e="T231" id="Seg_4338" s="T229">v</ta>
            <ta e="T232" id="Seg_4339" s="T231">ptcl</ta>
            <ta e="T234" id="Seg_4340" s="T232">ptcl</ta>
            <ta e="T235" id="Seg_4341" s="T234">dempro</ta>
            <ta e="T238" id="Seg_4342" s="T235">post</ta>
            <ta e="T241" id="Seg_4343" s="T238">v</ta>
            <ta e="T255" id="Seg_4344" s="T254">v</ta>
            <ta e="T257" id="Seg_4345" s="T255">adv</ta>
            <ta e="T259" id="Seg_4346" s="T257">v</ta>
            <ta e="T262" id="Seg_4347" s="T259">v</ta>
            <ta e="T268" id="Seg_4348" s="T266">v</ta>
            <ta e="T270" id="Seg_4349" s="T268">v</ta>
            <ta e="T271" id="Seg_4350" s="T270">n</ta>
            <ta e="T273" id="Seg_4351" s="T271">n</ta>
            <ta e="T274" id="Seg_4352" s="T273">n</ta>
            <ta e="T275" id="Seg_4353" s="T274">v</ta>
            <ta e="T299" id="Seg_4354" s="T295">n</ta>
            <ta e="T312" id="Seg_4355" s="T311">adv</ta>
            <ta e="T314" id="Seg_4356" s="T312">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-ChGS" />
         <annotation name="SyF" tierref="SyF-ChGS" />
         <annotation name="IST" tierref="IST-ChGS" />
         <annotation name="Top" tierref="Top-ChGS" />
         <annotation name="Foc" tierref="Foc-ChGS" />
         <annotation name="BOR" tierref="BOR-ChGS">
            <ta e="T40" id="Seg_4357" s="T38">RUS:cult</ta>
            <ta e="T153" id="Seg_4358" s="T151">RUS:cult</ta>
            <ta e="T159" id="Seg_4359" s="T157">RUS:cult</ta>
            <ta e="T231" id="Seg_4360" s="T229">RUS:core</ta>
            <ta e="T232" id="Seg_4361" s="T231">RUS:mod</ta>
            <ta e="T257" id="Seg_4362" s="T255">RUS:mod</ta>
            <ta e="T299" id="Seg_4363" s="T295">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-ChGS">
            <ta e="T153" id="Seg_4364" s="T151">Csub Csub</ta>
            <ta e="T159" id="Seg_4365" s="T157">Csub Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-ChGS">
            <ta e="T153" id="Seg_4366" s="T151">dir:bare</ta>
            <ta e="T159" id="Seg_4367" s="T157">dir:bare</ta>
            <ta e="T231" id="Seg_4368" s="T229">indir:infl</ta>
            <ta e="T232" id="Seg_4369" s="T231">dir:bare</ta>
            <ta e="T257" id="Seg_4370" s="T255">dir:bare</ta>
            <ta e="T299" id="Seg_4371" s="T295">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS-ChGS">
            <ta e="T217" id="Seg_4372" s="T215">RUS:int.ins</ta>
            <ta e="T303" id="Seg_4373" s="T300">RUS:ext</ta>
            <ta e="T374" id="Seg_4374" s="T373">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe-ChGS">
            <ta e="T40" id="Seg_4375" s="T31">– Ah, we'll try it together.</ta>
            <ta e="T49" id="Seg_4376" s="T41">– You tell then, tell.</ta>
            <ta e="T52" id="Seg_4377" s="T51">– "Kergen".</ta>
            <ta e="T95" id="Seg_4378" s="T91">– Did she betray [him]?</ta>
            <ta e="T104" id="Seg_4379" s="T98">– Does she betray [him] or what's up?</ta>
            <ta e="T165" id="Seg_4380" s="T151">– A court was it probably, it was a court, mhm.</ta>
            <ta e="T187" id="Seg_4381" s="T184">– In prison.</ta>
            <ta e="T225" id="Seg_4382" s="T215">– "He is dreaming", that probably doesn't exist in Dolgan.</ta>
            <ta e="T241" id="Seg_4383" s="T229">– He was dreaming, yes, one can say so.</ta>
            <ta e="T262" id="Seg_4384" s="T254">– "Hanɨːr" probably, he wishes, he wishes.</ta>
            <ta e="T275" id="Seg_4385" s="T266">– He wishes that his child, his wife, his family will be happy when he'll come out.</ta>
            <ta e="T299" id="Seg_4386" s="T295">– About the freedom.</ta>
            <ta e="T303" id="Seg_4387" s="T300">– Free.</ta>
            <ta e="T314" id="Seg_4388" s="T311">– They aren't really happy.</ta>
            <ta e="T374" id="Seg_4389" s="T373">– Good.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-ChGS">
            <ta e="T40" id="Seg_4390" s="T31">– Ah, wir versuchen es zu zweit.</ta>
            <ta e="T49" id="Seg_4391" s="T41">– Erzähl du dann, erzähl.</ta>
            <ta e="T52" id="Seg_4392" s="T51">– "Kergen".</ta>
            <ta e="T95" id="Seg_4393" s="T91">– Sie hat [ihn] betrogen?</ta>
            <ta e="T104" id="Seg_4394" s="T98">– Betrügt sie [ihn] oder was war?</ta>
            <ta e="T165" id="Seg_4395" s="T151">– Ein Gericht war das wahrscheinlich, ein Gericht war das, mhm.</ta>
            <ta e="T187" id="Seg_4396" s="T184">– Ins Gefängnis.</ta>
            <ta e="T225" id="Seg_4397" s="T215">– "Er träumt", das gibt es wahrscheinlich nicht auf Dolganisch.</ta>
            <ta e="T241" id="Seg_4398" s="T229">– Er träumte, ja, so kann man sagen.</ta>
            <ta e="T262" id="Seg_4399" s="T254">– "Hanɨːr" wahrscheinlich, er wünscht es sich, er wünscht es sich.</ta>
            <ta e="T275" id="Seg_4400" s="T266">– Er wünscht sich, wenn er herauskommt, dass sich sein Kind, seine Frau, seine Familie freuen.</ta>
            <ta e="T299" id="Seg_4401" s="T295">– Über die Freiheit.</ta>
            <ta e="T303" id="Seg_4402" s="T300">– Frei.</ta>
            <ta e="T314" id="Seg_4403" s="T311">– Sie freuen sich nicht wirklich.</ta>
            <ta e="T374" id="Seg_4404" s="T373">– Gut.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-ChGS" />
         <annotation name="ltr" tierref="ltr-ChGS">
            <ta e="T40" id="Seg_4405" s="T31">Аа, вдвоем попробуем.</ta>
            <ta e="T49" id="Seg_4406" s="T41">Ты рассказывай тогда.</ta>
            <ta e="T52" id="Seg_4407" s="T51">Семья.</ta>
            <ta e="T95" id="Seg_4408" s="T91">Изменяла?</ta>
            <ta e="T104" id="Seg_4409" s="T98">Изменяла или что там случилось?</ta>
            <ta e="T165" id="Seg_4410" s="T151">Суд был наверно, суд был наверное… мм.</ta>
            <ta e="T187" id="Seg_4411" s="T184">В тюрьму.</ta>
            <ta e="T225" id="Seg_4412" s="T215">Мечтает, мечтает, нет наверно по-долгански.</ta>
            <ta e="T241" id="Seg_4413" s="T229">Мечтайдаабыт можно да, так сказать.</ta>
            <ta e="T262" id="Seg_4414" s="T254">"һаныыр", наверно, "һаныыр", "һаныыр".</ta>
            <ta e="T275" id="Seg_4415" s="T266">Мечтает как выйдет ребенок жена семья обрадуется.</ta>
            <ta e="T299" id="Seg_4416" s="T295">От свободы.</ta>
            <ta e="T314" id="Seg_4417" s="T311">Сильно не радуются.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-ChGS" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-DCh"
                          name="ref"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-DCh"
                          name="st"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-DCh"
                          name="ts"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-DCh"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-DCh"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-DCh"
                          name="mb"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-DCh"
                          name="mp"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-DCh"
                          name="ge"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-DCh"
                          name="gg"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-DCh"
                          name="gr"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-DCh"
                          name="mc"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-DCh"
                          name="ps"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-DCh"
                          name="SeR"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-DCh"
                          name="SyF"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-DCh"
                          name="IST"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-DCh"
                          name="Top"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-DCh"
                          name="Foc"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-DCh"
                          name="BOR"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-DCh"
                          name="BOR-Phon"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-DCh"
                          name="BOR-Morph"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-DCh"
                          name="CS"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-DCh"
                          name="fe"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-DCh"
                          name="fg"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-DCh"
                          name="fr"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-DCh"
                          name="ltr"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-DCh"
                          name="nt"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-UoPP"
                          name="ref"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-UoPP"
                          name="st"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-UoPP"
                          name="ts"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-UoPP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-UoPP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-UoPP"
                          name="mb"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-UoPP"
                          name="mp"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-UoPP"
                          name="ge"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-UoPP"
                          name="gg"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-UoPP"
                          name="gr"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-UoPP"
                          name="mc"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-UoPP"
                          name="ps"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-UoPP"
                          name="SeR"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-UoPP"
                          name="SyF"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-UoPP"
                          name="IST"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-UoPP"
                          name="Top"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-UoPP"
                          name="Foc"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-UoPP"
                          name="BOR"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-UoPP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-UoPP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-UoPP"
                          name="CS"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-UoPP"
                          name="fe"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-UoPP"
                          name="fg"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-UoPP"
                          name="fr"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-UoPP"
                          name="ltr"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-UoPP"
                          name="nt"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-ChGS"
                          name="ref"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-ChGS"
                          name="st"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-ChGS"
                          name="ts"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-ChGS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-ChGS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-ChGS"
                          name="mb"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-ChGS"
                          name="mp"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-ChGS"
                          name="ge"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-ChGS"
                          name="gg"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-ChGS"
                          name="gr"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-ChGS"
                          name="mc"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-ChGS"
                          name="ps"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-ChGS"
                          name="SeR"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-ChGS"
                          name="SyF"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-ChGS"
                          name="IST"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-ChGS"
                          name="Top"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-ChGS"
                          name="Foc"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-ChGS"
                          name="BOR"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-ChGS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-ChGS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-ChGS"
                          name="CS"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-ChGS"
                          name="fe"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-ChGS"
                          name="fg"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-ChGS"
                          name="fr"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-ChGS"
                          name="ltr"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-ChGS"
                          name="nt"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
