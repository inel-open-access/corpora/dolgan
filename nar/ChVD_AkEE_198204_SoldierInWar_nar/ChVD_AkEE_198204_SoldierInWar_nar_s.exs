<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID84969F57-E176-6419-D2E2-E2E6513833A7">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>ChVD_AkEE_198204_SoldierInWar_nar</transcription-name>
         <referenced-file url="ChVD_AkEE_198204_SoldierInWar_nar.wav" />
         <referenced-file url="ChVD_AkEE_198204_SoldierInWar_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\ChVD_AkEE_198204_SoldierInWar_nar\ChVD_AkEE_198204_SoldierInWar_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1445</ud-information>
            <ud-information attribute-name="# HIAT:w">982</ud-information>
            <ud-information attribute-name="# e">970</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">128</ud-information>
            <ud-information attribute-name="# sc">4</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChVD">
            <abbreviation>ChVD</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="AkEE">
            <abbreviation>AkEE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.66" type="appl" />
         <tli id="T2" time="1.319" type="appl" />
         <tli id="T3" time="1.979" type="appl" />
         <tli id="T4" time="2.638" type="appl" />
         <tli id="T5" time="3.298" type="appl" />
         <tli id="T6" time="3.759" type="appl" />
         <tli id="T7" time="4.22" type="appl" />
         <tli id="T10" time="5.604" type="appl" />
         <tli id="T11" time="6.065" type="appl" />
         <tli id="T12" time="6.527" type="appl" />
         <tli id="T13" time="6.988" type="appl" />
         <tli id="T14" time="7.2690011181070435" />
         <tli id="T15" time="8.184" type="appl" />
         <tli id="T16" time="9.146665485279545" />
         <tli id="T17" time="9.721" type="appl" />
         <tli id="T18" time="10.522" type="appl" />
         <tli id="T19" time="11.322" type="appl" />
         <tli id="T9" time="11.7225" type="intp" />
         <tli id="T20" time="12.123" type="appl" />
         <tli id="T21" time="12.924" type="appl" />
         <tli id="T22" time="13.725" type="appl" />
         <tli id="T23" time="14.526" type="appl" />
         <tli id="T24" time="15.327" type="appl" />
         <tli id="T25" time="16.127" type="appl" />
         <tli id="T26" time="16.928" type="appl" />
         <tli id="T27" time="17.902332201363738" />
         <tli id="T28" time="18.656" type="appl" />
         <tli id="T29" time="19.582" type="appl" />
         <tli id="T30" time="20.508" type="appl" />
         <tli id="T31" time="21.435" type="appl" />
         <tli id="T32" time="22.362" type="appl" />
         <tli id="T33" time="23.14800082934906" />
         <tli id="T34" time="23.989" type="appl" />
         <tli id="T35" time="24.69" type="appl" />
         <tli id="T36" time="25.391" type="appl" />
         <tli id="T37" time="26.092" type="appl" />
         <tli id="T38" time="26.793" type="appl" />
         <tli id="T39" time="27.494" type="appl" />
         <tli id="T40" time="28.195" type="appl" />
         <tli id="T41" time="28.624" type="appl" />
         <tli id="T42" time="29.053" type="appl" />
         <tli id="T43" time="29.481" type="appl" />
         <tli id="T44" time="29.91" type="appl" />
         <tli id="T45" time="30.339" type="appl" />
         <tli id="T46" time="30.768" type="appl" />
         <tli id="T47" time="31.197" type="appl" />
         <tli id="T48" time="31.625" type="appl" />
         <tli id="T49" time="32.054" type="appl" />
         <tli id="T50" time="32.483" type="appl" />
         <tli id="T54" time="32.6975" type="intp" />
         <tli id="T51" time="32.912" type="appl" />
         <tli id="T52" time="33.341" type="appl" />
         <tli id="T53" time="33.769" type="appl" />
         <tli id="T55" time="34.686998186423544" />
         <tli id="T56" time="35.493" type="appl" />
         <tli id="T57" time="36.359" type="appl" />
         <tli id="T58" time="37.225" type="appl" />
         <tli id="T59" time="38.091" type="appl" />
         <tli id="T60" time="38.521" type="appl" />
         <tli id="T61" time="38.952" type="appl" />
         <tli id="T62" time="39.382" type="appl" />
         <tli id="T63" time="39.813" type="appl" />
         <tli id="T64" time="40.243" type="appl" />
         <tli id="T65" time="40.674" type="appl" />
         <tli id="T66" time="41.104" type="appl" />
         <tli id="T67" time="41.535" type="appl" />
         <tli id="T68" time="41.965" type="appl" />
         <tli id="T69" time="42.396" type="appl" />
         <tli id="T70" time="42.826" type="appl" />
         <tli id="T71" time="43.418" type="appl" />
         <tli id="T72" time="44.01" type="appl" />
         <tli id="T73" time="44.602" type="appl" />
         <tli id="T74" time="45.194" type="appl" />
         <tli id="T75" time="45.786" type="appl" />
         <tli id="T76" time="46.378" type="appl" />
         <tli id="T77" time="46.97" type="appl" />
         <tli id="T78" time="47.562" type="appl" />
         <tli id="T80" time="48.746" type="appl" />
         <tli id="T481" time="48.944" type="intp" />
         <tli id="T81" time="49.142" type="appl" />
         <tli id="T82" time="49.539" type="appl" />
         <tli id="T83" time="49.936" type="appl" />
         <tli id="T84" time="50.332" type="appl" />
         <tli id="T85" time="50.728" type="appl" />
         <tli id="T86" time="51.125" type="appl" />
         <tli id="T87" time="51.522" type="appl" />
         <tli id="T90" time="52.864333718080026" />
         <tli id="T91" time="53.306" type="appl" />
         <tli id="T92" time="53.901" type="appl" />
         <tli id="T93" time="54.496" type="appl" />
         <tli id="T94" time="55.091" type="appl" />
         <tli id="T95" time="55.686" type="appl" />
         <tli id="T96" time="56.281" type="appl" />
         <tli id="T97" time="56.876" type="appl" />
         <tli id="T98" time="57.471" type="appl" />
         <tli id="T99" time="58.066" type="appl" />
         <tli id="T100" time="58.694330101955" />
         <tli id="T101" time="59.356" type="appl" />
         <tli id="T102" time="60.051" type="appl" />
         <tli id="T103" time="60.746" type="appl" />
         <tli id="T104" time="61.441" type="appl" />
         <tli id="T105" time="62.136" type="appl" />
         <tli id="T106" time="62.831" type="appl" />
         <tli id="T107" time="63.526" type="appl" />
         <tli id="T108" time="64.221" type="appl" />
         <tli id="T109" time="65.0693331746426" />
         <tli id="T110" time="65.301" type="appl" />
         <tli id="T111" time="65.686" type="appl" />
         <tli id="T113" time="66.456" type="appl" />
         <tli id="T114" time="66.841" type="appl" />
         <tli id="T115" time="67.226" type="appl" />
         <tli id="T116" time="67.581" type="appl" />
         <tli id="T117" time="67.935" type="appl" />
         <tli id="T118" time="68.29" type="appl" />
         <tli id="T119" time="68.644" type="appl" />
         <tli id="T120" time="68.999" type="appl" />
         <tli id="T121" time="69.354" type="appl" />
         <tli id="T122" time="69.708" type="appl" />
         <tli id="T123" time="70.063" type="appl" />
         <tli id="T124" time="70.417" type="appl" />
         <tli id="T125" time="70.772" type="appl" />
         <tli id="T126" time="71.126" type="appl" />
         <tli id="T127" time="71.52100248118084" />
         <tli id="T128" time="71.876" type="appl" />
         <tli id="T129" time="72.27" type="appl" />
         <tli id="T130" time="72.665" type="appl" />
         <tli id="T131" time="73.06" type="appl" />
         <tli id="T132" time="73.454" type="appl" />
         <tli id="T133" time="73.849" type="appl" />
         <tli id="T134" time="74.244" type="appl" />
         <tli id="T135" time="74.638" type="appl" />
         <tli id="T136" time="75.033" type="appl" />
         <tli id="T138" time="75.822" type="appl" />
         <tli id="T139" time="76.217" type="appl" />
         <tli id="T140" time="76.612" type="appl" />
         <tli id="T141" time="77.006" type="appl" />
         <tli id="T142" time="77.37433532185251" />
         <tli id="T143" time="77.799" type="appl" />
         <tli id="T144" time="78.197" type="appl" />
         <tli id="T145" time="78.595" type="appl" />
         <tli id="T146" time="78.993" type="appl" />
         <tli id="T147" time="79.39" type="appl" />
         <tli id="T148" time="79.788" type="appl" />
         <tli id="T150" time="80.584" type="appl" />
         <tli id="T151" time="80.982" type="appl" />
         <tli id="T152" time="81.48000224764878" />
         <tli id="T153" time="81.838" type="appl" />
         <tli id="T154" time="82.297" type="appl" />
         <tli id="T155" time="82.755" type="appl" />
         <tli id="T156" time="83.213" type="appl" />
         <tli id="T157" time="83.671" type="appl" />
         <tli id="T158" time="84.13" type="appl" />
         <tli id="T160" time="85.046" type="appl" />
         <tli id="T161" time="85.505" type="appl" />
         <tli id="T162" time="85.963" type="appl" />
         <tli id="T163" time="86.421" type="appl" />
         <tli id="T164" time="86.879" type="appl" />
         <tli id="T165" time="87.338" type="appl" />
         <tli id="T166" time="88.1093333116217" />
         <tli id="T167" time="88.295" type="appl" />
         <tli id="T168" time="88.794" type="appl" />
         <tli id="T169" time="89.293" type="appl" />
         <tli id="T170" time="89.792" type="appl" />
         <tli id="T171" time="90.291" type="appl" />
         <tli id="T172" time="90.79" type="appl" />
         <tli id="T173" time="91.289" type="appl" />
         <tli id="T174" time="91.76799755261953" />
         <tli id="T175" time="92.471" type="appl" />
         <tli id="T176" time="93.154" type="appl" />
         <tli id="T177" time="93.837" type="appl" />
         <tli id="T178" time="94.52" type="appl" />
         <tli id="T179" time="95.00299882080559" />
         <tli id="T180" time="95.623" type="appl" />
         <tli id="T181" time="96.043" type="appl" />
         <tli id="T182" time="96.463" type="appl" />
         <tli id="T183" time="96.883" type="appl" />
         <tli id="T184" time="97.303" type="appl" />
         <tli id="T185" time="97.722" type="appl" />
         <tli id="T186" time="98.142" type="appl" />
         <tli id="T187" time="98.562" type="appl" />
         <tli id="T188" time="98.982" type="appl" />
         <tli id="T189" time="99.402" type="appl" />
         <tli id="T190" time="99.64866920845164" />
         <tli id="T191" time="100.286" type="appl" />
         <tli id="T192" time="100.75" type="appl" />
         <tli id="T193" time="101.214" type="appl" />
         <tli id="T194" time="101.635" type="appl" />
         <tli id="T195" time="102.056" type="appl" />
         <tli id="T196" time="102.47699959539776" />
         <tli id="T197" time="102.8246669034899" />
         <tli id="T198" time="103.432" type="appl" />
         <tli id="T199" time="103.966" type="appl" />
         <tli id="T200" time="104.5" type="appl" />
         <tli id="T201" time="105.033" type="appl" />
         <tli id="T202" time="105.567" type="appl" />
         <tli id="T203" time="106.101" type="appl" />
         <tli id="T204" time="106.4949977452911" />
         <tli id="T205" time="107.221" type="appl" />
         <tli id="T206" time="107.806" type="appl" />
         <tli id="T207" time="108.392" type="appl" />
         <tli id="T208" time="109.05133096986911" />
         <tli id="T209" time="109.426" type="appl" />
         <tli id="T211" time="110.321" type="appl" />
         <tli id="T212" time="110.769" type="appl" />
         <tli id="T213" time="111.216" type="appl" />
         <tli id="T214" time="112.03733624791825" />
         <tli id="T215" time="112.203" type="appl" />
         <tli id="T216" time="112.742" type="appl" />
         <tli id="T217" time="113.281" type="appl" />
         <tli id="T218" time="113.82" type="appl" />
         <tli id="T219" time="114.359" type="appl" />
         <tli id="T220" time="114.898" type="appl" />
         <tli id="T221" time="115.437" type="appl" />
         <tli id="T222" time="115.975" type="appl" />
         <tli id="T223" time="116.514" type="appl" />
         <tli id="T224" time="117.053" type="appl" />
         <tli id="T225" time="117.592" type="appl" />
         <tli id="T226" time="118.131" type="appl" />
         <tli id="T227" time="118.67" type="appl" />
         <tli id="T228" time="119.209" type="appl" />
         <tli id="T229" time="119.748" type="appl" />
         <tli id="T230" time="120.164" type="appl" />
         <tli id="T232" time="120.996" type="appl" />
         <tli id="T233" time="121.413" type="appl" />
         <tli id="T234" time="121.829" type="appl" />
         <tli id="T235" time="122.245" type="appl" />
         <tli id="T236" time="122.661" type="appl" />
         <tli id="T237" time="123.077" type="appl" />
         <tli id="T238" time="123.494" type="appl" />
         <tli id="T240" time="124.326" type="appl" />
         <tli id="T241" time="124.75533736564313" />
         <tli id="T243" time="125.382" type="appl" />
         <tli id="T244" time="125.701" type="appl" />
         <tli id="T245" time="126.021" type="appl" />
         <tli id="T246" time="126.341" type="appl" />
         <tli id="T247" time="126.661" type="appl" />
         <tli id="T248" time="126.981" type="appl" />
         <tli id="T249" time="127.3" type="appl" />
         <tli id="T250" time="127.62" type="appl" />
         <tli id="T251" time="127.8799942046907" />
         <tli id="T252" time="128.306" type="appl" />
         <tli id="T253" time="128.672" type="appl" />
         <tli id="T254" time="129.038" type="appl" />
         <tli id="T255" time="129.404" type="appl" />
         <tli id="T256" time="129.77" type="appl" />
         <tli id="T257" time="130.136" type="appl" />
         <tli id="T258" time="130.502" type="appl" />
         <tli id="T259" time="130.868" type="appl" />
         <tli id="T260" time="131.234" type="appl" />
         <tli id="T261" time="131.6" type="appl" />
         <tli id="T262" time="132.08599910569978" />
         <tli id="T263" time="132.412" type="appl" />
         <tli id="T264" time="132.857" type="appl" />
         <tli id="T265" time="133.302" type="appl" />
         <tli id="T266" time="133.748" type="appl" />
         <tli id="T267" time="134.194" type="appl" />
         <tli id="T268" time="135.39923560929546" />
         <tli id="T270" time="135.684" type="appl" />
         <tli id="T271" time="136.206" type="appl" />
         <tli id="T272" time="136.729" type="appl" />
         <tli id="T273" time="137.252" type="appl" />
         <tli id="T274" time="137.774" type="appl" />
         <tli id="T275" time="138.296" type="appl" />
         <tli id="T276" time="138.819" type="appl" />
         <tli id="T277" time="139.342" type="appl" />
         <tli id="T278" time="140.0706675702556" />
         <tli id="T279" time="140.341" type="appl" />
         <tli id="T280" time="140.819" type="appl" />
         <tli id="T281" time="141.296" type="appl" />
         <tli id="T282" time="141.774" type="appl" />
         <tli id="T283" time="142.251" type="appl" />
         <tli id="T284" time="142.728" type="appl" />
         <tli id="T285" time="143.206" type="appl" />
         <tli id="T286" time="143.683" type="appl" />
         <tli id="T287" time="144.161" type="appl" />
         <tli id="T288" time="144.638" type="appl" />
         <tli id="T289" time="145.125" type="appl" />
         <tli id="T290" time="145.611" type="appl" />
         <tli id="T291" time="146.098" type="appl" />
         <tli id="T292" time="146.584" type="appl" />
         <tli id="T293" time="147.071" type="appl" />
         <tli id="T294" time="147.558" type="appl" />
         <tli id="T295" time="148.044" type="appl" />
         <tli id="T296" time="148.59099707325953" />
         <tli id="T297" time="149.019" type="appl" />
         <tli id="T298" time="149.507" type="appl" />
         <tli id="T299" time="149.995" type="appl" />
         <tli id="T300" time="150.36967192765854" />
         <tli id="T301" time="151.034" type="appl" />
         <tli id="T302" time="151.585" type="appl" />
         <tli id="T303" time="152.135" type="appl" />
         <tli id="T304" time="152.686" type="appl" />
         <tli id="T305" time="153.0770003935493" />
         <tli id="T306" time="153.669" type="appl" />
         <tli id="T307" time="154.102" type="appl" />
         <tli id="T308" time="154.534" type="appl" />
         <tli id="T309" time="154.966" type="appl" />
         <tli id="T310" time="155.344" type="appl" />
         <tli id="T312" time="156.1" type="appl" />
         <tli id="T313" time="156.477" type="appl" />
         <tli id="T314" time="156.855" type="appl" />
         <tli id="T315" time="157.233" type="appl" />
         <tli id="T316" time="157.611" type="appl" />
         <tli id="T317" time="157.989" type="appl" />
         <tli id="T318" time="158.366" type="appl" />
         <tli id="T319" time="158.744" type="appl" />
         <tli id="T320" time="159.122" type="appl" />
         <tli id="T321" time="159.36666540634263" />
         <tli id="T322" time="160.056" type="appl" />
         <tli id="T323" time="160.611" type="appl" />
         <tli id="T324" time="161.034" type="appl" />
         <tli id="T325" time="161.458" type="appl" />
         <tli id="T326" time="161.881" type="appl" />
         <tli id="T327" time="162.305" type="appl" />
         <tli id="T328" time="162.64133441933612" />
         <tli id="T329" time="163.126" type="appl" />
         <tli id="T330" time="163.524" type="appl" />
         <tli id="T331" time="163.922" type="appl" />
         <tli id="T332" time="164.321" type="appl" />
         <tli id="T333" time="164.719" type="appl" />
         <tli id="T334" time="165.117" type="appl" />
         <tli id="T335" time="165.515" type="appl" />
         <tli id="T336" time="166.057" type="appl" />
         <tli id="T337" time="166.6" type="appl" />
         <tli id="T338" time="167.142" type="appl" />
         <tli id="T339" time="167.685" type="appl" />
         <tli id="T340" time="168.228" type="appl" />
         <tli id="T341" time="168.77" type="appl" />
         <tli id="T342" time="169.312" type="appl" />
         <tli id="T343" time="169.855" type="appl" />
         <tli id="T344" time="170.398" type="appl" />
         <tli id="T345" time="170.75333289422235" />
         <tli id="T346" time="171.32" type="appl" />
         <tli id="T347" time="171.7" type="appl" />
         <tli id="T348" time="172.08" type="appl" />
         <tli id="T349" time="172.459" type="appl" />
         <tli id="T350" time="172.839" type="appl" />
         <tli id="T351" time="173.219" type="appl" />
         <tli id="T352" time="173.599" type="appl" />
         <tli id="T353" time="173.979" type="appl" />
         <tli id="T354" time="174.358" type="appl" />
         <tli id="T355" time="174.738" type="appl" />
         <tli id="T357" time="176.03900617917554" />
         <tli id="T358" time="176.187" type="appl" />
         <tli id="T359" time="176.876" type="appl" />
         <tli id="T360" time="177.565" type="appl" />
         <tli id="T361" time="178.254" type="appl" />
         <tli id="T362" time="178.943" type="appl" />
         <tli id="T363" time="179.412" type="appl" />
         <tli id="T364" time="179.88" type="appl" />
         <tli id="T365" time="180.348" type="appl" />
         <tli id="T366" time="180.817" type="appl" />
         <tli id="T367" time="181.285" type="appl" />
         <tli id="T368" time="181.60733411841377" />
         <tli id="T369" time="182.24" type="appl" />
         <tli id="T370" time="182.726" type="appl" />
         <tli id="T371" time="183.211" type="appl" />
         <tli id="T372" time="183.697" type="appl" />
         <tli id="T373" time="184.183" type="appl" />
         <tli id="T374" time="184.669" type="appl" />
         <tli id="T375" time="185.154" type="appl" />
         <tli id="T376" time="185.64" type="appl" />
         <tli id="T377" time="186.19265979465004" />
         <tli id="T378" time="186.834" type="appl" />
         <tli id="T379" time="187.541" type="appl" />
         <tli id="T380" time="188.249" type="appl" />
         <tli id="T381" time="188.82367358720538" />
         <tli id="T382" time="189.418" type="appl" />
         <tli id="T383" time="189.88" type="appl" />
         <tli id="T384" time="190.341" type="appl" />
         <tli id="T385" time="190.802" type="appl" />
         <tli id="T386" time="191.264" type="appl" />
         <tli id="T387" time="191.79166464403636" />
         <tli id="T389" time="192.63" type="appl" />
         <tli id="T390" time="193.082" type="appl" />
         <tli id="T391" time="193.535" type="appl" />
         <tli id="T392" time="193.987" type="appl" />
         <tli id="T393" time="194.44" type="appl" />
         <tli id="T394" time="194.892" type="appl" />
         <tli id="T395" time="195.344" type="appl" />
         <tli id="T396" time="195.797" type="appl" />
         <tli id="T397" time="196.249" type="appl" />
         <tli id="T398" time="196.702" type="appl" />
         <tli id="T399" time="197.154" type="appl" />
         <tli id="T400" time="197.607" type="appl" />
         <tli id="T401" time="197.9189998440892" />
         <tli id="T402" time="198.426" type="appl" />
         <tli id="T403" time="198.794" type="appl" />
         <tli id="T404" time="199.161" type="appl" />
         <tli id="T405" time="199.528" type="appl" />
         <tli id="T406" time="199.896" type="appl" />
         <tli id="T407" time="200.263" type="appl" />
         <tli id="T408" time="200.631" type="appl" />
         <tli id="T409" time="200.998" type="appl" />
         <tli id="T410" time="201.365" type="appl" />
         <tli id="T411" time="201.733" type="appl" />
         <tli id="T412" time="202.1" type="appl" />
         <tli id="T413" time="202.473" type="appl" />
         <tli id="T414" time="202.846" type="appl" />
         <tli id="T415" time="203.22" type="appl" />
         <tli id="T416" time="203.593" type="appl" />
         <tli id="T417" time="204.011" type="appl" />
         <tli id="T418" time="204.43" type="appl" />
         <tli id="T419" time="204.848" type="appl" />
         <tli id="T420" time="205.266" type="appl" />
         <tli id="T421" time="205.685" type="appl" />
         <tli id="T422" time="206.103" type="appl" />
         <tli id="T423" time="206.522" type="appl" />
         <tli id="T424" time="206.7866711361896" />
         <tli id="T425" time="207.314" type="appl" />
         <tli id="T426" time="207.689" type="appl" />
         <tli id="T427" time="208.063" type="appl" />
         <tli id="T428" time="208.437" type="appl" />
         <tli id="T429" time="208.812" type="appl" />
         <tli id="T430" time="209.0593275768363" />
         <tli id="T431" time="209.651" type="appl" />
         <tli id="T432" time="210.115" type="appl" />
         <tli id="T433" time="210.58" type="appl" />
         <tli id="T434" time="211.09733690467348" />
         <tli id="T435" time="211.454" type="appl" />
         <tli id="T436" time="211.864" type="appl" />
         <tli id="T437" time="212.275" type="appl" />
         <tli id="T438" time="212.685" type="appl" />
         <tli id="T439" time="213.095" type="appl" />
         <tli id="T440" time="213.505" type="appl" />
         <tli id="T441" time="213.915" type="appl" />
         <tli id="T442" time="214.325" type="appl" />
         <tli id="T443" time="214.736" type="appl" />
         <tli id="T444" time="215.146" type="appl" />
         <tli id="T445" time="215.556" type="appl" />
         <tli id="T446" time="215.91932791149225" />
         <tli id="T447" time="216.565" type="appl" />
         <tli id="T448" time="217.165" type="appl" />
         <tli id="T449" time="217.764" type="appl" />
         <tli id="T450" time="218.364" type="appl" />
         <tli id="T451" time="219.378761506405" />
         <tli id="T452" time="219.582" type="appl" />
         <tli id="T453" time="220.201" type="appl" />
         <tli id="T454" time="220.82" type="appl" />
         <tli id="T455" time="221.438" type="appl" />
         <tli id="T456" time="222.057" type="appl" />
         <tli id="T457" time="222.676" type="appl" />
         <tli id="T458" time="223.295" type="appl" />
         <tli id="T459" time="223.914" type="appl" />
         <tli id="T461" time="225.152" type="appl" />
         <tli id="T462" time="225.771" type="appl" />
         <tli id="T463" time="226.39" type="appl" />
         <tli id="T464" time="227.008" type="appl" />
         <tli id="T465" time="227.627" type="appl" />
         <tli id="T466" time="228.246" type="appl" />
         <tli id="T467" time="228.865" type="appl" />
         <tli id="T468" time="229.787" type="appl" />
         <tli id="T469" time="230.71" type="appl" />
         <tli id="T470" time="231.632" type="appl" />
         <tli id="T471" time="232.554" type="appl" />
         <tli id="T472" time="233.043" type="appl" />
         <tli id="T473" time="233.531" type="appl" />
         <tli id="T474" time="234.02" type="appl" />
         <tli id="T475" time="234.62799833414485" />
         <tli id="T8" time="234.78849916707242" type="intp" />
         <tli id="T476" time="234.949" type="appl" />
         <tli id="T477" time="235.39" type="appl" />
         <tli id="T478" time="235.831" type="appl" />
         <tli id="T479" time="236.271" type="appl" />
         <tli id="T480" time="236.712" type="appl" />
         <tli id="T482" time="238.4119873884601" />
         <tli id="T483" time="238.46249369423003" type="intp" />
         <tli id="T484" time="238.513" type="appl" />
         <tli id="T485" time="238.973" type="appl" />
         <tli id="T486" time="239.433" type="appl" />
         <tli id="T487" time="239.892" type="appl" />
         <tli id="T488" time="240.352" type="appl" />
         <tli id="T489" time="240.812" type="appl" />
         <tli id="T490" time="241.271" type="appl" />
         <tli id="T491" time="241.731" type="appl" />
         <tli id="T492" time="242.191" type="appl" />
         <tli id="T493" time="242.65" type="appl" />
         <tli id="T494" time="243.11" type="appl" />
         <tli id="T495" time="243.644" type="appl" />
         <tli id="T496" time="244.177" type="appl" />
         <tli id="T497" time="244.711" type="appl" />
         <tli id="T498" time="245.244" type="appl" />
         <tli id="T499" time="246.46527525729456" />
         <tli id="T500" time="246.482" type="appl" />
         <tli id="T501" time="247.186" type="appl" />
         <tli id="T502" time="247.889" type="appl" />
         <tli id="T503" time="249.071927208217" />
         <tli id="T504" time="249.106" type="appl" />
         <tli id="T505" time="249.619" type="appl" />
         <tli id="T507" time="251.0519160302732" />
         <tli id="T508" time="251.128" type="appl" />
         <tli id="T509" time="251.612" type="appl" />
         <tli id="T510" time="252.095" type="appl" />
         <tli id="T511" time="252.578" type="appl" />
         <tli id="T512" time="253.062" type="appl" />
         <tli id="T513" time="253.545" type="appl" />
         <tli id="T514" time="254.028" type="appl" />
         <tli id="T515" time="254.512" type="appl" />
         <tli id="T516" time="254.995" type="appl" />
         <tli id="T517" time="255.478" type="appl" />
         <tli id="T518" time="255.962" type="appl" />
         <tli id="T519" time="256.83834169658314" />
         <tli id="T520" time="256.958" type="appl" />
         <tli id="T521" time="257.472" type="appl" />
         <tli id="T522" time="257.985" type="appl" />
         <tli id="T523" time="258.498" type="appl" />
         <tli id="T524" time="259.012" type="appl" />
         <tli id="T525" time="259.47832679265804" />
         <tli id="T527" time="260.118" type="appl" />
         <tli id="T528" time="260.414" type="appl" />
         <tli id="T529" time="260.71" type="appl" />
         <tli id="T530" time="261.007" type="appl" />
         <tli id="T531" time="261.303" type="appl" />
         <tli id="T532" time="261.6" type="appl" />
         <tli id="T533" time="261.896" type="appl" />
         <tli id="T534" time="262.192" type="appl" />
         <tli id="T535" time="262.489" type="appl" />
         <tli id="T536" time="262.785" type="appl" />
         <tli id="T537" time="263.081" type="appl" />
         <tli id="T538" time="263.378" type="appl" />
         <tli id="T539" time="263.674" type="appl" />
         <tli id="T540" time="263.971" type="appl" />
         <tli id="T541" time="264.267" type="appl" />
         <tli id="T542" time="264.563" type="appl" />
         <tli id="T543" time="264.86" type="appl" />
         <tli id="T544" time="265.156" type="appl" />
         <tli id="T545" time="265.788" type="appl" />
         <tli id="T546" time="266.419" type="appl" />
         <tli id="T547" time="267.051" type="appl" />
         <tli id="T548" time="267.683" type="appl" />
         <tli id="T549" time="268.314" type="appl" />
         <tli id="T550" time="269.16600647626876" />
         <tli id="T551" time="269.661" type="appl" />
         <tli id="T552" time="270.377" type="appl" />
         <tli id="T553" time="272.35179578269555" />
         <tli id="T554" time="272.36553052179704" type="intp" />
         <tli id="T555" time="272.3792652608985" type="intp" />
         <tli id="T556" time="272.393" type="appl" />
         <tli id="T557" time="272.827" type="appl" />
         <tli id="T558" time="273.26" type="appl" />
         <tli id="T559" time="273.694" type="appl" />
         <tli id="T560" time="274.128" type="appl" />
         <tli id="T561" time="274.562" type="appl" />
         <tli id="T562" time="274.995" type="appl" />
         <tli id="T563" time="275.429" type="appl" />
         <tli id="T564" time="275.863" type="appl" />
         <tli id="T565" time="276.296" type="appl" />
         <tli id="T566" time="276.73" type="appl" />
         <tli id="T567" time="277.164" type="appl" />
         <tli id="T568" time="277.598" type="appl" />
         <tli id="T569" time="278.031" type="appl" />
         <tli id="T570" time="278.465" type="appl" />
         <tli id="T571" time="278.899" type="appl" />
         <tli id="T572" time="279.332" type="appl" />
         <tli id="T573" time="279.766" type="appl" />
         <tli id="T574" time="280.105" type="appl" />
         <tli id="T575" time="280.443" type="appl" />
         <tli id="T576" time="280.782" type="appl" />
         <tli id="T577" time="281.121" type="appl" />
         <tli id="T578" time="281.459" type="appl" />
         <tli id="T579" time="281.798" type="appl" />
         <tli id="T580" time="282.137" type="appl" />
         <tli id="T582" time="282.814" type="appl" />
         <tli id="T583" time="283.153" type="appl" />
         <tli id="T584" time="283.491" type="appl" />
         <tli id="T585" time="283.70334628132673" />
         <tli id="T586" time="284.4" type="appl" />
         <tli id="T587" time="284.97" type="appl" />
         <tli id="T588" time="285.54" type="appl" />
         <tli id="T589" time="286.47171606927793" />
         <tli id="T590" time="286.57" type="appl" />
         <tli id="T592" time="287.49" type="appl" />
         <tli id="T593" time="287.95" type="appl" />
         <tli id="T594" time="288.41" type="appl" />
         <tli id="T595" time="288.871" type="appl" />
         <tli id="T596" time="289.331" type="appl" />
         <tli id="T597" time="289.791" type="appl" />
         <tli id="T598" time="290.251" type="appl" />
         <tli id="T599" time="291.6116870517873" />
         <tli id="T600" time="291.822" type="appl" />
         <tli id="T601" time="292.932" type="appl" />
         <tli id="T602" time="293.491" type="appl" />
         <tli id="T603" time="294.01667347453736" />
         <tli id="T604" time="294.626" type="appl" />
         <tli id="T605" time="295.203" type="appl" />
         <tli id="T606" time="295.779" type="appl" />
         <tli id="T607" time="296.361" type="appl" />
         <tli id="T608" time="296.942" type="appl" />
         <tli id="T609" time="297.524" type="appl" />
         <tli id="T610" time="298.2326652983324" />
         <tli id="T611" time="298.59" type="appl" />
         <tli id="T612" time="299.074" type="appl" />
         <tli id="T613" time="299.557" type="appl" />
         <tli id="T614" time="300.041" type="appl" />
         <tli id="T615" time="300.525" type="appl" />
         <tli id="T616" time="300.897" type="appl" />
         <tli id="T617" time="301.268" type="appl" />
         <tli id="T618" time="301.64" type="appl" />
         <tli id="T619" time="302.012" type="appl" />
         <tli id="T620" time="302.384" type="appl" />
         <tli id="T621" time="302.755" type="appl" />
         <tli id="T622" time="303.127" type="appl" />
         <tli id="T623" time="303.663" type="appl" />
         <tli id="T624" time="304.199" type="appl" />
         <tli id="T625" time="304.735" type="appl" />
         <tli id="T626" time="305.2043446900089" />
         <tli id="T627" time="305.607" type="appl" />
         <tli id="T628" time="305.942" type="appl" />
         <tli id="T629" time="306.278" type="appl" />
         <tli id="T630" time="306.613" type="appl" />
         <tli id="T631" time="307.42493111211473" />
         <tli id="T632" time="307.43" type="appl" />
         <tli id="T633" time="307.91" type="appl" />
         <tli id="T634" time="308.391" type="appl" />
         <tli id="T635" time="308.871" type="appl" />
         <tli id="T636" time="309.352" type="appl" />
         <tli id="T637" time="309.832" type="appl" />
         <tli id="T638" time="310.2529880631045" />
         <tli id="T639" time="310.827" type="appl" />
         <tli id="T640" time="311.341" type="appl" />
         <tli id="T641" time="311.64834476901007" />
         <tli id="T642" time="312.232" type="appl" />
         <tli id="T643" time="312.608" type="appl" />
         <tli id="T644" time="312.985" type="appl" />
         <tli id="T645" time="313.362" type="appl" />
         <tli id="T646" time="313.738" type="appl" />
         <tli id="T647" time="314.115" type="appl" />
         <tli id="T648" time="314.491" type="appl" />
         <tli id="T649" time="315.00798726062266" />
         <tli id="T651" time="315.704" type="appl" />
         <tli id="T652" time="316.123" type="appl" />
         <tli id="T653" time="316.541" type="appl" />
         <tli id="T654" time="316.959" type="appl" />
         <tli id="T655" time="317.377" type="appl" />
         <tli id="T656" time="317.795" type="appl" />
         <tli id="T657" time="318.213" type="appl" />
         <tli id="T658" time="318.632" type="appl" />
         <tli id="T659" time="319.05" type="appl" />
         <tli id="T660" time="319.468" type="appl" />
         <tli id="T661" time="319.89" type="appl" />
         <tli id="T662" time="320.312" type="appl" />
         <tli id="T663" time="320.734" type="appl" />
         <tli id="T664" time="321.156" type="appl" />
         <tli id="T665" time="321.578" type="appl" />
         <tli id="T666" time="322.0" type="appl" />
         <tli id="T667" time="322.422" type="appl" />
         <tli id="T668" time="322.844" type="appl" />
         <tli id="T669" time="323.265" type="appl" />
         <tli id="T670" time="323.687" type="appl" />
         <tli id="T671" time="324.109" type="appl" />
         <tli id="T672" time="324.531" type="appl" />
         <tli id="T673" time="324.953" type="appl" />
         <tli id="T674" time="325.375" type="appl" />
         <tli id="T675" time="325.797" type="appl" />
         <tli id="T676" time="326.67815575217605" />
         <tli id="T677" time="326.745" type="appl" />
         <tli id="T678" time="327.272" type="appl" />
         <tli id="T679" time="327.798" type="appl" />
         <tli id="T680" time="328.325" type="appl" />
         <tli id="T681" time="328.851" type="appl" />
         <tli id="T682" time="329.377" type="appl" />
         <tli id="T683" time="329.904" type="appl" />
         <tli id="T684" time="330.43" type="appl" />
         <tli id="T685" time="330.957" type="appl" />
         <tli id="T686" time="331.483" type="appl" />
         <tli id="T687" time="331.874" type="appl" />
         <tli id="T688" time="332.266" type="appl" />
         <tli id="T689" time="332.657" type="appl" />
         <tli id="T690" time="333.048" type="appl" />
         <tli id="T691" time="333.439" type="appl" />
         <tli id="T692" time="333.831" type="appl" />
         <tli id="T693" time="334.222" type="appl" />
         <tli id="T695" time="335.004" type="appl" />
         <tli id="T696" time="335.396" type="appl" />
         <tli id="T697" time="335.8403436107622" />
         <tli id="T698" time="336.313" type="appl" />
         <tli id="T699" time="336.838" type="appl" />
         <tli id="T700" time="337.364" type="appl" />
         <tli id="T701" time="337.889" type="appl" />
         <tli id="T702" time="338.415" type="appl" />
         <tli id="T703" time="338.94" type="appl" />
         <tli id="T704" time="339.33266764499336" />
         <tli id="T705" time="339.832" type="appl" />
         <tli id="T706" time="340.198" type="appl" />
         <tli id="T707" time="340.564" type="appl" />
         <tli id="T708" time="340.929" type="appl" />
         <tli id="T709" time="341.295" type="appl" />
         <tli id="T710" time="341.661" type="appl" />
         <tli id="T711" time="342.027" type="appl" />
         <tli id="T712" time="343.1447294574917" />
         <tli id="T714" time="343.414" type="appl" />
         <tli id="T715" time="343.924" type="appl" />
         <tli id="T716" time="344.435" type="appl" />
         <tli id="T717" time="344.945" type="appl" />
         <tli id="T718" time="345.456" type="appl" />
         <tli id="T719" time="346.145988555676" />
         <tli id="T720" time="346.476" type="appl" />
         <tli id="T721" time="346.985" type="appl" />
         <tli id="T723" time="348.004" type="appl" />
         <tli id="T724" time="348.514" type="appl" />
         <tli id="T725" time="349.023" type="appl" />
         <tli id="T726" time="349.532" type="appl" />
         <tli id="T728" time="350.552" type="appl" />
         <tli id="T729" time="351.061" type="appl" />
         <tli id="T730" time="351.57" type="appl" />
         <tli id="T731" time="352.08" type="appl" />
         <tli id="T732" time="352.59" type="appl" />
         <tli id="T733" time="353.099" type="appl" />
         <tli id="T734" time="353.608" type="appl" />
         <tli id="T735" time="354.21800027713914" />
         <tli id="T736" time="354.474" type="appl" />
         <tli id="T737" time="354.831" type="appl" />
         <tli id="T738" time="355.187" type="appl" />
         <tli id="T739" time="355.543" type="appl" />
         <tli id="T740" time="355.9" type="appl" />
         <tli id="T741" time="356.64934071777526" />
         <tli id="T742" time="356.75" type="appl" />
         <tli id="T743" time="357.245" type="appl" />
         <tli id="T744" time="357.739" type="appl" />
         <tli id="T745" time="358.234" type="appl" />
         <tli id="T746" time="358.728" type="appl" />
         <tli id="T747" time="359.222" type="appl" />
         <tli id="T748" time="359.717" type="appl" />
         <tli id="T749" time="359.917655599974" />
         <tli id="T750" time="360.531" type="appl" />
         <tli id="T751" time="360.852" type="appl" />
         <tli id="T752" time="361.172" type="appl" />
         <tli id="T753" time="361.493" type="appl" />
         <tli id="T754" time="361.813" type="appl" />
         <tli id="T755" time="362.134" type="appl" />
         <tli id="T756" time="362.454" type="appl" />
         <tli id="T757" time="362.775" type="appl" />
         <tli id="T758" time="363.095" type="appl" />
         <tli id="T759" time="363.416" type="appl" />
         <tli id="T760" time="363.736" type="appl" />
         <tli id="T761" time="364.057" type="appl" />
         <tli id="T762" time="364.4236718281152" />
         <tli id="T763" time="364.893" type="appl" />
         <tli id="T764" time="365.408" type="appl" />
         <tli id="T765" time="365.924" type="appl" />
         <tli id="T766" time="366.439" type="appl" />
         <tli id="T767" time="366.955" type="appl" />
         <tli id="T768" time="367.471" type="appl" />
         <tli id="T769" time="367.986" type="appl" />
         <tli id="T770" time="368.502" type="appl" />
         <tli id="T771" time="369.017" type="appl" />
         <tli id="T772" time="369.43299250653877" />
         <tli id="T773" time="369.953" type="appl" />
         <tli id="T774" time="370.374" type="appl" />
         <tli id="T775" time="370.794" type="appl" />
         <tli id="T776" time="371.215" type="appl" />
         <tli id="T777" time="371.86834334120107" />
         <tli id="T778" time="372.1" type="appl" />
         <tli id="T779" time="372.565" type="appl" />
         <tli id="T780" time="373.03" type="appl" />
         <tli id="T781" time="373.494" type="appl" />
         <tli id="T782" time="373.959" type="appl" />
         <tli id="T783" time="374.424" type="appl" />
         <tli id="T784" time="374.69567112964074" />
         <tli id="T785" time="375.176" type="appl" />
         <tli id="T786" time="375.463" type="appl" />
         <tli id="T787" time="375.751" type="appl" />
         <tli id="T788" time="376.038" type="appl" />
         <tli id="T789" time="376.325" type="appl" />
         <tli id="T790" time="376.612" type="appl" />
         <tli id="T791" time="376.899" type="appl" />
         <tli id="T792" time="377.187" type="appl" />
         <tli id="T793" time="377.474" type="appl" />
         <tli id="T794" time="377.9009915758052" />
         <tli id="T795" time="378.493" type="appl" />
         <tli id="T796" time="379.225" type="appl" />
         <tli id="T797" time="379.956" type="appl" />
         <tli id="T798" time="380.688" type="appl" />
         <tli id="T799" time="381.42" type="appl" />
         <tli id="T800" time="381.8986773403713" />
         <tli id="T801" time="382.862" type="appl" />
         <tli id="T802" time="383.572" type="appl" />
         <tli id="T803" time="384.283" type="appl" />
         <tli id="T804" time="384.993" type="appl" />
         <tli id="T805" time="385.458" type="appl" />
         <tli id="T806" time="385.924" type="appl" />
         <tli id="T807" time="386.39" type="appl" />
         <tli id="T808" time="386.855" type="appl" />
         <tli id="T809" time="387.2" type="appl" />
         <tli id="T810" time="387.544" type="appl" />
         <tli id="T811" time="387.889" type="appl" />
         <tli id="T812" time="388.233" type="appl" />
         <tli id="T813" time="388.578" type="appl" />
         <tli id="T814" time="388.922" type="appl" />
         <tli id="T815" time="389.267" type="appl" />
         <tli id="T816" time="389.611" type="appl" />
         <tli id="T817" time="389.956" type="appl" />
         <tli id="T818" time="390.3" type="appl" />
         <tli id="T819" time="390.645" type="appl" />
         <tli id="T820" time="390.989" type="appl" />
         <tli id="T821" time="391.78065280209336" />
         <tli id="T822" time="391.9" type="appl" />
         <tli id="T823" time="392.467" type="appl" />
         <tli id="T824" time="393.033" type="appl" />
         <tli id="T825" time="393.6" type="appl" />
         <tli id="T826" time="394.6326679511615" />
         <tli id="T827" time="394.659" type="appl" />
         <tli id="T828" time="395.153" type="appl" />
         <tli id="T829" time="395.646" type="appl" />
         <tli id="T830" time="396.14" type="appl" />
         <tli id="T831" time="396.633" type="appl" />
         <tli id="T832" time="397.127" type="appl" />
         <tli id="T833" time="397.62" type="appl" />
         <tli id="T834" time="398.114" type="appl" />
         <tli id="T835" time="398.607" type="appl" />
         <tli id="T837" time="399.594" type="appl" />
         <tli id="T838" time="400.088" type="appl" />
         <tli id="T839" time="400.581" type="appl" />
         <tli id="T840" time="401.075" type="appl" />
         <tli id="T841" time="401.568" type="appl" />
         <tli id="T842" time="402.049" type="appl" />
         <tli id="T843" time="402.531" type="appl" />
         <tli id="T844" time="403.012" type="appl" />
         <tli id="T845" time="403.494" type="appl" />
         <tli id="T846" time="403.975" type="appl" />
         <tli id="T847" time="404.456" type="appl" />
         <tli id="T848" time="404.938" type="appl" />
         <tli id="T849" time="405.419" type="appl" />
         <tli id="T850" time="405.901" type="appl" />
         <tli id="T851" time="406.72866737203753" />
         <tli id="T852" time="407.026" type="appl" />
         <tli id="T853" time="407.67" type="appl" />
         <tli id="T854" time="408.314" type="appl" />
         <tli id="T855" time="408.958" type="appl" />
         <tli id="T856" time="409.601" type="appl" />
         <tli id="T857" time="410.245" type="appl" />
         <tli id="T859" time="411.533" type="appl" />
         <tli id="T860" time="412.115" type="appl" />
         <tli id="T861" time="412.697" type="appl" />
         <tli id="T862" time="413.5856599111738" />
         <tli id="T863" time="413.945" type="appl" />
         <tli id="T864" time="414.611" type="appl" />
         <tli id="T865" time="415.278" type="appl" />
         <tli id="T866" time="415.944" type="appl" />
         <tli id="T867" time="416.52999224740245" />
         <tli id="T868" time="417.045" type="appl" />
         <tli id="T869" time="417.48" type="appl" />
         <tli id="T870" time="417.915" type="appl" />
         <tli id="T871" time="418.35" type="appl" />
         <tli id="T872" time="418.9250047681264" />
         <tli id="T873" time="419.233" type="appl" />
         <tli id="T874" time="419.682" type="appl" />
         <tli id="T875" time="420.13" type="appl" />
         <tli id="T877" time="421.027" type="appl" />
         <tli id="T878" time="421.56165654968606" />
         <tli id="T879" time="422.055" type="appl" />
         <tli id="T880" time="422.634" type="appl" />
         <tli id="T881" time="423.214" type="appl" />
         <tli id="T882" time="423.794" type="appl" />
         <tli id="T930" time="424.084" type="intp" />
         <tli id="T883" time="424.374" type="appl" />
         <tli id="T885" time="425.533" type="appl" />
         <tli id="T886" time="425.998" type="appl" />
         <tli id="T887" time="426.464" type="appl" />
         <tli id="T888" time="426.929" type="appl" />
         <tli id="T889" time="427.395" type="appl" />
         <tli id="T890" time="427.86" type="appl" />
         <tli id="T891" time="428.326" type="appl" />
         <tli id="T892" time="428.791" type="appl" />
         <tli id="T893" time="429.257" type="appl" />
         <tli id="T894" time="429.722" type="appl" />
         <tli id="T895" time="430.188" type="appl" />
         <tli id="T896" time="430.69967787803665" />
         <tli id="T897" time="431.204" type="appl" />
         <tli id="T898" time="431.754" type="appl" />
         <tli id="T899" time="432.305" type="appl" />
         <tli id="T900" time="433.3093246037189" />
         <tli id="T901" time="433.322" type="appl" />
         <tli id="T902" time="433.788" type="appl" />
         <tli id="T903" time="434.254" type="appl" />
         <tli id="T904" time="434.72" type="appl" />
         <tli id="T905" time="435.186" type="appl" />
         <tli id="T906" time="435.652" type="appl" />
         <tli id="T907" time="436.118" type="appl" />
         <tli id="T908" time="436.584" type="appl" />
         <tli id="T909" time="437.05" type="appl" />
         <tli id="T910" time="437.516" type="appl" />
         <tli id="T911" time="438.0820059930797" />
         <tli id="T912" time="438.439" type="appl" />
         <tli id="T913" time="438.896" type="appl" />
         <tli id="T914" time="439.353" type="appl" />
         <tli id="T915" time="439.81" type="appl" />
         <tli id="T916" time="440.267" type="appl" />
         <tli id="T917" time="440.724" type="appl" />
         <tli id="T918" time="441.181" type="appl" />
         <tli id="T919" time="441.638" type="appl" />
         <tli id="T920" time="442.095" type="appl" />
         <tli id="T921" time="442.552" type="appl" />
         <tli id="T922" time="443.009" type="appl" />
         <tli id="T923" time="443.466" type="appl" />
         <tli id="T924" time="443.923" type="appl" />
         <tli id="T928" time="445.094" type="appl" />
         <tli id="T929" time="445.387" type="appl" />
         <tli id="T931" time="445.972" type="appl" />
         <tli id="T932" time="446.411" type="appl" />
         <tli id="T933" time="446.849" type="appl" />
         <tli id="T934" time="447.288" type="appl" />
         <tli id="T937" time="447.507" type="intp" />
         <tli id="T935" time="447.726" type="appl" />
         <tli id="T936" time="448.165" type="appl" />
         <tli id="T938" time="449.042" type="appl" />
         <tli id="T939" time="449.471" type="appl" />
         <tli id="T940" time="449.901" type="appl" />
         <tli id="T941" time="450.33" type="appl" />
         <tli id="T942" time="450.759" type="appl" />
         <tli id="T943" time="451.189" type="appl" />
         <tli id="T944" time="451.618" type="appl" />
         <tli id="T945" time="452.24" type="appl" />
         <tli id="T946" time="452.862" type="appl" />
         <tli id="T947" time="453.484" type="appl" />
         <tli id="T948" time="454.105" type="appl" />
         <tli id="T949" time="454.727" type="appl" />
         <tli id="T950" time="455.349" type="appl" />
         <tli id="T951" time="456.77742128223025" />
         <tli id="T952" time="457.048" type="appl" />
         <tli id="T953" time="458.126" type="appl" />
         <tli id="T954" time="459.23631365065654" />
         <tli id="T955" time="459.629" type="appl" />
         <tli id="T956" time="460.055" type="appl" />
         <tli id="T957" time="460.48" type="appl" />
         <tli id="T958" time="460.906" type="appl" />
         <tli id="T959" time="461.332" type="appl" />
         <tli id="T960" time="461.758" type="appl" />
         <tli id="T961" time="462.184" type="appl" />
         <tli id="T962" time="462.61" type="appl" />
         <tli id="T963" time="463.035" type="appl" />
         <tli id="T964" time="463.461" type="appl" />
         <tli id="T965" time="463.887" type="appl" />
         <tli id="T966" time="464.0796717243484" />
         <tli id="T967" time="465.142" type="appl" />
         <tli id="T968" time="465.97" type="appl" />
         <tli id="T970" time="466.773" type="appl" />
         <tli id="T971" time="467.175" type="appl" />
         <tli id="T972" time="467.576" type="appl" />
         <tli id="T973" time="467.978" type="appl" />
         <tli id="T974" time="468.379" type="appl" />
         <tli id="T975" time="469.1409973175137" />
         <tli id="T976" time="469.263" type="appl" />
         <tli id="T977" time="469.744" type="appl" />
         <tli id="T978" time="470.226" type="appl" />
         <tli id="T979" time="470.708" type="appl" />
         <tli id="T980" time="471.19" type="appl" />
         <tli id="T981" time="471.672" type="appl" />
         <tli id="T982" time="472.153" type="appl" />
         <tli id="T983" time="472.5216553154842" />
         <tli id="T984" time="473.423" type="appl" />
         <tli id="T985" time="474.212" type="appl" />
         <tli id="T986" time="475.0" type="appl" />
         <tli id="T987" time="476.1639785029014" />
         <tli id="T988" time="476.182" type="appl" />
         <tli id="T990" time="476.97" type="appl" />
         <tli id="T991" time="477.364" type="appl" />
         <tli id="T992" time="477.758" type="appl" />
         <tli id="T993" time="478.153" type="appl" />
         <tli id="T994" time="478.547" type="appl" />
         <tli id="T995" time="478.941" type="appl" />
         <tli id="T996" time="479.335" type="appl" />
         <tli id="T997" time="479.729" type="appl" />
         <tli id="T998" time="480.123" type="appl" />
         <tli id="T999" time="480.517" type="appl" />
         <tli id="T1000" time="481.178" type="appl" />
         <tli id="T1001" time="481.84" type="appl" />
         <tli id="T1002" time="482.501" type="appl" />
         <tli id="T1003" time="483.163" type="appl" />
         <tli id="T1004" time="483.824" type="appl" />
         <tli id="T1005" time="484.485" type="appl" />
         <tli id="T1006" time="485.147" type="appl" />
         <tli id="T1007" time="485.808" type="appl" />
         <tli id="T1008" time="486.47" type="appl" />
         <tli id="T1009" time="487.131" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-ChVD"
                      id="tx-ChVD"
                      speaker="ChVD"
                      type="t">
         <timeline-fork end="T80" start="T78">
            <tli id="T78.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T90" start="T87">
            <tli id="T87.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T113" start="T111">
            <tli id="T111.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T138" start="T136">
            <tli id="T136.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T211" start="T209">
            <tli id="T209.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T255" start="T253">
            <tli id="T253.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T461" start="T459">
            <tli id="T459.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T565" start="T563">
            <tli id="T563.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T582" start="T580">
            <tli id="T580.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T592" start="T590">
            <tli id="T590.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T695" start="T693">
            <tli id="T693.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T714" start="T712">
            <tli id="T712.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T837" start="T835">
            <tli id="T835.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T877" start="T875">
            <tli id="T875.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T984" start="T983">
            <tli id="T983.tx-ChVD.1" />
         </timeline-fork>
         <timeline-fork end="T990" start="T988">
            <tli id="T988.tx-ChVD.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-ChVD">
            <ts e="T999" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dudʼinkaga</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kelbitim</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">v</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">sorаk</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">vtаrom</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Avgust</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ɨjga</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_28" n="HIAT:w" s="T7">vaabšʼe-ta</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_31" n="HIAT:w" s="T10">kamʼisʼsʼijanɨ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_34" n="HIAT:w" s="T11">praxadiː</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_37" n="HIAT:w" s="T12">gɨnnɨm</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_39" n="HIAT:ip">–</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_42" n="HIAT:w" s="T13">nʼestrajevojga</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_46" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_48" n="HIAT:w" s="T14">Narilʼskɨjga</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_51" n="HIAT:w" s="T15">ɨːppɨttara</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_55" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">Narilʼskɨjtan</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">kimŋe</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_63" n="HIAT:w" s="T18">eː</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_66" n="HIAT:w" s="T19">vajenʼizʼirovannɨj</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T9">axranaga</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">üleliː</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">tüstüm</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">dabravolsɨlarɨ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">gɨtta</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">emi͡e</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">ka</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">tübespitim</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_95" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">Krasnajarskajga</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">eː</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_103" n="HIAT:w" s="T29">di͡eri</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_106" n="HIAT:w" s="T30">barbɨtɨm</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_108" n="HIAT:ip">"</nts>
                  <ts e="T32" id="Seg_110" n="HIAT:w" s="T31">Spartak</ts>
                  <nts id="Seg_111" n="HIAT:ip">"</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">paraxotɨnan</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_118" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">Onton</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">ü͡örennim</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_126" n="HIAT:w" s="T35">Krasnajarskaj</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">pʼexoːtaga</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">s</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">tankаvɨmi</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_138" n="HIAT:w" s="T39">pulʼemʼočʼčʼikamʼi</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_142" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_144" n="HIAT:w" s="T40">U͡on</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_147" n="HIAT:w" s="T41">biːr</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_150" n="HIAT:w" s="T42">ɨjɨ</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_153" n="HIAT:w" s="T43">duː</ts>
                  <nts id="Seg_154" n="HIAT:ip">,</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_157" n="HIAT:w" s="T44">kahɨ</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_160" n="HIAT:w" s="T45">duː</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_163" n="HIAT:w" s="T46">ü͡örenen</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_166" n="HIAT:w" s="T47">baraːn</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_170" n="HIAT:w" s="T48">emi͡e</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_173" n="HIAT:w" s="T49">kattɨː</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_175" n="HIAT:ip">(</nts>
                  <nts id="Seg_176" n="HIAT:ip">(</nts>
                  <ats e="T54" id="Seg_177" n="HIAT:non-pho" s="T50">…</ats>
                  <nts id="Seg_178" n="HIAT:ip">)</nts>
                  <nts id="Seg_179" n="HIAT:ip">)</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_182" n="HIAT:w" s="T54">kimi͡eke</ts>
                  <nts id="Seg_183" n="HIAT:ip">,</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">Zvʼenʼigu͡orakka</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_188" n="HIAT:ip">–</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_191" n="HIAT:w" s="T52">mladšij</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_194" n="HIAT:w" s="T53">sʼeržant</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_198" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_200" n="HIAT:w" s="T55">Parasut</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_203" n="HIAT:w" s="T56">razbirajdaːččɨ</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_206" n="HIAT:w" s="T57">etim</ts>
                  <nts id="Seg_207" n="HIAT:ip">,</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_210" n="HIAT:w" s="T58">desanskʼij</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_214" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_216" n="HIAT:w" s="T59">Dʼe</ts>
                  <nts id="Seg_217" n="HIAT:ip">,</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">onno</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_223" n="HIAT:w" s="T61">uhun</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_226" n="HIAT:w" s="T62">da</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_229" n="HIAT:w" s="T63">ü͡öremmetegim</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_232" n="HIAT:w" s="T64">dogottorum</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_235" n="HIAT:w" s="T65">gɨtta</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_238" n="HIAT:w" s="T66">ba</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_241" n="HIAT:w" s="T67">kaːlɨmaːrɨbɨn</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_244" n="HIAT:w" s="T68">biːrge</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_247" n="HIAT:w" s="T69">barsɨbɨtɨm</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_251" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_253" n="HIAT:w" s="T70">Dʼe</ts>
                  <nts id="Seg_254" n="HIAT:ip">,</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_257" n="HIAT:w" s="T71">ol</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_260" n="HIAT:w" s="T72">kimneːtim</ts>
                  <nts id="Seg_261" n="HIAT:ip">,</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_264" n="HIAT:w" s="T73">kimi͡eke</ts>
                  <nts id="Seg_265" n="HIAT:ip">,</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_268" n="HIAT:w" s="T74">Muːromŋa</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_271" n="HIAT:w" s="T75">ü͡örene</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_274" n="HIAT:w" s="T76">kimneːtibit</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_277" n="HIAT:w" s="T77">zapasnoj</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_279" n="HIAT:ip">(</nts>
                  <ts e="T78.tx-ChVD.1" id="Seg_281" n="HIAT:w" s="T78">ku͡o</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_284" n="HIAT:w" s="T78.tx-ChVD.1">-</ts>
                  <nts id="Seg_285" n="HIAT:ip">)</nts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_289" n="HIAT:u" s="T80">
                  <ts e="T481" id="Seg_291" n="HIAT:w" s="T80">Prostа</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_294" n="HIAT:w" s="T481">kimniːr</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_297" n="HIAT:w" s="T81">etilere</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_300" n="HIAT:w" s="T82">onno</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_303" n="HIAT:w" s="T83">farmʼiravanʼije</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_306" n="HIAT:w" s="T84">oŋoror</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_309" n="HIAT:w" s="T85">etilere</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_312" n="HIAT:w" s="T86">kakʼije</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_314" n="HIAT:ip">(</nts>
                  <ts e="T87.tx-ChVD.1" id="Seg_316" n="HIAT:w" s="T87">čʼa</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_319" n="HIAT:w" s="T87.tx-ChVD.1">-</ts>
                  <nts id="Seg_320" n="HIAT:ip">)</nts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_324" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_326" n="HIAT:w" s="T90">Kʼem</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_330" n="HIAT:w" s="T91">tu͡okka</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_333" n="HIAT:w" s="T92">ü͡öremmikkin</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_336" n="HIAT:w" s="T93">kimneːk</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_339" n="HIAT:w" s="T94">kim</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_342" n="HIAT:w" s="T95">kimniːller</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_345" n="HIAT:w" s="T96">otto</ts>
                  <nts id="Seg_346" n="HIAT:ip">,</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_349" n="HIAT:w" s="T97">kakʼije</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_352" n="HIAT:w" s="T98">čʼastʼi</ts>
                  <nts id="Seg_353" n="HIAT:ip">,</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_356" n="HIAT:w" s="T99">pаdrazdʼelʼenʼija</ts>
                  <nts id="Seg_357" n="HIAT:ip">.</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_360" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_362" n="HIAT:w" s="T100">Dʼe</ts>
                  <nts id="Seg_363" n="HIAT:ip">,</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_366" n="HIAT:w" s="T101">onno</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_369" n="HIAT:w" s="T102">kimneːtim</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_372" n="HIAT:w" s="T103">da</ts>
                  <nts id="Seg_373" n="HIAT:ip">,</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_376" n="HIAT:w" s="T104">Muːromtan</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_379" n="HIAT:w" s="T105">kimi͡eke</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_382" n="HIAT:w" s="T106">tübestibit</ts>
                  <nts id="Seg_383" n="HIAT:ip">,</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_386" n="HIAT:w" s="T107">ol</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_389" n="HIAT:w" s="T108">Tuːlaga</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_393" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_395" n="HIAT:w" s="T109">Ol</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_398" n="HIAT:w" s="T110">Tula</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_400" n="HIAT:ip">(</nts>
                  <ts e="T111.tx-ChVD.1" id="Seg_402" n="HIAT:w" s="T111">taː</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_405" n="HIAT:w" s="T111.tx-ChVD.1">-</ts>
                  <nts id="Seg_406" n="HIAT:ip">)</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_409" n="HIAT:w" s="T113">razbʼilsʼa</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_412" n="HIAT:w" s="T114">bu͡o</ts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_416" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_418" n="HIAT:w" s="T115">Agɨs</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_421" n="HIAT:w" s="T116">kilametrɨ</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_424" n="HIAT:w" s="T117">duː</ts>
                  <nts id="Seg_425" n="HIAT:ip">,</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_428" n="HIAT:w" s="T118">kahɨ</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_431" n="HIAT:w" s="T119">duː</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_434" n="HIAT:w" s="T120">baran</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_437" n="HIAT:w" s="T121">baraːn</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_441" n="HIAT:w" s="T122">timir</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_444" n="HIAT:w" s="T123">oroktoruŋ</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_447" n="HIAT:w" s="T124">barɨta</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_450" n="HIAT:w" s="T125">aldʼammɨttara</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_453" n="HIAT:w" s="T126">bu͡olla</ts>
                  <nts id="Seg_454" n="HIAT:ip">.</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_457" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_459" n="HIAT:w" s="T127">Dʼe</ts>
                  <nts id="Seg_460" n="HIAT:ip">,</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_463" n="HIAT:w" s="T128">onno</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_466" n="HIAT:w" s="T129">kimniː</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_469" n="HIAT:w" s="T130">hɨttakpɨtɨna</ts>
                  <nts id="Seg_470" n="HIAT:ip">,</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_473" n="HIAT:w" s="T131">uže</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_476" n="HIAT:w" s="T132">nalʼotɨ</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_479" n="HIAT:w" s="T133">bu͡o</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_482" n="HIAT:w" s="T134">bi͡ek</ts>
                  <nts id="Seg_483" n="HIAT:ip">,</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_485" n="HIAT:ip">(</nts>
                  <ts e="T136" id="Seg_487" n="HIAT:w" s="T135">kötö</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136.tx-ChVD.1" id="Seg_490" n="HIAT:w" s="T136">hɨ</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_493" n="HIAT:w" s="T136.tx-ChVD.1">-</ts>
                  <nts id="Seg_494" n="HIAT:ip">)</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_497" n="HIAT:w" s="T138">kötöːččü</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_500" n="HIAT:w" s="T139">etilere</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_503" n="HIAT:w" s="T140">harsi͡erda</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_506" n="HIAT:w" s="T141">aːjɨ</ts>
                  <nts id="Seg_507" n="HIAT:ip">.</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_510" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_512" n="HIAT:w" s="T142">Dʼe</ts>
                  <nts id="Seg_513" n="HIAT:ip">,</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_516" n="HIAT:w" s="T143">onton</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_519" n="HIAT:w" s="T144">uhun</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_522" n="HIAT:w" s="T145">da</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_525" n="HIAT:w" s="T146">bu͡olla</ts>
                  <nts id="Seg_526" n="HIAT:ip">,</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_529" n="HIAT:w" s="T147">eː</ts>
                  <nts id="Seg_530" n="HIAT:ip">,</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_532" n="HIAT:ip">(</nts>
                  <ts e="T150" id="Seg_534" n="HIAT:w" s="T148">barɨm-</ts>
                  <nts id="Seg_535" n="HIAT:ip">)</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_538" n="HIAT:w" s="T150">kimniːbit</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_541" n="HIAT:w" s="T151">onno</ts>
                  <nts id="Seg_542" n="HIAT:ip">.</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_545" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_547" n="HIAT:w" s="T152">Ile</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_550" n="HIAT:w" s="T153">hatɨː</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_553" n="HIAT:w" s="T154">barbɨppɨt</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_556" n="HIAT:w" s="T155">ol</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_559" n="HIAT:w" s="T156">kimi͡eke</ts>
                  <nts id="Seg_560" n="HIAT:ip">,</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_563" n="HIAT:w" s="T157">kanna</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_565" n="HIAT:ip">(</nts>
                  <ts e="T160" id="Seg_567" n="HIAT:w" s="T158">ho-</ts>
                  <nts id="Seg_568" n="HIAT:ip">)</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_571" n="HIAT:w" s="T160">hol</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_574" n="HIAT:w" s="T161">vajna</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_577" n="HIAT:w" s="T162">bu͡olu͡ok</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_580" n="HIAT:w" s="T163">hire</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_583" n="HIAT:w" s="T164">bu͡olu͡or</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_586" n="HIAT:w" s="T165">di͡eri</ts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_590" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_592" n="HIAT:w" s="T166">Dʼe</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_595" n="HIAT:w" s="T167">onno</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_598" n="HIAT:w" s="T168">kimneːččiler</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_601" n="HIAT:w" s="T169">bu͡o</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_604" n="HIAT:w" s="T170">kim</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_606" n="HIAT:ip">–</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_609" n="HIAT:w" s="T171">partʼizanskɨj</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_612" n="HIAT:w" s="T172">iti</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_615" n="HIAT:w" s="T173">аtrʼad</ts>
                  <nts id="Seg_616" n="HIAT:ip">.</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_619" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_621" n="HIAT:w" s="T174">O</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_624" n="HIAT:w" s="T175">kimŋe</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_626" n="HIAT:ip">–</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_629" n="HIAT:w" s="T176">NOP</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_632" n="HIAT:w" s="T177">SOP</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_635" n="HIAT:w" s="T178">abaznačajdɨːllar</ts>
                  <nts id="Seg_636" n="HIAT:ip">.</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_639" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_641" n="HIAT:w" s="T179">Onu</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_644" n="HIAT:w" s="T180">da</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_647" n="HIAT:w" s="T181">bu</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_650" n="HIAT:w" s="T182">muja</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_652" n="HIAT:ip">(</nts>
                  <ts e="T184" id="Seg_654" n="HIAT:w" s="T183">kimniː-</ts>
                  <nts id="Seg_655" n="HIAT:ip">)</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_658" n="HIAT:w" s="T184">kimniːrge</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_661" n="HIAT:w" s="T185">ürek</ts>
                  <nts id="Seg_662" n="HIAT:ip">,</nts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_665" n="HIAT:w" s="T186">tu͡ok</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_668" n="HIAT:w" s="T187">ere</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_671" n="HIAT:w" s="T188">ürekkeːn</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_674" n="HIAT:w" s="T189">hɨtar</ts>
                  <nts id="Seg_675" n="HIAT:ip">.</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_678" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_680" n="HIAT:w" s="T190">Če</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_683" n="HIAT:w" s="T191">onno</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_686" n="HIAT:w" s="T192">dʼerʼevnʼaga</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_690" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_692" n="HIAT:w" s="T193">Onno</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_695" n="HIAT:w" s="T194">аbаranʼassa</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_698" n="HIAT:w" s="T195">gɨnallar</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_701" n="HIAT:w" s="T196">eni</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_705" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_707" n="HIAT:w" s="T197">Harsi͡erda</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_710" n="HIAT:w" s="T198">kimneːtibit</ts>
                  <nts id="Seg_711" n="HIAT:ip">,</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_714" n="HIAT:w" s="T199">ahɨː</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_717" n="HIAT:w" s="T200">ahɨː</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_720" n="HIAT:w" s="T201">tühen</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_723" n="HIAT:w" s="T202">baraːmmɨt</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_726" n="HIAT:w" s="T203">onu</ts>
                  <nts id="Seg_727" n="HIAT:ip">.</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_730" n="HIAT:u" s="T204">
                  <nts id="Seg_731" n="HIAT:ip">"</nts>
                  <ts e="T205" id="Seg_733" n="HIAT:w" s="T204">Dʼe</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_736" n="HIAT:w" s="T205">ɨtɨ͡alahallar</ts>
                  <nts id="Seg_737" n="HIAT:ip">"</nts>
                  <nts id="Seg_738" n="HIAT:ip">,</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_741" n="HIAT:w" s="T206">kamandʼirbɨtɨn</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_744" n="HIAT:w" s="T207">kepsiːr</ts>
                  <nts id="Seg_745" n="HIAT:ip">.</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_748" n="HIAT:u" s="T208">
                  <nts id="Seg_749" n="HIAT:ip">"</nts>
                  <ts e="T209" id="Seg_751" n="HIAT:w" s="T208">Kajtak</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_753" n="HIAT:ip">(</nts>
                  <ts e="T209.tx-ChVD.1" id="Seg_755" n="HIAT:w" s="T209">аboː</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_758" n="HIAT:w" s="T209.tx-ChVD.1">-</ts>
                  <nts id="Seg_759" n="HIAT:ip">)</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_762" n="HIAT:w" s="T211">kimni͡ekkitin</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_765" n="HIAT:w" s="T212">onton</ts>
                  <nts id="Seg_766" n="HIAT:ip">…</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_769" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_771" n="HIAT:w" s="T214">Dʼe</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_774" n="HIAT:w" s="T215">аkruzeːnije</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_777" n="HIAT:w" s="T216">tübestekkitine</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_780" n="HIAT:w" s="T217">bagar</ts>
                  <nts id="Seg_781" n="HIAT:ip">,</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_784" n="HIAT:w" s="T218">plʼen</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_787" n="HIAT:w" s="T219">tübestekketin</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_790" n="HIAT:w" s="T220">itte</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_793" n="HIAT:w" s="T221">dаkumʼentɨ</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_796" n="HIAT:w" s="T222">kim</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_799" n="HIAT:w" s="T223">kimneːmi͡ekkin</ts>
                  <nts id="Seg_800" n="HIAT:ip">,</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_803" n="HIAT:w" s="T224">naːda</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_806" n="HIAT:w" s="T225">bu͡olla</ts>
                  <nts id="Seg_807" n="HIAT:ip">,</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_810" n="HIAT:w" s="T226">bejegin</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_813" n="HIAT:w" s="T227">bi͡erimi͡ekkin</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_816" n="HIAT:w" s="T228">bagar</ts>
                  <nts id="Seg_817" n="HIAT:ip">.</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_820" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_822" n="HIAT:w" s="T229">Prostа</ts>
                  <nts id="Seg_823" n="HIAT:ip">,</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_825" n="HIAT:ip">(</nts>
                  <ts e="T232" id="Seg_827" n="HIAT:w" s="T230">gi-</ts>
                  <nts id="Seg_828" n="HIAT:ip">)</nts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_831" n="HIAT:w" s="T232">kajɨtalaːmaŋ</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_833" n="HIAT:ip">(</nts>
                  <ts e="T234" id="Seg_835" n="HIAT:w" s="T233">huruk-</ts>
                  <nts id="Seg_836" n="HIAT:ip">)</nts>
                  <nts id="Seg_837" n="HIAT:ip">,</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_840" n="HIAT:w" s="T234">eː</ts>
                  <nts id="Seg_841" n="HIAT:ip">,</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_844" n="HIAT:w" s="T235">huruktargɨt</ts>
                  <nts id="Seg_845" n="HIAT:ip">"</nts>
                  <nts id="Seg_846" n="HIAT:ip">,</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_849" n="HIAT:w" s="T236">diːr</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_852" n="HIAT:w" s="T237">bu͡olla</ts>
                  <nts id="Seg_853" n="HIAT:ip">,</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_855" n="HIAT:ip">"</nts>
                  <nts id="Seg_856" n="HIAT:ip">(</nts>
                  <ts e="T240" id="Seg_858" n="HIAT:w" s="T238">arak-</ts>
                  <nts id="Seg_859" n="HIAT:ip">)</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_862" n="HIAT:w" s="T240">kimneːŋ</ts>
                  <nts id="Seg_863" n="HIAT:ip">.</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_866" n="HIAT:u" s="T241">
                  <nts id="Seg_867" n="HIAT:ip">(</nts>
                  <ts e="T243" id="Seg_869" n="HIAT:w" s="T241">Kömü-</ts>
                  <nts id="Seg_870" n="HIAT:ip">)</nts>
                  <nts id="Seg_871" n="HIAT:ip">,</nts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_874" n="HIAT:w" s="T243">e</ts>
                  <nts id="Seg_875" n="HIAT:ip">,</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_878" n="HIAT:w" s="T244">kömüŋ</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_881" n="HIAT:w" s="T245">duː</ts>
                  <nts id="Seg_882" n="HIAT:ip">,</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_885" n="HIAT:w" s="T246">bejegit</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_888" n="HIAT:w" s="T247">hi͡eŋ</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_891" n="HIAT:w" s="T248">duː</ts>
                  <nts id="Seg_892" n="HIAT:ip">"</nts>
                  <nts id="Seg_893" n="HIAT:ip">,</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_896" n="HIAT:w" s="T249">di͡ečči</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_899" n="HIAT:w" s="T250">bugurduk</ts>
                  <nts id="Seg_900" n="HIAT:ip">.</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_903" n="HIAT:u" s="T251">
                  <nts id="Seg_904" n="HIAT:ip">"</nts>
                  <ts e="T252" id="Seg_906" n="HIAT:w" s="T251">Kajatalaːtɨŋ</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_909" n="HIAT:w" s="T252">daː</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253.tx-ChVD.1" id="Seg_912" n="HIAT:w" s="T253">vsʼo</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_915" n="HIAT:w" s="T253.tx-ChVD.1">ravno</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_917" n="HIAT:ip">(</nts>
                  <ts e="T256" id="Seg_919" n="HIAT:w" s="T255">kili͡ejd-</ts>
                  <nts id="Seg_920" n="HIAT:ip">)</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_923" n="HIAT:w" s="T256">iti</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_926" n="HIAT:w" s="T257">kimniːller</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_929" n="HIAT:w" s="T258">bu͡o</ts>
                  <nts id="Seg_930" n="HIAT:ip">,</nts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_933" n="HIAT:w" s="T259">ol</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_936" n="HIAT:w" s="T260">bulallar</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_939" n="HIAT:w" s="T261">bu͡o</ts>
                  <nts id="Seg_940" n="HIAT:ip">.</nts>
                  <nts id="Seg_941" n="HIAT:ip">"</nts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_944" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_946" n="HIAT:w" s="T262">Ol</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_949" n="HIAT:w" s="T263">ke</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_952" n="HIAT:w" s="T264">orotaŋ</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_955" n="HIAT:w" s="T265">keːhe</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_958" n="HIAT:w" s="T266">hüːrbüppün</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_961" n="HIAT:w" s="T267">biːrde</ts>
                  <nts id="Seg_962" n="HIAT:ip">.</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_965" n="HIAT:u" s="T268">
                  <nts id="Seg_966" n="HIAT:ip">(</nts>
                  <ts e="T270" id="Seg_968" n="HIAT:w" s="T268">Kann-</ts>
                  <nts id="Seg_969" n="HIAT:ip">)</nts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_972" n="HIAT:w" s="T270">kannuktarɨn</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_975" n="HIAT:w" s="T271">da</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_978" n="HIAT:w" s="T272">dʼüːlleːmmin</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_981" n="HIAT:w" s="T273">karaŋaga</ts>
                  <nts id="Seg_982" n="HIAT:ip">,</nts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_985" n="HIAT:w" s="T274">nʼemʼestere</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_988" n="HIAT:w" s="T275">da</ts>
                  <nts id="Seg_989" n="HIAT:ip">,</nts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_992" n="HIAT:w" s="T276">kimnere</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_995" n="HIAT:w" s="T277">da</ts>
                  <nts id="Seg_996" n="HIAT:ip">?</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_999" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_1001" n="HIAT:w" s="T278">Dʼe</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1004" n="HIAT:w" s="T279">ol</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1007" n="HIAT:w" s="T280">min</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1010" n="HIAT:w" s="T281">hɨldʼammɨn</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1013" n="HIAT:w" s="T282">kördüm</ts>
                  <nts id="Seg_1014" n="HIAT:ip">,</nts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1017" n="HIAT:w" s="T283">bi͡es</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1020" n="HIAT:w" s="T284">kihi</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1023" n="HIAT:w" s="T285">hɨldʼallar</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1026" n="HIAT:w" s="T286">araj</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1029" n="HIAT:w" s="T287">bugurduk</ts>
                  <nts id="Seg_1030" n="HIAT:ip">.</nts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1033" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1035" n="HIAT:w" s="T288">Dʼe</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1038" n="HIAT:w" s="T289">onton</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1041" n="HIAT:w" s="T290">ol</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1044" n="HIAT:w" s="T291">di͡egitten</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1047" n="HIAT:w" s="T292">nʼemsɨlarbɨt</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1050" n="HIAT:w" s="T293">keleller</ts>
                  <nts id="Seg_1051" n="HIAT:ip">,</nts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1054" n="HIAT:w" s="T294">transejtan</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1057" n="HIAT:w" s="T295">turallar</ts>
                  <nts id="Seg_1058" n="HIAT:ip">.</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1061" n="HIAT:u" s="T296">
                  <nts id="Seg_1062" n="HIAT:ip">"</nts>
                  <ts e="T297" id="Seg_1064" n="HIAT:w" s="T296">Töttörü</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1067" n="HIAT:w" s="T297">kürü͡ögüŋ</ts>
                  <nts id="Seg_1068" n="HIAT:ip">"</nts>
                  <nts id="Seg_1069" n="HIAT:ip">,</nts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1072" n="HIAT:w" s="T298">diː-diː</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1075" n="HIAT:w" s="T299">kimniːller</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1079" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1081" n="HIAT:w" s="T300">Ol</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1084" n="HIAT:w" s="T301">kihiŋ</ts>
                  <nts id="Seg_1085" n="HIAT:ip">,</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1088" n="HIAT:w" s="T302">kamandʼirdardaːktar</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1091" n="HIAT:w" s="T303">ol</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1094" n="HIAT:w" s="T304">dogottoro</ts>
                  <nts id="Seg_1095" n="HIAT:ip">.</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1098" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1100" n="HIAT:w" s="T305">Atɨttar</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1103" n="HIAT:w" s="T306">onto</ts>
                  <nts id="Seg_1104" n="HIAT:ip">,</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1107" n="HIAT:w" s="T307">atɨn</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1110" n="HIAT:w" s="T308">čʼaːstan</ts>
                  <nts id="Seg_1111" n="HIAT:ip">.</nts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1114" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1116" n="HIAT:w" s="T309">Bejem</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1118" n="HIAT:ip">(</nts>
                  <ts e="T312" id="Seg_1120" n="HIAT:w" s="T310">ho-</ts>
                  <nts id="Seg_1121" n="HIAT:ip">)</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1124" n="HIAT:w" s="T312">dogottorbun</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1127" n="HIAT:w" s="T313">ol</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1130" n="HIAT:w" s="T314">keːhe</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1133" n="HIAT:w" s="T315">hüːrbüppün</ts>
                  <nts id="Seg_1134" n="HIAT:ip">,</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1137" n="HIAT:w" s="T316">eni</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1140" n="HIAT:w" s="T317">ki͡e</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1143" n="HIAT:w" s="T318">duː</ts>
                  <nts id="Seg_1144" n="HIAT:ip">,</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1147" n="HIAT:w" s="T319">tu͡ok</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1150" n="HIAT:w" s="T320">duː</ts>
                  <nts id="Seg_1151" n="HIAT:ip">.</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1154" n="HIAT:u" s="T321">
                  <nts id="Seg_1155" n="HIAT:ip">"</nts>
                  <ts e="T322" id="Seg_1157" n="HIAT:w" s="T321">Astupajdaːŋ</ts>
                  <nts id="Seg_1158" n="HIAT:ip">"</nts>
                  <nts id="Seg_1159" n="HIAT:ip">,</nts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1162" n="HIAT:w" s="T322">diːr</ts>
                  <nts id="Seg_1163" n="HIAT:ip">.</nts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1166" n="HIAT:u" s="T323">
                  <nts id="Seg_1167" n="HIAT:ip">"</nts>
                  <ts e="T324" id="Seg_1169" n="HIAT:w" s="T323">Tolʼkа</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1172" n="HIAT:w" s="T324">nʼe</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1175" n="HIAT:w" s="T325">zamʼetnа</ts>
                  <nts id="Seg_1176" n="HIAT:ip">"</nts>
                  <nts id="Seg_1177" n="HIAT:ip">,</nts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1180" n="HIAT:w" s="T326">diːr</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1183" n="HIAT:w" s="T327">kamandʼirdara</ts>
                  <nts id="Seg_1184" n="HIAT:ip">.</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1187" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1189" n="HIAT:w" s="T328">Onton</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1192" n="HIAT:w" s="T329">kördüm</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1195" n="HIAT:w" s="T330">do</ts>
                  <nts id="Seg_1196" n="HIAT:ip">,</nts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1199" n="HIAT:w" s="T331">nʼemsɨlarɨŋ</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1202" n="HIAT:w" s="T332">hubu</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1205" n="HIAT:w" s="T333">kelen</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1208" n="HIAT:w" s="T334">iheller</ts>
                  <nts id="Seg_1209" n="HIAT:ip">.</nts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1212" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1214" n="HIAT:w" s="T335">Tuːgu</ts>
                  <nts id="Seg_1215" n="HIAT:ip">,</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1218" n="HIAT:w" s="T336">tuːgu</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1221" n="HIAT:w" s="T337">kadʼɨgɨnahallar</ts>
                  <nts id="Seg_1222" n="HIAT:ip">,</nts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1225" n="HIAT:w" s="T338">sepseteller</ts>
                  <nts id="Seg_1226" n="HIAT:ip">,</nts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1229" n="HIAT:w" s="T339">töttörü</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1232" n="HIAT:w" s="T340">hüːrebin</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1235" n="HIAT:w" s="T341">dogottorum</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1238" n="HIAT:w" s="T342">barbɨt</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1241" n="HIAT:w" s="T343">hirderin</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1244" n="HIAT:w" s="T344">di͡ek</ts>
                  <nts id="Seg_1245" n="HIAT:ip">.</nts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1248" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1250" n="HIAT:w" s="T345">Bu͡opsa</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1253" n="HIAT:w" s="T346">hurdurgahallar</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1256" n="HIAT:w" s="T347">agaj</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1259" n="HIAT:w" s="T348">itinne</ts>
                  <nts id="Seg_1260" n="HIAT:ip">,</nts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1263" n="HIAT:w" s="T349">okko</ts>
                  <nts id="Seg_1264" n="HIAT:ip">,</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1267" n="HIAT:w" s="T350">ɨtɨ͡alɨːllar</ts>
                  <nts id="Seg_1268" n="HIAT:ip">,</nts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1271" n="HIAT:w" s="T351">ile</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1274" n="HIAT:w" s="T352">kimneːri</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1277" n="HIAT:w" s="T353">gɨnallar</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1280" n="HIAT:w" s="T354">bɨhɨlaːk</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1283" n="HIAT:w" s="T355">otto</ts>
                  <nts id="Seg_1284" n="HIAT:ip">…</nts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1287" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1289" n="HIAT:w" s="T357">Plʼen</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1292" n="HIAT:w" s="T358">ɨlaːrɨ</ts>
                  <nts id="Seg_1293" n="HIAT:ip">,</nts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1296" n="HIAT:w" s="T359">uhuguttan</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1299" n="HIAT:w" s="T360">transejga</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1302" n="HIAT:w" s="T361">kellim</ts>
                  <nts id="Seg_1303" n="HIAT:ip">.</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1306" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1308" n="HIAT:w" s="T362">Kelbitim</ts>
                  <nts id="Seg_1309" n="HIAT:ip">,</nts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1312" n="HIAT:w" s="T363">bejem</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1314" n="HIAT:ip">(</nts>
                  <ts e="T365" id="Seg_1316" n="HIAT:w" s="T364">dogot-</ts>
                  <nts id="Seg_1317" n="HIAT:ip">)</nts>
                  <nts id="Seg_1318" n="HIAT:ip">,</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1321" n="HIAT:w" s="T365">e</ts>
                  <nts id="Seg_1322" n="HIAT:ip">,</nts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1325" n="HIAT:w" s="T366">rotabar</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1328" n="HIAT:w" s="T367">tübespippin</ts>
                  <nts id="Seg_1329" n="HIAT:ip">.</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1332" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1334" n="HIAT:w" s="T368">Dʼe</ts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1337" n="HIAT:w" s="T369">ol</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1340" n="HIAT:w" s="T370">kimniːbin</ts>
                  <nts id="Seg_1341" n="HIAT:ip">,</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1344" n="HIAT:w" s="T371">kaja</ts>
                  <nts id="Seg_1345" n="HIAT:ip">,</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1348" n="HIAT:w" s="T372">kamnʼem</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1350" n="HIAT:ip">(</nts>
                  <nts id="Seg_1351" n="HIAT:ip">(</nts>
                  <ats e="T374" id="Seg_1352" n="HIAT:non-pho" s="T373">…</ats>
                  <nts id="Seg_1353" n="HIAT:ip">)</nts>
                  <nts id="Seg_1354" n="HIAT:ip">)</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1356" n="HIAT:ip">–</nts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1359" n="HIAT:w" s="T374">pulemʼotɨnan</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1362" n="HIAT:w" s="T375">kimni͡e</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1365" n="HIAT:w" s="T376">sraːzu</ts>
                  <nts id="Seg_1366" n="HIAT:ip">.</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1369" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1371" n="HIAT:w" s="T377">Hüːrbetten</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1374" n="HIAT:w" s="T378">taksa</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1377" n="HIAT:w" s="T379">kihini</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1380" n="HIAT:w" s="T380">tüherdiler</ts>
                  <nts id="Seg_1381" n="HIAT:ip">.</nts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1384" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1386" n="HIAT:w" s="T381">Horoktorbut</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1389" n="HIAT:w" s="T382">töttörü</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1392" n="HIAT:w" s="T383">hüːrdüler</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1395" n="HIAT:w" s="T384">ol</ts>
                  <nts id="Seg_1396" n="HIAT:ip">,</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1399" n="HIAT:w" s="T385">dаzor</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1402" n="HIAT:w" s="T386">bu͡olaːččɨ</ts>
                  <nts id="Seg_1403" n="HIAT:ip">.</nts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1406" n="HIAT:u" s="T387">
                  <nts id="Seg_1407" n="HIAT:ip">(</nts>
                  <ts e="T389" id="Seg_1409" n="HIAT:w" s="T387">Oro-</ts>
                  <nts id="Seg_1410" n="HIAT:ip">)</nts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1413" n="HIAT:w" s="T389">orotaŋ</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1416" n="HIAT:w" s="T390">barɨ͡aga</ts>
                  <nts id="Seg_1417" n="HIAT:ip">,</nts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1420" n="HIAT:w" s="T391">össü͡ö</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1423" n="HIAT:w" s="T392">kim</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1426" n="HIAT:w" s="T393">vpʼerʼedʼi</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1429" n="HIAT:w" s="T394">hɨldʼɨ͡aga</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1432" n="HIAT:w" s="T395">razvʼečʼčʼik</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1435" n="HIAT:w" s="T396">kördük</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1438" n="HIAT:w" s="T397">orotagɨn</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1441" n="HIAT:w" s="T398">agaj</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1444" n="HIAT:w" s="T399">ketiːgin</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1447" n="HIAT:w" s="T400">bu͡o</ts>
                  <nts id="Seg_1448" n="HIAT:ip">.</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1451" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1453" n="HIAT:w" s="T401">Innikiler</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1456" n="HIAT:w" s="T402">ikki</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1459" n="HIAT:w" s="T403">kihi</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1462" n="HIAT:w" s="T404">bu͡olar</ts>
                  <nts id="Seg_1463" n="HIAT:ip">,</nts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1466" n="HIAT:w" s="T405">kaŋas</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1469" n="HIAT:w" s="T406">di͡ek</ts>
                  <nts id="Seg_1470" n="HIAT:ip">,</nts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1473" n="HIAT:w" s="T407">uŋa</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1476" n="HIAT:w" s="T408">di͡ek</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1479" n="HIAT:w" s="T409">emi͡e</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1482" n="HIAT:w" s="T410">ikkiliː</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1485" n="HIAT:w" s="T411">kihi</ts>
                  <nts id="Seg_1486" n="HIAT:ip">.</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1489" n="HIAT:u" s="T412">
                  <nts id="Seg_1490" n="HIAT:ip">"</nts>
                  <ts e="T413" id="Seg_1492" n="HIAT:w" s="T412">Če</ts>
                  <nts id="Seg_1493" n="HIAT:ip">,</nts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1496" n="HIAT:w" s="T413">kajti͡ek</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1499" n="HIAT:w" s="T414">bu͡olu͡okputuj</ts>
                  <nts id="Seg_1500" n="HIAT:ip">"</nts>
                  <nts id="Seg_1501" n="HIAT:ip">,</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1504" n="HIAT:w" s="T415">diːr</ts>
                  <nts id="Seg_1505" n="HIAT:ip">.</nts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1508" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1510" n="HIAT:w" s="T416">Bu</ts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1513" n="HIAT:w" s="T417">dʼerʼevnʼaga</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1516" n="HIAT:w" s="T418">pravʼerʼajdɨːbɨt</ts>
                  <nts id="Seg_1517" n="HIAT:ip">,</nts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1520" n="HIAT:w" s="T419">ile</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1522" n="HIAT:ip">(</nts>
                  <ts e="T421" id="Seg_1524" n="HIAT:w" s="T420">kimneː-</ts>
                  <nts id="Seg_1525" n="HIAT:ip">)</nts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1528" n="HIAT:w" s="T421">kim</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1531" n="HIAT:w" s="T422">da</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1534" n="HIAT:w" s="T423">hu͡ok</ts>
                  <nts id="Seg_1535" n="HIAT:ip">.</nts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1538" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1540" n="HIAT:w" s="T424">Nʼemsɨ</ts>
                  <nts id="Seg_1541" n="HIAT:ip">,</nts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1544" n="HIAT:w" s="T425">eː</ts>
                  <nts id="Seg_1545" n="HIAT:ip">,</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1548" n="HIAT:w" s="T426">nʼemsalar</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1551" n="HIAT:w" s="T427">oŋu͡or</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1554" n="HIAT:w" s="T428">ere</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1557" n="HIAT:w" s="T429">köstöllör</ts>
                  <nts id="Seg_1558" n="HIAT:ip">.</nts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1561" n="HIAT:u" s="T430">
                  <nts id="Seg_1562" n="HIAT:ip">"</nts>
                  <ts e="T431" id="Seg_1564" n="HIAT:w" s="T430">Töttörü</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1567" n="HIAT:w" s="T431">barɨ͡agɨŋ</ts>
                  <nts id="Seg_1568" n="HIAT:ip">"</nts>
                  <nts id="Seg_1569" n="HIAT:ip">,</nts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1572" n="HIAT:w" s="T432">diː-diː</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1575" n="HIAT:w" s="T433">gɨnar</ts>
                  <nts id="Seg_1576" n="HIAT:ip">.</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1579" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1581" n="HIAT:w" s="T434">Če</ts>
                  <nts id="Seg_1582" n="HIAT:ip">,</nts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1585" n="HIAT:w" s="T435">ol</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1588" n="HIAT:w" s="T436">kimniː</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1591" n="HIAT:w" s="T437">hɨldʼammɨt</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1594" n="HIAT:w" s="T438">u͡oldʼastɨbɨt</ts>
                  <nts id="Seg_1595" n="HIAT:ip">,</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1597" n="HIAT:ip">(</nts>
                  <ts e="T440" id="Seg_1599" n="HIAT:w" s="T439">ol</ts>
                  <nts id="Seg_1600" n="HIAT:ip">)</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1603" n="HIAT:w" s="T440">ol</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1606" n="HIAT:w" s="T441">ürekpitin</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1609" n="HIAT:w" s="T442">taŋnarɨ</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1612" n="HIAT:w" s="T443">bardɨbɨt</ts>
                  <nts id="Seg_1613" n="HIAT:ip">,</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1616" n="HIAT:w" s="T444">hüːrük</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1619" n="HIAT:w" s="T445">taŋnarɨ</ts>
                  <nts id="Seg_1620" n="HIAT:ip">.</nts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1623" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1625" n="HIAT:w" s="T446">Ürektenen</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1628" n="HIAT:w" s="T447">e</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1631" n="HIAT:w" s="T448">barammɨt</ts>
                  <nts id="Seg_1632" n="HIAT:ip">,</nts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1635" n="HIAT:w" s="T449">eː</ts>
                  <nts id="Seg_1636" n="HIAT:ip">,</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1639" n="HIAT:w" s="T450">kimniːbit</ts>
                  <nts id="Seg_1640" n="HIAT:ip">.</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1643" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1645" n="HIAT:w" s="T451">Bugurduk</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1648" n="HIAT:w" s="T452">kimnere</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1651" n="HIAT:w" s="T453">kimneːbitter</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1654" n="HIAT:w" s="T454">bu</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1657" n="HIAT:w" s="T455">artʼilʼlʼerʼija</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1660" n="HIAT:w" s="T456">bu͡olaːččɨ</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1663" n="HIAT:w" s="T457">bu͡o</ts>
                  <nts id="Seg_1664" n="HIAT:ip">,</nts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1667" n="HIAT:w" s="T458">puːskalara</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1669" n="HIAT:ip">(</nts>
                  <ts e="T459.tx-ChVD.1" id="Seg_1671" n="HIAT:w" s="T459">taː</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1674" n="HIAT:w" s="T459.tx-ChVD.1">-</ts>
                  <nts id="Seg_1675" n="HIAT:ip">)</nts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1678" n="HIAT:w" s="T461">e</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1681" n="HIAT:w" s="T462">kimnere</ts>
                  <nts id="Seg_1682" n="HIAT:ip">,</nts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1685" n="HIAT:w" s="T463">attar</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1687" n="HIAT:ip">–</nts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1690" n="HIAT:w" s="T464">barɨta</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1693" n="HIAT:w" s="T465">kimnellibitter</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1696" n="HIAT:w" s="T466">bugurduk</ts>
                  <nts id="Seg_1697" n="HIAT:ip">.</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T471" id="Seg_1700" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1702" n="HIAT:w" s="T467">Perevaročɨː</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1705" n="HIAT:w" s="T468">kimneːbitter</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1708" n="HIAT:w" s="T469">onto</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1711" n="HIAT:w" s="T470">ölörtöːbütter</ts>
                  <nts id="Seg_1712" n="HIAT:ip">.</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1715" n="HIAT:u" s="T471">
                  <ts e="T472" id="Seg_1717" n="HIAT:w" s="T471">Onno</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1720" n="HIAT:w" s="T472">biːr</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1723" n="HIAT:w" s="T473">kihini</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1726" n="HIAT:w" s="T474">bullubut</ts>
                  <nts id="Seg_1727" n="HIAT:ip">.</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1730" n="HIAT:u" s="T475">
                  <ts e="T8" id="Seg_1732" n="HIAT:w" s="T475">Pаluzivoj</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1735" n="HIAT:w" s="T8">ile</ts>
                  <nts id="Seg_1736" n="HIAT:ip">,</nts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1739" n="HIAT:w" s="T476">kajdi͡ek</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1742" n="HIAT:w" s="T477">ere</ts>
                  <nts id="Seg_1743" n="HIAT:ip">,</nts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1746" n="HIAT:w" s="T478">arɨččɨ</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1749" n="HIAT:w" s="T479">haŋarar</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1752" n="HIAT:w" s="T480">onton</ts>
                  <nts id="Seg_1753" n="HIAT:ip">.</nts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_1756" n="HIAT:u" s="T482">
                  <nts id="Seg_1757" n="HIAT:ip">"</nts>
                  <ts e="T483" id="Seg_1759" n="HIAT:w" s="T482">Bu</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1762" n="HIAT:w" s="T483">haŋardɨː</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1765" n="HIAT:w" s="T484">tabak</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1768" n="HIAT:w" s="T485">tardar</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1771" n="HIAT:w" s="T486">ɨksata</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1774" n="HIAT:w" s="T487">bu͡olla</ts>
                  <nts id="Seg_1775" n="HIAT:ip">"</nts>
                  <nts id="Seg_1776" n="HIAT:ip">,</nts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1779" n="HIAT:w" s="T488">diː-diː</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1782" n="HIAT:w" s="T489">gɨnar</ts>
                  <nts id="Seg_1783" n="HIAT:ip">,</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1785" n="HIAT:ip">"</nts>
                  <ts e="T491" id="Seg_1787" n="HIAT:w" s="T490">bihigi</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1790" n="HIAT:w" s="T491">rotabɨtɨn</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1793" n="HIAT:w" s="T492">kimneːbittere</ts>
                  <nts id="Seg_1794" n="HIAT:ip">"</nts>
                  <nts id="Seg_1795" n="HIAT:ip">,</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1798" n="HIAT:w" s="T493">diːr</ts>
                  <nts id="Seg_1799" n="HIAT:ip">.</nts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_1802" n="HIAT:u" s="T494">
                  <nts id="Seg_1803" n="HIAT:ip">"</nts>
                  <ts e="T495" id="Seg_1805" n="HIAT:w" s="T494">Nʼemsite</ts>
                  <nts id="Seg_1806" n="HIAT:ip">,</nts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1809" n="HIAT:w" s="T495">heː</ts>
                  <nts id="Seg_1810" n="HIAT:ip">,</nts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1813" n="HIAT:w" s="T496">barbɨttara</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1816" n="HIAT:w" s="T497">anakkaːn</ts>
                  <nts id="Seg_1817" n="HIAT:ip">"</nts>
                  <nts id="Seg_1818" n="HIAT:ip">,</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1821" n="HIAT:w" s="T498">diːr</ts>
                  <nts id="Seg_1822" n="HIAT:ip">.</nts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_1825" n="HIAT:u" s="T499">
                  <nts id="Seg_1826" n="HIAT:ip">"</nts>
                  <ts e="T500" id="Seg_1828" n="HIAT:w" s="T499">Haŋarsɨmaŋ</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1831" n="HIAT:w" s="T500">tɨŋnɨk</ts>
                  <nts id="Seg_1832" n="HIAT:ip">"</nts>
                  <nts id="Seg_1833" n="HIAT:ip">,</nts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1836" n="HIAT:w" s="T501">diː-diː</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1839" n="HIAT:w" s="T502">gɨnar</ts>
                  <nts id="Seg_1840" n="HIAT:ip">.</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_1843" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_1845" n="HIAT:w" s="T503">Dʼe</ts>
                  <nts id="Seg_1846" n="HIAT:ip">,</nts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1849" n="HIAT:w" s="T504">ol</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1852" n="HIAT:w" s="T505">kimneːtibit</ts>
                  <nts id="Seg_1853" n="HIAT:ip">…</nts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_1856" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_1858" n="HIAT:w" s="T507">Dʼe</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1861" n="HIAT:w" s="T508">onno</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1864" n="HIAT:w" s="T509">diː</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1867" n="HIAT:w" s="T510">eː</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1869" n="HIAT:ip">(</nts>
                  <ts e="T512" id="Seg_1871" n="HIAT:w" s="T511">dogo-</ts>
                  <nts id="Seg_1872" n="HIAT:ip">)</nts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1875" n="HIAT:w" s="T512">ikki</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1878" n="HIAT:w" s="T513">kihini</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1881" n="HIAT:w" s="T514">ɨːtallar</ts>
                  <nts id="Seg_1882" n="HIAT:ip">,</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1885" n="HIAT:w" s="T515">onno</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1888" n="HIAT:w" s="T516">kim</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1891" n="HIAT:w" s="T517">bu͡olaːččɨ</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1893" n="HIAT:ip">–</nts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1896" n="HIAT:w" s="T518">sančʼas</ts>
                  <nts id="Seg_1897" n="HIAT:ip">.</nts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_1900" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_1902" n="HIAT:w" s="T519">Onto</ts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1904" n="HIAT:ip">"</nts>
                  <ts e="T521" id="Seg_1906" n="HIAT:w" s="T520">illi͡ekpit</ts>
                  <nts id="Seg_1907" n="HIAT:ip">"</nts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1910" n="HIAT:w" s="T521">diːller</ts>
                  <nts id="Seg_1911" n="HIAT:ip">,</nts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1913" n="HIAT:ip">"</nts>
                  <nts id="Seg_1914" n="HIAT:ip">(</nts>
                  <ts e="T523" id="Seg_1916" n="HIAT:w" s="T522">min</ts>
                  <nts id="Seg_1917" n="HIAT:ip">)</nts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1920" n="HIAT:w" s="T523">bihigi</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1923" n="HIAT:w" s="T524">enigin</ts>
                  <nts id="Seg_1924" n="HIAT:ip">.</nts>
                  <nts id="Seg_1925" n="HIAT:ip">"</nts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_1928" n="HIAT:u" s="T525">
                  <nts id="Seg_1929" n="HIAT:ip">"</nts>
                  <nts id="Seg_1930" n="HIAT:ip">(</nts>
                  <ts e="T527" id="Seg_1932" n="HIAT:w" s="T525">Tu-</ts>
                  <nts id="Seg_1933" n="HIAT:ip">)</nts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1936" n="HIAT:w" s="T527">minigin</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1939" n="HIAT:w" s="T528">tu͡ok</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1942" n="HIAT:w" s="T529">kɨhallɨmaŋ</ts>
                  <nts id="Seg_1943" n="HIAT:ip">"</nts>
                  <nts id="Seg_1944" n="HIAT:ip">,</nts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1947" n="HIAT:w" s="T530">diːr</ts>
                  <nts id="Seg_1948" n="HIAT:ip">,</nts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1950" n="HIAT:ip">"</nts>
                  <ts e="T532" id="Seg_1952" n="HIAT:w" s="T531">min</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1955" n="HIAT:w" s="T532">e</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1958" n="HIAT:w" s="T533">hin</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1960" n="HIAT:ip">(</nts>
                  <ts e="T535" id="Seg_1962" n="HIAT:w" s="T534">kihi</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1965" n="HIAT:w" s="T535">bu͡olu͡ok-</ts>
                  <nts id="Seg_1966" n="HIAT:ip">)</nts>
                  <nts id="Seg_1967" n="HIAT:ip">,</nts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1970" n="HIAT:w" s="T536">e</ts>
                  <nts id="Seg_1971" n="HIAT:ip">,</nts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1974" n="HIAT:w" s="T537">kihi</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1977" n="HIAT:w" s="T538">bu͡olbatakpɨn</ts>
                  <nts id="Seg_1978" n="HIAT:ip">"</nts>
                  <nts id="Seg_1979" n="HIAT:ip">,</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1982" n="HIAT:w" s="T539">diːr</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1985" n="HIAT:w" s="T540">bu͡o</ts>
                  <nts id="Seg_1986" n="HIAT:ip">,</nts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1988" n="HIAT:ip">"</nts>
                  <nts id="Seg_1989" n="HIAT:ip">(</nts>
                  <ts e="T542" id="Seg_1991" n="HIAT:w" s="T541">ölü͡ö-</ts>
                  <nts id="Seg_1992" n="HIAT:ip">)</nts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1995" n="HIAT:w" s="T542">ölü͡öm</ts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1998" n="HIAT:w" s="T543">anɨ</ts>
                  <nts id="Seg_1999" n="HIAT:ip">.</nts>
                  <nts id="Seg_2000" n="HIAT:ip">"</nts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T550" id="Seg_2003" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_2005" n="HIAT:w" s="T544">Kihiler</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2008" n="HIAT:w" s="T545">holbok</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2011" n="HIAT:w" s="T546">kördük</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2014" n="HIAT:w" s="T547">hɨtallar</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2017" n="HIAT:w" s="T548">ile</ts>
                  <nts id="Seg_2018" n="HIAT:ip">,</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2021" n="HIAT:w" s="T549">olorton</ts>
                  <nts id="Seg_2022" n="HIAT:ip">.</nts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T553" id="Seg_2025" n="HIAT:u" s="T550">
                  <ts e="T551" id="Seg_2027" n="HIAT:w" s="T550">Honon</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2030" n="HIAT:w" s="T551">ol</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2033" n="HIAT:w" s="T552">kimneːtiler</ts>
                  <nts id="Seg_2034" n="HIAT:ip">.</nts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T573" id="Seg_2037" n="HIAT:u" s="T553">
                  <ts e="T554" id="Seg_2039" n="HIAT:w" s="T553">Ontugun</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2042" n="HIAT:w" s="T554">iltiler</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2045" n="HIAT:w" s="T555">ikki</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2048" n="HIAT:w" s="T556">kihi</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2051" n="HIAT:w" s="T557">kimi͡eke</ts>
                  <nts id="Seg_2052" n="HIAT:ip">,</nts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2055" n="HIAT:w" s="T558">ontuŋ</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2058" n="HIAT:w" s="T559">otto</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2061" n="HIAT:w" s="T560">di͡ebit</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2064" n="HIAT:w" s="T561">ühü</ts>
                  <nts id="Seg_2065" n="HIAT:ip">,</nts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2067" n="HIAT:ip">"</nts>
                  <ts e="T563" id="Seg_2069" n="HIAT:w" s="T562">ol</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563.tx-ChVD.1" id="Seg_2072" n="HIAT:w" s="T563">vsʼo</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2075" n="HIAT:w" s="T563.tx-ChVD.1">ravno</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2078" n="HIAT:w" s="T565">tiri͡ereme</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2081" n="HIAT:w" s="T566">e</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2084" n="HIAT:w" s="T567">kimneːn</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2087" n="HIAT:w" s="T568">ol</ts>
                  <nts id="Seg_2088" n="HIAT:ip">"</nts>
                  <nts id="Seg_2089" n="HIAT:ip">,</nts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2092" n="HIAT:w" s="T569">onton</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2095" n="HIAT:w" s="T570">töttörü</ts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2098" n="HIAT:w" s="T571">kelliler</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2101" n="HIAT:w" s="T572">oloruŋ</ts>
                  <nts id="Seg_2102" n="HIAT:ip">.</nts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2105" n="HIAT:u" s="T573">
                  <nts id="Seg_2106" n="HIAT:ip">"</nts>
                  <ts e="T574" id="Seg_2108" n="HIAT:w" s="T573">Hin</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2111" n="HIAT:w" s="T574">kihi</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2114" n="HIAT:w" s="T575">bu͡olu͡om</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2117" n="HIAT:w" s="T576">hu͡oga</ts>
                  <nts id="Seg_2118" n="HIAT:ip">,</nts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2121" n="HIAT:w" s="T577">tugu</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2124" n="HIAT:w" s="T578">minigin</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2127" n="HIAT:w" s="T579">gɨtta</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2129" n="HIAT:ip">(</nts>
                  <ts e="T580.tx-ChVD.1" id="Seg_2131" n="HIAT:w" s="T580">vаzʼiz</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2134" n="HIAT:w" s="T580.tx-ChVD.1">-</ts>
                  <nts id="Seg_2135" n="HIAT:ip">)</nts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2138" n="HIAT:w" s="T582">kimneːgit</ts>
                  <nts id="Seg_2139" n="HIAT:ip">"</nts>
                  <nts id="Seg_2140" n="HIAT:ip">,</nts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2143" n="HIAT:w" s="T583">diːr</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2146" n="HIAT:w" s="T584">ühü</ts>
                  <nts id="Seg_2147" n="HIAT:ip">.</nts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T589" id="Seg_2150" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_2152" n="HIAT:w" s="T585">Harsi͡erda</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2155" n="HIAT:w" s="T586">immit</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2158" n="HIAT:w" s="T587">haŋardɨː</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2161" n="HIAT:w" s="T588">taksar</ts>
                  <nts id="Seg_2162" n="HIAT:ip">.</nts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T599" id="Seg_2165" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_2167" n="HIAT:w" s="T589">Taksan</ts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2169" n="HIAT:ip">(</nts>
                  <ts e="T590.tx-ChVD.1" id="Seg_2171" n="HIAT:w" s="T590">erde</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2174" n="HIAT:w" s="T590.tx-ChVD.1">-</ts>
                  <nts id="Seg_2175" n="HIAT:ip">)</nts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2178" n="HIAT:w" s="T592">eː</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2181" n="HIAT:w" s="T593">kimneːn</ts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2184" n="HIAT:w" s="T594">erdegine</ts>
                  <nts id="Seg_2185" n="HIAT:ip">,</nts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2188" n="HIAT:w" s="T595">tu͡ok</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2191" n="HIAT:w" s="T596">ere</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2194" n="HIAT:w" s="T597">lʼejtʼenaːnɨ</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2197" n="HIAT:w" s="T598">körüstübüt</ts>
                  <nts id="Seg_2198" n="HIAT:ip">.</nts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2201" n="HIAT:u" s="T599">
                  <ts e="T600" id="Seg_2203" n="HIAT:w" s="T599">Ontubut</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2206" n="HIAT:w" s="T600">diːr</ts>
                  <nts id="Seg_2207" n="HIAT:ip">:</nts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2210" n="HIAT:u" s="T601">
                  <nts id="Seg_2211" n="HIAT:ip">"</nts>
                  <ts e="T602" id="Seg_2213" n="HIAT:w" s="T601">Kannɨk</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2216" n="HIAT:w" s="T602">čʼaːstaŋŋɨnɨj</ts>
                  <nts id="Seg_2217" n="HIAT:ip">?</nts>
                  <nts id="Seg_2218" n="HIAT:ip">"</nts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_2221" n="HIAT:u" s="T603">
                  <nts id="Seg_2222" n="HIAT:ip">"</nts>
                  <ts e="T604" id="Seg_2224" n="HIAT:w" s="T603">Ehigi</ts>
                  <nts id="Seg_2225" n="HIAT:ip">,</nts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2228" n="HIAT:w" s="T604">ehigitten</ts>
                  <nts id="Seg_2229" n="HIAT:ip">"</nts>
                  <nts id="Seg_2230" n="HIAT:ip">,</nts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2233" n="HIAT:w" s="T605">diːbit</ts>
                  <nts id="Seg_2234" n="HIAT:ip">.</nts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T610" id="Seg_2237" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_2239" n="HIAT:w" s="T606">Diːr</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2242" n="HIAT:w" s="T607">onton</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2245" n="HIAT:w" s="T608">biːrbit</ts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2248" n="HIAT:w" s="T609">onton</ts>
                  <nts id="Seg_2249" n="HIAT:ip">.</nts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2252" n="HIAT:u" s="T610">
                  <nts id="Seg_2253" n="HIAT:ip">"</nts>
                  <ts e="T611" id="Seg_2255" n="HIAT:w" s="T610">Oččogo</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2258" n="HIAT:w" s="T611">minigin</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2261" n="HIAT:w" s="T612">gɨtta</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2264" n="HIAT:w" s="T613">hüːrüŋ</ts>
                  <nts id="Seg_2265" n="HIAT:ip">"</nts>
                  <nts id="Seg_2266" n="HIAT:ip">,</nts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2269" n="HIAT:w" s="T614">diːr</ts>
                  <nts id="Seg_2270" n="HIAT:ip">.</nts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2273" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_2275" n="HIAT:w" s="T615">Inni</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2278" n="HIAT:w" s="T616">di͡ek</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2281" n="HIAT:w" s="T617">xаdʼilʼi</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2284" n="HIAT:w" s="T618">ere</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2287" n="HIAT:w" s="T619">kimneːmi͡ekke</ts>
                  <nts id="Seg_2288" n="HIAT:ip">.</nts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2291" n="HIAT:u" s="T622">
                  <ts e="T623" id="Seg_2293" n="HIAT:w" s="T622">Svʼaz</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2296" n="HIAT:w" s="T623">ɨla</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2299" n="HIAT:w" s="T624">kimniːr</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2302" n="HIAT:w" s="T625">eni</ts>
                  <nts id="Seg_2303" n="HIAT:ip">.</nts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T631" id="Seg_2306" n="HIAT:u" s="T626">
                  <nts id="Seg_2307" n="HIAT:ip">"</nts>
                  <ts e="T627" id="Seg_2309" n="HIAT:w" s="T626">Dʼe</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2312" n="HIAT:w" s="T627">onton</ts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2315" n="HIAT:w" s="T628">ol</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2318" n="HIAT:w" s="T629">kimniː</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2321" n="HIAT:w" s="T630">hatɨːbɨt</ts>
                  <nts id="Seg_2322" n="HIAT:ip">.</nts>
                  <nts id="Seg_2323" n="HIAT:ip">"</nts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T638" id="Seg_2326" n="HIAT:u" s="T631">
                  <ts e="T632" id="Seg_2328" n="HIAT:w" s="T631">Kas</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2331" n="HIAT:w" s="T632">da</ts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2334" n="HIAT:w" s="T633">künü</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2337" n="HIAT:w" s="T634">ol</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2340" n="HIAT:w" s="T635">hɨrɨttɨbɨt</ts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2343" n="HIAT:w" s="T636">ol</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2346" n="HIAT:w" s="T637">kurduk</ts>
                  <nts id="Seg_2347" n="HIAT:ip">.</nts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2350" n="HIAT:u" s="T638">
                  <ts e="T639" id="Seg_2352" n="HIAT:w" s="T638">Rotabɨtɨn</ts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2355" n="HIAT:w" s="T639">daː</ts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2358" n="HIAT:w" s="T640">bulbappɨt</ts>
                  <nts id="Seg_2359" n="HIAT:ip">.</nts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2362" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2364" n="HIAT:w" s="T641">Dʼe</ts>
                  <nts id="Seg_2365" n="HIAT:ip">,</nts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2368" n="HIAT:w" s="T642">onton</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2371" n="HIAT:w" s="T643">kojukkutun</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2373" n="HIAT:ip">(</nts>
                  <ts e="T645" id="Seg_2375" n="HIAT:w" s="T644">kimneːti-</ts>
                  <nts id="Seg_2376" n="HIAT:ip">)</nts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2379" n="HIAT:w" s="T645">eː</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2382" n="HIAT:w" s="T646">bullubut</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2385" n="HIAT:w" s="T647">emi͡e</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2388" n="HIAT:w" s="T648">e</ts>
                  <nts id="Seg_2389" n="HIAT:ip">.</nts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T660" id="Seg_2392" n="HIAT:u" s="T649">
                  <nts id="Seg_2393" n="HIAT:ip">(</nts>
                  <ts e="T651" id="Seg_2395" n="HIAT:w" s="T649">Ü-</ts>
                  <nts id="Seg_2396" n="HIAT:ip">)</nts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2399" n="HIAT:w" s="T651">ühüs</ts>
                  <nts id="Seg_2400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2402" n="HIAT:w" s="T652">kümmütüger</ts>
                  <nts id="Seg_2403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2405" n="HIAT:w" s="T653">duː</ts>
                  <nts id="Seg_2406" n="HIAT:ip">,</nts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2409" n="HIAT:w" s="T654">kahɨspɨtɨgar</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2412" n="HIAT:w" s="T655">duː</ts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2415" n="HIAT:w" s="T656">ɨntak</ts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2418" n="HIAT:w" s="T657">barabɨt</ts>
                  <nts id="Seg_2419" n="HIAT:ip">,</nts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2422" n="HIAT:w" s="T658">onno</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2425" n="HIAT:w" s="T659">kimi͡eke</ts>
                  <nts id="Seg_2426" n="HIAT:ip">.</nts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2429" n="HIAT:u" s="T660">
                  <ts e="T661" id="Seg_2431" n="HIAT:w" s="T660">Ol</ts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2434" n="HIAT:w" s="T661">ürekpitin</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2437" n="HIAT:w" s="T662">taksabɨt</ts>
                  <nts id="Seg_2438" n="HIAT:ip">,</nts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2441" n="HIAT:w" s="T663">dʼe</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2444" n="HIAT:w" s="T664">onton</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2447" n="HIAT:w" s="T665">ol</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2450" n="HIAT:w" s="T666">u͡oldʼahabɨt</ts>
                  <nts id="Seg_2451" n="HIAT:ip">,</nts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2454" n="HIAT:w" s="T667">ol</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2457" n="HIAT:w" s="T668">ɨnaraːttan</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2460" n="HIAT:w" s="T669">ɨtɨ͡alɨːllar</ts>
                  <nts id="Seg_2461" n="HIAT:ip">,</nts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2464" n="HIAT:w" s="T670">a</ts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2467" n="HIAT:w" s="T671">bihigi</ts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2470" n="HIAT:w" s="T672">betereːtten</ts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2473" n="HIAT:w" s="T673">emi͡e</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2476" n="HIAT:w" s="T674">ɨtɨ͡alahabɨt</ts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2479" n="HIAT:w" s="T675">bu͡olla</ts>
                  <nts id="Seg_2480" n="HIAT:ip">.</nts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2483" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2485" n="HIAT:w" s="T676">Dʼe</ts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2488" n="HIAT:w" s="T677">ol</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2491" n="HIAT:w" s="T678">kimneːtibit</ts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2494" n="HIAT:w" s="T679">daː</ts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2497" n="HIAT:w" s="T680">onno</ts>
                  <nts id="Seg_2498" n="HIAT:ip">,</nts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2501" n="HIAT:w" s="T681">kajdi͡ek</ts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2504" n="HIAT:w" s="T682">daː</ts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2507" n="HIAT:w" s="T683">kimneːbetter</ts>
                  <nts id="Seg_2508" n="HIAT:ip">,</nts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2511" n="HIAT:w" s="T684">ɨːppattar</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2514" n="HIAT:w" s="T685">ol</ts>
                  <nts id="Seg_2515" n="HIAT:ip">.</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2518" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2520" n="HIAT:w" s="T686">Harsi͡erda</ts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2523" n="HIAT:w" s="T687">kɨ͡attaːk</ts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2526" n="HIAT:w" s="T688">kelliler</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2529" n="HIAT:w" s="T689">ol</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2532" n="HIAT:w" s="T690">di͡ek</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2535" n="HIAT:w" s="T691">innibititten</ts>
                  <nts id="Seg_2536" n="HIAT:ip">,</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2539" n="HIAT:w" s="T692">bu</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2541" n="HIAT:ip">(</nts>
                  <ts e="T693.tx-ChVD.1" id="Seg_2543" n="HIAT:w" s="T693">bi</ts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2546" n="HIAT:w" s="T693.tx-ChVD.1">-</ts>
                  <nts id="Seg_2547" n="HIAT:ip">)</nts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2550" n="HIAT:w" s="T695">kamanda</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2553" n="HIAT:w" s="T696">bi͡ereller</ts>
                  <nts id="Seg_2554" n="HIAT:ip">:</nts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T704" id="Seg_2557" n="HIAT:u" s="T697">
                  <nts id="Seg_2558" n="HIAT:ip">"</nts>
                  <ts e="T698" id="Seg_2560" n="HIAT:w" s="T697">Nu</ts>
                  <nts id="Seg_2561" n="HIAT:ip">,</nts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2564" n="HIAT:w" s="T698">otto</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2567" n="HIAT:w" s="T699">üs</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2570" n="HIAT:w" s="T700">čʼaːska</ts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2573" n="HIAT:w" s="T701">Brʼanskajɨ</ts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2576" n="HIAT:w" s="T702">zanʼimajdɨ͡akkɨtɨn</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2579" n="HIAT:w" s="T703">naːda</ts>
                  <nts id="Seg_2580" n="HIAT:ip">.</nts>
                  <nts id="Seg_2581" n="HIAT:ip">"</nts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2584" n="HIAT:u" s="T704">
                  <ts e="T705" id="Seg_2586" n="HIAT:w" s="T704">Dʼe</ts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2589" n="HIAT:w" s="T705">onno</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2592" n="HIAT:w" s="T706">bugurduk</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2595" n="HIAT:w" s="T707">tu͡ok</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2598" n="HIAT:w" s="T708">ere</ts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2601" n="HIAT:w" s="T709">hɨːr</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2604" n="HIAT:w" s="T710">bu͡olar</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2606" n="HIAT:ip">–</nts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2609" n="HIAT:w" s="T711">dʼerbi</ts>
                  <nts id="Seg_2610" n="HIAT:ip">.</nts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_2613" n="HIAT:u" s="T712">
                  <nts id="Seg_2614" n="HIAT:ip">(</nts>
                  <ts e="T712.tx-ChVD.1" id="Seg_2616" n="HIAT:w" s="T712">Hu</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2619" n="HIAT:w" s="T712.tx-ChVD.1">-</ts>
                  <nts id="Seg_2620" n="HIAT:ip">)</nts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2623" n="HIAT:w" s="T714">dʼerbini</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2626" n="HIAT:w" s="T715">e</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2629" n="HIAT:w" s="T716">kimneːtibit</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2632" n="HIAT:w" s="T717">da</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2635" n="HIAT:w" s="T718">emi͡e</ts>
                  <nts id="Seg_2636" n="HIAT:ip">.</nts>
                  <nts id="Seg_2637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T735" id="Seg_2639" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_2641" n="HIAT:w" s="T719">Emi͡e</ts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2644" n="HIAT:w" s="T720">hɨraja</ts>
                  <nts id="Seg_2645" n="HIAT:ip">,</nts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2647" n="HIAT:ip">(</nts>
                  <ts e="T723" id="Seg_2649" n="HIAT:w" s="T721">hu͡o-</ts>
                  <nts id="Seg_2650" n="HIAT:ip">)</nts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2653" n="HIAT:w" s="T723">innitten</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2656" n="HIAT:w" s="T724">kimniːller</ts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2659" n="HIAT:w" s="T725">bu͡o</ts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2661" n="HIAT:ip">(</nts>
                  <ts e="T728" id="Seg_2663" n="HIAT:w" s="T726">uhugut-</ts>
                  <nts id="Seg_2664" n="HIAT:ip">)</nts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2667" n="HIAT:w" s="T728">nʼemʼester</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2670" n="HIAT:w" s="T729">bugurduk</ts>
                  <nts id="Seg_2671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2673" n="HIAT:w" s="T730">hɨtallar</ts>
                  <nts id="Seg_2674" n="HIAT:ip">,</nts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2677" n="HIAT:w" s="T731">аbаrаnʼatsa</ts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2680" n="HIAT:w" s="T732">gɨna</ts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2683" n="HIAT:w" s="T733">hɨtallar</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2686" n="HIAT:w" s="T734">bu͡o</ts>
                  <nts id="Seg_2687" n="HIAT:ip">.</nts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_2690" n="HIAT:u" s="T735">
                  <ts e="T736" id="Seg_2692" n="HIAT:w" s="T735">A</ts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2695" n="HIAT:w" s="T736">bihigi</ts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2698" n="HIAT:w" s="T737">bu</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2701" n="HIAT:w" s="T738">dʼi͡egitten</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2704" n="HIAT:w" s="T739">emi͡e</ts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2707" n="HIAT:w" s="T740">ɨtɨ͡alahabɨt</ts>
                  <nts id="Seg_2708" n="HIAT:ip">.</nts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T749" id="Seg_2711" n="HIAT:u" s="T741">
                  <ts e="T742" id="Seg_2713" n="HIAT:w" s="T741">Dʼe</ts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2716" n="HIAT:w" s="T742">onton</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2719" n="HIAT:w" s="T743">bu</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2722" n="HIAT:w" s="T744">tüːnü</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2725" n="HIAT:w" s="T745">huptu</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2728" n="HIAT:w" s="T746">hɨtɨː</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2731" n="HIAT:w" s="T747">munna</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2734" n="HIAT:w" s="T748">bu͡o</ts>
                  <nts id="Seg_2735" n="HIAT:ip">.</nts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_2738" n="HIAT:u" s="T749">
                  <ts e="T750" id="Seg_2740" n="HIAT:w" s="T749">Tüːnü</ts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2743" n="HIAT:w" s="T750">u͡onnar</ts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2746" n="HIAT:w" s="T751">duː</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2749" n="HIAT:w" s="T752">ikkiler</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2752" n="HIAT:w" s="T753">di͡ek</ts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2755" n="HIAT:w" s="T754">duː</ts>
                  <nts id="Seg_2756" n="HIAT:ip">,</nts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2759" n="HIAT:w" s="T755">ili</ts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2762" n="HIAT:w" s="T756">čʼaːstar</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2765" n="HIAT:w" s="T757">di͡ek</ts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2768" n="HIAT:w" s="T758">duː</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2771" n="HIAT:w" s="T759">ol</ts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2774" n="HIAT:w" s="T760">rakʼetɨ</ts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2777" n="HIAT:w" s="T761">ɨːtallar</ts>
                  <nts id="Seg_2778" n="HIAT:ip">.</nts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T772" id="Seg_2781" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_2783" n="HIAT:w" s="T762">Rakʼetɨ</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2786" n="HIAT:w" s="T763">ɨːttagɨna</ts>
                  <nts id="Seg_2787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2789" n="HIAT:w" s="T764">oččogo</ts>
                  <nts id="Seg_2790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2792" n="HIAT:w" s="T765">bu</ts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2795" n="HIAT:w" s="T766">barɨta</ts>
                  <nts id="Seg_2796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2798" n="HIAT:w" s="T767">аbаrаnʼassa</ts>
                  <nts id="Seg_2799" n="HIAT:ip">,</nts>
                  <nts id="Seg_2800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2802" n="HIAT:w" s="T768">kimniː</ts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2805" n="HIAT:w" s="T769">hɨtallar</ts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2808" n="HIAT:w" s="T770">barɨta</ts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2811" n="HIAT:w" s="T771">köstöllör</ts>
                  <nts id="Seg_2812" n="HIAT:ip">.</nts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T777" id="Seg_2815" n="HIAT:u" s="T772">
                  <ts e="T773" id="Seg_2817" n="HIAT:w" s="T772">Iliːbin</ts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2820" n="HIAT:w" s="T773">taptaran</ts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2823" n="HIAT:w" s="T774">bu</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2826" n="HIAT:w" s="T775">dʼüːleːbetim</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2829" n="HIAT:w" s="T776">onton</ts>
                  <nts id="Seg_2830" n="HIAT:ip">.</nts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_2833" n="HIAT:u" s="T777">
                  <ts e="T778" id="Seg_2835" n="HIAT:w" s="T777">Ol</ts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2838" n="HIAT:w" s="T778">muntubar</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2840" n="HIAT:ip">(</nts>
                  <ts e="T780" id="Seg_2842" n="HIAT:w" s="T779">tap-</ts>
                  <nts id="Seg_2843" n="HIAT:ip">)</nts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2846" n="HIAT:w" s="T780">kimneːbit</ts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2849" n="HIAT:w" s="T781">ol</ts>
                  <nts id="Seg_2850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2852" n="HIAT:w" s="T782">huptu</ts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2855" n="HIAT:w" s="T783">kötön</ts>
                  <nts id="Seg_2856" n="HIAT:ip">.</nts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T794" id="Seg_2859" n="HIAT:u" s="T784">
                  <ts e="T785" id="Seg_2861" n="HIAT:w" s="T784">Biːr</ts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2864" n="HIAT:w" s="T785">mina</ts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2867" n="HIAT:w" s="T786">kelle</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2870" n="HIAT:w" s="T787">da</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2873" n="HIAT:w" s="T788">barɨbɨtɨn</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2876" n="HIAT:w" s="T789">holoːbut</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2879" n="HIAT:w" s="T790">ol</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2881" n="HIAT:ip">–</nts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2884" n="HIAT:w" s="T791">u͡on</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2887" n="HIAT:w" s="T792">orduk</ts>
                  <nts id="Seg_2888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2890" n="HIAT:w" s="T793">kihini</ts>
                  <nts id="Seg_2891" n="HIAT:ip">.</nts>
                  <nts id="Seg_2892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T800" id="Seg_2894" n="HIAT:u" s="T794">
                  <ts e="T795" id="Seg_2896" n="HIAT:w" s="T794">Haːbɨn</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2899" n="HIAT:w" s="T795">burtuk</ts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2902" n="HIAT:w" s="T796">tutarbar</ts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2905" n="HIAT:w" s="T797">öjdönnüm</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2908" n="HIAT:w" s="T798">onton</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2911" n="HIAT:w" s="T799">innʼe</ts>
                  <nts id="Seg_2912" n="HIAT:ip">.</nts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T804" id="Seg_2915" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_2917" n="HIAT:w" s="T800">Innʼe</ts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2920" n="HIAT:w" s="T801">turabɨn</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2923" n="HIAT:w" s="T802">tu͡ok</ts>
                  <nts id="Seg_2924" n="HIAT:ip">,</nts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2927" n="HIAT:w" s="T803">tobuktuː</ts>
                  <nts id="Seg_2928" n="HIAT:ip">.</nts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T808" id="Seg_2931" n="HIAT:u" s="T804">
                  <ts e="T805" id="Seg_2933" n="HIAT:w" s="T804">Dʼe</ts>
                  <nts id="Seg_2934" n="HIAT:ip">,</nts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2937" n="HIAT:w" s="T805">onton</ts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2940" n="HIAT:w" s="T806">tüstüm</ts>
                  <nts id="Seg_2941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2943" n="HIAT:w" s="T807">bugurduk</ts>
                  <nts id="Seg_2944" n="HIAT:ip">.</nts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T821" id="Seg_2947" n="HIAT:u" s="T808">
                  <ts e="T809" id="Seg_2949" n="HIAT:w" s="T808">Dʼe</ts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2952" n="HIAT:w" s="T809">onton</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2955" n="HIAT:w" s="T810">kojut</ts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2958" n="HIAT:w" s="T811">kördüm</ts>
                  <nts id="Seg_2959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2961" n="HIAT:w" s="T812">bu͡o</ts>
                  <nts id="Seg_2962" n="HIAT:ip">,</nts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2965" n="HIAT:w" s="T813">čömüjelerim</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2968" n="HIAT:w" s="T814">barɨta</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2971" n="HIAT:w" s="T815">halɨbɨrɨː</ts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2974" n="HIAT:w" s="T816">hɨldʼallar</ts>
                  <nts id="Seg_2975" n="HIAT:ip">,</nts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2978" n="HIAT:w" s="T817">kaːnɨm</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2981" n="HIAT:w" s="T818">usta</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2984" n="HIAT:w" s="T819">hɨtar</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_2987" n="HIAT:w" s="T820">bugurduk</ts>
                  <nts id="Seg_2988" n="HIAT:ip">.</nts>
                  <nts id="Seg_2989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T826" id="Seg_2991" n="HIAT:u" s="T821">
                  <ts e="T822" id="Seg_2993" n="HIAT:w" s="T821">Dʼe</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2996" n="HIAT:w" s="T822">onton</ts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2999" n="HIAT:w" s="T823">kurbun</ts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3002" n="HIAT:w" s="T824">uhullum</ts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3005" n="HIAT:w" s="T825">bugurduk</ts>
                  <nts id="Seg_3006" n="HIAT:ip">.</nts>
                  <nts id="Seg_3007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T841" id="Seg_3009" n="HIAT:u" s="T826">
                  <ts e="T827" id="Seg_3011" n="HIAT:w" s="T826">Kurbun</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3014" n="HIAT:w" s="T827">uhulan</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3017" n="HIAT:w" s="T828">baraːmmɨn</ts>
                  <nts id="Seg_3018" n="HIAT:ip">,</nts>
                  <nts id="Seg_3019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3021" n="HIAT:w" s="T829">burtuk</ts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3024" n="HIAT:w" s="T830">kimneːtim</ts>
                  <nts id="Seg_3025" n="HIAT:ip">,</nts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3028" n="HIAT:w" s="T831">enʼi</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3031" n="HIAT:w" s="T832">kimmer</ts>
                  <nts id="Seg_3032" n="HIAT:ip">,</nts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3035" n="HIAT:w" s="T833">mu͡ojbar</ts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3038" n="HIAT:w" s="T834">innʼe</ts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3040" n="HIAT:ip">(</nts>
                  <ts e="T835.tx-ChVD.1" id="Seg_3042" n="HIAT:w" s="T835">kett</ts>
                  <nts id="Seg_3043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3045" n="HIAT:w" s="T835.tx-ChVD.1">-</ts>
                  <nts id="Seg_3046" n="HIAT:ip">)</nts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3049" n="HIAT:w" s="T837">kimneːtim</ts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3052" n="HIAT:w" s="T838">bu</ts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3055" n="HIAT:w" s="T839">baːjan</ts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3058" n="HIAT:w" s="T840">baraːmmɨn</ts>
                  <nts id="Seg_3059" n="HIAT:ip">.</nts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T851" id="Seg_3062" n="HIAT:u" s="T841">
                  <ts e="T842" id="Seg_3064" n="HIAT:w" s="T841">Onton</ts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3067" n="HIAT:w" s="T842">bu</ts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3070" n="HIAT:w" s="T843">pakʼettar</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3073" n="HIAT:w" s="T844">bu͡olaːččɨlar</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3076" n="HIAT:w" s="T845">každɨj</ts>
                  <nts id="Seg_3077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3079" n="HIAT:w" s="T846">pʼerʼevʼazаčʼnɨj</ts>
                  <nts id="Seg_3080" n="HIAT:ip">,</nts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3083" n="HIAT:w" s="T847">dʼe</ts>
                  <nts id="Seg_3084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3086" n="HIAT:w" s="T848">ontubunan</ts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3089" n="HIAT:w" s="T849">erijdim</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3092" n="HIAT:w" s="T850">bugurduk</ts>
                  <nts id="Seg_3093" n="HIAT:ip">.</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T859" id="Seg_3096" n="HIAT:u" s="T851">
                  <ts e="T852" id="Seg_3098" n="HIAT:w" s="T851">Onton</ts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3101" n="HIAT:w" s="T852">ommu͡otka</ts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3104" n="HIAT:w" s="T853">iti</ts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3107" n="HIAT:w" s="T854">bočinkagar</ts>
                  <nts id="Seg_3108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3110" n="HIAT:w" s="T855">kim</ts>
                  <nts id="Seg_3111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3113" n="HIAT:w" s="T856">bu͡olaːččɨ</ts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3115" n="HIAT:ip">(</nts>
                  <ts e="T859" id="Seg_3117" n="HIAT:w" s="T857">eri-</ts>
                  <nts id="Seg_3118" n="HIAT:ip">)</nts>
                  <nts id="Seg_3119" n="HIAT:ip">.</nts>
                  <nts id="Seg_3120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T862" id="Seg_3122" n="HIAT:u" s="T859">
                  <ts e="T860" id="Seg_3124" n="HIAT:w" s="T859">Onu</ts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3127" n="HIAT:w" s="T860">ommotkabɨnan</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3130" n="HIAT:w" s="T861">erijdim</ts>
                  <nts id="Seg_3131" n="HIAT:ip">.</nts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T867" id="Seg_3134" n="HIAT:u" s="T862">
                  <nts id="Seg_3135" n="HIAT:ip">"</nts>
                  <ts e="T863" id="Seg_3137" n="HIAT:w" s="T862">Bar</ts>
                  <nts id="Seg_3138" n="HIAT:ip">"</nts>
                  <nts id="Seg_3139" n="HIAT:ip">,</nts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3142" n="HIAT:w" s="T863">diːller</ts>
                  <nts id="Seg_3143" n="HIAT:ip">,</nts>
                  <nts id="Seg_3144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3145" n="HIAT:ip">"</nts>
                  <ts e="T865" id="Seg_3147" n="HIAT:w" s="T864">kim</ts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3150" n="HIAT:w" s="T865">kamandʼir</ts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3153" n="HIAT:w" s="T866">rotagar</ts>
                  <nts id="Seg_3154" n="HIAT:ip">.</nts>
                  <nts id="Seg_3155" n="HIAT:ip">"</nts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T872" id="Seg_3158" n="HIAT:u" s="T867">
                  <ts e="T868" id="Seg_3160" n="HIAT:w" s="T867">Hol</ts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3163" n="HIAT:w" s="T868">ünen</ts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3166" n="HIAT:w" s="T869">ümmütünen</ts>
                  <nts id="Seg_3167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3169" n="HIAT:w" s="T870">tijdim</ts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3172" n="HIAT:w" s="T871">ontubar</ts>
                  <nts id="Seg_3173" n="HIAT:ip">.</nts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T878" id="Seg_3176" n="HIAT:u" s="T872">
                  <ts e="T873" id="Seg_3178" n="HIAT:w" s="T872">Kilep</ts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3181" n="HIAT:w" s="T873">kördöːn</ts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3184" n="HIAT:w" s="T874">eː</ts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3186" n="HIAT:ip">(</nts>
                  <ts e="T875.tx-ChVD.1" id="Seg_3188" n="HIAT:w" s="T875">ki</ts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3191" n="HIAT:w" s="T875.tx-ChVD.1">-</ts>
                  <nts id="Seg_3192" n="HIAT:ip">)</nts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3195" n="HIAT:w" s="T877">as</ts>
                  <nts id="Seg_3196" n="HIAT:ip">.</nts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T885" id="Seg_3199" n="HIAT:u" s="T878">
                  <nts id="Seg_3200" n="HIAT:ip">"</nts>
                  <ts e="T879" id="Seg_3202" n="HIAT:w" s="T878">Kaja</ts>
                  <nts id="Seg_3203" n="HIAT:ip">"</nts>
                  <nts id="Seg_3204" n="HIAT:ip">,</nts>
                  <nts id="Seg_3205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3207" n="HIAT:w" s="T879">diːr</ts>
                  <nts id="Seg_3208" n="HIAT:ip">,</nts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3210" n="HIAT:ip">"</nts>
                  <ts e="T881" id="Seg_3212" n="HIAT:w" s="T880">raniː</ts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3214" n="HIAT:ip">(</nts>
                  <ts e="T882" id="Seg_3216" n="HIAT:w" s="T881">gɨn-</ts>
                  <nts id="Seg_3217" n="HIAT:ip">)</nts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3219" n="HIAT:ip">(</nts>
                  <nts id="Seg_3220" n="HIAT:ip">(</nts>
                  <ats e="T930" id="Seg_3221" n="HIAT:non-pho" s="T882">…</ats>
                  <nts id="Seg_3222" n="HIAT:ip">)</nts>
                  <nts id="Seg_3223" n="HIAT:ip">)</nts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3226" n="HIAT:w" s="T930">duː</ts>
                  <nts id="Seg_3227" n="HIAT:ip">"</nts>
                  <nts id="Seg_3228" n="HIAT:ip">,</nts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3231" n="HIAT:w" s="T883">diːr</ts>
                  <nts id="Seg_3232" n="HIAT:ip">.</nts>
                  <nts id="Seg_3233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T896" id="Seg_3235" n="HIAT:u" s="T885">
                  <nts id="Seg_3236" n="HIAT:ip">"</nts>
                  <ts e="T886" id="Seg_3238" n="HIAT:w" s="T885">Hmm</ts>
                  <nts id="Seg_3239" n="HIAT:ip">"</nts>
                  <nts id="Seg_3240" n="HIAT:ip">,</nts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3243" n="HIAT:w" s="T886">diːbin</ts>
                  <nts id="Seg_3244" n="HIAT:ip">,</nts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3247" n="HIAT:w" s="T887">haːbɨn</ts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3250" n="HIAT:w" s="T888">onton</ts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3253" n="HIAT:w" s="T889">ɨlbɨt</ts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3256" n="HIAT:w" s="T890">ete</ts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3259" n="HIAT:w" s="T891">ol</ts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3262" n="HIAT:w" s="T892">kimim</ts>
                  <nts id="Seg_3263" n="HIAT:ip">,</nts>
                  <nts id="Seg_3264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3266" n="HIAT:w" s="T893">kamandʼirbar</ts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3269" n="HIAT:w" s="T894">sdavajdɨːbɨn</ts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3272" n="HIAT:w" s="T895">bu͡o</ts>
                  <nts id="Seg_3273" n="HIAT:ip">.</nts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T900" id="Seg_3276" n="HIAT:u" s="T896">
                  <ts e="T897" id="Seg_3278" n="HIAT:w" s="T896">Kimŋe</ts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3281" n="HIAT:w" s="T897">nomʼerɨnan</ts>
                  <nts id="Seg_3282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3284" n="HIAT:w" s="T898">bu͡olaːččɨ</ts>
                  <nts id="Seg_3285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3287" n="HIAT:w" s="T899">etilere</ts>
                  <nts id="Seg_3288" n="HIAT:ip">.</nts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T911" id="Seg_3291" n="HIAT:u" s="T900">
                  <ts e="T901" id="Seg_3293" n="HIAT:w" s="T900">Ontum</ts>
                  <nts id="Seg_3294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3296" n="HIAT:w" s="T901">bu͡o</ts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3299" n="HIAT:w" s="T902">kim</ts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3302" n="HIAT:w" s="T903">patru͡ottar</ts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3305" n="HIAT:w" s="T904">karmaːmmar</ts>
                  <nts id="Seg_3306" n="HIAT:ip">,</nts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3309" n="HIAT:w" s="T905">tu͡ok</ts>
                  <nts id="Seg_3310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3312" n="HIAT:w" s="T906">daː</ts>
                  <nts id="Seg_3313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3315" n="HIAT:w" s="T907">hu͡ok</ts>
                  <nts id="Seg_3316" n="HIAT:ip">,</nts>
                  <nts id="Seg_3317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3319" n="HIAT:w" s="T908">granaːttarbɨn</ts>
                  <nts id="Seg_3320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3322" n="HIAT:w" s="T909">emi͡e</ts>
                  <nts id="Seg_3323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3325" n="HIAT:w" s="T910">ɨlbɨttar</ts>
                  <nts id="Seg_3326" n="HIAT:ip">.</nts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T924" id="Seg_3329" n="HIAT:u" s="T911">
                  <ts e="T912" id="Seg_3331" n="HIAT:w" s="T911">Oku͡opaga</ts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_3334" n="HIAT:w" s="T912">hɨttakpɨna</ts>
                  <nts id="Seg_3335" n="HIAT:ip">,</nts>
                  <nts id="Seg_3336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3338" n="HIAT:w" s="T913">kumak</ts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3341" n="HIAT:w" s="T914">tüheːktiːr</ts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3344" n="HIAT:w" s="T915">diːn</ts>
                  <nts id="Seg_3345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3347" n="HIAT:w" s="T916">ol</ts>
                  <nts id="Seg_3348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3350" n="HIAT:w" s="T917">di͡egitten</ts>
                  <nts id="Seg_3351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3353" n="HIAT:w" s="T918">kele</ts>
                  <nts id="Seg_3354" n="HIAT:ip">,</nts>
                  <nts id="Seg_3355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3357" n="HIAT:w" s="T919">ite</ts>
                  <nts id="Seg_3358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3360" n="HIAT:w" s="T920">miːnalar</ts>
                  <nts id="Seg_3361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3363" n="HIAT:w" s="T921">tüstekterine</ts>
                  <nts id="Seg_3364" n="HIAT:ip">,</nts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3367" n="HIAT:w" s="T922">hirim</ts>
                  <nts id="Seg_3368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3370" n="HIAT:w" s="T923">dorgujar</ts>
                  <nts id="Seg_3371" n="HIAT:ip">.</nts>
                  <nts id="Seg_3372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T931" id="Seg_3374" n="HIAT:u" s="T924">
                  <ts e="T928" id="Seg_3376" n="HIAT:w" s="T924">Šɨrk-šɨrk-šɨrk</ts>
                  <nts id="Seg_3377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3379" n="HIAT:w" s="T928">kimniːller</ts>
                  <nts id="Seg_3380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3382" n="HIAT:w" s="T929">araj</ts>
                  <nts id="Seg_3383" n="HIAT:ip">.</nts>
                  <nts id="Seg_3384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T938" id="Seg_3386" n="HIAT:u" s="T931">
                  <ts e="T932" id="Seg_3388" n="HIAT:w" s="T931">Ontum</ts>
                  <nts id="Seg_3389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3391" n="HIAT:w" s="T932">bu</ts>
                  <nts id="Seg_3392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3394" n="HIAT:w" s="T933">karda</ts>
                  <nts id="Seg_3395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3396" n="HIAT:ip">(</nts>
                  <nts id="Seg_3397" n="HIAT:ip">(</nts>
                  <ats e="T937" id="Seg_3398" n="HIAT:non-pho" s="T934">…</ats>
                  <nts id="Seg_3399" n="HIAT:ip">)</nts>
                  <nts id="Seg_3400" n="HIAT:ip">)</nts>
                  <nts id="Seg_3401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3403" n="HIAT:w" s="T937">zapаlnʼenʼije</ts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3406" n="HIAT:w" s="T935">keler</ts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3409" n="HIAT:w" s="T936">ebit</ts>
                  <nts id="Seg_3410" n="HIAT:ip">.</nts>
                  <nts id="Seg_3411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T944" id="Seg_3413" n="HIAT:u" s="T938">
                  <ts e="T939" id="Seg_3415" n="HIAT:w" s="T938">Bihigi</ts>
                  <nts id="Seg_3416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3418" n="HIAT:w" s="T939">onnubutugar</ts>
                  <nts id="Seg_3419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3421" n="HIAT:w" s="T940">össü͡ö</ts>
                  <nts id="Seg_3422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3424" n="HIAT:w" s="T941">nöŋü͡öler</ts>
                  <nts id="Seg_3425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3427" n="HIAT:w" s="T942">keleller</ts>
                  <nts id="Seg_3428" n="HIAT:ip">,</nts>
                  <nts id="Seg_3429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3431" n="HIAT:w" s="T943">bu͡olar</ts>
                  <nts id="Seg_3432" n="HIAT:ip">.</nts>
                  <nts id="Seg_3433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T951" id="Seg_3435" n="HIAT:u" s="T944">
                  <ts e="T945" id="Seg_3437" n="HIAT:w" s="T944">Aːjtaːn</ts>
                  <nts id="Seg_3438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3440" n="HIAT:w" s="T945">bu͡olaːktɨːr</ts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3443" n="HIAT:w" s="T946">diːn</ts>
                  <nts id="Seg_3444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3446" n="HIAT:w" s="T947">onton</ts>
                  <nts id="Seg_3447" n="HIAT:ip">,</nts>
                  <nts id="Seg_3448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3450" n="HIAT:w" s="T948">ɨtahallar</ts>
                  <nts id="Seg_3451" n="HIAT:ip">,</nts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3454" n="HIAT:w" s="T949">bu͡olallar</ts>
                  <nts id="Seg_3455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3457" n="HIAT:w" s="T950">araj</ts>
                  <nts id="Seg_3458" n="HIAT:ip">.</nts>
                  <nts id="Seg_3459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T954" id="Seg_3461" n="HIAT:u" s="T951">
                  <ts e="T952" id="Seg_3463" n="HIAT:w" s="T951">Horok-horok</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3466" n="HIAT:w" s="T952">ɨnčɨktɨːr</ts>
                  <nts id="Seg_3467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_3469" n="HIAT:w" s="T953">bu͡olar</ts>
                  <nts id="Seg_3470" n="HIAT:ip">.</nts>
                  <nts id="Seg_3471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T966" id="Seg_3473" n="HIAT:u" s="T954">
                  <ts e="T955" id="Seg_3475" n="HIAT:w" s="T954">ɨnčɨktɨːr</ts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3478" n="HIAT:w" s="T955">bu͡ollaktarɨna</ts>
                  <nts id="Seg_3479" n="HIAT:ip">,</nts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3482" n="HIAT:w" s="T956">min</ts>
                  <nts id="Seg_3483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3485" n="HIAT:w" s="T957">emi͡e</ts>
                  <nts id="Seg_3486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3488" n="HIAT:w" s="T958">emi͡e</ts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3491" n="HIAT:w" s="T959">ɨnčɨktɨːbɨn</ts>
                  <nts id="Seg_3492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3494" n="HIAT:w" s="T960">emi͡e</ts>
                  <nts id="Seg_3495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3497" n="HIAT:w" s="T961">onton</ts>
                  <nts id="Seg_3498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3500" n="HIAT:w" s="T962">emi͡e</ts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3503" n="HIAT:w" s="T963">dʼɨlɨs</ts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3506" n="HIAT:w" s="T964">gɨnabɨn</ts>
                  <nts id="Seg_3507" n="HIAT:ip">,</nts>
                  <nts id="Seg_3508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3510" n="HIAT:w" s="T965">ihilliːbin</ts>
                  <nts id="Seg_3511" n="HIAT:ip">.</nts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T968" id="Seg_3514" n="HIAT:u" s="T966">
                  <ts e="T967" id="Seg_3516" n="HIAT:w" s="T966">Sančʼaska</ts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_3519" n="HIAT:w" s="T967">kelbitim</ts>
                  <nts id="Seg_3520" n="HIAT:ip">.</nts>
                  <nts id="Seg_3521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T975" id="Seg_3523" n="HIAT:u" s="T968">
                  <nts id="Seg_3524" n="HIAT:ip">(</nts>
                  <ts e="T970" id="Seg_3526" n="HIAT:w" s="T968">San-</ts>
                  <nts id="Seg_3527" n="HIAT:ip">)</nts>
                  <nts id="Seg_3528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3530" n="HIAT:w" s="T970">pаlʼevoj</ts>
                  <nts id="Seg_3531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3533" n="HIAT:w" s="T971">üle</ts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_3536" n="HIAT:w" s="T972">kim</ts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_3539" n="HIAT:w" s="T973">bu͡olaːččɨ</ts>
                  <nts id="Seg_3540" n="HIAT:ip">,</nts>
                  <nts id="Seg_3541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3543" n="HIAT:w" s="T974">sančʼas</ts>
                  <nts id="Seg_3544" n="HIAT:ip">.</nts>
                  <nts id="Seg_3545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T983" id="Seg_3547" n="HIAT:u" s="T975">
                  <ts e="T976" id="Seg_3549" n="HIAT:w" s="T975">Oːl</ts>
                  <nts id="Seg_3550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3552" n="HIAT:w" s="T976">sančʼaska</ts>
                  <nts id="Seg_3553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3554" n="HIAT:ip">(</nts>
                  <ts e="T978" id="Seg_3556" n="HIAT:w" s="T977">kelli-</ts>
                  <nts id="Seg_3557" n="HIAT:ip">)</nts>
                  <nts id="Seg_3558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3560" n="HIAT:w" s="T978">eː</ts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_3563" n="HIAT:w" s="T979">kellim</ts>
                  <nts id="Seg_3564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_3566" n="HIAT:w" s="T980">da</ts>
                  <nts id="Seg_3567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3569" n="HIAT:w" s="T981">tɨlga</ts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3572" n="HIAT:w" s="T982">ɨːppɨttara</ts>
                  <nts id="Seg_3573" n="HIAT:ip">.</nts>
                  <nts id="Seg_3574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T987" id="Seg_3576" n="HIAT:u" s="T983">
                  <ts e="T983.tx-ChVD.1" id="Seg_3578" n="HIAT:w" s="T983">Ivanava</ts>
                  <nts id="Seg_3579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_3581" n="HIAT:w" s="T983.tx-ChVD.1">Suja</ts>
                  <nts id="Seg_3582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3584" n="HIAT:w" s="T984">hɨppɨtɨm</ts>
                  <nts id="Seg_3585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3586" n="HIAT:ip">–</nts>
                  <nts id="Seg_3587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3589" n="HIAT:w" s="T985">erge</ts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3592" n="HIAT:w" s="T986">hospitalʼga</ts>
                  <nts id="Seg_3593" n="HIAT:ip">.</nts>
                  <nts id="Seg_3594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T999" id="Seg_3596" n="HIAT:u" s="T987">
                  <ts e="T988" id="Seg_3598" n="HIAT:w" s="T987">Onton</ts>
                  <nts id="Seg_3599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3600" n="HIAT:ip">(</nts>
                  <ts e="T988.tx-ChVD.1" id="Seg_3602" n="HIAT:w" s="T988">sra</ts>
                  <nts id="Seg_3603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3605" n="HIAT:w" s="T988.tx-ChVD.1">-</ts>
                  <nts id="Seg_3606" n="HIAT:ip">)</nts>
                  <nts id="Seg_3607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3609" n="HIAT:w" s="T990">eː</ts>
                  <nts id="Seg_3610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_3612" n="HIAT:w" s="T991">srazu</ts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T993" id="Seg_3615" n="HIAT:w" s="T992">bi͡es</ts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_3618" n="HIAT:w" s="T993">ɨjɨ</ts>
                  <nts id="Seg_3619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3621" n="HIAT:w" s="T994">hɨttɨm</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3624" n="HIAT:w" s="T995">da</ts>
                  <nts id="Seg_3625" n="HIAT:ip">,</nts>
                  <nts id="Seg_3626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_3628" n="HIAT:w" s="T996">dʼi͡em</ts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T998" id="Seg_3631" n="HIAT:w" s="T997">di͡ek</ts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_3634" n="HIAT:w" s="T998">ɨːppɨttara</ts>
                  <nts id="Seg_3635" n="HIAT:ip">.</nts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-ChVD">
            <ts e="T999" id="Seg_3637" n="sc" s="T0">
               <ts e="T1" id="Seg_3639" n="e" s="T0">Dudʼinkaga </ts>
               <ts e="T2" id="Seg_3641" n="e" s="T1">kelbitim </ts>
               <ts e="T3" id="Seg_3643" n="e" s="T2">v </ts>
               <ts e="T4" id="Seg_3645" n="e" s="T3">sorаk </ts>
               <ts e="T5" id="Seg_3647" n="e" s="T4">vtаrom. </ts>
               <ts e="T6" id="Seg_3649" n="e" s="T5">Avgust </ts>
               <ts e="T7" id="Seg_3651" n="e" s="T6">ɨjga </ts>
               <ts e="T10" id="Seg_3653" n="e" s="T7">vaabšʼe-ta </ts>
               <ts e="T11" id="Seg_3655" n="e" s="T10">kamʼisʼsʼijanɨ </ts>
               <ts e="T12" id="Seg_3657" n="e" s="T11">praxadiː </ts>
               <ts e="T13" id="Seg_3659" n="e" s="T12">gɨnnɨm – </ts>
               <ts e="T14" id="Seg_3661" n="e" s="T13">nʼestrajevojga. </ts>
               <ts e="T15" id="Seg_3663" n="e" s="T14">Narilʼskɨjga </ts>
               <ts e="T16" id="Seg_3665" n="e" s="T15">ɨːppɨttara. </ts>
               <ts e="T17" id="Seg_3667" n="e" s="T16">Narilʼskɨjtan </ts>
               <ts e="T18" id="Seg_3669" n="e" s="T17">kimŋe </ts>
               <ts e="T19" id="Seg_3671" n="e" s="T18">eː </ts>
               <ts e="T9" id="Seg_3673" n="e" s="T19">vajenʼizʼirovannɨj </ts>
               <ts e="T20" id="Seg_3675" n="e" s="T9">axranaga </ts>
               <ts e="T21" id="Seg_3677" n="e" s="T20">üleliː </ts>
               <ts e="T22" id="Seg_3679" n="e" s="T21">tüstüm, </ts>
               <ts e="T23" id="Seg_3681" n="e" s="T22">dabravolsɨlarɨ </ts>
               <ts e="T24" id="Seg_3683" n="e" s="T23">gɨtta </ts>
               <ts e="T25" id="Seg_3685" n="e" s="T24">emi͡e </ts>
               <ts e="T26" id="Seg_3687" n="e" s="T25">ka </ts>
               <ts e="T27" id="Seg_3689" n="e" s="T26">tübespitim. </ts>
               <ts e="T28" id="Seg_3691" n="e" s="T27">Krasnajarskajga </ts>
               <ts e="T29" id="Seg_3693" n="e" s="T28">eː </ts>
               <ts e="T30" id="Seg_3695" n="e" s="T29">di͡eri </ts>
               <ts e="T31" id="Seg_3697" n="e" s="T30">barbɨtɨm </ts>
               <ts e="T32" id="Seg_3699" n="e" s="T31">"Spartak" </ts>
               <ts e="T33" id="Seg_3701" n="e" s="T32">paraxotɨnan. </ts>
               <ts e="T34" id="Seg_3703" n="e" s="T33">Onton </ts>
               <ts e="T35" id="Seg_3705" n="e" s="T34">ü͡örennim </ts>
               <ts e="T36" id="Seg_3707" n="e" s="T35">Krasnajarskaj </ts>
               <ts e="T37" id="Seg_3709" n="e" s="T36">pʼexoːtaga </ts>
               <ts e="T38" id="Seg_3711" n="e" s="T37">s </ts>
               <ts e="T39" id="Seg_3713" n="e" s="T38">tankаvɨmi </ts>
               <ts e="T40" id="Seg_3715" n="e" s="T39">pulʼemʼočʼčʼikamʼi. </ts>
               <ts e="T41" id="Seg_3717" n="e" s="T40">U͡on </ts>
               <ts e="T42" id="Seg_3719" n="e" s="T41">biːr </ts>
               <ts e="T43" id="Seg_3721" n="e" s="T42">ɨjɨ </ts>
               <ts e="T44" id="Seg_3723" n="e" s="T43">duː, </ts>
               <ts e="T45" id="Seg_3725" n="e" s="T44">kahɨ </ts>
               <ts e="T46" id="Seg_3727" n="e" s="T45">duː </ts>
               <ts e="T47" id="Seg_3729" n="e" s="T46">ü͡örenen </ts>
               <ts e="T48" id="Seg_3731" n="e" s="T47">baraːn, </ts>
               <ts e="T49" id="Seg_3733" n="e" s="T48">emi͡e </ts>
               <ts e="T50" id="Seg_3735" n="e" s="T49">kattɨː </ts>
               <ts e="T54" id="Seg_3737" n="e" s="T50">((…)) </ts>
               <ts e="T51" id="Seg_3739" n="e" s="T54">kimi͡eke, </ts>
               <ts e="T52" id="Seg_3741" n="e" s="T51">Zvʼenʼigu͡orakka – </ts>
               <ts e="T53" id="Seg_3743" n="e" s="T52">mladšij </ts>
               <ts e="T55" id="Seg_3745" n="e" s="T53">sʼeržant. </ts>
               <ts e="T56" id="Seg_3747" n="e" s="T55">Parasut </ts>
               <ts e="T57" id="Seg_3749" n="e" s="T56">razbirajdaːččɨ </ts>
               <ts e="T58" id="Seg_3751" n="e" s="T57">etim, </ts>
               <ts e="T59" id="Seg_3753" n="e" s="T58">desanskʼij. </ts>
               <ts e="T60" id="Seg_3755" n="e" s="T59">Dʼe, </ts>
               <ts e="T61" id="Seg_3757" n="e" s="T60">onno </ts>
               <ts e="T62" id="Seg_3759" n="e" s="T61">uhun </ts>
               <ts e="T63" id="Seg_3761" n="e" s="T62">da </ts>
               <ts e="T64" id="Seg_3763" n="e" s="T63">ü͡öremmetegim </ts>
               <ts e="T65" id="Seg_3765" n="e" s="T64">dogottorum </ts>
               <ts e="T66" id="Seg_3767" n="e" s="T65">gɨtta </ts>
               <ts e="T67" id="Seg_3769" n="e" s="T66">ba </ts>
               <ts e="T68" id="Seg_3771" n="e" s="T67">kaːlɨmaːrɨbɨn </ts>
               <ts e="T69" id="Seg_3773" n="e" s="T68">biːrge </ts>
               <ts e="T70" id="Seg_3775" n="e" s="T69">barsɨbɨtɨm. </ts>
               <ts e="T71" id="Seg_3777" n="e" s="T70">Dʼe, </ts>
               <ts e="T72" id="Seg_3779" n="e" s="T71">ol </ts>
               <ts e="T73" id="Seg_3781" n="e" s="T72">kimneːtim, </ts>
               <ts e="T74" id="Seg_3783" n="e" s="T73">kimi͡eke, </ts>
               <ts e="T75" id="Seg_3785" n="e" s="T74">Muːromŋa </ts>
               <ts e="T76" id="Seg_3787" n="e" s="T75">ü͡örene </ts>
               <ts e="T77" id="Seg_3789" n="e" s="T76">kimneːtibit </ts>
               <ts e="T78" id="Seg_3791" n="e" s="T77">zapasnoj </ts>
               <ts e="T80" id="Seg_3793" n="e" s="T78">(ku͡o -). </ts>
               <ts e="T481" id="Seg_3795" n="e" s="T80">Prostа </ts>
               <ts e="T81" id="Seg_3797" n="e" s="T481">kimniːr </ts>
               <ts e="T82" id="Seg_3799" n="e" s="T81">etilere </ts>
               <ts e="T83" id="Seg_3801" n="e" s="T82">onno </ts>
               <ts e="T84" id="Seg_3803" n="e" s="T83">farmʼiravanʼije </ts>
               <ts e="T85" id="Seg_3805" n="e" s="T84">oŋoror </ts>
               <ts e="T86" id="Seg_3807" n="e" s="T85">etilere </ts>
               <ts e="T87" id="Seg_3809" n="e" s="T86">kakʼije </ts>
               <ts e="T90" id="Seg_3811" n="e" s="T87">(čʼa -). </ts>
               <ts e="T91" id="Seg_3813" n="e" s="T90">Kʼem, </ts>
               <ts e="T92" id="Seg_3815" n="e" s="T91">tu͡okka </ts>
               <ts e="T93" id="Seg_3817" n="e" s="T92">ü͡öremmikkin </ts>
               <ts e="T94" id="Seg_3819" n="e" s="T93">kimneːk </ts>
               <ts e="T95" id="Seg_3821" n="e" s="T94">kim </ts>
               <ts e="T96" id="Seg_3823" n="e" s="T95">kimniːller </ts>
               <ts e="T97" id="Seg_3825" n="e" s="T96">otto, </ts>
               <ts e="T98" id="Seg_3827" n="e" s="T97">kakʼije </ts>
               <ts e="T99" id="Seg_3829" n="e" s="T98">čʼastʼi, </ts>
               <ts e="T100" id="Seg_3831" n="e" s="T99">pаdrazdʼelʼenʼija. </ts>
               <ts e="T101" id="Seg_3833" n="e" s="T100">Dʼe, </ts>
               <ts e="T102" id="Seg_3835" n="e" s="T101">onno </ts>
               <ts e="T103" id="Seg_3837" n="e" s="T102">kimneːtim </ts>
               <ts e="T104" id="Seg_3839" n="e" s="T103">da, </ts>
               <ts e="T105" id="Seg_3841" n="e" s="T104">Muːromtan </ts>
               <ts e="T106" id="Seg_3843" n="e" s="T105">kimi͡eke </ts>
               <ts e="T107" id="Seg_3845" n="e" s="T106">tübestibit, </ts>
               <ts e="T108" id="Seg_3847" n="e" s="T107">ol </ts>
               <ts e="T109" id="Seg_3849" n="e" s="T108">Tuːlaga. </ts>
               <ts e="T110" id="Seg_3851" n="e" s="T109">Ol </ts>
               <ts e="T111" id="Seg_3853" n="e" s="T110">Tula </ts>
               <ts e="T113" id="Seg_3855" n="e" s="T111">(taː -) </ts>
               <ts e="T114" id="Seg_3857" n="e" s="T113">razbʼilsʼa </ts>
               <ts e="T115" id="Seg_3859" n="e" s="T114">bu͡o. </ts>
               <ts e="T116" id="Seg_3861" n="e" s="T115">Agɨs </ts>
               <ts e="T117" id="Seg_3863" n="e" s="T116">kilametrɨ </ts>
               <ts e="T118" id="Seg_3865" n="e" s="T117">duː, </ts>
               <ts e="T119" id="Seg_3867" n="e" s="T118">kahɨ </ts>
               <ts e="T120" id="Seg_3869" n="e" s="T119">duː </ts>
               <ts e="T121" id="Seg_3871" n="e" s="T120">baran </ts>
               <ts e="T122" id="Seg_3873" n="e" s="T121">baraːn, </ts>
               <ts e="T123" id="Seg_3875" n="e" s="T122">timir </ts>
               <ts e="T124" id="Seg_3877" n="e" s="T123">oroktoruŋ </ts>
               <ts e="T125" id="Seg_3879" n="e" s="T124">barɨta </ts>
               <ts e="T126" id="Seg_3881" n="e" s="T125">aldʼammɨttara </ts>
               <ts e="T127" id="Seg_3883" n="e" s="T126">bu͡olla. </ts>
               <ts e="T128" id="Seg_3885" n="e" s="T127">Dʼe, </ts>
               <ts e="T129" id="Seg_3887" n="e" s="T128">onno </ts>
               <ts e="T130" id="Seg_3889" n="e" s="T129">kimniː </ts>
               <ts e="T131" id="Seg_3891" n="e" s="T130">hɨttakpɨtɨna, </ts>
               <ts e="T132" id="Seg_3893" n="e" s="T131">uže </ts>
               <ts e="T133" id="Seg_3895" n="e" s="T132">nalʼotɨ </ts>
               <ts e="T134" id="Seg_3897" n="e" s="T133">bu͡o </ts>
               <ts e="T135" id="Seg_3899" n="e" s="T134">bi͡ek, </ts>
               <ts e="T136" id="Seg_3901" n="e" s="T135">(kötö </ts>
               <ts e="T138" id="Seg_3903" n="e" s="T136">hɨ -) </ts>
               <ts e="T139" id="Seg_3905" n="e" s="T138">kötöːččü </ts>
               <ts e="T140" id="Seg_3907" n="e" s="T139">etilere </ts>
               <ts e="T141" id="Seg_3909" n="e" s="T140">harsi͡erda </ts>
               <ts e="T142" id="Seg_3911" n="e" s="T141">aːjɨ. </ts>
               <ts e="T143" id="Seg_3913" n="e" s="T142">Dʼe, </ts>
               <ts e="T144" id="Seg_3915" n="e" s="T143">onton </ts>
               <ts e="T145" id="Seg_3917" n="e" s="T144">uhun </ts>
               <ts e="T146" id="Seg_3919" n="e" s="T145">da </ts>
               <ts e="T147" id="Seg_3921" n="e" s="T146">bu͡olla, </ts>
               <ts e="T148" id="Seg_3923" n="e" s="T147">eː, </ts>
               <ts e="T150" id="Seg_3925" n="e" s="T148">(barɨm-) </ts>
               <ts e="T151" id="Seg_3927" n="e" s="T150">kimniːbit </ts>
               <ts e="T152" id="Seg_3929" n="e" s="T151">onno. </ts>
               <ts e="T153" id="Seg_3931" n="e" s="T152">Ile </ts>
               <ts e="T154" id="Seg_3933" n="e" s="T153">hatɨː </ts>
               <ts e="T155" id="Seg_3935" n="e" s="T154">barbɨppɨt </ts>
               <ts e="T156" id="Seg_3937" n="e" s="T155">ol </ts>
               <ts e="T157" id="Seg_3939" n="e" s="T156">kimi͡eke, </ts>
               <ts e="T158" id="Seg_3941" n="e" s="T157">kanna </ts>
               <ts e="T160" id="Seg_3943" n="e" s="T158">(ho-) </ts>
               <ts e="T161" id="Seg_3945" n="e" s="T160">hol </ts>
               <ts e="T162" id="Seg_3947" n="e" s="T161">vajna </ts>
               <ts e="T163" id="Seg_3949" n="e" s="T162">bu͡olu͡ok </ts>
               <ts e="T164" id="Seg_3951" n="e" s="T163">hire </ts>
               <ts e="T165" id="Seg_3953" n="e" s="T164">bu͡olu͡or </ts>
               <ts e="T166" id="Seg_3955" n="e" s="T165">di͡eri. </ts>
               <ts e="T167" id="Seg_3957" n="e" s="T166">Dʼe </ts>
               <ts e="T168" id="Seg_3959" n="e" s="T167">onno </ts>
               <ts e="T169" id="Seg_3961" n="e" s="T168">kimneːččiler </ts>
               <ts e="T170" id="Seg_3963" n="e" s="T169">bu͡o </ts>
               <ts e="T171" id="Seg_3965" n="e" s="T170">kim – </ts>
               <ts e="T172" id="Seg_3967" n="e" s="T171">partʼizanskɨj </ts>
               <ts e="T173" id="Seg_3969" n="e" s="T172">iti </ts>
               <ts e="T174" id="Seg_3971" n="e" s="T173">аtrʼad. </ts>
               <ts e="T175" id="Seg_3973" n="e" s="T174">O </ts>
               <ts e="T176" id="Seg_3975" n="e" s="T175">kimŋe – </ts>
               <ts e="T177" id="Seg_3977" n="e" s="T176">NOP </ts>
               <ts e="T178" id="Seg_3979" n="e" s="T177">SOP </ts>
               <ts e="T179" id="Seg_3981" n="e" s="T178">abaznačajdɨːllar. </ts>
               <ts e="T180" id="Seg_3983" n="e" s="T179">Onu </ts>
               <ts e="T181" id="Seg_3985" n="e" s="T180">da </ts>
               <ts e="T182" id="Seg_3987" n="e" s="T181">bu </ts>
               <ts e="T183" id="Seg_3989" n="e" s="T182">muja </ts>
               <ts e="T184" id="Seg_3991" n="e" s="T183">(kimniː-) </ts>
               <ts e="T185" id="Seg_3993" n="e" s="T184">kimniːrge </ts>
               <ts e="T186" id="Seg_3995" n="e" s="T185">ürek, </ts>
               <ts e="T187" id="Seg_3997" n="e" s="T186">tu͡ok </ts>
               <ts e="T188" id="Seg_3999" n="e" s="T187">ere </ts>
               <ts e="T189" id="Seg_4001" n="e" s="T188">ürekkeːn </ts>
               <ts e="T190" id="Seg_4003" n="e" s="T189">hɨtar. </ts>
               <ts e="T191" id="Seg_4005" n="e" s="T190">Če </ts>
               <ts e="T192" id="Seg_4007" n="e" s="T191">onno </ts>
               <ts e="T193" id="Seg_4009" n="e" s="T192">dʼerʼevnʼaga. </ts>
               <ts e="T194" id="Seg_4011" n="e" s="T193">Onno </ts>
               <ts e="T195" id="Seg_4013" n="e" s="T194">аbаranʼassa </ts>
               <ts e="T196" id="Seg_4015" n="e" s="T195">gɨnallar </ts>
               <ts e="T197" id="Seg_4017" n="e" s="T196">eni. </ts>
               <ts e="T198" id="Seg_4019" n="e" s="T197">Harsi͡erda </ts>
               <ts e="T199" id="Seg_4021" n="e" s="T198">kimneːtibit, </ts>
               <ts e="T200" id="Seg_4023" n="e" s="T199">ahɨː </ts>
               <ts e="T201" id="Seg_4025" n="e" s="T200">ahɨː </ts>
               <ts e="T202" id="Seg_4027" n="e" s="T201">tühen </ts>
               <ts e="T203" id="Seg_4029" n="e" s="T202">baraːmmɨt </ts>
               <ts e="T204" id="Seg_4031" n="e" s="T203">onu. </ts>
               <ts e="T205" id="Seg_4033" n="e" s="T204">"Dʼe </ts>
               <ts e="T206" id="Seg_4035" n="e" s="T205">ɨtɨ͡alahallar", </ts>
               <ts e="T207" id="Seg_4037" n="e" s="T206">kamandʼirbɨtɨn </ts>
               <ts e="T208" id="Seg_4039" n="e" s="T207">kepsiːr. </ts>
               <ts e="T209" id="Seg_4041" n="e" s="T208">"Kajtak </ts>
               <ts e="T211" id="Seg_4043" n="e" s="T209">(аboː -) </ts>
               <ts e="T212" id="Seg_4045" n="e" s="T211">kimni͡ekkitin </ts>
               <ts e="T214" id="Seg_4047" n="e" s="T212">onton… </ts>
               <ts e="T215" id="Seg_4049" n="e" s="T214">Dʼe </ts>
               <ts e="T216" id="Seg_4051" n="e" s="T215">аkruzeːnije </ts>
               <ts e="T217" id="Seg_4053" n="e" s="T216">tübestekkitine </ts>
               <ts e="T218" id="Seg_4055" n="e" s="T217">bagar, </ts>
               <ts e="T219" id="Seg_4057" n="e" s="T218">plʼen </ts>
               <ts e="T220" id="Seg_4059" n="e" s="T219">tübestekketin </ts>
               <ts e="T221" id="Seg_4061" n="e" s="T220">itte </ts>
               <ts e="T222" id="Seg_4063" n="e" s="T221">dаkumʼentɨ </ts>
               <ts e="T223" id="Seg_4065" n="e" s="T222">kim </ts>
               <ts e="T224" id="Seg_4067" n="e" s="T223">kimneːmi͡ekkin, </ts>
               <ts e="T225" id="Seg_4069" n="e" s="T224">naːda </ts>
               <ts e="T226" id="Seg_4071" n="e" s="T225">bu͡olla, </ts>
               <ts e="T227" id="Seg_4073" n="e" s="T226">bejegin </ts>
               <ts e="T228" id="Seg_4075" n="e" s="T227">bi͡erimi͡ekkin </ts>
               <ts e="T229" id="Seg_4077" n="e" s="T228">bagar. </ts>
               <ts e="T230" id="Seg_4079" n="e" s="T229">Prostа, </ts>
               <ts e="T232" id="Seg_4081" n="e" s="T230">(gi-) </ts>
               <ts e="T233" id="Seg_4083" n="e" s="T232">kajɨtalaːmaŋ </ts>
               <ts e="T234" id="Seg_4085" n="e" s="T233">(huruk-), </ts>
               <ts e="T235" id="Seg_4087" n="e" s="T234">eː, </ts>
               <ts e="T236" id="Seg_4089" n="e" s="T235">huruktargɨt", </ts>
               <ts e="T237" id="Seg_4091" n="e" s="T236">diːr </ts>
               <ts e="T238" id="Seg_4093" n="e" s="T237">bu͡olla, </ts>
               <ts e="T240" id="Seg_4095" n="e" s="T238">"(arak-) </ts>
               <ts e="T241" id="Seg_4097" n="e" s="T240">kimneːŋ. </ts>
               <ts e="T243" id="Seg_4099" n="e" s="T241">(Kömü-), </ts>
               <ts e="T244" id="Seg_4101" n="e" s="T243">e, </ts>
               <ts e="T245" id="Seg_4103" n="e" s="T244">kömüŋ </ts>
               <ts e="T246" id="Seg_4105" n="e" s="T245">duː, </ts>
               <ts e="T247" id="Seg_4107" n="e" s="T246">bejegit </ts>
               <ts e="T248" id="Seg_4109" n="e" s="T247">hi͡eŋ </ts>
               <ts e="T249" id="Seg_4111" n="e" s="T248">duː", </ts>
               <ts e="T250" id="Seg_4113" n="e" s="T249">di͡ečči </ts>
               <ts e="T251" id="Seg_4115" n="e" s="T250">bugurduk. </ts>
               <ts e="T252" id="Seg_4117" n="e" s="T251">"Kajatalaːtɨŋ </ts>
               <ts e="T253" id="Seg_4119" n="e" s="T252">daː </ts>
               <ts e="T255" id="Seg_4121" n="e" s="T253">vsʼo ravno </ts>
               <ts e="T256" id="Seg_4123" n="e" s="T255">(kili͡ejd-) </ts>
               <ts e="T257" id="Seg_4125" n="e" s="T256">iti </ts>
               <ts e="T258" id="Seg_4127" n="e" s="T257">kimniːller </ts>
               <ts e="T259" id="Seg_4129" n="e" s="T258">bu͡o, </ts>
               <ts e="T260" id="Seg_4131" n="e" s="T259">ol </ts>
               <ts e="T261" id="Seg_4133" n="e" s="T260">bulallar </ts>
               <ts e="T262" id="Seg_4135" n="e" s="T261">bu͡o." </ts>
               <ts e="T263" id="Seg_4137" n="e" s="T262">Ol </ts>
               <ts e="T264" id="Seg_4139" n="e" s="T263">ke </ts>
               <ts e="T265" id="Seg_4141" n="e" s="T264">orotaŋ </ts>
               <ts e="T266" id="Seg_4143" n="e" s="T265">keːhe </ts>
               <ts e="T267" id="Seg_4145" n="e" s="T266">hüːrbüppün </ts>
               <ts e="T268" id="Seg_4147" n="e" s="T267">biːrde. </ts>
               <ts e="T270" id="Seg_4149" n="e" s="T268">(Kann-) </ts>
               <ts e="T271" id="Seg_4151" n="e" s="T270">kannuktarɨn </ts>
               <ts e="T272" id="Seg_4153" n="e" s="T271">da </ts>
               <ts e="T273" id="Seg_4155" n="e" s="T272">dʼüːlleːmmin </ts>
               <ts e="T274" id="Seg_4157" n="e" s="T273">karaŋaga, </ts>
               <ts e="T275" id="Seg_4159" n="e" s="T274">nʼemʼestere </ts>
               <ts e="T276" id="Seg_4161" n="e" s="T275">da, </ts>
               <ts e="T277" id="Seg_4163" n="e" s="T276">kimnere </ts>
               <ts e="T278" id="Seg_4165" n="e" s="T277">da? </ts>
               <ts e="T279" id="Seg_4167" n="e" s="T278">Dʼe </ts>
               <ts e="T280" id="Seg_4169" n="e" s="T279">ol </ts>
               <ts e="T281" id="Seg_4171" n="e" s="T280">min </ts>
               <ts e="T282" id="Seg_4173" n="e" s="T281">hɨldʼammɨn </ts>
               <ts e="T283" id="Seg_4175" n="e" s="T282">kördüm, </ts>
               <ts e="T284" id="Seg_4177" n="e" s="T283">bi͡es </ts>
               <ts e="T285" id="Seg_4179" n="e" s="T284">kihi </ts>
               <ts e="T286" id="Seg_4181" n="e" s="T285">hɨldʼallar </ts>
               <ts e="T287" id="Seg_4183" n="e" s="T286">araj </ts>
               <ts e="T288" id="Seg_4185" n="e" s="T287">bugurduk. </ts>
               <ts e="T289" id="Seg_4187" n="e" s="T288">Dʼe </ts>
               <ts e="T290" id="Seg_4189" n="e" s="T289">onton </ts>
               <ts e="T291" id="Seg_4191" n="e" s="T290">ol </ts>
               <ts e="T292" id="Seg_4193" n="e" s="T291">di͡egitten </ts>
               <ts e="T293" id="Seg_4195" n="e" s="T292">nʼemsɨlarbɨt </ts>
               <ts e="T294" id="Seg_4197" n="e" s="T293">keleller, </ts>
               <ts e="T295" id="Seg_4199" n="e" s="T294">transejtan </ts>
               <ts e="T296" id="Seg_4201" n="e" s="T295">turallar. </ts>
               <ts e="T297" id="Seg_4203" n="e" s="T296">"Töttörü </ts>
               <ts e="T298" id="Seg_4205" n="e" s="T297">kürü͡ögüŋ", </ts>
               <ts e="T299" id="Seg_4207" n="e" s="T298">diː-diː </ts>
               <ts e="T300" id="Seg_4209" n="e" s="T299">kimniːller. </ts>
               <ts e="T301" id="Seg_4211" n="e" s="T300">Ol </ts>
               <ts e="T302" id="Seg_4213" n="e" s="T301">kihiŋ, </ts>
               <ts e="T303" id="Seg_4215" n="e" s="T302">kamandʼirdardaːktar </ts>
               <ts e="T304" id="Seg_4217" n="e" s="T303">ol </ts>
               <ts e="T305" id="Seg_4219" n="e" s="T304">dogottoro. </ts>
               <ts e="T306" id="Seg_4221" n="e" s="T305">Atɨttar </ts>
               <ts e="T307" id="Seg_4223" n="e" s="T306">onto, </ts>
               <ts e="T308" id="Seg_4225" n="e" s="T307">atɨn </ts>
               <ts e="T309" id="Seg_4227" n="e" s="T308">čʼaːstan. </ts>
               <ts e="T310" id="Seg_4229" n="e" s="T309">Bejem </ts>
               <ts e="T312" id="Seg_4231" n="e" s="T310">(ho-) </ts>
               <ts e="T313" id="Seg_4233" n="e" s="T312">dogottorbun </ts>
               <ts e="T314" id="Seg_4235" n="e" s="T313">ol </ts>
               <ts e="T315" id="Seg_4237" n="e" s="T314">keːhe </ts>
               <ts e="T316" id="Seg_4239" n="e" s="T315">hüːrbüppün, </ts>
               <ts e="T317" id="Seg_4241" n="e" s="T316">eni </ts>
               <ts e="T318" id="Seg_4243" n="e" s="T317">ki͡e </ts>
               <ts e="T319" id="Seg_4245" n="e" s="T318">duː, </ts>
               <ts e="T320" id="Seg_4247" n="e" s="T319">tu͡ok </ts>
               <ts e="T321" id="Seg_4249" n="e" s="T320">duː. </ts>
               <ts e="T322" id="Seg_4251" n="e" s="T321">"Astupajdaːŋ", </ts>
               <ts e="T323" id="Seg_4253" n="e" s="T322">diːr. </ts>
               <ts e="T324" id="Seg_4255" n="e" s="T323">"Tolʼkа </ts>
               <ts e="T325" id="Seg_4257" n="e" s="T324">nʼe </ts>
               <ts e="T326" id="Seg_4259" n="e" s="T325">zamʼetnа", </ts>
               <ts e="T327" id="Seg_4261" n="e" s="T326">diːr </ts>
               <ts e="T328" id="Seg_4263" n="e" s="T327">kamandʼirdara. </ts>
               <ts e="T329" id="Seg_4265" n="e" s="T328">Onton </ts>
               <ts e="T330" id="Seg_4267" n="e" s="T329">kördüm </ts>
               <ts e="T331" id="Seg_4269" n="e" s="T330">do, </ts>
               <ts e="T332" id="Seg_4271" n="e" s="T331">nʼemsɨlarɨŋ </ts>
               <ts e="T333" id="Seg_4273" n="e" s="T332">hubu </ts>
               <ts e="T334" id="Seg_4275" n="e" s="T333">kelen </ts>
               <ts e="T335" id="Seg_4277" n="e" s="T334">iheller. </ts>
               <ts e="T336" id="Seg_4279" n="e" s="T335">Tuːgu, </ts>
               <ts e="T337" id="Seg_4281" n="e" s="T336">tuːgu </ts>
               <ts e="T338" id="Seg_4283" n="e" s="T337">kadʼɨgɨnahallar, </ts>
               <ts e="T339" id="Seg_4285" n="e" s="T338">sepseteller, </ts>
               <ts e="T340" id="Seg_4287" n="e" s="T339">töttörü </ts>
               <ts e="T341" id="Seg_4289" n="e" s="T340">hüːrebin </ts>
               <ts e="T342" id="Seg_4291" n="e" s="T341">dogottorum </ts>
               <ts e="T343" id="Seg_4293" n="e" s="T342">barbɨt </ts>
               <ts e="T344" id="Seg_4295" n="e" s="T343">hirderin </ts>
               <ts e="T345" id="Seg_4297" n="e" s="T344">di͡ek. </ts>
               <ts e="T346" id="Seg_4299" n="e" s="T345">Bu͡opsa </ts>
               <ts e="T347" id="Seg_4301" n="e" s="T346">hurdurgahallar </ts>
               <ts e="T348" id="Seg_4303" n="e" s="T347">agaj </ts>
               <ts e="T349" id="Seg_4305" n="e" s="T348">itinne, </ts>
               <ts e="T350" id="Seg_4307" n="e" s="T349">okko, </ts>
               <ts e="T351" id="Seg_4309" n="e" s="T350">ɨtɨ͡alɨːllar, </ts>
               <ts e="T352" id="Seg_4311" n="e" s="T351">ile </ts>
               <ts e="T353" id="Seg_4313" n="e" s="T352">kimneːri </ts>
               <ts e="T354" id="Seg_4315" n="e" s="T353">gɨnallar </ts>
               <ts e="T355" id="Seg_4317" n="e" s="T354">bɨhɨlaːk </ts>
               <ts e="T357" id="Seg_4319" n="e" s="T355">otto… </ts>
               <ts e="T358" id="Seg_4321" n="e" s="T357">Plʼen </ts>
               <ts e="T359" id="Seg_4323" n="e" s="T358">ɨlaːrɨ, </ts>
               <ts e="T360" id="Seg_4325" n="e" s="T359">uhuguttan </ts>
               <ts e="T361" id="Seg_4327" n="e" s="T360">transejga </ts>
               <ts e="T362" id="Seg_4329" n="e" s="T361">kellim. </ts>
               <ts e="T363" id="Seg_4331" n="e" s="T362">Kelbitim, </ts>
               <ts e="T364" id="Seg_4333" n="e" s="T363">bejem </ts>
               <ts e="T365" id="Seg_4335" n="e" s="T364">(dogot-), </ts>
               <ts e="T366" id="Seg_4337" n="e" s="T365">e, </ts>
               <ts e="T367" id="Seg_4339" n="e" s="T366">rotabar </ts>
               <ts e="T368" id="Seg_4341" n="e" s="T367">tübespippin. </ts>
               <ts e="T369" id="Seg_4343" n="e" s="T368">Dʼe </ts>
               <ts e="T370" id="Seg_4345" n="e" s="T369">ol </ts>
               <ts e="T371" id="Seg_4347" n="e" s="T370">kimniːbin, </ts>
               <ts e="T372" id="Seg_4349" n="e" s="T371">kaja, </ts>
               <ts e="T373" id="Seg_4351" n="e" s="T372">kamnʼem </ts>
               <ts e="T374" id="Seg_4353" n="e" s="T373">((…)) – </ts>
               <ts e="T375" id="Seg_4355" n="e" s="T374">pulemʼotɨnan </ts>
               <ts e="T376" id="Seg_4357" n="e" s="T375">kimni͡e </ts>
               <ts e="T377" id="Seg_4359" n="e" s="T376">sraːzu. </ts>
               <ts e="T378" id="Seg_4361" n="e" s="T377">Hüːrbetten </ts>
               <ts e="T379" id="Seg_4363" n="e" s="T378">taksa </ts>
               <ts e="T380" id="Seg_4365" n="e" s="T379">kihini </ts>
               <ts e="T381" id="Seg_4367" n="e" s="T380">tüherdiler. </ts>
               <ts e="T382" id="Seg_4369" n="e" s="T381">Horoktorbut </ts>
               <ts e="T383" id="Seg_4371" n="e" s="T382">töttörü </ts>
               <ts e="T384" id="Seg_4373" n="e" s="T383">hüːrdüler </ts>
               <ts e="T385" id="Seg_4375" n="e" s="T384">ol, </ts>
               <ts e="T386" id="Seg_4377" n="e" s="T385">dаzor </ts>
               <ts e="T387" id="Seg_4379" n="e" s="T386">bu͡olaːččɨ. </ts>
               <ts e="T389" id="Seg_4381" n="e" s="T387">(Oro-) </ts>
               <ts e="T390" id="Seg_4383" n="e" s="T389">orotaŋ </ts>
               <ts e="T391" id="Seg_4385" n="e" s="T390">barɨ͡aga, </ts>
               <ts e="T392" id="Seg_4387" n="e" s="T391">össü͡ö </ts>
               <ts e="T393" id="Seg_4389" n="e" s="T392">kim </ts>
               <ts e="T394" id="Seg_4391" n="e" s="T393">vpʼerʼedʼi </ts>
               <ts e="T395" id="Seg_4393" n="e" s="T394">hɨldʼɨ͡aga </ts>
               <ts e="T396" id="Seg_4395" n="e" s="T395">razvʼečʼčʼik </ts>
               <ts e="T397" id="Seg_4397" n="e" s="T396">kördük </ts>
               <ts e="T398" id="Seg_4399" n="e" s="T397">orotagɨn </ts>
               <ts e="T399" id="Seg_4401" n="e" s="T398">agaj </ts>
               <ts e="T400" id="Seg_4403" n="e" s="T399">ketiːgin </ts>
               <ts e="T401" id="Seg_4405" n="e" s="T400">bu͡o. </ts>
               <ts e="T402" id="Seg_4407" n="e" s="T401">Innikiler </ts>
               <ts e="T403" id="Seg_4409" n="e" s="T402">ikki </ts>
               <ts e="T404" id="Seg_4411" n="e" s="T403">kihi </ts>
               <ts e="T405" id="Seg_4413" n="e" s="T404">bu͡olar, </ts>
               <ts e="T406" id="Seg_4415" n="e" s="T405">kaŋas </ts>
               <ts e="T407" id="Seg_4417" n="e" s="T406">di͡ek, </ts>
               <ts e="T408" id="Seg_4419" n="e" s="T407">uŋa </ts>
               <ts e="T409" id="Seg_4421" n="e" s="T408">di͡ek </ts>
               <ts e="T410" id="Seg_4423" n="e" s="T409">emi͡e </ts>
               <ts e="T411" id="Seg_4425" n="e" s="T410">ikkiliː </ts>
               <ts e="T412" id="Seg_4427" n="e" s="T411">kihi. </ts>
               <ts e="T413" id="Seg_4429" n="e" s="T412">"Če, </ts>
               <ts e="T414" id="Seg_4431" n="e" s="T413">kajti͡ek </ts>
               <ts e="T415" id="Seg_4433" n="e" s="T414">bu͡olu͡okputuj", </ts>
               <ts e="T416" id="Seg_4435" n="e" s="T415">diːr. </ts>
               <ts e="T417" id="Seg_4437" n="e" s="T416">Bu </ts>
               <ts e="T418" id="Seg_4439" n="e" s="T417">dʼerʼevnʼaga </ts>
               <ts e="T419" id="Seg_4441" n="e" s="T418">pravʼerʼajdɨːbɨt, </ts>
               <ts e="T420" id="Seg_4443" n="e" s="T419">ile </ts>
               <ts e="T421" id="Seg_4445" n="e" s="T420">(kimneː-) </ts>
               <ts e="T422" id="Seg_4447" n="e" s="T421">kim </ts>
               <ts e="T423" id="Seg_4449" n="e" s="T422">da </ts>
               <ts e="T424" id="Seg_4451" n="e" s="T423">hu͡ok. </ts>
               <ts e="T425" id="Seg_4453" n="e" s="T424">Nʼemsɨ, </ts>
               <ts e="T426" id="Seg_4455" n="e" s="T425">eː, </ts>
               <ts e="T427" id="Seg_4457" n="e" s="T426">nʼemsalar </ts>
               <ts e="T428" id="Seg_4459" n="e" s="T427">oŋu͡or </ts>
               <ts e="T429" id="Seg_4461" n="e" s="T428">ere </ts>
               <ts e="T430" id="Seg_4463" n="e" s="T429">köstöllör. </ts>
               <ts e="T431" id="Seg_4465" n="e" s="T430">"Töttörü </ts>
               <ts e="T432" id="Seg_4467" n="e" s="T431">barɨ͡agɨŋ", </ts>
               <ts e="T433" id="Seg_4469" n="e" s="T432">diː-diː </ts>
               <ts e="T434" id="Seg_4471" n="e" s="T433">gɨnar. </ts>
               <ts e="T435" id="Seg_4473" n="e" s="T434">Če, </ts>
               <ts e="T436" id="Seg_4475" n="e" s="T435">ol </ts>
               <ts e="T437" id="Seg_4477" n="e" s="T436">kimniː </ts>
               <ts e="T438" id="Seg_4479" n="e" s="T437">hɨldʼammɨt </ts>
               <ts e="T439" id="Seg_4481" n="e" s="T438">u͡oldʼastɨbɨt, </ts>
               <ts e="T440" id="Seg_4483" n="e" s="T439">(ol) </ts>
               <ts e="T441" id="Seg_4485" n="e" s="T440">ol </ts>
               <ts e="T442" id="Seg_4487" n="e" s="T441">ürekpitin </ts>
               <ts e="T443" id="Seg_4489" n="e" s="T442">taŋnarɨ </ts>
               <ts e="T444" id="Seg_4491" n="e" s="T443">bardɨbɨt, </ts>
               <ts e="T445" id="Seg_4493" n="e" s="T444">hüːrük </ts>
               <ts e="T446" id="Seg_4495" n="e" s="T445">taŋnarɨ. </ts>
               <ts e="T447" id="Seg_4497" n="e" s="T446">Ürektenen </ts>
               <ts e="T448" id="Seg_4499" n="e" s="T447">e </ts>
               <ts e="T449" id="Seg_4501" n="e" s="T448">barammɨt, </ts>
               <ts e="T450" id="Seg_4503" n="e" s="T449">eː, </ts>
               <ts e="T451" id="Seg_4505" n="e" s="T450">kimniːbit. </ts>
               <ts e="T452" id="Seg_4507" n="e" s="T451">Bugurduk </ts>
               <ts e="T453" id="Seg_4509" n="e" s="T452">kimnere </ts>
               <ts e="T454" id="Seg_4511" n="e" s="T453">kimneːbitter </ts>
               <ts e="T455" id="Seg_4513" n="e" s="T454">bu </ts>
               <ts e="T456" id="Seg_4515" n="e" s="T455">artʼilʼlʼerʼija </ts>
               <ts e="T457" id="Seg_4517" n="e" s="T456">bu͡olaːččɨ </ts>
               <ts e="T458" id="Seg_4519" n="e" s="T457">bu͡o, </ts>
               <ts e="T459" id="Seg_4521" n="e" s="T458">puːskalara </ts>
               <ts e="T461" id="Seg_4523" n="e" s="T459">(taː -) </ts>
               <ts e="T462" id="Seg_4525" n="e" s="T461">e </ts>
               <ts e="T463" id="Seg_4527" n="e" s="T462">kimnere, </ts>
               <ts e="T464" id="Seg_4529" n="e" s="T463">attar – </ts>
               <ts e="T465" id="Seg_4531" n="e" s="T464">barɨta </ts>
               <ts e="T466" id="Seg_4533" n="e" s="T465">kimnellibitter </ts>
               <ts e="T467" id="Seg_4535" n="e" s="T466">bugurduk. </ts>
               <ts e="T468" id="Seg_4537" n="e" s="T467">Perevaročɨː </ts>
               <ts e="T469" id="Seg_4539" n="e" s="T468">kimneːbitter </ts>
               <ts e="T470" id="Seg_4541" n="e" s="T469">onto </ts>
               <ts e="T471" id="Seg_4543" n="e" s="T470">ölörtöːbütter. </ts>
               <ts e="T472" id="Seg_4545" n="e" s="T471">Onno </ts>
               <ts e="T473" id="Seg_4547" n="e" s="T472">biːr </ts>
               <ts e="T474" id="Seg_4549" n="e" s="T473">kihini </ts>
               <ts e="T475" id="Seg_4551" n="e" s="T474">bullubut. </ts>
               <ts e="T8" id="Seg_4553" n="e" s="T475">Pаluzivoj </ts>
               <ts e="T476" id="Seg_4555" n="e" s="T8">ile, </ts>
               <ts e="T477" id="Seg_4557" n="e" s="T476">kajdi͡ek </ts>
               <ts e="T478" id="Seg_4559" n="e" s="T477">ere, </ts>
               <ts e="T479" id="Seg_4561" n="e" s="T478">arɨččɨ </ts>
               <ts e="T480" id="Seg_4563" n="e" s="T479">haŋarar </ts>
               <ts e="T482" id="Seg_4565" n="e" s="T480">onton. </ts>
               <ts e="T483" id="Seg_4567" n="e" s="T482">"Bu </ts>
               <ts e="T484" id="Seg_4569" n="e" s="T483">haŋardɨː </ts>
               <ts e="T485" id="Seg_4571" n="e" s="T484">tabak </ts>
               <ts e="T486" id="Seg_4573" n="e" s="T485">tardar </ts>
               <ts e="T487" id="Seg_4575" n="e" s="T486">ɨksata </ts>
               <ts e="T488" id="Seg_4577" n="e" s="T487">bu͡olla", </ts>
               <ts e="T489" id="Seg_4579" n="e" s="T488">diː-diː </ts>
               <ts e="T490" id="Seg_4581" n="e" s="T489">gɨnar, </ts>
               <ts e="T491" id="Seg_4583" n="e" s="T490">"bihigi </ts>
               <ts e="T492" id="Seg_4585" n="e" s="T491">rotabɨtɨn </ts>
               <ts e="T493" id="Seg_4587" n="e" s="T492">kimneːbittere", </ts>
               <ts e="T494" id="Seg_4589" n="e" s="T493">diːr. </ts>
               <ts e="T495" id="Seg_4591" n="e" s="T494">"Nʼemsite, </ts>
               <ts e="T496" id="Seg_4593" n="e" s="T495">heː, </ts>
               <ts e="T497" id="Seg_4595" n="e" s="T496">barbɨttara </ts>
               <ts e="T498" id="Seg_4597" n="e" s="T497">anakkaːn", </ts>
               <ts e="T499" id="Seg_4599" n="e" s="T498">diːr. </ts>
               <ts e="T500" id="Seg_4601" n="e" s="T499">"Haŋarsɨmaŋ </ts>
               <ts e="T501" id="Seg_4603" n="e" s="T500">tɨŋnɨk", </ts>
               <ts e="T502" id="Seg_4605" n="e" s="T501">diː-diː </ts>
               <ts e="T503" id="Seg_4607" n="e" s="T502">gɨnar. </ts>
               <ts e="T504" id="Seg_4609" n="e" s="T503">Dʼe, </ts>
               <ts e="T505" id="Seg_4611" n="e" s="T504">ol </ts>
               <ts e="T507" id="Seg_4613" n="e" s="T505">kimneːtibit… </ts>
               <ts e="T508" id="Seg_4615" n="e" s="T507">Dʼe </ts>
               <ts e="T509" id="Seg_4617" n="e" s="T508">onno </ts>
               <ts e="T510" id="Seg_4619" n="e" s="T509">diː </ts>
               <ts e="T511" id="Seg_4621" n="e" s="T510">eː </ts>
               <ts e="T512" id="Seg_4623" n="e" s="T511">(dogo-) </ts>
               <ts e="T513" id="Seg_4625" n="e" s="T512">ikki </ts>
               <ts e="T514" id="Seg_4627" n="e" s="T513">kihini </ts>
               <ts e="T515" id="Seg_4629" n="e" s="T514">ɨːtallar, </ts>
               <ts e="T516" id="Seg_4631" n="e" s="T515">onno </ts>
               <ts e="T517" id="Seg_4633" n="e" s="T516">kim </ts>
               <ts e="T518" id="Seg_4635" n="e" s="T517">bu͡olaːččɨ – </ts>
               <ts e="T519" id="Seg_4637" n="e" s="T518">sančʼas. </ts>
               <ts e="T520" id="Seg_4639" n="e" s="T519">Onto </ts>
               <ts e="T521" id="Seg_4641" n="e" s="T520">"illi͡ekpit" </ts>
               <ts e="T522" id="Seg_4643" n="e" s="T521">diːller, </ts>
               <ts e="T523" id="Seg_4645" n="e" s="T522">"(min) </ts>
               <ts e="T524" id="Seg_4647" n="e" s="T523">bihigi </ts>
               <ts e="T525" id="Seg_4649" n="e" s="T524">enigin." </ts>
               <ts e="T527" id="Seg_4651" n="e" s="T525">"(Tu-) </ts>
               <ts e="T528" id="Seg_4653" n="e" s="T527">minigin </ts>
               <ts e="T529" id="Seg_4655" n="e" s="T528">tu͡ok </ts>
               <ts e="T530" id="Seg_4657" n="e" s="T529">kɨhallɨmaŋ", </ts>
               <ts e="T531" id="Seg_4659" n="e" s="T530">diːr, </ts>
               <ts e="T532" id="Seg_4661" n="e" s="T531">"min </ts>
               <ts e="T533" id="Seg_4663" n="e" s="T532">e </ts>
               <ts e="T534" id="Seg_4665" n="e" s="T533">hin </ts>
               <ts e="T535" id="Seg_4667" n="e" s="T534">(kihi </ts>
               <ts e="T536" id="Seg_4669" n="e" s="T535">bu͡olu͡ok-), </ts>
               <ts e="T537" id="Seg_4671" n="e" s="T536">e, </ts>
               <ts e="T538" id="Seg_4673" n="e" s="T537">kihi </ts>
               <ts e="T539" id="Seg_4675" n="e" s="T538">bu͡olbatakpɨn", </ts>
               <ts e="T540" id="Seg_4677" n="e" s="T539">diːr </ts>
               <ts e="T541" id="Seg_4679" n="e" s="T540">bu͡o, </ts>
               <ts e="T542" id="Seg_4681" n="e" s="T541">"(ölü͡ö-) </ts>
               <ts e="T543" id="Seg_4683" n="e" s="T542">ölü͡öm </ts>
               <ts e="T544" id="Seg_4685" n="e" s="T543">anɨ." </ts>
               <ts e="T545" id="Seg_4687" n="e" s="T544">Kihiler </ts>
               <ts e="T546" id="Seg_4689" n="e" s="T545">holbok </ts>
               <ts e="T547" id="Seg_4691" n="e" s="T546">kördük </ts>
               <ts e="T548" id="Seg_4693" n="e" s="T547">hɨtallar </ts>
               <ts e="T549" id="Seg_4695" n="e" s="T548">ile, </ts>
               <ts e="T550" id="Seg_4697" n="e" s="T549">olorton. </ts>
               <ts e="T551" id="Seg_4699" n="e" s="T550">Honon </ts>
               <ts e="T552" id="Seg_4701" n="e" s="T551">ol </ts>
               <ts e="T553" id="Seg_4703" n="e" s="T552">kimneːtiler. </ts>
               <ts e="T554" id="Seg_4705" n="e" s="T553">Ontugun </ts>
               <ts e="T555" id="Seg_4707" n="e" s="T554">iltiler </ts>
               <ts e="T556" id="Seg_4709" n="e" s="T555">ikki </ts>
               <ts e="T557" id="Seg_4711" n="e" s="T556">kihi </ts>
               <ts e="T558" id="Seg_4713" n="e" s="T557">kimi͡eke, </ts>
               <ts e="T559" id="Seg_4715" n="e" s="T558">ontuŋ </ts>
               <ts e="T560" id="Seg_4717" n="e" s="T559">otto </ts>
               <ts e="T561" id="Seg_4719" n="e" s="T560">di͡ebit </ts>
               <ts e="T562" id="Seg_4721" n="e" s="T561">ühü, </ts>
               <ts e="T563" id="Seg_4723" n="e" s="T562">"ol </ts>
               <ts e="T565" id="Seg_4725" n="e" s="T563">vsʼo ravno </ts>
               <ts e="T566" id="Seg_4727" n="e" s="T565">tiri͡ereme </ts>
               <ts e="T567" id="Seg_4729" n="e" s="T566">e </ts>
               <ts e="T568" id="Seg_4731" n="e" s="T567">kimneːn </ts>
               <ts e="T569" id="Seg_4733" n="e" s="T568">ol", </ts>
               <ts e="T570" id="Seg_4735" n="e" s="T569">onton </ts>
               <ts e="T571" id="Seg_4737" n="e" s="T570">töttörü </ts>
               <ts e="T572" id="Seg_4739" n="e" s="T571">kelliler </ts>
               <ts e="T573" id="Seg_4741" n="e" s="T572">oloruŋ. </ts>
               <ts e="T574" id="Seg_4743" n="e" s="T573">"Hin </ts>
               <ts e="T575" id="Seg_4745" n="e" s="T574">kihi </ts>
               <ts e="T576" id="Seg_4747" n="e" s="T575">bu͡olu͡om </ts>
               <ts e="T577" id="Seg_4749" n="e" s="T576">hu͡oga, </ts>
               <ts e="T578" id="Seg_4751" n="e" s="T577">tugu </ts>
               <ts e="T579" id="Seg_4753" n="e" s="T578">minigin </ts>
               <ts e="T580" id="Seg_4755" n="e" s="T579">gɨtta </ts>
               <ts e="T582" id="Seg_4757" n="e" s="T580">(vаzʼiz -) </ts>
               <ts e="T583" id="Seg_4759" n="e" s="T582">kimneːgit", </ts>
               <ts e="T584" id="Seg_4761" n="e" s="T583">diːr </ts>
               <ts e="T585" id="Seg_4763" n="e" s="T584">ühü. </ts>
               <ts e="T586" id="Seg_4765" n="e" s="T585">Harsi͡erda </ts>
               <ts e="T587" id="Seg_4767" n="e" s="T586">immit </ts>
               <ts e="T588" id="Seg_4769" n="e" s="T587">haŋardɨː </ts>
               <ts e="T589" id="Seg_4771" n="e" s="T588">taksar. </ts>
               <ts e="T590" id="Seg_4773" n="e" s="T589">Taksan </ts>
               <ts e="T592" id="Seg_4775" n="e" s="T590">(erde -) </ts>
               <ts e="T593" id="Seg_4777" n="e" s="T592">eː </ts>
               <ts e="T594" id="Seg_4779" n="e" s="T593">kimneːn </ts>
               <ts e="T595" id="Seg_4781" n="e" s="T594">erdegine, </ts>
               <ts e="T596" id="Seg_4783" n="e" s="T595">tu͡ok </ts>
               <ts e="T597" id="Seg_4785" n="e" s="T596">ere </ts>
               <ts e="T598" id="Seg_4787" n="e" s="T597">lʼejtʼenaːnɨ </ts>
               <ts e="T599" id="Seg_4789" n="e" s="T598">körüstübüt. </ts>
               <ts e="T600" id="Seg_4791" n="e" s="T599">Ontubut </ts>
               <ts e="T601" id="Seg_4793" n="e" s="T600">diːr: </ts>
               <ts e="T602" id="Seg_4795" n="e" s="T601">"Kannɨk </ts>
               <ts e="T603" id="Seg_4797" n="e" s="T602">čʼaːstaŋŋɨnɨj?" </ts>
               <ts e="T604" id="Seg_4799" n="e" s="T603">"Ehigi, </ts>
               <ts e="T605" id="Seg_4801" n="e" s="T604">ehigitten", </ts>
               <ts e="T606" id="Seg_4803" n="e" s="T605">diːbit. </ts>
               <ts e="T607" id="Seg_4805" n="e" s="T606">Diːr </ts>
               <ts e="T608" id="Seg_4807" n="e" s="T607">onton </ts>
               <ts e="T609" id="Seg_4809" n="e" s="T608">biːrbit </ts>
               <ts e="T610" id="Seg_4811" n="e" s="T609">onton. </ts>
               <ts e="T611" id="Seg_4813" n="e" s="T610">"Oččogo </ts>
               <ts e="T612" id="Seg_4815" n="e" s="T611">minigin </ts>
               <ts e="T613" id="Seg_4817" n="e" s="T612">gɨtta </ts>
               <ts e="T614" id="Seg_4819" n="e" s="T613">hüːrüŋ", </ts>
               <ts e="T615" id="Seg_4821" n="e" s="T614">diːr. </ts>
               <ts e="T616" id="Seg_4823" n="e" s="T615">Inni </ts>
               <ts e="T617" id="Seg_4825" n="e" s="T616">di͡ek </ts>
               <ts e="T618" id="Seg_4827" n="e" s="T617">xаdʼilʼi </ts>
               <ts e="T619" id="Seg_4829" n="e" s="T618">ere </ts>
               <ts e="T622" id="Seg_4831" n="e" s="T619">kimneːmi͡ekke. </ts>
               <ts e="T623" id="Seg_4833" n="e" s="T622">Svʼaz </ts>
               <ts e="T624" id="Seg_4835" n="e" s="T623">ɨla </ts>
               <ts e="T625" id="Seg_4837" n="e" s="T624">kimniːr </ts>
               <ts e="T626" id="Seg_4839" n="e" s="T625">eni. </ts>
               <ts e="T627" id="Seg_4841" n="e" s="T626">"Dʼe </ts>
               <ts e="T628" id="Seg_4843" n="e" s="T627">onton </ts>
               <ts e="T629" id="Seg_4845" n="e" s="T628">ol </ts>
               <ts e="T630" id="Seg_4847" n="e" s="T629">kimniː </ts>
               <ts e="T631" id="Seg_4849" n="e" s="T630">hatɨːbɨt." </ts>
               <ts e="T632" id="Seg_4851" n="e" s="T631">Kas </ts>
               <ts e="T633" id="Seg_4853" n="e" s="T632">da </ts>
               <ts e="T634" id="Seg_4855" n="e" s="T633">künü </ts>
               <ts e="T635" id="Seg_4857" n="e" s="T634">ol </ts>
               <ts e="T636" id="Seg_4859" n="e" s="T635">hɨrɨttɨbɨt </ts>
               <ts e="T637" id="Seg_4861" n="e" s="T636">ol </ts>
               <ts e="T638" id="Seg_4863" n="e" s="T637">kurduk. </ts>
               <ts e="T639" id="Seg_4865" n="e" s="T638">Rotabɨtɨn </ts>
               <ts e="T640" id="Seg_4867" n="e" s="T639">daː </ts>
               <ts e="T641" id="Seg_4869" n="e" s="T640">bulbappɨt. </ts>
               <ts e="T642" id="Seg_4871" n="e" s="T641">Dʼe, </ts>
               <ts e="T643" id="Seg_4873" n="e" s="T642">onton </ts>
               <ts e="T644" id="Seg_4875" n="e" s="T643">kojukkutun </ts>
               <ts e="T645" id="Seg_4877" n="e" s="T644">(kimneːti-) </ts>
               <ts e="T646" id="Seg_4879" n="e" s="T645">eː </ts>
               <ts e="T647" id="Seg_4881" n="e" s="T646">bullubut </ts>
               <ts e="T648" id="Seg_4883" n="e" s="T647">emi͡e </ts>
               <ts e="T649" id="Seg_4885" n="e" s="T648">e. </ts>
               <ts e="T651" id="Seg_4887" n="e" s="T649">(Ü-) </ts>
               <ts e="T652" id="Seg_4889" n="e" s="T651">ühüs </ts>
               <ts e="T653" id="Seg_4891" n="e" s="T652">kümmütüger </ts>
               <ts e="T654" id="Seg_4893" n="e" s="T653">duː, </ts>
               <ts e="T655" id="Seg_4895" n="e" s="T654">kahɨspɨtɨgar </ts>
               <ts e="T656" id="Seg_4897" n="e" s="T655">duː </ts>
               <ts e="T657" id="Seg_4899" n="e" s="T656">ɨntak </ts>
               <ts e="T658" id="Seg_4901" n="e" s="T657">barabɨt, </ts>
               <ts e="T659" id="Seg_4903" n="e" s="T658">onno </ts>
               <ts e="T660" id="Seg_4905" n="e" s="T659">kimi͡eke. </ts>
               <ts e="T661" id="Seg_4907" n="e" s="T660">Ol </ts>
               <ts e="T662" id="Seg_4909" n="e" s="T661">ürekpitin </ts>
               <ts e="T663" id="Seg_4911" n="e" s="T662">taksabɨt, </ts>
               <ts e="T664" id="Seg_4913" n="e" s="T663">dʼe </ts>
               <ts e="T665" id="Seg_4915" n="e" s="T664">onton </ts>
               <ts e="T666" id="Seg_4917" n="e" s="T665">ol </ts>
               <ts e="T667" id="Seg_4919" n="e" s="T666">u͡oldʼahabɨt, </ts>
               <ts e="T668" id="Seg_4921" n="e" s="T667">ol </ts>
               <ts e="T669" id="Seg_4923" n="e" s="T668">ɨnaraːttan </ts>
               <ts e="T670" id="Seg_4925" n="e" s="T669">ɨtɨ͡alɨːllar, </ts>
               <ts e="T671" id="Seg_4927" n="e" s="T670">a </ts>
               <ts e="T672" id="Seg_4929" n="e" s="T671">bihigi </ts>
               <ts e="T673" id="Seg_4931" n="e" s="T672">betereːtten </ts>
               <ts e="T674" id="Seg_4933" n="e" s="T673">emi͡e </ts>
               <ts e="T675" id="Seg_4935" n="e" s="T674">ɨtɨ͡alahabɨt </ts>
               <ts e="T676" id="Seg_4937" n="e" s="T675">bu͡olla. </ts>
               <ts e="T677" id="Seg_4939" n="e" s="T676">Dʼe </ts>
               <ts e="T678" id="Seg_4941" n="e" s="T677">ol </ts>
               <ts e="T679" id="Seg_4943" n="e" s="T678">kimneːtibit </ts>
               <ts e="T680" id="Seg_4945" n="e" s="T679">daː </ts>
               <ts e="T681" id="Seg_4947" n="e" s="T680">onno, </ts>
               <ts e="T682" id="Seg_4949" n="e" s="T681">kajdi͡ek </ts>
               <ts e="T683" id="Seg_4951" n="e" s="T682">daː </ts>
               <ts e="T684" id="Seg_4953" n="e" s="T683">kimneːbetter, </ts>
               <ts e="T685" id="Seg_4955" n="e" s="T684">ɨːppattar </ts>
               <ts e="T686" id="Seg_4957" n="e" s="T685">ol. </ts>
               <ts e="T687" id="Seg_4959" n="e" s="T686">Harsi͡erda </ts>
               <ts e="T688" id="Seg_4961" n="e" s="T687">kɨ͡attaːk </ts>
               <ts e="T689" id="Seg_4963" n="e" s="T688">kelliler </ts>
               <ts e="T690" id="Seg_4965" n="e" s="T689">ol </ts>
               <ts e="T691" id="Seg_4967" n="e" s="T690">di͡ek </ts>
               <ts e="T692" id="Seg_4969" n="e" s="T691">innibititten, </ts>
               <ts e="T693" id="Seg_4971" n="e" s="T692">bu </ts>
               <ts e="T695" id="Seg_4973" n="e" s="T693">(bi -) </ts>
               <ts e="T696" id="Seg_4975" n="e" s="T695">kamanda </ts>
               <ts e="T697" id="Seg_4977" n="e" s="T696">bi͡ereller: </ts>
               <ts e="T698" id="Seg_4979" n="e" s="T697">"Nu, </ts>
               <ts e="T699" id="Seg_4981" n="e" s="T698">otto </ts>
               <ts e="T700" id="Seg_4983" n="e" s="T699">üs </ts>
               <ts e="T701" id="Seg_4985" n="e" s="T700">čʼaːska </ts>
               <ts e="T702" id="Seg_4987" n="e" s="T701">Brʼanskajɨ </ts>
               <ts e="T703" id="Seg_4989" n="e" s="T702">zanʼimajdɨ͡akkɨtɨn </ts>
               <ts e="T704" id="Seg_4991" n="e" s="T703">naːda." </ts>
               <ts e="T705" id="Seg_4993" n="e" s="T704">Dʼe </ts>
               <ts e="T706" id="Seg_4995" n="e" s="T705">onno </ts>
               <ts e="T707" id="Seg_4997" n="e" s="T706">bugurduk </ts>
               <ts e="T708" id="Seg_4999" n="e" s="T707">tu͡ok </ts>
               <ts e="T709" id="Seg_5001" n="e" s="T708">ere </ts>
               <ts e="T710" id="Seg_5003" n="e" s="T709">hɨːr </ts>
               <ts e="T711" id="Seg_5005" n="e" s="T710">bu͡olar – </ts>
               <ts e="T712" id="Seg_5007" n="e" s="T711">dʼerbi. </ts>
               <ts e="T714" id="Seg_5009" n="e" s="T712">(Hu -) </ts>
               <ts e="T715" id="Seg_5011" n="e" s="T714">dʼerbini </ts>
               <ts e="T716" id="Seg_5013" n="e" s="T715">e </ts>
               <ts e="T717" id="Seg_5015" n="e" s="T716">kimneːtibit </ts>
               <ts e="T718" id="Seg_5017" n="e" s="T717">da </ts>
               <ts e="T719" id="Seg_5019" n="e" s="T718">emi͡e. </ts>
               <ts e="T720" id="Seg_5021" n="e" s="T719">Emi͡e </ts>
               <ts e="T721" id="Seg_5023" n="e" s="T720">hɨraja, </ts>
               <ts e="T723" id="Seg_5025" n="e" s="T721">(hu͡o-) </ts>
               <ts e="T724" id="Seg_5027" n="e" s="T723">innitten </ts>
               <ts e="T725" id="Seg_5029" n="e" s="T724">kimniːller </ts>
               <ts e="T726" id="Seg_5031" n="e" s="T725">bu͡o </ts>
               <ts e="T728" id="Seg_5033" n="e" s="T726">(uhugut-) </ts>
               <ts e="T729" id="Seg_5035" n="e" s="T728">nʼemʼester </ts>
               <ts e="T730" id="Seg_5037" n="e" s="T729">bugurduk </ts>
               <ts e="T731" id="Seg_5039" n="e" s="T730">hɨtallar, </ts>
               <ts e="T732" id="Seg_5041" n="e" s="T731">аbаrаnʼatsa </ts>
               <ts e="T733" id="Seg_5043" n="e" s="T732">gɨna </ts>
               <ts e="T734" id="Seg_5045" n="e" s="T733">hɨtallar </ts>
               <ts e="T735" id="Seg_5047" n="e" s="T734">bu͡o. </ts>
               <ts e="T736" id="Seg_5049" n="e" s="T735">A </ts>
               <ts e="T737" id="Seg_5051" n="e" s="T736">bihigi </ts>
               <ts e="T738" id="Seg_5053" n="e" s="T737">bu </ts>
               <ts e="T739" id="Seg_5055" n="e" s="T738">dʼi͡egitten </ts>
               <ts e="T740" id="Seg_5057" n="e" s="T739">emi͡e </ts>
               <ts e="T741" id="Seg_5059" n="e" s="T740">ɨtɨ͡alahabɨt. </ts>
               <ts e="T742" id="Seg_5061" n="e" s="T741">Dʼe </ts>
               <ts e="T743" id="Seg_5063" n="e" s="T742">onton </ts>
               <ts e="T744" id="Seg_5065" n="e" s="T743">bu </ts>
               <ts e="T745" id="Seg_5067" n="e" s="T744">tüːnü </ts>
               <ts e="T746" id="Seg_5069" n="e" s="T745">huptu </ts>
               <ts e="T747" id="Seg_5071" n="e" s="T746">hɨtɨː </ts>
               <ts e="T748" id="Seg_5073" n="e" s="T747">munna </ts>
               <ts e="T749" id="Seg_5075" n="e" s="T748">bu͡o. </ts>
               <ts e="T750" id="Seg_5077" n="e" s="T749">Tüːnü </ts>
               <ts e="T751" id="Seg_5079" n="e" s="T750">u͡onnar </ts>
               <ts e="T752" id="Seg_5081" n="e" s="T751">duː </ts>
               <ts e="T753" id="Seg_5083" n="e" s="T752">ikkiler </ts>
               <ts e="T754" id="Seg_5085" n="e" s="T753">di͡ek </ts>
               <ts e="T755" id="Seg_5087" n="e" s="T754">duː, </ts>
               <ts e="T756" id="Seg_5089" n="e" s="T755">ili </ts>
               <ts e="T757" id="Seg_5091" n="e" s="T756">čʼaːstar </ts>
               <ts e="T758" id="Seg_5093" n="e" s="T757">di͡ek </ts>
               <ts e="T759" id="Seg_5095" n="e" s="T758">duː </ts>
               <ts e="T760" id="Seg_5097" n="e" s="T759">ol </ts>
               <ts e="T761" id="Seg_5099" n="e" s="T760">rakʼetɨ </ts>
               <ts e="T762" id="Seg_5101" n="e" s="T761">ɨːtallar. </ts>
               <ts e="T763" id="Seg_5103" n="e" s="T762">Rakʼetɨ </ts>
               <ts e="T764" id="Seg_5105" n="e" s="T763">ɨːttagɨna </ts>
               <ts e="T765" id="Seg_5107" n="e" s="T764">oččogo </ts>
               <ts e="T766" id="Seg_5109" n="e" s="T765">bu </ts>
               <ts e="T767" id="Seg_5111" n="e" s="T766">barɨta </ts>
               <ts e="T768" id="Seg_5113" n="e" s="T767">аbаrаnʼassa, </ts>
               <ts e="T769" id="Seg_5115" n="e" s="T768">kimniː </ts>
               <ts e="T770" id="Seg_5117" n="e" s="T769">hɨtallar </ts>
               <ts e="T771" id="Seg_5119" n="e" s="T770">barɨta </ts>
               <ts e="T772" id="Seg_5121" n="e" s="T771">köstöllör. </ts>
               <ts e="T773" id="Seg_5123" n="e" s="T772">Iliːbin </ts>
               <ts e="T774" id="Seg_5125" n="e" s="T773">taptaran </ts>
               <ts e="T775" id="Seg_5127" n="e" s="T774">bu </ts>
               <ts e="T776" id="Seg_5129" n="e" s="T775">dʼüːleːbetim </ts>
               <ts e="T777" id="Seg_5131" n="e" s="T776">onton. </ts>
               <ts e="T778" id="Seg_5133" n="e" s="T777">Ol </ts>
               <ts e="T779" id="Seg_5135" n="e" s="T778">muntubar </ts>
               <ts e="T780" id="Seg_5137" n="e" s="T779">(tap-) </ts>
               <ts e="T781" id="Seg_5139" n="e" s="T780">kimneːbit </ts>
               <ts e="T782" id="Seg_5141" n="e" s="T781">ol </ts>
               <ts e="T783" id="Seg_5143" n="e" s="T782">huptu </ts>
               <ts e="T784" id="Seg_5145" n="e" s="T783">kötön. </ts>
               <ts e="T785" id="Seg_5147" n="e" s="T784">Biːr </ts>
               <ts e="T786" id="Seg_5149" n="e" s="T785">mina </ts>
               <ts e="T787" id="Seg_5151" n="e" s="T786">kelle </ts>
               <ts e="T788" id="Seg_5153" n="e" s="T787">da </ts>
               <ts e="T789" id="Seg_5155" n="e" s="T788">barɨbɨtɨn </ts>
               <ts e="T790" id="Seg_5157" n="e" s="T789">holoːbut </ts>
               <ts e="T791" id="Seg_5159" n="e" s="T790">ol – </ts>
               <ts e="T792" id="Seg_5161" n="e" s="T791">u͡on </ts>
               <ts e="T793" id="Seg_5163" n="e" s="T792">orduk </ts>
               <ts e="T794" id="Seg_5165" n="e" s="T793">kihini. </ts>
               <ts e="T795" id="Seg_5167" n="e" s="T794">Haːbɨn </ts>
               <ts e="T796" id="Seg_5169" n="e" s="T795">burtuk </ts>
               <ts e="T797" id="Seg_5171" n="e" s="T796">tutarbar </ts>
               <ts e="T798" id="Seg_5173" n="e" s="T797">öjdönnüm </ts>
               <ts e="T799" id="Seg_5175" n="e" s="T798">onton </ts>
               <ts e="T800" id="Seg_5177" n="e" s="T799">innʼe. </ts>
               <ts e="T801" id="Seg_5179" n="e" s="T800">Innʼe </ts>
               <ts e="T802" id="Seg_5181" n="e" s="T801">turabɨn </ts>
               <ts e="T803" id="Seg_5183" n="e" s="T802">tu͡ok, </ts>
               <ts e="T804" id="Seg_5185" n="e" s="T803">tobuktuː. </ts>
               <ts e="T805" id="Seg_5187" n="e" s="T804">Dʼe, </ts>
               <ts e="T806" id="Seg_5189" n="e" s="T805">onton </ts>
               <ts e="T807" id="Seg_5191" n="e" s="T806">tüstüm </ts>
               <ts e="T808" id="Seg_5193" n="e" s="T807">bugurduk. </ts>
               <ts e="T809" id="Seg_5195" n="e" s="T808">Dʼe </ts>
               <ts e="T810" id="Seg_5197" n="e" s="T809">onton </ts>
               <ts e="T811" id="Seg_5199" n="e" s="T810">kojut </ts>
               <ts e="T812" id="Seg_5201" n="e" s="T811">kördüm </ts>
               <ts e="T813" id="Seg_5203" n="e" s="T812">bu͡o, </ts>
               <ts e="T814" id="Seg_5205" n="e" s="T813">čömüjelerim </ts>
               <ts e="T815" id="Seg_5207" n="e" s="T814">barɨta </ts>
               <ts e="T816" id="Seg_5209" n="e" s="T815">halɨbɨrɨː </ts>
               <ts e="T817" id="Seg_5211" n="e" s="T816">hɨldʼallar, </ts>
               <ts e="T818" id="Seg_5213" n="e" s="T817">kaːnɨm </ts>
               <ts e="T819" id="Seg_5215" n="e" s="T818">usta </ts>
               <ts e="T820" id="Seg_5217" n="e" s="T819">hɨtar </ts>
               <ts e="T821" id="Seg_5219" n="e" s="T820">bugurduk. </ts>
               <ts e="T822" id="Seg_5221" n="e" s="T821">Dʼe </ts>
               <ts e="T823" id="Seg_5223" n="e" s="T822">onton </ts>
               <ts e="T824" id="Seg_5225" n="e" s="T823">kurbun </ts>
               <ts e="T825" id="Seg_5227" n="e" s="T824">uhullum </ts>
               <ts e="T826" id="Seg_5229" n="e" s="T825">bugurduk. </ts>
               <ts e="T827" id="Seg_5231" n="e" s="T826">Kurbun </ts>
               <ts e="T828" id="Seg_5233" n="e" s="T827">uhulan </ts>
               <ts e="T829" id="Seg_5235" n="e" s="T828">baraːmmɨn, </ts>
               <ts e="T830" id="Seg_5237" n="e" s="T829">burtuk </ts>
               <ts e="T831" id="Seg_5239" n="e" s="T830">kimneːtim, </ts>
               <ts e="T832" id="Seg_5241" n="e" s="T831">enʼi </ts>
               <ts e="T833" id="Seg_5243" n="e" s="T832">kimmer, </ts>
               <ts e="T834" id="Seg_5245" n="e" s="T833">mu͡ojbar </ts>
               <ts e="T835" id="Seg_5247" n="e" s="T834">innʼe </ts>
               <ts e="T837" id="Seg_5249" n="e" s="T835">(kett -) </ts>
               <ts e="T838" id="Seg_5251" n="e" s="T837">kimneːtim </ts>
               <ts e="T839" id="Seg_5253" n="e" s="T838">bu </ts>
               <ts e="T840" id="Seg_5255" n="e" s="T839">baːjan </ts>
               <ts e="T841" id="Seg_5257" n="e" s="T840">baraːmmɨn. </ts>
               <ts e="T842" id="Seg_5259" n="e" s="T841">Onton </ts>
               <ts e="T843" id="Seg_5261" n="e" s="T842">bu </ts>
               <ts e="T844" id="Seg_5263" n="e" s="T843">pakʼettar </ts>
               <ts e="T845" id="Seg_5265" n="e" s="T844">bu͡olaːččɨlar </ts>
               <ts e="T846" id="Seg_5267" n="e" s="T845">každɨj </ts>
               <ts e="T847" id="Seg_5269" n="e" s="T846">pʼerʼevʼazаčʼnɨj, </ts>
               <ts e="T848" id="Seg_5271" n="e" s="T847">dʼe </ts>
               <ts e="T849" id="Seg_5273" n="e" s="T848">ontubunan </ts>
               <ts e="T850" id="Seg_5275" n="e" s="T849">erijdim </ts>
               <ts e="T851" id="Seg_5277" n="e" s="T850">bugurduk. </ts>
               <ts e="T852" id="Seg_5279" n="e" s="T851">Onton </ts>
               <ts e="T853" id="Seg_5281" n="e" s="T852">ommu͡otka </ts>
               <ts e="T854" id="Seg_5283" n="e" s="T853">iti </ts>
               <ts e="T855" id="Seg_5285" n="e" s="T854">bočinkagar </ts>
               <ts e="T856" id="Seg_5287" n="e" s="T855">kim </ts>
               <ts e="T857" id="Seg_5289" n="e" s="T856">bu͡olaːččɨ </ts>
               <ts e="T859" id="Seg_5291" n="e" s="T857">(eri-). </ts>
               <ts e="T860" id="Seg_5293" n="e" s="T859">Onu </ts>
               <ts e="T861" id="Seg_5295" n="e" s="T860">ommotkabɨnan </ts>
               <ts e="T862" id="Seg_5297" n="e" s="T861">erijdim. </ts>
               <ts e="T863" id="Seg_5299" n="e" s="T862">"Bar", </ts>
               <ts e="T864" id="Seg_5301" n="e" s="T863">diːller, </ts>
               <ts e="T865" id="Seg_5303" n="e" s="T864">"kim </ts>
               <ts e="T866" id="Seg_5305" n="e" s="T865">kamandʼir </ts>
               <ts e="T867" id="Seg_5307" n="e" s="T866">rotagar." </ts>
               <ts e="T868" id="Seg_5309" n="e" s="T867">Hol </ts>
               <ts e="T869" id="Seg_5311" n="e" s="T868">ünen </ts>
               <ts e="T870" id="Seg_5313" n="e" s="T869">ümmütünen </ts>
               <ts e="T871" id="Seg_5315" n="e" s="T870">tijdim </ts>
               <ts e="T872" id="Seg_5317" n="e" s="T871">ontubar. </ts>
               <ts e="T873" id="Seg_5319" n="e" s="T872">Kilep </ts>
               <ts e="T874" id="Seg_5321" n="e" s="T873">kördöːn </ts>
               <ts e="T875" id="Seg_5323" n="e" s="T874">eː </ts>
               <ts e="T877" id="Seg_5325" n="e" s="T875">(ki -) </ts>
               <ts e="T878" id="Seg_5327" n="e" s="T877">as. </ts>
               <ts e="T879" id="Seg_5329" n="e" s="T878">"Kaja", </ts>
               <ts e="T880" id="Seg_5331" n="e" s="T879">diːr, </ts>
               <ts e="T881" id="Seg_5333" n="e" s="T880">"raniː </ts>
               <ts e="T882" id="Seg_5335" n="e" s="T881">(gɨn-) </ts>
               <ts e="T930" id="Seg_5337" n="e" s="T882">((…)) </ts>
               <ts e="T883" id="Seg_5339" n="e" s="T930">duː", </ts>
               <ts e="T885" id="Seg_5341" n="e" s="T883">diːr. </ts>
               <ts e="T886" id="Seg_5343" n="e" s="T885">"Hmm", </ts>
               <ts e="T887" id="Seg_5345" n="e" s="T886">diːbin, </ts>
               <ts e="T888" id="Seg_5347" n="e" s="T887">haːbɨn </ts>
               <ts e="T889" id="Seg_5349" n="e" s="T888">onton </ts>
               <ts e="T890" id="Seg_5351" n="e" s="T889">ɨlbɨt </ts>
               <ts e="T891" id="Seg_5353" n="e" s="T890">ete </ts>
               <ts e="T892" id="Seg_5355" n="e" s="T891">ol </ts>
               <ts e="T893" id="Seg_5357" n="e" s="T892">kimim, </ts>
               <ts e="T894" id="Seg_5359" n="e" s="T893">kamandʼirbar </ts>
               <ts e="T895" id="Seg_5361" n="e" s="T894">sdavajdɨːbɨn </ts>
               <ts e="T896" id="Seg_5363" n="e" s="T895">bu͡o. </ts>
               <ts e="T897" id="Seg_5365" n="e" s="T896">Kimŋe </ts>
               <ts e="T898" id="Seg_5367" n="e" s="T897">nomʼerɨnan </ts>
               <ts e="T899" id="Seg_5369" n="e" s="T898">bu͡olaːččɨ </ts>
               <ts e="T900" id="Seg_5371" n="e" s="T899">etilere. </ts>
               <ts e="T901" id="Seg_5373" n="e" s="T900">Ontum </ts>
               <ts e="T902" id="Seg_5375" n="e" s="T901">bu͡o </ts>
               <ts e="T903" id="Seg_5377" n="e" s="T902">kim </ts>
               <ts e="T904" id="Seg_5379" n="e" s="T903">patru͡ottar </ts>
               <ts e="T905" id="Seg_5381" n="e" s="T904">karmaːmmar, </ts>
               <ts e="T906" id="Seg_5383" n="e" s="T905">tu͡ok </ts>
               <ts e="T907" id="Seg_5385" n="e" s="T906">daː </ts>
               <ts e="T908" id="Seg_5387" n="e" s="T907">hu͡ok, </ts>
               <ts e="T909" id="Seg_5389" n="e" s="T908">granaːttarbɨn </ts>
               <ts e="T910" id="Seg_5391" n="e" s="T909">emi͡e </ts>
               <ts e="T911" id="Seg_5393" n="e" s="T910">ɨlbɨttar. </ts>
               <ts e="T912" id="Seg_5395" n="e" s="T911">Oku͡opaga </ts>
               <ts e="T913" id="Seg_5397" n="e" s="T912">hɨttakpɨna, </ts>
               <ts e="T914" id="Seg_5399" n="e" s="T913">kumak </ts>
               <ts e="T915" id="Seg_5401" n="e" s="T914">tüheːktiːr </ts>
               <ts e="T916" id="Seg_5403" n="e" s="T915">diːn </ts>
               <ts e="T917" id="Seg_5405" n="e" s="T916">ol </ts>
               <ts e="T918" id="Seg_5407" n="e" s="T917">di͡egitten </ts>
               <ts e="T919" id="Seg_5409" n="e" s="T918">kele, </ts>
               <ts e="T920" id="Seg_5411" n="e" s="T919">ite </ts>
               <ts e="T921" id="Seg_5413" n="e" s="T920">miːnalar </ts>
               <ts e="T922" id="Seg_5415" n="e" s="T921">tüstekterine, </ts>
               <ts e="T923" id="Seg_5417" n="e" s="T922">hirim </ts>
               <ts e="T924" id="Seg_5419" n="e" s="T923">dorgujar. </ts>
               <ts e="T928" id="Seg_5421" n="e" s="T924">Šɨrk-šɨrk-šɨrk </ts>
               <ts e="T929" id="Seg_5423" n="e" s="T928">kimniːller </ts>
               <ts e="T931" id="Seg_5425" n="e" s="T929">araj. </ts>
               <ts e="T932" id="Seg_5427" n="e" s="T931">Ontum </ts>
               <ts e="T933" id="Seg_5429" n="e" s="T932">bu </ts>
               <ts e="T934" id="Seg_5431" n="e" s="T933">karda </ts>
               <ts e="T937" id="Seg_5433" n="e" s="T934">((…)) </ts>
               <ts e="T935" id="Seg_5435" n="e" s="T937">zapаlnʼenʼije </ts>
               <ts e="T936" id="Seg_5437" n="e" s="T935">keler </ts>
               <ts e="T938" id="Seg_5439" n="e" s="T936">ebit. </ts>
               <ts e="T939" id="Seg_5441" n="e" s="T938">Bihigi </ts>
               <ts e="T940" id="Seg_5443" n="e" s="T939">onnubutugar </ts>
               <ts e="T941" id="Seg_5445" n="e" s="T940">össü͡ö </ts>
               <ts e="T942" id="Seg_5447" n="e" s="T941">nöŋü͡öler </ts>
               <ts e="T943" id="Seg_5449" n="e" s="T942">keleller, </ts>
               <ts e="T944" id="Seg_5451" n="e" s="T943">bu͡olar. </ts>
               <ts e="T945" id="Seg_5453" n="e" s="T944">Aːjtaːn </ts>
               <ts e="T946" id="Seg_5455" n="e" s="T945">bu͡olaːktɨːr </ts>
               <ts e="T947" id="Seg_5457" n="e" s="T946">diːn </ts>
               <ts e="T948" id="Seg_5459" n="e" s="T947">onton, </ts>
               <ts e="T949" id="Seg_5461" n="e" s="T948">ɨtahallar, </ts>
               <ts e="T950" id="Seg_5463" n="e" s="T949">bu͡olallar </ts>
               <ts e="T951" id="Seg_5465" n="e" s="T950">araj. </ts>
               <ts e="T952" id="Seg_5467" n="e" s="T951">Horok-horok </ts>
               <ts e="T953" id="Seg_5469" n="e" s="T952">ɨnčɨktɨːr </ts>
               <ts e="T954" id="Seg_5471" n="e" s="T953">bu͡olar. </ts>
               <ts e="T955" id="Seg_5473" n="e" s="T954">ɨnčɨktɨːr </ts>
               <ts e="T956" id="Seg_5475" n="e" s="T955">bu͡ollaktarɨna, </ts>
               <ts e="T957" id="Seg_5477" n="e" s="T956">min </ts>
               <ts e="T958" id="Seg_5479" n="e" s="T957">emi͡e </ts>
               <ts e="T959" id="Seg_5481" n="e" s="T958">emi͡e </ts>
               <ts e="T960" id="Seg_5483" n="e" s="T959">ɨnčɨktɨːbɨn </ts>
               <ts e="T961" id="Seg_5485" n="e" s="T960">emi͡e </ts>
               <ts e="T962" id="Seg_5487" n="e" s="T961">onton </ts>
               <ts e="T963" id="Seg_5489" n="e" s="T962">emi͡e </ts>
               <ts e="T964" id="Seg_5491" n="e" s="T963">dʼɨlɨs </ts>
               <ts e="T965" id="Seg_5493" n="e" s="T964">gɨnabɨn, </ts>
               <ts e="T966" id="Seg_5495" n="e" s="T965">ihilliːbin. </ts>
               <ts e="T967" id="Seg_5497" n="e" s="T966">Sančʼaska </ts>
               <ts e="T968" id="Seg_5499" n="e" s="T967">kelbitim. </ts>
               <ts e="T970" id="Seg_5501" n="e" s="T968">(San-) </ts>
               <ts e="T971" id="Seg_5503" n="e" s="T970">pаlʼevoj </ts>
               <ts e="T972" id="Seg_5505" n="e" s="T971">üle </ts>
               <ts e="T973" id="Seg_5507" n="e" s="T972">kim </ts>
               <ts e="T974" id="Seg_5509" n="e" s="T973">bu͡olaːččɨ, </ts>
               <ts e="T975" id="Seg_5511" n="e" s="T974">sančʼas. </ts>
               <ts e="T976" id="Seg_5513" n="e" s="T975">Oːl </ts>
               <ts e="T977" id="Seg_5515" n="e" s="T976">sančʼaska </ts>
               <ts e="T978" id="Seg_5517" n="e" s="T977">(kelli-) </ts>
               <ts e="T979" id="Seg_5519" n="e" s="T978">eː </ts>
               <ts e="T980" id="Seg_5521" n="e" s="T979">kellim </ts>
               <ts e="T981" id="Seg_5523" n="e" s="T980">da </ts>
               <ts e="T982" id="Seg_5525" n="e" s="T981">tɨlga </ts>
               <ts e="T983" id="Seg_5527" n="e" s="T982">ɨːppɨttara. </ts>
               <ts e="T984" id="Seg_5529" n="e" s="T983">Ivanava Suja </ts>
               <ts e="T985" id="Seg_5531" n="e" s="T984">hɨppɨtɨm – </ts>
               <ts e="T986" id="Seg_5533" n="e" s="T985">erge </ts>
               <ts e="T987" id="Seg_5535" n="e" s="T986">hospitalʼga. </ts>
               <ts e="T988" id="Seg_5537" n="e" s="T987">Onton </ts>
               <ts e="T990" id="Seg_5539" n="e" s="T988">(sra -) </ts>
               <ts e="T991" id="Seg_5541" n="e" s="T990">eː </ts>
               <ts e="T992" id="Seg_5543" n="e" s="T991">srazu </ts>
               <ts e="T993" id="Seg_5545" n="e" s="T992">bi͡es </ts>
               <ts e="T994" id="Seg_5547" n="e" s="T993">ɨjɨ </ts>
               <ts e="T995" id="Seg_5549" n="e" s="T994">hɨttɨm </ts>
               <ts e="T996" id="Seg_5551" n="e" s="T995">da, </ts>
               <ts e="T997" id="Seg_5553" n="e" s="T996">dʼi͡em </ts>
               <ts e="T998" id="Seg_5555" n="e" s="T997">di͡ek </ts>
               <ts e="T999" id="Seg_5557" n="e" s="T998">ɨːppɨttara. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-ChVD">
            <ta e="T5" id="Seg_5558" s="T0">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.001 (001.001)</ta>
            <ta e="T14" id="Seg_5559" s="T5">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.002 (001.002)</ta>
            <ta e="T16" id="Seg_5560" s="T14">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.003 (001.003)</ta>
            <ta e="T27" id="Seg_5561" s="T16">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.004 (001.004)</ta>
            <ta e="T33" id="Seg_5562" s="T27">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.005 (001.005)</ta>
            <ta e="T40" id="Seg_5563" s="T33">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.006 (001.006)</ta>
            <ta e="T55" id="Seg_5564" s="T40">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.007 (001.007)</ta>
            <ta e="T59" id="Seg_5565" s="T55">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.008 (001.008)</ta>
            <ta e="T70" id="Seg_5566" s="T59">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.009 (001.009)</ta>
            <ta e="T80" id="Seg_5567" s="T70">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.010 (001.010)</ta>
            <ta e="T90" id="Seg_5568" s="T80">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.011 (001.011)</ta>
            <ta e="T100" id="Seg_5569" s="T90">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.012 (001.012)</ta>
            <ta e="T109" id="Seg_5570" s="T100">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.013 (001.013)</ta>
            <ta e="T115" id="Seg_5571" s="T109">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.014 (001.014)</ta>
            <ta e="T127" id="Seg_5572" s="T115">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.015 (001.015)</ta>
            <ta e="T142" id="Seg_5573" s="T127">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.016 (001.016)</ta>
            <ta e="T152" id="Seg_5574" s="T142">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.017 (001.017)</ta>
            <ta e="T166" id="Seg_5575" s="T152">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.018 (001.018)</ta>
            <ta e="T174" id="Seg_5576" s="T166">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.019 (001.019)</ta>
            <ta e="T179" id="Seg_5577" s="T174">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.020 (001.020)</ta>
            <ta e="T190" id="Seg_5578" s="T179">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.021 (001.021)</ta>
            <ta e="T193" id="Seg_5579" s="T190">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.022 (001.022)</ta>
            <ta e="T197" id="Seg_5580" s="T193">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.023 (001.023)</ta>
            <ta e="T204" id="Seg_5581" s="T197">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.024 (001.024)</ta>
            <ta e="T208" id="Seg_5582" s="T204">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.025 (001.025)</ta>
            <ta e="T214" id="Seg_5583" s="T208">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.026 (001.026)</ta>
            <ta e="T229" id="Seg_5584" s="T214">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.027 (001.027)</ta>
            <ta e="T241" id="Seg_5585" s="T229">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.028 (001.028)</ta>
            <ta e="T251" id="Seg_5586" s="T241">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.029 (001.029)</ta>
            <ta e="T262" id="Seg_5587" s="T251">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.030 (001.030)</ta>
            <ta e="T268" id="Seg_5588" s="T262">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.031 (001.031)</ta>
            <ta e="T278" id="Seg_5589" s="T268">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.032 (001.032)</ta>
            <ta e="T288" id="Seg_5590" s="T278">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.033 (001.033)</ta>
            <ta e="T296" id="Seg_5591" s="T288">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.034 (001.034)</ta>
            <ta e="T300" id="Seg_5592" s="T296">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.035 (001.035)</ta>
            <ta e="T305" id="Seg_5593" s="T300">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.036 (001.036)</ta>
            <ta e="T309" id="Seg_5594" s="T305">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.037 (001.037)</ta>
            <ta e="T321" id="Seg_5595" s="T309">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.038 (001.038)</ta>
            <ta e="T323" id="Seg_5596" s="T321">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.039 (001.039)</ta>
            <ta e="T328" id="Seg_5597" s="T323">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.040 (001.040)</ta>
            <ta e="T335" id="Seg_5598" s="T328">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.041 (001.041)</ta>
            <ta e="T345" id="Seg_5599" s="T335">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.042 (001.042)</ta>
            <ta e="T357" id="Seg_5600" s="T345">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.043 (001.043)</ta>
            <ta e="T362" id="Seg_5601" s="T357">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.044 (001.044)</ta>
            <ta e="T368" id="Seg_5602" s="T362">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.045 (001.045)</ta>
            <ta e="T377" id="Seg_5603" s="T368">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.046 (001.046)</ta>
            <ta e="T381" id="Seg_5604" s="T377">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.047 (001.047)</ta>
            <ta e="T387" id="Seg_5605" s="T381">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.048 (001.048)</ta>
            <ta e="T401" id="Seg_5606" s="T387">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.049 (001.049)</ta>
            <ta e="T412" id="Seg_5607" s="T401">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.050 (001.050)</ta>
            <ta e="T416" id="Seg_5608" s="T412">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.051 (001.051)</ta>
            <ta e="T424" id="Seg_5609" s="T416">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.052 (001.052)</ta>
            <ta e="T430" id="Seg_5610" s="T424">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.053 (001.053)</ta>
            <ta e="T434" id="Seg_5611" s="T430">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.054 (001.054)</ta>
            <ta e="T446" id="Seg_5612" s="T434">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.055 (001.055)</ta>
            <ta e="T451" id="Seg_5613" s="T446">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.056 (001.056)</ta>
            <ta e="T467" id="Seg_5614" s="T451">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.057 (001.057)</ta>
            <ta e="T471" id="Seg_5615" s="T467">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.058 (001.058)</ta>
            <ta e="T475" id="Seg_5616" s="T471">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.059 (001.059)</ta>
            <ta e="T482" id="Seg_5617" s="T475">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.060 (001.060)</ta>
            <ta e="T494" id="Seg_5618" s="T482">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.061 (001.061)</ta>
            <ta e="T499" id="Seg_5619" s="T494">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.062 (001.062)</ta>
            <ta e="T503" id="Seg_5620" s="T499">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.063 (001.063)</ta>
            <ta e="T507" id="Seg_5621" s="T503">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.064 (001.064)</ta>
            <ta e="T519" id="Seg_5622" s="T507">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.065 (001.065)</ta>
            <ta e="T525" id="Seg_5623" s="T519">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.066 (001.066)</ta>
            <ta e="T544" id="Seg_5624" s="T525">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.067 (001.067)</ta>
            <ta e="T550" id="Seg_5625" s="T544">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.068 (001.068)</ta>
            <ta e="T553" id="Seg_5626" s="T550">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.069 (001.069)</ta>
            <ta e="T573" id="Seg_5627" s="T553">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.070 (001.070)</ta>
            <ta e="T585" id="Seg_5628" s="T573">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.071 (001.071)</ta>
            <ta e="T589" id="Seg_5629" s="T585">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.072 (001.072)</ta>
            <ta e="T599" id="Seg_5630" s="T589">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.073 (001.073)</ta>
            <ta e="T601" id="Seg_5631" s="T599">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.074 (001.074)</ta>
            <ta e="T603" id="Seg_5632" s="T601">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.075 (001.075)</ta>
            <ta e="T606" id="Seg_5633" s="T603">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.076 (001.076)</ta>
            <ta e="T610" id="Seg_5634" s="T606">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.077 (001.077)</ta>
            <ta e="T615" id="Seg_5635" s="T610">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.078 (001.078)</ta>
            <ta e="T622" id="Seg_5636" s="T615">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.079 (001.079)</ta>
            <ta e="T626" id="Seg_5637" s="T622">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.080 (001.080)</ta>
            <ta e="T631" id="Seg_5638" s="T626">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.081 (001.081)</ta>
            <ta e="T638" id="Seg_5639" s="T631">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.082 (001.082)</ta>
            <ta e="T641" id="Seg_5640" s="T638">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.083 (001.083)</ta>
            <ta e="T649" id="Seg_5641" s="T641">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.084 (001.084)</ta>
            <ta e="T660" id="Seg_5642" s="T649">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.085 (001.085)</ta>
            <ta e="T676" id="Seg_5643" s="T660">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.086 (001.086)</ta>
            <ta e="T686" id="Seg_5644" s="T676">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.087 (001.087)</ta>
            <ta e="T697" id="Seg_5645" s="T686">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.088 (001.088)</ta>
            <ta e="T704" id="Seg_5646" s="T697">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.089 (001.089)</ta>
            <ta e="T712" id="Seg_5647" s="T704">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.090 (001.090)</ta>
            <ta e="T719" id="Seg_5648" s="T712">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.091 (001.091)</ta>
            <ta e="T735" id="Seg_5649" s="T719">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.092 (001.092)</ta>
            <ta e="T741" id="Seg_5650" s="T735">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.093 (001.093)</ta>
            <ta e="T749" id="Seg_5651" s="T741">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.094 (001.094)</ta>
            <ta e="T762" id="Seg_5652" s="T749">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.095 (001.095)</ta>
            <ta e="T772" id="Seg_5653" s="T762">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.096 (001.096)</ta>
            <ta e="T777" id="Seg_5654" s="T772">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.097 (001.097)</ta>
            <ta e="T784" id="Seg_5655" s="T777">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.098 (001.098)</ta>
            <ta e="T794" id="Seg_5656" s="T784">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.099 (001.099)</ta>
            <ta e="T800" id="Seg_5657" s="T794">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.100 (001.100)</ta>
            <ta e="T804" id="Seg_5658" s="T800">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.101 (001.101)</ta>
            <ta e="T808" id="Seg_5659" s="T804">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.102 (001.102)</ta>
            <ta e="T821" id="Seg_5660" s="T808">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.103 (001.103)</ta>
            <ta e="T826" id="Seg_5661" s="T821">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.104 (001.104)</ta>
            <ta e="T841" id="Seg_5662" s="T826">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.105 (001.105)</ta>
            <ta e="T851" id="Seg_5663" s="T841">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.106 (001.106)</ta>
            <ta e="T859" id="Seg_5664" s="T851">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.107 (001.107)</ta>
            <ta e="T862" id="Seg_5665" s="T859">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.108 (001.108)</ta>
            <ta e="T867" id="Seg_5666" s="T862">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.109 (001.109)</ta>
            <ta e="T872" id="Seg_5667" s="T867">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.110 (001.110)</ta>
            <ta e="T878" id="Seg_5668" s="T872">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.111 (001.111)</ta>
            <ta e="T885" id="Seg_5669" s="T878">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.112 (001.112)</ta>
            <ta e="T896" id="Seg_5670" s="T885">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.113 (001.113)</ta>
            <ta e="T900" id="Seg_5671" s="T896">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.114 (001.114)</ta>
            <ta e="T911" id="Seg_5672" s="T900">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.115 (001.115)</ta>
            <ta e="T924" id="Seg_5673" s="T911">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.116 (001.116)</ta>
            <ta e="T931" id="Seg_5674" s="T924">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.117 (001.117)</ta>
            <ta e="T938" id="Seg_5675" s="T931">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.118 (001.118)</ta>
            <ta e="T944" id="Seg_5676" s="T938">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.119 (001.119)</ta>
            <ta e="T951" id="Seg_5677" s="T944">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.120 (001.120)</ta>
            <ta e="T954" id="Seg_5678" s="T951">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.121 (001.121)</ta>
            <ta e="T966" id="Seg_5679" s="T954">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.122 (001.122)</ta>
            <ta e="T968" id="Seg_5680" s="T966">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.123 (001.123)</ta>
            <ta e="T975" id="Seg_5681" s="T968">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.124 (001.124)</ta>
            <ta e="T983" id="Seg_5682" s="T975">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.125 (001.125)</ta>
            <ta e="T987" id="Seg_5683" s="T983">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.126 (001.126)</ta>
            <ta e="T999" id="Seg_5684" s="T987">ChVD_AkEE_198204_SoldierInWar_nar.ChVD.127 (001.127)</ta>
         </annotation>
         <annotation name="st" tierref="st-ChVD">
            <ta e="T5" id="Seg_5685" s="T0">Дудинкага кэлбитим в сорок втором.</ta>
            <ta e="T14" id="Seg_5686" s="T5">Август ыйга вообще-то комиссияны проходи гынным – нестроевойга.</ta>
            <ta e="T16" id="Seg_5687" s="T14">Норильскыйга ыыппыттара.</ta>
            <ta e="T27" id="Seg_5688" s="T16">Норильскыйтан кимӈэ-э военизированный (военизированнай) охранага үлэлии түстүм, добровольссылары гытта эмиэ кат түбэспитим. </ta>
            <ta e="T33" id="Seg_5689" s="T27">Красноярскайга э-э диэри барбытым "Спартак" парахотынан.</ta>
            <ta e="T40" id="Seg_5690" s="T33">Онтон үөрэнним красноярскай пехотага с танковыми пулемётчиками.</ta>
            <ta e="T55" id="Seg_5691" s="T40">Уон биир ыйы дуу, каһы дуу үөрэнэн бараан, эмиэ каттыы (улэйдаабыттара?) кимиэкэ, Звенигуоракка – младший сержант.</ta>
            <ta e="T59" id="Seg_5692" s="T55">Парашют разбирайдааччы этим десантский.</ta>
            <ta e="T70" id="Seg_5693" s="T59">Дьэ, онно уһун да үөрэммэтэгим доготторум гытта ба каалымаарыбын бииргэ барсыбытым.</ta>
            <ta e="T80" id="Seg_5694" s="T70">Дьэ, ол кимнээтим, кимиэкэ, Мууромӈа үөрэнэ кимнээтибит запасной куо…</ta>
            <ta e="T90" id="Seg_5695" s="T80">Просто кимниир этилэрэ онно формирование оӈорор этилэрэ какие (ча-).</ta>
            <ta e="T100" id="Seg_5696" s="T90">Кем, туокка үөрэммиккин кимнээк ким кимнииллэр отто: какие части, подразделения.</ta>
            <ta e="T109" id="Seg_5697" s="T100">Дьэ, онно кимнээтим да, Мууромтан кимиэкэ түбэстибит, ол Туулага.</ta>
            <ta e="T115" id="Seg_5698" s="T109">Ол Тулага таа… разбиилься буо.</ta>
            <ta e="T127" id="Seg_5699" s="T115">Агыс километры дуу, каһы дуу баран бараан, тимир орокторуӈ барыта алдьаммытара буолла. </ta>
            <ta e="T142" id="Seg_5700" s="T127">Дьэ, онно кимнии һыттакпытына, уже налёты буо биэк, (көтө һы..) көтөөччү этилэрэ һарсиэрда аайы.</ta>
            <ta e="T152" id="Seg_5701" s="T142">Дьэ, онтон уһун да буолла, ээ, (барым..) кимниибит онно.</ta>
            <ta e="T166" id="Seg_5702" s="T152">Илэ һатыы барпыппыт ол кимиэкэ, канна (һо..) һол война буолуок һирэ буолуор диэри.</ta>
            <ta e="T174" id="Seg_5703" s="T166">Дьэ онно кимнээччилэр буо ким – партизанскый ити отряд.</ta>
            <ta e="T179" id="Seg_5704" s="T174">О кимнэ – НОП СОП обозначаайдыыллар.</ta>
            <ta e="T190" id="Seg_5705" s="T179">Ону да бу муйа (кимнии..) кимниигэ үрэк, туок эрэ үрэккээн һытар.</ta>
            <ta e="T193" id="Seg_5706" s="T190">Чэ онно деревняга. </ta>
            <ta e="T197" id="Seg_5707" s="T193">Онно оборанясса гыналлар эни.</ta>
            <ta e="T204" id="Seg_5708" s="T197">Һарсиэрда кимнээтибит, аһыы түһэн барааммыт ону.</ta>
            <ta e="T208" id="Seg_5709" s="T204">"Дьэ ытыалаһаллар! – командирбытын кэпсиир.</ta>
            <ta e="T214" id="Seg_5710" s="T208">– Кайтак (кайдак) обо-э кимниэккитин онтон…</ta>
            <ta e="T229" id="Seg_5711" s="T214">Дьэ окружение түбэстэккитинэ багар, плен түбэстэккэтин (түбэстэккитинэ) иттэ документы ким кимнээмиэккин, наада буолла, бэйэгин биэримиэккин багар..</ta>
            <ta e="T241" id="Seg_5712" s="T229">"Просто, (ги..) кайыталаамаӈ (һурук-), э-э, һуруктаргыын, – диир буолла, – арак .. кимнээӈ.</ta>
            <ta e="T251" id="Seg_5713" s="T241">(Көмү..), э, көмүӈ дуу, бэйэгит һиэӈ дуу," – диэччи бугурдук.</ta>
            <ta e="T262" id="Seg_5714" s="T251">Кайаталаатыӈ даа всё равно (килиэйди..) ити кимнииллэр буо, ол булуллар буо.</ta>
            <ta e="T268" id="Seg_5715" s="T262">Ол кэ оротагын кээһэ һүүрбүппүн биирдэ. </ta>
            <ta e="T278" id="Seg_5716" s="T268">(Канн..) каннуктарын (канныктарын) да дьүүллээммин караӈага: неместэрэ да, кимнэрэ да?</ta>
            <ta e="T288" id="Seg_5717" s="T278">Дьэ ол мин һылдьаммын көрдүм, биэс киһи һылдьаллар арай бугурдук.</ta>
            <ta e="T296" id="Seg_5718" s="T288">Дьэ онтон ол диэгиттэн немсыларбыт кэлэллэр, траншейтан тураллар.</ta>
            <ta e="T300" id="Seg_5719" s="T296">"Төттөрү күрүөгүӈ", – дии-дии кимнииллэр.</ta>
            <ta e="T305" id="Seg_5720" s="T300">Ол киһиӈ, командирдардаактар ол доготторо.</ta>
            <ta e="T309" id="Seg_5721" s="T305">Атыттар онто, атын чаaстан.</ta>
            <ta e="T321" id="Seg_5722" s="T309">Бэйэм (һо..) доготторбун ол кээһэ һүүрбүппүн, эни киэ дуу, туок дуу.</ta>
            <ta e="T323" id="Seg_5723" s="T321">"Оступайдааӈ! – диир.</ta>
            <ta e="T328" id="Seg_5724" s="T323">– Только не заметно," – диир командирдара.</ta>
            <ta e="T335" id="Seg_5725" s="T328">Онтон көрдүм до, немсыларыӈ һубу кэлэн иһэллэр.</ta>
            <ta e="T345" id="Seg_5726" s="T335">Туугу, туугу кадьыгынаһаллар, сэпсэтэллэр, төттөрү һүүрэбин доготторум барбыт һирдэрин диэк.</ta>
            <ta e="T357" id="Seg_5727" s="T345">Буопса һурдургаһаллар агай итиннэ, окко, ытыалыллар, илэ кимнээри гыналлар быһылаак отто…</ta>
            <ta e="T362" id="Seg_5728" s="T357">Плен ылаары, уһугуттан траншейга кэллим.</ta>
            <ta e="T368" id="Seg_5729" s="T362">Кэлбитим, бэйэм (догот..), э, ротабар түбэспиппин.</ta>
            <ta e="T377" id="Seg_5730" s="T368">Дьэ ол кимниибин, кайа, камнем (сүһэр?) – пулемётынан кимниэ сразу.</ta>
            <ta e="T381" id="Seg_5731" s="T377">Һүүрбэттэн такса киһини түһэрдилэр.</ta>
            <ta e="T387" id="Seg_5732" s="T381">Һорокторбут төттөрү һүүрдүлэр ол, дозор буолааччы.</ta>
            <ta e="T401" id="Seg_5733" s="T387">(Оро..) оротаӈ (ротаӈ) барыага, өссүө ким впереди һылдьыага разведчик көрдүк оротагын агай кэтиигин буо.</ta>
            <ta e="T412" id="Seg_5734" s="T401">Инникиилэр икки киһи буолар, каӈас диэк, уӈа диэк эмиэ иккилии киһи.</ta>
            <ta e="T416" id="Seg_5735" s="T412">"Чэ, кайтиэк (кайдак) буолуокпутуй?" – диир.</ta>
            <ta e="T424" id="Seg_5736" s="T416">Бу деревняга проверяйдыыбыт, илэ кимнэ.. ким да һуок.</ta>
            <ta e="T430" id="Seg_5737" s="T424">(Немсы), ээ, немсалар оӈуор эрэ көстөллөр.</ta>
            <ta e="T434" id="Seg_5738" s="T430">"Төттөрү барыагыӈ," – дии-дии гынар.</ta>
            <ta e="T446" id="Seg_5739" s="T434">Чэ, ол кимнии һылдьаммыт уолдьастыбыт, (ол) ол үрэкпитин таӈнары бардыбыт, һүрүк таӈнары.</ta>
            <ta e="T451" id="Seg_5740" s="T446">Үрэктэнэн э бараммыт, э-э, кимниибит.</ta>
            <ta e="T467" id="Seg_5741" s="T451">Бугурдук кимнэрэ кимнээбиттэр бу артиллерия буолааччы буо: пуускалара (таа…) э кимнэрэ, аттар, – барыта кимнэллибитэр бугурдук.</ta>
            <ta e="T471" id="Seg_5742" s="T467">Переворочи кимнээбиттэр онто өлөртөөбүттэр.</ta>
            <ta e="T475" id="Seg_5743" s="T471">Онно биир киһини буллубут.</ta>
            <ta e="T482" id="Seg_5744" s="T475">Полуживой илэ, кайдиэк эрэ, арыччы һаӈарар онтон.</ta>
            <ta e="T494" id="Seg_5745" s="T482">"Бу һаӈардыы табак тардар ыксата буолла, – дии-дии гынар, – биһиги ротабытын кимнээбиттэрэ, – диир.</ta>
            <ta e="T499" id="Seg_5746" s="T494">– Немситэ, һэ-э, барбыттара анаккаан (аныкаан), – диир.</ta>
            <ta e="T503" id="Seg_5747" s="T499">– Һаӈарсымаӈ тыыӈнык," – дии-дии гынар.</ta>
            <ta e="T507" id="Seg_5748" s="T503">Дьэ, ол кимнээтибит… </ta>
            <ta e="T519" id="Seg_5749" s="T507">Дьэ онно дии э-э (дого…) икки киһини ыыталлар, онно ким буолаaччы – санчас.</ta>
            <ta e="T525" id="Seg_5750" s="T519">Онто "иллиэкпит, – дииллэр, – (мин) биһиги энигин."</ta>
            <ta e="T544" id="Seg_5751" s="T525">"(Ту..) минигин туок кыһаллымаӈ, – диир, – мин э һин (киһи буолуок..), э, киһи буолбатакпын, – диир буо – (өлүө..) өлүөм аны."</ta>
            <ta e="T550" id="Seg_5752" s="T544">Киһилэр һолбок көрдүк һыталлар илэ, олортон.</ta>
            <ta e="T553" id="Seg_5753" s="T550">Һонон ол кимнээтилэр.</ta>
            <ta e="T573" id="Seg_5754" s="T553">Онтугун илтилэр икки киһи кимиэкэ, онтуӈ отто диэбит үһү, ол всё равно тириэрэмэ э кимнээн ол, онтон төттөрү кэллилэр олоруӈ.</ta>
            <ta e="T585" id="Seg_5755" s="T573">"Һин киһи буолуом һуога, тугу минигин гытта (возиз..) кимнээгит," – диир үһү.</ta>
            <ta e="T589" id="Seg_5756" s="T585">Һарсиэрда ииммит һаӈардыы таксар.</ta>
            <ta e="T599" id="Seg_5757" s="T589">Таксан (эрдэ..) э-э кэмнээн эрдэгинэ, туок эрэ лейтенааны көрүстүбүт.</ta>
            <ta e="T601" id="Seg_5758" s="T599">Онтубут диир:</ta>
            <ta e="T603" id="Seg_5759" s="T601">"Каннык чаaстаӈӈыный?"</ta>
            <ta e="T606" id="Seg_5760" s="T603">"Эһиги, эһигиттэн," – диибит.</ta>
            <ta e="T610" id="Seg_5761" s="T606">Диир онтон биирбин онтон. </ta>
            <ta e="T615" id="Seg_5762" s="T610">"Оччого минигин гытта һүүрүӈ," – диир.</ta>
            <ta e="T622" id="Seg_5763" s="T615">Инни диэк ходили эрэ кимниэй киэ ол.</ta>
            <ta e="T626" id="Seg_5764" s="T622">Связь ыла кимниир эни.</ta>
            <ta e="T631" id="Seg_5765" s="T626">"Дьэ онтон ол кимнии һатыыбыт."</ta>
            <ta e="T638" id="Seg_5766" s="T631">Кас да күнү ол һырыттыбыт ол курдук.</ta>
            <ta e="T641" id="Seg_5767" s="T638">Ротабытын даа булбаппыт.</ta>
            <ta e="T649" id="Seg_5768" s="T641">Дьэ, онтон койуккутун (кимнээтэ…) э-э буллубут эмиэ э.</ta>
            <ta e="T660" id="Seg_5769" s="T649">(Ү..)Үһүс күммүтүгэр дуу, каһыспытыгар дуу ынтак барабыт, онно кимиэкэ.</ta>
            <ta e="T676" id="Seg_5770" s="T660">Ол үрэкпитин таксабыт, дьэ онтон ол уолдьаһабыт, ол ынарааттан ытыалыыллар, а биһиги бэтэрэттэн эмиэ ытыалаһабыт буолла.</ta>
            <ta e="T686" id="Seg_5771" s="T676">Дьэ ол кимнээтибит даа онно, кайдиэк даа кимнээбэттэр, ыыппаттар ол.</ta>
            <ta e="T697" id="Seg_5772" s="T686">Һарсиэрда кыаттаакка дииллэр ол диэк иннибититтэн, бу (би..) команда биэрэллэр:</ta>
            <ta e="T704" id="Seg_5773" s="T697">"Ну, отто үс чааска Брянскайы занимайдыаккытын наада".</ta>
            <ta e="T712" id="Seg_5774" s="T704">Дьэ онно бугурдук туок эрэ һыыр буолар – (дьэрбии?).</ta>
            <ta e="T719" id="Seg_5775" s="T712">(Һу..) (дьэрбини?) э кимнээтибит да эмиэ.</ta>
            <ta e="T735" id="Seg_5776" s="T719">Эмиэ һырайа… (һуо..) инниттэн кимниилэр буо уһугут.. неместэр бугурдук һыталлар: оборонятса гына һыталлар буо.</ta>
            <ta e="T741" id="Seg_5777" s="T735">А биһиги бу дьиэгиттэн эмиэ ытыалаһабыт.</ta>
            <ta e="T749" id="Seg_5778" s="T741">Дьэ онтон бу түүнү һупту һытыы мунна буо.</ta>
            <ta e="T762" id="Seg_5779" s="T749">Түүнү уоннар дуу иккилэр диэк дуу, или чаастар диэк дуу ол ракеты ыыталлар.</ta>
            <ta e="T772" id="Seg_5780" s="T762">Ракеты ыттагына оччого бу барыта оборонясса, кимнии һыталлар барыта көстөллөр.</ta>
            <ta e="T777" id="Seg_5781" s="T772">Илиибин таптарарбын дьүүлээбэтим онтон.</ta>
            <ta e="T784" id="Seg_5782" s="T777">Ол мунтубар (тап-) кимнээбит ол һупту көтөн.</ta>
            <ta e="T794" id="Seg_5783" s="T784">Биир мина кэллэ да барыбытын һолообут ол – уон ордук киһини.</ta>
            <ta e="T800" id="Seg_5784" s="T794">Һаабын буртук (бугурдук) тутарбар өйдөннүм онтон иннэ.</ta>
            <ta e="T804" id="Seg_5785" s="T800">Инньэ турарбына туок, тобуктуу.</ta>
            <ta e="T808" id="Seg_5786" s="T804">Дьэ, онтон түстүм бугурдук.</ta>
            <ta e="T821" id="Seg_5787" s="T808">Дьэ онтон койут көрдүм буо, чөмүйэлэрим барыта һалыбырыы һылдьаллар, кааным уста һытар бугурдук.</ta>
            <ta e="T826" id="Seg_5788" s="T821">Дьэ онтон курбун уһуллум бугурдук.</ta>
            <ta e="T841" id="Seg_5789" s="T826">Курбун уһулан барааммын, буртук (бугурдук) кимнээтим, аньыы, киммэр муойбар иннэ (кэтт..) кимнээтим бу баайан барааммын.</ta>
            <ta e="T851" id="Seg_5790" s="T841">Онтон бу пакеттар буолааччылар каждый перевязочный, дьэ онтубунан эрийдим бугурдук.</ta>
            <ta e="T859" id="Seg_5791" s="T851">Онтон оммуотка ити ботинкагар ким буолааччы (эри…).</ta>
            <ta e="T862" id="Seg_5792" s="T859">Ону оммоткабынан эрийдим.</ta>
            <ta e="T867" id="Seg_5793" s="T862">"Бар, – дииллэр, – ким командир ротагар".</ta>
            <ta e="T872" id="Seg_5794" s="T867">Һол үнэн үүммүтүнэн тийдим онтубар.</ta>
            <ta e="T878" id="Seg_5795" s="T872">Килэп көрдөөн ээ (ки..) ас.</ta>
            <ta e="T885" id="Seg_5796" s="T878">"Кайа – диир, – рании гыннаппыккын дуу" – диир.</ta>
            <ta e="T896" id="Seg_5797" s="T885">"Һм-м," – диибин, һаабын онтон ылбыт этэ ол кимим, командирбар сдавайдыбын буо.</ta>
            <ta e="T900" id="Seg_5798" s="T896">Кимӈэ, номерынан буолаaччы этилэрэ.</ta>
            <ta e="T911" id="Seg_5799" s="T900">Онтум буо ким патруоттар кармааммар, туок даа һуок: гранааттарбын эмиэ ылбыттар.</ta>
            <ta e="T924" id="Seg_5800" s="T911">Окуопага һыттакпына, кумак түһээктиир диин ол диэгиттэн кэлэ, итэ мииналар түстэктэринэ, һирим доргуйар.</ta>
            <ta e="T931" id="Seg_5801" s="T924">Шырк-шыр-шырк кимнииллэр арай.</ta>
            <ta e="T938" id="Seg_5802" s="T931">Онтум ужэ кардары (за…) заполнение кэлэр эбит. </ta>
            <ta e="T944" id="Seg_5803" s="T938">Биһиги оннубутугар өсүө нөӈүөлэр кэлэллэр, буолар. </ta>
            <ta e="T951" id="Seg_5804" s="T944">Аайтаан (аайдаан) буолаактыр диин онтон, ытаһаллар, буолаллар арай. </ta>
            <ta e="T954" id="Seg_5805" s="T951">Һорок-һорок ынчыктыыр буолар.</ta>
            <ta e="T966" id="Seg_5806" s="T954">Ынчыктыыр буоллактарына, мин эмиэ эмиэ ынчыктыыбын эмиэ онтон эмиэ дьылыс гынабын, иһиллиибин.</ta>
            <ta e="T968" id="Seg_5807" s="T966">Санчастька кэлбитим.</ta>
            <ta e="T975" id="Seg_5808" s="T968">(Сан..) полевой үлэ ким буолаaччы, санчас.</ta>
            <ta e="T983" id="Seg_5809" s="T975">О-ол санчаска (кэлли..) э-э кэллим да тылга ыыппыттара.</ta>
            <ta e="T987" id="Seg_5810" s="T983">Иваново Шуйя һыппытым – эргэ һоспитальга.</ta>
            <ta e="T999" id="Seg_5811" s="T987">Онтон (сра..) э-э сразу биэс ыйы һыттым да, дьиэм диэк ыыппыттара.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-ChVD">
            <ta e="T5" id="Seg_5812" s="T0">Dudʼinkaga kelbitim v sorаk vtаrom. </ta>
            <ta e="T14" id="Seg_5813" s="T5">Avgust ɨjga vaabšʼe-ta kamʼisʼsʼijanɨ praxadiː gɨnnɨm – nʼestrajevojga. </ta>
            <ta e="T16" id="Seg_5814" s="T14">Narilʼskɨjga ɨːppɨttara. </ta>
            <ta e="T27" id="Seg_5815" s="T16">Narilʼskɨjtan kimŋe eː vajenʼizʼirovannɨj axranaga üleliː tüstüm, dabravolsɨlarɨ gɨtta emi͡e ka tübespitim. </ta>
            <ta e="T33" id="Seg_5816" s="T27">Krasnajarskajga eː di͡eri barbɨtɨm "Spartak" paraxotɨnan. </ta>
            <ta e="T40" id="Seg_5817" s="T33">Onton ü͡örennim Krasnajarskaj pʼexoːtaga s tankаvɨmi pulʼemʼočʼčʼikamʼi. </ta>
            <ta e="T55" id="Seg_5818" s="T40">U͡on biːr ɨjɨ duː, kahɨ duː ü͡örenen baraːn, emi͡e kattɨː ((…)) kimi͡eke, Zvʼenʼigu͡orakka – mladšij sʼeržant. </ta>
            <ta e="T59" id="Seg_5819" s="T55">Parasut razbirajdaːččɨ etim, desanskʼij. </ta>
            <ta e="T70" id="Seg_5820" s="T59">Dʼe, onno uhun da ü͡öremmetegim dogottorum gɨtta ba kaːlɨmaːrɨbɨn biːrge barsɨbɨtɨm. </ta>
            <ta e="T80" id="Seg_5821" s="T70">Dʼe, ol kimneːtim, kimi͡eke, Muːromŋa ü͡örene kimneːtibit zapasnoj (ku͡o -). </ta>
            <ta e="T90" id="Seg_5822" s="T80">Prostа kimniːr etilere onno farmʼiravanʼije oŋoror etilere kakʼije (čʼa -). </ta>
            <ta e="T100" id="Seg_5823" s="T90">Kʼem, tu͡okka ü͡öremmikkin kimneːk kim kimniːller otto, kakʼije čʼastʼi, pаdrazdʼelʼenʼija. </ta>
            <ta e="T109" id="Seg_5824" s="T100">Dʼe, onno kimneːtim da, Muːromtan kimi͡eke tübestibit, ol Tuːlaga. </ta>
            <ta e="T115" id="Seg_5825" s="T109">Ol Tula (taː -) razbʼilsʼa bu͡o. </ta>
            <ta e="T127" id="Seg_5826" s="T115">Agɨs kilametrɨ duː, kahɨ duː baran baraːn, timir oroktoruŋ barɨta aldʼammɨttara bu͡olla. </ta>
            <ta e="T142" id="Seg_5827" s="T127">Dʼe, onno kimniː hɨttakpɨtɨna, uže nalʼotɨ bu͡o bi͡ek, (kötö hɨ-) kötöːččü etilere harsi͡erda aːjɨ. </ta>
            <ta e="T152" id="Seg_5828" s="T142">Dʼe, onton uhun da bu͡olla, eː, (barɨm-) kimniːbit onno. </ta>
            <ta e="T166" id="Seg_5829" s="T152">Ile hatɨː barbɨppɨt ol kimi͡eke, kanna (ho-) hol vajna bu͡olu͡ok hire bu͡olu͡or di͡eri. </ta>
            <ta e="T174" id="Seg_5830" s="T166">Dʼe onno kimneːččiler bu͡o kim – partʼizanskɨj iti аtrʼad. </ta>
            <ta e="T179" id="Seg_5831" s="T174">O kimŋe – NOP SOP abaznačajdɨːllar. </ta>
            <ta e="T190" id="Seg_5832" s="T179">Onu da bu muja (kimniː-) kimniːrge ürek, tu͡ok ere ürekkeːn hɨtar. </ta>
            <ta e="T193" id="Seg_5833" s="T190">Če onno dʼerʼevnʼaga. </ta>
            <ta e="T197" id="Seg_5834" s="T193">Onno аbаranʼassa gɨnallar eni. </ta>
            <ta e="T204" id="Seg_5835" s="T197">Harsi͡erda kimneːtibit, ahɨː ahɨː tühen baraːmmɨt onu. </ta>
            <ta e="T208" id="Seg_5836" s="T204">"Dʼe ɨtɨ͡alahallar", kamandʼirbɨtɨn kepsiːr. </ta>
            <ta e="T214" id="Seg_5837" s="T208">"Kajtak (аboː -) kimni͡ekkitin onton… </ta>
            <ta e="T229" id="Seg_5838" s="T214">Dʼe аkruzeːnije tübestekkitine bagar, plʼen tübestekketin itte dаkumʼentɨ kim kimneːmi͡ekkin, naːda bu͡olla, bejegin bi͡erimi͡ekkin bagar. </ta>
            <ta e="T241" id="Seg_5839" s="T229">Prostа, (gi-) kajɨtalaːmaŋ (huruk-), eː, huruktargɨt", diːr bu͡olla, "(arak-) kimneːŋ. </ta>
            <ta e="T251" id="Seg_5840" s="T241">(Kömü-), e, kömüŋ duː, bejegit hi͡eŋ duː", di͡ečči bugurduk. </ta>
            <ta e="T262" id="Seg_5841" s="T251">"Kajatalaːtɨŋ daː vsʼo ravno (kili͡ejd-) iti kimniːller bu͡o, ol bulallar bu͡o." </ta>
            <ta e="T268" id="Seg_5842" s="T262">Ol ke orotaŋ keːhe hüːrbüppün biːrde. </ta>
            <ta e="T278" id="Seg_5843" s="T268">(Kann-) kannuktarɨn da dʼüːlleːmmin karaŋaga, nʼemʼestere da, kimnere da? </ta>
            <ta e="T288" id="Seg_5844" s="T278">Dʼe ol min hɨldʼammɨn kördüm, bi͡es kihi hɨldʼallar araj bugurduk. </ta>
            <ta e="T296" id="Seg_5845" s="T288">Dʼe onton ol di͡egitten nʼemsɨlarbɨt keleller, transejtan turallar. </ta>
            <ta e="T300" id="Seg_5846" s="T296">"Töttörü kürü͡ögüŋ", diː-diː kimniːller. </ta>
            <ta e="T305" id="Seg_5847" s="T300">Ol kihiŋ, kamandʼirdardaːktar ol dogottoro. </ta>
            <ta e="T309" id="Seg_5848" s="T305">Atɨttar onto, atɨn čʼaːstan. </ta>
            <ta e="T321" id="Seg_5849" s="T309">Bejem (ho-) dogottorbun ol keːhe hüːrbüppün, eni ki͡e duː, tu͡ok duː. </ta>
            <ta e="T323" id="Seg_5850" s="T321">"Astupajdaːŋ", diːr. </ta>
            <ta e="T328" id="Seg_5851" s="T323">"Tolʼkа nʼe zamʼetnа", diːr kamandʼirdara. </ta>
            <ta e="T335" id="Seg_5852" s="T328">Onton kördüm do, nʼemsɨlarɨŋ hubu kelen iheller. </ta>
            <ta e="T345" id="Seg_5853" s="T335">Tuːgu, tuːgu kadʼɨgɨnahallar, sepseteller, töttörü hüːrebin dogottorum barbɨt hirderin di͡ek. </ta>
            <ta e="T357" id="Seg_5854" s="T345">Bu͡opsa hurdurgahallar agaj itinne, okko, ɨtɨ͡alɨːllar, ile kimneːri gɨnallar bɨhɨlaːk otto… </ta>
            <ta e="T362" id="Seg_5855" s="T357">Plʼen ɨlaːrɨ, uhuguttan transejga kellim. </ta>
            <ta e="T368" id="Seg_5856" s="T362">Kelbitim, bejem (dogot-), e, rotabar tübespippin. </ta>
            <ta e="T377" id="Seg_5857" s="T368">Dʼe ol kimniːbin, kaja, kamnʼem ((…)) – pulemʼotɨnan kimni͡e sraːzu. </ta>
            <ta e="T381" id="Seg_5858" s="T377">Hüːrbetten taksa kihini tüherdiler. </ta>
            <ta e="T387" id="Seg_5859" s="T381">Horoktorbut töttörü hüːrdüler ol, dаzor bu͡olaːččɨ. </ta>
            <ta e="T401" id="Seg_5860" s="T387">(Oro-) orotaŋ barɨ͡aga, össü͡ö kim vpʼerʼedʼi hɨldʼɨ͡aga razvʼečʼčʼik kördük orotagɨn agaj ketiːgin bu͡o. </ta>
            <ta e="T412" id="Seg_5861" s="T401">Innikiler ikki kihi bu͡olar, kaŋas di͡ek, uŋa di͡ek emi͡e ikkiliː kihi. </ta>
            <ta e="T416" id="Seg_5862" s="T412">"Če, kajti͡ek bu͡olu͡okputuj", diːr. </ta>
            <ta e="T424" id="Seg_5863" s="T416">Bu dʼerʼevnʼaga pravʼerʼajdɨːbɨt, ile (kimneː-) kim da hu͡ok. </ta>
            <ta e="T430" id="Seg_5864" s="T424">Nʼemsɨ, eː, nʼemsalar oŋu͡or ere köstöllör. </ta>
            <ta e="T434" id="Seg_5865" s="T430">"Töttörü barɨ͡agɨŋ", diː-diː gɨnar. </ta>
            <ta e="T446" id="Seg_5866" s="T434">Če, ol kimniː hɨldʼammɨt u͡oldʼastɨbɨt, (ol) ol ürekpitin taŋnarɨ bardɨbɨt, hüːrük taŋnarɨ. </ta>
            <ta e="T451" id="Seg_5867" s="T446">Ürektenen e barammɨt, eː, kimniːbit. </ta>
            <ta e="T467" id="Seg_5868" s="T451">Bugurduk kimnere kimneːbitter bu artʼilʼlʼerʼija bu͡olaːččɨ bu͡o, puːskalara (taː -) e kimnere, attar – barɨta kimnellibitter bugurduk. </ta>
            <ta e="T471" id="Seg_5869" s="T467">Perevaročɨː kimneːbitter onto ölörtöːbütter. </ta>
            <ta e="T475" id="Seg_5870" s="T471">Onno biːr kihini bullubut. </ta>
            <ta e="T482" id="Seg_5871" s="T475">Pаluzivoj ile, kajdi͡ek ere, arɨččɨ haŋarar onton. </ta>
            <ta e="T494" id="Seg_5872" s="T482">"Bu haŋardɨː tabak tardar ɨksata bu͡olla", diː-diː gɨnar, "bihigi rotabɨtɨn kimneːbittere", diːr. </ta>
            <ta e="T499" id="Seg_5873" s="T494">"Nʼemsite, heː, barbɨttara anakkaːn", diːr. </ta>
            <ta e="T503" id="Seg_5874" s="T499">"Haŋarsɨmaŋ tɨŋnɨk", diː-diː gɨnar. </ta>
            <ta e="T507" id="Seg_5875" s="T503">Dʼe, ol kimneːtibit… </ta>
            <ta e="T519" id="Seg_5876" s="T507">Dʼe onno diː eː (dogo-) ikki kihini ɨːtallar, onno kim bu͡olaːččɨ – sančʼas. </ta>
            <ta e="T525" id="Seg_5877" s="T519">Onto "illi͡ekpit" diːller, "(min) bihigi enigin." </ta>
            <ta e="T544" id="Seg_5878" s="T525">"(Tu-) minigin tu͡ok kɨhallɨmaŋ", diːr, "min e hin (kihi bu͡olu͡ok-), e, kihi bu͡olbatakpɨn", diːr bu͡o, "(ölü͡ö-) ölü͡öm anɨ." </ta>
            <ta e="T550" id="Seg_5879" s="T544">Kihiler holbok kördük hɨtallar ile, olorton. </ta>
            <ta e="T553" id="Seg_5880" s="T550">Honon ol kimneːtiler. </ta>
            <ta e="T573" id="Seg_5881" s="T553">Ontugun iltiler ikki kihi kimi͡eke, ontuŋ otto di͡ebit ühü, "ol vsʼo ravno tiri͡ereme e kimneːn ol", onton töttörü kelliler oloruŋ. </ta>
            <ta e="T585" id="Seg_5882" s="T573">"Hin kihi bu͡olu͡om hu͡oga, tugu minigin gɨtta (vаzʼiz -) kimneːgit", diːr ühü. </ta>
            <ta e="T589" id="Seg_5883" s="T585">Harsi͡erda immit haŋardɨː taksar. </ta>
            <ta e="T599" id="Seg_5884" s="T589">Taksan (erde -) eː kimneːn erdegine, tu͡ok ere lʼejtʼenaːnɨ körüstübüt. </ta>
            <ta e="T601" id="Seg_5885" s="T599">Ontubut diːr:</ta>
            <ta e="T603" id="Seg_5886" s="T601">"Kannɨk čʼaːstaŋŋɨnɨj?" </ta>
            <ta e="T606" id="Seg_5887" s="T603">"Ehigi, ehigitten", diːbit. </ta>
            <ta e="T610" id="Seg_5888" s="T606">Diːr onton biːrbit onton. </ta>
            <ta e="T615" id="Seg_5889" s="T610">"Oččogo minigin gɨtta hüːrüŋ", diːr. </ta>
            <ta e="T622" id="Seg_5890" s="T615">Inni di͡ek xаdʼilʼi ere kimneːmi͡ekke. </ta>
            <ta e="T626" id="Seg_5891" s="T622">Svʼaz ɨla kimniːr eni. </ta>
            <ta e="T631" id="Seg_5892" s="T626">"Dʼe onton ol kimniː hatɨːbɨt." </ta>
            <ta e="T638" id="Seg_5893" s="T631">Kas da künü ol hɨrɨttɨbɨt ol kurduk. </ta>
            <ta e="T641" id="Seg_5894" s="T638">Rotabɨtɨn daː bulbappɨt. </ta>
            <ta e="T649" id="Seg_5895" s="T641">Dʼe, onton kojukkutun (kimneːti-) eː bullubut emi͡e e. </ta>
            <ta e="T660" id="Seg_5896" s="T649">(Ü-) ühüs kümmütüger duː, kahɨspɨtɨgar duː ɨntak barabɨt, onno kimi͡eke. </ta>
            <ta e="T676" id="Seg_5897" s="T660">Ol ürekpitin taksabɨt, dʼe onton ol u͡oldʼahabɨt, ol ɨnaraːttan ɨtɨ͡alɨːllar, a bihigi betereːtten emi͡e ɨtɨ͡alahabɨt bu͡olla. </ta>
            <ta e="T686" id="Seg_5898" s="T676">Dʼe ol kimneːtibit daː onno, kajdi͡ek daː kimneːbetter, ɨːppattar ol. </ta>
            <ta e="T697" id="Seg_5899" s="T686">Harsi͡erda kɨ͡attaːk kelliler ol di͡ek innibititten, bu (bi -) kamanda bi͡ereller: </ta>
            <ta e="T704" id="Seg_5900" s="T697">"Nu, otto üs čʼaːska Brʼanskajɨ zanʼimajdɨ͡akkɨtɨn naːda." </ta>
            <ta e="T712" id="Seg_5901" s="T704">Dʼe onno bugurduk tu͡ok ere hɨːr bu͡olar – dʼerbi. </ta>
            <ta e="T719" id="Seg_5902" s="T712">(Hu -) dʼerbini e kimneːtibit da emi͡e. </ta>
            <ta e="T735" id="Seg_5903" s="T719">Emi͡e hɨraja, (hu͡o-) innitten kimniːller bu͡o (uhugut-) nʼemʼester bugurduk hɨtallar, аbаrаnʼatsa gɨna hɨtallar bu͡o. </ta>
            <ta e="T741" id="Seg_5904" s="T735">A bihigi bu dʼi͡egitten emi͡e ɨtɨ͡alahabɨt. </ta>
            <ta e="T749" id="Seg_5905" s="T741">Dʼe onton bu tüːnü huptu hɨtɨː munna bu͡o. </ta>
            <ta e="T762" id="Seg_5906" s="T749">Tüːnü u͡onnar duː ikkiler di͡ek duː, ili čʼaːstar di͡ek duː ol rakʼetɨ ɨːtallar. </ta>
            <ta e="T772" id="Seg_5907" s="T762">Rakʼetɨ ɨːttagɨna oččogo bu barɨta аbаrаnʼassa, kimniː hɨtallar barɨta köstöllör. </ta>
            <ta e="T777" id="Seg_5908" s="T772">Iliːbin taptaran bu dʼüːleːbetim onton. </ta>
            <ta e="T784" id="Seg_5909" s="T777">Ol muntubar (tap-) kimneːbit ol huptu kötön. </ta>
            <ta e="T794" id="Seg_5910" s="T784">Biːr mina kelle da barɨbɨtɨn holoːbut ol – u͡on orduk kihini. </ta>
            <ta e="T800" id="Seg_5911" s="T794">Haːbɨn burtuk tutarbar öjdönnüm onton innʼe. </ta>
            <ta e="T804" id="Seg_5912" s="T800">Innʼe turabɨn tu͡ok, tobuktuː. </ta>
            <ta e="T808" id="Seg_5913" s="T804">Dʼe, onton tüstüm bugurduk. </ta>
            <ta e="T821" id="Seg_5914" s="T808">Dʼe onton kojut kördüm bu͡o, čömüjelerim barɨta halɨbɨrɨː hɨldʼallar, kaːnɨm usta hɨtar bugurduk. </ta>
            <ta e="T826" id="Seg_5915" s="T821">Dʼe onton kurbun uhullum bugurduk. </ta>
            <ta e="T841" id="Seg_5916" s="T826">Kurbun uhulan baraːmmɨn, burtuk kimneːtim, enʼi kimmer, mu͡ojbar innʼe (kett -) kimneːtim bu baːjan baraːmmɨn. </ta>
            <ta e="T851" id="Seg_5917" s="T841">Onton bu pakʼettar bu͡olaːččɨlar každɨj pʼerʼevʼazаčʼnɨj, dʼe ontubunan erijdim bugurduk. </ta>
            <ta e="T859" id="Seg_5918" s="T851">Onton ommu͡otka iti bočinkagar kim bu͡olaːččɨ (eri-). </ta>
            <ta e="T862" id="Seg_5919" s="T859">Onu ommotkabɨnan erijdim. </ta>
            <ta e="T867" id="Seg_5920" s="T862">"Bar", diːller, "kim kamandʼir rotagar." </ta>
            <ta e="T872" id="Seg_5921" s="T867">Hol ünen ümmütünen tijdim ontubar. </ta>
            <ta e="T878" id="Seg_5922" s="T872">Kilep kördöːn eː (ki -) as. </ta>
            <ta e="T885" id="Seg_5923" s="T878">"Kaja", diːr, "raniː (gɨn-) ((…)) duː", diːr. </ta>
            <ta e="T896" id="Seg_5924" s="T885">"Hmm", diːbin, haːbɨn onton ɨlbɨt ete ol kimim, kamandʼirbar sdavajdɨːbɨn bu͡o. </ta>
            <ta e="T900" id="Seg_5925" s="T896">Kimŋe nomʼerɨnan bu͡olaːččɨ etilere. </ta>
            <ta e="T911" id="Seg_5926" s="T900">Ontum bu͡o kim patru͡ottar karmaːmmar, tu͡ok daː hu͡ok, granaːttarbɨn emi͡e ɨlbɨttar. </ta>
            <ta e="T924" id="Seg_5927" s="T911">Oku͡opaga hɨttakpɨna, kumak tüheːktiːr diːn ol di͡egitten kele, ite miːnalar tüstekterine, hirim dorgujar. </ta>
            <ta e="T931" id="Seg_5928" s="T924">Šɨrk-šɨrk-šɨrk kimniːller araj. </ta>
            <ta e="T938" id="Seg_5929" s="T931">Ontum bu karda ((…)) zapаlnʼenʼije keler ebit. </ta>
            <ta e="T944" id="Seg_5930" s="T938">Bihigi onnubutugar össü͡ö nöŋü͡öler keleller, bu͡olar. </ta>
            <ta e="T951" id="Seg_5931" s="T944">Aːjtaːn bu͡olaːktɨːr diːn onton, ɨtahallar, bu͡olallar araj. </ta>
            <ta e="T954" id="Seg_5932" s="T951">Horok-horok ɨnčɨktɨːr bu͡olar. </ta>
            <ta e="T966" id="Seg_5933" s="T954">ɨnčɨktɨːr bu͡ollaktarɨna, min emi͡e emi͡e ɨnčɨktɨːbɨn emi͡e onton emi͡e dʼɨlɨs gɨnabɨn, ihilliːbin. </ta>
            <ta e="T968" id="Seg_5934" s="T966">((DMG)) Sančʼaska kelbitim. </ta>
            <ta e="T975" id="Seg_5935" s="T968">(San-) pаlʼevoj üle kim bu͡olaːččɨ, sančʼas. </ta>
            <ta e="T983" id="Seg_5936" s="T975">Oːl sančʼaska (kelli-) eː kellim da tɨlga ɨːppɨttara. </ta>
            <ta e="T987" id="Seg_5937" s="T983">Ivanava Suja hɨppɨtɨm – erge hospitalʼga. </ta>
            <ta e="T999" id="Seg_5938" s="T987">Onton (sra -) eː srazu bi͡es ɨjɨ hɨttɨm da, dʼi͡em di͡ek ɨːppɨttara. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-ChVD">
            <ta e="T1" id="Seg_5939" s="T0">Dudʼinka-ga</ta>
            <ta e="T2" id="Seg_5940" s="T1">kel-bit-i-m</ta>
            <ta e="T6" id="Seg_5941" s="T5">avgust</ta>
            <ta e="T7" id="Seg_5942" s="T6">ɨj-ga</ta>
            <ta e="T11" id="Seg_5943" s="T10">kamʼisʼsʼija-nɨ</ta>
            <ta e="T12" id="Seg_5944" s="T11">praxad-iː</ta>
            <ta e="T13" id="Seg_5945" s="T12">gɨn-nɨ-m</ta>
            <ta e="T14" id="Seg_5946" s="T13">nʼestrajevoj-ga</ta>
            <ta e="T15" id="Seg_5947" s="T14">Narilʼskɨj-ga</ta>
            <ta e="T16" id="Seg_5948" s="T15">ɨːp-pɨt-tara</ta>
            <ta e="T17" id="Seg_5949" s="T16">Narilʼskɨj-tan</ta>
            <ta e="T18" id="Seg_5950" s="T17">kim-ŋe</ta>
            <ta e="T19" id="Seg_5951" s="T18">eː</ta>
            <ta e="T9" id="Seg_5952" s="T19">vajenʼizʼirovannɨj </ta>
            <ta e="T20" id="Seg_5953" s="T9">axrana-ga</ta>
            <ta e="T21" id="Seg_5954" s="T20">ülel-iː</ta>
            <ta e="T22" id="Seg_5955" s="T21">tüs-tü-m</ta>
            <ta e="T23" id="Seg_5956" s="T22">dabravols-ɨ-lar-ɨ</ta>
            <ta e="T24" id="Seg_5957" s="T23">gɨtta</ta>
            <ta e="T25" id="Seg_5958" s="T24">emi͡e</ta>
            <ta e="T26" id="Seg_5959" s="T25">ka</ta>
            <ta e="T27" id="Seg_5960" s="T26">tübes-pit-i-m</ta>
            <ta e="T28" id="Seg_5961" s="T27">Krasnajarskaj-ga</ta>
            <ta e="T29" id="Seg_5962" s="T28">eː</ta>
            <ta e="T30" id="Seg_5963" s="T29">di͡eri</ta>
            <ta e="T31" id="Seg_5964" s="T30">bar-bɨt-ɨ-m</ta>
            <ta e="T32" id="Seg_5965" s="T31">Spartak</ta>
            <ta e="T33" id="Seg_5966" s="T32">paraxot-ɨ-nan</ta>
            <ta e="T34" id="Seg_5967" s="T33">onton</ta>
            <ta e="T35" id="Seg_5968" s="T34">ü͡ören-ni-m</ta>
            <ta e="T36" id="Seg_5969" s="T35">Krasnajarskaj</ta>
            <ta e="T37" id="Seg_5970" s="T36">pʼexoːta-ga</ta>
            <ta e="T41" id="Seg_5971" s="T40">u͡on</ta>
            <ta e="T42" id="Seg_5972" s="T41">biːr</ta>
            <ta e="T43" id="Seg_5973" s="T42">ɨj-ɨ</ta>
            <ta e="T44" id="Seg_5974" s="T43">duː</ta>
            <ta e="T45" id="Seg_5975" s="T44">kah-ɨ</ta>
            <ta e="T46" id="Seg_5976" s="T45">duː</ta>
            <ta e="T47" id="Seg_5977" s="T46">ü͡ören-en</ta>
            <ta e="T48" id="Seg_5978" s="T47">baraːn</ta>
            <ta e="T49" id="Seg_5979" s="T48">emi͡e</ta>
            <ta e="T50" id="Seg_5980" s="T49">katt-ɨː</ta>
            <ta e="T51" id="Seg_5981" s="T54">kimi͡e-ke</ta>
            <ta e="T52" id="Seg_5982" s="T51">Zvʼenʼigu͡orak-ka</ta>
            <ta e="T56" id="Seg_5983" s="T55">parasut</ta>
            <ta e="T57" id="Seg_5984" s="T56">razbiraj-daː-ččɨ</ta>
            <ta e="T58" id="Seg_5985" s="T57">e-ti-m</ta>
            <ta e="T60" id="Seg_5986" s="T59">dʼe</ta>
            <ta e="T61" id="Seg_5987" s="T60">onno</ta>
            <ta e="T62" id="Seg_5988" s="T61">uhun</ta>
            <ta e="T63" id="Seg_5989" s="T62">da</ta>
            <ta e="T64" id="Seg_5990" s="T63">ü͡örem-meteg-i-m</ta>
            <ta e="T65" id="Seg_5991" s="T64">dogot-tor-u-m</ta>
            <ta e="T66" id="Seg_5992" s="T65">gɨtta</ta>
            <ta e="T67" id="Seg_5993" s="T66">ba</ta>
            <ta e="T68" id="Seg_5994" s="T67">kaːl-ɨ-m-aːrɨ-bɨn</ta>
            <ta e="T69" id="Seg_5995" s="T68">biːrge</ta>
            <ta e="T70" id="Seg_5996" s="T69">bar-s-ɨ-bɨt-ɨ-m</ta>
            <ta e="T71" id="Seg_5997" s="T70">dʼe</ta>
            <ta e="T72" id="Seg_5998" s="T71">ol</ta>
            <ta e="T73" id="Seg_5999" s="T72">kim-neː-ti-m</ta>
            <ta e="T74" id="Seg_6000" s="T73">kimi͡e-ke</ta>
            <ta e="T75" id="Seg_6001" s="T74">Muːrom-ŋa</ta>
            <ta e="T76" id="Seg_6002" s="T75">ü͡ören-e</ta>
            <ta e="T77" id="Seg_6003" s="T76">kim-neː-ti-bit</ta>
            <ta e="T481" id="Seg_6004" s="T80">prosta</ta>
            <ta e="T81" id="Seg_6005" s="T481">kim-niː-r</ta>
            <ta e="T82" id="Seg_6006" s="T81">e-ti-lere</ta>
            <ta e="T83" id="Seg_6007" s="T82">onno</ta>
            <ta e="T84" id="Seg_6008" s="T83">farmʼiravanʼije</ta>
            <ta e="T85" id="Seg_6009" s="T84">oŋor-or</ta>
            <ta e="T86" id="Seg_6010" s="T85">e-ti-lere</ta>
            <ta e="T92" id="Seg_6011" s="T91">tu͡ok-ka</ta>
            <ta e="T93" id="Seg_6012" s="T92">ü͡örem-mik-ki-n</ta>
            <ta e="T94" id="Seg_6013" s="T93">kim-neːk</ta>
            <ta e="T95" id="Seg_6014" s="T94">kim</ta>
            <ta e="T96" id="Seg_6015" s="T95">kim-niː-l-ler</ta>
            <ta e="T97" id="Seg_6016" s="T96">otto</ta>
            <ta e="T101" id="Seg_6017" s="T100">dʼe</ta>
            <ta e="T102" id="Seg_6018" s="T101">onno</ta>
            <ta e="T103" id="Seg_6019" s="T102">kim-neː-ti-m</ta>
            <ta e="T104" id="Seg_6020" s="T103">da</ta>
            <ta e="T105" id="Seg_6021" s="T104">Muːrom-tan</ta>
            <ta e="T106" id="Seg_6022" s="T105">kimi͡e-ke</ta>
            <ta e="T107" id="Seg_6023" s="T106">tübes-ti-bit</ta>
            <ta e="T108" id="Seg_6024" s="T107">ol</ta>
            <ta e="T109" id="Seg_6025" s="T108">Tuːla-ga</ta>
            <ta e="T110" id="Seg_6026" s="T109">ol</ta>
            <ta e="T111" id="Seg_6027" s="T110">Tula</ta>
            <ta e="T115" id="Seg_6028" s="T114">bu͡o</ta>
            <ta e="T116" id="Seg_6029" s="T115">agɨs</ta>
            <ta e="T117" id="Seg_6030" s="T116">kilametr-ɨ</ta>
            <ta e="T118" id="Seg_6031" s="T117">duː</ta>
            <ta e="T119" id="Seg_6032" s="T118">kah-ɨ</ta>
            <ta e="T120" id="Seg_6033" s="T119">duː</ta>
            <ta e="T121" id="Seg_6034" s="T120">bar-an</ta>
            <ta e="T122" id="Seg_6035" s="T121">baraːn</ta>
            <ta e="T123" id="Seg_6036" s="T122">timir</ta>
            <ta e="T124" id="Seg_6037" s="T123">orok-tor-u-ŋ</ta>
            <ta e="T125" id="Seg_6038" s="T124">barɨta</ta>
            <ta e="T126" id="Seg_6039" s="T125">aldʼam-mɨt-tara</ta>
            <ta e="T127" id="Seg_6040" s="T126">bu͡ol-l-a</ta>
            <ta e="T128" id="Seg_6041" s="T127">dʼe</ta>
            <ta e="T129" id="Seg_6042" s="T128">onno</ta>
            <ta e="T130" id="Seg_6043" s="T129">kim-n-iː</ta>
            <ta e="T131" id="Seg_6044" s="T130">hɨt-tak-pɨtɨna</ta>
            <ta e="T132" id="Seg_6045" s="T131">uže</ta>
            <ta e="T134" id="Seg_6046" s="T133">bu͡o</ta>
            <ta e="T135" id="Seg_6047" s="T134">bi͡ek</ta>
            <ta e="T136" id="Seg_6048" s="T135">köt-ö</ta>
            <ta e="T139" id="Seg_6049" s="T138">köt-öːččü</ta>
            <ta e="T140" id="Seg_6050" s="T139">e-ti-lere</ta>
            <ta e="T141" id="Seg_6051" s="T140">harsi͡erda</ta>
            <ta e="T142" id="Seg_6052" s="T141">aːjɨ</ta>
            <ta e="T143" id="Seg_6053" s="T142">dʼe</ta>
            <ta e="T144" id="Seg_6054" s="T143">onton</ta>
            <ta e="T145" id="Seg_6055" s="T144">uhun</ta>
            <ta e="T146" id="Seg_6056" s="T145">da</ta>
            <ta e="T147" id="Seg_6057" s="T146">bu͡ol-l-a</ta>
            <ta e="T148" id="Seg_6058" s="T147">eː</ta>
            <ta e="T151" id="Seg_6059" s="T150">kim-n-iː-bit</ta>
            <ta e="T152" id="Seg_6060" s="T151">onno</ta>
            <ta e="T153" id="Seg_6061" s="T152">ile</ta>
            <ta e="T154" id="Seg_6062" s="T153">hatɨː</ta>
            <ta e="T155" id="Seg_6063" s="T154">bar-bɨp-pɨt</ta>
            <ta e="T156" id="Seg_6064" s="T155">ol</ta>
            <ta e="T157" id="Seg_6065" s="T156">kimi͡e-ke</ta>
            <ta e="T158" id="Seg_6066" s="T157">kanna</ta>
            <ta e="T161" id="Seg_6067" s="T160">hol</ta>
            <ta e="T162" id="Seg_6068" s="T161">vajna</ta>
            <ta e="T163" id="Seg_6069" s="T162">bu͡ol-u͡ok</ta>
            <ta e="T164" id="Seg_6070" s="T163">hir-e</ta>
            <ta e="T165" id="Seg_6071" s="T164">bu͡ol-u͡o-r</ta>
            <ta e="T166" id="Seg_6072" s="T165">di͡eri</ta>
            <ta e="T167" id="Seg_6073" s="T166">dʼe</ta>
            <ta e="T168" id="Seg_6074" s="T167">onno</ta>
            <ta e="T169" id="Seg_6075" s="T168">kim-neː-čči-ler</ta>
            <ta e="T170" id="Seg_6076" s="T169">bu͡o</ta>
            <ta e="T171" id="Seg_6077" s="T170">kim</ta>
            <ta e="T172" id="Seg_6078" s="T171">partʼizanskɨj</ta>
            <ta e="T173" id="Seg_6079" s="T172">iti</ta>
            <ta e="T174" id="Seg_6080" s="T173">аtrʼad</ta>
            <ta e="T175" id="Seg_6081" s="T174">o</ta>
            <ta e="T176" id="Seg_6082" s="T175">kim-ŋe</ta>
            <ta e="T177" id="Seg_6083" s="T176">NOP</ta>
            <ta e="T178" id="Seg_6084" s="T177">SOP</ta>
            <ta e="T179" id="Seg_6085" s="T178">abaznačaj-dɨː-l-lar</ta>
            <ta e="T180" id="Seg_6086" s="T179">o-nu</ta>
            <ta e="T181" id="Seg_6087" s="T180">da</ta>
            <ta e="T182" id="Seg_6088" s="T181">bu</ta>
            <ta e="T183" id="Seg_6089" s="T182">muja</ta>
            <ta e="T184" id="Seg_6090" s="T183">kim-niː</ta>
            <ta e="T185" id="Seg_6091" s="T184">kim-niː-r-ge</ta>
            <ta e="T186" id="Seg_6092" s="T185">ürek</ta>
            <ta e="T187" id="Seg_6093" s="T186">tu͡ok</ta>
            <ta e="T188" id="Seg_6094" s="T187">ere</ta>
            <ta e="T189" id="Seg_6095" s="T188">ürek-keːn</ta>
            <ta e="T190" id="Seg_6096" s="T189">hɨt-ar</ta>
            <ta e="T191" id="Seg_6097" s="T190">če</ta>
            <ta e="T192" id="Seg_6098" s="T191">onno</ta>
            <ta e="T193" id="Seg_6099" s="T192">dʼerʼevnʼa-ga</ta>
            <ta e="T194" id="Seg_6100" s="T193">onno</ta>
            <ta e="T196" id="Seg_6101" s="T195">gɨn-al-lar</ta>
            <ta e="T197" id="Seg_6102" s="T196">eni</ta>
            <ta e="T198" id="Seg_6103" s="T197">harsi͡erda</ta>
            <ta e="T199" id="Seg_6104" s="T198">kim-neː-ti-bit</ta>
            <ta e="T200" id="Seg_6105" s="T199">ah-ɨː</ta>
            <ta e="T201" id="Seg_6106" s="T200">ah-ɨː</ta>
            <ta e="T202" id="Seg_6107" s="T201">tüh-en</ta>
            <ta e="T203" id="Seg_6108" s="T202">bar-aːm-mɨt</ta>
            <ta e="T204" id="Seg_6109" s="T203">o-nu</ta>
            <ta e="T205" id="Seg_6110" s="T204">dʼe</ta>
            <ta e="T206" id="Seg_6111" s="T205">ɨt-ɨ͡al-a-h-al-lar</ta>
            <ta e="T207" id="Seg_6112" s="T206">kamandʼir-bɨtɨ-n</ta>
            <ta e="T208" id="Seg_6113" s="T207">kepsiː-r</ta>
            <ta e="T209" id="Seg_6114" s="T208">kajtak</ta>
            <ta e="T212" id="Seg_6115" s="T211">kim-n-i͡ek-kiti-n</ta>
            <ta e="T214" id="Seg_6116" s="T212">onton</ta>
            <ta e="T215" id="Seg_6117" s="T214">dʼe</ta>
            <ta e="T216" id="Seg_6118" s="T215">аkruzeːnije</ta>
            <ta e="T217" id="Seg_6119" s="T216">tübes-tek-kitine</ta>
            <ta e="T218" id="Seg_6120" s="T217">bagar</ta>
            <ta e="T219" id="Seg_6121" s="T218">plʼen</ta>
            <ta e="T220" id="Seg_6122" s="T219">tübes-tek-ketin</ta>
            <ta e="T221" id="Seg_6123" s="T220">itte</ta>
            <ta e="T223" id="Seg_6124" s="T222">kim</ta>
            <ta e="T224" id="Seg_6125" s="T223">kim-neː-m-i͡ek-ki-n</ta>
            <ta e="T225" id="Seg_6126" s="T224">naːda</ta>
            <ta e="T226" id="Seg_6127" s="T225">bu͡ol-l-a</ta>
            <ta e="T227" id="Seg_6128" s="T226">beje-gi-n</ta>
            <ta e="T228" id="Seg_6129" s="T227">bi͡er-i-m-i͡ek-ki-n</ta>
            <ta e="T229" id="Seg_6130" s="T228">bagar</ta>
            <ta e="T230" id="Seg_6131" s="T229">prosta</ta>
            <ta e="T233" id="Seg_6132" s="T232">kaj-ɨtalaː-ma-ŋ</ta>
            <ta e="T234" id="Seg_6133" s="T233">huruk</ta>
            <ta e="T235" id="Seg_6134" s="T234">eː</ta>
            <ta e="T236" id="Seg_6135" s="T235">huruk-tar-gɨt</ta>
            <ta e="T237" id="Seg_6136" s="T236">diː-r</ta>
            <ta e="T238" id="Seg_6137" s="T237">bu͡olla</ta>
            <ta e="T241" id="Seg_6138" s="T240">kim-neː-ŋ</ta>
            <ta e="T244" id="Seg_6139" s="T243">e</ta>
            <ta e="T245" id="Seg_6140" s="T244">köm-ü-ŋ</ta>
            <ta e="T246" id="Seg_6141" s="T245">duː</ta>
            <ta e="T247" id="Seg_6142" s="T246">beje-git</ta>
            <ta e="T248" id="Seg_6143" s="T247">hi͡e-ŋ</ta>
            <ta e="T249" id="Seg_6144" s="T248">duː</ta>
            <ta e="T250" id="Seg_6145" s="T249">di͡e-čči</ta>
            <ta e="T251" id="Seg_6146" s="T250">bugurduk</ta>
            <ta e="T252" id="Seg_6147" s="T251">kaj-atalaː-tɨ-ŋ</ta>
            <ta e="T253" id="Seg_6148" s="T252">daː</ta>
            <ta e="T255" id="Seg_6149" s="T253">vsʼo ravno</ta>
            <ta e="T256" id="Seg_6150" s="T255">kili͡ej-d</ta>
            <ta e="T257" id="Seg_6151" s="T256">iti</ta>
            <ta e="T258" id="Seg_6152" s="T257">kim-niː-l-ler</ta>
            <ta e="T259" id="Seg_6153" s="T258">bu͡o</ta>
            <ta e="T260" id="Seg_6154" s="T259">ol</ta>
            <ta e="T261" id="Seg_6155" s="T260">bul-al-lar</ta>
            <ta e="T262" id="Seg_6156" s="T261">bu͡o</ta>
            <ta e="T263" id="Seg_6157" s="T262">ol</ta>
            <ta e="T264" id="Seg_6158" s="T263">ke</ta>
            <ta e="T265" id="Seg_6159" s="T264">orota-ŋ</ta>
            <ta e="T266" id="Seg_6160" s="T265">keːh-e</ta>
            <ta e="T267" id="Seg_6161" s="T266">hüːr-büp-pün</ta>
            <ta e="T268" id="Seg_6162" s="T267">biːrde</ta>
            <ta e="T271" id="Seg_6163" s="T270">kannuk-tarɨ-n</ta>
            <ta e="T272" id="Seg_6164" s="T271">da</ta>
            <ta e="T273" id="Seg_6165" s="T272">dʼüːlleː-m-min</ta>
            <ta e="T274" id="Seg_6166" s="T273">karaŋa-ga</ta>
            <ta e="T275" id="Seg_6167" s="T274">nʼemʼes-tere</ta>
            <ta e="T276" id="Seg_6168" s="T275">da</ta>
            <ta e="T277" id="Seg_6169" s="T276">kim-nere</ta>
            <ta e="T278" id="Seg_6170" s="T277">da</ta>
            <ta e="T279" id="Seg_6171" s="T278">dʼe</ta>
            <ta e="T280" id="Seg_6172" s="T279">ol</ta>
            <ta e="T281" id="Seg_6173" s="T280">min</ta>
            <ta e="T282" id="Seg_6174" s="T281">hɨldʼ-am-mɨn</ta>
            <ta e="T283" id="Seg_6175" s="T282">kör-dü-m</ta>
            <ta e="T284" id="Seg_6176" s="T283">bi͡es</ta>
            <ta e="T285" id="Seg_6177" s="T284">kihi</ta>
            <ta e="T286" id="Seg_6178" s="T285">hɨldʼ-al-lar</ta>
            <ta e="T287" id="Seg_6179" s="T286">araj</ta>
            <ta e="T288" id="Seg_6180" s="T287">bugurduk</ta>
            <ta e="T289" id="Seg_6181" s="T288">dʼe</ta>
            <ta e="T290" id="Seg_6182" s="T289">onton</ta>
            <ta e="T291" id="Seg_6183" s="T290">ol</ta>
            <ta e="T292" id="Seg_6184" s="T291">di͡egi-tten</ta>
            <ta e="T293" id="Seg_6185" s="T292">nʼems-ɨ-lar-bɨt</ta>
            <ta e="T294" id="Seg_6186" s="T293">kel-el-ler</ta>
            <ta e="T295" id="Seg_6187" s="T294">transej-tan</ta>
            <ta e="T296" id="Seg_6188" s="T295">tur-al-lar</ta>
            <ta e="T297" id="Seg_6189" s="T296">töttörü</ta>
            <ta e="T298" id="Seg_6190" s="T297">kür-ü͡ögüŋ</ta>
            <ta e="T299" id="Seg_6191" s="T298">d-iː-d-iː</ta>
            <ta e="T300" id="Seg_6192" s="T299">kim-niː-l-ler</ta>
            <ta e="T301" id="Seg_6193" s="T300">ol</ta>
            <ta e="T302" id="Seg_6194" s="T301">kihi-ŋ</ta>
            <ta e="T303" id="Seg_6195" s="T302">kamandʼir-dar-daːk-tar</ta>
            <ta e="T304" id="Seg_6196" s="T303">ol</ta>
            <ta e="T305" id="Seg_6197" s="T304">dogot-tor-o</ta>
            <ta e="T306" id="Seg_6198" s="T305">atɨt-tar</ta>
            <ta e="T307" id="Seg_6199" s="T306">onto</ta>
            <ta e="T308" id="Seg_6200" s="T307">atɨn</ta>
            <ta e="T309" id="Seg_6201" s="T308">čʼaːs-tan</ta>
            <ta e="T310" id="Seg_6202" s="T309">beje-m</ta>
            <ta e="T313" id="Seg_6203" s="T312">dogot-tor-bu-n</ta>
            <ta e="T314" id="Seg_6204" s="T313">ol</ta>
            <ta e="T315" id="Seg_6205" s="T314">keːh-e</ta>
            <ta e="T316" id="Seg_6206" s="T315">hüːr-büp-pün</ta>
            <ta e="T317" id="Seg_6207" s="T316">eni</ta>
            <ta e="T318" id="Seg_6208" s="T317">ki͡e</ta>
            <ta e="T319" id="Seg_6209" s="T318">duː</ta>
            <ta e="T320" id="Seg_6210" s="T319">tu͡ok</ta>
            <ta e="T321" id="Seg_6211" s="T320">duː</ta>
            <ta e="T322" id="Seg_6212" s="T321">astupaj-daː-ŋ</ta>
            <ta e="T323" id="Seg_6213" s="T322">diː-r</ta>
            <ta e="T327" id="Seg_6214" s="T326">diː-r</ta>
            <ta e="T328" id="Seg_6215" s="T327">kamandʼir-dara</ta>
            <ta e="T329" id="Seg_6216" s="T328">onton</ta>
            <ta e="T330" id="Seg_6217" s="T329">kör-dü-m</ta>
            <ta e="T331" id="Seg_6218" s="T330">do</ta>
            <ta e="T332" id="Seg_6219" s="T331">nʼems-ɨ-lar-ɨ-ŋ</ta>
            <ta e="T333" id="Seg_6220" s="T332">hubu</ta>
            <ta e="T334" id="Seg_6221" s="T333">kel-en</ta>
            <ta e="T335" id="Seg_6222" s="T334">ih-el-ler</ta>
            <ta e="T336" id="Seg_6223" s="T335">tuːg-u</ta>
            <ta e="T337" id="Seg_6224" s="T336">tuːg-u</ta>
            <ta e="T338" id="Seg_6225" s="T337">kadʼɨgɨnah-al-lar</ta>
            <ta e="T339" id="Seg_6226" s="T338">sepset-el-ler</ta>
            <ta e="T340" id="Seg_6227" s="T339">töttörü</ta>
            <ta e="T341" id="Seg_6228" s="T340">hüːr-e-bin</ta>
            <ta e="T342" id="Seg_6229" s="T341">dogot-tor-u-m</ta>
            <ta e="T343" id="Seg_6230" s="T342">bar-bɨt</ta>
            <ta e="T344" id="Seg_6231" s="T343">hir-deri-n</ta>
            <ta e="T345" id="Seg_6232" s="T344">di͡ek</ta>
            <ta e="T346" id="Seg_6233" s="T345">bu͡opsa</ta>
            <ta e="T347" id="Seg_6234" s="T346">hurdurgah-al-lar</ta>
            <ta e="T348" id="Seg_6235" s="T347">agaj</ta>
            <ta e="T349" id="Seg_6236" s="T348">itinne</ta>
            <ta e="T350" id="Seg_6237" s="T349">ok-ko</ta>
            <ta e="T351" id="Seg_6238" s="T350">ɨt-ɨ͡alɨː-l-lar</ta>
            <ta e="T352" id="Seg_6239" s="T351">ile</ta>
            <ta e="T353" id="Seg_6240" s="T352">kim-n-eːri</ta>
            <ta e="T354" id="Seg_6241" s="T353">gɨn-al-lar</ta>
            <ta e="T355" id="Seg_6242" s="T354">bɨhɨlaːk</ta>
            <ta e="T357" id="Seg_6243" s="T355">otto</ta>
            <ta e="T358" id="Seg_6244" s="T357">plʼen</ta>
            <ta e="T359" id="Seg_6245" s="T358">ɨl-aːrɨ</ta>
            <ta e="T360" id="Seg_6246" s="T359">uhug-u-ttan</ta>
            <ta e="T361" id="Seg_6247" s="T360">transej-ga</ta>
            <ta e="T362" id="Seg_6248" s="T361">kel-li-m</ta>
            <ta e="T363" id="Seg_6249" s="T362">kel-bit-i-m</ta>
            <ta e="T364" id="Seg_6250" s="T363">beje-m</ta>
            <ta e="T365" id="Seg_6251" s="T364">dogot</ta>
            <ta e="T366" id="Seg_6252" s="T365">e</ta>
            <ta e="T367" id="Seg_6253" s="T366">rota-ba-r</ta>
            <ta e="T368" id="Seg_6254" s="T367">tübes-pip-pin</ta>
            <ta e="T369" id="Seg_6255" s="T368">dʼe</ta>
            <ta e="T370" id="Seg_6256" s="T369">ol</ta>
            <ta e="T371" id="Seg_6257" s="T370">kim-n-iː-bin</ta>
            <ta e="T372" id="Seg_6258" s="T371">kaja</ta>
            <ta e="T375" id="Seg_6259" s="T374">pulemʼot-ɨ-nan</ta>
            <ta e="T376" id="Seg_6260" s="T375">kim-n-i͡e</ta>
            <ta e="T377" id="Seg_6261" s="T376">sraːzu</ta>
            <ta e="T378" id="Seg_6262" s="T377">hüːrbe-tten</ta>
            <ta e="T379" id="Seg_6263" s="T378">taksa</ta>
            <ta e="T380" id="Seg_6264" s="T379">kihi-ni</ta>
            <ta e="T381" id="Seg_6265" s="T380">tüh-e-r-di-ler</ta>
            <ta e="T382" id="Seg_6266" s="T381">horok-tor-but</ta>
            <ta e="T383" id="Seg_6267" s="T382">töttörü</ta>
            <ta e="T384" id="Seg_6268" s="T383">hüːr-dü-ler</ta>
            <ta e="T385" id="Seg_6269" s="T384">ol</ta>
            <ta e="T386" id="Seg_6270" s="T385">dаzor</ta>
            <ta e="T387" id="Seg_6271" s="T386">bu͡ol-aːččɨ</ta>
            <ta e="T390" id="Seg_6272" s="T389">orota-ŋ</ta>
            <ta e="T391" id="Seg_6273" s="T390">bar-ɨ͡ag-a</ta>
            <ta e="T392" id="Seg_6274" s="T391">össü͡ö</ta>
            <ta e="T393" id="Seg_6275" s="T392">kim</ta>
            <ta e="T394" id="Seg_6276" s="T393">vpʼerʼedʼi</ta>
            <ta e="T395" id="Seg_6277" s="T394">hɨldʼ-ɨ͡ag-a</ta>
            <ta e="T396" id="Seg_6278" s="T395">razvʼečʼčʼik</ta>
            <ta e="T397" id="Seg_6279" s="T396">kördük</ta>
            <ta e="T398" id="Seg_6280" s="T397">orota-gɨ-n</ta>
            <ta e="T399" id="Seg_6281" s="T398">agaj</ta>
            <ta e="T400" id="Seg_6282" s="T399">ket-iː-gin</ta>
            <ta e="T401" id="Seg_6283" s="T400">bu͡o</ta>
            <ta e="T402" id="Seg_6284" s="T401">inniki-ler</ta>
            <ta e="T403" id="Seg_6285" s="T402">ikki</ta>
            <ta e="T404" id="Seg_6286" s="T403">kihi</ta>
            <ta e="T405" id="Seg_6287" s="T404">bu͡ol-ar</ta>
            <ta e="T406" id="Seg_6288" s="T405">kaŋas</ta>
            <ta e="T407" id="Seg_6289" s="T406">di͡ek</ta>
            <ta e="T408" id="Seg_6290" s="T407">uŋa</ta>
            <ta e="T409" id="Seg_6291" s="T408">di͡ek</ta>
            <ta e="T410" id="Seg_6292" s="T409">emi͡e</ta>
            <ta e="T411" id="Seg_6293" s="T410">ikki-liː</ta>
            <ta e="T412" id="Seg_6294" s="T411">kihi</ta>
            <ta e="T413" id="Seg_6295" s="T412">če</ta>
            <ta e="T414" id="Seg_6296" s="T413">kajti͡ek</ta>
            <ta e="T415" id="Seg_6297" s="T414">bu͡ol-u͡ok-put=uj</ta>
            <ta e="T416" id="Seg_6298" s="T415">diː-r</ta>
            <ta e="T417" id="Seg_6299" s="T416">bu</ta>
            <ta e="T418" id="Seg_6300" s="T417">dʼerʼevnʼa-ga</ta>
            <ta e="T419" id="Seg_6301" s="T418">pravʼerʼaj-d-ɨː-bɨt</ta>
            <ta e="T420" id="Seg_6302" s="T419">ile</ta>
            <ta e="T421" id="Seg_6303" s="T420">kim-neː</ta>
            <ta e="T422" id="Seg_6304" s="T421">kim</ta>
            <ta e="T423" id="Seg_6305" s="T422">da</ta>
            <ta e="T424" id="Seg_6306" s="T423">hu͡ok</ta>
            <ta e="T426" id="Seg_6307" s="T425">eː</ta>
            <ta e="T427" id="Seg_6308" s="T426">nʼems-a-lar</ta>
            <ta e="T428" id="Seg_6309" s="T427">oŋu͡or</ta>
            <ta e="T429" id="Seg_6310" s="T428">ere</ta>
            <ta e="T430" id="Seg_6311" s="T429">köst-öl-lör</ta>
            <ta e="T431" id="Seg_6312" s="T430">töttörü</ta>
            <ta e="T432" id="Seg_6313" s="T431">bar-ɨ͡agɨŋ</ta>
            <ta e="T433" id="Seg_6314" s="T432">d-iː-d-iː</ta>
            <ta e="T434" id="Seg_6315" s="T433">gɨn-ar</ta>
            <ta e="T435" id="Seg_6316" s="T434">če</ta>
            <ta e="T436" id="Seg_6317" s="T435">ol</ta>
            <ta e="T437" id="Seg_6318" s="T436">kim-n-iː</ta>
            <ta e="T438" id="Seg_6319" s="T437">hɨldʼ-am-mɨt</ta>
            <ta e="T439" id="Seg_6320" s="T438">u͡oldʼas-tɨ-bɨt</ta>
            <ta e="T440" id="Seg_6321" s="T439">ol</ta>
            <ta e="T441" id="Seg_6322" s="T440">ol</ta>
            <ta e="T442" id="Seg_6323" s="T441">ürek-piti-n</ta>
            <ta e="T443" id="Seg_6324" s="T442">taŋnarɨ</ta>
            <ta e="T444" id="Seg_6325" s="T443">bar-dɨ-bɨt</ta>
            <ta e="T445" id="Seg_6326" s="T444">hüːrük</ta>
            <ta e="T446" id="Seg_6327" s="T445">taŋnarɨ</ta>
            <ta e="T447" id="Seg_6328" s="T446">ürek-ten-en</ta>
            <ta e="T448" id="Seg_6329" s="T447">e</ta>
            <ta e="T449" id="Seg_6330" s="T448">bar-am-mɨt</ta>
            <ta e="T450" id="Seg_6331" s="T449">eː</ta>
            <ta e="T451" id="Seg_6332" s="T450">kim-n-iː-bit</ta>
            <ta e="T452" id="Seg_6333" s="T451">bugurduk</ta>
            <ta e="T453" id="Seg_6334" s="T452">kim-nere</ta>
            <ta e="T454" id="Seg_6335" s="T453">kim-neː-bit-ter</ta>
            <ta e="T455" id="Seg_6336" s="T454">bu</ta>
            <ta e="T456" id="Seg_6337" s="T455">artʼilʼlʼerʼija</ta>
            <ta e="T457" id="Seg_6338" s="T456">bu͡ol-aːččɨ</ta>
            <ta e="T458" id="Seg_6339" s="T457">bu͡o</ta>
            <ta e="T459" id="Seg_6340" s="T458">puːska-lara</ta>
            <ta e="T462" id="Seg_6341" s="T461">e</ta>
            <ta e="T463" id="Seg_6342" s="T462">kim-nere</ta>
            <ta e="T464" id="Seg_6343" s="T463">at-tar</ta>
            <ta e="T465" id="Seg_6344" s="T464">barɨta</ta>
            <ta e="T466" id="Seg_6345" s="T465">kim-n-e-ll-i-bit-ter</ta>
            <ta e="T467" id="Seg_6346" s="T466">bugurduk</ta>
            <ta e="T468" id="Seg_6347" s="T467">perevaroč-ɨː</ta>
            <ta e="T469" id="Seg_6348" s="T468">kim-neː-bit-ter</ta>
            <ta e="T470" id="Seg_6349" s="T469">onto</ta>
            <ta e="T471" id="Seg_6350" s="T470">ölör-töː-büt-ter</ta>
            <ta e="T472" id="Seg_6351" s="T471">onno</ta>
            <ta e="T473" id="Seg_6352" s="T472">biːr</ta>
            <ta e="T474" id="Seg_6353" s="T473">kihi-ni</ta>
            <ta e="T475" id="Seg_6354" s="T474">bul-lu-but</ta>
            <ta e="T8" id="Seg_6355" s="T475">paluzivoj</ta>
            <ta e="T476" id="Seg_6356" s="T8">ile</ta>
            <ta e="T477" id="Seg_6357" s="T476">kajdi͡ek</ta>
            <ta e="T478" id="Seg_6358" s="T477">ere</ta>
            <ta e="T479" id="Seg_6359" s="T478">arɨččɨ</ta>
            <ta e="T480" id="Seg_6360" s="T479">haŋar-ar</ta>
            <ta e="T482" id="Seg_6361" s="T480">onton</ta>
            <ta e="T483" id="Seg_6362" s="T482">bu</ta>
            <ta e="T484" id="Seg_6363" s="T483">haŋardɨː</ta>
            <ta e="T485" id="Seg_6364" s="T484">tabak</ta>
            <ta e="T486" id="Seg_6365" s="T485">tard-ar</ta>
            <ta e="T487" id="Seg_6366" s="T486">ɨksa-ta</ta>
            <ta e="T488" id="Seg_6367" s="T487">bu͡ol-l-a</ta>
            <ta e="T489" id="Seg_6368" s="T488">d-iː-d-iː</ta>
            <ta e="T490" id="Seg_6369" s="T489">gɨn-ar</ta>
            <ta e="T491" id="Seg_6370" s="T490">bihigi</ta>
            <ta e="T492" id="Seg_6371" s="T491">rota-bɨtɨ-n</ta>
            <ta e="T493" id="Seg_6372" s="T492">kim-neː-bit-tere</ta>
            <ta e="T494" id="Seg_6373" s="T493">diː-r</ta>
            <ta e="T495" id="Seg_6374" s="T494">nʼemsi-te</ta>
            <ta e="T496" id="Seg_6375" s="T495">heː</ta>
            <ta e="T497" id="Seg_6376" s="T496">bar-bɨt-tara</ta>
            <ta e="T498" id="Seg_6377" s="T497">anak-kaːn</ta>
            <ta e="T499" id="Seg_6378" s="T498">diː-r</ta>
            <ta e="T500" id="Seg_6379" s="T499">haŋar-s-ɨ-ma-ŋ</ta>
            <ta e="T501" id="Seg_6380" s="T500">tɨŋ-nɨk</ta>
            <ta e="T502" id="Seg_6381" s="T501">d-iː-d-iː</ta>
            <ta e="T503" id="Seg_6382" s="T502">gɨn-ar</ta>
            <ta e="T504" id="Seg_6383" s="T503">dʼe</ta>
            <ta e="T505" id="Seg_6384" s="T504">ol</ta>
            <ta e="T507" id="Seg_6385" s="T505">kim-neː-ti-bit</ta>
            <ta e="T508" id="Seg_6386" s="T507">dʼe</ta>
            <ta e="T509" id="Seg_6387" s="T508">onno</ta>
            <ta e="T510" id="Seg_6388" s="T509">diː</ta>
            <ta e="T511" id="Seg_6389" s="T510">eː</ta>
            <ta e="T512" id="Seg_6390" s="T511">dogo</ta>
            <ta e="T513" id="Seg_6391" s="T512">ikki</ta>
            <ta e="T514" id="Seg_6392" s="T513">kihi-ni</ta>
            <ta e="T515" id="Seg_6393" s="T514">ɨːt-al-lar</ta>
            <ta e="T516" id="Seg_6394" s="T515">onno</ta>
            <ta e="T517" id="Seg_6395" s="T516">kim</ta>
            <ta e="T518" id="Seg_6396" s="T517">bu͡ol-aːččɨ</ta>
            <ta e="T519" id="Seg_6397" s="T518">sančʼas</ta>
            <ta e="T520" id="Seg_6398" s="T519">onto</ta>
            <ta e="T521" id="Seg_6399" s="T520">ill-i͡ek-pit</ta>
            <ta e="T522" id="Seg_6400" s="T521">diː-l-ler</ta>
            <ta e="T523" id="Seg_6401" s="T522">min</ta>
            <ta e="T524" id="Seg_6402" s="T523">bihigi</ta>
            <ta e="T525" id="Seg_6403" s="T524">enigi-n</ta>
            <ta e="T528" id="Seg_6404" s="T527">minigi-n</ta>
            <ta e="T529" id="Seg_6405" s="T528">tu͡ok</ta>
            <ta e="T530" id="Seg_6406" s="T529">kɨhall-ɨ-ma-ŋ</ta>
            <ta e="T531" id="Seg_6407" s="T530">diː-r</ta>
            <ta e="T532" id="Seg_6408" s="T531">min</ta>
            <ta e="T533" id="Seg_6409" s="T532">e</ta>
            <ta e="T534" id="Seg_6410" s="T533">hin</ta>
            <ta e="T535" id="Seg_6411" s="T534">kihi</ta>
            <ta e="T536" id="Seg_6412" s="T535">bu͡ol-u͡ok</ta>
            <ta e="T537" id="Seg_6413" s="T536">e</ta>
            <ta e="T538" id="Seg_6414" s="T537">kihi</ta>
            <ta e="T539" id="Seg_6415" s="T538">bu͡ol-batak-pɨn</ta>
            <ta e="T540" id="Seg_6416" s="T539">diː-r</ta>
            <ta e="T541" id="Seg_6417" s="T540">bu͡o</ta>
            <ta e="T542" id="Seg_6418" s="T541">öl-ü͡ö</ta>
            <ta e="T543" id="Seg_6419" s="T542">öl-ü͡ö-m</ta>
            <ta e="T544" id="Seg_6420" s="T543">anɨ</ta>
            <ta e="T545" id="Seg_6421" s="T544">kihi-ler</ta>
            <ta e="T546" id="Seg_6422" s="T545">holbok</ta>
            <ta e="T547" id="Seg_6423" s="T546">kördük</ta>
            <ta e="T548" id="Seg_6424" s="T547">hɨt-al-lar</ta>
            <ta e="T549" id="Seg_6425" s="T548">ile</ta>
            <ta e="T550" id="Seg_6426" s="T549">o-lor-ton</ta>
            <ta e="T551" id="Seg_6427" s="T550">h-onon</ta>
            <ta e="T552" id="Seg_6428" s="T551">ol</ta>
            <ta e="T553" id="Seg_6429" s="T552">kim-neː-ti-ler</ta>
            <ta e="T554" id="Seg_6430" s="T553">on-tu-gu-n</ta>
            <ta e="T555" id="Seg_6431" s="T554">il-ti-ler</ta>
            <ta e="T556" id="Seg_6432" s="T555">ikki</ta>
            <ta e="T557" id="Seg_6433" s="T556">kihi</ta>
            <ta e="T558" id="Seg_6434" s="T557">kimi͡e-ke</ta>
            <ta e="T559" id="Seg_6435" s="T558">on-tu-ŋ</ta>
            <ta e="T560" id="Seg_6436" s="T559">otto</ta>
            <ta e="T561" id="Seg_6437" s="T560">di͡e-bit</ta>
            <ta e="T562" id="Seg_6438" s="T561">ühü</ta>
            <ta e="T563" id="Seg_6439" s="T562">ol</ta>
            <ta e="T565" id="Seg_6440" s="T563">vsʼo ravno </ta>
            <ta e="T566" id="Seg_6441" s="T565">tiri͡er-e-me</ta>
            <ta e="T567" id="Seg_6442" s="T566">e</ta>
            <ta e="T568" id="Seg_6443" s="T567">kim-neː-n</ta>
            <ta e="T569" id="Seg_6444" s="T568">ol</ta>
            <ta e="T570" id="Seg_6445" s="T569">onton</ta>
            <ta e="T571" id="Seg_6446" s="T570">töttörü</ta>
            <ta e="T572" id="Seg_6447" s="T571">kel-li-ler</ta>
            <ta e="T573" id="Seg_6448" s="T572">o-lor-u-ŋ</ta>
            <ta e="T574" id="Seg_6449" s="T573">hin</ta>
            <ta e="T575" id="Seg_6450" s="T574">kihi</ta>
            <ta e="T576" id="Seg_6451" s="T575">bu͡ol-u͡o-m</ta>
            <ta e="T577" id="Seg_6452" s="T576">hu͡og-a</ta>
            <ta e="T578" id="Seg_6453" s="T577">tug-u</ta>
            <ta e="T579" id="Seg_6454" s="T578">minigi-n</ta>
            <ta e="T580" id="Seg_6455" s="T579">gɨtta</ta>
            <ta e="T583" id="Seg_6456" s="T582">kim-neː-git</ta>
            <ta e="T584" id="Seg_6457" s="T583">diː-r</ta>
            <ta e="T585" id="Seg_6458" s="T584">ühü</ta>
            <ta e="T586" id="Seg_6459" s="T585">harsi͡erda</ta>
            <ta e="T587" id="Seg_6460" s="T586">im-mit</ta>
            <ta e="T588" id="Seg_6461" s="T587">haŋardɨː</ta>
            <ta e="T589" id="Seg_6462" s="T588">taks-ar</ta>
            <ta e="T590" id="Seg_6463" s="T589">taks-an</ta>
            <ta e="T593" id="Seg_6464" s="T592">eː</ta>
            <ta e="T594" id="Seg_6465" s="T593">kim-neː-n</ta>
            <ta e="T595" id="Seg_6466" s="T594">er-deg-ine</ta>
            <ta e="T596" id="Seg_6467" s="T595">tu͡ok</ta>
            <ta e="T597" id="Seg_6468" s="T596">ere</ta>
            <ta e="T598" id="Seg_6469" s="T597">lʼejtʼenaːn-ɨ</ta>
            <ta e="T599" id="Seg_6470" s="T598">körüs-tü-büt</ta>
            <ta e="T600" id="Seg_6471" s="T599">on-tu-but</ta>
            <ta e="T601" id="Seg_6472" s="T600">diː-r</ta>
            <ta e="T602" id="Seg_6473" s="T601">kannɨk</ta>
            <ta e="T603" id="Seg_6474" s="T602">čʼaːs-taŋ-ŋɨn=ɨj</ta>
            <ta e="T604" id="Seg_6475" s="T603">ehigi</ta>
            <ta e="T605" id="Seg_6476" s="T604">ehigi-tten</ta>
            <ta e="T606" id="Seg_6477" s="T605">d-iː-bit</ta>
            <ta e="T607" id="Seg_6478" s="T606">diː-r</ta>
            <ta e="T608" id="Seg_6479" s="T607">onton</ta>
            <ta e="T609" id="Seg_6480" s="T608">biːr-bit</ta>
            <ta e="T610" id="Seg_6481" s="T609">onton</ta>
            <ta e="T611" id="Seg_6482" s="T610">oččogo</ta>
            <ta e="T612" id="Seg_6483" s="T611">minigi-n</ta>
            <ta e="T613" id="Seg_6484" s="T612">gɨtta</ta>
            <ta e="T614" id="Seg_6485" s="T613">hüːr-ü-ŋ</ta>
            <ta e="T615" id="Seg_6486" s="T614">diː-r</ta>
            <ta e="T616" id="Seg_6487" s="T615">inni</ta>
            <ta e="T617" id="Seg_6488" s="T616">di͡ek</ta>
            <ta e="T619" id="Seg_6489" s="T618">ere</ta>
            <ta e="T622" id="Seg_6490" s="T619">kim-neː-m-i͡ek-ke</ta>
            <ta e="T623" id="Seg_6491" s="T622">svʼaz</ta>
            <ta e="T624" id="Seg_6492" s="T623">ɨl-a</ta>
            <ta e="T625" id="Seg_6493" s="T624">kim-niː-r</ta>
            <ta e="T626" id="Seg_6494" s="T625">eni</ta>
            <ta e="T627" id="Seg_6495" s="T626">dʼe</ta>
            <ta e="T628" id="Seg_6496" s="T627">onton</ta>
            <ta e="T629" id="Seg_6497" s="T628">ol</ta>
            <ta e="T630" id="Seg_6498" s="T629">kim-n-iː</ta>
            <ta e="T631" id="Seg_6499" s="T630">hat-ɨː-bɨt</ta>
            <ta e="T632" id="Seg_6500" s="T631">kas</ta>
            <ta e="T633" id="Seg_6501" s="T632">da</ta>
            <ta e="T634" id="Seg_6502" s="T633">kün-ü</ta>
            <ta e="T635" id="Seg_6503" s="T634">ol</ta>
            <ta e="T636" id="Seg_6504" s="T635">hɨrɨt-tɨ-bɨt</ta>
            <ta e="T637" id="Seg_6505" s="T636">ol</ta>
            <ta e="T638" id="Seg_6506" s="T637">kurduk</ta>
            <ta e="T639" id="Seg_6507" s="T638">rota-bɨtɨ-n</ta>
            <ta e="T640" id="Seg_6508" s="T639">daː</ta>
            <ta e="T641" id="Seg_6509" s="T640">bul-bap-pɨt</ta>
            <ta e="T642" id="Seg_6510" s="T641">dʼe</ta>
            <ta e="T643" id="Seg_6511" s="T642">onton</ta>
            <ta e="T644" id="Seg_6512" s="T643">kojuk-ku-tu-n</ta>
            <ta e="T645" id="Seg_6513" s="T644">kim-neː-ti</ta>
            <ta e="T646" id="Seg_6514" s="T645">eː</ta>
            <ta e="T647" id="Seg_6515" s="T646">bul-lu-but</ta>
            <ta e="T648" id="Seg_6516" s="T647">emi͡e</ta>
            <ta e="T649" id="Seg_6517" s="T648">e</ta>
            <ta e="T652" id="Seg_6518" s="T651">üh-üs</ta>
            <ta e="T653" id="Seg_6519" s="T652">küm-mütü-ger</ta>
            <ta e="T654" id="Seg_6520" s="T653">duː</ta>
            <ta e="T655" id="Seg_6521" s="T654">kah-ɨs-pɨtɨ-gar</ta>
            <ta e="T656" id="Seg_6522" s="T655">duː</ta>
            <ta e="T657" id="Seg_6523" s="T656">ɨntak</ta>
            <ta e="T658" id="Seg_6524" s="T657">bar-a-bɨt</ta>
            <ta e="T659" id="Seg_6525" s="T658">onno</ta>
            <ta e="T660" id="Seg_6526" s="T659">kimi͡e-ke</ta>
            <ta e="T661" id="Seg_6527" s="T660">ol</ta>
            <ta e="T662" id="Seg_6528" s="T661">ürek-piti-n</ta>
            <ta e="T663" id="Seg_6529" s="T662">taks-a-bɨt</ta>
            <ta e="T664" id="Seg_6530" s="T663">dʼe</ta>
            <ta e="T665" id="Seg_6531" s="T664">onton</ta>
            <ta e="T666" id="Seg_6532" s="T665">ol</ta>
            <ta e="T667" id="Seg_6533" s="T666">u͡oldʼah-a-bɨt</ta>
            <ta e="T668" id="Seg_6534" s="T667">ol</ta>
            <ta e="T669" id="Seg_6535" s="T668">ɨnaraː-ttan</ta>
            <ta e="T670" id="Seg_6536" s="T669">ɨt-ɨ͡alɨː-l-lar</ta>
            <ta e="T671" id="Seg_6537" s="T670">a</ta>
            <ta e="T672" id="Seg_6538" s="T671">bihigi</ta>
            <ta e="T673" id="Seg_6539" s="T672">betereː-tten</ta>
            <ta e="T674" id="Seg_6540" s="T673">emi͡e</ta>
            <ta e="T675" id="Seg_6541" s="T674">ɨt-ɨ͡al-a-h-a-bɨt</ta>
            <ta e="T676" id="Seg_6542" s="T675">bu͡olla</ta>
            <ta e="T677" id="Seg_6543" s="T676">dʼe</ta>
            <ta e="T678" id="Seg_6544" s="T677">ol</ta>
            <ta e="T679" id="Seg_6545" s="T678">kim-neː-ti-bit</ta>
            <ta e="T680" id="Seg_6546" s="T679">daː</ta>
            <ta e="T681" id="Seg_6547" s="T680">onno</ta>
            <ta e="T682" id="Seg_6548" s="T681">kajdi͡ek</ta>
            <ta e="T683" id="Seg_6549" s="T682">daː</ta>
            <ta e="T684" id="Seg_6550" s="T683">kim-neː-bet-ter</ta>
            <ta e="T685" id="Seg_6551" s="T684">ɨːp-pat-tar</ta>
            <ta e="T686" id="Seg_6552" s="T685">ol</ta>
            <ta e="T687" id="Seg_6553" s="T686">harsi͡erda</ta>
            <ta e="T688" id="Seg_6554" s="T687">kɨ͡at-taːk</ta>
            <ta e="T689" id="Seg_6555" s="T688">kel-li-ler</ta>
            <ta e="T690" id="Seg_6556" s="T689">ol</ta>
            <ta e="T691" id="Seg_6557" s="T690">di͡ek</ta>
            <ta e="T692" id="Seg_6558" s="T691">inni-biti-tten</ta>
            <ta e="T693" id="Seg_6559" s="T692">bu</ta>
            <ta e="T696" id="Seg_6560" s="T695">kamanda</ta>
            <ta e="T697" id="Seg_6561" s="T696">bi͡er-el-ler</ta>
            <ta e="T698" id="Seg_6562" s="T697">nu</ta>
            <ta e="T699" id="Seg_6563" s="T698">otto</ta>
            <ta e="T700" id="Seg_6564" s="T699">üs</ta>
            <ta e="T701" id="Seg_6565" s="T700">čʼaːs-ka</ta>
            <ta e="T702" id="Seg_6566" s="T701">Brʼanskaj-ɨ</ta>
            <ta e="T703" id="Seg_6567" s="T702">zanʼimaj-d-ɨ͡ak-kɨtɨ-n</ta>
            <ta e="T704" id="Seg_6568" s="T703">naːda</ta>
            <ta e="T705" id="Seg_6569" s="T704">dʼe</ta>
            <ta e="T706" id="Seg_6570" s="T705">onno</ta>
            <ta e="T707" id="Seg_6571" s="T706">bugurduk</ta>
            <ta e="T708" id="Seg_6572" s="T707">tu͡ok</ta>
            <ta e="T709" id="Seg_6573" s="T708">ere</ta>
            <ta e="T710" id="Seg_6574" s="T709">hɨːr</ta>
            <ta e="T711" id="Seg_6575" s="T710">bu͡ol-ar</ta>
            <ta e="T712" id="Seg_6576" s="T711">dʼerbi</ta>
            <ta e="T715" id="Seg_6577" s="T714">dʼerbi-ni</ta>
            <ta e="T716" id="Seg_6578" s="T715">e</ta>
            <ta e="T717" id="Seg_6579" s="T716">kim-neː-ti-bit</ta>
            <ta e="T718" id="Seg_6580" s="T717">da</ta>
            <ta e="T719" id="Seg_6581" s="T718">emi͡e</ta>
            <ta e="T720" id="Seg_6582" s="T719">emi͡e</ta>
            <ta e="T721" id="Seg_6583" s="T720">hɨraj-a</ta>
            <ta e="T724" id="Seg_6584" s="T723">inn-i-tten</ta>
            <ta e="T725" id="Seg_6585" s="T724">kim-niː-l-ler</ta>
            <ta e="T726" id="Seg_6586" s="T725">bu͡o</ta>
            <ta e="T729" id="Seg_6587" s="T728">nʼemʼes-ter</ta>
            <ta e="T730" id="Seg_6588" s="T729">bugurduk</ta>
            <ta e="T731" id="Seg_6589" s="T730">hɨt-al-lar</ta>
            <ta e="T733" id="Seg_6590" s="T732">gɨn-a</ta>
            <ta e="T734" id="Seg_6591" s="T733">hɨt-al-lar</ta>
            <ta e="T735" id="Seg_6592" s="T734">bu͡o</ta>
            <ta e="T736" id="Seg_6593" s="T735">a</ta>
            <ta e="T737" id="Seg_6594" s="T736">bihigi</ta>
            <ta e="T738" id="Seg_6595" s="T737">bu</ta>
            <ta e="T739" id="Seg_6596" s="T738">dʼi͡eg-i-tten</ta>
            <ta e="T740" id="Seg_6597" s="T739">emi͡e</ta>
            <ta e="T741" id="Seg_6598" s="T740">ɨt-ɨ͡al-a-h-a-bɨt</ta>
            <ta e="T742" id="Seg_6599" s="T741">dʼe</ta>
            <ta e="T743" id="Seg_6600" s="T742">onton</ta>
            <ta e="T744" id="Seg_6601" s="T743">bu</ta>
            <ta e="T745" id="Seg_6602" s="T744">tüːn-ü</ta>
            <ta e="T746" id="Seg_6603" s="T745">huptu</ta>
            <ta e="T747" id="Seg_6604" s="T746">hɨt-ɨː</ta>
            <ta e="T748" id="Seg_6605" s="T747">munna</ta>
            <ta e="T749" id="Seg_6606" s="T748">bu͡o</ta>
            <ta e="T750" id="Seg_6607" s="T749">tüːn-ü</ta>
            <ta e="T751" id="Seg_6608" s="T750">u͡on-nar</ta>
            <ta e="T752" id="Seg_6609" s="T751">duː</ta>
            <ta e="T753" id="Seg_6610" s="T752">ikki-ler</ta>
            <ta e="T754" id="Seg_6611" s="T753">di͡ek</ta>
            <ta e="T755" id="Seg_6612" s="T754">duː</ta>
            <ta e="T756" id="Seg_6613" s="T755">ili</ta>
            <ta e="T757" id="Seg_6614" s="T756">čʼaːs-tar</ta>
            <ta e="T758" id="Seg_6615" s="T757">di͡ek</ta>
            <ta e="T759" id="Seg_6616" s="T758">duː</ta>
            <ta e="T760" id="Seg_6617" s="T759">ol</ta>
            <ta e="T761" id="Seg_6618" s="T760">rakʼet-ɨ</ta>
            <ta e="T762" id="Seg_6619" s="T761">ɨːt-al-lar</ta>
            <ta e="T763" id="Seg_6620" s="T762">rakʼet-ɨ</ta>
            <ta e="T764" id="Seg_6621" s="T763">ɨːt-tag-ɨna</ta>
            <ta e="T765" id="Seg_6622" s="T764">oččogo</ta>
            <ta e="T766" id="Seg_6623" s="T765">bu</ta>
            <ta e="T767" id="Seg_6624" s="T766">barɨta</ta>
            <ta e="T769" id="Seg_6625" s="T768">kim-n-iː</ta>
            <ta e="T770" id="Seg_6626" s="T769">hɨt-al-lar</ta>
            <ta e="T771" id="Seg_6627" s="T770">barɨta</ta>
            <ta e="T772" id="Seg_6628" s="T771">köst-öl-lör</ta>
            <ta e="T773" id="Seg_6629" s="T772">iliː-bi-n</ta>
            <ta e="T774" id="Seg_6630" s="T773">tap-tar-an</ta>
            <ta e="T775" id="Seg_6631" s="T774">bu</ta>
            <ta e="T776" id="Seg_6632" s="T775">dʼüːleː-be-ti-m</ta>
            <ta e="T777" id="Seg_6633" s="T776">onton</ta>
            <ta e="T778" id="Seg_6634" s="T777">ol</ta>
            <ta e="T779" id="Seg_6635" s="T778">mun-tu-ba-r</ta>
            <ta e="T780" id="Seg_6636" s="T779">tap</ta>
            <ta e="T781" id="Seg_6637" s="T780">kim-neː-bit</ta>
            <ta e="T782" id="Seg_6638" s="T781">ol</ta>
            <ta e="T783" id="Seg_6639" s="T782">huptu</ta>
            <ta e="T784" id="Seg_6640" s="T783">köt-ön</ta>
            <ta e="T785" id="Seg_6641" s="T784">biːr</ta>
            <ta e="T786" id="Seg_6642" s="T785">mina</ta>
            <ta e="T787" id="Seg_6643" s="T786">kel-l-e</ta>
            <ta e="T788" id="Seg_6644" s="T787">da</ta>
            <ta e="T789" id="Seg_6645" s="T788">barɨ-bɨtɨ-n</ta>
            <ta e="T790" id="Seg_6646" s="T789">holoː-but</ta>
            <ta e="T791" id="Seg_6647" s="T790">ol</ta>
            <ta e="T792" id="Seg_6648" s="T791">u͡on</ta>
            <ta e="T793" id="Seg_6649" s="T792">orduk</ta>
            <ta e="T794" id="Seg_6650" s="T793">kihi-ni</ta>
            <ta e="T795" id="Seg_6651" s="T794">haː-bɨ-n</ta>
            <ta e="T796" id="Seg_6652" s="T795">burtuk</ta>
            <ta e="T797" id="Seg_6653" s="T796">tut-ar-ba-r</ta>
            <ta e="T798" id="Seg_6654" s="T797">öj-dön-nü-m</ta>
            <ta e="T799" id="Seg_6655" s="T798">onton</ta>
            <ta e="T800" id="Seg_6656" s="T799">innʼe</ta>
            <ta e="T801" id="Seg_6657" s="T800">innʼe</ta>
            <ta e="T802" id="Seg_6658" s="T801">tur-a-bɨn</ta>
            <ta e="T803" id="Seg_6659" s="T802">tu͡ok</ta>
            <ta e="T804" id="Seg_6660" s="T803">tobuk-tuː</ta>
            <ta e="T805" id="Seg_6661" s="T804">dʼe</ta>
            <ta e="T806" id="Seg_6662" s="T805">onton</ta>
            <ta e="T807" id="Seg_6663" s="T806">tüs-tü-m</ta>
            <ta e="T808" id="Seg_6664" s="T807">bugurduk</ta>
            <ta e="T809" id="Seg_6665" s="T808">dʼe</ta>
            <ta e="T810" id="Seg_6666" s="T809">onton</ta>
            <ta e="T811" id="Seg_6667" s="T810">kojut</ta>
            <ta e="T812" id="Seg_6668" s="T811">kör-dü-m</ta>
            <ta e="T813" id="Seg_6669" s="T812">bu͡o</ta>
            <ta e="T814" id="Seg_6670" s="T813">čömüje-ler-i-m</ta>
            <ta e="T815" id="Seg_6671" s="T814">barɨta</ta>
            <ta e="T816" id="Seg_6672" s="T815">halɨbɨr-ɨː</ta>
            <ta e="T817" id="Seg_6673" s="T816">hɨldʼ-al-lar</ta>
            <ta e="T818" id="Seg_6674" s="T817">kaːn-ɨ-m</ta>
            <ta e="T819" id="Seg_6675" s="T818">ust-a</ta>
            <ta e="T820" id="Seg_6676" s="T819">hɨt-ar</ta>
            <ta e="T821" id="Seg_6677" s="T820">bugurduk</ta>
            <ta e="T822" id="Seg_6678" s="T821">dʼe</ta>
            <ta e="T823" id="Seg_6679" s="T822">onton</ta>
            <ta e="T824" id="Seg_6680" s="T823">kur-bu-n</ta>
            <ta e="T825" id="Seg_6681" s="T824">uhul-lu-m</ta>
            <ta e="T826" id="Seg_6682" s="T825">bugurduk</ta>
            <ta e="T827" id="Seg_6683" s="T826">kur-bu-n</ta>
            <ta e="T828" id="Seg_6684" s="T827">uhul-an</ta>
            <ta e="T829" id="Seg_6685" s="T828">bar-aːm-mɨn</ta>
            <ta e="T830" id="Seg_6686" s="T829">burtuk</ta>
            <ta e="T831" id="Seg_6687" s="T830">kim-neː-ti-m</ta>
            <ta e="T832" id="Seg_6688" s="T831">enʼi</ta>
            <ta e="T833" id="Seg_6689" s="T832">kim-me-r</ta>
            <ta e="T834" id="Seg_6690" s="T833">mu͡oj-ba-r</ta>
            <ta e="T835" id="Seg_6691" s="T834">innʼe</ta>
            <ta e="T838" id="Seg_6692" s="T837">kim-neː-ti-m</ta>
            <ta e="T839" id="Seg_6693" s="T838">bu</ta>
            <ta e="T840" id="Seg_6694" s="T839">baːj-an</ta>
            <ta e="T841" id="Seg_6695" s="T840">bar-aːm-mɨn</ta>
            <ta e="T842" id="Seg_6696" s="T841">onton</ta>
            <ta e="T843" id="Seg_6697" s="T842">bu</ta>
            <ta e="T844" id="Seg_6698" s="T843">pakʼet-tar</ta>
            <ta e="T845" id="Seg_6699" s="T844">bu͡ol-aːččɨ-lar</ta>
            <ta e="T848" id="Seg_6700" s="T847">dʼe</ta>
            <ta e="T849" id="Seg_6701" s="T848">on-tu-bu-nan</ta>
            <ta e="T850" id="Seg_6702" s="T849">erij-di-m</ta>
            <ta e="T851" id="Seg_6703" s="T850">bugurduk</ta>
            <ta e="T852" id="Seg_6704" s="T851">onton</ta>
            <ta e="T853" id="Seg_6705" s="T852">ommu͡otka</ta>
            <ta e="T854" id="Seg_6706" s="T853">iti</ta>
            <ta e="T855" id="Seg_6707" s="T854">bočinka-gar</ta>
            <ta e="T856" id="Seg_6708" s="T855">kim</ta>
            <ta e="T857" id="Seg_6709" s="T856">bu͡ol-aːččɨ</ta>
            <ta e="T860" id="Seg_6710" s="T859">o-nu</ta>
            <ta e="T861" id="Seg_6711" s="T860">ommotka-bɨ-nan</ta>
            <ta e="T862" id="Seg_6712" s="T861">erij-di-m</ta>
            <ta e="T863" id="Seg_6713" s="T862">bar</ta>
            <ta e="T864" id="Seg_6714" s="T863">diː-l-ler</ta>
            <ta e="T865" id="Seg_6715" s="T864">kim</ta>
            <ta e="T866" id="Seg_6716" s="T865">kamandʼir</ta>
            <ta e="T867" id="Seg_6717" s="T866">rota-gar</ta>
            <ta e="T868" id="Seg_6718" s="T867">hol</ta>
            <ta e="T869" id="Seg_6719" s="T868">ün-en</ta>
            <ta e="T870" id="Seg_6720" s="T869">üm-müt-ü-nen</ta>
            <ta e="T871" id="Seg_6721" s="T870">tij-di-m</ta>
            <ta e="T872" id="Seg_6722" s="T871">on-tu-ba-r</ta>
            <ta e="T873" id="Seg_6723" s="T872">kilep</ta>
            <ta e="T874" id="Seg_6724" s="T873">kördöː-n</ta>
            <ta e="T875" id="Seg_6725" s="T874">eː</ta>
            <ta e="T878" id="Seg_6726" s="T877">as</ta>
            <ta e="T879" id="Seg_6727" s="T878">kaja</ta>
            <ta e="T880" id="Seg_6728" s="T879">diː-r</ta>
            <ta e="T881" id="Seg_6729" s="T880">ran-iː</ta>
            <ta e="T882" id="Seg_6730" s="T881">gɨn</ta>
            <ta e="T883" id="Seg_6731" s="T930">duː</ta>
            <ta e="T885" id="Seg_6732" s="T883">diː-r</ta>
            <ta e="T886" id="Seg_6733" s="T885">hmm</ta>
            <ta e="T887" id="Seg_6734" s="T886">d-iː-bin</ta>
            <ta e="T888" id="Seg_6735" s="T887">haː-bɨ-n</ta>
            <ta e="T889" id="Seg_6736" s="T888">onton</ta>
            <ta e="T890" id="Seg_6737" s="T889">ɨl-bɨt</ta>
            <ta e="T891" id="Seg_6738" s="T890">e-t-e</ta>
            <ta e="T892" id="Seg_6739" s="T891">ol</ta>
            <ta e="T893" id="Seg_6740" s="T892">kim-i-m</ta>
            <ta e="T894" id="Seg_6741" s="T893">kamandʼir-ba-r</ta>
            <ta e="T895" id="Seg_6742" s="T894">sdavaj-d-ɨː-bɨn</ta>
            <ta e="T896" id="Seg_6743" s="T895">bu͡o</ta>
            <ta e="T897" id="Seg_6744" s="T896">kim-ŋe</ta>
            <ta e="T898" id="Seg_6745" s="T897">nomʼer-ɨ-nan</ta>
            <ta e="T899" id="Seg_6746" s="T898">bu͡ol-aːččɨ</ta>
            <ta e="T900" id="Seg_6747" s="T899">e-ti-lere</ta>
            <ta e="T901" id="Seg_6748" s="T900">on-tu-m</ta>
            <ta e="T902" id="Seg_6749" s="T901">bu͡o</ta>
            <ta e="T903" id="Seg_6750" s="T902">kim</ta>
            <ta e="T904" id="Seg_6751" s="T903">patru͡ot-tar</ta>
            <ta e="T905" id="Seg_6752" s="T904">karmaːm-ma-r</ta>
            <ta e="T906" id="Seg_6753" s="T905">tu͡ok</ta>
            <ta e="T907" id="Seg_6754" s="T906">daː</ta>
            <ta e="T908" id="Seg_6755" s="T907">hu͡ok</ta>
            <ta e="T909" id="Seg_6756" s="T908">granaːt-tar-bɨ-n</ta>
            <ta e="T910" id="Seg_6757" s="T909">emi͡e</ta>
            <ta e="T911" id="Seg_6758" s="T910">ɨl-bɨt-tar</ta>
            <ta e="T912" id="Seg_6759" s="T911">oku͡opa-ga</ta>
            <ta e="T913" id="Seg_6760" s="T912">hɨt-tak-pɨna</ta>
            <ta e="T914" id="Seg_6761" s="T913">kumak</ta>
            <ta e="T915" id="Seg_6762" s="T914">tüh-eːktiː-r</ta>
            <ta e="T916" id="Seg_6763" s="T915">diːn</ta>
            <ta e="T917" id="Seg_6764" s="T916">ol</ta>
            <ta e="T918" id="Seg_6765" s="T917">di͡egi-tten</ta>
            <ta e="T919" id="Seg_6766" s="T918">kel-e</ta>
            <ta e="T920" id="Seg_6767" s="T919">ite</ta>
            <ta e="T921" id="Seg_6768" s="T920">miːna-lar</ta>
            <ta e="T922" id="Seg_6769" s="T921">tüs-tek-terine</ta>
            <ta e="T923" id="Seg_6770" s="T922">hir-i-m</ta>
            <ta e="T924" id="Seg_6771" s="T923">dorguj-ar</ta>
            <ta e="T929" id="Seg_6772" s="T928">kim-niː-l-ler</ta>
            <ta e="T931" id="Seg_6773" s="T929">araj</ta>
            <ta e="T932" id="Seg_6774" s="T931">on-tu-m</ta>
            <ta e="T933" id="Seg_6775" s="T932">bu</ta>
            <ta e="T934" id="Seg_6776" s="T933">karda</ta>
            <ta e="T935" id="Seg_6777" s="T937">zapаlnʼenʼije</ta>
            <ta e="T936" id="Seg_6778" s="T935">kel-er</ta>
            <ta e="T938" id="Seg_6779" s="T936">e-bit</ta>
            <ta e="T939" id="Seg_6780" s="T938">bihigi</ta>
            <ta e="T940" id="Seg_6781" s="T939">onnu-butu-gar</ta>
            <ta e="T941" id="Seg_6782" s="T940">össü͡ö</ta>
            <ta e="T942" id="Seg_6783" s="T941">nöŋü͡ö-ler</ta>
            <ta e="T943" id="Seg_6784" s="T942">kel-el-ler</ta>
            <ta e="T944" id="Seg_6785" s="T943">bu͡ol-ar</ta>
            <ta e="T945" id="Seg_6786" s="T944">aːjtaːn</ta>
            <ta e="T946" id="Seg_6787" s="T945">bu͡ol-aːktɨː-r</ta>
            <ta e="T947" id="Seg_6788" s="T946">diːn</ta>
            <ta e="T948" id="Seg_6789" s="T947">onton</ta>
            <ta e="T949" id="Seg_6790" s="T948">ɨt-a-h-al-lar</ta>
            <ta e="T950" id="Seg_6791" s="T949">bu͡ol-al-lar</ta>
            <ta e="T951" id="Seg_6792" s="T950">araj</ta>
            <ta e="T952" id="Seg_6793" s="T951">horok-horok</ta>
            <ta e="T953" id="Seg_6794" s="T952">ɨnčɨktɨː-r</ta>
            <ta e="T954" id="Seg_6795" s="T953">bu͡ol-ar</ta>
            <ta e="T955" id="Seg_6796" s="T954">ɨnčɨktɨː-r</ta>
            <ta e="T956" id="Seg_6797" s="T955">bu͡ol-lak-tarɨna</ta>
            <ta e="T957" id="Seg_6798" s="T956">min</ta>
            <ta e="T958" id="Seg_6799" s="T957">emi͡e</ta>
            <ta e="T959" id="Seg_6800" s="T958">emi͡e</ta>
            <ta e="T960" id="Seg_6801" s="T959">ɨnčɨkt-ɨː-bɨn</ta>
            <ta e="T961" id="Seg_6802" s="T960">emi͡e</ta>
            <ta e="T962" id="Seg_6803" s="T961">onton</ta>
            <ta e="T963" id="Seg_6804" s="T962">emi͡e</ta>
            <ta e="T964" id="Seg_6805" s="T963">dʼɨlɨ-s</ta>
            <ta e="T965" id="Seg_6806" s="T964">gɨn-a-bɨn</ta>
            <ta e="T966" id="Seg_6807" s="T965">ihill-iː-bin</ta>
            <ta e="T967" id="Seg_6808" s="T966">sančʼas-ka</ta>
            <ta e="T968" id="Seg_6809" s="T967">kel-bit-i-m</ta>
            <ta e="T971" id="Seg_6810" s="T970">pаlʼevoj</ta>
            <ta e="T972" id="Seg_6811" s="T971">üle</ta>
            <ta e="T973" id="Seg_6812" s="T972">kim</ta>
            <ta e="T974" id="Seg_6813" s="T973">bu͡ol-aːččɨ</ta>
            <ta e="T975" id="Seg_6814" s="T974">sančʼas</ta>
            <ta e="T976" id="Seg_6815" s="T975">oːl</ta>
            <ta e="T977" id="Seg_6816" s="T976">sančʼas-ka</ta>
            <ta e="T978" id="Seg_6817" s="T977">kel-li</ta>
            <ta e="T979" id="Seg_6818" s="T978">eː</ta>
            <ta e="T980" id="Seg_6819" s="T979">kel-li-m</ta>
            <ta e="T981" id="Seg_6820" s="T980">da</ta>
            <ta e="T982" id="Seg_6821" s="T981">tɨl-ga</ta>
            <ta e="T983" id="Seg_6822" s="T982">ɨːp-pɨt-tara</ta>
            <ta e="T984" id="Seg_6823" s="T983">Ivanava Suja</ta>
            <ta e="T985" id="Seg_6824" s="T984">hɨp-pɨt-ɨ-m</ta>
            <ta e="T986" id="Seg_6825" s="T985">erge</ta>
            <ta e="T987" id="Seg_6826" s="T986">hospitalʼ-ga</ta>
            <ta e="T988" id="Seg_6827" s="T987">onton</ta>
            <ta e="T991" id="Seg_6828" s="T990">eː</ta>
            <ta e="T992" id="Seg_6829" s="T991">srazu</ta>
            <ta e="T993" id="Seg_6830" s="T992">bi͡es</ta>
            <ta e="T994" id="Seg_6831" s="T993">ɨj-ɨ</ta>
            <ta e="T995" id="Seg_6832" s="T994">hɨt-tɨ-m</ta>
            <ta e="T996" id="Seg_6833" s="T995">da</ta>
            <ta e="T997" id="Seg_6834" s="T996">dʼi͡e-m</ta>
            <ta e="T998" id="Seg_6835" s="T997">di͡ek</ta>
            <ta e="T999" id="Seg_6836" s="T998">ɨːp-pɨt-tara</ta>
         </annotation>
         <annotation name="mp" tierref="mp-ChVD">
            <ta e="T1" id="Seg_6837" s="T0">Dudʼinka-GA</ta>
            <ta e="T2" id="Seg_6838" s="T1">kel-BIT-I-m</ta>
            <ta e="T6" id="Seg_6839" s="T5">avgust</ta>
            <ta e="T7" id="Seg_6840" s="T6">ɨj-GA</ta>
            <ta e="T11" id="Seg_6841" s="T10">kamʼisʼsʼija-nI</ta>
            <ta e="T12" id="Seg_6842" s="T11">praxad-A</ta>
            <ta e="T13" id="Seg_6843" s="T12">gɨn-TI-m</ta>
            <ta e="T14" id="Seg_6844" s="T13">nʼestrajevoj-GA</ta>
            <ta e="T15" id="Seg_6845" s="T14">Norilskaj-GA</ta>
            <ta e="T16" id="Seg_6846" s="T15">ɨːt-BIT-LArA</ta>
            <ta e="T17" id="Seg_6847" s="T16">Norilskaj-ttAn</ta>
            <ta e="T18" id="Seg_6848" s="T17">kim-GA</ta>
            <ta e="T19" id="Seg_6849" s="T18">eː</ta>
            <ta e="T9" id="Seg_6850" s="T19">vajenʼizʼirovannɨj </ta>
            <ta e="T20" id="Seg_6851" s="T9">axrana-GA</ta>
            <ta e="T21" id="Seg_6852" s="T20">üleleː-A</ta>
            <ta e="T22" id="Seg_6853" s="T21">tüs-TI-m</ta>
            <ta e="T23" id="Seg_6854" s="T22">dabravolʼes-I-LAr-nI</ta>
            <ta e="T24" id="Seg_6855" s="T23">kɨtta</ta>
            <ta e="T25" id="Seg_6856" s="T24">emi͡e</ta>
            <ta e="T26" id="Seg_6857" s="T25">ka</ta>
            <ta e="T27" id="Seg_6858" s="T26">tübes-BIT-I-m</ta>
            <ta e="T28" id="Seg_6859" s="T27">Krasnajarskaj-GA</ta>
            <ta e="T29" id="Seg_6860" s="T28">eː</ta>
            <ta e="T30" id="Seg_6861" s="T29">di͡eri</ta>
            <ta e="T31" id="Seg_6862" s="T30">bar-BIT-I-m</ta>
            <ta e="T32" id="Seg_6863" s="T31">Spartak</ta>
            <ta e="T33" id="Seg_6864" s="T32">poroku͡ot-tI-nAn</ta>
            <ta e="T34" id="Seg_6865" s="T33">onton</ta>
            <ta e="T35" id="Seg_6866" s="T34">ü͡ören-TI-m</ta>
            <ta e="T36" id="Seg_6867" s="T35">Krasnajarskaj</ta>
            <ta e="T37" id="Seg_6868" s="T36">pʼexoːta-GA</ta>
            <ta e="T41" id="Seg_6869" s="T40">u͡on</ta>
            <ta e="T42" id="Seg_6870" s="T41">biːr</ta>
            <ta e="T43" id="Seg_6871" s="T42">ɨj-nI</ta>
            <ta e="T44" id="Seg_6872" s="T43">du͡o</ta>
            <ta e="T45" id="Seg_6873" s="T44">kas-nI</ta>
            <ta e="T46" id="Seg_6874" s="T45">du͡o</ta>
            <ta e="T47" id="Seg_6875" s="T46">ü͡ören-An</ta>
            <ta e="T48" id="Seg_6876" s="T47">baran</ta>
            <ta e="T49" id="Seg_6877" s="T48">emi͡e</ta>
            <ta e="T50" id="Seg_6878" s="T49">kattaː-A</ta>
            <ta e="T51" id="Seg_6879" s="T54">kim-GA</ta>
            <ta e="T52" id="Seg_6880" s="T51">Zvʼenʼigu͡orat-GA</ta>
            <ta e="T56" id="Seg_6881" s="T55">parasut</ta>
            <ta e="T57" id="Seg_6882" s="T56">razbiraj-LAː-AːččI</ta>
            <ta e="T58" id="Seg_6883" s="T57">e-TI-m</ta>
            <ta e="T60" id="Seg_6884" s="T59">dʼe</ta>
            <ta e="T61" id="Seg_6885" s="T60">onno</ta>
            <ta e="T62" id="Seg_6886" s="T61">uhun</ta>
            <ta e="T63" id="Seg_6887" s="T62">da</ta>
            <ta e="T64" id="Seg_6888" s="T63">ü͡ören-BAtAK-I-m</ta>
            <ta e="T65" id="Seg_6889" s="T64">dogor-LAr-I-m</ta>
            <ta e="T66" id="Seg_6890" s="T65">kɨtta</ta>
            <ta e="T67" id="Seg_6891" s="T66">bu</ta>
            <ta e="T68" id="Seg_6892" s="T67">kaːl-I-m-AːrI-BIn</ta>
            <ta e="T69" id="Seg_6893" s="T68">biːrge</ta>
            <ta e="T70" id="Seg_6894" s="T69">bar-s-I-BIT-I-m</ta>
            <ta e="T71" id="Seg_6895" s="T70">dʼe</ta>
            <ta e="T72" id="Seg_6896" s="T71">ol</ta>
            <ta e="T73" id="Seg_6897" s="T72">kim-LAː-TI-m</ta>
            <ta e="T74" id="Seg_6898" s="T73">kim-GA</ta>
            <ta e="T75" id="Seg_6899" s="T74">Muːrom-GA</ta>
            <ta e="T76" id="Seg_6900" s="T75">ü͡ören-A</ta>
            <ta e="T77" id="Seg_6901" s="T76">kim-LAː-TI-BIt</ta>
            <ta e="T481" id="Seg_6902" s="T80">prosta</ta>
            <ta e="T81" id="Seg_6903" s="T481">kim-LAː-Ar</ta>
            <ta e="T82" id="Seg_6904" s="T81">e-TI-LArA</ta>
            <ta e="T83" id="Seg_6905" s="T82">onno</ta>
            <ta e="T84" id="Seg_6906" s="T83">farmʼiravanʼije</ta>
            <ta e="T85" id="Seg_6907" s="T84">oŋor-Ar</ta>
            <ta e="T86" id="Seg_6908" s="T85">e-TI-LArA</ta>
            <ta e="T92" id="Seg_6909" s="T91">tu͡ok-GA</ta>
            <ta e="T93" id="Seg_6910" s="T92">ü͡ören-BIT-GI-n</ta>
            <ta e="T94" id="Seg_6911" s="T93">kim-LAːK</ta>
            <ta e="T95" id="Seg_6912" s="T94">kim</ta>
            <ta e="T96" id="Seg_6913" s="T95">kim-LAː-Ar-LAr</ta>
            <ta e="T97" id="Seg_6914" s="T96">onton</ta>
            <ta e="T101" id="Seg_6915" s="T100">dʼe</ta>
            <ta e="T102" id="Seg_6916" s="T101">onno</ta>
            <ta e="T103" id="Seg_6917" s="T102">kim-LAː-TI-m</ta>
            <ta e="T104" id="Seg_6918" s="T103">da</ta>
            <ta e="T105" id="Seg_6919" s="T104">Muːrom-ttAn</ta>
            <ta e="T106" id="Seg_6920" s="T105">kim-GA</ta>
            <ta e="T107" id="Seg_6921" s="T106">tübes-TI-BIt</ta>
            <ta e="T108" id="Seg_6922" s="T107">ol</ta>
            <ta e="T109" id="Seg_6923" s="T108">Tuːla-GA</ta>
            <ta e="T110" id="Seg_6924" s="T109">ol</ta>
            <ta e="T111" id="Seg_6925" s="T110">Tuːla</ta>
            <ta e="T115" id="Seg_6926" s="T114">bu͡o</ta>
            <ta e="T116" id="Seg_6927" s="T115">agɨs</ta>
            <ta e="T117" id="Seg_6928" s="T116">kilometr-nI</ta>
            <ta e="T118" id="Seg_6929" s="T117">du͡o</ta>
            <ta e="T119" id="Seg_6930" s="T118">kas-nI</ta>
            <ta e="T120" id="Seg_6931" s="T119">du͡o</ta>
            <ta e="T121" id="Seg_6932" s="T120">bar-An</ta>
            <ta e="T122" id="Seg_6933" s="T121">baran</ta>
            <ta e="T123" id="Seg_6934" s="T122">timir</ta>
            <ta e="T124" id="Seg_6935" s="T123">orok-LAr-I-ŋ</ta>
            <ta e="T125" id="Seg_6936" s="T124">barɨta</ta>
            <ta e="T126" id="Seg_6937" s="T125">aldʼan-BIT-LArA</ta>
            <ta e="T127" id="Seg_6938" s="T126">bu͡ol-TI-tA</ta>
            <ta e="T128" id="Seg_6939" s="T127">dʼe</ta>
            <ta e="T129" id="Seg_6940" s="T128">onno</ta>
            <ta e="T130" id="Seg_6941" s="T129">kim-LAː-A</ta>
            <ta e="T131" id="Seg_6942" s="T130">hɨt-TAK-BItInA</ta>
            <ta e="T132" id="Seg_6943" s="T131">uže</ta>
            <ta e="T134" id="Seg_6944" s="T133">bu͡o</ta>
            <ta e="T135" id="Seg_6945" s="T134">bi͡ek</ta>
            <ta e="T136" id="Seg_6946" s="T135">köt-A</ta>
            <ta e="T139" id="Seg_6947" s="T138">köt-AːččI</ta>
            <ta e="T140" id="Seg_6948" s="T139">e-TI-LArA</ta>
            <ta e="T141" id="Seg_6949" s="T140">harsi͡erda</ta>
            <ta e="T142" id="Seg_6950" s="T141">aːjɨ</ta>
            <ta e="T143" id="Seg_6951" s="T142">dʼe</ta>
            <ta e="T144" id="Seg_6952" s="T143">onton</ta>
            <ta e="T145" id="Seg_6953" s="T144">uhun</ta>
            <ta e="T146" id="Seg_6954" s="T145">da</ta>
            <ta e="T147" id="Seg_6955" s="T146">bu͡ol-TI-tA</ta>
            <ta e="T148" id="Seg_6956" s="T147">eː</ta>
            <ta e="T151" id="Seg_6957" s="T150">kim-LAː-A-BIt</ta>
            <ta e="T152" id="Seg_6958" s="T151">onno</ta>
            <ta e="T153" id="Seg_6959" s="T152">ile</ta>
            <ta e="T154" id="Seg_6960" s="T153">hatɨː</ta>
            <ta e="T155" id="Seg_6961" s="T154">bar-BIT-BIt</ta>
            <ta e="T156" id="Seg_6962" s="T155">ol</ta>
            <ta e="T157" id="Seg_6963" s="T156">kim-GA</ta>
            <ta e="T158" id="Seg_6964" s="T157">kanna</ta>
            <ta e="T161" id="Seg_6965" s="T160">hol</ta>
            <ta e="T162" id="Seg_6966" s="T161">vajna</ta>
            <ta e="T163" id="Seg_6967" s="T162">bu͡ol-IAK</ta>
            <ta e="T164" id="Seg_6968" s="T163">hir-tA</ta>
            <ta e="T165" id="Seg_6969" s="T164">bu͡ol-IAK.[tI]-r</ta>
            <ta e="T166" id="Seg_6970" s="T165">di͡eri</ta>
            <ta e="T167" id="Seg_6971" s="T166">dʼe</ta>
            <ta e="T168" id="Seg_6972" s="T167">onno</ta>
            <ta e="T169" id="Seg_6973" s="T168">kim-LAː-AːččI-LAr</ta>
            <ta e="T170" id="Seg_6974" s="T169">bu͡o</ta>
            <ta e="T171" id="Seg_6975" s="T170">kim</ta>
            <ta e="T172" id="Seg_6976" s="T171">partʼizanskɨj</ta>
            <ta e="T173" id="Seg_6977" s="T172">iti</ta>
            <ta e="T174" id="Seg_6978" s="T173">аtrʼad</ta>
            <ta e="T175" id="Seg_6979" s="T174">o</ta>
            <ta e="T176" id="Seg_6980" s="T175">kim-GA</ta>
            <ta e="T177" id="Seg_6981" s="T176">NOP</ta>
            <ta e="T178" id="Seg_6982" s="T177">SOP</ta>
            <ta e="T179" id="Seg_6983" s="T178">abaznačaj-LAː-Ar-LAr</ta>
            <ta e="T180" id="Seg_6984" s="T179">ol-nI</ta>
            <ta e="T181" id="Seg_6985" s="T180">da</ta>
            <ta e="T182" id="Seg_6986" s="T181">bu</ta>
            <ta e="T183" id="Seg_6987" s="T182">munʼaː</ta>
            <ta e="T184" id="Seg_6988" s="T183">kim-LAː</ta>
            <ta e="T185" id="Seg_6989" s="T184">kim-LAː-Ar-GA</ta>
            <ta e="T186" id="Seg_6990" s="T185">ürek</ta>
            <ta e="T187" id="Seg_6991" s="T186">tu͡ok</ta>
            <ta e="T188" id="Seg_6992" s="T187">ere</ta>
            <ta e="T189" id="Seg_6993" s="T188">ürek-kAːN</ta>
            <ta e="T190" id="Seg_6994" s="T189">hɨt-Ar</ta>
            <ta e="T191" id="Seg_6995" s="T190">dʼe</ta>
            <ta e="T192" id="Seg_6996" s="T191">onno</ta>
            <ta e="T193" id="Seg_6997" s="T192">deri͡ebine-GA</ta>
            <ta e="T194" id="Seg_6998" s="T193">onno</ta>
            <ta e="T196" id="Seg_6999" s="T195">gɨn-Ar-LAr</ta>
            <ta e="T197" id="Seg_7000" s="T196">eni</ta>
            <ta e="T198" id="Seg_7001" s="T197">harsi͡erda</ta>
            <ta e="T199" id="Seg_7002" s="T198">kim-LAː-TI-BIt</ta>
            <ta e="T200" id="Seg_7003" s="T199">ahaː-A</ta>
            <ta e="T201" id="Seg_7004" s="T200">ahaː-A</ta>
            <ta e="T202" id="Seg_7005" s="T201">tüs-An</ta>
            <ta e="T203" id="Seg_7006" s="T202">bar-An-BIt</ta>
            <ta e="T204" id="Seg_7007" s="T203">ol-nI</ta>
            <ta e="T205" id="Seg_7008" s="T204">dʼe</ta>
            <ta e="T206" id="Seg_7009" s="T205">ɨt-IAlAː-A-s-Ar-LAr</ta>
            <ta e="T207" id="Seg_7010" s="T206">kamandʼir-BItI-n</ta>
            <ta e="T208" id="Seg_7011" s="T207">kepseː-Ar</ta>
            <ta e="T209" id="Seg_7012" s="T208">kajdak</ta>
            <ta e="T212" id="Seg_7013" s="T211">kim-LAː-IAK-GItI-n</ta>
            <ta e="T214" id="Seg_7014" s="T212">onton</ta>
            <ta e="T215" id="Seg_7015" s="T214">dʼe</ta>
            <ta e="T216" id="Seg_7016" s="T215">аkruzeːnije</ta>
            <ta e="T217" id="Seg_7017" s="T216">tübes-TAK-GItInA</ta>
            <ta e="T218" id="Seg_7018" s="T217">bagar</ta>
            <ta e="T219" id="Seg_7019" s="T218">plʼen</ta>
            <ta e="T220" id="Seg_7020" s="T219">tübes-TAK-GItInA</ta>
            <ta e="T221" id="Seg_7021" s="T220">itte</ta>
            <ta e="T223" id="Seg_7022" s="T222">kim</ta>
            <ta e="T224" id="Seg_7023" s="T223">kim-LAː-m-IAK-GI-n</ta>
            <ta e="T225" id="Seg_7024" s="T224">naːda</ta>
            <ta e="T226" id="Seg_7025" s="T225">bu͡ol-TI-tA</ta>
            <ta e="T227" id="Seg_7026" s="T226">beje-GI-n</ta>
            <ta e="T228" id="Seg_7027" s="T227">bi͡er-I-m-IAK-GI-n</ta>
            <ta e="T229" id="Seg_7028" s="T228">bagar</ta>
            <ta e="T230" id="Seg_7029" s="T229">prosta</ta>
            <ta e="T233" id="Seg_7030" s="T232">kaj-ItAlAː-BA-ŋ</ta>
            <ta e="T234" id="Seg_7031" s="T233">huruk</ta>
            <ta e="T235" id="Seg_7032" s="T234">eː</ta>
            <ta e="T236" id="Seg_7033" s="T235">huruk-LAr-GIt</ta>
            <ta e="T237" id="Seg_7034" s="T236">di͡e-Ar</ta>
            <ta e="T238" id="Seg_7035" s="T237">bu͡olla</ta>
            <ta e="T241" id="Seg_7036" s="T240">kim-LAː-ŋ</ta>
            <ta e="T244" id="Seg_7037" s="T243">e</ta>
            <ta e="T245" id="Seg_7038" s="T244">köm-I-ŋ</ta>
            <ta e="T246" id="Seg_7039" s="T245">du͡o</ta>
            <ta e="T247" id="Seg_7040" s="T246">beje-GIt</ta>
            <ta e="T248" id="Seg_7041" s="T247">hi͡e-ŋ</ta>
            <ta e="T249" id="Seg_7042" s="T248">du͡o</ta>
            <ta e="T250" id="Seg_7043" s="T249">di͡e-AːččI</ta>
            <ta e="T251" id="Seg_7044" s="T250">bugurduk</ta>
            <ta e="T252" id="Seg_7045" s="T251">kaj-ItAlAː-TI-ŋ</ta>
            <ta e="T253" id="Seg_7046" s="T252">da</ta>
            <ta e="T255" id="Seg_7047" s="T253">vsʼo ravno</ta>
            <ta e="T256" id="Seg_7048" s="T255">kili͡ej-LAː</ta>
            <ta e="T257" id="Seg_7049" s="T256">iti</ta>
            <ta e="T258" id="Seg_7050" s="T257">kim-LAː-Ar-LAr</ta>
            <ta e="T259" id="Seg_7051" s="T258">bu͡o</ta>
            <ta e="T260" id="Seg_7052" s="T259">ol</ta>
            <ta e="T261" id="Seg_7053" s="T260">bul-Ar-LAr</ta>
            <ta e="T262" id="Seg_7054" s="T261">bu͡o</ta>
            <ta e="T263" id="Seg_7055" s="T262">ol</ta>
            <ta e="T264" id="Seg_7056" s="T263">ka</ta>
            <ta e="T265" id="Seg_7057" s="T264">orota-ŋ</ta>
            <ta e="T266" id="Seg_7058" s="T265">keːs-A</ta>
            <ta e="T267" id="Seg_7059" s="T266">hüːr-BIT-BIn</ta>
            <ta e="T268" id="Seg_7060" s="T267">biːrde</ta>
            <ta e="T271" id="Seg_7061" s="T270">kannɨk-LArI-n</ta>
            <ta e="T272" id="Seg_7062" s="T271">da</ta>
            <ta e="T273" id="Seg_7063" s="T272">dʼüːlleː-An-BIn</ta>
            <ta e="T274" id="Seg_7064" s="T273">karaŋa-GA</ta>
            <ta e="T275" id="Seg_7065" s="T274">nʼemʼec-LArA</ta>
            <ta e="T276" id="Seg_7066" s="T275">da</ta>
            <ta e="T277" id="Seg_7067" s="T276">kim-LArA</ta>
            <ta e="T278" id="Seg_7068" s="T277">da</ta>
            <ta e="T279" id="Seg_7069" s="T278">dʼe</ta>
            <ta e="T280" id="Seg_7070" s="T279">ol</ta>
            <ta e="T281" id="Seg_7071" s="T280">min</ta>
            <ta e="T282" id="Seg_7072" s="T281">hɨrɨt-An-BIn</ta>
            <ta e="T283" id="Seg_7073" s="T282">kör-TI-m</ta>
            <ta e="T284" id="Seg_7074" s="T283">bi͡es</ta>
            <ta e="T285" id="Seg_7075" s="T284">kihi</ta>
            <ta e="T286" id="Seg_7076" s="T285">hɨrɨt-Ar-LAr</ta>
            <ta e="T287" id="Seg_7077" s="T286">agaj</ta>
            <ta e="T288" id="Seg_7078" s="T287">bugurduk</ta>
            <ta e="T289" id="Seg_7079" s="T288">dʼe</ta>
            <ta e="T290" id="Seg_7080" s="T289">onton</ta>
            <ta e="T291" id="Seg_7081" s="T290">ol</ta>
            <ta e="T292" id="Seg_7082" s="T291">di͡egi-ttAn</ta>
            <ta e="T293" id="Seg_7083" s="T292">nʼemʼec-I-LAr-BIt</ta>
            <ta e="T294" id="Seg_7084" s="T293">kel-Ar-LAr</ta>
            <ta e="T295" id="Seg_7085" s="T294">transej-ttAn</ta>
            <ta e="T296" id="Seg_7086" s="T295">tur-Ar-LAr</ta>
            <ta e="T297" id="Seg_7087" s="T296">töttörü</ta>
            <ta e="T298" id="Seg_7088" s="T297">küreː-IAgIŋ</ta>
            <ta e="T299" id="Seg_7089" s="T298">di͡e-A-di͡e-A</ta>
            <ta e="T300" id="Seg_7090" s="T299">kim-LAː-Ar-LAr</ta>
            <ta e="T301" id="Seg_7091" s="T300">ol</ta>
            <ta e="T302" id="Seg_7092" s="T301">kihi-ŋ</ta>
            <ta e="T303" id="Seg_7093" s="T302">kamandʼir-LAr-LAːK-LAr</ta>
            <ta e="T304" id="Seg_7094" s="T303">ol</ta>
            <ta e="T305" id="Seg_7095" s="T304">dogor-LAr-tA</ta>
            <ta e="T306" id="Seg_7096" s="T305">atɨn-LAr</ta>
            <ta e="T307" id="Seg_7097" s="T306">onton</ta>
            <ta e="T308" id="Seg_7098" s="T307">atɨn</ta>
            <ta e="T309" id="Seg_7099" s="T308">čaːst-ttAn</ta>
            <ta e="T310" id="Seg_7100" s="T309">beje-m</ta>
            <ta e="T313" id="Seg_7101" s="T312">dogor-LAr-BI-n</ta>
            <ta e="T314" id="Seg_7102" s="T313">ol</ta>
            <ta e="T315" id="Seg_7103" s="T314">keːs-A</ta>
            <ta e="T316" id="Seg_7104" s="T315">hüːr-BIT-BIn</ta>
            <ta e="T317" id="Seg_7105" s="T316">eni</ta>
            <ta e="T318" id="Seg_7106" s="T317">keː</ta>
            <ta e="T319" id="Seg_7107" s="T318">du͡o</ta>
            <ta e="T320" id="Seg_7108" s="T319">tu͡ok</ta>
            <ta e="T321" id="Seg_7109" s="T320">du͡o</ta>
            <ta e="T322" id="Seg_7110" s="T321">astupaj-LAː-ŋ</ta>
            <ta e="T323" id="Seg_7111" s="T322">di͡e-Ar</ta>
            <ta e="T327" id="Seg_7112" s="T326">di͡e-Ar</ta>
            <ta e="T328" id="Seg_7113" s="T327">kamandʼir-LArA</ta>
            <ta e="T329" id="Seg_7114" s="T328">onton</ta>
            <ta e="T330" id="Seg_7115" s="T329">kör-TI-m</ta>
            <ta e="T331" id="Seg_7116" s="T330">doː</ta>
            <ta e="T332" id="Seg_7117" s="T331">nʼemʼec-I-LAr-I-ŋ</ta>
            <ta e="T333" id="Seg_7118" s="T332">hubu</ta>
            <ta e="T334" id="Seg_7119" s="T333">kel-An</ta>
            <ta e="T335" id="Seg_7120" s="T334">is-Ar-LAr</ta>
            <ta e="T336" id="Seg_7121" s="T335">tu͡ok-nI</ta>
            <ta e="T337" id="Seg_7122" s="T336">tu͡ok-nI</ta>
            <ta e="T338" id="Seg_7123" s="T337">kadʼɨgɨnas-Ar-LAr</ta>
            <ta e="T339" id="Seg_7124" s="T338">kepset-Ar-LAr</ta>
            <ta e="T340" id="Seg_7125" s="T339">töttörü</ta>
            <ta e="T341" id="Seg_7126" s="T340">hüːr-A-BIn</ta>
            <ta e="T342" id="Seg_7127" s="T341">dogor-LAr-I-m</ta>
            <ta e="T343" id="Seg_7128" s="T342">bar-BIT</ta>
            <ta e="T344" id="Seg_7129" s="T343">hir-LArI-n</ta>
            <ta e="T345" id="Seg_7130" s="T344">dek</ta>
            <ta e="T346" id="Seg_7131" s="T345">bu͡opsa</ta>
            <ta e="T347" id="Seg_7132" s="T346">hurdurgas-Ar-LAr</ta>
            <ta e="T348" id="Seg_7133" s="T347">agaj</ta>
            <ta e="T349" id="Seg_7134" s="T348">itinne</ta>
            <ta e="T350" id="Seg_7135" s="T349">ot-GA</ta>
            <ta e="T351" id="Seg_7136" s="T350">ɨt-IAlAː-Ar-LAr</ta>
            <ta e="T352" id="Seg_7137" s="T351">ile</ta>
            <ta e="T353" id="Seg_7138" s="T352">kim-LAː-AːrI</ta>
            <ta e="T354" id="Seg_7139" s="T353">gɨn-Ar-LAr</ta>
            <ta e="T355" id="Seg_7140" s="T354">bɨhɨːlaːk</ta>
            <ta e="T357" id="Seg_7141" s="T355">onton</ta>
            <ta e="T358" id="Seg_7142" s="T357">plʼen</ta>
            <ta e="T359" id="Seg_7143" s="T358">ɨl-AːrI</ta>
            <ta e="T360" id="Seg_7144" s="T359">uhuk-I-ttAn</ta>
            <ta e="T361" id="Seg_7145" s="T360">transej-GA</ta>
            <ta e="T362" id="Seg_7146" s="T361">kel-TI-m</ta>
            <ta e="T363" id="Seg_7147" s="T362">kel-BIT-I-m</ta>
            <ta e="T364" id="Seg_7148" s="T363">beje-m</ta>
            <ta e="T365" id="Seg_7149" s="T364">dogor</ta>
            <ta e="T366" id="Seg_7150" s="T365">e</ta>
            <ta e="T367" id="Seg_7151" s="T366">orota-BA-r</ta>
            <ta e="T368" id="Seg_7152" s="T367">tübes-BIT-BIn</ta>
            <ta e="T369" id="Seg_7153" s="T368">dʼe</ta>
            <ta e="T370" id="Seg_7154" s="T369">ol</ta>
            <ta e="T371" id="Seg_7155" s="T370">kim-LAː-A-BIn</ta>
            <ta e="T372" id="Seg_7156" s="T371">kaja</ta>
            <ta e="T375" id="Seg_7157" s="T374">pulemʼot-I-nAn</ta>
            <ta e="T376" id="Seg_7158" s="T375">kim-LAː-IAK.[tA]</ta>
            <ta e="T377" id="Seg_7159" s="T376">sraːzu</ta>
            <ta e="T378" id="Seg_7160" s="T377">hüːrbe-ttAn</ta>
            <ta e="T379" id="Seg_7161" s="T378">taksa</ta>
            <ta e="T380" id="Seg_7162" s="T379">kihi-nI</ta>
            <ta e="T381" id="Seg_7163" s="T380">tüs-A-r-TI-LAr</ta>
            <ta e="T382" id="Seg_7164" s="T381">horok-LAr-BIt</ta>
            <ta e="T383" id="Seg_7165" s="T382">töttörü</ta>
            <ta e="T384" id="Seg_7166" s="T383">hüːr-TI-LAr</ta>
            <ta e="T385" id="Seg_7167" s="T384">ol</ta>
            <ta e="T386" id="Seg_7168" s="T385">dаzor</ta>
            <ta e="T387" id="Seg_7169" s="T386">bu͡ol-AːččI</ta>
            <ta e="T390" id="Seg_7170" s="T389">orota-ŋ</ta>
            <ta e="T391" id="Seg_7171" s="T390">bar-IAK-tA</ta>
            <ta e="T392" id="Seg_7172" s="T391">össü͡ö</ta>
            <ta e="T393" id="Seg_7173" s="T392">kim</ta>
            <ta e="T394" id="Seg_7174" s="T393">vpʼerʼedʼi</ta>
            <ta e="T395" id="Seg_7175" s="T394">hɨrɨt-IAK-tA</ta>
            <ta e="T396" id="Seg_7176" s="T395">razvʼečʼčʼik</ta>
            <ta e="T397" id="Seg_7177" s="T396">kördük</ta>
            <ta e="T398" id="Seg_7178" s="T397">orota-GI-n</ta>
            <ta e="T399" id="Seg_7179" s="T398">agaj</ta>
            <ta e="T400" id="Seg_7180" s="T399">keteː-A-GIn</ta>
            <ta e="T401" id="Seg_7181" s="T400">bu͡o</ta>
            <ta e="T402" id="Seg_7182" s="T401">inniki-LAr</ta>
            <ta e="T403" id="Seg_7183" s="T402">ikki</ta>
            <ta e="T404" id="Seg_7184" s="T403">kihi</ta>
            <ta e="T405" id="Seg_7185" s="T404">bu͡ol-Ar</ta>
            <ta e="T406" id="Seg_7186" s="T405">kaŋas</ta>
            <ta e="T407" id="Seg_7187" s="T406">dek</ta>
            <ta e="T408" id="Seg_7188" s="T407">uŋa</ta>
            <ta e="T409" id="Seg_7189" s="T408">dek</ta>
            <ta e="T410" id="Seg_7190" s="T409">emi͡e</ta>
            <ta e="T411" id="Seg_7191" s="T410">ikki-LIː</ta>
            <ta e="T412" id="Seg_7192" s="T411">kihi</ta>
            <ta e="T413" id="Seg_7193" s="T412">dʼe</ta>
            <ta e="T414" id="Seg_7194" s="T413">kajdi͡ek</ta>
            <ta e="T415" id="Seg_7195" s="T414">bu͡ol-IAK-BIt=Ij</ta>
            <ta e="T416" id="Seg_7196" s="T415">di͡e-Ar</ta>
            <ta e="T417" id="Seg_7197" s="T416">bu</ta>
            <ta e="T418" id="Seg_7198" s="T417">deri͡ebine-GA</ta>
            <ta e="T419" id="Seg_7199" s="T418">pravʼerʼaj-LAː-A-BIt</ta>
            <ta e="T420" id="Seg_7200" s="T419">ile</ta>
            <ta e="T421" id="Seg_7201" s="T420">kim-LAː</ta>
            <ta e="T422" id="Seg_7202" s="T421">kim</ta>
            <ta e="T423" id="Seg_7203" s="T422">da</ta>
            <ta e="T424" id="Seg_7204" s="T423">hu͡ok</ta>
            <ta e="T426" id="Seg_7205" s="T425">eː</ta>
            <ta e="T427" id="Seg_7206" s="T426">nʼemʼec-A-LAr</ta>
            <ta e="T428" id="Seg_7207" s="T427">onu͡or</ta>
            <ta e="T429" id="Seg_7208" s="T428">ere</ta>
            <ta e="T430" id="Seg_7209" s="T429">köhün-Ar-LAr</ta>
            <ta e="T431" id="Seg_7210" s="T430">töttörü</ta>
            <ta e="T432" id="Seg_7211" s="T431">bar-IAgIŋ</ta>
            <ta e="T433" id="Seg_7212" s="T432">di͡e-A-di͡e-A</ta>
            <ta e="T434" id="Seg_7213" s="T433">gɨn-Ar</ta>
            <ta e="T435" id="Seg_7214" s="T434">dʼe</ta>
            <ta e="T436" id="Seg_7215" s="T435">ol</ta>
            <ta e="T437" id="Seg_7216" s="T436">kim-LAː-A</ta>
            <ta e="T438" id="Seg_7217" s="T437">hɨrɨt-An-BIt</ta>
            <ta e="T439" id="Seg_7218" s="T438">u͡oldʼas-TI-BIt</ta>
            <ta e="T440" id="Seg_7219" s="T439">ol</ta>
            <ta e="T441" id="Seg_7220" s="T440">ol</ta>
            <ta e="T442" id="Seg_7221" s="T441">ürek-BItI-n</ta>
            <ta e="T443" id="Seg_7222" s="T442">taŋnarɨ</ta>
            <ta e="T444" id="Seg_7223" s="T443">bar-TI-BIt</ta>
            <ta e="T445" id="Seg_7224" s="T444">hüːrük</ta>
            <ta e="T446" id="Seg_7225" s="T445">taŋnarɨ</ta>
            <ta e="T447" id="Seg_7226" s="T446">ürek-LAN-An</ta>
            <ta e="T448" id="Seg_7227" s="T447">e</ta>
            <ta e="T449" id="Seg_7228" s="T448">bar-An-BIt</ta>
            <ta e="T450" id="Seg_7229" s="T449">eː</ta>
            <ta e="T451" id="Seg_7230" s="T450">kim-LAː-A-BIt</ta>
            <ta e="T452" id="Seg_7231" s="T451">bugurduk</ta>
            <ta e="T453" id="Seg_7232" s="T452">kim-LArA</ta>
            <ta e="T454" id="Seg_7233" s="T453">kim-LAː-BIT-LAr</ta>
            <ta e="T455" id="Seg_7234" s="T454">bu</ta>
            <ta e="T456" id="Seg_7235" s="T455">artʼilʼlʼerʼija</ta>
            <ta e="T457" id="Seg_7236" s="T456">bu͡ol-AːččI</ta>
            <ta e="T458" id="Seg_7237" s="T457">bu͡o</ta>
            <ta e="T459" id="Seg_7238" s="T458">puːska-LArA</ta>
            <ta e="T462" id="Seg_7239" s="T461">e</ta>
            <ta e="T463" id="Seg_7240" s="T462">kim-LArA</ta>
            <ta e="T464" id="Seg_7241" s="T463">at-LAr</ta>
            <ta e="T465" id="Seg_7242" s="T464">barɨta</ta>
            <ta e="T466" id="Seg_7243" s="T465">kim-LAː-A-LIN-I-BIT-LAr</ta>
            <ta e="T467" id="Seg_7244" s="T466">bugurduk</ta>
            <ta e="T468" id="Seg_7245" s="T467">perevaroč-A</ta>
            <ta e="T469" id="Seg_7246" s="T468">kim-LAː-BIT-LAr</ta>
            <ta e="T470" id="Seg_7247" s="T469">onton</ta>
            <ta e="T471" id="Seg_7248" s="T470">ölör-TAː-BIT-LAr</ta>
            <ta e="T472" id="Seg_7249" s="T471">onno</ta>
            <ta e="T473" id="Seg_7250" s="T472">biːr</ta>
            <ta e="T474" id="Seg_7251" s="T473">kihi-nI</ta>
            <ta e="T475" id="Seg_7252" s="T474">bul-TI-BIt</ta>
            <ta e="T8" id="Seg_7253" s="T475">paluzivoj</ta>
            <ta e="T476" id="Seg_7254" s="T8">ile</ta>
            <ta e="T477" id="Seg_7255" s="T476">kajdi͡ek</ta>
            <ta e="T478" id="Seg_7256" s="T477">ere</ta>
            <ta e="T479" id="Seg_7257" s="T478">arɨːččɨ</ta>
            <ta e="T480" id="Seg_7258" s="T479">haŋar-Ar</ta>
            <ta e="T482" id="Seg_7259" s="T480">onton</ta>
            <ta e="T483" id="Seg_7260" s="T482">bu</ta>
            <ta e="T484" id="Seg_7261" s="T483">haŋardɨː</ta>
            <ta e="T485" id="Seg_7262" s="T484">tabaːk</ta>
            <ta e="T486" id="Seg_7263" s="T485">tart-Ar</ta>
            <ta e="T487" id="Seg_7264" s="T486">ɨksa-tA</ta>
            <ta e="T488" id="Seg_7265" s="T487">bu͡ol-TI-tA</ta>
            <ta e="T489" id="Seg_7266" s="T488">di͡e-A-di͡e-A</ta>
            <ta e="T490" id="Seg_7267" s="T489">gɨn-Ar</ta>
            <ta e="T491" id="Seg_7268" s="T490">bihigi</ta>
            <ta e="T492" id="Seg_7269" s="T491">orota-BItI-n</ta>
            <ta e="T493" id="Seg_7270" s="T492">kim-LAː-BIT-LArA</ta>
            <ta e="T494" id="Seg_7271" s="T493">di͡e-Ar</ta>
            <ta e="T495" id="Seg_7272" s="T494">nʼemʼec-tA</ta>
            <ta e="T496" id="Seg_7273" s="T495">eː</ta>
            <ta e="T497" id="Seg_7274" s="T496">bar-BIT-LArA</ta>
            <ta e="T498" id="Seg_7275" s="T497">anɨ-kAːN</ta>
            <ta e="T499" id="Seg_7276" s="T498">di͡e-Ar</ta>
            <ta e="T500" id="Seg_7277" s="T499">haŋar-s-I-BA-ŋ</ta>
            <ta e="T501" id="Seg_7278" s="T500">tɨŋ-LIk</ta>
            <ta e="T502" id="Seg_7279" s="T501">di͡e-A-di͡e-A</ta>
            <ta e="T503" id="Seg_7280" s="T502">gɨn-Ar</ta>
            <ta e="T504" id="Seg_7281" s="T503">dʼe</ta>
            <ta e="T505" id="Seg_7282" s="T504">ol</ta>
            <ta e="T507" id="Seg_7283" s="T505">kim-LAː-TI-BIt</ta>
            <ta e="T508" id="Seg_7284" s="T507">dʼe</ta>
            <ta e="T509" id="Seg_7285" s="T508">onno</ta>
            <ta e="T510" id="Seg_7286" s="T509">diː</ta>
            <ta e="T511" id="Seg_7287" s="T510">eː</ta>
            <ta e="T512" id="Seg_7288" s="T511">dogor</ta>
            <ta e="T513" id="Seg_7289" s="T512">ikki</ta>
            <ta e="T514" id="Seg_7290" s="T513">kihi-nI</ta>
            <ta e="T515" id="Seg_7291" s="T514">ɨːt-Ar-LAr</ta>
            <ta e="T516" id="Seg_7292" s="T515">onno</ta>
            <ta e="T517" id="Seg_7293" s="T516">kim</ta>
            <ta e="T518" id="Seg_7294" s="T517">bu͡ol-AːččI</ta>
            <ta e="T519" id="Seg_7295" s="T518">sančʼas</ta>
            <ta e="T520" id="Seg_7296" s="T519">onton</ta>
            <ta e="T521" id="Seg_7297" s="T520">ilin-IAK-BIt</ta>
            <ta e="T522" id="Seg_7298" s="T521">di͡e-Ar-LAr</ta>
            <ta e="T523" id="Seg_7299" s="T522">min</ta>
            <ta e="T524" id="Seg_7300" s="T523">bihigi</ta>
            <ta e="T525" id="Seg_7301" s="T524">en-n</ta>
            <ta e="T528" id="Seg_7302" s="T527">min-n</ta>
            <ta e="T529" id="Seg_7303" s="T528">tu͡ok</ta>
            <ta e="T530" id="Seg_7304" s="T529">kɨhalɨn-I-BA-ŋ</ta>
            <ta e="T531" id="Seg_7305" s="T530">di͡e-Ar</ta>
            <ta e="T532" id="Seg_7306" s="T531">min</ta>
            <ta e="T533" id="Seg_7307" s="T532">e</ta>
            <ta e="T534" id="Seg_7308" s="T533">hin</ta>
            <ta e="T535" id="Seg_7309" s="T534">kihi</ta>
            <ta e="T536" id="Seg_7310" s="T535">bu͡ol-IAK</ta>
            <ta e="T537" id="Seg_7311" s="T536">e</ta>
            <ta e="T538" id="Seg_7312" s="T537">kihi</ta>
            <ta e="T539" id="Seg_7313" s="T538">bu͡ol-BAtAK-BIn</ta>
            <ta e="T540" id="Seg_7314" s="T539">di͡e-Ar</ta>
            <ta e="T541" id="Seg_7315" s="T540">bu͡o</ta>
            <ta e="T542" id="Seg_7316" s="T541">öl-IAK</ta>
            <ta e="T543" id="Seg_7317" s="T542">öl-IAK-m</ta>
            <ta e="T544" id="Seg_7318" s="T543">anɨ</ta>
            <ta e="T545" id="Seg_7319" s="T544">kihi-LAr</ta>
            <ta e="T546" id="Seg_7320" s="T545">olbok</ta>
            <ta e="T547" id="Seg_7321" s="T546">kördük</ta>
            <ta e="T548" id="Seg_7322" s="T547">hɨt-Ar-LAr</ta>
            <ta e="T549" id="Seg_7323" s="T548">ile</ta>
            <ta e="T550" id="Seg_7324" s="T549">ol-LAr-ttAn</ta>
            <ta e="T551" id="Seg_7325" s="T550">h-onon</ta>
            <ta e="T552" id="Seg_7326" s="T551">ol</ta>
            <ta e="T553" id="Seg_7327" s="T552">kim-LAː-TI-LAr</ta>
            <ta e="T554" id="Seg_7328" s="T553">ol-tI-GI-n</ta>
            <ta e="T555" id="Seg_7329" s="T554">ilt-TI-LAr</ta>
            <ta e="T556" id="Seg_7330" s="T555">ikki</ta>
            <ta e="T557" id="Seg_7331" s="T556">kihi</ta>
            <ta e="T558" id="Seg_7332" s="T557">kim-GA</ta>
            <ta e="T559" id="Seg_7333" s="T558">ol-tI-ŋ</ta>
            <ta e="T560" id="Seg_7334" s="T559">onton</ta>
            <ta e="T561" id="Seg_7335" s="T560">di͡e-BIT</ta>
            <ta e="T562" id="Seg_7336" s="T561">ühü</ta>
            <ta e="T563" id="Seg_7337" s="T562">ol</ta>
            <ta e="T565" id="Seg_7338" s="T563">vsʼo ravno </ta>
            <ta e="T566" id="Seg_7339" s="T565">tiri͡er-A-m</ta>
            <ta e="T567" id="Seg_7340" s="T566">e</ta>
            <ta e="T568" id="Seg_7341" s="T567">kim-LAː-An</ta>
            <ta e="T569" id="Seg_7342" s="T568">ol</ta>
            <ta e="T570" id="Seg_7343" s="T569">onton</ta>
            <ta e="T571" id="Seg_7344" s="T570">töttörü</ta>
            <ta e="T572" id="Seg_7345" s="T571">kel-TI-LAr</ta>
            <ta e="T573" id="Seg_7346" s="T572">ol-LAr-I-ŋ</ta>
            <ta e="T574" id="Seg_7347" s="T573">hin</ta>
            <ta e="T575" id="Seg_7348" s="T574">kihi</ta>
            <ta e="T576" id="Seg_7349" s="T575">bu͡ol-IAK-m</ta>
            <ta e="T577" id="Seg_7350" s="T576">hu͡ok-tA</ta>
            <ta e="T578" id="Seg_7351" s="T577">tu͡ok-nI</ta>
            <ta e="T579" id="Seg_7352" s="T578">min-n</ta>
            <ta e="T580" id="Seg_7353" s="T579">kɨtta</ta>
            <ta e="T583" id="Seg_7354" s="T582">kim-LAː-GIt</ta>
            <ta e="T584" id="Seg_7355" s="T583">di͡e-Ar</ta>
            <ta e="T585" id="Seg_7356" s="T584">ühü</ta>
            <ta e="T586" id="Seg_7357" s="T585">harsi͡erda</ta>
            <ta e="T587" id="Seg_7358" s="T586">im-BIt</ta>
            <ta e="T588" id="Seg_7359" s="T587">haŋardɨː</ta>
            <ta e="T589" id="Seg_7360" s="T588">tagɨs-Ar</ta>
            <ta e="T590" id="Seg_7361" s="T589">tagɨs-An</ta>
            <ta e="T593" id="Seg_7362" s="T592">eː</ta>
            <ta e="T594" id="Seg_7363" s="T593">kim-LAː-An</ta>
            <ta e="T595" id="Seg_7364" s="T594">er-TAK-InA</ta>
            <ta e="T596" id="Seg_7365" s="T595">tu͡ok</ta>
            <ta e="T597" id="Seg_7366" s="T596">ere</ta>
            <ta e="T598" id="Seg_7367" s="T597">lʼejtʼenaːn-nI</ta>
            <ta e="T599" id="Seg_7368" s="T598">körüs-TI-BIt</ta>
            <ta e="T600" id="Seg_7369" s="T599">ol-tI-BIt</ta>
            <ta e="T601" id="Seg_7370" s="T600">di͡e-Ar</ta>
            <ta e="T602" id="Seg_7371" s="T601">kannɨk</ta>
            <ta e="T603" id="Seg_7372" s="T602">čaːst-ttAn-GIn=Ij</ta>
            <ta e="T604" id="Seg_7373" s="T603">ehigi</ta>
            <ta e="T605" id="Seg_7374" s="T604">ehigi-ttAn</ta>
            <ta e="T606" id="Seg_7375" s="T605">di͡e-A-BIt</ta>
            <ta e="T607" id="Seg_7376" s="T606">di͡e-Ar</ta>
            <ta e="T608" id="Seg_7377" s="T607">onton</ta>
            <ta e="T609" id="Seg_7378" s="T608">biːr-BIt</ta>
            <ta e="T610" id="Seg_7379" s="T609">onton</ta>
            <ta e="T611" id="Seg_7380" s="T610">oččogo</ta>
            <ta e="T612" id="Seg_7381" s="T611">min-n</ta>
            <ta e="T613" id="Seg_7382" s="T612">kɨtta</ta>
            <ta e="T614" id="Seg_7383" s="T613">hüːr-I-ŋ</ta>
            <ta e="T615" id="Seg_7384" s="T614">di͡e-Ar</ta>
            <ta e="T616" id="Seg_7385" s="T615">ilin</ta>
            <ta e="T617" id="Seg_7386" s="T616">dek</ta>
            <ta e="T619" id="Seg_7387" s="T618">ere</ta>
            <ta e="T622" id="Seg_7388" s="T619">kim-LAː-m-IAK-GA</ta>
            <ta e="T623" id="Seg_7389" s="T622">svʼaz</ta>
            <ta e="T624" id="Seg_7390" s="T623">ɨl-A</ta>
            <ta e="T625" id="Seg_7391" s="T624">kim-LAː-Ar</ta>
            <ta e="T626" id="Seg_7392" s="T625">eni</ta>
            <ta e="T627" id="Seg_7393" s="T626">dʼe</ta>
            <ta e="T628" id="Seg_7394" s="T627">onton</ta>
            <ta e="T629" id="Seg_7395" s="T628">ol</ta>
            <ta e="T630" id="Seg_7396" s="T629">kim-LAː-A</ta>
            <ta e="T631" id="Seg_7397" s="T630">hataː-A-BIt</ta>
            <ta e="T632" id="Seg_7398" s="T631">kas</ta>
            <ta e="T633" id="Seg_7399" s="T632">da</ta>
            <ta e="T634" id="Seg_7400" s="T633">kün-nI</ta>
            <ta e="T635" id="Seg_7401" s="T634">ol</ta>
            <ta e="T636" id="Seg_7402" s="T635">hɨrɨt-TI-BIt</ta>
            <ta e="T637" id="Seg_7403" s="T636">ol</ta>
            <ta e="T638" id="Seg_7404" s="T637">kurduk</ta>
            <ta e="T639" id="Seg_7405" s="T638">orota-BItI-n</ta>
            <ta e="T640" id="Seg_7406" s="T639">da</ta>
            <ta e="T641" id="Seg_7407" s="T640">bul-BAT-BIt</ta>
            <ta e="T642" id="Seg_7408" s="T641">dʼe</ta>
            <ta e="T643" id="Seg_7409" s="T642">onton</ta>
            <ta e="T644" id="Seg_7410" s="T643">kojut-GI-tI-n</ta>
            <ta e="T645" id="Seg_7411" s="T644">kim-LAː-TI</ta>
            <ta e="T646" id="Seg_7412" s="T645">eː</ta>
            <ta e="T647" id="Seg_7413" s="T646">bul-TI-BIt</ta>
            <ta e="T648" id="Seg_7414" s="T647">emi͡e</ta>
            <ta e="T649" id="Seg_7415" s="T648">eː</ta>
            <ta e="T652" id="Seg_7416" s="T651">üs-Is</ta>
            <ta e="T653" id="Seg_7417" s="T652">kün-BItI-GAr</ta>
            <ta e="T654" id="Seg_7418" s="T653">du͡o</ta>
            <ta e="T655" id="Seg_7419" s="T654">kas-Is-BItI-GAr</ta>
            <ta e="T656" id="Seg_7420" s="T655">du͡o</ta>
            <ta e="T657" id="Seg_7421" s="T656">ɨntak</ta>
            <ta e="T658" id="Seg_7422" s="T657">bar-A-BIt</ta>
            <ta e="T659" id="Seg_7423" s="T658">onno</ta>
            <ta e="T660" id="Seg_7424" s="T659">kim-GA</ta>
            <ta e="T661" id="Seg_7425" s="T660">ol</ta>
            <ta e="T662" id="Seg_7426" s="T661">ürek-BItI-n</ta>
            <ta e="T663" id="Seg_7427" s="T662">tagɨs-A-BIt</ta>
            <ta e="T664" id="Seg_7428" s="T663">dʼe</ta>
            <ta e="T665" id="Seg_7429" s="T664">onton</ta>
            <ta e="T666" id="Seg_7430" s="T665">ol</ta>
            <ta e="T667" id="Seg_7431" s="T666">u͡oldʼas-A-BIt</ta>
            <ta e="T668" id="Seg_7432" s="T667">ol</ta>
            <ta e="T669" id="Seg_7433" s="T668">ɨnaraː-ttAn</ta>
            <ta e="T670" id="Seg_7434" s="T669">ɨt-IAlAː-Ar-LAr</ta>
            <ta e="T671" id="Seg_7435" s="T670">a</ta>
            <ta e="T672" id="Seg_7436" s="T671">bihigi</ta>
            <ta e="T673" id="Seg_7437" s="T672">betereː-ttAn</ta>
            <ta e="T674" id="Seg_7438" s="T673">emi͡e</ta>
            <ta e="T675" id="Seg_7439" s="T674">ɨt-IAlAː-A-s-A-BIt</ta>
            <ta e="T676" id="Seg_7440" s="T675">bu͡olla</ta>
            <ta e="T677" id="Seg_7441" s="T676">dʼe</ta>
            <ta e="T678" id="Seg_7442" s="T677">ol</ta>
            <ta e="T679" id="Seg_7443" s="T678">kim-LAː-TI-BIt</ta>
            <ta e="T680" id="Seg_7444" s="T679">da</ta>
            <ta e="T681" id="Seg_7445" s="T680">onno</ta>
            <ta e="T682" id="Seg_7446" s="T681">kajdi͡ek</ta>
            <ta e="T683" id="Seg_7447" s="T682">da</ta>
            <ta e="T684" id="Seg_7448" s="T683">kim-LAː-BAT-LAr</ta>
            <ta e="T685" id="Seg_7449" s="T684">ɨːt-BAT-LAr</ta>
            <ta e="T686" id="Seg_7450" s="T685">ol</ta>
            <ta e="T687" id="Seg_7451" s="T686">harsi͡erda</ta>
            <ta e="T688" id="Seg_7452" s="T687">kɨ͡ak-LAːK</ta>
            <ta e="T689" id="Seg_7453" s="T688">kel-TI-LAr</ta>
            <ta e="T690" id="Seg_7454" s="T689">ol</ta>
            <ta e="T691" id="Seg_7455" s="T690">dek</ta>
            <ta e="T692" id="Seg_7456" s="T691">ilin-BItI-ttAn</ta>
            <ta e="T693" id="Seg_7457" s="T692">bu</ta>
            <ta e="T696" id="Seg_7458" s="T695">kamanda</ta>
            <ta e="T697" id="Seg_7459" s="T696">bi͡er-Ar-LAr</ta>
            <ta e="T698" id="Seg_7460" s="T697">nu</ta>
            <ta e="T699" id="Seg_7461" s="T698">onton</ta>
            <ta e="T700" id="Seg_7462" s="T699">üs</ta>
            <ta e="T701" id="Seg_7463" s="T700">čaːs-GA</ta>
            <ta e="T702" id="Seg_7464" s="T701">Brʼanskaj-nI</ta>
            <ta e="T703" id="Seg_7465" s="T702">zanʼimaj-LAː-IAK-GItI-n</ta>
            <ta e="T704" id="Seg_7466" s="T703">naːda</ta>
            <ta e="T705" id="Seg_7467" s="T704">dʼe</ta>
            <ta e="T706" id="Seg_7468" s="T705">onno</ta>
            <ta e="T707" id="Seg_7469" s="T706">bugurduk</ta>
            <ta e="T708" id="Seg_7470" s="T707">tu͡ok</ta>
            <ta e="T709" id="Seg_7471" s="T708">ere</ta>
            <ta e="T710" id="Seg_7472" s="T709">hɨːr</ta>
            <ta e="T711" id="Seg_7473" s="T710">bu͡ol-Ar</ta>
            <ta e="T712" id="Seg_7474" s="T711">dʼerbi</ta>
            <ta e="T715" id="Seg_7475" s="T714">dʼerbi-nI</ta>
            <ta e="T716" id="Seg_7476" s="T715">e</ta>
            <ta e="T717" id="Seg_7477" s="T716">kim-LAː-TI-BIt</ta>
            <ta e="T718" id="Seg_7478" s="T717">da</ta>
            <ta e="T719" id="Seg_7479" s="T718">emi͡e</ta>
            <ta e="T720" id="Seg_7480" s="T719">emi͡e</ta>
            <ta e="T721" id="Seg_7481" s="T720">hɨraj-tA</ta>
            <ta e="T724" id="Seg_7482" s="T723">ilin-I-ttAn</ta>
            <ta e="T725" id="Seg_7483" s="T724">kim-LAː-Ar-LAr</ta>
            <ta e="T726" id="Seg_7484" s="T725">bu͡o</ta>
            <ta e="T729" id="Seg_7485" s="T728">nʼemʼec-LAr</ta>
            <ta e="T730" id="Seg_7486" s="T729">bugurduk</ta>
            <ta e="T731" id="Seg_7487" s="T730">hɨt-Ar-LAr</ta>
            <ta e="T733" id="Seg_7488" s="T732">gɨn-A</ta>
            <ta e="T734" id="Seg_7489" s="T733">hɨt-Ar-LAr</ta>
            <ta e="T735" id="Seg_7490" s="T734">bu͡o</ta>
            <ta e="T736" id="Seg_7491" s="T735">a</ta>
            <ta e="T737" id="Seg_7492" s="T736">bihigi</ta>
            <ta e="T738" id="Seg_7493" s="T737">bu</ta>
            <ta e="T739" id="Seg_7494" s="T738">dek-I-ttAn</ta>
            <ta e="T740" id="Seg_7495" s="T739">emi͡e</ta>
            <ta e="T741" id="Seg_7496" s="T740">ɨt-IAlAː-A-s-A-BIt</ta>
            <ta e="T742" id="Seg_7497" s="T741">dʼe</ta>
            <ta e="T743" id="Seg_7498" s="T742">onton</ta>
            <ta e="T744" id="Seg_7499" s="T743">bu</ta>
            <ta e="T745" id="Seg_7500" s="T744">tüːn-nI</ta>
            <ta e="T746" id="Seg_7501" s="T745">huptu</ta>
            <ta e="T747" id="Seg_7502" s="T746">hɨt-A</ta>
            <ta e="T748" id="Seg_7503" s="T747">manna</ta>
            <ta e="T749" id="Seg_7504" s="T748">bu͡o</ta>
            <ta e="T750" id="Seg_7505" s="T749">tüːn-nI</ta>
            <ta e="T751" id="Seg_7506" s="T750">u͡on-LAr</ta>
            <ta e="T752" id="Seg_7507" s="T751">du͡o</ta>
            <ta e="T753" id="Seg_7508" s="T752">ikki-LAr</ta>
            <ta e="T754" id="Seg_7509" s="T753">dek</ta>
            <ta e="T755" id="Seg_7510" s="T754">du͡o</ta>
            <ta e="T756" id="Seg_7511" s="T755">ili</ta>
            <ta e="T757" id="Seg_7512" s="T756">čaːs-LAr</ta>
            <ta e="T758" id="Seg_7513" s="T757">dek</ta>
            <ta e="T759" id="Seg_7514" s="T758">du͡o</ta>
            <ta e="T760" id="Seg_7515" s="T759">ol</ta>
            <ta e="T761" id="Seg_7516" s="T760">rakʼet-nI</ta>
            <ta e="T762" id="Seg_7517" s="T761">ɨːt-Ar-LAr</ta>
            <ta e="T763" id="Seg_7518" s="T762">rakʼet-nI</ta>
            <ta e="T764" id="Seg_7519" s="T763">ɨːt-TAK-InA</ta>
            <ta e="T765" id="Seg_7520" s="T764">oččogo</ta>
            <ta e="T766" id="Seg_7521" s="T765">bu</ta>
            <ta e="T767" id="Seg_7522" s="T766">barɨta</ta>
            <ta e="T769" id="Seg_7523" s="T768">kim-LAː-A</ta>
            <ta e="T770" id="Seg_7524" s="T769">hɨt-Ar-LAr</ta>
            <ta e="T771" id="Seg_7525" s="T770">barɨta</ta>
            <ta e="T772" id="Seg_7526" s="T771">köhün-Ar-LAr</ta>
            <ta e="T773" id="Seg_7527" s="T772">iliː-BI-n</ta>
            <ta e="T774" id="Seg_7528" s="T773">tap-TAr-An</ta>
            <ta e="T775" id="Seg_7529" s="T774">bu</ta>
            <ta e="T776" id="Seg_7530" s="T775">dʼüːlleː-BA-tI-m</ta>
            <ta e="T777" id="Seg_7531" s="T776">onton</ta>
            <ta e="T778" id="Seg_7532" s="T777">ol</ta>
            <ta e="T779" id="Seg_7533" s="T778">bu-tI-BA-r</ta>
            <ta e="T780" id="Seg_7534" s="T779">tap</ta>
            <ta e="T781" id="Seg_7535" s="T780">kim-LAː-BIT</ta>
            <ta e="T782" id="Seg_7536" s="T781">ol</ta>
            <ta e="T783" id="Seg_7537" s="T782">huptu</ta>
            <ta e="T784" id="Seg_7538" s="T783">köt-An</ta>
            <ta e="T785" id="Seg_7539" s="T784">biːr</ta>
            <ta e="T786" id="Seg_7540" s="T785">mina</ta>
            <ta e="T787" id="Seg_7541" s="T786">kel-TI-tA</ta>
            <ta e="T788" id="Seg_7542" s="T787">da</ta>
            <ta e="T789" id="Seg_7543" s="T788">barɨ-BItI-n</ta>
            <ta e="T790" id="Seg_7544" s="T789">holoː-BIT</ta>
            <ta e="T791" id="Seg_7545" s="T790">ol</ta>
            <ta e="T792" id="Seg_7546" s="T791">u͡on</ta>
            <ta e="T793" id="Seg_7547" s="T792">orduk</ta>
            <ta e="T794" id="Seg_7548" s="T793">kihi-nI</ta>
            <ta e="T795" id="Seg_7549" s="T794">haː-BI-n</ta>
            <ta e="T796" id="Seg_7550" s="T795">bugurduk</ta>
            <ta e="T797" id="Seg_7551" s="T796">tut-Ar-BA-r</ta>
            <ta e="T798" id="Seg_7552" s="T797">öj-LAN-TI-m</ta>
            <ta e="T799" id="Seg_7553" s="T798">onton</ta>
            <ta e="T800" id="Seg_7554" s="T799">innʼe</ta>
            <ta e="T801" id="Seg_7555" s="T800">innʼe</ta>
            <ta e="T802" id="Seg_7556" s="T801">tur-A-BIn</ta>
            <ta e="T803" id="Seg_7557" s="T802">tu͡ok</ta>
            <ta e="T804" id="Seg_7558" s="T803">tobuk-LIː</ta>
            <ta e="T805" id="Seg_7559" s="T804">dʼe</ta>
            <ta e="T806" id="Seg_7560" s="T805">onton</ta>
            <ta e="T807" id="Seg_7561" s="T806">tüs-TI-m</ta>
            <ta e="T808" id="Seg_7562" s="T807">bugurduk</ta>
            <ta e="T809" id="Seg_7563" s="T808">dʼe</ta>
            <ta e="T810" id="Seg_7564" s="T809">onton</ta>
            <ta e="T811" id="Seg_7565" s="T810">kojut</ta>
            <ta e="T812" id="Seg_7566" s="T811">kör-TI-m</ta>
            <ta e="T813" id="Seg_7567" s="T812">bu͡o</ta>
            <ta e="T814" id="Seg_7568" s="T813">čömüje-LAr-I-m</ta>
            <ta e="T815" id="Seg_7569" s="T814">barɨta</ta>
            <ta e="T816" id="Seg_7570" s="T815">halɨbɨraː-A</ta>
            <ta e="T817" id="Seg_7571" s="T816">hɨrɨt-Ar-LAr</ta>
            <ta e="T818" id="Seg_7572" s="T817">kaːn-I-m</ta>
            <ta e="T819" id="Seg_7573" s="T818">uhun-A</ta>
            <ta e="T820" id="Seg_7574" s="T819">hɨt-Ar</ta>
            <ta e="T821" id="Seg_7575" s="T820">bugurduk</ta>
            <ta e="T822" id="Seg_7576" s="T821">dʼe</ta>
            <ta e="T823" id="Seg_7577" s="T822">onton</ta>
            <ta e="T824" id="Seg_7578" s="T823">kur-BI-n</ta>
            <ta e="T825" id="Seg_7579" s="T824">ugul-TI-m</ta>
            <ta e="T826" id="Seg_7580" s="T825">bugurduk</ta>
            <ta e="T827" id="Seg_7581" s="T826">kur-BI-n</ta>
            <ta e="T828" id="Seg_7582" s="T827">ugul-An</ta>
            <ta e="T829" id="Seg_7583" s="T828">bar-An-BIn</ta>
            <ta e="T830" id="Seg_7584" s="T829">bugurduk</ta>
            <ta e="T831" id="Seg_7585" s="T830">kim-LAː-TI-m</ta>
            <ta e="T832" id="Seg_7586" s="T831">eni</ta>
            <ta e="T833" id="Seg_7587" s="T832">kim-BA-r</ta>
            <ta e="T834" id="Seg_7588" s="T833">moːj-BA-r</ta>
            <ta e="T835" id="Seg_7589" s="T834">innʼe</ta>
            <ta e="T838" id="Seg_7590" s="T837">kim-LAː-TI-m</ta>
            <ta e="T839" id="Seg_7591" s="T838">bu</ta>
            <ta e="T840" id="Seg_7592" s="T839">baːj-An</ta>
            <ta e="T841" id="Seg_7593" s="T840">bar-An-BIn</ta>
            <ta e="T842" id="Seg_7594" s="T841">onton</ta>
            <ta e="T843" id="Seg_7595" s="T842">bu</ta>
            <ta e="T844" id="Seg_7596" s="T843">pakʼet-LAr</ta>
            <ta e="T845" id="Seg_7597" s="T844">bu͡ol-AːččI-LAr</ta>
            <ta e="T848" id="Seg_7598" s="T847">dʼe</ta>
            <ta e="T849" id="Seg_7599" s="T848">ol-tI-BI-nAn</ta>
            <ta e="T850" id="Seg_7600" s="T849">erij-TI-m</ta>
            <ta e="T851" id="Seg_7601" s="T850">bugurduk</ta>
            <ta e="T852" id="Seg_7602" s="T851">onton</ta>
            <ta e="T853" id="Seg_7603" s="T852">ommu͡otka</ta>
            <ta e="T854" id="Seg_7604" s="T853">iti</ta>
            <ta e="T855" id="Seg_7605" s="T854">bočinka-GAr</ta>
            <ta e="T856" id="Seg_7606" s="T855">kim</ta>
            <ta e="T857" id="Seg_7607" s="T856">bu͡ol-AːččI</ta>
            <ta e="T860" id="Seg_7608" s="T859">ol-nI</ta>
            <ta e="T861" id="Seg_7609" s="T860">ommu͡otka-BI-nAn</ta>
            <ta e="T862" id="Seg_7610" s="T861">erij-TI-m</ta>
            <ta e="T863" id="Seg_7611" s="T862">bar</ta>
            <ta e="T864" id="Seg_7612" s="T863">di͡e-Ar-LAr</ta>
            <ta e="T865" id="Seg_7613" s="T864">kim</ta>
            <ta e="T866" id="Seg_7614" s="T865">kamandʼir</ta>
            <ta e="T867" id="Seg_7615" s="T866">orota-GAr</ta>
            <ta e="T868" id="Seg_7616" s="T867">hol</ta>
            <ta e="T869" id="Seg_7617" s="T868">ün-An</ta>
            <ta e="T870" id="Seg_7618" s="T869">ün-BIT-I-nAn</ta>
            <ta e="T871" id="Seg_7619" s="T870">tij-TI-m</ta>
            <ta e="T872" id="Seg_7620" s="T871">ol-tI-BA-r</ta>
            <ta e="T873" id="Seg_7621" s="T872">keli͡ep</ta>
            <ta e="T874" id="Seg_7622" s="T873">kördöː-An</ta>
            <ta e="T875" id="Seg_7623" s="T874">eː</ta>
            <ta e="T878" id="Seg_7624" s="T877">as</ta>
            <ta e="T879" id="Seg_7625" s="T878">kaja</ta>
            <ta e="T880" id="Seg_7626" s="T879">di͡e-Ar</ta>
            <ta e="T881" id="Seg_7627" s="T880">ran-A</ta>
            <ta e="T882" id="Seg_7628" s="T881">gɨn</ta>
            <ta e="T883" id="Seg_7629" s="T930">du͡o</ta>
            <ta e="T885" id="Seg_7630" s="T883">di͡e-Ar</ta>
            <ta e="T886" id="Seg_7631" s="T885">hmm</ta>
            <ta e="T887" id="Seg_7632" s="T886">di͡e-A-BIn</ta>
            <ta e="T888" id="Seg_7633" s="T887">haː-BI-n</ta>
            <ta e="T889" id="Seg_7634" s="T888">onton</ta>
            <ta e="T890" id="Seg_7635" s="T889">ɨl-BIT</ta>
            <ta e="T891" id="Seg_7636" s="T890">e-TI-tA</ta>
            <ta e="T892" id="Seg_7637" s="T891">ol</ta>
            <ta e="T893" id="Seg_7638" s="T892">kim-I-m</ta>
            <ta e="T894" id="Seg_7639" s="T893">kamandʼir-BA-r</ta>
            <ta e="T895" id="Seg_7640" s="T894">sdavaj-LAː-A-BIn</ta>
            <ta e="T896" id="Seg_7641" s="T895">bu͡o</ta>
            <ta e="T897" id="Seg_7642" s="T896">kim-GA</ta>
            <ta e="T898" id="Seg_7643" s="T897">nomʼer-I-nAn</ta>
            <ta e="T899" id="Seg_7644" s="T898">bu͡ol-AːččI</ta>
            <ta e="T900" id="Seg_7645" s="T899">e-TI-LArA</ta>
            <ta e="T901" id="Seg_7646" s="T900">ol-tI-m</ta>
            <ta e="T902" id="Seg_7647" s="T901">bu͡o</ta>
            <ta e="T903" id="Seg_7648" s="T902">kim</ta>
            <ta e="T904" id="Seg_7649" s="T903">patru͡on-LAr</ta>
            <ta e="T905" id="Seg_7650" s="T904">karmaːn-BA-r</ta>
            <ta e="T906" id="Seg_7651" s="T905">tu͡ok</ta>
            <ta e="T907" id="Seg_7652" s="T906">da</ta>
            <ta e="T908" id="Seg_7653" s="T907">hu͡ok</ta>
            <ta e="T909" id="Seg_7654" s="T908">granaːt-LAr-BI-n</ta>
            <ta e="T910" id="Seg_7655" s="T909">emi͡e</ta>
            <ta e="T911" id="Seg_7656" s="T910">ɨl-BIT-LAr</ta>
            <ta e="T912" id="Seg_7657" s="T911">oku͡opa-GA</ta>
            <ta e="T913" id="Seg_7658" s="T912">hɨt-TAK-BInA</ta>
            <ta e="T914" id="Seg_7659" s="T913">kumak</ta>
            <ta e="T915" id="Seg_7660" s="T914">tüs-AːktAː-Ar</ta>
            <ta e="T916" id="Seg_7661" s="T915">diː</ta>
            <ta e="T917" id="Seg_7662" s="T916">ol</ta>
            <ta e="T918" id="Seg_7663" s="T917">di͡egi-ttAn</ta>
            <ta e="T919" id="Seg_7664" s="T918">kel-A</ta>
            <ta e="T920" id="Seg_7665" s="T919">itte</ta>
            <ta e="T921" id="Seg_7666" s="T920">mina-LAr</ta>
            <ta e="T922" id="Seg_7667" s="T921">tüs-TAK-TArInA</ta>
            <ta e="T923" id="Seg_7668" s="T922">hir-I-m</ta>
            <ta e="T924" id="Seg_7669" s="T923">dorguj-Ar</ta>
            <ta e="T929" id="Seg_7670" s="T928">kim-LAː-Ar-LAr</ta>
            <ta e="T931" id="Seg_7671" s="T929">agaj</ta>
            <ta e="T932" id="Seg_7672" s="T931">ol-tI-m</ta>
            <ta e="T933" id="Seg_7673" s="T932">bu</ta>
            <ta e="T934" id="Seg_7674" s="T933">karda</ta>
            <ta e="T935" id="Seg_7675" s="T937">zapаlnʼenʼije</ta>
            <ta e="T936" id="Seg_7676" s="T935">kel-Ar</ta>
            <ta e="T938" id="Seg_7677" s="T936">e-BIT</ta>
            <ta e="T939" id="Seg_7678" s="T938">bihigi</ta>
            <ta e="T940" id="Seg_7679" s="T939">onnu-BItI-GAr</ta>
            <ta e="T941" id="Seg_7680" s="T940">össü͡ö</ta>
            <ta e="T942" id="Seg_7681" s="T941">nöŋü͡ö-LAr</ta>
            <ta e="T943" id="Seg_7682" s="T942">kel-Ar-LAr</ta>
            <ta e="T944" id="Seg_7683" s="T943">bu͡ol-Ar</ta>
            <ta e="T945" id="Seg_7684" s="T944">ajdaːn</ta>
            <ta e="T946" id="Seg_7685" s="T945">bu͡ol-AːktAː-Ar</ta>
            <ta e="T947" id="Seg_7686" s="T946">diː</ta>
            <ta e="T948" id="Seg_7687" s="T947">onton</ta>
            <ta e="T949" id="Seg_7688" s="T948">ɨtaː-A-s-Ar-LAr</ta>
            <ta e="T950" id="Seg_7689" s="T949">bu͡ol-Ar-LAr</ta>
            <ta e="T951" id="Seg_7690" s="T950">agaj</ta>
            <ta e="T952" id="Seg_7691" s="T951">horok-horok</ta>
            <ta e="T953" id="Seg_7692" s="T952">ɨnčɨktaː-Ar</ta>
            <ta e="T954" id="Seg_7693" s="T953">bu͡ol-Ar</ta>
            <ta e="T955" id="Seg_7694" s="T954">ɨnčɨktaː-Ar</ta>
            <ta e="T956" id="Seg_7695" s="T955">bu͡ol-TAK-TArInA</ta>
            <ta e="T957" id="Seg_7696" s="T956">min</ta>
            <ta e="T958" id="Seg_7697" s="T957">emi͡e</ta>
            <ta e="T959" id="Seg_7698" s="T958">emi͡e</ta>
            <ta e="T960" id="Seg_7699" s="T959">ɨnčɨktaː-A-BIn</ta>
            <ta e="T961" id="Seg_7700" s="T960">emi͡e</ta>
            <ta e="T962" id="Seg_7701" s="T961">onton</ta>
            <ta e="T963" id="Seg_7702" s="T962">emi͡e</ta>
            <ta e="T964" id="Seg_7703" s="T963">dʼɨlɨj-s</ta>
            <ta e="T965" id="Seg_7704" s="T964">gɨn-A-BIn</ta>
            <ta e="T966" id="Seg_7705" s="T965">ihilleː-A-BIn</ta>
            <ta e="T967" id="Seg_7706" s="T966">sančʼas-GA</ta>
            <ta e="T968" id="Seg_7707" s="T967">kel-BIT-I-m</ta>
            <ta e="T971" id="Seg_7708" s="T970">pаlʼevoj</ta>
            <ta e="T972" id="Seg_7709" s="T971">üle</ta>
            <ta e="T973" id="Seg_7710" s="T972">kim</ta>
            <ta e="T974" id="Seg_7711" s="T973">bu͡ol-AːččI</ta>
            <ta e="T975" id="Seg_7712" s="T974">sančʼas</ta>
            <ta e="T976" id="Seg_7713" s="T975">ol</ta>
            <ta e="T977" id="Seg_7714" s="T976">sančʼas-GA</ta>
            <ta e="T978" id="Seg_7715" s="T977">kel-TI</ta>
            <ta e="T979" id="Seg_7716" s="T978">eː</ta>
            <ta e="T980" id="Seg_7717" s="T979">kel-TI-m</ta>
            <ta e="T981" id="Seg_7718" s="T980">da</ta>
            <ta e="T982" id="Seg_7719" s="T981">tɨl-GA</ta>
            <ta e="T983" id="Seg_7720" s="T982">ɨːt-BIT-LArA</ta>
            <ta e="T984" id="Seg_7721" s="T983">Ivanava Suja</ta>
            <ta e="T985" id="Seg_7722" s="T984">hɨt-BIT-I-m</ta>
            <ta e="T986" id="Seg_7723" s="T985">erge</ta>
            <ta e="T987" id="Seg_7724" s="T986">hospitalʼ-GA</ta>
            <ta e="T988" id="Seg_7725" s="T987">onton</ta>
            <ta e="T991" id="Seg_7726" s="T990">eː</ta>
            <ta e="T992" id="Seg_7727" s="T991">srazu</ta>
            <ta e="T993" id="Seg_7728" s="T992">bi͡es</ta>
            <ta e="T994" id="Seg_7729" s="T993">ɨj-nI</ta>
            <ta e="T995" id="Seg_7730" s="T994">hɨt-TI-m</ta>
            <ta e="T996" id="Seg_7731" s="T995">da</ta>
            <ta e="T997" id="Seg_7732" s="T996">dʼi͡e-m</ta>
            <ta e="T998" id="Seg_7733" s="T997">dek</ta>
            <ta e="T999" id="Seg_7734" s="T998">ɨːt-BIT-LArA</ta>
         </annotation>
         <annotation name="ge" tierref="ge-ChVD">
            <ta e="T1" id="Seg_7735" s="T0">Dudinka-DAT/LOC</ta>
            <ta e="T2" id="Seg_7736" s="T1">come-PST2-EP-1SG</ta>
            <ta e="T6" id="Seg_7737" s="T5">august</ta>
            <ta e="T7" id="Seg_7738" s="T6">month-DAT/LOC</ta>
            <ta e="T11" id="Seg_7739" s="T10">commission-ACC</ta>
            <ta e="T12" id="Seg_7740" s="T11">go.through-CVB.SIM</ta>
            <ta e="T13" id="Seg_7741" s="T12">make-PST1-1SG</ta>
            <ta e="T14" id="Seg_7742" s="T13">non_combatant-DAT/LOC</ta>
            <ta e="T15" id="Seg_7743" s="T14">Norilsk-DAT/LOC</ta>
            <ta e="T16" id="Seg_7744" s="T15">send-PST2-3PL</ta>
            <ta e="T17" id="Seg_7745" s="T16">Norilsk-ABL</ta>
            <ta e="T18" id="Seg_7746" s="T17">who-DAT/LOC</ta>
            <ta e="T19" id="Seg_7747" s="T18">eh</ta>
            <ta e="T9" id="Seg_7748" s="T19">military</ta>
            <ta e="T20" id="Seg_7749" s="T9">guard-DAT/LOC</ta>
            <ta e="T21" id="Seg_7750" s="T20">work-CVB.SIM</ta>
            <ta e="T22" id="Seg_7751" s="T21">fall-PST1-1SG</ta>
            <ta e="T23" id="Seg_7752" s="T22">voluntary-EP-PL-ACC</ta>
            <ta e="T24" id="Seg_7753" s="T23">with</ta>
            <ta e="T25" id="Seg_7754" s="T24">again</ta>
            <ta e="T26" id="Seg_7755" s="T25">well</ta>
            <ta e="T27" id="Seg_7756" s="T26">get.into-PST2-EP-1SG</ta>
            <ta e="T28" id="Seg_7757" s="T27">Krasnoyarsk-DAT/LOC</ta>
            <ta e="T29" id="Seg_7758" s="T28">eh</ta>
            <ta e="T30" id="Seg_7759" s="T29">until</ta>
            <ta e="T31" id="Seg_7760" s="T30">go-PST2-EP-1SG</ta>
            <ta e="T32" id="Seg_7761" s="T31">Spartak</ta>
            <ta e="T33" id="Seg_7762" s="T32">steamer-3SG-INSTR</ta>
            <ta e="T34" id="Seg_7763" s="T33">then</ta>
            <ta e="T35" id="Seg_7764" s="T34">learn-PST1-1SG</ta>
            <ta e="T36" id="Seg_7765" s="T35">Krasnoyarsk</ta>
            <ta e="T37" id="Seg_7766" s="T36">infantry-DAT/LOC</ta>
            <ta e="T41" id="Seg_7767" s="T40">ten</ta>
            <ta e="T42" id="Seg_7768" s="T41">one</ta>
            <ta e="T43" id="Seg_7769" s="T42">month-ACC</ta>
            <ta e="T44" id="Seg_7770" s="T43">Q</ta>
            <ta e="T45" id="Seg_7771" s="T44">how.much-ACC</ta>
            <ta e="T46" id="Seg_7772" s="T45">Q</ta>
            <ta e="T47" id="Seg_7773" s="T46">learn-CVB.SEQ</ta>
            <ta e="T48" id="Seg_7774" s="T47">after</ta>
            <ta e="T49" id="Seg_7775" s="T48">again</ta>
            <ta e="T50" id="Seg_7776" s="T49">repeat-CVB.SIM</ta>
            <ta e="T51" id="Seg_7777" s="T54">who-DAT/LOC</ta>
            <ta e="T52" id="Seg_7778" s="T51">Zvenigorod-DAT/LOC</ta>
            <ta e="T56" id="Seg_7779" s="T55">parachute.[NOM]</ta>
            <ta e="T57" id="Seg_7780" s="T56">take.apart-VBZ-PTCP.HAB</ta>
            <ta e="T58" id="Seg_7781" s="T57">be-PST1-1SG</ta>
            <ta e="T60" id="Seg_7782" s="T59">well</ta>
            <ta e="T61" id="Seg_7783" s="T60">there</ta>
            <ta e="T62" id="Seg_7784" s="T61">long</ta>
            <ta e="T63" id="Seg_7785" s="T62">NEG</ta>
            <ta e="T64" id="Seg_7786" s="T63">learn-PST2.NEG-EP-1SG</ta>
            <ta e="T65" id="Seg_7787" s="T64">friend-PL-EP-1SG.[NOM]</ta>
            <ta e="T66" id="Seg_7788" s="T65">with</ta>
            <ta e="T67" id="Seg_7789" s="T66">this.[NOM]</ta>
            <ta e="T68" id="Seg_7790" s="T67">stay-EP-NEG-CVB.PURP-1SG</ta>
            <ta e="T69" id="Seg_7791" s="T68">together</ta>
            <ta e="T70" id="Seg_7792" s="T69">go-RECP/COLL-EP-PST2-EP-1SG</ta>
            <ta e="T71" id="Seg_7793" s="T70">well</ta>
            <ta e="T72" id="Seg_7794" s="T71">that</ta>
            <ta e="T73" id="Seg_7795" s="T72">who-VBZ-PST1-1SG</ta>
            <ta e="T74" id="Seg_7796" s="T73">who-DAT/LOC</ta>
            <ta e="T75" id="Seg_7797" s="T74">Murom-DAT/LOC</ta>
            <ta e="T76" id="Seg_7798" s="T75">learn-CVB.SIM</ta>
            <ta e="T77" id="Seg_7799" s="T76">who-VBZ-PST1-1PL</ta>
            <ta e="T481" id="Seg_7800" s="T80">simply</ta>
            <ta e="T81" id="Seg_7801" s="T481">who-VBZ-PTCP.PRS</ta>
            <ta e="T82" id="Seg_7802" s="T81">be-PST1-3PL</ta>
            <ta e="T83" id="Seg_7803" s="T82">there</ta>
            <ta e="T84" id="Seg_7804" s="T83">formation.[NOM]</ta>
            <ta e="T85" id="Seg_7805" s="T84">make-PTCP.PRS</ta>
            <ta e="T86" id="Seg_7806" s="T85">be-PST1-3PL</ta>
            <ta e="T92" id="Seg_7807" s="T91">what-DAT/LOC</ta>
            <ta e="T93" id="Seg_7808" s="T92">learn-PTCP.PST-2SG-ACC</ta>
            <ta e="T94" id="Seg_7809" s="T93">who-PROPR</ta>
            <ta e="T95" id="Seg_7810" s="T94">who.[NOM]</ta>
            <ta e="T96" id="Seg_7811" s="T95">who-VBZ-PRS-3PL</ta>
            <ta e="T97" id="Seg_7812" s="T96">then</ta>
            <ta e="T101" id="Seg_7813" s="T100">well</ta>
            <ta e="T102" id="Seg_7814" s="T101">there</ta>
            <ta e="T103" id="Seg_7815" s="T102">who-VBZ-PST1-1SG</ta>
            <ta e="T104" id="Seg_7816" s="T103">EMPH</ta>
            <ta e="T105" id="Seg_7817" s="T104">Murom-ABL</ta>
            <ta e="T106" id="Seg_7818" s="T105">who-DAT/LOC</ta>
            <ta e="T107" id="Seg_7819" s="T106">get.into-PST1-1PL</ta>
            <ta e="T108" id="Seg_7820" s="T107">that</ta>
            <ta e="T109" id="Seg_7821" s="T108">Tula-DAT/LOC</ta>
            <ta e="T110" id="Seg_7822" s="T109">that</ta>
            <ta e="T111" id="Seg_7823" s="T110">Tula.[NOM]</ta>
            <ta e="T115" id="Seg_7824" s="T114">EMPH</ta>
            <ta e="T116" id="Seg_7825" s="T115">eight</ta>
            <ta e="T117" id="Seg_7826" s="T116">kilometer-ACC</ta>
            <ta e="T118" id="Seg_7827" s="T117">Q</ta>
            <ta e="T119" id="Seg_7828" s="T118">how.much-ACC</ta>
            <ta e="T120" id="Seg_7829" s="T119">Q</ta>
            <ta e="T121" id="Seg_7830" s="T120">go-CVB.SEQ</ta>
            <ta e="T122" id="Seg_7831" s="T121">after</ta>
            <ta e="T123" id="Seg_7832" s="T122">iron</ta>
            <ta e="T124" id="Seg_7833" s="T123">path-PL-EP-2SG.[NOM]</ta>
            <ta e="T125" id="Seg_7834" s="T124">completely</ta>
            <ta e="T126" id="Seg_7835" s="T125">break-PST-3PL</ta>
            <ta e="T127" id="Seg_7836" s="T126">be-PST1-3SG</ta>
            <ta e="T128" id="Seg_7837" s="T127">well</ta>
            <ta e="T129" id="Seg_7838" s="T128">there</ta>
            <ta e="T130" id="Seg_7839" s="T129">who-VBZ-CVB.SIM</ta>
            <ta e="T131" id="Seg_7840" s="T130">lie-TEMP-1PL</ta>
            <ta e="T132" id="Seg_7841" s="T131">already</ta>
            <ta e="T134" id="Seg_7842" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_7843" s="T134">always</ta>
            <ta e="T136" id="Seg_7844" s="T135">fly-CVB.SIM</ta>
            <ta e="T139" id="Seg_7845" s="T138">fly-PTCP.HAB</ta>
            <ta e="T140" id="Seg_7846" s="T139">be-PST1-3PL</ta>
            <ta e="T141" id="Seg_7847" s="T140">morning.[NOM]</ta>
            <ta e="T142" id="Seg_7848" s="T141">every</ta>
            <ta e="T143" id="Seg_7849" s="T142">well</ta>
            <ta e="T144" id="Seg_7850" s="T143">then</ta>
            <ta e="T145" id="Seg_7851" s="T144">long</ta>
            <ta e="T146" id="Seg_7852" s="T145">NEG</ta>
            <ta e="T147" id="Seg_7853" s="T146">be-PST1-3SG</ta>
            <ta e="T148" id="Seg_7854" s="T147">hey</ta>
            <ta e="T151" id="Seg_7855" s="T150">who-VBZ-PRS-1PL</ta>
            <ta e="T152" id="Seg_7856" s="T151">thither</ta>
            <ta e="T153" id="Seg_7857" s="T152">indeed</ta>
            <ta e="T154" id="Seg_7858" s="T153">by.foot</ta>
            <ta e="T155" id="Seg_7859" s="T154">go-PST2-1PL</ta>
            <ta e="T156" id="Seg_7860" s="T155">that</ta>
            <ta e="T157" id="Seg_7861" s="T156">who-DAT/LOC</ta>
            <ta e="T158" id="Seg_7862" s="T157">where</ta>
            <ta e="T161" id="Seg_7863" s="T160">that.EMPH.[NOM]</ta>
            <ta e="T162" id="Seg_7864" s="T161">war.[NOM]</ta>
            <ta e="T163" id="Seg_7865" s="T162">be-PTCP.FUT</ta>
            <ta e="T164" id="Seg_7866" s="T163">place-3SG.[NOM]</ta>
            <ta e="T165" id="Seg_7867" s="T164">be-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T166" id="Seg_7868" s="T165">until</ta>
            <ta e="T167" id="Seg_7869" s="T166">well</ta>
            <ta e="T168" id="Seg_7870" s="T167">there</ta>
            <ta e="T169" id="Seg_7871" s="T168">who-VBZ-HAB-3PL</ta>
            <ta e="T170" id="Seg_7872" s="T169">EMPH</ta>
            <ta e="T171" id="Seg_7873" s="T170">who.[NOM]</ta>
            <ta e="T172" id="Seg_7874" s="T171">partizan</ta>
            <ta e="T173" id="Seg_7875" s="T172">that</ta>
            <ta e="T174" id="Seg_7876" s="T173">division</ta>
            <ta e="T175" id="Seg_7877" s="T174">oh</ta>
            <ta e="T176" id="Seg_7878" s="T175">who-DAT/LOC</ta>
            <ta e="T177" id="Seg_7879" s="T176">NOP</ta>
            <ta e="T178" id="Seg_7880" s="T177">SOP</ta>
            <ta e="T179" id="Seg_7881" s="T178">denote-VBZ-PRS-3PL</ta>
            <ta e="T180" id="Seg_7882" s="T179">that-ACC</ta>
            <ta e="T181" id="Seg_7883" s="T180">and</ta>
            <ta e="T182" id="Seg_7884" s="T181">this</ta>
            <ta e="T183" id="Seg_7885" s="T182">INTNS</ta>
            <ta e="T184" id="Seg_7886" s="T183">who-VBZ</ta>
            <ta e="T185" id="Seg_7887" s="T184">who-VBZ-PTCP.PRS-DAT/LOC</ta>
            <ta e="T186" id="Seg_7888" s="T185">river.[NOM]</ta>
            <ta e="T187" id="Seg_7889" s="T186">what</ta>
            <ta e="T188" id="Seg_7890" s="T187">INDEF</ta>
            <ta e="T189" id="Seg_7891" s="T188">river-DIM.[NOM]</ta>
            <ta e="T190" id="Seg_7892" s="T189">lie-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_7893" s="T190">well</ta>
            <ta e="T192" id="Seg_7894" s="T191">there</ta>
            <ta e="T193" id="Seg_7895" s="T192">village-DAT/LOC</ta>
            <ta e="T194" id="Seg_7896" s="T193">there</ta>
            <ta e="T196" id="Seg_7897" s="T195">make-PRS-3PL</ta>
            <ta e="T197" id="Seg_7898" s="T196">apparently</ta>
            <ta e="T198" id="Seg_7899" s="T197">in.the.morning</ta>
            <ta e="T199" id="Seg_7900" s="T198">who-VBZ-PST1-1PL</ta>
            <ta e="T200" id="Seg_7901" s="T199">eat-CVB.SIM</ta>
            <ta e="T201" id="Seg_7902" s="T200">eat-CVB.SIM</ta>
            <ta e="T202" id="Seg_7903" s="T201">fall-CVB.SEQ</ta>
            <ta e="T203" id="Seg_7904" s="T202">go-CVB.SEQ-1PL</ta>
            <ta e="T204" id="Seg_7905" s="T203">that-ACC</ta>
            <ta e="T205" id="Seg_7906" s="T204">well</ta>
            <ta e="T206" id="Seg_7907" s="T205">shoot-FREQ-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T207" id="Seg_7908" s="T206">commander-1PL-ACC</ta>
            <ta e="T208" id="Seg_7909" s="T207">tell-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_7910" s="T208">how</ta>
            <ta e="T212" id="Seg_7911" s="T211">who-VBZ-PTCP.FUT-2PL-ACC</ta>
            <ta e="T214" id="Seg_7912" s="T212">then</ta>
            <ta e="T215" id="Seg_7913" s="T214">well</ta>
            <ta e="T216" id="Seg_7914" s="T215">encirclement.[NOM]</ta>
            <ta e="T217" id="Seg_7915" s="T216">get.into-TEMP-2PL</ta>
            <ta e="T218" id="Seg_7916" s="T217">maybe</ta>
            <ta e="T219" id="Seg_7917" s="T218">imprisonment.[NOM]</ta>
            <ta e="T220" id="Seg_7918" s="T219">get.into-TEMP-2PL</ta>
            <ta e="T221" id="Seg_7919" s="T220">EMPH</ta>
            <ta e="T223" id="Seg_7920" s="T222">who.[NOM]</ta>
            <ta e="T224" id="Seg_7921" s="T223">who-VBZ-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T225" id="Seg_7922" s="T224">need.to</ta>
            <ta e="T226" id="Seg_7923" s="T225">be-PST1-3SG</ta>
            <ta e="T227" id="Seg_7924" s="T226">self-2SG-ACC</ta>
            <ta e="T228" id="Seg_7925" s="T227">give-EP-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T229" id="Seg_7926" s="T228">maybe</ta>
            <ta e="T230" id="Seg_7927" s="T229">einfach</ta>
            <ta e="T233" id="Seg_7928" s="T232">split-FREQ-NEG-IMP.2PL</ta>
            <ta e="T234" id="Seg_7929" s="T233">document</ta>
            <ta e="T235" id="Seg_7930" s="T234">eh</ta>
            <ta e="T236" id="Seg_7931" s="T235">document-PL-2PL.[NOM]</ta>
            <ta e="T237" id="Seg_7932" s="T236">say-PRS.[3SG]</ta>
            <ta e="T238" id="Seg_7933" s="T237">MOD</ta>
            <ta e="T241" id="Seg_7934" s="T240">who-VBZ-IMP.2PL</ta>
            <ta e="T244" id="Seg_7935" s="T243">eh</ta>
            <ta e="T245" id="Seg_7936" s="T244">bury-EP-IMP.2PL</ta>
            <ta e="T246" id="Seg_7937" s="T245">Q</ta>
            <ta e="T247" id="Seg_7938" s="T246">self-2PL.[NOM]</ta>
            <ta e="T248" id="Seg_7939" s="T247">eat-IMP.2PL</ta>
            <ta e="T249" id="Seg_7940" s="T248">Q</ta>
            <ta e="T250" id="Seg_7941" s="T249">say-HAB.[3SG]</ta>
            <ta e="T251" id="Seg_7942" s="T250">like.this</ta>
            <ta e="T252" id="Seg_7943" s="T251">split-FREQ-PST1-2SG</ta>
            <ta e="T253" id="Seg_7944" s="T252">and</ta>
            <ta e="T255" id="Seg_7945" s="T253">however</ta>
            <ta e="T256" id="Seg_7946" s="T255">glue-VBZ</ta>
            <ta e="T257" id="Seg_7947" s="T256">that.[NOM]</ta>
            <ta e="T258" id="Seg_7948" s="T257">who-VBZ-PRS-3PL</ta>
            <ta e="T259" id="Seg_7949" s="T258">EMPH</ta>
            <ta e="T260" id="Seg_7950" s="T259">that</ta>
            <ta e="T261" id="Seg_7951" s="T260">find-PRS-3PL</ta>
            <ta e="T262" id="Seg_7952" s="T261">EMPH</ta>
            <ta e="T263" id="Seg_7953" s="T262">that</ta>
            <ta e="T264" id="Seg_7954" s="T263">well</ta>
            <ta e="T265" id="Seg_7955" s="T264">company-2SG.[NOM]</ta>
            <ta e="T266" id="Seg_7956" s="T265">let-CVB.SIM</ta>
            <ta e="T267" id="Seg_7957" s="T266">run-PST2-1SG</ta>
            <ta e="T268" id="Seg_7958" s="T267">once</ta>
            <ta e="T271" id="Seg_7959" s="T270">what.kind.of-3PL-ACC</ta>
            <ta e="T272" id="Seg_7960" s="T271">INDEF</ta>
            <ta e="T273" id="Seg_7961" s="T272">notice-CVB.SEQ-1SG</ta>
            <ta e="T274" id="Seg_7962" s="T273">darkness-DAT/LOC</ta>
            <ta e="T275" id="Seg_7963" s="T274">German-3PL.[NOM]</ta>
            <ta e="T276" id="Seg_7964" s="T275">and</ta>
            <ta e="T277" id="Seg_7965" s="T276">who-3PL.[NOM]</ta>
            <ta e="T278" id="Seg_7966" s="T277">and</ta>
            <ta e="T279" id="Seg_7967" s="T278">well</ta>
            <ta e="T280" id="Seg_7968" s="T279">that</ta>
            <ta e="T281" id="Seg_7969" s="T280">1SG.[NOM]</ta>
            <ta e="T282" id="Seg_7970" s="T281">go-CVB.SEQ-1SG</ta>
            <ta e="T283" id="Seg_7971" s="T282">see-PST1-1SG</ta>
            <ta e="T284" id="Seg_7972" s="T283">five</ta>
            <ta e="T285" id="Seg_7973" s="T284">human.being.[NOM]</ta>
            <ta e="T286" id="Seg_7974" s="T285">go-PRS-3PL</ta>
            <ta e="T287" id="Seg_7975" s="T286">only</ta>
            <ta e="T288" id="Seg_7976" s="T287">like.this</ta>
            <ta e="T289" id="Seg_7977" s="T288">well</ta>
            <ta e="T290" id="Seg_7978" s="T289">then</ta>
            <ta e="T291" id="Seg_7979" s="T290">that</ta>
            <ta e="T292" id="Seg_7980" s="T291">side-ABL</ta>
            <ta e="T293" id="Seg_7981" s="T292">German-EP-PL-1PL.[NOM]</ta>
            <ta e="T294" id="Seg_7982" s="T293">come-PRS-3PL</ta>
            <ta e="T295" id="Seg_7983" s="T294">trench-ABL</ta>
            <ta e="T296" id="Seg_7984" s="T295">stand.up-PRS-3PL</ta>
            <ta e="T297" id="Seg_7985" s="T296">back</ta>
            <ta e="T298" id="Seg_7986" s="T297">escape-IMP.1PL</ta>
            <ta e="T299" id="Seg_7987" s="T298">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T300" id="Seg_7988" s="T299">who-VBZ-PRS-3PL</ta>
            <ta e="T301" id="Seg_7989" s="T300">that</ta>
            <ta e="T302" id="Seg_7990" s="T301">human.being-2SG.[NOM]</ta>
            <ta e="T303" id="Seg_7991" s="T302">commander-PL-PROPR-3PL</ta>
            <ta e="T304" id="Seg_7992" s="T303">that</ta>
            <ta e="T305" id="Seg_7993" s="T304">friend-PL-3SG.[NOM]</ta>
            <ta e="T306" id="Seg_7994" s="T305">different-PL.[NOM]</ta>
            <ta e="T307" id="Seg_7995" s="T306">then</ta>
            <ta e="T308" id="Seg_7996" s="T307">different</ta>
            <ta e="T309" id="Seg_7997" s="T308">part-ABL</ta>
            <ta e="T310" id="Seg_7998" s="T309">self-1SG.[NOM]</ta>
            <ta e="T313" id="Seg_7999" s="T312">friend-PL-1SG-ACC</ta>
            <ta e="T314" id="Seg_8000" s="T313">that</ta>
            <ta e="T315" id="Seg_8001" s="T314">let-CVB.SIM</ta>
            <ta e="T316" id="Seg_8002" s="T315">run-PST2-1SG</ta>
            <ta e="T317" id="Seg_8003" s="T316">apparently</ta>
            <ta e="T318" id="Seg_8004" s="T317">EMPH</ta>
            <ta e="T319" id="Seg_8005" s="T318">Q</ta>
            <ta e="T320" id="Seg_8006" s="T319">what.[NOM]</ta>
            <ta e="T321" id="Seg_8007" s="T320">Q</ta>
            <ta e="T322" id="Seg_8008" s="T321">retreat-VBZ-IMP.2PL</ta>
            <ta e="T323" id="Seg_8009" s="T322">say-PRS.[3SG]</ta>
            <ta e="T327" id="Seg_8010" s="T326">say-PRS.[3SG]</ta>
            <ta e="T328" id="Seg_8011" s="T327">commander-3PL.[NOM]</ta>
            <ta e="T329" id="Seg_8012" s="T328">then</ta>
            <ta e="T330" id="Seg_8013" s="T329">see-PST1-1SG</ta>
            <ta e="T331" id="Seg_8014" s="T330">well</ta>
            <ta e="T332" id="Seg_8015" s="T331">German-EP-PL-EP-2SG.[NOM]</ta>
            <ta e="T333" id="Seg_8016" s="T332">this.EMPH</ta>
            <ta e="T334" id="Seg_8017" s="T333">come-CVB.SEQ</ta>
            <ta e="T335" id="Seg_8018" s="T334">go-PRS-3PL</ta>
            <ta e="T336" id="Seg_8019" s="T335">what-ACC</ta>
            <ta e="T337" id="Seg_8020" s="T336">what-ACC</ta>
            <ta e="T338" id="Seg_8021" s="T337">utter-PRS-3PL</ta>
            <ta e="T339" id="Seg_8022" s="T338">chat-PRS-3PL</ta>
            <ta e="T340" id="Seg_8023" s="T339">back</ta>
            <ta e="T341" id="Seg_8024" s="T340">run-PRS-1SG</ta>
            <ta e="T342" id="Seg_8025" s="T341">friend-PL-EP-1SG.[NOM]</ta>
            <ta e="T343" id="Seg_8026" s="T342">go-PTCP.PST</ta>
            <ta e="T344" id="Seg_8027" s="T343">place-3PL-ACC</ta>
            <ta e="T345" id="Seg_8028" s="T344">to</ta>
            <ta e="T346" id="Seg_8029" s="T345">completely</ta>
            <ta e="T347" id="Seg_8030" s="T346">communicate-PRS-3PL</ta>
            <ta e="T348" id="Seg_8031" s="T347">only</ta>
            <ta e="T349" id="Seg_8032" s="T348">there</ta>
            <ta e="T350" id="Seg_8033" s="T349">grass-DAT/LOC</ta>
            <ta e="T351" id="Seg_8034" s="T350">shoot-FREQ-PRS-3PL</ta>
            <ta e="T352" id="Seg_8035" s="T351">indeed</ta>
            <ta e="T353" id="Seg_8036" s="T352">who-VBZ-CVB.PURP</ta>
            <ta e="T354" id="Seg_8037" s="T353">want-PRS-3PL</ta>
            <ta e="T355" id="Seg_8038" s="T354">apparently</ta>
            <ta e="T357" id="Seg_8039" s="T355">then</ta>
            <ta e="T358" id="Seg_8040" s="T357">imprisonment.[NOM]</ta>
            <ta e="T359" id="Seg_8041" s="T358">get-CVB.PURP</ta>
            <ta e="T360" id="Seg_8042" s="T359">end-EP-ABL</ta>
            <ta e="T361" id="Seg_8043" s="T360">trench-DAT/LOC</ta>
            <ta e="T362" id="Seg_8044" s="T361">come-PST1-1SG</ta>
            <ta e="T363" id="Seg_8045" s="T362">come-PST2-EP-1SG</ta>
            <ta e="T364" id="Seg_8046" s="T363">self-1SG.[NOM]</ta>
            <ta e="T365" id="Seg_8047" s="T364">friend</ta>
            <ta e="T366" id="Seg_8048" s="T365">eh</ta>
            <ta e="T367" id="Seg_8049" s="T366">company-1SG-DAT/LOC</ta>
            <ta e="T368" id="Seg_8050" s="T367">get.into-PST2-1SG</ta>
            <ta e="T369" id="Seg_8051" s="T368">well</ta>
            <ta e="T370" id="Seg_8052" s="T369">that</ta>
            <ta e="T371" id="Seg_8053" s="T370">who-VBZ-PRS-1SG</ta>
            <ta e="T372" id="Seg_8054" s="T371">well</ta>
            <ta e="T375" id="Seg_8055" s="T374">machine.gun-EP-INSTR</ta>
            <ta e="T376" id="Seg_8056" s="T375">who-VBZ-FUT.[3SG]</ta>
            <ta e="T377" id="Seg_8057" s="T376">immediately</ta>
            <ta e="T378" id="Seg_8058" s="T377">twenty-ABL</ta>
            <ta e="T379" id="Seg_8059" s="T378">over</ta>
            <ta e="T380" id="Seg_8060" s="T379">human.being-ACC</ta>
            <ta e="T381" id="Seg_8061" s="T380">fall-EP-CAUS-PST1-3PL</ta>
            <ta e="T382" id="Seg_8062" s="T381">some-PL-1PL.[NOM]</ta>
            <ta e="T383" id="Seg_8063" s="T382">back</ta>
            <ta e="T384" id="Seg_8064" s="T383">run-PST1-3PL</ta>
            <ta e="T385" id="Seg_8065" s="T384">that</ta>
            <ta e="T386" id="Seg_8066" s="T385">patrol.[NOM]</ta>
            <ta e="T387" id="Seg_8067" s="T386">be-HAB.[3SG]</ta>
            <ta e="T390" id="Seg_8068" s="T389">company-2SG.[NOM]</ta>
            <ta e="T391" id="Seg_8069" s="T390">go-FUT-3SG</ta>
            <ta e="T392" id="Seg_8070" s="T391">still</ta>
            <ta e="T393" id="Seg_8071" s="T392">who.[NOM]</ta>
            <ta e="T394" id="Seg_8072" s="T393">in.front</ta>
            <ta e="T395" id="Seg_8073" s="T394">go-FUT-3SG</ta>
            <ta e="T396" id="Seg_8074" s="T395">scout.[NOM]</ta>
            <ta e="T397" id="Seg_8075" s="T396">similar</ta>
            <ta e="T398" id="Seg_8076" s="T397">company-2SG-ACC</ta>
            <ta e="T399" id="Seg_8077" s="T398">only</ta>
            <ta e="T400" id="Seg_8078" s="T399">guard-PRS-2SG</ta>
            <ta e="T401" id="Seg_8079" s="T400">EMPH</ta>
            <ta e="T402" id="Seg_8080" s="T401">front-PL.[NOM]</ta>
            <ta e="T403" id="Seg_8081" s="T402">two</ta>
            <ta e="T404" id="Seg_8082" s="T403">human.being.[NOM]</ta>
            <ta e="T405" id="Seg_8083" s="T404">be-PRS.[3SG]</ta>
            <ta e="T406" id="Seg_8084" s="T405">left</ta>
            <ta e="T407" id="Seg_8085" s="T406">to</ta>
            <ta e="T408" id="Seg_8086" s="T407">right</ta>
            <ta e="T409" id="Seg_8087" s="T408">to</ta>
            <ta e="T410" id="Seg_8088" s="T409">again</ta>
            <ta e="T411" id="Seg_8089" s="T410">two-DISTR</ta>
            <ta e="T412" id="Seg_8090" s="T411">human.being.[NOM]</ta>
            <ta e="T413" id="Seg_8091" s="T412">well</ta>
            <ta e="T414" id="Seg_8092" s="T413">whereto</ta>
            <ta e="T415" id="Seg_8093" s="T414">be-FUT-1PL=Q</ta>
            <ta e="T416" id="Seg_8094" s="T415">say-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_8095" s="T416">this</ta>
            <ta e="T418" id="Seg_8096" s="T417">village-DAT/LOC</ta>
            <ta e="T419" id="Seg_8097" s="T418">check-VBZ-PRS-1PL</ta>
            <ta e="T420" id="Seg_8098" s="T419">indeed</ta>
            <ta e="T421" id="Seg_8099" s="T420">who-VBZ</ta>
            <ta e="T422" id="Seg_8100" s="T421">who.[NOM]</ta>
            <ta e="T423" id="Seg_8101" s="T422">NEG</ta>
            <ta e="T424" id="Seg_8102" s="T423">NEG.EX</ta>
            <ta e="T426" id="Seg_8103" s="T425">eh</ta>
            <ta e="T427" id="Seg_8104" s="T426">German-EP-PL.[NOM]</ta>
            <ta e="T428" id="Seg_8105" s="T427">on.the.other.shore</ta>
            <ta e="T429" id="Seg_8106" s="T428">just</ta>
            <ta e="T430" id="Seg_8107" s="T429">to.be.on.view-PRS-3PL</ta>
            <ta e="T431" id="Seg_8108" s="T430">back</ta>
            <ta e="T432" id="Seg_8109" s="T431">go-IMP.1PL</ta>
            <ta e="T433" id="Seg_8110" s="T432">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T434" id="Seg_8111" s="T433">make-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_8112" s="T434">well</ta>
            <ta e="T436" id="Seg_8113" s="T435">that</ta>
            <ta e="T437" id="Seg_8114" s="T436">who-VBZ-CVB.SIM</ta>
            <ta e="T438" id="Seg_8115" s="T437">go-CVB.SEQ-1PL</ta>
            <ta e="T439" id="Seg_8116" s="T438">meet-PST1-1PL</ta>
            <ta e="T440" id="Seg_8117" s="T439">that</ta>
            <ta e="T441" id="Seg_8118" s="T440">that</ta>
            <ta e="T442" id="Seg_8119" s="T441">river-1PL-ACC</ta>
            <ta e="T443" id="Seg_8120" s="T442">across</ta>
            <ta e="T444" id="Seg_8121" s="T443">go-PST1-1PL</ta>
            <ta e="T445" id="Seg_8122" s="T444">stream.[NOM]</ta>
            <ta e="T446" id="Seg_8123" s="T445">across</ta>
            <ta e="T447" id="Seg_8124" s="T446">river-VBZ-CVB.SEQ</ta>
            <ta e="T448" id="Seg_8125" s="T447">eh</ta>
            <ta e="T449" id="Seg_8126" s="T448">go-CVB.SEQ-1PL</ta>
            <ta e="T450" id="Seg_8127" s="T449">eh</ta>
            <ta e="T451" id="Seg_8128" s="T450">who-VBZ-PRS-1PL</ta>
            <ta e="T452" id="Seg_8129" s="T451">like.this</ta>
            <ta e="T453" id="Seg_8130" s="T452">who-3PL.[NOM]</ta>
            <ta e="T454" id="Seg_8131" s="T453">who-VBZ-PST2-3PL</ta>
            <ta e="T455" id="Seg_8132" s="T454">this</ta>
            <ta e="T456" id="Seg_8133" s="T455">artillery.[NOM]</ta>
            <ta e="T457" id="Seg_8134" s="T456">be-HAB.[3SG]</ta>
            <ta e="T458" id="Seg_8135" s="T457">EMPH</ta>
            <ta e="T459" id="Seg_8136" s="T458">cannon-3PL.[NOM]</ta>
            <ta e="T462" id="Seg_8137" s="T461">eh</ta>
            <ta e="T463" id="Seg_8138" s="T462">who-3PL.[NOM]</ta>
            <ta e="T464" id="Seg_8139" s="T463">horse-PL.[NOM]</ta>
            <ta e="T465" id="Seg_8140" s="T464">completely</ta>
            <ta e="T466" id="Seg_8141" s="T465">who-VBZ-EP-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T467" id="Seg_8142" s="T466">like.this</ta>
            <ta e="T468" id="Seg_8143" s="T467">turn.around-CVB.SIM</ta>
            <ta e="T469" id="Seg_8144" s="T468">who-VBZ-PST2-3PL</ta>
            <ta e="T470" id="Seg_8145" s="T469">then</ta>
            <ta e="T471" id="Seg_8146" s="T470">kill-ITER-PST2-3PL</ta>
            <ta e="T472" id="Seg_8147" s="T471">there</ta>
            <ta e="T473" id="Seg_8148" s="T472">one</ta>
            <ta e="T474" id="Seg_8149" s="T473">human.being-ACC</ta>
            <ta e="T475" id="Seg_8150" s="T474">find-PST1-1PL</ta>
            <ta e="T8" id="Seg_8151" s="T475">half_alive.[NOM]</ta>
            <ta e="T476" id="Seg_8152" s="T8">indeed</ta>
            <ta e="T477" id="Seg_8153" s="T476">whereto</ta>
            <ta e="T478" id="Seg_8154" s="T477">INDEF</ta>
            <ta e="T479" id="Seg_8155" s="T478">hardly</ta>
            <ta e="T480" id="Seg_8156" s="T479">speak-PRS.[3SG]</ta>
            <ta e="T482" id="Seg_8157" s="T480">then</ta>
            <ta e="T483" id="Seg_8158" s="T482">this</ta>
            <ta e="T484" id="Seg_8159" s="T483">just</ta>
            <ta e="T485" id="Seg_8160" s="T484">tobacco.[NOM]</ta>
            <ta e="T486" id="Seg_8161" s="T485">pull-PTCP.PRS</ta>
            <ta e="T487" id="Seg_8162" s="T486">proximity-3SG.[NOM]</ta>
            <ta e="T488" id="Seg_8163" s="T487">be-PST1-3SG</ta>
            <ta e="T489" id="Seg_8164" s="T488">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T490" id="Seg_8165" s="T489">make-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_8166" s="T490">1PL.[NOM]</ta>
            <ta e="T492" id="Seg_8167" s="T491">company-1PL-ACC</ta>
            <ta e="T493" id="Seg_8168" s="T492">who-VBZ-PST2-3PL</ta>
            <ta e="T494" id="Seg_8169" s="T493">say-PRS.[3SG]</ta>
            <ta e="T495" id="Seg_8170" s="T494">German-3SG.[NOM]</ta>
            <ta e="T496" id="Seg_8171" s="T495">AFFIRM</ta>
            <ta e="T497" id="Seg_8172" s="T496">go-PST2-3PL</ta>
            <ta e="T498" id="Seg_8173" s="T497">now-INTNS</ta>
            <ta e="T499" id="Seg_8174" s="T498">say-PRS.[3SG]</ta>
            <ta e="T500" id="Seg_8175" s="T499">speak-RECP/COLL-EP-NEG-IMP.2PL</ta>
            <ta e="T501" id="Seg_8176" s="T500">loud-ADVZ</ta>
            <ta e="T502" id="Seg_8177" s="T501">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T503" id="Seg_8178" s="T502">make-PRS.[3SG]</ta>
            <ta e="T504" id="Seg_8179" s="T503">well</ta>
            <ta e="T505" id="Seg_8180" s="T504">that</ta>
            <ta e="T507" id="Seg_8181" s="T505">who-VBZ-PST1-1PL</ta>
            <ta e="T508" id="Seg_8182" s="T507">well</ta>
            <ta e="T509" id="Seg_8183" s="T508">there</ta>
            <ta e="T510" id="Seg_8184" s="T509">EMPH</ta>
            <ta e="T511" id="Seg_8185" s="T510">eh</ta>
            <ta e="T512" id="Seg_8186" s="T511">friend</ta>
            <ta e="T513" id="Seg_8187" s="T512">two</ta>
            <ta e="T514" id="Seg_8188" s="T513">human.being-ACC</ta>
            <ta e="T515" id="Seg_8189" s="T514">send-PRS-3PL</ta>
            <ta e="T516" id="Seg_8190" s="T515">there</ta>
            <ta e="T517" id="Seg_8191" s="T516">who.[NOM]</ta>
            <ta e="T518" id="Seg_8192" s="T517">be-HAB.[3SG]</ta>
            <ta e="T519" id="Seg_8193" s="T518">infirmary.[NOM]</ta>
            <ta e="T520" id="Seg_8194" s="T519">then</ta>
            <ta e="T521" id="Seg_8195" s="T520">take.away-FUT-1PL</ta>
            <ta e="T522" id="Seg_8196" s="T521">say-PRS-3PL</ta>
            <ta e="T523" id="Seg_8197" s="T522">1SG.[NOM]</ta>
            <ta e="T524" id="Seg_8198" s="T523">1PL.[NOM]</ta>
            <ta e="T525" id="Seg_8199" s="T524">2SG-ACC</ta>
            <ta e="T528" id="Seg_8200" s="T527">1SG-ACC</ta>
            <ta e="T529" id="Seg_8201" s="T528">what.[NOM]</ta>
            <ta e="T530" id="Seg_8202" s="T529">worry-EP-NEG-IMP.2PL</ta>
            <ta e="T531" id="Seg_8203" s="T530">say-PRS.[3SG]</ta>
            <ta e="T532" id="Seg_8204" s="T531">1SG.[NOM]</ta>
            <ta e="T533" id="Seg_8205" s="T532">eh</ta>
            <ta e="T534" id="Seg_8206" s="T533">however</ta>
            <ta e="T535" id="Seg_8207" s="T534">human.being.[NOM]</ta>
            <ta e="T536" id="Seg_8208" s="T535">be-PTCP.FUT</ta>
            <ta e="T537" id="Seg_8209" s="T536">eh</ta>
            <ta e="T538" id="Seg_8210" s="T537">human.being.[NOM]</ta>
            <ta e="T539" id="Seg_8211" s="T538">be-PST2.NEG-1SG</ta>
            <ta e="T540" id="Seg_8212" s="T539">say-PRS.[3SG]</ta>
            <ta e="T541" id="Seg_8213" s="T540">EMPH</ta>
            <ta e="T542" id="Seg_8214" s="T541">die-FUT</ta>
            <ta e="T543" id="Seg_8215" s="T542">die-FUT-1SG</ta>
            <ta e="T544" id="Seg_8216" s="T543">now</ta>
            <ta e="T545" id="Seg_8217" s="T544">human.being-PL.[NOM]</ta>
            <ta e="T546" id="Seg_8218" s="T545">padding.[NOM]</ta>
            <ta e="T547" id="Seg_8219" s="T546">similar</ta>
            <ta e="T548" id="Seg_8220" s="T547">lie-PRS-3PL</ta>
            <ta e="T549" id="Seg_8221" s="T548">indeed</ta>
            <ta e="T550" id="Seg_8222" s="T549">that-PL-ABL</ta>
            <ta e="T551" id="Seg_8223" s="T550">EMPH-then</ta>
            <ta e="T552" id="Seg_8224" s="T551">that</ta>
            <ta e="T553" id="Seg_8225" s="T552">who-VBZ-PST1-3PL</ta>
            <ta e="T554" id="Seg_8226" s="T553">that-3SG-2SG-ACC</ta>
            <ta e="T555" id="Seg_8227" s="T554">carry-PST1-3PL</ta>
            <ta e="T556" id="Seg_8228" s="T555">two</ta>
            <ta e="T557" id="Seg_8229" s="T556">human.being.[NOM]</ta>
            <ta e="T558" id="Seg_8230" s="T557">who-DAT/LOC</ta>
            <ta e="T559" id="Seg_8231" s="T558">that-3SG-2SG.[NOM]</ta>
            <ta e="T560" id="Seg_8232" s="T559">then</ta>
            <ta e="T561" id="Seg_8233" s="T560">say-PST2.[3SG]</ta>
            <ta e="T562" id="Seg_8234" s="T561">it.is.said</ta>
            <ta e="T563" id="Seg_8235" s="T562">that</ta>
            <ta e="T565" id="Seg_8236" s="T563">however</ta>
            <ta e="T566" id="Seg_8237" s="T565">bring-EP-NEG.[IMP.2SG]</ta>
            <ta e="T567" id="Seg_8238" s="T566">eh</ta>
            <ta e="T568" id="Seg_8239" s="T567">who-VBZ-CVB.SEQ</ta>
            <ta e="T569" id="Seg_8240" s="T568">that</ta>
            <ta e="T570" id="Seg_8241" s="T569">then</ta>
            <ta e="T571" id="Seg_8242" s="T570">back</ta>
            <ta e="T572" id="Seg_8243" s="T571">come-PST1-3PL</ta>
            <ta e="T573" id="Seg_8244" s="T572">that-PL-EP-2SG.[NOM]</ta>
            <ta e="T574" id="Seg_8245" s="T573">anyway</ta>
            <ta e="T575" id="Seg_8246" s="T574">human.being.[NOM]</ta>
            <ta e="T576" id="Seg_8247" s="T575">be-FUT-1SG</ta>
            <ta e="T577" id="Seg_8248" s="T576">NEG-3SG</ta>
            <ta e="T578" id="Seg_8249" s="T577">what-ACC</ta>
            <ta e="T579" id="Seg_8250" s="T578">1SG-ACC</ta>
            <ta e="T580" id="Seg_8251" s="T579">with</ta>
            <ta e="T583" id="Seg_8252" s="T582">who-VBZ-2PL</ta>
            <ta e="T584" id="Seg_8253" s="T583">say-PRS.[3SG]</ta>
            <ta e="T585" id="Seg_8254" s="T584">it.is.said</ta>
            <ta e="T586" id="Seg_8255" s="T585">in.the.morning</ta>
            <ta e="T587" id="Seg_8256" s="T586">dawn-1PL.[NOM]</ta>
            <ta e="T588" id="Seg_8257" s="T587">just</ta>
            <ta e="T589" id="Seg_8258" s="T588">go.out-PRS.[3SG]</ta>
            <ta e="T590" id="Seg_8259" s="T589">go.out-CVB.SEQ</ta>
            <ta e="T593" id="Seg_8260" s="T592">eh</ta>
            <ta e="T594" id="Seg_8261" s="T593">who-VBZ-CVB.SEQ</ta>
            <ta e="T595" id="Seg_8262" s="T594">be-TEMP-3SG</ta>
            <ta e="T596" id="Seg_8263" s="T595">what.[NOM]</ta>
            <ta e="T597" id="Seg_8264" s="T596">INDEF</ta>
            <ta e="T598" id="Seg_8265" s="T597">lieutenant-ACC</ta>
            <ta e="T599" id="Seg_8266" s="T598">meet-PST1-1PL</ta>
            <ta e="T600" id="Seg_8267" s="T599">that-3SG-1PL.[NOM]</ta>
            <ta e="T601" id="Seg_8268" s="T600">say-PRS.[3SG]</ta>
            <ta e="T602" id="Seg_8269" s="T601">what.kind.of</ta>
            <ta e="T603" id="Seg_8270" s="T602">part-ABL-2SG=Q</ta>
            <ta e="T604" id="Seg_8271" s="T603">2PL.[NOM]</ta>
            <ta e="T605" id="Seg_8272" s="T604">2PL-ABL</ta>
            <ta e="T606" id="Seg_8273" s="T605">say-PRS-1PL</ta>
            <ta e="T607" id="Seg_8274" s="T606">say-PRS.[3SG]</ta>
            <ta e="T608" id="Seg_8275" s="T607">then</ta>
            <ta e="T609" id="Seg_8276" s="T608">one-1PL.[NOM]</ta>
            <ta e="T610" id="Seg_8277" s="T609">then</ta>
            <ta e="T611" id="Seg_8278" s="T610">then</ta>
            <ta e="T612" id="Seg_8279" s="T611">1SG-ACC</ta>
            <ta e="T613" id="Seg_8280" s="T612">with</ta>
            <ta e="T614" id="Seg_8281" s="T613">run-EP-IMP.2PL</ta>
            <ta e="T615" id="Seg_8282" s="T614">say-PRS.[3SG]</ta>
            <ta e="T616" id="Seg_8283" s="T615">front.[NOM]</ta>
            <ta e="T617" id="Seg_8284" s="T616">to</ta>
            <ta e="T619" id="Seg_8285" s="T618">just</ta>
            <ta e="T622" id="Seg_8286" s="T619">who-VBZ-NEG-PTCP.FUT-DAT/LOC</ta>
            <ta e="T623" id="Seg_8287" s="T622">connection.[NOM]</ta>
            <ta e="T624" id="Seg_8288" s="T623">take-CVB.SIM</ta>
            <ta e="T625" id="Seg_8289" s="T624">who-VBZ-PRS.[3SG]</ta>
            <ta e="T626" id="Seg_8290" s="T625">apparently</ta>
            <ta e="T627" id="Seg_8291" s="T626">well</ta>
            <ta e="T628" id="Seg_8292" s="T627">then</ta>
            <ta e="T629" id="Seg_8293" s="T628">that</ta>
            <ta e="T630" id="Seg_8294" s="T629">who-VBZ-CVB.SIM</ta>
            <ta e="T631" id="Seg_8295" s="T630">try-PRS-1PL</ta>
            <ta e="T632" id="Seg_8296" s="T631">how.much</ta>
            <ta e="T633" id="Seg_8297" s="T632">INDEF</ta>
            <ta e="T634" id="Seg_8298" s="T633">day-ACC</ta>
            <ta e="T635" id="Seg_8299" s="T634">that</ta>
            <ta e="T636" id="Seg_8300" s="T635">go-PST1-1PL</ta>
            <ta e="T637" id="Seg_8301" s="T636">that.[NOM]</ta>
            <ta e="T638" id="Seg_8302" s="T637">like</ta>
            <ta e="T639" id="Seg_8303" s="T638">company-1PL-ACC</ta>
            <ta e="T640" id="Seg_8304" s="T639">NEG</ta>
            <ta e="T641" id="Seg_8305" s="T640">find-NEG-1PL</ta>
            <ta e="T642" id="Seg_8306" s="T641">well</ta>
            <ta e="T643" id="Seg_8307" s="T642">then</ta>
            <ta e="T644" id="Seg_8308" s="T643">late-ADJZ-3SG-ACC</ta>
            <ta e="T645" id="Seg_8309" s="T644">who-VBZ-PST1</ta>
            <ta e="T646" id="Seg_8310" s="T645">eh</ta>
            <ta e="T647" id="Seg_8311" s="T646">find-PST1-1PL</ta>
            <ta e="T648" id="Seg_8312" s="T647">again</ta>
            <ta e="T649" id="Seg_8313" s="T648">AFFIRM</ta>
            <ta e="T652" id="Seg_8314" s="T651">three-ORD</ta>
            <ta e="T653" id="Seg_8315" s="T652">day-1PL-DAT/LOC</ta>
            <ta e="T654" id="Seg_8316" s="T653">Q</ta>
            <ta e="T655" id="Seg_8317" s="T654">how.much-ORD-1PL-DAT/LOC</ta>
            <ta e="T656" id="Seg_8318" s="T655">Q</ta>
            <ta e="T657" id="Seg_8319" s="T656">away</ta>
            <ta e="T658" id="Seg_8320" s="T657">go-PRS-1PL</ta>
            <ta e="T659" id="Seg_8321" s="T658">thither</ta>
            <ta e="T660" id="Seg_8322" s="T659">who-DAT/LOC</ta>
            <ta e="T661" id="Seg_8323" s="T660">that</ta>
            <ta e="T662" id="Seg_8324" s="T661">river-1PL-ACC</ta>
            <ta e="T663" id="Seg_8325" s="T662">go.out-PRS-1PL</ta>
            <ta e="T664" id="Seg_8326" s="T663">well</ta>
            <ta e="T665" id="Seg_8327" s="T664">then</ta>
            <ta e="T666" id="Seg_8328" s="T665">that</ta>
            <ta e="T667" id="Seg_8329" s="T666">meet-PRS-1PL</ta>
            <ta e="T668" id="Seg_8330" s="T667">that</ta>
            <ta e="T669" id="Seg_8331" s="T668">remote-ABL</ta>
            <ta e="T670" id="Seg_8332" s="T669">shoot-FREQ-PRS-3PL</ta>
            <ta e="T671" id="Seg_8333" s="T670">and</ta>
            <ta e="T672" id="Seg_8334" s="T671">1PL.[NOM]</ta>
            <ta e="T673" id="Seg_8335" s="T672">that.side-ABL</ta>
            <ta e="T674" id="Seg_8336" s="T673">again</ta>
            <ta e="T675" id="Seg_8337" s="T674">shoot-FREQ-EP-RECP/COLL-PRS-1PL</ta>
            <ta e="T676" id="Seg_8338" s="T675">MOD</ta>
            <ta e="T677" id="Seg_8339" s="T676">well</ta>
            <ta e="T678" id="Seg_8340" s="T677">that</ta>
            <ta e="T679" id="Seg_8341" s="T678">who-VBZ-PST1-1PL</ta>
            <ta e="T680" id="Seg_8342" s="T679">and</ta>
            <ta e="T681" id="Seg_8343" s="T680">there</ta>
            <ta e="T682" id="Seg_8344" s="T681">whereto</ta>
            <ta e="T683" id="Seg_8345" s="T682">NEG</ta>
            <ta e="T684" id="Seg_8346" s="T683">who-VBZ-NEG-3PL</ta>
            <ta e="T685" id="Seg_8347" s="T684">release-NEG-3PL</ta>
            <ta e="T686" id="Seg_8348" s="T685">that</ta>
            <ta e="T687" id="Seg_8349" s="T686">in.the.morning</ta>
            <ta e="T688" id="Seg_8350" s="T687">ability-PROPR.[NOM]</ta>
            <ta e="T689" id="Seg_8351" s="T688">come-PST1-3PL</ta>
            <ta e="T690" id="Seg_8352" s="T689">that.[NOM]</ta>
            <ta e="T691" id="Seg_8353" s="T690">to</ta>
            <ta e="T692" id="Seg_8354" s="T691">front-1PL-ABL</ta>
            <ta e="T693" id="Seg_8355" s="T692">this</ta>
            <ta e="T696" id="Seg_8356" s="T695">command.[NOM]</ta>
            <ta e="T697" id="Seg_8357" s="T696">give-PRS-3PL</ta>
            <ta e="T698" id="Seg_8358" s="T697">so</ta>
            <ta e="T699" id="Seg_8359" s="T698">then</ta>
            <ta e="T700" id="Seg_8360" s="T699">three</ta>
            <ta e="T701" id="Seg_8361" s="T700">hour-DAT/LOC</ta>
            <ta e="T702" id="Seg_8362" s="T701">Bryansk-ACC</ta>
            <ta e="T703" id="Seg_8363" s="T702">conquer-VBZ-PTCP.FUT-2PL-ACC</ta>
            <ta e="T704" id="Seg_8364" s="T703">need.to</ta>
            <ta e="T705" id="Seg_8365" s="T704">well</ta>
            <ta e="T706" id="Seg_8366" s="T705">there</ta>
            <ta e="T707" id="Seg_8367" s="T706">like.this</ta>
            <ta e="T708" id="Seg_8368" s="T707">what.[NOM]</ta>
            <ta e="T709" id="Seg_8369" s="T708">INDEF</ta>
            <ta e="T710" id="Seg_8370" s="T709">mountain.[NOM]</ta>
            <ta e="T711" id="Seg_8371" s="T710">be-PRS.[3SG]</ta>
            <ta e="T712" id="Seg_8372" s="T711">small.belt.[NOM]</ta>
            <ta e="T715" id="Seg_8373" s="T714">small.belt-ACC</ta>
            <ta e="T716" id="Seg_8374" s="T715">eh</ta>
            <ta e="T717" id="Seg_8375" s="T716">who-VBZ-PST1-1PL</ta>
            <ta e="T718" id="Seg_8376" s="T717">and</ta>
            <ta e="T719" id="Seg_8377" s="T718">also</ta>
            <ta e="T720" id="Seg_8378" s="T719">again</ta>
            <ta e="T721" id="Seg_8379" s="T720">face-3SG.[NOM]</ta>
            <ta e="T724" id="Seg_8380" s="T723">front-EP-ABL</ta>
            <ta e="T725" id="Seg_8381" s="T724">who-VBZ-PRS-3PL</ta>
            <ta e="T726" id="Seg_8382" s="T725">EMPH</ta>
            <ta e="T729" id="Seg_8383" s="T728">German-PL.[NOM]</ta>
            <ta e="T730" id="Seg_8384" s="T729">like.this</ta>
            <ta e="T731" id="Seg_8385" s="T730">lie-PRS-3PL</ta>
            <ta e="T733" id="Seg_8386" s="T732">make-CVB.SIM</ta>
            <ta e="T734" id="Seg_8387" s="T733">lie-PRS-3PL</ta>
            <ta e="T735" id="Seg_8388" s="T734">EMPH</ta>
            <ta e="T736" id="Seg_8389" s="T735">and</ta>
            <ta e="T737" id="Seg_8390" s="T736">1PL.[NOM]</ta>
            <ta e="T738" id="Seg_8391" s="T737">this</ta>
            <ta e="T739" id="Seg_8392" s="T738">side-EP-ABL</ta>
            <ta e="T740" id="Seg_8393" s="T739">also</ta>
            <ta e="T741" id="Seg_8394" s="T740">shoot-FREQ-EP-RECP/COLL-PRS-1PL</ta>
            <ta e="T742" id="Seg_8395" s="T741">well</ta>
            <ta e="T743" id="Seg_8396" s="T742">then</ta>
            <ta e="T744" id="Seg_8397" s="T743">this</ta>
            <ta e="T745" id="Seg_8398" s="T744">night-ACC</ta>
            <ta e="T746" id="Seg_8399" s="T745">through</ta>
            <ta e="T747" id="Seg_8400" s="T746">lie-CVB.SIM</ta>
            <ta e="T748" id="Seg_8401" s="T747">here</ta>
            <ta e="T749" id="Seg_8402" s="T748">EMPH</ta>
            <ta e="T750" id="Seg_8403" s="T749">night-ACC</ta>
            <ta e="T751" id="Seg_8404" s="T750">ten-PL.[NOM]</ta>
            <ta e="T752" id="Seg_8405" s="T751">Q</ta>
            <ta e="T753" id="Seg_8406" s="T752">two-PL.[NOM]</ta>
            <ta e="T754" id="Seg_8407" s="T753">to</ta>
            <ta e="T755" id="Seg_8408" s="T754">Q</ta>
            <ta e="T756" id="Seg_8409" s="T755">or</ta>
            <ta e="T757" id="Seg_8410" s="T756">hour-PL.[NOM]</ta>
            <ta e="T758" id="Seg_8411" s="T757">to</ta>
            <ta e="T759" id="Seg_8412" s="T758">Q</ta>
            <ta e="T760" id="Seg_8413" s="T759">that</ta>
            <ta e="T761" id="Seg_8414" s="T760">missile-ACC</ta>
            <ta e="T762" id="Seg_8415" s="T761">send-PRS-3PL</ta>
            <ta e="T763" id="Seg_8416" s="T762">missile-ACC</ta>
            <ta e="T764" id="Seg_8417" s="T763">send-TEMP-3SG</ta>
            <ta e="T765" id="Seg_8418" s="T764">then</ta>
            <ta e="T766" id="Seg_8419" s="T765">this</ta>
            <ta e="T767" id="Seg_8420" s="T766">completely</ta>
            <ta e="T769" id="Seg_8421" s="T768">who-VBZ-CVB.SIM</ta>
            <ta e="T770" id="Seg_8422" s="T769">lie-PTCP.PRS-PL.[NOM]</ta>
            <ta e="T771" id="Seg_8423" s="T770">completely</ta>
            <ta e="T772" id="Seg_8424" s="T771">to.be.on.view-PRS-3PL</ta>
            <ta e="T773" id="Seg_8425" s="T772">hand-1SG-ACC</ta>
            <ta e="T774" id="Seg_8426" s="T773">strike-CAUS-CVB.SEQ</ta>
            <ta e="T775" id="Seg_8427" s="T774">this</ta>
            <ta e="T776" id="Seg_8428" s="T775">notice-NEG-PST1-1SG</ta>
            <ta e="T777" id="Seg_8429" s="T776">then</ta>
            <ta e="T778" id="Seg_8430" s="T777">that</ta>
            <ta e="T779" id="Seg_8431" s="T778">this-3SG-1SG-DAT/LOC</ta>
            <ta e="T780" id="Seg_8432" s="T779">strike</ta>
            <ta e="T781" id="Seg_8433" s="T780">who-VBZ-PST2.[3SG]</ta>
            <ta e="T782" id="Seg_8434" s="T781">that</ta>
            <ta e="T783" id="Seg_8435" s="T782">straight</ta>
            <ta e="T784" id="Seg_8436" s="T783">fly-CVB.SEQ</ta>
            <ta e="T785" id="Seg_8437" s="T784">one</ta>
            <ta e="T786" id="Seg_8438" s="T785">mine.[NOM]</ta>
            <ta e="T787" id="Seg_8439" s="T786">come-PST1-3SG</ta>
            <ta e="T788" id="Seg_8440" s="T787">and</ta>
            <ta e="T789" id="Seg_8441" s="T788">every-1PL-ACC</ta>
            <ta e="T790" id="Seg_8442" s="T789">hit-PST2.[3SG]</ta>
            <ta e="T791" id="Seg_8443" s="T790">that</ta>
            <ta e="T792" id="Seg_8444" s="T791">ten</ta>
            <ta e="T793" id="Seg_8445" s="T792">more</ta>
            <ta e="T794" id="Seg_8446" s="T793">human.being-ACC</ta>
            <ta e="T795" id="Seg_8447" s="T794">rifle-1SG-ACC</ta>
            <ta e="T796" id="Seg_8448" s="T795">like.this</ta>
            <ta e="T797" id="Seg_8449" s="T796">hold-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T798" id="Seg_8450" s="T797">mind-VBZ-PST1-1SG</ta>
            <ta e="T799" id="Seg_8451" s="T798">then</ta>
            <ta e="T800" id="Seg_8452" s="T799">so</ta>
            <ta e="T801" id="Seg_8453" s="T800">so</ta>
            <ta e="T802" id="Seg_8454" s="T801">stand-PRS-1SG</ta>
            <ta e="T803" id="Seg_8455" s="T802">what.[NOM]</ta>
            <ta e="T804" id="Seg_8456" s="T803">knee-SIM</ta>
            <ta e="T805" id="Seg_8457" s="T804">well</ta>
            <ta e="T806" id="Seg_8458" s="T805">then</ta>
            <ta e="T807" id="Seg_8459" s="T806">fall-PST1-1SG</ta>
            <ta e="T808" id="Seg_8460" s="T807">like.this</ta>
            <ta e="T809" id="Seg_8461" s="T808">well</ta>
            <ta e="T810" id="Seg_8462" s="T809">then</ta>
            <ta e="T811" id="Seg_8463" s="T810">later</ta>
            <ta e="T812" id="Seg_8464" s="T811">see-PST1-1SG</ta>
            <ta e="T813" id="Seg_8465" s="T812">EMPH</ta>
            <ta e="T814" id="Seg_8466" s="T813">finger-PL-EP-1SG.[NOM]</ta>
            <ta e="T815" id="Seg_8467" s="T814">completely</ta>
            <ta e="T816" id="Seg_8468" s="T815">shiver-CVB.SIM</ta>
            <ta e="T817" id="Seg_8469" s="T816">go-PRS-3PL</ta>
            <ta e="T818" id="Seg_8470" s="T817">blood-EP-1SG.[NOM]</ta>
            <ta e="T819" id="Seg_8471" s="T818">flow-CVB.SIM</ta>
            <ta e="T820" id="Seg_8472" s="T819">lie-PRS.[3SG]</ta>
            <ta e="T821" id="Seg_8473" s="T820">like.this</ta>
            <ta e="T822" id="Seg_8474" s="T821">well</ta>
            <ta e="T823" id="Seg_8475" s="T822">then</ta>
            <ta e="T824" id="Seg_8476" s="T823">belt-1SG-ACC</ta>
            <ta e="T825" id="Seg_8477" s="T824">undress-PST1-1SG</ta>
            <ta e="T826" id="Seg_8478" s="T825">like.this</ta>
            <ta e="T827" id="Seg_8479" s="T826">belt-1SG-ACC</ta>
            <ta e="T828" id="Seg_8480" s="T827">undress-CVB.SEQ</ta>
            <ta e="T829" id="Seg_8481" s="T828">go-CVB.SEQ-1SG</ta>
            <ta e="T830" id="Seg_8482" s="T829">like.this</ta>
            <ta e="T831" id="Seg_8483" s="T830">who-VBZ-PST1-1SG</ta>
            <ta e="T832" id="Seg_8484" s="T831">apparently</ta>
            <ta e="T833" id="Seg_8485" s="T832">who-1SG-DAT/LOC</ta>
            <ta e="T834" id="Seg_8486" s="T833">neck-1SG-DAT/LOC</ta>
            <ta e="T835" id="Seg_8487" s="T834">so</ta>
            <ta e="T838" id="Seg_8488" s="T837">who-VBZ-PST1-1SG</ta>
            <ta e="T839" id="Seg_8489" s="T838">this</ta>
            <ta e="T840" id="Seg_8490" s="T839">tie-CVB.SEQ</ta>
            <ta e="T841" id="Seg_8491" s="T840">go-CVB.SEQ-1SG</ta>
            <ta e="T842" id="Seg_8492" s="T841">then</ta>
            <ta e="T843" id="Seg_8493" s="T842">this</ta>
            <ta e="T844" id="Seg_8494" s="T843">parcel-PL.[NOM]</ta>
            <ta e="T845" id="Seg_8495" s="T844">be-HAB-3PL</ta>
            <ta e="T848" id="Seg_8496" s="T847">well</ta>
            <ta e="T849" id="Seg_8497" s="T848">that-3SG-1SG-INSTR</ta>
            <ta e="T850" id="Seg_8498" s="T849">turn.around-PST1-1SG</ta>
            <ta e="T851" id="Seg_8499" s="T850">like.this</ta>
            <ta e="T852" id="Seg_8500" s="T851">then</ta>
            <ta e="T853" id="Seg_8501" s="T852">puttee.[NOM]</ta>
            <ta e="T854" id="Seg_8502" s="T853">that.[NOM]</ta>
            <ta e="T855" id="Seg_8503" s="T854">high.boots-DAT/LOC</ta>
            <ta e="T856" id="Seg_8504" s="T855">who.[NOM]</ta>
            <ta e="T857" id="Seg_8505" s="T856">be-HAB.[3SG]</ta>
            <ta e="T860" id="Seg_8506" s="T859">that-ACC</ta>
            <ta e="T861" id="Seg_8507" s="T860">puttee-1SG-INSTR</ta>
            <ta e="T862" id="Seg_8508" s="T861">turn.around-PST1-1SG</ta>
            <ta e="T863" id="Seg_8509" s="T862">go.[IMP.2SG]</ta>
            <ta e="T864" id="Seg_8510" s="T863">say-PRS-3PL</ta>
            <ta e="T865" id="Seg_8511" s="T864">who.[NOM]</ta>
            <ta e="T866" id="Seg_8512" s="T865">commander.[NOM]</ta>
            <ta e="T867" id="Seg_8513" s="T866">company-DAT/LOC</ta>
            <ta e="T868" id="Seg_8514" s="T867">that.EMPH.[NOM]</ta>
            <ta e="T869" id="Seg_8515" s="T868">crawl-CVB.SEQ</ta>
            <ta e="T870" id="Seg_8516" s="T869">crawl-PTCP.PST-EP-INSTR</ta>
            <ta e="T871" id="Seg_8517" s="T870">reach-PST1-1SG</ta>
            <ta e="T872" id="Seg_8518" s="T871">that-3SG-1SG-DAT/LOC</ta>
            <ta e="T873" id="Seg_8519" s="T872">bread.[NOM]</ta>
            <ta e="T874" id="Seg_8520" s="T873">search-CVB.SEQ</ta>
            <ta e="T875" id="Seg_8521" s="T874">eh</ta>
            <ta e="T878" id="Seg_8522" s="T877">food.[NOM]</ta>
            <ta e="T879" id="Seg_8523" s="T878">well</ta>
            <ta e="T880" id="Seg_8524" s="T879">say-PRS.[3SG]</ta>
            <ta e="T881" id="Seg_8525" s="T880">wound-CVB.SIM</ta>
            <ta e="T882" id="Seg_8526" s="T881">make</ta>
            <ta e="T883" id="Seg_8527" s="T930">Q</ta>
            <ta e="T885" id="Seg_8528" s="T883">say-PRS.[3SG]</ta>
            <ta e="T886" id="Seg_8529" s="T885">hmm</ta>
            <ta e="T887" id="Seg_8530" s="T886">say-PRS-1SG</ta>
            <ta e="T888" id="Seg_8531" s="T887">rifle-1SG-ACC</ta>
            <ta e="T889" id="Seg_8532" s="T888">then</ta>
            <ta e="T890" id="Seg_8533" s="T889">take-PTCP.PST</ta>
            <ta e="T891" id="Seg_8534" s="T890">be-PST1-3SG</ta>
            <ta e="T892" id="Seg_8535" s="T891">that</ta>
            <ta e="T893" id="Seg_8536" s="T892">who-EP-1SG.[NOM]</ta>
            <ta e="T894" id="Seg_8537" s="T893">commander-1SG-DAT/LOC</ta>
            <ta e="T895" id="Seg_8538" s="T894">give-VBZ-PRS-1SG</ta>
            <ta e="T896" id="Seg_8539" s="T895">EMPH</ta>
            <ta e="T897" id="Seg_8540" s="T896">who-DAT/LOC</ta>
            <ta e="T898" id="Seg_8541" s="T897">number-EP-INSTR</ta>
            <ta e="T899" id="Seg_8542" s="T898">be-PTCP.HAB</ta>
            <ta e="T900" id="Seg_8543" s="T899">be-PST1-3PL</ta>
            <ta e="T901" id="Seg_8544" s="T900">that-3SG-1SG.[NOM]</ta>
            <ta e="T902" id="Seg_8545" s="T901">EMPH</ta>
            <ta e="T903" id="Seg_8546" s="T902">who.[NOM]</ta>
            <ta e="T904" id="Seg_8547" s="T903">cartridge-PL.[NOM]</ta>
            <ta e="T905" id="Seg_8548" s="T904">pocket-1SG-DAT/LOC</ta>
            <ta e="T906" id="Seg_8549" s="T905">what.[NOM]</ta>
            <ta e="T907" id="Seg_8550" s="T906">NEG</ta>
            <ta e="T908" id="Seg_8551" s="T907">NEG.EX</ta>
            <ta e="T909" id="Seg_8552" s="T908">grenade-PL-1SG-ACC</ta>
            <ta e="T910" id="Seg_8553" s="T909">also</ta>
            <ta e="T911" id="Seg_8554" s="T910">take-PST2-3PL</ta>
            <ta e="T912" id="Seg_8555" s="T911">trench-DAT/LOC</ta>
            <ta e="T913" id="Seg_8556" s="T912">lie.down-TEMP-1SG</ta>
            <ta e="T914" id="Seg_8557" s="T913">sand.[NOM]</ta>
            <ta e="T915" id="Seg_8558" s="T914">fall-FREQ-PRS.[3SG]</ta>
            <ta e="T916" id="Seg_8559" s="T915">EMPH</ta>
            <ta e="T917" id="Seg_8560" s="T916">that</ta>
            <ta e="T918" id="Seg_8561" s="T917">side-ABL</ta>
            <ta e="T919" id="Seg_8562" s="T918">come-CVB.SIM</ta>
            <ta e="T920" id="Seg_8563" s="T919">EMPH</ta>
            <ta e="T921" id="Seg_8564" s="T920">mine-PL.[NOM]</ta>
            <ta e="T922" id="Seg_8565" s="T921">fall-TEMP-3PL</ta>
            <ta e="T923" id="Seg_8566" s="T922">earth-EP-1SG.[NOM]</ta>
            <ta e="T924" id="Seg_8567" s="T923">tremble-PRS.[3SG]</ta>
            <ta e="T929" id="Seg_8568" s="T928">who-VBZ-PRS-3PL</ta>
            <ta e="T931" id="Seg_8569" s="T929">only</ta>
            <ta e="T932" id="Seg_8570" s="T931">that-3SG-1SG.[NOM]</ta>
            <ta e="T933" id="Seg_8571" s="T932">this</ta>
            <ta e="T934" id="Seg_8572" s="T933">relief.[NOM]</ta>
            <ta e="T935" id="Seg_8573" s="T937">completion.[NOM]</ta>
            <ta e="T936" id="Seg_8574" s="T935">come-PTCP.PRS</ta>
            <ta e="T938" id="Seg_8575" s="T936">be-PST2.[3SG]</ta>
            <ta e="T939" id="Seg_8576" s="T938">1PL.[NOM]</ta>
            <ta e="T940" id="Seg_8577" s="T939">place-1PL-DAT/LOC</ta>
            <ta e="T941" id="Seg_8578" s="T940">still</ta>
            <ta e="T942" id="Seg_8579" s="T941">next-PL.[NOM]</ta>
            <ta e="T943" id="Seg_8580" s="T942">come-PRS-3PL</ta>
            <ta e="T944" id="Seg_8581" s="T943">be-PRS.[3SG]</ta>
            <ta e="T945" id="Seg_8582" s="T944">noise.[NOM]</ta>
            <ta e="T946" id="Seg_8583" s="T945">become-FREQ-PRS.[3SG]</ta>
            <ta e="T947" id="Seg_8584" s="T946">EMPH</ta>
            <ta e="T948" id="Seg_8585" s="T947">then</ta>
            <ta e="T949" id="Seg_8586" s="T948">cry-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T950" id="Seg_8587" s="T949">become-PRS-3PL</ta>
            <ta e="T951" id="Seg_8588" s="T950">only</ta>
            <ta e="T952" id="Seg_8589" s="T951">some.[NOM]-some.[NOM]</ta>
            <ta e="T953" id="Seg_8590" s="T952">moan-PTCP.PRS</ta>
            <ta e="T954" id="Seg_8591" s="T953">be-PRS.[3SG]</ta>
            <ta e="T955" id="Seg_8592" s="T954">moan-PTCP.PRS</ta>
            <ta e="T956" id="Seg_8593" s="T955">be-TEMP-3PL</ta>
            <ta e="T957" id="Seg_8594" s="T956">1SG.[NOM]</ta>
            <ta e="T958" id="Seg_8595" s="T957">also</ta>
            <ta e="T959" id="Seg_8596" s="T958">also</ta>
            <ta e="T960" id="Seg_8597" s="T959">moan-PRS-1SG</ta>
            <ta e="T961" id="Seg_8598" s="T960">also</ta>
            <ta e="T962" id="Seg_8599" s="T961">then</ta>
            <ta e="T963" id="Seg_8600" s="T962">again</ta>
            <ta e="T964" id="Seg_8601" s="T963">fade.away-NMNZ.[NOM]</ta>
            <ta e="T965" id="Seg_8602" s="T964">make-PRS-1SG</ta>
            <ta e="T966" id="Seg_8603" s="T965">listen-PRS-1SG</ta>
            <ta e="T967" id="Seg_8604" s="T966">infirmary-DAT/LOC</ta>
            <ta e="T968" id="Seg_8605" s="T967">come-PST2-EP-1SG</ta>
            <ta e="T971" id="Seg_8606" s="T970">field</ta>
            <ta e="T972" id="Seg_8607" s="T971">work.[NOM]</ta>
            <ta e="T973" id="Seg_8608" s="T972">who.[NOM]</ta>
            <ta e="T974" id="Seg_8609" s="T973">be-HAB.[3SG]</ta>
            <ta e="T975" id="Seg_8610" s="T974">infirmary.[NOM]</ta>
            <ta e="T976" id="Seg_8611" s="T975">that</ta>
            <ta e="T977" id="Seg_8612" s="T976">infirmary-DAT/LOC</ta>
            <ta e="T978" id="Seg_8613" s="T977">come-PST1</ta>
            <ta e="T979" id="Seg_8614" s="T978">eh</ta>
            <ta e="T980" id="Seg_8615" s="T979">come-PST1-1SG</ta>
            <ta e="T981" id="Seg_8616" s="T980">and</ta>
            <ta e="T982" id="Seg_8617" s="T981">home.front-DAT/LOC</ta>
            <ta e="T983" id="Seg_8618" s="T982">send-PST2-3PL</ta>
            <ta e="T984" id="Seg_8619" s="T983">Ivanovo.Shuja.[NOM]</ta>
            <ta e="T985" id="Seg_8620" s="T984">lie-PST2-EP-1SG</ta>
            <ta e="T986" id="Seg_8621" s="T985">old</ta>
            <ta e="T987" id="Seg_8622" s="T986">hospital-DAT/LOC</ta>
            <ta e="T988" id="Seg_8623" s="T987">then</ta>
            <ta e="T991" id="Seg_8624" s="T990">eh</ta>
            <ta e="T992" id="Seg_8625" s="T991">immediately</ta>
            <ta e="T993" id="Seg_8626" s="T992">five</ta>
            <ta e="T994" id="Seg_8627" s="T993">month-ACC</ta>
            <ta e="T995" id="Seg_8628" s="T994">lie-PST1-1SG</ta>
            <ta e="T996" id="Seg_8629" s="T995">and</ta>
            <ta e="T997" id="Seg_8630" s="T996">house-1SG.[NOM]</ta>
            <ta e="T998" id="Seg_8631" s="T997">to</ta>
            <ta e="T999" id="Seg_8632" s="T998">send-PST2-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg-ChVD">
            <ta e="T1" id="Seg_8633" s="T0">Dudinka-DAT/LOC</ta>
            <ta e="T2" id="Seg_8634" s="T1">kommen-PST2-EP-1SG</ta>
            <ta e="T6" id="Seg_8635" s="T5">August</ta>
            <ta e="T7" id="Seg_8636" s="T6">Monat-DAT/LOC</ta>
            <ta e="T11" id="Seg_8637" s="T10">Kommission-ACC</ta>
            <ta e="T12" id="Seg_8638" s="T11">durchgehen-CVB.SIM</ta>
            <ta e="T13" id="Seg_8639" s="T12">machen-PST1-1SG</ta>
            <ta e="T14" id="Seg_8640" s="T13">Hilfssoldat-DAT/LOC</ta>
            <ta e="T15" id="Seg_8641" s="T14">Norilsk-DAT/LOC</ta>
            <ta e="T16" id="Seg_8642" s="T15">schicken-PST2-3PL</ta>
            <ta e="T17" id="Seg_8643" s="T16">Norilsk-ABL</ta>
            <ta e="T18" id="Seg_8644" s="T17">wer-DAT/LOC</ta>
            <ta e="T19" id="Seg_8645" s="T18">äh</ta>
            <ta e="T9" id="Seg_8646" s="T19">militärisch</ta>
            <ta e="T20" id="Seg_8647" s="T9">Wache-DAT/LOC</ta>
            <ta e="T21" id="Seg_8648" s="T20">arbeiten-CVB.SIM</ta>
            <ta e="T22" id="Seg_8649" s="T21">fallen-PST1-1SG</ta>
            <ta e="T23" id="Seg_8650" s="T22">Freiwilliger-EP-PL-ACC</ta>
            <ta e="T24" id="Seg_8651" s="T23">mit</ta>
            <ta e="T25" id="Seg_8652" s="T24">wieder</ta>
            <ta e="T26" id="Seg_8653" s="T25">nun</ta>
            <ta e="T27" id="Seg_8654" s="T26">geraten-PST2-EP-1SG</ta>
            <ta e="T28" id="Seg_8655" s="T27">Krasnojarsk-DAT/LOC</ta>
            <ta e="T29" id="Seg_8656" s="T28">äh</ta>
            <ta e="T30" id="Seg_8657" s="T29">bis.zu</ta>
            <ta e="T31" id="Seg_8658" s="T30">gehen-PST2-EP-1SG</ta>
            <ta e="T32" id="Seg_8659" s="T31">Spartak</ta>
            <ta e="T33" id="Seg_8660" s="T32">Dampfer-3SG-INSTR</ta>
            <ta e="T34" id="Seg_8661" s="T33">dann</ta>
            <ta e="T35" id="Seg_8662" s="T34">lernen-PST1-1SG</ta>
            <ta e="T36" id="Seg_8663" s="T35">Krasnojarsk</ta>
            <ta e="T37" id="Seg_8664" s="T36">Infanterie-DAT/LOC</ta>
            <ta e="T41" id="Seg_8665" s="T40">zehn</ta>
            <ta e="T42" id="Seg_8666" s="T41">eins</ta>
            <ta e="T43" id="Seg_8667" s="T42">Monat-ACC</ta>
            <ta e="T44" id="Seg_8668" s="T43">Q</ta>
            <ta e="T45" id="Seg_8669" s="T44">wie.viel-ACC</ta>
            <ta e="T46" id="Seg_8670" s="T45">Q</ta>
            <ta e="T47" id="Seg_8671" s="T46">lernen-CVB.SEQ</ta>
            <ta e="T48" id="Seg_8672" s="T47">nachdem</ta>
            <ta e="T49" id="Seg_8673" s="T48">wieder</ta>
            <ta e="T50" id="Seg_8674" s="T49">wiederholen-CVB.SIM</ta>
            <ta e="T51" id="Seg_8675" s="T54">wer-DAT/LOC</ta>
            <ta e="T52" id="Seg_8676" s="T51">Zvenigorod-DAT/LOC</ta>
            <ta e="T56" id="Seg_8677" s="T55">Fallschirm.[NOM]</ta>
            <ta e="T57" id="Seg_8678" s="T56">auseinandernehmen-VBZ-PTCP.HAB</ta>
            <ta e="T58" id="Seg_8679" s="T57">sein-PST1-1SG</ta>
            <ta e="T60" id="Seg_8680" s="T59">doch</ta>
            <ta e="T61" id="Seg_8681" s="T60">dort</ta>
            <ta e="T62" id="Seg_8682" s="T61">lang</ta>
            <ta e="T63" id="Seg_8683" s="T62">NEG</ta>
            <ta e="T64" id="Seg_8684" s="T63">lernen-PST2.NEG-EP-1SG</ta>
            <ta e="T65" id="Seg_8685" s="T64">Freund-PL-EP-1SG.[NOM]</ta>
            <ta e="T66" id="Seg_8686" s="T65">mit</ta>
            <ta e="T67" id="Seg_8687" s="T66">dieses.[NOM]</ta>
            <ta e="T68" id="Seg_8688" s="T67">bleiben-EP-NEG-CVB.PURP-1SG</ta>
            <ta e="T69" id="Seg_8689" s="T68">zusammen</ta>
            <ta e="T70" id="Seg_8690" s="T69">gehen-RECP/COLL-EP-PST2-EP-1SG</ta>
            <ta e="T71" id="Seg_8691" s="T70">doch</ta>
            <ta e="T72" id="Seg_8692" s="T71">jenes</ta>
            <ta e="T73" id="Seg_8693" s="T72">wer-VBZ-PST1-1SG</ta>
            <ta e="T74" id="Seg_8694" s="T73">wer-DAT/LOC</ta>
            <ta e="T75" id="Seg_8695" s="T74">Murom-DAT/LOC</ta>
            <ta e="T76" id="Seg_8696" s="T75">lernen-CVB.SIM</ta>
            <ta e="T77" id="Seg_8697" s="T76">wer-VBZ-PST1-1PL</ta>
            <ta e="T481" id="Seg_8698" s="T80">einfach</ta>
            <ta e="T81" id="Seg_8699" s="T481">wer-VBZ-PTCP.PRS</ta>
            <ta e="T82" id="Seg_8700" s="T81">sein-PST1-3PL</ta>
            <ta e="T83" id="Seg_8701" s="T82">dort</ta>
            <ta e="T84" id="Seg_8702" s="T83">Formierung.[NOM]</ta>
            <ta e="T85" id="Seg_8703" s="T84">machen-PTCP.PRS</ta>
            <ta e="T86" id="Seg_8704" s="T85">sein-PST1-3PL</ta>
            <ta e="T92" id="Seg_8705" s="T91">was-DAT/LOC</ta>
            <ta e="T93" id="Seg_8706" s="T92">lernen-PTCP.PST-2SG-ACC</ta>
            <ta e="T94" id="Seg_8707" s="T93">wer-PROPR</ta>
            <ta e="T95" id="Seg_8708" s="T94">wer.[NOM]</ta>
            <ta e="T96" id="Seg_8709" s="T95">wer-VBZ-PRS-3PL</ta>
            <ta e="T97" id="Seg_8710" s="T96">dann</ta>
            <ta e="T101" id="Seg_8711" s="T100">doch</ta>
            <ta e="T102" id="Seg_8712" s="T101">dort</ta>
            <ta e="T103" id="Seg_8713" s="T102">wer-VBZ-PST1-1SG</ta>
            <ta e="T104" id="Seg_8714" s="T103">EMPH</ta>
            <ta e="T105" id="Seg_8715" s="T104">Murom-ABL</ta>
            <ta e="T106" id="Seg_8716" s="T105">wer-DAT/LOC</ta>
            <ta e="T107" id="Seg_8717" s="T106">geraten-PST1-1PL</ta>
            <ta e="T108" id="Seg_8718" s="T107">jenes</ta>
            <ta e="T109" id="Seg_8719" s="T108">Tula-DAT/LOC</ta>
            <ta e="T110" id="Seg_8720" s="T109">jenes</ta>
            <ta e="T111" id="Seg_8721" s="T110">Tula.[NOM]</ta>
            <ta e="T115" id="Seg_8722" s="T114">EMPH</ta>
            <ta e="T116" id="Seg_8723" s="T115">acht</ta>
            <ta e="T117" id="Seg_8724" s="T116">Kilometer-ACC</ta>
            <ta e="T118" id="Seg_8725" s="T117">Q</ta>
            <ta e="T119" id="Seg_8726" s="T118">wie.viel-ACC</ta>
            <ta e="T120" id="Seg_8727" s="T119">Q</ta>
            <ta e="T121" id="Seg_8728" s="T120">gehen-CVB.SEQ</ta>
            <ta e="T122" id="Seg_8729" s="T121">nachdem</ta>
            <ta e="T123" id="Seg_8730" s="T122">eisern</ta>
            <ta e="T124" id="Seg_8731" s="T123">Pfad-PL-EP-2SG.[NOM]</ta>
            <ta e="T125" id="Seg_8732" s="T124">ganz</ta>
            <ta e="T126" id="Seg_8733" s="T125">brechen-PST-3PL</ta>
            <ta e="T127" id="Seg_8734" s="T126">sein-PST1-3SG</ta>
            <ta e="T128" id="Seg_8735" s="T127">doch</ta>
            <ta e="T129" id="Seg_8736" s="T128">dort</ta>
            <ta e="T130" id="Seg_8737" s="T129">wer-VBZ-CVB.SIM</ta>
            <ta e="T131" id="Seg_8738" s="T130">liegen-TEMP-1PL</ta>
            <ta e="T132" id="Seg_8739" s="T131">schon</ta>
            <ta e="T134" id="Seg_8740" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_8741" s="T134">immer</ta>
            <ta e="T136" id="Seg_8742" s="T135">fliegen-CVB.SIM</ta>
            <ta e="T139" id="Seg_8743" s="T138">fliegen-PTCP.HAB</ta>
            <ta e="T140" id="Seg_8744" s="T139">sein-PST1-3PL</ta>
            <ta e="T141" id="Seg_8745" s="T140">Morgen.[NOM]</ta>
            <ta e="T142" id="Seg_8746" s="T141">jeder</ta>
            <ta e="T143" id="Seg_8747" s="T142">doch</ta>
            <ta e="T144" id="Seg_8748" s="T143">dann</ta>
            <ta e="T145" id="Seg_8749" s="T144">lang</ta>
            <ta e="T146" id="Seg_8750" s="T145">NEG</ta>
            <ta e="T147" id="Seg_8751" s="T146">sein-PST1-3SG</ta>
            <ta e="T148" id="Seg_8752" s="T147">hey</ta>
            <ta e="T151" id="Seg_8753" s="T150">wer-VBZ-PRS-1PL</ta>
            <ta e="T152" id="Seg_8754" s="T151">dorthin</ta>
            <ta e="T153" id="Seg_8755" s="T152">tatsächlich</ta>
            <ta e="T154" id="Seg_8756" s="T153">zu.Fuß</ta>
            <ta e="T155" id="Seg_8757" s="T154">gehen-PST2-1PL</ta>
            <ta e="T156" id="Seg_8758" s="T155">jenes</ta>
            <ta e="T157" id="Seg_8759" s="T156">wer-DAT/LOC</ta>
            <ta e="T158" id="Seg_8760" s="T157">wo</ta>
            <ta e="T161" id="Seg_8761" s="T160">jenes.EMPH.[NOM]</ta>
            <ta e="T162" id="Seg_8762" s="T161">Krieg.[NOM]</ta>
            <ta e="T163" id="Seg_8763" s="T162">sein-PTCP.FUT</ta>
            <ta e="T164" id="Seg_8764" s="T163">Ort-3SG.[NOM]</ta>
            <ta e="T165" id="Seg_8765" s="T164">sein-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T166" id="Seg_8766" s="T165">bis.zu</ta>
            <ta e="T167" id="Seg_8767" s="T166">doch</ta>
            <ta e="T168" id="Seg_8768" s="T167">dort</ta>
            <ta e="T169" id="Seg_8769" s="T168">wer-VBZ-HAB-3PL</ta>
            <ta e="T170" id="Seg_8770" s="T169">EMPH</ta>
            <ta e="T171" id="Seg_8771" s="T170">wer.[NOM]</ta>
            <ta e="T172" id="Seg_8772" s="T171">partisanisch</ta>
            <ta e="T173" id="Seg_8773" s="T172">dieses</ta>
            <ta e="T174" id="Seg_8774" s="T173">Abteilung</ta>
            <ta e="T175" id="Seg_8775" s="T174">oh</ta>
            <ta e="T176" id="Seg_8776" s="T175">wer-DAT/LOC</ta>
            <ta e="T177" id="Seg_8777" s="T176">NOP</ta>
            <ta e="T178" id="Seg_8778" s="T177">SOP</ta>
            <ta e="T179" id="Seg_8779" s="T178">kennzeichnen-VBZ-PRS-3PL</ta>
            <ta e="T180" id="Seg_8780" s="T179">jenes-ACC</ta>
            <ta e="T181" id="Seg_8781" s="T180">und</ta>
            <ta e="T182" id="Seg_8782" s="T181">dieses</ta>
            <ta e="T183" id="Seg_8783" s="T182">INTNS</ta>
            <ta e="T184" id="Seg_8784" s="T183">wer-VBZ</ta>
            <ta e="T185" id="Seg_8785" s="T184">wer-VBZ-PTCP.PRS-DAT/LOC</ta>
            <ta e="T186" id="Seg_8786" s="T185">Fluss.[NOM]</ta>
            <ta e="T187" id="Seg_8787" s="T186">was</ta>
            <ta e="T188" id="Seg_8788" s="T187">INDEF</ta>
            <ta e="T189" id="Seg_8789" s="T188">Fluss-DIM.[NOM]</ta>
            <ta e="T190" id="Seg_8790" s="T189">liegen-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_8791" s="T190">doch</ta>
            <ta e="T192" id="Seg_8792" s="T191">dort</ta>
            <ta e="T193" id="Seg_8793" s="T192">Dorf-DAT/LOC</ta>
            <ta e="T194" id="Seg_8794" s="T193">dort</ta>
            <ta e="T196" id="Seg_8795" s="T195">machen-PRS-3PL</ta>
            <ta e="T197" id="Seg_8796" s="T196">offenbar</ta>
            <ta e="T198" id="Seg_8797" s="T197">am.Morgen</ta>
            <ta e="T199" id="Seg_8798" s="T198">wer-VBZ-PST1-1PL</ta>
            <ta e="T200" id="Seg_8799" s="T199">essen-CVB.SIM</ta>
            <ta e="T201" id="Seg_8800" s="T200">essen-CVB.SIM</ta>
            <ta e="T202" id="Seg_8801" s="T201">fallen-CVB.SEQ</ta>
            <ta e="T203" id="Seg_8802" s="T202">gehen-CVB.SEQ-1PL</ta>
            <ta e="T204" id="Seg_8803" s="T203">jenes-ACC</ta>
            <ta e="T205" id="Seg_8804" s="T204">doch</ta>
            <ta e="T206" id="Seg_8805" s="T205">schießen-FREQ-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T207" id="Seg_8806" s="T206">Befehlshaber-1PL-ACC</ta>
            <ta e="T208" id="Seg_8807" s="T207">erzählen-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_8808" s="T208">wie</ta>
            <ta e="T212" id="Seg_8809" s="T211">wer-VBZ-PTCP.FUT-2PL-ACC</ta>
            <ta e="T214" id="Seg_8810" s="T212">dann</ta>
            <ta e="T215" id="Seg_8811" s="T214">doch</ta>
            <ta e="T216" id="Seg_8812" s="T215">Einkesselung.[NOM]</ta>
            <ta e="T217" id="Seg_8813" s="T216">geraten-TEMP-2PL</ta>
            <ta e="T218" id="Seg_8814" s="T217">vielleicht</ta>
            <ta e="T219" id="Seg_8815" s="T218">Gefangenschaft.[NOM]</ta>
            <ta e="T220" id="Seg_8816" s="T219">geraten-TEMP-2PL</ta>
            <ta e="T221" id="Seg_8817" s="T220">EMPH</ta>
            <ta e="T223" id="Seg_8818" s="T222">wer.[NOM]</ta>
            <ta e="T224" id="Seg_8819" s="T223">wer-VBZ-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T225" id="Seg_8820" s="T224">man.muss</ta>
            <ta e="T226" id="Seg_8821" s="T225">sein-PST1-3SG</ta>
            <ta e="T227" id="Seg_8822" s="T226">selbst-2SG-ACC</ta>
            <ta e="T228" id="Seg_8823" s="T227">geben-EP-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T229" id="Seg_8824" s="T228">vielleicht</ta>
            <ta e="T230" id="Seg_8825" s="T229">simply</ta>
            <ta e="T233" id="Seg_8826" s="T232">spalten-FREQ-NEG-IMP.2PL</ta>
            <ta e="T234" id="Seg_8827" s="T233">Dokument</ta>
            <ta e="T235" id="Seg_8828" s="T234">äh</ta>
            <ta e="T236" id="Seg_8829" s="T235">Dokument-PL-2PL.[NOM]</ta>
            <ta e="T237" id="Seg_8830" s="T236">sagen-PRS.[3SG]</ta>
            <ta e="T238" id="Seg_8831" s="T237">MOD</ta>
            <ta e="T241" id="Seg_8832" s="T240">wer-VBZ-IMP.2PL</ta>
            <ta e="T244" id="Seg_8833" s="T243">äh</ta>
            <ta e="T245" id="Seg_8834" s="T244">vergraben-EP-IMP.2PL</ta>
            <ta e="T246" id="Seg_8835" s="T245">Q</ta>
            <ta e="T247" id="Seg_8836" s="T246">selbst-2PL.[NOM]</ta>
            <ta e="T248" id="Seg_8837" s="T247">essen-IMP.2PL</ta>
            <ta e="T249" id="Seg_8838" s="T248">Q</ta>
            <ta e="T250" id="Seg_8839" s="T249">sagen-HAB.[3SG]</ta>
            <ta e="T251" id="Seg_8840" s="T250">so</ta>
            <ta e="T252" id="Seg_8841" s="T251">spalten-FREQ-PST1-2SG</ta>
            <ta e="T253" id="Seg_8842" s="T252">und</ta>
            <ta e="T255" id="Seg_8843" s="T253">gleichwohl</ta>
            <ta e="T256" id="Seg_8844" s="T255">kleben-VBZ</ta>
            <ta e="T257" id="Seg_8845" s="T256">dieses.[NOM]</ta>
            <ta e="T258" id="Seg_8846" s="T257">wer-VBZ-PRS-3PL</ta>
            <ta e="T259" id="Seg_8847" s="T258">EMPH</ta>
            <ta e="T260" id="Seg_8848" s="T259">jenes</ta>
            <ta e="T261" id="Seg_8849" s="T260">finden-PRS-3PL</ta>
            <ta e="T262" id="Seg_8850" s="T261">EMPH</ta>
            <ta e="T263" id="Seg_8851" s="T262">jenes</ta>
            <ta e="T264" id="Seg_8852" s="T263">nun</ta>
            <ta e="T265" id="Seg_8853" s="T264">Kompanie-2SG.[NOM]</ta>
            <ta e="T266" id="Seg_8854" s="T265">lassen-CVB.SIM</ta>
            <ta e="T267" id="Seg_8855" s="T266">laufen-PST2-1SG</ta>
            <ta e="T268" id="Seg_8856" s="T267">einmal</ta>
            <ta e="T271" id="Seg_8857" s="T270">was.für.ein-3PL-ACC</ta>
            <ta e="T272" id="Seg_8858" s="T271">INDEF</ta>
            <ta e="T273" id="Seg_8859" s="T272">bemerken-CVB.SEQ-1SG</ta>
            <ta e="T274" id="Seg_8860" s="T273">Dunkelheit-DAT/LOC</ta>
            <ta e="T275" id="Seg_8861" s="T274">Deutscher-3PL.[NOM]</ta>
            <ta e="T276" id="Seg_8862" s="T275">und</ta>
            <ta e="T277" id="Seg_8863" s="T276">wer-3PL.[NOM]</ta>
            <ta e="T278" id="Seg_8864" s="T277">und</ta>
            <ta e="T279" id="Seg_8865" s="T278">doch</ta>
            <ta e="T280" id="Seg_8866" s="T279">jenes</ta>
            <ta e="T281" id="Seg_8867" s="T280">1SG.[NOM]</ta>
            <ta e="T282" id="Seg_8868" s="T281">gehen-CVB.SEQ-1SG</ta>
            <ta e="T283" id="Seg_8869" s="T282">sehen-PST1-1SG</ta>
            <ta e="T284" id="Seg_8870" s="T283">fünf</ta>
            <ta e="T285" id="Seg_8871" s="T284">Mensch.[NOM]</ta>
            <ta e="T286" id="Seg_8872" s="T285">gehen-PRS-3PL</ta>
            <ta e="T287" id="Seg_8873" s="T286">nur</ta>
            <ta e="T288" id="Seg_8874" s="T287">so</ta>
            <ta e="T289" id="Seg_8875" s="T288">doch</ta>
            <ta e="T290" id="Seg_8876" s="T289">dann</ta>
            <ta e="T291" id="Seg_8877" s="T290">jenes</ta>
            <ta e="T292" id="Seg_8878" s="T291">Seite-ABL</ta>
            <ta e="T293" id="Seg_8879" s="T292">Deutscher-EP-PL-1PL.[NOM]</ta>
            <ta e="T294" id="Seg_8880" s="T293">kommen-PRS-3PL</ta>
            <ta e="T295" id="Seg_8881" s="T294">Schützengraben-ABL</ta>
            <ta e="T296" id="Seg_8882" s="T295">aufstehen-PRS-3PL</ta>
            <ta e="T297" id="Seg_8883" s="T296">zurück</ta>
            <ta e="T298" id="Seg_8884" s="T297">entfliehen-IMP.1PL</ta>
            <ta e="T299" id="Seg_8885" s="T298">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T300" id="Seg_8886" s="T299">wer-VBZ-PRS-3PL</ta>
            <ta e="T301" id="Seg_8887" s="T300">jenes</ta>
            <ta e="T302" id="Seg_8888" s="T301">Mensch-2SG.[NOM]</ta>
            <ta e="T303" id="Seg_8889" s="T302">Befehlshaber-PL-PROPR-3PL</ta>
            <ta e="T304" id="Seg_8890" s="T303">jenes</ta>
            <ta e="T305" id="Seg_8891" s="T304">Freund-PL-3SG.[NOM]</ta>
            <ta e="T306" id="Seg_8892" s="T305">anders-PL.[NOM]</ta>
            <ta e="T307" id="Seg_8893" s="T306">dann</ta>
            <ta e="T308" id="Seg_8894" s="T307">anders</ta>
            <ta e="T309" id="Seg_8895" s="T308">Teil-ABL</ta>
            <ta e="T310" id="Seg_8896" s="T309">selbst-1SG.[NOM]</ta>
            <ta e="T313" id="Seg_8897" s="T312">Freund-PL-1SG-ACC</ta>
            <ta e="T314" id="Seg_8898" s="T313">jenes</ta>
            <ta e="T315" id="Seg_8899" s="T314">lassen-CVB.SIM</ta>
            <ta e="T316" id="Seg_8900" s="T315">laufen-PST2-1SG</ta>
            <ta e="T317" id="Seg_8901" s="T316">offenbar</ta>
            <ta e="T318" id="Seg_8902" s="T317">EMPH</ta>
            <ta e="T319" id="Seg_8903" s="T318">Q</ta>
            <ta e="T320" id="Seg_8904" s="T319">was.[NOM]</ta>
            <ta e="T321" id="Seg_8905" s="T320">Q</ta>
            <ta e="T322" id="Seg_8906" s="T321">zurückweichen-VBZ-IMP.2PL</ta>
            <ta e="T323" id="Seg_8907" s="T322">sagen-PRS.[3SG]</ta>
            <ta e="T327" id="Seg_8908" s="T326">sagen-PRS.[3SG]</ta>
            <ta e="T328" id="Seg_8909" s="T327">Befehlshaber-3PL.[NOM]</ta>
            <ta e="T329" id="Seg_8910" s="T328">dann</ta>
            <ta e="T330" id="Seg_8911" s="T329">sehen-PST1-1SG</ta>
            <ta e="T331" id="Seg_8912" s="T330">nun</ta>
            <ta e="T332" id="Seg_8913" s="T331">Deutscher-EP-PL-EP-2SG.[NOM]</ta>
            <ta e="T333" id="Seg_8914" s="T332">dieses.EMPH</ta>
            <ta e="T334" id="Seg_8915" s="T333">kommen-CVB.SEQ</ta>
            <ta e="T335" id="Seg_8916" s="T334">gehen-PRS-3PL</ta>
            <ta e="T336" id="Seg_8917" s="T335">was-ACC</ta>
            <ta e="T337" id="Seg_8918" s="T336">was-ACC</ta>
            <ta e="T338" id="Seg_8919" s="T337">sagen-PRS-3PL</ta>
            <ta e="T339" id="Seg_8920" s="T338">sich.unterhalten-PRS-3PL</ta>
            <ta e="T340" id="Seg_8921" s="T339">zurück</ta>
            <ta e="T341" id="Seg_8922" s="T340">laufen-PRS-1SG</ta>
            <ta e="T342" id="Seg_8923" s="T341">Freund-PL-EP-1SG.[NOM]</ta>
            <ta e="T343" id="Seg_8924" s="T342">gehen-PTCP.PST</ta>
            <ta e="T344" id="Seg_8925" s="T343">Ort-3PL-ACC</ta>
            <ta e="T345" id="Seg_8926" s="T344">zu</ta>
            <ta e="T346" id="Seg_8927" s="T345">ganz</ta>
            <ta e="T347" id="Seg_8928" s="T346">sich.austauschen-PRS-3PL</ta>
            <ta e="T348" id="Seg_8929" s="T347">nur</ta>
            <ta e="T349" id="Seg_8930" s="T348">dort</ta>
            <ta e="T350" id="Seg_8931" s="T349">Gras-DAT/LOC</ta>
            <ta e="T351" id="Seg_8932" s="T350">schießen-FREQ-PRS-3PL</ta>
            <ta e="T352" id="Seg_8933" s="T351">tatsächlich</ta>
            <ta e="T353" id="Seg_8934" s="T352">wer-VBZ-CVB.PURP</ta>
            <ta e="T354" id="Seg_8935" s="T353">wollen-PRS-3PL</ta>
            <ta e="T355" id="Seg_8936" s="T354">offenbar</ta>
            <ta e="T357" id="Seg_8937" s="T355">dann</ta>
            <ta e="T358" id="Seg_8938" s="T357">Gefangenschaft.[NOM]</ta>
            <ta e="T359" id="Seg_8939" s="T358">bekommen-CVB.PURP</ta>
            <ta e="T360" id="Seg_8940" s="T359">Ende-EP-ABL</ta>
            <ta e="T361" id="Seg_8941" s="T360">Schützengraben-DAT/LOC</ta>
            <ta e="T362" id="Seg_8942" s="T361">kommen-PST1-1SG</ta>
            <ta e="T363" id="Seg_8943" s="T362">kommen-PST2-EP-1SG</ta>
            <ta e="T364" id="Seg_8944" s="T363">selbst-1SG.[NOM]</ta>
            <ta e="T365" id="Seg_8945" s="T364">Freund</ta>
            <ta e="T366" id="Seg_8946" s="T365">äh</ta>
            <ta e="T367" id="Seg_8947" s="T366">Kompanie-1SG-DAT/LOC</ta>
            <ta e="T368" id="Seg_8948" s="T367">geraten-PST2-1SG</ta>
            <ta e="T369" id="Seg_8949" s="T368">doch</ta>
            <ta e="T370" id="Seg_8950" s="T369">jenes</ta>
            <ta e="T371" id="Seg_8951" s="T370">wer-VBZ-PRS-1SG</ta>
            <ta e="T372" id="Seg_8952" s="T371">na</ta>
            <ta e="T375" id="Seg_8953" s="T374">Maschinengewehr-EP-INSTR</ta>
            <ta e="T376" id="Seg_8954" s="T375">wer-VBZ-FUT.[3SG]</ta>
            <ta e="T377" id="Seg_8955" s="T376">sofort</ta>
            <ta e="T378" id="Seg_8956" s="T377">zwanzig-ABL</ta>
            <ta e="T379" id="Seg_8957" s="T378">über</ta>
            <ta e="T380" id="Seg_8958" s="T379">Mensch-ACC</ta>
            <ta e="T381" id="Seg_8959" s="T380">fallen-EP-CAUS-PST1-3PL</ta>
            <ta e="T382" id="Seg_8960" s="T381">mancher-PL-1PL.[NOM]</ta>
            <ta e="T383" id="Seg_8961" s="T382">zurück</ta>
            <ta e="T384" id="Seg_8962" s="T383">laufen-PST1-3PL</ta>
            <ta e="T385" id="Seg_8963" s="T384">jenes</ta>
            <ta e="T386" id="Seg_8964" s="T385">Patrouille.[NOM]</ta>
            <ta e="T387" id="Seg_8965" s="T386">sein-HAB.[3SG]</ta>
            <ta e="T390" id="Seg_8966" s="T389">Kompanie-2SG.[NOM]</ta>
            <ta e="T391" id="Seg_8967" s="T390">gehen-FUT-3SG</ta>
            <ta e="T392" id="Seg_8968" s="T391">noch</ta>
            <ta e="T393" id="Seg_8969" s="T392">wer.[NOM]</ta>
            <ta e="T394" id="Seg_8970" s="T393">vorweg</ta>
            <ta e="T395" id="Seg_8971" s="T394">gehen-FUT-3SG</ta>
            <ta e="T396" id="Seg_8972" s="T395">Aufklärer.[NOM]</ta>
            <ta e="T397" id="Seg_8973" s="T396">ähnlich</ta>
            <ta e="T398" id="Seg_8974" s="T397">Kompanie-2SG-ACC</ta>
            <ta e="T399" id="Seg_8975" s="T398">nur</ta>
            <ta e="T400" id="Seg_8976" s="T399">hüten-PRS-2SG</ta>
            <ta e="T401" id="Seg_8977" s="T400">EMPH</ta>
            <ta e="T402" id="Seg_8978" s="T401">vorderer-PL.[NOM]</ta>
            <ta e="T403" id="Seg_8979" s="T402">zwei</ta>
            <ta e="T404" id="Seg_8980" s="T403">Mensch.[NOM]</ta>
            <ta e="T405" id="Seg_8981" s="T404">sein-PRS.[3SG]</ta>
            <ta e="T406" id="Seg_8982" s="T405">linker</ta>
            <ta e="T407" id="Seg_8983" s="T406">zu</ta>
            <ta e="T408" id="Seg_8984" s="T407">rechter</ta>
            <ta e="T409" id="Seg_8985" s="T408">zu</ta>
            <ta e="T410" id="Seg_8986" s="T409">wieder</ta>
            <ta e="T411" id="Seg_8987" s="T410">zwei-DISTR</ta>
            <ta e="T412" id="Seg_8988" s="T411">Mensch.[NOM]</ta>
            <ta e="T413" id="Seg_8989" s="T412">doch</ta>
            <ta e="T414" id="Seg_8990" s="T413">wohin</ta>
            <ta e="T415" id="Seg_8991" s="T414">sein-FUT-1PL=Q</ta>
            <ta e="T416" id="Seg_8992" s="T415">sagen-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_8993" s="T416">dieses</ta>
            <ta e="T418" id="Seg_8994" s="T417">Dorf-DAT/LOC</ta>
            <ta e="T419" id="Seg_8995" s="T418">überprüfen-VBZ-PRS-1PL</ta>
            <ta e="T420" id="Seg_8996" s="T419">tatsächlich</ta>
            <ta e="T421" id="Seg_8997" s="T420">wer-VBZ</ta>
            <ta e="T422" id="Seg_8998" s="T421">wer.[NOM]</ta>
            <ta e="T423" id="Seg_8999" s="T422">NEG</ta>
            <ta e="T424" id="Seg_9000" s="T423">NEG.EX</ta>
            <ta e="T426" id="Seg_9001" s="T425">äh</ta>
            <ta e="T427" id="Seg_9002" s="T426">Deutscher-EP-PL.[NOM]</ta>
            <ta e="T428" id="Seg_9003" s="T427">am.anderen.Ufer</ta>
            <ta e="T429" id="Seg_9004" s="T428">nur</ta>
            <ta e="T430" id="Seg_9005" s="T429">zu.sehen.sein-PRS-3PL</ta>
            <ta e="T431" id="Seg_9006" s="T430">zurück</ta>
            <ta e="T432" id="Seg_9007" s="T431">gehen-IMP.1PL</ta>
            <ta e="T433" id="Seg_9008" s="T432">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T434" id="Seg_9009" s="T433">machen-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_9010" s="T434">doch</ta>
            <ta e="T436" id="Seg_9011" s="T435">jenes</ta>
            <ta e="T437" id="Seg_9012" s="T436">wer-VBZ-CVB.SIM</ta>
            <ta e="T438" id="Seg_9013" s="T437">gehen-CVB.SEQ-1PL</ta>
            <ta e="T439" id="Seg_9014" s="T438">sich.treffen-PST1-1PL</ta>
            <ta e="T440" id="Seg_9015" s="T439">jenes</ta>
            <ta e="T441" id="Seg_9016" s="T440">jenes</ta>
            <ta e="T442" id="Seg_9017" s="T441">Fluss-1PL-ACC</ta>
            <ta e="T443" id="Seg_9018" s="T442">quer</ta>
            <ta e="T444" id="Seg_9019" s="T443">gehen-PST1-1PL</ta>
            <ta e="T445" id="Seg_9020" s="T444">Strömung.[NOM]</ta>
            <ta e="T446" id="Seg_9021" s="T445">quer</ta>
            <ta e="T447" id="Seg_9022" s="T446">Fluss-VBZ-CVB.SEQ</ta>
            <ta e="T448" id="Seg_9023" s="T447">äh</ta>
            <ta e="T449" id="Seg_9024" s="T448">gehen-CVB.SEQ-1PL</ta>
            <ta e="T450" id="Seg_9025" s="T449">äh</ta>
            <ta e="T451" id="Seg_9026" s="T450">wer-VBZ-PRS-1PL</ta>
            <ta e="T452" id="Seg_9027" s="T451">so</ta>
            <ta e="T453" id="Seg_9028" s="T452">wer-3PL.[NOM]</ta>
            <ta e="T454" id="Seg_9029" s="T453">wer-VBZ-PST2-3PL</ta>
            <ta e="T455" id="Seg_9030" s="T454">dieses</ta>
            <ta e="T456" id="Seg_9031" s="T455">Artillerie.[NOM]</ta>
            <ta e="T457" id="Seg_9032" s="T456">sein-HAB.[3SG]</ta>
            <ta e="T458" id="Seg_9033" s="T457">EMPH</ta>
            <ta e="T459" id="Seg_9034" s="T458">Kanone-3PL.[NOM]</ta>
            <ta e="T462" id="Seg_9035" s="T461">äh</ta>
            <ta e="T463" id="Seg_9036" s="T462">wer-3PL.[NOM]</ta>
            <ta e="T464" id="Seg_9037" s="T463">Pferd-PL.[NOM]</ta>
            <ta e="T465" id="Seg_9038" s="T464">ganz</ta>
            <ta e="T466" id="Seg_9039" s="T465">wer-VBZ-EP-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T467" id="Seg_9040" s="T466">so</ta>
            <ta e="T468" id="Seg_9041" s="T467">sich.umdrehen-CVB.SIM</ta>
            <ta e="T469" id="Seg_9042" s="T468">wer-VBZ-PST2-3PL</ta>
            <ta e="T470" id="Seg_9043" s="T469">dann</ta>
            <ta e="T471" id="Seg_9044" s="T470">töten-ITER-PST2-3PL</ta>
            <ta e="T472" id="Seg_9045" s="T471">dort</ta>
            <ta e="T473" id="Seg_9046" s="T472">eins</ta>
            <ta e="T474" id="Seg_9047" s="T473">Mensch-ACC</ta>
            <ta e="T475" id="Seg_9048" s="T474">finden-PST1-1PL</ta>
            <ta e="T8" id="Seg_9049" s="T475">halb.am.Leben.[NOM]</ta>
            <ta e="T476" id="Seg_9050" s="T8">tatsächlich</ta>
            <ta e="T477" id="Seg_9051" s="T476">wohin</ta>
            <ta e="T478" id="Seg_9052" s="T477">INDEF</ta>
            <ta e="T479" id="Seg_9053" s="T478">kaum</ta>
            <ta e="T480" id="Seg_9054" s="T479">sprechen-PRS.[3SG]</ta>
            <ta e="T482" id="Seg_9055" s="T480">dann</ta>
            <ta e="T483" id="Seg_9056" s="T482">dieses</ta>
            <ta e="T484" id="Seg_9057" s="T483">soeben</ta>
            <ta e="T485" id="Seg_9058" s="T484">Tabak.[NOM]</ta>
            <ta e="T486" id="Seg_9059" s="T485">ziehen-PTCP.PRS</ta>
            <ta e="T487" id="Seg_9060" s="T486">Nähe-3SG.[NOM]</ta>
            <ta e="T488" id="Seg_9061" s="T487">sein-PST1-3SG</ta>
            <ta e="T489" id="Seg_9062" s="T488">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T490" id="Seg_9063" s="T489">machen-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_9064" s="T490">1PL.[NOM]</ta>
            <ta e="T492" id="Seg_9065" s="T491">Kompanie-1PL-ACC</ta>
            <ta e="T493" id="Seg_9066" s="T492">wer-VBZ-PST2-3PL</ta>
            <ta e="T494" id="Seg_9067" s="T493">sagen-PRS.[3SG]</ta>
            <ta e="T495" id="Seg_9068" s="T494">Deutscher-3SG.[NOM]</ta>
            <ta e="T496" id="Seg_9069" s="T495">AFFIRM</ta>
            <ta e="T497" id="Seg_9070" s="T496">gehen-PST2-3PL</ta>
            <ta e="T498" id="Seg_9071" s="T497">jetzt-INTNS</ta>
            <ta e="T499" id="Seg_9072" s="T498">sagen-PRS.[3SG]</ta>
            <ta e="T500" id="Seg_9073" s="T499">sprechen-RECP/COLL-EP-NEG-IMP.2PL</ta>
            <ta e="T501" id="Seg_9074" s="T500">laut-ADVZ</ta>
            <ta e="T502" id="Seg_9075" s="T501">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T503" id="Seg_9076" s="T502">machen-PRS.[3SG]</ta>
            <ta e="T504" id="Seg_9077" s="T503">doch</ta>
            <ta e="T505" id="Seg_9078" s="T504">jenes</ta>
            <ta e="T507" id="Seg_9079" s="T505">wer-VBZ-PST1-1PL</ta>
            <ta e="T508" id="Seg_9080" s="T507">doch</ta>
            <ta e="T509" id="Seg_9081" s="T508">dort</ta>
            <ta e="T510" id="Seg_9082" s="T509">EMPH</ta>
            <ta e="T511" id="Seg_9083" s="T510">äh</ta>
            <ta e="T512" id="Seg_9084" s="T511">Freund</ta>
            <ta e="T513" id="Seg_9085" s="T512">zwei</ta>
            <ta e="T514" id="Seg_9086" s="T513">Mensch-ACC</ta>
            <ta e="T515" id="Seg_9087" s="T514">schicken-PRS-3PL</ta>
            <ta e="T516" id="Seg_9088" s="T515">dort</ta>
            <ta e="T517" id="Seg_9089" s="T516">wer.[NOM]</ta>
            <ta e="T518" id="Seg_9090" s="T517">sein-HAB.[3SG]</ta>
            <ta e="T519" id="Seg_9091" s="T518">Lazarett.[NOM]</ta>
            <ta e="T520" id="Seg_9092" s="T519">dann</ta>
            <ta e="T521" id="Seg_9093" s="T520">mitnehmen-FUT-1PL</ta>
            <ta e="T522" id="Seg_9094" s="T521">sagen-PRS-3PL</ta>
            <ta e="T523" id="Seg_9095" s="T522">1SG.[NOM]</ta>
            <ta e="T524" id="Seg_9096" s="T523">1PL.[NOM]</ta>
            <ta e="T525" id="Seg_9097" s="T524">2SG-ACC</ta>
            <ta e="T528" id="Seg_9098" s="T527">1SG-ACC</ta>
            <ta e="T529" id="Seg_9099" s="T528">was.[NOM]</ta>
            <ta e="T530" id="Seg_9100" s="T529">beunruhigen-EP-NEG-IMP.2PL</ta>
            <ta e="T531" id="Seg_9101" s="T530">sagen-PRS.[3SG]</ta>
            <ta e="T532" id="Seg_9102" s="T531">1SG.[NOM]</ta>
            <ta e="T533" id="Seg_9103" s="T532">äh</ta>
            <ta e="T534" id="Seg_9104" s="T533">doch</ta>
            <ta e="T535" id="Seg_9105" s="T534">Mensch.[NOM]</ta>
            <ta e="T536" id="Seg_9106" s="T535">sein-PTCP.FUT</ta>
            <ta e="T537" id="Seg_9107" s="T536">äh</ta>
            <ta e="T538" id="Seg_9108" s="T537">Mensch.[NOM]</ta>
            <ta e="T539" id="Seg_9109" s="T538">sein-PST2.NEG-1SG</ta>
            <ta e="T540" id="Seg_9110" s="T539">sagen-PRS.[3SG]</ta>
            <ta e="T541" id="Seg_9111" s="T540">EMPH</ta>
            <ta e="T542" id="Seg_9112" s="T541">sterben-FUT</ta>
            <ta e="T543" id="Seg_9113" s="T542">sterben-FUT-1SG</ta>
            <ta e="T544" id="Seg_9114" s="T543">jetzt</ta>
            <ta e="T545" id="Seg_9115" s="T544">Mensch-PL.[NOM]</ta>
            <ta e="T546" id="Seg_9116" s="T545">Unterlage.[NOM]</ta>
            <ta e="T547" id="Seg_9117" s="T546">ähnlich</ta>
            <ta e="T548" id="Seg_9118" s="T547">liegen-PRS-3PL</ta>
            <ta e="T549" id="Seg_9119" s="T548">tatsächlich</ta>
            <ta e="T550" id="Seg_9120" s="T549">jenes-PL-ABL</ta>
            <ta e="T551" id="Seg_9121" s="T550">EMPH-dann</ta>
            <ta e="T552" id="Seg_9122" s="T551">jenes</ta>
            <ta e="T553" id="Seg_9123" s="T552">wer-VBZ-PST1-3PL</ta>
            <ta e="T554" id="Seg_9124" s="T553">jenes-3SG-2SG-ACC</ta>
            <ta e="T555" id="Seg_9125" s="T554">tragen-PST1-3PL</ta>
            <ta e="T556" id="Seg_9126" s="T555">zwei</ta>
            <ta e="T557" id="Seg_9127" s="T556">Mensch.[NOM]</ta>
            <ta e="T558" id="Seg_9128" s="T557">wer-DAT/LOC</ta>
            <ta e="T559" id="Seg_9129" s="T558">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T560" id="Seg_9130" s="T559">dann</ta>
            <ta e="T561" id="Seg_9131" s="T560">sagen-PST2.[3SG]</ta>
            <ta e="T562" id="Seg_9132" s="T561">man.sagt</ta>
            <ta e="T563" id="Seg_9133" s="T562">jenes</ta>
            <ta e="T565" id="Seg_9134" s="T563">gleichwohl</ta>
            <ta e="T566" id="Seg_9135" s="T565">bringen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T567" id="Seg_9136" s="T566">äh</ta>
            <ta e="T568" id="Seg_9137" s="T567">wer-VBZ-CVB.SEQ</ta>
            <ta e="T569" id="Seg_9138" s="T568">jenes</ta>
            <ta e="T570" id="Seg_9139" s="T569">dann</ta>
            <ta e="T571" id="Seg_9140" s="T570">zurück</ta>
            <ta e="T572" id="Seg_9141" s="T571">kommen-PST1-3PL</ta>
            <ta e="T573" id="Seg_9142" s="T572">jenes-PL-EP-2SG.[NOM]</ta>
            <ta e="T574" id="Seg_9143" s="T573">sowieso</ta>
            <ta e="T575" id="Seg_9144" s="T574">Mensch.[NOM]</ta>
            <ta e="T576" id="Seg_9145" s="T575">sein-FUT-1SG</ta>
            <ta e="T577" id="Seg_9146" s="T576">NEG-3SG</ta>
            <ta e="T578" id="Seg_9147" s="T577">was-ACC</ta>
            <ta e="T579" id="Seg_9148" s="T578">1SG-ACC</ta>
            <ta e="T580" id="Seg_9149" s="T579">mit</ta>
            <ta e="T583" id="Seg_9150" s="T582">wer-VBZ-2PL</ta>
            <ta e="T584" id="Seg_9151" s="T583">sagen-PRS.[3SG]</ta>
            <ta e="T585" id="Seg_9152" s="T584">man.sagt</ta>
            <ta e="T586" id="Seg_9153" s="T585">am.Morgen</ta>
            <ta e="T587" id="Seg_9154" s="T586">Morgenrot-1PL.[NOM]</ta>
            <ta e="T588" id="Seg_9155" s="T587">soeben</ta>
            <ta e="T589" id="Seg_9156" s="T588">hinausgehen-PRS.[3SG]</ta>
            <ta e="T590" id="Seg_9157" s="T589">hinausgehen-CVB.SEQ</ta>
            <ta e="T593" id="Seg_9158" s="T592">äh</ta>
            <ta e="T594" id="Seg_9159" s="T593">wer-VBZ-CVB.SEQ</ta>
            <ta e="T595" id="Seg_9160" s="T594">sein-TEMP-3SG</ta>
            <ta e="T596" id="Seg_9161" s="T595">was.[NOM]</ta>
            <ta e="T597" id="Seg_9162" s="T596">INDEF</ta>
            <ta e="T598" id="Seg_9163" s="T597">Leutnant-ACC</ta>
            <ta e="T599" id="Seg_9164" s="T598">treffen-PST1-1PL</ta>
            <ta e="T600" id="Seg_9165" s="T599">jenes-3SG-1PL.[NOM]</ta>
            <ta e="T601" id="Seg_9166" s="T600">sagen-PRS.[3SG]</ta>
            <ta e="T602" id="Seg_9167" s="T601">was.für.ein</ta>
            <ta e="T603" id="Seg_9168" s="T602">Teil-ABL-2SG=Q</ta>
            <ta e="T604" id="Seg_9169" s="T603">2PL.[NOM]</ta>
            <ta e="T605" id="Seg_9170" s="T604">2PL-ABL</ta>
            <ta e="T606" id="Seg_9171" s="T605">sagen-PRS-1PL</ta>
            <ta e="T607" id="Seg_9172" s="T606">sagen-PRS.[3SG]</ta>
            <ta e="T608" id="Seg_9173" s="T607">dann</ta>
            <ta e="T609" id="Seg_9174" s="T608">eins-1PL.[NOM]</ta>
            <ta e="T610" id="Seg_9175" s="T609">dann</ta>
            <ta e="T611" id="Seg_9176" s="T610">dann</ta>
            <ta e="T612" id="Seg_9177" s="T611">1SG-ACC</ta>
            <ta e="T613" id="Seg_9178" s="T612">mit</ta>
            <ta e="T614" id="Seg_9179" s="T613">laufen-EP-IMP.2PL</ta>
            <ta e="T615" id="Seg_9180" s="T614">sagen-PRS.[3SG]</ta>
            <ta e="T616" id="Seg_9181" s="T615">Vorderteil.[NOM]</ta>
            <ta e="T617" id="Seg_9182" s="T616">zu</ta>
            <ta e="T619" id="Seg_9183" s="T618">nur</ta>
            <ta e="T622" id="Seg_9184" s="T619">wer-VBZ-NEG-PTCP.FUT-DAT/LOC</ta>
            <ta e="T623" id="Seg_9185" s="T622">Verbindung.[NOM]</ta>
            <ta e="T624" id="Seg_9186" s="T623">nehmen-CVB.SIM</ta>
            <ta e="T625" id="Seg_9187" s="T624">wer-VBZ-PRS.[3SG]</ta>
            <ta e="T626" id="Seg_9188" s="T625">offenbar</ta>
            <ta e="T627" id="Seg_9189" s="T626">doch</ta>
            <ta e="T628" id="Seg_9190" s="T627">dann</ta>
            <ta e="T629" id="Seg_9191" s="T628">jenes</ta>
            <ta e="T630" id="Seg_9192" s="T629">wer-VBZ-CVB.SIM</ta>
            <ta e="T631" id="Seg_9193" s="T630">versuchen-PRS-1PL</ta>
            <ta e="T632" id="Seg_9194" s="T631">wie.viel</ta>
            <ta e="T633" id="Seg_9195" s="T632">INDEF</ta>
            <ta e="T634" id="Seg_9196" s="T633">Tag-ACC</ta>
            <ta e="T635" id="Seg_9197" s="T634">jenes</ta>
            <ta e="T636" id="Seg_9198" s="T635">gehen-PST1-1PL</ta>
            <ta e="T637" id="Seg_9199" s="T636">jenes.[NOM]</ta>
            <ta e="T638" id="Seg_9200" s="T637">wie</ta>
            <ta e="T639" id="Seg_9201" s="T638">Kompanie-1PL-ACC</ta>
            <ta e="T640" id="Seg_9202" s="T639">NEG</ta>
            <ta e="T641" id="Seg_9203" s="T640">finden-NEG-1PL</ta>
            <ta e="T642" id="Seg_9204" s="T641">doch</ta>
            <ta e="T643" id="Seg_9205" s="T642">dann</ta>
            <ta e="T644" id="Seg_9206" s="T643">spät-ADJZ-3SG-ACC</ta>
            <ta e="T645" id="Seg_9207" s="T644">wer-VBZ-PST1</ta>
            <ta e="T646" id="Seg_9208" s="T645">äh</ta>
            <ta e="T647" id="Seg_9209" s="T646">finden-PST1-1PL</ta>
            <ta e="T648" id="Seg_9210" s="T647">wieder</ta>
            <ta e="T649" id="Seg_9211" s="T648">AFFIRM</ta>
            <ta e="T652" id="Seg_9212" s="T651">drei-ORD</ta>
            <ta e="T653" id="Seg_9213" s="T652">Tag-1PL-DAT/LOC</ta>
            <ta e="T654" id="Seg_9214" s="T653">Q</ta>
            <ta e="T655" id="Seg_9215" s="T654">wie.viel-ORD-1PL-DAT/LOC</ta>
            <ta e="T656" id="Seg_9216" s="T655">Q</ta>
            <ta e="T657" id="Seg_9217" s="T656">weg</ta>
            <ta e="T658" id="Seg_9218" s="T657">gehen-PRS-1PL</ta>
            <ta e="T659" id="Seg_9219" s="T658">dorthin</ta>
            <ta e="T660" id="Seg_9220" s="T659">wer-DAT/LOC</ta>
            <ta e="T661" id="Seg_9221" s="T660">jenes</ta>
            <ta e="T662" id="Seg_9222" s="T661">Fluss-1PL-ACC</ta>
            <ta e="T663" id="Seg_9223" s="T662">hinausgehen-PRS-1PL</ta>
            <ta e="T664" id="Seg_9224" s="T663">doch</ta>
            <ta e="T665" id="Seg_9225" s="T664">dann</ta>
            <ta e="T666" id="Seg_9226" s="T665">jenes</ta>
            <ta e="T667" id="Seg_9227" s="T666">sich.treffen-PRS-1PL</ta>
            <ta e="T668" id="Seg_9228" s="T667">jenes</ta>
            <ta e="T669" id="Seg_9229" s="T668">entfernt-ABL</ta>
            <ta e="T670" id="Seg_9230" s="T669">schießen-FREQ-PRS-3PL</ta>
            <ta e="T671" id="Seg_9231" s="T670">und</ta>
            <ta e="T672" id="Seg_9232" s="T671">1PL.[NOM]</ta>
            <ta e="T673" id="Seg_9233" s="T672">jene.Seite-ABL</ta>
            <ta e="T674" id="Seg_9234" s="T673">wieder</ta>
            <ta e="T675" id="Seg_9235" s="T674">schießen-FREQ-EP-RECP/COLL-PRS-1PL</ta>
            <ta e="T676" id="Seg_9236" s="T675">MOD</ta>
            <ta e="T677" id="Seg_9237" s="T676">doch</ta>
            <ta e="T678" id="Seg_9238" s="T677">jenes</ta>
            <ta e="T679" id="Seg_9239" s="T678">wer-VBZ-PST1-1PL</ta>
            <ta e="T680" id="Seg_9240" s="T679">und</ta>
            <ta e="T681" id="Seg_9241" s="T680">dort</ta>
            <ta e="T682" id="Seg_9242" s="T681">wohin</ta>
            <ta e="T683" id="Seg_9243" s="T682">NEG</ta>
            <ta e="T684" id="Seg_9244" s="T683">wer-VBZ-NEG-3PL</ta>
            <ta e="T685" id="Seg_9245" s="T684">lassen-NEG-3PL</ta>
            <ta e="T686" id="Seg_9246" s="T685">jenes</ta>
            <ta e="T687" id="Seg_9247" s="T686">am.Morgen</ta>
            <ta e="T688" id="Seg_9248" s="T687">Fähigkeit-PROPR.[NOM]</ta>
            <ta e="T689" id="Seg_9249" s="T688">kommen-PST1-3PL</ta>
            <ta e="T690" id="Seg_9250" s="T689">jenes.[NOM]</ta>
            <ta e="T691" id="Seg_9251" s="T690">zu</ta>
            <ta e="T692" id="Seg_9252" s="T691">Vorderteil-1PL-ABL</ta>
            <ta e="T693" id="Seg_9253" s="T692">dieses</ta>
            <ta e="T696" id="Seg_9254" s="T695">Kommando.[NOM]</ta>
            <ta e="T697" id="Seg_9255" s="T696">geben-PRS-3PL</ta>
            <ta e="T698" id="Seg_9256" s="T697">also</ta>
            <ta e="T699" id="Seg_9257" s="T698">dann</ta>
            <ta e="T700" id="Seg_9258" s="T699">drei</ta>
            <ta e="T701" id="Seg_9259" s="T700">Stunde-DAT/LOC</ta>
            <ta e="T702" id="Seg_9260" s="T701">Brjansk-ACC</ta>
            <ta e="T703" id="Seg_9261" s="T702">einnehmen-VBZ-PTCP.FUT-2PL-ACC</ta>
            <ta e="T704" id="Seg_9262" s="T703">man.muss</ta>
            <ta e="T705" id="Seg_9263" s="T704">doch</ta>
            <ta e="T706" id="Seg_9264" s="T705">dort</ta>
            <ta e="T707" id="Seg_9265" s="T706">so</ta>
            <ta e="T708" id="Seg_9266" s="T707">was.[NOM]</ta>
            <ta e="T709" id="Seg_9267" s="T708">INDEF</ta>
            <ta e="T710" id="Seg_9268" s="T709">Berg.[NOM]</ta>
            <ta e="T711" id="Seg_9269" s="T710">sein-PRS.[3SG]</ta>
            <ta e="T712" id="Seg_9270" s="T711">schmaler.Streifen.[NOM]</ta>
            <ta e="T715" id="Seg_9271" s="T714">schmaler.Streifen-ACC</ta>
            <ta e="T716" id="Seg_9272" s="T715">äh</ta>
            <ta e="T717" id="Seg_9273" s="T716">wer-VBZ-PST1-1PL</ta>
            <ta e="T718" id="Seg_9274" s="T717">und</ta>
            <ta e="T719" id="Seg_9275" s="T718">auch</ta>
            <ta e="T720" id="Seg_9276" s="T719">wieder</ta>
            <ta e="T721" id="Seg_9277" s="T720">Gesicht-3SG.[NOM]</ta>
            <ta e="T724" id="Seg_9278" s="T723">Vorderteil-EP-ABL</ta>
            <ta e="T725" id="Seg_9279" s="T724">wer-VBZ-PRS-3PL</ta>
            <ta e="T726" id="Seg_9280" s="T725">EMPH</ta>
            <ta e="T729" id="Seg_9281" s="T728">Deutscher-PL.[NOM]</ta>
            <ta e="T730" id="Seg_9282" s="T729">so</ta>
            <ta e="T731" id="Seg_9283" s="T730">liegen-PRS-3PL</ta>
            <ta e="T733" id="Seg_9284" s="T732">machen-CVB.SIM</ta>
            <ta e="T734" id="Seg_9285" s="T733">liegen-PRS-3PL</ta>
            <ta e="T735" id="Seg_9286" s="T734">EMPH</ta>
            <ta e="T736" id="Seg_9287" s="T735">und</ta>
            <ta e="T737" id="Seg_9288" s="T736">1PL.[NOM]</ta>
            <ta e="T738" id="Seg_9289" s="T737">dieses</ta>
            <ta e="T739" id="Seg_9290" s="T738">Seite-EP-ABL</ta>
            <ta e="T740" id="Seg_9291" s="T739">auch</ta>
            <ta e="T741" id="Seg_9292" s="T740">schießen-FREQ-EP-RECP/COLL-PRS-1PL</ta>
            <ta e="T742" id="Seg_9293" s="T741">doch</ta>
            <ta e="T743" id="Seg_9294" s="T742">dann</ta>
            <ta e="T744" id="Seg_9295" s="T743">dieses</ta>
            <ta e="T745" id="Seg_9296" s="T744">Nacht-ACC</ta>
            <ta e="T746" id="Seg_9297" s="T745">durch</ta>
            <ta e="T747" id="Seg_9298" s="T746">liegen-CVB.SIM</ta>
            <ta e="T748" id="Seg_9299" s="T747">hier</ta>
            <ta e="T749" id="Seg_9300" s="T748">EMPH</ta>
            <ta e="T750" id="Seg_9301" s="T749">Nacht-ACC</ta>
            <ta e="T751" id="Seg_9302" s="T750">zehn-PL.[NOM]</ta>
            <ta e="T752" id="Seg_9303" s="T751">Q</ta>
            <ta e="T753" id="Seg_9304" s="T752">zwei-PL.[NOM]</ta>
            <ta e="T754" id="Seg_9305" s="T753">zu</ta>
            <ta e="T755" id="Seg_9306" s="T754">Q</ta>
            <ta e="T756" id="Seg_9307" s="T755">oder</ta>
            <ta e="T757" id="Seg_9308" s="T756">Stunde-PL.[NOM]</ta>
            <ta e="T758" id="Seg_9309" s="T757">zu</ta>
            <ta e="T759" id="Seg_9310" s="T758">Q</ta>
            <ta e="T760" id="Seg_9311" s="T759">jenes</ta>
            <ta e="T761" id="Seg_9312" s="T760">Rakete-ACC</ta>
            <ta e="T762" id="Seg_9313" s="T761">schicken-PRS-3PL</ta>
            <ta e="T763" id="Seg_9314" s="T762">Rakete-ACC</ta>
            <ta e="T764" id="Seg_9315" s="T763">schicken-TEMP-3SG</ta>
            <ta e="T765" id="Seg_9316" s="T764">dann</ta>
            <ta e="T766" id="Seg_9317" s="T765">dieses</ta>
            <ta e="T767" id="Seg_9318" s="T766">ganz</ta>
            <ta e="T769" id="Seg_9319" s="T768">wer-VBZ-CVB.SIM</ta>
            <ta e="T770" id="Seg_9320" s="T769">liegen-PTCP.PRS-PL.[NOM]</ta>
            <ta e="T771" id="Seg_9321" s="T770">ganz</ta>
            <ta e="T772" id="Seg_9322" s="T771">zu.sehen.sein-PRS-3PL</ta>
            <ta e="T773" id="Seg_9323" s="T772">Hand-1SG-ACC</ta>
            <ta e="T774" id="Seg_9324" s="T773">treffen-CAUS-CVB.SEQ</ta>
            <ta e="T775" id="Seg_9325" s="T774">dieses</ta>
            <ta e="T776" id="Seg_9326" s="T775">bemerken-NEG-PST1-1SG</ta>
            <ta e="T777" id="Seg_9327" s="T776">dann</ta>
            <ta e="T778" id="Seg_9328" s="T777">jenes</ta>
            <ta e="T779" id="Seg_9329" s="T778">dieses-3SG-1SG-DAT/LOC</ta>
            <ta e="T780" id="Seg_9330" s="T779">treffen</ta>
            <ta e="T781" id="Seg_9331" s="T780">wer-VBZ-PST2.[3SG]</ta>
            <ta e="T782" id="Seg_9332" s="T781">jenes</ta>
            <ta e="T783" id="Seg_9333" s="T782">direkt</ta>
            <ta e="T784" id="Seg_9334" s="T783">fliegen-CVB.SEQ</ta>
            <ta e="T785" id="Seg_9335" s="T784">eins</ta>
            <ta e="T786" id="Seg_9336" s="T785">Mine.[NOM]</ta>
            <ta e="T787" id="Seg_9337" s="T786">kommen-PST1-3SG</ta>
            <ta e="T788" id="Seg_9338" s="T787">und</ta>
            <ta e="T789" id="Seg_9339" s="T788">jeder-1PL-ACC</ta>
            <ta e="T790" id="Seg_9340" s="T789">treffen-PST2.[3SG]</ta>
            <ta e="T791" id="Seg_9341" s="T790">jenes</ta>
            <ta e="T792" id="Seg_9342" s="T791">zehn</ta>
            <ta e="T793" id="Seg_9343" s="T792">mehr</ta>
            <ta e="T794" id="Seg_9344" s="T793">Mensch-ACC</ta>
            <ta e="T795" id="Seg_9345" s="T794">Gewehr-1SG-ACC</ta>
            <ta e="T796" id="Seg_9346" s="T795">so</ta>
            <ta e="T797" id="Seg_9347" s="T796">halten-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T798" id="Seg_9348" s="T797">Verstand-VBZ-PST1-1SG</ta>
            <ta e="T799" id="Seg_9349" s="T798">dann</ta>
            <ta e="T800" id="Seg_9350" s="T799">so</ta>
            <ta e="T801" id="Seg_9351" s="T800">so</ta>
            <ta e="T802" id="Seg_9352" s="T801">stehen-PRS-1SG</ta>
            <ta e="T803" id="Seg_9353" s="T802">was.[NOM]</ta>
            <ta e="T804" id="Seg_9354" s="T803">Knie-SIM</ta>
            <ta e="T805" id="Seg_9355" s="T804">doch</ta>
            <ta e="T806" id="Seg_9356" s="T805">dann</ta>
            <ta e="T807" id="Seg_9357" s="T806">fallen-PST1-1SG</ta>
            <ta e="T808" id="Seg_9358" s="T807">so</ta>
            <ta e="T809" id="Seg_9359" s="T808">doch</ta>
            <ta e="T810" id="Seg_9360" s="T809">dann</ta>
            <ta e="T811" id="Seg_9361" s="T810">später</ta>
            <ta e="T812" id="Seg_9362" s="T811">sehen-PST1-1SG</ta>
            <ta e="T813" id="Seg_9363" s="T812">EMPH</ta>
            <ta e="T814" id="Seg_9364" s="T813">Finger-PL-EP-1SG.[NOM]</ta>
            <ta e="T815" id="Seg_9365" s="T814">ganz</ta>
            <ta e="T816" id="Seg_9366" s="T815">zittern-CVB.SIM</ta>
            <ta e="T817" id="Seg_9367" s="T816">gehen-PRS-3PL</ta>
            <ta e="T818" id="Seg_9368" s="T817">Blut-EP-1SG.[NOM]</ta>
            <ta e="T819" id="Seg_9369" s="T818">fließen-CVB.SIM</ta>
            <ta e="T820" id="Seg_9370" s="T819">liegen-PRS.[3SG]</ta>
            <ta e="T821" id="Seg_9371" s="T820">so</ta>
            <ta e="T822" id="Seg_9372" s="T821">doch</ta>
            <ta e="T823" id="Seg_9373" s="T822">dann</ta>
            <ta e="T824" id="Seg_9374" s="T823">Gürtel-1SG-ACC</ta>
            <ta e="T825" id="Seg_9375" s="T824">ausziehen-PST1-1SG</ta>
            <ta e="T826" id="Seg_9376" s="T825">so</ta>
            <ta e="T827" id="Seg_9377" s="T826">Gürtel-1SG-ACC</ta>
            <ta e="T828" id="Seg_9378" s="T827">ausziehen-CVB.SEQ</ta>
            <ta e="T829" id="Seg_9379" s="T828">gehen-CVB.SEQ-1SG</ta>
            <ta e="T830" id="Seg_9380" s="T829">so</ta>
            <ta e="T831" id="Seg_9381" s="T830">wer-VBZ-PST1-1SG</ta>
            <ta e="T832" id="Seg_9382" s="T831">offenbar</ta>
            <ta e="T833" id="Seg_9383" s="T832">wer-1SG-DAT/LOC</ta>
            <ta e="T834" id="Seg_9384" s="T833">Hals-1SG-DAT/LOC</ta>
            <ta e="T835" id="Seg_9385" s="T834">so</ta>
            <ta e="T838" id="Seg_9386" s="T837">wer-VBZ-PST1-1SG</ta>
            <ta e="T839" id="Seg_9387" s="T838">dieses</ta>
            <ta e="T840" id="Seg_9388" s="T839">binden-CVB.SEQ</ta>
            <ta e="T841" id="Seg_9389" s="T840">gehen-CVB.SEQ-1SG</ta>
            <ta e="T842" id="Seg_9390" s="T841">dann</ta>
            <ta e="T843" id="Seg_9391" s="T842">dieses</ta>
            <ta e="T844" id="Seg_9392" s="T843">Paket-PL.[NOM]</ta>
            <ta e="T845" id="Seg_9393" s="T844">sein-HAB-3PL</ta>
            <ta e="T848" id="Seg_9394" s="T847">doch</ta>
            <ta e="T849" id="Seg_9395" s="T848">jenes-3SG-1SG-INSTR</ta>
            <ta e="T850" id="Seg_9396" s="T849">umdrehen-PST1-1SG</ta>
            <ta e="T851" id="Seg_9397" s="T850">so</ta>
            <ta e="T852" id="Seg_9398" s="T851">dann</ta>
            <ta e="T853" id="Seg_9399" s="T852">Gamaschen.[NOM]</ta>
            <ta e="T854" id="Seg_9400" s="T853">dieses.[NOM]</ta>
            <ta e="T855" id="Seg_9401" s="T854">hohe.Stiefel-DAT/LOC</ta>
            <ta e="T856" id="Seg_9402" s="T855">wer.[NOM]</ta>
            <ta e="T857" id="Seg_9403" s="T856">sein-HAB.[3SG]</ta>
            <ta e="T860" id="Seg_9404" s="T859">jenes-ACC</ta>
            <ta e="T861" id="Seg_9405" s="T860">Gamaschen-1SG-INSTR</ta>
            <ta e="T862" id="Seg_9406" s="T861">umdrehen-PST1-1SG</ta>
            <ta e="T863" id="Seg_9407" s="T862">gehen.[IMP.2SG]</ta>
            <ta e="T864" id="Seg_9408" s="T863">sagen-PRS-3PL</ta>
            <ta e="T865" id="Seg_9409" s="T864">wer.[NOM]</ta>
            <ta e="T866" id="Seg_9410" s="T865">Befehlshaber.[NOM]</ta>
            <ta e="T867" id="Seg_9411" s="T866">Kompanie-DAT/LOC</ta>
            <ta e="T868" id="Seg_9412" s="T867">jenes.EMPH.[NOM]</ta>
            <ta e="T869" id="Seg_9413" s="T868">kriechen-CVB.SEQ</ta>
            <ta e="T870" id="Seg_9414" s="T869">kriechen-PTCP.PST-EP-INSTR</ta>
            <ta e="T871" id="Seg_9415" s="T870">ankommen-PST1-1SG</ta>
            <ta e="T872" id="Seg_9416" s="T871">jenes-3SG-1SG-DAT/LOC</ta>
            <ta e="T873" id="Seg_9417" s="T872">Brot.[NOM]</ta>
            <ta e="T874" id="Seg_9418" s="T873">suchen-CVB.SEQ</ta>
            <ta e="T875" id="Seg_9419" s="T874">äh</ta>
            <ta e="T878" id="Seg_9420" s="T877">Nahrung.[NOM]</ta>
            <ta e="T879" id="Seg_9421" s="T878">na</ta>
            <ta e="T880" id="Seg_9422" s="T879">sagen-PRS.[3SG]</ta>
            <ta e="T881" id="Seg_9423" s="T880">verwunden-CVB.SIM</ta>
            <ta e="T882" id="Seg_9424" s="T881">machen</ta>
            <ta e="T883" id="Seg_9425" s="T930">Q</ta>
            <ta e="T885" id="Seg_9426" s="T883">sagen-PRS.[3SG]</ta>
            <ta e="T886" id="Seg_9427" s="T885">hmm</ta>
            <ta e="T887" id="Seg_9428" s="T886">sagen-PRS-1SG</ta>
            <ta e="T888" id="Seg_9429" s="T887">Gewehr-1SG-ACC</ta>
            <ta e="T889" id="Seg_9430" s="T888">dann</ta>
            <ta e="T890" id="Seg_9431" s="T889">nehmen-PTCP.PST</ta>
            <ta e="T891" id="Seg_9432" s="T890">sein-PST1-3SG</ta>
            <ta e="T892" id="Seg_9433" s="T891">jenes</ta>
            <ta e="T893" id="Seg_9434" s="T892">wer-EP-1SG.[NOM]</ta>
            <ta e="T894" id="Seg_9435" s="T893">Befehlshaber-1SG-DAT/LOC</ta>
            <ta e="T895" id="Seg_9436" s="T894">abgeben-VBZ-PRS-1SG</ta>
            <ta e="T896" id="Seg_9437" s="T895">EMPH</ta>
            <ta e="T897" id="Seg_9438" s="T896">wer-DAT/LOC</ta>
            <ta e="T898" id="Seg_9439" s="T897">Nummer-EP-INSTR</ta>
            <ta e="T899" id="Seg_9440" s="T898">sein-PTCP.HAB</ta>
            <ta e="T900" id="Seg_9441" s="T899">sein-PST1-3PL</ta>
            <ta e="T901" id="Seg_9442" s="T900">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T902" id="Seg_9443" s="T901">EMPH</ta>
            <ta e="T903" id="Seg_9444" s="T902">wer.[NOM]</ta>
            <ta e="T904" id="Seg_9445" s="T903">Patrone-PL.[NOM]</ta>
            <ta e="T905" id="Seg_9446" s="T904">Tasche-1SG-DAT/LOC</ta>
            <ta e="T906" id="Seg_9447" s="T905">was.[NOM]</ta>
            <ta e="T907" id="Seg_9448" s="T906">NEG</ta>
            <ta e="T908" id="Seg_9449" s="T907">NEG.EX</ta>
            <ta e="T909" id="Seg_9450" s="T908">Granate-PL-1SG-ACC</ta>
            <ta e="T910" id="Seg_9451" s="T909">auch</ta>
            <ta e="T911" id="Seg_9452" s="T910">nehmen-PST2-3PL</ta>
            <ta e="T912" id="Seg_9453" s="T911">Schützengraben-DAT/LOC</ta>
            <ta e="T913" id="Seg_9454" s="T912">sich.hinlegen-TEMP-1SG</ta>
            <ta e="T914" id="Seg_9455" s="T913">Sand.[NOM]</ta>
            <ta e="T915" id="Seg_9456" s="T914">fallen-FREQ-PRS.[3SG]</ta>
            <ta e="T916" id="Seg_9457" s="T915">EMPH</ta>
            <ta e="T917" id="Seg_9458" s="T916">jenes</ta>
            <ta e="T918" id="Seg_9459" s="T917">Seite-ABL</ta>
            <ta e="T919" id="Seg_9460" s="T918">kommen-CVB.SIM</ta>
            <ta e="T920" id="Seg_9461" s="T919">EMPH</ta>
            <ta e="T921" id="Seg_9462" s="T920">Mine-PL.[NOM]</ta>
            <ta e="T922" id="Seg_9463" s="T921">fallen-TEMP-3PL</ta>
            <ta e="T923" id="Seg_9464" s="T922">Erde-EP-1SG.[NOM]</ta>
            <ta e="T924" id="Seg_9465" s="T923">zittern-PRS.[3SG]</ta>
            <ta e="T929" id="Seg_9466" s="T928">wer-VBZ-PRS-3PL</ta>
            <ta e="T931" id="Seg_9467" s="T929">nur</ta>
            <ta e="T932" id="Seg_9468" s="T931">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T933" id="Seg_9469" s="T932">dieses</ta>
            <ta e="T934" id="Seg_9470" s="T933">Ablösung.[NOM]</ta>
            <ta e="T935" id="Seg_9471" s="T937">Ergänzung.[NOM]</ta>
            <ta e="T936" id="Seg_9472" s="T935">kommen-PTCP.PRS</ta>
            <ta e="T938" id="Seg_9473" s="T936">sein-PST2.[3SG]</ta>
            <ta e="T939" id="Seg_9474" s="T938">1PL.[NOM]</ta>
            <ta e="T940" id="Seg_9475" s="T939">Stelle-1PL-DAT/LOC</ta>
            <ta e="T941" id="Seg_9476" s="T940">noch</ta>
            <ta e="T942" id="Seg_9477" s="T941">nächster-PL.[NOM]</ta>
            <ta e="T943" id="Seg_9478" s="T942">kommen-PRS-3PL</ta>
            <ta e="T944" id="Seg_9479" s="T943">sein-PRS.[3SG]</ta>
            <ta e="T945" id="Seg_9480" s="T944">Lärm.[NOM]</ta>
            <ta e="T946" id="Seg_9481" s="T945">werden-FREQ-PRS.[3SG]</ta>
            <ta e="T947" id="Seg_9482" s="T946">EMPH</ta>
            <ta e="T948" id="Seg_9483" s="T947">dann</ta>
            <ta e="T949" id="Seg_9484" s="T948">weinen-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T950" id="Seg_9485" s="T949">werden-PRS-3PL</ta>
            <ta e="T951" id="Seg_9486" s="T950">nur</ta>
            <ta e="T952" id="Seg_9487" s="T951">mancher.[NOM]-mancher.[NOM]</ta>
            <ta e="T953" id="Seg_9488" s="T952">stöhnen-PTCP.PRS</ta>
            <ta e="T954" id="Seg_9489" s="T953">sein-PRS.[3SG]</ta>
            <ta e="T955" id="Seg_9490" s="T954">stöhnen-PTCP.PRS</ta>
            <ta e="T956" id="Seg_9491" s="T955">sein-TEMP-3PL</ta>
            <ta e="T957" id="Seg_9492" s="T956">1SG.[NOM]</ta>
            <ta e="T958" id="Seg_9493" s="T957">auch</ta>
            <ta e="T959" id="Seg_9494" s="T958">auch</ta>
            <ta e="T960" id="Seg_9495" s="T959">stöhnen-PRS-1SG</ta>
            <ta e="T961" id="Seg_9496" s="T960">auch</ta>
            <ta e="T962" id="Seg_9497" s="T961">dann</ta>
            <ta e="T963" id="Seg_9498" s="T962">wieder</ta>
            <ta e="T964" id="Seg_9499" s="T963">abklingen-NMNZ.[NOM]</ta>
            <ta e="T965" id="Seg_9500" s="T964">machen-PRS-1SG</ta>
            <ta e="T966" id="Seg_9501" s="T965">zuhören-PRS-1SG</ta>
            <ta e="T967" id="Seg_9502" s="T966">Lazarett-DAT/LOC</ta>
            <ta e="T968" id="Seg_9503" s="T967">kommen-PST2-EP-1SG</ta>
            <ta e="T971" id="Seg_9504" s="T970">Feld</ta>
            <ta e="T972" id="Seg_9505" s="T971">Arbeit.[NOM]</ta>
            <ta e="T973" id="Seg_9506" s="T972">wer.[NOM]</ta>
            <ta e="T974" id="Seg_9507" s="T973">sein-HAB.[3SG]</ta>
            <ta e="T975" id="Seg_9508" s="T974">Lazarett.[NOM]</ta>
            <ta e="T976" id="Seg_9509" s="T975">jenes</ta>
            <ta e="T977" id="Seg_9510" s="T976">Lazarett-DAT/LOC</ta>
            <ta e="T978" id="Seg_9511" s="T977">kommen-PST1</ta>
            <ta e="T979" id="Seg_9512" s="T978">äh</ta>
            <ta e="T980" id="Seg_9513" s="T979">kommen-PST1-1SG</ta>
            <ta e="T981" id="Seg_9514" s="T980">und</ta>
            <ta e="T982" id="Seg_9515" s="T981">Heimatfront-DAT/LOC</ta>
            <ta e="T983" id="Seg_9516" s="T982">schicken-PST2-3PL</ta>
            <ta e="T984" id="Seg_9517" s="T983">Ivanovo.Šuja.[NOM]</ta>
            <ta e="T985" id="Seg_9518" s="T984">liegen-PST2-EP-1SG</ta>
            <ta e="T986" id="Seg_9519" s="T985">alt</ta>
            <ta e="T987" id="Seg_9520" s="T986">Krankenhaus-DAT/LOC</ta>
            <ta e="T988" id="Seg_9521" s="T987">dann</ta>
            <ta e="T991" id="Seg_9522" s="T990">äh</ta>
            <ta e="T992" id="Seg_9523" s="T991">sofort</ta>
            <ta e="T993" id="Seg_9524" s="T992">fünf</ta>
            <ta e="T994" id="Seg_9525" s="T993">Monat-ACC</ta>
            <ta e="T995" id="Seg_9526" s="T994">liegen-PST1-1SG</ta>
            <ta e="T996" id="Seg_9527" s="T995">und</ta>
            <ta e="T997" id="Seg_9528" s="T996">Haus-1SG.[NOM]</ta>
            <ta e="T998" id="Seg_9529" s="T997">zu</ta>
            <ta e="T999" id="Seg_9530" s="T998">schicken-PST2-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr-ChVD">
            <ta e="T1" id="Seg_9531" s="T0">Дудинка-DAT/LOC</ta>
            <ta e="T2" id="Seg_9532" s="T1">приходить-PST2-EP-1SG</ta>
            <ta e="T6" id="Seg_9533" s="T5">август</ta>
            <ta e="T7" id="Seg_9534" s="T6">месяц-DAT/LOC</ta>
            <ta e="T11" id="Seg_9535" s="T10">комиссия-ACC</ta>
            <ta e="T12" id="Seg_9536" s="T11">проходить-CVB.SIM</ta>
            <ta e="T13" id="Seg_9537" s="T12">делать-PST1-1SG</ta>
            <ta e="T14" id="Seg_9538" s="T13">нестроевой-DAT/LOC</ta>
            <ta e="T15" id="Seg_9539" s="T14">Норильск-DAT/LOC</ta>
            <ta e="T16" id="Seg_9540" s="T15">послать-PST2-3PL</ta>
            <ta e="T17" id="Seg_9541" s="T16">Норильск-ABL</ta>
            <ta e="T18" id="Seg_9542" s="T17">кто-DAT/LOC</ta>
            <ta e="T19" id="Seg_9543" s="T18">ээ</ta>
            <ta e="T9" id="Seg_9544" s="T19">военизированная</ta>
            <ta e="T20" id="Seg_9545" s="T9">охрана-DAT/LOC</ta>
            <ta e="T21" id="Seg_9546" s="T20">работать-CVB.SIM</ta>
            <ta e="T22" id="Seg_9547" s="T21">падать-PST1-1SG</ta>
            <ta e="T23" id="Seg_9548" s="T22">доброволец-EP-PL-ACC</ta>
            <ta e="T24" id="Seg_9549" s="T23">с</ta>
            <ta e="T25" id="Seg_9550" s="T24">опять</ta>
            <ta e="T26" id="Seg_9551" s="T25">вот</ta>
            <ta e="T27" id="Seg_9552" s="T26">попадать-PST2-EP-1SG</ta>
            <ta e="T28" id="Seg_9553" s="T27">Красноярск-DAT/LOC</ta>
            <ta e="T29" id="Seg_9554" s="T28">ээ</ta>
            <ta e="T30" id="Seg_9555" s="T29">пока</ta>
            <ta e="T31" id="Seg_9556" s="T30">идти-PST2-EP-1SG</ta>
            <ta e="T32" id="Seg_9557" s="T31">Спартак</ta>
            <ta e="T33" id="Seg_9558" s="T32">пароход-3SG-INSTR</ta>
            <ta e="T34" id="Seg_9559" s="T33">потом</ta>
            <ta e="T35" id="Seg_9560" s="T34">учиться-PST1-1SG</ta>
            <ta e="T36" id="Seg_9561" s="T35">Красноярск</ta>
            <ta e="T37" id="Seg_9562" s="T36">пехота-DAT/LOC</ta>
            <ta e="T41" id="Seg_9563" s="T40">десять</ta>
            <ta e="T42" id="Seg_9564" s="T41">один</ta>
            <ta e="T43" id="Seg_9565" s="T42">месяц-ACC</ta>
            <ta e="T44" id="Seg_9566" s="T43">Q</ta>
            <ta e="T45" id="Seg_9567" s="T44">сколько-ACC</ta>
            <ta e="T46" id="Seg_9568" s="T45">Q</ta>
            <ta e="T47" id="Seg_9569" s="T46">учиться-CVB.SEQ</ta>
            <ta e="T48" id="Seg_9570" s="T47">после</ta>
            <ta e="T49" id="Seg_9571" s="T48">опять</ta>
            <ta e="T50" id="Seg_9572" s="T49">повторять-CVB.SIM</ta>
            <ta e="T51" id="Seg_9573" s="T54">кто-DAT/LOC</ta>
            <ta e="T52" id="Seg_9574" s="T51">Звенигород-DAT/LOC</ta>
            <ta e="T56" id="Seg_9575" s="T55">парашют.[NOM]</ta>
            <ta e="T57" id="Seg_9576" s="T56">разбирать-VBZ-PTCP.HAB</ta>
            <ta e="T58" id="Seg_9577" s="T57">быть-PST1-1SG</ta>
            <ta e="T60" id="Seg_9578" s="T59">вот</ta>
            <ta e="T61" id="Seg_9579" s="T60">там</ta>
            <ta e="T62" id="Seg_9580" s="T61">длинный</ta>
            <ta e="T63" id="Seg_9581" s="T62">NEG</ta>
            <ta e="T64" id="Seg_9582" s="T63">учиться-PST2.NEG-EP-1SG</ta>
            <ta e="T65" id="Seg_9583" s="T64">друг-PL-EP-1SG.[NOM]</ta>
            <ta e="T66" id="Seg_9584" s="T65">с</ta>
            <ta e="T67" id="Seg_9585" s="T66">этот.[NOM]</ta>
            <ta e="T68" id="Seg_9586" s="T67">оставаться-EP-NEG-CVB.PURP-1SG</ta>
            <ta e="T69" id="Seg_9587" s="T68">вместе</ta>
            <ta e="T70" id="Seg_9588" s="T69">идти-RECP/COLL-EP-PST2-EP-1SG</ta>
            <ta e="T71" id="Seg_9589" s="T70">вот</ta>
            <ta e="T72" id="Seg_9590" s="T71">тот</ta>
            <ta e="T73" id="Seg_9591" s="T72">кто-VBZ-PST1-1SG</ta>
            <ta e="T74" id="Seg_9592" s="T73">кто-DAT/LOC</ta>
            <ta e="T75" id="Seg_9593" s="T74">Муром-DAT/LOC</ta>
            <ta e="T76" id="Seg_9594" s="T75">учиться-CVB.SIM</ta>
            <ta e="T77" id="Seg_9595" s="T76">кто-VBZ-PST1-1PL</ta>
            <ta e="T481" id="Seg_9596" s="T80">просто</ta>
            <ta e="T81" id="Seg_9597" s="T481">кто-VBZ-PTCP.PRS</ta>
            <ta e="T82" id="Seg_9598" s="T81">быть-PST1-3PL</ta>
            <ta e="T83" id="Seg_9599" s="T82">там</ta>
            <ta e="T84" id="Seg_9600" s="T83">формирование.[NOM]</ta>
            <ta e="T85" id="Seg_9601" s="T84">делать-PTCP.PRS</ta>
            <ta e="T86" id="Seg_9602" s="T85">быть-PST1-3PL</ta>
            <ta e="T92" id="Seg_9603" s="T91">что-DAT/LOC</ta>
            <ta e="T93" id="Seg_9604" s="T92">учиться-PTCP.PST-2SG-ACC</ta>
            <ta e="T94" id="Seg_9605" s="T93">кто-PROPR</ta>
            <ta e="T95" id="Seg_9606" s="T94">кто.[NOM]</ta>
            <ta e="T96" id="Seg_9607" s="T95">кто-VBZ-PRS-3PL</ta>
            <ta e="T97" id="Seg_9608" s="T96">потом</ta>
            <ta e="T101" id="Seg_9609" s="T100">вот</ta>
            <ta e="T102" id="Seg_9610" s="T101">там</ta>
            <ta e="T103" id="Seg_9611" s="T102">кто-VBZ-PST1-1SG</ta>
            <ta e="T104" id="Seg_9612" s="T103">EMPH</ta>
            <ta e="T105" id="Seg_9613" s="T104">Муром-ABL</ta>
            <ta e="T106" id="Seg_9614" s="T105">кто-DAT/LOC</ta>
            <ta e="T107" id="Seg_9615" s="T106">попадать-PST1-1PL</ta>
            <ta e="T108" id="Seg_9616" s="T107">тот</ta>
            <ta e="T109" id="Seg_9617" s="T108">Тула-DAT/LOC</ta>
            <ta e="T110" id="Seg_9618" s="T109">тот</ta>
            <ta e="T111" id="Seg_9619" s="T110">Тула.[NOM]</ta>
            <ta e="T115" id="Seg_9620" s="T114">EMPH</ta>
            <ta e="T116" id="Seg_9621" s="T115">восемь</ta>
            <ta e="T117" id="Seg_9622" s="T116">километр-ACC</ta>
            <ta e="T118" id="Seg_9623" s="T117">Q</ta>
            <ta e="T119" id="Seg_9624" s="T118">сколько-ACC</ta>
            <ta e="T120" id="Seg_9625" s="T119">Q</ta>
            <ta e="T121" id="Seg_9626" s="T120">идти-CVB.SEQ</ta>
            <ta e="T122" id="Seg_9627" s="T121">после</ta>
            <ta e="T123" id="Seg_9628" s="T122">железный</ta>
            <ta e="T124" id="Seg_9629" s="T123">тропка-PL-EP-2SG.[NOM]</ta>
            <ta e="T125" id="Seg_9630" s="T124">полностью</ta>
            <ta e="T126" id="Seg_9631" s="T125">ломаться-PST-3PL</ta>
            <ta e="T127" id="Seg_9632" s="T126">быть-PST1-3SG</ta>
            <ta e="T128" id="Seg_9633" s="T127">вот</ta>
            <ta e="T129" id="Seg_9634" s="T128">там</ta>
            <ta e="T130" id="Seg_9635" s="T129">кто-VBZ-CVB.SIM</ta>
            <ta e="T131" id="Seg_9636" s="T130">лежать-TEMP-1PL</ta>
            <ta e="T132" id="Seg_9637" s="T131">уже</ta>
            <ta e="T134" id="Seg_9638" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_9639" s="T134">всегда</ta>
            <ta e="T136" id="Seg_9640" s="T135">летать-CVB.SIM</ta>
            <ta e="T139" id="Seg_9641" s="T138">летать-PTCP.HAB</ta>
            <ta e="T140" id="Seg_9642" s="T139">быть-PST1-3PL</ta>
            <ta e="T141" id="Seg_9643" s="T140">утро.[NOM]</ta>
            <ta e="T142" id="Seg_9644" s="T141">каждый</ta>
            <ta e="T143" id="Seg_9645" s="T142">вот</ta>
            <ta e="T144" id="Seg_9646" s="T143">потом</ta>
            <ta e="T145" id="Seg_9647" s="T144">длинный</ta>
            <ta e="T146" id="Seg_9648" s="T145">NEG</ta>
            <ta e="T147" id="Seg_9649" s="T146">быть-PST1-3SG</ta>
            <ta e="T148" id="Seg_9650" s="T147">ээ</ta>
            <ta e="T151" id="Seg_9651" s="T150">кто-VBZ-PRS-1PL</ta>
            <ta e="T152" id="Seg_9652" s="T151">туда</ta>
            <ta e="T153" id="Seg_9653" s="T152">вправду</ta>
            <ta e="T154" id="Seg_9654" s="T153">пешком</ta>
            <ta e="T155" id="Seg_9655" s="T154">идти-PST2-1PL</ta>
            <ta e="T156" id="Seg_9656" s="T155">тот</ta>
            <ta e="T157" id="Seg_9657" s="T156">кто-DAT/LOC</ta>
            <ta e="T158" id="Seg_9658" s="T157">где</ta>
            <ta e="T161" id="Seg_9659" s="T160">тот.EMPH.[NOM]</ta>
            <ta e="T162" id="Seg_9660" s="T161">война.[NOM]</ta>
            <ta e="T163" id="Seg_9661" s="T162">быть-PTCP.FUT</ta>
            <ta e="T164" id="Seg_9662" s="T163">место-3SG.[NOM]</ta>
            <ta e="T165" id="Seg_9663" s="T164">быть-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T166" id="Seg_9664" s="T165">пока</ta>
            <ta e="T167" id="Seg_9665" s="T166">вот</ta>
            <ta e="T168" id="Seg_9666" s="T167">там</ta>
            <ta e="T169" id="Seg_9667" s="T168">кто-VBZ-HAB-3PL</ta>
            <ta e="T170" id="Seg_9668" s="T169">EMPH</ta>
            <ta e="T171" id="Seg_9669" s="T170">кто.[NOM]</ta>
            <ta e="T172" id="Seg_9670" s="T171">партизанский</ta>
            <ta e="T173" id="Seg_9671" s="T172">тот</ta>
            <ta e="T174" id="Seg_9672" s="T173">отряд</ta>
            <ta e="T175" id="Seg_9673" s="T174">о</ta>
            <ta e="T176" id="Seg_9674" s="T175">кто-DAT/LOC</ta>
            <ta e="T177" id="Seg_9675" s="T176">НОП</ta>
            <ta e="T178" id="Seg_9676" s="T177">СОП</ta>
            <ta e="T179" id="Seg_9677" s="T178">обозначать-VBZ-PRS-3PL</ta>
            <ta e="T180" id="Seg_9678" s="T179">тот-ACC</ta>
            <ta e="T181" id="Seg_9679" s="T180">да</ta>
            <ta e="T182" id="Seg_9680" s="T181">этот</ta>
            <ta e="T183" id="Seg_9681" s="T182">INTNS</ta>
            <ta e="T184" id="Seg_9682" s="T183">кто-VBZ</ta>
            <ta e="T185" id="Seg_9683" s="T184">кто-VBZ-PTCP.PRS-DAT/LOC</ta>
            <ta e="T186" id="Seg_9684" s="T185">река.[NOM]</ta>
            <ta e="T187" id="Seg_9685" s="T186">что</ta>
            <ta e="T188" id="Seg_9686" s="T187">INDEF</ta>
            <ta e="T189" id="Seg_9687" s="T188">река-DIM.[NOM]</ta>
            <ta e="T190" id="Seg_9688" s="T189">лежать-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_9689" s="T190">вот</ta>
            <ta e="T192" id="Seg_9690" s="T191">там</ta>
            <ta e="T193" id="Seg_9691" s="T192">деревня-DAT/LOC</ta>
            <ta e="T194" id="Seg_9692" s="T193">там</ta>
            <ta e="T196" id="Seg_9693" s="T195">делать-PRS-3PL</ta>
            <ta e="T197" id="Seg_9694" s="T196">очевидно</ta>
            <ta e="T198" id="Seg_9695" s="T197">утром</ta>
            <ta e="T199" id="Seg_9696" s="T198">кто-VBZ-PST1-1PL</ta>
            <ta e="T200" id="Seg_9697" s="T199">есть-CVB.SIM</ta>
            <ta e="T201" id="Seg_9698" s="T200">есть-CVB.SIM</ta>
            <ta e="T202" id="Seg_9699" s="T201">падать-CVB.SEQ</ta>
            <ta e="T203" id="Seg_9700" s="T202">идти-CVB.SEQ-1PL</ta>
            <ta e="T204" id="Seg_9701" s="T203">тот-ACC</ta>
            <ta e="T205" id="Seg_9702" s="T204">вот</ta>
            <ta e="T206" id="Seg_9703" s="T205">стрелять-FREQ-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T207" id="Seg_9704" s="T206">командир-1PL-ACC</ta>
            <ta e="T208" id="Seg_9705" s="T207">рассказывать-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_9706" s="T208">как</ta>
            <ta e="T212" id="Seg_9707" s="T211">кто-VBZ-PTCP.FUT-2PL-ACC</ta>
            <ta e="T214" id="Seg_9708" s="T212">потом</ta>
            <ta e="T215" id="Seg_9709" s="T214">вот</ta>
            <ta e="T216" id="Seg_9710" s="T215">окружение.[NOM]</ta>
            <ta e="T217" id="Seg_9711" s="T216">попадать-TEMP-2PL</ta>
            <ta e="T218" id="Seg_9712" s="T217">можно</ta>
            <ta e="T219" id="Seg_9713" s="T218">плен.[NOM]</ta>
            <ta e="T220" id="Seg_9714" s="T219">попадать-TEMP-2PL</ta>
            <ta e="T221" id="Seg_9715" s="T220">EMPH</ta>
            <ta e="T223" id="Seg_9716" s="T222">кто.[NOM]</ta>
            <ta e="T224" id="Seg_9717" s="T223">кто-VBZ-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T225" id="Seg_9718" s="T224">надо</ta>
            <ta e="T226" id="Seg_9719" s="T225">быть-PST1-3SG</ta>
            <ta e="T227" id="Seg_9720" s="T226">сам-2SG-ACC</ta>
            <ta e="T228" id="Seg_9721" s="T227">давать-EP-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T229" id="Seg_9722" s="T228">можно</ta>
            <ta e="T230" id="Seg_9723" s="T229">просто</ta>
            <ta e="T233" id="Seg_9724" s="T232">колоть-FREQ-NEG-IMP.2PL</ta>
            <ta e="T234" id="Seg_9725" s="T233">документ</ta>
            <ta e="T235" id="Seg_9726" s="T234">ээ</ta>
            <ta e="T236" id="Seg_9727" s="T235">документ-PL-2PL.[NOM]</ta>
            <ta e="T237" id="Seg_9728" s="T236">говорить-PRS.[3SG]</ta>
            <ta e="T238" id="Seg_9729" s="T237">MOD</ta>
            <ta e="T241" id="Seg_9730" s="T240">кто-VBZ-IMP.2PL</ta>
            <ta e="T244" id="Seg_9731" s="T243">э</ta>
            <ta e="T245" id="Seg_9732" s="T244">закопать-EP-IMP.2PL</ta>
            <ta e="T246" id="Seg_9733" s="T245">Q</ta>
            <ta e="T247" id="Seg_9734" s="T246">сам-2PL.[NOM]</ta>
            <ta e="T248" id="Seg_9735" s="T247">есть-IMP.2PL</ta>
            <ta e="T249" id="Seg_9736" s="T248">Q</ta>
            <ta e="T250" id="Seg_9737" s="T249">говорить-HAB.[3SG]</ta>
            <ta e="T251" id="Seg_9738" s="T250">так</ta>
            <ta e="T252" id="Seg_9739" s="T251">колоть-FREQ-PST1-2SG</ta>
            <ta e="T253" id="Seg_9740" s="T252">да</ta>
            <ta e="T255" id="Seg_9741" s="T253">все.равно</ta>
            <ta e="T256" id="Seg_9742" s="T255">клеить-VBZ</ta>
            <ta e="T257" id="Seg_9743" s="T256">тот.[NOM]</ta>
            <ta e="T258" id="Seg_9744" s="T257">кто-VBZ-PRS-3PL</ta>
            <ta e="T259" id="Seg_9745" s="T258">EMPH</ta>
            <ta e="T260" id="Seg_9746" s="T259">тот</ta>
            <ta e="T261" id="Seg_9747" s="T260">найти-PRS-3PL</ta>
            <ta e="T262" id="Seg_9748" s="T261">EMPH</ta>
            <ta e="T263" id="Seg_9749" s="T262">тот</ta>
            <ta e="T264" id="Seg_9750" s="T263">вот</ta>
            <ta e="T265" id="Seg_9751" s="T264">рота-2SG.[NOM]</ta>
            <ta e="T266" id="Seg_9752" s="T265">оставлять-CVB.SIM</ta>
            <ta e="T267" id="Seg_9753" s="T266">бегать-PST2-1SG</ta>
            <ta e="T268" id="Seg_9754" s="T267">однажды</ta>
            <ta e="T271" id="Seg_9755" s="T270">какой-3PL-ACC</ta>
            <ta e="T272" id="Seg_9756" s="T271">INDEF</ta>
            <ta e="T273" id="Seg_9757" s="T272">замечать-CVB.SEQ-1SG</ta>
            <ta e="T274" id="Seg_9758" s="T273">темнота-DAT/LOC</ta>
            <ta e="T275" id="Seg_9759" s="T274">немец-3PL.[NOM]</ta>
            <ta e="T276" id="Seg_9760" s="T275">да</ta>
            <ta e="T277" id="Seg_9761" s="T276">кто-3PL.[NOM]</ta>
            <ta e="T278" id="Seg_9762" s="T277">да</ta>
            <ta e="T279" id="Seg_9763" s="T278">вот</ta>
            <ta e="T280" id="Seg_9764" s="T279">тот</ta>
            <ta e="T281" id="Seg_9765" s="T280">1SG.[NOM]</ta>
            <ta e="T282" id="Seg_9766" s="T281">идти-CVB.SEQ-1SG</ta>
            <ta e="T283" id="Seg_9767" s="T282">видеть-PST1-1SG</ta>
            <ta e="T284" id="Seg_9768" s="T283">пять</ta>
            <ta e="T285" id="Seg_9769" s="T284">человек.[NOM]</ta>
            <ta e="T286" id="Seg_9770" s="T285">идти-PRS-3PL</ta>
            <ta e="T287" id="Seg_9771" s="T286">только</ta>
            <ta e="T288" id="Seg_9772" s="T287">так</ta>
            <ta e="T289" id="Seg_9773" s="T288">вот</ta>
            <ta e="T290" id="Seg_9774" s="T289">потом</ta>
            <ta e="T291" id="Seg_9775" s="T290">тот</ta>
            <ta e="T292" id="Seg_9776" s="T291">сторона-ABL</ta>
            <ta e="T293" id="Seg_9777" s="T292">немец-EP-PL-1PL.[NOM]</ta>
            <ta e="T294" id="Seg_9778" s="T293">приходить-PRS-3PL</ta>
            <ta e="T295" id="Seg_9779" s="T294">траншея-ABL</ta>
            <ta e="T296" id="Seg_9780" s="T295">вставать-PRS-3PL</ta>
            <ta e="T297" id="Seg_9781" s="T296">назад</ta>
            <ta e="T298" id="Seg_9782" s="T297">спасаться-IMP.1PL</ta>
            <ta e="T299" id="Seg_9783" s="T298">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T300" id="Seg_9784" s="T299">кто-VBZ-PRS-3PL</ta>
            <ta e="T301" id="Seg_9785" s="T300">тот</ta>
            <ta e="T302" id="Seg_9786" s="T301">человек-2SG.[NOM]</ta>
            <ta e="T303" id="Seg_9787" s="T302">командир-PL-PROPR-3PL</ta>
            <ta e="T304" id="Seg_9788" s="T303">тот</ta>
            <ta e="T305" id="Seg_9789" s="T304">друг-PL-3SG.[NOM]</ta>
            <ta e="T306" id="Seg_9790" s="T305">другой-PL.[NOM]</ta>
            <ta e="T307" id="Seg_9791" s="T306">потом</ta>
            <ta e="T308" id="Seg_9792" s="T307">другой</ta>
            <ta e="T309" id="Seg_9793" s="T308">часть-ABL</ta>
            <ta e="T310" id="Seg_9794" s="T309">сам-1SG.[NOM]</ta>
            <ta e="T313" id="Seg_9795" s="T312">друг-PL-1SG-ACC</ta>
            <ta e="T314" id="Seg_9796" s="T313">тот</ta>
            <ta e="T315" id="Seg_9797" s="T314">оставлять-CVB.SIM</ta>
            <ta e="T316" id="Seg_9798" s="T315">бегать-PST2-1SG</ta>
            <ta e="T317" id="Seg_9799" s="T316">очевидно</ta>
            <ta e="T318" id="Seg_9800" s="T317">EMPH</ta>
            <ta e="T319" id="Seg_9801" s="T318">Q</ta>
            <ta e="T320" id="Seg_9802" s="T319">что.[NOM]</ta>
            <ta e="T321" id="Seg_9803" s="T320">Q</ta>
            <ta e="T322" id="Seg_9804" s="T321">отступать-VBZ-IMP.2PL</ta>
            <ta e="T323" id="Seg_9805" s="T322">говорить-PRS.[3SG]</ta>
            <ta e="T327" id="Seg_9806" s="T326">говорить-PRS.[3SG]</ta>
            <ta e="T328" id="Seg_9807" s="T327">командир-3PL.[NOM]</ta>
            <ta e="T329" id="Seg_9808" s="T328">потом</ta>
            <ta e="T330" id="Seg_9809" s="T329">видеть-PST1-1SG</ta>
            <ta e="T331" id="Seg_9810" s="T330">ну</ta>
            <ta e="T332" id="Seg_9811" s="T331">немец-EP-PL-EP-2SG.[NOM]</ta>
            <ta e="T333" id="Seg_9812" s="T332">этот.EMPH</ta>
            <ta e="T334" id="Seg_9813" s="T333">приходить-CVB.SEQ</ta>
            <ta e="T335" id="Seg_9814" s="T334">идти-PRS-3PL</ta>
            <ta e="T336" id="Seg_9815" s="T335">что-ACC</ta>
            <ta e="T337" id="Seg_9816" s="T336">что-ACC</ta>
            <ta e="T338" id="Seg_9817" s="T337">проговаривать-PRS-3PL</ta>
            <ta e="T339" id="Seg_9818" s="T338">разговаривать-PRS-3PL</ta>
            <ta e="T340" id="Seg_9819" s="T339">назад</ta>
            <ta e="T341" id="Seg_9820" s="T340">бегать-PRS-1SG</ta>
            <ta e="T342" id="Seg_9821" s="T341">друг-PL-EP-1SG.[NOM]</ta>
            <ta e="T343" id="Seg_9822" s="T342">идти-PTCP.PST</ta>
            <ta e="T344" id="Seg_9823" s="T343">место-3PL-ACC</ta>
            <ta e="T345" id="Seg_9824" s="T344">к</ta>
            <ta e="T346" id="Seg_9825" s="T345">вовсе</ta>
            <ta e="T347" id="Seg_9826" s="T346">общаться-PRS-3PL</ta>
            <ta e="T348" id="Seg_9827" s="T347">только</ta>
            <ta e="T349" id="Seg_9828" s="T348">там</ta>
            <ta e="T350" id="Seg_9829" s="T349">трава-DAT/LOC</ta>
            <ta e="T351" id="Seg_9830" s="T350">стрелять-FREQ-PRS-3PL</ta>
            <ta e="T352" id="Seg_9831" s="T351">вправду</ta>
            <ta e="T353" id="Seg_9832" s="T352">кто-VBZ-CVB.PURP</ta>
            <ta e="T354" id="Seg_9833" s="T353">хотеть-PRS-3PL</ta>
            <ta e="T355" id="Seg_9834" s="T354">наверное</ta>
            <ta e="T357" id="Seg_9835" s="T355">потом</ta>
            <ta e="T358" id="Seg_9836" s="T357">плен.[NOM]</ta>
            <ta e="T359" id="Seg_9837" s="T358">получить-CVB.PURP</ta>
            <ta e="T360" id="Seg_9838" s="T359">конец-EP-ABL</ta>
            <ta e="T361" id="Seg_9839" s="T360">траншея-DAT/LOC</ta>
            <ta e="T362" id="Seg_9840" s="T361">приходить-PST1-1SG</ta>
            <ta e="T363" id="Seg_9841" s="T362">приходить-PST2-EP-1SG</ta>
            <ta e="T364" id="Seg_9842" s="T363">сам-1SG.[NOM]</ta>
            <ta e="T365" id="Seg_9843" s="T364">друг</ta>
            <ta e="T366" id="Seg_9844" s="T365">э</ta>
            <ta e="T367" id="Seg_9845" s="T366">рота-1SG-DAT/LOC</ta>
            <ta e="T368" id="Seg_9846" s="T367">попадать-PST2-1SG</ta>
            <ta e="T369" id="Seg_9847" s="T368">вот</ta>
            <ta e="T370" id="Seg_9848" s="T369">тот</ta>
            <ta e="T371" id="Seg_9849" s="T370">кто-VBZ-PRS-1SG</ta>
            <ta e="T372" id="Seg_9850" s="T371">эй</ta>
            <ta e="T375" id="Seg_9851" s="T374">пулемет-EP-INSTR</ta>
            <ta e="T376" id="Seg_9852" s="T375">кто-VBZ-FUT.[3SG]</ta>
            <ta e="T377" id="Seg_9853" s="T376">сразу</ta>
            <ta e="T378" id="Seg_9854" s="T377">двадцать-ABL</ta>
            <ta e="T379" id="Seg_9855" s="T378">больше</ta>
            <ta e="T380" id="Seg_9856" s="T379">человек-ACC</ta>
            <ta e="T381" id="Seg_9857" s="T380">падать-EP-CAUS-PST1-3PL</ta>
            <ta e="T382" id="Seg_9858" s="T381">некоторый-PL-1PL.[NOM]</ta>
            <ta e="T383" id="Seg_9859" s="T382">назад</ta>
            <ta e="T384" id="Seg_9860" s="T383">бегать-PST1-3PL</ta>
            <ta e="T385" id="Seg_9861" s="T384">тот</ta>
            <ta e="T386" id="Seg_9862" s="T385">дозор.[NOM]</ta>
            <ta e="T387" id="Seg_9863" s="T386">быть-HAB.[3SG]</ta>
            <ta e="T390" id="Seg_9864" s="T389">рота-2SG.[NOM]</ta>
            <ta e="T391" id="Seg_9865" s="T390">идти-FUT-3SG</ta>
            <ta e="T392" id="Seg_9866" s="T391">еще</ta>
            <ta e="T393" id="Seg_9867" s="T392">кто.[NOM]</ta>
            <ta e="T394" id="Seg_9868" s="T393">впереди</ta>
            <ta e="T395" id="Seg_9869" s="T394">идти-FUT-3SG</ta>
            <ta e="T396" id="Seg_9870" s="T395">разведчик.[NOM]</ta>
            <ta e="T397" id="Seg_9871" s="T396">подобно</ta>
            <ta e="T398" id="Seg_9872" s="T397">рота-2SG-ACC</ta>
            <ta e="T399" id="Seg_9873" s="T398">только</ta>
            <ta e="T400" id="Seg_9874" s="T399">охранять-PRS-2SG</ta>
            <ta e="T401" id="Seg_9875" s="T400">EMPH</ta>
            <ta e="T402" id="Seg_9876" s="T401">передний-PL.[NOM]</ta>
            <ta e="T403" id="Seg_9877" s="T402">два</ta>
            <ta e="T404" id="Seg_9878" s="T403">человек.[NOM]</ta>
            <ta e="T405" id="Seg_9879" s="T404">быть-PRS.[3SG]</ta>
            <ta e="T406" id="Seg_9880" s="T405">левый</ta>
            <ta e="T407" id="Seg_9881" s="T406">к</ta>
            <ta e="T408" id="Seg_9882" s="T407">правый</ta>
            <ta e="T409" id="Seg_9883" s="T408">к</ta>
            <ta e="T410" id="Seg_9884" s="T409">опять</ta>
            <ta e="T411" id="Seg_9885" s="T410">два-DISTR</ta>
            <ta e="T412" id="Seg_9886" s="T411">человек.[NOM]</ta>
            <ta e="T413" id="Seg_9887" s="T412">вот</ta>
            <ta e="T414" id="Seg_9888" s="T413">куда</ta>
            <ta e="T415" id="Seg_9889" s="T414">быть-FUT-1PL=Q</ta>
            <ta e="T416" id="Seg_9890" s="T415">говорить-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_9891" s="T416">этот</ta>
            <ta e="T418" id="Seg_9892" s="T417">деревня-DAT/LOC</ta>
            <ta e="T419" id="Seg_9893" s="T418">проверять-VBZ-PRS-1PL</ta>
            <ta e="T420" id="Seg_9894" s="T419">вправду</ta>
            <ta e="T421" id="Seg_9895" s="T420">кто-VBZ</ta>
            <ta e="T422" id="Seg_9896" s="T421">кто.[NOM]</ta>
            <ta e="T423" id="Seg_9897" s="T422">NEG</ta>
            <ta e="T424" id="Seg_9898" s="T423">NEG.EX</ta>
            <ta e="T426" id="Seg_9899" s="T425">ээ</ta>
            <ta e="T427" id="Seg_9900" s="T426">немец-EP-PL.[NOM]</ta>
            <ta e="T428" id="Seg_9901" s="T427">на.том.берегу</ta>
            <ta e="T429" id="Seg_9902" s="T428">только</ta>
            <ta e="T430" id="Seg_9903" s="T429">быть.видно-PRS-3PL</ta>
            <ta e="T431" id="Seg_9904" s="T430">назад</ta>
            <ta e="T432" id="Seg_9905" s="T431">идти-IMP.1PL</ta>
            <ta e="T433" id="Seg_9906" s="T432">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T434" id="Seg_9907" s="T433">делать-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_9908" s="T434">вот</ta>
            <ta e="T436" id="Seg_9909" s="T435">тот</ta>
            <ta e="T437" id="Seg_9910" s="T436">кто-VBZ-CVB.SIM</ta>
            <ta e="T438" id="Seg_9911" s="T437">идти-CVB.SEQ-1PL</ta>
            <ta e="T439" id="Seg_9912" s="T438">встречаться-PST1-1PL</ta>
            <ta e="T440" id="Seg_9913" s="T439">тот</ta>
            <ta e="T441" id="Seg_9914" s="T440">тот</ta>
            <ta e="T442" id="Seg_9915" s="T441">река-1PL-ACC</ta>
            <ta e="T443" id="Seg_9916" s="T442">наперерез</ta>
            <ta e="T444" id="Seg_9917" s="T443">идти-PST1-1PL</ta>
            <ta e="T445" id="Seg_9918" s="T444">течение.[NOM]</ta>
            <ta e="T446" id="Seg_9919" s="T445">наперерез</ta>
            <ta e="T447" id="Seg_9920" s="T446">река-VBZ-CVB.SEQ</ta>
            <ta e="T448" id="Seg_9921" s="T447">э</ta>
            <ta e="T449" id="Seg_9922" s="T448">идти-CVB.SEQ-1PL</ta>
            <ta e="T450" id="Seg_9923" s="T449">ээ</ta>
            <ta e="T451" id="Seg_9924" s="T450">кто-VBZ-PRS-1PL</ta>
            <ta e="T452" id="Seg_9925" s="T451">так</ta>
            <ta e="T453" id="Seg_9926" s="T452">кто-3PL.[NOM]</ta>
            <ta e="T454" id="Seg_9927" s="T453">кто-VBZ-PST2-3PL</ta>
            <ta e="T455" id="Seg_9928" s="T454">этот</ta>
            <ta e="T456" id="Seg_9929" s="T455">артиллерия.[NOM]</ta>
            <ta e="T457" id="Seg_9930" s="T456">быть-HAB.[3SG]</ta>
            <ta e="T458" id="Seg_9931" s="T457">EMPH</ta>
            <ta e="T459" id="Seg_9932" s="T458">пушка-3PL.[NOM]</ta>
            <ta e="T462" id="Seg_9933" s="T461">э</ta>
            <ta e="T463" id="Seg_9934" s="T462">кто-3PL.[NOM]</ta>
            <ta e="T464" id="Seg_9935" s="T463">лошадь-PL.[NOM]</ta>
            <ta e="T465" id="Seg_9936" s="T464">полностью</ta>
            <ta e="T466" id="Seg_9937" s="T465">кто-VBZ-EP-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T467" id="Seg_9938" s="T466">так</ta>
            <ta e="T468" id="Seg_9939" s="T467">переворачивать-CVB.SIM</ta>
            <ta e="T469" id="Seg_9940" s="T468">кто-VBZ-PST2-3PL</ta>
            <ta e="T470" id="Seg_9941" s="T469">потом</ta>
            <ta e="T471" id="Seg_9942" s="T470">убить-ITER-PST2-3PL</ta>
            <ta e="T472" id="Seg_9943" s="T471">там</ta>
            <ta e="T473" id="Seg_9944" s="T472">один</ta>
            <ta e="T474" id="Seg_9945" s="T473">человек-ACC</ta>
            <ta e="T475" id="Seg_9946" s="T474">найти-PST1-1PL</ta>
            <ta e="T8" id="Seg_9947" s="T475">полуживой.[NOM]</ta>
            <ta e="T476" id="Seg_9948" s="T8">вправду</ta>
            <ta e="T477" id="Seg_9949" s="T476">куда</ta>
            <ta e="T478" id="Seg_9950" s="T477">INDEF</ta>
            <ta e="T479" id="Seg_9951" s="T478">еле</ta>
            <ta e="T480" id="Seg_9952" s="T479">говорить-PRS.[3SG]</ta>
            <ta e="T482" id="Seg_9953" s="T480">потом</ta>
            <ta e="T483" id="Seg_9954" s="T482">этот</ta>
            <ta e="T484" id="Seg_9955" s="T483">только.что</ta>
            <ta e="T485" id="Seg_9956" s="T484">табак.[NOM]</ta>
            <ta e="T486" id="Seg_9957" s="T485">тянуть-PTCP.PRS</ta>
            <ta e="T487" id="Seg_9958" s="T486">близость-3SG.[NOM]</ta>
            <ta e="T488" id="Seg_9959" s="T487">быть-PST1-3SG</ta>
            <ta e="T489" id="Seg_9960" s="T488">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T490" id="Seg_9961" s="T489">делать-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_9962" s="T490">1PL.[NOM]</ta>
            <ta e="T492" id="Seg_9963" s="T491">рота-1PL-ACC</ta>
            <ta e="T493" id="Seg_9964" s="T492">кто-VBZ-PST2-3PL</ta>
            <ta e="T494" id="Seg_9965" s="T493">говорить-PRS.[3SG]</ta>
            <ta e="T495" id="Seg_9966" s="T494">немец-3SG.[NOM]</ta>
            <ta e="T496" id="Seg_9967" s="T495">AFFIRM</ta>
            <ta e="T497" id="Seg_9968" s="T496">идти-PST2-3PL</ta>
            <ta e="T498" id="Seg_9969" s="T497">теперь-INTNS</ta>
            <ta e="T499" id="Seg_9970" s="T498">говорить-PRS.[3SG]</ta>
            <ta e="T500" id="Seg_9971" s="T499">говорить-RECP/COLL-EP-NEG-IMP.2PL</ta>
            <ta e="T501" id="Seg_9972" s="T500">громкий-ADVZ</ta>
            <ta e="T502" id="Seg_9973" s="T501">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T503" id="Seg_9974" s="T502">делать-PRS.[3SG]</ta>
            <ta e="T504" id="Seg_9975" s="T503">вот</ta>
            <ta e="T505" id="Seg_9976" s="T504">тот</ta>
            <ta e="T507" id="Seg_9977" s="T505">кто-VBZ-PST1-1PL</ta>
            <ta e="T508" id="Seg_9978" s="T507">вот</ta>
            <ta e="T509" id="Seg_9979" s="T508">там</ta>
            <ta e="T510" id="Seg_9980" s="T509">EMPH</ta>
            <ta e="T511" id="Seg_9981" s="T510">ээ</ta>
            <ta e="T512" id="Seg_9982" s="T511">друг</ta>
            <ta e="T513" id="Seg_9983" s="T512">два</ta>
            <ta e="T514" id="Seg_9984" s="T513">человек-ACC</ta>
            <ta e="T515" id="Seg_9985" s="T514">послать-PRS-3PL</ta>
            <ta e="T516" id="Seg_9986" s="T515">там</ta>
            <ta e="T517" id="Seg_9987" s="T516">кто.[NOM]</ta>
            <ta e="T518" id="Seg_9988" s="T517">быть-HAB.[3SG]</ta>
            <ta e="T519" id="Seg_9989" s="T518">санчасть.[NOM]</ta>
            <ta e="T520" id="Seg_9990" s="T519">потом</ta>
            <ta e="T521" id="Seg_9991" s="T520">уносить-FUT-1PL</ta>
            <ta e="T522" id="Seg_9992" s="T521">говорить-PRS-3PL</ta>
            <ta e="T523" id="Seg_9993" s="T522">1SG.[NOM]</ta>
            <ta e="T524" id="Seg_9994" s="T523">1PL.[NOM]</ta>
            <ta e="T525" id="Seg_9995" s="T524">2SG-ACC</ta>
            <ta e="T528" id="Seg_9996" s="T527">1SG-ACC</ta>
            <ta e="T529" id="Seg_9997" s="T528">что.[NOM]</ta>
            <ta e="T530" id="Seg_9998" s="T529">заботить-EP-NEG-IMP.2PL</ta>
            <ta e="T531" id="Seg_9999" s="T530">говорить-PRS.[3SG]</ta>
            <ta e="T532" id="Seg_10000" s="T531">1SG.[NOM]</ta>
            <ta e="T533" id="Seg_10001" s="T532">э</ta>
            <ta e="T534" id="Seg_10002" s="T533">ведь</ta>
            <ta e="T535" id="Seg_10003" s="T534">человек.[NOM]</ta>
            <ta e="T536" id="Seg_10004" s="T535">быть-PTCP.FUT</ta>
            <ta e="T537" id="Seg_10005" s="T536">э</ta>
            <ta e="T538" id="Seg_10006" s="T537">человек.[NOM]</ta>
            <ta e="T539" id="Seg_10007" s="T538">быть-PST2.NEG-1SG</ta>
            <ta e="T540" id="Seg_10008" s="T539">говорить-PRS.[3SG]</ta>
            <ta e="T541" id="Seg_10009" s="T540">EMPH</ta>
            <ta e="T542" id="Seg_10010" s="T541">умирать-FUT</ta>
            <ta e="T543" id="Seg_10011" s="T542">умирать-FUT-1SG</ta>
            <ta e="T544" id="Seg_10012" s="T543">теперь</ta>
            <ta e="T545" id="Seg_10013" s="T544">человек-PL.[NOM]</ta>
            <ta e="T546" id="Seg_10014" s="T545">подстилка.[NOM]</ta>
            <ta e="T547" id="Seg_10015" s="T546">подобно</ta>
            <ta e="T548" id="Seg_10016" s="T547">лежать-PRS-3PL</ta>
            <ta e="T549" id="Seg_10017" s="T548">вправду</ta>
            <ta e="T550" id="Seg_10018" s="T549">тот-PL-ABL</ta>
            <ta e="T551" id="Seg_10019" s="T550">EMPH-вот</ta>
            <ta e="T552" id="Seg_10020" s="T551">тот</ta>
            <ta e="T553" id="Seg_10021" s="T552">кто-VBZ-PST1-3PL</ta>
            <ta e="T554" id="Seg_10022" s="T553">тот-3SG-2SG-ACC</ta>
            <ta e="T555" id="Seg_10023" s="T554">носить-PST1-3PL</ta>
            <ta e="T556" id="Seg_10024" s="T555">два</ta>
            <ta e="T557" id="Seg_10025" s="T556">человек.[NOM]</ta>
            <ta e="T558" id="Seg_10026" s="T557">кто-DAT/LOC</ta>
            <ta e="T559" id="Seg_10027" s="T558">тот-3SG-2SG.[NOM]</ta>
            <ta e="T560" id="Seg_10028" s="T559">потом</ta>
            <ta e="T561" id="Seg_10029" s="T560">говорить-PST2.[3SG]</ta>
            <ta e="T562" id="Seg_10030" s="T561">говорят</ta>
            <ta e="T563" id="Seg_10031" s="T562">тот</ta>
            <ta e="T565" id="Seg_10032" s="T563">все.равно</ta>
            <ta e="T566" id="Seg_10033" s="T565">доводить-EP-NEG.[IMP.2SG]</ta>
            <ta e="T567" id="Seg_10034" s="T566">э</ta>
            <ta e="T568" id="Seg_10035" s="T567">кто-VBZ-CVB.SEQ</ta>
            <ta e="T569" id="Seg_10036" s="T568">тот</ta>
            <ta e="T570" id="Seg_10037" s="T569">потом</ta>
            <ta e="T571" id="Seg_10038" s="T570">назад</ta>
            <ta e="T572" id="Seg_10039" s="T571">приходить-PST1-3PL</ta>
            <ta e="T573" id="Seg_10040" s="T572">тот-PL-EP-2SG.[NOM]</ta>
            <ta e="T574" id="Seg_10041" s="T573">в.любом.случае</ta>
            <ta e="T575" id="Seg_10042" s="T574">человек.[NOM]</ta>
            <ta e="T576" id="Seg_10043" s="T575">быть-FUT-1SG</ta>
            <ta e="T577" id="Seg_10044" s="T576">NEG-3SG</ta>
            <ta e="T578" id="Seg_10045" s="T577">что-ACC</ta>
            <ta e="T579" id="Seg_10046" s="T578">1SG-ACC</ta>
            <ta e="T580" id="Seg_10047" s="T579">с</ta>
            <ta e="T583" id="Seg_10048" s="T582">кто-VBZ-2PL</ta>
            <ta e="T584" id="Seg_10049" s="T583">говорить-PRS.[3SG]</ta>
            <ta e="T585" id="Seg_10050" s="T584">говорят</ta>
            <ta e="T586" id="Seg_10051" s="T585">утром</ta>
            <ta e="T587" id="Seg_10052" s="T586">заря-1PL.[NOM]</ta>
            <ta e="T588" id="Seg_10053" s="T587">только.что</ta>
            <ta e="T589" id="Seg_10054" s="T588">выйти-PRS.[3SG]</ta>
            <ta e="T590" id="Seg_10055" s="T589">выйти-CVB.SEQ</ta>
            <ta e="T593" id="Seg_10056" s="T592">ээ</ta>
            <ta e="T594" id="Seg_10057" s="T593">кто-VBZ-CVB.SEQ</ta>
            <ta e="T595" id="Seg_10058" s="T594">быть-TEMP-3SG</ta>
            <ta e="T596" id="Seg_10059" s="T595">что.[NOM]</ta>
            <ta e="T597" id="Seg_10060" s="T596">INDEF</ta>
            <ta e="T598" id="Seg_10061" s="T597">лейтенант-ACC</ta>
            <ta e="T599" id="Seg_10062" s="T598">встречать-PST1-1PL</ta>
            <ta e="T600" id="Seg_10063" s="T599">тот-3SG-1PL.[NOM]</ta>
            <ta e="T601" id="Seg_10064" s="T600">говорить-PRS.[3SG]</ta>
            <ta e="T602" id="Seg_10065" s="T601">какой</ta>
            <ta e="T603" id="Seg_10066" s="T602">часть-ABL-2SG=Q</ta>
            <ta e="T604" id="Seg_10067" s="T603">2PL.[NOM]</ta>
            <ta e="T605" id="Seg_10068" s="T604">2PL-ABL</ta>
            <ta e="T606" id="Seg_10069" s="T605">говорить-PRS-1PL</ta>
            <ta e="T607" id="Seg_10070" s="T606">говорить-PRS.[3SG]</ta>
            <ta e="T608" id="Seg_10071" s="T607">потом</ta>
            <ta e="T609" id="Seg_10072" s="T608">один-1PL.[NOM]</ta>
            <ta e="T610" id="Seg_10073" s="T609">потом</ta>
            <ta e="T611" id="Seg_10074" s="T610">тогда</ta>
            <ta e="T612" id="Seg_10075" s="T611">1SG-ACC</ta>
            <ta e="T613" id="Seg_10076" s="T612">с</ta>
            <ta e="T614" id="Seg_10077" s="T613">бегать-EP-IMP.2PL</ta>
            <ta e="T615" id="Seg_10078" s="T614">говорить-PRS.[3SG]</ta>
            <ta e="T616" id="Seg_10079" s="T615">передняя.часть.[NOM]</ta>
            <ta e="T617" id="Seg_10080" s="T616">к</ta>
            <ta e="T619" id="Seg_10081" s="T618">только</ta>
            <ta e="T622" id="Seg_10082" s="T619">кто-VBZ-NEG-PTCP.FUT-DAT/LOC</ta>
            <ta e="T623" id="Seg_10083" s="T622">связь.[NOM]</ta>
            <ta e="T624" id="Seg_10084" s="T623">взять-CVB.SIM</ta>
            <ta e="T625" id="Seg_10085" s="T624">кто-VBZ-PRS.[3SG]</ta>
            <ta e="T626" id="Seg_10086" s="T625">очевидно</ta>
            <ta e="T627" id="Seg_10087" s="T626">вот</ta>
            <ta e="T628" id="Seg_10088" s="T627">потом</ta>
            <ta e="T629" id="Seg_10089" s="T628">тот</ta>
            <ta e="T630" id="Seg_10090" s="T629">кто-VBZ-CVB.SIM</ta>
            <ta e="T631" id="Seg_10091" s="T630">пытаться-PRS-1PL</ta>
            <ta e="T632" id="Seg_10092" s="T631">сколько</ta>
            <ta e="T633" id="Seg_10093" s="T632">INDEF</ta>
            <ta e="T634" id="Seg_10094" s="T633">день-ACC</ta>
            <ta e="T635" id="Seg_10095" s="T634">тот</ta>
            <ta e="T636" id="Seg_10096" s="T635">идти-PST1-1PL</ta>
            <ta e="T637" id="Seg_10097" s="T636">тот.[NOM]</ta>
            <ta e="T638" id="Seg_10098" s="T637">как</ta>
            <ta e="T639" id="Seg_10099" s="T638">рота-1PL-ACC</ta>
            <ta e="T640" id="Seg_10100" s="T639">NEG</ta>
            <ta e="T641" id="Seg_10101" s="T640">найти-NEG-1PL</ta>
            <ta e="T642" id="Seg_10102" s="T641">вот</ta>
            <ta e="T643" id="Seg_10103" s="T642">потом</ta>
            <ta e="T644" id="Seg_10104" s="T643">поздний-ADJZ-3SG-ACC</ta>
            <ta e="T645" id="Seg_10105" s="T644">кто-VBZ-PST1</ta>
            <ta e="T646" id="Seg_10106" s="T645">ээ</ta>
            <ta e="T647" id="Seg_10107" s="T646">найти-PST1-1PL</ta>
            <ta e="T648" id="Seg_10108" s="T647">опять</ta>
            <ta e="T649" id="Seg_10109" s="T648">AFFIRM</ta>
            <ta e="T652" id="Seg_10110" s="T651">три-ORD</ta>
            <ta e="T653" id="Seg_10111" s="T652">день-1PL-DAT/LOC</ta>
            <ta e="T654" id="Seg_10112" s="T653">Q</ta>
            <ta e="T655" id="Seg_10113" s="T654">сколько-ORD-1PL-DAT/LOC</ta>
            <ta e="T656" id="Seg_10114" s="T655">Q</ta>
            <ta e="T657" id="Seg_10115" s="T656">прочь</ta>
            <ta e="T658" id="Seg_10116" s="T657">идти-PRS-1PL</ta>
            <ta e="T659" id="Seg_10117" s="T658">туда</ta>
            <ta e="T660" id="Seg_10118" s="T659">кто-DAT/LOC</ta>
            <ta e="T661" id="Seg_10119" s="T660">тот</ta>
            <ta e="T662" id="Seg_10120" s="T661">река-1PL-ACC</ta>
            <ta e="T663" id="Seg_10121" s="T662">выйти-PRS-1PL</ta>
            <ta e="T664" id="Seg_10122" s="T663">вот</ta>
            <ta e="T665" id="Seg_10123" s="T664">потом</ta>
            <ta e="T666" id="Seg_10124" s="T665">тот</ta>
            <ta e="T667" id="Seg_10125" s="T666">встречаться-PRS-1PL</ta>
            <ta e="T668" id="Seg_10126" s="T667">тот</ta>
            <ta e="T669" id="Seg_10127" s="T668">далекий-ABL</ta>
            <ta e="T670" id="Seg_10128" s="T669">стрелять-FREQ-PRS-3PL</ta>
            <ta e="T671" id="Seg_10129" s="T670">а</ta>
            <ta e="T672" id="Seg_10130" s="T671">1PL.[NOM]</ta>
            <ta e="T673" id="Seg_10131" s="T672">та.сторона-ABL</ta>
            <ta e="T674" id="Seg_10132" s="T673">опять</ta>
            <ta e="T675" id="Seg_10133" s="T674">стрелять-FREQ-EP-RECP/COLL-PRS-1PL</ta>
            <ta e="T676" id="Seg_10134" s="T675">MOD</ta>
            <ta e="T677" id="Seg_10135" s="T676">вот</ta>
            <ta e="T678" id="Seg_10136" s="T677">тот</ta>
            <ta e="T679" id="Seg_10137" s="T678">кто-VBZ-PST1-1PL</ta>
            <ta e="T680" id="Seg_10138" s="T679">да</ta>
            <ta e="T681" id="Seg_10139" s="T680">там</ta>
            <ta e="T682" id="Seg_10140" s="T681">куда</ta>
            <ta e="T683" id="Seg_10141" s="T682">NEG</ta>
            <ta e="T684" id="Seg_10142" s="T683">кто-VBZ-NEG-3PL</ta>
            <ta e="T685" id="Seg_10143" s="T684">пустить-NEG-3PL</ta>
            <ta e="T686" id="Seg_10144" s="T685">тот</ta>
            <ta e="T687" id="Seg_10145" s="T686">утром</ta>
            <ta e="T688" id="Seg_10146" s="T687">способность-PROPR.[NOM]</ta>
            <ta e="T689" id="Seg_10147" s="T688">приходить-PST1-3PL</ta>
            <ta e="T690" id="Seg_10148" s="T689">тот.[NOM]</ta>
            <ta e="T691" id="Seg_10149" s="T690">к</ta>
            <ta e="T692" id="Seg_10150" s="T691">передняя.часть-1PL-ABL</ta>
            <ta e="T693" id="Seg_10151" s="T692">этот</ta>
            <ta e="T696" id="Seg_10152" s="T695">команда.[NOM]</ta>
            <ta e="T697" id="Seg_10153" s="T696">давать-PRS-3PL</ta>
            <ta e="T698" id="Seg_10154" s="T697">ну</ta>
            <ta e="T699" id="Seg_10155" s="T698">потом</ta>
            <ta e="T700" id="Seg_10156" s="T699">три</ta>
            <ta e="T701" id="Seg_10157" s="T700">час-DAT/LOC</ta>
            <ta e="T702" id="Seg_10158" s="T701">Брянск-ACC</ta>
            <ta e="T703" id="Seg_10159" s="T702">занять-VBZ-PTCP.FUT-2PL-ACC</ta>
            <ta e="T704" id="Seg_10160" s="T703">надо</ta>
            <ta e="T705" id="Seg_10161" s="T704">вот</ta>
            <ta e="T706" id="Seg_10162" s="T705">там</ta>
            <ta e="T707" id="Seg_10163" s="T706">так</ta>
            <ta e="T708" id="Seg_10164" s="T707">что.[NOM]</ta>
            <ta e="T709" id="Seg_10165" s="T708">INDEF</ta>
            <ta e="T710" id="Seg_10166" s="T709">гора.[NOM]</ta>
            <ta e="T711" id="Seg_10167" s="T710">быть-PRS.[3SG]</ta>
            <ta e="T712" id="Seg_10168" s="T711">узкая.полоса.[NOM]</ta>
            <ta e="T715" id="Seg_10169" s="T714">узкая.полоса-ACC</ta>
            <ta e="T716" id="Seg_10170" s="T715">э</ta>
            <ta e="T717" id="Seg_10171" s="T716">кто-VBZ-PST1-1PL</ta>
            <ta e="T718" id="Seg_10172" s="T717">да</ta>
            <ta e="T719" id="Seg_10173" s="T718">тоже</ta>
            <ta e="T720" id="Seg_10174" s="T719">опять</ta>
            <ta e="T721" id="Seg_10175" s="T720">лицо-3SG.[NOM]</ta>
            <ta e="T724" id="Seg_10176" s="T723">передняя.часть-EP-ABL</ta>
            <ta e="T725" id="Seg_10177" s="T724">кто-VBZ-PRS-3PL</ta>
            <ta e="T726" id="Seg_10178" s="T725">EMPH</ta>
            <ta e="T729" id="Seg_10179" s="T728">немец-PL.[NOM]</ta>
            <ta e="T730" id="Seg_10180" s="T729">так</ta>
            <ta e="T731" id="Seg_10181" s="T730">лежать-PRS-3PL</ta>
            <ta e="T733" id="Seg_10182" s="T732">делать-CVB.SIM</ta>
            <ta e="T734" id="Seg_10183" s="T733">лежать-PRS-3PL</ta>
            <ta e="T735" id="Seg_10184" s="T734">EMPH</ta>
            <ta e="T736" id="Seg_10185" s="T735">а</ta>
            <ta e="T737" id="Seg_10186" s="T736">1PL.[NOM]</ta>
            <ta e="T738" id="Seg_10187" s="T737">этот</ta>
            <ta e="T739" id="Seg_10188" s="T738">сторона-EP-ABL</ta>
            <ta e="T740" id="Seg_10189" s="T739">тоже</ta>
            <ta e="T741" id="Seg_10190" s="T740">стрелять-FREQ-EP-RECP/COLL-PRS-1PL</ta>
            <ta e="T742" id="Seg_10191" s="T741">вот</ta>
            <ta e="T743" id="Seg_10192" s="T742">потом</ta>
            <ta e="T744" id="Seg_10193" s="T743">этот</ta>
            <ta e="T745" id="Seg_10194" s="T744">ночь-ACC</ta>
            <ta e="T746" id="Seg_10195" s="T745">сквозь</ta>
            <ta e="T747" id="Seg_10196" s="T746">лежать-CVB.SIM</ta>
            <ta e="T748" id="Seg_10197" s="T747">здесь</ta>
            <ta e="T749" id="Seg_10198" s="T748">EMPH</ta>
            <ta e="T750" id="Seg_10199" s="T749">ночь-ACC</ta>
            <ta e="T751" id="Seg_10200" s="T750">десять-PL.[NOM]</ta>
            <ta e="T752" id="Seg_10201" s="T751">Q</ta>
            <ta e="T753" id="Seg_10202" s="T752">два-PL.[NOM]</ta>
            <ta e="T754" id="Seg_10203" s="T753">к</ta>
            <ta e="T755" id="Seg_10204" s="T754">Q</ta>
            <ta e="T756" id="Seg_10205" s="T755">или</ta>
            <ta e="T757" id="Seg_10206" s="T756">час-PL.[NOM]</ta>
            <ta e="T758" id="Seg_10207" s="T757">к</ta>
            <ta e="T759" id="Seg_10208" s="T758">Q</ta>
            <ta e="T760" id="Seg_10209" s="T759">тот</ta>
            <ta e="T761" id="Seg_10210" s="T760">ракета-ACC</ta>
            <ta e="T762" id="Seg_10211" s="T761">послать-PRS-3PL</ta>
            <ta e="T763" id="Seg_10212" s="T762">ракета-ACC</ta>
            <ta e="T764" id="Seg_10213" s="T763">послать-TEMP-3SG</ta>
            <ta e="T765" id="Seg_10214" s="T764">тогда</ta>
            <ta e="T766" id="Seg_10215" s="T765">этот</ta>
            <ta e="T767" id="Seg_10216" s="T766">полностью</ta>
            <ta e="T769" id="Seg_10217" s="T768">кто-VBZ-CVB.SIM</ta>
            <ta e="T770" id="Seg_10218" s="T769">лежать-PTCP.PRS-PL.[NOM]</ta>
            <ta e="T771" id="Seg_10219" s="T770">полностью</ta>
            <ta e="T772" id="Seg_10220" s="T771">быть.видно-PRS-3PL</ta>
            <ta e="T773" id="Seg_10221" s="T772">рука-1SG-ACC</ta>
            <ta e="T774" id="Seg_10222" s="T773">попадать.в-CAUS-CVB.SEQ</ta>
            <ta e="T775" id="Seg_10223" s="T774">этот</ta>
            <ta e="T776" id="Seg_10224" s="T775">замечать-NEG-PST1-1SG</ta>
            <ta e="T777" id="Seg_10225" s="T776">потом</ta>
            <ta e="T778" id="Seg_10226" s="T777">тот</ta>
            <ta e="T779" id="Seg_10227" s="T778">этот-3SG-1SG-DAT/LOC</ta>
            <ta e="T780" id="Seg_10228" s="T779">попадать.в</ta>
            <ta e="T781" id="Seg_10229" s="T780">кто-VBZ-PST2.[3SG]</ta>
            <ta e="T782" id="Seg_10230" s="T781">тот</ta>
            <ta e="T783" id="Seg_10231" s="T782">прямо</ta>
            <ta e="T784" id="Seg_10232" s="T783">летать-CVB.SEQ</ta>
            <ta e="T785" id="Seg_10233" s="T784">один</ta>
            <ta e="T786" id="Seg_10234" s="T785">мина.[NOM]</ta>
            <ta e="T787" id="Seg_10235" s="T786">приходить-PST1-3SG</ta>
            <ta e="T788" id="Seg_10236" s="T787">да</ta>
            <ta e="T789" id="Seg_10237" s="T788">каждый-1PL-ACC</ta>
            <ta e="T790" id="Seg_10238" s="T789">накрыть-PST2.[3SG]</ta>
            <ta e="T791" id="Seg_10239" s="T790">тот</ta>
            <ta e="T792" id="Seg_10240" s="T791">десять</ta>
            <ta e="T793" id="Seg_10241" s="T792">больше</ta>
            <ta e="T794" id="Seg_10242" s="T793">человек-ACC</ta>
            <ta e="T795" id="Seg_10243" s="T794">ружье-1SG-ACC</ta>
            <ta e="T796" id="Seg_10244" s="T795">так</ta>
            <ta e="T797" id="Seg_10245" s="T796">держать-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T798" id="Seg_10246" s="T797">ум-VBZ-PST1-1SG</ta>
            <ta e="T799" id="Seg_10247" s="T798">потом</ta>
            <ta e="T800" id="Seg_10248" s="T799">так</ta>
            <ta e="T801" id="Seg_10249" s="T800">так</ta>
            <ta e="T802" id="Seg_10250" s="T801">стоять-PRS-1SG</ta>
            <ta e="T803" id="Seg_10251" s="T802">что.[NOM]</ta>
            <ta e="T804" id="Seg_10252" s="T803">колено-SIM</ta>
            <ta e="T805" id="Seg_10253" s="T804">вот</ta>
            <ta e="T806" id="Seg_10254" s="T805">потом</ta>
            <ta e="T807" id="Seg_10255" s="T806">падать-PST1-1SG</ta>
            <ta e="T808" id="Seg_10256" s="T807">так</ta>
            <ta e="T809" id="Seg_10257" s="T808">вот</ta>
            <ta e="T810" id="Seg_10258" s="T809">потом</ta>
            <ta e="T811" id="Seg_10259" s="T810">позже</ta>
            <ta e="T812" id="Seg_10260" s="T811">видеть-PST1-1SG</ta>
            <ta e="T813" id="Seg_10261" s="T812">EMPH</ta>
            <ta e="T814" id="Seg_10262" s="T813">палец-PL-EP-1SG.[NOM]</ta>
            <ta e="T815" id="Seg_10263" s="T814">полностью</ta>
            <ta e="T816" id="Seg_10264" s="T815">дрожать-CVB.SIM</ta>
            <ta e="T817" id="Seg_10265" s="T816">идти-PRS-3PL</ta>
            <ta e="T818" id="Seg_10266" s="T817">кровь-EP-1SG.[NOM]</ta>
            <ta e="T819" id="Seg_10267" s="T818">течь-CVB.SIM</ta>
            <ta e="T820" id="Seg_10268" s="T819">лежать-PRS.[3SG]</ta>
            <ta e="T821" id="Seg_10269" s="T820">так</ta>
            <ta e="T822" id="Seg_10270" s="T821">вот</ta>
            <ta e="T823" id="Seg_10271" s="T822">потом</ta>
            <ta e="T824" id="Seg_10272" s="T823">пояс-1SG-ACC</ta>
            <ta e="T825" id="Seg_10273" s="T824">снимать-PST1-1SG</ta>
            <ta e="T826" id="Seg_10274" s="T825">так</ta>
            <ta e="T827" id="Seg_10275" s="T826">пояс-1SG-ACC</ta>
            <ta e="T828" id="Seg_10276" s="T827">снимать-CVB.SEQ</ta>
            <ta e="T829" id="Seg_10277" s="T828">идти-CVB.SEQ-1SG</ta>
            <ta e="T830" id="Seg_10278" s="T829">так</ta>
            <ta e="T831" id="Seg_10279" s="T830">кто-VBZ-PST1-1SG</ta>
            <ta e="T832" id="Seg_10280" s="T831">очевидно</ta>
            <ta e="T833" id="Seg_10281" s="T832">кто-1SG-DAT/LOC</ta>
            <ta e="T834" id="Seg_10282" s="T833">шея-1SG-DAT/LOC</ta>
            <ta e="T835" id="Seg_10283" s="T834">так</ta>
            <ta e="T838" id="Seg_10284" s="T837">кто-VBZ-PST1-1SG</ta>
            <ta e="T839" id="Seg_10285" s="T838">этот</ta>
            <ta e="T840" id="Seg_10286" s="T839">связывать-CVB.SEQ</ta>
            <ta e="T841" id="Seg_10287" s="T840">идти-CVB.SEQ-1SG</ta>
            <ta e="T842" id="Seg_10288" s="T841">потом</ta>
            <ta e="T843" id="Seg_10289" s="T842">этот</ta>
            <ta e="T844" id="Seg_10290" s="T843">пакет-PL.[NOM]</ta>
            <ta e="T845" id="Seg_10291" s="T844">быть-HAB-3PL</ta>
            <ta e="T848" id="Seg_10292" s="T847">вот</ta>
            <ta e="T849" id="Seg_10293" s="T848">тот-3SG-1SG-INSTR</ta>
            <ta e="T850" id="Seg_10294" s="T849">повернуть-PST1-1SG</ta>
            <ta e="T851" id="Seg_10295" s="T850">так</ta>
            <ta e="T852" id="Seg_10296" s="T851">потом</ta>
            <ta e="T853" id="Seg_10297" s="T852">обмотка.[NOM]</ta>
            <ta e="T854" id="Seg_10298" s="T853">тот.[NOM]</ta>
            <ta e="T855" id="Seg_10299" s="T854">ботинки-DAT/LOC</ta>
            <ta e="T856" id="Seg_10300" s="T855">кто.[NOM]</ta>
            <ta e="T857" id="Seg_10301" s="T856">быть-HAB.[3SG]</ta>
            <ta e="T860" id="Seg_10302" s="T859">тот-ACC</ta>
            <ta e="T861" id="Seg_10303" s="T860">обмотка-1SG-INSTR</ta>
            <ta e="T862" id="Seg_10304" s="T861">повернуть-PST1-1SG</ta>
            <ta e="T863" id="Seg_10305" s="T862">идти.[IMP.2SG]</ta>
            <ta e="T864" id="Seg_10306" s="T863">говорить-PRS-3PL</ta>
            <ta e="T865" id="Seg_10307" s="T864">кто.[NOM]</ta>
            <ta e="T866" id="Seg_10308" s="T865">командир.[NOM]</ta>
            <ta e="T867" id="Seg_10309" s="T866">рота-DAT/LOC</ta>
            <ta e="T868" id="Seg_10310" s="T867">тот.EMPH.[NOM]</ta>
            <ta e="T869" id="Seg_10311" s="T868">ползать-CVB.SEQ</ta>
            <ta e="T870" id="Seg_10312" s="T869">ползать-PTCP.PST-EP-INSTR</ta>
            <ta e="T871" id="Seg_10313" s="T870">доезжать-PST1-1SG</ta>
            <ta e="T872" id="Seg_10314" s="T871">тот-3SG-1SG-DAT/LOC</ta>
            <ta e="T873" id="Seg_10315" s="T872">хлеб.[NOM]</ta>
            <ta e="T874" id="Seg_10316" s="T873">искать-CVB.SEQ</ta>
            <ta e="T875" id="Seg_10317" s="T874">ээ</ta>
            <ta e="T878" id="Seg_10318" s="T877">пища.[NOM]</ta>
            <ta e="T879" id="Seg_10319" s="T878">эй</ta>
            <ta e="T880" id="Seg_10320" s="T879">говорить-PRS.[3SG]</ta>
            <ta e="T881" id="Seg_10321" s="T880">ранить-CVB.SIM</ta>
            <ta e="T882" id="Seg_10322" s="T881">делать</ta>
            <ta e="T883" id="Seg_10323" s="T930">Q</ta>
            <ta e="T885" id="Seg_10324" s="T883">говорить-PRS.[3SG]</ta>
            <ta e="T886" id="Seg_10325" s="T885">мм</ta>
            <ta e="T887" id="Seg_10326" s="T886">говорить-PRS-1SG</ta>
            <ta e="T888" id="Seg_10327" s="T887">ружье-1SG-ACC</ta>
            <ta e="T889" id="Seg_10328" s="T888">потом</ta>
            <ta e="T890" id="Seg_10329" s="T889">взять-PTCP.PST</ta>
            <ta e="T891" id="Seg_10330" s="T890">быть-PST1-3SG</ta>
            <ta e="T892" id="Seg_10331" s="T891">тот</ta>
            <ta e="T893" id="Seg_10332" s="T892">кто-EP-1SG.[NOM]</ta>
            <ta e="T894" id="Seg_10333" s="T893">командир-1SG-DAT/LOC</ta>
            <ta e="T895" id="Seg_10334" s="T894">сдавать-VBZ-PRS-1SG</ta>
            <ta e="T896" id="Seg_10335" s="T895">EMPH</ta>
            <ta e="T897" id="Seg_10336" s="T896">кто-DAT/LOC</ta>
            <ta e="T898" id="Seg_10337" s="T897">номер-EP-INSTR</ta>
            <ta e="T899" id="Seg_10338" s="T898">быть-PTCP.HAB</ta>
            <ta e="T900" id="Seg_10339" s="T899">быть-PST1-3PL</ta>
            <ta e="T901" id="Seg_10340" s="T900">тот-3SG-1SG.[NOM]</ta>
            <ta e="T902" id="Seg_10341" s="T901">EMPH</ta>
            <ta e="T903" id="Seg_10342" s="T902">кто.[NOM]</ta>
            <ta e="T904" id="Seg_10343" s="T903">патрон-PL.[NOM]</ta>
            <ta e="T905" id="Seg_10344" s="T904">карман-1SG-DAT/LOC</ta>
            <ta e="T906" id="Seg_10345" s="T905">что.[NOM]</ta>
            <ta e="T907" id="Seg_10346" s="T906">NEG</ta>
            <ta e="T908" id="Seg_10347" s="T907">NEG.EX</ta>
            <ta e="T909" id="Seg_10348" s="T908">граната-PL-1SG-ACC</ta>
            <ta e="T910" id="Seg_10349" s="T909">тоже</ta>
            <ta e="T911" id="Seg_10350" s="T910">взять-PST2-3PL</ta>
            <ta e="T912" id="Seg_10351" s="T911">окоп-DAT/LOC</ta>
            <ta e="T913" id="Seg_10352" s="T912">ложиться-TEMP-1SG</ta>
            <ta e="T914" id="Seg_10353" s="T913">песок.[NOM]</ta>
            <ta e="T915" id="Seg_10354" s="T914">падать-FREQ-PRS.[3SG]</ta>
            <ta e="T916" id="Seg_10355" s="T915">EMPH</ta>
            <ta e="T917" id="Seg_10356" s="T916">тот</ta>
            <ta e="T918" id="Seg_10357" s="T917">сторона-ABL</ta>
            <ta e="T919" id="Seg_10358" s="T918">приходить-CVB.SIM</ta>
            <ta e="T920" id="Seg_10359" s="T919">EMPH</ta>
            <ta e="T921" id="Seg_10360" s="T920">мина-PL.[NOM]</ta>
            <ta e="T922" id="Seg_10361" s="T921">падать-TEMP-3PL</ta>
            <ta e="T923" id="Seg_10362" s="T922">земля-EP-1SG.[NOM]</ta>
            <ta e="T924" id="Seg_10363" s="T923">дрожать-PRS.[3SG]</ta>
            <ta e="T929" id="Seg_10364" s="T928">кто-VBZ-PRS-3PL</ta>
            <ta e="T931" id="Seg_10365" s="T929">только</ta>
            <ta e="T932" id="Seg_10366" s="T931">тот-3SG-1SG.[NOM]</ta>
            <ta e="T933" id="Seg_10367" s="T932">этот</ta>
            <ta e="T934" id="Seg_10368" s="T933">смена.[NOM]</ta>
            <ta e="T935" id="Seg_10369" s="T937">заполнение.[NOM]</ta>
            <ta e="T936" id="Seg_10370" s="T935">приходить-PTCP.PRS</ta>
            <ta e="T938" id="Seg_10371" s="T936">быть-PST2.[3SG]</ta>
            <ta e="T939" id="Seg_10372" s="T938">1PL.[NOM]</ta>
            <ta e="T940" id="Seg_10373" s="T939">место-1PL-DAT/LOC</ta>
            <ta e="T941" id="Seg_10374" s="T940">еще</ta>
            <ta e="T942" id="Seg_10375" s="T941">следующий-PL.[NOM]</ta>
            <ta e="T943" id="Seg_10376" s="T942">приходить-PRS-3PL</ta>
            <ta e="T944" id="Seg_10377" s="T943">быть-PRS.[3SG]</ta>
            <ta e="T945" id="Seg_10378" s="T944">шум.[NOM]</ta>
            <ta e="T946" id="Seg_10379" s="T945">становиться-FREQ-PRS.[3SG]</ta>
            <ta e="T947" id="Seg_10380" s="T946">EMPH</ta>
            <ta e="T948" id="Seg_10381" s="T947">потом</ta>
            <ta e="T949" id="Seg_10382" s="T948">плакать-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T950" id="Seg_10383" s="T949">становиться-PRS-3PL</ta>
            <ta e="T951" id="Seg_10384" s="T950">только</ta>
            <ta e="T952" id="Seg_10385" s="T951">некоторый.[NOM]-некоторый.[NOM]</ta>
            <ta e="T953" id="Seg_10386" s="T952">бредить-PTCP.PRS</ta>
            <ta e="T954" id="Seg_10387" s="T953">быть-PRS.[3SG]</ta>
            <ta e="T955" id="Seg_10388" s="T954">бредить-PTCP.PRS</ta>
            <ta e="T956" id="Seg_10389" s="T955">быть-TEMP-3PL</ta>
            <ta e="T957" id="Seg_10390" s="T956">1SG.[NOM]</ta>
            <ta e="T958" id="Seg_10391" s="T957">тоже</ta>
            <ta e="T959" id="Seg_10392" s="T958">тоже</ta>
            <ta e="T960" id="Seg_10393" s="T959">бредить-PRS-1SG</ta>
            <ta e="T961" id="Seg_10394" s="T960">тоже</ta>
            <ta e="T962" id="Seg_10395" s="T961">потом</ta>
            <ta e="T963" id="Seg_10396" s="T962">опять</ta>
            <ta e="T964" id="Seg_10397" s="T963">затихать-NMNZ.[NOM]</ta>
            <ta e="T965" id="Seg_10398" s="T964">делать-PRS-1SG</ta>
            <ta e="T966" id="Seg_10399" s="T965">слушать-PRS-1SG</ta>
            <ta e="T967" id="Seg_10400" s="T966">санчасть-DAT/LOC</ta>
            <ta e="T968" id="Seg_10401" s="T967">приходить-PST2-EP-1SG</ta>
            <ta e="T971" id="Seg_10402" s="T970">полевой</ta>
            <ta e="T972" id="Seg_10403" s="T971">работа.[NOM]</ta>
            <ta e="T973" id="Seg_10404" s="T972">кто.[NOM]</ta>
            <ta e="T974" id="Seg_10405" s="T973">быть-HAB.[3SG]</ta>
            <ta e="T975" id="Seg_10406" s="T974">санчасть.[NOM]</ta>
            <ta e="T976" id="Seg_10407" s="T975">тот</ta>
            <ta e="T977" id="Seg_10408" s="T976">санчасть-DAT/LOC</ta>
            <ta e="T978" id="Seg_10409" s="T977">приходить-PST1</ta>
            <ta e="T979" id="Seg_10410" s="T978">ээ</ta>
            <ta e="T980" id="Seg_10411" s="T979">приходить-PST1-1SG</ta>
            <ta e="T981" id="Seg_10412" s="T980">да</ta>
            <ta e="T982" id="Seg_10413" s="T981">тыл-DAT/LOC</ta>
            <ta e="T983" id="Seg_10414" s="T982">послать-PST2-3PL</ta>
            <ta e="T984" id="Seg_10415" s="T983">Иваново.Шуя.[NOM]</ta>
            <ta e="T985" id="Seg_10416" s="T984">лежать-PST2-EP-1SG</ta>
            <ta e="T986" id="Seg_10417" s="T985">старый</ta>
            <ta e="T987" id="Seg_10418" s="T986">госпиталь-DAT/LOC</ta>
            <ta e="T988" id="Seg_10419" s="T987">потом</ta>
            <ta e="T991" id="Seg_10420" s="T990">ээ</ta>
            <ta e="T992" id="Seg_10421" s="T991">сразу</ta>
            <ta e="T993" id="Seg_10422" s="T992">пять</ta>
            <ta e="T994" id="Seg_10423" s="T993">месяц-ACC</ta>
            <ta e="T995" id="Seg_10424" s="T994">лежать-PST1-1SG</ta>
            <ta e="T996" id="Seg_10425" s="T995">да</ta>
            <ta e="T997" id="Seg_10426" s="T996">дом-1SG.[NOM]</ta>
            <ta e="T998" id="Seg_10427" s="T997">к</ta>
            <ta e="T999" id="Seg_10428" s="T998">послать-PST2-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc-ChVD">
            <ta e="T1" id="Seg_10429" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_10430" s="T1">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T6" id="Seg_10431" s="T5">n</ta>
            <ta e="T7" id="Seg_10432" s="T6">n-n:case</ta>
            <ta e="T11" id="Seg_10433" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_10434" s="T11">v-v:cvb</ta>
            <ta e="T13" id="Seg_10435" s="T12">v-v:tense-v:poss.pn</ta>
            <ta e="T14" id="Seg_10436" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_10437" s="T14">propr-n:case</ta>
            <ta e="T16" id="Seg_10438" s="T15">v-v:tense-v:poss.pn</ta>
            <ta e="T17" id="Seg_10439" s="T16">propr-n:case</ta>
            <ta e="T18" id="Seg_10440" s="T17">que-pro:case</ta>
            <ta e="T19" id="Seg_10441" s="T18">interj</ta>
            <ta e="T9" id="Seg_10442" s="T19">adj</ta>
            <ta e="T20" id="Seg_10443" s="T9">n-n:case</ta>
            <ta e="T21" id="Seg_10444" s="T20">v-v:cvb</ta>
            <ta e="T22" id="Seg_10445" s="T21">v-v:tense-v:poss.pn</ta>
            <ta e="T23" id="Seg_10446" s="T22">n-n:(ins)-n:(num)-n:case</ta>
            <ta e="T24" id="Seg_10447" s="T23">post</ta>
            <ta e="T25" id="Seg_10448" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_10449" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_10450" s="T26">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T28" id="Seg_10451" s="T27">propr-n:case</ta>
            <ta e="T29" id="Seg_10452" s="T28">interj</ta>
            <ta e="T30" id="Seg_10453" s="T29">post</ta>
            <ta e="T31" id="Seg_10454" s="T30">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T32" id="Seg_10455" s="T31">propr</ta>
            <ta e="T33" id="Seg_10456" s="T32">n-n:poss-n:case</ta>
            <ta e="T34" id="Seg_10457" s="T33">adv</ta>
            <ta e="T35" id="Seg_10458" s="T34">v-v:tense-v:poss.pn</ta>
            <ta e="T36" id="Seg_10459" s="T35">propr</ta>
            <ta e="T37" id="Seg_10460" s="T36">n-n:case</ta>
            <ta e="T41" id="Seg_10461" s="T40">cardnum</ta>
            <ta e="T42" id="Seg_10462" s="T41">cardnum</ta>
            <ta e="T43" id="Seg_10463" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_10464" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_10465" s="T44">que-pro:case</ta>
            <ta e="T46" id="Seg_10466" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_10467" s="T46">v-v:cvb</ta>
            <ta e="T48" id="Seg_10468" s="T47">post</ta>
            <ta e="T49" id="Seg_10469" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_10470" s="T49">v-v:cvb</ta>
            <ta e="T51" id="Seg_10471" s="T54">que-pro:case</ta>
            <ta e="T52" id="Seg_10472" s="T51">propr-n:case</ta>
            <ta e="T56" id="Seg_10473" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_10474" s="T56">v-v&gt;v-v:ptcp</ta>
            <ta e="T58" id="Seg_10475" s="T57">v-v:tense-v:poss.pn</ta>
            <ta e="T60" id="Seg_10476" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_10477" s="T60">adv</ta>
            <ta e="T62" id="Seg_10478" s="T61">adj</ta>
            <ta e="T63" id="Seg_10479" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_10480" s="T63">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T65" id="Seg_10481" s="T64">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T66" id="Seg_10482" s="T65">post</ta>
            <ta e="T67" id="Seg_10483" s="T66">dempro-pro:case</ta>
            <ta e="T68" id="Seg_10484" s="T67">v-v:(ins)-v:(neg)-v:cvb-v:pred.pn</ta>
            <ta e="T69" id="Seg_10485" s="T68">adv</ta>
            <ta e="T70" id="Seg_10486" s="T69">v-v&gt;v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T71" id="Seg_10487" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_10488" s="T71">dempro</ta>
            <ta e="T73" id="Seg_10489" s="T72">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T74" id="Seg_10490" s="T73">que-pro:case</ta>
            <ta e="T75" id="Seg_10491" s="T74">propr-n:case</ta>
            <ta e="T76" id="Seg_10492" s="T75">v-v:cvb</ta>
            <ta e="T77" id="Seg_10493" s="T76">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T481" id="Seg_10494" s="T80">adv</ta>
            <ta e="T81" id="Seg_10495" s="T481">que-que&gt;v-v:ptcp</ta>
            <ta e="T82" id="Seg_10496" s="T81">v-v:tense-v:poss.pn</ta>
            <ta e="T83" id="Seg_10497" s="T82">adv</ta>
            <ta e="T84" id="Seg_10498" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_10499" s="T84">v-v:ptcp</ta>
            <ta e="T86" id="Seg_10500" s="T85">v-v:tense-v:poss.pn</ta>
            <ta e="T92" id="Seg_10501" s="T91">que-pro:case</ta>
            <ta e="T93" id="Seg_10502" s="T92">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T94" id="Seg_10503" s="T93">que-que&gt;adj</ta>
            <ta e="T95" id="Seg_10504" s="T94">que-pro:case</ta>
            <ta e="T96" id="Seg_10505" s="T95">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T97" id="Seg_10506" s="T96">adv</ta>
            <ta e="T101" id="Seg_10507" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_10508" s="T101">adv</ta>
            <ta e="T103" id="Seg_10509" s="T102">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T104" id="Seg_10510" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_10511" s="T104">propr-n:case</ta>
            <ta e="T106" id="Seg_10512" s="T105">que-pro:case</ta>
            <ta e="T107" id="Seg_10513" s="T106">v-v:tense-v:poss.pn</ta>
            <ta e="T108" id="Seg_10514" s="T107">dempro</ta>
            <ta e="T109" id="Seg_10515" s="T108">propr-n:case</ta>
            <ta e="T110" id="Seg_10516" s="T109">dempro</ta>
            <ta e="T111" id="Seg_10517" s="T110">propr-n:case</ta>
            <ta e="T115" id="Seg_10518" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_10519" s="T115">cardnum</ta>
            <ta e="T117" id="Seg_10520" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_10521" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_10522" s="T118">que-pro:case</ta>
            <ta e="T120" id="Seg_10523" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_10524" s="T120">v-v:cvb</ta>
            <ta e="T122" id="Seg_10525" s="T121">post</ta>
            <ta e="T123" id="Seg_10526" s="T122">adj</ta>
            <ta e="T124" id="Seg_10527" s="T123">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T125" id="Seg_10528" s="T124">adv</ta>
            <ta e="T126" id="Seg_10529" s="T125">v-v:tense-v:poss.pn</ta>
            <ta e="T127" id="Seg_10530" s="T126">v-v:tense-v:poss.pn</ta>
            <ta e="T128" id="Seg_10531" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_10532" s="T128">adv</ta>
            <ta e="T130" id="Seg_10533" s="T129">que-que&gt;v-v:cvb</ta>
            <ta e="T131" id="Seg_10534" s="T130">v-v:mood-v:temp.pn</ta>
            <ta e="T132" id="Seg_10535" s="T131">ptcl</ta>
            <ta e="T134" id="Seg_10536" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_10537" s="T134">adv</ta>
            <ta e="T136" id="Seg_10538" s="T135">v-v:cvb</ta>
            <ta e="T139" id="Seg_10539" s="T138">v-v:ptcp</ta>
            <ta e="T140" id="Seg_10540" s="T139">v-v:tense-v:poss.pn</ta>
            <ta e="T141" id="Seg_10541" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_10542" s="T141">adj</ta>
            <ta e="T143" id="Seg_10543" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_10544" s="T143">adv</ta>
            <ta e="T145" id="Seg_10545" s="T144">adj</ta>
            <ta e="T146" id="Seg_10546" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_10547" s="T146">v-v:tense-v:poss.pn</ta>
            <ta e="T148" id="Seg_10548" s="T147">interj</ta>
            <ta e="T151" id="Seg_10549" s="T150">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T152" id="Seg_10550" s="T151">adv</ta>
            <ta e="T153" id="Seg_10551" s="T152">adv</ta>
            <ta e="T154" id="Seg_10552" s="T153">adv</ta>
            <ta e="T155" id="Seg_10553" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_10554" s="T155">dempro</ta>
            <ta e="T157" id="Seg_10555" s="T156">que-pro:case</ta>
            <ta e="T158" id="Seg_10556" s="T157">que</ta>
            <ta e="T161" id="Seg_10557" s="T160">dempro-pro:case</ta>
            <ta e="T162" id="Seg_10558" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_10559" s="T162">v-v:ptcp</ta>
            <ta e="T164" id="Seg_10560" s="T163">n-n:(poss)-n:case</ta>
            <ta e="T165" id="Seg_10561" s="T164">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T166" id="Seg_10562" s="T165">post</ta>
            <ta e="T167" id="Seg_10563" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_10564" s="T167">adv</ta>
            <ta e="T169" id="Seg_10565" s="T168">que-que&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T170" id="Seg_10566" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_10567" s="T170">que-pro:case</ta>
            <ta e="T172" id="Seg_10568" s="T171">adj</ta>
            <ta e="T173" id="Seg_10569" s="T172">dempro</ta>
            <ta e="T174" id="Seg_10570" s="T173">n</ta>
            <ta e="T175" id="Seg_10571" s="T174">interj</ta>
            <ta e="T176" id="Seg_10572" s="T175">que-pro:case</ta>
            <ta e="T177" id="Seg_10573" s="T176">propr</ta>
            <ta e="T178" id="Seg_10574" s="T177">propr</ta>
            <ta e="T179" id="Seg_10575" s="T178">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_10576" s="T179">dempro-pro:case</ta>
            <ta e="T181" id="Seg_10577" s="T180">conj</ta>
            <ta e="T182" id="Seg_10578" s="T181">dempro</ta>
            <ta e="T183" id="Seg_10579" s="T182">post</ta>
            <ta e="T184" id="Seg_10580" s="T183">que-que&gt;v</ta>
            <ta e="T185" id="Seg_10581" s="T184">que-que&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T186" id="Seg_10582" s="T185">n-n:case</ta>
            <ta e="T187" id="Seg_10583" s="T186">que</ta>
            <ta e="T188" id="Seg_10584" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_10585" s="T188">n-n&gt;n-n:case</ta>
            <ta e="T190" id="Seg_10586" s="T189">v-v:tense-v:pred.pn</ta>
            <ta e="T191" id="Seg_10587" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_10588" s="T191">adv</ta>
            <ta e="T193" id="Seg_10589" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_10590" s="T193">adv</ta>
            <ta e="T196" id="Seg_10591" s="T195">v-v:tense-v:pred.pn</ta>
            <ta e="T197" id="Seg_10592" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_10593" s="T197">adv</ta>
            <ta e="T199" id="Seg_10594" s="T198">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T200" id="Seg_10595" s="T199">v-v:cvb</ta>
            <ta e="T201" id="Seg_10596" s="T200">v-v:cvb</ta>
            <ta e="T202" id="Seg_10597" s="T201">v-v:cvb</ta>
            <ta e="T203" id="Seg_10598" s="T202">v-v:cvb-v:pred.pn</ta>
            <ta e="T204" id="Seg_10599" s="T203">dempro-pro:case</ta>
            <ta e="T205" id="Seg_10600" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_10601" s="T205">v-v&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_10602" s="T206">n-n:poss-n:case</ta>
            <ta e="T208" id="Seg_10603" s="T207">v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_10604" s="T208">que</ta>
            <ta e="T212" id="Seg_10605" s="T211">que-que&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T214" id="Seg_10606" s="T212">adv</ta>
            <ta e="T215" id="Seg_10607" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_10608" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_10609" s="T216">v-v:mood-v:temp.pn</ta>
            <ta e="T218" id="Seg_10610" s="T217">adv</ta>
            <ta e="T219" id="Seg_10611" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_10612" s="T219">v-v:mood-v:temp.pn</ta>
            <ta e="T221" id="Seg_10613" s="T220">ptcl</ta>
            <ta e="T223" id="Seg_10614" s="T222">que-pro:case</ta>
            <ta e="T224" id="Seg_10615" s="T223">que-que&gt;v-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T225" id="Seg_10616" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_10617" s="T225">v-v:tense-v:poss.pn</ta>
            <ta e="T227" id="Seg_10618" s="T226">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T228" id="Seg_10619" s="T227">v-v:(ins)-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T229" id="Seg_10620" s="T228">adv</ta>
            <ta e="T230" id="Seg_10621" s="T229">adv</ta>
            <ta e="T233" id="Seg_10622" s="T232">v-v&gt;v-v:(neg)-v:mood.pn</ta>
            <ta e="T234" id="Seg_10623" s="T233">n</ta>
            <ta e="T235" id="Seg_10624" s="T234">interj</ta>
            <ta e="T236" id="Seg_10625" s="T235">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T237" id="Seg_10626" s="T236">v-v:tense-v:pred.pn</ta>
            <ta e="T238" id="Seg_10627" s="T237">ptcl</ta>
            <ta e="T241" id="Seg_10628" s="T240">que-que&gt;v-v:mood.pn</ta>
            <ta e="T244" id="Seg_10629" s="T243">interj</ta>
            <ta e="T245" id="Seg_10630" s="T244">v-v:(ins)-v:mood.pn</ta>
            <ta e="T246" id="Seg_10631" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_10632" s="T246">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T248" id="Seg_10633" s="T247">v-v:mood.pn</ta>
            <ta e="T249" id="Seg_10634" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_10635" s="T249">v-v:mood-v:pred.pn</ta>
            <ta e="T251" id="Seg_10636" s="T250">adv</ta>
            <ta e="T252" id="Seg_10637" s="T251">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T253" id="Seg_10638" s="T252">conj</ta>
            <ta e="T255" id="Seg_10639" s="T253">adv</ta>
            <ta e="T256" id="Seg_10640" s="T255">v-v&gt;v</ta>
            <ta e="T257" id="Seg_10641" s="T256">dempro-pro:case</ta>
            <ta e="T258" id="Seg_10642" s="T257">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_10643" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_10644" s="T259">dempro</ta>
            <ta e="T261" id="Seg_10645" s="T260">v-v:tense-v:pred.pn</ta>
            <ta e="T262" id="Seg_10646" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_10647" s="T262">dempro</ta>
            <ta e="T264" id="Seg_10648" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_10649" s="T264">n-n:(poss)-n:case</ta>
            <ta e="T266" id="Seg_10650" s="T265">v-v:cvb</ta>
            <ta e="T267" id="Seg_10651" s="T266">v-v:tense-v:pred.pn</ta>
            <ta e="T268" id="Seg_10652" s="T267">adv</ta>
            <ta e="T271" id="Seg_10653" s="T270">que-pro:(poss)-pro:case</ta>
            <ta e="T272" id="Seg_10654" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_10655" s="T272">v-v:cvb-v:pred.pn</ta>
            <ta e="T274" id="Seg_10656" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_10657" s="T274">n-n:(poss)-n:case</ta>
            <ta e="T276" id="Seg_10658" s="T275">conj</ta>
            <ta e="T277" id="Seg_10659" s="T276">que-pro:(poss)-pro:case</ta>
            <ta e="T278" id="Seg_10660" s="T277">conj</ta>
            <ta e="T279" id="Seg_10661" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_10662" s="T279">dempro</ta>
            <ta e="T281" id="Seg_10663" s="T280">pers-pro:case</ta>
            <ta e="T282" id="Seg_10664" s="T281">v-v:cvb-v:pred.pn</ta>
            <ta e="T283" id="Seg_10665" s="T282">v-v:tense-v:poss.pn</ta>
            <ta e="T284" id="Seg_10666" s="T283">cardnum</ta>
            <ta e="T285" id="Seg_10667" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_10668" s="T285">v-v:tense-v:pred.pn</ta>
            <ta e="T287" id="Seg_10669" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_10670" s="T287">adv</ta>
            <ta e="T289" id="Seg_10671" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_10672" s="T289">adv</ta>
            <ta e="T291" id="Seg_10673" s="T290">dempro</ta>
            <ta e="T292" id="Seg_10674" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_10675" s="T292">n-n:(ins)-n:(num)-n:(poss)-n:case</ta>
            <ta e="T294" id="Seg_10676" s="T293">v-v:tense-v:pred.pn</ta>
            <ta e="T295" id="Seg_10677" s="T294">n-n:case</ta>
            <ta e="T296" id="Seg_10678" s="T295">v-v:tense-v:pred.pn</ta>
            <ta e="T297" id="Seg_10679" s="T296">adv</ta>
            <ta e="T298" id="Seg_10680" s="T297">v-v:mood.pn</ta>
            <ta e="T299" id="Seg_10681" s="T298">v-v:cvb-v-v:cvb</ta>
            <ta e="T300" id="Seg_10682" s="T299">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T301" id="Seg_10683" s="T300">dempro</ta>
            <ta e="T302" id="Seg_10684" s="T301">n-n:(poss)-n:case</ta>
            <ta e="T303" id="Seg_10685" s="T302">n-n:(num)-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T304" id="Seg_10686" s="T303">dempro</ta>
            <ta e="T305" id="Seg_10687" s="T304">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T306" id="Seg_10688" s="T305">adj-n:(num)-n:case</ta>
            <ta e="T307" id="Seg_10689" s="T306">adv</ta>
            <ta e="T308" id="Seg_10690" s="T307">adj</ta>
            <ta e="T309" id="Seg_10691" s="T308">n-n:case</ta>
            <ta e="T310" id="Seg_10692" s="T309">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T313" id="Seg_10693" s="T312">n-n:(num)-n:poss-n:case</ta>
            <ta e="T314" id="Seg_10694" s="T313">dempro</ta>
            <ta e="T315" id="Seg_10695" s="T314">v-v:cvb</ta>
            <ta e="T316" id="Seg_10696" s="T315">v-v:tense-v:pred.pn</ta>
            <ta e="T317" id="Seg_10697" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_10698" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_10699" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_10700" s="T319">que-pro:case</ta>
            <ta e="T321" id="Seg_10701" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_10702" s="T321">v-v&gt;v-v:mood.pn</ta>
            <ta e="T323" id="Seg_10703" s="T322">v-v:tense-v:pred.pn</ta>
            <ta e="T327" id="Seg_10704" s="T326">v-v:tense-v:pred.pn</ta>
            <ta e="T328" id="Seg_10705" s="T327">n-n:(poss)-n:case</ta>
            <ta e="T329" id="Seg_10706" s="T328">adv</ta>
            <ta e="T330" id="Seg_10707" s="T329">v-v:tense-v:poss.pn</ta>
            <ta e="T331" id="Seg_10708" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_10709" s="T331">n-n:(ins)-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T333" id="Seg_10710" s="T332">dempro</ta>
            <ta e="T334" id="Seg_10711" s="T333">v-v:cvb</ta>
            <ta e="T335" id="Seg_10712" s="T334">v-v:tense-v:pred.pn</ta>
            <ta e="T336" id="Seg_10713" s="T335">que-pro:case</ta>
            <ta e="T337" id="Seg_10714" s="T336">que-pro:case</ta>
            <ta e="T338" id="Seg_10715" s="T337">v-v:tense-v:pred.pn</ta>
            <ta e="T339" id="Seg_10716" s="T338">v-v:tense-v:pred.pn</ta>
            <ta e="T340" id="Seg_10717" s="T339">adv</ta>
            <ta e="T341" id="Seg_10718" s="T340">v-v:tense-v:pred.pn</ta>
            <ta e="T342" id="Seg_10719" s="T341">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T343" id="Seg_10720" s="T342">v-v:ptcp</ta>
            <ta e="T344" id="Seg_10721" s="T343">n-n:poss-n:case</ta>
            <ta e="T345" id="Seg_10722" s="T344">post</ta>
            <ta e="T346" id="Seg_10723" s="T345">adv</ta>
            <ta e="T347" id="Seg_10724" s="T346">v-v:tense-v:pred.pn</ta>
            <ta e="T348" id="Seg_10725" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_10726" s="T348">adv</ta>
            <ta e="T350" id="Seg_10727" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_10728" s="T350">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T352" id="Seg_10729" s="T351">adv</ta>
            <ta e="T353" id="Seg_10730" s="T352">que-que&gt;v-v:cvb</ta>
            <ta e="T354" id="Seg_10731" s="T353">v-v:tense-v:pred.pn</ta>
            <ta e="T355" id="Seg_10732" s="T354">adv</ta>
            <ta e="T357" id="Seg_10733" s="T355">adv</ta>
            <ta e="T358" id="Seg_10734" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_10735" s="T358">v-v:cvb</ta>
            <ta e="T360" id="Seg_10736" s="T359">n-n:(ins)-n:case</ta>
            <ta e="T361" id="Seg_10737" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_10738" s="T361">v-v:tense-v:poss.pn</ta>
            <ta e="T363" id="Seg_10739" s="T362">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T364" id="Seg_10740" s="T363">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T365" id="Seg_10741" s="T364">n</ta>
            <ta e="T366" id="Seg_10742" s="T365">interj</ta>
            <ta e="T367" id="Seg_10743" s="T366">n-n:poss-n:case</ta>
            <ta e="T368" id="Seg_10744" s="T367">v-v:tense-v:pred.pn</ta>
            <ta e="T369" id="Seg_10745" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_10746" s="T369">dempro</ta>
            <ta e="T371" id="Seg_10747" s="T370">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T372" id="Seg_10748" s="T371">interj</ta>
            <ta e="T375" id="Seg_10749" s="T374">n-n:(ins)-n:case</ta>
            <ta e="T376" id="Seg_10750" s="T375">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T377" id="Seg_10751" s="T376">adv</ta>
            <ta e="T378" id="Seg_10752" s="T377">cardnum-n:case</ta>
            <ta e="T379" id="Seg_10753" s="T378">post</ta>
            <ta e="T380" id="Seg_10754" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_10755" s="T380">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T382" id="Seg_10756" s="T381">indfpro-pro:(num)-pro:(poss)-pro:case</ta>
            <ta e="T383" id="Seg_10757" s="T382">adv</ta>
            <ta e="T384" id="Seg_10758" s="T383">v-v:tense-v:pred.pn</ta>
            <ta e="T385" id="Seg_10759" s="T384">dempro</ta>
            <ta e="T386" id="Seg_10760" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_10761" s="T386">v-v:mood-v:pred.pn</ta>
            <ta e="T390" id="Seg_10762" s="T389">n-n:(poss)-n:case</ta>
            <ta e="T391" id="Seg_10763" s="T390">v-v:tense-v:poss.pn</ta>
            <ta e="T392" id="Seg_10764" s="T391">adv</ta>
            <ta e="T393" id="Seg_10765" s="T392">que-pro:case</ta>
            <ta e="T394" id="Seg_10766" s="T393">adv</ta>
            <ta e="T395" id="Seg_10767" s="T394">v-v:tense-v:poss.pn</ta>
            <ta e="T396" id="Seg_10768" s="T395">n-n:case</ta>
            <ta e="T397" id="Seg_10769" s="T396">post</ta>
            <ta e="T398" id="Seg_10770" s="T397">n-n:poss-n:case</ta>
            <ta e="T399" id="Seg_10771" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_10772" s="T399">v-v:tense-v:pred.pn</ta>
            <ta e="T401" id="Seg_10773" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_10774" s="T401">adj-n:(num)-n:case</ta>
            <ta e="T403" id="Seg_10775" s="T402">cardnum</ta>
            <ta e="T404" id="Seg_10776" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_10777" s="T404">v-v:tense-v:pred.pn</ta>
            <ta e="T406" id="Seg_10778" s="T405">adj</ta>
            <ta e="T407" id="Seg_10779" s="T406">post</ta>
            <ta e="T408" id="Seg_10780" s="T407">adj</ta>
            <ta e="T409" id="Seg_10781" s="T408">post</ta>
            <ta e="T410" id="Seg_10782" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_10783" s="T410">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T412" id="Seg_10784" s="T411">n-n:case</ta>
            <ta e="T413" id="Seg_10785" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_10786" s="T413">que</ta>
            <ta e="T415" id="Seg_10787" s="T414">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T416" id="Seg_10788" s="T415">v-v:tense-v:pred.pn</ta>
            <ta e="T417" id="Seg_10789" s="T416">dempro</ta>
            <ta e="T418" id="Seg_10790" s="T417">n-n:case</ta>
            <ta e="T419" id="Seg_10791" s="T418">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T420" id="Seg_10792" s="T419">adv</ta>
            <ta e="T421" id="Seg_10793" s="T420">que-que&gt;v</ta>
            <ta e="T422" id="Seg_10794" s="T421">que-pro:case</ta>
            <ta e="T423" id="Seg_10795" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_10796" s="T423">ptcl</ta>
            <ta e="T426" id="Seg_10797" s="T425">interj</ta>
            <ta e="T427" id="Seg_10798" s="T426">n-n:(ins)-n:(num)-n:case</ta>
            <ta e="T428" id="Seg_10799" s="T427">adv</ta>
            <ta e="T429" id="Seg_10800" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_10801" s="T429">v-v:tense-v:pred.pn</ta>
            <ta e="T431" id="Seg_10802" s="T430">adv</ta>
            <ta e="T432" id="Seg_10803" s="T431">v-v:mood.pn</ta>
            <ta e="T433" id="Seg_10804" s="T432">v-v:cvb-v-v:cvb</ta>
            <ta e="T434" id="Seg_10805" s="T433">v-v:tense-v:pred.pn</ta>
            <ta e="T435" id="Seg_10806" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_10807" s="T435">dempro</ta>
            <ta e="T437" id="Seg_10808" s="T436">que-que&gt;v-v:cvb</ta>
            <ta e="T438" id="Seg_10809" s="T437">v-v:cvb-v:pred.pn</ta>
            <ta e="T439" id="Seg_10810" s="T438">v-v:tense-v:poss.pn</ta>
            <ta e="T440" id="Seg_10811" s="T439">dempro</ta>
            <ta e="T441" id="Seg_10812" s="T440">dempro</ta>
            <ta e="T442" id="Seg_10813" s="T441">n-n:poss-n:case</ta>
            <ta e="T443" id="Seg_10814" s="T442">post</ta>
            <ta e="T444" id="Seg_10815" s="T443">v-v:tense-v:poss.pn</ta>
            <ta e="T445" id="Seg_10816" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_10817" s="T445">post</ta>
            <ta e="T447" id="Seg_10818" s="T446">n-n&gt;v-v:cvb</ta>
            <ta e="T448" id="Seg_10819" s="T447">interj</ta>
            <ta e="T449" id="Seg_10820" s="T448">v-v:cvb-v:pred.pn</ta>
            <ta e="T450" id="Seg_10821" s="T449">interj</ta>
            <ta e="T451" id="Seg_10822" s="T450">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T452" id="Seg_10823" s="T451">adv</ta>
            <ta e="T453" id="Seg_10824" s="T452">que-pro:(poss)-pro:case</ta>
            <ta e="T454" id="Seg_10825" s="T453">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T455" id="Seg_10826" s="T454">dempro</ta>
            <ta e="T456" id="Seg_10827" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_10828" s="T456">v-v:mood-v:pred.pn</ta>
            <ta e="T458" id="Seg_10829" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_10830" s="T458">n-n:(poss)-n:case</ta>
            <ta e="T462" id="Seg_10831" s="T461">interj</ta>
            <ta e="T463" id="Seg_10832" s="T462">que-pro:(poss)-pro:case</ta>
            <ta e="T464" id="Seg_10833" s="T463">n-n:(num)-n:case</ta>
            <ta e="T465" id="Seg_10834" s="T464">adv</ta>
            <ta e="T466" id="Seg_10835" s="T465">que-que&gt;v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T467" id="Seg_10836" s="T466">adv</ta>
            <ta e="T468" id="Seg_10837" s="T467">v-v:cvb</ta>
            <ta e="T469" id="Seg_10838" s="T468">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T470" id="Seg_10839" s="T469">adv</ta>
            <ta e="T471" id="Seg_10840" s="T470">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T472" id="Seg_10841" s="T471">adv</ta>
            <ta e="T473" id="Seg_10842" s="T472">cardnum</ta>
            <ta e="T474" id="Seg_10843" s="T473">n-n:case</ta>
            <ta e="T475" id="Seg_10844" s="T474">v-v:tense-v:poss.pn</ta>
            <ta e="T8" id="Seg_10845" s="T475">adj-n:case</ta>
            <ta e="T476" id="Seg_10846" s="T8">adv</ta>
            <ta e="T477" id="Seg_10847" s="T476">que</ta>
            <ta e="T478" id="Seg_10848" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_10849" s="T478">adv</ta>
            <ta e="T480" id="Seg_10850" s="T479">v-v:tense-v:pred.pn</ta>
            <ta e="T482" id="Seg_10851" s="T480">adv</ta>
            <ta e="T483" id="Seg_10852" s="T482">dempro</ta>
            <ta e="T484" id="Seg_10853" s="T483">adv</ta>
            <ta e="T485" id="Seg_10854" s="T484">n-n:case</ta>
            <ta e="T486" id="Seg_10855" s="T485">v-v:ptcp</ta>
            <ta e="T487" id="Seg_10856" s="T486">n-n:(poss)-n:case</ta>
            <ta e="T488" id="Seg_10857" s="T487">v-v:tense-v:poss.pn</ta>
            <ta e="T489" id="Seg_10858" s="T488">v-v:cvb-v-v:cvb</ta>
            <ta e="T490" id="Seg_10859" s="T489">v-v:tense-v:pred.pn</ta>
            <ta e="T491" id="Seg_10860" s="T490">pers-pro:case</ta>
            <ta e="T492" id="Seg_10861" s="T491">n-n:poss-n:case</ta>
            <ta e="T493" id="Seg_10862" s="T492">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T494" id="Seg_10863" s="T493">v-v:tense-v:pred.pn</ta>
            <ta e="T495" id="Seg_10864" s="T494">n-n:(poss)-n:case</ta>
            <ta e="T496" id="Seg_10865" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_10866" s="T496">v-v:tense-v:poss.pn</ta>
            <ta e="T498" id="Seg_10867" s="T497">adv-adv&gt;adv</ta>
            <ta e="T499" id="Seg_10868" s="T498">v-v:tense-v:pred.pn</ta>
            <ta e="T500" id="Seg_10869" s="T499">v-v&gt;v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T501" id="Seg_10870" s="T500">adj-adj&gt;adv</ta>
            <ta e="T502" id="Seg_10871" s="T501">v-v:cvb-v-v:cvb</ta>
            <ta e="T503" id="Seg_10872" s="T502">v-v:tense-v:pred.pn</ta>
            <ta e="T504" id="Seg_10873" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_10874" s="T504">dempro</ta>
            <ta e="T507" id="Seg_10875" s="T505">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T508" id="Seg_10876" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_10877" s="T508">adv</ta>
            <ta e="T510" id="Seg_10878" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_10879" s="T510">interj</ta>
            <ta e="T512" id="Seg_10880" s="T511">n</ta>
            <ta e="T513" id="Seg_10881" s="T512">cardnum</ta>
            <ta e="T514" id="Seg_10882" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_10883" s="T514">v-v:tense-v:pred.pn</ta>
            <ta e="T516" id="Seg_10884" s="T515">adv</ta>
            <ta e="T517" id="Seg_10885" s="T516">que-pro:case</ta>
            <ta e="T518" id="Seg_10886" s="T517">v-v:mood-v:pred.pn</ta>
            <ta e="T519" id="Seg_10887" s="T518">n-n:case</ta>
            <ta e="T520" id="Seg_10888" s="T519">adv</ta>
            <ta e="T521" id="Seg_10889" s="T520">v-v:tense-v:poss.pn</ta>
            <ta e="T522" id="Seg_10890" s="T521">v-v:tense-v:pred.pn</ta>
            <ta e="T523" id="Seg_10891" s="T522">pers-pro:case</ta>
            <ta e="T524" id="Seg_10892" s="T523">pers-pro:case</ta>
            <ta e="T525" id="Seg_10893" s="T524">pers-pro:case</ta>
            <ta e="T528" id="Seg_10894" s="T527">pers-pro:case</ta>
            <ta e="T529" id="Seg_10895" s="T528">que-pro:case</ta>
            <ta e="T530" id="Seg_10896" s="T529">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T531" id="Seg_10897" s="T530">v-v:tense-v:pred.pn</ta>
            <ta e="T532" id="Seg_10898" s="T531">pers-pro:case</ta>
            <ta e="T533" id="Seg_10899" s="T532">interj</ta>
            <ta e="T534" id="Seg_10900" s="T533">ptcl</ta>
            <ta e="T535" id="Seg_10901" s="T534">n-n:case</ta>
            <ta e="T536" id="Seg_10902" s="T535">v-v:ptcp</ta>
            <ta e="T537" id="Seg_10903" s="T536">interj</ta>
            <ta e="T538" id="Seg_10904" s="T537">n-n:case</ta>
            <ta e="T539" id="Seg_10905" s="T538">v-v:neg-v:pred.pn</ta>
            <ta e="T540" id="Seg_10906" s="T539">v-v:tense-v:pred.pn</ta>
            <ta e="T541" id="Seg_10907" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_10908" s="T541">v-v:tense</ta>
            <ta e="T543" id="Seg_10909" s="T542">v-v:tense-v:poss.pn</ta>
            <ta e="T544" id="Seg_10910" s="T543">adv</ta>
            <ta e="T545" id="Seg_10911" s="T544">n-n:(num)-n:case</ta>
            <ta e="T546" id="Seg_10912" s="T545">n-n:case</ta>
            <ta e="T547" id="Seg_10913" s="T546">post</ta>
            <ta e="T548" id="Seg_10914" s="T547">v-v:tense-v:pred.pn</ta>
            <ta e="T549" id="Seg_10915" s="T548">adv</ta>
            <ta e="T550" id="Seg_10916" s="T549">dempro-pro:(num)-pro:case</ta>
            <ta e="T551" id="Seg_10917" s="T550">adv&gt;adv-adv</ta>
            <ta e="T552" id="Seg_10918" s="T551">dempro</ta>
            <ta e="T553" id="Seg_10919" s="T552">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T554" id="Seg_10920" s="T553">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T555" id="Seg_10921" s="T554">v-v:tense-v:pred.pn</ta>
            <ta e="T556" id="Seg_10922" s="T555">cardnum</ta>
            <ta e="T557" id="Seg_10923" s="T556">n-n:case</ta>
            <ta e="T558" id="Seg_10924" s="T557">que-pro:case</ta>
            <ta e="T559" id="Seg_10925" s="T558">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T560" id="Seg_10926" s="T559">adv</ta>
            <ta e="T561" id="Seg_10927" s="T560">v-v:tense-v:pred.pn</ta>
            <ta e="T562" id="Seg_10928" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_10929" s="T562">dempro</ta>
            <ta e="T565" id="Seg_10930" s="T563">adv</ta>
            <ta e="T566" id="Seg_10931" s="T565">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T567" id="Seg_10932" s="T566">interj</ta>
            <ta e="T568" id="Seg_10933" s="T567">que-que&gt;v-v:cvb</ta>
            <ta e="T569" id="Seg_10934" s="T568">dempro</ta>
            <ta e="T570" id="Seg_10935" s="T569">adv</ta>
            <ta e="T571" id="Seg_10936" s="T570">adv</ta>
            <ta e="T572" id="Seg_10937" s="T571">v-v:tense-v:pred.pn</ta>
            <ta e="T573" id="Seg_10938" s="T572">dempro-pro:(num)-pro:(ins)-pro:(poss)-pro:case</ta>
            <ta e="T574" id="Seg_10939" s="T573">adv</ta>
            <ta e="T575" id="Seg_10940" s="T574">n-n:case</ta>
            <ta e="T576" id="Seg_10941" s="T575">v-v:tense-v:poss.pn</ta>
            <ta e="T577" id="Seg_10942" s="T576">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T578" id="Seg_10943" s="T577">que-pro:case</ta>
            <ta e="T579" id="Seg_10944" s="T578">pers-pro:case</ta>
            <ta e="T580" id="Seg_10945" s="T579">post</ta>
            <ta e="T583" id="Seg_10946" s="T582">que-que&gt;v-v:pred.pn</ta>
            <ta e="T584" id="Seg_10947" s="T583">v-v:tense-v:pred.pn</ta>
            <ta e="T585" id="Seg_10948" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_10949" s="T585">adv</ta>
            <ta e="T587" id="Seg_10950" s="T586">n-n:(poss)-n:case</ta>
            <ta e="T588" id="Seg_10951" s="T587">adv</ta>
            <ta e="T589" id="Seg_10952" s="T588">v-v:tense-v:pred.pn</ta>
            <ta e="T590" id="Seg_10953" s="T589">v-v:cvb</ta>
            <ta e="T593" id="Seg_10954" s="T592">interj</ta>
            <ta e="T594" id="Seg_10955" s="T593">que-que&gt;v-v:cvb</ta>
            <ta e="T595" id="Seg_10956" s="T594">v-v:mood-v:temp.pn</ta>
            <ta e="T596" id="Seg_10957" s="T595">que-pro:case</ta>
            <ta e="T597" id="Seg_10958" s="T596">ptcl</ta>
            <ta e="T598" id="Seg_10959" s="T597">n-n:case</ta>
            <ta e="T599" id="Seg_10960" s="T598">v-v:tense-v:poss.pn</ta>
            <ta e="T600" id="Seg_10961" s="T599">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T601" id="Seg_10962" s="T600">v-v:tense-v:pred.pn</ta>
            <ta e="T602" id="Seg_10963" s="T601">que</ta>
            <ta e="T603" id="Seg_10964" s="T602">n-n:case-n:(pred.pn)-ptcl</ta>
            <ta e="T604" id="Seg_10965" s="T603">pers-pro:case</ta>
            <ta e="T605" id="Seg_10966" s="T604">pers-pro:case</ta>
            <ta e="T606" id="Seg_10967" s="T605">v-v:tense-v:pred.pn</ta>
            <ta e="T607" id="Seg_10968" s="T606">v-v:tense-v:pred.pn</ta>
            <ta e="T608" id="Seg_10969" s="T607">adv</ta>
            <ta e="T609" id="Seg_10970" s="T608">cardnum-n:(poss)-n:case</ta>
            <ta e="T610" id="Seg_10971" s="T609">adv</ta>
            <ta e="T611" id="Seg_10972" s="T610">adv</ta>
            <ta e="T612" id="Seg_10973" s="T611">pers-pro:case</ta>
            <ta e="T613" id="Seg_10974" s="T612">post</ta>
            <ta e="T614" id="Seg_10975" s="T613">v-v:(ins)-v:mood.pn</ta>
            <ta e="T615" id="Seg_10976" s="T614">v-v:tense-v:pred.pn</ta>
            <ta e="T616" id="Seg_10977" s="T615">n-n:case</ta>
            <ta e="T617" id="Seg_10978" s="T616">post</ta>
            <ta e="T619" id="Seg_10979" s="T618">ptcl</ta>
            <ta e="T622" id="Seg_10980" s="T619">que-que&gt;v-v:(neg)-v:ptcp-v:(case)</ta>
            <ta e="T623" id="Seg_10981" s="T622">n-n:case</ta>
            <ta e="T624" id="Seg_10982" s="T623">v-v:cvb</ta>
            <ta e="T625" id="Seg_10983" s="T624">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T626" id="Seg_10984" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_10985" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_10986" s="T627">adv</ta>
            <ta e="T629" id="Seg_10987" s="T628">dempro</ta>
            <ta e="T630" id="Seg_10988" s="T629">que-que&gt;v-v:cvb</ta>
            <ta e="T631" id="Seg_10989" s="T630">v-v:tense-v:pred.pn</ta>
            <ta e="T632" id="Seg_10990" s="T631">que</ta>
            <ta e="T633" id="Seg_10991" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_10992" s="T633">n-n:case</ta>
            <ta e="T635" id="Seg_10993" s="T634">dempro</ta>
            <ta e="T636" id="Seg_10994" s="T635">v-v:tense-v:poss.pn</ta>
            <ta e="T637" id="Seg_10995" s="T636">dempro-pro:case</ta>
            <ta e="T638" id="Seg_10996" s="T637">post</ta>
            <ta e="T639" id="Seg_10997" s="T638">n-n:poss-n:case</ta>
            <ta e="T640" id="Seg_10998" s="T639">ptcl</ta>
            <ta e="T641" id="Seg_10999" s="T640">v-v:(neg)-v:pred.pn</ta>
            <ta e="T642" id="Seg_11000" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_11001" s="T642">adv</ta>
            <ta e="T644" id="Seg_11002" s="T643">adj-adj&gt;adj-n:poss-n:case</ta>
            <ta e="T645" id="Seg_11003" s="T644">que-que&gt;v-v:tense</ta>
            <ta e="T646" id="Seg_11004" s="T645">interj</ta>
            <ta e="T647" id="Seg_11005" s="T646">v-v:tense-v:poss.pn</ta>
            <ta e="T648" id="Seg_11006" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_11007" s="T648">ptcl</ta>
            <ta e="T652" id="Seg_11008" s="T651">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T653" id="Seg_11009" s="T652">n-n:poss-n:case</ta>
            <ta e="T654" id="Seg_11010" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_11011" s="T654">que-que&gt;ordnum-n:poss-n:case</ta>
            <ta e="T656" id="Seg_11012" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_11013" s="T656">adv</ta>
            <ta e="T658" id="Seg_11014" s="T657">v-v:tense-v:pred.pn</ta>
            <ta e="T659" id="Seg_11015" s="T658">adv</ta>
            <ta e="T660" id="Seg_11016" s="T659">que-pro:case</ta>
            <ta e="T661" id="Seg_11017" s="T660">dempro</ta>
            <ta e="T662" id="Seg_11018" s="T661">n-n:poss-n:case</ta>
            <ta e="T663" id="Seg_11019" s="T662">v-v:tense-v:pred.pn</ta>
            <ta e="T664" id="Seg_11020" s="T663">ptcl</ta>
            <ta e="T665" id="Seg_11021" s="T664">adv</ta>
            <ta e="T666" id="Seg_11022" s="T665">dempro</ta>
            <ta e="T667" id="Seg_11023" s="T666">v-v:tense-v:pred.pn</ta>
            <ta e="T668" id="Seg_11024" s="T667">dempro</ta>
            <ta e="T669" id="Seg_11025" s="T668">adj-n:case</ta>
            <ta e="T670" id="Seg_11026" s="T669">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T671" id="Seg_11027" s="T670">conj</ta>
            <ta e="T672" id="Seg_11028" s="T671">pers-pro:case</ta>
            <ta e="T673" id="Seg_11029" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_11030" s="T673">ptcl</ta>
            <ta e="T675" id="Seg_11031" s="T674">v-v&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T676" id="Seg_11032" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_11033" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_11034" s="T677">dempro</ta>
            <ta e="T679" id="Seg_11035" s="T678">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T680" id="Seg_11036" s="T679">conj</ta>
            <ta e="T681" id="Seg_11037" s="T680">adv</ta>
            <ta e="T682" id="Seg_11038" s="T681">que</ta>
            <ta e="T683" id="Seg_11039" s="T682">ptcl</ta>
            <ta e="T684" id="Seg_11040" s="T683">que-que&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T685" id="Seg_11041" s="T684">v-v:(neg)-v:pred.pn</ta>
            <ta e="T686" id="Seg_11042" s="T685">dempro</ta>
            <ta e="T687" id="Seg_11043" s="T686">adv</ta>
            <ta e="T688" id="Seg_11044" s="T687">n-n&gt;adj-n:case</ta>
            <ta e="T689" id="Seg_11045" s="T688">v-v:tense-v:pred.pn</ta>
            <ta e="T690" id="Seg_11046" s="T689">dempro-pro:case</ta>
            <ta e="T691" id="Seg_11047" s="T690">post</ta>
            <ta e="T692" id="Seg_11048" s="T691">n-n:poss-n:case</ta>
            <ta e="T693" id="Seg_11049" s="T692">dempro</ta>
            <ta e="T696" id="Seg_11050" s="T695">n-n:case</ta>
            <ta e="T697" id="Seg_11051" s="T696">v-v:tense-v:pred.pn</ta>
            <ta e="T698" id="Seg_11052" s="T697">ptcl</ta>
            <ta e="T699" id="Seg_11053" s="T698">adv</ta>
            <ta e="T700" id="Seg_11054" s="T699">cardnum</ta>
            <ta e="T701" id="Seg_11055" s="T700">n-n:case</ta>
            <ta e="T702" id="Seg_11056" s="T701">propr-n:case</ta>
            <ta e="T703" id="Seg_11057" s="T702">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T704" id="Seg_11058" s="T703">ptcl</ta>
            <ta e="T705" id="Seg_11059" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_11060" s="T705">adv</ta>
            <ta e="T707" id="Seg_11061" s="T706">adv</ta>
            <ta e="T708" id="Seg_11062" s="T707">que-pro:case</ta>
            <ta e="T709" id="Seg_11063" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_11064" s="T709">n-n:case</ta>
            <ta e="T711" id="Seg_11065" s="T710">v-v:tense-v:pred.pn</ta>
            <ta e="T712" id="Seg_11066" s="T711">n-n:case</ta>
            <ta e="T715" id="Seg_11067" s="T714">n-n:case</ta>
            <ta e="T716" id="Seg_11068" s="T715">interj</ta>
            <ta e="T717" id="Seg_11069" s="T716">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T718" id="Seg_11070" s="T717">conj</ta>
            <ta e="T719" id="Seg_11071" s="T718">ptcl</ta>
            <ta e="T720" id="Seg_11072" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_11073" s="T720">n-n:(poss)-n:case</ta>
            <ta e="T724" id="Seg_11074" s="T723">n-n:(ins)-n:case</ta>
            <ta e="T725" id="Seg_11075" s="T724">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T726" id="Seg_11076" s="T725">ptcl</ta>
            <ta e="T729" id="Seg_11077" s="T728">n-n:(num)-n:case</ta>
            <ta e="T730" id="Seg_11078" s="T729">adv</ta>
            <ta e="T731" id="Seg_11079" s="T730">v-v:tense-v:pred.pn</ta>
            <ta e="T733" id="Seg_11080" s="T732">v-v:cvb</ta>
            <ta e="T734" id="Seg_11081" s="T733">v-v:tense-v:pred.pn</ta>
            <ta e="T735" id="Seg_11082" s="T734">ptcl</ta>
            <ta e="T736" id="Seg_11083" s="T735">conj</ta>
            <ta e="T737" id="Seg_11084" s="T736">pers-pro:case</ta>
            <ta e="T738" id="Seg_11085" s="T737">dempro</ta>
            <ta e="T739" id="Seg_11086" s="T738">n-n:(ins)-n:case</ta>
            <ta e="T740" id="Seg_11087" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_11088" s="T740">v-v&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T742" id="Seg_11089" s="T741">ptcl</ta>
            <ta e="T743" id="Seg_11090" s="T742">adv</ta>
            <ta e="T744" id="Seg_11091" s="T743">dempro</ta>
            <ta e="T745" id="Seg_11092" s="T744">n-n:case</ta>
            <ta e="T746" id="Seg_11093" s="T745">post</ta>
            <ta e="T747" id="Seg_11094" s="T746">v-v:cvb</ta>
            <ta e="T748" id="Seg_11095" s="T747">adv</ta>
            <ta e="T749" id="Seg_11096" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_11097" s="T749">n-n:case</ta>
            <ta e="T751" id="Seg_11098" s="T750">cardnum-n:(num)-n:case</ta>
            <ta e="T752" id="Seg_11099" s="T751">ptcl</ta>
            <ta e="T753" id="Seg_11100" s="T752">cardnum-n:(num)-n:case</ta>
            <ta e="T754" id="Seg_11101" s="T753">post</ta>
            <ta e="T755" id="Seg_11102" s="T754">ptcl</ta>
            <ta e="T756" id="Seg_11103" s="T755">conj</ta>
            <ta e="T757" id="Seg_11104" s="T756">n-n:(num)-n:case</ta>
            <ta e="T758" id="Seg_11105" s="T757">post</ta>
            <ta e="T759" id="Seg_11106" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_11107" s="T759">dempro</ta>
            <ta e="T761" id="Seg_11108" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_11109" s="T761">v-v:tense-v:pred.pn</ta>
            <ta e="T763" id="Seg_11110" s="T762">n-n:case</ta>
            <ta e="T764" id="Seg_11111" s="T763">v-v:mood-v:temp.pn</ta>
            <ta e="T765" id="Seg_11112" s="T764">adv</ta>
            <ta e="T766" id="Seg_11113" s="T765">dempro</ta>
            <ta e="T767" id="Seg_11114" s="T766">adv</ta>
            <ta e="T769" id="Seg_11115" s="T768">que-que&gt;v-v:cvb</ta>
            <ta e="T770" id="Seg_11116" s="T769">v-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T771" id="Seg_11117" s="T770">adv</ta>
            <ta e="T772" id="Seg_11118" s="T771">v-v:tense-v:pred.pn</ta>
            <ta e="T773" id="Seg_11119" s="T772">n-n:poss-n:case</ta>
            <ta e="T774" id="Seg_11120" s="T773">v-v&gt;v-v:cvb</ta>
            <ta e="T775" id="Seg_11121" s="T774">dempro</ta>
            <ta e="T776" id="Seg_11122" s="T775">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T777" id="Seg_11123" s="T776">adv</ta>
            <ta e="T778" id="Seg_11124" s="T777">dempro</ta>
            <ta e="T779" id="Seg_11125" s="T778">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T780" id="Seg_11126" s="T779">v</ta>
            <ta e="T781" id="Seg_11127" s="T780">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T782" id="Seg_11128" s="T781">dempro</ta>
            <ta e="T783" id="Seg_11129" s="T782">adv</ta>
            <ta e="T784" id="Seg_11130" s="T783">v-v:cvb</ta>
            <ta e="T785" id="Seg_11131" s="T784">cardnum</ta>
            <ta e="T786" id="Seg_11132" s="T785">n-n:case</ta>
            <ta e="T787" id="Seg_11133" s="T786">v-v:tense-v:poss.pn</ta>
            <ta e="T788" id="Seg_11134" s="T787">conj</ta>
            <ta e="T789" id="Seg_11135" s="T788">adj-n:poss-n:case</ta>
            <ta e="T790" id="Seg_11136" s="T789">v-v:tense-v:pred.pn</ta>
            <ta e="T791" id="Seg_11137" s="T790">dempro</ta>
            <ta e="T792" id="Seg_11138" s="T791">cardnum</ta>
            <ta e="T793" id="Seg_11139" s="T792">post</ta>
            <ta e="T794" id="Seg_11140" s="T793">n-n:case</ta>
            <ta e="T795" id="Seg_11141" s="T794">n-n:poss-n:case</ta>
            <ta e="T796" id="Seg_11142" s="T795">adv</ta>
            <ta e="T797" id="Seg_11143" s="T796">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T798" id="Seg_11144" s="T797">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T799" id="Seg_11145" s="T798">adv</ta>
            <ta e="T800" id="Seg_11146" s="T799">adv</ta>
            <ta e="T801" id="Seg_11147" s="T800">adv</ta>
            <ta e="T802" id="Seg_11148" s="T801">v-v:tense-v:pred.pn</ta>
            <ta e="T803" id="Seg_11149" s="T802">que-pro:case</ta>
            <ta e="T804" id="Seg_11150" s="T803">n-n&gt;adv</ta>
            <ta e="T805" id="Seg_11151" s="T804">ptcl</ta>
            <ta e="T806" id="Seg_11152" s="T805">adv</ta>
            <ta e="T807" id="Seg_11153" s="T806">v-v:tense-v:poss.pn</ta>
            <ta e="T808" id="Seg_11154" s="T807">adv</ta>
            <ta e="T809" id="Seg_11155" s="T808">ptcl</ta>
            <ta e="T810" id="Seg_11156" s="T809">adv</ta>
            <ta e="T811" id="Seg_11157" s="T810">adv</ta>
            <ta e="T812" id="Seg_11158" s="T811">v-v:tense-v:poss.pn</ta>
            <ta e="T813" id="Seg_11159" s="T812">ptcl</ta>
            <ta e="T814" id="Seg_11160" s="T813">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T815" id="Seg_11161" s="T814">adv</ta>
            <ta e="T816" id="Seg_11162" s="T815">v-v:cvb</ta>
            <ta e="T817" id="Seg_11163" s="T816">v-v:tense-v:pred.pn</ta>
            <ta e="T818" id="Seg_11164" s="T817">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T819" id="Seg_11165" s="T818">v-v:cvb</ta>
            <ta e="T820" id="Seg_11166" s="T819">v-v:tense-v:pred.pn</ta>
            <ta e="T821" id="Seg_11167" s="T820">adv</ta>
            <ta e="T822" id="Seg_11168" s="T821">ptcl</ta>
            <ta e="T823" id="Seg_11169" s="T822">adv</ta>
            <ta e="T824" id="Seg_11170" s="T823">n-n:poss-n:case</ta>
            <ta e="T825" id="Seg_11171" s="T824">v-v:tense-v:poss.pn</ta>
            <ta e="T826" id="Seg_11172" s="T825">adv</ta>
            <ta e="T827" id="Seg_11173" s="T826">n-n:poss-n:case</ta>
            <ta e="T828" id="Seg_11174" s="T827">v-v:cvb</ta>
            <ta e="T829" id="Seg_11175" s="T828">v-v:cvb-v:pred.pn</ta>
            <ta e="T830" id="Seg_11176" s="T829">adv</ta>
            <ta e="T831" id="Seg_11177" s="T830">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T832" id="Seg_11178" s="T831">ptcl</ta>
            <ta e="T833" id="Seg_11179" s="T832">que-pro:(poss)-pro:case</ta>
            <ta e="T834" id="Seg_11180" s="T833">n-n:poss-n:case</ta>
            <ta e="T835" id="Seg_11181" s="T834">adv</ta>
            <ta e="T838" id="Seg_11182" s="T837">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T839" id="Seg_11183" s="T838">dempro</ta>
            <ta e="T840" id="Seg_11184" s="T839">v-v:cvb</ta>
            <ta e="T841" id="Seg_11185" s="T840">v-v:cvb-v:pred.pn</ta>
            <ta e="T842" id="Seg_11186" s="T841">adv</ta>
            <ta e="T843" id="Seg_11187" s="T842">dempro</ta>
            <ta e="T844" id="Seg_11188" s="T843">n-n:(num)-n:case</ta>
            <ta e="T845" id="Seg_11189" s="T844">v-v:mood-v:pred.pn</ta>
            <ta e="T848" id="Seg_11190" s="T847">ptcl</ta>
            <ta e="T849" id="Seg_11191" s="T848">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T850" id="Seg_11192" s="T849">v-v:tense-v:poss.pn</ta>
            <ta e="T851" id="Seg_11193" s="T850">adv</ta>
            <ta e="T852" id="Seg_11194" s="T851">adv</ta>
            <ta e="T853" id="Seg_11195" s="T852">n-n:case</ta>
            <ta e="T854" id="Seg_11196" s="T853">dempro-pro:case</ta>
            <ta e="T855" id="Seg_11197" s="T854">n-n:case</ta>
            <ta e="T856" id="Seg_11198" s="T855">que-pro:case</ta>
            <ta e="T857" id="Seg_11199" s="T856">v-v:mood-v:pred.pn</ta>
            <ta e="T860" id="Seg_11200" s="T859">dempro-pro:case</ta>
            <ta e="T861" id="Seg_11201" s="T860">n-n:poss-n:case</ta>
            <ta e="T862" id="Seg_11202" s="T861">v-v:tense-v:poss.pn</ta>
            <ta e="T863" id="Seg_11203" s="T862">v-v:mood.pn</ta>
            <ta e="T864" id="Seg_11204" s="T863">v-v:tense-v:pred.pn</ta>
            <ta e="T865" id="Seg_11205" s="T864">que-pro:case</ta>
            <ta e="T866" id="Seg_11206" s="T865">n-n:case</ta>
            <ta e="T867" id="Seg_11207" s="T866">n-n:case</ta>
            <ta e="T868" id="Seg_11208" s="T867">dempro-pro:case</ta>
            <ta e="T869" id="Seg_11209" s="T868">v-v:cvb</ta>
            <ta e="T870" id="Seg_11210" s="T869">v-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T871" id="Seg_11211" s="T870">v-v:tense-v:poss.pn</ta>
            <ta e="T872" id="Seg_11212" s="T871">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T873" id="Seg_11213" s="T872">n-n:case</ta>
            <ta e="T874" id="Seg_11214" s="T873">v-v:cvb</ta>
            <ta e="T875" id="Seg_11215" s="T874">interj</ta>
            <ta e="T878" id="Seg_11216" s="T877">n-n:case</ta>
            <ta e="T879" id="Seg_11217" s="T878">interj</ta>
            <ta e="T880" id="Seg_11218" s="T879">v-v:tense-v:pred.pn</ta>
            <ta e="T881" id="Seg_11219" s="T880">v-v:cvb</ta>
            <ta e="T882" id="Seg_11220" s="T881">v</ta>
            <ta e="T883" id="Seg_11221" s="T930">ptcl</ta>
            <ta e="T885" id="Seg_11222" s="T883">v-v:tense-v:pred.pn</ta>
            <ta e="T886" id="Seg_11223" s="T885">interj</ta>
            <ta e="T887" id="Seg_11224" s="T886">v-v:tense-v:pred.pn</ta>
            <ta e="T888" id="Seg_11225" s="T887">n-n:poss-n:case</ta>
            <ta e="T889" id="Seg_11226" s="T888">adv</ta>
            <ta e="T890" id="Seg_11227" s="T889">v-v:ptcp</ta>
            <ta e="T891" id="Seg_11228" s="T890">v-v:tense-v:poss.pn</ta>
            <ta e="T892" id="Seg_11229" s="T891">dempro</ta>
            <ta e="T893" id="Seg_11230" s="T892">que-pro:(ins)-pro:(poss)-pro:case</ta>
            <ta e="T894" id="Seg_11231" s="T893">n-n:poss-n:case</ta>
            <ta e="T895" id="Seg_11232" s="T894">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T896" id="Seg_11233" s="T895">ptcl</ta>
            <ta e="T897" id="Seg_11234" s="T896">que-pro:case</ta>
            <ta e="T898" id="Seg_11235" s="T897">n-n:(ins)-n:case</ta>
            <ta e="T899" id="Seg_11236" s="T898">v-v:ptcp</ta>
            <ta e="T900" id="Seg_11237" s="T899">v-v:tense-v:poss.pn</ta>
            <ta e="T901" id="Seg_11238" s="T900">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T902" id="Seg_11239" s="T901">ptcl</ta>
            <ta e="T903" id="Seg_11240" s="T902">que-pro:case</ta>
            <ta e="T904" id="Seg_11241" s="T903">n-n:(num)-n:case</ta>
            <ta e="T905" id="Seg_11242" s="T904">n-n:poss-n:case</ta>
            <ta e="T906" id="Seg_11243" s="T905">que-pro:case</ta>
            <ta e="T907" id="Seg_11244" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_11245" s="T907">ptcl</ta>
            <ta e="T909" id="Seg_11246" s="T908">n-n:(num)-n:poss-n:case</ta>
            <ta e="T910" id="Seg_11247" s="T909">ptcl</ta>
            <ta e="T911" id="Seg_11248" s="T910">v-v:tense-v:pred.pn</ta>
            <ta e="T912" id="Seg_11249" s="T911">n-n:case</ta>
            <ta e="T913" id="Seg_11250" s="T912">v-v:mood-v:temp.pn</ta>
            <ta e="T914" id="Seg_11251" s="T913">n-n:case</ta>
            <ta e="T915" id="Seg_11252" s="T914">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T916" id="Seg_11253" s="T915">ptcl</ta>
            <ta e="T917" id="Seg_11254" s="T916">dempro</ta>
            <ta e="T918" id="Seg_11255" s="T917">n-n:case</ta>
            <ta e="T919" id="Seg_11256" s="T918">v-v:cvb</ta>
            <ta e="T920" id="Seg_11257" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_11258" s="T920">n-n:(num)-n:case</ta>
            <ta e="T922" id="Seg_11259" s="T921">v-v:mood-v:temp.pn</ta>
            <ta e="T923" id="Seg_11260" s="T922">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T924" id="Seg_11261" s="T923">v-v:tense-v:pred.pn</ta>
            <ta e="T929" id="Seg_11262" s="T928">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T931" id="Seg_11263" s="T929">ptcl</ta>
            <ta e="T932" id="Seg_11264" s="T931">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T933" id="Seg_11265" s="T932">dempro</ta>
            <ta e="T934" id="Seg_11266" s="T933">n-n:case</ta>
            <ta e="T935" id="Seg_11267" s="T937">n-n:case</ta>
            <ta e="T936" id="Seg_11268" s="T935">v-v:ptcp</ta>
            <ta e="T938" id="Seg_11269" s="T936">v-v:tense-v:pred.pn</ta>
            <ta e="T939" id="Seg_11270" s="T938">pers-pro:case</ta>
            <ta e="T940" id="Seg_11271" s="T939">n-n:poss-n:case</ta>
            <ta e="T941" id="Seg_11272" s="T940">adv</ta>
            <ta e="T942" id="Seg_11273" s="T941">adj-n:(num)-n:case</ta>
            <ta e="T943" id="Seg_11274" s="T942">v-v:tense-v:pred.pn</ta>
            <ta e="T944" id="Seg_11275" s="T943">v-v:tense-v:pred.pn</ta>
            <ta e="T945" id="Seg_11276" s="T944">n-n:case</ta>
            <ta e="T946" id="Seg_11277" s="T945">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T947" id="Seg_11278" s="T946">ptcl</ta>
            <ta e="T948" id="Seg_11279" s="T947">adv</ta>
            <ta e="T949" id="Seg_11280" s="T948">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T950" id="Seg_11281" s="T949">v-v:tense-v:pred.pn</ta>
            <ta e="T951" id="Seg_11282" s="T950">ptcl</ta>
            <ta e="T952" id="Seg_11283" s="T951">indfpro-pro:case-indfpro-pro:case</ta>
            <ta e="T953" id="Seg_11284" s="T952">v-v:ptcp</ta>
            <ta e="T954" id="Seg_11285" s="T953">v-v:tense-v:pred.pn</ta>
            <ta e="T955" id="Seg_11286" s="T954">v-v:ptcp</ta>
            <ta e="T956" id="Seg_11287" s="T955">v-v:mood-v:temp.pn</ta>
            <ta e="T957" id="Seg_11288" s="T956">pers-pro:case</ta>
            <ta e="T958" id="Seg_11289" s="T957">ptcl</ta>
            <ta e="T959" id="Seg_11290" s="T958">ptcl</ta>
            <ta e="T960" id="Seg_11291" s="T959">v-v:tense-v:pred.pn</ta>
            <ta e="T961" id="Seg_11292" s="T960">ptcl</ta>
            <ta e="T962" id="Seg_11293" s="T961">adv</ta>
            <ta e="T963" id="Seg_11294" s="T962">ptcl</ta>
            <ta e="T964" id="Seg_11295" s="T963">v-v&gt;n-n:case</ta>
            <ta e="T965" id="Seg_11296" s="T964">v-v:tense-v:pred.pn</ta>
            <ta e="T966" id="Seg_11297" s="T965">v-v:tense-v:pred.pn</ta>
            <ta e="T967" id="Seg_11298" s="T966">n-n:case</ta>
            <ta e="T968" id="Seg_11299" s="T967">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T971" id="Seg_11300" s="T970">adj</ta>
            <ta e="T972" id="Seg_11301" s="T971">n-n:case</ta>
            <ta e="T973" id="Seg_11302" s="T972">que-pro:case</ta>
            <ta e="T974" id="Seg_11303" s="T973">v-v:mood-v:pred.pn</ta>
            <ta e="T975" id="Seg_11304" s="T974">n-n:case</ta>
            <ta e="T976" id="Seg_11305" s="T975">dempro</ta>
            <ta e="T977" id="Seg_11306" s="T976">n-n:case</ta>
            <ta e="T978" id="Seg_11307" s="T977">v-v:tense</ta>
            <ta e="T979" id="Seg_11308" s="T978">interj</ta>
            <ta e="T980" id="Seg_11309" s="T979">v-v:tense-v:poss.pn</ta>
            <ta e="T981" id="Seg_11310" s="T980">conj</ta>
            <ta e="T982" id="Seg_11311" s="T981">n-n:case</ta>
            <ta e="T983" id="Seg_11312" s="T982">v-v:tense-v:poss.pn</ta>
            <ta e="T984" id="Seg_11313" s="T983">propr-n:case</ta>
            <ta e="T985" id="Seg_11314" s="T984">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T986" id="Seg_11315" s="T985">adj</ta>
            <ta e="T987" id="Seg_11316" s="T986">n-n:case</ta>
            <ta e="T988" id="Seg_11317" s="T987">adv</ta>
            <ta e="T991" id="Seg_11318" s="T990">interj</ta>
            <ta e="T992" id="Seg_11319" s="T991">adv</ta>
            <ta e="T993" id="Seg_11320" s="T992">cardnum</ta>
            <ta e="T994" id="Seg_11321" s="T993">n-n:case</ta>
            <ta e="T995" id="Seg_11322" s="T994">v-v:tense-v:poss.pn</ta>
            <ta e="T996" id="Seg_11323" s="T995">conj</ta>
            <ta e="T997" id="Seg_11324" s="T996">n-n:(poss)-n:case</ta>
            <ta e="T998" id="Seg_11325" s="T997">post</ta>
            <ta e="T999" id="Seg_11326" s="T998">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-ChVD">
            <ta e="T1" id="Seg_11327" s="T0">propr</ta>
            <ta e="T2" id="Seg_11328" s="T1">v</ta>
            <ta e="T6" id="Seg_11329" s="T5">n</ta>
            <ta e="T7" id="Seg_11330" s="T6">n</ta>
            <ta e="T11" id="Seg_11331" s="T10">n</ta>
            <ta e="T12" id="Seg_11332" s="T11">v</ta>
            <ta e="T13" id="Seg_11333" s="T12">aux</ta>
            <ta e="T14" id="Seg_11334" s="T13">n</ta>
            <ta e="T15" id="Seg_11335" s="T14">propr</ta>
            <ta e="T16" id="Seg_11336" s="T15">v</ta>
            <ta e="T17" id="Seg_11337" s="T16">propr</ta>
            <ta e="T18" id="Seg_11338" s="T17">que</ta>
            <ta e="T19" id="Seg_11339" s="T18">interj</ta>
            <ta e="T9" id="Seg_11340" s="T19">adj</ta>
            <ta e="T20" id="Seg_11341" s="T9">n</ta>
            <ta e="T21" id="Seg_11342" s="T20">v</ta>
            <ta e="T22" id="Seg_11343" s="T21">aux</ta>
            <ta e="T23" id="Seg_11344" s="T22">n</ta>
            <ta e="T24" id="Seg_11345" s="T23">post</ta>
            <ta e="T25" id="Seg_11346" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_11347" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_11348" s="T26">v</ta>
            <ta e="T28" id="Seg_11349" s="T27">propr</ta>
            <ta e="T29" id="Seg_11350" s="T28">interj</ta>
            <ta e="T30" id="Seg_11351" s="T29">post</ta>
            <ta e="T31" id="Seg_11352" s="T30">v</ta>
            <ta e="T32" id="Seg_11353" s="T31">propr</ta>
            <ta e="T33" id="Seg_11354" s="T32">n</ta>
            <ta e="T34" id="Seg_11355" s="T33">adv</ta>
            <ta e="T35" id="Seg_11356" s="T34">v</ta>
            <ta e="T36" id="Seg_11357" s="T35">propr</ta>
            <ta e="T37" id="Seg_11358" s="T36">n</ta>
            <ta e="T41" id="Seg_11359" s="T40">cardnum</ta>
            <ta e="T42" id="Seg_11360" s="T41">cardnum</ta>
            <ta e="T43" id="Seg_11361" s="T42">n</ta>
            <ta e="T44" id="Seg_11362" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_11363" s="T44">que</ta>
            <ta e="T46" id="Seg_11364" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_11365" s="T46">v</ta>
            <ta e="T48" id="Seg_11366" s="T47">post</ta>
            <ta e="T49" id="Seg_11367" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_11368" s="T49">v</ta>
            <ta e="T51" id="Seg_11369" s="T54">que</ta>
            <ta e="T52" id="Seg_11370" s="T51">propr</ta>
            <ta e="T56" id="Seg_11371" s="T55">n</ta>
            <ta e="T57" id="Seg_11372" s="T56">v</ta>
            <ta e="T58" id="Seg_11373" s="T57">aux</ta>
            <ta e="T60" id="Seg_11374" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_11375" s="T60">adv</ta>
            <ta e="T62" id="Seg_11376" s="T61">adj</ta>
            <ta e="T63" id="Seg_11377" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_11378" s="T63">v</ta>
            <ta e="T65" id="Seg_11379" s="T64">n</ta>
            <ta e="T66" id="Seg_11380" s="T65">post</ta>
            <ta e="T67" id="Seg_11381" s="T66">dempro</ta>
            <ta e="T68" id="Seg_11382" s="T67">v</ta>
            <ta e="T69" id="Seg_11383" s="T68">adv</ta>
            <ta e="T70" id="Seg_11384" s="T69">v</ta>
            <ta e="T71" id="Seg_11385" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_11386" s="T71">dempro</ta>
            <ta e="T73" id="Seg_11387" s="T72">v</ta>
            <ta e="T74" id="Seg_11388" s="T73">que</ta>
            <ta e="T75" id="Seg_11389" s="T74">propr</ta>
            <ta e="T76" id="Seg_11390" s="T75">v</ta>
            <ta e="T77" id="Seg_11391" s="T76">v</ta>
            <ta e="T481" id="Seg_11392" s="T80">adv</ta>
            <ta e="T81" id="Seg_11393" s="T481">v</ta>
            <ta e="T82" id="Seg_11394" s="T81">aux</ta>
            <ta e="T83" id="Seg_11395" s="T82">adv</ta>
            <ta e="T84" id="Seg_11396" s="T83">n</ta>
            <ta e="T85" id="Seg_11397" s="T84">v</ta>
            <ta e="T86" id="Seg_11398" s="T85">aux</ta>
            <ta e="T92" id="Seg_11399" s="T91">que</ta>
            <ta e="T93" id="Seg_11400" s="T92">v</ta>
            <ta e="T94" id="Seg_11401" s="T93">adj</ta>
            <ta e="T95" id="Seg_11402" s="T94">que</ta>
            <ta e="T96" id="Seg_11403" s="T95">v</ta>
            <ta e="T97" id="Seg_11404" s="T96">adv</ta>
            <ta e="T101" id="Seg_11405" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_11406" s="T101">adv</ta>
            <ta e="T103" id="Seg_11407" s="T102">v</ta>
            <ta e="T104" id="Seg_11408" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_11409" s="T104">propr</ta>
            <ta e="T106" id="Seg_11410" s="T105">que</ta>
            <ta e="T107" id="Seg_11411" s="T106">v</ta>
            <ta e="T108" id="Seg_11412" s="T107">dempro</ta>
            <ta e="T109" id="Seg_11413" s="T108">propr</ta>
            <ta e="T110" id="Seg_11414" s="T109">dempro</ta>
            <ta e="T111" id="Seg_11415" s="T110">propr</ta>
            <ta e="T115" id="Seg_11416" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_11417" s="T115">cardnum</ta>
            <ta e="T117" id="Seg_11418" s="T116">n</ta>
            <ta e="T118" id="Seg_11419" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_11420" s="T118">que</ta>
            <ta e="T120" id="Seg_11421" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_11422" s="T120">v</ta>
            <ta e="T122" id="Seg_11423" s="T121">post</ta>
            <ta e="T123" id="Seg_11424" s="T122">adj</ta>
            <ta e="T124" id="Seg_11425" s="T123">n</ta>
            <ta e="T125" id="Seg_11426" s="T124">adv</ta>
            <ta e="T126" id="Seg_11427" s="T125">v</ta>
            <ta e="T127" id="Seg_11428" s="T126">aux</ta>
            <ta e="T128" id="Seg_11429" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_11430" s="T128">adv</ta>
            <ta e="T130" id="Seg_11431" s="T129">v</ta>
            <ta e="T131" id="Seg_11432" s="T130">aux</ta>
            <ta e="T132" id="Seg_11433" s="T131">ptcl</ta>
            <ta e="T134" id="Seg_11434" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_11435" s="T134">adv</ta>
            <ta e="T136" id="Seg_11436" s="T135">v</ta>
            <ta e="T139" id="Seg_11437" s="T138">v</ta>
            <ta e="T140" id="Seg_11438" s="T139">aux</ta>
            <ta e="T141" id="Seg_11439" s="T140">n</ta>
            <ta e="T142" id="Seg_11440" s="T141">adj</ta>
            <ta e="T143" id="Seg_11441" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_11442" s="T143">adv</ta>
            <ta e="T145" id="Seg_11443" s="T144">adj</ta>
            <ta e="T146" id="Seg_11444" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_11445" s="T146">cop</ta>
            <ta e="T148" id="Seg_11446" s="T147">interj</ta>
            <ta e="T151" id="Seg_11447" s="T150">v</ta>
            <ta e="T152" id="Seg_11448" s="T151">adv</ta>
            <ta e="T153" id="Seg_11449" s="T152">adv</ta>
            <ta e="T154" id="Seg_11450" s="T153">adv</ta>
            <ta e="T155" id="Seg_11451" s="T154">v</ta>
            <ta e="T156" id="Seg_11452" s="T155">dempro</ta>
            <ta e="T157" id="Seg_11453" s="T156">que</ta>
            <ta e="T158" id="Seg_11454" s="T157">que</ta>
            <ta e="T161" id="Seg_11455" s="T160">dempro</ta>
            <ta e="T162" id="Seg_11456" s="T161">n</ta>
            <ta e="T163" id="Seg_11457" s="T162">cop</ta>
            <ta e="T164" id="Seg_11458" s="T163">n</ta>
            <ta e="T165" id="Seg_11459" s="T164">cop</ta>
            <ta e="T166" id="Seg_11460" s="T165">post</ta>
            <ta e="T167" id="Seg_11461" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_11462" s="T167">adv</ta>
            <ta e="T169" id="Seg_11463" s="T168">v</ta>
            <ta e="T170" id="Seg_11464" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_11465" s="T170">que</ta>
            <ta e="T172" id="Seg_11466" s="T171">adj</ta>
            <ta e="T173" id="Seg_11467" s="T172">dempro</ta>
            <ta e="T174" id="Seg_11468" s="T173">n</ta>
            <ta e="T175" id="Seg_11469" s="T174">interj</ta>
            <ta e="T176" id="Seg_11470" s="T175">que</ta>
            <ta e="T177" id="Seg_11471" s="T176">propr</ta>
            <ta e="T178" id="Seg_11472" s="T177">propr</ta>
            <ta e="T179" id="Seg_11473" s="T178">v</ta>
            <ta e="T180" id="Seg_11474" s="T179">dempro</ta>
            <ta e="T181" id="Seg_11475" s="T180">conj</ta>
            <ta e="T182" id="Seg_11476" s="T181">dempro</ta>
            <ta e="T183" id="Seg_11477" s="T182">post</ta>
            <ta e="T184" id="Seg_11478" s="T183">v</ta>
            <ta e="T185" id="Seg_11479" s="T184">v</ta>
            <ta e="T186" id="Seg_11480" s="T185">n</ta>
            <ta e="T187" id="Seg_11481" s="T186">que</ta>
            <ta e="T188" id="Seg_11482" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_11483" s="T188">n</ta>
            <ta e="T190" id="Seg_11484" s="T189">v</ta>
            <ta e="T191" id="Seg_11485" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_11486" s="T191">adv</ta>
            <ta e="T193" id="Seg_11487" s="T192">n</ta>
            <ta e="T194" id="Seg_11488" s="T193">adv</ta>
            <ta e="T196" id="Seg_11489" s="T195">v</ta>
            <ta e="T197" id="Seg_11490" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_11491" s="T197">adv</ta>
            <ta e="T199" id="Seg_11492" s="T198">v</ta>
            <ta e="T200" id="Seg_11493" s="T199">v</ta>
            <ta e="T201" id="Seg_11494" s="T200">v</ta>
            <ta e="T202" id="Seg_11495" s="T201">aux</ta>
            <ta e="T203" id="Seg_11496" s="T202">aux</ta>
            <ta e="T204" id="Seg_11497" s="T203">dempro</ta>
            <ta e="T205" id="Seg_11498" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_11499" s="T205">v</ta>
            <ta e="T207" id="Seg_11500" s="T206">n</ta>
            <ta e="T208" id="Seg_11501" s="T207">v</ta>
            <ta e="T209" id="Seg_11502" s="T208">que</ta>
            <ta e="T212" id="Seg_11503" s="T211">v</ta>
            <ta e="T214" id="Seg_11504" s="T212">adv</ta>
            <ta e="T215" id="Seg_11505" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_11506" s="T215">n</ta>
            <ta e="T217" id="Seg_11507" s="T216">v</ta>
            <ta e="T218" id="Seg_11508" s="T217">adv</ta>
            <ta e="T219" id="Seg_11509" s="T218">n</ta>
            <ta e="T220" id="Seg_11510" s="T219">v</ta>
            <ta e="T221" id="Seg_11511" s="T220">ptcl</ta>
            <ta e="T223" id="Seg_11512" s="T222">que</ta>
            <ta e="T224" id="Seg_11513" s="T223">v</ta>
            <ta e="T225" id="Seg_11514" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_11515" s="T225">cop</ta>
            <ta e="T227" id="Seg_11516" s="T226">emphpro</ta>
            <ta e="T228" id="Seg_11517" s="T227">v</ta>
            <ta e="T229" id="Seg_11518" s="T228">adv</ta>
            <ta e="T230" id="Seg_11519" s="T229">adv</ta>
            <ta e="T233" id="Seg_11520" s="T232">v</ta>
            <ta e="T234" id="Seg_11521" s="T233">n</ta>
            <ta e="T235" id="Seg_11522" s="T234">interj</ta>
            <ta e="T236" id="Seg_11523" s="T235">n</ta>
            <ta e="T237" id="Seg_11524" s="T236">v</ta>
            <ta e="T238" id="Seg_11525" s="T237">ptcl</ta>
            <ta e="T241" id="Seg_11526" s="T240">v</ta>
            <ta e="T244" id="Seg_11527" s="T243">interj</ta>
            <ta e="T245" id="Seg_11528" s="T244">v</ta>
            <ta e="T246" id="Seg_11529" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_11530" s="T246">emphpro</ta>
            <ta e="T248" id="Seg_11531" s="T247">v</ta>
            <ta e="T249" id="Seg_11532" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_11533" s="T249">v</ta>
            <ta e="T251" id="Seg_11534" s="T250">adv</ta>
            <ta e="T252" id="Seg_11535" s="T251">v</ta>
            <ta e="T253" id="Seg_11536" s="T252">conj</ta>
            <ta e="T255" id="Seg_11537" s="T253">adv</ta>
            <ta e="T256" id="Seg_11538" s="T255">v</ta>
            <ta e="T257" id="Seg_11539" s="T256">dempro</ta>
            <ta e="T258" id="Seg_11540" s="T257">v</ta>
            <ta e="T259" id="Seg_11541" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_11542" s="T259">dempro</ta>
            <ta e="T261" id="Seg_11543" s="T260">v</ta>
            <ta e="T262" id="Seg_11544" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_11545" s="T262">dempro</ta>
            <ta e="T264" id="Seg_11546" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_11547" s="T264">n</ta>
            <ta e="T266" id="Seg_11548" s="T265">v</ta>
            <ta e="T267" id="Seg_11549" s="T266">v</ta>
            <ta e="T268" id="Seg_11550" s="T267">adv</ta>
            <ta e="T271" id="Seg_11551" s="T270">que</ta>
            <ta e="T272" id="Seg_11552" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_11553" s="T272">v</ta>
            <ta e="T274" id="Seg_11554" s="T273">n</ta>
            <ta e="T275" id="Seg_11555" s="T274">n</ta>
            <ta e="T276" id="Seg_11556" s="T275">conj</ta>
            <ta e="T277" id="Seg_11557" s="T276">que</ta>
            <ta e="T278" id="Seg_11558" s="T277">conj</ta>
            <ta e="T279" id="Seg_11559" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_11560" s="T279">dempro</ta>
            <ta e="T281" id="Seg_11561" s="T280">pers</ta>
            <ta e="T282" id="Seg_11562" s="T281">v</ta>
            <ta e="T283" id="Seg_11563" s="T282">v</ta>
            <ta e="T284" id="Seg_11564" s="T283">cardnum</ta>
            <ta e="T285" id="Seg_11565" s="T284">n</ta>
            <ta e="T286" id="Seg_11566" s="T285">v</ta>
            <ta e="T287" id="Seg_11567" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_11568" s="T287">adv</ta>
            <ta e="T289" id="Seg_11569" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_11570" s="T289">adv</ta>
            <ta e="T291" id="Seg_11571" s="T290">dempro</ta>
            <ta e="T292" id="Seg_11572" s="T291">n</ta>
            <ta e="T293" id="Seg_11573" s="T292">n</ta>
            <ta e="T294" id="Seg_11574" s="T293">v</ta>
            <ta e="T295" id="Seg_11575" s="T294">n</ta>
            <ta e="T296" id="Seg_11576" s="T295">v</ta>
            <ta e="T297" id="Seg_11577" s="T296">adv</ta>
            <ta e="T298" id="Seg_11578" s="T297">v</ta>
            <ta e="T299" id="Seg_11579" s="T298">v</ta>
            <ta e="T300" id="Seg_11580" s="T299">v</ta>
            <ta e="T301" id="Seg_11581" s="T300">dempro</ta>
            <ta e="T302" id="Seg_11582" s="T301">n</ta>
            <ta e="T303" id="Seg_11583" s="T302">adj</ta>
            <ta e="T304" id="Seg_11584" s="T303">dempro</ta>
            <ta e="T305" id="Seg_11585" s="T304">n</ta>
            <ta e="T306" id="Seg_11586" s="T305">n</ta>
            <ta e="T307" id="Seg_11587" s="T306">adv</ta>
            <ta e="T308" id="Seg_11588" s="T307">adj</ta>
            <ta e="T309" id="Seg_11589" s="T308">n</ta>
            <ta e="T310" id="Seg_11590" s="T309">emphpro</ta>
            <ta e="T313" id="Seg_11591" s="T312">n</ta>
            <ta e="T314" id="Seg_11592" s="T313">dempro</ta>
            <ta e="T315" id="Seg_11593" s="T314">v</ta>
            <ta e="T316" id="Seg_11594" s="T315">v</ta>
            <ta e="T317" id="Seg_11595" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_11596" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_11597" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_11598" s="T319">que</ta>
            <ta e="T321" id="Seg_11599" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_11600" s="T321">v</ta>
            <ta e="T323" id="Seg_11601" s="T322">v</ta>
            <ta e="T327" id="Seg_11602" s="T326">v</ta>
            <ta e="T328" id="Seg_11603" s="T327">n</ta>
            <ta e="T329" id="Seg_11604" s="T328">adv</ta>
            <ta e="T330" id="Seg_11605" s="T329">v</ta>
            <ta e="T331" id="Seg_11606" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_11607" s="T331">n</ta>
            <ta e="T333" id="Seg_11608" s="T332">dempro</ta>
            <ta e="T334" id="Seg_11609" s="T333">v</ta>
            <ta e="T335" id="Seg_11610" s="T334">aux</ta>
            <ta e="T336" id="Seg_11611" s="T335">que</ta>
            <ta e="T337" id="Seg_11612" s="T336">que</ta>
            <ta e="T338" id="Seg_11613" s="T337">v</ta>
            <ta e="T339" id="Seg_11614" s="T338">v</ta>
            <ta e="T340" id="Seg_11615" s="T339">adv</ta>
            <ta e="T341" id="Seg_11616" s="T340">v</ta>
            <ta e="T342" id="Seg_11617" s="T341">n</ta>
            <ta e="T343" id="Seg_11618" s="T342">v</ta>
            <ta e="T344" id="Seg_11619" s="T343">n</ta>
            <ta e="T345" id="Seg_11620" s="T344">post</ta>
            <ta e="T346" id="Seg_11621" s="T345">adv</ta>
            <ta e="T347" id="Seg_11622" s="T346">v</ta>
            <ta e="T348" id="Seg_11623" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_11624" s="T348">adv</ta>
            <ta e="T350" id="Seg_11625" s="T349">n</ta>
            <ta e="T351" id="Seg_11626" s="T350">v</ta>
            <ta e="T352" id="Seg_11627" s="T351">adv</ta>
            <ta e="T353" id="Seg_11628" s="T352">v</ta>
            <ta e="T354" id="Seg_11629" s="T353">v</ta>
            <ta e="T355" id="Seg_11630" s="T354">adv</ta>
            <ta e="T357" id="Seg_11631" s="T355">adv</ta>
            <ta e="T358" id="Seg_11632" s="T357">n</ta>
            <ta e="T359" id="Seg_11633" s="T358">v</ta>
            <ta e="T360" id="Seg_11634" s="T359">n</ta>
            <ta e="T361" id="Seg_11635" s="T360">n</ta>
            <ta e="T362" id="Seg_11636" s="T361">v</ta>
            <ta e="T363" id="Seg_11637" s="T362">v</ta>
            <ta e="T364" id="Seg_11638" s="T363">emphpro</ta>
            <ta e="T365" id="Seg_11639" s="T364">n</ta>
            <ta e="T366" id="Seg_11640" s="T365">interj</ta>
            <ta e="T367" id="Seg_11641" s="T366">n</ta>
            <ta e="T368" id="Seg_11642" s="T367">v</ta>
            <ta e="T369" id="Seg_11643" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_11644" s="T369">dempro</ta>
            <ta e="T371" id="Seg_11645" s="T370">v</ta>
            <ta e="T372" id="Seg_11646" s="T371">interj</ta>
            <ta e="T375" id="Seg_11647" s="T374">n</ta>
            <ta e="T376" id="Seg_11648" s="T375">v</ta>
            <ta e="T377" id="Seg_11649" s="T376">adv</ta>
            <ta e="T378" id="Seg_11650" s="T377">cardnum</ta>
            <ta e="T379" id="Seg_11651" s="T378">post</ta>
            <ta e="T380" id="Seg_11652" s="T379">n</ta>
            <ta e="T381" id="Seg_11653" s="T380">v</ta>
            <ta e="T382" id="Seg_11654" s="T381">indfpro</ta>
            <ta e="T383" id="Seg_11655" s="T382">adv</ta>
            <ta e="T384" id="Seg_11656" s="T383">v</ta>
            <ta e="T385" id="Seg_11657" s="T384">dempro</ta>
            <ta e="T386" id="Seg_11658" s="T385">n</ta>
            <ta e="T387" id="Seg_11659" s="T386">v</ta>
            <ta e="T390" id="Seg_11660" s="T389">n</ta>
            <ta e="T391" id="Seg_11661" s="T390">v</ta>
            <ta e="T392" id="Seg_11662" s="T391">adv</ta>
            <ta e="T393" id="Seg_11663" s="T392">que</ta>
            <ta e="T394" id="Seg_11664" s="T393">adv</ta>
            <ta e="T395" id="Seg_11665" s="T394">v</ta>
            <ta e="T396" id="Seg_11666" s="T395">n</ta>
            <ta e="T397" id="Seg_11667" s="T396">post</ta>
            <ta e="T398" id="Seg_11668" s="T397">n</ta>
            <ta e="T399" id="Seg_11669" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_11670" s="T399">v</ta>
            <ta e="T401" id="Seg_11671" s="T400">ptcl</ta>
            <ta e="T402" id="Seg_11672" s="T401">n</ta>
            <ta e="T403" id="Seg_11673" s="T402">cardnum</ta>
            <ta e="T404" id="Seg_11674" s="T403">n</ta>
            <ta e="T405" id="Seg_11675" s="T404">cop</ta>
            <ta e="T406" id="Seg_11676" s="T405">adj</ta>
            <ta e="T407" id="Seg_11677" s="T406">post</ta>
            <ta e="T408" id="Seg_11678" s="T407">adj</ta>
            <ta e="T409" id="Seg_11679" s="T408">post</ta>
            <ta e="T410" id="Seg_11680" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_11681" s="T410">distrnum</ta>
            <ta e="T412" id="Seg_11682" s="T411">n</ta>
            <ta e="T413" id="Seg_11683" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_11684" s="T413">que</ta>
            <ta e="T415" id="Seg_11685" s="T414">cop</ta>
            <ta e="T416" id="Seg_11686" s="T415">v</ta>
            <ta e="T417" id="Seg_11687" s="T416">dempro</ta>
            <ta e="T418" id="Seg_11688" s="T417">n</ta>
            <ta e="T419" id="Seg_11689" s="T418">v</ta>
            <ta e="T420" id="Seg_11690" s="T419">adv</ta>
            <ta e="T421" id="Seg_11691" s="T420">v</ta>
            <ta e="T422" id="Seg_11692" s="T421">que</ta>
            <ta e="T423" id="Seg_11693" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_11694" s="T423">ptcl</ta>
            <ta e="T426" id="Seg_11695" s="T425">interj</ta>
            <ta e="T427" id="Seg_11696" s="T426">n</ta>
            <ta e="T428" id="Seg_11697" s="T427">adv</ta>
            <ta e="T429" id="Seg_11698" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_11699" s="T429">v</ta>
            <ta e="T431" id="Seg_11700" s="T430">adv</ta>
            <ta e="T432" id="Seg_11701" s="T431">v</ta>
            <ta e="T433" id="Seg_11702" s="T432">v</ta>
            <ta e="T434" id="Seg_11703" s="T433">aux</ta>
            <ta e="T435" id="Seg_11704" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_11705" s="T435">dempro</ta>
            <ta e="T437" id="Seg_11706" s="T436">v</ta>
            <ta e="T438" id="Seg_11707" s="T437">v</ta>
            <ta e="T439" id="Seg_11708" s="T438">v</ta>
            <ta e="T440" id="Seg_11709" s="T439">dempro</ta>
            <ta e="T441" id="Seg_11710" s="T440">dempro</ta>
            <ta e="T442" id="Seg_11711" s="T441">n</ta>
            <ta e="T443" id="Seg_11712" s="T442">post</ta>
            <ta e="T444" id="Seg_11713" s="T443">v</ta>
            <ta e="T445" id="Seg_11714" s="T444">n</ta>
            <ta e="T446" id="Seg_11715" s="T445">post</ta>
            <ta e="T447" id="Seg_11716" s="T446">v</ta>
            <ta e="T448" id="Seg_11717" s="T447">interj</ta>
            <ta e="T449" id="Seg_11718" s="T448">v</ta>
            <ta e="T450" id="Seg_11719" s="T449">interj</ta>
            <ta e="T451" id="Seg_11720" s="T450">v</ta>
            <ta e="T452" id="Seg_11721" s="T451">adv</ta>
            <ta e="T453" id="Seg_11722" s="T452">que</ta>
            <ta e="T454" id="Seg_11723" s="T453">v</ta>
            <ta e="T455" id="Seg_11724" s="T454">dempro</ta>
            <ta e="T456" id="Seg_11725" s="T455">n</ta>
            <ta e="T457" id="Seg_11726" s="T456">cop</ta>
            <ta e="T458" id="Seg_11727" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_11728" s="T458">n</ta>
            <ta e="T462" id="Seg_11729" s="T461">interj</ta>
            <ta e="T463" id="Seg_11730" s="T462">que</ta>
            <ta e="T464" id="Seg_11731" s="T463">n</ta>
            <ta e="T465" id="Seg_11732" s="T464">adv</ta>
            <ta e="T466" id="Seg_11733" s="T465">v</ta>
            <ta e="T467" id="Seg_11734" s="T466">adv</ta>
            <ta e="T468" id="Seg_11735" s="T467">v</ta>
            <ta e="T469" id="Seg_11736" s="T468">aux</ta>
            <ta e="T470" id="Seg_11737" s="T469">adv</ta>
            <ta e="T471" id="Seg_11738" s="T470">v</ta>
            <ta e="T472" id="Seg_11739" s="T471">adv</ta>
            <ta e="T473" id="Seg_11740" s="T472">cardnum</ta>
            <ta e="T474" id="Seg_11741" s="T473">n</ta>
            <ta e="T475" id="Seg_11742" s="T474">v</ta>
            <ta e="T8" id="Seg_11743" s="T475">adj</ta>
            <ta e="T476" id="Seg_11744" s="T8">adv</ta>
            <ta e="T477" id="Seg_11745" s="T476">que</ta>
            <ta e="T478" id="Seg_11746" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_11747" s="T478">adv</ta>
            <ta e="T480" id="Seg_11748" s="T479">v</ta>
            <ta e="T482" id="Seg_11749" s="T480">adv</ta>
            <ta e="T483" id="Seg_11750" s="T482">dempro</ta>
            <ta e="T484" id="Seg_11751" s="T483">adv</ta>
            <ta e="T485" id="Seg_11752" s="T484">n</ta>
            <ta e="T486" id="Seg_11753" s="T485">v</ta>
            <ta e="T487" id="Seg_11754" s="T486">n</ta>
            <ta e="T488" id="Seg_11755" s="T487">cop</ta>
            <ta e="T489" id="Seg_11756" s="T488">v</ta>
            <ta e="T490" id="Seg_11757" s="T489">aux</ta>
            <ta e="T491" id="Seg_11758" s="T490">pers</ta>
            <ta e="T492" id="Seg_11759" s="T491">n</ta>
            <ta e="T493" id="Seg_11760" s="T492">v</ta>
            <ta e="T494" id="Seg_11761" s="T493">v</ta>
            <ta e="T495" id="Seg_11762" s="T494">n</ta>
            <ta e="T496" id="Seg_11763" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_11764" s="T496">v</ta>
            <ta e="T498" id="Seg_11765" s="T497">adv</ta>
            <ta e="T499" id="Seg_11766" s="T498">v</ta>
            <ta e="T500" id="Seg_11767" s="T499">v</ta>
            <ta e="T501" id="Seg_11768" s="T500">adv</ta>
            <ta e="T502" id="Seg_11769" s="T501">v</ta>
            <ta e="T503" id="Seg_11770" s="T502">aux</ta>
            <ta e="T504" id="Seg_11771" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_11772" s="T504">dempro</ta>
            <ta e="T507" id="Seg_11773" s="T505">v</ta>
            <ta e="T508" id="Seg_11774" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_11775" s="T508">adv</ta>
            <ta e="T510" id="Seg_11776" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_11777" s="T510">interj</ta>
            <ta e="T512" id="Seg_11778" s="T511">n</ta>
            <ta e="T513" id="Seg_11779" s="T512">cardnum</ta>
            <ta e="T514" id="Seg_11780" s="T513">n</ta>
            <ta e="T515" id="Seg_11781" s="T514">v</ta>
            <ta e="T516" id="Seg_11782" s="T515">adv</ta>
            <ta e="T517" id="Seg_11783" s="T516">que</ta>
            <ta e="T518" id="Seg_11784" s="T517">cop</ta>
            <ta e="T519" id="Seg_11785" s="T518">n</ta>
            <ta e="T520" id="Seg_11786" s="T519">adv</ta>
            <ta e="T521" id="Seg_11787" s="T520">v</ta>
            <ta e="T522" id="Seg_11788" s="T521">v</ta>
            <ta e="T523" id="Seg_11789" s="T522">pers</ta>
            <ta e="T524" id="Seg_11790" s="T523">pers</ta>
            <ta e="T525" id="Seg_11791" s="T524">pers</ta>
            <ta e="T528" id="Seg_11792" s="T527">pers</ta>
            <ta e="T529" id="Seg_11793" s="T528">que</ta>
            <ta e="T530" id="Seg_11794" s="T529">v</ta>
            <ta e="T531" id="Seg_11795" s="T530">v</ta>
            <ta e="T532" id="Seg_11796" s="T531">pers</ta>
            <ta e="T533" id="Seg_11797" s="T532">interj</ta>
            <ta e="T534" id="Seg_11798" s="T533">ptcl</ta>
            <ta e="T535" id="Seg_11799" s="T534">n</ta>
            <ta e="T536" id="Seg_11800" s="T535">cop</ta>
            <ta e="T537" id="Seg_11801" s="T536">interj</ta>
            <ta e="T538" id="Seg_11802" s="T537">n</ta>
            <ta e="T539" id="Seg_11803" s="T538">cop</ta>
            <ta e="T540" id="Seg_11804" s="T539">v</ta>
            <ta e="T541" id="Seg_11805" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_11806" s="T541">v</ta>
            <ta e="T543" id="Seg_11807" s="T542">v</ta>
            <ta e="T544" id="Seg_11808" s="T543">adv</ta>
            <ta e="T545" id="Seg_11809" s="T544">n</ta>
            <ta e="T546" id="Seg_11810" s="T545">n</ta>
            <ta e="T547" id="Seg_11811" s="T546">post</ta>
            <ta e="T548" id="Seg_11812" s="T547">v</ta>
            <ta e="T549" id="Seg_11813" s="T548">adv</ta>
            <ta e="T550" id="Seg_11814" s="T549">dempro</ta>
            <ta e="T551" id="Seg_11815" s="T550">adv</ta>
            <ta e="T552" id="Seg_11816" s="T551">dempro</ta>
            <ta e="T553" id="Seg_11817" s="T552">v</ta>
            <ta e="T554" id="Seg_11818" s="T553">dempro</ta>
            <ta e="T555" id="Seg_11819" s="T554">v</ta>
            <ta e="T556" id="Seg_11820" s="T555">cardnum</ta>
            <ta e="T557" id="Seg_11821" s="T556">n</ta>
            <ta e="T558" id="Seg_11822" s="T557">que</ta>
            <ta e="T559" id="Seg_11823" s="T558">dempro</ta>
            <ta e="T560" id="Seg_11824" s="T559">adv</ta>
            <ta e="T561" id="Seg_11825" s="T560">v</ta>
            <ta e="T562" id="Seg_11826" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_11827" s="T562">dempro</ta>
            <ta e="T565" id="Seg_11828" s="T563">adv</ta>
            <ta e="T566" id="Seg_11829" s="T565">v</ta>
            <ta e="T567" id="Seg_11830" s="T566">interj</ta>
            <ta e="T568" id="Seg_11831" s="T567">v</ta>
            <ta e="T569" id="Seg_11832" s="T568">dempro</ta>
            <ta e="T570" id="Seg_11833" s="T569">adv</ta>
            <ta e="T571" id="Seg_11834" s="T570">adv</ta>
            <ta e="T572" id="Seg_11835" s="T571">v</ta>
            <ta e="T573" id="Seg_11836" s="T572">dempro</ta>
            <ta e="T574" id="Seg_11837" s="T573">adv</ta>
            <ta e="T575" id="Seg_11838" s="T574">n</ta>
            <ta e="T576" id="Seg_11839" s="T575">cop</ta>
            <ta e="T577" id="Seg_11840" s="T576">ptcl</ta>
            <ta e="T578" id="Seg_11841" s="T577">que</ta>
            <ta e="T579" id="Seg_11842" s="T578">pers</ta>
            <ta e="T580" id="Seg_11843" s="T579">post</ta>
            <ta e="T583" id="Seg_11844" s="T582">v</ta>
            <ta e="T584" id="Seg_11845" s="T583">v</ta>
            <ta e="T585" id="Seg_11846" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_11847" s="T585">adv</ta>
            <ta e="T587" id="Seg_11848" s="T586">n</ta>
            <ta e="T588" id="Seg_11849" s="T587">adv</ta>
            <ta e="T589" id="Seg_11850" s="T588">v</ta>
            <ta e="T590" id="Seg_11851" s="T589">v</ta>
            <ta e="T593" id="Seg_11852" s="T592">interj</ta>
            <ta e="T594" id="Seg_11853" s="T593">v</ta>
            <ta e="T595" id="Seg_11854" s="T594">aux</ta>
            <ta e="T596" id="Seg_11855" s="T595">que</ta>
            <ta e="T597" id="Seg_11856" s="T596">ptcl</ta>
            <ta e="T598" id="Seg_11857" s="T597">n</ta>
            <ta e="T599" id="Seg_11858" s="T598">v</ta>
            <ta e="T600" id="Seg_11859" s="T599">dempro</ta>
            <ta e="T601" id="Seg_11860" s="T600">v</ta>
            <ta e="T602" id="Seg_11861" s="T601">que</ta>
            <ta e="T603" id="Seg_11862" s="T602">n</ta>
            <ta e="T604" id="Seg_11863" s="T603">pers</ta>
            <ta e="T605" id="Seg_11864" s="T604">pers</ta>
            <ta e="T606" id="Seg_11865" s="T605">v</ta>
            <ta e="T607" id="Seg_11866" s="T606">v</ta>
            <ta e="T608" id="Seg_11867" s="T607">adv</ta>
            <ta e="T609" id="Seg_11868" s="T608">cardnum</ta>
            <ta e="T610" id="Seg_11869" s="T609">adv</ta>
            <ta e="T611" id="Seg_11870" s="T610">adv</ta>
            <ta e="T612" id="Seg_11871" s="T611">pers</ta>
            <ta e="T613" id="Seg_11872" s="T612">post</ta>
            <ta e="T614" id="Seg_11873" s="T613">v</ta>
            <ta e="T615" id="Seg_11874" s="T614">v</ta>
            <ta e="T616" id="Seg_11875" s="T615">n</ta>
            <ta e="T617" id="Seg_11876" s="T616">post</ta>
            <ta e="T619" id="Seg_11877" s="T618">ptcl</ta>
            <ta e="T622" id="Seg_11878" s="T619">v</ta>
            <ta e="T623" id="Seg_11879" s="T622">n</ta>
            <ta e="T624" id="Seg_11880" s="T623">v</ta>
            <ta e="T625" id="Seg_11881" s="T624">v</ta>
            <ta e="T626" id="Seg_11882" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_11883" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_11884" s="T627">adv</ta>
            <ta e="T629" id="Seg_11885" s="T628">dempro</ta>
            <ta e="T630" id="Seg_11886" s="T629">v</ta>
            <ta e="T631" id="Seg_11887" s="T630">v</ta>
            <ta e="T632" id="Seg_11888" s="T631">que</ta>
            <ta e="T633" id="Seg_11889" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_11890" s="T633">n</ta>
            <ta e="T635" id="Seg_11891" s="T634">dempro</ta>
            <ta e="T636" id="Seg_11892" s="T635">v</ta>
            <ta e="T637" id="Seg_11893" s="T636">dempro</ta>
            <ta e="T638" id="Seg_11894" s="T637">post</ta>
            <ta e="T639" id="Seg_11895" s="T638">n</ta>
            <ta e="T640" id="Seg_11896" s="T639">ptcl</ta>
            <ta e="T641" id="Seg_11897" s="T640">v</ta>
            <ta e="T642" id="Seg_11898" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_11899" s="T642">adv</ta>
            <ta e="T644" id="Seg_11900" s="T643">adj</ta>
            <ta e="T645" id="Seg_11901" s="T644">v</ta>
            <ta e="T646" id="Seg_11902" s="T645">interj</ta>
            <ta e="T647" id="Seg_11903" s="T646">v</ta>
            <ta e="T648" id="Seg_11904" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_11905" s="T648">ptcl</ta>
            <ta e="T652" id="Seg_11906" s="T651">ordnum</ta>
            <ta e="T653" id="Seg_11907" s="T652">n</ta>
            <ta e="T654" id="Seg_11908" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_11909" s="T654">ordnum</ta>
            <ta e="T656" id="Seg_11910" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_11911" s="T656">adv</ta>
            <ta e="T658" id="Seg_11912" s="T657">v</ta>
            <ta e="T659" id="Seg_11913" s="T658">adv</ta>
            <ta e="T660" id="Seg_11914" s="T659">que</ta>
            <ta e="T661" id="Seg_11915" s="T660">dempro</ta>
            <ta e="T662" id="Seg_11916" s="T661">n</ta>
            <ta e="T663" id="Seg_11917" s="T662">v</ta>
            <ta e="T664" id="Seg_11918" s="T663">ptcl</ta>
            <ta e="T665" id="Seg_11919" s="T664">adv</ta>
            <ta e="T666" id="Seg_11920" s="T665">dempro</ta>
            <ta e="T667" id="Seg_11921" s="T666">v</ta>
            <ta e="T668" id="Seg_11922" s="T667">dempro</ta>
            <ta e="T669" id="Seg_11923" s="T668">adj</ta>
            <ta e="T670" id="Seg_11924" s="T669">v</ta>
            <ta e="T671" id="Seg_11925" s="T670">conj</ta>
            <ta e="T672" id="Seg_11926" s="T671">pers</ta>
            <ta e="T673" id="Seg_11927" s="T672">n</ta>
            <ta e="T674" id="Seg_11928" s="T673">ptcl</ta>
            <ta e="T675" id="Seg_11929" s="T674">v</ta>
            <ta e="T676" id="Seg_11930" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_11931" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_11932" s="T677">dempro</ta>
            <ta e="T679" id="Seg_11933" s="T678">v</ta>
            <ta e="T680" id="Seg_11934" s="T679">conj</ta>
            <ta e="T681" id="Seg_11935" s="T680">adv</ta>
            <ta e="T682" id="Seg_11936" s="T681">que</ta>
            <ta e="T683" id="Seg_11937" s="T682">ptcl</ta>
            <ta e="T684" id="Seg_11938" s="T683">v</ta>
            <ta e="T685" id="Seg_11939" s="T684">v</ta>
            <ta e="T686" id="Seg_11940" s="T685">dempro</ta>
            <ta e="T687" id="Seg_11941" s="T686">adv</ta>
            <ta e="T688" id="Seg_11942" s="T687">n</ta>
            <ta e="T689" id="Seg_11943" s="T688">v</ta>
            <ta e="T690" id="Seg_11944" s="T689">dempro</ta>
            <ta e="T691" id="Seg_11945" s="T690">post</ta>
            <ta e="T692" id="Seg_11946" s="T691">n</ta>
            <ta e="T693" id="Seg_11947" s="T692">dempro</ta>
            <ta e="T696" id="Seg_11948" s="T695">n</ta>
            <ta e="T697" id="Seg_11949" s="T696">v</ta>
            <ta e="T698" id="Seg_11950" s="T697">ptcl</ta>
            <ta e="T699" id="Seg_11951" s="T698">adv</ta>
            <ta e="T700" id="Seg_11952" s="T699">cardnum</ta>
            <ta e="T701" id="Seg_11953" s="T700">n</ta>
            <ta e="T702" id="Seg_11954" s="T701">propr</ta>
            <ta e="T703" id="Seg_11955" s="T702">v</ta>
            <ta e="T704" id="Seg_11956" s="T703">ptcl</ta>
            <ta e="T705" id="Seg_11957" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_11958" s="T705">adv</ta>
            <ta e="T707" id="Seg_11959" s="T706">adv</ta>
            <ta e="T708" id="Seg_11960" s="T707">que</ta>
            <ta e="T709" id="Seg_11961" s="T708">ptcl</ta>
            <ta e="T710" id="Seg_11962" s="T709">n</ta>
            <ta e="T711" id="Seg_11963" s="T710">cop</ta>
            <ta e="T712" id="Seg_11964" s="T711">n</ta>
            <ta e="T715" id="Seg_11965" s="T714">n</ta>
            <ta e="T716" id="Seg_11966" s="T715">interj</ta>
            <ta e="T717" id="Seg_11967" s="T716">v</ta>
            <ta e="T718" id="Seg_11968" s="T717">conj</ta>
            <ta e="T719" id="Seg_11969" s="T718">ptcl</ta>
            <ta e="T720" id="Seg_11970" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_11971" s="T720">n</ta>
            <ta e="T724" id="Seg_11972" s="T723">n</ta>
            <ta e="T725" id="Seg_11973" s="T724">v</ta>
            <ta e="T726" id="Seg_11974" s="T725">ptcl</ta>
            <ta e="T729" id="Seg_11975" s="T728">n</ta>
            <ta e="T730" id="Seg_11976" s="T729">adv</ta>
            <ta e="T731" id="Seg_11977" s="T730">v</ta>
            <ta e="T733" id="Seg_11978" s="T732">v</ta>
            <ta e="T734" id="Seg_11979" s="T733">aux</ta>
            <ta e="T735" id="Seg_11980" s="T734">ptcl</ta>
            <ta e="T736" id="Seg_11981" s="T735">conj</ta>
            <ta e="T737" id="Seg_11982" s="T736">pers</ta>
            <ta e="T738" id="Seg_11983" s="T737">dempro</ta>
            <ta e="T739" id="Seg_11984" s="T738">n</ta>
            <ta e="T740" id="Seg_11985" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_11986" s="T740">v</ta>
            <ta e="T742" id="Seg_11987" s="T741">ptcl</ta>
            <ta e="T743" id="Seg_11988" s="T742">adv</ta>
            <ta e="T744" id="Seg_11989" s="T743">dempro</ta>
            <ta e="T745" id="Seg_11990" s="T744">n</ta>
            <ta e="T746" id="Seg_11991" s="T745">post</ta>
            <ta e="T747" id="Seg_11992" s="T746">v</ta>
            <ta e="T748" id="Seg_11993" s="T747">adv</ta>
            <ta e="T749" id="Seg_11994" s="T748">ptcl</ta>
            <ta e="T750" id="Seg_11995" s="T749">n</ta>
            <ta e="T751" id="Seg_11996" s="T750">cardnum</ta>
            <ta e="T752" id="Seg_11997" s="T751">ptcl</ta>
            <ta e="T753" id="Seg_11998" s="T752">cardnum</ta>
            <ta e="T754" id="Seg_11999" s="T753">post</ta>
            <ta e="T755" id="Seg_12000" s="T754">ptcl</ta>
            <ta e="T756" id="Seg_12001" s="T755">conj</ta>
            <ta e="T757" id="Seg_12002" s="T756">n</ta>
            <ta e="T758" id="Seg_12003" s="T757">post</ta>
            <ta e="T759" id="Seg_12004" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_12005" s="T759">dempro</ta>
            <ta e="T761" id="Seg_12006" s="T760">n</ta>
            <ta e="T762" id="Seg_12007" s="T761">v</ta>
            <ta e="T763" id="Seg_12008" s="T762">n</ta>
            <ta e="T764" id="Seg_12009" s="T763">v</ta>
            <ta e="T765" id="Seg_12010" s="T764">adv</ta>
            <ta e="T766" id="Seg_12011" s="T765">dempro</ta>
            <ta e="T767" id="Seg_12012" s="T766">adv</ta>
            <ta e="T769" id="Seg_12013" s="T768">v</ta>
            <ta e="T770" id="Seg_12014" s="T769">v</ta>
            <ta e="T771" id="Seg_12015" s="T770">adv</ta>
            <ta e="T772" id="Seg_12016" s="T771">v</ta>
            <ta e="T773" id="Seg_12017" s="T772">n</ta>
            <ta e="T774" id="Seg_12018" s="T773">v</ta>
            <ta e="T775" id="Seg_12019" s="T774">dempro</ta>
            <ta e="T776" id="Seg_12020" s="T775">v</ta>
            <ta e="T777" id="Seg_12021" s="T776">adv</ta>
            <ta e="T778" id="Seg_12022" s="T777">dempro</ta>
            <ta e="T779" id="Seg_12023" s="T778">dempro</ta>
            <ta e="T780" id="Seg_12024" s="T779">v</ta>
            <ta e="T781" id="Seg_12025" s="T780">v</ta>
            <ta e="T782" id="Seg_12026" s="T781">dempro</ta>
            <ta e="T783" id="Seg_12027" s="T782">adv</ta>
            <ta e="T784" id="Seg_12028" s="T783">v</ta>
            <ta e="T785" id="Seg_12029" s="T784">cardnum</ta>
            <ta e="T786" id="Seg_12030" s="T785">n</ta>
            <ta e="T787" id="Seg_12031" s="T786">v</ta>
            <ta e="T788" id="Seg_12032" s="T787">conj</ta>
            <ta e="T789" id="Seg_12033" s="T788">adj</ta>
            <ta e="T790" id="Seg_12034" s="T789">v</ta>
            <ta e="T791" id="Seg_12035" s="T790">dempro</ta>
            <ta e="T792" id="Seg_12036" s="T791">cardnum</ta>
            <ta e="T793" id="Seg_12037" s="T792">post</ta>
            <ta e="T794" id="Seg_12038" s="T793">n</ta>
            <ta e="T795" id="Seg_12039" s="T794">n</ta>
            <ta e="T796" id="Seg_12040" s="T795">adv</ta>
            <ta e="T797" id="Seg_12041" s="T796">v</ta>
            <ta e="T798" id="Seg_12042" s="T797">v</ta>
            <ta e="T799" id="Seg_12043" s="T798">adv</ta>
            <ta e="T800" id="Seg_12044" s="T799">adv</ta>
            <ta e="T801" id="Seg_12045" s="T800">adv</ta>
            <ta e="T802" id="Seg_12046" s="T801">v</ta>
            <ta e="T803" id="Seg_12047" s="T802">que</ta>
            <ta e="T804" id="Seg_12048" s="T803">adv</ta>
            <ta e="T805" id="Seg_12049" s="T804">ptcl</ta>
            <ta e="T806" id="Seg_12050" s="T805">adv</ta>
            <ta e="T807" id="Seg_12051" s="T806">v</ta>
            <ta e="T808" id="Seg_12052" s="T807">adv</ta>
            <ta e="T809" id="Seg_12053" s="T808">ptcl</ta>
            <ta e="T810" id="Seg_12054" s="T809">adv</ta>
            <ta e="T811" id="Seg_12055" s="T810">adv</ta>
            <ta e="T812" id="Seg_12056" s="T811">v</ta>
            <ta e="T813" id="Seg_12057" s="T812">ptcl</ta>
            <ta e="T814" id="Seg_12058" s="T813">n</ta>
            <ta e="T815" id="Seg_12059" s="T814">adv</ta>
            <ta e="T816" id="Seg_12060" s="T815">v</ta>
            <ta e="T817" id="Seg_12061" s="T816">aux</ta>
            <ta e="T818" id="Seg_12062" s="T817">n</ta>
            <ta e="T819" id="Seg_12063" s="T818">v</ta>
            <ta e="T820" id="Seg_12064" s="T819">aux</ta>
            <ta e="T821" id="Seg_12065" s="T820">adv</ta>
            <ta e="T822" id="Seg_12066" s="T821">ptcl</ta>
            <ta e="T823" id="Seg_12067" s="T822">adv</ta>
            <ta e="T824" id="Seg_12068" s="T823">n</ta>
            <ta e="T825" id="Seg_12069" s="T824">v</ta>
            <ta e="T826" id="Seg_12070" s="T825">adv</ta>
            <ta e="T827" id="Seg_12071" s="T826">n</ta>
            <ta e="T828" id="Seg_12072" s="T827">v</ta>
            <ta e="T829" id="Seg_12073" s="T828">aux</ta>
            <ta e="T830" id="Seg_12074" s="T829">adv</ta>
            <ta e="T831" id="Seg_12075" s="T830">v</ta>
            <ta e="T832" id="Seg_12076" s="T831">ptcl</ta>
            <ta e="T833" id="Seg_12077" s="T832">que</ta>
            <ta e="T834" id="Seg_12078" s="T833">n</ta>
            <ta e="T835" id="Seg_12079" s="T834">adv</ta>
            <ta e="T838" id="Seg_12080" s="T837">v</ta>
            <ta e="T839" id="Seg_12081" s="T838">dempro</ta>
            <ta e="T840" id="Seg_12082" s="T839">v</ta>
            <ta e="T841" id="Seg_12083" s="T840">aux</ta>
            <ta e="T842" id="Seg_12084" s="T841">adv</ta>
            <ta e="T843" id="Seg_12085" s="T842">dempro</ta>
            <ta e="T844" id="Seg_12086" s="T843">n</ta>
            <ta e="T845" id="Seg_12087" s="T844">cop</ta>
            <ta e="T848" id="Seg_12088" s="T847">ptcl</ta>
            <ta e="T849" id="Seg_12089" s="T848">dempro</ta>
            <ta e="T850" id="Seg_12090" s="T849">v</ta>
            <ta e="T851" id="Seg_12091" s="T850">adv</ta>
            <ta e="T852" id="Seg_12092" s="T851">adv</ta>
            <ta e="T853" id="Seg_12093" s="T852">n</ta>
            <ta e="T854" id="Seg_12094" s="T853">dempro</ta>
            <ta e="T855" id="Seg_12095" s="T854">n</ta>
            <ta e="T856" id="Seg_12096" s="T855">que</ta>
            <ta e="T857" id="Seg_12097" s="T856">cop</ta>
            <ta e="T860" id="Seg_12098" s="T859">dempro</ta>
            <ta e="T861" id="Seg_12099" s="T860">n</ta>
            <ta e="T862" id="Seg_12100" s="T861">v</ta>
            <ta e="T863" id="Seg_12101" s="T862">v</ta>
            <ta e="T864" id="Seg_12102" s="T863">v</ta>
            <ta e="T865" id="Seg_12103" s="T864">que</ta>
            <ta e="T866" id="Seg_12104" s="T865">n</ta>
            <ta e="T867" id="Seg_12105" s="T866">n</ta>
            <ta e="T868" id="Seg_12106" s="T867">dempro</ta>
            <ta e="T869" id="Seg_12107" s="T868">v</ta>
            <ta e="T870" id="Seg_12108" s="T869">v</ta>
            <ta e="T871" id="Seg_12109" s="T870">v</ta>
            <ta e="T872" id="Seg_12110" s="T871">dempro</ta>
            <ta e="T873" id="Seg_12111" s="T872">n</ta>
            <ta e="T874" id="Seg_12112" s="T873">v</ta>
            <ta e="T875" id="Seg_12113" s="T874">interj</ta>
            <ta e="T878" id="Seg_12114" s="T877">n</ta>
            <ta e="T879" id="Seg_12115" s="T878">interj</ta>
            <ta e="T880" id="Seg_12116" s="T879">v</ta>
            <ta e="T881" id="Seg_12117" s="T880">v</ta>
            <ta e="T882" id="Seg_12118" s="T881">v</ta>
            <ta e="T883" id="Seg_12119" s="T930">ptcl</ta>
            <ta e="T885" id="Seg_12120" s="T883">v</ta>
            <ta e="T886" id="Seg_12121" s="T885">interj</ta>
            <ta e="T887" id="Seg_12122" s="T886">v</ta>
            <ta e="T888" id="Seg_12123" s="T887">n</ta>
            <ta e="T889" id="Seg_12124" s="T888">adv</ta>
            <ta e="T890" id="Seg_12125" s="T889">v</ta>
            <ta e="T891" id="Seg_12126" s="T890">aux</ta>
            <ta e="T892" id="Seg_12127" s="T891">dempro</ta>
            <ta e="T893" id="Seg_12128" s="T892">que</ta>
            <ta e="T894" id="Seg_12129" s="T893">n</ta>
            <ta e="T895" id="Seg_12130" s="T894">v</ta>
            <ta e="T896" id="Seg_12131" s="T895">ptcl</ta>
            <ta e="T897" id="Seg_12132" s="T896">que</ta>
            <ta e="T898" id="Seg_12133" s="T897">n</ta>
            <ta e="T899" id="Seg_12134" s="T898">cop</ta>
            <ta e="T900" id="Seg_12135" s="T899">aux</ta>
            <ta e="T901" id="Seg_12136" s="T900">dempro</ta>
            <ta e="T902" id="Seg_12137" s="T901">ptcl</ta>
            <ta e="T903" id="Seg_12138" s="T902">que</ta>
            <ta e="T904" id="Seg_12139" s="T903">n</ta>
            <ta e="T905" id="Seg_12140" s="T904">n</ta>
            <ta e="T906" id="Seg_12141" s="T905">que</ta>
            <ta e="T907" id="Seg_12142" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_12143" s="T907">ptcl</ta>
            <ta e="T909" id="Seg_12144" s="T908">n</ta>
            <ta e="T910" id="Seg_12145" s="T909">ptcl</ta>
            <ta e="T911" id="Seg_12146" s="T910">v</ta>
            <ta e="T912" id="Seg_12147" s="T911">n</ta>
            <ta e="T913" id="Seg_12148" s="T912">v</ta>
            <ta e="T914" id="Seg_12149" s="T913">n</ta>
            <ta e="T915" id="Seg_12150" s="T914">v</ta>
            <ta e="T916" id="Seg_12151" s="T915">ptcl</ta>
            <ta e="T917" id="Seg_12152" s="T916">dempro</ta>
            <ta e="T918" id="Seg_12153" s="T917">n</ta>
            <ta e="T919" id="Seg_12154" s="T918">v</ta>
            <ta e="T920" id="Seg_12155" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_12156" s="T920">n</ta>
            <ta e="T922" id="Seg_12157" s="T921">v</ta>
            <ta e="T923" id="Seg_12158" s="T922">n</ta>
            <ta e="T924" id="Seg_12159" s="T923">v</ta>
            <ta e="T929" id="Seg_12160" s="T928">v</ta>
            <ta e="T931" id="Seg_12161" s="T929">ptcl</ta>
            <ta e="T932" id="Seg_12162" s="T931">dempro</ta>
            <ta e="T933" id="Seg_12163" s="T932">dempro</ta>
            <ta e="T934" id="Seg_12164" s="T933">n</ta>
            <ta e="T935" id="Seg_12165" s="T937">n</ta>
            <ta e="T936" id="Seg_12166" s="T935">v</ta>
            <ta e="T938" id="Seg_12167" s="T936">aux</ta>
            <ta e="T939" id="Seg_12168" s="T938">pers</ta>
            <ta e="T940" id="Seg_12169" s="T939">n</ta>
            <ta e="T941" id="Seg_12170" s="T940">adv</ta>
            <ta e="T942" id="Seg_12171" s="T941">n</ta>
            <ta e="T943" id="Seg_12172" s="T942">v</ta>
            <ta e="T944" id="Seg_12173" s="T943">cop</ta>
            <ta e="T945" id="Seg_12174" s="T944">n</ta>
            <ta e="T946" id="Seg_12175" s="T945">cop</ta>
            <ta e="T947" id="Seg_12176" s="T946">ptcl</ta>
            <ta e="T948" id="Seg_12177" s="T947">adv</ta>
            <ta e="T949" id="Seg_12178" s="T948">v</ta>
            <ta e="T950" id="Seg_12179" s="T949">cop</ta>
            <ta e="T951" id="Seg_12180" s="T950">ptcl</ta>
            <ta e="T952" id="Seg_12181" s="T951">indfpro</ta>
            <ta e="T953" id="Seg_12182" s="T952">v</ta>
            <ta e="T954" id="Seg_12183" s="T953">aux</ta>
            <ta e="T955" id="Seg_12184" s="T954">v</ta>
            <ta e="T956" id="Seg_12185" s="T955">aux</ta>
            <ta e="T957" id="Seg_12186" s="T956">pers</ta>
            <ta e="T958" id="Seg_12187" s="T957">ptcl</ta>
            <ta e="T959" id="Seg_12188" s="T958">ptcl</ta>
            <ta e="T960" id="Seg_12189" s="T959">v</ta>
            <ta e="T961" id="Seg_12190" s="T960">ptcl</ta>
            <ta e="T962" id="Seg_12191" s="T961">adv</ta>
            <ta e="T963" id="Seg_12192" s="T962">ptcl</ta>
            <ta e="T964" id="Seg_12193" s="T963">n</ta>
            <ta e="T965" id="Seg_12194" s="T964">v</ta>
            <ta e="T966" id="Seg_12195" s="T965">v</ta>
            <ta e="T967" id="Seg_12196" s="T966">n</ta>
            <ta e="T968" id="Seg_12197" s="T967">v</ta>
            <ta e="T971" id="Seg_12198" s="T970">adj</ta>
            <ta e="T972" id="Seg_12199" s="T971">n</ta>
            <ta e="T973" id="Seg_12200" s="T972">que</ta>
            <ta e="T974" id="Seg_12201" s="T973">cop</ta>
            <ta e="T975" id="Seg_12202" s="T974">n</ta>
            <ta e="T976" id="Seg_12203" s="T975">dempro</ta>
            <ta e="T977" id="Seg_12204" s="T976">n</ta>
            <ta e="T978" id="Seg_12205" s="T977">v</ta>
            <ta e="T979" id="Seg_12206" s="T978">interj</ta>
            <ta e="T980" id="Seg_12207" s="T979">v</ta>
            <ta e="T981" id="Seg_12208" s="T980">conj</ta>
            <ta e="T982" id="Seg_12209" s="T981">n</ta>
            <ta e="T983" id="Seg_12210" s="T982">v</ta>
            <ta e="T984" id="Seg_12211" s="T983">propr</ta>
            <ta e="T985" id="Seg_12212" s="T984">v</ta>
            <ta e="T986" id="Seg_12213" s="T985">adj</ta>
            <ta e="T987" id="Seg_12214" s="T986">n</ta>
            <ta e="T988" id="Seg_12215" s="T987">adv</ta>
            <ta e="T991" id="Seg_12216" s="T990">interj</ta>
            <ta e="T992" id="Seg_12217" s="T991">adv</ta>
            <ta e="T993" id="Seg_12218" s="T992">cardnum</ta>
            <ta e="T994" id="Seg_12219" s="T993">n</ta>
            <ta e="T995" id="Seg_12220" s="T994">v</ta>
            <ta e="T996" id="Seg_12221" s="T995">conj</ta>
            <ta e="T997" id="Seg_12222" s="T996">n</ta>
            <ta e="T998" id="Seg_12223" s="T997">post</ta>
            <ta e="T999" id="Seg_12224" s="T998">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-ChVD">
            <ta e="T1" id="Seg_12225" s="T0">np:G</ta>
            <ta e="T2" id="Seg_12226" s="T1">0.1.h:A</ta>
            <ta e="T7" id="Seg_12227" s="T6">n:Time</ta>
            <ta e="T11" id="Seg_12228" s="T10">np:Th</ta>
            <ta e="T13" id="Seg_12229" s="T11">0.1.h:A</ta>
            <ta e="T15" id="Seg_12230" s="T14">np:G</ta>
            <ta e="T16" id="Seg_12231" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_12232" s="T16">np:So</ta>
            <ta e="T18" id="Seg_12233" s="T17">pro:L</ta>
            <ta e="T20" id="Seg_12234" s="T19">np:L</ta>
            <ta e="T22" id="Seg_12235" s="T20">0.1.h:A</ta>
            <ta e="T24" id="Seg_12236" s="T22">pp:Com</ta>
            <ta e="T27" id="Seg_12237" s="T26">0.1.h:Th</ta>
            <ta e="T28" id="Seg_12238" s="T27">np:G</ta>
            <ta e="T31" id="Seg_12239" s="T30">0.1.h:A</ta>
            <ta e="T33" id="Seg_12240" s="T32">np:Ins</ta>
            <ta e="T34" id="Seg_12241" s="T33">adv:Time</ta>
            <ta e="T35" id="Seg_12242" s="T34">0.1.h:A</ta>
            <ta e="T37" id="Seg_12243" s="T36">np:L</ta>
            <ta e="T43" id="Seg_12244" s="T42">n:Time</ta>
            <ta e="T45" id="Seg_12245" s="T44">pro:Time</ta>
            <ta e="T51" id="Seg_12246" s="T54">pro:L</ta>
            <ta e="T52" id="Seg_12247" s="T51">np:L</ta>
            <ta e="T56" id="Seg_12248" s="T55">np:P</ta>
            <ta e="T58" id="Seg_12249" s="T56">0.1.h:A</ta>
            <ta e="T61" id="Seg_12250" s="T60">adv:L</ta>
            <ta e="T64" id="Seg_12251" s="T63">0.1.h:A</ta>
            <ta e="T66" id="Seg_12252" s="T64">0.1.h:Poss pp:Com</ta>
            <ta e="T68" id="Seg_12253" s="T67">0.1.h:Th</ta>
            <ta e="T70" id="Seg_12254" s="T69">0.1.h:A</ta>
            <ta e="T73" id="Seg_12255" s="T72">0.1.h:A</ta>
            <ta e="T74" id="Seg_12256" s="T73">pro:L</ta>
            <ta e="T75" id="Seg_12257" s="T74">np:L</ta>
            <ta e="T77" id="Seg_12258" s="T76">0.1.h:A</ta>
            <ta e="T82" id="Seg_12259" s="T481">0.3.h:A</ta>
            <ta e="T83" id="Seg_12260" s="T82">adv:L</ta>
            <ta e="T84" id="Seg_12261" s="T83">np:Th</ta>
            <ta e="T86" id="Seg_12262" s="T84">0.3.h:A</ta>
            <ta e="T96" id="Seg_12263" s="T95">0.3.h:A</ta>
            <ta e="T105" id="Seg_12264" s="T104">np:So</ta>
            <ta e="T106" id="Seg_12265" s="T105">pro:G</ta>
            <ta e="T107" id="Seg_12266" s="T106">0.1.h:Th</ta>
            <ta e="T109" id="Seg_12267" s="T108">np:G</ta>
            <ta e="T111" id="Seg_12268" s="T110">np:P</ta>
            <ta e="T124" id="Seg_12269" s="T123">np:Th</ta>
            <ta e="T129" id="Seg_12270" s="T128">adv:L</ta>
            <ta e="T140" id="Seg_12271" s="T138">0.3.h:A</ta>
            <ta e="T141" id="Seg_12272" s="T140">n:Time</ta>
            <ta e="T152" id="Seg_12273" s="T151">adv:G</ta>
            <ta e="T155" id="Seg_12274" s="T154">0.1.h:A</ta>
            <ta e="T157" id="Seg_12275" s="T156">pro:G</ta>
            <ta e="T166" id="Seg_12276" s="T164">pp:G</ta>
            <ta e="T179" id="Seg_12277" s="T178">0.3.h:A</ta>
            <ta e="T189" id="Seg_12278" s="T188">np:Th</ta>
            <ta e="T192" id="Seg_12279" s="T191">adv:L</ta>
            <ta e="T193" id="Seg_12280" s="T192">np:L</ta>
            <ta e="T196" id="Seg_12281" s="T195">0.3.h:A</ta>
            <ta e="T198" id="Seg_12282" s="T197">adv:Time</ta>
            <ta e="T203" id="Seg_12283" s="T202">0.1.h:A</ta>
            <ta e="T206" id="Seg_12284" s="T205">0.3.h:A</ta>
            <ta e="T207" id="Seg_12285" s="T206">0.1.h:Poss np.h:R</ta>
            <ta e="T208" id="Seg_12286" s="T207">0.3.h:A</ta>
            <ta e="T217" id="Seg_12287" s="T216">0.2.h:Th</ta>
            <ta e="T220" id="Seg_12288" s="T219">0.2.h:Th</ta>
            <ta e="T227" id="Seg_12289" s="T226">pro.h:Th</ta>
            <ta e="T228" id="Seg_12290" s="T227">0.2.h:A</ta>
            <ta e="T233" id="Seg_12291" s="T232">0.2.h:A</ta>
            <ta e="T236" id="Seg_12292" s="T235">0.2.h:Poss np:P</ta>
            <ta e="T237" id="Seg_12293" s="T236">0.3.h:A</ta>
            <ta e="T245" id="Seg_12294" s="T244">0.2.h:A</ta>
            <ta e="T247" id="Seg_12295" s="T246">pro.h:A</ta>
            <ta e="T250" id="Seg_12296" s="T249">0.3.h:A</ta>
            <ta e="T252" id="Seg_12297" s="T251">0.2.h:A</ta>
            <ta e="T258" id="Seg_12298" s="T257">0.3.h:A</ta>
            <ta e="T261" id="Seg_12299" s="T260">0.3.h:E</ta>
            <ta e="T265" id="Seg_12300" s="T264">np:Th</ta>
            <ta e="T267" id="Seg_12301" s="T266">0.1.h:A</ta>
            <ta e="T268" id="Seg_12302" s="T267">adv:Time</ta>
            <ta e="T271" id="Seg_12303" s="T270">pro.h:St</ta>
            <ta e="T273" id="Seg_12304" s="T272">0.1.h:E</ta>
            <ta e="T281" id="Seg_12305" s="T280">pro.h:E</ta>
            <ta e="T282" id="Seg_12306" s="T281">0.1.h:A</ta>
            <ta e="T285" id="Seg_12307" s="T284">np.h:A</ta>
            <ta e="T292" id="Seg_12308" s="T291">np:So</ta>
            <ta e="T293" id="Seg_12309" s="T292">np.h:A</ta>
            <ta e="T295" id="Seg_12310" s="T294">np:So</ta>
            <ta e="T296" id="Seg_12311" s="T295">0.3.h:A</ta>
            <ta e="T298" id="Seg_12312" s="T297">0.1.h:A</ta>
            <ta e="T305" id="Seg_12313" s="T304">np.h:Poss</ta>
            <ta e="T309" id="Seg_12314" s="T308">np:So</ta>
            <ta e="T310" id="Seg_12315" s="T309">pro.h:A</ta>
            <ta e="T313" id="Seg_12316" s="T312">0.1.h:Poss np.h:Th</ta>
            <ta e="T322" id="Seg_12317" s="T321">0.2.h:A</ta>
            <ta e="T323" id="Seg_12318" s="T322">0.3.h:A</ta>
            <ta e="T328" id="Seg_12319" s="T327">np.h:A</ta>
            <ta e="T330" id="Seg_12320" s="T329">0.1.h:E</ta>
            <ta e="T332" id="Seg_12321" s="T331">np.h:A</ta>
            <ta e="T336" id="Seg_12322" s="T335">pro:Th</ta>
            <ta e="T337" id="Seg_12323" s="T336">pro:Th</ta>
            <ta e="T338" id="Seg_12324" s="T337">0.3.h:A</ta>
            <ta e="T339" id="Seg_12325" s="T338">0.3.h:A</ta>
            <ta e="T341" id="Seg_12326" s="T340">0.1.h:A</ta>
            <ta e="T342" id="Seg_12327" s="T341">0.1.h:Poss</ta>
            <ta e="T345" id="Seg_12328" s="T343">pp:G</ta>
            <ta e="T347" id="Seg_12329" s="T346">0.3.h:A</ta>
            <ta e="T349" id="Seg_12330" s="T348">adv:L</ta>
            <ta e="T350" id="Seg_12331" s="T349">np:L</ta>
            <ta e="T351" id="Seg_12332" s="T350">0.3.h:A</ta>
            <ta e="T354" id="Seg_12333" s="T353">0.3.h:E</ta>
            <ta e="T360" id="Seg_12334" s="T359">n:Time</ta>
            <ta e="T361" id="Seg_12335" s="T360">np:G</ta>
            <ta e="T362" id="Seg_12336" s="T361">0.1.h:Th</ta>
            <ta e="T363" id="Seg_12337" s="T362">0.1.h:Th</ta>
            <ta e="T364" id="Seg_12338" s="T363">pro.h:Poss</ta>
            <ta e="T367" id="Seg_12339" s="T366">np:G</ta>
            <ta e="T368" id="Seg_12340" s="T367">0.1.h:Th</ta>
            <ta e="T375" id="Seg_12341" s="T374">np:Ins</ta>
            <ta e="T380" id="Seg_12342" s="T379">np.h:P</ta>
            <ta e="T381" id="Seg_12343" s="T380">0.3.h:A</ta>
            <ta e="T382" id="Seg_12344" s="T381">pro.h:A</ta>
            <ta e="T386" id="Seg_12345" s="T385">np:Th</ta>
            <ta e="T390" id="Seg_12346" s="T389">np.h:A</ta>
            <ta e="T393" id="Seg_12347" s="T392">pro.h:A</ta>
            <ta e="T398" id="Seg_12348" s="T397">np:Th</ta>
            <ta e="T400" id="Seg_12349" s="T399">0.2.h:A</ta>
            <ta e="T402" id="Seg_12350" s="T401">np.h:Th</ta>
            <ta e="T407" id="Seg_12351" s="T405">pp:L</ta>
            <ta e="T409" id="Seg_12352" s="T407">pp:L</ta>
            <ta e="T415" id="Seg_12353" s="T414">0.1.h:Th</ta>
            <ta e="T416" id="Seg_12354" s="T415">0.3.h:A</ta>
            <ta e="T418" id="Seg_12355" s="T417">np:L</ta>
            <ta e="T419" id="Seg_12356" s="T418">0.1.h:A</ta>
            <ta e="T422" id="Seg_12357" s="T421">pro.h:Th</ta>
            <ta e="T427" id="Seg_12358" s="T426">np.h:Th</ta>
            <ta e="T428" id="Seg_12359" s="T427">adv:L</ta>
            <ta e="T432" id="Seg_12360" s="T431">0.1.h:A</ta>
            <ta e="T434" id="Seg_12361" s="T432">0.3.h:A</ta>
            <ta e="T438" id="Seg_12362" s="T437">0.1.h:A</ta>
            <ta e="T439" id="Seg_12363" s="T438">0.1.h:A</ta>
            <ta e="T443" id="Seg_12364" s="T441">pp:Path</ta>
            <ta e="T444" id="Seg_12365" s="T443">0.1.h:A</ta>
            <ta e="T446" id="Seg_12366" s="T444">pp:Path</ta>
            <ta e="T449" id="Seg_12367" s="T448">0.1.h:A</ta>
            <ta e="T456" id="Seg_12368" s="T455">np:Th</ta>
            <ta e="T469" id="Seg_12369" s="T467">0.3.h:A</ta>
            <ta e="T471" id="Seg_12370" s="T470">0.3.h:A</ta>
            <ta e="T472" id="Seg_12371" s="T471">adv:L</ta>
            <ta e="T474" id="Seg_12372" s="T473">np.h:Th</ta>
            <ta e="T475" id="Seg_12373" s="T474">0.1.h:E</ta>
            <ta e="T8" id="Seg_12374" s="T475">0.3.h:Th</ta>
            <ta e="T480" id="Seg_12375" s="T479">0.3.h:A</ta>
            <ta e="T487" id="Seg_12376" s="T486">np:Th</ta>
            <ta e="T490" id="Seg_12377" s="T488">0.3.h:A</ta>
            <ta e="T491" id="Seg_12378" s="T490">pro.h:Poss</ta>
            <ta e="T494" id="Seg_12379" s="T493">0.3.h:A</ta>
            <ta e="T497" id="Seg_12380" s="T496">0.3.h:A</ta>
            <ta e="T498" id="Seg_12381" s="T497">adv:Time</ta>
            <ta e="T499" id="Seg_12382" s="T498">0.3.h:A</ta>
            <ta e="T500" id="Seg_12383" s="T499">0.2.h:A</ta>
            <ta e="T503" id="Seg_12384" s="T501">0.3.h:A</ta>
            <ta e="T514" id="Seg_12385" s="T513">np.h:Th</ta>
            <ta e="T515" id="Seg_12386" s="T514">0.3.h:A</ta>
            <ta e="T516" id="Seg_12387" s="T515">adv:L</ta>
            <ta e="T517" id="Seg_12388" s="T516">pro:Th</ta>
            <ta e="T520" id="Seg_12389" s="T519">adv:Time</ta>
            <ta e="T521" id="Seg_12390" s="T520">0.1.h:A</ta>
            <ta e="T522" id="Seg_12391" s="T521">0.3.h:A</ta>
            <ta e="T524" id="Seg_12392" s="T523">pro.h:A</ta>
            <ta e="T525" id="Seg_12393" s="T524">pro.h:Th</ta>
            <ta e="T528" id="Seg_12394" s="T527">pro.h:Th</ta>
            <ta e="T530" id="Seg_12395" s="T529">0.2.h:E</ta>
            <ta e="T531" id="Seg_12396" s="T530">0.3.h:A</ta>
            <ta e="T532" id="Seg_12397" s="T531">pro.h:P</ta>
            <ta e="T540" id="Seg_12398" s="T539">0.3.h:A</ta>
            <ta e="T543" id="Seg_12399" s="T542">0.1.h:P</ta>
            <ta e="T544" id="Seg_12400" s="T543">adv:Time</ta>
            <ta e="T545" id="Seg_12401" s="T544">np.h:Th</ta>
            <ta e="T554" id="Seg_12402" s="T553">pro.h:Th</ta>
            <ta e="T557" id="Seg_12403" s="T556">np.h:A</ta>
            <ta e="T558" id="Seg_12404" s="T557">pro:G</ta>
            <ta e="T559" id="Seg_12405" s="T558">pro.h:A</ta>
            <ta e="T566" id="Seg_12406" s="T565">0.2.h:A</ta>
            <ta e="T570" id="Seg_12407" s="T569">adv:Time</ta>
            <ta e="T573" id="Seg_12408" s="T572">pro.h:A</ta>
            <ta e="T576" id="Seg_12409" s="T575">0.1.h:Th</ta>
            <ta e="T584" id="Seg_12410" s="T583">0.3.h:A</ta>
            <ta e="T586" id="Seg_12411" s="T585">adv:Time</ta>
            <ta e="T587" id="Seg_12412" s="T586">np:Th</ta>
            <ta e="T598" id="Seg_12413" s="T597">np.h:Th</ta>
            <ta e="T599" id="Seg_12414" s="T598">0.1.h:E</ta>
            <ta e="T600" id="Seg_12415" s="T599">pro.h:A</ta>
            <ta e="T603" id="Seg_12416" s="T602">0.2.h:Th np:So</ta>
            <ta e="T605" id="Seg_12417" s="T604">pro:So</ta>
            <ta e="T606" id="Seg_12418" s="T605">0.1.h:A</ta>
            <ta e="T609" id="Seg_12419" s="T608">np.h:A</ta>
            <ta e="T610" id="Seg_12420" s="T609">adv:Time</ta>
            <ta e="T613" id="Seg_12421" s="T611">pp:Com</ta>
            <ta e="T614" id="Seg_12422" s="T613">0.2.h:A</ta>
            <ta e="T615" id="Seg_12423" s="T614">0.3.h:A</ta>
            <ta e="T617" id="Seg_12424" s="T615">pp:G</ta>
            <ta e="T628" id="Seg_12425" s="T627">adv:Time</ta>
            <ta e="T631" id="Seg_12426" s="T630">0.1.h:A</ta>
            <ta e="T634" id="Seg_12427" s="T633">n:Time</ta>
            <ta e="T636" id="Seg_12428" s="T635">0.1.h:A</ta>
            <ta e="T639" id="Seg_12429" s="T638">0.1.h:Poss np.h:Th</ta>
            <ta e="T641" id="Seg_12430" s="T640">0.1.h:E</ta>
            <ta e="T644" id="Seg_12431" s="T643">n:Time</ta>
            <ta e="T647" id="Seg_12432" s="T646">0.1.h:E</ta>
            <ta e="T653" id="Seg_12433" s="T652">n:Time</ta>
            <ta e="T655" id="Seg_12434" s="T654">n:Time</ta>
            <ta e="T658" id="Seg_12435" s="T657">0.1.h:A</ta>
            <ta e="T659" id="Seg_12436" s="T658">adv:G</ta>
            <ta e="T660" id="Seg_12437" s="T659">pro:G</ta>
            <ta e="T662" id="Seg_12438" s="T661">np:Th</ta>
            <ta e="T663" id="Seg_12439" s="T662">0.1.h:A</ta>
            <ta e="T665" id="Seg_12440" s="T664">adv:Time</ta>
            <ta e="T667" id="Seg_12441" s="T666">0.1.h:A</ta>
            <ta e="T669" id="Seg_12442" s="T668">np:So</ta>
            <ta e="T670" id="Seg_12443" s="T669">0.3.h:A</ta>
            <ta e="T672" id="Seg_12444" s="T671">pro.h:A</ta>
            <ta e="T673" id="Seg_12445" s="T672">np:So</ta>
            <ta e="T685" id="Seg_12446" s="T684">0.3.h:A</ta>
            <ta e="T687" id="Seg_12447" s="T686">adv:Time</ta>
            <ta e="T692" id="Seg_12448" s="T691">np:So</ta>
            <ta e="T696" id="Seg_12449" s="T695">np:Th</ta>
            <ta e="T697" id="Seg_12450" s="T696">0.3.h:A</ta>
            <ta e="T701" id="Seg_12451" s="T700">n:Time</ta>
            <ta e="T702" id="Seg_12452" s="T701">np:Th</ta>
            <ta e="T703" id="Seg_12453" s="T702">0.2.h:A</ta>
            <ta e="T706" id="Seg_12454" s="T705">adv:L</ta>
            <ta e="T710" id="Seg_12455" s="T709">np:Th</ta>
            <ta e="T721" id="Seg_12456" s="T720">0.3.h:Poss</ta>
            <ta e="T724" id="Seg_12457" s="T723">np:So</ta>
            <ta e="T729" id="Seg_12458" s="T728">np.h:Th</ta>
            <ta e="T734" id="Seg_12459" s="T732">0.3.h:A</ta>
            <ta e="T737" id="Seg_12460" s="T736">pro.h:A</ta>
            <ta e="T739" id="Seg_12461" s="T738">np:So</ta>
            <ta e="T743" id="Seg_12462" s="T742">adv:Time</ta>
            <ta e="T746" id="Seg_12463" s="T744">pp:Time</ta>
            <ta e="T748" id="Seg_12464" s="T747">adv:L</ta>
            <ta e="T750" id="Seg_12465" s="T749">n:Time</ta>
            <ta e="T751" id="Seg_12466" s="T750">n:Time</ta>
            <ta e="T753" id="Seg_12467" s="T752">n:Time</ta>
            <ta e="T757" id="Seg_12468" s="T756">n:Time</ta>
            <ta e="T761" id="Seg_12469" s="T760">np:Th</ta>
            <ta e="T762" id="Seg_12470" s="T761">0.3.h:A</ta>
            <ta e="T763" id="Seg_12471" s="T762">np:Th</ta>
            <ta e="T764" id="Seg_12472" s="T763">0.3.h:A</ta>
            <ta e="T765" id="Seg_12473" s="T764">adv:Time</ta>
            <ta e="T770" id="Seg_12474" s="T769">np.h:Th</ta>
            <ta e="T773" id="Seg_12475" s="T772">0.1.h:Poss</ta>
            <ta e="T776" id="Seg_12476" s="T775">0.1.h:E</ta>
            <ta e="T779" id="Seg_12477" s="T778">pro:L</ta>
            <ta e="T786" id="Seg_12478" s="T785">np:Th</ta>
            <ta e="T789" id="Seg_12479" s="T788">np.h:P</ta>
            <ta e="T794" id="Seg_12480" s="T793">np.h:P</ta>
            <ta e="T795" id="Seg_12481" s="T794">0.1.h:Poss np:Th</ta>
            <ta e="T797" id="Seg_12482" s="T796">0.1.h:A</ta>
            <ta e="T798" id="Seg_12483" s="T797">0.1.h:E</ta>
            <ta e="T802" id="Seg_12484" s="T801">0.1.h:A</ta>
            <ta e="T807" id="Seg_12485" s="T806">0.1.h:P</ta>
            <ta e="T811" id="Seg_12486" s="T810">adv:Time</ta>
            <ta e="T812" id="Seg_12487" s="T811">0.1.h:E</ta>
            <ta e="T814" id="Seg_12488" s="T813">0.1.h:Poss np:Th</ta>
            <ta e="T818" id="Seg_12489" s="T817">0.1.h:Poss np:Th</ta>
            <ta e="T823" id="Seg_12490" s="T822">adv:Time</ta>
            <ta e="T824" id="Seg_12491" s="T823">0.1.h:Poss np:Th</ta>
            <ta e="T825" id="Seg_12492" s="T824">0.1.h:A</ta>
            <ta e="T827" id="Seg_12493" s="T826">0.1.h:Poss np:Th</ta>
            <ta e="T829" id="Seg_12494" s="T827">0.1.h:A</ta>
            <ta e="T834" id="Seg_12495" s="T833">0.1.h:Poss np:G</ta>
            <ta e="T841" id="Seg_12496" s="T839">0.1.h:A</ta>
            <ta e="T844" id="Seg_12497" s="T843">np:Th</ta>
            <ta e="T849" id="Seg_12498" s="T848">pro:Ins</ta>
            <ta e="T850" id="Seg_12499" s="T849">0.1.h:A</ta>
            <ta e="T853" id="Seg_12500" s="T852">np:Th</ta>
            <ta e="T855" id="Seg_12501" s="T854">np:L</ta>
            <ta e="T860" id="Seg_12502" s="T859">pro:Th</ta>
            <ta e="T861" id="Seg_12503" s="T860">0.1.h:Poss np:Ins</ta>
            <ta e="T862" id="Seg_12504" s="T861">0.1.h:A</ta>
            <ta e="T863" id="Seg_12505" s="T862">0.2.h:A</ta>
            <ta e="T864" id="Seg_12506" s="T863">0.3.h:A</ta>
            <ta e="T867" id="Seg_12507" s="T866">np:G</ta>
            <ta e="T871" id="Seg_12508" s="T870">0.1.h:Th</ta>
            <ta e="T872" id="Seg_12509" s="T871">pro:G</ta>
            <ta e="T880" id="Seg_12510" s="T879">0.3.h:A</ta>
            <ta e="T885" id="Seg_12511" s="T883">0.3.h:A</ta>
            <ta e="T887" id="Seg_12512" s="T886">0.1.h:A</ta>
            <ta e="T888" id="Seg_12513" s="T887">0.1.h:Poss np:Th</ta>
            <ta e="T891" id="Seg_12514" s="T889">0.3.h:A</ta>
            <ta e="T893" id="Seg_12515" s="T892">0.1.h:Poss</ta>
            <ta e="T894" id="Seg_12516" s="T893">np.h:R</ta>
            <ta e="T895" id="Seg_12517" s="T894">0.1.h:A</ta>
            <ta e="T897" id="Seg_12518" s="T896">pro:L</ta>
            <ta e="T905" id="Seg_12519" s="T904">0.1.h:Poss np:L</ta>
            <ta e="T906" id="Seg_12520" s="T905">pro:Th</ta>
            <ta e="T909" id="Seg_12521" s="T908">0.1.h:Poss np:Th</ta>
            <ta e="T911" id="Seg_12522" s="T910">0.3.h:A</ta>
            <ta e="T912" id="Seg_12523" s="T911">np:G</ta>
            <ta e="T913" id="Seg_12524" s="T912">0.1.h:A</ta>
            <ta e="T914" id="Seg_12525" s="T913">np:P</ta>
            <ta e="T918" id="Seg_12526" s="T917">np:So</ta>
            <ta e="T921" id="Seg_12527" s="T920">np:P</ta>
            <ta e="T923" id="Seg_12528" s="T922">np:Th</ta>
            <ta e="T935" id="Seg_12529" s="T937">np.h:A</ta>
            <ta e="T942" id="Seg_12530" s="T941">np.h:A</ta>
            <ta e="T945" id="Seg_12531" s="T944">np:Th</ta>
            <ta e="T949" id="Seg_12532" s="T948">0.3.h:A</ta>
            <ta e="T952" id="Seg_12533" s="T951">pro.h:A</ta>
            <ta e="T956" id="Seg_12534" s="T954">0.3.h:A</ta>
            <ta e="T957" id="Seg_12535" s="T956">pro.h:A</ta>
            <ta e="T962" id="Seg_12536" s="T961">adv:Time</ta>
            <ta e="T965" id="Seg_12537" s="T964">0.1.h:Th</ta>
            <ta e="T966" id="Seg_12538" s="T965">0.1.h:A</ta>
            <ta e="T967" id="Seg_12539" s="T966">np:G</ta>
            <ta e="T968" id="Seg_12540" s="T967">0.1.h:Th</ta>
            <ta e="T977" id="Seg_12541" s="T976">np:G</ta>
            <ta e="T980" id="Seg_12542" s="T979">0.1.h:Th</ta>
            <ta e="T982" id="Seg_12543" s="T981">np:G</ta>
            <ta e="T983" id="Seg_12544" s="T982">0.3.h:A</ta>
            <ta e="T984" id="Seg_12545" s="T983">np:L</ta>
            <ta e="T985" id="Seg_12546" s="T984">0.1.h:Th</ta>
            <ta e="T987" id="Seg_12547" s="T986">np:L</ta>
            <ta e="T994" id="Seg_12548" s="T993">n:Time</ta>
            <ta e="T995" id="Seg_12549" s="T994">0.1.h:Th</ta>
            <ta e="T998" id="Seg_12550" s="T996">0.1.h:Poss pp:G</ta>
            <ta e="T999" id="Seg_12551" s="T998">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-ChVD">
            <ta e="T2" id="Seg_12552" s="T1">0.1.h:S v:pred</ta>
            <ta e="T11" id="Seg_12553" s="T10">np:O</ta>
            <ta e="T13" id="Seg_12554" s="T11">0.1.h:S v:pred</ta>
            <ta e="T16" id="Seg_12555" s="T15">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_12556" s="T20">0.1.h:S v:pred</ta>
            <ta e="T27" id="Seg_12557" s="T26">0.1.h:S v:pred</ta>
            <ta e="T31" id="Seg_12558" s="T30">0.1.h:S v:pred</ta>
            <ta e="T35" id="Seg_12559" s="T34">0.1.h:S v:pred</ta>
            <ta e="T48" id="Seg_12560" s="T40">s:temp</ta>
            <ta e="T56" id="Seg_12561" s="T55">np:O</ta>
            <ta e="T58" id="Seg_12562" s="T56">0.1.h:S v:pred</ta>
            <ta e="T64" id="Seg_12563" s="T63">0.1.h:S v:pred</ta>
            <ta e="T68" id="Seg_12564" s="T66">s:purp</ta>
            <ta e="T70" id="Seg_12565" s="T69">0.1.h:S v:pred</ta>
            <ta e="T73" id="Seg_12566" s="T72">0.1.h:S v:pred</ta>
            <ta e="T77" id="Seg_12567" s="T76">0.1.h:S v:pred</ta>
            <ta e="T82" id="Seg_12568" s="T481">0.3.h:S v:pred</ta>
            <ta e="T86" id="Seg_12569" s="T84">0.3.h:S v:pred</ta>
            <ta e="T93" id="Seg_12570" s="T91">s:comp</ta>
            <ta e="T96" id="Seg_12571" s="T95">0.3.h:S v:pred</ta>
            <ta e="T103" id="Seg_12572" s="T102">0.1.h:S v:pred</ta>
            <ta e="T107" id="Seg_12573" s="T106">0.1.h:S v:pred</ta>
            <ta e="T111" id="Seg_12574" s="T110">np:S</ta>
            <ta e="T114" id="Seg_12575" s="T113">v:pred</ta>
            <ta e="T122" id="Seg_12576" s="T115">s:temp</ta>
            <ta e="T124" id="Seg_12577" s="T123">np:S</ta>
            <ta e="T126" id="Seg_12578" s="T125">adj:pred</ta>
            <ta e="T127" id="Seg_12579" s="T126">cop</ta>
            <ta e="T131" id="Seg_12580" s="T128">s:temp</ta>
            <ta e="T140" id="Seg_12581" s="T138">0.3.h:S v:pred</ta>
            <ta e="T145" id="Seg_12582" s="T144">adj:pred</ta>
            <ta e="T147" id="Seg_12583" s="T146">cop</ta>
            <ta e="T151" id="Seg_12584" s="T150">0.1.h:S v:pred</ta>
            <ta e="T155" id="Seg_12585" s="T154">0.1.h:S v:pred</ta>
            <ta e="T166" id="Seg_12586" s="T157">s:rel</ta>
            <ta e="T169" id="Seg_12587" s="T168">0.3.h:S v:pred</ta>
            <ta e="T179" id="Seg_12588" s="T178">0.3.h:S v:pred</ta>
            <ta e="T189" id="Seg_12589" s="T188">np:S</ta>
            <ta e="T190" id="Seg_12590" s="T189">v:pred</ta>
            <ta e="T196" id="Seg_12591" s="T195">0.3.h:S v:pred</ta>
            <ta e="T199" id="Seg_12592" s="T198">0.1.h:S v:pred</ta>
            <ta e="T203" id="Seg_12593" s="T199">s:temp</ta>
            <ta e="T204" id="Seg_12594" s="T203">pro:O</ta>
            <ta e="T206" id="Seg_12595" s="T205">0.3.h:S v:pred</ta>
            <ta e="T207" id="Seg_12596" s="T206">np.h:O</ta>
            <ta e="T208" id="Seg_12597" s="T207">0.3.h:S v:pred</ta>
            <ta e="T218" id="Seg_12598" s="T215">s:cond</ta>
            <ta e="T220" id="Seg_12599" s="T218">s:cond</ta>
            <ta e="T224" id="Seg_12600" s="T220">s:purp</ta>
            <ta e="T225" id="Seg_12601" s="T224">ptcl:pred</ta>
            <ta e="T229" id="Seg_12602" s="T226">s:purp</ta>
            <ta e="T233" id="Seg_12603" s="T232">0.2.h:S v:pred</ta>
            <ta e="T236" id="Seg_12604" s="T235">np:O</ta>
            <ta e="T237" id="Seg_12605" s="T236">0.3.h:S v:pred</ta>
            <ta e="T241" id="Seg_12606" s="T240">0.2.h:S v:pred</ta>
            <ta e="T245" id="Seg_12607" s="T244">0.2.h:S v:pred</ta>
            <ta e="T247" id="Seg_12608" s="T246">pro.h:S</ta>
            <ta e="T248" id="Seg_12609" s="T247">v:pred</ta>
            <ta e="T250" id="Seg_12610" s="T249">0.3.h:S v:pred</ta>
            <ta e="T252" id="Seg_12611" s="T251">0.2.h:S v:pred</ta>
            <ta e="T258" id="Seg_12612" s="T257">0.3.h:S v:pred</ta>
            <ta e="T261" id="Seg_12613" s="T260">0.3.h:S v:pred</ta>
            <ta e="T266" id="Seg_12614" s="T264">s:adv</ta>
            <ta e="T267" id="Seg_12615" s="T266">0.1.h:S v:pred</ta>
            <ta e="T274" id="Seg_12616" s="T270">s:adv</ta>
            <ta e="T281" id="Seg_12617" s="T280">pro.h:S</ta>
            <ta e="T282" id="Seg_12618" s="T281">s:adv</ta>
            <ta e="T283" id="Seg_12619" s="T282">v:pred</ta>
            <ta e="T285" id="Seg_12620" s="T284">np.h:S</ta>
            <ta e="T286" id="Seg_12621" s="T285">v:pred</ta>
            <ta e="T293" id="Seg_12622" s="T292">np.h:S</ta>
            <ta e="T294" id="Seg_12623" s="T293">v:pred</ta>
            <ta e="T296" id="Seg_12624" s="T295">0.3.h:S v:pred</ta>
            <ta e="T298" id="Seg_12625" s="T297">0.1.h:S v:pred</ta>
            <ta e="T300" id="Seg_12626" s="T299">0.3.h:S v:pred</ta>
            <ta e="T303" id="Seg_12627" s="T302">adj:pred</ta>
            <ta e="T305" id="Seg_12628" s="T304">np.h:S</ta>
            <ta e="T310" id="Seg_12629" s="T309">pro.h:S</ta>
            <ta e="T315" id="Seg_12630" s="T312">s:adv</ta>
            <ta e="T316" id="Seg_12631" s="T315">v:pred</ta>
            <ta e="T322" id="Seg_12632" s="T321">0.2.h:S v:pred</ta>
            <ta e="T323" id="Seg_12633" s="T322">0.3.h:S v:pred</ta>
            <ta e="T327" id="Seg_12634" s="T326">v:pred</ta>
            <ta e="T328" id="Seg_12635" s="T327">np.h:S</ta>
            <ta e="T330" id="Seg_12636" s="T329">0.1.h:S v:pred</ta>
            <ta e="T332" id="Seg_12637" s="T331">np.h:S</ta>
            <ta e="T335" id="Seg_12638" s="T333">v:pred</ta>
            <ta e="T336" id="Seg_12639" s="T335">pro:O</ta>
            <ta e="T337" id="Seg_12640" s="T336">pro:O</ta>
            <ta e="T338" id="Seg_12641" s="T337">0.3.h:S v:pred</ta>
            <ta e="T339" id="Seg_12642" s="T338">0.3.h:S v:pred</ta>
            <ta e="T341" id="Seg_12643" s="T340">0.1.h:S v:pred</ta>
            <ta e="T343" id="Seg_12644" s="T341">s:rel</ta>
            <ta e="T347" id="Seg_12645" s="T346">0.3.h:S v:pred</ta>
            <ta e="T351" id="Seg_12646" s="T350">0.3.h:S v:pred</ta>
            <ta e="T354" id="Seg_12647" s="T353">0.3.h:S v:pred</ta>
            <ta e="T362" id="Seg_12648" s="T361">0.1.h:S v:pred</ta>
            <ta e="T363" id="Seg_12649" s="T362">0.1.h:S v:pred</ta>
            <ta e="T368" id="Seg_12650" s="T367">0.1.h:S v:pred</ta>
            <ta e="T371" id="Seg_12651" s="T370">0.1.h:S v:pred</ta>
            <ta e="T376" id="Seg_12652" s="T375">0.3.h:S v:pred</ta>
            <ta e="T380" id="Seg_12653" s="T379">np.h:O</ta>
            <ta e="T381" id="Seg_12654" s="T380">0.3.h:S v:pred</ta>
            <ta e="T382" id="Seg_12655" s="T381">pro.h:S</ta>
            <ta e="T384" id="Seg_12656" s="T383">v:pred</ta>
            <ta e="T386" id="Seg_12657" s="T385">np:S</ta>
            <ta e="T387" id="Seg_12658" s="T386">v:pred</ta>
            <ta e="T390" id="Seg_12659" s="T389">np.h:S</ta>
            <ta e="T391" id="Seg_12660" s="T390">v:pred</ta>
            <ta e="T393" id="Seg_12661" s="T392">pro.h:S</ta>
            <ta e="T395" id="Seg_12662" s="T394">v:pred</ta>
            <ta e="T398" id="Seg_12663" s="T397">np:S</ta>
            <ta e="T400" id="Seg_12664" s="T399">0.2.h:S v:pred</ta>
            <ta e="T402" id="Seg_12665" s="T401">np.h:S</ta>
            <ta e="T404" id="Seg_12666" s="T403">n:pred</ta>
            <ta e="T405" id="Seg_12667" s="T404">cop</ta>
            <ta e="T415" id="Seg_12668" s="T414">0.1.h:S v:pred</ta>
            <ta e="T416" id="Seg_12669" s="T415">0.3.h:S v:pred</ta>
            <ta e="T419" id="Seg_12670" s="T418">0.1.h:S v:pred</ta>
            <ta e="T422" id="Seg_12671" s="T421">pro.h:S</ta>
            <ta e="T424" id="Seg_12672" s="T423">ptcl:pred</ta>
            <ta e="T427" id="Seg_12673" s="T426">np.h:S</ta>
            <ta e="T430" id="Seg_12674" s="T429">v:pred</ta>
            <ta e="T432" id="Seg_12675" s="T431">0.1.h:S v:pred</ta>
            <ta e="T434" id="Seg_12676" s="T432">0.3.h:S v:pred</ta>
            <ta e="T438" id="Seg_12677" s="T436">s:adv</ta>
            <ta e="T439" id="Seg_12678" s="T438">0.1.h:S v:pred</ta>
            <ta e="T444" id="Seg_12679" s="T443">0.1.h:S v:pred</ta>
            <ta e="T449" id="Seg_12680" s="T446">s:adv</ta>
            <ta e="T451" id="Seg_12681" s="T450">0.1.h:S v:pred</ta>
            <ta e="T454" id="Seg_12682" s="T453">v:pred</ta>
            <ta e="T456" id="Seg_12683" s="T455">np:S</ta>
            <ta e="T457" id="Seg_12684" s="T456">v:pred</ta>
            <ta e="T466" id="Seg_12685" s="T465">v:pred</ta>
            <ta e="T469" id="Seg_12686" s="T467">0.3.h:S v:pred</ta>
            <ta e="T471" id="Seg_12687" s="T470">0.3.h:S v:pred</ta>
            <ta e="T474" id="Seg_12688" s="T473">np.h:O</ta>
            <ta e="T475" id="Seg_12689" s="T474">0.1.h:S v:pred</ta>
            <ta e="T8" id="Seg_12690" s="T475">0.3.h:S adj:pred</ta>
            <ta e="T480" id="Seg_12691" s="T479">0.3.h:S v:pred</ta>
            <ta e="T486" id="Seg_12692" s="T484">s:rel</ta>
            <ta e="T487" id="Seg_12693" s="T486">np:S</ta>
            <ta e="T488" id="Seg_12694" s="T487">v:pred</ta>
            <ta e="T490" id="Seg_12695" s="T488">0.3.h:S v:pred</ta>
            <ta e="T492" id="Seg_12696" s="T491">np.h:O</ta>
            <ta e="T493" id="Seg_12697" s="T492">0.3.h:S v:pred</ta>
            <ta e="T494" id="Seg_12698" s="T493">0.3.h:S v:pred</ta>
            <ta e="T497" id="Seg_12699" s="T496">0.3.h:S v:pred</ta>
            <ta e="T499" id="Seg_12700" s="T498">0.3.h:S v:pred</ta>
            <ta e="T500" id="Seg_12701" s="T499">0.2.h:S v:pred</ta>
            <ta e="T503" id="Seg_12702" s="T501">0.3.h:S v:pred</ta>
            <ta e="T507" id="Seg_12703" s="T505">0.1.h:S v:pred</ta>
            <ta e="T514" id="Seg_12704" s="T513">np.h:O</ta>
            <ta e="T515" id="Seg_12705" s="T514">0.3.h:S v:pred</ta>
            <ta e="T517" id="Seg_12706" s="T516">pro:S</ta>
            <ta e="T518" id="Seg_12707" s="T517">v:pred</ta>
            <ta e="T521" id="Seg_12708" s="T520">0.1.h:S v:pred</ta>
            <ta e="T522" id="Seg_12709" s="T521">0.3.h:S v:pred</ta>
            <ta e="T528" id="Seg_12710" s="T527">pro.h:O</ta>
            <ta e="T530" id="Seg_12711" s="T529">0.2.h:S v:pred</ta>
            <ta e="T531" id="Seg_12712" s="T530">0.3.h:S v:pred</ta>
            <ta e="T532" id="Seg_12713" s="T531">pro.h:S</ta>
            <ta e="T538" id="Seg_12714" s="T537">n:pred</ta>
            <ta e="T539" id="Seg_12715" s="T538">cop</ta>
            <ta e="T540" id="Seg_12716" s="T539">0.3.h:S v:pred</ta>
            <ta e="T543" id="Seg_12717" s="T542">0.1.h:S v:pred</ta>
            <ta e="T545" id="Seg_12718" s="T544">np.h:S</ta>
            <ta e="T548" id="Seg_12719" s="T547">v:pred</ta>
            <ta e="T553" id="Seg_12720" s="T552">0.3.h:S v:pred</ta>
            <ta e="T554" id="Seg_12721" s="T553">pro.h:O</ta>
            <ta e="T555" id="Seg_12722" s="T554">v:pred</ta>
            <ta e="T557" id="Seg_12723" s="T556">np.h:S</ta>
            <ta e="T559" id="Seg_12724" s="T558">pro.h:S</ta>
            <ta e="T561" id="Seg_12725" s="T560">v:pred</ta>
            <ta e="T566" id="Seg_12726" s="T565">0.2.h:S v:pred</ta>
            <ta e="T572" id="Seg_12727" s="T571">v:pred</ta>
            <ta e="T573" id="Seg_12728" s="T572">pro.h:S</ta>
            <ta e="T575" id="Seg_12729" s="T574">n:pred</ta>
            <ta e="T576" id="Seg_12730" s="T575">0.1.h:S cop</ta>
            <ta e="T583" id="Seg_12731" s="T582">0.2.h:S v:pred</ta>
            <ta e="T584" id="Seg_12732" s="T583">0.3.h:S v:pred</ta>
            <ta e="T587" id="Seg_12733" s="T586">np:S</ta>
            <ta e="T589" id="Seg_12734" s="T588">v:pred</ta>
            <ta e="T595" id="Seg_12735" s="T589">s:temp</ta>
            <ta e="T598" id="Seg_12736" s="T597">np.h:O</ta>
            <ta e="T599" id="Seg_12737" s="T598">0.1.h:S v:pred</ta>
            <ta e="T600" id="Seg_12738" s="T599">pro.h:S</ta>
            <ta e="T601" id="Seg_12739" s="T600">v:pred</ta>
            <ta e="T603" id="Seg_12740" s="T602">0.2.h:S n:pred</ta>
            <ta e="T606" id="Seg_12741" s="T605">0.1.h:S v:pred</ta>
            <ta e="T607" id="Seg_12742" s="T606">v:pred</ta>
            <ta e="T609" id="Seg_12743" s="T608">np.h:S</ta>
            <ta e="T614" id="Seg_12744" s="T613">0.2.h:S v:pred</ta>
            <ta e="T615" id="Seg_12745" s="T614">0.3.h:S v:pred</ta>
            <ta e="T624" id="Seg_12746" s="T622">s:purp</ta>
            <ta e="T625" id="Seg_12747" s="T624">0.3.h:S v:pred</ta>
            <ta e="T631" id="Seg_12748" s="T630">0.1.h:S v:pred</ta>
            <ta e="T636" id="Seg_12749" s="T635">0.1.h:S v:pred</ta>
            <ta e="T639" id="Seg_12750" s="T638">np.h:O</ta>
            <ta e="T641" id="Seg_12751" s="T640">0.1.h:S v:pred</ta>
            <ta e="T647" id="Seg_12752" s="T646">0.1.h:S v:pred</ta>
            <ta e="T658" id="Seg_12753" s="T657">0.1.h:S v:pred</ta>
            <ta e="T662" id="Seg_12754" s="T661">np:O</ta>
            <ta e="T663" id="Seg_12755" s="T662">0.1.h:S v:pred</ta>
            <ta e="T667" id="Seg_12756" s="T666">0.1.h:S v:pred</ta>
            <ta e="T670" id="Seg_12757" s="T669">0.3.h:S v:pred</ta>
            <ta e="T672" id="Seg_12758" s="T671">pro.h:S</ta>
            <ta e="T675" id="Seg_12759" s="T674">v:pred</ta>
            <ta e="T679" id="Seg_12760" s="T678">0.1.h:S v:pred</ta>
            <ta e="T684" id="Seg_12761" s="T683">0.3.h:S v:pred</ta>
            <ta e="T685" id="Seg_12762" s="T684">0.3.h:S v:pred</ta>
            <ta e="T689" id="Seg_12763" s="T688">v:pred</ta>
            <ta e="T696" id="Seg_12764" s="T695">np:O</ta>
            <ta e="T697" id="Seg_12765" s="T696">0.3.h:S v:pred</ta>
            <ta e="T703" id="Seg_12766" s="T701">s:comp</ta>
            <ta e="T704" id="Seg_12767" s="T703">ptcl:pred</ta>
            <ta e="T710" id="Seg_12768" s="T709">np:S</ta>
            <ta e="T711" id="Seg_12769" s="T710">v:pred</ta>
            <ta e="T715" id="Seg_12770" s="T714">np:O</ta>
            <ta e="T717" id="Seg_12771" s="T716">0.1.h:S v:pred</ta>
            <ta e="T725" id="Seg_12772" s="T724">0.3.h:S v:pred</ta>
            <ta e="T729" id="Seg_12773" s="T728">np.h:S</ta>
            <ta e="T731" id="Seg_12774" s="T730">v:pred</ta>
            <ta e="T734" id="Seg_12775" s="T732">0.3.h:S v:pred</ta>
            <ta e="T737" id="Seg_12776" s="T736">pro.h:S</ta>
            <ta e="T741" id="Seg_12777" s="T740">v:pred</ta>
            <ta e="T761" id="Seg_12778" s="T760">np:O</ta>
            <ta e="T762" id="Seg_12779" s="T761">0.3.h:S v:pred</ta>
            <ta e="T764" id="Seg_12780" s="T762">s:temp</ta>
            <ta e="T770" id="Seg_12781" s="T769">np.h:S</ta>
            <ta e="T772" id="Seg_12782" s="T771">v:pred</ta>
            <ta e="T774" id="Seg_12783" s="T772">s:comp</ta>
            <ta e="T776" id="Seg_12784" s="T775">0.1.h:S v:pred</ta>
            <ta e="T781" id="Seg_12785" s="T780">0.3:S v:pred</ta>
            <ta e="T784" id="Seg_12786" s="T781">s:adv</ta>
            <ta e="T786" id="Seg_12787" s="T785">np:S</ta>
            <ta e="T787" id="Seg_12788" s="T786">v:pred</ta>
            <ta e="T789" id="Seg_12789" s="T788">np.h:O</ta>
            <ta e="T790" id="Seg_12790" s="T789">0.3:S v:pred</ta>
            <ta e="T797" id="Seg_12791" s="T794">s:adv</ta>
            <ta e="T798" id="Seg_12792" s="T797">0.1.h:S v:pred</ta>
            <ta e="T802" id="Seg_12793" s="T801">0.1.h:S v:pred</ta>
            <ta e="T807" id="Seg_12794" s="T806">0.1.h:S v:pred</ta>
            <ta e="T812" id="Seg_12795" s="T811">0.1.h:S v:pred</ta>
            <ta e="T814" id="Seg_12796" s="T813">np:S</ta>
            <ta e="T817" id="Seg_12797" s="T815">v:pred</ta>
            <ta e="T818" id="Seg_12798" s="T817">np:S</ta>
            <ta e="T820" id="Seg_12799" s="T818">v:pred</ta>
            <ta e="T824" id="Seg_12800" s="T823">np:O</ta>
            <ta e="T825" id="Seg_12801" s="T824">0.1.h:S v:pred</ta>
            <ta e="T829" id="Seg_12802" s="T826">s:temp</ta>
            <ta e="T831" id="Seg_12803" s="T830">0.1.h:S v:pred</ta>
            <ta e="T838" id="Seg_12804" s="T837">0.1.h:S v:pred</ta>
            <ta e="T841" id="Seg_12805" s="T839">s:adv</ta>
            <ta e="T844" id="Seg_12806" s="T843">np:S</ta>
            <ta e="T845" id="Seg_12807" s="T844">v:pred</ta>
            <ta e="T850" id="Seg_12808" s="T849">0.1.h:S v:pred</ta>
            <ta e="T853" id="Seg_12809" s="T852">np:S</ta>
            <ta e="T857" id="Seg_12810" s="T856">v:pred</ta>
            <ta e="T860" id="Seg_12811" s="T859">pro:O</ta>
            <ta e="T862" id="Seg_12812" s="T861">0.1.h:S v:pred</ta>
            <ta e="T863" id="Seg_12813" s="T862">0.2.h:S v:pred</ta>
            <ta e="T864" id="Seg_12814" s="T863">0.3.h:S v:pred</ta>
            <ta e="T869" id="Seg_12815" s="T867">s:adv</ta>
            <ta e="T870" id="Seg_12816" s="T869">s:adv</ta>
            <ta e="T871" id="Seg_12817" s="T870">0.1.h:S v:pred</ta>
            <ta e="T874" id="Seg_12818" s="T872">s:adv</ta>
            <ta e="T880" id="Seg_12819" s="T879">0.3.h:S v:pred</ta>
            <ta e="T885" id="Seg_12820" s="T883">0.3.h:S v:pred</ta>
            <ta e="T887" id="Seg_12821" s="T886">0.1.h:S v:pred</ta>
            <ta e="T888" id="Seg_12822" s="T887">np:O</ta>
            <ta e="T891" id="Seg_12823" s="T889">0.3.h:S v:pred</ta>
            <ta e="T895" id="Seg_12824" s="T894">0.1.h:S v:pred</ta>
            <ta e="T900" id="Seg_12825" s="T898">v:pred</ta>
            <ta e="T906" id="Seg_12826" s="T905">pro:S</ta>
            <ta e="T908" id="Seg_12827" s="T907">ptcl:pred</ta>
            <ta e="T909" id="Seg_12828" s="T908">np:O</ta>
            <ta e="T911" id="Seg_12829" s="T910">0.3.h:S v:pred</ta>
            <ta e="T913" id="Seg_12830" s="T911">s:temp</ta>
            <ta e="T914" id="Seg_12831" s="T913">np:S</ta>
            <ta e="T915" id="Seg_12832" s="T914">v:pred</ta>
            <ta e="T919" id="Seg_12833" s="T916">s:adv</ta>
            <ta e="T922" id="Seg_12834" s="T919">s:temp</ta>
            <ta e="T923" id="Seg_12835" s="T922">np:S</ta>
            <ta e="T924" id="Seg_12836" s="T923">v:pred</ta>
            <ta e="T929" id="Seg_12837" s="T928">0.3:S v:pred</ta>
            <ta e="T935" id="Seg_12838" s="T937">np.h:S</ta>
            <ta e="T938" id="Seg_12839" s="T935">v:pred</ta>
            <ta e="T942" id="Seg_12840" s="T941">np.h:S</ta>
            <ta e="T943" id="Seg_12841" s="T942">v:pred</ta>
            <ta e="T945" id="Seg_12842" s="T944">np:S</ta>
            <ta e="T946" id="Seg_12843" s="T945">v:pred</ta>
            <ta e="T949" id="Seg_12844" s="T948">0.3.h:S v:pred</ta>
            <ta e="T952" id="Seg_12845" s="T951">pro.h:S</ta>
            <ta e="T954" id="Seg_12846" s="T952">v:pred</ta>
            <ta e="T956" id="Seg_12847" s="T954">s:temp</ta>
            <ta e="T957" id="Seg_12848" s="T956">pro.h:S</ta>
            <ta e="T960" id="Seg_12849" s="T959">v:pred</ta>
            <ta e="T964" id="Seg_12850" s="T963">np:O</ta>
            <ta e="T965" id="Seg_12851" s="T964">0.1.h:S v:pred</ta>
            <ta e="T966" id="Seg_12852" s="T965">0.1.h:S v:pred</ta>
            <ta e="T968" id="Seg_12853" s="T967">0.1.h:S v:pred</ta>
            <ta e="T980" id="Seg_12854" s="T979">0.1.h:S v:pred</ta>
            <ta e="T983" id="Seg_12855" s="T982">0.3.h:S v:pred</ta>
            <ta e="T985" id="Seg_12856" s="T984">0.1.h:S v:pred</ta>
            <ta e="T995" id="Seg_12857" s="T994">0.1.h:S v:pred</ta>
            <ta e="T999" id="Seg_12858" s="T998">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-ChVD">
            <ta e="T1" id="Seg_12859" s="T0">accs-gen</ta>
            <ta e="T2" id="Seg_12860" s="T1">0.accs-sit</ta>
            <ta e="T11" id="Seg_12861" s="T10">accs-gen</ta>
            <ta e="T13" id="Seg_12862" s="T11">0.giv-active</ta>
            <ta e="T15" id="Seg_12863" s="T14">accs-gen</ta>
            <ta e="T17" id="Seg_12864" s="T16">giv-active</ta>
            <ta e="T20" id="Seg_12865" s="T19">new</ta>
            <ta e="T22" id="Seg_12866" s="T20">0.giv-inactive</ta>
            <ta e="T23" id="Seg_12867" s="T22">accs-inf</ta>
            <ta e="T27" id="Seg_12868" s="T26">0.giv-active</ta>
            <ta e="T28" id="Seg_12869" s="T27">accs-gen</ta>
            <ta e="T31" id="Seg_12870" s="T30">0.giv-active</ta>
            <ta e="T33" id="Seg_12871" s="T32">new</ta>
            <ta e="T35" id="Seg_12872" s="T34">0.giv-active</ta>
            <ta e="T36" id="Seg_12873" s="T35">giv-active</ta>
            <ta e="T37" id="Seg_12874" s="T36">new</ta>
            <ta e="T52" id="Seg_12875" s="T51">new</ta>
            <ta e="T56" id="Seg_12876" s="T55">new</ta>
            <ta e="T58" id="Seg_12877" s="T56">0.giv-active</ta>
            <ta e="T64" id="Seg_12878" s="T63">0.giv-active</ta>
            <ta e="T65" id="Seg_12879" s="T64">accs-inf</ta>
            <ta e="T68" id="Seg_12880" s="T67">0.giv-active</ta>
            <ta e="T70" id="Seg_12881" s="T69">0.giv-active</ta>
            <ta e="T73" id="Seg_12882" s="T72">0.giv-active</ta>
            <ta e="T75" id="Seg_12883" s="T74">new</ta>
            <ta e="T77" id="Seg_12884" s="T76">0.accs-aggr</ta>
            <ta e="T84" id="Seg_12885" s="T83">new</ta>
            <ta e="T103" id="Seg_12886" s="T102">0.giv-inactive</ta>
            <ta e="T105" id="Seg_12887" s="T104">giv-inactive</ta>
            <ta e="T107" id="Seg_12888" s="T106">0.giv-inactive</ta>
            <ta e="T109" id="Seg_12889" s="T108">new</ta>
            <ta e="T111" id="Seg_12890" s="T110">giv-active</ta>
            <ta e="T124" id="Seg_12891" s="T123">accs-sit</ta>
            <ta e="T131" id="Seg_12892" s="T130">0.giv-inactive</ta>
            <ta e="T151" id="Seg_12893" s="T150">0.giv-active</ta>
            <ta e="T155" id="Seg_12894" s="T154">0.giv-active</ta>
            <ta e="T162" id="Seg_12895" s="T161">accs-sit</ta>
            <ta e="T174" id="Seg_12896" s="T173">new</ta>
            <ta e="T177" id="Seg_12897" s="T176">new</ta>
            <ta e="T178" id="Seg_12898" s="T177">new</ta>
            <ta e="T186" id="Seg_12899" s="T185">new</ta>
            <ta e="T189" id="Seg_12900" s="T188">giv-active</ta>
            <ta e="T193" id="Seg_12901" s="T192">accs-sit</ta>
            <ta e="T196" id="Seg_12902" s="T195">0.accs-sit</ta>
            <ta e="T199" id="Seg_12903" s="T198">0.giv-inactive</ta>
            <ta e="T203" id="Seg_12904" s="T202">0.giv-active</ta>
            <ta e="T207" id="Seg_12905" s="T206">accs-inf</ta>
            <ta e="T208" id="Seg_12906" s="T207">0.quot-sp</ta>
            <ta e="T212" id="Seg_12907" s="T211">0.giv-inactive-Q</ta>
            <ta e="T216" id="Seg_12908" s="T215">new-Q</ta>
            <ta e="T217" id="Seg_12909" s="T216">0.giv-active-Q</ta>
            <ta e="T219" id="Seg_12910" s="T218">new-Q</ta>
            <ta e="T220" id="Seg_12911" s="T219">0.giv-active-Q</ta>
            <ta e="T222" id="Seg_12912" s="T221">new-Q</ta>
            <ta e="T224" id="Seg_12913" s="T223">0.giv-active-Q</ta>
            <ta e="T227" id="Seg_12914" s="T226">giv-active-Q</ta>
            <ta e="T228" id="Seg_12915" s="T227">0.giv-active-Q</ta>
            <ta e="T233" id="Seg_12916" s="T232">0.giv-active-Q</ta>
            <ta e="T236" id="Seg_12917" s="T235">giv-active-Q</ta>
            <ta e="T237" id="Seg_12918" s="T236">0.giv-inactive 0.quot-sp</ta>
            <ta e="T241" id="Seg_12919" s="T240">0.giv-active-Q</ta>
            <ta e="T245" id="Seg_12920" s="T244">0.giv-active-Q</ta>
            <ta e="T247" id="Seg_12921" s="T246">giv-active-Q</ta>
            <ta e="T250" id="Seg_12922" s="T249">0.giv-active 0.quot-sp</ta>
            <ta e="T252" id="Seg_12923" s="T251">0.giv-active-Q</ta>
            <ta e="T265" id="Seg_12924" s="T264">accs-inf</ta>
            <ta e="T267" id="Seg_12925" s="T266">0.giv-active</ta>
            <ta e="T271" id="Seg_12926" s="T270">new</ta>
            <ta e="T273" id="Seg_12927" s="T272">0.giv-active</ta>
            <ta e="T281" id="Seg_12928" s="T280">giv-active</ta>
            <ta e="T282" id="Seg_12929" s="T281">0.giv-active</ta>
            <ta e="T285" id="Seg_12930" s="T284">giv-inactive</ta>
            <ta e="T292" id="Seg_12931" s="T291">accs-sit</ta>
            <ta e="T293" id="Seg_12932" s="T292">accs-inf</ta>
            <ta e="T295" id="Seg_12933" s="T294">accs-inf</ta>
            <ta e="T296" id="Seg_12934" s="T295">0.giv-active</ta>
            <ta e="T298" id="Seg_12935" s="T297">0.accs-aggr-Q</ta>
            <ta e="T300" id="Seg_12936" s="T299">0.giv-inactive 0.quot-sp</ta>
            <ta e="T302" id="Seg_12937" s="T301">giv-active</ta>
            <ta e="T303" id="Seg_12938" s="T302">accs-inf</ta>
            <ta e="T305" id="Seg_12939" s="T304">giv-active</ta>
            <ta e="T309" id="Seg_12940" s="T308">accs-inf</ta>
            <ta e="T310" id="Seg_12941" s="T309">giv-inactive</ta>
            <ta e="T313" id="Seg_12942" s="T312">giv-inactive</ta>
            <ta e="T322" id="Seg_12943" s="T321">0.giv-inactive-Q</ta>
            <ta e="T323" id="Seg_12944" s="T322">0.giv-inactive 0.quot-sp</ta>
            <ta e="T327" id="Seg_12945" s="T326">quot-sp</ta>
            <ta e="T328" id="Seg_12946" s="T327">giv-active</ta>
            <ta e="T330" id="Seg_12947" s="T329">0.giv-inactive</ta>
            <ta e="T332" id="Seg_12948" s="T331">giv-inactive</ta>
            <ta e="T338" id="Seg_12949" s="T337">0.giv-active</ta>
            <ta e="T339" id="Seg_12950" s="T338">0.giv-active</ta>
            <ta e="T341" id="Seg_12951" s="T340">0.giv-active</ta>
            <ta e="T342" id="Seg_12952" s="T341">giv-inactive</ta>
            <ta e="T344" id="Seg_12953" s="T343">accs-inf</ta>
            <ta e="T347" id="Seg_12954" s="T346">0.giv-inactive</ta>
            <ta e="T350" id="Seg_12955" s="T349">accs-sit</ta>
            <ta e="T351" id="Seg_12956" s="T350">0.giv-active</ta>
            <ta e="T354" id="Seg_12957" s="T353">0.giv-active</ta>
            <ta e="T361" id="Seg_12958" s="T360">giv-inactive</ta>
            <ta e="T362" id="Seg_12959" s="T361">0.giv-inactive</ta>
            <ta e="T363" id="Seg_12960" s="T362">0.giv-active</ta>
            <ta e="T364" id="Seg_12961" s="T363">giv-active</ta>
            <ta e="T367" id="Seg_12962" s="T366">giv-inactive</ta>
            <ta e="T368" id="Seg_12963" s="T367">0.giv-active</ta>
            <ta e="T371" id="Seg_12964" s="T370">0.giv-active</ta>
            <ta e="T375" id="Seg_12965" s="T374">accs-inf</ta>
            <ta e="T380" id="Seg_12966" s="T379">accs-inf</ta>
            <ta e="T381" id="Seg_12967" s="T380">0.giv-inactive</ta>
            <ta e="T382" id="Seg_12968" s="T381">accs-inf</ta>
            <ta e="T386" id="Seg_12969" s="T385">new</ta>
            <ta e="T390" id="Seg_12970" s="T389">giv-inactive</ta>
            <ta e="T398" id="Seg_12971" s="T397">giv-active</ta>
            <ta e="T404" id="Seg_12972" s="T403">accs-inf</ta>
            <ta e="T412" id="Seg_12973" s="T411">accs-inf</ta>
            <ta e="T415" id="Seg_12974" s="T414">0.giv-inactive-Q</ta>
            <ta e="T416" id="Seg_12975" s="T415">0.giv-inactive 0.quot-sp</ta>
            <ta e="T418" id="Seg_12976" s="T417">accs-sit</ta>
            <ta e="T419" id="Seg_12977" s="T418">0.giv-active</ta>
            <ta e="T427" id="Seg_12978" s="T426">giv-inactive</ta>
            <ta e="T432" id="Seg_12979" s="T431">0.giv-inactive-Q</ta>
            <ta e="T434" id="Seg_12980" s="T432">0.giv-inactive 0.quot-sp</ta>
            <ta e="T438" id="Seg_12981" s="T437">0.giv-active</ta>
            <ta e="T439" id="Seg_12982" s="T438">0.giv-active</ta>
            <ta e="T442" id="Seg_12983" s="T441">giv-inactive</ta>
            <ta e="T444" id="Seg_12984" s="T443">0.giv-active</ta>
            <ta e="T445" id="Seg_12985" s="T444">accs-inf</ta>
            <ta e="T449" id="Seg_12986" s="T448">0.giv-active</ta>
            <ta e="T451" id="Seg_12987" s="T450">0.giv-active</ta>
            <ta e="T456" id="Seg_12988" s="T455">new</ta>
            <ta e="T459" id="Seg_12989" s="T458">new</ta>
            <ta e="T464" id="Seg_12990" s="T463">new</ta>
            <ta e="T469" id="Seg_12991" s="T467">0.giv-inactive</ta>
            <ta e="T471" id="Seg_12992" s="T470">0.giv-active</ta>
            <ta e="T474" id="Seg_12993" s="T473">new</ta>
            <ta e="T475" id="Seg_12994" s="T474">0.giv-inactive</ta>
            <ta e="T8" id="Seg_12995" s="T475">0.giv-active</ta>
            <ta e="T480" id="Seg_12996" s="T479">0.giv-active</ta>
            <ta e="T487" id="Seg_12997" s="T486">new-Q</ta>
            <ta e="T490" id="Seg_12998" s="T488">0.giv-active 0.quot-sp</ta>
            <ta e="T491" id="Seg_12999" s="T490">accs-inf-Q</ta>
            <ta e="T492" id="Seg_13000" s="T491">accs-inf-Q</ta>
            <ta e="T494" id="Seg_13001" s="T493">0.giv-active 0.quot-sp</ta>
            <ta e="T495" id="Seg_13002" s="T494">giv-inactive-Q</ta>
            <ta e="T497" id="Seg_13003" s="T496">0.giv-active-Q</ta>
            <ta e="T499" id="Seg_13004" s="T498">0.giv-active 0.quot-sp</ta>
            <ta e="T500" id="Seg_13005" s="T499">0.giv-inactive-Q</ta>
            <ta e="T503" id="Seg_13006" s="T501">0.giv-active 0.quot-sp</ta>
            <ta e="T507" id="Seg_13007" s="T505">0.giv-active</ta>
            <ta e="T514" id="Seg_13008" s="T513">new</ta>
            <ta e="T519" id="Seg_13009" s="T518">new</ta>
            <ta e="T521" id="Seg_13010" s="T520">0.giv-active-Q</ta>
            <ta e="T522" id="Seg_13011" s="T521">0.giv-active 0.quot-sp</ta>
            <ta e="T524" id="Seg_13012" s="T523">giv-active-Q</ta>
            <ta e="T525" id="Seg_13013" s="T524">giv-inactive-Q</ta>
            <ta e="T528" id="Seg_13014" s="T527">giv-active-Q</ta>
            <ta e="T530" id="Seg_13015" s="T529">0.giv-active-Q</ta>
            <ta e="T531" id="Seg_13016" s="T530">0.giv-active 0.quot-sp</ta>
            <ta e="T532" id="Seg_13017" s="T531">giv-active-Q</ta>
            <ta e="T540" id="Seg_13018" s="T539">0.giv-active 0.quot-sp</ta>
            <ta e="T543" id="Seg_13019" s="T542">0.giv-active-Q</ta>
            <ta e="T545" id="Seg_13020" s="T544">accs-inf</ta>
            <ta e="T550" id="Seg_13021" s="T549">giv-inactive</ta>
            <ta e="T554" id="Seg_13022" s="T553">giv-inactive</ta>
            <ta e="T557" id="Seg_13023" s="T556">giv-inactive</ta>
            <ta e="T559" id="Seg_13024" s="T558">giv-active</ta>
            <ta e="T561" id="Seg_13025" s="T560">quot-sp</ta>
            <ta e="T566" id="Seg_13026" s="T565">0.giv-active-Q</ta>
            <ta e="T573" id="Seg_13027" s="T572">giv-active</ta>
            <ta e="T576" id="Seg_13028" s="T575">0.giv-active-Q</ta>
            <ta e="T579" id="Seg_13029" s="T578">giv-active-Q</ta>
            <ta e="T583" id="Seg_13030" s="T582">0.giv-active-Q</ta>
            <ta e="T584" id="Seg_13031" s="T583">0.giv-active 0.quot-sp</ta>
            <ta e="T598" id="Seg_13032" s="T597">new</ta>
            <ta e="T599" id="Seg_13033" s="T598">0.giv-inactive</ta>
            <ta e="T600" id="Seg_13034" s="T599">giv-active</ta>
            <ta e="T601" id="Seg_13035" s="T600">quot-sp</ta>
            <ta e="T603" id="Seg_13036" s="T602">0.giv-active-Q</ta>
            <ta e="T604" id="Seg_13037" s="T603">giv-active-Q</ta>
            <ta e="T605" id="Seg_13038" s="T604">giv-active-Q</ta>
            <ta e="T606" id="Seg_13039" s="T605">0.giv-active 0.quot-sp</ta>
            <ta e="T607" id="Seg_13040" s="T606">quot-sp</ta>
            <ta e="T609" id="Seg_13041" s="T608">giv-active</ta>
            <ta e="T612" id="Seg_13042" s="T611">giv-inactive-Q</ta>
            <ta e="T614" id="Seg_13043" s="T613">0.giv-active-Q</ta>
            <ta e="T615" id="Seg_13044" s="T614">0.giv-active 0.quot-sp</ta>
            <ta e="T625" id="Seg_13045" s="T624">0.giv-inactive</ta>
            <ta e="T631" id="Seg_13046" s="T630">0.giv-inactive-Q</ta>
            <ta e="T636" id="Seg_13047" s="T635">0.giv-active</ta>
            <ta e="T639" id="Seg_13048" s="T638">giv-inactive</ta>
            <ta e="T641" id="Seg_13049" s="T640">0.giv-active</ta>
            <ta e="T647" id="Seg_13050" s="T646">0.giv-active</ta>
            <ta e="T658" id="Seg_13051" s="T657">0.giv-active</ta>
            <ta e="T662" id="Seg_13052" s="T661">giv-inactive</ta>
            <ta e="T663" id="Seg_13053" s="T662">0.giv-active</ta>
            <ta e="T667" id="Seg_13054" s="T666">0.giv-active</ta>
            <ta e="T669" id="Seg_13055" s="T668">accs-sit</ta>
            <ta e="T670" id="Seg_13056" s="T669">0.giv-inactive</ta>
            <ta e="T672" id="Seg_13057" s="T671">giv-active</ta>
            <ta e="T673" id="Seg_13058" s="T672">accs-sit</ta>
            <ta e="T679" id="Seg_13059" s="T678">0.giv-active</ta>
            <ta e="T684" id="Seg_13060" s="T683">0.giv-active</ta>
            <ta e="T685" id="Seg_13061" s="T684">0.giv-active</ta>
            <ta e="T692" id="Seg_13062" s="T691">accs-inf</ta>
            <ta e="T696" id="Seg_13063" s="T695">new</ta>
            <ta e="T702" id="Seg_13064" s="T701">new-Q</ta>
            <ta e="T703" id="Seg_13065" s="T702">0.giv-inactive-Q</ta>
            <ta e="T710" id="Seg_13066" s="T709">new</ta>
            <ta e="T712" id="Seg_13067" s="T711">new</ta>
            <ta e="T715" id="Seg_13068" s="T714">giv-active</ta>
            <ta e="T717" id="Seg_13069" s="T716">0.giv-inactive</ta>
            <ta e="T724" id="Seg_13070" s="T723">accs-sit</ta>
            <ta e="T725" id="Seg_13071" s="T724">0.giv-inactive</ta>
            <ta e="T729" id="Seg_13072" s="T728">giv-active</ta>
            <ta e="T734" id="Seg_13073" s="T732">0.giv-active</ta>
            <ta e="T737" id="Seg_13074" s="T736">giv-inactive</ta>
            <ta e="T739" id="Seg_13075" s="T738">giv-inactive</ta>
            <ta e="T761" id="Seg_13076" s="T760">new</ta>
            <ta e="T762" id="Seg_13077" s="T761">0.giv-inactive</ta>
            <ta e="T763" id="Seg_13078" s="T762">giv-active</ta>
            <ta e="T770" id="Seg_13079" s="T769">giv-inactive</ta>
            <ta e="T773" id="Seg_13080" s="T772">accs-inf</ta>
            <ta e="T776" id="Seg_13081" s="T775">0.giv-inactive</ta>
            <ta e="T779" id="Seg_13082" s="T778">giv-active</ta>
            <ta e="T781" id="Seg_13083" s="T780">0.giv-active</ta>
            <ta e="T786" id="Seg_13084" s="T785">new</ta>
            <ta e="T789" id="Seg_13085" s="T788">giv-inactive</ta>
            <ta e="T790" id="Seg_13086" s="T789">0.giv-active</ta>
            <ta e="T795" id="Seg_13087" s="T794">accs-inf</ta>
            <ta e="T797" id="Seg_13088" s="T796">0.giv-active</ta>
            <ta e="T798" id="Seg_13089" s="T797">0.giv-active</ta>
            <ta e="T802" id="Seg_13090" s="T801">0.giv-active</ta>
            <ta e="T807" id="Seg_13091" s="T806">0.giv-active</ta>
            <ta e="T812" id="Seg_13092" s="T811">0.giv-active</ta>
            <ta e="T814" id="Seg_13093" s="T813">accs-inf</ta>
            <ta e="T818" id="Seg_13094" s="T817">accs-inf</ta>
            <ta e="T824" id="Seg_13095" s="T823">new</ta>
            <ta e="T825" id="Seg_13096" s="T824">0.giv-active</ta>
            <ta e="T827" id="Seg_13097" s="T826">giv-active</ta>
            <ta e="T829" id="Seg_13098" s="T828">0.giv-active</ta>
            <ta e="T831" id="Seg_13099" s="T830">0.giv-active</ta>
            <ta e="T834" id="Seg_13100" s="T833">accs-inf</ta>
            <ta e="T838" id="Seg_13101" s="T837">0.giv-active</ta>
            <ta e="T841" id="Seg_13102" s="T840">0.giv-active</ta>
            <ta e="T844" id="Seg_13103" s="T843">new</ta>
            <ta e="T849" id="Seg_13104" s="T848">giv-active</ta>
            <ta e="T850" id="Seg_13105" s="T849">0.giv-active</ta>
            <ta e="T853" id="Seg_13106" s="T852">new</ta>
            <ta e="T855" id="Seg_13107" s="T854">accs-inf</ta>
            <ta e="T860" id="Seg_13108" s="T859">giv-inactive</ta>
            <ta e="T861" id="Seg_13109" s="T860">giv-active</ta>
            <ta e="T862" id="Seg_13110" s="T861">0.giv-inactive</ta>
            <ta e="T863" id="Seg_13111" s="T862">0.giv-active-Q</ta>
            <ta e="T864" id="Seg_13112" s="T863">0.quot-sp</ta>
            <ta e="T867" id="Seg_13113" s="T866">giv-inactive-Q</ta>
            <ta e="T871" id="Seg_13114" s="T870">0.giv-active</ta>
            <ta e="T872" id="Seg_13115" s="T871">giv-active</ta>
            <ta e="T873" id="Seg_13116" s="T872">new</ta>
            <ta e="T878" id="Seg_13117" s="T877">new</ta>
            <ta e="T880" id="Seg_13118" s="T879">0.giv-inactive 0.quot-sp</ta>
            <ta e="T885" id="Seg_13119" s="T883">0.giv-inactive 0.quot-sp</ta>
            <ta e="T887" id="Seg_13120" s="T886">0.giv-inactive 0.quot-sp</ta>
            <ta e="T888" id="Seg_13121" s="T887">giv-inactive</ta>
            <ta e="T891" id="Seg_13122" s="T889">0.giv-active</ta>
            <ta e="T894" id="Seg_13123" s="T893">giv-active</ta>
            <ta e="T895" id="Seg_13124" s="T894">0.giv-active</ta>
            <ta e="T904" id="Seg_13125" s="T903">new</ta>
            <ta e="T905" id="Seg_13126" s="T904">accs-inf</ta>
            <ta e="T909" id="Seg_13127" s="T908">new</ta>
            <ta e="T912" id="Seg_13128" s="T911">giv-inactive</ta>
            <ta e="T913" id="Seg_13129" s="T912">0.giv-active</ta>
            <ta e="T914" id="Seg_13130" s="T913">accs-inf</ta>
            <ta e="T918" id="Seg_13131" s="T917">accs-inf</ta>
            <ta e="T921" id="Seg_13132" s="T920">new</ta>
            <ta e="T923" id="Seg_13133" s="T922">accs-inf</ta>
            <ta e="T929" id="Seg_13134" s="T928">0.giv-active</ta>
            <ta e="T935" id="Seg_13135" s="T937">new</ta>
            <ta e="T939" id="Seg_13136" s="T938">giv-inactive</ta>
            <ta e="T942" id="Seg_13137" s="T941">giv-active</ta>
            <ta e="T945" id="Seg_13138" s="T944">new</ta>
            <ta e="T952" id="Seg_13139" s="T951">accs-inf</ta>
            <ta e="T956" id="Seg_13140" s="T954">0.giv-active</ta>
            <ta e="T957" id="Seg_13141" s="T956">giv-inactive</ta>
            <ta e="T965" id="Seg_13142" s="T964">0.giv-active</ta>
            <ta e="T966" id="Seg_13143" s="T965">0.giv-active</ta>
            <ta e="T967" id="Seg_13144" s="T966">giv-inactive</ta>
            <ta e="T968" id="Seg_13145" s="T967">0.giv-active</ta>
            <ta e="T975" id="Seg_13146" s="T974">giv-active</ta>
            <ta e="T977" id="Seg_13147" s="T976">giv-active</ta>
            <ta e="T980" id="Seg_13148" s="T979">0.giv-active</ta>
            <ta e="T982" id="Seg_13149" s="T981">accs-inf</ta>
            <ta e="T984" id="Seg_13150" s="T983">new</ta>
            <ta e="T985" id="Seg_13151" s="T984">0.giv-active</ta>
            <ta e="T987" id="Seg_13152" s="T986">new</ta>
            <ta e="T995" id="Seg_13153" s="T994">0.giv-active</ta>
            <ta e="T997" id="Seg_13154" s="T996">accs-inf</ta>
         </annotation>
         <annotation name="Top" tierref="Top-ChVD">
            <ta e="T7" id="Seg_13155" s="T5">top.int.concr</ta>
            <ta e="T17" id="Seg_13156" s="T16">top.int.concr</ta>
            <ta e="T27" id="Seg_13157" s="T26">0.top.int.concr</ta>
            <ta e="T30" id="Seg_13158" s="T27">top.int.concr</ta>
            <ta e="T34" id="Seg_13159" s="T33">top.int.concr</ta>
            <ta e="T58" id="Seg_13160" s="T56">0.top.int.concr</ta>
            <ta e="T61" id="Seg_13161" s="T60">top.int.concr</ta>
            <ta e="T70" id="Seg_13162" s="T69">0.top.int.concr</ta>
            <ta e="T77" id="Seg_13163" s="T76">0.top.int.concr</ta>
            <ta e="T105" id="Seg_13164" s="T104">top.int.concr</ta>
            <ta e="T111" id="Seg_13165" s="T109">top.int.concr</ta>
            <ta e="T124" id="Seg_13166" s="T122">top.int.concr</ta>
            <ta e="T155" id="Seg_13167" s="T154">0.top.int.concr</ta>
            <ta e="T190" id="Seg_13168" s="T189">0.top.int.abstr</ta>
            <ta e="T198" id="Seg_13169" s="T197">top.int.concr</ta>
            <ta e="T265" id="Seg_13170" s="T262">top.int.concr</ta>
            <ta e="T273" id="Seg_13171" s="T272">0.top.int.concr</ta>
            <ta e="T281" id="Seg_13172" s="T280">0.top.int.concr</ta>
            <ta e="T286" id="Seg_13173" s="T285">0.top.int.abstr</ta>
            <ta e="T290" id="Seg_13174" s="T289">top.int.concr</ta>
            <ta e="T296" id="Seg_13175" s="T295">0.top.int.concr</ta>
            <ta e="T303" id="Seg_13176" s="T302">0.top.int.concr</ta>
            <ta e="T316" id="Seg_13177" s="T315">0.top.int.concr</ta>
            <ta e="T329" id="Seg_13178" s="T328">top.int.concr</ta>
            <ta e="T332" id="Seg_13179" s="T331">top.int.concr</ta>
            <ta e="T338" id="Seg_13180" s="T337">0.top.int.concr</ta>
            <ta e="T339" id="Seg_13181" s="T338">0.top.int.concr</ta>
            <ta e="T341" id="Seg_13182" s="T340">0.top.int.concr</ta>
            <ta e="T347" id="Seg_13183" s="T346">0.top.int.concr</ta>
            <ta e="T351" id="Seg_13184" s="T350">0.top.int.concr</ta>
            <ta e="T354" id="Seg_13185" s="T353">0.top.int.concr</ta>
            <ta e="T360" id="Seg_13186" s="T359">top.int.concr</ta>
            <ta e="T363" id="Seg_13187" s="T362">0.top.int.concr</ta>
            <ta e="T368" id="Seg_13188" s="T367">0.top.int.concr</ta>
            <ta e="T371" id="Seg_13189" s="T370">0.top.int.concr</ta>
            <ta e="T381" id="Seg_13190" s="T380">0.top.int.concr</ta>
            <ta e="T382" id="Seg_13191" s="T381">top.int.concr</ta>
            <ta e="T387" id="Seg_13192" s="T386">0.top.int.abstr</ta>
            <ta e="T390" id="Seg_13193" s="T389">top.int.concr</ta>
            <ta e="T402" id="Seg_13194" s="T401">top.int.concr</ta>
            <ta e="T407" id="Seg_13195" s="T405">top.int.concr</ta>
            <ta e="T409" id="Seg_13196" s="T407">top.int.concr</ta>
            <ta e="T418" id="Seg_13197" s="T416">top.int.concr</ta>
            <ta e="T427" id="Seg_13198" s="T426">top.int.concr</ta>
            <ta e="T439" id="Seg_13199" s="T438">0.top.int.concr</ta>
            <ta e="T444" id="Seg_13200" s="T443">0.top.int.concr</ta>
            <ta e="T457" id="Seg_13201" s="T456">0.top.int.abstr</ta>
            <ta e="T469" id="Seg_13202" s="T467">0.top.int.concr</ta>
            <ta e="T471" id="Seg_13203" s="T470">0.top.int.concr</ta>
            <ta e="T472" id="Seg_13204" s="T471">top.int.concr</ta>
            <ta e="T8" id="Seg_13205" s="T475">0.top.int.concr</ta>
            <ta e="T480" id="Seg_13206" s="T479">0.top.int.concr</ta>
            <ta e="T488" id="Seg_13207" s="T487">0.top.int.abstr</ta>
            <ta e="T492" id="Seg_13208" s="T490">top.int.concr</ta>
            <ta e="T495" id="Seg_13209" s="T494">top.ext</ta>
            <ta e="T497" id="Seg_13210" s="T496">0.top.int.concr</ta>
            <ta e="T507" id="Seg_13211" s="T505">0.top.int.concr</ta>
            <ta e="T516" id="Seg_13212" s="T515">top.int.concr</ta>
            <ta e="T521" id="Seg_13213" s="T520">0.top.int.concr</ta>
            <ta e="T532" id="Seg_13214" s="T531">top.int.concr</ta>
            <ta e="T543" id="Seg_13215" s="T542">0.top.int.concr</ta>
            <ta e="T545" id="Seg_13216" s="T544">top.int.concr</ta>
            <ta e="T554" id="Seg_13217" s="T553">top.int.concr</ta>
            <ta e="T559" id="Seg_13218" s="T558">top.int.concr</ta>
            <ta e="T570" id="Seg_13219" s="T569">top.int.concr</ta>
            <ta e="T576" id="Seg_13220" s="T575">0.top.int.concr</ta>
            <ta e="T586" id="Seg_13221" s="T585">top.int.concr</ta>
            <ta e="T595" id="Seg_13222" s="T589">top.int.concr</ta>
            <ta e="T600" id="Seg_13223" s="T599">top.int.concr</ta>
            <ta e="T625" id="Seg_13224" s="T624">0.top.int.concr</ta>
            <ta e="T628" id="Seg_13225" s="T627">top.int.concr</ta>
            <ta e="T636" id="Seg_13226" s="T635">0.top.int.concr</ta>
            <ta e="T639" id="Seg_13227" s="T638">top.int.concr</ta>
            <ta e="T644" id="Seg_13228" s="T642">top.int.concr</ta>
            <ta e="T653" id="Seg_13229" s="T651">top.int.concr</ta>
            <ta e="T655" id="Seg_13230" s="T654">top.int.concr</ta>
            <ta e="T663" id="Seg_13231" s="T662">0.top.int.concr</ta>
            <ta e="T667" id="Seg_13232" s="T666">0.top.int.concr</ta>
            <ta e="T669" id="Seg_13233" s="T667">top.int.concr</ta>
            <ta e="T672" id="Seg_13234" s="T671">top.int.concr.contr</ta>
            <ta e="T684" id="Seg_13235" s="T683">0.top.int.concr</ta>
            <ta e="T706" id="Seg_13236" s="T705">top.int.concr</ta>
            <ta e="T729" id="Seg_13237" s="T728">top.int.concr</ta>
            <ta e="T737" id="Seg_13238" s="T736">top.int.concr.contr</ta>
            <ta e="T751" id="Seg_13239" s="T749">top.int.concr</ta>
            <ta e="T754" id="Seg_13240" s="T752">top.int.concr</ta>
            <ta e="T758" id="Seg_13241" s="T756">top.int.concr</ta>
            <ta e="T770" id="Seg_13242" s="T768">top.int.concr</ta>
            <ta e="T776" id="Seg_13243" s="T775">0.top.int.concr</ta>
            <ta e="T779" id="Seg_13244" s="T778">top.int.concr</ta>
            <ta e="T787" id="Seg_13245" s="T786">0.top.int.abstr</ta>
            <ta e="T790" id="Seg_13246" s="T789">0.top.int.concr</ta>
            <ta e="T798" id="Seg_13247" s="T797">0.top.int.concr</ta>
            <ta e="T802" id="Seg_13248" s="T801">0.top.int.concr</ta>
            <ta e="T806" id="Seg_13249" s="T805">top.int.concr</ta>
            <ta e="T811" id="Seg_13250" s="T809">top.int.concr</ta>
            <ta e="T814" id="Seg_13251" s="T813">top.int.concr</ta>
            <ta e="T818" id="Seg_13252" s="T817">top.int.concr</ta>
            <ta e="T825" id="Seg_13253" s="T824">0.top.int.concr</ta>
            <ta e="T845" id="Seg_13254" s="T844">0.top.int.abstr</ta>
            <ta e="T849" id="Seg_13255" s="T848">top.int.concr</ta>
            <ta e="T857" id="Seg_13256" s="T856">0.top.int.abstr</ta>
            <ta e="T860" id="Seg_13257" s="T859">top.int.concr</ta>
            <ta e="T871" id="Seg_13258" s="T870">0.top.int.concr</ta>
            <ta e="T888" id="Seg_13259" s="T887">top.int.concr</ta>
            <ta e="T895" id="Seg_13260" s="T894">0.top.int.concr</ta>
            <ta e="T897" id="Seg_13261" s="T896">top.int.concr</ta>
            <ta e="T911" id="Seg_13262" s="T910">0.top.int.concr</ta>
            <ta e="T914" id="Seg_13263" s="T913">top.int.concr</ta>
            <ta e="T923" id="Seg_13264" s="T922">top.int.concr</ta>
            <ta e="T929" id="Seg_13265" s="T928">0.top.int.concr</ta>
            <ta e="T940" id="Seg_13266" s="T938">top.int.concr</ta>
            <ta e="T946" id="Seg_13267" s="T945">0.top.int.abstr</ta>
            <ta e="T952" id="Seg_13268" s="T951">top.int.concr</ta>
            <ta e="T957" id="Seg_13269" s="T956">top.int.concr</ta>
            <ta e="T962" id="Seg_13270" s="T961">top.int.concr</ta>
            <ta e="T966" id="Seg_13271" s="T965">0.top.int.concr</ta>
            <ta e="T968" id="Seg_13272" s="T967">0.top.int.concr</ta>
            <ta e="T980" id="Seg_13273" s="T979">0.top.int.concr</ta>
            <ta e="T985" id="Seg_13274" s="T984">0.top.int.concr</ta>
            <ta e="T995" id="Seg_13275" s="T994">0.top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc-ChVD">
            <ta e="T5" id="Seg_13276" s="T2">foc.nar</ta>
            <ta e="T13" id="Seg_13277" s="T7">foc.int</ta>
            <ta e="T16" id="Seg_13278" s="T14">foc.wid</ta>
            <ta e="T22" id="Seg_13279" s="T17">foc.int</ta>
            <ta e="T27" id="Seg_13280" s="T22">foc.int</ta>
            <ta e="T33" id="Seg_13281" s="T31">foc.nar</ta>
            <ta e="T40" id="Seg_13282" s="T34">foc.int</ta>
            <ta e="T58" id="Seg_13283" s="T55">foc.int</ta>
            <ta e="T64" id="Seg_13284" s="T61">foc.int</ta>
            <ta e="T70" id="Seg_13285" s="T64">foc.int</ta>
            <ta e="T75" id="Seg_13286" s="T74">foc.nar</ta>
            <ta e="T84" id="Seg_13287" s="T83">foc.nar</ta>
            <ta e="T106" id="Seg_13288" s="T105">foc.nar</ta>
            <ta e="T115" id="Seg_13289" s="T113">foc.int</ta>
            <ta e="T127" id="Seg_13290" s="T124">foc.int</ta>
            <ta e="T154" id="Seg_13291" s="T153">foc.nar</ta>
            <ta e="T190" id="Seg_13292" s="T186">foc.wid</ta>
            <ta e="T199" id="Seg_13293" s="T198">foc.int</ta>
            <ta e="T206" id="Seg_13294" s="T205">foc.wid</ta>
            <ta e="T208" id="Seg_13295" s="T206">foc.int</ta>
            <ta e="T267" id="Seg_13296" s="T265">foc.int</ta>
            <ta e="T272" id="Seg_13297" s="T270">foc.nar</ta>
            <ta e="T283" id="Seg_13298" s="T281">foc.int</ta>
            <ta e="T288" id="Seg_13299" s="T283">foc.wid</ta>
            <ta e="T294" id="Seg_13300" s="T292">foc.int</ta>
            <ta e="T296" id="Seg_13301" s="T294">foc.int</ta>
            <ta e="T298" id="Seg_13302" s="T296">foc.int</ta>
            <ta e="T303" id="Seg_13303" s="T302">foc.int</ta>
            <ta e="T316" id="Seg_13304" s="T309">foc.int</ta>
            <ta e="T322" id="Seg_13305" s="T321">foc.int</ta>
            <ta e="T330" id="Seg_13306" s="T329">foc.int</ta>
            <ta e="T335" id="Seg_13307" s="T332">foc.int</ta>
            <ta e="T338" id="Seg_13308" s="T335">foc.int</ta>
            <ta e="T339" id="Seg_13309" s="T338">foc.int</ta>
            <ta e="T341" id="Seg_13310" s="T339">foc.int</ta>
            <ta e="T347" id="Seg_13311" s="T346">foc.int</ta>
            <ta e="T351" id="Seg_13312" s="T350">foc.int</ta>
            <ta e="T354" id="Seg_13313" s="T351">foc.int</ta>
            <ta e="T362" id="Seg_13314" s="T360">foc.int</ta>
            <ta e="T363" id="Seg_13315" s="T362">foc.int</ta>
            <ta e="T368" id="Seg_13316" s="T363">foc.int</ta>
            <ta e="T371" id="Seg_13317" s="T369">foc.int</ta>
            <ta e="T381" id="Seg_13318" s="T377">foc.int</ta>
            <ta e="T384" id="Seg_13319" s="T382">foc.int</ta>
            <ta e="T387" id="Seg_13320" s="T385">foc.wid</ta>
            <ta e="T391" id="Seg_13321" s="T390">foc.int</ta>
            <ta e="T393" id="Seg_13322" s="T392">foc.nar</ta>
            <ta e="T398" id="Seg_13323" s="T397">foc.nar</ta>
            <ta e="T404" id="Seg_13324" s="T402">foc.nar</ta>
            <ta e="T412" id="Seg_13325" s="T409">foc.int</ta>
            <ta e="T414" id="Seg_13326" s="T413">foc.nar</ta>
            <ta e="T419" id="Seg_13327" s="T418">foc.int</ta>
            <ta e="T428" id="Seg_13328" s="T427">foc.nar</ta>
            <ta e="T432" id="Seg_13329" s="T430">foc.int</ta>
            <ta e="T439" id="Seg_13330" s="T435">foc.int</ta>
            <ta e="T444" id="Seg_13331" s="T440">foc.int</ta>
            <ta e="T457" id="Seg_13332" s="T454">foc.wid</ta>
            <ta e="T469" id="Seg_13333" s="T467">foc.int</ta>
            <ta e="T471" id="Seg_13334" s="T470">foc.int</ta>
            <ta e="T475" id="Seg_13335" s="T472">foc.int</ta>
            <ta e="T8" id="Seg_13336" s="T475">foc.int</ta>
            <ta e="T480" id="Seg_13337" s="T478">foc.int</ta>
            <ta e="T488" id="Seg_13338" s="T482">foc.wid</ta>
            <ta e="T493" id="Seg_13339" s="T492">foc.int</ta>
            <ta e="T498" id="Seg_13340" s="T497">foc.nar</ta>
            <ta e="T501" id="Seg_13341" s="T499">foc.int</ta>
            <ta e="T507" id="Seg_13342" s="T504">foc.int</ta>
            <ta e="T515" id="Seg_13343" s="T512">foc.int</ta>
            <ta e="T517" id="Seg_13344" s="T516">foc.nar</ta>
            <ta e="T521" id="Seg_13345" s="T520">foc.int</ta>
            <ta e="T529" id="Seg_13346" s="T528">foc.nar</ta>
            <ta e="T539" id="Seg_13347" s="T537">foc.int</ta>
            <ta e="T543" id="Seg_13348" s="T542">foc.int</ta>
            <ta e="T548" id="Seg_13349" s="T545">foc.int</ta>
            <ta e="T555" id="Seg_13350" s="T554">foc.ver</ta>
            <ta e="T561" id="Seg_13351" s="T559">foc.int</ta>
            <ta e="T572" id="Seg_13352" s="T570">foc.int</ta>
            <ta e="T577" id="Seg_13353" s="T573">foc.int</ta>
            <ta e="T578" id="Seg_13354" s="T577">foc.nar</ta>
            <ta e="T588" id="Seg_13355" s="T587">foc.nar</ta>
            <ta e="T599" id="Seg_13356" s="T595">foc.int</ta>
            <ta e="T601" id="Seg_13357" s="T600">foc.int</ta>
            <ta e="T602" id="Seg_13358" s="T601">foc.nar</ta>
            <ta e="T605" id="Seg_13359" s="T604">foc.nar</ta>
            <ta e="T614" id="Seg_13360" s="T610">foc.int</ta>
            <ta e="T625" id="Seg_13361" s="T622">foc.int</ta>
            <ta e="T631" id="Seg_13362" s="T629">foc.int</ta>
            <ta e="T634" id="Seg_13363" s="T631">foc.nar</ta>
            <ta e="T641" id="Seg_13364" s="T640">foc.int</ta>
            <ta e="T648" id="Seg_13365" s="T646">foc.int</ta>
            <ta e="T657" id="Seg_13366" s="T656">foc.nar</ta>
            <ta e="T663" id="Seg_13367" s="T660">foc.int</ta>
            <ta e="T667" id="Seg_13368" s="T664">foc.int</ta>
            <ta e="T670" id="Seg_13369" s="T669">foc.int</ta>
            <ta e="T675" id="Seg_13370" s="T672">foc.int</ta>
            <ta e="T683" id="Seg_13371" s="T681">foc.nar</ta>
            <ta e="T704" id="Seg_13372" s="T698">foc.wid</ta>
            <ta e="T710" id="Seg_13373" s="T707">foc.nar</ta>
            <ta e="T731" id="Seg_13374" s="T729">foc.int</ta>
            <ta e="T741" id="Seg_13375" s="T737">foc.int</ta>
            <ta e="T762" id="Seg_13376" s="T760">foc.int</ta>
            <ta e="T772" id="Seg_13377" s="T770">foc.int</ta>
            <ta e="T776" id="Seg_13378" s="T772">foc.int</ta>
            <ta e="T781" id="Seg_13379" s="T780">foc.int</ta>
            <ta e="T787" id="Seg_13380" s="T784">foc.wid</ta>
            <ta e="T789" id="Seg_13381" s="T788">foc.nar</ta>
            <ta e="T798" id="Seg_13382" s="T794">foc.int</ta>
            <ta e="T802" id="Seg_13383" s="T800">foc.int</ta>
            <ta e="T807" id="Seg_13384" s="T806">foc.nar</ta>
            <ta e="T812" id="Seg_13385" s="T811">foc.int</ta>
            <ta e="T817" id="Seg_13386" s="T814">foc.int</ta>
            <ta e="T821" id="Seg_13387" s="T818">foc.int</ta>
            <ta e="T825" id="Seg_13388" s="T822">foc.int</ta>
            <ta e="T845" id="Seg_13389" s="T841">foc.wid</ta>
            <ta e="T850" id="Seg_13390" s="T849">foc.int</ta>
            <ta e="T857" id="Seg_13391" s="T851">foc.wid</ta>
            <ta e="T862" id="Seg_13392" s="T860">foc.int</ta>
            <ta e="T871" id="Seg_13393" s="T870">foc.nar</ta>
            <ta e="T891" id="Seg_13394" s="T889">foc.int</ta>
            <ta e="T895" id="Seg_13395" s="T893">foc.int</ta>
            <ta e="T900" id="Seg_13396" s="T897">foc.int</ta>
            <ta e="T909" id="Seg_13397" s="T908">foc.nar</ta>
            <ta e="T915" id="Seg_13398" s="T914">foc.int</ta>
            <ta e="T924" id="Seg_13399" s="T923">foc.int</ta>
            <ta e="T928" id="Seg_13400" s="T924">foc.nar</ta>
            <ta e="T938" id="Seg_13401" s="T937">foc.wid</ta>
            <ta e="T943" id="Seg_13402" s="T941">foc.int</ta>
            <ta e="T946" id="Seg_13403" s="T944">foc.wid</ta>
            <ta e="T953" id="Seg_13404" s="T952">foc.nar</ta>
            <ta e="T960" id="Seg_13405" s="T959">foc.int</ta>
            <ta e="T965" id="Seg_13406" s="T962">foc.int</ta>
            <ta e="T966" id="Seg_13407" s="T965">foc.int</ta>
            <ta e="T968" id="Seg_13408" s="T966">foc.int</ta>
            <ta e="T980" id="Seg_13409" s="T975">foc.int</ta>
            <ta e="T983" id="Seg_13410" s="T981">foc.wid</ta>
            <ta e="T984" id="Seg_13411" s="T983">foc.nar</ta>
            <ta e="T995" id="Seg_13412" s="T992">foc.int</ta>
            <ta e="T999" id="Seg_13413" s="T996">foc.wid</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR-ChVD">
            <ta e="T1" id="Seg_13414" s="T0">RUS:cult</ta>
            <ta e="T6" id="Seg_13415" s="T5">RUS:cult</ta>
            <ta e="T11" id="Seg_13416" s="T10">RUS:cult</ta>
            <ta e="T12" id="Seg_13417" s="T11">RUS:core</ta>
            <ta e="T14" id="Seg_13418" s="T13">RUS:cult</ta>
            <ta e="T15" id="Seg_13419" s="T14">RUS:cult</ta>
            <ta e="T17" id="Seg_13420" s="T16">RUS:cult</ta>
            <ta e="T9" id="Seg_13421" s="T19">RUS:cult</ta>
            <ta e="T20" id="Seg_13422" s="T9">RUS:cult</ta>
            <ta e="T23" id="Seg_13423" s="T22">RUS:cult</ta>
            <ta e="T28" id="Seg_13424" s="T27">RUS:cult</ta>
            <ta e="T32" id="Seg_13425" s="T31">RUS:cult</ta>
            <ta e="T33" id="Seg_13426" s="T32">RUS:cult</ta>
            <ta e="T36" id="Seg_13427" s="T35">RUS:cult</ta>
            <ta e="T37" id="Seg_13428" s="T36">RUS:cult</ta>
            <ta e="T52" id="Seg_13429" s="T51">RUS:cult</ta>
            <ta e="T56" id="Seg_13430" s="T55">RUS:cult</ta>
            <ta e="T57" id="Seg_13431" s="T56">RUS:core</ta>
            <ta e="T65" id="Seg_13432" s="T64">EV:core</ta>
            <ta e="T75" id="Seg_13433" s="T74">RUS:cult</ta>
            <ta e="T481" id="Seg_13434" s="T80">RUS:mod</ta>
            <ta e="T84" id="Seg_13435" s="T83">RUS:cult</ta>
            <ta e="T105" id="Seg_13436" s="T104">RUS:cult</ta>
            <ta e="T109" id="Seg_13437" s="T108">RUS:cult</ta>
            <ta e="T111" id="Seg_13438" s="T110">RUS:cult</ta>
            <ta e="T117" id="Seg_13439" s="T116">RUS:cult</ta>
            <ta e="T132" id="Seg_13440" s="T131">RUS:mod</ta>
            <ta e="T135" id="Seg_13441" s="T134">RUS:core</ta>
            <ta e="T162" id="Seg_13442" s="T161">RUS:cult</ta>
            <ta e="T172" id="Seg_13443" s="T171">RUS:cult</ta>
            <ta e="T174" id="Seg_13444" s="T173">RUS:cult</ta>
            <ta e="T177" id="Seg_13445" s="T176">RUS:cult</ta>
            <ta e="T178" id="Seg_13446" s="T177">RUS:cult</ta>
            <ta e="T179" id="Seg_13447" s="T178">RUS:cult</ta>
            <ta e="T181" id="Seg_13448" s="T180">RUS:gram</ta>
            <ta e="T189" id="Seg_13449" s="T188">EV:gram (DIM)</ta>
            <ta e="T193" id="Seg_13450" s="T192">RUS:cult</ta>
            <ta e="T207" id="Seg_13451" s="T206">RUS:cult</ta>
            <ta e="T216" id="Seg_13452" s="T215">RUS:cult</ta>
            <ta e="T219" id="Seg_13453" s="T218">RUS:cult</ta>
            <ta e="T225" id="Seg_13454" s="T224">RUS:mod</ta>
            <ta e="T230" id="Seg_13455" s="T229">RUS:mod</ta>
            <ta e="T253" id="Seg_13456" s="T252">RUS:gram</ta>
            <ta e="T255" id="Seg_13457" s="T253">RUS:mod</ta>
            <ta e="T256" id="Seg_13458" s="T255">RUS:cult</ta>
            <ta e="T265" id="Seg_13459" s="T264">RUS:cult</ta>
            <ta e="T275" id="Seg_13460" s="T274">RUS:cult</ta>
            <ta e="T276" id="Seg_13461" s="T275">RUS:gram</ta>
            <ta e="T278" id="Seg_13462" s="T277">RUS:gram</ta>
            <ta e="T293" id="Seg_13463" s="T292">RUS:cult</ta>
            <ta e="T295" id="Seg_13464" s="T294">RUS:cult</ta>
            <ta e="T303" id="Seg_13465" s="T302">RUS:cult</ta>
            <ta e="T305" id="Seg_13466" s="T304">EV:core</ta>
            <ta e="T309" id="Seg_13467" s="T308">RUS:cult</ta>
            <ta e="T313" id="Seg_13468" s="T312">EV:core</ta>
            <ta e="T322" id="Seg_13469" s="T321">RUS:cult</ta>
            <ta e="T328" id="Seg_13470" s="T327">RUS:cult</ta>
            <ta e="T332" id="Seg_13471" s="T331">RUS:cult</ta>
            <ta e="T342" id="Seg_13472" s="T341">EV:core</ta>
            <ta e="T346" id="Seg_13473" s="T345">RUS:mod</ta>
            <ta e="T358" id="Seg_13474" s="T357">RUS:cult</ta>
            <ta e="T361" id="Seg_13475" s="T360">RUS:cult</ta>
            <ta e="T365" id="Seg_13476" s="T364">EV:core</ta>
            <ta e="T367" id="Seg_13477" s="T366">RUS:cult</ta>
            <ta e="T375" id="Seg_13478" s="T374">RUS:cult</ta>
            <ta e="T377" id="Seg_13479" s="T376">RUS:mod</ta>
            <ta e="T386" id="Seg_13480" s="T385">RUS:cult</ta>
            <ta e="T390" id="Seg_13481" s="T389">RUS:cult</ta>
            <ta e="T392" id="Seg_13482" s="T391">RUS:mod</ta>
            <ta e="T394" id="Seg_13483" s="T393">RUS:core</ta>
            <ta e="T396" id="Seg_13484" s="T395">RUS:cult</ta>
            <ta e="T398" id="Seg_13485" s="T397">RUS:cult</ta>
            <ta e="T418" id="Seg_13486" s="T417">RUS:cult</ta>
            <ta e="T419" id="Seg_13487" s="T418">RUS:cult</ta>
            <ta e="T427" id="Seg_13488" s="T426">RUS:cult</ta>
            <ta e="T456" id="Seg_13489" s="T455">RUS:cult</ta>
            <ta e="T459" id="Seg_13490" s="T458">RUS:cult</ta>
            <ta e="T468" id="Seg_13491" s="T467">RUS:core</ta>
            <ta e="T8" id="Seg_13492" s="T475">RUS:cult</ta>
            <ta e="T485" id="Seg_13493" s="T484">RUS:cult</ta>
            <ta e="T492" id="Seg_13494" s="T491">RUS:cult</ta>
            <ta e="T495" id="Seg_13495" s="T494">RUS:cult</ta>
            <ta e="T498" id="Seg_13496" s="T497">EV:gram (INTNS)</ta>
            <ta e="T512" id="Seg_13497" s="T511">EV:core</ta>
            <ta e="T519" id="Seg_13498" s="T518">RUS:cult</ta>
            <ta e="T565" id="Seg_13499" s="T563">RUS:mod</ta>
            <ta e="T598" id="Seg_13500" s="T597">RUS:cult</ta>
            <ta e="T603" id="Seg_13501" s="T602">RUS:cult</ta>
            <ta e="T623" id="Seg_13502" s="T622">RUS:cult</ta>
            <ta e="T639" id="Seg_13503" s="T638">RUS:cult</ta>
            <ta e="T671" id="Seg_13504" s="T670">RUS:gram</ta>
            <ta e="T680" id="Seg_13505" s="T679">RUS:gram</ta>
            <ta e="T696" id="Seg_13506" s="T695">RUS:cult</ta>
            <ta e="T698" id="Seg_13507" s="T697">RUS:disc</ta>
            <ta e="T701" id="Seg_13508" s="T700">RUS:cult</ta>
            <ta e="T702" id="Seg_13509" s="T701">RUS:cult</ta>
            <ta e="T703" id="Seg_13510" s="T702">RUS:cult</ta>
            <ta e="T704" id="Seg_13511" s="T703">RUS:mod</ta>
            <ta e="T718" id="Seg_13512" s="T717">RUS:gram</ta>
            <ta e="T729" id="Seg_13513" s="T728">RUS:cult</ta>
            <ta e="T732" id="Seg_13514" s="T731">RUS:cult</ta>
            <ta e="T736" id="Seg_13515" s="T735">RUS:gram</ta>
            <ta e="T756" id="Seg_13516" s="T755">RUS:gram</ta>
            <ta e="T757" id="Seg_13517" s="T756">RUS:cult</ta>
            <ta e="T761" id="Seg_13518" s="T760">RUS:cult</ta>
            <ta e="T763" id="Seg_13519" s="T762">RUS:cult</ta>
            <ta e="T786" id="Seg_13520" s="T785">RUS:cult</ta>
            <ta e="T788" id="Seg_13521" s="T787">RUS:gram</ta>
            <ta e="T844" id="Seg_13522" s="T843">RUS:cult</ta>
            <ta e="T846" id="Seg_13523" s="T845">RUS:gram</ta>
            <ta e="T847" id="Seg_13524" s="T846">RUS:cult</ta>
            <ta e="T853" id="Seg_13525" s="T852">RUS:cult</ta>
            <ta e="T855" id="Seg_13526" s="T854">RUS:cult</ta>
            <ta e="T861" id="Seg_13527" s="T860">RUS:cult</ta>
            <ta e="T866" id="Seg_13528" s="T865">RUS:cult</ta>
            <ta e="T867" id="Seg_13529" s="T866">RUS:cult</ta>
            <ta e="T873" id="Seg_13530" s="T872">RUS:cult</ta>
            <ta e="T881" id="Seg_13531" s="T880">RUS:core</ta>
            <ta e="T894" id="Seg_13532" s="T893">RUS:cult</ta>
            <ta e="T895" id="Seg_13533" s="T894">RUS:core</ta>
            <ta e="T898" id="Seg_13534" s="T897">RUS:cult</ta>
            <ta e="T904" id="Seg_13535" s="T903">RUS:cult</ta>
            <ta e="T905" id="Seg_13536" s="T904">RUS:cult</ta>
            <ta e="T909" id="Seg_13537" s="T908">RUS:cult</ta>
            <ta e="T912" id="Seg_13538" s="T911">RUS:cult</ta>
            <ta e="T921" id="Seg_13539" s="T920">RUS:cult</ta>
            <ta e="T935" id="Seg_13540" s="T937">RUS:cult</ta>
            <ta e="T941" id="Seg_13541" s="T940">RUS:mod</ta>
            <ta e="T967" id="Seg_13542" s="T966">RUS:cult</ta>
            <ta e="T971" id="Seg_13543" s="T970">RUS:cult</ta>
            <ta e="T975" id="Seg_13544" s="T974">RUS:cult</ta>
            <ta e="T977" id="Seg_13545" s="T976">RUS:cult</ta>
            <ta e="T981" id="Seg_13546" s="T980">RUS:gram</ta>
            <ta e="T982" id="Seg_13547" s="T981">RUS:cult</ta>
            <ta e="T984" id="Seg_13548" s="T983">RUS:cult</ta>
            <ta e="T987" id="Seg_13549" s="T986">RUS:cult</ta>
            <ta e="T992" id="Seg_13550" s="T991">RUS:mod</ta>
            <ta e="T996" id="Seg_13551" s="T995">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-ChVD">
            <ta e="T52" id="Seg_13552" s="T51">Vsub</ta>
            <ta e="T56" id="Seg_13553" s="T55">Csub</ta>
            <ta e="T75" id="Seg_13554" s="T74">Vsub</ta>
            <ta e="T105" id="Seg_13555" s="T104">Vsub</ta>
            <ta e="T109" id="Seg_13556" s="T108">Vsub</ta>
            <ta e="T135" id="Seg_13557" s="T134">fortition Vsub</ta>
            <ta e="T216" id="Seg_13558" s="T215">Csub Vsub</ta>
            <ta e="T256" id="Seg_13559" s="T255">medVins Vsub</ta>
            <ta e="T265" id="Seg_13560" s="T264">inVins</ta>
            <ta e="T275" id="Seg_13561" s="T274">lenition</ta>
            <ta e="T293" id="Seg_13562" s="T292">lenition</ta>
            <ta e="T295" id="Seg_13563" s="T294">Csub</ta>
            <ta e="T309" id="Seg_13564" s="T308">Vsub</ta>
            <ta e="T322" id="Seg_13565" s="T321">medCdel</ta>
            <ta e="T332" id="Seg_13566" s="T331">lenition</ta>
            <ta e="T346" id="Seg_13567" s="T345">fortition Vsub fortition Vsub</ta>
            <ta e="T361" id="Seg_13568" s="T360">Csub</ta>
            <ta e="T377" id="Seg_13569" s="T376">Vsub</ta>
            <ta e="T390" id="Seg_13570" s="T389">inVins</ta>
            <ta e="T398" id="Seg_13571" s="T397">inVins</ta>
            <ta e="T427" id="Seg_13572" s="T426">lenition</ta>
            <ta e="T459" id="Seg_13573" s="T458">Csub</ta>
            <ta e="T8" id="Seg_13574" s="T475">Csub</ta>
            <ta e="T495" id="Seg_13575" s="T494">lenition</ta>
            <ta e="T519" id="Seg_13576" s="T518">finCdel</ta>
            <ta e="T598" id="Seg_13577" s="T597">Vsub medCdel</ta>
            <ta e="T603" id="Seg_13578" s="T602">Vsub medCdel</ta>
            <ta e="T701" id="Seg_13579" s="T700">Vsub</ta>
            <ta e="T729" id="Seg_13580" s="T728">lenition</ta>
            <ta e="T757" id="Seg_13581" s="T756">Vsub</ta>
            <ta e="T761" id="Seg_13582" s="T760">finVdel</ta>
            <ta e="T763" id="Seg_13583" s="T762">finVdel</ta>
            <ta e="T853" id="Seg_13584" s="T852">lenition Vsub</ta>
            <ta e="T855" id="Seg_13585" s="T854">lenition</ta>
            <ta e="T861" id="Seg_13586" s="T860">lenition</ta>
            <ta e="T873" id="Seg_13587" s="T872">fortition medVins fortition</ta>
            <ta e="T904" id="Seg_13588" s="T903">Vsub</ta>
            <ta e="T905" id="Seg_13589" s="T904">Vsub</ta>
            <ta e="T909" id="Seg_13590" s="T908">Vsub finVdel</ta>
            <ta e="T912" id="Seg_13591" s="T911">Vsub finVins</ta>
            <ta e="T921" id="Seg_13592" s="T920">Vsub</ta>
            <ta e="T941" id="Seg_13593" s="T940">Vsub Csub Vsub</ta>
            <ta e="T967" id="Seg_13594" s="T966">finCdel</ta>
            <ta e="T975" id="Seg_13595" s="T974">finCdel</ta>
            <ta e="T977" id="Seg_13596" s="T976">finCdel</ta>
            <ta e="T984" id="Seg_13597" s="T983">Csub</ta>
            <ta e="T987" id="Seg_13598" s="T986">lenition</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-ChVD">
            <ta e="T1" id="Seg_13599" s="T0">dir:infl</ta>
            <ta e="T6" id="Seg_13600" s="T5">dir:bare</ta>
            <ta e="T11" id="Seg_13601" s="T10">dir:infl</ta>
            <ta e="T12" id="Seg_13602" s="T11">dir:infl</ta>
            <ta e="T14" id="Seg_13603" s="T13">dir:infl</ta>
            <ta e="T15" id="Seg_13604" s="T14">indir:infl</ta>
            <ta e="T17" id="Seg_13605" s="T16">indir:infl</ta>
            <ta e="T9" id="Seg_13606" s="T19">dir:bare</ta>
            <ta e="T20" id="Seg_13607" s="T9">dir:infl</ta>
            <ta e="T23" id="Seg_13608" s="T22">dir:infl</ta>
            <ta e="T28" id="Seg_13609" s="T27">indir:infl</ta>
            <ta e="T32" id="Seg_13610" s="T31">dir:bare</ta>
            <ta e="T33" id="Seg_13611" s="T32">dir:infl</ta>
            <ta e="T36" id="Seg_13612" s="T35">indir:bare</ta>
            <ta e="T37" id="Seg_13613" s="T36">dir:infl</ta>
            <ta e="T52" id="Seg_13614" s="T51">dir:infl</ta>
            <ta e="T56" id="Seg_13615" s="T55">dir:bare</ta>
            <ta e="T57" id="Seg_13616" s="T56">indir:infl</ta>
            <ta e="T65" id="Seg_13617" s="T64">dir:infl</ta>
            <ta e="T75" id="Seg_13618" s="T74">dir:infl</ta>
            <ta e="T481" id="Seg_13619" s="T80">dir:bare</ta>
            <ta e="T84" id="Seg_13620" s="T83">dir:bare</ta>
            <ta e="T105" id="Seg_13621" s="T104">dir:infl</ta>
            <ta e="T109" id="Seg_13622" s="T108">dir:infl</ta>
            <ta e="T111" id="Seg_13623" s="T110">dir:bare</ta>
            <ta e="T117" id="Seg_13624" s="T116">dir:infl</ta>
            <ta e="T132" id="Seg_13625" s="T131">dir:bare</ta>
            <ta e="T135" id="Seg_13626" s="T134">dir:bare</ta>
            <ta e="T162" id="Seg_13627" s="T161">dir:bare</ta>
            <ta e="T172" id="Seg_13628" s="T171">dir:bare</ta>
            <ta e="T174" id="Seg_13629" s="T173">dir:bare</ta>
            <ta e="T177" id="Seg_13630" s="T176">dir:bare</ta>
            <ta e="T178" id="Seg_13631" s="T177">dir:bare</ta>
            <ta e="T179" id="Seg_13632" s="T178">indir:infl</ta>
            <ta e="T181" id="Seg_13633" s="T180">dir:bare</ta>
            <ta e="T193" id="Seg_13634" s="T192">dir:infl</ta>
            <ta e="T207" id="Seg_13635" s="T206">dir:infl</ta>
            <ta e="T216" id="Seg_13636" s="T215">dir:bare</ta>
            <ta e="T219" id="Seg_13637" s="T218">dir:bare</ta>
            <ta e="T225" id="Seg_13638" s="T224">dir:bare</ta>
            <ta e="T230" id="Seg_13639" s="T229">dir:bare</ta>
            <ta e="T253" id="Seg_13640" s="T252">dir:bare</ta>
            <ta e="T255" id="Seg_13641" s="T253">dir:bare</ta>
            <ta e="T256" id="Seg_13642" s="T255">indir:bare</ta>
            <ta e="T265" id="Seg_13643" s="T264">dir:infl</ta>
            <ta e="T275" id="Seg_13644" s="T274">dir:infl</ta>
            <ta e="T276" id="Seg_13645" s="T275">dir:bare</ta>
            <ta e="T278" id="Seg_13646" s="T277">dir:bare</ta>
            <ta e="T293" id="Seg_13647" s="T292">dir:infl</ta>
            <ta e="T295" id="Seg_13648" s="T294">dir:infl</ta>
            <ta e="T303" id="Seg_13649" s="T302">dir:infl</ta>
            <ta e="T305" id="Seg_13650" s="T304">dir:infl</ta>
            <ta e="T309" id="Seg_13651" s="T308">dir:infl</ta>
            <ta e="T313" id="Seg_13652" s="T312">dir:infl</ta>
            <ta e="T322" id="Seg_13653" s="T321">indir:infl</ta>
            <ta e="T328" id="Seg_13654" s="T327">dir:infl</ta>
            <ta e="T332" id="Seg_13655" s="T331">dir:infl</ta>
            <ta e="T342" id="Seg_13656" s="T341">dir:infl</ta>
            <ta e="T346" id="Seg_13657" s="T345">dir:bare</ta>
            <ta e="T358" id="Seg_13658" s="T357">dir:bare</ta>
            <ta e="T361" id="Seg_13659" s="T360">dir:infl</ta>
            <ta e="T365" id="Seg_13660" s="T364">dir:bare</ta>
            <ta e="T367" id="Seg_13661" s="T366">dir:infl</ta>
            <ta e="T377" id="Seg_13662" s="T376">dir:bare</ta>
            <ta e="T386" id="Seg_13663" s="T385">dir:bare</ta>
            <ta e="T390" id="Seg_13664" s="T389">dir:infl</ta>
            <ta e="T394" id="Seg_13665" s="T393">dir:bare</ta>
            <ta e="T396" id="Seg_13666" s="T395">dir:bare</ta>
            <ta e="T398" id="Seg_13667" s="T397">dir:infl</ta>
            <ta e="T418" id="Seg_13668" s="T417">dir:infl</ta>
            <ta e="T419" id="Seg_13669" s="T418">indir:infl</ta>
            <ta e="T427" id="Seg_13670" s="T426">dir:infl</ta>
            <ta e="T456" id="Seg_13671" s="T455">dir:bare</ta>
            <ta e="T459" id="Seg_13672" s="T458">dir:infl</ta>
            <ta e="T468" id="Seg_13673" s="T467">dir:infl</ta>
            <ta e="T8" id="Seg_13674" s="T475">dir:bare</ta>
            <ta e="T485" id="Seg_13675" s="T484">dir:bare</ta>
            <ta e="T492" id="Seg_13676" s="T491">dir:infl</ta>
            <ta e="T495" id="Seg_13677" s="T494">dir:infl</ta>
            <ta e="T512" id="Seg_13678" s="T511">dir:bare</ta>
            <ta e="T519" id="Seg_13679" s="T518">dir:bare</ta>
            <ta e="T565" id="Seg_13680" s="T563">dir:bare</ta>
            <ta e="T598" id="Seg_13681" s="T597">dir:infl</ta>
            <ta e="T603" id="Seg_13682" s="T602">dir:infl</ta>
            <ta e="T623" id="Seg_13683" s="T622">dir:bare</ta>
            <ta e="T639" id="Seg_13684" s="T638">dir:infl</ta>
            <ta e="T671" id="Seg_13685" s="T670">dir:bare</ta>
            <ta e="T680" id="Seg_13686" s="T679">dir:bare</ta>
            <ta e="T696" id="Seg_13687" s="T695">dir:bare</ta>
            <ta e="T701" id="Seg_13688" s="T700">dir:infl</ta>
            <ta e="T702" id="Seg_13689" s="T701">indir:infl</ta>
            <ta e="T703" id="Seg_13690" s="T702">indir:infl</ta>
            <ta e="T704" id="Seg_13691" s="T703">dir:bare</ta>
            <ta e="T718" id="Seg_13692" s="T717">dir:bare</ta>
            <ta e="T729" id="Seg_13693" s="T728">dir:infl</ta>
            <ta e="T736" id="Seg_13694" s="T735">dir:bare</ta>
            <ta e="T756" id="Seg_13695" s="T755">dir:bare</ta>
            <ta e="T757" id="Seg_13696" s="T756">dir:infl</ta>
            <ta e="T761" id="Seg_13697" s="T760">dir:infl</ta>
            <ta e="T786" id="Seg_13698" s="T785">dir:bare</ta>
            <ta e="T788" id="Seg_13699" s="T787">dir:bare</ta>
            <ta e="T844" id="Seg_13700" s="T843">dir:infl</ta>
            <ta e="T853" id="Seg_13701" s="T852">dir:bare</ta>
            <ta e="T855" id="Seg_13702" s="T854">dir:infl</ta>
            <ta e="T861" id="Seg_13703" s="T860">dir:infl</ta>
            <ta e="T866" id="Seg_13704" s="T865">dir:bare</ta>
            <ta e="T867" id="Seg_13705" s="T866">dir:infl</ta>
            <ta e="T873" id="Seg_13706" s="T872">dir:bare</ta>
            <ta e="T881" id="Seg_13707" s="T880">dir:infl</ta>
            <ta e="T894" id="Seg_13708" s="T893">dir:infl</ta>
            <ta e="T895" id="Seg_13709" s="T894">indir:infl</ta>
            <ta e="T898" id="Seg_13710" s="T897">dir:infl</ta>
            <ta e="T904" id="Seg_13711" s="T903">dir:infl</ta>
            <ta e="T905" id="Seg_13712" s="T904">dir:infl</ta>
            <ta e="T909" id="Seg_13713" s="T908">dir:infl</ta>
            <ta e="T912" id="Seg_13714" s="T911">dir:infl</ta>
            <ta e="T921" id="Seg_13715" s="T920">dir:infl</ta>
            <ta e="T935" id="Seg_13716" s="T937">dir:bare</ta>
            <ta e="T941" id="Seg_13717" s="T940">dir:bare</ta>
            <ta e="T967" id="Seg_13718" s="T966">dir:infl</ta>
            <ta e="T971" id="Seg_13719" s="T970">dir:bare</ta>
            <ta e="T975" id="Seg_13720" s="T974">dir:bare</ta>
            <ta e="T977" id="Seg_13721" s="T976">dir:infl</ta>
            <ta e="T981" id="Seg_13722" s="T980">dir:bare</ta>
            <ta e="T982" id="Seg_13723" s="T981">dir:infl</ta>
            <ta e="T984" id="Seg_13724" s="T983">dir:bare</ta>
            <ta e="T987" id="Seg_13725" s="T986">dir:infl</ta>
            <ta e="T992" id="Seg_13726" s="T991">dir:bare</ta>
            <ta e="T996" id="Seg_13727" s="T995">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-ChVD">
            <ta e="T5" id="Seg_13728" s="T2">RUS:int.ins</ta>
            <ta e="T10" id="Seg_13729" s="T7">RUS:int.ins</ta>
            <ta e="T40" id="Seg_13730" s="T37">RUS:int.ins</ta>
            <ta e="T55" id="Seg_13731" s="T52">RUS:ext</ta>
            <ta e="T59" id="Seg_13732" s="T58">RUS:ext</ta>
            <ta e="T80" id="Seg_13733" s="T77">RUS:int.ins</ta>
            <ta e="T90" id="Seg_13734" s="T86">RUS:ext</ta>
            <ta e="T91" id="Seg_13735" s="T90">RUS:int.ins</ta>
            <ta e="T100" id="Seg_13736" s="T97">RUS:ext</ta>
            <ta e="T114" id="Seg_13737" s="T113">RUS:int.ins</ta>
            <ta e="T124" id="Seg_13738" s="T122">RUS:calq</ta>
            <ta e="T133" id="Seg_13739" s="T132">RUS:int.ins</ta>
            <ta e="T195" id="Seg_13740" s="T194">RUS:int.alt</ta>
            <ta e="T222" id="Seg_13741" s="T221">RUS:int.ins</ta>
            <ta e="T326" id="Seg_13742" s="T323">RUS:ext</ta>
            <ta e="T618" id="Seg_13743" s="T617">RUS:int.ins</ta>
            <ta e="T768" id="Seg_13744" s="T767">RUS:int.alt</ta>
         </annotation>
         <annotation name="fe" tierref="fe-ChVD">
            <ta e="T5" id="Seg_13745" s="T0">I came to Dudinka in 1942.</ta>
            <ta e="T14" id="Seg_13746" s="T5">In August I passed the [military] commission [and was assigned] to the non-combatant.</ta>
            <ta e="T16" id="Seg_13747" s="T14">I was sent to Norilsk.</ta>
            <ta e="T27" id="Seg_13748" s="T16">From Norilsk there I worked in a military protection unit, I got together with volunteers again.</ta>
            <ta e="T33" id="Seg_13749" s="T27">To Kranosyarsk I went on the steamer "Spartak".</ta>
            <ta e="T40" id="Seg_13750" s="T33">Then I learned in the Krasnoyarsk infantery together with tank gunners.</ta>
            <ta e="T55" id="Seg_13751" s="T40">Having learned eleven months or how long, repeating (…) in whatchamacallit, in Zvenigorod, sergeant. </ta>
            <ta e="T59" id="Seg_13752" s="T55">I took parachutes apart, paratrooper [ones].</ta>
            <ta e="T70" id="Seg_13753" s="T59">Well, I didn't learn there for a long time, with my friends, in order to not stay behind, I went together [with them].</ta>
            <ta e="T80" id="Seg_13754" s="T70">Well, I whatchamacallit, in whatchamacallit, we learned and whatchamacallit in Murom, in reserve…</ta>
            <ta e="T90" id="Seg_13755" s="T80">They were just whatchamacalliting, they were forming, some (units?).</ta>
            <ta e="T100" id="Seg_13756" s="T90">There they did that [=asked] who learned what, who did what: which units, subdivisions.</ta>
            <ta e="T109" id="Seg_13757" s="T100">Well, I whatchamacallit there, from Murom we came to whatchamacallit, to Tula.</ta>
            <ta e="T115" id="Seg_13758" s="T109">This Tula… was destroyed. [?]</ta>
            <ta e="T127" id="Seg_13759" s="T115">After we had gone eight or how many kilometers, the railroads were completely destroyed.</ta>
            <ta e="T142" id="Seg_13760" s="T127">Well, when we were whatchamacalliting, there were always raids, they flew every morning.</ta>
            <ta e="T152" id="Seg_13761" s="T142">And then it was not long, eh, we (went?) there.</ta>
            <ta e="T166" id="Seg_13762" s="T152">We went there indeed by foot, until we reached the places where this war took place.</ta>
            <ta e="T174" id="Seg_13763" s="T166">And then they whatchamacallit there, this partizan division.</ta>
            <ta e="T179" id="Seg_13764" s="T174">Oh, there, they denoted as "NOP" and "SOP".</ta>
            <ta e="T190" id="Seg_13765" s="T179">And there, to there, a river, some small river flows there.</ta>
            <ta e="T193" id="Seg_13766" s="T190">There in the village.</ta>
            <ta e="T197" id="Seg_13767" s="T193">There they were apparently defending themselves.</ta>
            <ta e="T204" id="Seg_13768" s="T197">In the morning we whatchamacallit, after we had eaten, that.</ta>
            <ta e="T208" id="Seg_13769" s="T204">"They are shooting", our commander is told.</ta>
            <ta e="T214" id="Seg_13770" s="T208">"How, for that you whatchamacallit then…</ta>
            <ta e="T229" id="Seg_13771" s="T214">If you are encircled, if you get into prison, for that they don't whatchamacallit the documents, one has to, in order to not deliver oneself up.</ta>
            <ta e="T241" id="Seg_13772" s="T229">Don't just tear apart the documents", he says, though, "whatchamacallit.</ta>
            <ta e="T251" id="Seg_13773" s="T241">Bury them or eat them yourselves", he says so.</ta>
            <ta e="T262" id="Seg_13774" s="T251">"You tore it apart and nevertheless they glue [it], they whatchamacallit, they find it."</ta>
            <ta e="T268" id="Seg_13775" s="T262">Once I left the company and ran.</ta>
            <ta e="T278" id="Seg_13776" s="T268">I noticed some [people] in the darkness: Are they Germans, who are they?</ta>
            <ta e="T288" id="Seg_13777" s="T278">I went and saw that there were five men going.</ta>
            <ta e="T296" id="Seg_13778" s="T288">And from there the Germans are coming, they are coming out of the trench.</ta>
            <ta e="T300" id="Seg_13779" s="T296">"Let's escape", they say.</ta>
            <ta e="T305" id="Seg_13780" s="T300">This man, these friends have commanders. [?]</ta>
            <ta e="T309" id="Seg_13781" s="T305">Others, from another unit.</ta>
            <ta e="T321" id="Seg_13782" s="T309">I myself let my friends and ran, apparently, and so.</ta>
            <ta e="T323" id="Seg_13783" s="T321">"Retreat!", he says.</ta>
            <ta e="T328" id="Seg_13784" s="T323">"But unnoticed", their commander says.</ta>
            <ta e="T335" id="Seg_13785" s="T328">Then I saw, the Germans are coming.</ta>
            <ta e="T345" id="Seg_13786" s="T335">They are saying something, they are chatting, I run back to where my friends were.</ta>
            <ta e="T357" id="Seg_13787" s="T345">They are communicating there, in the grass, they are shooting, indeed they want to whatchamacallit, apparently, then…</ta>
            <ta e="T362" id="Seg_13788" s="T357">To imprison, in the end I reached the trench.</ta>
            <ta e="T368" id="Seg_13789" s="T362">I came, to my friends, eh, I got into my company.</ta>
            <ta e="T377" id="Seg_13790" s="T368">And there I whatchamacallit, like a stone (…), one whatchamacallits immediately with a machine gun.</ta>
            <ta e="T381" id="Seg_13791" s="T377">More than twenty people are shot down.</ta>
            <ta e="T387" id="Seg_13792" s="T381">Some of us run back, they are patrolling.</ta>
            <ta e="T401" id="Seg_13793" s="T387">The company goes off, who does now go in front, one only guards the company as a scout. </ta>
            <ta e="T412" id="Seg_13794" s="T401">There are two people in the front, on the left and on the right also two people on each side.</ta>
            <ta e="T416" id="Seg_13795" s="T412">"Well, what shall be with us?" he says.</ta>
            <ta e="T424" id="Seg_13796" s="T416">In this village we check whether there is indeed nobody.</ta>
            <ta e="T430" id="Seg_13797" s="T424">The Germans can be seen only on the other shore.</ta>
            <ta e="T434" id="Seg_13798" s="T430">"Let's go back", he says.</ta>
            <ta e="T446" id="Seg_13799" s="T434">Well, there we went and met, we went across the river, against the stream.</ta>
            <ta e="T451" id="Seg_13800" s="T446">We cross the river, eh, and whatchamacallit.</ta>
            <ta e="T467" id="Seg_13801" s="T451">So they whatchamacallit, there is artillery: cannons, eh, such, horses, everything is whatchamacallited. </ta>
            <ta e="T471" id="Seg_13802" s="T467">They turned [them all] over and then they killed everybody.</ta>
            <ta e="T475" id="Seg_13803" s="T471">There we found one man.</ta>
            <ta e="T482" id="Seg_13804" s="T475">He is indeed half alive, somewhere, he hardly speaks.</ta>
            <ta e="T494" id="Seg_13805" s="T482">"It was just time to smoke", he says, "they whatchamacallit our company", he says.</ta>
            <ta e="T499" id="Seg_13806" s="T494">"The Germans, yes, they have just gone", he says.</ta>
            <ta e="T503" id="Seg_13807" s="T499">"Don't talk loudly", he warned.</ta>
            <ta e="T507" id="Seg_13808" s="T503">Well, and so we whatchamacallit…</ta>
            <ta e="T519" id="Seg_13809" s="T507">And there, well, eh, friend… two men were sent, there is whatchamacallit, an infirmary.</ta>
            <ta e="T525" id="Seg_13810" s="T519">Then "we will take you", they say, "I, we you."</ta>
            <ta e="T544" id="Seg_13811" s="T525">"Don't worry about me", he says, "I, eh, survive, eh, don't survive in either case", he says, "I will die now."</ta>
            <ta e="T550" id="Seg_13812" s="T544">The people are indeed lying like a padding, (from them?).</ta>
            <ta e="T553" id="Seg_13813" s="T550">So they whatchamacallit.</ta>
            <ta e="T573" id="Seg_13814" s="T553">The two men carried him there [=to the infirmary], he just said, "Don't bring me and so on," then they came back.</ta>
            <ta e="T585" id="Seg_13815" s="T573">"I don't survive in either case, why do you whatchamacallit with me", he says.</ta>
            <ta e="T589" id="Seg_13816" s="T585">In the morning it just gets light.</ta>
            <ta e="T599" id="Seg_13817" s="T589">When it got so, we met some lieutenant.</ta>
            <ta e="T601" id="Seg_13818" s="T599">He says:</ta>
            <ta e="T603" id="Seg_13819" s="T601">"From which unit are you?"</ta>
            <ta e="T606" id="Seg_13820" s="T603">"Yours, from yours", we say.</ta>
            <ta e="T610" id="Seg_13821" s="T606">Somebody of us says.</ta>
            <ta e="T615" id="Seg_13822" s="T610">"Then run together with me", he says.</ta>
            <ta e="T622" id="Seg_13823" s="T615">We went on, in order not to whatchamacallit. [?]</ta>
            <ta e="T626" id="Seg_13824" s="T622">To keep the connection he whatchamacallits apparently. [?]</ta>
            <ta e="T631" id="Seg_13825" s="T626">"And then we try to whatchamacallit."</ta>
            <ta e="T638" id="Seg_13826" s="T631">We went so for some days.</ta>
            <ta e="T641" id="Seg_13827" s="T638">We don't find our company.</ta>
            <ta e="T649" id="Seg_13828" s="T641">And then later we did, eh, we found it again.</ta>
            <ta e="T660" id="Seg_13829" s="T649">On the third day or when we go away, to whatchamacallit.</ta>
            <ta e="T676" id="Seg_13830" s="T660">We cross the river, then we (meet?), they are shooting from far away, we are also shooting from this side.</ta>
            <ta e="T686" id="Seg_13831" s="T676">And we whatchamacallit so there, they whatchamacallit to nowhere, they didn't let us.</ta>
            <ta e="T697" id="Seg_13832" s="T686">In the morning, to those who can (?), those from in front of us give this order: </ta>
            <ta e="T704" id="Seg_13833" s="T697">"Well, (at three o’clock?) you shall conquer Bryansk."</ta>
            <ta e="T712" id="Seg_13834" s="T704">Well, and there was a kind of mountain, (a small belt?).</ta>
            <ta e="T719" id="Seg_13835" s="T712">The small belt, eh, we also whatchamacallit.</ta>
            <ta e="T735" id="Seg_13836" s="T719">Again (his face?)… from before us they whatchamacallit, the Germans are lying there: they are defending themselves.</ta>
            <ta e="T741" id="Seg_13837" s="T735">And we are also shooting from this side.</ta>
            <ta e="T749" id="Seg_13838" s="T741">And then we are lying the whole night there.</ta>
            <ta e="T762" id="Seg_13839" s="T749">And in the night around ten o’clock, around two or one o’clock they send a missile.</ta>
            <ta e="T772" id="Seg_13840" s="T762">When shooting a missile, then all defending, all lying people are visible.</ta>
            <ta e="T777" id="Seg_13841" s="T772">I did not notice how it stroke my hand.</ta>
            <ta e="T784" id="Seg_13842" s="T777">And so it stroke me, it flew through it.</ta>
            <ta e="T794" id="Seg_13843" s="T784">A mine came and hit us all, more than ten people.</ta>
            <ta e="T800" id="Seg_13844" s="T794">I was holding my rifle like that and then came to myself.</ta>
            <ta e="T804" id="Seg_13845" s="T800">And so I am standing on my knees.</ta>
            <ta e="T808" id="Seg_13846" s="T804">And then I fell like that.</ta>
            <ta e="T821" id="Seg_13847" s="T808">And later I saw my fingers trembling, the blood flowing.</ta>
            <ta e="T826" id="Seg_13848" s="T821">And I took my belt off.</ta>
            <ta e="T841" id="Seg_13849" s="T826">I took my belt off, I whatchamacallit, and apparently to my whatchamacallit, I whatchamacallit, I tied it to my neck.</ta>
            <ta e="T851" id="Seg_13850" s="T841">Then there are those parcels, tied up, and with that I bandaged it.</ta>
            <ta e="T859" id="Seg_13851" s="T851">Then there is puttee in the high boots.</ta>
            <ta e="T862" id="Seg_13852" s="T859">I bandaged it with my puttees.</ta>
            <ta e="T867" id="Seg_13853" s="T862">"Go", they say, "to the commander."</ta>
            <ta e="T872" id="Seg_13854" s="T867">Crawling I came to him.</ta>
            <ta e="T878" id="Seg_13855" s="T872">I asked for bread, eh, for food.</ta>
            <ta e="T885" id="Seg_13856" s="T878">"Well", he says, "they wounded you", he says.</ta>
            <ta e="T896" id="Seg_13857" s="T885">"Hmm", I say, he took my rifle then and my whatchamacallit, I give it to my commander.</ta>
            <ta e="T900" id="Seg_13858" s="T896">There one was in numbers.</ta>
            <ta e="T911" id="Seg_13859" s="T900">Then some patrons in my pocket, there is nothing: they took away also my grenades.</ta>
            <ta e="T924" id="Seg_13860" s="T911">When I lie down into the trench, sand is rilling from the side, when mines are falling, the earth is trembling.</ta>
            <ta e="T931" id="Seg_13861" s="T924">They just make shirk-shirk-shirk.</ta>
            <ta e="T938" id="Seg_13862" s="T931">And the relief, the completion came.</ta>
            <ta e="T944" id="Seg_13863" s="T938">In place of us the next ones are coming, like that it is.</ta>
            <ta e="T951" id="Seg_13864" s="T944">It is loud, they are crying and so.</ta>
            <ta e="T954" id="Seg_13865" s="T951">Some are moaning.</ta>
            <ta e="T966" id="Seg_13866" s="T954">When they moan, I also moan, they I calm down and listen.</ta>
            <ta e="T968" id="Seg_13867" s="T966">I came to the infirmary.</ta>
            <ta e="T975" id="Seg_13868" s="T968">There is work in the field, an infirmary.</ta>
            <ta e="T983" id="Seg_13869" s="T975">I came to the infirmary and I was sent to the home front.</ta>
            <ta e="T987" id="Seg_13870" s="T983">I was lying in Ivanovo Shuya, in an old hospital.</ta>
            <ta e="T999" id="Seg_13871" s="T987">Then, eh, immediately five months I was lying there, then they sent me home.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-ChVD">
            <ta e="T5" id="Seg_13872" s="T0">Nach Dudinka kam ich 1942.</ta>
            <ta e="T14" id="Seg_13873" s="T5">Im August ging ich durch eine Kommission [und wurde] einem Hilfssoldaten [zugeteilt].</ta>
            <ta e="T16" id="Seg_13874" s="T14">Man schickte mich nach Norilsk.</ta>
            <ta e="T27" id="Seg_13875" s="T16">Von Norilsk da arbeitete ich in einer militärischen Schutzeinheit, ich kam wieder mit Freiwilligen zusammen.</ta>
            <ta e="T33" id="Seg_13876" s="T27">Nach Krasnojarsk fuhr ich mit dem Dampfer "Spartak".</ta>
            <ta e="T40" id="Seg_13877" s="T33">Dann lernte ich in der Krasnojarsker Infanterie mit Panzerschützen.</ta>
            <ta e="T55" id="Seg_13878" s="T40">Nachdem ich elf Monate oder wie viel gelernt hatte, wiederholte (…) in Dings, in Zvenigorod, Unterfeldwebel.</ta>
            <ta e="T59" id="Seg_13879" s="T55">Ich habe Fallschirme auseinander genommen, (solche) für die Landung.</ta>
            <ta e="T70" id="Seg_13880" s="T59">Nun, dort habe ich nicht lange gelernt, mit meinen Freunden, um nicht zurückzubleiben, ging ich [mit ihnen] zusammen.</ta>
            <ta e="T80" id="Seg_13881" s="T70">Nun, ich habe gedingst, in Dings, in Murom haben wir gelernt und gedingst, in Reserve…</ta>
            <ta e="T90" id="Seg_13882" s="T80">Einfach wurde da gedingst, dort wurde formiert, irgendwelche (Einheiten?).</ta>
            <ta e="T100" id="Seg_13883" s="T90">Dort wurde gemacht [=gefragt], wer worauf lernt, wer was macht: welche Teile, Abteilungen.</ta>
            <ta e="T109" id="Seg_13884" s="T100">Nun, da habe ich gedingst, von Murom sind wir nach Dings geraten, nach Tula.</ta>
            <ta e="T115" id="Seg_13885" s="T109">Tula… da war zerstört. [?]</ta>
            <ta e="T127" id="Seg_13886" s="T115">Nachdem wir acht Kilometer oder wie viel gefahren waren, war die Eisenbahn ganz zerstört.</ta>
            <ta e="T142" id="Seg_13887" s="T127">Nun, als wir da gedingst haben, waren schon immer Luftangriffe, sind sie jeden Morgen geflogen.</ta>
            <ta e="T152" id="Seg_13888" s="T142">Und dann war es nicht lange, äh, wir (gingen?) dorthin.</ta>
            <ta e="T166" id="Seg_13889" s="T152">Tatsächlich gingen wir zu Fuß dahin, wo die Orte waren, an denen dieser Krieg war.</ta>
            <ta e="T174" id="Seg_13890" s="T166">Und dort wurde dann gedingst, diese Partisanenabteilung.</ta>
            <ta e="T179" id="Seg_13891" s="T174">Oh, da, man kennzeichnet als "NOP" und "SOP".</ta>
            <ta e="T190" id="Seg_13892" s="T179">Und da, dahin, ein Fluss, irgendein Flüsschen fließt da.</ta>
            <ta e="T193" id="Seg_13893" s="T190">Dort im Dorf.</ta>
            <ta e="T197" id="Seg_13894" s="T193">Dort verteidigen sie sich offenbar.</ta>
            <ta e="T204" id="Seg_13895" s="T197">Am Morgen haben wir gedingst, nachdem wir gegessen hatten, das.</ta>
            <ta e="T208" id="Seg_13896" s="T204">"Da wird geschossen", erzählt man unserem Befehlshaber.</ta>
            <ta e="T214" id="Seg_13897" s="T208">"Wie, damit ihr so dingst dann…</ta>
            <ta e="T229" id="Seg_13898" s="T214">Wenn ihr eingekesselt seid, wenn ihr in Gefangenschaft geratet, die Dokumente damit man nicht dingst, muss man, damit man sich selbst nicht ausliefert.</ta>
            <ta e="T241" id="Seg_13899" s="T229">Zerreißt die Dokumente nicht einfach", sagt er wohl, "dingst dann so.</ta>
            <ta e="T251" id="Seg_13900" s="T241">Vergrabt sie oder esst sie selber", sagt er so.</ta>
            <ta e="T262" id="Seg_13901" s="T251">"Du hast es zerrissen und dennoch kleben sie, dingsen es so, sie finden es."</ta>
            <ta e="T268" id="Seg_13902" s="T262">Einmal ließ ich die Kompanie und lief.</ta>
            <ta e="T278" id="Seg_13903" s="T268">Irgendwelche erkannte ich in der Dunkelheit: Sind es Deutsche, wer sind sie?</ta>
            <ta e="T288" id="Seg_13904" s="T278">Ich ging und sah, dass dort fünf Menschen gehen.</ta>
            <ta e="T296" id="Seg_13905" s="T288">Und von dort kommen die Deutschen, sie steigen aus dem Schützengraben.</ta>
            <ta e="T300" id="Seg_13906" s="T296">"Fliehen wir", sagen sie.</ta>
            <ta e="T305" id="Seg_13907" s="T300">Dieser Mensch, diese Freunde haben Befehlshaber. [?]</ta>
            <ta e="T309" id="Seg_13908" s="T305">Andere, aus einer anderen Einheit.</ta>
            <ta e="T321" id="Seg_13909" s="T309">Ich selber ließ meine Freunde und lief, offenbar, oder so.</ta>
            <ta e="T323" id="Seg_13910" s="T321">"Weicht zurück!", sagt er.</ta>
            <ta e="T328" id="Seg_13911" s="T323">"Nur unbemerkt", sagt ihr Befehlshaber.</ta>
            <ta e="T335" id="Seg_13912" s="T328">Dann sah ich, die Deutschen kommen da.</ta>
            <ta e="T345" id="Seg_13913" s="T335">Etwas sagen sie, sie unterhalten sich, ich laufe zurück dorthin, wo meine Freunde waren.</ta>
            <ta e="T357" id="Seg_13914" s="T345">Sie tauschen sich dort aus, im Gras, sie schießen, tatsächlich wollen sie offenbar dingsen…</ta>
            <ta e="T362" id="Seg_13915" s="T357">Gefangen nehmen, am Ende kam ich zum Schützengraben.</ta>
            <ta e="T368" id="Seg_13916" s="T362">Ich kam, zu meinen Freunden, äh, zu meiner Kompanie gelangte ich.</ta>
            <ta e="T377" id="Seg_13917" s="T368">Und da mache ich so, wie ein Stein (…), mit einem Maschinengewehr dingst man sofort.</ta>
            <ta e="T381" id="Seg_13918" s="T377">Mehr als zwanzig Leute streckten sie nieder.</ta>
            <ta e="T387" id="Seg_13919" s="T381">Einige laufen zurück, es wird patrouilliert.</ta>
            <ta e="T401" id="Seg_13920" s="T387">Die Kompanie geht los, wer geht jetzt noch vor, man schützt nur die Kompanie als Aufklärer. </ta>
            <ta e="T412" id="Seg_13921" s="T401">Vorne sind zwei Leute, links und rechts auch je zwei Leute.</ta>
            <ta e="T416" id="Seg_13922" s="T412">"Nun, was soll mit uns werden?", sagt er. </ta>
            <ta e="T424" id="Seg_13923" s="T416">In diesem Dorf überprüfen wir, ob tatsächlich niemand da ist.</ta>
            <ta e="T430" id="Seg_13924" s="T424">Die Deutschen sind nur am anderen Ufer nur zu sehen.</ta>
            <ta e="T434" id="Seg_13925" s="T430">"Gehen wir zurück", sagt er.</ta>
            <ta e="T446" id="Seg_13926" s="T434">Nun, da sind wir gegangen und haben uns getroffen, über den Fluss sind wir gegangen, gegen die Strömung.</ta>
            <ta e="T451" id="Seg_13927" s="T446">Wir überqueren den Fluss, äh, und dingsen da.</ta>
            <ta e="T467" id="Seg_13928" s="T451">So wurde dann gedingst, da gibt es Artillerie: Kanonen, äh, solche, Pferde, alles wird so gedingst.</ta>
            <ta e="T471" id="Seg_13929" s="T467">Sie drehten sich um und dann töteten sie.</ta>
            <ta e="T475" id="Seg_13930" s="T471">Dort fanden wir einen Menschen.</ta>
            <ta e="T482" id="Seg_13931" s="T475">Er ist tatsächlich halb am Leben, irgendwo, er spricht kaum.</ta>
            <ta e="T494" id="Seg_13932" s="T482">"Es war gerade Zeit gewesen zu rauchen", sagt er, "unsere Kompanie haben sie so gedingt", sagt er.</ta>
            <ta e="T499" id="Seg_13933" s="T494">"Die Deutschen, ja, sind gerade weg", sagt er.</ta>
            <ta e="T503" id="Seg_13934" s="T499">"Redet nicht laut", sagt er.</ta>
            <ta e="T507" id="Seg_13935" s="T503">Nun, und so dingsten wir…</ta>
            <ta e="T519" id="Seg_13936" s="T507">Und da also, äh, Freund… zwei Leute werden geschickt, da gibt es Dings, ein Lazarett.</ta>
            <ta e="T525" id="Seg_13937" s="T519">Dann "wir nehmen dich mit", sagen sie, "ich, wir dich."</ta>
            <ta e="T544" id="Seg_13938" s="T525">"Sorgt euch nicht um mich", sagt er, "ich äh überlebe, äh, überlebe doch nicht", sagt er, "ich sterbe jetzt."</ta>
            <ta e="T550" id="Seg_13939" s="T544">Die Menschen liegen tatsächlich wie eine Unterlage, (von denen?).</ta>
            <ta e="T553" id="Seg_13940" s="T550">So haben sie gedingst.</ta>
            <ta e="T573" id="Seg_13941" s="T553">Diesen brachten die zwei Leute dahin, jener sagte noch "bring mich nicht und so", dann kamen diese zurück.</ta>
            <ta e="T585" id="Seg_13942" s="T573">"Ich überlebe sowieso nicht, warum dingst ihr mit mir", sagt er.</ta>
            <ta e="T589" id="Seg_13943" s="T585">Am Morgen wird es gerade hell.</ta>
            <ta e="T599" id="Seg_13944" s="T589">Als es so wurde, trafen wir irgendeinen Leutnant.</ta>
            <ta e="T601" id="Seg_13945" s="T599">Jener sagt:</ta>
            <ta e="T603" id="Seg_13946" s="T601">"Aus welcher Abteilung bist du?"</ta>
            <ta e="T606" id="Seg_13947" s="T603">"Sie, aus Ihrer", sagen wir.</ta>
            <ta e="T610" id="Seg_13948" s="T606">Sagt dann einer von uns.</ta>
            <ta e="T615" id="Seg_13949" s="T610">"Dann lauft mir mit", sagt er.</ta>
            <ta e="T622" id="Seg_13950" s="T615">Wir liefen weiter, um nicht zu dingsen [?]</ta>
            <ta e="T626" id="Seg_13951" s="T622">Um den Anschluss zu halten dingst er offenbar. [?]</ta>
            <ta e="T631" id="Seg_13952" s="T626">"Und dann versuchen wir zu dingsen."</ta>
            <ta e="T638" id="Seg_13953" s="T631">Einige Tage sind wir so gelaufen.</ta>
            <ta e="T641" id="Seg_13954" s="T638">Unsere Kompanie finden wir nicht.</ta>
            <ta e="T649" id="Seg_13955" s="T641">Und dann später, haben wir, äh, fanden wir sie wieder.</ta>
            <ta e="T660" id="Seg_13956" s="T649">Am dritten Tag oder am wievielten gehen wir weg, nach Dings.</ta>
            <ta e="T676" id="Seg_13957" s="T660">Wir überqueren den Fluss, dann (treffen?) wir uns, aus der Ferne wird geschossen, wir schießen auch von dieser Seite.</ta>
            <ta e="T686" id="Seg_13958" s="T676">Und wir dingsten so da, nirgendwohin dingste man, ließ man uns.</ta>
            <ta e="T697" id="Seg_13959" s="T686">Am Morgen, denen die können (?) von vor uns, man gibt den Befehl: </ta>
            <ta e="T704" id="Seg_13960" s="T697">"Nun, um drei Uhr sollt ihr Brjansk einnehmen."</ta>
            <ta e="T712" id="Seg_13961" s="T704">Nun und da war irgendein Berg, (ein schmaler Streifen?).</ta>
            <ta e="T719" id="Seg_13962" s="T712">Den schmalen Streifen, äh, haben wir auch gedingst.</ta>
            <ta e="T735" id="Seg_13963" s="T719">Wieder (sein Gesicht?)… von vorne dingsen sie so, die Deutschen liegen da: sie verteidigen sich.</ta>
            <ta e="T741" id="Seg_13964" s="T735">Und wir schießen auch von dieser Seite.</ta>
            <ta e="T749" id="Seg_13965" s="T741">Und dann liegen wir die ganze Nacht da.</ta>
            <ta e="T762" id="Seg_13966" s="T749">Und in der Nacht gegen zehn, gegen zwei oder gegen eins schießen sie eine Rakete.</ta>
            <ta e="T772" id="Seg_13967" s="T762">Wenn man eine Rakete schießt, dann sind alle sich Verteidigende, alle Liegende sichtbar.</ta>
            <ta e="T777" id="Seg_13968" s="T772">Wie sie meine Hand traf, bemerke ich nicht.</ta>
            <ta e="T784" id="Seg_13969" s="T777">Und so traf sie mich da, sie flog hindurch.</ta>
            <ta e="T794" id="Seg_13970" s="T784">Eine Mine flog und traf uns alle, mehr als zehn Menschen.</ta>
            <ta e="T800" id="Seg_13971" s="T794">Ich hielt mein Gewehr so und kam dann wieder zu mir.</ta>
            <ta e="T804" id="Seg_13972" s="T800">Und so stehe ich, auf den Knien.</ta>
            <ta e="T808" id="Seg_13973" s="T804">Und dann fiel ich so.</ta>
            <ta e="T821" id="Seg_13974" s="T808">Und dann später sah ich, dass meine Finger zittern, dass das Blut so fließt.</ta>
            <ta e="T826" id="Seg_13975" s="T821">Und dann zog ich meinen Gürtel aus.</ta>
            <ta e="T841" id="Seg_13976" s="T826">Ich zog meinen Gürtel aus, so dingste ich, und offenbar an mein Dings, an meinen Hals machte ich, band ich ihn.</ta>
            <ta e="T851" id="Seg_13977" s="T841">Dann gibt es diese Pakete, eingewickelt, und damit verband ich es.</ta>
            <ta e="T859" id="Seg_13978" s="T851">Dann gibt es Gamaschen an den hohen Stiefeln.</ta>
            <ta e="T862" id="Seg_13979" s="T859">Mit meinen Gamaschen verband ich es.</ta>
            <ta e="T867" id="Seg_13980" s="T862">"Geh", sagen sie, "zum Kompanieführer."</ta>
            <ta e="T872" id="Seg_13981" s="T867">Kriechend gelangte ich zu jenem.</ta>
            <ta e="T878" id="Seg_13982" s="T872">Ich bat um Brot, äh, um Essen.</ta>
            <ta e="T885" id="Seg_13983" s="T878">"Na", sagt er, "sie haben dich verwundet", sagt er.</ta>
            <ta e="T896" id="Seg_13984" s="T885">"Hmm", sage ich, er nahm dann mein Gewehr und mein Dings, ich gebe es meinem Befehlshaber.</ta>
            <ta e="T900" id="Seg_13985" s="T896">Dort war man nach Nummern.</ta>
            <ta e="T911" id="Seg_13986" s="T900">Dann einige Patronen in meiner Tasche, da ist nichts: auch meine Granaten haben sie weggenommen.</ta>
            <ta e="T924" id="Seg_13987" s="T911">Als ich mich in den Schützengraben lege, rieselt Sand von der Seite hinein, als Minen fallen, zittert die Erde.</ta>
            <ta e="T931" id="Seg_13988" s="T924">Schirk-schirk-schirk machen sie nur.</ta>
            <ta e="T938" id="Seg_13989" s="T931">Und die Ablösung, die Ergänzung kam.</ta>
            <ta e="T944" id="Seg_13990" s="T938">Anstelle von uns kommen die nächsten, so geht es.</ta>
            <ta e="T951" id="Seg_13991" s="T944">Es ist laut, es wird geweint und so.</ta>
            <ta e="T954" id="Seg_13992" s="T951">Einige stöhnen.</ta>
            <ta e="T966" id="Seg_13993" s="T954">Wenn sie stöhnen, dann stöhne ich auch, dann werde ich wieder ruhiger und lausche.</ta>
            <ta e="T968" id="Seg_13994" s="T966">Ich kam ins Lazarett.</ta>
            <ta e="T975" id="Seg_13995" s="T968">Arbeit gibt es im Feld, ein Lazarett.</ta>
            <ta e="T983" id="Seg_13996" s="T975">‎‎‎‎Ich kam in das Lazarett und man schickte mich an die Heimatfront.</ta>
            <ta e="T987" id="Seg_13997" s="T983">Ich lag in Ivanovo Shuja, in einem alten Krankenhaus.</ta>
            <ta e="T999" id="Seg_13998" s="T987">Dann, äh, sofort fünf Monate lag ich da, dann ließen sie mich nach Hause.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-ChVD">
            <ta e="T5" id="Seg_13999" s="T0">В Дудинку я приехал в сорок втором.</ta>
            <ta e="T14" id="Seg_14000" s="T5">В августе вообще-то комиссию прошёл – [отправили] в нестроевую.</ta>
            <ta e="T16" id="Seg_14001" s="T14">В Норильск направили.</ta>
            <ta e="T27" id="Seg_14002" s="T16">Из Норильска в этой, в военизированной охране поработал немного, к добровольцам снова попал.</ta>
            <ta e="T33" id="Seg_14003" s="T27">До Красноярска ехал на пароходе "Спартак".</ta>
            <ta e="T40" id="Seg_14004" s="T33">Потом учился в красноярской пехоте с танковыми пулемётчиками.</ta>
            <ta e="T55" id="Seg_14005" s="T40">Проучившись одиннадцать месяцев или сколько, вновь (…) в этом, в Звенигороде младшим сержантом.</ta>
            <ta e="T59" id="Seg_14006" s="T55">Парашют разбирал десантный.</ta>
            <ta e="T70" id="Seg_14007" s="T59">А там долго не учился, чтобы от друзей не отстать, вместе [с ними] ушёл.</ta>
            <ta e="T80" id="Seg_14008" s="T70">И вот, я там это самое, в этом, в Муроме мы учились и это самое, в запасном…</ta>
            <ta e="T90" id="Seg_14009" s="T80">Просто делали там это, формирование, делали какие-то (части?).</ta>
            <ta e="T100" id="Seg_14010" s="T90">Кем, на кого учился это делали [=узнавали]: какие части, подразделения.</ta>
            <ta e="T109" id="Seg_14011" s="T100">Ну, вот там побывал, да, и из Мурома в эту попали, в Тулу.</ta>
            <ta e="T115" id="Seg_14012" s="T109">В той Туле… и разбился вот. [?]</ta>
            <ta e="T127" id="Seg_14013" s="T115">Километров восемь или сколько проехали, железные дороги были все разрушены.</ta>
            <ta e="T142" id="Seg_14014" s="T127">И вот, когда там были, уже налёты были постоянно, каждое утро летали.</ta>
            <ta e="T152" id="Seg_14015" s="T142">И вот, недолго там побыв, (все?) мы туда (отправились?).</ta>
            <ta e="T166" id="Seg_14016" s="T152">Правда, пешком шли туда, пока не дошли до места, где та война шла.</ta>
            <ta e="T174" id="Seg_14017" s="T166">Вот там создавали вот это — партизанский отряд.</ta>
            <ta e="T179" id="Seg_14018" s="T174">О, это называли НОП, СОП.</ta>
            <ta e="T190" id="Seg_14019" s="T179">И там по этому (большая?) река, какая-то речушка протекает.</ta>
            <ta e="T193" id="Seg_14020" s="T190">Там в деревне.</ta>
            <ta e="T197" id="Seg_14021" s="T193">Они там оборону держали, наверное.</ta>
            <ta e="T204" id="Seg_14022" s="T197">На утро, это самое, немного поели.</ta>
            <ta e="T208" id="Seg_14023" s="T204">"Вот перестреливаются! — говорят нашему командиру. </ta>
            <ta e="T214" id="Seg_14024" s="T208">— Как чтобы потом сделали…</ta>
            <ta e="T229" id="Seg_14025" s="T214">Если в окружение попадёшь, в плен если попадёшь, всякие документы это… чтобы не делал это, надо будет, чтобы себя не выдал, может…</ta>
            <ta e="T241" id="Seg_14026" s="T229">"Просто так не рвите документы, — говорит, — делайте по-другому.</ta>
            <ta e="T251" id="Seg_14027" s="T241">Или закопайте, или сами съешьте," — так говорит.</ta>
            <ta e="T262" id="Seg_14028" s="T251">Если порвёшь, всё равно склеивают или соединяют и находят".</ta>
            <ta e="T268" id="Seg_14029" s="T262">И вот однажды я роту оставил и побежал.</ta>
            <ta e="T278" id="Seg_14030" s="T268">Которые там ходят, не разобрался в темноте: немцы ли, кто ли?</ta>
            <ta e="T288" id="Seg_14031" s="T278">И вот, прохаживаясь, увидел, пять человек прям вот так идут. </ta>
            <ta e="T296" id="Seg_14032" s="T288">И вот оттуда немцы эти самые идут, встают из траншеи встают.</ta>
            <ta e="T300" id="Seg_14033" s="T296">"Обратно побежим," — говорят.</ta>
            <ta e="T305" id="Seg_14034" s="T300">Тот человек, командиров имеют те их друзья. [?]</ta>
            <ta e="T309" id="Seg_14035" s="T305">Другие, с другой части.</ta>
            <ta e="T321" id="Seg_14036" s="T309">Я сам (…) друзей оставил вот и побежал, наверно, или как.</ta>
            <ta e="T323" id="Seg_14037" s="T321">"Отступайте! — говорит.</ta>
            <ta e="T328" id="Seg_14038" s="T323">— Только не заметно," — говорит их командир.</ta>
            <ta e="T335" id="Seg_14039" s="T328">Потом смотрю, немцы вот сюда уже приближаются.</ta>
            <ta e="T345" id="Seg_14040" s="T335">Что-то проговаривают, шепчутся, я обратно бегу туда, где друзья были.</ta>
            <ta e="T357" id="Seg_14041" s="T345">Вовсю друг с другом общаются только там, в кустах, стреляют, точно что-то хотят сделать…</ta>
            <ta e="T362" id="Seg_14042" s="T357">Я совсем близко к траншее подошёл, чтобы их в плен взять.</ta>
            <ta e="T368" id="Seg_14043" s="T362">Подошёл, к своим… в свою роту попал.</ta>
            <ta e="T377" id="Seg_14044" s="T368">И вот делаю это, ой, камнем (падает?) — из пулемёта это делает сразу.</ta>
            <ta e="T381" id="Seg_14045" s="T377">Больше двадцати человек повалили.</ta>
            <ta e="T387" id="Seg_14046" s="T381">Некоторые обратно побежали вот, дозор бывает.</ta>
            <ta e="T401" id="Seg_14047" s="T387">Рота сейчас уйдёт, кто ещё впереди ходить будет, [ты] роту только охраняешь как разведчик. </ta>
            <ta e="T412" id="Seg_14048" s="T401">Впереди два человека бывает, справа и слева тоже по два человека.</ta>
            <ta e="T416" id="Seg_14049" s="T412">"Ну как нам быть?" — говорит.</ta>
            <ta e="T424" id="Seg_14050" s="T416">В этой деревне проверяем, и вправду, никого нет.</ta>
            <ta e="T430" id="Seg_14051" s="T424">Немцы только на том берегу виднеются.</ta>
            <ta e="T434" id="Seg_14052" s="T430">"Обратно вернёмся," — говорит. </ta>
            <ta e="T446" id="Seg_14053" s="T434">Вот, там пока проходили, (встретили?) и через речку пошли, наперерез течению.</ta>
            <ta e="T451" id="Seg_14054" s="T446">Реку переходя, э-э, это самое делаем.</ta>
            <ta e="T467" id="Seg_14055" s="T451">Вот так делают, артиллерия бывает: пушки, эти самые, лошади, — всё вот так совершенно.</ta>
            <ta e="T471" id="Seg_14056" s="T467">Попереворачивали, потом поубивали.</ta>
            <ta e="T475" id="Seg_14057" s="T471">Одного человека мы там нашли.</ta>
            <ta e="T482" id="Seg_14058" s="T475">Полуживой на самом деле, еле говорит. </ta>
            <ta e="T494" id="Seg_14059" s="T482">"Вот только пришло время покурить, — говорит, — нашу роту это самое сделали [=расстреляли], — говорит.</ta>
            <ta e="T499" id="Seg_14060" s="T494">— Эти немцы, да, ушли только что, — говорит.</ta>
            <ta e="T503" id="Seg_14061" s="T499">— Не разговаривайте громко", — предупредил.</ta>
            <ta e="T507" id="Seg_14062" s="T503">И вот мы это сделали…</ta>
            <ta e="T519" id="Seg_14063" s="T507">И вот туда (…) двоих людей отправляют, там это бывает, санчасть.</ta>
            <ta e="T525" id="Seg_14064" s="T519">Потом говорят: "Мы тебя донесём".</ta>
            <ta e="T544" id="Seg_14065" s="T525">"Вы обо мне не заботьтесь, — говорит, — я всё равно уже не человек, — говорит, — умру сейчас".</ta>
            <ta e="T550" id="Seg_14066" s="T544">Люди вправду как подстилка лежат, (от них?).</ta>
            <ta e="T553" id="Seg_14067" s="T550">Так и вот сделали. </ta>
            <ta e="T573" id="Seg_14068" s="T553">Его понесли два человека в эту [=в санчасть?] , а тот сказал им, что всё равно не донесете, они потом обратно вернулись.</ta>
            <ta e="T585" id="Seg_14069" s="T573">"Всё равно человеком не стану, зачем со мной возитесь," — так вроде сказал.</ta>
            <ta e="T589" id="Seg_14070" s="T585">Утром заря только занимается.</ta>
            <ta e="T599" id="Seg_14071" s="T589">Когда (…) стало, мы какого-то лейтенанта встретили.</ta>
            <ta e="T601" id="Seg_14072" s="T599">Он спрашивает:</ta>
            <ta e="T603" id="Seg_14073" s="T601">"С какой части?"</ta>
            <ta e="T606" id="Seg_14074" s="T603">"С вашей, вашей," — отвечаем.</ta>
            <ta e="T610" id="Seg_14075" s="T606">Говорит один из нас.</ta>
            <ta e="T615" id="Seg_14076" s="T610">"Тогда со мной бегите," — говорит.</ta>
            <ta e="T622" id="Seg_14077" s="T615">Вперед пошли вот это не делать. [?]</ta>
            <ta e="T626" id="Seg_14078" s="T622">Связь чтобы взять, делает, наверное. [?]</ta>
            <ta e="T631" id="Seg_14079" s="T626">"Вот потом это делать стараемся."</ta>
            <ta e="T638" id="Seg_14080" s="T631">Несколько дней вот так ходили.</ta>
            <ta e="T641" id="Seg_14081" s="T638">Роту свою найти не можем.</ta>
            <ta e="T649" id="Seg_14082" s="T641">Ну, потом опять нашли.</ta>
            <ta e="T660" id="Seg_14083" s="T649">На третий день или на какой день отходим дальше, вот туда.</ta>
            <ta e="T676" id="Seg_14084" s="T660">Вот реку переходим и потом тут (?оказываемся), издалека стреляют, а мы с этой стороны тоже стреляем.</ta>
            <ta e="T686" id="Seg_14085" s="T676">И вот это самое мы там сделали, никуда не это, не пускают.</ta>
            <ta e="T697" id="Seg_14086" s="T686">Утром тем, кто может (?), оттуда говорят, такую команду дают: </ta>
            <ta e="T704" id="Seg_14087" s="T697">"В три часа вы должны занять Брянск".</ta>
            <ta e="T712" id="Seg_14088" s="T704">И вот какая то гора образовалась, (склон?).</ta>
            <ta e="T719" id="Seg_14089" s="T712">(Cклон?) одолели и вот опять.</ta>
            <ta e="T735" id="Seg_14090" s="T719">Опять (лицо его?)… спереди это делают вот… немцы вот так лежат, обороняются.</ta>
            <ta e="T741" id="Seg_14091" s="T735">А мы отсюда стреляем тоже.</ta>
            <ta e="T749" id="Seg_14092" s="T741">И вот всю ночь напролёт лежим тут.</ta>
            <ta e="T762" id="Seg_14093" s="T749">И ночью около десяти или к двум часам, или около часа что ли ракету пустили.</ta>
            <ta e="T772" id="Seg_14094" s="T762">Ракета если запускается, тут все обороняющиеся, кто на земле лежит, всех видно.</ta>
            <ta e="T777" id="Seg_14095" s="T772">В руку как попали, я и не заметил.</ta>
            <ta e="T784" id="Seg_14096" s="T777">И вот сюда мне попала, насквозь пролетела.</ta>
            <ta e="T794" id="Seg_14097" s="T784">Одна мина прилетела и накрыла — большe десяти человек.</ta>
            <ta e="T800" id="Seg_14098" s="T794">Я очнулся, ружьё вот так держа.</ta>
            <ta e="T804" id="Seg_14099" s="T800">Так стою на четвереньках.</ta>
            <ta e="T808" id="Seg_14100" s="T804">И вот так упал.</ta>
            <ta e="T821" id="Seg_14101" s="T808">И потом заметил, что пальцы все висят, болтаются, а кровь вот так течёт.</ta>
            <ta e="T826" id="Seg_14102" s="T821">И потом снял вот так ремень.</ta>
            <ta e="T841" id="Seg_14103" s="T826">Ремень снял и так сделал сюда, на шею, так привязал и закинул.</ta>
            <ta e="T851" id="Seg_14104" s="T841">Потом пакеты бывают у каждого перевязочные, вот им обмотал вот так.</ta>
            <ta e="T859" id="Seg_14105" s="T851">Потом обмотка в ботинках бывает.</ta>
            <ta e="T862" id="Seg_14106" s="T859">Этой обмоткой замотал.</ta>
            <ta e="T867" id="Seg_14107" s="T862">"Иди, — говорят, — к командиру роты."</ta>
            <ta e="T872" id="Seg_14108" s="T867">И вот дополз до него.</ta>
            <ta e="T878" id="Seg_14109" s="T872">Хлеба просил, еды.</ta>
            <ta e="T885" id="Seg_14110" s="T878">"Ой, — говорит, — ранили (…)" — говорит.</ta>
            <ta e="T896" id="Seg_14111" s="T885">"Ага," — отвечаю, ружьё моё вот забрал мой этот, я ведь сдаю командиру своему.</ta>
            <ta e="T900" id="Seg_14112" s="T896">Там, по номерам бывают.</ta>
            <ta e="T911" id="Seg_14113" s="T900">Потом патроны одни в кармане, ничего нет: гранаты мои тоже забрали.</ta>
            <ta e="T924" id="Seg_14114" s="T911">В окоп когда ложился, песком как засыпет, оттуда летит, когда мины падают, земля дрожит.</ta>
            <ta e="T931" id="Seg_14115" s="T924">Только шырк-шырк-шырк делают [=свистят].</ta>
            <ta e="T938" id="Seg_14116" s="T931">И потом смена, пополнение прибывает, оказывается.</ta>
            <ta e="T944" id="Seg_14117" s="T938">Вместо нас следующие прибывают, так происходит.</ta>
            <ta e="T951" id="Seg_14118" s="T944">Шум стоит, все плачут и всё такое вдруг.</ta>
            <ta e="T954" id="Seg_14119" s="T951">Некоторые стонут.</ta>
            <ta e="T966" id="Seg_14120" s="T954">Если стонут, я тоже начинаю стонать, потом успокаиваюсь, прислушиваюсь.</ta>
            <ta e="T968" id="Seg_14121" s="T966">В санчасть пришёл.</ta>
            <ta e="T975" id="Seg_14122" s="T968">Полевая работа бывает, санчасть.</ta>
            <ta e="T983" id="Seg_14123" s="T975">Там в санчасть как прибыл, меня в тыл направили.</ta>
            <ta e="T987" id="Seg_14124" s="T983">В Иваново-Шуе лежал, в старом госпитале.</ta>
            <ta e="T999" id="Seg_14125" s="T987">Потом пять месяцев подряд как отлежал, домой отпустили.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-ChVD">
            <ta e="T5" id="Seg_14126" s="T0">В Дудинку приехал в сорок втором.</ta>
            <ta e="T14" id="Seg_14127" s="T5">В августе вообще-то комиссию прошёл – в нестроевой.</ta>
            <ta e="T16" id="Seg_14128" s="T14">В Норильск направили.</ta>
            <ta e="T27" id="Seg_14129" s="T16">Из Норильска в этом в военизированной охране поработал немного, с добровольцами опять снова попал.</ta>
            <ta e="T33" id="Seg_14130" s="T27">Красноярска э-э до ехал на "Спартак" пароходе.</ta>
            <ta e="T40" id="Seg_14131" s="T33">Потом выучился в краснорской пехоте с танковыми пулемётчиками.</ta>
            <ta e="T55" id="Seg_14132" s="T40">Одиннадцать месяцев или же сколько, проучившись, вновь ( ? ) в этом, в Звенигород городе младшим сержантом.</ta>
            <ta e="T59" id="Seg_14133" s="T55">Парашют разбирал десантский.</ta>
            <ta e="T70" id="Seg_14134" s="T59">А там долго не учился, чтобы от друзей не отстать вместе с ними ушёл.</ta>
            <ta e="T80" id="Seg_14135" s="T70">И вот, это самое сделал, там, в Муроме (учились) (проделали) в запасном…</ta>
            <ta e="T90" id="Seg_14136" s="T80">Просто делал это там, формирование, делали какие части.</ta>
            <ta e="T100" id="Seg_14137" s="T90">Кем, на кого учился узнавали: какие части, подразделения.</ta>
            <ta e="T109" id="Seg_14138" s="T100">Ну, вот там побывал, да, и из Мурома в эту попали, в Тулу.</ta>
            <ta e="T115" id="Seg_14139" s="T109">В той Туле и разбился вот.</ta>
            <ta e="T127" id="Seg_14140" s="T115">Восемь километров или сколько же проехав, железные дороги все разрушены были.</ta>
            <ta e="T142" id="Seg_14141" s="T127">И вот, там когда были, уже налётами постоянно, летали каждое утро.</ta>
            <ta e="T152" id="Seg_14142" s="T142">И вот не долго побыв сделали там, м-м все .. делаем там.</ta>
            <ta e="T166" id="Seg_14143" s="T152">Правда, пешком шли туда, где та война идёт, земля пока не появится.</ta>
            <ta e="T174" id="Seg_14144" s="T166">Вот там создавали вот это – партизанский отряд.</ta>
            <ta e="T179" id="Seg_14145" s="T174">О это – НОП СОП обозначали.</ta>
            <ta e="T190" id="Seg_14146" s="T179">И там большая это.. этому река, какая-то речушка протекает.</ta>
            <ta e="T193" id="Seg_14147" s="T190">Да там в деревне.</ta>
            <ta e="T197" id="Seg_14148" s="T193">Там оборону они держали, наверное.</ta>
            <ta e="T204" id="Seg_14149" s="T197">На утро, этот самое, покушав немного это.</ta>
            <ta e="T208" id="Seg_14150" s="T204">"Вот перестреливаются! – командир наш рассказывает.</ta>
            <ta e="T214" id="Seg_14151" s="T208">– Как сделали чтобы потом….</ta>
            <ta e="T229" id="Seg_14152" s="T214">Если в окружение попадёшь, в плен если попадёшь, всякие документы это… чтобы не делал это, надо будут, себя не выдал чтобы, может…</ta>
            <ta e="T241" id="Seg_14153" s="T229">"Просто не рвите, э-э, документы, – говорит вот – разры.. по другому.</ta>
            <ta e="T251" id="Seg_14154" s="T241">Закопай.., э, или закопайте, или сами съешьте," – говорил вот.</ta>
            <ta e="T262" id="Seg_14155" s="T251">Если порвёшь, всё равно склеивают или соединяют вот и находят.</ta>
            <ta e="T268" id="Seg_14156" s="T262">И вот роту оставив, побежал однажды.</ta>
            <ta e="T278" id="Seg_14157" s="T268">Которые там ходят, не разобрался… в темноте: немцы ли, кто ли?</ta>
            <ta e="T288" id="Seg_14158" s="T278">И вот прохаживаясь, увидел, как пять человек прямь вот так. </ta>
            <ta e="T296" id="Seg_14159" s="T288">И вот оттуда немцы эти самые идут, с траншеи встают.</ta>
            <ta e="T300" id="Seg_14160" s="T296">"Обратную побежим," – поговаривают.</ta>
            <ta e="T305" id="Seg_14161" s="T300">Тот человек, командира имеют те их друзья.</ta>
            <ta e="T309" id="Seg_14162" s="T305">Другие, с другой части.</ta>
            <ta e="T321" id="Seg_14163" s="T309">Сам (…) друзей оставив вот побежал, наверно или как же.</ta>
            <ta e="T323" id="Seg_14164" s="T321">"Отступайте! – говорит.</ta>
            <ta e="T328" id="Seg_14165" s="T323">– Только не заметно," – говорит их командир.</ta>
            <ta e="T335" id="Seg_14166" s="T328">Потом смотрю, немцы вот сюда уже приближаются.</ta>
            <ta e="T345" id="Seg_14167" s="T335">Что, что проговаривают, шепчутся, обратно бегу, где друзья побывали.</ta>
            <ta e="T357" id="Seg_14168" s="T345">Вовсю друг с другом общаются только там, в кустаах, стреляют, точно что-то хотят сделать…</ta>
            <ta e="T362" id="Seg_14169" s="T357">В плен взять, вообще к трашее подошёл.</ta>
            <ta e="T368" id="Seg_14170" s="T362">Подошёл, к своим друз.. э, в роту свою попал.</ta>
            <ta e="T377" id="Seg_14171" s="T368">И вот делаю это, ой, камнем (падает?) – пулемётом сделает сразу.</ta>
            <ta e="T381" id="Seg_14172" s="T377">Больше двадцати человек повалили.</ta>
            <ta e="T387" id="Seg_14173" s="T381">Некоторые обратно побежали вот, дозор бывает.</ta>
            <ta e="T401" id="Seg_14174" s="T387">Рота уйдёт сейчас, ещё впереди ходить будет сейчас как разведчик роту только охраняешь вот.</ta>
            <ta e="T412" id="Seg_14175" s="T401">Впереди два человека бывает, справа и слева тоже по два человека.</ta>
            <ta e="T416" id="Seg_14176" s="T412">"Ну как нам быть?" – говорит.</ta>
            <ta e="T424" id="Seg_14177" s="T416">В этой деревне проверяем, и вправду, никого.. никого нет.</ta>
            <ta e="T430" id="Seg_14178" s="T424">Немцы, э немцев только на том берегу только виднеются.</ta>
            <ta e="T434" id="Seg_14179" s="T430">"Обратно вернёмся," – говорит. </ta>
            <ta e="T446" id="Seg_14180" s="T434">Вот, там пока проходили попали и речку напереез пошли, наперерез течению.</ta>
            <ta e="T451" id="Seg_14181" s="T446">Реку переходя, э-э, делаем, э-э, это самое делаем.</ta>
            <ta e="T467" id="Seg_14182" s="T451">Вот так бывает вот артилерия бывает вот: пушки та..э, лошади, – всё буквально вот так.</ta>
            <ta e="T471" id="Seg_14183" s="T467">Перевёрнытыми сделали потом поубивали.</ta>
            <ta e="T475" id="Seg_14184" s="T471">Там одного человека нашли.</ta>
            <ta e="T482" id="Seg_14185" s="T475">Полуживой на самом деле, где-то еле говорит. </ta>
            <ta e="T494" id="Seg_14186" s="T482">"Вот только табак покурить время настало, – говорит, – нашу роту (это самое сделали) (расстреляли), – говорит.</ta>
            <ta e="T499" id="Seg_14187" s="T494">– Эти немцы, да, ушли только что, – говорит.</ta>
            <ta e="T503" id="Seg_14188" s="T499">– Не разговаривайте громко", – предупредил.</ta>
            <ta e="T507" id="Seg_14189" s="T503">И вот это сделали…</ta>
            <ta e="T519" id="Seg_14190" s="T507">И вот туда (…) э-э (друг..) двоих людей направляют, там это бывает – санчасть.</ta>
            <ta e="T525" id="Seg_14191" s="T519">Потом "донесём, – говорят, – мы тебя."</ta>
            <ta e="T544" id="Seg_14192" s="T525">"Зачем.. вы не обращайте внимания, – говорит, – я всё равно уже не человек, – говорит, – умр.. умру сейчас.. ."</ta>
            <ta e="T550" id="Seg_14193" s="T544">Люди как (подстилка) лежат точно, от них.</ta>
            <ta e="T553" id="Seg_14194" s="T550">Так и вот сделали. </ta>
            <ta e="T573" id="Seg_14195" s="T553">Его понесли два человека к этому, а тот вот сказал им, что всё равно не донесете, потом обратно вернулись эти.</ta>
            <ta e="T585" id="Seg_14196" s="T573">"Всё равно человеком не стану, для чего со мной возитесь," – вроде, говорит.</ta>
            <ta e="T589" id="Seg_14197" s="T585">Утром заря наша только появляется.</ta>
            <ta e="T599" id="Seg_14198" s="T589">Когда рано .. э-э появялось, какого-то лейтенанта встретили.</ta>
            <ta e="T601" id="Seg_14199" s="T599">Он спрашивает:</ta>
            <ta e="T603" id="Seg_14200" s="T601">"С какой части?"</ta>
            <ta e="T606" id="Seg_14201" s="T603">"С вашей, вашей," – отвечаем.</ta>
            <ta e="T610" id="Seg_14202" s="T606">Говорит один из нас.</ta>
            <ta e="T615" id="Seg_14203" s="T610">"Тогда со мной бегите," – говорит.</ta>
            <ta e="T622" id="Seg_14204" s="T615">Вперед пошли ли что сделать вот это.</ta>
            <ta e="T626" id="Seg_14205" s="T622">Связь чтобы взять, делает, наверное.</ta>
            <ta e="T631" id="Seg_14206" s="T626">"Вот потом делать стараемся."</ta>
            <ta e="T638" id="Seg_14207" s="T631">Несколько дней ходили вот так.</ta>
            <ta e="T641" id="Seg_14208" s="T638">Роту даже найти не можем.</ta>
            <ta e="T649" id="Seg_14209" s="T641">И потом нашли опять.</ta>
            <ta e="T660" id="Seg_14210" s="T649">Тре.. на третий день или на какой ли день дальше отходим, вот туда.</ta>
            <ta e="T676" id="Seg_14211" s="T660">Вот реку переходим и потом тут оказываемся, из дали стреляют, а мы с этой стороны тоже стреляем.</ta>
            <ta e="T686" id="Seg_14212" s="T676">И вот это самое сделали мы там, никуда не пускают вот.</ta>
            <ta e="T697" id="Seg_14213" s="T686">Утром тем, то может, оттуда говорят, это команду дают:</ta>
            <ta e="T704" id="Seg_14214" s="T697">"В три часа Брянск занять должны."</ta>
            <ta e="T712" id="Seg_14215" s="T704">И вот какая то гора образовалась – склон (?)</ta>
            <ta e="T719" id="Seg_14216" s="T712">Во.. (склон ?) одолели и вот опять.</ta>
            <ta e="T735" id="Seg_14217" s="T719">Опять лицо его … спереди это делают вот все-таки.. немцы вот так лежат: обороняются лежат.</ta>
            <ta e="T741" id="Seg_14218" s="T735">А мы отсюда стреляем тоже.</ta>
            <ta e="T749" id="Seg_14219" s="T741">И вот всю ночь напролёт лежим тут.</ta>
            <ta e="T762" id="Seg_14220" s="T749">И ночью около десяти или к двум часам, или около часа что ли ракету пустили.</ta>
            <ta e="T772" id="Seg_14221" s="T762">Ракета если запускается, тут все обороняющие на земле лежащие – всех видно.</ta>
            <ta e="T777" id="Seg_14222" s="T772">В руку как попали, так и не заметил.</ta>
            <ta e="T784" id="Seg_14223" s="T777">И вот сюда мне попала, насквозь прилетая.</ta>
            <ta e="T794" id="Seg_14224" s="T784">Одна мина прилетела и накрыла – большe десяти человек.</ta>
            <ta e="T800" id="Seg_14225" s="T794">Ружьё вот так держа, очнулся.</ta>
            <ta e="T804" id="Seg_14226" s="T800">И тут я стою на четвереньках.</ta>
            <ta e="T808" id="Seg_14227" s="T804">И упал вот так.</ta>
            <ta e="T821" id="Seg_14228" s="T808">И потом заметил, что пальцы все висят, болтаются, а кровь течет вот так.</ta>
            <ta e="T826" id="Seg_14229" s="T821">И потом ремень снял вот так.</ta>
            <ta e="T841" id="Seg_14230" s="T826">Ремень снял, и так сделал сюда, на шею и так закинул, привязав.</ta>
            <ta e="T851" id="Seg_14231" s="T841">Потом пакеты бывают у каждого -- перевязочный, вот им обмотал вот так.</ta>
            <ta e="T859" id="Seg_14232" s="T851">Потом обмотка на ботинках бывает.</ta>
            <ta e="T862" id="Seg_14233" s="T859">Этой обмоткой намотал.</ta>
            <ta e="T867" id="Seg_14234" s="T862">"Иди, – говорят, – к командиру роты."</ta>
            <ta e="T872" id="Seg_14235" s="T867">И вот дополз до него.</ta>
            <ta e="T878" id="Seg_14236" s="T872">Хлеб прося ээ, еду.</ta>
            <ta e="T885" id="Seg_14237" s="T878">"Ой – говорит, – поранили да" – говорит.</ta>
            <ta e="T896" id="Seg_14238" s="T885">"Да-а" – отвечаю, ружьё моё вот забрал этот мой, командиру своему сдаю ведь.</ta>
            <ta e="T900" id="Seg_14239" s="T896">Там, по номерам бывают.</ta>
            <ta e="T911" id="Seg_14240" s="T900">Потом патроны одни в кармане, ничего нет: гранаты мои тоже забрали.</ta>
            <ta e="T924" id="Seg_14241" s="T911">В окопе когда ложился, песком как засыпет оттуда летит, когда мины падают, земля дрожит.</ta>
            <ta e="T931" id="Seg_14242" s="T924">Шырк-шырк- шырк это делают (т.е. свистят) только.</ta>
            <ta e="T938" id="Seg_14243" s="T931">И потом смена пополнения прибывает, оказывается.</ta>
            <ta e="T944" id="Seg_14244" s="T938">Вместо нас следующие прибывают, происходит.</ta>
            <ta e="T951" id="Seg_14245" s="T944">Шум такой стоит, плачут и всё такое вдруг.</ta>
            <ta e="T954" id="Seg_14246" s="T951">Некоторые стонут.</ta>
            <ta e="T966" id="Seg_14247" s="T954">Если стонут, я тоже начинаю стонать, потом успокаиваюсь, прислушиваюсь.</ta>
            <ta e="T968" id="Seg_14248" s="T966">В санчасть пришёл.</ta>
            <ta e="T975" id="Seg_14249" s="T968">Сан.. полевая работа бывает, санчасть.</ta>
            <ta e="T983" id="Seg_14250" s="T975">Там в санчасть как прибы.. э-э прибыл как, меня в тыл направили.</ta>
            <ta e="T987" id="Seg_14251" s="T983">В Иваново Шуйе лежал – в старом госпитале.</ta>
            <ta e="T999" id="Seg_14252" s="T987">После сра.. э-э пять месяцев отлежал как, в сторону дома отпустили.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-ChVD">
            <ta e="T208" id="Seg_14253" s="T204">[DCh]: Not, clear who speaks to the commander; but as there is accusative marking, it has to be the direct object of the verb.</ta>
            <ta e="T697" id="Seg_14254" s="T686">[DCh]: Sense of the first part not clear, even for native speakers.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-AkEE"
                      id="tx-AkEE"
                      speaker="AkEE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-AkEE">
            <ts e="T1009" id="Seg_14255" n="sc" s="T999">
               <ts e="T1009" id="Seg_14257" n="HIAT:u" s="T999">
                  <ts e="T1000" id="Seg_14259" n="HIAT:w" s="T999">Ehigi</ts>
                  <nts id="Seg_14260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_14262" n="HIAT:w" s="T1000">ihilleːtigit</ts>
                  <nts id="Seg_14263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_14265" n="HIAT:w" s="T1001">Ulakan</ts>
                  <nts id="Seg_14266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_14268" n="HIAT:w" s="T1002">Atʼečʼestvʼennɨj</ts>
                  <nts id="Seg_14269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_14271" n="HIAT:w" s="T1003">heriːge</ts>
                  <nts id="Seg_14272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_14274" n="HIAT:w" s="T1004">heriːlespit</ts>
                  <nts id="Seg_14275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_14277" n="HIAT:w" s="T1005">Vasʼilʼij</ts>
                  <nts id="Seg_14278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_14280" n="HIAT:w" s="T1006">Danʼilovʼičʼ</ts>
                  <nts id="Seg_14281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_14283" n="HIAT:w" s="T1007">Čʼuprʼin</ts>
                  <nts id="Seg_14284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_14286" n="HIAT:w" s="T1008">öjdölün</ts>
                  <nts id="Seg_14287" n="HIAT:ip">.</nts>
                  <nts id="Seg_14288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-AkEE">
            <ts e="T1009" id="Seg_14289" n="sc" s="T999">
               <ts e="T1000" id="Seg_14291" n="e" s="T999">Ehigi </ts>
               <ts e="T1001" id="Seg_14293" n="e" s="T1000">ihilleːtigit </ts>
               <ts e="T1002" id="Seg_14295" n="e" s="T1001">Ulakan </ts>
               <ts e="T1003" id="Seg_14297" n="e" s="T1002">Atʼečʼestvʼennɨj </ts>
               <ts e="T1004" id="Seg_14299" n="e" s="T1003">heriːge </ts>
               <ts e="T1005" id="Seg_14301" n="e" s="T1004">heriːlespit </ts>
               <ts e="T1006" id="Seg_14303" n="e" s="T1005">Vasʼilʼij </ts>
               <ts e="T1007" id="Seg_14305" n="e" s="T1006">Danʼilovʼičʼ </ts>
               <ts e="T1008" id="Seg_14307" n="e" s="T1007">Čʼuprʼin </ts>
               <ts e="T1009" id="Seg_14309" n="e" s="T1008">öjdölün. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-AkEE">
            <ta e="T1009" id="Seg_14310" s="T999">ChVD_AkEE_198204_SoldierInWar_nar.AkEE.001 (001.128)</ta>
         </annotation>
         <annotation name="st" tierref="st-AkEE">
            <ta e="T1009" id="Seg_14311" s="T999">Эһиги иһиллэтигит Улакан Отечественный һэриигэ һэриилэспит Василий Данилович Чуприн өйдөлүн.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-AkEE">
            <ta e="T1009" id="Seg_14312" s="T999">Ehigi ihilleːtigit Ulakan Atʼečʼestvʼennɨj heriːge heriːlespit Vasʼilʼij Danʼilovʼičʼ Čʼuprʼin öjdölün. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-AkEE">
            <ta e="T1000" id="Seg_14313" s="T999">ehigi</ta>
            <ta e="T1001" id="Seg_14314" s="T1000">ihilleː-ti-git</ta>
            <ta e="T1002" id="Seg_14315" s="T1001">Ulakan</ta>
            <ta e="T1003" id="Seg_14316" s="T1002">Atʼečʼestvʼennɨj</ta>
            <ta e="T1004" id="Seg_14317" s="T1003">heriː-ge</ta>
            <ta e="T1005" id="Seg_14318" s="T1004">heriː-l-e-s-pit</ta>
            <ta e="T1006" id="Seg_14319" s="T1005">Vasʼilʼij</ta>
            <ta e="T1007" id="Seg_14320" s="T1006">Danʼilovʼičʼ</ta>
            <ta e="T1008" id="Seg_14321" s="T1007">Čʼuprʼin</ta>
            <ta e="T1009" id="Seg_14322" s="T1008">öjdöl-ü-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp-AkEE">
            <ta e="T1000" id="Seg_14323" s="T999">ehigi</ta>
            <ta e="T1001" id="Seg_14324" s="T1000">ihilleː-TI-GIt</ta>
            <ta e="T1002" id="Seg_14325" s="T1001">ulakan</ta>
            <ta e="T1003" id="Seg_14326" s="T1002">atʼečʼestvʼennɨj</ta>
            <ta e="T1004" id="Seg_14327" s="T1003">heriː-GA</ta>
            <ta e="T1005" id="Seg_14328" s="T1004">heriː-LAː-A-s-BIT</ta>
            <ta e="T1006" id="Seg_14329" s="T1005">Bahiːlaj</ta>
            <ta e="T1007" id="Seg_14330" s="T1006">Danʼilovʼičʼ</ta>
            <ta e="T1008" id="Seg_14331" s="T1007">Čʼuprʼin</ta>
            <ta e="T1009" id="Seg_14332" s="T1008">öjdöl-tI-n</ta>
         </annotation>
         <annotation name="ge" tierref="ge-AkEE">
            <ta e="T1000" id="Seg_14333" s="T999">2PL.[NOM]</ta>
            <ta e="T1001" id="Seg_14334" s="T1000">listen-PST1-2PL</ta>
            <ta e="T1002" id="Seg_14335" s="T1001">big</ta>
            <ta e="T1003" id="Seg_14336" s="T1002">national</ta>
            <ta e="T1004" id="Seg_14337" s="T1003">war-DAT/LOC</ta>
            <ta e="T1005" id="Seg_14338" s="T1004">war-VBZ-EP-RECP/COLL-PTCP.PST</ta>
            <ta e="T1006" id="Seg_14339" s="T1005">Vasiliy</ta>
            <ta e="T1007" id="Seg_14340" s="T1006">Danilovich</ta>
            <ta e="T1008" id="Seg_14341" s="T1007">Chuprin.[NOM]</ta>
            <ta e="T1009" id="Seg_14342" s="T1008">memory-3SG-ACC</ta>
         </annotation>
         <annotation name="gg" tierref="gg-AkEE">
            <ta e="T1000" id="Seg_14343" s="T999">2PL.[NOM]</ta>
            <ta e="T1001" id="Seg_14344" s="T1000">zuhören-PST1-2PL</ta>
            <ta e="T1002" id="Seg_14345" s="T1001">groß</ta>
            <ta e="T1003" id="Seg_14346" s="T1002">vaterländisch</ta>
            <ta e="T1004" id="Seg_14347" s="T1003">Krieg-DAT/LOC</ta>
            <ta e="T1005" id="Seg_14348" s="T1004">Krieg-VBZ-EP-RECP/COLL-PTCP.PST</ta>
            <ta e="T1006" id="Seg_14349" s="T1005">Vasilij</ta>
            <ta e="T1007" id="Seg_14350" s="T1006">Danilowitsch</ta>
            <ta e="T1008" id="Seg_14351" s="T1007">Tschuprin.[NOM]</ta>
            <ta e="T1009" id="Seg_14352" s="T1008">Erinnerung-3SG-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr-AkEE">
            <ta e="T1000" id="Seg_14353" s="T999">2PL.[NOM]</ta>
            <ta e="T1001" id="Seg_14354" s="T1000">слушать-PST1-2PL</ta>
            <ta e="T1002" id="Seg_14355" s="T1001">большой</ta>
            <ta e="T1003" id="Seg_14356" s="T1002">отечественный</ta>
            <ta e="T1004" id="Seg_14357" s="T1003">война-DAT/LOC</ta>
            <ta e="T1005" id="Seg_14358" s="T1004">война-VBZ-EP-RECP/COLL-PTCP.PST</ta>
            <ta e="T1006" id="Seg_14359" s="T1005">Василий</ta>
            <ta e="T1007" id="Seg_14360" s="T1006">Данилович</ta>
            <ta e="T1008" id="Seg_14361" s="T1007">Чуприн.[NOM]</ta>
            <ta e="T1009" id="Seg_14362" s="T1008">воспоминание-3SG-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc-AkEE">
            <ta e="T1000" id="Seg_14363" s="T999">pers-pro:case</ta>
            <ta e="T1001" id="Seg_14364" s="T1000">v-v:tense-v:poss.pn</ta>
            <ta e="T1002" id="Seg_14365" s="T1001">adj</ta>
            <ta e="T1003" id="Seg_14366" s="T1002">adj</ta>
            <ta e="T1004" id="Seg_14367" s="T1003">n-n:case</ta>
            <ta e="T1005" id="Seg_14368" s="T1004">n-n&gt;v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T1006" id="Seg_14369" s="T1005">propr</ta>
            <ta e="T1007" id="Seg_14370" s="T1006">propr</ta>
            <ta e="T1008" id="Seg_14371" s="T1007">propr-n:case</ta>
            <ta e="T1009" id="Seg_14372" s="T1008">n-n:poss-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-AkEE">
            <ta e="T1000" id="Seg_14373" s="T999">pers</ta>
            <ta e="T1001" id="Seg_14374" s="T1000">v</ta>
            <ta e="T1002" id="Seg_14375" s="T1001">adj</ta>
            <ta e="T1003" id="Seg_14376" s="T1002">adj</ta>
            <ta e="T1004" id="Seg_14377" s="T1003">n</ta>
            <ta e="T1005" id="Seg_14378" s="T1004">v</ta>
            <ta e="T1006" id="Seg_14379" s="T1005">propr</ta>
            <ta e="T1007" id="Seg_14380" s="T1006">propr</ta>
            <ta e="T1008" id="Seg_14381" s="T1007">propr</ta>
            <ta e="T1009" id="Seg_14382" s="T1008">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-AkEE">
            <ta e="T1000" id="Seg_14383" s="T999">pro.h:A</ta>
            <ta e="T1008" id="Seg_14384" s="T1007">np.h:Poss</ta>
            <ta e="T1009" id="Seg_14385" s="T1008">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-AkEE">
            <ta e="T1000" id="Seg_14386" s="T999">pro.h:S</ta>
            <ta e="T1001" id="Seg_14387" s="T1000">v:pred</ta>
            <ta e="T1005" id="Seg_14388" s="T1001">s:rel</ta>
            <ta e="T1009" id="Seg_14389" s="T1008">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST-AkEE">
            <ta e="T1000" id="Seg_14390" s="T999">accs-sit</ta>
            <ta e="T1004" id="Seg_14391" s="T1003">accs-gen</ta>
            <ta e="T1008" id="Seg_14392" s="T1007">giv-active</ta>
            <ta e="T1009" id="Seg_14393" s="T1008">accs-inf</ta>
         </annotation>
         <annotation name="Top" tierref="Top-AkEE">
            <ta e="T1000" id="Seg_14394" s="T999">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc-AkEE">
            <ta e="T1009" id="Seg_14395" s="T1000">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR-AkEE">
            <ta e="T1003" id="Seg_14396" s="T1002">RUS:cult</ta>
            <ta e="T1006" id="Seg_14397" s="T1005">RUS:cult</ta>
            <ta e="T1007" id="Seg_14398" s="T1006">RUS:cult</ta>
            <ta e="T1008" id="Seg_14399" s="T1007">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-AkEE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-AkEE">
            <ta e="T1003" id="Seg_14400" s="T1002">dir:bare</ta>
            <ta e="T1006" id="Seg_14401" s="T1005">dir:bare</ta>
            <ta e="T1007" id="Seg_14402" s="T1006">dir:bare</ta>
            <ta e="T1008" id="Seg_14403" s="T1007">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-AkEE">
            <ta e="T1004" id="Seg_14404" s="T1001">RUS:calq</ta>
         </annotation>
         <annotation name="fe" tierref="fe-AkEE">
            <ta e="T1009" id="Seg_14405" s="T999">You heard the memories of Vasiliy Danilovich Churpin, who has fought in the Great Patriotic War.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-AkEE">
            <ta e="T1009" id="Seg_14406" s="T999">Sie hörten die Erinnerungen von Vasilij Danilowitsch Tschuprin, der im Großer Vaterländischen Krieg gekämpft hat.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-AkEE">
            <ta e="T1009" id="Seg_14407" s="T999">Вы прослушали воспоминания Василия Даниловича Чуприна, воевавшего в Великую Отечественную войну.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-AkEE">
            <ta e="T1009" id="Seg_14408" s="T999">Вы прослушали в Великую Отечественную войну воевавшего Василия Даниловича Чуприна воспоминания.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-AkEE" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T9" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T54" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T80" />
            <conversion-tli id="T481" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T8" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T930" />
            <conversion-tli id="T883" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T937" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-ChVD"
                          name="ref"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-ChVD"
                          name="st"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-ChVD"
                          name="ts"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-ChVD"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-ChVD"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-ChVD"
                          name="mb"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-ChVD"
                          name="mp"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-ChVD"
                          name="ge"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-ChVD"
                          name="gg"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-ChVD"
                          name="gr"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-ChVD"
                          name="mc"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-ChVD"
                          name="ps"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-ChVD"
                          name="SeR"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-ChVD"
                          name="SyF"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-ChVD"
                          name="IST"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-ChVD"
                          name="Top"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-ChVD"
                          name="Foc"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-ChVD"
                          name="BOR"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-ChVD"
                          name="BOR-Phon"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-ChVD"
                          name="BOR-Morph"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-ChVD"
                          name="CS"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-ChVD"
                          name="fe"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-ChVD"
                          name="fg"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-ChVD"
                          name="fr"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-ChVD"
                          name="ltr"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-ChVD"
                          name="nt"
                          segmented-tier-id="tx-ChVD"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-AkEE"
                          name="ref"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-AkEE"
                          name="st"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-AkEE"
                          name="ts"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-AkEE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-AkEE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-AkEE"
                          name="mb"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-AkEE"
                          name="mp"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-AkEE"
                          name="ge"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-AkEE"
                          name="gg"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-AkEE"
                          name="gr"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-AkEE"
                          name="mc"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-AkEE"
                          name="ps"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-AkEE"
                          name="SeR"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-AkEE"
                          name="SyF"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-AkEE"
                          name="IST"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-AkEE"
                          name="Top"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-AkEE"
                          name="Foc"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-AkEE"
                          name="BOR"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-AkEE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-AkEE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-AkEE"
                          name="CS"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-AkEE"
                          name="fe"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-AkEE"
                          name="fg"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-AkEE"
                          name="fr"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-AkEE"
                          name="ltr"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-AkEE"
                          name="nt"
                          segmented-tier-id="tx-AkEE"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
