<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE331A2B4-1FAB-1959-E4A6-2A973B651271">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>LaVN_KuNS_1999_MusicRepressions_conv</transcription-name>
         <referenced-file url="LaVN_KuNS_1999_MusicRepressions_conv.wav" />
         <referenced-file url="LaVN_KuNS_1999_MusicRepressions_conv.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\conv\LaVN_KuNS_1999_MusicRepressions_conv\LaVN_KuNS_1999_MusicRepressions_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">678</ud-information>
            <ud-information attribute-name="# HIAT:w">462</ud-information>
            <ud-information attribute-name="# e">465</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">3</ud-information>
            <ud-information attribute-name="# HIAT:u">72</ud-information>
            <ud-information attribute-name="# sc">62</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KuNS">
            <abbreviation>KuNS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="LaVN">
            <abbreviation>LaVN</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.5209999999999582" type="appl" />
         <tli id="T2" time="1.0419999999999163" type="appl" />
         <tli id="T3" time="1.5619999999998981" type="appl" />
         <tli id="T4" time="2.0829999999998563" type="appl" />
         <tli id="T5" time="2.3889999999998963" type="appl" />
         <tli id="T6" time="2.9829999999999472" type="appl" />
         <tli id="T7" time="3.576999999999998" type="appl" />
         <tli id="T8" time="4.170999999999822" type="appl" />
         <tli id="T9" time="4.763999999999896" type="appl" />
         <tli id="T10" time="5.357999999999947" type="appl" />
         <tli id="T11" time="5.951999999999998" type="appl" />
         <tli id="T12" time="6.545999999999822" type="appl" />
         <tli id="T13" time="7.139999999999873" type="appl" />
         <tli id="T14" time="7.733999999999924" type="appl" />
         <tli id="T15" time="8.182999999999993" type="appl" />
         <tli id="T16" time="8.631999999999834" type="appl" />
         <tli id="T17" time="9.08199999999988" type="appl" />
         <tli id="T18" time="9.530999999999949" type="appl" />
         <tli id="T19" time="9.980000000000018" type="appl" />
         <tli id="T20" time="10.42899999999986" type="appl" />
         <tli id="T21" time="10.878999999999905" type="appl" />
         <tli id="T22" time="11.327999999999975" type="appl" />
         <tli id="T23" time="11.776999999999816" type="appl" />
         <tli id="T24" time="12.230999999999995" type="appl" />
         <tli id="T25" time="12.684999999999945" type="appl" />
         <tli id="T26" time="13.139999999999873" type="appl" />
         <tli id="T27" time="13.593999999999824" type="appl" />
         <tli id="T28" time="14.048000000000002" type="appl" />
         <tli id="T29" time="14.501999999999953" type="appl" />
         <tli id="T30" time="14.955999999999904" type="appl" />
         <tli id="T31" time="15.41099999999983" type="appl" />
         <tli id="T32" time="15.865000000000009" type="appl" />
         <tli id="T33" time="16.31899999999996" type="appl" />
         <tli id="T34" time="17.29399999999987" type="appl" />
         <tli id="T35" time="18.269000000000005" type="appl" />
         <tli id="T36" time="18.930999999999813" type="appl" />
         <tli id="T37" time="19.592999999999847" type="appl" />
         <tli id="T38" time="20.25499999999988" type="appl" />
         <tli id="T39" time="20.91599999999994" type="appl" />
         <tli id="T40" time="21.577999999999975" type="appl" />
         <tli id="T41" time="22.24000000000001" type="appl" />
         <tli id="T42" time="22.901999999999816" type="appl" />
         <tli id="T43" time="23.56399999999985" type="appl" />
         <tli id="T44" time="23.93399999999997" type="appl" />
         <tli id="T45" time="24.302999999999884" type="appl" />
         <tli id="T46" time="24.819999999999936" type="appl" />
         <tli id="T47" time="25.33699999999999" type="appl" />
         <tli id="T48" time="25.772598443158163" />
         <tli id="T49" time="26.538999999999987" type="appl" />
         <tli id="T50" time="27.223999999999933" type="appl" />
         <tli id="T51" time="27.9079999999999" type="appl" />
         <tli id="T52" time="28.592999999999847" type="appl" />
         <tli id="T53" time="29.62582190413732" />
         <tli id="T54" time="30.327999999999975" type="appl" />
         <tli id="T55" />
         <tli id="T56" time="30.861999999999853" type="appl" />
         <tli id="T57" time="31.403999999999996" type="appl" />
         <tli id="T58" time="31.945999999999913" type="appl" />
         <tli id="T59" time="32.4219999999998" type="appl" />
         <tli id="T60" time="32.896999999999935" type="appl" />
         <tli id="T61" time="33.37299999999982" type="appl" />
         <tli id="T62" time="33.84899999999993" type="appl" />
         <tli id="T63" time="34.32399999999984" type="appl" />
         <tli id="T64" time="34.799999999999955" type="appl" />
         <tli id="T65" time="35.27599999999984" type="appl" />
         <tli id="T66" time="35.750999999999976" type="appl" />
         <tli id="T67" time="36.22699999999986" type="appl" />
         <tli id="T68" time="36.69699999999989" type="appl" />
         <tli id="T69" time="37.166999999999916" type="appl" />
         <tli id="T70" time="37.475999999999885" type="appl" />
         <tli id="T71" time="37.78399999999988" type="appl" />
         <tli id="T72" time="38.09299999999985" type="appl" />
         <tli id="T73" time="38.40099999999984" type="appl" />
         <tli id="T74" time="38.552234039519824" />
         <tli id="T75" time="39.179999999999836" type="appl" />
         <tli id="T76" time="39.649999999999864" type="appl" />
         <tli id="T77" time="40.118999999999915" type="appl" />
         <tli id="T78" time="40.58899999999994" type="appl" />
         <tli id="T79" time="41.05899999999997" type="appl" />
         <tli id="T80" time="41.528999999999996" type="appl" />
         <tli id="T81" time="42.06399999999985" type="appl" />
         <tli id="T82" time="42.59899999999993" type="appl" />
         <tli id="T83" time="43.134000000000015" type="appl" />
         <tli id="T84" />
         <tli id="T85" time="43.511999999999944" type="appl" />
         <tli id="T86" time="43.90899999999988" type="appl" />
         <tli id="T87" time="44.30599999999981" type="appl" />
         <tli id="T88" time="44.59206181745601" />
         <tli id="T89" time="45.09999999999991" type="appl" />
         <tli id="T90" time="45.27800000000002" type="appl" />
         <tli id="T91" time="45.83699999999999" type="appl" />
         <tli id="T92" time="46.39599999999996" type="appl" />
         <tli id="T93" time="46.955999999999904" type="appl" />
         <tli id="T94" />
         <tli id="T95" time="47.480999999999995" type="appl" />
         <tli id="T96" time="47.92530010550027" />
         <tli id="T97" time="48.771999999999935" type="appl" />
         <tli id="T98" time="49.46100000000001" type="appl" />
         <tli id="T99" time="50.149999999999864" type="appl" />
         <tli id="T100" time="50.83899999999994" type="appl" />
         <tli id="T101" time="51.52800000000002" type="appl" />
         <tli id="T102" time="52.22399999999993" type="appl" />
         <tli id="T103" time="52.92099999999982" type="appl" />
         <tli id="T104" time="53.61699999999996" type="appl" />
         <tli id="T105" time="54.125999999999976" type="appl" />
         <tli id="T106" time="54.63499999999999" type="appl" />
         <tli id="T107" time="55.144000000000005" type="appl" />
         <tli id="T108" time="55.65300000000002" type="appl" />
         <tli id="T109" time="56.16199999999981" type="appl" />
         <tli id="T110" time="56.669999999999845" type="appl" />
         <tli id="T111" time="57.17899999999986" type="appl" />
         <tli id="T112" time="57.687999999999874" type="appl" />
         <tli id="T113" time="58.19699999999989" type="appl" />
         <tli id="T114" time="58.705999999999904" type="appl" />
         <tli id="T115" time="59.21499999999992" type="appl" />
         <tli id="T116" time="59.610999999999876" type="appl" />
         <tli id="T117" time="60.006999999999834" type="appl" />
         <tli id="T118" time="60.40300000000002" type="appl" />
         <tli id="T119" time="60.79899999999998" type="appl" />
         <tli id="T120" time="61.194999999999936" type="appl" />
         <tli id="T121" time="61.590999999999894" type="appl" />
         <tli id="T122" time="62.02399999999989" type="appl" />
         <tli id="T123" time="62.45699999999988" type="appl" />
         <tli id="T124" time="62.888999999999896" type="appl" />
         <tli id="T125" time="63.32199999999989" type="appl" />
         <tli id="T126" time="63.75499999999988" type="appl" />
         <tli id="T127" time="64.18799999999987" type="appl" />
         <tli id="T128" time="64.62099999999987" type="appl" />
         <tli id="T129" time="65.05399999999986" type="appl" />
         <tli id="T130" time="65.48599999999988" type="appl" />
         <tli id="T131" time="65.91899999999987" type="appl" />
         <tli id="T132" time="66.35199999999986" type="appl" />
         <tli id="T133" time="67.0619999999999" type="appl" />
         <tli id="T134" time="67.77099999999996" type="appl" />
         <tli id="T135" time="68.16472299050497" />
         <tli id="T136" time="68.66499999999996" type="appl" />
         <tli id="T137" time="69.19599999999991" type="appl" />
         <tli id="T138" time="69.63801431382055" />
         <tli id="T139" time="70.17599999999993" type="appl" />
         <tli id="T140" time="70.6239999999998" type="appl" />
         <tli id="T141" time="71.07299999999987" type="appl" />
         <tli id="T142" time="71.52099999999996" type="appl" />
         <tli id="T143" time="71.9699999999998" type="appl" />
         <tli id="T144" time="72.41799999999989" type="appl" />
         <tli id="T145" time="72.86699999999996" type="appl" />
         <tli id="T146" time="73.31499999999983" type="appl" />
         <tli id="T147" time="73.7639999999999" type="appl" />
         <tli id="T148" time="74.5809999999999" type="appl" />
         <tli id="T149" time="75.39699999999993" type="appl" />
         <tli id="T150" time="76.21399999999994" type="appl" />
         <tli id="T151" time="77.02999999999997" type="appl" />
         <tli id="T152" time="77.84699999999998" type="appl" />
         <tli id="T153" time="78.41799999999989" type="appl" />
         <tli id="T154" time="78.85775141855093" />
         <tli id="T155" time="79.50199999999995" type="appl" />
         <tli id="T156" time="80.01499999999987" type="appl" />
         <tli id="T157" time="80.529" type="appl" />
         <tli id="T158" time="81.04199999999992" type="appl" />
         <tli id="T159" time="81.55499999999984" type="appl" />
         <tli id="T160" time="82.06799999999998" type="appl" />
         <tli id="T162" time="82.32499999999993" type="intp" />
         <tli id="T161" time="82.58199999999988" type="appl" />
         <tli id="T163" time="83.60799999999995" type="appl" />
         <tli id="T164" time="84.471" type="appl" />
         <tli id="T165" time="85.33399999999983" type="appl" />
         <tli id="T166" time="86.19699999999989" type="appl" />
         <tli id="T167" time="87.07999999999993" type="appl" />
         <tli id="T168" time="88.3241481565966" />
         <tli id="T169" time="88.8449999999998" type="appl" />
         <tli id="T170" />
         <tli id="T171" time="89.29199999999992" type="appl" />
         <tli id="T172" time="89.91300000000001" type="appl" />
         <tli id="T173" time="90.125" type="appl" />
         <tli id="T174" time="90.81074391947763" />
         <tli id="T175" time="91.19799999999987" type="appl" />
         <tli id="T176" time="91.5179999999998" type="appl" />
         <tli id="T177" time="91.83699999999999" type="appl" />
         <tli id="T178" time="92.15699999999993" type="appl" />
         <tli id="T179" time="92.47599999999989" type="appl" />
         <tli id="T180" time="92.79599999999982" type="appl" />
         <tli id="T181" time="93.06401300219554" />
         <tli id="T182" time="95.66393886687005" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" time="95.94393088306579" />
         <tli id="T187" time="96.00199999999995" type="appl" />
         <tli id="T188" time="96.62599999999998" type="appl" />
         <tli id="T189" time="97.2489999999998" type="appl" />
         <tli id="T190" time="97.87199999999984" type="appl" />
         <tli id="T191" time="98.49599999999987" type="appl" />
         <tli id="T192" time="99.11899999999991" type="appl" />
         <tli id="T193" time="99.74199999999996" type="appl" />
         <tli id="T194" time="100.36599999999999" type="appl" />
         <tli id="T195" time="100.9889999999998" type="appl" />
         <tli id="T196" time="101.529" type="appl" />
         <tli id="T197" time="102.06799999999998" type="appl" />
         <tli id="T198" time="102.60799999999995" type="appl" />
         <tli id="T199" time="103.14799999999991" type="appl" />
         <tli id="T200" time="103.68799999999987" type="appl" />
         <tli id="T201" time="104.22699999999986" type="appl" />
         <tli id="T202" time="104.76699999999983" type="appl" />
         <tli id="T203" time="105.10500000000002" type="appl" />
         <tli id="T204" time="105.44399999999996" type="appl" />
         <tli id="T205" time="105.78199999999993" type="appl" />
         <tli id="T206" time="106.12099999999987" type="appl" />
         <tli id="T207" time="106.45899999999983" type="appl" />
         <tli id="T208" time="106.798" type="appl" />
         <tli id="T209" time="107.13599999999997" type="appl" />
         <tli id="T210" time="107.47499999999991" type="appl" />
         <tli id="T211" time="107.81299999999987" type="appl" />
         <tli id="T212" time="108.15199999999982" type="appl" />
         <tli id="T213" time="108.49000000000001" type="appl" />
         <tli id="T214" time="108.82899999999995" type="appl" />
         <tli id="T215" time="109.16699999999992" type="appl" />
         <tli id="T216" time="109.346" type="appl" />
         <tli id="T217" time="109.74599999999987" type="appl" />
         <tli id="T218" time="110.14499999999998" type="appl" />
         <tli id="T219" time="110.54399999999987" type="appl" />
         <tli id="T220" time="110.94299999999998" type="appl" />
         <tli id="T221" time="111.34199999999987" type="appl" />
         <tli id="T222" time="111.74199999999996" type="appl" />
         <tli id="T223" time="112.14099999999985" type="appl" />
         <tli id="T224" time="112.60899999999992" type="appl" />
         <tli id="T225" time="113.077" type="appl" />
         <tli id="T226" time="113.54499999999985" type="appl" />
         <tli id="T227" time="114.0139999999999" type="appl" />
         <tli id="T228" time="114.48199999999997" type="appl" />
         <tli id="T229" time="114.94999999999982" type="appl" />
         <tli id="T230" time="115.44499999999994" type="appl" />
         <tli id="T231" time="115.93899999999985" type="appl" />
         <tli id="T232" time="116.43399999999997" type="appl" />
         <tli id="T233" time="116.92899999999986" type="appl" />
         <tli id="T234" time="117.42399999999998" type="appl" />
         <tli id="T235" time="117.91799999999989" type="appl" />
         <tli id="T236" time="118.41300000000001" type="appl" />
         <tli id="T237" time="118.78599999999983" type="appl" />
         <tli id="T238" time="119.15899999999988" type="appl" />
         <tli id="T239" time="119.53199999999993" type="appl" />
         <tli id="T240" time="119.90499999999997" type="appl" />
         <tli id="T241" time="120.27699999999982" type="appl" />
         <tli id="T242" time="120.64999999999986" type="appl" />
         <tli id="T244" time="121.39599999999996" type="appl" />
         <tli id="T245" time="121.90499999999997" type="appl" />
         <tli id="T246" time="122.41399999999999" type="appl" />
         <tli id="T247" time="122.92399999999998" type="appl" />
         <tli id="T248" time="123.43299999999999" type="appl" />
         <tli id="T249" time="123.94200000000001" type="appl" />
         <tli id="T250" time="124.45100000000002" type="appl" />
         <tli id="T251" time="124.95999999999981" type="appl" />
         <tli id="T252" time="125.46899999999982" type="appl" />
         <tli id="T253" time="125.97799999999984" type="appl" />
         <tli id="T254" time="126.48799999999983" type="appl" />
         <tli id="T255" time="126.99699999999984" type="appl" />
         <tli id="T256" time="127.50599999999986" type="appl" />
         <tli id="T380" time="127.62699999999988" type="intp" />
         <tli id="T257" time="127.86899999999991" type="appl" />
         <tli id="T258" time="128.23299999999995" type="appl" />
         <tli id="T259" time="128.596" type="appl" />
         <tli id="T260" time="128.95899999999983" type="appl" />
         <tli id="T261" time="129.32299999999987" type="appl" />
         <tli id="T262" time="129.68599999999992" type="appl" />
         <tli id="T263" time="130.04899999999998" type="appl" />
         <tli id="T264" time="130.413" type="appl" />
         <tli id="T265" time="130.77599999999984" type="appl" />
         <tli id="T266" time="131.1389999999999" type="appl" />
         <tli id="T267" time="131.50299999999993" type="appl" />
         <tli id="T268" time="131.86599999999999" type="appl" />
         <tli id="T269" time="132.22899999999981" type="appl" />
         <tli id="T271" time="132.9559999999999" type="appl" />
         <tli id="T272" time="133.56799999999998" type="appl" />
         <tli id="T273" time="134.17999999999984" type="appl" />
         <tli id="T274" time="134.79099999999994" type="appl" />
         <tli id="T275" time="135.40300000000002" type="appl" />
         <tli id="T276" time="136.01499999999987" type="appl" />
         <tli id="T243" time="136.35199999999986" type="intp" />
         <tli id="T277" time="136.68899999999985" type="appl" />
         <tli id="T278" time="137.3639999999998" type="appl" />
         <tli id="T279" time="138.038" type="appl" />
         <tli id="T281" time="139.38699999999994" type="appl" />
         <tli id="T282" time="139.86599999999999" type="appl" />
         <tli id="T283" time="140.34399999999982" type="appl" />
         <tli id="T284" time="140.8219999999999" type="appl" />
         <tli id="T285" time="141.30099999999993" type="appl" />
         <tli id="T286" time="141.77999999999997" type="appl" />
         <tli id="T287" time="142.2579999999998" type="appl" />
         <tli id="T288" time="142.73599999999988" type="appl" />
         <tli id="T289" time="143.21499999999992" type="appl" />
         <tli id="T290" time="143.69399999999996" type="appl" />
         <tli id="T291" time="144.1719999999998" type="appl" />
         <tli id="T292" time="144.64999999999986" type="appl" />
         <tli id="T293" time="145.1289999999999" type="appl" />
         <tli id="T294" time="145.52800000000002" type="appl" />
         <tli id="T295" time="145.9269999999999" type="appl" />
         <tli id="T296" time="146.32600000000002" type="appl" />
         <tli id="T297" time="146.7249999999999" type="appl" />
         <tli id="T298" time="147.1239999999998" type="appl" />
         <tli id="T299" time="147.5229999999999" type="appl" />
         <tli id="T300" time="147.9219999999998" type="appl" />
         <tli id="T301" time="148.3209999999999" type="appl" />
         <tli id="T302" time="148.7199999999998" type="appl" />
         <tli id="T303" time="149.11899999999991" type="appl" />
         <tli id="T304" time="149.5179999999998" type="appl" />
         <tli id="T305" time="149.91699999999992" type="appl" />
         <tli id="T306" time="150.3159999999998" type="appl" />
         <tli id="T307" time="150.7159999999999" type="appl" />
         <tli id="T308" time="151.115" type="appl" />
         <tli id="T309" time="151.5139999999999" type="appl" />
         <tli id="T310" time="151.913" type="appl" />
         <tli id="T311" time="152.3119999999999" type="appl" />
         <tli id="T312" time="152.711" type="appl" />
         <tli id="T313" time="153.1099999999999" type="appl" />
         <tli id="T314" time="153.50900000000001" type="appl" />
         <tli id="T315" time="153.9079999999999" type="appl" />
         <tli id="T316" time="154.30700000000002" type="appl" />
         <tli id="T317" time="154.7059999999999" type="appl" />
         <tli id="T318" time="155.10500000000002" type="appl" />
         <tli id="T319" time="155.5039999999999" type="appl" />
         <tli id="T320" time="155.99" type="appl" />
         <tli id="T321" time="156.47699999999986" type="appl" />
         <tli id="T322" time="156.96299999999997" type="appl" />
         <tli id="T323" time="157.44899999999984" type="appl" />
         <tli id="T324" time="157.93599999999992" type="appl" />
         <tli id="T325" time="158.4219999999998" type="appl" />
         <tli id="T326" time="158.9079999999999" type="appl" />
         <tli id="T327" time="159.394" type="appl" />
         <tli id="T328" time="159.88099999999986" type="appl" />
         <tli id="T329" time="160.36699999999996" type="appl" />
         <tli id="T330" time="160.70899999999983" type="appl" />
         <tli id="T331" time="161.0519999999999" type="appl" />
         <tli id="T332" time="161.394" type="appl" />
         <tli id="T333" time="161.73699999999985" type="appl" />
         <tli id="T334" time="162.07899999999995" type="appl" />
         <tli id="T335" time="162.42099999999982" type="appl" />
         <tli id="T336" time="162.7639999999999" type="appl" />
         <tli id="T337" time="163.106" type="appl" />
         <tli id="T338" time="163.44899999999984" type="appl" />
         <tli id="T339" time="163.79099999999994" type="appl" />
         <tli id="T340" time="164.1329999999998" type="appl" />
         <tli id="T341" time="164.47599999999989" type="appl" />
         <tli id="T342" time="164.81799999999998" type="appl" />
         <tli id="T343" time="165.16099999999983" type="appl" />
         <tli id="T344" time="165.50299999999993" type="appl" />
         <tli id="T345" time="166.0999999999999" type="appl" />
         <tli id="T346" time="166.76857802743004" />
         <tli id="T347" time="167.29499999999985" type="appl" />
         <tli id="T348" time="167.346" type="appl" />
         <tli id="T349" time="167.52855635710415" />
         <tli id="T350" time="168.481" type="appl" />
         <tli id="T351" />
         <tli id="T352" />
         <tli id="T353" time="168.5909999999999" type="appl" />
         <tli id="T354" time="168.90699999999993" type="appl" />
         <tli id="T355" time="169.30183912634374" />
         <tli id="T356" time="169.53999999999996" type="appl" />
         <tli id="T357" />
         <tli id="T358" time="169.98599999999988" type="appl" />
         <tli id="T359" time="170.481" type="appl" />
         <tli id="T360" time="170.97699999999986" type="appl" />
         <tli id="T361" time="171.47299999999996" type="appl" />
         <tli id="T362" time="171.96899999999982" type="appl" />
         <tli id="T363" time="172.46399999999994" type="appl" />
         <tli id="T364" time="172.9599999999998" type="appl" />
         <tli id="T365" time="173.4559999999999" type="appl" />
         <tli id="T366" time="173.87599999999998" type="appl" />
         <tli id="T367" time="174.29499999999985" type="appl" />
         <tli id="T368" time="174.71499999999992" type="appl" />
         <tli id="T369" time="175.13400000000001" type="appl" />
         <tli id="T370" time="175.55399999999986" type="appl" />
         <tli id="T371" time="175.86831855379089" />
         <tli id="T372" time="176.10099999999989" type="appl" />
         <tli id="T373" time="176.67099999999982" type="appl" />
         <tli id="T374" time="177.24" type="appl" />
         <tli id="T375" time="177.80999999999995" type="appl" />
         <tli id="T376" time="178.3789999999999" type="appl" />
         <tli id="T270" time="178.63219999999993" type="intp" />
         <tli id="T377" time="178.80099999999993" type="appl" />
         <tli id="T378" time="179.22399999999993" type="appl" />
         <tli id="T379" time="179.64599999999996" type="appl" />
         <tli id="T381" time="180.49099999999999" type="appl" />
         <tli id="T382" time="181.06799999999998" type="appl" />
         <tli id="T383" time="181.64599999999996" type="appl" />
         <tli id="T384" time="182.22299999999996" type="appl" />
         <tli id="T385" time="182.79999999999995" type="appl" />
         <tli id="T386" time="183.37699999999995" type="appl" />
         <tli id="T387" time="183.95499999999993" type="appl" />
         <tli id="T388" time="184.53199999999993" type="appl" />
         <tli id="T389" time="185.07600000000002" type="appl" />
         <tli id="T390" time="185.61899999999991" type="appl" />
         <tli id="T391" time="186.24799999999982" type="appl" />
         <tli id="T392" time="186.87599999999998" type="appl" />
         <tli id="T393" time="187.50499999999988" type="appl" />
         <tli id="T394" time="188.13400000000001" type="appl" />
         <tli id="T395" time="188.76199999999994" type="appl" />
         <tli id="T396" time="189.39099999999985" type="appl" />
         <tli id="T397" time="190.01999999999998" type="appl" />
         <tli id="T398" time="190.6479999999999" type="appl" />
         <tli id="T399" time="191.27699999999982" type="appl" />
         <tli id="T400" time="191.8789999999999" type="appl" />
         <tli id="T401" time="192.481" type="appl" />
         <tli id="T402" time="193.08199999999988" type="appl" />
         <tli id="T403" time="193.68399999999997" type="appl" />
         <tli id="T404" time="194.28599999999983" type="appl" />
         <tli id="T405" />
         <tli id="T406" time="194.50599999999986" type="appl" />
         <tli id="T407" time="194.88799999999992" type="appl" />
         <tli id="T408" time="195.03443871004532" />
         <tli id="T409" time="195.53199999999993" type="appl" />
         <tli id="T410" time="196.0229999999999" type="appl" />
         <tli id="T411" time="196.5139999999999" type="appl" />
         <tli id="T412" time="197.00599999999986" type="appl" />
         <tli id="T413" time="197.43999999999983" type="appl" />
         <tli id="T414" time="197.8739999999998" type="appl" />
         <tli id="T415" time="198.30700000000002" type="appl" />
         <tli id="T416" time="198.74099999999999" type="appl" />
         <tli id="T417" time="199.17499999999995" type="appl" />
         <tli id="T418" time="199.60899999999992" type="appl" />
         <tli id="T419" time="200.04199999999992" type="appl" />
         <tli id="T420" time="200.47599999999989" type="appl" />
         <tli id="T421" time="200.90999999999985" type="appl" />
         <tli id="T422" time="201.39499999999998" type="appl" />
         <tli id="T423" time="201.88099999999986" type="appl" />
         <tli id="T424" time="202.36599999999999" type="appl" />
         <tli id="T425" time="202.85099999999989" type="appl" />
         <tli id="T426" time="203.337" type="appl" />
         <tli id="T427" time="203.8219999999999" type="appl" />
         <tli id="T428" time="204.30700000000002" type="appl" />
         <tli id="T429" time="204.9074905192324" />
         <tli id="T430" time="205.27800000000002" type="appl" />
         <tli id="T431" time="205.4699999999998" type="appl" />
         <tli id="T432" time="206.02599999999984" type="appl" />
         <tli id="T433" time="206.58299999999986" type="appl" />
         <tli id="T434" time="207.13999999999987" type="appl" />
         <tli id="T435" time="208.0429999999999" type="appl" />
         <tli id="T436" time="208.7407145504833" />
         <tli id="T437" time="209.41999999999985" type="appl" />
         <tli id="T438" time="209.8929999999998" type="appl" />
         <tli id="T439" time="210.36599999999999" type="appl" />
         <tli id="T440" time="210.83899999999994" type="appl" />
         <tli id="T441" time="211.3119999999999" type="appl" />
         <tli id="T442" time="211.78599999999983" type="appl" />
         <tli id="T443" time="212.25900000000001" type="appl" />
         <tli id="T444" time="212.73199999999997" type="appl" />
         <tli id="T445" time="213.20499999999993" type="appl" />
         <tli id="T446" time="213.67799999999988" type="appl" />
         <tli id="T447" time="214.15099999999984" type="appl" />
         <tli id="T448" time="214.6239999999998" type="appl" />
         <tli id="T449" time="215.15199999999982" type="appl" />
         <tli id="T450" time="215.67999999999984" type="appl" />
         <tli id="T451" time="216.20699999999988" type="appl" />
         <tli id="T452" time="216.7349999999999" type="appl" />
         <tli id="T453" time="217.26299999999992" type="appl" />
         <tli id="T454" time="217.79099999999994" type="appl" />
         <tli id="T455" time="218.31899999999996" type="appl" />
         <tli id="T456" time="218.84699999999998" type="appl" />
         <tli id="T457" time="219.3739999999998" type="appl" />
         <tli id="T458" time="219.90199999999982" type="appl" />
         <tli id="T459" time="220.09372415956202" />
         <tli id="T460" time="220.7389999999998" type="appl" />
         <tli id="T461" time="221.26699999999983" type="appl" />
         <tli id="T462" time="221.79599999999982" type="appl" />
         <tli id="T280" time="222.03645454545438" type="intp" />
         <tli id="T463" time="222.32499999999982" type="appl" />
         <tli id="T464" time="222.85399999999981" type="appl" />
         <tli id="T466" time="223.91099999999983" type="appl" />
         <tli id="T467" time="224.529" type="appl" />
         <tli id="T468" time="225.14699999999993" type="appl" />
         <tli id="T469" time="225.76499999999987" type="appl" />
         <tli id="T470" time="226.3829999999998" type="appl" />
         <tli id="T471" time="227.00099999999998" type="appl" />
         <tli id="T472" time="227.61899999999991" type="appl" />
         <tli id="T473" time="228.23699999999985" type="appl" />
         <tli id="T474" time="228.85500000000002" type="appl" />
         <tli id="T475" time="229.47299999999996" type="appl" />
         <tli id="T476" time="230.34899999999993" type="appl" />
         <tli id="T477" time="231.2249999999999" type="appl" />
         <tli id="T478" time="232.10199999999986" type="appl" />
         <tli id="T479" time="232.97799999999984" type="appl" />
         <tli id="T480" time="233.85399999999981" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KuNS"
                      id="tx-KuNS"
                      speaker="KuNS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KuNS">
            <ts e="T4" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">bagaraːččɨ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">etim</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">diːgin</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T54" id="Seg_17" n="sc" s="T53">
               <ts e="T54" id="Seg_19" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_21" n="HIAT:w" s="T53">Aldʼatalaːŋ</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T83" id="Seg_24" n="sc" s="T80">
               <ts e="T83" id="Seg_26" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_28" n="HIAT:w" s="T80">Patʼefonnaːk</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_31" n="HIAT:w" s="T81">etiŋ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_34" n="HIAT:w" s="T82">du͡o</ts>
                  <nts id="Seg_35" n="HIAT:ip">?</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T93" id="Seg_37" n="sc" s="T88">
               <ts e="T93" id="Seg_39" n="HIAT:u" s="T88">
                  <ts e="T90" id="Seg_41" n="HIAT:w" s="T88">Patʼefonuŋ</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_44" n="HIAT:w" s="T90">kantan</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_47" n="HIAT:w" s="T91">kelbitej</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_50" n="HIAT:w" s="T92">eni͡eke</ts>
                  <nts id="Seg_51" n="HIAT:ip">?</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T135" id="Seg_53" n="sc" s="T132">
               <ts e="T135" id="Seg_55" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_57" n="HIAT:w" s="T132">Lʼidʼija</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_60" n="HIAT:w" s="T133">Ruslanava</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_63" n="HIAT:w" s="T134">ɨllɨːr</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_66" n="sc" s="T168">
               <ts e="T172" id="Seg_68" n="HIAT:u" s="T168">
                  <ts e="T170" id="Seg_70" n="HIAT:w" s="T168">Iti</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_73" n="HIAT:w" s="T170">Ruslaːnava</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_76" n="HIAT:w" s="T171">gi͡etterin</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T186" id="Seg_79" n="sc" s="T181">
               <ts e="T186" id="Seg_81" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_83" n="HIAT:w" s="T181">Ol</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_85" n="HIAT:ip">(</nts>
                  <nts id="Seg_86" n="HIAT:ip">(</nts>
                  <ats e="T183" id="Seg_87" n="HIAT:non-pho" s="T182">…</ats>
                  <nts id="Seg_88" n="HIAT:ip">)</nts>
                  <nts id="Seg_89" n="HIAT:ip">)</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_92" n="HIAT:w" s="T183">kanna</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_95" n="HIAT:w" s="T184">baːllar</ts>
                  <nts id="Seg_96" n="HIAT:ip">,</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_99" n="HIAT:w" s="T185">baːllar</ts>
                  <nts id="Seg_100" n="HIAT:ip">?</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T215" id="Seg_102" n="sc" s="T202">
               <ts e="T215" id="Seg_104" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_106" n="HIAT:w" s="T202">Iti</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_109" n="HIAT:w" s="T203">kɨtaːnak</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_112" n="HIAT:w" s="T204">kem</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_115" n="HIAT:w" s="T205">ete</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_118" n="HIAT:w" s="T206">taːk</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_121" n="HIAT:w" s="T207">ta</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_124" n="HIAT:w" s="T208">bu</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_127" n="HIAT:w" s="T209">ke</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_130" n="HIAT:w" s="T210">olokko</ts>
                  <nts id="Seg_131" n="HIAT:ip">,</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_134" n="HIAT:w" s="T211">iti</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_137" n="HIAT:w" s="T212">Stalʼin</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_140" n="HIAT:w" s="T213">kemiger</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_143" n="HIAT:w" s="T214">eː</ts>
                  <nts id="Seg_144" n="HIAT:ip">?</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T293" id="Seg_146" n="sc" s="T281">
               <ts e="T293" id="Seg_148" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_150" n="HIAT:w" s="T281">Onuga</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_153" n="HIAT:w" s="T282">tu͡ok</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_156" n="HIAT:w" s="T283">ihin</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_159" n="HIAT:w" s="T284">kaːjaːččɨ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_162" n="HIAT:w" s="T285">etilere</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_165" n="HIAT:ip">"</nts>
                  <ts e="T287" id="Seg_167" n="HIAT:w" s="T286">iti</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_170" n="HIAT:w" s="T287">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_173" n="HIAT:w" s="T288">ihilleːmeŋ</ts>
                  <nts id="Seg_174" n="HIAT:ip">"</nts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_178" n="HIAT:w" s="T289">di͡ečči</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_181" n="HIAT:w" s="T290">etilere</ts>
                  <nts id="Seg_182" n="HIAT:ip">,</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_185" n="HIAT:w" s="T291">onuga</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_188" n="HIAT:w" s="T292">tuːgu</ts>
                  <nts id="Seg_189" n="HIAT:ip">?</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T329" id="Seg_191" n="sc" s="T319">
               <ts e="T329" id="Seg_193" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_195" n="HIAT:w" s="T319">Haŋagɨt</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_198" n="HIAT:w" s="T320">itte</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_201" n="HIAT:w" s="T321">ihin</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_204" n="HIAT:w" s="T322">eni</ts>
                  <nts id="Seg_205" n="HIAT:ip">,</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_208" n="HIAT:w" s="T323">iste</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_211" n="HIAT:w" s="T324">hɨldʼallara</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_214" n="HIAT:w" s="T325">bu͡ol</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_218" n="HIAT:w" s="T326">tu͡ok</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_221" n="HIAT:w" s="T327">di͡en</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_224" n="HIAT:w" s="T328">kepsetergitin</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T347" id="Seg_227" n="sc" s="T344">
               <ts e="T347" id="Seg_229" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_231" n="HIAT:w" s="T344">Anɨ</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_234" n="HIAT:w" s="T345">öjdöːtökkö</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_237" n="HIAT:w" s="T346">onton</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T356" id="Seg_240" n="sc" s="T349">
               <ts e="T356" id="Seg_242" n="HIAT:u" s="T349">
                  <ts e="T351" id="Seg_244" n="HIAT:w" s="T349">Oččogo</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_247" n="HIAT:w" s="T351">ba</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_250" n="HIAT:w" s="T352">kihi</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_253" n="HIAT:w" s="T353">kuttanɨ͡ak</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_256" n="HIAT:w" s="T354">ete</ts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_260" n="HIAT:w" s="T355">eː</ts>
                  <nts id="Seg_261" n="HIAT:ip">?</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T371" id="Seg_263" n="sc" s="T365">
               <ts e="T371" id="Seg_265" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_267" n="HIAT:w" s="T365">Horok</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_270" n="HIAT:w" s="T366">kenne</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_273" n="HIAT:w" s="T367">haːlaːk</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_276" n="HIAT:w" s="T368">daː</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_279" n="HIAT:w" s="T369">keli͡ektere</ts>
                  <nts id="Seg_280" n="HIAT:ip">,</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_283" n="HIAT:w" s="T370">eː</ts>
                  <nts id="Seg_284" n="HIAT:ip">?</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T408" id="Seg_286" n="sc" s="T403">
               <ts e="T408" id="Seg_288" n="HIAT:u" s="T403">
                  <ts e="T405" id="Seg_290" n="HIAT:w" s="T403">Dogoruŋ</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_293" n="HIAT:w" s="T405">aːta</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_296" n="HIAT:w" s="T406">kim</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_299" n="HIAT:w" s="T407">etej</ts>
                  <nts id="Seg_300" n="HIAT:ip">?</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T430" id="Seg_302" n="sc" s="T421">
               <ts e="T430" id="Seg_304" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_306" n="HIAT:w" s="T421">Ol</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_309" n="HIAT:w" s="T422">kihileriŋ</ts>
                  <nts id="Seg_310" n="HIAT:ip">,</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_313" n="HIAT:w" s="T423">diːgin</ts>
                  <nts id="Seg_314" n="HIAT:ip">,</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_317" n="HIAT:w" s="T424">kamsamollarɨŋ</ts>
                  <nts id="Seg_318" n="HIAT:ip">,</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_321" n="HIAT:w" s="T425">iti</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_324" n="HIAT:w" s="T426">bejegit</ts>
                  <nts id="Seg_325" n="HIAT:ip">,</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_328" n="HIAT:w" s="T427">haka</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_331" n="HIAT:w" s="T428">kihilergit</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_334" n="HIAT:w" s="T429">du͡o</ts>
                  <nts id="Seg_335" n="HIAT:ip">?</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T459" id="Seg_337" n="sc" s="T448">
               <ts e="T459" id="Seg_339" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_341" n="HIAT:w" s="T448">Bejeŋ</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_344" n="HIAT:w" s="T449">ke</ts>
                  <nts id="Seg_345" n="HIAT:ip">,</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_348" n="HIAT:w" s="T450">olokkun</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_351" n="HIAT:w" s="T451">kördökkö</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_355" n="HIAT:w" s="T452">en</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_358" n="HIAT:w" s="T453">bagas</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_361" n="HIAT:w" s="T454">bejeŋ</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_364" n="HIAT:w" s="T455">kajdak</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_367" n="HIAT:w" s="T456">hanaːgɨnan</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_370" n="HIAT:w" s="T457">olorbutuŋ</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_373" n="HIAT:w" s="T458">ol</ts>
                  <nts id="Seg_374" n="HIAT:ip">?</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KuNS">
            <ts e="T4" id="Seg_376" n="sc" s="T0">
               <ts e="T1" id="Seg_378" n="e" s="T0">ɨrɨ͡alarɨ </ts>
               <ts e="T2" id="Seg_380" n="e" s="T1">bagaraːččɨ </ts>
               <ts e="T3" id="Seg_382" n="e" s="T2">etim, </ts>
               <ts e="T4" id="Seg_384" n="e" s="T3">diːgin. </ts>
            </ts>
            <ts e="T54" id="Seg_385" n="sc" s="T53">
               <ts e="T54" id="Seg_387" n="e" s="T53">Aldʼatalaːŋ. </ts>
            </ts>
            <ts e="T83" id="Seg_388" n="sc" s="T80">
               <ts e="T81" id="Seg_390" n="e" s="T80">Patʼefonnaːk </ts>
               <ts e="T82" id="Seg_392" n="e" s="T81">etiŋ </ts>
               <ts e="T83" id="Seg_394" n="e" s="T82">du͡o? </ts>
            </ts>
            <ts e="T93" id="Seg_395" n="sc" s="T88">
               <ts e="T90" id="Seg_397" n="e" s="T88">Patʼefonuŋ </ts>
               <ts e="T91" id="Seg_399" n="e" s="T90">kantan </ts>
               <ts e="T92" id="Seg_401" n="e" s="T91">kelbitej </ts>
               <ts e="T93" id="Seg_403" n="e" s="T92">eni͡eke? </ts>
            </ts>
            <ts e="T135" id="Seg_404" n="sc" s="T132">
               <ts e="T133" id="Seg_406" n="e" s="T132">Lʼidʼija </ts>
               <ts e="T134" id="Seg_408" n="e" s="T133">Ruslanava </ts>
               <ts e="T135" id="Seg_410" n="e" s="T134">ɨllɨːr. </ts>
            </ts>
            <ts e="T172" id="Seg_411" n="sc" s="T168">
               <ts e="T170" id="Seg_413" n="e" s="T168">Iti </ts>
               <ts e="T171" id="Seg_415" n="e" s="T170">Ruslaːnava </ts>
               <ts e="T172" id="Seg_417" n="e" s="T171">gi͡etterin. </ts>
            </ts>
            <ts e="T186" id="Seg_418" n="sc" s="T181">
               <ts e="T182" id="Seg_420" n="e" s="T181">Ol </ts>
               <ts e="T183" id="Seg_422" n="e" s="T182">((…)) </ts>
               <ts e="T184" id="Seg_424" n="e" s="T183">kanna </ts>
               <ts e="T185" id="Seg_426" n="e" s="T184">baːllar, </ts>
               <ts e="T186" id="Seg_428" n="e" s="T185">baːllar? </ts>
            </ts>
            <ts e="T215" id="Seg_429" n="sc" s="T202">
               <ts e="T203" id="Seg_431" n="e" s="T202">Iti </ts>
               <ts e="T204" id="Seg_433" n="e" s="T203">kɨtaːnak </ts>
               <ts e="T205" id="Seg_435" n="e" s="T204">kem </ts>
               <ts e="T206" id="Seg_437" n="e" s="T205">ete </ts>
               <ts e="T207" id="Seg_439" n="e" s="T206">taːk </ts>
               <ts e="T208" id="Seg_441" n="e" s="T207">ta </ts>
               <ts e="T209" id="Seg_443" n="e" s="T208">bu </ts>
               <ts e="T210" id="Seg_445" n="e" s="T209">ke </ts>
               <ts e="T211" id="Seg_447" n="e" s="T210">olokko, </ts>
               <ts e="T212" id="Seg_449" n="e" s="T211">iti </ts>
               <ts e="T213" id="Seg_451" n="e" s="T212">Stalʼin </ts>
               <ts e="T214" id="Seg_453" n="e" s="T213">kemiger </ts>
               <ts e="T215" id="Seg_455" n="e" s="T214">eː? </ts>
            </ts>
            <ts e="T293" id="Seg_456" n="sc" s="T281">
               <ts e="T282" id="Seg_458" n="e" s="T281">Onuga </ts>
               <ts e="T283" id="Seg_460" n="e" s="T282">tu͡ok </ts>
               <ts e="T284" id="Seg_462" n="e" s="T283">ihin </ts>
               <ts e="T285" id="Seg_464" n="e" s="T284">kaːjaːččɨ </ts>
               <ts e="T286" id="Seg_466" n="e" s="T285">etilere, </ts>
               <ts e="T287" id="Seg_468" n="e" s="T286">"iti </ts>
               <ts e="T288" id="Seg_470" n="e" s="T287">ɨrɨ͡alarɨ </ts>
               <ts e="T289" id="Seg_472" n="e" s="T288">ihilleːmeŋ", </ts>
               <ts e="T290" id="Seg_474" n="e" s="T289">di͡ečči </ts>
               <ts e="T291" id="Seg_476" n="e" s="T290">etilere, </ts>
               <ts e="T292" id="Seg_478" n="e" s="T291">onuga </ts>
               <ts e="T293" id="Seg_480" n="e" s="T292">tuːgu? </ts>
            </ts>
            <ts e="T329" id="Seg_481" n="sc" s="T319">
               <ts e="T320" id="Seg_483" n="e" s="T319">Haŋagɨt </ts>
               <ts e="T321" id="Seg_485" n="e" s="T320">itte </ts>
               <ts e="T322" id="Seg_487" n="e" s="T321">ihin </ts>
               <ts e="T323" id="Seg_489" n="e" s="T322">eni, </ts>
               <ts e="T324" id="Seg_491" n="e" s="T323">iste </ts>
               <ts e="T325" id="Seg_493" n="e" s="T324">hɨldʼallara </ts>
               <ts e="T326" id="Seg_495" n="e" s="T325">bu͡ol, </ts>
               <ts e="T327" id="Seg_497" n="e" s="T326">tu͡ok </ts>
               <ts e="T328" id="Seg_499" n="e" s="T327">di͡en </ts>
               <ts e="T329" id="Seg_501" n="e" s="T328">kepsetergitin. </ts>
            </ts>
            <ts e="T347" id="Seg_502" n="sc" s="T344">
               <ts e="T345" id="Seg_504" n="e" s="T344">Anɨ </ts>
               <ts e="T346" id="Seg_506" n="e" s="T345">öjdöːtökkö </ts>
               <ts e="T347" id="Seg_508" n="e" s="T346">onton. </ts>
            </ts>
            <ts e="T356" id="Seg_509" n="sc" s="T349">
               <ts e="T351" id="Seg_511" n="e" s="T349">Oččogo </ts>
               <ts e="T352" id="Seg_513" n="e" s="T351">ba </ts>
               <ts e="T353" id="Seg_515" n="e" s="T352">kihi </ts>
               <ts e="T354" id="Seg_517" n="e" s="T353">kuttanɨ͡ak </ts>
               <ts e="T355" id="Seg_519" n="e" s="T354">ete, </ts>
               <ts e="T356" id="Seg_521" n="e" s="T355">eː? </ts>
            </ts>
            <ts e="T371" id="Seg_522" n="sc" s="T365">
               <ts e="T366" id="Seg_524" n="e" s="T365">Horok </ts>
               <ts e="T367" id="Seg_526" n="e" s="T366">kenne </ts>
               <ts e="T368" id="Seg_528" n="e" s="T367">haːlaːk </ts>
               <ts e="T369" id="Seg_530" n="e" s="T368">daː </ts>
               <ts e="T370" id="Seg_532" n="e" s="T369">keli͡ektere, </ts>
               <ts e="T371" id="Seg_534" n="e" s="T370">eː? </ts>
            </ts>
            <ts e="T408" id="Seg_535" n="sc" s="T403">
               <ts e="T405" id="Seg_537" n="e" s="T403">Dogoruŋ </ts>
               <ts e="T406" id="Seg_539" n="e" s="T405">aːta </ts>
               <ts e="T407" id="Seg_541" n="e" s="T406">kim </ts>
               <ts e="T408" id="Seg_543" n="e" s="T407">etej? </ts>
            </ts>
            <ts e="T430" id="Seg_544" n="sc" s="T421">
               <ts e="T422" id="Seg_546" n="e" s="T421">Ol </ts>
               <ts e="T423" id="Seg_548" n="e" s="T422">kihileriŋ, </ts>
               <ts e="T424" id="Seg_550" n="e" s="T423">diːgin, </ts>
               <ts e="T425" id="Seg_552" n="e" s="T424">kamsamollarɨŋ, </ts>
               <ts e="T426" id="Seg_554" n="e" s="T425">iti </ts>
               <ts e="T427" id="Seg_556" n="e" s="T426">bejegit, </ts>
               <ts e="T428" id="Seg_558" n="e" s="T427">haka </ts>
               <ts e="T429" id="Seg_560" n="e" s="T428">kihilergit </ts>
               <ts e="T430" id="Seg_562" n="e" s="T429">du͡o? </ts>
            </ts>
            <ts e="T459" id="Seg_563" n="sc" s="T448">
               <ts e="T449" id="Seg_565" n="e" s="T448">Bejeŋ </ts>
               <ts e="T450" id="Seg_567" n="e" s="T449">ke, </ts>
               <ts e="T451" id="Seg_569" n="e" s="T450">olokkun </ts>
               <ts e="T452" id="Seg_571" n="e" s="T451">kördökkö, </ts>
               <ts e="T453" id="Seg_573" n="e" s="T452">en </ts>
               <ts e="T454" id="Seg_575" n="e" s="T453">bagas </ts>
               <ts e="T455" id="Seg_577" n="e" s="T454">bejeŋ </ts>
               <ts e="T456" id="Seg_579" n="e" s="T455">kajdak </ts>
               <ts e="T457" id="Seg_581" n="e" s="T456">hanaːgɨnan </ts>
               <ts e="T458" id="Seg_583" n="e" s="T457">olorbutuŋ </ts>
               <ts e="T459" id="Seg_585" n="e" s="T458">ol? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KuNS">
            <ta e="T4" id="Seg_586" s="T0">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.001 (001.001)</ta>
            <ta e="T54" id="Seg_587" s="T53">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.002 (001.010)</ta>
            <ta e="T83" id="Seg_588" s="T80">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.003 (001.016)</ta>
            <ta e="T93" id="Seg_589" s="T88">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.004 (001.018)</ta>
            <ta e="T135" id="Seg_590" s="T132">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.005 (001.025)</ta>
            <ta e="T172" id="Seg_591" s="T168">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.006 (001.033)</ta>
            <ta e="T186" id="Seg_592" s="T181">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.007 (001.036)</ta>
            <ta e="T215" id="Seg_593" s="T202">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.008 (001.039)</ta>
            <ta e="T293" id="Seg_594" s="T281">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.009 (001.048)</ta>
            <ta e="T329" id="Seg_595" s="T319">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.010 (001.050)</ta>
            <ta e="T347" id="Seg_596" s="T344">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.011 (001.052)</ta>
            <ta e="T356" id="Seg_597" s="T349">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.012 (001.054)</ta>
            <ta e="T371" id="Seg_598" s="T365">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.013 (001.056)</ta>
            <ta e="T408" id="Seg_599" s="T403">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.014 (001.063)</ta>
            <ta e="T430" id="Seg_600" s="T421">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.015 (001.066)</ta>
            <ta e="T459" id="Seg_601" s="T448">LaVN_KuNS_1999_MusicRepressions_conv.KuNS.016 (001.070)</ta>
         </annotation>
         <annotation name="st" tierref="st-KuNS">
            <ta e="T4" id="Seg_602" s="T0">НК: Ырыалары багарааччы этим, диигин.</ta>
            <ta e="T54" id="Seg_603" s="T53">НК: Алдьаталааӈ.</ta>
            <ta e="T83" id="Seg_604" s="T80">НК: Патефоннаак этиӈ дуо?</ta>
            <ta e="T93" id="Seg_605" s="T88">НК: Патефонуӈ кантан кэлбитэй эниэкэ?</ta>
            <ta e="T135" id="Seg_606" s="T132">НК: Лидия Русланова ыллыыр.</ta>
            <ta e="T172" id="Seg_607" s="T168">НК: Ити Русланова гиэттэрин.</ta>
            <ta e="T215" id="Seg_608" s="T202">НК: Ити кытаанак кэм этэ таак-то бу киэ олокко, ити Сталин кэмигэр э-э?</ta>
            <ta e="T293" id="Seg_609" s="T281">НК: Онуга туок иһин каайаччы этилэрэ, ити ырыалары иһиллээбэӈ, диэччи этилэрэ, онуга туугу?</ta>
            <ta e="T329" id="Seg_610" s="T319">НК: Һаӈагыт иттэ иһин эни, истэ һылдьаллара буол, туок диэн кэпсэтэргитин. </ta>
            <ta e="T347" id="Seg_611" s="T344">НК: Аны өйдөөтөккө онтон.</ta>
            <ta e="T356" id="Seg_612" s="T349">НК: Оччого ба киһи куттаныак этэ, э-э?</ta>
            <ta e="T371" id="Seg_613" s="T365">НК: Һорок кэннэ һаалак даа кэлиэктэрэ, э-э?</ta>
            <ta e="T408" id="Seg_614" s="T403">НК: Догоруӈ аата ким этэй?</ta>
            <ta e="T430" id="Seg_615" s="T421">НК: Ол киһилэриӈ, диигин, комсомолларыӈ, ити бэйэгит, һака киһилэргит дуо?</ta>
            <ta e="T459" id="Seg_616" s="T448">НК: Бэйэӈ киэ, олоккун көрдөккө, эн багас бэйэӈ кайдак һанаагынан олорбутуӈ ол?</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KuNS">
            <ta e="T4" id="Seg_617" s="T0">– ɨrɨ͡alarɨ bagaraːččɨ etim, diːgin. </ta>
            <ta e="T54" id="Seg_618" s="T53">– Aldʼatalaːŋ. </ta>
            <ta e="T83" id="Seg_619" s="T80">– Patʼefonnaːk etiŋ du͡o? </ta>
            <ta e="T93" id="Seg_620" s="T88">– Patʼefonuŋ kantan kelbitej eni͡eke? </ta>
            <ta e="T135" id="Seg_621" s="T132">– Lʼidʼija Ruslanava ɨllɨːr. </ta>
            <ta e="T172" id="Seg_622" s="T168">– Iti Ruslaːnava gi͡etterin. </ta>
            <ta e="T186" id="Seg_623" s="T181">Ol ((…)) kanna baːllar, baːllar?</ta>
            <ta e="T215" id="Seg_624" s="T202">– Iti kɨtaːnak kem ete taːk ta bu ke olokko, iti Stalʼin kemiger eː? </ta>
            <ta e="T293" id="Seg_625" s="T281">– Onuga tu͡ok ihin kaːjaːččɨ etilere, "iti ɨrɨ͡alarɨ ihilleːmeŋ", di͡ečči etilere, onuga tuːgu? </ta>
            <ta e="T329" id="Seg_626" s="T319">– Haŋagɨt itte ihin eni, iste hɨldʼallara bu͡ol, tu͡ok di͡en kepsetergitin. </ta>
            <ta e="T347" id="Seg_627" s="T344">– Anɨ öjdöːtökkö onton. </ta>
            <ta e="T356" id="Seg_628" s="T349">– Oččogo ba kihi kuttanɨ͡ak ete, eː? </ta>
            <ta e="T371" id="Seg_629" s="T365">– Horok kenne haːlaːk daː keli͡ektere, eː? </ta>
            <ta e="T408" id="Seg_630" s="T403">– Dogoruŋ aːta kim etej? </ta>
            <ta e="T430" id="Seg_631" s="T421">– Ol kihileriŋ, diːgin, kamsamollarɨŋ, iti bejegit, haka kihilergit du͡o? </ta>
            <ta e="T459" id="Seg_632" s="T448">– Bejeŋ ke, olokkun kördökkö, en bagas bejeŋ kajdak hanaːgɨnan olorbutuŋ ol? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KuNS">
            <ta e="T1" id="Seg_633" s="T0">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T2" id="Seg_634" s="T1">bagar-aːččɨ</ta>
            <ta e="T3" id="Seg_635" s="T2">e-ti-m</ta>
            <ta e="T4" id="Seg_636" s="T3">d-iː-gin</ta>
            <ta e="T54" id="Seg_637" s="T53">aldʼat-alaː-ŋ</ta>
            <ta e="T81" id="Seg_638" s="T80">patʼefon-naːk</ta>
            <ta e="T82" id="Seg_639" s="T81">e-ti-ŋ</ta>
            <ta e="T83" id="Seg_640" s="T82">du͡o</ta>
            <ta e="T90" id="Seg_641" s="T88">patʼefon-u-ŋ</ta>
            <ta e="T91" id="Seg_642" s="T90">kantan</ta>
            <ta e="T92" id="Seg_643" s="T91">kel-bit-e=j</ta>
            <ta e="T93" id="Seg_644" s="T92">eni͡e-ke</ta>
            <ta e="T133" id="Seg_645" s="T132">Lʼidʼija</ta>
            <ta e="T134" id="Seg_646" s="T133">Ruslanava</ta>
            <ta e="T135" id="Seg_647" s="T134">ɨllɨː-r</ta>
            <ta e="T170" id="Seg_648" s="T168">iti</ta>
            <ta e="T171" id="Seg_649" s="T170">Ruslaːnava</ta>
            <ta e="T172" id="Seg_650" s="T171">gi͡et-teri-n</ta>
            <ta e="T182" id="Seg_651" s="T181">ol</ta>
            <ta e="T184" id="Seg_652" s="T183">kanna</ta>
            <ta e="T185" id="Seg_653" s="T184">baːl-lar</ta>
            <ta e="T186" id="Seg_654" s="T185">baːl-lar</ta>
            <ta e="T203" id="Seg_655" s="T202">iti</ta>
            <ta e="T204" id="Seg_656" s="T203">kɨtaːnak</ta>
            <ta e="T205" id="Seg_657" s="T204">kem</ta>
            <ta e="T206" id="Seg_658" s="T205">e-t-e</ta>
            <ta e="T207" id="Seg_659" s="T206">taːk</ta>
            <ta e="T208" id="Seg_660" s="T207">ta</ta>
            <ta e="T209" id="Seg_661" s="T208">bu</ta>
            <ta e="T210" id="Seg_662" s="T209">ke</ta>
            <ta e="T211" id="Seg_663" s="T210">olok-ko</ta>
            <ta e="T212" id="Seg_664" s="T211">iti</ta>
            <ta e="T213" id="Seg_665" s="T212">Stalʼin</ta>
            <ta e="T214" id="Seg_666" s="T213">kem-i-ger</ta>
            <ta e="T215" id="Seg_667" s="T214">eː</ta>
            <ta e="T282" id="Seg_668" s="T281">onu-ga</ta>
            <ta e="T283" id="Seg_669" s="T282">tu͡ok</ta>
            <ta e="T284" id="Seg_670" s="T283">ihin</ta>
            <ta e="T285" id="Seg_671" s="T284">kaːj-aːččɨ</ta>
            <ta e="T286" id="Seg_672" s="T285">e-ti-lere</ta>
            <ta e="T287" id="Seg_673" s="T286">iti</ta>
            <ta e="T288" id="Seg_674" s="T287">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T289" id="Seg_675" s="T288">ihilleː-me-ŋ</ta>
            <ta e="T290" id="Seg_676" s="T289">di͡e-čči</ta>
            <ta e="T291" id="Seg_677" s="T290">e-ti-lere</ta>
            <ta e="T292" id="Seg_678" s="T291">onu-ga</ta>
            <ta e="T293" id="Seg_679" s="T292">tuːg-u</ta>
            <ta e="T320" id="Seg_680" s="T319">haŋa-gɨt</ta>
            <ta e="T321" id="Seg_681" s="T320">itte</ta>
            <ta e="T322" id="Seg_682" s="T321">ihin</ta>
            <ta e="T323" id="Seg_683" s="T322">eni</ta>
            <ta e="T324" id="Seg_684" s="T323">ist-e</ta>
            <ta e="T325" id="Seg_685" s="T324">hɨldʼ-al-lara</ta>
            <ta e="T326" id="Seg_686" s="T325">bu͡ol</ta>
            <ta e="T327" id="Seg_687" s="T326">tu͡ok</ta>
            <ta e="T328" id="Seg_688" s="T327">di͡e-n</ta>
            <ta e="T329" id="Seg_689" s="T328">kepset-er-giti-n</ta>
            <ta e="T345" id="Seg_690" s="T344">anɨ</ta>
            <ta e="T346" id="Seg_691" s="T345">öjdöː-tök-kö</ta>
            <ta e="T347" id="Seg_692" s="T346">onton</ta>
            <ta e="T351" id="Seg_693" s="T349">oččogo</ta>
            <ta e="T352" id="Seg_694" s="T351">ba</ta>
            <ta e="T353" id="Seg_695" s="T352">kihi</ta>
            <ta e="T354" id="Seg_696" s="T353">kuttan-ɨ͡ak</ta>
            <ta e="T355" id="Seg_697" s="T354">e-t-e</ta>
            <ta e="T356" id="Seg_698" s="T355">eː</ta>
            <ta e="T366" id="Seg_699" s="T365">horok</ta>
            <ta e="T367" id="Seg_700" s="T366">kenne</ta>
            <ta e="T368" id="Seg_701" s="T367">haː-laːk</ta>
            <ta e="T369" id="Seg_702" s="T368">daː</ta>
            <ta e="T370" id="Seg_703" s="T369">kel-i͡ek-tere</ta>
            <ta e="T371" id="Seg_704" s="T370">eː</ta>
            <ta e="T405" id="Seg_705" s="T403">dogor-u-ŋ</ta>
            <ta e="T406" id="Seg_706" s="T405">aːt-a</ta>
            <ta e="T407" id="Seg_707" s="T406">kim</ta>
            <ta e="T408" id="Seg_708" s="T407">e-t-e=j</ta>
            <ta e="T422" id="Seg_709" s="T421">ol</ta>
            <ta e="T423" id="Seg_710" s="T422">kihi-ler-i-ŋ</ta>
            <ta e="T424" id="Seg_711" s="T423">d-iː-gin</ta>
            <ta e="T425" id="Seg_712" s="T424">kamsamol-lar-ɨ-ŋ</ta>
            <ta e="T426" id="Seg_713" s="T425">iti</ta>
            <ta e="T427" id="Seg_714" s="T426">beje-git</ta>
            <ta e="T428" id="Seg_715" s="T427">haka</ta>
            <ta e="T429" id="Seg_716" s="T428">kihi-ler-git</ta>
            <ta e="T430" id="Seg_717" s="T429">du͡o</ta>
            <ta e="T449" id="Seg_718" s="T448">beje-ŋ</ta>
            <ta e="T450" id="Seg_719" s="T449">ke</ta>
            <ta e="T451" id="Seg_720" s="T450">olok-ku-n</ta>
            <ta e="T452" id="Seg_721" s="T451">kör-dök-kö</ta>
            <ta e="T453" id="Seg_722" s="T452">en</ta>
            <ta e="T454" id="Seg_723" s="T453">bagas</ta>
            <ta e="T455" id="Seg_724" s="T454">beje-ŋ</ta>
            <ta e="T456" id="Seg_725" s="T455">kajdak</ta>
            <ta e="T457" id="Seg_726" s="T456">hanaː-gɨ-nan</ta>
            <ta e="T458" id="Seg_727" s="T457">olor-but-u-ŋ</ta>
            <ta e="T459" id="Seg_728" s="T458">ol</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KuNS">
            <ta e="T1" id="Seg_729" s="T0">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T2" id="Seg_730" s="T1">bagar-AːččI</ta>
            <ta e="T3" id="Seg_731" s="T2">e-TI-m</ta>
            <ta e="T4" id="Seg_732" s="T3">di͡e-A-GIn</ta>
            <ta e="T54" id="Seg_733" s="T53">aldʼat-AlAː-ŋ</ta>
            <ta e="T81" id="Seg_734" s="T80">patʼefon-LAːK</ta>
            <ta e="T82" id="Seg_735" s="T81">e-TI-ŋ</ta>
            <ta e="T83" id="Seg_736" s="T82">du͡o</ta>
            <ta e="T90" id="Seg_737" s="T88">patʼefon-I-ŋ</ta>
            <ta e="T91" id="Seg_738" s="T90">kantan</ta>
            <ta e="T92" id="Seg_739" s="T91">kel-BIT-tA=Ij</ta>
            <ta e="T93" id="Seg_740" s="T92">en-GA</ta>
            <ta e="T133" id="Seg_741" s="T132">Lʼidʼija</ta>
            <ta e="T134" id="Seg_742" s="T133">Ruslanava</ta>
            <ta e="T135" id="Seg_743" s="T134">ɨllaː-Ar</ta>
            <ta e="T170" id="Seg_744" s="T168">iti</ta>
            <ta e="T171" id="Seg_745" s="T170">Ruslanava</ta>
            <ta e="T172" id="Seg_746" s="T171">gi͡en-LArI-n</ta>
            <ta e="T182" id="Seg_747" s="T181">ol</ta>
            <ta e="T184" id="Seg_748" s="T183">kanna</ta>
            <ta e="T185" id="Seg_749" s="T184">baːr-LAr</ta>
            <ta e="T186" id="Seg_750" s="T185">baːr-LAr</ta>
            <ta e="T203" id="Seg_751" s="T202">iti</ta>
            <ta e="T204" id="Seg_752" s="T203">kɨtaːnak</ta>
            <ta e="T205" id="Seg_753" s="T204">kem</ta>
            <ta e="T206" id="Seg_754" s="T205">e-TI-tA</ta>
            <ta e="T207" id="Seg_755" s="T206">taːk</ta>
            <ta e="T208" id="Seg_756" s="T207">ta</ta>
            <ta e="T209" id="Seg_757" s="T208">bu</ta>
            <ta e="T210" id="Seg_758" s="T209">ka</ta>
            <ta e="T211" id="Seg_759" s="T210">olok-GA</ta>
            <ta e="T212" id="Seg_760" s="T211">iti</ta>
            <ta e="T213" id="Seg_761" s="T212">Stalʼin</ta>
            <ta e="T214" id="Seg_762" s="T213">kem-tI-GAr</ta>
            <ta e="T215" id="Seg_763" s="T214">eː</ta>
            <ta e="T282" id="Seg_764" s="T281">ol-GA</ta>
            <ta e="T283" id="Seg_765" s="T282">tu͡ok</ta>
            <ta e="T284" id="Seg_766" s="T283">ihin</ta>
            <ta e="T285" id="Seg_767" s="T284">kaːj-AːččI</ta>
            <ta e="T286" id="Seg_768" s="T285">e-TI-LArA</ta>
            <ta e="T287" id="Seg_769" s="T286">iti</ta>
            <ta e="T288" id="Seg_770" s="T287">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T289" id="Seg_771" s="T288">ihilleː-m-ŋ</ta>
            <ta e="T290" id="Seg_772" s="T289">di͡e-AːččI</ta>
            <ta e="T291" id="Seg_773" s="T290">e-TI-LArA</ta>
            <ta e="T292" id="Seg_774" s="T291">ol-GA</ta>
            <ta e="T293" id="Seg_775" s="T292">tu͡ok-nI</ta>
            <ta e="T320" id="Seg_776" s="T319">haŋa-GIt</ta>
            <ta e="T321" id="Seg_777" s="T320">itte</ta>
            <ta e="T322" id="Seg_778" s="T321">ihin</ta>
            <ta e="T323" id="Seg_779" s="T322">eni</ta>
            <ta e="T324" id="Seg_780" s="T323">ihit-A</ta>
            <ta e="T325" id="Seg_781" s="T324">hɨrɨt-Ar-LArA</ta>
            <ta e="T326" id="Seg_782" s="T325">bu͡ol</ta>
            <ta e="T327" id="Seg_783" s="T326">tu͡ok</ta>
            <ta e="T328" id="Seg_784" s="T327">di͡e-An</ta>
            <ta e="T329" id="Seg_785" s="T328">kepset-Ar-GItI-n</ta>
            <ta e="T345" id="Seg_786" s="T344">anɨ</ta>
            <ta e="T346" id="Seg_787" s="T345">öjdöː-TAK-GA</ta>
            <ta e="T347" id="Seg_788" s="T346">onton</ta>
            <ta e="T351" id="Seg_789" s="T349">oččogo</ta>
            <ta e="T352" id="Seg_790" s="T351">bu</ta>
            <ta e="T353" id="Seg_791" s="T352">kihi</ta>
            <ta e="T354" id="Seg_792" s="T353">kuttan-IAK</ta>
            <ta e="T355" id="Seg_793" s="T354">e-TI-tA</ta>
            <ta e="T356" id="Seg_794" s="T355">eː</ta>
            <ta e="T366" id="Seg_795" s="T365">horok</ta>
            <ta e="T367" id="Seg_796" s="T366">genne</ta>
            <ta e="T368" id="Seg_797" s="T367">haː-LAːK</ta>
            <ta e="T369" id="Seg_798" s="T368">da</ta>
            <ta e="T370" id="Seg_799" s="T369">kel-IAK-LArA</ta>
            <ta e="T371" id="Seg_800" s="T370">eː</ta>
            <ta e="T405" id="Seg_801" s="T403">dogor-I-ŋ</ta>
            <ta e="T406" id="Seg_802" s="T405">aːt-tA</ta>
            <ta e="T407" id="Seg_803" s="T406">kim</ta>
            <ta e="T408" id="Seg_804" s="T407">e-TI-tA=Ij</ta>
            <ta e="T422" id="Seg_805" s="T421">ol</ta>
            <ta e="T423" id="Seg_806" s="T422">kihi-LAr-I-ŋ</ta>
            <ta e="T424" id="Seg_807" s="T423">di͡e-A-GIn</ta>
            <ta e="T425" id="Seg_808" s="T424">kamsamol-LAr-I-ŋ</ta>
            <ta e="T426" id="Seg_809" s="T425">iti</ta>
            <ta e="T427" id="Seg_810" s="T426">beje-GIt</ta>
            <ta e="T428" id="Seg_811" s="T427">haka</ta>
            <ta e="T429" id="Seg_812" s="T428">kihi-LAr-GIt</ta>
            <ta e="T430" id="Seg_813" s="T429">du͡o</ta>
            <ta e="T449" id="Seg_814" s="T448">beje-ŋ</ta>
            <ta e="T450" id="Seg_815" s="T449">ka</ta>
            <ta e="T451" id="Seg_816" s="T450">olok-GI-n</ta>
            <ta e="T452" id="Seg_817" s="T451">kör-TAK-GA</ta>
            <ta e="T453" id="Seg_818" s="T452">en</ta>
            <ta e="T454" id="Seg_819" s="T453">bagas</ta>
            <ta e="T455" id="Seg_820" s="T454">beje-ŋ</ta>
            <ta e="T456" id="Seg_821" s="T455">kajdak</ta>
            <ta e="T457" id="Seg_822" s="T456">hanaː-GI-nAn</ta>
            <ta e="T458" id="Seg_823" s="T457">olor-BIT-I-ŋ</ta>
            <ta e="T459" id="Seg_824" s="T458">ol</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KuNS">
            <ta e="T1" id="Seg_825" s="T0">song-PL-ACC</ta>
            <ta e="T2" id="Seg_826" s="T1">love-PTCP.HAB</ta>
            <ta e="T3" id="Seg_827" s="T2">be-PST1-1SG</ta>
            <ta e="T4" id="Seg_828" s="T3">say-PRS-2SG</ta>
            <ta e="T54" id="Seg_829" s="T53">break-FREQ-IMP.2PL</ta>
            <ta e="T81" id="Seg_830" s="T80">gramophone-PROPR.[NOM]</ta>
            <ta e="T82" id="Seg_831" s="T81">be-PST1-2SG</ta>
            <ta e="T83" id="Seg_832" s="T82">Q</ta>
            <ta e="T90" id="Seg_833" s="T88">gramophone-EP-2SG.[NOM]</ta>
            <ta e="T91" id="Seg_834" s="T90">where.from</ta>
            <ta e="T92" id="Seg_835" s="T91">come-PST2-3SG=Q</ta>
            <ta e="T93" id="Seg_836" s="T92">2SG-DAT/LOC</ta>
            <ta e="T133" id="Seg_837" s="T132">Lidiya</ta>
            <ta e="T134" id="Seg_838" s="T133">Ruslanova.[NOM]</ta>
            <ta e="T135" id="Seg_839" s="T134">sing-PRS.[3SG]</ta>
            <ta e="T170" id="Seg_840" s="T168">that</ta>
            <ta e="T171" id="Seg_841" s="T170">Ruslanova.[NOM]</ta>
            <ta e="T172" id="Seg_842" s="T171">own-3PL-ACC</ta>
            <ta e="T182" id="Seg_843" s="T181">that.[NOM]</ta>
            <ta e="T184" id="Seg_844" s="T183">where</ta>
            <ta e="T185" id="Seg_845" s="T184">there.is-3PL</ta>
            <ta e="T186" id="Seg_846" s="T185">there.is-3PL</ta>
            <ta e="T203" id="Seg_847" s="T202">that.[NOM]</ta>
            <ta e="T204" id="Seg_848" s="T203">difficult</ta>
            <ta e="T205" id="Seg_849" s="T204">time.[NOM]</ta>
            <ta e="T206" id="Seg_850" s="T205">be-PST1-3SG</ta>
            <ta e="T207" id="Seg_851" s="T206">so</ta>
            <ta e="T208" id="Seg_852" s="T207">EMPH</ta>
            <ta e="T209" id="Seg_853" s="T208">this</ta>
            <ta e="T210" id="Seg_854" s="T209">well</ta>
            <ta e="T211" id="Seg_855" s="T210">life-DAT/LOC</ta>
            <ta e="T212" id="Seg_856" s="T211">that</ta>
            <ta e="T213" id="Seg_857" s="T212">Stalin.[NOM]</ta>
            <ta e="T214" id="Seg_858" s="T213">time-3SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_859" s="T214">AFFIRM</ta>
            <ta e="T282" id="Seg_860" s="T281">that-DAT/LOC</ta>
            <ta e="T283" id="Seg_861" s="T282">what.[NOM]</ta>
            <ta e="T284" id="Seg_862" s="T283">because.of</ta>
            <ta e="T285" id="Seg_863" s="T284">block-PTCP.HAB</ta>
            <ta e="T286" id="Seg_864" s="T285">be-PST1-3PL</ta>
            <ta e="T287" id="Seg_865" s="T286">that</ta>
            <ta e="T288" id="Seg_866" s="T287">song-PL-ACC</ta>
            <ta e="T289" id="Seg_867" s="T288">listen-NEG-IMP.2PL</ta>
            <ta e="T290" id="Seg_868" s="T289">say-PTCP.HAB</ta>
            <ta e="T291" id="Seg_869" s="T290">be-PST1-3PL</ta>
            <ta e="T292" id="Seg_870" s="T291">that-DAT/LOC</ta>
            <ta e="T293" id="Seg_871" s="T292">what-ACC</ta>
            <ta e="T320" id="Seg_872" s="T319">word-2PL.[NOM]</ta>
            <ta e="T321" id="Seg_873" s="T320">EMPH</ta>
            <ta e="T322" id="Seg_874" s="T321">because.of</ta>
            <ta e="T323" id="Seg_875" s="T322">apparently</ta>
            <ta e="T324" id="Seg_876" s="T323">hear-CVB.SIM</ta>
            <ta e="T325" id="Seg_877" s="T324">go-PRS-3PL</ta>
            <ta e="T326" id="Seg_878" s="T325">maybe</ta>
            <ta e="T327" id="Seg_879" s="T326">what.[NOM]</ta>
            <ta e="T328" id="Seg_880" s="T327">say-CVB.SEQ</ta>
            <ta e="T329" id="Seg_881" s="T328">chat-PTCP.PRS-2PL-ACC</ta>
            <ta e="T345" id="Seg_882" s="T344">now</ta>
            <ta e="T346" id="Seg_883" s="T345">remember-PTCP.COND-DAT/LOC</ta>
            <ta e="T347" id="Seg_884" s="T346">then</ta>
            <ta e="T351" id="Seg_885" s="T349">then</ta>
            <ta e="T352" id="Seg_886" s="T351">this</ta>
            <ta e="T353" id="Seg_887" s="T352">human.being.[NOM]</ta>
            <ta e="T354" id="Seg_888" s="T353">be.afraid-PTCP.FUT</ta>
            <ta e="T355" id="Seg_889" s="T354">be-PST1-3SG</ta>
            <ta e="T356" id="Seg_890" s="T355">AFFIRM</ta>
            <ta e="T366" id="Seg_891" s="T365">some</ta>
            <ta e="T367" id="Seg_892" s="T366">as</ta>
            <ta e="T368" id="Seg_893" s="T367">weapon-PROPR</ta>
            <ta e="T369" id="Seg_894" s="T368">and</ta>
            <ta e="T370" id="Seg_895" s="T369">come-FUT-3PL</ta>
            <ta e="T371" id="Seg_896" s="T370">AFFIRM</ta>
            <ta e="T405" id="Seg_897" s="T403">friend-EP-2SG.[NOM]</ta>
            <ta e="T406" id="Seg_898" s="T405">name-3SG.[NOM]</ta>
            <ta e="T407" id="Seg_899" s="T406">who.[NOM]</ta>
            <ta e="T408" id="Seg_900" s="T407">be-PST1-3SG=Q</ta>
            <ta e="T422" id="Seg_901" s="T421">that</ta>
            <ta e="T423" id="Seg_902" s="T422">human.being-PL-EP-2SG.[NOM]</ta>
            <ta e="T424" id="Seg_903" s="T423">say-PRS-2SG</ta>
            <ta e="T425" id="Seg_904" s="T424">komsomol-PL-EP-2SG.[NOM]</ta>
            <ta e="T426" id="Seg_905" s="T425">that.[NOM]</ta>
            <ta e="T427" id="Seg_906" s="T426">self-2PL.[NOM]</ta>
            <ta e="T428" id="Seg_907" s="T427">Dolgan</ta>
            <ta e="T429" id="Seg_908" s="T428">human.being-PL-2PL.[NOM]</ta>
            <ta e="T430" id="Seg_909" s="T429">Q</ta>
            <ta e="T449" id="Seg_910" s="T448">self-2SG.[NOM]</ta>
            <ta e="T450" id="Seg_911" s="T449">well</ta>
            <ta e="T451" id="Seg_912" s="T450">life-2SG-ACC</ta>
            <ta e="T452" id="Seg_913" s="T451">see-PTCP.COND-DAT/LOC</ta>
            <ta e="T453" id="Seg_914" s="T452">2SG.[NOM]</ta>
            <ta e="T454" id="Seg_915" s="T453">EMPH</ta>
            <ta e="T455" id="Seg_916" s="T454">self-2SG.[NOM]</ta>
            <ta e="T456" id="Seg_917" s="T455">how</ta>
            <ta e="T457" id="Seg_918" s="T456">thought-2SG-INSTR</ta>
            <ta e="T458" id="Seg_919" s="T457">live-PTCP.PST-EP-2SG.[NOM]</ta>
            <ta e="T459" id="Seg_920" s="T458">that</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KuNS">
            <ta e="T1" id="Seg_921" s="T0">Lied-PL-ACC</ta>
            <ta e="T2" id="Seg_922" s="T1">lieben-PTCP.HAB</ta>
            <ta e="T3" id="Seg_923" s="T2">sein-PST1-1SG</ta>
            <ta e="T4" id="Seg_924" s="T3">sagen-PRS-2SG</ta>
            <ta e="T54" id="Seg_925" s="T53">zerbrechen-FREQ-IMP.2PL</ta>
            <ta e="T81" id="Seg_926" s="T80">Plattenspieler-PROPR.[NOM]</ta>
            <ta e="T82" id="Seg_927" s="T81">sein-PST1-2SG</ta>
            <ta e="T83" id="Seg_928" s="T82">Q</ta>
            <ta e="T90" id="Seg_929" s="T88">Plattenspieler-EP-2SG.[NOM]</ta>
            <ta e="T91" id="Seg_930" s="T90">woher</ta>
            <ta e="T92" id="Seg_931" s="T91">kommen-PST2-3SG=Q</ta>
            <ta e="T93" id="Seg_932" s="T92">2SG-DAT/LOC</ta>
            <ta e="T133" id="Seg_933" s="T132">Lidija</ta>
            <ta e="T134" id="Seg_934" s="T133">Ruslanova.[NOM]</ta>
            <ta e="T135" id="Seg_935" s="T134">singen-PRS.[3SG]</ta>
            <ta e="T170" id="Seg_936" s="T168">dieses</ta>
            <ta e="T171" id="Seg_937" s="T170">Ruslanova.[NOM]</ta>
            <ta e="T172" id="Seg_938" s="T171">eigen-3PL-ACC</ta>
            <ta e="T182" id="Seg_939" s="T181">jener.[NOM]</ta>
            <ta e="T184" id="Seg_940" s="T183">wo</ta>
            <ta e="T185" id="Seg_941" s="T184">es.gibt-3PL</ta>
            <ta e="T186" id="Seg_942" s="T185">es.gibt-3PL</ta>
            <ta e="T203" id="Seg_943" s="T202">dieses.[NOM]</ta>
            <ta e="T204" id="Seg_944" s="T203">schwer</ta>
            <ta e="T205" id="Seg_945" s="T204">Zeit.[NOM]</ta>
            <ta e="T206" id="Seg_946" s="T205">sein-PST1-3SG</ta>
            <ta e="T207" id="Seg_947" s="T206">so</ta>
            <ta e="T208" id="Seg_948" s="T207">EMPH</ta>
            <ta e="T209" id="Seg_949" s="T208">dieses</ta>
            <ta e="T210" id="Seg_950" s="T209">nun</ta>
            <ta e="T211" id="Seg_951" s="T210">Leben-DAT/LOC</ta>
            <ta e="T212" id="Seg_952" s="T211">dieses</ta>
            <ta e="T213" id="Seg_953" s="T212">Stalin.[NOM]</ta>
            <ta e="T214" id="Seg_954" s="T213">Zeit-3SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_955" s="T214">AFFIRM</ta>
            <ta e="T282" id="Seg_956" s="T281">jenes-DAT/LOC</ta>
            <ta e="T283" id="Seg_957" s="T282">was.[NOM]</ta>
            <ta e="T284" id="Seg_958" s="T283">wegen</ta>
            <ta e="T285" id="Seg_959" s="T284">versperren-PTCP.HAB</ta>
            <ta e="T286" id="Seg_960" s="T285">sein-PST1-3PL</ta>
            <ta e="T287" id="Seg_961" s="T286">dieses</ta>
            <ta e="T288" id="Seg_962" s="T287">Lied-PL-ACC</ta>
            <ta e="T289" id="Seg_963" s="T288">zuhören-NEG-IMP.2PL</ta>
            <ta e="T290" id="Seg_964" s="T289">sagen-PTCP.HAB</ta>
            <ta e="T291" id="Seg_965" s="T290">sein-PST1-3PL</ta>
            <ta e="T292" id="Seg_966" s="T291">jenes-DAT/LOC</ta>
            <ta e="T293" id="Seg_967" s="T292">was-ACC</ta>
            <ta e="T320" id="Seg_968" s="T319">Wort-2PL.[NOM]</ta>
            <ta e="T321" id="Seg_969" s="T320">EMPH</ta>
            <ta e="T322" id="Seg_970" s="T321">wegen</ta>
            <ta e="T323" id="Seg_971" s="T322">offenbar</ta>
            <ta e="T324" id="Seg_972" s="T323">hören-CVB.SIM</ta>
            <ta e="T325" id="Seg_973" s="T324">gehen-PRS-3PL</ta>
            <ta e="T326" id="Seg_974" s="T325">vielleicht</ta>
            <ta e="T327" id="Seg_975" s="T326">was.[NOM]</ta>
            <ta e="T328" id="Seg_976" s="T327">sagen-CVB.SEQ</ta>
            <ta e="T329" id="Seg_977" s="T328">sich.unterhalten-PTCP.PRS-2PL-ACC</ta>
            <ta e="T345" id="Seg_978" s="T344">jetzt</ta>
            <ta e="T346" id="Seg_979" s="T345">erinnern-PTCP.COND-DAT/LOC</ta>
            <ta e="T347" id="Seg_980" s="T346">dann</ta>
            <ta e="T351" id="Seg_981" s="T349">dann</ta>
            <ta e="T352" id="Seg_982" s="T351">dieses</ta>
            <ta e="T353" id="Seg_983" s="T352">Mensch.[NOM]</ta>
            <ta e="T354" id="Seg_984" s="T353">Angst.haben-PTCP.FUT</ta>
            <ta e="T355" id="Seg_985" s="T354">sein-PST1-3SG</ta>
            <ta e="T356" id="Seg_986" s="T355">AFFIRM</ta>
            <ta e="T366" id="Seg_987" s="T365">mancher</ta>
            <ta e="T367" id="Seg_988" s="T366">als</ta>
            <ta e="T368" id="Seg_989" s="T367">Waffe-PROPR</ta>
            <ta e="T369" id="Seg_990" s="T368">und</ta>
            <ta e="T370" id="Seg_991" s="T369">kommen-FUT-3PL</ta>
            <ta e="T371" id="Seg_992" s="T370">AFFIRM</ta>
            <ta e="T405" id="Seg_993" s="T403">Freund-EP-2SG.[NOM]</ta>
            <ta e="T406" id="Seg_994" s="T405">Name-3SG.[NOM]</ta>
            <ta e="T407" id="Seg_995" s="T406">wer.[NOM]</ta>
            <ta e="T408" id="Seg_996" s="T407">sein-PST1-3SG=Q</ta>
            <ta e="T422" id="Seg_997" s="T421">jenes</ta>
            <ta e="T423" id="Seg_998" s="T422">Mensch-PL-EP-2SG.[NOM]</ta>
            <ta e="T424" id="Seg_999" s="T423">sagen-PRS-2SG</ta>
            <ta e="T425" id="Seg_1000" s="T424">Komsomol-PL-EP-2SG.[NOM]</ta>
            <ta e="T426" id="Seg_1001" s="T425">dieses.[NOM]</ta>
            <ta e="T427" id="Seg_1002" s="T426">selbst-2PL.[NOM]</ta>
            <ta e="T428" id="Seg_1003" s="T427">dolganisch</ta>
            <ta e="T429" id="Seg_1004" s="T428">Mensch-PL-2PL.[NOM]</ta>
            <ta e="T430" id="Seg_1005" s="T429">Q</ta>
            <ta e="T449" id="Seg_1006" s="T448">selbst-2SG.[NOM]</ta>
            <ta e="T450" id="Seg_1007" s="T449">nun</ta>
            <ta e="T451" id="Seg_1008" s="T450">Leben-2SG-ACC</ta>
            <ta e="T452" id="Seg_1009" s="T451">sehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T453" id="Seg_1010" s="T452">2SG.[NOM]</ta>
            <ta e="T454" id="Seg_1011" s="T453">EMPH</ta>
            <ta e="T455" id="Seg_1012" s="T454">selbst-2SG.[NOM]</ta>
            <ta e="T456" id="Seg_1013" s="T455">wie</ta>
            <ta e="T457" id="Seg_1014" s="T456">Gedanke-2SG-INSTR</ta>
            <ta e="T458" id="Seg_1015" s="T457">leben-PTCP.PST-EP-2SG.[NOM]</ta>
            <ta e="T459" id="Seg_1016" s="T458">jenes</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KuNS">
            <ta e="T1" id="Seg_1017" s="T0">песня-PL-ACC</ta>
            <ta e="T2" id="Seg_1018" s="T1">любить-PTCP.HAB</ta>
            <ta e="T3" id="Seg_1019" s="T2">быть-PST1-1SG</ta>
            <ta e="T4" id="Seg_1020" s="T3">говорить-PRS-2SG</ta>
            <ta e="T54" id="Seg_1021" s="T53">разбивать-FREQ-IMP.2PL</ta>
            <ta e="T81" id="Seg_1022" s="T80">патефон-PROPR.[NOM]</ta>
            <ta e="T82" id="Seg_1023" s="T81">быть-PST1-2SG</ta>
            <ta e="T83" id="Seg_1024" s="T82">Q</ta>
            <ta e="T90" id="Seg_1025" s="T88">патефон-EP-2SG.[NOM]</ta>
            <ta e="T91" id="Seg_1026" s="T90">откуда</ta>
            <ta e="T92" id="Seg_1027" s="T91">приходить-PST2-3SG=Q</ta>
            <ta e="T93" id="Seg_1028" s="T92">2SG-DAT/LOC</ta>
            <ta e="T133" id="Seg_1029" s="T132">Лидия</ta>
            <ta e="T134" id="Seg_1030" s="T133">Русланова.[NOM]</ta>
            <ta e="T135" id="Seg_1031" s="T134">петь-PRS.[3SG]</ta>
            <ta e="T170" id="Seg_1032" s="T168">тот</ta>
            <ta e="T171" id="Seg_1033" s="T170">Русланова.[NOM]</ta>
            <ta e="T172" id="Seg_1034" s="T171">собственный-3PL-ACC</ta>
            <ta e="T182" id="Seg_1035" s="T181">тот.[NOM]</ta>
            <ta e="T184" id="Seg_1036" s="T183">где</ta>
            <ta e="T185" id="Seg_1037" s="T184">есть-3PL</ta>
            <ta e="T186" id="Seg_1038" s="T185">есть-3PL</ta>
            <ta e="T203" id="Seg_1039" s="T202">тот.[NOM]</ta>
            <ta e="T204" id="Seg_1040" s="T203">трудный</ta>
            <ta e="T205" id="Seg_1041" s="T204">час.[NOM]</ta>
            <ta e="T206" id="Seg_1042" s="T205">быть-PST1-3SG</ta>
            <ta e="T207" id="Seg_1043" s="T206">так</ta>
            <ta e="T208" id="Seg_1044" s="T207">EMPH</ta>
            <ta e="T209" id="Seg_1045" s="T208">этот</ta>
            <ta e="T210" id="Seg_1046" s="T209">вот</ta>
            <ta e="T211" id="Seg_1047" s="T210">жизнь-DAT/LOC</ta>
            <ta e="T212" id="Seg_1048" s="T211">тот</ta>
            <ta e="T213" id="Seg_1049" s="T212">Сталин.[NOM]</ta>
            <ta e="T214" id="Seg_1050" s="T213">час-3SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_1051" s="T214">AFFIRM</ta>
            <ta e="T282" id="Seg_1052" s="T281">тот-DAT/LOC</ta>
            <ta e="T283" id="Seg_1053" s="T282">что.[NOM]</ta>
            <ta e="T284" id="Seg_1054" s="T283">из_за</ta>
            <ta e="T285" id="Seg_1055" s="T284">заложить-PTCP.HAB</ta>
            <ta e="T286" id="Seg_1056" s="T285">быть-PST1-3PL</ta>
            <ta e="T287" id="Seg_1057" s="T286">тот</ta>
            <ta e="T288" id="Seg_1058" s="T287">песня-PL-ACC</ta>
            <ta e="T289" id="Seg_1059" s="T288">слушать-NEG-IMP.2PL</ta>
            <ta e="T290" id="Seg_1060" s="T289">говорить-PTCP.HAB</ta>
            <ta e="T291" id="Seg_1061" s="T290">быть-PST1-3PL</ta>
            <ta e="T292" id="Seg_1062" s="T291">тот-DAT/LOC</ta>
            <ta e="T293" id="Seg_1063" s="T292">что-ACC</ta>
            <ta e="T320" id="Seg_1064" s="T319">слово-2PL.[NOM]</ta>
            <ta e="T321" id="Seg_1065" s="T320">EMPH</ta>
            <ta e="T322" id="Seg_1066" s="T321">из_за</ta>
            <ta e="T323" id="Seg_1067" s="T322">очевидно</ta>
            <ta e="T324" id="Seg_1068" s="T323">слышать-CVB.SIM</ta>
            <ta e="T325" id="Seg_1069" s="T324">идти-PRS-3PL</ta>
            <ta e="T326" id="Seg_1070" s="T325">может</ta>
            <ta e="T327" id="Seg_1071" s="T326">что.[NOM]</ta>
            <ta e="T328" id="Seg_1072" s="T327">говорить-CVB.SEQ</ta>
            <ta e="T329" id="Seg_1073" s="T328">разговаривать-PTCP.PRS-2PL-ACC</ta>
            <ta e="T345" id="Seg_1074" s="T344">теперь</ta>
            <ta e="T346" id="Seg_1075" s="T345">помнить-PTCP.COND-DAT/LOC</ta>
            <ta e="T347" id="Seg_1076" s="T346">потом</ta>
            <ta e="T351" id="Seg_1077" s="T349">тогда</ta>
            <ta e="T352" id="Seg_1078" s="T351">этот</ta>
            <ta e="T353" id="Seg_1079" s="T352">человек.[NOM]</ta>
            <ta e="T354" id="Seg_1080" s="T353">бояться-PTCP.FUT</ta>
            <ta e="T355" id="Seg_1081" s="T354">быть-PST1-3SG</ta>
            <ta e="T356" id="Seg_1082" s="T355">AFFIRM</ta>
            <ta e="T366" id="Seg_1083" s="T365">некоторый</ta>
            <ta e="T367" id="Seg_1084" s="T366">когда</ta>
            <ta e="T368" id="Seg_1085" s="T367">оружие-PROPR</ta>
            <ta e="T369" id="Seg_1086" s="T368">да</ta>
            <ta e="T370" id="Seg_1087" s="T369">приходить-FUT-3PL</ta>
            <ta e="T371" id="Seg_1088" s="T370">AFFIRM</ta>
            <ta e="T405" id="Seg_1089" s="T403">друг-EP-2SG.[NOM]</ta>
            <ta e="T406" id="Seg_1090" s="T405">имя-3SG.[NOM]</ta>
            <ta e="T407" id="Seg_1091" s="T406">кто.[NOM]</ta>
            <ta e="T408" id="Seg_1092" s="T407">быть-PST1-3SG=Q</ta>
            <ta e="T422" id="Seg_1093" s="T421">тот</ta>
            <ta e="T423" id="Seg_1094" s="T422">человек-PL-EP-2SG.[NOM]</ta>
            <ta e="T424" id="Seg_1095" s="T423">говорить-PRS-2SG</ta>
            <ta e="T425" id="Seg_1096" s="T424">комсомол-PL-EP-2SG.[NOM]</ta>
            <ta e="T426" id="Seg_1097" s="T425">тот.[NOM]</ta>
            <ta e="T427" id="Seg_1098" s="T426">сам-2PL.[NOM]</ta>
            <ta e="T428" id="Seg_1099" s="T427">долганский</ta>
            <ta e="T429" id="Seg_1100" s="T428">человек-PL-2PL.[NOM]</ta>
            <ta e="T430" id="Seg_1101" s="T429">Q</ta>
            <ta e="T449" id="Seg_1102" s="T448">сам-2SG.[NOM]</ta>
            <ta e="T450" id="Seg_1103" s="T449">вот</ta>
            <ta e="T451" id="Seg_1104" s="T450">жизнь-2SG-ACC</ta>
            <ta e="T452" id="Seg_1105" s="T451">видеть-PTCP.COND-DAT/LOC</ta>
            <ta e="T453" id="Seg_1106" s="T452">2SG.[NOM]</ta>
            <ta e="T454" id="Seg_1107" s="T453">EMPH</ta>
            <ta e="T455" id="Seg_1108" s="T454">сам-2SG.[NOM]</ta>
            <ta e="T456" id="Seg_1109" s="T455">как</ta>
            <ta e="T457" id="Seg_1110" s="T456">мысль-2SG-INSTR</ta>
            <ta e="T458" id="Seg_1111" s="T457">жить-PTCP.PST-EP-2SG.[NOM]</ta>
            <ta e="T459" id="Seg_1112" s="T458">тот</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KuNS">
            <ta e="T1" id="Seg_1113" s="T0">n-n:(num)-n:case</ta>
            <ta e="T2" id="Seg_1114" s="T1">v-v:ptcp</ta>
            <ta e="T3" id="Seg_1115" s="T2">v-v:tense-v:poss.pn</ta>
            <ta e="T4" id="Seg_1116" s="T3">v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_1117" s="T53">v-v&gt;v-v:mood.pn</ta>
            <ta e="T81" id="Seg_1118" s="T80">n-n&gt;adj-n:case</ta>
            <ta e="T82" id="Seg_1119" s="T81">v-v:tense-v:poss.pn</ta>
            <ta e="T83" id="Seg_1120" s="T82">ptcl</ta>
            <ta e="T90" id="Seg_1121" s="T88">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T91" id="Seg_1122" s="T90">que</ta>
            <ta e="T92" id="Seg_1123" s="T91">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T93" id="Seg_1124" s="T92">pers-pro:case</ta>
            <ta e="T133" id="Seg_1125" s="T132">propr</ta>
            <ta e="T134" id="Seg_1126" s="T133">propr-n:case</ta>
            <ta e="T135" id="Seg_1127" s="T134">v-v:tense-v:pred.pn</ta>
            <ta e="T170" id="Seg_1128" s="T168">dempro</ta>
            <ta e="T171" id="Seg_1129" s="T170">propr-n:case</ta>
            <ta e="T172" id="Seg_1130" s="T171">adj-n:poss-n:case</ta>
            <ta e="T182" id="Seg_1131" s="T181">dempro-pro:case</ta>
            <ta e="T184" id="Seg_1132" s="T183">que</ta>
            <ta e="T185" id="Seg_1133" s="T184">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T186" id="Seg_1134" s="T185">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T203" id="Seg_1135" s="T202">dempro-pro:case</ta>
            <ta e="T204" id="Seg_1136" s="T203">adj</ta>
            <ta e="T205" id="Seg_1137" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_1138" s="T205">v-v:tense-v:poss.pn</ta>
            <ta e="T207" id="Seg_1139" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_1140" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_1141" s="T208">dempro</ta>
            <ta e="T210" id="Seg_1142" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_1143" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_1144" s="T211">dempro</ta>
            <ta e="T213" id="Seg_1145" s="T212">propr-n:case</ta>
            <ta e="T214" id="Seg_1146" s="T213">n-n:poss-n:case</ta>
            <ta e="T215" id="Seg_1147" s="T214">ptcl</ta>
            <ta e="T282" id="Seg_1148" s="T281">dempro-pro:case</ta>
            <ta e="T283" id="Seg_1149" s="T282">que-pro:case</ta>
            <ta e="T284" id="Seg_1150" s="T283">post</ta>
            <ta e="T285" id="Seg_1151" s="T284">v-v:ptcp</ta>
            <ta e="T286" id="Seg_1152" s="T285">v-v:tense-v:poss.pn</ta>
            <ta e="T287" id="Seg_1153" s="T286">dempro</ta>
            <ta e="T288" id="Seg_1154" s="T287">n-n:(num)-n:case</ta>
            <ta e="T289" id="Seg_1155" s="T288">v-v:(neg)-v:mood.pn</ta>
            <ta e="T290" id="Seg_1156" s="T289">v-v:ptcp</ta>
            <ta e="T291" id="Seg_1157" s="T290">v-v:tense-v:poss.pn</ta>
            <ta e="T292" id="Seg_1158" s="T291">dempro-pro:case</ta>
            <ta e="T293" id="Seg_1159" s="T292">que-pro:case</ta>
            <ta e="T320" id="Seg_1160" s="T319">n-n:(poss)-n:case</ta>
            <ta e="T321" id="Seg_1161" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_1162" s="T321">post</ta>
            <ta e="T323" id="Seg_1163" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_1164" s="T323">v-v:cvb</ta>
            <ta e="T325" id="Seg_1165" s="T324">v-v:tense-v:poss.pn</ta>
            <ta e="T326" id="Seg_1166" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_1167" s="T326">que-pro:case</ta>
            <ta e="T328" id="Seg_1168" s="T327">v-v:cvb</ta>
            <ta e="T329" id="Seg_1169" s="T328">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T345" id="Seg_1170" s="T344">adv</ta>
            <ta e="T346" id="Seg_1171" s="T345">v-v:ptcp-v:(case)</ta>
            <ta e="T347" id="Seg_1172" s="T346">adv</ta>
            <ta e="T351" id="Seg_1173" s="T349">adv</ta>
            <ta e="T352" id="Seg_1174" s="T351">dempro</ta>
            <ta e="T353" id="Seg_1175" s="T352">n-n:case</ta>
            <ta e="T354" id="Seg_1176" s="T353">v-v:ptcp</ta>
            <ta e="T355" id="Seg_1177" s="T354">v-v:tense-v:poss.pn</ta>
            <ta e="T356" id="Seg_1178" s="T355">ptcl</ta>
            <ta e="T366" id="Seg_1179" s="T365">indfpro</ta>
            <ta e="T367" id="Seg_1180" s="T366">post</ta>
            <ta e="T368" id="Seg_1181" s="T367">n-n&gt;adj</ta>
            <ta e="T369" id="Seg_1182" s="T368">conj</ta>
            <ta e="T370" id="Seg_1183" s="T369">v-v:tense-v:poss.pn</ta>
            <ta e="T371" id="Seg_1184" s="T370">ptcl</ta>
            <ta e="T405" id="Seg_1185" s="T403">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T406" id="Seg_1186" s="T405">n-n:(poss)-n:case</ta>
            <ta e="T407" id="Seg_1187" s="T406">que-pro:case</ta>
            <ta e="T408" id="Seg_1188" s="T407">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T422" id="Seg_1189" s="T421">dempro</ta>
            <ta e="T423" id="Seg_1190" s="T422">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T424" id="Seg_1191" s="T423">v-v:tense-v:pred.pn</ta>
            <ta e="T425" id="Seg_1192" s="T424">propr-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T426" id="Seg_1193" s="T425">dempro-pro:case</ta>
            <ta e="T427" id="Seg_1194" s="T426">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T428" id="Seg_1195" s="T427">adj</ta>
            <ta e="T429" id="Seg_1196" s="T428">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T430" id="Seg_1197" s="T429">ptcl</ta>
            <ta e="T449" id="Seg_1198" s="T448">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T450" id="Seg_1199" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_1200" s="T450">n-n:poss-n:case</ta>
            <ta e="T452" id="Seg_1201" s="T451">v-v:ptcp-v:(case)</ta>
            <ta e="T453" id="Seg_1202" s="T452">pers-pro:case</ta>
            <ta e="T454" id="Seg_1203" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_1204" s="T454">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T456" id="Seg_1205" s="T455">que</ta>
            <ta e="T457" id="Seg_1206" s="T456">n-n:poss-n:case</ta>
            <ta e="T458" id="Seg_1207" s="T457">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T459" id="Seg_1208" s="T458">dempro</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KuNS">
            <ta e="T1" id="Seg_1209" s="T0">n</ta>
            <ta e="T2" id="Seg_1210" s="T1">v</ta>
            <ta e="T3" id="Seg_1211" s="T2">aux</ta>
            <ta e="T4" id="Seg_1212" s="T3">v</ta>
            <ta e="T54" id="Seg_1213" s="T53">v</ta>
            <ta e="T81" id="Seg_1214" s="T80">adj</ta>
            <ta e="T82" id="Seg_1215" s="T81">cop</ta>
            <ta e="T83" id="Seg_1216" s="T82">ptcl</ta>
            <ta e="T90" id="Seg_1217" s="T88">n</ta>
            <ta e="T91" id="Seg_1218" s="T90">que</ta>
            <ta e="T92" id="Seg_1219" s="T91">v</ta>
            <ta e="T93" id="Seg_1220" s="T92">pers</ta>
            <ta e="T133" id="Seg_1221" s="T132">propr</ta>
            <ta e="T134" id="Seg_1222" s="T133">propr</ta>
            <ta e="T135" id="Seg_1223" s="T134">v</ta>
            <ta e="T170" id="Seg_1224" s="T168">dempro</ta>
            <ta e="T171" id="Seg_1225" s="T170">propr</ta>
            <ta e="T172" id="Seg_1226" s="T171">n</ta>
            <ta e="T182" id="Seg_1227" s="T181">dempro</ta>
            <ta e="T184" id="Seg_1228" s="T183">que</ta>
            <ta e="T185" id="Seg_1229" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_1230" s="T185">ptcl</ta>
            <ta e="T203" id="Seg_1231" s="T202">dempro</ta>
            <ta e="T204" id="Seg_1232" s="T203">adj</ta>
            <ta e="T205" id="Seg_1233" s="T204">n</ta>
            <ta e="T206" id="Seg_1234" s="T205">cop</ta>
            <ta e="T207" id="Seg_1235" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_1236" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_1237" s="T208">dempro</ta>
            <ta e="T210" id="Seg_1238" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_1239" s="T210">n</ta>
            <ta e="T212" id="Seg_1240" s="T211">dempro</ta>
            <ta e="T213" id="Seg_1241" s="T212">propr</ta>
            <ta e="T214" id="Seg_1242" s="T213">n</ta>
            <ta e="T215" id="Seg_1243" s="T214">ptcl</ta>
            <ta e="T282" id="Seg_1244" s="T281">dempro</ta>
            <ta e="T283" id="Seg_1245" s="T282">que</ta>
            <ta e="T284" id="Seg_1246" s="T283">post</ta>
            <ta e="T285" id="Seg_1247" s="T284">v</ta>
            <ta e="T286" id="Seg_1248" s="T285">aux</ta>
            <ta e="T287" id="Seg_1249" s="T286">dempro</ta>
            <ta e="T288" id="Seg_1250" s="T287">n</ta>
            <ta e="T289" id="Seg_1251" s="T288">v</ta>
            <ta e="T290" id="Seg_1252" s="T289">v</ta>
            <ta e="T291" id="Seg_1253" s="T290">aux</ta>
            <ta e="T292" id="Seg_1254" s="T291">dempro</ta>
            <ta e="T293" id="Seg_1255" s="T292">que</ta>
            <ta e="T320" id="Seg_1256" s="T319">n</ta>
            <ta e="T321" id="Seg_1257" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_1258" s="T321">post</ta>
            <ta e="T323" id="Seg_1259" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_1260" s="T323">v</ta>
            <ta e="T325" id="Seg_1261" s="T324">aux</ta>
            <ta e="T326" id="Seg_1262" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_1263" s="T326">que</ta>
            <ta e="T328" id="Seg_1264" s="T327">v</ta>
            <ta e="T329" id="Seg_1265" s="T328">v</ta>
            <ta e="T345" id="Seg_1266" s="T344">adv</ta>
            <ta e="T346" id="Seg_1267" s="T345">v</ta>
            <ta e="T347" id="Seg_1268" s="T346">adv</ta>
            <ta e="T351" id="Seg_1269" s="T349">adv</ta>
            <ta e="T352" id="Seg_1270" s="T351">dempro</ta>
            <ta e="T353" id="Seg_1271" s="T352">n</ta>
            <ta e="T354" id="Seg_1272" s="T353">v</ta>
            <ta e="T355" id="Seg_1273" s="T354">cop</ta>
            <ta e="T356" id="Seg_1274" s="T355">ptcl</ta>
            <ta e="T366" id="Seg_1275" s="T365">indfpro</ta>
            <ta e="T367" id="Seg_1276" s="T366">post</ta>
            <ta e="T368" id="Seg_1277" s="T367">adj</ta>
            <ta e="T369" id="Seg_1278" s="T368">conj</ta>
            <ta e="T370" id="Seg_1279" s="T369">v</ta>
            <ta e="T371" id="Seg_1280" s="T370">ptcl</ta>
            <ta e="T405" id="Seg_1281" s="T403">n</ta>
            <ta e="T406" id="Seg_1282" s="T405">n</ta>
            <ta e="T407" id="Seg_1283" s="T406">que</ta>
            <ta e="T408" id="Seg_1284" s="T407">cop</ta>
            <ta e="T422" id="Seg_1285" s="T421">dempro</ta>
            <ta e="T423" id="Seg_1286" s="T422">n</ta>
            <ta e="T424" id="Seg_1287" s="T423">v</ta>
            <ta e="T425" id="Seg_1288" s="T424">propr</ta>
            <ta e="T426" id="Seg_1289" s="T425">dempro</ta>
            <ta e="T427" id="Seg_1290" s="T426">emphpro</ta>
            <ta e="T428" id="Seg_1291" s="T427">adj</ta>
            <ta e="T429" id="Seg_1292" s="T428">n</ta>
            <ta e="T430" id="Seg_1293" s="T429">ptcl</ta>
            <ta e="T449" id="Seg_1294" s="T448">emphpro</ta>
            <ta e="T450" id="Seg_1295" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_1296" s="T450">n</ta>
            <ta e="T452" id="Seg_1297" s="T451">v</ta>
            <ta e="T453" id="Seg_1298" s="T452">pers</ta>
            <ta e="T454" id="Seg_1299" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_1300" s="T454">emphpro</ta>
            <ta e="T456" id="Seg_1301" s="T455">que</ta>
            <ta e="T457" id="Seg_1302" s="T456">n</ta>
            <ta e="T458" id="Seg_1303" s="T457">v</ta>
            <ta e="T459" id="Seg_1304" s="T458">dempro</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KuNS" />
         <annotation name="SyF" tierref="SyF-KuNS" />
         <annotation name="IST" tierref="IST-KuNS" />
         <annotation name="Top" tierref="Top-KuNS" />
         <annotation name="Foc" tierref="Foc-KuNS" />
         <annotation name="BOR" tierref="BOR-KuNS">
            <ta e="T81" id="Seg_1305" s="T80">RUS:cult</ta>
            <ta e="T90" id="Seg_1306" s="T88">RUS:cult</ta>
            <ta e="T133" id="Seg_1307" s="T132">RUS:cult</ta>
            <ta e="T134" id="Seg_1308" s="T133">RUS:cult</ta>
            <ta e="T171" id="Seg_1309" s="T170">RUS:cult</ta>
            <ta e="T207" id="Seg_1310" s="T206">RUS:disc</ta>
            <ta e="T208" id="Seg_1311" s="T207">RUS:disc</ta>
            <ta e="T213" id="Seg_1312" s="T212">RUS:cult</ta>
            <ta e="T369" id="Seg_1313" s="T368">RUS:gram</ta>
            <ta e="T405" id="Seg_1314" s="T403">EV:core</ta>
            <ta e="T425" id="Seg_1315" s="T424">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KuNS" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KuNS" />
         <annotation name="CS" tierref="CS-KuNS" />
         <annotation name="fe" tierref="fe-KuNS">
            <ta e="T4" id="Seg_1316" s="T0">– You say that you liked songs.</ta>
            <ta e="T54" id="Seg_1317" s="T53">– Break them.</ta>
            <ta e="T83" id="Seg_1318" s="T80">– You had a gramophone?</ta>
            <ta e="T93" id="Seg_1319" s="T88">– From where did the gramophone come to you?</ta>
            <ta e="T135" id="Seg_1320" s="T132">– Lidiya Ruslanova sings.</ta>
            <ta e="T172" id="Seg_1321" s="T168">– Those of Ruslanova.</ta>
            <ta e="T186" id="Seg_1322" s="T181">They (…) where are they, are they existing?</ta>
            <ta e="T215" id="Seg_1323" s="T202">– That was a difficult time in life, in the time of Stalin, right?</ta>
            <ta e="T293" id="Seg_1324" s="T281">– And for some reason they forbad it, "don't listen to these songs", they said and for what?</ta>
            <ta e="T329" id="Seg_1325" s="T319">– Because of one word apparently, they were wiretapping, what You were talking about.</ta>
            <ta e="T347" id="Seg_1326" s="T344">– Remembering it now.</ta>
            <ta e="T356" id="Seg_1327" s="T349">– At that time the people were afraid, right?</ta>
            <ta e="T371" id="Seg_1328" s="T365">– At any time they may come with weapons, too, yes?</ta>
            <ta e="T408" id="Seg_1329" s="T403">– What was the name of your friend?</ta>
            <ta e="T430" id="Seg_1330" s="T421">– Those people, you say, the Komsomol people, were they Dolgans theirselves?</ta>
            <ta e="T459" id="Seg_1331" s="T448">– You yourself, having a look at your life, what do you think about your life?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KuNS">
            <ta e="T4" id="Seg_1332" s="T0">– Du sagst, dass du Lieder mochtest.</ta>
            <ta e="T54" id="Seg_1333" s="T53">– Zerstört sie.</ta>
            <ta e="T83" id="Seg_1334" s="T80">– Du hattest einen Plattenspieler?</ta>
            <ta e="T93" id="Seg_1335" s="T88">– Woher kam der Plattenspieler zu dir?</ta>
            <ta e="T135" id="Seg_1336" s="T132">– Lidija Ruslanova singt.</ta>
            <ta e="T172" id="Seg_1337" s="T168">– Diese von Ruslanova.</ta>
            <ta e="T186" id="Seg_1338" s="T181">Die (…) wo sind die, gibt es die?</ta>
            <ta e="T215" id="Seg_1339" s="T202">– Das war eine schwierige Zeit im Leben, in der Stalinzeit, ja?</ta>
            <ta e="T293" id="Seg_1340" s="T281">– Und wegen irgendetwas verbaten sie das, "hört diese Lieder nicht", sagten sie, und wozu?</ta>
            <ta e="T329" id="Seg_1341" s="T319">– Für ein Wort wahrscheinlich, sie hörten wohl ab, worüber ihr euch unterhaltet.</ta>
            <ta e="T347" id="Seg_1342" s="T344">– Wenn man sich jetzt erinnert.</ta>
            <ta e="T356" id="Seg_1343" s="T349">– Damals hatten die Menschen Angst, ja?</ta>
            <ta e="T371" id="Seg_1344" s="T365">– Zu jeglicher Zeit kommen sie auch bewaffnet, ja?</ta>
            <ta e="T408" id="Seg_1345" s="T403">– Wie war der Name deiner Freundin?</ta>
            <ta e="T430" id="Seg_1346" s="T421">– Diese Leute, sagst du, die Komsomolzen, waren das selbst eure Dolganen?</ta>
            <ta e="T459" id="Seg_1347" s="T448">– Du selbst, wenn du dein Leben betrachtest, mit was für Gedanken siehst du dein Leben?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KuNS" />
         <annotation name="ltr" tierref="ltr-KuNS">
            <ta e="T4" id="Seg_1348" s="T0">НК: Песни любила говоришь.</ta>
            <ta e="T54" id="Seg_1349" s="T53">НК: Разломайте.</ta>
            <ta e="T83" id="Seg_1350" s="T80">НК: Патефон у тебя был да?</ta>
            <ta e="T93" id="Seg_1351" s="T88">НК: Патефон откуда появился у тебя?</ta>
            <ta e="T135" id="Seg_1352" s="T132">НК: Лидия Русланова поёт.</ta>
            <ta e="T172" id="Seg_1353" s="T168">НК: Это Руслановой которые.</ta>
            <ta e="T215" id="Seg_1354" s="T202">НК: Это жёсткое время было так-то в жизни-то, это в Сталинское время, да?</ta>
            <ta e="T293" id="Seg_1355" s="T281">НК: Ещё за что ограничивали, эти песни не слушайте, говорили, и ещё за что?</ta>
            <ta e="T329" id="Seg_1356" s="T319">НК: За слова тоже наверно, подслушивали ходили, наверно, о чём разговариваете.</ta>
            <ta e="T347" id="Seg_1357" s="T344">НК: Сейчас если вспомнить.</ta>
            <ta e="T356" id="Seg_1358" s="T349">НК: Тогда человеку страшно было да?</ta>
            <ta e="T371" id="Seg_1359" s="T365">НК: В любое время с ружьём даже придут да?</ta>
            <ta e="T408" id="Seg_1360" s="T403">НК: Подругу как звали?</ta>
            <ta e="T430" id="Seg_1361" s="T421">НК: Эти люди, говоришь, комсомольцы, это свои долганы были да?</ta>
            <ta e="T459" id="Seg_1362" s="T448">НК: Сама-то, твою жизнь если просмотреть, ты-то сама с ками мыслями прожила?</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KuNS" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-LaVN"
                      id="tx-LaVN"
                      speaker="LaVN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-LaVN">
            <ts e="T80" id="Seg_1363" n="sc" s="T4">
               <ts e="T14" id="Seg_1365" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_1367" n="HIAT:w" s="T4">ɨbɨːj</ts>
                  <nts id="Seg_1368" n="HIAT:ip">,</nts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_1371" n="HIAT:w" s="T5">ol</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_1374" n="HIAT:w" s="T6">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_1377" n="HIAT:w" s="T7">ol</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_1380" n="HIAT:w" s="T8">ke</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_1383" n="HIAT:w" s="T9">kimi</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_1386" n="HIAT:w" s="T10">ile</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_1389" n="HIAT:w" s="T11">dʼe</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_1392" n="HIAT:w" s="T12">bihi͡eke</ts>
                  <nts id="Seg_1393" n="HIAT:ip">…</nts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_1396" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_1398" n="HIAT:w" s="T14">Stadaga</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_1401" n="HIAT:w" s="T15">olorobut</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_1404" n="HIAT:w" s="T16">bu͡olla</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_1407" n="HIAT:w" s="T17">bihigi</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_1410" n="HIAT:w" s="T18">tabannan</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_1413" n="HIAT:w" s="T19">ke</ts>
                  <nts id="Seg_1414" n="HIAT:ip">,</nts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_1417" n="HIAT:w" s="T20">tɨ͡aga</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_1420" n="HIAT:w" s="T21">ke</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_1423" n="HIAT:w" s="T22">olorobut</ts>
                  <nts id="Seg_1424" n="HIAT:ip">.</nts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_1427" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_1429" n="HIAT:w" s="T23">Kamsamol</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_1432" n="HIAT:w" s="T24">keler</ts>
                  <nts id="Seg_1433" n="HIAT:ip">,</nts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_1436" n="HIAT:w" s="T25">kamsamol</ts>
                  <nts id="Seg_1437" n="HIAT:ip">,</nts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_1440" n="HIAT:w" s="T26">tagda</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_1443" n="HIAT:w" s="T27">dʼe</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_1446" n="HIAT:w" s="T28">ɨbɨːjdar</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_1449" n="HIAT:w" s="T29">kamsamol</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_1452" n="HIAT:w" s="T30">dʼe</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_1455" n="HIAT:w" s="T31">ile</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_1458" n="HIAT:w" s="T32">bu͡opsa</ts>
                  <nts id="Seg_1459" n="HIAT:ip">.</nts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_1462" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_1464" n="HIAT:w" s="T33">Partʼejnajdar</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_1467" n="HIAT:w" s="T34">kelliler</ts>
                  <nts id="Seg_1468" n="HIAT:ip">.</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_1471" n="HIAT:u" s="T35">
                  <nts id="Seg_1472" n="HIAT:ip">"</nts>
                  <ts e="T36" id="Seg_1474" n="HIAT:w" s="T35">Aha</ts>
                  <nts id="Seg_1475" n="HIAT:ip">,</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_1478" n="HIAT:w" s="T36">ɨbɨ͡aj</ts>
                  <nts id="Seg_1479" n="HIAT:ip">"</nts>
                  <nts id="Seg_1480" n="HIAT:ip">,</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_1483" n="HIAT:w" s="T37">diːller</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_1486" n="HIAT:w" s="T38">miːgin</ts>
                  <nts id="Seg_1487" n="HIAT:ip">,</nts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1489" n="HIAT:ip">"</nts>
                  <ts e="T40" id="Seg_1491" n="HIAT:w" s="T39">kaja</ts>
                  <nts id="Seg_1492" n="HIAT:ip">,</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_1495" n="HIAT:w" s="T40">Barbaː</ts>
                  <nts id="Seg_1496" n="HIAT:ip">,</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_1499" n="HIAT:w" s="T41">palačinkalarɨŋ</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_1502" n="HIAT:w" s="T42">baːllar</ts>
                  <nts id="Seg_1503" n="HIAT:ip">?</nts>
                  <nts id="Seg_1504" n="HIAT:ip">"</nts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_1507" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_1509" n="HIAT:w" s="T43">Min</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_1512" n="HIAT:w" s="T44">diːbin</ts>
                  <nts id="Seg_1513" n="HIAT:ip">:</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_1516" n="HIAT:u" s="T45">
                  <nts id="Seg_1517" n="HIAT:ip">"</nts>
                  <ts e="T46" id="Seg_1519" n="HIAT:w" s="T45">Hu͡ok</ts>
                  <nts id="Seg_1520" n="HIAT:ip">,</nts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_1523" n="HIAT:w" s="T46">Ruslan</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_1526" n="HIAT:w" s="T47">gi͡ettere</ts>
                  <nts id="Seg_1527" n="HIAT:ip">.</nts>
                  <nts id="Seg_1528" n="HIAT:ip">"</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_1531" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_1533" n="HIAT:w" s="T48">Huːttaːbɨttara</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_1536" n="HIAT:w" s="T49">ginini</ts>
                  <nts id="Seg_1537" n="HIAT:ip">,</nts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_1540" n="HIAT:w" s="T50">di͡ebittere</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1542" n="HIAT:ip">"</nts>
                  <ts e="T53" id="Seg_1544" n="HIAT:w" s="T51">kimneːŋ</ts>
                  <nts id="Seg_1545" n="HIAT:ip">,</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_1548" n="HIAT:w" s="T53">aldʼatalaːŋ</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_1551" n="HIAT:w" s="T56">gini</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_1554" n="HIAT:w" s="T57">palačinkatɨn</ts>
                  <nts id="Seg_1555" n="HIAT:ip">.</nts>
                  <nts id="Seg_1556" n="HIAT:ip">"</nts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_1559" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_1561" n="HIAT:w" s="T58">Bu</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_1564" n="HIAT:w" s="T59">tɨ͡a</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_1567" n="HIAT:w" s="T60">kihitiger</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_1570" n="HIAT:w" s="T61">barbɨttara</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1572" n="HIAT:ip">"</nts>
                  <nts id="Seg_1573" n="HIAT:ip">(</nts>
                  <ts e="T64" id="Seg_1575" n="HIAT:w" s="T62">palačin-</ts>
                  <nts id="Seg_1576" n="HIAT:ip">)</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_1579" n="HIAT:w" s="T64">barɨta</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_1582" n="HIAT:w" s="T65">aldʼatalaːŋ</ts>
                  <nts id="Seg_1583" n="HIAT:ip">"</nts>
                  <nts id="Seg_1584" n="HIAT:ip">,</nts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_1587" n="HIAT:w" s="T66">di͡ebittere</ts>
                  <nts id="Seg_1588" n="HIAT:ip">.</nts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_1591" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_1593" n="HIAT:w" s="T67">Min</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_1596" n="HIAT:w" s="T68">diːbin</ts>
                  <nts id="Seg_1597" n="HIAT:ip">:</nts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_1600" n="HIAT:u" s="T69">
                  <nts id="Seg_1601" n="HIAT:ip">"</nts>
                  <ts e="T70" id="Seg_1603" n="HIAT:w" s="T69">Hu͡ok</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_1606" n="HIAT:w" s="T70">mini͡eke</ts>
                  <nts id="Seg_1607" n="HIAT:ip">,</nts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_1610" n="HIAT:w" s="T71">biːr</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_1613" n="HIAT:w" s="T72">daː</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_1616" n="HIAT:w" s="T73">hu͡ok</ts>
                  <nts id="Seg_1617" n="HIAT:ip">.</nts>
                  <nts id="Seg_1618" n="HIAT:ip">"</nts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_1621" n="HIAT:u" s="T74">
                  <nts id="Seg_1622" n="HIAT:ip">"</nts>
                  <ts e="T75" id="Seg_1624" n="HIAT:w" s="T74">Tuːgu</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_1627" n="HIAT:w" s="T75">ɨllatagɨn</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_1630" n="HIAT:w" s="T76">iti</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_1633" n="HIAT:w" s="T77">patʼefoŋŋar</ts>
                  <nts id="Seg_1634" n="HIAT:ip">"</nts>
                  <nts id="Seg_1635" n="HIAT:ip">,</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_1638" n="HIAT:w" s="T78">diːller</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_1641" n="HIAT:w" s="T79">minigin</ts>
                  <nts id="Seg_1642" n="HIAT:ip">.</nts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T89" id="Seg_1644" n="sc" s="T82">
               <ts e="T89" id="Seg_1646" n="HIAT:u" s="T82">
                  <ts e="T84" id="Seg_1648" n="HIAT:w" s="T82">Heː</ts>
                  <nts id="Seg_1649" n="HIAT:ip">,</nts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_1652" n="HIAT:w" s="T84">ol</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_1655" n="HIAT:w" s="T85">ke</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_1658" n="HIAT:w" s="T86">ɨllatabɨn</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_1661" n="HIAT:w" s="T87">bejem</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_1664" n="HIAT:w" s="T88">ke</ts>
                  <nts id="Seg_1665" n="HIAT:ip">.</nts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T132" id="Seg_1667" n="sc" s="T92">
               <ts e="T96" id="Seg_1669" n="HIAT:u" s="T92">
                  <ts e="T94" id="Seg_1671" n="HIAT:w" s="T92">Min</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_1674" n="HIAT:w" s="T94">bejem</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_1677" n="HIAT:w" s="T95">ɨlɨmmɨtɨm</ts>
                  <nts id="Seg_1678" n="HIAT:ip">.</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_1681" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_1683" n="HIAT:w" s="T96">Heː</ts>
                  <nts id="Seg_1684" n="HIAT:ip">,</nts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_1687" n="HIAT:w" s="T97">Čornajga</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_1690" n="HIAT:w" s="T98">hɨldʼammɨn</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_1693" n="HIAT:w" s="T99">ke</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_1696" n="HIAT:w" s="T100">oččogo</ts>
                  <nts id="Seg_1697" n="HIAT:ip">.</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_1700" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_1702" n="HIAT:w" s="T101">Bejem</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_1705" n="HIAT:w" s="T102">ɨlɨmmɨtɨm</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_1708" n="HIAT:w" s="T103">ol</ts>
                  <nts id="Seg_1709" n="HIAT:ip">.</nts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_1712" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_1714" n="HIAT:w" s="T104">Bejem</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_1717" n="HIAT:w" s="T105">ɨlan</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_1720" n="HIAT:w" s="T106">baraːn</ts>
                  <nts id="Seg_1721" n="HIAT:ip">,</nts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_1724" n="HIAT:w" s="T107">ontuŋ</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_1727" n="HIAT:w" s="T108">inneleːk</ts>
                  <nts id="Seg_1728" n="HIAT:ip">,</nts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_1731" n="HIAT:w" s="T109">tuspa</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_1734" n="HIAT:w" s="T110">korobkakaːn</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_1737" n="HIAT:w" s="T111">ki͡e</ts>
                  <nts id="Seg_1738" n="HIAT:ip">,</nts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_1741" n="HIAT:w" s="T112">onu</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_1744" n="HIAT:w" s="T113">bejebit</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_1747" n="HIAT:w" s="T114">erijebit</ts>
                  <nts id="Seg_1748" n="HIAT:ip">.</nts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_1751" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_1753" n="HIAT:w" s="T115">Onton</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_1756" n="HIAT:w" s="T116">palačinkanɨ</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_1759" n="HIAT:w" s="T117">uːrabɨt</ts>
                  <nts id="Seg_1760" n="HIAT:ip">,</nts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_1763" n="HIAT:w" s="T118">ol</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1765" n="HIAT:ip">(</nts>
                  <ts e="T121" id="Seg_1767" n="HIAT:w" s="T119">ɨll-</ts>
                  <nts id="Seg_1768" n="HIAT:ip">)</nts>
                  <nts id="Seg_1769" n="HIAT:ip">…</nts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_1772" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_1774" n="HIAT:w" s="T121">Dʼe</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1776" n="HIAT:ip">"</nts>
                  <ts e="T123" id="Seg_1778" n="HIAT:w" s="T122">Balʼenka</ts>
                  <nts id="Seg_1779" n="HIAT:ip">,</nts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_1782" n="HIAT:w" s="T123">balʼenka</ts>
                  <nts id="Seg_1783" n="HIAT:ip">"</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_1786" n="HIAT:w" s="T124">dva</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_1789" n="HIAT:w" s="T125">bɨla</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_1792" n="HIAT:w" s="T126">mini͡e</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1794" n="HIAT:ip">(</nts>
                  <ts e="T129" id="Seg_1796" n="HIAT:w" s="T127">plastin-</ts>
                  <nts id="Seg_1797" n="HIAT:ip">)</nts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_1800" n="HIAT:w" s="T129">bosku͡oj</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_1803" n="HIAT:w" s="T130">mɨnʼaː</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_1806" n="HIAT:w" s="T131">ɨrɨ͡alaːk</ts>
                  <nts id="Seg_1807" n="HIAT:ip">.</nts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_1809" n="sc" s="T135">
               <ts e="T138" id="Seg_1811" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_1813" n="HIAT:w" s="T135">Aha</ts>
                  <nts id="Seg_1814" n="HIAT:ip">,</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_1817" n="HIAT:w" s="T136">Ruslaːnava</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_1820" n="HIAT:w" s="T137">ɨllɨːr</ts>
                  <nts id="Seg_1821" n="HIAT:ip">:</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_1824" n="HIAT:u" s="T138">
                  <nts id="Seg_1825" n="HIAT:ip">"</nts>
                  <ts e="T139" id="Seg_1827" n="HIAT:w" s="T138">Balʼenka</ts>
                  <nts id="Seg_1828" n="HIAT:ip">,</nts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_1831" n="HIAT:w" s="T139">balʼenka</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_1834" n="HIAT:w" s="T140">passɨtaja</ts>
                  <nts id="Seg_1835" n="HIAT:ip">"</nts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_1838" n="HIAT:w" s="T141">bu͡ol</ts>
                  <nts id="Seg_1839" n="HIAT:ip">,</nts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_1842" n="HIAT:w" s="T142">dʼe</ts>
                  <nts id="Seg_1843" n="HIAT:ip">,</nts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1845" n="HIAT:ip">"</nts>
                  <ts e="T144" id="Seg_1847" n="HIAT:w" s="T143">ontubun</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_1850" n="HIAT:w" s="T144">dʼe</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_1853" n="HIAT:w" s="T145">kisteː</ts>
                  <nts id="Seg_1854" n="HIAT:ip">"</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_1857" n="HIAT:w" s="T146">diːn</ts>
                  <nts id="Seg_1858" n="HIAT:ip">.</nts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_1861" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_1863" n="HIAT:w" s="T147">Ogobor</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_1866" n="HIAT:w" s="T148">kim</ts>
                  <nts id="Seg_1867" n="HIAT:ip">,</nts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_1870" n="HIAT:w" s="T149">periːnekeːn</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_1873" n="HIAT:w" s="T150">obu͡oja</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_1876" n="HIAT:w" s="T151">oŋorbutum</ts>
                  <nts id="Seg_1877" n="HIAT:ip">.</nts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_1880" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_1882" n="HIAT:w" s="T152">Kuččuguj</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_1885" n="HIAT:w" s="T153">ogo</ts>
                  <nts id="Seg_1886" n="HIAT:ip">.</nts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_1889" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_1891" n="HIAT:w" s="T154">Ol</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_1894" n="HIAT:w" s="T155">ihinen</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_1897" n="HIAT:w" s="T156">tigen</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_1900" n="HIAT:w" s="T157">keːhen</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_1903" n="HIAT:w" s="T158">baraːn</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_1906" n="HIAT:w" s="T159">onno</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1908" n="HIAT:ip">(</nts>
                  <nts id="Seg_1909" n="HIAT:ip">(</nts>
                  <ats e="T162" id="Seg_1910" n="HIAT:non-pho" s="T160">…</ats>
                  <nts id="Seg_1911" n="HIAT:ip">)</nts>
                  <nts id="Seg_1912" n="HIAT:ip">)</nts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_1915" n="HIAT:w" s="T162">onno</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_1918" n="HIAT:w" s="T161">uːrabɨn</ts>
                  <nts id="Seg_1919" n="HIAT:ip">.</nts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_1922" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_1924" n="HIAT:w" s="T163">Ikki</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_1927" n="HIAT:w" s="T164">palačinkanɨ</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_1930" n="HIAT:w" s="T165">kisteːbitim</ts>
                  <nts id="Seg_1931" n="HIAT:ip">.</nts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_1934" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_1936" n="HIAT:w" s="T166">Hanɨga</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_1939" n="HIAT:w" s="T167">di͡eri</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_1942" n="HIAT:w" s="T168">kisteːbitim</ts>
                  <nts id="Seg_1943" n="HIAT:ip">.</nts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_1945" n="sc" s="T171">
               <ts e="T174" id="Seg_1947" n="HIAT:u" s="T171">
                  <ts e="T173" id="Seg_1949" n="HIAT:w" s="T171">Ruslaːnava</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_1952" n="HIAT:w" s="T173">gi͡etterin</ts>
                  <nts id="Seg_1953" n="HIAT:ip">.</nts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_1956" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_1958" n="HIAT:w" s="T174">Oːj</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_1961" n="HIAT:w" s="T175">ile</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_1964" n="HIAT:w" s="T176">kakʼije</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_1967" n="HIAT:w" s="T177">bɨlʼi</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_1970" n="HIAT:w" s="T178">ile</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1972" n="HIAT:ip">(</nts>
                  <ts e="T181" id="Seg_1974" n="HIAT:w" s="T179">za-</ts>
                  <nts id="Seg_1975" n="HIAT:ip">)</nts>
                  <nts id="Seg_1976" n="HIAT:ip">…</nts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T202" id="Seg_1978" n="sc" s="T185">
               <ts e="T195" id="Seg_1980" n="HIAT:u" s="T185">
                  <ts e="T187" id="Seg_1982" n="HIAT:w" s="T185">Ol</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_1985" n="HIAT:w" s="T187">palačinkalarbɨn</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_1988" n="HIAT:w" s="T188">bu</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_1991" n="HIAT:w" s="T189">ogolor</ts>
                  <nts id="Seg_1992" n="HIAT:ip">,</nts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_1995" n="HIAT:w" s="T190">ulaːtan</ts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_1998" n="HIAT:w" s="T191">baraːnnar</ts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_2001" n="HIAT:w" s="T192">čeŋkičči</ts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_2004" n="HIAT:w" s="T193">aldʼatalaːbɨttar</ts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_2007" n="HIAT:w" s="T194">bu͡o</ts>
                  <nts id="Seg_2008" n="HIAT:ip">.</nts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_2011" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_2013" n="HIAT:w" s="T195">Bu</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_2016" n="HIAT:w" s="T196">nʼečʼajanno</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_2019" n="HIAT:w" s="T197">Lʼusʼa</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_2022" n="HIAT:w" s="T198">ol</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_2025" n="HIAT:w" s="T199">хazʼaina</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_2028" n="HIAT:w" s="T200">et</ts>
                  <nts id="Seg_2029" n="HIAT:ip">,</nts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_2032" n="HIAT:w" s="T201">barɨtɨn</ts>
                  <nts id="Seg_2033" n="HIAT:ip">.</nts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T281" id="Seg_2035" n="sc" s="T214">
               <ts e="T223" id="Seg_2037" n="HIAT:u" s="T214">
                  <ts e="T216" id="Seg_2039" n="HIAT:w" s="T214">Oččogo</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_2042" n="HIAT:w" s="T216">Stalʼinɨŋ</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_2045" n="HIAT:w" s="T217">ete</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_2048" n="HIAT:w" s="T218">bu͡olla</ts>
                  <nts id="Seg_2049" n="HIAT:ip">,</nts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_2052" n="HIAT:w" s="T219">heː</ts>
                  <nts id="Seg_2053" n="HIAT:ip">,</nts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_2056" n="HIAT:w" s="T220">oččogo</ts>
                  <nts id="Seg_2057" n="HIAT:ip">,</nts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_2060" n="HIAT:w" s="T221">ol</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2062" n="HIAT:ip">(</nts>
                  <ts e="T223" id="Seg_2064" n="HIAT:w" s="T222">huːt-</ts>
                  <nts id="Seg_2065" n="HIAT:ip">)</nts>
                  <nts id="Seg_2066" n="HIAT:ip">.</nts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_2069" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_2071" n="HIAT:w" s="T223">Ol</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_2074" n="HIAT:w" s="T224">kenni</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_2077" n="HIAT:w" s="T225">ginini</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_2080" n="HIAT:w" s="T226">huːttaːbɨttar</ts>
                  <nts id="Seg_2081" n="HIAT:ip">,</nts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_2084" n="HIAT:w" s="T227">ginini</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_2087" n="HIAT:w" s="T228">ol</ts>
                  <nts id="Seg_2088" n="HIAT:ip">.</nts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_2091" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_2093" n="HIAT:w" s="T229">Patamusta</ts>
                  <nts id="Seg_2094" n="HIAT:ip">,</nts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_2097" n="HIAT:w" s="T230">noː</ts>
                  <nts id="Seg_2098" n="HIAT:ip">,</nts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_2101" n="HIAT:w" s="T231">tagda-to</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_2104" n="HIAT:w" s="T234">vajna</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_2107" n="HIAT:w" s="T235">bɨla</ts>
                  <nts id="Seg_2108" n="HIAT:ip">.</nts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_2111" n="HIAT:u" s="T236">
                  <ts e="T238" id="Seg_2113" n="HIAT:w" s="T236">Onʼi-ta</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_2116" n="HIAT:w" s="T238">pʼesnʼi</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_2119" n="HIAT:w" s="T239">pʼeʼli</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_2122" n="HIAT:w" s="T240">хadʼilʼi-ta</ts>
                  <nts id="Seg_2123" n="HIAT:ip">.</nts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_2126" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_2128" n="HIAT:w" s="T244">Oččogo</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_2131" n="HIAT:w" s="T245">gini</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_2134" n="HIAT:w" s="T246">bajnaːga</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_2137" n="HIAT:w" s="T247">ol</ts>
                  <nts id="Seg_2138" n="HIAT:ip">,</nts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_2141" n="HIAT:w" s="T248">barar</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_2144" n="HIAT:w" s="T249">da</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_2147" n="HIAT:w" s="T250">vajnaːga</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_2150" n="HIAT:w" s="T251">ke</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_2153" n="HIAT:w" s="T252">ɨllatallar</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_2156" n="HIAT:w" s="T253">diː</ts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_2159" n="HIAT:w" s="T254">artʼiːstarɨ</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_2162" n="HIAT:w" s="T255">ke</ts>
                  <nts id="Seg_2163" n="HIAT:ip">.</nts>
                  <nts id="Seg_2164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_2166" n="HIAT:u" s="T256">
                  <ts e="T380" id="Seg_2168" n="HIAT:w" s="T256">No</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_2171" n="HIAT:w" s="T380">ana</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_2174" n="HIAT:w" s="T257">pa</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_2177" n="HIAT:w" s="T258">starastʼi</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_2180" n="HIAT:w" s="T259">kakʼije-ta</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_2183" n="HIAT:w" s="T262">pʼesnʼi</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_2186" n="HIAT:w" s="T263">pʼela</ts>
                  <nts id="Seg_2187" n="HIAT:ip">,</nts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_2190" n="HIAT:w" s="T264">ol</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_2193" n="HIAT:w" s="T265">ihin</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_2196" n="HIAT:w" s="T266">gilleri</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_2199" n="HIAT:w" s="T267">huːttuːllar</ts>
                  <nts id="Seg_2200" n="HIAT:ip">,</nts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_2203" n="HIAT:w" s="T268">oː</ts>
                  <nts id="Seg_2204" n="HIAT:ip">,</nts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_2207" n="HIAT:w" s="T269">muŋnaːktarɨ</ts>
                  <nts id="Seg_2208" n="HIAT:ip">.</nts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_2211" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_2213" n="HIAT:w" s="T271">Erin</ts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_2216" n="HIAT:w" s="T272">gɨtta</ts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_2219" n="HIAT:w" s="T273">ikki͡ennerin</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_2222" n="HIAT:w" s="T274">huːttaːbɨttara</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_2225" n="HIAT:w" s="T275">eni</ts>
                  <nts id="Seg_2226" n="HIAT:ip">.</nts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_2229" n="HIAT:u" s="T276">
                  <ts e="T243" id="Seg_2231" n="HIAT:w" s="T276">Takʼie</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_2234" n="HIAT:w" s="T243">bɨlʼi</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_2237" n="HIAT:w" s="T277">anʼi</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_2240" n="HIAT:w" s="T278">ranse</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_2243" n="HIAT:w" s="T279">lʼudʼi</ts>
                  <nts id="Seg_2244" n="HIAT:ip">.</nts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T319" id="Seg_2246" n="sc" s="T293">
               <ts e="T319" id="Seg_2248" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_2250" n="HIAT:w" s="T293">Dʼe</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_2253" n="HIAT:w" s="T294">ol</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_2256" n="HIAT:w" s="T295">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_2259" n="HIAT:w" s="T296">ihilleːmi͡ekke</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_2262" n="HIAT:w" s="T297">ginner</ts>
                  <nts id="Seg_2263" n="HIAT:ip">,</nts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_2266" n="HIAT:w" s="T298">prosta</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_2269" n="HIAT:w" s="T299">ginner</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_2272" n="HIAT:w" s="T300">tu͡ogu</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_2275" n="HIAT:w" s="T301">ere</ts>
                  <nts id="Seg_2276" n="HIAT:ip">,</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_2279" n="HIAT:w" s="T302">no</ts>
                  <nts id="Seg_2280" n="HIAT:ip">,</nts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_2283" n="HIAT:w" s="T303">kak</ts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_2286" n="HIAT:w" s="T304">ta</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_2289" n="HIAT:w" s="T305">brʼedʼitelʼ</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_2292" n="HIAT:w" s="T306">li</ts>
                  <nts id="Seg_2293" n="HIAT:ip">,</nts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_2296" n="HIAT:w" s="T307">čʼo</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_2299" n="HIAT:w" s="T308">li</ts>
                  <nts id="Seg_2300" n="HIAT:ip">,</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_2303" n="HIAT:w" s="T309">kakoj</ts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_2306" n="HIAT:w" s="T310">li</ts>
                  <nts id="Seg_2307" n="HIAT:ip">,</nts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_2310" n="HIAT:w" s="T311">oččogo</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_2313" n="HIAT:w" s="T312">ol</ts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_2316" n="HIAT:w" s="T313">iti</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_2319" n="HIAT:w" s="T314">ol</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_2322" n="HIAT:w" s="T315">bihi͡ettere</ts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_2325" n="HIAT:w" s="T316">ol</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_2328" n="HIAT:w" s="T317">uhuttuːllar</ts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_2331" n="HIAT:w" s="T318">ol</ts>
                  <nts id="Seg_2332" n="HIAT:ip">.</nts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T344" id="Seg_2334" n="sc" s="T329">
               <ts e="T344" id="Seg_2336" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_2338" n="HIAT:w" s="T329">Dʼe</ts>
                  <nts id="Seg_2339" n="HIAT:ip">,</nts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_2342" n="HIAT:w" s="T330">eni</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_2345" n="HIAT:w" s="T331">ke</ts>
                  <nts id="Seg_2346" n="HIAT:ip">,</nts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_2349" n="HIAT:w" s="T332">dʼe</ts>
                  <nts id="Seg_2350" n="HIAT:ip">,</nts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_2353" n="HIAT:w" s="T333">ol</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_2356" n="HIAT:w" s="T334">dʼe</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_2359" n="HIAT:w" s="T335">uhulaktaːn</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_2362" n="HIAT:w" s="T336">keːheller</ts>
                  <nts id="Seg_2363" n="HIAT:ip">,</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_2366" n="HIAT:w" s="T337">ol</ts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_2369" n="HIAT:w" s="T338">kistiːbit</ts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_2372" n="HIAT:w" s="T339">dʼe</ts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_2375" n="HIAT:w" s="T340">ontugun</ts>
                  <nts id="Seg_2376" n="HIAT:ip">,</nts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_2379" n="HIAT:w" s="T341">oj</ts>
                  <nts id="Seg_2380" n="HIAT:ip">,</nts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_2383" n="HIAT:w" s="T342">takaja</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_2386" n="HIAT:w" s="T343">ile</ts>
                  <nts id="Seg_2387" n="HIAT:ip">.</nts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T350" id="Seg_2389" n="sc" s="T346">
               <ts e="T350" id="Seg_2391" n="HIAT:u" s="T346">
                  <ts e="T348" id="Seg_2393" n="HIAT:w" s="T346">De</ts>
                  <nts id="Seg_2394" n="HIAT:ip">,</nts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_2397" n="HIAT:w" s="T348">anɨ</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_2400" n="HIAT:w" s="T349">öjdöːtökkö</ts>
                  <nts id="Seg_2401" n="HIAT:ip">.</nts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T365" id="Seg_2403" n="sc" s="T355">
               <ts e="T365" id="Seg_2405" n="HIAT:u" s="T355">
                  <ts e="T357" id="Seg_2407" n="HIAT:w" s="T355">No</ts>
                  <nts id="Seg_2408" n="HIAT:ip">,</nts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_2411" n="HIAT:w" s="T357">kihi</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_2414" n="HIAT:w" s="T358">kuttanɨ͡ak</ts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_2417" n="HIAT:w" s="T359">bu͡olu͡o</ts>
                  <nts id="Seg_2418" n="HIAT:ip">,</nts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_2421" n="HIAT:w" s="T360">oččogo</ts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_2424" n="HIAT:w" s="T361">keleller</ts>
                  <nts id="Seg_2425" n="HIAT:ip">,</nts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_2428" n="HIAT:w" s="T362">aldʼatalɨːllar</ts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_2431" n="HIAT:w" s="T363">ke</ts>
                  <nts id="Seg_2432" n="HIAT:ip">,</nts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_2435" n="HIAT:w" s="T364">keleller</ts>
                  <nts id="Seg_2436" n="HIAT:ip">.</nts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T404" id="Seg_2438" n="sc" s="T370">
               <ts e="T376" id="Seg_2440" n="HIAT:u" s="T370">
                  <ts e="T372" id="Seg_2442" n="HIAT:w" s="T370">Nuː</ts>
                  <nts id="Seg_2443" n="HIAT:ip">,</nts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_2446" n="HIAT:w" s="T372">mini͡ene</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_2449" n="HIAT:w" s="T373">padruskam</ts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_2452" n="HIAT:w" s="T374">bɨla</ts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_2455" n="HIAT:w" s="T375">ana</ts>
                  <nts id="Seg_2456" n="HIAT:ip">.</nts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_2459" n="HIAT:u" s="T376">
                  <ts e="T270" id="Seg_2461" n="HIAT:w" s="T376">Ana</ts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_2464" n="HIAT:w" s="T270">v</ts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_2467" n="HIAT:w" s="T377">gorat</ts>
                  <nts id="Seg_2468" n="HIAT:ip">,</nts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_2471" n="HIAT:w" s="T378">ɨbɨːj</ts>
                  <nts id="Seg_2472" n="HIAT:ip">,</nts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_2475" n="HIAT:w" s="T379">barbat</ts>
                  <nts id="Seg_2476" n="HIAT:ip">.</nts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T388" id="Seg_2479" n="HIAT:u" s="T381">
                  <nts id="Seg_2480" n="HIAT:ip">"</nts>
                  <ts e="T382" id="Seg_2482" n="HIAT:w" s="T381">Öllübüt</ts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_2485" n="HIAT:w" s="T382">agaj</ts>
                  <nts id="Seg_2486" n="HIAT:ip">,</nts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_2489" n="HIAT:w" s="T383">kimŋin</ts>
                  <nts id="Seg_2490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_2492" n="HIAT:w" s="T384">kisteː</ts>
                  <nts id="Seg_2493" n="HIAT:ip">"</nts>
                  <nts id="Seg_2494" n="HIAT:ip">,</nts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_2497" n="HIAT:w" s="T385">diːr</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_2500" n="HIAT:w" s="T386">minigin</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_2503" n="HIAT:w" s="T387">ke</ts>
                  <nts id="Seg_2504" n="HIAT:ip">.</nts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_2507" n="HIAT:u" s="T388">
                  <nts id="Seg_2508" n="HIAT:ip">"</nts>
                  <ts e="T389" id="Seg_2510" n="HIAT:w" s="T388">Togo</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_2513" n="HIAT:w" s="T389">du</ts>
                  <nts id="Seg_2514" n="HIAT:ip">?</nts>
                  <nts id="Seg_2515" n="HIAT:ip">"</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_2518" n="HIAT:u" s="T390">
                  <nts id="Seg_2519" n="HIAT:ip">"</nts>
                  <ts e="T391" id="Seg_2521" n="HIAT:w" s="T390">Kaja</ts>
                  <nts id="Seg_2522" n="HIAT:ip">,</nts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_2525" n="HIAT:w" s="T391">aldʼatalɨːllar</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_2528" n="HIAT:w" s="T392">Ruslaːnav</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_2531" n="HIAT:w" s="T393">palačinkalarɨn</ts>
                  <nts id="Seg_2532" n="HIAT:ip">,</nts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_2535" n="HIAT:w" s="T394">min</ts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_2538" n="HIAT:w" s="T395">biːrkeːmmin</ts>
                  <nts id="Seg_2539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_2541" n="HIAT:w" s="T396">kisteːbitim</ts>
                  <nts id="Seg_2542" n="HIAT:ip">"</nts>
                  <nts id="Seg_2543" n="HIAT:ip">,</nts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_2546" n="HIAT:w" s="T397">diːr</ts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_2549" n="HIAT:w" s="T398">dogorum</ts>
                  <nts id="Seg_2550" n="HIAT:ip">.</nts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_2553" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_2555" n="HIAT:w" s="T399">Dʼe</ts>
                  <nts id="Seg_2556" n="HIAT:ip">,</nts>
                  <nts id="Seg_2557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_2559" n="HIAT:w" s="T400">ol</ts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_2562" n="HIAT:w" s="T401">ikki͡en</ts>
                  <nts id="Seg_2563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_2565" n="HIAT:w" s="T402">kistiːbit</ts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_2568" n="HIAT:w" s="T403">diː</ts>
                  <nts id="Seg_2569" n="HIAT:ip">.</nts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T421" id="Seg_2571" n="sc" s="T408">
               <ts e="T412" id="Seg_2573" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_2575" n="HIAT:w" s="T408">Anna</ts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_2578" n="HIAT:w" s="T409">ete</ts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_2581" n="HIAT:w" s="T410">pakojnaja</ts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_2584" n="HIAT:w" s="T411">bu</ts>
                  <nts id="Seg_2585" n="HIAT:ip">.</nts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_2588" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_2590" n="HIAT:w" s="T412">Nʼina</ts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_2593" n="HIAT:w" s="T413">inʼete</ts>
                  <nts id="Seg_2594" n="HIAT:ip">,</nts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_2597" n="HIAT:w" s="T414">bu</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_2600" n="HIAT:w" s="T415">Goru͡o</ts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_2603" n="HIAT:w" s="T416">Nʼina</ts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_2606" n="HIAT:w" s="T417">inʼete</ts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_2609" n="HIAT:w" s="T418">ete</ts>
                  <nts id="Seg_2610" n="HIAT:ip">,</nts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_2613" n="HIAT:w" s="T419">Anna</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_2616" n="HIAT:w" s="T420">pakojna</ts>
                  <nts id="Seg_2617" n="HIAT:ip">.</nts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T448" id="Seg_2619" n="sc" s="T429">
               <ts e="T434" id="Seg_2621" n="HIAT:u" s="T429">
                  <ts e="T431" id="Seg_2623" n="HIAT:w" s="T429">Haka</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_2626" n="HIAT:w" s="T431">kihilere</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_2629" n="HIAT:w" s="T432">etilere</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_2632" n="HIAT:w" s="T433">bihi͡ettere</ts>
                  <nts id="Seg_2633" n="HIAT:ip">.</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_2636" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_2638" n="HIAT:w" s="T434">Hakalar</ts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_2641" n="HIAT:w" s="T435">etiler</ts>
                  <nts id="Seg_2642" n="HIAT:ip">.</nts>
                  <nts id="Seg_2643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_2645" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_2647" n="HIAT:w" s="T436">Nu</ts>
                  <nts id="Seg_2648" n="HIAT:ip">,</nts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_2651" n="HIAT:w" s="T437">ranʼse</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_2654" n="HIAT:w" s="T438">znaješʼ</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_2657" n="HIAT:w" s="T439">kak</ts>
                  <nts id="Seg_2658" n="HIAT:ip">,</nts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_2661" n="HIAT:w" s="T440">vsʼakaje</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_2664" n="HIAT:w" s="T441">bɨla</ts>
                  <nts id="Seg_2665" n="HIAT:ip">,</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_2668" n="HIAT:w" s="T442">harak</ts>
                  <nts id="Seg_2669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_2671" n="HIAT:w" s="T443">nʼuːččamsɨjaːččɨ</ts>
                  <nts id="Seg_2672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_2674" n="HIAT:w" s="T444">ete</ts>
                  <nts id="Seg_2675" n="HIAT:ip">,</nts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_2678" n="HIAT:w" s="T445">horok</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_2681" n="HIAT:w" s="T446">takʼie</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_2684" n="HIAT:w" s="T447">bɨlʼi</ts>
                  <nts id="Seg_2685" n="HIAT:ip">.</nts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T480" id="Seg_2687" n="sc" s="T459">
               <ts e="T466" id="Seg_2689" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_2691" n="HIAT:w" s="T459">Aragiːnɨ</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_2694" n="HIAT:w" s="T460">da</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_2697" n="HIAT:w" s="T461">ispetegim</ts>
                  <nts id="Seg_2698" n="HIAT:ip">,</nts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2700" n="HIAT:ip">(</nts>
                  <nts id="Seg_2701" n="HIAT:ip">(</nts>
                  <ats e="T280" id="Seg_2702" n="HIAT:non-pho" s="T462">…</ats>
                  <nts id="Seg_2703" n="HIAT:ip">)</nts>
                  <nts id="Seg_2704" n="HIAT:ip">)</nts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_2707" n="HIAT:w" s="T280">üjege</ts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_2710" n="HIAT:w" s="T463">da</ts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_2713" n="HIAT:w" s="T464">ihispetegim</ts>
                  <nts id="Seg_2714" n="HIAT:ip">.</nts>
                  <nts id="Seg_2715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_2717" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_2719" n="HIAT:w" s="T466">Kɨrdʼɨ͡akpar</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_2722" n="HIAT:w" s="T467">di͡eri</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_2725" n="HIAT:w" s="T468">itiččeleːgi</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_2728" n="HIAT:w" s="T469">karajbɨtɨm</ts>
                  <nts id="Seg_2729" n="HIAT:ip">,</nts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_2732" n="HIAT:w" s="T470">biːr</ts>
                  <nts id="Seg_2733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_2735" n="HIAT:w" s="T471">ürümkanɨ</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_2738" n="HIAT:w" s="T472">ispetegim</ts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_2741" n="HIAT:w" s="T473">kaččaga</ts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_2744" n="HIAT:w" s="T474">da</ts>
                  <nts id="Seg_2745" n="HIAT:ip">.</nts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_2748" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_2750" n="HIAT:w" s="T475">Bi͡ek</ts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2752" n="HIAT:ip">(</nts>
                  <ts e="T477" id="Seg_2754" n="HIAT:w" s="T476">is-</ts>
                  <nts id="Seg_2755" n="HIAT:ip">)</nts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_2758" n="HIAT:w" s="T477">ihimmije</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_2761" n="HIAT:w" s="T478">hɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_2764" n="HIAT:w" s="T479">bi͡ek</ts>
                  <nts id="Seg_2765" n="HIAT:ip">.</nts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-LaVN">
            <ts e="T80" id="Seg_2767" n="sc" s="T4">
               <ts e="T5" id="Seg_2769" n="e" s="T4">ɨbɨːj, </ts>
               <ts e="T6" id="Seg_2771" n="e" s="T5">ol </ts>
               <ts e="T7" id="Seg_2773" n="e" s="T6">ɨrɨ͡alarɨ </ts>
               <ts e="T8" id="Seg_2775" n="e" s="T7">ol </ts>
               <ts e="T9" id="Seg_2777" n="e" s="T8">ke </ts>
               <ts e="T10" id="Seg_2779" n="e" s="T9">kimi </ts>
               <ts e="T11" id="Seg_2781" n="e" s="T10">ile </ts>
               <ts e="T12" id="Seg_2783" n="e" s="T11">dʼe </ts>
               <ts e="T14" id="Seg_2785" n="e" s="T12">bihi͡eke… </ts>
               <ts e="T15" id="Seg_2787" n="e" s="T14">Stadaga </ts>
               <ts e="T16" id="Seg_2789" n="e" s="T15">olorobut </ts>
               <ts e="T17" id="Seg_2791" n="e" s="T16">bu͡olla </ts>
               <ts e="T18" id="Seg_2793" n="e" s="T17">bihigi </ts>
               <ts e="T19" id="Seg_2795" n="e" s="T18">tabannan </ts>
               <ts e="T20" id="Seg_2797" n="e" s="T19">ke, </ts>
               <ts e="T21" id="Seg_2799" n="e" s="T20">tɨ͡aga </ts>
               <ts e="T22" id="Seg_2801" n="e" s="T21">ke </ts>
               <ts e="T23" id="Seg_2803" n="e" s="T22">olorobut. </ts>
               <ts e="T24" id="Seg_2805" n="e" s="T23">Kamsamol </ts>
               <ts e="T25" id="Seg_2807" n="e" s="T24">keler, </ts>
               <ts e="T26" id="Seg_2809" n="e" s="T25">kamsamol, </ts>
               <ts e="T27" id="Seg_2811" n="e" s="T26">tagda </ts>
               <ts e="T28" id="Seg_2813" n="e" s="T27">dʼe </ts>
               <ts e="T29" id="Seg_2815" n="e" s="T28">ɨbɨːjdar </ts>
               <ts e="T30" id="Seg_2817" n="e" s="T29">kamsamol </ts>
               <ts e="T31" id="Seg_2819" n="e" s="T30">dʼe </ts>
               <ts e="T32" id="Seg_2821" n="e" s="T31">ile </ts>
               <ts e="T33" id="Seg_2823" n="e" s="T32">bu͡opsa. </ts>
               <ts e="T34" id="Seg_2825" n="e" s="T33">Partʼejnajdar </ts>
               <ts e="T35" id="Seg_2827" n="e" s="T34">kelliler. </ts>
               <ts e="T36" id="Seg_2829" n="e" s="T35">"Aha, </ts>
               <ts e="T37" id="Seg_2831" n="e" s="T36">ɨbɨ͡aj", </ts>
               <ts e="T38" id="Seg_2833" n="e" s="T37">diːller </ts>
               <ts e="T39" id="Seg_2835" n="e" s="T38">miːgin, </ts>
               <ts e="T40" id="Seg_2837" n="e" s="T39">"kaja, </ts>
               <ts e="T41" id="Seg_2839" n="e" s="T40">Barbaː, </ts>
               <ts e="T42" id="Seg_2841" n="e" s="T41">palačinkalarɨŋ </ts>
               <ts e="T43" id="Seg_2843" n="e" s="T42">baːllar?" </ts>
               <ts e="T44" id="Seg_2845" n="e" s="T43">Min </ts>
               <ts e="T45" id="Seg_2847" n="e" s="T44">diːbin: </ts>
               <ts e="T46" id="Seg_2849" n="e" s="T45">"Hu͡ok, </ts>
               <ts e="T47" id="Seg_2851" n="e" s="T46">Ruslan </ts>
               <ts e="T48" id="Seg_2853" n="e" s="T47">gi͡ettere." </ts>
               <ts e="T49" id="Seg_2855" n="e" s="T48">Huːttaːbɨttara </ts>
               <ts e="T50" id="Seg_2857" n="e" s="T49">ginini, </ts>
               <ts e="T51" id="Seg_2859" n="e" s="T50">di͡ebittere </ts>
               <ts e="T53" id="Seg_2861" n="e" s="T51">"kimneːŋ, </ts>
               <ts e="T56" id="Seg_2863" n="e" s="T53">aldʼatalaːŋ </ts>
               <ts e="T57" id="Seg_2865" n="e" s="T56">gini </ts>
               <ts e="T58" id="Seg_2867" n="e" s="T57">palačinkatɨn." </ts>
               <ts e="T59" id="Seg_2869" n="e" s="T58">Bu </ts>
               <ts e="T60" id="Seg_2871" n="e" s="T59">tɨ͡a </ts>
               <ts e="T61" id="Seg_2873" n="e" s="T60">kihitiger </ts>
               <ts e="T62" id="Seg_2875" n="e" s="T61">barbɨttara </ts>
               <ts e="T64" id="Seg_2877" n="e" s="T62">"(palačin-) </ts>
               <ts e="T65" id="Seg_2879" n="e" s="T64">barɨta </ts>
               <ts e="T66" id="Seg_2881" n="e" s="T65">aldʼatalaːŋ", </ts>
               <ts e="T67" id="Seg_2883" n="e" s="T66">di͡ebittere. </ts>
               <ts e="T68" id="Seg_2885" n="e" s="T67">Min </ts>
               <ts e="T69" id="Seg_2887" n="e" s="T68">diːbin: </ts>
               <ts e="T70" id="Seg_2889" n="e" s="T69">"Hu͡ok </ts>
               <ts e="T71" id="Seg_2891" n="e" s="T70">mini͡eke, </ts>
               <ts e="T72" id="Seg_2893" n="e" s="T71">biːr </ts>
               <ts e="T73" id="Seg_2895" n="e" s="T72">daː </ts>
               <ts e="T74" id="Seg_2897" n="e" s="T73">hu͡ok." </ts>
               <ts e="T75" id="Seg_2899" n="e" s="T74">"Tuːgu </ts>
               <ts e="T76" id="Seg_2901" n="e" s="T75">ɨllatagɨn </ts>
               <ts e="T77" id="Seg_2903" n="e" s="T76">iti </ts>
               <ts e="T78" id="Seg_2905" n="e" s="T77">patʼefoŋŋar", </ts>
               <ts e="T79" id="Seg_2907" n="e" s="T78">diːller </ts>
               <ts e="T80" id="Seg_2909" n="e" s="T79">minigin. </ts>
            </ts>
            <ts e="T89" id="Seg_2910" n="sc" s="T82">
               <ts e="T84" id="Seg_2912" n="e" s="T82">Heː, </ts>
               <ts e="T85" id="Seg_2914" n="e" s="T84">ol </ts>
               <ts e="T86" id="Seg_2916" n="e" s="T85">ke </ts>
               <ts e="T87" id="Seg_2918" n="e" s="T86">ɨllatabɨn </ts>
               <ts e="T88" id="Seg_2920" n="e" s="T87">bejem </ts>
               <ts e="T89" id="Seg_2922" n="e" s="T88">ke. </ts>
            </ts>
            <ts e="T132" id="Seg_2923" n="sc" s="T92">
               <ts e="T94" id="Seg_2925" n="e" s="T92">Min </ts>
               <ts e="T95" id="Seg_2927" n="e" s="T94">bejem </ts>
               <ts e="T96" id="Seg_2929" n="e" s="T95">ɨlɨmmɨtɨm. </ts>
               <ts e="T97" id="Seg_2931" n="e" s="T96">Heː, </ts>
               <ts e="T98" id="Seg_2933" n="e" s="T97">Čornajga </ts>
               <ts e="T99" id="Seg_2935" n="e" s="T98">hɨldʼammɨn </ts>
               <ts e="T100" id="Seg_2937" n="e" s="T99">ke </ts>
               <ts e="T101" id="Seg_2939" n="e" s="T100">oččogo. </ts>
               <ts e="T102" id="Seg_2941" n="e" s="T101">Bejem </ts>
               <ts e="T103" id="Seg_2943" n="e" s="T102">ɨlɨmmɨtɨm </ts>
               <ts e="T104" id="Seg_2945" n="e" s="T103">ol. </ts>
               <ts e="T105" id="Seg_2947" n="e" s="T104">Bejem </ts>
               <ts e="T106" id="Seg_2949" n="e" s="T105">ɨlan </ts>
               <ts e="T107" id="Seg_2951" n="e" s="T106">baraːn, </ts>
               <ts e="T108" id="Seg_2953" n="e" s="T107">ontuŋ </ts>
               <ts e="T109" id="Seg_2955" n="e" s="T108">inneleːk, </ts>
               <ts e="T110" id="Seg_2957" n="e" s="T109">tuspa </ts>
               <ts e="T111" id="Seg_2959" n="e" s="T110">korobkakaːn </ts>
               <ts e="T112" id="Seg_2961" n="e" s="T111">ki͡e, </ts>
               <ts e="T113" id="Seg_2963" n="e" s="T112">onu </ts>
               <ts e="T114" id="Seg_2965" n="e" s="T113">bejebit </ts>
               <ts e="T115" id="Seg_2967" n="e" s="T114">erijebit. </ts>
               <ts e="T116" id="Seg_2969" n="e" s="T115">Onton </ts>
               <ts e="T117" id="Seg_2971" n="e" s="T116">palačinkanɨ </ts>
               <ts e="T118" id="Seg_2973" n="e" s="T117">uːrabɨt, </ts>
               <ts e="T119" id="Seg_2975" n="e" s="T118">ol </ts>
               <ts e="T121" id="Seg_2977" n="e" s="T119">(ɨll-)… </ts>
               <ts e="T122" id="Seg_2979" n="e" s="T121">Dʼe </ts>
               <ts e="T123" id="Seg_2981" n="e" s="T122">"Balʼenka, </ts>
               <ts e="T124" id="Seg_2983" n="e" s="T123">balʼenka" </ts>
               <ts e="T125" id="Seg_2985" n="e" s="T124">dva </ts>
               <ts e="T126" id="Seg_2987" n="e" s="T125">bɨla </ts>
               <ts e="T127" id="Seg_2989" n="e" s="T126">mini͡e </ts>
               <ts e="T129" id="Seg_2991" n="e" s="T127">(plastin-) </ts>
               <ts e="T130" id="Seg_2993" n="e" s="T129">bosku͡oj </ts>
               <ts e="T131" id="Seg_2995" n="e" s="T130">mɨnʼaː </ts>
               <ts e="T132" id="Seg_2997" n="e" s="T131">ɨrɨ͡alaːk. </ts>
            </ts>
            <ts e="T169" id="Seg_2998" n="sc" s="T135">
               <ts e="T136" id="Seg_3000" n="e" s="T135">Aha, </ts>
               <ts e="T137" id="Seg_3002" n="e" s="T136">Ruslaːnava </ts>
               <ts e="T138" id="Seg_3004" n="e" s="T137">ɨllɨːr: </ts>
               <ts e="T139" id="Seg_3006" n="e" s="T138">"Balʼenka, </ts>
               <ts e="T140" id="Seg_3008" n="e" s="T139">balʼenka </ts>
               <ts e="T141" id="Seg_3010" n="e" s="T140">passɨtaja" </ts>
               <ts e="T142" id="Seg_3012" n="e" s="T141">bu͡ol, </ts>
               <ts e="T143" id="Seg_3014" n="e" s="T142">dʼe, </ts>
               <ts e="T144" id="Seg_3016" n="e" s="T143">"ontubun </ts>
               <ts e="T145" id="Seg_3018" n="e" s="T144">dʼe </ts>
               <ts e="T146" id="Seg_3020" n="e" s="T145">kisteː" </ts>
               <ts e="T147" id="Seg_3022" n="e" s="T146">diːn. </ts>
               <ts e="T148" id="Seg_3024" n="e" s="T147">Ogobor </ts>
               <ts e="T149" id="Seg_3026" n="e" s="T148">kim, </ts>
               <ts e="T150" id="Seg_3028" n="e" s="T149">periːnekeːn </ts>
               <ts e="T151" id="Seg_3030" n="e" s="T150">obu͡oja </ts>
               <ts e="T152" id="Seg_3032" n="e" s="T151">oŋorbutum. </ts>
               <ts e="T153" id="Seg_3034" n="e" s="T152">Kuččuguj </ts>
               <ts e="T154" id="Seg_3036" n="e" s="T153">ogo. </ts>
               <ts e="T155" id="Seg_3038" n="e" s="T154">Ol </ts>
               <ts e="T156" id="Seg_3040" n="e" s="T155">ihinen </ts>
               <ts e="T157" id="Seg_3042" n="e" s="T156">tigen </ts>
               <ts e="T158" id="Seg_3044" n="e" s="T157">keːhen </ts>
               <ts e="T159" id="Seg_3046" n="e" s="T158">baraːn </ts>
               <ts e="T160" id="Seg_3048" n="e" s="T159">onno </ts>
               <ts e="T162" id="Seg_3050" n="e" s="T160">((…)) </ts>
               <ts e="T161" id="Seg_3052" n="e" s="T162">onno </ts>
               <ts e="T163" id="Seg_3054" n="e" s="T161">uːrabɨn. </ts>
               <ts e="T164" id="Seg_3056" n="e" s="T163">Ikki </ts>
               <ts e="T165" id="Seg_3058" n="e" s="T164">palačinkanɨ </ts>
               <ts e="T166" id="Seg_3060" n="e" s="T165">kisteːbitim. </ts>
               <ts e="T167" id="Seg_3062" n="e" s="T166">Hanɨga </ts>
               <ts e="T168" id="Seg_3064" n="e" s="T167">di͡eri </ts>
               <ts e="T169" id="Seg_3066" n="e" s="T168">kisteːbitim. </ts>
            </ts>
            <ts e="T181" id="Seg_3067" n="sc" s="T171">
               <ts e="T173" id="Seg_3069" n="e" s="T171">Ruslaːnava </ts>
               <ts e="T174" id="Seg_3071" n="e" s="T173">gi͡etterin. </ts>
               <ts e="T175" id="Seg_3073" n="e" s="T174">Oːj </ts>
               <ts e="T176" id="Seg_3075" n="e" s="T175">ile </ts>
               <ts e="T177" id="Seg_3077" n="e" s="T176">kakʼije </ts>
               <ts e="T178" id="Seg_3079" n="e" s="T177">bɨlʼi </ts>
               <ts e="T179" id="Seg_3081" n="e" s="T178">ile </ts>
               <ts e="T181" id="Seg_3083" n="e" s="T179">(za-)… </ts>
            </ts>
            <ts e="T202" id="Seg_3084" n="sc" s="T185">
               <ts e="T187" id="Seg_3086" n="e" s="T185">Ol </ts>
               <ts e="T188" id="Seg_3088" n="e" s="T187">palačinkalarbɨn </ts>
               <ts e="T189" id="Seg_3090" n="e" s="T188">bu </ts>
               <ts e="T190" id="Seg_3092" n="e" s="T189">ogolor, </ts>
               <ts e="T191" id="Seg_3094" n="e" s="T190">ulaːtan </ts>
               <ts e="T192" id="Seg_3096" n="e" s="T191">baraːnnar </ts>
               <ts e="T193" id="Seg_3098" n="e" s="T192">čeŋkičči </ts>
               <ts e="T194" id="Seg_3100" n="e" s="T193">aldʼatalaːbɨttar </ts>
               <ts e="T195" id="Seg_3102" n="e" s="T194">bu͡o. </ts>
               <ts e="T196" id="Seg_3104" n="e" s="T195">Bu </ts>
               <ts e="T197" id="Seg_3106" n="e" s="T196">nʼečʼajanno </ts>
               <ts e="T198" id="Seg_3108" n="e" s="T197">Lʼusʼa </ts>
               <ts e="T199" id="Seg_3110" n="e" s="T198">ol </ts>
               <ts e="T200" id="Seg_3112" n="e" s="T199">хazʼaina </ts>
               <ts e="T201" id="Seg_3114" n="e" s="T200">et, </ts>
               <ts e="T202" id="Seg_3116" n="e" s="T201">barɨtɨn. </ts>
            </ts>
            <ts e="T281" id="Seg_3117" n="sc" s="T214">
               <ts e="T216" id="Seg_3119" n="e" s="T214">Oččogo </ts>
               <ts e="T217" id="Seg_3121" n="e" s="T216">Stalʼinɨŋ </ts>
               <ts e="T218" id="Seg_3123" n="e" s="T217">ete </ts>
               <ts e="T219" id="Seg_3125" n="e" s="T218">bu͡olla, </ts>
               <ts e="T220" id="Seg_3127" n="e" s="T219">heː, </ts>
               <ts e="T221" id="Seg_3129" n="e" s="T220">oččogo, </ts>
               <ts e="T222" id="Seg_3131" n="e" s="T221">ol </ts>
               <ts e="T223" id="Seg_3133" n="e" s="T222">(huːt-). </ts>
               <ts e="T224" id="Seg_3135" n="e" s="T223">Ol </ts>
               <ts e="T225" id="Seg_3137" n="e" s="T224">kenni </ts>
               <ts e="T226" id="Seg_3139" n="e" s="T225">ginini </ts>
               <ts e="T227" id="Seg_3141" n="e" s="T226">huːttaːbɨttar, </ts>
               <ts e="T228" id="Seg_3143" n="e" s="T227">ginini </ts>
               <ts e="T229" id="Seg_3145" n="e" s="T228">ol. </ts>
               <ts e="T230" id="Seg_3147" n="e" s="T229">Patamusta, </ts>
               <ts e="T231" id="Seg_3149" n="e" s="T230">noː, </ts>
               <ts e="T234" id="Seg_3151" n="e" s="T231">tagda-to </ts>
               <ts e="T235" id="Seg_3153" n="e" s="T234">vajna </ts>
               <ts e="T236" id="Seg_3155" n="e" s="T235">bɨla. </ts>
               <ts e="T238" id="Seg_3157" n="e" s="T236">Onʼi-ta </ts>
               <ts e="T239" id="Seg_3159" n="e" s="T238">pʼesnʼi </ts>
               <ts e="T240" id="Seg_3161" n="e" s="T239">pʼeʼli </ts>
               <ts e="T244" id="Seg_3163" n="e" s="T240">хadʼilʼi-ta. </ts>
               <ts e="T245" id="Seg_3165" n="e" s="T244">Oččogo </ts>
               <ts e="T246" id="Seg_3167" n="e" s="T245">gini </ts>
               <ts e="T247" id="Seg_3169" n="e" s="T246">bajnaːga </ts>
               <ts e="T248" id="Seg_3171" n="e" s="T247">ol, </ts>
               <ts e="T249" id="Seg_3173" n="e" s="T248">barar </ts>
               <ts e="T250" id="Seg_3175" n="e" s="T249">da </ts>
               <ts e="T251" id="Seg_3177" n="e" s="T250">vajnaːga </ts>
               <ts e="T252" id="Seg_3179" n="e" s="T251">ke </ts>
               <ts e="T253" id="Seg_3181" n="e" s="T252">ɨllatallar </ts>
               <ts e="T254" id="Seg_3183" n="e" s="T253">diː </ts>
               <ts e="T255" id="Seg_3185" n="e" s="T254">artʼiːstarɨ </ts>
               <ts e="T256" id="Seg_3187" n="e" s="T255">ke. </ts>
               <ts e="T380" id="Seg_3189" n="e" s="T256">No </ts>
               <ts e="T257" id="Seg_3191" n="e" s="T380">ana </ts>
               <ts e="T258" id="Seg_3193" n="e" s="T257">pa </ts>
               <ts e="T259" id="Seg_3195" n="e" s="T258">starastʼi </ts>
               <ts e="T262" id="Seg_3197" n="e" s="T259">kakʼije-ta </ts>
               <ts e="T263" id="Seg_3199" n="e" s="T262">pʼesnʼi </ts>
               <ts e="T264" id="Seg_3201" n="e" s="T263">pʼela, </ts>
               <ts e="T265" id="Seg_3203" n="e" s="T264">ol </ts>
               <ts e="T266" id="Seg_3205" n="e" s="T265">ihin </ts>
               <ts e="T267" id="Seg_3207" n="e" s="T266">gilleri </ts>
               <ts e="T268" id="Seg_3209" n="e" s="T267">huːttuːllar, </ts>
               <ts e="T269" id="Seg_3211" n="e" s="T268">oː, </ts>
               <ts e="T271" id="Seg_3213" n="e" s="T269">muŋnaːktarɨ. </ts>
               <ts e="T272" id="Seg_3215" n="e" s="T271">Erin </ts>
               <ts e="T273" id="Seg_3217" n="e" s="T272">gɨtta </ts>
               <ts e="T274" id="Seg_3219" n="e" s="T273">ikki͡ennerin </ts>
               <ts e="T275" id="Seg_3221" n="e" s="T274">huːttaːbɨttara </ts>
               <ts e="T276" id="Seg_3223" n="e" s="T275">eni. </ts>
               <ts e="T243" id="Seg_3225" n="e" s="T276">Takʼie </ts>
               <ts e="T277" id="Seg_3227" n="e" s="T243">bɨlʼi </ts>
               <ts e="T278" id="Seg_3229" n="e" s="T277">anʼi </ts>
               <ts e="T279" id="Seg_3231" n="e" s="T278">ranse </ts>
               <ts e="T281" id="Seg_3233" n="e" s="T279">lʼudʼi. </ts>
            </ts>
            <ts e="T319" id="Seg_3234" n="sc" s="T293">
               <ts e="T294" id="Seg_3236" n="e" s="T293">Dʼe </ts>
               <ts e="T295" id="Seg_3238" n="e" s="T294">ol </ts>
               <ts e="T296" id="Seg_3240" n="e" s="T295">ɨrɨ͡alarɨ </ts>
               <ts e="T297" id="Seg_3242" n="e" s="T296">ihilleːmi͡ekke </ts>
               <ts e="T298" id="Seg_3244" n="e" s="T297">ginner, </ts>
               <ts e="T299" id="Seg_3246" n="e" s="T298">prosta </ts>
               <ts e="T300" id="Seg_3248" n="e" s="T299">ginner </ts>
               <ts e="T301" id="Seg_3250" n="e" s="T300">tu͡ogu </ts>
               <ts e="T302" id="Seg_3252" n="e" s="T301">ere, </ts>
               <ts e="T303" id="Seg_3254" n="e" s="T302">no, </ts>
               <ts e="T304" id="Seg_3256" n="e" s="T303">kak </ts>
               <ts e="T305" id="Seg_3258" n="e" s="T304">ta </ts>
               <ts e="T306" id="Seg_3260" n="e" s="T305">brʼedʼitelʼ </ts>
               <ts e="T307" id="Seg_3262" n="e" s="T306">li, </ts>
               <ts e="T308" id="Seg_3264" n="e" s="T307">čʼo </ts>
               <ts e="T309" id="Seg_3266" n="e" s="T308">li, </ts>
               <ts e="T310" id="Seg_3268" n="e" s="T309">kakoj </ts>
               <ts e="T311" id="Seg_3270" n="e" s="T310">li, </ts>
               <ts e="T312" id="Seg_3272" n="e" s="T311">oččogo </ts>
               <ts e="T313" id="Seg_3274" n="e" s="T312">ol </ts>
               <ts e="T314" id="Seg_3276" n="e" s="T313">iti </ts>
               <ts e="T315" id="Seg_3278" n="e" s="T314">ol </ts>
               <ts e="T316" id="Seg_3280" n="e" s="T315">bihi͡ettere </ts>
               <ts e="T317" id="Seg_3282" n="e" s="T316">ol </ts>
               <ts e="T318" id="Seg_3284" n="e" s="T317">uhuttuːllar </ts>
               <ts e="T319" id="Seg_3286" n="e" s="T318">ol. </ts>
            </ts>
            <ts e="T344" id="Seg_3287" n="sc" s="T329">
               <ts e="T330" id="Seg_3289" n="e" s="T329">Dʼe, </ts>
               <ts e="T331" id="Seg_3291" n="e" s="T330">eni </ts>
               <ts e="T332" id="Seg_3293" n="e" s="T331">ke, </ts>
               <ts e="T333" id="Seg_3295" n="e" s="T332">dʼe, </ts>
               <ts e="T334" id="Seg_3297" n="e" s="T333">ol </ts>
               <ts e="T335" id="Seg_3299" n="e" s="T334">dʼe </ts>
               <ts e="T336" id="Seg_3301" n="e" s="T335">uhulaktaːn </ts>
               <ts e="T337" id="Seg_3303" n="e" s="T336">keːheller, </ts>
               <ts e="T338" id="Seg_3305" n="e" s="T337">ol </ts>
               <ts e="T339" id="Seg_3307" n="e" s="T338">kistiːbit </ts>
               <ts e="T340" id="Seg_3309" n="e" s="T339">dʼe </ts>
               <ts e="T341" id="Seg_3311" n="e" s="T340">ontugun, </ts>
               <ts e="T342" id="Seg_3313" n="e" s="T341">oj, </ts>
               <ts e="T343" id="Seg_3315" n="e" s="T342">takaja </ts>
               <ts e="T344" id="Seg_3317" n="e" s="T343">ile. </ts>
            </ts>
            <ts e="T350" id="Seg_3318" n="sc" s="T346">
               <ts e="T348" id="Seg_3320" n="e" s="T346">De, </ts>
               <ts e="T349" id="Seg_3322" n="e" s="T348">anɨ </ts>
               <ts e="T350" id="Seg_3324" n="e" s="T349">öjdöːtökkö. </ts>
            </ts>
            <ts e="T365" id="Seg_3325" n="sc" s="T355">
               <ts e="T357" id="Seg_3327" n="e" s="T355">No, </ts>
               <ts e="T358" id="Seg_3329" n="e" s="T357">kihi </ts>
               <ts e="T359" id="Seg_3331" n="e" s="T358">kuttanɨ͡ak </ts>
               <ts e="T360" id="Seg_3333" n="e" s="T359">bu͡olu͡o, </ts>
               <ts e="T361" id="Seg_3335" n="e" s="T360">oččogo </ts>
               <ts e="T362" id="Seg_3337" n="e" s="T361">keleller, </ts>
               <ts e="T363" id="Seg_3339" n="e" s="T362">aldʼatalɨːllar </ts>
               <ts e="T364" id="Seg_3341" n="e" s="T363">ke, </ts>
               <ts e="T365" id="Seg_3343" n="e" s="T364">keleller. </ts>
            </ts>
            <ts e="T404" id="Seg_3344" n="sc" s="T370">
               <ts e="T372" id="Seg_3346" n="e" s="T370">Nuː, </ts>
               <ts e="T373" id="Seg_3348" n="e" s="T372">mini͡ene </ts>
               <ts e="T374" id="Seg_3350" n="e" s="T373">padruskam </ts>
               <ts e="T375" id="Seg_3352" n="e" s="T374">bɨla </ts>
               <ts e="T376" id="Seg_3354" n="e" s="T375">ana. </ts>
               <ts e="T270" id="Seg_3356" n="e" s="T376">Ana </ts>
               <ts e="T377" id="Seg_3358" n="e" s="T270">v </ts>
               <ts e="T378" id="Seg_3360" n="e" s="T377">gorat, </ts>
               <ts e="T379" id="Seg_3362" n="e" s="T378">ɨbɨːj, </ts>
               <ts e="T381" id="Seg_3364" n="e" s="T379">barbat. </ts>
               <ts e="T382" id="Seg_3366" n="e" s="T381">"Öllübüt </ts>
               <ts e="T383" id="Seg_3368" n="e" s="T382">agaj, </ts>
               <ts e="T384" id="Seg_3370" n="e" s="T383">kimŋin </ts>
               <ts e="T385" id="Seg_3372" n="e" s="T384">kisteː", </ts>
               <ts e="T386" id="Seg_3374" n="e" s="T385">diːr </ts>
               <ts e="T387" id="Seg_3376" n="e" s="T386">minigin </ts>
               <ts e="T388" id="Seg_3378" n="e" s="T387">ke. </ts>
               <ts e="T389" id="Seg_3380" n="e" s="T388">"Togo </ts>
               <ts e="T390" id="Seg_3382" n="e" s="T389">du?" </ts>
               <ts e="T391" id="Seg_3384" n="e" s="T390">"Kaja, </ts>
               <ts e="T392" id="Seg_3386" n="e" s="T391">aldʼatalɨːllar </ts>
               <ts e="T393" id="Seg_3388" n="e" s="T392">Ruslaːnav </ts>
               <ts e="T394" id="Seg_3390" n="e" s="T393">palačinkalarɨn, </ts>
               <ts e="T395" id="Seg_3392" n="e" s="T394">min </ts>
               <ts e="T396" id="Seg_3394" n="e" s="T395">biːrkeːmmin </ts>
               <ts e="T397" id="Seg_3396" n="e" s="T396">kisteːbitim", </ts>
               <ts e="T398" id="Seg_3398" n="e" s="T397">diːr </ts>
               <ts e="T399" id="Seg_3400" n="e" s="T398">dogorum. </ts>
               <ts e="T400" id="Seg_3402" n="e" s="T399">Dʼe, </ts>
               <ts e="T401" id="Seg_3404" n="e" s="T400">ol </ts>
               <ts e="T402" id="Seg_3406" n="e" s="T401">ikki͡en </ts>
               <ts e="T403" id="Seg_3408" n="e" s="T402">kistiːbit </ts>
               <ts e="T404" id="Seg_3410" n="e" s="T403">diː. </ts>
            </ts>
            <ts e="T421" id="Seg_3411" n="sc" s="T408">
               <ts e="T409" id="Seg_3413" n="e" s="T408">Anna </ts>
               <ts e="T410" id="Seg_3415" n="e" s="T409">ete </ts>
               <ts e="T411" id="Seg_3417" n="e" s="T410">pakojnaja </ts>
               <ts e="T412" id="Seg_3419" n="e" s="T411">bu. </ts>
               <ts e="T413" id="Seg_3421" n="e" s="T412">Nʼina </ts>
               <ts e="T414" id="Seg_3423" n="e" s="T413">inʼete, </ts>
               <ts e="T415" id="Seg_3425" n="e" s="T414">bu </ts>
               <ts e="T416" id="Seg_3427" n="e" s="T415">Goru͡o </ts>
               <ts e="T417" id="Seg_3429" n="e" s="T416">Nʼina </ts>
               <ts e="T418" id="Seg_3431" n="e" s="T417">inʼete </ts>
               <ts e="T419" id="Seg_3433" n="e" s="T418">ete, </ts>
               <ts e="T420" id="Seg_3435" n="e" s="T419">Anna </ts>
               <ts e="T421" id="Seg_3437" n="e" s="T420">pakojna. </ts>
            </ts>
            <ts e="T448" id="Seg_3438" n="sc" s="T429">
               <ts e="T431" id="Seg_3440" n="e" s="T429">Haka </ts>
               <ts e="T432" id="Seg_3442" n="e" s="T431">kihilere </ts>
               <ts e="T433" id="Seg_3444" n="e" s="T432">etilere </ts>
               <ts e="T434" id="Seg_3446" n="e" s="T433">bihi͡ettere. </ts>
               <ts e="T435" id="Seg_3448" n="e" s="T434">Hakalar </ts>
               <ts e="T436" id="Seg_3450" n="e" s="T435">etiler. </ts>
               <ts e="T437" id="Seg_3452" n="e" s="T436">Nu, </ts>
               <ts e="T438" id="Seg_3454" n="e" s="T437">ranʼse </ts>
               <ts e="T439" id="Seg_3456" n="e" s="T438">znaješʼ </ts>
               <ts e="T440" id="Seg_3458" n="e" s="T439">kak, </ts>
               <ts e="T441" id="Seg_3460" n="e" s="T440">vsʼakaje </ts>
               <ts e="T442" id="Seg_3462" n="e" s="T441">bɨla, </ts>
               <ts e="T443" id="Seg_3464" n="e" s="T442">harak </ts>
               <ts e="T444" id="Seg_3466" n="e" s="T443">nʼuːččamsɨjaːččɨ </ts>
               <ts e="T445" id="Seg_3468" n="e" s="T444">ete, </ts>
               <ts e="T446" id="Seg_3470" n="e" s="T445">horok </ts>
               <ts e="T447" id="Seg_3472" n="e" s="T446">takʼie </ts>
               <ts e="T448" id="Seg_3474" n="e" s="T447">bɨlʼi. </ts>
            </ts>
            <ts e="T480" id="Seg_3475" n="sc" s="T459">
               <ts e="T460" id="Seg_3477" n="e" s="T459">Aragiːnɨ </ts>
               <ts e="T461" id="Seg_3479" n="e" s="T460">da </ts>
               <ts e="T462" id="Seg_3481" n="e" s="T461">ispetegim, </ts>
               <ts e="T280" id="Seg_3483" n="e" s="T462">((…)) </ts>
               <ts e="T463" id="Seg_3485" n="e" s="T280">üjege </ts>
               <ts e="T464" id="Seg_3487" n="e" s="T463">da </ts>
               <ts e="T466" id="Seg_3489" n="e" s="T464">ihispetegim. </ts>
               <ts e="T467" id="Seg_3491" n="e" s="T466">Kɨrdʼɨ͡akpar </ts>
               <ts e="T468" id="Seg_3493" n="e" s="T467">di͡eri </ts>
               <ts e="T469" id="Seg_3495" n="e" s="T468">itiččeleːgi </ts>
               <ts e="T470" id="Seg_3497" n="e" s="T469">karajbɨtɨm, </ts>
               <ts e="T471" id="Seg_3499" n="e" s="T470">biːr </ts>
               <ts e="T472" id="Seg_3501" n="e" s="T471">ürümkanɨ </ts>
               <ts e="T473" id="Seg_3503" n="e" s="T472">ispetegim </ts>
               <ts e="T474" id="Seg_3505" n="e" s="T473">kaččaga </ts>
               <ts e="T475" id="Seg_3507" n="e" s="T474">da. </ts>
               <ts e="T476" id="Seg_3509" n="e" s="T475">Bi͡ek </ts>
               <ts e="T477" id="Seg_3511" n="e" s="T476">(is-) </ts>
               <ts e="T478" id="Seg_3513" n="e" s="T477">ihimmije </ts>
               <ts e="T479" id="Seg_3515" n="e" s="T478">hɨldʼɨbɨtɨm </ts>
               <ts e="T480" id="Seg_3517" n="e" s="T479">bi͡ek. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-LaVN">
            <ta e="T14" id="Seg_3518" s="T4">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.001 (001.002)</ta>
            <ta e="T23" id="Seg_3519" s="T14">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.002 (001.003)</ta>
            <ta e="T33" id="Seg_3520" s="T23">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.003 (001.004)</ta>
            <ta e="T35" id="Seg_3521" s="T33">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.004 (001.005)</ta>
            <ta e="T43" id="Seg_3522" s="T35">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.005 (001.006)</ta>
            <ta e="T45" id="Seg_3523" s="T43">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.006 (001.007)</ta>
            <ta e="T48" id="Seg_3524" s="T45">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.007 (001.008)</ta>
            <ta e="T58" id="Seg_3525" s="T48">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.008 (001.009)</ta>
            <ta e="T67" id="Seg_3526" s="T58">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.009 (001.012)</ta>
            <ta e="T69" id="Seg_3527" s="T67">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.010 (001.013)</ta>
            <ta e="T74" id="Seg_3528" s="T69">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.011 (001.014)</ta>
            <ta e="T80" id="Seg_3529" s="T74">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.012 (001.015)</ta>
            <ta e="T89" id="Seg_3530" s="T82">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.013 (001.017)</ta>
            <ta e="T96" id="Seg_3531" s="T92">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.014 (001.019)</ta>
            <ta e="T101" id="Seg_3532" s="T96">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.015 (001.020)</ta>
            <ta e="T104" id="Seg_3533" s="T101">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.016 (001.021)</ta>
            <ta e="T115" id="Seg_3534" s="T104">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.017 (001.022)</ta>
            <ta e="T121" id="Seg_3535" s="T115">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.018 (001.023)</ta>
            <ta e="T132" id="Seg_3536" s="T121">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.019 (001.024)</ta>
            <ta e="T138" id="Seg_3537" s="T135">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.020 (001.026)</ta>
            <ta e="T147" id="Seg_3538" s="T138">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.021 (001.027)</ta>
            <ta e="T152" id="Seg_3539" s="T147">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.022 (001.028)</ta>
            <ta e="T154" id="Seg_3540" s="T152">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.023 (001.029)</ta>
            <ta e="T163" id="Seg_3541" s="T154">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.024 (001.030)</ta>
            <ta e="T166" id="Seg_3542" s="T163">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.025 (001.031)</ta>
            <ta e="T169" id="Seg_3543" s="T166">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.026 (001.032)</ta>
            <ta e="T174" id="Seg_3544" s="T171">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.027 (001.034)</ta>
            <ta e="T181" id="Seg_3545" s="T174">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.028 (001.035)</ta>
            <ta e="T195" id="Seg_3546" s="T185">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.029 (001.037)</ta>
            <ta e="T202" id="Seg_3547" s="T195">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.030 (001.038)</ta>
            <ta e="T223" id="Seg_3548" s="T214">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.031 (001.040)</ta>
            <ta e="T229" id="Seg_3549" s="T223">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.032 (001.041)</ta>
            <ta e="T236" id="Seg_3550" s="T229">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.033 (001.042)</ta>
            <ta e="T244" id="Seg_3551" s="T236">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.034 (001.043)</ta>
            <ta e="T256" id="Seg_3552" s="T244">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.035 (001.044)</ta>
            <ta e="T271" id="Seg_3553" s="T256">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.036 (001.045)</ta>
            <ta e="T276" id="Seg_3554" s="T271">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.037 (001.046)</ta>
            <ta e="T281" id="Seg_3555" s="T276">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.038 (001.047)</ta>
            <ta e="T319" id="Seg_3556" s="T293">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.039 (001.049)</ta>
            <ta e="T344" id="Seg_3557" s="T329">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.040 (001.051)</ta>
            <ta e="T350" id="Seg_3558" s="T346">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.041 (001.053)</ta>
            <ta e="T365" id="Seg_3559" s="T355">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.042 (001.055)</ta>
            <ta e="T376" id="Seg_3560" s="T370">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.043 (001.057)</ta>
            <ta e="T381" id="Seg_3561" s="T376">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.044 (001.058)</ta>
            <ta e="T388" id="Seg_3562" s="T381">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.045 (001.059)</ta>
            <ta e="T390" id="Seg_3563" s="T388">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.046 (001.060)</ta>
            <ta e="T399" id="Seg_3564" s="T390">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.047 (001.061)</ta>
            <ta e="T404" id="Seg_3565" s="T399">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.048 (001.062)</ta>
            <ta e="T412" id="Seg_3566" s="T408">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.049 (001.064)</ta>
            <ta e="T421" id="Seg_3567" s="T412">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.050 (001.065)</ta>
            <ta e="T434" id="Seg_3568" s="T429">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.051 (001.067)</ta>
            <ta e="T436" id="Seg_3569" s="T434">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.052 (001.068)</ta>
            <ta e="T448" id="Seg_3570" s="T436">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.053 (001.069)</ta>
            <ta e="T466" id="Seg_3571" s="T459">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.054 (001.071)</ta>
            <ta e="T475" id="Seg_3572" s="T466">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.055 (001.072)</ta>
            <ta e="T480" id="Seg_3573" s="T475">LaVN_KuNS_1999_MusicRepressions_conv.LaVN.056 (001.073)</ta>
         </annotation>
         <annotation name="st" tierref="st-LaVN">
            <ta e="T14" id="Seg_3574" s="T4">ВН: Ыбыый, ол ырыалары ол киэ кими илэ-илэ дьэ биһиэкэ…</ta>
            <ta e="T23" id="Seg_3575" s="T14">Стадага олоробут буолла биһиги табаннан кэ, тыага кэ олоробут.</ta>
            <ta e="T33" id="Seg_3576" s="T23">Комсомол кэлэр, комсомол, тогда дьэ ыбыыйдар комсомол дьэ илэ буопса.</ta>
            <ta e="T35" id="Seg_3577" s="T33">Партейнардар кэллилэр.</ta>
            <ta e="T43" id="Seg_3578" s="T35">"Аһа, ыбыай, – дииллэр миигин, – кайа, Барбаа, пластинкаларыӈ бааллар?"</ta>
            <ta e="T45" id="Seg_3579" s="T43">Мин диибин:</ta>
            <ta e="T48" id="Seg_3580" s="T45">"Һуок, Руслан гиэттэрэ."</ta>
            <ta e="T58" id="Seg_3581" s="T48">Һудтаабыттара гинини, диэбиттэрэ "кимнээӈ…ВН: …алдьаталааӈ гини паластинкатын.</ta>
            <ta e="T67" id="Seg_3582" s="T58">Бу тыа киһитигэр барбыттара (пластин-) барыта алдьаталааӈ", – диэбиттэрэ.</ta>
            <ta e="T69" id="Seg_3583" s="T67">Мин диибин:</ta>
            <ta e="T74" id="Seg_3584" s="T69">"Һуок миниэкэ, биир даа һуок".</ta>
            <ta e="T80" id="Seg_3585" s="T74">"Туугу ыллатагын ити патефоӈӈар", – дииллэр минигин.</ta>
            <ta e="T89" id="Seg_3586" s="T82">ВН: Һэ-э, ол киэ ыллатабыт бэйэм киэ.</ta>
            <ta e="T96" id="Seg_3587" s="T92">ВН: Мин бэйэм ылыммытым.</ta>
            <ta e="T101" id="Seg_3588" s="T96">Һэ-э, Чёрнайга һылдьаммын киэ оччого.</ta>
            <ta e="T104" id="Seg_3589" s="T101">Бэйэм ылыммытым ол.</ta>
            <ta e="T115" id="Seg_3590" s="T104">Бэйэм ылан бараан, онтуӈ иннэлээк, туспа коробкакаан киэ, ону бэйэбит эрийэбит.</ta>
            <ta e="T121" id="Seg_3591" s="T115">Онтон паластинканы уурабыт, ол (ылл-)…</ta>
            <ta e="T132" id="Seg_3592" s="T121">Дьэ "Баленка, баленка" два была миниэ (пластин-) боскуой мыньаа ырыалаак.</ta>
            <ta e="T138" id="Seg_3593" s="T135">ВН: Аһа, Руслаанова ыллыыр:</ta>
            <ta e="T147" id="Seg_3594" s="T138">"Баленка, баленка пассытайа" буол, дьэ, онтубун дьэ кистээ диин.</ta>
            <ta e="T152" id="Seg_3595" s="T147">Огобор ким, пэриинэкээн обуойа оӈорбутум.</ta>
            <ta e="T154" id="Seg_3596" s="T152">Куччугуй ого.</ta>
            <ta e="T163" id="Seg_3597" s="T154">Ол иһинэн тигэн кээһэн бараан… онно уурабын.</ta>
            <ta e="T166" id="Seg_3598" s="T163">Икки паластинканы кистээбитим.</ta>
            <ta e="T169" id="Seg_3599" s="T166">Һаныга диэрии кистээбитим.</ta>
            <ta e="T174" id="Seg_3600" s="T171">ВН: Русланова гиэттэрин.</ta>
            <ta e="T181" id="Seg_3601" s="T174">О-ой илэ какие были илэ. </ta>
            <ta e="T195" id="Seg_3602" s="T185">ВН: Ол пластинкаларбын бу оголор, улаатан барааннар чеӈкиччи алдьаталаабыттар.</ta>
            <ta e="T202" id="Seg_3603" s="T195">Бу нечаянно Люся ол хозяина эт, барытын.</ta>
            <ta e="T223" id="Seg_3604" s="T214">ВН: Оччого Сталиныӈ этэ буолла, һэ-э, оччого, ол (һуд-).</ta>
            <ta e="T229" id="Seg_3605" s="T223">Ол кэнни гинини һудтаабыттар, гинини ол.</ta>
            <ta e="T236" id="Seg_3606" s="T229">Патамуста… но-о, тогда-то война была.</ta>
            <ta e="T244" id="Seg_3607" s="T236">Они-то песни пели ходили-то.</ta>
            <ta e="T256" id="Seg_3608" s="T244">Оччого гини бойнаага ол… барар да войнаага киэ ыллаталлар дии артиистары киэ.</ta>
            <ta e="T271" id="Seg_3609" s="T256">Но она по старости какие-то песни пела, ол иһин гиллэри һудтууллар, о-о, муӈнаактары да.</ta>
            <ta e="T276" id="Seg_3610" s="T271">Эрин гытта иккиэннэрин һудтаабыттара эни.</ta>
            <ta e="T281" id="Seg_3611" s="T276">Какие были они раньше люди.</ta>
            <ta e="T319" id="Seg_3612" s="T293">ВН: Дьэ ол ырыалары иһиллээмиэккэ гиннэр, просто гиннэр туогу эрэ, но, как-то бредитель ли, чё ли, какой-ли, дьэ ол ити ол биһиэттэрэ ол уһуттууллар ол.</ta>
            <ta e="T344" id="Seg_3613" s="T329">ВН: Дьэ, эни киэ, дьэ, ол дьэ улаһактаан (уһултаан) кээһэллэр, ол кистиибит дьэ онтугун, ой, такая илэ.</ta>
            <ta e="T350" id="Seg_3614" s="T346">ВН: Дэ, аны өйдөөтөккө.</ta>
            <ta e="T365" id="Seg_3615" s="T355">ВН: Но, киһи куттаныак буолуо, оччого кэлэллэр, алдьаталыыллар киэ, кэлэллэр.</ta>
            <ta e="T376" id="Seg_3616" s="T370">НК: Нуу, миниэнэ падрускам была она.</ta>
            <ta e="T381" id="Seg_3617" s="T376">Она в город, ыбыай, барбат.</ta>
            <ta e="T388" id="Seg_3618" s="T381">"Өллүбүт агай, кимӈин кистээ", – диир минигин киэ.</ta>
            <ta e="T390" id="Seg_3619" s="T388">"Того, ду…?"</ta>
            <ta e="T399" id="Seg_3620" s="T390">"Кайа, алдьаталыыллар Русланов пластинкаларын, мин бииркээммин кистээбитим", – диир догорум. </ta>
            <ta e="T404" id="Seg_3621" s="T399">Дьэ, ол иккиэн кистиибит дии.</ta>
            <ta e="T412" id="Seg_3622" s="T408">ВН: Анна этэ покойная бу.</ta>
            <ta e="T421" id="Seg_3623" s="T412">Нина иньэтэ, бу Горуо Нина иньэтэ этэ, Анна покойна.</ta>
            <ta e="T434" id="Seg_3624" s="T429">ВН: Һака киһилэрэ этилэрэ биһиэттэрэ.</ta>
            <ta e="T436" id="Seg_3625" s="T434">Һакалар этилэр.</ta>
            <ta e="T448" id="Seg_3626" s="T436">Ну, раньше знаешь как, всякое было, һарак (һорок) ньууччамсыйааччы этэ, һорок такие были.</ta>
            <ta e="T466" id="Seg_3627" s="T459">ВН: Арагиины да испэтэгим, би иччэ үйэгэ да иһиспэтэгим.</ta>
            <ta e="T475" id="Seg_3628" s="T466">Кырдьыакпар диэри итиччэлээги карайбытым, биир үрүмканы испэтэгим каччага да.</ta>
            <ta e="T480" id="Seg_3629" s="T475">Биэк ис иһиммийэ һылдьыбытым биэк.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-LaVN">
            <ta e="T14" id="Seg_3630" s="T4">– ɨbɨːj, ol ɨrɨ͡alarɨ ol ke kimi ile dʼe bihi͡eke… </ta>
            <ta e="T23" id="Seg_3631" s="T14">Stadaga olorobut bu͡olla bihigi tabannan ke, tɨ͡aga ke olorobut. </ta>
            <ta e="T33" id="Seg_3632" s="T23">Kamsamol keler, kamsamol, tagda dʼe ɨbɨːjdar kamsamol dʼe ile bu͡opsa. </ta>
            <ta e="T35" id="Seg_3633" s="T33">Partʼejnajdar kelliler. </ta>
            <ta e="T43" id="Seg_3634" s="T35">"Aha, ɨbɨ͡aj", diːller miːgin, "kaja, Barbaː, palačinkalarɨŋ baːllar?" </ta>
            <ta e="T45" id="Seg_3635" s="T43">Min diːbin: </ta>
            <ta e="T48" id="Seg_3636" s="T45">"Hu͡ok, Ruslan gi͡ettere." </ta>
            <ta e="T58" id="Seg_3637" s="T48">Huːttaːbɨttara ginini, di͡ebittere "kimneːŋ, aldʼatalaːŋ gini palačinkatɨn." </ta>
            <ta e="T67" id="Seg_3638" s="T58">Bu tɨ͡a kihitiger barbɨttara "(palačin-) barɨta aldʼatalaːŋ", di͡ebittere. </ta>
            <ta e="T69" id="Seg_3639" s="T67">Min diːbin: </ta>
            <ta e="T74" id="Seg_3640" s="T69">"Hu͡ok mini͡eke, biːr daː hu͡ok." </ta>
            <ta e="T80" id="Seg_3641" s="T74">"Tuːgu ɨllatagɨn iti patʼefoŋŋar", diːller minigin. </ta>
            <ta e="T89" id="Seg_3642" s="T82">– Heː, ol ke ɨllatabɨn bejem ke. </ta>
            <ta e="T96" id="Seg_3643" s="T92">– Min bejem ɨlɨmmɨtɨm. </ta>
            <ta e="T101" id="Seg_3644" s="T96">Heː, Čornajga hɨldʼammɨn ke oččogo. ((LAUGH))</ta>
            <ta e="T104" id="Seg_3645" s="T101">Bejem ɨlɨmmɨtɨm ol. </ta>
            <ta e="T115" id="Seg_3646" s="T104">Bejem ɨlan baraːn, ontuŋ inneleːk, tuspa korobkakaːn ki͡e, onu bejebit erijebit. </ta>
            <ta e="T121" id="Seg_3647" s="T115">Onton palačinkanɨ uːrabɨt, ol (ɨll-)… </ta>
            <ta e="T132" id="Seg_3648" s="T121">Dʼe "Balʼenka, balʼenka" dva bɨla mini͡e (plastin-) bosku͡oj mɨnʼaː ɨrɨ͡alaːk. </ta>
            <ta e="T138" id="Seg_3649" s="T135">– Aha, Ruslaːnava ɨllɨːr: </ta>
            <ta e="T147" id="Seg_3650" s="T138">"Balʼenka, balʼenka passɨtaja" bu͡ol, dʼe, "ontubun dʼe kisteː" diːn. </ta>
            <ta e="T152" id="Seg_3651" s="T147">Ogobor kim, periːnekeːn obu͡oja oŋorbutum. </ta>
            <ta e="T154" id="Seg_3652" s="T152">Kuččuguj ogo. </ta>
            <ta e="T163" id="Seg_3653" s="T154">Ol ihinen tigen keːhen baraːn onno ((…)) onno uːrabɨn. </ta>
            <ta e="T166" id="Seg_3654" s="T163">Ikki palačinkanɨ kisteːbitim. </ta>
            <ta e="T169" id="Seg_3655" s="T166">Hanɨga di͡eri kisteːbitim. </ta>
            <ta e="T174" id="Seg_3656" s="T171">– Ruslaːnava gi͡etterin. </ta>
            <ta e="T181" id="Seg_3657" s="T174">Oːj ile kakʼije bɨlʼi ile (za-)… </ta>
            <ta e="T195" id="Seg_3658" s="T185">– Ol palačinkalarbɨn bu ogolor, ulaːtan baraːnnar čeŋkičči aldʼatalaːbɨttar bu͡o. </ta>
            <ta e="T202" id="Seg_3659" s="T195">Bu nʼečʼajanno Lʼusʼa ol хazʼaina et, barɨtɨn. </ta>
            <ta e="T223" id="Seg_3660" s="T214">– Oččogo Stalʼinɨŋ ete bu͡olla, heː, oččogo, ol (huːt-). </ta>
            <ta e="T229" id="Seg_3661" s="T223">Ol kenni ginini huːttaːbɨttar, ginini ol. </ta>
            <ta e="T236" id="Seg_3662" s="T229">Patamusta, noː, tagda-to vajna bɨla. </ta>
            <ta e="T244" id="Seg_3663" s="T236">Onʼi-ta pʼesnʼi pʼeʼli хadʼilʼi-ta. </ta>
            <ta e="T256" id="Seg_3664" s="T244">Oččogo gini bajnaːga ol, barar da vajnaːga ke ɨllatallar diː artʼiːstarɨ ke. </ta>
            <ta e="T271" id="Seg_3665" s="T256">No ana pa starastʼi kakʼije-ta pʼesnʼi pʼela, ol ihin gilleri huːttuːllar, oː, muŋnaːktarɨ. </ta>
            <ta e="T276" id="Seg_3666" s="T271">Erin gɨtta ikki͡ennerin huːttaːbɨttara eni. </ta>
            <ta e="T281" id="Seg_3667" s="T276">Takʼie bɨlʼi anʼi ranse lʼudʼi. </ta>
            <ta e="T319" id="Seg_3668" s="T293">– Dʼe ol ɨrɨ͡alarɨ ihilleːmi͡ekke ginner, prosta ginner tu͡ogu ere, no, kak ta brʼedʼitelʼ li, čʼo li, kakoj li, oččogo ol iti ol bihi͡ettere ol uhuttuːllar ol. </ta>
            <ta e="T344" id="Seg_3669" s="T329">– Dʼe, eni ke, dʼe, ol dʼe uhulaktaːn keːheller, ol kistiːbit dʼe ontugun, oj, takaja ile. </ta>
            <ta e="T350" id="Seg_3670" s="T346">– De, anɨ öjdöːtökkö. </ta>
            <ta e="T365" id="Seg_3671" s="T355">– No, kihi kuttanɨ͡ak bu͡olu͡o, oččogo keleller, aldʼatalɨːllar ke, keleller. </ta>
            <ta e="T376" id="Seg_3672" s="T370">– Nuː, mini͡ene padruskam bɨla ana. </ta>
            <ta e="T381" id="Seg_3673" s="T376">Ana v gorat, ɨbɨːj, barbat. </ta>
            <ta e="T388" id="Seg_3674" s="T381">"Öllübüt agaj, kimŋin kisteː", diːr minigin ke. </ta>
            <ta e="T390" id="Seg_3675" s="T388">"Togo du?" </ta>
            <ta e="T399" id="Seg_3676" s="T390">"Kaja, aldʼatalɨːllar Ruslaːnav palačinkalarɨn, min biːrkeːmmin kisteːbitim", diːr dogorum. </ta>
            <ta e="T404" id="Seg_3677" s="T399">Dʼe, ol ikki͡en kistiːbit diː. </ta>
            <ta e="T412" id="Seg_3678" s="T408">– Anna ete pakojnaja bu. </ta>
            <ta e="T421" id="Seg_3679" s="T412">Nʼina inʼete, bu Goru͡o Nʼina inʼete ete, Anna pakojna. </ta>
            <ta e="T434" id="Seg_3680" s="T429">– Haka kihilere etilere bihi͡ettere. </ta>
            <ta e="T436" id="Seg_3681" s="T434">Hakalar etiler. </ta>
            <ta e="T448" id="Seg_3682" s="T436">Nu, ranʼse znaješʼ kak, vsʼakaje bɨla, harak nʼuːččamsɨjaːččɨ ete, horok takʼie bɨlʼi. </ta>
            <ta e="T466" id="Seg_3683" s="T459">– Aragiːnɨ da ispetegim, ((…)) üjege da ihispetegim. </ta>
            <ta e="T475" id="Seg_3684" s="T466">Kɨrdʼɨ͡akpar di͡eri itiččeleːgi karajbɨtɨm, biːr ürümkanɨ ispetegim kaččaga da. </ta>
            <ta e="T480" id="Seg_3685" s="T475">Bi͡ek (is-) ihimmije hɨldʼɨbɨtɨm bi͡ek. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-LaVN">
            <ta e="T5" id="Seg_3686" s="T4">ɨbɨːj</ta>
            <ta e="T6" id="Seg_3687" s="T5">ol</ta>
            <ta e="T7" id="Seg_3688" s="T6">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T8" id="Seg_3689" s="T7">ol</ta>
            <ta e="T9" id="Seg_3690" s="T8">ke</ta>
            <ta e="T10" id="Seg_3691" s="T9">kim-i</ta>
            <ta e="T11" id="Seg_3692" s="T10">ile</ta>
            <ta e="T12" id="Seg_3693" s="T11">dʼe</ta>
            <ta e="T14" id="Seg_3694" s="T12">bihi͡e-ke</ta>
            <ta e="T15" id="Seg_3695" s="T14">stada-ga</ta>
            <ta e="T16" id="Seg_3696" s="T15">olor-o-but</ta>
            <ta e="T17" id="Seg_3697" s="T16">bu͡olla</ta>
            <ta e="T18" id="Seg_3698" s="T17">bihigi</ta>
            <ta e="T19" id="Seg_3699" s="T18">taba-nnan</ta>
            <ta e="T20" id="Seg_3700" s="T19">ke</ta>
            <ta e="T21" id="Seg_3701" s="T20">tɨ͡a-ga</ta>
            <ta e="T22" id="Seg_3702" s="T21">ke</ta>
            <ta e="T23" id="Seg_3703" s="T22">olor-o-but</ta>
            <ta e="T24" id="Seg_3704" s="T23">kamsamol</ta>
            <ta e="T25" id="Seg_3705" s="T24">kel-er</ta>
            <ta e="T26" id="Seg_3706" s="T25">kamsamol</ta>
            <ta e="T28" id="Seg_3707" s="T27">dʼe</ta>
            <ta e="T29" id="Seg_3708" s="T28">ɨbɨːj-dar</ta>
            <ta e="T30" id="Seg_3709" s="T29">kamsamol</ta>
            <ta e="T31" id="Seg_3710" s="T30">dʼe</ta>
            <ta e="T32" id="Seg_3711" s="T31">ile</ta>
            <ta e="T33" id="Seg_3712" s="T32">bu͡opsa</ta>
            <ta e="T34" id="Seg_3713" s="T33">partʼejnaj-dar</ta>
            <ta e="T35" id="Seg_3714" s="T34">kel-li-ler</ta>
            <ta e="T36" id="Seg_3715" s="T35">aha</ta>
            <ta e="T37" id="Seg_3716" s="T36">ɨbɨ͡aj</ta>
            <ta e="T38" id="Seg_3717" s="T37">diː-l-ler</ta>
            <ta e="T39" id="Seg_3718" s="T38">miːgi-n</ta>
            <ta e="T40" id="Seg_3719" s="T39">kaja</ta>
            <ta e="T41" id="Seg_3720" s="T40">Barbaː</ta>
            <ta e="T42" id="Seg_3721" s="T41">palačinka-lar-ɨ-ŋ</ta>
            <ta e="T43" id="Seg_3722" s="T42">baːl-lar</ta>
            <ta e="T44" id="Seg_3723" s="T43">min</ta>
            <ta e="T45" id="Seg_3724" s="T44">d-iː-bin</ta>
            <ta e="T46" id="Seg_3725" s="T45">hu͡ok</ta>
            <ta e="T47" id="Seg_3726" s="T46">Ruslan</ta>
            <ta e="T48" id="Seg_3727" s="T47">gi͡et-tere</ta>
            <ta e="T49" id="Seg_3728" s="T48">huːt-taː-bɨt-tara</ta>
            <ta e="T50" id="Seg_3729" s="T49">gini-ni</ta>
            <ta e="T51" id="Seg_3730" s="T50">di͡e-bit-tere</ta>
            <ta e="T53" id="Seg_3731" s="T51">kim-neː-ŋ</ta>
            <ta e="T56" id="Seg_3732" s="T53">aldʼat-alaː-ŋ</ta>
            <ta e="T57" id="Seg_3733" s="T56">gini</ta>
            <ta e="T58" id="Seg_3734" s="T57">palačinka-tɨ-n</ta>
            <ta e="T59" id="Seg_3735" s="T58">bu</ta>
            <ta e="T60" id="Seg_3736" s="T59">tɨ͡a</ta>
            <ta e="T61" id="Seg_3737" s="T60">kihi-ti-ger</ta>
            <ta e="T62" id="Seg_3738" s="T61">bar-bɨt-tara</ta>
            <ta e="T65" id="Seg_3739" s="T64">barɨ-ta</ta>
            <ta e="T66" id="Seg_3740" s="T65">aldʼat-alaː-ŋ</ta>
            <ta e="T67" id="Seg_3741" s="T66">di͡e-bit-tere</ta>
            <ta e="T68" id="Seg_3742" s="T67">min</ta>
            <ta e="T69" id="Seg_3743" s="T68">d-iː-bin</ta>
            <ta e="T70" id="Seg_3744" s="T69">hu͡ok</ta>
            <ta e="T71" id="Seg_3745" s="T70">mini͡e-ke</ta>
            <ta e="T72" id="Seg_3746" s="T71">biːr</ta>
            <ta e="T73" id="Seg_3747" s="T72">daː</ta>
            <ta e="T74" id="Seg_3748" s="T73">hu͡ok</ta>
            <ta e="T75" id="Seg_3749" s="T74">tuːg-u</ta>
            <ta e="T76" id="Seg_3750" s="T75">ɨll-a-t-a-gɨn</ta>
            <ta e="T77" id="Seg_3751" s="T76">iti</ta>
            <ta e="T78" id="Seg_3752" s="T77">patʼefoŋ-ŋa-r</ta>
            <ta e="T79" id="Seg_3753" s="T78">diː-l-ler</ta>
            <ta e="T80" id="Seg_3754" s="T79">minigi-n</ta>
            <ta e="T84" id="Seg_3755" s="T82">heː</ta>
            <ta e="T85" id="Seg_3756" s="T84">ol</ta>
            <ta e="T86" id="Seg_3757" s="T85">ke</ta>
            <ta e="T87" id="Seg_3758" s="T86">ɨll-a-t-a-bɨn</ta>
            <ta e="T88" id="Seg_3759" s="T87">beje-m</ta>
            <ta e="T89" id="Seg_3760" s="T88">ke</ta>
            <ta e="T94" id="Seg_3761" s="T92">min</ta>
            <ta e="T95" id="Seg_3762" s="T94">beje-m</ta>
            <ta e="T96" id="Seg_3763" s="T95">ɨlɨm-mɨt-ɨ-m</ta>
            <ta e="T97" id="Seg_3764" s="T96">heː</ta>
            <ta e="T98" id="Seg_3765" s="T97">Čornaj-ga</ta>
            <ta e="T99" id="Seg_3766" s="T98">hɨldʼ-am-mɨn</ta>
            <ta e="T100" id="Seg_3767" s="T99">ke</ta>
            <ta e="T101" id="Seg_3768" s="T100">oččogo</ta>
            <ta e="T102" id="Seg_3769" s="T101">beje-m</ta>
            <ta e="T103" id="Seg_3770" s="T102">ɨlɨm-mɨt-ɨ-m</ta>
            <ta e="T104" id="Seg_3771" s="T103">ol</ta>
            <ta e="T105" id="Seg_3772" s="T104">beje-m</ta>
            <ta e="T106" id="Seg_3773" s="T105">ɨl-an</ta>
            <ta e="T107" id="Seg_3774" s="T106">baraːn</ta>
            <ta e="T108" id="Seg_3775" s="T107">on-tu-ŋ</ta>
            <ta e="T109" id="Seg_3776" s="T108">inne-leːk</ta>
            <ta e="T110" id="Seg_3777" s="T109">tuspa</ta>
            <ta e="T111" id="Seg_3778" s="T110">korobka-kaːn</ta>
            <ta e="T112" id="Seg_3779" s="T111">ki͡e</ta>
            <ta e="T113" id="Seg_3780" s="T112">o-nu</ta>
            <ta e="T114" id="Seg_3781" s="T113">beje-bit</ta>
            <ta e="T115" id="Seg_3782" s="T114">erij-e-bit</ta>
            <ta e="T116" id="Seg_3783" s="T115">onton</ta>
            <ta e="T117" id="Seg_3784" s="T116">palačinka-nɨ</ta>
            <ta e="T118" id="Seg_3785" s="T117">uːr-a-bɨt</ta>
            <ta e="T119" id="Seg_3786" s="T118">ol</ta>
            <ta e="T122" id="Seg_3787" s="T121">dʼe</ta>
            <ta e="T127" id="Seg_3788" s="T126">mini͡e</ta>
            <ta e="T130" id="Seg_3789" s="T129">bosku͡oj</ta>
            <ta e="T131" id="Seg_3790" s="T130">mɨnʼaː</ta>
            <ta e="T132" id="Seg_3791" s="T131">ɨrɨ͡a-laːk</ta>
            <ta e="T136" id="Seg_3792" s="T135">aha</ta>
            <ta e="T137" id="Seg_3793" s="T136">Ruslaːnava</ta>
            <ta e="T138" id="Seg_3794" s="T137">ɨllɨː-r</ta>
            <ta e="T142" id="Seg_3795" s="T141">bu͡ol</ta>
            <ta e="T143" id="Seg_3796" s="T142">dʼe</ta>
            <ta e="T144" id="Seg_3797" s="T143">on-tu-bu-n</ta>
            <ta e="T145" id="Seg_3798" s="T144">dʼe</ta>
            <ta e="T146" id="Seg_3799" s="T145">kisteː</ta>
            <ta e="T147" id="Seg_3800" s="T146">diːn</ta>
            <ta e="T148" id="Seg_3801" s="T147">ogo-bo-r</ta>
            <ta e="T149" id="Seg_3802" s="T148">kim</ta>
            <ta e="T150" id="Seg_3803" s="T149">periːne-keːn</ta>
            <ta e="T151" id="Seg_3804" s="T150">obu͡oj-a</ta>
            <ta e="T152" id="Seg_3805" s="T151">oŋor-but-u-m</ta>
            <ta e="T153" id="Seg_3806" s="T152">kuččuguj</ta>
            <ta e="T154" id="Seg_3807" s="T153">ogo</ta>
            <ta e="T155" id="Seg_3808" s="T154">ol</ta>
            <ta e="T156" id="Seg_3809" s="T155">ih-i-nen</ta>
            <ta e="T157" id="Seg_3810" s="T156">tig-en</ta>
            <ta e="T158" id="Seg_3811" s="T157">keːh-en</ta>
            <ta e="T159" id="Seg_3812" s="T158">baraːn</ta>
            <ta e="T160" id="Seg_3813" s="T159">onno</ta>
            <ta e="T161" id="Seg_3814" s="T162">onno</ta>
            <ta e="T163" id="Seg_3815" s="T161">uːr-a-bɨn</ta>
            <ta e="T164" id="Seg_3816" s="T163">ikki</ta>
            <ta e="T165" id="Seg_3817" s="T164">palačinka-nɨ</ta>
            <ta e="T166" id="Seg_3818" s="T165">kisteː-bit-i-m</ta>
            <ta e="T167" id="Seg_3819" s="T166">h-anɨ-ga</ta>
            <ta e="T168" id="Seg_3820" s="T167">di͡eri</ta>
            <ta e="T169" id="Seg_3821" s="T168">kisteː-bit-i-m</ta>
            <ta e="T173" id="Seg_3822" s="T171">Ruslaːnava</ta>
            <ta e="T174" id="Seg_3823" s="T173">gi͡et-teri-n</ta>
            <ta e="T175" id="Seg_3824" s="T174">oːj</ta>
            <ta e="T176" id="Seg_3825" s="T175">ile</ta>
            <ta e="T187" id="Seg_3826" s="T185">ol</ta>
            <ta e="T188" id="Seg_3827" s="T187">palačinka-lar-bɨ-n</ta>
            <ta e="T189" id="Seg_3828" s="T188">bu</ta>
            <ta e="T190" id="Seg_3829" s="T189">ogo-lor</ta>
            <ta e="T191" id="Seg_3830" s="T190">ulaːt-an</ta>
            <ta e="T192" id="Seg_3831" s="T191">bar-aːn-nar</ta>
            <ta e="T193" id="Seg_3832" s="T192">čeŋki-čči</ta>
            <ta e="T194" id="Seg_3833" s="T193">aldʼat-alaː-bɨt-tar</ta>
            <ta e="T195" id="Seg_3834" s="T194">bu͡o</ta>
            <ta e="T196" id="Seg_3835" s="T195">bu</ta>
            <ta e="T197" id="Seg_3836" s="T196">nʼečʼajanno</ta>
            <ta e="T198" id="Seg_3837" s="T197">Lʼusʼa</ta>
            <ta e="T199" id="Seg_3838" s="T198">ol</ta>
            <ta e="T200" id="Seg_3839" s="T199">хazʼaina</ta>
            <ta e="T201" id="Seg_3840" s="T200">et</ta>
            <ta e="T202" id="Seg_3841" s="T201">barɨ-tɨ-n</ta>
            <ta e="T216" id="Seg_3842" s="T214">oččogo</ta>
            <ta e="T217" id="Seg_3843" s="T216">Stalʼin-ɨ-ŋ</ta>
            <ta e="T218" id="Seg_3844" s="T217">e-t-e</ta>
            <ta e="T219" id="Seg_3845" s="T218">bu͡olla</ta>
            <ta e="T220" id="Seg_3846" s="T219">heː</ta>
            <ta e="T221" id="Seg_3847" s="T220">oččogo</ta>
            <ta e="T222" id="Seg_3848" s="T221">ol</ta>
            <ta e="T223" id="Seg_3849" s="T222">huːt</ta>
            <ta e="T224" id="Seg_3850" s="T223">ol</ta>
            <ta e="T225" id="Seg_3851" s="T224">kenni</ta>
            <ta e="T226" id="Seg_3852" s="T225">gini-ni</ta>
            <ta e="T227" id="Seg_3853" s="T226">huːt-taː-bɨt-tar</ta>
            <ta e="T228" id="Seg_3854" s="T227">gini-ni</ta>
            <ta e="T229" id="Seg_3855" s="T228">ol</ta>
            <ta e="T245" id="Seg_3856" s="T244">oččogo</ta>
            <ta e="T246" id="Seg_3857" s="T245">gini</ta>
            <ta e="T247" id="Seg_3858" s="T246">bajnaː-ga</ta>
            <ta e="T248" id="Seg_3859" s="T247">ol</ta>
            <ta e="T249" id="Seg_3860" s="T248">bar-ar</ta>
            <ta e="T250" id="Seg_3861" s="T249">da</ta>
            <ta e="T251" id="Seg_3862" s="T250">vajnaː-ga</ta>
            <ta e="T252" id="Seg_3863" s="T251">ke</ta>
            <ta e="T253" id="Seg_3864" s="T252">ɨll-a-t-al-lar</ta>
            <ta e="T254" id="Seg_3865" s="T253">diː</ta>
            <ta e="T255" id="Seg_3866" s="T254">artʼiːs-tar-ɨ</ta>
            <ta e="T256" id="Seg_3867" s="T255">ke</ta>
            <ta e="T265" id="Seg_3868" s="T264">ol</ta>
            <ta e="T266" id="Seg_3869" s="T265">ihin</ta>
            <ta e="T267" id="Seg_3870" s="T266">giller-i</ta>
            <ta e="T268" id="Seg_3871" s="T267">huːt-tuː-l-lar</ta>
            <ta e="T269" id="Seg_3872" s="T268">oː</ta>
            <ta e="T271" id="Seg_3873" s="T269">muŋ-naːk-tar-ɨ</ta>
            <ta e="T272" id="Seg_3874" s="T271">er-i-n</ta>
            <ta e="T273" id="Seg_3875" s="T272">gɨtta</ta>
            <ta e="T274" id="Seg_3876" s="T273">ikk-i͡en-neri-n</ta>
            <ta e="T275" id="Seg_3877" s="T274">huːt-taː-bɨt-tara</ta>
            <ta e="T276" id="Seg_3878" s="T275">eni</ta>
            <ta e="T294" id="Seg_3879" s="T293">dʼe</ta>
            <ta e="T295" id="Seg_3880" s="T294">ol</ta>
            <ta e="T296" id="Seg_3881" s="T295">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T297" id="Seg_3882" s="T296">ihilleː-m-i͡ek-ke</ta>
            <ta e="T298" id="Seg_3883" s="T297">ginner</ta>
            <ta e="T299" id="Seg_3884" s="T298">prosta</ta>
            <ta e="T300" id="Seg_3885" s="T299">ginner</ta>
            <ta e="T301" id="Seg_3886" s="T300">tu͡og-u</ta>
            <ta e="T302" id="Seg_3887" s="T301">ere</ta>
            <ta e="T312" id="Seg_3888" s="T311">oččogo</ta>
            <ta e="T313" id="Seg_3889" s="T312">ol</ta>
            <ta e="T314" id="Seg_3890" s="T313">iti</ta>
            <ta e="T315" id="Seg_3891" s="T314">ol</ta>
            <ta e="T316" id="Seg_3892" s="T315">bihi͡ettere</ta>
            <ta e="T317" id="Seg_3893" s="T316">ol</ta>
            <ta e="T318" id="Seg_3894" s="T317">uhut-tuː-l-lar</ta>
            <ta e="T319" id="Seg_3895" s="T318">ol</ta>
            <ta e="T330" id="Seg_3896" s="T329">dʼe</ta>
            <ta e="T331" id="Seg_3897" s="T330">eni</ta>
            <ta e="T332" id="Seg_3898" s="T331">ke</ta>
            <ta e="T333" id="Seg_3899" s="T332">dʼe</ta>
            <ta e="T334" id="Seg_3900" s="T333">ol</ta>
            <ta e="T335" id="Seg_3901" s="T334">dʼe</ta>
            <ta e="T336" id="Seg_3902" s="T335">uhul-aktaː-n</ta>
            <ta e="T337" id="Seg_3903" s="T336">keːh-el-ler</ta>
            <ta e="T338" id="Seg_3904" s="T337">ol</ta>
            <ta e="T339" id="Seg_3905" s="T338">kist-iː-bit</ta>
            <ta e="T340" id="Seg_3906" s="T339">dʼe</ta>
            <ta e="T341" id="Seg_3907" s="T340">on-tu-gu-n</ta>
            <ta e="T342" id="Seg_3908" s="T341">oj</ta>
            <ta e="T344" id="Seg_3909" s="T343">ile</ta>
            <ta e="T348" id="Seg_3910" s="T346">de</ta>
            <ta e="T349" id="Seg_3911" s="T348">anɨ</ta>
            <ta e="T350" id="Seg_3912" s="T349">öjdöː-tök-kö</ta>
            <ta e="T357" id="Seg_3913" s="T355">no</ta>
            <ta e="T358" id="Seg_3914" s="T357">kihi</ta>
            <ta e="T359" id="Seg_3915" s="T358">kuttan-ɨ͡ak</ta>
            <ta e="T360" id="Seg_3916" s="T359">bu͡ol-u͡o</ta>
            <ta e="T361" id="Seg_3917" s="T360">oččogo</ta>
            <ta e="T362" id="Seg_3918" s="T361">kel-el-ler</ta>
            <ta e="T363" id="Seg_3919" s="T362">aldʼat-alɨː-l-lar</ta>
            <ta e="T364" id="Seg_3920" s="T363">ke</ta>
            <ta e="T365" id="Seg_3921" s="T364">kel-el-ler</ta>
            <ta e="T372" id="Seg_3922" s="T370">nuː</ta>
            <ta e="T373" id="Seg_3923" s="T372">mini͡ene</ta>
            <ta e="T374" id="Seg_3924" s="T373">padruska-m</ta>
            <ta e="T379" id="Seg_3925" s="T378">ɨbɨːj</ta>
            <ta e="T381" id="Seg_3926" s="T379">bar-bat</ta>
            <ta e="T382" id="Seg_3927" s="T381">öl-lü-büt</ta>
            <ta e="T383" id="Seg_3928" s="T382">agaj</ta>
            <ta e="T384" id="Seg_3929" s="T383">kim-ŋi-n</ta>
            <ta e="T385" id="Seg_3930" s="T384">kisteː</ta>
            <ta e="T386" id="Seg_3931" s="T385">diː-r</ta>
            <ta e="T387" id="Seg_3932" s="T386">minigi-n</ta>
            <ta e="T388" id="Seg_3933" s="T387">ke</ta>
            <ta e="T389" id="Seg_3934" s="T388">togo</ta>
            <ta e="T390" id="Seg_3935" s="T389">du</ta>
            <ta e="T391" id="Seg_3936" s="T390">kaja</ta>
            <ta e="T392" id="Seg_3937" s="T391">aldʼat-alɨː-l-lar</ta>
            <ta e="T393" id="Seg_3938" s="T392">Ruslaːnav</ta>
            <ta e="T394" id="Seg_3939" s="T393">palačinka-lar-ɨ-n</ta>
            <ta e="T395" id="Seg_3940" s="T394">min</ta>
            <ta e="T396" id="Seg_3941" s="T395">biːr-keːm-mi-n</ta>
            <ta e="T397" id="Seg_3942" s="T396">kisteː-bit-i-m</ta>
            <ta e="T398" id="Seg_3943" s="T397">diː-r</ta>
            <ta e="T399" id="Seg_3944" s="T398">dogor-u-m</ta>
            <ta e="T400" id="Seg_3945" s="T399">dʼe</ta>
            <ta e="T401" id="Seg_3946" s="T400">ol</ta>
            <ta e="T402" id="Seg_3947" s="T401">ikk-i͡en</ta>
            <ta e="T403" id="Seg_3948" s="T402">kist-iː-bit</ta>
            <ta e="T404" id="Seg_3949" s="T403">diː</ta>
            <ta e="T409" id="Seg_3950" s="T408">Anna</ta>
            <ta e="T410" id="Seg_3951" s="T409">e-t-e</ta>
            <ta e="T412" id="Seg_3952" s="T411">bu</ta>
            <ta e="T413" id="Seg_3953" s="T412">Nʼina</ta>
            <ta e="T414" id="Seg_3954" s="T413">inʼe-te</ta>
            <ta e="T415" id="Seg_3955" s="T414">bu</ta>
            <ta e="T416" id="Seg_3956" s="T415">Goru͡o</ta>
            <ta e="T417" id="Seg_3957" s="T416">Nʼina</ta>
            <ta e="T418" id="Seg_3958" s="T417">inʼe-te</ta>
            <ta e="T419" id="Seg_3959" s="T418">e-t-e</ta>
            <ta e="T420" id="Seg_3960" s="T419">Anna</ta>
            <ta e="T431" id="Seg_3961" s="T429">haka</ta>
            <ta e="T432" id="Seg_3962" s="T431">kihi-lere</ta>
            <ta e="T433" id="Seg_3963" s="T432">e-ti-lere</ta>
            <ta e="T434" id="Seg_3964" s="T433">bihi͡ettere</ta>
            <ta e="T435" id="Seg_3965" s="T434">haka-lar</ta>
            <ta e="T436" id="Seg_3966" s="T435">e-ti-ler</ta>
            <ta e="T443" id="Seg_3967" s="T442">harak</ta>
            <ta e="T444" id="Seg_3968" s="T443">nʼuːčča-msɨj-aːččɨ</ta>
            <ta e="T445" id="Seg_3969" s="T444">e-t-e</ta>
            <ta e="T446" id="Seg_3970" s="T445">horok</ta>
            <ta e="T460" id="Seg_3971" s="T459">aragiː-nɨ</ta>
            <ta e="T461" id="Seg_3972" s="T460">da</ta>
            <ta e="T462" id="Seg_3973" s="T461">is-peteg-i-m</ta>
            <ta e="T463" id="Seg_3974" s="T280">üje-ge</ta>
            <ta e="T464" id="Seg_3975" s="T463">da</ta>
            <ta e="T466" id="Seg_3976" s="T464">ih-i-s-peteg-i-m</ta>
            <ta e="T467" id="Seg_3977" s="T466">kɨrdʼ-ɨ͡ak-pa-r</ta>
            <ta e="T468" id="Seg_3978" s="T467">di͡eri</ta>
            <ta e="T469" id="Seg_3979" s="T468">itičče-leːg-i</ta>
            <ta e="T470" id="Seg_3980" s="T469">karaj-bɨt-ɨ-m</ta>
            <ta e="T471" id="Seg_3981" s="T470">biːr</ta>
            <ta e="T472" id="Seg_3982" s="T471">ürümka-nɨ</ta>
            <ta e="T473" id="Seg_3983" s="T472">is-peteg-i-m</ta>
            <ta e="T474" id="Seg_3984" s="T473">kaččaga</ta>
            <ta e="T475" id="Seg_3985" s="T474">da</ta>
            <ta e="T476" id="Seg_3986" s="T475">bi͡ek</ta>
            <ta e="T477" id="Seg_3987" s="T476">is</ta>
            <ta e="T478" id="Seg_3988" s="T477">ih-i-m-mije</ta>
            <ta e="T479" id="Seg_3989" s="T478">hɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T480" id="Seg_3990" s="T479">bi͡ek</ta>
         </annotation>
         <annotation name="mp" tierref="mp-LaVN">
            <ta e="T5" id="Seg_3991" s="T4">ɨbɨj</ta>
            <ta e="T6" id="Seg_3992" s="T5">ol</ta>
            <ta e="T7" id="Seg_3993" s="T6">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T8" id="Seg_3994" s="T7">ol</ta>
            <ta e="T9" id="Seg_3995" s="T8">ka</ta>
            <ta e="T10" id="Seg_3996" s="T9">kim-nI</ta>
            <ta e="T11" id="Seg_3997" s="T10">ile</ta>
            <ta e="T12" id="Seg_3998" s="T11">dʼe</ta>
            <ta e="T14" id="Seg_3999" s="T12">bihigi-GA</ta>
            <ta e="T15" id="Seg_4000" s="T14">stada-GA</ta>
            <ta e="T16" id="Seg_4001" s="T15">olor-A-BIt</ta>
            <ta e="T17" id="Seg_4002" s="T16">bu͡olla</ta>
            <ta e="T18" id="Seg_4003" s="T17">bihigi</ta>
            <ta e="T19" id="Seg_4004" s="T18">taba-nAn</ta>
            <ta e="T20" id="Seg_4005" s="T19">ka</ta>
            <ta e="T21" id="Seg_4006" s="T20">tɨ͡a-GA</ta>
            <ta e="T22" id="Seg_4007" s="T21">ka</ta>
            <ta e="T23" id="Seg_4008" s="T22">olor-A-BIt</ta>
            <ta e="T24" id="Seg_4009" s="T23">kamsamol</ta>
            <ta e="T25" id="Seg_4010" s="T24">kel-Ar</ta>
            <ta e="T26" id="Seg_4011" s="T25">kamsamol</ta>
            <ta e="T28" id="Seg_4012" s="T27">dʼe</ta>
            <ta e="T29" id="Seg_4013" s="T28">ɨbɨj-LAr</ta>
            <ta e="T30" id="Seg_4014" s="T29">kamsamol</ta>
            <ta e="T31" id="Seg_4015" s="T30">dʼe</ta>
            <ta e="T32" id="Seg_4016" s="T31">ile</ta>
            <ta e="T33" id="Seg_4017" s="T32">bu͡opsa</ta>
            <ta e="T34" id="Seg_4018" s="T33">partʼejnaj-LAr</ta>
            <ta e="T35" id="Seg_4019" s="T34">kel-TI-LAr</ta>
            <ta e="T36" id="Seg_4020" s="T35">aː</ta>
            <ta e="T37" id="Seg_4021" s="T36">ɨbɨj</ta>
            <ta e="T38" id="Seg_4022" s="T37">di͡e-Ar-LAr</ta>
            <ta e="T39" id="Seg_4023" s="T38">min-n</ta>
            <ta e="T40" id="Seg_4024" s="T39">kaja</ta>
            <ta e="T41" id="Seg_4025" s="T40">Barbaːra</ta>
            <ta e="T42" id="Seg_4026" s="T41">palačinka-LAr-I-ŋ</ta>
            <ta e="T43" id="Seg_4027" s="T42">baːr-LAr</ta>
            <ta e="T44" id="Seg_4028" s="T43">min</ta>
            <ta e="T45" id="Seg_4029" s="T44">di͡e-A-BIn</ta>
            <ta e="T46" id="Seg_4030" s="T45">hu͡ok</ta>
            <ta e="T47" id="Seg_4031" s="T46">Ruslanava</ta>
            <ta e="T48" id="Seg_4032" s="T47">gi͡en-LArA</ta>
            <ta e="T49" id="Seg_4033" s="T48">huːt-LAː-BIT-LArA</ta>
            <ta e="T50" id="Seg_4034" s="T49">gini-nI</ta>
            <ta e="T51" id="Seg_4035" s="T50">di͡e-BIT-LArA</ta>
            <ta e="T53" id="Seg_4036" s="T51">kim-LAː-ŋ</ta>
            <ta e="T56" id="Seg_4037" s="T53">aldʼat-AlAː-ŋ</ta>
            <ta e="T57" id="Seg_4038" s="T56">gini</ta>
            <ta e="T58" id="Seg_4039" s="T57">palačinka-tI-n</ta>
            <ta e="T59" id="Seg_4040" s="T58">bu</ta>
            <ta e="T60" id="Seg_4041" s="T59">tɨ͡a</ta>
            <ta e="T61" id="Seg_4042" s="T60">kihi-tI-GAr</ta>
            <ta e="T62" id="Seg_4043" s="T61">bar-BIT-LArA</ta>
            <ta e="T65" id="Seg_4044" s="T64">barɨ-tA</ta>
            <ta e="T66" id="Seg_4045" s="T65">aldʼat-AlAː-ŋ</ta>
            <ta e="T67" id="Seg_4046" s="T66">di͡e-BIT-LArA</ta>
            <ta e="T68" id="Seg_4047" s="T67">min</ta>
            <ta e="T69" id="Seg_4048" s="T68">di͡e-A-BIn</ta>
            <ta e="T70" id="Seg_4049" s="T69">hu͡ok</ta>
            <ta e="T71" id="Seg_4050" s="T70">min-GA</ta>
            <ta e="T72" id="Seg_4051" s="T71">biːr</ta>
            <ta e="T73" id="Seg_4052" s="T72">da</ta>
            <ta e="T74" id="Seg_4053" s="T73">hu͡ok</ta>
            <ta e="T75" id="Seg_4054" s="T74">tu͡ok-nI</ta>
            <ta e="T76" id="Seg_4055" s="T75">ɨllaː-A-t-A-GIn</ta>
            <ta e="T77" id="Seg_4056" s="T76">iti</ta>
            <ta e="T78" id="Seg_4057" s="T77">patʼefon-GA-r</ta>
            <ta e="T79" id="Seg_4058" s="T78">di͡e-Ar-LAr</ta>
            <ta e="T80" id="Seg_4059" s="T79">min-n</ta>
            <ta e="T84" id="Seg_4060" s="T82">eː</ta>
            <ta e="T85" id="Seg_4061" s="T84">ol</ta>
            <ta e="T86" id="Seg_4062" s="T85">ka</ta>
            <ta e="T87" id="Seg_4063" s="T86">ɨllaː-A-t-A-BIn</ta>
            <ta e="T88" id="Seg_4064" s="T87">beje-m</ta>
            <ta e="T89" id="Seg_4065" s="T88">ka</ta>
            <ta e="T94" id="Seg_4066" s="T92">min</ta>
            <ta e="T95" id="Seg_4067" s="T94">beje-m</ta>
            <ta e="T96" id="Seg_4068" s="T95">ɨlɨn-BIT-I-m</ta>
            <ta e="T97" id="Seg_4069" s="T96">eː</ta>
            <ta e="T98" id="Seg_4070" s="T97">Čornaj-GA</ta>
            <ta e="T99" id="Seg_4071" s="T98">hɨrɨt-An-BIn</ta>
            <ta e="T100" id="Seg_4072" s="T99">ka</ta>
            <ta e="T101" id="Seg_4073" s="T100">oččogo</ta>
            <ta e="T102" id="Seg_4074" s="T101">beje-m</ta>
            <ta e="T103" id="Seg_4075" s="T102">ɨlɨn-BIT-I-m</ta>
            <ta e="T104" id="Seg_4076" s="T103">ol</ta>
            <ta e="T105" id="Seg_4077" s="T104">beje-m</ta>
            <ta e="T106" id="Seg_4078" s="T105">ɨl-An</ta>
            <ta e="T107" id="Seg_4079" s="T106">baran</ta>
            <ta e="T108" id="Seg_4080" s="T107">ol-tI-ŋ</ta>
            <ta e="T109" id="Seg_4081" s="T108">iŋne-LAːK</ta>
            <ta e="T110" id="Seg_4082" s="T109">tuspa</ta>
            <ta e="T111" id="Seg_4083" s="T110">koru͡opka-kAːN</ta>
            <ta e="T112" id="Seg_4084" s="T111">keː</ta>
            <ta e="T113" id="Seg_4085" s="T112">ol-nI</ta>
            <ta e="T114" id="Seg_4086" s="T113">beje-BIt</ta>
            <ta e="T115" id="Seg_4087" s="T114">erij-A-BIt</ta>
            <ta e="T116" id="Seg_4088" s="T115">onton</ta>
            <ta e="T117" id="Seg_4089" s="T116">palačinka-nI</ta>
            <ta e="T118" id="Seg_4090" s="T117">uːr-A-BIt</ta>
            <ta e="T119" id="Seg_4091" s="T118">ol</ta>
            <ta e="T122" id="Seg_4092" s="T121">dʼe</ta>
            <ta e="T127" id="Seg_4093" s="T126">min</ta>
            <ta e="T130" id="Seg_4094" s="T129">bosku͡oj</ta>
            <ta e="T131" id="Seg_4095" s="T130">munʼaː</ta>
            <ta e="T132" id="Seg_4096" s="T131">ɨrɨ͡a-LAːK</ta>
            <ta e="T136" id="Seg_4097" s="T135">aː</ta>
            <ta e="T137" id="Seg_4098" s="T136">Ruslanava</ta>
            <ta e="T138" id="Seg_4099" s="T137">ɨllaː-Ar</ta>
            <ta e="T142" id="Seg_4100" s="T141">bu͡ol</ta>
            <ta e="T143" id="Seg_4101" s="T142">dʼe</ta>
            <ta e="T144" id="Seg_4102" s="T143">ol-tI-BI-n</ta>
            <ta e="T145" id="Seg_4103" s="T144">dʼe</ta>
            <ta e="T146" id="Seg_4104" s="T145">kisteː</ta>
            <ta e="T147" id="Seg_4105" s="T146">diː</ta>
            <ta e="T148" id="Seg_4106" s="T147">ogo-BA-r</ta>
            <ta e="T149" id="Seg_4107" s="T148">kim</ta>
            <ta e="T150" id="Seg_4108" s="T149">periːne-kAːN</ta>
            <ta e="T151" id="Seg_4109" s="T150">obu͡oj-tA</ta>
            <ta e="T152" id="Seg_4110" s="T151">oŋor-BIT-I-m</ta>
            <ta e="T153" id="Seg_4111" s="T152">küččügüj</ta>
            <ta e="T154" id="Seg_4112" s="T153">ogo</ta>
            <ta e="T155" id="Seg_4113" s="T154">ol</ta>
            <ta e="T156" id="Seg_4114" s="T155">is-tI-nAn</ta>
            <ta e="T157" id="Seg_4115" s="T156">tik-An</ta>
            <ta e="T158" id="Seg_4116" s="T157">keːs-An</ta>
            <ta e="T159" id="Seg_4117" s="T158">baran</ta>
            <ta e="T160" id="Seg_4118" s="T159">onno</ta>
            <ta e="T161" id="Seg_4119" s="T162">onno</ta>
            <ta e="T163" id="Seg_4120" s="T161">uːr-A-BIn</ta>
            <ta e="T164" id="Seg_4121" s="T163">ikki</ta>
            <ta e="T165" id="Seg_4122" s="T164">palačinka-nI</ta>
            <ta e="T166" id="Seg_4123" s="T165">kisteː-BIT-I-m</ta>
            <ta e="T167" id="Seg_4124" s="T166">[C^1][V^1][C^2]-anɨ-GA</ta>
            <ta e="T168" id="Seg_4125" s="T167">di͡eri</ta>
            <ta e="T169" id="Seg_4126" s="T168">kisteː-BIT-I-m</ta>
            <ta e="T173" id="Seg_4127" s="T171">Ruslanava</ta>
            <ta e="T174" id="Seg_4128" s="T173">gi͡en-LArI-n</ta>
            <ta e="T175" id="Seg_4129" s="T174">oj</ta>
            <ta e="T176" id="Seg_4130" s="T175">ile</ta>
            <ta e="T187" id="Seg_4131" s="T185">ol</ta>
            <ta e="T188" id="Seg_4132" s="T187">palačinka-LAr-BI-n</ta>
            <ta e="T189" id="Seg_4133" s="T188">bu</ta>
            <ta e="T190" id="Seg_4134" s="T189">ogo-LAr</ta>
            <ta e="T191" id="Seg_4135" s="T190">ulaːt-An</ta>
            <ta e="T192" id="Seg_4136" s="T191">bar-An-LAr</ta>
            <ta e="T193" id="Seg_4137" s="T192">čeŋki-ččI</ta>
            <ta e="T194" id="Seg_4138" s="T193">aldʼat-AlAː-BIT-LAr</ta>
            <ta e="T195" id="Seg_4139" s="T194">bu͡o</ta>
            <ta e="T196" id="Seg_4140" s="T195">bu</ta>
            <ta e="T197" id="Seg_4141" s="T196">nʼečʼajanno</ta>
            <ta e="T198" id="Seg_4142" s="T197">Lʼusʼa</ta>
            <ta e="T199" id="Seg_4143" s="T198">ol</ta>
            <ta e="T200" id="Seg_4144" s="T199">küheːjke</ta>
            <ta e="T201" id="Seg_4145" s="T200">eːt</ta>
            <ta e="T202" id="Seg_4146" s="T201">barɨ-tI-n</ta>
            <ta e="T216" id="Seg_4147" s="T214">oččogo</ta>
            <ta e="T217" id="Seg_4148" s="T216">Stalʼin-I-ŋ</ta>
            <ta e="T218" id="Seg_4149" s="T217">e-TI-tA</ta>
            <ta e="T219" id="Seg_4150" s="T218">bu͡olla</ta>
            <ta e="T220" id="Seg_4151" s="T219">eː</ta>
            <ta e="T221" id="Seg_4152" s="T220">oččogo</ta>
            <ta e="T222" id="Seg_4153" s="T221">ol</ta>
            <ta e="T223" id="Seg_4154" s="T222">huːt</ta>
            <ta e="T224" id="Seg_4155" s="T223">ol</ta>
            <ta e="T225" id="Seg_4156" s="T224">kelin</ta>
            <ta e="T226" id="Seg_4157" s="T225">gini-nI</ta>
            <ta e="T227" id="Seg_4158" s="T226">huːt-LAː-BIT-LAr</ta>
            <ta e="T228" id="Seg_4159" s="T227">gini-nI</ta>
            <ta e="T229" id="Seg_4160" s="T228">ol</ta>
            <ta e="T245" id="Seg_4161" s="T244">oččogo</ta>
            <ta e="T246" id="Seg_4162" s="T245">gini</ta>
            <ta e="T247" id="Seg_4163" s="T246">vajna-GA</ta>
            <ta e="T248" id="Seg_4164" s="T247">ol</ta>
            <ta e="T249" id="Seg_4165" s="T248">bar-Ar</ta>
            <ta e="T250" id="Seg_4166" s="T249">da</ta>
            <ta e="T251" id="Seg_4167" s="T250">vajna-GA</ta>
            <ta e="T252" id="Seg_4168" s="T251">ka</ta>
            <ta e="T253" id="Seg_4169" s="T252">ɨllaː-A-t-Ar-LAr</ta>
            <ta e="T254" id="Seg_4170" s="T253">diː</ta>
            <ta e="T255" id="Seg_4171" s="T254">artʼist-LAr-nI</ta>
            <ta e="T256" id="Seg_4172" s="T255">ka</ta>
            <ta e="T265" id="Seg_4173" s="T264">ol</ta>
            <ta e="T266" id="Seg_4174" s="T265">ihin</ta>
            <ta e="T267" id="Seg_4175" s="T266">giniler-nI</ta>
            <ta e="T268" id="Seg_4176" s="T267">huːt-LAː-Ar-LAr</ta>
            <ta e="T269" id="Seg_4177" s="T268">oː</ta>
            <ta e="T271" id="Seg_4178" s="T269">muŋ-LAːK-LAr-nI</ta>
            <ta e="T272" id="Seg_4179" s="T271">er-tI-n</ta>
            <ta e="T273" id="Seg_4180" s="T272">kɨtta</ta>
            <ta e="T274" id="Seg_4181" s="T273">ikki-IAn-LArI-n</ta>
            <ta e="T275" id="Seg_4182" s="T274">huːt-LAː-BIT-LArA</ta>
            <ta e="T276" id="Seg_4183" s="T275">eni</ta>
            <ta e="T294" id="Seg_4184" s="T293">dʼe</ta>
            <ta e="T295" id="Seg_4185" s="T294">ol</ta>
            <ta e="T296" id="Seg_4186" s="T295">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T297" id="Seg_4187" s="T296">ihilleː-m-IAK-GA</ta>
            <ta e="T298" id="Seg_4188" s="T297">giniler</ta>
            <ta e="T299" id="Seg_4189" s="T298">prosta</ta>
            <ta e="T300" id="Seg_4190" s="T299">giniler</ta>
            <ta e="T301" id="Seg_4191" s="T300">tu͡ok-nI</ta>
            <ta e="T302" id="Seg_4192" s="T301">ere</ta>
            <ta e="T312" id="Seg_4193" s="T311">oččogo</ta>
            <ta e="T313" id="Seg_4194" s="T312">ol</ta>
            <ta e="T314" id="Seg_4195" s="T313">iti</ta>
            <ta e="T315" id="Seg_4196" s="T314">ol</ta>
            <ta e="T316" id="Seg_4197" s="T315">bihi͡ettere</ta>
            <ta e="T317" id="Seg_4198" s="T316">ol</ta>
            <ta e="T318" id="Seg_4199" s="T317">uhun-TAː-Ar-LAr</ta>
            <ta e="T319" id="Seg_4200" s="T318">ol</ta>
            <ta e="T330" id="Seg_4201" s="T329">dʼe</ta>
            <ta e="T331" id="Seg_4202" s="T330">eni</ta>
            <ta e="T332" id="Seg_4203" s="T331">ka</ta>
            <ta e="T333" id="Seg_4204" s="T332">dʼe</ta>
            <ta e="T334" id="Seg_4205" s="T333">ol</ta>
            <ta e="T335" id="Seg_4206" s="T334">dʼe</ta>
            <ta e="T336" id="Seg_4207" s="T335">ugul-AːktAː-An</ta>
            <ta e="T337" id="Seg_4208" s="T336">keːs-Ar-LAr</ta>
            <ta e="T338" id="Seg_4209" s="T337">ol</ta>
            <ta e="T339" id="Seg_4210" s="T338">kisteː-A-BIt</ta>
            <ta e="T340" id="Seg_4211" s="T339">dʼe</ta>
            <ta e="T341" id="Seg_4212" s="T340">ol-tI-GI-n</ta>
            <ta e="T342" id="Seg_4213" s="T341">oj</ta>
            <ta e="T344" id="Seg_4214" s="T343">ile</ta>
            <ta e="T348" id="Seg_4215" s="T346">dʼe</ta>
            <ta e="T349" id="Seg_4216" s="T348">anɨ</ta>
            <ta e="T350" id="Seg_4217" s="T349">öjdöː-TAK-GA</ta>
            <ta e="T357" id="Seg_4218" s="T355">noː</ta>
            <ta e="T358" id="Seg_4219" s="T357">kihi</ta>
            <ta e="T359" id="Seg_4220" s="T358">kuttan-IAK</ta>
            <ta e="T360" id="Seg_4221" s="T359">bu͡ol-IAK</ta>
            <ta e="T361" id="Seg_4222" s="T360">oččogo</ta>
            <ta e="T362" id="Seg_4223" s="T361">kel-Ar-LAr</ta>
            <ta e="T363" id="Seg_4224" s="T362">aldʼat-AlAː-Ar-LAr</ta>
            <ta e="T364" id="Seg_4225" s="T363">ka</ta>
            <ta e="T365" id="Seg_4226" s="T364">kel-Ar-LAr</ta>
            <ta e="T372" id="Seg_4227" s="T370">nu</ta>
            <ta e="T373" id="Seg_4228" s="T372">mini͡ene</ta>
            <ta e="T374" id="Seg_4229" s="T373">padruska-m</ta>
            <ta e="T379" id="Seg_4230" s="T378">ɨbɨj</ta>
            <ta e="T381" id="Seg_4231" s="T379">bar-BAT</ta>
            <ta e="T382" id="Seg_4232" s="T381">öl-TI-BIt</ta>
            <ta e="T383" id="Seg_4233" s="T382">agaj</ta>
            <ta e="T384" id="Seg_4234" s="T383">kim-GI-n</ta>
            <ta e="T385" id="Seg_4235" s="T384">kisteː</ta>
            <ta e="T386" id="Seg_4236" s="T385">di͡e-Ar</ta>
            <ta e="T387" id="Seg_4237" s="T386">min-n</ta>
            <ta e="T388" id="Seg_4238" s="T387">ka</ta>
            <ta e="T389" id="Seg_4239" s="T388">togo</ta>
            <ta e="T390" id="Seg_4240" s="T389">du͡o</ta>
            <ta e="T391" id="Seg_4241" s="T390">kaja</ta>
            <ta e="T392" id="Seg_4242" s="T391">aldʼat-AlAː-Ar-LAr</ta>
            <ta e="T393" id="Seg_4243" s="T392">Ruslanava</ta>
            <ta e="T394" id="Seg_4244" s="T393">palačinka-LAr-tI-n</ta>
            <ta e="T395" id="Seg_4245" s="T394">min</ta>
            <ta e="T396" id="Seg_4246" s="T395">biːr-kAːN-BI-n</ta>
            <ta e="T397" id="Seg_4247" s="T396">kisteː-BIT-I-m</ta>
            <ta e="T398" id="Seg_4248" s="T397">di͡e-Ar</ta>
            <ta e="T399" id="Seg_4249" s="T398">dogor-I-m</ta>
            <ta e="T400" id="Seg_4250" s="T399">dʼe</ta>
            <ta e="T401" id="Seg_4251" s="T400">ol</ta>
            <ta e="T402" id="Seg_4252" s="T401">ikki-IAn</ta>
            <ta e="T403" id="Seg_4253" s="T402">kisteː-A-BIt</ta>
            <ta e="T404" id="Seg_4254" s="T403">diː</ta>
            <ta e="T409" id="Seg_4255" s="T408">Anna</ta>
            <ta e="T410" id="Seg_4256" s="T409">e-TI-tA</ta>
            <ta e="T412" id="Seg_4257" s="T411">bu</ta>
            <ta e="T413" id="Seg_4258" s="T412">Nʼina</ta>
            <ta e="T414" id="Seg_4259" s="T413">inʼe-tA</ta>
            <ta e="T415" id="Seg_4260" s="T414">bu</ta>
            <ta e="T416" id="Seg_4261" s="T415">Goru͡o</ta>
            <ta e="T417" id="Seg_4262" s="T416">Nʼina</ta>
            <ta e="T418" id="Seg_4263" s="T417">inʼe-tA</ta>
            <ta e="T419" id="Seg_4264" s="T418">e-TI-tA</ta>
            <ta e="T420" id="Seg_4265" s="T419">Anna</ta>
            <ta e="T431" id="Seg_4266" s="T429">haka</ta>
            <ta e="T432" id="Seg_4267" s="T431">kihi-LArA</ta>
            <ta e="T433" id="Seg_4268" s="T432">e-TI-LArA</ta>
            <ta e="T434" id="Seg_4269" s="T433">bihi͡ettere</ta>
            <ta e="T435" id="Seg_4270" s="T434">haka-LAr</ta>
            <ta e="T436" id="Seg_4271" s="T435">e-TI-LAr</ta>
            <ta e="T443" id="Seg_4272" s="T442">horok</ta>
            <ta e="T444" id="Seg_4273" s="T443">nuːčča-msIj-AːččI</ta>
            <ta e="T445" id="Seg_4274" s="T444">e-TI-tA</ta>
            <ta e="T446" id="Seg_4275" s="T445">horok</ta>
            <ta e="T460" id="Seg_4276" s="T459">aragiː-nI</ta>
            <ta e="T461" id="Seg_4277" s="T460">da</ta>
            <ta e="T462" id="Seg_4278" s="T461">is-BAtAK-I-m</ta>
            <ta e="T463" id="Seg_4279" s="T280">üje-GA</ta>
            <ta e="T464" id="Seg_4280" s="T463">da</ta>
            <ta e="T466" id="Seg_4281" s="T464">is-I-s-BAtAK-I-m</ta>
            <ta e="T467" id="Seg_4282" s="T466">kɨrɨj-IAK-BA-r</ta>
            <ta e="T468" id="Seg_4283" s="T467">di͡eri</ta>
            <ta e="T469" id="Seg_4284" s="T468">itičče-LAːK-nI</ta>
            <ta e="T470" id="Seg_4285" s="T469">karaj-BIT-I-m</ta>
            <ta e="T471" id="Seg_4286" s="T470">biːr</ta>
            <ta e="T472" id="Seg_4287" s="T471">ürümka-nI</ta>
            <ta e="T473" id="Seg_4288" s="T472">is-BAtAK-I-m</ta>
            <ta e="T474" id="Seg_4289" s="T473">kaččaga</ta>
            <ta e="T475" id="Seg_4290" s="T474">da</ta>
            <ta e="T476" id="Seg_4291" s="T475">bi͡ek</ta>
            <ta e="T477" id="Seg_4292" s="T476">is</ta>
            <ta e="T478" id="Seg_4293" s="T477">is-I-n-mInA</ta>
            <ta e="T479" id="Seg_4294" s="T478">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T480" id="Seg_4295" s="T479">bi͡ek</ta>
         </annotation>
         <annotation name="ge" tierref="ge-LaVN">
            <ta e="T5" id="Seg_4296" s="T4">oh.dear</ta>
            <ta e="T6" id="Seg_4297" s="T5">that</ta>
            <ta e="T7" id="Seg_4298" s="T6">song-PL-ACC</ta>
            <ta e="T8" id="Seg_4299" s="T7">that</ta>
            <ta e="T9" id="Seg_4300" s="T8">well</ta>
            <ta e="T10" id="Seg_4301" s="T9">who-ACC</ta>
            <ta e="T11" id="Seg_4302" s="T10">indeed</ta>
            <ta e="T12" id="Seg_4303" s="T11">well</ta>
            <ta e="T14" id="Seg_4304" s="T12">1PL-DAT/LOC</ta>
            <ta e="T15" id="Seg_4305" s="T14">herd-DAT/LOC</ta>
            <ta e="T16" id="Seg_4306" s="T15">live-PRS-1PL</ta>
            <ta e="T17" id="Seg_4307" s="T16">MOD</ta>
            <ta e="T18" id="Seg_4308" s="T17">1PL.[NOM]</ta>
            <ta e="T19" id="Seg_4309" s="T18">reindeer-INSTR</ta>
            <ta e="T20" id="Seg_4310" s="T19">well</ta>
            <ta e="T21" id="Seg_4311" s="T20">tundra-DAT/LOC</ta>
            <ta e="T22" id="Seg_4312" s="T21">well</ta>
            <ta e="T23" id="Seg_4313" s="T22">live-PRS-1PL</ta>
            <ta e="T24" id="Seg_4314" s="T23">komsomol.[NOM]</ta>
            <ta e="T25" id="Seg_4315" s="T24">come-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_4316" s="T25">komsomol.[NOM]</ta>
            <ta e="T28" id="Seg_4317" s="T27">well</ta>
            <ta e="T29" id="Seg_4318" s="T28">oh.dear-PL.[NOM]</ta>
            <ta e="T30" id="Seg_4319" s="T29">komsomol.[NOM]</ta>
            <ta e="T31" id="Seg_4320" s="T30">well</ta>
            <ta e="T32" id="Seg_4321" s="T31">indeed</ta>
            <ta e="T33" id="Seg_4322" s="T32">completely</ta>
            <ta e="T34" id="Seg_4323" s="T33">party.member-PL.[NOM]</ta>
            <ta e="T35" id="Seg_4324" s="T34">come-PST1-3PL</ta>
            <ta e="T36" id="Seg_4325" s="T35">ah</ta>
            <ta e="T37" id="Seg_4326" s="T36">oh.dear</ta>
            <ta e="T38" id="Seg_4327" s="T37">say-PRS-3PL</ta>
            <ta e="T39" id="Seg_4328" s="T38">1SG-ACC</ta>
            <ta e="T40" id="Seg_4329" s="T39">well</ta>
            <ta e="T41" id="Seg_4330" s="T40">Barbara</ta>
            <ta e="T42" id="Seg_4331" s="T41">record-PL-EP-2SG.[NOM]</ta>
            <ta e="T43" id="Seg_4332" s="T42">there.is-3PL</ta>
            <ta e="T44" id="Seg_4333" s="T43">1SG.[NOM]</ta>
            <ta e="T45" id="Seg_4334" s="T44">say-PRS-1SG</ta>
            <ta e="T46" id="Seg_4335" s="T45">no</ta>
            <ta e="T47" id="Seg_4336" s="T46">Ruslanova.[NOM]</ta>
            <ta e="T48" id="Seg_4337" s="T47">own-3PL.[NOM]</ta>
            <ta e="T49" id="Seg_4338" s="T48">court-VBZ-PST2-3PL</ta>
            <ta e="T50" id="Seg_4339" s="T49">3SG-ACC</ta>
            <ta e="T51" id="Seg_4340" s="T50">say-PST2-3PL</ta>
            <ta e="T53" id="Seg_4341" s="T51">who-VBZ-IMP.2PL</ta>
            <ta e="T56" id="Seg_4342" s="T53">break-FREQ-IMP.2PL</ta>
            <ta e="T57" id="Seg_4343" s="T56">3SG.[NOM]</ta>
            <ta e="T58" id="Seg_4344" s="T57">record-3SG-ACC</ta>
            <ta e="T59" id="Seg_4345" s="T58">this</ta>
            <ta e="T60" id="Seg_4346" s="T59">Dolgan</ta>
            <ta e="T61" id="Seg_4347" s="T60">human.being-3SG-DAT/LOC</ta>
            <ta e="T62" id="Seg_4348" s="T61">go-PST2-3PL</ta>
            <ta e="T65" id="Seg_4349" s="T64">every-3SG.[NOM]</ta>
            <ta e="T66" id="Seg_4350" s="T65">break-FREQ-IMP.2PL</ta>
            <ta e="T67" id="Seg_4351" s="T66">say-PST2-3PL</ta>
            <ta e="T68" id="Seg_4352" s="T67">1SG.[NOM]</ta>
            <ta e="T69" id="Seg_4353" s="T68">say-PRS-1SG</ta>
            <ta e="T70" id="Seg_4354" s="T69">NEG.EX</ta>
            <ta e="T71" id="Seg_4355" s="T70">1SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_4356" s="T71">one</ta>
            <ta e="T73" id="Seg_4357" s="T72">NEG</ta>
            <ta e="T74" id="Seg_4358" s="T73">NEG.EX</ta>
            <ta e="T75" id="Seg_4359" s="T74">what-ACC</ta>
            <ta e="T76" id="Seg_4360" s="T75">sing-EP-CAUS-PRS-2SG</ta>
            <ta e="T77" id="Seg_4361" s="T76">that</ta>
            <ta e="T78" id="Seg_4362" s="T77">gramophone-2SG-DAT/LOC</ta>
            <ta e="T79" id="Seg_4363" s="T78">say-PRS-3PL</ta>
            <ta e="T80" id="Seg_4364" s="T79">1SG-ACC</ta>
            <ta e="T84" id="Seg_4365" s="T82">AFFIRM</ta>
            <ta e="T85" id="Seg_4366" s="T84">that</ta>
            <ta e="T86" id="Seg_4367" s="T85">well</ta>
            <ta e="T87" id="Seg_4368" s="T86">sing-EP-CAUS-PRS-1SG</ta>
            <ta e="T88" id="Seg_4369" s="T87">self-1SG.[NOM]</ta>
            <ta e="T89" id="Seg_4370" s="T88">well</ta>
            <ta e="T94" id="Seg_4371" s="T92">1SG.[NOM]</ta>
            <ta e="T95" id="Seg_4372" s="T94">self-1SG.[NOM]</ta>
            <ta e="T96" id="Seg_4373" s="T95">buy-PST2-EP-1SG</ta>
            <ta e="T97" id="Seg_4374" s="T96">AFFIRM</ta>
            <ta e="T98" id="Seg_4375" s="T97">Black.Sea-DAT/LOC</ta>
            <ta e="T99" id="Seg_4376" s="T98">go-CVB.SEQ-1SG</ta>
            <ta e="T100" id="Seg_4377" s="T99">well</ta>
            <ta e="T101" id="Seg_4378" s="T100">then</ta>
            <ta e="T102" id="Seg_4379" s="T101">self-1SG.[NOM]</ta>
            <ta e="T103" id="Seg_4380" s="T102">buy-PST2-EP-1SG</ta>
            <ta e="T104" id="Seg_4381" s="T103">that</ta>
            <ta e="T105" id="Seg_4382" s="T104">self-1SG.[NOM]</ta>
            <ta e="T106" id="Seg_4383" s="T105">take-CVB.SEQ</ta>
            <ta e="T107" id="Seg_4384" s="T106">after</ta>
            <ta e="T108" id="Seg_4385" s="T107">that-3SG-2SG.[NOM]</ta>
            <ta e="T109" id="Seg_4386" s="T108">needle-PROPR.[NOM]</ta>
            <ta e="T110" id="Seg_4387" s="T109">individually</ta>
            <ta e="T111" id="Seg_4388" s="T110">box-DIM.[NOM]</ta>
            <ta e="T112" id="Seg_4389" s="T111">EMPH</ta>
            <ta e="T113" id="Seg_4390" s="T112">that-ACC</ta>
            <ta e="T114" id="Seg_4391" s="T113">self-1PL.[NOM]</ta>
            <ta e="T115" id="Seg_4392" s="T114">turn.around-PRS-1PL</ta>
            <ta e="T116" id="Seg_4393" s="T115">then</ta>
            <ta e="T117" id="Seg_4394" s="T116">record-ACC</ta>
            <ta e="T118" id="Seg_4395" s="T117">lay-PRS-1PL</ta>
            <ta e="T119" id="Seg_4396" s="T118">that</ta>
            <ta e="T122" id="Seg_4397" s="T121">well</ta>
            <ta e="T127" id="Seg_4398" s="T126">1SG</ta>
            <ta e="T130" id="Seg_4399" s="T129">beautiful</ta>
            <ta e="T131" id="Seg_4400" s="T130">INTNS</ta>
            <ta e="T132" id="Seg_4401" s="T131">song-PROPR</ta>
            <ta e="T136" id="Seg_4402" s="T135">ah</ta>
            <ta e="T137" id="Seg_4403" s="T136">Ruslanova.[NOM]</ta>
            <ta e="T138" id="Seg_4404" s="T137">sing-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_4405" s="T141">maybe</ta>
            <ta e="T143" id="Seg_4406" s="T142">well</ta>
            <ta e="T144" id="Seg_4407" s="T143">that-3SG-1SG-ACC</ta>
            <ta e="T145" id="Seg_4408" s="T144">well</ta>
            <ta e="T146" id="Seg_4409" s="T145">hide.[IMP.2SG]</ta>
            <ta e="T147" id="Seg_4410" s="T146">EMPH</ta>
            <ta e="T148" id="Seg_4411" s="T147">child-1SG-DAT/LOC</ta>
            <ta e="T149" id="Seg_4412" s="T148">who.[NOM]</ta>
            <ta e="T150" id="Seg_4413" s="T149">feather_bed-DIM.[NOM]</ta>
            <ta e="T151" id="Seg_4414" s="T150">leftover-3SG.[NOM]</ta>
            <ta e="T152" id="Seg_4415" s="T151">make-PST2-EP-1SG</ta>
            <ta e="T153" id="Seg_4416" s="T152">small</ta>
            <ta e="T154" id="Seg_4417" s="T153">child.[NOM]</ta>
            <ta e="T155" id="Seg_4418" s="T154">that.[NOM]</ta>
            <ta e="T156" id="Seg_4419" s="T155">inside-3SG-INSTR</ta>
            <ta e="T157" id="Seg_4420" s="T156">sew-CVB.SEQ</ta>
            <ta e="T158" id="Seg_4421" s="T157">let-CVB.SEQ</ta>
            <ta e="T159" id="Seg_4422" s="T158">after</ta>
            <ta e="T160" id="Seg_4423" s="T159">thither</ta>
            <ta e="T161" id="Seg_4424" s="T162">thither</ta>
            <ta e="T163" id="Seg_4425" s="T161">lay-PRS-1SG</ta>
            <ta e="T164" id="Seg_4426" s="T163">two</ta>
            <ta e="T165" id="Seg_4427" s="T164">record-ACC</ta>
            <ta e="T166" id="Seg_4428" s="T165">hide-PST2-EP-1SG</ta>
            <ta e="T167" id="Seg_4429" s="T166">EMPH-now-DAT/LOC</ta>
            <ta e="T168" id="Seg_4430" s="T167">until</ta>
            <ta e="T169" id="Seg_4431" s="T168">hide-PST2-EP-1SG</ta>
            <ta e="T173" id="Seg_4432" s="T171">Ruslanova.[NOM]</ta>
            <ta e="T174" id="Seg_4433" s="T173">own-3PL-ACC</ta>
            <ta e="T175" id="Seg_4434" s="T174">EXCL</ta>
            <ta e="T176" id="Seg_4435" s="T175">indeed</ta>
            <ta e="T187" id="Seg_4436" s="T185">that</ta>
            <ta e="T188" id="Seg_4437" s="T187">record-PL-1SG-ACC</ta>
            <ta e="T189" id="Seg_4438" s="T188">this</ta>
            <ta e="T190" id="Seg_4439" s="T189">child-PL.[NOM]</ta>
            <ta e="T191" id="Seg_4440" s="T190">grow-CVB.SEQ</ta>
            <ta e="T192" id="Seg_4441" s="T191">go-CVB.SEQ-3PL</ta>
            <ta e="T193" id="Seg_4442" s="T192">completely-ADVZ</ta>
            <ta e="T194" id="Seg_4443" s="T193">break-FREQ-PST2-3PL</ta>
            <ta e="T195" id="Seg_4444" s="T194">EMPH</ta>
            <ta e="T196" id="Seg_4445" s="T195">this</ta>
            <ta e="T197" id="Seg_4446" s="T196">accidentally</ta>
            <ta e="T198" id="Seg_4447" s="T197">Lyusya.[NOM]</ta>
            <ta e="T199" id="Seg_4448" s="T198">that</ta>
            <ta e="T200" id="Seg_4449" s="T199">housewife</ta>
            <ta e="T201" id="Seg_4450" s="T200">EVID</ta>
            <ta e="T202" id="Seg_4451" s="T201">whole-3SG-ACC</ta>
            <ta e="T216" id="Seg_4452" s="T214">then</ta>
            <ta e="T217" id="Seg_4453" s="T216">Stalin-EP-2SG.[NOM]</ta>
            <ta e="T218" id="Seg_4454" s="T217">be-PST1-3SG</ta>
            <ta e="T219" id="Seg_4455" s="T218">MOD</ta>
            <ta e="T220" id="Seg_4456" s="T219">AFFIRM</ta>
            <ta e="T221" id="Seg_4457" s="T220">then</ta>
            <ta e="T222" id="Seg_4458" s="T221">that</ta>
            <ta e="T223" id="Seg_4459" s="T222">court</ta>
            <ta e="T224" id="Seg_4460" s="T223">that</ta>
            <ta e="T225" id="Seg_4461" s="T224">back.[NOM]</ta>
            <ta e="T226" id="Seg_4462" s="T225">3SG-ACC</ta>
            <ta e="T227" id="Seg_4463" s="T226">court-VBZ-PST2-3PL</ta>
            <ta e="T228" id="Seg_4464" s="T227">3SG-ACC</ta>
            <ta e="T229" id="Seg_4465" s="T228">that</ta>
            <ta e="T245" id="Seg_4466" s="T244">then</ta>
            <ta e="T246" id="Seg_4467" s="T245">3SG.[NOM]</ta>
            <ta e="T247" id="Seg_4468" s="T246">war-DAT/LOC</ta>
            <ta e="T248" id="Seg_4469" s="T247">that</ta>
            <ta e="T249" id="Seg_4470" s="T248">go-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_4471" s="T249">EMPH</ta>
            <ta e="T251" id="Seg_4472" s="T250">war-DAT/LOC</ta>
            <ta e="T252" id="Seg_4473" s="T251">well</ta>
            <ta e="T253" id="Seg_4474" s="T252">sing-EP-CAUS-PRS-3PL</ta>
            <ta e="T254" id="Seg_4475" s="T253">EMPH</ta>
            <ta e="T255" id="Seg_4476" s="T254">artist-PL-ACC</ta>
            <ta e="T256" id="Seg_4477" s="T255">well</ta>
            <ta e="T265" id="Seg_4478" s="T264">that.[NOM]</ta>
            <ta e="T266" id="Seg_4479" s="T265">because.of</ta>
            <ta e="T267" id="Seg_4480" s="T266">3PL-ACC</ta>
            <ta e="T268" id="Seg_4481" s="T267">court-VBZ-PRS-3PL</ta>
            <ta e="T269" id="Seg_4482" s="T268">oh</ta>
            <ta e="T271" id="Seg_4483" s="T269">misery-PROPR-PL-ACC</ta>
            <ta e="T272" id="Seg_4484" s="T271">husband-3SG-ACC</ta>
            <ta e="T273" id="Seg_4485" s="T272">with</ta>
            <ta e="T274" id="Seg_4486" s="T273">two-COLL-3PL-ACC</ta>
            <ta e="T275" id="Seg_4487" s="T274">court-VBZ-PST2-3PL</ta>
            <ta e="T276" id="Seg_4488" s="T275">apparently</ta>
            <ta e="T294" id="Seg_4489" s="T293">well</ta>
            <ta e="T295" id="Seg_4490" s="T294">that</ta>
            <ta e="T296" id="Seg_4491" s="T295">song-PL-ACC</ta>
            <ta e="T297" id="Seg_4492" s="T296">listen-NEG-PTCP.FUT-DAT/LOC</ta>
            <ta e="T298" id="Seg_4493" s="T297">3PL.[NOM]</ta>
            <ta e="T299" id="Seg_4494" s="T298">simply</ta>
            <ta e="T300" id="Seg_4495" s="T299">3PL.[NOM]</ta>
            <ta e="T301" id="Seg_4496" s="T300">what-ACC</ta>
            <ta e="T302" id="Seg_4497" s="T301">INDEF</ta>
            <ta e="T312" id="Seg_4498" s="T311">then</ta>
            <ta e="T313" id="Seg_4499" s="T312">that</ta>
            <ta e="T314" id="Seg_4500" s="T313">that</ta>
            <ta e="T315" id="Seg_4501" s="T314">that</ta>
            <ta e="T316" id="Seg_4502" s="T315">our.[NOM]</ta>
            <ta e="T317" id="Seg_4503" s="T316">that</ta>
            <ta e="T318" id="Seg_4504" s="T317">get.of-ITER-PRS-3PL</ta>
            <ta e="T319" id="Seg_4505" s="T318">that</ta>
            <ta e="T330" id="Seg_4506" s="T329">well</ta>
            <ta e="T331" id="Seg_4507" s="T330">apparently</ta>
            <ta e="T332" id="Seg_4508" s="T331">well</ta>
            <ta e="T333" id="Seg_4509" s="T332">well</ta>
            <ta e="T334" id="Seg_4510" s="T333">that</ta>
            <ta e="T335" id="Seg_4511" s="T334">well</ta>
            <ta e="T336" id="Seg_4512" s="T335">take.away-EMOT-CVB.SEQ</ta>
            <ta e="T337" id="Seg_4513" s="T336">throw-PRS-3PL</ta>
            <ta e="T338" id="Seg_4514" s="T337">that</ta>
            <ta e="T339" id="Seg_4515" s="T338">hide-PRS-1PL</ta>
            <ta e="T340" id="Seg_4516" s="T339">well</ta>
            <ta e="T341" id="Seg_4517" s="T340">that-3SG-2SG-ACC</ta>
            <ta e="T342" id="Seg_4518" s="T341">EXCL</ta>
            <ta e="T344" id="Seg_4519" s="T343">indeed</ta>
            <ta e="T348" id="Seg_4520" s="T346">well</ta>
            <ta e="T349" id="Seg_4521" s="T348">now</ta>
            <ta e="T350" id="Seg_4522" s="T349">remember-PTCP.COND-DAT/LOC</ta>
            <ta e="T357" id="Seg_4523" s="T355">noo</ta>
            <ta e="T358" id="Seg_4524" s="T357">human.being.[NOM]</ta>
            <ta e="T359" id="Seg_4525" s="T358">be.afraid-PTCP.FUT</ta>
            <ta e="T360" id="Seg_4526" s="T359">be-FUT.[3SG]</ta>
            <ta e="T361" id="Seg_4527" s="T360">then</ta>
            <ta e="T362" id="Seg_4528" s="T361">come-PRS-3PL</ta>
            <ta e="T363" id="Seg_4529" s="T362">break-FREQ-PTCP.PRS-3PL</ta>
            <ta e="T364" id="Seg_4530" s="T363">well</ta>
            <ta e="T365" id="Seg_4531" s="T364">come-PRS-3PL</ta>
            <ta e="T372" id="Seg_4532" s="T370">so</ta>
            <ta e="T373" id="Seg_4533" s="T372">my</ta>
            <ta e="T374" id="Seg_4534" s="T373">friend-1SG.[NOM]</ta>
            <ta e="T379" id="Seg_4535" s="T378">oh.dear</ta>
            <ta e="T381" id="Seg_4536" s="T379">go-NEG.[3SG]</ta>
            <ta e="T382" id="Seg_4537" s="T381">die-PST1-1PL</ta>
            <ta e="T383" id="Seg_4538" s="T382">only</ta>
            <ta e="T384" id="Seg_4539" s="T383">who-2SG-ACC</ta>
            <ta e="T385" id="Seg_4540" s="T384">hide.[IMP.2SG]</ta>
            <ta e="T386" id="Seg_4541" s="T385">say-PRS.[3SG]</ta>
            <ta e="T387" id="Seg_4542" s="T386">1SG-ACC</ta>
            <ta e="T388" id="Seg_4543" s="T387">well</ta>
            <ta e="T389" id="Seg_4544" s="T388">why</ta>
            <ta e="T390" id="Seg_4545" s="T389">Q</ta>
            <ta e="T391" id="Seg_4546" s="T390">well</ta>
            <ta e="T392" id="Seg_4547" s="T391">break-FREQ-PRS-3PL</ta>
            <ta e="T393" id="Seg_4548" s="T392">Ruslanova.[NOM]</ta>
            <ta e="T394" id="Seg_4549" s="T393">record-PL-3SG-ACC</ta>
            <ta e="T395" id="Seg_4550" s="T394">1SG.[NOM]</ta>
            <ta e="T396" id="Seg_4551" s="T395">one-DIM-1SG-ACC</ta>
            <ta e="T397" id="Seg_4552" s="T396">hide-PST2-EP-1SG</ta>
            <ta e="T398" id="Seg_4553" s="T397">say-PRS.[3SG]</ta>
            <ta e="T399" id="Seg_4554" s="T398">friend-EP-1SG.[NOM]</ta>
            <ta e="T400" id="Seg_4555" s="T399">well</ta>
            <ta e="T401" id="Seg_4556" s="T400">that</ta>
            <ta e="T402" id="Seg_4557" s="T401">two-COLL</ta>
            <ta e="T403" id="Seg_4558" s="T402">hide-PRS-1PL</ta>
            <ta e="T404" id="Seg_4559" s="T403">EMPH</ta>
            <ta e="T409" id="Seg_4560" s="T408">Anna.[NOM]</ta>
            <ta e="T410" id="Seg_4561" s="T409">be-PST1-3SG</ta>
            <ta e="T412" id="Seg_4562" s="T411">this</ta>
            <ta e="T413" id="Seg_4563" s="T412">Nina.[NOM]</ta>
            <ta e="T414" id="Seg_4564" s="T413">mother-3SG.[NOM]</ta>
            <ta e="T415" id="Seg_4565" s="T414">this</ta>
            <ta e="T416" id="Seg_4566" s="T415">Goruo</ta>
            <ta e="T417" id="Seg_4567" s="T416">Nina.[NOM]</ta>
            <ta e="T418" id="Seg_4568" s="T417">mother-3SG.[NOM]</ta>
            <ta e="T419" id="Seg_4569" s="T418">be-PST1-3SG</ta>
            <ta e="T420" id="Seg_4570" s="T419">Anna.[NOM]</ta>
            <ta e="T431" id="Seg_4571" s="T429">Dolgan</ta>
            <ta e="T432" id="Seg_4572" s="T431">human.being-3PL.[NOM]</ta>
            <ta e="T433" id="Seg_4573" s="T432">be-PST1-3PL</ta>
            <ta e="T434" id="Seg_4574" s="T433">our.[NOM]</ta>
            <ta e="T435" id="Seg_4575" s="T434">Dolgan-PL.[NOM]</ta>
            <ta e="T436" id="Seg_4576" s="T435">be-PST1-3PL</ta>
            <ta e="T443" id="Seg_4577" s="T442">some</ta>
            <ta e="T444" id="Seg_4578" s="T443">Russian-DRV-PTCP.HAB</ta>
            <ta e="T445" id="Seg_4579" s="T444">be-PST1-3SG</ta>
            <ta e="T446" id="Seg_4580" s="T445">some</ta>
            <ta e="T460" id="Seg_4581" s="T459">spirit-ACC</ta>
            <ta e="T461" id="Seg_4582" s="T460">NEG</ta>
            <ta e="T462" id="Seg_4583" s="T461">drink-PST2.NEG-EP-1SG</ta>
            <ta e="T463" id="Seg_4584" s="T280">time-DAT/LOC</ta>
            <ta e="T464" id="Seg_4585" s="T463">NEG</ta>
            <ta e="T466" id="Seg_4586" s="T464">drink-EP-RECP/COLL-PST2.NEG-EP-1SG</ta>
            <ta e="T467" id="Seg_4587" s="T466">age-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T468" id="Seg_4588" s="T467">until</ta>
            <ta e="T469" id="Seg_4589" s="T468">that.much-PROPR-ACC</ta>
            <ta e="T470" id="Seg_4590" s="T469">bury-PST2-EP-1SG</ta>
            <ta e="T471" id="Seg_4591" s="T470">one</ta>
            <ta e="T472" id="Seg_4592" s="T471">shot.glass-ACC</ta>
            <ta e="T473" id="Seg_4593" s="T472">drink-PST2.NEG-EP-1SG</ta>
            <ta e="T474" id="Seg_4594" s="T473">when</ta>
            <ta e="T475" id="Seg_4595" s="T474">NEG</ta>
            <ta e="T476" id="Seg_4596" s="T475">always</ta>
            <ta e="T477" id="Seg_4597" s="T476">drink</ta>
            <ta e="T478" id="Seg_4598" s="T477">go-EP-MED-NEG.CVB</ta>
            <ta e="T479" id="Seg_4599" s="T478">go-EP-PST2-EP-1SG</ta>
            <ta e="T480" id="Seg_4600" s="T479">always</ta>
         </annotation>
         <annotation name="gg" tierref="gg-LaVN">
            <ta e="T5" id="Seg_4601" s="T4">oh.nein</ta>
            <ta e="T6" id="Seg_4602" s="T5">jenes</ta>
            <ta e="T7" id="Seg_4603" s="T6">Lied-PL-ACC</ta>
            <ta e="T8" id="Seg_4604" s="T7">jenes</ta>
            <ta e="T9" id="Seg_4605" s="T8">nun</ta>
            <ta e="T10" id="Seg_4606" s="T9">wer-ACC</ta>
            <ta e="T11" id="Seg_4607" s="T10">tatsächlich</ta>
            <ta e="T12" id="Seg_4608" s="T11">doch</ta>
            <ta e="T14" id="Seg_4609" s="T12">1PL-DAT/LOC</ta>
            <ta e="T15" id="Seg_4610" s="T14">Herde-DAT/LOC</ta>
            <ta e="T16" id="Seg_4611" s="T15">leben-PRS-1PL</ta>
            <ta e="T17" id="Seg_4612" s="T16">MOD</ta>
            <ta e="T18" id="Seg_4613" s="T17">1PL.[NOM]</ta>
            <ta e="T19" id="Seg_4614" s="T18">Rentier-INSTR</ta>
            <ta e="T20" id="Seg_4615" s="T19">nun</ta>
            <ta e="T21" id="Seg_4616" s="T20">Tundra-DAT/LOC</ta>
            <ta e="T22" id="Seg_4617" s="T21">nun</ta>
            <ta e="T23" id="Seg_4618" s="T22">leben-PRS-1PL</ta>
            <ta e="T24" id="Seg_4619" s="T23">Komsomol.[NOM]</ta>
            <ta e="T25" id="Seg_4620" s="T24">kommen-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_4621" s="T25">Komsomol.[NOM]</ta>
            <ta e="T28" id="Seg_4622" s="T27">doch</ta>
            <ta e="T29" id="Seg_4623" s="T28">oh.nein-PL.[NOM]</ta>
            <ta e="T30" id="Seg_4624" s="T29">Komsomol.[NOM]</ta>
            <ta e="T31" id="Seg_4625" s="T30">doch</ta>
            <ta e="T32" id="Seg_4626" s="T31">tatsächlich</ta>
            <ta e="T33" id="Seg_4627" s="T32">ganz</ta>
            <ta e="T34" id="Seg_4628" s="T33">Parteimitglied-PL.[NOM]</ta>
            <ta e="T35" id="Seg_4629" s="T34">kommen-PST1-3PL</ta>
            <ta e="T36" id="Seg_4630" s="T35">ah</ta>
            <ta e="T37" id="Seg_4631" s="T36">oh.nein</ta>
            <ta e="T38" id="Seg_4632" s="T37">sagen-PRS-3PL</ta>
            <ta e="T39" id="Seg_4633" s="T38">1SG-ACC</ta>
            <ta e="T40" id="Seg_4634" s="T39">na</ta>
            <ta e="T41" id="Seg_4635" s="T40">Barbara</ta>
            <ta e="T42" id="Seg_4636" s="T41">Schallplatte-PL-EP-2SG.[NOM]</ta>
            <ta e="T43" id="Seg_4637" s="T42">es.gibt-3PL</ta>
            <ta e="T44" id="Seg_4638" s="T43">1SG.[NOM]</ta>
            <ta e="T45" id="Seg_4639" s="T44">sagen-PRS-1SG</ta>
            <ta e="T46" id="Seg_4640" s="T45">nein</ta>
            <ta e="T47" id="Seg_4641" s="T46">Ruslanova.[NOM]</ta>
            <ta e="T48" id="Seg_4642" s="T47">eigen-3PL.[NOM]</ta>
            <ta e="T49" id="Seg_4643" s="T48">Gericht-VBZ-PST2-3PL</ta>
            <ta e="T50" id="Seg_4644" s="T49">3SG-ACC</ta>
            <ta e="T51" id="Seg_4645" s="T50">sagen-PST2-3PL</ta>
            <ta e="T53" id="Seg_4646" s="T51">wer-VBZ-IMP.2PL</ta>
            <ta e="T56" id="Seg_4647" s="T53">zerbrechen-FREQ-IMP.2PL</ta>
            <ta e="T57" id="Seg_4648" s="T56">3SG.[NOM]</ta>
            <ta e="T58" id="Seg_4649" s="T57">Schallplatte-3SG-ACC</ta>
            <ta e="T59" id="Seg_4650" s="T58">dieses</ta>
            <ta e="T60" id="Seg_4651" s="T59">dolganisch</ta>
            <ta e="T61" id="Seg_4652" s="T60">Mensch-3SG-DAT/LOC</ta>
            <ta e="T62" id="Seg_4653" s="T61">gehen-PST2-3PL</ta>
            <ta e="T65" id="Seg_4654" s="T64">jeder-3SG.[NOM]</ta>
            <ta e="T66" id="Seg_4655" s="T65">zerbrechen-FREQ-IMP.2PL</ta>
            <ta e="T67" id="Seg_4656" s="T66">sagen-PST2-3PL</ta>
            <ta e="T68" id="Seg_4657" s="T67">1SG.[NOM]</ta>
            <ta e="T69" id="Seg_4658" s="T68">sagen-PRS-1SG</ta>
            <ta e="T70" id="Seg_4659" s="T69">NEG.EX</ta>
            <ta e="T71" id="Seg_4660" s="T70">1SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_4661" s="T71">eins</ta>
            <ta e="T73" id="Seg_4662" s="T72">NEG</ta>
            <ta e="T74" id="Seg_4663" s="T73">NEG.EX</ta>
            <ta e="T75" id="Seg_4664" s="T74">was-ACC</ta>
            <ta e="T76" id="Seg_4665" s="T75">singen-EP-CAUS-PRS-2SG</ta>
            <ta e="T77" id="Seg_4666" s="T76">dieses</ta>
            <ta e="T78" id="Seg_4667" s="T77">Plattenspieler-2SG-DAT/LOC</ta>
            <ta e="T79" id="Seg_4668" s="T78">sagen-PRS-3PL</ta>
            <ta e="T80" id="Seg_4669" s="T79">1SG-ACC</ta>
            <ta e="T84" id="Seg_4670" s="T82">AFFIRM</ta>
            <ta e="T85" id="Seg_4671" s="T84">jenes</ta>
            <ta e="T86" id="Seg_4672" s="T85">nun</ta>
            <ta e="T87" id="Seg_4673" s="T86">singen-EP-CAUS-PRS-1SG</ta>
            <ta e="T88" id="Seg_4674" s="T87">selbst-1SG.[NOM]</ta>
            <ta e="T89" id="Seg_4675" s="T88">nun</ta>
            <ta e="T94" id="Seg_4676" s="T92">1SG.[NOM]</ta>
            <ta e="T95" id="Seg_4677" s="T94">selbst-1SG.[NOM]</ta>
            <ta e="T96" id="Seg_4678" s="T95">kaufen-PST2-EP-1SG</ta>
            <ta e="T97" id="Seg_4679" s="T96">AFFIRM</ta>
            <ta e="T98" id="Seg_4680" s="T97">Schwarzes.Meer-DAT/LOC</ta>
            <ta e="T99" id="Seg_4681" s="T98">gehen-CVB.SEQ-1SG</ta>
            <ta e="T100" id="Seg_4682" s="T99">nun</ta>
            <ta e="T101" id="Seg_4683" s="T100">dann</ta>
            <ta e="T102" id="Seg_4684" s="T101">selbst-1SG.[NOM]</ta>
            <ta e="T103" id="Seg_4685" s="T102">kaufen-PST2-EP-1SG</ta>
            <ta e="T104" id="Seg_4686" s="T103">jenes</ta>
            <ta e="T105" id="Seg_4687" s="T104">selbst-1SG.[NOM]</ta>
            <ta e="T106" id="Seg_4688" s="T105">nehmen-CVB.SEQ</ta>
            <ta e="T107" id="Seg_4689" s="T106">nachdem</ta>
            <ta e="T108" id="Seg_4690" s="T107">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T109" id="Seg_4691" s="T108">Nadel-PROPR.[NOM]</ta>
            <ta e="T110" id="Seg_4692" s="T109">einzeln</ta>
            <ta e="T111" id="Seg_4693" s="T110">Schachtel-DIM.[NOM]</ta>
            <ta e="T112" id="Seg_4694" s="T111">EMPH</ta>
            <ta e="T113" id="Seg_4695" s="T112">jenes-ACC</ta>
            <ta e="T114" id="Seg_4696" s="T113">selbst-1PL.[NOM]</ta>
            <ta e="T115" id="Seg_4697" s="T114">umdrehen-PRS-1PL</ta>
            <ta e="T116" id="Seg_4698" s="T115">dann</ta>
            <ta e="T117" id="Seg_4699" s="T116">Schallplatte-ACC</ta>
            <ta e="T118" id="Seg_4700" s="T117">legen-PRS-1PL</ta>
            <ta e="T119" id="Seg_4701" s="T118">jenes</ta>
            <ta e="T122" id="Seg_4702" s="T121">doch</ta>
            <ta e="T127" id="Seg_4703" s="T126">1SG</ta>
            <ta e="T130" id="Seg_4704" s="T129">schön</ta>
            <ta e="T131" id="Seg_4705" s="T130">INTNS</ta>
            <ta e="T132" id="Seg_4706" s="T131">Lied-PROPR</ta>
            <ta e="T136" id="Seg_4707" s="T135">ah</ta>
            <ta e="T137" id="Seg_4708" s="T136">Ruslanova.[NOM]</ta>
            <ta e="T138" id="Seg_4709" s="T137">singen-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_4710" s="T141">vielleicht</ta>
            <ta e="T143" id="Seg_4711" s="T142">doch</ta>
            <ta e="T144" id="Seg_4712" s="T143">jenes-3SG-1SG-ACC</ta>
            <ta e="T145" id="Seg_4713" s="T144">doch</ta>
            <ta e="T146" id="Seg_4714" s="T145">verstecken.[IMP.2SG]</ta>
            <ta e="T147" id="Seg_4715" s="T146">EMPH</ta>
            <ta e="T148" id="Seg_4716" s="T147">Kind-1SG-DAT/LOC</ta>
            <ta e="T149" id="Seg_4717" s="T148">wer.[NOM]</ta>
            <ta e="T150" id="Seg_4718" s="T149">Federbett-DIM.[NOM]</ta>
            <ta e="T151" id="Seg_4719" s="T150">Überreste-3SG.[NOM]</ta>
            <ta e="T152" id="Seg_4720" s="T151">machen-PST2-EP-1SG</ta>
            <ta e="T153" id="Seg_4721" s="T152">klein</ta>
            <ta e="T154" id="Seg_4722" s="T153">Kind.[NOM]</ta>
            <ta e="T155" id="Seg_4723" s="T154">jenes.[NOM]</ta>
            <ta e="T156" id="Seg_4724" s="T155">Inneres-3SG-INSTR</ta>
            <ta e="T157" id="Seg_4725" s="T156">nähen-CVB.SEQ</ta>
            <ta e="T158" id="Seg_4726" s="T157">lassen-CVB.SEQ</ta>
            <ta e="T159" id="Seg_4727" s="T158">nachdem</ta>
            <ta e="T160" id="Seg_4728" s="T159">dorthin</ta>
            <ta e="T161" id="Seg_4729" s="T162">dorthin</ta>
            <ta e="T163" id="Seg_4730" s="T161">legen-PRS-1SG</ta>
            <ta e="T164" id="Seg_4731" s="T163">zwei</ta>
            <ta e="T165" id="Seg_4732" s="T164">Schallplatte-ACC</ta>
            <ta e="T166" id="Seg_4733" s="T165">verstecken-PST2-EP-1SG</ta>
            <ta e="T167" id="Seg_4734" s="T166">EMPH-jetzt-DAT/LOC</ta>
            <ta e="T168" id="Seg_4735" s="T167">bis.zu</ta>
            <ta e="T169" id="Seg_4736" s="T168">verstecken-PST2-EP-1SG</ta>
            <ta e="T173" id="Seg_4737" s="T171">Ruslanova.[NOM]</ta>
            <ta e="T174" id="Seg_4738" s="T173">eigen-3PL-ACC</ta>
            <ta e="T175" id="Seg_4739" s="T174">EXCL</ta>
            <ta e="T176" id="Seg_4740" s="T175">tatsächlich</ta>
            <ta e="T187" id="Seg_4741" s="T185">jenes</ta>
            <ta e="T188" id="Seg_4742" s="T187">Schallplatte-PL-1SG-ACC</ta>
            <ta e="T189" id="Seg_4743" s="T188">dieses</ta>
            <ta e="T190" id="Seg_4744" s="T189">Kind-PL.[NOM]</ta>
            <ta e="T191" id="Seg_4745" s="T190">wachsen-CVB.SEQ</ta>
            <ta e="T192" id="Seg_4746" s="T191">gehen-CVB.SEQ-3PL</ta>
            <ta e="T193" id="Seg_4747" s="T192">ganz-ADVZ</ta>
            <ta e="T194" id="Seg_4748" s="T193">zerbrechen-FREQ-PST2-3PL</ta>
            <ta e="T195" id="Seg_4749" s="T194">EMPH</ta>
            <ta e="T196" id="Seg_4750" s="T195">dieses</ta>
            <ta e="T197" id="Seg_4751" s="T196">versehentlich</ta>
            <ta e="T198" id="Seg_4752" s="T197">Ljusja.[NOM]</ta>
            <ta e="T199" id="Seg_4753" s="T198">jenes</ta>
            <ta e="T200" id="Seg_4754" s="T199">Hausfrau</ta>
            <ta e="T201" id="Seg_4755" s="T200">EVID</ta>
            <ta e="T202" id="Seg_4756" s="T201">ganz-3SG-ACC</ta>
            <ta e="T216" id="Seg_4757" s="T214">dann</ta>
            <ta e="T217" id="Seg_4758" s="T216">Stalin-EP-2SG.[NOM]</ta>
            <ta e="T218" id="Seg_4759" s="T217">sein-PST1-3SG</ta>
            <ta e="T219" id="Seg_4760" s="T218">MOD</ta>
            <ta e="T220" id="Seg_4761" s="T219">AFFIRM</ta>
            <ta e="T221" id="Seg_4762" s="T220">dann</ta>
            <ta e="T222" id="Seg_4763" s="T221">jenes</ta>
            <ta e="T223" id="Seg_4764" s="T222">Gericht</ta>
            <ta e="T224" id="Seg_4765" s="T223">jenes</ta>
            <ta e="T225" id="Seg_4766" s="T224">Hinterteil.[NOM]</ta>
            <ta e="T226" id="Seg_4767" s="T225">3SG-ACC</ta>
            <ta e="T227" id="Seg_4768" s="T226">Gericht-VBZ-PST2-3PL</ta>
            <ta e="T228" id="Seg_4769" s="T227">3SG-ACC</ta>
            <ta e="T229" id="Seg_4770" s="T228">jenes</ta>
            <ta e="T245" id="Seg_4771" s="T244">dann</ta>
            <ta e="T246" id="Seg_4772" s="T245">3SG.[NOM]</ta>
            <ta e="T247" id="Seg_4773" s="T246">Krieg-DAT/LOC</ta>
            <ta e="T248" id="Seg_4774" s="T247">jenes</ta>
            <ta e="T249" id="Seg_4775" s="T248">gehen-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_4776" s="T249">EMPH</ta>
            <ta e="T251" id="Seg_4777" s="T250">Krieg-DAT/LOC</ta>
            <ta e="T252" id="Seg_4778" s="T251">nun</ta>
            <ta e="T253" id="Seg_4779" s="T252">singen-EP-CAUS-PRS-3PL</ta>
            <ta e="T254" id="Seg_4780" s="T253">EMPH</ta>
            <ta e="T255" id="Seg_4781" s="T254">Künstler-PL-ACC</ta>
            <ta e="T256" id="Seg_4782" s="T255">nun</ta>
            <ta e="T265" id="Seg_4783" s="T264">jenes.[NOM]</ta>
            <ta e="T266" id="Seg_4784" s="T265">wegen</ta>
            <ta e="T267" id="Seg_4785" s="T266">3PL-ACC</ta>
            <ta e="T268" id="Seg_4786" s="T267">Gericht-VBZ-PRS-3PL</ta>
            <ta e="T269" id="Seg_4787" s="T268">oh</ta>
            <ta e="T271" id="Seg_4788" s="T269">Unglück-PROPR-PL-ACC</ta>
            <ta e="T272" id="Seg_4789" s="T271">Ehemann-3SG-ACC</ta>
            <ta e="T273" id="Seg_4790" s="T272">mit</ta>
            <ta e="T274" id="Seg_4791" s="T273">zwei-COLL-3PL-ACC</ta>
            <ta e="T275" id="Seg_4792" s="T274">Gericht-VBZ-PST2-3PL</ta>
            <ta e="T276" id="Seg_4793" s="T275">offenbar</ta>
            <ta e="T294" id="Seg_4794" s="T293">doch</ta>
            <ta e="T295" id="Seg_4795" s="T294">jenes</ta>
            <ta e="T296" id="Seg_4796" s="T295">Lied-PL-ACC</ta>
            <ta e="T297" id="Seg_4797" s="T296">zuhören-NEG-PTCP.FUT-DAT/LOC</ta>
            <ta e="T298" id="Seg_4798" s="T297">3PL.[NOM]</ta>
            <ta e="T299" id="Seg_4799" s="T298">einfach</ta>
            <ta e="T300" id="Seg_4800" s="T299">3PL.[NOM]</ta>
            <ta e="T301" id="Seg_4801" s="T300">was-ACC</ta>
            <ta e="T302" id="Seg_4802" s="T301">INDEF</ta>
            <ta e="T312" id="Seg_4803" s="T311">dann</ta>
            <ta e="T313" id="Seg_4804" s="T312">jenes</ta>
            <ta e="T314" id="Seg_4805" s="T313">dieses</ta>
            <ta e="T315" id="Seg_4806" s="T314">jenes</ta>
            <ta e="T316" id="Seg_4807" s="T315">unsere.[NOM]</ta>
            <ta e="T317" id="Seg_4808" s="T316">jenes</ta>
            <ta e="T318" id="Seg_4809" s="T317">ausziehen-ITER-PRS-3PL</ta>
            <ta e="T319" id="Seg_4810" s="T318">jenes</ta>
            <ta e="T330" id="Seg_4811" s="T329">doch</ta>
            <ta e="T331" id="Seg_4812" s="T330">offenbar</ta>
            <ta e="T332" id="Seg_4813" s="T331">nun</ta>
            <ta e="T333" id="Seg_4814" s="T332">doch</ta>
            <ta e="T334" id="Seg_4815" s="T333">jenes</ta>
            <ta e="T335" id="Seg_4816" s="T334">doch</ta>
            <ta e="T336" id="Seg_4817" s="T335">wegnehmen-EMOT-CVB.SEQ</ta>
            <ta e="T337" id="Seg_4818" s="T336">werfen-PRS-3PL</ta>
            <ta e="T338" id="Seg_4819" s="T337">jenes</ta>
            <ta e="T339" id="Seg_4820" s="T338">verstecken-PRS-1PL</ta>
            <ta e="T340" id="Seg_4821" s="T339">doch</ta>
            <ta e="T341" id="Seg_4822" s="T340">jenes-3SG-2SG-ACC</ta>
            <ta e="T342" id="Seg_4823" s="T341">EXCL</ta>
            <ta e="T344" id="Seg_4824" s="T343">tatsächlich</ta>
            <ta e="T348" id="Seg_4825" s="T346">doch</ta>
            <ta e="T349" id="Seg_4826" s="T348">jetzt</ta>
            <ta e="T350" id="Seg_4827" s="T349">erinnern-PTCP.COND-DAT/LOC</ta>
            <ta e="T357" id="Seg_4828" s="T355">noo</ta>
            <ta e="T358" id="Seg_4829" s="T357">Mensch.[NOM]</ta>
            <ta e="T359" id="Seg_4830" s="T358">Angst.haben-PTCP.FUT</ta>
            <ta e="T360" id="Seg_4831" s="T359">sein-FUT.[3SG]</ta>
            <ta e="T361" id="Seg_4832" s="T360">dann</ta>
            <ta e="T362" id="Seg_4833" s="T361">kommen-PRS-3PL</ta>
            <ta e="T363" id="Seg_4834" s="T362">zerbrechen-FREQ-PTCP.PRS-3PL</ta>
            <ta e="T364" id="Seg_4835" s="T363">nun</ta>
            <ta e="T365" id="Seg_4836" s="T364">kommen-PRS-3PL</ta>
            <ta e="T372" id="Seg_4837" s="T370">also</ta>
            <ta e="T373" id="Seg_4838" s="T372">mein</ta>
            <ta e="T374" id="Seg_4839" s="T373">Freundin-1SG.[NOM]</ta>
            <ta e="T379" id="Seg_4840" s="T378">oh.nein</ta>
            <ta e="T381" id="Seg_4841" s="T379">gehen-NEG.[3SG]</ta>
            <ta e="T382" id="Seg_4842" s="T381">sterben-PST1-1PL</ta>
            <ta e="T383" id="Seg_4843" s="T382">nur</ta>
            <ta e="T384" id="Seg_4844" s="T383">wer-2SG-ACC</ta>
            <ta e="T385" id="Seg_4845" s="T384">verstecken.[IMP.2SG]</ta>
            <ta e="T386" id="Seg_4846" s="T385">sagen-PRS.[3SG]</ta>
            <ta e="T387" id="Seg_4847" s="T386">1SG-ACC</ta>
            <ta e="T388" id="Seg_4848" s="T387">nun</ta>
            <ta e="T389" id="Seg_4849" s="T388">warum</ta>
            <ta e="T390" id="Seg_4850" s="T389">Q</ta>
            <ta e="T391" id="Seg_4851" s="T390">na</ta>
            <ta e="T392" id="Seg_4852" s="T391">zerbrechen-FREQ-PRS-3PL</ta>
            <ta e="T393" id="Seg_4853" s="T392">Ruslanova.[NOM]</ta>
            <ta e="T394" id="Seg_4854" s="T393">Schallplatte-PL-3SG-ACC</ta>
            <ta e="T395" id="Seg_4855" s="T394">1SG.[NOM]</ta>
            <ta e="T396" id="Seg_4856" s="T395">eins-DIM-1SG-ACC</ta>
            <ta e="T397" id="Seg_4857" s="T396">verstecken-PST2-EP-1SG</ta>
            <ta e="T398" id="Seg_4858" s="T397">sagen-PRS.[3SG]</ta>
            <ta e="T399" id="Seg_4859" s="T398">Freund-EP-1SG.[NOM]</ta>
            <ta e="T400" id="Seg_4860" s="T399">doch</ta>
            <ta e="T401" id="Seg_4861" s="T400">jenes</ta>
            <ta e="T402" id="Seg_4862" s="T401">zwei-COLL</ta>
            <ta e="T403" id="Seg_4863" s="T402">verstecken-PRS-1PL</ta>
            <ta e="T404" id="Seg_4864" s="T403">EMPH</ta>
            <ta e="T409" id="Seg_4865" s="T408">Anna.[NOM]</ta>
            <ta e="T410" id="Seg_4866" s="T409">sein-PST1-3SG</ta>
            <ta e="T412" id="Seg_4867" s="T411">dieses</ta>
            <ta e="T413" id="Seg_4868" s="T412">Nina.[NOM]</ta>
            <ta e="T414" id="Seg_4869" s="T413">Mutter-3SG.[NOM]</ta>
            <ta e="T415" id="Seg_4870" s="T414">dieses</ta>
            <ta e="T416" id="Seg_4871" s="T415">Goruo</ta>
            <ta e="T417" id="Seg_4872" s="T416">Nina.[NOM]</ta>
            <ta e="T418" id="Seg_4873" s="T417">Mutter-3SG.[NOM]</ta>
            <ta e="T419" id="Seg_4874" s="T418">sein-PST1-3SG</ta>
            <ta e="T420" id="Seg_4875" s="T419">Anna.[NOM]</ta>
            <ta e="T431" id="Seg_4876" s="T429">dolganisch</ta>
            <ta e="T432" id="Seg_4877" s="T431">Mensch-3PL.[NOM]</ta>
            <ta e="T433" id="Seg_4878" s="T432">sein-PST1-3PL</ta>
            <ta e="T434" id="Seg_4879" s="T433">unsere.[NOM]</ta>
            <ta e="T435" id="Seg_4880" s="T434">Dolgane-PL.[NOM]</ta>
            <ta e="T436" id="Seg_4881" s="T435">sein-PST1-3PL</ta>
            <ta e="T443" id="Seg_4882" s="T442">mancher</ta>
            <ta e="T444" id="Seg_4883" s="T443">Russe-DRV-PTCP.HAB</ta>
            <ta e="T445" id="Seg_4884" s="T444">sein-PST1-3SG</ta>
            <ta e="T446" id="Seg_4885" s="T445">mancher</ta>
            <ta e="T460" id="Seg_4886" s="T459">Schnaps-ACC</ta>
            <ta e="T461" id="Seg_4887" s="T460">NEG</ta>
            <ta e="T462" id="Seg_4888" s="T461">trinken-PST2.NEG-EP-1SG</ta>
            <ta e="T463" id="Seg_4889" s="T280">Zeit-DAT/LOC</ta>
            <ta e="T464" id="Seg_4890" s="T463">NEG</ta>
            <ta e="T466" id="Seg_4891" s="T464">trinken-EP-RECP/COLL-PST2.NEG-EP-1SG</ta>
            <ta e="T467" id="Seg_4892" s="T466">altern-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T468" id="Seg_4893" s="T467">bis.zu</ta>
            <ta e="T469" id="Seg_4894" s="T468">so.viel-PROPR-ACC</ta>
            <ta e="T470" id="Seg_4895" s="T469">begraben-PST2-EP-1SG</ta>
            <ta e="T471" id="Seg_4896" s="T470">eins</ta>
            <ta e="T472" id="Seg_4897" s="T471">Schnapsglas-ACC</ta>
            <ta e="T473" id="Seg_4898" s="T472">trinken-PST2.NEG-EP-1SG</ta>
            <ta e="T474" id="Seg_4899" s="T473">wann</ta>
            <ta e="T475" id="Seg_4900" s="T474">NEG</ta>
            <ta e="T476" id="Seg_4901" s="T475">immer</ta>
            <ta e="T477" id="Seg_4902" s="T476">trinken</ta>
            <ta e="T478" id="Seg_4903" s="T477">gehen-EP-MED-NEG.CVB</ta>
            <ta e="T479" id="Seg_4904" s="T478">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T480" id="Seg_4905" s="T479">immer</ta>
         </annotation>
         <annotation name="gr" tierref="gr-LaVN">
            <ta e="T5" id="Seg_4906" s="T4">вот.беда</ta>
            <ta e="T6" id="Seg_4907" s="T5">тот</ta>
            <ta e="T7" id="Seg_4908" s="T6">песня-PL-ACC</ta>
            <ta e="T8" id="Seg_4909" s="T7">тот</ta>
            <ta e="T9" id="Seg_4910" s="T8">вот</ta>
            <ta e="T10" id="Seg_4911" s="T9">кто-ACC</ta>
            <ta e="T11" id="Seg_4912" s="T10">вправду</ta>
            <ta e="T12" id="Seg_4913" s="T11">вот</ta>
            <ta e="T14" id="Seg_4914" s="T12">1PL-DAT/LOC</ta>
            <ta e="T15" id="Seg_4915" s="T14">стадо-DAT/LOC</ta>
            <ta e="T16" id="Seg_4916" s="T15">жить-PRS-1PL</ta>
            <ta e="T17" id="Seg_4917" s="T16">MOD</ta>
            <ta e="T18" id="Seg_4918" s="T17">1PL.[NOM]</ta>
            <ta e="T19" id="Seg_4919" s="T18">олень-INSTR</ta>
            <ta e="T20" id="Seg_4920" s="T19">вот</ta>
            <ta e="T21" id="Seg_4921" s="T20">тундра-DAT/LOC</ta>
            <ta e="T22" id="Seg_4922" s="T21">вот</ta>
            <ta e="T23" id="Seg_4923" s="T22">жить-PRS-1PL</ta>
            <ta e="T24" id="Seg_4924" s="T23">комсомол.[NOM]</ta>
            <ta e="T25" id="Seg_4925" s="T24">приходить-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_4926" s="T25">комсомол.[NOM]</ta>
            <ta e="T28" id="Seg_4927" s="T27">вот</ta>
            <ta e="T29" id="Seg_4928" s="T28">вот.беда-PL.[NOM]</ta>
            <ta e="T30" id="Seg_4929" s="T29">комсомол.[NOM]</ta>
            <ta e="T31" id="Seg_4930" s="T30">вот</ta>
            <ta e="T32" id="Seg_4931" s="T31">вправду</ta>
            <ta e="T33" id="Seg_4932" s="T32">вовсе</ta>
            <ta e="T34" id="Seg_4933" s="T33">партийний-PL.[NOM]</ta>
            <ta e="T35" id="Seg_4934" s="T34">приходить-PST1-3PL</ta>
            <ta e="T36" id="Seg_4935" s="T35">ах</ta>
            <ta e="T37" id="Seg_4936" s="T36">вот.беда</ta>
            <ta e="T38" id="Seg_4937" s="T37">говорить-PRS-3PL</ta>
            <ta e="T39" id="Seg_4938" s="T38">1SG-ACC</ta>
            <ta e="T40" id="Seg_4939" s="T39">эй</ta>
            <ta e="T41" id="Seg_4940" s="T40">Варвара</ta>
            <ta e="T42" id="Seg_4941" s="T41">пластинка-PL-EP-2SG.[NOM]</ta>
            <ta e="T43" id="Seg_4942" s="T42">есть-3PL</ta>
            <ta e="T44" id="Seg_4943" s="T43">1SG.[NOM]</ta>
            <ta e="T45" id="Seg_4944" s="T44">говорить-PRS-1SG</ta>
            <ta e="T46" id="Seg_4945" s="T45">нет</ta>
            <ta e="T47" id="Seg_4946" s="T46">Русланова.[NOM]</ta>
            <ta e="T48" id="Seg_4947" s="T47">собственный-3PL.[NOM]</ta>
            <ta e="T49" id="Seg_4948" s="T48">суд-VBZ-PST2-3PL</ta>
            <ta e="T50" id="Seg_4949" s="T49">3SG-ACC</ta>
            <ta e="T51" id="Seg_4950" s="T50">говорить-PST2-3PL</ta>
            <ta e="T53" id="Seg_4951" s="T51">кто-VBZ-IMP.2PL</ta>
            <ta e="T56" id="Seg_4952" s="T53">разбивать-FREQ-IMP.2PL</ta>
            <ta e="T57" id="Seg_4953" s="T56">3SG.[NOM]</ta>
            <ta e="T58" id="Seg_4954" s="T57">пластинка-3SG-ACC</ta>
            <ta e="T59" id="Seg_4955" s="T58">этот</ta>
            <ta e="T60" id="Seg_4956" s="T59">долганский</ta>
            <ta e="T61" id="Seg_4957" s="T60">человек-3SG-DAT/LOC</ta>
            <ta e="T62" id="Seg_4958" s="T61">идти-PST2-3PL</ta>
            <ta e="T65" id="Seg_4959" s="T64">каждый-3SG.[NOM]</ta>
            <ta e="T66" id="Seg_4960" s="T65">разбивать-FREQ-IMP.2PL</ta>
            <ta e="T67" id="Seg_4961" s="T66">говорить-PST2-3PL</ta>
            <ta e="T68" id="Seg_4962" s="T67">1SG.[NOM]</ta>
            <ta e="T69" id="Seg_4963" s="T68">говорить-PRS-1SG</ta>
            <ta e="T70" id="Seg_4964" s="T69">NEG.EX</ta>
            <ta e="T71" id="Seg_4965" s="T70">1SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_4966" s="T71">один</ta>
            <ta e="T73" id="Seg_4967" s="T72">NEG</ta>
            <ta e="T74" id="Seg_4968" s="T73">NEG.EX</ta>
            <ta e="T75" id="Seg_4969" s="T74">что-ACC</ta>
            <ta e="T76" id="Seg_4970" s="T75">петь-EP-CAUS-PRS-2SG</ta>
            <ta e="T77" id="Seg_4971" s="T76">тот</ta>
            <ta e="T78" id="Seg_4972" s="T77">патефон-2SG-DAT/LOC</ta>
            <ta e="T79" id="Seg_4973" s="T78">говорить-PRS-3PL</ta>
            <ta e="T80" id="Seg_4974" s="T79">1SG-ACC</ta>
            <ta e="T84" id="Seg_4975" s="T82">AFFIRM</ta>
            <ta e="T85" id="Seg_4976" s="T84">тот</ta>
            <ta e="T86" id="Seg_4977" s="T85">вот</ta>
            <ta e="T87" id="Seg_4978" s="T86">петь-EP-CAUS-PRS-1SG</ta>
            <ta e="T88" id="Seg_4979" s="T87">сам-1SG.[NOM]</ta>
            <ta e="T89" id="Seg_4980" s="T88">вот</ta>
            <ta e="T94" id="Seg_4981" s="T92">1SG.[NOM]</ta>
            <ta e="T95" id="Seg_4982" s="T94">сам-1SG.[NOM]</ta>
            <ta e="T96" id="Seg_4983" s="T95">покупать-PST2-EP-1SG</ta>
            <ta e="T97" id="Seg_4984" s="T96">AFFIRM</ta>
            <ta e="T98" id="Seg_4985" s="T97">Черное.Море-DAT/LOC</ta>
            <ta e="T99" id="Seg_4986" s="T98">идти-CVB.SEQ-1SG</ta>
            <ta e="T100" id="Seg_4987" s="T99">вот</ta>
            <ta e="T101" id="Seg_4988" s="T100">тогда</ta>
            <ta e="T102" id="Seg_4989" s="T101">сам-1SG.[NOM]</ta>
            <ta e="T103" id="Seg_4990" s="T102">покупать-PST2-EP-1SG</ta>
            <ta e="T104" id="Seg_4991" s="T103">тот</ta>
            <ta e="T105" id="Seg_4992" s="T104">сам-1SG.[NOM]</ta>
            <ta e="T106" id="Seg_4993" s="T105">взять-CVB.SEQ</ta>
            <ta e="T107" id="Seg_4994" s="T106">после</ta>
            <ta e="T108" id="Seg_4995" s="T107">тот-3SG-2SG.[NOM]</ta>
            <ta e="T109" id="Seg_4996" s="T108">пгла-PROPR.[NOM]</ta>
            <ta e="T110" id="Seg_4997" s="T109">отдельно</ta>
            <ta e="T111" id="Seg_4998" s="T110">коробка-DIM.[NOM]</ta>
            <ta e="T112" id="Seg_4999" s="T111">EMPH</ta>
            <ta e="T113" id="Seg_5000" s="T112">тот-ACC</ta>
            <ta e="T114" id="Seg_5001" s="T113">сам-1PL.[NOM]</ta>
            <ta e="T115" id="Seg_5002" s="T114">повернуть-PRS-1PL</ta>
            <ta e="T116" id="Seg_5003" s="T115">потом</ta>
            <ta e="T117" id="Seg_5004" s="T116">пластинка-ACC</ta>
            <ta e="T118" id="Seg_5005" s="T117">класть-PRS-1PL</ta>
            <ta e="T119" id="Seg_5006" s="T118">тот</ta>
            <ta e="T122" id="Seg_5007" s="T121">вот</ta>
            <ta e="T127" id="Seg_5008" s="T126">1SG</ta>
            <ta e="T130" id="Seg_5009" s="T129">красивый</ta>
            <ta e="T131" id="Seg_5010" s="T130">INTNS</ta>
            <ta e="T132" id="Seg_5011" s="T131">песня-PROPR</ta>
            <ta e="T136" id="Seg_5012" s="T135">ах</ta>
            <ta e="T137" id="Seg_5013" s="T136">Русланова.[NOM]</ta>
            <ta e="T138" id="Seg_5014" s="T137">петь-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_5015" s="T141">может</ta>
            <ta e="T143" id="Seg_5016" s="T142">вот</ta>
            <ta e="T144" id="Seg_5017" s="T143">тот-3SG-1SG-ACC</ta>
            <ta e="T145" id="Seg_5018" s="T144">вот</ta>
            <ta e="T146" id="Seg_5019" s="T145">скрывать.[IMP.2SG]</ta>
            <ta e="T147" id="Seg_5020" s="T146">EMPH</ta>
            <ta e="T148" id="Seg_5021" s="T147">ребенок-1SG-DAT/LOC</ta>
            <ta e="T149" id="Seg_5022" s="T148">кто.[NOM]</ta>
            <ta e="T150" id="Seg_5023" s="T149">перина-DIM.[NOM]</ta>
            <ta e="T151" id="Seg_5024" s="T150">остатки-3SG.[NOM]</ta>
            <ta e="T152" id="Seg_5025" s="T151">делать-PST2-EP-1SG</ta>
            <ta e="T153" id="Seg_5026" s="T152">маленький</ta>
            <ta e="T154" id="Seg_5027" s="T153">ребенок.[NOM]</ta>
            <ta e="T155" id="Seg_5028" s="T154">тот.[NOM]</ta>
            <ta e="T156" id="Seg_5029" s="T155">нутро-3SG-INSTR</ta>
            <ta e="T157" id="Seg_5030" s="T156">шить-CVB.SEQ</ta>
            <ta e="T158" id="Seg_5031" s="T157">оставлять-CVB.SEQ</ta>
            <ta e="T159" id="Seg_5032" s="T158">после</ta>
            <ta e="T160" id="Seg_5033" s="T159">туда</ta>
            <ta e="T161" id="Seg_5034" s="T162">туда</ta>
            <ta e="T163" id="Seg_5035" s="T161">класть-PRS-1SG</ta>
            <ta e="T164" id="Seg_5036" s="T163">два</ta>
            <ta e="T165" id="Seg_5037" s="T164">пластинка-ACC</ta>
            <ta e="T166" id="Seg_5038" s="T165">скрывать-PST2-EP-1SG</ta>
            <ta e="T167" id="Seg_5039" s="T166">EMPH-теперь-DAT/LOC</ta>
            <ta e="T168" id="Seg_5040" s="T167">пока</ta>
            <ta e="T169" id="Seg_5041" s="T168">скрывать-PST2-EP-1SG</ta>
            <ta e="T173" id="Seg_5042" s="T171">Русланова.[NOM]</ta>
            <ta e="T174" id="Seg_5043" s="T173">собственный-3PL-ACC</ta>
            <ta e="T175" id="Seg_5044" s="T174">EXCL</ta>
            <ta e="T176" id="Seg_5045" s="T175">вправду</ta>
            <ta e="T187" id="Seg_5046" s="T185">тот</ta>
            <ta e="T188" id="Seg_5047" s="T187">пластинка-PL-1SG-ACC</ta>
            <ta e="T189" id="Seg_5048" s="T188">этот</ta>
            <ta e="T190" id="Seg_5049" s="T189">ребенок-PL.[NOM]</ta>
            <ta e="T191" id="Seg_5050" s="T190">расти-CVB.SEQ</ta>
            <ta e="T192" id="Seg_5051" s="T191">идти-CVB.SEQ-3PL</ta>
            <ta e="T193" id="Seg_5052" s="T192">полностью-ADVZ</ta>
            <ta e="T194" id="Seg_5053" s="T193">разбивать-FREQ-PST2-3PL</ta>
            <ta e="T195" id="Seg_5054" s="T194">EMPH</ta>
            <ta e="T196" id="Seg_5055" s="T195">этот</ta>
            <ta e="T197" id="Seg_5056" s="T196">нечаянно</ta>
            <ta e="T198" id="Seg_5057" s="T197">Люся.[NOM]</ta>
            <ta e="T199" id="Seg_5058" s="T198">тот</ta>
            <ta e="T200" id="Seg_5059" s="T199">хозяйка</ta>
            <ta e="T201" id="Seg_5060" s="T200">EVID</ta>
            <ta e="T202" id="Seg_5061" s="T201">целый-3SG-ACC</ta>
            <ta e="T216" id="Seg_5062" s="T214">тогда</ta>
            <ta e="T217" id="Seg_5063" s="T216">Сталин-EP-2SG.[NOM]</ta>
            <ta e="T218" id="Seg_5064" s="T217">быть-PST1-3SG</ta>
            <ta e="T219" id="Seg_5065" s="T218">MOD</ta>
            <ta e="T220" id="Seg_5066" s="T219">AFFIRM</ta>
            <ta e="T221" id="Seg_5067" s="T220">тогда</ta>
            <ta e="T222" id="Seg_5068" s="T221">тот</ta>
            <ta e="T223" id="Seg_5069" s="T222">суд</ta>
            <ta e="T224" id="Seg_5070" s="T223">тот</ta>
            <ta e="T225" id="Seg_5071" s="T224">задняя.часть.[NOM]</ta>
            <ta e="T226" id="Seg_5072" s="T225">3SG-ACC</ta>
            <ta e="T227" id="Seg_5073" s="T226">суд-VBZ-PST2-3PL</ta>
            <ta e="T228" id="Seg_5074" s="T227">3SG-ACC</ta>
            <ta e="T229" id="Seg_5075" s="T228">тот</ta>
            <ta e="T245" id="Seg_5076" s="T244">тогда</ta>
            <ta e="T246" id="Seg_5077" s="T245">3SG.[NOM]</ta>
            <ta e="T247" id="Seg_5078" s="T246">война-DAT/LOC</ta>
            <ta e="T248" id="Seg_5079" s="T247">тот</ta>
            <ta e="T249" id="Seg_5080" s="T248">идти-PRS.[3SG]</ta>
            <ta e="T250" id="Seg_5081" s="T249">EMPH</ta>
            <ta e="T251" id="Seg_5082" s="T250">война-DAT/LOC</ta>
            <ta e="T252" id="Seg_5083" s="T251">вот</ta>
            <ta e="T253" id="Seg_5084" s="T252">петь-EP-CAUS-PRS-3PL</ta>
            <ta e="T254" id="Seg_5085" s="T253">EMPH</ta>
            <ta e="T255" id="Seg_5086" s="T254">артист-PL-ACC</ta>
            <ta e="T256" id="Seg_5087" s="T255">вот</ta>
            <ta e="T265" id="Seg_5088" s="T264">тот.[NOM]</ta>
            <ta e="T266" id="Seg_5089" s="T265">из_за</ta>
            <ta e="T267" id="Seg_5090" s="T266">3PL-ACC</ta>
            <ta e="T268" id="Seg_5091" s="T267">суд-VBZ-PRS-3PL</ta>
            <ta e="T269" id="Seg_5092" s="T268">о</ta>
            <ta e="T271" id="Seg_5093" s="T269">беда-PROPR-PL-ACC</ta>
            <ta e="T272" id="Seg_5094" s="T271">муж-3SG-ACC</ta>
            <ta e="T273" id="Seg_5095" s="T272">с</ta>
            <ta e="T274" id="Seg_5096" s="T273">два-COLL-3PL-ACC</ta>
            <ta e="T275" id="Seg_5097" s="T274">суд-VBZ-PST2-3PL</ta>
            <ta e="T276" id="Seg_5098" s="T275">очевидно</ta>
            <ta e="T294" id="Seg_5099" s="T293">вот</ta>
            <ta e="T295" id="Seg_5100" s="T294">тот</ta>
            <ta e="T296" id="Seg_5101" s="T295">песня-PL-ACC</ta>
            <ta e="T297" id="Seg_5102" s="T296">слушать-NEG-PTCP.FUT-DAT/LOC</ta>
            <ta e="T298" id="Seg_5103" s="T297">3PL.[NOM]</ta>
            <ta e="T299" id="Seg_5104" s="T298">просто</ta>
            <ta e="T300" id="Seg_5105" s="T299">3PL.[NOM]</ta>
            <ta e="T301" id="Seg_5106" s="T300">что-ACC</ta>
            <ta e="T302" id="Seg_5107" s="T301">INDEF</ta>
            <ta e="T312" id="Seg_5108" s="T311">тогда</ta>
            <ta e="T313" id="Seg_5109" s="T312">тот</ta>
            <ta e="T314" id="Seg_5110" s="T313">тот</ta>
            <ta e="T315" id="Seg_5111" s="T314">тот</ta>
            <ta e="T316" id="Seg_5112" s="T315">наши.[NOM]</ta>
            <ta e="T317" id="Seg_5113" s="T316">тот</ta>
            <ta e="T318" id="Seg_5114" s="T317">снимать-ITER-PRS-3PL</ta>
            <ta e="T319" id="Seg_5115" s="T318">тот</ta>
            <ta e="T330" id="Seg_5116" s="T329">вот</ta>
            <ta e="T331" id="Seg_5117" s="T330">очевидно</ta>
            <ta e="T332" id="Seg_5118" s="T331">вот</ta>
            <ta e="T333" id="Seg_5119" s="T332">вот</ta>
            <ta e="T334" id="Seg_5120" s="T333">тот</ta>
            <ta e="T335" id="Seg_5121" s="T334">вот</ta>
            <ta e="T336" id="Seg_5122" s="T335">снимать-EMOT-CVB.SEQ</ta>
            <ta e="T337" id="Seg_5123" s="T336">бросать-PRS-3PL</ta>
            <ta e="T338" id="Seg_5124" s="T337">тот</ta>
            <ta e="T339" id="Seg_5125" s="T338">скрывать-PRS-1PL</ta>
            <ta e="T340" id="Seg_5126" s="T339">вот</ta>
            <ta e="T341" id="Seg_5127" s="T340">тот-3SG-2SG-ACC</ta>
            <ta e="T342" id="Seg_5128" s="T341">EXCL</ta>
            <ta e="T344" id="Seg_5129" s="T343">вправду</ta>
            <ta e="T348" id="Seg_5130" s="T346">вот</ta>
            <ta e="T349" id="Seg_5131" s="T348">теперь</ta>
            <ta e="T350" id="Seg_5132" s="T349">помнить-PTCP.COND-DAT/LOC</ta>
            <ta e="T357" id="Seg_5133" s="T355">ноо</ta>
            <ta e="T358" id="Seg_5134" s="T357">человек.[NOM]</ta>
            <ta e="T359" id="Seg_5135" s="T358">бояться-PTCP.FUT</ta>
            <ta e="T360" id="Seg_5136" s="T359">быть-FUT.[3SG]</ta>
            <ta e="T361" id="Seg_5137" s="T360">тогда</ta>
            <ta e="T362" id="Seg_5138" s="T361">приходить-PRS-3PL</ta>
            <ta e="T363" id="Seg_5139" s="T362">разбивать-FREQ-PTCP.PRS-3PL</ta>
            <ta e="T364" id="Seg_5140" s="T363">вот</ta>
            <ta e="T365" id="Seg_5141" s="T364">приходить-PRS-3PL</ta>
            <ta e="T372" id="Seg_5142" s="T370">ну</ta>
            <ta e="T373" id="Seg_5143" s="T372">мой</ta>
            <ta e="T374" id="Seg_5144" s="T373">подружка-1SG.[NOM]</ta>
            <ta e="T379" id="Seg_5145" s="T378">вот.беда</ta>
            <ta e="T381" id="Seg_5146" s="T379">идти-NEG.[3SG]</ta>
            <ta e="T382" id="Seg_5147" s="T381">умирать-PST1-1PL</ta>
            <ta e="T383" id="Seg_5148" s="T382">только</ta>
            <ta e="T384" id="Seg_5149" s="T383">кто-2SG-ACC</ta>
            <ta e="T385" id="Seg_5150" s="T384">скрывать.[IMP.2SG]</ta>
            <ta e="T386" id="Seg_5151" s="T385">говорить-PRS.[3SG]</ta>
            <ta e="T387" id="Seg_5152" s="T386">1SG-ACC</ta>
            <ta e="T388" id="Seg_5153" s="T387">вот</ta>
            <ta e="T389" id="Seg_5154" s="T388">почему</ta>
            <ta e="T390" id="Seg_5155" s="T389">Q</ta>
            <ta e="T391" id="Seg_5156" s="T390">эй</ta>
            <ta e="T392" id="Seg_5157" s="T391">разбивать-FREQ-PRS-3PL</ta>
            <ta e="T393" id="Seg_5158" s="T392">Русланова.[NOM]</ta>
            <ta e="T394" id="Seg_5159" s="T393">пластинка-PL-3SG-ACC</ta>
            <ta e="T395" id="Seg_5160" s="T394">1SG.[NOM]</ta>
            <ta e="T396" id="Seg_5161" s="T395">один-DIM-1SG-ACC</ta>
            <ta e="T397" id="Seg_5162" s="T396">скрывать-PST2-EP-1SG</ta>
            <ta e="T398" id="Seg_5163" s="T397">говорить-PRS.[3SG]</ta>
            <ta e="T399" id="Seg_5164" s="T398">друг-EP-1SG.[NOM]</ta>
            <ta e="T400" id="Seg_5165" s="T399">вот</ta>
            <ta e="T401" id="Seg_5166" s="T400">тот</ta>
            <ta e="T402" id="Seg_5167" s="T401">два-COLL</ta>
            <ta e="T403" id="Seg_5168" s="T402">скрывать-PRS-1PL</ta>
            <ta e="T404" id="Seg_5169" s="T403">EMPH</ta>
            <ta e="T409" id="Seg_5170" s="T408">Анна.[NOM]</ta>
            <ta e="T410" id="Seg_5171" s="T409">быть-PST1-3SG</ta>
            <ta e="T412" id="Seg_5172" s="T411">этот</ta>
            <ta e="T413" id="Seg_5173" s="T412">Нина.[NOM]</ta>
            <ta e="T414" id="Seg_5174" s="T413">мать-3SG.[NOM]</ta>
            <ta e="T415" id="Seg_5175" s="T414">этот</ta>
            <ta e="T416" id="Seg_5176" s="T415">Горуо</ta>
            <ta e="T417" id="Seg_5177" s="T416">Нина.[NOM]</ta>
            <ta e="T418" id="Seg_5178" s="T417">мать-3SG.[NOM]</ta>
            <ta e="T419" id="Seg_5179" s="T418">быть-PST1-3SG</ta>
            <ta e="T420" id="Seg_5180" s="T419">Анна.[NOM]</ta>
            <ta e="T431" id="Seg_5181" s="T429">долганский</ta>
            <ta e="T432" id="Seg_5182" s="T431">человек-3PL.[NOM]</ta>
            <ta e="T433" id="Seg_5183" s="T432">быть-PST1-3PL</ta>
            <ta e="T434" id="Seg_5184" s="T433">наши.[NOM]</ta>
            <ta e="T435" id="Seg_5185" s="T434">долганин-PL.[NOM]</ta>
            <ta e="T436" id="Seg_5186" s="T435">быть-PST1-3PL</ta>
            <ta e="T443" id="Seg_5187" s="T442">некоторый</ta>
            <ta e="T444" id="Seg_5188" s="T443">русский-DRV-PTCP.HAB</ta>
            <ta e="T445" id="Seg_5189" s="T444">быть-PST1-3SG</ta>
            <ta e="T446" id="Seg_5190" s="T445">некоторый</ta>
            <ta e="T460" id="Seg_5191" s="T459">спиртное-ACC</ta>
            <ta e="T461" id="Seg_5192" s="T460">NEG</ta>
            <ta e="T462" id="Seg_5193" s="T461">пить-PST2.NEG-EP-1SG</ta>
            <ta e="T463" id="Seg_5194" s="T280">время-DAT/LOC</ta>
            <ta e="T464" id="Seg_5195" s="T463">NEG</ta>
            <ta e="T466" id="Seg_5196" s="T464">пить-EP-RECP/COLL-PST2.NEG-EP-1SG</ta>
            <ta e="T467" id="Seg_5197" s="T466">постареть-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T468" id="Seg_5198" s="T467">пока</ta>
            <ta e="T469" id="Seg_5199" s="T468">столько-PROPR-ACC</ta>
            <ta e="T470" id="Seg_5200" s="T469">хоронить-PST2-EP-1SG</ta>
            <ta e="T471" id="Seg_5201" s="T470">один</ta>
            <ta e="T472" id="Seg_5202" s="T471">рюмка-ACC</ta>
            <ta e="T473" id="Seg_5203" s="T472">пить-PST2.NEG-EP-1SG</ta>
            <ta e="T474" id="Seg_5204" s="T473">когда</ta>
            <ta e="T475" id="Seg_5205" s="T474">NEG</ta>
            <ta e="T476" id="Seg_5206" s="T475">всегда</ta>
            <ta e="T477" id="Seg_5207" s="T476">пить</ta>
            <ta e="T478" id="Seg_5208" s="T477">идти-EP-MED-NEG.CVB</ta>
            <ta e="T479" id="Seg_5209" s="T478">идти-EP-PST2-EP-1SG</ta>
            <ta e="T480" id="Seg_5210" s="T479">всегда</ta>
         </annotation>
         <annotation name="mc" tierref="mc-LaVN">
            <ta e="T5" id="Seg_5211" s="T4">interj</ta>
            <ta e="T6" id="Seg_5212" s="T5">dempro</ta>
            <ta e="T7" id="Seg_5213" s="T6">n-n:(num)-n:case</ta>
            <ta e="T8" id="Seg_5214" s="T7">dempro</ta>
            <ta e="T9" id="Seg_5215" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_5216" s="T9">que-pro:case</ta>
            <ta e="T11" id="Seg_5217" s="T10">adv</ta>
            <ta e="T12" id="Seg_5218" s="T11">ptcl</ta>
            <ta e="T14" id="Seg_5219" s="T12">pers-pro:case</ta>
            <ta e="T15" id="Seg_5220" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_5221" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_5222" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_5223" s="T17">pers-pro:case</ta>
            <ta e="T19" id="Seg_5224" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_5225" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_5226" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_5227" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_5228" s="T22">v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_5229" s="T23">propr-n:case</ta>
            <ta e="T25" id="Seg_5230" s="T24">v-v:tense-v:pred.pn</ta>
            <ta e="T26" id="Seg_5231" s="T25">propr-n:case</ta>
            <ta e="T28" id="Seg_5232" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_5233" s="T28">interj-n:(num)-n:case</ta>
            <ta e="T30" id="Seg_5234" s="T29">propr-n:case</ta>
            <ta e="T31" id="Seg_5235" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_5236" s="T31">adv</ta>
            <ta e="T33" id="Seg_5237" s="T32">adv</ta>
            <ta e="T34" id="Seg_5238" s="T33">n-n:(num)-n:case</ta>
            <ta e="T35" id="Seg_5239" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_5240" s="T35">interj</ta>
            <ta e="T37" id="Seg_5241" s="T36">interj</ta>
            <ta e="T38" id="Seg_5242" s="T37">v-v:tense-v:pred.pn</ta>
            <ta e="T39" id="Seg_5243" s="T38">pers-pro:case</ta>
            <ta e="T40" id="Seg_5244" s="T39">interj</ta>
            <ta e="T41" id="Seg_5245" s="T40">propr</ta>
            <ta e="T42" id="Seg_5246" s="T41">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T43" id="Seg_5247" s="T42">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T44" id="Seg_5248" s="T43">pers-pro:case</ta>
            <ta e="T45" id="Seg_5249" s="T44">v-v:tense-v:pred.pn</ta>
            <ta e="T46" id="Seg_5250" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_5251" s="T46">propr-n:case</ta>
            <ta e="T48" id="Seg_5252" s="T47">adj-n:(poss)-n:case</ta>
            <ta e="T49" id="Seg_5253" s="T48">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T50" id="Seg_5254" s="T49">pers-pro:case</ta>
            <ta e="T51" id="Seg_5255" s="T50">v-v:tense-v:poss.pn</ta>
            <ta e="T53" id="Seg_5256" s="T51">que-que&gt;v-v:mood.pn</ta>
            <ta e="T56" id="Seg_5257" s="T53">v-v&gt;v-v:mood.pn</ta>
            <ta e="T57" id="Seg_5258" s="T56">pers-pro:case</ta>
            <ta e="T58" id="Seg_5259" s="T57">n-n:poss-n:case</ta>
            <ta e="T59" id="Seg_5260" s="T58">dempro</ta>
            <ta e="T60" id="Seg_5261" s="T59">adj</ta>
            <ta e="T61" id="Seg_5262" s="T60">n-n:poss-n:case</ta>
            <ta e="T62" id="Seg_5263" s="T61">v-v:tense-v:poss.pn</ta>
            <ta e="T65" id="Seg_5264" s="T64">adj-n:(poss)-n:case</ta>
            <ta e="T66" id="Seg_5265" s="T65">v-v&gt;v-v:mood.pn</ta>
            <ta e="T67" id="Seg_5266" s="T66">v-v:tense-v:poss.pn</ta>
            <ta e="T68" id="Seg_5267" s="T67">pers-pro:case</ta>
            <ta e="T69" id="Seg_5268" s="T68">v-v:tense-v:pred.pn</ta>
            <ta e="T70" id="Seg_5269" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_5270" s="T70">pers-pro:case</ta>
            <ta e="T72" id="Seg_5271" s="T71">cardnum</ta>
            <ta e="T73" id="Seg_5272" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_5273" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_5274" s="T74">que-pro:case</ta>
            <ta e="T76" id="Seg_5275" s="T75">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_5276" s="T76">dempro</ta>
            <ta e="T78" id="Seg_5277" s="T77">n-n:poss-n:case</ta>
            <ta e="T79" id="Seg_5278" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_5279" s="T79">pers-pro:case</ta>
            <ta e="T84" id="Seg_5280" s="T82">ptcl</ta>
            <ta e="T85" id="Seg_5281" s="T84">dempro</ta>
            <ta e="T86" id="Seg_5282" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_5283" s="T86">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T88" id="Seg_5284" s="T87">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T89" id="Seg_5285" s="T88">ptcl</ta>
            <ta e="T94" id="Seg_5286" s="T92">pers-pro:case</ta>
            <ta e="T95" id="Seg_5287" s="T94">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T96" id="Seg_5288" s="T95">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T97" id="Seg_5289" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_5290" s="T97">propr-n:case</ta>
            <ta e="T99" id="Seg_5291" s="T98">v-v:cvb-v:pred.pn</ta>
            <ta e="T100" id="Seg_5292" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_5293" s="T100">adv</ta>
            <ta e="T102" id="Seg_5294" s="T101">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T103" id="Seg_5295" s="T102">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T104" id="Seg_5296" s="T103">dempro</ta>
            <ta e="T105" id="Seg_5297" s="T104">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T106" id="Seg_5298" s="T105">v-v:cvb</ta>
            <ta e="T107" id="Seg_5299" s="T106">post</ta>
            <ta e="T108" id="Seg_5300" s="T107">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T109" id="Seg_5301" s="T108">n-n&gt;adj-n:case</ta>
            <ta e="T110" id="Seg_5302" s="T109">adv</ta>
            <ta e="T111" id="Seg_5303" s="T110">n-n&gt;n-n:case</ta>
            <ta e="T112" id="Seg_5304" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_5305" s="T112">dempro-pro:case</ta>
            <ta e="T114" id="Seg_5306" s="T113">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T115" id="Seg_5307" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_5308" s="T115">adv</ta>
            <ta e="T117" id="Seg_5309" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_5310" s="T117">v-v:tense-v:pred.pn</ta>
            <ta e="T119" id="Seg_5311" s="T118">dempro</ta>
            <ta e="T122" id="Seg_5312" s="T121">ptcl</ta>
            <ta e="T127" id="Seg_5313" s="T126">pers</ta>
            <ta e="T130" id="Seg_5314" s="T129">adj</ta>
            <ta e="T131" id="Seg_5315" s="T130">post</ta>
            <ta e="T132" id="Seg_5316" s="T131">n-n&gt;adj</ta>
            <ta e="T136" id="Seg_5317" s="T135">interj</ta>
            <ta e="T137" id="Seg_5318" s="T136">propr-n:case</ta>
            <ta e="T138" id="Seg_5319" s="T137">v-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_5320" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_5321" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_5322" s="T143">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T145" id="Seg_5323" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_5324" s="T145">v-v:mood.pn</ta>
            <ta e="T147" id="Seg_5325" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_5326" s="T147">n-n:poss-n:case</ta>
            <ta e="T149" id="Seg_5327" s="T148">que-pro:case</ta>
            <ta e="T150" id="Seg_5328" s="T149">n-n&gt;n-n:case</ta>
            <ta e="T151" id="Seg_5329" s="T150">n-n:(poss)-n:case</ta>
            <ta e="T152" id="Seg_5330" s="T151">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T153" id="Seg_5331" s="T152">adj</ta>
            <ta e="T154" id="Seg_5332" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_5333" s="T154">dempro-pro:case</ta>
            <ta e="T156" id="Seg_5334" s="T155">n-n:poss-n:case</ta>
            <ta e="T157" id="Seg_5335" s="T156">v-v:cvb</ta>
            <ta e="T158" id="Seg_5336" s="T157">v-v:cvb</ta>
            <ta e="T159" id="Seg_5337" s="T158">post</ta>
            <ta e="T160" id="Seg_5338" s="T159">adv</ta>
            <ta e="T161" id="Seg_5339" s="T162">adv</ta>
            <ta e="T163" id="Seg_5340" s="T161">v-v:tense-v:pred.pn</ta>
            <ta e="T164" id="Seg_5341" s="T163">cardnum</ta>
            <ta e="T165" id="Seg_5342" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_5343" s="T165">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T167" id="Seg_5344" s="T166">adv&gt;adv-adv-n:case</ta>
            <ta e="T168" id="Seg_5345" s="T167">post</ta>
            <ta e="T169" id="Seg_5346" s="T168">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T173" id="Seg_5347" s="T171">propr-n:case</ta>
            <ta e="T174" id="Seg_5348" s="T173">adj-n:poss-n:case</ta>
            <ta e="T175" id="Seg_5349" s="T174">interj</ta>
            <ta e="T176" id="Seg_5350" s="T175">adv</ta>
            <ta e="T187" id="Seg_5351" s="T185">dempro</ta>
            <ta e="T188" id="Seg_5352" s="T187">n-n:(num)-n:poss-n:case</ta>
            <ta e="T189" id="Seg_5353" s="T188">dempro</ta>
            <ta e="T190" id="Seg_5354" s="T189">n-n:(num)-n:case</ta>
            <ta e="T191" id="Seg_5355" s="T190">v-v:cvb</ta>
            <ta e="T192" id="Seg_5356" s="T191">v-v:cvb-v:pred.pn</ta>
            <ta e="T193" id="Seg_5357" s="T192">adv-adv&gt;adv</ta>
            <ta e="T194" id="Seg_5358" s="T193">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T195" id="Seg_5359" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_5360" s="T195">dempro</ta>
            <ta e="T197" id="Seg_5361" s="T196">adv</ta>
            <ta e="T198" id="Seg_5362" s="T197">propr-n:case</ta>
            <ta e="T199" id="Seg_5363" s="T198">dempro</ta>
            <ta e="T200" id="Seg_5364" s="T199">n</ta>
            <ta e="T201" id="Seg_5365" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_5366" s="T201">adj-n:poss-n:case</ta>
            <ta e="T216" id="Seg_5367" s="T214">adv</ta>
            <ta e="T217" id="Seg_5368" s="T216">propr-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T218" id="Seg_5369" s="T217">v-v:tense-v:poss.pn</ta>
            <ta e="T219" id="Seg_5370" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_5371" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_5372" s="T220">adv</ta>
            <ta e="T222" id="Seg_5373" s="T221">dempro</ta>
            <ta e="T223" id="Seg_5374" s="T222">n</ta>
            <ta e="T224" id="Seg_5375" s="T223">dempro</ta>
            <ta e="T225" id="Seg_5376" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_5377" s="T225">pers-pro:case</ta>
            <ta e="T227" id="Seg_5378" s="T226">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T228" id="Seg_5379" s="T227">pers-pro:case</ta>
            <ta e="T229" id="Seg_5380" s="T228">dempro</ta>
            <ta e="T245" id="Seg_5381" s="T244">adv</ta>
            <ta e="T246" id="Seg_5382" s="T245">pers-pro:case</ta>
            <ta e="T247" id="Seg_5383" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_5384" s="T247">dempro</ta>
            <ta e="T249" id="Seg_5385" s="T248">v-v:tense-v:pred.pn</ta>
            <ta e="T250" id="Seg_5386" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_5387" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_5388" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_5389" s="T252">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T254" id="Seg_5390" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_5391" s="T254">n-n:(num)-n:case</ta>
            <ta e="T256" id="Seg_5392" s="T255">ptcl</ta>
            <ta e="T265" id="Seg_5393" s="T264">dempro-pro:case</ta>
            <ta e="T266" id="Seg_5394" s="T265">post</ta>
            <ta e="T267" id="Seg_5395" s="T266">pers-pro:case</ta>
            <ta e="T268" id="Seg_5396" s="T267">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T269" id="Seg_5397" s="T268">interj</ta>
            <ta e="T271" id="Seg_5398" s="T269">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T272" id="Seg_5399" s="T271">n-n:poss-n:case</ta>
            <ta e="T273" id="Seg_5400" s="T272">post</ta>
            <ta e="T274" id="Seg_5401" s="T273">cardnum-cardnum&gt;collnum-n:poss-n:case</ta>
            <ta e="T275" id="Seg_5402" s="T274">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T276" id="Seg_5403" s="T275">ptcl</ta>
            <ta e="T294" id="Seg_5404" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_5405" s="T294">dempro</ta>
            <ta e="T296" id="Seg_5406" s="T295">n-n:(num)-n:case</ta>
            <ta e="T297" id="Seg_5407" s="T296">v-v:(neg)-v:ptcp-v:(case)</ta>
            <ta e="T298" id="Seg_5408" s="T297">pers-pro:case</ta>
            <ta e="T299" id="Seg_5409" s="T298">adv</ta>
            <ta e="T300" id="Seg_5410" s="T299">pers-pro:case</ta>
            <ta e="T301" id="Seg_5411" s="T300">que-pro:case</ta>
            <ta e="T302" id="Seg_5412" s="T301">ptcl</ta>
            <ta e="T312" id="Seg_5413" s="T311">adv</ta>
            <ta e="T313" id="Seg_5414" s="T312">dempro</ta>
            <ta e="T314" id="Seg_5415" s="T313">dempro</ta>
            <ta e="T315" id="Seg_5416" s="T314">dempro</ta>
            <ta e="T316" id="Seg_5417" s="T315">posspr-pro:case</ta>
            <ta e="T317" id="Seg_5418" s="T316">dempro</ta>
            <ta e="T318" id="Seg_5419" s="T317">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T319" id="Seg_5420" s="T318">dempro</ta>
            <ta e="T330" id="Seg_5421" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_5422" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_5423" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_5424" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_5425" s="T333">dempro</ta>
            <ta e="T335" id="Seg_5426" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_5427" s="T335">v-v&gt;v-v:cvb</ta>
            <ta e="T337" id="Seg_5428" s="T336">v-v:tense-v:pred.pn</ta>
            <ta e="T338" id="Seg_5429" s="T337">dempro</ta>
            <ta e="T339" id="Seg_5430" s="T338">v-v:tense-v:pred.pn</ta>
            <ta e="T340" id="Seg_5431" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_5432" s="T340">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T342" id="Seg_5433" s="T341">interj</ta>
            <ta e="T344" id="Seg_5434" s="T343">adv</ta>
            <ta e="T348" id="Seg_5435" s="T346">ptcl</ta>
            <ta e="T349" id="Seg_5436" s="T348">adv</ta>
            <ta e="T350" id="Seg_5437" s="T349">v-v:ptcp-v:(case)</ta>
            <ta e="T357" id="Seg_5438" s="T355">interj</ta>
            <ta e="T358" id="Seg_5439" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_5440" s="T358">v-v:ptcp</ta>
            <ta e="T360" id="Seg_5441" s="T359">v-v:tense-v:pred.pn</ta>
            <ta e="T361" id="Seg_5442" s="T360">adv</ta>
            <ta e="T362" id="Seg_5443" s="T361">v-v:tense-v:pred.pn</ta>
            <ta e="T363" id="Seg_5444" s="T362">v-v&gt;v-v:ptcp-v:pred.pn</ta>
            <ta e="T364" id="Seg_5445" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_5446" s="T364">v-v:tense-v:pred.pn</ta>
            <ta e="T372" id="Seg_5447" s="T370">ptcl</ta>
            <ta e="T373" id="Seg_5448" s="T372">posspr</ta>
            <ta e="T374" id="Seg_5449" s="T373">n-n:(poss)-n:case</ta>
            <ta e="T379" id="Seg_5450" s="T378">interj</ta>
            <ta e="T381" id="Seg_5451" s="T379">v-v:(neg)-v:pred.pn</ta>
            <ta e="T382" id="Seg_5452" s="T381">v-v:tense-v:poss.pn</ta>
            <ta e="T383" id="Seg_5453" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_5454" s="T383">que-pro:(poss)-pro:case</ta>
            <ta e="T385" id="Seg_5455" s="T384">v-v:mood.pn</ta>
            <ta e="T386" id="Seg_5456" s="T385">v-v:tense-v:pred.pn</ta>
            <ta e="T387" id="Seg_5457" s="T386">pers-pro:case</ta>
            <ta e="T388" id="Seg_5458" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_5459" s="T388">que</ta>
            <ta e="T390" id="Seg_5460" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_5461" s="T390">interj</ta>
            <ta e="T392" id="Seg_5462" s="T391">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T393" id="Seg_5463" s="T392">propr-n:case</ta>
            <ta e="T394" id="Seg_5464" s="T393">n-n:(num)-n:poss-n:case</ta>
            <ta e="T395" id="Seg_5465" s="T394">pers-pro:case</ta>
            <ta e="T396" id="Seg_5466" s="T395">cardnum-n&gt;n-n:poss-n:case</ta>
            <ta e="T397" id="Seg_5467" s="T396">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T398" id="Seg_5468" s="T397">v-v:tense-v:pred.pn</ta>
            <ta e="T399" id="Seg_5469" s="T398">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T400" id="Seg_5470" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_5471" s="T400">dempro</ta>
            <ta e="T402" id="Seg_5472" s="T401">cardnum-cardnum&gt;collnum</ta>
            <ta e="T403" id="Seg_5473" s="T402">v-v:tense-v:pred.pn</ta>
            <ta e="T404" id="Seg_5474" s="T403">ptcl</ta>
            <ta e="T409" id="Seg_5475" s="T408">propr-n:case</ta>
            <ta e="T410" id="Seg_5476" s="T409">v-v:tense-v:poss.pn</ta>
            <ta e="T412" id="Seg_5477" s="T411">dempro</ta>
            <ta e="T413" id="Seg_5478" s="T412">propr-n:case</ta>
            <ta e="T414" id="Seg_5479" s="T413">n-n:(poss)-n:case</ta>
            <ta e="T415" id="Seg_5480" s="T414">dempro</ta>
            <ta e="T416" id="Seg_5481" s="T415">propr</ta>
            <ta e="T417" id="Seg_5482" s="T416">propr-n:case</ta>
            <ta e="T418" id="Seg_5483" s="T417">n-n:(poss)-n:case</ta>
            <ta e="T419" id="Seg_5484" s="T418">v-v:tense-v:poss.pn</ta>
            <ta e="T420" id="Seg_5485" s="T419">propr-n:case</ta>
            <ta e="T431" id="Seg_5486" s="T429">adj</ta>
            <ta e="T432" id="Seg_5487" s="T431">n-n:(poss)-n:case</ta>
            <ta e="T433" id="Seg_5488" s="T432">v-v:tense-v:poss.pn</ta>
            <ta e="T434" id="Seg_5489" s="T433">posspr-pro:case</ta>
            <ta e="T435" id="Seg_5490" s="T434">n-n:(num)-n:case</ta>
            <ta e="T436" id="Seg_5491" s="T435">v-v:tense-v:pred.pn</ta>
            <ta e="T443" id="Seg_5492" s="T442">indfpro</ta>
            <ta e="T444" id="Seg_5493" s="T443">n-n&gt;v-v:ptcp</ta>
            <ta e="T445" id="Seg_5494" s="T444">v-v:tense-v:poss.pn</ta>
            <ta e="T446" id="Seg_5495" s="T445">indfpro</ta>
            <ta e="T460" id="Seg_5496" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_5497" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_5498" s="T461">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T463" id="Seg_5499" s="T280">n-n:case</ta>
            <ta e="T464" id="Seg_5500" s="T463">ptcl</ta>
            <ta e="T466" id="Seg_5501" s="T464">v-v:(ins)-v&gt;v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T467" id="Seg_5502" s="T466">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T468" id="Seg_5503" s="T467">post</ta>
            <ta e="T469" id="Seg_5504" s="T468">adv-adv&gt;adj-n:case</ta>
            <ta e="T470" id="Seg_5505" s="T469">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T471" id="Seg_5506" s="T470">cardnum</ta>
            <ta e="T472" id="Seg_5507" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_5508" s="T472">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T474" id="Seg_5509" s="T473">que</ta>
            <ta e="T475" id="Seg_5510" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_5511" s="T475">adv</ta>
            <ta e="T477" id="Seg_5512" s="T476">v</ta>
            <ta e="T478" id="Seg_5513" s="T477">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T479" id="Seg_5514" s="T478">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T480" id="Seg_5515" s="T479">adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps-LaVN">
            <ta e="T5" id="Seg_5516" s="T4">interj</ta>
            <ta e="T6" id="Seg_5517" s="T5">dempro</ta>
            <ta e="T7" id="Seg_5518" s="T6">n</ta>
            <ta e="T8" id="Seg_5519" s="T7">dempro</ta>
            <ta e="T9" id="Seg_5520" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_5521" s="T9">que</ta>
            <ta e="T11" id="Seg_5522" s="T10">adv</ta>
            <ta e="T12" id="Seg_5523" s="T11">ptcl</ta>
            <ta e="T14" id="Seg_5524" s="T12">pers</ta>
            <ta e="T15" id="Seg_5525" s="T14">n</ta>
            <ta e="T16" id="Seg_5526" s="T15">v</ta>
            <ta e="T17" id="Seg_5527" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_5528" s="T17">pers</ta>
            <ta e="T19" id="Seg_5529" s="T18">n</ta>
            <ta e="T20" id="Seg_5530" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_5531" s="T20">n</ta>
            <ta e="T22" id="Seg_5532" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_5533" s="T22">v</ta>
            <ta e="T24" id="Seg_5534" s="T23">propr</ta>
            <ta e="T25" id="Seg_5535" s="T24">v</ta>
            <ta e="T26" id="Seg_5536" s="T25">propr</ta>
            <ta e="T28" id="Seg_5537" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_5538" s="T28">n</ta>
            <ta e="T30" id="Seg_5539" s="T29">propr</ta>
            <ta e="T31" id="Seg_5540" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_5541" s="T31">adv</ta>
            <ta e="T33" id="Seg_5542" s="T32">adv</ta>
            <ta e="T34" id="Seg_5543" s="T33">n</ta>
            <ta e="T35" id="Seg_5544" s="T34">v</ta>
            <ta e="T36" id="Seg_5545" s="T35">interj</ta>
            <ta e="T37" id="Seg_5546" s="T36">interj</ta>
            <ta e="T38" id="Seg_5547" s="T37">v</ta>
            <ta e="T39" id="Seg_5548" s="T38">pers</ta>
            <ta e="T40" id="Seg_5549" s="T39">interj</ta>
            <ta e="T41" id="Seg_5550" s="T40">propr</ta>
            <ta e="T42" id="Seg_5551" s="T41">n</ta>
            <ta e="T43" id="Seg_5552" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_5553" s="T43">pers</ta>
            <ta e="T45" id="Seg_5554" s="T44">v</ta>
            <ta e="T46" id="Seg_5555" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_5556" s="T46">propr</ta>
            <ta e="T48" id="Seg_5557" s="T47">adj</ta>
            <ta e="T49" id="Seg_5558" s="T48">v</ta>
            <ta e="T50" id="Seg_5559" s="T49">pers</ta>
            <ta e="T51" id="Seg_5560" s="T50">v</ta>
            <ta e="T53" id="Seg_5561" s="T51">v</ta>
            <ta e="T56" id="Seg_5562" s="T53">v</ta>
            <ta e="T57" id="Seg_5563" s="T56">pers</ta>
            <ta e="T58" id="Seg_5564" s="T57">n</ta>
            <ta e="T59" id="Seg_5565" s="T58">dempro</ta>
            <ta e="T60" id="Seg_5566" s="T59">adj</ta>
            <ta e="T61" id="Seg_5567" s="T60">n</ta>
            <ta e="T62" id="Seg_5568" s="T61">v</ta>
            <ta e="T65" id="Seg_5569" s="T64">adj</ta>
            <ta e="T66" id="Seg_5570" s="T65">v</ta>
            <ta e="T67" id="Seg_5571" s="T66">v</ta>
            <ta e="T68" id="Seg_5572" s="T67">pers</ta>
            <ta e="T69" id="Seg_5573" s="T68">v</ta>
            <ta e="T70" id="Seg_5574" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_5575" s="T70">pers</ta>
            <ta e="T72" id="Seg_5576" s="T71">cardnum</ta>
            <ta e="T73" id="Seg_5577" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_5578" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_5579" s="T74">que</ta>
            <ta e="T76" id="Seg_5580" s="T75">v</ta>
            <ta e="T77" id="Seg_5581" s="T76">dempro</ta>
            <ta e="T78" id="Seg_5582" s="T77">n</ta>
            <ta e="T79" id="Seg_5583" s="T78">v</ta>
            <ta e="T80" id="Seg_5584" s="T79">pers</ta>
            <ta e="T84" id="Seg_5585" s="T82">ptcl</ta>
            <ta e="T85" id="Seg_5586" s="T84">dempro</ta>
            <ta e="T86" id="Seg_5587" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_5588" s="T86">v</ta>
            <ta e="T88" id="Seg_5589" s="T87">emphpro</ta>
            <ta e="T89" id="Seg_5590" s="T88">ptcl</ta>
            <ta e="T94" id="Seg_5591" s="T92">pers</ta>
            <ta e="T95" id="Seg_5592" s="T94">emphpro</ta>
            <ta e="T96" id="Seg_5593" s="T95">v</ta>
            <ta e="T97" id="Seg_5594" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_5595" s="T97">propr</ta>
            <ta e="T99" id="Seg_5596" s="T98">v</ta>
            <ta e="T100" id="Seg_5597" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_5598" s="T100">adv</ta>
            <ta e="T102" id="Seg_5599" s="T101">emphpro</ta>
            <ta e="T103" id="Seg_5600" s="T102">v</ta>
            <ta e="T104" id="Seg_5601" s="T103">dempro</ta>
            <ta e="T105" id="Seg_5602" s="T104">emphpro</ta>
            <ta e="T106" id="Seg_5603" s="T105">v</ta>
            <ta e="T107" id="Seg_5604" s="T106">post</ta>
            <ta e="T108" id="Seg_5605" s="T107">dempro</ta>
            <ta e="T109" id="Seg_5606" s="T108">adj</ta>
            <ta e="T110" id="Seg_5607" s="T109">adv</ta>
            <ta e="T111" id="Seg_5608" s="T110">n</ta>
            <ta e="T112" id="Seg_5609" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_5610" s="T112">dempro</ta>
            <ta e="T114" id="Seg_5611" s="T113">emphpro</ta>
            <ta e="T115" id="Seg_5612" s="T114">v</ta>
            <ta e="T116" id="Seg_5613" s="T115">adv</ta>
            <ta e="T117" id="Seg_5614" s="T116">n</ta>
            <ta e="T118" id="Seg_5615" s="T117">v</ta>
            <ta e="T119" id="Seg_5616" s="T118">dempro</ta>
            <ta e="T122" id="Seg_5617" s="T121">ptcl</ta>
            <ta e="T127" id="Seg_5618" s="T126">pers</ta>
            <ta e="T130" id="Seg_5619" s="T129">adj</ta>
            <ta e="T131" id="Seg_5620" s="T130">post</ta>
            <ta e="T132" id="Seg_5621" s="T131">adj</ta>
            <ta e="T136" id="Seg_5622" s="T135">interj</ta>
            <ta e="T137" id="Seg_5623" s="T136">propr</ta>
            <ta e="T138" id="Seg_5624" s="T137">v</ta>
            <ta e="T142" id="Seg_5625" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_5626" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_5627" s="T143">dempro</ta>
            <ta e="T145" id="Seg_5628" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_5629" s="T145">v</ta>
            <ta e="T147" id="Seg_5630" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_5631" s="T147">n</ta>
            <ta e="T149" id="Seg_5632" s="T148">que</ta>
            <ta e="T150" id="Seg_5633" s="T149">n</ta>
            <ta e="T151" id="Seg_5634" s="T150">n</ta>
            <ta e="T152" id="Seg_5635" s="T151">v</ta>
            <ta e="T153" id="Seg_5636" s="T152">adj</ta>
            <ta e="T154" id="Seg_5637" s="T153">n</ta>
            <ta e="T155" id="Seg_5638" s="T154">dempro</ta>
            <ta e="T156" id="Seg_5639" s="T155">n</ta>
            <ta e="T157" id="Seg_5640" s="T156">v</ta>
            <ta e="T158" id="Seg_5641" s="T157">aux</ta>
            <ta e="T159" id="Seg_5642" s="T158">post</ta>
            <ta e="T160" id="Seg_5643" s="T159">adv</ta>
            <ta e="T161" id="Seg_5644" s="T162">adv</ta>
            <ta e="T163" id="Seg_5645" s="T161">v</ta>
            <ta e="T164" id="Seg_5646" s="T163">cardnum</ta>
            <ta e="T165" id="Seg_5647" s="T164">n</ta>
            <ta e="T166" id="Seg_5648" s="T165">v</ta>
            <ta e="T167" id="Seg_5649" s="T166">adv</ta>
            <ta e="T168" id="Seg_5650" s="T167">post</ta>
            <ta e="T169" id="Seg_5651" s="T168">v</ta>
            <ta e="T173" id="Seg_5652" s="T171">propr</ta>
            <ta e="T174" id="Seg_5653" s="T173">n</ta>
            <ta e="T175" id="Seg_5654" s="T174">interj</ta>
            <ta e="T176" id="Seg_5655" s="T175">adv</ta>
            <ta e="T187" id="Seg_5656" s="T185">dempro</ta>
            <ta e="T188" id="Seg_5657" s="T187">n</ta>
            <ta e="T189" id="Seg_5658" s="T188">dempro</ta>
            <ta e="T190" id="Seg_5659" s="T189">n</ta>
            <ta e="T191" id="Seg_5660" s="T190">v</ta>
            <ta e="T192" id="Seg_5661" s="T191">aux</ta>
            <ta e="T193" id="Seg_5662" s="T192">adv</ta>
            <ta e="T194" id="Seg_5663" s="T193">v</ta>
            <ta e="T195" id="Seg_5664" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_5665" s="T195">dempro</ta>
            <ta e="T197" id="Seg_5666" s="T196">adv</ta>
            <ta e="T198" id="Seg_5667" s="T197">propr</ta>
            <ta e="T199" id="Seg_5668" s="T198">dempro</ta>
            <ta e="T200" id="Seg_5669" s="T199">n</ta>
            <ta e="T201" id="Seg_5670" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_5671" s="T201">adj</ta>
            <ta e="T216" id="Seg_5672" s="T214">adv</ta>
            <ta e="T217" id="Seg_5673" s="T216">propr</ta>
            <ta e="T218" id="Seg_5674" s="T217">cop</ta>
            <ta e="T219" id="Seg_5675" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_5676" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_5677" s="T220">adv</ta>
            <ta e="T222" id="Seg_5678" s="T221">dempro</ta>
            <ta e="T223" id="Seg_5679" s="T222">n</ta>
            <ta e="T224" id="Seg_5680" s="T223">dempro</ta>
            <ta e="T225" id="Seg_5681" s="T224">n</ta>
            <ta e="T226" id="Seg_5682" s="T225">pers</ta>
            <ta e="T227" id="Seg_5683" s="T226">v</ta>
            <ta e="T228" id="Seg_5684" s="T227">pers</ta>
            <ta e="T229" id="Seg_5685" s="T228">dempro</ta>
            <ta e="T245" id="Seg_5686" s="T244">adv</ta>
            <ta e="T246" id="Seg_5687" s="T245">pers</ta>
            <ta e="T247" id="Seg_5688" s="T246">n</ta>
            <ta e="T248" id="Seg_5689" s="T247">dempro</ta>
            <ta e="T249" id="Seg_5690" s="T248">v</ta>
            <ta e="T250" id="Seg_5691" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_5692" s="T250">n</ta>
            <ta e="T252" id="Seg_5693" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_5694" s="T252">v</ta>
            <ta e="T254" id="Seg_5695" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_5696" s="T254">n</ta>
            <ta e="T256" id="Seg_5697" s="T255">ptcl</ta>
            <ta e="T265" id="Seg_5698" s="T264">dempro</ta>
            <ta e="T266" id="Seg_5699" s="T265">post</ta>
            <ta e="T267" id="Seg_5700" s="T266">pers</ta>
            <ta e="T268" id="Seg_5701" s="T267">v</ta>
            <ta e="T269" id="Seg_5702" s="T268">interj</ta>
            <ta e="T271" id="Seg_5703" s="T269">n</ta>
            <ta e="T272" id="Seg_5704" s="T271">n</ta>
            <ta e="T273" id="Seg_5705" s="T272">post</ta>
            <ta e="T274" id="Seg_5706" s="T273">collnum</ta>
            <ta e="T275" id="Seg_5707" s="T274">v</ta>
            <ta e="T276" id="Seg_5708" s="T275">ptcl</ta>
            <ta e="T294" id="Seg_5709" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_5710" s="T294">dempro</ta>
            <ta e="T296" id="Seg_5711" s="T295">n</ta>
            <ta e="T297" id="Seg_5712" s="T296">v</ta>
            <ta e="T298" id="Seg_5713" s="T297">pers</ta>
            <ta e="T299" id="Seg_5714" s="T298">adv</ta>
            <ta e="T300" id="Seg_5715" s="T299">pers</ta>
            <ta e="T301" id="Seg_5716" s="T300">que</ta>
            <ta e="T302" id="Seg_5717" s="T301">ptcl</ta>
            <ta e="T312" id="Seg_5718" s="T311">adv</ta>
            <ta e="T313" id="Seg_5719" s="T312">dempro</ta>
            <ta e="T314" id="Seg_5720" s="T313">dempro</ta>
            <ta e="T315" id="Seg_5721" s="T314">dempro</ta>
            <ta e="T316" id="Seg_5722" s="T315">posspr</ta>
            <ta e="T317" id="Seg_5723" s="T316">dempro</ta>
            <ta e="T318" id="Seg_5724" s="T317">v</ta>
            <ta e="T319" id="Seg_5725" s="T318">dempro</ta>
            <ta e="T330" id="Seg_5726" s="T329">ptcl</ta>
            <ta e="T331" id="Seg_5727" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_5728" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_5729" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_5730" s="T333">dempro</ta>
            <ta e="T335" id="Seg_5731" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_5732" s="T335">v</ta>
            <ta e="T337" id="Seg_5733" s="T336">aux</ta>
            <ta e="T338" id="Seg_5734" s="T337">dempro</ta>
            <ta e="T339" id="Seg_5735" s="T338">v</ta>
            <ta e="T340" id="Seg_5736" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_5737" s="T340">dempro</ta>
            <ta e="T342" id="Seg_5738" s="T341">interj</ta>
            <ta e="T344" id="Seg_5739" s="T343">adv</ta>
            <ta e="T348" id="Seg_5740" s="T346">ptcl</ta>
            <ta e="T349" id="Seg_5741" s="T348">adv</ta>
            <ta e="T350" id="Seg_5742" s="T349">v</ta>
            <ta e="T357" id="Seg_5743" s="T355">interj</ta>
            <ta e="T358" id="Seg_5744" s="T357">n</ta>
            <ta e="T359" id="Seg_5745" s="T358">v</ta>
            <ta e="T360" id="Seg_5746" s="T359">cop</ta>
            <ta e="T361" id="Seg_5747" s="T360">adv</ta>
            <ta e="T362" id="Seg_5748" s="T361">v</ta>
            <ta e="T363" id="Seg_5749" s="T362">v</ta>
            <ta e="T364" id="Seg_5750" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_5751" s="T364">v</ta>
            <ta e="T372" id="Seg_5752" s="T370">ptcl</ta>
            <ta e="T373" id="Seg_5753" s="T372">posspr</ta>
            <ta e="T374" id="Seg_5754" s="T373">n</ta>
            <ta e="T379" id="Seg_5755" s="T378">interj</ta>
            <ta e="T381" id="Seg_5756" s="T379">v</ta>
            <ta e="T382" id="Seg_5757" s="T381">v</ta>
            <ta e="T383" id="Seg_5758" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_5759" s="T383">que</ta>
            <ta e="T385" id="Seg_5760" s="T384">v</ta>
            <ta e="T386" id="Seg_5761" s="T385">v</ta>
            <ta e="T387" id="Seg_5762" s="T386">pers</ta>
            <ta e="T388" id="Seg_5763" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_5764" s="T388">que</ta>
            <ta e="T390" id="Seg_5765" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_5766" s="T390">interj</ta>
            <ta e="T392" id="Seg_5767" s="T391">v</ta>
            <ta e="T393" id="Seg_5768" s="T392">propr</ta>
            <ta e="T394" id="Seg_5769" s="T393">n</ta>
            <ta e="T395" id="Seg_5770" s="T394">pers</ta>
            <ta e="T396" id="Seg_5771" s="T395">cardnum</ta>
            <ta e="T397" id="Seg_5772" s="T396">v</ta>
            <ta e="T398" id="Seg_5773" s="T397">v</ta>
            <ta e="T399" id="Seg_5774" s="T398">n</ta>
            <ta e="T400" id="Seg_5775" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_5776" s="T400">dempro</ta>
            <ta e="T402" id="Seg_5777" s="T401">collnum</ta>
            <ta e="T403" id="Seg_5778" s="T402">v</ta>
            <ta e="T404" id="Seg_5779" s="T403">ptcl</ta>
            <ta e="T409" id="Seg_5780" s="T408">propr</ta>
            <ta e="T410" id="Seg_5781" s="T409">cop</ta>
            <ta e="T412" id="Seg_5782" s="T411">dempro</ta>
            <ta e="T413" id="Seg_5783" s="T412">propr</ta>
            <ta e="T414" id="Seg_5784" s="T413">n</ta>
            <ta e="T415" id="Seg_5785" s="T414">dempro</ta>
            <ta e="T416" id="Seg_5786" s="T415">propr</ta>
            <ta e="T417" id="Seg_5787" s="T416">propr</ta>
            <ta e="T418" id="Seg_5788" s="T417">n</ta>
            <ta e="T419" id="Seg_5789" s="T418">cop</ta>
            <ta e="T420" id="Seg_5790" s="T419">propr</ta>
            <ta e="T431" id="Seg_5791" s="T429">adj</ta>
            <ta e="T432" id="Seg_5792" s="T431">n</ta>
            <ta e="T433" id="Seg_5793" s="T432">cop</ta>
            <ta e="T434" id="Seg_5794" s="T433">posspr</ta>
            <ta e="T435" id="Seg_5795" s="T434">n</ta>
            <ta e="T436" id="Seg_5796" s="T435">cop</ta>
            <ta e="T443" id="Seg_5797" s="T442">indfpro</ta>
            <ta e="T444" id="Seg_5798" s="T443">v</ta>
            <ta e="T445" id="Seg_5799" s="T444">aux</ta>
            <ta e="T446" id="Seg_5800" s="T445">indfpro</ta>
            <ta e="T460" id="Seg_5801" s="T459">n</ta>
            <ta e="T461" id="Seg_5802" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_5803" s="T461">v</ta>
            <ta e="T463" id="Seg_5804" s="T280">n</ta>
            <ta e="T464" id="Seg_5805" s="T463">ptcl</ta>
            <ta e="T466" id="Seg_5806" s="T464">v</ta>
            <ta e="T467" id="Seg_5807" s="T466">v</ta>
            <ta e="T468" id="Seg_5808" s="T467">post</ta>
            <ta e="T469" id="Seg_5809" s="T468">n</ta>
            <ta e="T470" id="Seg_5810" s="T469">v</ta>
            <ta e="T471" id="Seg_5811" s="T470">cardnum</ta>
            <ta e="T472" id="Seg_5812" s="T471">n</ta>
            <ta e="T473" id="Seg_5813" s="T472">v</ta>
            <ta e="T474" id="Seg_5814" s="T473">que</ta>
            <ta e="T475" id="Seg_5815" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_5816" s="T475">adv</ta>
            <ta e="T477" id="Seg_5817" s="T476">v</ta>
            <ta e="T478" id="Seg_5818" s="T477">v</ta>
            <ta e="T479" id="Seg_5819" s="T478">aux</ta>
            <ta e="T480" id="Seg_5820" s="T479">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-LaVN" />
         <annotation name="SyF" tierref="SyF-LaVN" />
         <annotation name="IST" tierref="IST-LaVN" />
         <annotation name="Top" tierref="Top-LaVN" />
         <annotation name="Foc" tierref="Foc-LaVN" />
         <annotation name="BOR" tierref="BOR-LaVN">
            <ta e="T15" id="Seg_5821" s="T14">RUS:cult</ta>
            <ta e="T24" id="Seg_5822" s="T23">RUS:cult</ta>
            <ta e="T26" id="Seg_5823" s="T25">RUS:cult</ta>
            <ta e="T30" id="Seg_5824" s="T29">RUS:cult</ta>
            <ta e="T33" id="Seg_5825" s="T32">RUS:mod</ta>
            <ta e="T34" id="Seg_5826" s="T33">RUS:cult</ta>
            <ta e="T41" id="Seg_5827" s="T40">RUS:cult</ta>
            <ta e="T42" id="Seg_5828" s="T41">RUS:cult</ta>
            <ta e="T47" id="Seg_5829" s="T46">RUS:cult</ta>
            <ta e="T49" id="Seg_5830" s="T48">RUS:cult</ta>
            <ta e="T58" id="Seg_5831" s="T57">RUS:cult</ta>
            <ta e="T78" id="Seg_5832" s="T77">RUS:cult</ta>
            <ta e="T98" id="Seg_5833" s="T97">RUS:cult</ta>
            <ta e="T111" id="Seg_5834" s="T110">RUS:cultEV:gram (DIM)</ta>
            <ta e="T117" id="Seg_5835" s="T116">RUS:cult</ta>
            <ta e="T130" id="Seg_5836" s="T129">RUS:core</ta>
            <ta e="T137" id="Seg_5837" s="T136">RUS:cult</ta>
            <ta e="T150" id="Seg_5838" s="T149">RUS:cultEV:gram (DIM)</ta>
            <ta e="T165" id="Seg_5839" s="T164">RUS:cult</ta>
            <ta e="T173" id="Seg_5840" s="T171">RUS:cult</ta>
            <ta e="T188" id="Seg_5841" s="T187">RUS:cult</ta>
            <ta e="T197" id="Seg_5842" s="T196">RUS:mod</ta>
            <ta e="T198" id="Seg_5843" s="T197">RUS:cult</ta>
            <ta e="T200" id="Seg_5844" s="T199">RUS:cult</ta>
            <ta e="T217" id="Seg_5845" s="T216">RUS:cult</ta>
            <ta e="T223" id="Seg_5846" s="T222">RUS:cult</ta>
            <ta e="T227" id="Seg_5847" s="T226">RUS:cult</ta>
            <ta e="T247" id="Seg_5848" s="T246">RUS:cult</ta>
            <ta e="T251" id="Seg_5849" s="T250">RUS:cult</ta>
            <ta e="T255" id="Seg_5850" s="T254">RUS:cult</ta>
            <ta e="T268" id="Seg_5851" s="T267">RUS:cult</ta>
            <ta e="T275" id="Seg_5852" s="T274">RUS:cult</ta>
            <ta e="T299" id="Seg_5853" s="T298">RUS:mod</ta>
            <ta e="T372" id="Seg_5854" s="T370">RUS:disc</ta>
            <ta e="T374" id="Seg_5855" s="T373">RUS:core</ta>
            <ta e="T393" id="Seg_5856" s="T392">RUS:cult</ta>
            <ta e="T394" id="Seg_5857" s="T393">RUS:cult</ta>
            <ta e="T396" id="Seg_5858" s="T395">EV:gram (DIM)</ta>
            <ta e="T399" id="Seg_5859" s="T398">EV:core</ta>
            <ta e="T409" id="Seg_5860" s="T408">RUS:cult</ta>
            <ta e="T413" id="Seg_5861" s="T412">RUS:cult</ta>
            <ta e="T417" id="Seg_5862" s="T416">RUS:cult</ta>
            <ta e="T420" id="Seg_5863" s="T419">RUS:cult</ta>
            <ta e="T472" id="Seg_5864" s="T471">RUS:cult</ta>
            <ta e="T476" id="Seg_5865" s="T475">RUS:core</ta>
            <ta e="T480" id="Seg_5866" s="T479">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-LaVN" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-LaVN" />
         <annotation name="CS" tierref="CS-LaVN" />
         <annotation name="fe" tierref="fe-LaVN">
            <ta e="T14" id="Seg_5867" s="T4">– Oh dear, the songs indeed those, so at our place…</ta>
            <ta e="T23" id="Seg_5868" s="T14">We are living with the herd, so we with the reindeer, we are living in tundra.</ta>
            <ta e="T33" id="Seg_5869" s="T23">The Komsomol comes, the Komsomol, at that time there were terrible Komsomol [people].</ta>
            <ta e="T35" id="Seg_5870" s="T33">Members of the party came.</ta>
            <ta e="T43" id="Seg_5871" s="T35">"Listen", they say to me, "Barbara, do you have records?"</ta>
            <ta e="T45" id="Seg_5872" s="T43">I say:</ta>
            <ta e="T48" id="Seg_5873" s="T45">"No, they are Ruslanova's."</ta>
            <ta e="T58" id="Seg_5874" s="T48">They condemned her and said "do whatchamacallit, break her record."</ta>
            <ta e="T67" id="Seg_5875" s="T58">They came to the Dolgans and said "destroy everything!".</ta>
            <ta e="T69" id="Seg_5876" s="T67">I say:</ta>
            <ta e="T74" id="Seg_5877" s="T69">"I don't have, not a single one."</ta>
            <ta e="T80" id="Seg_5878" s="T74">"What are you spinning there on the gramophone", they ask me.</ta>
            <ta e="T89" id="Seg_5879" s="T82">– Yes, I myself make it play.</ta>
            <ta e="T96" id="Seg_5880" s="T92">– I myself bought it.</ta>
            <ta e="T101" id="Seg_5881" s="T96">Yes, when I went to the Black Sea.</ta>
            <ta e="T104" id="Seg_5882" s="T101">I myself bought it.</ta>
            <ta e="T115" id="Seg_5883" s="T104">Having bought it myself, it has a needle, a single box, we turn it ourselves.</ta>
            <ta e="T121" id="Seg_5884" s="T115">They we put a gramophone there, that…</ta>
            <ta e="T132" id="Seg_5885" s="T121">Well "Valenki, Valenki", I have two with very beautiful songs.</ta>
            <ta e="T138" id="Seg_5886" s="T135">– Aha, Ruslanova sings.</ta>
            <ta e="T147" id="Seg_5887" s="T138">"Valenki, Valenki, not seamed" maybe, "hide them" though.</ta>
            <ta e="T152" id="Seg_5888" s="T147">I made my child a whatchamacallit, such a feather bed.</ta>
            <ta e="T154" id="Seg_5889" s="T152">The small child.</ta>
            <ta e="T163" id="Seg_5890" s="T154">Having sewn it, there (…) I put it there.</ta>
            <ta e="T166" id="Seg_5891" s="T163">I hid two records.</ta>
            <ta e="T169" id="Seg_5892" s="T166">Until now I am hinding them.</ta>
            <ta e="T174" id="Seg_5893" s="T171">– Those of Ruslanova.</ta>
            <ta e="T181" id="Seg_5894" s="T174">Oh, indeed, what sort of ones were those.</ta>
            <ta e="T195" id="Seg_5895" s="T185">– Those my records, the children, having grown up, broke them.</ta>
            <ta e="T202" id="Seg_5896" s="T195">Well, Lyusya accidentally, the housewife, at all.</ta>
            <ta e="T223" id="Seg_5897" s="T214">– At that time there was Stalin, yes, then, that.</ta>
            <ta e="T229" id="Seg_5898" s="T223">After that she was condemned, that one.</ta>
            <ta e="T236" id="Seg_5899" s="T229">Because, well, there was war at that time.</ta>
            <ta e="T244" id="Seg_5900" s="T236">They sang and went around.</ta>
            <ta e="T256" id="Seg_5901" s="T244">Then she into war, she goes into war, they make artists sing.</ta>
            <ta e="T271" id="Seg_5902" s="T256">But she sang some songs in the old tradition, therefore they were condemned, the poor ones.</ta>
            <ta e="T276" id="Seg_5903" s="T271">Together with her husband she was condemned, apparently.</ta>
            <ta e="T281" id="Seg_5904" s="T276">Like that people were before.</ta>
            <ta e="T319" id="Seg_5905" s="T293">– And for that they don't listen to those songs, they simply something, wreckers, something, they took away ours.</ta>
            <ta e="T344" id="Seg_5906" s="T329">– And apparently so, they take those away then, and we are hiding, well, that one, oj, indeed.</ta>
            <ta e="T350" id="Seg_5907" s="T346">– Yes, remembering it now.</ta>
            <ta e="T365" id="Seg_5908" s="T355">– Well, people are afraid, at that time they came, were destroying, they came. </ta>
            <ta e="T376" id="Seg_5909" s="T370">– Well, she was my friend.</ta>
            <ta e="T381" id="Seg_5910" s="T376">She, oh dear, doesn't go to town.</ta>
            <ta e="T388" id="Seg_5911" s="T381">"We will die, hide that whatchamacallit", she says to me.</ta>
            <ta e="T390" id="Seg_5912" s="T388">"Why then?"</ta>
            <ta e="T399" id="Seg_5913" s="T390">"Well, they break records of Ruslanova, I have hidden one", my friend says.</ta>
            <ta e="T404" id="Seg_5914" s="T399">Well, so both of us hid one.</ta>
            <ta e="T412" id="Seg_5915" s="T408">– It was Anna, the late.</ta>
            <ta e="T421" id="Seg_5916" s="T412">Nina's mother, it was this Nina Goruo's mother, the late Anna.</ta>
            <ta e="T434" id="Seg_5917" s="T429">– Ours were Dolgans.</ta>
            <ta e="T436" id="Seg_5918" s="T434">They were Dolgans.</ta>
            <ta e="T448" id="Seg_5919" s="T436">Well, earlier, you know how, they was everything, some acted like Russians, some were like that.</ta>
            <ta e="T466" id="Seg_5920" s="T459">– I didn't drink alcohol, until now(?) I didn't drink.</ta>
            <ta e="T475" id="Seg_5921" s="T466">Until I got old I buried many people, I never drank a single glass.</ta>
            <ta e="T480" id="Seg_5922" s="T475">I was always stayed sober.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-LaVN">
            <ta e="T14" id="Seg_5923" s="T4">– Oh ja, die Lieder diese wirklich, also bei uns… </ta>
            <ta e="T23" id="Seg_5924" s="T14">Wir leben mit der Herde, also wir mit Rentieren, wir leben in der Tundra.</ta>
            <ta e="T33" id="Seg_5925" s="T23">Der Komsomol kommt, der Komsomol, damals gab es fürchterliche Komsomolzen.</ta>
            <ta e="T35" id="Seg_5926" s="T33">Parteimitglieder kamen.</ta>
            <ta e="T43" id="Seg_5927" s="T35">"Hör mal", sagen sie mir, "Barbara, hast du Schallplatten?"</ta>
            <ta e="T45" id="Seg_5928" s="T43">Ich sage:</ta>
            <ta e="T48" id="Seg_5929" s="T45">"Nein, das sind Ruslanovas."</ta>
            <ta e="T58" id="Seg_5930" s="T48">Sie verurteilen sie und sagten "macht dies, zerstört ihre Schallplatte."</ta>
            <ta e="T67" id="Seg_5931" s="T58">Sie kamen zu den Dolganen und sagten "Zerstört alles!".</ta>
            <ta e="T69" id="Seg_5932" s="T67">Ich sage:</ta>
            <ta e="T74" id="Seg_5933" s="T69">"Ich habe keine, keine einzige."</ta>
            <ta e="T80" id="Seg_5934" s="T74">"Was spielst du auf dem Plattenspieler", fragen sie mich.</ta>
            <ta e="T89" id="Seg_5935" s="T82">– Ja, den lasse ich selbst spielen.</ta>
            <ta e="T96" id="Seg_5936" s="T92">– Ich habe ihn selbst gekauft. </ta>
            <ta e="T101" id="Seg_5937" s="T96">Ja, als ich ans Schwarze Meer gefahren bin.</ta>
            <ta e="T104" id="Seg_5938" s="T101">Ich habe ihn selbst gekauft.</ta>
            <ta e="T115" id="Seg_5939" s="T104">Ich habe ihn selbst gekauft, er hat eine Nadel, ein einzelnes Schächtelchen, wir drehen ihn selbst.</ta>
            <ta e="T121" id="Seg_5940" s="T115">Dann legen wir eine Schallplatte auf, diese…</ta>
            <ta e="T132" id="Seg_5941" s="T121">Nun "Valenki, Valenki", zwei habe ich mit sehr schönen Lieder.</ta>
            <ta e="T138" id="Seg_5942" s="T135">– Ja, Ruslanova singt.</ta>
            <ta e="T147" id="Seg_5943" s="T138">"Valenki, Valenki, nicht gesäumt" vielleicht, "verstecke diese" also.</ta>
            <ta e="T152" id="Seg_5944" s="T147">Ich machte meinem Kind so ein Dings, so ein altes Federbett.</ta>
            <ta e="T154" id="Seg_5945" s="T152">Das kleine Kind.</ta>
            <ta e="T163" id="Seg_5946" s="T154">Nachdem ich es genäht habe, dorthin (…) lege ich sie dorthin.</ta>
            <ta e="T166" id="Seg_5947" s="T163">Zwei Schallplatten versteckte ich.</ta>
            <ta e="T169" id="Seg_5948" s="T166">Bis jetzt habe ich sie versteckt.</ta>
            <ta e="T174" id="Seg_5949" s="T171">– Die von Ruslanova.</ta>
            <ta e="T181" id="Seg_5950" s="T174">Oh, wirklich, was waren das für welche.</ta>
            <ta e="T195" id="Seg_5951" s="T185">– Diese Schallplatten von mir haben die Kinder, als sie größer waren, kaputt gemacht.</ta>
            <ta e="T202" id="Seg_5952" s="T195">Nun ausversehen Ljusja, die Hausfrau, ganz.</ta>
            <ta e="T223" id="Seg_5953" s="T214">– Damals war Stalin, ja, damals, dieses.</ta>
            <ta e="T229" id="Seg_5954" s="T223">Danach hat man sie verurteilt, jene.</ta>
            <ta e="T236" id="Seg_5955" s="T229">Weil, nun es war Krieg damals.</ta>
            <ta e="T244" id="Seg_5956" s="T236">Sie sangen und gingen so herum.</ta>
            <ta e="T256" id="Seg_5957" s="T244">Dann sie in den Krieg, sie geht in den Krieg, sie lassen die Künstler singen.</ta>
            <ta e="T271" id="Seg_5958" s="T256">Aber sie sang einige Lieder wie früher, deshalb hat man sie verurteilt, oh, die Armen.</ta>
            <ta e="T276" id="Seg_5959" s="T271">Mit ihrem Mann hat man sie verurteilt.</ta>
            <ta e="T281" id="Seg_5960" s="T276">So waren die Leute früher.</ta>
            <ta e="T319" id="Seg_5961" s="T293">– Und damit sie diese Lieder nicht hören, einfach sie irgendwas, Schädlinge, irgendwas, damals nahmen sie die unseren weg.</ta>
            <ta e="T344" id="Seg_5962" s="T329">– Und offenbar so, sie nehmen sie dann weg, und wir verstecken, oh, solche.</ta>
            <ta e="T350" id="Seg_5963" s="T346">– Ja, wenn man sich jetzt erinnert.</ta>
            <ta e="T365" id="Seg_5964" s="T355">– Die Menschen haben Angst, damals kamen sie, zerstörten, sie kamen.</ta>
            <ta e="T376" id="Seg_5965" s="T370">– Nun, sie war meine Freundin.</ta>
            <ta e="T381" id="Seg_5966" s="T376">Sie geht, mein Gott, nicht in die Stadt.</ta>
            <ta e="T388" id="Seg_5967" s="T381">"Wir sterben, versteck das Ding", sagt sie mir.</ta>
            <ta e="T390" id="Seg_5968" s="T388">"Warum denn?"</ta>
            <ta e="T399" id="Seg_5969" s="T390">"Hey, sie zerstören Ruslanova-Schallplatten, ich habe eine versteckt", sagt meine Freundin.</ta>
            <ta e="T404" id="Seg_5970" s="T399">Nun, so verstecken wir beide eine.</ta>
            <ta e="T412" id="Seg_5971" s="T408">– Anna hieß sie, die Selige.</ta>
            <ta e="T421" id="Seg_5972" s="T412">Die Mutter von Nina, von dieser Nina Goruo war sie die Mutter, die verstorbene Anna.</ta>
            <ta e="T434" id="Seg_5973" s="T429">– Unsere waren Dolganen.</ta>
            <ta e="T436" id="Seg_5974" s="T434">Sie waren Dolganen.</ta>
            <ta e="T448" id="Seg_5975" s="T436">Nun, früher, du weißt wie, es gab alles, einige hielten sich für Russen, einige waren so.</ta>
            <ta e="T466" id="Seg_5976" s="T459">– Ich habe keinen Alkohol getrunken, bis heute(?) habe ich nicht getrunken.</ta>
            <ta e="T475" id="Seg_5977" s="T466">Bis ich alt geworden bin habe ich viele begraben, nie habe ich auch nur ein Gläschen getrunken.</ta>
            <ta e="T480" id="Seg_5978" s="T475">Immer bin ich nüchtern geblieben.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-LaVN" />
         <annotation name="ltr" tierref="ltr-LaVN">
            <ta e="T14" id="Seg_5979" s="T4">ВН: Ой, те песни ну это-то, правда-правда, ну у нас…</ta>
            <ta e="T23" id="Seg_5980" s="T14">В стаде находимся, значит, мы на оленях, в тундре ведь находимся.</ta>
            <ta e="T33" id="Seg_5981" s="T23">Комсомол появился, комсомольцы, тогда ярые комсомольцы были вовсе.</ta>
            <ta e="T35" id="Seg_5982" s="T33">Партийные пришли.</ta>
            <ta e="T43" id="Seg_5983" s="T35">"Слушай, – говорят мне, – ну, Варвара, пластинки твои есть?"</ta>
            <ta e="T45" id="Seg_5984" s="T43">Я говорю:</ta>
            <ta e="T48" id="Seg_5985" s="T45">"Нет, Руслановой которые."</ta>
            <ta e="T58" id="Seg_5986" s="T48">Судили её и сказали "этовайте… ВН: …разломайте её пластинку.</ta>
            <ta e="T67" id="Seg_5987" s="T58">Это к долганам ушли (пластин-) всё разбейте," – сказали.</ta>
            <ta e="T69" id="Seg_5988" s="T67">Я говорю:</ta>
            <ta e="T74" id="Seg_5989" s="T69">"Нет у меня, ни одной нет".</ta>
            <ta e="T80" id="Seg_5990" s="T74">"Что крутишь в этом патефоне", – спрашивают у меня.</ta>
            <ta e="T89" id="Seg_5991" s="T82">ВН: Да, слушаю сама-то.</ta>
            <ta e="T96" id="Seg_5992" s="T92">ВН: Я сама покупала.</ta>
            <ta e="T101" id="Seg_5993" s="T96">Да, в тот раз, на Чёрное море когда ездила тогда.</ta>
            <ta e="T104" id="Seg_5994" s="T101">Сама покупала ведь.</ta>
            <ta e="T115" id="Seg_5995" s="T104">Сама как купила и он иглу имеет, отдельная коробочка, его сами крутим.</ta>
            <ta e="T121" id="Seg_5996" s="T115">Потом пластинку ставим, те (песн-)…</ta>
            <ta e="T132" id="Seg_5997" s="T121">Вот "Валенки, валенки" две было у меня пластинки, с красивой очень песней.</ta>
            <ta e="T138" id="Seg_5998" s="T135">ВН: Да, Русланова поёт:</ta>
            <ta e="T147" id="Seg_5999" s="T138">"Валенки, валенки не подшиты" будь, и вот её давай прятать ведь.</ta>
            <ta e="T152" id="Seg_6000" s="T147">Ребёнку это, перинку небольшую сделала.</ta>
            <ta e="T154" id="Seg_6001" s="T152">Маленькому ребёнку.</ta>
            <ta e="T163" id="Seg_6002" s="T154">Внутрь того сшила… и туда вложила.</ta>
            <ta e="T166" id="Seg_6003" s="T163">Две пластинки спрятала.</ta>
            <ta e="T169" id="Seg_6004" s="T166">До сих пор прятала.</ta>
            <ta e="T174" id="Seg_6005" s="T171">ВН: Руслановой которые.</ta>
            <ta e="T181" id="Seg_6006" s="T174">О-ой, правда, какие были, правда. </ta>
            <ta e="T195" id="Seg_6007" s="T185">ВН: Те пластинки мои эти дети, как подросли вдребезги разбили.</ta>
            <ta e="T202" id="Seg_6008" s="T195">Вот нечаянно Люся вот хозяйкой была, всё.</ta>
            <ta e="T223" id="Seg_6009" s="T214">ВН: Тогда Сталин был, да тогда.</ta>
            <ta e="T229" id="Seg_6010" s="T223">После этого её судили, её тогда.</ta>
            <ta e="T236" id="Seg_6011" s="T229">Потому что, но тогда-то война была.</ta>
            <ta e="T244" id="Seg_6012" s="T236">Они-то песни пели ходили-то.</ta>
            <ta e="T256" id="Seg_6013" s="T244">Тогда она в войну вот… как идёт в войну ведь, просят спеть артистов-то.</ta>
            <ta e="T271" id="Seg_6014" s="T256">Но она под староть какие-то песни пела, поэтому их судили, эх, бедненькие.</ta>
            <ta e="T276" id="Seg_6015" s="T271">С мужем их вдвоём судили.</ta>
            <ta e="T281" id="Seg_6016" s="T276">Какие были они раньше люди.</ta>
            <ta e="T319" id="Seg_6017" s="T293">ВН: И вот песни чтоб не слушали, просто о них вроде вредители или какие-то ли, и вот это наши вот снимают вот.</ta>
            <ta e="T344" id="Seg_6018" s="T329">ВН: И наверно, и вот, так снимали ведь, и поэтому прячем это, ой такая, правда.</ta>
            <ta e="T350" id="Seg_6019" s="T346">ВН: Да сейчас если вспомнить.</ta>
            <ta e="T365" id="Seg_6020" s="T355">ВН: Человеку страшно было, тогда приходили и разламывали ведь, приходят.</ta>
            <ta e="T376" id="Seg_6021" s="T370">НК: Ну, моя подрушка была она.</ta>
            <ta e="T381" id="Seg_6022" s="T376">Она в город, жуть, как боялась идти.</ta>
            <ta e="T388" id="Seg_6023" s="T381">"Смерть наша пришла, это спрячь", – говорит мне.</ta>
            <ta e="T390" id="Seg_6024" s="T388">"Зачем, да…?"</ta>
            <ta e="T399" id="Seg_6025" s="T390">"Боже, разбивают Руслановой пластинки, я одну спрятала", – говорит подруга.</ta>
            <ta e="T404" id="Seg_6026" s="T399">И вот вдвоём прячем ведь.</ta>
            <ta e="T412" id="Seg_6027" s="T408">ВН: Анна звали, покойная та.</ta>
            <ta e="T421" id="Seg_6028" s="T412">Нинина мать, вот этой Нины Гроо мать была, Анна покойная.</ta>
            <ta e="T434" id="Seg_6029" s="T429">ВН: Долганы были наши.</ta>
            <ta e="T436" id="Seg_6030" s="T434">Долганы были.</ta>
            <ta e="T448" id="Seg_6031" s="T436">Ну раньше знаешь как, всякое было, некоторые по русских делались, некоторые такие были.</ta>
            <ta e="T466" id="Seg_6032" s="T459">ВН: Спиртное не употребляла, до сих пор не пила.</ta>
            <ta e="T475" id="Seg_6033" s="T466">До самой старости скольких похоронила, ни одной рюмки не выпила никогда.</ta>
            <ta e="T480" id="Seg_6034" s="T475">Всегда трезвая ходила, всегда.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-LaVN" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T162" />
            <conversion-tli id="T161" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T380" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T243" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T270" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T280" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KuNS"
                          name="ref"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KuNS"
                          name="st"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KuNS"
                          name="ts"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KuNS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KuNS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KuNS"
                          name="mb"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KuNS"
                          name="mp"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KuNS"
                          name="ge"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KuNS"
                          name="gg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KuNS"
                          name="gr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KuNS"
                          name="mc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KuNS"
                          name="ps"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KuNS"
                          name="SeR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KuNS"
                          name="SyF"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KuNS"
                          name="IST"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KuNS"
                          name="Top"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KuNS"
                          name="Foc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KuNS"
                          name="BOR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KuNS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KuNS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KuNS"
                          name="CS"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KuNS"
                          name="fe"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KuNS"
                          name="fg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KuNS"
                          name="fr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KuNS"
                          name="ltr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KuNS"
                          name="nt"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-LaVN"
                          name="ref"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-LaVN"
                          name="st"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-LaVN"
                          name="ts"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-LaVN"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-LaVN"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-LaVN"
                          name="mb"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-LaVN"
                          name="mp"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-LaVN"
                          name="ge"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-LaVN"
                          name="gg"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-LaVN"
                          name="gr"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-LaVN"
                          name="mc"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-LaVN"
                          name="ps"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-LaVN"
                          name="SeR"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-LaVN"
                          name="SyF"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-LaVN"
                          name="IST"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-LaVN"
                          name="Top"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-LaVN"
                          name="Foc"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-LaVN"
                          name="BOR"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-LaVN"
                          name="BOR-Phon"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-LaVN"
                          name="BOR-Morph"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-LaVN"
                          name="CS"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-LaVN"
                          name="fe"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-LaVN"
                          name="fg"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-LaVN"
                          name="fr"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-LaVN"
                          name="ltr"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-LaVN"
                          name="nt"
                          segmented-tier-id="tx-LaVN"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
