<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1FD9A7D3-7E4A-1EF0-16E7-CAB88B04C829">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>ChSA_KuNS_2004_ReindeerHerding_conv</transcription-name>
         <referenced-file url="ChSA_KuNS_2004_ReindeerHerding_conv.wav" />
         <referenced-file url="ChSA_KuNS_2004_ReindeerHerding_conv.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\conv\ChSA_KuNS_2004_ReindeerHerding_conv\ChSA_KuNS_2004_ReindeerHerding_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1049</ud-information>
            <ud-information attribute-name="# HIAT:w">700</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# e">700</ud-information>
            <ud-information attribute-name="# HIAT:u">112</ud-information>
            <ud-information attribute-name="# sc">72</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KuNS">
            <abbreviation>KuNS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="ChSA">
            <abbreviation>ChSA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.403" type="appl" />
         <tli id="T2" time="0.807" type="appl" />
         <tli id="T3" time="1.21" type="appl" />
         <tli id="T4" time="1.613" type="appl" />
         <tli id="T5" time="2.017" type="appl" />
         <tli id="T6" time="2.173320578540904" />
         <tli id="T7" time="2.861" type="appl" />
         <tli id="T8" time="3.353" type="appl" />
         <tli id="T9" time="3.844" type="appl" />
         <tli id="T10" time="4.336" type="appl" />
         <tli id="T11" time="4.827" type="appl" />
         <tli id="T12" time="5.319" type="appl" />
         <tli id="T13" time="5.81" type="appl" />
         <tli id="T14" time="6.402" type="appl" />
         <tli id="T15" time="6.994" type="appl" />
         <tli id="T16" time="7.586" type="appl" />
         <tli id="T17" time="7.963" type="appl" />
         <tli id="T18" time="8.34" type="appl" />
         <tli id="T19" time="8.717" type="appl" />
         <tli id="T20" time="9.094" type="appl" />
         <tli id="T21" time="9.471" type="appl" />
         <tli id="T22" time="9.848" type="appl" />
         <tli id="T23" time="10.225" type="appl" />
         <tli id="T24" time="10.602" type="appl" />
         <tli id="T25" time="10.98" type="appl" />
         <tli id="T26" time="11.357" type="appl" />
         <tli id="T27" time="11.734" type="appl" />
         <tli id="T28" time="12.111" type="appl" />
         <tli id="T29" time="12.488" type="appl" />
         <tli id="T30" time="12.865" type="appl" />
         <tli id="T31" time="13.242" type="appl" />
         <tli id="T32" time="13.619" type="appl" />
         <tli id="T33" time="14.306" type="appl" />
         <tli id="T34" time="14.993" type="appl" />
         <tli id="T35" time="15.68" type="appl" />
         <tli id="T36" time="16.104" type="appl" />
         <tli id="T37" time="16.527" type="appl" />
         <tli id="T38" time="16.951" type="appl" />
         <tli id="T39" time="17.374" type="appl" />
         <tli id="T40" time="17.798" type="appl" />
         <tli id="T41" time="18.221" type="appl" />
         <tli id="T42" time="18.645" type="appl" />
         <tli id="T43" time="19.05" type="appl" />
         <tli id="T44" time="19.455" type="appl" />
         <tli id="T45" time="19.86" type="appl" />
         <tli id="T46" time="20.265" type="appl" />
         <tli id="T47" time="20.67" type="appl" />
         <tli id="T48" time="21.075" type="appl" />
         <tli id="T49" time="21.48" type="appl" />
         <tli id="T50" time="21.893" type="appl" />
         <tli id="T51" time="22.307" type="appl" />
         <tli id="T52" time="22.72" type="appl" />
         <tli id="T53" time="23.185" type="appl" />
         <tli id="T54" time="23.65" type="appl" />
         <tli id="T55" time="24.079" type="appl" />
         <tli id="T56" time="24.507" type="appl" />
         <tli id="T57" time="24.936" type="appl" />
         <tli id="T58" time="25.364" type="appl" />
         <tli id="T59" time="25.793" type="appl" />
         <tli id="T60" time="26.221" type="appl" />
         <tli id="T61" time="26.65" type="appl" />
         <tli id="T62" time="27.31" type="appl" />
         <tli id="T63" time="27.97" type="appl" />
         <tli id="T64" time="28.486499484985533" />
         <tli id="T65" time="28.948" type="appl" />
         <tli id="T66" time="29.266" type="appl" />
         <tli id="T67" time="29.584" type="appl" />
         <tli id="T68" time="29.902" type="appl" />
         <tli id="T69" time="30.22" type="appl" />
         <tli id="T70" time="30.843" type="appl" />
         <tli id="T71" time="31.467" type="appl" />
         <tli id="T72" time="31.886479531169154" />
         <tli id="T73" time="32.463" type="appl" />
         <tli id="T74" time="32.837" type="appl" />
         <tli id="T75" time="33.21" type="appl" />
         <tli id="T76" time="33.583" type="appl" />
         <tli id="T77" time="33.957" type="appl" />
         <tli id="T78" time="34.33" type="appl" />
         <tli id="T79" time="34.794" type="appl" />
         <tli id="T80" time="35.258" type="appl" />
         <tli id="T81" time="35.721" type="appl" />
         <tli id="T82" time="36.185" type="appl" />
         <tli id="T83" time="36.649" type="appl" />
         <tli id="T84" time="37.199" type="appl" />
         <tli id="T85" time="37.75" type="appl" />
         <tli id="T86" time="38.3" type="appl" />
         <tli id="T87" time="38.85" type="appl" />
         <tli id="T88" time="39.495" type="appl" />
         <tli id="T89" time="39.906432463637586" />
         <tli id="T90" time="40.594" type="appl" />
         <tli id="T91" time="41.019" type="appl" />
         <tli id="T92" time="41.443" type="appl" />
         <tli id="T93" time="41.867" type="appl" />
         <tli id="T94" time="42.291" type="appl" />
         <tli id="T95" time="42.716" type="appl" />
         <tli id="T96" time="43.14" type="appl" />
         <tli id="T97" time="43.604" type="appl" />
         <tli id="T98" time="44.068" type="appl" />
         <tli id="T99" time="44.533" type="appl" />
         <tli id="T100" time="44.997" type="appl" />
         <tli id="T101" time="45.874" type="appl" />
         <tli id="T102" time="46.75" type="appl" />
         <tli id="T103" time="47.627" type="appl" />
         <tli id="T104" time="48.503" type="appl" />
         <tli id="T105" time="49.38" type="appl" />
         <tli id="T106" time="49.946" type="appl" />
         <tli id="T107" time="50.513" type="appl" />
         <tli id="T108" time="51.079" type="appl" />
         <tli id="T109" time="51.645" type="appl" />
         <tli id="T110" time="52.211" type="appl" />
         <tli id="T111" time="52.778" type="appl" />
         <tli id="T112" time="53.344" type="appl" />
         <tli id="T113" time="53.929" type="appl" />
         <tli id="T114" time="54.513" type="appl" />
         <tli id="T115" time="55.098" type="appl" />
         <tli id="T116" time="55.682" type="appl" />
         <tli id="T117" time="56.267" type="appl" />
         <tli id="T118" time="56.852" type="appl" />
         <tli id="T119" time="57.436" type="appl" />
         <tli id="T120" time="58.021" type="appl" />
         <tli id="T121" time="58.605" type="appl" />
         <tli id="T122" time="58.77965503372746" />
         <tli id="T123" time="59.542" type="appl" />
         <tli id="T124" time="60.014" type="appl" />
         <tli id="T125" time="60.486" type="appl" />
         <tli id="T126" time="60.958" type="appl" />
         <tli id="T127" time="61.05297502539142" />
         <tli id="T128" time="61.509" type="appl" />
         <tli id="T129" time="61.998" type="appl" />
         <tli id="T130" time="62.487" type="appl" />
         <tli id="T131" time="62.976" type="appl" />
         <tli id="T132" time="63.465" type="appl" />
         <tli id="T133" time="63.954" type="appl" />
         <tli id="T134" time="64.475" type="appl" />
         <tli id="T135" time="64.996" type="appl" />
         <tli id="T136" time="65.516" type="appl" />
         <tli id="T137" time="66.037" type="appl" />
         <tli id="T138" time="66.558" type="appl" />
         <tli id="T139" time="67.47" type="appl" />
         <tli id="T140" time="68.15" type="appl" />
         <tli id="T141" time="68.83" type="appl" />
         <tli id="T142" time="69.523" type="appl" />
         <tli id="T143" time="70.227" type="appl" />
         <tli id="T144" time="70.93" type="appl" />
         <tli id="T145" time="71.633" type="appl" />
         <tli id="T146" time="72.337" type="appl" />
         <tli id="T147" time="72.52624102437575" />
         <tli id="T148" time="73.438" type="appl" />
         <tli id="T149" time="74.035" type="appl" />
         <tli id="T150" time="74.633" type="appl" />
         <tli id="T151" time="75.23" type="appl" />
         <tli id="T152" time="75.783" type="appl" />
         <tli id="T153" time="76.337" type="appl" />
         <tli id="T154" time="76.89" type="appl" />
         <tli id="T155" time="77.443" type="appl" />
         <tli id="T156" time="77.997" type="appl" />
         <tli id="T157" time="78.55" type="appl" />
         <tli id="T158" time="79.103" type="appl" />
         <tli id="T159" time="79.657" type="appl" />
         <tli id="T160" time="80.21" type="appl" />
         <tli id="T161" time="80.763" type="appl" />
         <tli id="T162" time="81.317" type="appl" />
         <tli id="T163" time="81.87" type="appl" />
         <tli id="T164" time="82.423" type="appl" />
         <tli id="T165" time="82.977" type="appl" />
         <tli id="T166" time="83.53" type="appl" />
         <tli id="T167" time="84.65" type="appl" />
         <tli id="T168" time="85.376" type="appl" />
         <tli id="T169" time="86.103" type="appl" />
         <tli id="T170" time="86.83" type="appl" />
         <tli id="T171" time="87.242" type="appl" />
         <tli id="T172" time="87.654" type="appl" />
         <tli id="T173" time="88.066" type="appl" />
         <tli id="T174" time="88.478" type="appl" />
         <tli id="T175" time="88.89" type="appl" />
         <tli id="T176" time="89.302" type="appl" />
         <tli id="T177" time="89.714" type="appl" />
         <tli id="T178" time="90.126" type="appl" />
         <tli id="T179" time="90.538" type="appl" />
         <tli id="T180" time="90.95" type="appl" />
         <tli id="T181" time="91.434" type="appl" />
         <tli id="T182" time="91.919" type="appl" />
         <tli id="T183" time="92.403" type="appl" />
         <tli id="T184" time="92.887" type="appl" />
         <tli id="T185" time="93.372" type="appl" />
         <tli id="T186" time="93.856" type="appl" />
         <tli id="T187" time="94.543" type="appl" />
         <tli id="T188" time="95.23" type="appl" />
         <tli id="T189" time="95.806" type="appl" />
         <tli id="T190" time="96.382" type="appl" />
         <tli id="T191" time="96.958" type="appl" />
         <tli id="T192" time="97.534" type="appl" />
         <tli id="T193" time="97.97275835042065" />
         <tli id="T194" time="98.74" type="appl" />
         <tli id="T195" time="99.369" type="appl" />
         <tli id="T196" time="99.999" type="appl" />
         <tli id="T197" time="100.628" type="appl" />
         <tli id="T198" time="101.258" type="appl" />
         <tli id="T199" time="101.888" type="appl" />
         <tli id="T200" time="102.517" type="appl" />
         <tli id="T201" time="103.147" type="appl" />
         <tli id="T202" time="103.713" type="appl" />
         <tli id="T203" time="104.28" type="appl" />
         <tli id="T204" time="104.846" type="appl" />
         <tli id="T205" time="105.412" type="appl" />
         <tli id="T206" time="105.978" type="appl" />
         <tli id="T207" time="106.545" type="appl" />
         <tli id="T208" time="107.111" type="appl" />
         <tli id="T209" time="107.677" type="appl" />
         <tli id="T210" time="108.244" type="appl" />
         <tli id="T211" time="108.81" type="appl" />
         <tli id="T212" time="109.156" type="appl" />
         <tli id="T213" time="109.502" type="appl" />
         <tli id="T214" time="109.848" type="appl" />
         <tli id="T215" time="110.193" type="appl" />
         <tli id="T216" time="110.539" type="appl" />
         <tli id="T217" time="110.885" type="appl" />
         <tli id="T218" time="111.322" type="appl" />
         <tli id="T219" time="111.759" type="appl" />
         <tli id="T220" time="112.195" type="appl" />
         <tli id="T221" time="112.632" type="appl" />
         <tli id="T222" time="113.069" type="appl" />
         <tli id="T223" time="113.506" type="appl" />
         <tli id="T224" time="113.943" type="appl" />
         <tli id="T225" time="114.38" type="appl" />
         <tli id="T226" time="114.816" type="appl" />
         <tli id="T227" time="115.253" type="appl" />
         <tli id="T228" time="115.69" type="appl" />
         <tli id="T229" time="116.153" type="appl" />
         <tli id="T230" time="116.616" type="appl" />
         <tli id="T231" time="117.079" type="appl" />
         <tli id="T232" time="117.542" type="appl" />
         <tli id="T233" time="118.005" type="appl" />
         <tli id="T234" time="118.468" type="appl" />
         <tli id="T235" time="118.931" type="appl" />
         <tli id="T236" time="119.395" type="appl" />
         <tli id="T237" time="119.858" type="appl" />
         <tli id="T238" time="120.321" type="appl" />
         <tli id="T239" time="120.784" type="appl" />
         <tli id="T240" time="121.247" type="appl" />
         <tli id="T241" time="121.71" type="appl" />
         <tli id="T242" time="122.173" type="appl" />
         <tli id="T243" time="122.636" type="appl" />
         <tli id="T244" time="123.212" type="appl" />
         <tli id="T245" time="123.789" type="appl" />
         <tli id="T246" time="124.365" type="appl" />
         <tli id="T247" time="124.942" type="appl" />
         <tli id="T248" time="125.518" type="appl" />
         <tli id="T249" time="126.095" type="appl" />
         <tli id="T250" time="126.671" type="appl" />
         <tli id="T251" time="127.248" type="appl" />
         <tli id="T252" time="127.824" type="appl" />
         <tli id="T253" time="128.401" type="appl" />
         <tli id="T254" time="128.977" type="appl" />
         <tli id="T255" time="129.554" type="appl" />
         <tli id="T256" time="129.65257242780214" />
         <tli id="T257" time="130.9992311911925" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" time="131.081" type="appl" />
         <tli id="T261" time="131.751" type="appl" />
         <tli id="T262" time="132.421" type="appl" />
         <tli id="T263" time="133.09" type="appl" />
         <tli id="T264" time="133.76" type="appl" />
         <tli id="T265" time="134.43" type="appl" />
         <tli id="T266" time="135.014" type="appl" />
         <tli id="T267" time="135.598" type="appl" />
         <tli id="T268" time="136.182" type="appl" />
         <tli id="T269" time="136.766" type="appl" />
         <tli id="T270" time="137.35" type="appl" />
         <tli id="T271" time="137.943" type="appl" />
         <tli id="T272" time="138.536" type="appl" />
         <tli id="T273" time="139.169" type="appl" />
         <tli id="T274" time="139.802" type="appl" />
         <tli id="T275" time="140.435" type="appl" />
         <tli id="T276" time="141.068" type="appl" />
         <tli id="T277" time="141.701" type="appl" />
         <tli id="T278" time="142.334" type="appl" />
         <tli id="T279" time="142.967" type="appl" />
         <tli id="T280" time="143.6" type="appl" />
         <tli id="T281" time="143.983" type="appl" />
         <tli id="T282" time="144.366" type="appl" />
         <tli id="T283" time="144.749" type="appl" />
         <tli id="T284" time="145.132" type="appl" />
         <tli id="T285" time="145.515" type="appl" />
         <tli id="T286" time="145.898" type="appl" />
         <tli id="T287" time="146.281" type="appl" />
         <tli id="T288" time="146.664" type="appl" />
         <tli id="T289" time="147.053" type="appl" />
         <tli id="T290" time="147.443" type="appl" />
         <tli id="T291" time="147.832" type="appl" />
         <tli id="T292" time="148.222" type="appl" />
         <tli id="T293" time="148.612" type="appl" />
         <tli id="T294" time="149.001" type="appl" />
         <tli id="T295" time="149.39" type="appl" />
         <tli id="T296" time="149.78" type="appl" />
         <tli id="T297" time="150.156" type="appl" />
         <tli id="T298" time="150.532" type="appl" />
         <tli id="T299" time="150.908" type="appl" />
         <tli id="T300" time="151.284" type="appl" />
         <tli id="T301" time="151.35911170304504" />
         <tli id="T302" time="152.039" type="appl" />
         <tli id="T303" time="152.498" type="appl" />
         <tli id="T304" time="152.956" type="appl" />
         <tli id="T305" time="153.415" type="appl" />
         <tli id="T306" time="153.874" type="appl" />
         <tli id="T307" time="154.302" type="appl" />
         <tli id="T308" time="154.5924260606903" />
         <tli id="T309" time="155.261" type="appl" />
         <tli id="T310" time="155.792" type="appl" />
         <tli id="T311" time="156.324" type="appl" />
         <tli id="T312" time="156.855" type="appl" />
         <tli id="T313" time="157.386" type="appl" />
         <tli id="T314" time="157.918" type="appl" />
         <tli id="T315" time="158.449" type="appl" />
         <tli id="T316" time="158.98" type="appl" />
         <tli id="T317" time="159.343" type="appl" />
         <tli id="T318" time="159.45906416601193" />
         <tli id="T319" time="159.55906357913497" />
         <tli id="T320" time="159.87906170112873" />
         <tli id="T321" time="160.309" type="appl" />
         <tli id="T322" time="160.754" type="appl" />
         <tli id="T323" time="161.199" type="appl" />
         <tli id="T324" time="161.643" type="appl" />
         <tli id="T325" time="162.088" type="appl" />
         <tli id="T326" time="162.533" type="appl" />
         <tli id="T327" time="162.978" type="appl" />
         <tli id="T328" time="163.422" type="appl" />
         <tli id="T329" time="163.867" type="appl" />
         <tli id="T330" time="164.312" type="appl" />
         <tli id="T331" time="164.756" type="appl" />
         <tli id="T332" time="165.201" type="appl" />
         <tli id="T333" time="165.653" type="appl" />
         <tli id="T334" time="166.104" type="appl" />
         <tli id="T335" time="166.556" type="appl" />
         <tli id="T336" time="167.008" type="appl" />
         <tli id="T337" time="167.46" type="appl" />
         <tli id="T338" time="167.911" type="appl" />
         <tli id="T339" time="168.363" type="appl" />
         <tli id="T340" time="168.815" type="appl" />
         <tli id="T341" time="169.266" type="appl" />
         <tli id="T342" time="169.718" type="appl" />
         <tli id="T343" time="170.17" type="appl" />
         <tli id="T344" time="170.622" type="appl" />
         <tli id="T345" time="171.073" type="appl" />
         <tli id="T346" time="171.525" type="appl" />
         <tli id="T347" time="172.049" type="appl" />
         <tli id="T348" time="172.573" type="appl" />
         <tli id="T349" time="173.098" type="appl" />
         <tli id="T350" time="173.622" type="appl" />
         <tli id="T351" time="174.146" type="appl" />
         <tli id="T352" time="174.41897636921982" />
         <tli id="T353" time="175.14" type="appl" />
         <tli id="T354" time="175.61" type="appl" />
         <tli id="T355" time="176.08" type="appl" />
         <tli id="T356" time="176.55" type="appl" />
         <tli id="T357" time="177.02" type="appl" />
         <tli id="T358" time="177.805" type="appl" />
         <tli id="T359" time="178.59" type="appl" />
         <tli id="T360" time="179.375" type="appl" />
         <tli id="T361" time="180.16" type="appl" />
         <tli id="T362" time="180.714" type="appl" />
         <tli id="T363" time="181.269" type="appl" />
         <tli id="T364" time="181.823" type="appl" />
         <tli id="T365" time="182.378" type="appl" />
         <tli id="T366" time="182.932" type="appl" />
         <tli id="T367" time="183.487" type="appl" />
         <tli id="T368" time="184.041" type="appl" />
         <tli id="T369" time="184.596" type="appl" />
         <tli id="T370" time="185.15" type="appl" />
         <tli id="T371" time="185.705" type="appl" />
         <tli id="T372" time="186.259" type="appl" />
         <tli id="T373" time="186.885" type="appl" />
         <tli id="T374" time="187.51" type="appl" />
         <tli id="T375" time="188.136" type="appl" />
         <tli id="T376" time="188.762" type="appl" />
         <tli id="T377" time="189.514" type="appl" />
         <tli id="T378" time="190.266" type="appl" />
         <tli id="T379" time="191.018" type="appl" />
         <tli id="T380" time="191.51220938571947" />
         <tli id="T381" time="191.865" type="appl" />
         <tli id="T382" time="192.37" type="appl" />
         <tli id="T383" time="192.875" type="appl" />
         <tli id="T384" time="193.38" type="appl" />
         <tli id="T385" time="193.877" type="appl" />
         <tli id="T386" time="194.373" type="appl" />
         <tli id="T387" time="194.87" type="appl" />
         <tli id="T388" time="195.367" type="appl" />
         <tli id="T389" time="195.863" type="appl" />
         <tli id="T390" time="196.36" type="appl" />
         <tli id="T391" time="196.761" type="appl" />
         <tli id="T392" time="197.163" type="appl" />
         <tli id="T393" time="197.564" type="appl" />
         <tli id="T394" time="198.066" type="appl" />
         <tli id="T395" time="198.567" type="appl" />
         <tli id="T396" time="199.068" type="appl" />
         <tli id="T397" time="199.57" type="appl" />
         <tli id="T398" time="200.033" type="appl" />
         <tli id="T399" time="200.496" type="appl" />
         <tli id="T400" time="200.959" type="appl" />
         <tli id="T401" time="201.422" type="appl" />
         <tli id="T402" time="201.885" type="appl" />
         <tli id="T403" time="202.522" type="appl" />
         <tli id="T404" time="203.159" type="appl" />
         <tli id="T405" time="203.796" type="appl" />
         <tli id="T406" time="204.432" type="appl" />
         <tli id="T407" time="205.069" type="appl" />
         <tli id="T408" time="205.706" type="appl" />
         <tli id="T409" time="206.343" type="appl" />
         <tli id="T410" time="206.98" type="appl" />
         <tli id="T411" time="207.396" type="appl" />
         <tli id="T412" time="207.812" type="appl" />
         <tli id="T413" time="208.228" type="appl" />
         <tli id="T414" time="208.644" type="appl" />
         <tli id="T415" time="209.06" type="appl" />
         <tli id="T416" time="209.366" type="appl" />
         <tli id="T417" time="209.713" type="appl" />
         <tli id="T418" time="210.048" type="appl" />
         <tli id="T419" time="210.383" type="appl" />
         <tli id="T420" time="210.718" type="appl" />
         <tli id="T421" time="211.053" type="appl" />
         <tli id="T422" time="211.388" type="appl" />
         <tli id="T423" time="211.722" type="appl" />
         <tli id="T424" time="212.057" type="appl" />
         <tli id="T425" time="212.392" type="appl" />
         <tli id="T426" time="212.727" type="appl" />
         <tli id="T427" time="213.062" type="appl" />
         <tli id="T428" time="213.573" type="appl" />
         <tli id="T429" time="214.083" type="appl" />
         <tli id="T430" time="214.594" type="appl" />
         <tli id="T431" time="215.104" type="appl" />
         <tli id="T432" time="215.615" type="appl" />
         <tli id="T433" time="216.125" type="appl" />
         <tli id="T434" time="216.636" type="appl" />
         <tli id="T435" time="217.147" type="appl" />
         <tli id="T436" time="217.657" type="appl" />
         <tli id="T437" time="218.168" type="appl" />
         <tli id="T438" time="218.678" type="appl" />
         <tli id="T439" time="219.189" type="appl" />
         <tli id="T440" time="219.699" type="appl" />
         <tli id="T441" time="220.21" type="appl" />
         <tli id="T442" time="220.681" type="appl" />
         <tli id="T443" time="221.142" type="appl" />
         <tli id="T444" time="221.602" type="appl" />
         <tli id="T445" time="222.063" type="appl" />
         <tli id="T446" time="222.524" type="appl" />
         <tli id="T447" time="223.109" type="appl" />
         <tli id="T448" time="223.695" type="appl" />
         <tli id="T449" time="224.28" type="appl" />
         <tli id="T450" time="225.185" type="appl" />
         <tli id="T451" time="226.09" type="appl" />
         <tli id="T452" time="226.589" type="appl" />
         <tli id="T453" time="227.089" type="appl" />
         <tli id="T454" time="227.588" type="appl" />
         <tli id="T455" time="228.087" type="appl" />
         <tli id="T456" time="228.586" type="appl" />
         <tli id="T457" time="229.086" type="appl" />
         <tli id="T458" time="229.585" type="appl" />
         <tli id="T459" time="230.098" type="appl" />
         <tli id="T460" time="230.611" type="appl" />
         <tli id="T461" time="231.124" type="appl" />
         <tli id="T462" time="231.638" type="appl" />
         <tli id="T463" time="232.151" type="appl" />
         <tli id="T464" time="232.664" type="appl" />
         <tli id="T465" time="233.177" type="appl" />
         <tli id="T466" time="233.491" type="appl" />
         <tli id="T467" time="233.806" type="appl" />
         <tli id="T468" time="234.12" type="appl" />
         <tli id="T469" time="234.434" type="appl" />
         <tli id="T470" time="234.749" type="appl" />
         <tli id="T471" time="235.063" type="appl" />
         <tli id="T472" time="235.377" type="appl" />
         <tli id="T473" time="235.692" type="appl" />
         <tli id="T474" time="236.006" type="appl" />
         <tli id="T475" time="236.32" type="appl" />
         <tli id="T476" time="236.635" type="appl" />
         <tli id="T477" time="236.949" type="appl" />
         <tli id="T478" time="237.493" type="appl" />
         <tli id="T479" time="238.038" type="appl" />
         <tli id="T480" time="238.582" type="appl" />
         <tli id="T481" time="239.127" type="appl" />
         <tli id="T482" time="239.671" type="appl" />
         <tli id="T483" time="240.216" type="appl" />
         <tli id="T484" time="240.76" type="appl" />
         <tli id="T485" time="241.304" type="appl" />
         <tli id="T486" time="241.848" type="appl" />
         <tli id="T487" time="242.393" type="appl" />
         <tli id="T488" time="242.937" type="appl" />
         <tli id="T489" time="243.481" type="appl" />
         <tli id="T490" time="244.096" type="appl" />
         <tli id="T491" time="244.71" type="appl" />
         <tli id="T492" time="245.228" type="appl" />
         <tli id="T493" time="245.746" type="appl" />
         <tli id="T494" time="246.264" type="appl" />
         <tli id="T495" time="246.782" type="appl" />
         <tli id="T496" time="247.15188284738323" />
         <tli id="T497" time="247.795" type="appl" />
         <tli id="T498" time="248.11854384090603" />
         <tli id="T499" time="248.685" type="appl" />
         <tli id="T500" time="249.08" type="appl" />
         <tli id="T501" time="249.475" type="appl" />
         <tli id="T502" time="249.87" type="appl" />
         <tli id="T503" time="250.265" type="appl" />
         <tli id="T504" time="250.66" type="appl" />
         <tli id="T505" time="251.055" type="appl" />
         <tli id="T506" time="251.45" type="appl" />
         <tli id="T507" time="251.845" type="appl" />
         <tli id="T508" time="252.24" type="appl" />
         <tli id="T509" time="252.635" type="appl" />
         <tli id="T510" time="253.03" type="appl" />
         <tli id="T511" time="253.79" type="appl" />
         <tli id="T512" time="254.549" type="appl" />
         <tli id="T513" time="255.071" type="appl" />
         <tli id="T514" time="255.593" type="appl" />
         <tli id="T515" time="256.116" type="appl" />
         <tli id="T516" time="256.638" type="appl" />
         <tli id="T517" time="257.16" type="appl" />
         <tli id="T518" time="257.508" type="appl" />
         <tli id="T519" time="257.855" type="appl" />
         <tli id="T520" time="258.202" type="appl" />
         <tli id="T521" time="258.3584837447061" />
         <tli id="T522" time="258.942" type="appl" />
         <tli id="T523" time="259.335" type="appl" />
         <tli id="T524" time="259.727" type="appl" />
         <tli id="T525" time="260.119" type="appl" />
         <tli id="T526" time="260.511" type="appl" />
         <tli id="T527" time="260.904" type="appl" />
         <tli id="T528" time="261.296" type="appl" />
         <tli id="T529" time="261.688" type="appl" />
         <tli id="T530" time="262.08" type="appl" />
         <tli id="T531" time="262.473" type="appl" />
         <tli id="T532" time="262.865" type="appl" />
         <tli id="T533" time="263.622" type="appl" />
         <tli id="T534" time="264.379" type="appl" />
         <tli id="T535" time="265.136" type="appl" />
         <tli id="T536" time="265.893" type="appl" />
         <tli id="T537" time="266.65" type="appl" />
         <tli id="T538" time="267.208" type="appl" />
         <tli id="T539" time="267.767" type="appl" />
         <tli id="T540" time="268.325" type="appl" />
         <tli id="T541" time="268.793" type="appl" />
         <tli id="T542" time="269.262" type="appl" />
         <tli id="T543" time="269.73" type="appl" />
         <tli id="T544" time="270.198" type="appl" />
         <tli id="T545" time="270.667" type="appl" />
         <tli id="T546" time="271.135" type="appl" />
         <tli id="T547" time="271.803" type="appl" />
         <tli id="T548" time="272.472" type="appl" />
         <tli id="T549" time="273.03839759116954" />
         <tli id="T550" time="273.501" type="appl" />
         <tli id="T551" time="274.112" type="appl" />
         <tli id="T552" time="274.535" type="appl" />
         <tli id="T553" time="274.959" type="appl" />
         <tli id="T554" time="275.382" type="appl" />
         <tli id="T555" time="275.806" type="appl" />
         <tli id="T556" time="276.229" type="appl" />
         <tli id="T557" time="276.652" type="appl" />
         <tli id="T558" time="277.076" type="appl" />
         <tli id="T559" time="277.499" type="appl" />
         <tli id="T560" time="277.923" type="appl" />
         <tli id="T561" time="278.346" type="appl" />
         <tli id="T562" time="278.769" type="appl" />
         <tli id="T563" time="279.193" type="appl" />
         <tli id="T564" time="279.616" type="appl" />
         <tli id="T565" time="280.04" type="appl" />
         <tli id="T566" time="280.463" type="appl" />
         <tli id="T567" time="280.886" type="appl" />
         <tli id="T568" time="281.31" type="appl" />
         <tli id="T569" time="281.733" type="appl" />
         <tli id="T570" time="282.157" type="appl" />
         <tli id="T571" time="282.3316763840714" />
         <tli id="T572" time="282.668" type="appl" />
         <tli id="T573" time="283.147" type="appl" />
         <tli id="T574" time="283.59" type="appl" />
         <tli id="T575" time="284.034" type="appl" />
         <tli id="T576" time="284.477" type="appl" />
         <tli id="T577" time="284.92" type="appl" />
         <tli id="T578" time="285.363" type="appl" />
         <tli id="T579" time="285.807" type="appl" />
         <tli id="T580" time="286.0383212971657" />
         <tli id="T581" time="286.573" type="appl" />
         <tli id="T582" time="286.876" type="appl" />
         <tli id="T583" time="287.18" type="appl" />
         <tli id="T584" time="287.483" type="appl" />
         <tli id="T585" time="287.786" type="appl" />
         <tli id="T586" time="288.361" type="appl" />
         <tli id="T587" time="288.935" type="appl" />
         <tli id="T588" time="289.51" type="appl" />
         <tli id="T589" time="290.085" type="appl" />
         <tli id="T590" time="290.66" type="appl" />
         <tli id="T591" time="291.234" type="appl" />
         <tli id="T592" time="291.809" type="appl" />
         <tli id="T593" time="292.384" type="appl" />
         <tli id="T594" time="292.959" type="appl" />
         <tli id="T595" time="293.533" type="appl" />
         <tli id="T596" time="294.108" type="appl" />
         <tli id="T597" time="294.683" type="appl" />
         <tli id="T598" time="295.258" type="appl" />
         <tli id="T599" time="295.832" type="appl" />
         <tli id="T600" time="296.407" type="appl" />
         <tli id="T601" time="296.982" type="appl" />
         <tli id="T602" time="297.557" type="appl" />
         <tli id="T603" time="298.131" type="appl" />
         <tli id="T604" time="298.706" type="appl" />
         <tli id="T605" time="299.244" type="appl" />
         <tli id="T606" time="299.782" type="appl" />
         <tli id="T607" time="300.32" type="appl" />
         <tli id="T608" time="300.858" type="appl" />
         <tli id="T609" time="301.396" type="appl" />
         <tli id="T610" time="301.934" type="appl" />
         <tli id="T611" time="302.472" type="appl" />
         <tli id="T612" time="303.01" type="appl" />
         <tli id="T613" time="303.421" type="appl" />
         <tli id="T614" time="303.822" type="appl" />
         <tli id="T615" time="304.222" type="appl" />
         <tli id="T616" time="304.623" type="appl" />
         <tli id="T617" time="305.024" type="appl" />
         <tli id="T618" time="305.425" type="appl" />
         <tli id="T619" time="305.825" type="appl" />
         <tli id="T620" time="306.226" type="appl" />
         <tli id="T621" time="306.627" type="appl" />
         <tli id="T622" time="307.045" type="appl" />
         <tli id="T623" time="307.462" type="appl" />
         <tli id="T624" time="307.88" type="appl" />
         <tli id="T625" time="308.297" type="appl" />
         <tli id="T626" time="308.715" type="appl" />
         <tli id="T627" time="309.132" type="appl" />
         <tli id="T628" time="309.35151780991896" />
         <tli id="T629" time="310.011" type="appl" />
         <tli id="T630" time="310.473" type="appl" />
         <tli id="T631" time="310.934" type="appl" />
         <tli id="T632" time="311.395" type="appl" />
         <tli id="T633" time="311.857" type="appl" />
         <tli id="T634" time="312.318" type="appl" />
         <tli id="T635" time="312.624" type="appl" />
         <tli id="T636" time="312.93" type="appl" />
         <tli id="T637" time="313.235" type="appl" />
         <tli id="T638" time="313.541" type="appl" />
         <tli id="T639" time="313.847" type="appl" />
         <tli id="T640" time="314.153" type="appl" />
         <tli id="T641" time="314.458" type="appl" />
         <tli id="T642" time="314.764" type="appl" />
         <tli id="T643" time="314.86481878676966" />
         <tli id="T644" time="315.454" type="appl" />
         <tli id="T645" time="315.867" type="appl" />
         <tli id="T646" time="316.281" type="appl" />
         <tli id="T647" time="316.694" type="appl" />
         <tli id="T648" time="317.108" type="appl" />
         <tli id="T649" time="317.521" type="appl" />
         <tli id="T650" time="317.935" type="appl" />
         <tli id="T651" time="318.348" type="appl" />
         <tli id="T652" time="318.762" type="appl" />
         <tli id="T653" time="319.222" type="appl" />
         <tli id="T654" time="319.681" type="appl" />
         <tli id="T655" time="320.141" type="appl" />
         <tli id="T656" time="320.6" type="appl" />
         <tli id="T657" time="321.06" type="appl" />
         <tli id="T658" time="321.718" type="appl" />
         <tli id="T659" time="322.375" type="appl" />
         <tli id="T660" time="322.84464285714284" type="intp" />
         <tli id="T661" time="323.69" type="appl" />
         <tli id="T662" time="324.098" type="appl" />
         <tli id="T663" time="324.507" type="appl" />
         <tli id="T664" time="324.916" type="appl" />
         <tli id="T665" time="325.324" type="appl" />
         <tli id="T666" time="325.732" type="appl" />
         <tli id="T667" time="326.141" type="appl" />
         <tli id="T668" time="326.55" type="appl" />
         <tli id="T669" time="326.958" type="appl" />
         <tli id="T670" time="327.407" type="appl" />
         <tli id="T671" time="327.857" type="appl" />
         <tli id="T672" time="328.306" type="appl" />
         <tli id="T673" time="328.755" type="appl" />
         <tli id="T674" time="329.204" type="appl" />
         <tli id="T675" time="329.654" type="appl" />
         <tli id="T676" time="330.103" type="appl" />
         <tli id="T677" time="330.552" type="appl" />
         <tli id="T678" time="331.001" type="appl" />
         <tli id="T679" time="331.451" type="appl" />
         <tli id="T680" time="331.9" type="appl" />
         <tli id="T681" time="332.286" type="appl" />
         <tli id="T682" time="332.672" type="appl" />
         <tli id="T683" time="333.059" type="appl" />
         <tli id="T684" time="333.445" type="appl" />
         <tli id="T685" time="333.831" type="appl" />
         <tli id="T686" time="334.217" type="appl" />
         <tli id="T687" time="334.604" type="appl" />
         <tli id="T688" time="334.99" type="appl" />
         <tli id="T689" time="335.376" type="appl" />
         <tli id="T690" time="335.762" type="appl" />
         <tli id="T691" time="336.149" type="appl" />
         <tli id="T692" time="336.535" type="appl" />
         <tli id="T693" time="336.921" type="appl" />
         <tli id="T694" time="337.711" type="appl" />
         <tli id="T695" time="338.5" type="appl" />
         <tli id="T696" time="339.29" type="appl" />
         <tli id="T697" time="340.08" type="appl" />
         <tli id="T698" time="340.498" type="appl" />
         <tli id="T699" time="340.915" type="appl" />
         <tli id="T700" time="341.332" type="appl" />
         <tli id="T701" time="341.5379955804573" />
         <tli id="T702" time="342.079" type="appl" />
         <tli id="T703" time="342.408" type="appl" />
         <tli id="T704" time="342.737" type="appl" />
         <tli id="T705" time="343.066" type="appl" />
         <tli id="T706" time="343.394" type="appl" />
         <tli id="T707" time="343.723" type="appl" />
         <tli id="T708" time="344.052" type="appl" />
         <tli id="T709" time="344.381" type="appl" />
         <tli id="T710" time="344.71" type="appl" />
         <tli id="T711" time="345.166" type="appl" />
         <tli id="T712" time="345.621" type="appl" />
         <tli id="T713" time="346.077" type="appl" />
         <tli id="T714" time="346.532" type="appl" />
         <tli id="T715" time="346.988" type="appl" />
         <tli id="T716" time="347.443" type="appl" />
         <tli id="T717" time="347.899" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KuNS"
                      id="tx-KuNS"
                      speaker="KuNS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KuNS">
            <ts e="T6" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <ts e="T1" id="Seg_5" n="HIAT:w" s="T0">Teːtegit</ts>
                  <nts id="Seg_6" n="HIAT:ip">,</nts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_9" n="HIAT:w" s="T1">inʼegit</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_12" n="HIAT:w" s="T2">kim</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">etej</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">ki͡e</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">oččogo</ts>
                  <nts id="Seg_22" n="HIAT:ip">?</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T89" id="Seg_24" n="sc" s="T87">
               <ts e="T89" id="Seg_26" n="HIAT:u" s="T87">
                  <nts id="Seg_27" n="HIAT:ip">–</nts>
                  <ts e="T88" id="Seg_29" n="HIAT:w" s="T87">Papigaj</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_32" n="HIAT:w" s="T88">tɨ͡atɨgar</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T127" id="Seg_35" n="sc" s="T122">
               <ts e="T127" id="Seg_37" n="HIAT:u" s="T122">
                  <nts id="Seg_38" n="HIAT:ip">–</nts>
                  <ts e="T123" id="Seg_40" n="HIAT:w" s="T122">Brigadagɨt</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_43" n="HIAT:w" s="T123">ke</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_46" n="HIAT:w" s="T124">kannɨk</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_49" n="HIAT:w" s="T125">etej</ts>
                  <nts id="Seg_50" n="HIAT:ip">,</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_53" n="HIAT:w" s="T126">brigadagɨt</ts>
                  <nts id="Seg_54" n="HIAT:ip">?</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_56" n="sc" s="T141">
               <ts e="T147" id="Seg_58" n="HIAT:u" s="T141">
                  <nts id="Seg_59" n="HIAT:ip">–</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_62" n="HIAT:w" s="T141">Ogo</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_65" n="HIAT:w" s="T142">haːskɨtɨttan</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_68" n="HIAT:w" s="T143">iti</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_71" n="HIAT:w" s="T144">brigadaga</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_74" n="HIAT:w" s="T145">üleleːn</ts>
                  <nts id="Seg_75" n="HIAT:ip">,</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_78" n="HIAT:w" s="T146">eː</ts>
                  <nts id="Seg_79" n="HIAT:ip">?</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T166" id="Seg_81" n="sc" s="T151">
               <ts e="T166" id="Seg_83" n="HIAT:u" s="T151">
                  <nts id="Seg_84" n="HIAT:ip">–</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_87" n="HIAT:w" s="T151">Ol</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_90" n="HIAT:w" s="T152">eː</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_93" n="HIAT:w" s="T153">haːstarga</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_96" n="HIAT:w" s="T154">oččogo</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_99" n="HIAT:w" s="T155">atɨn</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_102" n="HIAT:w" s="T156">kihiler</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_105" n="HIAT:w" s="T157">eː</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_109" n="HIAT:w" s="T158">ehiginneːger</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_112" n="HIAT:w" s="T159">kɨrdʼagas</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_115" n="HIAT:w" s="T160">kihiler</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_118" n="HIAT:w" s="T161">bastɨːr</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_121" n="HIAT:w" s="T162">etilere</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_124" n="HIAT:w" s="T163">bu͡o</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_127" n="HIAT:w" s="T164">olokkutun</ts>
                  <nts id="Seg_128" n="HIAT:ip">,</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_131" n="HIAT:w" s="T165">brigaːdagɨtɨn</ts>
                  <nts id="Seg_132" n="HIAT:ip">?</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_134" n="sc" s="T243">
               <ts e="T256" id="Seg_136" n="HIAT:u" s="T243">
                  <nts id="Seg_137" n="HIAT:ip">–</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_140" n="HIAT:w" s="T243">Sʼidar</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_143" n="HIAT:w" s="T244">Alʼeksʼejevʼičʼ</ts>
                  <nts id="Seg_144" n="HIAT:ip">,</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_147" n="HIAT:w" s="T245">ol</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_150" n="HIAT:w" s="T246">u͡on</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_153" n="HIAT:w" s="T247">agɨs</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_156" n="HIAT:w" s="T248">tɨːhɨčča</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_159" n="HIAT:w" s="T249">kuraŋa</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_162" n="HIAT:w" s="T250">taba</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_165" n="HIAT:w" s="T251">ol</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_168" n="HIAT:w" s="T252">kajdi͡et</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_170" n="HIAT:ip">"</nts>
                  <ts e="T254" id="Seg_172" n="HIAT:w" s="T253">ehilinne</ts>
                  <nts id="Seg_173" n="HIAT:ip">"</nts>
                  <nts id="Seg_174" n="HIAT:ip">,</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_177" n="HIAT:w" s="T254">kajdi͡ek</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_180" n="HIAT:w" s="T255">barbɨtaj</ts>
                  <nts id="Seg_181" n="HIAT:ip">?</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T301" id="Seg_183" n="sc" s="T288">
               <ts e="T296" id="Seg_185" n="HIAT:u" s="T288">
                  <nts id="Seg_186" n="HIAT:ip">–</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_189" n="HIAT:w" s="T288">Urut</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_192" n="HIAT:w" s="T289">iti</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_195" n="HIAT:w" s="T290">börönü</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_198" n="HIAT:w" s="T291">itte</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_201" n="HIAT:w" s="T292">ölörtöröːččü</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_204" n="HIAT:w" s="T293">etilere</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_207" n="HIAT:w" s="T294">bu͡o</ts>
                  <nts id="Seg_208" n="HIAT:ip">,</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_211" n="HIAT:w" s="T295">i͡e</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_215" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_217" n="HIAT:w" s="T296">Anɨ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_220" n="HIAT:w" s="T297">onnuk</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_223" n="HIAT:w" s="T298">da</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_226" n="HIAT:w" s="T299">hu͡ok</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_229" n="HIAT:w" s="T300">bu͡olbut</ts>
                  <nts id="Seg_230" n="HIAT:ip">?</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T319" id="Seg_232" n="sc" s="T316">
               <ts e="T319" id="Seg_234" n="HIAT:u" s="T316">
                  <nts id="Seg_235" n="HIAT:ip">–</nts>
                  <ts e="T317" id="Seg_237" n="HIAT:w" s="T316">Taːk</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_240" n="HIAT:w" s="T317">daː</ts>
                  <nts id="Seg_241" n="HIAT:ip">…</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T352" id="Seg_243" n="sc" s="T346">
               <ts e="T352" id="Seg_245" n="HIAT:u" s="T346">
                  <nts id="Seg_246" n="HIAT:ip">–</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_248" n="HIAT:ip">"</nts>
                  <ts e="T347" id="Seg_250" n="HIAT:w" s="T346">Tabahɨttarga</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_253" n="HIAT:w" s="T347">ügüs</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_256" n="HIAT:w" s="T348">karčɨnɨ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_259" n="HIAT:w" s="T349">tölöːŋ</ts>
                  <nts id="Seg_260" n="HIAT:ip">"</nts>
                  <nts id="Seg_261" n="HIAT:ip">,</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_264" n="HIAT:w" s="T350">diːgit</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_267" n="HIAT:w" s="T351">bu͡o</ts>
                  <nts id="Seg_268" n="HIAT:ip">.</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T380" id="Seg_270" n="sc" s="T376">
               <ts e="T380" id="Seg_272" n="HIAT:u" s="T376">
                  <nts id="Seg_273" n="HIAT:ip">–</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_276" n="HIAT:w" s="T376">Iːtellerin</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_279" n="HIAT:w" s="T377">ihin</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_282" n="HIAT:w" s="T378">duː</ts>
                  <nts id="Seg_283" n="HIAT:ip">,</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_286" n="HIAT:w" s="T379">ölördöktörünen</ts>
                  <nts id="Seg_287" n="HIAT:ip">?</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T417" id="Seg_289" n="sc" s="T415">
               <ts e="T417" id="Seg_291" n="HIAT:u" s="T415">
                  <nts id="Seg_292" n="HIAT:ip">–</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_295" n="HIAT:w" s="T415">Dʼe</ts>
                  <nts id="Seg_296" n="HIAT:ip">,</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_299" n="HIAT:w" s="T416">ile</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T441" id="Seg_302" n="sc" s="T427">
               <ts e="T441" id="Seg_304" n="HIAT:u" s="T427">
                  <nts id="Seg_305" n="HIAT:ip">–</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_308" n="HIAT:w" s="T427">Onno</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_311" n="HIAT:w" s="T428">bagas</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_314" n="HIAT:w" s="T429">rʼespublʼika</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_317" n="HIAT:w" s="T430">eː</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_320" n="HIAT:w" s="T431">dʼeputaːttara</ts>
                  <nts id="Seg_321" n="HIAT:ip">,</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_324" n="HIAT:w" s="T432">tuspa</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_327" n="HIAT:w" s="T433">itte</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_330" n="HIAT:w" s="T434">zakonnarɨ</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_333" n="HIAT:w" s="T435">tahaːrannar</ts>
                  <nts id="Seg_334" n="HIAT:ip">,</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_337" n="HIAT:w" s="T436">kömüsküːller</ts>
                  <nts id="Seg_338" n="HIAT:ip">,</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_341" n="HIAT:w" s="T437">bɨhɨlak</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_345" n="HIAT:w" s="T438">tabahɨt</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_348" n="HIAT:w" s="T439">ülehitin</ts>
                  <nts id="Seg_349" n="HIAT:ip">,</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_352" n="HIAT:w" s="T440">eː</ts>
                  <nts id="Seg_353" n="HIAT:ip">?</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T549" id="Seg_355" n="sc" s="T546">
               <ts e="T549" id="Seg_357" n="HIAT:u" s="T546">
                  <nts id="Seg_358" n="HIAT:ip">–</nts>
                  <ts e="T547" id="Seg_360" n="HIAT:w" s="T546">Karčɨ</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_363" n="HIAT:w" s="T547">oŋostollor</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_366" n="HIAT:w" s="T548">bu͡o</ts>
                  <nts id="Seg_367" n="HIAT:ip">?</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T571" id="Seg_369" n="sc" s="T551">
               <ts e="T571" id="Seg_371" n="HIAT:u" s="T551">
                  <nts id="Seg_372" n="HIAT:ip">–</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_375" n="HIAT:w" s="T551">Anɨ</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_378" n="HIAT:w" s="T552">e</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_381" n="HIAT:w" s="T553">haŋa</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_384" n="HIAT:w" s="T554">üjege</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_386" n="HIAT:ip">"</nts>
                  <ts e="T556" id="Seg_388" n="HIAT:w" s="T555">iti</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_391" n="HIAT:w" s="T556">rɨnak</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_394" n="HIAT:w" s="T557">tuhunan</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_397" n="HIAT:w" s="T558">üleleːŋ</ts>
                  <nts id="Seg_398" n="HIAT:ip">"</nts>
                  <nts id="Seg_399" n="HIAT:ip">,</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_402" n="HIAT:w" s="T559">diːller</ts>
                  <nts id="Seg_403" n="HIAT:ip">,</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_406" n="HIAT:w" s="T560">ontularɨŋ</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_409" n="HIAT:w" s="T561">olokko</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_412" n="HIAT:w" s="T562">tuksubat</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_415" n="HIAT:w" s="T563">bu͡o</ts>
                  <nts id="Seg_416" n="HIAT:ip">,</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_419" n="HIAT:w" s="T564">onu</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_422" n="HIAT:w" s="T565">tuksara</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_425" n="HIAT:w" s="T566">hataːnar</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_428" n="HIAT:w" s="T567">eŋin-eŋinnik</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_430" n="HIAT:ip">(</nts>
                  <ts e="T569" id="Seg_432" n="HIAT:w" s="T568">ki</ts>
                  <nts id="Seg_433" n="HIAT:ip">)</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_436" n="HIAT:w" s="T569">hübeliːller</ts>
                  <nts id="Seg_437" n="HIAT:ip">,</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_440" n="HIAT:w" s="T570">eː</ts>
                  <nts id="Seg_441" n="HIAT:ip">?</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T580" id="Seg_443" n="sc" s="T573">
               <ts e="T580" id="Seg_445" n="HIAT:u" s="T573">
                  <nts id="Seg_446" n="HIAT:ip">–</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_449" n="HIAT:w" s="T573">Anɨ</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_452" n="HIAT:w" s="T574">ki͡e</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_455" n="HIAT:w" s="T575">hi͡ese</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_458" n="HIAT:w" s="T576">tabalɨːr</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_461" n="HIAT:w" s="T577">kihiler</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_464" n="HIAT:w" s="T578">kaːlallar</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_467" n="HIAT:w" s="T579">hi͡ese</ts>
                  <nts id="Seg_468" n="HIAT:ip">?</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T612" id="Seg_470" n="sc" s="T585">
               <ts e="T604" id="Seg_472" n="HIAT:u" s="T585">
                  <nts id="Seg_473" n="HIAT:ip">–</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_476" n="HIAT:w" s="T585">Sʼidar</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_479" n="HIAT:w" s="T586">Alʼeksʼejevʼičʼ</ts>
                  <nts id="Seg_480" n="HIAT:ip">,</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_483" n="HIAT:w" s="T587">urukku</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_486" n="HIAT:w" s="T588">dʼɨllarga</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_489" n="HIAT:w" s="T589">iti</ts>
                  <nts id="Seg_490" n="HIAT:ip">,</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_493" n="HIAT:w" s="T590">agɨhu͡onnaːk</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_496" n="HIAT:w" s="T591">itte</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_499" n="HIAT:w" s="T592">dʼɨllarga</ts>
                  <nts id="Seg_500" n="HIAT:ip">,</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_503" n="HIAT:w" s="T593">bɨhɨlaːk</ts>
                  <nts id="Seg_504" n="HIAT:ip">,</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_507" n="HIAT:w" s="T594">ehigini</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_510" n="HIAT:w" s="T595">iti</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_513" n="HIAT:w" s="T596">dʼeputat</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_516" n="HIAT:w" s="T597">itte</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_519" n="HIAT:w" s="T598">Vʼerxoːvnava</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_522" n="HIAT:w" s="T599">Savʼeta</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_525" n="HIAT:w" s="T600">di͡en</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_528" n="HIAT:w" s="T601">talallarɨgar</ts>
                  <nts id="Seg_529" n="HIAT:ip">,</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_532" n="HIAT:w" s="T602">oŋorollorugar</ts>
                  <nts id="Seg_533" n="HIAT:ip">…</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_536" n="HIAT:u" s="T604">
                  <ts e="T605" id="Seg_538" n="HIAT:w" s="T604">Kim</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_541" n="HIAT:w" s="T605">eː</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_544" n="HIAT:w" s="T606">hanaːtɨnan</ts>
                  <nts id="Seg_545" n="HIAT:ip">,</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_548" n="HIAT:w" s="T607">kajdak</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_551" n="HIAT:w" s="T608">dʼeputat</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_554" n="HIAT:w" s="T609">di͡en</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_557" n="HIAT:w" s="T610">taksɨbɨt</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_560" n="HIAT:w" s="T611">etigitij</ts>
                  <nts id="Seg_561" n="HIAT:ip">?</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T652" id="Seg_563" n="sc" s="T643">
               <ts e="T652" id="Seg_565" n="HIAT:u" s="T643">
                  <nts id="Seg_566" n="HIAT:ip">–</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_569" n="HIAT:w" s="T643">Iti</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_572" n="HIAT:w" s="T644">rajon</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_575" n="HIAT:w" s="T645">tojonnoro</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_578" n="HIAT:w" s="T646">körönnör</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_582" n="HIAT:w" s="T647">eni</ts>
                  <nts id="Seg_583" n="HIAT:ip">,</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_586" n="HIAT:w" s="T648">ehigi</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_589" n="HIAT:w" s="T649">iti</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_592" n="HIAT:w" s="T650">ülegitin</ts>
                  <nts id="Seg_593" n="HIAT:ip">,</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_596" n="HIAT:w" s="T651">eː</ts>
                  <nts id="Seg_597" n="HIAT:ip">?</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T693" id="Seg_599" n="sc" s="T669">
               <ts e="T680" id="Seg_601" n="HIAT:u" s="T669">
                  <nts id="Seg_602" n="HIAT:ip">–</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_605" n="HIAT:w" s="T669">Oččogo</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_608" n="HIAT:w" s="T670">tabalɨːr</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_611" n="HIAT:w" s="T671">kihiler</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_614" n="HIAT:w" s="T672">iti</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_617" n="HIAT:w" s="T673">oččugutun</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_620" n="HIAT:w" s="T674">hurujuŋ</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_623" n="HIAT:w" s="T675">di͡en</ts>
                  <nts id="Seg_624" n="HIAT:ip">,</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_627" n="HIAT:w" s="T676">itte</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_630" n="HIAT:w" s="T677">kördöhöːččü</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_633" n="HIAT:w" s="T678">etiler</ts>
                  <nts id="Seg_634" n="HIAT:ip">,</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_637" n="HIAT:w" s="T679">eː</ts>
                  <nts id="Seg_638" n="HIAT:ip">?</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T693" id="Seg_641" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_643" n="HIAT:w" s="T680">Oččot</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_646" n="HIAT:w" s="T681">itte</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_649" n="HIAT:w" s="T682">hurunaːččɨ</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_652" n="HIAT:w" s="T683">etigit</ts>
                  <nts id="Seg_653" n="HIAT:ip">,</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_656" n="HIAT:w" s="T684">barɨtɨn</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_659" n="HIAT:w" s="T685">kepseːčči</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_662" n="HIAT:w" s="T686">etigit</ts>
                  <nts id="Seg_663" n="HIAT:ip">,</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_666" n="HIAT:w" s="T687">kas</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_669" n="HIAT:w" s="T688">taba</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_672" n="HIAT:w" s="T689">töröːtö</ts>
                  <nts id="Seg_673" n="HIAT:ip">,</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_676" n="HIAT:w" s="T690">kas</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_679" n="HIAT:w" s="T691">ɨ͡arɨjda</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_682" n="HIAT:w" s="T692">di͡en</ts>
                  <nts id="Seg_683" n="HIAT:ip">.</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KuNS">
            <ts e="T6" id="Seg_685" n="sc" s="T0">
               <ts e="T1" id="Seg_687" n="e" s="T0">–Teːtegit, </ts>
               <ts e="T2" id="Seg_689" n="e" s="T1">inʼegit </ts>
               <ts e="T3" id="Seg_691" n="e" s="T2">kim </ts>
               <ts e="T4" id="Seg_693" n="e" s="T3">etej </ts>
               <ts e="T5" id="Seg_695" n="e" s="T4">ki͡e </ts>
               <ts e="T6" id="Seg_697" n="e" s="T5">oččogo? </ts>
            </ts>
            <ts e="T89" id="Seg_698" n="sc" s="T87">
               <ts e="T88" id="Seg_700" n="e" s="T87">–Papigaj </ts>
               <ts e="T89" id="Seg_702" n="e" s="T88">tɨ͡atɨgar. </ts>
            </ts>
            <ts e="T127" id="Seg_703" n="sc" s="T122">
               <ts e="T123" id="Seg_705" n="e" s="T122">–Brigadagɨt </ts>
               <ts e="T124" id="Seg_707" n="e" s="T123">ke </ts>
               <ts e="T125" id="Seg_709" n="e" s="T124">kannɨk </ts>
               <ts e="T126" id="Seg_711" n="e" s="T125">etej, </ts>
               <ts e="T127" id="Seg_713" n="e" s="T126">brigadagɨt? </ts>
            </ts>
            <ts e="T147" id="Seg_714" n="sc" s="T141">
               <ts e="T142" id="Seg_716" n="e" s="T141">– Ogo </ts>
               <ts e="T143" id="Seg_718" n="e" s="T142">haːskɨtɨttan </ts>
               <ts e="T144" id="Seg_720" n="e" s="T143">iti </ts>
               <ts e="T145" id="Seg_722" n="e" s="T144">brigadaga </ts>
               <ts e="T146" id="Seg_724" n="e" s="T145">üleleːn, </ts>
               <ts e="T147" id="Seg_726" n="e" s="T146">eː? </ts>
            </ts>
            <ts e="T166" id="Seg_727" n="sc" s="T151">
               <ts e="T152" id="Seg_729" n="e" s="T151">– Ol </ts>
               <ts e="T153" id="Seg_731" n="e" s="T152">eː </ts>
               <ts e="T154" id="Seg_733" n="e" s="T153">haːstarga </ts>
               <ts e="T155" id="Seg_735" n="e" s="T154">oččogo </ts>
               <ts e="T156" id="Seg_737" n="e" s="T155">atɨn </ts>
               <ts e="T157" id="Seg_739" n="e" s="T156">kihiler </ts>
               <ts e="T158" id="Seg_741" n="e" s="T157">eː, </ts>
               <ts e="T159" id="Seg_743" n="e" s="T158">ehiginneːger </ts>
               <ts e="T160" id="Seg_745" n="e" s="T159">kɨrdʼagas </ts>
               <ts e="T161" id="Seg_747" n="e" s="T160">kihiler </ts>
               <ts e="T162" id="Seg_749" n="e" s="T161">bastɨːr </ts>
               <ts e="T163" id="Seg_751" n="e" s="T162">etilere </ts>
               <ts e="T164" id="Seg_753" n="e" s="T163">bu͡o </ts>
               <ts e="T165" id="Seg_755" n="e" s="T164">olokkutun, </ts>
               <ts e="T166" id="Seg_757" n="e" s="T165">brigaːdagɨtɨn? </ts>
            </ts>
            <ts e="T256" id="Seg_758" n="sc" s="T243">
               <ts e="T244" id="Seg_760" n="e" s="T243">– Sʼidar </ts>
               <ts e="T245" id="Seg_762" n="e" s="T244">Alʼeksʼejevʼičʼ, </ts>
               <ts e="T246" id="Seg_764" n="e" s="T245">ol </ts>
               <ts e="T247" id="Seg_766" n="e" s="T246">u͡on </ts>
               <ts e="T248" id="Seg_768" n="e" s="T247">agɨs </ts>
               <ts e="T249" id="Seg_770" n="e" s="T248">tɨːhɨčča </ts>
               <ts e="T250" id="Seg_772" n="e" s="T249">kuraŋa </ts>
               <ts e="T251" id="Seg_774" n="e" s="T250">taba </ts>
               <ts e="T252" id="Seg_776" n="e" s="T251">ol </ts>
               <ts e="T253" id="Seg_778" n="e" s="T252">kajdi͡et </ts>
               <ts e="T254" id="Seg_780" n="e" s="T253">"ehilinne", </ts>
               <ts e="T255" id="Seg_782" n="e" s="T254">kajdi͡ek </ts>
               <ts e="T256" id="Seg_784" n="e" s="T255">barbɨtaj? </ts>
            </ts>
            <ts e="T301" id="Seg_785" n="sc" s="T288">
               <ts e="T289" id="Seg_787" n="e" s="T288">– Urut </ts>
               <ts e="T290" id="Seg_789" n="e" s="T289">iti </ts>
               <ts e="T291" id="Seg_791" n="e" s="T290">börönü </ts>
               <ts e="T292" id="Seg_793" n="e" s="T291">itte </ts>
               <ts e="T293" id="Seg_795" n="e" s="T292">ölörtöröːččü </ts>
               <ts e="T294" id="Seg_797" n="e" s="T293">etilere </ts>
               <ts e="T295" id="Seg_799" n="e" s="T294">bu͡o, </ts>
               <ts e="T296" id="Seg_801" n="e" s="T295">i͡e. </ts>
               <ts e="T297" id="Seg_803" n="e" s="T296">Anɨ </ts>
               <ts e="T298" id="Seg_805" n="e" s="T297">onnuk </ts>
               <ts e="T299" id="Seg_807" n="e" s="T298">da </ts>
               <ts e="T300" id="Seg_809" n="e" s="T299">hu͡ok </ts>
               <ts e="T301" id="Seg_811" n="e" s="T300">bu͡olbut? </ts>
            </ts>
            <ts e="T319" id="Seg_812" n="sc" s="T316">
               <ts e="T317" id="Seg_814" n="e" s="T316">–Taːk </ts>
               <ts e="T319" id="Seg_816" n="e" s="T317">daː… </ts>
            </ts>
            <ts e="T352" id="Seg_817" n="sc" s="T346">
               <ts e="T347" id="Seg_819" n="e" s="T346">– "Tabahɨttarga </ts>
               <ts e="T348" id="Seg_821" n="e" s="T347">ügüs </ts>
               <ts e="T349" id="Seg_823" n="e" s="T348">karčɨnɨ </ts>
               <ts e="T350" id="Seg_825" n="e" s="T349">tölöːŋ", </ts>
               <ts e="T351" id="Seg_827" n="e" s="T350">diːgit </ts>
               <ts e="T352" id="Seg_829" n="e" s="T351">bu͡o. </ts>
            </ts>
            <ts e="T380" id="Seg_830" n="sc" s="T376">
               <ts e="T377" id="Seg_832" n="e" s="T376">– Iːtellerin </ts>
               <ts e="T378" id="Seg_834" n="e" s="T377">ihin </ts>
               <ts e="T379" id="Seg_836" n="e" s="T378">duː, </ts>
               <ts e="T380" id="Seg_838" n="e" s="T379">ölördöktörünen? </ts>
            </ts>
            <ts e="T417" id="Seg_839" n="sc" s="T415">
               <ts e="T416" id="Seg_841" n="e" s="T415">– Dʼe, </ts>
               <ts e="T417" id="Seg_843" n="e" s="T416">ile. </ts>
            </ts>
            <ts e="T441" id="Seg_844" n="sc" s="T427">
               <ts e="T428" id="Seg_846" n="e" s="T427">– Onno </ts>
               <ts e="T429" id="Seg_848" n="e" s="T428">bagas </ts>
               <ts e="T430" id="Seg_850" n="e" s="T429">rʼespublʼika </ts>
               <ts e="T431" id="Seg_852" n="e" s="T430">eː </ts>
               <ts e="T432" id="Seg_854" n="e" s="T431">dʼeputaːttara, </ts>
               <ts e="T433" id="Seg_856" n="e" s="T432">tuspa </ts>
               <ts e="T434" id="Seg_858" n="e" s="T433">itte </ts>
               <ts e="T435" id="Seg_860" n="e" s="T434">zakonnarɨ </ts>
               <ts e="T436" id="Seg_862" n="e" s="T435">tahaːrannar, </ts>
               <ts e="T437" id="Seg_864" n="e" s="T436">kömüsküːller, </ts>
               <ts e="T438" id="Seg_866" n="e" s="T437">bɨhɨlak, </ts>
               <ts e="T439" id="Seg_868" n="e" s="T438">tabahɨt </ts>
               <ts e="T440" id="Seg_870" n="e" s="T439">ülehitin, </ts>
               <ts e="T441" id="Seg_872" n="e" s="T440">eː? </ts>
            </ts>
            <ts e="T549" id="Seg_873" n="sc" s="T546">
               <ts e="T547" id="Seg_875" n="e" s="T546">–Karčɨ </ts>
               <ts e="T548" id="Seg_877" n="e" s="T547">oŋostollor </ts>
               <ts e="T549" id="Seg_879" n="e" s="T548">bu͡o? </ts>
            </ts>
            <ts e="T571" id="Seg_880" n="sc" s="T551">
               <ts e="T552" id="Seg_882" n="e" s="T551">– Anɨ </ts>
               <ts e="T553" id="Seg_884" n="e" s="T552">e </ts>
               <ts e="T554" id="Seg_886" n="e" s="T553">haŋa </ts>
               <ts e="T555" id="Seg_888" n="e" s="T554">üjege </ts>
               <ts e="T556" id="Seg_890" n="e" s="T555">"iti </ts>
               <ts e="T557" id="Seg_892" n="e" s="T556">rɨnak </ts>
               <ts e="T558" id="Seg_894" n="e" s="T557">tuhunan </ts>
               <ts e="T559" id="Seg_896" n="e" s="T558">üleleːŋ", </ts>
               <ts e="T560" id="Seg_898" n="e" s="T559">diːller, </ts>
               <ts e="T561" id="Seg_900" n="e" s="T560">ontularɨŋ </ts>
               <ts e="T562" id="Seg_902" n="e" s="T561">olokko </ts>
               <ts e="T563" id="Seg_904" n="e" s="T562">tuksubat </ts>
               <ts e="T564" id="Seg_906" n="e" s="T563">bu͡o, </ts>
               <ts e="T565" id="Seg_908" n="e" s="T564">onu </ts>
               <ts e="T566" id="Seg_910" n="e" s="T565">tuksara </ts>
               <ts e="T567" id="Seg_912" n="e" s="T566">hataːnar </ts>
               <ts e="T568" id="Seg_914" n="e" s="T567">eŋin-eŋinnik </ts>
               <ts e="T569" id="Seg_916" n="e" s="T568">(ki) </ts>
               <ts e="T570" id="Seg_918" n="e" s="T569">hübeliːller, </ts>
               <ts e="T571" id="Seg_920" n="e" s="T570">eː? </ts>
            </ts>
            <ts e="T580" id="Seg_921" n="sc" s="T573">
               <ts e="T574" id="Seg_923" n="e" s="T573">– Anɨ </ts>
               <ts e="T575" id="Seg_925" n="e" s="T574">ki͡e </ts>
               <ts e="T576" id="Seg_927" n="e" s="T575">hi͡ese </ts>
               <ts e="T577" id="Seg_929" n="e" s="T576">tabalɨːr </ts>
               <ts e="T578" id="Seg_931" n="e" s="T577">kihiler </ts>
               <ts e="T579" id="Seg_933" n="e" s="T578">kaːlallar </ts>
               <ts e="T580" id="Seg_935" n="e" s="T579">hi͡ese? </ts>
            </ts>
            <ts e="T612" id="Seg_936" n="sc" s="T585">
               <ts e="T586" id="Seg_938" n="e" s="T585">– Sʼidar </ts>
               <ts e="T587" id="Seg_940" n="e" s="T586">Alʼeksʼejevʼičʼ, </ts>
               <ts e="T588" id="Seg_942" n="e" s="T587">urukku </ts>
               <ts e="T589" id="Seg_944" n="e" s="T588">dʼɨllarga </ts>
               <ts e="T590" id="Seg_946" n="e" s="T589">iti, </ts>
               <ts e="T591" id="Seg_948" n="e" s="T590">agɨhu͡onnaːk </ts>
               <ts e="T592" id="Seg_950" n="e" s="T591">itte </ts>
               <ts e="T593" id="Seg_952" n="e" s="T592">dʼɨllarga, </ts>
               <ts e="T594" id="Seg_954" n="e" s="T593">bɨhɨlaːk, </ts>
               <ts e="T595" id="Seg_956" n="e" s="T594">ehigini </ts>
               <ts e="T596" id="Seg_958" n="e" s="T595">iti </ts>
               <ts e="T597" id="Seg_960" n="e" s="T596">dʼeputat </ts>
               <ts e="T598" id="Seg_962" n="e" s="T597">itte </ts>
               <ts e="T599" id="Seg_964" n="e" s="T598">Vʼerxoːvnava </ts>
               <ts e="T600" id="Seg_966" n="e" s="T599">Savʼeta </ts>
               <ts e="T601" id="Seg_968" n="e" s="T600">di͡en </ts>
               <ts e="T602" id="Seg_970" n="e" s="T601">talallarɨgar, </ts>
               <ts e="T604" id="Seg_972" n="e" s="T602">oŋorollorugar… </ts>
               <ts e="T605" id="Seg_974" n="e" s="T604">Kim </ts>
               <ts e="T606" id="Seg_976" n="e" s="T605">eː </ts>
               <ts e="T607" id="Seg_978" n="e" s="T606">hanaːtɨnan, </ts>
               <ts e="T608" id="Seg_980" n="e" s="T607">kajdak </ts>
               <ts e="T609" id="Seg_982" n="e" s="T608">dʼeputat </ts>
               <ts e="T610" id="Seg_984" n="e" s="T609">di͡en </ts>
               <ts e="T611" id="Seg_986" n="e" s="T610">taksɨbɨt </ts>
               <ts e="T612" id="Seg_988" n="e" s="T611">etigitij? </ts>
            </ts>
            <ts e="T652" id="Seg_989" n="sc" s="T643">
               <ts e="T644" id="Seg_991" n="e" s="T643">– Iti </ts>
               <ts e="T645" id="Seg_993" n="e" s="T644">rajon </ts>
               <ts e="T646" id="Seg_995" n="e" s="T645">tojonnoro </ts>
               <ts e="T647" id="Seg_997" n="e" s="T646">körönnör, </ts>
               <ts e="T648" id="Seg_999" n="e" s="T647">eni, </ts>
               <ts e="T649" id="Seg_1001" n="e" s="T648">ehigi </ts>
               <ts e="T650" id="Seg_1003" n="e" s="T649">iti </ts>
               <ts e="T651" id="Seg_1005" n="e" s="T650">ülegitin, </ts>
               <ts e="T652" id="Seg_1007" n="e" s="T651">eː? </ts>
            </ts>
            <ts e="T693" id="Seg_1008" n="sc" s="T669">
               <ts e="T670" id="Seg_1010" n="e" s="T669">– Oččogo </ts>
               <ts e="T671" id="Seg_1012" n="e" s="T670">tabalɨːr </ts>
               <ts e="T672" id="Seg_1014" n="e" s="T671">kihiler </ts>
               <ts e="T673" id="Seg_1016" n="e" s="T672">iti </ts>
               <ts e="T674" id="Seg_1018" n="e" s="T673">oččugutun </ts>
               <ts e="T675" id="Seg_1020" n="e" s="T674">hurujuŋ </ts>
               <ts e="T676" id="Seg_1022" n="e" s="T675">di͡en, </ts>
               <ts e="T677" id="Seg_1024" n="e" s="T676">itte </ts>
               <ts e="T678" id="Seg_1026" n="e" s="T677">kördöhöːččü </ts>
               <ts e="T679" id="Seg_1028" n="e" s="T678">etiler, </ts>
               <ts e="T680" id="Seg_1030" n="e" s="T679">eː? </ts>
               <ts e="T681" id="Seg_1032" n="e" s="T680">Oččot </ts>
               <ts e="T682" id="Seg_1034" n="e" s="T681">itte </ts>
               <ts e="T683" id="Seg_1036" n="e" s="T682">hurunaːččɨ </ts>
               <ts e="T684" id="Seg_1038" n="e" s="T683">etigit, </ts>
               <ts e="T685" id="Seg_1040" n="e" s="T684">barɨtɨn </ts>
               <ts e="T686" id="Seg_1042" n="e" s="T685">kepseːčči </ts>
               <ts e="T687" id="Seg_1044" n="e" s="T686">etigit, </ts>
               <ts e="T688" id="Seg_1046" n="e" s="T687">kas </ts>
               <ts e="T689" id="Seg_1048" n="e" s="T688">taba </ts>
               <ts e="T690" id="Seg_1050" n="e" s="T689">töröːtö, </ts>
               <ts e="T691" id="Seg_1052" n="e" s="T690">kas </ts>
               <ts e="T692" id="Seg_1054" n="e" s="T691">ɨ͡arɨjda </ts>
               <ts e="T693" id="Seg_1056" n="e" s="T692">di͡en. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KuNS">
            <ta e="T6" id="Seg_1057" s="T0">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.001 (001.001)</ta>
            <ta e="T89" id="Seg_1058" s="T87">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.002 (001.017)</ta>
            <ta e="T127" id="Seg_1059" s="T122">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.003 (001.023)</ta>
            <ta e="T147" id="Seg_1060" s="T141">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.004 (001.028)</ta>
            <ta e="T166" id="Seg_1061" s="T151">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.005 (001.030)</ta>
            <ta e="T256" id="Seg_1062" s="T243">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.006 (001.041)</ta>
            <ta e="T296" id="Seg_1063" s="T288">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.007 (001.048)</ta>
            <ta e="T301" id="Seg_1064" s="T296">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.008 (001.049)</ta>
            <ta e="T319" id="Seg_1065" s="T316">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.009 (001.053)</ta>
            <ta e="T352" id="Seg_1066" s="T346">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.010 (001.056)</ta>
            <ta e="T380" id="Seg_1067" s="T376">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.011 (001.061)</ta>
            <ta e="T417" id="Seg_1068" s="T415">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.012 (001.069)</ta>
            <ta e="T441" id="Seg_1069" s="T427">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.013 (001.071)</ta>
            <ta e="T549" id="Seg_1070" s="T546">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.014 (001.091)</ta>
            <ta e="T571" id="Seg_1071" s="T551">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.015 (001.093)</ta>
            <ta e="T580" id="Seg_1072" s="T573">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.016 (001.095)</ta>
            <ta e="T604" id="Seg_1073" s="T585">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.017 (001.097)</ta>
            <ta e="T612" id="Seg_1074" s="T604">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.018 (001.098)</ta>
            <ta e="T652" id="Seg_1075" s="T643">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.019 (001.103)</ta>
            <ta e="T680" id="Seg_1076" s="T669">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.020 (001.107)</ta>
            <ta e="T693" id="Seg_1077" s="T680">ChSA_KuNS_2004_ReindeerHerding_conv.KuNS.021 (001.108)</ta>
         </annotation>
         <annotation name="st" tierref="st-KuNS">
            <ta e="T6" id="Seg_1078" s="T0">К: Тээтэгит, иньэгит ким этэй киэ оччого?</ta>
            <ta e="T89" id="Seg_1079" s="T87">К: Попигай тыатыгар.</ta>
            <ta e="T127" id="Seg_1080" s="T122">К: Бригадагыт киэ каннык этэй, бригадагыт?</ta>
            <ta e="T147" id="Seg_1081" s="T141">К: Ого һааскытыттан ити бригадага үлэлээн, э-э?</ta>
            <ta e="T166" id="Seg_1082" s="T151">К: Ол ээ һаастарга оччого атын киһилэр ээ, эһигиннээгэр кырдьагас киһилэр бастыыр этилэрэ буо олоккутун, бригаадагытын?</ta>
            <ta e="T256" id="Seg_1083" s="T243">К: Сидор Алексеевич, ол уон агыс тыыһычча кураӈа таба ол кайдиэт (кайдиэк) "эһиллиннэ", кайдиэк барбытай?</ta>
            <ta e="T296" id="Seg_1084" s="T288">К: Урут ити бөрөнү иттэ өлөртөрөөччи этилэрэ буо, ээ.</ta>
            <ta e="T301" id="Seg_1085" s="T296">Аны оннук да һуок буолбут?</ta>
            <ta e="T319" id="Seg_1086" s="T316">К: Таак даа…</ta>
            <ta e="T352" id="Seg_1087" s="T346">К: Табаһыттарга үгүс карчыны төлөөӈ, диигит буо.</ta>
            <ta e="T380" id="Seg_1088" s="T376">К: Иитэллэрин иһин дуу, өлөрдөктөрүнэн? </ta>
            <ta e="T417" id="Seg_1089" s="T415">К: Дьэ, илэ.</ta>
            <ta e="T441" id="Seg_1090" s="T427">К: Онно багас республика э-э депутаттара, туспа иттэ законнары таһаараннар, көмүскүүллэр, быһылак, табаһыт үлэһитин, э-э?</ta>
            <ta e="T549" id="Seg_1091" s="T546">К: Карчы оӈостоллор буо?</ta>
            <ta e="T571" id="Seg_1092" s="T551">К: Аны э һаӈа үйэгэ ити рынок туһунан үлэлээӈ, дииллэр, онтуларыӈ олокко туксубат буо, oну туксара һатаанар эӈин-эӈинник, хи, һүбэлииллэр, э-э?</ta>
            <ta e="T580" id="Seg_1093" s="T573">К: Аны киэ һиэсэ табалыыр киһилэр каалаллар һиэсэ?</ta>
            <ta e="T604" id="Seg_1094" s="T585">К: Сидор Алексеевич, урукку дьылларга ити, агыһуоннак (агыс уоннаак) иттэ дьылларгаа, быһылаак, эһигини ити депутат иттэ Верховного Совета диэн талалларыгар, оӈoроллоругар…</ta>
            <ta e="T612" id="Seg_1095" s="T604">Ким ии һанаатынан, кайдак депутат диэн таксыбыт этигитий?</ta>
            <ta e="T652" id="Seg_1096" s="T643">К: Ити район тойонноро көрөннөр, эни, эһиги ити үлэгитин, э-э? </ta>
            <ta e="T680" id="Seg_1097" s="T669">К: Оччого табалыыр киһилэр ити оччугутун (отчёткытын) һуруйуӈ диэн, иттэ көрдөһөөччү этилэр, э-э?</ta>
            <ta e="T693" id="Seg_1098" s="T680">Отчёт иттэ һурунааччы этигит, барытын кэпсээччи этигит, кас таба төрөөтө, кас ыарыйда диэн.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KuNS">
            <ta e="T6" id="Seg_1099" s="T0">–Teːtegit, inʼegit kim etej ki͡e oččogo? </ta>
            <ta e="T89" id="Seg_1100" s="T87">–Papigaj tɨ͡atɨgar. </ta>
            <ta e="T127" id="Seg_1101" s="T122">– Brigadagɨt ke kannɨk etej, brigadagɨt? </ta>
            <ta e="T147" id="Seg_1102" s="T141">– Ogo haːskɨtɨttan iti brigadaga üleleːn, eː? </ta>
            <ta e="T166" id="Seg_1103" s="T151">– Ol eː haːstarga oččogo atɨn kihiler eː, ehiginneːger kɨrdʼagas kihiler bastɨːr etilere bu͡o olokkutun, brigaːdagɨtɨn? </ta>
            <ta e="T256" id="Seg_1104" s="T243">– Sʼidar Alʼeksʼejevʼičʼ, ol u͡on agɨs tɨːhɨčča kuraŋa taba ol kajdi͡et "ehilinne", kajdi͡ek barbɨtaj? </ta>
            <ta e="T296" id="Seg_1105" s="T288">– Urut iti börönü itte ölörtöröːččü etilere bu͡o, i͡e. </ta>
            <ta e="T301" id="Seg_1106" s="T296">Anɨ onnuk da hu͡ok bu͡olbut? </ta>
            <ta e="T319" id="Seg_1107" s="T316">–Taːk daː… </ta>
            <ta e="T352" id="Seg_1108" s="T346">– "Tabahɨttarga ügüs karčɨnɨ tölöːŋ", diːgit bu͡o. </ta>
            <ta e="T380" id="Seg_1109" s="T376">– Iːtellerin ihin duː, ölördöktörünen? </ta>
            <ta e="T417" id="Seg_1110" s="T415">– Dʼe, ile. </ta>
            <ta e="T441" id="Seg_1111" s="T427">– Onno bagas rʼespublʼika eː dʼeputaːttara, tuspa itte zakonnarɨ tahaːrannar, kömüsküːller, bɨhɨlak, tabahɨt ülehitin, eː? </ta>
            <ta e="T549" id="Seg_1112" s="T546">–Karčɨ oŋostollor bu͡o? </ta>
            <ta e="T571" id="Seg_1113" s="T551">– Anɨ e haŋa üjege "iti rɨnak tuhunan üleleːŋ", diːller, ontularɨŋ olokko tuksubat bu͡o, onu tuksara hataːnar eŋin-eŋinnik (ki) hübeliːller, eː? </ta>
            <ta e="T580" id="Seg_1114" s="T573">– Anɨ ki͡e hi͡ese tabalɨːr kihiler kaːlallar hi͡ese? </ta>
            <ta e="T604" id="Seg_1115" s="T585">– Sʼidar Alʼeksʼejevʼičʼ, urukku dʼɨllarga iti, agɨhu͡onnaːk itte dʼɨllarga, bɨhɨlaːk, ehigini iti dʼeputat itte Vʼerxoːvnava Savʼeta di͡en talallarɨgar, oŋorollorugar… </ta>
            <ta e="T612" id="Seg_1116" s="T604">Kim eː hanaːtɨnan, kajdak dʼeputat di͡en taksɨbɨt etigitij? </ta>
            <ta e="T652" id="Seg_1117" s="T643">– Iti rajon tojonnoro körönnör, eni, ehigi iti ülegitin, eː? </ta>
            <ta e="T680" id="Seg_1118" s="T669">– Oččogo tabalɨːr kihiler iti oččugutun hurujuŋ di͡en, itte kördöhöːččü etiler, eː? </ta>
            <ta e="T693" id="Seg_1119" s="T680">Oččot itte hurunaːččɨ etigit, barɨtɨn kepseːčči etigit, kas taba töröːtö, kas ɨ͡arɨjda di͡en. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KuNS">
            <ta e="T1" id="Seg_1120" s="T0">teːte-git</ta>
            <ta e="T2" id="Seg_1121" s="T1">inʼe-git</ta>
            <ta e="T3" id="Seg_1122" s="T2">kim</ta>
            <ta e="T4" id="Seg_1123" s="T3">e-t-e=j</ta>
            <ta e="T5" id="Seg_1124" s="T4">ki͡e</ta>
            <ta e="T6" id="Seg_1125" s="T5">oččogo</ta>
            <ta e="T88" id="Seg_1126" s="T87">Papigaj</ta>
            <ta e="T89" id="Seg_1127" s="T88">tɨ͡a-tɨ-gar</ta>
            <ta e="T123" id="Seg_1128" s="T122">brigada-gɨt</ta>
            <ta e="T124" id="Seg_1129" s="T123">ke</ta>
            <ta e="T125" id="Seg_1130" s="T124">kannɨk</ta>
            <ta e="T126" id="Seg_1131" s="T125">e-t-e=j</ta>
            <ta e="T127" id="Seg_1132" s="T126">brigada-gɨt</ta>
            <ta e="T142" id="Seg_1133" s="T141">ogo</ta>
            <ta e="T143" id="Seg_1134" s="T142">haːs-kɨtɨ-ttan</ta>
            <ta e="T144" id="Seg_1135" s="T143">iti</ta>
            <ta e="T145" id="Seg_1136" s="T144">brigada-ga</ta>
            <ta e="T146" id="Seg_1137" s="T145">üleleː-n</ta>
            <ta e="T147" id="Seg_1138" s="T146">eː</ta>
            <ta e="T152" id="Seg_1139" s="T151">ol</ta>
            <ta e="T153" id="Seg_1140" s="T152">eː</ta>
            <ta e="T154" id="Seg_1141" s="T153">haːs-tar-ga</ta>
            <ta e="T155" id="Seg_1142" s="T154">oččogo</ta>
            <ta e="T156" id="Seg_1143" s="T155">atɨn</ta>
            <ta e="T157" id="Seg_1144" s="T156">kihi-ler</ta>
            <ta e="T158" id="Seg_1145" s="T157">eː</ta>
            <ta e="T159" id="Seg_1146" s="T158">ehigi-nneːger</ta>
            <ta e="T160" id="Seg_1147" s="T159">kɨrdʼagas</ta>
            <ta e="T161" id="Seg_1148" s="T160">kihi-ler</ta>
            <ta e="T162" id="Seg_1149" s="T161">bastɨː-r</ta>
            <ta e="T163" id="Seg_1150" s="T162">e-ti-lere</ta>
            <ta e="T164" id="Seg_1151" s="T163">bu͡o</ta>
            <ta e="T165" id="Seg_1152" s="T164">olok-kutu-n</ta>
            <ta e="T166" id="Seg_1153" s="T165">brigaːda-gɨtɨ-n</ta>
            <ta e="T244" id="Seg_1154" s="T243">Sʼidar </ta>
            <ta e="T245" id="Seg_1155" s="T244">Alʼeksʼejevʼičʼ</ta>
            <ta e="T246" id="Seg_1156" s="T245">ol</ta>
            <ta e="T247" id="Seg_1157" s="T246">u͡on</ta>
            <ta e="T248" id="Seg_1158" s="T247">agɨs</ta>
            <ta e="T249" id="Seg_1159" s="T248">tɨːhɨčča</ta>
            <ta e="T250" id="Seg_1160" s="T249">kuraŋ-a</ta>
            <ta e="T251" id="Seg_1161" s="T250">taba</ta>
            <ta e="T252" id="Seg_1162" s="T251">ol</ta>
            <ta e="T253" id="Seg_1163" s="T252">kajdi͡et</ta>
            <ta e="T254" id="Seg_1164" s="T253">eh-i-lin-n-e</ta>
            <ta e="T255" id="Seg_1165" s="T254">kajdi͡ek</ta>
            <ta e="T256" id="Seg_1166" s="T255">bar-bɨt-a=j</ta>
            <ta e="T289" id="Seg_1167" s="T288">urut</ta>
            <ta e="T290" id="Seg_1168" s="T289">iti</ta>
            <ta e="T291" id="Seg_1169" s="T290">börö-nü</ta>
            <ta e="T292" id="Seg_1170" s="T291">itte</ta>
            <ta e="T293" id="Seg_1171" s="T292">ölör-tör-öːččü</ta>
            <ta e="T294" id="Seg_1172" s="T293">e-ti-lere</ta>
            <ta e="T295" id="Seg_1173" s="T294">bu͡o</ta>
            <ta e="T296" id="Seg_1174" s="T295">i͡e</ta>
            <ta e="T297" id="Seg_1175" s="T296">anɨ</ta>
            <ta e="T298" id="Seg_1176" s="T297">onnuk</ta>
            <ta e="T299" id="Seg_1177" s="T298">da</ta>
            <ta e="T300" id="Seg_1178" s="T299">hu͡ok</ta>
            <ta e="T301" id="Seg_1179" s="T300">bu͡ol-but</ta>
            <ta e="T319" id="Seg_1180" s="T317">daː</ta>
            <ta e="T347" id="Seg_1181" s="T346">taba-hɨt-tar-ga</ta>
            <ta e="T348" id="Seg_1182" s="T347">ügüs</ta>
            <ta e="T349" id="Seg_1183" s="T348">karčɨ-nɨ</ta>
            <ta e="T350" id="Seg_1184" s="T349">tölöː-ŋ</ta>
            <ta e="T351" id="Seg_1185" s="T350">d-iː-git</ta>
            <ta e="T352" id="Seg_1186" s="T351">bu͡o</ta>
            <ta e="T377" id="Seg_1187" s="T376">iːt-el-leri-n</ta>
            <ta e="T378" id="Seg_1188" s="T377">ihin</ta>
            <ta e="T379" id="Seg_1189" s="T378">duː</ta>
            <ta e="T380" id="Seg_1190" s="T379">ölör-dök-törü-nen</ta>
            <ta e="T416" id="Seg_1191" s="T415">dʼe</ta>
            <ta e="T417" id="Seg_1192" s="T416">ile</ta>
            <ta e="T428" id="Seg_1193" s="T427">onno</ta>
            <ta e="T429" id="Seg_1194" s="T428">bagas</ta>
            <ta e="T430" id="Seg_1195" s="T429">rʼespublʼika</ta>
            <ta e="T431" id="Seg_1196" s="T430">eː</ta>
            <ta e="T432" id="Seg_1197" s="T431">dʼeputaːt-tar-a</ta>
            <ta e="T433" id="Seg_1198" s="T432">tuspa</ta>
            <ta e="T434" id="Seg_1199" s="T433">itte</ta>
            <ta e="T435" id="Seg_1200" s="T434">zakon-nar-ɨ</ta>
            <ta e="T436" id="Seg_1201" s="T435">tahaːr-an-nar</ta>
            <ta e="T437" id="Seg_1202" s="T436">kömüsküː-l-ler</ta>
            <ta e="T438" id="Seg_1203" s="T437">bɨhɨlaːk</ta>
            <ta e="T439" id="Seg_1204" s="T438">taba-hɨt</ta>
            <ta e="T440" id="Seg_1205" s="T439">ülehit-i-n</ta>
            <ta e="T441" id="Seg_1206" s="T440">eː</ta>
            <ta e="T547" id="Seg_1207" s="T546">karčɨ</ta>
            <ta e="T548" id="Seg_1208" s="T547">oŋost-ol-lor</ta>
            <ta e="T549" id="Seg_1209" s="T548">bu͡o</ta>
            <ta e="T552" id="Seg_1210" s="T551">anɨ</ta>
            <ta e="T553" id="Seg_1211" s="T552">e</ta>
            <ta e="T554" id="Seg_1212" s="T553">haŋa</ta>
            <ta e="T555" id="Seg_1213" s="T554">üje-ge</ta>
            <ta e="T556" id="Seg_1214" s="T555">iti</ta>
            <ta e="T557" id="Seg_1215" s="T556">rɨnak</ta>
            <ta e="T558" id="Seg_1216" s="T557">tuh-u-nan</ta>
            <ta e="T559" id="Seg_1217" s="T558">üleleː-ŋ</ta>
            <ta e="T560" id="Seg_1218" s="T559">diː-l-ler</ta>
            <ta e="T561" id="Seg_1219" s="T560">on-tu-lar-ɨ-ŋ</ta>
            <ta e="T562" id="Seg_1220" s="T561">olok-ko</ta>
            <ta e="T563" id="Seg_1221" s="T562">tuks-u-bat</ta>
            <ta e="T564" id="Seg_1222" s="T563">bu͡o</ta>
            <ta e="T565" id="Seg_1223" s="T564">o-nu</ta>
            <ta e="T566" id="Seg_1224" s="T565">tuks-a-r-a</ta>
            <ta e="T567" id="Seg_1225" s="T566">hataː-n-ar</ta>
            <ta e="T568" id="Seg_1226" s="T567">eŋin-eŋin-nik</ta>
            <ta e="T570" id="Seg_1227" s="T569">hübeliː-l-ler</ta>
            <ta e="T571" id="Seg_1228" s="T570">eː</ta>
            <ta e="T574" id="Seg_1229" s="T573">anɨ</ta>
            <ta e="T575" id="Seg_1230" s="T574">ki͡e</ta>
            <ta e="T576" id="Seg_1231" s="T575">hi͡ese</ta>
            <ta e="T577" id="Seg_1232" s="T576">taba-lɨː-r</ta>
            <ta e="T578" id="Seg_1233" s="T577">kihi-ler</ta>
            <ta e="T579" id="Seg_1234" s="T578">kaːl-al-lar</ta>
            <ta e="T580" id="Seg_1235" s="T579">hi͡ese</ta>
            <ta e="T586" id="Seg_1236" s="T585">Sʼidar </ta>
            <ta e="T587" id="Seg_1237" s="T586">Alʼeksʼejevʼičʼ</ta>
            <ta e="T588" id="Seg_1238" s="T587">urukku</ta>
            <ta e="T589" id="Seg_1239" s="T588">dʼɨl-lar-ga</ta>
            <ta e="T590" id="Seg_1240" s="T589">iti</ta>
            <ta e="T591" id="Seg_1241" s="T590">agɨhu͡on-naːk</ta>
            <ta e="T592" id="Seg_1242" s="T591">itte</ta>
            <ta e="T593" id="Seg_1243" s="T592">dʼɨl-lar-ga</ta>
            <ta e="T594" id="Seg_1244" s="T593">bɨhɨlaːk</ta>
            <ta e="T595" id="Seg_1245" s="T594">ehigi-ni</ta>
            <ta e="T596" id="Seg_1246" s="T595">iti</ta>
            <ta e="T597" id="Seg_1247" s="T596">dʼeputat</ta>
            <ta e="T598" id="Seg_1248" s="T597">itte</ta>
            <ta e="T601" id="Seg_1249" s="T600">di͡e-n</ta>
            <ta e="T602" id="Seg_1250" s="T601">tal-al-larɨ-gar</ta>
            <ta e="T604" id="Seg_1251" s="T602">oŋor-ol-loru-gar</ta>
            <ta e="T605" id="Seg_1252" s="T604">kim</ta>
            <ta e="T606" id="Seg_1253" s="T605">eː</ta>
            <ta e="T607" id="Seg_1254" s="T606">hanaː-tɨ-nan</ta>
            <ta e="T608" id="Seg_1255" s="T607">kajdak</ta>
            <ta e="T609" id="Seg_1256" s="T608">dʼeputat</ta>
            <ta e="T610" id="Seg_1257" s="T609">di͡e-n</ta>
            <ta e="T611" id="Seg_1258" s="T610">taks-ɨ-bɨt</ta>
            <ta e="T612" id="Seg_1259" s="T611">e-ti-git=ij</ta>
            <ta e="T644" id="Seg_1260" s="T643">iti</ta>
            <ta e="T645" id="Seg_1261" s="T644">rajon</ta>
            <ta e="T646" id="Seg_1262" s="T645">tojon-nor-o</ta>
            <ta e="T647" id="Seg_1263" s="T646">kör-ön-nör</ta>
            <ta e="T648" id="Seg_1264" s="T647">eni</ta>
            <ta e="T649" id="Seg_1265" s="T648">ehigi</ta>
            <ta e="T650" id="Seg_1266" s="T649">iti</ta>
            <ta e="T651" id="Seg_1267" s="T650">üle-giti-n</ta>
            <ta e="T652" id="Seg_1268" s="T651">eː</ta>
            <ta e="T670" id="Seg_1269" s="T669">oččogo</ta>
            <ta e="T671" id="Seg_1270" s="T670">taba-lɨː-r</ta>
            <ta e="T672" id="Seg_1271" s="T671">kihi-ler</ta>
            <ta e="T673" id="Seg_1272" s="T672">iti</ta>
            <ta e="T674" id="Seg_1273" s="T673">očču-gutu-n</ta>
            <ta e="T675" id="Seg_1274" s="T674">huruj-u-ŋ</ta>
            <ta e="T676" id="Seg_1275" s="T675">di͡e-n</ta>
            <ta e="T677" id="Seg_1276" s="T676">itte</ta>
            <ta e="T678" id="Seg_1277" s="T677">körd-ö-h-öːččü</ta>
            <ta e="T679" id="Seg_1278" s="T678">e-ti-ler</ta>
            <ta e="T680" id="Seg_1279" s="T679">eː</ta>
            <ta e="T681" id="Seg_1280" s="T680">oččot</ta>
            <ta e="T682" id="Seg_1281" s="T681">itte</ta>
            <ta e="T683" id="Seg_1282" s="T682">huru-n-aːččɨ</ta>
            <ta e="T684" id="Seg_1283" s="T683">e-ti-git</ta>
            <ta e="T685" id="Seg_1284" s="T684">barɨ-tɨ-n</ta>
            <ta e="T686" id="Seg_1285" s="T685">keps-eːčči</ta>
            <ta e="T687" id="Seg_1286" s="T686">e-ti-git</ta>
            <ta e="T688" id="Seg_1287" s="T687">kas</ta>
            <ta e="T689" id="Seg_1288" s="T688">taba</ta>
            <ta e="T690" id="Seg_1289" s="T689">töröː-t-ö</ta>
            <ta e="T691" id="Seg_1290" s="T690">kas</ta>
            <ta e="T692" id="Seg_1291" s="T691">ɨ͡arɨj-d-a</ta>
            <ta e="T693" id="Seg_1292" s="T692">di͡e-n</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KuNS">
            <ta e="T1" id="Seg_1293" s="T0">teːte-GIt</ta>
            <ta e="T2" id="Seg_1294" s="T1">inʼe-GIt</ta>
            <ta e="T3" id="Seg_1295" s="T2">kim</ta>
            <ta e="T4" id="Seg_1296" s="T3">e-TI-tA=Ij</ta>
            <ta e="T5" id="Seg_1297" s="T4">keː</ta>
            <ta e="T6" id="Seg_1298" s="T5">oččogo</ta>
            <ta e="T88" id="Seg_1299" s="T87">Popigaj</ta>
            <ta e="T89" id="Seg_1300" s="T88">tɨ͡a-tI-GAr</ta>
            <ta e="T123" id="Seg_1301" s="T122">brigada-GIt</ta>
            <ta e="T124" id="Seg_1302" s="T123">ka</ta>
            <ta e="T125" id="Seg_1303" s="T124">kannɨk</ta>
            <ta e="T126" id="Seg_1304" s="T125">e-TI-tA=Ij</ta>
            <ta e="T127" id="Seg_1305" s="T126">brigada-GIt</ta>
            <ta e="T142" id="Seg_1306" s="T141">ogo</ta>
            <ta e="T143" id="Seg_1307" s="T142">haːs-GItI-ttAn</ta>
            <ta e="T144" id="Seg_1308" s="T143">iti</ta>
            <ta e="T145" id="Seg_1309" s="T144">brigada-GA</ta>
            <ta e="T146" id="Seg_1310" s="T145">üleleː-An</ta>
            <ta e="T147" id="Seg_1311" s="T146">eː</ta>
            <ta e="T152" id="Seg_1312" s="T151">ol</ta>
            <ta e="T153" id="Seg_1313" s="T152">eː</ta>
            <ta e="T154" id="Seg_1314" s="T153">haːs-LAr-GA</ta>
            <ta e="T155" id="Seg_1315" s="T154">oččogo</ta>
            <ta e="T156" id="Seg_1316" s="T155">atɨn</ta>
            <ta e="T157" id="Seg_1317" s="T156">kihi-LAr</ta>
            <ta e="T158" id="Seg_1318" s="T157">eː</ta>
            <ta e="T159" id="Seg_1319" s="T158">ehigi-TAːgAr</ta>
            <ta e="T160" id="Seg_1320" s="T159">kɨrdʼagas</ta>
            <ta e="T161" id="Seg_1321" s="T160">kihi-LAr</ta>
            <ta e="T162" id="Seg_1322" s="T161">bastaː-Ar</ta>
            <ta e="T163" id="Seg_1323" s="T162">e-TI-LArA</ta>
            <ta e="T164" id="Seg_1324" s="T163">bu͡o</ta>
            <ta e="T165" id="Seg_1325" s="T164">olok-GItI-n</ta>
            <ta e="T166" id="Seg_1326" s="T165">brigada-GItI-n</ta>
            <ta e="T244" id="Seg_1327" s="T243">Sʼidar </ta>
            <ta e="T245" id="Seg_1328" s="T244">Alʼeksʼejevʼičʼ</ta>
            <ta e="T246" id="Seg_1329" s="T245">ol</ta>
            <ta e="T247" id="Seg_1330" s="T246">u͡on</ta>
            <ta e="T248" id="Seg_1331" s="T247">agɨs</ta>
            <ta e="T249" id="Seg_1332" s="T248">tɨːhɨčča</ta>
            <ta e="T250" id="Seg_1333" s="T249">kuraŋ-tA</ta>
            <ta e="T251" id="Seg_1334" s="T250">taba</ta>
            <ta e="T252" id="Seg_1335" s="T251">ol</ta>
            <ta e="T253" id="Seg_1336" s="T252">kajdi͡ek</ta>
            <ta e="T254" id="Seg_1337" s="T253">es-I-LIN-TI-tA</ta>
            <ta e="T255" id="Seg_1338" s="T254">kajdi͡ek</ta>
            <ta e="T256" id="Seg_1339" s="T255">bar-BIT-tA=Ij</ta>
            <ta e="T289" id="Seg_1340" s="T288">urut</ta>
            <ta e="T290" id="Seg_1341" s="T289">iti</ta>
            <ta e="T291" id="Seg_1342" s="T290">börö-nI</ta>
            <ta e="T292" id="Seg_1343" s="T291">itte</ta>
            <ta e="T293" id="Seg_1344" s="T292">ölör-TAr-AːččI</ta>
            <ta e="T294" id="Seg_1345" s="T293">e-TI-LArA</ta>
            <ta e="T295" id="Seg_1346" s="T294">bu͡o</ta>
            <ta e="T296" id="Seg_1347" s="T295">eː</ta>
            <ta e="T297" id="Seg_1348" s="T296">anɨ</ta>
            <ta e="T298" id="Seg_1349" s="T297">onnuk</ta>
            <ta e="T299" id="Seg_1350" s="T298">da</ta>
            <ta e="T300" id="Seg_1351" s="T299">hu͡ok</ta>
            <ta e="T301" id="Seg_1352" s="T300">bu͡ol-BIT</ta>
            <ta e="T319" id="Seg_1353" s="T317">da</ta>
            <ta e="T347" id="Seg_1354" s="T346">taba-ČIt-LAr-GA</ta>
            <ta e="T348" id="Seg_1355" s="T347">ügüs</ta>
            <ta e="T349" id="Seg_1356" s="T348">karčɨ-nI</ta>
            <ta e="T350" id="Seg_1357" s="T349">tölöː-ŋ</ta>
            <ta e="T351" id="Seg_1358" s="T350">di͡e-A-GIt</ta>
            <ta e="T352" id="Seg_1359" s="T351">bu͡o</ta>
            <ta e="T377" id="Seg_1360" s="T376">iːt-Ar-LArI-n</ta>
            <ta e="T378" id="Seg_1361" s="T377">ihin</ta>
            <ta e="T379" id="Seg_1362" s="T378">du͡o</ta>
            <ta e="T380" id="Seg_1363" s="T379">ölör-TAK-LArI-nAn</ta>
            <ta e="T416" id="Seg_1364" s="T415">dʼe</ta>
            <ta e="T417" id="Seg_1365" s="T416">ile</ta>
            <ta e="T428" id="Seg_1366" s="T427">onno</ta>
            <ta e="T429" id="Seg_1367" s="T428">bagas</ta>
            <ta e="T430" id="Seg_1368" s="T429">respublika</ta>
            <ta e="T431" id="Seg_1369" s="T430">eː</ta>
            <ta e="T432" id="Seg_1370" s="T431">deputat-LAr-tA</ta>
            <ta e="T433" id="Seg_1371" s="T432">tuspa</ta>
            <ta e="T434" id="Seg_1372" s="T433">itte</ta>
            <ta e="T435" id="Seg_1373" s="T434">hoku͡on-LAr-nI</ta>
            <ta e="T436" id="Seg_1374" s="T435">tahaːr-An-LAr</ta>
            <ta e="T437" id="Seg_1375" s="T436">kömüskeː-Ar-LAr</ta>
            <ta e="T438" id="Seg_1376" s="T437">bɨhɨːlaːk</ta>
            <ta e="T439" id="Seg_1377" s="T438">taba-ČIt</ta>
            <ta e="T440" id="Seg_1378" s="T439">ülehit-tI-n</ta>
            <ta e="T441" id="Seg_1379" s="T440">eː</ta>
            <ta e="T547" id="Seg_1380" s="T546">karčɨ</ta>
            <ta e="T548" id="Seg_1381" s="T547">oŋohun-Ar-LAr</ta>
            <ta e="T549" id="Seg_1382" s="T548">bu͡o</ta>
            <ta e="T552" id="Seg_1383" s="T551">anɨ</ta>
            <ta e="T553" id="Seg_1384" s="T552">e</ta>
            <ta e="T554" id="Seg_1385" s="T553">haŋa</ta>
            <ta e="T555" id="Seg_1386" s="T554">üje-GA</ta>
            <ta e="T556" id="Seg_1387" s="T555">iti</ta>
            <ta e="T557" id="Seg_1388" s="T556">rɨnak</ta>
            <ta e="T558" id="Seg_1389" s="T557">tus-tI-nAn</ta>
            <ta e="T559" id="Seg_1390" s="T558">üleleː-ŋ</ta>
            <ta e="T560" id="Seg_1391" s="T559">di͡e-Ar-LAr</ta>
            <ta e="T561" id="Seg_1392" s="T560">ol-tI-LAr-I-ŋ</ta>
            <ta e="T562" id="Seg_1393" s="T561">olok-GA</ta>
            <ta e="T563" id="Seg_1394" s="T562">tugus-I-BAT</ta>
            <ta e="T564" id="Seg_1395" s="T563">bu͡o</ta>
            <ta e="T565" id="Seg_1396" s="T564">ol-nI</ta>
            <ta e="T566" id="Seg_1397" s="T565">tugus-A-r-A</ta>
            <ta e="T567" id="Seg_1398" s="T566">hataː-n-Ar</ta>
            <ta e="T568" id="Seg_1399" s="T567">eŋin-eŋin-LIk</ta>
            <ta e="T570" id="Seg_1400" s="T569">hübeleː-Ar-LAr</ta>
            <ta e="T571" id="Seg_1401" s="T570">eː</ta>
            <ta e="T574" id="Seg_1402" s="T573">anɨ</ta>
            <ta e="T575" id="Seg_1403" s="T574">keː</ta>
            <ta e="T576" id="Seg_1404" s="T575">hi͡ese</ta>
            <ta e="T577" id="Seg_1405" s="T576">taba-LAː-Ar</ta>
            <ta e="T578" id="Seg_1406" s="T577">kihi-LAr</ta>
            <ta e="T579" id="Seg_1407" s="T578">kaːl-Ar-LAr</ta>
            <ta e="T580" id="Seg_1408" s="T579">hi͡ese</ta>
            <ta e="T586" id="Seg_1409" s="T585">Sʼidar </ta>
            <ta e="T587" id="Seg_1410" s="T586">Alʼeksʼejevʼičʼ</ta>
            <ta e="T588" id="Seg_1411" s="T587">urukku</ta>
            <ta e="T589" id="Seg_1412" s="T588">dʼɨl-LAr-GA</ta>
            <ta e="T590" id="Seg_1413" s="T589">iti</ta>
            <ta e="T591" id="Seg_1414" s="T590">agɨhu͡on-LAːK</ta>
            <ta e="T592" id="Seg_1415" s="T591">itte</ta>
            <ta e="T593" id="Seg_1416" s="T592">dʼɨl-LAr-GA</ta>
            <ta e="T594" id="Seg_1417" s="T593">bɨhɨːlaːk</ta>
            <ta e="T595" id="Seg_1418" s="T594">ehigi-nI</ta>
            <ta e="T596" id="Seg_1419" s="T595">iti</ta>
            <ta e="T597" id="Seg_1420" s="T596">deputat</ta>
            <ta e="T598" id="Seg_1421" s="T597">itte</ta>
            <ta e="T601" id="Seg_1422" s="T600">di͡e-An</ta>
            <ta e="T602" id="Seg_1423" s="T601">tal-Ar-LArI-GAr</ta>
            <ta e="T604" id="Seg_1424" s="T602">oŋor-Ar-LArI-GAr</ta>
            <ta e="T605" id="Seg_1425" s="T604">kim</ta>
            <ta e="T606" id="Seg_1426" s="T605">eː</ta>
            <ta e="T607" id="Seg_1427" s="T606">hanaː-tI-nAn</ta>
            <ta e="T608" id="Seg_1428" s="T607">kajdak</ta>
            <ta e="T609" id="Seg_1429" s="T608">deputat</ta>
            <ta e="T610" id="Seg_1430" s="T609">di͡e-An</ta>
            <ta e="T611" id="Seg_1431" s="T610">tagɨs-I-BIT</ta>
            <ta e="T612" id="Seg_1432" s="T611">e-TI-GIt=Ij</ta>
            <ta e="T644" id="Seg_1433" s="T643">iti</ta>
            <ta e="T645" id="Seg_1434" s="T644">rajon</ta>
            <ta e="T646" id="Seg_1435" s="T645">tojon-LAr-tA</ta>
            <ta e="T647" id="Seg_1436" s="T646">kör-An-LAr</ta>
            <ta e="T648" id="Seg_1437" s="T647">eni</ta>
            <ta e="T649" id="Seg_1438" s="T648">ehigi</ta>
            <ta e="T650" id="Seg_1439" s="T649">iti</ta>
            <ta e="T651" id="Seg_1440" s="T650">üle-GItI-n</ta>
            <ta e="T652" id="Seg_1441" s="T651">eː</ta>
            <ta e="T670" id="Seg_1442" s="T669">oččogo</ta>
            <ta e="T671" id="Seg_1443" s="T670">taba-LAː-Ar</ta>
            <ta e="T672" id="Seg_1444" s="T671">kihi-LAr</ta>
            <ta e="T673" id="Seg_1445" s="T672">iti</ta>
            <ta e="T674" id="Seg_1446" s="T673">oččot-GItI-n</ta>
            <ta e="T675" id="Seg_1447" s="T674">huruj-I-ŋ</ta>
            <ta e="T676" id="Seg_1448" s="T675">di͡e-An</ta>
            <ta e="T677" id="Seg_1449" s="T676">itte</ta>
            <ta e="T678" id="Seg_1450" s="T677">kördöː-A-s-AːččI</ta>
            <ta e="T679" id="Seg_1451" s="T678">e-TI-LAr</ta>
            <ta e="T680" id="Seg_1452" s="T679">eː</ta>
            <ta e="T681" id="Seg_1453" s="T680">oččot</ta>
            <ta e="T682" id="Seg_1454" s="T681">itte</ta>
            <ta e="T683" id="Seg_1455" s="T682">huruj-n-AːččI</ta>
            <ta e="T684" id="Seg_1456" s="T683">e-TI-GIt</ta>
            <ta e="T685" id="Seg_1457" s="T684">barɨ-tI-n</ta>
            <ta e="T686" id="Seg_1458" s="T685">kepseː-AːččI</ta>
            <ta e="T687" id="Seg_1459" s="T686">e-TI-GIt</ta>
            <ta e="T688" id="Seg_1460" s="T687">kas</ta>
            <ta e="T689" id="Seg_1461" s="T688">taba</ta>
            <ta e="T690" id="Seg_1462" s="T689">töröː-TI-tA</ta>
            <ta e="T691" id="Seg_1463" s="T690">kas</ta>
            <ta e="T692" id="Seg_1464" s="T691">ɨ͡arɨj-TI-tA</ta>
            <ta e="T693" id="Seg_1465" s="T692">di͡e-An</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KuNS">
            <ta e="T1" id="Seg_1466" s="T0">father-2PL.[NOM]</ta>
            <ta e="T2" id="Seg_1467" s="T1">mother-2PL.[NOM]</ta>
            <ta e="T3" id="Seg_1468" s="T2">who.[NOM]</ta>
            <ta e="T4" id="Seg_1469" s="T3">be-PST1-3SG=Q</ta>
            <ta e="T5" id="Seg_1470" s="T4">EMPH</ta>
            <ta e="T6" id="Seg_1471" s="T5">then</ta>
            <ta e="T88" id="Seg_1472" s="T87">Popigaj.[NOM]</ta>
            <ta e="T89" id="Seg_1473" s="T88">tundra-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_1474" s="T122">brigade-2PL.[NOM]</ta>
            <ta e="T124" id="Seg_1475" s="T123">well</ta>
            <ta e="T125" id="Seg_1476" s="T124">what.kind.of</ta>
            <ta e="T126" id="Seg_1477" s="T125">be-PST1-3SG=Q</ta>
            <ta e="T127" id="Seg_1478" s="T126">brigade-2PL.[NOM]</ta>
            <ta e="T142" id="Seg_1479" s="T141">young</ta>
            <ta e="T143" id="Seg_1480" s="T142">age-2PL-ABL</ta>
            <ta e="T144" id="Seg_1481" s="T143">that</ta>
            <ta e="T145" id="Seg_1482" s="T144">brigade-DAT/LOC</ta>
            <ta e="T146" id="Seg_1483" s="T145">work-CVB.SEQ</ta>
            <ta e="T147" id="Seg_1484" s="T146">EMPH</ta>
            <ta e="T152" id="Seg_1485" s="T151">that</ta>
            <ta e="T153" id="Seg_1486" s="T152">eh</ta>
            <ta e="T154" id="Seg_1487" s="T153">age-PL-DAT/LOC</ta>
            <ta e="T155" id="Seg_1488" s="T154">then</ta>
            <ta e="T156" id="Seg_1489" s="T155">different</ta>
            <ta e="T157" id="Seg_1490" s="T156">human.being-PL.[NOM]</ta>
            <ta e="T158" id="Seg_1491" s="T157">eh</ta>
            <ta e="T159" id="Seg_1492" s="T158">2PL-COMP</ta>
            <ta e="T160" id="Seg_1493" s="T159">old</ta>
            <ta e="T161" id="Seg_1494" s="T160">human.being-PL.[NOM]</ta>
            <ta e="T162" id="Seg_1495" s="T161">lead-PTCP.PRS</ta>
            <ta e="T163" id="Seg_1496" s="T162">be-PST1-3PL</ta>
            <ta e="T164" id="Seg_1497" s="T163">EMPH</ta>
            <ta e="T165" id="Seg_1498" s="T164">life-2PL-ACC</ta>
            <ta e="T166" id="Seg_1499" s="T165">brigade-2PL-ACC</ta>
            <ta e="T244" id="Seg_1500" s="T243">Sidor</ta>
            <ta e="T245" id="Seg_1501" s="T244">Alekseevich.[NOM]</ta>
            <ta e="T246" id="Seg_1502" s="T245">that</ta>
            <ta e="T247" id="Seg_1503" s="T246">ten</ta>
            <ta e="T248" id="Seg_1504" s="T247">eight</ta>
            <ta e="T249" id="Seg_1505" s="T248">thousand</ta>
            <ta e="T250" id="Seg_1506" s="T249">approximately-3SG.[NOM]</ta>
            <ta e="T251" id="Seg_1507" s="T250">reindeer.[NOM]</ta>
            <ta e="T252" id="Seg_1508" s="T251">that</ta>
            <ta e="T253" id="Seg_1509" s="T252">whereto</ta>
            <ta e="T254" id="Seg_1510" s="T253">throw.away-EP-PASS/REFL-PST1-3SG</ta>
            <ta e="T255" id="Seg_1511" s="T254">whereto</ta>
            <ta e="T256" id="Seg_1512" s="T255">go-PST2-3SG=Q</ta>
            <ta e="T289" id="Seg_1513" s="T288">before</ta>
            <ta e="T290" id="Seg_1514" s="T289">that</ta>
            <ta e="T291" id="Seg_1515" s="T290">wolf-ACC</ta>
            <ta e="T292" id="Seg_1516" s="T291">EMPH</ta>
            <ta e="T293" id="Seg_1517" s="T292">kill-CAUS-PTCP.HAB</ta>
            <ta e="T294" id="Seg_1518" s="T293">be-PST1-3PL</ta>
            <ta e="T295" id="Seg_1519" s="T294">EMPH</ta>
            <ta e="T296" id="Seg_1520" s="T295">AFFIRM</ta>
            <ta e="T297" id="Seg_1521" s="T296">now</ta>
            <ta e="T298" id="Seg_1522" s="T297">such.[NOM]</ta>
            <ta e="T299" id="Seg_1523" s="T298">EMPH</ta>
            <ta e="T300" id="Seg_1524" s="T299">NEG.EX</ta>
            <ta e="T301" id="Seg_1525" s="T300">become-PST2.[3SG]</ta>
            <ta e="T319" id="Seg_1526" s="T317">and</ta>
            <ta e="T347" id="Seg_1527" s="T346">reindeer-AG-PL-DAT/LOC</ta>
            <ta e="T348" id="Seg_1528" s="T347">many</ta>
            <ta e="T349" id="Seg_1529" s="T348">money-ACC</ta>
            <ta e="T350" id="Seg_1530" s="T349">pay-IMP.2PL</ta>
            <ta e="T351" id="Seg_1531" s="T350">say-PRS-2PL</ta>
            <ta e="T352" id="Seg_1532" s="T351">EMPH</ta>
            <ta e="T377" id="Seg_1533" s="T376">bring.up-PTCP.PRS-3PL-ACC</ta>
            <ta e="T378" id="Seg_1534" s="T377">because.of</ta>
            <ta e="T379" id="Seg_1535" s="T378">Q</ta>
            <ta e="T380" id="Seg_1536" s="T379">kill-PTCP.COND-3PL-INSTR</ta>
            <ta e="T416" id="Seg_1537" s="T415">well</ta>
            <ta e="T417" id="Seg_1538" s="T416">indeed</ta>
            <ta e="T428" id="Seg_1539" s="T427">there</ta>
            <ta e="T429" id="Seg_1540" s="T428">EMPH</ta>
            <ta e="T430" id="Seg_1541" s="T429">republic.[NOM]</ta>
            <ta e="T431" id="Seg_1542" s="T430">eh</ta>
            <ta e="T432" id="Seg_1543" s="T431">deputy-PL-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_1544" s="T432">individually</ta>
            <ta e="T434" id="Seg_1545" s="T433">EMPH</ta>
            <ta e="T435" id="Seg_1546" s="T434">law-PL-ACC</ta>
            <ta e="T436" id="Seg_1547" s="T435">take.out-CVB.SEQ-3PL</ta>
            <ta e="T437" id="Seg_1548" s="T436">protect-PRS-3PL</ta>
            <ta e="T438" id="Seg_1549" s="T437">apparently</ta>
            <ta e="T439" id="Seg_1550" s="T438">reindeer-AG.[NOM]</ta>
            <ta e="T440" id="Seg_1551" s="T439">worker-3SG-ACC</ta>
            <ta e="T441" id="Seg_1552" s="T440">AFFIRM</ta>
            <ta e="T547" id="Seg_1553" s="T546">money.[NOM]</ta>
            <ta e="T548" id="Seg_1554" s="T547">make-PRS-3PL</ta>
            <ta e="T549" id="Seg_1555" s="T548">EMPH</ta>
            <ta e="T552" id="Seg_1556" s="T551">now</ta>
            <ta e="T553" id="Seg_1557" s="T552">eh</ta>
            <ta e="T554" id="Seg_1558" s="T553">new</ta>
            <ta e="T555" id="Seg_1559" s="T554">time-DAT/LOC</ta>
            <ta e="T556" id="Seg_1560" s="T555">that.[NOM]</ta>
            <ta e="T557" id="Seg_1561" s="T556">market.[NOM]</ta>
            <ta e="T558" id="Seg_1562" s="T557">side-3SG-INSTR</ta>
            <ta e="T559" id="Seg_1563" s="T558">work-IMP.2PL</ta>
            <ta e="T560" id="Seg_1564" s="T559">say-PRS-3PL</ta>
            <ta e="T561" id="Seg_1565" s="T560">that-3SG-PL-EP-2SG.[NOM]</ta>
            <ta e="T562" id="Seg_1566" s="T561">life-DAT/LOC</ta>
            <ta e="T563" id="Seg_1567" s="T562">work-EP-NEG.[3SG]</ta>
            <ta e="T564" id="Seg_1568" s="T563">EMPH</ta>
            <ta e="T565" id="Seg_1569" s="T564">that-ACC</ta>
            <ta e="T566" id="Seg_1570" s="T565">work-EP-CAUS-CVB.SIM</ta>
            <ta e="T567" id="Seg_1571" s="T566">try-MED-PRS.[3SG]</ta>
            <ta e="T568" id="Seg_1572" s="T567">different-different-ADVZ</ta>
            <ta e="T570" id="Seg_1573" s="T569">advise-PRS-3PL</ta>
            <ta e="T571" id="Seg_1574" s="T570">AFFIRM</ta>
            <ta e="T574" id="Seg_1575" s="T573">now</ta>
            <ta e="T575" id="Seg_1576" s="T574">EMPH</ta>
            <ta e="T576" id="Seg_1577" s="T575">a.little</ta>
            <ta e="T577" id="Seg_1578" s="T576">reindeer-VBZ-PTCP.PRS</ta>
            <ta e="T578" id="Seg_1579" s="T577">human.being-PL.[NOM]</ta>
            <ta e="T579" id="Seg_1580" s="T578">stay-PRS-3PL</ta>
            <ta e="T580" id="Seg_1581" s="T579">a.little</ta>
            <ta e="T586" id="Seg_1582" s="T585">Sidor</ta>
            <ta e="T587" id="Seg_1583" s="T586">Alekseevich.[NOM]</ta>
            <ta e="T588" id="Seg_1584" s="T587">former</ta>
            <ta e="T589" id="Seg_1585" s="T588">year-PL-DAT/LOC</ta>
            <ta e="T590" id="Seg_1586" s="T589">that</ta>
            <ta e="T591" id="Seg_1587" s="T590">eighty-PROPR</ta>
            <ta e="T592" id="Seg_1588" s="T591">EMPH</ta>
            <ta e="T593" id="Seg_1589" s="T592">year-PL-DAT/LOC</ta>
            <ta e="T594" id="Seg_1590" s="T593">apparently</ta>
            <ta e="T595" id="Seg_1591" s="T594">2PL-ACC</ta>
            <ta e="T596" id="Seg_1592" s="T595">that</ta>
            <ta e="T597" id="Seg_1593" s="T596">deputy.[NOM]</ta>
            <ta e="T598" id="Seg_1594" s="T597">EMPH</ta>
            <ta e="T601" id="Seg_1595" s="T600">say-CVB.SEQ</ta>
            <ta e="T602" id="Seg_1596" s="T601">choose-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T604" id="Seg_1597" s="T602">make-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T605" id="Seg_1598" s="T604">who.[NOM]</ta>
            <ta e="T606" id="Seg_1599" s="T605">eh</ta>
            <ta e="T607" id="Seg_1600" s="T606">wish-3SG-INSTR</ta>
            <ta e="T608" id="Seg_1601" s="T607">how</ta>
            <ta e="T609" id="Seg_1602" s="T608">deputy.[NOM]</ta>
            <ta e="T610" id="Seg_1603" s="T609">say-CVB.SEQ</ta>
            <ta e="T611" id="Seg_1604" s="T610">go.out-EP-PTCP.PST</ta>
            <ta e="T612" id="Seg_1605" s="T611">be-PST1-2PL=Q</ta>
            <ta e="T644" id="Seg_1606" s="T643">that</ta>
            <ta e="T645" id="Seg_1607" s="T644">rayon.[NOM]</ta>
            <ta e="T646" id="Seg_1608" s="T645">head-PL-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_1609" s="T646">see-CVB.SEQ-3PL</ta>
            <ta e="T648" id="Seg_1610" s="T647">apparently</ta>
            <ta e="T649" id="Seg_1611" s="T648">2PL.[NOM]</ta>
            <ta e="T650" id="Seg_1612" s="T649">that</ta>
            <ta e="T651" id="Seg_1613" s="T650">work-2PL-ACC</ta>
            <ta e="T652" id="Seg_1614" s="T651">AFFIRM</ta>
            <ta e="T670" id="Seg_1615" s="T669">then</ta>
            <ta e="T671" id="Seg_1616" s="T670">reindeer-VBZ-PTCP.PRS</ta>
            <ta e="T672" id="Seg_1617" s="T671">human.being-PL.[NOM]</ta>
            <ta e="T673" id="Seg_1618" s="T672">that</ta>
            <ta e="T674" id="Seg_1619" s="T673">report-2PL-ACC</ta>
            <ta e="T675" id="Seg_1620" s="T674">write-EP-IMP.2PL</ta>
            <ta e="T676" id="Seg_1621" s="T675">say-CVB.SEQ</ta>
            <ta e="T677" id="Seg_1622" s="T676">EMPH</ta>
            <ta e="T678" id="Seg_1623" s="T677">beg-EP-RECP/COLL-PTCP.HAB</ta>
            <ta e="T679" id="Seg_1624" s="T678">be-PST1-3PL</ta>
            <ta e="T680" id="Seg_1625" s="T679">AFFIRM</ta>
            <ta e="T681" id="Seg_1626" s="T680">report.[NOM]</ta>
            <ta e="T682" id="Seg_1627" s="T681">EMPH</ta>
            <ta e="T683" id="Seg_1628" s="T682">write-MED-PTCP.HAB</ta>
            <ta e="T684" id="Seg_1629" s="T683">be-PST1-2PL</ta>
            <ta e="T685" id="Seg_1630" s="T684">whole-3SG-ACC</ta>
            <ta e="T686" id="Seg_1631" s="T685">tell-PTCP.HAB</ta>
            <ta e="T687" id="Seg_1632" s="T686">be-PST1-2PL</ta>
            <ta e="T688" id="Seg_1633" s="T687">how.much</ta>
            <ta e="T689" id="Seg_1634" s="T688">reindeer.[NOM]</ta>
            <ta e="T690" id="Seg_1635" s="T689">be.born-PST1-3SG</ta>
            <ta e="T691" id="Seg_1636" s="T690">how.much</ta>
            <ta e="T692" id="Seg_1637" s="T691">be.sick-PST1-3SG</ta>
            <ta e="T693" id="Seg_1638" s="T692">say-CVB.SEQ</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KuNS">
            <ta e="T1" id="Seg_1639" s="T0">Vater-2PL.[NOM]</ta>
            <ta e="T2" id="Seg_1640" s="T1">Mutter-2PL.[NOM]</ta>
            <ta e="T3" id="Seg_1641" s="T2">wer.[NOM]</ta>
            <ta e="T4" id="Seg_1642" s="T3">sein-PST1-3SG=Q</ta>
            <ta e="T5" id="Seg_1643" s="T4">EMPH</ta>
            <ta e="T6" id="Seg_1644" s="T5">dann</ta>
            <ta e="T88" id="Seg_1645" s="T87">Popigaj.[NOM]</ta>
            <ta e="T89" id="Seg_1646" s="T88">Tundra-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_1647" s="T122">Brigade-2PL.[NOM]</ta>
            <ta e="T124" id="Seg_1648" s="T123">nun</ta>
            <ta e="T125" id="Seg_1649" s="T124">was.für.ein</ta>
            <ta e="T126" id="Seg_1650" s="T125">sein-PST1-3SG=Q</ta>
            <ta e="T127" id="Seg_1651" s="T126">Brigade-2PL.[NOM]</ta>
            <ta e="T142" id="Seg_1652" s="T141">jung</ta>
            <ta e="T143" id="Seg_1653" s="T142">Alter-2PL-ABL</ta>
            <ta e="T144" id="Seg_1654" s="T143">dieses</ta>
            <ta e="T145" id="Seg_1655" s="T144">Brigade-DAT/LOC</ta>
            <ta e="T146" id="Seg_1656" s="T145">arbeiten-CVB.SEQ</ta>
            <ta e="T147" id="Seg_1657" s="T146">EMPH</ta>
            <ta e="T152" id="Seg_1658" s="T151">jenes</ta>
            <ta e="T153" id="Seg_1659" s="T152">äh</ta>
            <ta e="T154" id="Seg_1660" s="T153">Alter-PL-DAT/LOC</ta>
            <ta e="T155" id="Seg_1661" s="T154">dann</ta>
            <ta e="T156" id="Seg_1662" s="T155">anders</ta>
            <ta e="T157" id="Seg_1663" s="T156">Mensch-PL.[NOM]</ta>
            <ta e="T158" id="Seg_1664" s="T157">äh</ta>
            <ta e="T159" id="Seg_1665" s="T158">2PL-COMP</ta>
            <ta e="T160" id="Seg_1666" s="T159">alt</ta>
            <ta e="T161" id="Seg_1667" s="T160">Mensch-PL.[NOM]</ta>
            <ta e="T162" id="Seg_1668" s="T161">leiten-PTCP.PRS</ta>
            <ta e="T163" id="Seg_1669" s="T162">sein-PST1-3PL</ta>
            <ta e="T164" id="Seg_1670" s="T163">EMPH</ta>
            <ta e="T165" id="Seg_1671" s="T164">Leben-2PL-ACC</ta>
            <ta e="T166" id="Seg_1672" s="T165">Brigade-2PL-ACC</ta>
            <ta e="T244" id="Seg_1673" s="T243">Sidor</ta>
            <ta e="T245" id="Seg_1674" s="T244">Alekseevič.[NOM]</ta>
            <ta e="T246" id="Seg_1675" s="T245">jenes</ta>
            <ta e="T247" id="Seg_1676" s="T246">zehn</ta>
            <ta e="T248" id="Seg_1677" s="T247">acht</ta>
            <ta e="T249" id="Seg_1678" s="T248">tausend</ta>
            <ta e="T250" id="Seg_1679" s="T249">ungefähr-3SG.[NOM]</ta>
            <ta e="T251" id="Seg_1680" s="T250">Rentier.[NOM]</ta>
            <ta e="T252" id="Seg_1681" s="T251">jenes</ta>
            <ta e="T253" id="Seg_1682" s="T252">wohin</ta>
            <ta e="T254" id="Seg_1683" s="T253">wegwerfen-EP-PASS/REFL-PST1-3SG</ta>
            <ta e="T255" id="Seg_1684" s="T254">wohin</ta>
            <ta e="T256" id="Seg_1685" s="T255">gehen-PST2-3SG=Q</ta>
            <ta e="T289" id="Seg_1686" s="T288">früher</ta>
            <ta e="T290" id="Seg_1687" s="T289">dieses</ta>
            <ta e="T291" id="Seg_1688" s="T290">Wolf-ACC</ta>
            <ta e="T292" id="Seg_1689" s="T291">EMPH</ta>
            <ta e="T293" id="Seg_1690" s="T292">töten-CAUS-PTCP.HAB</ta>
            <ta e="T294" id="Seg_1691" s="T293">sein-PST1-3PL</ta>
            <ta e="T295" id="Seg_1692" s="T294">EMPH</ta>
            <ta e="T296" id="Seg_1693" s="T295">AFFIRM</ta>
            <ta e="T297" id="Seg_1694" s="T296">jetzt</ta>
            <ta e="T298" id="Seg_1695" s="T297">solch.[NOM]</ta>
            <ta e="T299" id="Seg_1696" s="T298">EMPH</ta>
            <ta e="T300" id="Seg_1697" s="T299">NEG.EX</ta>
            <ta e="T301" id="Seg_1698" s="T300">werden-PST2.[3SG]</ta>
            <ta e="T319" id="Seg_1699" s="T317">und</ta>
            <ta e="T347" id="Seg_1700" s="T346">Rentier-AG-PL-DAT/LOC</ta>
            <ta e="T348" id="Seg_1701" s="T347">viel</ta>
            <ta e="T349" id="Seg_1702" s="T348">Geld-ACC</ta>
            <ta e="T350" id="Seg_1703" s="T349">bezahlen-IMP.2PL</ta>
            <ta e="T351" id="Seg_1704" s="T350">sagen-PRS-2PL</ta>
            <ta e="T352" id="Seg_1705" s="T351">EMPH</ta>
            <ta e="T377" id="Seg_1706" s="T376">aufziehen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T378" id="Seg_1707" s="T377">wegen</ta>
            <ta e="T379" id="Seg_1708" s="T378">Q</ta>
            <ta e="T380" id="Seg_1709" s="T379">töten-PTCP.COND-3PL-INSTR</ta>
            <ta e="T416" id="Seg_1710" s="T415">doch</ta>
            <ta e="T417" id="Seg_1711" s="T416">tatsächlich</ta>
            <ta e="T428" id="Seg_1712" s="T427">dort</ta>
            <ta e="T429" id="Seg_1713" s="T428">EMPH</ta>
            <ta e="T430" id="Seg_1714" s="T429">Republik.[NOM]</ta>
            <ta e="T431" id="Seg_1715" s="T430">äh</ta>
            <ta e="T432" id="Seg_1716" s="T431">Abgeordneter-PL-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_1717" s="T432">einzeln</ta>
            <ta e="T434" id="Seg_1718" s="T433">EMPH</ta>
            <ta e="T435" id="Seg_1719" s="T434">Gesetz-PL-ACC</ta>
            <ta e="T436" id="Seg_1720" s="T435">herausnehmen-CVB.SEQ-3PL</ta>
            <ta e="T437" id="Seg_1721" s="T436">schützen-PRS-3PL</ta>
            <ta e="T438" id="Seg_1722" s="T437">offenbar</ta>
            <ta e="T439" id="Seg_1723" s="T438">Rentier-AG.[NOM]</ta>
            <ta e="T440" id="Seg_1724" s="T439">Arbeiter-3SG-ACC</ta>
            <ta e="T441" id="Seg_1725" s="T440">AFFIRM</ta>
            <ta e="T547" id="Seg_1726" s="T546">Geld.[NOM]</ta>
            <ta e="T548" id="Seg_1727" s="T547">machen-PRS-3PL</ta>
            <ta e="T549" id="Seg_1728" s="T548">EMPH</ta>
            <ta e="T552" id="Seg_1729" s="T551">jetzt</ta>
            <ta e="T553" id="Seg_1730" s="T552">äh</ta>
            <ta e="T554" id="Seg_1731" s="T553">neu</ta>
            <ta e="T555" id="Seg_1732" s="T554">Zeit-DAT/LOC</ta>
            <ta e="T556" id="Seg_1733" s="T555">dieses.[NOM]</ta>
            <ta e="T557" id="Seg_1734" s="T556">Markt.[NOM]</ta>
            <ta e="T558" id="Seg_1735" s="T557">Seite-3SG-INSTR</ta>
            <ta e="T559" id="Seg_1736" s="T558">arbeiten-IMP.2PL</ta>
            <ta e="T560" id="Seg_1737" s="T559">sagen-PRS-3PL</ta>
            <ta e="T561" id="Seg_1738" s="T560">jenes-3SG-PL-EP-2SG.[NOM]</ta>
            <ta e="T562" id="Seg_1739" s="T561">Leben-DAT/LOC</ta>
            <ta e="T563" id="Seg_1740" s="T562">klappen-EP-NEG.[3SG]</ta>
            <ta e="T564" id="Seg_1741" s="T563">EMPH</ta>
            <ta e="T565" id="Seg_1742" s="T564">jenes-ACC</ta>
            <ta e="T566" id="Seg_1743" s="T565">klappen-EP-CAUS-CVB.SIM</ta>
            <ta e="T567" id="Seg_1744" s="T566">versuchen-MED-PRS.[3SG]</ta>
            <ta e="T568" id="Seg_1745" s="T567">unterschiedlich-unterschiedlich-ADVZ</ta>
            <ta e="T570" id="Seg_1746" s="T569">raten-PRS-3PL</ta>
            <ta e="T571" id="Seg_1747" s="T570">AFFIRM</ta>
            <ta e="T574" id="Seg_1748" s="T573">jetzt</ta>
            <ta e="T575" id="Seg_1749" s="T574">EMPH</ta>
            <ta e="T576" id="Seg_1750" s="T575">ein.Bisschen</ta>
            <ta e="T577" id="Seg_1751" s="T576">Rentier-VBZ-PTCP.PRS</ta>
            <ta e="T578" id="Seg_1752" s="T577">Mensch-PL.[NOM]</ta>
            <ta e="T579" id="Seg_1753" s="T578">bleiben-PRS-3PL</ta>
            <ta e="T580" id="Seg_1754" s="T579">ein.Bisschen</ta>
            <ta e="T586" id="Seg_1755" s="T585">Sidor</ta>
            <ta e="T587" id="Seg_1756" s="T586">Alekseevič.[NOM]</ta>
            <ta e="T588" id="Seg_1757" s="T587">früher</ta>
            <ta e="T589" id="Seg_1758" s="T588">Jahr-PL-DAT/LOC</ta>
            <ta e="T590" id="Seg_1759" s="T589">dieses</ta>
            <ta e="T591" id="Seg_1760" s="T590">achtzig-PROPR</ta>
            <ta e="T592" id="Seg_1761" s="T591">EMPH</ta>
            <ta e="T593" id="Seg_1762" s="T592">Jahr-PL-DAT/LOC</ta>
            <ta e="T594" id="Seg_1763" s="T593">offenbar</ta>
            <ta e="T595" id="Seg_1764" s="T594">2PL-ACC</ta>
            <ta e="T596" id="Seg_1765" s="T595">dieses</ta>
            <ta e="T597" id="Seg_1766" s="T596">Abgeordneter.[NOM]</ta>
            <ta e="T598" id="Seg_1767" s="T597">EMPH</ta>
            <ta e="T601" id="Seg_1768" s="T600">sagen-CVB.SEQ</ta>
            <ta e="T602" id="Seg_1769" s="T601">wählen-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T604" id="Seg_1770" s="T602">machen-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T605" id="Seg_1771" s="T604">wer.[NOM]</ta>
            <ta e="T606" id="Seg_1772" s="T605">äh</ta>
            <ta e="T607" id="Seg_1773" s="T606">Wunsch-3SG-INSTR</ta>
            <ta e="T608" id="Seg_1774" s="T607">wie</ta>
            <ta e="T609" id="Seg_1775" s="T608">Abgeordneter.[NOM]</ta>
            <ta e="T610" id="Seg_1776" s="T609">sagen-CVB.SEQ</ta>
            <ta e="T611" id="Seg_1777" s="T610">hinausgehen-EP-PTCP.PST</ta>
            <ta e="T612" id="Seg_1778" s="T611">sein-PST1-2PL=Q</ta>
            <ta e="T644" id="Seg_1779" s="T643">dieses</ta>
            <ta e="T645" id="Seg_1780" s="T644">Rajon.[NOM]</ta>
            <ta e="T646" id="Seg_1781" s="T645">Chef-PL-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_1782" s="T646">sehen-CVB.SEQ-3PL</ta>
            <ta e="T648" id="Seg_1783" s="T647">offenbar</ta>
            <ta e="T649" id="Seg_1784" s="T648">2PL.[NOM]</ta>
            <ta e="T650" id="Seg_1785" s="T649">dieses</ta>
            <ta e="T651" id="Seg_1786" s="T650">Arbeit-2PL-ACC</ta>
            <ta e="T652" id="Seg_1787" s="T651">AFFIRM</ta>
            <ta e="T670" id="Seg_1788" s="T669">dann</ta>
            <ta e="T671" id="Seg_1789" s="T670">Rentier-VBZ-PTCP.PRS</ta>
            <ta e="T672" id="Seg_1790" s="T671">Mensch-PL.[NOM]</ta>
            <ta e="T673" id="Seg_1791" s="T672">dieses</ta>
            <ta e="T674" id="Seg_1792" s="T673">Bericht-2PL-ACC</ta>
            <ta e="T675" id="Seg_1793" s="T674">schreiben-EP-IMP.2PL</ta>
            <ta e="T676" id="Seg_1794" s="T675">sagen-CVB.SEQ</ta>
            <ta e="T677" id="Seg_1795" s="T676">EMPH</ta>
            <ta e="T678" id="Seg_1796" s="T677">bitten-EP-RECP/COLL-PTCP.HAB</ta>
            <ta e="T679" id="Seg_1797" s="T678">sein-PST1-3PL</ta>
            <ta e="T680" id="Seg_1798" s="T679">AFFIRM</ta>
            <ta e="T681" id="Seg_1799" s="T680">Bericht.[NOM]</ta>
            <ta e="T682" id="Seg_1800" s="T681">EMPH</ta>
            <ta e="T683" id="Seg_1801" s="T682">schreiben-MED-PTCP.HAB</ta>
            <ta e="T684" id="Seg_1802" s="T683">sein-PST1-2PL</ta>
            <ta e="T685" id="Seg_1803" s="T684">ganz-3SG-ACC</ta>
            <ta e="T686" id="Seg_1804" s="T685">erzählen-PTCP.HAB</ta>
            <ta e="T687" id="Seg_1805" s="T686">sein-PST1-2PL</ta>
            <ta e="T688" id="Seg_1806" s="T687">wie.viel</ta>
            <ta e="T689" id="Seg_1807" s="T688">Rentier.[NOM]</ta>
            <ta e="T690" id="Seg_1808" s="T689">geboren.werden-PST1-3SG</ta>
            <ta e="T691" id="Seg_1809" s="T690">wie.viel</ta>
            <ta e="T692" id="Seg_1810" s="T691">krank.sein-PST1-3SG</ta>
            <ta e="T693" id="Seg_1811" s="T692">sagen-CVB.SEQ</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KuNS">
            <ta e="T1" id="Seg_1812" s="T0">отец-2PL.[NOM]</ta>
            <ta e="T2" id="Seg_1813" s="T1">мать-2PL.[NOM]</ta>
            <ta e="T3" id="Seg_1814" s="T2">кто.[NOM]</ta>
            <ta e="T4" id="Seg_1815" s="T3">быть-PST1-3SG=Q</ta>
            <ta e="T5" id="Seg_1816" s="T4">EMPH</ta>
            <ta e="T6" id="Seg_1817" s="T5">тогда</ta>
            <ta e="T88" id="Seg_1818" s="T87">Попигай.[NOM]</ta>
            <ta e="T89" id="Seg_1819" s="T88">тундра-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_1820" s="T122">бригада-2PL.[NOM]</ta>
            <ta e="T124" id="Seg_1821" s="T123">вот</ta>
            <ta e="T125" id="Seg_1822" s="T124">какой</ta>
            <ta e="T126" id="Seg_1823" s="T125">быть-PST1-3SG=Q</ta>
            <ta e="T127" id="Seg_1824" s="T126">бригада-2PL.[NOM]</ta>
            <ta e="T142" id="Seg_1825" s="T141">молодой</ta>
            <ta e="T143" id="Seg_1826" s="T142">возраст-2PL-ABL</ta>
            <ta e="T144" id="Seg_1827" s="T143">тот</ta>
            <ta e="T145" id="Seg_1828" s="T144">бригада-DAT/LOC</ta>
            <ta e="T146" id="Seg_1829" s="T145">работать-CVB.SEQ</ta>
            <ta e="T147" id="Seg_1830" s="T146">EMPH</ta>
            <ta e="T152" id="Seg_1831" s="T151">тот</ta>
            <ta e="T153" id="Seg_1832" s="T152">ээ</ta>
            <ta e="T154" id="Seg_1833" s="T153">возраст-PL-DAT/LOC</ta>
            <ta e="T155" id="Seg_1834" s="T154">тогда</ta>
            <ta e="T156" id="Seg_1835" s="T155">другой</ta>
            <ta e="T157" id="Seg_1836" s="T156">человек-PL.[NOM]</ta>
            <ta e="T158" id="Seg_1837" s="T157">ээ</ta>
            <ta e="T159" id="Seg_1838" s="T158">2PL-COMP</ta>
            <ta e="T160" id="Seg_1839" s="T159">старый</ta>
            <ta e="T161" id="Seg_1840" s="T160">человек-PL.[NOM]</ta>
            <ta e="T162" id="Seg_1841" s="T161">руководить-PTCP.PRS</ta>
            <ta e="T163" id="Seg_1842" s="T162">быть-PST1-3PL</ta>
            <ta e="T164" id="Seg_1843" s="T163">EMPH</ta>
            <ta e="T165" id="Seg_1844" s="T164">жизнь-2PL-ACC</ta>
            <ta e="T166" id="Seg_1845" s="T165">бригада-2PL-ACC</ta>
            <ta e="T244" id="Seg_1846" s="T243">Сидор</ta>
            <ta e="T245" id="Seg_1847" s="T244">Алексеевич.[NOM]</ta>
            <ta e="T246" id="Seg_1848" s="T245">тот</ta>
            <ta e="T247" id="Seg_1849" s="T246">десять</ta>
            <ta e="T248" id="Seg_1850" s="T247">восемь</ta>
            <ta e="T249" id="Seg_1851" s="T248">тысяча</ta>
            <ta e="T250" id="Seg_1852" s="T249">около-3SG.[NOM]</ta>
            <ta e="T251" id="Seg_1853" s="T250">олень.[NOM]</ta>
            <ta e="T252" id="Seg_1854" s="T251">тот</ta>
            <ta e="T253" id="Seg_1855" s="T252">куда</ta>
            <ta e="T254" id="Seg_1856" s="T253">выбрасывать-EP-PASS/REFL-PST1-3SG</ta>
            <ta e="T255" id="Seg_1857" s="T254">куда</ta>
            <ta e="T256" id="Seg_1858" s="T255">идти-PST2-3SG=Q</ta>
            <ta e="T289" id="Seg_1859" s="T288">раньше</ta>
            <ta e="T290" id="Seg_1860" s="T289">тот</ta>
            <ta e="T291" id="Seg_1861" s="T290">волк-ACC</ta>
            <ta e="T292" id="Seg_1862" s="T291">EMPH</ta>
            <ta e="T293" id="Seg_1863" s="T292">убить-CAUS-PTCP.HAB</ta>
            <ta e="T294" id="Seg_1864" s="T293">быть-PST1-3PL</ta>
            <ta e="T295" id="Seg_1865" s="T294">EMPH</ta>
            <ta e="T296" id="Seg_1866" s="T295">AFFIRM</ta>
            <ta e="T297" id="Seg_1867" s="T296">теперь</ta>
            <ta e="T298" id="Seg_1868" s="T297">такой.[NOM]</ta>
            <ta e="T299" id="Seg_1869" s="T298">EMPH</ta>
            <ta e="T300" id="Seg_1870" s="T299">NEG.EX</ta>
            <ta e="T301" id="Seg_1871" s="T300">становиться-PST2.[3SG]</ta>
            <ta e="T319" id="Seg_1872" s="T317">да</ta>
            <ta e="T347" id="Seg_1873" s="T346">олень-AG-PL-DAT/LOC</ta>
            <ta e="T348" id="Seg_1874" s="T347">много</ta>
            <ta e="T349" id="Seg_1875" s="T348">деньги-ACC</ta>
            <ta e="T350" id="Seg_1876" s="T349">платить-IMP.2PL</ta>
            <ta e="T351" id="Seg_1877" s="T350">говорить-PRS-2PL</ta>
            <ta e="T352" id="Seg_1878" s="T351">EMPH</ta>
            <ta e="T377" id="Seg_1879" s="T376">воспитывать-PTCP.PRS-3PL-ACC</ta>
            <ta e="T378" id="Seg_1880" s="T377">из_за</ta>
            <ta e="T379" id="Seg_1881" s="T378">Q</ta>
            <ta e="T380" id="Seg_1882" s="T379">убить-PTCP.COND-3PL-INSTR</ta>
            <ta e="T416" id="Seg_1883" s="T415">вот</ta>
            <ta e="T417" id="Seg_1884" s="T416">вправду</ta>
            <ta e="T428" id="Seg_1885" s="T427">там</ta>
            <ta e="T429" id="Seg_1886" s="T428">EMPH</ta>
            <ta e="T430" id="Seg_1887" s="T429">республика.[NOM]</ta>
            <ta e="T431" id="Seg_1888" s="T430">ээ</ta>
            <ta e="T432" id="Seg_1889" s="T431">депутат-PL-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_1890" s="T432">отдельно</ta>
            <ta e="T434" id="Seg_1891" s="T433">EMPH</ta>
            <ta e="T435" id="Seg_1892" s="T434">закон-PL-ACC</ta>
            <ta e="T436" id="Seg_1893" s="T435">вынимать-CVB.SEQ-3PL</ta>
            <ta e="T437" id="Seg_1894" s="T436">защищать-PRS-3PL</ta>
            <ta e="T438" id="Seg_1895" s="T437">наверное</ta>
            <ta e="T439" id="Seg_1896" s="T438">олень-AG.[NOM]</ta>
            <ta e="T440" id="Seg_1897" s="T439">работник-3SG-ACC</ta>
            <ta e="T441" id="Seg_1898" s="T440">AFFIRM</ta>
            <ta e="T547" id="Seg_1899" s="T546">деньги.[NOM]</ta>
            <ta e="T548" id="Seg_1900" s="T547">делать-PRS-3PL</ta>
            <ta e="T549" id="Seg_1901" s="T548">EMPH</ta>
            <ta e="T552" id="Seg_1902" s="T551">теперь</ta>
            <ta e="T553" id="Seg_1903" s="T552">э</ta>
            <ta e="T554" id="Seg_1904" s="T553">новый</ta>
            <ta e="T555" id="Seg_1905" s="T554">время-DAT/LOC</ta>
            <ta e="T556" id="Seg_1906" s="T555">тот.[NOM]</ta>
            <ta e="T557" id="Seg_1907" s="T556">рынок.[NOM]</ta>
            <ta e="T558" id="Seg_1908" s="T557">сторона-3SG-INSTR</ta>
            <ta e="T559" id="Seg_1909" s="T558">работать-IMP.2PL</ta>
            <ta e="T560" id="Seg_1910" s="T559">говорить-PRS-3PL</ta>
            <ta e="T561" id="Seg_1911" s="T560">тот-3SG-PL-EP-2SG.[NOM]</ta>
            <ta e="T562" id="Seg_1912" s="T561">жизнь-DAT/LOC</ta>
            <ta e="T563" id="Seg_1913" s="T562">получаться-EP-NEG.[3SG]</ta>
            <ta e="T564" id="Seg_1914" s="T563">EMPH</ta>
            <ta e="T565" id="Seg_1915" s="T564">тот-ACC</ta>
            <ta e="T566" id="Seg_1916" s="T565">получаться-EP-CAUS-CVB.SIM</ta>
            <ta e="T567" id="Seg_1917" s="T566">пытаться-MED-PRS.[3SG]</ta>
            <ta e="T568" id="Seg_1918" s="T567">разный-разный-ADVZ</ta>
            <ta e="T570" id="Seg_1919" s="T569">советовать-PRS-3PL</ta>
            <ta e="T571" id="Seg_1920" s="T570">AFFIRM</ta>
            <ta e="T574" id="Seg_1921" s="T573">теперь</ta>
            <ta e="T575" id="Seg_1922" s="T574">EMPH</ta>
            <ta e="T576" id="Seg_1923" s="T575">немного</ta>
            <ta e="T577" id="Seg_1924" s="T576">олень-VBZ-PTCP.PRS</ta>
            <ta e="T578" id="Seg_1925" s="T577">человек-PL.[NOM]</ta>
            <ta e="T579" id="Seg_1926" s="T578">оставаться-PRS-3PL</ta>
            <ta e="T580" id="Seg_1927" s="T579">немного</ta>
            <ta e="T586" id="Seg_1928" s="T585">Сидор</ta>
            <ta e="T587" id="Seg_1929" s="T586">Алексеевич.[NOM]</ta>
            <ta e="T588" id="Seg_1930" s="T587">прежний</ta>
            <ta e="T589" id="Seg_1931" s="T588">год-PL-DAT/LOC</ta>
            <ta e="T590" id="Seg_1932" s="T589">тот</ta>
            <ta e="T591" id="Seg_1933" s="T590">восемьдесять-PROPR</ta>
            <ta e="T592" id="Seg_1934" s="T591">EMPH</ta>
            <ta e="T593" id="Seg_1935" s="T592">год-PL-DAT/LOC</ta>
            <ta e="T594" id="Seg_1936" s="T593">наверное</ta>
            <ta e="T595" id="Seg_1937" s="T594">2PL-ACC</ta>
            <ta e="T596" id="Seg_1938" s="T595">тот</ta>
            <ta e="T597" id="Seg_1939" s="T596">депутат.[NOM]</ta>
            <ta e="T598" id="Seg_1940" s="T597">EMPH</ta>
            <ta e="T601" id="Seg_1941" s="T600">говорить-CVB.SEQ</ta>
            <ta e="T602" id="Seg_1942" s="T601">выбирать-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T604" id="Seg_1943" s="T602">делать-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T605" id="Seg_1944" s="T604">кто.[NOM]</ta>
            <ta e="T606" id="Seg_1945" s="T605">ээ</ta>
            <ta e="T607" id="Seg_1946" s="T606">желание-3SG-INSTR</ta>
            <ta e="T608" id="Seg_1947" s="T607">как</ta>
            <ta e="T609" id="Seg_1948" s="T608">депутат.[NOM]</ta>
            <ta e="T610" id="Seg_1949" s="T609">говорить-CVB.SEQ</ta>
            <ta e="T611" id="Seg_1950" s="T610">выйти-EP-PTCP.PST</ta>
            <ta e="T612" id="Seg_1951" s="T611">быть-PST1-2PL=Q</ta>
            <ta e="T644" id="Seg_1952" s="T643">тот</ta>
            <ta e="T645" id="Seg_1953" s="T644">район.[NOM]</ta>
            <ta e="T646" id="Seg_1954" s="T645">начальник-PL-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_1955" s="T646">видеть-CVB.SEQ-3PL</ta>
            <ta e="T648" id="Seg_1956" s="T647">очевидно</ta>
            <ta e="T649" id="Seg_1957" s="T648">2PL.[NOM]</ta>
            <ta e="T650" id="Seg_1958" s="T649">тот</ta>
            <ta e="T651" id="Seg_1959" s="T650">работа-2PL-ACC</ta>
            <ta e="T652" id="Seg_1960" s="T651">AFFIRM</ta>
            <ta e="T670" id="Seg_1961" s="T669">тогда</ta>
            <ta e="T671" id="Seg_1962" s="T670">олень-VBZ-PTCP.PRS</ta>
            <ta e="T672" id="Seg_1963" s="T671">человек-PL.[NOM]</ta>
            <ta e="T673" id="Seg_1964" s="T672">тот</ta>
            <ta e="T674" id="Seg_1965" s="T673">отчет-2PL-ACC</ta>
            <ta e="T675" id="Seg_1966" s="T674">писать-EP-IMP.2PL</ta>
            <ta e="T676" id="Seg_1967" s="T675">говорить-CVB.SEQ</ta>
            <ta e="T677" id="Seg_1968" s="T676">EMPH</ta>
            <ta e="T678" id="Seg_1969" s="T677">попросить-EP-RECP/COLL-PTCP.HAB</ta>
            <ta e="T679" id="Seg_1970" s="T678">быть-PST1-3PL</ta>
            <ta e="T680" id="Seg_1971" s="T679">AFFIRM</ta>
            <ta e="T681" id="Seg_1972" s="T680">отчет.[NOM]</ta>
            <ta e="T682" id="Seg_1973" s="T681">EMPH</ta>
            <ta e="T683" id="Seg_1974" s="T682">писать-MED-PTCP.HAB</ta>
            <ta e="T684" id="Seg_1975" s="T683">быть-PST1-2PL</ta>
            <ta e="T685" id="Seg_1976" s="T684">целый-3SG-ACC</ta>
            <ta e="T686" id="Seg_1977" s="T685">рассказывать-PTCP.HAB</ta>
            <ta e="T687" id="Seg_1978" s="T686">быть-PST1-2PL</ta>
            <ta e="T688" id="Seg_1979" s="T687">сколько</ta>
            <ta e="T689" id="Seg_1980" s="T688">олень.[NOM]</ta>
            <ta e="T690" id="Seg_1981" s="T689">родиться-PST1-3SG</ta>
            <ta e="T691" id="Seg_1982" s="T690">сколько</ta>
            <ta e="T692" id="Seg_1983" s="T691">быть.больным-PST1-3SG</ta>
            <ta e="T693" id="Seg_1984" s="T692">говорить-CVB.SEQ</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KuNS">
            <ta e="T1" id="Seg_1985" s="T0">n-n:(poss)-n:case</ta>
            <ta e="T2" id="Seg_1986" s="T1">n-n:(poss)-n:case</ta>
            <ta e="T3" id="Seg_1987" s="T2">que-pro:case</ta>
            <ta e="T4" id="Seg_1988" s="T3">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T5" id="Seg_1989" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_1990" s="T5">adv</ta>
            <ta e="T88" id="Seg_1991" s="T87">propr-n:case</ta>
            <ta e="T89" id="Seg_1992" s="T88">n-n:poss-n:case</ta>
            <ta e="T123" id="Seg_1993" s="T122">n-n:(poss)-n:case</ta>
            <ta e="T124" id="Seg_1994" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_1995" s="T124">que</ta>
            <ta e="T126" id="Seg_1996" s="T125">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T127" id="Seg_1997" s="T126">n-n:(poss)-n:case</ta>
            <ta e="T142" id="Seg_1998" s="T141">adj</ta>
            <ta e="T143" id="Seg_1999" s="T142">n-n:poss-n:case</ta>
            <ta e="T144" id="Seg_2000" s="T143">dempro</ta>
            <ta e="T145" id="Seg_2001" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_2002" s="T145">v-v:cvb</ta>
            <ta e="T147" id="Seg_2003" s="T146">ptcl</ta>
            <ta e="T152" id="Seg_2004" s="T151">dempro</ta>
            <ta e="T153" id="Seg_2005" s="T152">interj</ta>
            <ta e="T154" id="Seg_2006" s="T153">n-n:(num)-n:case</ta>
            <ta e="T155" id="Seg_2007" s="T154">adv</ta>
            <ta e="T156" id="Seg_2008" s="T155">adj</ta>
            <ta e="T157" id="Seg_2009" s="T156">n-n:(num)-n:case</ta>
            <ta e="T158" id="Seg_2010" s="T157">interj</ta>
            <ta e="T159" id="Seg_2011" s="T158">pers-pro:case</ta>
            <ta e="T160" id="Seg_2012" s="T159">adj</ta>
            <ta e="T161" id="Seg_2013" s="T160">n-n:(num)-n:case</ta>
            <ta e="T162" id="Seg_2014" s="T161">v-v:ptcp</ta>
            <ta e="T163" id="Seg_2015" s="T162">v-v:tense-v:poss.pn</ta>
            <ta e="T164" id="Seg_2016" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_2017" s="T164">n-n:poss-n:case</ta>
            <ta e="T166" id="Seg_2018" s="T165">n-n:poss-n:case</ta>
            <ta e="T244" id="Seg_2019" s="T243">propr</ta>
            <ta e="T245" id="Seg_2020" s="T244">propr-n:case</ta>
            <ta e="T246" id="Seg_2021" s="T245">dempro</ta>
            <ta e="T247" id="Seg_2022" s="T246">cardnum</ta>
            <ta e="T248" id="Seg_2023" s="T247">cardnum</ta>
            <ta e="T249" id="Seg_2024" s="T248">cardnum</ta>
            <ta e="T250" id="Seg_2025" s="T249">post-n:(poss)-n:case</ta>
            <ta e="T251" id="Seg_2026" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_2027" s="T251">dempro</ta>
            <ta e="T253" id="Seg_2028" s="T252">que</ta>
            <ta e="T254" id="Seg_2029" s="T253">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T255" id="Seg_2030" s="T254">que</ta>
            <ta e="T256" id="Seg_2031" s="T255">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T289" id="Seg_2032" s="T288">adv</ta>
            <ta e="T290" id="Seg_2033" s="T289">dempro</ta>
            <ta e="T291" id="Seg_2034" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_2035" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_2036" s="T292">v-v&gt;v-v:ptcp</ta>
            <ta e="T294" id="Seg_2037" s="T293">v-v:tense-v:poss.pn</ta>
            <ta e="T295" id="Seg_2038" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_2039" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_2040" s="T296">adv</ta>
            <ta e="T298" id="Seg_2041" s="T297">dempro-pro:case</ta>
            <ta e="T299" id="Seg_2042" s="T298">ptcl</ta>
            <ta e="T300" id="Seg_2043" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_2044" s="T300">v-v:tense-v:pred.pn</ta>
            <ta e="T319" id="Seg_2045" s="T317">conj</ta>
            <ta e="T347" id="Seg_2046" s="T346">n-n&gt;n-n:(num)-n:case</ta>
            <ta e="T348" id="Seg_2047" s="T347">quant</ta>
            <ta e="T349" id="Seg_2048" s="T348">n-n:case</ta>
            <ta e="T350" id="Seg_2049" s="T349">v-v:mood.pn</ta>
            <ta e="T351" id="Seg_2050" s="T350">v-v:tense-v:pred.pn</ta>
            <ta e="T352" id="Seg_2051" s="T351">ptcl</ta>
            <ta e="T377" id="Seg_2052" s="T376">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T378" id="Seg_2053" s="T377">post</ta>
            <ta e="T379" id="Seg_2054" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_2055" s="T379">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T416" id="Seg_2056" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_2057" s="T416">adv</ta>
            <ta e="T428" id="Seg_2058" s="T427">adv</ta>
            <ta e="T429" id="Seg_2059" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_2060" s="T429">n-n:case</ta>
            <ta e="T431" id="Seg_2061" s="T430">interj</ta>
            <ta e="T432" id="Seg_2062" s="T431">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T433" id="Seg_2063" s="T432">adv</ta>
            <ta e="T434" id="Seg_2064" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_2065" s="T434">n-n:(num)-n:case</ta>
            <ta e="T436" id="Seg_2066" s="T435">v-v:cvb-v:pred.pn</ta>
            <ta e="T437" id="Seg_2067" s="T436">v-v:tense-v:pred.pn</ta>
            <ta e="T438" id="Seg_2068" s="T437">adv</ta>
            <ta e="T439" id="Seg_2069" s="T438">n-n&gt;n-n:case</ta>
            <ta e="T440" id="Seg_2070" s="T439">n-n:poss-n:case</ta>
            <ta e="T441" id="Seg_2071" s="T440">ptcl</ta>
            <ta e="T547" id="Seg_2072" s="T546">n-n:case</ta>
            <ta e="T548" id="Seg_2073" s="T547">v-v:tense-v:pred.pn</ta>
            <ta e="T549" id="Seg_2074" s="T548">ptcl</ta>
            <ta e="T552" id="Seg_2075" s="T551">adv</ta>
            <ta e="T553" id="Seg_2076" s="T552">interj</ta>
            <ta e="T554" id="Seg_2077" s="T553">adj</ta>
            <ta e="T555" id="Seg_2078" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_2079" s="T555">dempro-pro:case</ta>
            <ta e="T557" id="Seg_2080" s="T556">n-n:case</ta>
            <ta e="T558" id="Seg_2081" s="T557">n-n:poss-n:case</ta>
            <ta e="T559" id="Seg_2082" s="T558">v-v:mood.pn</ta>
            <ta e="T560" id="Seg_2083" s="T559">v-v:tense-v:pred.pn</ta>
            <ta e="T561" id="Seg_2084" s="T560">dempro-pro:(poss)-pro:(num)-pro:(ins)-pro:(poss)-pro:case</ta>
            <ta e="T562" id="Seg_2085" s="T561">n-n:case</ta>
            <ta e="T563" id="Seg_2086" s="T562">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T564" id="Seg_2087" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_2088" s="T564">dempro-pro:case</ta>
            <ta e="T566" id="Seg_2089" s="T565">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T567" id="Seg_2090" s="T566">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T568" id="Seg_2091" s="T567">adj-adj-adj&gt;adv</ta>
            <ta e="T570" id="Seg_2092" s="T569">v-v:tense-v:pred.pn</ta>
            <ta e="T571" id="Seg_2093" s="T570">ptcl</ta>
            <ta e="T574" id="Seg_2094" s="T573">adv</ta>
            <ta e="T575" id="Seg_2095" s="T574">ptcl</ta>
            <ta e="T576" id="Seg_2096" s="T575">quant</ta>
            <ta e="T577" id="Seg_2097" s="T576">n-n&gt;v-v:ptcp</ta>
            <ta e="T578" id="Seg_2098" s="T577">n-n:(num)-n:case</ta>
            <ta e="T579" id="Seg_2099" s="T578">v-v:tense-v:pred.pn</ta>
            <ta e="T580" id="Seg_2100" s="T579">quant</ta>
            <ta e="T586" id="Seg_2101" s="T585">propr</ta>
            <ta e="T587" id="Seg_2102" s="T586">propr-n:case</ta>
            <ta e="T588" id="Seg_2103" s="T587">adj</ta>
            <ta e="T589" id="Seg_2104" s="T588">n-n:(num)-n:case</ta>
            <ta e="T590" id="Seg_2105" s="T589">dempro</ta>
            <ta e="T591" id="Seg_2106" s="T590">cardnum-n&gt;adj</ta>
            <ta e="T592" id="Seg_2107" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_2108" s="T592">n-n:(num)-n:case</ta>
            <ta e="T594" id="Seg_2109" s="T593">adv</ta>
            <ta e="T595" id="Seg_2110" s="T594">pers-pro:case</ta>
            <ta e="T596" id="Seg_2111" s="T595">dempro</ta>
            <ta e="T597" id="Seg_2112" s="T596">n-n:case</ta>
            <ta e="T598" id="Seg_2113" s="T597">ptcl</ta>
            <ta e="T601" id="Seg_2114" s="T600">v-v:cvb</ta>
            <ta e="T602" id="Seg_2115" s="T601">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T604" id="Seg_2116" s="T602">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T605" id="Seg_2117" s="T604">que-pro:case</ta>
            <ta e="T606" id="Seg_2118" s="T605">interj</ta>
            <ta e="T607" id="Seg_2119" s="T606">n-n:poss-n:case</ta>
            <ta e="T608" id="Seg_2120" s="T607">que</ta>
            <ta e="T609" id="Seg_2121" s="T608">n-n:case</ta>
            <ta e="T610" id="Seg_2122" s="T609">v-v:cvb</ta>
            <ta e="T611" id="Seg_2123" s="T610">v-v:(ins)-v:ptcp</ta>
            <ta e="T612" id="Seg_2124" s="T611">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T644" id="Seg_2125" s="T643">dempro</ta>
            <ta e="T645" id="Seg_2126" s="T644">n-n:case</ta>
            <ta e="T646" id="Seg_2127" s="T645">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T647" id="Seg_2128" s="T646">v-v:cvb-v:pred.pn</ta>
            <ta e="T648" id="Seg_2129" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_2130" s="T648">pers-pro:case</ta>
            <ta e="T650" id="Seg_2131" s="T649">dempro</ta>
            <ta e="T651" id="Seg_2132" s="T650">n-n:poss-n:case</ta>
            <ta e="T652" id="Seg_2133" s="T651">ptcl</ta>
            <ta e="T670" id="Seg_2134" s="T669">adv</ta>
            <ta e="T671" id="Seg_2135" s="T670">n-n&gt;v-v:ptcp</ta>
            <ta e="T672" id="Seg_2136" s="T671">n-n:(num)-n:case</ta>
            <ta e="T673" id="Seg_2137" s="T672">dempro</ta>
            <ta e="T674" id="Seg_2138" s="T673">n-n:poss-n:case</ta>
            <ta e="T675" id="Seg_2139" s="T674">v-v:(ins)-v:mood.pn</ta>
            <ta e="T676" id="Seg_2140" s="T675">v-v:cvb</ta>
            <ta e="T677" id="Seg_2141" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_2142" s="T677">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T679" id="Seg_2143" s="T678">v-v:tense-v:pred.pn</ta>
            <ta e="T680" id="Seg_2144" s="T679">ptcl</ta>
            <ta e="T681" id="Seg_2145" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_2146" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_2147" s="T682">v-v&gt;v-v:ptcp</ta>
            <ta e="T684" id="Seg_2148" s="T683">v-v:tense-v:poss.pn</ta>
            <ta e="T685" id="Seg_2149" s="T684">adj-n:poss-n:case</ta>
            <ta e="T686" id="Seg_2150" s="T685">v-v:ptcp</ta>
            <ta e="T687" id="Seg_2151" s="T686">v-v:tense-v:poss.pn</ta>
            <ta e="T688" id="Seg_2152" s="T687">que</ta>
            <ta e="T689" id="Seg_2153" s="T688">n-n:case</ta>
            <ta e="T690" id="Seg_2154" s="T689">v-v:tense-v:poss.pn</ta>
            <ta e="T691" id="Seg_2155" s="T690">que</ta>
            <ta e="T692" id="Seg_2156" s="T691">v-v:tense-v:poss.pn</ta>
            <ta e="T693" id="Seg_2157" s="T692">v-v:cvb</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KuNS">
            <ta e="T1" id="Seg_2158" s="T0">n</ta>
            <ta e="T2" id="Seg_2159" s="T1">n</ta>
            <ta e="T3" id="Seg_2160" s="T2">que</ta>
            <ta e="T4" id="Seg_2161" s="T3">cop</ta>
            <ta e="T5" id="Seg_2162" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_2163" s="T5">adv</ta>
            <ta e="T88" id="Seg_2164" s="T87">propr</ta>
            <ta e="T89" id="Seg_2165" s="T88">n</ta>
            <ta e="T123" id="Seg_2166" s="T122">n</ta>
            <ta e="T124" id="Seg_2167" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_2168" s="T124">que</ta>
            <ta e="T126" id="Seg_2169" s="T125">cop</ta>
            <ta e="T127" id="Seg_2170" s="T126">n</ta>
            <ta e="T142" id="Seg_2171" s="T141">adj</ta>
            <ta e="T143" id="Seg_2172" s="T142">n</ta>
            <ta e="T144" id="Seg_2173" s="T143">dempro</ta>
            <ta e="T145" id="Seg_2174" s="T144">n</ta>
            <ta e="T146" id="Seg_2175" s="T145">v</ta>
            <ta e="T147" id="Seg_2176" s="T146">ptcl</ta>
            <ta e="T152" id="Seg_2177" s="T151">dempro</ta>
            <ta e="T153" id="Seg_2178" s="T152">interj</ta>
            <ta e="T154" id="Seg_2179" s="T153">n</ta>
            <ta e="T155" id="Seg_2180" s="T154">adv</ta>
            <ta e="T156" id="Seg_2181" s="T155">adj</ta>
            <ta e="T157" id="Seg_2182" s="T156">n</ta>
            <ta e="T158" id="Seg_2183" s="T157">interj</ta>
            <ta e="T159" id="Seg_2184" s="T158">pers</ta>
            <ta e="T160" id="Seg_2185" s="T159">adj</ta>
            <ta e="T161" id="Seg_2186" s="T160">n</ta>
            <ta e="T162" id="Seg_2187" s="T161">v</ta>
            <ta e="T163" id="Seg_2188" s="T162">aux</ta>
            <ta e="T164" id="Seg_2189" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_2190" s="T164">n</ta>
            <ta e="T166" id="Seg_2191" s="T165">n</ta>
            <ta e="T245" id="Seg_2192" s="T244">propr</ta>
            <ta e="T246" id="Seg_2193" s="T245">dempro</ta>
            <ta e="T247" id="Seg_2194" s="T246">cardnum</ta>
            <ta e="T248" id="Seg_2195" s="T247">cardnum</ta>
            <ta e="T249" id="Seg_2196" s="T248">cardnum</ta>
            <ta e="T250" id="Seg_2197" s="T249">post</ta>
            <ta e="T251" id="Seg_2198" s="T250">n</ta>
            <ta e="T252" id="Seg_2199" s="T251">dempro</ta>
            <ta e="T253" id="Seg_2200" s="T252">que</ta>
            <ta e="T254" id="Seg_2201" s="T253">v</ta>
            <ta e="T255" id="Seg_2202" s="T254">que</ta>
            <ta e="T256" id="Seg_2203" s="T255">v</ta>
            <ta e="T289" id="Seg_2204" s="T288">adv</ta>
            <ta e="T290" id="Seg_2205" s="T289">dempro</ta>
            <ta e="T291" id="Seg_2206" s="T290">n</ta>
            <ta e="T292" id="Seg_2207" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_2208" s="T292">v</ta>
            <ta e="T294" id="Seg_2209" s="T293">aux</ta>
            <ta e="T295" id="Seg_2210" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_2211" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_2212" s="T296">adv</ta>
            <ta e="T298" id="Seg_2213" s="T297">dempro</ta>
            <ta e="T299" id="Seg_2214" s="T298">ptcl</ta>
            <ta e="T300" id="Seg_2215" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_2216" s="T300">cop</ta>
            <ta e="T319" id="Seg_2217" s="T317">conj</ta>
            <ta e="T347" id="Seg_2218" s="T346">n</ta>
            <ta e="T348" id="Seg_2219" s="T347">quant</ta>
            <ta e="T349" id="Seg_2220" s="T348">n</ta>
            <ta e="T350" id="Seg_2221" s="T349">v</ta>
            <ta e="T351" id="Seg_2222" s="T350">v</ta>
            <ta e="T352" id="Seg_2223" s="T351">ptcl</ta>
            <ta e="T377" id="Seg_2224" s="T376">v</ta>
            <ta e="T378" id="Seg_2225" s="T377">post</ta>
            <ta e="T379" id="Seg_2226" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_2227" s="T379">v</ta>
            <ta e="T416" id="Seg_2228" s="T415">ptcl</ta>
            <ta e="T417" id="Seg_2229" s="T416">adv</ta>
            <ta e="T428" id="Seg_2230" s="T427">adv</ta>
            <ta e="T429" id="Seg_2231" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_2232" s="T429">n</ta>
            <ta e="T431" id="Seg_2233" s="T430">interj</ta>
            <ta e="T432" id="Seg_2234" s="T431">n</ta>
            <ta e="T433" id="Seg_2235" s="T432">adv</ta>
            <ta e="T434" id="Seg_2236" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_2237" s="T434">n</ta>
            <ta e="T436" id="Seg_2238" s="T435">v</ta>
            <ta e="T437" id="Seg_2239" s="T436">v</ta>
            <ta e="T438" id="Seg_2240" s="T437">adv</ta>
            <ta e="T439" id="Seg_2241" s="T438">n</ta>
            <ta e="T440" id="Seg_2242" s="T439">n</ta>
            <ta e="T441" id="Seg_2243" s="T440">ptcl</ta>
            <ta e="T547" id="Seg_2244" s="T546">n</ta>
            <ta e="T548" id="Seg_2245" s="T547">v</ta>
            <ta e="T549" id="Seg_2246" s="T548">ptcl</ta>
            <ta e="T552" id="Seg_2247" s="T551">adv</ta>
            <ta e="T553" id="Seg_2248" s="T552">interj</ta>
            <ta e="T554" id="Seg_2249" s="T553">adj</ta>
            <ta e="T555" id="Seg_2250" s="T554">n</ta>
            <ta e="T556" id="Seg_2251" s="T555">dempro</ta>
            <ta e="T557" id="Seg_2252" s="T556">n</ta>
            <ta e="T558" id="Seg_2253" s="T557">n</ta>
            <ta e="T559" id="Seg_2254" s="T558">v</ta>
            <ta e="T560" id="Seg_2255" s="T559">v</ta>
            <ta e="T561" id="Seg_2256" s="T560">dempro</ta>
            <ta e="T562" id="Seg_2257" s="T561">n</ta>
            <ta e="T563" id="Seg_2258" s="T562">v</ta>
            <ta e="T564" id="Seg_2259" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_2260" s="T564">dempro</ta>
            <ta e="T566" id="Seg_2261" s="T565">v</ta>
            <ta e="T567" id="Seg_2262" s="T566">v</ta>
            <ta e="T568" id="Seg_2263" s="T567">adv</ta>
            <ta e="T570" id="Seg_2264" s="T569">v</ta>
            <ta e="T571" id="Seg_2265" s="T570">ptcl</ta>
            <ta e="T574" id="Seg_2266" s="T573">adv</ta>
            <ta e="T575" id="Seg_2267" s="T574">ptcl</ta>
            <ta e="T576" id="Seg_2268" s="T575">quant</ta>
            <ta e="T577" id="Seg_2269" s="T576">v</ta>
            <ta e="T578" id="Seg_2270" s="T577">n</ta>
            <ta e="T579" id="Seg_2271" s="T578">v</ta>
            <ta e="T580" id="Seg_2272" s="T579">quant</ta>
            <ta e="T586" id="Seg_2273" s="T585">propr</ta>
            <ta e="T587" id="Seg_2274" s="T586">propr</ta>
            <ta e="T588" id="Seg_2275" s="T587">adj</ta>
            <ta e="T589" id="Seg_2276" s="T588">n</ta>
            <ta e="T590" id="Seg_2277" s="T589">dempro</ta>
            <ta e="T591" id="Seg_2278" s="T590">adj</ta>
            <ta e="T592" id="Seg_2279" s="T591">ptcl</ta>
            <ta e="T593" id="Seg_2280" s="T592">n</ta>
            <ta e="T594" id="Seg_2281" s="T593">adv</ta>
            <ta e="T595" id="Seg_2282" s="T594">pers</ta>
            <ta e="T596" id="Seg_2283" s="T595">dempro</ta>
            <ta e="T597" id="Seg_2284" s="T596">n</ta>
            <ta e="T598" id="Seg_2285" s="T597">ptcl</ta>
            <ta e="T601" id="Seg_2286" s="T600">v</ta>
            <ta e="T602" id="Seg_2287" s="T601">v</ta>
            <ta e="T604" id="Seg_2288" s="T602">v</ta>
            <ta e="T605" id="Seg_2289" s="T604">que</ta>
            <ta e="T606" id="Seg_2290" s="T605">interj</ta>
            <ta e="T607" id="Seg_2291" s="T606">n</ta>
            <ta e="T608" id="Seg_2292" s="T607">que</ta>
            <ta e="T609" id="Seg_2293" s="T608">n</ta>
            <ta e="T610" id="Seg_2294" s="T609">v</ta>
            <ta e="T611" id="Seg_2295" s="T610">v</ta>
            <ta e="T612" id="Seg_2296" s="T611">aux</ta>
            <ta e="T644" id="Seg_2297" s="T643">dempro</ta>
            <ta e="T645" id="Seg_2298" s="T644">n</ta>
            <ta e="T646" id="Seg_2299" s="T645">n</ta>
            <ta e="T647" id="Seg_2300" s="T646">v</ta>
            <ta e="T648" id="Seg_2301" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_2302" s="T648">pers</ta>
            <ta e="T650" id="Seg_2303" s="T649">dempro</ta>
            <ta e="T651" id="Seg_2304" s="T650">n</ta>
            <ta e="T652" id="Seg_2305" s="T651">ptcl</ta>
            <ta e="T670" id="Seg_2306" s="T669">adv</ta>
            <ta e="T671" id="Seg_2307" s="T670">v</ta>
            <ta e="T672" id="Seg_2308" s="T671">n</ta>
            <ta e="T673" id="Seg_2309" s="T672">dempro</ta>
            <ta e="T674" id="Seg_2310" s="T673">n</ta>
            <ta e="T675" id="Seg_2311" s="T674">v</ta>
            <ta e="T676" id="Seg_2312" s="T675">v</ta>
            <ta e="T677" id="Seg_2313" s="T676">ptcl</ta>
            <ta e="T678" id="Seg_2314" s="T677">v</ta>
            <ta e="T679" id="Seg_2315" s="T678">aux</ta>
            <ta e="T680" id="Seg_2316" s="T679">ptcl</ta>
            <ta e="T681" id="Seg_2317" s="T680">n</ta>
            <ta e="T682" id="Seg_2318" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_2319" s="T682">v</ta>
            <ta e="T684" id="Seg_2320" s="T683">aux</ta>
            <ta e="T685" id="Seg_2321" s="T684">adj</ta>
            <ta e="T686" id="Seg_2322" s="T685">v</ta>
            <ta e="T687" id="Seg_2323" s="T686">aux</ta>
            <ta e="T688" id="Seg_2324" s="T687">que</ta>
            <ta e="T689" id="Seg_2325" s="T688">n</ta>
            <ta e="T690" id="Seg_2326" s="T689">v</ta>
            <ta e="T691" id="Seg_2327" s="T690">que</ta>
            <ta e="T692" id="Seg_2328" s="T691">v</ta>
            <ta e="T693" id="Seg_2329" s="T692">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KuNS">
            <ta e="T1" id="Seg_2330" s="T0">0.2.h:Poss np.h:Th</ta>
            <ta e="T2" id="Seg_2331" s="T1">0.2.h:Poss np.h:Th</ta>
            <ta e="T88" id="Seg_2332" s="T87">np:Poss</ta>
            <ta e="T89" id="Seg_2333" s="T88">np:L</ta>
            <ta e="T123" id="Seg_2334" s="T122">0.2.h:Poss np:Th</ta>
            <ta e="T127" id="Seg_2335" s="T126">0.2.h:Poss np:Th</ta>
            <ta e="T143" id="Seg_2336" s="T142">n:Time</ta>
            <ta e="T145" id="Seg_2337" s="T144">np:L</ta>
            <ta e="T154" id="Seg_2338" s="T153">n:Time</ta>
            <ta e="T157" id="Seg_2339" s="T156">np.h:A</ta>
            <ta e="T161" id="Seg_2340" s="T160">np.h:A</ta>
            <ta e="T165" id="Seg_2341" s="T164">0.2.h:Poss np:Th</ta>
            <ta e="T166" id="Seg_2342" s="T165">0.2.h:Poss np:Th</ta>
            <ta e="T251" id="Seg_2343" s="T250">np:Th</ta>
            <ta e="T253" id="Seg_2344" s="T252">pro:G</ta>
            <ta e="T255" id="Seg_2345" s="T254">np:Th</ta>
            <ta e="T289" id="Seg_2346" s="T288">adv:Time</ta>
            <ta e="T291" id="Seg_2347" s="T290">np:P</ta>
            <ta e="T294" id="Seg_2348" s="T293">0.3.h:A</ta>
            <ta e="T297" id="Seg_2349" s="T296">adv:Time</ta>
            <ta e="T298" id="Seg_2350" s="T297">pro:Th</ta>
            <ta e="T347" id="Seg_2351" s="T346">np.h:R</ta>
            <ta e="T349" id="Seg_2352" s="T348">np:Th</ta>
            <ta e="T350" id="Seg_2353" s="T349">0.2.h:A</ta>
            <ta e="T351" id="Seg_2354" s="T350">0.2.h:A</ta>
            <ta e="T377" id="Seg_2355" s="T376">0.3.h:A</ta>
            <ta e="T380" id="Seg_2356" s="T379">0.3.h:A</ta>
            <ta e="T430" id="Seg_2357" s="T429">np:Poss</ta>
            <ta e="T432" id="Seg_2358" s="T431">np.h:A</ta>
            <ta e="T435" id="Seg_2359" s="T434">np:Th</ta>
            <ta e="T436" id="Seg_2360" s="T435">0.3.h:A</ta>
            <ta e="T440" id="Seg_2361" s="T439">np.h:Th</ta>
            <ta e="T547" id="Seg_2362" s="T546">np:Th</ta>
            <ta e="T548" id="Seg_2363" s="T547">0.3.h:A</ta>
            <ta e="T555" id="Seg_2364" s="T554">n:Time</ta>
            <ta e="T559" id="Seg_2365" s="T558">0.2.h:A</ta>
            <ta e="T560" id="Seg_2366" s="T559">0.3.h:A</ta>
            <ta e="T570" id="Seg_2367" s="T569">0.3.h:A</ta>
            <ta e="T578" id="Seg_2368" s="T577">np.h:Th</ta>
            <ta e="T589" id="Seg_2369" s="T588">n:Time</ta>
            <ta e="T593" id="Seg_2370" s="T592">n:Time</ta>
            <ta e="T595" id="Seg_2371" s="T594">pro.h:Th</ta>
            <ta e="T602" id="Seg_2372" s="T601">0.3.h:A</ta>
            <ta e="T612" id="Seg_2373" s="T611">0.2.h:Th</ta>
            <ta e="T645" id="Seg_2374" s="T644">np:Poss</ta>
            <ta e="T646" id="Seg_2375" s="T645">np.h:E</ta>
            <ta e="T649" id="Seg_2376" s="T648">pro.h:Poss</ta>
            <ta e="T651" id="Seg_2377" s="T650">np:St</ta>
            <ta e="T672" id="Seg_2378" s="T671">np.h:A</ta>
            <ta e="T674" id="Seg_2379" s="T673">0.2.h:Poss np:P</ta>
            <ta e="T675" id="Seg_2380" s="T674">0.2.h:A</ta>
            <ta e="T681" id="Seg_2381" s="T680">np:P</ta>
            <ta e="T684" id="Seg_2382" s="T683">0.2.h:A</ta>
            <ta e="T685" id="Seg_2383" s="T684">np:Th</ta>
            <ta e="T687" id="Seg_2384" s="T686">0.2.h:A</ta>
            <ta e="T689" id="Seg_2385" s="T688">np:Th</ta>
            <ta e="T691" id="Seg_2386" s="T690">pro:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-KuNS">
            <ta e="T1" id="Seg_2387" s="T0">np.h:S</ta>
            <ta e="T2" id="Seg_2388" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_2389" s="T2">pro:pred</ta>
            <ta e="T4" id="Seg_2390" s="T3">cop</ta>
            <ta e="T123" id="Seg_2391" s="T122">np:S</ta>
            <ta e="T125" id="Seg_2392" s="T124">pro:pred</ta>
            <ta e="T126" id="Seg_2393" s="T125">cop</ta>
            <ta e="T146" id="Seg_2394" s="T141">s:adv</ta>
            <ta e="T157" id="Seg_2395" s="T156">np.h:S</ta>
            <ta e="T161" id="Seg_2396" s="T160">np.h:S</ta>
            <ta e="T163" id="Seg_2397" s="T162">v:pred</ta>
            <ta e="T165" id="Seg_2398" s="T164">np:O</ta>
            <ta e="T166" id="Seg_2399" s="T165">np:O</ta>
            <ta e="T251" id="Seg_2400" s="T250">np:S</ta>
            <ta e="T254" id="Seg_2401" s="T253">v:pred</ta>
            <ta e="T255" id="Seg_2402" s="T254">np:S</ta>
            <ta e="T256" id="Seg_2403" s="T255">v:pred</ta>
            <ta e="T291" id="Seg_2404" s="T290">np:O</ta>
            <ta e="T294" id="Seg_2405" s="T293">0.3.h:S v:pred</ta>
            <ta e="T298" id="Seg_2406" s="T297">pro:S</ta>
            <ta e="T300" id="Seg_2407" s="T299">ptcl:pred</ta>
            <ta e="T301" id="Seg_2408" s="T300">cop</ta>
            <ta e="T349" id="Seg_2409" s="T348">np:O</ta>
            <ta e="T350" id="Seg_2410" s="T349">0.2.h:S v:pred</ta>
            <ta e="T351" id="Seg_2411" s="T350">0.2.h:S v:pred</ta>
            <ta e="T378" id="Seg_2412" s="T376">s:adv</ta>
            <ta e="T380" id="Seg_2413" s="T379">s:adv</ta>
            <ta e="T432" id="Seg_2414" s="T431">np.h:S</ta>
            <ta e="T436" id="Seg_2415" s="T432">s:adv</ta>
            <ta e="T437" id="Seg_2416" s="T436">v:pred</ta>
            <ta e="T440" id="Seg_2417" s="T439">np.h:O</ta>
            <ta e="T547" id="Seg_2418" s="T546">np:O</ta>
            <ta e="T548" id="Seg_2419" s="T547">0.3.h:S v:pred</ta>
            <ta e="T559" id="Seg_2420" s="T558">0.2.h:S v:pred</ta>
            <ta e="T560" id="Seg_2421" s="T559">0.3.h:S v:pred</ta>
            <ta e="T570" id="Seg_2422" s="T569">0.3.h:S v:pred</ta>
            <ta e="T578" id="Seg_2423" s="T577">np.h:S</ta>
            <ta e="T579" id="Seg_2424" s="T578">v:pred</ta>
            <ta e="T602" id="Seg_2425" s="T594">s:temp</ta>
            <ta e="T612" id="Seg_2426" s="T611">0.2.h:S v:pred</ta>
            <ta e="T646" id="Seg_2427" s="T645">np.h:S</ta>
            <ta e="T647" id="Seg_2428" s="T646">v:pred</ta>
            <ta e="T651" id="Seg_2429" s="T650">np:O</ta>
            <ta e="T672" id="Seg_2430" s="T671">np.h:S</ta>
            <ta e="T674" id="Seg_2431" s="T673">np:O</ta>
            <ta e="T675" id="Seg_2432" s="T674">0.2.h:S v:pred</ta>
            <ta e="T679" id="Seg_2433" s="T678">v:pred</ta>
            <ta e="T681" id="Seg_2434" s="T680">np:O</ta>
            <ta e="T684" id="Seg_2435" s="T683">0.2.h:S v:pred</ta>
            <ta e="T685" id="Seg_2436" s="T684">np:O</ta>
            <ta e="T687" id="Seg_2437" s="T686">0.2.h:S v:pred</ta>
            <ta e="T689" id="Seg_2438" s="T688">np:S</ta>
            <ta e="T690" id="Seg_2439" s="T689">v:pred</ta>
            <ta e="T691" id="Seg_2440" s="T690">pro:S</ta>
            <ta e="T692" id="Seg_2441" s="T691">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-KuNS">
            <ta e="T1" id="Seg_2442" s="T0">accs-inf</ta>
            <ta e="T2" id="Seg_2443" s="T1">accs-inf</ta>
            <ta e="T88" id="Seg_2444" s="T87">accs-gen</ta>
            <ta e="T89" id="Seg_2445" s="T88">accs-inf</ta>
            <ta e="T123" id="Seg_2446" s="T122">accs-inf</ta>
            <ta e="T127" id="Seg_2447" s="T126">giv-active</ta>
            <ta e="T143" id="Seg_2448" s="T142">accs-inf</ta>
            <ta e="T145" id="Seg_2449" s="T144">giv-inactive</ta>
            <ta e="T157" id="Seg_2450" s="T156">new</ta>
            <ta e="T159" id="Seg_2451" s="T158">giv-active</ta>
            <ta e="T161" id="Seg_2452" s="T160">new</ta>
            <ta e="T165" id="Seg_2453" s="T164">accs-inf</ta>
            <ta e="T166" id="Seg_2454" s="T165">giv-inactive</ta>
            <ta e="T245" id="Seg_2455" s="T244">giv-active</ta>
            <ta e="T251" id="Seg_2456" s="T250">giv-active</ta>
            <ta e="T291" id="Seg_2457" s="T290">giv-inactive</ta>
            <ta e="T294" id="Seg_2458" s="T293">0.accs-gen</ta>
            <ta e="T347" id="Seg_2459" s="T346">giv-active-Q</ta>
            <ta e="T350" id="Seg_2460" s="T349">0.giv-active-Q</ta>
            <ta e="T351" id="Seg_2461" s="T350">0.giv-active 0.quot-sp</ta>
            <ta e="T377" id="Seg_2462" s="T376">0.giv-inactive</ta>
            <ta e="T380" id="Seg_2463" s="T379">0.giv-active</ta>
            <ta e="T430" id="Seg_2464" s="T429">new</ta>
            <ta e="T432" id="Seg_2465" s="T431">accs-inf</ta>
            <ta e="T435" id="Seg_2466" s="T434">new</ta>
            <ta e="T436" id="Seg_2467" s="T435">0.giv-active</ta>
            <ta e="T440" id="Seg_2468" s="T439">giv-inactive</ta>
            <ta e="T548" id="Seg_2469" s="T547">0.giv-active</ta>
            <ta e="T559" id="Seg_2470" s="T558">0.giv-active-Q</ta>
            <ta e="T560" id="Seg_2471" s="T559">0.giv-active 0.quot-sp</ta>
            <ta e="T570" id="Seg_2472" s="T569">0.giv-active</ta>
            <ta e="T587" id="Seg_2473" s="T586">giv-inactive</ta>
            <ta e="T595" id="Seg_2474" s="T594">giv-active</ta>
            <ta e="T600" id="Seg_2475" s="T599">new</ta>
            <ta e="T602" id="Seg_2476" s="T601">0.giv-inactive</ta>
            <ta e="T604" id="Seg_2477" s="T602">0.giv-active</ta>
            <ta e="T612" id="Seg_2478" s="T611">0.giv-active</ta>
            <ta e="T645" id="Seg_2479" s="T644">accs-gen</ta>
            <ta e="T646" id="Seg_2480" s="T645">accs-inf</ta>
            <ta e="T649" id="Seg_2481" s="T648">giv-inactive</ta>
            <ta e="T651" id="Seg_2482" s="T650">accs-inf</ta>
            <ta e="T672" id="Seg_2483" s="T671">giv-inactive</ta>
            <ta e="T674" id="Seg_2484" s="T673">new-Q</ta>
            <ta e="T675" id="Seg_2485" s="T674">0.giv-active-Q</ta>
            <ta e="T676" id="Seg_2486" s="T675">0.quot-sp</ta>
            <ta e="T681" id="Seg_2487" s="T680">giv-active</ta>
            <ta e="T684" id="Seg_2488" s="T683">0.giv-active</ta>
            <ta e="T685" id="Seg_2489" s="T684">giv-active</ta>
            <ta e="T687" id="Seg_2490" s="T686">0.giv-active</ta>
         </annotation>
         <annotation name="Top" tierref="Top-KuNS">
            <ta e="T1" id="Seg_2491" s="T0">top.int.concr</ta>
            <ta e="T2" id="Seg_2492" s="T1">top.int.concr</ta>
            <ta e="T123" id="Seg_2493" s="T122">top.int.concr</ta>
            <ta e="T154" id="Seg_2494" s="T151">top.int.concr</ta>
            <ta e="T251" id="Seg_2495" s="T245">top.ext</ta>
            <ta e="T291" id="Seg_2496" s="T289">top.int.concr</ta>
            <ta e="T298" id="Seg_2497" s="T297">top.int.concr</ta>
            <ta e="T351" id="Seg_2498" s="T350">0.top.int.concr</ta>
            <ta e="T428" id="Seg_2499" s="T427">top.int.concr</ta>
            <ta e="T548" id="Seg_2500" s="T547">0.top.int.concr</ta>
            <ta e="T555" id="Seg_2501" s="T553">top.int.concr</ta>
            <ta e="T590" id="Seg_2502" s="T587">top.int.concr</ta>
            <ta e="T593" id="Seg_2503" s="T590">top.int.concr</ta>
            <ta e="T672" id="Seg_2504" s="T670">top.int.concr</ta>
            <ta e="T684" id="Seg_2505" s="T683">0.top.int.concr</ta>
            <ta e="T687" id="Seg_2506" s="T686">0.top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc-KuNS">
            <ta e="T3" id="Seg_2507" s="T2">foc.nar</ta>
            <ta e="T125" id="Seg_2508" s="T124">foc.nar</ta>
            <ta e="T163" id="Seg_2509" s="T162">foc.ver</ta>
            <ta e="T253" id="Seg_2510" s="T252">foc.nar</ta>
            <ta e="T255" id="Seg_2511" s="T254">foc.nar</ta>
            <ta e="T294" id="Seg_2512" s="T292">foc.nar</ta>
            <ta e="T301" id="Seg_2513" s="T299">foc.int</ta>
            <ta e="T350" id="Seg_2514" s="T347">foc.int</ta>
            <ta e="T351" id="Seg_2515" s="T350">foc.int</ta>
            <ta e="T417" id="Seg_2516" s="T416">foc.ver</ta>
            <ta e="T440" id="Seg_2517" s="T427">foc.wid</ta>
            <ta e="T548" id="Seg_2518" s="T547">foc.ver</ta>
            <ta e="T559" id="Seg_2519" s="T555">foc.int</ta>
            <ta e="T570" id="Seg_2520" s="T560">foc.int</ta>
            <ta e="T579" id="Seg_2521" s="T578">foc.ver</ta>
            <ta e="T605" id="Seg_2522" s="T604">foc.nar</ta>
            <ta e="T608" id="Seg_2523" s="T607">foc.nar</ta>
            <ta e="T647" id="Seg_2524" s="T646">foc.ver</ta>
            <ta e="T679" id="Seg_2525" s="T677">foc.ver</ta>
            <ta e="T684" id="Seg_2526" s="T680">foc.int</ta>
            <ta e="T687" id="Seg_2527" s="T684">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR-KuNS">
            <ta e="T88" id="Seg_2528" s="T87">NGAN:cult</ta>
            <ta e="T123" id="Seg_2529" s="T122">RUS:cult</ta>
            <ta e="T127" id="Seg_2530" s="T126">RUS:cult</ta>
            <ta e="T145" id="Seg_2531" s="T144">RUS:cult</ta>
            <ta e="T166" id="Seg_2532" s="T165">RUS:cult</ta>
            <ta e="T249" id="Seg_2533" s="T248">RUS:cult</ta>
            <ta e="T319" id="Seg_2534" s="T317">RUS:gram</ta>
            <ta e="T430" id="Seg_2535" s="T429">RUS:cult</ta>
            <ta e="T432" id="Seg_2536" s="T431">RUS:cult</ta>
            <ta e="T435" id="Seg_2537" s="T434">RUS:cult</ta>
            <ta e="T557" id="Seg_2538" s="T556">RUS:cult</ta>
            <ta e="T597" id="Seg_2539" s="T596">RUS:cult</ta>
            <ta e="T609" id="Seg_2540" s="T608">RUS:cult</ta>
            <ta e="T645" id="Seg_2541" s="T644">RUS:cult</ta>
            <ta e="T674" id="Seg_2542" s="T673">RUS:cult</ta>
            <ta e="T681" id="Seg_2543" s="T680">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KuNS" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KuNS" />
         <annotation name="CS" tierref="CS-KuNS" />
         <annotation name="fe" tierref="fe-KuNS">
            <ta e="T6" id="Seg_2544" s="T0">– Your father and your mother, who were they?</ta>
            <ta e="T89" id="Seg_2545" s="T87">– In the tundra of Popigay.</ta>
            <ta e="T127" id="Seg_2546" s="T122">– Your brigade, what kind of brigade was yours?</ta>
            <ta e="T147" id="Seg_2547" s="T141">– Since You were young, You worked in that brigade, right?</ta>
            <ta e="T166" id="Seg_2548" s="T151">– During that time, did other people, people older than You, did they lead Your life and brigade?</ta>
            <ta e="T256" id="Seg_2549" s="T243">– Sidor Alekseevich, around 18,000 reindeer, where they were "thrown away", where did they go to?</ta>
            <ta e="T296" id="Seg_2550" s="T288">– Earlier one might hunt the wolf, right.</ta>
            <ta e="T301" id="Seg_2551" s="T296">Today there is no such thing?</ta>
            <ta e="T319" id="Seg_2552" s="T316">– So…</ta>
            <ta e="T352" id="Seg_2553" s="T346">– "Pay the reindeer herders more money", you say though.</ta>
            <ta e="T380" id="Seg_2554" s="T376">– For guarding or when they kill them?</ta>
            <ta e="T417" id="Seg_2555" s="T415">– Yeah, that's right.</ta>
            <ta e="T441" id="Seg_2556" s="T427">– There the republic's deputies protect the reindeer herders, apparently, passing various laws, right?</ta>
            <ta e="T549" id="Seg_2557" s="T546">– Did they earn money?</ta>
            <ta e="T571" id="Seg_2558" s="T551">– Now in the new era they say "work according to market economy", but that doesn't work, one gives various advise in order to let it work, right?</ta>
            <ta e="T580" id="Seg_2559" s="T573">– And now there are fewer reindeer herders left, few?</ta>
            <ta e="T604" id="Seg_2560" s="T585">– Sidor Alekseevich, in earlier years, in the eighties probably, as you were nominated to the Verkhovnyj Soviet, as you were elected…</ta>
            <ta e="T612" id="Seg_2561" s="T604">According to whose will, how did you become a deputy?</ta>
            <ta e="T652" id="Seg_2562" s="T643">– The heads of the rayon saw your work apparently, right?</ta>
            <ta e="T680" id="Seg_2563" s="T669">– Then the reindeer herders asked You to write the report, right?</ta>
            <ta e="T693" id="Seg_2564" s="T680">You did write Your report and told everything, how many reindeer were born, how many are sick. </ta>
         </annotation>
         <annotation name="fg" tierref="fg-KuNS">
            <ta e="T6" id="Seg_2565" s="T0">– Ihr Vater und ihre Mutter, wer war das denn?</ta>
            <ta e="T89" id="Seg_2566" s="T87">– In der Tundra von Popigaj.</ta>
            <ta e="T127" id="Seg_2567" s="T122">– Eure Brigade, was für eine Brigade war eure?</ta>
            <ta e="T147" id="Seg_2568" s="T141">– Seit Ihrer Jugend haben Sie in dieser Brigade gearbeitet, ja?</ta>
            <ta e="T166" id="Seg_2569" s="T151">– Haben in der Zeit andere Leute, ältere Leute als Sie, Ihr Leben und Ihre Brigade gelenkt?</ta>
            <ta e="T256" id="Seg_2570" s="T243">– Sidor Alekseevič, ungefähr 18000 Rentiere, wohin wurden die "weggeworfen", wo sind die gelandet?</ta>
            <ta e="T296" id="Seg_2571" s="T288">– Früher durfte man den Wolf jagen, ja.</ta>
            <ta e="T301" id="Seg_2572" s="T296">Heute gibt es sowas nicht mehr?</ta>
            <ta e="T319" id="Seg_2573" s="T316">– So…</ta>
            <ta e="T352" id="Seg_2574" s="T346">– "Zahlt den Rentierhirten mehr Geld", sagen Sie also.</ta>
            <ta e="T380" id="Seg_2575" s="T376">– Fürs Aufpassen oder wenn sie sie töten?</ta>
            <ta e="T417" id="Seg_2576" s="T415">– Ja, das stimmt.</ta>
            <ta e="T441" id="Seg_2577" s="T427">– Dort beschützten die Abgeordneten der Republik offenbar die Rentierhirten, indem sie verschiedene Gesetze verabschieden, ja?</ta>
            <ta e="T549" id="Seg_2578" s="T546">– Haben sie Geld gemacht?</ta>
            <ta e="T571" id="Seg_2579" s="T551">– Jetzt in der neuen Zeit sagen sie "arbeitet gemäß der Marktwirtschaft", aber das klappt nicht so, man rät auf unterschiedliche Art und Weise das klappen zu lassen, ja?</ta>
            <ta e="T580" id="Seg_2580" s="T573">– Und jetzt sind weniger Rentiermenschen geblieben, wenig?</ta>
            <ta e="T604" id="Seg_2581" s="T585">– Sidor Alekseevič, in früheren Jahren, in den achtziger Jahren wahrscheinlich, als man Sie da als Abgeordneter in den Verxovnij Sovet gewählt hat, als man Sie dazu gemacht hat…</ta>
            <ta e="T612" id="Seg_2582" s="T604">Nach wessen Willen, wie sind Sie Abgeordneter geworden?</ta>
            <ta e="T652" id="Seg_2583" s="T643">– Die Chefs aus dem Rajon sahen wahrscheinlich Ihre Arbeit, ja?</ta>
            <ta e="T680" id="Seg_2584" s="T669">– Dann haben die Rentierhirten Sie gebeten "schreiben Sie Ihren Bericht", ja?</ta>
            <ta e="T693" id="Seg_2585" s="T680">Sie schrieben Ihren Bericht und erzählten alles, wieviele Rentiere geboren wurden, wieviele krank sind.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KuNS">
            <ta e="T6" id="Seg_2586" s="T0">— Вот ваши отец и мать, они кто были?</ta>
            <ta e="T89" id="Seg_2587" s="T87">— В Попигайской тундре.</ta>
            <ta e="T127" id="Seg_2588" s="T122">— Бригада вот какая была у вас, ваша бригада?</ta>
            <ta e="T147" id="Seg_2589" s="T141">— С молодости в этой бригаде работали, да?</ta>
            <ta e="T166" id="Seg_2590" s="T151">— В те времена другие тогда другие люди, постарше вас люди руководили и жизнью, и бригадой?</ta>
            <ta e="T256" id="Seg_2591" s="T243">— Сидор Алексеевич, вот те более восемнадцати тысяч голов куда "разбросаны" были, куда девались?</ta>
            <ta e="T296" id="Seg_2592" s="T288">— Раньше и волков разрешали убивать ведь.</ta>
            <ta e="T301" id="Seg_2593" s="T296">Сейчас и такого даже нет?</ta>
            <ta e="T319" id="Seg_2594" s="T316">— Так-то…</ta>
            <ta e="T352" id="Seg_2595" s="T346">— Чтобы оленеводам больше денег платили, говорите ведь.</ta>
            <ta e="T380" id="Seg_2596" s="T376">— За содержание или за убой?</ta>
            <ta e="T417" id="Seg_2597" s="T415">— Да, правда.</ta>
            <ta e="T441" id="Seg_2598" s="T427">— Там ведь республиканские депутаты в защиту оленеводов издают отдельные законы, защищают, кажется, работу оленевода, да?</ta>
            <ta e="T549" id="Seg_2599" s="T546">— Зарабатывали?</ta>
            <ta e="T571" id="Seg_2600" s="T551">— Сейчас в новое время говорят: "Работайте по-рыночному", но то не очень получается, то советуют по-разному подправить, да?</ta>
            <ta e="T580" id="Seg_2601" s="T573">— А сейчас оленеводов более-менее остаётся немножко?</ta>
            <ta e="T604" id="Seg_2602" s="T585">— Сидор Алексеевич, в былые времена, где-то в восьмидесятые годы, кажется, вас вот в депутаты Верховного Совета когда выдвигали, избирали…</ta>
            <ta e="T612" id="Seg_2603" s="T604">По чьей воле, как в депутаты вышли?</ta>
            <ta e="T652" id="Seg_2604" s="T643">— Руководители из района когда увидели, наверное, эту вашу работу, да?</ta>
            <ta e="T680" id="Seg_2605" s="T669">— Тогда оленеводы просили, чтобы Вы отчёты им писали, об этом просили, да?</ta>
            <ta e="T693" id="Seg_2606" s="T680">Вы отчёты свои писали, обо всём рассказывали, сколько оленей отелилось, сколько заболело.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-KuNS">
            <ta e="T6" id="Seg_2607" s="T0">K: Отец ваш, мать ваша кто были вот тогда?</ta>
            <ta e="T89" id="Seg_2608" s="T87">K: В Попигайской тундре.</ta>
            <ta e="T127" id="Seg_2609" s="T122">K: Бригада вот какая была у вас, ваша бригада?</ta>
            <ta e="T147" id="Seg_2610" s="T141">К: С молодости в этой бригаде работая, да?</ta>
            <ta e="T166" id="Seg_2611" s="T151">– В те времена другие тогда другие люди ээ, постарше вас люди руководили и жизнью, и бригадой?</ta>
            <ta e="T256" id="Seg_2612" s="T243">– Сидор Алексеевич, вот из свыше восемнадцати тысяч голов куда "разбросаны были", куда девались?</ta>
            <ta e="T296" id="Seg_2613" s="T288">K: Раньше это и волка разрешали убивать, ведь.</ta>
            <ta e="T301" id="Seg_2614" s="T296">Сейчас и такого даже нет?</ta>
            <ta e="T319" id="Seg_2615" s="T316">K: Так-то…</ta>
            <ta e="T352" id="Seg_2616" s="T346">K: Чтобы оленеводам больше денег платили, говорите ведь.</ta>
            <ta e="T380" id="Seg_2617" s="T376">K: За содержание или за убой…?</ta>
            <ta e="T417" id="Seg_2618" s="T415">К: Да, правда.</ta>
            <ta e="T441" id="Seg_2619" s="T427">K: Там-то республиканские депутаты в защиту оленеводов издают отдельные законы, защищают, кажется, работу оленевода, да?</ta>
            <ta e="T549" id="Seg_2620" s="T546">K: Зарабатывали?</ta>
            <ta e="T571" id="Seg_2621" s="T551">К: Сейчас в новое время к этим рыночным отношениям пытаются подвести, говорят так, но то не очень не получается ведь, то подправить чтобы по разному, хи, советуют, да?</ta>
            <ta e="T580" id="Seg_2622" s="T573">K: А сейчас более менее оленеводы остаются немножко?</ta>
            <ta e="T604" id="Seg_2623" s="T585">K: Сидор Алексеевич, в былые времена, где-то может, в восьмидесятые годы, кажется, вас вот в депутаты Верховного Совета когда выдвигали, избирали…</ta>
            <ta e="T612" id="Seg_2624" s="T604">По это чьей воле, как в депутаты вышли?</ta>
            <ta e="T652" id="Seg_2625" s="T643">K: Руководители из района увидев, наверное, эту вашу работу, да?</ta>
            <ta e="T680" id="Seg_2626" s="T669">K: Тогда оленеводы это, чтоб отчет написали им, об этом просили, да?</ta>
            <ta e="T693" id="Seg_2627" s="T680">Отчёт свой вы писали, обо всём рассказывали, сколько оленей отелилось, сколько заболело.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KuNS" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-ChSA"
                      id="tx-ChSA"
                      speaker="ChSA"
                      type="t">
         <timeline-fork end="T366" start="T365">
            <tli id="T365.tx-ChSA.1" />
         </timeline-fork>
         <timeline-fork end="T398" start="T397">
            <tli id="T397.tx-ChSA.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-ChSA">
            <ts e="T87" id="Seg_2628" n="sc" s="T6">
               <ts e="T13" id="Seg_2630" n="HIAT:u" s="T6">
                  <nts id="Seg_2631" n="HIAT:ip">–</nts>
                  <ts e="T7" id="Seg_2633" n="HIAT:w" s="T6">Teːtelerbit</ts>
                  <nts id="Seg_2634" n="HIAT:ip">,</nts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_2637" n="HIAT:w" s="T7">inʼelerbit</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_2640" n="HIAT:w" s="T8">dʼe</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_2643" n="HIAT:w" s="T9">otto</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_2646" n="HIAT:w" s="T10">kɨrdʼagastar</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_2649" n="HIAT:w" s="T11">ete</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_2652" n="HIAT:w" s="T12">bu͡o</ts>
                  <nts id="Seg_2653" n="HIAT:ip">.</nts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_2656" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_2658" n="HIAT:w" s="T13">Unu</ts>
                  <nts id="Seg_2659" n="HIAT:ip">,</nts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_2662" n="HIAT:w" s="T14">Anɨ</ts>
                  <nts id="Seg_2663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_2665" n="HIAT:w" s="T15">bilbetter</ts>
                  <nts id="Seg_2666" n="HIAT:ip">.</nts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_2669" n="HIAT:u" s="T16">
                  <nts id="Seg_2670" n="HIAT:ip">(</nts>
                  <ts e="T18" id="Seg_2672" n="HIAT:w" s="T16">Krʼest-</ts>
                  <nts id="Seg_2673" n="HIAT:ip">)</nts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_2676" n="HIAT:w" s="T18">meːne</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2678" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_2680" n="HIAT:w" s="T19">hɨldʼala-</ts>
                  <nts id="Seg_2681" n="HIAT:ip">)</nts>
                  <nts id="Seg_2682" n="HIAT:ip">,</nts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_2685" n="HIAT:w" s="T21">iliː</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_2688" n="HIAT:w" s="T22">battɨːllara</ts>
                  <nts id="Seg_2689" n="HIAT:ip">;</nts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_2692" n="HIAT:w" s="T23">munnuk</ts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_2695" n="HIAT:w" s="T24">gɨnaːččɨbɨn</ts>
                  <nts id="Seg_2696" n="HIAT:ip">,</nts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_2699" n="HIAT:w" s="T25">min</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_2702" n="HIAT:w" s="T26">emi͡e</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_2705" n="HIAT:w" s="T27">hanɨ</ts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_2708" n="HIAT:w" s="T28">hin</ts>
                  <nts id="Seg_2709" n="HIAT:ip">,</nts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_2712" n="HIAT:w" s="T29">itinnik</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_2715" n="HIAT:w" s="T30">keri͡ete</ts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_2718" n="HIAT:w" s="T31">dʼe</ts>
                  <nts id="Seg_2719" n="HIAT:ip">.</nts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_2722" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_2724" n="HIAT:w" s="T32">Öjüm</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_2727" n="HIAT:w" s="T33">köppüte</ts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_2730" n="HIAT:w" s="T34">bert</ts>
                  <nts id="Seg_2731" n="HIAT:ip">.</nts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_2734" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_2736" n="HIAT:w" s="T35">Dʼɨlɨ</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_2739" n="HIAT:w" s="T36">bɨha</ts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_2742" n="HIAT:w" s="T37">ɨ͡aldʼɨbɨtɨm</ts>
                  <nts id="Seg_2743" n="HIAT:ip">,</nts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_2746" n="HIAT:w" s="T38">kata</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_2749" n="HIAT:w" s="T39">ol</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_2752" n="HIAT:w" s="T40">ereːti</ts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_2755" n="HIAT:w" s="T41">amabɨn</ts>
                  <nts id="Seg_2756" n="HIAT:ip">.</nts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_2759" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_2761" n="HIAT:w" s="T42">Hi͡ese</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_2764" n="HIAT:w" s="T43">kihige</ts>
                  <nts id="Seg_2765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_2767" n="HIAT:w" s="T44">kihi</ts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_2770" n="HIAT:w" s="T45">deter</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_2773" n="HIAT:w" s="T46">kördük</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_2776" n="HIAT:w" s="T47">hɨldʼabɨn</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_2779" n="HIAT:w" s="T48">eːt</ts>
                  <nts id="Seg_2780" n="HIAT:ip">.</nts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_2783" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_2785" n="HIAT:w" s="T49">Üleber</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_2788" n="HIAT:w" s="T50">daː</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_2791" n="HIAT:w" s="T51">ke</ts>
                  <nts id="Seg_2792" n="HIAT:ip">.</nts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_2795" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_2797" n="HIAT:w" s="T52">Ülege</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_2800" n="HIAT:w" s="T53">kuhagan</ts>
                  <nts id="Seg_2801" n="HIAT:ip">.</nts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_2804" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_2806" n="HIAT:w" s="T54">Bu</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_2809" n="HIAT:w" s="T55">ogolorbun</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_2812" n="HIAT:w" s="T56">iːtineːribin</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_2815" n="HIAT:w" s="T57">bu</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_2818" n="HIAT:w" s="T58">tugu</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_2821" n="HIAT:w" s="T59">dolu͡oj</ts>
                  <nts id="Seg_2822" n="HIAT:ip">…</nts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_2825" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_2827" n="HIAT:w" s="T61">Tɨːmmɨn</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_2830" n="HIAT:w" s="T62">bɨha</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_2833" n="HIAT:w" s="T63">möŋüneːččibin</ts>
                  <nts id="Seg_2834" n="HIAT:ip">.</nts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_2837" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_2839" n="HIAT:w" s="T64">Taba</ts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_2842" n="HIAT:w" s="T65">hɨrɨːta</ts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_2845" n="HIAT:w" s="T66">ajɨː</ts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_2848" n="HIAT:w" s="T67">du͡o</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_2851" n="HIAT:w" s="T68">urut</ts>
                  <nts id="Seg_2852" n="HIAT:ip">.</nts>
                  <nts id="Seg_2853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_2855" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_2857" n="HIAT:w" s="T69">Elbege</ts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_2860" n="HIAT:w" s="T70">daː</ts>
                  <nts id="Seg_2861" n="HIAT:ip">,</nts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_2864" n="HIAT:w" s="T71">ɨ͡arakana</ts>
                  <nts id="Seg_2865" n="HIAT:ip">.</nts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_2868" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_2870" n="HIAT:w" s="T72">Kihi</ts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_2873" n="HIAT:w" s="T73">heni͡ete</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_2876" n="HIAT:w" s="T74">esten</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_2879" n="HIAT:w" s="T75">baran</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_2882" n="HIAT:w" s="T76">keleːčči</ts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_2885" n="HIAT:w" s="T77">bukatɨn</ts>
                  <nts id="Seg_2886" n="HIAT:ip">.</nts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_2889" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_2891" n="HIAT:w" s="T78">U͡on</ts>
                  <nts id="Seg_2892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_2894" n="HIAT:w" s="T79">biːrderge</ts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_2897" n="HIAT:w" s="T80">keleːččibit</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_2900" n="HIAT:w" s="T81">karaŋa</ts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_2903" n="HIAT:w" s="T82">kɨhɨn</ts>
                  <nts id="Seg_2904" n="HIAT:ip">.</nts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_2907" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_2909" n="HIAT:w" s="T83">Oː</ts>
                  <nts id="Seg_2910" n="HIAT:ip">,</nts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_2913" n="HIAT:w" s="T84">tɨmnɨː</ts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_2916" n="HIAT:w" s="T85">bagaj</ts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_2919" n="HIAT:w" s="T86">bu͡o</ts>
                  <nts id="Seg_2920" n="HIAT:ip">.</nts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T122" id="Seg_2922" n="sc" s="T89">
               <ts e="T96" id="Seg_2924" n="HIAT:u" s="T89">
                  <nts id="Seg_2925" n="HIAT:ip">–</nts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_2928" n="HIAT:w" s="T89">Pöpügej</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_2931" n="HIAT:w" s="T90">tɨ͡atɨgar</ts>
                  <nts id="Seg_2932" n="HIAT:ip">,</nts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_2935" n="HIAT:w" s="T91">mas</ts>
                  <nts id="Seg_2936" n="HIAT:ip">,</nts>
                  <nts id="Seg_2937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_2939" n="HIAT:w" s="T92">mas</ts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_2942" n="HIAT:w" s="T93">bu͡o</ts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_2945" n="HIAT:w" s="T94">haːtar</ts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_2948" n="HIAT:w" s="T95">bukatɨn</ts>
                  <nts id="Seg_2949" n="HIAT:ip">.</nts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_2952" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_2954" n="HIAT:w" s="T96">Taba</ts>
                  <nts id="Seg_2955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_2957" n="HIAT:w" s="T97">kanna</ts>
                  <nts id="Seg_2958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_2960" n="HIAT:w" s="T98">da</ts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_2963" n="HIAT:w" s="T99">köstübet</ts>
                  <nts id="Seg_2964" n="HIAT:ip">.</nts>
                  <nts id="Seg_2965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_2967" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_2969" n="HIAT:w" s="T100">Onnuktarga</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_2972" n="HIAT:w" s="T101">üleleːn</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_2975" n="HIAT:w" s="T102">muŋnammɨtɨm</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_2978" n="HIAT:w" s="T103">min</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_2981" n="HIAT:w" s="T104">haːmaj</ts>
                  <nts id="Seg_2982" n="HIAT:ip">.</nts>
                  <nts id="Seg_2983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_2985" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_2987" n="HIAT:w" s="T105">Ol</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_2990" n="HIAT:w" s="T106">gɨnaːrɨ</ts>
                  <nts id="Seg_2991" n="HIAT:ip">,</nts>
                  <nts id="Seg_2992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_2994" n="HIAT:w" s="T107">dʼolloːk</ts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_2997" n="HIAT:w" s="T108">bu͡olammɨn</ts>
                  <nts id="Seg_2998" n="HIAT:ip">,</nts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_3001" n="HIAT:w" s="T109">tabam</ts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_3004" n="HIAT:w" s="T110">iːtillibite</ts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_3007" n="HIAT:w" s="T111">hürdeːk</ts>
                  <nts id="Seg_3008" n="HIAT:ip">.</nts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_3011" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_3013" n="HIAT:w" s="T112">U͡ontan</ts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_3016" n="HIAT:w" s="T113">taksa</ts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_3019" n="HIAT:w" s="T114">tɨːhɨččanɨ</ts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_3022" n="HIAT:w" s="T115">iːppitim</ts>
                  <nts id="Seg_3023" n="HIAT:ip">,</nts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_3026" n="HIAT:w" s="T116">kas</ts>
                  <nts id="Seg_3027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_3029" n="HIAT:w" s="T117">da</ts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_3032" n="HIAT:w" s="T118">stadaga</ts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_3035" n="HIAT:w" s="T119">bi͡erbitim</ts>
                  <nts id="Seg_3036" n="HIAT:ip">,</nts>
                  <nts id="Seg_3037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_3039" n="HIAT:w" s="T120">stadaga</ts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_3042" n="HIAT:w" s="T121">bi͡erbitim</ts>
                  <nts id="Seg_3043" n="HIAT:ip">.</nts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_3045" n="sc" s="T127">
               <ts e="T133" id="Seg_3047" n="HIAT:u" s="T127">
                  <nts id="Seg_3048" n="HIAT:ip">–</nts>
                  <ts e="T128" id="Seg_3050" n="HIAT:w" s="T127">Vtaroj</ts>
                  <nts id="Seg_3051" n="HIAT:ip">,</nts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_3054" n="HIAT:w" s="T128">vtaroj</ts>
                  <nts id="Seg_3055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_3057" n="HIAT:w" s="T129">brigada</ts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_3060" n="HIAT:w" s="T130">meːr</ts>
                  <nts id="Seg_3061" n="HIAT:ip">,</nts>
                  <nts id="Seg_3062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_3064" n="HIAT:w" s="T131">anɨga</ts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_3067" n="HIAT:w" s="T132">di͡eri</ts>
                  <nts id="Seg_3068" n="HIAT:ip">.</nts>
                  <nts id="Seg_3069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_3071" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_3073" n="HIAT:w" s="T133">Bu</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_3076" n="HIAT:w" s="T134">anɨ</ts>
                  <nts id="Seg_3077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_3079" n="HIAT:w" s="T135">u͡olɨm</ts>
                  <nts id="Seg_3080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_3082" n="HIAT:w" s="T136">iti</ts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_3085" n="HIAT:w" s="T137">obu͡ojdanaːččɨ</ts>
                  <nts id="Seg_3086" n="HIAT:ip">.</nts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_3089" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_3091" n="HIAT:w" s="T138">Valodʼam</ts>
                  <nts id="Seg_3092" n="HIAT:ip">.</nts>
                  <nts id="Seg_3093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_3095" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_3097" n="HIAT:w" s="T139">Hogotok</ts>
                  <nts id="Seg_3098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_3100" n="HIAT:w" s="T140">u͡ollaːkpɨn</ts>
                  <nts id="Seg_3101" n="HIAT:ip">.</nts>
                  <nts id="Seg_3102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T151" id="Seg_3103" n="sc" s="T147">
               <ts e="T151" id="Seg_3105" n="HIAT:u" s="T147">
                  <nts id="Seg_3106" n="HIAT:ip">–</nts>
                  <ts e="T148" id="Seg_3108" n="HIAT:w" s="T147">Mm</ts>
                  <nts id="Seg_3109" n="HIAT:ip">,</nts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_3112" n="HIAT:w" s="T148">kɨrdʼabɨt</ts>
                  <nts id="Seg_3113" n="HIAT:ip">,</nts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_3116" n="HIAT:w" s="T149">ikki͡emmit</ts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_3119" n="HIAT:w" s="T150">kɨrɨjdɨbɨt</ts>
                  <nts id="Seg_3120" n="HIAT:ip">.</nts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_3122" n="sc" s="T166">
               <ts e="T170" id="Seg_3124" n="HIAT:u" s="T166">
                  <nts id="Seg_3125" n="HIAT:ip">–</nts>
                  <ts e="T167" id="Seg_3127" n="HIAT:w" s="T166">Brʼigadʼiːrdarbɨt</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_3130" n="HIAT:w" s="T167">hi͡ese</ts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_3133" n="HIAT:w" s="T168">kihiler</ts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_3136" n="HIAT:w" s="T169">etilere</ts>
                  <nts id="Seg_3137" n="HIAT:ip">.</nts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_3140" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_3142" n="HIAT:w" s="T170">Onton</ts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3144" n="HIAT:ip">(</nts>
                  <ts e="T173" id="Seg_3146" n="HIAT:w" s="T171">ü͡öt-</ts>
                  <nts id="Seg_3147" n="HIAT:ip">)</nts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_3150" n="HIAT:w" s="T173">ü͡örene</ts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_3153" n="HIAT:w" s="T174">ü͡öremmitinen</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_3156" n="HIAT:w" s="T175">honon</ts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_3159" n="HIAT:w" s="T176">ü͡örenen</ts>
                  <nts id="Seg_3160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_3162" n="HIAT:w" s="T177">kaːllakpɨt</ts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_3165" n="HIAT:w" s="T178">diː</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_3168" n="HIAT:w" s="T179">bihigi</ts>
                  <nts id="Seg_3169" n="HIAT:ip">.</nts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_3172" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_3174" n="HIAT:w" s="T180">Iti</ts>
                  <nts id="Seg_3175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_3177" n="HIAT:w" s="T181">kečehiːge</ts>
                  <nts id="Seg_3178" n="HIAT:ip">,</nts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_3181" n="HIAT:w" s="T182">tabaga</ts>
                  <nts id="Seg_3182" n="HIAT:ip">,</nts>
                  <nts id="Seg_3183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3184" n="HIAT:ip">"</nts>
                  <ts e="T184" id="Seg_3186" n="HIAT:w" s="T183">tabanɨ</ts>
                  <nts id="Seg_3187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_3189" n="HIAT:w" s="T184">hüterimeŋ</ts>
                  <nts id="Seg_3190" n="HIAT:ip">"</nts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_3193" n="HIAT:w" s="T185">di͡en</ts>
                  <nts id="Seg_3194" n="HIAT:ip">.</nts>
                  <nts id="Seg_3195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_3197" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_3199" n="HIAT:w" s="T186">Di͡eččibin</ts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_3202" n="HIAT:w" s="T187">bu͡o</ts>
                  <nts id="Seg_3203" n="HIAT:ip">:</nts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_3206" n="HIAT:u" s="T188">
                  <nts id="Seg_3207" n="HIAT:ip">"</nts>
                  <ts e="T189" id="Seg_3209" n="HIAT:w" s="T188">Tabanɨ</ts>
                  <nts id="Seg_3210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_3212" n="HIAT:w" s="T189">hüterimeŋ</ts>
                  <nts id="Seg_3213" n="HIAT:ip">,</nts>
                  <nts id="Seg_3214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_3216" n="HIAT:w" s="T190">harsɨŋŋɨŋɨtɨn</ts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_3219" n="HIAT:w" s="T191">öjdöːŋ</ts>
                  <nts id="Seg_3220" n="HIAT:ip">"</nts>
                  <nts id="Seg_3221" n="HIAT:ip">,</nts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_3224" n="HIAT:w" s="T192">di͡eččibin</ts>
                  <nts id="Seg_3225" n="HIAT:ip">.</nts>
                  <nts id="Seg_3226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_3228" n="HIAT:u" s="T193">
                  <nts id="Seg_3229" n="HIAT:ip">"</nts>
                  <ts e="T194" id="Seg_3231" n="HIAT:w" s="T193">Harsi͡erda</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_3234" n="HIAT:w" s="T194">plaːnnana</ts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_3237" n="HIAT:w" s="T195">turuŋ</ts>
                  <nts id="Seg_3238" n="HIAT:ip">"</nts>
                  <nts id="Seg_3239" n="HIAT:ip">,</nts>
                  <nts id="Seg_3240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_3242" n="HIAT:w" s="T196">di͡eččibin</ts>
                  <nts id="Seg_3243" n="HIAT:ip">,</nts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3245" n="HIAT:ip">"</nts>
                  <ts e="T198" id="Seg_3247" n="HIAT:w" s="T197">kajdi͡ek</ts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_3250" n="HIAT:w" s="T198">üleliːrgitin</ts>
                  <nts id="Seg_3251" n="HIAT:ip">,</nts>
                  <nts id="Seg_3252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_3254" n="HIAT:w" s="T199">kajdi͡ek</ts>
                  <nts id="Seg_3255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_3257" n="HIAT:w" s="T200">barargɨtɨn</ts>
                  <nts id="Seg_3258" n="HIAT:ip">.</nts>
                  <nts id="Seg_3259" n="HIAT:ip">"</nts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_3262" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_3264" n="HIAT:w" s="T201">Tabaŋ</ts>
                  <nts id="Seg_3265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_3267" n="HIAT:w" s="T202">hütere</ts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_3270" n="HIAT:w" s="T203">ügühe</ts>
                  <nts id="Seg_3271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_3273" n="HIAT:w" s="T204">muŋa</ts>
                  <nts id="Seg_3274" n="HIAT:ip">,</nts>
                  <nts id="Seg_3275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_3277" n="HIAT:w" s="T205">öj</ts>
                  <nts id="Seg_3278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_3280" n="HIAT:w" s="T206">kimniːr</ts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_3283" n="HIAT:w" s="T207">hajɨn</ts>
                  <nts id="Seg_3284" n="HIAT:ip">,</nts>
                  <nts id="Seg_3285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_3287" n="HIAT:w" s="T208">hürdeːk</ts>
                  <nts id="Seg_3288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_3290" n="HIAT:w" s="T209">ajallanna</ts>
                  <nts id="Seg_3291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_3293" n="HIAT:w" s="T210">bu͡olaːččɨ</ts>
                  <nts id="Seg_3294" n="HIAT:ip">.</nts>
                  <nts id="Seg_3295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_3297" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_3299" n="HIAT:w" s="T211">Ol</ts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_3302" n="HIAT:w" s="T212">bačča</ts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_3305" n="HIAT:w" s="T213">hɨldʼɨ͡akpar</ts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_3308" n="HIAT:w" s="T214">di͡eri</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_3311" n="HIAT:w" s="T215">bu͡o</ts>
                  <nts id="Seg_3312" n="HIAT:ip">…</nts>
                  <nts id="Seg_3313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_3315" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_3317" n="HIAT:w" s="T217">Tu</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_3320" n="HIAT:w" s="T218">meːr</ts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_3323" n="HIAT:w" s="T219">üčügej</ts>
                  <nts id="Seg_3324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_3326" n="HIAT:w" s="T220">aːtɨnan</ts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_3329" n="HIAT:w" s="T221">tagɨstɨm</ts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_3332" n="HIAT:w" s="T222">eːt</ts>
                  <nts id="Seg_3333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_3335" n="HIAT:w" s="T223">otto</ts>
                  <nts id="Seg_3336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3337" n="HIAT:ip">–</nts>
                  <nts id="Seg_3338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_3340" n="HIAT:w" s="T224">hajɨn</ts>
                  <nts id="Seg_3341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_3343" n="HIAT:w" s="T225">da</ts>
                  <nts id="Seg_3344" n="HIAT:ip">,</nts>
                  <nts id="Seg_3345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_3347" n="HIAT:w" s="T226">kɨhɨn</ts>
                  <nts id="Seg_3348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_3350" n="HIAT:w" s="T227">da</ts>
                  <nts id="Seg_3351" n="HIAT:ip">.</nts>
                  <nts id="Seg_3352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_3354" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_3356" n="HIAT:w" s="T228">Hin</ts>
                  <nts id="Seg_3357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_3359" n="HIAT:w" s="T229">üleleːtekke</ts>
                  <nts id="Seg_3360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_3362" n="HIAT:w" s="T230">elbek</ts>
                  <nts id="Seg_3363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_3365" n="HIAT:w" s="T231">dʼɨlɨ</ts>
                  <nts id="Seg_3366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_3368" n="HIAT:w" s="T232">üleleːtim</ts>
                  <nts id="Seg_3369" n="HIAT:ip">,</nts>
                  <nts id="Seg_3370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_3372" n="HIAT:w" s="T233">bi͡eh-u͡on</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_3375" n="HIAT:w" s="T234">bi͡es</ts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_3378" n="HIAT:w" s="T235">dʼɨlɨ</ts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_3381" n="HIAT:w" s="T236">pʼensʼijaga</ts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_3384" n="HIAT:w" s="T237">kiːren</ts>
                  <nts id="Seg_3385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_3387" n="HIAT:w" s="T238">baraːn</ts>
                  <nts id="Seg_3388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_3390" n="HIAT:w" s="T239">össü͡ö</ts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_3393" n="HIAT:w" s="T240">üleleːbitim</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_3396" n="HIAT:w" s="T241">tabam</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_3399" n="HIAT:w" s="T242">iːtilleritten</ts>
                  <nts id="Seg_3400" n="HIAT:ip">.</nts>
                  <nts id="Seg_3401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T288" id="Seg_3402" n="sc" s="T256">
               <ts e="T260" id="Seg_3404" n="HIAT:u" s="T256">
                  <nts id="Seg_3405" n="HIAT:ip">–</nts>
                  <ts e="T257" id="Seg_3407" n="HIAT:w" s="T256">Hüppüte</ts>
                  <nts id="Seg_3408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_3410" n="HIAT:w" s="T257">bu͡o</ts>
                  <nts id="Seg_3411" n="HIAT:ip">,</nts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_3414" n="HIAT:w" s="T258">ölön</ts>
                  <nts id="Seg_3415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_3417" n="HIAT:w" s="T259">hüten</ts>
                  <nts id="Seg_3418" n="HIAT:ip">.</nts>
                  <nts id="Seg_3419" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_3421" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_3423" n="HIAT:w" s="T260">Hüppüte</ts>
                  <nts id="Seg_3424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_3426" n="HIAT:w" s="T261">iti</ts>
                  <nts id="Seg_3427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_3429" n="HIAT:w" s="T262">kimnerge</ts>
                  <nts id="Seg_3430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_3432" n="HIAT:w" s="T263">Haskɨlaːktarga</ts>
                  <nts id="Seg_3433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_3435" n="HIAT:w" s="T264">baran</ts>
                  <nts id="Seg_3436" n="HIAT:ip">.</nts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_3439" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_3441" n="HIAT:w" s="T265">Baran</ts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_3444" n="HIAT:w" s="T266">daːganɨ</ts>
                  <nts id="Seg_3445" n="HIAT:ip">,</nts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_3448" n="HIAT:w" s="T267">kɨːl</ts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_3451" n="HIAT:w" s="T268">da</ts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_3454" n="HIAT:w" s="T269">iller</ts>
                  <nts id="Seg_3455" n="HIAT:ip">.</nts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_3458" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_3460" n="HIAT:w" s="T270">Kɨːl</ts>
                  <nts id="Seg_3461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_3463" n="HIAT:w" s="T271">illeːčči</ts>
                  <nts id="Seg_3464" n="HIAT:ip">.</nts>
                  <nts id="Seg_3465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_3467" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_3469" n="HIAT:w" s="T272">Mʼigrasʼijatɨgar</ts>
                  <nts id="Seg_3470" n="HIAT:ip">,</nts>
                  <nts id="Seg_3471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_3473" n="HIAT:w" s="T273">kühün</ts>
                  <nts id="Seg_3474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_3476" n="HIAT:w" s="T274">keliːtiger</ts>
                  <nts id="Seg_3477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_3479" n="HIAT:w" s="T275">ke</ts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_3482" n="HIAT:w" s="T276">börö</ts>
                  <nts id="Seg_3483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_3485" n="HIAT:w" s="T277">hiːr</ts>
                  <nts id="Seg_3486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_3488" n="HIAT:w" s="T278">ulakan</ts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_3491" n="HIAT:w" s="T279">hürdeːk</ts>
                  <nts id="Seg_3492" n="HIAT:ip">.</nts>
                  <nts id="Seg_3493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_3495" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_3497" n="HIAT:w" s="T280">ɨ͡aldʼar</ts>
                  <nts id="Seg_3498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_3500" n="HIAT:w" s="T281">onuga</ts>
                  <nts id="Seg_3501" n="HIAT:ip">,</nts>
                  <nts id="Seg_3502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_3504" n="HIAT:w" s="T282">atakta</ts>
                  <nts id="Seg_3505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_3507" n="HIAT:w" s="T283">daː</ts>
                  <nts id="Seg_3508" n="HIAT:ip">,</nts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_3511" n="HIAT:w" s="T284">honno</ts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_3514" n="HIAT:w" s="T285">ör</ts>
                  <nts id="Seg_3515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_3517" n="HIAT:w" s="T286">baraːččɨta</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_3520" n="HIAT:w" s="T287">hu͡ok</ts>
                  <nts id="Seg_3521" n="HIAT:ip">.</nts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T316" id="Seg_3523" n="sc" s="T301">
               <ts e="T306" id="Seg_3525" n="HIAT:u" s="T301">
                  <nts id="Seg_3526" n="HIAT:ip">–</nts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_3529" n="HIAT:w" s="T301">Ölörtöröllör</ts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_3532" n="HIAT:w" s="T302">hanɨ</ts>
                  <nts id="Seg_3533" n="HIAT:ip">,</nts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_3536" n="HIAT:w" s="T303">otto</ts>
                  <nts id="Seg_3537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_3539" n="HIAT:w" s="T304">baːr</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_3542" n="HIAT:w" s="T305">bu͡ollagɨna</ts>
                  <nts id="Seg_3543" n="HIAT:ip">.</nts>
                  <nts id="Seg_3544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_3546" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_3548" n="HIAT:w" s="T306">Diːller</ts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_3551" n="HIAT:w" s="T307">diː</ts>
                  <nts id="Seg_3552" n="HIAT:ip">.</nts>
                  <nts id="Seg_3553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_3555" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_3557" n="HIAT:w" s="T308">Anɨ</ts>
                  <nts id="Seg_3558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_3560" n="HIAT:w" s="T309">prʼemʼijalɨːr</ts>
                  <nts id="Seg_3561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_3563" n="HIAT:w" s="T310">bu͡olbuttar</ts>
                  <nts id="Seg_3564" n="HIAT:ip">,</nts>
                  <nts id="Seg_3565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_3567" n="HIAT:w" s="T311">urut</ts>
                  <nts id="Seg_3568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_3570" n="HIAT:w" s="T312">prʼemʼijalaːččɨta</ts>
                  <nts id="Seg_3571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_3573" n="HIAT:w" s="T313">hu͡oktar</ts>
                  <nts id="Seg_3574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3575" n="HIAT:ip">(</nts>
                  <nts id="Seg_3576" n="HIAT:ip">(</nts>
                  <ats e="T315" id="Seg_3577" n="HIAT:non-pho" s="T314">PAUSE</ats>
                  <nts id="Seg_3578" n="HIAT:ip">)</nts>
                  <nts id="Seg_3579" n="HIAT:ip">)</nts>
                  <nts id="Seg_3580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_3582" n="HIAT:w" s="T315">etiler</ts>
                  <nts id="Seg_3583" n="HIAT:ip">.</nts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T346" id="Seg_3585" n="sc" s="T319">
               <ts e="T332" id="Seg_3587" n="HIAT:u" s="T319">
                  <nts id="Seg_3588" n="HIAT:ip">–</nts>
                  <nts id="Seg_3589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_3591" n="HIAT:w" s="T319">Bihi͡enebit</ts>
                  <nts id="Seg_3592" n="HIAT:ip">,</nts>
                  <nts id="Seg_3593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_3595" n="HIAT:w" s="T320">kimmit</ts>
                  <nts id="Seg_3596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_3598" n="HIAT:w" s="T321">ki͡e</ts>
                  <nts id="Seg_3599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_3601" n="HIAT:w" s="T322">rascenkabɨt</ts>
                  <nts id="Seg_3602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_3604" n="HIAT:w" s="T323">kuhagan</ts>
                  <nts id="Seg_3605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_3607" n="HIAT:w" s="T324">ete</ts>
                  <nts id="Seg_3608" n="HIAT:ip">,</nts>
                  <nts id="Seg_3609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_3611" n="HIAT:w" s="T325">urut</ts>
                  <nts id="Seg_3612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_3614" n="HIAT:w" s="T326">kɨra</ts>
                  <nts id="Seg_3615" n="HIAT:ip">,</nts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_3618" n="HIAT:w" s="T327">küččügüj</ts>
                  <nts id="Seg_3619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_3621" n="HIAT:w" s="T328">ete</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_3624" n="HIAT:w" s="T329">tababɨt</ts>
                  <nts id="Seg_3625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_3627" n="HIAT:w" s="T330">ki͡ene</ts>
                  <nts id="Seg_3628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_3630" n="HIAT:w" s="T331">ke</ts>
                  <nts id="Seg_3631" n="HIAT:ip">.</nts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_3634" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_3636" n="HIAT:w" s="T332">Onu</ts>
                  <nts id="Seg_3637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_3639" n="HIAT:w" s="T333">min</ts>
                  <nts id="Seg_3640" n="HIAT:ip">,</nts>
                  <nts id="Seg_3641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3642" n="HIAT:ip">"</nts>
                  <ts e="T335" id="Seg_3644" n="HIAT:w" s="T334">ürdetiŋ</ts>
                  <nts id="Seg_3645" n="HIAT:ip">"</nts>
                  <nts id="Seg_3646" n="HIAT:ip">,</nts>
                  <nts id="Seg_3647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_3649" n="HIAT:w" s="T335">diː</ts>
                  <nts id="Seg_3650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_3652" n="HIAT:w" s="T336">hataːn</ts>
                  <nts id="Seg_3653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_3655" n="HIAT:w" s="T337">anɨ</ts>
                  <nts id="Seg_3656" n="HIAT:ip">,</nts>
                  <nts id="Seg_3657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_3659" n="HIAT:w" s="T338">tɨl</ts>
                  <nts id="Seg_3660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_3662" n="HIAT:w" s="T339">isti͡ektere</ts>
                  <nts id="Seg_3663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_3665" n="HIAT:w" s="T340">bu͡o</ts>
                  <nts id="Seg_3666" n="HIAT:ip">,</nts>
                  <nts id="Seg_3667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_3669" n="HIAT:w" s="T341">anɨ</ts>
                  <nts id="Seg_3670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_3672" n="HIAT:w" s="T342">ülehitterbit</ts>
                  <nts id="Seg_3673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_3675" n="HIAT:w" s="T343">kuhagattar</ts>
                  <nts id="Seg_3676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_3678" n="HIAT:w" s="T344">onuga</ts>
                  <nts id="Seg_3679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_3681" n="HIAT:w" s="T345">dʼe</ts>
                  <nts id="Seg_3682" n="HIAT:ip">.</nts>
                  <nts id="Seg_3683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T376" id="Seg_3684" n="sc" s="T352">
               <ts e="T357" id="Seg_3686" n="HIAT:u" s="T352">
                  <nts id="Seg_3687" n="HIAT:ip">–</nts>
                  <ts e="T353" id="Seg_3689" n="HIAT:w" s="T352">Mm</ts>
                  <nts id="Seg_3690" n="HIAT:ip">,</nts>
                  <nts id="Seg_3691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_3693" n="HIAT:w" s="T353">diːbit</ts>
                  <nts id="Seg_3694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_3696" n="HIAT:w" s="T354">bu͡ol</ts>
                  <nts id="Seg_3697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_3699" n="HIAT:w" s="T355">kü͡ömej</ts>
                  <nts id="Seg_3700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_3702" n="HIAT:w" s="T356">iːti͡ekterin</ts>
                  <nts id="Seg_3703" n="HIAT:ip">.</nts>
                  <nts id="Seg_3704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_3706" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_3708" n="HIAT:w" s="T357">Taba</ts>
                  <nts id="Seg_3709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_3711" n="HIAT:w" s="T358">rascenkatɨn</ts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_3714" n="HIAT:w" s="T359">ürdeti͡ekke</ts>
                  <nts id="Seg_3715" n="HIAT:ip">,</nts>
                  <nts id="Seg_3716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_3718" n="HIAT:w" s="T360">diːbit</ts>
                  <nts id="Seg_3719" n="HIAT:ip">.</nts>
                  <nts id="Seg_3720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_3722" n="HIAT:u" s="T361">
                  <nts id="Seg_3723" n="HIAT:ip">(</nts>
                  <ts e="T363" id="Seg_3725" n="HIAT:w" s="T361">Bihi͡e-</ts>
                  <nts id="Seg_3726" n="HIAT:ip">)</nts>
                  <nts id="Seg_3727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_3729" n="HIAT:w" s="T363">gini͡ennere</ts>
                  <nts id="Seg_3730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_3732" n="HIAT:w" s="T364">kimnere</ts>
                  <nts id="Seg_3733" n="HIAT:ip">,</nts>
                  <nts id="Seg_3734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365.tx-ChSA.1" id="Seg_3736" n="HIAT:w" s="T365">Ürüŋ</ts>
                  <nts id="Seg_3737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_3739" n="HIAT:w" s="T365.tx-ChSA.1">Kajalar</ts>
                  <nts id="Seg_3740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_3742" n="HIAT:w" s="T366">gi͡ettere</ts>
                  <nts id="Seg_3743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_3745" n="HIAT:w" s="T367">bu͡o</ts>
                  <nts id="Seg_3746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_3748" n="HIAT:w" s="T368">biːr</ts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_3751" n="HIAT:w" s="T369">tabaga</ts>
                  <nts id="Seg_3752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_3754" n="HIAT:w" s="T370">kas</ts>
                  <nts id="Seg_3755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_3757" n="HIAT:w" s="T371">di͡ebitterej</ts>
                  <nts id="Seg_3758" n="HIAT:ip">?</nts>
                  <nts id="Seg_3759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_3761" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_3763" n="HIAT:w" s="T372">Huːtkaga</ts>
                  <nts id="Seg_3764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_3766" n="HIAT:w" s="T373">tü͡ört</ts>
                  <nts id="Seg_3767" n="HIAT:ip">,</nts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_3770" n="HIAT:w" s="T374">tü͡ört</ts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_3773" n="HIAT:w" s="T375">holku͡obaj</ts>
                  <nts id="Seg_3774" n="HIAT:ip">.</nts>
                  <nts id="Seg_3775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T415" id="Seg_3776" n="sc" s="T380">
               <ts e="T384" id="Seg_3778" n="HIAT:u" s="T380">
                  <nts id="Seg_3779" n="HIAT:ip">–</nts>
                  <nts id="Seg_3780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_3782" n="HIAT:w" s="T380">Iːtellerin</ts>
                  <nts id="Seg_3783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_3785" n="HIAT:w" s="T381">ihin</ts>
                  <nts id="Seg_3786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_3788" n="HIAT:w" s="T382">bu͡o</ts>
                  <nts id="Seg_3789" n="HIAT:ip">,</nts>
                  <nts id="Seg_3790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_3792" n="HIAT:w" s="T383">he-he</ts>
                  <nts id="Seg_3793" n="HIAT:ip">.</nts>
                  <nts id="Seg_3794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_3796" n="HIAT:u" s="T384">
                  <nts id="Seg_3797" n="HIAT:ip">(</nts>
                  <ts e="T386" id="Seg_3799" n="HIAT:w" s="T384">Tabalɨːlla-</ts>
                  <nts id="Seg_3800" n="HIAT:ip">)</nts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_3803" n="HIAT:w" s="T386">tabalaːn</ts>
                  <nts id="Seg_3804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_3806" n="HIAT:w" s="T387">iːten</ts>
                  <nts id="Seg_3807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_3809" n="HIAT:w" s="T388">taksallarɨn</ts>
                  <nts id="Seg_3810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_3812" n="HIAT:w" s="T389">ihin</ts>
                  <nts id="Seg_3813" n="HIAT:ip">.</nts>
                  <nts id="Seg_3814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_3816" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_3818" n="HIAT:w" s="T390">Gini͡ennere</ts>
                  <nts id="Seg_3819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_3821" n="HIAT:w" s="T391">emi͡e</ts>
                  <nts id="Seg_3822" n="HIAT:ip">…</nts>
                  <nts id="Seg_3823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_3825" n="HIAT:u" s="T393">
                  <ts e="T394" id="Seg_3827" n="HIAT:w" s="T393">Purgaː</ts>
                  <nts id="Seg_3828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_3830" n="HIAT:w" s="T394">dʼaːŋɨ</ts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_3833" n="HIAT:w" s="T395">gɨnar</ts>
                  <nts id="Seg_3834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_3836" n="HIAT:w" s="T396">bu͡o</ts>
                  <nts id="Seg_3837" n="HIAT:ip">.</nts>
                  <nts id="Seg_3838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402" id="Seg_3840" n="HIAT:u" s="T397">
                  <ts e="T397.tx-ChSA.1" id="Seg_3842" n="HIAT:w" s="T397">Ürüŋ</ts>
                  <nts id="Seg_3843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_3845" n="HIAT:w" s="T397.tx-ChSA.1">Kajaŋ</ts>
                  <nts id="Seg_3846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_3848" n="HIAT:w" s="T398">hire</ts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_3851" n="HIAT:w" s="T399">hürdeːk</ts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_3854" n="HIAT:w" s="T400">dʼaːŋɨ</ts>
                  <nts id="Seg_3855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_3857" n="HIAT:w" s="T401">hir</ts>
                  <nts id="Seg_3858" n="HIAT:ip">.</nts>
                  <nts id="Seg_3859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_3861" n="HIAT:u" s="T402">
                  <ts e="T403" id="Seg_3863" n="HIAT:w" s="T402">Ol</ts>
                  <nts id="Seg_3864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_3866" n="HIAT:w" s="T403">ihin</ts>
                  <nts id="Seg_3867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_3869" n="HIAT:w" s="T404">ol</ts>
                  <nts id="Seg_3870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_3872" n="HIAT:w" s="T405">rascenkalarɨŋ</ts>
                  <nts id="Seg_3873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_3875" n="HIAT:w" s="T406">ürdeppitter</ts>
                  <nts id="Seg_3876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_3878" n="HIAT:w" s="T407">maŋnajgɨttan</ts>
                  <nts id="Seg_3879" n="HIAT:ip">,</nts>
                  <nts id="Seg_3880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_3882" n="HIAT:w" s="T408">karaːntan</ts>
                  <nts id="Seg_3883" n="HIAT:ip">,</nts>
                  <nts id="Seg_3884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_3886" n="HIAT:w" s="T409">urukkuttan</ts>
                  <nts id="Seg_3887" n="HIAT:ip">.</nts>
                  <nts id="Seg_3888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_3890" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_3892" n="HIAT:w" s="T410">Taba</ts>
                  <nts id="Seg_3893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_3895" n="HIAT:w" s="T411">iːtiː</ts>
                  <nts id="Seg_3896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_3898" n="HIAT:w" s="T412">hürdeːk</ts>
                  <nts id="Seg_3899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_3901" n="HIAT:w" s="T413">bu͡o</ts>
                  <nts id="Seg_3902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_3904" n="HIAT:w" s="T414">kɨtaːnaga</ts>
                  <nts id="Seg_3905" n="HIAT:ip">.</nts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T427" id="Seg_3907" n="sc" s="T417">
               <ts e="T427" id="Seg_3909" n="HIAT:u" s="T417">
                  <nts id="Seg_3910" n="HIAT:ip">–</nts>
                  <nts id="Seg_3911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_3913" n="HIAT:w" s="T417">E</ts>
                  <nts id="Seg_3914" n="HIAT:ip">,</nts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3916" n="HIAT:ip">(</nts>
                  <ts e="T419" id="Seg_3918" n="HIAT:w" s="T418">ölör</ts>
                  <nts id="Seg_3919" n="HIAT:ip">)</nts>
                  <nts id="Seg_3920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_3922" n="HIAT:w" s="T419">ölör</ts>
                  <nts id="Seg_3923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_3925" n="HIAT:w" s="T420">kihiŋ</ts>
                  <nts id="Seg_3926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_3928" n="HIAT:w" s="T421">ölör</ts>
                  <nts id="Seg_3929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_3931" n="HIAT:w" s="T422">ol</ts>
                  <nts id="Seg_3932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3933" n="HIAT:ip">(</nts>
                  <ts e="T424" id="Seg_3935" n="HIAT:w" s="T423">kɨ</ts>
                  <nts id="Seg_3936" n="HIAT:ip">)</nts>
                  <nts id="Seg_3937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_3939" n="HIAT:w" s="T424">tabanɨ</ts>
                  <nts id="Seg_3940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_3942" n="HIAT:w" s="T425">gɨtta</ts>
                  <nts id="Seg_3943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_3945" n="HIAT:w" s="T426">barsan</ts>
                  <nts id="Seg_3946" n="HIAT:ip">.</nts>
                  <nts id="Seg_3947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T546" id="Seg_3948" n="sc" s="T441">
               <ts e="T446" id="Seg_3950" n="HIAT:u" s="T441">
                  <nts id="Seg_3951" n="HIAT:ip">–</nts>
                  <nts id="Seg_3952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_3954" n="HIAT:w" s="T441">Iti</ts>
                  <nts id="Seg_3955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_3957" n="HIAT:w" s="T442">haka</ts>
                  <nts id="Seg_3958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_3960" n="HIAT:w" s="T443">hiriger</ts>
                  <nts id="Seg_3961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_3963" n="HIAT:w" s="T444">onnuktar</ts>
                  <nts id="Seg_3964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_3966" n="HIAT:w" s="T445">ete</ts>
                  <nts id="Seg_3967" n="HIAT:ip">.</nts>
                  <nts id="Seg_3968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_3970" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_3972" n="HIAT:w" s="T446">Tabanɨ</ts>
                  <nts id="Seg_3973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_3975" n="HIAT:w" s="T447">hürdeːk</ts>
                  <nts id="Seg_3976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_3978" n="HIAT:w" s="T448">kɨlɨːlɨːllar</ts>
                  <nts id="Seg_3979" n="HIAT:ip">.</nts>
                  <nts id="Seg_3980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_3982" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_3984" n="HIAT:w" s="T449">Iːti͡ekterin</ts>
                  <nts id="Seg_3985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_3987" n="HIAT:w" s="T450">bagarallar</ts>
                  <nts id="Seg_3988" n="HIAT:ip">.</nts>
                  <nts id="Seg_3989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_3991" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_3993" n="HIAT:w" s="T451">Ara</ts>
                  <nts id="Seg_3994" n="HIAT:ip">,</nts>
                  <nts id="Seg_3995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_3997" n="HIAT:w" s="T452">karčɨ</ts>
                  <nts id="Seg_3998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_4000" n="HIAT:w" s="T453">ɨlallar</ts>
                  <nts id="Seg_4001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_4003" n="HIAT:w" s="T454">üčügej</ts>
                  <nts id="Seg_4004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_4006" n="HIAT:w" s="T455">bu͡olla</ts>
                  <nts id="Seg_4007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_4009" n="HIAT:w" s="T456">tabahɨttarɨŋ</ts>
                  <nts id="Seg_4010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_4012" n="HIAT:w" s="T457">ɨjga</ts>
                  <nts id="Seg_4013" n="HIAT:ip">.</nts>
                  <nts id="Seg_4014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T465" id="Seg_4016" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_4018" n="HIAT:w" s="T458">Tü͡örtüː</ts>
                  <nts id="Seg_4019" n="HIAT:ip">,</nts>
                  <nts id="Seg_4020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_4022" n="HIAT:w" s="T459">bi͡estiː</ts>
                  <nts id="Seg_4023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_4025" n="HIAT:w" s="T460">tɨːhɨččanɨ</ts>
                  <nts id="Seg_4026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_4028" n="HIAT:w" s="T461">biːr</ts>
                  <nts id="Seg_4029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_4031" n="HIAT:w" s="T462">ɨjga</ts>
                  <nts id="Seg_4032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_4034" n="HIAT:w" s="T463">ɨlallar</ts>
                  <nts id="Seg_4035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_4037" n="HIAT:w" s="T464">ginner</ts>
                  <nts id="Seg_4038" n="HIAT:ip">.</nts>
                  <nts id="Seg_4039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_4041" n="HIAT:u" s="T465">
                  <ts e="T466" id="Seg_4043" n="HIAT:w" s="T465">Bihigi</ts>
                  <nts id="Seg_4044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_4046" n="HIAT:w" s="T466">ol</ts>
                  <nts id="Seg_4047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_4049" n="HIAT:w" s="T467">kaččaga</ts>
                  <nts id="Seg_4050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4051" n="HIAT:ip">(</nts>
                  <ts e="T470" id="Seg_4053" n="HIAT:w" s="T468">t-</ts>
                  <nts id="Seg_4054" n="HIAT:ip">)</nts>
                  <nts id="Seg_4055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_4057" n="HIAT:w" s="T470">onnuga</ts>
                  <nts id="Seg_4058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_4060" n="HIAT:w" s="T471">daː</ts>
                  <nts id="Seg_4061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_4063" n="HIAT:w" s="T472">hu͡okput</ts>
                  <nts id="Seg_4064" n="HIAT:ip">,</nts>
                  <nts id="Seg_4065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_4067" n="HIAT:w" s="T474">törüt</ts>
                  <nts id="Seg_4068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_4070" n="HIAT:w" s="T475">hu͡ok</ts>
                  <nts id="Seg_4071" n="HIAT:ip">…</nts>
                  <nts id="Seg_4072" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_4074" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_4076" n="HIAT:w" s="T477">Meːne</ts>
                  <nts id="Seg_4077" n="HIAT:ip">,</nts>
                  <nts id="Seg_4078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_4080" n="HIAT:w" s="T478">bi͡es</ts>
                  <nts id="Seg_4081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_4083" n="HIAT:w" s="T479">hüːs</ts>
                  <nts id="Seg_4084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_4086" n="HIAT:w" s="T480">bu͡olu͡o</ts>
                  <nts id="Seg_4087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_4089" n="HIAT:w" s="T481">duː</ts>
                  <nts id="Seg_4090" n="HIAT:ip">,</nts>
                  <nts id="Seg_4091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_4093" n="HIAT:w" s="T482">avansa</ts>
                  <nts id="Seg_4094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_4096" n="HIAT:w" s="T483">kördük</ts>
                  <nts id="Seg_4097" n="HIAT:ip">.</nts>
                  <nts id="Seg_4098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_4100" n="HIAT:u" s="T484">
                  <ts e="T486" id="Seg_4102" n="HIAT:w" s="T484">Rascenkatabɨt</ts>
                  <nts id="Seg_4103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_4105" n="HIAT:w" s="T486">agɨjaga</ts>
                  <nts id="Seg_4106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_4108" n="HIAT:w" s="T487">bert</ts>
                  <nts id="Seg_4109" n="HIAT:ip">,</nts>
                  <nts id="Seg_4110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_4112" n="HIAT:w" s="T488">kʼepʼejkʼi</ts>
                  <nts id="Seg_4113" n="HIAT:ip">.</nts>
                  <nts id="Seg_4114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_4116" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_4118" n="HIAT:w" s="T489">Anɨga</ts>
                  <nts id="Seg_4119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_4121" n="HIAT:w" s="T490">di͡eri</ts>
                  <nts id="Seg_4122" n="HIAT:ip">.</nts>
                  <nts id="Seg_4123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_4125" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_4127" n="HIAT:w" s="T491">Uhuguttan</ts>
                  <nts id="Seg_4128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_4130" n="HIAT:w" s="T492">hüterdiler</ts>
                  <nts id="Seg_4131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_4133" n="HIAT:w" s="T493">anɨ</ts>
                  <nts id="Seg_4134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_4136" n="HIAT:w" s="T494">tu͡ogɨn</ts>
                  <nts id="Seg_4137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_4139" n="HIAT:w" s="T495">daːganɨ</ts>
                  <nts id="Seg_4140" n="HIAT:ip">.</nts>
                  <nts id="Seg_4141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T498" id="Seg_4143" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_4145" n="HIAT:w" s="T496">Haŋara</ts>
                  <nts id="Seg_4146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_4148" n="HIAT:w" s="T497">hataːn</ts>
                  <nts id="Seg_4149" n="HIAT:ip">:</nts>
                  <nts id="Seg_4150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_4152" n="HIAT:u" s="T498">
                  <nts id="Seg_4153" n="HIAT:ip">"</nts>
                  <ts e="T499" id="Seg_4155" n="HIAT:w" s="T498">Togo</ts>
                  <nts id="Seg_4156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_4158" n="HIAT:w" s="T499">tɨːtagɨt</ts>
                  <nts id="Seg_4159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_4161" n="HIAT:w" s="T500">iti</ts>
                  <nts id="Seg_4162" n="HIAT:ip">,</nts>
                  <nts id="Seg_4163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_4165" n="HIAT:w" s="T501">tu͡ok</ts>
                  <nts id="Seg_4166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_4168" n="HIAT:w" s="T502">gɨnaːrɨ</ts>
                  <nts id="Seg_4169" n="HIAT:ip">,</nts>
                  <nts id="Seg_4170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_4172" n="HIAT:w" s="T503">hopku͡oskɨt</ts>
                  <nts id="Seg_4173" n="HIAT:ip">,</nts>
                  <nts id="Seg_4174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_4176" n="HIAT:w" s="T504">hopku͡oskɨt</ts>
                  <nts id="Seg_4177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_4179" n="HIAT:w" s="T505">tabata</ts>
                  <nts id="Seg_4180" n="HIAT:ip">,</nts>
                  <nts id="Seg_4181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_4183" n="HIAT:w" s="T506">bejegit</ts>
                  <nts id="Seg_4184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_4186" n="HIAT:w" s="T507">hiːgit</ts>
                  <nts id="Seg_4187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_4189" n="HIAT:w" s="T508">bu͡o</ts>
                  <nts id="Seg_4190" n="HIAT:ip">"</nts>
                  <nts id="Seg_4191" n="HIAT:ip">,</nts>
                  <nts id="Seg_4192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_4194" n="HIAT:w" s="T509">di͡eččibin</ts>
                  <nts id="Seg_4195" n="HIAT:ip">.</nts>
                  <nts id="Seg_4196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T512" id="Seg_4198" n="HIAT:u" s="T510">
                  <nts id="Seg_4199" n="HIAT:ip">"</nts>
                  <ts e="T511" id="Seg_4201" n="HIAT:w" s="T510">Taŋnagɨt</ts>
                  <nts id="Seg_4202" n="HIAT:ip">,</nts>
                  <nts id="Seg_4203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_4205" n="HIAT:w" s="T511">kanʼɨːgɨt</ts>
                  <nts id="Seg_4206" n="HIAT:ip">.</nts>
                  <nts id="Seg_4207" n="HIAT:ip">"</nts>
                  <nts id="Seg_4208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_4210" n="HIAT:u" s="T512">
                  <nts id="Seg_4211" n="HIAT:ip">"</nts>
                  <ts e="T513" id="Seg_4213" n="HIAT:w" s="T512">Togo</ts>
                  <nts id="Seg_4214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_4216" n="HIAT:w" s="T513">hopku͡os</ts>
                  <nts id="Seg_4217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_4219" n="HIAT:w" s="T514">gi͡ene</ts>
                  <nts id="Seg_4220" n="HIAT:ip">,</nts>
                  <nts id="Seg_4221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_4223" n="HIAT:w" s="T515">diːgit</ts>
                  <nts id="Seg_4224" n="HIAT:ip">"</nts>
                  <nts id="Seg_4225" n="HIAT:ip">,</nts>
                  <nts id="Seg_4226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_4228" n="HIAT:w" s="T516">diːbin</ts>
                  <nts id="Seg_4229" n="HIAT:ip">.</nts>
                  <nts id="Seg_4230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_4232" n="HIAT:u" s="T517">
                  <nts id="Seg_4233" n="HIAT:ip">"</nts>
                  <ts e="T518" id="Seg_4235" n="HIAT:w" s="T517">Bejegit</ts>
                  <nts id="Seg_4236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_4238" n="HIAT:w" s="T518">baːjgɨt</ts>
                  <nts id="Seg_4239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_4241" n="HIAT:w" s="T519">diːn</ts>
                  <nts id="Seg_4242" n="HIAT:ip">"</nts>
                  <nts id="Seg_4243" n="HIAT:ip">,</nts>
                  <nts id="Seg_4244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_4246" n="HIAT:w" s="T520">diːbin</ts>
                  <nts id="Seg_4247" n="HIAT:ip">.</nts>
                  <nts id="Seg_4248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_4250" n="HIAT:u" s="T521">
                  <nts id="Seg_4251" n="HIAT:ip">"</nts>
                  <ts e="T522" id="Seg_4253" n="HIAT:w" s="T521">Ol</ts>
                  <nts id="Seg_4254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_4256" n="HIAT:w" s="T522">ihin</ts>
                  <nts id="Seg_4257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_4259" n="HIAT:w" s="T523">karčɨ</ts>
                  <nts id="Seg_4260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_4262" n="HIAT:w" s="T524">ɨlagɨt</ts>
                  <nts id="Seg_4263" n="HIAT:ip">,</nts>
                  <nts id="Seg_4264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4265" n="HIAT:ip">(</nts>
                  <ts e="T526" id="Seg_4267" n="HIAT:w" s="T525">tuguj</ts>
                  <nts id="Seg_4268" n="HIAT:ip">)</nts>
                  <nts id="Seg_4269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_4271" n="HIAT:w" s="T526">tu͡ogunan</ts>
                  <nts id="Seg_4272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4273" n="HIAT:ip">(</nts>
                  <ts e="T529" id="Seg_4275" n="HIAT:w" s="T527">ɨlaːbɨ-</ts>
                  <nts id="Seg_4276" n="HIAT:ip">)</nts>
                  <nts id="Seg_4277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_4279" n="HIAT:w" s="T529">ɨlɨ͡appɨt</ts>
                  <nts id="Seg_4280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_4282" n="HIAT:w" s="T530">diːgit</ts>
                  <nts id="Seg_4283" n="HIAT:ip">"</nts>
                  <nts id="Seg_4284" n="HIAT:ip">,</nts>
                  <nts id="Seg_4285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_4287" n="HIAT:w" s="T531">diːbin</ts>
                  <nts id="Seg_4288" n="HIAT:ip">.</nts>
                  <nts id="Seg_4289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_4291" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_4293" n="HIAT:w" s="T532">Anʼɨː</ts>
                  <nts id="Seg_4294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_4296" n="HIAT:w" s="T533">bu͡olu͡o</ts>
                  <nts id="Seg_4297" n="HIAT:ip">,</nts>
                  <nts id="Seg_4298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_4300" n="HIAT:w" s="T534">ulakannɨk</ts>
                  <nts id="Seg_4301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_4303" n="HIAT:w" s="T535">albɨnnaːbɨtɨm</ts>
                  <nts id="Seg_4304" n="HIAT:ip">,</nts>
                  <nts id="Seg_4305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_4307" n="HIAT:w" s="T536">di͡etekpinen</ts>
                  <nts id="Seg_4308" n="HIAT:ip">.</nts>
                  <nts id="Seg_4309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_4311" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_4313" n="HIAT:w" s="T537">Atɨːlammatagɨm</ts>
                  <nts id="Seg_4314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_4316" n="HIAT:w" s="T538">daː</ts>
                  <nts id="Seg_4317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_4319" n="HIAT:w" s="T539">haːtar</ts>
                  <nts id="Seg_4320" n="HIAT:ip">.</nts>
                  <nts id="Seg_4321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_4323" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_4325" n="HIAT:w" s="T540">Barɨta</ts>
                  <nts id="Seg_4326" n="HIAT:ip">,</nts>
                  <nts id="Seg_4327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_4329" n="HIAT:w" s="T541">kihiŋ</ts>
                  <nts id="Seg_4330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_4332" n="HIAT:w" s="T542">barɨta</ts>
                  <nts id="Seg_4333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_4335" n="HIAT:w" s="T543">atɨːlammɨta</ts>
                  <nts id="Seg_4336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_4338" n="HIAT:w" s="T544">ol</ts>
                  <nts id="Seg_4339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_4341" n="HIAT:w" s="T545">hopku͡ostan</ts>
                  <nts id="Seg_4342" n="HIAT:ip">.</nts>
                  <nts id="Seg_4343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T551" id="Seg_4344" n="sc" s="T549">
               <ts e="T551" id="Seg_4346" n="HIAT:u" s="T549">
                  <nts id="Seg_4347" n="HIAT:ip">–</nts>
                  <nts id="Seg_4348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_4350" n="HIAT:w" s="T549">Karčɨ</ts>
                  <nts id="Seg_4351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_4353" n="HIAT:w" s="T550">oŋostollor</ts>
                  <nts id="Seg_4354" n="HIAT:ip">.</nts>
                  <nts id="Seg_4355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T573" id="Seg_4356" n="sc" s="T571">
               <ts e="T573" id="Seg_4358" n="HIAT:u" s="T571">
                  <nts id="Seg_4359" n="HIAT:ip">–</nts>
                  <nts id="Seg_4360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_4362" n="HIAT:w" s="T571">Onnuk</ts>
                  <nts id="Seg_4363" n="HIAT:ip">,</nts>
                  <nts id="Seg_4364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_4366" n="HIAT:w" s="T572">onnuk</ts>
                  <nts id="Seg_4367" n="HIAT:ip">.</nts>
                  <nts id="Seg_4368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T585" id="Seg_4369" n="sc" s="T580">
               <ts e="T585" id="Seg_4371" n="HIAT:u" s="T580">
                  <nts id="Seg_4372" n="HIAT:ip">–</nts>
                  <nts id="Seg_4373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_4375" n="HIAT:w" s="T580">Tabahɨt</ts>
                  <nts id="Seg_4376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_4378" n="HIAT:w" s="T581">kihi</ts>
                  <nts id="Seg_4379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_4381" n="HIAT:w" s="T582">kemči</ts>
                  <nts id="Seg_4382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_4384" n="HIAT:w" s="T583">dʼe</ts>
                  <nts id="Seg_4385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_4387" n="HIAT:w" s="T584">onu</ts>
                  <nts id="Seg_4388" n="HIAT:ip">.</nts>
                  <nts id="Seg_4389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T643" id="Seg_4390" n="sc" s="T612">
               <ts e="T621" id="Seg_4392" n="HIAT:u" s="T612">
                  <nts id="Seg_4393" n="HIAT:ip">–</nts>
                  <ts e="T613" id="Seg_4395" n="HIAT:w" s="T612">Ti</ts>
                  <nts id="Seg_4396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_4398" n="HIAT:w" s="T613">üčügejdik</ts>
                  <nts id="Seg_4399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_4401" n="HIAT:w" s="T614">üleliːrim</ts>
                  <nts id="Seg_4402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_4404" n="HIAT:w" s="T615">ihin</ts>
                  <nts id="Seg_4405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_4407" n="HIAT:w" s="T616">taksɨbɨtɨm</ts>
                  <nts id="Seg_4408" n="HIAT:ip">,</nts>
                  <nts id="Seg_4409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_4411" n="HIAT:w" s="T617">kihileri</ts>
                  <nts id="Seg_4412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_4414" n="HIAT:w" s="T618">gɨtta</ts>
                  <nts id="Seg_4415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_4417" n="HIAT:w" s="T619">kepseterim</ts>
                  <nts id="Seg_4418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_4420" n="HIAT:w" s="T620">ihin</ts>
                  <nts id="Seg_4421" n="HIAT:ip">.</nts>
                  <nts id="Seg_4422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T628" id="Seg_4424" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_4426" n="HIAT:w" s="T621">Hi͡ese</ts>
                  <nts id="Seg_4427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_4429" n="HIAT:w" s="T622">eder</ts>
                  <nts id="Seg_4430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_4432" n="HIAT:w" s="T623">erdekpine</ts>
                  <nts id="Seg_4433" n="HIAT:ip">,</nts>
                  <nts id="Seg_4434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_4436" n="HIAT:w" s="T624">hürdeːk</ts>
                  <nts id="Seg_4437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_4439" n="HIAT:w" s="T625">huːstraj</ts>
                  <nts id="Seg_4440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_4442" n="HIAT:w" s="T626">etim</ts>
                  <nts id="Seg_4443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_4445" n="HIAT:w" s="T627">bu͡ol</ts>
                  <nts id="Seg_4446" n="HIAT:ip">.</nts>
                  <nts id="Seg_4447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_4449" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_4451" n="HIAT:w" s="T628">ɨ͡aldʼɨbɨtɨm</ts>
                  <nts id="Seg_4452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_4454" n="HIAT:w" s="T629">daː</ts>
                  <nts id="Seg_4455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_4457" n="HIAT:w" s="T630">ihin</ts>
                  <nts id="Seg_4458" n="HIAT:ip">,</nts>
                  <nts id="Seg_4459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_4461" n="HIAT:w" s="T631">kepsetiː</ts>
                  <nts id="Seg_4462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_4464" n="HIAT:w" s="T632">obu͡ojdaːk</ts>
                  <nts id="Seg_4465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_4467" n="HIAT:w" s="T633">etim</ts>
                  <nts id="Seg_4468" n="HIAT:ip">.</nts>
                  <nts id="Seg_4469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T643" id="Seg_4471" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_4473" n="HIAT:w" s="T634">Nʼuːčča</ts>
                  <nts id="Seg_4474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_4476" n="HIAT:w" s="T635">daː</ts>
                  <nts id="Seg_4477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_4479" n="HIAT:w" s="T636">bu͡ol</ts>
                  <nts id="Seg_4480" n="HIAT:ip">,</nts>
                  <nts id="Seg_4481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_4483" n="HIAT:w" s="T637">haka</ts>
                  <nts id="Seg_4484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_4486" n="HIAT:w" s="T638">daː</ts>
                  <nts id="Seg_4487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_4489" n="HIAT:w" s="T639">bu͡ol</ts>
                  <nts id="Seg_4490" n="HIAT:ip">,</nts>
                  <nts id="Seg_4491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_4493" n="HIAT:w" s="T640">omuk</ts>
                  <nts id="Seg_4494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_4496" n="HIAT:w" s="T641">daː</ts>
                  <nts id="Seg_4497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_4499" n="HIAT:w" s="T642">bu͡ol</ts>
                  <nts id="Seg_4500" n="HIAT:ip">.</nts>
                  <nts id="Seg_4501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T669" id="Seg_4502" n="sc" s="T652">
               <ts e="T657" id="Seg_4504" n="HIAT:u" s="T652">
                  <nts id="Seg_4505" n="HIAT:ip">–</nts>
                  <nts id="Seg_4506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_4508" n="HIAT:w" s="T652">Kün</ts>
                  <nts id="Seg_4509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_4511" n="HIAT:w" s="T653">aːjɨ</ts>
                  <nts id="Seg_4512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_4514" n="HIAT:w" s="T654">bu͡o</ts>
                  <nts id="Seg_4515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_4517" n="HIAT:w" s="T655">köröllörö</ts>
                  <nts id="Seg_4518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_4520" n="HIAT:w" s="T656">bagas</ts>
                  <nts id="Seg_4521" n="HIAT:ip">.</nts>
                  <nts id="Seg_4522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T661" id="Seg_4524" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_4526" n="HIAT:w" s="T657">Hɨːhanɨ</ts>
                  <nts id="Seg_4527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_4529" n="HIAT:w" s="T658">tu͡oktara</ts>
                  <nts id="Seg_4530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4531" n="HIAT:ip">(</nts>
                  <nts id="Seg_4532" n="HIAT:ip">(</nts>
                  <ats e="T660" id="Seg_4533" n="HIAT:non-pho" s="T659">…</ats>
                  <nts id="Seg_4534" n="HIAT:ip">)</nts>
                  <nts id="Seg_4535" n="HIAT:ip">)</nts>
                  <nts id="Seg_4536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_4538" n="HIAT:w" s="T660">gɨnagɨn</ts>
                  <nts id="Seg_4539" n="HIAT:ip">.</nts>
                  <nts id="Seg_4540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T669" id="Seg_4542" n="HIAT:u" s="T661">
                  <ts e="T662" id="Seg_4544" n="HIAT:w" s="T661">Olus</ts>
                  <nts id="Seg_4545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_4547" n="HIAT:w" s="T662">hɨːhata</ts>
                  <nts id="Seg_4548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_4550" n="HIAT:w" s="T663">hu͡okpun</ts>
                  <nts id="Seg_4551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_4553" n="HIAT:w" s="T664">bu͡o</ts>
                  <nts id="Seg_4554" n="HIAT:ip">,</nts>
                  <nts id="Seg_4555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_4557" n="HIAT:w" s="T665">ol</ts>
                  <nts id="Seg_4558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_4560" n="HIAT:w" s="T666">ihin</ts>
                  <nts id="Seg_4561" n="HIAT:ip">,</nts>
                  <nts id="Seg_4562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_4564" n="HIAT:w" s="T667">nʼuːččalar</ts>
                  <nts id="Seg_4565" n="HIAT:ip">…</nts>
                  <nts id="Seg_4566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T717" id="Seg_4567" n="sc" s="T693">
               <ts e="T697" id="Seg_4569" n="HIAT:u" s="T693">
                  <nts id="Seg_4570" n="HIAT:ip">–</nts>
                  <nts id="Seg_4571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_4573" n="HIAT:w" s="T693">Töröːbütün</ts>
                  <nts id="Seg_4574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_4576" n="HIAT:w" s="T694">kepsiːgin</ts>
                  <nts id="Seg_4577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_4579" n="HIAT:w" s="T695">bu͡o</ts>
                  <nts id="Seg_4580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_4582" n="HIAT:w" s="T696">mekti͡etin</ts>
                  <nts id="Seg_4583" n="HIAT:ip">.</nts>
                  <nts id="Seg_4584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T701" id="Seg_4586" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_4588" n="HIAT:w" s="T697">Tugutuŋ</ts>
                  <nts id="Seg_4589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_4591" n="HIAT:w" s="T698">kuččukaːn</ts>
                  <nts id="Seg_4592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_4594" n="HIAT:w" s="T699">bu͡o</ts>
                  <nts id="Seg_4595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_4597" n="HIAT:w" s="T700">ebit</ts>
                  <nts id="Seg_4598" n="HIAT:ip">.</nts>
                  <nts id="Seg_4599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_4601" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_4603" n="HIAT:w" s="T701">Tüːnü</ts>
                  <nts id="Seg_4604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_4606" n="HIAT:w" s="T702">bɨha</ts>
                  <nts id="Seg_4607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_4609" n="HIAT:w" s="T703">utujbakka</ts>
                  <nts id="Seg_4610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_4612" n="HIAT:w" s="T704">hɨldʼagɨn</ts>
                  <nts id="Seg_4613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_4615" n="HIAT:w" s="T705">bu͡o</ts>
                  <nts id="Seg_4616" n="HIAT:ip">,</nts>
                  <nts id="Seg_4617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_4619" n="HIAT:w" s="T706">ol</ts>
                  <nts id="Seg_4620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_4622" n="HIAT:w" s="T707">tugut</ts>
                  <nts id="Seg_4623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_4625" n="HIAT:w" s="T708">törüːrün</ts>
                  <nts id="Seg_4626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_4628" n="HIAT:w" s="T709">ketiː</ts>
                  <nts id="Seg_4629" n="HIAT:ip">.</nts>
                  <nts id="Seg_4630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T717" id="Seg_4632" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_4634" n="HIAT:w" s="T710">Ügüs</ts>
                  <nts id="Seg_4635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_4637" n="HIAT:w" s="T711">tabaga</ts>
                  <nts id="Seg_4638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4639" n="HIAT:ip">(</nts>
                  <ts e="T714" id="Seg_4641" n="HIAT:w" s="T712">el-</ts>
                  <nts id="Seg_4642" n="HIAT:ip">)</nts>
                  <nts id="Seg_4643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_4645" n="HIAT:w" s="T714">elbek</ts>
                  <nts id="Seg_4646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_4648" n="HIAT:w" s="T715">tugut</ts>
                  <nts id="Seg_4649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_4651" n="HIAT:w" s="T716">bu͡o</ts>
                  <nts id="Seg_4652" n="HIAT:ip">.</nts>
                  <nts id="Seg_4653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-ChSA">
            <ts e="T87" id="Seg_4654" n="sc" s="T6">
               <ts e="T7" id="Seg_4656" n="e" s="T6">–Teːtelerbit, </ts>
               <ts e="T8" id="Seg_4658" n="e" s="T7">inʼelerbit </ts>
               <ts e="T9" id="Seg_4660" n="e" s="T8">dʼe </ts>
               <ts e="T10" id="Seg_4662" n="e" s="T9">otto </ts>
               <ts e="T11" id="Seg_4664" n="e" s="T10">kɨrdʼagastar </ts>
               <ts e="T12" id="Seg_4666" n="e" s="T11">ete </ts>
               <ts e="T13" id="Seg_4668" n="e" s="T12">bu͡o. </ts>
               <ts e="T14" id="Seg_4670" n="e" s="T13">Unu, </ts>
               <ts e="T15" id="Seg_4672" n="e" s="T14">Anɨ </ts>
               <ts e="T16" id="Seg_4674" n="e" s="T15">bilbetter. </ts>
               <ts e="T18" id="Seg_4676" n="e" s="T16">(Krʼest-) </ts>
               <ts e="T19" id="Seg_4678" n="e" s="T18">meːne </ts>
               <ts e="T21" id="Seg_4680" n="e" s="T19">(hɨldʼala-), </ts>
               <ts e="T22" id="Seg_4682" n="e" s="T21">iliː </ts>
               <ts e="T23" id="Seg_4684" n="e" s="T22">battɨːllara; </ts>
               <ts e="T24" id="Seg_4686" n="e" s="T23">munnuk </ts>
               <ts e="T25" id="Seg_4688" n="e" s="T24">gɨnaːččɨbɨn, </ts>
               <ts e="T26" id="Seg_4690" n="e" s="T25">min </ts>
               <ts e="T27" id="Seg_4692" n="e" s="T26">emi͡e </ts>
               <ts e="T28" id="Seg_4694" n="e" s="T27">hanɨ </ts>
               <ts e="T29" id="Seg_4696" n="e" s="T28">hin, </ts>
               <ts e="T30" id="Seg_4698" n="e" s="T29">itinnik </ts>
               <ts e="T31" id="Seg_4700" n="e" s="T30">keri͡ete </ts>
               <ts e="T32" id="Seg_4702" n="e" s="T31">dʼe. </ts>
               <ts e="T33" id="Seg_4704" n="e" s="T32">Öjüm </ts>
               <ts e="T34" id="Seg_4706" n="e" s="T33">köppüte </ts>
               <ts e="T35" id="Seg_4708" n="e" s="T34">bert. </ts>
               <ts e="T36" id="Seg_4710" n="e" s="T35">Dʼɨlɨ </ts>
               <ts e="T37" id="Seg_4712" n="e" s="T36">bɨha </ts>
               <ts e="T38" id="Seg_4714" n="e" s="T37">ɨ͡aldʼɨbɨtɨm, </ts>
               <ts e="T39" id="Seg_4716" n="e" s="T38">kata </ts>
               <ts e="T40" id="Seg_4718" n="e" s="T39">ol </ts>
               <ts e="T41" id="Seg_4720" n="e" s="T40">ereːti </ts>
               <ts e="T42" id="Seg_4722" n="e" s="T41">amabɨn. </ts>
               <ts e="T43" id="Seg_4724" n="e" s="T42">Hi͡ese </ts>
               <ts e="T44" id="Seg_4726" n="e" s="T43">kihige </ts>
               <ts e="T45" id="Seg_4728" n="e" s="T44">kihi </ts>
               <ts e="T46" id="Seg_4730" n="e" s="T45">deter </ts>
               <ts e="T47" id="Seg_4732" n="e" s="T46">kördük </ts>
               <ts e="T48" id="Seg_4734" n="e" s="T47">hɨldʼabɨn </ts>
               <ts e="T49" id="Seg_4736" n="e" s="T48">eːt. </ts>
               <ts e="T50" id="Seg_4738" n="e" s="T49">Üleber </ts>
               <ts e="T51" id="Seg_4740" n="e" s="T50">daː </ts>
               <ts e="T52" id="Seg_4742" n="e" s="T51">ke. </ts>
               <ts e="T53" id="Seg_4744" n="e" s="T52">Ülege </ts>
               <ts e="T54" id="Seg_4746" n="e" s="T53">kuhagan. </ts>
               <ts e="T55" id="Seg_4748" n="e" s="T54">Bu </ts>
               <ts e="T56" id="Seg_4750" n="e" s="T55">ogolorbun </ts>
               <ts e="T57" id="Seg_4752" n="e" s="T56">iːtineːribin </ts>
               <ts e="T58" id="Seg_4754" n="e" s="T57">bu </ts>
               <ts e="T59" id="Seg_4756" n="e" s="T58">tugu </ts>
               <ts e="T61" id="Seg_4758" n="e" s="T59">dolu͡oj… </ts>
               <ts e="T62" id="Seg_4760" n="e" s="T61">Tɨːmmɨn </ts>
               <ts e="T63" id="Seg_4762" n="e" s="T62">bɨha </ts>
               <ts e="T64" id="Seg_4764" n="e" s="T63">möŋüneːččibin. </ts>
               <ts e="T65" id="Seg_4766" n="e" s="T64">Taba </ts>
               <ts e="T66" id="Seg_4768" n="e" s="T65">hɨrɨːta </ts>
               <ts e="T67" id="Seg_4770" n="e" s="T66">ajɨː </ts>
               <ts e="T68" id="Seg_4772" n="e" s="T67">du͡o </ts>
               <ts e="T69" id="Seg_4774" n="e" s="T68">urut. </ts>
               <ts e="T70" id="Seg_4776" n="e" s="T69">Elbege </ts>
               <ts e="T71" id="Seg_4778" n="e" s="T70">daː, </ts>
               <ts e="T72" id="Seg_4780" n="e" s="T71">ɨ͡arakana. </ts>
               <ts e="T73" id="Seg_4782" n="e" s="T72">Kihi </ts>
               <ts e="T74" id="Seg_4784" n="e" s="T73">heni͡ete </ts>
               <ts e="T75" id="Seg_4786" n="e" s="T74">esten </ts>
               <ts e="T76" id="Seg_4788" n="e" s="T75">baran </ts>
               <ts e="T77" id="Seg_4790" n="e" s="T76">keleːčči </ts>
               <ts e="T78" id="Seg_4792" n="e" s="T77">bukatɨn. </ts>
               <ts e="T79" id="Seg_4794" n="e" s="T78">U͡on </ts>
               <ts e="T80" id="Seg_4796" n="e" s="T79">biːrderge </ts>
               <ts e="T81" id="Seg_4798" n="e" s="T80">keleːččibit </ts>
               <ts e="T82" id="Seg_4800" n="e" s="T81">karaŋa </ts>
               <ts e="T83" id="Seg_4802" n="e" s="T82">kɨhɨn. </ts>
               <ts e="T84" id="Seg_4804" n="e" s="T83">Oː, </ts>
               <ts e="T85" id="Seg_4806" n="e" s="T84">tɨmnɨː </ts>
               <ts e="T86" id="Seg_4808" n="e" s="T85">bagaj </ts>
               <ts e="T87" id="Seg_4810" n="e" s="T86">bu͡o. </ts>
            </ts>
            <ts e="T122" id="Seg_4811" n="sc" s="T89">
               <ts e="T90" id="Seg_4813" n="e" s="T89">– Pöpügej </ts>
               <ts e="T91" id="Seg_4815" n="e" s="T90">tɨ͡atɨgar, </ts>
               <ts e="T92" id="Seg_4817" n="e" s="T91">mas, </ts>
               <ts e="T93" id="Seg_4819" n="e" s="T92">mas </ts>
               <ts e="T94" id="Seg_4821" n="e" s="T93">bu͡o </ts>
               <ts e="T95" id="Seg_4823" n="e" s="T94">haːtar </ts>
               <ts e="T96" id="Seg_4825" n="e" s="T95">bukatɨn. </ts>
               <ts e="T97" id="Seg_4827" n="e" s="T96">Taba </ts>
               <ts e="T98" id="Seg_4829" n="e" s="T97">kanna </ts>
               <ts e="T99" id="Seg_4831" n="e" s="T98">da </ts>
               <ts e="T100" id="Seg_4833" n="e" s="T99">köstübet. </ts>
               <ts e="T101" id="Seg_4835" n="e" s="T100">Onnuktarga </ts>
               <ts e="T102" id="Seg_4837" n="e" s="T101">üleleːn </ts>
               <ts e="T103" id="Seg_4839" n="e" s="T102">muŋnammɨtɨm </ts>
               <ts e="T104" id="Seg_4841" n="e" s="T103">min </ts>
               <ts e="T105" id="Seg_4843" n="e" s="T104">haːmaj. </ts>
               <ts e="T106" id="Seg_4845" n="e" s="T105">Ol </ts>
               <ts e="T107" id="Seg_4847" n="e" s="T106">gɨnaːrɨ, </ts>
               <ts e="T108" id="Seg_4849" n="e" s="T107">dʼolloːk </ts>
               <ts e="T109" id="Seg_4851" n="e" s="T108">bu͡olammɨn, </ts>
               <ts e="T110" id="Seg_4853" n="e" s="T109">tabam </ts>
               <ts e="T111" id="Seg_4855" n="e" s="T110">iːtillibite </ts>
               <ts e="T112" id="Seg_4857" n="e" s="T111">hürdeːk. </ts>
               <ts e="T113" id="Seg_4859" n="e" s="T112">U͡ontan </ts>
               <ts e="T114" id="Seg_4861" n="e" s="T113">taksa </ts>
               <ts e="T115" id="Seg_4863" n="e" s="T114">tɨːhɨččanɨ </ts>
               <ts e="T116" id="Seg_4865" n="e" s="T115">iːppitim, </ts>
               <ts e="T117" id="Seg_4867" n="e" s="T116">kas </ts>
               <ts e="T118" id="Seg_4869" n="e" s="T117">da </ts>
               <ts e="T119" id="Seg_4871" n="e" s="T118">stadaga </ts>
               <ts e="T120" id="Seg_4873" n="e" s="T119">bi͡erbitim, </ts>
               <ts e="T121" id="Seg_4875" n="e" s="T120">stadaga </ts>
               <ts e="T122" id="Seg_4877" n="e" s="T121">bi͡erbitim. </ts>
            </ts>
            <ts e="T141" id="Seg_4878" n="sc" s="T127">
               <ts e="T128" id="Seg_4880" n="e" s="T127">–Vtaroj, </ts>
               <ts e="T129" id="Seg_4882" n="e" s="T128">vtaroj </ts>
               <ts e="T130" id="Seg_4884" n="e" s="T129">brigada </ts>
               <ts e="T131" id="Seg_4886" n="e" s="T130">meːr, </ts>
               <ts e="T132" id="Seg_4888" n="e" s="T131">anɨga </ts>
               <ts e="T133" id="Seg_4890" n="e" s="T132">di͡eri. </ts>
               <ts e="T134" id="Seg_4892" n="e" s="T133">Bu </ts>
               <ts e="T135" id="Seg_4894" n="e" s="T134">anɨ </ts>
               <ts e="T136" id="Seg_4896" n="e" s="T135">u͡olɨm </ts>
               <ts e="T137" id="Seg_4898" n="e" s="T136">iti </ts>
               <ts e="T138" id="Seg_4900" n="e" s="T137">obu͡ojdanaːččɨ. </ts>
               <ts e="T139" id="Seg_4902" n="e" s="T138">Valodʼam. </ts>
               <ts e="T140" id="Seg_4904" n="e" s="T139">Hogotok </ts>
               <ts e="T141" id="Seg_4906" n="e" s="T140">u͡ollaːkpɨn. </ts>
            </ts>
            <ts e="T151" id="Seg_4907" n="sc" s="T147">
               <ts e="T148" id="Seg_4909" n="e" s="T147">–Mm, </ts>
               <ts e="T149" id="Seg_4911" n="e" s="T148">kɨrdʼabɨt, </ts>
               <ts e="T150" id="Seg_4913" n="e" s="T149">ikki͡emmit </ts>
               <ts e="T151" id="Seg_4915" n="e" s="T150">kɨrɨjdɨbɨt. </ts>
            </ts>
            <ts e="T243" id="Seg_4916" n="sc" s="T166">
               <ts e="T167" id="Seg_4918" n="e" s="T166">–Brʼigadʼiːrdarbɨt </ts>
               <ts e="T168" id="Seg_4920" n="e" s="T167">hi͡ese </ts>
               <ts e="T169" id="Seg_4922" n="e" s="T168">kihiler </ts>
               <ts e="T170" id="Seg_4924" n="e" s="T169">etilere. </ts>
               <ts e="T171" id="Seg_4926" n="e" s="T170">Onton </ts>
               <ts e="T173" id="Seg_4928" n="e" s="T171">(ü͡öt-) </ts>
               <ts e="T174" id="Seg_4930" n="e" s="T173">ü͡örene </ts>
               <ts e="T175" id="Seg_4932" n="e" s="T174">ü͡öremmitinen </ts>
               <ts e="T176" id="Seg_4934" n="e" s="T175">honon </ts>
               <ts e="T177" id="Seg_4936" n="e" s="T176">ü͡örenen </ts>
               <ts e="T178" id="Seg_4938" n="e" s="T177">kaːllakpɨt </ts>
               <ts e="T179" id="Seg_4940" n="e" s="T178">diː </ts>
               <ts e="T180" id="Seg_4942" n="e" s="T179">bihigi. </ts>
               <ts e="T181" id="Seg_4944" n="e" s="T180">Iti </ts>
               <ts e="T182" id="Seg_4946" n="e" s="T181">kečehiːge, </ts>
               <ts e="T183" id="Seg_4948" n="e" s="T182">tabaga, </ts>
               <ts e="T184" id="Seg_4950" n="e" s="T183">"tabanɨ </ts>
               <ts e="T185" id="Seg_4952" n="e" s="T184">hüterimeŋ" </ts>
               <ts e="T186" id="Seg_4954" n="e" s="T185">di͡en. </ts>
               <ts e="T187" id="Seg_4956" n="e" s="T186">Di͡eččibin </ts>
               <ts e="T188" id="Seg_4958" n="e" s="T187">bu͡o: </ts>
               <ts e="T189" id="Seg_4960" n="e" s="T188">"Tabanɨ </ts>
               <ts e="T190" id="Seg_4962" n="e" s="T189">hüterimeŋ, </ts>
               <ts e="T191" id="Seg_4964" n="e" s="T190">harsɨŋŋɨŋɨtɨn </ts>
               <ts e="T192" id="Seg_4966" n="e" s="T191">öjdöːŋ", </ts>
               <ts e="T193" id="Seg_4968" n="e" s="T192">di͡eččibin. </ts>
               <ts e="T194" id="Seg_4970" n="e" s="T193">"Harsi͡erda </ts>
               <ts e="T195" id="Seg_4972" n="e" s="T194">plaːnnana </ts>
               <ts e="T196" id="Seg_4974" n="e" s="T195">turuŋ", </ts>
               <ts e="T197" id="Seg_4976" n="e" s="T196">di͡eččibin, </ts>
               <ts e="T198" id="Seg_4978" n="e" s="T197">"kajdi͡ek </ts>
               <ts e="T199" id="Seg_4980" n="e" s="T198">üleliːrgitin, </ts>
               <ts e="T200" id="Seg_4982" n="e" s="T199">kajdi͡ek </ts>
               <ts e="T201" id="Seg_4984" n="e" s="T200">barargɨtɨn." </ts>
               <ts e="T202" id="Seg_4986" n="e" s="T201">Tabaŋ </ts>
               <ts e="T203" id="Seg_4988" n="e" s="T202">hütere </ts>
               <ts e="T204" id="Seg_4990" n="e" s="T203">ügühe </ts>
               <ts e="T205" id="Seg_4992" n="e" s="T204">muŋa, </ts>
               <ts e="T206" id="Seg_4994" n="e" s="T205">öj </ts>
               <ts e="T207" id="Seg_4996" n="e" s="T206">kimniːr </ts>
               <ts e="T208" id="Seg_4998" n="e" s="T207">hajɨn, </ts>
               <ts e="T209" id="Seg_5000" n="e" s="T208">hürdeːk </ts>
               <ts e="T210" id="Seg_5002" n="e" s="T209">ajallanna </ts>
               <ts e="T211" id="Seg_5004" n="e" s="T210">bu͡olaːččɨ. </ts>
               <ts e="T212" id="Seg_5006" n="e" s="T211">Ol </ts>
               <ts e="T213" id="Seg_5008" n="e" s="T212">bačča </ts>
               <ts e="T214" id="Seg_5010" n="e" s="T213">hɨldʼɨ͡akpar </ts>
               <ts e="T215" id="Seg_5012" n="e" s="T214">di͡eri </ts>
               <ts e="T217" id="Seg_5014" n="e" s="T215">bu͡o… </ts>
               <ts e="T218" id="Seg_5016" n="e" s="T217">Tu </ts>
               <ts e="T219" id="Seg_5018" n="e" s="T218">meːr </ts>
               <ts e="T220" id="Seg_5020" n="e" s="T219">üčügej </ts>
               <ts e="T221" id="Seg_5022" n="e" s="T220">aːtɨnan </ts>
               <ts e="T222" id="Seg_5024" n="e" s="T221">tagɨstɨm </ts>
               <ts e="T223" id="Seg_5026" n="e" s="T222">eːt </ts>
               <ts e="T224" id="Seg_5028" n="e" s="T223">otto – </ts>
               <ts e="T225" id="Seg_5030" n="e" s="T224">hajɨn </ts>
               <ts e="T226" id="Seg_5032" n="e" s="T225">da, </ts>
               <ts e="T227" id="Seg_5034" n="e" s="T226">kɨhɨn </ts>
               <ts e="T228" id="Seg_5036" n="e" s="T227">da. </ts>
               <ts e="T229" id="Seg_5038" n="e" s="T228">Hin </ts>
               <ts e="T230" id="Seg_5040" n="e" s="T229">üleleːtekke </ts>
               <ts e="T231" id="Seg_5042" n="e" s="T230">elbek </ts>
               <ts e="T232" id="Seg_5044" n="e" s="T231">dʼɨlɨ </ts>
               <ts e="T233" id="Seg_5046" n="e" s="T232">üleleːtim, </ts>
               <ts e="T234" id="Seg_5048" n="e" s="T233">bi͡eh-u͡on </ts>
               <ts e="T235" id="Seg_5050" n="e" s="T234">bi͡es </ts>
               <ts e="T236" id="Seg_5052" n="e" s="T235">dʼɨlɨ </ts>
               <ts e="T237" id="Seg_5054" n="e" s="T236">pʼensʼijaga </ts>
               <ts e="T238" id="Seg_5056" n="e" s="T237">kiːren </ts>
               <ts e="T239" id="Seg_5058" n="e" s="T238">baraːn </ts>
               <ts e="T240" id="Seg_5060" n="e" s="T239">össü͡ö </ts>
               <ts e="T241" id="Seg_5062" n="e" s="T240">üleleːbitim </ts>
               <ts e="T242" id="Seg_5064" n="e" s="T241">tabam </ts>
               <ts e="T243" id="Seg_5066" n="e" s="T242">iːtilleritten. </ts>
            </ts>
            <ts e="T288" id="Seg_5067" n="sc" s="T256">
               <ts e="T257" id="Seg_5069" n="e" s="T256">–Hüppüte </ts>
               <ts e="T258" id="Seg_5071" n="e" s="T257">bu͡o, </ts>
               <ts e="T259" id="Seg_5073" n="e" s="T258">ölön </ts>
               <ts e="T260" id="Seg_5075" n="e" s="T259">hüten. </ts>
               <ts e="T261" id="Seg_5077" n="e" s="T260">Hüppüte </ts>
               <ts e="T262" id="Seg_5079" n="e" s="T261">iti </ts>
               <ts e="T263" id="Seg_5081" n="e" s="T262">kimnerge </ts>
               <ts e="T264" id="Seg_5083" n="e" s="T263">Haskɨlaːktarga </ts>
               <ts e="T265" id="Seg_5085" n="e" s="T264">baran. </ts>
               <ts e="T266" id="Seg_5087" n="e" s="T265">Baran </ts>
               <ts e="T267" id="Seg_5089" n="e" s="T266">daːganɨ, </ts>
               <ts e="T268" id="Seg_5091" n="e" s="T267">kɨːl </ts>
               <ts e="T269" id="Seg_5093" n="e" s="T268">da </ts>
               <ts e="T270" id="Seg_5095" n="e" s="T269">iller. </ts>
               <ts e="T271" id="Seg_5097" n="e" s="T270">Kɨːl </ts>
               <ts e="T272" id="Seg_5099" n="e" s="T271">illeːčči. </ts>
               <ts e="T273" id="Seg_5101" n="e" s="T272">Mʼigrasʼijatɨgar, </ts>
               <ts e="T274" id="Seg_5103" n="e" s="T273">kühün </ts>
               <ts e="T275" id="Seg_5105" n="e" s="T274">keliːtiger </ts>
               <ts e="T276" id="Seg_5107" n="e" s="T275">ke </ts>
               <ts e="T277" id="Seg_5109" n="e" s="T276">börö </ts>
               <ts e="T278" id="Seg_5111" n="e" s="T277">hiːr </ts>
               <ts e="T279" id="Seg_5113" n="e" s="T278">ulakan </ts>
               <ts e="T280" id="Seg_5115" n="e" s="T279">hürdeːk. </ts>
               <ts e="T281" id="Seg_5117" n="e" s="T280">ɨ͡aldʼar </ts>
               <ts e="T282" id="Seg_5119" n="e" s="T281">onuga, </ts>
               <ts e="T283" id="Seg_5121" n="e" s="T282">atakta </ts>
               <ts e="T284" id="Seg_5123" n="e" s="T283">daː, </ts>
               <ts e="T285" id="Seg_5125" n="e" s="T284">honno </ts>
               <ts e="T286" id="Seg_5127" n="e" s="T285">ör </ts>
               <ts e="T287" id="Seg_5129" n="e" s="T286">baraːččɨta </ts>
               <ts e="T288" id="Seg_5131" n="e" s="T287">hu͡ok. </ts>
            </ts>
            <ts e="T316" id="Seg_5132" n="sc" s="T301">
               <ts e="T302" id="Seg_5134" n="e" s="T301">– Ölörtöröllör </ts>
               <ts e="T303" id="Seg_5136" n="e" s="T302">hanɨ, </ts>
               <ts e="T304" id="Seg_5138" n="e" s="T303">otto </ts>
               <ts e="T305" id="Seg_5140" n="e" s="T304">baːr </ts>
               <ts e="T306" id="Seg_5142" n="e" s="T305">bu͡ollagɨna. </ts>
               <ts e="T307" id="Seg_5144" n="e" s="T306">Diːller </ts>
               <ts e="T308" id="Seg_5146" n="e" s="T307">diː. </ts>
               <ts e="T309" id="Seg_5148" n="e" s="T308">Anɨ </ts>
               <ts e="T310" id="Seg_5150" n="e" s="T309">prʼemʼijalɨːr </ts>
               <ts e="T311" id="Seg_5152" n="e" s="T310">bu͡olbuttar, </ts>
               <ts e="T312" id="Seg_5154" n="e" s="T311">urut </ts>
               <ts e="T313" id="Seg_5156" n="e" s="T312">prʼemʼijalaːččɨta </ts>
               <ts e="T314" id="Seg_5158" n="e" s="T313">hu͡oktar </ts>
               <ts e="T315" id="Seg_5160" n="e" s="T314">((PAUSE)) </ts>
               <ts e="T316" id="Seg_5162" n="e" s="T315">etiler. </ts>
            </ts>
            <ts e="T346" id="Seg_5163" n="sc" s="T319">
               <ts e="T320" id="Seg_5165" n="e" s="T319">– Bihi͡enebit, </ts>
               <ts e="T321" id="Seg_5167" n="e" s="T320">kimmit </ts>
               <ts e="T322" id="Seg_5169" n="e" s="T321">ki͡e </ts>
               <ts e="T323" id="Seg_5171" n="e" s="T322">rascenkabɨt </ts>
               <ts e="T324" id="Seg_5173" n="e" s="T323">kuhagan </ts>
               <ts e="T325" id="Seg_5175" n="e" s="T324">ete, </ts>
               <ts e="T326" id="Seg_5177" n="e" s="T325">urut </ts>
               <ts e="T327" id="Seg_5179" n="e" s="T326">kɨra, </ts>
               <ts e="T328" id="Seg_5181" n="e" s="T327">küččügüj </ts>
               <ts e="T329" id="Seg_5183" n="e" s="T328">ete </ts>
               <ts e="T330" id="Seg_5185" n="e" s="T329">tababɨt </ts>
               <ts e="T331" id="Seg_5187" n="e" s="T330">ki͡ene </ts>
               <ts e="T332" id="Seg_5189" n="e" s="T331">ke. </ts>
               <ts e="T333" id="Seg_5191" n="e" s="T332">Onu </ts>
               <ts e="T334" id="Seg_5193" n="e" s="T333">min, </ts>
               <ts e="T335" id="Seg_5195" n="e" s="T334">"ürdetiŋ", </ts>
               <ts e="T336" id="Seg_5197" n="e" s="T335">diː </ts>
               <ts e="T337" id="Seg_5199" n="e" s="T336">hataːn </ts>
               <ts e="T338" id="Seg_5201" n="e" s="T337">anɨ, </ts>
               <ts e="T339" id="Seg_5203" n="e" s="T338">tɨl </ts>
               <ts e="T340" id="Seg_5205" n="e" s="T339">isti͡ektere </ts>
               <ts e="T341" id="Seg_5207" n="e" s="T340">bu͡o, </ts>
               <ts e="T342" id="Seg_5209" n="e" s="T341">anɨ </ts>
               <ts e="T343" id="Seg_5211" n="e" s="T342">ülehitterbit </ts>
               <ts e="T344" id="Seg_5213" n="e" s="T343">kuhagattar </ts>
               <ts e="T345" id="Seg_5215" n="e" s="T344">onuga </ts>
               <ts e="T346" id="Seg_5217" n="e" s="T345">dʼe. </ts>
            </ts>
            <ts e="T376" id="Seg_5218" n="sc" s="T352">
               <ts e="T353" id="Seg_5220" n="e" s="T352">–Mm, </ts>
               <ts e="T354" id="Seg_5222" n="e" s="T353">diːbit </ts>
               <ts e="T355" id="Seg_5224" n="e" s="T354">bu͡ol </ts>
               <ts e="T356" id="Seg_5226" n="e" s="T355">kü͡ömej </ts>
               <ts e="T357" id="Seg_5228" n="e" s="T356">iːti͡ekterin. </ts>
               <ts e="T358" id="Seg_5230" n="e" s="T357">Taba </ts>
               <ts e="T359" id="Seg_5232" n="e" s="T358">rascenkatɨn </ts>
               <ts e="T360" id="Seg_5234" n="e" s="T359">ürdeti͡ekke, </ts>
               <ts e="T361" id="Seg_5236" n="e" s="T360">diːbit. </ts>
               <ts e="T363" id="Seg_5238" n="e" s="T361">(Bihi͡e-) </ts>
               <ts e="T364" id="Seg_5240" n="e" s="T363">gini͡ennere </ts>
               <ts e="T365" id="Seg_5242" n="e" s="T364">kimnere, </ts>
               <ts e="T366" id="Seg_5244" n="e" s="T365">Ürüŋ Kajalar </ts>
               <ts e="T367" id="Seg_5246" n="e" s="T366">gi͡ettere </ts>
               <ts e="T368" id="Seg_5248" n="e" s="T367">bu͡o </ts>
               <ts e="T369" id="Seg_5250" n="e" s="T368">biːr </ts>
               <ts e="T370" id="Seg_5252" n="e" s="T369">tabaga </ts>
               <ts e="T371" id="Seg_5254" n="e" s="T370">kas </ts>
               <ts e="T372" id="Seg_5256" n="e" s="T371">di͡ebitterej? </ts>
               <ts e="T373" id="Seg_5258" n="e" s="T372">Huːtkaga </ts>
               <ts e="T374" id="Seg_5260" n="e" s="T373">tü͡ört, </ts>
               <ts e="T375" id="Seg_5262" n="e" s="T374">tü͡ört </ts>
               <ts e="T376" id="Seg_5264" n="e" s="T375">holku͡obaj. </ts>
            </ts>
            <ts e="T415" id="Seg_5265" n="sc" s="T380">
               <ts e="T381" id="Seg_5267" n="e" s="T380">– Iːtellerin </ts>
               <ts e="T382" id="Seg_5269" n="e" s="T381">ihin </ts>
               <ts e="T383" id="Seg_5271" n="e" s="T382">bu͡o, </ts>
               <ts e="T384" id="Seg_5273" n="e" s="T383">he-he. </ts>
               <ts e="T386" id="Seg_5275" n="e" s="T384">(Tabalɨːlla-) </ts>
               <ts e="T387" id="Seg_5277" n="e" s="T386">tabalaːn </ts>
               <ts e="T388" id="Seg_5279" n="e" s="T387">iːten </ts>
               <ts e="T389" id="Seg_5281" n="e" s="T388">taksallarɨn </ts>
               <ts e="T390" id="Seg_5283" n="e" s="T389">ihin. </ts>
               <ts e="T391" id="Seg_5285" n="e" s="T390">Gini͡ennere </ts>
               <ts e="T393" id="Seg_5287" n="e" s="T391">emi͡e… </ts>
               <ts e="T394" id="Seg_5289" n="e" s="T393">Purgaː </ts>
               <ts e="T395" id="Seg_5291" n="e" s="T394">dʼaːŋɨ </ts>
               <ts e="T396" id="Seg_5293" n="e" s="T395">gɨnar </ts>
               <ts e="T397" id="Seg_5295" n="e" s="T396">bu͡o. </ts>
               <ts e="T398" id="Seg_5297" n="e" s="T397">Ürüŋ Kajaŋ </ts>
               <ts e="T399" id="Seg_5299" n="e" s="T398">hire </ts>
               <ts e="T400" id="Seg_5301" n="e" s="T399">hürdeːk </ts>
               <ts e="T401" id="Seg_5303" n="e" s="T400">dʼaːŋɨ </ts>
               <ts e="T402" id="Seg_5305" n="e" s="T401">hir. </ts>
               <ts e="T403" id="Seg_5307" n="e" s="T402">Ol </ts>
               <ts e="T404" id="Seg_5309" n="e" s="T403">ihin </ts>
               <ts e="T405" id="Seg_5311" n="e" s="T404">ol </ts>
               <ts e="T406" id="Seg_5313" n="e" s="T405">rascenkalarɨŋ </ts>
               <ts e="T407" id="Seg_5315" n="e" s="T406">ürdeppitter </ts>
               <ts e="T408" id="Seg_5317" n="e" s="T407">maŋnajgɨttan, </ts>
               <ts e="T409" id="Seg_5319" n="e" s="T408">karaːntan, </ts>
               <ts e="T410" id="Seg_5321" n="e" s="T409">urukkuttan. </ts>
               <ts e="T411" id="Seg_5323" n="e" s="T410">Taba </ts>
               <ts e="T412" id="Seg_5325" n="e" s="T411">iːtiː </ts>
               <ts e="T413" id="Seg_5327" n="e" s="T412">hürdeːk </ts>
               <ts e="T414" id="Seg_5329" n="e" s="T413">bu͡o </ts>
               <ts e="T415" id="Seg_5331" n="e" s="T414">kɨtaːnaga. </ts>
            </ts>
            <ts e="T427" id="Seg_5332" n="sc" s="T417">
               <ts e="T418" id="Seg_5334" n="e" s="T417">– E, </ts>
               <ts e="T419" id="Seg_5336" n="e" s="T418">(ölör) </ts>
               <ts e="T420" id="Seg_5338" n="e" s="T419">ölör </ts>
               <ts e="T421" id="Seg_5340" n="e" s="T420">kihiŋ </ts>
               <ts e="T422" id="Seg_5342" n="e" s="T421">ölör </ts>
               <ts e="T423" id="Seg_5344" n="e" s="T422">ol </ts>
               <ts e="T424" id="Seg_5346" n="e" s="T423">(kɨ) </ts>
               <ts e="T425" id="Seg_5348" n="e" s="T424">tabanɨ </ts>
               <ts e="T426" id="Seg_5350" n="e" s="T425">gɨtta </ts>
               <ts e="T427" id="Seg_5352" n="e" s="T426">barsan. </ts>
            </ts>
            <ts e="T546" id="Seg_5353" n="sc" s="T441">
               <ts e="T442" id="Seg_5355" n="e" s="T441">– Iti </ts>
               <ts e="T443" id="Seg_5357" n="e" s="T442">haka </ts>
               <ts e="T444" id="Seg_5359" n="e" s="T443">hiriger </ts>
               <ts e="T445" id="Seg_5361" n="e" s="T444">onnuktar </ts>
               <ts e="T446" id="Seg_5363" n="e" s="T445">ete. </ts>
               <ts e="T447" id="Seg_5365" n="e" s="T446">Tabanɨ </ts>
               <ts e="T448" id="Seg_5367" n="e" s="T447">hürdeːk </ts>
               <ts e="T449" id="Seg_5369" n="e" s="T448">kɨlɨːlɨːllar. </ts>
               <ts e="T450" id="Seg_5371" n="e" s="T449">Iːti͡ekterin </ts>
               <ts e="T451" id="Seg_5373" n="e" s="T450">bagarallar. </ts>
               <ts e="T452" id="Seg_5375" n="e" s="T451">Ara, </ts>
               <ts e="T453" id="Seg_5377" n="e" s="T452">karčɨ </ts>
               <ts e="T454" id="Seg_5379" n="e" s="T453">ɨlallar </ts>
               <ts e="T455" id="Seg_5381" n="e" s="T454">üčügej </ts>
               <ts e="T456" id="Seg_5383" n="e" s="T455">bu͡olla </ts>
               <ts e="T457" id="Seg_5385" n="e" s="T456">tabahɨttarɨŋ </ts>
               <ts e="T458" id="Seg_5387" n="e" s="T457">ɨjga. </ts>
               <ts e="T459" id="Seg_5389" n="e" s="T458">Tü͡örtüː, </ts>
               <ts e="T460" id="Seg_5391" n="e" s="T459">bi͡estiː </ts>
               <ts e="T461" id="Seg_5393" n="e" s="T460">tɨːhɨččanɨ </ts>
               <ts e="T462" id="Seg_5395" n="e" s="T461">biːr </ts>
               <ts e="T463" id="Seg_5397" n="e" s="T462">ɨjga </ts>
               <ts e="T464" id="Seg_5399" n="e" s="T463">ɨlallar </ts>
               <ts e="T465" id="Seg_5401" n="e" s="T464">ginner. </ts>
               <ts e="T466" id="Seg_5403" n="e" s="T465">Bihigi </ts>
               <ts e="T467" id="Seg_5405" n="e" s="T466">ol </ts>
               <ts e="T468" id="Seg_5407" n="e" s="T467">kaččaga </ts>
               <ts e="T470" id="Seg_5409" n="e" s="T468">(t-) </ts>
               <ts e="T471" id="Seg_5411" n="e" s="T470">onnuga </ts>
               <ts e="T472" id="Seg_5413" n="e" s="T471">daː </ts>
               <ts e="T474" id="Seg_5415" n="e" s="T472">hu͡okput, </ts>
               <ts e="T475" id="Seg_5417" n="e" s="T474">törüt </ts>
               <ts e="T477" id="Seg_5419" n="e" s="T475">hu͡ok… </ts>
               <ts e="T478" id="Seg_5421" n="e" s="T477">Meːne, </ts>
               <ts e="T479" id="Seg_5423" n="e" s="T478">bi͡es </ts>
               <ts e="T480" id="Seg_5425" n="e" s="T479">hüːs </ts>
               <ts e="T481" id="Seg_5427" n="e" s="T480">bu͡olu͡o </ts>
               <ts e="T482" id="Seg_5429" n="e" s="T481">duː, </ts>
               <ts e="T483" id="Seg_5431" n="e" s="T482">avansa </ts>
               <ts e="T484" id="Seg_5433" n="e" s="T483">kördük. </ts>
               <ts e="T486" id="Seg_5435" n="e" s="T484">Rascenkatabɨt </ts>
               <ts e="T487" id="Seg_5437" n="e" s="T486">agɨjaga </ts>
               <ts e="T488" id="Seg_5439" n="e" s="T487">bert, </ts>
               <ts e="T489" id="Seg_5441" n="e" s="T488">kʼepʼejkʼi. </ts>
               <ts e="T490" id="Seg_5443" n="e" s="T489">Anɨga </ts>
               <ts e="T491" id="Seg_5445" n="e" s="T490">di͡eri. </ts>
               <ts e="T492" id="Seg_5447" n="e" s="T491">Uhuguttan </ts>
               <ts e="T493" id="Seg_5449" n="e" s="T492">hüterdiler </ts>
               <ts e="T494" id="Seg_5451" n="e" s="T493">anɨ </ts>
               <ts e="T495" id="Seg_5453" n="e" s="T494">tu͡ogɨn </ts>
               <ts e="T496" id="Seg_5455" n="e" s="T495">daːganɨ. </ts>
               <ts e="T497" id="Seg_5457" n="e" s="T496">Haŋara </ts>
               <ts e="T498" id="Seg_5459" n="e" s="T497">hataːn: </ts>
               <ts e="T499" id="Seg_5461" n="e" s="T498">"Togo </ts>
               <ts e="T500" id="Seg_5463" n="e" s="T499">tɨːtagɨt </ts>
               <ts e="T501" id="Seg_5465" n="e" s="T500">iti, </ts>
               <ts e="T502" id="Seg_5467" n="e" s="T501">tu͡ok </ts>
               <ts e="T503" id="Seg_5469" n="e" s="T502">gɨnaːrɨ, </ts>
               <ts e="T504" id="Seg_5471" n="e" s="T503">hopku͡oskɨt, </ts>
               <ts e="T505" id="Seg_5473" n="e" s="T504">hopku͡oskɨt </ts>
               <ts e="T506" id="Seg_5475" n="e" s="T505">tabata, </ts>
               <ts e="T507" id="Seg_5477" n="e" s="T506">bejegit </ts>
               <ts e="T508" id="Seg_5479" n="e" s="T507">hiːgit </ts>
               <ts e="T509" id="Seg_5481" n="e" s="T508">bu͡o", </ts>
               <ts e="T510" id="Seg_5483" n="e" s="T509">di͡eččibin. </ts>
               <ts e="T511" id="Seg_5485" n="e" s="T510">"Taŋnagɨt, </ts>
               <ts e="T512" id="Seg_5487" n="e" s="T511">kanʼɨːgɨt." </ts>
               <ts e="T513" id="Seg_5489" n="e" s="T512">"Togo </ts>
               <ts e="T514" id="Seg_5491" n="e" s="T513">hopku͡os </ts>
               <ts e="T515" id="Seg_5493" n="e" s="T514">gi͡ene, </ts>
               <ts e="T516" id="Seg_5495" n="e" s="T515">diːgit", </ts>
               <ts e="T517" id="Seg_5497" n="e" s="T516">diːbin. </ts>
               <ts e="T518" id="Seg_5499" n="e" s="T517">"Bejegit </ts>
               <ts e="T519" id="Seg_5501" n="e" s="T518">baːjgɨt </ts>
               <ts e="T520" id="Seg_5503" n="e" s="T519">diːn", </ts>
               <ts e="T521" id="Seg_5505" n="e" s="T520">diːbin. </ts>
               <ts e="T522" id="Seg_5507" n="e" s="T521">"Ol </ts>
               <ts e="T523" id="Seg_5509" n="e" s="T522">ihin </ts>
               <ts e="T524" id="Seg_5511" n="e" s="T523">karčɨ </ts>
               <ts e="T525" id="Seg_5513" n="e" s="T524">ɨlagɨt, </ts>
               <ts e="T526" id="Seg_5515" n="e" s="T525">(tuguj) </ts>
               <ts e="T527" id="Seg_5517" n="e" s="T526">tu͡ogunan </ts>
               <ts e="T529" id="Seg_5519" n="e" s="T527">(ɨlaːbɨ-) </ts>
               <ts e="T530" id="Seg_5521" n="e" s="T529">ɨlɨ͡appɨt </ts>
               <ts e="T531" id="Seg_5523" n="e" s="T530">diːgit", </ts>
               <ts e="T532" id="Seg_5525" n="e" s="T531">diːbin. </ts>
               <ts e="T533" id="Seg_5527" n="e" s="T532">Anʼɨː </ts>
               <ts e="T534" id="Seg_5529" n="e" s="T533">bu͡olu͡o, </ts>
               <ts e="T535" id="Seg_5531" n="e" s="T534">ulakannɨk </ts>
               <ts e="T536" id="Seg_5533" n="e" s="T535">albɨnnaːbɨtɨm, </ts>
               <ts e="T537" id="Seg_5535" n="e" s="T536">di͡etekpinen. </ts>
               <ts e="T538" id="Seg_5537" n="e" s="T537">Atɨːlammatagɨm </ts>
               <ts e="T539" id="Seg_5539" n="e" s="T538">daː </ts>
               <ts e="T540" id="Seg_5541" n="e" s="T539">haːtar. </ts>
               <ts e="T541" id="Seg_5543" n="e" s="T540">Barɨta, </ts>
               <ts e="T542" id="Seg_5545" n="e" s="T541">kihiŋ </ts>
               <ts e="T543" id="Seg_5547" n="e" s="T542">barɨta </ts>
               <ts e="T544" id="Seg_5549" n="e" s="T543">atɨːlammɨta </ts>
               <ts e="T545" id="Seg_5551" n="e" s="T544">ol </ts>
               <ts e="T546" id="Seg_5553" n="e" s="T545">hopku͡ostan. </ts>
            </ts>
            <ts e="T551" id="Seg_5554" n="sc" s="T549">
               <ts e="T550" id="Seg_5556" n="e" s="T549">– Karčɨ </ts>
               <ts e="T551" id="Seg_5558" n="e" s="T550">oŋostollor. </ts>
            </ts>
            <ts e="T573" id="Seg_5559" n="sc" s="T571">
               <ts e="T572" id="Seg_5561" n="e" s="T571">– Onnuk, </ts>
               <ts e="T573" id="Seg_5563" n="e" s="T572">onnuk. </ts>
            </ts>
            <ts e="T585" id="Seg_5564" n="sc" s="T580">
               <ts e="T581" id="Seg_5566" n="e" s="T580">– Tabahɨt </ts>
               <ts e="T582" id="Seg_5568" n="e" s="T581">kihi </ts>
               <ts e="T583" id="Seg_5570" n="e" s="T582">kemči </ts>
               <ts e="T584" id="Seg_5572" n="e" s="T583">dʼe </ts>
               <ts e="T585" id="Seg_5574" n="e" s="T584">onu. </ts>
            </ts>
            <ts e="T643" id="Seg_5575" n="sc" s="T612">
               <ts e="T613" id="Seg_5577" n="e" s="T612">–Ti </ts>
               <ts e="T614" id="Seg_5579" n="e" s="T613">üčügejdik </ts>
               <ts e="T615" id="Seg_5581" n="e" s="T614">üleliːrim </ts>
               <ts e="T616" id="Seg_5583" n="e" s="T615">ihin </ts>
               <ts e="T617" id="Seg_5585" n="e" s="T616">taksɨbɨtɨm, </ts>
               <ts e="T618" id="Seg_5587" n="e" s="T617">kihileri </ts>
               <ts e="T619" id="Seg_5589" n="e" s="T618">gɨtta </ts>
               <ts e="T620" id="Seg_5591" n="e" s="T619">kepseterim </ts>
               <ts e="T621" id="Seg_5593" n="e" s="T620">ihin. </ts>
               <ts e="T622" id="Seg_5595" n="e" s="T621">Hi͡ese </ts>
               <ts e="T623" id="Seg_5597" n="e" s="T622">eder </ts>
               <ts e="T624" id="Seg_5599" n="e" s="T623">erdekpine, </ts>
               <ts e="T625" id="Seg_5601" n="e" s="T624">hürdeːk </ts>
               <ts e="T626" id="Seg_5603" n="e" s="T625">huːstraj </ts>
               <ts e="T627" id="Seg_5605" n="e" s="T626">etim </ts>
               <ts e="T628" id="Seg_5607" n="e" s="T627">bu͡ol. </ts>
               <ts e="T629" id="Seg_5609" n="e" s="T628">ɨ͡aldʼɨbɨtɨm </ts>
               <ts e="T630" id="Seg_5611" n="e" s="T629">daː </ts>
               <ts e="T631" id="Seg_5613" n="e" s="T630">ihin, </ts>
               <ts e="T632" id="Seg_5615" n="e" s="T631">kepsetiː </ts>
               <ts e="T633" id="Seg_5617" n="e" s="T632">obu͡ojdaːk </ts>
               <ts e="T634" id="Seg_5619" n="e" s="T633">etim. </ts>
               <ts e="T635" id="Seg_5621" n="e" s="T634">Nʼuːčča </ts>
               <ts e="T636" id="Seg_5623" n="e" s="T635">daː </ts>
               <ts e="T637" id="Seg_5625" n="e" s="T636">bu͡ol, </ts>
               <ts e="T638" id="Seg_5627" n="e" s="T637">haka </ts>
               <ts e="T639" id="Seg_5629" n="e" s="T638">daː </ts>
               <ts e="T640" id="Seg_5631" n="e" s="T639">bu͡ol, </ts>
               <ts e="T641" id="Seg_5633" n="e" s="T640">omuk </ts>
               <ts e="T642" id="Seg_5635" n="e" s="T641">daː </ts>
               <ts e="T643" id="Seg_5637" n="e" s="T642">bu͡ol. </ts>
            </ts>
            <ts e="T669" id="Seg_5638" n="sc" s="T652">
               <ts e="T653" id="Seg_5640" n="e" s="T652">– Kün </ts>
               <ts e="T654" id="Seg_5642" n="e" s="T653">aːjɨ </ts>
               <ts e="T655" id="Seg_5644" n="e" s="T654">bu͡o </ts>
               <ts e="T656" id="Seg_5646" n="e" s="T655">köröllörö </ts>
               <ts e="T657" id="Seg_5648" n="e" s="T656">bagas. </ts>
               <ts e="T658" id="Seg_5650" n="e" s="T657">Hɨːhanɨ </ts>
               <ts e="T659" id="Seg_5652" n="e" s="T658">tu͡oktara </ts>
               <ts e="T660" id="Seg_5654" n="e" s="T659">((…)) </ts>
               <ts e="T661" id="Seg_5656" n="e" s="T660">gɨnagɨn. </ts>
               <ts e="T662" id="Seg_5658" n="e" s="T661">Olus </ts>
               <ts e="T663" id="Seg_5660" n="e" s="T662">hɨːhata </ts>
               <ts e="T664" id="Seg_5662" n="e" s="T663">hu͡okpun </ts>
               <ts e="T665" id="Seg_5664" n="e" s="T664">bu͡o, </ts>
               <ts e="T666" id="Seg_5666" n="e" s="T665">ol </ts>
               <ts e="T667" id="Seg_5668" n="e" s="T666">ihin, </ts>
               <ts e="T669" id="Seg_5670" n="e" s="T667">nʼuːččalar… </ts>
            </ts>
            <ts e="T717" id="Seg_5671" n="sc" s="T693">
               <ts e="T694" id="Seg_5673" n="e" s="T693">– Töröːbütün </ts>
               <ts e="T695" id="Seg_5675" n="e" s="T694">kepsiːgin </ts>
               <ts e="T696" id="Seg_5677" n="e" s="T695">bu͡o </ts>
               <ts e="T697" id="Seg_5679" n="e" s="T696">mekti͡etin. </ts>
               <ts e="T698" id="Seg_5681" n="e" s="T697">Tugutuŋ </ts>
               <ts e="T699" id="Seg_5683" n="e" s="T698">kuččukaːn </ts>
               <ts e="T700" id="Seg_5685" n="e" s="T699">bu͡o </ts>
               <ts e="T701" id="Seg_5687" n="e" s="T700">ebit. </ts>
               <ts e="T702" id="Seg_5689" n="e" s="T701">Tüːnü </ts>
               <ts e="T703" id="Seg_5691" n="e" s="T702">bɨha </ts>
               <ts e="T704" id="Seg_5693" n="e" s="T703">utujbakka </ts>
               <ts e="T705" id="Seg_5695" n="e" s="T704">hɨldʼagɨn </ts>
               <ts e="T706" id="Seg_5697" n="e" s="T705">bu͡o, </ts>
               <ts e="T707" id="Seg_5699" n="e" s="T706">ol </ts>
               <ts e="T708" id="Seg_5701" n="e" s="T707">tugut </ts>
               <ts e="T709" id="Seg_5703" n="e" s="T708">törüːrün </ts>
               <ts e="T710" id="Seg_5705" n="e" s="T709">ketiː. </ts>
               <ts e="T711" id="Seg_5707" n="e" s="T710">Ügüs </ts>
               <ts e="T712" id="Seg_5709" n="e" s="T711">tabaga </ts>
               <ts e="T714" id="Seg_5711" n="e" s="T712">(el-) </ts>
               <ts e="T715" id="Seg_5713" n="e" s="T714">elbek </ts>
               <ts e="T716" id="Seg_5715" n="e" s="T715">tugut </ts>
               <ts e="T717" id="Seg_5717" n="e" s="T716">bu͡o. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-ChSA">
            <ta e="T13" id="Seg_5718" s="T6">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.001 (001.002)</ta>
            <ta e="T16" id="Seg_5719" s="T13">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.002 (001.003)</ta>
            <ta e="T32" id="Seg_5720" s="T16">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.003 (001.004)</ta>
            <ta e="T35" id="Seg_5721" s="T32">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.004 (001.005)</ta>
            <ta e="T42" id="Seg_5722" s="T35">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.005 (001.006)</ta>
            <ta e="T49" id="Seg_5723" s="T42">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.006 (001.007)</ta>
            <ta e="T52" id="Seg_5724" s="T49">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.007 (001.008)</ta>
            <ta e="T54" id="Seg_5725" s="T52">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.008 (001.009)</ta>
            <ta e="T61" id="Seg_5726" s="T54">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.009 (001.010)</ta>
            <ta e="T64" id="Seg_5727" s="T61">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.010 (001.011)</ta>
            <ta e="T69" id="Seg_5728" s="T64">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.011 (001.012)</ta>
            <ta e="T72" id="Seg_5729" s="T69">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.012 (001.013)</ta>
            <ta e="T78" id="Seg_5730" s="T72">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.013 (001.014)</ta>
            <ta e="T83" id="Seg_5731" s="T78">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.014 (001.015)</ta>
            <ta e="T87" id="Seg_5732" s="T83">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.015 (001.016)</ta>
            <ta e="T96" id="Seg_5733" s="T89">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.016 (001.018)</ta>
            <ta e="T100" id="Seg_5734" s="T96">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.017 (001.019)</ta>
            <ta e="T105" id="Seg_5735" s="T100">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.018 (001.020)</ta>
            <ta e="T112" id="Seg_5736" s="T105">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.019 (001.021)</ta>
            <ta e="T122" id="Seg_5737" s="T112">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.020 (001.022)</ta>
            <ta e="T133" id="Seg_5738" s="T127">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.021 (001.024)</ta>
            <ta e="T138" id="Seg_5739" s="T133">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.022 (001.025)</ta>
            <ta e="T139" id="Seg_5740" s="T138">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.023 (001.026)</ta>
            <ta e="T141" id="Seg_5741" s="T139">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.024 (001.027)</ta>
            <ta e="T151" id="Seg_5742" s="T147">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.025 (001.029)</ta>
            <ta e="T170" id="Seg_5743" s="T166">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.026 (001.031)</ta>
            <ta e="T180" id="Seg_5744" s="T170">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.027 (001.032)</ta>
            <ta e="T186" id="Seg_5745" s="T180">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.028 (001.033)</ta>
            <ta e="T188" id="Seg_5746" s="T186">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.029 (001.034)</ta>
            <ta e="T193" id="Seg_5747" s="T188">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.030 (001.035)</ta>
            <ta e="T201" id="Seg_5748" s="T193">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.031 (001.036)</ta>
            <ta e="T211" id="Seg_5749" s="T201">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.032 (001.037)</ta>
            <ta e="T217" id="Seg_5750" s="T211">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.033 (001.038)</ta>
            <ta e="T228" id="Seg_5751" s="T217">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.034 (001.039)</ta>
            <ta e="T243" id="Seg_5752" s="T228">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.035 (001.040)</ta>
            <ta e="T260" id="Seg_5753" s="T256">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.036 (001.042)</ta>
            <ta e="T265" id="Seg_5754" s="T260">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.037 (001.043)</ta>
            <ta e="T270" id="Seg_5755" s="T265">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.038 (001.044)</ta>
            <ta e="T272" id="Seg_5756" s="T270">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.039 (001.045)</ta>
            <ta e="T280" id="Seg_5757" s="T272">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.040 (001.046)</ta>
            <ta e="T288" id="Seg_5758" s="T280">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.041 (001.047)</ta>
            <ta e="T306" id="Seg_5759" s="T301">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.042 (001.050)</ta>
            <ta e="T308" id="Seg_5760" s="T306">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.043 (001.051)</ta>
            <ta e="T316" id="Seg_5761" s="T308">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.044 (001.052)</ta>
            <ta e="T332" id="Seg_5762" s="T319">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.045 (001.054)</ta>
            <ta e="T346" id="Seg_5763" s="T332">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.046 (001.055)</ta>
            <ta e="T357" id="Seg_5764" s="T352">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.047 (001.057)</ta>
            <ta e="T361" id="Seg_5765" s="T357">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.048 (001.058)</ta>
            <ta e="T372" id="Seg_5766" s="T361">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.049 (001.059)</ta>
            <ta e="T376" id="Seg_5767" s="T372">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.050 (001.060)</ta>
            <ta e="T384" id="Seg_5768" s="T380">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.051 (001.062)</ta>
            <ta e="T390" id="Seg_5769" s="T384">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.052 (001.063)</ta>
            <ta e="T393" id="Seg_5770" s="T390">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.053 (001.064)</ta>
            <ta e="T397" id="Seg_5771" s="T393">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.054 (001.065)</ta>
            <ta e="T402" id="Seg_5772" s="T397">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.055 (001.066)</ta>
            <ta e="T410" id="Seg_5773" s="T402">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.056 (001.067)</ta>
            <ta e="T415" id="Seg_5774" s="T410">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.057 (001.068)</ta>
            <ta e="T427" id="Seg_5775" s="T417">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.058 (001.070)</ta>
            <ta e="T446" id="Seg_5776" s="T441">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.059 (001.072)</ta>
            <ta e="T449" id="Seg_5777" s="T446">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.060 (001.073)</ta>
            <ta e="T451" id="Seg_5778" s="T449">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.061 (001.074)</ta>
            <ta e="T458" id="Seg_5779" s="T451">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.062 (001.075)</ta>
            <ta e="T465" id="Seg_5780" s="T458">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.063 (001.076)</ta>
            <ta e="T477" id="Seg_5781" s="T465">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.064 (001.077)</ta>
            <ta e="T484" id="Seg_5782" s="T477">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.065 (001.078)</ta>
            <ta e="T489" id="Seg_5783" s="T484">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.066 (001.079)</ta>
            <ta e="T491" id="Seg_5784" s="T489">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.067 (001.080)</ta>
            <ta e="T496" id="Seg_5785" s="T491">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.068 (001.081)</ta>
            <ta e="T498" id="Seg_5786" s="T496">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.069 (001.082)</ta>
            <ta e="T510" id="Seg_5787" s="T498">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.070 (001.083)</ta>
            <ta e="T512" id="Seg_5788" s="T510">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.071 (001.084)</ta>
            <ta e="T517" id="Seg_5789" s="T512">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.072 (001.085)</ta>
            <ta e="T521" id="Seg_5790" s="T517">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.073 (001.086)</ta>
            <ta e="T532" id="Seg_5791" s="T521">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.074 (001.087)</ta>
            <ta e="T537" id="Seg_5792" s="T532">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.075 (001.088)</ta>
            <ta e="T540" id="Seg_5793" s="T537">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.076 (001.089)</ta>
            <ta e="T546" id="Seg_5794" s="T540">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.077 (001.090)</ta>
            <ta e="T551" id="Seg_5795" s="T549">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.078 (001.092)</ta>
            <ta e="T573" id="Seg_5796" s="T571">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.079 (001.094)</ta>
            <ta e="T585" id="Seg_5797" s="T580">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.080 (001.096)</ta>
            <ta e="T621" id="Seg_5798" s="T612">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.081 (001.099)</ta>
            <ta e="T628" id="Seg_5799" s="T621">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.082 (001.100)</ta>
            <ta e="T634" id="Seg_5800" s="T628">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.083 (001.101)</ta>
            <ta e="T643" id="Seg_5801" s="T634">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.084 (001.102)</ta>
            <ta e="T657" id="Seg_5802" s="T652">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.085 (001.104)</ta>
            <ta e="T661" id="Seg_5803" s="T657">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.086 (001.105)</ta>
            <ta e="T669" id="Seg_5804" s="T661">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.087 (001.106)</ta>
            <ta e="T697" id="Seg_5805" s="T693">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.088 (001.109)</ta>
            <ta e="T701" id="Seg_5806" s="T697">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.089 (001.110)</ta>
            <ta e="T710" id="Seg_5807" s="T701">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.090 (001.111)</ta>
            <ta e="T717" id="Seg_5808" s="T710">ChSA_KuNS_2004_ReindeerHerding_conv.ChSA.091 (001.112)</ta>
         </annotation>
         <annotation name="st" tierref="st-ChSA">
            <ta e="T13" id="Seg_5809" s="T6">Ч: Тээтэлэрбит, иньэлэрбит дьэ отто кырдьагастар этэ буо.</ta>
            <ta e="T16" id="Seg_5810" s="T13">Уну, Аны билбэттэр.</ta>
            <ta e="T32" id="Seg_5811" s="T16">(Крест…) мээнэ һылдьала (һылдьаллара), илии баттыллара – муннук гынааччыбын, мин эмиэ һаны һин, итинник кэриэтэ дьэ.</ta>
            <ta e="T35" id="Seg_5812" s="T32">Өйүм көппүтэ бэрт. </ta>
            <ta e="T42" id="Seg_5813" s="T35">Дьылы быһа ыалдьыбытым, ката ол эрээти амабын.</ta>
            <ta e="T49" id="Seg_5814" s="T42">Һиэсэ киһигэ киһи дэтэр көрдүк һылдьабын ээт.</ta>
            <ta e="T52" id="Seg_5815" s="T49">Үлэбэр даа кэ.</ta>
            <ta e="T54" id="Seg_5816" s="T52">Үлэгэ куһаган.</ta>
            <ta e="T61" id="Seg_5817" s="T54">Бу оголорбун итинээрибин бу тугу долуой…</ta>
            <ta e="T64" id="Seg_5818" s="T61">Тыыммын быһа мөӈүнээччибин.</ta>
            <ta e="T69" id="Seg_5819" s="T64">Таба һырыыта айыы дуо урут.</ta>
            <ta e="T72" id="Seg_5820" s="T69">Элбэгэ даа, ыаракана.</ta>
            <ta e="T78" id="Seg_5821" s="T72">Киһи һэниэтэ эстэн баран кэлээччи букатын.</ta>
            <ta e="T83" id="Seg_5822" s="T78">Уон биирдэргэ кэлэччибит караӈа кыһын.</ta>
            <ta e="T87" id="Seg_5823" s="T83">Оо, тымныы багай буо.</ta>
            <ta e="T96" id="Seg_5824" s="T89">Ч: Пөпүгэй тыатыгар, мас, мас буо һаатар букатын.</ta>
            <ta e="T100" id="Seg_5825" s="T96">Таба канна да көстүбэт.</ta>
            <ta e="T105" id="Seg_5826" s="T100">Оннуктарга үлэлээн муӈнаммытым мин һаaмай.</ta>
            <ta e="T112" id="Seg_5827" s="T105">Ол гынаары, дьоллоок буоламмын, табам иитиллибитэ һүрдээк.</ta>
            <ta e="T122" id="Seg_5828" s="T112">Уонтан такса тыыһыччаны ииппитимм кас да стадага биэрбитим, стадага биэрбитим.</ta>
            <ta e="T133" id="Seg_5829" s="T127">Ч: Второй, второй бригада мээр, аныга диэри.</ta>
            <ta e="T138" id="Seg_5830" s="T133">Бу аны уолым ити обуойданааччы.</ta>
            <ta e="T139" id="Seg_5831" s="T138">Володьам.</ta>
            <ta e="T141" id="Seg_5832" s="T139">Һоготок уоллaакпын.</ta>
            <ta e="T151" id="Seg_5833" s="T147">Ч: М-м, кырдьабыт, иккиэммит кырыйдыбыт.</ta>
            <ta e="T170" id="Seg_5834" s="T166">Ч: Бригадиирдарбыт һиэсэ киһилэр этилэрэ.</ta>
            <ta e="T180" id="Seg_5835" s="T170">Онтон үөрэннэ үөрэммитинэн һонон үөрэнэн кааллакпыт дии биһиги.</ta>
            <ta e="T186" id="Seg_5836" s="T180">Ити кэчэһиигэ, табага, табаны һүтэримэӈ диэн.</ta>
            <ta e="T188" id="Seg_5837" s="T186">Диэччибин буо:</ta>
            <ta e="T193" id="Seg_5838" s="T188">"Табаны һүтэримэӈ, hарсыӈӈыӈытын өйдөөӈ", – диэччибин.</ta>
            <ta e="T201" id="Seg_5839" s="T193">"Һарсиэрда планнана туруӈ, – диэччибин, – кайдиэк үлэлииргитин, кайдиэк бараргытын".</ta>
            <ta e="T211" id="Seg_5840" s="T201">Табаӈ һүтэрэ үгүһэ муӈа өй кимниирэ киэ (һайын), һүрдээк айалланна буолааччы.</ta>
            <ta e="T217" id="Seg_5841" s="T211">Ол бачча һылдьыакпар диэри буо…</ta>
            <ta e="T228" id="Seg_5842" s="T217">Ту мээр үчүгэй аатынан тагыстым ээт отто – һайын да, кыһын да.</ta>
            <ta e="T243" id="Seg_5843" s="T228">Һин үлэлээтэккэ элбэк дьылы үлэлээтим, биэһуон (биэс уон) биэс дьылы пенсияга киирэн бараан өссүө үлэлээбитим табам иитиллэриттэн.</ta>
            <ta e="T260" id="Seg_5844" s="T256">Ч: Һүппүтэ буо, өлөн һүтэн.</ta>
            <ta e="T265" id="Seg_5845" s="T260">Һүппүтэ ити кимнэргэ Һаскылаактарга баран.</ta>
            <ta e="T270" id="Seg_5846" s="T265">Баран дааганы, кыыл да иллэр.</ta>
            <ta e="T272" id="Seg_5847" s="T270">Кыыл илээччи.</ta>
            <ta e="T280" id="Seg_5848" s="T272">Миграциятыгар, күһүн кэлиитигэр киэ бөрө һиир улакан һүрдээк. </ta>
            <ta e="T288" id="Seg_5849" s="T280">Ыалдьар онуга, атактаата даа, һонно өр барааччыта һуок.</ta>
            <ta e="T306" id="Seg_5850" s="T301">Ч: Өлөртөрөллөр һаны, отто баар буоллагына.</ta>
            <ta e="T308" id="Seg_5851" s="T306">Дииллэр дии.</ta>
            <ta e="T316" id="Seg_5852" s="T308">Аны премиялыыр буолбуттар, урут премиялaаччыта һуоктар. Этилэр.</ta>
            <ta e="T332" id="Seg_5853" s="T319">Ч: Биһиэнэбит, киммит киэ расценкабыт куһаган этэ, урут кыра, күччүгүй этэ табабыт киэнэ киэ. </ta>
            <ta e="T346" id="Seg_5854" s="T332">Ону мин, үрдэтиӈ, дии һатаан аны, тыл истиэктэрэ буо, аны үлээһиттэрбит куһагаттар онуга дьэ.</ta>
            <ta e="T357" id="Seg_5855" s="T352">Ч: М-м, диибит буол күөмэй ииттэктэрин.</ta>
            <ta e="T361" id="Seg_5856" s="T357">Таба расценкатын үрдэтиэккэ, диибит.</ta>
            <ta e="T372" id="Seg_5857" s="T361">(Биһиэ…) гиниэннэрэ кимнэрэ, Үрүӈ Кайалар гиэттэрэ буо биир табага кас диэбиттэрэй?</ta>
            <ta e="T376" id="Seg_5858" s="T372">Һууткага түөрт, түөрт һолкуобай.</ta>
            <ta e="T384" id="Seg_5859" s="T380">Ч: Иитэллэрин иһин буо, һэ-һэ.</ta>
            <ta e="T390" id="Seg_5860" s="T384">(Табалыылла…) табалаан итэн таксалларын иһин.</ta>
            <ta e="T393" id="Seg_5861" s="T390">Гиниэннэрэ эмиэ…</ta>
            <ta e="T397" id="Seg_5862" s="T393">Пургаа дьааӈы гынар буо.</ta>
            <ta e="T402" id="Seg_5863" s="T397">Үрүӈ Кайаӈ һирэ һүрдээк дьааӈы һир.</ta>
            <ta e="T410" id="Seg_5864" s="T402">Ол иһин ол расценкаларыӈ үрдэппиттэр маӈнайгыттан, караантан, уруккуттан.</ta>
            <ta e="T415" id="Seg_5865" s="T410">Таба иитии һүрдээк буо кытаанага.</ta>
            <ta e="T427" id="Seg_5866" s="T417">Ч: Э, (өлөр) өлөр киһиӈ өлөр ол (кы?) таабаны гытта барсан.</ta>
            <ta e="T446" id="Seg_5867" s="T441">Ч: Ити һака һиригэр оннуктар этэ.</ta>
            <ta e="T449" id="Seg_5868" s="T446">Табаны һүрдээк кылыылыыллар.</ta>
            <ta e="T451" id="Seg_5869" s="T449">Иитиэктэрин багараллар.</ta>
            <ta e="T458" id="Seg_5870" s="T451">Ара, карчы ылаллара үчүгэй буолла табаһыттарыӈ ыйга.</ta>
            <ta e="T465" id="Seg_5871" s="T458">Түөртүү, биэстии тыыһыччаны биир ыйга ылаллар гиннэр.</ta>
            <ta e="T477" id="Seg_5872" s="T465">Биһиги ол каччага (тэ…) оннуга даа һуок буо, төрүт һуок… </ta>
            <ta e="T484" id="Seg_5873" s="T477">Мээнэ, биэс һүүс буолуо дуу, аванса көрдүк. </ta>
            <ta e="T489" id="Seg_5874" s="T484">Расценката быт агыйага бэрт, кэпэйки.</ta>
            <ta e="T491" id="Seg_5875" s="T489">Аныга диэри.</ta>
            <ta e="T496" id="Seg_5876" s="T491">Ууһугуттан һүтэрдилэр аны туогын дааганы.</ta>
            <ta e="T498" id="Seg_5877" s="T496">Һаӈара һатаан:</ta>
            <ta e="T510" id="Seg_5878" s="T498">"Того тыытагыт ити, туок гынаары, һопкуоскыт, һопкуоскыт табата, бэйэгит һиигит буо, – диэччибин.</ta>
            <ta e="T512" id="Seg_5879" s="T510">– Таӈнагыт, каньыыгыт."</ta>
            <ta e="T517" id="Seg_5880" s="T512">"Того һопкуос гиэнэ, диигит, – диибин.</ta>
            <ta e="T521" id="Seg_5881" s="T517">– Бэйэгит баайгыт диин, – диибин.</ta>
            <ta e="T532" id="Seg_5882" s="T521">– Ол иһин каарчы ылагыт, (туугу..) туогунан ылыаппыт диигит," – диибин.</ta>
            <ta e="T537" id="Seg_5883" s="T532">Аньыы буолуо, улаканнык албыннаабытым, диэтэкпинэн.</ta>
            <ta e="T540" id="Seg_5884" s="T537">Атыыламматагым даа һаатар.</ta>
            <ta e="T546" id="Seg_5885" s="T540">Барыта, киһиӈ барыта атыыламмыта ол һопкуостан.</ta>
            <ta e="T551" id="Seg_5886" s="T549">Ч: Карчы оӈостоллор.</ta>
            <ta e="T573" id="Seg_5887" s="T571">Ч: Оннук, оннук.</ta>
            <ta e="T585" id="Seg_5888" s="T580">Ч: Табаһыт киһи кэмчирдэ дьэ ону.</ta>
            <ta e="T621" id="Seg_5889" s="T612">Ч: Ти үчүгэйдик үлэлиирим иһин таксыбытым, киһилээри гытта кэпсэтэрим иһин.</ta>
            <ta e="T628" id="Seg_5890" s="T621">Һиэсэ эдэр эрдэкпинэ, һүрдээк һуустрай этим буол.</ta>
            <ta e="T634" id="Seg_5891" s="T628">Ыалдьыбытым даа иһин, кэпсэтии обуойдаак этим. </ta>
            <ta e="T643" id="Seg_5892" s="T634">Ньуучча даа буол, һака даа буол, омук даа буол.</ta>
            <ta e="T657" id="Seg_5893" s="T652">Ч: Күн аайы буо көрөллөрө багас.</ta>
            <ta e="T661" id="Seg_5894" s="T657">Һыыһаны туоктара эрдэтэн гынагын.</ta>
            <ta e="T669" id="Seg_5895" s="T661">Олус һыыһата һуокпун буо, ол иһин, ньууччалар…. </ta>
            <ta e="T697" id="Seg_5896" s="T693">Ч: Төрөөбүтүн кэпсиигин буо мэктиэтин.</ta>
            <ta e="T701" id="Seg_5897" s="T697">Тугутуӈ куччукаан буо.</ta>
            <ta e="T710" id="Seg_5898" s="T701">Түүнү быһа утуйбакка һылдьагын буо, ол тугут төрүүрүн кэтии.</ta>
            <ta e="T717" id="Seg_5899" s="T710">Үгүс табага (эл…) элбэк тугут буо.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-ChSA">
            <ta e="T13" id="Seg_5900" s="T6">–Teːtelerbit, inʼelerbit dʼe otto kɨrdʼagastar ete bu͡o. </ta>
            <ta e="T16" id="Seg_5901" s="T13">Unu, Anɨ bilbetter. </ta>
            <ta e="T32" id="Seg_5902" s="T16">(Krʼest-) meːne (hɨldʼala-), iliː battɨːllara; munnuk gɨnaːččɨbɨn, min emi͡e hanɨ hin, itinnik keri͡ete dʼe. </ta>
            <ta e="T35" id="Seg_5903" s="T32">Öjüm köppüte bert. </ta>
            <ta e="T42" id="Seg_5904" s="T35">Dʼɨlɨ bɨha ɨ͡aldʼɨbɨtɨm, kata ol ereːti amabɨn. </ta>
            <ta e="T49" id="Seg_5905" s="T42">Hi͡ese kihige kihi deter kördük hɨldʼabɨn eːt. </ta>
            <ta e="T52" id="Seg_5906" s="T49">Üleber daː ke. </ta>
            <ta e="T54" id="Seg_5907" s="T52">Ülege kuhagan. </ta>
            <ta e="T61" id="Seg_5908" s="T54">Bu ogolorbun iːtineːribin bu tugu dolu͡oj… </ta>
            <ta e="T64" id="Seg_5909" s="T61">Tɨːmmɨn bɨha möŋüneːččibin. </ta>
            <ta e="T69" id="Seg_5910" s="T64">Taba hɨrɨːta ajɨː du͡o urut. </ta>
            <ta e="T72" id="Seg_5911" s="T69">Elbege daː, ɨ͡arakana. </ta>
            <ta e="T78" id="Seg_5912" s="T72">Kihi heni͡ete esten baran keleːčči bukatɨn. </ta>
            <ta e="T83" id="Seg_5913" s="T78">U͡on biːrderge keleːččibit karaŋa kɨhɨn. </ta>
            <ta e="T87" id="Seg_5914" s="T83">Oː, tɨmnɨː bagaj bu͡o. </ta>
            <ta e="T96" id="Seg_5915" s="T89">– Pöpügej tɨ͡atɨgar, mas, mas bu͡o haːtar bukatɨn. </ta>
            <ta e="T100" id="Seg_5916" s="T96">Taba kanna da köstübet. </ta>
            <ta e="T105" id="Seg_5917" s="T100">Onnuktarga üleleːn muŋnammɨtɨm min haːmaj. </ta>
            <ta e="T112" id="Seg_5918" s="T105">Ol gɨnaːrɨ, dʼolloːk bu͡olammɨn, tabam iːtillibite hürdeːk. </ta>
            <ta e="T122" id="Seg_5919" s="T112">U͡ontan taksa tɨːhɨččanɨ iːppitim, kas da stadaga bi͡erbitim, stadaga bi͡erbitim. </ta>
            <ta e="T133" id="Seg_5920" s="T127">–Vtaroj, vtaroj brigada meːr, anɨga di͡eri. </ta>
            <ta e="T138" id="Seg_5921" s="T133">Bu anɨ u͡olɨm iti obu͡ojdanaːččɨ. </ta>
            <ta e="T139" id="Seg_5922" s="T138">Valodʼam. </ta>
            <ta e="T141" id="Seg_5923" s="T139">Hogotok u͡ollaːkpɨn. </ta>
            <ta e="T151" id="Seg_5924" s="T147">–Mm, kɨrdʼabɨt, ikki͡emmit kɨrɨjdɨbɨt. </ta>
            <ta e="T170" id="Seg_5925" s="T166">–Brʼigadʼiːrdarbɨt hi͡ese kihiler etilere. </ta>
            <ta e="T180" id="Seg_5926" s="T170">Onton (ü͡öt-) ü͡örene ü͡öremmitinen honon ü͡örenen kaːllakpɨt diː bihigi. </ta>
            <ta e="T186" id="Seg_5927" s="T180">Iti kečehiːge, tabaga, "tabanɨ hüterimeŋ" di͡en. </ta>
            <ta e="T188" id="Seg_5928" s="T186">Di͡eččibin bu͡o: </ta>
            <ta e="T193" id="Seg_5929" s="T188">"Tabanɨ hüterimeŋ, harsɨŋŋɨŋɨtɨn öjdöːŋ", di͡eččibin. </ta>
            <ta e="T201" id="Seg_5930" s="T193">"Harsi͡erda plaːnnana turuŋ", di͡eččibin, "kajdi͡ek üleliːrgitin, kajdi͡ek barargɨtɨn." </ta>
            <ta e="T211" id="Seg_5931" s="T201">Tabaŋ hütere ügühe muŋa, öj kimniːr hajɨn, hürdeːk ajallanna bu͡olaːččɨ. </ta>
            <ta e="T217" id="Seg_5932" s="T211">Ol bačča hɨldʼɨ͡akpar di͡eri bu͡o… </ta>
            <ta e="T228" id="Seg_5933" s="T217">Tu meːr üčügej aːtɨnan tagɨstɨm eːt otto – hajɨn da, kɨhɨn da. </ta>
            <ta e="T243" id="Seg_5934" s="T228">Hin üleleːtekke elbek dʼɨlɨ üleleːtim, bi͡eh-u͡on bi͡es dʼɨlɨ pʼensʼijaga kiːren baraːn össü͡ö üleleːbitim tabam iːtilleritten. </ta>
            <ta e="T260" id="Seg_5935" s="T256">–Hüppüte bu͡o, ölön hüten. </ta>
            <ta e="T265" id="Seg_5936" s="T260">Hüppüte iti kimnerge Haskɨlaːktarga baran. </ta>
            <ta e="T270" id="Seg_5937" s="T265">Baran daːganɨ, kɨːl da iller. </ta>
            <ta e="T272" id="Seg_5938" s="T270">Kɨːl illeːčči. </ta>
            <ta e="T280" id="Seg_5939" s="T272">Mʼigrasʼijatɨgar, kühün keliːtiger ke börö hiːr ulakan hürdeːk. </ta>
            <ta e="T288" id="Seg_5940" s="T280">ɨ͡aldʼar onuga, atakta daː, honno ör baraːččɨta hu͡ok. </ta>
            <ta e="T306" id="Seg_5941" s="T301">– Ölörtöröllör hanɨ, otto baːr bu͡ollagɨna. </ta>
            <ta e="T308" id="Seg_5942" s="T306">Diːller diː. </ta>
            <ta e="T316" id="Seg_5943" s="T308">Anɨ prʼemʼijalɨːr bu͡olbuttar, urut prʼemʼijalaːččɨta hu͡oktar ((PAUSE)) etiler. </ta>
            <ta e="T332" id="Seg_5944" s="T319">– Bihi͡enebit, kimmit ki͡e rascenkabɨt kuhagan ete, urut kɨra, küččügüj ete tababɨt ki͡ene ke. </ta>
            <ta e="T346" id="Seg_5945" s="T332">Onu min, "ürdetiŋ", diː hataːn anɨ, tɨl isti͡ektere bu͡o, anɨ ülehitterbit kuhagattar onuga dʼe. </ta>
            <ta e="T357" id="Seg_5946" s="T352">–Mm, diːbit bu͡ol kü͡ömej iːti͡ekterin. </ta>
            <ta e="T361" id="Seg_5947" s="T357">Taba rascenkatɨn ürdeti͡ekke, diːbit. </ta>
            <ta e="T372" id="Seg_5948" s="T361">(Bihi͡e-) gini͡ennere kimnere, Ürüŋ Kajalar gi͡ettere bu͡o biːr tabaga kas di͡ebitterej? </ta>
            <ta e="T376" id="Seg_5949" s="T372">Huːtkaga tü͡ört, tü͡ört holku͡obaj. </ta>
            <ta e="T384" id="Seg_5950" s="T380">– Iːtellerin ihin bu͡o, he-he. </ta>
            <ta e="T390" id="Seg_5951" s="T384">(Tabalɨːlla-) tabalaːn iːten taksallarɨn ihin. </ta>
            <ta e="T393" id="Seg_5952" s="T390">Gini͡ennere emi͡e… </ta>
            <ta e="T397" id="Seg_5953" s="T393">Purgaː dʼaːŋɨ gɨnar bu͡o. </ta>
            <ta e="T402" id="Seg_5954" s="T397">Ürüŋ Kajaŋ hire hürdeːk dʼaːŋɨ hir. </ta>
            <ta e="T410" id="Seg_5955" s="T402">Ol ihin ol rascenkalarɨŋ ürdeppitter maŋnajgɨttan, (karaːntan), urukkuttan. </ta>
            <ta e="T415" id="Seg_5956" s="T410">Taba iːtiː hürdeːk bu͡o kɨtaːnaga. </ta>
            <ta e="T427" id="Seg_5957" s="T417">– E, (ölör) ölör kihiŋ ölör ol (kɨ) tabanɨ gɨtta barsan. </ta>
            <ta e="T446" id="Seg_5958" s="T441">– Iti haka hiriger onnuktar ete. </ta>
            <ta e="T449" id="Seg_5959" s="T446">Tabanɨ hürdeːk kɨlɨːlɨːllar. </ta>
            <ta e="T451" id="Seg_5960" s="T449">Iːti͡ekterin bagarallar. </ta>
            <ta e="T458" id="Seg_5961" s="T451">Ara, karčɨ ɨlallar üčügej bu͡olla tabahɨttarɨŋ ɨjga. </ta>
            <ta e="T465" id="Seg_5962" s="T458">Tü͡örtüː, bi͡estiː tɨːhɨččanɨ biːr ɨjga ɨlallar ginner. </ta>
            <ta e="T477" id="Seg_5963" s="T465">Bihigi ol kaččaga (t-) onnuga daː hu͡okput, törüt hu͡ok… </ta>
            <ta e="T484" id="Seg_5964" s="T477">Meːne, bi͡es hüːs bu͡olu͡o duː, avansa kördük. </ta>
            <ta e="T489" id="Seg_5965" s="T484">Rascenkatabɨt agɨjaga bert, kʼepʼejkʼi. </ta>
            <ta e="T491" id="Seg_5966" s="T489">Anɨga di͡eri. </ta>
            <ta e="T496" id="Seg_5967" s="T491">Uhuguttan hüterdiler anɨ tu͡ogɨn daːganɨ. </ta>
            <ta e="T498" id="Seg_5968" s="T496">Haŋara hataːn: </ta>
            <ta e="T510" id="Seg_5969" s="T498">"Togo tɨːtagɨt iti, tu͡ok gɨnaːrɨ, hopku͡oskɨt, hopku͡oskɨt tabata, bejegit hiːgit bu͡o", di͡eččibin. </ta>
            <ta e="T512" id="Seg_5970" s="T510">"Taŋnagɨt, kanʼɨːgɨt." </ta>
            <ta e="T517" id="Seg_5971" s="T512">"Togo hopku͡os gi͡ene, diːgit", diːbin. </ta>
            <ta e="T521" id="Seg_5972" s="T517">"Bejegit baːjgɨt diːn", diːbin. </ta>
            <ta e="T532" id="Seg_5973" s="T521">"Ol ihin karčɨ ɨlagɨt, (tuguj) tu͡ogunan (ɨlaːbɨ-) ɨlɨ͡appɨt diːgit", diːbin. </ta>
            <ta e="T537" id="Seg_5974" s="T532">Anʼɨː bu͡olu͡o, ulakannɨk albɨnnaːbɨtɨm, di͡etekpinen. </ta>
            <ta e="T540" id="Seg_5975" s="T537">Atɨːlammatagɨm daː haːtar. </ta>
            <ta e="T546" id="Seg_5976" s="T540">Barɨta, kihiŋ barɨta atɨːlammɨta ol hopku͡ostan. </ta>
            <ta e="T551" id="Seg_5977" s="T549">– Karčɨ oŋostollor. </ta>
            <ta e="T573" id="Seg_5978" s="T571">– Onnuk, onnuk. </ta>
            <ta e="T585" id="Seg_5979" s="T580">– Tabahɨt kihi kemči dʼe onu. </ta>
            <ta e="T621" id="Seg_5980" s="T612">–Ti üčügejdik üleliːrim ihin taksɨbɨtɨm, kihileri gɨtta kepseterim ihin. </ta>
            <ta e="T628" id="Seg_5981" s="T621">Hi͡ese eder erdekpine, hürdeːk huːstraj etim bu͡ol. </ta>
            <ta e="T634" id="Seg_5982" s="T628">ɨ͡aldʼɨbɨtɨm daː ihin, kepsetiː obu͡ojdaːk etim. </ta>
            <ta e="T643" id="Seg_5983" s="T634">Nʼuːčča daː bu͡ol, haka daː bu͡ol, omuk daː bu͡ol. </ta>
            <ta e="T657" id="Seg_5984" s="T652">– Kün aːjɨ bu͡o köröllörö bagas. </ta>
            <ta e="T661" id="Seg_5985" s="T657">Hɨːhanɨ tu͡oktara ((…)) gɨnagɨn. </ta>
            <ta e="T669" id="Seg_5986" s="T661">Olus hɨːhata hu͡okpun bu͡o, ol ihin, nʼuːččalar… </ta>
            <ta e="T697" id="Seg_5987" s="T693">– Töröːbütün kepsiːgin bu͡o mekti͡etin. </ta>
            <ta e="T701" id="Seg_5988" s="T697">Tugutuŋ kuččukaːn bu͡o ebit. </ta>
            <ta e="T710" id="Seg_5989" s="T701">Tüːnü bɨha utujbakka hɨldʼagɨn bu͡o, ol tugut törüːrün ketiː. </ta>
            <ta e="T717" id="Seg_5990" s="T710">Ügüs tabaga (el-) elbek tugut bu͡o. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-ChSA">
            <ta e="T7" id="Seg_5991" s="T6">teːte-ler-bit</ta>
            <ta e="T8" id="Seg_5992" s="T7">inʼe-ler-bit</ta>
            <ta e="T9" id="Seg_5993" s="T8">dʼe</ta>
            <ta e="T10" id="Seg_5994" s="T9">otto</ta>
            <ta e="T11" id="Seg_5995" s="T10">kɨrdʼagas-tar</ta>
            <ta e="T12" id="Seg_5996" s="T11">e-t-e</ta>
            <ta e="T13" id="Seg_5997" s="T12">bu͡o</ta>
            <ta e="T14" id="Seg_5998" s="T13">U-nu</ta>
            <ta e="T15" id="Seg_5999" s="T14">A-nɨ</ta>
            <ta e="T16" id="Seg_6000" s="T15">bil-bet-ter</ta>
            <ta e="T19" id="Seg_6001" s="T18">meːne</ta>
            <ta e="T22" id="Seg_6002" s="T21">iliː</ta>
            <ta e="T23" id="Seg_6003" s="T22">battɨː-l-lara</ta>
            <ta e="T24" id="Seg_6004" s="T23">munnuk</ta>
            <ta e="T25" id="Seg_6005" s="T24">gɨn-aːččɨ-bɨn</ta>
            <ta e="T26" id="Seg_6006" s="T25">min</ta>
            <ta e="T27" id="Seg_6007" s="T26">emi͡e</ta>
            <ta e="T28" id="Seg_6008" s="T27">h-anɨ</ta>
            <ta e="T29" id="Seg_6009" s="T28">hin</ta>
            <ta e="T30" id="Seg_6010" s="T29">itinnik</ta>
            <ta e="T31" id="Seg_6011" s="T30">keri͡ete</ta>
            <ta e="T32" id="Seg_6012" s="T31">dʼe</ta>
            <ta e="T33" id="Seg_6013" s="T32">öj-ü-m</ta>
            <ta e="T34" id="Seg_6014" s="T33">köp-püt-e</ta>
            <ta e="T35" id="Seg_6015" s="T34">bert</ta>
            <ta e="T36" id="Seg_6016" s="T35">dʼɨl-ɨ</ta>
            <ta e="T37" id="Seg_6017" s="T36">bɨha</ta>
            <ta e="T38" id="Seg_6018" s="T37">ɨ͡aldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T39" id="Seg_6019" s="T38">kata</ta>
            <ta e="T40" id="Seg_6020" s="T39">ol</ta>
            <ta e="T41" id="Seg_6021" s="T40">ereːti</ta>
            <ta e="T42" id="Seg_6022" s="T41">ama-bɨn</ta>
            <ta e="T43" id="Seg_6023" s="T42">hi͡ese</ta>
            <ta e="T44" id="Seg_6024" s="T43">kihi-ge</ta>
            <ta e="T45" id="Seg_6025" s="T44">kihi</ta>
            <ta e="T46" id="Seg_6026" s="T45">d-e-t-er</ta>
            <ta e="T47" id="Seg_6027" s="T46">kördük</ta>
            <ta e="T48" id="Seg_6028" s="T47">hɨldʼ-a-bɨn</ta>
            <ta e="T49" id="Seg_6029" s="T48">eːt</ta>
            <ta e="T50" id="Seg_6030" s="T49">üle-be-r</ta>
            <ta e="T51" id="Seg_6031" s="T50">daː</ta>
            <ta e="T52" id="Seg_6032" s="T51">ke</ta>
            <ta e="T53" id="Seg_6033" s="T52">üle-ge</ta>
            <ta e="T54" id="Seg_6034" s="T53">kuhagan</ta>
            <ta e="T55" id="Seg_6035" s="T54">bu</ta>
            <ta e="T56" id="Seg_6036" s="T55">ogo-lor-bu-n</ta>
            <ta e="T57" id="Seg_6037" s="T56">iːt-i-n-eːri-bin</ta>
            <ta e="T58" id="Seg_6038" s="T57">bu</ta>
            <ta e="T59" id="Seg_6039" s="T58">tug-u</ta>
            <ta e="T61" id="Seg_6040" s="T59">dolu͡oj</ta>
            <ta e="T62" id="Seg_6041" s="T61">tɨːm-mɨ-n</ta>
            <ta e="T63" id="Seg_6042" s="T62">bɨh-a</ta>
            <ta e="T64" id="Seg_6043" s="T63">möŋ-ü-n-eːčči-bin</ta>
            <ta e="T65" id="Seg_6044" s="T64">taba</ta>
            <ta e="T66" id="Seg_6045" s="T65">hɨrɨː-ta</ta>
            <ta e="T67" id="Seg_6046" s="T66">ajɨː</ta>
            <ta e="T68" id="Seg_6047" s="T67">du͡o</ta>
            <ta e="T69" id="Seg_6048" s="T68">urut</ta>
            <ta e="T70" id="Seg_6049" s="T69">elbeg-e</ta>
            <ta e="T71" id="Seg_6050" s="T70">daː</ta>
            <ta e="T72" id="Seg_6051" s="T71">ɨ͡arakan-a</ta>
            <ta e="T73" id="Seg_6052" s="T72">kihi</ta>
            <ta e="T74" id="Seg_6053" s="T73">heni͡e-te</ta>
            <ta e="T75" id="Seg_6054" s="T74">es-t-en</ta>
            <ta e="T76" id="Seg_6055" s="T75">bar-an</ta>
            <ta e="T77" id="Seg_6056" s="T76">kel-eːčči</ta>
            <ta e="T78" id="Seg_6057" s="T77">bukatɨn</ta>
            <ta e="T79" id="Seg_6058" s="T78">u͡on</ta>
            <ta e="T80" id="Seg_6059" s="T79">biːr-der-ge</ta>
            <ta e="T81" id="Seg_6060" s="T80">kel-eːčči-bit</ta>
            <ta e="T82" id="Seg_6061" s="T81">karaŋa</ta>
            <ta e="T83" id="Seg_6062" s="T82">kɨhɨn</ta>
            <ta e="T84" id="Seg_6063" s="T83">oː</ta>
            <ta e="T85" id="Seg_6064" s="T84">tɨmnɨː</ta>
            <ta e="T86" id="Seg_6065" s="T85">bagaj</ta>
            <ta e="T87" id="Seg_6066" s="T86">bu͡o</ta>
            <ta e="T90" id="Seg_6067" s="T89">Pöpügej</ta>
            <ta e="T91" id="Seg_6068" s="T90">tɨ͡a-tɨ-gar</ta>
            <ta e="T92" id="Seg_6069" s="T91">mas</ta>
            <ta e="T93" id="Seg_6070" s="T92">mas</ta>
            <ta e="T94" id="Seg_6071" s="T93">bu͡o</ta>
            <ta e="T95" id="Seg_6072" s="T94">haːtar</ta>
            <ta e="T96" id="Seg_6073" s="T95">bukatɨn</ta>
            <ta e="T97" id="Seg_6074" s="T96">taba</ta>
            <ta e="T98" id="Seg_6075" s="T97">kanna</ta>
            <ta e="T99" id="Seg_6076" s="T98">da</ta>
            <ta e="T100" id="Seg_6077" s="T99">köst-ü-bet</ta>
            <ta e="T101" id="Seg_6078" s="T100">onnuk-tar-ga</ta>
            <ta e="T102" id="Seg_6079" s="T101">üleleː-n</ta>
            <ta e="T103" id="Seg_6080" s="T102">muŋ-nam-mɨt-ɨ-m</ta>
            <ta e="T104" id="Seg_6081" s="T103">min</ta>
            <ta e="T105" id="Seg_6082" s="T104">haːmaj</ta>
            <ta e="T106" id="Seg_6083" s="T105">ol</ta>
            <ta e="T107" id="Seg_6084" s="T106">gɨn-aːrɨ</ta>
            <ta e="T108" id="Seg_6085" s="T107">dʼolloːk</ta>
            <ta e="T109" id="Seg_6086" s="T108">bu͡ol-am-mɨn</ta>
            <ta e="T110" id="Seg_6087" s="T109">taba-m</ta>
            <ta e="T111" id="Seg_6088" s="T110">iːt-i-ll-i-bit-e</ta>
            <ta e="T112" id="Seg_6089" s="T111">hürdeːk</ta>
            <ta e="T113" id="Seg_6090" s="T112">u͡on-tan</ta>
            <ta e="T114" id="Seg_6091" s="T113">taksa</ta>
            <ta e="T115" id="Seg_6092" s="T114">tɨːhɨčča-nɨ</ta>
            <ta e="T116" id="Seg_6093" s="T115">iːp-pit-i-m</ta>
            <ta e="T117" id="Seg_6094" s="T116">kas</ta>
            <ta e="T118" id="Seg_6095" s="T117">da</ta>
            <ta e="T119" id="Seg_6096" s="T118">stada-ga</ta>
            <ta e="T120" id="Seg_6097" s="T119">bi͡er-bit-i-m</ta>
            <ta e="T121" id="Seg_6098" s="T120">stada-ga</ta>
            <ta e="T122" id="Seg_6099" s="T121">bi͡er-bit-i-m</ta>
            <ta e="T130" id="Seg_6100" s="T129">brigada</ta>
            <ta e="T131" id="Seg_6101" s="T130">meːr</ta>
            <ta e="T132" id="Seg_6102" s="T131">anɨ-ga</ta>
            <ta e="T133" id="Seg_6103" s="T132">di͡eri</ta>
            <ta e="T134" id="Seg_6104" s="T133">bu</ta>
            <ta e="T135" id="Seg_6105" s="T134">anɨ</ta>
            <ta e="T136" id="Seg_6106" s="T135">u͡ol-ɨ-m</ta>
            <ta e="T137" id="Seg_6107" s="T136">iti</ta>
            <ta e="T138" id="Seg_6108" s="T137">obu͡ojdan-aːččɨ</ta>
            <ta e="T139" id="Seg_6109" s="T138">Valodʼa-m</ta>
            <ta e="T140" id="Seg_6110" s="T139">hogotok</ta>
            <ta e="T141" id="Seg_6111" s="T140">u͡ol-laːk-pɨn</ta>
            <ta e="T148" id="Seg_6112" s="T147">mm</ta>
            <ta e="T149" id="Seg_6113" s="T148">kɨrdʼ-a-bɨt</ta>
            <ta e="T150" id="Seg_6114" s="T149">ikk-i͡em-mit</ta>
            <ta e="T151" id="Seg_6115" s="T150">kɨrɨj-dɨ-bɨt</ta>
            <ta e="T167" id="Seg_6116" s="T166">brʼigadʼiːr-dar-bɨt</ta>
            <ta e="T168" id="Seg_6117" s="T167">hi͡ese</ta>
            <ta e="T169" id="Seg_6118" s="T168">kihi-ler</ta>
            <ta e="T170" id="Seg_6119" s="T169">e-ti-lere</ta>
            <ta e="T171" id="Seg_6120" s="T170">onton</ta>
            <ta e="T174" id="Seg_6121" s="T173">ü͡ören-e</ta>
            <ta e="T175" id="Seg_6122" s="T174">ü͡örem-mit-i-nen</ta>
            <ta e="T176" id="Seg_6123" s="T175">h-onon</ta>
            <ta e="T177" id="Seg_6124" s="T176">ü͡ören-en</ta>
            <ta e="T178" id="Seg_6125" s="T177">kaːl-lak-pɨt</ta>
            <ta e="T179" id="Seg_6126" s="T178">diː</ta>
            <ta e="T180" id="Seg_6127" s="T179">bihigi</ta>
            <ta e="T181" id="Seg_6128" s="T180">iti</ta>
            <ta e="T182" id="Seg_6129" s="T181">kečeh-iː-ge</ta>
            <ta e="T183" id="Seg_6130" s="T182">taba-ga</ta>
            <ta e="T184" id="Seg_6131" s="T183">taba-nɨ</ta>
            <ta e="T185" id="Seg_6132" s="T184">hüter-i-me-ŋ</ta>
            <ta e="T186" id="Seg_6133" s="T185">di͡e-n</ta>
            <ta e="T187" id="Seg_6134" s="T186">di͡e-čči-bin</ta>
            <ta e="T188" id="Seg_6135" s="T187">bu͡o</ta>
            <ta e="T189" id="Seg_6136" s="T188">taba-nɨ</ta>
            <ta e="T190" id="Seg_6137" s="T189">hüter-i-me-ŋ</ta>
            <ta e="T191" id="Seg_6138" s="T190">harsɨŋŋɨ-ŋɨtɨ-n</ta>
            <ta e="T192" id="Seg_6139" s="T191">öjdöː-ŋ</ta>
            <ta e="T193" id="Seg_6140" s="T192">di͡e-čči-bin</ta>
            <ta e="T194" id="Seg_6141" s="T193">harsi͡erda</ta>
            <ta e="T195" id="Seg_6142" s="T194">plaːn-nan-a</ta>
            <ta e="T196" id="Seg_6143" s="T195">tur-u-ŋ</ta>
            <ta e="T197" id="Seg_6144" s="T196">di͡e-čči-bin</ta>
            <ta e="T198" id="Seg_6145" s="T197">kajdi͡ek</ta>
            <ta e="T199" id="Seg_6146" s="T198">üleliː-r-giti-n</ta>
            <ta e="T200" id="Seg_6147" s="T199">kajdi͡ek</ta>
            <ta e="T201" id="Seg_6148" s="T200">bar-ar-gɨtɨ-n</ta>
            <ta e="T202" id="Seg_6149" s="T201">taba-ŋ</ta>
            <ta e="T203" id="Seg_6150" s="T202">hüt-er-e</ta>
            <ta e="T204" id="Seg_6151" s="T203">ügüh-e</ta>
            <ta e="T205" id="Seg_6152" s="T204">muŋ-a</ta>
            <ta e="T206" id="Seg_6153" s="T205">öj</ta>
            <ta e="T207" id="Seg_6154" s="T206">kim-niː-r</ta>
            <ta e="T208" id="Seg_6155" s="T207">hajɨn</ta>
            <ta e="T209" id="Seg_6156" s="T208">hürdeːk</ta>
            <ta e="T210" id="Seg_6157" s="T209">ajal-lan-n-a</ta>
            <ta e="T211" id="Seg_6158" s="T210">bu͡ol-aːččɨ</ta>
            <ta e="T212" id="Seg_6159" s="T211">ol</ta>
            <ta e="T213" id="Seg_6160" s="T212">bačča</ta>
            <ta e="T214" id="Seg_6161" s="T213">hɨldʼ-ɨ͡ak-pa-r</ta>
            <ta e="T215" id="Seg_6162" s="T214">di͡eri</ta>
            <ta e="T217" id="Seg_6163" s="T215">bu͡o</ta>
            <ta e="T218" id="Seg_6164" s="T217">tu</ta>
            <ta e="T219" id="Seg_6165" s="T218">meːr</ta>
            <ta e="T220" id="Seg_6166" s="T219">üčügej</ta>
            <ta e="T221" id="Seg_6167" s="T220">aːt-ɨ-nan</ta>
            <ta e="T222" id="Seg_6168" s="T221">tagɨs-tɨ-m</ta>
            <ta e="T223" id="Seg_6169" s="T222">eːt</ta>
            <ta e="T224" id="Seg_6170" s="T223">otto</ta>
            <ta e="T225" id="Seg_6171" s="T224">hajɨn</ta>
            <ta e="T226" id="Seg_6172" s="T225">da</ta>
            <ta e="T227" id="Seg_6173" s="T226">kɨhɨn</ta>
            <ta e="T228" id="Seg_6174" s="T227">da</ta>
            <ta e="T229" id="Seg_6175" s="T228">hin</ta>
            <ta e="T230" id="Seg_6176" s="T229">üleleː-tek-ke</ta>
            <ta e="T231" id="Seg_6177" s="T230">elbek</ta>
            <ta e="T232" id="Seg_6178" s="T231">dʼɨl-ɨ</ta>
            <ta e="T233" id="Seg_6179" s="T232">üleleː-ti-m</ta>
            <ta e="T234" id="Seg_6180" s="T233">bi͡eh-u͡on</ta>
            <ta e="T235" id="Seg_6181" s="T234">bi͡es</ta>
            <ta e="T236" id="Seg_6182" s="T235">dʼɨl-ɨ</ta>
            <ta e="T237" id="Seg_6183" s="T236">pʼensʼija-ga</ta>
            <ta e="T238" id="Seg_6184" s="T237">kiːr-en</ta>
            <ta e="T239" id="Seg_6185" s="T238">baraːn</ta>
            <ta e="T240" id="Seg_6186" s="T239">össü͡ö</ta>
            <ta e="T241" id="Seg_6187" s="T240">üleleː-bit-i-m</ta>
            <ta e="T242" id="Seg_6188" s="T241">taba-m</ta>
            <ta e="T243" id="Seg_6189" s="T242">iːt-i-ll-er-i-tten</ta>
            <ta e="T257" id="Seg_6190" s="T256">hüp-püt-e</ta>
            <ta e="T258" id="Seg_6191" s="T257">bu͡o</ta>
            <ta e="T259" id="Seg_6192" s="T258">öl-ön</ta>
            <ta e="T260" id="Seg_6193" s="T259">hüt-en</ta>
            <ta e="T261" id="Seg_6194" s="T260">hüp-püt-e</ta>
            <ta e="T262" id="Seg_6195" s="T261">iti</ta>
            <ta e="T263" id="Seg_6196" s="T262">kim-ner-ge</ta>
            <ta e="T264" id="Seg_6197" s="T263">Haskɨlaːk-tar-ga</ta>
            <ta e="T265" id="Seg_6198" s="T264">bar-an</ta>
            <ta e="T266" id="Seg_6199" s="T265">bar-an</ta>
            <ta e="T267" id="Seg_6200" s="T266">daːganɨ</ta>
            <ta e="T268" id="Seg_6201" s="T267">kɨːl</ta>
            <ta e="T269" id="Seg_6202" s="T268">da</ta>
            <ta e="T270" id="Seg_6203" s="T269">ill-er</ta>
            <ta e="T271" id="Seg_6204" s="T270">kɨːl</ta>
            <ta e="T272" id="Seg_6205" s="T271">ill-eːčči</ta>
            <ta e="T273" id="Seg_6206" s="T272">mʼigrasʼija-tɨ-gar</ta>
            <ta e="T274" id="Seg_6207" s="T273">kühün</ta>
            <ta e="T275" id="Seg_6208" s="T274">kel-iː-ti-ger</ta>
            <ta e="T276" id="Seg_6209" s="T275">ke</ta>
            <ta e="T277" id="Seg_6210" s="T276">börö</ta>
            <ta e="T278" id="Seg_6211" s="T277">hiː-r</ta>
            <ta e="T279" id="Seg_6212" s="T278">ulakan</ta>
            <ta e="T280" id="Seg_6213" s="T279">hürdeːk</ta>
            <ta e="T281" id="Seg_6214" s="T280">ɨ͡aldʼ-ar</ta>
            <ta e="T282" id="Seg_6215" s="T281">onu-ga</ta>
            <ta e="T283" id="Seg_6216" s="T282">atak-ta</ta>
            <ta e="T284" id="Seg_6217" s="T283">daː</ta>
            <ta e="T285" id="Seg_6218" s="T284">honno</ta>
            <ta e="T286" id="Seg_6219" s="T285">ör</ta>
            <ta e="T287" id="Seg_6220" s="T286">bar-aːččɨ-ta</ta>
            <ta e="T288" id="Seg_6221" s="T287">hu͡ok</ta>
            <ta e="T302" id="Seg_6222" s="T301">ölör-tör-öl-lör</ta>
            <ta e="T303" id="Seg_6223" s="T302">hanɨ</ta>
            <ta e="T304" id="Seg_6224" s="T303">otto</ta>
            <ta e="T305" id="Seg_6225" s="T304">baːr</ta>
            <ta e="T306" id="Seg_6226" s="T305">bu͡ol-lag-ɨna</ta>
            <ta e="T307" id="Seg_6227" s="T306">diː-l-ler</ta>
            <ta e="T308" id="Seg_6228" s="T307">diː</ta>
            <ta e="T309" id="Seg_6229" s="T308">anɨ</ta>
            <ta e="T310" id="Seg_6230" s="T309">prʼemʼija-lɨː-r</ta>
            <ta e="T311" id="Seg_6231" s="T310">bu͡ol-but-tar</ta>
            <ta e="T312" id="Seg_6232" s="T311">urut</ta>
            <ta e="T313" id="Seg_6233" s="T312">prʼemʼija-l-aːččɨ-ta</ta>
            <ta e="T314" id="Seg_6234" s="T313">hu͡ok-tar</ta>
            <ta e="T316" id="Seg_6235" s="T315">e-ti-ler</ta>
            <ta e="T320" id="Seg_6236" s="T319">bihi͡ene-bit</ta>
            <ta e="T321" id="Seg_6237" s="T320">kim-mit</ta>
            <ta e="T322" id="Seg_6238" s="T321">ki͡e</ta>
            <ta e="T323" id="Seg_6239" s="T322">rascenka-bɨt</ta>
            <ta e="T324" id="Seg_6240" s="T323">kuhagan</ta>
            <ta e="T325" id="Seg_6241" s="T324">e-t-e</ta>
            <ta e="T326" id="Seg_6242" s="T325">urut</ta>
            <ta e="T327" id="Seg_6243" s="T326">kɨra</ta>
            <ta e="T328" id="Seg_6244" s="T327">küččügüj</ta>
            <ta e="T329" id="Seg_6245" s="T328">e-t-e</ta>
            <ta e="T330" id="Seg_6246" s="T329">taba-bɨt</ta>
            <ta e="T331" id="Seg_6247" s="T330">ki͡en-e</ta>
            <ta e="T332" id="Seg_6248" s="T331">ke</ta>
            <ta e="T333" id="Seg_6249" s="T332">o-nu</ta>
            <ta e="T334" id="Seg_6250" s="T333">min</ta>
            <ta e="T335" id="Seg_6251" s="T334">ürd-e-t-i-ŋ</ta>
            <ta e="T336" id="Seg_6252" s="T335">d-iː</ta>
            <ta e="T337" id="Seg_6253" s="T336">hataː-n</ta>
            <ta e="T338" id="Seg_6254" s="T337">anɨ</ta>
            <ta e="T339" id="Seg_6255" s="T338">tɨl</ta>
            <ta e="T340" id="Seg_6256" s="T339">ist-i͡ek-tere</ta>
            <ta e="T341" id="Seg_6257" s="T340">bu͡o</ta>
            <ta e="T342" id="Seg_6258" s="T341">anɨ</ta>
            <ta e="T343" id="Seg_6259" s="T342">ülehit-ter-bit</ta>
            <ta e="T344" id="Seg_6260" s="T343">kuhagat-tar</ta>
            <ta e="T345" id="Seg_6261" s="T344">onu-ga</ta>
            <ta e="T346" id="Seg_6262" s="T345">dʼe</ta>
            <ta e="T353" id="Seg_6263" s="T352">mm</ta>
            <ta e="T354" id="Seg_6264" s="T353">d-iː-bit</ta>
            <ta e="T355" id="Seg_6265" s="T354">bu͡ol</ta>
            <ta e="T356" id="Seg_6266" s="T355">kü͡ömej</ta>
            <ta e="T357" id="Seg_6267" s="T356">iːt-i͡ek-teri-n</ta>
            <ta e="T358" id="Seg_6268" s="T357">taba</ta>
            <ta e="T359" id="Seg_6269" s="T358">rascenka-tɨ-n</ta>
            <ta e="T360" id="Seg_6270" s="T359">ürd-e-t-i͡ek-ke</ta>
            <ta e="T361" id="Seg_6271" s="T360">d-iː-bit</ta>
            <ta e="T364" id="Seg_6272" s="T363">gini͡ennere</ta>
            <ta e="T365" id="Seg_6273" s="T364">kim-nere</ta>
            <ta e="T366" id="Seg_6274" s="T365">Ürüŋ Kaja-lar</ta>
            <ta e="T367" id="Seg_6275" s="T366">gi͡et-tere</ta>
            <ta e="T368" id="Seg_6276" s="T367">bu͡o</ta>
            <ta e="T369" id="Seg_6277" s="T368">biːr</ta>
            <ta e="T370" id="Seg_6278" s="T369">taba-ga</ta>
            <ta e="T371" id="Seg_6279" s="T370">kas</ta>
            <ta e="T372" id="Seg_6280" s="T371">di͡e-bit-tere=j</ta>
            <ta e="T373" id="Seg_6281" s="T372">huːtka-ga</ta>
            <ta e="T374" id="Seg_6282" s="T373">tü͡ört</ta>
            <ta e="T375" id="Seg_6283" s="T374">tü͡ört</ta>
            <ta e="T376" id="Seg_6284" s="T375">holku͡obaj</ta>
            <ta e="T381" id="Seg_6285" s="T380">iːt-el-leri-n</ta>
            <ta e="T382" id="Seg_6286" s="T381">ihin</ta>
            <ta e="T383" id="Seg_6287" s="T382">bu͡o</ta>
            <ta e="T384" id="Seg_6288" s="T383">he-he</ta>
            <ta e="T387" id="Seg_6289" s="T386">taba-laː-n</ta>
            <ta e="T388" id="Seg_6290" s="T387">iːt-en</ta>
            <ta e="T389" id="Seg_6291" s="T388">taks-al-larɨ-n</ta>
            <ta e="T390" id="Seg_6292" s="T389">ihin</ta>
            <ta e="T391" id="Seg_6293" s="T390">gini͡ennere</ta>
            <ta e="T393" id="Seg_6294" s="T391">emi͡e</ta>
            <ta e="T394" id="Seg_6295" s="T393">purgaː</ta>
            <ta e="T395" id="Seg_6296" s="T394">dʼaːŋɨ</ta>
            <ta e="T396" id="Seg_6297" s="T395">gɨn-ar</ta>
            <ta e="T397" id="Seg_6298" s="T396">bu͡o</ta>
            <ta e="T398" id="Seg_6299" s="T397">Ürüŋ Kaja-ŋ</ta>
            <ta e="T399" id="Seg_6300" s="T398">hir-e</ta>
            <ta e="T400" id="Seg_6301" s="T399">hürdeːk</ta>
            <ta e="T401" id="Seg_6302" s="T400">dʼaːŋɨ</ta>
            <ta e="T402" id="Seg_6303" s="T401">hir</ta>
            <ta e="T403" id="Seg_6304" s="T402">ol</ta>
            <ta e="T404" id="Seg_6305" s="T403">ihin</ta>
            <ta e="T405" id="Seg_6306" s="T404">ol</ta>
            <ta e="T406" id="Seg_6307" s="T405">rascenka-lar-ɨ-ŋ</ta>
            <ta e="T407" id="Seg_6308" s="T406">ürd-e-p-pit-ter</ta>
            <ta e="T408" id="Seg_6309" s="T407">maŋnaj-gɨ-ttan</ta>
            <ta e="T409" id="Seg_6310" s="T408">karaːn-tan</ta>
            <ta e="T410" id="Seg_6311" s="T409">urukku-ttan</ta>
            <ta e="T411" id="Seg_6312" s="T410">taba</ta>
            <ta e="T412" id="Seg_6313" s="T411">iːt-iː</ta>
            <ta e="T413" id="Seg_6314" s="T412">hürdeːk</ta>
            <ta e="T414" id="Seg_6315" s="T413">bu͡o</ta>
            <ta e="T415" id="Seg_6316" s="T414">kɨtaːnag-a</ta>
            <ta e="T418" id="Seg_6317" s="T417">e</ta>
            <ta e="T419" id="Seg_6318" s="T418">öl-ör</ta>
            <ta e="T420" id="Seg_6319" s="T419">öl-ör</ta>
            <ta e="T421" id="Seg_6320" s="T420">kihi-ŋ</ta>
            <ta e="T422" id="Seg_6321" s="T421">öl-ör</ta>
            <ta e="T423" id="Seg_6322" s="T422">ol</ta>
            <ta e="T425" id="Seg_6323" s="T424">taba-nɨ</ta>
            <ta e="T426" id="Seg_6324" s="T425">gɨtta</ta>
            <ta e="T427" id="Seg_6325" s="T426">bars-an</ta>
            <ta e="T442" id="Seg_6326" s="T441">iti</ta>
            <ta e="T443" id="Seg_6327" s="T442">haka</ta>
            <ta e="T444" id="Seg_6328" s="T443">hir-i-ger</ta>
            <ta e="T445" id="Seg_6329" s="T444">onnuk-tar</ta>
            <ta e="T446" id="Seg_6330" s="T445">e-t-e</ta>
            <ta e="T447" id="Seg_6331" s="T446">taba-nɨ</ta>
            <ta e="T448" id="Seg_6332" s="T447">hürdeːk</ta>
            <ta e="T449" id="Seg_6333" s="T448">kɨlɨːlɨː-l-lar</ta>
            <ta e="T450" id="Seg_6334" s="T449">iːt-i͡ek-teri-n</ta>
            <ta e="T451" id="Seg_6335" s="T450">bagar-al-lar</ta>
            <ta e="T452" id="Seg_6336" s="T451">ara</ta>
            <ta e="T453" id="Seg_6337" s="T452">karčɨ</ta>
            <ta e="T454" id="Seg_6338" s="T453">ɨl-al-lar</ta>
            <ta e="T455" id="Seg_6339" s="T454">üčügej</ta>
            <ta e="T456" id="Seg_6340" s="T455">bu͡ol-l-a</ta>
            <ta e="T457" id="Seg_6341" s="T456">taba-hɨt-tar-ɨ-ŋ</ta>
            <ta e="T458" id="Seg_6342" s="T457">ɨj-ga</ta>
            <ta e="T459" id="Seg_6343" s="T458">tü͡ör-tüː</ta>
            <ta e="T460" id="Seg_6344" s="T459">bi͡es-tiː</ta>
            <ta e="T461" id="Seg_6345" s="T460">tɨːhɨčča-nɨ</ta>
            <ta e="T462" id="Seg_6346" s="T461">biːr</ta>
            <ta e="T463" id="Seg_6347" s="T462">ɨj-ga</ta>
            <ta e="T464" id="Seg_6348" s="T463">ɨl-al-lar</ta>
            <ta e="T465" id="Seg_6349" s="T464">ginner</ta>
            <ta e="T466" id="Seg_6350" s="T465">bihigi</ta>
            <ta e="T467" id="Seg_6351" s="T466">ol</ta>
            <ta e="T468" id="Seg_6352" s="T467">kaččaga</ta>
            <ta e="T471" id="Seg_6353" s="T470">onnug-a</ta>
            <ta e="T472" id="Seg_6354" s="T471">daː</ta>
            <ta e="T474" id="Seg_6355" s="T472">hu͡ok-put</ta>
            <ta e="T475" id="Seg_6356" s="T474">törüt</ta>
            <ta e="T477" id="Seg_6357" s="T475">hu͡ok</ta>
            <ta e="T478" id="Seg_6358" s="T477">meːne</ta>
            <ta e="T479" id="Seg_6359" s="T478">bi͡es</ta>
            <ta e="T480" id="Seg_6360" s="T479">hüːs</ta>
            <ta e="T481" id="Seg_6361" s="T480">bu͡olu͡o</ta>
            <ta e="T482" id="Seg_6362" s="T481">duː</ta>
            <ta e="T483" id="Seg_6363" s="T482">avansa</ta>
            <ta e="T484" id="Seg_6364" s="T483">kördük</ta>
            <ta e="T486" id="Seg_6365" s="T484">rascenka-ta-bɨt</ta>
            <ta e="T487" id="Seg_6366" s="T486">agɨjag-a</ta>
            <ta e="T488" id="Seg_6367" s="T487">bert</ta>
            <ta e="T489" id="Seg_6368" s="T488">kʼepʼejkʼi</ta>
            <ta e="T490" id="Seg_6369" s="T489">anɨ-ga</ta>
            <ta e="T491" id="Seg_6370" s="T490">di͡eri</ta>
            <ta e="T492" id="Seg_6371" s="T491">uhug-u-ttan</ta>
            <ta e="T493" id="Seg_6372" s="T492">hüter-di-ler</ta>
            <ta e="T494" id="Seg_6373" s="T493">anɨ</ta>
            <ta e="T495" id="Seg_6374" s="T494">tu͡og-ɨ-n</ta>
            <ta e="T496" id="Seg_6375" s="T495">daːganɨ</ta>
            <ta e="T497" id="Seg_6376" s="T496">haŋar-a</ta>
            <ta e="T498" id="Seg_6377" s="T497">hataː-n</ta>
            <ta e="T499" id="Seg_6378" s="T498">togo</ta>
            <ta e="T500" id="Seg_6379" s="T499">tɨːt-a-gɨt</ta>
            <ta e="T501" id="Seg_6380" s="T500">iti</ta>
            <ta e="T502" id="Seg_6381" s="T501">tu͡ok</ta>
            <ta e="T503" id="Seg_6382" s="T502">gɨn-aːrɨ</ta>
            <ta e="T504" id="Seg_6383" s="T503">hopku͡os-kɨt</ta>
            <ta e="T505" id="Seg_6384" s="T504">hopku͡os-kɨt</ta>
            <ta e="T506" id="Seg_6385" s="T505">taba-ta</ta>
            <ta e="T507" id="Seg_6386" s="T506">beje-git</ta>
            <ta e="T508" id="Seg_6387" s="T507">h-iː-git</ta>
            <ta e="T509" id="Seg_6388" s="T508">bu͡o</ta>
            <ta e="T510" id="Seg_6389" s="T509">di͡e-čči-bin</ta>
            <ta e="T511" id="Seg_6390" s="T510">taŋn-a-gɨt</ta>
            <ta e="T512" id="Seg_6391" s="T511">kanʼ-ɨː-gɨt</ta>
            <ta e="T513" id="Seg_6392" s="T512">togo</ta>
            <ta e="T514" id="Seg_6393" s="T513">hopku͡os</ta>
            <ta e="T515" id="Seg_6394" s="T514">gi͡en-e</ta>
            <ta e="T516" id="Seg_6395" s="T515">d-iː-git</ta>
            <ta e="T517" id="Seg_6396" s="T516">d-iː-bin</ta>
            <ta e="T518" id="Seg_6397" s="T517">beje-git</ta>
            <ta e="T519" id="Seg_6398" s="T518">baːj-gɨt</ta>
            <ta e="T520" id="Seg_6399" s="T519">diːn</ta>
            <ta e="T521" id="Seg_6400" s="T520">d-iː-bin</ta>
            <ta e="T522" id="Seg_6401" s="T521">ol</ta>
            <ta e="T523" id="Seg_6402" s="T522">ihin</ta>
            <ta e="T524" id="Seg_6403" s="T523">karčɨ</ta>
            <ta e="T525" id="Seg_6404" s="T524">ɨl-a-gɨt</ta>
            <ta e="T526" id="Seg_6405" s="T525">tug-u=j</ta>
            <ta e="T527" id="Seg_6406" s="T526">tu͡og-u-nan</ta>
            <ta e="T530" id="Seg_6407" s="T529">ɨl-ɨ͡ap-pɨt</ta>
            <ta e="T531" id="Seg_6408" s="T530">d-iː-git</ta>
            <ta e="T532" id="Seg_6409" s="T531">d-iː-bin</ta>
            <ta e="T533" id="Seg_6410" s="T532">anʼɨː</ta>
            <ta e="T534" id="Seg_6411" s="T533">bu͡ol-u͡o</ta>
            <ta e="T535" id="Seg_6412" s="T534">ulakan-nɨk</ta>
            <ta e="T536" id="Seg_6413" s="T535">albɨn-naː-bɨt-ɨ-m</ta>
            <ta e="T537" id="Seg_6414" s="T536">di͡e-tek-pinen</ta>
            <ta e="T538" id="Seg_6415" s="T537">atɨːl-a-m-matag-ɨ-m</ta>
            <ta e="T539" id="Seg_6416" s="T538">daː</ta>
            <ta e="T540" id="Seg_6417" s="T539">haːtar</ta>
            <ta e="T541" id="Seg_6418" s="T540">barɨta</ta>
            <ta e="T542" id="Seg_6419" s="T541">kihi-ŋ</ta>
            <ta e="T543" id="Seg_6420" s="T542">barɨ-ta</ta>
            <ta e="T544" id="Seg_6421" s="T543">atɨːl-a-m-mɨt-a</ta>
            <ta e="T545" id="Seg_6422" s="T544">ol</ta>
            <ta e="T546" id="Seg_6423" s="T545">hopku͡os-tan</ta>
            <ta e="T550" id="Seg_6424" s="T549">karčɨ</ta>
            <ta e="T551" id="Seg_6425" s="T550">oŋost-ol-lor</ta>
            <ta e="T572" id="Seg_6426" s="T571">onnuk</ta>
            <ta e="T573" id="Seg_6427" s="T572">onnuk</ta>
            <ta e="T581" id="Seg_6428" s="T580">taba-hɨt</ta>
            <ta e="T582" id="Seg_6429" s="T581">kihi</ta>
            <ta e="T583" id="Seg_6430" s="T582">kemči</ta>
            <ta e="T584" id="Seg_6431" s="T583">dʼe</ta>
            <ta e="T585" id="Seg_6432" s="T584">o-nu</ta>
            <ta e="T613" id="Seg_6433" s="T612">ti</ta>
            <ta e="T614" id="Seg_6434" s="T613">üčügej-dik</ta>
            <ta e="T615" id="Seg_6435" s="T614">üleliː-r-i-m</ta>
            <ta e="T616" id="Seg_6436" s="T615">ihin</ta>
            <ta e="T617" id="Seg_6437" s="T616">taks-ɨ-bɨt-ɨ-m</ta>
            <ta e="T618" id="Seg_6438" s="T617">kihi-ler-i</ta>
            <ta e="T619" id="Seg_6439" s="T618">gɨtta</ta>
            <ta e="T620" id="Seg_6440" s="T619">kepset-er-i-m</ta>
            <ta e="T621" id="Seg_6441" s="T620">ihin</ta>
            <ta e="T622" id="Seg_6442" s="T621">hi͡ese</ta>
            <ta e="T623" id="Seg_6443" s="T622">eder</ta>
            <ta e="T624" id="Seg_6444" s="T623">er-dek-pine</ta>
            <ta e="T625" id="Seg_6445" s="T624">hürdeːk</ta>
            <ta e="T626" id="Seg_6446" s="T625">huːstraj</ta>
            <ta e="T627" id="Seg_6447" s="T626">e-ti-m</ta>
            <ta e="T628" id="Seg_6448" s="T627">bu͡ol</ta>
            <ta e="T629" id="Seg_6449" s="T628">ɨ͡aldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T630" id="Seg_6450" s="T629">daː</ta>
            <ta e="T631" id="Seg_6451" s="T630">ihin</ta>
            <ta e="T632" id="Seg_6452" s="T631">kepset-iː</ta>
            <ta e="T633" id="Seg_6453" s="T632">obu͡oj-daːk</ta>
            <ta e="T634" id="Seg_6454" s="T633">e-ti-m</ta>
            <ta e="T635" id="Seg_6455" s="T634">nʼuːčča</ta>
            <ta e="T636" id="Seg_6456" s="T635">daː</ta>
            <ta e="T637" id="Seg_6457" s="T636">bu͡ol</ta>
            <ta e="T638" id="Seg_6458" s="T637">haka</ta>
            <ta e="T639" id="Seg_6459" s="T638">daː</ta>
            <ta e="T640" id="Seg_6460" s="T639">bu͡ol</ta>
            <ta e="T641" id="Seg_6461" s="T640">omuk</ta>
            <ta e="T642" id="Seg_6462" s="T641">daː</ta>
            <ta e="T643" id="Seg_6463" s="T642">bu͡ol</ta>
            <ta e="T653" id="Seg_6464" s="T652">kün</ta>
            <ta e="T654" id="Seg_6465" s="T653">aːjɨ</ta>
            <ta e="T655" id="Seg_6466" s="T654">bu͡o</ta>
            <ta e="T656" id="Seg_6467" s="T655">kör-öl-lörö</ta>
            <ta e="T657" id="Seg_6468" s="T656">bagas</ta>
            <ta e="T658" id="Seg_6469" s="T657">hɨːha-nɨ</ta>
            <ta e="T659" id="Seg_6470" s="T658">tu͡ok-tara</ta>
            <ta e="T661" id="Seg_6471" s="T660">gɨn-a-gɨn</ta>
            <ta e="T662" id="Seg_6472" s="T661">olus</ta>
            <ta e="T663" id="Seg_6473" s="T662">hɨːha-ta</ta>
            <ta e="T664" id="Seg_6474" s="T663">hu͡ok-pun</ta>
            <ta e="T665" id="Seg_6475" s="T664">bu͡o</ta>
            <ta e="T666" id="Seg_6476" s="T665">ol</ta>
            <ta e="T667" id="Seg_6477" s="T666">ihin</ta>
            <ta e="T669" id="Seg_6478" s="T667">nʼuːčča-lar</ta>
            <ta e="T694" id="Seg_6479" s="T693">töröː-büt-ü-n</ta>
            <ta e="T695" id="Seg_6480" s="T694">keps-iː-gin</ta>
            <ta e="T696" id="Seg_6481" s="T695">bu͡o</ta>
            <ta e="T697" id="Seg_6482" s="T696">mekti͡e-ti-n</ta>
            <ta e="T698" id="Seg_6483" s="T697">tugut-u-ŋ</ta>
            <ta e="T699" id="Seg_6484" s="T698">kuččukaːn</ta>
            <ta e="T700" id="Seg_6485" s="T699">bu͡o</ta>
            <ta e="T701" id="Seg_6486" s="T700">e-bit</ta>
            <ta e="T702" id="Seg_6487" s="T701">tüːn-ü</ta>
            <ta e="T703" id="Seg_6488" s="T702">bɨha</ta>
            <ta e="T704" id="Seg_6489" s="T703">utuj-bakka</ta>
            <ta e="T705" id="Seg_6490" s="T704">hɨldʼ-a-gɨn</ta>
            <ta e="T706" id="Seg_6491" s="T705">bu͡o</ta>
            <ta e="T707" id="Seg_6492" s="T706">ol</ta>
            <ta e="T708" id="Seg_6493" s="T707">tugut</ta>
            <ta e="T709" id="Seg_6494" s="T708">törüː-r-ü-n</ta>
            <ta e="T710" id="Seg_6495" s="T709">ket-iː</ta>
            <ta e="T711" id="Seg_6496" s="T710">ügüs</ta>
            <ta e="T712" id="Seg_6497" s="T711">taba-ga</ta>
            <ta e="T715" id="Seg_6498" s="T714">elbek</ta>
            <ta e="T716" id="Seg_6499" s="T715">tugut</ta>
            <ta e="T717" id="Seg_6500" s="T716">bu͡o</ta>
         </annotation>
         <annotation name="mp" tierref="mp-ChSA">
            <ta e="T7" id="Seg_6501" s="T6">teːte-LAr-BIt</ta>
            <ta e="T8" id="Seg_6502" s="T7">inʼe-LAr-BIt</ta>
            <ta e="T9" id="Seg_6503" s="T8">dʼe</ta>
            <ta e="T10" id="Seg_6504" s="T9">otto</ta>
            <ta e="T11" id="Seg_6505" s="T10">kɨrdʼagas-LAr</ta>
            <ta e="T12" id="Seg_6506" s="T11">e-TI-tA</ta>
            <ta e="T13" id="Seg_6507" s="T12">bu͡o</ta>
            <ta e="T14" id="Seg_6508" s="T13">U-nI</ta>
            <ta e="T15" id="Seg_6509" s="T14">A-nI</ta>
            <ta e="T16" id="Seg_6510" s="T15">bil-BAT-LAr</ta>
            <ta e="T19" id="Seg_6511" s="T18">meːne</ta>
            <ta e="T22" id="Seg_6512" s="T21">iliː</ta>
            <ta e="T23" id="Seg_6513" s="T22">battaː-Ar-LArA</ta>
            <ta e="T24" id="Seg_6514" s="T23">mannɨk</ta>
            <ta e="T25" id="Seg_6515" s="T24">gɨn-AːččI-BIn</ta>
            <ta e="T26" id="Seg_6516" s="T25">min</ta>
            <ta e="T27" id="Seg_6517" s="T26">emi͡e</ta>
            <ta e="T28" id="Seg_6518" s="T27">h-anɨ</ta>
            <ta e="T29" id="Seg_6519" s="T28">hin</ta>
            <ta e="T30" id="Seg_6520" s="T29">itinnik</ta>
            <ta e="T31" id="Seg_6521" s="T30">keri͡ete</ta>
            <ta e="T32" id="Seg_6522" s="T31">dʼe</ta>
            <ta e="T33" id="Seg_6523" s="T32">öj-I-m</ta>
            <ta e="T34" id="Seg_6524" s="T33">köt-BIT-tA</ta>
            <ta e="T35" id="Seg_6525" s="T34">bert</ta>
            <ta e="T36" id="Seg_6526" s="T35">dʼɨl-nI</ta>
            <ta e="T37" id="Seg_6527" s="T36">bɨha</ta>
            <ta e="T38" id="Seg_6528" s="T37">ɨ͡arɨj-I-BIT-I-m</ta>
            <ta e="T39" id="Seg_6529" s="T38">kata</ta>
            <ta e="T40" id="Seg_6530" s="T39">ol</ta>
            <ta e="T41" id="Seg_6531" s="T40">ereːti</ta>
            <ta e="T42" id="Seg_6532" s="T41">ama-BIn</ta>
            <ta e="T43" id="Seg_6533" s="T42">hi͡ese</ta>
            <ta e="T44" id="Seg_6534" s="T43">kihi-GA</ta>
            <ta e="T45" id="Seg_6535" s="T44">kihi</ta>
            <ta e="T46" id="Seg_6536" s="T45">di͡e-A-t-Ar</ta>
            <ta e="T47" id="Seg_6537" s="T46">kördük</ta>
            <ta e="T48" id="Seg_6538" s="T47">hɨrɨt-A-BIn</ta>
            <ta e="T49" id="Seg_6539" s="T48">eːt</ta>
            <ta e="T50" id="Seg_6540" s="T49">üle-BA-r</ta>
            <ta e="T51" id="Seg_6541" s="T50">da</ta>
            <ta e="T52" id="Seg_6542" s="T51">ka</ta>
            <ta e="T53" id="Seg_6543" s="T52">üle-GA</ta>
            <ta e="T54" id="Seg_6544" s="T53">kuhagan</ta>
            <ta e="T55" id="Seg_6545" s="T54">bu</ta>
            <ta e="T56" id="Seg_6546" s="T55">ogo-LAr-BI-n</ta>
            <ta e="T57" id="Seg_6547" s="T56">iːt-I-n-AːrI-BIn</ta>
            <ta e="T58" id="Seg_6548" s="T57">bu</ta>
            <ta e="T59" id="Seg_6549" s="T58">tu͡ok-nI</ta>
            <ta e="T61" id="Seg_6550" s="T59">dolu͡oj</ta>
            <ta e="T62" id="Seg_6551" s="T61">tɨːn-BI-n</ta>
            <ta e="T63" id="Seg_6552" s="T62">bɨs-A</ta>
            <ta e="T64" id="Seg_6553" s="T63">möŋ-I-n-AːččI-BIn</ta>
            <ta e="T65" id="Seg_6554" s="T64">taba</ta>
            <ta e="T66" id="Seg_6555" s="T65">hɨrɨː-tA</ta>
            <ta e="T67" id="Seg_6556" s="T66">ajɨː</ta>
            <ta e="T68" id="Seg_6557" s="T67">du͡o</ta>
            <ta e="T69" id="Seg_6558" s="T68">urut</ta>
            <ta e="T70" id="Seg_6559" s="T69">elbek-tA</ta>
            <ta e="T71" id="Seg_6560" s="T70">da</ta>
            <ta e="T72" id="Seg_6561" s="T71">ɨ͡arakan-tA</ta>
            <ta e="T73" id="Seg_6562" s="T72">kihi</ta>
            <ta e="T74" id="Seg_6563" s="T73">heni͡e-tA</ta>
            <ta e="T75" id="Seg_6564" s="T74">es-n-An</ta>
            <ta e="T76" id="Seg_6565" s="T75">bar-An</ta>
            <ta e="T77" id="Seg_6566" s="T76">kel-AːččI</ta>
            <ta e="T78" id="Seg_6567" s="T77">bukatɨn</ta>
            <ta e="T79" id="Seg_6568" s="T78">u͡on</ta>
            <ta e="T80" id="Seg_6569" s="T79">biːr-LAr-GA</ta>
            <ta e="T81" id="Seg_6570" s="T80">kel-AːččI-BIt</ta>
            <ta e="T82" id="Seg_6571" s="T81">karaŋa</ta>
            <ta e="T83" id="Seg_6572" s="T82">kɨhɨn</ta>
            <ta e="T84" id="Seg_6573" s="T83">oː</ta>
            <ta e="T85" id="Seg_6574" s="T84">tɨmnɨː</ta>
            <ta e="T86" id="Seg_6575" s="T85">bagajɨ</ta>
            <ta e="T87" id="Seg_6576" s="T86">bu͡o</ta>
            <ta e="T90" id="Seg_6577" s="T89">Popigaj</ta>
            <ta e="T91" id="Seg_6578" s="T90">tɨ͡a-tI-GAr</ta>
            <ta e="T92" id="Seg_6579" s="T91">mas</ta>
            <ta e="T93" id="Seg_6580" s="T92">mas</ta>
            <ta e="T94" id="Seg_6581" s="T93">bu͡o</ta>
            <ta e="T95" id="Seg_6582" s="T94">haːtar</ta>
            <ta e="T96" id="Seg_6583" s="T95">bukatɨn</ta>
            <ta e="T97" id="Seg_6584" s="T96">taba</ta>
            <ta e="T98" id="Seg_6585" s="T97">kanna</ta>
            <ta e="T99" id="Seg_6586" s="T98">da</ta>
            <ta e="T100" id="Seg_6587" s="T99">köhün-I-BAT</ta>
            <ta e="T101" id="Seg_6588" s="T100">onnuk-LAr-GA</ta>
            <ta e="T102" id="Seg_6589" s="T101">üleleː-An</ta>
            <ta e="T103" id="Seg_6590" s="T102">muŋ-LAN-BIT-I-m</ta>
            <ta e="T104" id="Seg_6591" s="T103">min</ta>
            <ta e="T105" id="Seg_6592" s="T104">haːmaj</ta>
            <ta e="T106" id="Seg_6593" s="T105">ol</ta>
            <ta e="T107" id="Seg_6594" s="T106">gɨn-AːrI</ta>
            <ta e="T108" id="Seg_6595" s="T107">dʼolloːk</ta>
            <ta e="T109" id="Seg_6596" s="T108">bu͡ol-An-BIn</ta>
            <ta e="T110" id="Seg_6597" s="T109">taba-m</ta>
            <ta e="T111" id="Seg_6598" s="T110">iːt-I-LIN-I-BIT-tA</ta>
            <ta e="T112" id="Seg_6599" s="T111">hürdeːk</ta>
            <ta e="T113" id="Seg_6600" s="T112">u͡on-ttAn</ta>
            <ta e="T114" id="Seg_6601" s="T113">taksa</ta>
            <ta e="T115" id="Seg_6602" s="T114">tɨːhɨčča-nI</ta>
            <ta e="T116" id="Seg_6603" s="T115">iːt-BIT-I-m</ta>
            <ta e="T117" id="Seg_6604" s="T116">kas</ta>
            <ta e="T118" id="Seg_6605" s="T117">da</ta>
            <ta e="T119" id="Seg_6606" s="T118">stada-GA</ta>
            <ta e="T120" id="Seg_6607" s="T119">bi͡er-BIT-I-m</ta>
            <ta e="T121" id="Seg_6608" s="T120">stada-GA</ta>
            <ta e="T122" id="Seg_6609" s="T121">bi͡er-BIT-I-m</ta>
            <ta e="T130" id="Seg_6610" s="T129">brigada</ta>
            <ta e="T131" id="Seg_6611" s="T130">meːr</ta>
            <ta e="T132" id="Seg_6612" s="T131">anɨ-GA</ta>
            <ta e="T133" id="Seg_6613" s="T132">di͡eri</ta>
            <ta e="T134" id="Seg_6614" s="T133">bu</ta>
            <ta e="T135" id="Seg_6615" s="T134">anɨ</ta>
            <ta e="T136" id="Seg_6616" s="T135">u͡ol-I-m</ta>
            <ta e="T137" id="Seg_6617" s="T136">iti</ta>
            <ta e="T138" id="Seg_6618" s="T137">obu͡ojdan-AːččI</ta>
            <ta e="T139" id="Seg_6619" s="T138">Valodʼa-m</ta>
            <ta e="T140" id="Seg_6620" s="T139">čogotok</ta>
            <ta e="T141" id="Seg_6621" s="T140">u͡ol-LAːK-BIn</ta>
            <ta e="T148" id="Seg_6622" s="T147">mm</ta>
            <ta e="T149" id="Seg_6623" s="T148">kɨrɨj-A-BIt</ta>
            <ta e="T150" id="Seg_6624" s="T149">ikki-IAn-BIt</ta>
            <ta e="T151" id="Seg_6625" s="T150">kɨrɨj-TI-BIt</ta>
            <ta e="T167" id="Seg_6626" s="T166">brʼigadʼiːr-LAr-BIt</ta>
            <ta e="T168" id="Seg_6627" s="T167">hi͡ese</ta>
            <ta e="T169" id="Seg_6628" s="T168">kihi-LAr</ta>
            <ta e="T170" id="Seg_6629" s="T169">e-TI-LArA</ta>
            <ta e="T171" id="Seg_6630" s="T170">onton</ta>
            <ta e="T174" id="Seg_6631" s="T173">ü͡ören-A</ta>
            <ta e="T175" id="Seg_6632" s="T174">ü͡ören-BIT-I-nAn</ta>
            <ta e="T176" id="Seg_6633" s="T175">h-onon</ta>
            <ta e="T177" id="Seg_6634" s="T176">ü͡ören-An</ta>
            <ta e="T178" id="Seg_6635" s="T177">kaːl-TAK-BIt</ta>
            <ta e="T179" id="Seg_6636" s="T178">diː</ta>
            <ta e="T180" id="Seg_6637" s="T179">bihigi</ta>
            <ta e="T181" id="Seg_6638" s="T180">iti</ta>
            <ta e="T182" id="Seg_6639" s="T181">kečes-Iː-GA</ta>
            <ta e="T183" id="Seg_6640" s="T182">taba-GA</ta>
            <ta e="T184" id="Seg_6641" s="T183">taba-nI</ta>
            <ta e="T185" id="Seg_6642" s="T184">hüter-I-m-ŋ</ta>
            <ta e="T186" id="Seg_6643" s="T185">di͡e-An</ta>
            <ta e="T187" id="Seg_6644" s="T186">di͡e-AːččI-BIn</ta>
            <ta e="T188" id="Seg_6645" s="T187">bu͡o</ta>
            <ta e="T189" id="Seg_6646" s="T188">taba-nI</ta>
            <ta e="T190" id="Seg_6647" s="T189">hüter-I-m-ŋ</ta>
            <ta e="T191" id="Seg_6648" s="T190">harsɨŋŋɨ-GItI-n</ta>
            <ta e="T192" id="Seg_6649" s="T191">öjdöː-ŋ</ta>
            <ta e="T193" id="Seg_6650" s="T192">di͡e-AːččI-BIn</ta>
            <ta e="T194" id="Seg_6651" s="T193">harsi͡erda</ta>
            <ta e="T195" id="Seg_6652" s="T194">plaːn-LAN-A</ta>
            <ta e="T196" id="Seg_6653" s="T195">tur-I-ŋ</ta>
            <ta e="T197" id="Seg_6654" s="T196">di͡e-AːččI-BIn</ta>
            <ta e="T198" id="Seg_6655" s="T197">kajdi͡ek</ta>
            <ta e="T199" id="Seg_6656" s="T198">üleleː-Ar-GItI-n</ta>
            <ta e="T200" id="Seg_6657" s="T199">kajdi͡ek</ta>
            <ta e="T201" id="Seg_6658" s="T200">bar-Ar-GItI-n</ta>
            <ta e="T202" id="Seg_6659" s="T201">taba-ŋ</ta>
            <ta e="T203" id="Seg_6660" s="T202">hüt-Ar-A</ta>
            <ta e="T204" id="Seg_6661" s="T203">ügüs-tA</ta>
            <ta e="T205" id="Seg_6662" s="T204">muŋ-tA</ta>
            <ta e="T206" id="Seg_6663" s="T205">öj</ta>
            <ta e="T207" id="Seg_6664" s="T206">kim-LAː-Ar</ta>
            <ta e="T208" id="Seg_6665" s="T207">hajɨn</ta>
            <ta e="T209" id="Seg_6666" s="T208">hürdeːk</ta>
            <ta e="T210" id="Seg_6667" s="T209">ajan-LAN-n-A</ta>
            <ta e="T211" id="Seg_6668" s="T210">bu͡ol-AːččI</ta>
            <ta e="T212" id="Seg_6669" s="T211">ol</ta>
            <ta e="T213" id="Seg_6670" s="T212">bačča</ta>
            <ta e="T214" id="Seg_6671" s="T213">hɨrɨt-IAK-BA-r</ta>
            <ta e="T215" id="Seg_6672" s="T214">di͡eri</ta>
            <ta e="T217" id="Seg_6673" s="T215">bu͡o</ta>
            <ta e="T218" id="Seg_6674" s="T217">du͡o</ta>
            <ta e="T219" id="Seg_6675" s="T218">meːr</ta>
            <ta e="T220" id="Seg_6676" s="T219">üčügej</ta>
            <ta e="T221" id="Seg_6677" s="T220">aːt-I-nAn</ta>
            <ta e="T222" id="Seg_6678" s="T221">tagɨs-TI-m</ta>
            <ta e="T223" id="Seg_6679" s="T222">eːt</ta>
            <ta e="T224" id="Seg_6680" s="T223">otto</ta>
            <ta e="T225" id="Seg_6681" s="T224">hajɨn</ta>
            <ta e="T226" id="Seg_6682" s="T225">da</ta>
            <ta e="T227" id="Seg_6683" s="T226">kɨhɨn</ta>
            <ta e="T228" id="Seg_6684" s="T227">da</ta>
            <ta e="T229" id="Seg_6685" s="T228">hin</ta>
            <ta e="T230" id="Seg_6686" s="T229">üleleː-TAK-GA</ta>
            <ta e="T231" id="Seg_6687" s="T230">elbek</ta>
            <ta e="T232" id="Seg_6688" s="T231">dʼɨl-nI</ta>
            <ta e="T233" id="Seg_6689" s="T232">üleleː-TI-m</ta>
            <ta e="T234" id="Seg_6690" s="T233">bi͡es-u͡on</ta>
            <ta e="T235" id="Seg_6691" s="T234">bi͡es</ta>
            <ta e="T236" id="Seg_6692" s="T235">dʼɨl-nI</ta>
            <ta e="T237" id="Seg_6693" s="T236">pʼensija-GA</ta>
            <ta e="T238" id="Seg_6694" s="T237">kiːr-An</ta>
            <ta e="T239" id="Seg_6695" s="T238">baran</ta>
            <ta e="T240" id="Seg_6696" s="T239">össü͡ö</ta>
            <ta e="T241" id="Seg_6697" s="T240">üleleː-BIT-I-m</ta>
            <ta e="T242" id="Seg_6698" s="T241">taba-m</ta>
            <ta e="T243" id="Seg_6699" s="T242">iːt-I-LIN-Ar-tI-ttAn</ta>
            <ta e="T257" id="Seg_6700" s="T256">hüt-BIT-tA</ta>
            <ta e="T258" id="Seg_6701" s="T257">bu͡o</ta>
            <ta e="T259" id="Seg_6702" s="T258">öl-An</ta>
            <ta e="T260" id="Seg_6703" s="T259">hüt-An</ta>
            <ta e="T261" id="Seg_6704" s="T260">hüt-BIT-tA</ta>
            <ta e="T262" id="Seg_6705" s="T261">iti</ta>
            <ta e="T263" id="Seg_6706" s="T262">kim-LAr-GA</ta>
            <ta e="T264" id="Seg_6707" s="T263">Haskɨlaːk-LAr-GA</ta>
            <ta e="T265" id="Seg_6708" s="T264">bar-An</ta>
            <ta e="T266" id="Seg_6709" s="T265">bar-An</ta>
            <ta e="T267" id="Seg_6710" s="T266">daːganɨ</ta>
            <ta e="T268" id="Seg_6711" s="T267">kɨːl</ta>
            <ta e="T269" id="Seg_6712" s="T268">da</ta>
            <ta e="T270" id="Seg_6713" s="T269">ilin-Ar</ta>
            <ta e="T271" id="Seg_6714" s="T270">kɨːl</ta>
            <ta e="T272" id="Seg_6715" s="T271">ilin-AːččI</ta>
            <ta e="T273" id="Seg_6716" s="T272">mʼigracɨja-tI-GAr</ta>
            <ta e="T274" id="Seg_6717" s="T273">kühün</ta>
            <ta e="T275" id="Seg_6718" s="T274">kel-Iː-tI-GAr</ta>
            <ta e="T276" id="Seg_6719" s="T275">ka</ta>
            <ta e="T277" id="Seg_6720" s="T276">börö</ta>
            <ta e="T278" id="Seg_6721" s="T277">hi͡e-Ar</ta>
            <ta e="T279" id="Seg_6722" s="T278">ulakan</ta>
            <ta e="T280" id="Seg_6723" s="T279">hürdeːk</ta>
            <ta e="T281" id="Seg_6724" s="T280">ɨ͡arɨj-Ar</ta>
            <ta e="T282" id="Seg_6725" s="T281">ol-GA</ta>
            <ta e="T283" id="Seg_6726" s="T282">atak-tA</ta>
            <ta e="T284" id="Seg_6727" s="T283">da</ta>
            <ta e="T285" id="Seg_6728" s="T284">honno</ta>
            <ta e="T286" id="Seg_6729" s="T285">ör</ta>
            <ta e="T287" id="Seg_6730" s="T286">bar-AːččI-tA</ta>
            <ta e="T288" id="Seg_6731" s="T287">hu͡ok</ta>
            <ta e="T302" id="Seg_6732" s="T301">ölör-TAr-Ar-LAr</ta>
            <ta e="T303" id="Seg_6733" s="T302">hannɨ</ta>
            <ta e="T304" id="Seg_6734" s="T303">otto</ta>
            <ta e="T305" id="Seg_6735" s="T304">baːr</ta>
            <ta e="T306" id="Seg_6736" s="T305">bu͡ol-TAK-InA</ta>
            <ta e="T307" id="Seg_6737" s="T306">di͡e-Ar-LAr</ta>
            <ta e="T308" id="Seg_6738" s="T307">diː</ta>
            <ta e="T309" id="Seg_6739" s="T308">anɨ</ta>
            <ta e="T310" id="Seg_6740" s="T309">premija-LAː-Ar</ta>
            <ta e="T311" id="Seg_6741" s="T310">bu͡ol-BIT-LAr</ta>
            <ta e="T312" id="Seg_6742" s="T311">urut</ta>
            <ta e="T313" id="Seg_6743" s="T312">premija-LAː-AːččI-tA</ta>
            <ta e="T314" id="Seg_6744" s="T313">hu͡ok-LAr</ta>
            <ta e="T316" id="Seg_6745" s="T315">e-TI-LAr</ta>
            <ta e="T320" id="Seg_6746" s="T319">bihi͡ene-BIt</ta>
            <ta e="T321" id="Seg_6747" s="T320">kim-BIt</ta>
            <ta e="T322" id="Seg_6748" s="T321">keː</ta>
            <ta e="T323" id="Seg_6749" s="T322">rascenka-BIt</ta>
            <ta e="T324" id="Seg_6750" s="T323">kuhagan</ta>
            <ta e="T325" id="Seg_6751" s="T324">e-TI-tA</ta>
            <ta e="T326" id="Seg_6752" s="T325">urut</ta>
            <ta e="T327" id="Seg_6753" s="T326">kɨra</ta>
            <ta e="T328" id="Seg_6754" s="T327">küččügüj</ta>
            <ta e="T329" id="Seg_6755" s="T328">e-TI-tA</ta>
            <ta e="T330" id="Seg_6756" s="T329">taba-BIt</ta>
            <ta e="T331" id="Seg_6757" s="T330">gi͡en-tA</ta>
            <ta e="T332" id="Seg_6758" s="T331">ka</ta>
            <ta e="T333" id="Seg_6759" s="T332">ol-nI</ta>
            <ta e="T334" id="Seg_6760" s="T333">min</ta>
            <ta e="T335" id="Seg_6761" s="T334">ürdeː-A-t-I-ŋ</ta>
            <ta e="T336" id="Seg_6762" s="T335">di͡e-A</ta>
            <ta e="T337" id="Seg_6763" s="T336">hataː-An</ta>
            <ta e="T338" id="Seg_6764" s="T337">anɨ</ta>
            <ta e="T339" id="Seg_6765" s="T338">tɨl</ta>
            <ta e="T340" id="Seg_6766" s="T339">ihit-IAK-LArA</ta>
            <ta e="T341" id="Seg_6767" s="T340">bu͡o</ta>
            <ta e="T342" id="Seg_6768" s="T341">anɨ</ta>
            <ta e="T343" id="Seg_6769" s="T342">ülehit-LAr-BIt</ta>
            <ta e="T344" id="Seg_6770" s="T343">kuhagan-LAr</ta>
            <ta e="T345" id="Seg_6771" s="T344">ol-GA</ta>
            <ta e="T346" id="Seg_6772" s="T345">dʼe</ta>
            <ta e="T353" id="Seg_6773" s="T352">mm</ta>
            <ta e="T354" id="Seg_6774" s="T353">di͡e-A-BIt</ta>
            <ta e="T355" id="Seg_6775" s="T354">bu͡ol</ta>
            <ta e="T356" id="Seg_6776" s="T355">kü͡ömej</ta>
            <ta e="T357" id="Seg_6777" s="T356">iːt-IAK-LArI-n</ta>
            <ta e="T358" id="Seg_6778" s="T357">taba</ta>
            <ta e="T359" id="Seg_6779" s="T358">rascenka-tI-n</ta>
            <ta e="T360" id="Seg_6780" s="T359">ürdeː-A-t-IAK-GA</ta>
            <ta e="T361" id="Seg_6781" s="T360">di͡e-A-BIt</ta>
            <ta e="T364" id="Seg_6782" s="T363">gini͡ennere</ta>
            <ta e="T365" id="Seg_6783" s="T364">kim-LArA</ta>
            <ta e="T366" id="Seg_6784" s="T365">Ürüŋ Kaja-LAr</ta>
            <ta e="T367" id="Seg_6785" s="T366">gi͡en-LArA</ta>
            <ta e="T368" id="Seg_6786" s="T367">bu͡o</ta>
            <ta e="T369" id="Seg_6787" s="T368">biːr</ta>
            <ta e="T370" id="Seg_6788" s="T369">taba-GA</ta>
            <ta e="T371" id="Seg_6789" s="T370">kas</ta>
            <ta e="T372" id="Seg_6790" s="T371">di͡e-BIT-LArA=Ij</ta>
            <ta e="T373" id="Seg_6791" s="T372">suːtka-GA</ta>
            <ta e="T374" id="Seg_6792" s="T373">tü͡ört</ta>
            <ta e="T375" id="Seg_6793" s="T374">tü͡ört</ta>
            <ta e="T376" id="Seg_6794" s="T375">holku͡obaj</ta>
            <ta e="T381" id="Seg_6795" s="T380">iːt-Ar-LArI-n</ta>
            <ta e="T382" id="Seg_6796" s="T381">ihin</ta>
            <ta e="T383" id="Seg_6797" s="T382">bu͡o</ta>
            <ta e="T384" id="Seg_6798" s="T383">he-he</ta>
            <ta e="T387" id="Seg_6799" s="T386">taba-LAː-An</ta>
            <ta e="T388" id="Seg_6800" s="T387">iːt-An</ta>
            <ta e="T389" id="Seg_6801" s="T388">tagɨs-Ar-LArI-n</ta>
            <ta e="T390" id="Seg_6802" s="T389">ihin</ta>
            <ta e="T391" id="Seg_6803" s="T390">gini͡ennere</ta>
            <ta e="T393" id="Seg_6804" s="T391">emi͡e</ta>
            <ta e="T394" id="Seg_6805" s="T393">purgaː</ta>
            <ta e="T395" id="Seg_6806" s="T394">dʼadaŋɨ</ta>
            <ta e="T396" id="Seg_6807" s="T395">gɨn-Ar</ta>
            <ta e="T397" id="Seg_6808" s="T396">bu͡o</ta>
            <ta e="T398" id="Seg_6809" s="T397">Ürüŋ Kaja-ŋ</ta>
            <ta e="T399" id="Seg_6810" s="T398">hir-tA</ta>
            <ta e="T400" id="Seg_6811" s="T399">hürdeːk</ta>
            <ta e="T401" id="Seg_6812" s="T400">dʼaːŋɨ</ta>
            <ta e="T402" id="Seg_6813" s="T401">hir</ta>
            <ta e="T403" id="Seg_6814" s="T402">ol</ta>
            <ta e="T404" id="Seg_6815" s="T403">ihin</ta>
            <ta e="T405" id="Seg_6816" s="T404">ol</ta>
            <ta e="T406" id="Seg_6817" s="T405">rascenka-LAr-I-ŋ</ta>
            <ta e="T407" id="Seg_6818" s="T406">ürdeː-A-t-BIT-LAr</ta>
            <ta e="T408" id="Seg_6819" s="T407">maŋnaj-GI-ttAn</ta>
            <ta e="T409" id="Seg_6820" s="T408">karaːn-ttAn</ta>
            <ta e="T410" id="Seg_6821" s="T409">urukku-ttAn</ta>
            <ta e="T411" id="Seg_6822" s="T410">taba</ta>
            <ta e="T412" id="Seg_6823" s="T411">iːt-Iː</ta>
            <ta e="T413" id="Seg_6824" s="T412">hürdeːk</ta>
            <ta e="T414" id="Seg_6825" s="T413">bu͡o</ta>
            <ta e="T415" id="Seg_6826" s="T414">kɨtaːnak-tA</ta>
            <ta e="T418" id="Seg_6827" s="T417">eː</ta>
            <ta e="T419" id="Seg_6828" s="T418">öl-Ar</ta>
            <ta e="T420" id="Seg_6829" s="T419">öl-Ar</ta>
            <ta e="T421" id="Seg_6830" s="T420">kihi-ŋ</ta>
            <ta e="T422" id="Seg_6831" s="T421">öl-Ar</ta>
            <ta e="T423" id="Seg_6832" s="T422">ol</ta>
            <ta e="T425" id="Seg_6833" s="T424">taba-nI</ta>
            <ta e="T426" id="Seg_6834" s="T425">kɨtta</ta>
            <ta e="T427" id="Seg_6835" s="T426">barɨs-An</ta>
            <ta e="T442" id="Seg_6836" s="T441">iti</ta>
            <ta e="T443" id="Seg_6837" s="T442">haka</ta>
            <ta e="T444" id="Seg_6838" s="T443">hir-tI-GAr</ta>
            <ta e="T445" id="Seg_6839" s="T444">onnuk-LAr</ta>
            <ta e="T446" id="Seg_6840" s="T445">e-TI-tA</ta>
            <ta e="T447" id="Seg_6841" s="T446">taba-nI</ta>
            <ta e="T448" id="Seg_6842" s="T447">hürdeːk</ta>
            <ta e="T449" id="Seg_6843" s="T448">kɨlɨːlaː-Ar-LAr</ta>
            <ta e="T450" id="Seg_6844" s="T449">iːt-IAK-LArI-n</ta>
            <ta e="T451" id="Seg_6845" s="T450">bagar-Ar-LAr</ta>
            <ta e="T452" id="Seg_6846" s="T451">araː</ta>
            <ta e="T453" id="Seg_6847" s="T452">karčɨ</ta>
            <ta e="T454" id="Seg_6848" s="T453">ɨl-Ar-LAr</ta>
            <ta e="T455" id="Seg_6849" s="T454">üčügej</ta>
            <ta e="T456" id="Seg_6850" s="T455">bu͡ol-TI-tA</ta>
            <ta e="T457" id="Seg_6851" s="T456">taba-ČIt-LAr-I-ŋ</ta>
            <ta e="T458" id="Seg_6852" s="T457">ɨj-GA</ta>
            <ta e="T459" id="Seg_6853" s="T458">tü͡ört-LIː</ta>
            <ta e="T460" id="Seg_6854" s="T459">bi͡es-LIː</ta>
            <ta e="T461" id="Seg_6855" s="T460">tɨːhɨčča-nI</ta>
            <ta e="T462" id="Seg_6856" s="T461">biːr</ta>
            <ta e="T463" id="Seg_6857" s="T462">ɨj-GA</ta>
            <ta e="T464" id="Seg_6858" s="T463">ɨl-Ar-LAr</ta>
            <ta e="T465" id="Seg_6859" s="T464">giniler</ta>
            <ta e="T466" id="Seg_6860" s="T465">bihigi</ta>
            <ta e="T467" id="Seg_6861" s="T466">ol</ta>
            <ta e="T468" id="Seg_6862" s="T467">kaččaga</ta>
            <ta e="T471" id="Seg_6863" s="T470">onnuk-tA</ta>
            <ta e="T472" id="Seg_6864" s="T471">da</ta>
            <ta e="T474" id="Seg_6865" s="T472">hu͡ok-BIT</ta>
            <ta e="T475" id="Seg_6866" s="T474">törüt</ta>
            <ta e="T477" id="Seg_6867" s="T475">hu͡ok</ta>
            <ta e="T478" id="Seg_6868" s="T477">meːne</ta>
            <ta e="T479" id="Seg_6869" s="T478">bi͡es</ta>
            <ta e="T480" id="Seg_6870" s="T479">hüːs</ta>
            <ta e="T481" id="Seg_6871" s="T480">bu͡olu͡o</ta>
            <ta e="T482" id="Seg_6872" s="T481">du͡o</ta>
            <ta e="T483" id="Seg_6873" s="T482">avansa</ta>
            <ta e="T484" id="Seg_6874" s="T483">kördük</ta>
            <ta e="T486" id="Seg_6875" s="T484">rascenka-tA-BIT</ta>
            <ta e="T487" id="Seg_6876" s="T486">agɨjak-tA</ta>
            <ta e="T488" id="Seg_6877" s="T487">bert</ta>
            <ta e="T489" id="Seg_6878" s="T488">kepejki</ta>
            <ta e="T490" id="Seg_6879" s="T489">anɨ-GA</ta>
            <ta e="T491" id="Seg_6880" s="T490">di͡eri</ta>
            <ta e="T492" id="Seg_6881" s="T491">uhuk-I-ttAn</ta>
            <ta e="T493" id="Seg_6882" s="T492">hüter-TI-LAr</ta>
            <ta e="T494" id="Seg_6883" s="T493">anɨ</ta>
            <ta e="T495" id="Seg_6884" s="T494">tu͡ok-tI-n</ta>
            <ta e="T496" id="Seg_6885" s="T495">daːganɨ</ta>
            <ta e="T497" id="Seg_6886" s="T496">haŋar-A</ta>
            <ta e="T498" id="Seg_6887" s="T497">hataː-An</ta>
            <ta e="T499" id="Seg_6888" s="T498">togo</ta>
            <ta e="T500" id="Seg_6889" s="T499">tɨːt-A-GIt</ta>
            <ta e="T501" id="Seg_6890" s="T500">iti</ta>
            <ta e="T502" id="Seg_6891" s="T501">tu͡ok</ta>
            <ta e="T503" id="Seg_6892" s="T502">gɨn-AːrI</ta>
            <ta e="T504" id="Seg_6893" s="T503">hopku͡os-GIt</ta>
            <ta e="T505" id="Seg_6894" s="T504">hopku͡os-GIt</ta>
            <ta e="T506" id="Seg_6895" s="T505">taba-tA</ta>
            <ta e="T507" id="Seg_6896" s="T506">beje-GIt</ta>
            <ta e="T508" id="Seg_6897" s="T507">hi͡e-A-GIt</ta>
            <ta e="T509" id="Seg_6898" s="T508">bu͡o</ta>
            <ta e="T510" id="Seg_6899" s="T509">di͡e-AːččI-BIn</ta>
            <ta e="T511" id="Seg_6900" s="T510">taŋɨn-A-GIt</ta>
            <ta e="T512" id="Seg_6901" s="T511">kanʼaː-A-GIt</ta>
            <ta e="T513" id="Seg_6902" s="T512">togo</ta>
            <ta e="T514" id="Seg_6903" s="T513">hopku͡os</ta>
            <ta e="T515" id="Seg_6904" s="T514">gi͡en-tA</ta>
            <ta e="T516" id="Seg_6905" s="T515">di͡e-A-GIt</ta>
            <ta e="T517" id="Seg_6906" s="T516">di͡e-A-BIn</ta>
            <ta e="T518" id="Seg_6907" s="T517">beje-GIt</ta>
            <ta e="T519" id="Seg_6908" s="T518">baːj-GIt</ta>
            <ta e="T520" id="Seg_6909" s="T519">diː</ta>
            <ta e="T521" id="Seg_6910" s="T520">di͡e-A-BIn</ta>
            <ta e="T522" id="Seg_6911" s="T521">ol</ta>
            <ta e="T523" id="Seg_6912" s="T522">ihin</ta>
            <ta e="T524" id="Seg_6913" s="T523">karčɨ</ta>
            <ta e="T525" id="Seg_6914" s="T524">ɨl-A-GIt</ta>
            <ta e="T526" id="Seg_6915" s="T525">tu͡ok-nI=Ij</ta>
            <ta e="T527" id="Seg_6916" s="T526">tu͡ok-I-nAn</ta>
            <ta e="T530" id="Seg_6917" s="T529">ɨl-IAK-BIt</ta>
            <ta e="T531" id="Seg_6918" s="T530">di͡e-A-GIt</ta>
            <ta e="T532" id="Seg_6919" s="T531">di͡e-A-BIn</ta>
            <ta e="T533" id="Seg_6920" s="T532">anʼɨː</ta>
            <ta e="T534" id="Seg_6921" s="T533">bu͡ol-IAK.[tA]</ta>
            <ta e="T535" id="Seg_6922" s="T534">ulakan-LIk</ta>
            <ta e="T536" id="Seg_6923" s="T535">albun-LAː-BIT-I-m</ta>
            <ta e="T537" id="Seg_6924" s="T536">di͡e-TAK-BInA</ta>
            <ta e="T538" id="Seg_6925" s="T537">atɨːlaː-A-n-BAtAK-I-m</ta>
            <ta e="T539" id="Seg_6926" s="T538">da</ta>
            <ta e="T540" id="Seg_6927" s="T539">haːtar</ta>
            <ta e="T541" id="Seg_6928" s="T540">barɨta</ta>
            <ta e="T542" id="Seg_6929" s="T541">kihi-ŋ</ta>
            <ta e="T543" id="Seg_6930" s="T542">barɨ-tA</ta>
            <ta e="T544" id="Seg_6931" s="T543">atɨːlaː-A-n-BIT-tA</ta>
            <ta e="T545" id="Seg_6932" s="T544">ol</ta>
            <ta e="T546" id="Seg_6933" s="T545">hopku͡os-ttAn</ta>
            <ta e="T550" id="Seg_6934" s="T549">karčɨ</ta>
            <ta e="T551" id="Seg_6935" s="T550">oŋohun-Ar-LAr</ta>
            <ta e="T572" id="Seg_6936" s="T571">onnuk</ta>
            <ta e="T573" id="Seg_6937" s="T572">onnuk</ta>
            <ta e="T581" id="Seg_6938" s="T580">taba-ČIt</ta>
            <ta e="T582" id="Seg_6939" s="T581">kihi</ta>
            <ta e="T583" id="Seg_6940" s="T582">kemči</ta>
            <ta e="T584" id="Seg_6941" s="T583">dʼe</ta>
            <ta e="T585" id="Seg_6942" s="T584">ol-nI</ta>
            <ta e="T613" id="Seg_6943" s="T612">iti</ta>
            <ta e="T614" id="Seg_6944" s="T613">üčügej-LIk</ta>
            <ta e="T615" id="Seg_6945" s="T614">üleleː-Ar-I-m</ta>
            <ta e="T616" id="Seg_6946" s="T615">ihin</ta>
            <ta e="T617" id="Seg_6947" s="T616">tagɨs-I-BIT-I-m</ta>
            <ta e="T618" id="Seg_6948" s="T617">kihi-LAr-nI</ta>
            <ta e="T619" id="Seg_6949" s="T618">kɨtta</ta>
            <ta e="T620" id="Seg_6950" s="T619">kepset-Ar-I-m</ta>
            <ta e="T621" id="Seg_6951" s="T620">ihin</ta>
            <ta e="T622" id="Seg_6952" s="T621">hi͡ese</ta>
            <ta e="T623" id="Seg_6953" s="T622">eder</ta>
            <ta e="T624" id="Seg_6954" s="T623">er-TAK-BInA</ta>
            <ta e="T625" id="Seg_6955" s="T624">hürdeːk</ta>
            <ta e="T626" id="Seg_6956" s="T625">huːstraj</ta>
            <ta e="T627" id="Seg_6957" s="T626">e-TI-m</ta>
            <ta e="T628" id="Seg_6958" s="T627">bu͡ol</ta>
            <ta e="T629" id="Seg_6959" s="T628">ɨ͡arɨj-I-BIT-I-m</ta>
            <ta e="T630" id="Seg_6960" s="T629">da</ta>
            <ta e="T631" id="Seg_6961" s="T630">ihin</ta>
            <ta e="T632" id="Seg_6962" s="T631">kepset-Iː</ta>
            <ta e="T633" id="Seg_6963" s="T632">obu͡oj-LAːK</ta>
            <ta e="T634" id="Seg_6964" s="T633">e-TI-m</ta>
            <ta e="T635" id="Seg_6965" s="T634">nuːčča</ta>
            <ta e="T636" id="Seg_6966" s="T635">da</ta>
            <ta e="T637" id="Seg_6967" s="T636">bu͡ol</ta>
            <ta e="T638" id="Seg_6968" s="T637">haka</ta>
            <ta e="T639" id="Seg_6969" s="T638">da</ta>
            <ta e="T640" id="Seg_6970" s="T639">bu͡ol</ta>
            <ta e="T641" id="Seg_6971" s="T640">omuk</ta>
            <ta e="T642" id="Seg_6972" s="T641">da</ta>
            <ta e="T643" id="Seg_6973" s="T642">bu͡ol</ta>
            <ta e="T653" id="Seg_6974" s="T652">kün</ta>
            <ta e="T654" id="Seg_6975" s="T653">aːjɨ</ta>
            <ta e="T655" id="Seg_6976" s="T654">bu͡o</ta>
            <ta e="T656" id="Seg_6977" s="T655">kör-Ar-LArA</ta>
            <ta e="T657" id="Seg_6978" s="T656">bagas</ta>
            <ta e="T658" id="Seg_6979" s="T657">hɨːha-nI</ta>
            <ta e="T659" id="Seg_6980" s="T658">tu͡ok-LArA</ta>
            <ta e="T661" id="Seg_6981" s="T660">gɨn-A-GIn</ta>
            <ta e="T662" id="Seg_6982" s="T661">olus</ta>
            <ta e="T663" id="Seg_6983" s="T662">hɨːha-tA</ta>
            <ta e="T664" id="Seg_6984" s="T663">hu͡ok-BIn</ta>
            <ta e="T665" id="Seg_6985" s="T664">bu͡o</ta>
            <ta e="T666" id="Seg_6986" s="T665">ol</ta>
            <ta e="T667" id="Seg_6987" s="T666">ihin</ta>
            <ta e="T669" id="Seg_6988" s="T667">nuːčča-LAr</ta>
            <ta e="T694" id="Seg_6989" s="T693">töröː-BIT-tI-n</ta>
            <ta e="T695" id="Seg_6990" s="T694">kepseː-A-GIn</ta>
            <ta e="T696" id="Seg_6991" s="T695">bu͡o</ta>
            <ta e="T697" id="Seg_6992" s="T696">mekti͡e-tI-n</ta>
            <ta e="T698" id="Seg_6993" s="T697">tugut-I-ŋ</ta>
            <ta e="T699" id="Seg_6994" s="T698">küččükkeːn</ta>
            <ta e="T700" id="Seg_6995" s="T699">bu͡o</ta>
            <ta e="T701" id="Seg_6996" s="T700">e-BIT</ta>
            <ta e="T702" id="Seg_6997" s="T701">tüːn-nI</ta>
            <ta e="T703" id="Seg_6998" s="T702">bɨha</ta>
            <ta e="T704" id="Seg_6999" s="T703">utuj-BAkkA</ta>
            <ta e="T705" id="Seg_7000" s="T704">hɨrɨt-A-GIn</ta>
            <ta e="T706" id="Seg_7001" s="T705">bu͡o</ta>
            <ta e="T707" id="Seg_7002" s="T706">ol</ta>
            <ta e="T708" id="Seg_7003" s="T707">tugut</ta>
            <ta e="T709" id="Seg_7004" s="T708">töröː-Ar-tI-n</ta>
            <ta e="T710" id="Seg_7005" s="T709">keteː-A</ta>
            <ta e="T711" id="Seg_7006" s="T710">ügüs</ta>
            <ta e="T712" id="Seg_7007" s="T711">taba-GA</ta>
            <ta e="T715" id="Seg_7008" s="T714">elbek</ta>
            <ta e="T716" id="Seg_7009" s="T715">tugut</ta>
            <ta e="T717" id="Seg_7010" s="T716">bu͡o</ta>
         </annotation>
         <annotation name="ge" tierref="ge-ChSA">
            <ta e="T7" id="Seg_7011" s="T6">father-PL-1PL.[NOM]</ta>
            <ta e="T8" id="Seg_7012" s="T7">mother-PL-1PL.[NOM]</ta>
            <ta e="T9" id="Seg_7013" s="T8">well</ta>
            <ta e="T10" id="Seg_7014" s="T9">EMPH</ta>
            <ta e="T11" id="Seg_7015" s="T10">old-PL.[NOM]</ta>
            <ta e="T12" id="Seg_7016" s="T11">be-PST1-3SG</ta>
            <ta e="T13" id="Seg_7017" s="T12">EMPH</ta>
            <ta e="T14" id="Seg_7018" s="T13">U-ACC</ta>
            <ta e="T15" id="Seg_7019" s="T14">A-ACC</ta>
            <ta e="T16" id="Seg_7020" s="T15">know-NEG-3PL</ta>
            <ta e="T19" id="Seg_7021" s="T18">simply</ta>
            <ta e="T22" id="Seg_7022" s="T21">hand.[NOM]</ta>
            <ta e="T23" id="Seg_7023" s="T22">weigh-PRS-3PL</ta>
            <ta e="T24" id="Seg_7024" s="T23">such</ta>
            <ta e="T25" id="Seg_7025" s="T24">make-HAB-1SG</ta>
            <ta e="T26" id="Seg_7026" s="T25">1SG.[NOM]</ta>
            <ta e="T27" id="Seg_7027" s="T26">also</ta>
            <ta e="T28" id="Seg_7028" s="T27">EMPH-now</ta>
            <ta e="T29" id="Seg_7029" s="T28">however</ta>
            <ta e="T30" id="Seg_7030" s="T29">such.[NOM]</ta>
            <ta e="T31" id="Seg_7031" s="T30">like</ta>
            <ta e="T32" id="Seg_7032" s="T31">well</ta>
            <ta e="T33" id="Seg_7033" s="T32">mind-EP-1SG.[NOM]</ta>
            <ta e="T34" id="Seg_7034" s="T33">fly-PST2-3SG</ta>
            <ta e="T35" id="Seg_7035" s="T34">very</ta>
            <ta e="T36" id="Seg_7036" s="T35">year-ACC</ta>
            <ta e="T37" id="Seg_7037" s="T36">during</ta>
            <ta e="T38" id="Seg_7038" s="T37">be.sick-EP-PST2-EP-1SG</ta>
            <ta e="T39" id="Seg_7039" s="T38">instead</ta>
            <ta e="T40" id="Seg_7040" s="T39">that.[NOM]</ta>
            <ta e="T41" id="Seg_7041" s="T40">in.spite.of</ta>
            <ta e="T42" id="Seg_7042" s="T41">normal-1SG</ta>
            <ta e="T43" id="Seg_7043" s="T42">a.little</ta>
            <ta e="T44" id="Seg_7044" s="T43">human.being-DAT/LOC</ta>
            <ta e="T45" id="Seg_7045" s="T44">human.being.[NOM]</ta>
            <ta e="T46" id="Seg_7046" s="T45">say-EP-CAUS-PTCP.PRS.[NOM]</ta>
            <ta e="T47" id="Seg_7047" s="T46">similar</ta>
            <ta e="T48" id="Seg_7048" s="T47">go-PRS-1SG</ta>
            <ta e="T49" id="Seg_7049" s="T48">EVID</ta>
            <ta e="T50" id="Seg_7050" s="T49">work-1SG-DAT/LOC</ta>
            <ta e="T51" id="Seg_7051" s="T50">and</ta>
            <ta e="T52" id="Seg_7052" s="T51">well</ta>
            <ta e="T53" id="Seg_7053" s="T52">work-DAT/LOC</ta>
            <ta e="T54" id="Seg_7054" s="T53">bad.[NOM]</ta>
            <ta e="T55" id="Seg_7055" s="T54">this</ta>
            <ta e="T56" id="Seg_7056" s="T55">child-PL-1SG-ACC</ta>
            <ta e="T57" id="Seg_7057" s="T56">bring.up-EP-MED-CVB.PURP-1SG</ta>
            <ta e="T58" id="Seg_7058" s="T57">this</ta>
            <ta e="T59" id="Seg_7059" s="T58">what-ACC</ta>
            <ta e="T61" id="Seg_7060" s="T59">completely</ta>
            <ta e="T62" id="Seg_7061" s="T61">power-1SG-ACC</ta>
            <ta e="T63" id="Seg_7062" s="T62">cut-CVB.SIM</ta>
            <ta e="T64" id="Seg_7063" s="T63">move-EP-REFL-HAB-1SG</ta>
            <ta e="T65" id="Seg_7064" s="T64">reindeer.[NOM]</ta>
            <ta e="T66" id="Seg_7065" s="T65">trip-3SG.[NOM]</ta>
            <ta e="T67" id="Seg_7066" s="T66">holy.[NOM]</ta>
            <ta e="T68" id="Seg_7067" s="T67">MOD</ta>
            <ta e="T69" id="Seg_7068" s="T68">before</ta>
            <ta e="T70" id="Seg_7069" s="T69">many-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_7070" s="T70">and</ta>
            <ta e="T72" id="Seg_7071" s="T71">heavy-3SG.[NOM]</ta>
            <ta e="T73" id="Seg_7072" s="T72">human.being.[NOM]</ta>
            <ta e="T74" id="Seg_7073" s="T73">power-3SG.[NOM]</ta>
            <ta e="T75" id="Seg_7074" s="T74">throw.away-REFL-CVB.SEQ</ta>
            <ta e="T76" id="Seg_7075" s="T75">go-CVB.SEQ</ta>
            <ta e="T77" id="Seg_7076" s="T76">come-HAB.[3SG]</ta>
            <ta e="T78" id="Seg_7077" s="T77">completely</ta>
            <ta e="T79" id="Seg_7078" s="T78">ten</ta>
            <ta e="T80" id="Seg_7079" s="T79">one-PL-DAT/LOC</ta>
            <ta e="T81" id="Seg_7080" s="T80">come-HAB-1PL</ta>
            <ta e="T82" id="Seg_7081" s="T81">polar.night.[NOM]</ta>
            <ta e="T83" id="Seg_7082" s="T82">in.winter</ta>
            <ta e="T84" id="Seg_7083" s="T83">oh</ta>
            <ta e="T85" id="Seg_7084" s="T84">cold.[NOM]</ta>
            <ta e="T86" id="Seg_7085" s="T85">very</ta>
            <ta e="T87" id="Seg_7086" s="T86">EMPH</ta>
            <ta e="T90" id="Seg_7087" s="T89">Popigaj.[NOM]</ta>
            <ta e="T91" id="Seg_7088" s="T90">tundra-3SG-DAT/LOC</ta>
            <ta e="T92" id="Seg_7089" s="T91">wood.[NOM]</ta>
            <ta e="T93" id="Seg_7090" s="T92">wood.[NOM]</ta>
            <ta e="T94" id="Seg_7091" s="T93">EMPH</ta>
            <ta e="T95" id="Seg_7092" s="T94">though</ta>
            <ta e="T96" id="Seg_7093" s="T95">completely</ta>
            <ta e="T97" id="Seg_7094" s="T96">reindeer.[NOM]</ta>
            <ta e="T98" id="Seg_7095" s="T97">where</ta>
            <ta e="T99" id="Seg_7096" s="T98">NEG</ta>
            <ta e="T100" id="Seg_7097" s="T99">to.be.on.view-EP-NEG.[3SG]</ta>
            <ta e="T101" id="Seg_7098" s="T100">such-PL-DAT/LOC</ta>
            <ta e="T102" id="Seg_7099" s="T101">work-CVB.SEQ</ta>
            <ta e="T103" id="Seg_7100" s="T102">misery-VBZ-PST2-EP-1SG</ta>
            <ta e="T104" id="Seg_7101" s="T103">1SG.[NOM]</ta>
            <ta e="T105" id="Seg_7102" s="T104">most</ta>
            <ta e="T106" id="Seg_7103" s="T105">that.[NOM]</ta>
            <ta e="T107" id="Seg_7104" s="T106">make-CVB.PURP</ta>
            <ta e="T108" id="Seg_7105" s="T107">happy.[NOM]</ta>
            <ta e="T109" id="Seg_7106" s="T108">be-CVB.SEQ-1SG</ta>
            <ta e="T110" id="Seg_7107" s="T109">reindeer-1SG.[NOM]</ta>
            <ta e="T111" id="Seg_7108" s="T110">bring.up-EP-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T112" id="Seg_7109" s="T111">very</ta>
            <ta e="T113" id="Seg_7110" s="T112">ten-ABL</ta>
            <ta e="T114" id="Seg_7111" s="T113">over</ta>
            <ta e="T115" id="Seg_7112" s="T114">thousand-ACC</ta>
            <ta e="T116" id="Seg_7113" s="T115">bring.up-PST2-EP-1SG</ta>
            <ta e="T117" id="Seg_7114" s="T116">how.much</ta>
            <ta e="T118" id="Seg_7115" s="T117">INDEF</ta>
            <ta e="T119" id="Seg_7116" s="T118">herd-DAT/LOC</ta>
            <ta e="T120" id="Seg_7117" s="T119">give-PST2-EP-1SG</ta>
            <ta e="T121" id="Seg_7118" s="T120">herd-DAT/LOC</ta>
            <ta e="T122" id="Seg_7119" s="T121">give-PST2-EP-1SG</ta>
            <ta e="T130" id="Seg_7120" s="T129">brigade.[NOM]</ta>
            <ta e="T131" id="Seg_7121" s="T130">always</ta>
            <ta e="T132" id="Seg_7122" s="T131">now-DAT/LOC</ta>
            <ta e="T133" id="Seg_7123" s="T132">until</ta>
            <ta e="T134" id="Seg_7124" s="T133">this</ta>
            <ta e="T135" id="Seg_7125" s="T134">now</ta>
            <ta e="T136" id="Seg_7126" s="T135">son-EP-1SG.[NOM]</ta>
            <ta e="T137" id="Seg_7127" s="T136">that.[NOM]</ta>
            <ta e="T138" id="Seg_7128" s="T137">deal.with-HAB.[3SG]</ta>
            <ta e="T139" id="Seg_7129" s="T138">Volodya-1SG.[NOM]</ta>
            <ta e="T140" id="Seg_7130" s="T139">lonely</ta>
            <ta e="T141" id="Seg_7131" s="T140">boy-PROPR-1SG</ta>
            <ta e="T148" id="Seg_7132" s="T147">mm</ta>
            <ta e="T149" id="Seg_7133" s="T148">age-PRS-1PL</ta>
            <ta e="T150" id="Seg_7134" s="T149">two-COLL-1PL.[NOM]</ta>
            <ta e="T151" id="Seg_7135" s="T150">age-PST1-1PL</ta>
            <ta e="T167" id="Seg_7136" s="T166">brigade.leader-PL-1PL.[NOM]</ta>
            <ta e="T168" id="Seg_7137" s="T167">a.little</ta>
            <ta e="T169" id="Seg_7138" s="T168">human.being-PL.[NOM]</ta>
            <ta e="T170" id="Seg_7139" s="T169">be-PST1-3PL</ta>
            <ta e="T171" id="Seg_7140" s="T170">then</ta>
            <ta e="T174" id="Seg_7141" s="T173">learn-CVB.SIM</ta>
            <ta e="T175" id="Seg_7142" s="T174">learn-PTCP.PST-EP-INSTR</ta>
            <ta e="T176" id="Seg_7143" s="T175">EMPH-then</ta>
            <ta e="T177" id="Seg_7144" s="T176">learn-CVB.SEQ</ta>
            <ta e="T178" id="Seg_7145" s="T177">stay-INFER-1PL</ta>
            <ta e="T179" id="Seg_7146" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_7147" s="T179">1PL.[NOM]</ta>
            <ta e="T181" id="Seg_7148" s="T180">that</ta>
            <ta e="T182" id="Seg_7149" s="T181">fear-NMNZ-DAT/LOC</ta>
            <ta e="T183" id="Seg_7150" s="T182">reindeer-DAT/LOC</ta>
            <ta e="T184" id="Seg_7151" s="T183">reindeer-ACC</ta>
            <ta e="T185" id="Seg_7152" s="T184">lose-EP-NEG-IMP.2PL</ta>
            <ta e="T186" id="Seg_7153" s="T185">think-CVB.SEQ</ta>
            <ta e="T187" id="Seg_7154" s="T186">say-HAB-1SG</ta>
            <ta e="T188" id="Seg_7155" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_7156" s="T188">reindeer-ACC</ta>
            <ta e="T190" id="Seg_7157" s="T189">lose-EP-NEG-IMP.2PL</ta>
            <ta e="T191" id="Seg_7158" s="T190">next.morning-2PL-ACC</ta>
            <ta e="T192" id="Seg_7159" s="T191">remember-IMP.2PL</ta>
            <ta e="T193" id="Seg_7160" s="T192">say-HAB-1SG</ta>
            <ta e="T194" id="Seg_7161" s="T193">in.the.morning</ta>
            <ta e="T195" id="Seg_7162" s="T194">plan-VBZ-CVB.SIM</ta>
            <ta e="T196" id="Seg_7163" s="T195">stand-EP-IMP.2PL</ta>
            <ta e="T197" id="Seg_7164" s="T196">say-HAB-1SG</ta>
            <ta e="T198" id="Seg_7165" s="T197">whereto</ta>
            <ta e="T199" id="Seg_7166" s="T198">work-PTCP.PRS-2PL-ACC</ta>
            <ta e="T200" id="Seg_7167" s="T199">whereto</ta>
            <ta e="T201" id="Seg_7168" s="T200">go-PTCP.PRS-2PL-ACC</ta>
            <ta e="T202" id="Seg_7169" s="T201">reindeer-2SG.[NOM]</ta>
            <ta e="T203" id="Seg_7170" s="T202">lose-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T204" id="Seg_7171" s="T203">many-3SG</ta>
            <ta e="T205" id="Seg_7172" s="T204">misery-3SG.[NOM]</ta>
            <ta e="T206" id="Seg_7173" s="T205">mind.[NOM]</ta>
            <ta e="T207" id="Seg_7174" s="T206">who-VBZ-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_7175" s="T207">in.summer</ta>
            <ta e="T209" id="Seg_7176" s="T208">very</ta>
            <ta e="T210" id="Seg_7177" s="T209">way-VBZ-MED-CVB.SIM</ta>
            <ta e="T211" id="Seg_7178" s="T210">be-HAB.[3SG]</ta>
            <ta e="T212" id="Seg_7179" s="T211">that</ta>
            <ta e="T213" id="Seg_7180" s="T212">so.much</ta>
            <ta e="T214" id="Seg_7181" s="T213">go-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_7182" s="T214">until</ta>
            <ta e="T217" id="Seg_7183" s="T215">EMPH</ta>
            <ta e="T218" id="Seg_7184" s="T217">MOD</ta>
            <ta e="T219" id="Seg_7185" s="T218">always</ta>
            <ta e="T220" id="Seg_7186" s="T219">good</ta>
            <ta e="T221" id="Seg_7187" s="T220">name-EP-INSTR</ta>
            <ta e="T222" id="Seg_7188" s="T221">go.out-PST1-1SG</ta>
            <ta e="T223" id="Seg_7189" s="T222">EVID</ta>
            <ta e="T224" id="Seg_7190" s="T223">EMPH</ta>
            <ta e="T225" id="Seg_7191" s="T224">in.summer</ta>
            <ta e="T226" id="Seg_7192" s="T225">and</ta>
            <ta e="T227" id="Seg_7193" s="T226">in.winter</ta>
            <ta e="T228" id="Seg_7194" s="T227">and</ta>
            <ta e="T229" id="Seg_7195" s="T228">however</ta>
            <ta e="T230" id="Seg_7196" s="T229">work-PTCP.COND-DAT/LOC</ta>
            <ta e="T231" id="Seg_7197" s="T230">many</ta>
            <ta e="T232" id="Seg_7198" s="T231">year-ACC</ta>
            <ta e="T233" id="Seg_7199" s="T232">work-PST1-1SG</ta>
            <ta e="T234" id="Seg_7200" s="T233">five-ten</ta>
            <ta e="T235" id="Seg_7201" s="T234">five</ta>
            <ta e="T236" id="Seg_7202" s="T235">year-ACC</ta>
            <ta e="T237" id="Seg_7203" s="T236">retirement-DAT/LOC</ta>
            <ta e="T238" id="Seg_7204" s="T237">go.in-CVB.SEQ</ta>
            <ta e="T239" id="Seg_7205" s="T238">after</ta>
            <ta e="T240" id="Seg_7206" s="T239">still</ta>
            <ta e="T241" id="Seg_7207" s="T240">work-PST2-EP-1SG</ta>
            <ta e="T242" id="Seg_7208" s="T241">reindeer-1SG.[NOM]</ta>
            <ta e="T243" id="Seg_7209" s="T242">bring.up-EP-PASS/REFL-PTCP.PRS-3SG-ABL</ta>
            <ta e="T257" id="Seg_7210" s="T256">get.lost-PST2-3SG</ta>
            <ta e="T258" id="Seg_7211" s="T257">EMPH</ta>
            <ta e="T259" id="Seg_7212" s="T258">die-CVB.SEQ</ta>
            <ta e="T260" id="Seg_7213" s="T259">get.lost-CVB.SEQ</ta>
            <ta e="T261" id="Seg_7214" s="T260">get.lost-PST2-3SG</ta>
            <ta e="T262" id="Seg_7215" s="T261">that</ta>
            <ta e="T263" id="Seg_7216" s="T262">who-PL-DAT/LOC</ta>
            <ta e="T264" id="Seg_7217" s="T263">Saskylaax-PL-DAT/LOC</ta>
            <ta e="T265" id="Seg_7218" s="T264">go-CVB.SEQ</ta>
            <ta e="T266" id="Seg_7219" s="T265">go-CVB.SEQ</ta>
            <ta e="T267" id="Seg_7220" s="T266">maybe</ta>
            <ta e="T268" id="Seg_7221" s="T267">wild.reindeer.[NOM]</ta>
            <ta e="T269" id="Seg_7222" s="T268">and</ta>
            <ta e="T270" id="Seg_7223" s="T269">take.away-PRS.[3SG]</ta>
            <ta e="T271" id="Seg_7224" s="T270">wild.reindeer.[NOM]</ta>
            <ta e="T272" id="Seg_7225" s="T271">take.away-HAB.[3SG]</ta>
            <ta e="T273" id="Seg_7226" s="T272">migration-3SG-DAT/LOC</ta>
            <ta e="T274" id="Seg_7227" s="T273">in.autumn</ta>
            <ta e="T275" id="Seg_7228" s="T274">come-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T276" id="Seg_7229" s="T275">well</ta>
            <ta e="T277" id="Seg_7230" s="T276">wolf.[NOM]</ta>
            <ta e="T278" id="Seg_7231" s="T277">eat-PRS.[3SG]</ta>
            <ta e="T279" id="Seg_7232" s="T278">big</ta>
            <ta e="T280" id="Seg_7233" s="T279">very</ta>
            <ta e="T281" id="Seg_7234" s="T280">be.sick-PRS.[3SG]</ta>
            <ta e="T282" id="Seg_7235" s="T281">that-DAT/LOC</ta>
            <ta e="T283" id="Seg_7236" s="T282">foot-3SG.[NOM]</ta>
            <ta e="T284" id="Seg_7237" s="T283">and</ta>
            <ta e="T285" id="Seg_7238" s="T284">immediately</ta>
            <ta e="T286" id="Seg_7239" s="T285">long</ta>
            <ta e="T287" id="Seg_7240" s="T286">go-PTCP.HAB-3SG</ta>
            <ta e="T288" id="Seg_7241" s="T287">NEG.[3SG]</ta>
            <ta e="T302" id="Seg_7242" s="T301">kill-CAUS-PRS-3PL</ta>
            <ta e="T303" id="Seg_7243" s="T302">recently</ta>
            <ta e="T304" id="Seg_7244" s="T303">EMPH</ta>
            <ta e="T305" id="Seg_7245" s="T304">there.is</ta>
            <ta e="T306" id="Seg_7246" s="T305">be-TEMP-3SG</ta>
            <ta e="T307" id="Seg_7247" s="T306">say-PRS-3PL</ta>
            <ta e="T308" id="Seg_7248" s="T307">EMPH</ta>
            <ta e="T309" id="Seg_7249" s="T308">now</ta>
            <ta e="T310" id="Seg_7250" s="T309">premium-VBZ-PTCP.PRS</ta>
            <ta e="T311" id="Seg_7251" s="T310">become-PST2-3PL</ta>
            <ta e="T312" id="Seg_7252" s="T311">before</ta>
            <ta e="T313" id="Seg_7253" s="T312">premium-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T314" id="Seg_7254" s="T313">NEG-3PL</ta>
            <ta e="T316" id="Seg_7255" s="T315">be-PST1-3PL</ta>
            <ta e="T320" id="Seg_7256" s="T319">our-1PL.[NOM]</ta>
            <ta e="T321" id="Seg_7257" s="T320">who-1PL.[NOM]</ta>
            <ta e="T322" id="Seg_7258" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_7259" s="T322">rate-1PL.[NOM]</ta>
            <ta e="T324" id="Seg_7260" s="T323">bad.[NOM]</ta>
            <ta e="T325" id="Seg_7261" s="T324">be-PST1-3SG</ta>
            <ta e="T326" id="Seg_7262" s="T325">before</ta>
            <ta e="T327" id="Seg_7263" s="T326">small.[NOM]</ta>
            <ta e="T328" id="Seg_7264" s="T327">small.[NOM]</ta>
            <ta e="T329" id="Seg_7265" s="T328">be-PST1-3SG</ta>
            <ta e="T330" id="Seg_7266" s="T329">reindeer-1PL.[NOM]</ta>
            <ta e="T331" id="Seg_7267" s="T330">own-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_7268" s="T331">well</ta>
            <ta e="T333" id="Seg_7269" s="T332">that-ACC</ta>
            <ta e="T334" id="Seg_7270" s="T333">1SG.[NOM]</ta>
            <ta e="T335" id="Seg_7271" s="T334">get.higher-EP-CAUS-EP-IMP.2PL</ta>
            <ta e="T336" id="Seg_7272" s="T335">say-CVB.SIM</ta>
            <ta e="T337" id="Seg_7273" s="T336">do.in.vain-CVB.SEQ</ta>
            <ta e="T338" id="Seg_7274" s="T337">now</ta>
            <ta e="T339" id="Seg_7275" s="T338">word.[NOM]</ta>
            <ta e="T340" id="Seg_7276" s="T339">hear-FUT-3PL</ta>
            <ta e="T341" id="Seg_7277" s="T340">EMPH</ta>
            <ta e="T342" id="Seg_7278" s="T341">now</ta>
            <ta e="T343" id="Seg_7279" s="T342">worker-PL-1PL.[NOM]</ta>
            <ta e="T344" id="Seg_7280" s="T343">bad-PL.[NOM]</ta>
            <ta e="T345" id="Seg_7281" s="T344">that-DAT/LOC</ta>
            <ta e="T346" id="Seg_7282" s="T345">well</ta>
            <ta e="T353" id="Seg_7283" s="T352">mm</ta>
            <ta e="T354" id="Seg_7284" s="T353">say-PRS-1PL</ta>
            <ta e="T355" id="Seg_7285" s="T354">EMPH</ta>
            <ta e="T356" id="Seg_7286" s="T355">voice.[NOM]</ta>
            <ta e="T357" id="Seg_7287" s="T356">feed-PTCP.FUT-3PL-ACC</ta>
            <ta e="T358" id="Seg_7288" s="T357">reindeer.[NOM]</ta>
            <ta e="T359" id="Seg_7289" s="T358">rate-3SG-ACC</ta>
            <ta e="T360" id="Seg_7290" s="T359">get.higher-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T361" id="Seg_7291" s="T360">say-PRS-1PL</ta>
            <ta e="T364" id="Seg_7292" s="T363">their</ta>
            <ta e="T365" id="Seg_7293" s="T364">who-3PL.[NOM]</ta>
            <ta e="T366" id="Seg_7294" s="T365">Urung.Xaya-PL.[NOM]</ta>
            <ta e="T367" id="Seg_7295" s="T366">own-3PL.[NOM]</ta>
            <ta e="T368" id="Seg_7296" s="T367">EMPH</ta>
            <ta e="T369" id="Seg_7297" s="T368">one</ta>
            <ta e="T370" id="Seg_7298" s="T369">reindeer-DAT/LOC</ta>
            <ta e="T371" id="Seg_7299" s="T370">how.much</ta>
            <ta e="T372" id="Seg_7300" s="T371">say-PST2-3PL=Q</ta>
            <ta e="T373" id="Seg_7301" s="T372">day.and.night-DAT/LOC</ta>
            <ta e="T374" id="Seg_7302" s="T373">four</ta>
            <ta e="T375" id="Seg_7303" s="T374">four</ta>
            <ta e="T376" id="Seg_7304" s="T375">ruble.[NOM]</ta>
            <ta e="T381" id="Seg_7305" s="T380">bring.up-PTCP.PRS-3PL-ACC</ta>
            <ta e="T382" id="Seg_7306" s="T381">because.of</ta>
            <ta e="T383" id="Seg_7307" s="T382">EMPH</ta>
            <ta e="T384" id="Seg_7308" s="T383">ha-ha</ta>
            <ta e="T387" id="Seg_7309" s="T386">reindeer-VBZ-CVB.SEQ</ta>
            <ta e="T388" id="Seg_7310" s="T387">bring.up-CVB.SEQ</ta>
            <ta e="T389" id="Seg_7311" s="T388">go.out-PTCP.PRS-3PL-ACC</ta>
            <ta e="T390" id="Seg_7312" s="T389">because.of</ta>
            <ta e="T391" id="Seg_7313" s="T390">their.[NOM]</ta>
            <ta e="T393" id="Seg_7314" s="T391">also</ta>
            <ta e="T394" id="Seg_7315" s="T393">snowstorm.[NOM]</ta>
            <ta e="T395" id="Seg_7316" s="T394">poor.[NOM]</ta>
            <ta e="T396" id="Seg_7317" s="T395">make-PRS.[3SG]</ta>
            <ta e="T397" id="Seg_7318" s="T396">EMPH</ta>
            <ta e="T398" id="Seg_7319" s="T397">Urung.Xaya-2SG.[NOM]</ta>
            <ta e="T399" id="Seg_7320" s="T398">place-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_7321" s="T399">very</ta>
            <ta e="T401" id="Seg_7322" s="T400">poor</ta>
            <ta e="T402" id="Seg_7323" s="T401">earth.[NOM]</ta>
            <ta e="T403" id="Seg_7324" s="T402">that.[NOM]</ta>
            <ta e="T404" id="Seg_7325" s="T403">because.of</ta>
            <ta e="T405" id="Seg_7326" s="T404">that</ta>
            <ta e="T406" id="Seg_7327" s="T405">rate-PL-EP-2SG.[NOM]</ta>
            <ta e="T407" id="Seg_7328" s="T406">get.higher-EP-CAUS-PST2-3PL</ta>
            <ta e="T408" id="Seg_7329" s="T407">at.first-ADJZ-ABL</ta>
            <ta e="T409" id="Seg_7330" s="T408">cloud.of.insects-ABL</ta>
            <ta e="T410" id="Seg_7331" s="T409">former-ABL</ta>
            <ta e="T411" id="Seg_7332" s="T410">reindeer.[NOM]</ta>
            <ta e="T412" id="Seg_7333" s="T411">bring.up-NMNZ.[NOM]</ta>
            <ta e="T413" id="Seg_7334" s="T412">very</ta>
            <ta e="T414" id="Seg_7335" s="T413">EMPH</ta>
            <ta e="T415" id="Seg_7336" s="T414">difficult-3SG</ta>
            <ta e="T418" id="Seg_7337" s="T417">AFFIRM</ta>
            <ta e="T419" id="Seg_7338" s="T418">die-PTCP.PRS</ta>
            <ta e="T420" id="Seg_7339" s="T419">die-PTCP.PRS</ta>
            <ta e="T421" id="Seg_7340" s="T420">human.being-2SG.[NOM]</ta>
            <ta e="T422" id="Seg_7341" s="T421">die-PRS.[3SG]</ta>
            <ta e="T423" id="Seg_7342" s="T422">that</ta>
            <ta e="T425" id="Seg_7343" s="T424">reindeer-ACC</ta>
            <ta e="T426" id="Seg_7344" s="T425">with</ta>
            <ta e="T427" id="Seg_7345" s="T426">come.along-CVB.SEQ</ta>
            <ta e="T442" id="Seg_7346" s="T441">that</ta>
            <ta e="T443" id="Seg_7347" s="T442">Yakut</ta>
            <ta e="T444" id="Seg_7348" s="T443">place-3SG-DAT/LOC</ta>
            <ta e="T445" id="Seg_7349" s="T444">such-PL.[NOM]</ta>
            <ta e="T446" id="Seg_7350" s="T445">be-PST1-3SG</ta>
            <ta e="T447" id="Seg_7351" s="T446">reindeer-ACC</ta>
            <ta e="T448" id="Seg_7352" s="T447">very</ta>
            <ta e="T449" id="Seg_7353" s="T448">esteem-PRS-3PL</ta>
            <ta e="T450" id="Seg_7354" s="T449">feed-PTCP.FUT-3PL-ACC</ta>
            <ta e="T451" id="Seg_7355" s="T450">want-PRS-3PL</ta>
            <ta e="T452" id="Seg_7356" s="T451">oh.dear</ta>
            <ta e="T453" id="Seg_7357" s="T452">money.[NOM]</ta>
            <ta e="T454" id="Seg_7358" s="T453">take-PRS-3PL</ta>
            <ta e="T455" id="Seg_7359" s="T454">good.[NOM]</ta>
            <ta e="T456" id="Seg_7360" s="T455">be-PST1-3SG</ta>
            <ta e="T457" id="Seg_7361" s="T456">reindeer-AG-PL-EP-2SG.[NOM]</ta>
            <ta e="T458" id="Seg_7362" s="T457">month-DAT/LOC</ta>
            <ta e="T459" id="Seg_7363" s="T458">four-DISTR</ta>
            <ta e="T460" id="Seg_7364" s="T459">five-DISTR</ta>
            <ta e="T461" id="Seg_7365" s="T460">thousand-ACC</ta>
            <ta e="T462" id="Seg_7366" s="T461">one</ta>
            <ta e="T463" id="Seg_7367" s="T462">month-DAT/LOC</ta>
            <ta e="T464" id="Seg_7368" s="T463">take-PRS-3PL</ta>
            <ta e="T465" id="Seg_7369" s="T464">3PL.[NOM]</ta>
            <ta e="T466" id="Seg_7370" s="T465">1PL.[NOM]</ta>
            <ta e="T467" id="Seg_7371" s="T466">that</ta>
            <ta e="T468" id="Seg_7372" s="T467">when</ta>
            <ta e="T471" id="Seg_7373" s="T470">such-POSS</ta>
            <ta e="T472" id="Seg_7374" s="T471">EMPH</ta>
            <ta e="T474" id="Seg_7375" s="T472">NEG-1PL</ta>
            <ta e="T475" id="Seg_7376" s="T474">at.all</ta>
            <ta e="T477" id="Seg_7377" s="T475">NEG.EX</ta>
            <ta e="T478" id="Seg_7378" s="T477">simply</ta>
            <ta e="T479" id="Seg_7379" s="T478">five</ta>
            <ta e="T480" id="Seg_7380" s="T479">hundred</ta>
            <ta e="T481" id="Seg_7381" s="T480">probably</ta>
            <ta e="T482" id="Seg_7382" s="T481">Q</ta>
            <ta e="T483" id="Seg_7383" s="T482">advance.[NOM]</ta>
            <ta e="T484" id="Seg_7384" s="T483">similar</ta>
            <ta e="T486" id="Seg_7385" s="T484">rate-3SG-1PL.[NOM]</ta>
            <ta e="T487" id="Seg_7386" s="T486">little-3SG.[NOM]</ta>
            <ta e="T488" id="Seg_7387" s="T487">very</ta>
            <ta e="T489" id="Seg_7388" s="T488">copecks.[NOM]</ta>
            <ta e="T490" id="Seg_7389" s="T489">now-DAT/LOC</ta>
            <ta e="T491" id="Seg_7390" s="T490">until</ta>
            <ta e="T492" id="Seg_7391" s="T491">end-EP-ABL</ta>
            <ta e="T493" id="Seg_7392" s="T492">lose-PST1-3PL</ta>
            <ta e="T494" id="Seg_7393" s="T493">now</ta>
            <ta e="T495" id="Seg_7394" s="T494">what-3SG-ACC</ta>
            <ta e="T496" id="Seg_7395" s="T495">EMPH</ta>
            <ta e="T497" id="Seg_7396" s="T496">speak-CVB.SIM</ta>
            <ta e="T498" id="Seg_7397" s="T497">do.in.vain-CVB.SEQ</ta>
            <ta e="T499" id="Seg_7398" s="T498">why</ta>
            <ta e="T500" id="Seg_7399" s="T499">touch-PRS-2PL</ta>
            <ta e="T501" id="Seg_7400" s="T500">that.[NOM]</ta>
            <ta e="T502" id="Seg_7401" s="T501">what.[NOM]</ta>
            <ta e="T503" id="Seg_7402" s="T502">make-CVB.PURP</ta>
            <ta e="T504" id="Seg_7403" s="T503">sovkhoz-2PL.[NOM]</ta>
            <ta e="T505" id="Seg_7404" s="T504">sovkhoz-2PL.[NOM]</ta>
            <ta e="T506" id="Seg_7405" s="T505">reindeer-3SG.[NOM]</ta>
            <ta e="T507" id="Seg_7406" s="T506">self-2PL.[NOM]</ta>
            <ta e="T508" id="Seg_7407" s="T507">eat-PRS-2PL</ta>
            <ta e="T509" id="Seg_7408" s="T508">EMPH</ta>
            <ta e="T510" id="Seg_7409" s="T509">say-HAB-1SG</ta>
            <ta e="T511" id="Seg_7410" s="T510">dress-PRS-2PL</ta>
            <ta e="T512" id="Seg_7411" s="T511">and.so.on-PRS-2PL</ta>
            <ta e="T513" id="Seg_7412" s="T512">why</ta>
            <ta e="T514" id="Seg_7413" s="T513">sovkhoz.[NOM]</ta>
            <ta e="T515" id="Seg_7414" s="T514">own-3SG.[NOM]</ta>
            <ta e="T516" id="Seg_7415" s="T515">say-PRS-2PL</ta>
            <ta e="T517" id="Seg_7416" s="T516">say-PRS-1SG</ta>
            <ta e="T518" id="Seg_7417" s="T517">self-2PL.[NOM]</ta>
            <ta e="T519" id="Seg_7418" s="T518">wealth-2PL.[NOM]</ta>
            <ta e="T520" id="Seg_7419" s="T519">EMPH</ta>
            <ta e="T521" id="Seg_7420" s="T520">say-PRS-1SG</ta>
            <ta e="T522" id="Seg_7421" s="T521">that.[NOM]</ta>
            <ta e="T523" id="Seg_7422" s="T522">because.of</ta>
            <ta e="T524" id="Seg_7423" s="T523">money.[NOM]</ta>
            <ta e="T525" id="Seg_7424" s="T524">get-PRS-2PL</ta>
            <ta e="T526" id="Seg_7425" s="T525">what-ACC=Q</ta>
            <ta e="T527" id="Seg_7426" s="T526">what-EP-INSTR</ta>
            <ta e="T530" id="Seg_7427" s="T529">take-FUT-1PL</ta>
            <ta e="T531" id="Seg_7428" s="T530">think-PRS-2PL</ta>
            <ta e="T532" id="Seg_7429" s="T531">say-PRS-1SG</ta>
            <ta e="T533" id="Seg_7430" s="T532">sin.[NOM]</ta>
            <ta e="T534" id="Seg_7431" s="T533">be-FUT.[3SG]</ta>
            <ta e="T535" id="Seg_7432" s="T534">big-ADVZ</ta>
            <ta e="T536" id="Seg_7433" s="T535">deception-VBZ-PST2-EP-1SG</ta>
            <ta e="T537" id="Seg_7434" s="T536">say-TEMP-1SG</ta>
            <ta e="T538" id="Seg_7435" s="T537">sell-EP-MED-PST2.NEG-EP-1SG</ta>
            <ta e="T539" id="Seg_7436" s="T538">and</ta>
            <ta e="T540" id="Seg_7437" s="T539">though</ta>
            <ta e="T541" id="Seg_7438" s="T540">completely</ta>
            <ta e="T542" id="Seg_7439" s="T541">human.being-2SG.[NOM]</ta>
            <ta e="T543" id="Seg_7440" s="T542">every-3SG.[NOM]</ta>
            <ta e="T544" id="Seg_7441" s="T543">sell-EP-MED-PST2-3SG</ta>
            <ta e="T545" id="Seg_7442" s="T544">that</ta>
            <ta e="T546" id="Seg_7443" s="T545">sovkhoz-ABL</ta>
            <ta e="T550" id="Seg_7444" s="T549">money.[NOM]</ta>
            <ta e="T551" id="Seg_7445" s="T550">make-PRS-3PL</ta>
            <ta e="T572" id="Seg_7446" s="T571">such.[NOM]</ta>
            <ta e="T573" id="Seg_7447" s="T572">such.[NOM]</ta>
            <ta e="T581" id="Seg_7448" s="T580">reindeer-AG.[NOM]</ta>
            <ta e="T582" id="Seg_7449" s="T581">human.being.[NOM]</ta>
            <ta e="T583" id="Seg_7450" s="T582">not.enough.[NOM]</ta>
            <ta e="T584" id="Seg_7451" s="T583">well</ta>
            <ta e="T585" id="Seg_7452" s="T584">that-ACC</ta>
            <ta e="T613" id="Seg_7453" s="T612">that</ta>
            <ta e="T614" id="Seg_7454" s="T613">good-ADVZ</ta>
            <ta e="T615" id="Seg_7455" s="T614">work-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T616" id="Seg_7456" s="T615">because.of</ta>
            <ta e="T617" id="Seg_7457" s="T616">go.out-EP-PST2-EP-1SG</ta>
            <ta e="T618" id="Seg_7458" s="T617">human.being-PL-ACC</ta>
            <ta e="T619" id="Seg_7459" s="T618">with</ta>
            <ta e="T620" id="Seg_7460" s="T619">chat-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T621" id="Seg_7461" s="T620">because.of</ta>
            <ta e="T622" id="Seg_7462" s="T621">better</ta>
            <ta e="T623" id="Seg_7463" s="T622">young.[NOM]</ta>
            <ta e="T624" id="Seg_7464" s="T623">be-TEMP-1SG</ta>
            <ta e="T625" id="Seg_7465" s="T624">very</ta>
            <ta e="T626" id="Seg_7466" s="T625">agile.[NOM]</ta>
            <ta e="T627" id="Seg_7467" s="T626">be-PST1-1SG</ta>
            <ta e="T628" id="Seg_7468" s="T627">EMPH</ta>
            <ta e="T629" id="Seg_7469" s="T628">be.sick-EP-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T630" id="Seg_7470" s="T629">and</ta>
            <ta e="T631" id="Seg_7471" s="T630">because.of</ta>
            <ta e="T632" id="Seg_7472" s="T631">chat-NMNZ.[NOM]</ta>
            <ta e="T633" id="Seg_7473" s="T632">leftover-PROPR.[NOM]</ta>
            <ta e="T634" id="Seg_7474" s="T633">be-PST1-1SG</ta>
            <ta e="T635" id="Seg_7475" s="T634">Russian.[NOM]</ta>
            <ta e="T636" id="Seg_7476" s="T635">and</ta>
            <ta e="T637" id="Seg_7477" s="T636">maybe</ta>
            <ta e="T638" id="Seg_7478" s="T637">Dolgan.[NOM]</ta>
            <ta e="T639" id="Seg_7479" s="T638">and</ta>
            <ta e="T640" id="Seg_7480" s="T639">maybe</ta>
            <ta e="T641" id="Seg_7481" s="T640">foreign.[NOM]</ta>
            <ta e="T642" id="Seg_7482" s="T641">and</ta>
            <ta e="T643" id="Seg_7483" s="T642">maybe</ta>
            <ta e="T653" id="Seg_7484" s="T652">day.[NOM]</ta>
            <ta e="T654" id="Seg_7485" s="T653">every</ta>
            <ta e="T655" id="Seg_7486" s="T654">EMPH</ta>
            <ta e="T656" id="Seg_7487" s="T655">see-PRS-3PL</ta>
            <ta e="T657" id="Seg_7488" s="T656">EMPH</ta>
            <ta e="T658" id="Seg_7489" s="T657">mistake-ACC</ta>
            <ta e="T659" id="Seg_7490" s="T658">what-3PL.[NOM]</ta>
            <ta e="T661" id="Seg_7491" s="T660">make-PRS-2SG</ta>
            <ta e="T662" id="Seg_7492" s="T661">very</ta>
            <ta e="T663" id="Seg_7493" s="T662">mistake-POSS</ta>
            <ta e="T664" id="Seg_7494" s="T663">NEG-1SG</ta>
            <ta e="T665" id="Seg_7495" s="T664">EMPH</ta>
            <ta e="T666" id="Seg_7496" s="T665">that.[NOM]</ta>
            <ta e="T667" id="Seg_7497" s="T666">because.of</ta>
            <ta e="T669" id="Seg_7498" s="T667">Russian-PL.[NOM]</ta>
            <ta e="T694" id="Seg_7499" s="T693">give.birth-PTCP.PST-3SG-ACC</ta>
            <ta e="T695" id="Seg_7500" s="T694">tell-PRS-2SG</ta>
            <ta e="T696" id="Seg_7501" s="T695">EMPH</ta>
            <ta e="T697" id="Seg_7502" s="T696">surety-3SG-ACC</ta>
            <ta e="T698" id="Seg_7503" s="T697">calf-EP-2SG.[NOM]</ta>
            <ta e="T699" id="Seg_7504" s="T698">tiny.[NOM]</ta>
            <ta e="T700" id="Seg_7505" s="T699">EMPH</ta>
            <ta e="T701" id="Seg_7506" s="T700">be-PST2.[3SG]</ta>
            <ta e="T702" id="Seg_7507" s="T701">night-ACC</ta>
            <ta e="T703" id="Seg_7508" s="T702">during</ta>
            <ta e="T704" id="Seg_7509" s="T703">sleep-NEG.CVB.SIM</ta>
            <ta e="T705" id="Seg_7510" s="T704">go-PRS-2SG</ta>
            <ta e="T706" id="Seg_7511" s="T705">EMPH</ta>
            <ta e="T707" id="Seg_7512" s="T706">that</ta>
            <ta e="T708" id="Seg_7513" s="T707">reindeer.calf.[NOM]</ta>
            <ta e="T709" id="Seg_7514" s="T708">give.birth-PTCP.PRS-3SG-ACC</ta>
            <ta e="T710" id="Seg_7515" s="T709">guard-CVB.SIM</ta>
            <ta e="T711" id="Seg_7516" s="T710">many</ta>
            <ta e="T712" id="Seg_7517" s="T711">reindeer-DAT/LOC</ta>
            <ta e="T715" id="Seg_7518" s="T714">many</ta>
            <ta e="T716" id="Seg_7519" s="T715">reindeer.calf.[NOM]</ta>
            <ta e="T717" id="Seg_7520" s="T716">EMPH</ta>
         </annotation>
         <annotation name="gg" tierref="gg-ChSA">
            <ta e="T7" id="Seg_7521" s="T6">Vater-PL-1PL.[NOM]</ta>
            <ta e="T8" id="Seg_7522" s="T7">Mutter-PL-1PL.[NOM]</ta>
            <ta e="T9" id="Seg_7523" s="T8">doch</ta>
            <ta e="T10" id="Seg_7524" s="T9">EMPH</ta>
            <ta e="T11" id="Seg_7525" s="T10">alt-PL.[NOM]</ta>
            <ta e="T12" id="Seg_7526" s="T11">sein-PST1-3SG</ta>
            <ta e="T13" id="Seg_7527" s="T12">EMPH</ta>
            <ta e="T14" id="Seg_7528" s="T13">U-ACC</ta>
            <ta e="T15" id="Seg_7529" s="T14">A-ACC</ta>
            <ta e="T16" id="Seg_7530" s="T15">wissen-NEG-3PL</ta>
            <ta e="T19" id="Seg_7531" s="T18">einfach</ta>
            <ta e="T22" id="Seg_7532" s="T21">Hand.[NOM]</ta>
            <ta e="T23" id="Seg_7533" s="T22">lasten-PRS-3PL</ta>
            <ta e="T24" id="Seg_7534" s="T23">solch</ta>
            <ta e="T25" id="Seg_7535" s="T24">machen-HAB-1SG</ta>
            <ta e="T26" id="Seg_7536" s="T25">1SG.[NOM]</ta>
            <ta e="T27" id="Seg_7537" s="T26">auch</ta>
            <ta e="T28" id="Seg_7538" s="T27">EMPH-jetzt</ta>
            <ta e="T29" id="Seg_7539" s="T28">doch</ta>
            <ta e="T30" id="Seg_7540" s="T29">solch.[NOM]</ta>
            <ta e="T31" id="Seg_7541" s="T30">wie</ta>
            <ta e="T32" id="Seg_7542" s="T31">doch</ta>
            <ta e="T33" id="Seg_7543" s="T32">Verstand-EP-1SG.[NOM]</ta>
            <ta e="T34" id="Seg_7544" s="T33">fliegen-PST2-3SG</ta>
            <ta e="T35" id="Seg_7545" s="T34">sehr</ta>
            <ta e="T36" id="Seg_7546" s="T35">Jahr-ACC</ta>
            <ta e="T37" id="Seg_7547" s="T36">während</ta>
            <ta e="T38" id="Seg_7548" s="T37">krank.sein-EP-PST2-EP-1SG</ta>
            <ta e="T39" id="Seg_7549" s="T38">dafür</ta>
            <ta e="T40" id="Seg_7550" s="T39">jenes.[NOM]</ta>
            <ta e="T41" id="Seg_7551" s="T40">trotz</ta>
            <ta e="T42" id="Seg_7552" s="T41">normal-1SG</ta>
            <ta e="T43" id="Seg_7553" s="T42">ein.Bisschen</ta>
            <ta e="T44" id="Seg_7554" s="T43">Mensch-DAT/LOC</ta>
            <ta e="T45" id="Seg_7555" s="T44">Mensch.[NOM]</ta>
            <ta e="T46" id="Seg_7556" s="T45">sagen-EP-CAUS-PTCP.PRS.[NOM]</ta>
            <ta e="T47" id="Seg_7557" s="T46">ähnlich</ta>
            <ta e="T48" id="Seg_7558" s="T47">gehen-PRS-1SG</ta>
            <ta e="T49" id="Seg_7559" s="T48">EVID</ta>
            <ta e="T50" id="Seg_7560" s="T49">Arbeit-1SG-DAT/LOC</ta>
            <ta e="T51" id="Seg_7561" s="T50">und</ta>
            <ta e="T52" id="Seg_7562" s="T51">nun</ta>
            <ta e="T53" id="Seg_7563" s="T52">Arbeit-DAT/LOC</ta>
            <ta e="T54" id="Seg_7564" s="T53">schlecht.[NOM]</ta>
            <ta e="T55" id="Seg_7565" s="T54">dieses</ta>
            <ta e="T56" id="Seg_7566" s="T55">Kind-PL-1SG-ACC</ta>
            <ta e="T57" id="Seg_7567" s="T56">aufziehen-EP-MED-CVB.PURP-1SG</ta>
            <ta e="T58" id="Seg_7568" s="T57">dieses</ta>
            <ta e="T59" id="Seg_7569" s="T58">was-ACC</ta>
            <ta e="T61" id="Seg_7570" s="T59">ganz.und.gar</ta>
            <ta e="T62" id="Seg_7571" s="T61">Kraft-1SG-ACC</ta>
            <ta e="T63" id="Seg_7572" s="T62">schneiden-CVB.SIM</ta>
            <ta e="T64" id="Seg_7573" s="T63">sich.bewegen-EP-REFL-HAB-1SG</ta>
            <ta e="T65" id="Seg_7574" s="T64">Rentier.[NOM]</ta>
            <ta e="T66" id="Seg_7575" s="T65">Reise-3SG.[NOM]</ta>
            <ta e="T67" id="Seg_7576" s="T66">heilig.[NOM]</ta>
            <ta e="T68" id="Seg_7577" s="T67">MOD</ta>
            <ta e="T69" id="Seg_7578" s="T68">früher</ta>
            <ta e="T70" id="Seg_7579" s="T69">viel-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_7580" s="T70">und</ta>
            <ta e="T72" id="Seg_7581" s="T71">schwer-3SG.[NOM]</ta>
            <ta e="T73" id="Seg_7582" s="T72">Mensch.[NOM]</ta>
            <ta e="T74" id="Seg_7583" s="T73">Kraft-3SG.[NOM]</ta>
            <ta e="T75" id="Seg_7584" s="T74">wegwerfen-REFL-CVB.SEQ</ta>
            <ta e="T76" id="Seg_7585" s="T75">gehen-CVB.SEQ</ta>
            <ta e="T77" id="Seg_7586" s="T76">kommen-HAB.[3SG]</ta>
            <ta e="T78" id="Seg_7587" s="T77">ganz</ta>
            <ta e="T79" id="Seg_7588" s="T78">zehn</ta>
            <ta e="T80" id="Seg_7589" s="T79">eins-PL-DAT/LOC</ta>
            <ta e="T81" id="Seg_7590" s="T80">kommen-HAB-1PL</ta>
            <ta e="T82" id="Seg_7591" s="T81">Polarnacht.[NOM]</ta>
            <ta e="T83" id="Seg_7592" s="T82">im.Winter</ta>
            <ta e="T84" id="Seg_7593" s="T83">oh</ta>
            <ta e="T85" id="Seg_7594" s="T84">Kälte.[NOM]</ta>
            <ta e="T86" id="Seg_7595" s="T85">sehr</ta>
            <ta e="T87" id="Seg_7596" s="T86">EMPH</ta>
            <ta e="T90" id="Seg_7597" s="T89">Popigaj.[NOM]</ta>
            <ta e="T91" id="Seg_7598" s="T90">Tundra-3SG-DAT/LOC</ta>
            <ta e="T92" id="Seg_7599" s="T91">Holz.[NOM]</ta>
            <ta e="T93" id="Seg_7600" s="T92">Holz.[NOM]</ta>
            <ta e="T94" id="Seg_7601" s="T93">EMPH</ta>
            <ta e="T95" id="Seg_7602" s="T94">wenn.auch</ta>
            <ta e="T96" id="Seg_7603" s="T95">ganz</ta>
            <ta e="T97" id="Seg_7604" s="T96">Rentier.[NOM]</ta>
            <ta e="T98" id="Seg_7605" s="T97">wo</ta>
            <ta e="T99" id="Seg_7606" s="T98">NEG</ta>
            <ta e="T100" id="Seg_7607" s="T99">zu.sehen.sein-EP-NEG.[3SG]</ta>
            <ta e="T101" id="Seg_7608" s="T100">solch-PL-DAT/LOC</ta>
            <ta e="T102" id="Seg_7609" s="T101">arbeiten-CVB.SEQ</ta>
            <ta e="T103" id="Seg_7610" s="T102">Unglück-VBZ-PST2-EP-1SG</ta>
            <ta e="T104" id="Seg_7611" s="T103">1SG.[NOM]</ta>
            <ta e="T105" id="Seg_7612" s="T104">meist</ta>
            <ta e="T106" id="Seg_7613" s="T105">jenes.[NOM]</ta>
            <ta e="T107" id="Seg_7614" s="T106">machen-CVB.PURP</ta>
            <ta e="T108" id="Seg_7615" s="T107">glücklich.[NOM]</ta>
            <ta e="T109" id="Seg_7616" s="T108">sein-CVB.SEQ-1SG</ta>
            <ta e="T110" id="Seg_7617" s="T109">Rentier-1SG.[NOM]</ta>
            <ta e="T111" id="Seg_7618" s="T110">aufziehen-EP-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T112" id="Seg_7619" s="T111">sehr</ta>
            <ta e="T113" id="Seg_7620" s="T112">zehn-ABL</ta>
            <ta e="T114" id="Seg_7621" s="T113">über</ta>
            <ta e="T115" id="Seg_7622" s="T114">tausend-ACC</ta>
            <ta e="T116" id="Seg_7623" s="T115">aufziehen-PST2-EP-1SG</ta>
            <ta e="T117" id="Seg_7624" s="T116">wie.viel</ta>
            <ta e="T118" id="Seg_7625" s="T117">INDEF</ta>
            <ta e="T119" id="Seg_7626" s="T118">Herde-DAT/LOC</ta>
            <ta e="T120" id="Seg_7627" s="T119">geben-PST2-EP-1SG</ta>
            <ta e="T121" id="Seg_7628" s="T120">Herde-DAT/LOC</ta>
            <ta e="T122" id="Seg_7629" s="T121">geben-PST2-EP-1SG</ta>
            <ta e="T130" id="Seg_7630" s="T129">Brigade.[NOM]</ta>
            <ta e="T131" id="Seg_7631" s="T130">immer</ta>
            <ta e="T132" id="Seg_7632" s="T131">jetzt-DAT/LOC</ta>
            <ta e="T133" id="Seg_7633" s="T132">bis.zu</ta>
            <ta e="T134" id="Seg_7634" s="T133">dieses</ta>
            <ta e="T135" id="Seg_7635" s="T134">jetzt</ta>
            <ta e="T136" id="Seg_7636" s="T135">Sohn-EP-1SG.[NOM]</ta>
            <ta e="T137" id="Seg_7637" s="T136">dieses.[NOM]</ta>
            <ta e="T138" id="Seg_7638" s="T137">sich.beschäftigen-HAB.[3SG]</ta>
            <ta e="T139" id="Seg_7639" s="T138">Volodja-1SG.[NOM]</ta>
            <ta e="T140" id="Seg_7640" s="T139">einsam</ta>
            <ta e="T141" id="Seg_7641" s="T140">Junge-PROPR-1SG</ta>
            <ta e="T148" id="Seg_7642" s="T147">mm</ta>
            <ta e="T149" id="Seg_7643" s="T148">altern-PRS-1PL</ta>
            <ta e="T150" id="Seg_7644" s="T149">zwei-COLL-1PL.[NOM]</ta>
            <ta e="T151" id="Seg_7645" s="T150">altern-PST1-1PL</ta>
            <ta e="T167" id="Seg_7646" s="T166">Brigadeleiter-PL-1PL.[NOM]</ta>
            <ta e="T168" id="Seg_7647" s="T167">ein.Bisschen</ta>
            <ta e="T169" id="Seg_7648" s="T168">Mensch-PL.[NOM]</ta>
            <ta e="T170" id="Seg_7649" s="T169">sein-PST1-3PL</ta>
            <ta e="T171" id="Seg_7650" s="T170">dann</ta>
            <ta e="T174" id="Seg_7651" s="T173">lernen-CVB.SIM</ta>
            <ta e="T175" id="Seg_7652" s="T174">lernen-PTCP.PST-EP-INSTR</ta>
            <ta e="T176" id="Seg_7653" s="T175">EMPH-dann</ta>
            <ta e="T177" id="Seg_7654" s="T176">lernen-CVB.SEQ</ta>
            <ta e="T178" id="Seg_7655" s="T177">bleiben-INFER-1PL</ta>
            <ta e="T179" id="Seg_7656" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_7657" s="T179">1PL.[NOM]</ta>
            <ta e="T181" id="Seg_7658" s="T180">dieses</ta>
            <ta e="T182" id="Seg_7659" s="T181">fürchten-NMNZ-DAT/LOC</ta>
            <ta e="T183" id="Seg_7660" s="T182">Rentier-DAT/LOC</ta>
            <ta e="T184" id="Seg_7661" s="T183">Rentier-ACC</ta>
            <ta e="T185" id="Seg_7662" s="T184">verlieren-EP-NEG-IMP.2PL</ta>
            <ta e="T186" id="Seg_7663" s="T185">denken-CVB.SEQ</ta>
            <ta e="T187" id="Seg_7664" s="T186">sagen-HAB-1SG</ta>
            <ta e="T188" id="Seg_7665" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_7666" s="T188">Rentier-ACC</ta>
            <ta e="T190" id="Seg_7667" s="T189">verlieren-EP-NEG-IMP.2PL</ta>
            <ta e="T191" id="Seg_7668" s="T190">nächster.Morgen-2PL-ACC</ta>
            <ta e="T192" id="Seg_7669" s="T191">erinnern-IMP.2PL</ta>
            <ta e="T193" id="Seg_7670" s="T192">sagen-HAB-1SG</ta>
            <ta e="T194" id="Seg_7671" s="T193">am.Morgen</ta>
            <ta e="T195" id="Seg_7672" s="T194">Plan-VBZ-CVB.SIM</ta>
            <ta e="T196" id="Seg_7673" s="T195">stehen-EP-IMP.2PL</ta>
            <ta e="T197" id="Seg_7674" s="T196">sagen-HAB-1SG</ta>
            <ta e="T198" id="Seg_7675" s="T197">wohin</ta>
            <ta e="T199" id="Seg_7676" s="T198">arbeiten-PTCP.PRS-2PL-ACC</ta>
            <ta e="T200" id="Seg_7677" s="T199">wohin</ta>
            <ta e="T201" id="Seg_7678" s="T200">gehen-PTCP.PRS-2PL-ACC</ta>
            <ta e="T202" id="Seg_7679" s="T201">Rentier-2SG.[NOM]</ta>
            <ta e="T203" id="Seg_7680" s="T202">verlieren-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T204" id="Seg_7681" s="T203">viel-3SG</ta>
            <ta e="T205" id="Seg_7682" s="T204">Unglück-3SG.[NOM]</ta>
            <ta e="T206" id="Seg_7683" s="T205">Verstand.[NOM]</ta>
            <ta e="T207" id="Seg_7684" s="T206">wer-VBZ-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_7685" s="T207">im.Sommer</ta>
            <ta e="T209" id="Seg_7686" s="T208">sehr</ta>
            <ta e="T210" id="Seg_7687" s="T209">Weg-VBZ-MED-CVB.SIM</ta>
            <ta e="T211" id="Seg_7688" s="T210">sein-HAB.[3SG]</ta>
            <ta e="T212" id="Seg_7689" s="T211">jenes</ta>
            <ta e="T213" id="Seg_7690" s="T212">so.viel</ta>
            <ta e="T214" id="Seg_7691" s="T213">gehen-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_7692" s="T214">bis.zu</ta>
            <ta e="T217" id="Seg_7693" s="T215">EMPH</ta>
            <ta e="T218" id="Seg_7694" s="T217">MOD</ta>
            <ta e="T219" id="Seg_7695" s="T218">immer</ta>
            <ta e="T220" id="Seg_7696" s="T219">gut</ta>
            <ta e="T221" id="Seg_7697" s="T220">Name-EP-INSTR</ta>
            <ta e="T222" id="Seg_7698" s="T221">hinausgehen-PST1-1SG</ta>
            <ta e="T223" id="Seg_7699" s="T222">EVID</ta>
            <ta e="T224" id="Seg_7700" s="T223">EMPH</ta>
            <ta e="T225" id="Seg_7701" s="T224">im.Sommer</ta>
            <ta e="T226" id="Seg_7702" s="T225">und</ta>
            <ta e="T227" id="Seg_7703" s="T226">im.Winter</ta>
            <ta e="T228" id="Seg_7704" s="T227">und</ta>
            <ta e="T229" id="Seg_7705" s="T228">doch</ta>
            <ta e="T230" id="Seg_7706" s="T229">arbeiten-PTCP.COND-DAT/LOC</ta>
            <ta e="T231" id="Seg_7707" s="T230">viel</ta>
            <ta e="T232" id="Seg_7708" s="T231">Jahr-ACC</ta>
            <ta e="T233" id="Seg_7709" s="T232">arbeiten-PST1-1SG</ta>
            <ta e="T234" id="Seg_7710" s="T233">fünf-zehn</ta>
            <ta e="T235" id="Seg_7711" s="T234">fünf</ta>
            <ta e="T236" id="Seg_7712" s="T235">Jahr-ACC</ta>
            <ta e="T237" id="Seg_7713" s="T236">Rente-DAT/LOC</ta>
            <ta e="T238" id="Seg_7714" s="T237">hineingehen-CVB.SEQ</ta>
            <ta e="T239" id="Seg_7715" s="T238">nachdem</ta>
            <ta e="T240" id="Seg_7716" s="T239">noch</ta>
            <ta e="T241" id="Seg_7717" s="T240">arbeiten-PST2-EP-1SG</ta>
            <ta e="T242" id="Seg_7718" s="T241">Rentier-1SG.[NOM]</ta>
            <ta e="T243" id="Seg_7719" s="T242">aufziehen-EP-PASS/REFL-PTCP.PRS-3SG-ABL</ta>
            <ta e="T257" id="Seg_7720" s="T256">verlorengehen-PST2-3SG</ta>
            <ta e="T258" id="Seg_7721" s="T257">EMPH</ta>
            <ta e="T259" id="Seg_7722" s="T258">sterben-CVB.SEQ</ta>
            <ta e="T260" id="Seg_7723" s="T259">verlorengehen-CVB.SEQ</ta>
            <ta e="T261" id="Seg_7724" s="T260">verlorengehen-PST2-3SG</ta>
            <ta e="T262" id="Seg_7725" s="T261">dieses</ta>
            <ta e="T263" id="Seg_7726" s="T262">wer-PL-DAT/LOC</ta>
            <ta e="T264" id="Seg_7727" s="T263">Saskylaax-PL-DAT/LOC</ta>
            <ta e="T265" id="Seg_7728" s="T264">gehen-CVB.SEQ</ta>
            <ta e="T266" id="Seg_7729" s="T265">gehen-CVB.SEQ</ta>
            <ta e="T267" id="Seg_7730" s="T266">vielleicht</ta>
            <ta e="T268" id="Seg_7731" s="T267">wildes.Rentier.[NOM]</ta>
            <ta e="T269" id="Seg_7732" s="T268">und</ta>
            <ta e="T270" id="Seg_7733" s="T269">mitnehmen-PRS.[3SG]</ta>
            <ta e="T271" id="Seg_7734" s="T270">wildes.Rentier.[NOM]</ta>
            <ta e="T272" id="Seg_7735" s="T271">mitnehmen-HAB.[3SG]</ta>
            <ta e="T273" id="Seg_7736" s="T272">Wanderung-3SG-DAT/LOC</ta>
            <ta e="T274" id="Seg_7737" s="T273">im.Herbst</ta>
            <ta e="T275" id="Seg_7738" s="T274">kommen-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T276" id="Seg_7739" s="T275">nun</ta>
            <ta e="T277" id="Seg_7740" s="T276">Wolf.[NOM]</ta>
            <ta e="T278" id="Seg_7741" s="T277">essen-PRS.[3SG]</ta>
            <ta e="T279" id="Seg_7742" s="T278">groß</ta>
            <ta e="T280" id="Seg_7743" s="T279">sehr</ta>
            <ta e="T281" id="Seg_7744" s="T280">krank.sein-PRS.[3SG]</ta>
            <ta e="T282" id="Seg_7745" s="T281">jenes-DAT/LOC</ta>
            <ta e="T283" id="Seg_7746" s="T282">Fuß-3SG.[NOM]</ta>
            <ta e="T284" id="Seg_7747" s="T283">und</ta>
            <ta e="T285" id="Seg_7748" s="T284">sofort</ta>
            <ta e="T286" id="Seg_7749" s="T285">lange</ta>
            <ta e="T287" id="Seg_7750" s="T286">gehen-PTCP.HAB-3SG</ta>
            <ta e="T288" id="Seg_7751" s="T287">NEG.[3SG]</ta>
            <ta e="T302" id="Seg_7752" s="T301">töten-CAUS-PRS-3PL</ta>
            <ta e="T303" id="Seg_7753" s="T302">vor.Kurzem</ta>
            <ta e="T304" id="Seg_7754" s="T303">EMPH</ta>
            <ta e="T305" id="Seg_7755" s="T304">es.gibt</ta>
            <ta e="T306" id="Seg_7756" s="T305">sein-TEMP-3SG</ta>
            <ta e="T307" id="Seg_7757" s="T306">sagen-PRS-3PL</ta>
            <ta e="T308" id="Seg_7758" s="T307">EMPH</ta>
            <ta e="T309" id="Seg_7759" s="T308">jetzt</ta>
            <ta e="T310" id="Seg_7760" s="T309">Prämie-VBZ-PTCP.PRS</ta>
            <ta e="T311" id="Seg_7761" s="T310">werden-PST2-3PL</ta>
            <ta e="T312" id="Seg_7762" s="T311">früher</ta>
            <ta e="T313" id="Seg_7763" s="T312">Prämie-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T314" id="Seg_7764" s="T313">NEG-3PL</ta>
            <ta e="T316" id="Seg_7765" s="T315">sein-PST1-3PL</ta>
            <ta e="T320" id="Seg_7766" s="T319">unser-1PL.[NOM]</ta>
            <ta e="T321" id="Seg_7767" s="T320">wer-1PL.[NOM]</ta>
            <ta e="T322" id="Seg_7768" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_7769" s="T322">Tarif-1PL.[NOM]</ta>
            <ta e="T324" id="Seg_7770" s="T323">schlecht.[NOM]</ta>
            <ta e="T325" id="Seg_7771" s="T324">sein-PST1-3SG</ta>
            <ta e="T326" id="Seg_7772" s="T325">früher</ta>
            <ta e="T327" id="Seg_7773" s="T326">klein.[NOM]</ta>
            <ta e="T328" id="Seg_7774" s="T327">klein.[NOM]</ta>
            <ta e="T329" id="Seg_7775" s="T328">sein-PST1-3SG</ta>
            <ta e="T330" id="Seg_7776" s="T329">Rentier-1PL.[NOM]</ta>
            <ta e="T331" id="Seg_7777" s="T330">eigen-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_7778" s="T331">nun</ta>
            <ta e="T333" id="Seg_7779" s="T332">jenes-ACC</ta>
            <ta e="T334" id="Seg_7780" s="T333">1SG.[NOM]</ta>
            <ta e="T335" id="Seg_7781" s="T334">höher.werden-EP-CAUS-EP-IMP.2PL</ta>
            <ta e="T336" id="Seg_7782" s="T335">sagen-CVB.SIM</ta>
            <ta e="T337" id="Seg_7783" s="T336">vergeblich.tun-CVB.SEQ</ta>
            <ta e="T338" id="Seg_7784" s="T337">jetzt</ta>
            <ta e="T339" id="Seg_7785" s="T338">Wort.[NOM]</ta>
            <ta e="T340" id="Seg_7786" s="T339">hören-FUT-3PL</ta>
            <ta e="T341" id="Seg_7787" s="T340">EMPH</ta>
            <ta e="T342" id="Seg_7788" s="T341">jetzt</ta>
            <ta e="T343" id="Seg_7789" s="T342">Arbeiter-PL-1PL.[NOM]</ta>
            <ta e="T344" id="Seg_7790" s="T343">schlecht-PL.[NOM]</ta>
            <ta e="T345" id="Seg_7791" s="T344">jenes-DAT/LOC</ta>
            <ta e="T346" id="Seg_7792" s="T345">doch</ta>
            <ta e="T353" id="Seg_7793" s="T352">mm</ta>
            <ta e="T354" id="Seg_7794" s="T353">sagen-PRS-1PL</ta>
            <ta e="T355" id="Seg_7795" s="T354">EMPH</ta>
            <ta e="T356" id="Seg_7796" s="T355">Stimme.[NOM]</ta>
            <ta e="T357" id="Seg_7797" s="T356">füttern-PTCP.FUT-3PL-ACC</ta>
            <ta e="T358" id="Seg_7798" s="T357">Rentier.[NOM]</ta>
            <ta e="T359" id="Seg_7799" s="T358">Tarif-3SG-ACC</ta>
            <ta e="T360" id="Seg_7800" s="T359">höher.werden-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T361" id="Seg_7801" s="T360">sagen-PRS-1PL</ta>
            <ta e="T364" id="Seg_7802" s="T363">ihr</ta>
            <ta e="T365" id="Seg_7803" s="T364">wer-3PL.[NOM]</ta>
            <ta e="T366" id="Seg_7804" s="T365">Ürüng.Xaja-PL.[NOM]</ta>
            <ta e="T367" id="Seg_7805" s="T366">eigen-3PL.[NOM]</ta>
            <ta e="T368" id="Seg_7806" s="T367">EMPH</ta>
            <ta e="T369" id="Seg_7807" s="T368">eins</ta>
            <ta e="T370" id="Seg_7808" s="T369">Rentier-DAT/LOC</ta>
            <ta e="T371" id="Seg_7809" s="T370">wie.viel</ta>
            <ta e="T372" id="Seg_7810" s="T371">sagen-PST2-3PL=Q</ta>
            <ta e="T373" id="Seg_7811" s="T372">Tag.und.Nacht-DAT/LOC</ta>
            <ta e="T374" id="Seg_7812" s="T373">vier</ta>
            <ta e="T375" id="Seg_7813" s="T374">vier</ta>
            <ta e="T376" id="Seg_7814" s="T375">Rubel.[NOM]</ta>
            <ta e="T381" id="Seg_7815" s="T380">aufziehen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T382" id="Seg_7816" s="T381">wegen</ta>
            <ta e="T383" id="Seg_7817" s="T382">EMPH</ta>
            <ta e="T384" id="Seg_7818" s="T383">ha-ha</ta>
            <ta e="T387" id="Seg_7819" s="T386">Rentier-VBZ-CVB.SEQ</ta>
            <ta e="T388" id="Seg_7820" s="T387">aufziehen-CVB.SEQ</ta>
            <ta e="T389" id="Seg_7821" s="T388">hinausgehen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T390" id="Seg_7822" s="T389">wegen</ta>
            <ta e="T391" id="Seg_7823" s="T390">ihr.[NOM]</ta>
            <ta e="T393" id="Seg_7824" s="T391">auch</ta>
            <ta e="T394" id="Seg_7825" s="T393">Schneesturm.[NOM]</ta>
            <ta e="T395" id="Seg_7826" s="T394">arm.[NOM]</ta>
            <ta e="T396" id="Seg_7827" s="T395">machen-PRS.[3SG]</ta>
            <ta e="T397" id="Seg_7828" s="T396">EMPH</ta>
            <ta e="T398" id="Seg_7829" s="T397">Ürüng.Xaja-2SG.[NOM]</ta>
            <ta e="T399" id="Seg_7830" s="T398">Ort-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_7831" s="T399">sehr</ta>
            <ta e="T401" id="Seg_7832" s="T400">arm</ta>
            <ta e="T402" id="Seg_7833" s="T401">Erde.[NOM]</ta>
            <ta e="T403" id="Seg_7834" s="T402">jenes.[NOM]</ta>
            <ta e="T404" id="Seg_7835" s="T403">wegen</ta>
            <ta e="T405" id="Seg_7836" s="T404">jenes</ta>
            <ta e="T406" id="Seg_7837" s="T405">Tarif-PL-EP-2SG.[NOM]</ta>
            <ta e="T407" id="Seg_7838" s="T406">höher.werden-EP-CAUS-PST2-3PL</ta>
            <ta e="T408" id="Seg_7839" s="T407">zuerst-ADJZ-ABL</ta>
            <ta e="T409" id="Seg_7840" s="T408">Insektenwolke-ABL</ta>
            <ta e="T410" id="Seg_7841" s="T409">früher-ABL</ta>
            <ta e="T411" id="Seg_7842" s="T410">Rentier.[NOM]</ta>
            <ta e="T412" id="Seg_7843" s="T411">aufziehen-NMNZ.[NOM]</ta>
            <ta e="T413" id="Seg_7844" s="T412">sehr</ta>
            <ta e="T414" id="Seg_7845" s="T413">EMPH</ta>
            <ta e="T415" id="Seg_7846" s="T414">schwer-3SG</ta>
            <ta e="T418" id="Seg_7847" s="T417">AFFIRM</ta>
            <ta e="T419" id="Seg_7848" s="T418">sterben-PTCP.PRS</ta>
            <ta e="T420" id="Seg_7849" s="T419">sterben-PTCP.PRS</ta>
            <ta e="T421" id="Seg_7850" s="T420">Mensch-2SG.[NOM]</ta>
            <ta e="T422" id="Seg_7851" s="T421">sterben-PRS.[3SG]</ta>
            <ta e="T423" id="Seg_7852" s="T422">jenes</ta>
            <ta e="T425" id="Seg_7853" s="T424">Rentier-ACC</ta>
            <ta e="T426" id="Seg_7854" s="T425">mit</ta>
            <ta e="T427" id="Seg_7855" s="T426">mitkommen-CVB.SEQ</ta>
            <ta e="T442" id="Seg_7856" s="T441">dieses</ta>
            <ta e="T443" id="Seg_7857" s="T442">jakutisch</ta>
            <ta e="T444" id="Seg_7858" s="T443">Ort-3SG-DAT/LOC</ta>
            <ta e="T445" id="Seg_7859" s="T444">solch-PL.[NOM]</ta>
            <ta e="T446" id="Seg_7860" s="T445">sein-PST1-3SG</ta>
            <ta e="T447" id="Seg_7861" s="T446">Rentier-ACC</ta>
            <ta e="T448" id="Seg_7862" s="T447">sehr</ta>
            <ta e="T449" id="Seg_7863" s="T448">achten-PRS-3PL</ta>
            <ta e="T450" id="Seg_7864" s="T449">füttern-PTCP.FUT-3PL-ACC</ta>
            <ta e="T451" id="Seg_7865" s="T450">wollen-PRS-3PL</ta>
            <ta e="T452" id="Seg_7866" s="T451">oh.nein</ta>
            <ta e="T453" id="Seg_7867" s="T452">Geld.[NOM]</ta>
            <ta e="T454" id="Seg_7868" s="T453">nehmen-PRS-3PL</ta>
            <ta e="T455" id="Seg_7869" s="T454">gut.[NOM]</ta>
            <ta e="T456" id="Seg_7870" s="T455">sein-PST1-3SG</ta>
            <ta e="T457" id="Seg_7871" s="T456">Rentier-AG-PL-EP-2SG.[NOM]</ta>
            <ta e="T458" id="Seg_7872" s="T457">Monat-DAT/LOC</ta>
            <ta e="T459" id="Seg_7873" s="T458">vier-DISTR</ta>
            <ta e="T460" id="Seg_7874" s="T459">fünf-DISTR</ta>
            <ta e="T461" id="Seg_7875" s="T460">tausend-ACC</ta>
            <ta e="T462" id="Seg_7876" s="T461">eins</ta>
            <ta e="T463" id="Seg_7877" s="T462">Monat-DAT/LOC</ta>
            <ta e="T464" id="Seg_7878" s="T463">nehmen-PRS-3PL</ta>
            <ta e="T465" id="Seg_7879" s="T464">3PL.[NOM]</ta>
            <ta e="T466" id="Seg_7880" s="T465">1PL.[NOM]</ta>
            <ta e="T467" id="Seg_7881" s="T466">jenes</ta>
            <ta e="T468" id="Seg_7882" s="T467">wann</ta>
            <ta e="T471" id="Seg_7883" s="T470">solch-POSS</ta>
            <ta e="T472" id="Seg_7884" s="T471">EMPH</ta>
            <ta e="T474" id="Seg_7885" s="T472">NEG-1PL</ta>
            <ta e="T475" id="Seg_7886" s="T474">ganz</ta>
            <ta e="T477" id="Seg_7887" s="T475">NEG.EX</ta>
            <ta e="T478" id="Seg_7888" s="T477">einfach</ta>
            <ta e="T479" id="Seg_7889" s="T478">fünf</ta>
            <ta e="T480" id="Seg_7890" s="T479">hundert</ta>
            <ta e="T481" id="Seg_7891" s="T480">wahrscheinlich</ta>
            <ta e="T482" id="Seg_7892" s="T481">Q</ta>
            <ta e="T483" id="Seg_7893" s="T482">Vorschuss.[NOM]</ta>
            <ta e="T484" id="Seg_7894" s="T483">ähnlich</ta>
            <ta e="T486" id="Seg_7895" s="T484">Tarif-3SG-1PL.[NOM]</ta>
            <ta e="T487" id="Seg_7896" s="T486">wenig-3SG.[NOM]</ta>
            <ta e="T488" id="Seg_7897" s="T487">sehr</ta>
            <ta e="T489" id="Seg_7898" s="T488">Kopeken.[NOM]</ta>
            <ta e="T490" id="Seg_7899" s="T489">jetzt-DAT/LOC</ta>
            <ta e="T491" id="Seg_7900" s="T490">bis.zu</ta>
            <ta e="T492" id="Seg_7901" s="T491">Ende-EP-ABL</ta>
            <ta e="T493" id="Seg_7902" s="T492">verlieren-PST1-3PL</ta>
            <ta e="T494" id="Seg_7903" s="T493">jetzt</ta>
            <ta e="T495" id="Seg_7904" s="T494">was-3SG-ACC</ta>
            <ta e="T496" id="Seg_7905" s="T495">EMPH</ta>
            <ta e="T497" id="Seg_7906" s="T496">sprechen-CVB.SIM</ta>
            <ta e="T498" id="Seg_7907" s="T497">vergeblich.tun-CVB.SEQ</ta>
            <ta e="T499" id="Seg_7908" s="T498">warum</ta>
            <ta e="T500" id="Seg_7909" s="T499">berühren-PRS-2PL</ta>
            <ta e="T501" id="Seg_7910" s="T500">dieses.[NOM]</ta>
            <ta e="T502" id="Seg_7911" s="T501">was.[NOM]</ta>
            <ta e="T503" id="Seg_7912" s="T502">machen-CVB.PURP</ta>
            <ta e="T504" id="Seg_7913" s="T503">Sowchose-2PL.[NOM]</ta>
            <ta e="T505" id="Seg_7914" s="T504">Sowchose-2PL.[NOM]</ta>
            <ta e="T506" id="Seg_7915" s="T505">Rentier-3SG.[NOM]</ta>
            <ta e="T507" id="Seg_7916" s="T506">selbst-2PL.[NOM]</ta>
            <ta e="T508" id="Seg_7917" s="T507">essen-PRS-2PL</ta>
            <ta e="T509" id="Seg_7918" s="T508">EMPH</ta>
            <ta e="T510" id="Seg_7919" s="T509">sagen-HAB-1SG</ta>
            <ta e="T511" id="Seg_7920" s="T510">sich.anziehen-PRS-2PL</ta>
            <ta e="T512" id="Seg_7921" s="T511">und.so.weiter-PRS-2PL</ta>
            <ta e="T513" id="Seg_7922" s="T512">warum</ta>
            <ta e="T514" id="Seg_7923" s="T513">Sowchose.[NOM]</ta>
            <ta e="T515" id="Seg_7924" s="T514">eigen-3SG.[NOM]</ta>
            <ta e="T516" id="Seg_7925" s="T515">sagen-PRS-2PL</ta>
            <ta e="T517" id="Seg_7926" s="T516">sagen-PRS-1SG</ta>
            <ta e="T518" id="Seg_7927" s="T517">selbst-2PL.[NOM]</ta>
            <ta e="T519" id="Seg_7928" s="T518">Reichtum-2PL.[NOM]</ta>
            <ta e="T520" id="Seg_7929" s="T519">EMPH</ta>
            <ta e="T521" id="Seg_7930" s="T520">sagen-PRS-1SG</ta>
            <ta e="T522" id="Seg_7931" s="T521">jenes.[NOM]</ta>
            <ta e="T523" id="Seg_7932" s="T522">wegen</ta>
            <ta e="T524" id="Seg_7933" s="T523">Geld.[NOM]</ta>
            <ta e="T525" id="Seg_7934" s="T524">bekommen-PRS-2PL</ta>
            <ta e="T526" id="Seg_7935" s="T525">was-ACC=Q</ta>
            <ta e="T527" id="Seg_7936" s="T526">was-EP-INSTR</ta>
            <ta e="T530" id="Seg_7937" s="T529">nehmen-FUT-1PL</ta>
            <ta e="T531" id="Seg_7938" s="T530">denken-PRS-2PL</ta>
            <ta e="T532" id="Seg_7939" s="T531">sagen-PRS-1SG</ta>
            <ta e="T533" id="Seg_7940" s="T532">Sünde.[NOM]</ta>
            <ta e="T534" id="Seg_7941" s="T533">sein-FUT.[3SG]</ta>
            <ta e="T535" id="Seg_7942" s="T534">groß-ADVZ</ta>
            <ta e="T536" id="Seg_7943" s="T535">Betrug-VBZ-PST2-EP-1SG</ta>
            <ta e="T537" id="Seg_7944" s="T536">sagen-TEMP-1SG</ta>
            <ta e="T538" id="Seg_7945" s="T537">verkaufen-EP-MED-PST2.NEG-EP-1SG</ta>
            <ta e="T539" id="Seg_7946" s="T538">und</ta>
            <ta e="T540" id="Seg_7947" s="T539">wenn.auch</ta>
            <ta e="T541" id="Seg_7948" s="T540">ganz</ta>
            <ta e="T542" id="Seg_7949" s="T541">Mensch-2SG.[NOM]</ta>
            <ta e="T543" id="Seg_7950" s="T542">jeder-3SG.[NOM]</ta>
            <ta e="T544" id="Seg_7951" s="T543">verkaufen-EP-MED-PST2-3SG</ta>
            <ta e="T545" id="Seg_7952" s="T544">jenes</ta>
            <ta e="T546" id="Seg_7953" s="T545">Sowchose-ABL</ta>
            <ta e="T550" id="Seg_7954" s="T549">Geld.[NOM]</ta>
            <ta e="T551" id="Seg_7955" s="T550">machen-PRS-3PL</ta>
            <ta e="T572" id="Seg_7956" s="T571">solch.[NOM]</ta>
            <ta e="T573" id="Seg_7957" s="T572">solch.[NOM]</ta>
            <ta e="T581" id="Seg_7958" s="T580">Rentier-AG.[NOM]</ta>
            <ta e="T582" id="Seg_7959" s="T581">Mensch.[NOM]</ta>
            <ta e="T583" id="Seg_7960" s="T582">nicht.ausreichend.[NOM]</ta>
            <ta e="T584" id="Seg_7961" s="T583">doch</ta>
            <ta e="T585" id="Seg_7962" s="T584">jenes-ACC</ta>
            <ta e="T613" id="Seg_7963" s="T612">dieses</ta>
            <ta e="T614" id="Seg_7964" s="T613">gut-ADVZ</ta>
            <ta e="T615" id="Seg_7965" s="T614">arbeiten-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T616" id="Seg_7966" s="T615">wegen</ta>
            <ta e="T617" id="Seg_7967" s="T616">hinausgehen-EP-PST2-EP-1SG</ta>
            <ta e="T618" id="Seg_7968" s="T617">Mensch-PL-ACC</ta>
            <ta e="T619" id="Seg_7969" s="T618">mit</ta>
            <ta e="T620" id="Seg_7970" s="T619">sich.unterhalten-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T621" id="Seg_7971" s="T620">wegen</ta>
            <ta e="T622" id="Seg_7972" s="T621">besser</ta>
            <ta e="T623" id="Seg_7973" s="T622">jung.[NOM]</ta>
            <ta e="T624" id="Seg_7974" s="T623">sein-TEMP-1SG</ta>
            <ta e="T625" id="Seg_7975" s="T624">sehr</ta>
            <ta e="T626" id="Seg_7976" s="T625">agil.[NOM]</ta>
            <ta e="T627" id="Seg_7977" s="T626">sein-PST1-1SG</ta>
            <ta e="T628" id="Seg_7978" s="T627">EMPH</ta>
            <ta e="T629" id="Seg_7979" s="T628">krank.sein-EP-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T630" id="Seg_7980" s="T629">und</ta>
            <ta e="T631" id="Seg_7981" s="T630">wegen</ta>
            <ta e="T632" id="Seg_7982" s="T631">sich.unterhalten-NMNZ.[NOM]</ta>
            <ta e="T633" id="Seg_7983" s="T632">Überreste-PROPR.[NOM]</ta>
            <ta e="T634" id="Seg_7984" s="T633">sein-PST1-1SG</ta>
            <ta e="T635" id="Seg_7985" s="T634">Russe.[NOM]</ta>
            <ta e="T636" id="Seg_7986" s="T635">und</ta>
            <ta e="T637" id="Seg_7987" s="T636">vielleicht</ta>
            <ta e="T638" id="Seg_7988" s="T637">Dolgane.[NOM]</ta>
            <ta e="T639" id="Seg_7989" s="T638">und</ta>
            <ta e="T640" id="Seg_7990" s="T639">vielleicht</ta>
            <ta e="T641" id="Seg_7991" s="T640">fremd.[NOM]</ta>
            <ta e="T642" id="Seg_7992" s="T641">und</ta>
            <ta e="T643" id="Seg_7993" s="T642">vielleicht</ta>
            <ta e="T653" id="Seg_7994" s="T652">Tag.[NOM]</ta>
            <ta e="T654" id="Seg_7995" s="T653">jeder</ta>
            <ta e="T655" id="Seg_7996" s="T654">EMPH</ta>
            <ta e="T656" id="Seg_7997" s="T655">sehen-PRS-3PL</ta>
            <ta e="T657" id="Seg_7998" s="T656">EMPH</ta>
            <ta e="T658" id="Seg_7999" s="T657">Fehler-ACC</ta>
            <ta e="T659" id="Seg_8000" s="T658">was-3PL.[NOM]</ta>
            <ta e="T661" id="Seg_8001" s="T660">machen-PRS-2SG</ta>
            <ta e="T662" id="Seg_8002" s="T661">sehr</ta>
            <ta e="T663" id="Seg_8003" s="T662">Fehler-POSS</ta>
            <ta e="T664" id="Seg_8004" s="T663">NEG-1SG</ta>
            <ta e="T665" id="Seg_8005" s="T664">EMPH</ta>
            <ta e="T666" id="Seg_8006" s="T665">jenes.[NOM]</ta>
            <ta e="T667" id="Seg_8007" s="T666">wegen</ta>
            <ta e="T669" id="Seg_8008" s="T667">Russe-PL.[NOM]</ta>
            <ta e="T694" id="Seg_8009" s="T693">gebären-PTCP.PST-3SG-ACC</ta>
            <ta e="T695" id="Seg_8010" s="T694">erzählen-PRS-2SG</ta>
            <ta e="T696" id="Seg_8011" s="T695">EMPH</ta>
            <ta e="T697" id="Seg_8012" s="T696">Sicherheit-3SG-ACC</ta>
            <ta e="T698" id="Seg_8013" s="T697">Kalb-EP-2SG.[NOM]</ta>
            <ta e="T699" id="Seg_8014" s="T698">winzig.[NOM]</ta>
            <ta e="T700" id="Seg_8015" s="T699">EMPH</ta>
            <ta e="T701" id="Seg_8016" s="T700">sein-PST2.[3SG]</ta>
            <ta e="T702" id="Seg_8017" s="T701">Nacht-ACC</ta>
            <ta e="T703" id="Seg_8018" s="T702">während</ta>
            <ta e="T704" id="Seg_8019" s="T703">schlafen-NEG.CVB.SIM</ta>
            <ta e="T705" id="Seg_8020" s="T704">gehen-PRS-2SG</ta>
            <ta e="T706" id="Seg_8021" s="T705">EMPH</ta>
            <ta e="T707" id="Seg_8022" s="T706">jenes</ta>
            <ta e="T708" id="Seg_8023" s="T707">Rentierkalb.[NOM]</ta>
            <ta e="T709" id="Seg_8024" s="T708">gebären-PTCP.PRS-3SG-ACC</ta>
            <ta e="T710" id="Seg_8025" s="T709">hüten-CVB.SIM</ta>
            <ta e="T711" id="Seg_8026" s="T710">viel</ta>
            <ta e="T712" id="Seg_8027" s="T711">Rentier-DAT/LOC</ta>
            <ta e="T715" id="Seg_8028" s="T714">viel</ta>
            <ta e="T716" id="Seg_8029" s="T715">Rentierkalb.[NOM]</ta>
            <ta e="T717" id="Seg_8030" s="T716">EMPH</ta>
         </annotation>
         <annotation name="gr" tierref="gr-ChSA">
            <ta e="T7" id="Seg_8031" s="T6">отец-PL-1PL.[NOM]</ta>
            <ta e="T8" id="Seg_8032" s="T7">мать-PL-1PL.[NOM]</ta>
            <ta e="T9" id="Seg_8033" s="T8">вот</ta>
            <ta e="T10" id="Seg_8034" s="T9">EMPH</ta>
            <ta e="T11" id="Seg_8035" s="T10">старый-PL.[NOM]</ta>
            <ta e="T12" id="Seg_8036" s="T11">быть-PST1-3SG</ta>
            <ta e="T13" id="Seg_8037" s="T12">EMPH</ta>
            <ta e="T14" id="Seg_8038" s="T13">У-ACC</ta>
            <ta e="T15" id="Seg_8039" s="T14">А-ACC</ta>
            <ta e="T16" id="Seg_8040" s="T15">знать-NEG-3PL</ta>
            <ta e="T19" id="Seg_8041" s="T18">просто</ta>
            <ta e="T22" id="Seg_8042" s="T21">рука.[NOM]</ta>
            <ta e="T23" id="Seg_8043" s="T22">давить-PRS-3PL</ta>
            <ta e="T24" id="Seg_8044" s="T23">такой</ta>
            <ta e="T25" id="Seg_8045" s="T24">делать-HAB-1SG</ta>
            <ta e="T26" id="Seg_8046" s="T25">1SG.[NOM]</ta>
            <ta e="T27" id="Seg_8047" s="T26">тоже</ta>
            <ta e="T28" id="Seg_8048" s="T27">EMPH-теперь</ta>
            <ta e="T29" id="Seg_8049" s="T28">ведь</ta>
            <ta e="T30" id="Seg_8050" s="T29">такой.[NOM]</ta>
            <ta e="T31" id="Seg_8051" s="T30">как</ta>
            <ta e="T32" id="Seg_8052" s="T31">вот</ta>
            <ta e="T33" id="Seg_8053" s="T32">ум-EP-1SG.[NOM]</ta>
            <ta e="T34" id="Seg_8054" s="T33">летать-PST2-3SG</ta>
            <ta e="T35" id="Seg_8055" s="T34">очень</ta>
            <ta e="T36" id="Seg_8056" s="T35">год-ACC</ta>
            <ta e="T37" id="Seg_8057" s="T36">в.течение</ta>
            <ta e="T38" id="Seg_8058" s="T37">быть.больным-EP-PST2-EP-1SG</ta>
            <ta e="T39" id="Seg_8059" s="T38">зато</ta>
            <ta e="T40" id="Seg_8060" s="T39">тот.[NOM]</ta>
            <ta e="T41" id="Seg_8061" s="T40">несмотря.на</ta>
            <ta e="T42" id="Seg_8062" s="T41">нормальный-1SG</ta>
            <ta e="T43" id="Seg_8063" s="T42">немного</ta>
            <ta e="T44" id="Seg_8064" s="T43">человек-DAT/LOC</ta>
            <ta e="T45" id="Seg_8065" s="T44">человек.[NOM]</ta>
            <ta e="T46" id="Seg_8066" s="T45">говорить-EP-CAUS-PTCP.PRS.[NOM]</ta>
            <ta e="T47" id="Seg_8067" s="T46">подобно</ta>
            <ta e="T48" id="Seg_8068" s="T47">идти-PRS-1SG</ta>
            <ta e="T49" id="Seg_8069" s="T48">EVID</ta>
            <ta e="T50" id="Seg_8070" s="T49">работа-1SG-DAT/LOC</ta>
            <ta e="T51" id="Seg_8071" s="T50">да</ta>
            <ta e="T52" id="Seg_8072" s="T51">вот</ta>
            <ta e="T53" id="Seg_8073" s="T52">работа-DAT/LOC</ta>
            <ta e="T54" id="Seg_8074" s="T53">плохой.[NOM]</ta>
            <ta e="T55" id="Seg_8075" s="T54">этот</ta>
            <ta e="T56" id="Seg_8076" s="T55">ребенок-PL-1SG-ACC</ta>
            <ta e="T57" id="Seg_8077" s="T56">воспитывать-EP-MED-CVB.PURP-1SG</ta>
            <ta e="T58" id="Seg_8078" s="T57">этот</ta>
            <ta e="T59" id="Seg_8079" s="T58">что-ACC</ta>
            <ta e="T61" id="Seg_8080" s="T59">совсем</ta>
            <ta e="T62" id="Seg_8081" s="T61">сила-1SG-ACC</ta>
            <ta e="T63" id="Seg_8082" s="T62">резать-CVB.SIM</ta>
            <ta e="T64" id="Seg_8083" s="T63">двигаться-EP-REFL-HAB-1SG</ta>
            <ta e="T65" id="Seg_8084" s="T64">олень.[NOM]</ta>
            <ta e="T66" id="Seg_8085" s="T65">поездка-3SG.[NOM]</ta>
            <ta e="T67" id="Seg_8086" s="T66">святой.[NOM]</ta>
            <ta e="T68" id="Seg_8087" s="T67">MOD</ta>
            <ta e="T69" id="Seg_8088" s="T68">раньше</ta>
            <ta e="T70" id="Seg_8089" s="T69">много-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_8090" s="T70">да</ta>
            <ta e="T72" id="Seg_8091" s="T71">тяжелый-3SG.[NOM]</ta>
            <ta e="T73" id="Seg_8092" s="T72">человек.[NOM]</ta>
            <ta e="T74" id="Seg_8093" s="T73">сила-3SG.[NOM]</ta>
            <ta e="T75" id="Seg_8094" s="T74">выбрасывать-REFL-CVB.SEQ</ta>
            <ta e="T76" id="Seg_8095" s="T75">идти-CVB.SEQ</ta>
            <ta e="T77" id="Seg_8096" s="T76">приходить-HAB.[3SG]</ta>
            <ta e="T78" id="Seg_8097" s="T77">совсем</ta>
            <ta e="T79" id="Seg_8098" s="T78">десять</ta>
            <ta e="T80" id="Seg_8099" s="T79">один-PL-DAT/LOC</ta>
            <ta e="T81" id="Seg_8100" s="T80">приходить-HAB-1PL</ta>
            <ta e="T82" id="Seg_8101" s="T81">полярная.ночь.[NOM]</ta>
            <ta e="T83" id="Seg_8102" s="T82">зимой</ta>
            <ta e="T84" id="Seg_8103" s="T83">о</ta>
            <ta e="T85" id="Seg_8104" s="T84">холод.[NOM]</ta>
            <ta e="T86" id="Seg_8105" s="T85">очень</ta>
            <ta e="T87" id="Seg_8106" s="T86">EMPH</ta>
            <ta e="T90" id="Seg_8107" s="T89">Попигай.[NOM]</ta>
            <ta e="T91" id="Seg_8108" s="T90">тундра-3SG-DAT/LOC</ta>
            <ta e="T92" id="Seg_8109" s="T91">дерево.[NOM]</ta>
            <ta e="T93" id="Seg_8110" s="T92">дерево.[NOM]</ta>
            <ta e="T94" id="Seg_8111" s="T93">EMPH</ta>
            <ta e="T95" id="Seg_8112" s="T94">хоть</ta>
            <ta e="T96" id="Seg_8113" s="T95">совсем</ta>
            <ta e="T97" id="Seg_8114" s="T96">олень.[NOM]</ta>
            <ta e="T98" id="Seg_8115" s="T97">где</ta>
            <ta e="T99" id="Seg_8116" s="T98">NEG</ta>
            <ta e="T100" id="Seg_8117" s="T99">быть.видно-EP-NEG.[3SG]</ta>
            <ta e="T101" id="Seg_8118" s="T100">такой-PL-DAT/LOC</ta>
            <ta e="T102" id="Seg_8119" s="T101">работать-CVB.SEQ</ta>
            <ta e="T103" id="Seg_8120" s="T102">беда-VBZ-PST2-EP-1SG</ta>
            <ta e="T104" id="Seg_8121" s="T103">1SG.[NOM]</ta>
            <ta e="T105" id="Seg_8122" s="T104">самый</ta>
            <ta e="T106" id="Seg_8123" s="T105">тот.[NOM]</ta>
            <ta e="T107" id="Seg_8124" s="T106">делать-CVB.PURP</ta>
            <ta e="T108" id="Seg_8125" s="T107">счастливый.[NOM]</ta>
            <ta e="T109" id="Seg_8126" s="T108">быть-CVB.SEQ-1SG</ta>
            <ta e="T110" id="Seg_8127" s="T109">олень-1SG.[NOM]</ta>
            <ta e="T111" id="Seg_8128" s="T110">воспитывать-EP-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T112" id="Seg_8129" s="T111">очень</ta>
            <ta e="T113" id="Seg_8130" s="T112">десять-ABL</ta>
            <ta e="T114" id="Seg_8131" s="T113">больше</ta>
            <ta e="T115" id="Seg_8132" s="T114">тысяча-ACC</ta>
            <ta e="T116" id="Seg_8133" s="T115">воспитывать-PST2-EP-1SG</ta>
            <ta e="T117" id="Seg_8134" s="T116">сколько</ta>
            <ta e="T118" id="Seg_8135" s="T117">INDEF</ta>
            <ta e="T119" id="Seg_8136" s="T118">стадо-DAT/LOC</ta>
            <ta e="T120" id="Seg_8137" s="T119">давать-PST2-EP-1SG</ta>
            <ta e="T121" id="Seg_8138" s="T120">стадо-DAT/LOC</ta>
            <ta e="T122" id="Seg_8139" s="T121">давать-PST2-EP-1SG</ta>
            <ta e="T130" id="Seg_8140" s="T129">бригада.[NOM]</ta>
            <ta e="T131" id="Seg_8141" s="T130">всегда</ta>
            <ta e="T132" id="Seg_8142" s="T131">теперь-DAT/LOC</ta>
            <ta e="T133" id="Seg_8143" s="T132">пока</ta>
            <ta e="T134" id="Seg_8144" s="T133">этот</ta>
            <ta e="T135" id="Seg_8145" s="T134">теперь</ta>
            <ta e="T136" id="Seg_8146" s="T135">сын-EP-1SG.[NOM]</ta>
            <ta e="T137" id="Seg_8147" s="T136">тот.[NOM]</ta>
            <ta e="T138" id="Seg_8148" s="T137">заниматься-HAB.[3SG]</ta>
            <ta e="T139" id="Seg_8149" s="T138">Володя-1SG.[NOM]</ta>
            <ta e="T140" id="Seg_8150" s="T139">одинокий</ta>
            <ta e="T141" id="Seg_8151" s="T140">мальчик-PROPR-1SG</ta>
            <ta e="T148" id="Seg_8152" s="T147">мм</ta>
            <ta e="T149" id="Seg_8153" s="T148">постареть-PRS-1PL</ta>
            <ta e="T150" id="Seg_8154" s="T149">два-COLL-1PL.[NOM]</ta>
            <ta e="T151" id="Seg_8155" s="T150">постареть-PST1-1PL</ta>
            <ta e="T167" id="Seg_8156" s="T166">бригадир-PL-1PL.[NOM]</ta>
            <ta e="T168" id="Seg_8157" s="T167">немного</ta>
            <ta e="T169" id="Seg_8158" s="T168">человек-PL.[NOM]</ta>
            <ta e="T170" id="Seg_8159" s="T169">быть-PST1-3PL</ta>
            <ta e="T171" id="Seg_8160" s="T170">потом</ta>
            <ta e="T174" id="Seg_8161" s="T173">учиться-CVB.SIM</ta>
            <ta e="T175" id="Seg_8162" s="T174">учиться-PTCP.PST-EP-INSTR</ta>
            <ta e="T176" id="Seg_8163" s="T175">EMPH-вот</ta>
            <ta e="T177" id="Seg_8164" s="T176">учиться-CVB.SEQ</ta>
            <ta e="T178" id="Seg_8165" s="T177">оставаться-INFER-1PL</ta>
            <ta e="T179" id="Seg_8166" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_8167" s="T179">1PL.[NOM]</ta>
            <ta e="T181" id="Seg_8168" s="T180">тот</ta>
            <ta e="T182" id="Seg_8169" s="T181">опасаться-NMNZ-DAT/LOC</ta>
            <ta e="T183" id="Seg_8170" s="T182">олень-DAT/LOC</ta>
            <ta e="T184" id="Seg_8171" s="T183">олень-ACC</ta>
            <ta e="T185" id="Seg_8172" s="T184">потерять-EP-NEG-IMP.2PL</ta>
            <ta e="T186" id="Seg_8173" s="T185">думать-CVB.SEQ</ta>
            <ta e="T187" id="Seg_8174" s="T186">говорить-HAB-1SG</ta>
            <ta e="T188" id="Seg_8175" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_8176" s="T188">олень-ACC</ta>
            <ta e="T190" id="Seg_8177" s="T189">потерять-EP-NEG-IMP.2PL</ta>
            <ta e="T191" id="Seg_8178" s="T190">следующее.утро-2PL-ACC</ta>
            <ta e="T192" id="Seg_8179" s="T191">помнить-IMP.2PL</ta>
            <ta e="T193" id="Seg_8180" s="T192">говорить-HAB-1SG</ta>
            <ta e="T194" id="Seg_8181" s="T193">утром</ta>
            <ta e="T195" id="Seg_8182" s="T194">план-VBZ-CVB.SIM</ta>
            <ta e="T196" id="Seg_8183" s="T195">стоять-EP-IMP.2PL</ta>
            <ta e="T197" id="Seg_8184" s="T196">говорить-HAB-1SG</ta>
            <ta e="T198" id="Seg_8185" s="T197">куда</ta>
            <ta e="T199" id="Seg_8186" s="T198">работать-PTCP.PRS-2PL-ACC</ta>
            <ta e="T200" id="Seg_8187" s="T199">куда</ta>
            <ta e="T201" id="Seg_8188" s="T200">идти-PTCP.PRS-2PL-ACC</ta>
            <ta e="T202" id="Seg_8189" s="T201">олень-2SG.[NOM]</ta>
            <ta e="T203" id="Seg_8190" s="T202">потерять-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T204" id="Seg_8191" s="T203">много-3SG</ta>
            <ta e="T205" id="Seg_8192" s="T204">беда-3SG.[NOM]</ta>
            <ta e="T206" id="Seg_8193" s="T205">ум.[NOM]</ta>
            <ta e="T207" id="Seg_8194" s="T206">кто-VBZ-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_8195" s="T207">летом</ta>
            <ta e="T209" id="Seg_8196" s="T208">очень</ta>
            <ta e="T210" id="Seg_8197" s="T209">путь-VBZ-MED-CVB.SIM</ta>
            <ta e="T211" id="Seg_8198" s="T210">быть-HAB.[3SG]</ta>
            <ta e="T212" id="Seg_8199" s="T211">тот</ta>
            <ta e="T213" id="Seg_8200" s="T212">столько</ta>
            <ta e="T214" id="Seg_8201" s="T213">идти-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T215" id="Seg_8202" s="T214">пока</ta>
            <ta e="T217" id="Seg_8203" s="T215">EMPH</ta>
            <ta e="T218" id="Seg_8204" s="T217">MOD</ta>
            <ta e="T219" id="Seg_8205" s="T218">всегда</ta>
            <ta e="T220" id="Seg_8206" s="T219">хороший</ta>
            <ta e="T221" id="Seg_8207" s="T220">имя-EP-INSTR</ta>
            <ta e="T222" id="Seg_8208" s="T221">выйти-PST1-1SG</ta>
            <ta e="T223" id="Seg_8209" s="T222">EVID</ta>
            <ta e="T224" id="Seg_8210" s="T223">EMPH</ta>
            <ta e="T225" id="Seg_8211" s="T224">летом</ta>
            <ta e="T226" id="Seg_8212" s="T225">да</ta>
            <ta e="T227" id="Seg_8213" s="T226">зимой</ta>
            <ta e="T228" id="Seg_8214" s="T227">да</ta>
            <ta e="T229" id="Seg_8215" s="T228">ведь</ta>
            <ta e="T230" id="Seg_8216" s="T229">работать-PTCP.COND-DAT/LOC</ta>
            <ta e="T231" id="Seg_8217" s="T230">много</ta>
            <ta e="T232" id="Seg_8218" s="T231">год-ACC</ta>
            <ta e="T233" id="Seg_8219" s="T232">работать-PST1-1SG</ta>
            <ta e="T234" id="Seg_8220" s="T233">пять-десять</ta>
            <ta e="T235" id="Seg_8221" s="T234">пять</ta>
            <ta e="T236" id="Seg_8222" s="T235">год-ACC</ta>
            <ta e="T237" id="Seg_8223" s="T236">пенсия-DAT/LOC</ta>
            <ta e="T238" id="Seg_8224" s="T237">входить-CVB.SEQ</ta>
            <ta e="T239" id="Seg_8225" s="T238">после</ta>
            <ta e="T240" id="Seg_8226" s="T239">еще</ta>
            <ta e="T241" id="Seg_8227" s="T240">работать-PST2-EP-1SG</ta>
            <ta e="T242" id="Seg_8228" s="T241">олень-1SG.[NOM]</ta>
            <ta e="T243" id="Seg_8229" s="T242">воспитывать-EP-PASS/REFL-PTCP.PRS-3SG-ABL</ta>
            <ta e="T257" id="Seg_8230" s="T256">пропадать-PST2-3SG</ta>
            <ta e="T258" id="Seg_8231" s="T257">EMPH</ta>
            <ta e="T259" id="Seg_8232" s="T258">умирать-CVB.SEQ</ta>
            <ta e="T260" id="Seg_8233" s="T259">пропадать-CVB.SEQ</ta>
            <ta e="T261" id="Seg_8234" s="T260">пропадать-PST2-3SG</ta>
            <ta e="T262" id="Seg_8235" s="T261">тот</ta>
            <ta e="T263" id="Seg_8236" s="T262">кто-PL-DAT/LOC</ta>
            <ta e="T264" id="Seg_8237" s="T263">Саскылаах-PL-DAT/LOC</ta>
            <ta e="T265" id="Seg_8238" s="T264">идти-CVB.SEQ</ta>
            <ta e="T266" id="Seg_8239" s="T265">идти-CVB.SEQ</ta>
            <ta e="T267" id="Seg_8240" s="T266">может</ta>
            <ta e="T268" id="Seg_8241" s="T267">дикий.олень.[NOM]</ta>
            <ta e="T269" id="Seg_8242" s="T268">да</ta>
            <ta e="T270" id="Seg_8243" s="T269">уносить-PRS.[3SG]</ta>
            <ta e="T271" id="Seg_8244" s="T270">дикий.олень.[NOM]</ta>
            <ta e="T272" id="Seg_8245" s="T271">уносить-HAB.[3SG]</ta>
            <ta e="T273" id="Seg_8246" s="T272">миграция-3SG-DAT/LOC</ta>
            <ta e="T274" id="Seg_8247" s="T273">осенью</ta>
            <ta e="T275" id="Seg_8248" s="T274">приходить-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T276" id="Seg_8249" s="T275">вот</ta>
            <ta e="T277" id="Seg_8250" s="T276">волк.[NOM]</ta>
            <ta e="T278" id="Seg_8251" s="T277">есть-PRS.[3SG]</ta>
            <ta e="T279" id="Seg_8252" s="T278">большой</ta>
            <ta e="T280" id="Seg_8253" s="T279">очень</ta>
            <ta e="T281" id="Seg_8254" s="T280">быть.больным-PRS.[3SG]</ta>
            <ta e="T282" id="Seg_8255" s="T281">тот-DAT/LOC</ta>
            <ta e="T283" id="Seg_8256" s="T282">нога-3SG.[NOM]</ta>
            <ta e="T284" id="Seg_8257" s="T283">да</ta>
            <ta e="T285" id="Seg_8258" s="T284">сразу</ta>
            <ta e="T286" id="Seg_8259" s="T285">долго</ta>
            <ta e="T287" id="Seg_8260" s="T286">идти-PTCP.HAB-3SG</ta>
            <ta e="T288" id="Seg_8261" s="T287">NEG.[3SG]</ta>
            <ta e="T302" id="Seg_8262" s="T301">убить-CAUS-PRS-3PL</ta>
            <ta e="T303" id="Seg_8263" s="T302">недавно</ta>
            <ta e="T304" id="Seg_8264" s="T303">EMPH</ta>
            <ta e="T305" id="Seg_8265" s="T304">есть</ta>
            <ta e="T306" id="Seg_8266" s="T305">быть-TEMP-3SG</ta>
            <ta e="T307" id="Seg_8267" s="T306">говорить-PRS-3PL</ta>
            <ta e="T308" id="Seg_8268" s="T307">EMPH</ta>
            <ta e="T309" id="Seg_8269" s="T308">теперь</ta>
            <ta e="T310" id="Seg_8270" s="T309">премия-VBZ-PTCP.PRS</ta>
            <ta e="T311" id="Seg_8271" s="T310">становиться-PST2-3PL</ta>
            <ta e="T312" id="Seg_8272" s="T311">раньше</ta>
            <ta e="T313" id="Seg_8273" s="T312">премия-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T314" id="Seg_8274" s="T313">NEG-3PL</ta>
            <ta e="T316" id="Seg_8275" s="T315">быть-PST1-3PL</ta>
            <ta e="T320" id="Seg_8276" s="T319">наш-1PL.[NOM]</ta>
            <ta e="T321" id="Seg_8277" s="T320">кто-1PL.[NOM]</ta>
            <ta e="T322" id="Seg_8278" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_8279" s="T322">расценка-1PL.[NOM]</ta>
            <ta e="T324" id="Seg_8280" s="T323">плохой.[NOM]</ta>
            <ta e="T325" id="Seg_8281" s="T324">быть-PST1-3SG</ta>
            <ta e="T326" id="Seg_8282" s="T325">раньше</ta>
            <ta e="T327" id="Seg_8283" s="T326">маленький.[NOM]</ta>
            <ta e="T328" id="Seg_8284" s="T327">маленький.[NOM]</ta>
            <ta e="T329" id="Seg_8285" s="T328">быть-PST1-3SG</ta>
            <ta e="T330" id="Seg_8286" s="T329">олень-1PL.[NOM]</ta>
            <ta e="T331" id="Seg_8287" s="T330">собственный-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_8288" s="T331">вот</ta>
            <ta e="T333" id="Seg_8289" s="T332">тот-ACC</ta>
            <ta e="T334" id="Seg_8290" s="T333">1SG.[NOM]</ta>
            <ta e="T335" id="Seg_8291" s="T334">возвышаться-EP-CAUS-EP-IMP.2PL</ta>
            <ta e="T336" id="Seg_8292" s="T335">говорить-CVB.SIM</ta>
            <ta e="T337" id="Seg_8293" s="T336">делать.напрасно-CVB.SEQ</ta>
            <ta e="T338" id="Seg_8294" s="T337">теперь</ta>
            <ta e="T339" id="Seg_8295" s="T338">слово.[NOM]</ta>
            <ta e="T340" id="Seg_8296" s="T339">слышать-FUT-3PL</ta>
            <ta e="T341" id="Seg_8297" s="T340">EMPH</ta>
            <ta e="T342" id="Seg_8298" s="T341">теперь</ta>
            <ta e="T343" id="Seg_8299" s="T342">работник-PL-1PL.[NOM]</ta>
            <ta e="T344" id="Seg_8300" s="T343">плохой-PL.[NOM]</ta>
            <ta e="T345" id="Seg_8301" s="T344">тот-DAT/LOC</ta>
            <ta e="T346" id="Seg_8302" s="T345">вот</ta>
            <ta e="T353" id="Seg_8303" s="T352">мм</ta>
            <ta e="T354" id="Seg_8304" s="T353">говорить-PRS-1PL</ta>
            <ta e="T355" id="Seg_8305" s="T354">EMPH</ta>
            <ta e="T356" id="Seg_8306" s="T355">голос.[NOM]</ta>
            <ta e="T357" id="Seg_8307" s="T356">кормить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T358" id="Seg_8308" s="T357">олень.[NOM]</ta>
            <ta e="T359" id="Seg_8309" s="T358">расценка-3SG-ACC</ta>
            <ta e="T360" id="Seg_8310" s="T359">возвышаться-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T361" id="Seg_8311" s="T360">говорить-PRS-1PL</ta>
            <ta e="T364" id="Seg_8312" s="T363">их</ta>
            <ta e="T365" id="Seg_8313" s="T364">кто-3PL.[NOM]</ta>
            <ta e="T366" id="Seg_8314" s="T365">Юрюнг.Хая-PL.[NOM]</ta>
            <ta e="T367" id="Seg_8315" s="T366">собственный-3PL.[NOM]</ta>
            <ta e="T368" id="Seg_8316" s="T367">EMPH</ta>
            <ta e="T369" id="Seg_8317" s="T368">один</ta>
            <ta e="T370" id="Seg_8318" s="T369">олень-DAT/LOC</ta>
            <ta e="T371" id="Seg_8319" s="T370">сколько</ta>
            <ta e="T372" id="Seg_8320" s="T371">говорить-PST2-3PL=Q</ta>
            <ta e="T373" id="Seg_8321" s="T372">сутка-DAT/LOC</ta>
            <ta e="T374" id="Seg_8322" s="T373">четыре</ta>
            <ta e="T375" id="Seg_8323" s="T374">четыре</ta>
            <ta e="T376" id="Seg_8324" s="T375">рубль.[NOM]</ta>
            <ta e="T381" id="Seg_8325" s="T380">воспитывать-PTCP.PRS-3PL-ACC</ta>
            <ta e="T382" id="Seg_8326" s="T381">из_за</ta>
            <ta e="T383" id="Seg_8327" s="T382">EMPH</ta>
            <ta e="T384" id="Seg_8328" s="T383">хе-хе</ta>
            <ta e="T387" id="Seg_8329" s="T386">олень-VBZ-CVB.SEQ</ta>
            <ta e="T388" id="Seg_8330" s="T387">воспитывать-CVB.SEQ</ta>
            <ta e="T389" id="Seg_8331" s="T388">выйти-PTCP.PRS-3PL-ACC</ta>
            <ta e="T390" id="Seg_8332" s="T389">из_за</ta>
            <ta e="T391" id="Seg_8333" s="T390">их.[NOM]</ta>
            <ta e="T393" id="Seg_8334" s="T391">тоже</ta>
            <ta e="T394" id="Seg_8335" s="T393">пурга.[NOM]</ta>
            <ta e="T395" id="Seg_8336" s="T394">бедный.[NOM]</ta>
            <ta e="T396" id="Seg_8337" s="T395">делать-PRS.[3SG]</ta>
            <ta e="T397" id="Seg_8338" s="T396">EMPH</ta>
            <ta e="T398" id="Seg_8339" s="T397">Юрюнг.Хая-2SG.[NOM]</ta>
            <ta e="T399" id="Seg_8340" s="T398">место-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_8341" s="T399">очень</ta>
            <ta e="T401" id="Seg_8342" s="T400">бедный</ta>
            <ta e="T402" id="Seg_8343" s="T401">земля.[NOM]</ta>
            <ta e="T403" id="Seg_8344" s="T402">тот.[NOM]</ta>
            <ta e="T404" id="Seg_8345" s="T403">из_за</ta>
            <ta e="T405" id="Seg_8346" s="T404">тот</ta>
            <ta e="T406" id="Seg_8347" s="T405">расценка-PL-EP-2SG.[NOM]</ta>
            <ta e="T407" id="Seg_8348" s="T406">возвышаться-EP-CAUS-PST2-3PL</ta>
            <ta e="T408" id="Seg_8349" s="T407">сначала-ADJZ-ABL</ta>
            <ta e="T409" id="Seg_8350" s="T408">туча.насекомых-ABL</ta>
            <ta e="T410" id="Seg_8351" s="T409">прежний-ABL</ta>
            <ta e="T411" id="Seg_8352" s="T410">олень.[NOM]</ta>
            <ta e="T412" id="Seg_8353" s="T411">воспитывать-NMNZ.[NOM]</ta>
            <ta e="T413" id="Seg_8354" s="T412">очень</ta>
            <ta e="T414" id="Seg_8355" s="T413">EMPH</ta>
            <ta e="T415" id="Seg_8356" s="T414">трудный-3SG</ta>
            <ta e="T418" id="Seg_8357" s="T417">AFFIRM</ta>
            <ta e="T419" id="Seg_8358" s="T418">умирать-PTCP.PRS</ta>
            <ta e="T420" id="Seg_8359" s="T419">умирать-PTCP.PRS</ta>
            <ta e="T421" id="Seg_8360" s="T420">человек-2SG.[NOM]</ta>
            <ta e="T422" id="Seg_8361" s="T421">умирать-PRS.[3SG]</ta>
            <ta e="T423" id="Seg_8362" s="T422">тот</ta>
            <ta e="T425" id="Seg_8363" s="T424">олень-ACC</ta>
            <ta e="T426" id="Seg_8364" s="T425">с</ta>
            <ta e="T427" id="Seg_8365" s="T426">сопровождать-CVB.SEQ</ta>
            <ta e="T442" id="Seg_8366" s="T441">тот</ta>
            <ta e="T443" id="Seg_8367" s="T442">якутский</ta>
            <ta e="T444" id="Seg_8368" s="T443">место-3SG-DAT/LOC</ta>
            <ta e="T445" id="Seg_8369" s="T444">такой-PL.[NOM]</ta>
            <ta e="T446" id="Seg_8370" s="T445">быть-PST1-3SG</ta>
            <ta e="T447" id="Seg_8371" s="T446">олень-ACC</ta>
            <ta e="T448" id="Seg_8372" s="T447">очень</ta>
            <ta e="T449" id="Seg_8373" s="T448">уважать-PRS-3PL</ta>
            <ta e="T450" id="Seg_8374" s="T449">кормить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T451" id="Seg_8375" s="T450">хотеть-PRS-3PL</ta>
            <ta e="T452" id="Seg_8376" s="T451">вот.беда</ta>
            <ta e="T453" id="Seg_8377" s="T452">деньги.[NOM]</ta>
            <ta e="T454" id="Seg_8378" s="T453">взять-PRS-3PL</ta>
            <ta e="T455" id="Seg_8379" s="T454">хороший.[NOM]</ta>
            <ta e="T456" id="Seg_8380" s="T455">быть-PST1-3SG</ta>
            <ta e="T457" id="Seg_8381" s="T456">олень-AG-PL-EP-2SG.[NOM]</ta>
            <ta e="T458" id="Seg_8382" s="T457">месяц-DAT/LOC</ta>
            <ta e="T459" id="Seg_8383" s="T458">четыре-DISTR</ta>
            <ta e="T460" id="Seg_8384" s="T459">пять-DISTR</ta>
            <ta e="T461" id="Seg_8385" s="T460">тысяча-ACC</ta>
            <ta e="T462" id="Seg_8386" s="T461">один</ta>
            <ta e="T463" id="Seg_8387" s="T462">месяц-DAT/LOC</ta>
            <ta e="T464" id="Seg_8388" s="T463">взять-PRS-3PL</ta>
            <ta e="T465" id="Seg_8389" s="T464">3PL.[NOM]</ta>
            <ta e="T466" id="Seg_8390" s="T465">1PL.[NOM]</ta>
            <ta e="T467" id="Seg_8391" s="T466">тот</ta>
            <ta e="T468" id="Seg_8392" s="T467">когда</ta>
            <ta e="T471" id="Seg_8393" s="T470">такой-POSS</ta>
            <ta e="T472" id="Seg_8394" s="T471">EMPH</ta>
            <ta e="T474" id="Seg_8395" s="T472">NEG-1PL</ta>
            <ta e="T475" id="Seg_8396" s="T474">совсем</ta>
            <ta e="T477" id="Seg_8397" s="T475">NEG.EX</ta>
            <ta e="T478" id="Seg_8398" s="T477">просто</ta>
            <ta e="T479" id="Seg_8399" s="T478">пять</ta>
            <ta e="T480" id="Seg_8400" s="T479">сто</ta>
            <ta e="T481" id="Seg_8401" s="T480">наверное</ta>
            <ta e="T482" id="Seg_8402" s="T481">Q</ta>
            <ta e="T483" id="Seg_8403" s="T482">аванс.[NOM]</ta>
            <ta e="T484" id="Seg_8404" s="T483">подобно</ta>
            <ta e="T486" id="Seg_8405" s="T484">расценка-3SG-1PL.[NOM]</ta>
            <ta e="T487" id="Seg_8406" s="T486">мало-3SG.[NOM]</ta>
            <ta e="T488" id="Seg_8407" s="T487">очень</ta>
            <ta e="T489" id="Seg_8408" s="T488">копейки.[NOM]</ta>
            <ta e="T490" id="Seg_8409" s="T489">теперь-DAT/LOC</ta>
            <ta e="T491" id="Seg_8410" s="T490">пока</ta>
            <ta e="T492" id="Seg_8411" s="T491">конец-EP-ABL</ta>
            <ta e="T493" id="Seg_8412" s="T492">потерять-PST1-3PL</ta>
            <ta e="T494" id="Seg_8413" s="T493">теперь</ta>
            <ta e="T495" id="Seg_8414" s="T494">что-3SG-ACC</ta>
            <ta e="T496" id="Seg_8415" s="T495">EMPH</ta>
            <ta e="T497" id="Seg_8416" s="T496">говорить-CVB.SIM</ta>
            <ta e="T498" id="Seg_8417" s="T497">делать.напрасно-CVB.SEQ</ta>
            <ta e="T499" id="Seg_8418" s="T498">почему</ta>
            <ta e="T500" id="Seg_8419" s="T499">трогать-PRS-2PL</ta>
            <ta e="T501" id="Seg_8420" s="T500">тот.[NOM]</ta>
            <ta e="T502" id="Seg_8421" s="T501">что.[NOM]</ta>
            <ta e="T503" id="Seg_8422" s="T502">делать-CVB.PURP</ta>
            <ta e="T504" id="Seg_8423" s="T503">совхоз-2PL.[NOM]</ta>
            <ta e="T505" id="Seg_8424" s="T504">совхоз-2PL.[NOM]</ta>
            <ta e="T506" id="Seg_8425" s="T505">олень-3SG.[NOM]</ta>
            <ta e="T507" id="Seg_8426" s="T506">сам-2PL.[NOM]</ta>
            <ta e="T508" id="Seg_8427" s="T507">есть-PRS-2PL</ta>
            <ta e="T509" id="Seg_8428" s="T508">EMPH</ta>
            <ta e="T510" id="Seg_8429" s="T509">говорить-HAB-1SG</ta>
            <ta e="T511" id="Seg_8430" s="T510">одеваться-PRS-2PL</ta>
            <ta e="T512" id="Seg_8431" s="T511">и.так.далее-PRS-2PL</ta>
            <ta e="T513" id="Seg_8432" s="T512">почему</ta>
            <ta e="T514" id="Seg_8433" s="T513">совхоз.[NOM]</ta>
            <ta e="T515" id="Seg_8434" s="T514">собственный-3SG.[NOM]</ta>
            <ta e="T516" id="Seg_8435" s="T515">говорить-PRS-2PL</ta>
            <ta e="T517" id="Seg_8436" s="T516">говорить-PRS-1SG</ta>
            <ta e="T518" id="Seg_8437" s="T517">сам-2PL.[NOM]</ta>
            <ta e="T519" id="Seg_8438" s="T518">богатство-2PL.[NOM]</ta>
            <ta e="T520" id="Seg_8439" s="T519">EMPH</ta>
            <ta e="T521" id="Seg_8440" s="T520">говорить-PRS-1SG</ta>
            <ta e="T522" id="Seg_8441" s="T521">тот.[NOM]</ta>
            <ta e="T523" id="Seg_8442" s="T522">из_за</ta>
            <ta e="T524" id="Seg_8443" s="T523">деньги.[NOM]</ta>
            <ta e="T525" id="Seg_8444" s="T524">получить-PRS-2PL</ta>
            <ta e="T526" id="Seg_8445" s="T525">что-ACC=Q</ta>
            <ta e="T527" id="Seg_8446" s="T526">что-EP-INSTR</ta>
            <ta e="T530" id="Seg_8447" s="T529">взять-FUT-1PL</ta>
            <ta e="T531" id="Seg_8448" s="T530">думать-PRS-2PL</ta>
            <ta e="T532" id="Seg_8449" s="T531">говорить-PRS-1SG</ta>
            <ta e="T533" id="Seg_8450" s="T532">грех.[NOM]</ta>
            <ta e="T534" id="Seg_8451" s="T533">быть-FUT.[3SG]</ta>
            <ta e="T535" id="Seg_8452" s="T534">большой-ADVZ</ta>
            <ta e="T536" id="Seg_8453" s="T535">обман-VBZ-PST2-EP-1SG</ta>
            <ta e="T537" id="Seg_8454" s="T536">говорить-TEMP-1SG</ta>
            <ta e="T538" id="Seg_8455" s="T537">продавать-EP-MED-PST2.NEG-EP-1SG</ta>
            <ta e="T539" id="Seg_8456" s="T538">да</ta>
            <ta e="T540" id="Seg_8457" s="T539">хоть</ta>
            <ta e="T541" id="Seg_8458" s="T540">полностью</ta>
            <ta e="T542" id="Seg_8459" s="T541">человек-2SG.[NOM]</ta>
            <ta e="T543" id="Seg_8460" s="T542">каждый-3SG.[NOM]</ta>
            <ta e="T544" id="Seg_8461" s="T543">продавать-EP-MED-PST2-3SG</ta>
            <ta e="T545" id="Seg_8462" s="T544">тот</ta>
            <ta e="T546" id="Seg_8463" s="T545">совхоз-ABL</ta>
            <ta e="T550" id="Seg_8464" s="T549">деньги.[NOM]</ta>
            <ta e="T551" id="Seg_8465" s="T550">делать-PRS-3PL</ta>
            <ta e="T572" id="Seg_8466" s="T571">такой.[NOM]</ta>
            <ta e="T573" id="Seg_8467" s="T572">такой.[NOM]</ta>
            <ta e="T581" id="Seg_8468" s="T580">олень-AG.[NOM]</ta>
            <ta e="T582" id="Seg_8469" s="T581">человек.[NOM]</ta>
            <ta e="T583" id="Seg_8470" s="T582">недостаточный.[NOM]</ta>
            <ta e="T584" id="Seg_8471" s="T583">вот</ta>
            <ta e="T585" id="Seg_8472" s="T584">тот-ACC</ta>
            <ta e="T613" id="Seg_8473" s="T612">тот</ta>
            <ta e="T614" id="Seg_8474" s="T613">хороший-ADVZ</ta>
            <ta e="T615" id="Seg_8475" s="T614">работать-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T616" id="Seg_8476" s="T615">из_за</ta>
            <ta e="T617" id="Seg_8477" s="T616">выйти-EP-PST2-EP-1SG</ta>
            <ta e="T618" id="Seg_8478" s="T617">человек-PL-ACC</ta>
            <ta e="T619" id="Seg_8479" s="T618">с</ta>
            <ta e="T620" id="Seg_8480" s="T619">разговаривать-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T621" id="Seg_8481" s="T620">из_за</ta>
            <ta e="T622" id="Seg_8482" s="T621">лучше</ta>
            <ta e="T623" id="Seg_8483" s="T622">молодой.[NOM]</ta>
            <ta e="T624" id="Seg_8484" s="T623">быть-TEMP-1SG</ta>
            <ta e="T625" id="Seg_8485" s="T624">очень</ta>
            <ta e="T626" id="Seg_8486" s="T625">шустрый.[NOM]</ta>
            <ta e="T627" id="Seg_8487" s="T626">быть-PST1-1SG</ta>
            <ta e="T628" id="Seg_8488" s="T627">EMPH</ta>
            <ta e="T629" id="Seg_8489" s="T628">быть.больным-EP-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T630" id="Seg_8490" s="T629">да</ta>
            <ta e="T631" id="Seg_8491" s="T630">из_за</ta>
            <ta e="T632" id="Seg_8492" s="T631">разговаривать-NMNZ.[NOM]</ta>
            <ta e="T633" id="Seg_8493" s="T632">остатки-PROPR.[NOM]</ta>
            <ta e="T634" id="Seg_8494" s="T633">быть-PST1-1SG</ta>
            <ta e="T635" id="Seg_8495" s="T634">русский.[NOM]</ta>
            <ta e="T636" id="Seg_8496" s="T635">да</ta>
            <ta e="T637" id="Seg_8497" s="T636">может</ta>
            <ta e="T638" id="Seg_8498" s="T637">долганин.[NOM]</ta>
            <ta e="T639" id="Seg_8499" s="T638">да</ta>
            <ta e="T640" id="Seg_8500" s="T639">может</ta>
            <ta e="T641" id="Seg_8501" s="T640">чужой.[NOM]</ta>
            <ta e="T642" id="Seg_8502" s="T641">да</ta>
            <ta e="T643" id="Seg_8503" s="T642">может</ta>
            <ta e="T653" id="Seg_8504" s="T652">день.[NOM]</ta>
            <ta e="T654" id="Seg_8505" s="T653">каждый</ta>
            <ta e="T655" id="Seg_8506" s="T654">EMPH</ta>
            <ta e="T656" id="Seg_8507" s="T655">видеть-PRS-3PL</ta>
            <ta e="T657" id="Seg_8508" s="T656">EMPH</ta>
            <ta e="T658" id="Seg_8509" s="T657">ошибка-ACC</ta>
            <ta e="T659" id="Seg_8510" s="T658">что-3PL.[NOM]</ta>
            <ta e="T661" id="Seg_8511" s="T660">делать-PRS-2SG</ta>
            <ta e="T662" id="Seg_8512" s="T661">очень</ta>
            <ta e="T663" id="Seg_8513" s="T662">ошибка-POSS</ta>
            <ta e="T664" id="Seg_8514" s="T663">NEG-1SG</ta>
            <ta e="T665" id="Seg_8515" s="T664">EMPH</ta>
            <ta e="T666" id="Seg_8516" s="T665">тот.[NOM]</ta>
            <ta e="T667" id="Seg_8517" s="T666">из_за</ta>
            <ta e="T669" id="Seg_8518" s="T667">русский-PL.[NOM]</ta>
            <ta e="T694" id="Seg_8519" s="T693">родить-PTCP.PST-3SG-ACC</ta>
            <ta e="T695" id="Seg_8520" s="T694">рассказывать-PRS-2SG</ta>
            <ta e="T696" id="Seg_8521" s="T695">EMPH</ta>
            <ta e="T697" id="Seg_8522" s="T696">порука-3SG-ACC</ta>
            <ta e="T698" id="Seg_8523" s="T697">теленок-EP-2SG.[NOM]</ta>
            <ta e="T699" id="Seg_8524" s="T698">крошечный.[NOM]</ta>
            <ta e="T700" id="Seg_8525" s="T699">EMPH</ta>
            <ta e="T701" id="Seg_8526" s="T700">быть-PST2.[3SG]</ta>
            <ta e="T702" id="Seg_8527" s="T701">ночь-ACC</ta>
            <ta e="T703" id="Seg_8528" s="T702">в.течение</ta>
            <ta e="T704" id="Seg_8529" s="T703">спать-NEG.CVB.SIM</ta>
            <ta e="T705" id="Seg_8530" s="T704">идти-PRS-2SG</ta>
            <ta e="T706" id="Seg_8531" s="T705">EMPH</ta>
            <ta e="T707" id="Seg_8532" s="T706">тот</ta>
            <ta e="T708" id="Seg_8533" s="T707">олененок.[NOM]</ta>
            <ta e="T709" id="Seg_8534" s="T708">родить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T710" id="Seg_8535" s="T709">охранять-CVB.SIM</ta>
            <ta e="T711" id="Seg_8536" s="T710">много</ta>
            <ta e="T712" id="Seg_8537" s="T711">олень-DAT/LOC</ta>
            <ta e="T715" id="Seg_8538" s="T714">много</ta>
            <ta e="T716" id="Seg_8539" s="T715">олененок.[NOM]</ta>
            <ta e="T717" id="Seg_8540" s="T716">EMPH</ta>
         </annotation>
         <annotation name="mc" tierref="mc-ChSA">
            <ta e="T7" id="Seg_8541" s="T6">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T8" id="Seg_8542" s="T7">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T9" id="Seg_8543" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_8544" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_8545" s="T10">adj-n:(num)-n:case</ta>
            <ta e="T12" id="Seg_8546" s="T11">v-v:tense-v:poss.pn</ta>
            <ta e="T13" id="Seg_8547" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_8548" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_8549" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_8550" s="T15">v-v:(neg)-v:pred.pn</ta>
            <ta e="T19" id="Seg_8551" s="T18">adv</ta>
            <ta e="T22" id="Seg_8552" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_8553" s="T22">v-v:tense-v:poss.pn</ta>
            <ta e="T24" id="Seg_8554" s="T23">dempro</ta>
            <ta e="T25" id="Seg_8555" s="T24">v-v:mood-v:pred.pn</ta>
            <ta e="T26" id="Seg_8556" s="T25">pers-pro:case</ta>
            <ta e="T27" id="Seg_8557" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_8558" s="T27">adv&gt;adv-adv</ta>
            <ta e="T29" id="Seg_8559" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_8560" s="T29">dempro-pro:case</ta>
            <ta e="T31" id="Seg_8561" s="T30">post</ta>
            <ta e="T32" id="Seg_8562" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_8563" s="T32">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T34" id="Seg_8564" s="T33">v-v:tense-v:poss.pn</ta>
            <ta e="T35" id="Seg_8565" s="T34">adv</ta>
            <ta e="T36" id="Seg_8566" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_8567" s="T36">post</ta>
            <ta e="T38" id="Seg_8568" s="T37">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T39" id="Seg_8569" s="T38">adv</ta>
            <ta e="T40" id="Seg_8570" s="T39">dempro-pro:case</ta>
            <ta e="T41" id="Seg_8571" s="T40">post</ta>
            <ta e="T42" id="Seg_8572" s="T41">adj-n:(pred.pn)</ta>
            <ta e="T43" id="Seg_8573" s="T42">quant</ta>
            <ta e="T44" id="Seg_8574" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_8575" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_8576" s="T45">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T47" id="Seg_8577" s="T46">post</ta>
            <ta e="T48" id="Seg_8578" s="T47">v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_8579" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_8580" s="T49">n-n:poss-n:case</ta>
            <ta e="T51" id="Seg_8581" s="T50">conj</ta>
            <ta e="T52" id="Seg_8582" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_8583" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_8584" s="T53">adj-n:case</ta>
            <ta e="T55" id="Seg_8585" s="T54">dempro</ta>
            <ta e="T56" id="Seg_8586" s="T55">n-n:(num)-n:poss-n:case</ta>
            <ta e="T57" id="Seg_8587" s="T56">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T58" id="Seg_8588" s="T57">dempro</ta>
            <ta e="T59" id="Seg_8589" s="T58">que-pro:case</ta>
            <ta e="T61" id="Seg_8590" s="T59">adv</ta>
            <ta e="T62" id="Seg_8591" s="T61">n-n:poss-n:case</ta>
            <ta e="T63" id="Seg_8592" s="T62">v-v:cvb</ta>
            <ta e="T64" id="Seg_8593" s="T63">v-v:(ins)-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T65" id="Seg_8594" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_8595" s="T65">n-n:(poss)-n:case</ta>
            <ta e="T67" id="Seg_8596" s="T66">adj-n:case</ta>
            <ta e="T68" id="Seg_8597" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_8598" s="T68">adv</ta>
            <ta e="T70" id="Seg_8599" s="T69">quant-n:(poss)-n:case</ta>
            <ta e="T71" id="Seg_8600" s="T70">conj</ta>
            <ta e="T72" id="Seg_8601" s="T71">adj-n:(poss)-n:case</ta>
            <ta e="T73" id="Seg_8602" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_8603" s="T73">n-n:(poss)-n:case</ta>
            <ta e="T75" id="Seg_8604" s="T74">v-v&gt;v-v:cvb</ta>
            <ta e="T76" id="Seg_8605" s="T75">v-v:cvb</ta>
            <ta e="T77" id="Seg_8606" s="T76">v-v:mood-v:pred.pn</ta>
            <ta e="T78" id="Seg_8607" s="T77">adv</ta>
            <ta e="T79" id="Seg_8608" s="T78">cardnum</ta>
            <ta e="T80" id="Seg_8609" s="T79">cardnum-n:(num)-n:case</ta>
            <ta e="T81" id="Seg_8610" s="T80">v-v:mood-v:pred.pn</ta>
            <ta e="T82" id="Seg_8611" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_8612" s="T82">adv</ta>
            <ta e="T84" id="Seg_8613" s="T83">interj</ta>
            <ta e="T85" id="Seg_8614" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_8615" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_8616" s="T86">ptcl</ta>
            <ta e="T90" id="Seg_8617" s="T89">propr-n:case</ta>
            <ta e="T91" id="Seg_8618" s="T90">n-n:poss-n:case</ta>
            <ta e="T92" id="Seg_8619" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_8620" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_8621" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_8622" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_8623" s="T95">adv</ta>
            <ta e="T97" id="Seg_8624" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_8625" s="T97">que</ta>
            <ta e="T99" id="Seg_8626" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_8627" s="T99">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T101" id="Seg_8628" s="T100">dempro-pro:(num)-pro:case</ta>
            <ta e="T102" id="Seg_8629" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_8630" s="T102">n-n&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T104" id="Seg_8631" s="T103">pers-pro:case</ta>
            <ta e="T105" id="Seg_8632" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_8633" s="T105">dempro-pro:case</ta>
            <ta e="T107" id="Seg_8634" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_8635" s="T107">adj-n:case</ta>
            <ta e="T109" id="Seg_8636" s="T108">v-v:cvb-v:pred.pn</ta>
            <ta e="T110" id="Seg_8637" s="T109">n-n:(poss)-n:case</ta>
            <ta e="T111" id="Seg_8638" s="T110">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T112" id="Seg_8639" s="T111">adv</ta>
            <ta e="T113" id="Seg_8640" s="T112">cardnum-n:case</ta>
            <ta e="T114" id="Seg_8641" s="T113">postp</ta>
            <ta e="T115" id="Seg_8642" s="T114">cardnum-n:case</ta>
            <ta e="T116" id="Seg_8643" s="T115">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T117" id="Seg_8644" s="T116">que</ta>
            <ta e="T118" id="Seg_8645" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_8646" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_8647" s="T119">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T121" id="Seg_8648" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_8649" s="T121">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T130" id="Seg_8650" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_8651" s="T130">adv</ta>
            <ta e="T132" id="Seg_8652" s="T131">adv-n:case</ta>
            <ta e="T133" id="Seg_8653" s="T132">post</ta>
            <ta e="T134" id="Seg_8654" s="T133">dempro</ta>
            <ta e="T135" id="Seg_8655" s="T134">adv</ta>
            <ta e="T136" id="Seg_8656" s="T135">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T137" id="Seg_8657" s="T136">dempro-pro:case</ta>
            <ta e="T138" id="Seg_8658" s="T137">v-v:mood-v:pred.pn</ta>
            <ta e="T139" id="Seg_8659" s="T138">propr-n:(poss)-n:case</ta>
            <ta e="T140" id="Seg_8660" s="T139">adj</ta>
            <ta e="T141" id="Seg_8661" s="T140">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T148" id="Seg_8662" s="T147">interj</ta>
            <ta e="T149" id="Seg_8663" s="T148">v-v:tense-v:pred.pn</ta>
            <ta e="T150" id="Seg_8664" s="T149">cardnum-cardnum&gt;collnum-n:(poss)-n:case</ta>
            <ta e="T151" id="Seg_8665" s="T150">v-v:tense-v:poss.pn</ta>
            <ta e="T167" id="Seg_8666" s="T166">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T168" id="Seg_8667" s="T167">quant</ta>
            <ta e="T169" id="Seg_8668" s="T168">n-n:(num)-n:case</ta>
            <ta e="T170" id="Seg_8669" s="T169">v-v:tense-v:poss.pn</ta>
            <ta e="T171" id="Seg_8670" s="T170">adv</ta>
            <ta e="T174" id="Seg_8671" s="T173">v-v:cvb</ta>
            <ta e="T175" id="Seg_8672" s="T174">v-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T176" id="Seg_8673" s="T175">adv&gt;adv-adv</ta>
            <ta e="T177" id="Seg_8674" s="T176">v-v:cvb</ta>
            <ta e="T178" id="Seg_8675" s="T177">v-v:mood-v:poss.pn</ta>
            <ta e="T179" id="Seg_8676" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_8677" s="T179">pers-pro:case</ta>
            <ta e="T181" id="Seg_8678" s="T180">dempro</ta>
            <ta e="T182" id="Seg_8679" s="T181">v-v&gt;n-n:case</ta>
            <ta e="T183" id="Seg_8680" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_8681" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_8682" s="T184">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T186" id="Seg_8683" s="T185">v-v:cvb</ta>
            <ta e="T187" id="Seg_8684" s="T186">v-v:mood-v:pred.pn</ta>
            <ta e="T188" id="Seg_8685" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_8686" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_8687" s="T189">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T191" id="Seg_8688" s="T190">n-n:poss-n:case</ta>
            <ta e="T192" id="Seg_8689" s="T191">v-v:mood.pn</ta>
            <ta e="T193" id="Seg_8690" s="T192">v-v:mood-v:pred.pn</ta>
            <ta e="T194" id="Seg_8691" s="T193">adv</ta>
            <ta e="T195" id="Seg_8692" s="T194">n-n&gt;v-v:cvb</ta>
            <ta e="T196" id="Seg_8693" s="T195">v-v:(ins)-v:mood.pn</ta>
            <ta e="T197" id="Seg_8694" s="T196">v-v:mood-v:pred.pn</ta>
            <ta e="T198" id="Seg_8695" s="T197">que</ta>
            <ta e="T199" id="Seg_8696" s="T198">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T200" id="Seg_8697" s="T199">que</ta>
            <ta e="T201" id="Seg_8698" s="T200">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T202" id="Seg_8699" s="T201">n-n:(poss)-n:case</ta>
            <ta e="T203" id="Seg_8700" s="T202">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T204" id="Seg_8701" s="T203">quant-n:(poss)</ta>
            <ta e="T205" id="Seg_8702" s="T204">n-n:(poss)-n:case</ta>
            <ta e="T206" id="Seg_8703" s="T205">n-n:case</ta>
            <ta e="T207" id="Seg_8704" s="T206">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T208" id="Seg_8705" s="T207">adv</ta>
            <ta e="T209" id="Seg_8706" s="T208">adv</ta>
            <ta e="T210" id="Seg_8707" s="T209">n-n&gt;v-v&gt;v-v:cvb</ta>
            <ta e="T211" id="Seg_8708" s="T210">v-v:mood-v:pred.pn</ta>
            <ta e="T212" id="Seg_8709" s="T211">dempro</ta>
            <ta e="T213" id="Seg_8710" s="T212">adv</ta>
            <ta e="T214" id="Seg_8711" s="T213">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T215" id="Seg_8712" s="T214">post</ta>
            <ta e="T217" id="Seg_8713" s="T215">ptcl</ta>
            <ta e="T218" id="Seg_8714" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_8715" s="T218">adv</ta>
            <ta e="T220" id="Seg_8716" s="T219">adj</ta>
            <ta e="T221" id="Seg_8717" s="T220">n-n:(ins)-n:case</ta>
            <ta e="T222" id="Seg_8718" s="T221">v-v:tense-v:poss.pn</ta>
            <ta e="T223" id="Seg_8719" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_8720" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_8721" s="T224">adv</ta>
            <ta e="T226" id="Seg_8722" s="T225">conj</ta>
            <ta e="T227" id="Seg_8723" s="T226">adv</ta>
            <ta e="T228" id="Seg_8724" s="T227">conj</ta>
            <ta e="T229" id="Seg_8725" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_8726" s="T229">v-v:ptcp-v:(case)</ta>
            <ta e="T231" id="Seg_8727" s="T230">quant</ta>
            <ta e="T232" id="Seg_8728" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_8729" s="T232">v-v:tense-v:poss.pn</ta>
            <ta e="T234" id="Seg_8730" s="T233">cardnum-cardnum</ta>
            <ta e="T235" id="Seg_8731" s="T234">cardnum</ta>
            <ta e="T236" id="Seg_8732" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_8733" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_8734" s="T237">v-v:cvb</ta>
            <ta e="T239" id="Seg_8735" s="T238">post</ta>
            <ta e="T240" id="Seg_8736" s="T239">adv</ta>
            <ta e="T241" id="Seg_8737" s="T240">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T242" id="Seg_8738" s="T241">n-n:(poss)-n:case</ta>
            <ta e="T243" id="Seg_8739" s="T242">v-v:(ins)-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T257" id="Seg_8740" s="T256">v-v:tense-v:poss.pn</ta>
            <ta e="T258" id="Seg_8741" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_8742" s="T258">v-v:cvb</ta>
            <ta e="T260" id="Seg_8743" s="T259">v-v:cvb</ta>
            <ta e="T261" id="Seg_8744" s="T260">v-v:tense-v:poss.pn</ta>
            <ta e="T262" id="Seg_8745" s="T261">dempro</ta>
            <ta e="T263" id="Seg_8746" s="T262">que-pro:(num)-pro:case</ta>
            <ta e="T264" id="Seg_8747" s="T263">propr-n:(num)-n:case</ta>
            <ta e="T265" id="Seg_8748" s="T264">v-v:cvb</ta>
            <ta e="T266" id="Seg_8749" s="T265">v-v:cvb</ta>
            <ta e="T267" id="Seg_8750" s="T266">adv</ta>
            <ta e="T268" id="Seg_8751" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_8752" s="T268">conj</ta>
            <ta e="T270" id="Seg_8753" s="T269">v-v:tense-v:pred.pn</ta>
            <ta e="T271" id="Seg_8754" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_8755" s="T271">v-v:mood-v:pred.pn</ta>
            <ta e="T273" id="Seg_8756" s="T272">n-n:poss-n:case</ta>
            <ta e="T274" id="Seg_8757" s="T273">adv</ta>
            <ta e="T275" id="Seg_8758" s="T274">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T276" id="Seg_8759" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_8760" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_8761" s="T277">v-v:tense-v:pred.pn</ta>
            <ta e="T279" id="Seg_8762" s="T278">adj</ta>
            <ta e="T280" id="Seg_8763" s="T279">adv</ta>
            <ta e="T281" id="Seg_8764" s="T280">v-v:tense-v:pred.pn</ta>
            <ta e="T282" id="Seg_8765" s="T281">dempro-pro:case</ta>
            <ta e="T283" id="Seg_8766" s="T282">n-n:(poss)-n:case</ta>
            <ta e="T284" id="Seg_8767" s="T283">conj</ta>
            <ta e="T285" id="Seg_8768" s="T284">adv</ta>
            <ta e="T286" id="Seg_8769" s="T285">adv</ta>
            <ta e="T287" id="Seg_8770" s="T286">v-v:ptcp-v:(poss)</ta>
            <ta e="T288" id="Seg_8771" s="T287">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T302" id="Seg_8772" s="T301">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T303" id="Seg_8773" s="T302">adv</ta>
            <ta e="T304" id="Seg_8774" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_8775" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_8776" s="T305">v-v:mood-v:temp.pn</ta>
            <ta e="T307" id="Seg_8777" s="T306">v-v:tense-v:pred.pn</ta>
            <ta e="T308" id="Seg_8778" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_8779" s="T308">adv</ta>
            <ta e="T310" id="Seg_8780" s="T309">n-n&gt;v-v:ptcp</ta>
            <ta e="T311" id="Seg_8781" s="T310">v-v:tense-v:pred.pn</ta>
            <ta e="T312" id="Seg_8782" s="T311">adv</ta>
            <ta e="T313" id="Seg_8783" s="T312">n-n&gt;v-v:ptcp-v:(poss)</ta>
            <ta e="T314" id="Seg_8784" s="T313">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T316" id="Seg_8785" s="T315">v-v:tense-v:pred.pn</ta>
            <ta e="T320" id="Seg_8786" s="T319">posspr-pro:(poss)-pro:case</ta>
            <ta e="T321" id="Seg_8787" s="T320">que-pro:(poss)-pro:case</ta>
            <ta e="T322" id="Seg_8788" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_8789" s="T322">n-n:(poss)-n:case</ta>
            <ta e="T324" id="Seg_8790" s="T323">adj-n:case</ta>
            <ta e="T325" id="Seg_8791" s="T324">v-v:tense-v:poss.pn</ta>
            <ta e="T326" id="Seg_8792" s="T325">adv</ta>
            <ta e="T327" id="Seg_8793" s="T326">adj-n:case</ta>
            <ta e="T328" id="Seg_8794" s="T327">adj-n:case</ta>
            <ta e="T329" id="Seg_8795" s="T328">v-v:tense-v:poss.pn</ta>
            <ta e="T330" id="Seg_8796" s="T329">n-n:(poss)-n:case</ta>
            <ta e="T331" id="Seg_8797" s="T330">adj-n:(poss)-n:case</ta>
            <ta e="T332" id="Seg_8798" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_8799" s="T332">dempro-pro:case</ta>
            <ta e="T334" id="Seg_8800" s="T333">pers-pro:case</ta>
            <ta e="T335" id="Seg_8801" s="T334">v-v:(ins)-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T336" id="Seg_8802" s="T335">v-v:cvb</ta>
            <ta e="T337" id="Seg_8803" s="T336">v-v:cvb</ta>
            <ta e="T338" id="Seg_8804" s="T337">adv</ta>
            <ta e="T339" id="Seg_8805" s="T338">n-n:case</ta>
            <ta e="T340" id="Seg_8806" s="T339">v-v:tense-v:(poss)</ta>
            <ta e="T341" id="Seg_8807" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_8808" s="T341">adv</ta>
            <ta e="T343" id="Seg_8809" s="T342">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T344" id="Seg_8810" s="T343">adj-n:(num)-n:case</ta>
            <ta e="T345" id="Seg_8811" s="T344">dempro-pro:case</ta>
            <ta e="T346" id="Seg_8812" s="T345">ptcl</ta>
            <ta e="T353" id="Seg_8813" s="T352">interj</ta>
            <ta e="T354" id="Seg_8814" s="T353">v-v:tense-v:pred.pn</ta>
            <ta e="T355" id="Seg_8815" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_8816" s="T355">n-n:case</ta>
            <ta e="T357" id="Seg_8817" s="T356">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T358" id="Seg_8818" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_8819" s="T358">n-n:poss-n:case</ta>
            <ta e="T360" id="Seg_8820" s="T359">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T361" id="Seg_8821" s="T360">v-v:tense-v:pred.pn</ta>
            <ta e="T364" id="Seg_8822" s="T363">posspr</ta>
            <ta e="T365" id="Seg_8823" s="T364">que-pro:(poss)-pro:case</ta>
            <ta e="T366" id="Seg_8824" s="T365">propr-n:(num)-n:case</ta>
            <ta e="T367" id="Seg_8825" s="T366">adj-n:(poss)-n:case</ta>
            <ta e="T368" id="Seg_8826" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_8827" s="T368">cardnum</ta>
            <ta e="T370" id="Seg_8828" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_8829" s="T370">que</ta>
            <ta e="T372" id="Seg_8830" s="T371">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T373" id="Seg_8831" s="T372">n-n:case</ta>
            <ta e="T374" id="Seg_8832" s="T373">cardnum</ta>
            <ta e="T375" id="Seg_8833" s="T374">cardnum</ta>
            <ta e="T376" id="Seg_8834" s="T375">n-n:case</ta>
            <ta e="T381" id="Seg_8835" s="T380">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T382" id="Seg_8836" s="T381">post</ta>
            <ta e="T383" id="Seg_8837" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_8838" s="T383">interj</ta>
            <ta e="T387" id="Seg_8839" s="T386">n-n&gt;v-v:cvb</ta>
            <ta e="T388" id="Seg_8840" s="T387">v-v:cvb</ta>
            <ta e="T389" id="Seg_8841" s="T388">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T390" id="Seg_8842" s="T389">post</ta>
            <ta e="T391" id="Seg_8843" s="T390">posspr-pro:case</ta>
            <ta e="T393" id="Seg_8844" s="T391">ptcl</ta>
            <ta e="T394" id="Seg_8845" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_8846" s="T394">adj-n:case</ta>
            <ta e="T396" id="Seg_8847" s="T395">v-v:tense-v:pred.pn</ta>
            <ta e="T397" id="Seg_8848" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_8849" s="T397">propr-n:(poss)-n:case</ta>
            <ta e="T399" id="Seg_8850" s="T398">n-n:(poss)-n:case</ta>
            <ta e="T400" id="Seg_8851" s="T399">adv</ta>
            <ta e="T401" id="Seg_8852" s="T400">adj</ta>
            <ta e="T402" id="Seg_8853" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_8854" s="T402">dempro-pro:case</ta>
            <ta e="T404" id="Seg_8855" s="T403">post</ta>
            <ta e="T405" id="Seg_8856" s="T404">dempro</ta>
            <ta e="T406" id="Seg_8857" s="T405">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T407" id="Seg_8858" s="T406">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T408" id="Seg_8859" s="T407">adv-adv&gt;adj-n:case</ta>
            <ta e="T409" id="Seg_8860" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_8861" s="T409">adj-n:case</ta>
            <ta e="T411" id="Seg_8862" s="T410">n-n:case</ta>
            <ta e="T412" id="Seg_8863" s="T411">v-v&gt;n-n:case</ta>
            <ta e="T413" id="Seg_8864" s="T412">adv</ta>
            <ta e="T414" id="Seg_8865" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_8866" s="T414">adj-n:poss.pn</ta>
            <ta e="T418" id="Seg_8867" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_8868" s="T418">v-v:ptcp</ta>
            <ta e="T420" id="Seg_8869" s="T419">v-v:ptcp</ta>
            <ta e="T421" id="Seg_8870" s="T420">n-n:(poss)-n:case</ta>
            <ta e="T422" id="Seg_8871" s="T421">v-v:tense-v:pred.pn</ta>
            <ta e="T423" id="Seg_8872" s="T422">dempro</ta>
            <ta e="T425" id="Seg_8873" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_8874" s="T425">post</ta>
            <ta e="T427" id="Seg_8875" s="T426">v-v:cvb</ta>
            <ta e="T442" id="Seg_8876" s="T441">dempro</ta>
            <ta e="T443" id="Seg_8877" s="T442">adj</ta>
            <ta e="T444" id="Seg_8878" s="T443">n-n:poss-n:case</ta>
            <ta e="T445" id="Seg_8879" s="T444">dempro-pro:(num)-pro:case</ta>
            <ta e="T446" id="Seg_8880" s="T445">v-v:tense-v:poss.pn</ta>
            <ta e="T447" id="Seg_8881" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_8882" s="T447">adv</ta>
            <ta e="T449" id="Seg_8883" s="T448">v-v:tense-v:pred.pn</ta>
            <ta e="T450" id="Seg_8884" s="T449">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T451" id="Seg_8885" s="T450">v-v:tense-v:pred.pn</ta>
            <ta e="T452" id="Seg_8886" s="T451">interj</ta>
            <ta e="T453" id="Seg_8887" s="T452">n-n:case</ta>
            <ta e="T454" id="Seg_8888" s="T453">v-v:tense-v:pred.pn</ta>
            <ta e="T455" id="Seg_8889" s="T454">adj-n:case</ta>
            <ta e="T456" id="Seg_8890" s="T455">v-v:tense-v:poss.pn</ta>
            <ta e="T457" id="Seg_8891" s="T456">n-n&gt;n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T458" id="Seg_8892" s="T457">n-n:case</ta>
            <ta e="T459" id="Seg_8893" s="T458">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T460" id="Seg_8894" s="T459">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T461" id="Seg_8895" s="T460">cardnum-n:case</ta>
            <ta e="T462" id="Seg_8896" s="T461">cardnum</ta>
            <ta e="T463" id="Seg_8897" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_8898" s="T463">v-v:tense-v:pred.pn</ta>
            <ta e="T465" id="Seg_8899" s="T464">pers-pro:case</ta>
            <ta e="T466" id="Seg_8900" s="T465">pers-pro:case</ta>
            <ta e="T467" id="Seg_8901" s="T466">dempro</ta>
            <ta e="T468" id="Seg_8902" s="T467">que</ta>
            <ta e="T471" id="Seg_8903" s="T470">dempro-pro:(poss)</ta>
            <ta e="T472" id="Seg_8904" s="T471">ptcl</ta>
            <ta e="T474" id="Seg_8905" s="T472">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T475" id="Seg_8906" s="T474">adv</ta>
            <ta e="T477" id="Seg_8907" s="T475">ptcl</ta>
            <ta e="T478" id="Seg_8908" s="T477">adv</ta>
            <ta e="T479" id="Seg_8909" s="T478">cardnum</ta>
            <ta e="T480" id="Seg_8910" s="T479">cardnum</ta>
            <ta e="T481" id="Seg_8911" s="T480">adv</ta>
            <ta e="T482" id="Seg_8912" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_8913" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_8914" s="T483">post</ta>
            <ta e="T486" id="Seg_8915" s="T484">n-n:(poss)-n:(poss)-n:case</ta>
            <ta e="T487" id="Seg_8916" s="T486">quant-n:(poss)-n:case</ta>
            <ta e="T488" id="Seg_8917" s="T487">adv</ta>
            <ta e="T489" id="Seg_8918" s="T488">n-n:case</ta>
            <ta e="T490" id="Seg_8919" s="T489">adv-n:case</ta>
            <ta e="T491" id="Seg_8920" s="T490">post</ta>
            <ta e="T492" id="Seg_8921" s="T491">n-n:(ins)-n:case</ta>
            <ta e="T493" id="Seg_8922" s="T492">v-v:tense-v:pred.pn</ta>
            <ta e="T494" id="Seg_8923" s="T493">adv</ta>
            <ta e="T495" id="Seg_8924" s="T494">que-pro:(poss)-pro:case</ta>
            <ta e="T496" id="Seg_8925" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_8926" s="T496">v-v:cvb</ta>
            <ta e="T498" id="Seg_8927" s="T497">v-v:cvb</ta>
            <ta e="T499" id="Seg_8928" s="T498">que</ta>
            <ta e="T500" id="Seg_8929" s="T499">v-v:tense-v:pred.pn</ta>
            <ta e="T501" id="Seg_8930" s="T500">dempro-pro:case</ta>
            <ta e="T502" id="Seg_8931" s="T501">que-pro:case</ta>
            <ta e="T503" id="Seg_8932" s="T502">v-v:cvb</ta>
            <ta e="T504" id="Seg_8933" s="T503">n-n:(poss)-n:case</ta>
            <ta e="T505" id="Seg_8934" s="T504">n-n:(poss)-n:case</ta>
            <ta e="T506" id="Seg_8935" s="T505">n-n:(poss)-n:case</ta>
            <ta e="T507" id="Seg_8936" s="T506">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T508" id="Seg_8937" s="T507">v-v:tense-v:pred.pn</ta>
            <ta e="T509" id="Seg_8938" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_8939" s="T509">v-v:mood-v:pred.pn</ta>
            <ta e="T511" id="Seg_8940" s="T510">v-v:tense-v:pred.pn</ta>
            <ta e="T512" id="Seg_8941" s="T511">v-v:tense-v:pred.pn</ta>
            <ta e="T513" id="Seg_8942" s="T512">que</ta>
            <ta e="T514" id="Seg_8943" s="T513">n-n:case</ta>
            <ta e="T515" id="Seg_8944" s="T514">adj-n:(poss)-n:case</ta>
            <ta e="T516" id="Seg_8945" s="T515">v-v:tense-v:pred.pn</ta>
            <ta e="T517" id="Seg_8946" s="T516">v-v:tense-v:pred.pn</ta>
            <ta e="T518" id="Seg_8947" s="T517">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T519" id="Seg_8948" s="T518">n-n:(poss)-n:case</ta>
            <ta e="T520" id="Seg_8949" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_8950" s="T520">v-v:tense-v:pred.pn</ta>
            <ta e="T522" id="Seg_8951" s="T521">dempro-pro:case</ta>
            <ta e="T523" id="Seg_8952" s="T522">post</ta>
            <ta e="T524" id="Seg_8953" s="T523">n-n:case</ta>
            <ta e="T525" id="Seg_8954" s="T524">v-v:tense-v:pred.pn</ta>
            <ta e="T526" id="Seg_8955" s="T525">que-pro:case-ptcl</ta>
            <ta e="T527" id="Seg_8956" s="T526">que-pro:(ins)-pro:case</ta>
            <ta e="T530" id="Seg_8957" s="T529">v-v:tense-v:poss.pn</ta>
            <ta e="T531" id="Seg_8958" s="T530">v-v:tense-v:pred.pn</ta>
            <ta e="T532" id="Seg_8959" s="T531">v-v:tense-v:pred.pn</ta>
            <ta e="T533" id="Seg_8960" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_8961" s="T533">v-v:tense-v:poss.pn</ta>
            <ta e="T535" id="Seg_8962" s="T534">adj-adj&gt;adv</ta>
            <ta e="T536" id="Seg_8963" s="T535">n-n&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T537" id="Seg_8964" s="T536">v-v:mood-v:temp.pn</ta>
            <ta e="T538" id="Seg_8965" s="T537">v-v:(ins)-v&gt;v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T539" id="Seg_8966" s="T538">conj</ta>
            <ta e="T540" id="Seg_8967" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_8968" s="T540">adv</ta>
            <ta e="T542" id="Seg_8969" s="T541">n-n:(poss)-n:case</ta>
            <ta e="T543" id="Seg_8970" s="T542">adj-n:(poss)-n:case</ta>
            <ta e="T544" id="Seg_8971" s="T543">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T545" id="Seg_8972" s="T544">dempro</ta>
            <ta e="T546" id="Seg_8973" s="T545">n-n:case</ta>
            <ta e="T550" id="Seg_8974" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_8975" s="T550">v-v:tense-v:pred.pn</ta>
            <ta e="T572" id="Seg_8976" s="T571">dempro-pro:case</ta>
            <ta e="T573" id="Seg_8977" s="T572">dempro-pro:case</ta>
            <ta e="T581" id="Seg_8978" s="T580">n-n&gt;n-n:case</ta>
            <ta e="T582" id="Seg_8979" s="T581">n-n:case</ta>
            <ta e="T583" id="Seg_8980" s="T582">adj-n:case</ta>
            <ta e="T584" id="Seg_8981" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_8982" s="T584">dempro-pro:case</ta>
            <ta e="T613" id="Seg_8983" s="T612">dempro</ta>
            <ta e="T614" id="Seg_8984" s="T613">adj-adj&gt;adv</ta>
            <ta e="T615" id="Seg_8985" s="T614">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T616" id="Seg_8986" s="T615">post</ta>
            <ta e="T617" id="Seg_8987" s="T616">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T618" id="Seg_8988" s="T617">n-n:(num)-n:case</ta>
            <ta e="T619" id="Seg_8989" s="T618">post</ta>
            <ta e="T620" id="Seg_8990" s="T619">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T621" id="Seg_8991" s="T620">post</ta>
            <ta e="T622" id="Seg_8992" s="T621">adv</ta>
            <ta e="T623" id="Seg_8993" s="T622">adj-n:case</ta>
            <ta e="T624" id="Seg_8994" s="T623">v-v:mood-v:temp.pn</ta>
            <ta e="T625" id="Seg_8995" s="T624">adv</ta>
            <ta e="T626" id="Seg_8996" s="T625">adj-n:case</ta>
            <ta e="T627" id="Seg_8997" s="T626">v-v:tense-v:poss.pn</ta>
            <ta e="T628" id="Seg_8998" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_8999" s="T628">v-v:(ins)-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T630" id="Seg_9000" s="T629">conj</ta>
            <ta e="T631" id="Seg_9001" s="T630">post</ta>
            <ta e="T632" id="Seg_9002" s="T631">v-v&gt;n-n:case</ta>
            <ta e="T633" id="Seg_9003" s="T632">n-n&gt;adj-n:case</ta>
            <ta e="T634" id="Seg_9004" s="T633">v-v:tense-v:poss.pn</ta>
            <ta e="T635" id="Seg_9005" s="T634">n-n:case</ta>
            <ta e="T636" id="Seg_9006" s="T635">conj</ta>
            <ta e="T637" id="Seg_9007" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_9008" s="T637">n-n:case</ta>
            <ta e="T639" id="Seg_9009" s="T638">conj</ta>
            <ta e="T640" id="Seg_9010" s="T639">ptcl</ta>
            <ta e="T641" id="Seg_9011" s="T640">adj-n:case</ta>
            <ta e="T642" id="Seg_9012" s="T641">conj</ta>
            <ta e="T643" id="Seg_9013" s="T642">ptcl</ta>
            <ta e="T653" id="Seg_9014" s="T652">n-n:case</ta>
            <ta e="T654" id="Seg_9015" s="T653">adj</ta>
            <ta e="T655" id="Seg_9016" s="T654">ptcl</ta>
            <ta e="T656" id="Seg_9017" s="T655">v-v:tense-v:poss.pn</ta>
            <ta e="T657" id="Seg_9018" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_9019" s="T657">n-n:case</ta>
            <ta e="T659" id="Seg_9020" s="T658">que-pro:(poss)-pro:case</ta>
            <ta e="T661" id="Seg_9021" s="T660">v-v:tense-v:pred.pn</ta>
            <ta e="T662" id="Seg_9022" s="T661">adv</ta>
            <ta e="T663" id="Seg_9023" s="T662">n-n:(poss)</ta>
            <ta e="T664" id="Seg_9024" s="T663">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T665" id="Seg_9025" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_9026" s="T665">dempro-pro:case</ta>
            <ta e="T667" id="Seg_9027" s="T666">post</ta>
            <ta e="T669" id="Seg_9028" s="T667">n-n:(num)-n:case</ta>
            <ta e="T694" id="Seg_9029" s="T693">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T695" id="Seg_9030" s="T694">v-v:tense-v:pred.pn</ta>
            <ta e="T696" id="Seg_9031" s="T695">ptcl</ta>
            <ta e="T697" id="Seg_9032" s="T696">n-n:poss-n:case</ta>
            <ta e="T698" id="Seg_9033" s="T697">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T699" id="Seg_9034" s="T698">adj-n:case</ta>
            <ta e="T700" id="Seg_9035" s="T699">ptcl</ta>
            <ta e="T701" id="Seg_9036" s="T700">v-v:tense-v:pred.pn</ta>
            <ta e="T702" id="Seg_9037" s="T701">n-n:case</ta>
            <ta e="T703" id="Seg_9038" s="T702">post</ta>
            <ta e="T704" id="Seg_9039" s="T703">v-v:cvb</ta>
            <ta e="T705" id="Seg_9040" s="T704">v-v:tense-v:pred.pn</ta>
            <ta e="T706" id="Seg_9041" s="T705">ptcl</ta>
            <ta e="T707" id="Seg_9042" s="T706">dempro</ta>
            <ta e="T708" id="Seg_9043" s="T707">n-n:case</ta>
            <ta e="T709" id="Seg_9044" s="T708">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T710" id="Seg_9045" s="T709">v-v:cvb</ta>
            <ta e="T711" id="Seg_9046" s="T710">quant</ta>
            <ta e="T712" id="Seg_9047" s="T711">n-n:case</ta>
            <ta e="T715" id="Seg_9048" s="T714">quant</ta>
            <ta e="T716" id="Seg_9049" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_9050" s="T716">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-ChSA">
            <ta e="T7" id="Seg_9051" s="T6">n</ta>
            <ta e="T8" id="Seg_9052" s="T7">n</ta>
            <ta e="T9" id="Seg_9053" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_9054" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_9055" s="T10">adj</ta>
            <ta e="T12" id="Seg_9056" s="T11">cop</ta>
            <ta e="T13" id="Seg_9057" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_9058" s="T13">n</ta>
            <ta e="T15" id="Seg_9059" s="T14">n</ta>
            <ta e="T16" id="Seg_9060" s="T15">v</ta>
            <ta e="T19" id="Seg_9061" s="T18">adv</ta>
            <ta e="T22" id="Seg_9062" s="T21">n</ta>
            <ta e="T23" id="Seg_9063" s="T22">v</ta>
            <ta e="T24" id="Seg_9064" s="T23">dempro</ta>
            <ta e="T25" id="Seg_9065" s="T24">v</ta>
            <ta e="T26" id="Seg_9066" s="T25">pers</ta>
            <ta e="T27" id="Seg_9067" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_9068" s="T27">adv</ta>
            <ta e="T29" id="Seg_9069" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_9070" s="T29">dempro</ta>
            <ta e="T31" id="Seg_9071" s="T30">post</ta>
            <ta e="T32" id="Seg_9072" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_9073" s="T32">n</ta>
            <ta e="T34" id="Seg_9074" s="T33">v</ta>
            <ta e="T35" id="Seg_9075" s="T34">adv</ta>
            <ta e="T36" id="Seg_9076" s="T35">n</ta>
            <ta e="T37" id="Seg_9077" s="T36">post</ta>
            <ta e="T38" id="Seg_9078" s="T37">v</ta>
            <ta e="T39" id="Seg_9079" s="T38">adv</ta>
            <ta e="T40" id="Seg_9080" s="T39">dempro</ta>
            <ta e="T41" id="Seg_9081" s="T40">post</ta>
            <ta e="T42" id="Seg_9082" s="T41">adj</ta>
            <ta e="T43" id="Seg_9083" s="T42">quant</ta>
            <ta e="T44" id="Seg_9084" s="T43">n</ta>
            <ta e="T45" id="Seg_9085" s="T44">n</ta>
            <ta e="T46" id="Seg_9086" s="T45">v</ta>
            <ta e="T47" id="Seg_9087" s="T46">post</ta>
            <ta e="T48" id="Seg_9088" s="T47">v</ta>
            <ta e="T49" id="Seg_9089" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_9090" s="T49">n</ta>
            <ta e="T51" id="Seg_9091" s="T50">conj</ta>
            <ta e="T52" id="Seg_9092" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_9093" s="T52">n</ta>
            <ta e="T54" id="Seg_9094" s="T53">adj</ta>
            <ta e="T55" id="Seg_9095" s="T54">dempro</ta>
            <ta e="T56" id="Seg_9096" s="T55">n</ta>
            <ta e="T57" id="Seg_9097" s="T56">v</ta>
            <ta e="T58" id="Seg_9098" s="T57">dempro</ta>
            <ta e="T59" id="Seg_9099" s="T58">que</ta>
            <ta e="T61" id="Seg_9100" s="T59">adv</ta>
            <ta e="T62" id="Seg_9101" s="T61">n</ta>
            <ta e="T63" id="Seg_9102" s="T62">v</ta>
            <ta e="T64" id="Seg_9103" s="T63">v</ta>
            <ta e="T65" id="Seg_9104" s="T64">n</ta>
            <ta e="T66" id="Seg_9105" s="T65">n</ta>
            <ta e="T67" id="Seg_9106" s="T66">adj</ta>
            <ta e="T68" id="Seg_9107" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_9108" s="T68">adv</ta>
            <ta e="T70" id="Seg_9109" s="T69">quant</ta>
            <ta e="T71" id="Seg_9110" s="T70">conj</ta>
            <ta e="T72" id="Seg_9111" s="T71">adj</ta>
            <ta e="T73" id="Seg_9112" s="T72">n</ta>
            <ta e="T74" id="Seg_9113" s="T73">n</ta>
            <ta e="T75" id="Seg_9114" s="T74">v</ta>
            <ta e="T76" id="Seg_9115" s="T75">v</ta>
            <ta e="T77" id="Seg_9116" s="T76">aux</ta>
            <ta e="T78" id="Seg_9117" s="T77">adv</ta>
            <ta e="T79" id="Seg_9118" s="T78">cardnum</ta>
            <ta e="T80" id="Seg_9119" s="T79">cardnum</ta>
            <ta e="T81" id="Seg_9120" s="T80">v</ta>
            <ta e="T82" id="Seg_9121" s="T81">n</ta>
            <ta e="T83" id="Seg_9122" s="T82">adv</ta>
            <ta e="T84" id="Seg_9123" s="T83">interj</ta>
            <ta e="T85" id="Seg_9124" s="T84">n</ta>
            <ta e="T86" id="Seg_9125" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_9126" s="T86">ptcl</ta>
            <ta e="T90" id="Seg_9127" s="T89">propr</ta>
            <ta e="T91" id="Seg_9128" s="T90">n</ta>
            <ta e="T92" id="Seg_9129" s="T91">n</ta>
            <ta e="T93" id="Seg_9130" s="T92">n</ta>
            <ta e="T94" id="Seg_9131" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_9132" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_9133" s="T95">adv</ta>
            <ta e="T97" id="Seg_9134" s="T96">n</ta>
            <ta e="T98" id="Seg_9135" s="T97">que</ta>
            <ta e="T99" id="Seg_9136" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_9137" s="T99">v</ta>
            <ta e="T101" id="Seg_9138" s="T100">dempro</ta>
            <ta e="T102" id="Seg_9139" s="T101">v</ta>
            <ta e="T103" id="Seg_9140" s="T102">v</ta>
            <ta e="T104" id="Seg_9141" s="T103">pers</ta>
            <ta e="T105" id="Seg_9142" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_9143" s="T105">dempro</ta>
            <ta e="T107" id="Seg_9144" s="T106">v</ta>
            <ta e="T108" id="Seg_9145" s="T107">adj</ta>
            <ta e="T109" id="Seg_9146" s="T108">cop</ta>
            <ta e="T110" id="Seg_9147" s="T109">n</ta>
            <ta e="T111" id="Seg_9148" s="T110">v</ta>
            <ta e="T112" id="Seg_9149" s="T111">adv</ta>
            <ta e="T113" id="Seg_9150" s="T112">cardnum</ta>
            <ta e="T114" id="Seg_9151" s="T113">postp</ta>
            <ta e="T115" id="Seg_9152" s="T114">cardnum</ta>
            <ta e="T116" id="Seg_9153" s="T115">v</ta>
            <ta e="T117" id="Seg_9154" s="T116">que</ta>
            <ta e="T118" id="Seg_9155" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_9156" s="T118">n</ta>
            <ta e="T120" id="Seg_9157" s="T119">v</ta>
            <ta e="T121" id="Seg_9158" s="T120">n</ta>
            <ta e="T122" id="Seg_9159" s="T121">v</ta>
            <ta e="T130" id="Seg_9160" s="T129">n</ta>
            <ta e="T131" id="Seg_9161" s="T130">adv</ta>
            <ta e="T132" id="Seg_9162" s="T131">adv</ta>
            <ta e="T133" id="Seg_9163" s="T132">post</ta>
            <ta e="T134" id="Seg_9164" s="T133">dempro</ta>
            <ta e="T135" id="Seg_9165" s="T134">adv</ta>
            <ta e="T136" id="Seg_9166" s="T135">n</ta>
            <ta e="T137" id="Seg_9167" s="T136">dempro</ta>
            <ta e="T138" id="Seg_9168" s="T137">v</ta>
            <ta e="T139" id="Seg_9169" s="T138">propr</ta>
            <ta e="T140" id="Seg_9170" s="T139">adj</ta>
            <ta e="T141" id="Seg_9171" s="T140">adj</ta>
            <ta e="T148" id="Seg_9172" s="T147">interj</ta>
            <ta e="T149" id="Seg_9173" s="T148">v</ta>
            <ta e="T150" id="Seg_9174" s="T149">collnum</ta>
            <ta e="T151" id="Seg_9175" s="T150">v</ta>
            <ta e="T167" id="Seg_9176" s="T166">n</ta>
            <ta e="T168" id="Seg_9177" s="T167">quant</ta>
            <ta e="T169" id="Seg_9178" s="T168">n</ta>
            <ta e="T170" id="Seg_9179" s="T169">cop</ta>
            <ta e="T171" id="Seg_9180" s="T170">adv</ta>
            <ta e="T174" id="Seg_9181" s="T173">v</ta>
            <ta e="T175" id="Seg_9182" s="T174">v</ta>
            <ta e="T176" id="Seg_9183" s="T175">adv</ta>
            <ta e="T177" id="Seg_9184" s="T176">v</ta>
            <ta e="T178" id="Seg_9185" s="T177">aux</ta>
            <ta e="T179" id="Seg_9186" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_9187" s="T179">pers</ta>
            <ta e="T181" id="Seg_9188" s="T180">dempro</ta>
            <ta e="T182" id="Seg_9189" s="T181">n</ta>
            <ta e="T183" id="Seg_9190" s="T182">n</ta>
            <ta e="T184" id="Seg_9191" s="T183">n</ta>
            <ta e="T185" id="Seg_9192" s="T184">v</ta>
            <ta e="T186" id="Seg_9193" s="T185">v</ta>
            <ta e="T187" id="Seg_9194" s="T186">v</ta>
            <ta e="T188" id="Seg_9195" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_9196" s="T188">n</ta>
            <ta e="T190" id="Seg_9197" s="T189">v</ta>
            <ta e="T191" id="Seg_9198" s="T190">n</ta>
            <ta e="T192" id="Seg_9199" s="T191">v</ta>
            <ta e="T193" id="Seg_9200" s="T192">v</ta>
            <ta e="T194" id="Seg_9201" s="T193">adv</ta>
            <ta e="T195" id="Seg_9202" s="T194">v</ta>
            <ta e="T196" id="Seg_9203" s="T195">aux</ta>
            <ta e="T197" id="Seg_9204" s="T196">v</ta>
            <ta e="T198" id="Seg_9205" s="T197">que</ta>
            <ta e="T199" id="Seg_9206" s="T198">v</ta>
            <ta e="T200" id="Seg_9207" s="T199">que</ta>
            <ta e="T201" id="Seg_9208" s="T200">v</ta>
            <ta e="T202" id="Seg_9209" s="T201">n</ta>
            <ta e="T203" id="Seg_9210" s="T202">v</ta>
            <ta e="T204" id="Seg_9211" s="T203">quant</ta>
            <ta e="T205" id="Seg_9212" s="T204">n</ta>
            <ta e="T206" id="Seg_9213" s="T205">n</ta>
            <ta e="T207" id="Seg_9214" s="T206">v</ta>
            <ta e="T208" id="Seg_9215" s="T207">adv</ta>
            <ta e="T209" id="Seg_9216" s="T208">adv</ta>
            <ta e="T210" id="Seg_9217" s="T209">v</ta>
            <ta e="T211" id="Seg_9218" s="T210">aux</ta>
            <ta e="T212" id="Seg_9219" s="T211">dempro</ta>
            <ta e="T213" id="Seg_9220" s="T212">adv</ta>
            <ta e="T214" id="Seg_9221" s="T213">v</ta>
            <ta e="T215" id="Seg_9222" s="T214">post</ta>
            <ta e="T217" id="Seg_9223" s="T215">ptcl</ta>
            <ta e="T218" id="Seg_9224" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_9225" s="T218">adv</ta>
            <ta e="T220" id="Seg_9226" s="T219">adj</ta>
            <ta e="T221" id="Seg_9227" s="T220">n</ta>
            <ta e="T222" id="Seg_9228" s="T221">v</ta>
            <ta e="T223" id="Seg_9229" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_9230" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_9231" s="T224">adv</ta>
            <ta e="T226" id="Seg_9232" s="T225">conj</ta>
            <ta e="T227" id="Seg_9233" s="T226">adv</ta>
            <ta e="T228" id="Seg_9234" s="T227">conj</ta>
            <ta e="T229" id="Seg_9235" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_9236" s="T229">v</ta>
            <ta e="T231" id="Seg_9237" s="T230">quant</ta>
            <ta e="T232" id="Seg_9238" s="T231">n</ta>
            <ta e="T233" id="Seg_9239" s="T232">v</ta>
            <ta e="T234" id="Seg_9240" s="T233">cardnum</ta>
            <ta e="T235" id="Seg_9241" s="T234">cardnum</ta>
            <ta e="T236" id="Seg_9242" s="T235">n</ta>
            <ta e="T237" id="Seg_9243" s="T236">n</ta>
            <ta e="T238" id="Seg_9244" s="T237">v</ta>
            <ta e="T239" id="Seg_9245" s="T238">post</ta>
            <ta e="T240" id="Seg_9246" s="T239">adv</ta>
            <ta e="T241" id="Seg_9247" s="T240">v</ta>
            <ta e="T242" id="Seg_9248" s="T241">n</ta>
            <ta e="T243" id="Seg_9249" s="T242">v</ta>
            <ta e="T257" id="Seg_9250" s="T256">v</ta>
            <ta e="T258" id="Seg_9251" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_9252" s="T258">v</ta>
            <ta e="T260" id="Seg_9253" s="T259">v</ta>
            <ta e="T261" id="Seg_9254" s="T260">v</ta>
            <ta e="T262" id="Seg_9255" s="T261">dempro</ta>
            <ta e="T263" id="Seg_9256" s="T262">que</ta>
            <ta e="T264" id="Seg_9257" s="T263">propr</ta>
            <ta e="T265" id="Seg_9258" s="T264">v</ta>
            <ta e="T266" id="Seg_9259" s="T265">v</ta>
            <ta e="T267" id="Seg_9260" s="T266">adv</ta>
            <ta e="T268" id="Seg_9261" s="T267">n</ta>
            <ta e="T269" id="Seg_9262" s="T268">conj</ta>
            <ta e="T270" id="Seg_9263" s="T269">v</ta>
            <ta e="T271" id="Seg_9264" s="T270">n</ta>
            <ta e="T272" id="Seg_9265" s="T271">v</ta>
            <ta e="T273" id="Seg_9266" s="T272">n</ta>
            <ta e="T274" id="Seg_9267" s="T273">adv</ta>
            <ta e="T275" id="Seg_9268" s="T274">n</ta>
            <ta e="T276" id="Seg_9269" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_9270" s="T276">n</ta>
            <ta e="T278" id="Seg_9271" s="T277">v</ta>
            <ta e="T279" id="Seg_9272" s="T278">adj</ta>
            <ta e="T280" id="Seg_9273" s="T279">adv</ta>
            <ta e="T281" id="Seg_9274" s="T280">v</ta>
            <ta e="T282" id="Seg_9275" s="T281">dempro</ta>
            <ta e="T283" id="Seg_9276" s="T282">n</ta>
            <ta e="T284" id="Seg_9277" s="T283">conj</ta>
            <ta e="T285" id="Seg_9278" s="T284">adv</ta>
            <ta e="T286" id="Seg_9279" s="T285">adv</ta>
            <ta e="T287" id="Seg_9280" s="T286">v</ta>
            <ta e="T288" id="Seg_9281" s="T287">ptcl</ta>
            <ta e="T302" id="Seg_9282" s="T301">v</ta>
            <ta e="T303" id="Seg_9283" s="T302">adv</ta>
            <ta e="T304" id="Seg_9284" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_9285" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_9286" s="T305">cop</ta>
            <ta e="T307" id="Seg_9287" s="T306">v</ta>
            <ta e="T308" id="Seg_9288" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_9289" s="T308">adv</ta>
            <ta e="T310" id="Seg_9290" s="T309">v</ta>
            <ta e="T311" id="Seg_9291" s="T310">aux</ta>
            <ta e="T312" id="Seg_9292" s="T311">adv</ta>
            <ta e="T313" id="Seg_9293" s="T312">v</ta>
            <ta e="T314" id="Seg_9294" s="T313">ptcl</ta>
            <ta e="T316" id="Seg_9295" s="T315">aux</ta>
            <ta e="T320" id="Seg_9296" s="T319">posspr</ta>
            <ta e="T321" id="Seg_9297" s="T320">que</ta>
            <ta e="T322" id="Seg_9298" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_9299" s="T322">n</ta>
            <ta e="T324" id="Seg_9300" s="T323">adj</ta>
            <ta e="T325" id="Seg_9301" s="T324">cop</ta>
            <ta e="T326" id="Seg_9302" s="T325">adv</ta>
            <ta e="T327" id="Seg_9303" s="T326">adj</ta>
            <ta e="T328" id="Seg_9304" s="T327">adj</ta>
            <ta e="T329" id="Seg_9305" s="T328">cop</ta>
            <ta e="T330" id="Seg_9306" s="T329">n</ta>
            <ta e="T331" id="Seg_9307" s="T330">adj</ta>
            <ta e="T332" id="Seg_9308" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_9309" s="T332">dempro</ta>
            <ta e="T334" id="Seg_9310" s="T333">pers</ta>
            <ta e="T335" id="Seg_9311" s="T334">v</ta>
            <ta e="T336" id="Seg_9312" s="T335">v</ta>
            <ta e="T337" id="Seg_9313" s="T336">v</ta>
            <ta e="T338" id="Seg_9314" s="T337">adv</ta>
            <ta e="T339" id="Seg_9315" s="T338">n</ta>
            <ta e="T340" id="Seg_9316" s="T339">v</ta>
            <ta e="T341" id="Seg_9317" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_9318" s="T341">adv</ta>
            <ta e="T343" id="Seg_9319" s="T342">n</ta>
            <ta e="T344" id="Seg_9320" s="T343">adj</ta>
            <ta e="T345" id="Seg_9321" s="T344">dempro</ta>
            <ta e="T346" id="Seg_9322" s="T345">ptcl</ta>
            <ta e="T353" id="Seg_9323" s="T352">interj</ta>
            <ta e="T354" id="Seg_9324" s="T353">v</ta>
            <ta e="T355" id="Seg_9325" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_9326" s="T355">n</ta>
            <ta e="T357" id="Seg_9327" s="T356">v</ta>
            <ta e="T358" id="Seg_9328" s="T357">n</ta>
            <ta e="T359" id="Seg_9329" s="T358">n</ta>
            <ta e="T360" id="Seg_9330" s="T359">v</ta>
            <ta e="T361" id="Seg_9331" s="T360">v</ta>
            <ta e="T364" id="Seg_9332" s="T363">posspr</ta>
            <ta e="T365" id="Seg_9333" s="T364">que</ta>
            <ta e="T366" id="Seg_9334" s="T365">propr</ta>
            <ta e="T367" id="Seg_9335" s="T366">adj</ta>
            <ta e="T368" id="Seg_9336" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_9337" s="T368">cardnum</ta>
            <ta e="T370" id="Seg_9338" s="T369">n</ta>
            <ta e="T371" id="Seg_9339" s="T370">que</ta>
            <ta e="T372" id="Seg_9340" s="T371">v</ta>
            <ta e="T373" id="Seg_9341" s="T372">n</ta>
            <ta e="T374" id="Seg_9342" s="T373">cardnum</ta>
            <ta e="T375" id="Seg_9343" s="T374">cardnum</ta>
            <ta e="T376" id="Seg_9344" s="T375">n</ta>
            <ta e="T381" id="Seg_9345" s="T380">v</ta>
            <ta e="T382" id="Seg_9346" s="T381">post</ta>
            <ta e="T383" id="Seg_9347" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_9348" s="T383">interj</ta>
            <ta e="T387" id="Seg_9349" s="T386">v</ta>
            <ta e="T388" id="Seg_9350" s="T387">v</ta>
            <ta e="T389" id="Seg_9351" s="T388">aux</ta>
            <ta e="T390" id="Seg_9352" s="T389">post</ta>
            <ta e="T391" id="Seg_9353" s="T390">posspr</ta>
            <ta e="T393" id="Seg_9354" s="T391">ptcl</ta>
            <ta e="T394" id="Seg_9355" s="T393">n</ta>
            <ta e="T395" id="Seg_9356" s="T394">adj</ta>
            <ta e="T396" id="Seg_9357" s="T395">v</ta>
            <ta e="T397" id="Seg_9358" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_9359" s="T397">propr</ta>
            <ta e="T399" id="Seg_9360" s="T398">n</ta>
            <ta e="T400" id="Seg_9361" s="T399">adv</ta>
            <ta e="T401" id="Seg_9362" s="T400">adj</ta>
            <ta e="T402" id="Seg_9363" s="T401">n</ta>
            <ta e="T403" id="Seg_9364" s="T402">dempro</ta>
            <ta e="T404" id="Seg_9365" s="T403">post</ta>
            <ta e="T405" id="Seg_9366" s="T404">dempro</ta>
            <ta e="T406" id="Seg_9367" s="T405">n</ta>
            <ta e="T407" id="Seg_9368" s="T406">v</ta>
            <ta e="T408" id="Seg_9369" s="T407">adj</ta>
            <ta e="T409" id="Seg_9370" s="T408">n</ta>
            <ta e="T410" id="Seg_9371" s="T409">adj</ta>
            <ta e="T411" id="Seg_9372" s="T410">n</ta>
            <ta e="T412" id="Seg_9373" s="T411">n</ta>
            <ta e="T413" id="Seg_9374" s="T412">adv</ta>
            <ta e="T414" id="Seg_9375" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_9376" s="T414">adj</ta>
            <ta e="T418" id="Seg_9377" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_9378" s="T418">v</ta>
            <ta e="T420" id="Seg_9379" s="T419">v</ta>
            <ta e="T421" id="Seg_9380" s="T420">n</ta>
            <ta e="T422" id="Seg_9381" s="T421">v</ta>
            <ta e="T423" id="Seg_9382" s="T422">dempro</ta>
            <ta e="T425" id="Seg_9383" s="T424">n</ta>
            <ta e="T426" id="Seg_9384" s="T425">post</ta>
            <ta e="T427" id="Seg_9385" s="T426">v</ta>
            <ta e="T442" id="Seg_9386" s="T441">dempro</ta>
            <ta e="T443" id="Seg_9387" s="T442">adj</ta>
            <ta e="T444" id="Seg_9388" s="T443">n</ta>
            <ta e="T445" id="Seg_9389" s="T444">dempro</ta>
            <ta e="T446" id="Seg_9390" s="T445">cop</ta>
            <ta e="T447" id="Seg_9391" s="T446">n</ta>
            <ta e="T448" id="Seg_9392" s="T447">adv</ta>
            <ta e="T449" id="Seg_9393" s="T448">v</ta>
            <ta e="T450" id="Seg_9394" s="T449">v</ta>
            <ta e="T451" id="Seg_9395" s="T450">v</ta>
            <ta e="T452" id="Seg_9396" s="T451">interj</ta>
            <ta e="T453" id="Seg_9397" s="T452">n</ta>
            <ta e="T454" id="Seg_9398" s="T453">v</ta>
            <ta e="T455" id="Seg_9399" s="T454">adj</ta>
            <ta e="T456" id="Seg_9400" s="T455">cop</ta>
            <ta e="T457" id="Seg_9401" s="T456">n</ta>
            <ta e="T458" id="Seg_9402" s="T457">n</ta>
            <ta e="T459" id="Seg_9403" s="T458">distrnum</ta>
            <ta e="T460" id="Seg_9404" s="T459">distrnum</ta>
            <ta e="T461" id="Seg_9405" s="T460">cardnum</ta>
            <ta e="T462" id="Seg_9406" s="T461">cardnum</ta>
            <ta e="T463" id="Seg_9407" s="T462">n</ta>
            <ta e="T464" id="Seg_9408" s="T463">v</ta>
            <ta e="T465" id="Seg_9409" s="T464">pers</ta>
            <ta e="T466" id="Seg_9410" s="T465">pers</ta>
            <ta e="T467" id="Seg_9411" s="T466">dempro</ta>
            <ta e="T468" id="Seg_9412" s="T467">que</ta>
            <ta e="T471" id="Seg_9413" s="T470">dempro</ta>
            <ta e="T472" id="Seg_9414" s="T471">ptcl</ta>
            <ta e="T474" id="Seg_9415" s="T472">ptcl</ta>
            <ta e="T475" id="Seg_9416" s="T474">adv</ta>
            <ta e="T477" id="Seg_9417" s="T475">ptcl</ta>
            <ta e="T478" id="Seg_9418" s="T477">adv</ta>
            <ta e="T479" id="Seg_9419" s="T478">cardnum</ta>
            <ta e="T480" id="Seg_9420" s="T479">cardnum</ta>
            <ta e="T481" id="Seg_9421" s="T480">adv</ta>
            <ta e="T482" id="Seg_9422" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_9423" s="T482">n</ta>
            <ta e="T484" id="Seg_9424" s="T483">post</ta>
            <ta e="T486" id="Seg_9425" s="T484">n</ta>
            <ta e="T487" id="Seg_9426" s="T486">quant</ta>
            <ta e="T488" id="Seg_9427" s="T487">adv</ta>
            <ta e="T489" id="Seg_9428" s="T488">n</ta>
            <ta e="T490" id="Seg_9429" s="T489">adv</ta>
            <ta e="T491" id="Seg_9430" s="T490">post</ta>
            <ta e="T492" id="Seg_9431" s="T491">n</ta>
            <ta e="T493" id="Seg_9432" s="T492">v</ta>
            <ta e="T494" id="Seg_9433" s="T493">adv</ta>
            <ta e="T495" id="Seg_9434" s="T494">que</ta>
            <ta e="T496" id="Seg_9435" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_9436" s="T496">v</ta>
            <ta e="T498" id="Seg_9437" s="T497">v</ta>
            <ta e="T499" id="Seg_9438" s="T498">que</ta>
            <ta e="T500" id="Seg_9439" s="T499">v</ta>
            <ta e="T501" id="Seg_9440" s="T500">dempro</ta>
            <ta e="T502" id="Seg_9441" s="T501">que</ta>
            <ta e="T503" id="Seg_9442" s="T502">v</ta>
            <ta e="T504" id="Seg_9443" s="T503">n</ta>
            <ta e="T505" id="Seg_9444" s="T504">n</ta>
            <ta e="T506" id="Seg_9445" s="T505">n</ta>
            <ta e="T507" id="Seg_9446" s="T506">emphpro</ta>
            <ta e="T508" id="Seg_9447" s="T507">v</ta>
            <ta e="T509" id="Seg_9448" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_9449" s="T509">v</ta>
            <ta e="T511" id="Seg_9450" s="T510">v</ta>
            <ta e="T512" id="Seg_9451" s="T511">v</ta>
            <ta e="T513" id="Seg_9452" s="T512">que</ta>
            <ta e="T514" id="Seg_9453" s="T513">n</ta>
            <ta e="T515" id="Seg_9454" s="T514">adj</ta>
            <ta e="T516" id="Seg_9455" s="T515">v</ta>
            <ta e="T517" id="Seg_9456" s="T516">v</ta>
            <ta e="T518" id="Seg_9457" s="T517">emphpro</ta>
            <ta e="T519" id="Seg_9458" s="T518">n</ta>
            <ta e="T520" id="Seg_9459" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_9460" s="T520">v</ta>
            <ta e="T522" id="Seg_9461" s="T521">dempro</ta>
            <ta e="T523" id="Seg_9462" s="T522">post</ta>
            <ta e="T524" id="Seg_9463" s="T523">n</ta>
            <ta e="T525" id="Seg_9464" s="T524">v</ta>
            <ta e="T526" id="Seg_9465" s="T525">que</ta>
            <ta e="T527" id="Seg_9466" s="T526">que</ta>
            <ta e="T530" id="Seg_9467" s="T529">v</ta>
            <ta e="T531" id="Seg_9468" s="T530">v</ta>
            <ta e="T532" id="Seg_9469" s="T531">v</ta>
            <ta e="T533" id="Seg_9470" s="T532">n</ta>
            <ta e="T534" id="Seg_9471" s="T533">cop</ta>
            <ta e="T535" id="Seg_9472" s="T534">adv</ta>
            <ta e="T536" id="Seg_9473" s="T535">v</ta>
            <ta e="T537" id="Seg_9474" s="T536">v</ta>
            <ta e="T538" id="Seg_9475" s="T537">v</ta>
            <ta e="T539" id="Seg_9476" s="T538">conj</ta>
            <ta e="T540" id="Seg_9477" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_9478" s="T540">adv</ta>
            <ta e="T542" id="Seg_9479" s="T541">n</ta>
            <ta e="T543" id="Seg_9480" s="T542">adj</ta>
            <ta e="T544" id="Seg_9481" s="T543">v</ta>
            <ta e="T545" id="Seg_9482" s="T544">dempro</ta>
            <ta e="T546" id="Seg_9483" s="T545">n</ta>
            <ta e="T550" id="Seg_9484" s="T549">n</ta>
            <ta e="T551" id="Seg_9485" s="T550">v</ta>
            <ta e="T572" id="Seg_9486" s="T571">dempro</ta>
            <ta e="T573" id="Seg_9487" s="T572">dempro</ta>
            <ta e="T581" id="Seg_9488" s="T580">n</ta>
            <ta e="T582" id="Seg_9489" s="T581">n</ta>
            <ta e="T583" id="Seg_9490" s="T582">adj</ta>
            <ta e="T584" id="Seg_9491" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_9492" s="T584">dempro</ta>
            <ta e="T613" id="Seg_9493" s="T612">dempro</ta>
            <ta e="T614" id="Seg_9494" s="T613">adv</ta>
            <ta e="T615" id="Seg_9495" s="T614">v</ta>
            <ta e="T616" id="Seg_9496" s="T615">post</ta>
            <ta e="T617" id="Seg_9497" s="T616">v</ta>
            <ta e="T618" id="Seg_9498" s="T617">n</ta>
            <ta e="T619" id="Seg_9499" s="T618">post</ta>
            <ta e="T620" id="Seg_9500" s="T619">v</ta>
            <ta e="T621" id="Seg_9501" s="T620">post</ta>
            <ta e="T622" id="Seg_9502" s="T621">adv</ta>
            <ta e="T623" id="Seg_9503" s="T622">adj</ta>
            <ta e="T624" id="Seg_9504" s="T623">cop</ta>
            <ta e="T625" id="Seg_9505" s="T624">adv</ta>
            <ta e="T626" id="Seg_9506" s="T625">adj</ta>
            <ta e="T627" id="Seg_9507" s="T626">cop</ta>
            <ta e="T628" id="Seg_9508" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_9509" s="T628">v</ta>
            <ta e="T630" id="Seg_9510" s="T629">conj</ta>
            <ta e="T631" id="Seg_9511" s="T630">post</ta>
            <ta e="T632" id="Seg_9512" s="T631">n</ta>
            <ta e="T633" id="Seg_9513" s="T632">adj</ta>
            <ta e="T634" id="Seg_9514" s="T633">cop</ta>
            <ta e="T635" id="Seg_9515" s="T634">n</ta>
            <ta e="T636" id="Seg_9516" s="T635">conj</ta>
            <ta e="T637" id="Seg_9517" s="T636">ptcl</ta>
            <ta e="T638" id="Seg_9518" s="T637">n</ta>
            <ta e="T639" id="Seg_9519" s="T638">conj</ta>
            <ta e="T640" id="Seg_9520" s="T639">ptcl</ta>
            <ta e="T641" id="Seg_9521" s="T640">n</ta>
            <ta e="T642" id="Seg_9522" s="T641">conj</ta>
            <ta e="T643" id="Seg_9523" s="T642">ptcl</ta>
            <ta e="T653" id="Seg_9524" s="T652">n</ta>
            <ta e="T654" id="Seg_9525" s="T653">adj</ta>
            <ta e="T655" id="Seg_9526" s="T654">ptcl</ta>
            <ta e="T656" id="Seg_9527" s="T655">v</ta>
            <ta e="T657" id="Seg_9528" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_9529" s="T657">n</ta>
            <ta e="T659" id="Seg_9530" s="T658">que</ta>
            <ta e="T661" id="Seg_9531" s="T660">v</ta>
            <ta e="T662" id="Seg_9532" s="T661">adv</ta>
            <ta e="T663" id="Seg_9533" s="T662">n</ta>
            <ta e="T664" id="Seg_9534" s="T663">ptcl</ta>
            <ta e="T665" id="Seg_9535" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_9536" s="T665">dempro</ta>
            <ta e="T667" id="Seg_9537" s="T666">post</ta>
            <ta e="T669" id="Seg_9538" s="T667">n</ta>
            <ta e="T694" id="Seg_9539" s="T693">v</ta>
            <ta e="T695" id="Seg_9540" s="T694">v</ta>
            <ta e="T696" id="Seg_9541" s="T695">ptcl</ta>
            <ta e="T697" id="Seg_9542" s="T696">n</ta>
            <ta e="T698" id="Seg_9543" s="T697">n</ta>
            <ta e="T699" id="Seg_9544" s="T698">adj</ta>
            <ta e="T700" id="Seg_9545" s="T699">ptcl</ta>
            <ta e="T701" id="Seg_9546" s="T700">cop</ta>
            <ta e="T702" id="Seg_9547" s="T701">n</ta>
            <ta e="T703" id="Seg_9548" s="T702">post</ta>
            <ta e="T704" id="Seg_9549" s="T703">v</ta>
            <ta e="T705" id="Seg_9550" s="T704">v</ta>
            <ta e="T706" id="Seg_9551" s="T705">ptcl</ta>
            <ta e="T707" id="Seg_9552" s="T706">dempro</ta>
            <ta e="T708" id="Seg_9553" s="T707">n</ta>
            <ta e="T709" id="Seg_9554" s="T708">v</ta>
            <ta e="T710" id="Seg_9555" s="T709">v</ta>
            <ta e="T711" id="Seg_9556" s="T710">quant</ta>
            <ta e="T712" id="Seg_9557" s="T711">n</ta>
            <ta e="T715" id="Seg_9558" s="T714">quant</ta>
            <ta e="T716" id="Seg_9559" s="T715">n</ta>
            <ta e="T717" id="Seg_9560" s="T716">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-ChSA">
            <ta e="T7" id="Seg_9561" s="T6">0.1.h:Poss np.h:Th</ta>
            <ta e="T8" id="Seg_9562" s="T7">0.1.h:Poss np.h:Th</ta>
            <ta e="T14" id="Seg_9563" s="T13">np:Th</ta>
            <ta e="T15" id="Seg_9564" s="T14">np:Th</ta>
            <ta e="T16" id="Seg_9565" s="T15">0.3.h:E</ta>
            <ta e="T23" id="Seg_9566" s="T22">0.3.h:A</ta>
            <ta e="T25" id="Seg_9567" s="T24">0.1.h:A</ta>
            <ta e="T28" id="Seg_9568" s="T27">adv:Time</ta>
            <ta e="T33" id="Seg_9569" s="T32">0.1.h:Poss np:Th</ta>
            <ta e="T37" id="Seg_9570" s="T35">pp:Time</ta>
            <ta e="T38" id="Seg_9571" s="T37">0.1.h:Th</ta>
            <ta e="T42" id="Seg_9572" s="T41">0.1.h:Th</ta>
            <ta e="T48" id="Seg_9573" s="T47">0.1.h:Th</ta>
            <ta e="T50" id="Seg_9574" s="T49">np:L</ta>
            <ta e="T53" id="Seg_9575" s="T52">np:L</ta>
            <ta e="T56" id="Seg_9576" s="T55">0.1.h:Poss np.h:Th</ta>
            <ta e="T57" id="Seg_9577" s="T56">0.1.h:A</ta>
            <ta e="T64" id="Seg_9578" s="T63">0.1.h:A</ta>
            <ta e="T66" id="Seg_9579" s="T65">np:Th</ta>
            <ta e="T69" id="Seg_9580" s="T68">adv:Time</ta>
            <ta e="T70" id="Seg_9581" s="T69">0.3:Th</ta>
            <ta e="T72" id="Seg_9582" s="T71">0.3:Th</ta>
            <ta e="T73" id="Seg_9583" s="T72">np.h:Poss</ta>
            <ta e="T74" id="Seg_9584" s="T73">np:Th</ta>
            <ta e="T80" id="Seg_9585" s="T79">n:Time</ta>
            <ta e="T81" id="Seg_9586" s="T80">0.1.h:A</ta>
            <ta e="T82" id="Seg_9587" s="T81">n:Time</ta>
            <ta e="T83" id="Seg_9588" s="T82">adv:Time</ta>
            <ta e="T90" id="Seg_9589" s="T89">np:Poss</ta>
            <ta e="T91" id="Seg_9590" s="T90">np:L</ta>
            <ta e="T97" id="Seg_9591" s="T96">np:St</ta>
            <ta e="T98" id="Seg_9592" s="T97">pro:L</ta>
            <ta e="T101" id="Seg_9593" s="T100">pro:L</ta>
            <ta e="T104" id="Seg_9594" s="T103">pro.h:A</ta>
            <ta e="T109" id="Seg_9595" s="T108">0.1.h:Th</ta>
            <ta e="T110" id="Seg_9596" s="T109">0.1.h:Poss np:Th</ta>
            <ta e="T115" id="Seg_9597" s="T114">np:Th</ta>
            <ta e="T116" id="Seg_9598" s="T115">0.1.h:A</ta>
            <ta e="T119" id="Seg_9599" s="T118">np:G</ta>
            <ta e="T120" id="Seg_9600" s="T119">0.1.h:A</ta>
            <ta e="T121" id="Seg_9601" s="T120">np:G</ta>
            <ta e="T122" id="Seg_9602" s="T121">0.1.h:A</ta>
            <ta e="T130" id="Seg_9603" s="T129">0.3:Th</ta>
            <ta e="T133" id="Seg_9604" s="T131">pp:Time</ta>
            <ta e="T135" id="Seg_9605" s="T134">adv:Time</ta>
            <ta e="T136" id="Seg_9606" s="T135">0.1.h:Poss np.h:A</ta>
            <ta e="T137" id="Seg_9607" s="T136">pro:Th</ta>
            <ta e="T139" id="Seg_9608" s="T138">0.1.h:Poss</ta>
            <ta e="T141" id="Seg_9609" s="T140">0.1.h:Poss np.h:Th</ta>
            <ta e="T149" id="Seg_9610" s="T148">0.1.h:Th</ta>
            <ta e="T150" id="Seg_9611" s="T149">np.h:Th</ta>
            <ta e="T167" id="Seg_9612" s="T166">0.1.h:Poss np.h:Th</ta>
            <ta e="T180" id="Seg_9613" s="T179">pro.h:A</ta>
            <ta e="T184" id="Seg_9614" s="T183">np:Th</ta>
            <ta e="T185" id="Seg_9615" s="T184">0.2.h:E</ta>
            <ta e="T187" id="Seg_9616" s="T186">0.1.h:A</ta>
            <ta e="T189" id="Seg_9617" s="T188">np:Th</ta>
            <ta e="T190" id="Seg_9618" s="T189">0.2.h:E</ta>
            <ta e="T191" id="Seg_9619" s="T190">np:Th</ta>
            <ta e="T192" id="Seg_9620" s="T191">0.2.h:A</ta>
            <ta e="T193" id="Seg_9621" s="T192">0.1.h:A</ta>
            <ta e="T194" id="Seg_9622" s="T193">adv:Time</ta>
            <ta e="T196" id="Seg_9623" s="T195">0.2.h:A</ta>
            <ta e="T197" id="Seg_9624" s="T196">0.1.h:A</ta>
            <ta e="T198" id="Seg_9625" s="T197">pro:L</ta>
            <ta e="T199" id="Seg_9626" s="T198">0.2.h:A</ta>
            <ta e="T200" id="Seg_9627" s="T199">pro:G</ta>
            <ta e="T201" id="Seg_9628" s="T200">0.2.h:A</ta>
            <ta e="T206" id="Seg_9629" s="T205">np:A</ta>
            <ta e="T208" id="Seg_9630" s="T207">adv:Time</ta>
            <ta e="T211" id="Seg_9631" s="T210">0.3:A</ta>
            <ta e="T222" id="Seg_9632" s="T221">0.1.h:A</ta>
            <ta e="T225" id="Seg_9633" s="T224">adv:Time</ta>
            <ta e="T227" id="Seg_9634" s="T226">adv:Time</ta>
            <ta e="T232" id="Seg_9635" s="T231">n:Time</ta>
            <ta e="T233" id="Seg_9636" s="T232">0.1.h:A</ta>
            <ta e="T238" id="Seg_9637" s="T237">0.1.h:A</ta>
            <ta e="T241" id="Seg_9638" s="T240">0.1.h:A</ta>
            <ta e="T242" id="Seg_9639" s="T241">0.1.h:Poss np:Th</ta>
            <ta e="T257" id="Seg_9640" s="T256">0.3:Th</ta>
            <ta e="T261" id="Seg_9641" s="T260">0.3:Th</ta>
            <ta e="T263" id="Seg_9642" s="T262">pro:G</ta>
            <ta e="T264" id="Seg_9643" s="T263">np:G</ta>
            <ta e="T268" id="Seg_9644" s="T267">np:A</ta>
            <ta e="T271" id="Seg_9645" s="T270">np:A</ta>
            <ta e="T273" id="Seg_9646" s="T272">n:Time</ta>
            <ta e="T274" id="Seg_9647" s="T273">adv:Time</ta>
            <ta e="T275" id="Seg_9648" s="T274">n:Time</ta>
            <ta e="T277" id="Seg_9649" s="T276">np:A</ta>
            <ta e="T281" id="Seg_9650" s="T280">0.3:Th</ta>
            <ta e="T283" id="Seg_9651" s="T282">0.3:Poss</ta>
            <ta e="T287" id="Seg_9652" s="T286">0.3:A</ta>
            <ta e="T302" id="Seg_9653" s="T301">0.3.h:A</ta>
            <ta e="T303" id="Seg_9654" s="T302">adv:Time</ta>
            <ta e="T305" id="Seg_9655" s="T304">0.3:Th</ta>
            <ta e="T307" id="Seg_9656" s="T306">0.3.h:A</ta>
            <ta e="T309" id="Seg_9657" s="T308">adv:Time</ta>
            <ta e="T311" id="Seg_9658" s="T310">0.3.h:A</ta>
            <ta e="T312" id="Seg_9659" s="T311">adv:Time</ta>
            <ta e="T316" id="Seg_9660" s="T315">0.3.h:A</ta>
            <ta e="T320" id="Seg_9661" s="T319">pro.h:Poss</ta>
            <ta e="T321" id="Seg_9662" s="T320">0.1.h:Poss np:Th</ta>
            <ta e="T323" id="Seg_9663" s="T322">0.1.h:Poss np:Th</ta>
            <ta e="T326" id="Seg_9664" s="T325">adv:Time</ta>
            <ta e="T331" id="Seg_9665" s="T330">np:Th</ta>
            <ta e="T334" id="Seg_9666" s="T333">pro.h:A</ta>
            <ta e="T335" id="Seg_9667" s="T334">0.2.h:A</ta>
            <ta e="T339" id="Seg_9668" s="T338">np:St</ta>
            <ta e="T340" id="Seg_9669" s="T339">0.3.h:E</ta>
            <ta e="T342" id="Seg_9670" s="T341">adv:Time</ta>
            <ta e="T343" id="Seg_9671" s="T342">0.1.h:Poss np.h:Th</ta>
            <ta e="T354" id="Seg_9672" s="T353">0.1.h:A</ta>
            <ta e="T359" id="Seg_9673" s="T358">np:Th</ta>
            <ta e="T361" id="Seg_9674" s="T360">0.1.h:A</ta>
            <ta e="T364" id="Seg_9675" s="T363">pro.h:Poss</ta>
            <ta e="T366" id="Seg_9676" s="T365">np.h:Poss</ta>
            <ta e="T372" id="Seg_9677" s="T371">0.3.h:A</ta>
            <ta e="T373" id="Seg_9678" s="T372">n:Time</ta>
            <ta e="T381" id="Seg_9679" s="T380">0.3.h:A</ta>
            <ta e="T389" id="Seg_9680" s="T388">0.3.h:A</ta>
            <ta e="T391" id="Seg_9681" s="T390">pro.h:Poss</ta>
            <ta e="T398" id="Seg_9682" s="T397">np:Poss</ta>
            <ta e="T399" id="Seg_9683" s="T398">np:Th</ta>
            <ta e="T404" id="Seg_9684" s="T402">pp:Cau</ta>
            <ta e="T406" id="Seg_9685" s="T405">np:Th</ta>
            <ta e="T407" id="Seg_9686" s="T406">0.3.h:A</ta>
            <ta e="T409" id="Seg_9687" s="T408">np:Cau</ta>
            <ta e="T412" id="Seg_9688" s="T411">np:Th</ta>
            <ta e="T421" id="Seg_9689" s="T420">np.h:P</ta>
            <ta e="T426" id="Seg_9690" s="T424">pp:Com</ta>
            <ta e="T444" id="Seg_9691" s="T443">np:L</ta>
            <ta e="T445" id="Seg_9692" s="T444">pro:Th</ta>
            <ta e="T447" id="Seg_9693" s="T446">np:Th</ta>
            <ta e="T449" id="Seg_9694" s="T448">0.3.h:A</ta>
            <ta e="T450" id="Seg_9695" s="T449">0.3.h:A</ta>
            <ta e="T451" id="Seg_9696" s="T450">0.3.h:E</ta>
            <ta e="T453" id="Seg_9697" s="T452">np:Th</ta>
            <ta e="T457" id="Seg_9698" s="T456">np.h:R</ta>
            <ta e="T461" id="Seg_9699" s="T460">np:Th</ta>
            <ta e="T463" id="Seg_9700" s="T462">n:Time</ta>
            <ta e="T465" id="Seg_9701" s="T464">pro.h:R</ta>
            <ta e="T466" id="Seg_9702" s="T465">pro.h:Poss</ta>
            <ta e="T471" id="Seg_9703" s="T470">pro:Th</ta>
            <ta e="T486" id="Seg_9704" s="T484">0.1.h:Poss np:Th</ta>
            <ta e="T491" id="Seg_9705" s="T489">pp:Time</ta>
            <ta e="T493" id="Seg_9706" s="T492">0.3.h:E</ta>
            <ta e="T495" id="Seg_9707" s="T494">pro:Th</ta>
            <ta e="T499" id="Seg_9708" s="T498">pro:Cau</ta>
            <ta e="T500" id="Seg_9709" s="T499">0.2.h:A</ta>
            <ta e="T501" id="Seg_9710" s="T500">pro:Th</ta>
            <ta e="T504" id="Seg_9711" s="T503">0.2.h:Poss np:Poss</ta>
            <ta e="T505" id="Seg_9712" s="T504">0.2.h:Poss np:Poss</ta>
            <ta e="T507" id="Seg_9713" s="T506">pro.h:A</ta>
            <ta e="T510" id="Seg_9714" s="T509">0.1.h:A</ta>
            <ta e="T511" id="Seg_9715" s="T510">0.2.h:A</ta>
            <ta e="T512" id="Seg_9716" s="T511">0.2.h:A</ta>
            <ta e="T513" id="Seg_9717" s="T512">pro:Cau</ta>
            <ta e="T514" id="Seg_9718" s="T513">np:Poss</ta>
            <ta e="T516" id="Seg_9719" s="T515">0.2.h:A</ta>
            <ta e="T517" id="Seg_9720" s="T516">0.1.h:A</ta>
            <ta e="T518" id="Seg_9721" s="T517">pro.h:Poss</ta>
            <ta e="T519" id="Seg_9722" s="T518">0.3:Th</ta>
            <ta e="T521" id="Seg_9723" s="T520">0.1.h:A</ta>
            <ta e="T523" id="Seg_9724" s="T521">pp:Cau</ta>
            <ta e="T524" id="Seg_9725" s="T523">np:Th</ta>
            <ta e="T525" id="Seg_9726" s="T524">0.2.h:R</ta>
            <ta e="T530" id="Seg_9727" s="T529">0.1.h:R</ta>
            <ta e="T531" id="Seg_9728" s="T530">0.2.h:E</ta>
            <ta e="T532" id="Seg_9729" s="T531">0.1.h:A</ta>
            <ta e="T538" id="Seg_9730" s="T537">0.1.h:A</ta>
            <ta e="T542" id="Seg_9731" s="T541">np.h:A</ta>
            <ta e="T550" id="Seg_9732" s="T549">np:Th</ta>
            <ta e="T551" id="Seg_9733" s="T550">0.3.h:A</ta>
            <ta e="T582" id="Seg_9734" s="T581">np.h:Th</ta>
            <ta e="T615" id="Seg_9735" s="T614">0.1.h:A</ta>
            <ta e="T617" id="Seg_9736" s="T616">0.1.h:Th</ta>
            <ta e="T619" id="Seg_9737" s="T617">pp:Com</ta>
            <ta e="T620" id="Seg_9738" s="T619">0.1.h:A</ta>
            <ta e="T624" id="Seg_9739" s="T623">0.1.h:Th</ta>
            <ta e="T627" id="Seg_9740" s="T626">0.1.h:Th</ta>
            <ta e="T629" id="Seg_9741" s="T628">0.1.h:Th</ta>
            <ta e="T634" id="Seg_9742" s="T633">0.1.h:A</ta>
            <ta e="T653" id="Seg_9743" s="T652">n:Time</ta>
            <ta e="T656" id="Seg_9744" s="T655">0.3.h:A</ta>
            <ta e="T658" id="Seg_9745" s="T657">np:Th</ta>
            <ta e="T661" id="Seg_9746" s="T660">0.2.h:A</ta>
            <ta e="T663" id="Seg_9747" s="T662">np:Th</ta>
            <ta e="T664" id="Seg_9748" s="T663">0.1.h:Poss</ta>
            <ta e="T667" id="Seg_9749" s="T665">pp:Cau</ta>
            <ta e="T695" id="Seg_9750" s="T694">0.2.h:A</ta>
            <ta e="T697" id="Seg_9751" s="T696">np:Th</ta>
            <ta e="T698" id="Seg_9752" s="T697">np:Th</ta>
            <ta e="T703" id="Seg_9753" s="T701">pp:Time</ta>
            <ta e="T705" id="Seg_9754" s="T704">0.2.h:A</ta>
            <ta e="T708" id="Seg_9755" s="T707">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-ChSA">
            <ta e="T7" id="Seg_9756" s="T6">np.h:S</ta>
            <ta e="T8" id="Seg_9757" s="T7">np.h:S</ta>
            <ta e="T11" id="Seg_9758" s="T10">adj:pred</ta>
            <ta e="T12" id="Seg_9759" s="T11">cop</ta>
            <ta e="T14" id="Seg_9760" s="T13">np:O</ta>
            <ta e="T15" id="Seg_9761" s="T14">np:O</ta>
            <ta e="T16" id="Seg_9762" s="T15">0.3.h:S v:pred</ta>
            <ta e="T22" id="Seg_9763" s="T21">np:O</ta>
            <ta e="T23" id="Seg_9764" s="T22">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_9765" s="T24">0.1.h:S v:pred</ta>
            <ta e="T33" id="Seg_9766" s="T32">np:S</ta>
            <ta e="T34" id="Seg_9767" s="T33">v:pred</ta>
            <ta e="T38" id="Seg_9768" s="T37">0.1.h:S v:pred</ta>
            <ta e="T42" id="Seg_9769" s="T41">0.1.h:S adj:pred</ta>
            <ta e="T47" id="Seg_9770" s="T43">s:adv</ta>
            <ta e="T48" id="Seg_9771" s="T47">0.1.h:S v:pred</ta>
            <ta e="T54" id="Seg_9772" s="T53">adj:pred</ta>
            <ta e="T57" id="Seg_9773" s="T55">s:purp</ta>
            <ta e="T63" id="Seg_9774" s="T61">s:adv</ta>
            <ta e="T64" id="Seg_9775" s="T63">0.1.h:S v:pred</ta>
            <ta e="T66" id="Seg_9776" s="T65">np:S</ta>
            <ta e="T67" id="Seg_9777" s="T66">n:pred</ta>
            <ta e="T70" id="Seg_9778" s="T69">0.3:S quant:pred</ta>
            <ta e="T72" id="Seg_9779" s="T71">0.3:S adj:pred</ta>
            <ta e="T74" id="Seg_9780" s="T73">np:S</ta>
            <ta e="T77" id="Seg_9781" s="T76">v:pred</ta>
            <ta e="T81" id="Seg_9782" s="T80">0.1.h:S v:pred</ta>
            <ta e="T97" id="Seg_9783" s="T96">np:S</ta>
            <ta e="T100" id="Seg_9784" s="T99">v:pred</ta>
            <ta e="T103" id="Seg_9785" s="T102">v:pred</ta>
            <ta e="T104" id="Seg_9786" s="T103">pro.h:S</ta>
            <ta e="T109" id="Seg_9787" s="T107">s:adv</ta>
            <ta e="T110" id="Seg_9788" s="T109">np:S</ta>
            <ta e="T111" id="Seg_9789" s="T110">v:pred</ta>
            <ta e="T115" id="Seg_9790" s="T114">np:O</ta>
            <ta e="T116" id="Seg_9791" s="T115">0.1.h:S v:pred</ta>
            <ta e="T120" id="Seg_9792" s="T119">0.1.h:S v:pred</ta>
            <ta e="T122" id="Seg_9793" s="T121">0.1.h:S v:pred</ta>
            <ta e="T130" id="Seg_9794" s="T129">0.3:S n:pred</ta>
            <ta e="T136" id="Seg_9795" s="T135">np.h:S</ta>
            <ta e="T137" id="Seg_9796" s="T136">pro:O</ta>
            <ta e="T138" id="Seg_9797" s="T137">v:pred</ta>
            <ta e="T141" id="Seg_9798" s="T140">0.1.h:S adj:pred</ta>
            <ta e="T149" id="Seg_9799" s="T148">0.1.h:S v:pred</ta>
            <ta e="T150" id="Seg_9800" s="T149">np.h:S</ta>
            <ta e="T151" id="Seg_9801" s="T150">v:pred</ta>
            <ta e="T167" id="Seg_9802" s="T166">np.h:S</ta>
            <ta e="T169" id="Seg_9803" s="T168">n:pred</ta>
            <ta e="T170" id="Seg_9804" s="T169">cop</ta>
            <ta e="T175" id="Seg_9805" s="T173">s:adv</ta>
            <ta e="T178" id="Seg_9806" s="T177">v:pred</ta>
            <ta e="T180" id="Seg_9807" s="T179">pro.h:S</ta>
            <ta e="T184" id="Seg_9808" s="T183">np:O</ta>
            <ta e="T185" id="Seg_9809" s="T184">0.2.h:S v:pred</ta>
            <ta e="T186" id="Seg_9810" s="T185">s:adv</ta>
            <ta e="T187" id="Seg_9811" s="T186">0.1.h:S v:pred</ta>
            <ta e="T189" id="Seg_9812" s="T188">np:O</ta>
            <ta e="T190" id="Seg_9813" s="T189">0.2.h:S v:pred</ta>
            <ta e="T191" id="Seg_9814" s="T190">np:O</ta>
            <ta e="T192" id="Seg_9815" s="T191">0.2.h:S v:pred</ta>
            <ta e="T193" id="Seg_9816" s="T192">0.1.h:S v:pred</ta>
            <ta e="T196" id="Seg_9817" s="T195">0.2.h:S v:pred</ta>
            <ta e="T197" id="Seg_9818" s="T196">0.1.h:S v:pred</ta>
            <ta e="T199" id="Seg_9819" s="T197">s:adv</ta>
            <ta e="T201" id="Seg_9820" s="T199">s:adv</ta>
            <ta e="T203" id="Seg_9821" s="T201">s:adv</ta>
            <ta e="T205" id="Seg_9822" s="T204">n:pred</ta>
            <ta e="T206" id="Seg_9823" s="T205">np:S</ta>
            <ta e="T207" id="Seg_9824" s="T206">v:pred</ta>
            <ta e="T211" id="Seg_9825" s="T210">0.3:S v:pred</ta>
            <ta e="T215" id="Seg_9826" s="T211">s:temp</ta>
            <ta e="T222" id="Seg_9827" s="T221">0.1.h:S v:pred</ta>
            <ta e="T233" id="Seg_9828" s="T232">0.1.h:S v:pred</ta>
            <ta e="T239" id="Seg_9829" s="T233">s:temp</ta>
            <ta e="T241" id="Seg_9830" s="T240">0.1.h:S v:pred</ta>
            <ta e="T243" id="Seg_9831" s="T241">s:adv</ta>
            <ta e="T257" id="Seg_9832" s="T256">0.3:S v:pred</ta>
            <ta e="T260" id="Seg_9833" s="T258">s:adv</ta>
            <ta e="T261" id="Seg_9834" s="T260">0.3:S v:pred</ta>
            <ta e="T265" id="Seg_9835" s="T261">s:adv</ta>
            <ta e="T267" id="Seg_9836" s="T265">s:adv</ta>
            <ta e="T268" id="Seg_9837" s="T267">np:S</ta>
            <ta e="T270" id="Seg_9838" s="T269">v:pred</ta>
            <ta e="T271" id="Seg_9839" s="T270">np:S</ta>
            <ta e="T272" id="Seg_9840" s="T271">v:pred</ta>
            <ta e="T277" id="Seg_9841" s="T276">np:S</ta>
            <ta e="T278" id="Seg_9842" s="T277">v:pred</ta>
            <ta e="T281" id="Seg_9843" s="T280">0.3:S v:pred</ta>
            <ta e="T287" id="Seg_9844" s="T286">0.3:S v:pred</ta>
            <ta e="T302" id="Seg_9845" s="T301">0.3.h:S v:pred</ta>
            <ta e="T306" id="Seg_9846" s="T303">s:cond</ta>
            <ta e="T307" id="Seg_9847" s="T306">0.3.h:S v:pred</ta>
            <ta e="T311" id="Seg_9848" s="T310">0.3.h:S v:pred</ta>
            <ta e="T316" id="Seg_9849" s="T315">0.3.h:S v:pred</ta>
            <ta e="T321" id="Seg_9850" s="T320">np:S</ta>
            <ta e="T323" id="Seg_9851" s="T322">np:S</ta>
            <ta e="T324" id="Seg_9852" s="T323">adj:pred</ta>
            <ta e="T325" id="Seg_9853" s="T324">cop</ta>
            <ta e="T327" id="Seg_9854" s="T326">adj:pred</ta>
            <ta e="T328" id="Seg_9855" s="T327">adj:pred</ta>
            <ta e="T329" id="Seg_9856" s="T328">cop</ta>
            <ta e="T331" id="Seg_9857" s="T330">np:S</ta>
            <ta e="T337" id="Seg_9858" s="T332">s:adv</ta>
            <ta e="T339" id="Seg_9859" s="T338">np:O</ta>
            <ta e="T340" id="Seg_9860" s="T339">0.3.h:S v:pred</ta>
            <ta e="T343" id="Seg_9861" s="T342">np.h:S</ta>
            <ta e="T344" id="Seg_9862" s="T343">adj:pred</ta>
            <ta e="T354" id="Seg_9863" s="T353">0.1.h:S v:pred</ta>
            <ta e="T357" id="Seg_9864" s="T355">s:purp</ta>
            <ta e="T360" id="Seg_9865" s="T357">s:purp</ta>
            <ta e="T361" id="Seg_9866" s="T360">0.1.h:S v:pred</ta>
            <ta e="T372" id="Seg_9867" s="T371">0.3.h:S v:pred</ta>
            <ta e="T382" id="Seg_9868" s="T380">s:adv</ta>
            <ta e="T390" id="Seg_9869" s="T386">s:adv</ta>
            <ta e="T399" id="Seg_9870" s="T398">np:S</ta>
            <ta e="T402" id="Seg_9871" s="T401">n:pred</ta>
            <ta e="T406" id="Seg_9872" s="T405">np:O</ta>
            <ta e="T407" id="Seg_9873" s="T406">0.3.h:S v:pred</ta>
            <ta e="T412" id="Seg_9874" s="T411">np:S</ta>
            <ta e="T415" id="Seg_9875" s="T414">adj:pred</ta>
            <ta e="T421" id="Seg_9876" s="T420">np.h:S</ta>
            <ta e="T422" id="Seg_9877" s="T421">v:pred</ta>
            <ta e="T427" id="Seg_9878" s="T422">s:adv</ta>
            <ta e="T445" id="Seg_9879" s="T444">pro:S</ta>
            <ta e="T446" id="Seg_9880" s="T445">cop</ta>
            <ta e="T447" id="Seg_9881" s="T446">np:O</ta>
            <ta e="T449" id="Seg_9882" s="T448">0.3.h:S v:pred</ta>
            <ta e="T450" id="Seg_9883" s="T449">s:comp</ta>
            <ta e="T451" id="Seg_9884" s="T450">0.3.h:S v:pred</ta>
            <ta e="T453" id="Seg_9885" s="T452">np:O</ta>
            <ta e="T454" id="Seg_9886" s="T453">v:pred</ta>
            <ta e="T457" id="Seg_9887" s="T456">np.h:S</ta>
            <ta e="T461" id="Seg_9888" s="T460">np:O</ta>
            <ta e="T464" id="Seg_9889" s="T463">v:pred</ta>
            <ta e="T465" id="Seg_9890" s="T464">pro.h:S</ta>
            <ta e="T466" id="Seg_9891" s="T465">pro.h:S</ta>
            <ta e="T474" id="Seg_9892" s="T472">ptcl:pred</ta>
            <ta e="T477" id="Seg_9893" s="T475">ptcl:pred</ta>
            <ta e="T486" id="Seg_9894" s="T484">np:S</ta>
            <ta e="T487" id="Seg_9895" s="T486">quant:pred</ta>
            <ta e="T493" id="Seg_9896" s="T492">0.3.h:S v:pred</ta>
            <ta e="T495" id="Seg_9897" s="T494">pro:O</ta>
            <ta e="T498" id="Seg_9898" s="T496">s:adv</ta>
            <ta e="T500" id="Seg_9899" s="T499">0.2.h:S v:pred</ta>
            <ta e="T501" id="Seg_9900" s="T500">pro:O</ta>
            <ta e="T503" id="Seg_9901" s="T501">s:purp</ta>
            <ta e="T507" id="Seg_9902" s="T506">pro.h:S</ta>
            <ta e="T508" id="Seg_9903" s="T507">v:pred</ta>
            <ta e="T510" id="Seg_9904" s="T509">0.1.h:S v:pred</ta>
            <ta e="T511" id="Seg_9905" s="T510">0.2.h:S v:pred</ta>
            <ta e="T512" id="Seg_9906" s="T511">0.2.h:S v:pred</ta>
            <ta e="T516" id="Seg_9907" s="T515">0.2.h:S v:pred</ta>
            <ta e="T517" id="Seg_9908" s="T516">0.1.h:S v:pred</ta>
            <ta e="T519" id="Seg_9909" s="T518">0.3:S n:pred</ta>
            <ta e="T521" id="Seg_9910" s="T520">0.1.h:S v:pred</ta>
            <ta e="T524" id="Seg_9911" s="T523">np:O</ta>
            <ta e="T525" id="Seg_9912" s="T524">0.2.h:S v:pred</ta>
            <ta e="T530" id="Seg_9913" s="T526">s:comp</ta>
            <ta e="T531" id="Seg_9914" s="T530">0.2.h:S v:pred</ta>
            <ta e="T532" id="Seg_9915" s="T531">0.1.h:S v:pred</ta>
            <ta e="T533" id="Seg_9916" s="T532">np:S</ta>
            <ta e="T534" id="Seg_9917" s="T533">cop</ta>
            <ta e="T538" id="Seg_9918" s="T537">0.1.h:S v:pred</ta>
            <ta e="T542" id="Seg_9919" s="T541">np.h:S</ta>
            <ta e="T544" id="Seg_9920" s="T543">v:pred</ta>
            <ta e="T550" id="Seg_9921" s="T549">np:O</ta>
            <ta e="T551" id="Seg_9922" s="T550">0.3.h:S v:pred</ta>
            <ta e="T582" id="Seg_9923" s="T581">np.h:S</ta>
            <ta e="T583" id="Seg_9924" s="T582">adj:pred</ta>
            <ta e="T616" id="Seg_9925" s="T613">s:adv</ta>
            <ta e="T617" id="Seg_9926" s="T616">0.1.h:S v:pred</ta>
            <ta e="T621" id="Seg_9927" s="T617">s:adv</ta>
            <ta e="T624" id="Seg_9928" s="T621">s:temp</ta>
            <ta e="T626" id="Seg_9929" s="T625">adj:pred</ta>
            <ta e="T627" id="Seg_9930" s="T626">0.1.h:S cop</ta>
            <ta e="T631" id="Seg_9931" s="T628">s:adv</ta>
            <ta e="T633" id="Seg_9932" s="T632">adj:pred</ta>
            <ta e="T634" id="Seg_9933" s="T633">0.1.h:S v:pred</ta>
            <ta e="T656" id="Seg_9934" s="T655">0.3.h:S v:pred</ta>
            <ta e="T658" id="Seg_9935" s="T657">np:O</ta>
            <ta e="T661" id="Seg_9936" s="T660">0.2.h:S v:pred</ta>
            <ta e="T664" id="Seg_9937" s="T663">0.1.h:S ptcl:pred</ta>
            <ta e="T694" id="Seg_9938" s="T693">s:comp</ta>
            <ta e="T695" id="Seg_9939" s="T694">0.2.h:S v:pred</ta>
            <ta e="T697" id="Seg_9940" s="T696">np:O</ta>
            <ta e="T698" id="Seg_9941" s="T697">np:S</ta>
            <ta e="T699" id="Seg_9942" s="T698">adj:pred</ta>
            <ta e="T701" id="Seg_9943" s="T700">cop</ta>
            <ta e="T704" id="Seg_9944" s="T703">s:adv</ta>
            <ta e="T705" id="Seg_9945" s="T704">0.2.h:S v:pred</ta>
            <ta e="T710" id="Seg_9946" s="T706">s:purp</ta>
            <ta e="T716" id="Seg_9947" s="T715">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-ChSA">
            <ta e="T7" id="Seg_9948" s="T6">giv-active</ta>
            <ta e="T8" id="Seg_9949" s="T7">giv-active</ta>
            <ta e="T16" id="Seg_9950" s="T15">0.giv-active</ta>
            <ta e="T23" id="Seg_9951" s="T22">0.giv-active</ta>
            <ta e="T25" id="Seg_9952" s="T24">0.accs-sit</ta>
            <ta e="T26" id="Seg_9953" s="T25">giv-active</ta>
            <ta e="T33" id="Seg_9954" s="T32">accs-inf</ta>
            <ta e="T38" id="Seg_9955" s="T37">0.giv-inactive</ta>
            <ta e="T42" id="Seg_9956" s="T41">0.giv-active</ta>
            <ta e="T48" id="Seg_9957" s="T47">0.giv-active</ta>
            <ta e="T50" id="Seg_9958" s="T49">accs-inf</ta>
            <ta e="T53" id="Seg_9959" s="T52">giv-active</ta>
            <ta e="T56" id="Seg_9960" s="T55">accs-inf</ta>
            <ta e="T57" id="Seg_9961" s="T56">0.giv-inactive</ta>
            <ta e="T62" id="Seg_9962" s="T61">accs-inf</ta>
            <ta e="T64" id="Seg_9963" s="T63">0.giv-active</ta>
            <ta e="T66" id="Seg_9964" s="T65">accs-gen</ta>
            <ta e="T81" id="Seg_9965" s="T80">0.accs-inf</ta>
            <ta e="T90" id="Seg_9966" s="T89">giv-active</ta>
            <ta e="T91" id="Seg_9967" s="T90">giv-active</ta>
            <ta e="T97" id="Seg_9968" s="T96">accs-inf</ta>
            <ta e="T104" id="Seg_9969" s="T103">giv-inactive</ta>
            <ta e="T109" id="Seg_9970" s="T108">0.giv-active</ta>
            <ta e="T110" id="Seg_9971" s="T109">accs-inf</ta>
            <ta e="T116" id="Seg_9972" s="T115">0.giv-active</ta>
            <ta e="T120" id="Seg_9973" s="T119">0.giv-active</ta>
            <ta e="T122" id="Seg_9974" s="T121">0.giv-active</ta>
            <ta e="T130" id="Seg_9975" s="T129">0.giv-active</ta>
            <ta e="T136" id="Seg_9976" s="T135">accs-inf</ta>
            <ta e="T137" id="Seg_9977" s="T136">giv-active</ta>
            <ta e="T139" id="Seg_9978" s="T138">giv-active</ta>
            <ta e="T141" id="Seg_9979" s="T140">0.giv-inactive giv-active</ta>
            <ta e="T149" id="Seg_9980" s="T148">0.accs-aggr</ta>
            <ta e="T150" id="Seg_9981" s="T149">giv-active</ta>
            <ta e="T167" id="Seg_9982" s="T166">accs-inf</ta>
            <ta e="T180" id="Seg_9983" s="T179">giv-inactive</ta>
            <ta e="T184" id="Seg_9984" s="T183">giv-inactive-Q</ta>
            <ta e="T185" id="Seg_9985" s="T184">0.giv-active-Q</ta>
            <ta e="T186" id="Seg_9986" s="T185">0.quot-th</ta>
            <ta e="T187" id="Seg_9987" s="T186">0.giv-inactive 0.quot-sp</ta>
            <ta e="T189" id="Seg_9988" s="T188">giv-inactive-Q</ta>
            <ta e="T190" id="Seg_9989" s="T189">0.giv-inactive-Q</ta>
            <ta e="T191" id="Seg_9990" s="T190">accs-sit-Q</ta>
            <ta e="T192" id="Seg_9991" s="T191">0.giv-active-Q</ta>
            <ta e="T193" id="Seg_9992" s="T192">0.giv-active 0.quot-sp</ta>
            <ta e="T196" id="Seg_9993" s="T195">0.giv-active-Q</ta>
            <ta e="T197" id="Seg_9994" s="T196">0.giv-active 0.quot-sp</ta>
            <ta e="T199" id="Seg_9995" s="T198">0.giv-active-Q</ta>
            <ta e="T201" id="Seg_9996" s="T200">0.giv-active-Q</ta>
            <ta e="T202" id="Seg_9997" s="T201">giv-inactive</ta>
            <ta e="T211" id="Seg_9998" s="T210">0.giv-active</ta>
            <ta e="T222" id="Seg_9999" s="T221">0.giv-inactive</ta>
            <ta e="T233" id="Seg_10000" s="T232">0.giv-active</ta>
            <ta e="T237" id="Seg_10001" s="T236">accs-inf</ta>
            <ta e="T238" id="Seg_10002" s="T237">0.giv-active</ta>
            <ta e="T241" id="Seg_10003" s="T240">0.giv-active</ta>
            <ta e="T242" id="Seg_10004" s="T241">giv-inactive</ta>
            <ta e="T257" id="Seg_10005" s="T256">0.giv-active</ta>
            <ta e="T261" id="Seg_10006" s="T260">0.giv-active</ta>
            <ta e="T264" id="Seg_10007" s="T263">accs-gen</ta>
            <ta e="T268" id="Seg_10008" s="T267">accs-gen</ta>
            <ta e="T271" id="Seg_10009" s="T270">giv-active</ta>
            <ta e="T273" id="Seg_10010" s="T272">accs-inf</ta>
            <ta e="T275" id="Seg_10011" s="T274">accs-inf</ta>
            <ta e="T277" id="Seg_10012" s="T276">accs-gen</ta>
            <ta e="T281" id="Seg_10013" s="T280">0.giv-inactive</ta>
            <ta e="T283" id="Seg_10014" s="T282">accs-inf</ta>
            <ta e="T287" id="Seg_10015" s="T286">0.giv-active</ta>
            <ta e="T302" id="Seg_10016" s="T301">0.giv-inactive</ta>
            <ta e="T307" id="Seg_10017" s="T306">0.giv-active</ta>
            <ta e="T311" id="Seg_10018" s="T310">0.giv-active</ta>
            <ta e="T316" id="Seg_10019" s="T315">0.giv-active</ta>
            <ta e="T320" id="Seg_10020" s="T319">giv-inactive</ta>
            <ta e="T323" id="Seg_10021" s="T322">accs-inf</ta>
            <ta e="T330" id="Seg_10022" s="T329">giv-inactive</ta>
            <ta e="T331" id="Seg_10023" s="T330">giv-active</ta>
            <ta e="T334" id="Seg_10024" s="T333">giv-inactive</ta>
            <ta e="T335" id="Seg_10025" s="T334">0.giv-inactive-Q</ta>
            <ta e="T336" id="Seg_10026" s="T335">0.quot-sp</ta>
            <ta e="T340" id="Seg_10027" s="T339">0.giv-active</ta>
            <ta e="T343" id="Seg_10028" s="T342">accs-inf</ta>
            <ta e="T354" id="Seg_10029" s="T353">0.giv-active</ta>
            <ta e="T359" id="Seg_10030" s="T358">giv-inactive</ta>
            <ta e="T361" id="Seg_10031" s="T360">0.giv-active</ta>
            <ta e="T364" id="Seg_10032" s="T363">new</ta>
            <ta e="T366" id="Seg_10033" s="T365">accs-gen</ta>
            <ta e="T367" id="Seg_10034" s="T366">accs-inf</ta>
            <ta e="T372" id="Seg_10035" s="T371">0.giv-inactive</ta>
            <ta e="T381" id="Seg_10036" s="T380">0.giv-active</ta>
            <ta e="T389" id="Seg_10037" s="T388">0.giv-active</ta>
            <ta e="T391" id="Seg_10038" s="T390">giv-active</ta>
            <ta e="T398" id="Seg_10039" s="T397">giv-inactive</ta>
            <ta e="T399" id="Seg_10040" s="T398">accs-inf</ta>
            <ta e="T406" id="Seg_10041" s="T405">giv-inactive</ta>
            <ta e="T407" id="Seg_10042" s="T406">0.giv-inactive</ta>
            <ta e="T409" id="Seg_10043" s="T408">new</ta>
            <ta e="T412" id="Seg_10044" s="T411">giv-inactive</ta>
            <ta e="T444" id="Seg_10045" s="T443">giv-active</ta>
            <ta e="T449" id="Seg_10046" s="T448">0.giv-inactive</ta>
            <ta e="T450" id="Seg_10047" s="T449">0.giv-active</ta>
            <ta e="T451" id="Seg_10048" s="T450">0.giv-active</ta>
            <ta e="T457" id="Seg_10049" s="T456">0.giv-inactive</ta>
            <ta e="T465" id="Seg_10050" s="T464">giv-active</ta>
            <ta e="T466" id="Seg_10051" s="T465">giv-inactive</ta>
            <ta e="T486" id="Seg_10052" s="T484">0.giv-inactive</ta>
            <ta e="T500" id="Seg_10053" s="T499">0.giv-inactive-Q</ta>
            <ta e="T504" id="Seg_10054" s="T503">new-Q</ta>
            <ta e="T505" id="Seg_10055" s="T504">giv-active-Q</ta>
            <ta e="T506" id="Seg_10056" s="T505">accs-inf-Q</ta>
            <ta e="T507" id="Seg_10057" s="T506">giv-active-Q</ta>
            <ta e="T510" id="Seg_10058" s="T509">0.giv-inactive 0.quot-sp</ta>
            <ta e="T511" id="Seg_10059" s="T510">0.giv-active-Q</ta>
            <ta e="T512" id="Seg_10060" s="T511">0.giv-active-Q</ta>
            <ta e="T514" id="Seg_10061" s="T513">giv-inactive-Q</ta>
            <ta e="T516" id="Seg_10062" s="T515">0.giv-active-Q</ta>
            <ta e="T517" id="Seg_10063" s="T516">0.giv-inactive 0.quot-sp</ta>
            <ta e="T518" id="Seg_10064" s="T517">giv-active-Q</ta>
            <ta e="T519" id="Seg_10065" s="T518">accs-inf-Q</ta>
            <ta e="T521" id="Seg_10066" s="T520">0.giv-active 0.quot-sp</ta>
            <ta e="T525" id="Seg_10067" s="T524">0.giv-active-Q</ta>
            <ta e="T530" id="Seg_10068" s="T529">0.accs-aggr-Q</ta>
            <ta e="T531" id="Seg_10069" s="T530">0.giv-active-Q</ta>
            <ta e="T532" id="Seg_10070" s="T531">0.giv-active 0.quot-sp</ta>
            <ta e="T538" id="Seg_10071" s="T537">0.giv-active</ta>
            <ta e="T542" id="Seg_10072" s="T541">giv-inactive</ta>
            <ta e="T546" id="Seg_10073" s="T545">giv-inactive</ta>
            <ta e="T551" id="Seg_10074" s="T550">0.giv-active</ta>
            <ta e="T615" id="Seg_10075" s="T614">0.giv-active</ta>
            <ta e="T617" id="Seg_10076" s="T616">0.giv-active</ta>
            <ta e="T620" id="Seg_10077" s="T619">0.giv-active</ta>
            <ta e="T624" id="Seg_10078" s="T623">0.giv-active</ta>
            <ta e="T627" id="Seg_10079" s="T626">0.giv-active</ta>
            <ta e="T629" id="Seg_10080" s="T628">0.giv-active</ta>
            <ta e="T634" id="Seg_10081" s="T633">0.giv-active</ta>
            <ta e="T656" id="Seg_10082" s="T655">0.giv-active</ta>
            <ta e="T664" id="Seg_10083" s="T663">0.giv-inactive</ta>
         </annotation>
         <annotation name="Top" tierref="Top-ChSA">
            <ta e="T7" id="Seg_10084" s="T6">top.int.concr</ta>
            <ta e="T8" id="Seg_10085" s="T7">top.int.concr</ta>
            <ta e="T16" id="Seg_10086" s="T15">0.top.int.concr</ta>
            <ta e="T23" id="Seg_10087" s="T22">0.top.int.concr</ta>
            <ta e="T25" id="Seg_10088" s="T24">0.top.int.concr</ta>
            <ta e="T33" id="Seg_10089" s="T32">top.int.concr</ta>
            <ta e="T38" id="Seg_10090" s="T37">0.top.int.concr</ta>
            <ta e="T42" id="Seg_10091" s="T41">0.top.int.concr</ta>
            <ta e="T48" id="Seg_10092" s="T47">0.top.int.concr</ta>
            <ta e="T53" id="Seg_10093" s="T52">top.int.concr</ta>
            <ta e="T64" id="Seg_10094" s="T63">0.top.int.concr</ta>
            <ta e="T66" id="Seg_10095" s="T64">top.int.concr</ta>
            <ta e="T70" id="Seg_10096" s="T69">0.top.int.concr</ta>
            <ta e="T72" id="Seg_10097" s="T71">0.top.int.concr</ta>
            <ta e="T74" id="Seg_10098" s="T72">top.int.concr</ta>
            <ta e="T81" id="Seg_10099" s="T80">0.top.int.concr</ta>
            <ta e="T97" id="Seg_10100" s="T96">top.int.concr</ta>
            <ta e="T101" id="Seg_10101" s="T100">top.int.concr</ta>
            <ta e="T109" id="Seg_10102" s="T108">0.top.int.concr</ta>
            <ta e="T110" id="Seg_10103" s="T109">top.int.concr</ta>
            <ta e="T116" id="Seg_10104" s="T115">0.top.int.concr</ta>
            <ta e="T120" id="Seg_10105" s="T119">0.top.int.concr</ta>
            <ta e="T122" id="Seg_10106" s="T121">0.top.int.concr</ta>
            <ta e="T130" id="Seg_10107" s="T129">0.top.int.concr</ta>
            <ta e="T141" id="Seg_10108" s="T140">0.top.int.concr</ta>
            <ta e="T149" id="Seg_10109" s="T148">0.top.int.concr</ta>
            <ta e="T150" id="Seg_10110" s="T149">top.int.concr</ta>
            <ta e="T167" id="Seg_10111" s="T166">top.int.concr</ta>
            <ta e="T187" id="Seg_10112" s="T186">0.top.int.concr</ta>
            <ta e="T194" id="Seg_10113" s="T193">top.int.concr</ta>
            <ta e="T203" id="Seg_10114" s="T201">top.int.concr</ta>
            <ta e="T211" id="Seg_10115" s="T210">0.top.int.concr</ta>
            <ta e="T222" id="Seg_10116" s="T221">0.top.int.concr</ta>
            <ta e="T233" id="Seg_10117" s="T232">0.top.int.concr</ta>
            <ta e="T239" id="Seg_10118" s="T233">top.int.concr</ta>
            <ta e="T257" id="Seg_10119" s="T256">0.top.int.concr</ta>
            <ta e="T261" id="Seg_10120" s="T260">0.top.int.concr</ta>
            <ta e="T271" id="Seg_10121" s="T270">top.int.concr</ta>
            <ta e="T273" id="Seg_10122" s="T272">top.int.concr</ta>
            <ta e="T275" id="Seg_10123" s="T273">top.int.concr</ta>
            <ta e="T281" id="Seg_10124" s="T280">0.top.int.concr</ta>
            <ta e="T287" id="Seg_10125" s="T286">0.top.int.concr</ta>
            <ta e="T302" id="Seg_10126" s="T301">0.top.int.concr</ta>
            <ta e="T307" id="Seg_10127" s="T306">0.top.int.concr</ta>
            <ta e="T309" id="Seg_10128" s="T308">top.int.concr</ta>
            <ta e="T312" id="Seg_10129" s="T311">top.int.concr.contr</ta>
            <ta e="T323" id="Seg_10130" s="T319">top.int.concr</ta>
            <ta e="T343" id="Seg_10131" s="T342">top.int.concr</ta>
            <ta e="T354" id="Seg_10132" s="T353">0.top.int.concr</ta>
            <ta e="T365" id="Seg_10133" s="T363">top.int.concr</ta>
            <ta e="T367" id="Seg_10134" s="T365">top.int.concr</ta>
            <ta e="T373" id="Seg_10135" s="T372">top.int.concr</ta>
            <ta e="T399" id="Seg_10136" s="T397">top.int.concr.contr</ta>
            <ta e="T404" id="Seg_10137" s="T402">top.int.concr</ta>
            <ta e="T412" id="Seg_10138" s="T410">top.int.concr</ta>
            <ta e="T421" id="Seg_10139" s="T419">top.int.concr</ta>
            <ta e="T444" id="Seg_10140" s="T441">top.int.concr</ta>
            <ta e="T447" id="Seg_10141" s="T446">top.int.concr</ta>
            <ta e="T451" id="Seg_10142" s="T450">0.top.int.concr</ta>
            <ta e="T466" id="Seg_10143" s="T465">top.int.concr</ta>
            <ta e="T486" id="Seg_10144" s="T484">top.int.concr</ta>
            <ta e="T511" id="Seg_10145" s="T510">0.top.int.concr</ta>
            <ta e="T512" id="Seg_10146" s="T511">0.top.int.concr</ta>
            <ta e="T519" id="Seg_10147" s="T518">0.top.int.concr</ta>
            <ta e="T523" id="Seg_10148" s="T521">top.int.concr</ta>
            <ta e="T538" id="Seg_10149" s="T537">0.top.int.concr</ta>
            <ta e="T542" id="Seg_10150" s="T541">top.int.concr</ta>
            <ta e="T551" id="Seg_10151" s="T550">0.top.int.concr</ta>
            <ta e="T582" id="Seg_10152" s="T580">top.int.concr</ta>
            <ta e="T624" id="Seg_10153" s="T621">top.int.concr</ta>
            <ta e="T634" id="Seg_10154" s="T633">0.top.int.concr</ta>
            <ta e="T656" id="Seg_10155" s="T655">0.top.int.concr</ta>
            <ta e="T664" id="Seg_10156" s="T663">0.top.int.concr</ta>
            <ta e="T698" id="Seg_10157" s="T697">top.int.concr</ta>
            <ta e="T712" id="Seg_10158" s="T710">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc-ChSA">
            <ta e="T13" id="Seg_10159" s="T8">foc.int</ta>
            <ta e="T16" id="Seg_10160" s="T13">foc.int</ta>
            <ta e="T23" id="Seg_10161" s="T21">foc.int</ta>
            <ta e="T25" id="Seg_10162" s="T23">foc.int</ta>
            <ta e="T35" id="Seg_10163" s="T33">foc.int</ta>
            <ta e="T38" id="Seg_10164" s="T35">foc.int</ta>
            <ta e="T42" id="Seg_10165" s="T39">foc.int</ta>
            <ta e="T48" id="Seg_10166" s="T42">foc.int</ta>
            <ta e="T54" id="Seg_10167" s="T53">foc.int</ta>
            <ta e="T64" id="Seg_10168" s="T61">foc.int</ta>
            <ta e="T67" id="Seg_10169" s="T66">foc.nar</ta>
            <ta e="T70" id="Seg_10170" s="T69">foc.int</ta>
            <ta e="T72" id="Seg_10171" s="T71">foc.int</ta>
            <ta e="T78" id="Seg_10172" s="T74">foc.int</ta>
            <ta e="T81" id="Seg_10173" s="T78">foc.int</ta>
            <ta e="T85" id="Seg_10174" s="T84">foc.nar</ta>
            <ta e="T91" id="Seg_10175" s="T89">foc.ver</ta>
            <ta e="T100" id="Seg_10176" s="T97">foc.int</ta>
            <ta e="T103" id="Seg_10177" s="T101">foc.int</ta>
            <ta e="T109" id="Seg_10178" s="T107">foc.int</ta>
            <ta e="T112" id="Seg_10179" s="T110">foc.int</ta>
            <ta e="T116" id="Seg_10180" s="T112">foc.int</ta>
            <ta e="T120" id="Seg_10181" s="T116">foc.int</ta>
            <ta e="T122" id="Seg_10182" s="T120">foc.int</ta>
            <ta e="T129" id="Seg_10183" s="T128">foc.nar</ta>
            <ta e="T136" id="Seg_10184" s="T135">foc.nar</ta>
            <ta e="T141" id="Seg_10185" s="T139">foc.int</ta>
            <ta e="T149" id="Seg_10186" s="T148">foc.int</ta>
            <ta e="T151" id="Seg_10187" s="T150">foc.int</ta>
            <ta e="T170" id="Seg_10188" s="T167">foc.int</ta>
            <ta e="T178" id="Seg_10189" s="T173">foc.int</ta>
            <ta e="T185" id="Seg_10190" s="T183">foc.int</ta>
            <ta e="T187" id="Seg_10191" s="T186">foc.int</ta>
            <ta e="T190" id="Seg_10192" s="T188">foc.int</ta>
            <ta e="T192" id="Seg_10193" s="T190">foc.int</ta>
            <ta e="T196" id="Seg_10194" s="T194">foc.int</ta>
            <ta e="T205" id="Seg_10195" s="T203">foc.int</ta>
            <ta e="T211" id="Seg_10196" s="T208">foc.int</ta>
            <ta e="T221" id="Seg_10197" s="T219">foc.nar</ta>
            <ta e="T233" id="Seg_10198" s="T229">foc.int</ta>
            <ta e="T241" id="Seg_10199" s="T239">foc.nar</ta>
            <ta e="T258" id="Seg_10200" s="T256">foc.int</ta>
            <ta e="T264" id="Seg_10201" s="T263">foc.nar</ta>
            <ta e="T269" id="Seg_10202" s="T267">foc.nar</ta>
            <ta e="T272" id="Seg_10203" s="T271">foc.int</ta>
            <ta e="T280" id="Seg_10204" s="T276">foc.wid</ta>
            <ta e="T282" id="Seg_10205" s="T280">foc.int</ta>
            <ta e="T288" id="Seg_10206" s="T284">foc.int</ta>
            <ta e="T302" id="Seg_10207" s="T301">foc.ver</ta>
            <ta e="T307" id="Seg_10208" s="T306">foc.int</ta>
            <ta e="T311" id="Seg_10209" s="T309">foc.int</ta>
            <ta e="T316" id="Seg_10210" s="T312">foc.int</ta>
            <ta e="T325" id="Seg_10211" s="T323">foc.int</ta>
            <ta e="T329" id="Seg_10212" s="T326">foc.int</ta>
            <ta e="T344" id="Seg_10213" s="T343">foc.int</ta>
            <ta e="T354" id="Seg_10214" s="T353">foc.ver</ta>
            <ta e="T371" id="Seg_10215" s="T370">foc.nar</ta>
            <ta e="T376" id="Seg_10216" s="T373">foc.nar</ta>
            <ta e="T382" id="Seg_10217" s="T380">foc.contr</ta>
            <ta e="T390" id="Seg_10218" s="T386">foc.contr</ta>
            <ta e="T402" id="Seg_10219" s="T399">foc.int</ta>
            <ta e="T408" id="Seg_10220" s="T404">foc.int</ta>
            <ta e="T415" id="Seg_10221" s="T412">foc.nar</ta>
            <ta e="T427" id="Seg_10222" s="T421">foc.int</ta>
            <ta e="T446" id="Seg_10223" s="T445">foc.ver</ta>
            <ta e="T449" id="Seg_10224" s="T447">foc.nar</ta>
            <ta e="T450" id="Seg_10225" s="T449">foc.nar</ta>
            <ta e="T454" id="Seg_10226" s="T452">foc.int</ta>
            <ta e="T461" id="Seg_10227" s="T458">foc.nar</ta>
            <ta e="T474" id="Seg_10228" s="T466">foc.int</ta>
            <ta e="T480" id="Seg_10229" s="T478">foc.nar</ta>
            <ta e="T488" id="Seg_10230" s="T486">foc.int</ta>
            <ta e="T496" id="Seg_10231" s="T491">foc.wid</ta>
            <ta e="T499" id="Seg_10232" s="T498">foc.nar</ta>
            <ta e="T507" id="Seg_10233" s="T506">foc.nar</ta>
            <ta e="T511" id="Seg_10234" s="T510">foc.int</ta>
            <ta e="T512" id="Seg_10235" s="T511">foc.int</ta>
            <ta e="T513" id="Seg_10236" s="T512">foc.nar</ta>
            <ta e="T519" id="Seg_10237" s="T517">foc.nar</ta>
            <ta e="T525" id="Seg_10238" s="T523">foc.int</ta>
            <ta e="T527" id="Seg_10239" s="T526">foc.nar</ta>
            <ta e="T538" id="Seg_10240" s="T537">foc.ver</ta>
            <ta e="T546" id="Seg_10241" s="T542">foc.int</ta>
            <ta e="T551" id="Seg_10242" s="T550">foc.ver</ta>
            <ta e="T583" id="Seg_10243" s="T582">foc.nar</ta>
            <ta e="T616" id="Seg_10244" s="T612">foc.nar</ta>
            <ta e="T621" id="Seg_10245" s="T617">foc.nar</ta>
            <ta e="T627" id="Seg_10246" s="T624">foc.int</ta>
            <ta e="T634" id="Seg_10247" s="T631">foc.int</ta>
            <ta e="T654" id="Seg_10248" s="T652">foc.nar</ta>
            <ta e="T664" id="Seg_10249" s="T661">foc.int</ta>
            <ta e="T694" id="Seg_10250" s="T693">foc.nar</ta>
            <ta e="T701" id="Seg_10251" s="T698">foc.int</ta>
            <ta e="T710" id="Seg_10252" s="T701">foc.wid</ta>
            <ta e="T716" id="Seg_10253" s="T714">foc.nar</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR-ChSA">
            <ta e="T14" id="Seg_10254" s="T13">RUS:cult</ta>
            <ta e="T15" id="Seg_10255" s="T14">RUS:cult</ta>
            <ta e="T51" id="Seg_10256" s="T50">RUS:gram</ta>
            <ta e="T71" id="Seg_10257" s="T70">RUS:gram</ta>
            <ta e="T90" id="Seg_10258" s="T89">NGAN:cult</ta>
            <ta e="T105" id="Seg_10259" s="T104">RUS:gram</ta>
            <ta e="T115" id="Seg_10260" s="T114">RUS:cult</ta>
            <ta e="T119" id="Seg_10261" s="T118">RUS:cult</ta>
            <ta e="T121" id="Seg_10262" s="T120">RUS:cult</ta>
            <ta e="T130" id="Seg_10263" s="T129">RUS:cult</ta>
            <ta e="T139" id="Seg_10264" s="T138">RUS:cult</ta>
            <ta e="T167" id="Seg_10265" s="T166">RUS:cult</ta>
            <ta e="T195" id="Seg_10266" s="T194">RUS:cult</ta>
            <ta e="T226" id="Seg_10267" s="T225">RUS:gram</ta>
            <ta e="T228" id="Seg_10268" s="T227">RUS:gram</ta>
            <ta e="T237" id="Seg_10269" s="T236">RUS:cult</ta>
            <ta e="T240" id="Seg_10270" s="T239">RUS:mod</ta>
            <ta e="T264" id="Seg_10271" s="T263">YAK:cult</ta>
            <ta e="T269" id="Seg_10272" s="T268">RUS:gram</ta>
            <ta e="T273" id="Seg_10273" s="T272">RUS:cult</ta>
            <ta e="T284" id="Seg_10274" s="T283">RUS:gram</ta>
            <ta e="T310" id="Seg_10275" s="T309">RUS:cult</ta>
            <ta e="T313" id="Seg_10276" s="T312">RUS:cult</ta>
            <ta e="T323" id="Seg_10277" s="T322">RUS:cult</ta>
            <ta e="T359" id="Seg_10278" s="T358">RUS:cult</ta>
            <ta e="T366" id="Seg_10279" s="T365">YAK:cult</ta>
            <ta e="T373" id="Seg_10280" s="T372">RUS:cult</ta>
            <ta e="T376" id="Seg_10281" s="T375">RUS:cult</ta>
            <ta e="T394" id="Seg_10282" s="T393">RUS:core</ta>
            <ta e="T398" id="Seg_10283" s="T397">YAK:cult</ta>
            <ta e="T406" id="Seg_10284" s="T405">RUS:cult</ta>
            <ta e="T461" id="Seg_10285" s="T460">RUS:cult</ta>
            <ta e="T483" id="Seg_10286" s="T482">RUS:cult</ta>
            <ta e="T486" id="Seg_10287" s="T484">RUS:cult</ta>
            <ta e="T489" id="Seg_10288" s="T488">RUS:cult</ta>
            <ta e="T504" id="Seg_10289" s="T503">RUS:cult</ta>
            <ta e="T505" id="Seg_10290" s="T504">RUS:cult</ta>
            <ta e="T514" id="Seg_10291" s="T513">RUS:cult</ta>
            <ta e="T539" id="Seg_10292" s="T538">RUS:gram</ta>
            <ta e="T546" id="Seg_10293" s="T545">RUS:cult</ta>
            <ta e="T626" id="Seg_10294" s="T625">RUS:cult</ta>
            <ta e="T630" id="Seg_10295" s="T629">RUS:gram</ta>
            <ta e="T636" id="Seg_10296" s="T635">RUS:gram</ta>
            <ta e="T639" id="Seg_10297" s="T638">RUS:gram</ta>
            <ta e="T642" id="Seg_10298" s="T641">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-ChSA" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-ChSA" />
         <annotation name="CS" tierref="CS-ChSA" />
         <annotation name="fe" tierref="fe-ChSA">
            <ta e="T13" id="Seg_10299" s="T6">– Our fathers and our mothers were quite old.</ta>
            <ta e="T16" id="Seg_10300" s="T13">They don't know neither U nor A.</ta>
            <ta e="T32" id="Seg_10301" s="T16">A cross… they simply signed – so I do likewise, even today, like that.</ta>
            <ta e="T35" id="Seg_10302" s="T32">My mind weakened a lot.</ta>
            <ta e="T42" id="Seg_10303" s="T35">I was ill for one year, but regardless of that I am quite fine now.</ta>
            <ta e="T49" id="Seg_10304" s="T42">More or less people talk about me as a human. [?]</ta>
            <ta e="T52" id="Seg_10305" s="T49">Also at work.</ta>
            <ta e="T54" id="Seg_10306" s="T52">At work it is bad.</ta>
            <ta e="T61" id="Seg_10307" s="T54">Well, in order to bring my children, that all…</ta>
            <ta e="T64" id="Seg_10308" s="T61">With the last of my strength I was toiling.</ta>
            <ta e="T69" id="Seg_10309" s="T64">Reindeer herding was somehow holy earlier. </ta>
            <ta e="T72" id="Seg_10310" s="T69">There was a lot and it was hard.</ta>
            <ta e="T78" id="Seg_10311" s="T72">The human's strength was completely spent.</ta>
            <ta e="T83" id="Seg_10312" s="T78">In the polar night in winter we came around 11 o'clock.</ta>
            <ta e="T87" id="Seg_10313" s="T83">Oh, and very cold.</ta>
            <ta e="T96" id="Seg_10314" s="T89">– In the tundra of Popigaz, although there is everywhere forest, forest.</ta>
            <ta e="T100" id="Seg_10315" s="T96">The reindeer are nowhere visible.</ta>
            <ta e="T105" id="Seg_10316" s="T100">Well, I was toiling a lot at that time.</ta>
            <ta e="T112" id="Seg_10317" s="T105">But nevertheless I was happy and my reindeer were prospering.</ta>
            <ta e="T122" id="Seg_10318" s="T112">I held more than ten thousand, I gave them to some herds, I gave them to herds.</ta>
            <ta e="T133" id="Seg_10319" s="T127">– The second, it was always the second brigade, until today.</ta>
            <ta e="T138" id="Seg_10320" s="T133">Now my son is looking for it.</ta>
            <ta e="T139" id="Seg_10321" s="T138">My Volodya.</ta>
            <ta e="T141" id="Seg_10322" s="T139">I have one single son.</ta>
            <ta e="T151" id="Seg_10323" s="T147">– Mm, we are getting older, both of us became old.</ta>
            <ta e="T170" id="Seg_10324" s="T166">– Our brigade leaders were quite good people.</ta>
            <ta e="T180" id="Seg_10325" s="T170">Then by learning we apparently also gained experience.</ta>
            <ta e="T186" id="Seg_10326" s="T180">In worries and thinking "do not lose the reindeer".</ta>
            <ta e="T188" id="Seg_10327" s="T186">I always say:</ta>
            <ta e="T193" id="Seg_10328" s="T188">"Don't lose the reindeer, think about tomorrow", I always say. </ta>
            <ta e="T201" id="Seg_10329" s="T193">"Plan in the morning", I always say, "where you work, where you go".</ta>
            <ta e="T211" id="Seg_10330" s="T201">Many reindeer get lost (?), the mind does something in summer, they go very far away. </ta>
            <ta e="T217" id="Seg_10331" s="T211">So much gone through until…</ta>
            <ta e="T228" id="Seg_10332" s="T217">I always went out with a good name, both in summer and winter.</ta>
            <ta e="T243" id="Seg_10333" s="T228">However, I did work many years, after I have retired in the age of fifty-five, I am still working, because my reindeer are doing well.</ta>
            <ta e="T260" id="Seg_10334" s="T256">– They got lost, they died out.</ta>
            <ta e="T265" id="Seg_10335" s="T260">They got lost also having gone somewhere to Saskylaax.</ta>
            <ta e="T270" id="Seg_10336" s="T265">They run away or wild reindeer take them along.</ta>
            <ta e="T272" id="Seg_10337" s="T270">Wild reindeer always take [some].</ta>
            <ta e="T280" id="Seg_10338" s="T272">During the migration, during the arrival in autumn wolfs eat many [of them].</ta>
            <ta e="T288" id="Seg_10339" s="T280">And they become sick, their hooves, and then they can't go any longer.</ta>
            <ta e="T306" id="Seg_10340" s="T301">– It is allowed also today, if there are any.</ta>
            <ta e="T308" id="Seg_10341" s="T306">They say.</ta>
            <ta e="T316" id="Seg_10342" s="T308">Now they started to give premia, earlier there were no premia.</ta>
            <ta e="T332" id="Seg_10343" s="T319">– Our, our whatchamacallits, our rates were bad, earlier they were small, for our reindeer.</ta>
            <ta e="T346" id="Seg_10344" s="T332">There I said in vain "raise them", now one listens to the words, our workers are bad nowadays.</ta>
            <ta e="T357" id="Seg_10345" s="T352">– Mm, we say, in order to feed ourselves.</ta>
            <ta e="T361" id="Seg_10346" s="T357">That one should raise the rate for a reindeer, we say.</ta>
            <ta e="T372" id="Seg_10347" s="T361">We… their whatchamacallit, they from Urung Xaya, how much they said for a reindeer?</ta>
            <ta e="T376" id="Seg_10348" s="T372">Four, four rubles per day.</ta>
            <ta e="T384" id="Seg_10349" s="T380">– For guarding, of course, ha-ha.</ta>
            <ta e="T390" id="Seg_10350" s="T384">For guarding the reindeer.</ta>
            <ta e="T393" id="Seg_10351" s="T390">Their also…</ta>
            <ta e="T397" id="Seg_10352" s="T393">Blizzards do harm.</ta>
            <ta e="T402" id="Seg_10353" s="T397">The territory of Urung Xaya is very stark land.</ta>
            <ta e="T410" id="Seg_10354" s="T402">Therefore one raised their rates, from the beginning, because of the (insect clouds?), since long time.</ta>
            <ta e="T415" id="Seg_10355" s="T410">Reindeer herding is difficult.</ta>
            <ta e="T427" id="Seg_10356" s="T417">– Yes, a dying human dies together with his reindeer, he accompanies them.</ta>
            <ta e="T446" id="Seg_10357" s="T441">– In Yakutia there are such.</ta>
            <ta e="T449" id="Seg_10358" s="T446">There one esteems the reindeer a lot.</ta>
            <ta e="T451" id="Seg_10359" s="T449">One wants to raise them.</ta>
            <ta e="T458" id="Seg_10360" s="T451">Oh, there the reindeer herders make good money within a month. </ta>
            <ta e="T465" id="Seg_10361" s="T458">So four to five thousand they get per month.</ta>
            <ta e="T477" id="Seg_10362" s="T465">We don't have such thing, never, not at all…</ta>
            <ta e="T484" id="Seg_10363" s="T477">Simply, maybe 500 rubles, as advance.</ta>
            <ta e="T489" id="Seg_10364" s="T484">Our rate is very small, copecks.</ta>
            <ta e="T491" id="Seg_10365" s="T489">Until now.</ta>
            <ta e="T496" id="Seg_10366" s="T491">In the end everything is lost now.</ta>
            <ta e="T498" id="Seg_10367" s="T496">I have been saying in vain:</ta>
            <ta e="T510" id="Seg_10368" s="T498">"Why do you touch them, what for, the reindeer are from your sovkhoz, your sovkhoz, you yourselves eat", I always said.</ta>
            <ta e="T512" id="Seg_10369" s="T510">"You dress and so on."</ta>
            <ta e="T517" id="Seg_10370" s="T512">"Why do you say 'the sovkhoz ones'?", I say.</ta>
            <ta e="T521" id="Seg_10371" s="T517">"That's your own wealth", I say.</ta>
            <ta e="T532" id="Seg_10372" s="T521">"Therefore you get your money, what do you think, for what we get money", I say.</ta>
            <ta e="T537" id="Seg_10373" s="T532">It shall be a sin, if I say I betrayed a lot.</ta>
            <ta e="T540" id="Seg_10374" s="T537">Although I didn't sell either.</ta>
            <ta e="T546" id="Seg_10375" s="T540">Everything, everything was sold by the Kolkhoz people.</ta>
            <ta e="T551" id="Seg_10376" s="T549">– They earned money.</ta>
            <ta e="T573" id="Seg_10377" s="T571">– Yes, exactly.</ta>
            <ta e="T585" id="Seg_10378" s="T580">– There are not enough reindeer herders.</ta>
            <ta e="T621" id="Seg_10379" s="T612">– I became [that] because I worked well, because I talk well to people.</ta>
            <ta e="T628" id="Seg_10380" s="T621">Generally I was very agile when I was younger.</ta>
            <ta e="T634" id="Seg_10381" s="T628">Although I got sick, I always talked to people.</ta>
            <ta e="T643" id="Seg_10382" s="T634">Be it a Russian or a Dolgan or a foreigner.</ta>
            <ta e="T657" id="Seg_10383" s="T652">– The watched every day.</ta>
            <ta e="T661" id="Seg_10384" s="T657">A mistake, something (…) you make.</ta>
            <ta e="T669" id="Seg_10385" s="T661">I had not many mistakes, therefore, the Russians…</ta>
            <ta e="T697" id="Seg_10386" s="T693">– One had even to give account for the births.</ta>
            <ta e="T701" id="Seg_10387" s="T697">The calf was still very small.</ta>
            <ta e="T710" id="Seg_10388" s="T701">You go the whole night without sleeping to watch the birth of the reindeer calf.</ta>
            <ta e="T717" id="Seg_10389" s="T710">With many reindeer there are many calves, too.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-ChSA">
            <ta e="T13" id="Seg_10390" s="T6">– Unsere Väter, unsere Mütter waren ziemlich alt.</ta>
            <ta e="T16" id="Seg_10391" s="T13">Sie kennen kein U und kein A.</ta>
            <ta e="T32" id="Seg_10392" s="T16">Ein Kreuz… einfach unterschrieben sie – so mache ich das auch, auch jetzt noch, so auf diese Weise.</ta>
            <ta e="T35" id="Seg_10393" s="T32">Mein Verstand ist auch ziemlich weggeflogen.</ta>
            <ta e="T42" id="Seg_10394" s="T35">Ein Jahr lang war ich krank, aber jetzt geht es mir dennoch ganz gut.</ta>
            <ta e="T49" id="Seg_10395" s="T42">Mehr oder weniger reden die Leute über mich wie über einen Menschen. [?]</ta>
            <ta e="T52" id="Seg_10396" s="T49">Auch auf Arbeit.</ta>
            <ta e="T54" id="Seg_10397" s="T52">Auf Arbeit ist es schlecht.</ta>
            <ta e="T61" id="Seg_10398" s="T54">Nun, um meine Kinder groß zu bekommen, das alles ganz…</ta>
            <ta e="T64" id="Seg_10399" s="T61">Mit letzter Kraft habe ich geschuftet.</ta>
            <ta e="T69" id="Seg_10400" s="T64">Die Rentierzucht war früher irgendwie heilig. </ta>
            <ta e="T72" id="Seg_10401" s="T69">Viel gab es und schwierig war es.</ta>
            <ta e="T78" id="Seg_10402" s="T72">Die Kräfte des Menschen wurden ganz aufgezehrt.</ta>
            <ta e="T83" id="Seg_10403" s="T78">Gegen elf kamen wir in der Polarnacht im Winter.</ta>
            <ta e="T87" id="Seg_10404" s="T83">Oh, und sehr kalt.</ta>
            <ta e="T96" id="Seg_10405" s="T89">– In der Tundra von Popigaj, wenn da auch immer Wald, Wald ist.</ta>
            <ta e="T100" id="Seg_10406" s="T96">Die Rentiere sind nirgendwo zu sehen.</ta>
            <ta e="T105" id="Seg_10407" s="T100">Nun, damals schuftete ich sehr.</ta>
            <ta e="T112" id="Seg_10408" s="T105">Aber dennoch, ich war glücklich und meine Rentiere gediehen.</ta>
            <ta e="T122" id="Seg_10409" s="T112">Über zehntausend hielt ich, ich gab sie in einige Herden, ich gab sie in Herden.</ta>
            <ta e="T133" id="Seg_10410" s="T127">– Die zweite, die zweite Brigade war es immer, bis heute.</ta>
            <ta e="T138" id="Seg_10411" s="T133">Jetzt kümmert sich mein Sohn darum.</ta>
            <ta e="T139" id="Seg_10412" s="T138">Mein Volodja.</ta>
            <ta e="T141" id="Seg_10413" s="T139">Einen einzigen Sohn habe ich.</ta>
            <ta e="T151" id="Seg_10414" s="T147">– Mm, wir werden alt, wir beide sind alt geworden.</ta>
            <ta e="T170" id="Seg_10415" s="T166">– Unsere Brigadeleiter waren ganz gute Leute.</ta>
            <ta e="T180" id="Seg_10416" s="T170">Dann, durch das Lernen, sammelten wir wohl auch Erfahrung.</ta>
            <ta e="T186" id="Seg_10417" s="T180">In der Furcht und denkend "verliert nicht die Rentiere".</ta>
            <ta e="T188" id="Seg_10418" s="T186">Ich sage immer: </ta>
            <ta e="T193" id="Seg_10419" s="T188">"Verliert die Rentiere nicht, denkt an den morgigen Tag", sage ich immer.</ta>
            <ta e="T201" id="Seg_10420" s="T193">"Plant am Morgen", sage ich immer, "wo ihr arbeitet, wohin ihr geht."</ta>
            <ta e="T211" id="Seg_10421" s="T201">Es gehen viele Rentiere verloren (?), der Verstand macht etwas im Sommer, sie gehen sehr weit weg. </ta>
            <ta e="T217" id="Seg_10422" s="T211">So viel durchgemacht bis zu…</ta>
            <ta e="T228" id="Seg_10423" s="T217">Ich bin immer mit gutem Namen rausgegangen, sowohl im Sommer als auch im Winter.</ta>
            <ta e="T243" id="Seg_10424" s="T228">Dennoch habe ich viele Jahre gearbeitet, nachdem ich mit fünfundfünfzig Jahren in Rente gegangen bin, arbeitete ich immer noch, weil sich meine Rentiere gut gehalten haben.</ta>
            <ta e="T260" id="Seg_10425" s="T256">– Die sind verloren gegangen, gestorben.</ta>
            <ta e="T265" id="Seg_10426" s="T260">Verloren gegangen und irgendwohin nach Saskylaax gekommen.</ta>
            <ta e="T270" id="Seg_10427" s="T265">Sie laufen weg oder wilde Rentiere nehmen sie mit.</ta>
            <ta e="T272" id="Seg_10428" s="T270">Wilde Rentiere nehmen immer [welche] mit.</ta>
            <ta e="T280" id="Seg_10429" s="T272">Während der Wanderung, während der Ankunft im Herbst, frisst der Wolf sehr viele.</ta>
            <ta e="T288" id="Seg_10430" s="T280">Und sie werden krank, ihre Hufe, und dann gehen sie nicht mehr lange.</ta>
            <ta e="T306" id="Seg_10431" s="T301">– Es wird auch jetzt erlaubt, wenn es einen gibt.</ta>
            <ta e="T308" id="Seg_10432" s="T306">Sagen sie.</ta>
            <ta e="T316" id="Seg_10433" s="T308">Jetzt wurde angefangen, Prämien zu geben, früher wurde nicht prämiert.</ta>
            <ta e="T332" id="Seg_10434" s="T319">– Unsere, diese, unsere Tarife waren schlecht, früher waren sie klein, niedrig, für unsere Rentiere.</ta>
            <ta e="T346" id="Seg_10435" s="T332">Da habe ich vergeblich gesagt "erhöht sie", jetzt hört man die Worte, unsere Arbeiter sind jetzt schlecht.</ta>
            <ta e="T357" id="Seg_10436" s="T352">– Mm, sagen wir, um uns zu ernähren.</ta>
            <ta e="T361" id="Seg_10437" s="T357">Dass man den Tarif für ein Rentier erhöht, sagen wir.</ta>
            <ta e="T372" id="Seg_10438" s="T361">Wir… ihre da, die aus Ürüng Xaja, für ein Rentier wie viel sagten sie?</ta>
            <ta e="T376" id="Seg_10439" s="T372">Am Tag vier, vier Rubel.</ta>
            <ta e="T384" id="Seg_10440" s="T380">– Fürs Aufpassen natürlich, haha.</ta>
            <ta e="T390" id="Seg_10441" s="T384">Für das Aufpassen auf die Rentiere.</ta>
            <ta e="T393" id="Seg_10442" s="T390">Ihre auch…</ta>
            <ta e="T397" id="Seg_10443" s="T393">Schneestürme richten Schaden an.</ta>
            <ta e="T402" id="Seg_10444" s="T397">Das Gebiet von Ürüng Xaja ist sehr kahles Land. </ta>
            <ta e="T410" id="Seg_10445" s="T402">Deshalb hat man ihre Tarife erhöht, seit Anfang an, wegen der (Insektenwolken?), seit langem.</ta>
            <ta e="T415" id="Seg_10446" s="T410">Die Rentierzucht ist sehr schwierig.</ta>
            <ta e="T427" id="Seg_10447" s="T417">– Ja, ein sterbender, sterbender Mensch stirbt mit seinen Rentieren, er begleitet sie.</ta>
            <ta e="T446" id="Seg_10448" s="T441">– In Jakutien gab es solche.</ta>
            <ta e="T449" id="Seg_10449" s="T446">Dort wertschätzt man Rentiere sehr.</ta>
            <ta e="T451" id="Seg_10450" s="T449">Man möchte sie großziehen.</ta>
            <ta e="T458" id="Seg_10451" s="T451">Ach, da bekommen die Rentierhirten gutes Geld im Monat.</ta>
            <ta e="T465" id="Seg_10452" s="T458">So vier- bis fünftausend bekommen sie in einem Monat.</ta>
            <ta e="T477" id="Seg_10453" s="T465">Wir haben so etwas nicht, nie, überhaupt nicht…</ta>
            <ta e="T484" id="Seg_10454" s="T477">Einfach, 500 Rubel vielleicht, als Vorschuss.</ta>
            <ta e="T489" id="Seg_10455" s="T484">Unser Tarif ist ganz gering, Kopeken.</ta>
            <ta e="T491" id="Seg_10456" s="T489">Bis jetzt.</ta>
            <ta e="T496" id="Seg_10457" s="T491">Im Endeffekt hat man jetzt alles verloren.</ta>
            <ta e="T498" id="Seg_10458" s="T496">Ich habe vergeblich gesagt:</ta>
            <ta e="T510" id="Seg_10459" s="T498">"Warum rührt ihr sie an, um was zu machen, die Rentiere eurer Sowchose, eurer Sowchose, ihr esst doch selbst", sagte ich immer.</ta>
            <ta e="T512" id="Seg_10460" s="T510">"Ihr zieht euch an und so weiter."</ta>
            <ta e="T517" id="Seg_10461" s="T512">"Warum sagt ihr 'die der Sowchose'?", sage ich.</ta>
            <ta e="T521" id="Seg_10462" s="T517">"Das ist doch euer eigenes Kapital", sage ich.</ta>
            <ta e="T532" id="Seg_10463" s="T521">"Deshalb bekommt ihr Geld, was glaubt ihr denn, wofür wir Geld bekommen", sage ich.</ta>
            <ta e="T537" id="Seg_10464" s="T532">Es soll Sünde sein, wenn ich sage, dass ich sehr betrogen habe.</ta>
            <ta e="T540" id="Seg_10465" s="T537">Obgleich auch ich nicht verkauft habe.</ta>
            <ta e="T546" id="Seg_10466" s="T540">Alles, alles haben die Leute von der Kolchose verkauft.</ta>
            <ta e="T551" id="Seg_10467" s="T549">– Geld haben sie gemacht.</ta>
            <ta e="T573" id="Seg_10468" s="T571">– Ja, genau.</ta>
            <ta e="T585" id="Seg_10469" s="T580">– Rentierhirten gibt es nicht genug.</ta>
            <ta e="T621" id="Seg_10470" s="T612">– Weil ich gut gearbeitet habe, bin ich das geworden, weil ich mit Leuten rede.</ta>
            <ta e="T628" id="Seg_10471" s="T621">Überhaupt, als ich jung war, war ich sehr agil.</ta>
            <ta e="T634" id="Seg_10472" s="T628">Obwohl ich krank wurde, redete ich immer mit den Leuten.</ta>
            <ta e="T643" id="Seg_10473" s="T634">Sei es ein Russe, sei es ein Dolgane, sei es ein Fremder.</ta>
            <ta e="T657" id="Seg_10474" s="T652">– Jeden Tag haben die geschaut.</ta>
            <ta e="T661" id="Seg_10475" s="T657">Einen Fehler, irgendwas (…) macht man.</ta>
            <ta e="T669" id="Seg_10476" s="T661">Viele Fehler habe ich nicht, deshalb, die Russen…</ta>
            <ta e="T697" id="Seg_10477" s="T693">– Sogar über die Geburten hat man Rechenschaft abgelegt.</ta>
            <ta e="T701" id="Seg_10478" s="T697">Das Kalb war noch ganz klein.</ta>
            <ta e="T710" id="Seg_10479" s="T701">Die ganze Nacht gehst du ohne zu schlafen, um die Geburt des Rentierkalbes zu beobachten.</ta>
            <ta e="T717" id="Seg_10480" s="T710">Bei vielen Rentieren gibt es auch viele Kälber.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-ChSA">
            <ta e="T13" id="Seg_10481" s="T6">— Наши отцы и матери ведь были пожилые.</ta>
            <ta e="T16" id="Seg_10482" s="T13">Ни У, ни А не знают.</ta>
            <ta e="T32" id="Seg_10483" s="T16">Крест… просто (ходят), [когда] расписываются — так делали, я и сейчас тоже таким же образом делаю.</ta>
            <ta e="T35" id="Seg_10484" s="T32">С памятью у меня сильно хуже стало.</ta>
            <ta e="T42" id="Seg_10485" s="T35">Целый год болел ведь, но, несмотря на это, мне получше.</ta>
            <ta e="T49" id="Seg_10486" s="T42">Более менее люди обо мне, как о человеке могут сказать. [?]</ta>
            <ta e="T52" id="Seg_10487" s="T49">На работе даже.</ta>
            <ta e="T54" id="Seg_10488" s="T52">На работе плохо.</ta>
            <ta e="T61" id="Seg_10489" s="T54">Вот детей чтобы вырастить, это совсем… </ta>
            <ta e="T64" id="Seg_10490" s="T61">Из последних сил выбивался.</ta>
            <ta e="T69" id="Seg_10491" s="T64">Оленеводство разве святое было раньше. </ta>
            <ta e="T72" id="Seg_10492" s="T69">Да и много было, тяжко было.</ta>
            <ta e="T78" id="Seg_10493" s="T72">Человеческие силы разом все иссякали.</ta>
            <ta e="T83" id="Seg_10494" s="T78">Зимой, полярной ночью к одиннадцати приходили.</ta>
            <ta e="T87" id="Seg_10495" s="T83">О-о, очень холодно.</ta>
            <ta e="T96" id="Seg_10496" s="T89">— В Попигайской тундре, там ведь лес, лес везде.</ta>
            <ta e="T100" id="Seg_10497" s="T96">Оленя нигде не видно.</ta>
            <ta e="T105" id="Seg_10498" s="T100">Вот тогда от работы выматывался я очень.</ta>
            <ta e="T112" id="Seg_10499" s="T105">Но всё-таки я был удачливым, олени у меня очень хорошо разводились.</ta>
            <ta e="T122" id="Seg_10500" s="T112">Больше десяти тысяч голов держал, нескольким стадам отдавал, стаду отдавали.</ta>
            <ta e="T133" id="Seg_10501" s="T127">— Вторая, вторая бригада всегда, и до сих пор.</ta>
            <ta e="T138" id="Seg_10502" s="T133">Теперь мой сын этим занимается.</ta>
            <ta e="T139" id="Seg_10503" s="T138">Володя мой.</ta>
            <ta e="T141" id="Seg_10504" s="T139">Единственный сын у меня.</ta>
            <ta e="T151" id="Seg_10505" s="T147">— М-м, стареем, вдвоём состарились.</ta>
            <ta e="T170" id="Seg_10506" s="T166">— Бригадиры нормальные люди были.</ta>
            <ta e="T180" id="Seg_10507" s="T170">Поэтому, обучаясь, обучаясь, мы и набрались опыта.</ta>
            <ta e="T186" id="Seg_10508" s="T180">Переживая из-за оленя, думая, как бы оленя не потерять.</ta>
            <ta e="T188" id="Seg_10509" s="T186">Говорил я:</ta>
            <ta e="T193" id="Seg_10510" s="T188">"Оленя не теряйте, o завтрашнем дне думайте", — говорил.</ta>
            <ta e="T201" id="Seg_10511" s="T193">"Утром планируйте, — говорил, — где работать будете, куда пойдёте".</ta>
            <ta e="T211" id="Seg_10512" s="T201">Оленя пропадало много прямо (?), ум что-то делает летом, очень далеко они уходят. </ta>
            <ta e="T217" id="Seg_10513" s="T211">Вот, дожив до сих пор…</ta>
            <ta e="T228" id="Seg_10514" s="T217">Всё время с хорошим именем выходил вот — и летом, и зимой.</ta>
            <ta e="T243" id="Seg_10515" s="T228">Всё равно много лет проработал, в пятьдесят пять лет, выйдя на пенсию, ещё поработал, так как олень у меня хорошо сохранялся.</ta>
            <ta e="T260" id="Seg_10516" s="T256">— Пропали же, был падёж.</ta>
            <ta e="T265" id="Seg_10517" s="T260">Пропали [ещё] в сторону этого, в сторону Саскылаха перебравшись.</ta>
            <ta e="T270" id="Seg_10518" s="T265">Сами уходят, или дикий олень уводит.</ta>
            <ta e="T272" id="Seg_10519" s="T270">Дикий олень уводит.</ta>
            <ta e="T280" id="Seg_10520" s="T272">Во время миграции, с приходом осени, волки очень много съедают.</ta>
            <ta e="T288" id="Seg_10521" s="T280">Заболевают ещё, как копытницей заболеет, так долго не продержится.</ta>
            <ta e="T306" id="Seg_10522" s="T301">— Разрешают сейчас, если есть.</ta>
            <ta e="T308" id="Seg_10523" s="T306">Говорят.</ta>
            <ta e="T316" id="Seg_10524" s="T308">Сейчас премию стали давать, раньше не премировали. Было.</ta>
            <ta e="T332" id="Seg_10525" s="T319">— Наши… эти вот, расценки были плохие, раньше низкие, мизерные же были за оленя.</ta>
            <ta e="T346" id="Seg_10526" s="T332">То, что я постоянно просил: "Сделайте выше", сейчас разве эти слова услышат, сейчас наши работники невнимательные. </ta>
            <ta e="T357" id="Seg_10527" s="T352">— Да, говорим ведь, чтобы нам прокормиться.</ta>
            <ta e="T361" id="Seg_10528" s="T357">Расценку на одного оленя чтобы повысили, просим.</ta>
            <ta e="T372" id="Seg_10529" s="T361">У нас… у них, у Юрюнг-Хаинских на одного оленя сколько же, говорили?</ta>
            <ta e="T376" id="Seg_10530" s="T372">В сутки четыре, четыре рубля.</ta>
            <ta e="T384" id="Seg_10531" s="T380">— За содержание, конечно, да, да.</ta>
            <ta e="T390" id="Seg_10532" s="T384">За содержание стада.</ta>
            <ta e="T393" id="Seg_10533" s="T390">Их тоже…</ta>
            <ta e="T397" id="Seg_10534" s="T393">Пурга плохое делает. </ta>
            <ta e="T402" id="Seg_10535" s="T397">В районе Юрюнг-Хаи земля очень голая.</ta>
            <ta e="T410" id="Seg_10536" s="T402">Поэтому те расценки им повысили с самого начала, (из-за насекомых?), с давних пор.</ta>
            <ta e="T415" id="Seg_10537" s="T410">Содержать оленя там очень сложно.</ta>
            <ta e="T427" id="Seg_10538" s="T417">— Да, человек умирает вместе с этим оленем.</ta>
            <ta e="T446" id="Seg_10539" s="T441">— В Якутской земле так было.</ta>
            <ta e="T449" id="Seg_10540" s="T446">Оленя очень ценят.</ta>
            <ta e="T451" id="Seg_10541" s="T449">Вырастить хотят.</ta>
            <ta e="T458" id="Seg_10542" s="T451">Ой, денег в месяц оленеводы прилично получают.</ta>
            <ta e="T465" id="Seg_10543" s="T458">По четыре, по пять тысяч в один месяц они получают.</ta>
            <ta e="T477" id="Seg_10544" s="T465">Мы вот когдa… такого нет же, совершенно нет…</ta>
            <ta e="T484" id="Seg_10545" s="T477">Просто рублей 500, наверное, будет, в виде аванса.</ta>
            <ta e="T489" id="Seg_10546" s="T484">Наша расценка маленькая очень, копейки.</ta>
            <ta e="T491" id="Seg_10547" s="T489">До сих пор.</ta>
            <ta e="T496" id="Seg_10548" s="T491">В итоге растеряли сейчас всё.</ta>
            <ta e="T498" id="Seg_10549" s="T496">Говорил ведь:</ta>
            <ta e="T510" id="Seg_10550" s="T498">"Зачем трогаете их, зачем вам это надо, это ведь ваш совхозный, совхозный олень, сами ведь кушаете, — говорил.</ta>
            <ta e="T512" id="Seg_10551" s="T510"> — [Сами] одеваетесь и прочее.</ta>
            <ta e="T517" id="Seg_10552" s="T512">Почему говорите "совхозное"? — говорю.</ta>
            <ta e="T521" id="Seg_10553" s="T517"> — Ваше богатство ведь, — говорю.</ta>
            <ta e="T532" id="Seg_10554" s="T521">— За это деньги получаете, что же… за что же получать будем, думаете?" — говорю.</ta>
            <ta e="T537" id="Seg_10555" s="T532">Грех будет, если скажу, что сильно обманул.</ta>
            <ta e="T540" id="Seg_10556" s="T537">Так и я не продавал даже.</ta>
            <ta e="T546" id="Seg_10557" s="T540">Все, все люди продавали вот от совхоза.</ta>
            <ta e="T551" id="Seg_10558" s="T549">— Да, зарабатывали.</ta>
            <ta e="T573" id="Seg_10559" s="T571">— Так, так.</ta>
            <ta e="T585" id="Seg_10560" s="T580">— Оленеводов всё меньше становится, вот сейчас.</ta>
            <ta e="T621" id="Seg_10561" s="T612">— За добросовестный труд вышел, за способность с людьми находить общий язык.</ta>
            <ta e="T628" id="Seg_10562" s="T621">В общем-то, когда помоложе был, очень шустрый был.</ta>
            <ta e="T634" id="Seg_10563" s="T628">Хоть и заболел, всё равно общался с людьми.</ta>
            <ta e="T643" id="Seg_10564" s="T634">Будь то русский, долганин или другой [национальности].</ta>
            <ta e="T657" id="Seg_10565" s="T652">— Каждый день ведь наблюдали.</ta>
            <ta e="T661" id="Seg_10566" s="T657">Промахи никогда не пропускали.</ta>
            <ta e="T669" id="Seg_10567" s="T661">Больших промахов не было, поэтому, русские…</ta>
            <ta e="T697" id="Seg_10568" s="T693">— Даже об отёлах рассказывали.</ta>
            <ta e="T701" id="Seg_10569" s="T697">Оленята же маленькие.</ta>
            <ta e="T710" id="Seg_10570" s="T701">Ночами не спавши ходишь ведь, за рождением телят следишь.</ta>
            <ta e="T717" id="Seg_10571" s="T710">При большом стаде ведь много молодняка.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-ChSA">
            <ta e="T13" id="Seg_10572" s="T6">Ч: Отцы наши, мамы наши были пожилые были ведь.</ta>
            <ta e="T16" id="Seg_10573" s="T13">Ни У, ни А не знали (т.е. неграмотные).</ta>
            <ta e="T32" id="Seg_10574" s="T16">Крест…просто ходил…(ходили), расписывались – так делали (т.е. крестик ставили), я тоже и сейчас, таким же образом вот.</ta>
            <ta e="T35" id="Seg_10575" s="T32">Ум мой улетел сильно (с памятью хуже стало).</ta>
            <ta e="T42" id="Seg_10576" s="T35">Целый год болел ведь, но, несмотря на это, мне получше.</ta>
            <ta e="T49" id="Seg_10577" s="T42">Более менее люди обо мне, как о человеке могут сказать.</ta>
            <ta e="T52" id="Seg_10578" s="T49">На работе даже.</ta>
            <ta e="T54" id="Seg_10579" s="T52">На работе плохо.</ta>
            <ta e="T61" id="Seg_10580" s="T54">Вот детей, чтобы вырастить вот это совсем…</ta>
            <ta e="T64" id="Seg_10581" s="T61">Из последних сил выматывался.</ta>
            <ta e="T69" id="Seg_10582" s="T64">Оленя хождение (оленеводство?) разве святое раньше.</ta>
            <ta e="T72" id="Seg_10583" s="T69">Да и много, тяжко было.</ta>
            <ta e="T78" id="Seg_10584" s="T72">Человеческие силы (взорвавшись) приходили вовсе (т.е. иссякали).</ta>
            <ta e="T83" id="Seg_10585" s="T78">К одиннадцати приходили тёмной ночью (Полярной ночью).</ta>
            <ta e="T87" id="Seg_10586" s="T83">– О-о, очень холодно.</ta>
            <ta e="T96" id="Seg_10587" s="T89">Ч: В Попигайской тундре, лес лес ведь везде.</ta>
            <ta e="T100" id="Seg_10588" s="T96">Оленя нигде не видно.</ta>
            <ta e="T105" id="Seg_10589" s="T100">Вот тогда работая выматывался я очень.</ta>
            <ta e="T112" id="Seg_10590" s="T105">Но всё таки, будучи удачливым, олень мой выводился очень.</ta>
            <ta e="T122" id="Seg_10591" s="T112">Больше десяти тысяч (голов) держал, нескольким стадам отдавал, стаду отдавали.</ta>
            <ta e="T133" id="Seg_10592" s="T127">Ч: Вторая, вторая бригада всегда, до сих пор.</ta>
            <ta e="T138" id="Seg_10593" s="T133">Вот сейчас мой сын возится (этим занимается).</ta>
            <ta e="T139" id="Seg_10594" s="T138">Володя мой.</ta>
            <ta e="T141" id="Seg_10595" s="T139">Единственный сын у меня.</ta>
            <ta e="T151" id="Seg_10596" s="T147">Ч: М-м, стареем, вдвоём состарились.</ta>
            <ta e="T170" id="Seg_10597" s="T166">Ч: Бригадиры нормальные люди были.</ta>
            <ta e="T180" id="Seg_10598" s="T170">Поэтому, обучаясь, обучаясь, так и набрались опыта мы.</ta>
            <ta e="T186" id="Seg_10599" s="T180">В этих волнениях, из-за оленя, оленя не теряйте, думая.</ta>
            <ta e="T188" id="Seg_10600" s="T186">Говорил я:</ta>
            <ta e="T193" id="Seg_10601" s="T188">"Оленя не теряйте, o завтрашнем дне думайте", — говорил.</ta>
            <ta e="T201" id="Seg_10602" s="T193">"Утром планируйте, — говорил, — где работать будете, куда пойдёте".</ta>
            <ta e="T211" id="Seg_10603" s="T201">Оленя пропадало много прямо ум что-то делает (летом), очень на длинные расстояния ходящий бывает.</ta>
            <ta e="T217" id="Seg_10604" s="T211">Вот, дожив до сих пор…</ta>
            <ta e="T228" id="Seg_10605" s="T217">Всё время с хорошим именем вышел вот — и зимой, и летом.</ta>
            <ta e="T243" id="Seg_10606" s="T228">Всё равно, работая, много лет проработал, в пятьдесят пять лет на пенсию войдя, ещё поработал, так как олень мой хорошо сохранялся.</ta>
            <ta e="T260" id="Seg_10607" s="T256">Ч: Пропали ведь, был падёж.</ta>
            <ta e="T265" id="Seg_10608" s="T260">Пропал и это в сторону Саскылаха перебравшись.</ta>
            <ta e="T270" id="Seg_10609" s="T265">Уходя, или дикий олень уводит.</ta>
            <ta e="T272" id="Seg_10610" s="T270">Дикий олень заманивал (уводил).</ta>
            <ta e="T280" id="Seg_10611" s="T272">Во время миграции, с приходом осенней поры, волк съедает сильно очень .</ta>
            <ta e="T288" id="Seg_10612" s="T280">Заболевает ещё, как копытницей заболеет, так долго не уходит (т.е. не продержится).</ta>
            <ta e="T306" id="Seg_10613" s="T301">Ч: Разрешают сейчас, если есть.</ta>
            <ta e="T308" id="Seg_10614" s="T306">Говорят.</ta>
            <ta e="T316" id="Seg_10615" s="T308">Сейчас премию стали давать, раньше не премировали. Было.</ta>
            <ta e="T332" id="Seg_10616" s="T319">Ч: Наши, эти вот расценки были неважные, раньше не большие, маленькие (мизерные) были за оленя же.</ta>
            <ta e="T346" id="Seg_10617" s="T332">То я, сделайте выше, просил-просил, язык (слово) услышат сейчас (разве), сейчас наши работники не внимательные (не хорошие) вот. </ta>
            <ta e="T357" id="Seg_10618" s="T352">Ч: Да, говорим ведь чтобы прокормить нас.</ta>
            <ta e="T361" id="Seg_10619" s="T357">Расценку на одного оленя, чтобы повысили, просим.</ta>
            <ta e="T372" id="Seg_10620" s="T361">Наш… их это, у Юрюнг-Хаинских на одного оленя сколько же говорили?</ta>
            <ta e="T376" id="Seg_10621" s="T372">В сутки четыре, четыре рубля.</ta>
            <ta e="T384" id="Seg_10622" s="T380">Ч: За содержание, конечно, да, да.</ta>
            <ta e="T390" id="Seg_10623" s="T384">За содержания стада.</ta>
            <ta e="T393" id="Seg_10624" s="T390">Их тоже…</ta>
            <ta e="T397" id="Seg_10625" s="T393">Пурга плохое делает.</ta>
            <ta e="T402" id="Seg_10626" s="T397">Юрюнг-Хаи земля где условия тяжелые.</ta>
            <ta e="T410" id="Seg_10627" s="T402">Поэтому те расценки им повысили с самого начала, с… давно.</ta>
            <ta e="T415" id="Seg_10628" s="T410">Содержание оленя очень сложно.</ta>
            <ta e="T427" id="Seg_10629" s="T417">Ч: Э, умира… умирающий человек умирает это (?) вместе с оленем уходя.</ta>
            <ta e="T446" id="Seg_10630" s="T441">Ч: В Якутской земле так было.</ta>
            <ta e="T449" id="Seg_10631" s="T446">Оленя очень ценят (уважают).</ta>
            <ta e="T451" id="Seg_10632" s="T449">Вырастить хотят.</ta>
            <ta e="T458" id="Seg_10633" s="T451">Ой, деньги получают хорошо оленеводы в месяц.</ta>
            <ta e="T465" id="Seg_10634" s="T458">По четыре, по пять тысяч в один месяц получают они.</ta>
            <ta e="T477" id="Seg_10635" s="T465">Мы вот когдa и до такого нет же, совершенно нет…</ta>
            <ta e="T484" id="Seg_10636" s="T477">Просто, 500 рублей будет ли, в виде аванса.</ta>
            <ta e="T489" id="Seg_10637" s="T484">Расценка… наша маленькие очень, копейки.</ta>
            <ta e="T491" id="Seg_10638" s="T489">До сих пор.</ta>
            <ta e="T496" id="Seg_10639" s="T491">В итоге растеряли сейчас всё.</ta>
            <ta e="T498" id="Seg_10640" s="T496">Говорил ведь:</ta>
            <ta e="T510" id="Seg_10641" s="T498">"Почему трогаете, зачем вам это надо, совхозный, совхозный олень, сами кушаете ведь, — говорил.</ta>
            <ta e="T512" id="Seg_10642" s="T510">– Одеваетесь и прочее."</ta>
            <ta e="T517" id="Seg_10643" s="T512">"Почему совхозное говорите, – говорю.</ta>
            <ta e="T521" id="Seg_10644" s="T517">– Ваше богатство ведь, – говорю.</ta>
            <ta e="T532" id="Seg_10645" s="T521">– Поэтому деньги получаете, что, чем получать думаете", – говорю.</ta>
            <ta e="T537" id="Seg_10646" s="T532">Грех будет, если скажете, что сильно обманули.</ta>
            <ta e="T540" id="Seg_10647" s="T537">Так и я не продавал даже.</ta>
            <ta e="T546" id="Seg_10648" s="T540">Все, все люди продавали вот от совхоза.</ta>
            <ta e="T551" id="Seg_10649" s="T549">Ч: Да, зарабатывали.</ta>
            <ta e="T573" id="Seg_10650" s="T571">Ч: Так, так.</ta>
            <ta e="T585" id="Seg_10651" s="T580">Ч: Оленеводов всё меньше становится, вот сейчас.</ta>
            <ta e="T621" id="Seg_10652" s="T612">Ч: За добросовестный труд вышел, за способность с людьми находить общий язык.</ta>
            <ta e="T628" id="Seg_10653" s="T621">Вообщем-то, когда помоложе был, очень шустрый был.</ta>
            <ta e="T634" id="Seg_10654" s="T628">Хоть и заболел, все равно общался с людьми.</ta>
            <ta e="T643" id="Seg_10655" s="T634">Будь то русский, долганин или хоть другой национальности.</ta>
            <ta e="T657" id="Seg_10656" s="T652">Ч: Каждый день ведь смотрели (наблюдали).</ta>
            <ta e="T661" id="Seg_10657" s="T657">Промах никогда не пропустили (заметили без разу).</ta>
            <ta e="T669" id="Seg_10658" s="T661">Больших промахов не было, поэтому, русские…</ta>
            <ta e="T697" id="Seg_10659" s="T693">Ч: Об отёлах докладывали (рассказывали) даже.</ta>
            <ta e="T701" id="Seg_10660" s="T697">Оленята же маленькие.</ta>
            <ta e="T710" id="Seg_10661" s="T701">Ночами, не спавши, ходишь ведь, вот за отёлом (за рождением телят) следишь.</ta>
            <ta e="T717" id="Seg_10662" s="T710">При большом стаде много молодняка (телят) ведь.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-ChSA">
            <ta e="T16" id="Seg_10663" s="T13">[DCh]: The sentence means that they were illiterate.</ta>
            <ta e="T32" id="Seg_10664" s="T16">[DCh]: "iliː battaː-" means 'to sign'.</ta>
            <ta e="T35" id="Seg_10665" s="T32">[AAV]: lit. "my mind flew away"</ta>
            <ta e="T357" id="Seg_10666" s="T352">[DCh]: "kü͡ömej iːt-" means to nourish (oneself).</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KuNS"
                          name="ref"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KuNS"
                          name="st"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KuNS"
                          name="ts"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KuNS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KuNS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KuNS"
                          name="mb"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KuNS"
                          name="mp"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KuNS"
                          name="ge"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KuNS"
                          name="gg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KuNS"
                          name="gr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KuNS"
                          name="mc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KuNS"
                          name="ps"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KuNS"
                          name="SeR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KuNS"
                          name="SyF"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KuNS"
                          name="IST"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KuNS"
                          name="Top"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KuNS"
                          name="Foc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KuNS"
                          name="BOR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KuNS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KuNS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KuNS"
                          name="CS"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KuNS"
                          name="fe"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KuNS"
                          name="fg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KuNS"
                          name="fr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KuNS"
                          name="ltr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KuNS"
                          name="nt"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-ChSA"
                          name="ref"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-ChSA"
                          name="st"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-ChSA"
                          name="ts"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-ChSA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-ChSA"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-ChSA"
                          name="mb"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-ChSA"
                          name="mp"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-ChSA"
                          name="ge"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-ChSA"
                          name="gg"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-ChSA"
                          name="gr"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-ChSA"
                          name="mc"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-ChSA"
                          name="ps"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-ChSA"
                          name="SeR"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-ChSA"
                          name="SyF"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-ChSA"
                          name="IST"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-ChSA"
                          name="Top"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-ChSA"
                          name="Foc"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-ChSA"
                          name="BOR"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-ChSA"
                          name="BOR-Phon"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-ChSA"
                          name="BOR-Morph"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-ChSA"
                          name="CS"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-ChSA"
                          name="fe"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-ChSA"
                          name="fg"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-ChSA"
                          name="fr"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-ChSA"
                          name="ltr"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-ChSA"
                          name="nt"
                          segmented-tier-id="tx-ChSA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
