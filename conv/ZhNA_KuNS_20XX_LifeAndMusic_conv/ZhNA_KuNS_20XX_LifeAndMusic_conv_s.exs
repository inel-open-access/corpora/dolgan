<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID890BA169-6573-EED9-8CA3-7D1609602FAE">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>ZhNA_KuNS_20XX_LifeAndMusic_conv</transcription-name>
         <referenced-file url="ZhNA_KuNS_20XX_LifeAndMusic_conv.wav" />
         <referenced-file url="ZhNA_KuNS_20XX_LifeAndMusic_conv.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\conv\ZhNA_KuNS_20XX_LifeAndMusic_conv\ZhNA_KuNS_20XX_LifeAndMusic_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">653</ud-information>
            <ud-information attribute-name="# HIAT:w">414</ud-information>
            <ud-information attribute-name="# e">415</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">78</ud-information>
            <ud-information attribute-name="# sc">76</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ZhNA">
            <abbreviation>ZhNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KuNS">
            <abbreviation>KuNS</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.663" type="appl" />
         <tli id="T3" time="1.326" type="appl" />
         <tli id="T4" time="1.989" type="appl" />
         <tli id="T5" time="2.652" type="appl" />
         <tli id="T6" time="3.3150000000000004" type="appl" />
         <tli id="T7" time="3.978" type="appl" />
         <tli id="T8" time="4.480444444444444" type="appl" />
         <tli id="T9" time="4.982888888888889" type="appl" />
         <tli id="T10" time="5.485333333333333" type="appl" />
         <tli id="T11" time="5.987777777777778" type="appl" />
         <tli id="T12" time="6.490222222222222" type="appl" />
         <tli id="T13" time="6.992666666666667" type="appl" />
         <tli id="T14" time="7.495111111111111" type="appl" />
         <tli id="T15" time="7.9975555555555555" type="appl" />
         <tli id="T16" time="8.5" type="appl" />
         <tli id="T17" time="9.099230769230768" type="appl" />
         <tli id="T18" time="9.698461538461538" type="appl" />
         <tli id="T19" time="10.297692307692307" type="appl" />
         <tli id="T20" time="10.896923076923077" type="appl" />
         <tli id="T21" time="11.496153846153845" type="appl" />
         <tli id="T22" time="12.095384615384614" type="appl" />
         <tli id="T23" time="12.694615384615384" type="appl" />
         <tli id="T24" time="13.293846153846154" type="appl" />
         <tli id="T25" time="13.893076923076922" type="appl" />
         <tli id="T26" time="14.49230769230769" type="appl" />
         <tli id="T27" time="15.09153846153846" type="appl" />
         <tli id="T28" time="15.690769230769229" type="appl" />
         <tli id="T29" time="16.27" type="appl" />
         <tli id="T30" time="16.29" type="appl" />
         <tli id="T31" time="16.6175" type="appl" />
         <tli id="T32" time="16.965" type="appl" />
         <tli id="T33" time="17.3125" type="appl" />
         <tli id="T34" time="17.66" type="appl" />
         <tli id="T35" time="18.298333333333332" type="appl" />
         <tli id="T36" time="18.936666666666667" type="appl" />
         <tli id="T37" time="19.575" type="appl" />
         <tli id="T38" time="20.2954" type="appl" />
         <tli id="T39" time="21.0158" type="appl" />
         <tli id="T40" time="21.7362" type="appl" />
         <tli id="T41" time="22.456599999999998" type="appl" />
         <tli id="T42" time="23.177" type="appl" />
         <tli id="T43" time="23.731125" type="appl" />
         <tli id="T44" time="24.285249999999998" type="appl" />
         <tli id="T45" time="24.839375" type="appl" />
         <tli id="T46" time="25.3935" type="appl" />
         <tli id="T47" time="25.947625" type="appl" />
         <tli id="T48" time="26.50175" type="appl" />
         <tli id="T49" time="27.055875" type="appl" />
         <tli id="T50" time="27.61" type="appl" />
         <tli id="T51" time="28.0232" type="appl" />
         <tli id="T52" time="28.4364" type="appl" />
         <tli id="T53" time="28.8496" type="appl" />
         <tli id="T54" time="29.2628" type="appl" />
         <tli id="T55" time="29.676" type="appl" />
         <tli id="T56" time="31.07" type="appl" />
         <tli id="T57" time="31.524" type="appl" />
         <tli id="T58" time="31.978" type="appl" />
         <tli id="T59" time="32.432" type="appl" />
         <tli id="T60" time="32.886" type="appl" />
         <tli id="T61" time="33.34" type="appl" />
         <tli id="T62" time="33.794000000000004" type="appl" />
         <tli id="T63" time="34.248000000000005" type="appl" />
         <tli id="T64" time="34.702" type="appl" />
         <tli id="T65" time="35.156" type="appl" />
         <tli id="T66" time="35.61" type="appl" />
         <tli id="T67" time="36.064" type="appl" />
         <tli id="T68" time="36.518" type="appl" />
         <tli id="T69" time="36.972" type="appl" />
         <tli id="T70" time="37.426" type="appl" />
         <tli id="T71" time="38.233000000000004" type="appl" />
         <tli id="T72" time="39.04" type="appl" />
         <tli id="T73" time="39.58985714285714" type="appl" />
         <tli id="T74" time="40.139714285714284" type="appl" />
         <tli id="T75" time="40.689571428571426" type="appl" />
         <tli id="T76" time="41.239428571428576" type="appl" />
         <tli id="T77" time="41.78928571428572" type="appl" />
         <tli id="T78" time="42.33914285714286" type="appl" />
         <tli id="T79" time="42.889" type="appl" />
         <tli id="T80" time="43.73266666666667" type="appl" />
         <tli id="T81" time="44.57633333333334" type="appl" />
         <tli id="T82" time="45.42" type="appl" />
         <tli id="T83" time="45.9152" type="appl" />
         <tli id="T84" time="46.4104" type="appl" />
         <tli id="T85" time="46.9056" type="appl" />
         <tli id="T86" time="47.400800000000004" type="appl" />
         <tli id="T87" time="47.896" type="appl" />
         <tli id="T88" time="48.38066666666667" type="appl" />
         <tli id="T89" time="48.86533333333333" type="appl" />
         <tli id="T90" time="49.22566618853074" />
         <tli id="T91" time="49.35" type="appl" />
         <tli id="T92" time="49.80833333333334" type="appl" />
         <tli id="T93" time="50.29666666666667" type="appl" />
         <tli id="T94" time="50.785" type="appl" />
         <tli id="T95" time="51.27333333333333" type="appl" />
         <tli id="T96" time="51.76166666666667" type="appl" />
         <tli id="T97" time="51.88561212694133" />
         <tli id="T98" time="52.8298" type="appl" />
         <tli id="T99" time="53.4096" type="appl" />
         <tli id="T100" time="53.9894" type="appl" />
         <tli id="T101" time="54.5692" type="appl" />
         <tli id="T102" time="55.149" type="appl" />
         <tli id="T103" time="55.763555555555556" type="appl" />
         <tli id="T104" time="56.37811111111111" type="appl" />
         <tli id="T105" time="56.992666666666665" type="appl" />
         <tli id="T106" time="57.60722222222222" type="appl" />
         <tli id="T107" time="58.22177777777778" type="appl" />
         <tli id="T108" time="58.836333333333336" type="appl" />
         <tli id="T109" time="59.45088888888889" type="appl" />
         <tli id="T110" time="60.065444444444445" type="appl" />
         <tli id="T111" time="60.68" type="appl" />
         <tli id="T112" time="61.022999999999996" type="appl" />
         <tli id="T113" time="61.366" type="appl" />
         <tli id="T114" time="61.709" type="appl" />
         <tli id="T115" time="62.052" type="appl" />
         <tli id="T116" time="62.394999999999996" type="appl" />
         <tli id="T117" time="62.738" type="appl" />
         <tli id="T118" time="63.081" type="appl" />
         <tli id="T119" time="63.424" type="appl" />
         <tli id="T120" time="63.766999999999996" type="appl" />
         <tli id="T121" time="64.1" type="appl" />
         <tli id="T122" time="64.11" type="appl" />
         <tli id="T123" time="64.55499999999999" type="appl" />
         <tli id="T124" time="64.59" type="appl" />
         <tli id="T125" time="65.00999999999999" type="appl" />
         <tli id="T126" time="65.465" type="appl" />
         <tli id="T127" time="65.555" type="appl" />
         <tli id="T128" time="65.92" type="appl" />
         <tli id="T129" time="66.52" type="appl" />
         <tli id="T130" time="68.37" type="appl" />
         <tli id="T131" time="69.05166666666668" type="appl" />
         <tli id="T132" time="69.73333333333333" type="appl" />
         <tli id="T133" time="70.415" type="appl" />
         <tli id="T134" time="71.00125" type="appl" />
         <tli id="T135" time="71.5875" type="appl" />
         <tli id="T136" time="72.17375000000001" type="appl" />
         <tli id="T137" time="72.76" type="appl" />
         <tli id="T138" time="72.77" type="appl" />
         <tli id="T139" time="73.238" type="appl" />
         <tli id="T140" time="73.706" type="appl" />
         <tli id="T141" time="74.13" type="appl" />
         <tli id="T142" time="74.17399999999999" type="appl" />
         <tli id="T143" time="74.642" type="appl" />
         <tli id="T144" time="74.91199999999999" type="appl" />
         <tli id="T145" time="75.11" type="appl" />
         <tli id="T146" time="75.694" type="appl" />
         <tli id="T147" time="76.476" type="appl" />
         <tli id="T148" time="77.25800000000001" type="appl" />
         <tli id="T149" time="78.04" type="appl" />
         <tli id="T150" time="79.57" type="appl" />
         <tli id="T151" time="81.1" type="appl" />
         <tli id="T152" time="81.13" type="appl" />
         <tli id="T153" time="81.53" type="appl" />
         <tli id="T154" time="81.92999999999999" type="appl" />
         <tli id="T155" time="82.33" type="appl" />
         <tli id="T156" time="82.73" type="appl" />
         <tli id="T157" time="83.13000000000001" type="appl" />
         <tli id="T158" time="83.53" type="appl" />
         <tli id="T159" time="83.71" type="appl" />
         <tli id="T160" time="83.93" type="appl" />
         <tli id="T161" time="84.17287499999999" type="appl" />
         <tli id="T162" time="84.63575" type="appl" />
         <tli id="T163" time="85.098625" type="appl" />
         <tli id="T164" time="85.5615" type="appl" />
         <tli id="T165" time="86.02437499999999" type="appl" />
         <tli id="T166" time="86.48724999999999" type="appl" />
         <tli id="T167" time="86.950125" type="appl" />
         <tli id="T168" time="87.413" type="appl" />
         <tli id="T169" time="88.04866666666666" type="appl" />
         <tli id="T170" time="88.68433333333333" type="appl" />
         <tli id="T171" time="89.17" type="appl" />
         <tli id="T172" time="89.32" type="appl" />
         <tli id="T173" time="89.7075" type="appl" />
         <tli id="T174" time="90.245" type="appl" />
         <tli id="T175" time="90.7825" type="appl" />
         <tli id="T176" time="90.91" type="appl" />
         <tli id="T177" time="91.32" type="appl" />
         <tli id="T178" time="91.455" type="appl" />
         <tli id="T179" time="92.0" type="appl" />
         <tli id="T180" time="92.545" type="appl" />
         <tli id="T181" time="93.09" type="appl" />
         <tli id="T182" time="93.63499999999999" type="appl" />
         <tli id="T183" time="94.17999999999999" type="appl" />
         <tli id="T184" time="94.725" type="appl" />
         <tli id="T185" time="95.27" type="appl" />
         <tli id="T186" time="95.78399999999999" type="appl" />
         <tli id="T187" time="96.298" type="appl" />
         <tli id="T188" time="96.812" type="appl" />
         <tli id="T189" time="97.32600000000001" type="appl" />
         <tli id="T190" time="97.84" type="appl" />
         <tli id="T191" time="98.24000000000001" type="appl" />
         <tli id="T192" time="98.64" type="appl" />
         <tli id="T193" time="99.03999999999999" type="appl" />
         <tli id="T194" time="99.44" type="appl" />
         <tli id="T195" time="99.84" type="appl" />
         <tli id="T196" time="100.24" type="appl" />
         <tli id="T197" time="101.026" type="appl" />
         <tli id="T198" time="101.54181818181817" type="appl" />
         <tli id="T199" time="102.05763636363636" type="appl" />
         <tli id="T200" time="102.57345454545454" type="appl" />
         <tli id="T201" time="103.08927272727273" type="appl" />
         <tli id="T202" time="103.6050909090909" type="appl" />
         <tli id="T203" time="104.1209090909091" type="appl" />
         <tli id="T204" time="104.63672727272727" type="appl" />
         <tli id="T205" time="105.15254545454546" type="appl" />
         <tli id="T206" time="105.66836363636364" type="appl" />
         <tli id="T207" time="106.18418181818183" type="appl" />
         <tli id="T208" time="106.6" type="appl" />
         <tli id="T209" time="106.7" type="appl" />
         <tli id="T210" time="107.146" type="appl" />
         <tli id="T211" time="107.692" type="appl" />
         <tli id="T212" time="108.238" type="appl" />
         <tli id="T213" time="108.78399999999999" type="appl" />
         <tli id="T214" time="109.33" type="appl" />
         <tli id="T215" time="109.876" type="appl" />
         <tli id="T216" time="110.422" type="appl" />
         <tli id="T217" time="110.968" type="appl" />
         <tli id="T218" time="111.39542857142857" type="appl" />
         <tli id="T219" time="111.82285714285715" type="appl" />
         <tli id="T220" time="112.25028571428571" type="appl" />
         <tli id="T221" time="112.67771428571429" type="appl" />
         <tli id="T222" time="113.10514285714285" type="appl" />
         <tli id="T223" time="113.53257142857143" type="appl" />
         <tli id="T224" time="113.92" type="appl" />
         <tli id="T225" time="113.96" type="appl" />
         <tli id="T226" time="114.38" type="appl" />
         <tli id="T227" time="114.46" type="appl" />
         <tli id="T228" time="114.84" type="appl" />
         <tli id="T229" time="115.261" type="appl" />
         <tli id="T230" time="115.72181818181818" type="appl" />
         <tli id="T231" time="116.18263636363636" type="appl" />
         <tli id="T232" time="116.64345454545455" type="appl" />
         <tli id="T233" time="117.10427272727273" type="appl" />
         <tli id="T234" time="117.56509090909091" type="appl" />
         <tli id="T235" time="118.02590909090908" type="appl" />
         <tli id="T236" time="118.48672727272726" type="appl" />
         <tli id="T237" time="118.94754545454545" type="appl" />
         <tli id="T238" time="119.40836363636363" type="appl" />
         <tli id="T239" time="119.86918181818181" type="appl" />
         <tli id="T240" time="120.01" type="appl" />
         <tli id="T241" time="120.33" type="appl" />
         <tli id="T242" time="120.7505" type="appl" />
         <tli id="T243" time="120.96" type="appl" />
         <tli id="T244" time="121.171" type="appl" />
         <tli id="T245" time="121.754625" type="appl" />
         <tli id="T246" time="122.33825" type="appl" />
         <tli id="T247" time="122.921875" type="appl" />
         <tli id="T248" time="123.50550000000001" type="appl" />
         <tli id="T249" time="124.08912500000001" type="appl" />
         <tli id="T250" time="124.62" type="appl" />
         <tli id="T251" time="124.67275000000001" type="appl" />
         <tli id="T252" time="125.256375" type="appl" />
         <tli id="T253" time="125.39457142857144" type="appl" />
         <tli id="T254" time="125.84" type="appl" />
         <tli id="T255" time="126.16914285714286" type="appl" />
         <tli id="T256" time="126.9437142857143" type="appl" />
         <tli id="T257" time="127.71828571428571" type="appl" />
         <tli id="T258" time="128.49285714285713" type="appl" />
         <tli id="T259" time="129.26742857142858" type="appl" />
         <tli id="T260" time="130.042" type="appl" />
         <tli id="T261" time="131.88049999999998" type="appl" />
         <tli id="T262" time="133.719" type="appl" />
         <tli id="T263" time="134.3368888888889" type="appl" />
         <tli id="T264" time="134.95477777777776" type="appl" />
         <tli id="T265" time="135.57266666666666" type="appl" />
         <tli id="T266" time="136.19055555555556" type="appl" />
         <tli id="T267" time="136.80844444444443" type="appl" />
         <tli id="T268" time="137.42633333333333" type="appl" />
         <tli id="T269" time="138.04422222222223" type="appl" />
         <tli id="T270" time="138.6621111111111" type="appl" />
         <tli id="T271" time="139.28" type="appl" />
         <tli id="T272" time="139.685" type="appl" />
         <tli id="T273" time="140.09" type="appl" />
         <tli id="T274" time="140.495" type="appl" />
         <tli id="T275" time="140.9" type="appl" />
         <tli id="T276" time="141.48375000000001" type="appl" />
         <tli id="T277" time="142.0675" type="appl" />
         <tli id="T278" time="142.65125" type="appl" />
         <tli id="T279" time="143.235" type="appl" />
         <tli id="T280" time="144.04250000000002" type="appl" />
         <tli id="T281" time="144.16" type="appl" />
         <tli id="T282" time="144.70749999999998" type="appl" />
         <tli id="T283" time="144.85" type="appl" />
         <tli id="T284" time="145.255" type="appl" />
         <tli id="T285" time="145.914375" type="appl" />
         <tli id="T286" time="146.57375" type="appl" />
         <tli id="T287" time="147.233125" type="appl" />
         <tli id="T288" time="147.89249999999998" type="appl" />
         <tli id="T289" time="148.551875" type="appl" />
         <tli id="T290" time="149.21125" type="appl" />
         <tli id="T291" time="149.75" type="appl" />
         <tli id="T292" time="149.870625" type="appl" />
         <tli id="T293" time="150.31742857142856" type="appl" />
         <tli id="T294" time="150.53" type="appl" />
         <tli id="T295" time="150.88485714285716" type="appl" />
         <tli id="T296" time="151.45228571428572" type="appl" />
         <tli id="T297" time="152.0197142857143" type="appl" />
         <tli id="T298" time="152.58714285714285" type="appl" />
         <tli id="T299" time="153.15457142857144" type="appl" />
         <tli id="T300" time="153.722" type="appl" />
         <tli id="T301" time="154.77100000000002" type="appl" />
         <tli id="T302" time="155.82" type="appl" />
         <tli id="T303" time="156.299625" type="appl" />
         <tli id="T304" time="156.77925" type="appl" />
         <tli id="T305" time="157.258875" type="appl" />
         <tli id="T306" time="157.7385" type="appl" />
         <tli id="T307" time="158.21812500000001" type="appl" />
         <tli id="T308" time="158.69775" type="appl" />
         <tli id="T309" time="159.177375" type="appl" />
         <tli id="T310" time="159.657" type="appl" />
         <tli id="T311" time="160.258" type="appl" />
         <tli id="T312" time="160.859" type="appl" />
         <tli id="T313" time="161.42" type="appl" />
         <tli id="T314" time="161.46" type="appl" />
         <tli id="T315" time="162.08999999999997" type="appl" />
         <tli id="T316" time="162.76" type="appl" />
         <tli id="T317" time="163.43" type="appl" />
         <tli id="T318" time="163.6" type="appl" />
         <tli id="T319" time="164.1" type="appl" />
         <tli id="T320" time="164.2942" type="appl" />
         <tli id="T321" time="164.98839999999998" type="appl" />
         <tli id="T322" time="165.6826" type="appl" />
         <tli id="T323" time="166.3768" type="appl" />
         <tli id="T324" time="167.071" type="appl" />
         <tli id="T325" time="167.589" type="appl" />
         <tli id="T326" time="168.107" type="appl" />
         <tli id="T327" time="168.625" type="appl" />
         <tli id="T328" time="169.143" type="appl" />
         <tli id="T329" time="169.661" type="appl" />
         <tli id="T330" time="170.179" type="appl" />
         <tli id="T331" time="170.697" type="appl" />
         <tli id="T332" time="171.215" type="appl" />
         <tli id="T333" time="171.9474285714286" type="appl" />
         <tli id="T334" time="172.67985714285714" type="appl" />
         <tli id="T335" time="173.41228571428573" type="appl" />
         <tli id="T336" time="174.1447142857143" type="appl" />
         <tli id="T337" time="174.87714285714287" type="appl" />
         <tli id="T338" time="175.60957142857143" type="appl" />
         <tli id="T339" time="176.342" type="appl" />
         <tli id="T340" time="176.72925" type="appl" />
         <tli id="T341" time="177.1165" type="appl" />
         <tli id="T342" time="177.50375" type="appl" />
         <tli id="T343" time="177.89100000000002" type="appl" />
         <tli id="T344" time="178.27825" type="appl" />
         <tli id="T345" time="178.6655" type="appl" />
         <tli id="T346" time="179.05275" type="appl" />
         <tli id="T347" time="179.32" type="appl" />
         <tli id="T348" time="179.44" type="appl" />
         <tli id="T349" time="179.92000000000002" type="appl" />
         <tli id="T350" time="180.52" type="appl" />
         <tli id="T351" time="180.55" type="appl" />
         <tli id="T352" time="180.89914285714286" type="appl" />
         <tli id="T353" time="181.24828571428571" type="appl" />
         <tli id="T354" time="181.59742857142857" type="appl" />
         <tli id="T355" time="181.94657142857145" type="appl" />
         <tli id="T356" time="182.2957142857143" type="appl" />
         <tli id="T357" time="182.64485714285715" type="appl" />
         <tli id="T358" time="182.994" type="appl" />
         <tli id="T359" time="183.3576" type="appl" />
         <tli id="T360" time="183.7212" type="appl" />
         <tli id="T361" time="184.0848" type="appl" />
         <tli id="T362" time="184.4484" type="appl" />
         <tli id="T363" time="184.812" type="appl" />
         <tli id="T364" time="185.1756" type="appl" />
         <tli id="T365" time="185.5392" type="appl" />
         <tli id="T366" time="185.90279999999998" type="appl" />
         <tli id="T367" time="186.2664" type="appl" />
         <tli id="T368" time="186.63" type="appl" />
         <tli id="T369" time="187.29714285714286" type="appl" />
         <tli id="T370" time="187.96428571428572" type="appl" />
         <tli id="T371" time="188.6314285714286" type="appl" />
         <tli id="T372" time="189.29857142857142" type="appl" />
         <tli id="T373" time="189.96571428571428" type="appl" />
         <tli id="T374" time="190.63285714285715" type="appl" />
         <tli id="T375" time="191.04" type="appl" />
         <tli id="T376" time="191.3" type="appl" />
         <tli id="T377" time="191.69066666666666" type="appl" />
         <tli id="T378" time="192.34133333333332" type="appl" />
         <tli id="T379" time="192.992" type="appl" />
         <tli id="T380" time="193.839" type="appl" />
         <tli id="T381" time="194.68599999999998" type="appl" />
         <tli id="T382" time="195.533" type="appl" />
         <tli id="T383" time="195.84" type="appl" />
         <tli id="T384" time="196.26333333333335" type="appl" />
         <tli id="T385" time="196.38" type="appl" />
         <tli id="T386" time="196.68666666666667" type="appl" />
         <tli id="T387" time="196.88" type="appl" />
         <tli id="T388" time="197.11" type="appl" />
         <tli id="T389" time="197.34366666666665" type="appl" />
         <tli id="T390" time="197.80733333333333" type="appl" />
         <tli id="T391" time="198.271" type="appl" />
         <tli id="T392" time="198.78324999999998" type="appl" />
         <tli id="T393" time="199.2955" type="appl" />
         <tli id="T394" time="199.80775" type="appl" />
         <tli id="T395" time="200.32" type="appl" />
         <tli id="T396" time="200.84583333333333" type="appl" />
         <tli id="T397" time="201.37166666666667" type="appl" />
         <tli id="T398" time="201.89749999999998" type="appl" />
         <tli id="T399" time="202.42333333333332" type="appl" />
         <tli id="T400" time="202.94916666666666" type="appl" />
         <tli id="T401" time="203.475" type="appl" />
         <tli id="T402" time="204.00083333333333" type="appl" />
         <tli id="T403" time="204.52666666666667" type="appl" />
         <tli id="T404" time="205.0525" type="appl" />
         <tli id="T405" time="205.57833333333332" type="appl" />
         <tli id="T406" time="206.10416666666666" type="appl" />
         <tli id="T407" time="206.18" type="appl" />
         <tli id="T408" time="206.63" type="appl" />
         <tli id="T409" time="207.00366666666667" type="appl" />
         <tli id="T410" time="207.82733333333334" type="appl" />
         <tli id="T411" time="208.651" type="appl" />
         <tli id="T412" time="209.17433333333335" type="appl" />
         <tli id="T413" time="209.69766666666666" type="appl" />
         <tli id="T414" time="210.221" type="appl" />
         <tli id="T415" time="210.74433333333334" type="appl" />
         <tli id="T416" time="211.26766666666666" type="appl" />
         <tli id="T417" time="211.791" type="appl" />
         <tli id="T418" time="212.5605" type="appl" />
         <tli id="T419" time="213.33" type="appl" />
         <tli id="T420" time="213.66500000000002" type="appl" />
         <tli id="T421" time="214.0" type="appl" />
         <tli id="T422" time="214.335" type="appl" />
         <tli id="T423" time="214.67000000000002" type="appl" />
         <tli id="T424" time="215.005" type="appl" />
         <tli id="T425" time="215.34" type="appl" />
         <tli id="T426" time="215.675" type="appl" />
         <tli id="T427" time="216.01000000000002" type="appl" />
         <tli id="T428" time="216.345" type="appl" />
         <tli id="T429" time="216.68" type="appl" />
         <tli id="T430" time="217.104" type="appl" />
         <tli id="T431" time="217.52800000000002" type="appl" />
         <tli id="T432" time="217.952" type="appl" />
         <tli id="T433" time="218.376" type="appl" />
         <tli id="T434" time="218.8" type="appl" />
         <tli id="T435" time="219.17475000000002" type="appl" />
         <tli id="T436" time="219.54950000000002" type="appl" />
         <tli id="T437" time="219.92425" type="appl" />
         <tli id="T438" time="220.299" type="appl" />
         <tli id="T439" time="220.84466666666668" type="appl" />
         <tli id="T440" time="221.39033333333333" type="appl" />
         <tli id="T441" time="221.936" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-ZhNA"
                      id="tx-ZhNA"
                      speaker="ZhNA"
                      type="t">
         <timeline-fork end="T336" start="T335">
            <tli id="T335.tx-ZhNA.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-ZhNA">
            <ts e="T30" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Min</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">bu͡ollagɨna</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">törütüm</ts>
                  <nts id="Seg_13" n="HIAT:ip">,</nts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">bejem</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">törütüm</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">ki͡eŋ</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Dʼakuːskajga</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">daː</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">baːllar</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">nʼuːččalar</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_41" n="HIAT:w" s="T11">daː</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_44" n="HIAT:w" s="T12">baːllar</ts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">dalgattar</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">da</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">baːllar</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_58" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">Bastakɨːnnan</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">bu͡o</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">Džakutidžattan</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">min</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">ebem</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">bu͡ollagɨna</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">Xatangattan</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_81" n="HIAT:w" s="T23">ogonnʼorgo</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_84" n="HIAT:w" s="T24">taksɨbɨta</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">iti</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">ispidisijaga</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_94" n="HIAT:w" s="T27">üleliː</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_97" n="HIAT:w" s="T28">hɨldʼan</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_100" n="sc" s="T34">
               <ts e="T37" id="Seg_102" n="HIAT:u" s="T34">
                  <nts id="Seg_103" n="HIAT:ip">–</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_106" n="HIAT:w" s="T34">Tʼimapʼejeba</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_109" n="HIAT:w" s="T35">Darʼja</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_112" n="HIAT:w" s="T36">di͡en</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_116" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_118" n="HIAT:w" s="T37">Onton</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_121" n="HIAT:w" s="T38">bu͡ollagɨna</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_124" n="HIAT:w" s="T39">baran</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_127" n="HIAT:w" s="T40">ogonnʼorgo</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_130" n="HIAT:w" s="T41">tagɨsta</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_134" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_136" n="HIAT:w" s="T42">Ogonnʼordoro</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_139" n="HIAT:w" s="T43">üs</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_142" n="HIAT:w" s="T44">kɨːhɨ</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_145" n="HIAT:w" s="T45">töröːbüt</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_148" n="HIAT:w" s="T46">ol</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_151" n="HIAT:w" s="T47">Džakuːskajga</ts>
                  <nts id="Seg_152" n="HIAT:ip">,</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_155" n="HIAT:w" s="T48">Dʼakuːskajga</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_158" n="HIAT:w" s="T49">baran</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_162" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_164" n="HIAT:w" s="T50">Onton</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_167" n="HIAT:w" s="T51">töttörü</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_170" n="HIAT:w" s="T52">köhön</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_173" n="HIAT:w" s="T53">kelbitter</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_176" n="HIAT:w" s="T54">manna</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_180" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_182" n="HIAT:w" s="T55">Pöpügejge</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_186" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_188" n="HIAT:w" s="T56">Papigajtan</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_191" n="HIAT:w" s="T57">Katangaga</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_194" n="HIAT:w" s="T58">da</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_197" n="HIAT:w" s="T59">üleliː</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_200" n="HIAT:w" s="T60">hɨddʼɨbɨta</ts>
                  <nts id="Seg_201" n="HIAT:ip">,</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_204" n="HIAT:w" s="T61">Kasistajga</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_207" n="HIAT:w" s="T62">da</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_210" n="HIAT:w" s="T63">üleliː</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_213" n="HIAT:w" s="T64">hɨddʼɨbɨta</ts>
                  <nts id="Seg_214" n="HIAT:ip">,</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_217" n="HIAT:w" s="T65">hirge</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_220" n="HIAT:w" s="T66">barɨtɨgar</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_223" n="HIAT:w" s="T67">dʼe</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_226" n="HIAT:w" s="T68">üleliː</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_229" n="HIAT:w" s="T69">hɨddžɨbɨt</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_233" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_235" n="HIAT:w" s="T70">Huragɨn</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_238" n="HIAT:w" s="T71">ihittekpine</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_242" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_244" n="HIAT:w" s="T72">Onton</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_247" n="HIAT:w" s="T73">bu͡ollagɨna</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_250" n="HIAT:w" s="T74">ɨlgɨn</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_253" n="HIAT:w" s="T75">kɨːhɨn</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_256" n="HIAT:w" s="T76">u͡ola</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_259" n="HIAT:w" s="T77">bu͡olabɨn</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_262" n="HIAT:w" s="T78">min</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_266" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_268" n="HIAT:w" s="T79">Bejebit</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_271" n="HIAT:w" s="T80">agɨstarbɨt</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_274" n="HIAT:w" s="T81">törüppütüger</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_278" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_280" n="HIAT:w" s="T82">Aŋarbɨt</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_283" n="HIAT:w" s="T83">otto</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_286" n="HIAT:w" s="T84">hu͡ok</ts>
                  <nts id="Seg_287" n="HIAT:ip">,</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_290" n="HIAT:w" s="T85">aŋarbɨt</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_293" n="HIAT:w" s="T86">baːr</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_297" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_299" n="HIAT:w" s="T87">Dʼe</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_302" n="HIAT:w" s="T88">iti</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_305" n="HIAT:w" s="T89">kördük</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T122" id="Seg_308" n="sc" s="T97">
               <ts e="T102" id="Seg_310" n="HIAT:u" s="T97">
                  <nts id="Seg_311" n="HIAT:ip">–</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_314" n="HIAT:w" s="T97">Min</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_317" n="HIAT:w" s="T98">bu͡ollagɨna</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_320" n="HIAT:w" s="T99">Džakutijaga</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_323" n="HIAT:w" s="T100">ü͡örene</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_326" n="HIAT:w" s="T101">hɨldžɨbɨtɨm</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_330" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_332" n="HIAT:w" s="T102">Togu͡on</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_335" n="HIAT:w" s="T103">ɨllaːktarga</ts>
                  <nts id="Seg_336" n="HIAT:ip">,</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_339" n="HIAT:w" s="T104">togu͡on</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_342" n="HIAT:w" s="T105">ɨllaːktarga</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_345" n="HIAT:w" s="T106">bu͡o</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_348" n="HIAT:w" s="T107">manna</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_351" n="HIAT:w" s="T108">ɨllɨː</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_354" n="HIAT:w" s="T109">hɨldžɨbɨtɨm</ts>
                  <nts id="Seg_355" n="HIAT:ip">,</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_358" n="HIAT:w" s="T110">festivalʼga</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_362" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_364" n="HIAT:w" s="T111">Onno</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_367" n="HIAT:w" s="T112">össü͡ö</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_370" n="HIAT:w" s="T113">mi͡este</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_373" n="HIAT:w" s="T114">duː</ts>
                  <nts id="Seg_374" n="HIAT:ip">,</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_377" n="HIAT:w" s="T115">tu͡ok</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_380" n="HIAT:w" s="T116">duː</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_383" n="HIAT:w" s="T117">baːr</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_386" n="HIAT:w" s="T118">kördük</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_389" n="HIAT:w" s="T119">ete</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_392" n="HIAT:w" s="T120">bu͡o</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_395" n="sc" s="T124">
               <ts e="T129" id="Seg_397" n="HIAT:u" s="T124">
                  <nts id="Seg_398" n="HIAT:ip">–</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_401" n="HIAT:w" s="T124">Heː</ts>
                  <nts id="Seg_402" n="HIAT:ip">,</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_405" n="HIAT:w" s="T127">Tajmɨːrga</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_409" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_411" n="HIAT:w" s="T129">ɨllaːbɨtɨm</ts>
                  <nts id="Seg_412" n="HIAT:ip">.</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_415" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_417" n="HIAT:w" s="T130">Onno</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_420" n="HIAT:w" s="T131">küččügüj</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_423" n="HIAT:w" s="T132">etim</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_427" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_429" n="HIAT:w" s="T133">Kas</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_432" n="HIAT:w" s="T134">haːstaːkpɨn</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_435" n="HIAT:w" s="T135">bejem</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_438" n="HIAT:w" s="T136">bilbeppin</ts>
                  <nts id="Seg_439" n="HIAT:ip">.</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T151" id="Seg_441" n="sc" s="T141">
               <ts e="T149" id="Seg_443" n="HIAT:u" s="T141">
                  <nts id="Seg_444" n="HIAT:ip">–</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_447" n="HIAT:w" s="T141">Heː</ts>
                  <nts id="Seg_448" n="HIAT:ip">,</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_451" n="HIAT:w" s="T144">Dudʼinkattan</ts>
                  <nts id="Seg_452" n="HIAT:ip">,</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_455" n="HIAT:w" s="T146">mantan</ts>
                  <nts id="Seg_456" n="HIAT:ip">,</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_459" n="HIAT:w" s="T147">ɨlbɨttara</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_462" n="HIAT:w" s="T148">iti</ts>
                  <nts id="Seg_463" n="HIAT:ip">.</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_466" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_468" n="HIAT:w" s="T149">Tubatdʼelʼenʼijattan</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_471" n="HIAT:w" s="T150">ɨlbɨttara</ts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_474" n="sc" s="T159">
               <ts e="T168" id="Seg_476" n="HIAT:u" s="T159">
                  <nts id="Seg_477" n="HIAT:ip">–</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_480" n="HIAT:w" s="T159">Küččügüj</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_483" n="HIAT:w" s="T161">erdekpine</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_486" n="HIAT:w" s="T162">bu͡ollagɨna</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_489" n="HIAT:w" s="T163">bejem</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_492" n="HIAT:w" s="T164">če</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_495" n="HIAT:w" s="T165">kohujammɨn</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_498" n="HIAT:w" s="T166">ɨllɨːr</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_501" n="HIAT:w" s="T167">etim</ts>
                  <nts id="Seg_502" n="HIAT:ip">.</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_505" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_507" n="HIAT:w" s="T168">Ol</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_510" n="HIAT:w" s="T169">barɨta</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_513" n="HIAT:w" s="T170">umnullubut</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T197" id="Seg_516" n="sc" s="T176">
               <ts e="T185" id="Seg_518" n="HIAT:u" s="T176">
                  <nts id="Seg_519" n="HIAT:ip">–</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_522" n="HIAT:w" s="T176">Heː</ts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_526" n="HIAT:w" s="T178">hubu</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_528" n="HIAT:ip">(</nts>
                  <ts e="T180" id="Seg_530" n="HIAT:w" s="T179">ta-</ts>
                  <nts id="Seg_531" n="HIAT:ip">)</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_533" n="HIAT:ip">(</nts>
                  <ts e="T181" id="Seg_535" n="HIAT:w" s="T180">taŋa-</ts>
                  <nts id="Seg_536" n="HIAT:ip">)</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_539" n="HIAT:w" s="T181">haŋarar</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_542" n="HIAT:w" s="T182">haŋalarbɨn</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_545" n="HIAT:w" s="T183">ɨllɨːr</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_548" n="HIAT:w" s="T184">etim</ts>
                  <nts id="Seg_549" n="HIAT:ip">.</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_552" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_554" n="HIAT:w" s="T185">Kolobura</ts>
                  <nts id="Seg_555" n="HIAT:ip">,</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_558" n="HIAT:w" s="T186">kihini</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_561" n="HIAT:w" s="T187">kördökpüne</ts>
                  <nts id="Seg_562" n="HIAT:ip">,</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_565" n="HIAT:w" s="T188">kihini</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_568" n="HIAT:w" s="T189">kohujabɨn</ts>
                  <nts id="Seg_569" n="HIAT:ip">.</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_572" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_574" n="HIAT:w" s="T190">Tugu</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_577" n="HIAT:w" s="T191">ere</ts>
                  <nts id="Seg_578" n="HIAT:ip">,</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_581" n="HIAT:w" s="T192">ɨtɨ</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_584" n="HIAT:w" s="T193">kohujdakpɨna</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_587" n="HIAT:w" s="T194">ɨtɨ</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_590" n="HIAT:w" s="T195">kohujabɨn</ts>
                  <nts id="Seg_591" n="HIAT:ip">.</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_594" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_596" n="HIAT:w" s="T196">Itinnik</ts>
                  <nts id="Seg_597" n="HIAT:ip">.</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T225" id="Seg_599" n="sc" s="T208">
               <ts e="T217" id="Seg_601" n="HIAT:u" s="T208">
                  <nts id="Seg_602" n="HIAT:ip">–</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_605" n="HIAT:w" s="T208">Kim</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_608" n="HIAT:w" s="T210">da</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_611" n="HIAT:w" s="T211">ɨllaːbat</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_614" n="HIAT:w" s="T212">ete</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_617" n="HIAT:w" s="T213">törüppütten</ts>
                  <nts id="Seg_618" n="HIAT:ip">,</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_621" n="HIAT:w" s="T214">no</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_624" n="HIAT:w" s="T215">možet</ts>
                  <nts id="Seg_625" n="HIAT:ip">,</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_628" n="HIAT:w" s="T216">iti</ts>
                  <nts id="Seg_629" n="HIAT:ip">…</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_632" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_634" n="HIAT:w" s="T217">Možet</ts>
                  <nts id="Seg_635" n="HIAT:ip">,</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_638" n="HIAT:w" s="T218">ojuttar</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_641" n="HIAT:w" s="T219">emi͡e</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_644" n="HIAT:w" s="T220">baːllara</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_647" n="HIAT:w" s="T221">bu͡olu͡o</ts>
                  <nts id="Seg_648" n="HIAT:ip">,</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_651" n="HIAT:w" s="T222">olor</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_653" n="HIAT:ip">(</nts>
                  <nts id="Seg_654" n="HIAT:ip">(</nts>
                  <ats e="T225" id="Seg_655" n="HIAT:non-pho" s="T223">…</ats>
                  <nts id="Seg_656" n="HIAT:ip">)</nts>
                  <nts id="Seg_657" n="HIAT:ip">)</nts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T229" id="Seg_660" n="sc" s="T227">
               <ts e="T229" id="Seg_662" n="HIAT:u" s="T227">
                  <nts id="Seg_663" n="HIAT:ip">–</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_666" n="HIAT:w" s="T227">Heː</ts>
                  <nts id="Seg_667" n="HIAT:ip">.</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T243" id="Seg_669" n="sc" s="T240">
               <ts e="T243" id="Seg_671" n="HIAT:u" s="T240">
                  <nts id="Seg_672" n="HIAT:ip">–</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_675" n="HIAT:w" s="T240">Mm</ts>
                  <nts id="Seg_676" n="HIAT:ip">.</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T279" id="Seg_678" n="sc" s="T250">
               <ts e="T260" id="Seg_680" n="HIAT:u" s="T250">
                  <nts id="Seg_681" n="HIAT:ip">–</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_684" n="HIAT:w" s="T250">Mm</ts>
                  <nts id="Seg_685" n="HIAT:ip">,</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_688" n="HIAT:w" s="T253">katallalar</ts>
                  <nts id="Seg_689" n="HIAT:ip">,</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_692" n="HIAT:w" s="T255">nastaivajdɨːllar</ts>
                  <nts id="Seg_693" n="HIAT:ip">,</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_696" n="HIAT:w" s="T256">üleliːr</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_699" n="HIAT:w" s="T257">etim</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_702" n="HIAT:w" s="T258">ol</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_705" n="HIAT:w" s="T259">Dʼakutijaga</ts>
                  <nts id="Seg_706" n="HIAT:ip">.</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_709" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_711" n="HIAT:w" s="T260">Tu͡okka</ts>
                  <nts id="Seg_712" n="HIAT:ip">,</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_715" n="HIAT:w" s="T261">tʼexrabotnʼigɨnan</ts>
                  <nts id="Seg_716" n="HIAT:ip">.</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_719" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_721" n="HIAT:w" s="T262">Ol</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_724" n="HIAT:w" s="T263">üleleːmmin</ts>
                  <nts id="Seg_725" n="HIAT:ip">,</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_728" n="HIAT:w" s="T264">če</ts>
                  <nts id="Seg_729" n="HIAT:ip">,</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_732" n="HIAT:w" s="T265">bejem</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_735" n="HIAT:w" s="T266">tu͡okpar</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_738" n="HIAT:w" s="T267">hɨldʼaːččɨbɨn</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_741" n="HIAT:w" s="T268">min</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_744" n="HIAT:w" s="T269">kansertarga</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_747" n="HIAT:w" s="T270">tu͡oktarga</ts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_751" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_753" n="HIAT:w" s="T271">Vaxtʼorša</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_756" n="HIAT:w" s="T272">kördük</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_759" n="HIAT:w" s="T273">etim</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_762" n="HIAT:w" s="T274">bejem</ts>
                  <nts id="Seg_763" n="HIAT:ip">.</nts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_766" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_768" n="HIAT:w" s="T275">Bilʼet</ts>
                  <nts id="Seg_769" n="HIAT:ip">,</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_772" n="HIAT:w" s="T276">eŋin</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_775" n="HIAT:w" s="T277">atɨːlɨːbɨn</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_778" n="HIAT:w" s="T278">itinnikterge</ts>
                  <nts id="Seg_779" n="HIAT:ip">.</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T284" id="Seg_781" n="sc" s="T281">
               <ts e="T284" id="Seg_783" n="HIAT:u" s="T281">
                  <nts id="Seg_784" n="HIAT:ip">–</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_787" n="HIAT:w" s="T281">Kuluːbka</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_790" n="HIAT:w" s="T282">üleleːbitim</ts>
                  <nts id="Seg_791" n="HIAT:ip">.</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T314" id="Seg_793" n="sc" s="T291">
               <ts e="T300" id="Seg_795" n="HIAT:u" s="T291">
                  <nts id="Seg_796" n="HIAT:ip">–</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_799" n="HIAT:w" s="T291">Mm</ts>
                  <nts id="Seg_800" n="HIAT:ip">,</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_803" n="HIAT:w" s="T293">ol</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_806" n="HIAT:w" s="T295">senaga</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_809" n="HIAT:w" s="T296">taksɨ͡akpɨn</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_812" n="HIAT:w" s="T297">anaːn</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_815" n="HIAT:w" s="T298">ispisʼalno</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_818" n="HIAT:w" s="T299">ɨlbɨttara</ts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_822" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_824" n="HIAT:w" s="T300">Bejebin</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_827" n="HIAT:w" s="T301">billeri͡ekpin</ts>
                  <nts id="Seg_828" n="HIAT:ip">.</nts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_831" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_833" n="HIAT:w" s="T302">Bejem</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_836" n="HIAT:w" s="T303">bu͡ollagɨna</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_839" n="HIAT:w" s="T304">alta</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_842" n="HIAT:w" s="T305">duː</ts>
                  <nts id="Seg_843" n="HIAT:ip">,</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_846" n="HIAT:w" s="T306">tü͡ört</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_849" n="HIAT:w" s="T307">duː</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_852" n="HIAT:w" s="T308">gramatalaːkpɨn</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_855" n="HIAT:w" s="T309">anɨ</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_859" n="HIAT:u" s="T310">
                  <ts e="T311" id="Seg_861" n="HIAT:w" s="T310">Ol</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_864" n="HIAT:w" s="T311">di͡ek</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_867" n="HIAT:w" s="T312">hɨtallar</ts>
                  <nts id="Seg_868" n="HIAT:ip">.</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T339" id="Seg_870" n="sc" s="T318">
               <ts e="T324" id="Seg_872" n="HIAT:u" s="T318">
                  <nts id="Seg_873" n="HIAT:ip">–</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_876" n="HIAT:w" s="T318">Jakuːskajga</ts>
                  <nts id="Seg_877" n="HIAT:ip">,</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_880" n="HIAT:w" s="T320">abrazavaːnʼijattan</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_883" n="HIAT:w" s="T321">da</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_886" n="HIAT:w" s="T322">baːr</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_889" n="HIAT:w" s="T323">etilere</ts>
                  <nts id="Seg_890" n="HIAT:ip">.</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_893" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_895" n="HIAT:w" s="T324">Baːllar</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_898" n="HIAT:w" s="T325">eni</ts>
                  <nts id="Seg_899" n="HIAT:ip">,</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_902" n="HIAT:w" s="T326">kajdi͡ek</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_905" n="HIAT:w" s="T327">duː</ts>
                  <nts id="Seg_906" n="HIAT:ip">,</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_909" n="HIAT:w" s="T328">kördökkö</ts>
                  <nts id="Seg_910" n="HIAT:ip">,</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_913" n="HIAT:w" s="T329">onton</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_916" n="HIAT:w" s="T330">tu͡oktan</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_919" n="HIAT:w" s="T331">emi͡e</ts>
                  <nts id="Seg_920" n="HIAT:ip">…</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_923" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_925" n="HIAT:w" s="T332">Kulʼturattan</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_928" n="HIAT:w" s="T333">kas</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_931" n="HIAT:w" s="T334">daːganɨ</ts>
                  <nts id="Seg_932" n="HIAT:ip">,</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335.tx-ZhNA.1" id="Seg_935" n="HIAT:w" s="T335">glava</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_938" n="HIAT:w" s="T335.tx-ZhNA.1">admʼinʼistrasʼijattan</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_941" n="HIAT:w" s="T336">emi͡e</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_944" n="HIAT:w" s="T337">baːr</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_947" n="HIAT:w" s="T338">bu͡olu͡oktaːk</ts>
                  <nts id="Seg_948" n="HIAT:ip">.</nts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T350" id="Seg_950" n="sc" s="T347">
               <ts e="T350" id="Seg_952" n="HIAT:u" s="T347">
                  <nts id="Seg_953" n="HIAT:ip">–</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_956" n="HIAT:w" s="T347">Alʼenʼoskaj</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_959" n="HIAT:w" s="T349">uluːs</ts>
                  <nts id="Seg_960" n="HIAT:ip">.</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T376" id="Seg_962" n="sc" s="T358">
               <ts e="T368" id="Seg_964" n="HIAT:u" s="T358">
                  <nts id="Seg_965" n="HIAT:ip">–</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_968" n="HIAT:w" s="T358">Alʼenʼoskaj</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_971" n="HIAT:w" s="T359">uluːs</ts>
                  <nts id="Seg_972" n="HIAT:ip">,</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_975" n="HIAT:w" s="T360">arɨː</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_978" n="HIAT:w" s="T361">bu͡o</ts>
                  <nts id="Seg_979" n="HIAT:ip">,</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_982" n="HIAT:w" s="T362">min</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_985" n="HIAT:w" s="T363">bu͡o</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_988" n="HIAT:w" s="T364">oččogo</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_991" n="HIAT:w" s="T365">onno</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_994" n="HIAT:w" s="T366">Xarɨjalaːkka</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_997" n="HIAT:w" s="T367">etim</ts>
                  <nts id="Seg_998" n="HIAT:ip">.</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1001" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1003" n="HIAT:w" s="T368">Ginner</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1006" n="HIAT:w" s="T369">bu͡ollagɨna</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1009" n="HIAT:w" s="T370">iti</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1012" n="HIAT:w" s="T371">bi͡erekke</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1015" n="HIAT:w" s="T372">mannɨk</ts>
                  <nts id="Seg_1016" n="HIAT:ip">,</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1019" n="HIAT:w" s="T373">oŋu͡or-oŋu͡or</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1022" n="HIAT:w" s="T374">olorollor</ts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T385" id="Seg_1025" n="sc" s="T379">
               <ts e="T385" id="Seg_1027" n="HIAT:u" s="T379">
                  <nts id="Seg_1028" n="HIAT:ip">–</nts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1031" n="HIAT:w" s="T379">Ölöːnüŋ</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1034" n="HIAT:w" s="T380">tuspa</ts>
                  <nts id="Seg_1035" n="HIAT:ip">,</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1038" n="HIAT:w" s="T381">Xarɨjalaːgɨŋ</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1041" n="HIAT:w" s="T382">tuspa</ts>
                  <nts id="Seg_1042" n="HIAT:ip">.</nts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T391" id="Seg_1044" n="sc" s="T387">
               <ts e="T391" id="Seg_1046" n="HIAT:u" s="T387">
                  <nts id="Seg_1047" n="HIAT:ip">–</nts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1050" n="HIAT:w" s="T387">Eː</ts>
                  <nts id="Seg_1051" n="HIAT:ip">,</nts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1054" n="HIAT:w" s="T389">ikki</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1057" n="HIAT:w" s="T390">tu͡ok</ts>
                  <nts id="Seg_1058" n="HIAT:ip">.</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T417" id="Seg_1060" n="sc" s="T407">
               <ts e="T411" id="Seg_1062" n="HIAT:u" s="T407">
                  <nts id="Seg_1063" n="HIAT:ip">–</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1066" n="HIAT:w" s="T407">Dolu͡oj</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1069" n="HIAT:w" s="T409">küččügüjbütten</ts>
                  <nts id="Seg_1070" n="HIAT:ip">,</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1073" n="HIAT:w" s="T410">di͡eččiler</ts>
                  <nts id="Seg_1074" n="HIAT:ip">.</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1077" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1079" n="HIAT:w" s="T411">Bu</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1082" n="HIAT:w" s="T412">dʼe</ts>
                  <nts id="Seg_1083" n="HIAT:ip">,</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1086" n="HIAT:w" s="T413">haːdikka</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1089" n="HIAT:w" s="T414">hɨldʼarbɨttan</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1092" n="HIAT:w" s="T415">ɨllɨːr</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1095" n="HIAT:w" s="T416">ebippin</ts>
                  <nts id="Seg_1096" n="HIAT:ip">.</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T441" id="Seg_1098" n="sc" s="T429">
               <ts e="T434" id="Seg_1100" n="HIAT:u" s="T429">
                  <nts id="Seg_1101" n="HIAT:ip">–</nts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1104" n="HIAT:w" s="T429">Bu</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1107" n="HIAT:w" s="T430">nʼuːččalarɨ</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1110" n="HIAT:w" s="T431">kennikiː</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1113" n="HIAT:w" s="T432">ɨllɨːr</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1116" n="HIAT:w" s="T433">bu͡oltum</ts>
                  <nts id="Seg_1117" n="HIAT:ip">.</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1120" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1122" n="HIAT:w" s="T434">Onton</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1125" n="HIAT:w" s="T435">bu</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1128" n="HIAT:w" s="T436">anɨ</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1131" n="HIAT:w" s="T437">hakalarga</ts>
                  <nts id="Seg_1132" n="HIAT:ip">.</nts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1135" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1137" n="HIAT:w" s="T438">Hakalarɨ</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1140" n="HIAT:w" s="T439">ere</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1143" n="HIAT:w" s="T440">ɨllɨːbɨn</ts>
                  <nts id="Seg_1144" n="HIAT:ip">.</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-ZhNA">
            <ts e="T30" id="Seg_1146" n="sc" s="T1">
               <ts e="T2" id="Seg_1148" n="e" s="T1">– Min </ts>
               <ts e="T3" id="Seg_1150" n="e" s="T2">bu͡ollagɨna </ts>
               <ts e="T4" id="Seg_1152" n="e" s="T3">törütüm, </ts>
               <ts e="T5" id="Seg_1154" n="e" s="T4">bejem </ts>
               <ts e="T6" id="Seg_1156" n="e" s="T5">törütüm </ts>
               <ts e="T7" id="Seg_1158" n="e" s="T6">ki͡eŋ. </ts>
               <ts e="T8" id="Seg_1160" n="e" s="T7">Dʼakuːskajga </ts>
               <ts e="T9" id="Seg_1162" n="e" s="T8">daː </ts>
               <ts e="T10" id="Seg_1164" n="e" s="T9">baːllar, </ts>
               <ts e="T11" id="Seg_1166" n="e" s="T10">nʼuːččalar </ts>
               <ts e="T12" id="Seg_1168" n="e" s="T11">daː </ts>
               <ts e="T13" id="Seg_1170" n="e" s="T12">baːllar, </ts>
               <ts e="T14" id="Seg_1172" n="e" s="T13">dalgattar </ts>
               <ts e="T15" id="Seg_1174" n="e" s="T14">da </ts>
               <ts e="T16" id="Seg_1176" n="e" s="T15">baːllar. </ts>
               <ts e="T17" id="Seg_1178" n="e" s="T16">Bastakɨːnnan </ts>
               <ts e="T18" id="Seg_1180" n="e" s="T17">bu͡o </ts>
               <ts e="T19" id="Seg_1182" n="e" s="T18">Džakutidžattan </ts>
               <ts e="T20" id="Seg_1184" n="e" s="T19">min </ts>
               <ts e="T21" id="Seg_1186" n="e" s="T20">ebem </ts>
               <ts e="T22" id="Seg_1188" n="e" s="T21">bu͡ollagɨna </ts>
               <ts e="T23" id="Seg_1190" n="e" s="T22">Xatangattan </ts>
               <ts e="T24" id="Seg_1192" n="e" s="T23">ogonnʼorgo </ts>
               <ts e="T25" id="Seg_1194" n="e" s="T24">taksɨbɨta, </ts>
               <ts e="T26" id="Seg_1196" n="e" s="T25">iti </ts>
               <ts e="T27" id="Seg_1198" n="e" s="T26">ispidisijaga </ts>
               <ts e="T28" id="Seg_1200" n="e" s="T27">üleliː </ts>
               <ts e="T30" id="Seg_1202" n="e" s="T28">hɨldʼan. </ts>
            </ts>
            <ts e="T91" id="Seg_1203" n="sc" s="T34">
               <ts e="T35" id="Seg_1205" n="e" s="T34">– Tʼimapʼejeba </ts>
               <ts e="T36" id="Seg_1207" n="e" s="T35">Darʼja </ts>
               <ts e="T37" id="Seg_1209" n="e" s="T36">di͡en. </ts>
               <ts e="T38" id="Seg_1211" n="e" s="T37">Onton </ts>
               <ts e="T39" id="Seg_1213" n="e" s="T38">bu͡ollagɨna </ts>
               <ts e="T40" id="Seg_1215" n="e" s="T39">baran </ts>
               <ts e="T41" id="Seg_1217" n="e" s="T40">ogonnʼorgo </ts>
               <ts e="T42" id="Seg_1219" n="e" s="T41">tagɨsta. </ts>
               <ts e="T43" id="Seg_1221" n="e" s="T42">Ogonnʼordoro </ts>
               <ts e="T44" id="Seg_1223" n="e" s="T43">üs </ts>
               <ts e="T45" id="Seg_1225" n="e" s="T44">kɨːhɨ </ts>
               <ts e="T46" id="Seg_1227" n="e" s="T45">töröːbüt </ts>
               <ts e="T47" id="Seg_1229" n="e" s="T46">ol </ts>
               <ts e="T48" id="Seg_1231" n="e" s="T47">Džakuːskajga, </ts>
               <ts e="T49" id="Seg_1233" n="e" s="T48">Dʼakuːskajga </ts>
               <ts e="T50" id="Seg_1235" n="e" s="T49">baran. </ts>
               <ts e="T51" id="Seg_1237" n="e" s="T50">Onton </ts>
               <ts e="T52" id="Seg_1239" n="e" s="T51">töttörü </ts>
               <ts e="T53" id="Seg_1241" n="e" s="T52">köhön </ts>
               <ts e="T54" id="Seg_1243" n="e" s="T53">kelbitter </ts>
               <ts e="T55" id="Seg_1245" n="e" s="T54">manna. </ts>
               <ts e="T56" id="Seg_1247" n="e" s="T55">Pöpügejge. </ts>
               <ts e="T57" id="Seg_1249" n="e" s="T56">Papigajtan </ts>
               <ts e="T58" id="Seg_1251" n="e" s="T57">Katangaga </ts>
               <ts e="T59" id="Seg_1253" n="e" s="T58">da </ts>
               <ts e="T60" id="Seg_1255" n="e" s="T59">üleliː </ts>
               <ts e="T61" id="Seg_1257" n="e" s="T60">hɨddʼɨbɨta, </ts>
               <ts e="T62" id="Seg_1259" n="e" s="T61">Kasistajga </ts>
               <ts e="T63" id="Seg_1261" n="e" s="T62">da </ts>
               <ts e="T64" id="Seg_1263" n="e" s="T63">üleliː </ts>
               <ts e="T65" id="Seg_1265" n="e" s="T64">hɨddʼɨbɨta, </ts>
               <ts e="T66" id="Seg_1267" n="e" s="T65">hirge </ts>
               <ts e="T67" id="Seg_1269" n="e" s="T66">barɨtɨgar </ts>
               <ts e="T68" id="Seg_1271" n="e" s="T67">dʼe </ts>
               <ts e="T69" id="Seg_1273" n="e" s="T68">üleliː </ts>
               <ts e="T70" id="Seg_1275" n="e" s="T69">hɨddžɨbɨt. </ts>
               <ts e="T71" id="Seg_1277" n="e" s="T70">Huragɨn </ts>
               <ts e="T72" id="Seg_1279" n="e" s="T71">ihittekpine. </ts>
               <ts e="T73" id="Seg_1281" n="e" s="T72">Onton </ts>
               <ts e="T74" id="Seg_1283" n="e" s="T73">bu͡ollagɨna </ts>
               <ts e="T75" id="Seg_1285" n="e" s="T74">ɨlgɨn </ts>
               <ts e="T76" id="Seg_1287" n="e" s="T75">kɨːhɨn </ts>
               <ts e="T77" id="Seg_1289" n="e" s="T76">u͡ola </ts>
               <ts e="T78" id="Seg_1291" n="e" s="T77">bu͡olabɨn </ts>
               <ts e="T79" id="Seg_1293" n="e" s="T78">min. </ts>
               <ts e="T80" id="Seg_1295" n="e" s="T79">Bejebit </ts>
               <ts e="T81" id="Seg_1297" n="e" s="T80">agɨstarbɨt </ts>
               <ts e="T82" id="Seg_1299" n="e" s="T81">törüppütüger. </ts>
               <ts e="T83" id="Seg_1301" n="e" s="T82">Aŋarbɨt </ts>
               <ts e="T84" id="Seg_1303" n="e" s="T83">otto </ts>
               <ts e="T85" id="Seg_1305" n="e" s="T84">hu͡ok, </ts>
               <ts e="T86" id="Seg_1307" n="e" s="T85">aŋarbɨt </ts>
               <ts e="T87" id="Seg_1309" n="e" s="T86">baːr. </ts>
               <ts e="T88" id="Seg_1311" n="e" s="T87">Dʼe </ts>
               <ts e="T89" id="Seg_1313" n="e" s="T88">iti </ts>
               <ts e="T91" id="Seg_1315" n="e" s="T89">kördük. </ts>
            </ts>
            <ts e="T122" id="Seg_1316" n="sc" s="T97">
               <ts e="T98" id="Seg_1318" n="e" s="T97">– Min </ts>
               <ts e="T99" id="Seg_1320" n="e" s="T98">bu͡ollagɨna </ts>
               <ts e="T100" id="Seg_1322" n="e" s="T99">Džakutijaga </ts>
               <ts e="T101" id="Seg_1324" n="e" s="T100">ü͡örene </ts>
               <ts e="T102" id="Seg_1326" n="e" s="T101">hɨldžɨbɨtɨm. </ts>
               <ts e="T103" id="Seg_1328" n="e" s="T102">Togu͡on </ts>
               <ts e="T104" id="Seg_1330" n="e" s="T103">ɨllaːktarga, </ts>
               <ts e="T105" id="Seg_1332" n="e" s="T104">togu͡on </ts>
               <ts e="T106" id="Seg_1334" n="e" s="T105">ɨllaːktarga </ts>
               <ts e="T107" id="Seg_1336" n="e" s="T106">bu͡o </ts>
               <ts e="T108" id="Seg_1338" n="e" s="T107">manna </ts>
               <ts e="T109" id="Seg_1340" n="e" s="T108">ɨllɨː </ts>
               <ts e="T110" id="Seg_1342" n="e" s="T109">hɨldžɨbɨtɨm, </ts>
               <ts e="T111" id="Seg_1344" n="e" s="T110">festivalʼga. </ts>
               <ts e="T112" id="Seg_1346" n="e" s="T111">Onno </ts>
               <ts e="T113" id="Seg_1348" n="e" s="T112">össü͡ö </ts>
               <ts e="T114" id="Seg_1350" n="e" s="T113">mi͡este </ts>
               <ts e="T115" id="Seg_1352" n="e" s="T114">duː, </ts>
               <ts e="T116" id="Seg_1354" n="e" s="T115">tu͡ok </ts>
               <ts e="T117" id="Seg_1356" n="e" s="T116">duː </ts>
               <ts e="T118" id="Seg_1358" n="e" s="T117">baːr </ts>
               <ts e="T119" id="Seg_1360" n="e" s="T118">kördük </ts>
               <ts e="T120" id="Seg_1362" n="e" s="T119">ete </ts>
               <ts e="T122" id="Seg_1364" n="e" s="T120">bu͡o. </ts>
            </ts>
            <ts e="T137" id="Seg_1365" n="sc" s="T124">
               <ts e="T127" id="Seg_1367" n="e" s="T124">– Heː, </ts>
               <ts e="T129" id="Seg_1369" n="e" s="T127">Tajmɨːrga. </ts>
               <ts e="T130" id="Seg_1371" n="e" s="T129">ɨllaːbɨtɨm. </ts>
               <ts e="T131" id="Seg_1373" n="e" s="T130">Onno </ts>
               <ts e="T132" id="Seg_1375" n="e" s="T131">küččügüj </ts>
               <ts e="T133" id="Seg_1377" n="e" s="T132">etim. </ts>
               <ts e="T134" id="Seg_1379" n="e" s="T133">Kas </ts>
               <ts e="T135" id="Seg_1381" n="e" s="T134">haːstaːkpɨn </ts>
               <ts e="T136" id="Seg_1383" n="e" s="T135">bejem </ts>
               <ts e="T137" id="Seg_1385" n="e" s="T136">bilbeppin. </ts>
            </ts>
            <ts e="T151" id="Seg_1386" n="sc" s="T141">
               <ts e="T144" id="Seg_1388" n="e" s="T141">– Heː, </ts>
               <ts e="T146" id="Seg_1390" n="e" s="T144">Dudʼinkattan, </ts>
               <ts e="T147" id="Seg_1392" n="e" s="T146">mantan, </ts>
               <ts e="T148" id="Seg_1394" n="e" s="T147">ɨlbɨttara </ts>
               <ts e="T149" id="Seg_1396" n="e" s="T148">iti. </ts>
               <ts e="T150" id="Seg_1398" n="e" s="T149">Tubatdʼelʼenʼijattan </ts>
               <ts e="T151" id="Seg_1400" n="e" s="T150">ɨlbɨttara. </ts>
            </ts>
            <ts e="T172" id="Seg_1401" n="sc" s="T159">
               <ts e="T161" id="Seg_1403" n="e" s="T159">– Küččügüj </ts>
               <ts e="T162" id="Seg_1405" n="e" s="T161">erdekpine </ts>
               <ts e="T163" id="Seg_1407" n="e" s="T162">bu͡ollagɨna </ts>
               <ts e="T164" id="Seg_1409" n="e" s="T163">bejem </ts>
               <ts e="T165" id="Seg_1411" n="e" s="T164">če </ts>
               <ts e="T166" id="Seg_1413" n="e" s="T165">kohujammɨn </ts>
               <ts e="T167" id="Seg_1415" n="e" s="T166">ɨllɨːr </ts>
               <ts e="T168" id="Seg_1417" n="e" s="T167">etim. </ts>
               <ts e="T169" id="Seg_1419" n="e" s="T168">Ol </ts>
               <ts e="T170" id="Seg_1421" n="e" s="T169">barɨta </ts>
               <ts e="T172" id="Seg_1423" n="e" s="T170">umnullubut. </ts>
            </ts>
            <ts e="T197" id="Seg_1424" n="sc" s="T176">
               <ts e="T178" id="Seg_1426" n="e" s="T176">– Heː, </ts>
               <ts e="T179" id="Seg_1428" n="e" s="T178">hubu </ts>
               <ts e="T180" id="Seg_1430" n="e" s="T179">(ta-) </ts>
               <ts e="T181" id="Seg_1432" n="e" s="T180">(taŋa-) </ts>
               <ts e="T182" id="Seg_1434" n="e" s="T181">haŋarar </ts>
               <ts e="T183" id="Seg_1436" n="e" s="T182">haŋalarbɨn </ts>
               <ts e="T184" id="Seg_1438" n="e" s="T183">ɨllɨːr </ts>
               <ts e="T185" id="Seg_1440" n="e" s="T184">etim. </ts>
               <ts e="T186" id="Seg_1442" n="e" s="T185">Kolobura, </ts>
               <ts e="T187" id="Seg_1444" n="e" s="T186">kihini </ts>
               <ts e="T188" id="Seg_1446" n="e" s="T187">kördökpüne, </ts>
               <ts e="T189" id="Seg_1448" n="e" s="T188">kihini </ts>
               <ts e="T190" id="Seg_1450" n="e" s="T189">kohujabɨn. </ts>
               <ts e="T191" id="Seg_1452" n="e" s="T190">Tugu </ts>
               <ts e="T192" id="Seg_1454" n="e" s="T191">ere, </ts>
               <ts e="T193" id="Seg_1456" n="e" s="T192">ɨtɨ </ts>
               <ts e="T194" id="Seg_1458" n="e" s="T193">kohujdakpɨna </ts>
               <ts e="T195" id="Seg_1460" n="e" s="T194">ɨtɨ </ts>
               <ts e="T196" id="Seg_1462" n="e" s="T195">kohujabɨn. </ts>
               <ts e="T197" id="Seg_1464" n="e" s="T196">Itinnik. </ts>
            </ts>
            <ts e="T225" id="Seg_1465" n="sc" s="T208">
               <ts e="T210" id="Seg_1467" n="e" s="T208">– Kim </ts>
               <ts e="T211" id="Seg_1469" n="e" s="T210">da </ts>
               <ts e="T212" id="Seg_1471" n="e" s="T211">ɨllaːbat </ts>
               <ts e="T213" id="Seg_1473" n="e" s="T212">ete </ts>
               <ts e="T214" id="Seg_1475" n="e" s="T213">törüppütten, </ts>
               <ts e="T215" id="Seg_1477" n="e" s="T214">no </ts>
               <ts e="T216" id="Seg_1479" n="e" s="T215">možet, </ts>
               <ts e="T217" id="Seg_1481" n="e" s="T216">iti… </ts>
               <ts e="T218" id="Seg_1483" n="e" s="T217">Možet, </ts>
               <ts e="T219" id="Seg_1485" n="e" s="T218">ojuttar </ts>
               <ts e="T220" id="Seg_1487" n="e" s="T219">emi͡e </ts>
               <ts e="T221" id="Seg_1489" n="e" s="T220">baːllara </ts>
               <ts e="T222" id="Seg_1491" n="e" s="T221">bu͡olu͡o, </ts>
               <ts e="T223" id="Seg_1493" n="e" s="T222">olor </ts>
               <ts e="T225" id="Seg_1495" n="e" s="T223">((…)). </ts>
            </ts>
            <ts e="T229" id="Seg_1496" n="sc" s="T227">
               <ts e="T229" id="Seg_1498" n="e" s="T227">– Heː. </ts>
            </ts>
            <ts e="T243" id="Seg_1499" n="sc" s="T240">
               <ts e="T243" id="Seg_1501" n="e" s="T240">– Mm. </ts>
            </ts>
            <ts e="T279" id="Seg_1502" n="sc" s="T250">
               <ts e="T253" id="Seg_1504" n="e" s="T250">– Mm, </ts>
               <ts e="T255" id="Seg_1506" n="e" s="T253">katallalar, </ts>
               <ts e="T256" id="Seg_1508" n="e" s="T255">nastaivajdɨːllar, </ts>
               <ts e="T257" id="Seg_1510" n="e" s="T256">üleliːr </ts>
               <ts e="T258" id="Seg_1512" n="e" s="T257">etim </ts>
               <ts e="T259" id="Seg_1514" n="e" s="T258">ol </ts>
               <ts e="T260" id="Seg_1516" n="e" s="T259">Dʼakutijaga. </ts>
               <ts e="T261" id="Seg_1518" n="e" s="T260">Tu͡okka, </ts>
               <ts e="T262" id="Seg_1520" n="e" s="T261">tʼexrabotnʼigɨnan. </ts>
               <ts e="T263" id="Seg_1522" n="e" s="T262">Ol </ts>
               <ts e="T264" id="Seg_1524" n="e" s="T263">üleleːmmin, </ts>
               <ts e="T265" id="Seg_1526" n="e" s="T264">če, </ts>
               <ts e="T266" id="Seg_1528" n="e" s="T265">bejem </ts>
               <ts e="T267" id="Seg_1530" n="e" s="T266">tu͡okpar </ts>
               <ts e="T268" id="Seg_1532" n="e" s="T267">hɨldʼaːččɨbɨn </ts>
               <ts e="T269" id="Seg_1534" n="e" s="T268">min </ts>
               <ts e="T270" id="Seg_1536" n="e" s="T269">kansertarga </ts>
               <ts e="T271" id="Seg_1538" n="e" s="T270">tu͡oktarga. </ts>
               <ts e="T272" id="Seg_1540" n="e" s="T271">Vaxtʼorša </ts>
               <ts e="T273" id="Seg_1542" n="e" s="T272">kördük </ts>
               <ts e="T274" id="Seg_1544" n="e" s="T273">etim </ts>
               <ts e="T275" id="Seg_1546" n="e" s="T274">bejem. </ts>
               <ts e="T276" id="Seg_1548" n="e" s="T275">Bilʼet, </ts>
               <ts e="T277" id="Seg_1550" n="e" s="T276">eŋin </ts>
               <ts e="T278" id="Seg_1552" n="e" s="T277">atɨːlɨːbɨn </ts>
               <ts e="T279" id="Seg_1554" n="e" s="T278">itinnikterge. </ts>
            </ts>
            <ts e="T284" id="Seg_1555" n="sc" s="T281">
               <ts e="T282" id="Seg_1557" n="e" s="T281">– Kuluːbka </ts>
               <ts e="T284" id="Seg_1559" n="e" s="T282">üleleːbitim. </ts>
            </ts>
            <ts e="T314" id="Seg_1560" n="sc" s="T291">
               <ts e="T293" id="Seg_1562" n="e" s="T291">– Mm, </ts>
               <ts e="T295" id="Seg_1564" n="e" s="T293">ol </ts>
               <ts e="T296" id="Seg_1566" n="e" s="T295">senaga </ts>
               <ts e="T297" id="Seg_1568" n="e" s="T296">taksɨ͡akpɨn </ts>
               <ts e="T298" id="Seg_1570" n="e" s="T297">anaːn </ts>
               <ts e="T299" id="Seg_1572" n="e" s="T298">ispisʼalno </ts>
               <ts e="T300" id="Seg_1574" n="e" s="T299">ɨlbɨttara. </ts>
               <ts e="T301" id="Seg_1576" n="e" s="T300">Bejebin </ts>
               <ts e="T302" id="Seg_1578" n="e" s="T301">billeri͡ekpin. </ts>
               <ts e="T303" id="Seg_1580" n="e" s="T302">Bejem </ts>
               <ts e="T304" id="Seg_1582" n="e" s="T303">bu͡ollagɨna </ts>
               <ts e="T305" id="Seg_1584" n="e" s="T304">alta </ts>
               <ts e="T306" id="Seg_1586" n="e" s="T305">duː, </ts>
               <ts e="T307" id="Seg_1588" n="e" s="T306">tü͡ört </ts>
               <ts e="T308" id="Seg_1590" n="e" s="T307">duː </ts>
               <ts e="T309" id="Seg_1592" n="e" s="T308">gramatalaːkpɨn </ts>
               <ts e="T310" id="Seg_1594" n="e" s="T309">anɨ. </ts>
               <ts e="T311" id="Seg_1596" n="e" s="T310">Ol </ts>
               <ts e="T312" id="Seg_1598" n="e" s="T311">di͡ek </ts>
               <ts e="T314" id="Seg_1600" n="e" s="T312">hɨtallar. </ts>
            </ts>
            <ts e="T339" id="Seg_1601" n="sc" s="T318">
               <ts e="T320" id="Seg_1603" n="e" s="T318">– Jakuːskajga, </ts>
               <ts e="T321" id="Seg_1605" n="e" s="T320">abrazavaːnʼijattan </ts>
               <ts e="T322" id="Seg_1607" n="e" s="T321">da </ts>
               <ts e="T323" id="Seg_1609" n="e" s="T322">baːr </ts>
               <ts e="T324" id="Seg_1611" n="e" s="T323">etilere. </ts>
               <ts e="T325" id="Seg_1613" n="e" s="T324">Baːllar </ts>
               <ts e="T326" id="Seg_1615" n="e" s="T325">eni, </ts>
               <ts e="T327" id="Seg_1617" n="e" s="T326">kajdi͡ek </ts>
               <ts e="T328" id="Seg_1619" n="e" s="T327">duː, </ts>
               <ts e="T329" id="Seg_1621" n="e" s="T328">kördökkö, </ts>
               <ts e="T330" id="Seg_1623" n="e" s="T329">onton </ts>
               <ts e="T331" id="Seg_1625" n="e" s="T330">tu͡oktan </ts>
               <ts e="T332" id="Seg_1627" n="e" s="T331">emi͡e… </ts>
               <ts e="T333" id="Seg_1629" n="e" s="T332">Kulʼturattan </ts>
               <ts e="T334" id="Seg_1631" n="e" s="T333">kas </ts>
               <ts e="T335" id="Seg_1633" n="e" s="T334">daːganɨ, </ts>
               <ts e="T336" id="Seg_1635" n="e" s="T335">glava admʼinʼistrasʼijattan </ts>
               <ts e="T337" id="Seg_1637" n="e" s="T336">emi͡e </ts>
               <ts e="T338" id="Seg_1639" n="e" s="T337">baːr </ts>
               <ts e="T339" id="Seg_1641" n="e" s="T338">bu͡olu͡oktaːk. </ts>
            </ts>
            <ts e="T350" id="Seg_1642" n="sc" s="T347">
               <ts e="T349" id="Seg_1644" n="e" s="T347">– Alʼenʼoskaj </ts>
               <ts e="T350" id="Seg_1646" n="e" s="T349">uluːs. </ts>
            </ts>
            <ts e="T376" id="Seg_1647" n="sc" s="T358">
               <ts e="T359" id="Seg_1649" n="e" s="T358">– Alʼenʼoskaj </ts>
               <ts e="T360" id="Seg_1651" n="e" s="T359">uluːs, </ts>
               <ts e="T361" id="Seg_1653" n="e" s="T360">arɨː </ts>
               <ts e="T362" id="Seg_1655" n="e" s="T361">bu͡o, </ts>
               <ts e="T363" id="Seg_1657" n="e" s="T362">min </ts>
               <ts e="T364" id="Seg_1659" n="e" s="T363">bu͡o </ts>
               <ts e="T365" id="Seg_1661" n="e" s="T364">oččogo </ts>
               <ts e="T366" id="Seg_1663" n="e" s="T365">onno </ts>
               <ts e="T367" id="Seg_1665" n="e" s="T366">Xarɨjalaːkka </ts>
               <ts e="T368" id="Seg_1667" n="e" s="T367">etim. </ts>
               <ts e="T369" id="Seg_1669" n="e" s="T368">Ginner </ts>
               <ts e="T370" id="Seg_1671" n="e" s="T369">bu͡ollagɨna </ts>
               <ts e="T371" id="Seg_1673" n="e" s="T370">iti </ts>
               <ts e="T372" id="Seg_1675" n="e" s="T371">bi͡erekke </ts>
               <ts e="T373" id="Seg_1677" n="e" s="T372">mannɨk, </ts>
               <ts e="T374" id="Seg_1679" n="e" s="T373">oŋu͡or-oŋu͡or </ts>
               <ts e="T376" id="Seg_1681" n="e" s="T374">olorollor. </ts>
            </ts>
            <ts e="T385" id="Seg_1682" n="sc" s="T379">
               <ts e="T380" id="Seg_1684" n="e" s="T379">– Ölöːnüŋ </ts>
               <ts e="T381" id="Seg_1686" n="e" s="T380">tuspa, </ts>
               <ts e="T382" id="Seg_1688" n="e" s="T381">Xarɨjalaːgɨŋ </ts>
               <ts e="T385" id="Seg_1690" n="e" s="T382">tuspa. </ts>
            </ts>
            <ts e="T391" id="Seg_1691" n="sc" s="T387">
               <ts e="T389" id="Seg_1693" n="e" s="T387">– Eː, </ts>
               <ts e="T390" id="Seg_1695" n="e" s="T389">ikki </ts>
               <ts e="T391" id="Seg_1697" n="e" s="T390">tu͡ok. </ts>
            </ts>
            <ts e="T417" id="Seg_1698" n="sc" s="T407">
               <ts e="T409" id="Seg_1700" n="e" s="T407">– Dolu͡oj </ts>
               <ts e="T410" id="Seg_1702" n="e" s="T409">küččügüjbütten, </ts>
               <ts e="T411" id="Seg_1704" n="e" s="T410">di͡eččiler. </ts>
               <ts e="T412" id="Seg_1706" n="e" s="T411">Bu </ts>
               <ts e="T413" id="Seg_1708" n="e" s="T412">dʼe, </ts>
               <ts e="T414" id="Seg_1710" n="e" s="T413">haːdikka </ts>
               <ts e="T415" id="Seg_1712" n="e" s="T414">hɨldʼarbɨttan </ts>
               <ts e="T416" id="Seg_1714" n="e" s="T415">ɨllɨːr </ts>
               <ts e="T417" id="Seg_1716" n="e" s="T416">ebippin. </ts>
            </ts>
            <ts e="T441" id="Seg_1717" n="sc" s="T429">
               <ts e="T430" id="Seg_1719" n="e" s="T429">– Bu </ts>
               <ts e="T431" id="Seg_1721" n="e" s="T430">nʼuːččalarɨ </ts>
               <ts e="T432" id="Seg_1723" n="e" s="T431">kennikiː </ts>
               <ts e="T433" id="Seg_1725" n="e" s="T432">ɨllɨːr </ts>
               <ts e="T434" id="Seg_1727" n="e" s="T433">bu͡oltum. </ts>
               <ts e="T435" id="Seg_1729" n="e" s="T434">Onton </ts>
               <ts e="T436" id="Seg_1731" n="e" s="T435">bu </ts>
               <ts e="T437" id="Seg_1733" n="e" s="T436">anɨ </ts>
               <ts e="T438" id="Seg_1735" n="e" s="T437">hakalarga. </ts>
               <ts e="T439" id="Seg_1737" n="e" s="T438">Hakalarɨ </ts>
               <ts e="T440" id="Seg_1739" n="e" s="T439">ere </ts>
               <ts e="T441" id="Seg_1741" n="e" s="T440">ɨllɨːbɨn. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-ZhNA">
            <ta e="T7" id="Seg_1742" s="T1">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.001 (001.001)</ta>
            <ta e="T16" id="Seg_1743" s="T7">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.002 (001.002)</ta>
            <ta e="T30" id="Seg_1744" s="T16">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.003 (001.003)</ta>
            <ta e="T37" id="Seg_1745" s="T34">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.004 (001.005)</ta>
            <ta e="T42" id="Seg_1746" s="T37">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.005 (001.006)</ta>
            <ta e="T50" id="Seg_1747" s="T42">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.006 (001.007)</ta>
            <ta e="T55" id="Seg_1748" s="T50">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.007 (001.008)</ta>
            <ta e="T56" id="Seg_1749" s="T55">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.008 (001.009)</ta>
            <ta e="T70" id="Seg_1750" s="T56">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.009 (001.010)</ta>
            <ta e="T72" id="Seg_1751" s="T70">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.010 (001.011)</ta>
            <ta e="T79" id="Seg_1752" s="T72">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.011 (001.012)</ta>
            <ta e="T82" id="Seg_1753" s="T79">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.012 (001.013)</ta>
            <ta e="T87" id="Seg_1754" s="T82">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.013 (001.014)</ta>
            <ta e="T91" id="Seg_1755" s="T87">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.014 (001.015)</ta>
            <ta e="T102" id="Seg_1756" s="T97">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.015 (001.017)</ta>
            <ta e="T111" id="Seg_1757" s="T102">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.016 (001.018)</ta>
            <ta e="T122" id="Seg_1758" s="T111">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.017 (001.019)</ta>
            <ta e="T129" id="Seg_1759" s="T124">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.018 (001.021)</ta>
            <ta e="T130" id="Seg_1760" s="T129">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.019 (001.022)</ta>
            <ta e="T133" id="Seg_1761" s="T130">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.020 (001.023)</ta>
            <ta e="T137" id="Seg_1762" s="T133">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.021 (001.024)</ta>
            <ta e="T149" id="Seg_1763" s="T141">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.022 (001.026)</ta>
            <ta e="T151" id="Seg_1764" s="T149">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.023 (001.027)</ta>
            <ta e="T168" id="Seg_1765" s="T159">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.024 (001.029)</ta>
            <ta e="T172" id="Seg_1766" s="T168">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.025 (001.030)</ta>
            <ta e="T185" id="Seg_1767" s="T176">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.026 (001.032)</ta>
            <ta e="T190" id="Seg_1768" s="T185">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.027 (001.033)</ta>
            <ta e="T196" id="Seg_1769" s="T190">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.028 (001.034)</ta>
            <ta e="T197" id="Seg_1770" s="T196">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.029 (001.035)</ta>
            <ta e="T217" id="Seg_1771" s="T208">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.030 (001.037)</ta>
            <ta e="T225" id="Seg_1772" s="T217">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.031 (001.038)</ta>
            <ta e="T229" id="Seg_1773" s="T227">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.032 (001.040)</ta>
            <ta e="T243" id="Seg_1774" s="T240">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.033 (001.042)</ta>
            <ta e="T260" id="Seg_1775" s="T250">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.034 (001.045)</ta>
            <ta e="T262" id="Seg_1776" s="T260">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.035 (001.046)</ta>
            <ta e="T271" id="Seg_1777" s="T262">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.036 (001.047)</ta>
            <ta e="T275" id="Seg_1778" s="T271">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.037 (001.048)</ta>
            <ta e="T279" id="Seg_1779" s="T275">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.038 (001.049)</ta>
            <ta e="T284" id="Seg_1780" s="T281">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.039 (001.051)</ta>
            <ta e="T300" id="Seg_1781" s="T291">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.040 (001.053)</ta>
            <ta e="T302" id="Seg_1782" s="T300">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.041 (001.054)</ta>
            <ta e="T310" id="Seg_1783" s="T302">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.042 (001.055)</ta>
            <ta e="T314" id="Seg_1784" s="T310">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.043 (001.056)</ta>
            <ta e="T324" id="Seg_1785" s="T318">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.044 (001.058)</ta>
            <ta e="T332" id="Seg_1786" s="T324">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.045 (001.059)</ta>
            <ta e="T339" id="Seg_1787" s="T332">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.046 (001.060)</ta>
            <ta e="T350" id="Seg_1788" s="T347">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.047 (001.062)</ta>
            <ta e="T368" id="Seg_1789" s="T358">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.048 (001.064)</ta>
            <ta e="T376" id="Seg_1790" s="T368">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.049 (001.065)</ta>
            <ta e="T385" id="Seg_1791" s="T379">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.050 (001.067)</ta>
            <ta e="T391" id="Seg_1792" s="T387">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.051 (001.069)</ta>
            <ta e="T411" id="Seg_1793" s="T407">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.052 (001.072)</ta>
            <ta e="T417" id="Seg_1794" s="T411">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.053 (001.073)</ta>
            <ta e="T434" id="Seg_1795" s="T429">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.054 (001.076)</ta>
            <ta e="T438" id="Seg_1796" s="T434">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.055 (001.077)</ta>
            <ta e="T441" id="Seg_1797" s="T438">ZhNA_KuNS_20XX_LifeAndMusic_conv.ZhNA.056 (001.078)</ta>
         </annotation>
         <annotation name="st" tierref="st-ZhNA">
            <ta e="T7" id="Seg_1798" s="T1">Ж: Мин буоллагына төрүтүм, бэйэм төрүтүм киэӈ. </ta>
            <ta e="T16" id="Seg_1799" s="T7">Дьакуускайга даа бааллар, ньууччалар даа бааллар, долгаттар да бааллар.</ta>
            <ta e="T30" id="Seg_1800" s="T16">Бастакыыннан буо Якутияттан мин эбэм буоллагына Хатангаттан огонньорго таксыбыта, ити испидицийага үлэлии һылдьан.</ta>
            <ta e="T37" id="Seg_1801" s="T34">Ж: Тимопиэйэба Дарья диэн.</ta>
            <ta e="T42" id="Seg_1802" s="T37">Онтон буоллагына баран огонньорго тагыста. </ta>
            <ta e="T50" id="Seg_1803" s="T42">Огонньордоро үс кыыһы төрөөбүт ол Дьакуускайга, Дьакуускайга баран.</ta>
            <ta e="T55" id="Seg_1804" s="T50">Онтон төттөрү көһөн кэлбиттэр манна.</ta>
            <ta e="T56" id="Seg_1805" s="T55">Попүгэйгэ.</ta>
            <ta e="T70" id="Seg_1806" s="T56">Попигайтан Катангага да үлэлии һыддьыбыта (һылдьыбыта), Касистайга да үлэлии һыддьыбыта (һылдьыбыта), һиргэ барытыгай дьэ үлэлии һыддьыбыт (һылдьыбыт).</ta>
            <ta e="T72" id="Seg_1807" s="T70">Һурагын иһиттэкпинэ.</ta>
            <ta e="T79" id="Seg_1808" s="T72">Онтон буоллагына ылгын кыыһын уола буолабын мин.</ta>
            <ta e="T82" id="Seg_1809" s="T79">Бэйэбит агыстарбыт төрүппүтүгэр.</ta>
            <ta e="T87" id="Seg_1810" s="T82">Аӈарбыт отто һуок, аӈарбыт баар.</ta>
            <ta e="T91" id="Seg_1811" s="T87">Дьэ ити көрдүк.</ta>
            <ta e="T102" id="Seg_1812" s="T97">Ж: Мин буоллагына Дьакутийага үөрэнэ һыддьыбытым (һылдьыбытым).</ta>
            <ta e="T111" id="Seg_1813" s="T102">Тогуон (тогус уон) ыллаактарга (дьыллаактарга), тогуон ыллаактарга (дьыллаактарга) буо манна ыллыы һыддьыбытым (һылдьыбытым), фестивальга. </ta>
            <ta e="T122" id="Seg_1814" s="T111">Онно өссүө миэстэ дуу, туок дуу баар көрдүк этэ буо.</ta>
            <ta e="T129" id="Seg_1815" s="T124">Ж: Һэ-э, Таймыырга.</ta>
            <ta e="T130" id="Seg_1816" s="T129">Ыллаабытым.</ta>
            <ta e="T133" id="Seg_1817" s="T130">Онно күччүгүй этим.</ta>
            <ta e="T137" id="Seg_1818" s="T133">Кас һаастаакпын бэйэм билбэппин.</ta>
            <ta e="T149" id="Seg_1819" s="T141">Ж: Һэ-э, Дудинкаттан, мантан, ылбыттара ити.</ta>
            <ta e="T151" id="Seg_1820" s="T149">Туботделенийаттан ылбыттара.</ta>
            <ta e="T168" id="Seg_1821" s="T159">Ж: Күччүгүй эрдэкпинэ буоллагына бэйэм чэ коһуйаммын ыллыр этим.</ta>
            <ta e="T172" id="Seg_1822" s="T168">Ол барыта умнуллубут.</ta>
            <ta e="T185" id="Seg_1823" s="T176">Ж: Һэ-э, һубу (таа..) (таӈа..) һаӈарар һаӈаларбын ыллыыр этим.</ta>
            <ta e="T190" id="Seg_1824" s="T185">Колобура, киһини көрдөкпүнэ, киһини коһуйабын.</ta>
            <ta e="T196" id="Seg_1825" s="T190">Тугу эрэ, ыты коһуйдакпына ыты коһуйабын.</ta>
            <ta e="T197" id="Seg_1826" s="T196">Итинник. </ta>
            <ta e="T217" id="Seg_1827" s="T208">Ж: Ким да ыллаабат этэ төрүппүттэн, но может, ити… </ta>
            <ta e="T225" id="Seg_1828" s="T217">Может, ойуттар эмиэ бааллара буолуо, олор…</ta>
            <ta e="T229" id="Seg_1829" s="T227">Ж: Һэ-э.</ta>
            <ta e="T243" id="Seg_1830" s="T240">Ж: Мм.</ta>
            <ta e="T260" id="Seg_1831" s="T250">Ж: Һэ-э, каталлалар, настаивайдыыллар, үлэлиир этим ол Дьакутийага.</ta>
            <ta e="T262" id="Seg_1832" s="T260">Туокка, техработнигынан.</ta>
            <ta e="T271" id="Seg_1833" s="T262">Ол үлэлээммин, чэ, бэйэм туокпар һылдьааччыбын мин концертарга туоктарга.</ta>
            <ta e="T275" id="Seg_1834" s="T271">Вахтёрша көрдүк этим бэйэм.</ta>
            <ta e="T279" id="Seg_1835" s="T275">Билет, эӈин атыылыыбын итинниктэргэ.</ta>
            <ta e="T284" id="Seg_1836" s="T281">Ж: Кулуубка үлэлээбитим.</ta>
            <ta e="T300" id="Seg_1837" s="T291">Ж: Һэ-э, oл сценага таксыакпын анаан исписээльно ылбыттара.</ta>
            <ta e="T302" id="Seg_1838" s="T300">Бэйэбин биллээриэкпин.</ta>
            <ta e="T310" id="Seg_1839" s="T302">Бэйэм буоллагына алта дуу, түөрт дуу грамоталаакпын аны.</ta>
            <ta e="T314" id="Seg_1840" s="T310">Ол диэк һыталлар.</ta>
            <ta e="T324" id="Seg_1841" s="T318">Ж: Якутскайга, oбразованияттан да баар этилэрэ.</ta>
            <ta e="T332" id="Seg_1842" s="T324">Ж: Бааллар эни, кайдиэк дуу, көрдөккө, онтон туоктан эмиэ…</ta>
            <ta e="T339" id="Seg_1843" s="T332">Культураттан кас дааганы, глава администрацияттан эмиэ баар буолуоктаак.</ta>
            <ta e="T350" id="Seg_1844" s="T347">Ж: Оленёкский улуус.</ta>
            <ta e="T368" id="Seg_1845" s="T358">Ж: Оленёкский улуус, ары буо, мин буо оччого онно Карыйалаакка этим.</ta>
            <ta e="T376" id="Seg_1846" s="T368">Гиннэр буоллагына ити биэриккэ маннык, оӈуор-оӈуор олороллор.</ta>
            <ta e="T385" id="Seg_1847" s="T379">Ж: Өлөөнүӈ туспа, Карыйалаагыӈ туспа.</ta>
            <ta e="T391" id="Seg_1848" s="T387">Ж: Ээ, икки туок.</ta>
            <ta e="T411" id="Seg_1849" s="T407">Ж: Долуой күччүгүйбүттэн, диэччилэр.</ta>
            <ta e="T417" id="Seg_1850" s="T411">Бу дьэ, һаадикка һылдьарбыттан ыллыыр, эбиппин.</ta>
            <ta e="T434" id="Seg_1851" s="T429">Ж: Бу ньууччалары кэнникии ыллыыр буолтум.</ta>
            <ta e="T438" id="Seg_1852" s="T434">Онтон бу аны һакаларга. </ta>
            <ta e="T441" id="Seg_1853" s="T438">Һакалары эрэ ыллыыбын.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-ZhNA">
            <ta e="T7" id="Seg_1854" s="T1">– Min bu͡ollagɨna törütüm, bejem törütüm ki͡eŋ. </ta>
            <ta e="T16" id="Seg_1855" s="T7">Dʼakuːskajga daː baːllar, nʼuːččalar daː baːllar, dalgattar da baːllar. </ta>
            <ta e="T30" id="Seg_1856" s="T16">Bastakɨːnnan bu͡o Džakutidžattan min ebem bu͡ollagɨna Xatangattan ogonnʼorgo taksɨbɨta, iti ispidisijaga üleliː hɨldʼan. </ta>
            <ta e="T37" id="Seg_1857" s="T34">– Tʼimapʼejeba Darʼja di͡en. </ta>
            <ta e="T42" id="Seg_1858" s="T37">Onton bu͡ollagɨna baran ogonnʼorgo tagɨsta. </ta>
            <ta e="T50" id="Seg_1859" s="T42">Ogonnʼordoro üs kɨːhɨ töröːbüt ol Džakuːskajga, Dʼakuːskajga baran. </ta>
            <ta e="T55" id="Seg_1860" s="T50">Onton töttörü köhön kelbitter manna. </ta>
            <ta e="T56" id="Seg_1861" s="T55">Pöpügejge. </ta>
            <ta e="T70" id="Seg_1862" s="T56">Papigajtan Katangaga da üleliː hɨddʼɨbɨta, Kasistajga da üleliː hɨddʼɨbɨta, hirge barɨtɨgar dʼe üleliː hɨddžɨbɨt. </ta>
            <ta e="T72" id="Seg_1863" s="T70">Huragɨn ihittekpine. </ta>
            <ta e="T79" id="Seg_1864" s="T72">Onton bu͡ollagɨna ɨlgɨn kɨːhɨn u͡ola bu͡olabɨn min. </ta>
            <ta e="T82" id="Seg_1865" s="T79">Bejebit agɨstarbɨt törüppütüger. </ta>
            <ta e="T87" id="Seg_1866" s="T82">Aŋarbɨt otto hu͡ok, aŋarbɨt baːr. </ta>
            <ta e="T91" id="Seg_1867" s="T87">Dʼe iti kördük. </ta>
            <ta e="T102" id="Seg_1868" s="T97">– Min bu͡ollagɨna Džakutijaga ü͡örene hɨldžɨbɨtɨm. </ta>
            <ta e="T111" id="Seg_1869" s="T102">Togu͡on ɨllaːktarga, togu͡on ɨllaːktarga bu͡o manna ɨllɨː hɨldžɨbɨtɨm, festivalʼga. </ta>
            <ta e="T122" id="Seg_1870" s="T111">Onno össü͡ö mi͡este duː, tu͡ok duː baːr kördük ete bu͡o. </ta>
            <ta e="T129" id="Seg_1871" s="T124">– Heː, Tajmɨːrga. </ta>
            <ta e="T130" id="Seg_1872" s="T129">ɨllaːbɨtɨm. </ta>
            <ta e="T133" id="Seg_1873" s="T130">Onno küččügüj etim. </ta>
            <ta e="T137" id="Seg_1874" s="T133">Kas haːstaːkpɨn bejem bilbeppin. </ta>
            <ta e="T149" id="Seg_1875" s="T141">– Heː, Dudʼinkattan, mantan, ɨlbɨttara iti. </ta>
            <ta e="T151" id="Seg_1876" s="T149">Tubatdʼelʼenʼijattan ɨlbɨttara. </ta>
            <ta e="T168" id="Seg_1877" s="T159">– Küččügüj erdekpine bu͡ollagɨna bejem če kohujammɨn ɨllɨːr etim. </ta>
            <ta e="T172" id="Seg_1878" s="T168">Ol barɨta umnullubut. </ta>
            <ta e="T185" id="Seg_1879" s="T176">– Heː, hubu (ta-) (taŋa-) haŋarar haŋalarbɨn ɨllɨːr etim. </ta>
            <ta e="T190" id="Seg_1880" s="T185">Kolobura, kihini kördökpüne, kihini kohujabɨn. </ta>
            <ta e="T196" id="Seg_1881" s="T190">Tugu ere, ɨtɨ kohujdakpɨna ɨtɨ kohujabɨn. </ta>
            <ta e="T197" id="Seg_1882" s="T196">Itinnik. </ta>
            <ta e="T217" id="Seg_1883" s="T208">– Kim da ɨllaːbat ete törüppütten, no možet, iti… </ta>
            <ta e="T225" id="Seg_1884" s="T217">Možet, ojuttar emi͡e baːllara bu͡olu͡o, olor ((…)). </ta>
            <ta e="T229" id="Seg_1885" s="T227">– Heː. </ta>
            <ta e="T243" id="Seg_1886" s="T240">– Mm. </ta>
            <ta e="T260" id="Seg_1887" s="T250">– Mm, katallalar, nastaivajdɨːllar, üleliːr etim ol Dʼakutijaga. </ta>
            <ta e="T262" id="Seg_1888" s="T260">Tu͡okka, tʼexrabotnʼigɨnan. </ta>
            <ta e="T271" id="Seg_1889" s="T262">Ol üleleːmmin, če, bejem tu͡okpar hɨldʼaːččɨbɨn min kansertarga tu͡oktarga. </ta>
            <ta e="T275" id="Seg_1890" s="T271">Vaxtʼorša kördük etim bejem. </ta>
            <ta e="T279" id="Seg_1891" s="T275">Bilʼet, eŋin atɨːlɨːbɨn itinnikterge. </ta>
            <ta e="T284" id="Seg_1892" s="T281">– Kuluːbka üleleːbitim. </ta>
            <ta e="T300" id="Seg_1893" s="T291">– Mm, ol senaga taksɨ͡akpɨn anaːn ispisʼalno ɨlbɨttara. </ta>
            <ta e="T302" id="Seg_1894" s="T300">Bejebin billeri͡ekpin. </ta>
            <ta e="T310" id="Seg_1895" s="T302">Bejem bu͡ollagɨna alta duː, tü͡ört duː gramatalaːkpɨn anɨ. </ta>
            <ta e="T314" id="Seg_1896" s="T310">Ol di͡ek hɨtallar. </ta>
            <ta e="T324" id="Seg_1897" s="T318">– Jakuːskajga, abrazavaːnʼijattan da baːr etilere. </ta>
            <ta e="T332" id="Seg_1898" s="T324">Baːllar eni, kajdi͡ek duː, kördökkö, onton tu͡oktan emi͡e… </ta>
            <ta e="T339" id="Seg_1899" s="T332">Kulʼturattan kas daːganɨ, glava admʼinʼistrasʼijattan emi͡e baːr bu͡olu͡oktaːk. </ta>
            <ta e="T350" id="Seg_1900" s="T347">– Alʼenʼoskaj uluːs. </ta>
            <ta e="T368" id="Seg_1901" s="T358">– Alʼenʼoskaj uluːs, arɨː bu͡o, min bu͡o oččogo onno Xarɨjalaːkka etim. </ta>
            <ta e="T376" id="Seg_1902" s="T368">Ginner bu͡ollagɨna iti bi͡erekke mannɨk, oŋu͡or-oŋu͡or olorollor. </ta>
            <ta e="T385" id="Seg_1903" s="T379">– Ölöːnüŋ tuspa, Xarɨjalaːgɨŋ tuspa. </ta>
            <ta e="T391" id="Seg_1904" s="T387">– Eː, ikki tu͡ok. </ta>
            <ta e="T411" id="Seg_1905" s="T407">– Dolu͡oj küččügüjbütten, di͡eččiler. </ta>
            <ta e="T417" id="Seg_1906" s="T411">Bu dʼe, haːdikka hɨldʼarbɨttan ɨllɨːr ebippin. </ta>
            <ta e="T434" id="Seg_1907" s="T429">– Bu nʼuːččalarɨ kennikiː ɨllɨːr bu͡oltum. </ta>
            <ta e="T438" id="Seg_1908" s="T434">Onton bu anɨ hakalarga. </ta>
            <ta e="T441" id="Seg_1909" s="T438">Hakalarɨ ere ɨllɨːbɨn. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-ZhNA">
            <ta e="T2" id="Seg_1910" s="T1">min</ta>
            <ta e="T3" id="Seg_1911" s="T2">bu͡ollagɨna</ta>
            <ta e="T4" id="Seg_1912" s="T3">törüt-ü-m</ta>
            <ta e="T5" id="Seg_1913" s="T4">beje-m</ta>
            <ta e="T6" id="Seg_1914" s="T5">törüt-ü-m</ta>
            <ta e="T7" id="Seg_1915" s="T6">ki͡eŋ</ta>
            <ta e="T8" id="Seg_1916" s="T7">Dʼakuːskaj-ga</ta>
            <ta e="T9" id="Seg_1917" s="T8">daː</ta>
            <ta e="T10" id="Seg_1918" s="T9">baːl-lar</ta>
            <ta e="T11" id="Seg_1919" s="T10">nʼuːčča-lar</ta>
            <ta e="T12" id="Seg_1920" s="T11">daː</ta>
            <ta e="T13" id="Seg_1921" s="T12">baːl-lar</ta>
            <ta e="T14" id="Seg_1922" s="T13">dalgat-tar</ta>
            <ta e="T15" id="Seg_1923" s="T14">da</ta>
            <ta e="T16" id="Seg_1924" s="T15">baːl-lar</ta>
            <ta e="T17" id="Seg_1925" s="T16">bastakɨː-nnan</ta>
            <ta e="T18" id="Seg_1926" s="T17">bu͡o</ta>
            <ta e="T19" id="Seg_1927" s="T18">Džakutidža-ttan</ta>
            <ta e="T20" id="Seg_1928" s="T19">min</ta>
            <ta e="T21" id="Seg_1929" s="T20">ebe-m</ta>
            <ta e="T22" id="Seg_1930" s="T21">bu͡ollagɨna</ta>
            <ta e="T23" id="Seg_1931" s="T22">Xatanga-ttan</ta>
            <ta e="T24" id="Seg_1932" s="T23">ogonnʼor-go</ta>
            <ta e="T25" id="Seg_1933" s="T24">taks-ɨ-bɨt-a</ta>
            <ta e="T26" id="Seg_1934" s="T25">iti</ta>
            <ta e="T27" id="Seg_1935" s="T26">ispidisija-ga</ta>
            <ta e="T28" id="Seg_1936" s="T27">ülel-iː</ta>
            <ta e="T30" id="Seg_1937" s="T28">hɨldʼ-an</ta>
            <ta e="T35" id="Seg_1938" s="T34">Tʼimapʼejeba</ta>
            <ta e="T36" id="Seg_1939" s="T35">Darʼja</ta>
            <ta e="T37" id="Seg_1940" s="T36">di͡e-n</ta>
            <ta e="T38" id="Seg_1941" s="T37">onton</ta>
            <ta e="T39" id="Seg_1942" s="T38">bu͡ollagɨna</ta>
            <ta e="T40" id="Seg_1943" s="T39">bar-an</ta>
            <ta e="T41" id="Seg_1944" s="T40">ogonnʼor-go</ta>
            <ta e="T42" id="Seg_1945" s="T41">tagɨs-t-a</ta>
            <ta e="T43" id="Seg_1946" s="T42">ogonnʼor-doro</ta>
            <ta e="T44" id="Seg_1947" s="T43">üs</ta>
            <ta e="T45" id="Seg_1948" s="T44">kɨːh-ɨ</ta>
            <ta e="T46" id="Seg_1949" s="T45">töröː-büt</ta>
            <ta e="T47" id="Seg_1950" s="T46">ol</ta>
            <ta e="T48" id="Seg_1951" s="T47">Džakuːskaj-ga</ta>
            <ta e="T49" id="Seg_1952" s="T48">Dʼakuːskaj-ga</ta>
            <ta e="T50" id="Seg_1953" s="T49">bar-an</ta>
            <ta e="T51" id="Seg_1954" s="T50">onton</ta>
            <ta e="T52" id="Seg_1955" s="T51">töttörü</ta>
            <ta e="T53" id="Seg_1956" s="T52">köh-ön</ta>
            <ta e="T54" id="Seg_1957" s="T53">kel-bit-ter</ta>
            <ta e="T55" id="Seg_1958" s="T54">manna</ta>
            <ta e="T56" id="Seg_1959" s="T55">Pöpügej-ge</ta>
            <ta e="T57" id="Seg_1960" s="T56">Papigaj-tan</ta>
            <ta e="T58" id="Seg_1961" s="T57">Katanga-ga</ta>
            <ta e="T59" id="Seg_1962" s="T58">da</ta>
            <ta e="T60" id="Seg_1963" s="T59">ülel-iː</ta>
            <ta e="T61" id="Seg_1964" s="T60">hɨddʼ-ɨ-bɨt-a</ta>
            <ta e="T62" id="Seg_1965" s="T61">Kasistaj-ga</ta>
            <ta e="T63" id="Seg_1966" s="T62">da</ta>
            <ta e="T64" id="Seg_1967" s="T63">ülel-iː</ta>
            <ta e="T65" id="Seg_1968" s="T64">hɨddʼ-ɨ-bɨt-a</ta>
            <ta e="T66" id="Seg_1969" s="T65">hir-ge</ta>
            <ta e="T67" id="Seg_1970" s="T66">barɨ-tɨ-gar</ta>
            <ta e="T68" id="Seg_1971" s="T67">dʼe</ta>
            <ta e="T69" id="Seg_1972" s="T68">ülel-iː</ta>
            <ta e="T70" id="Seg_1973" s="T69">hɨddž-ɨ-bɨt</ta>
            <ta e="T71" id="Seg_1974" s="T70">hurag-ɨ-n</ta>
            <ta e="T72" id="Seg_1975" s="T71">ihit-tek-pine</ta>
            <ta e="T73" id="Seg_1976" s="T72">onton</ta>
            <ta e="T74" id="Seg_1977" s="T73">bu͡ollagɨna</ta>
            <ta e="T75" id="Seg_1978" s="T74">ɨlgɨn</ta>
            <ta e="T76" id="Seg_1979" s="T75">kɨːh-ɨ-n</ta>
            <ta e="T77" id="Seg_1980" s="T76">u͡ol-a</ta>
            <ta e="T78" id="Seg_1981" s="T77">bu͡ol-a-bɨn</ta>
            <ta e="T79" id="Seg_1982" s="T78">min</ta>
            <ta e="T80" id="Seg_1983" s="T79">beje-bit</ta>
            <ta e="T81" id="Seg_1984" s="T80">agɨs-tar-bɨt</ta>
            <ta e="T82" id="Seg_1985" s="T81">törüp-pütü-ger</ta>
            <ta e="T83" id="Seg_1986" s="T82">aŋar-bɨt</ta>
            <ta e="T84" id="Seg_1987" s="T83">otto</ta>
            <ta e="T85" id="Seg_1988" s="T84">hu͡ok</ta>
            <ta e="T86" id="Seg_1989" s="T85">aŋar-bɨt</ta>
            <ta e="T87" id="Seg_1990" s="T86">baːr</ta>
            <ta e="T88" id="Seg_1991" s="T87">dʼe</ta>
            <ta e="T89" id="Seg_1992" s="T88">iti</ta>
            <ta e="T91" id="Seg_1993" s="T89">kördük</ta>
            <ta e="T98" id="Seg_1994" s="T97">min</ta>
            <ta e="T99" id="Seg_1995" s="T98">bu͡ollagɨna</ta>
            <ta e="T100" id="Seg_1996" s="T99">Džakutija-ga</ta>
            <ta e="T101" id="Seg_1997" s="T100">ü͡ören-e</ta>
            <ta e="T102" id="Seg_1998" s="T101">hɨldžɨ-bɨt-ɨ-m</ta>
            <ta e="T103" id="Seg_1999" s="T102">tog-u͡on</ta>
            <ta e="T104" id="Seg_2000" s="T103">ɨl-laːk-tar-ga</ta>
            <ta e="T105" id="Seg_2001" s="T104">tog-u͡on</ta>
            <ta e="T106" id="Seg_2002" s="T105">ɨl-laːk-tar-ga</ta>
            <ta e="T107" id="Seg_2003" s="T106">bu͡o</ta>
            <ta e="T108" id="Seg_2004" s="T107">manna</ta>
            <ta e="T109" id="Seg_2005" s="T108">ɨll-ɨː</ta>
            <ta e="T110" id="Seg_2006" s="T109">hɨldžɨ-bɨt-ɨ-m</ta>
            <ta e="T111" id="Seg_2007" s="T110">festivalʼ-ga</ta>
            <ta e="T112" id="Seg_2008" s="T111">onno</ta>
            <ta e="T113" id="Seg_2009" s="T112">össü͡ö</ta>
            <ta e="T114" id="Seg_2010" s="T113">mi͡este</ta>
            <ta e="T115" id="Seg_2011" s="T114">duː</ta>
            <ta e="T116" id="Seg_2012" s="T115">tu͡ok</ta>
            <ta e="T117" id="Seg_2013" s="T116">duː</ta>
            <ta e="T118" id="Seg_2014" s="T117">baːr</ta>
            <ta e="T119" id="Seg_2015" s="T118">kördük</ta>
            <ta e="T120" id="Seg_2016" s="T119">e-t-e</ta>
            <ta e="T122" id="Seg_2017" s="T120">bu͡o</ta>
            <ta e="T127" id="Seg_2018" s="T124">heː</ta>
            <ta e="T129" id="Seg_2019" s="T127">Tajmɨːr-ga</ta>
            <ta e="T130" id="Seg_2020" s="T129">ɨllaː-bɨt-ɨ-m</ta>
            <ta e="T131" id="Seg_2021" s="T130">onno</ta>
            <ta e="T132" id="Seg_2022" s="T131">küččügüj</ta>
            <ta e="T133" id="Seg_2023" s="T132">e-ti-m</ta>
            <ta e="T134" id="Seg_2024" s="T133">kas</ta>
            <ta e="T135" id="Seg_2025" s="T134">haːs-taːk-pɨn</ta>
            <ta e="T136" id="Seg_2026" s="T135">beje-m</ta>
            <ta e="T137" id="Seg_2027" s="T136">bil-bep-pin</ta>
            <ta e="T144" id="Seg_2028" s="T141">heː</ta>
            <ta e="T146" id="Seg_2029" s="T144">Dudʼinka-ttan</ta>
            <ta e="T147" id="Seg_2030" s="T146">mantan</ta>
            <ta e="T148" id="Seg_2031" s="T147">ɨl-bɨt-tara</ta>
            <ta e="T149" id="Seg_2032" s="T148">iti</ta>
            <ta e="T150" id="Seg_2033" s="T149">tubatdʼelʼenʼija-ttan</ta>
            <ta e="T151" id="Seg_2034" s="T150">ɨl-bɨt-tara</ta>
            <ta e="T161" id="Seg_2035" s="T159">küččügüj</ta>
            <ta e="T162" id="Seg_2036" s="T161">er-dek-pine</ta>
            <ta e="T163" id="Seg_2037" s="T162">bu͡ollagɨna</ta>
            <ta e="T164" id="Seg_2038" s="T163">beje-m</ta>
            <ta e="T165" id="Seg_2039" s="T164">če</ta>
            <ta e="T166" id="Seg_2040" s="T165">kohuj-am-mɨn</ta>
            <ta e="T167" id="Seg_2041" s="T166">ɨllɨː-r</ta>
            <ta e="T168" id="Seg_2042" s="T167">e-ti-m</ta>
            <ta e="T169" id="Seg_2043" s="T168">ol</ta>
            <ta e="T170" id="Seg_2044" s="T169">barɨ-ta</ta>
            <ta e="T172" id="Seg_2045" s="T170">umn-u-ll-u-but</ta>
            <ta e="T178" id="Seg_2046" s="T176">heː</ta>
            <ta e="T179" id="Seg_2047" s="T178">hubu</ta>
            <ta e="T182" id="Seg_2048" s="T181">haŋar-ar</ta>
            <ta e="T183" id="Seg_2049" s="T182">haŋa-lar-bɨ-n</ta>
            <ta e="T184" id="Seg_2050" s="T183">ɨllɨː-r</ta>
            <ta e="T185" id="Seg_2051" s="T184">e-ti-m</ta>
            <ta e="T186" id="Seg_2052" s="T185">kolobur-a</ta>
            <ta e="T187" id="Seg_2053" s="T186">kihi-ni</ta>
            <ta e="T188" id="Seg_2054" s="T187">kör-dök-püne</ta>
            <ta e="T189" id="Seg_2055" s="T188">kihi-ni</ta>
            <ta e="T190" id="Seg_2056" s="T189">kohuj-a-bɨn</ta>
            <ta e="T191" id="Seg_2057" s="T190">tug-u</ta>
            <ta e="T192" id="Seg_2058" s="T191">ere</ta>
            <ta e="T193" id="Seg_2059" s="T192">ɨt-ɨ</ta>
            <ta e="T194" id="Seg_2060" s="T193">kohuj-dak-pɨna</ta>
            <ta e="T195" id="Seg_2061" s="T194">ɨt-ɨ</ta>
            <ta e="T196" id="Seg_2062" s="T195">kohuj-a-bɨn</ta>
            <ta e="T197" id="Seg_2063" s="T196">itinnik</ta>
            <ta e="T210" id="Seg_2064" s="T208">kim</ta>
            <ta e="T211" id="Seg_2065" s="T210">da</ta>
            <ta e="T212" id="Seg_2066" s="T211">ɨllaː-bat</ta>
            <ta e="T213" id="Seg_2067" s="T212">e-t-e</ta>
            <ta e="T214" id="Seg_2068" s="T213">törüp-pü-tten</ta>
            <ta e="T215" id="Seg_2069" s="T214">no</ta>
            <ta e="T216" id="Seg_2070" s="T215">možet</ta>
            <ta e="T217" id="Seg_2071" s="T216">iti</ta>
            <ta e="T218" id="Seg_2072" s="T217">možet</ta>
            <ta e="T219" id="Seg_2073" s="T218">ojut-tar</ta>
            <ta e="T220" id="Seg_2074" s="T219">emi͡e</ta>
            <ta e="T221" id="Seg_2075" s="T220">baːl-lara</ta>
            <ta e="T222" id="Seg_2076" s="T221">bu͡ol-u͡o</ta>
            <ta e="T223" id="Seg_2077" s="T222">o-lor</ta>
            <ta e="T229" id="Seg_2078" s="T227">heː</ta>
            <ta e="T243" id="Seg_2079" s="T240">mm</ta>
            <ta e="T253" id="Seg_2080" s="T250">mm</ta>
            <ta e="T255" id="Seg_2081" s="T253">kat-a-ll-a-lar</ta>
            <ta e="T256" id="Seg_2082" s="T255">nastaivaj-dɨː-l-lar</ta>
            <ta e="T257" id="Seg_2083" s="T256">üleliː-r</ta>
            <ta e="T258" id="Seg_2084" s="T257">e-ti-m</ta>
            <ta e="T259" id="Seg_2085" s="T258">ol</ta>
            <ta e="T260" id="Seg_2086" s="T259">Dʼakutija-ga</ta>
            <ta e="T261" id="Seg_2087" s="T260">tu͡ok-ka</ta>
            <ta e="T262" id="Seg_2088" s="T261">tʼexrabotnʼig-ɨ-nan</ta>
            <ta e="T263" id="Seg_2089" s="T262">ol</ta>
            <ta e="T264" id="Seg_2090" s="T263">üleleː-m-min</ta>
            <ta e="T265" id="Seg_2091" s="T264">če</ta>
            <ta e="T266" id="Seg_2092" s="T265">beje-m</ta>
            <ta e="T267" id="Seg_2093" s="T266">tu͡ok-pa-r</ta>
            <ta e="T268" id="Seg_2094" s="T267">hɨldʼ-aːččɨ-bɨn</ta>
            <ta e="T269" id="Seg_2095" s="T268">min</ta>
            <ta e="T270" id="Seg_2096" s="T269">kanser-tar-ga</ta>
            <ta e="T271" id="Seg_2097" s="T270">tu͡ok-tar-ga</ta>
            <ta e="T272" id="Seg_2098" s="T271">vaxtʼorša</ta>
            <ta e="T273" id="Seg_2099" s="T272">kördük</ta>
            <ta e="T274" id="Seg_2100" s="T273">e-ti-m</ta>
            <ta e="T275" id="Seg_2101" s="T274">beje-m</ta>
            <ta e="T276" id="Seg_2102" s="T275">bilʼet</ta>
            <ta e="T277" id="Seg_2103" s="T276">eŋin</ta>
            <ta e="T278" id="Seg_2104" s="T277">atɨːl-ɨː-bɨn</ta>
            <ta e="T279" id="Seg_2105" s="T278">itinnik-ter-ge</ta>
            <ta e="T282" id="Seg_2106" s="T281">kuluːb-ka</ta>
            <ta e="T284" id="Seg_2107" s="T282">üleleː-bit-i-m</ta>
            <ta e="T293" id="Seg_2108" s="T291">mm</ta>
            <ta e="T295" id="Seg_2109" s="T293">ol</ta>
            <ta e="T296" id="Seg_2110" s="T295">sena-ga</ta>
            <ta e="T297" id="Seg_2111" s="T296">taks-ɨ͡ak-pɨ-n</ta>
            <ta e="T298" id="Seg_2112" s="T297">anaːn</ta>
            <ta e="T299" id="Seg_2113" s="T298">ispisʼalno</ta>
            <ta e="T300" id="Seg_2114" s="T299">ɨl-bɨt-tara</ta>
            <ta e="T301" id="Seg_2115" s="T300">beje-bi-n</ta>
            <ta e="T302" id="Seg_2116" s="T301">bil-ler-i͡ek-pi-n</ta>
            <ta e="T303" id="Seg_2117" s="T302">beje-m</ta>
            <ta e="T304" id="Seg_2118" s="T303">bu͡ollagɨna</ta>
            <ta e="T305" id="Seg_2119" s="T304">alta</ta>
            <ta e="T306" id="Seg_2120" s="T305">duː</ta>
            <ta e="T307" id="Seg_2121" s="T306">tü͡ört</ta>
            <ta e="T308" id="Seg_2122" s="T307">duː</ta>
            <ta e="T309" id="Seg_2123" s="T308">gramata-laːk-pɨn</ta>
            <ta e="T310" id="Seg_2124" s="T309">anɨ</ta>
            <ta e="T311" id="Seg_2125" s="T310">ol</ta>
            <ta e="T312" id="Seg_2126" s="T311">di͡ek</ta>
            <ta e="T314" id="Seg_2127" s="T312">hɨt-al-lar</ta>
            <ta e="T320" id="Seg_2128" s="T318">Jakuːskaj-ga</ta>
            <ta e="T321" id="Seg_2129" s="T320">abrazavaːnʼija-ttan</ta>
            <ta e="T322" id="Seg_2130" s="T321">da</ta>
            <ta e="T323" id="Seg_2131" s="T322">baːr</ta>
            <ta e="T324" id="Seg_2132" s="T323">e-ti-lere</ta>
            <ta e="T325" id="Seg_2133" s="T324">baːl-lar</ta>
            <ta e="T326" id="Seg_2134" s="T325">eni</ta>
            <ta e="T327" id="Seg_2135" s="T326">kajdi͡ek</ta>
            <ta e="T328" id="Seg_2136" s="T327">duː</ta>
            <ta e="T329" id="Seg_2137" s="T328">kör-dök-kö</ta>
            <ta e="T330" id="Seg_2138" s="T329">onton</ta>
            <ta e="T331" id="Seg_2139" s="T330">tu͡ok-tan</ta>
            <ta e="T332" id="Seg_2140" s="T331">emi͡e</ta>
            <ta e="T333" id="Seg_2141" s="T332">kulʼtura-ttan</ta>
            <ta e="T334" id="Seg_2142" s="T333">kas</ta>
            <ta e="T335" id="Seg_2143" s="T334">daːganɨ</ta>
            <ta e="T336" id="Seg_2144" s="T335">glava admʼinʼistrasʼija-ttan</ta>
            <ta e="T337" id="Seg_2145" s="T336">emi͡e</ta>
            <ta e="T338" id="Seg_2146" s="T337">baːr</ta>
            <ta e="T339" id="Seg_2147" s="T338">bu͡ol-u͡ok-taːk</ta>
            <ta e="T349" id="Seg_2148" s="T347">Alʼenʼoskaj</ta>
            <ta e="T350" id="Seg_2149" s="T349">uluːs</ta>
            <ta e="T359" id="Seg_2150" s="T358">Alʼenʼoskaj</ta>
            <ta e="T360" id="Seg_2151" s="T359">uluːs</ta>
            <ta e="T361" id="Seg_2152" s="T360">arɨː</ta>
            <ta e="T362" id="Seg_2153" s="T361">bu͡o</ta>
            <ta e="T363" id="Seg_2154" s="T362">min</ta>
            <ta e="T364" id="Seg_2155" s="T363">bu͡o</ta>
            <ta e="T365" id="Seg_2156" s="T364">oččogo</ta>
            <ta e="T366" id="Seg_2157" s="T365">onno</ta>
            <ta e="T367" id="Seg_2158" s="T366">Xarɨjalaːk-ka</ta>
            <ta e="T368" id="Seg_2159" s="T367">e-ti-m</ta>
            <ta e="T369" id="Seg_2160" s="T368">ginner</ta>
            <ta e="T370" id="Seg_2161" s="T369">bu͡ollagɨna</ta>
            <ta e="T371" id="Seg_2162" s="T370">iti</ta>
            <ta e="T372" id="Seg_2163" s="T371">bi͡erek-ke</ta>
            <ta e="T373" id="Seg_2164" s="T372">mannɨk</ta>
            <ta e="T374" id="Seg_2165" s="T373">oŋu͡or-oŋu͡or</ta>
            <ta e="T376" id="Seg_2166" s="T374">olor-ol-lor</ta>
            <ta e="T380" id="Seg_2167" s="T379">Ölöːn-ü-ŋ</ta>
            <ta e="T381" id="Seg_2168" s="T380">tuspa</ta>
            <ta e="T382" id="Seg_2169" s="T381">Xarɨjalaːg-ɨ-ŋ</ta>
            <ta e="T385" id="Seg_2170" s="T382">tuspa</ta>
            <ta e="T389" id="Seg_2171" s="T387">eː</ta>
            <ta e="T390" id="Seg_2172" s="T389">ikki</ta>
            <ta e="T391" id="Seg_2173" s="T390">tu͡ok</ta>
            <ta e="T409" id="Seg_2174" s="T407">dolu͡oj</ta>
            <ta e="T410" id="Seg_2175" s="T409">küččügüj-bü-tten</ta>
            <ta e="T411" id="Seg_2176" s="T410">di͡e-čči-ler</ta>
            <ta e="T412" id="Seg_2177" s="T411">bu</ta>
            <ta e="T413" id="Seg_2178" s="T412">dʼe</ta>
            <ta e="T414" id="Seg_2179" s="T413">haːdik-ka</ta>
            <ta e="T415" id="Seg_2180" s="T414">hɨldʼ-ar-bɨ-ttan</ta>
            <ta e="T416" id="Seg_2181" s="T415">ɨllɨː-r</ta>
            <ta e="T417" id="Seg_2182" s="T416">e-bip-pin</ta>
            <ta e="T430" id="Seg_2183" s="T429">bu</ta>
            <ta e="T431" id="Seg_2184" s="T430">nʼuːčča-lar-ɨ</ta>
            <ta e="T432" id="Seg_2185" s="T431">kenni-kiː</ta>
            <ta e="T433" id="Seg_2186" s="T432">ɨllɨː-r</ta>
            <ta e="T434" id="Seg_2187" s="T433">bu͡ol-t-u-m</ta>
            <ta e="T435" id="Seg_2188" s="T434">onton</ta>
            <ta e="T436" id="Seg_2189" s="T435">bu</ta>
            <ta e="T437" id="Seg_2190" s="T436">anɨ</ta>
            <ta e="T438" id="Seg_2191" s="T437">haka-lar-ga</ta>
            <ta e="T439" id="Seg_2192" s="T438">haka-lar-ɨ</ta>
            <ta e="T440" id="Seg_2193" s="T439">ere</ta>
            <ta e="T441" id="Seg_2194" s="T440">ɨll-ɨː-bɨn</ta>
         </annotation>
         <annotation name="mp" tierref="mp-ZhNA">
            <ta e="T2" id="Seg_2195" s="T1">min</ta>
            <ta e="T3" id="Seg_2196" s="T2">bu͡ollagɨna</ta>
            <ta e="T4" id="Seg_2197" s="T3">törüt-I-m</ta>
            <ta e="T5" id="Seg_2198" s="T4">beje-m</ta>
            <ta e="T6" id="Seg_2199" s="T5">törüt-I-m</ta>
            <ta e="T7" id="Seg_2200" s="T6">ki͡eŋ</ta>
            <ta e="T8" id="Seg_2201" s="T7">Jakuːtskaj-GA</ta>
            <ta e="T9" id="Seg_2202" s="T8">da</ta>
            <ta e="T10" id="Seg_2203" s="T9">baːr-LAr</ta>
            <ta e="T11" id="Seg_2204" s="T10">nuːčča-LAr</ta>
            <ta e="T12" id="Seg_2205" s="T11">da</ta>
            <ta e="T13" id="Seg_2206" s="T12">baːr-LAr</ta>
            <ta e="T14" id="Seg_2207" s="T13">dulgaːn-LAr</ta>
            <ta e="T15" id="Seg_2208" s="T14">da</ta>
            <ta e="T16" id="Seg_2209" s="T15">baːr-LAr</ta>
            <ta e="T17" id="Seg_2210" s="T16">bastakɨ-nAn</ta>
            <ta e="T18" id="Seg_2211" s="T17">bu͡o</ta>
            <ta e="T19" id="Seg_2212" s="T18">Jakuːtʼija-ttAn</ta>
            <ta e="T20" id="Seg_2213" s="T19">min</ta>
            <ta e="T21" id="Seg_2214" s="T20">ebe-m</ta>
            <ta e="T22" id="Seg_2215" s="T21">bu͡ollagɨna</ta>
            <ta e="T23" id="Seg_2216" s="T22">Katanga-ttAn</ta>
            <ta e="T24" id="Seg_2217" s="T23">ogonnʼor-GA</ta>
            <ta e="T25" id="Seg_2218" s="T24">tagɨs-I-BIT-tA</ta>
            <ta e="T26" id="Seg_2219" s="T25">iti</ta>
            <ta e="T27" id="Seg_2220" s="T26">ekspʼedʼicɨja-GA</ta>
            <ta e="T28" id="Seg_2221" s="T27">üleleː-A</ta>
            <ta e="T30" id="Seg_2222" s="T28">hɨrɨt-An</ta>
            <ta e="T35" id="Seg_2223" s="T34">Tʼimafʼejevna</ta>
            <ta e="T36" id="Seg_2224" s="T35">Darʼja</ta>
            <ta e="T37" id="Seg_2225" s="T36">di͡e-An</ta>
            <ta e="T38" id="Seg_2226" s="T37">onton</ta>
            <ta e="T39" id="Seg_2227" s="T38">bu͡ollagɨna</ta>
            <ta e="T40" id="Seg_2228" s="T39">bar-An</ta>
            <ta e="T41" id="Seg_2229" s="T40">ogonnʼor-GA</ta>
            <ta e="T42" id="Seg_2230" s="T41">tagɨs-TI-tA</ta>
            <ta e="T43" id="Seg_2231" s="T42">ogonnʼor-LArA</ta>
            <ta e="T44" id="Seg_2232" s="T43">üs</ta>
            <ta e="T45" id="Seg_2233" s="T44">kɨːs-nI</ta>
            <ta e="T46" id="Seg_2234" s="T45">töröː-BIT</ta>
            <ta e="T47" id="Seg_2235" s="T46">ol</ta>
            <ta e="T48" id="Seg_2236" s="T47">Jakuːtskaj-GA</ta>
            <ta e="T49" id="Seg_2237" s="T48">Jakuːtskaj-GA</ta>
            <ta e="T50" id="Seg_2238" s="T49">bar-An</ta>
            <ta e="T51" id="Seg_2239" s="T50">onton</ta>
            <ta e="T52" id="Seg_2240" s="T51">töttörü</ta>
            <ta e="T53" id="Seg_2241" s="T52">kös-An</ta>
            <ta e="T54" id="Seg_2242" s="T53">kel-BIT-LAr</ta>
            <ta e="T55" id="Seg_2243" s="T54">manna</ta>
            <ta e="T56" id="Seg_2244" s="T55">Popigaj-GA</ta>
            <ta e="T57" id="Seg_2245" s="T56">Popigaj-ttAn</ta>
            <ta e="T58" id="Seg_2246" s="T57">Katanga-GA</ta>
            <ta e="T59" id="Seg_2247" s="T58">da</ta>
            <ta e="T60" id="Seg_2248" s="T59">üleleː-A</ta>
            <ta e="T61" id="Seg_2249" s="T60">hɨrɨt-I-BIT-tA</ta>
            <ta e="T62" id="Seg_2250" s="T61">Kasʼistaj-GA</ta>
            <ta e="T63" id="Seg_2251" s="T62">da</ta>
            <ta e="T64" id="Seg_2252" s="T63">üleleː-A</ta>
            <ta e="T65" id="Seg_2253" s="T64">hɨrɨt-I-BIT-tA</ta>
            <ta e="T66" id="Seg_2254" s="T65">hir-GA</ta>
            <ta e="T67" id="Seg_2255" s="T66">barɨ-tI-GAr</ta>
            <ta e="T68" id="Seg_2256" s="T67">dʼe</ta>
            <ta e="T69" id="Seg_2257" s="T68">üleleː-A</ta>
            <ta e="T70" id="Seg_2258" s="T69">hɨrɨt-I-BIT</ta>
            <ta e="T71" id="Seg_2259" s="T70">hurak-tI-n</ta>
            <ta e="T72" id="Seg_2260" s="T71">ihit-TAK-BInA</ta>
            <ta e="T73" id="Seg_2261" s="T72">onton</ta>
            <ta e="T74" id="Seg_2262" s="T73">bu͡ollagɨna</ta>
            <ta e="T75" id="Seg_2263" s="T74">ɨlgɨn</ta>
            <ta e="T76" id="Seg_2264" s="T75">kɨːs-tI-n</ta>
            <ta e="T77" id="Seg_2265" s="T76">u͡ol-tA</ta>
            <ta e="T78" id="Seg_2266" s="T77">bu͡ol-A-BIn</ta>
            <ta e="T79" id="Seg_2267" s="T78">min</ta>
            <ta e="T80" id="Seg_2268" s="T79">beje-BIt</ta>
            <ta e="T81" id="Seg_2269" s="T80">agɨs-LAr-BIt</ta>
            <ta e="T82" id="Seg_2270" s="T81">törüt-BItI-GAr</ta>
            <ta e="T83" id="Seg_2271" s="T82">aŋar-BIt</ta>
            <ta e="T84" id="Seg_2272" s="T83">otto</ta>
            <ta e="T85" id="Seg_2273" s="T84">hu͡ok</ta>
            <ta e="T86" id="Seg_2274" s="T85">aŋar-BIt</ta>
            <ta e="T87" id="Seg_2275" s="T86">baːr</ta>
            <ta e="T88" id="Seg_2276" s="T87">dʼe</ta>
            <ta e="T89" id="Seg_2277" s="T88">iti</ta>
            <ta e="T91" id="Seg_2278" s="T89">kördük</ta>
            <ta e="T98" id="Seg_2279" s="T97">min</ta>
            <ta e="T99" id="Seg_2280" s="T98">bu͡ollagɨna</ta>
            <ta e="T100" id="Seg_2281" s="T99">Jakuːtʼija-GA</ta>
            <ta e="T101" id="Seg_2282" s="T100">ü͡ören-A</ta>
            <ta e="T102" id="Seg_2283" s="T101">hɨrɨt-BIT-I-m</ta>
            <ta e="T103" id="Seg_2284" s="T102">togus-u͡on</ta>
            <ta e="T104" id="Seg_2285" s="T103">dʼɨl-LAːK-LAr-GA</ta>
            <ta e="T105" id="Seg_2286" s="T104">togus-u͡on</ta>
            <ta e="T106" id="Seg_2287" s="T105">dʼɨl-LAːK-LAr-GA</ta>
            <ta e="T107" id="Seg_2288" s="T106">bu͡o</ta>
            <ta e="T108" id="Seg_2289" s="T107">manna</ta>
            <ta e="T109" id="Seg_2290" s="T108">ɨllaː-A</ta>
            <ta e="T110" id="Seg_2291" s="T109">hɨrɨt-BIT-I-m</ta>
            <ta e="T111" id="Seg_2292" s="T110">fʼestʼival-GA</ta>
            <ta e="T112" id="Seg_2293" s="T111">onno</ta>
            <ta e="T113" id="Seg_2294" s="T112">össü͡ö</ta>
            <ta e="T114" id="Seg_2295" s="T113">mi͡este</ta>
            <ta e="T115" id="Seg_2296" s="T114">du͡o</ta>
            <ta e="T116" id="Seg_2297" s="T115">tu͡ok</ta>
            <ta e="T117" id="Seg_2298" s="T116">du͡o</ta>
            <ta e="T118" id="Seg_2299" s="T117">baːr</ta>
            <ta e="T119" id="Seg_2300" s="T118">kördük</ta>
            <ta e="T120" id="Seg_2301" s="T119">e-TI-tA</ta>
            <ta e="T122" id="Seg_2302" s="T120">bu͡o</ta>
            <ta e="T127" id="Seg_2303" s="T124">eː</ta>
            <ta e="T129" id="Seg_2304" s="T127">Tajmɨr-GA</ta>
            <ta e="T130" id="Seg_2305" s="T129">ɨllaː-BIT-I-m</ta>
            <ta e="T131" id="Seg_2306" s="T130">onno</ta>
            <ta e="T132" id="Seg_2307" s="T131">küččügüj</ta>
            <ta e="T133" id="Seg_2308" s="T132">e-TI-m</ta>
            <ta e="T134" id="Seg_2309" s="T133">kas</ta>
            <ta e="T135" id="Seg_2310" s="T134">haːs-LAːK-BIn</ta>
            <ta e="T136" id="Seg_2311" s="T135">beje-m</ta>
            <ta e="T137" id="Seg_2312" s="T136">bil-BAT-BIn</ta>
            <ta e="T144" id="Seg_2313" s="T141">eː</ta>
            <ta e="T146" id="Seg_2314" s="T144">Dudʼinka-ttAn</ta>
            <ta e="T147" id="Seg_2315" s="T146">mantan</ta>
            <ta e="T148" id="Seg_2316" s="T147">ɨl-BIT-LArA</ta>
            <ta e="T149" id="Seg_2317" s="T148">iti</ta>
            <ta e="T150" id="Seg_2318" s="T149">tubatdʼelʼenʼija-ttAn</ta>
            <ta e="T151" id="Seg_2319" s="T150">ɨl-BIT-LArA</ta>
            <ta e="T161" id="Seg_2320" s="T159">küččügüj</ta>
            <ta e="T162" id="Seg_2321" s="T161">er-TAK-BInA</ta>
            <ta e="T163" id="Seg_2322" s="T162">bu͡ollagɨna</ta>
            <ta e="T164" id="Seg_2323" s="T163">beje-m</ta>
            <ta e="T165" id="Seg_2324" s="T164">dʼe</ta>
            <ta e="T166" id="Seg_2325" s="T165">kohuj-An-BIn</ta>
            <ta e="T167" id="Seg_2326" s="T166">ɨllaː-Ar</ta>
            <ta e="T168" id="Seg_2327" s="T167">e-TI-m</ta>
            <ta e="T169" id="Seg_2328" s="T168">ol</ta>
            <ta e="T170" id="Seg_2329" s="T169">barɨ-tA</ta>
            <ta e="T172" id="Seg_2330" s="T170">umun-I-LIN-I-BIT</ta>
            <ta e="T178" id="Seg_2331" s="T176">eː</ta>
            <ta e="T179" id="Seg_2332" s="T178">hubu</ta>
            <ta e="T182" id="Seg_2333" s="T181">haŋar-Ar</ta>
            <ta e="T183" id="Seg_2334" s="T182">haŋa-LAr-BI-n</ta>
            <ta e="T184" id="Seg_2335" s="T183">ɨllaː-Ar</ta>
            <ta e="T185" id="Seg_2336" s="T184">e-TI-m</ta>
            <ta e="T186" id="Seg_2337" s="T185">kolobur-tA</ta>
            <ta e="T187" id="Seg_2338" s="T186">kihi-nI</ta>
            <ta e="T188" id="Seg_2339" s="T187">kör-TAK-BInA</ta>
            <ta e="T189" id="Seg_2340" s="T188">kihi-nI</ta>
            <ta e="T190" id="Seg_2341" s="T189">kohuj-A-BIn</ta>
            <ta e="T191" id="Seg_2342" s="T190">tu͡ok-nI</ta>
            <ta e="T192" id="Seg_2343" s="T191">ere</ta>
            <ta e="T193" id="Seg_2344" s="T192">ɨt-nI</ta>
            <ta e="T194" id="Seg_2345" s="T193">kohuj-TAK-BInA</ta>
            <ta e="T195" id="Seg_2346" s="T194">ɨt-nI</ta>
            <ta e="T196" id="Seg_2347" s="T195">kohuj-A-BIn</ta>
            <ta e="T197" id="Seg_2348" s="T196">itinnik</ta>
            <ta e="T210" id="Seg_2349" s="T208">kim</ta>
            <ta e="T211" id="Seg_2350" s="T210">da</ta>
            <ta e="T212" id="Seg_2351" s="T211">ɨllaː-BAT</ta>
            <ta e="T213" id="Seg_2352" s="T212">e-TI-tA</ta>
            <ta e="T214" id="Seg_2353" s="T213">törüt-BI-ttAn</ta>
            <ta e="T215" id="Seg_2354" s="T214">no</ta>
            <ta e="T216" id="Seg_2355" s="T215">mozet</ta>
            <ta e="T217" id="Seg_2356" s="T216">iti</ta>
            <ta e="T218" id="Seg_2357" s="T217">mozet</ta>
            <ta e="T219" id="Seg_2358" s="T218">ojun-LAr</ta>
            <ta e="T220" id="Seg_2359" s="T219">emi͡e</ta>
            <ta e="T221" id="Seg_2360" s="T220">baːr-LArA</ta>
            <ta e="T222" id="Seg_2361" s="T221">bu͡ol-IAK.[tA]</ta>
            <ta e="T223" id="Seg_2362" s="T222">ol-LAr</ta>
            <ta e="T229" id="Seg_2363" s="T227">eː</ta>
            <ta e="T243" id="Seg_2364" s="T240">mm</ta>
            <ta e="T253" id="Seg_2365" s="T250">mm</ta>
            <ta e="T255" id="Seg_2366" s="T253">kataː-A-LIN-A-LAr</ta>
            <ta e="T256" id="Seg_2367" s="T255">nastaivaj-LAː-Ar-LAr</ta>
            <ta e="T257" id="Seg_2368" s="T256">üleleː-Ar</ta>
            <ta e="T258" id="Seg_2369" s="T257">e-TI-m</ta>
            <ta e="T259" id="Seg_2370" s="T258">ol</ta>
            <ta e="T260" id="Seg_2371" s="T259">Jakuːtʼija-GA</ta>
            <ta e="T261" id="Seg_2372" s="T260">tu͡ok-GA</ta>
            <ta e="T262" id="Seg_2373" s="T261">tʼexrabotnʼik-I-nAn</ta>
            <ta e="T263" id="Seg_2374" s="T262">ol</ta>
            <ta e="T264" id="Seg_2375" s="T263">üleleː-An-BIn</ta>
            <ta e="T265" id="Seg_2376" s="T264">dʼe</ta>
            <ta e="T266" id="Seg_2377" s="T265">beje-m</ta>
            <ta e="T267" id="Seg_2378" s="T266">tu͡ok-BA-r</ta>
            <ta e="T268" id="Seg_2379" s="T267">hɨrɨt-AːččI-BIn</ta>
            <ta e="T269" id="Seg_2380" s="T268">min</ta>
            <ta e="T270" id="Seg_2381" s="T269">kancert-LAr-GA</ta>
            <ta e="T271" id="Seg_2382" s="T270">tu͡ok-LAr-GA</ta>
            <ta e="T272" id="Seg_2383" s="T271">vaxtʼorša</ta>
            <ta e="T273" id="Seg_2384" s="T272">kördük</ta>
            <ta e="T274" id="Seg_2385" s="T273">e-TI-m</ta>
            <ta e="T275" id="Seg_2386" s="T274">beje-m</ta>
            <ta e="T276" id="Seg_2387" s="T275">bilʼet</ta>
            <ta e="T277" id="Seg_2388" s="T276">eŋin</ta>
            <ta e="T278" id="Seg_2389" s="T277">atɨːlaː-A-BIn</ta>
            <ta e="T279" id="Seg_2390" s="T278">itinnik-LAr-GA</ta>
            <ta e="T282" id="Seg_2391" s="T281">kuluːb-GA</ta>
            <ta e="T284" id="Seg_2392" s="T282">üleleː-BIT-I-m</ta>
            <ta e="T293" id="Seg_2393" s="T291">mm</ta>
            <ta e="T295" id="Seg_2394" s="T293">ol</ta>
            <ta e="T296" id="Seg_2395" s="T295">sɨ͡ana-GA</ta>
            <ta e="T297" id="Seg_2396" s="T296">tagɨs-IAK-BI-n</ta>
            <ta e="T298" id="Seg_2397" s="T297">anaːn</ta>
            <ta e="T299" id="Seg_2398" s="T298">ispisʼalno</ta>
            <ta e="T300" id="Seg_2399" s="T299">ɨl-BIT-LArA</ta>
            <ta e="T301" id="Seg_2400" s="T300">beje-BI-n</ta>
            <ta e="T302" id="Seg_2401" s="T301">bil-TAr-IAK-BI-n</ta>
            <ta e="T303" id="Seg_2402" s="T302">beje-m</ta>
            <ta e="T304" id="Seg_2403" s="T303">bu͡ollagɨna</ta>
            <ta e="T305" id="Seg_2404" s="T304">alta</ta>
            <ta e="T306" id="Seg_2405" s="T305">du͡o</ta>
            <ta e="T307" id="Seg_2406" s="T306">tü͡ört</ta>
            <ta e="T308" id="Seg_2407" s="T307">du͡o</ta>
            <ta e="T309" id="Seg_2408" s="T308">gramata-LAːK-BIn</ta>
            <ta e="T310" id="Seg_2409" s="T309">anɨ</ta>
            <ta e="T311" id="Seg_2410" s="T310">ol</ta>
            <ta e="T312" id="Seg_2411" s="T311">dek</ta>
            <ta e="T314" id="Seg_2412" s="T312">hɨt-Ar-LAr</ta>
            <ta e="T320" id="Seg_2413" s="T318">Jakuːtskaj-GA</ta>
            <ta e="T321" id="Seg_2414" s="T320">abrazavaːnʼija-ttAn</ta>
            <ta e="T322" id="Seg_2415" s="T321">da</ta>
            <ta e="T323" id="Seg_2416" s="T322">baːr</ta>
            <ta e="T324" id="Seg_2417" s="T323">e-TI-LArA</ta>
            <ta e="T325" id="Seg_2418" s="T324">baːr-LAr</ta>
            <ta e="T326" id="Seg_2419" s="T325">eni</ta>
            <ta e="T327" id="Seg_2420" s="T326">kajdi͡ek</ta>
            <ta e="T328" id="Seg_2421" s="T327">du͡o</ta>
            <ta e="T329" id="Seg_2422" s="T328">kör-TAK-GA</ta>
            <ta e="T330" id="Seg_2423" s="T329">onton</ta>
            <ta e="T331" id="Seg_2424" s="T330">tu͡ok-ttAn</ta>
            <ta e="T332" id="Seg_2425" s="T331">emi͡e</ta>
            <ta e="T333" id="Seg_2426" s="T332">kulʼtuːra-ttAn</ta>
            <ta e="T334" id="Seg_2427" s="T333">kas</ta>
            <ta e="T335" id="Seg_2428" s="T334">daːganɨ</ta>
            <ta e="T336" id="Seg_2429" s="T335">glava admʼinʼistrasʼija-ttAn</ta>
            <ta e="T337" id="Seg_2430" s="T336">emi͡e</ta>
            <ta e="T338" id="Seg_2431" s="T337">baːr</ta>
            <ta e="T339" id="Seg_2432" s="T338">bu͡ol-IAK-LAːK</ta>
            <ta e="T349" id="Seg_2433" s="T347">Alʼenʼoskaj</ta>
            <ta e="T350" id="Seg_2434" s="T349">uluːs</ta>
            <ta e="T359" id="Seg_2435" s="T358">Alʼenʼoskaj</ta>
            <ta e="T360" id="Seg_2436" s="T359">uluːs</ta>
            <ta e="T361" id="Seg_2437" s="T360">arɨː</ta>
            <ta e="T362" id="Seg_2438" s="T361">bu͡o</ta>
            <ta e="T363" id="Seg_2439" s="T362">min</ta>
            <ta e="T364" id="Seg_2440" s="T363">bu͡o</ta>
            <ta e="T365" id="Seg_2441" s="T364">oččogo</ta>
            <ta e="T366" id="Seg_2442" s="T365">onno</ta>
            <ta e="T367" id="Seg_2443" s="T366">Xarɨjalaːk-GA</ta>
            <ta e="T368" id="Seg_2444" s="T367">e-TI-m</ta>
            <ta e="T369" id="Seg_2445" s="T368">giniler</ta>
            <ta e="T370" id="Seg_2446" s="T369">bu͡ollagɨna</ta>
            <ta e="T371" id="Seg_2447" s="T370">iti</ta>
            <ta e="T372" id="Seg_2448" s="T371">bi͡erek-GA</ta>
            <ta e="T373" id="Seg_2449" s="T372">mannɨk</ta>
            <ta e="T374" id="Seg_2450" s="T373">onu͡or-onu͡or</ta>
            <ta e="T376" id="Seg_2451" s="T374">olor-Ar-LAr</ta>
            <ta e="T380" id="Seg_2452" s="T379">Ölöːn-I-ŋ</ta>
            <ta e="T381" id="Seg_2453" s="T380">tuspa</ta>
            <ta e="T382" id="Seg_2454" s="T381">Xarɨjalaːk-I-ŋ</ta>
            <ta e="T385" id="Seg_2455" s="T382">tuspa</ta>
            <ta e="T389" id="Seg_2456" s="T387">eː</ta>
            <ta e="T390" id="Seg_2457" s="T389">ikki</ta>
            <ta e="T391" id="Seg_2458" s="T390">tu͡ok</ta>
            <ta e="T409" id="Seg_2459" s="T407">dolu͡oj</ta>
            <ta e="T410" id="Seg_2460" s="T409">küččügüj-BI-ttAn</ta>
            <ta e="T411" id="Seg_2461" s="T410">di͡e-AːččI-LAr</ta>
            <ta e="T412" id="Seg_2462" s="T411">bu</ta>
            <ta e="T413" id="Seg_2463" s="T412">dʼe</ta>
            <ta e="T414" id="Seg_2464" s="T413">saːdik-GA</ta>
            <ta e="T415" id="Seg_2465" s="T414">hɨrɨt-Ar-BI-ttAn</ta>
            <ta e="T416" id="Seg_2466" s="T415">ɨllaː-Ar</ta>
            <ta e="T417" id="Seg_2467" s="T416">e-BIT-BIn</ta>
            <ta e="T430" id="Seg_2468" s="T429">bu</ta>
            <ta e="T431" id="Seg_2469" s="T430">nuːčča-LAr-nI</ta>
            <ta e="T432" id="Seg_2470" s="T431">kelin-GI</ta>
            <ta e="T433" id="Seg_2471" s="T432">ɨllaː-Ar</ta>
            <ta e="T434" id="Seg_2472" s="T433">bu͡ol-BIT-I-m</ta>
            <ta e="T435" id="Seg_2473" s="T434">onton</ta>
            <ta e="T436" id="Seg_2474" s="T435">bu</ta>
            <ta e="T437" id="Seg_2475" s="T436">anɨ</ta>
            <ta e="T438" id="Seg_2476" s="T437">haka-LAr-GA</ta>
            <ta e="T439" id="Seg_2477" s="T438">haka-LAr-nI</ta>
            <ta e="T440" id="Seg_2478" s="T439">ere</ta>
            <ta e="T441" id="Seg_2479" s="T440">ɨllaː-A-BIn</ta>
         </annotation>
         <annotation name="ge" tierref="ge-ZhNA">
            <ta e="T2" id="Seg_2480" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_2481" s="T2">though</ta>
            <ta e="T4" id="Seg_2482" s="T3">lineage-EP-1SG.[NOM]</ta>
            <ta e="T5" id="Seg_2483" s="T4">self-1SG.[NOM]</ta>
            <ta e="T6" id="Seg_2484" s="T5">lineage-EP-1SG.[NOM]</ta>
            <ta e="T7" id="Seg_2485" s="T6">broad.[NOM]</ta>
            <ta e="T8" id="Seg_2486" s="T7">Yakutsk-DAT/LOC</ta>
            <ta e="T9" id="Seg_2487" s="T8">and</ta>
            <ta e="T10" id="Seg_2488" s="T9">there.is-3PL</ta>
            <ta e="T11" id="Seg_2489" s="T10">Russian-PL.[NOM]</ta>
            <ta e="T12" id="Seg_2490" s="T11">and</ta>
            <ta e="T13" id="Seg_2491" s="T12">there.is-3PL</ta>
            <ta e="T14" id="Seg_2492" s="T13">Dolgan-PL.[NOM]</ta>
            <ta e="T15" id="Seg_2493" s="T14">and</ta>
            <ta e="T16" id="Seg_2494" s="T15">there.is-3PL</ta>
            <ta e="T17" id="Seg_2495" s="T16">front-INSTR</ta>
            <ta e="T18" id="Seg_2496" s="T17">EMPH</ta>
            <ta e="T19" id="Seg_2497" s="T18">Yakutia-ABL</ta>
            <ta e="T20" id="Seg_2498" s="T19">1SG.[NOM]</ta>
            <ta e="T21" id="Seg_2499" s="T20">grandmother-1SG.[NOM]</ta>
            <ta e="T22" id="Seg_2500" s="T21">though</ta>
            <ta e="T23" id="Seg_2501" s="T22">Khatanga-ABL</ta>
            <ta e="T24" id="Seg_2502" s="T23">old.man-DAT/LOC</ta>
            <ta e="T25" id="Seg_2503" s="T24">go.out-EP-PST2-3SG</ta>
            <ta e="T26" id="Seg_2504" s="T25">that.[NOM]</ta>
            <ta e="T27" id="Seg_2505" s="T26">expedition-DAT/LOC</ta>
            <ta e="T28" id="Seg_2506" s="T27">work-CVB.SIM</ta>
            <ta e="T30" id="Seg_2507" s="T28">go-CVB.SEQ</ta>
            <ta e="T35" id="Seg_2508" s="T34">Timofeevna</ta>
            <ta e="T36" id="Seg_2509" s="T35">Darya</ta>
            <ta e="T37" id="Seg_2510" s="T36">say-CVB.SEQ</ta>
            <ta e="T38" id="Seg_2511" s="T37">then</ta>
            <ta e="T39" id="Seg_2512" s="T38">though</ta>
            <ta e="T40" id="Seg_2513" s="T39">go-CVB.SEQ</ta>
            <ta e="T41" id="Seg_2514" s="T40">old.man-DAT/LOC</ta>
            <ta e="T42" id="Seg_2515" s="T41">go.out-PST1-3SG</ta>
            <ta e="T43" id="Seg_2516" s="T42">old.man-3PL.[NOM]</ta>
            <ta e="T44" id="Seg_2517" s="T43">three</ta>
            <ta e="T45" id="Seg_2518" s="T44">girl-ACC</ta>
            <ta e="T46" id="Seg_2519" s="T45">give.birth-PST2.[3SG]</ta>
            <ta e="T47" id="Seg_2520" s="T46">that</ta>
            <ta e="T48" id="Seg_2521" s="T47">Yakutsk-DAT/LOC</ta>
            <ta e="T49" id="Seg_2522" s="T48">Yakutsk-DAT/LOC</ta>
            <ta e="T50" id="Seg_2523" s="T49">go-CVB.SEQ</ta>
            <ta e="T51" id="Seg_2524" s="T50">then</ta>
            <ta e="T52" id="Seg_2525" s="T51">back</ta>
            <ta e="T53" id="Seg_2526" s="T52">nomadize-CVB.SEQ</ta>
            <ta e="T54" id="Seg_2527" s="T53">come-PST2-3PL</ta>
            <ta e="T55" id="Seg_2528" s="T54">hither</ta>
            <ta e="T56" id="Seg_2529" s="T55">Popigaj-DAT/LOC</ta>
            <ta e="T57" id="Seg_2530" s="T56">Popigaj-ABL</ta>
            <ta e="T58" id="Seg_2531" s="T57">Khatanga-DAT/LOC</ta>
            <ta e="T59" id="Seg_2532" s="T58">and</ta>
            <ta e="T60" id="Seg_2533" s="T59">work-CVB.SIM</ta>
            <ta e="T61" id="Seg_2534" s="T60">go-EP-PST2-3SG</ta>
            <ta e="T62" id="Seg_2535" s="T61">Kosistiy-DAT/LOC</ta>
            <ta e="T63" id="Seg_2536" s="T62">and</ta>
            <ta e="T64" id="Seg_2537" s="T63">work-CVB.SIM</ta>
            <ta e="T65" id="Seg_2538" s="T64">go-EP-PST2-3SG</ta>
            <ta e="T66" id="Seg_2539" s="T65">place-DAT/LOC</ta>
            <ta e="T67" id="Seg_2540" s="T66">every-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_2541" s="T67">well</ta>
            <ta e="T69" id="Seg_2542" s="T68">work-CVB.SIM</ta>
            <ta e="T70" id="Seg_2543" s="T69">go-EP-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_2544" s="T70">legend-3SG-ACC</ta>
            <ta e="T72" id="Seg_2545" s="T71">hear-TEMP-1SG</ta>
            <ta e="T73" id="Seg_2546" s="T72">then</ta>
            <ta e="T74" id="Seg_2547" s="T73">though</ta>
            <ta e="T75" id="Seg_2548" s="T74">little</ta>
            <ta e="T76" id="Seg_2549" s="T75">daughter-3SG-GEN</ta>
            <ta e="T77" id="Seg_2550" s="T76">son-3SG.[NOM]</ta>
            <ta e="T78" id="Seg_2551" s="T77">be-PRS-1SG</ta>
            <ta e="T79" id="Seg_2552" s="T78">1SG.[NOM]</ta>
            <ta e="T80" id="Seg_2553" s="T79">self-1PL.[NOM]</ta>
            <ta e="T81" id="Seg_2554" s="T80">eight-PL-1PL</ta>
            <ta e="T82" id="Seg_2555" s="T81">ancestor-1PL-DAT/LOC</ta>
            <ta e="T83" id="Seg_2556" s="T82">half-1PL.[NOM]</ta>
            <ta e="T84" id="Seg_2557" s="T83">EMPH</ta>
            <ta e="T85" id="Seg_2558" s="T84">NEG.EX</ta>
            <ta e="T86" id="Seg_2559" s="T85">half-1PL.[NOM]</ta>
            <ta e="T87" id="Seg_2560" s="T86">there.is</ta>
            <ta e="T88" id="Seg_2561" s="T87">well</ta>
            <ta e="T89" id="Seg_2562" s="T88">that.[NOM]</ta>
            <ta e="T91" id="Seg_2563" s="T89">similar</ta>
            <ta e="T98" id="Seg_2564" s="T97">1SG.[NOM]</ta>
            <ta e="T99" id="Seg_2565" s="T98">though</ta>
            <ta e="T100" id="Seg_2566" s="T99">Yakutia-DAT/LOC</ta>
            <ta e="T101" id="Seg_2567" s="T100">learn-CVB.SIM</ta>
            <ta e="T102" id="Seg_2568" s="T101">go-PST2-EP-1SG</ta>
            <ta e="T103" id="Seg_2569" s="T102">nine-ten</ta>
            <ta e="T104" id="Seg_2570" s="T103">year-PROPR-PL-DAT/LOC</ta>
            <ta e="T105" id="Seg_2571" s="T104">nine-ten</ta>
            <ta e="T106" id="Seg_2572" s="T105">year-PROPR-PL-DAT/LOC</ta>
            <ta e="T107" id="Seg_2573" s="T106">EMPH</ta>
            <ta e="T108" id="Seg_2574" s="T107">here</ta>
            <ta e="T109" id="Seg_2575" s="T108">sing-CVB.SIM</ta>
            <ta e="T110" id="Seg_2576" s="T109">go-PST2-EP-1SG</ta>
            <ta e="T111" id="Seg_2577" s="T110">festival-DAT/LOC</ta>
            <ta e="T112" id="Seg_2578" s="T111">there</ta>
            <ta e="T113" id="Seg_2579" s="T112">still</ta>
            <ta e="T114" id="Seg_2580" s="T113">place.[NOM]</ta>
            <ta e="T115" id="Seg_2581" s="T114">Q</ta>
            <ta e="T116" id="Seg_2582" s="T115">what.[NOM]</ta>
            <ta e="T117" id="Seg_2583" s="T116">Q</ta>
            <ta e="T118" id="Seg_2584" s="T117">there.is</ta>
            <ta e="T119" id="Seg_2585" s="T118">similar</ta>
            <ta e="T120" id="Seg_2586" s="T119">be-PST1-3SG</ta>
            <ta e="T122" id="Seg_2587" s="T120">EMPH</ta>
            <ta e="T127" id="Seg_2588" s="T124">AFFIRM</ta>
            <ta e="T129" id="Seg_2589" s="T127">Taymyr-DAT/LOC</ta>
            <ta e="T130" id="Seg_2590" s="T129">sing-PST2-EP-1SG</ta>
            <ta e="T131" id="Seg_2591" s="T130">there</ta>
            <ta e="T132" id="Seg_2592" s="T131">small.[NOM]</ta>
            <ta e="T133" id="Seg_2593" s="T132">be-PST1-1SG</ta>
            <ta e="T134" id="Seg_2594" s="T133">how.much</ta>
            <ta e="T135" id="Seg_2595" s="T134">age-PROPR-1SG</ta>
            <ta e="T136" id="Seg_2596" s="T135">self-1SG.[NOM]</ta>
            <ta e="T137" id="Seg_2597" s="T136">know-NEG-1SG</ta>
            <ta e="T144" id="Seg_2598" s="T141">AFFIRM</ta>
            <ta e="T146" id="Seg_2599" s="T144">Dudinka-ABL</ta>
            <ta e="T147" id="Seg_2600" s="T146">from.here</ta>
            <ta e="T148" id="Seg_2601" s="T147">take-PST2-3PL</ta>
            <ta e="T149" id="Seg_2602" s="T148">that.[NOM]</ta>
            <ta e="T150" id="Seg_2603" s="T149">tuberculosis.section-ABL</ta>
            <ta e="T151" id="Seg_2604" s="T150">take-PST2-3PL</ta>
            <ta e="T161" id="Seg_2605" s="T159">small.[NOM]</ta>
            <ta e="T162" id="Seg_2606" s="T161">be-TEMP-1SG</ta>
            <ta e="T163" id="Seg_2607" s="T162">though</ta>
            <ta e="T164" id="Seg_2608" s="T163">self-1SG.[NOM]</ta>
            <ta e="T165" id="Seg_2609" s="T164">well</ta>
            <ta e="T166" id="Seg_2610" s="T165">compose-CVB.SEQ-1SG</ta>
            <ta e="T167" id="Seg_2611" s="T166">sing-PTCP.PRS</ta>
            <ta e="T168" id="Seg_2612" s="T167">be-PST1-1SG</ta>
            <ta e="T169" id="Seg_2613" s="T168">that</ta>
            <ta e="T170" id="Seg_2614" s="T169">every-3SG.[NOM]</ta>
            <ta e="T172" id="Seg_2615" s="T170">forget-EP-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T178" id="Seg_2616" s="T176">AFFIRM</ta>
            <ta e="T179" id="Seg_2617" s="T178">this.EMPH</ta>
            <ta e="T182" id="Seg_2618" s="T181">speak-PTCP.PRS</ta>
            <ta e="T183" id="Seg_2619" s="T182">word-PL-1SG-ACC</ta>
            <ta e="T184" id="Seg_2620" s="T183">sing-PTCP.PRS</ta>
            <ta e="T185" id="Seg_2621" s="T184">be-PST1-1SG</ta>
            <ta e="T186" id="Seg_2622" s="T185">example-3SG.[NOM]</ta>
            <ta e="T187" id="Seg_2623" s="T186">human.being-ACC</ta>
            <ta e="T188" id="Seg_2624" s="T187">see-TEMP-1SG</ta>
            <ta e="T189" id="Seg_2625" s="T188">human.being-ACC</ta>
            <ta e="T190" id="Seg_2626" s="T189">sing.of-PRS-1SG</ta>
            <ta e="T191" id="Seg_2627" s="T190">what-ACC</ta>
            <ta e="T192" id="Seg_2628" s="T191">INDEF</ta>
            <ta e="T193" id="Seg_2629" s="T192">dog-ACC</ta>
            <ta e="T194" id="Seg_2630" s="T193">sing.of-TEMP-1SG</ta>
            <ta e="T195" id="Seg_2631" s="T194">dog-ACC</ta>
            <ta e="T196" id="Seg_2632" s="T195">sing.of-PRS-1SG</ta>
            <ta e="T197" id="Seg_2633" s="T196">such</ta>
            <ta e="T210" id="Seg_2634" s="T208">who.[NOM]</ta>
            <ta e="T211" id="Seg_2635" s="T210">NEG</ta>
            <ta e="T212" id="Seg_2636" s="T211">sing-NEG.PTCP</ta>
            <ta e="T213" id="Seg_2637" s="T212">be-PST1-3SG</ta>
            <ta e="T214" id="Seg_2638" s="T213">lineage-1SG-ABL</ta>
            <ta e="T215" id="Seg_2639" s="T214">but</ta>
            <ta e="T216" id="Seg_2640" s="T215">maybe</ta>
            <ta e="T217" id="Seg_2641" s="T216">that.[NOM]</ta>
            <ta e="T218" id="Seg_2642" s="T217">maybe</ta>
            <ta e="T219" id="Seg_2643" s="T218">shaman-PL.[NOM]</ta>
            <ta e="T220" id="Seg_2644" s="T219">also</ta>
            <ta e="T221" id="Seg_2645" s="T220">there.is-3PL</ta>
            <ta e="T222" id="Seg_2646" s="T221">be-FUT.[3SG]</ta>
            <ta e="T223" id="Seg_2647" s="T222">that-PL.[NOM]</ta>
            <ta e="T229" id="Seg_2648" s="T227">AFFIRM</ta>
            <ta e="T243" id="Seg_2649" s="T240">mm</ta>
            <ta e="T253" id="Seg_2650" s="T250">mm</ta>
            <ta e="T255" id="Seg_2651" s="T253">fix-EP-PASS/REFL-PRS-3PL</ta>
            <ta e="T256" id="Seg_2652" s="T255">persist-VBZ-PRS-3PL</ta>
            <ta e="T257" id="Seg_2653" s="T256">work-PTCP.PRS</ta>
            <ta e="T258" id="Seg_2654" s="T257">be-PST1-1SG</ta>
            <ta e="T259" id="Seg_2655" s="T258">that</ta>
            <ta e="T260" id="Seg_2656" s="T259">Yakutia-DAT/LOC</ta>
            <ta e="T261" id="Seg_2657" s="T260">what-DAT/LOC</ta>
            <ta e="T262" id="Seg_2658" s="T261">technician-EP-INSTR</ta>
            <ta e="T263" id="Seg_2659" s="T262">that</ta>
            <ta e="T264" id="Seg_2660" s="T263">work-CVB.SEQ-1SG</ta>
            <ta e="T265" id="Seg_2661" s="T264">well</ta>
            <ta e="T266" id="Seg_2662" s="T265">self-1SG.[NOM]</ta>
            <ta e="T267" id="Seg_2663" s="T266">what-1SG-DAT/LOC</ta>
            <ta e="T268" id="Seg_2664" s="T267">go-HAB-1SG</ta>
            <ta e="T269" id="Seg_2665" s="T268">1SG.[NOM]</ta>
            <ta e="T270" id="Seg_2666" s="T269">concert-PL-DAT/LOC</ta>
            <ta e="T271" id="Seg_2667" s="T270">what-PL-DAT/LOC</ta>
            <ta e="T272" id="Seg_2668" s="T271">female.watchman.[NOM]</ta>
            <ta e="T273" id="Seg_2669" s="T272">similar</ta>
            <ta e="T274" id="Seg_2670" s="T273">be-PST1-1SG</ta>
            <ta e="T275" id="Seg_2671" s="T274">self-1SG.[NOM]</ta>
            <ta e="T276" id="Seg_2672" s="T275">ticket.[NOM]</ta>
            <ta e="T277" id="Seg_2673" s="T276">different.[NOM]</ta>
            <ta e="T278" id="Seg_2674" s="T277">sell-PRS-1SG</ta>
            <ta e="T279" id="Seg_2675" s="T278">such-PL-DAT/LOC</ta>
            <ta e="T282" id="Seg_2676" s="T281">club-DAT/LOC</ta>
            <ta e="T284" id="Seg_2677" s="T282">work-PST2-EP-1SG</ta>
            <ta e="T293" id="Seg_2678" s="T291">mm</ta>
            <ta e="T295" id="Seg_2679" s="T293">that</ta>
            <ta e="T296" id="Seg_2680" s="T295">stage-DAT/LOC</ta>
            <ta e="T297" id="Seg_2681" s="T296">go.out-PTCP.FUT-1SG-ACC</ta>
            <ta e="T298" id="Seg_2682" s="T297">especially</ta>
            <ta e="T299" id="Seg_2683" s="T298">especially</ta>
            <ta e="T300" id="Seg_2684" s="T299">take-PST2-3PL</ta>
            <ta e="T301" id="Seg_2685" s="T300">self-1SG-ACC</ta>
            <ta e="T302" id="Seg_2686" s="T301">know-CAUS-PTCP.FUT-1SG-ACC</ta>
            <ta e="T303" id="Seg_2687" s="T302">self-1SG.[NOM]</ta>
            <ta e="T304" id="Seg_2688" s="T303">though</ta>
            <ta e="T305" id="Seg_2689" s="T304">six</ta>
            <ta e="T306" id="Seg_2690" s="T305">Q</ta>
            <ta e="T307" id="Seg_2691" s="T306">four</ta>
            <ta e="T308" id="Seg_2692" s="T307">Q</ta>
            <ta e="T309" id="Seg_2693" s="T308">certificate-PROPR-1SG</ta>
            <ta e="T310" id="Seg_2694" s="T309">now</ta>
            <ta e="T311" id="Seg_2695" s="T310">that.[NOM]</ta>
            <ta e="T312" id="Seg_2696" s="T311">to</ta>
            <ta e="T314" id="Seg_2697" s="T312">lie-PRS-3PL</ta>
            <ta e="T320" id="Seg_2698" s="T318">Yakutsk-DAT/LOC</ta>
            <ta e="T321" id="Seg_2699" s="T320">education-ABL</ta>
            <ta e="T322" id="Seg_2700" s="T321">and</ta>
            <ta e="T323" id="Seg_2701" s="T322">there.is</ta>
            <ta e="T324" id="Seg_2702" s="T323">be-PST1-3PL</ta>
            <ta e="T325" id="Seg_2703" s="T324">there.is-3PL</ta>
            <ta e="T326" id="Seg_2704" s="T325">apparently</ta>
            <ta e="T327" id="Seg_2705" s="T326">whereto</ta>
            <ta e="T328" id="Seg_2706" s="T327">Q</ta>
            <ta e="T329" id="Seg_2707" s="T328">see-PTCP.COND-DAT/LOC</ta>
            <ta e="T330" id="Seg_2708" s="T329">then</ta>
            <ta e="T331" id="Seg_2709" s="T330">what-ABL</ta>
            <ta e="T332" id="Seg_2710" s="T331">also</ta>
            <ta e="T333" id="Seg_2711" s="T332">culture-ABL</ta>
            <ta e="T334" id="Seg_2712" s="T333">how.much</ta>
            <ta e="T335" id="Seg_2713" s="T334">EMPH</ta>
            <ta e="T336" id="Seg_2714" s="T335">head.of.administration-ABL</ta>
            <ta e="T337" id="Seg_2715" s="T336">also</ta>
            <ta e="T338" id="Seg_2716" s="T337">there.is</ta>
            <ta e="T339" id="Seg_2717" s="T338">be-FUT-NEC.[3SG]</ta>
            <ta e="T349" id="Seg_2718" s="T347">Olenyokskiy</ta>
            <ta e="T350" id="Seg_2719" s="T349">ulus</ta>
            <ta e="T359" id="Seg_2720" s="T358">Olenyokskiy</ta>
            <ta e="T360" id="Seg_2721" s="T359">ulus</ta>
            <ta e="T361" id="Seg_2722" s="T360">island.[NOM]</ta>
            <ta e="T362" id="Seg_2723" s="T361">EMPH</ta>
            <ta e="T363" id="Seg_2724" s="T362">1SG.[NOM]</ta>
            <ta e="T364" id="Seg_2725" s="T363">EMPH</ta>
            <ta e="T365" id="Seg_2726" s="T364">then</ta>
            <ta e="T366" id="Seg_2727" s="T365">there</ta>
            <ta e="T367" id="Seg_2728" s="T366">Khariyalaak-DAT/LOC</ta>
            <ta e="T368" id="Seg_2729" s="T367">be-PST1-1SG</ta>
            <ta e="T369" id="Seg_2730" s="T368">3PL.[NOM]</ta>
            <ta e="T370" id="Seg_2731" s="T369">though</ta>
            <ta e="T371" id="Seg_2732" s="T370">that.[NOM]</ta>
            <ta e="T372" id="Seg_2733" s="T371">shore-DAT/LOC</ta>
            <ta e="T373" id="Seg_2734" s="T372">such</ta>
            <ta e="T374" id="Seg_2735" s="T373">on.the.other.shore-on.the.other.shore</ta>
            <ta e="T376" id="Seg_2736" s="T374">live-PRS-3PL</ta>
            <ta e="T380" id="Seg_2737" s="T379">Olenyok-EP-2SG.[NOM]</ta>
            <ta e="T381" id="Seg_2738" s="T380">individually</ta>
            <ta e="T382" id="Seg_2739" s="T381">Khariyalaak-EP-2SG.[NOM]</ta>
            <ta e="T385" id="Seg_2740" s="T382">individually</ta>
            <ta e="T389" id="Seg_2741" s="T387">AFFIRM</ta>
            <ta e="T390" id="Seg_2742" s="T389">two</ta>
            <ta e="T391" id="Seg_2743" s="T390">what.[NOM]</ta>
            <ta e="T409" id="Seg_2744" s="T407">completely</ta>
            <ta e="T410" id="Seg_2745" s="T409">small-1SG-ABL</ta>
            <ta e="T411" id="Seg_2746" s="T410">say-HAB-3PL</ta>
            <ta e="T412" id="Seg_2747" s="T411">this</ta>
            <ta e="T413" id="Seg_2748" s="T412">well</ta>
            <ta e="T414" id="Seg_2749" s="T413">kindergarten-DAT/LOC</ta>
            <ta e="T415" id="Seg_2750" s="T414">go-PTCP.PRS-1SG-ABL</ta>
            <ta e="T416" id="Seg_2751" s="T415">sing-PTCP.PRS</ta>
            <ta e="T417" id="Seg_2752" s="T416">be-PST2-1SG</ta>
            <ta e="T430" id="Seg_2753" s="T429">this</ta>
            <ta e="T431" id="Seg_2754" s="T430">Russian-PL-ACC</ta>
            <ta e="T432" id="Seg_2755" s="T431">back-ADJZ</ta>
            <ta e="T433" id="Seg_2756" s="T432">sing-PTCP.PRS</ta>
            <ta e="T434" id="Seg_2757" s="T433">become-PST2-EP-1SG</ta>
            <ta e="T435" id="Seg_2758" s="T434">then</ta>
            <ta e="T436" id="Seg_2759" s="T435">this</ta>
            <ta e="T437" id="Seg_2760" s="T436">now</ta>
            <ta e="T438" id="Seg_2761" s="T437">Dolgan-PL-DAT/LOC</ta>
            <ta e="T439" id="Seg_2762" s="T438">Dolgan-PL-ACC</ta>
            <ta e="T440" id="Seg_2763" s="T439">just</ta>
            <ta e="T441" id="Seg_2764" s="T440">sing-PRS-1SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg-ZhNA">
            <ta e="T2" id="Seg_2765" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_2766" s="T2">aber</ta>
            <ta e="T4" id="Seg_2767" s="T3">Geschlecht-EP-1SG.[NOM]</ta>
            <ta e="T5" id="Seg_2768" s="T4">selbst-1SG.[NOM]</ta>
            <ta e="T6" id="Seg_2769" s="T5">Geschlecht-EP-1SG.[NOM]</ta>
            <ta e="T7" id="Seg_2770" s="T6">breit.[NOM]</ta>
            <ta e="T8" id="Seg_2771" s="T7">Jakutsk-DAT/LOC</ta>
            <ta e="T9" id="Seg_2772" s="T8">und</ta>
            <ta e="T10" id="Seg_2773" s="T9">es.gibt-3PL</ta>
            <ta e="T11" id="Seg_2774" s="T10">Russe-PL.[NOM]</ta>
            <ta e="T12" id="Seg_2775" s="T11">und</ta>
            <ta e="T13" id="Seg_2776" s="T12">es.gibt-3PL</ta>
            <ta e="T14" id="Seg_2777" s="T13">Dolgane-PL.[NOM]</ta>
            <ta e="T15" id="Seg_2778" s="T14">und</ta>
            <ta e="T16" id="Seg_2779" s="T15">es.gibt-3PL</ta>
            <ta e="T17" id="Seg_2780" s="T16">vorderster-INSTR</ta>
            <ta e="T18" id="Seg_2781" s="T17">EMPH</ta>
            <ta e="T19" id="Seg_2782" s="T18">Jakutien-ABL</ta>
            <ta e="T20" id="Seg_2783" s="T19">1SG.[NOM]</ta>
            <ta e="T21" id="Seg_2784" s="T20">Großmutter-1SG.[NOM]</ta>
            <ta e="T22" id="Seg_2785" s="T21">aber</ta>
            <ta e="T23" id="Seg_2786" s="T22">Chatanga-ABL</ta>
            <ta e="T24" id="Seg_2787" s="T23">alter.Mann-DAT/LOC</ta>
            <ta e="T25" id="Seg_2788" s="T24">hinausgehen-EP-PST2-3SG</ta>
            <ta e="T26" id="Seg_2789" s="T25">dieses.[NOM]</ta>
            <ta e="T27" id="Seg_2790" s="T26">Expedition-DAT/LOC</ta>
            <ta e="T28" id="Seg_2791" s="T27">arbeiten-CVB.SIM</ta>
            <ta e="T30" id="Seg_2792" s="T28">gehen-CVB.SEQ</ta>
            <ta e="T35" id="Seg_2793" s="T34">Timofeevna</ta>
            <ta e="T36" id="Seg_2794" s="T35">Darja</ta>
            <ta e="T37" id="Seg_2795" s="T36">sagen-CVB.SEQ</ta>
            <ta e="T38" id="Seg_2796" s="T37">dann</ta>
            <ta e="T39" id="Seg_2797" s="T38">aber</ta>
            <ta e="T40" id="Seg_2798" s="T39">gehen-CVB.SEQ</ta>
            <ta e="T41" id="Seg_2799" s="T40">alter.Mann-DAT/LOC</ta>
            <ta e="T42" id="Seg_2800" s="T41">hinausgehen-PST1-3SG</ta>
            <ta e="T43" id="Seg_2801" s="T42">alter.Mann-3PL.[NOM]</ta>
            <ta e="T44" id="Seg_2802" s="T43">drei</ta>
            <ta e="T45" id="Seg_2803" s="T44">Mädchen-ACC</ta>
            <ta e="T46" id="Seg_2804" s="T45">gebären-PST2.[3SG]</ta>
            <ta e="T47" id="Seg_2805" s="T46">jenes</ta>
            <ta e="T48" id="Seg_2806" s="T47">Jakutsk-DAT/LOC</ta>
            <ta e="T49" id="Seg_2807" s="T48">Jakutsk-DAT/LOC</ta>
            <ta e="T50" id="Seg_2808" s="T49">gehen-CVB.SEQ</ta>
            <ta e="T51" id="Seg_2809" s="T50">dann</ta>
            <ta e="T52" id="Seg_2810" s="T51">zurück</ta>
            <ta e="T53" id="Seg_2811" s="T52">nomadisieren-CVB.SEQ</ta>
            <ta e="T54" id="Seg_2812" s="T53">kommen-PST2-3PL</ta>
            <ta e="T55" id="Seg_2813" s="T54">hierher</ta>
            <ta e="T56" id="Seg_2814" s="T55">Popigaj-DAT/LOC</ta>
            <ta e="T57" id="Seg_2815" s="T56">Popigaj-ABL</ta>
            <ta e="T58" id="Seg_2816" s="T57">Chatanga-DAT/LOC</ta>
            <ta e="T59" id="Seg_2817" s="T58">und</ta>
            <ta e="T60" id="Seg_2818" s="T59">arbeiten-CVB.SIM</ta>
            <ta e="T61" id="Seg_2819" s="T60">gehen-EP-PST2-3SG</ta>
            <ta e="T62" id="Seg_2820" s="T61">Kosistyj-DAT/LOC</ta>
            <ta e="T63" id="Seg_2821" s="T62">und</ta>
            <ta e="T64" id="Seg_2822" s="T63">arbeiten-CVB.SIM</ta>
            <ta e="T65" id="Seg_2823" s="T64">gehen-EP-PST2-3SG</ta>
            <ta e="T66" id="Seg_2824" s="T65">Ort-DAT/LOC</ta>
            <ta e="T67" id="Seg_2825" s="T66">jeder-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_2826" s="T67">doch</ta>
            <ta e="T69" id="Seg_2827" s="T68">arbeiten-CVB.SIM</ta>
            <ta e="T70" id="Seg_2828" s="T69">gehen-EP-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_2829" s="T70">Legende-3SG-ACC</ta>
            <ta e="T72" id="Seg_2830" s="T71">hören-TEMP-1SG</ta>
            <ta e="T73" id="Seg_2831" s="T72">dann</ta>
            <ta e="T74" id="Seg_2832" s="T73">aber</ta>
            <ta e="T75" id="Seg_2833" s="T74">klein</ta>
            <ta e="T76" id="Seg_2834" s="T75">Tochter-3SG-GEN</ta>
            <ta e="T77" id="Seg_2835" s="T76">Sohn-3SG.[NOM]</ta>
            <ta e="T78" id="Seg_2836" s="T77">sein-PRS-1SG</ta>
            <ta e="T79" id="Seg_2837" s="T78">1SG.[NOM]</ta>
            <ta e="T80" id="Seg_2838" s="T79">selbst-1PL.[NOM]</ta>
            <ta e="T81" id="Seg_2839" s="T80">acht-PL-1PL</ta>
            <ta e="T82" id="Seg_2840" s="T81">Vorfahr-1PL-DAT/LOC</ta>
            <ta e="T83" id="Seg_2841" s="T82">Hälfte-1PL.[NOM]</ta>
            <ta e="T84" id="Seg_2842" s="T83">EMPH</ta>
            <ta e="T85" id="Seg_2843" s="T84">NEG.EX</ta>
            <ta e="T86" id="Seg_2844" s="T85">Hälfte-1PL.[NOM]</ta>
            <ta e="T87" id="Seg_2845" s="T86">es.gibt</ta>
            <ta e="T88" id="Seg_2846" s="T87">doch</ta>
            <ta e="T89" id="Seg_2847" s="T88">dieses.[NOM]</ta>
            <ta e="T91" id="Seg_2848" s="T89">ähnlich</ta>
            <ta e="T98" id="Seg_2849" s="T97">1SG.[NOM]</ta>
            <ta e="T99" id="Seg_2850" s="T98">aber</ta>
            <ta e="T100" id="Seg_2851" s="T99">Jakutien-DAT/LOC</ta>
            <ta e="T101" id="Seg_2852" s="T100">lernen-CVB.SIM</ta>
            <ta e="T102" id="Seg_2853" s="T101">gehen-PST2-EP-1SG</ta>
            <ta e="T103" id="Seg_2854" s="T102">neun-zehn</ta>
            <ta e="T104" id="Seg_2855" s="T103">Jahr-PROPR-PL-DAT/LOC</ta>
            <ta e="T105" id="Seg_2856" s="T104">neun-zehn</ta>
            <ta e="T106" id="Seg_2857" s="T105">Jahr-PROPR-PL-DAT/LOC</ta>
            <ta e="T107" id="Seg_2858" s="T106">EMPH</ta>
            <ta e="T108" id="Seg_2859" s="T107">hier</ta>
            <ta e="T109" id="Seg_2860" s="T108">singen-CVB.SIM</ta>
            <ta e="T110" id="Seg_2861" s="T109">gehen-PST2-EP-1SG</ta>
            <ta e="T111" id="Seg_2862" s="T110">Festival-DAT/LOC</ta>
            <ta e="T112" id="Seg_2863" s="T111">dort</ta>
            <ta e="T113" id="Seg_2864" s="T112">noch</ta>
            <ta e="T114" id="Seg_2865" s="T113">Ort.[NOM]</ta>
            <ta e="T115" id="Seg_2866" s="T114">Q</ta>
            <ta e="T116" id="Seg_2867" s="T115">was.[NOM]</ta>
            <ta e="T117" id="Seg_2868" s="T116">Q</ta>
            <ta e="T118" id="Seg_2869" s="T117">es.gibt</ta>
            <ta e="T119" id="Seg_2870" s="T118">ähnlich</ta>
            <ta e="T120" id="Seg_2871" s="T119">sein-PST1-3SG</ta>
            <ta e="T122" id="Seg_2872" s="T120">EMPH</ta>
            <ta e="T127" id="Seg_2873" s="T124">AFFIRM</ta>
            <ta e="T129" id="Seg_2874" s="T127">Taimyr-DAT/LOC</ta>
            <ta e="T130" id="Seg_2875" s="T129">singen-PST2-EP-1SG</ta>
            <ta e="T131" id="Seg_2876" s="T130">dort</ta>
            <ta e="T132" id="Seg_2877" s="T131">klein.[NOM]</ta>
            <ta e="T133" id="Seg_2878" s="T132">sein-PST1-1SG</ta>
            <ta e="T134" id="Seg_2879" s="T133">wie.viel</ta>
            <ta e="T135" id="Seg_2880" s="T134">Alter-PROPR-1SG</ta>
            <ta e="T136" id="Seg_2881" s="T135">selbst-1SG.[NOM]</ta>
            <ta e="T137" id="Seg_2882" s="T136">wissen-NEG-1SG</ta>
            <ta e="T144" id="Seg_2883" s="T141">AFFIRM</ta>
            <ta e="T146" id="Seg_2884" s="T144">Dudinka-ABL</ta>
            <ta e="T147" id="Seg_2885" s="T146">von.hier</ta>
            <ta e="T148" id="Seg_2886" s="T147">nehmen-PST2-3PL</ta>
            <ta e="T149" id="Seg_2887" s="T148">dieses.[NOM]</ta>
            <ta e="T150" id="Seg_2888" s="T149">Tuberkuloseabteilung-ABL</ta>
            <ta e="T151" id="Seg_2889" s="T150">nehmen-PST2-3PL</ta>
            <ta e="T161" id="Seg_2890" s="T159">klein.[NOM]</ta>
            <ta e="T162" id="Seg_2891" s="T161">sein-TEMP-1SG</ta>
            <ta e="T163" id="Seg_2892" s="T162">aber</ta>
            <ta e="T164" id="Seg_2893" s="T163">selbst-1SG.[NOM]</ta>
            <ta e="T165" id="Seg_2894" s="T164">doch</ta>
            <ta e="T166" id="Seg_2895" s="T165">verfassen-CVB.SEQ-1SG</ta>
            <ta e="T167" id="Seg_2896" s="T166">singen-PTCP.PRS</ta>
            <ta e="T168" id="Seg_2897" s="T167">sein-PST1-1SG</ta>
            <ta e="T169" id="Seg_2898" s="T168">jenes</ta>
            <ta e="T170" id="Seg_2899" s="T169">jeder-3SG.[NOM]</ta>
            <ta e="T172" id="Seg_2900" s="T170">vergessen-EP-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T178" id="Seg_2901" s="T176">AFFIRM</ta>
            <ta e="T179" id="Seg_2902" s="T178">dieses.EMPH</ta>
            <ta e="T182" id="Seg_2903" s="T181">sprechen-PTCP.PRS</ta>
            <ta e="T183" id="Seg_2904" s="T182">Wort-PL-1SG-ACC</ta>
            <ta e="T184" id="Seg_2905" s="T183">singen-PTCP.PRS</ta>
            <ta e="T185" id="Seg_2906" s="T184">sein-PST1-1SG</ta>
            <ta e="T186" id="Seg_2907" s="T185">Beispiel-3SG.[NOM]</ta>
            <ta e="T187" id="Seg_2908" s="T186">Mensch-ACC</ta>
            <ta e="T188" id="Seg_2909" s="T187">sehen-TEMP-1SG</ta>
            <ta e="T189" id="Seg_2910" s="T188">Mensch-ACC</ta>
            <ta e="T190" id="Seg_2911" s="T189">besingen-PRS-1SG</ta>
            <ta e="T191" id="Seg_2912" s="T190">was-ACC</ta>
            <ta e="T192" id="Seg_2913" s="T191">INDEF</ta>
            <ta e="T193" id="Seg_2914" s="T192">Hund-ACC</ta>
            <ta e="T194" id="Seg_2915" s="T193">besingen-TEMP-1SG</ta>
            <ta e="T195" id="Seg_2916" s="T194">Hund-ACC</ta>
            <ta e="T196" id="Seg_2917" s="T195">besingen-PRS-1SG</ta>
            <ta e="T197" id="Seg_2918" s="T196">solch</ta>
            <ta e="T210" id="Seg_2919" s="T208">wer.[NOM]</ta>
            <ta e="T211" id="Seg_2920" s="T210">NEG</ta>
            <ta e="T212" id="Seg_2921" s="T211">singen-NEG.PTCP</ta>
            <ta e="T213" id="Seg_2922" s="T212">sein-PST1-3SG</ta>
            <ta e="T214" id="Seg_2923" s="T213">Geschlecht-1SG-ABL</ta>
            <ta e="T215" id="Seg_2924" s="T214">aber</ta>
            <ta e="T216" id="Seg_2925" s="T215">vielleicht</ta>
            <ta e="T217" id="Seg_2926" s="T216">dieses.[NOM]</ta>
            <ta e="T218" id="Seg_2927" s="T217">vielleicht</ta>
            <ta e="T219" id="Seg_2928" s="T218">Schamane-PL.[NOM]</ta>
            <ta e="T220" id="Seg_2929" s="T219">auch</ta>
            <ta e="T221" id="Seg_2930" s="T220">es.gibt-3PL</ta>
            <ta e="T222" id="Seg_2931" s="T221">sein-FUT.[3SG]</ta>
            <ta e="T223" id="Seg_2932" s="T222">jenes-PL.[NOM]</ta>
            <ta e="T229" id="Seg_2933" s="T227">AFFIRM</ta>
            <ta e="T243" id="Seg_2934" s="T240">mm</ta>
            <ta e="T253" id="Seg_2935" s="T250">mm</ta>
            <ta e="T255" id="Seg_2936" s="T253">befestigen-EP-PASS/REFL-PRS-3PL</ta>
            <ta e="T256" id="Seg_2937" s="T255">fortdauern-VBZ-PRS-3PL</ta>
            <ta e="T257" id="Seg_2938" s="T256">arbeiten-PTCP.PRS</ta>
            <ta e="T258" id="Seg_2939" s="T257">sein-PST1-1SG</ta>
            <ta e="T259" id="Seg_2940" s="T258">jenes</ta>
            <ta e="T260" id="Seg_2941" s="T259">Jakutien-DAT/LOC</ta>
            <ta e="T261" id="Seg_2942" s="T260">was-DAT/LOC</ta>
            <ta e="T262" id="Seg_2943" s="T261">Techniker-EP-INSTR</ta>
            <ta e="T263" id="Seg_2944" s="T262">jenes</ta>
            <ta e="T264" id="Seg_2945" s="T263">arbeiten-CVB.SEQ-1SG</ta>
            <ta e="T265" id="Seg_2946" s="T264">doch</ta>
            <ta e="T266" id="Seg_2947" s="T265">selbst-1SG.[NOM]</ta>
            <ta e="T267" id="Seg_2948" s="T266">was-1SG-DAT/LOC</ta>
            <ta e="T268" id="Seg_2949" s="T267">gehen-HAB-1SG</ta>
            <ta e="T269" id="Seg_2950" s="T268">1SG.[NOM]</ta>
            <ta e="T270" id="Seg_2951" s="T269">Konzert-PL-DAT/LOC</ta>
            <ta e="T271" id="Seg_2952" s="T270">was-PL-DAT/LOC</ta>
            <ta e="T272" id="Seg_2953" s="T271">Wachfrau.[NOM]</ta>
            <ta e="T273" id="Seg_2954" s="T272">ähnlich</ta>
            <ta e="T274" id="Seg_2955" s="T273">sein-PST1-1SG</ta>
            <ta e="T275" id="Seg_2956" s="T274">selbst-1SG.[NOM]</ta>
            <ta e="T276" id="Seg_2957" s="T275">Ticket.[NOM]</ta>
            <ta e="T277" id="Seg_2958" s="T276">unterschiedlich.[NOM]</ta>
            <ta e="T278" id="Seg_2959" s="T277">verkaufen-PRS-1SG</ta>
            <ta e="T279" id="Seg_2960" s="T278">solch-PL-DAT/LOC</ta>
            <ta e="T282" id="Seg_2961" s="T281">Klub-DAT/LOC</ta>
            <ta e="T284" id="Seg_2962" s="T282">arbeiten-PST2-EP-1SG</ta>
            <ta e="T293" id="Seg_2963" s="T291">mm</ta>
            <ta e="T295" id="Seg_2964" s="T293">jenes</ta>
            <ta e="T296" id="Seg_2965" s="T295">Bühne-DAT/LOC</ta>
            <ta e="T297" id="Seg_2966" s="T296">hinausgehen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T298" id="Seg_2967" s="T297">besonders</ta>
            <ta e="T299" id="Seg_2968" s="T298">besonders</ta>
            <ta e="T300" id="Seg_2969" s="T299">nehmen-PST2-3PL</ta>
            <ta e="T301" id="Seg_2970" s="T300">selbst-1SG-ACC</ta>
            <ta e="T302" id="Seg_2971" s="T301">wissen-CAUS-PTCP.FUT-1SG-ACC</ta>
            <ta e="T303" id="Seg_2972" s="T302">selbst-1SG.[NOM]</ta>
            <ta e="T304" id="Seg_2973" s="T303">aber</ta>
            <ta e="T305" id="Seg_2974" s="T304">sechs</ta>
            <ta e="T306" id="Seg_2975" s="T305">Q</ta>
            <ta e="T307" id="Seg_2976" s="T306">vier</ta>
            <ta e="T308" id="Seg_2977" s="T307">Q</ta>
            <ta e="T309" id="Seg_2978" s="T308">Urkunde-PROPR-1SG</ta>
            <ta e="T310" id="Seg_2979" s="T309">jetzt</ta>
            <ta e="T311" id="Seg_2980" s="T310">jenes.[NOM]</ta>
            <ta e="T312" id="Seg_2981" s="T311">zu</ta>
            <ta e="T314" id="Seg_2982" s="T312">liegen-PRS-3PL</ta>
            <ta e="T320" id="Seg_2983" s="T318">Jakutsk-DAT/LOC</ta>
            <ta e="T321" id="Seg_2984" s="T320">Ausbildung-ABL</ta>
            <ta e="T322" id="Seg_2985" s="T321">und</ta>
            <ta e="T323" id="Seg_2986" s="T322">es.gibt</ta>
            <ta e="T324" id="Seg_2987" s="T323">sein-PST1-3PL</ta>
            <ta e="T325" id="Seg_2988" s="T324">es.gibt-3PL</ta>
            <ta e="T326" id="Seg_2989" s="T325">offenbar</ta>
            <ta e="T327" id="Seg_2990" s="T326">wohin</ta>
            <ta e="T328" id="Seg_2991" s="T327">Q</ta>
            <ta e="T329" id="Seg_2992" s="T328">sehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T330" id="Seg_2993" s="T329">dann</ta>
            <ta e="T331" id="Seg_2994" s="T330">was-ABL</ta>
            <ta e="T332" id="Seg_2995" s="T331">auch</ta>
            <ta e="T333" id="Seg_2996" s="T332">Kultur-ABL</ta>
            <ta e="T334" id="Seg_2997" s="T333">wie.viel</ta>
            <ta e="T335" id="Seg_2998" s="T334">EMPH</ta>
            <ta e="T336" id="Seg_2999" s="T335">Verwaltungschef-ABL</ta>
            <ta e="T337" id="Seg_3000" s="T336">auch</ta>
            <ta e="T338" id="Seg_3001" s="T337">es.gibt</ta>
            <ta e="T339" id="Seg_3002" s="T338">sein-FUT-NEC.[3SG]</ta>
            <ta e="T349" id="Seg_3003" s="T347">Olenjokskij</ta>
            <ta e="T350" id="Seg_3004" s="T349">Ulus</ta>
            <ta e="T359" id="Seg_3005" s="T358">Olenjokskij</ta>
            <ta e="T360" id="Seg_3006" s="T359">Ulus</ta>
            <ta e="T361" id="Seg_3007" s="T360">Insel.[NOM]</ta>
            <ta e="T362" id="Seg_3008" s="T361">EMPH</ta>
            <ta e="T363" id="Seg_3009" s="T362">1SG.[NOM]</ta>
            <ta e="T364" id="Seg_3010" s="T363">EMPH</ta>
            <ta e="T365" id="Seg_3011" s="T364">dann</ta>
            <ta e="T366" id="Seg_3012" s="T365">dort</ta>
            <ta e="T367" id="Seg_3013" s="T366">Charijalaak-DAT/LOC</ta>
            <ta e="T368" id="Seg_3014" s="T367">sein-PST1-1SG</ta>
            <ta e="T369" id="Seg_3015" s="T368">3PL.[NOM]</ta>
            <ta e="T370" id="Seg_3016" s="T369">aber</ta>
            <ta e="T371" id="Seg_3017" s="T370">dieses.[NOM]</ta>
            <ta e="T372" id="Seg_3018" s="T371">Ufer-DAT/LOC</ta>
            <ta e="T373" id="Seg_3019" s="T372">solch</ta>
            <ta e="T374" id="Seg_3020" s="T373">am.anderen.Ufer-am.anderen.Ufer</ta>
            <ta e="T376" id="Seg_3021" s="T374">leben-PRS-3PL</ta>
            <ta e="T380" id="Seg_3022" s="T379">Olenjok-EP-2SG.[NOM]</ta>
            <ta e="T381" id="Seg_3023" s="T380">einzeln</ta>
            <ta e="T382" id="Seg_3024" s="T381">Charijalaak-EP-2SG.[NOM]</ta>
            <ta e="T385" id="Seg_3025" s="T382">einzeln</ta>
            <ta e="T389" id="Seg_3026" s="T387">AFFIRM</ta>
            <ta e="T390" id="Seg_3027" s="T389">zwei</ta>
            <ta e="T391" id="Seg_3028" s="T390">was.[NOM]</ta>
            <ta e="T409" id="Seg_3029" s="T407">ganz.und.gar</ta>
            <ta e="T410" id="Seg_3030" s="T409">klein-1SG-ABL</ta>
            <ta e="T411" id="Seg_3031" s="T410">sagen-HAB-3PL</ta>
            <ta e="T412" id="Seg_3032" s="T411">dieses</ta>
            <ta e="T413" id="Seg_3033" s="T412">doch</ta>
            <ta e="T414" id="Seg_3034" s="T413">Kindergarten-DAT/LOC</ta>
            <ta e="T415" id="Seg_3035" s="T414">gehen-PTCP.PRS-1SG-ABL</ta>
            <ta e="T416" id="Seg_3036" s="T415">singen-PTCP.PRS</ta>
            <ta e="T417" id="Seg_3037" s="T416">sein-PST2-1SG</ta>
            <ta e="T430" id="Seg_3038" s="T429">dieses</ta>
            <ta e="T431" id="Seg_3039" s="T430">russisch-PL-ACC</ta>
            <ta e="T432" id="Seg_3040" s="T431">Hinterteil-ADJZ</ta>
            <ta e="T433" id="Seg_3041" s="T432">singen-PTCP.PRS</ta>
            <ta e="T434" id="Seg_3042" s="T433">werden-PST2-EP-1SG</ta>
            <ta e="T435" id="Seg_3043" s="T434">dann</ta>
            <ta e="T436" id="Seg_3044" s="T435">dieses</ta>
            <ta e="T437" id="Seg_3045" s="T436">jetzt</ta>
            <ta e="T438" id="Seg_3046" s="T437">dolganisch-PL-DAT/LOC</ta>
            <ta e="T439" id="Seg_3047" s="T438">dolganisch-PL-ACC</ta>
            <ta e="T440" id="Seg_3048" s="T439">nur</ta>
            <ta e="T441" id="Seg_3049" s="T440">singen-PRS-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr-ZhNA">
            <ta e="T2" id="Seg_3050" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_3051" s="T2">однако</ta>
            <ta e="T4" id="Seg_3052" s="T3">род-EP-1SG.[NOM]</ta>
            <ta e="T5" id="Seg_3053" s="T4">сам-1SG.[NOM]</ta>
            <ta e="T6" id="Seg_3054" s="T5">род-EP-1SG.[NOM]</ta>
            <ta e="T7" id="Seg_3055" s="T6">широкий.[NOM]</ta>
            <ta e="T8" id="Seg_3056" s="T7">Якутск-DAT/LOC</ta>
            <ta e="T9" id="Seg_3057" s="T8">да</ta>
            <ta e="T10" id="Seg_3058" s="T9">есть-3PL</ta>
            <ta e="T11" id="Seg_3059" s="T10">русский-PL.[NOM]</ta>
            <ta e="T12" id="Seg_3060" s="T11">да</ta>
            <ta e="T13" id="Seg_3061" s="T12">есть-3PL</ta>
            <ta e="T14" id="Seg_3062" s="T13">долган-PL.[NOM]</ta>
            <ta e="T15" id="Seg_3063" s="T14">да</ta>
            <ta e="T16" id="Seg_3064" s="T15">есть-3PL</ta>
            <ta e="T17" id="Seg_3065" s="T16">передовой-INSTR</ta>
            <ta e="T18" id="Seg_3066" s="T17">EMPH</ta>
            <ta e="T19" id="Seg_3067" s="T18">Якутия-ABL</ta>
            <ta e="T20" id="Seg_3068" s="T19">1SG.[NOM]</ta>
            <ta e="T21" id="Seg_3069" s="T20">бабушка-1SG.[NOM]</ta>
            <ta e="T22" id="Seg_3070" s="T21">однако</ta>
            <ta e="T23" id="Seg_3071" s="T22">Хатанга-ABL</ta>
            <ta e="T24" id="Seg_3072" s="T23">старик-DAT/LOC</ta>
            <ta e="T25" id="Seg_3073" s="T24">выйти-EP-PST2-3SG</ta>
            <ta e="T26" id="Seg_3074" s="T25">тот.[NOM]</ta>
            <ta e="T27" id="Seg_3075" s="T26">экспедиция-DAT/LOC</ta>
            <ta e="T28" id="Seg_3076" s="T27">работать-CVB.SIM</ta>
            <ta e="T30" id="Seg_3077" s="T28">идти-CVB.SEQ</ta>
            <ta e="T35" id="Seg_3078" s="T34">Тимофеевна</ta>
            <ta e="T36" id="Seg_3079" s="T35">Дарья</ta>
            <ta e="T37" id="Seg_3080" s="T36">говорить-CVB.SEQ</ta>
            <ta e="T38" id="Seg_3081" s="T37">потом</ta>
            <ta e="T39" id="Seg_3082" s="T38">однако</ta>
            <ta e="T40" id="Seg_3083" s="T39">идти-CVB.SEQ</ta>
            <ta e="T41" id="Seg_3084" s="T40">старик-DAT/LOC</ta>
            <ta e="T42" id="Seg_3085" s="T41">выйти-PST1-3SG</ta>
            <ta e="T43" id="Seg_3086" s="T42">старик-3PL.[NOM]</ta>
            <ta e="T44" id="Seg_3087" s="T43">три</ta>
            <ta e="T45" id="Seg_3088" s="T44">девушка-ACC</ta>
            <ta e="T46" id="Seg_3089" s="T45">родить-PST2.[3SG]</ta>
            <ta e="T47" id="Seg_3090" s="T46">тот</ta>
            <ta e="T48" id="Seg_3091" s="T47">Якутск-DAT/LOC</ta>
            <ta e="T49" id="Seg_3092" s="T48">Якутск-DAT/LOC</ta>
            <ta e="T50" id="Seg_3093" s="T49">идти-CVB.SEQ</ta>
            <ta e="T51" id="Seg_3094" s="T50">потом</ta>
            <ta e="T52" id="Seg_3095" s="T51">назад</ta>
            <ta e="T53" id="Seg_3096" s="T52">кочевать-CVB.SEQ</ta>
            <ta e="T54" id="Seg_3097" s="T53">приходить-PST2-3PL</ta>
            <ta e="T55" id="Seg_3098" s="T54">сюда</ta>
            <ta e="T56" id="Seg_3099" s="T55">Попигай-DAT/LOC</ta>
            <ta e="T57" id="Seg_3100" s="T56">Попигай-ABL</ta>
            <ta e="T58" id="Seg_3101" s="T57">Хатанга-DAT/LOC</ta>
            <ta e="T59" id="Seg_3102" s="T58">да</ta>
            <ta e="T60" id="Seg_3103" s="T59">работать-CVB.SIM</ta>
            <ta e="T61" id="Seg_3104" s="T60">идти-EP-PST2-3SG</ta>
            <ta e="T62" id="Seg_3105" s="T61">Косистый-DAT/LOC</ta>
            <ta e="T63" id="Seg_3106" s="T62">да</ta>
            <ta e="T64" id="Seg_3107" s="T63">работать-CVB.SIM</ta>
            <ta e="T65" id="Seg_3108" s="T64">идти-EP-PST2-3SG</ta>
            <ta e="T66" id="Seg_3109" s="T65">место-DAT/LOC</ta>
            <ta e="T67" id="Seg_3110" s="T66">каждый-3SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_3111" s="T67">вот</ta>
            <ta e="T69" id="Seg_3112" s="T68">работать-CVB.SIM</ta>
            <ta e="T70" id="Seg_3113" s="T69">идти-EP-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_3114" s="T70">легенда-3SG-ACC</ta>
            <ta e="T72" id="Seg_3115" s="T71">слышать-TEMP-1SG</ta>
            <ta e="T73" id="Seg_3116" s="T72">потом</ta>
            <ta e="T74" id="Seg_3117" s="T73">однако</ta>
            <ta e="T75" id="Seg_3118" s="T74">маленький</ta>
            <ta e="T76" id="Seg_3119" s="T75">дочь-3SG-GEN</ta>
            <ta e="T77" id="Seg_3120" s="T76">сын-3SG.[NOM]</ta>
            <ta e="T78" id="Seg_3121" s="T77">быть-PRS-1SG</ta>
            <ta e="T79" id="Seg_3122" s="T78">1SG.[NOM]</ta>
            <ta e="T80" id="Seg_3123" s="T79">сам-1PL.[NOM]</ta>
            <ta e="T81" id="Seg_3124" s="T80">восемь-PL-1PL</ta>
            <ta e="T82" id="Seg_3125" s="T81">предок-1PL-DAT/LOC</ta>
            <ta e="T83" id="Seg_3126" s="T82">половина-1PL.[NOM]</ta>
            <ta e="T84" id="Seg_3127" s="T83">EMPH</ta>
            <ta e="T85" id="Seg_3128" s="T84">NEG.EX</ta>
            <ta e="T86" id="Seg_3129" s="T85">половина-1PL.[NOM]</ta>
            <ta e="T87" id="Seg_3130" s="T86">есть</ta>
            <ta e="T88" id="Seg_3131" s="T87">вот</ta>
            <ta e="T89" id="Seg_3132" s="T88">тот.[NOM]</ta>
            <ta e="T91" id="Seg_3133" s="T89">подобно</ta>
            <ta e="T98" id="Seg_3134" s="T97">1SG.[NOM]</ta>
            <ta e="T99" id="Seg_3135" s="T98">однако</ta>
            <ta e="T100" id="Seg_3136" s="T99">Якутия-DAT/LOC</ta>
            <ta e="T101" id="Seg_3137" s="T100">учиться-CVB.SIM</ta>
            <ta e="T102" id="Seg_3138" s="T101">идти-PST2-EP-1SG</ta>
            <ta e="T103" id="Seg_3139" s="T102">девять-десять</ta>
            <ta e="T104" id="Seg_3140" s="T103">год-PROPR-PL-DAT/LOC</ta>
            <ta e="T105" id="Seg_3141" s="T104">девять-десять</ta>
            <ta e="T106" id="Seg_3142" s="T105">год-PROPR-PL-DAT/LOC</ta>
            <ta e="T107" id="Seg_3143" s="T106">EMPH</ta>
            <ta e="T108" id="Seg_3144" s="T107">здесь</ta>
            <ta e="T109" id="Seg_3145" s="T108">петь-CVB.SIM</ta>
            <ta e="T110" id="Seg_3146" s="T109">идти-PST2-EP-1SG</ta>
            <ta e="T111" id="Seg_3147" s="T110">фестиваль-DAT/LOC</ta>
            <ta e="T112" id="Seg_3148" s="T111">там</ta>
            <ta e="T113" id="Seg_3149" s="T112">еще</ta>
            <ta e="T114" id="Seg_3150" s="T113">место.[NOM]</ta>
            <ta e="T115" id="Seg_3151" s="T114">Q</ta>
            <ta e="T116" id="Seg_3152" s="T115">что.[NOM]</ta>
            <ta e="T117" id="Seg_3153" s="T116">Q</ta>
            <ta e="T118" id="Seg_3154" s="T117">есть</ta>
            <ta e="T119" id="Seg_3155" s="T118">подобно</ta>
            <ta e="T120" id="Seg_3156" s="T119">быть-PST1-3SG</ta>
            <ta e="T122" id="Seg_3157" s="T120">EMPH</ta>
            <ta e="T127" id="Seg_3158" s="T124">AFFIRM</ta>
            <ta e="T129" id="Seg_3159" s="T127">Таймыр-DAT/LOC</ta>
            <ta e="T130" id="Seg_3160" s="T129">петь-PST2-EP-1SG</ta>
            <ta e="T131" id="Seg_3161" s="T130">там</ta>
            <ta e="T132" id="Seg_3162" s="T131">маленький.[NOM]</ta>
            <ta e="T133" id="Seg_3163" s="T132">быть-PST1-1SG</ta>
            <ta e="T134" id="Seg_3164" s="T133">сколько</ta>
            <ta e="T135" id="Seg_3165" s="T134">возраст-PROPR-1SG</ta>
            <ta e="T136" id="Seg_3166" s="T135">сам-1SG.[NOM]</ta>
            <ta e="T137" id="Seg_3167" s="T136">знать-NEG-1SG</ta>
            <ta e="T144" id="Seg_3168" s="T141">AFFIRM</ta>
            <ta e="T146" id="Seg_3169" s="T144">Дудинка-ABL</ta>
            <ta e="T147" id="Seg_3170" s="T146">отсюда</ta>
            <ta e="T148" id="Seg_3171" s="T147">взять-PST2-3PL</ta>
            <ta e="T149" id="Seg_3172" s="T148">тот.[NOM]</ta>
            <ta e="T150" id="Seg_3173" s="T149">туботделения-ABL</ta>
            <ta e="T151" id="Seg_3174" s="T150">взять-PST2-3PL</ta>
            <ta e="T161" id="Seg_3175" s="T159">маленький.[NOM]</ta>
            <ta e="T162" id="Seg_3176" s="T161">быть-TEMP-1SG</ta>
            <ta e="T163" id="Seg_3177" s="T162">однако</ta>
            <ta e="T164" id="Seg_3178" s="T163">сам-1SG.[NOM]</ta>
            <ta e="T165" id="Seg_3179" s="T164">вот</ta>
            <ta e="T166" id="Seg_3180" s="T165">сложить-CVB.SEQ-1SG</ta>
            <ta e="T167" id="Seg_3181" s="T166">петь-PTCP.PRS</ta>
            <ta e="T168" id="Seg_3182" s="T167">быть-PST1-1SG</ta>
            <ta e="T169" id="Seg_3183" s="T168">тот</ta>
            <ta e="T170" id="Seg_3184" s="T169">каждый-3SG.[NOM]</ta>
            <ta e="T172" id="Seg_3185" s="T170">забывать-EP-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T178" id="Seg_3186" s="T176">AFFIRM</ta>
            <ta e="T179" id="Seg_3187" s="T178">этот.EMPH</ta>
            <ta e="T182" id="Seg_3188" s="T181">говорить-PTCP.PRS</ta>
            <ta e="T183" id="Seg_3189" s="T182">слово-PL-1SG-ACC</ta>
            <ta e="T184" id="Seg_3190" s="T183">петь-PTCP.PRS</ta>
            <ta e="T185" id="Seg_3191" s="T184">быть-PST1-1SG</ta>
            <ta e="T186" id="Seg_3192" s="T185">пример-3SG.[NOM]</ta>
            <ta e="T187" id="Seg_3193" s="T186">человек-ACC</ta>
            <ta e="T188" id="Seg_3194" s="T187">видеть-TEMP-1SG</ta>
            <ta e="T189" id="Seg_3195" s="T188">человек-ACC</ta>
            <ta e="T190" id="Seg_3196" s="T189">воспевать-PRS-1SG</ta>
            <ta e="T191" id="Seg_3197" s="T190">что-ACC</ta>
            <ta e="T192" id="Seg_3198" s="T191">INDEF</ta>
            <ta e="T193" id="Seg_3199" s="T192">собака-ACC</ta>
            <ta e="T194" id="Seg_3200" s="T193">воспевать-TEMP-1SG</ta>
            <ta e="T195" id="Seg_3201" s="T194">собака-ACC</ta>
            <ta e="T196" id="Seg_3202" s="T195">воспевать-PRS-1SG</ta>
            <ta e="T197" id="Seg_3203" s="T196">такой</ta>
            <ta e="T210" id="Seg_3204" s="T208">кто.[NOM]</ta>
            <ta e="T211" id="Seg_3205" s="T210">NEG</ta>
            <ta e="T212" id="Seg_3206" s="T211">петь-NEG.PTCP</ta>
            <ta e="T213" id="Seg_3207" s="T212">быть-PST1-3SG</ta>
            <ta e="T214" id="Seg_3208" s="T213">род-1SG-ABL</ta>
            <ta e="T215" id="Seg_3209" s="T214">но</ta>
            <ta e="T216" id="Seg_3210" s="T215">может</ta>
            <ta e="T217" id="Seg_3211" s="T216">тот.[NOM]</ta>
            <ta e="T218" id="Seg_3212" s="T217">может</ta>
            <ta e="T219" id="Seg_3213" s="T218">шаман-PL.[NOM]</ta>
            <ta e="T220" id="Seg_3214" s="T219">тоже</ta>
            <ta e="T221" id="Seg_3215" s="T220">есть-3PL</ta>
            <ta e="T222" id="Seg_3216" s="T221">быть-FUT.[3SG]</ta>
            <ta e="T223" id="Seg_3217" s="T222">тот-PL.[NOM]</ta>
            <ta e="T229" id="Seg_3218" s="T227">AFFIRM</ta>
            <ta e="T243" id="Seg_3219" s="T240">мм</ta>
            <ta e="T253" id="Seg_3220" s="T250">мм</ta>
            <ta e="T255" id="Seg_3221" s="T253">закреплять-EP-PASS/REFL-PRS-3PL</ta>
            <ta e="T256" id="Seg_3222" s="T255">настаивать-VBZ-PRS-3PL</ta>
            <ta e="T257" id="Seg_3223" s="T256">работать-PTCP.PRS</ta>
            <ta e="T258" id="Seg_3224" s="T257">быть-PST1-1SG</ta>
            <ta e="T259" id="Seg_3225" s="T258">тот</ta>
            <ta e="T260" id="Seg_3226" s="T259">Якутия-DAT/LOC</ta>
            <ta e="T261" id="Seg_3227" s="T260">что-DAT/LOC</ta>
            <ta e="T262" id="Seg_3228" s="T261">техработник-EP-INSTR</ta>
            <ta e="T263" id="Seg_3229" s="T262">тот</ta>
            <ta e="T264" id="Seg_3230" s="T263">работать-CVB.SEQ-1SG</ta>
            <ta e="T265" id="Seg_3231" s="T264">вот</ta>
            <ta e="T266" id="Seg_3232" s="T265">сам-1SG.[NOM]</ta>
            <ta e="T267" id="Seg_3233" s="T266">что-1SG-DAT/LOC</ta>
            <ta e="T268" id="Seg_3234" s="T267">идти-HAB-1SG</ta>
            <ta e="T269" id="Seg_3235" s="T268">1SG.[NOM]</ta>
            <ta e="T270" id="Seg_3236" s="T269">концерт-PL-DAT/LOC</ta>
            <ta e="T271" id="Seg_3237" s="T270">что-PL-DAT/LOC</ta>
            <ta e="T272" id="Seg_3238" s="T271">вахтерша.[NOM]</ta>
            <ta e="T273" id="Seg_3239" s="T272">подобно</ta>
            <ta e="T274" id="Seg_3240" s="T273">быть-PST1-1SG</ta>
            <ta e="T275" id="Seg_3241" s="T274">сам-1SG.[NOM]</ta>
            <ta e="T276" id="Seg_3242" s="T275">билет.[NOM]</ta>
            <ta e="T277" id="Seg_3243" s="T276">разный.[NOM]</ta>
            <ta e="T278" id="Seg_3244" s="T277">продавать-PRS-1SG</ta>
            <ta e="T279" id="Seg_3245" s="T278">такой-PL-DAT/LOC</ta>
            <ta e="T282" id="Seg_3246" s="T281">клуб-DAT/LOC</ta>
            <ta e="T284" id="Seg_3247" s="T282">работать-PST2-EP-1SG</ta>
            <ta e="T293" id="Seg_3248" s="T291">мм</ta>
            <ta e="T295" id="Seg_3249" s="T293">тот</ta>
            <ta e="T296" id="Seg_3250" s="T295">сцена-DAT/LOC</ta>
            <ta e="T297" id="Seg_3251" s="T296">выйти-PTCP.FUT-1SG-ACC</ta>
            <ta e="T298" id="Seg_3252" s="T297">специально</ta>
            <ta e="T299" id="Seg_3253" s="T298">специально</ta>
            <ta e="T300" id="Seg_3254" s="T299">взять-PST2-3PL</ta>
            <ta e="T301" id="Seg_3255" s="T300">сам-1SG-ACC</ta>
            <ta e="T302" id="Seg_3256" s="T301">знать-CAUS-PTCP.FUT-1SG-ACC</ta>
            <ta e="T303" id="Seg_3257" s="T302">сам-1SG.[NOM]</ta>
            <ta e="T304" id="Seg_3258" s="T303">однако</ta>
            <ta e="T305" id="Seg_3259" s="T304">шесть</ta>
            <ta e="T306" id="Seg_3260" s="T305">Q</ta>
            <ta e="T307" id="Seg_3261" s="T306">четыре</ta>
            <ta e="T308" id="Seg_3262" s="T307">Q</ta>
            <ta e="T309" id="Seg_3263" s="T308">грамота-PROPR-1SG</ta>
            <ta e="T310" id="Seg_3264" s="T309">теперь</ta>
            <ta e="T311" id="Seg_3265" s="T310">тот.[NOM]</ta>
            <ta e="T312" id="Seg_3266" s="T311">к</ta>
            <ta e="T314" id="Seg_3267" s="T312">лежать-PRS-3PL</ta>
            <ta e="T320" id="Seg_3268" s="T318">Якутск-DAT/LOC</ta>
            <ta e="T321" id="Seg_3269" s="T320">образование-ABL</ta>
            <ta e="T322" id="Seg_3270" s="T321">да</ta>
            <ta e="T323" id="Seg_3271" s="T322">есть</ta>
            <ta e="T324" id="Seg_3272" s="T323">быть-PST1-3PL</ta>
            <ta e="T325" id="Seg_3273" s="T324">есть-3PL</ta>
            <ta e="T326" id="Seg_3274" s="T325">очевидно</ta>
            <ta e="T327" id="Seg_3275" s="T326">куда</ta>
            <ta e="T328" id="Seg_3276" s="T327">Q</ta>
            <ta e="T329" id="Seg_3277" s="T328">видеть-PTCP.COND-DAT/LOC</ta>
            <ta e="T330" id="Seg_3278" s="T329">потом</ta>
            <ta e="T331" id="Seg_3279" s="T330">что-ABL</ta>
            <ta e="T332" id="Seg_3280" s="T331">тоже</ta>
            <ta e="T333" id="Seg_3281" s="T332">культура-ABL</ta>
            <ta e="T334" id="Seg_3282" s="T333">сколько</ta>
            <ta e="T335" id="Seg_3283" s="T334">EMPH</ta>
            <ta e="T336" id="Seg_3284" s="T335">глава.администрации-ABL</ta>
            <ta e="T337" id="Seg_3285" s="T336">тоже</ta>
            <ta e="T338" id="Seg_3286" s="T337">есть</ta>
            <ta e="T339" id="Seg_3287" s="T338">быть-FUT-NEC.[3SG]</ta>
            <ta e="T349" id="Seg_3288" s="T347">Оленекский</ta>
            <ta e="T350" id="Seg_3289" s="T349">улус</ta>
            <ta e="T359" id="Seg_3290" s="T358">Оленекский</ta>
            <ta e="T360" id="Seg_3291" s="T359">улус</ta>
            <ta e="T361" id="Seg_3292" s="T360">остров.[NOM]</ta>
            <ta e="T362" id="Seg_3293" s="T361">EMPH</ta>
            <ta e="T363" id="Seg_3294" s="T362">1SG.[NOM]</ta>
            <ta e="T364" id="Seg_3295" s="T363">EMPH</ta>
            <ta e="T365" id="Seg_3296" s="T364">тогда</ta>
            <ta e="T366" id="Seg_3297" s="T365">там</ta>
            <ta e="T367" id="Seg_3298" s="T366">Харыялах-DAT/LOC</ta>
            <ta e="T368" id="Seg_3299" s="T367">быть-PST1-1SG</ta>
            <ta e="T369" id="Seg_3300" s="T368">3PL.[NOM]</ta>
            <ta e="T370" id="Seg_3301" s="T369">однако</ta>
            <ta e="T371" id="Seg_3302" s="T370">тот.[NOM]</ta>
            <ta e="T372" id="Seg_3303" s="T371">берег-DAT/LOC</ta>
            <ta e="T373" id="Seg_3304" s="T372">такой</ta>
            <ta e="T374" id="Seg_3305" s="T373">на.том.берегу-на.том.берегу</ta>
            <ta e="T376" id="Seg_3306" s="T374">жить-PRS-3PL</ta>
            <ta e="T380" id="Seg_3307" s="T379">Оленек-EP-2SG.[NOM]</ta>
            <ta e="T381" id="Seg_3308" s="T380">отдельно</ta>
            <ta e="T382" id="Seg_3309" s="T381">Харыялах-EP-2SG.[NOM]</ta>
            <ta e="T385" id="Seg_3310" s="T382">отдельно</ta>
            <ta e="T389" id="Seg_3311" s="T387">AFFIRM</ta>
            <ta e="T390" id="Seg_3312" s="T389">два</ta>
            <ta e="T391" id="Seg_3313" s="T390">что.[NOM]</ta>
            <ta e="T409" id="Seg_3314" s="T407">совсем</ta>
            <ta e="T410" id="Seg_3315" s="T409">маленький-1SG-ABL</ta>
            <ta e="T411" id="Seg_3316" s="T410">говорить-HAB-3PL</ta>
            <ta e="T412" id="Seg_3317" s="T411">этот</ta>
            <ta e="T413" id="Seg_3318" s="T412">вот</ta>
            <ta e="T414" id="Seg_3319" s="T413">садик-DAT/LOC</ta>
            <ta e="T415" id="Seg_3320" s="T414">идти-PTCP.PRS-1SG-ABL</ta>
            <ta e="T416" id="Seg_3321" s="T415">петь-PTCP.PRS</ta>
            <ta e="T417" id="Seg_3322" s="T416">быть-PST2-1SG</ta>
            <ta e="T430" id="Seg_3323" s="T429">этот</ta>
            <ta e="T431" id="Seg_3324" s="T430">русский-PL-ACC</ta>
            <ta e="T432" id="Seg_3325" s="T431">задняя.часть-ADJZ</ta>
            <ta e="T433" id="Seg_3326" s="T432">петь-PTCP.PRS</ta>
            <ta e="T434" id="Seg_3327" s="T433">становиться-PST2-EP-1SG</ta>
            <ta e="T435" id="Seg_3328" s="T434">потом</ta>
            <ta e="T436" id="Seg_3329" s="T435">этот</ta>
            <ta e="T437" id="Seg_3330" s="T436">теперь</ta>
            <ta e="T438" id="Seg_3331" s="T437">долганский-PL-DAT/LOC</ta>
            <ta e="T439" id="Seg_3332" s="T438">долганский-PL-ACC</ta>
            <ta e="T440" id="Seg_3333" s="T439">только</ta>
            <ta e="T441" id="Seg_3334" s="T440">петь-PRS-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc-ZhNA">
            <ta e="T2" id="Seg_3335" s="T1">pers.[pro:case]</ta>
            <ta e="T3" id="Seg_3336" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_3337" s="T3">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T5" id="Seg_3338" s="T4">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T6" id="Seg_3339" s="T5">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T7" id="Seg_3340" s="T6">adj.[n:case]</ta>
            <ta e="T8" id="Seg_3341" s="T7">propr-n:case</ta>
            <ta e="T9" id="Seg_3342" s="T8">conj</ta>
            <ta e="T10" id="Seg_3343" s="T9">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T11" id="Seg_3344" s="T10">n-n:(num).[n:case]</ta>
            <ta e="T12" id="Seg_3345" s="T11">conj</ta>
            <ta e="T13" id="Seg_3346" s="T12">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T14" id="Seg_3347" s="T13">n-n:(num).[n:case]</ta>
            <ta e="T15" id="Seg_3348" s="T14">conj</ta>
            <ta e="T16" id="Seg_3349" s="T15">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T17" id="Seg_3350" s="T16">adj-n:case</ta>
            <ta e="T18" id="Seg_3351" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_3352" s="T18">propr-n:case</ta>
            <ta e="T20" id="Seg_3353" s="T19">pers.[pro:case]</ta>
            <ta e="T21" id="Seg_3354" s="T20">n-n:(poss).[n:case]</ta>
            <ta e="T22" id="Seg_3355" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_3356" s="T22">propr-n:case</ta>
            <ta e="T24" id="Seg_3357" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_3358" s="T24">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T26" id="Seg_3359" s="T25">dempro.[pro:case]</ta>
            <ta e="T27" id="Seg_3360" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_3361" s="T27">v-v:cvb</ta>
            <ta e="T30" id="Seg_3362" s="T28">v-v:cvb</ta>
            <ta e="T35" id="Seg_3363" s="T34">propr</ta>
            <ta e="T36" id="Seg_3364" s="T35">propr</ta>
            <ta e="T37" id="Seg_3365" s="T36">v-v:cvb</ta>
            <ta e="T38" id="Seg_3366" s="T37">adv</ta>
            <ta e="T39" id="Seg_3367" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_3368" s="T39">v-v:cvb</ta>
            <ta e="T41" id="Seg_3369" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_3370" s="T41">v-v:tense-v:poss.pn</ta>
            <ta e="T43" id="Seg_3371" s="T42">n-n:(poss).[n:case]</ta>
            <ta e="T44" id="Seg_3372" s="T43">cardnum</ta>
            <ta e="T45" id="Seg_3373" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_3374" s="T45">v-v:tense.[v:pred.pn]</ta>
            <ta e="T47" id="Seg_3375" s="T46">dempro</ta>
            <ta e="T48" id="Seg_3376" s="T47">propr-n:case</ta>
            <ta e="T49" id="Seg_3377" s="T48">propr-n:case</ta>
            <ta e="T50" id="Seg_3378" s="T49">v-v:cvb</ta>
            <ta e="T51" id="Seg_3379" s="T50">adv</ta>
            <ta e="T52" id="Seg_3380" s="T51">adv</ta>
            <ta e="T53" id="Seg_3381" s="T52">v-v:cvb</ta>
            <ta e="T54" id="Seg_3382" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_3383" s="T54">adv</ta>
            <ta e="T56" id="Seg_3384" s="T55">propr-n:case</ta>
            <ta e="T57" id="Seg_3385" s="T56">propr-n:case</ta>
            <ta e="T58" id="Seg_3386" s="T57">propr-n:case</ta>
            <ta e="T59" id="Seg_3387" s="T58">conj</ta>
            <ta e="T60" id="Seg_3388" s="T59">v-v:cvb</ta>
            <ta e="T61" id="Seg_3389" s="T60">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T62" id="Seg_3390" s="T61">propr-n:case</ta>
            <ta e="T63" id="Seg_3391" s="T62">conj</ta>
            <ta e="T64" id="Seg_3392" s="T63">v-v:cvb</ta>
            <ta e="T65" id="Seg_3393" s="T64">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T66" id="Seg_3394" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_3395" s="T66">adj-n:poss-n:case</ta>
            <ta e="T68" id="Seg_3396" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_3397" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_3398" s="T69">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T71" id="Seg_3399" s="T70">n-n:poss-n:case</ta>
            <ta e="T72" id="Seg_3400" s="T71">v-v:mood-v:temp.pn</ta>
            <ta e="T73" id="Seg_3401" s="T72">adv</ta>
            <ta e="T74" id="Seg_3402" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_3403" s="T74">adj</ta>
            <ta e="T76" id="Seg_3404" s="T75">n-n:poss-n:case</ta>
            <ta e="T77" id="Seg_3405" s="T76">n-n:(poss).[n:case]</ta>
            <ta e="T78" id="Seg_3406" s="T77">v-v:tense-v:pred.pn</ta>
            <ta e="T79" id="Seg_3407" s="T78">pers.[pro:case]</ta>
            <ta e="T80" id="Seg_3408" s="T79">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T81" id="Seg_3409" s="T80">cardnum-n:(num)-n:(pred.pn)</ta>
            <ta e="T82" id="Seg_3410" s="T81">n-n:poss-n:case</ta>
            <ta e="T83" id="Seg_3411" s="T82">n-n:(poss).[n:case]</ta>
            <ta e="T84" id="Seg_3412" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_3413" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_3414" s="T85">n-n:(poss).[n:case]</ta>
            <ta e="T87" id="Seg_3415" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_3416" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_3417" s="T88">dempro.[pro:case]</ta>
            <ta e="T91" id="Seg_3418" s="T89">post</ta>
            <ta e="T98" id="Seg_3419" s="T97">pers.[pro:case]</ta>
            <ta e="T99" id="Seg_3420" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_3421" s="T99">propr-n:case</ta>
            <ta e="T101" id="Seg_3422" s="T100">v-v:cvb</ta>
            <ta e="T102" id="Seg_3423" s="T101">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T103" id="Seg_3424" s="T102">cardnum-cardnum</ta>
            <ta e="T104" id="Seg_3425" s="T103">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T105" id="Seg_3426" s="T104">cardnum-cardnum</ta>
            <ta e="T106" id="Seg_3427" s="T105">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T107" id="Seg_3428" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_3429" s="T107">adv</ta>
            <ta e="T109" id="Seg_3430" s="T108">v-v:cvb</ta>
            <ta e="T110" id="Seg_3431" s="T109">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T111" id="Seg_3432" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_3433" s="T111">adv</ta>
            <ta e="T113" id="Seg_3434" s="T112">adv</ta>
            <ta e="T114" id="Seg_3435" s="T113">n.[n:case]</ta>
            <ta e="T115" id="Seg_3436" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_3437" s="T115">que.[pro:case]</ta>
            <ta e="T117" id="Seg_3438" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_3439" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_3440" s="T118">post</ta>
            <ta e="T120" id="Seg_3441" s="T119">v-v:tense-v:poss.pn</ta>
            <ta e="T122" id="Seg_3442" s="T120">ptcl</ta>
            <ta e="T127" id="Seg_3443" s="T124">ptcl</ta>
            <ta e="T129" id="Seg_3444" s="T127">propr-n:case</ta>
            <ta e="T130" id="Seg_3445" s="T129">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T131" id="Seg_3446" s="T130">adv</ta>
            <ta e="T132" id="Seg_3447" s="T131">adj.[n:case]</ta>
            <ta e="T133" id="Seg_3448" s="T132">v-v:tense-v:poss.pn</ta>
            <ta e="T134" id="Seg_3449" s="T133">que</ta>
            <ta e="T135" id="Seg_3450" s="T134">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T136" id="Seg_3451" s="T135">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T137" id="Seg_3452" s="T136">v-v:(neg)-v:pred.pn</ta>
            <ta e="T144" id="Seg_3453" s="T141">ptcl</ta>
            <ta e="T146" id="Seg_3454" s="T144">propr-n:case</ta>
            <ta e="T147" id="Seg_3455" s="T146">adv</ta>
            <ta e="T148" id="Seg_3456" s="T147">v-v:tense-v:poss.pn</ta>
            <ta e="T149" id="Seg_3457" s="T148">dempro.[pro:case]</ta>
            <ta e="T150" id="Seg_3458" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_3459" s="T150">v-v:tense-v:poss.pn</ta>
            <ta e="T161" id="Seg_3460" s="T159">adj.[n:case]</ta>
            <ta e="T162" id="Seg_3461" s="T161">v-v:mood-v:temp.pn</ta>
            <ta e="T163" id="Seg_3462" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_3463" s="T163">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T165" id="Seg_3464" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_3465" s="T165">v-v:cvb-v:pred.pn</ta>
            <ta e="T167" id="Seg_3466" s="T166">v-v:ptcp</ta>
            <ta e="T168" id="Seg_3467" s="T167">v-v:tense-v:poss.pn</ta>
            <ta e="T169" id="Seg_3468" s="T168">dempro</ta>
            <ta e="T170" id="Seg_3469" s="T169">adj-n:(poss).[n:case]</ta>
            <ta e="T172" id="Seg_3470" s="T170">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp.[v:(case)]</ta>
            <ta e="T178" id="Seg_3471" s="T176">ptcl</ta>
            <ta e="T179" id="Seg_3472" s="T178">dempro</ta>
            <ta e="T182" id="Seg_3473" s="T181">v-v:ptcp</ta>
            <ta e="T183" id="Seg_3474" s="T182">n-n:(num)-n:poss-n:case</ta>
            <ta e="T184" id="Seg_3475" s="T183">v-v:ptcp</ta>
            <ta e="T185" id="Seg_3476" s="T184">v-v:tense-v:poss.pn</ta>
            <ta e="T186" id="Seg_3477" s="T185">n-n:(poss).[n:case]</ta>
            <ta e="T187" id="Seg_3478" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_3479" s="T187">v-v:mood-v:temp.pn</ta>
            <ta e="T189" id="Seg_3480" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_3481" s="T189">v-v:tense-v:pred.pn</ta>
            <ta e="T191" id="Seg_3482" s="T190">que-pro:case</ta>
            <ta e="T192" id="Seg_3483" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_3484" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_3485" s="T193">v-v:mood-v:temp.pn</ta>
            <ta e="T195" id="Seg_3486" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_3487" s="T195">v-v:tense-v:pred.pn</ta>
            <ta e="T197" id="Seg_3488" s="T196">dempro</ta>
            <ta e="T210" id="Seg_3489" s="T208">que.[pro:case]</ta>
            <ta e="T211" id="Seg_3490" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_3491" s="T211">v-v:ptcp</ta>
            <ta e="T213" id="Seg_3492" s="T212">v-v:tense-v:poss.pn</ta>
            <ta e="T214" id="Seg_3493" s="T213">n-n:poss-n:case</ta>
            <ta e="T215" id="Seg_3494" s="T214">conj</ta>
            <ta e="T216" id="Seg_3495" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_3496" s="T216">dempro.[pro:case]</ta>
            <ta e="T218" id="Seg_3497" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_3498" s="T218">n-n:(num).[n:case]</ta>
            <ta e="T220" id="Seg_3499" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_3500" s="T220">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T222" id="Seg_3501" s="T221">v-v:tense.[v:poss.pn]</ta>
            <ta e="T223" id="Seg_3502" s="T222">dempro-pro:(num).[pro:case]</ta>
            <ta e="T229" id="Seg_3503" s="T227">ptcl</ta>
            <ta e="T243" id="Seg_3504" s="T240">interj</ta>
            <ta e="T253" id="Seg_3505" s="T250">interj</ta>
            <ta e="T255" id="Seg_3506" s="T253">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T256" id="Seg_3507" s="T255">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T257" id="Seg_3508" s="T256">v-v:ptcp</ta>
            <ta e="T258" id="Seg_3509" s="T257">v-v:tense-v:poss.pn</ta>
            <ta e="T259" id="Seg_3510" s="T258">dempro</ta>
            <ta e="T260" id="Seg_3511" s="T259">propr-n:case</ta>
            <ta e="T261" id="Seg_3512" s="T260">que-pro:case</ta>
            <ta e="T262" id="Seg_3513" s="T261">n-n:(ins)-n:case</ta>
            <ta e="T263" id="Seg_3514" s="T262">dempro</ta>
            <ta e="T264" id="Seg_3515" s="T263">v-v:cvb-v:pred.pn</ta>
            <ta e="T265" id="Seg_3516" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_3517" s="T265">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T267" id="Seg_3518" s="T266">que-pro:(poss)-pro:case</ta>
            <ta e="T268" id="Seg_3519" s="T267">v-v:mood-v:pred.pn</ta>
            <ta e="T269" id="Seg_3520" s="T268">pers.[pro:case]</ta>
            <ta e="T270" id="Seg_3521" s="T269">n-n:(num)-n:case</ta>
            <ta e="T271" id="Seg_3522" s="T270">que-pro:(num)-pro:case</ta>
            <ta e="T272" id="Seg_3523" s="T271">n.[n:case]</ta>
            <ta e="T273" id="Seg_3524" s="T272">post</ta>
            <ta e="T274" id="Seg_3525" s="T273">v-v:tense-v:poss.pn</ta>
            <ta e="T275" id="Seg_3526" s="T274">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T276" id="Seg_3527" s="T275">n.[n:case]</ta>
            <ta e="T277" id="Seg_3528" s="T276">adj.[n:case]</ta>
            <ta e="T278" id="Seg_3529" s="T277">v-v:tense-v:pred.pn</ta>
            <ta e="T279" id="Seg_3530" s="T278">dempro-pro:(num)-pro:case</ta>
            <ta e="T282" id="Seg_3531" s="T281">n-n:case</ta>
            <ta e="T284" id="Seg_3532" s="T282">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T293" id="Seg_3533" s="T291">interj</ta>
            <ta e="T295" id="Seg_3534" s="T293">dempro</ta>
            <ta e="T296" id="Seg_3535" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_3536" s="T296">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T298" id="Seg_3537" s="T297">adv</ta>
            <ta e="T299" id="Seg_3538" s="T298">adv</ta>
            <ta e="T300" id="Seg_3539" s="T299">v-v:tense-v:poss.pn</ta>
            <ta e="T301" id="Seg_3540" s="T300">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T302" id="Seg_3541" s="T301">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T303" id="Seg_3542" s="T302">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T304" id="Seg_3543" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_3544" s="T304">cardnum</ta>
            <ta e="T306" id="Seg_3545" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_3546" s="T306">cardnum</ta>
            <ta e="T308" id="Seg_3547" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_3548" s="T308">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T310" id="Seg_3549" s="T309">adv</ta>
            <ta e="T311" id="Seg_3550" s="T310">dempro.[pro:case]</ta>
            <ta e="T312" id="Seg_3551" s="T311">post</ta>
            <ta e="T314" id="Seg_3552" s="T312">v-v:tense-v:pred.pn</ta>
            <ta e="T320" id="Seg_3553" s="T318">propr-n:case</ta>
            <ta e="T321" id="Seg_3554" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_3555" s="T321">conj</ta>
            <ta e="T323" id="Seg_3556" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_3557" s="T323">v-v:tense-v:poss.pn</ta>
            <ta e="T325" id="Seg_3558" s="T324">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T326" id="Seg_3559" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_3560" s="T326">que</ta>
            <ta e="T328" id="Seg_3561" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_3562" s="T328">v-v:ptcp-v:(case)</ta>
            <ta e="T330" id="Seg_3563" s="T329">adv</ta>
            <ta e="T331" id="Seg_3564" s="T330">que-pro:case</ta>
            <ta e="T332" id="Seg_3565" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_3566" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_3567" s="T333">que</ta>
            <ta e="T335" id="Seg_3568" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_3569" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_3570" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_3571" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_3572" s="T338">v-v:tense-v:mood.[v:pred.pn]</ta>
            <ta e="T349" id="Seg_3573" s="T347">propr</ta>
            <ta e="T350" id="Seg_3574" s="T349">n</ta>
            <ta e="T359" id="Seg_3575" s="T358">propr</ta>
            <ta e="T360" id="Seg_3576" s="T359">n</ta>
            <ta e="T361" id="Seg_3577" s="T360">n.[n:case]</ta>
            <ta e="T362" id="Seg_3578" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_3579" s="T362">pers.[pro:case]</ta>
            <ta e="T364" id="Seg_3580" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_3581" s="T364">adv</ta>
            <ta e="T366" id="Seg_3582" s="T365">adv</ta>
            <ta e="T367" id="Seg_3583" s="T366">propr-n:case</ta>
            <ta e="T368" id="Seg_3584" s="T367">v-v:tense-v:poss.pn</ta>
            <ta e="T369" id="Seg_3585" s="T368">pers.[pro:case]</ta>
            <ta e="T370" id="Seg_3586" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_3587" s="T370">dempro.[pro:case]</ta>
            <ta e="T372" id="Seg_3588" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_3589" s="T372">dempro</ta>
            <ta e="T374" id="Seg_3590" s="T373">adv-adv</ta>
            <ta e="T376" id="Seg_3591" s="T374">v-v:tense-v:pred.pn</ta>
            <ta e="T380" id="Seg_3592" s="T379">propr-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T381" id="Seg_3593" s="T380">adv</ta>
            <ta e="T382" id="Seg_3594" s="T381">propr-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T385" id="Seg_3595" s="T382">adv</ta>
            <ta e="T389" id="Seg_3596" s="T387">ptcl</ta>
            <ta e="T390" id="Seg_3597" s="T389">cardnum</ta>
            <ta e="T391" id="Seg_3598" s="T390">que.[pro:case]</ta>
            <ta e="T409" id="Seg_3599" s="T407">adv</ta>
            <ta e="T410" id="Seg_3600" s="T409">adj-n:poss-n:case</ta>
            <ta e="T411" id="Seg_3601" s="T410">v-v:mood-v:pred.pn</ta>
            <ta e="T412" id="Seg_3602" s="T411">dempro</ta>
            <ta e="T413" id="Seg_3603" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_3604" s="T413">n-n:case</ta>
            <ta e="T415" id="Seg_3605" s="T414">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T416" id="Seg_3606" s="T415">v-v:ptcp</ta>
            <ta e="T417" id="Seg_3607" s="T416">v-v:tense-v:pred.pn</ta>
            <ta e="T430" id="Seg_3608" s="T429">dempro</ta>
            <ta e="T431" id="Seg_3609" s="T430">adj-n:(num)-n:case</ta>
            <ta e="T432" id="Seg_3610" s="T431">n-n&gt;adj</ta>
            <ta e="T433" id="Seg_3611" s="T432">v-v:ptcp</ta>
            <ta e="T434" id="Seg_3612" s="T433">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T435" id="Seg_3613" s="T434">adv</ta>
            <ta e="T436" id="Seg_3614" s="T435">dempro</ta>
            <ta e="T437" id="Seg_3615" s="T436">adv</ta>
            <ta e="T438" id="Seg_3616" s="T437">adj-n:(num)-n:case</ta>
            <ta e="T439" id="Seg_3617" s="T438">adj-n:(num)-n:case</ta>
            <ta e="T440" id="Seg_3618" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_3619" s="T440">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-ZhNA">
            <ta e="T2" id="Seg_3620" s="T1">pers</ta>
            <ta e="T3" id="Seg_3621" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_3622" s="T3">n</ta>
            <ta e="T5" id="Seg_3623" s="T4">emphpro</ta>
            <ta e="T6" id="Seg_3624" s="T5">n</ta>
            <ta e="T7" id="Seg_3625" s="T6">adj</ta>
            <ta e="T8" id="Seg_3626" s="T7">propr</ta>
            <ta e="T9" id="Seg_3627" s="T8">conj</ta>
            <ta e="T10" id="Seg_3628" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_3629" s="T10">n</ta>
            <ta e="T12" id="Seg_3630" s="T11">conj</ta>
            <ta e="T13" id="Seg_3631" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_3632" s="T13">n</ta>
            <ta e="T15" id="Seg_3633" s="T14">conj</ta>
            <ta e="T16" id="Seg_3634" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_3635" s="T16">adv</ta>
            <ta e="T18" id="Seg_3636" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_3637" s="T18">propr</ta>
            <ta e="T20" id="Seg_3638" s="T19">pers</ta>
            <ta e="T21" id="Seg_3639" s="T20">n</ta>
            <ta e="T22" id="Seg_3640" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_3641" s="T22">propr</ta>
            <ta e="T24" id="Seg_3642" s="T23">n</ta>
            <ta e="T25" id="Seg_3643" s="T24">v</ta>
            <ta e="T26" id="Seg_3644" s="T25">dempro</ta>
            <ta e="T27" id="Seg_3645" s="T26">n</ta>
            <ta e="T28" id="Seg_3646" s="T27">v</ta>
            <ta e="T30" id="Seg_3647" s="T28">aux</ta>
            <ta e="T35" id="Seg_3648" s="T34">propr</ta>
            <ta e="T36" id="Seg_3649" s="T35">propr</ta>
            <ta e="T37" id="Seg_3650" s="T36">v</ta>
            <ta e="T38" id="Seg_3651" s="T37">adv</ta>
            <ta e="T39" id="Seg_3652" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_3653" s="T39">v</ta>
            <ta e="T41" id="Seg_3654" s="T40">n</ta>
            <ta e="T42" id="Seg_3655" s="T41">v</ta>
            <ta e="T43" id="Seg_3656" s="T42">n</ta>
            <ta e="T44" id="Seg_3657" s="T43">cardnum</ta>
            <ta e="T45" id="Seg_3658" s="T44">n</ta>
            <ta e="T46" id="Seg_3659" s="T45">v</ta>
            <ta e="T47" id="Seg_3660" s="T46">dempro</ta>
            <ta e="T48" id="Seg_3661" s="T47">propr</ta>
            <ta e="T49" id="Seg_3662" s="T48">propr</ta>
            <ta e="T50" id="Seg_3663" s="T49">v</ta>
            <ta e="T51" id="Seg_3664" s="T50">adv</ta>
            <ta e="T52" id="Seg_3665" s="T51">adv</ta>
            <ta e="T53" id="Seg_3666" s="T52">v</ta>
            <ta e="T54" id="Seg_3667" s="T53">v</ta>
            <ta e="T55" id="Seg_3668" s="T54">adv</ta>
            <ta e="T56" id="Seg_3669" s="T55">propr</ta>
            <ta e="T57" id="Seg_3670" s="T56">propr</ta>
            <ta e="T58" id="Seg_3671" s="T57">propr</ta>
            <ta e="T59" id="Seg_3672" s="T58">conj</ta>
            <ta e="T60" id="Seg_3673" s="T59">v</ta>
            <ta e="T61" id="Seg_3674" s="T60">v</ta>
            <ta e="T62" id="Seg_3675" s="T61">propr</ta>
            <ta e="T63" id="Seg_3676" s="T62">conj</ta>
            <ta e="T64" id="Seg_3677" s="T63">v</ta>
            <ta e="T65" id="Seg_3678" s="T64">v</ta>
            <ta e="T66" id="Seg_3679" s="T65">n</ta>
            <ta e="T67" id="Seg_3680" s="T66">adj</ta>
            <ta e="T68" id="Seg_3681" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_3682" s="T68">v</ta>
            <ta e="T70" id="Seg_3683" s="T69">v</ta>
            <ta e="T71" id="Seg_3684" s="T70">n</ta>
            <ta e="T72" id="Seg_3685" s="T71">v</ta>
            <ta e="T73" id="Seg_3686" s="T72">adv</ta>
            <ta e="T74" id="Seg_3687" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_3688" s="T74">adj</ta>
            <ta e="T76" id="Seg_3689" s="T75">n</ta>
            <ta e="T77" id="Seg_3690" s="T76">n</ta>
            <ta e="T78" id="Seg_3691" s="T77">cop</ta>
            <ta e="T79" id="Seg_3692" s="T78">pers</ta>
            <ta e="T80" id="Seg_3693" s="T79">emphpro</ta>
            <ta e="T81" id="Seg_3694" s="T80">cardnum</ta>
            <ta e="T82" id="Seg_3695" s="T81">n</ta>
            <ta e="T83" id="Seg_3696" s="T82">n</ta>
            <ta e="T84" id="Seg_3697" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_3698" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_3699" s="T85">n</ta>
            <ta e="T87" id="Seg_3700" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_3701" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_3702" s="T88">dempro</ta>
            <ta e="T91" id="Seg_3703" s="T89">post</ta>
            <ta e="T98" id="Seg_3704" s="T97">pers</ta>
            <ta e="T99" id="Seg_3705" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_3706" s="T99">propr</ta>
            <ta e="T101" id="Seg_3707" s="T100">v</ta>
            <ta e="T102" id="Seg_3708" s="T101">aux</ta>
            <ta e="T103" id="Seg_3709" s="T102">cardnum</ta>
            <ta e="T104" id="Seg_3710" s="T103">n</ta>
            <ta e="T105" id="Seg_3711" s="T104">cardnum</ta>
            <ta e="T106" id="Seg_3712" s="T105">n</ta>
            <ta e="T107" id="Seg_3713" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_3714" s="T107">adv</ta>
            <ta e="T109" id="Seg_3715" s="T108">v</ta>
            <ta e="T110" id="Seg_3716" s="T109">aux</ta>
            <ta e="T111" id="Seg_3717" s="T110">n</ta>
            <ta e="T112" id="Seg_3718" s="T111">adv</ta>
            <ta e="T113" id="Seg_3719" s="T112">adv</ta>
            <ta e="T114" id="Seg_3720" s="T113">n</ta>
            <ta e="T115" id="Seg_3721" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_3722" s="T115">que</ta>
            <ta e="T117" id="Seg_3723" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_3724" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_3725" s="T118">post</ta>
            <ta e="T120" id="Seg_3726" s="T119">cop</ta>
            <ta e="T122" id="Seg_3727" s="T120">ptcl</ta>
            <ta e="T127" id="Seg_3728" s="T124">ptcl</ta>
            <ta e="T129" id="Seg_3729" s="T127">propr</ta>
            <ta e="T130" id="Seg_3730" s="T129">v</ta>
            <ta e="T131" id="Seg_3731" s="T130">adv</ta>
            <ta e="T132" id="Seg_3732" s="T131">adj</ta>
            <ta e="T133" id="Seg_3733" s="T132">cop</ta>
            <ta e="T134" id="Seg_3734" s="T133">que</ta>
            <ta e="T135" id="Seg_3735" s="T134">adj</ta>
            <ta e="T136" id="Seg_3736" s="T135">emphpro</ta>
            <ta e="T137" id="Seg_3737" s="T136">v</ta>
            <ta e="T144" id="Seg_3738" s="T141">ptcl</ta>
            <ta e="T146" id="Seg_3739" s="T144">propr</ta>
            <ta e="T147" id="Seg_3740" s="T146">adv</ta>
            <ta e="T148" id="Seg_3741" s="T147">v</ta>
            <ta e="T149" id="Seg_3742" s="T148">dempro</ta>
            <ta e="T150" id="Seg_3743" s="T149">n</ta>
            <ta e="T151" id="Seg_3744" s="T150">v</ta>
            <ta e="T161" id="Seg_3745" s="T159">adj</ta>
            <ta e="T162" id="Seg_3746" s="T161">cop</ta>
            <ta e="T163" id="Seg_3747" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_3748" s="T163">emphpro</ta>
            <ta e="T165" id="Seg_3749" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_3750" s="T165">v</ta>
            <ta e="T167" id="Seg_3751" s="T166">v</ta>
            <ta e="T168" id="Seg_3752" s="T167">aux</ta>
            <ta e="T169" id="Seg_3753" s="T168">dempro</ta>
            <ta e="T170" id="Seg_3754" s="T169">adj</ta>
            <ta e="T172" id="Seg_3755" s="T170">adj</ta>
            <ta e="T178" id="Seg_3756" s="T176">ptcl</ta>
            <ta e="T179" id="Seg_3757" s="T178">dempro</ta>
            <ta e="T182" id="Seg_3758" s="T181">v</ta>
            <ta e="T183" id="Seg_3759" s="T182">n</ta>
            <ta e="T184" id="Seg_3760" s="T183">v</ta>
            <ta e="T185" id="Seg_3761" s="T184">aux</ta>
            <ta e="T186" id="Seg_3762" s="T185">n</ta>
            <ta e="T187" id="Seg_3763" s="T186">n</ta>
            <ta e="T188" id="Seg_3764" s="T187">v</ta>
            <ta e="T189" id="Seg_3765" s="T188">n</ta>
            <ta e="T190" id="Seg_3766" s="T189">v</ta>
            <ta e="T191" id="Seg_3767" s="T190">que</ta>
            <ta e="T192" id="Seg_3768" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_3769" s="T192">n</ta>
            <ta e="T194" id="Seg_3770" s="T193">v</ta>
            <ta e="T195" id="Seg_3771" s="T194">n</ta>
            <ta e="T196" id="Seg_3772" s="T195">v</ta>
            <ta e="T197" id="Seg_3773" s="T196">dempro</ta>
            <ta e="T210" id="Seg_3774" s="T208">que</ta>
            <ta e="T211" id="Seg_3775" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_3776" s="T211">v</ta>
            <ta e="T213" id="Seg_3777" s="T212">aux</ta>
            <ta e="T214" id="Seg_3778" s="T213">n</ta>
            <ta e="T215" id="Seg_3779" s="T214">conj</ta>
            <ta e="T216" id="Seg_3780" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_3781" s="T216">dempro</ta>
            <ta e="T218" id="Seg_3782" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_3783" s="T218">n</ta>
            <ta e="T220" id="Seg_3784" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_3785" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_3786" s="T221">cop</ta>
            <ta e="T223" id="Seg_3787" s="T222">dempro</ta>
            <ta e="T229" id="Seg_3788" s="T227">ptcl</ta>
            <ta e="T243" id="Seg_3789" s="T240">interj</ta>
            <ta e="T253" id="Seg_3790" s="T250">interj</ta>
            <ta e="T255" id="Seg_3791" s="T253">v</ta>
            <ta e="T256" id="Seg_3792" s="T255">v</ta>
            <ta e="T257" id="Seg_3793" s="T256">v</ta>
            <ta e="T258" id="Seg_3794" s="T257">aux</ta>
            <ta e="T259" id="Seg_3795" s="T258">dempro</ta>
            <ta e="T260" id="Seg_3796" s="T259">propr</ta>
            <ta e="T261" id="Seg_3797" s="T260">que</ta>
            <ta e="T262" id="Seg_3798" s="T261">n</ta>
            <ta e="T263" id="Seg_3799" s="T262">dempro</ta>
            <ta e="T264" id="Seg_3800" s="T263">v</ta>
            <ta e="T265" id="Seg_3801" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_3802" s="T265">emphpro</ta>
            <ta e="T267" id="Seg_3803" s="T266">que</ta>
            <ta e="T268" id="Seg_3804" s="T267">v</ta>
            <ta e="T269" id="Seg_3805" s="T268">pers</ta>
            <ta e="T270" id="Seg_3806" s="T269">n</ta>
            <ta e="T271" id="Seg_3807" s="T270">que</ta>
            <ta e="T272" id="Seg_3808" s="T271">n</ta>
            <ta e="T273" id="Seg_3809" s="T272">post</ta>
            <ta e="T274" id="Seg_3810" s="T273">cop</ta>
            <ta e="T275" id="Seg_3811" s="T274">emphpro</ta>
            <ta e="T276" id="Seg_3812" s="T275">n</ta>
            <ta e="T277" id="Seg_3813" s="T276">adj</ta>
            <ta e="T278" id="Seg_3814" s="T277">v</ta>
            <ta e="T279" id="Seg_3815" s="T278">dempro</ta>
            <ta e="T282" id="Seg_3816" s="T281">n</ta>
            <ta e="T284" id="Seg_3817" s="T282">v</ta>
            <ta e="T293" id="Seg_3818" s="T291">interj</ta>
            <ta e="T295" id="Seg_3819" s="T293">dempro</ta>
            <ta e="T296" id="Seg_3820" s="T295">n</ta>
            <ta e="T297" id="Seg_3821" s="T296">v</ta>
            <ta e="T298" id="Seg_3822" s="T297">adv</ta>
            <ta e="T299" id="Seg_3823" s="T298">adv</ta>
            <ta e="T300" id="Seg_3824" s="T299">v</ta>
            <ta e="T301" id="Seg_3825" s="T300">emphpro</ta>
            <ta e="T302" id="Seg_3826" s="T301">v</ta>
            <ta e="T303" id="Seg_3827" s="T302">emphpro</ta>
            <ta e="T304" id="Seg_3828" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_3829" s="T304">cardnum</ta>
            <ta e="T306" id="Seg_3830" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_3831" s="T306">cardnum</ta>
            <ta e="T308" id="Seg_3832" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_3833" s="T308">adj</ta>
            <ta e="T310" id="Seg_3834" s="T309">adv</ta>
            <ta e="T311" id="Seg_3835" s="T310">dempro</ta>
            <ta e="T312" id="Seg_3836" s="T311">post</ta>
            <ta e="T314" id="Seg_3837" s="T312">v</ta>
            <ta e="T320" id="Seg_3838" s="T318">propr</ta>
            <ta e="T321" id="Seg_3839" s="T320">n</ta>
            <ta e="T322" id="Seg_3840" s="T321">conj</ta>
            <ta e="T323" id="Seg_3841" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_3842" s="T323">cop</ta>
            <ta e="T325" id="Seg_3843" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_3844" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_3845" s="T326">que</ta>
            <ta e="T328" id="Seg_3846" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_3847" s="T328">v</ta>
            <ta e="T330" id="Seg_3848" s="T329">adv</ta>
            <ta e="T331" id="Seg_3849" s="T330">que</ta>
            <ta e="T332" id="Seg_3850" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_3851" s="T332">n</ta>
            <ta e="T334" id="Seg_3852" s="T333">que</ta>
            <ta e="T335" id="Seg_3853" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_3854" s="T335">n</ta>
            <ta e="T337" id="Seg_3855" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_3856" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_3857" s="T338">cop</ta>
            <ta e="T349" id="Seg_3858" s="T347">propr</ta>
            <ta e="T350" id="Seg_3859" s="T349">n</ta>
            <ta e="T359" id="Seg_3860" s="T358">propr</ta>
            <ta e="T360" id="Seg_3861" s="T359">n</ta>
            <ta e="T361" id="Seg_3862" s="T360">n</ta>
            <ta e="T362" id="Seg_3863" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_3864" s="T362">pers</ta>
            <ta e="T364" id="Seg_3865" s="T363">ptcl</ta>
            <ta e="T365" id="Seg_3866" s="T364">adv</ta>
            <ta e="T366" id="Seg_3867" s="T365">adv</ta>
            <ta e="T367" id="Seg_3868" s="T366">propr</ta>
            <ta e="T368" id="Seg_3869" s="T367">cop</ta>
            <ta e="T369" id="Seg_3870" s="T368">pers</ta>
            <ta e="T370" id="Seg_3871" s="T369">ptcl</ta>
            <ta e="T371" id="Seg_3872" s="T370">dempro</ta>
            <ta e="T372" id="Seg_3873" s="T371">n</ta>
            <ta e="T373" id="Seg_3874" s="T372">dempro</ta>
            <ta e="T374" id="Seg_3875" s="T373">adv</ta>
            <ta e="T376" id="Seg_3876" s="T374">v</ta>
            <ta e="T380" id="Seg_3877" s="T379">propr</ta>
            <ta e="T381" id="Seg_3878" s="T380">adv</ta>
            <ta e="T382" id="Seg_3879" s="T381">propr</ta>
            <ta e="T385" id="Seg_3880" s="T382">adv</ta>
            <ta e="T389" id="Seg_3881" s="T387">ptcl</ta>
            <ta e="T390" id="Seg_3882" s="T389">cardnum</ta>
            <ta e="T391" id="Seg_3883" s="T390">que</ta>
            <ta e="T409" id="Seg_3884" s="T407">adv</ta>
            <ta e="T410" id="Seg_3885" s="T409">adj</ta>
            <ta e="T411" id="Seg_3886" s="T410">v</ta>
            <ta e="T412" id="Seg_3887" s="T411">dempro</ta>
            <ta e="T413" id="Seg_3888" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_3889" s="T413">n</ta>
            <ta e="T415" id="Seg_3890" s="T414">v</ta>
            <ta e="T416" id="Seg_3891" s="T415">v</ta>
            <ta e="T417" id="Seg_3892" s="T416">aux</ta>
            <ta e="T430" id="Seg_3893" s="T429">dempro</ta>
            <ta e="T431" id="Seg_3894" s="T430">n</ta>
            <ta e="T432" id="Seg_3895" s="T431">adj</ta>
            <ta e="T433" id="Seg_3896" s="T432">v</ta>
            <ta e="T434" id="Seg_3897" s="T433">aux</ta>
            <ta e="T435" id="Seg_3898" s="T434">adv</ta>
            <ta e="T436" id="Seg_3899" s="T435">dempro</ta>
            <ta e="T437" id="Seg_3900" s="T436">adv</ta>
            <ta e="T438" id="Seg_3901" s="T437">n</ta>
            <ta e="T439" id="Seg_3902" s="T438">n</ta>
            <ta e="T440" id="Seg_3903" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_3904" s="T440">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-ZhNA">
            <ta e="T2" id="Seg_3905" s="T1">pro.h:Poss</ta>
            <ta e="T4" id="Seg_3906" s="T3">np:Th</ta>
            <ta e="T5" id="Seg_3907" s="T4">pro.h:Poss</ta>
            <ta e="T6" id="Seg_3908" s="T5">np:Th</ta>
            <ta e="T8" id="Seg_3909" s="T7">np:L</ta>
            <ta e="T10" id="Seg_3910" s="T9">0.3.h:Th</ta>
            <ta e="T11" id="Seg_3911" s="T10">np.h:Th</ta>
            <ta e="T14" id="Seg_3912" s="T13">np.h:Th</ta>
            <ta e="T17" id="Seg_3913" s="T16">adv:Time</ta>
            <ta e="T19" id="Seg_3914" s="T18">np:So</ta>
            <ta e="T20" id="Seg_3915" s="T19">pro.h:Poss</ta>
            <ta e="T21" id="Seg_3916" s="T20">np.h:A</ta>
            <ta e="T23" id="Seg_3917" s="T22">np:So</ta>
            <ta e="T27" id="Seg_3918" s="T26">np:L</ta>
            <ta e="T42" id="Seg_3919" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_3920" s="T42">np.h:A</ta>
            <ta e="T45" id="Seg_3921" s="T44">np.h:P</ta>
            <ta e="T48" id="Seg_3922" s="T47">np:L</ta>
            <ta e="T49" id="Seg_3923" s="T48">np:G</ta>
            <ta e="T51" id="Seg_3924" s="T50">adv:Time</ta>
            <ta e="T54" id="Seg_3925" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_3926" s="T54">adv:G</ta>
            <ta e="T56" id="Seg_3927" s="T55">np:G</ta>
            <ta e="T57" id="Seg_3928" s="T56">np:So</ta>
            <ta e="T58" id="Seg_3929" s="T57">np:G</ta>
            <ta e="T61" id="Seg_3930" s="T59">0.3.h:A</ta>
            <ta e="T62" id="Seg_3931" s="T61">np:G</ta>
            <ta e="T65" id="Seg_3932" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_3933" s="T65">np:G</ta>
            <ta e="T70" id="Seg_3934" s="T69">0.3.h:A</ta>
            <ta e="T71" id="Seg_3935" s="T70">np:St</ta>
            <ta e="T72" id="Seg_3936" s="T71">0.1.h:E</ta>
            <ta e="T76" id="Seg_3937" s="T75">np.h:Poss</ta>
            <ta e="T79" id="Seg_3938" s="T78">pro.h:Th</ta>
            <ta e="T80" id="Seg_3939" s="T79">pro.h:Th</ta>
            <ta e="T82" id="Seg_3940" s="T81">0.1.h:Poss</ta>
            <ta e="T83" id="Seg_3941" s="T82">np.h:Th</ta>
            <ta e="T86" id="Seg_3942" s="T85">np.h:Th</ta>
            <ta e="T98" id="Seg_3943" s="T97">pro.h:A</ta>
            <ta e="T100" id="Seg_3944" s="T99">np:L</ta>
            <ta e="T104" id="Seg_3945" s="T103">n:Time</ta>
            <ta e="T106" id="Seg_3946" s="T105">n:Time</ta>
            <ta e="T110" id="Seg_3947" s="T108">0.1.h:A</ta>
            <ta e="T111" id="Seg_3948" s="T110">np:L</ta>
            <ta e="T114" id="Seg_3949" s="T113">np:Th</ta>
            <ta e="T116" id="Seg_3950" s="T115">pro:Th</ta>
            <ta e="T129" id="Seg_3951" s="T127">np:L</ta>
            <ta e="T130" id="Seg_3952" s="T129">0.1.h:A</ta>
            <ta e="T131" id="Seg_3953" s="T130">adv:Time</ta>
            <ta e="T133" id="Seg_3954" s="T132">0.1.h:Th</ta>
            <ta e="T136" id="Seg_3955" s="T135">pro.h:E</ta>
            <ta e="T146" id="Seg_3956" s="T144">np:So</ta>
            <ta e="T147" id="Seg_3957" s="T146">adv:So</ta>
            <ta e="T148" id="Seg_3958" s="T147">0.3.h:A</ta>
            <ta e="T150" id="Seg_3959" s="T149">np:So</ta>
            <ta e="T151" id="Seg_3960" s="T150">0.3.h:A</ta>
            <ta e="T162" id="Seg_3961" s="T161">0.1.h:Th</ta>
            <ta e="T164" id="Seg_3962" s="T163">pro.h:A</ta>
            <ta e="T170" id="Seg_3963" s="T169">np:Th</ta>
            <ta e="T183" id="Seg_3964" s="T182">np:Th</ta>
            <ta e="T185" id="Seg_3965" s="T183">0.1.h:A</ta>
            <ta e="T187" id="Seg_3966" s="T186">np.h:St</ta>
            <ta e="T188" id="Seg_3967" s="T187">0.1.h:E</ta>
            <ta e="T189" id="Seg_3968" s="T188">np.h:Th</ta>
            <ta e="T190" id="Seg_3969" s="T189">0.1.h:A</ta>
            <ta e="T193" id="Seg_3970" s="T192">np:Th</ta>
            <ta e="T194" id="Seg_3971" s="T193">0.1.h:A</ta>
            <ta e="T195" id="Seg_3972" s="T194">np:Th</ta>
            <ta e="T196" id="Seg_3973" s="T195">0.1.h:A</ta>
            <ta e="T210" id="Seg_3974" s="T208">pro.h:A</ta>
            <ta e="T214" id="Seg_3975" s="T213">0.1.h:Poss</ta>
            <ta e="T219" id="Seg_3976" s="T218">np.h:Th</ta>
            <ta e="T255" id="Seg_3977" s="T253">0.3:Th</ta>
            <ta e="T256" id="Seg_3978" s="T255">0.3:Th</ta>
            <ta e="T258" id="Seg_3979" s="T256">0.1.h:A</ta>
            <ta e="T260" id="Seg_3980" s="T259">np:L</ta>
            <ta e="T264" id="Seg_3981" s="T263">0.1.h:A</ta>
            <ta e="T267" id="Seg_3982" s="T266">pro:G</ta>
            <ta e="T269" id="Seg_3983" s="T268">pro.h:A</ta>
            <ta e="T270" id="Seg_3984" s="T269">np:G</ta>
            <ta e="T271" id="Seg_3985" s="T270">pro:G</ta>
            <ta e="T275" id="Seg_3986" s="T274">pro.h:Th</ta>
            <ta e="T276" id="Seg_3987" s="T275">np:Th</ta>
            <ta e="T277" id="Seg_3988" s="T276">np:Th</ta>
            <ta e="T278" id="Seg_3989" s="T277">0.1.h:A</ta>
            <ta e="T279" id="Seg_3990" s="T278">pro:L</ta>
            <ta e="T282" id="Seg_3991" s="T281">np:L</ta>
            <ta e="T284" id="Seg_3992" s="T282">0.1.h:A</ta>
            <ta e="T296" id="Seg_3993" s="T295">np:G</ta>
            <ta e="T297" id="Seg_3994" s="T296">0.1.h:A</ta>
            <ta e="T300" id="Seg_3995" s="T299">0.3.h:A</ta>
            <ta e="T301" id="Seg_3996" s="T300">pro.h:Th</ta>
            <ta e="T302" id="Seg_3997" s="T301">0.1.h:A</ta>
            <ta e="T303" id="Seg_3998" s="T302">pro.h:Poss</ta>
            <ta e="T309" id="Seg_3999" s="T308">np:Th</ta>
            <ta e="T310" id="Seg_4000" s="T309">adv:Time</ta>
            <ta e="T312" id="Seg_4001" s="T310">pp:L</ta>
            <ta e="T314" id="Seg_4002" s="T312">0.3:Th</ta>
            <ta e="T320" id="Seg_4003" s="T318">np:L</ta>
            <ta e="T324" id="Seg_4004" s="T323">0.3:Th</ta>
            <ta e="T325" id="Seg_4005" s="T324">0.3:Th</ta>
            <ta e="T327" id="Seg_4006" s="T326">pro:L</ta>
            <ta e="T333" id="Seg_4007" s="T332">np:So</ta>
            <ta e="T336" id="Seg_4008" s="T335">np:So</ta>
            <ta e="T339" id="Seg_4009" s="T338">0.3:Th</ta>
            <ta e="T363" id="Seg_4010" s="T362">pro.h:Th</ta>
            <ta e="T365" id="Seg_4011" s="T364">adv:Time</ta>
            <ta e="T367" id="Seg_4012" s="T366">np:L</ta>
            <ta e="T369" id="Seg_4013" s="T368">pro.h:Th</ta>
            <ta e="T372" id="Seg_4014" s="T371">np:L</ta>
            <ta e="T374" id="Seg_4015" s="T373">adv:L</ta>
            <ta e="T410" id="Seg_4016" s="T409">n:Time</ta>
            <ta e="T411" id="Seg_4017" s="T410">0.3.h:A</ta>
            <ta e="T414" id="Seg_4018" s="T413">np:G</ta>
            <ta e="T415" id="Seg_4019" s="T414">0.1.h:A</ta>
            <ta e="T417" id="Seg_4020" s="T415">0.1.h:A</ta>
            <ta e="T431" id="Seg_4021" s="T430">np:Th</ta>
            <ta e="T432" id="Seg_4022" s="T431">n:Time</ta>
            <ta e="T434" id="Seg_4023" s="T432">0.1.h:A</ta>
            <ta e="T439" id="Seg_4024" s="T438">np:Th</ta>
            <ta e="T441" id="Seg_4025" s="T440">0.1.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-ZhNA">
            <ta e="T4" id="Seg_4026" s="T3">np:S</ta>
            <ta e="T6" id="Seg_4027" s="T5">np:S</ta>
            <ta e="T7" id="Seg_4028" s="T6">adj:pred</ta>
            <ta e="T10" id="Seg_4029" s="T9">0.3.h:S ptcl:pred</ta>
            <ta e="T11" id="Seg_4030" s="T10">np.h:S</ta>
            <ta e="T13" id="Seg_4031" s="T12">ptcl:pred</ta>
            <ta e="T14" id="Seg_4032" s="T13">np.h:S</ta>
            <ta e="T16" id="Seg_4033" s="T15">ptcl:pred</ta>
            <ta e="T21" id="Seg_4034" s="T20">np.h:S</ta>
            <ta e="T25" id="Seg_4035" s="T24">v:pred</ta>
            <ta e="T30" id="Seg_4036" s="T25">s:adv</ta>
            <ta e="T40" id="Seg_4037" s="T37">s:temp</ta>
            <ta e="T42" id="Seg_4038" s="T41">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_4039" s="T42">np.h:S</ta>
            <ta e="T45" id="Seg_4040" s="T44">np.h:O</ta>
            <ta e="T46" id="Seg_4041" s="T45">v:pred</ta>
            <ta e="T50" id="Seg_4042" s="T48">s:temp</ta>
            <ta e="T54" id="Seg_4043" s="T53">0.3.h:S v:pred</ta>
            <ta e="T60" id="Seg_4044" s="T59">s:purp</ta>
            <ta e="T61" id="Seg_4045" s="T60">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_4046" s="T63">s:purp</ta>
            <ta e="T65" id="Seg_4047" s="T64">0.3.h:S v:pred</ta>
            <ta e="T69" id="Seg_4048" s="T68">s:purp</ta>
            <ta e="T70" id="Seg_4049" s="T69">0.3.h:S v:pred</ta>
            <ta e="T77" id="Seg_4050" s="T76">n:pred</ta>
            <ta e="T78" id="Seg_4051" s="T77">cop</ta>
            <ta e="T79" id="Seg_4052" s="T78">pro.h:S</ta>
            <ta e="T80" id="Seg_4053" s="T79">pro.h:S</ta>
            <ta e="T81" id="Seg_4054" s="T80">n:pred</ta>
            <ta e="T83" id="Seg_4055" s="T82">np.h:S</ta>
            <ta e="T85" id="Seg_4056" s="T84">ptcl:pred</ta>
            <ta e="T86" id="Seg_4057" s="T85">np.h:S</ta>
            <ta e="T87" id="Seg_4058" s="T86">ptcl:pred</ta>
            <ta e="T98" id="Seg_4059" s="T97">pro.h:S</ta>
            <ta e="T102" id="Seg_4060" s="T100">v:pred</ta>
            <ta e="T110" id="Seg_4061" s="T108">0.1.h:S v:pred</ta>
            <ta e="T114" id="Seg_4062" s="T113">np:S</ta>
            <ta e="T116" id="Seg_4063" s="T115">pro:S</ta>
            <ta e="T118" id="Seg_4064" s="T117">ptcl:pred</ta>
            <ta e="T120" id="Seg_4065" s="T119">cop</ta>
            <ta e="T130" id="Seg_4066" s="T129">0.1.h:S v:pred</ta>
            <ta e="T132" id="Seg_4067" s="T131">adj:pred</ta>
            <ta e="T133" id="Seg_4068" s="T132">0.1.h:S cop</ta>
            <ta e="T135" id="Seg_4069" s="T133">s:comp</ta>
            <ta e="T136" id="Seg_4070" s="T135">pro.h:S</ta>
            <ta e="T137" id="Seg_4071" s="T136">v:pred</ta>
            <ta e="T148" id="Seg_4072" s="T147">0.3.h:S v:pred</ta>
            <ta e="T151" id="Seg_4073" s="T150">0.3.h:S v:pred</ta>
            <ta e="T162" id="Seg_4074" s="T159">s:temp</ta>
            <ta e="T164" id="Seg_4075" s="T163">pro.h:S</ta>
            <ta e="T166" id="Seg_4076" s="T165">s:adv</ta>
            <ta e="T168" id="Seg_4077" s="T166">v:pred</ta>
            <ta e="T170" id="Seg_4078" s="T169">np:S</ta>
            <ta e="T172" id="Seg_4079" s="T170">adj:pred</ta>
            <ta e="T183" id="Seg_4080" s="T182">np:O</ta>
            <ta e="T185" id="Seg_4081" s="T183">0.1.h:S v:pred</ta>
            <ta e="T188" id="Seg_4082" s="T186">s:temp</ta>
            <ta e="T189" id="Seg_4083" s="T188">np.h:O</ta>
            <ta e="T190" id="Seg_4084" s="T189">0.1.h:S v:pred</ta>
            <ta e="T194" id="Seg_4085" s="T192">s:temp</ta>
            <ta e="T195" id="Seg_4086" s="T194">np:O</ta>
            <ta e="T196" id="Seg_4087" s="T195">0.1.h:S v:pred</ta>
            <ta e="T210" id="Seg_4088" s="T208">pro.h:S</ta>
            <ta e="T213" id="Seg_4089" s="T211">v:pred</ta>
            <ta e="T219" id="Seg_4090" s="T218">np.h:S</ta>
            <ta e="T221" id="Seg_4091" s="T220">ptcl:pred</ta>
            <ta e="T255" id="Seg_4092" s="T253">0.3:S v:pred</ta>
            <ta e="T256" id="Seg_4093" s="T255">0.3:S v:pred</ta>
            <ta e="T258" id="Seg_4094" s="T256">0.1.h:S v:pred</ta>
            <ta e="T264" id="Seg_4095" s="T262">s:adv</ta>
            <ta e="T268" id="Seg_4096" s="T267">v:pred</ta>
            <ta e="T269" id="Seg_4097" s="T268">pro.h:S</ta>
            <ta e="T272" id="Seg_4098" s="T271">n:pred</ta>
            <ta e="T274" id="Seg_4099" s="T273">cop</ta>
            <ta e="T275" id="Seg_4100" s="T274">pro.h:S</ta>
            <ta e="T276" id="Seg_4101" s="T275">np:O</ta>
            <ta e="T277" id="Seg_4102" s="T276">np:O</ta>
            <ta e="T278" id="Seg_4103" s="T277">0.1.h:S v:pred</ta>
            <ta e="T284" id="Seg_4104" s="T282">0.1.h:S v:pred</ta>
            <ta e="T297" id="Seg_4105" s="T293">s:purp</ta>
            <ta e="T300" id="Seg_4106" s="T299">0.3.h:S v:pred</ta>
            <ta e="T302" id="Seg_4107" s="T300">s:purp</ta>
            <ta e="T303" id="Seg_4108" s="T302">pro.h:S</ta>
            <ta e="T309" id="Seg_4109" s="T308">adj:pred</ta>
            <ta e="T314" id="Seg_4110" s="T312">0.3:S v:pred</ta>
            <ta e="T323" id="Seg_4111" s="T322">ptcl:pred</ta>
            <ta e="T324" id="Seg_4112" s="T323">0.3:S cop</ta>
            <ta e="T325" id="Seg_4113" s="T324">0.3:S ptcl:pred</ta>
            <ta e="T329" id="Seg_4114" s="T328">s:cond</ta>
            <ta e="T338" id="Seg_4115" s="T337">ptcl:pred</ta>
            <ta e="T339" id="Seg_4116" s="T338">0.3:S cop</ta>
            <ta e="T363" id="Seg_4117" s="T362">pro.h:S</ta>
            <ta e="T367" id="Seg_4118" s="T366">n:pred</ta>
            <ta e="T368" id="Seg_4119" s="T367">cop</ta>
            <ta e="T369" id="Seg_4120" s="T368">pro.h:S</ta>
            <ta e="T376" id="Seg_4121" s="T374">v:pred</ta>
            <ta e="T411" id="Seg_4122" s="T410">0.3.h:S v:pred</ta>
            <ta e="T415" id="Seg_4123" s="T413">s:temp</ta>
            <ta e="T417" id="Seg_4124" s="T415">0.1.h:S v:pred</ta>
            <ta e="T431" id="Seg_4125" s="T430">np:O</ta>
            <ta e="T434" id="Seg_4126" s="T432">0.1.h:S v:pred</ta>
            <ta e="T439" id="Seg_4127" s="T438">np:O</ta>
            <ta e="T441" id="Seg_4128" s="T440">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-ZhNA" />
         <annotation name="Top" tierref="Top-ZhNA" />
         <annotation name="Foc" tierref="Foc-ZhNA" />
         <annotation name="BOR" tierref="BOR-ZhNA">
            <ta e="T8" id="Seg_4129" s="T7">RUS:cult</ta>
            <ta e="T9" id="Seg_4130" s="T8">RUS:gram</ta>
            <ta e="T12" id="Seg_4131" s="T11">RUS:gram</ta>
            <ta e="T15" id="Seg_4132" s="T14">RUS:gram</ta>
            <ta e="T19" id="Seg_4133" s="T18">RUS:cult</ta>
            <ta e="T23" id="Seg_4134" s="T22">RUS:cult</ta>
            <ta e="T27" id="Seg_4135" s="T26">RUS:cult</ta>
            <ta e="T35" id="Seg_4136" s="T34">RUS:cult</ta>
            <ta e="T36" id="Seg_4137" s="T35">RUS:cult</ta>
            <ta e="T48" id="Seg_4138" s="T47">RUS:cult</ta>
            <ta e="T49" id="Seg_4139" s="T48">RUS:cult</ta>
            <ta e="T56" id="Seg_4140" s="T55">NGAN:cult</ta>
            <ta e="T57" id="Seg_4141" s="T56">NGAN:cult</ta>
            <ta e="T58" id="Seg_4142" s="T57">RUS:cult</ta>
            <ta e="T59" id="Seg_4143" s="T58">RUS:gram</ta>
            <ta e="T62" id="Seg_4144" s="T61">RUS:cult</ta>
            <ta e="T63" id="Seg_4145" s="T62">RUS:gram</ta>
            <ta e="T100" id="Seg_4146" s="T99">RUS:cult</ta>
            <ta e="T111" id="Seg_4147" s="T110">RUS:cult</ta>
            <ta e="T113" id="Seg_4148" s="T112">RUS:mod</ta>
            <ta e="T114" id="Seg_4149" s="T113">RUS:core</ta>
            <ta e="T146" id="Seg_4150" s="T144">RUS:cult</ta>
            <ta e="T150" id="Seg_4151" s="T149">RUS:cult</ta>
            <ta e="T215" id="Seg_4152" s="T214">RUS:gram</ta>
            <ta e="T216" id="Seg_4153" s="T215">RUS:mod</ta>
            <ta e="T218" id="Seg_4154" s="T217">RUS:mod</ta>
            <ta e="T256" id="Seg_4155" s="T255">RUS:cult</ta>
            <ta e="T260" id="Seg_4156" s="T259">RUS:cult</ta>
            <ta e="T262" id="Seg_4157" s="T261">RUS:cult</ta>
            <ta e="T270" id="Seg_4158" s="T269">RUS:cult</ta>
            <ta e="T272" id="Seg_4159" s="T271">RUS:cult</ta>
            <ta e="T276" id="Seg_4160" s="T275">RUS:cult</ta>
            <ta e="T282" id="Seg_4161" s="T281">RUS:cult</ta>
            <ta e="T296" id="Seg_4162" s="T295">RUS:cult</ta>
            <ta e="T299" id="Seg_4163" s="T298">RUS:cult</ta>
            <ta e="T309" id="Seg_4164" s="T308">RUS:cult</ta>
            <ta e="T320" id="Seg_4165" s="T318">RUS:cult</ta>
            <ta e="T321" id="Seg_4166" s="T320">RUS:cult</ta>
            <ta e="T322" id="Seg_4167" s="T321">RUS:gram</ta>
            <ta e="T333" id="Seg_4168" s="T332">RUS:cult</ta>
            <ta e="T336" id="Seg_4169" s="T335">RUS:cult</ta>
            <ta e="T349" id="Seg_4170" s="T347">RUS:cult</ta>
            <ta e="T350" id="Seg_4171" s="T349">YAK:cult</ta>
            <ta e="T359" id="Seg_4172" s="T358">RUS:cult</ta>
            <ta e="T360" id="Seg_4173" s="T359">YAK:cult</ta>
            <ta e="T367" id="Seg_4174" s="T366">YAK:cult</ta>
            <ta e="T372" id="Seg_4175" s="T371">RUS:core</ta>
            <ta e="T380" id="Seg_4176" s="T379">YAK:cult</ta>
            <ta e="T382" id="Seg_4177" s="T381">YAK:cult</ta>
            <ta e="T414" id="Seg_4178" s="T413">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-ZhNA">
            <ta e="T8" id="Seg_4179" s="T7">fortition medCdel</ta>
            <ta e="T19" id="Seg_4180" s="T18">fortition fortition</ta>
            <ta e="T27" id="Seg_4181" s="T26">Vsub medCdel Vsub Vsub lenition</ta>
            <ta e="T35" id="Seg_4182" s="T34">fortition fortition</ta>
            <ta e="T48" id="Seg_4183" s="T47">fortition medCdel</ta>
            <ta e="T49" id="Seg_4184" s="T48">fortition medCdel</ta>
            <ta e="T58" id="Seg_4185" s="T57">fortition</ta>
            <ta e="T100" id="Seg_4186" s="T99">fortition</ta>
            <ta e="T113" id="Seg_4187" s="T112">Vsub Csub Vsub</ta>
            <ta e="T114" id="Seg_4188" s="T113">Vsub Vsub</ta>
            <ta e="T260" id="Seg_4189" s="T259">fortition</ta>
            <ta e="T270" id="Seg_4190" s="T269">lenition</ta>
            <ta e="T282" id="Seg_4191" s="T281">medVins</ta>
            <ta e="T299" id="Seg_4192" s="T298">Vsub Vsub lenition</ta>
            <ta e="T320" id="Seg_4193" s="T318">medCdel</ta>
            <ta e="T336" id="Seg_4194" s="T335">lenition</ta>
            <ta e="T349" id="Seg_4195" s="T347">medCdel</ta>
            <ta e="T359" id="Seg_4196" s="T358">medCdel</ta>
            <ta e="T367" id="Seg_4197" s="T366">fortition</ta>
            <ta e="T372" id="Seg_4198" s="T371">Vsub fortition</ta>
            <ta e="T382" id="Seg_4199" s="T381">fortition</ta>
            <ta e="T414" id="Seg_4200" s="T413">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-ZhNA">
            <ta e="T8" id="Seg_4201" s="T7">indir:infl</ta>
            <ta e="T9" id="Seg_4202" s="T8">dir:bare</ta>
            <ta e="T12" id="Seg_4203" s="T11">dir:bare</ta>
            <ta e="T15" id="Seg_4204" s="T14">dir:bare</ta>
            <ta e="T19" id="Seg_4205" s="T18">dir:infl</ta>
            <ta e="T23" id="Seg_4206" s="T22">dir:infl</ta>
            <ta e="T27" id="Seg_4207" s="T26">dir:infl</ta>
            <ta e="T35" id="Seg_4208" s="T34">dir:bare</ta>
            <ta e="T36" id="Seg_4209" s="T35">dir:bare</ta>
            <ta e="T48" id="Seg_4210" s="T47">indir:infl</ta>
            <ta e="T49" id="Seg_4211" s="T48">indir:infl</ta>
            <ta e="T56" id="Seg_4212" s="T55">dir:infl</ta>
            <ta e="T57" id="Seg_4213" s="T56">dir:infl</ta>
            <ta e="T58" id="Seg_4214" s="T57">dir:infl</ta>
            <ta e="T59" id="Seg_4215" s="T58">dir:bare</ta>
            <ta e="T62" id="Seg_4216" s="T61">indir:infl</ta>
            <ta e="T63" id="Seg_4217" s="T62">dir:bare</ta>
            <ta e="T100" id="Seg_4218" s="T99">dir:infl</ta>
            <ta e="T111" id="Seg_4219" s="T110">dir:infl</ta>
            <ta e="T113" id="Seg_4220" s="T112">dir:bare</ta>
            <ta e="T114" id="Seg_4221" s="T113">dir:bare</ta>
            <ta e="T146" id="Seg_4222" s="T144">dir:infl</ta>
            <ta e="T150" id="Seg_4223" s="T149">dir:infl</ta>
            <ta e="T215" id="Seg_4224" s="T214">dir:bare</ta>
            <ta e="T216" id="Seg_4225" s="T215">dir:bare</ta>
            <ta e="T218" id="Seg_4226" s="T217">dir:bare</ta>
            <ta e="T256" id="Seg_4227" s="T255">indir:infl</ta>
            <ta e="T260" id="Seg_4228" s="T259">dir:infl</ta>
            <ta e="T262" id="Seg_4229" s="T261">dir:infl</ta>
            <ta e="T270" id="Seg_4230" s="T269">dir:infl</ta>
            <ta e="T272" id="Seg_4231" s="T271">dir:bare</ta>
            <ta e="T276" id="Seg_4232" s="T275">dir:bare</ta>
            <ta e="T282" id="Seg_4233" s="T281">dir:infl</ta>
            <ta e="T296" id="Seg_4234" s="T295">dir:infl</ta>
            <ta e="T299" id="Seg_4235" s="T298">dir:bare</ta>
            <ta e="T309" id="Seg_4236" s="T308">dir:infl</ta>
            <ta e="T320" id="Seg_4237" s="T318">indir:infl</ta>
            <ta e="T321" id="Seg_4238" s="T320">dir:infl</ta>
            <ta e="T322" id="Seg_4239" s="T321">dir:bare</ta>
            <ta e="T333" id="Seg_4240" s="T332">dir:infl</ta>
            <ta e="T336" id="Seg_4241" s="T335">dir:infl</ta>
            <ta e="T349" id="Seg_4242" s="T347">indir:bare</ta>
            <ta e="T350" id="Seg_4243" s="T349">dir:bare</ta>
            <ta e="T359" id="Seg_4244" s="T358">indir:bare</ta>
            <ta e="T360" id="Seg_4245" s="T359">dir:bare</ta>
            <ta e="T367" id="Seg_4246" s="T366">dir:infl</ta>
            <ta e="T372" id="Seg_4247" s="T371">dir:infl</ta>
            <ta e="T380" id="Seg_4248" s="T379">dir:infl</ta>
            <ta e="T382" id="Seg_4249" s="T381">dir:infl</ta>
            <ta e="T414" id="Seg_4250" s="T413">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS-ZhNA" />
         <annotation name="fe" tierref="fe-ZhNA">
            <ta e="T7" id="Seg_4251" s="T1">– My family, my own family is broad.</ta>
            <ta e="T16" id="Seg_4252" s="T7">There are some in Yakutsk, there are Russians and there are Dolgans.</ta>
            <ta e="T30" id="Seg_4253" s="T16">In the beginning my grandmother from Yakutia married my grandfather from Khatanga, when he(?) was working at these expeditions.</ta>
            <ta e="T37" id="Seg_4254" s="T34">– Darya Timofeevna.</ta>
            <ta e="T42" id="Seg_4255" s="T37">Then she went [away] and married the old man.</ta>
            <ta e="T50" id="Seg_4256" s="T42">The old man fathered three children in Yakutsk, after he had gone to Yakutsk.</ta>
            <ta e="T55" id="Seg_4257" s="T50">Then they came back her.</ta>
            <ta e="T56" id="Seg_4258" s="T55">To Popigaj.</ta>
            <ta e="T70" id="Seg_4259" s="T56">From Popigaj he went to Khatanga to work, he went to Kosistiy to work, he went everywhere to work.</ta>
            <ta e="T72" id="Seg_4260" s="T70">When I hear the stories.</ta>
            <ta e="T79" id="Seg_4261" s="T72">Then the son of the small daughter, that's me.</ta>
            <ta e="T82" id="Seg_4262" s="T79">We ourselves are eight people, from our parents.</ta>
            <ta e="T87" id="Seg_4263" s="T82">One half of us doesn't exist [ = has died], the other half exists.</ta>
            <ta e="T91" id="Seg_4264" s="T87">Well, like that.</ta>
            <ta e="T102" id="Seg_4265" s="T97">– I learned in Yakutia.</ta>
            <ta e="T111" id="Seg_4266" s="T102">In the nineties, in the nineties I was singing there, on a festival.</ta>
            <ta e="T122" id="Seg_4267" s="T111">There was still a place, something.</ta>
            <ta e="T129" id="Seg_4268" s="T124">– Yes, on Taimyr.</ta>
            <ta e="T130" id="Seg_4269" s="T129">I was singing.</ta>
            <ta e="T133" id="Seg_4270" s="T130">At that time I was small.</ta>
            <ta e="T137" id="Seg_4271" s="T133">I don't know how old I was.</ta>
            <ta e="T149" id="Seg_4272" s="T141">– Yes, from Dudinka, from here, they took.</ta>
            <ta e="T151" id="Seg_4273" s="T149">They took from the tuberculosis section.</ta>
            <ta e="T168" id="Seg_4274" s="T159">– When I was small, I myself composed and sang.</ta>
            <ta e="T172" id="Seg_4275" s="T168">That is all forgotten.</ta>
            <ta e="T185" id="Seg_4276" s="T176">– Yes, I was simply singing my speaking words.</ta>
            <ta e="T190" id="Seg_4277" s="T185">For example, when I see somebody, I besing that one.</ta>
            <ta e="T196" id="Seg_4278" s="T190">Something, when I besing a dog, then I besing a dog.</ta>
            <ta e="T197" id="Seg_4279" s="T196">Like that.</ta>
            <ta e="T217" id="Seg_4280" s="T208">– Nobody from my family sang, but maybe, this…</ta>
            <ta e="T225" id="Seg_4281" s="T217">Maybe, there are also shamans, they (…).</ta>
            <ta e="T229" id="Seg_4282" s="T227">– Yes.</ta>
            <ta e="T243" id="Seg_4283" s="T240">– Mm.</ta>
            <ta e="T260" id="Seg_4284" s="T250">– Hm, they stayed, they persist, I was working there in Yakutia.</ta>
            <ta e="T262" id="Seg_4285" s="T260">As a whatchamacallit, a technician.</ta>
            <ta e="T271" id="Seg_4286" s="T262">Working so, well, I went there, to concerts and so.</ta>
            <ta e="T275" id="Seg_4287" s="T271">I myself was like a (female) watchman.</ta>
            <ta e="T279" id="Seg_4288" s="T275">I am selling tickets and stuff at such.</ta>
            <ta e="T284" id="Seg_4289" s="T281">– I worked in a club.</ta>
            <ta e="T300" id="Seg_4290" s="T291">– Hm, for that I go onto the stage, they especially took [me(?)].</ta>
            <ta e="T302" id="Seg_4291" s="T300">For that I present myself.</ta>
            <ta e="T310" id="Seg_4292" s="T302">I myself have now six or four certificates.</ta>
            <ta e="T314" id="Seg_4293" s="T310">They are there.</ta>
            <ta e="T324" id="Seg_4294" s="T318">– In Yakutsk, from the education there were also some.</ta>
            <ta e="T332" id="Seg_4295" s="T324">They exist probably, somewhere, when one takes a look, then also from whatchamacallit…</ta>
            <ta e="T339" id="Seg_4296" s="T332">From culture, there must be also [one] from the head of administration.</ta>
            <ta e="T350" id="Seg_4297" s="T347">– The Olenyok district.</ta>
            <ta e="T368" id="Seg_4298" s="T358">– The Olenyok district, [that is] an island, I was in Khariyalaak at that time.</ta>
            <ta e="T376" id="Seg_4299" s="T368">They are living on that shore, opposite to each other.</ta>
            <ta e="T385" id="Seg_4300" s="T379">– Olenyok separately, Khariyalaak separately.</ta>
            <ta e="T391" id="Seg_4301" s="T387">– Yes, two whatchamacallit.</ta>
            <ta e="T411" id="Seg_4302" s="T407">– When I was small at all, they say.</ta>
            <ta e="T417" id="Seg_4303" s="T411">Well, since I was in kindergarten, I am singing.</ta>
            <ta e="T434" id="Seg_4304" s="T429">– The Russian ones I started to sing later on.</ta>
            <ta e="T438" id="Seg_4305" s="T434">Then on the Dolgan ones (?).</ta>
            <ta e="T441" id="Seg_4306" s="T438">I only sing Dolgan ones.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-ZhNA">
            <ta e="T7" id="Seg_4307" s="T1">– Meine Familie, meine eigene Familie ist breit.</ta>
            <ta e="T16" id="Seg_4308" s="T7">Es gibt welche in Jakutsk, es gibt Russen und es gibt Dolganen.</ta>
            <ta e="T30" id="Seg_4309" s="T16">Am Anfang heiratete meine Großmutter aus Jakutien meinen Großvater aus Xatanga, als er(?) bei diesen Expeditionen arbeitete.</ta>
            <ta e="T37" id="Seg_4310" s="T34">– Darja Timofejevna.</ta>
            <ta e="T42" id="Seg_4311" s="T37">Dann aber ging sie und heiratete den alten Mann.</ta>
            <ta e="T50" id="Seg_4312" s="T42">Der alte Mann brachte drei Mädchen in Jakutsk zur Welt, nachdem er nach Jakutsk gegangen war.</ta>
            <ta e="T55" id="Seg_4313" s="T50">Dann kamen sie hierher zurück.</ta>
            <ta e="T56" id="Seg_4314" s="T55">Nach Popigaj.</ta>
            <ta e="T70" id="Seg_4315" s="T56">Aus Popigaj ging er nach Chatanga arbeiten, er ging nach Kosistij arbeiten, er ging überall hin arbeiten.</ta>
            <ta e="T72" id="Seg_4316" s="T70">Wenn ich die Geschichten höre.</ta>
            <ta e="T79" id="Seg_4317" s="T72">Dann aber der Sohn der kleinen Tochter bin ich.</ta>
            <ta e="T82" id="Seg_4318" s="T79">Wir selbst sind acht bei unseren Eltern.</ta>
            <ta e="T87" id="Seg_4319" s="T82">Die eine Hälfte von uns gibt es nicht [=ist gestorben], die andere Hälfte gibt es.</ta>
            <ta e="T91" id="Seg_4320" s="T87">Nun, so.</ta>
            <ta e="T102" id="Seg_4321" s="T97">– Ich habe in Jakutien gelernt. </ta>
            <ta e="T111" id="Seg_4322" s="T102">In den Neunzigerjahren, in den Neunzigerjahren habe ich dort gesungen, auf einem Festival.</ta>
            <ta e="T122" id="Seg_4323" s="T111">Da gab es noch einen Platz, irgendetwas.</ta>
            <ta e="T129" id="Seg_4324" s="T124">– Ja, auf der Taimyr.</ta>
            <ta e="T130" id="Seg_4325" s="T129">Ich habe gesungen.</ta>
            <ta e="T133" id="Seg_4326" s="T130">Damals war ich klein.</ta>
            <ta e="T137" id="Seg_4327" s="T133">Wie alt ich bin [=war], weiß ich nicht.</ta>
            <ta e="T149" id="Seg_4328" s="T141">– Ja, aus Dudinka, von hier, wurde genommen.</ta>
            <ta e="T151" id="Seg_4329" s="T149">Aus der Tuberkuloseabteilung wurde genommen.</ta>
            <ta e="T168" id="Seg_4330" s="T159">– Als ich klein war, habe ich selbst verfasst und gesungen.</ta>
            <ta e="T172" id="Seg_4331" s="T168">Das ist alles vergessen.</ta>
            <ta e="T185" id="Seg_4332" s="T176">– Ja, einfach meine sprechenden Worte habe ich gesungen.</ta>
            <ta e="T190" id="Seg_4333" s="T185">Zum Beispiel, wenn ich jemanden sehe, dann besinge ich den.</ta>
            <ta e="T196" id="Seg_4334" s="T190">Irgendetwas, wenn ich einen Hund besinge, dann besinge ich einen Hund.</ta>
            <ta e="T197" id="Seg_4335" s="T196">So.</ta>
            <ta e="T217" id="Seg_4336" s="T208">– Niemand aus meiner Familie hat gesungen, aber vielleicht, dieser…</ta>
            <ta e="T225" id="Seg_4337" s="T217">Vielleicht gibt es auch Schamanen, die (…).</ta>
            <ta e="T229" id="Seg_4338" s="T227">– Ja.</ta>
            <ta e="T243" id="Seg_4339" s="T240">– Mm.</ta>
            <ta e="T260" id="Seg_4340" s="T250">– Hm, sie sind hängen geblieben, sie bestehen fort, ich habe da in Jakutien gearbeitet.</ta>
            <ta e="T262" id="Seg_4341" s="T260">Als Dings, als Techniker.</ta>
            <ta e="T271" id="Seg_4342" s="T262">So arbeitend, nun, ich ging dahin, auf Konzerte und sowas.</ta>
            <ta e="T275" id="Seg_4343" s="T271">Ich war selbst wie so eine Wachfrau.</ta>
            <ta e="T279" id="Seg_4344" s="T275">Tickets und so verkaufe ich auf solchen.</ta>
            <ta e="T284" id="Seg_4345" s="T281">– Ich habe im Klub gearbeitet.</ta>
            <ta e="T300" id="Seg_4346" s="T291">– Ja, damit ich auf die Bühne gehe, nahm man [mich(?)] besonders.</ta>
            <ta e="T302" id="Seg_4347" s="T300">Damit ich mich selber präsentiere.</ta>
            <ta e="T310" id="Seg_4348" s="T302">Ich selbst habe jetzt also sechs oder vier Urkunden.</ta>
            <ta e="T314" id="Seg_4349" s="T310">Sie sind dort.</ta>
            <ta e="T324" id="Seg_4350" s="T318">– In Jakutsk, von der Ausbildung gab es auch welche.</ta>
            <ta e="T332" id="Seg_4351" s="T324">Es gibt sie wohl, irgendwo, wenn man guckt, dann auch von Dings…</ta>
            <ta e="T339" id="Seg_4352" s="T332">Einige von der Kultur, vom Verwaltungschef muss es auch [eine] geben.</ta>
            <ta e="T350" id="Seg_4353" s="T347">– Der Kreis Olenjok.</ta>
            <ta e="T368" id="Seg_4354" s="T358">– Der Kreis Olenjok, [das ist] eine Insel, ich war damals da in Charijalaak.</ta>
            <ta e="T376" id="Seg_4355" s="T368">Sie leben auf jenem Ufer, einander gegenüber.</ta>
            <ta e="T385" id="Seg_4356" s="T379">– Olenjok einzeln, Charijalaak einzeln.</ta>
            <ta e="T391" id="Seg_4357" s="T387">– Ja, zwei Dings.</ta>
            <ta e="T411" id="Seg_4358" s="T407">– Als ich ganz klein war, sagen sie.</ta>
            <ta e="T417" id="Seg_4359" s="T411">Nun also, seit ich in den Kindergarten ging, singe ich.</ta>
            <ta e="T434" id="Seg_4360" s="T429">– Die Russischen habe ich später angefangen zu singen.</ta>
            <ta e="T438" id="Seg_4361" s="T434">Dann auf den Dolganischen (?).</ta>
            <ta e="T441" id="Seg_4362" s="T438">Ich singe nur Dolganische.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-ZhNA" />
         <annotation name="ltr" tierref="ltr-ZhNA">
            <ta e="T7" id="Seg_4363" s="T1">Ж: Мои вот предки, моя родова широкая.</ta>
            <ta e="T16" id="Seg_4364" s="T7">И в Якутске есть, и русские есть, и долганы есть.</ta>
            <ta e="T30" id="Seg_4365" s="T16">В самом начале из Якутии моя бабушка, значит, из Хатанги за деда вышла замуж, это в экспедиции работая.</ta>
            <ta e="T37" id="Seg_4366" s="T34">Ж: Тимофеева Дарья.</ta>
            <ta e="T42" id="Seg_4367" s="T37">Потом, значит, уехав, вышла замуж за деда.</ta>
            <ta e="T50" id="Seg_4368" s="T42">Старик тот троих дочерей родил в Якутске, уехав в Якутск.</ta>
            <ta e="T55" id="Seg_4369" s="T50">Потом обратно вернулись (переехали) сюда.</ta>
            <ta e="T56" id="Seg_4370" s="T55">В Попигай.</ta>
            <ta e="T70" id="Seg_4371" s="T56">Из Попигая и в Хатангу ездил на заработки (на работу) и в Косистый ездил на работу, везде (во всех землях) работал.</ta>
            <ta e="T72" id="Seg_4372" s="T70">Если послушать из рассказов.</ta>
            <ta e="T79" id="Seg_4373" s="T72">Из этого получается, я являюсь сыном младшей дочери.</ta>
            <ta e="T82" id="Seg_4374" s="T79">Нас самих восемь у родителей наших.</ta>
            <ta e="T87" id="Seg_4375" s="T82">Половины-то нет, половина нас есть.</ta>
            <ta e="T91" id="Seg_4376" s="T87">Вот так вот.</ta>
            <ta e="T102" id="Seg_4377" s="T97">Ж: Я, значит, в Якутии учился.</ta>
            <ta e="T111" id="Seg_4378" s="T102">В девяностых годах, в девяностых годах вот здесь пел, на фестивале.</ta>
            <ta e="T122" id="Seg_4379" s="T111">Тогда ещё место какое-то было по-моему.</ta>
            <ta e="T129" id="Seg_4380" s="T124">Ж: Да, на Таймыре.</ta>
            <ta e="T130" id="Seg_4381" s="T129">Спел.</ta>
            <ta e="T133" id="Seg_4382" s="T130">Тогда маленький был.</ta>
            <ta e="T137" id="Seg_4383" s="T133">Сколько лет было не помню.</ta>
            <ta e="T149" id="Seg_4384" s="T141">Ж: Да, из Дудинки, отсюда, забрали вот.</ta>
            <ta e="T151" id="Seg_4385" s="T149">Из туботделения забирали.</ta>
            <ta e="T168" id="Seg_4386" s="T159">Ж: Когда маленький был, сам воспевал и пел.</ta>
            <ta e="T172" id="Seg_4387" s="T168">Это всё позабылось.</ta>
            <ta e="T185" id="Seg_4388" s="T176">Ж: Да, тут же… говорящие мною слова пел.</ta>
            <ta e="T190" id="Seg_4389" s="T185">Допустим, если человека увижу, человека воспеваю.</ta>
            <ta e="T196" id="Seg_4390" s="T190">Если собаку увижу то про собаку пою.</ta>
            <ta e="T197" id="Seg_4391" s="T196">Вот так вот.</ta>
            <ta e="T217" id="Seg_4392" s="T208">Ж: Никто не пел их родных, может, это…</ta>
            <ta e="T225" id="Seg_4393" s="T217">Может, шаманы тоже были, наверное, те.</ta>
            <ta e="T229" id="Seg_4394" s="T227">Ж: Да.</ta>
            <ta e="T243" id="Seg_4395" s="T240">Ж: Да.</ta>
            <ta e="T260" id="Seg_4396" s="T250">Ж: Да, запомнились, настаивали, работал тогда в Якутии.</ta>
            <ta e="T262" id="Seg_4397" s="T260">Этим, техработником.</ta>
            <ta e="T271" id="Seg_4398" s="T262">Так работая, ну, сам туда ходил, на концерты и тому подобно.</ta>
            <ta e="T275" id="Seg_4399" s="T271">Вроде вахтёрши был сам.</ta>
            <ta e="T279" id="Seg_4400" s="T275">Билеты ещё что-то продавал вот на таких.</ta>
            <ta e="T284" id="Seg_4401" s="T281">Ж: В клубе работал.</ta>
            <ta e="T300" id="Seg_4402" s="T291">Ж: Да, на сцену чтобы мне выходить, специально взяли.</ta>
            <ta e="T302" id="Seg_4403" s="T300">Чтобы себя показывал.</ta>
            <ta e="T310" id="Seg_4404" s="T302">У меня, значит, или шесть, или четыре грамоты есть сейчас.</ta>
            <ta e="T314" id="Seg_4405" s="T310">Там находятся.</ta>
            <ta e="T324" id="Seg_4406" s="T318">Ж: В Якутске, oт образования тоже были.</ta>
            <ta e="T332" id="Seg_4407" s="T324">Может, есть или где, если посмотреть, потом оттуда тоже…</ta>
            <ta e="T339" id="Seg_4408" s="T332">От культуры несколько, от главы администрации тоже должна быть.</ta>
            <ta e="T350" id="Seg_4409" s="T347">Ж: Оленёкский улус.</ta>
            <ta e="T368" id="Seg_4410" s="T358">Ж: Оленёкский улус, oстров же, а я в то время был в Карыйалаке.</ta>
            <ta e="T376" id="Seg_4411" s="T368">Они же на том берегу так, напротив друг друга живут. </ta>
            <ta e="T385" id="Seg_4412" s="T379">Ж: Оленёк отдельно, Карыйалак отдельно.</ta>
            <ta e="T391" id="Seg_4413" s="T387">Ж: Да, два.</ta>
            <ta e="T411" id="Seg_4414" s="T407">Ж: Вообще, с раннего детства, говорят.</ta>
            <ta e="T417" id="Seg_4415" s="T411">Вот же, в детский сад ещё когда ходил пою, оказывается.</ta>
            <ta e="T434" id="Seg_4416" s="T429">Ж: Эти русские позже начал петь.</ta>
            <ta e="T438" id="Seg_4417" s="T434">Потом вот на долганских.</ta>
            <ta e="T441" id="Seg_4418" s="T438">Долганские только пою.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-ZhNA">
            <ta e="T7" id="Seg_4419" s="T1">[DCh]: "kieŋ" may also be the name of the lineage.</ta>
            <ta e="T151" id="Seg_4420" s="T149">[DCh]: "туботделение" is an acronyme standing for "туберкулезное отделение" 'section for tuberculosis'.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KuNS"
                      id="tx-KuNS"
                      speaker="KuNS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KuNS">
            <ts e="T34" id="Seg_4421" n="sc" s="T29">
               <ts e="T34" id="Seg_4423" n="HIAT:u" s="T29">
                  <nts id="Seg_4424" n="HIAT:ip">–</nts>
                  <nts id="Seg_4425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_4427" n="HIAT:w" s="T29">Ebeŋ</ts>
                  <nts id="Seg_4428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_4430" n="HIAT:w" s="T31">aːta</ts>
                  <nts id="Seg_4431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_4433" n="HIAT:w" s="T32">kim</ts>
                  <nts id="Seg_4434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_4436" n="HIAT:w" s="T33">etej</ts>
                  <nts id="Seg_4437" n="HIAT:ip">?</nts>
                  <nts id="Seg_4438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T97" id="Seg_4439" n="sc" s="T90">
               <ts e="T97" id="Seg_4441" n="HIAT:u" s="T90">
                  <nts id="Seg_4442" n="HIAT:ip">–</nts>
                  <nts id="Seg_4443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_4445" n="HIAT:w" s="T90">Kanna</ts>
                  <nts id="Seg_4446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_4448" n="HIAT:w" s="T92">ü͡öremmikkinij</ts>
                  <nts id="Seg_4449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_4451" n="HIAT:w" s="T93">ki͡e</ts>
                  <nts id="Seg_4452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_4454" n="HIAT:w" s="T94">usku͡olanɨ</ts>
                  <nts id="Seg_4455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_4457" n="HIAT:w" s="T95">itte</ts>
                  <nts id="Seg_4458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_4460" n="HIAT:w" s="T96">büterbitiŋ</ts>
                  <nts id="Seg_4461" n="HIAT:ip">?</nts>
                  <nts id="Seg_4462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T128" id="Seg_4463" n="sc" s="T121">
               <ts e="T128" id="Seg_4465" n="HIAT:u" s="T121">
                  <nts id="Seg_4466" n="HIAT:ip">–</nts>
                  <nts id="Seg_4467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_4469" n="HIAT:w" s="T121">Manna</ts>
                  <nts id="Seg_4470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_4472" n="HIAT:w" s="T123">gɨnagɨn</ts>
                  <nts id="Seg_4473" n="HIAT:ip">,</nts>
                  <nts id="Seg_4474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_4476" n="HIAT:w" s="T125">Tajmɨːrga</ts>
                  <nts id="Seg_4477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_4479" n="HIAT:w" s="T126">gɨnagɨn</ts>
                  <nts id="Seg_4480" n="HIAT:ip">?</nts>
                  <nts id="Seg_4481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T145" id="Seg_4482" n="sc" s="T138">
               <ts e="T145" id="Seg_4484" n="HIAT:u" s="T138">
                  <nts id="Seg_4485" n="HIAT:ip">–</nts>
                  <nts id="Seg_4486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_4488" n="HIAT:w" s="T138">Dudʼinkaga</ts>
                  <nts id="Seg_4489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_4491" n="HIAT:w" s="T139">emi͡e</ts>
                  <nts id="Seg_4492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_4494" n="HIAT:w" s="T140">egelbit</ts>
                  <nts id="Seg_4495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_4497" n="HIAT:w" s="T142">etilere</ts>
                  <nts id="Seg_4498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4499" n="HIAT:ip">(</nts>
                  <nts id="Seg_4500" n="HIAT:ip">(</nts>
                  <ats e="T145" id="Seg_4501" n="HIAT:non-pho" s="T143">…</ats>
                  <nts id="Seg_4502" n="HIAT:ip">)</nts>
                  <nts id="Seg_4503" n="HIAT:ip">)</nts>
                  <nts id="Seg_4504" n="HIAT:ip">.</nts>
                  <nts id="Seg_4505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T160" id="Seg_4506" n="sc" s="T152">
               <ts e="T160" id="Seg_4508" n="HIAT:u" s="T152">
                  <nts id="Seg_4509" n="HIAT:ip">–</nts>
                  <nts id="Seg_4510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_4512" n="HIAT:w" s="T152">Oččogo</ts>
                  <nts id="Seg_4513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_4515" n="HIAT:w" s="T153">tu͡ok</ts>
                  <nts id="Seg_4516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_4518" n="HIAT:w" s="T154">ɨrɨ͡alarɨn</ts>
                  <nts id="Seg_4519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_4521" n="HIAT:w" s="T155">ɨllɨːr</ts>
                  <nts id="Seg_4522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_4524" n="HIAT:w" s="T156">etiŋ</ts>
                  <nts id="Seg_4525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_4527" n="HIAT:w" s="T157">küččügüj</ts>
                  <nts id="Seg_4528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_4530" n="HIAT:w" s="T158">erdekkinen</ts>
                  <nts id="Seg_4531" n="HIAT:ip">?</nts>
                  <nts id="Seg_4532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T177" id="Seg_4533" n="sc" s="T171">
               <ts e="T177" id="Seg_4535" n="HIAT:u" s="T171">
                  <nts id="Seg_4536" n="HIAT:ip">–</nts>
                  <nts id="Seg_4537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_4539" n="HIAT:w" s="T171">Haŋalarɨ</ts>
                  <nts id="Seg_4540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_4542" n="HIAT:w" s="T173">hubu</ts>
                  <nts id="Seg_4543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_4545" n="HIAT:w" s="T174">taharaŋŋɨn</ts>
                  <nts id="Seg_4546" n="HIAT:ip">,</nts>
                  <nts id="Seg_4547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_4549" n="HIAT:w" s="T175">eː</ts>
                  <nts id="Seg_4550" n="HIAT:ip">?</nts>
                  <nts id="Seg_4551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T209" id="Seg_4552" n="sc" s="T197">
               <ts e="T209" id="Seg_4554" n="HIAT:u" s="T197">
                  <nts id="Seg_4555" n="HIAT:ip">–</nts>
                  <nts id="Seg_4556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_4558" n="HIAT:w" s="T197">Kimi</ts>
                  <nts id="Seg_4559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_4561" n="HIAT:w" s="T198">isten</ts>
                  <nts id="Seg_4562" n="HIAT:ip">,</nts>
                  <nts id="Seg_4563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_4565" n="HIAT:w" s="T199">ebeŋ</ts>
                  <nts id="Seg_4566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_4568" n="HIAT:w" s="T200">itte</ts>
                  <nts id="Seg_4569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_4571" n="HIAT:w" s="T201">ɨllaːččɨ</ts>
                  <nts id="Seg_4572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_4574" n="HIAT:w" s="T202">ete</ts>
                  <nts id="Seg_4575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_4577" n="HIAT:w" s="T203">duː</ts>
                  <nts id="Seg_4578" n="HIAT:ip">,</nts>
                  <nts id="Seg_4579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_4581" n="HIAT:w" s="T204">kergennergitten</ts>
                  <nts id="Seg_4582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_4584" n="HIAT:w" s="T205">kim</ts>
                  <nts id="Seg_4585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_4587" n="HIAT:w" s="T206">ɨllaːččɨ</ts>
                  <nts id="Seg_4588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_4590" n="HIAT:w" s="T207">etej</ts>
                  <nts id="Seg_4591" n="HIAT:ip">?</nts>
                  <nts id="Seg_4592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T228" id="Seg_4593" n="sc" s="T224">
               <ts e="T228" id="Seg_4595" n="HIAT:u" s="T224">
                  <nts id="Seg_4596" n="HIAT:ip">–</nts>
                  <nts id="Seg_4597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_4599" n="HIAT:w" s="T224">ɨraːk</ts>
                  <nts id="Seg_4600" n="HIAT:ip">,</nts>
                  <nts id="Seg_4601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_4603" n="HIAT:w" s="T226">heː</ts>
                  <nts id="Seg_4604" n="HIAT:ip">?</nts>
                  <nts id="Seg_4605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T254" id="Seg_4606" n="sc" s="T229">
               <ts e="T241" id="Seg_4608" n="HIAT:u" s="T229">
                  <nts id="Seg_4609" n="HIAT:ip">–</nts>
                  <nts id="Seg_4610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_4612" n="HIAT:w" s="T229">Bu</ts>
                  <nts id="Seg_4613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_4615" n="HIAT:w" s="T230">ke</ts>
                  <nts id="Seg_4616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_4618" n="HIAT:w" s="T231">tak-ta</ts>
                  <nts id="Seg_4619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_4621" n="HIAT:w" s="T232">ɨrɨ͡alargɨn</ts>
                  <nts id="Seg_4622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_4624" n="HIAT:w" s="T233">jakuttar</ts>
                  <nts id="Seg_4625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_4627" n="HIAT:w" s="T234">ɨrɨ͡alarɨnan</ts>
                  <nts id="Seg_4628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_4630" n="HIAT:w" s="T235">taːk</ts>
                  <nts id="Seg_4631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_4633" n="HIAT:w" s="T236">daː</ts>
                  <nts id="Seg_4634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_4636" n="HIAT:w" s="T237">tardagɨn</ts>
                  <nts id="Seg_4637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_4639" n="HIAT:w" s="T238">ebit</ts>
                  <nts id="Seg_4640" n="HIAT:ip">,</nts>
                  <nts id="Seg_4641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_4643" n="HIAT:w" s="T239">eː</ts>
                  <nts id="Seg_4644" n="HIAT:ip">?</nts>
                  <nts id="Seg_4645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_4647" n="HIAT:u" s="T241">
                  <nts id="Seg_4648" n="HIAT:ip">–</nts>
                  <nts id="Seg_4649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_4651" n="HIAT:w" s="T241">Hin</ts>
                  <nts id="Seg_4652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_4654" n="HIAT:w" s="T242">biːr</ts>
                  <nts id="Seg_4655" n="HIAT:ip">…</nts>
                  <nts id="Seg_4656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_4658" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_4660" n="HIAT:w" s="T244">Ol</ts>
                  <nts id="Seg_4661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_4663" n="HIAT:w" s="T245">menʼiːger</ts>
                  <nts id="Seg_4664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4665" n="HIAT:ip">(</nts>
                  <ts e="T247" id="Seg_4667" n="HIAT:w" s="T246">katɨl-</ts>
                  <nts id="Seg_4668" n="HIAT:ip">)</nts>
                  <nts id="Seg_4669" n="HIAT:ip">,</nts>
                  <nts id="Seg_4670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_4672" n="HIAT:w" s="T247">e</ts>
                  <nts id="Seg_4673" n="HIAT:ip">,</nts>
                  <nts id="Seg_4674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_4676" n="HIAT:w" s="T248">katallɨbɨttar</ts>
                  <nts id="Seg_4677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_4679" n="HIAT:w" s="T249">bu͡o</ts>
                  <nts id="Seg_4680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_4682" n="HIAT:w" s="T251">ginner</ts>
                  <nts id="Seg_4683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_4685" n="HIAT:w" s="T252">ɨrɨ͡alara</ts>
                  <nts id="Seg_4686" n="HIAT:ip">.</nts>
                  <nts id="Seg_4687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T283" id="Seg_4688" n="sc" s="T279">
               <ts e="T283" id="Seg_4690" n="HIAT:u" s="T279">
                  <nts id="Seg_4691" n="HIAT:ip">–</nts>
                  <nts id="Seg_4692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_4694" n="HIAT:w" s="T279">Kluːbka</ts>
                  <nts id="Seg_4695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_4697" n="HIAT:w" s="T280">üleleːbitiŋ</ts>
                  <nts id="Seg_4698" n="HIAT:ip">?</nts>
                  <nts id="Seg_4699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T294" id="Seg_4700" n="sc" s="T284">
               <ts e="T294" id="Seg_4702" n="HIAT:u" s="T284">
                  <nts id="Seg_4703" n="HIAT:ip">–</nts>
                  <nts id="Seg_4704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_4706" n="HIAT:w" s="T284">Onton</ts>
                  <nts id="Seg_4707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_4709" n="HIAT:w" s="T285">kü͡ömejdeːkkin</ts>
                  <nts id="Seg_4710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_4712" n="HIAT:w" s="T286">körönnör</ts>
                  <nts id="Seg_4713" n="HIAT:ip">,</nts>
                  <nts id="Seg_4714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_4716" n="HIAT:w" s="T287">senata</ts>
                  <nts id="Seg_4717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_4719" n="HIAT:w" s="T288">itte</ts>
                  <nts id="Seg_4720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_4722" n="HIAT:w" s="T289">taksar</ts>
                  <nts id="Seg_4723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_4725" n="HIAT:w" s="T290">bu͡olluŋ</ts>
                  <nts id="Seg_4726" n="HIAT:ip">,</nts>
                  <nts id="Seg_4727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_4729" n="HIAT:w" s="T292">eː</ts>
                  <nts id="Seg_4730" n="HIAT:ip">?</nts>
                  <nts id="Seg_4731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T319" id="Seg_4732" n="sc" s="T313">
               <ts e="T319" id="Seg_4734" n="HIAT:u" s="T313">
                  <nts id="Seg_4735" n="HIAT:ip">–</nts>
                  <nts id="Seg_4736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_4738" n="HIAT:w" s="T313">Jakutskajga</ts>
                  <nts id="Seg_4739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_4741" n="HIAT:w" s="T315">ɨlbɨt</ts>
                  <nts id="Seg_4742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_4744" n="HIAT:w" s="T316">gramatalarɨŋ</ts>
                  <nts id="Seg_4745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_4747" n="HIAT:w" s="T317">du͡o</ts>
                  <nts id="Seg_4748" n="HIAT:ip">?</nts>
                  <nts id="Seg_4749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T348" id="Seg_4750" n="sc" s="T339">
               <ts e="T348" id="Seg_4752" n="HIAT:u" s="T339">
                  <nts id="Seg_4753" n="HIAT:ip">–</nts>
                  <nts id="Seg_4754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_4756" n="HIAT:w" s="T339">Kannɨk</ts>
                  <nts id="Seg_4757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_4759" n="HIAT:w" s="T340">rajon</ts>
                  <nts id="Seg_4760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_4762" n="HIAT:w" s="T341">ete</ts>
                  <nts id="Seg_4763" n="HIAT:ip">,</nts>
                  <nts id="Seg_4764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_4766" n="HIAT:w" s="T342">diːgin</ts>
                  <nts id="Seg_4767" n="HIAT:ip">,</nts>
                  <nts id="Seg_4768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_4770" n="HIAT:w" s="T343">kannɨk</ts>
                  <nts id="Seg_4771" n="HIAT:ip">,</nts>
                  <nts id="Seg_4772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_4774" n="HIAT:w" s="T344">uluːstar</ts>
                  <nts id="Seg_4775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_4777" n="HIAT:w" s="T345">bu͡o</ts>
                  <nts id="Seg_4778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_4780" n="HIAT:w" s="T346">onno</ts>
                  <nts id="Seg_4781" n="HIAT:ip">?</nts>
                  <nts id="Seg_4782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T358" id="Seg_4783" n="sc" s="T351">
               <ts e="T358" id="Seg_4785" n="HIAT:u" s="T351">
                  <nts id="Seg_4786" n="HIAT:ip">–</nts>
                  <nts id="Seg_4787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_4789" n="HIAT:w" s="T351">Kuččuguj</ts>
                  <nts id="Seg_4790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_4792" n="HIAT:w" s="T352">tuguj</ts>
                  <nts id="Seg_4793" n="HIAT:ip">,</nts>
                  <nts id="Seg_4794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_4796" n="HIAT:w" s="T353">pasʼolok</ts>
                  <nts id="Seg_4797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_4799" n="HIAT:w" s="T354">duː</ts>
                  <nts id="Seg_4800" n="HIAT:ip">,</nts>
                  <nts id="Seg_4801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_4803" n="HIAT:w" s="T355">gorat</ts>
                  <nts id="Seg_4804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_4806" n="HIAT:w" s="T356">ete</ts>
                  <nts id="Seg_4807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_4809" n="HIAT:w" s="T357">duː</ts>
                  <nts id="Seg_4810" n="HIAT:ip">?</nts>
                  <nts id="Seg_4811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T379" id="Seg_4812" n="sc" s="T375">
               <ts e="T379" id="Seg_4814" n="HIAT:u" s="T375">
                  <nts id="Seg_4815" n="HIAT:ip">–</nts>
                  <nts id="Seg_4816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_4818" n="HIAT:w" s="T375">Kimneːgi</ts>
                  <nts id="Seg_4819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_4821" n="HIAT:w" s="T377">gɨnagɨn</ts>
                  <nts id="Seg_4822" n="HIAT:ip">,</nts>
                  <nts id="Seg_4823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_4825" n="HIAT:w" s="T378">giniler</ts>
                  <nts id="Seg_4826" n="HIAT:ip">?</nts>
                  <nts id="Seg_4827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T388" id="Seg_4828" n="sc" s="T383">
               <ts e="T388" id="Seg_4830" n="HIAT:u" s="T383">
                  <nts id="Seg_4831" n="HIAT:ip">–</nts>
                  <nts id="Seg_4832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_4834" n="HIAT:w" s="T383">Aː</ts>
                  <nts id="Seg_4835" n="HIAT:ip">,</nts>
                  <nts id="Seg_4836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_4838" n="HIAT:w" s="T384">iti</ts>
                  <nts id="Seg_4839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_4841" n="HIAT:w" s="T386">ikki</ts>
                  <nts id="Seg_4842" n="HIAT:ip">.</nts>
                  <nts id="Seg_4843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T408" id="Seg_4844" n="sc" s="T391">
               <ts e="T395" id="Seg_4846" n="HIAT:u" s="T391">
                  <nts id="Seg_4847" n="HIAT:ip">–</nts>
                  <nts id="Seg_4848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_4850" n="HIAT:w" s="T391">Ikki</ts>
                  <nts id="Seg_4851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_4853" n="HIAT:w" s="T392">pasʼolok</ts>
                  <nts id="Seg_4854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_4856" n="HIAT:w" s="T393">kördükter</ts>
                  <nts id="Seg_4857" n="HIAT:ip">,</nts>
                  <nts id="Seg_4858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_4860" n="HIAT:w" s="T394">eː</ts>
                  <nts id="Seg_4861" n="HIAT:ip">?</nts>
                  <nts id="Seg_4862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_4864" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_4866" n="HIAT:w" s="T395">Bu</ts>
                  <nts id="Seg_4867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_4869" n="HIAT:w" s="T396">ke</ts>
                  <nts id="Seg_4870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_4872" n="HIAT:w" s="T397">tak-ta</ts>
                  <nts id="Seg_4873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_4875" n="HIAT:w" s="T398">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_4876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_4878" n="HIAT:w" s="T399">ɨllɨːbɨn</ts>
                  <nts id="Seg_4879" n="HIAT:ip">,</nts>
                  <nts id="Seg_4880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_4882" n="HIAT:w" s="T400">diːgin</ts>
                  <nts id="Seg_4883" n="HIAT:ip">,</nts>
                  <nts id="Seg_4884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_4886" n="HIAT:w" s="T401">kaččattan</ts>
                  <nts id="Seg_4887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_4889" n="HIAT:w" s="T402">tak-ta</ts>
                  <nts id="Seg_4890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_4892" n="HIAT:w" s="T403">ileliː</ts>
                  <nts id="Seg_4893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_4895" n="HIAT:w" s="T404">ɨllɨːr</ts>
                  <nts id="Seg_4896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_4898" n="HIAT:w" s="T405">bu͡olbukkɨnɨj</ts>
                  <nts id="Seg_4899" n="HIAT:ip">,</nts>
                  <nts id="Seg_4900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_4902" n="HIAT:w" s="T406">ki͡e</ts>
                  <nts id="Seg_4903" n="HIAT:ip">?</nts>
                  <nts id="Seg_4904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T429" id="Seg_4905" n="sc" s="T417">
               <ts e="T419" id="Seg_4907" n="HIAT:u" s="T417">
                  <nts id="Seg_4908" n="HIAT:ip">–</nts>
                  <nts id="Seg_4909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_4911" n="HIAT:w" s="T417">Bejeŋ</ts>
                  <nts id="Seg_4912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_4914" n="HIAT:w" s="T418">ɨllaːn</ts>
                  <nts id="Seg_4915" n="HIAT:ip">…</nts>
                  <nts id="Seg_4916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T429" id="Seg_4918" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_4920" n="HIAT:w" s="T419">Nʼuːčča</ts>
                  <nts id="Seg_4921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_4923" n="HIAT:w" s="T420">itte</ts>
                  <nts id="Seg_4924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_4926" n="HIAT:w" s="T421">ɨrɨ͡alarɨn</ts>
                  <nts id="Seg_4927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_4929" n="HIAT:w" s="T422">ɨllaːččɨ</ts>
                  <nts id="Seg_4930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_4932" n="HIAT:w" s="T423">etiŋ</ts>
                  <nts id="Seg_4933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_4935" n="HIAT:w" s="T424">oččogo</ts>
                  <nts id="Seg_4936" n="HIAT:ip">,</nts>
                  <nts id="Seg_4937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_4939" n="HIAT:w" s="T425">eː</ts>
                  <nts id="Seg_4940" n="HIAT:ip">,</nts>
                  <nts id="Seg_4941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_4943" n="HIAT:w" s="T426">duː</ts>
                  <nts id="Seg_4944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_4946" n="HIAT:w" s="T427">hakalɨː</ts>
                  <nts id="Seg_4947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_4949" n="HIAT:w" s="T428">ere</ts>
                  <nts id="Seg_4950" n="HIAT:ip">?</nts>
                  <nts id="Seg_4951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KuNS">
            <ts e="T34" id="Seg_4952" n="sc" s="T29">
               <ts e="T31" id="Seg_4954" n="e" s="T29">– Ebeŋ </ts>
               <ts e="T32" id="Seg_4956" n="e" s="T31">aːta </ts>
               <ts e="T33" id="Seg_4958" n="e" s="T32">kim </ts>
               <ts e="T34" id="Seg_4960" n="e" s="T33">etej? </ts>
            </ts>
            <ts e="T97" id="Seg_4961" n="sc" s="T90">
               <ts e="T92" id="Seg_4963" n="e" s="T90">– Kanna </ts>
               <ts e="T93" id="Seg_4965" n="e" s="T92">ü͡öremmikkinij </ts>
               <ts e="T94" id="Seg_4967" n="e" s="T93">ki͡e </ts>
               <ts e="T95" id="Seg_4969" n="e" s="T94">usku͡olanɨ </ts>
               <ts e="T96" id="Seg_4971" n="e" s="T95">itte </ts>
               <ts e="T97" id="Seg_4973" n="e" s="T96">büterbitiŋ? </ts>
            </ts>
            <ts e="T128" id="Seg_4974" n="sc" s="T121">
               <ts e="T123" id="Seg_4976" n="e" s="T121">– Manna </ts>
               <ts e="T125" id="Seg_4978" n="e" s="T123">gɨnagɨn, </ts>
               <ts e="T126" id="Seg_4980" n="e" s="T125">Tajmɨːrga </ts>
               <ts e="T128" id="Seg_4982" n="e" s="T126">gɨnagɨn? </ts>
            </ts>
            <ts e="T145" id="Seg_4983" n="sc" s="T138">
               <ts e="T139" id="Seg_4985" n="e" s="T138">– Dudʼinkaga </ts>
               <ts e="T140" id="Seg_4987" n="e" s="T139">emi͡e </ts>
               <ts e="T142" id="Seg_4989" n="e" s="T140">egelbit </ts>
               <ts e="T143" id="Seg_4991" n="e" s="T142">etilere </ts>
               <ts e="T145" id="Seg_4993" n="e" s="T143">((…)). </ts>
            </ts>
            <ts e="T160" id="Seg_4994" n="sc" s="T152">
               <ts e="T153" id="Seg_4996" n="e" s="T152">– Oččogo </ts>
               <ts e="T154" id="Seg_4998" n="e" s="T153">tu͡ok </ts>
               <ts e="T155" id="Seg_5000" n="e" s="T154">ɨrɨ͡alarɨn </ts>
               <ts e="T156" id="Seg_5002" n="e" s="T155">ɨllɨːr </ts>
               <ts e="T157" id="Seg_5004" n="e" s="T156">etiŋ </ts>
               <ts e="T158" id="Seg_5006" n="e" s="T157">küččügüj </ts>
               <ts e="T160" id="Seg_5008" n="e" s="T158">erdekkinen? </ts>
            </ts>
            <ts e="T177" id="Seg_5009" n="sc" s="T171">
               <ts e="T173" id="Seg_5011" n="e" s="T171">– Haŋalarɨ </ts>
               <ts e="T174" id="Seg_5013" n="e" s="T173">hubu </ts>
               <ts e="T175" id="Seg_5015" n="e" s="T174">taharaŋŋɨn, </ts>
               <ts e="T177" id="Seg_5017" n="e" s="T175">eː? </ts>
            </ts>
            <ts e="T209" id="Seg_5018" n="sc" s="T197">
               <ts e="T198" id="Seg_5020" n="e" s="T197">– Kimi </ts>
               <ts e="T199" id="Seg_5022" n="e" s="T198">isten, </ts>
               <ts e="T200" id="Seg_5024" n="e" s="T199">ebeŋ </ts>
               <ts e="T201" id="Seg_5026" n="e" s="T200">itte </ts>
               <ts e="T202" id="Seg_5028" n="e" s="T201">ɨllaːččɨ </ts>
               <ts e="T203" id="Seg_5030" n="e" s="T202">ete </ts>
               <ts e="T204" id="Seg_5032" n="e" s="T203">duː, </ts>
               <ts e="T205" id="Seg_5034" n="e" s="T204">kergennergitten </ts>
               <ts e="T206" id="Seg_5036" n="e" s="T205">kim </ts>
               <ts e="T207" id="Seg_5038" n="e" s="T206">ɨllaːččɨ </ts>
               <ts e="T209" id="Seg_5040" n="e" s="T207">etej? </ts>
            </ts>
            <ts e="T228" id="Seg_5041" n="sc" s="T224">
               <ts e="T226" id="Seg_5043" n="e" s="T224">– ɨraːk, </ts>
               <ts e="T228" id="Seg_5045" n="e" s="T226">heː? </ts>
            </ts>
            <ts e="T254" id="Seg_5046" n="sc" s="T229">
               <ts e="T230" id="Seg_5048" n="e" s="T229">– Bu </ts>
               <ts e="T231" id="Seg_5050" n="e" s="T230">ke </ts>
               <ts e="T232" id="Seg_5052" n="e" s="T231">tak-ta </ts>
               <ts e="T233" id="Seg_5054" n="e" s="T232">ɨrɨ͡alargɨn </ts>
               <ts e="T234" id="Seg_5056" n="e" s="T233">jakuttar </ts>
               <ts e="T235" id="Seg_5058" n="e" s="T234">ɨrɨ͡alarɨnan </ts>
               <ts e="T236" id="Seg_5060" n="e" s="T235">taːk </ts>
               <ts e="T237" id="Seg_5062" n="e" s="T236">daː </ts>
               <ts e="T238" id="Seg_5064" n="e" s="T237">tardagɨn </ts>
               <ts e="T239" id="Seg_5066" n="e" s="T238">ebit, </ts>
               <ts e="T241" id="Seg_5068" n="e" s="T239">eː? </ts>
               <ts e="T242" id="Seg_5070" n="e" s="T241">– Hin </ts>
               <ts e="T244" id="Seg_5072" n="e" s="T242">biːr… </ts>
               <ts e="T245" id="Seg_5074" n="e" s="T244">Ol </ts>
               <ts e="T246" id="Seg_5076" n="e" s="T245">menʼiːger </ts>
               <ts e="T247" id="Seg_5078" n="e" s="T246">(katɨl-), </ts>
               <ts e="T248" id="Seg_5080" n="e" s="T247">e, </ts>
               <ts e="T249" id="Seg_5082" n="e" s="T248">katallɨbɨttar </ts>
               <ts e="T251" id="Seg_5084" n="e" s="T249">bu͡o </ts>
               <ts e="T252" id="Seg_5086" n="e" s="T251">ginner </ts>
               <ts e="T254" id="Seg_5088" n="e" s="T252">ɨrɨ͡alara. </ts>
            </ts>
            <ts e="T283" id="Seg_5089" n="sc" s="T279">
               <ts e="T280" id="Seg_5091" n="e" s="T279">– Kluːbka </ts>
               <ts e="T283" id="Seg_5093" n="e" s="T280">üleleːbitiŋ? </ts>
            </ts>
            <ts e="T294" id="Seg_5094" n="sc" s="T284">
               <ts e="T285" id="Seg_5096" n="e" s="T284">– Onton </ts>
               <ts e="T286" id="Seg_5098" n="e" s="T285">kü͡ömejdeːkkin </ts>
               <ts e="T287" id="Seg_5100" n="e" s="T286">körönnör, </ts>
               <ts e="T288" id="Seg_5102" n="e" s="T287">senata </ts>
               <ts e="T289" id="Seg_5104" n="e" s="T288">itte </ts>
               <ts e="T290" id="Seg_5106" n="e" s="T289">taksar </ts>
               <ts e="T292" id="Seg_5108" n="e" s="T290">bu͡olluŋ, </ts>
               <ts e="T294" id="Seg_5110" n="e" s="T292">eː? </ts>
            </ts>
            <ts e="T319" id="Seg_5111" n="sc" s="T313">
               <ts e="T315" id="Seg_5113" n="e" s="T313">– Jakutskajga </ts>
               <ts e="T316" id="Seg_5115" n="e" s="T315">ɨlbɨt </ts>
               <ts e="T317" id="Seg_5117" n="e" s="T316">gramatalarɨŋ </ts>
               <ts e="T319" id="Seg_5119" n="e" s="T317">du͡o? </ts>
            </ts>
            <ts e="T348" id="Seg_5120" n="sc" s="T339">
               <ts e="T340" id="Seg_5122" n="e" s="T339">– Kannɨk </ts>
               <ts e="T341" id="Seg_5124" n="e" s="T340">rajon </ts>
               <ts e="T342" id="Seg_5126" n="e" s="T341">ete, </ts>
               <ts e="T343" id="Seg_5128" n="e" s="T342">diːgin, </ts>
               <ts e="T344" id="Seg_5130" n="e" s="T343">kannɨk, </ts>
               <ts e="T345" id="Seg_5132" n="e" s="T344">uluːstar </ts>
               <ts e="T346" id="Seg_5134" n="e" s="T345">bu͡o </ts>
               <ts e="T348" id="Seg_5136" n="e" s="T346">onno? </ts>
            </ts>
            <ts e="T358" id="Seg_5137" n="sc" s="T351">
               <ts e="T352" id="Seg_5139" n="e" s="T351">– Kuččuguj </ts>
               <ts e="T353" id="Seg_5141" n="e" s="T352">tuguj, </ts>
               <ts e="T354" id="Seg_5143" n="e" s="T353">pasʼolok </ts>
               <ts e="T355" id="Seg_5145" n="e" s="T354">duː, </ts>
               <ts e="T356" id="Seg_5147" n="e" s="T355">gorat </ts>
               <ts e="T357" id="Seg_5149" n="e" s="T356">ete </ts>
               <ts e="T358" id="Seg_5151" n="e" s="T357">duː? </ts>
            </ts>
            <ts e="T379" id="Seg_5152" n="sc" s="T375">
               <ts e="T377" id="Seg_5154" n="e" s="T375">– Kimneːgi </ts>
               <ts e="T378" id="Seg_5156" n="e" s="T377">gɨnagɨn, </ts>
               <ts e="T379" id="Seg_5158" n="e" s="T378">giniler? </ts>
            </ts>
            <ts e="T388" id="Seg_5159" n="sc" s="T383">
               <ts e="T384" id="Seg_5161" n="e" s="T383">– Aː, </ts>
               <ts e="T386" id="Seg_5163" n="e" s="T384">iti </ts>
               <ts e="T388" id="Seg_5165" n="e" s="T386">ikki. </ts>
            </ts>
            <ts e="T408" id="Seg_5166" n="sc" s="T391">
               <ts e="T392" id="Seg_5168" n="e" s="T391">– Ikki </ts>
               <ts e="T393" id="Seg_5170" n="e" s="T392">pasʼolok </ts>
               <ts e="T394" id="Seg_5172" n="e" s="T393">kördükter, </ts>
               <ts e="T395" id="Seg_5174" n="e" s="T394">eː? </ts>
               <ts e="T396" id="Seg_5176" n="e" s="T395">Bu </ts>
               <ts e="T397" id="Seg_5178" n="e" s="T396">ke </ts>
               <ts e="T398" id="Seg_5180" n="e" s="T397">tak-ta </ts>
               <ts e="T399" id="Seg_5182" n="e" s="T398">ɨrɨ͡alarɨ </ts>
               <ts e="T400" id="Seg_5184" n="e" s="T399">ɨllɨːbɨn, </ts>
               <ts e="T401" id="Seg_5186" n="e" s="T400">diːgin, </ts>
               <ts e="T402" id="Seg_5188" n="e" s="T401">kaččattan </ts>
               <ts e="T403" id="Seg_5190" n="e" s="T402">tak-ta </ts>
               <ts e="T404" id="Seg_5192" n="e" s="T403">ileliː </ts>
               <ts e="T405" id="Seg_5194" n="e" s="T404">ɨllɨːr </ts>
               <ts e="T406" id="Seg_5196" n="e" s="T405">bu͡olbukkɨnɨj, </ts>
               <ts e="T408" id="Seg_5198" n="e" s="T406">ki͡e? </ts>
            </ts>
            <ts e="T429" id="Seg_5199" n="sc" s="T417">
               <ts e="T418" id="Seg_5201" n="e" s="T417">– Bejeŋ </ts>
               <ts e="T419" id="Seg_5203" n="e" s="T418">ɨllaːn… </ts>
               <ts e="T420" id="Seg_5205" n="e" s="T419">Nʼuːčča </ts>
               <ts e="T421" id="Seg_5207" n="e" s="T420">itte </ts>
               <ts e="T422" id="Seg_5209" n="e" s="T421">ɨrɨ͡alarɨn </ts>
               <ts e="T423" id="Seg_5211" n="e" s="T422">ɨllaːččɨ </ts>
               <ts e="T424" id="Seg_5213" n="e" s="T423">etiŋ </ts>
               <ts e="T425" id="Seg_5215" n="e" s="T424">oččogo, </ts>
               <ts e="T426" id="Seg_5217" n="e" s="T425">eː, </ts>
               <ts e="T427" id="Seg_5219" n="e" s="T426">duː </ts>
               <ts e="T428" id="Seg_5221" n="e" s="T427">hakalɨː </ts>
               <ts e="T429" id="Seg_5223" n="e" s="T428">ere? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KuNS">
            <ta e="T34" id="Seg_5224" s="T29">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.001 (001.004)</ta>
            <ta e="T97" id="Seg_5225" s="T90">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.002 (001.016)</ta>
            <ta e="T128" id="Seg_5226" s="T121">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.003 (001.020)</ta>
            <ta e="T145" id="Seg_5227" s="T138">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.004 (001.025)</ta>
            <ta e="T160" id="Seg_5228" s="T152">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.005 (001.028)</ta>
            <ta e="T177" id="Seg_5229" s="T171">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.006 (001.031)</ta>
            <ta e="T209" id="Seg_5230" s="T197">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.007 (001.036)</ta>
            <ta e="T228" id="Seg_5231" s="T224">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.008 (001.039)</ta>
            <ta e="T241" id="Seg_5232" s="T229">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.009 (001.041)</ta>
            <ta e="T244" id="Seg_5233" s="T241">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.010 (001.043)</ta>
            <ta e="T254" id="Seg_5234" s="T244">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.011 (001.044)</ta>
            <ta e="T283" id="Seg_5235" s="T279">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.012 (001.050)</ta>
            <ta e="T294" id="Seg_5236" s="T284">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.013 (001.052)</ta>
            <ta e="T319" id="Seg_5237" s="T313">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.014 (001.057)</ta>
            <ta e="T348" id="Seg_5238" s="T339">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.015 (001.061)</ta>
            <ta e="T358" id="Seg_5239" s="T351">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.016 (001.063)</ta>
            <ta e="T379" id="Seg_5240" s="T375">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.017 (001.066)</ta>
            <ta e="T388" id="Seg_5241" s="T383">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.018 (001.068)</ta>
            <ta e="T395" id="Seg_5242" s="T391">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.019 (001.070)</ta>
            <ta e="T408" id="Seg_5243" s="T395">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.020 (001.071)</ta>
            <ta e="T419" id="Seg_5244" s="T417">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.021 (001.074)</ta>
            <ta e="T429" id="Seg_5245" s="T419">ZhNA_KuNS_20XX_LifeAndMusic_conv.KuNS.022 (001.075)</ta>
         </annotation>
         <annotation name="st" tierref="st-KuNS">
            <ta e="T34" id="Seg_5246" s="T29">К: Эбэӈ аата ким этэй?</ta>
            <ta e="T97" id="Seg_5247" s="T90">К: Канна үөрэммиккиний киэ ускуоланы иттэ бүтэрбитиӈ?</ta>
            <ta e="T128" id="Seg_5248" s="T121">К: Манна гынагын, Таймыырга гынагын?</ta>
            <ta e="T145" id="Seg_5249" s="T138">К: Дудинкага эмиэ эгэлбит этилэрэ ((…)).</ta>
            <ta e="T160" id="Seg_5250" s="T152">К: Оччого туок ырыаларын ыллыыр этиӈ күччүгүй эрдэккинэн?</ta>
            <ta e="T177" id="Seg_5251" s="T171">К: Һаӈалары һубу таһараӈӈын, э-э?</ta>
            <ta e="T209" id="Seg_5252" s="T197">К: Кими истэн, эбэӈ иттэ ыллааччы этэ дуу, кэргэннэргиттэн ким ыллааччы этэй?</ta>
            <ta e="T228" id="Seg_5253" s="T224">К: Ыраак, һээ?</ta>
            <ta e="T241" id="Seg_5254" s="T229">К: Бу кэ таак даа ырыаларгын якуттар ыарыаларынан таак даа тардагын эбит, э-э?</ta>
            <ta e="T244" id="Seg_5255" s="T241">К: Һин биир…</ta>
            <ta e="T254" id="Seg_5256" s="T244">К: Ол мэньиигэр (катыл..), э, каталлыбыттар буо гиннэр ырыалара.</ta>
            <ta e="T283" id="Seg_5257" s="T279">К: Клуубка үлэлээбитиӈ?</ta>
            <ta e="T294" id="Seg_5258" s="T284">К: Онтон күөмэйдээккин көрөннөр, сценага иттэ таксар буоллуӈ, ээ?</ta>
            <ta e="T319" id="Seg_5259" s="T313">К: Якутскайга ылбыт грамоталарыӈ дуо?</ta>
            <ta e="T348" id="Seg_5260" s="T339">К: Каннык район этэ, диигин, каннык, улуустар буо онно?</ta>
            <ta e="T358" id="Seg_5261" s="T351">К: Куччугуй тугуй, посёлок дуу, город этэ дуу?</ta>
            <ta e="T379" id="Seg_5262" s="T375">К: Кимнээги гынагын, гинилэр?</ta>
            <ta e="T388" id="Seg_5263" s="T383">К: А-а, ити икки.</ta>
            <ta e="T395" id="Seg_5264" s="T391">К: Икки посёлок көрдүктэр, э-э?</ta>
            <ta e="T408" id="Seg_5265" s="T395">К: Бу кэ таак даа ырыалары ыллыыбын, диигин, каччаттан таак даа илэлии ыллыыр буолбуккыный, киэ?</ta>
            <ta e="T419" id="Seg_5266" s="T417">К: Бэйэӈ ыллаан…</ta>
            <ta e="T429" id="Seg_5267" s="T419">Ньуучча иттэ ырыаларын ыллааччы этиӈ оччого, э-э, дуу һакалыы эрэ?</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KuNS">
            <ta e="T34" id="Seg_5268" s="T29">– Ebeŋ aːta kim etej? </ta>
            <ta e="T97" id="Seg_5269" s="T90">– Kanna ü͡öremmikkinij ki͡e usku͡olanɨ itte büterbitiŋ? </ta>
            <ta e="T128" id="Seg_5270" s="T121">– Manna gɨnagɨn, Tajmɨːrga gɨnagɨn? </ta>
            <ta e="T145" id="Seg_5271" s="T138">– Dudʼinkaga emi͡e egelbit etilere ((…)). </ta>
            <ta e="T160" id="Seg_5272" s="T152">– Oččogo tu͡ok ɨrɨ͡alarɨn ɨllɨːr etiŋ küččügüj erdekkinen? </ta>
            <ta e="T177" id="Seg_5273" s="T171">– Haŋalarɨ hubu taharaŋŋɨn, eː? </ta>
            <ta e="T209" id="Seg_5274" s="T197">– Kimi isten, ebeŋ itte ɨllaːččɨ ete duː, kergennergitten kim ɨllaːččɨ etej? </ta>
            <ta e="T228" id="Seg_5275" s="T224">– ɨraːk, heː? </ta>
            <ta e="T241" id="Seg_5276" s="T229">– Bu ke tak-ta ɨrɨ͡alargɨn jakuttar ɨrɨ͡alarɨnan taːk daː tardagɨn ebit, eː? </ta>
            <ta e="T244" id="Seg_5277" s="T241">– Hin biːr… </ta>
            <ta e="T254" id="Seg_5278" s="T244">Ol menʼiːger (katɨl-), e, katallɨbɨttar bu͡o ginner ɨrɨ͡alara. </ta>
            <ta e="T283" id="Seg_5279" s="T279">– Kluːbka üleleːbitiŋ? </ta>
            <ta e="T294" id="Seg_5280" s="T284">– Onton kü͡ömejdeːkkin körönnör, senata itte taksar bu͡olluŋ, eː? </ta>
            <ta e="T319" id="Seg_5281" s="T313">– Jakutskajga ɨlbɨt gramatalarɨŋ du͡o? </ta>
            <ta e="T348" id="Seg_5282" s="T339">– Kannɨk rajon ete, diːgin, kannɨk, uluːstar bu͡o onno? </ta>
            <ta e="T358" id="Seg_5283" s="T351">– Kuččuguj tuguj, pasʼolok duː, gorat ete duː? </ta>
            <ta e="T379" id="Seg_5284" s="T375">– Kimneːgi gɨnagɨn, giniler? </ta>
            <ta e="T388" id="Seg_5285" s="T383">– Aː, iti ikki. </ta>
            <ta e="T395" id="Seg_5286" s="T391">– Ikki pasʼolok kördükter, eː? </ta>
            <ta e="T408" id="Seg_5287" s="T395">Bu ke tak-ta ɨrɨ͡alarɨ ɨllɨːbɨn, diːgin, kaččattan tak-ta ileliː ɨllɨːr bu͡olbukkɨnɨj, ki͡e? </ta>
            <ta e="T419" id="Seg_5288" s="T417">– Bejeŋ ɨllaːn… </ta>
            <ta e="T429" id="Seg_5289" s="T419">Nʼuːčča itte ɨrɨ͡alarɨn ɨllaːččɨ etiŋ oččogo, eː, duː hakalɨː ere? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KuNS">
            <ta e="T31" id="Seg_5290" s="T29">ebe-ŋ</ta>
            <ta e="T32" id="Seg_5291" s="T31">aːt-a</ta>
            <ta e="T33" id="Seg_5292" s="T32">kim</ta>
            <ta e="T34" id="Seg_5293" s="T33">e-t-e=j</ta>
            <ta e="T92" id="Seg_5294" s="T90">kanna</ta>
            <ta e="T93" id="Seg_5295" s="T92">ü͡örem-mik-kin=ij</ta>
            <ta e="T94" id="Seg_5296" s="T93">ki͡e</ta>
            <ta e="T95" id="Seg_5297" s="T94">usku͡ola-nɨ</ta>
            <ta e="T96" id="Seg_5298" s="T95">itte</ta>
            <ta e="T97" id="Seg_5299" s="T96">büt-e-r-bit-i-ŋ</ta>
            <ta e="T123" id="Seg_5300" s="T121">manna</ta>
            <ta e="T125" id="Seg_5301" s="T123">gɨn-a-gɨn</ta>
            <ta e="T126" id="Seg_5302" s="T125">Tajmɨːr-ga</ta>
            <ta e="T128" id="Seg_5303" s="T126">gɨn-a-gɨn</ta>
            <ta e="T139" id="Seg_5304" s="T138">Dudʼinka-ga</ta>
            <ta e="T140" id="Seg_5305" s="T139">emi͡e</ta>
            <ta e="T142" id="Seg_5306" s="T140">egel-bit</ta>
            <ta e="T143" id="Seg_5307" s="T142">e-ti-lere</ta>
            <ta e="T153" id="Seg_5308" s="T152">oččogo</ta>
            <ta e="T154" id="Seg_5309" s="T153">tu͡ok</ta>
            <ta e="T155" id="Seg_5310" s="T154">ɨrɨ͡a-lar-ɨ-n</ta>
            <ta e="T156" id="Seg_5311" s="T155">ɨllɨː-r</ta>
            <ta e="T157" id="Seg_5312" s="T156">e-ti-ŋ</ta>
            <ta e="T158" id="Seg_5313" s="T157">küččügüj</ta>
            <ta e="T160" id="Seg_5314" s="T158">er-dek-kinen</ta>
            <ta e="T173" id="Seg_5315" s="T171">haŋa-lar-ɨ</ta>
            <ta e="T174" id="Seg_5316" s="T173">hubu</ta>
            <ta e="T175" id="Seg_5317" s="T174">tahar-aŋ-ŋɨn</ta>
            <ta e="T177" id="Seg_5318" s="T175">eː</ta>
            <ta e="T198" id="Seg_5319" s="T197">kim-i</ta>
            <ta e="T199" id="Seg_5320" s="T198">ist-en</ta>
            <ta e="T200" id="Seg_5321" s="T199">ebe-ŋ</ta>
            <ta e="T201" id="Seg_5322" s="T200">itte</ta>
            <ta e="T202" id="Seg_5323" s="T201">ɨllaː-ččɨ</ta>
            <ta e="T203" id="Seg_5324" s="T202">e-t-e</ta>
            <ta e="T204" id="Seg_5325" s="T203">duː</ta>
            <ta e="T205" id="Seg_5326" s="T204">kergen-ner-gi-tten</ta>
            <ta e="T206" id="Seg_5327" s="T205">kim</ta>
            <ta e="T207" id="Seg_5328" s="T206">ɨllaː-ččɨ</ta>
            <ta e="T209" id="Seg_5329" s="T207">e-t-e=j</ta>
            <ta e="T226" id="Seg_5330" s="T224">ɨraːk</ta>
            <ta e="T228" id="Seg_5331" s="T226">heː</ta>
            <ta e="T230" id="Seg_5332" s="T229">bu</ta>
            <ta e="T231" id="Seg_5333" s="T230">ke</ta>
            <ta e="T232" id="Seg_5334" s="T231">tak-ta</ta>
            <ta e="T233" id="Seg_5335" s="T232">ɨrɨ͡a-lar-gɨ-n</ta>
            <ta e="T234" id="Seg_5336" s="T233">jakut-tar</ta>
            <ta e="T235" id="Seg_5337" s="T234">ɨrɨ͡a-larɨ-nan</ta>
            <ta e="T236" id="Seg_5338" s="T235">taːk</ta>
            <ta e="T237" id="Seg_5339" s="T236">daː</ta>
            <ta e="T238" id="Seg_5340" s="T237">tard-a-gɨn</ta>
            <ta e="T239" id="Seg_5341" s="T238">e-bit</ta>
            <ta e="T241" id="Seg_5342" s="T239">eː</ta>
            <ta e="T242" id="Seg_5343" s="T241">hin</ta>
            <ta e="T244" id="Seg_5344" s="T242">biːr</ta>
            <ta e="T245" id="Seg_5345" s="T244">ol</ta>
            <ta e="T246" id="Seg_5346" s="T245">menʼi-ge-r</ta>
            <ta e="T248" id="Seg_5347" s="T247">e</ta>
            <ta e="T249" id="Seg_5348" s="T248">kat-a-ll-ɨ-bɨt-tar</ta>
            <ta e="T251" id="Seg_5349" s="T249">bu͡o</ta>
            <ta e="T252" id="Seg_5350" s="T251">ginner</ta>
            <ta e="T254" id="Seg_5351" s="T252">ɨrɨ͡a-lara</ta>
            <ta e="T280" id="Seg_5352" s="T279">kluːb-ka</ta>
            <ta e="T283" id="Seg_5353" s="T280">üleleː-bit-i-ŋ</ta>
            <ta e="T285" id="Seg_5354" s="T284">onton</ta>
            <ta e="T286" id="Seg_5355" s="T285">kü͡ömej-deːk-kin</ta>
            <ta e="T287" id="Seg_5356" s="T286">kör-ön-nör</ta>
            <ta e="T288" id="Seg_5357" s="T287">sena-ta</ta>
            <ta e="T289" id="Seg_5358" s="T288">itte</ta>
            <ta e="T290" id="Seg_5359" s="T289">taks-ar</ta>
            <ta e="T292" id="Seg_5360" s="T290">bu͡ol-lu-ŋ</ta>
            <ta e="T294" id="Seg_5361" s="T292">eː</ta>
            <ta e="T315" id="Seg_5362" s="T313">Jakutskaj-ga</ta>
            <ta e="T316" id="Seg_5363" s="T315">ɨl-bɨt</ta>
            <ta e="T317" id="Seg_5364" s="T316">gramata-lar-ɨ-ŋ</ta>
            <ta e="T319" id="Seg_5365" s="T317">du͡o</ta>
            <ta e="T340" id="Seg_5366" s="T339">kannɨk</ta>
            <ta e="T341" id="Seg_5367" s="T340">rajon</ta>
            <ta e="T342" id="Seg_5368" s="T341">e-t-e</ta>
            <ta e="T343" id="Seg_5369" s="T342">d-iː-gin</ta>
            <ta e="T344" id="Seg_5370" s="T343">kannɨk</ta>
            <ta e="T345" id="Seg_5371" s="T344">uluːs-tar</ta>
            <ta e="T346" id="Seg_5372" s="T345">bu͡o</ta>
            <ta e="T348" id="Seg_5373" s="T346">onno</ta>
            <ta e="T352" id="Seg_5374" s="T351">kuččuguj</ta>
            <ta e="T353" id="Seg_5375" s="T352">tug=uj</ta>
            <ta e="T354" id="Seg_5376" s="T353">pasʼolok</ta>
            <ta e="T355" id="Seg_5377" s="T354">duː</ta>
            <ta e="T356" id="Seg_5378" s="T355">gorat</ta>
            <ta e="T357" id="Seg_5379" s="T356">e-t-e</ta>
            <ta e="T358" id="Seg_5380" s="T357">duː</ta>
            <ta e="T377" id="Seg_5381" s="T375">kim-neːg-i</ta>
            <ta e="T378" id="Seg_5382" s="T377">gɨn-a-gɨn</ta>
            <ta e="T379" id="Seg_5383" s="T378">giniler</ta>
            <ta e="T384" id="Seg_5384" s="T383">aː</ta>
            <ta e="T386" id="Seg_5385" s="T384">iti</ta>
            <ta e="T388" id="Seg_5386" s="T386">ikki</ta>
            <ta e="T392" id="Seg_5387" s="T391">ikki</ta>
            <ta e="T393" id="Seg_5388" s="T392">pasʼolok</ta>
            <ta e="T394" id="Seg_5389" s="T393">kördük-ter</ta>
            <ta e="T395" id="Seg_5390" s="T394">eː</ta>
            <ta e="T396" id="Seg_5391" s="T395">bu</ta>
            <ta e="T397" id="Seg_5392" s="T396">ke</ta>
            <ta e="T398" id="Seg_5393" s="T397">tak-ta</ta>
            <ta e="T399" id="Seg_5394" s="T398">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T400" id="Seg_5395" s="T399">ɨll-ɨː-bɨn</ta>
            <ta e="T401" id="Seg_5396" s="T400">d-iː-gin</ta>
            <ta e="T402" id="Seg_5397" s="T401">kačča-ttan</ta>
            <ta e="T403" id="Seg_5398" s="T402">tak-ta</ta>
            <ta e="T404" id="Seg_5399" s="T403">ile-liː</ta>
            <ta e="T405" id="Seg_5400" s="T404">ɨllɨː-r</ta>
            <ta e="T406" id="Seg_5401" s="T405">bu͡ol-buk-kɨn=ɨj</ta>
            <ta e="T408" id="Seg_5402" s="T406">ki͡e</ta>
            <ta e="T418" id="Seg_5403" s="T417">beje-ŋ</ta>
            <ta e="T419" id="Seg_5404" s="T418">ɨllaː-n</ta>
            <ta e="T420" id="Seg_5405" s="T419">nʼuːčča</ta>
            <ta e="T421" id="Seg_5406" s="T420">itte</ta>
            <ta e="T422" id="Seg_5407" s="T421">ɨrɨ͡a-lar-ɨ-n</ta>
            <ta e="T423" id="Seg_5408" s="T422">ɨllaː-ččɨ</ta>
            <ta e="T424" id="Seg_5409" s="T423">e-ti-ŋ</ta>
            <ta e="T425" id="Seg_5410" s="T424">oččogo</ta>
            <ta e="T426" id="Seg_5411" s="T425">eː</ta>
            <ta e="T427" id="Seg_5412" s="T426">duː</ta>
            <ta e="T428" id="Seg_5413" s="T427">haka-lɨː</ta>
            <ta e="T429" id="Seg_5414" s="T428">ere</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KuNS">
            <ta e="T31" id="Seg_5415" s="T29">ebe-ŋ</ta>
            <ta e="T32" id="Seg_5416" s="T31">aːt-tA</ta>
            <ta e="T33" id="Seg_5417" s="T32">kim</ta>
            <ta e="T34" id="Seg_5418" s="T33">e-TI-tA=Ij</ta>
            <ta e="T92" id="Seg_5419" s="T90">kanna</ta>
            <ta e="T93" id="Seg_5420" s="T92">ü͡ören-BIT-GIn=Ij</ta>
            <ta e="T94" id="Seg_5421" s="T93">keː</ta>
            <ta e="T95" id="Seg_5422" s="T94">usku͡ola-nI</ta>
            <ta e="T96" id="Seg_5423" s="T95">itte</ta>
            <ta e="T97" id="Seg_5424" s="T96">büt-A-r-BIT-I-ŋ</ta>
            <ta e="T123" id="Seg_5425" s="T121">manna</ta>
            <ta e="T125" id="Seg_5426" s="T123">gɨn-A-GIn</ta>
            <ta e="T126" id="Seg_5427" s="T125">Tajmɨr-GA</ta>
            <ta e="T128" id="Seg_5428" s="T126">gɨn-A-GIn</ta>
            <ta e="T139" id="Seg_5429" s="T138">Dudʼinka-GA</ta>
            <ta e="T140" id="Seg_5430" s="T139">emi͡e</ta>
            <ta e="T142" id="Seg_5431" s="T140">egel-BIT</ta>
            <ta e="T143" id="Seg_5432" s="T142">e-TI-LArA</ta>
            <ta e="T153" id="Seg_5433" s="T152">oččogo</ta>
            <ta e="T154" id="Seg_5434" s="T153">tu͡ok</ta>
            <ta e="T155" id="Seg_5435" s="T154">ɨrɨ͡a-LAr-tI-n</ta>
            <ta e="T156" id="Seg_5436" s="T155">ɨllaː-Ar</ta>
            <ta e="T157" id="Seg_5437" s="T156">e-TI-ŋ</ta>
            <ta e="T158" id="Seg_5438" s="T157">küččügüj</ta>
            <ta e="T160" id="Seg_5439" s="T158">er-TAK-GInA</ta>
            <ta e="T173" id="Seg_5440" s="T171">haŋa-LAr-nI</ta>
            <ta e="T174" id="Seg_5441" s="T173">hubu</ta>
            <ta e="T175" id="Seg_5442" s="T174">tahaːr-An-GIn</ta>
            <ta e="T177" id="Seg_5443" s="T175">eː</ta>
            <ta e="T198" id="Seg_5444" s="T197">kim-nI</ta>
            <ta e="T199" id="Seg_5445" s="T198">ihit-An</ta>
            <ta e="T200" id="Seg_5446" s="T199">ebe-ŋ</ta>
            <ta e="T201" id="Seg_5447" s="T200">itte</ta>
            <ta e="T202" id="Seg_5448" s="T201">ɨllaː-AːččI</ta>
            <ta e="T203" id="Seg_5449" s="T202">e-TI-tA</ta>
            <ta e="T204" id="Seg_5450" s="T203">du͡o</ta>
            <ta e="T205" id="Seg_5451" s="T204">kergen-LAr-GI-ttAn</ta>
            <ta e="T206" id="Seg_5452" s="T205">kim</ta>
            <ta e="T207" id="Seg_5453" s="T206">ɨllaː-AːččI</ta>
            <ta e="T209" id="Seg_5454" s="T207">e-TI-tA=Ij</ta>
            <ta e="T226" id="Seg_5455" s="T224">ɨraːk</ta>
            <ta e="T228" id="Seg_5456" s="T226">eː</ta>
            <ta e="T230" id="Seg_5457" s="T229">bu</ta>
            <ta e="T231" id="Seg_5458" s="T230">ka</ta>
            <ta e="T232" id="Seg_5459" s="T231">taːk-ta</ta>
            <ta e="T233" id="Seg_5460" s="T232">ɨrɨ͡a-LAr-GI-n</ta>
            <ta e="T234" id="Seg_5461" s="T233">jakut-LAr</ta>
            <ta e="T235" id="Seg_5462" s="T234">ɨrɨ͡a-LArI-nAn</ta>
            <ta e="T236" id="Seg_5463" s="T235">taːk</ta>
            <ta e="T237" id="Seg_5464" s="T236">da</ta>
            <ta e="T238" id="Seg_5465" s="T237">tart-A-GIn</ta>
            <ta e="T239" id="Seg_5466" s="T238">e-BIT</ta>
            <ta e="T241" id="Seg_5467" s="T239">eː</ta>
            <ta e="T242" id="Seg_5468" s="T241">hin</ta>
            <ta e="T244" id="Seg_5469" s="T242">biːr</ta>
            <ta e="T245" id="Seg_5470" s="T244">ol</ta>
            <ta e="T246" id="Seg_5471" s="T245">menʼiː-GA-r</ta>
            <ta e="T248" id="Seg_5472" s="T247">e</ta>
            <ta e="T249" id="Seg_5473" s="T248">kataː-A-LIN-I-BIT-LAr</ta>
            <ta e="T251" id="Seg_5474" s="T249">bu͡o</ta>
            <ta e="T252" id="Seg_5475" s="T251">giniler</ta>
            <ta e="T254" id="Seg_5476" s="T252">ɨrɨ͡a-LArA</ta>
            <ta e="T280" id="Seg_5477" s="T279">kuluːb-GA</ta>
            <ta e="T283" id="Seg_5478" s="T280">üleleː-BIT-I-ŋ</ta>
            <ta e="T285" id="Seg_5479" s="T284">onton</ta>
            <ta e="T286" id="Seg_5480" s="T285">kü͡ömej-LAːK-GIn</ta>
            <ta e="T287" id="Seg_5481" s="T286">kör-An-LAr</ta>
            <ta e="T288" id="Seg_5482" s="T287">sɨ͡ana-tA</ta>
            <ta e="T289" id="Seg_5483" s="T288">itte</ta>
            <ta e="T290" id="Seg_5484" s="T289">tagɨs-Ar</ta>
            <ta e="T292" id="Seg_5485" s="T290">bu͡ol-TI-ŋ</ta>
            <ta e="T294" id="Seg_5486" s="T292">eː</ta>
            <ta e="T315" id="Seg_5487" s="T313">Jakuːtskaj-GA</ta>
            <ta e="T316" id="Seg_5488" s="T315">ɨl-BIT</ta>
            <ta e="T317" id="Seg_5489" s="T316">gramata-LAr-I-ŋ</ta>
            <ta e="T319" id="Seg_5490" s="T317">du͡o</ta>
            <ta e="T340" id="Seg_5491" s="T339">kannɨk</ta>
            <ta e="T341" id="Seg_5492" s="T340">rajon</ta>
            <ta e="T342" id="Seg_5493" s="T341">e-TI-tA</ta>
            <ta e="T343" id="Seg_5494" s="T342">di͡e-A-GIn</ta>
            <ta e="T344" id="Seg_5495" s="T343">kannɨk</ta>
            <ta e="T345" id="Seg_5496" s="T344">uluːs-LAr</ta>
            <ta e="T346" id="Seg_5497" s="T345">bu͡o</ta>
            <ta e="T348" id="Seg_5498" s="T346">onno</ta>
            <ta e="T352" id="Seg_5499" s="T351">küččügüj</ta>
            <ta e="T353" id="Seg_5500" s="T352">tu͡ok=Ij</ta>
            <ta e="T354" id="Seg_5501" s="T353">pasʼolak</ta>
            <ta e="T355" id="Seg_5502" s="T354">du͡o</ta>
            <ta e="T356" id="Seg_5503" s="T355">gu͡orat</ta>
            <ta e="T357" id="Seg_5504" s="T356">e-TI-tA</ta>
            <ta e="T358" id="Seg_5505" s="T357">du͡o</ta>
            <ta e="T377" id="Seg_5506" s="T375">kim-LAːK-nI</ta>
            <ta e="T378" id="Seg_5507" s="T377">gɨn-A-GIn</ta>
            <ta e="T379" id="Seg_5508" s="T378">giniler</ta>
            <ta e="T384" id="Seg_5509" s="T383">aː</ta>
            <ta e="T386" id="Seg_5510" s="T384">iti</ta>
            <ta e="T388" id="Seg_5511" s="T386">ikki</ta>
            <ta e="T392" id="Seg_5512" s="T391">ikki</ta>
            <ta e="T393" id="Seg_5513" s="T392">pasʼolak</ta>
            <ta e="T394" id="Seg_5514" s="T393">kördük-LAr</ta>
            <ta e="T395" id="Seg_5515" s="T394">eː</ta>
            <ta e="T396" id="Seg_5516" s="T395">bu</ta>
            <ta e="T397" id="Seg_5517" s="T396">ka</ta>
            <ta e="T398" id="Seg_5518" s="T397">taːk-ta</ta>
            <ta e="T399" id="Seg_5519" s="T398">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T400" id="Seg_5520" s="T399">ɨllaː-A-BIn</ta>
            <ta e="T401" id="Seg_5521" s="T400">di͡e-A-GIn</ta>
            <ta e="T402" id="Seg_5522" s="T401">kaččaga-ttAn</ta>
            <ta e="T403" id="Seg_5523" s="T402">taːk-ta</ta>
            <ta e="T404" id="Seg_5524" s="T403">ile-LIː</ta>
            <ta e="T405" id="Seg_5525" s="T404">ɨllaː-Ar</ta>
            <ta e="T406" id="Seg_5526" s="T405">bu͡ol-BIT-GIn=Ij</ta>
            <ta e="T408" id="Seg_5527" s="T406">keː</ta>
            <ta e="T418" id="Seg_5528" s="T417">beje-ŋ</ta>
            <ta e="T419" id="Seg_5529" s="T418">ɨllaː-An</ta>
            <ta e="T420" id="Seg_5530" s="T419">nuːčča</ta>
            <ta e="T421" id="Seg_5531" s="T420">itte</ta>
            <ta e="T422" id="Seg_5532" s="T421">ɨrɨ͡a-LAr-tI-n</ta>
            <ta e="T423" id="Seg_5533" s="T422">ɨllaː-AːččI</ta>
            <ta e="T424" id="Seg_5534" s="T423">e-TI-ŋ</ta>
            <ta e="T425" id="Seg_5535" s="T424">oččogo</ta>
            <ta e="T426" id="Seg_5536" s="T425">eː</ta>
            <ta e="T427" id="Seg_5537" s="T426">du͡o</ta>
            <ta e="T428" id="Seg_5538" s="T427">haka-LIː</ta>
            <ta e="T429" id="Seg_5539" s="T428">ere</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KuNS">
            <ta e="T31" id="Seg_5540" s="T29">grandmother-2SG.[NOM]</ta>
            <ta e="T32" id="Seg_5541" s="T31">name-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_5542" s="T32">who.[NOM]</ta>
            <ta e="T34" id="Seg_5543" s="T33">be-PST1-3SG=Q</ta>
            <ta e="T92" id="Seg_5544" s="T90">where</ta>
            <ta e="T93" id="Seg_5545" s="T92">learn-PST2-2SG=Q</ta>
            <ta e="T94" id="Seg_5546" s="T93">EMPH</ta>
            <ta e="T95" id="Seg_5547" s="T94">school-ACC</ta>
            <ta e="T96" id="Seg_5548" s="T95">EMPH</ta>
            <ta e="T97" id="Seg_5549" s="T96">stop-EP-CAUS-PST2-EP-2SG</ta>
            <ta e="T123" id="Seg_5550" s="T121">here</ta>
            <ta e="T125" id="Seg_5551" s="T123">make-PRS-2SG</ta>
            <ta e="T126" id="Seg_5552" s="T125">Taymyr-DAT/LOC</ta>
            <ta e="T128" id="Seg_5553" s="T126">make-PRS-2SG</ta>
            <ta e="T139" id="Seg_5554" s="T138">Dudinka-DAT/LOC</ta>
            <ta e="T140" id="Seg_5555" s="T139">again</ta>
            <ta e="T142" id="Seg_5556" s="T140">bring-PTCP.PST</ta>
            <ta e="T143" id="Seg_5557" s="T142">be-PST1-3PL</ta>
            <ta e="T153" id="Seg_5558" s="T152">then</ta>
            <ta e="T154" id="Seg_5559" s="T153">what.[NOM]</ta>
            <ta e="T155" id="Seg_5560" s="T154">song-PL-3SG-ACC</ta>
            <ta e="T156" id="Seg_5561" s="T155">sing-PTCP.PRS</ta>
            <ta e="T157" id="Seg_5562" s="T156">be-PST1-2SG</ta>
            <ta e="T158" id="Seg_5563" s="T157">small.[NOM]</ta>
            <ta e="T160" id="Seg_5564" s="T158">be-TEMP-2SG</ta>
            <ta e="T173" id="Seg_5565" s="T171">word-PL-ACC</ta>
            <ta e="T174" id="Seg_5566" s="T173">this.EMPH</ta>
            <ta e="T175" id="Seg_5567" s="T174">take.out-CVB.SEQ-2SG</ta>
            <ta e="T177" id="Seg_5568" s="T175">AFFIRM</ta>
            <ta e="T198" id="Seg_5569" s="T197">who-ACC</ta>
            <ta e="T199" id="Seg_5570" s="T198">hear-CVB.SEQ</ta>
            <ta e="T200" id="Seg_5571" s="T199">grandmother-2SG.[NOM]</ta>
            <ta e="T201" id="Seg_5572" s="T200">EMPH</ta>
            <ta e="T202" id="Seg_5573" s="T201">sing-PTCP.HAB</ta>
            <ta e="T203" id="Seg_5574" s="T202">be-PST1-3SG</ta>
            <ta e="T204" id="Seg_5575" s="T203">Q</ta>
            <ta e="T205" id="Seg_5576" s="T204">parents-PL-2SG-ABL</ta>
            <ta e="T206" id="Seg_5577" s="T205">who.[NOM]</ta>
            <ta e="T207" id="Seg_5578" s="T206">sing-PTCP.HAB</ta>
            <ta e="T209" id="Seg_5579" s="T207">be-PST1-3SG=Q</ta>
            <ta e="T226" id="Seg_5580" s="T224">distant.[NOM]</ta>
            <ta e="T228" id="Seg_5581" s="T226">AFFIRM</ta>
            <ta e="T230" id="Seg_5582" s="T229">this</ta>
            <ta e="T231" id="Seg_5583" s="T230">well</ta>
            <ta e="T232" id="Seg_5584" s="T231">so-EMPH</ta>
            <ta e="T233" id="Seg_5585" s="T232">song-PL-2SG-ACC</ta>
            <ta e="T234" id="Seg_5586" s="T233">Yakut-PL.[NOM]</ta>
            <ta e="T235" id="Seg_5587" s="T234">song-3PL-INSTR</ta>
            <ta e="T236" id="Seg_5588" s="T235">so</ta>
            <ta e="T237" id="Seg_5589" s="T236">and</ta>
            <ta e="T238" id="Seg_5590" s="T237">pull-PRS-2SG</ta>
            <ta e="T239" id="Seg_5591" s="T238">be-PST2.[3SG]</ta>
            <ta e="T241" id="Seg_5592" s="T239">AFFIRM</ta>
            <ta e="T242" id="Seg_5593" s="T241">anyway</ta>
            <ta e="T244" id="Seg_5594" s="T242">one</ta>
            <ta e="T245" id="Seg_5595" s="T244">that</ta>
            <ta e="T246" id="Seg_5596" s="T245">head-2SG-DAT/LOC</ta>
            <ta e="T248" id="Seg_5597" s="T247">eh</ta>
            <ta e="T249" id="Seg_5598" s="T248">fix-EP-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T251" id="Seg_5599" s="T249">EMPH</ta>
            <ta e="T252" id="Seg_5600" s="T251">3PL.[NOM]</ta>
            <ta e="T254" id="Seg_5601" s="T252">song-3PL.[NOM]</ta>
            <ta e="T280" id="Seg_5602" s="T279">club-DAT/LOC</ta>
            <ta e="T283" id="Seg_5603" s="T280">work-PST2-EP-2SG</ta>
            <ta e="T285" id="Seg_5604" s="T284">then</ta>
            <ta e="T286" id="Seg_5605" s="T285">voice-PROPR-2SG</ta>
            <ta e="T287" id="Seg_5606" s="T286">see-CVB.SEQ-3PL</ta>
            <ta e="T288" id="Seg_5607" s="T287">stage-3SG.[NOM]</ta>
            <ta e="T289" id="Seg_5608" s="T288">EMPH</ta>
            <ta e="T290" id="Seg_5609" s="T289">go.out-PTCP.PRS</ta>
            <ta e="T292" id="Seg_5610" s="T290">be-PST1-2SG</ta>
            <ta e="T294" id="Seg_5611" s="T292">AFFIRM</ta>
            <ta e="T315" id="Seg_5612" s="T313">Yakutsk-DAT/LOC</ta>
            <ta e="T316" id="Seg_5613" s="T315">take-PTCP.PST</ta>
            <ta e="T317" id="Seg_5614" s="T316">certificate-PL-EP-2SG.[NOM]</ta>
            <ta e="T319" id="Seg_5615" s="T317">Q</ta>
            <ta e="T340" id="Seg_5616" s="T339">what.kind.of</ta>
            <ta e="T341" id="Seg_5617" s="T340">rayon.[NOM]</ta>
            <ta e="T342" id="Seg_5618" s="T341">be-PST1-3SG</ta>
            <ta e="T343" id="Seg_5619" s="T342">say-PRS-2SG</ta>
            <ta e="T344" id="Seg_5620" s="T343">what.kind.of</ta>
            <ta e="T345" id="Seg_5621" s="T344">ulus-PL.[NOM]</ta>
            <ta e="T346" id="Seg_5622" s="T345">EMPH</ta>
            <ta e="T348" id="Seg_5623" s="T346">there</ta>
            <ta e="T352" id="Seg_5624" s="T351">small</ta>
            <ta e="T353" id="Seg_5625" s="T352">what.[NOM]=Q</ta>
            <ta e="T354" id="Seg_5626" s="T353">village.[NOM]</ta>
            <ta e="T355" id="Seg_5627" s="T354">Q</ta>
            <ta e="T356" id="Seg_5628" s="T355">city.[NOM]</ta>
            <ta e="T357" id="Seg_5629" s="T356">be-PST1-3SG</ta>
            <ta e="T358" id="Seg_5630" s="T357">Q</ta>
            <ta e="T377" id="Seg_5631" s="T375">who-PROPR-ACC</ta>
            <ta e="T378" id="Seg_5632" s="T377">make-PRS-2SG</ta>
            <ta e="T379" id="Seg_5633" s="T378">3PL.[NOM]</ta>
            <ta e="T384" id="Seg_5634" s="T383">ah</ta>
            <ta e="T386" id="Seg_5635" s="T384">that</ta>
            <ta e="T388" id="Seg_5636" s="T386">two</ta>
            <ta e="T392" id="Seg_5637" s="T391">two</ta>
            <ta e="T393" id="Seg_5638" s="T392">village.[NOM]</ta>
            <ta e="T394" id="Seg_5639" s="T393">like-3PL</ta>
            <ta e="T395" id="Seg_5640" s="T394">AFFIRM</ta>
            <ta e="T396" id="Seg_5641" s="T395">this</ta>
            <ta e="T397" id="Seg_5642" s="T396">well</ta>
            <ta e="T398" id="Seg_5643" s="T397">so-EMPH</ta>
            <ta e="T399" id="Seg_5644" s="T398">song-PL-ACC</ta>
            <ta e="T400" id="Seg_5645" s="T399">sing-PRS-1SG</ta>
            <ta e="T401" id="Seg_5646" s="T400">say-PRS-2SG</ta>
            <ta e="T402" id="Seg_5647" s="T401">when-ABL</ta>
            <ta e="T403" id="Seg_5648" s="T402">so-EMPH</ta>
            <ta e="T404" id="Seg_5649" s="T403">real-SIM</ta>
            <ta e="T405" id="Seg_5650" s="T404">sing-PTCP.PRS</ta>
            <ta e="T406" id="Seg_5651" s="T405">become-PST2-2SG=Q</ta>
            <ta e="T408" id="Seg_5652" s="T406">EMPH</ta>
            <ta e="T418" id="Seg_5653" s="T417">self-2SG.[NOM]</ta>
            <ta e="T419" id="Seg_5654" s="T418">sing-CVB.SEQ</ta>
            <ta e="T420" id="Seg_5655" s="T419">Russian</ta>
            <ta e="T421" id="Seg_5656" s="T420">EMPH</ta>
            <ta e="T422" id="Seg_5657" s="T421">song-PL-3SG-ACC</ta>
            <ta e="T423" id="Seg_5658" s="T422">sing-PTCP.HAB</ta>
            <ta e="T424" id="Seg_5659" s="T423">be-PST1-2SG</ta>
            <ta e="T425" id="Seg_5660" s="T424">then</ta>
            <ta e="T426" id="Seg_5661" s="T425">eh</ta>
            <ta e="T427" id="Seg_5662" s="T426">Q</ta>
            <ta e="T428" id="Seg_5663" s="T427">Dolgan-SIM</ta>
            <ta e="T429" id="Seg_5664" s="T428">just</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KuNS">
            <ta e="T31" id="Seg_5665" s="T29">Großmutter-2SG.[NOM]</ta>
            <ta e="T32" id="Seg_5666" s="T31">Name-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_5667" s="T32">wer.[NOM]</ta>
            <ta e="T34" id="Seg_5668" s="T33">sein-PST1-3SG=Q</ta>
            <ta e="T92" id="Seg_5669" s="T90">wo</ta>
            <ta e="T93" id="Seg_5670" s="T92">lernen-PST2-2SG=Q</ta>
            <ta e="T94" id="Seg_5671" s="T93">EMPH</ta>
            <ta e="T95" id="Seg_5672" s="T94">Schule-ACC</ta>
            <ta e="T96" id="Seg_5673" s="T95">EMPH</ta>
            <ta e="T97" id="Seg_5674" s="T96">aufhören-EP-CAUS-PST2-EP-2SG</ta>
            <ta e="T123" id="Seg_5675" s="T121">hier</ta>
            <ta e="T125" id="Seg_5676" s="T123">machen-PRS-2SG</ta>
            <ta e="T126" id="Seg_5677" s="T125">Taimyr-DAT/LOC</ta>
            <ta e="T128" id="Seg_5678" s="T126">machen-PRS-2SG</ta>
            <ta e="T139" id="Seg_5679" s="T138">Dudinka-DAT/LOC</ta>
            <ta e="T140" id="Seg_5680" s="T139">wieder</ta>
            <ta e="T142" id="Seg_5681" s="T140">bringen-PTCP.PST</ta>
            <ta e="T143" id="Seg_5682" s="T142">sein-PST1-3PL</ta>
            <ta e="T153" id="Seg_5683" s="T152">dann</ta>
            <ta e="T154" id="Seg_5684" s="T153">was.[NOM]</ta>
            <ta e="T155" id="Seg_5685" s="T154">Lied-PL-3SG-ACC</ta>
            <ta e="T156" id="Seg_5686" s="T155">singen-PTCP.PRS</ta>
            <ta e="T157" id="Seg_5687" s="T156">sein-PST1-2SG</ta>
            <ta e="T158" id="Seg_5688" s="T157">klein.[NOM]</ta>
            <ta e="T160" id="Seg_5689" s="T158">sein-TEMP-2SG</ta>
            <ta e="T173" id="Seg_5690" s="T171">Wort-PL-ACC</ta>
            <ta e="T174" id="Seg_5691" s="T173">dieses.EMPH</ta>
            <ta e="T175" id="Seg_5692" s="T174">herausnehmen-CVB.SEQ-2SG</ta>
            <ta e="T177" id="Seg_5693" s="T175">AFFIRM</ta>
            <ta e="T198" id="Seg_5694" s="T197">wer-ACC</ta>
            <ta e="T199" id="Seg_5695" s="T198">hören-CVB.SEQ</ta>
            <ta e="T200" id="Seg_5696" s="T199">Großmutter-2SG.[NOM]</ta>
            <ta e="T201" id="Seg_5697" s="T200">EMPH</ta>
            <ta e="T202" id="Seg_5698" s="T201">singen-PTCP.HAB</ta>
            <ta e="T203" id="Seg_5699" s="T202">sein-PST1-3SG</ta>
            <ta e="T204" id="Seg_5700" s="T203">Q</ta>
            <ta e="T205" id="Seg_5701" s="T204">Eltern-PL-2SG-ABL</ta>
            <ta e="T206" id="Seg_5702" s="T205">wer.[NOM]</ta>
            <ta e="T207" id="Seg_5703" s="T206">singen-PTCP.HAB</ta>
            <ta e="T209" id="Seg_5704" s="T207">sein-PST1-3SG=Q</ta>
            <ta e="T226" id="Seg_5705" s="T224">fern.[NOM]</ta>
            <ta e="T228" id="Seg_5706" s="T226">AFFIRM</ta>
            <ta e="T230" id="Seg_5707" s="T229">dieses</ta>
            <ta e="T231" id="Seg_5708" s="T230">nun</ta>
            <ta e="T232" id="Seg_5709" s="T231">so-EMPH</ta>
            <ta e="T233" id="Seg_5710" s="T232">Lied-PL-2SG-ACC</ta>
            <ta e="T234" id="Seg_5711" s="T233">Jakut-PL.[NOM]</ta>
            <ta e="T235" id="Seg_5712" s="T234">Lied-3PL-INSTR</ta>
            <ta e="T236" id="Seg_5713" s="T235">so</ta>
            <ta e="T237" id="Seg_5714" s="T236">und</ta>
            <ta e="T238" id="Seg_5715" s="T237">ziehen-PRS-2SG</ta>
            <ta e="T239" id="Seg_5716" s="T238">sein-PST2.[3SG]</ta>
            <ta e="T241" id="Seg_5717" s="T239">AFFIRM</ta>
            <ta e="T242" id="Seg_5718" s="T241">sowieso</ta>
            <ta e="T244" id="Seg_5719" s="T242">eins</ta>
            <ta e="T245" id="Seg_5720" s="T244">jenes</ta>
            <ta e="T246" id="Seg_5721" s="T245">Kopf-2SG-DAT/LOC</ta>
            <ta e="T248" id="Seg_5722" s="T247">äh</ta>
            <ta e="T249" id="Seg_5723" s="T248">befestigen-EP-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T251" id="Seg_5724" s="T249">EMPH</ta>
            <ta e="T252" id="Seg_5725" s="T251">3PL.[NOM]</ta>
            <ta e="T254" id="Seg_5726" s="T252">Lied-3PL.[NOM]</ta>
            <ta e="T280" id="Seg_5727" s="T279">Klub-DAT/LOC</ta>
            <ta e="T283" id="Seg_5728" s="T280">arbeiten-PST2-EP-2SG</ta>
            <ta e="T285" id="Seg_5729" s="T284">dann</ta>
            <ta e="T286" id="Seg_5730" s="T285">Stimme-PROPR-2SG</ta>
            <ta e="T287" id="Seg_5731" s="T286">sehen-CVB.SEQ-3PL</ta>
            <ta e="T288" id="Seg_5732" s="T287">Bühne-3SG.[NOM]</ta>
            <ta e="T289" id="Seg_5733" s="T288">EMPH</ta>
            <ta e="T290" id="Seg_5734" s="T289">hinausgehen-PTCP.PRS</ta>
            <ta e="T292" id="Seg_5735" s="T290">sein-PST1-2SG</ta>
            <ta e="T294" id="Seg_5736" s="T292">AFFIRM</ta>
            <ta e="T315" id="Seg_5737" s="T313">Jakutsk-DAT/LOC</ta>
            <ta e="T316" id="Seg_5738" s="T315">nehmen-PTCP.PST</ta>
            <ta e="T317" id="Seg_5739" s="T316">Urkunde-PL-EP-2SG.[NOM]</ta>
            <ta e="T319" id="Seg_5740" s="T317">Q</ta>
            <ta e="T340" id="Seg_5741" s="T339">was.für.ein</ta>
            <ta e="T341" id="Seg_5742" s="T340">Rajon.[NOM]</ta>
            <ta e="T342" id="Seg_5743" s="T341">sein-PST1-3SG</ta>
            <ta e="T343" id="Seg_5744" s="T342">sagen-PRS-2SG</ta>
            <ta e="T344" id="Seg_5745" s="T343">was.für.ein</ta>
            <ta e="T345" id="Seg_5746" s="T344">Ulus-PL.[NOM]</ta>
            <ta e="T346" id="Seg_5747" s="T345">EMPH</ta>
            <ta e="T348" id="Seg_5748" s="T346">dort</ta>
            <ta e="T352" id="Seg_5749" s="T351">klein</ta>
            <ta e="T353" id="Seg_5750" s="T352">was.[NOM]=Q</ta>
            <ta e="T354" id="Seg_5751" s="T353">Dorf.[NOM]</ta>
            <ta e="T355" id="Seg_5752" s="T354">Q</ta>
            <ta e="T356" id="Seg_5753" s="T355">Stadt.[NOM]</ta>
            <ta e="T357" id="Seg_5754" s="T356">sein-PST1-3SG</ta>
            <ta e="T358" id="Seg_5755" s="T357">Q</ta>
            <ta e="T377" id="Seg_5756" s="T375">wer-PROPR-ACC</ta>
            <ta e="T378" id="Seg_5757" s="T377">machen-PRS-2SG</ta>
            <ta e="T379" id="Seg_5758" s="T378">3PL.[NOM]</ta>
            <ta e="T384" id="Seg_5759" s="T383">ah</ta>
            <ta e="T386" id="Seg_5760" s="T384">dieses</ta>
            <ta e="T388" id="Seg_5761" s="T386">zwei</ta>
            <ta e="T392" id="Seg_5762" s="T391">zwei</ta>
            <ta e="T393" id="Seg_5763" s="T392">Dorf.[NOM]</ta>
            <ta e="T394" id="Seg_5764" s="T393">wie-3PL</ta>
            <ta e="T395" id="Seg_5765" s="T394">AFFIRM</ta>
            <ta e="T396" id="Seg_5766" s="T395">dieses</ta>
            <ta e="T397" id="Seg_5767" s="T396">nun</ta>
            <ta e="T398" id="Seg_5768" s="T397">so-EMPH</ta>
            <ta e="T399" id="Seg_5769" s="T398">Lied-PL-ACC</ta>
            <ta e="T400" id="Seg_5770" s="T399">singen-PRS-1SG</ta>
            <ta e="T401" id="Seg_5771" s="T400">sagen-PRS-2SG</ta>
            <ta e="T402" id="Seg_5772" s="T401">wann-ABL</ta>
            <ta e="T403" id="Seg_5773" s="T402">so-EMPH</ta>
            <ta e="T404" id="Seg_5774" s="T403">echt-SIM</ta>
            <ta e="T405" id="Seg_5775" s="T404">singen-PTCP.PRS</ta>
            <ta e="T406" id="Seg_5776" s="T405">werden-PST2-2SG=Q</ta>
            <ta e="T408" id="Seg_5777" s="T406">EMPH</ta>
            <ta e="T418" id="Seg_5778" s="T417">selbst-2SG.[NOM]</ta>
            <ta e="T419" id="Seg_5779" s="T418">singen-CVB.SEQ</ta>
            <ta e="T420" id="Seg_5780" s="T419">russisch</ta>
            <ta e="T421" id="Seg_5781" s="T420">EMPH</ta>
            <ta e="T422" id="Seg_5782" s="T421">Lied-PL-3SG-ACC</ta>
            <ta e="T423" id="Seg_5783" s="T422">singen-PTCP.HAB</ta>
            <ta e="T424" id="Seg_5784" s="T423">sein-PST1-2SG</ta>
            <ta e="T425" id="Seg_5785" s="T424">dann</ta>
            <ta e="T426" id="Seg_5786" s="T425">äh</ta>
            <ta e="T427" id="Seg_5787" s="T426">Q</ta>
            <ta e="T428" id="Seg_5788" s="T427">dolganisch-SIM</ta>
            <ta e="T429" id="Seg_5789" s="T428">nur</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KuNS">
            <ta e="T31" id="Seg_5790" s="T29">бабушка-2SG.[NOM]</ta>
            <ta e="T32" id="Seg_5791" s="T31">имя-3SG.[NOM]</ta>
            <ta e="T33" id="Seg_5792" s="T32">кто.[NOM]</ta>
            <ta e="T34" id="Seg_5793" s="T33">быть-PST1-3SG=Q</ta>
            <ta e="T92" id="Seg_5794" s="T90">где</ta>
            <ta e="T93" id="Seg_5795" s="T92">учиться-PST2-2SG=Q</ta>
            <ta e="T94" id="Seg_5796" s="T93">EMPH</ta>
            <ta e="T95" id="Seg_5797" s="T94">школа-ACC</ta>
            <ta e="T96" id="Seg_5798" s="T95">EMPH</ta>
            <ta e="T97" id="Seg_5799" s="T96">кончать-EP-CAUS-PST2-EP-2SG</ta>
            <ta e="T123" id="Seg_5800" s="T121">здесь</ta>
            <ta e="T125" id="Seg_5801" s="T123">делать-PRS-2SG</ta>
            <ta e="T126" id="Seg_5802" s="T125">Таймыр-DAT/LOC</ta>
            <ta e="T128" id="Seg_5803" s="T126">делать-PRS-2SG</ta>
            <ta e="T139" id="Seg_5804" s="T138">Дудинка-DAT/LOC</ta>
            <ta e="T140" id="Seg_5805" s="T139">опять</ta>
            <ta e="T142" id="Seg_5806" s="T140">принести-PTCP.PST</ta>
            <ta e="T143" id="Seg_5807" s="T142">быть-PST1-3PL</ta>
            <ta e="T153" id="Seg_5808" s="T152">тогда</ta>
            <ta e="T154" id="Seg_5809" s="T153">что.[NOM]</ta>
            <ta e="T155" id="Seg_5810" s="T154">песня-PL-3SG-ACC</ta>
            <ta e="T156" id="Seg_5811" s="T155">петь-PTCP.PRS</ta>
            <ta e="T157" id="Seg_5812" s="T156">быть-PST1-2SG</ta>
            <ta e="T158" id="Seg_5813" s="T157">маленький.[NOM]</ta>
            <ta e="T160" id="Seg_5814" s="T158">быть-TEMP-2SG</ta>
            <ta e="T173" id="Seg_5815" s="T171">слово-PL-ACC</ta>
            <ta e="T174" id="Seg_5816" s="T173">этот.EMPH</ta>
            <ta e="T175" id="Seg_5817" s="T174">вынимать-CVB.SEQ-2SG</ta>
            <ta e="T177" id="Seg_5818" s="T175">AFFIRM</ta>
            <ta e="T198" id="Seg_5819" s="T197">кто-ACC</ta>
            <ta e="T199" id="Seg_5820" s="T198">слышать-CVB.SEQ</ta>
            <ta e="T200" id="Seg_5821" s="T199">бабушка-2SG.[NOM]</ta>
            <ta e="T201" id="Seg_5822" s="T200">EMPH</ta>
            <ta e="T202" id="Seg_5823" s="T201">петь-PTCP.HAB</ta>
            <ta e="T203" id="Seg_5824" s="T202">быть-PST1-3SG</ta>
            <ta e="T204" id="Seg_5825" s="T203">Q</ta>
            <ta e="T205" id="Seg_5826" s="T204">родители-PL-2SG-ABL</ta>
            <ta e="T206" id="Seg_5827" s="T205">кто.[NOM]</ta>
            <ta e="T207" id="Seg_5828" s="T206">петь-PTCP.HAB</ta>
            <ta e="T209" id="Seg_5829" s="T207">быть-PST1-3SG=Q</ta>
            <ta e="T226" id="Seg_5830" s="T224">далекий.[NOM]</ta>
            <ta e="T228" id="Seg_5831" s="T226">AFFIRM</ta>
            <ta e="T230" id="Seg_5832" s="T229">этот</ta>
            <ta e="T231" id="Seg_5833" s="T230">вот</ta>
            <ta e="T232" id="Seg_5834" s="T231">так-EMPH</ta>
            <ta e="T233" id="Seg_5835" s="T232">песня-PL-2SG-ACC</ta>
            <ta e="T234" id="Seg_5836" s="T233">Якут-PL.[NOM]</ta>
            <ta e="T235" id="Seg_5837" s="T234">песня-3PL-INSTR</ta>
            <ta e="T236" id="Seg_5838" s="T235">так</ta>
            <ta e="T237" id="Seg_5839" s="T236">да</ta>
            <ta e="T238" id="Seg_5840" s="T237">тянуть-PRS-2SG</ta>
            <ta e="T239" id="Seg_5841" s="T238">быть-PST2.[3SG]</ta>
            <ta e="T241" id="Seg_5842" s="T239">AFFIRM</ta>
            <ta e="T242" id="Seg_5843" s="T241">в.любом.случае</ta>
            <ta e="T244" id="Seg_5844" s="T242">один</ta>
            <ta e="T245" id="Seg_5845" s="T244">тот</ta>
            <ta e="T246" id="Seg_5846" s="T245">голова-2SG-DAT/LOC</ta>
            <ta e="T248" id="Seg_5847" s="T247">э</ta>
            <ta e="T249" id="Seg_5848" s="T248">закреплять-EP-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T251" id="Seg_5849" s="T249">EMPH</ta>
            <ta e="T252" id="Seg_5850" s="T251">3PL.[NOM]</ta>
            <ta e="T254" id="Seg_5851" s="T252">песня-3PL.[NOM]</ta>
            <ta e="T280" id="Seg_5852" s="T279">клуб-DAT/LOC</ta>
            <ta e="T283" id="Seg_5853" s="T280">работать-PST2-EP-2SG</ta>
            <ta e="T285" id="Seg_5854" s="T284">потом</ta>
            <ta e="T286" id="Seg_5855" s="T285">голос-PROPR-2SG</ta>
            <ta e="T287" id="Seg_5856" s="T286">видеть-CVB.SEQ-3PL</ta>
            <ta e="T288" id="Seg_5857" s="T287">сцена-3SG.[NOM]</ta>
            <ta e="T289" id="Seg_5858" s="T288">EMPH</ta>
            <ta e="T290" id="Seg_5859" s="T289">выйти-PTCP.PRS</ta>
            <ta e="T292" id="Seg_5860" s="T290">быть-PST1-2SG</ta>
            <ta e="T294" id="Seg_5861" s="T292">AFFIRM</ta>
            <ta e="T315" id="Seg_5862" s="T313">Якутск-DAT/LOC</ta>
            <ta e="T316" id="Seg_5863" s="T315">взять-PTCP.PST</ta>
            <ta e="T317" id="Seg_5864" s="T316">грамота-PL-EP-2SG.[NOM]</ta>
            <ta e="T319" id="Seg_5865" s="T317">Q</ta>
            <ta e="T340" id="Seg_5866" s="T339">какой</ta>
            <ta e="T341" id="Seg_5867" s="T340">район.[NOM]</ta>
            <ta e="T342" id="Seg_5868" s="T341">быть-PST1-3SG</ta>
            <ta e="T343" id="Seg_5869" s="T342">говорить-PRS-2SG</ta>
            <ta e="T344" id="Seg_5870" s="T343">какой</ta>
            <ta e="T345" id="Seg_5871" s="T344">улус-PL.[NOM]</ta>
            <ta e="T346" id="Seg_5872" s="T345">EMPH</ta>
            <ta e="T348" id="Seg_5873" s="T346">там</ta>
            <ta e="T352" id="Seg_5874" s="T351">маленький</ta>
            <ta e="T353" id="Seg_5875" s="T352">что.[NOM]=Q</ta>
            <ta e="T354" id="Seg_5876" s="T353">поселок.[NOM]</ta>
            <ta e="T355" id="Seg_5877" s="T354">Q</ta>
            <ta e="T356" id="Seg_5878" s="T355">город.[NOM]</ta>
            <ta e="T357" id="Seg_5879" s="T356">быть-PST1-3SG</ta>
            <ta e="T358" id="Seg_5880" s="T357">Q</ta>
            <ta e="T377" id="Seg_5881" s="T375">кто-PROPR-ACC</ta>
            <ta e="T378" id="Seg_5882" s="T377">делать-PRS-2SG</ta>
            <ta e="T379" id="Seg_5883" s="T378">3PL.[NOM]</ta>
            <ta e="T384" id="Seg_5884" s="T383">ах</ta>
            <ta e="T386" id="Seg_5885" s="T384">тот</ta>
            <ta e="T388" id="Seg_5886" s="T386">два</ta>
            <ta e="T392" id="Seg_5887" s="T391">два</ta>
            <ta e="T393" id="Seg_5888" s="T392">поселок.[NOM]</ta>
            <ta e="T394" id="Seg_5889" s="T393">как-3PL</ta>
            <ta e="T395" id="Seg_5890" s="T394">AFFIRM</ta>
            <ta e="T396" id="Seg_5891" s="T395">этот</ta>
            <ta e="T397" id="Seg_5892" s="T396">вот</ta>
            <ta e="T398" id="Seg_5893" s="T397">так-EMPH</ta>
            <ta e="T399" id="Seg_5894" s="T398">песня-PL-ACC</ta>
            <ta e="T400" id="Seg_5895" s="T399">петь-PRS-1SG</ta>
            <ta e="T401" id="Seg_5896" s="T400">говорить-PRS-2SG</ta>
            <ta e="T402" id="Seg_5897" s="T401">когда-ABL</ta>
            <ta e="T403" id="Seg_5898" s="T402">так-EMPH</ta>
            <ta e="T404" id="Seg_5899" s="T403">настоящий-SIM</ta>
            <ta e="T405" id="Seg_5900" s="T404">петь-PTCP.PRS</ta>
            <ta e="T406" id="Seg_5901" s="T405">становиться-PST2-2SG=Q</ta>
            <ta e="T408" id="Seg_5902" s="T406">EMPH</ta>
            <ta e="T418" id="Seg_5903" s="T417">сам-2SG.[NOM]</ta>
            <ta e="T419" id="Seg_5904" s="T418">петь-CVB.SEQ</ta>
            <ta e="T420" id="Seg_5905" s="T419">русский</ta>
            <ta e="T421" id="Seg_5906" s="T420">EMPH</ta>
            <ta e="T422" id="Seg_5907" s="T421">песня-PL-3SG-ACC</ta>
            <ta e="T423" id="Seg_5908" s="T422">петь-PTCP.HAB</ta>
            <ta e="T424" id="Seg_5909" s="T423">быть-PST1-2SG</ta>
            <ta e="T425" id="Seg_5910" s="T424">тогда</ta>
            <ta e="T426" id="Seg_5911" s="T425">ээ</ta>
            <ta e="T427" id="Seg_5912" s="T426">Q</ta>
            <ta e="T428" id="Seg_5913" s="T427">долганский-SIM</ta>
            <ta e="T429" id="Seg_5914" s="T428">только</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KuNS">
            <ta e="T31" id="Seg_5915" s="T29">n-n:(poss).[n:case]</ta>
            <ta e="T32" id="Seg_5916" s="T31">n-n:(poss).[n:case]</ta>
            <ta e="T33" id="Seg_5917" s="T32">que.[pro:case]</ta>
            <ta e="T34" id="Seg_5918" s="T33">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T92" id="Seg_5919" s="T90">que</ta>
            <ta e="T93" id="Seg_5920" s="T92">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T94" id="Seg_5921" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_5922" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_5923" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_5924" s="T96">v-v:(ins)-v&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T123" id="Seg_5925" s="T121">adv</ta>
            <ta e="T125" id="Seg_5926" s="T123">v-v:tense-v:pred.pn</ta>
            <ta e="T126" id="Seg_5927" s="T125">propr-n:case</ta>
            <ta e="T128" id="Seg_5928" s="T126">v-v:tense-v:pred.pn</ta>
            <ta e="T139" id="Seg_5929" s="T138">propr-n:case</ta>
            <ta e="T140" id="Seg_5930" s="T139">ptcl</ta>
            <ta e="T142" id="Seg_5931" s="T140">v-v:ptcp</ta>
            <ta e="T143" id="Seg_5932" s="T142">v-v:tense-v:poss.pn</ta>
            <ta e="T153" id="Seg_5933" s="T152">adv</ta>
            <ta e="T154" id="Seg_5934" s="T153">que.[pro:case]</ta>
            <ta e="T155" id="Seg_5935" s="T154">n-n:(num)-n:poss-n:case</ta>
            <ta e="T156" id="Seg_5936" s="T155">v-v:ptcp</ta>
            <ta e="T157" id="Seg_5937" s="T156">v-v:tense-v:poss.pn</ta>
            <ta e="T158" id="Seg_5938" s="T157">adj.[n:case]</ta>
            <ta e="T160" id="Seg_5939" s="T158">v-v:mood-v:temp.pn</ta>
            <ta e="T173" id="Seg_5940" s="T171">n-n:(num)-n:case</ta>
            <ta e="T174" id="Seg_5941" s="T173">dempro</ta>
            <ta e="T175" id="Seg_5942" s="T174">v-v:cvb-v:pred.pn</ta>
            <ta e="T177" id="Seg_5943" s="T175">ptcl</ta>
            <ta e="T198" id="Seg_5944" s="T197">que-pro:case</ta>
            <ta e="T199" id="Seg_5945" s="T198">v-v:cvb</ta>
            <ta e="T200" id="Seg_5946" s="T199">n-n:(poss).[n:case]</ta>
            <ta e="T201" id="Seg_5947" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_5948" s="T201">v-v:ptcp</ta>
            <ta e="T203" id="Seg_5949" s="T202">v-v:tense-v:poss.pn</ta>
            <ta e="T204" id="Seg_5950" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_5951" s="T204">n-n:(num)-n:poss-n:case</ta>
            <ta e="T206" id="Seg_5952" s="T205">que.[pro:case]</ta>
            <ta e="T207" id="Seg_5953" s="T206">v-v:ptcp</ta>
            <ta e="T209" id="Seg_5954" s="T207">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T226" id="Seg_5955" s="T224">adj.[n:case]</ta>
            <ta e="T228" id="Seg_5956" s="T226">ptcl</ta>
            <ta e="T230" id="Seg_5957" s="T229">dempro</ta>
            <ta e="T231" id="Seg_5958" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_5959" s="T231">ptcl-ptcl</ta>
            <ta e="T233" id="Seg_5960" s="T232">n-n:(num)-n:poss-n:case</ta>
            <ta e="T234" id="Seg_5961" s="T233">n-n:(num).[n:case]</ta>
            <ta e="T235" id="Seg_5962" s="T234">n-n:poss-n:case</ta>
            <ta e="T236" id="Seg_5963" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_5964" s="T236">conj</ta>
            <ta e="T238" id="Seg_5965" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_5966" s="T238">v-v:tense.[v:pred.pn]</ta>
            <ta e="T241" id="Seg_5967" s="T239">ptcl</ta>
            <ta e="T242" id="Seg_5968" s="T241">adv</ta>
            <ta e="T244" id="Seg_5969" s="T242">cardnum</ta>
            <ta e="T245" id="Seg_5970" s="T244">dempro</ta>
            <ta e="T246" id="Seg_5971" s="T245">n-n:poss-n:case</ta>
            <ta e="T248" id="Seg_5972" s="T247">interj</ta>
            <ta e="T249" id="Seg_5973" s="T248">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T251" id="Seg_5974" s="T249">ptcl</ta>
            <ta e="T252" id="Seg_5975" s="T251">pers.[pro:case]</ta>
            <ta e="T254" id="Seg_5976" s="T252">n-n:(poss).[n:case]</ta>
            <ta e="T280" id="Seg_5977" s="T279">n-n:case</ta>
            <ta e="T283" id="Seg_5978" s="T280">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T285" id="Seg_5979" s="T284">adv</ta>
            <ta e="T286" id="Seg_5980" s="T285">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T287" id="Seg_5981" s="T286">v-v:cvb-v:pred.pn</ta>
            <ta e="T288" id="Seg_5982" s="T287">n-n:(poss).[n:case]</ta>
            <ta e="T289" id="Seg_5983" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_5984" s="T289">v-v:ptcp</ta>
            <ta e="T292" id="Seg_5985" s="T290">v-v:tense-v:poss.pn</ta>
            <ta e="T294" id="Seg_5986" s="T292">ptcl</ta>
            <ta e="T315" id="Seg_5987" s="T313">propr-n:case</ta>
            <ta e="T316" id="Seg_5988" s="T315">v-v:ptcp</ta>
            <ta e="T317" id="Seg_5989" s="T316">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T319" id="Seg_5990" s="T317">ptcl</ta>
            <ta e="T340" id="Seg_5991" s="T339">que</ta>
            <ta e="T341" id="Seg_5992" s="T340">n.[n:case]</ta>
            <ta e="T342" id="Seg_5993" s="T341">v-v:tense-v:poss.pn</ta>
            <ta e="T343" id="Seg_5994" s="T342">v-v:tense-v:pred.pn</ta>
            <ta e="T344" id="Seg_5995" s="T343">que</ta>
            <ta e="T345" id="Seg_5996" s="T344">n-n:(num).[n:case]</ta>
            <ta e="T346" id="Seg_5997" s="T345">ptcl</ta>
            <ta e="T348" id="Seg_5998" s="T346">adv</ta>
            <ta e="T352" id="Seg_5999" s="T351">adj</ta>
            <ta e="T353" id="Seg_6000" s="T352">que.[pro:case]=ptcl</ta>
            <ta e="T354" id="Seg_6001" s="T353">n.[n:case]</ta>
            <ta e="T355" id="Seg_6002" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_6003" s="T355">n.[n:case]</ta>
            <ta e="T357" id="Seg_6004" s="T356">v-v:tense-v:poss.pn</ta>
            <ta e="T358" id="Seg_6005" s="T357">ptcl</ta>
            <ta e="T377" id="Seg_6006" s="T375">que-que&gt;adj-n:case</ta>
            <ta e="T378" id="Seg_6007" s="T377">v-v:tense-v:pred.pn</ta>
            <ta e="T379" id="Seg_6008" s="T378">pers.[pro:case]</ta>
            <ta e="T384" id="Seg_6009" s="T383">interj</ta>
            <ta e="T386" id="Seg_6010" s="T384">dempro</ta>
            <ta e="T388" id="Seg_6011" s="T386">cardnum</ta>
            <ta e="T392" id="Seg_6012" s="T391">cardnum</ta>
            <ta e="T393" id="Seg_6013" s="T392">n.[n:case]</ta>
            <ta e="T394" id="Seg_6014" s="T393">post-n:(pred.pn)</ta>
            <ta e="T395" id="Seg_6015" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_6016" s="T395">dempro</ta>
            <ta e="T397" id="Seg_6017" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_6018" s="T397">ptcl-ptcl</ta>
            <ta e="T399" id="Seg_6019" s="T398">n-n:(num)-n:case</ta>
            <ta e="T400" id="Seg_6020" s="T399">v-v:tense-v:pred.pn</ta>
            <ta e="T401" id="Seg_6021" s="T400">v-v:tense-v:pred.pn</ta>
            <ta e="T402" id="Seg_6022" s="T401">que-pro:case</ta>
            <ta e="T403" id="Seg_6023" s="T402">ptcl-ptcl</ta>
            <ta e="T404" id="Seg_6024" s="T403">adj-adj&gt;adv</ta>
            <ta e="T405" id="Seg_6025" s="T404">v-v:ptcp</ta>
            <ta e="T406" id="Seg_6026" s="T405">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T408" id="Seg_6027" s="T406">ptcl</ta>
            <ta e="T418" id="Seg_6028" s="T417">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T419" id="Seg_6029" s="T418">v-v:cvb</ta>
            <ta e="T420" id="Seg_6030" s="T419">adj</ta>
            <ta e="T421" id="Seg_6031" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_6032" s="T421">n-n:(num)-n:poss-n:case</ta>
            <ta e="T423" id="Seg_6033" s="T422">v-v:ptcp</ta>
            <ta e="T424" id="Seg_6034" s="T423">v-v:tense-v:poss.pn</ta>
            <ta e="T425" id="Seg_6035" s="T424">adv</ta>
            <ta e="T426" id="Seg_6036" s="T425">interj</ta>
            <ta e="T427" id="Seg_6037" s="T426">ptcl</ta>
            <ta e="T428" id="Seg_6038" s="T427">adj-adj&gt;adv</ta>
            <ta e="T429" id="Seg_6039" s="T428">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KuNS">
            <ta e="T31" id="Seg_6040" s="T29">n</ta>
            <ta e="T32" id="Seg_6041" s="T31">n</ta>
            <ta e="T33" id="Seg_6042" s="T32">que</ta>
            <ta e="T34" id="Seg_6043" s="T33">cop</ta>
            <ta e="T92" id="Seg_6044" s="T90">que</ta>
            <ta e="T93" id="Seg_6045" s="T92">v</ta>
            <ta e="T94" id="Seg_6046" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_6047" s="T94">n</ta>
            <ta e="T96" id="Seg_6048" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_6049" s="T96">v</ta>
            <ta e="T123" id="Seg_6050" s="T121">adv</ta>
            <ta e="T125" id="Seg_6051" s="T123">v</ta>
            <ta e="T126" id="Seg_6052" s="T125">propr</ta>
            <ta e="T128" id="Seg_6053" s="T126">v</ta>
            <ta e="T139" id="Seg_6054" s="T138">propr</ta>
            <ta e="T140" id="Seg_6055" s="T139">ptcl</ta>
            <ta e="T142" id="Seg_6056" s="T140">v</ta>
            <ta e="T143" id="Seg_6057" s="T142">aux</ta>
            <ta e="T153" id="Seg_6058" s="T152">adv</ta>
            <ta e="T154" id="Seg_6059" s="T153">que</ta>
            <ta e="T155" id="Seg_6060" s="T154">n</ta>
            <ta e="T156" id="Seg_6061" s="T155">v</ta>
            <ta e="T157" id="Seg_6062" s="T156">aux</ta>
            <ta e="T158" id="Seg_6063" s="T157">adj</ta>
            <ta e="T160" id="Seg_6064" s="T158">cop</ta>
            <ta e="T173" id="Seg_6065" s="T171">n</ta>
            <ta e="T174" id="Seg_6066" s="T173">dempro</ta>
            <ta e="T175" id="Seg_6067" s="T174">v</ta>
            <ta e="T177" id="Seg_6068" s="T175">ptcl</ta>
            <ta e="T198" id="Seg_6069" s="T197">que</ta>
            <ta e="T199" id="Seg_6070" s="T198">v</ta>
            <ta e="T200" id="Seg_6071" s="T199">n</ta>
            <ta e="T201" id="Seg_6072" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_6073" s="T201">v</ta>
            <ta e="T203" id="Seg_6074" s="T202">aux</ta>
            <ta e="T204" id="Seg_6075" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_6076" s="T204">n</ta>
            <ta e="T206" id="Seg_6077" s="T205">que</ta>
            <ta e="T207" id="Seg_6078" s="T206">v</ta>
            <ta e="T209" id="Seg_6079" s="T207">aux</ta>
            <ta e="T226" id="Seg_6080" s="T224">adj</ta>
            <ta e="T228" id="Seg_6081" s="T226">ptcl</ta>
            <ta e="T230" id="Seg_6082" s="T229">dempro</ta>
            <ta e="T231" id="Seg_6083" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_6084" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_6085" s="T232">n</ta>
            <ta e="T234" id="Seg_6086" s="T233">n</ta>
            <ta e="T235" id="Seg_6087" s="T234">n</ta>
            <ta e="T236" id="Seg_6088" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_6089" s="T236">conj</ta>
            <ta e="T238" id="Seg_6090" s="T237">v</ta>
            <ta e="T239" id="Seg_6091" s="T238">aux</ta>
            <ta e="T241" id="Seg_6092" s="T239">ptcl</ta>
            <ta e="T242" id="Seg_6093" s="T241">adv</ta>
            <ta e="T244" id="Seg_6094" s="T242">cardnum</ta>
            <ta e="T245" id="Seg_6095" s="T244">dempro</ta>
            <ta e="T246" id="Seg_6096" s="T245">n</ta>
            <ta e="T248" id="Seg_6097" s="T247">interj</ta>
            <ta e="T249" id="Seg_6098" s="T248">v</ta>
            <ta e="T251" id="Seg_6099" s="T249">ptcl</ta>
            <ta e="T252" id="Seg_6100" s="T251">pers</ta>
            <ta e="T254" id="Seg_6101" s="T252">n</ta>
            <ta e="T280" id="Seg_6102" s="T279">n</ta>
            <ta e="T283" id="Seg_6103" s="T280">v</ta>
            <ta e="T285" id="Seg_6104" s="T284">adv</ta>
            <ta e="T286" id="Seg_6105" s="T285">adj</ta>
            <ta e="T287" id="Seg_6106" s="T286">v</ta>
            <ta e="T288" id="Seg_6107" s="T287">n</ta>
            <ta e="T289" id="Seg_6108" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_6109" s="T289">v</ta>
            <ta e="T292" id="Seg_6110" s="T290">aux</ta>
            <ta e="T294" id="Seg_6111" s="T292">ptcl</ta>
            <ta e="T315" id="Seg_6112" s="T313">propr</ta>
            <ta e="T316" id="Seg_6113" s="T315">v</ta>
            <ta e="T317" id="Seg_6114" s="T316">n</ta>
            <ta e="T319" id="Seg_6115" s="T317">ptcl</ta>
            <ta e="T340" id="Seg_6116" s="T339">que</ta>
            <ta e="T341" id="Seg_6117" s="T340">n</ta>
            <ta e="T342" id="Seg_6118" s="T341">cop</ta>
            <ta e="T343" id="Seg_6119" s="T342">v</ta>
            <ta e="T344" id="Seg_6120" s="T343">que</ta>
            <ta e="T345" id="Seg_6121" s="T344">n</ta>
            <ta e="T346" id="Seg_6122" s="T345">ptcl</ta>
            <ta e="T348" id="Seg_6123" s="T346">adv</ta>
            <ta e="T352" id="Seg_6124" s="T351">adj</ta>
            <ta e="T353" id="Seg_6125" s="T352">que</ta>
            <ta e="T354" id="Seg_6126" s="T353">n</ta>
            <ta e="T355" id="Seg_6127" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_6128" s="T355">n</ta>
            <ta e="T357" id="Seg_6129" s="T356">cop</ta>
            <ta e="T358" id="Seg_6130" s="T357">ptcl</ta>
            <ta e="T377" id="Seg_6131" s="T375">adj</ta>
            <ta e="T378" id="Seg_6132" s="T377">v</ta>
            <ta e="T379" id="Seg_6133" s="T378">pers</ta>
            <ta e="T384" id="Seg_6134" s="T383">interj</ta>
            <ta e="T386" id="Seg_6135" s="T384">dempro</ta>
            <ta e="T388" id="Seg_6136" s="T386">cardnum</ta>
            <ta e="T392" id="Seg_6137" s="T391">cardnum</ta>
            <ta e="T393" id="Seg_6138" s="T392">n</ta>
            <ta e="T394" id="Seg_6139" s="T393">post</ta>
            <ta e="T395" id="Seg_6140" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_6141" s="T395">dempro</ta>
            <ta e="T397" id="Seg_6142" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_6143" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_6144" s="T398">n</ta>
            <ta e="T400" id="Seg_6145" s="T399">v</ta>
            <ta e="T401" id="Seg_6146" s="T400">v</ta>
            <ta e="T402" id="Seg_6147" s="T401">que</ta>
            <ta e="T403" id="Seg_6148" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_6149" s="T403">adv</ta>
            <ta e="T405" id="Seg_6150" s="T404">v</ta>
            <ta e="T406" id="Seg_6151" s="T405">aux</ta>
            <ta e="T408" id="Seg_6152" s="T406">ptcl</ta>
            <ta e="T418" id="Seg_6153" s="T417">emphpro</ta>
            <ta e="T419" id="Seg_6154" s="T418">v</ta>
            <ta e="T420" id="Seg_6155" s="T419">adj</ta>
            <ta e="T421" id="Seg_6156" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_6157" s="T421">n</ta>
            <ta e="T423" id="Seg_6158" s="T422">v</ta>
            <ta e="T424" id="Seg_6159" s="T423">aux</ta>
            <ta e="T425" id="Seg_6160" s="T424">adv</ta>
            <ta e="T426" id="Seg_6161" s="T425">interj</ta>
            <ta e="T427" id="Seg_6162" s="T426">ptcl</ta>
            <ta e="T428" id="Seg_6163" s="T427">adv</ta>
            <ta e="T429" id="Seg_6164" s="T428">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KuNS">
            <ta e="T31" id="Seg_6165" s="T29">0.2.h:Poss np.h:Poss</ta>
            <ta e="T32" id="Seg_6166" s="T31">np:Th</ta>
            <ta e="T92" id="Seg_6167" s="T90">pro:L</ta>
            <ta e="T93" id="Seg_6168" s="T92">0.2.h:A</ta>
            <ta e="T95" id="Seg_6169" s="T94">np:Th</ta>
            <ta e="T97" id="Seg_6170" s="T96">0.2.h:A</ta>
            <ta e="T123" id="Seg_6171" s="T121">adv:L</ta>
            <ta e="T126" id="Seg_6172" s="T125">np:L</ta>
            <ta e="T139" id="Seg_6173" s="T138">np:G</ta>
            <ta e="T143" id="Seg_6174" s="T140">0.3.h:A</ta>
            <ta e="T155" id="Seg_6175" s="T154">np:Th</ta>
            <ta e="T157" id="Seg_6176" s="T155">0.2.h:A</ta>
            <ta e="T160" id="Seg_6177" s="T158">0.2.h:Th</ta>
            <ta e="T173" id="Seg_6178" s="T171">np:Th</ta>
            <ta e="T175" id="Seg_6179" s="T174">0.2.h:A</ta>
            <ta e="T200" id="Seg_6180" s="T199">0.2.h:Poss np.h:A</ta>
            <ta e="T205" id="Seg_6181" s="T204">0.2.h:Poss</ta>
            <ta e="T206" id="Seg_6182" s="T205">pro.h:A</ta>
            <ta e="T233" id="Seg_6183" s="T232">0.2.h:Poss np:Th</ta>
            <ta e="T239" id="Seg_6184" s="T237">0.2.h:A</ta>
            <ta e="T246" id="Seg_6185" s="T245">0.2.h:Poss np:L</ta>
            <ta e="T252" id="Seg_6186" s="T251">pro.h:Poss</ta>
            <ta e="T254" id="Seg_6187" s="T252">np:Th</ta>
            <ta e="T280" id="Seg_6188" s="T279">np:L</ta>
            <ta e="T283" id="Seg_6189" s="T280">0.2.h:A</ta>
            <ta e="T287" id="Seg_6190" s="T286">0.3.h:E</ta>
            <ta e="T292" id="Seg_6191" s="T289">0.2.h:A</ta>
            <ta e="T315" id="Seg_6192" s="T313">np:L</ta>
            <ta e="T317" id="Seg_6193" s="T316">0.2.h:Poss</ta>
            <ta e="T342" id="Seg_6194" s="T341">0.3:Th</ta>
            <ta e="T343" id="Seg_6195" s="T342">0.2.h:A</ta>
            <ta e="T357" id="Seg_6196" s="T356">0.3:Th</ta>
            <ta e="T377" id="Seg_6197" s="T375">pro.h:Th</ta>
            <ta e="T378" id="Seg_6198" s="T377">0.2.h:A</ta>
            <ta e="T393" id="Seg_6199" s="T392">0.3:Th</ta>
            <ta e="T399" id="Seg_6200" s="T398">np:Th</ta>
            <ta e="T400" id="Seg_6201" s="T399">0.1.h:A</ta>
            <ta e="T401" id="Seg_6202" s="T400">0.2.h:A</ta>
            <ta e="T402" id="Seg_6203" s="T401">pro:Time</ta>
            <ta e="T406" id="Seg_6204" s="T404">0.2.h:A</ta>
            <ta e="T422" id="Seg_6205" s="T421">np:Th</ta>
            <ta e="T424" id="Seg_6206" s="T422">0.2.h:A</ta>
            <ta e="T425" id="Seg_6207" s="T424">adv:Time</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-KuNS">
            <ta e="T32" id="Seg_6208" s="T31">np:S</ta>
            <ta e="T33" id="Seg_6209" s="T32">pro:pred</ta>
            <ta e="T34" id="Seg_6210" s="T33">cop</ta>
            <ta e="T93" id="Seg_6211" s="T92">0.2.h:S v:pred</ta>
            <ta e="T95" id="Seg_6212" s="T94">np:O</ta>
            <ta e="T97" id="Seg_6213" s="T96">0.2.h:S v:pred</ta>
            <ta e="T125" id="Seg_6214" s="T123">0.2.h:S v:pred</ta>
            <ta e="T128" id="Seg_6215" s="T126">0.2.h:S v:pred</ta>
            <ta e="T143" id="Seg_6216" s="T140">0.3.h:S v:pred</ta>
            <ta e="T155" id="Seg_6217" s="T154">np:O</ta>
            <ta e="T157" id="Seg_6218" s="T155">0.2.h:S v:pred</ta>
            <ta e="T160" id="Seg_6219" s="T157">s:temp</ta>
            <ta e="T175" id="Seg_6220" s="T171">s:adv</ta>
            <ta e="T199" id="Seg_6221" s="T197">s:adv</ta>
            <ta e="T200" id="Seg_6222" s="T199">np.h:S</ta>
            <ta e="T203" id="Seg_6223" s="T201">v:pred</ta>
            <ta e="T206" id="Seg_6224" s="T205">pro.h:S</ta>
            <ta e="T209" id="Seg_6225" s="T206">v:pred</ta>
            <ta e="T233" id="Seg_6226" s="T232">np:O</ta>
            <ta e="T239" id="Seg_6227" s="T237">0.2.h:S v:pred</ta>
            <ta e="T249" id="Seg_6228" s="T248">v:pred</ta>
            <ta e="T254" id="Seg_6229" s="T252">np:S</ta>
            <ta e="T283" id="Seg_6230" s="T280">0.2.h:S v:pred</ta>
            <ta e="T287" id="Seg_6231" s="T284">s:temp</ta>
            <ta e="T292" id="Seg_6232" s="T289">0.2.h:S v:pred</ta>
            <ta e="T316" id="Seg_6233" s="T313">s:rel</ta>
            <ta e="T341" id="Seg_6234" s="T340">n:pred</ta>
            <ta e="T342" id="Seg_6235" s="T341">0.3:S cop</ta>
            <ta e="T343" id="Seg_6236" s="T342">0.2.h:S v:pred</ta>
            <ta e="T354" id="Seg_6237" s="T353">n:pred</ta>
            <ta e="T356" id="Seg_6238" s="T355">n:pred</ta>
            <ta e="T357" id="Seg_6239" s="T356">0.3:S cop</ta>
            <ta e="T377" id="Seg_6240" s="T375">pro.h:O</ta>
            <ta e="T378" id="Seg_6241" s="T377">0.2.h:S v:pred</ta>
            <ta e="T393" id="Seg_6242" s="T392">0.3:S n:pred</ta>
            <ta e="T399" id="Seg_6243" s="T398">np:O</ta>
            <ta e="T400" id="Seg_6244" s="T399">0.1.h:S v:pred</ta>
            <ta e="T401" id="Seg_6245" s="T400">0.2.h:S v:pred</ta>
            <ta e="T406" id="Seg_6246" s="T404">0.2.h:S v:pred</ta>
            <ta e="T419" id="Seg_6247" s="T417">s:adv</ta>
            <ta e="T422" id="Seg_6248" s="T421">np:O</ta>
            <ta e="T424" id="Seg_6249" s="T422">0.2.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-KuNS" />
         <annotation name="Top" tierref="Top-KuNS" />
         <annotation name="Foc" tierref="Foc-KuNS" />
         <annotation name="BOR" tierref="BOR-KuNS">
            <ta e="T95" id="Seg_6250" s="T94">RUS:cult</ta>
            <ta e="T139" id="Seg_6251" s="T138">RUS:cult</ta>
            <ta e="T232" id="Seg_6252" s="T231">RUS:disc RUS:disc</ta>
            <ta e="T234" id="Seg_6253" s="T233">RUS:cult</ta>
            <ta e="T236" id="Seg_6254" s="T235">RUS:disc</ta>
            <ta e="T237" id="Seg_6255" s="T236">RUS:gram</ta>
            <ta e="T280" id="Seg_6256" s="T279">RUS:cult</ta>
            <ta e="T288" id="Seg_6257" s="T287">RUS:cult</ta>
            <ta e="T315" id="Seg_6258" s="T313">RUS:cult</ta>
            <ta e="T317" id="Seg_6259" s="T316">RUS:cult</ta>
            <ta e="T341" id="Seg_6260" s="T340">RUS:cult</ta>
            <ta e="T345" id="Seg_6261" s="T344">YAK:cult</ta>
            <ta e="T354" id="Seg_6262" s="T353">RUS:cult</ta>
            <ta e="T356" id="Seg_6263" s="T355">RUS:cult</ta>
            <ta e="T393" id="Seg_6264" s="T392">RUS:cult</ta>
            <ta e="T398" id="Seg_6265" s="T397">RUS:disc RUS:disc</ta>
            <ta e="T403" id="Seg_6266" s="T402">RUS:disc RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KuNS">
            <ta e="T95" id="Seg_6267" s="T94">inVins Csub Vsub</ta>
            <ta e="T356" id="Seg_6268" s="T355">fortition</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-KuNS">
            <ta e="T95" id="Seg_6269" s="T94">dir:infl</ta>
            <ta e="T139" id="Seg_6270" s="T138">dir:infl</ta>
            <ta e="T232" id="Seg_6271" s="T231">dir:bare dir:bare</ta>
            <ta e="T234" id="Seg_6272" s="T233">dir:bare</ta>
            <ta e="T236" id="Seg_6273" s="T235">dir:bare</ta>
            <ta e="T237" id="Seg_6274" s="T236">dir:bare</ta>
            <ta e="T280" id="Seg_6275" s="T279">dir:infl</ta>
            <ta e="T288" id="Seg_6276" s="T287">dir:infl</ta>
            <ta e="T315" id="Seg_6277" s="T313">indir:infl</ta>
            <ta e="T317" id="Seg_6278" s="T316">dir:infl</ta>
            <ta e="T341" id="Seg_6279" s="T340">dir:bare</ta>
            <ta e="T345" id="Seg_6280" s="T344">dir:bare</ta>
            <ta e="T354" id="Seg_6281" s="T353">dir:bare</ta>
            <ta e="T356" id="Seg_6282" s="T355">dir:bare</ta>
            <ta e="T393" id="Seg_6283" s="T392">dir:bare</ta>
            <ta e="T398" id="Seg_6284" s="T397">dir:bare dir:bare</ta>
            <ta e="T403" id="Seg_6285" s="T402">dir:bare dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-KuNS" />
         <annotation name="fe" tierref="fe-KuNS">
            <ta e="T34" id="Seg_6286" s="T29">– How was your grandmother called?</ta>
            <ta e="T97" id="Seg_6287" s="T90">– Where did you learn and finish school?</ta>
            <ta e="T128" id="Seg_6288" s="T121">– Do you mean here, do you mean on Taimyr?</ta>
            <ta e="T145" id="Seg_6289" s="T138">– They took (…) again to Dudinka.</ta>
            <ta e="T160" id="Seg_6290" s="T152">– What kind songs did you sing, when you were small?</ta>
            <ta e="T177" id="Seg_6291" s="T171">– Just taking words, right?</ta>
            <ta e="T209" id="Seg_6292" s="T197">– Hearing whom, did your grandmother sing, who of your parents did sing?</ta>
            <ta e="T228" id="Seg_6293" s="T224">– Long ago, yes?</ta>
            <ta e="T241" id="Seg_6294" s="T229">– And so you get(?) your songs like the Yakut songs, right?</ta>
            <ta e="T244" id="Seg_6295" s="T241">– In any case…</ta>
            <ta e="T254" id="Seg_6296" s="T244">– They stayed in your head, their songs. </ta>
            <ta e="T283" id="Seg_6297" s="T279">– Did you work in a club?</ta>
            <ta e="T294" id="Seg_6298" s="T284">– And then, when they saw that you have [a good] voice, you also went onto the stage, right?</ta>
            <ta e="T319" id="Seg_6299" s="T313">– Certificates which you got in Yakutsk?</ta>
            <ta e="T348" id="Seg_6300" s="T339">– Which rayon was that, you say, which districts there?</ta>
            <ta e="T358" id="Seg_6301" s="T351">– A small thing, was it a village or a town?</ta>
            <ta e="T379" id="Seg_6302" s="T375">– What do you mean, saying "they"?</ta>
            <ta e="T388" id="Seg_6303" s="T383">– Ah, those two.</ta>
            <ta e="T395" id="Seg_6304" s="T391">– Like two settlements, right?</ta>
            <ta e="T408" id="Seg_6305" s="T395">"And so I am singing songs" you say, when did you start to sing seriously?</ta>
            <ta e="T419" id="Seg_6306" s="T417">– You yourself singing…</ta>
            <ta e="T429" id="Seg_6307" s="T419">Did you sing Russian songs then, or only in Dolgan?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KuNS">
            <ta e="T34" id="Seg_6308" s="T29">– Wie hieß deine Großmutter?</ta>
            <ta e="T97" id="Seg_6309" s="T90">– Wo hast du gelernt und die Schule beendet?</ta>
            <ta e="T128" id="Seg_6310" s="T121">– Meinst du hier, meinst du auf der Taimyr?</ta>
            <ta e="T145" id="Seg_6311" s="T138">– Man hat (…) wieder nach Dudinka gebracht.</ta>
            <ta e="T160" id="Seg_6312" s="T152">– Was für Lieder hast du gesungen, als du klein warst?</ta>
            <ta e="T177" id="Seg_6313" s="T171">– Einfach so Wörter nehmend, ja?</ta>
            <ta e="T209" id="Seg_6314" s="T197">– Wen hast du gehört, hat deine Oma gesungen, wer von deinen Eltern hat gesungen?</ta>
            <ta e="T228" id="Seg_6315" s="T224">– Vor langer Zeit, ja?</ta>
            <ta e="T241" id="Seg_6316" s="T229">– Und so holst(?) du deine Lieder wohl wie die jakutischen Lieder, ja?</ta>
            <ta e="T244" id="Seg_6317" s="T241">– Auf jeden Fall…</ta>
            <ta e="T254" id="Seg_6318" s="T244">– In deinem Kopf sind sie hängen geblieben, ihre Lieder.</ta>
            <ta e="T283" id="Seg_6319" s="T279">– Du hast im Klub gearbeitet?</ta>
            <ta e="T294" id="Seg_6320" s="T284">– Und dann, als sie sahen, dass du [eine gute] Stimme hast, bist du auch auf die Bühne gegangen, ja?</ta>
            <ta e="T319" id="Seg_6321" s="T313">– Urkunden, die du in Jakutsk bekommen hast?</ta>
            <ta e="T348" id="Seg_6322" s="T339">– Welcher Rajon war das, sagst du, welche Kreise dort?</ta>
            <ta e="T358" id="Seg_6323" s="T351">– Ein kleines Dings, war das ein Dorf oder eine Stadt?</ta>
            <ta e="T379" id="Seg_6324" s="T375">– Wen meinst du [mit] "sie"?</ta>
            <ta e="T388" id="Seg_6325" s="T383">– Ah, die beiden.</ta>
            <ta e="T395" id="Seg_6326" s="T391">– Sie sind wie zwei Siedlungen, ja?</ta>
            <ta e="T408" id="Seg_6327" s="T395">"Und so singe ich Lieder", sagst du, wann hast du angefangen, wirklich zu singen?</ta>
            <ta e="T419" id="Seg_6328" s="T417">– Du selbst singend…</ta>
            <ta e="T429" id="Seg_6329" s="T419">Hast du damals russische Lieder gesungen, oder nur auf Dolganisch?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KuNS" />
         <annotation name="ltr" tierref="ltr-KuNS">
            <ta e="T34" id="Seg_6330" s="T29">К: Бабушку твою как звали?</ta>
            <ta e="T97" id="Seg_6331" s="T90">К: Где учился ты и школу закончил?</ta>
            <ta e="T128" id="Seg_6332" s="T121">К: Здесь, имеешь ввиду на Таймыре?</ta>
            <ta e="T145" id="Seg_6333" s="T138">К: В Дудинку опять привезли.</ta>
            <ta e="T160" id="Seg_6334" s="T152">К: Тогда какие песни пели когда маленький был?</ta>
            <ta e="T177" id="Seg_6335" s="T171">К: Слова тут же сочиняя, да?</ta>
            <ta e="T209" id="Seg_6336" s="T197">К: Кого слушая, может, бабушка пела или кто-нибудь из родителей пели?</ta>
            <ta e="T228" id="Seg_6337" s="T224">К: Где-то в прошлом, да?</ta>
            <ta e="T241" id="Seg_6338" s="T229">К: А вот так-то песни твои на якутский манер так-то, да?</ta>
            <ta e="T244" id="Seg_6339" s="T241">К: Все равно…</ta>
            <ta e="T254" id="Seg_6340" s="T244">К: В памяти остались, наверно, их песни.</ta>
            <ta e="T283" id="Seg_6341" s="T279">К: В клубе работал.</ta>
            <ta e="T294" id="Seg_6342" s="T284">К: И потом, увидев твой красивый голос, и на сцену стал выходить, да?</ta>
            <ta e="T319" id="Seg_6343" s="T313">К: В Якутске которые получил грамоты что ли?</ta>
            <ta e="T348" id="Seg_6344" s="T339">К: Какой район был, говоришь, какой, улусы же там?</ta>
            <ta e="T358" id="Seg_6345" s="T351">К: Маленький что, или посёлок, или город был?</ta>
            <ta e="T379" id="Seg_6346" s="T375">К: Кого ты имеешь ввиду, они?</ta>
            <ta e="T388" id="Seg_6347" s="T383">К: А-а эти двое.</ta>
            <ta e="T395" id="Seg_6348" s="T391">К: Как два посёлка, да?</ta>
            <ta e="T408" id="Seg_6349" s="T395">К: А вот так-то песни поёшь, говоришь, с какого времени (возраста) так-то серьёзно стал петь?</ta>
            <ta e="T419" id="Seg_6350" s="T417">К: Сам исполняя…</ta>
            <ta e="T429" id="Seg_6351" s="T419">И русские песни пел тогда, да, или же только по-долгански?</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KuNS">
            <ta e="T177" id="Seg_6352" s="T171">[DCh]: It is meant that the singer just sings without having a predefined text.</ta>
            <ta e="T348" id="Seg_6353" s="T339">[DCh]: "ulus" designates historically a smaller administration unit in Turkic and Mongolic communities.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-ZhNA"
                          name="ref"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-ZhNA"
                          name="st"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-ZhNA"
                          name="ts"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-ZhNA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-ZhNA"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-ZhNA"
                          name="mb"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-ZhNA"
                          name="mp"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-ZhNA"
                          name="ge"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-ZhNA"
                          name="gg"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-ZhNA"
                          name="gr"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-ZhNA"
                          name="mc"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-ZhNA"
                          name="ps"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-ZhNA"
                          name="SeR"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-ZhNA"
                          name="SyF"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-ZhNA"
                          name="IST"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-ZhNA"
                          name="Top"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-ZhNA"
                          name="Foc"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-ZhNA"
                          name="BOR"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-ZhNA"
                          name="BOR-Phon"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-ZhNA"
                          name="BOR-Morph"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-ZhNA"
                          name="CS"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-ZhNA"
                          name="fe"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-ZhNA"
                          name="fg"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-ZhNA"
                          name="fr"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-ZhNA"
                          name="ltr"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-ZhNA"
                          name="nt"
                          segmented-tier-id="tx-ZhNA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KuNS"
                          name="ref"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KuNS"
                          name="st"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KuNS"
                          name="ts"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KuNS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KuNS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KuNS"
                          name="mb"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KuNS"
                          name="mp"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KuNS"
                          name="ge"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KuNS"
                          name="gg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KuNS"
                          name="gr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KuNS"
                          name="mc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KuNS"
                          name="ps"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KuNS"
                          name="SeR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KuNS"
                          name="SyF"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KuNS"
                          name="IST"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KuNS"
                          name="Top"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KuNS"
                          name="Foc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KuNS"
                          name="BOR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KuNS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KuNS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KuNS"
                          name="CS"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KuNS"
                          name="fe"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KuNS"
                          name="fg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KuNS"
                          name="fr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KuNS"
                          name="ltr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KuNS"
                          name="nt"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
