<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID0644A115-A295-6B5C-AF4B-43D3B2DC4177">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>UkOA_KuNS_20XX_LifeNovorybnoe_conv</transcription-name>
         <referenced-file url="UkOA_KuNS_20XX_LifeNovorybnoe_conv.wav" />
         <referenced-file url="UkOA_KuNS_20XX_LifeNovorybnoe_conv.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\conv\UkOA_KuNS_20XX_LifeNovorybnoe_conv\UkOA_KuNS_20XX_LifeNovorybnoe_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1035</ud-information>
            <ud-information attribute-name="# HIAT:w">752</ud-information>
            <ud-information attribute-name="# e">750</ud-information>
            <ud-information attribute-name="# HIAT:u">72</ud-information>
            <ud-information attribute-name="# sc">40</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KuNS">
            <abbreviation>KuNS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="UkOA">
            <abbreviation>UkOA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.6716666666666667" type="appl" />
         <tli id="T3" time="1.3433333333333335" type="appl" />
         <tli id="T4" time="2.015" type="appl" />
         <tli id="T5" time="2.686666666666667" type="appl" />
         <tli id="T6" time="3.358333333333334" type="appl" />
         <tli id="T7" time="4.03" type="appl" />
         <tli id="T8" time="4.481166666666667" type="appl" />
         <tli id="T9" time="4.932333333333334" type="appl" />
         <tli id="T10" time="5.383500000000001" type="appl" />
         <tli id="T11" time="5.834666666666667" type="appl" />
         <tli id="T12" time="6.285833333333334" type="appl" />
         <tli id="T13" time="6.737" type="appl" />
         <tli id="T14" time="7.1881666666666675" type="appl" />
         <tli id="T15" time="7.639333333333334" type="appl" />
         <tli id="T16" time="8.0905" type="appl" />
         <tli id="T17" time="8.541666666666668" type="appl" />
         <tli id="T18" time="8.992833333333333" type="appl" />
         <tli id="T19" time="9.444" type="appl" />
         <tli id="T20" time="10.038333333333334" type="appl" />
         <tli id="T21" time="10.632666666666667" type="appl" />
         <tli id="T22" time="11.227" type="appl" />
         <tli id="T23" time="11.821333333333333" type="appl" />
         <tli id="T24" time="12.415666666666667" type="appl" />
         <tli id="T25" time="13.01" type="appl" />
         <tli id="T26" time="13.511666666666667" type="appl" />
         <tli id="T27" time="14.013333333333334" type="appl" />
         <tli id="T28" time="14.515" type="appl" />
         <tli id="T29" time="15.016666666666666" type="appl" />
         <tli id="T30" time="15.518333333333333" type="appl" />
         <tli id="T31" time="16.02" type="appl" />
         <tli id="T32" time="16.506777777777778" type="appl" />
         <tli id="T33" time="16.993555555555556" type="appl" />
         <tli id="T34" time="17.480333333333334" type="appl" />
         <tli id="T35" time="17.967111111111112" type="appl" />
         <tli id="T36" time="18.45388888888889" type="appl" />
         <tli id="T37" time="18.940666666666665" type="appl" />
         <tli id="T38" time="19.427444444444443" type="appl" />
         <tli id="T39" time="19.91422222222222" type="appl" />
         <tli id="T40" time="20.401" type="appl" />
         <tli id="T41" time="21.028470588235294" type="appl" />
         <tli id="T42" time="21.655941176470588" type="appl" />
         <tli id="T43" time="22.283411764705882" type="appl" />
         <tli id="T44" time="22.910882352941176" type="appl" />
         <tli id="T45" time="23.53835294117647" type="appl" />
         <tli id="T46" time="24.165823529411764" type="appl" />
         <tli id="T47" time="24.793294117647058" type="appl" />
         <tli id="T48" time="25.420764705882355" type="appl" />
         <tli id="T49" time="26.048235294117646" type="appl" />
         <tli id="T50" time="26.675705882352943" type="appl" />
         <tli id="T51" time="27.303176470588237" type="appl" />
         <tli id="T52" time="27.93064705882353" type="appl" />
         <tli id="T53" time="28.558117647058825" type="appl" />
         <tli id="T54" time="29.18558823529412" type="appl" />
         <tli id="T55" time="29.813058823529413" type="appl" />
         <tli id="T56" time="30.440529411764707" type="appl" />
         <tli id="T57" time="31.068" type="appl" />
         <tli id="T58" time="31.744" type="appl" />
         <tli id="T59" time="32.42" type="appl" />
         <tli id="T60" time="32.832166666666666" type="appl" />
         <tli id="T61" time="33.24433333333334" type="appl" />
         <tli id="T62" time="33.6565" type="appl" />
         <tli id="T63" time="34.068666666666665" type="appl" />
         <tli id="T64" time="34.48083333333334" type="appl" />
         <tli id="T65" time="34.893" type="appl" />
         <tli id="T66" time="35.516" type="appl" />
         <tli id="T67" time="36.139" type="appl" />
         <tli id="T68" time="36.762" type="appl" />
         <tli id="T69" time="37.385" type="appl" />
         <tli id="T70" time="38.008" type="appl" />
         <tli id="T71" time="38.631" type="appl" />
         <tli id="T72" time="39.254000000000005" type="appl" />
         <tli id="T73" time="39.877" type="appl" />
         <tli id="T74" time="40.5" type="appl" />
         <tli id="T75" time="41.123000000000005" type="appl" />
         <tli id="T76" time="41.746" type="appl" />
         <tli id="T77" time="42.369" type="appl" />
         <tli id="T78" time="42.992000000000004" type="appl" />
         <tli id="T79" time="43.615" type="appl" />
         <tli id="T80" time="44.266" type="appl" />
         <tli id="T81" time="44.917" type="appl" />
         <tli id="T82" time="45.568" type="appl" />
         <tli id="T83" time="46.219" type="appl" />
         <tli id="T84" time="46.87" type="appl" />
         <tli id="T85" time="47.413333333333334" type="appl" />
         <tli id="T86" time="47.95666666666666" type="appl" />
         <tli id="T87" time="48.5" type="appl" />
         <tli id="T88" time="49.04333333333334" type="appl" />
         <tli id="T89" time="49.586666666666666" type="appl" />
         <tli id="T90" time="50.13" type="appl" />
         <tli id="T91" time="50.645625" type="appl" />
         <tli id="T92" time="51.16125" type="appl" />
         <tli id="T93" time="51.676875" type="appl" />
         <tli id="T94" time="52.1925" type="appl" />
         <tli id="T95" time="52.708125" type="appl" />
         <tli id="T96" time="53.22375" type="appl" />
         <tli id="T97" time="53.739375" type="appl" />
         <tli id="T98" time="54.255" type="appl" />
         <tli id="T99" time="54.770625" type="appl" />
         <tli id="T100" time="55.28625" type="appl" />
         <tli id="T101" time="55.801875" type="appl" />
         <tli id="T102" time="56.3175" type="appl" />
         <tli id="T103" time="56.833125" type="appl" />
         <tli id="T104" time="57.34875" type="appl" />
         <tli id="T105" time="57.864375" type="appl" />
         <tli id="T106" time="58.38" type="appl" />
         <tli id="T107" time="59.2164" type="appl" />
         <tli id="T108" time="60.0528" type="appl" />
         <tli id="T109" time="60.8892" type="appl" />
         <tli id="T110" time="61.7256" type="appl" />
         <tli id="T111" time="62.562" type="appl" />
         <tli id="T112" time="62.95171428571428" type="appl" />
         <tli id="T113" time="63.34142857142857" type="appl" />
         <tli id="T114" time="63.731142857142856" type="appl" />
         <tli id="T115" time="64.12085714285715" type="appl" />
         <tli id="T116" time="64.51057142857144" type="appl" />
         <tli id="T117" time="64.90028571428572" type="appl" />
         <tli id="T118" time="65.27" type="appl" />
         <tli id="T119" time="65.29" type="appl" />
         <tli id="T120" time="65.99333333333333" type="appl" />
         <tli id="T121" time="66.71666666666667" type="appl" />
         <tli id="T122" time="67.44" type="appl" />
         <tli id="T123" time="67.45" type="appl" />
         <tli id="T124" time="67.914" type="appl" />
         <tli id="T125" time="68.378" type="appl" />
         <tli id="T126" time="68.842" type="appl" />
         <tli id="T127" time="69.306" type="appl" />
         <tli id="T128" time="69.95657894736841" type="appl" />
         <tli id="T129" time="70.60715789473684" type="appl" />
         <tli id="T130" time="71.25773684210526" type="appl" />
         <tli id="T131" time="71.90831578947368" type="appl" />
         <tli id="T132" time="72.5588947368421" type="appl" />
         <tli id="T133" time="73.20947368421052" type="appl" />
         <tli id="T134" time="73.86005263157895" type="appl" />
         <tli id="T135" time="74.51063157894737" type="appl" />
         <tli id="T136" time="75.16121052631578" type="appl" />
         <tli id="T137" time="75.81178947368421" type="appl" />
         <tli id="T138" time="76.46236842105263" type="appl" />
         <tli id="T139" time="77.11294736842105" type="appl" />
         <tli id="T140" time="77.76352631578948" type="appl" />
         <tli id="T141" time="78.4141052631579" type="appl" />
         <tli id="T142" time="79.06468421052631" type="appl" />
         <tli id="T143" time="79.71526315789474" type="appl" />
         <tli id="T144" time="80.36584210526316" type="appl" />
         <tli id="T145" time="81.01642105263159" type="appl" />
         <tli id="T146" time="81.667" type="appl" />
         <tli id="T147" time="82.31757894736842" type="appl" />
         <tli id="T148" time="82.96815789473685" type="appl" />
         <tli id="T149" time="83.61873684210526" type="appl" />
         <tli id="T150" time="84.2693157894737" type="appl" />
         <tli id="T151" time="84.91989473684211" type="appl" />
         <tli id="T152" time="85.57047368421053" type="appl" />
         <tli id="T153" time="86.22105263157894" type="appl" />
         <tli id="T154" time="86.87163157894737" type="appl" />
         <tli id="T155" time="87.52221052631579" type="appl" />
         <tli id="T156" time="88.17278947368422" type="appl" />
         <tli id="T157" time="88.82336842105263" type="appl" />
         <tli id="T158" time="89.47394736842105" type="appl" />
         <tli id="T159" time="90.12452631578948" type="appl" />
         <tli id="T160" time="90.7751052631579" type="appl" />
         <tli id="T161" time="91.42568421052633" type="appl" />
         <tli id="T162" time="92.07626315789474" type="appl" />
         <tli id="T163" time="92.72684210526316" type="appl" />
         <tli id="T164" time="93.37742105263158" type="appl" />
         <tli id="T165" time="94.028" type="appl" />
         <tli id="T166" time="95.2534" type="appl" />
         <tli id="T167" time="96.4788" type="appl" />
         <tli id="T168" time="97.7042" type="appl" />
         <tli id="T169" time="98.92960000000001" type="appl" />
         <tli id="T170" time="100.155" type="appl" />
         <tli id="T171" time="100.64475" type="appl" />
         <tli id="T172" time="101.1345" type="appl" />
         <tli id="T173" time="101.62425" type="appl" />
         <tli id="T174" time="102.114" type="appl" />
         <tli id="T175" time="102.60375" type="appl" />
         <tli id="T176" time="103.0935" type="appl" />
         <tli id="T177" time="103.58325" type="appl" />
         <tli id="T178" time="104.07300000000001" type="appl" />
         <tli id="T179" time="104.56275" type="appl" />
         <tli id="T180" time="105.0525" type="appl" />
         <tli id="T181" time="105.54225" type="appl" />
         <tli id="T182" time="106.032" type="appl" />
         <tli id="T183" time="106.52175" type="appl" />
         <tli id="T184" time="107.0115" type="appl" />
         <tli id="T185" time="107.50125" type="appl" />
         <tli id="T186" time="107.991" type="appl" />
         <tli id="T187" time="108.732" type="appl" />
         <tli id="T188" time="109.473" type="appl" />
         <tli id="T189" time="110.214" type="appl" />
         <tli id="T190" time="110.955" type="appl" />
         <tli id="T191" time="111.696" type="appl" />
         <tli id="T192" time="112.17394736842105" type="appl" />
         <tli id="T193" time="112.65189473684211" type="appl" />
         <tli id="T194" time="113.12984210526315" type="appl" />
         <tli id="T195" time="113.6077894736842" type="appl" />
         <tli id="T196" time="114.08573684210526" type="appl" />
         <tli id="T197" time="114.56368421052632" type="appl" />
         <tli id="T198" time="115.04163157894737" type="appl" />
         <tli id="T199" time="115.51957894736842" type="appl" />
         <tli id="T200" time="115.99752631578947" type="appl" />
         <tli id="T201" time="116.47547368421053" type="appl" />
         <tli id="T202" time="116.95342105263158" type="appl" />
         <tli id="T203" time="117.43136842105264" type="appl" />
         <tli id="T204" time="117.90931578947368" type="appl" />
         <tli id="T205" time="118.38726315789474" type="appl" />
         <tli id="T206" time="118.86521052631579" type="appl" />
         <tli id="T207" time="119.34315789473685" type="appl" />
         <tli id="T208" time="119.82110526315789" type="appl" />
         <tli id="T209" time="120.29905263157895" type="appl" />
         <tli id="T210" time="120.777" type="appl" />
         <tli id="T211" time="121.25061111111111" type="appl" />
         <tli id="T212" time="121.72422222222222" type="appl" />
         <tli id="T213" time="122.19783333333334" type="appl" />
         <tli id="T214" time="122.67144444444445" type="appl" />
         <tli id="T215" time="123.14505555555556" type="appl" />
         <tli id="T216" time="123.61866666666667" type="appl" />
         <tli id="T217" time="124.09227777777778" type="appl" />
         <tli id="T218" time="124.56588888888889" type="appl" />
         <tli id="T219" time="125.0395" type="appl" />
         <tli id="T220" time="125.5131111111111" type="appl" />
         <tli id="T221" time="125.98672222222221" type="appl" />
         <tli id="T222" time="126.46033333333332" type="appl" />
         <tli id="T223" time="126.93394444444444" type="appl" />
         <tli id="T224" time="127.40755555555555" type="appl" />
         <tli id="T225" time="127.88116666666666" type="appl" />
         <tli id="T226" time="128.35477777777777" type="appl" />
         <tli id="T227" time="128.8283888888889" type="appl" />
         <tli id="T228" time="129.302" type="appl" />
         <tli id="T229" time="129.856" type="appl" />
         <tli id="T230" time="130.41" type="appl" />
         <tli id="T231" time="130.964" type="appl" />
         <tli id="T232" time="131.518" type="appl" />
         <tli id="T233" time="132.072" type="appl" />
         <tli id="T234" time="132.818" type="appl" />
         <tli id="T235" time="133.564" type="appl" />
         <tli id="T236" time="134.31" type="appl" />
         <tli id="T237" time="135.04766666666666" type="appl" />
         <tli id="T238" time="135.78533333333334" type="appl" />
         <tli id="T239" time="136.523" type="appl" />
         <tli id="T240" time="137.26066666666668" type="appl" />
         <tli id="T241" time="137.99833333333333" type="appl" />
         <tli id="T242" time="138.73600000000002" type="appl" />
         <tli id="T243" time="139.47366666666667" type="appl" />
         <tli id="T244" time="140.21133333333336" type="appl" />
         <tli id="T245" time="140.949" type="appl" />
         <tli id="T246" time="141.36318181818183" type="appl" />
         <tli id="T247" time="141.77736363636365" type="appl" />
         <tli id="T248" time="142.19154545454546" type="appl" />
         <tli id="T249" time="142.60572727272728" type="appl" />
         <tli id="T250" time="143.0199090909091" type="appl" />
         <tli id="T251" time="143.4340909090909" type="appl" />
         <tli id="T252" time="143.84827272727273" type="appl" />
         <tli id="T253" time="144.26245454545455" type="appl" />
         <tli id="T254" time="144.67663636363636" type="appl" />
         <tli id="T255" time="145.09081818181818" type="appl" />
         <tli id="T256" time="145.505" type="appl" />
         <tli id="T257" time="145.81230769230768" type="appl" />
         <tli id="T258" time="146.11961538461537" type="appl" />
         <tli id="T259" time="146.42692307692306" type="appl" />
         <tli id="T260" time="146.73423076923078" type="appl" />
         <tli id="T261" time="147.04153846153847" type="appl" />
         <tli id="T262" time="147.34884615384615" type="appl" />
         <tli id="T263" time="147.65615384615384" type="appl" />
         <tli id="T264" time="147.96346153846153" type="appl" />
         <tli id="T265" time="148.27076923076922" type="appl" />
         <tli id="T266" time="148.57807692307694" type="appl" />
         <tli id="T267" time="148.88538461538462" type="appl" />
         <tli id="T268" time="149.1926923076923" type="appl" />
         <tli id="T269" time="149.5" type="appl" />
         <tli id="T270" time="149.87833333333333" type="appl" />
         <tli id="T271" time="150.25666666666666" type="appl" />
         <tli id="T272" time="150.635" type="appl" />
         <tli id="T273" time="151.01333333333332" type="appl" />
         <tli id="T274" time="151.39166666666668" type="appl" />
         <tli id="T275" time="151.77" type="appl" />
         <tli id="T276" time="152.14833333333334" type="appl" />
         <tli id="T277" time="152.52666666666667" type="appl" />
         <tli id="T278" time="152.905" type="appl" />
         <tli id="T279" time="153.28333333333333" type="appl" />
         <tli id="T280" time="153.66166666666666" type="appl" />
         <tli id="T281" time="154.04" type="appl" />
         <tli id="T282" time="154.41833333333332" type="appl" />
         <tli id="T283" time="154.79666666666668" type="appl" />
         <tli id="T284" time="155.175" type="appl" />
         <tli id="T285" time="155.55333333333334" type="appl" />
         <tli id="T286" time="155.93166666666667" type="appl" />
         <tli id="T287" time="156.31" type="appl" />
         <tli id="T288" time="156.61683333333335" type="appl" />
         <tli id="T289" time="156.92366666666666" type="appl" />
         <tli id="T290" time="157.2305" type="appl" />
         <tli id="T291" time="157.53733333333335" type="appl" />
         <tli id="T292" time="157.84416666666667" type="appl" />
         <tli id="T293" time="158.151" type="appl" />
         <tli id="T294" time="158.5668" type="appl" />
         <tli id="T295" time="158.9826" type="appl" />
         <tli id="T296" time="159.3984" type="appl" />
         <tli id="T297" time="159.8142" type="appl" />
         <tli id="T298" time="160.23" type="appl" />
         <tli id="T299" time="160.57899999999998" type="appl" />
         <tli id="T300" time="160.928" type="appl" />
         <tli id="T301" time="161.277" type="appl" />
         <tli id="T302" time="161.626" type="appl" />
         <tli id="T303" time="161.975" type="appl" />
         <tli id="T304" time="162.32399999999998" type="appl" />
         <tli id="T305" time="162.673" type="appl" />
         <tli id="T306" time="163.022" type="appl" />
         <tli id="T307" time="163.371" type="appl" />
         <tli id="T308" time="163.72" type="appl" />
         <tli id="T309" time="164.09833333333333" type="appl" />
         <tli id="T310" time="164.47666666666666" type="appl" />
         <tli id="T311" time="164.855" type="appl" />
         <tli id="T312" time="165.23333333333332" type="appl" />
         <tli id="T313" time="165.61166666666665" type="appl" />
         <tli id="T314" time="165.99" type="appl" />
         <tli id="T315" time="166.36833333333334" type="appl" />
         <tli id="T316" time="166.74666666666667" type="appl" />
         <tli id="T317" time="167.125" type="appl" />
         <tli id="T318" time="167.50333333333333" type="appl" />
         <tli id="T319" time="167.88166666666666" type="appl" />
         <tli id="T320" time="168.23" type="appl" />
         <tli id="T321" time="168.26" type="appl" />
         <tli id="T322" time="168.81166666666667" type="appl" />
         <tli id="T323" time="169.39333333333332" type="appl" />
         <tli id="T324" time="169.975" type="appl" />
         <tli id="T325" time="170.55666666666667" type="appl" />
         <tli id="T326" time="171.13833333333332" type="appl" />
         <tli id="T327" time="171.72" type="appl" />
         <tli id="T328" time="172.287" type="appl" />
         <tli id="T329" time="172.85399999999998" type="appl" />
         <tli id="T330" time="173.421" type="appl" />
         <tli id="T331" time="173.988" type="appl" />
         <tli id="T332" time="174.555" type="appl" />
         <tli id="T333" time="175.122" type="appl" />
         <tli id="T334" time="175.689" type="appl" />
         <tli id="T335" time="176.256" type="appl" />
         <tli id="T336" time="176.83725" type="appl" />
         <tli id="T337" time="177.4185" type="appl" />
         <tli id="T338" time="177.99975" type="appl" />
         <tli id="T339" time="178.58100000000002" type="appl" />
         <tli id="T340" time="179.16225" type="appl" />
         <tli id="T341" time="179.7435" type="appl" />
         <tli id="T342" time="180.32475" type="appl" />
         <tli id="T343" time="180.906" type="appl" />
         <tli id="T344" time="181.44166666666666" type="appl" />
         <tli id="T345" time="181.97733333333335" type="appl" />
         <tli id="T346" time="182.513" type="appl" />
         <tli id="T347" time="183.04866666666666" type="appl" />
         <tli id="T348" time="183.58433333333335" type="appl" />
         <tli id="T349" time="184.11" type="appl" />
         <tli id="T350" time="184.12" type="appl" />
         <tli id="T351" time="184.50557142857144" type="appl" />
         <tli id="T352" time="184.90114285714287" type="appl" />
         <tli id="T353" time="185.2967142857143" type="appl" />
         <tli id="T354" time="185.69228571428573" type="appl" />
         <tli id="T355" time="186.08785714285716" type="appl" />
         <tli id="T356" time="186.4834285714286" type="appl" />
         <tli id="T357" time="186.87900000000002" type="appl" />
         <tli id="T358" time="187.27457142857142" type="appl" />
         <tli id="T359" time="187.67014285714285" type="appl" />
         <tli id="T360" time="187.799" type="appl" />
         <tli id="T361" time="188.06571428571428" type="appl" />
         <tli id="T362" time="188.4609375" type="appl" />
         <tli id="T363" time="188.4612857142857" type="appl" />
         <tli id="T364" time="188.85685714285714" type="appl" />
         <tli id="T365" time="189.122875" type="appl" />
         <tli id="T366" time="189.25242857142857" type="appl" />
         <tli id="T367" time="189.648" type="appl" />
         <tli id="T368" time="189.78481250000002" type="appl" />
         <tli id="T369" time="190.44675" type="appl" />
         <tli id="T370" time="191.1086875" type="appl" />
         <tli id="T371" time="191.770625" type="appl" />
         <tli id="T372" time="192.4325625" type="appl" />
         <tli id="T373" time="193.09449999999998" type="appl" />
         <tli id="T374" time="193.7564375" type="appl" />
         <tli id="T375" time="194.418375" type="appl" />
         <tli id="T376" time="195.0803125" type="appl" />
         <tli id="T377" time="195.74224999999998" type="appl" />
         <tli id="T378" time="196.40418749999998" type="appl" />
         <tli id="T379" time="197.066125" type="appl" />
         <tli id="T380" time="197.7280625" type="appl" />
         <tli id="T381" time="198.39" type="appl" />
         <tli id="T382" time="198.99142857142857" type="appl" />
         <tli id="T383" time="199.59285714285713" type="appl" />
         <tli id="T384" time="200.1942857142857" type="appl" />
         <tli id="T385" time="200.79571428571427" type="appl" />
         <tli id="T386" time="201.39714285714285" type="appl" />
         <tli id="T387" time="201.9985714285714" type="appl" />
         <tli id="T388" time="202.6" type="appl" />
         <tli id="T389" time="203.20142857142858" type="appl" />
         <tli id="T390" time="203.80285714285714" type="appl" />
         <tli id="T391" time="204.40428571428572" type="appl" />
         <tli id="T392" time="205.00571428571428" type="appl" />
         <tli id="T393" time="205.60714285714286" type="appl" />
         <tli id="T394" time="206.20857142857142" type="appl" />
         <tli id="T395" time="206.81" type="appl" />
         <tli id="T396" time="207.26222222222222" type="appl" />
         <tli id="T397" time="207.71444444444444" type="appl" />
         <tli id="T398" time="208.16666666666666" type="appl" />
         <tli id="T399" time="208.61888888888888" type="appl" />
         <tli id="T400" time="209.07111111111112" type="appl" />
         <tli id="T401" time="209.52333333333334" type="appl" />
         <tli id="T402" time="209.97555555555556" type="appl" />
         <tli id="T403" time="210.42777777777778" type="appl" />
         <tli id="T404" time="210.88" type="appl" />
         <tli id="T405" time="211.25911764705882" type="appl" />
         <tli id="T406" time="211.63823529411764" type="appl" />
         <tli id="T407" time="212.01735294117645" type="appl" />
         <tli id="T408" time="212.39647058823527" type="appl" />
         <tli id="T409" time="212.77558823529412" type="appl" />
         <tli id="T410" time="213.15470588235294" type="appl" />
         <tli id="T411" time="213.53382352941176" type="appl" />
         <tli id="T412" time="213.91294117647058" type="appl" />
         <tli id="T413" time="214.2920588235294" type="appl" />
         <tli id="T414" time="214.67117647058822" type="appl" />
         <tli id="T415" time="215.05029411764704" type="appl" />
         <tli id="T416" time="215.42941176470586" type="appl" />
         <tli id="T417" time="215.8085294117647" type="appl" />
         <tli id="T418" time="216.18764705882353" type="appl" />
         <tli id="T419" time="216.56676470588235" type="appl" />
         <tli id="T420" time="216.94588235294117" type="appl" />
         <tli id="T421" time="217.325" type="appl" />
         <tli id="T422" time="218.0306923076923" type="appl" />
         <tli id="T423" time="218.7363846153846" type="appl" />
         <tli id="T424" time="219.4420769230769" type="appl" />
         <tli id="T425" time="220.14776923076923" type="appl" />
         <tli id="T426" time="220.85346153846152" type="appl" />
         <tli id="T427" time="221.55915384615383" type="appl" />
         <tli id="T428" time="222.26484615384615" type="appl" />
         <tli id="T429" time="222.97053846153847" type="appl" />
         <tli id="T430" time="223.67623076923076" type="appl" />
         <tli id="T431" time="224.38192307692307" type="appl" />
         <tli id="T432" time="225.0876153846154" type="appl" />
         <tli id="T433" time="225.79330769230768" type="appl" />
         <tli id="T434" time="226.499" type="appl" />
         <tli id="T435" time="227.11033333333333" type="appl" />
         <tli id="T436" time="227.72166666666666" type="appl" />
         <tli id="T437" time="228.333" type="appl" />
         <tli id="T438" time="228.94433333333333" type="appl" />
         <tli id="T439" time="229.55566666666667" type="appl" />
         <tli id="T440" time="230.167" type="appl" />
         <tli id="T441" time="230.77833333333334" type="appl" />
         <tli id="T442" time="231.38966666666667" type="appl" />
         <tli id="T443" time="232.001" type="appl" />
         <tli id="T444" time="232.61233333333334" type="appl" />
         <tli id="T445" time="233.22366666666667" type="appl" />
         <tli id="T446" time="233.835" type="appl" />
         <tli id="T447" time="234.4325" type="appl" />
         <tli id="T448" time="235.01" type="appl" />
         <tli id="T449" time="235.03" type="appl" />
         <tli id="T450" time="235.72" type="appl" />
         <tli id="T451" time="236.42" type="appl" />
         <tli id="T452" time="236.43" type="appl" />
         <tli id="T453" time="236.81" type="appl" />
         <tli id="T454" time="237.2" type="appl" />
         <tli id="T455" time="237.58" type="appl" />
         <tli id="T456" time="237.59" type="appl" />
         <tli id="T457" time="237.945" type="appl" />
         <tli id="T458" time="238.11" type="appl" />
         <tli id="T459" time="238.31" type="appl" />
         <tli id="T460" time="238.64244444444446" type="appl" />
         <tli id="T461" time="239.1748888888889" type="appl" />
         <tli id="T462" time="239.70733333333334" type="appl" />
         <tli id="T463" time="240.2397777777778" type="appl" />
         <tli id="T464" time="240.7722222222222" type="appl" />
         <tli id="T465" time="241.30466666666666" type="appl" />
         <tli id="T466" time="241.8371111111111" type="appl" />
         <tli id="T467" time="242.36955555555554" type="appl" />
         <tli id="T468" time="242.902" type="appl" />
         <tli id="T469" time="243.30399999999997" type="appl" />
         <tli id="T470" time="243.706" type="appl" />
         <tli id="T471" time="244.108" type="appl" />
         <tli id="T472" time="244.51" type="appl" />
         <tli id="T473" time="244.91199999999998" type="appl" />
         <tli id="T474" time="245.314" type="appl" />
         <tli id="T475" time="245.716" type="appl" />
         <tli id="T476" time="246.118" type="appl" />
         <tli id="T477" time="246.51999999999998" type="appl" />
         <tli id="T478" time="246.922" type="appl" />
         <tli id="T479" time="247.37623529411763" type="appl" />
         <tli id="T480" time="247.8304705882353" type="appl" />
         <tli id="T481" time="248.28470588235294" type="appl" />
         <tli id="T482" time="248.73894117647058" type="appl" />
         <tli id="T483" time="249.19317647058824" type="appl" />
         <tli id="T484" time="249.64741176470588" type="appl" />
         <tli id="T485" time="250.10164705882352" type="appl" />
         <tli id="T486" time="250.55588235294118" type="appl" />
         <tli id="T487" time="251.01011764705882" type="appl" />
         <tli id="T488" time="251.4643529411765" type="appl" />
         <tli id="T489" time="251.91858823529412" type="appl" />
         <tli id="T490" time="252.37282352941176" type="appl" />
         <tli id="T491" time="252.82705882352943" type="appl" />
         <tli id="T492" time="253.28129411764706" type="appl" />
         <tli id="T493" time="253.7355294117647" type="appl" />
         <tli id="T494" time="254.18976470588237" type="appl" />
         <tli id="T495" time="254.644" type="appl" />
         <tli id="T496" time="255.05731034482758" type="appl" />
         <tli id="T497" time="255.47062068965516" type="appl" />
         <tli id="T498" time="255.88393103448277" type="appl" />
         <tli id="T499" time="256.2972413793103" type="appl" />
         <tli id="T500" time="256.71055172413793" type="appl" />
         <tli id="T501" time="257.12386206896554" type="appl" />
         <tli id="T502" time="257.5371724137931" type="appl" />
         <tli id="T503" time="257.9504827586207" type="appl" />
         <tli id="T504" time="258.3637931034483" type="appl" />
         <tli id="T505" time="258.77710344827585" type="appl" />
         <tli id="T506" time="259.19041379310346" type="appl" />
         <tli id="T507" time="259.603724137931" type="appl" />
         <tli id="T508" time="260.0170344827586" type="appl" />
         <tli id="T509" time="260.4303448275862" type="appl" />
         <tli id="T510" time="260.8436551724138" type="appl" />
         <tli id="T511" time="261.2569655172414" type="appl" />
         <tli id="T512" time="261.670275862069" type="appl" />
         <tli id="T513" time="262.08358620689654" type="appl" />
         <tli id="T514" time="262.49689655172415" type="appl" />
         <tli id="T515" time="262.9102068965517" type="appl" />
         <tli id="T516" time="263.3235172413793" type="appl" />
         <tli id="T517" time="263.7368275862069" type="appl" />
         <tli id="T518" time="264.15013793103446" type="appl" />
         <tli id="T519" time="264.5634482758621" type="appl" />
         <tli id="T520" time="264.9767586206897" type="appl" />
         <tli id="T521" time="265.39006896551723" type="appl" />
         <tli id="T522" time="265.80337931034484" type="appl" />
         <tli id="T523" time="266.2166896551724" type="appl" />
         <tli id="T524" time="266.63" type="appl" />
         <tli id="T525" time="267.42125" type="appl" />
         <tli id="T526" time="268.2125" type="appl" />
         <tli id="T527" time="269.00375" type="appl" />
         <tli id="T528" time="269.795" type="appl" />
         <tli id="T529" time="270.58625" type="appl" />
         <tli id="T530" time="271.3775" type="appl" />
         <tli id="T531" time="272.16875" type="appl" />
         <tli id="T532" time="272.96000000000004" type="appl" />
         <tli id="T533" time="273.75125" type="appl" />
         <tli id="T534" time="274.5425" type="appl" />
         <tli id="T535" time="275.33375" type="appl" />
         <tli id="T536" time="276.125" type="appl" />
         <tli id="T537" time="276.91625" type="appl" />
         <tli id="T538" time="277.70750000000004" type="appl" />
         <tli id="T539" time="278.49875000000003" type="appl" />
         <tli id="T540" time="279.29" type="appl" />
         <tli id="T541" time="280.5853333333333" type="appl" />
         <tli id="T542" time="281.8806666666667" type="appl" />
         <tli id="T543" time="283.176" type="appl" />
         <tli id="T544" time="283.5708125" type="appl" />
         <tli id="T545" time="283.965625" type="appl" />
         <tli id="T546" time="284.3604375" type="appl" />
         <tli id="T547" time="284.75525" type="appl" />
         <tli id="T548" time="285.1500625" type="appl" />
         <tli id="T549" time="285.544875" type="appl" />
         <tli id="T550" time="285.9396875" type="appl" />
         <tli id="T551" time="286.3345" type="appl" />
         <tli id="T552" time="286.7293125" type="appl" />
         <tli id="T553" time="287.124125" type="appl" />
         <tli id="T554" time="287.5189375" type="appl" />
         <tli id="T555" time="287.91375" type="appl" />
         <tli id="T556" time="288.3085625" type="appl" />
         <tli id="T557" time="288.703375" type="appl" />
         <tli id="T558" time="289.0981875" type="appl" />
         <tli id="T559" time="289.493" type="appl" />
         <tli id="T560" time="290.1875" type="appl" />
         <tli id="T561" time="290.882" type="appl" />
         <tli id="T562" time="291.5765" type="appl" />
         <tli id="T563" time="292.271" type="appl" />
         <tli id="T564" time="292.9655" type="appl" />
         <tli id="T565" time="293.66" type="appl" />
         <tli id="T566" time="294.140375" type="appl" />
         <tli id="T567" time="294.62075000000004" type="appl" />
         <tli id="T568" time="295.101125" type="appl" />
         <tli id="T569" time="295.5815" type="appl" />
         <tli id="T570" time="296.06187500000004" type="appl" />
         <tli id="T571" time="296.54225" type="appl" />
         <tli id="T572" time="297.022625" type="appl" />
         <tli id="T573" time="297.50300000000004" type="appl" />
         <tli id="T574" time="297.983375" type="appl" />
         <tli id="T575" time="298.46375" type="appl" />
         <tli id="T576" time="298.944125" type="appl" />
         <tli id="T577" time="299.4245" type="appl" />
         <tli id="T578" time="299.904875" type="appl" />
         <tli id="T579" time="300.38525" type="appl" />
         <tli id="T580" time="300.865625" type="appl" />
         <tli id="T581" time="301.346" type="appl" />
         <tli id="T582" time="301.96654545454544" type="appl" />
         <tli id="T583" time="302.58709090909093" type="appl" />
         <tli id="T584" time="303.20763636363637" type="appl" />
         <tli id="T585" time="303.8281818181818" type="appl" />
         <tli id="T586" time="304.4487272727273" type="appl" />
         <tli id="T587" time="305.06927272727273" type="appl" />
         <tli id="T588" time="305.6898181818182" type="appl" />
         <tli id="T589" time="306.31036363636366" type="appl" />
         <tli id="T590" time="306.9309090909091" type="appl" />
         <tli id="T591" time="307.5514545454546" type="appl" />
         <tli id="T592" time="308.172" type="appl" />
         <tli id="T593" time="308.58633333333336" type="appl" />
         <tli id="T594" time="309.0006666666667" type="appl" />
         <tli id="T595" time="309.415" type="appl" />
         <tli id="T596" time="309.82933333333335" type="appl" />
         <tli id="T597" time="310.2436666666667" type="appl" />
         <tli id="T598" time="310.658" type="appl" />
         <tli id="T599" time="311.07233333333335" type="appl" />
         <tli id="T600" time="311.4866666666667" type="appl" />
         <tli id="T601" time="311.901" type="appl" />
         <tli id="T602" time="312.31533333333334" type="appl" />
         <tli id="T603" time="312.7296666666667" type="appl" />
         <tli id="T604" time="313.144" type="appl" />
         <tli id="T605" time="313.55833333333334" type="appl" />
         <tli id="T606" time="313.97266666666667" type="appl" />
         <tli id="T607" time="314.387" type="appl" />
         <tli id="T608" time="314.80133333333333" type="appl" />
         <tli id="T609" time="315.21566666666666" type="appl" />
         <tli id="T610" time="315.63" type="appl" />
         <tli id="T611" time="316.44" type="appl" />
         <tli id="T612" time="317.25" type="appl" />
         <tli id="T613" time="318.06" type="appl" />
         <tli id="T614" time="318.5068823529412" type="appl" />
         <tli id="T615" time="318.95376470588235" type="appl" />
         <tli id="T616" time="319.40064705882355" type="appl" />
         <tli id="T617" time="319.8475294117647" type="appl" />
         <tli id="T618" time="320.2944117647059" type="appl" />
         <tli id="T619" time="320.74129411764704" type="appl" />
         <tli id="T620" time="321.18817647058825" type="appl" />
         <tli id="T621" time="321.6350588235294" type="appl" />
         <tli id="T622" time="322.0819411764706" type="appl" />
         <tli id="T623" time="322.52882352941174" type="appl" />
         <tli id="T624" time="322.97570588235294" type="appl" />
         <tli id="T625" time="323.4225882352941" type="appl" />
         <tli id="T626" time="323.8694705882353" type="appl" />
         <tli id="T627" time="324.31635294117643" type="appl" />
         <tli id="T628" time="324.76323529411764" type="appl" />
         <tli id="T629" time="325.2101176470588" type="appl" />
         <tli id="T630" time="325.657" type="appl" />
         <tli id="T631" time="325.75" type="appl" />
         <tli id="T632" time="326.2335" type="appl" />
         <tli id="T633" time="326.435" type="appl" />
         <tli id="T634" time="326.81" type="appl" />
         <tli id="T635" time="327.12" type="appl" />
         <tli id="T636" time="327.805" type="appl" />
         <tli id="T637" time="328.49" type="appl" />
         <tli id="T638" time="328.52" type="appl" />
         <tli id="T639" time="329.0642105263158" type="appl" />
         <tli id="T640" time="329.60842105263157" type="appl" />
         <tli id="T641" time="330.15263157894736" type="appl" />
         <tli id="T642" time="330.69684210526316" type="appl" />
         <tli id="T643" time="331.24105263157895" type="appl" />
         <tli id="T644" time="331.78526315789475" type="appl" />
         <tli id="T645" time="332.32947368421054" type="appl" />
         <tli id="T646" time="332.87368421052633" type="appl" />
         <tli id="T647" time="333.41789473684213" type="appl" />
         <tli id="T648" time="333.96210526315787" type="appl" />
         <tli id="T649" time="334.50631578947366" type="appl" />
         <tli id="T650" time="335.05052631578945" type="appl" />
         <tli id="T651" time="335.59473684210525" type="appl" />
         <tli id="T652" time="336.13894736842104" type="appl" />
         <tli id="T653" time="336.68315789473684" type="appl" />
         <tli id="T654" time="337.22736842105263" type="appl" />
         <tli id="T655" time="337.7715789473684" type="appl" />
         <tli id="T656" time="338.3157894736842" type="appl" />
         <tli id="T657" time="338.83" type="appl" />
         <tli id="T658" time="338.86" type="appl" />
         <tli id="T659" time="339.214" type="appl" />
         <tli id="T660" time="339.598" type="appl" />
         <tli id="T661" time="339.98199999999997" type="appl" />
         <tli id="T662" time="340.366" type="appl" />
         <tli id="T663" time="340.75" type="appl" />
         <tli id="T664" time="341.35875" type="appl" />
         <tli id="T665" time="341.9675" type="appl" />
         <tli id="T666" time="342.57625" type="appl" />
         <tli id="T667" time="343.185" type="appl" />
         <tli id="T668" time="343.79375" type="appl" />
         <tli id="T669" time="344.40250000000003" type="appl" />
         <tli id="T670" time="345.01125" type="appl" />
         <tli id="T671" time="345.62" type="appl" />
         <tli id="T672" time="345.99966666666666" type="appl" />
         <tli id="T673" time="346.37933333333336" type="appl" />
         <tli id="T674" time="346.759" type="appl" />
         <tli id="T675" time="347.13866666666667" type="appl" />
         <tli id="T676" time="347.5183333333333" type="appl" />
         <tli id="T677" time="347.898" type="appl" />
         <tli id="T678" time="348.2776666666667" type="appl" />
         <tli id="T679" time="348.6573333333333" type="appl" />
         <tli id="T680" time="349.03700000000003" type="appl" />
         <tli id="T681" time="349.4166666666667" type="appl" />
         <tli id="T682" time="349.79633333333334" type="appl" />
         <tli id="T683" time="350.176" type="appl" />
         <tli id="T684" time="350.5556666666667" type="appl" />
         <tli id="T685" time="350.93533333333335" type="appl" />
         <tli id="T686" time="351.315" type="appl" />
         <tli id="T687" time="351.69466666666665" type="appl" />
         <tli id="T688" time="352.07433333333336" type="appl" />
         <tli id="T689" time="352.454" type="appl" />
         <tli id="T690" time="353.0352380952381" type="appl" />
         <tli id="T691" time="353.6164761904762" type="appl" />
         <tli id="T692" time="354.1977142857143" type="appl" />
         <tli id="T693" time="354.7789523809524" type="appl" />
         <tli id="T694" time="355.3601904761905" type="appl" />
         <tli id="T695" time="355.94142857142856" type="appl" />
         <tli id="T696" time="356.5226666666667" type="appl" />
         <tli id="T697" time="357.1039047619048" type="appl" />
         <tli id="T698" time="357.68514285714286" type="appl" />
         <tli id="T699" time="358.266380952381" type="appl" />
         <tli id="T700" time="358.84761904761905" type="appl" />
         <tli id="T701" time="359.42885714285717" type="appl" />
         <tli id="T702" time="360.01009523809523" type="appl" />
         <tli id="T703" time="360.59133333333335" type="appl" />
         <tli id="T704" time="361.1725714285715" type="appl" />
         <tli id="T705" time="361.75380952380954" type="appl" />
         <tli id="T706" time="362.33504761904766" type="appl" />
         <tli id="T707" time="362.9162857142857" type="appl" />
         <tli id="T708" time="363.49752380952384" type="appl" />
         <tli id="T709" time="364.0787619047619" type="appl" />
         <tli id="T710" time="364.66" type="appl" />
         <tli id="T711" time="365.34285714285716" type="appl" />
         <tli id="T712" time="366.0257142857143" type="appl" />
         <tli id="T713" time="366.7085714285715" type="appl" />
         <tli id="T714" time="367.3914285714286" type="appl" />
         <tli id="T715" time="368.07428571428574" type="appl" />
         <tli id="T716" time="368.75714285714287" type="appl" />
         <tli id="T717" time="369.44000000000005" type="appl" />
         <tli id="T718" time="370.1228571428572" type="appl" />
         <tli id="T719" time="370.8057142857143" type="appl" />
         <tli id="T720" time="371.48857142857145" type="appl" />
         <tli id="T721" time="372.1714285714286" type="appl" />
         <tli id="T722" time="372.85428571428577" type="appl" />
         <tli id="T723" time="373.5371428571429" type="appl" />
         <tli id="T724" time="374.21" type="appl" />
         <tli id="T725" time="374.22" type="appl" />
         <tli id="T726" time="374.64573684210524" type="appl" />
         <tli id="T727" time="375.0814736842105" type="appl" />
         <tli id="T728" time="375.51721052631575" type="appl" />
         <tli id="T729" time="375.952947368421" type="appl" />
         <tli id="T730" time="376.3886842105263" type="appl" />
         <tli id="T731" time="376.8244210526316" type="appl" />
         <tli id="T732" time="377.26015789473684" type="appl" />
         <tli id="T733" time="377.6958947368421" type="appl" />
         <tli id="T734" time="378.13163157894735" type="appl" />
         <tli id="T735" time="378.5673684210526" type="appl" />
         <tli id="T736" time="379.00310526315786" type="appl" />
         <tli id="T737" time="379.4388421052631" type="appl" />
         <tli id="T738" time="379.8745789473684" type="appl" />
         <tli id="T739" time="380.31031578947363" type="appl" />
         <tli id="T740" time="380.74605263157895" type="appl" />
         <tli id="T741" time="381.1817894736842" type="appl" />
         <tli id="T742" time="381.61752631578946" type="appl" />
         <tli id="T743" time="382.0532631578947" type="appl" />
         <tli id="T744" time="382.489" type="appl" />
         <tli id="T745" time="383.1572" type="appl" />
         <tli id="T746" time="383.8254" type="appl" />
         <tli id="T747" time="384.49359999999996" type="appl" />
         <tli id="T748" time="385.16179999999997" type="appl" />
         <tli id="T749" time="385.83" type="appl" />
         <tli id="T750" time="386.25557142857144" type="appl" />
         <tli id="T751" time="386.68114285714285" type="appl" />
         <tli id="T752" time="387.10671428571425" type="appl" />
         <tli id="T753" time="387.5322857142857" type="appl" />
         <tli id="T754" time="387.95785714285716" type="appl" />
         <tli id="T755" time="388.38342857142857" type="appl" />
         <tli id="T756" time="388.80899999999997" type="appl" />
         <tli id="T757" time="389.2345714285714" type="appl" />
         <tli id="T758" time="389.6601428571429" type="appl" />
         <tli id="T759" time="390.0857142857143" type="appl" />
         <tli id="T760" time="390.5112857142857" type="appl" />
         <tli id="T761" time="390.93685714285715" type="appl" />
         <tli id="T762" time="391.3624285714286" type="appl" />
         <tli id="T763" time="391.788" type="appl" />
         <tli id="T764" time="393.77" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KuNS"
                      id="tx-KuNS"
                      speaker="KuNS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KuNS">
            <ts e="T7" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Tɨ͡aga</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">hɨldʼaːččɨgɨn</ts>
                  <nts id="Seg_10" n="HIAT:ip">,</nts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">tɨ͡aga</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">bara</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tüheːččigin</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">kergennerger</ts>
                  <nts id="Seg_23" n="HIAT:ip">?</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T84" id="Seg_25" n="sc" s="T65">
               <ts e="T79" id="Seg_27" n="HIAT:u" s="T65">
                  <nts id="Seg_28" n="HIAT:ip">–</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_31" n="HIAT:w" s="T65">Tɨ͡ataːgɨlar</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_34" n="HIAT:w" s="T66">taːk</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_37" n="HIAT:w" s="T67">da</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_40" n="HIAT:w" s="T68">hanaːlarɨn</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_43" n="HIAT:w" s="T69">istegin</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_47" n="HIAT:w" s="T70">e</ts>
                  <nts id="Seg_48" n="HIAT:ip">,</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_51" n="HIAT:w" s="T71">oloktoro</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_54" n="HIAT:w" s="T72">kɨtaːnagɨn</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_57" n="HIAT:w" s="T73">körögün</ts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_61" n="HIAT:w" s="T74">tak</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_64" n="HIAT:w" s="T75">ta</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_68" n="HIAT:w" s="T76">hanargɨːllarɨn</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_71" n="HIAT:w" s="T77">istegin</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_74" n="HIAT:w" s="T78">de</ts>
                  <nts id="Seg_75" n="HIAT:ip">?</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_78" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_80" n="HIAT:w" s="T79">Ologu</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_83" n="HIAT:w" s="T80">üčügejdik</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_86" n="HIAT:w" s="T81">kajdak</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_89" n="HIAT:w" s="T82">oŋoru͡okka</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_92" n="HIAT:w" s="T83">di͡en</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T122" id="Seg_95" n="sc" s="T118">
               <ts e="T122" id="Seg_97" n="HIAT:u" s="T118">
                  <nts id="Seg_98" n="HIAT:ip">–</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_101" n="HIAT:w" s="T118">Olus</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_104" n="HIAT:w" s="T120">hanaːrgaːbattar</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_107" n="HIAT:w" s="T121">da</ts>
                  <nts id="Seg_108" n="HIAT:ip">?</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T335" id="Seg_110" n="sc" s="T320">
               <ts e="T327" id="Seg_112" n="HIAT:u" s="T320">
                  <nts id="Seg_113" n="HIAT:ip">–</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_116" n="HIAT:w" s="T320">Tu͡ok</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_119" n="HIAT:w" s="T322">kömölöhör</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_122" n="HIAT:w" s="T323">kördügüj</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_126" n="HIAT:w" s="T324">diːn</ts>
                  <nts id="Seg_127" n="HIAT:ip">,</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_130" n="HIAT:w" s="T325">olok</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_133" n="HIAT:w" s="T326">tuksarɨgar</ts>
                  <nts id="Seg_134" n="HIAT:ip">?</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_137" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_139" n="HIAT:w" s="T327">Tu͡ok</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_142" n="HIAT:w" s="T328">kömölöhör</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_145" n="HIAT:w" s="T329">kördügüj</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_147" n="HIAT:ip">–</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_150" n="HIAT:w" s="T330">kihiler</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_153" n="HIAT:w" s="T331">bejelere</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_156" n="HIAT:w" s="T332">duː</ts>
                  <nts id="Seg_157" n="HIAT:ip">,</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_160" n="HIAT:w" s="T333">tojonnor</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_163" n="HIAT:w" s="T334">du</ts>
                  <nts id="Seg_164" n="HIAT:ip">?</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T367" id="Seg_166" n="sc" s="T349">
               <ts e="T367" id="Seg_168" n="HIAT:u" s="T349">
                  <nts id="Seg_169" n="HIAT:ip">–</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_172" n="HIAT:w" s="T349">Dʼe</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_176" n="HIAT:w" s="T351">kihiŋ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_179" n="HIAT:w" s="T352">barɨta</ts>
                  <nts id="Seg_180" n="HIAT:ip">,</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_183" n="HIAT:w" s="T353">eː</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_187" n="HIAT:w" s="T354">hin</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_190" n="HIAT:w" s="T355">da</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_193" n="HIAT:w" s="T356">bejetin</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_196" n="HIAT:w" s="T357">hanaːtɨn</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_199" n="HIAT:w" s="T358">hin</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_202" n="HIAT:w" s="T359">biːr</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_205" n="HIAT:w" s="T361">haŋarɨ͡an</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_208" n="HIAT:w" s="T363">naːda</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_210" n="HIAT:ip">–</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_213" n="HIAT:w" s="T364">tu͡ok</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_216" n="HIAT:w" s="T366">tuksarɨn</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T452" id="Seg_219" n="sc" s="T448">
               <ts e="T452" id="Seg_221" n="HIAT:u" s="T448">
                  <nts id="Seg_222" n="HIAT:ip">–</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_225" n="HIAT:w" s="T448">Atɨn</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_228" n="HIAT:w" s="T450">ologu</ts>
                  <nts id="Seg_229" n="HIAT:ip">…</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T459" id="Seg_231" n="sc" s="T455">
               <ts e="T459" id="Seg_233" n="HIAT:u" s="T455">
                  <nts id="Seg_234" n="HIAT:ip">–</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_237" n="HIAT:w" s="T455">Batallar</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_240" n="HIAT:w" s="T457">du</ts>
                  <nts id="Seg_241" n="HIAT:ip">?</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T634" id="Seg_243" n="sc" s="T630">
               <ts e="T634" id="Seg_245" n="HIAT:u" s="T630">
                  <nts id="Seg_246" n="HIAT:ip">–</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_249" n="HIAT:w" s="T630">Hatanɨ͡aktara</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_252" n="HIAT:w" s="T632">hu͡oga</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T658" id="Seg_255" n="sc" s="T638">
               <ts e="T658" id="Seg_257" n="HIAT:u" s="T638">
                  <nts id="Seg_258" n="HIAT:ip">–</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_261" n="HIAT:w" s="T638">Dʼe</ts>
                  <nts id="Seg_262" n="HIAT:ip">,</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_265" n="HIAT:w" s="T639">tojonnor</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_268" n="HIAT:w" s="T640">taːk</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_271" n="HIAT:w" s="T641">daːganɨ</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_274" n="HIAT:w" s="T642">dumajdaːnnar</ts>
                  <nts id="Seg_275" n="HIAT:ip">,</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_278" n="HIAT:w" s="T643">diːbin</ts>
                  <nts id="Seg_279" n="HIAT:ip">,</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_282" n="HIAT:w" s="T644">inni</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_285" n="HIAT:w" s="T645">di͡ek</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_288" n="HIAT:w" s="T646">kajdak</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_290" n="HIAT:ip">(</nts>
                  <ts e="T648" id="Seg_292" n="HIAT:w" s="T647">olok-</ts>
                  <nts id="Seg_293" n="HIAT:ip">)</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_296" n="HIAT:w" s="T648">ologu</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_299" n="HIAT:w" s="T649">tuksarɨ͡akka</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_302" n="HIAT:w" s="T650">di͡en</ts>
                  <nts id="Seg_303" n="HIAT:ip">,</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_306" n="HIAT:w" s="T651">iti</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_309" n="HIAT:w" s="T652">tuhunan</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_312" n="HIAT:w" s="T653">kepsetiː</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_315" n="HIAT:w" s="T654">ulakan</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_318" n="HIAT:w" s="T655">bu͡olu͡o</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_321" n="HIAT:w" s="T656">bu͡o</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T763" id="Seg_324" n="sc" s="T724">
               <ts e="T744" id="Seg_326" n="HIAT:u" s="T724">
                  <nts id="Seg_327" n="HIAT:ip">–</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_330" n="HIAT:w" s="T724">Inni</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_333" n="HIAT:w" s="T726">di͡ek</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_336" n="HIAT:w" s="T727">deŋŋe</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_339" n="HIAT:w" s="T728">kelen</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_342" n="HIAT:w" s="T729">istekkinen</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_344" n="HIAT:ip">(</nts>
                  <ts e="T731" id="Seg_346" n="HIAT:w" s="T730">manna</ts>
                  <nts id="Seg_347" n="HIAT:ip">)</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_350" n="HIAT:w" s="T731">Nosku͡ottan</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_353" n="HIAT:w" s="T732">manna</ts>
                  <nts id="Seg_354" n="HIAT:ip">,</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_357" n="HIAT:w" s="T733">Dudʼinkaga</ts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_361" n="HIAT:w" s="T734">radʼia</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_364" n="HIAT:w" s="T735">kelen</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_367" n="HIAT:w" s="T736">ihi͡eŋ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_370" n="HIAT:w" s="T737">diː</ts>
                  <nts id="Seg_371" n="HIAT:ip">,</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_374" n="HIAT:w" s="T738">kiːri͡eŋ</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_377" n="HIAT:w" s="T739">dʼiː</ts>
                  <nts id="Seg_378" n="HIAT:ip">,</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_381" n="HIAT:w" s="T740">enigin</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_384" n="HIAT:w" s="T741">manna</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_387" n="HIAT:w" s="T742">kihiler</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_390" n="HIAT:w" s="T743">bileller</ts>
                  <nts id="Seg_391" n="HIAT:ip">.</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T749" id="Seg_394" n="HIAT:u" s="T744">
                  <ts e="T745" id="Seg_396" n="HIAT:w" s="T744">Üleliːr</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_399" n="HIAT:w" s="T745">da</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_402" n="HIAT:w" s="T746">kihiler</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_405" n="HIAT:w" s="T747">ɨrɨ͡alargɨn</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_408" n="HIAT:w" s="T748">isteːččiler</ts>
                  <nts id="Seg_409" n="HIAT:ip">.</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T763" id="Seg_412" n="HIAT:u" s="T749">
                  <ts e="T750" id="Seg_414" n="HIAT:w" s="T749">Tak</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_417" n="HIAT:w" s="T750">što</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_420" n="HIAT:w" s="T751">bert</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_423" n="HIAT:w" s="T752">eteŋŋe</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_426" n="HIAT:w" s="T753">bagajdɨk</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_429" n="HIAT:w" s="T754">olor</ts>
                  <nts id="Seg_430" n="HIAT:ip">,</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_433" n="HIAT:w" s="T755">diːbin</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_437" n="HIAT:w" s="T756">bert</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_440" n="HIAT:w" s="T757">eteŋŋe</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_443" n="HIAT:w" s="T758">bagaj</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_446" n="HIAT:w" s="T759">oloktoːk</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_449" n="HIAT:w" s="T760">bu͡ol</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_452" n="HIAT:w" s="T761">inni</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_455" n="HIAT:w" s="T762">di͡ek</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KuNS">
            <ts e="T7" id="Seg_458" n="sc" s="T1">
               <ts e="T2" id="Seg_460" n="e" s="T1">– Tɨ͡aga </ts>
               <ts e="T3" id="Seg_462" n="e" s="T2">hɨldʼaːččɨgɨn, </ts>
               <ts e="T4" id="Seg_464" n="e" s="T3">tɨ͡aga </ts>
               <ts e="T5" id="Seg_466" n="e" s="T4">bara </ts>
               <ts e="T6" id="Seg_468" n="e" s="T5">tüheːččigin </ts>
               <ts e="T7" id="Seg_470" n="e" s="T6">kergennerger? </ts>
            </ts>
            <ts e="T84" id="Seg_471" n="sc" s="T65">
               <ts e="T66" id="Seg_473" n="e" s="T65">– Tɨ͡ataːgɨlar </ts>
               <ts e="T67" id="Seg_475" n="e" s="T66">taːk </ts>
               <ts e="T68" id="Seg_477" n="e" s="T67">da </ts>
               <ts e="T69" id="Seg_479" n="e" s="T68">hanaːlarɨn </ts>
               <ts e="T70" id="Seg_481" n="e" s="T69">istegin, </ts>
               <ts e="T71" id="Seg_483" n="e" s="T70">e, </ts>
               <ts e="T72" id="Seg_485" n="e" s="T71">oloktoro </ts>
               <ts e="T73" id="Seg_487" n="e" s="T72">kɨtaːnagɨn </ts>
               <ts e="T74" id="Seg_489" n="e" s="T73">körögün, </ts>
               <ts e="T75" id="Seg_491" n="e" s="T74">tak </ts>
               <ts e="T76" id="Seg_493" n="e" s="T75">ta, </ts>
               <ts e="T77" id="Seg_495" n="e" s="T76">hanargɨːllarɨn </ts>
               <ts e="T78" id="Seg_497" n="e" s="T77">istegin </ts>
               <ts e="T79" id="Seg_499" n="e" s="T78">de? </ts>
               <ts e="T80" id="Seg_501" n="e" s="T79">Ologu </ts>
               <ts e="T81" id="Seg_503" n="e" s="T80">üčügejdik </ts>
               <ts e="T82" id="Seg_505" n="e" s="T81">kajdak </ts>
               <ts e="T83" id="Seg_507" n="e" s="T82">oŋoru͡okka </ts>
               <ts e="T84" id="Seg_509" n="e" s="T83">di͡en. </ts>
            </ts>
            <ts e="T122" id="Seg_510" n="sc" s="T118">
               <ts e="T120" id="Seg_512" n="e" s="T118">– Olus </ts>
               <ts e="T121" id="Seg_514" n="e" s="T120">hanaːrgaːbattar </ts>
               <ts e="T122" id="Seg_516" n="e" s="T121">da? </ts>
            </ts>
            <ts e="T335" id="Seg_517" n="sc" s="T320">
               <ts e="T322" id="Seg_519" n="e" s="T320">– Tu͡ok </ts>
               <ts e="T323" id="Seg_521" n="e" s="T322">kömölöhör </ts>
               <ts e="T324" id="Seg_523" n="e" s="T323">kördügüj, </ts>
               <ts e="T325" id="Seg_525" n="e" s="T324">diːn, </ts>
               <ts e="T326" id="Seg_527" n="e" s="T325">olok </ts>
               <ts e="T327" id="Seg_529" n="e" s="T326">tuksarɨgar? </ts>
               <ts e="T328" id="Seg_531" n="e" s="T327">Tu͡ok </ts>
               <ts e="T329" id="Seg_533" n="e" s="T328">kömölöhör </ts>
               <ts e="T330" id="Seg_535" n="e" s="T329">kördügüj – </ts>
               <ts e="T331" id="Seg_537" n="e" s="T330">kihiler </ts>
               <ts e="T332" id="Seg_539" n="e" s="T331">bejelere </ts>
               <ts e="T333" id="Seg_541" n="e" s="T332">duː, </ts>
               <ts e="T334" id="Seg_543" n="e" s="T333">tojonnor </ts>
               <ts e="T335" id="Seg_545" n="e" s="T334">du? </ts>
            </ts>
            <ts e="T367" id="Seg_546" n="sc" s="T349">
               <ts e="T351" id="Seg_548" n="e" s="T349">– Dʼe, </ts>
               <ts e="T352" id="Seg_550" n="e" s="T351">kihiŋ </ts>
               <ts e="T353" id="Seg_552" n="e" s="T352">barɨta, </ts>
               <ts e="T354" id="Seg_554" n="e" s="T353">eː, </ts>
               <ts e="T355" id="Seg_556" n="e" s="T354">hin </ts>
               <ts e="T356" id="Seg_558" n="e" s="T355">da </ts>
               <ts e="T357" id="Seg_560" n="e" s="T356">bejetin </ts>
               <ts e="T358" id="Seg_562" n="e" s="T357">hanaːtɨn </ts>
               <ts e="T359" id="Seg_564" n="e" s="T358">hin </ts>
               <ts e="T361" id="Seg_566" n="e" s="T359">biːr </ts>
               <ts e="T363" id="Seg_568" n="e" s="T361">haŋarɨ͡an </ts>
               <ts e="T364" id="Seg_570" n="e" s="T363">naːda – </ts>
               <ts e="T366" id="Seg_572" n="e" s="T364">tu͡ok </ts>
               <ts e="T367" id="Seg_574" n="e" s="T366">tuksarɨn. </ts>
            </ts>
            <ts e="T452" id="Seg_575" n="sc" s="T448">
               <ts e="T450" id="Seg_577" n="e" s="T448">– Atɨn </ts>
               <ts e="T452" id="Seg_579" n="e" s="T450">ologu… </ts>
            </ts>
            <ts e="T459" id="Seg_580" n="sc" s="T455">
               <ts e="T457" id="Seg_582" n="e" s="T455">– Batallar </ts>
               <ts e="T459" id="Seg_584" n="e" s="T457">du? </ts>
            </ts>
            <ts e="T634" id="Seg_585" n="sc" s="T630">
               <ts e="T632" id="Seg_587" n="e" s="T630">– Hatanɨ͡aktara </ts>
               <ts e="T634" id="Seg_589" n="e" s="T632">hu͡oga. </ts>
            </ts>
            <ts e="T658" id="Seg_590" n="sc" s="T638">
               <ts e="T639" id="Seg_592" n="e" s="T638">– Dʼe, </ts>
               <ts e="T640" id="Seg_594" n="e" s="T639">tojonnor </ts>
               <ts e="T641" id="Seg_596" n="e" s="T640">taːk </ts>
               <ts e="T642" id="Seg_598" n="e" s="T641">daːganɨ </ts>
               <ts e="T643" id="Seg_600" n="e" s="T642">dumajdaːnnar, </ts>
               <ts e="T644" id="Seg_602" n="e" s="T643">diːbin, </ts>
               <ts e="T645" id="Seg_604" n="e" s="T644">inni </ts>
               <ts e="T646" id="Seg_606" n="e" s="T645">di͡ek </ts>
               <ts e="T647" id="Seg_608" n="e" s="T646">kajdak </ts>
               <ts e="T648" id="Seg_610" n="e" s="T647">(olok-) </ts>
               <ts e="T649" id="Seg_612" n="e" s="T648">ologu </ts>
               <ts e="T650" id="Seg_614" n="e" s="T649">tuksarɨ͡akka </ts>
               <ts e="T651" id="Seg_616" n="e" s="T650">di͡en, </ts>
               <ts e="T652" id="Seg_618" n="e" s="T651">iti </ts>
               <ts e="T653" id="Seg_620" n="e" s="T652">tuhunan </ts>
               <ts e="T654" id="Seg_622" n="e" s="T653">kepsetiː </ts>
               <ts e="T655" id="Seg_624" n="e" s="T654">ulakan </ts>
               <ts e="T656" id="Seg_626" n="e" s="T655">bu͡olu͡o </ts>
               <ts e="T658" id="Seg_628" n="e" s="T656">bu͡o. </ts>
            </ts>
            <ts e="T763" id="Seg_629" n="sc" s="T724">
               <ts e="T726" id="Seg_631" n="e" s="T724">– Inni </ts>
               <ts e="T727" id="Seg_633" n="e" s="T726">di͡ek </ts>
               <ts e="T728" id="Seg_635" n="e" s="T727">deŋŋe </ts>
               <ts e="T729" id="Seg_637" n="e" s="T728">kelen </ts>
               <ts e="T730" id="Seg_639" n="e" s="T729">istekkinen </ts>
               <ts e="T731" id="Seg_641" n="e" s="T730">(manna) </ts>
               <ts e="T732" id="Seg_643" n="e" s="T731">Nosku͡ottan </ts>
               <ts e="T733" id="Seg_645" n="e" s="T732">manna, </ts>
               <ts e="T734" id="Seg_647" n="e" s="T733">Dudʼinkaga, </ts>
               <ts e="T735" id="Seg_649" n="e" s="T734">radʼia </ts>
               <ts e="T736" id="Seg_651" n="e" s="T735">kelen </ts>
               <ts e="T737" id="Seg_653" n="e" s="T736">ihi͡eŋ </ts>
               <ts e="T738" id="Seg_655" n="e" s="T737">diː, </ts>
               <ts e="T739" id="Seg_657" n="e" s="T738">kiːri͡eŋ </ts>
               <ts e="T740" id="Seg_659" n="e" s="T739">dʼiː, </ts>
               <ts e="T741" id="Seg_661" n="e" s="T740">enigin </ts>
               <ts e="T742" id="Seg_663" n="e" s="T741">manna </ts>
               <ts e="T743" id="Seg_665" n="e" s="T742">kihiler </ts>
               <ts e="T744" id="Seg_667" n="e" s="T743">bileller. </ts>
               <ts e="T745" id="Seg_669" n="e" s="T744">Üleliːr </ts>
               <ts e="T746" id="Seg_671" n="e" s="T745">da </ts>
               <ts e="T747" id="Seg_673" n="e" s="T746">kihiler </ts>
               <ts e="T748" id="Seg_675" n="e" s="T747">ɨrɨ͡alargɨn </ts>
               <ts e="T749" id="Seg_677" n="e" s="T748">isteːččiler. </ts>
               <ts e="T750" id="Seg_679" n="e" s="T749">Tak </ts>
               <ts e="T751" id="Seg_681" n="e" s="T750">što </ts>
               <ts e="T752" id="Seg_683" n="e" s="T751">bert </ts>
               <ts e="T753" id="Seg_685" n="e" s="T752">eteŋŋe </ts>
               <ts e="T754" id="Seg_687" n="e" s="T753">bagajdɨk </ts>
               <ts e="T755" id="Seg_689" n="e" s="T754">olor, </ts>
               <ts e="T756" id="Seg_691" n="e" s="T755">diːbin, </ts>
               <ts e="T757" id="Seg_693" n="e" s="T756">bert </ts>
               <ts e="T758" id="Seg_695" n="e" s="T757">eteŋŋe </ts>
               <ts e="T759" id="Seg_697" n="e" s="T758">bagaj </ts>
               <ts e="T760" id="Seg_699" n="e" s="T759">oloktoːk </ts>
               <ts e="T761" id="Seg_701" n="e" s="T760">bu͡ol </ts>
               <ts e="T762" id="Seg_703" n="e" s="T761">inni </ts>
               <ts e="T763" id="Seg_705" n="e" s="T762">di͡ek. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KuNS">
            <ta e="T7" id="Seg_706" s="T1">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.001 (001.001)</ta>
            <ta e="T79" id="Seg_707" s="T65">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.002 (001.009)</ta>
            <ta e="T84" id="Seg_708" s="T79">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.003 (001.010)</ta>
            <ta e="T122" id="Seg_709" s="T118">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.004 (001.015)</ta>
            <ta e="T327" id="Seg_710" s="T320">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.005 (001.033)</ta>
            <ta e="T335" id="Seg_711" s="T327">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.006 (001.034)</ta>
            <ta e="T367" id="Seg_712" s="T349">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.007 (001.037)</ta>
            <ta e="T452" id="Seg_713" s="T448">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.008 (001.045)</ta>
            <ta e="T459" id="Seg_714" s="T455">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.009 (001.047)</ta>
            <ta e="T634" id="Seg_715" s="T630">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.010 (001.061)</ta>
            <ta e="T658" id="Seg_716" s="T638">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.011 (001.063)</ta>
            <ta e="T744" id="Seg_717" s="T724">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.012 (001.069)</ta>
            <ta e="T749" id="Seg_718" s="T744">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.013 (001.070)</ta>
            <ta e="T763" id="Seg_719" s="T749">UkOA_KuNS_20XX_LifeNovorybnoe_conv.KuNS.014 (001.071)</ta>
         </annotation>
         <annotation name="st" tierref="st-KuNS">
            <ta e="T7" id="Seg_720" s="T1">–Тыага һылдьааччыгын, тыага бара түһээччигин кэргэннэргэр? </ta>
            <ta e="T79" id="Seg_721" s="T65">Тыатаагылар таак да һанааларын истэгин, олокторо кытаанагын көрөгүн, һанаргыларын истэгин?</ta>
            <ta e="T84" id="Seg_722" s="T79">Ологу үчүгэйдик кайдак оӈоруока диэн. </ta>
            <ta e="T122" id="Seg_723" s="T118">– Олус һанааргаабаттар да? </ta>
            <ta e="T327" id="Seg_724" s="T320">– Туок көмөлөһөр көрдүгүй, ди (диигин), оллок туксарыгар?</ta>
            <ta e="T335" id="Seg_725" s="T327">Туок көмөлөһөр көрдүгүй –киһилэр бэйлэрэ дуу, тойоннор ду?</ta>
            <ta e="T367" id="Seg_726" s="T349">– Дьэ, киһиӈ барыта, ээ, һин да бэйтин һанаатын һин биир һаӈарыан наада – туок туксарын. </ta>
            <ta e="T452" id="Seg_727" s="T448">–Атын ологу.. </ta>
            <ta e="T459" id="Seg_728" s="T455">– Баталлар да ?</ta>
            <ta e="T634" id="Seg_729" s="T630">– Һатаныактара һуога. </ta>
            <ta e="T658" id="Seg_730" s="T638">– Тойоннор таак дааганы думайдаанылар, диибит, инни диэк кайдак олок.. ологу туксарыакка диэн, ити туһунан.. кэпсэтии улакан буолуо буо. </ta>
            <ta e="T744" id="Seg_731" s="T724">– Инни диэк дэӈӈэ кэлэн истэккинэн (манна) Носкуоттан манна, Дудинкага, радио кэлэн иһиэӈ дии, киириэӈ дьии, энигин манна киһилэр билэллэр. </ta>
            <ta e="T749" id="Seg_732" s="T744">Үлэлиир да киһилэр ырыаларгын истээччилэр. </ta>
            <ta e="T763" id="Seg_733" s="T749">Так что бэрт этэӈӈэ багайдык олор, дибин, бэрт этэӈӈэ багай олоктоок буол инни диэк.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KuNS">
            <ta e="T7" id="Seg_734" s="T1">– Tɨ͡aga hɨldʼaːččɨgɨn, tɨ͡aga bara tüheːččigin kergennerger? </ta>
            <ta e="T79" id="Seg_735" s="T65">– Tɨ͡ataːgɨlar taːk da hanaːlarɨn istegin, e, oloktoro kɨtaːnagɨn körögün, tak ta, hanargɨːllarɨn istegin de? </ta>
            <ta e="T84" id="Seg_736" s="T79">Ologu üčügejdik kajdak oŋoru͡okka di͡en. </ta>
            <ta e="T122" id="Seg_737" s="T118">– Olus hanaːrgaːbattar da? </ta>
            <ta e="T327" id="Seg_738" s="T320">– Tu͡ok kömölöhör kördügüj, diːn, olok tuksarɨgar? </ta>
            <ta e="T335" id="Seg_739" s="T327">Tu͡ok kömölöhör kördügüj – kihiler bejelere duː, tojonnor du? </ta>
            <ta e="T367" id="Seg_740" s="T349">– Dʼe, kihiŋ barɨta, eː, hin da bejetin hanaːtɨn hin biːr haŋarɨ͡an naːda – tu͡ok tuksarɨn. </ta>
            <ta e="T452" id="Seg_741" s="T448">– Atɨn ologu… </ta>
            <ta e="T459" id="Seg_742" s="T455">– Batallar du? </ta>
            <ta e="T634" id="Seg_743" s="T630">– Hatanɨ͡aktara hu͡oga. </ta>
            <ta e="T658" id="Seg_744" s="T638">– Dʼe, tojonnor taːk daːganɨ dumajdaːnnar, diːbin, inni di͡ek kajdak (olok-) ologu tuksarɨ͡akka di͡en, iti tuhunan kepsetiː ulakan bu͡olu͡o bu͡o. </ta>
            <ta e="T744" id="Seg_745" s="T724">– Inni di͡ek deŋŋe kelen istekkinen (manna) Nosku͡ottan manna, Dudʼinkaga, radʼia kelen ihi͡eŋ diː, kiːri͡eŋ dʼiː, enigin manna kihiler bileller. </ta>
            <ta e="T749" id="Seg_746" s="T744">Üleliːr da kihiler ɨrɨ͡alargɨn isteːččiler. </ta>
            <ta e="T763" id="Seg_747" s="T749">Tak što bert eteŋŋe bagajdɨk olor, diːbin, bert eteŋŋe bagaj oloktoːk bu͡ol inni di͡ek. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KuNS">
            <ta e="T2" id="Seg_748" s="T1">tɨ͡a-ga</ta>
            <ta e="T3" id="Seg_749" s="T2">hɨldʼ-aːččɨ-gɨn</ta>
            <ta e="T4" id="Seg_750" s="T3">tɨ͡a-ga</ta>
            <ta e="T5" id="Seg_751" s="T4">bar-a</ta>
            <ta e="T6" id="Seg_752" s="T5">tüh-eːčči-gin</ta>
            <ta e="T7" id="Seg_753" s="T6">kergen-ner-ge-r</ta>
            <ta e="T66" id="Seg_754" s="T65">tɨ͡a-taːgɨ-lar</ta>
            <ta e="T67" id="Seg_755" s="T66">taːk</ta>
            <ta e="T68" id="Seg_756" s="T67">da</ta>
            <ta e="T69" id="Seg_757" s="T68">hanaː-larɨ-n</ta>
            <ta e="T70" id="Seg_758" s="T69">ist-e-gin</ta>
            <ta e="T71" id="Seg_759" s="T70">e</ta>
            <ta e="T72" id="Seg_760" s="T71">olok-toro</ta>
            <ta e="T73" id="Seg_761" s="T72">kɨtaːnag-ɨ-n</ta>
            <ta e="T74" id="Seg_762" s="T73">kör-ö-gün</ta>
            <ta e="T75" id="Seg_763" s="T74">tak</ta>
            <ta e="T76" id="Seg_764" s="T75">ta</ta>
            <ta e="T77" id="Seg_765" s="T76">hanargɨː-l-larɨ-n</ta>
            <ta e="T78" id="Seg_766" s="T77">ist-e-gin</ta>
            <ta e="T79" id="Seg_767" s="T78">de</ta>
            <ta e="T80" id="Seg_768" s="T79">olog-u</ta>
            <ta e="T81" id="Seg_769" s="T80">üčügej-dik</ta>
            <ta e="T82" id="Seg_770" s="T81">kajdak</ta>
            <ta e="T83" id="Seg_771" s="T82">oŋor-u͡ok-ka</ta>
            <ta e="T84" id="Seg_772" s="T83">di͡e-n</ta>
            <ta e="T120" id="Seg_773" s="T118">olus</ta>
            <ta e="T121" id="Seg_774" s="T120">hanaːrgaː-bat-tar</ta>
            <ta e="T122" id="Seg_775" s="T121">da</ta>
            <ta e="T322" id="Seg_776" s="T320">tu͡ok</ta>
            <ta e="T323" id="Seg_777" s="T322">kömölöh-ör</ta>
            <ta e="T324" id="Seg_778" s="T323">kördüg=üj</ta>
            <ta e="T325" id="Seg_779" s="T324">diːn</ta>
            <ta e="T326" id="Seg_780" s="T325">olok</ta>
            <ta e="T327" id="Seg_781" s="T326">tuks-ar-ɨ-gar</ta>
            <ta e="T328" id="Seg_782" s="T327">tu͡ok</ta>
            <ta e="T329" id="Seg_783" s="T328">kömölöh-ör</ta>
            <ta e="T330" id="Seg_784" s="T329">kördüg=üj</ta>
            <ta e="T331" id="Seg_785" s="T330">kihi-ler</ta>
            <ta e="T332" id="Seg_786" s="T331">beje-lere</ta>
            <ta e="T333" id="Seg_787" s="T332">duː</ta>
            <ta e="T334" id="Seg_788" s="T333">tojon-nor</ta>
            <ta e="T335" id="Seg_789" s="T334">du</ta>
            <ta e="T351" id="Seg_790" s="T349">dʼe</ta>
            <ta e="T352" id="Seg_791" s="T351">kihi-ŋ</ta>
            <ta e="T353" id="Seg_792" s="T352">barɨ-ta</ta>
            <ta e="T354" id="Seg_793" s="T353">eː</ta>
            <ta e="T355" id="Seg_794" s="T354">hin</ta>
            <ta e="T356" id="Seg_795" s="T355">da</ta>
            <ta e="T357" id="Seg_796" s="T356">beje-ti-n</ta>
            <ta e="T358" id="Seg_797" s="T357">hanaː-tɨ-n</ta>
            <ta e="T359" id="Seg_798" s="T358">hin</ta>
            <ta e="T361" id="Seg_799" s="T359">biːr</ta>
            <ta e="T363" id="Seg_800" s="T361">haŋar-ɨ͡a-n</ta>
            <ta e="T364" id="Seg_801" s="T363">naːda</ta>
            <ta e="T366" id="Seg_802" s="T364">tu͡ok</ta>
            <ta e="T367" id="Seg_803" s="T366">tuks-ar-ɨ-n</ta>
            <ta e="T450" id="Seg_804" s="T448">atɨn</ta>
            <ta e="T452" id="Seg_805" s="T450">olog-u</ta>
            <ta e="T457" id="Seg_806" s="T455">bat-al-lar</ta>
            <ta e="T459" id="Seg_807" s="T457">du</ta>
            <ta e="T632" id="Seg_808" s="T630">hatan-ɨ͡ak-tara</ta>
            <ta e="T634" id="Seg_809" s="T632">hu͡og-a</ta>
            <ta e="T639" id="Seg_810" s="T638">dʼe</ta>
            <ta e="T640" id="Seg_811" s="T639">tojon-nor</ta>
            <ta e="T641" id="Seg_812" s="T640">taːk</ta>
            <ta e="T642" id="Seg_813" s="T641">daːganɨ</ta>
            <ta e="T643" id="Seg_814" s="T642">dumaj-daː-n-nar</ta>
            <ta e="T644" id="Seg_815" s="T643">d-iː-bin</ta>
            <ta e="T645" id="Seg_816" s="T644">inni</ta>
            <ta e="T646" id="Seg_817" s="T645">di͡ek</ta>
            <ta e="T647" id="Seg_818" s="T646">kajdak</ta>
            <ta e="T648" id="Seg_819" s="T647">olok</ta>
            <ta e="T649" id="Seg_820" s="T648">olog-u</ta>
            <ta e="T650" id="Seg_821" s="T649">tuks-a-r-ɨ͡ak-ka</ta>
            <ta e="T651" id="Seg_822" s="T650">di͡e-n</ta>
            <ta e="T652" id="Seg_823" s="T651">iti</ta>
            <ta e="T653" id="Seg_824" s="T652">tuh-u-nan</ta>
            <ta e="T654" id="Seg_825" s="T653">kepset-iː</ta>
            <ta e="T655" id="Seg_826" s="T654">ulakan</ta>
            <ta e="T656" id="Seg_827" s="T655">bu͡ol-u͡o</ta>
            <ta e="T658" id="Seg_828" s="T656">bu͡o</ta>
            <ta e="T726" id="Seg_829" s="T724">inni</ta>
            <ta e="T727" id="Seg_830" s="T726">di͡ek</ta>
            <ta e="T728" id="Seg_831" s="T727">deŋŋe</ta>
            <ta e="T729" id="Seg_832" s="T728">kel-en</ta>
            <ta e="T730" id="Seg_833" s="T729">is-tek-kinen</ta>
            <ta e="T731" id="Seg_834" s="T730">manna</ta>
            <ta e="T732" id="Seg_835" s="T731">Nosku͡o-ttan</ta>
            <ta e="T733" id="Seg_836" s="T732">manna</ta>
            <ta e="T734" id="Seg_837" s="T733">Dudʼinka-ga</ta>
            <ta e="T735" id="Seg_838" s="T734">radʼia</ta>
            <ta e="T736" id="Seg_839" s="T735">kel-en</ta>
            <ta e="T737" id="Seg_840" s="T736">ih-i͡e-ŋ</ta>
            <ta e="T738" id="Seg_841" s="T737">diː</ta>
            <ta e="T739" id="Seg_842" s="T738">kiːr-i͡e-ŋ</ta>
            <ta e="T740" id="Seg_843" s="T739">dʼiː</ta>
            <ta e="T741" id="Seg_844" s="T740">enigi-n</ta>
            <ta e="T742" id="Seg_845" s="T741">manna</ta>
            <ta e="T743" id="Seg_846" s="T742">kihi-ler</ta>
            <ta e="T744" id="Seg_847" s="T743">bil-el-ler</ta>
            <ta e="T745" id="Seg_848" s="T744">üleliː-r</ta>
            <ta e="T746" id="Seg_849" s="T745">da</ta>
            <ta e="T747" id="Seg_850" s="T746">kihi-ler</ta>
            <ta e="T748" id="Seg_851" s="T747">ɨrɨ͡a-lar-gɨ-n</ta>
            <ta e="T749" id="Seg_852" s="T748">ist-eːčči-ler</ta>
            <ta e="T750" id="Seg_853" s="T749">tak</ta>
            <ta e="T751" id="Seg_854" s="T750">što</ta>
            <ta e="T752" id="Seg_855" s="T751">bert</ta>
            <ta e="T753" id="Seg_856" s="T752">eteŋŋe</ta>
            <ta e="T754" id="Seg_857" s="T753">bagaj-dɨk</ta>
            <ta e="T755" id="Seg_858" s="T754">olor</ta>
            <ta e="T756" id="Seg_859" s="T755">d-iː-bin</ta>
            <ta e="T757" id="Seg_860" s="T756">bert</ta>
            <ta e="T758" id="Seg_861" s="T757">eteŋŋe</ta>
            <ta e="T759" id="Seg_862" s="T758">bagaj</ta>
            <ta e="T760" id="Seg_863" s="T759">olok-toːk</ta>
            <ta e="T761" id="Seg_864" s="T760">bu͡ol</ta>
            <ta e="T762" id="Seg_865" s="T761">inni</ta>
            <ta e="T763" id="Seg_866" s="T762">di͡ek</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KuNS">
            <ta e="T2" id="Seg_867" s="T1">tɨ͡a-GA</ta>
            <ta e="T3" id="Seg_868" s="T2">hɨrɨt-AːččI-GIn</ta>
            <ta e="T4" id="Seg_869" s="T3">tɨ͡a-GA</ta>
            <ta e="T5" id="Seg_870" s="T4">bar-A</ta>
            <ta e="T6" id="Seg_871" s="T5">tüs-AːččI-GIn</ta>
            <ta e="T7" id="Seg_872" s="T6">kergen-LAr-GA-r</ta>
            <ta e="T66" id="Seg_873" s="T65">tɨ͡a-LAːgI-LAr</ta>
            <ta e="T67" id="Seg_874" s="T66">taːk</ta>
            <ta e="T68" id="Seg_875" s="T67">da</ta>
            <ta e="T69" id="Seg_876" s="T68">hanaː-LArI-n</ta>
            <ta e="T70" id="Seg_877" s="T69">ihit-A-GIn</ta>
            <ta e="T71" id="Seg_878" s="T70">e</ta>
            <ta e="T72" id="Seg_879" s="T71">olok-LArA</ta>
            <ta e="T73" id="Seg_880" s="T72">kɨtaːnak-tI-n</ta>
            <ta e="T74" id="Seg_881" s="T73">kör-A-GIn</ta>
            <ta e="T75" id="Seg_882" s="T74">taːk</ta>
            <ta e="T76" id="Seg_883" s="T75">da</ta>
            <ta e="T77" id="Seg_884" s="T76">hanaːrgaː-Ar-LArI-n</ta>
            <ta e="T78" id="Seg_885" s="T77">ihit-A-GIn</ta>
            <ta e="T79" id="Seg_886" s="T78">dʼe</ta>
            <ta e="T80" id="Seg_887" s="T79">olok-nI</ta>
            <ta e="T81" id="Seg_888" s="T80">üčügej-LIk</ta>
            <ta e="T82" id="Seg_889" s="T81">kajdak</ta>
            <ta e="T83" id="Seg_890" s="T82">oŋor-IAK-GA</ta>
            <ta e="T84" id="Seg_891" s="T83">di͡e-An</ta>
            <ta e="T120" id="Seg_892" s="T118">olus</ta>
            <ta e="T121" id="Seg_893" s="T120">hanaːrgaː-BAT-LAr</ta>
            <ta e="T122" id="Seg_894" s="T121">da</ta>
            <ta e="T322" id="Seg_895" s="T320">tu͡ok</ta>
            <ta e="T323" id="Seg_896" s="T322">kömölös-Ar</ta>
            <ta e="T324" id="Seg_897" s="T323">kördük=Ij</ta>
            <ta e="T325" id="Seg_898" s="T324">diː</ta>
            <ta e="T326" id="Seg_899" s="T325">olok</ta>
            <ta e="T327" id="Seg_900" s="T326">tugus-Ar-tI-GAr</ta>
            <ta e="T328" id="Seg_901" s="T327">tu͡ok</ta>
            <ta e="T329" id="Seg_902" s="T328">kömölös-Ar</ta>
            <ta e="T330" id="Seg_903" s="T329">kördük=Ij</ta>
            <ta e="T331" id="Seg_904" s="T330">kihi-LAr</ta>
            <ta e="T332" id="Seg_905" s="T331">beje-LArA</ta>
            <ta e="T333" id="Seg_906" s="T332">du͡o</ta>
            <ta e="T334" id="Seg_907" s="T333">tojon-LAr</ta>
            <ta e="T335" id="Seg_908" s="T334">du͡o</ta>
            <ta e="T351" id="Seg_909" s="T349">dʼe</ta>
            <ta e="T352" id="Seg_910" s="T351">kihi-ŋ</ta>
            <ta e="T353" id="Seg_911" s="T352">barɨ-tA</ta>
            <ta e="T354" id="Seg_912" s="T353">eː</ta>
            <ta e="T355" id="Seg_913" s="T354">hin</ta>
            <ta e="T356" id="Seg_914" s="T355">da</ta>
            <ta e="T357" id="Seg_915" s="T356">beje-tI-n</ta>
            <ta e="T358" id="Seg_916" s="T357">hanaː-tI-n</ta>
            <ta e="T359" id="Seg_917" s="T358">hin</ta>
            <ta e="T361" id="Seg_918" s="T359">biːr</ta>
            <ta e="T363" id="Seg_919" s="T361">haŋar-IAK.[tI]-n</ta>
            <ta e="T364" id="Seg_920" s="T363">naːda</ta>
            <ta e="T366" id="Seg_921" s="T364">tu͡ok</ta>
            <ta e="T367" id="Seg_922" s="T366">tugus-Ar-tI-n</ta>
            <ta e="T450" id="Seg_923" s="T448">atɨn</ta>
            <ta e="T452" id="Seg_924" s="T450">olok-nI</ta>
            <ta e="T457" id="Seg_925" s="T455">bat-Ar-LAr</ta>
            <ta e="T459" id="Seg_926" s="T457">du͡o</ta>
            <ta e="T632" id="Seg_927" s="T630">hatan-IAK-LArA</ta>
            <ta e="T634" id="Seg_928" s="T632">hu͡ok-tA</ta>
            <ta e="T639" id="Seg_929" s="T638">dʼe</ta>
            <ta e="T640" id="Seg_930" s="T639">tojon-LAr</ta>
            <ta e="T641" id="Seg_931" s="T640">taːk</ta>
            <ta e="T642" id="Seg_932" s="T641">daːganɨ</ta>
            <ta e="T643" id="Seg_933" s="T642">dumaj-LAː-An-LAr</ta>
            <ta e="T644" id="Seg_934" s="T643">di͡e-A-BIn</ta>
            <ta e="T645" id="Seg_935" s="T644">ilin</ta>
            <ta e="T646" id="Seg_936" s="T645">dek</ta>
            <ta e="T647" id="Seg_937" s="T646">kajdak</ta>
            <ta e="T648" id="Seg_938" s="T647">olok</ta>
            <ta e="T649" id="Seg_939" s="T648">olok-nI</ta>
            <ta e="T650" id="Seg_940" s="T649">tugus-A-r-IAK-GA</ta>
            <ta e="T651" id="Seg_941" s="T650">di͡e-An</ta>
            <ta e="T652" id="Seg_942" s="T651">iti</ta>
            <ta e="T653" id="Seg_943" s="T652">tus-tI-nAn</ta>
            <ta e="T654" id="Seg_944" s="T653">kepset-Iː</ta>
            <ta e="T655" id="Seg_945" s="T654">ulakan</ta>
            <ta e="T656" id="Seg_946" s="T655">bu͡ol-IAK.[tA]</ta>
            <ta e="T658" id="Seg_947" s="T656">bu͡o</ta>
            <ta e="T726" id="Seg_948" s="T724">ilin</ta>
            <ta e="T727" id="Seg_949" s="T726">dek</ta>
            <ta e="T728" id="Seg_950" s="T727">deŋŋe</ta>
            <ta e="T729" id="Seg_951" s="T728">kel-An</ta>
            <ta e="T730" id="Seg_952" s="T729">is-TAK-GInA</ta>
            <ta e="T731" id="Seg_953" s="T730">manna</ta>
            <ta e="T732" id="Seg_954" s="T731">Nosku͡o-ttAn</ta>
            <ta e="T733" id="Seg_955" s="T732">manna</ta>
            <ta e="T734" id="Seg_956" s="T733">Dudʼinka-GA</ta>
            <ta e="T735" id="Seg_957" s="T734">radʼio</ta>
            <ta e="T736" id="Seg_958" s="T735">kel-An</ta>
            <ta e="T737" id="Seg_959" s="T736">is-IAK-ŋ</ta>
            <ta e="T738" id="Seg_960" s="T737">diː</ta>
            <ta e="T739" id="Seg_961" s="T738">kiːr-IAK-ŋ</ta>
            <ta e="T740" id="Seg_962" s="T739">diː</ta>
            <ta e="T741" id="Seg_963" s="T740">en-n</ta>
            <ta e="T742" id="Seg_964" s="T741">manna</ta>
            <ta e="T743" id="Seg_965" s="T742">kihi-LAr</ta>
            <ta e="T744" id="Seg_966" s="T743">bil-Ar-LAr</ta>
            <ta e="T745" id="Seg_967" s="T744">üleleː-Ar</ta>
            <ta e="T746" id="Seg_968" s="T745">da</ta>
            <ta e="T747" id="Seg_969" s="T746">kihi-LAr</ta>
            <ta e="T748" id="Seg_970" s="T747">ɨrɨ͡a-LAr-GI-n</ta>
            <ta e="T749" id="Seg_971" s="T748">ihit-AːččI-LAr</ta>
            <ta e="T750" id="Seg_972" s="T749">taːk</ta>
            <ta e="T751" id="Seg_973" s="T750">što</ta>
            <ta e="T752" id="Seg_974" s="T751">bert</ta>
            <ta e="T753" id="Seg_975" s="T752">eteŋŋe</ta>
            <ta e="T754" id="Seg_976" s="T753">bagajɨ-LIk</ta>
            <ta e="T755" id="Seg_977" s="T754">olor</ta>
            <ta e="T756" id="Seg_978" s="T755">di͡e-A-BIn</ta>
            <ta e="T757" id="Seg_979" s="T756">bert</ta>
            <ta e="T758" id="Seg_980" s="T757">eteŋŋe</ta>
            <ta e="T759" id="Seg_981" s="T758">bagajɨ</ta>
            <ta e="T760" id="Seg_982" s="T759">olok-LAːK</ta>
            <ta e="T761" id="Seg_983" s="T760">bu͡ol</ta>
            <ta e="T762" id="Seg_984" s="T761">ilin</ta>
            <ta e="T763" id="Seg_985" s="T762">dek</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KuNS">
            <ta e="T2" id="Seg_986" s="T1">tundra-DAT/LOC</ta>
            <ta e="T3" id="Seg_987" s="T2">be-HAB-2SG</ta>
            <ta e="T4" id="Seg_988" s="T3">tundra-DAT/LOC</ta>
            <ta e="T5" id="Seg_989" s="T4">go-CVB.SIM</ta>
            <ta e="T6" id="Seg_990" s="T5">fall-HAB-2SG</ta>
            <ta e="T7" id="Seg_991" s="T6">parents-PL-2SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_992" s="T65">tundra-ADJZ-PL.[NOM]</ta>
            <ta e="T67" id="Seg_993" s="T66">so</ta>
            <ta e="T68" id="Seg_994" s="T67">EMPH</ta>
            <ta e="T69" id="Seg_995" s="T68">wish-3PL-ACC</ta>
            <ta e="T70" id="Seg_996" s="T69">hear-PRS-2SG</ta>
            <ta e="T71" id="Seg_997" s="T70">eh</ta>
            <ta e="T72" id="Seg_998" s="T71">life-3PL.[NOM]</ta>
            <ta e="T73" id="Seg_999" s="T72">difficult-3SG-ACC</ta>
            <ta e="T74" id="Seg_1000" s="T73">see-PRS-2SG</ta>
            <ta e="T75" id="Seg_1001" s="T74">so</ta>
            <ta e="T76" id="Seg_1002" s="T75">and</ta>
            <ta e="T77" id="Seg_1003" s="T76">be.sad-PTCP.PRS-3PL-ACC</ta>
            <ta e="T78" id="Seg_1004" s="T77">hear-PRS-2SG</ta>
            <ta e="T79" id="Seg_1005" s="T78">well</ta>
            <ta e="T80" id="Seg_1006" s="T79">life-ACC</ta>
            <ta e="T81" id="Seg_1007" s="T80">good-ADVZ</ta>
            <ta e="T82" id="Seg_1008" s="T81">how</ta>
            <ta e="T83" id="Seg_1009" s="T82">make-PTCP.FUT-DAT/LOC</ta>
            <ta e="T84" id="Seg_1010" s="T83">say-CVB.SEQ</ta>
            <ta e="T120" id="Seg_1011" s="T118">very</ta>
            <ta e="T121" id="Seg_1012" s="T120">be.sad-NEG-3PL</ta>
            <ta e="T122" id="Seg_1013" s="T121">EMPH</ta>
            <ta e="T322" id="Seg_1014" s="T320">what.[NOM]</ta>
            <ta e="T323" id="Seg_1015" s="T322">help-PTCP.PRS.[NOM]</ta>
            <ta e="T324" id="Seg_1016" s="T323">like=Q</ta>
            <ta e="T325" id="Seg_1017" s="T324">EMPH</ta>
            <ta e="T326" id="Seg_1018" s="T325">life.[NOM]</ta>
            <ta e="T327" id="Seg_1019" s="T326">improve-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T328" id="Seg_1020" s="T327">what.[NOM]</ta>
            <ta e="T329" id="Seg_1021" s="T328">help-PTCP.PRS.[NOM]</ta>
            <ta e="T330" id="Seg_1022" s="T329">like=Q</ta>
            <ta e="T331" id="Seg_1023" s="T330">human.being-PL.[NOM]</ta>
            <ta e="T332" id="Seg_1024" s="T331">self-3PL.[NOM]</ta>
            <ta e="T333" id="Seg_1025" s="T332">Q</ta>
            <ta e="T334" id="Seg_1026" s="T333">lord-PL.[NOM]</ta>
            <ta e="T335" id="Seg_1027" s="T334">Q</ta>
            <ta e="T351" id="Seg_1028" s="T349">well</ta>
            <ta e="T352" id="Seg_1029" s="T351">human.being-2SG.[NOM]</ta>
            <ta e="T353" id="Seg_1030" s="T352">every-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_1031" s="T353">eh</ta>
            <ta e="T355" id="Seg_1032" s="T354">however</ta>
            <ta e="T356" id="Seg_1033" s="T355">EMPH</ta>
            <ta e="T357" id="Seg_1034" s="T356">self-3SG-GEN</ta>
            <ta e="T358" id="Seg_1035" s="T357">thought-3SG-ACC</ta>
            <ta e="T359" id="Seg_1036" s="T358">however</ta>
            <ta e="T361" id="Seg_1037" s="T359">one</ta>
            <ta e="T363" id="Seg_1038" s="T361">speak-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T364" id="Seg_1039" s="T363">need.to</ta>
            <ta e="T366" id="Seg_1040" s="T364">what.[NOM]</ta>
            <ta e="T367" id="Seg_1041" s="T366">improve-PTCP.PRS-3SG-ACC</ta>
            <ta e="T450" id="Seg_1042" s="T448">different.[NOM]</ta>
            <ta e="T452" id="Seg_1043" s="T450">life-ACC</ta>
            <ta e="T457" id="Seg_1044" s="T455">follow-PRS-3PL</ta>
            <ta e="T459" id="Seg_1045" s="T457">Q</ta>
            <ta e="T632" id="Seg_1046" s="T630">be.able-FUT-3PL</ta>
            <ta e="T634" id="Seg_1047" s="T632">NEG-3SG</ta>
            <ta e="T639" id="Seg_1048" s="T638">well</ta>
            <ta e="T640" id="Seg_1049" s="T639">lord-PL.[NOM]</ta>
            <ta e="T641" id="Seg_1050" s="T640">so</ta>
            <ta e="T642" id="Seg_1051" s="T641">EMPH</ta>
            <ta e="T643" id="Seg_1052" s="T642">think-VBZ-CVB.SEQ-3PL</ta>
            <ta e="T644" id="Seg_1053" s="T643">think-PRS-1SG</ta>
            <ta e="T645" id="Seg_1054" s="T644">front.[NOM]</ta>
            <ta e="T646" id="Seg_1055" s="T645">to</ta>
            <ta e="T647" id="Seg_1056" s="T646">how</ta>
            <ta e="T648" id="Seg_1057" s="T647">life</ta>
            <ta e="T649" id="Seg_1058" s="T648">life-ACC</ta>
            <ta e="T650" id="Seg_1059" s="T649">improve-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T651" id="Seg_1060" s="T650">say-CVB.SEQ</ta>
            <ta e="T652" id="Seg_1061" s="T651">that.[NOM]</ta>
            <ta e="T653" id="Seg_1062" s="T652">side-3SG-INSTR</ta>
            <ta e="T654" id="Seg_1063" s="T653">chat-NMNZ.[NOM]</ta>
            <ta e="T655" id="Seg_1064" s="T654">big.[NOM]</ta>
            <ta e="T656" id="Seg_1065" s="T655">be-FUT.[3SG]</ta>
            <ta e="T658" id="Seg_1066" s="T656">EMPH</ta>
            <ta e="T726" id="Seg_1067" s="T724">front.[NOM]</ta>
            <ta e="T727" id="Seg_1068" s="T726">to</ta>
            <ta e="T728" id="Seg_1069" s="T727">sometimes</ta>
            <ta e="T729" id="Seg_1070" s="T728">come-CVB.SEQ</ta>
            <ta e="T730" id="Seg_1071" s="T729">go-TEMP-2SG</ta>
            <ta e="T731" id="Seg_1072" s="T730">hither</ta>
            <ta e="T732" id="Seg_1073" s="T731">Khatanga-ABL</ta>
            <ta e="T733" id="Seg_1074" s="T732">hither</ta>
            <ta e="T734" id="Seg_1075" s="T733">Dudinka-DAT/LOC</ta>
            <ta e="T735" id="Seg_1076" s="T734">radio.[NOM]</ta>
            <ta e="T736" id="Seg_1077" s="T735">come-CVB.SEQ</ta>
            <ta e="T737" id="Seg_1078" s="T736">go-FUT-2SG</ta>
            <ta e="T738" id="Seg_1079" s="T737">EMPH</ta>
            <ta e="T739" id="Seg_1080" s="T738">go.in-FUT-2SG</ta>
            <ta e="T740" id="Seg_1081" s="T739">EMPH</ta>
            <ta e="T741" id="Seg_1082" s="T740">2SG-ACC</ta>
            <ta e="T742" id="Seg_1083" s="T741">here</ta>
            <ta e="T743" id="Seg_1084" s="T742">human.being-PL.[NOM]</ta>
            <ta e="T744" id="Seg_1085" s="T743">know-PRS-3PL</ta>
            <ta e="T745" id="Seg_1086" s="T744">work-PTCP.PRS</ta>
            <ta e="T746" id="Seg_1087" s="T745">and</ta>
            <ta e="T747" id="Seg_1088" s="T746">human.being-PL.[NOM]</ta>
            <ta e="T748" id="Seg_1089" s="T747">song-PL-2SG-ACC</ta>
            <ta e="T749" id="Seg_1090" s="T748">hear-HAB-3PL</ta>
            <ta e="T750" id="Seg_1091" s="T749">so</ta>
            <ta e="T751" id="Seg_1092" s="T750">that</ta>
            <ta e="T752" id="Seg_1093" s="T751">very</ta>
            <ta e="T753" id="Seg_1094" s="T752">happy.[NOM]</ta>
            <ta e="T754" id="Seg_1095" s="T753">very-ADVZ</ta>
            <ta e="T755" id="Seg_1096" s="T754">live.[IMP.2SG]</ta>
            <ta e="T756" id="Seg_1097" s="T755">say-PRS-1SG</ta>
            <ta e="T757" id="Seg_1098" s="T756">very</ta>
            <ta e="T758" id="Seg_1099" s="T757">happy.[NOM]</ta>
            <ta e="T759" id="Seg_1100" s="T758">very</ta>
            <ta e="T760" id="Seg_1101" s="T759">life-PROPR.[NOM]</ta>
            <ta e="T761" id="Seg_1102" s="T760">be.[IMP.2SG]</ta>
            <ta e="T762" id="Seg_1103" s="T761">front.[NOM]</ta>
            <ta e="T763" id="Seg_1104" s="T762">to</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KuNS">
            <ta e="T2" id="Seg_1105" s="T1">Tundra-DAT/LOC</ta>
            <ta e="T3" id="Seg_1106" s="T2">sich.befinden-HAB-2SG</ta>
            <ta e="T4" id="Seg_1107" s="T3">Tundra-DAT/LOC</ta>
            <ta e="T5" id="Seg_1108" s="T4">gehen-CVB.SIM</ta>
            <ta e="T6" id="Seg_1109" s="T5">fallen-HAB-2SG</ta>
            <ta e="T7" id="Seg_1110" s="T6">Eltern-PL-2SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1111" s="T65">Tundra-ADJZ-PL.[NOM]</ta>
            <ta e="T67" id="Seg_1112" s="T66">so</ta>
            <ta e="T68" id="Seg_1113" s="T67">EMPH</ta>
            <ta e="T69" id="Seg_1114" s="T68">Wunsch-3PL-ACC</ta>
            <ta e="T70" id="Seg_1115" s="T69">hören-PRS-2SG</ta>
            <ta e="T71" id="Seg_1116" s="T70">äh</ta>
            <ta e="T72" id="Seg_1117" s="T71">Leben-3PL.[NOM]</ta>
            <ta e="T73" id="Seg_1118" s="T72">schwer-3SG-ACC</ta>
            <ta e="T74" id="Seg_1119" s="T73">sehen-PRS-2SG</ta>
            <ta e="T75" id="Seg_1120" s="T74">so</ta>
            <ta e="T76" id="Seg_1121" s="T75">und</ta>
            <ta e="T77" id="Seg_1122" s="T76">traurig.sein-PTCP.PRS-3PL-ACC</ta>
            <ta e="T78" id="Seg_1123" s="T77">hören-PRS-2SG</ta>
            <ta e="T79" id="Seg_1124" s="T78">doch</ta>
            <ta e="T80" id="Seg_1125" s="T79">Leben-ACC</ta>
            <ta e="T81" id="Seg_1126" s="T80">gut-ADVZ</ta>
            <ta e="T82" id="Seg_1127" s="T81">wie</ta>
            <ta e="T83" id="Seg_1128" s="T82">machen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T84" id="Seg_1129" s="T83">sagen-CVB.SEQ</ta>
            <ta e="T120" id="Seg_1130" s="T118">sehr</ta>
            <ta e="T121" id="Seg_1131" s="T120">traurig.sein-NEG-3PL</ta>
            <ta e="T122" id="Seg_1132" s="T121">EMPH</ta>
            <ta e="T322" id="Seg_1133" s="T320">was.[NOM]</ta>
            <ta e="T323" id="Seg_1134" s="T322">helfen-PTCP.PRS.[NOM]</ta>
            <ta e="T324" id="Seg_1135" s="T323">wie=Q</ta>
            <ta e="T325" id="Seg_1136" s="T324">EMPH</ta>
            <ta e="T326" id="Seg_1137" s="T325">Leben.[NOM]</ta>
            <ta e="T327" id="Seg_1138" s="T326">verbessern-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T328" id="Seg_1139" s="T327">was.[NOM]</ta>
            <ta e="T329" id="Seg_1140" s="T328">helfen-PTCP.PRS.[NOM]</ta>
            <ta e="T330" id="Seg_1141" s="T329">wie=Q</ta>
            <ta e="T331" id="Seg_1142" s="T330">Mensch-PL.[NOM]</ta>
            <ta e="T332" id="Seg_1143" s="T331">selbst-3PL.[NOM]</ta>
            <ta e="T333" id="Seg_1144" s="T332">Q</ta>
            <ta e="T334" id="Seg_1145" s="T333">Herr-PL.[NOM]</ta>
            <ta e="T335" id="Seg_1146" s="T334">Q</ta>
            <ta e="T351" id="Seg_1147" s="T349">doch</ta>
            <ta e="T352" id="Seg_1148" s="T351">Mensch-2SG.[NOM]</ta>
            <ta e="T353" id="Seg_1149" s="T352">jeder-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_1150" s="T353">äh</ta>
            <ta e="T355" id="Seg_1151" s="T354">doch</ta>
            <ta e="T356" id="Seg_1152" s="T355">EMPH</ta>
            <ta e="T357" id="Seg_1153" s="T356">selbst-3SG-GEN</ta>
            <ta e="T358" id="Seg_1154" s="T357">Gedanke-3SG-ACC</ta>
            <ta e="T359" id="Seg_1155" s="T358">doch</ta>
            <ta e="T361" id="Seg_1156" s="T359">eins</ta>
            <ta e="T363" id="Seg_1157" s="T361">sprechen-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T364" id="Seg_1158" s="T363">man.muss</ta>
            <ta e="T366" id="Seg_1159" s="T364">was.[NOM]</ta>
            <ta e="T367" id="Seg_1160" s="T366">verbessern-PTCP.PRS-3SG-ACC</ta>
            <ta e="T450" id="Seg_1161" s="T448">anders.[NOM]</ta>
            <ta e="T452" id="Seg_1162" s="T450">Leben-ACC</ta>
            <ta e="T457" id="Seg_1163" s="T455">folgen-PRS-3PL</ta>
            <ta e="T459" id="Seg_1164" s="T457">Q</ta>
            <ta e="T632" id="Seg_1165" s="T630">vermögen-FUT-3PL</ta>
            <ta e="T634" id="Seg_1166" s="T632">NEG-3SG</ta>
            <ta e="T639" id="Seg_1167" s="T638">doch</ta>
            <ta e="T640" id="Seg_1168" s="T639">Herr-PL.[NOM]</ta>
            <ta e="T641" id="Seg_1169" s="T640">so</ta>
            <ta e="T642" id="Seg_1170" s="T641">EMPH</ta>
            <ta e="T643" id="Seg_1171" s="T642">denken-VBZ-CVB.SEQ-3PL</ta>
            <ta e="T644" id="Seg_1172" s="T643">denken-PRS-1SG</ta>
            <ta e="T645" id="Seg_1173" s="T644">Vorderteil.[NOM]</ta>
            <ta e="T646" id="Seg_1174" s="T645">zu</ta>
            <ta e="T647" id="Seg_1175" s="T646">wie</ta>
            <ta e="T648" id="Seg_1176" s="T647">Leben</ta>
            <ta e="T649" id="Seg_1177" s="T648">Leben-ACC</ta>
            <ta e="T650" id="Seg_1178" s="T649">verbessern-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T651" id="Seg_1179" s="T650">sagen-CVB.SEQ</ta>
            <ta e="T652" id="Seg_1180" s="T651">dieses.[NOM]</ta>
            <ta e="T653" id="Seg_1181" s="T652">Seite-3SG-INSTR</ta>
            <ta e="T654" id="Seg_1182" s="T653">sich.unterhalten-NMNZ.[NOM]</ta>
            <ta e="T655" id="Seg_1183" s="T654">groß.[NOM]</ta>
            <ta e="T656" id="Seg_1184" s="T655">sein-FUT.[3SG]</ta>
            <ta e="T658" id="Seg_1185" s="T656">EMPH</ta>
            <ta e="T726" id="Seg_1186" s="T724">Vorderteil.[NOM]</ta>
            <ta e="T727" id="Seg_1187" s="T726">zu</ta>
            <ta e="T728" id="Seg_1188" s="T727">manchmal</ta>
            <ta e="T729" id="Seg_1189" s="T728">kommen-CVB.SEQ</ta>
            <ta e="T730" id="Seg_1190" s="T729">gehen-TEMP-2SG</ta>
            <ta e="T731" id="Seg_1191" s="T730">hierher</ta>
            <ta e="T732" id="Seg_1192" s="T731">Chatanga-ABL</ta>
            <ta e="T733" id="Seg_1193" s="T732">hierher</ta>
            <ta e="T734" id="Seg_1194" s="T733">Dudinka-DAT/LOC</ta>
            <ta e="T735" id="Seg_1195" s="T734">radio.[NOM]</ta>
            <ta e="T736" id="Seg_1196" s="T735">kommen-CVB.SEQ</ta>
            <ta e="T737" id="Seg_1197" s="T736">gehen-FUT-2SG</ta>
            <ta e="T738" id="Seg_1198" s="T737">EMPH</ta>
            <ta e="T739" id="Seg_1199" s="T738">hineingehen-FUT-2SG</ta>
            <ta e="T740" id="Seg_1200" s="T739">EMPH</ta>
            <ta e="T741" id="Seg_1201" s="T740">2SG-ACC</ta>
            <ta e="T742" id="Seg_1202" s="T741">hier</ta>
            <ta e="T743" id="Seg_1203" s="T742">Mensch-PL.[NOM]</ta>
            <ta e="T744" id="Seg_1204" s="T743">wissen-PRS-3PL</ta>
            <ta e="T745" id="Seg_1205" s="T744">arbeiten-PTCP.PRS</ta>
            <ta e="T746" id="Seg_1206" s="T745">und</ta>
            <ta e="T747" id="Seg_1207" s="T746">Mensch-PL.[NOM]</ta>
            <ta e="T748" id="Seg_1208" s="T747">Lied-PL-2SG-ACC</ta>
            <ta e="T749" id="Seg_1209" s="T748">hören-HAB-3PL</ta>
            <ta e="T750" id="Seg_1210" s="T749">so</ta>
            <ta e="T751" id="Seg_1211" s="T750">dass</ta>
            <ta e="T752" id="Seg_1212" s="T751">sehr</ta>
            <ta e="T753" id="Seg_1213" s="T752">glücklich.[NOM]</ta>
            <ta e="T754" id="Seg_1214" s="T753">sehr-ADVZ</ta>
            <ta e="T755" id="Seg_1215" s="T754">leben.[IMP.2SG]</ta>
            <ta e="T756" id="Seg_1216" s="T755">sagen-PRS-1SG</ta>
            <ta e="T757" id="Seg_1217" s="T756">sehr</ta>
            <ta e="T758" id="Seg_1218" s="T757">glücklich.[NOM]</ta>
            <ta e="T759" id="Seg_1219" s="T758">sehr</ta>
            <ta e="T760" id="Seg_1220" s="T759">Leben-PROPR.[NOM]</ta>
            <ta e="T761" id="Seg_1221" s="T760">sein.[IMP.2SG]</ta>
            <ta e="T762" id="Seg_1222" s="T761">Vorderteil.[NOM]</ta>
            <ta e="T763" id="Seg_1223" s="T762">zu</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KuNS">
            <ta e="T2" id="Seg_1224" s="T1">тундра-DAT/LOC</ta>
            <ta e="T3" id="Seg_1225" s="T2">находиться-HAB-2SG</ta>
            <ta e="T4" id="Seg_1226" s="T3">тундра-DAT/LOC</ta>
            <ta e="T5" id="Seg_1227" s="T4">идти-CVB.SIM</ta>
            <ta e="T6" id="Seg_1228" s="T5">падать-HAB-2SG</ta>
            <ta e="T7" id="Seg_1229" s="T6">родители-PL-2SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1230" s="T65">тундра-ADJZ-PL.[NOM]</ta>
            <ta e="T67" id="Seg_1231" s="T66">так</ta>
            <ta e="T68" id="Seg_1232" s="T67">EMPH</ta>
            <ta e="T69" id="Seg_1233" s="T68">желание-3PL-ACC</ta>
            <ta e="T70" id="Seg_1234" s="T69">слышать-PRS-2SG</ta>
            <ta e="T71" id="Seg_1235" s="T70">э</ta>
            <ta e="T72" id="Seg_1236" s="T71">жизнь-3PL.[NOM]</ta>
            <ta e="T73" id="Seg_1237" s="T72">трудный-3SG-ACC</ta>
            <ta e="T74" id="Seg_1238" s="T73">видеть-PRS-2SG</ta>
            <ta e="T75" id="Seg_1239" s="T74">так</ta>
            <ta e="T76" id="Seg_1240" s="T75">да</ta>
            <ta e="T77" id="Seg_1241" s="T76">грустить-PTCP.PRS-3PL-ACC</ta>
            <ta e="T78" id="Seg_1242" s="T77">слышать-PRS-2SG</ta>
            <ta e="T79" id="Seg_1243" s="T78">вот</ta>
            <ta e="T80" id="Seg_1244" s="T79">жизнь-ACC</ta>
            <ta e="T81" id="Seg_1245" s="T80">хороший-ADVZ</ta>
            <ta e="T82" id="Seg_1246" s="T81">как</ta>
            <ta e="T83" id="Seg_1247" s="T82">делать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T84" id="Seg_1248" s="T83">говорить-CVB.SEQ</ta>
            <ta e="T120" id="Seg_1249" s="T118">очень</ta>
            <ta e="T121" id="Seg_1250" s="T120">грустить-NEG-3PL</ta>
            <ta e="T122" id="Seg_1251" s="T121">EMPH</ta>
            <ta e="T322" id="Seg_1252" s="T320">что.[NOM]</ta>
            <ta e="T323" id="Seg_1253" s="T322">помогать-PTCP.PRS.[NOM]</ta>
            <ta e="T324" id="Seg_1254" s="T323">как=Q</ta>
            <ta e="T325" id="Seg_1255" s="T324">EMPH</ta>
            <ta e="T326" id="Seg_1256" s="T325">жизнь.[NOM]</ta>
            <ta e="T327" id="Seg_1257" s="T326">улучшать-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T328" id="Seg_1258" s="T327">что.[NOM]</ta>
            <ta e="T329" id="Seg_1259" s="T328">помогать-PTCP.PRS.[NOM]</ta>
            <ta e="T330" id="Seg_1260" s="T329">как=Q</ta>
            <ta e="T331" id="Seg_1261" s="T330">человек-PL.[NOM]</ta>
            <ta e="T332" id="Seg_1262" s="T331">сам-3PL.[NOM]</ta>
            <ta e="T333" id="Seg_1263" s="T332">Q</ta>
            <ta e="T334" id="Seg_1264" s="T333">господин-PL.[NOM]</ta>
            <ta e="T335" id="Seg_1265" s="T334">Q</ta>
            <ta e="T351" id="Seg_1266" s="T349">вот</ta>
            <ta e="T352" id="Seg_1267" s="T351">человек-2SG.[NOM]</ta>
            <ta e="T353" id="Seg_1268" s="T352">каждый-3SG.[NOM]</ta>
            <ta e="T354" id="Seg_1269" s="T353">ээ</ta>
            <ta e="T355" id="Seg_1270" s="T354">ведь</ta>
            <ta e="T356" id="Seg_1271" s="T355">EMPH</ta>
            <ta e="T357" id="Seg_1272" s="T356">сам-3SG-GEN</ta>
            <ta e="T358" id="Seg_1273" s="T357">мысль-3SG-ACC</ta>
            <ta e="T359" id="Seg_1274" s="T358">ведь</ta>
            <ta e="T361" id="Seg_1275" s="T359">один</ta>
            <ta e="T363" id="Seg_1276" s="T361">говорить-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T364" id="Seg_1277" s="T363">надо</ta>
            <ta e="T366" id="Seg_1278" s="T364">что.[NOM]</ta>
            <ta e="T367" id="Seg_1279" s="T366">улучшать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T450" id="Seg_1280" s="T448">другой.[NOM]</ta>
            <ta e="T452" id="Seg_1281" s="T450">жизнь-ACC</ta>
            <ta e="T457" id="Seg_1282" s="T455">следовать-PRS-3PL</ta>
            <ta e="T459" id="Seg_1283" s="T457">Q</ta>
            <ta e="T632" id="Seg_1284" s="T630">уметь-FUT-3PL</ta>
            <ta e="T634" id="Seg_1285" s="T632">NEG-3SG</ta>
            <ta e="T639" id="Seg_1286" s="T638">вот</ta>
            <ta e="T640" id="Seg_1287" s="T639">господин-PL.[NOM]</ta>
            <ta e="T641" id="Seg_1288" s="T640">так</ta>
            <ta e="T642" id="Seg_1289" s="T641">EMPH</ta>
            <ta e="T643" id="Seg_1290" s="T642">думать-VBZ-CVB.SEQ-3PL</ta>
            <ta e="T644" id="Seg_1291" s="T643">думать-PRS-1SG</ta>
            <ta e="T645" id="Seg_1292" s="T644">передняя.часть.[NOM]</ta>
            <ta e="T646" id="Seg_1293" s="T645">к</ta>
            <ta e="T647" id="Seg_1294" s="T646">как</ta>
            <ta e="T648" id="Seg_1295" s="T647">жизнь</ta>
            <ta e="T649" id="Seg_1296" s="T648">жизнь-ACC</ta>
            <ta e="T650" id="Seg_1297" s="T649">улучшать-EP-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T651" id="Seg_1298" s="T650">говорить-CVB.SEQ</ta>
            <ta e="T652" id="Seg_1299" s="T651">тот.[NOM]</ta>
            <ta e="T653" id="Seg_1300" s="T652">сторона-3SG-INSTR</ta>
            <ta e="T654" id="Seg_1301" s="T653">разговаривать-NMNZ.[NOM]</ta>
            <ta e="T655" id="Seg_1302" s="T654">большой.[NOM]</ta>
            <ta e="T656" id="Seg_1303" s="T655">быть-FUT.[3SG]</ta>
            <ta e="T658" id="Seg_1304" s="T656">EMPH</ta>
            <ta e="T726" id="Seg_1305" s="T724">передняя.часть.[NOM]</ta>
            <ta e="T727" id="Seg_1306" s="T726">к</ta>
            <ta e="T728" id="Seg_1307" s="T727">иногда</ta>
            <ta e="T729" id="Seg_1308" s="T728">приходить-CVB.SEQ</ta>
            <ta e="T730" id="Seg_1309" s="T729">идти-TEMP-2SG</ta>
            <ta e="T731" id="Seg_1310" s="T730">сюда</ta>
            <ta e="T732" id="Seg_1311" s="T731">Хатанга-ABL</ta>
            <ta e="T733" id="Seg_1312" s="T732">сюда</ta>
            <ta e="T734" id="Seg_1313" s="T733">Дудинка-DAT/LOC</ta>
            <ta e="T735" id="Seg_1314" s="T734">радио.[NOM]</ta>
            <ta e="T736" id="Seg_1315" s="T735">приходить-CVB.SEQ</ta>
            <ta e="T737" id="Seg_1316" s="T736">идти-FUT-2SG</ta>
            <ta e="T738" id="Seg_1317" s="T737">EMPH</ta>
            <ta e="T739" id="Seg_1318" s="T738">входить-FUT-2SG</ta>
            <ta e="T740" id="Seg_1319" s="T739">EMPH</ta>
            <ta e="T741" id="Seg_1320" s="T740">2SG-ACC</ta>
            <ta e="T742" id="Seg_1321" s="T741">здесь</ta>
            <ta e="T743" id="Seg_1322" s="T742">человек-PL.[NOM]</ta>
            <ta e="T744" id="Seg_1323" s="T743">знать-PRS-3PL</ta>
            <ta e="T745" id="Seg_1324" s="T744">работать-PTCP.PRS</ta>
            <ta e="T746" id="Seg_1325" s="T745">да</ta>
            <ta e="T747" id="Seg_1326" s="T746">человек-PL.[NOM]</ta>
            <ta e="T748" id="Seg_1327" s="T747">песня-PL-2SG-ACC</ta>
            <ta e="T749" id="Seg_1328" s="T748">слышать-HAB-3PL</ta>
            <ta e="T750" id="Seg_1329" s="T749">так</ta>
            <ta e="T751" id="Seg_1330" s="T750">что</ta>
            <ta e="T752" id="Seg_1331" s="T751">очень</ta>
            <ta e="T753" id="Seg_1332" s="T752">счастливый.[NOM]</ta>
            <ta e="T754" id="Seg_1333" s="T753">очень-ADVZ</ta>
            <ta e="T755" id="Seg_1334" s="T754">жить.[IMP.2SG]</ta>
            <ta e="T756" id="Seg_1335" s="T755">говорить-PRS-1SG</ta>
            <ta e="T757" id="Seg_1336" s="T756">очень</ta>
            <ta e="T758" id="Seg_1337" s="T757">счастливый.[NOM]</ta>
            <ta e="T759" id="Seg_1338" s="T758">очень</ta>
            <ta e="T760" id="Seg_1339" s="T759">жизнь-PROPR.[NOM]</ta>
            <ta e="T761" id="Seg_1340" s="T760">быть.[IMP.2SG]</ta>
            <ta e="T762" id="Seg_1341" s="T761">передняя.часть.[NOM]</ta>
            <ta e="T763" id="Seg_1342" s="T762">к</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KuNS">
            <ta e="T2" id="Seg_1343" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_1344" s="T2">v-v:mood-v:pred.pn</ta>
            <ta e="T4" id="Seg_1345" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1346" s="T4">v-v:cvb</ta>
            <ta e="T6" id="Seg_1347" s="T5">v-v:mood-v:pred.pn</ta>
            <ta e="T7" id="Seg_1348" s="T6">n-n:(num)-n:poss-n:case</ta>
            <ta e="T66" id="Seg_1349" s="T65">n-n&gt;adj-n:(num).[n:case]</ta>
            <ta e="T67" id="Seg_1350" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_1351" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_1352" s="T68">n-n:poss-n:case</ta>
            <ta e="T70" id="Seg_1353" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_1354" s="T70">interj</ta>
            <ta e="T72" id="Seg_1355" s="T71">n-n:(poss).[n:case]</ta>
            <ta e="T73" id="Seg_1356" s="T72">adj-n:poss-n:case</ta>
            <ta e="T74" id="Seg_1357" s="T73">v-v:tense-v:pred.pn</ta>
            <ta e="T75" id="Seg_1358" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1359" s="T75">conj</ta>
            <ta e="T77" id="Seg_1360" s="T76">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T78" id="Seg_1361" s="T77">v-v:tense-v:pred.pn</ta>
            <ta e="T79" id="Seg_1362" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1363" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1364" s="T80">adj-adj&gt;adv</ta>
            <ta e="T82" id="Seg_1365" s="T81">que</ta>
            <ta e="T83" id="Seg_1366" s="T82">v-v:ptcp-v:(case)</ta>
            <ta e="T84" id="Seg_1367" s="T83">v-v:cvb</ta>
            <ta e="T120" id="Seg_1368" s="T118">adv</ta>
            <ta e="T121" id="Seg_1369" s="T120">v-v:(neg)-v:pred.pn</ta>
            <ta e="T122" id="Seg_1370" s="T121">ptcl</ta>
            <ta e="T322" id="Seg_1371" s="T320">que.[pro:case]</ta>
            <ta e="T323" id="Seg_1372" s="T322">v-v:ptcp.[v:(case)]</ta>
            <ta e="T324" id="Seg_1373" s="T323">post=ptcl</ta>
            <ta e="T325" id="Seg_1374" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_1375" s="T325">n.[n:case]</ta>
            <ta e="T327" id="Seg_1376" s="T326">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T328" id="Seg_1377" s="T327">que.[pro:case]</ta>
            <ta e="T329" id="Seg_1378" s="T328">v-v:ptcp.[v:(case)]</ta>
            <ta e="T330" id="Seg_1379" s="T329">post=ptcl</ta>
            <ta e="T331" id="Seg_1380" s="T330">n-n:(num).[n:case]</ta>
            <ta e="T332" id="Seg_1381" s="T331">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T333" id="Seg_1382" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_1383" s="T333">n-n:(num).[n:case]</ta>
            <ta e="T335" id="Seg_1384" s="T334">ptcl</ta>
            <ta e="T351" id="Seg_1385" s="T349">ptcl</ta>
            <ta e="T352" id="Seg_1386" s="T351">n-n:(poss).[n:case]</ta>
            <ta e="T353" id="Seg_1387" s="T352">adj-n:(poss).[n:case]</ta>
            <ta e="T354" id="Seg_1388" s="T353">interj</ta>
            <ta e="T355" id="Seg_1389" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_1390" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_1391" s="T356">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T358" id="Seg_1392" s="T357">n-n:poss-n:case</ta>
            <ta e="T359" id="Seg_1393" s="T358">ptcl</ta>
            <ta e="T361" id="Seg_1394" s="T359">cardnum</ta>
            <ta e="T363" id="Seg_1395" s="T361">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T364" id="Seg_1396" s="T363">ptcl</ta>
            <ta e="T366" id="Seg_1397" s="T364">que.[pro:case]</ta>
            <ta e="T367" id="Seg_1398" s="T366">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T450" id="Seg_1399" s="T448">adj.[n:case]</ta>
            <ta e="T452" id="Seg_1400" s="T450">n-n:case</ta>
            <ta e="T457" id="Seg_1401" s="T455">v-v:tense-v:pred.pn</ta>
            <ta e="T459" id="Seg_1402" s="T457">ptcl</ta>
            <ta e="T632" id="Seg_1403" s="T630">v-v:tense-v:poss.pn</ta>
            <ta e="T634" id="Seg_1404" s="T632">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T639" id="Seg_1405" s="T638">ptcl</ta>
            <ta e="T640" id="Seg_1406" s="T639">n-n:(num).[n:case]</ta>
            <ta e="T641" id="Seg_1407" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_1408" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_1409" s="T642">v-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T644" id="Seg_1410" s="T643">v-v:tense-v:pred.pn</ta>
            <ta e="T645" id="Seg_1411" s="T644">n.[n:case]</ta>
            <ta e="T646" id="Seg_1412" s="T645">post</ta>
            <ta e="T647" id="Seg_1413" s="T646">que</ta>
            <ta e="T648" id="Seg_1414" s="T647">n</ta>
            <ta e="T649" id="Seg_1415" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_1416" s="T649">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T651" id="Seg_1417" s="T650">v-v:cvb</ta>
            <ta e="T652" id="Seg_1418" s="T651">dempro.[pro:case]</ta>
            <ta e="T653" id="Seg_1419" s="T652">n-n:poss-n:case</ta>
            <ta e="T654" id="Seg_1420" s="T653">v-v&gt;n.[n:case]</ta>
            <ta e="T655" id="Seg_1421" s="T654">adj.[n:case]</ta>
            <ta e="T656" id="Seg_1422" s="T655">v-v:tense.[v:poss.pn]</ta>
            <ta e="T658" id="Seg_1423" s="T656">ptcl</ta>
            <ta e="T726" id="Seg_1424" s="T724">n.[n:case]</ta>
            <ta e="T727" id="Seg_1425" s="T726">post</ta>
            <ta e="T728" id="Seg_1426" s="T727">adv</ta>
            <ta e="T729" id="Seg_1427" s="T728">v-v:cvb</ta>
            <ta e="T730" id="Seg_1428" s="T729">v-v:mood-v:temp.pn</ta>
            <ta e="T731" id="Seg_1429" s="T730">adv</ta>
            <ta e="T732" id="Seg_1430" s="T731">propr-n:case</ta>
            <ta e="T733" id="Seg_1431" s="T732">adv</ta>
            <ta e="T734" id="Seg_1432" s="T733">propr-n:case</ta>
            <ta e="T735" id="Seg_1433" s="T734">n.[n:case]</ta>
            <ta e="T736" id="Seg_1434" s="T735">v-v:cvb</ta>
            <ta e="T737" id="Seg_1435" s="T736">v-v:tense-v:poss.pn</ta>
            <ta e="T738" id="Seg_1436" s="T737">ptcl</ta>
            <ta e="T739" id="Seg_1437" s="T738">v-v:tense-v:poss.pn</ta>
            <ta e="T740" id="Seg_1438" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_1439" s="T740">pers-pro:case</ta>
            <ta e="T742" id="Seg_1440" s="T741">adv</ta>
            <ta e="T743" id="Seg_1441" s="T742">n-n:(num).[n:case]</ta>
            <ta e="T744" id="Seg_1442" s="T743">v-v:tense-v:pred.pn</ta>
            <ta e="T745" id="Seg_1443" s="T744">v-v:ptcp</ta>
            <ta e="T746" id="Seg_1444" s="T745">conj</ta>
            <ta e="T747" id="Seg_1445" s="T746">n-n:(num).[n:case]</ta>
            <ta e="T748" id="Seg_1446" s="T747">n-n:(num)-n:poss-n:case</ta>
            <ta e="T749" id="Seg_1447" s="T748">v-v:mood-v:pred.pn</ta>
            <ta e="T750" id="Seg_1448" s="T749">ptcl</ta>
            <ta e="T751" id="Seg_1449" s="T750">conj</ta>
            <ta e="T752" id="Seg_1450" s="T751">adv</ta>
            <ta e="T753" id="Seg_1451" s="T752">adj.[n:case]</ta>
            <ta e="T754" id="Seg_1452" s="T753">ptcl-ptcl&gt;adv</ta>
            <ta e="T755" id="Seg_1453" s="T754">v.[v:mood.pn]</ta>
            <ta e="T756" id="Seg_1454" s="T755">v-v:tense-v:pred.pn</ta>
            <ta e="T757" id="Seg_1455" s="T756">adv</ta>
            <ta e="T758" id="Seg_1456" s="T757">adj.[n:case]</ta>
            <ta e="T759" id="Seg_1457" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_1458" s="T759">n-n&gt;adj.[n:case]</ta>
            <ta e="T761" id="Seg_1459" s="T760">v.[v:mood.pn]</ta>
            <ta e="T762" id="Seg_1460" s="T761">n.[n:case]</ta>
            <ta e="T763" id="Seg_1461" s="T762">post</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KuNS">
            <ta e="T2" id="Seg_1462" s="T1">n</ta>
            <ta e="T3" id="Seg_1463" s="T2">v</ta>
            <ta e="T4" id="Seg_1464" s="T3">n</ta>
            <ta e="T5" id="Seg_1465" s="T4">v</ta>
            <ta e="T6" id="Seg_1466" s="T5">aux</ta>
            <ta e="T7" id="Seg_1467" s="T6">n</ta>
            <ta e="T66" id="Seg_1468" s="T65">n</ta>
            <ta e="T67" id="Seg_1469" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_1470" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_1471" s="T68">n</ta>
            <ta e="T70" id="Seg_1472" s="T69">v</ta>
            <ta e="T71" id="Seg_1473" s="T70">interj</ta>
            <ta e="T72" id="Seg_1474" s="T71">n</ta>
            <ta e="T73" id="Seg_1475" s="T72">n</ta>
            <ta e="T74" id="Seg_1476" s="T73">v</ta>
            <ta e="T75" id="Seg_1477" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_1478" s="T75">conj</ta>
            <ta e="T77" id="Seg_1479" s="T76">v</ta>
            <ta e="T78" id="Seg_1480" s="T77">v</ta>
            <ta e="T79" id="Seg_1481" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1482" s="T79">n</ta>
            <ta e="T81" id="Seg_1483" s="T80">adv</ta>
            <ta e="T82" id="Seg_1484" s="T81">que</ta>
            <ta e="T83" id="Seg_1485" s="T82">v</ta>
            <ta e="T84" id="Seg_1486" s="T83">v</ta>
            <ta e="T120" id="Seg_1487" s="T118">adv</ta>
            <ta e="T121" id="Seg_1488" s="T120">v</ta>
            <ta e="T122" id="Seg_1489" s="T121">ptcl</ta>
            <ta e="T322" id="Seg_1490" s="T320">que</ta>
            <ta e="T323" id="Seg_1491" s="T322">v</ta>
            <ta e="T324" id="Seg_1492" s="T323">post</ta>
            <ta e="T325" id="Seg_1493" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_1494" s="T325">n</ta>
            <ta e="T327" id="Seg_1495" s="T326">v</ta>
            <ta e="T328" id="Seg_1496" s="T327">que</ta>
            <ta e="T329" id="Seg_1497" s="T328">v</ta>
            <ta e="T330" id="Seg_1498" s="T329">post</ta>
            <ta e="T331" id="Seg_1499" s="T330">n</ta>
            <ta e="T332" id="Seg_1500" s="T331">emphpro</ta>
            <ta e="T333" id="Seg_1501" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_1502" s="T333">n</ta>
            <ta e="T335" id="Seg_1503" s="T334">ptcl</ta>
            <ta e="T351" id="Seg_1504" s="T349">ptcl</ta>
            <ta e="T352" id="Seg_1505" s="T351">n</ta>
            <ta e="T353" id="Seg_1506" s="T352">adj</ta>
            <ta e="T354" id="Seg_1507" s="T353">interj</ta>
            <ta e="T355" id="Seg_1508" s="T354">ptcl</ta>
            <ta e="T356" id="Seg_1509" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_1510" s="T356">emphpro</ta>
            <ta e="T358" id="Seg_1511" s="T357">n</ta>
            <ta e="T359" id="Seg_1512" s="T358">ptcl</ta>
            <ta e="T361" id="Seg_1513" s="T359">cardnum</ta>
            <ta e="T363" id="Seg_1514" s="T361">v</ta>
            <ta e="T364" id="Seg_1515" s="T363">ptcl</ta>
            <ta e="T366" id="Seg_1516" s="T364">que</ta>
            <ta e="T367" id="Seg_1517" s="T366">v</ta>
            <ta e="T450" id="Seg_1518" s="T448">adj</ta>
            <ta e="T452" id="Seg_1519" s="T450">n</ta>
            <ta e="T457" id="Seg_1520" s="T455">v</ta>
            <ta e="T459" id="Seg_1521" s="T457">ptcl</ta>
            <ta e="T632" id="Seg_1522" s="T630">v</ta>
            <ta e="T634" id="Seg_1523" s="T632">ptcl</ta>
            <ta e="T639" id="Seg_1524" s="T638">ptcl</ta>
            <ta e="T640" id="Seg_1525" s="T639">n</ta>
            <ta e="T641" id="Seg_1526" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_1527" s="T641">ptcl</ta>
            <ta e="T643" id="Seg_1528" s="T642">v</ta>
            <ta e="T644" id="Seg_1529" s="T643">v</ta>
            <ta e="T645" id="Seg_1530" s="T644">n</ta>
            <ta e="T646" id="Seg_1531" s="T645">post</ta>
            <ta e="T647" id="Seg_1532" s="T646">que</ta>
            <ta e="T648" id="Seg_1533" s="T647">n</ta>
            <ta e="T649" id="Seg_1534" s="T648">n</ta>
            <ta e="T650" id="Seg_1535" s="T649">v</ta>
            <ta e="T651" id="Seg_1536" s="T650">v</ta>
            <ta e="T652" id="Seg_1537" s="T651">dempro</ta>
            <ta e="T653" id="Seg_1538" s="T652">n</ta>
            <ta e="T654" id="Seg_1539" s="T653">n</ta>
            <ta e="T655" id="Seg_1540" s="T654">adj</ta>
            <ta e="T656" id="Seg_1541" s="T655">cop</ta>
            <ta e="T658" id="Seg_1542" s="T656">ptcl</ta>
            <ta e="T726" id="Seg_1543" s="T724">n</ta>
            <ta e="T727" id="Seg_1544" s="T726">post</ta>
            <ta e="T728" id="Seg_1545" s="T727">adv</ta>
            <ta e="T729" id="Seg_1546" s="T728">v</ta>
            <ta e="T730" id="Seg_1547" s="T729">aux</ta>
            <ta e="T731" id="Seg_1548" s="T730">adv</ta>
            <ta e="T732" id="Seg_1549" s="T731">propr</ta>
            <ta e="T733" id="Seg_1550" s="T732">adv</ta>
            <ta e="T734" id="Seg_1551" s="T733">propr</ta>
            <ta e="T735" id="Seg_1552" s="T734">n</ta>
            <ta e="T736" id="Seg_1553" s="T735">v</ta>
            <ta e="T737" id="Seg_1554" s="T736">aux</ta>
            <ta e="T738" id="Seg_1555" s="T737">ptcl</ta>
            <ta e="T739" id="Seg_1556" s="T738">v</ta>
            <ta e="T740" id="Seg_1557" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_1558" s="T740">pers</ta>
            <ta e="T742" id="Seg_1559" s="T741">adv</ta>
            <ta e="T743" id="Seg_1560" s="T742">n</ta>
            <ta e="T744" id="Seg_1561" s="T743">v</ta>
            <ta e="T745" id="Seg_1562" s="T744">v</ta>
            <ta e="T746" id="Seg_1563" s="T745">conj</ta>
            <ta e="T747" id="Seg_1564" s="T746">n</ta>
            <ta e="T748" id="Seg_1565" s="T747">n</ta>
            <ta e="T749" id="Seg_1566" s="T748">v</ta>
            <ta e="T750" id="Seg_1567" s="T749">ptcl</ta>
            <ta e="T751" id="Seg_1568" s="T750">conj</ta>
            <ta e="T752" id="Seg_1569" s="T751">adv</ta>
            <ta e="T753" id="Seg_1570" s="T752">adj</ta>
            <ta e="T754" id="Seg_1571" s="T753">adv</ta>
            <ta e="T755" id="Seg_1572" s="T754">v</ta>
            <ta e="T756" id="Seg_1573" s="T755">v</ta>
            <ta e="T757" id="Seg_1574" s="T756">adv</ta>
            <ta e="T758" id="Seg_1575" s="T757">adj</ta>
            <ta e="T759" id="Seg_1576" s="T758">ptcl</ta>
            <ta e="T760" id="Seg_1577" s="T759">adj</ta>
            <ta e="T761" id="Seg_1578" s="T760">cop</ta>
            <ta e="T762" id="Seg_1579" s="T761">n</ta>
            <ta e="T763" id="Seg_1580" s="T762">post</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KuNS" />
         <annotation name="SyF" tierref="SyF-KuNS" />
         <annotation name="IST" tierref="IST-KuNS" />
         <annotation name="Top" tierref="Top-KuNS" />
         <annotation name="Foc" tierref="Foc-KuNS" />
         <annotation name="BOR" tierref="BOR-KuNS">
            <ta e="T67" id="Seg_1581" s="T66">RUS:disc</ta>
            <ta e="T75" id="Seg_1582" s="T74">RUS:disc</ta>
            <ta e="T76" id="Seg_1583" s="T75">RUS:gram</ta>
            <ta e="T364" id="Seg_1584" s="T363">RUS:mod</ta>
            <ta e="T641" id="Seg_1585" s="T640">RUS:disc</ta>
            <ta e="T643" id="Seg_1586" s="T642">RUS:core</ta>
            <ta e="T734" id="Seg_1587" s="T733">RUS:cult</ta>
            <ta e="T735" id="Seg_1588" s="T734">RUS:cult</ta>
            <ta e="T746" id="Seg_1589" s="T745">RUS:gram</ta>
            <ta e="T750" id="Seg_1590" s="T749">RUS:disc</ta>
            <ta e="T751" id="Seg_1591" s="T750">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KuNS" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KuNS">
            <ta e="T67" id="Seg_1592" s="T66">dir:bare</ta>
            <ta e="T75" id="Seg_1593" s="T74">dir:bare</ta>
            <ta e="T76" id="Seg_1594" s="T75">dir:bare</ta>
            <ta e="T364" id="Seg_1595" s="T363">dir:bare</ta>
            <ta e="T641" id="Seg_1596" s="T640">dir:bare</ta>
            <ta e="T643" id="Seg_1597" s="T642">indir:infl</ta>
            <ta e="T734" id="Seg_1598" s="T733">dir:infl</ta>
            <ta e="T735" id="Seg_1599" s="T734">dir:bare</ta>
            <ta e="T746" id="Seg_1600" s="T745">dir:bare</ta>
            <ta e="T750" id="Seg_1601" s="T749">dir:bare</ta>
            <ta e="T751" id="Seg_1602" s="T750">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-KuNS" />
         <annotation name="fe" tierref="fe-KuNS">
            <ta e="T7" id="Seg_1603" s="T1">– Are you sometimes, do you go sometimes to the tundra to your parents?</ta>
            <ta e="T79" id="Seg_1604" s="T65">– Your hear the wishes of the tundra people, you see the difficulties of their life, you hear that they are sad?</ta>
            <ta e="T84" id="Seg_1605" s="T79">How life could be improved.</ta>
            <ta e="T122" id="Seg_1606" s="T118">– They are very sad, aren't they?</ta>
            <ta e="T327" id="Seg_1607" s="T320">– What would help in order to improve the life?</ta>
            <ta e="T335" id="Seg_1608" s="T327">What would help – the people themselves or the politicians?</ta>
            <ta e="T367" id="Seg_1609" s="T349">– Well, all people, eh, nevertheless one has to speak out, what has to be improved.</ta>
            <ta e="T452" id="Seg_1610" s="T448">– A different life...</ta>
            <ta e="T459" id="Seg_1611" s="T455">– Do they follow?</ta>
            <ta e="T634" id="Seg_1612" s="T630">– They won't survive it.</ta>
            <ta e="T658" id="Seg_1613" s="T638">– The politicians think so, I believe there will be a lot of discussion about how to improve life in the future.</ta>
            <ta e="T744" id="Seg_1614" s="T724">– When you sometimes come in the future, here, from Khantaga here to Dudinka, the you'll come to the radio, you'll come in, the people know you here.</ta>
            <ta e="T749" id="Seg_1615" s="T744">And the people working [here] listens to your songs.</ta>
            <ta e="T763" id="Seg_1616" s="T749">So, I say, live very happily, have a good life in the future.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KuNS">
            <ta e="T7" id="Seg_1617" s="T1">– Bist du manchmal, gehst du manchmal in die Tundra zu deinen Eltern?</ta>
            <ta e="T79" id="Seg_1618" s="T65">– Du hörst die Wünsche der Tundraleute, du siehst die Schwierigkeiten ihres Leben, du hörst, dass sie traurig sind?</ta>
            <ta e="T84" id="Seg_1619" s="T79">Wie das Leben besser gemacht werden kann.</ta>
            <ta e="T122" id="Seg_1620" s="T118">– Sie sind nicht sehr traurig, ja?</ta>
            <ta e="T327" id="Seg_1621" s="T320">– Was würde helfen, damit das Leben besser wird?</ta>
            <ta e="T335" id="Seg_1622" s="T327">Was würde helfen – die Leute selbst oder die Politiker?</ta>
            <ta e="T367" id="Seg_1623" s="T349">– Nun, alle Leute, äh, man muss dennoch seine Meinung sagen, was zu verbessern ist.</ta>
            <ta e="T452" id="Seg_1624" s="T448">– Ein anderes Leben…</ta>
            <ta e="T459" id="Seg_1625" s="T455">– Verfolgen sie?</ta>
            <ta e="T634" id="Seg_1626" s="T630">– Sie überleben es nicht.</ta>
            <ta e="T658" id="Seg_1627" s="T638">– Die Politiker denken so, glaube ich, wie man in der Zukunft das Leben verbessern kann, darüber wird es viel Diskussion geben.</ta>
            <ta e="T744" id="Seg_1628" s="T724">– Wenn du in Zukunft manchmal kommst, hierher, aus Chatanga hierher nach Dudinka, dann kommst du im Radio vorbei, du kommst herein, die Leute kennen dich hier.</ta>
            <ta e="T749" id="Seg_1629" s="T744">Und die Leute, die [hier] arbeiten, hören deine Lieder.</ta>
            <ta e="T763" id="Seg_1630" s="T749">So, lebe sehr glücklich, sage ich, habe ein sehr gutes Leben in der Zukunft.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KuNS" />
         <annotation name="ltr" tierref="ltr-KuNS">
            <ta e="T7" id="Seg_1631" s="T1">–В тундре бываешь, в тундру иногда ездиешь к родителям? </ta>
            <ta e="T79" id="Seg_1632" s="T65">Тундравиков желания слышишь, жизнь крепкая видишь, грусть слышишь?</ta>
            <ta e="T84" id="Seg_1633" s="T79">Жизнь лучше как чтобы сделать. </ta>
            <ta e="T122" id="Seg_1634" s="T118">– Сильно не печалятся да?</ta>
            <ta e="T327" id="Seg_1635" s="T320">– Что помочь сможет, думаешь, чтобы жизнь улучшилась?</ta>
            <ta e="T335" id="Seg_1636" s="T327">Что может помочь – люди сами ли, руководители ли?</ta>
            <ta e="T367" id="Seg_1637" s="T349">– Ведь, люд весь, ээ, тоже своё мнение тоже сказать должен – что улучшится. </ta>
            <ta e="T452" id="Seg_1638" s="T448">– Другую жизнь ..</ta>
            <ta e="T459" id="Seg_1639" s="T455">– Догоняют да ?</ta>
            <ta e="T634" id="Seg_1640" s="T630">– Не выживут. </ta>
            <ta e="T658" id="Seg_1641" s="T638">– Руководители так-то подумают, наверное, думаю, в будущем как жизнь жизнь улучшить чтобы, об этом .. разговор большой будет ведь. </ta>
            <ta e="T744" id="Seg_1642" s="T724">– В будущем иногда, если приезжать будешь сюда из Хатанги сюда, в Дудинку, на радио наведывайся, захаживай, тебя здесь люди знают.</ta>
            <ta e="T749" id="Seg_1643" s="T744">И работающие сотрудники песни твои слушают. </ta>
            <ta e="T763" id="Seg_1644" s="T749">Так что очень счастливо живи, желаю, очень благополучную жизнь имей в будещем.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KuNS" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-UkOA"
                      id="tx-UkOA"
                      speaker="UkOA"
                      type="t">
         <timeline-fork end="T373" start="T372">
            <tli id="T372.tx-UkOA.1" />
         </timeline-fork>
         <timeline-fork end="T553" start="T552">
            <tli id="T552.tx-UkOA.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-UkOA">
            <ts e="T65" id="Seg_1645" n="sc" s="T7">
               <ts e="T19" id="Seg_1647" n="HIAT:u" s="T7">
                  <nts id="Seg_1648" n="HIAT:ip">–</nts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_1651" n="HIAT:w" s="T7">Aː</ts>
                  <nts id="Seg_1652" n="HIAT:ip">,</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_1655" n="HIAT:w" s="T8">bejem</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_1658" n="HIAT:w" s="T9">Nosku͡oga</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_1661" n="HIAT:w" s="T10">dʼi͡eleːk</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_1664" n="HIAT:w" s="T11">bu͡olammɨn</ts>
                  <nts id="Seg_1665" n="HIAT:ip">,</nts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_1668" n="HIAT:w" s="T12">eː</ts>
                  <nts id="Seg_1669" n="HIAT:ip">,</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_1672" n="HIAT:w" s="T13">tɨ͡aga</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_1675" n="HIAT:w" s="T14">agɨjaktɨk</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_1678" n="HIAT:w" s="T15">bu͡olabɨn</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_1681" n="HIAT:w" s="T16">bu͡o</ts>
                  <nts id="Seg_1682" n="HIAT:ip">,</nts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_1685" n="HIAT:w" s="T17">hin</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_1688" n="HIAT:w" s="T18">biːr</ts>
                  <nts id="Seg_1689" n="HIAT:ip">.</nts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_1692" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_1694" n="HIAT:w" s="T19">Haːstarɨ</ts>
                  <nts id="Seg_1695" n="HIAT:ip">,</nts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_1698" n="HIAT:w" s="T20">kühünneri</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_1701" n="HIAT:w" s="T21">bu͡ol</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_1704" n="HIAT:w" s="T22">bi͡ek</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_1707" n="HIAT:w" s="T23">tɨ͡aga</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_1710" n="HIAT:w" s="T24">hɨldʼabɨn</ts>
                  <nts id="Seg_1711" n="HIAT:ip">.</nts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_1714" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_1716" n="HIAT:w" s="T25">Kas</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_1719" n="HIAT:w" s="T26">dʼɨl</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_1722" n="HIAT:w" s="T27">aːjɨ</ts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_1725" n="HIAT:w" s="T28">inʼemeːpper</ts>
                  <nts id="Seg_1726" n="HIAT:ip">,</nts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_1729" n="HIAT:w" s="T29">teːtemeːpper</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_1732" n="HIAT:w" s="T30">kömölöhö</ts>
                  <nts id="Seg_1733" n="HIAT:ip">.</nts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_1736" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_1738" n="HIAT:w" s="T31">Ol</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_1741" n="HIAT:w" s="T32">di͡ek</ts>
                  <nts id="Seg_1742" n="HIAT:ip">,</nts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_1745" n="HIAT:w" s="T33">tɨ͡aga</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_1748" n="HIAT:w" s="T34">hɨldʼa</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_1751" n="HIAT:w" s="T35">bu͡olannar</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1753" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_1755" n="HIAT:w" s="T36">gi͡e-</ts>
                  <nts id="Seg_1756" n="HIAT:ip">)</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_1759" n="HIAT:w" s="T37">tabalaːktar</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_1762" n="HIAT:w" s="T38">bu͡o</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_1765" n="HIAT:w" s="T39">ol</ts>
                  <nts id="Seg_1766" n="HIAT:ip">.</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_1769" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_1771" n="HIAT:w" s="T40">Tabalar</ts>
                  <nts id="Seg_1772" n="HIAT:ip">,</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_1775" n="HIAT:w" s="T41">tabalarɨn</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_1778" n="HIAT:w" s="T42">da</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_1781" n="HIAT:w" s="T43">körö</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_1784" n="HIAT:w" s="T44">hotoru</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_1787" n="HIAT:w" s="T45">daːganɨ</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_1790" n="HIAT:w" s="T46">onnugu</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_1793" n="HIAT:w" s="T47">bejem</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_1796" n="HIAT:w" s="T48">daːganɨ</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1798" n="HIAT:ip">(</nts>
                  <ts e="T50" id="Seg_1800" n="HIAT:w" s="T49">zaž-</ts>
                  <nts id="Seg_1801" n="HIAT:ip">)</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_1804" n="HIAT:w" s="T50">kimniːbin</ts>
                  <nts id="Seg_1805" n="HIAT:ip">,</nts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_1808" n="HIAT:w" s="T51">iːtillebin</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_1811" n="HIAT:w" s="T52">eː</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_1814" n="HIAT:w" s="T53">kiminen</ts>
                  <nts id="Seg_1815" n="HIAT:ip">,</nts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_1818" n="HIAT:w" s="T54">energʼijanan</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_1821" n="HIAT:w" s="T55">di͡ebikke</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_1824" n="HIAT:w" s="T56">di͡eri</ts>
                  <nts id="Seg_1825" n="HIAT:ip">.</nts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_1828" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_1830" n="HIAT:w" s="T57">Svʼežij</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_1833" n="HIAT:w" s="T58">vozdux</ts>
                  <nts id="Seg_1834" n="HIAT:ip">.</nts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_1837" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_1839" n="HIAT:w" s="T59">Nosku͡oga</ts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_1842" n="HIAT:w" s="T60">hɨtan</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_1845" n="HIAT:w" s="T61">tɨːnɨn</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1847" n="HIAT:ip">(</nts>
                  <ts e="T63" id="Seg_1849" n="HIAT:w" s="T62">kaːjallar</ts>
                  <nts id="Seg_1850" n="HIAT:ip">)</nts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_1853" n="HIAT:w" s="T63">hol</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_1856" n="HIAT:w" s="T64">kenne</ts>
                  <nts id="Seg_1857" n="HIAT:ip">.</nts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T119" id="Seg_1859" n="sc" s="T84">
               <ts e="T90" id="Seg_1861" n="HIAT:u" s="T84">
                  <nts id="Seg_1862" n="HIAT:ip">–</nts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_1865" n="HIAT:w" s="T84">Tɨ͡ataːgɨlarɨŋ</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_1868" n="HIAT:w" s="T85">bejelere</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_1871" n="HIAT:w" s="T86">bileller</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_1874" n="HIAT:w" s="T87">bu͡o</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_1877" n="HIAT:w" s="T88">kajdak</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_1880" n="HIAT:w" s="T89">oloru͡oktarɨn</ts>
                  <nts id="Seg_1881" n="HIAT:ip">.</nts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_1884" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_1886" n="HIAT:w" s="T90">Anɨ</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_1889" n="HIAT:w" s="T91">üčügej</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_1892" n="HIAT:w" s="T92">ologu</ts>
                  <nts id="Seg_1893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_1895" n="HIAT:w" s="T93">bilbet</ts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_1898" n="HIAT:w" s="T94">bu͡olannar</ts>
                  <nts id="Seg_1899" n="HIAT:ip">,</nts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_1902" n="HIAT:w" s="T95">giller</ts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_1905" n="HIAT:w" s="T96">eː</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_1908" n="HIAT:w" s="T97">üčügejdik</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_1911" n="HIAT:w" s="T98">oloru͡opput</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_1914" n="HIAT:w" s="T99">di͡en</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_1917" n="HIAT:w" s="T100">ispetter</ts>
                  <nts id="Seg_1918" n="HIAT:ip">,</nts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_1921" n="HIAT:w" s="T101">bɨhɨlak</ts>
                  <nts id="Seg_1922" n="HIAT:ip">,</nts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_1925" n="HIAT:w" s="T102">olus</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_1928" n="HIAT:w" s="T103">daːganɨ</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_1931" n="HIAT:w" s="T104">bejelere</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_1934" n="HIAT:w" s="T105">daːganɨ</ts>
                  <nts id="Seg_1935" n="HIAT:ip">.</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_1938" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_1940" n="HIAT:w" s="T106">Tu͡ok</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_1943" n="HIAT:w" s="T107">baːr</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_1946" n="HIAT:w" s="T108">onon</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_1949" n="HIAT:w" s="T109">ü͡öre</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_1952" n="HIAT:w" s="T110">hɨldʼallar</ts>
                  <nts id="Seg_1953" n="HIAT:ip">.</nts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_1956" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_1958" n="HIAT:w" s="T111">Hu͡ok</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_1961" n="HIAT:w" s="T112">bu͡ollagɨna</ts>
                  <nts id="Seg_1962" n="HIAT:ip">,</nts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_1965" n="HIAT:w" s="T113">kajdak</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_1968" n="HIAT:w" s="T114">gɨnɨ͡aj</ts>
                  <nts id="Seg_1969" n="HIAT:ip">,</nts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_1972" n="HIAT:w" s="T115">hu͡ok</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_1975" n="HIAT:w" s="T116">da</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_1978" n="HIAT:w" s="T117">hu͡ok</ts>
                  <nts id="Seg_1979" n="HIAT:ip">.</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T321" id="Seg_1981" n="sc" s="T123">
               <ts e="T127" id="Seg_1983" n="HIAT:u" s="T123">
                  <nts id="Seg_1984" n="HIAT:ip">–</nts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_1987" n="HIAT:w" s="T123">Olus</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_1990" n="HIAT:w" s="T124">da</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_1993" n="HIAT:w" s="T125">hanaːrgaːbattar</ts>
                  <nts id="Seg_1994" n="HIAT:ip">,</nts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_1997" n="HIAT:w" s="T126">eː</ts>
                  <nts id="Seg_1998" n="HIAT:ip">.</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_2001" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_2003" n="HIAT:w" s="T127">A</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_2006" n="HIAT:w" s="T128">jeslʼi</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_2009" n="HIAT:w" s="T129">bu</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2011" n="HIAT:ip">(</nts>
                  <ts e="T131" id="Seg_2013" n="HIAT:w" s="T130">üčü-</ts>
                  <nts id="Seg_2014" n="HIAT:ip">)</nts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_2017" n="HIAT:w" s="T131">üčügejdik</ts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_2020" n="HIAT:w" s="T132">eː</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_2023" n="HIAT:w" s="T133">kihiliː</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_2026" n="HIAT:w" s="T134">bu</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_2029" n="HIAT:w" s="T135">ileliː</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_2032" n="HIAT:w" s="T136">oloror</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_2035" n="HIAT:w" s="T137">kihi</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_2038" n="HIAT:w" s="T138">ba</ts>
                  <nts id="Seg_2039" n="HIAT:ip">,</nts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_2042" n="HIAT:w" s="T139">ile</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_2045" n="HIAT:w" s="T140">daːganɨ</ts>
                  <nts id="Seg_2046" n="HIAT:ip">,</nts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_2049" n="HIAT:w" s="T141">hepsekiː</ts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_2052" n="HIAT:w" s="T142">hogus</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_2055" n="HIAT:w" s="T143">boloktoru</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_2058" n="HIAT:w" s="T144">admʼinʼistracɨjattan</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_2061" n="HIAT:w" s="T145">bi͡eri͡ek</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_2064" n="HIAT:w" s="T146">etilere</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_2067" n="HIAT:w" s="T147">bu͡olla</ts>
                  <nts id="Seg_2068" n="HIAT:ip">,</nts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_2071" n="HIAT:w" s="T148">bu</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_2074" n="HIAT:w" s="T149">nʼeabxadʼimɨj</ts>
                  <nts id="Seg_2075" n="HIAT:ip">,</nts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_2078" n="HIAT:w" s="T150">muŋ</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_2081" n="HIAT:w" s="T151">muŋ</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_2084" n="HIAT:w" s="T152">naːdalaːk</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_2087" n="HIAT:w" s="T153">ebi͡ennʼelere</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_2090" n="HIAT:w" s="T154">tu͡oktara</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2092" n="HIAT:ip">(</nts>
                  <ts e="T156" id="Seg_2094" n="HIAT:w" s="T155">ki-</ts>
                  <nts id="Seg_2095" n="HIAT:ip">)</nts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_2098" n="HIAT:w" s="T156">bu</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_2101" n="HIAT:w" s="T157">ke</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_2104" n="HIAT:w" s="T158">pomašʼ</ts>
                  <nts id="Seg_2105" n="HIAT:ip">,</nts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_2108" n="HIAT:w" s="T159">datacɨja</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_2111" n="HIAT:w" s="T160">kördük</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_2114" n="HIAT:w" s="T161">ulakannɨk</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_2117" n="HIAT:w" s="T162">kelere</ts>
                  <nts id="Seg_2118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_2120" n="HIAT:w" s="T163">bu͡ola</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_2123" n="HIAT:w" s="T164">tɨ͡ataːgɨlarga</ts>
                  <nts id="Seg_2124" n="HIAT:ip">.</nts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_2127" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_2129" n="HIAT:w" s="T165">Bejelerin</ts>
                  <nts id="Seg_2130" n="HIAT:ip">,</nts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_2133" n="HIAT:w" s="T166">bejelerin</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_2136" n="HIAT:w" s="T167">kɨ͡aktarɨnan</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_2139" n="HIAT:w" s="T168">hɨldʼanaːktɨːllar</ts>
                  <nts id="Seg_2140" n="HIAT:ip">,</nts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_2143" n="HIAT:w" s="T169">tɨ͡ataːgɨlarbɨt</ts>
                  <nts id="Seg_2144" n="HIAT:ip">.</nts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_2147" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_2149" n="HIAT:w" s="T170">Ol</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_2152" n="HIAT:w" s="T171">ihin</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_2155" n="HIAT:w" s="T172">anɨ</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_2158" n="HIAT:w" s="T173">össü͡ö</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_2161" n="HIAT:w" s="T174">iti</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_2164" n="HIAT:w" s="T175">tɨːhɨčča</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_2167" n="HIAT:w" s="T176">bi͡es</ts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_2170" n="HIAT:w" s="T177">hüːs</ts>
                  <nts id="Seg_2171" n="HIAT:ip">,</nts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_2174" n="HIAT:w" s="T178">iti</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_2177" n="HIAT:w" s="T179">kim</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_2180" n="HIAT:w" s="T180">kam</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_2183" n="HIAT:w" s="T181">paltaraškanɨ</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_2186" n="HIAT:w" s="T182">bi͡eren</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_2189" n="HIAT:w" s="T183">bu͡olannar</ts>
                  <nts id="Seg_2190" n="HIAT:ip">,</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_2193" n="HIAT:w" s="T184">ješʼo</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_2196" n="HIAT:w" s="T185">hi͡ese</ts>
                  <nts id="Seg_2197" n="HIAT:ip">.</nts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_2200" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_2202" n="HIAT:w" s="T186">Aː</ts>
                  <nts id="Seg_2203" n="HIAT:ip">,</nts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_2206" n="HIAT:w" s="T187">a</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_2209" n="HIAT:w" s="T188">tak</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_2212" n="HIAT:w" s="T189">hapsi͡em</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_2215" n="HIAT:w" s="T190">bu͡o</ts>
                  <nts id="Seg_2216" n="HIAT:ip">.</nts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_2219" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_2221" n="HIAT:w" s="T191">Tu͡ok</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_2224" n="HIAT:w" s="T192">olus</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_2227" n="HIAT:w" s="T193">daːganɨ</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2229" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_2231" n="HIAT:w" s="T194">ast-</ts>
                  <nts id="Seg_2232" n="HIAT:ip">)</nts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_2235" n="HIAT:w" s="T195">ahɨnan</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_2238" n="HIAT:w" s="T196">daːganɨ</ts>
                  <nts id="Seg_2239" n="HIAT:ip">,</nts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_2242" n="HIAT:w" s="T197">kili͡ebinen</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_2245" n="HIAT:w" s="T198">da</ts>
                  <nts id="Seg_2246" n="HIAT:ip">,</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_2249" n="HIAT:w" s="T199">tugunan</ts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_2252" n="HIAT:w" s="T200">da</ts>
                  <nts id="Seg_2253" n="HIAT:ip">,</nts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_2256" n="HIAT:w" s="T201">ol</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_2259" n="HIAT:w" s="T202">pʼikarnʼabɨt</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_2262" n="HIAT:w" s="T203">üleleːbet</ts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_2265" n="HIAT:w" s="T204">bu͡olan</ts>
                  <nts id="Seg_2266" n="HIAT:ip">,</nts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_2269" n="HIAT:w" s="T205">bejelere</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_2272" n="HIAT:w" s="T206">buharɨnaːktɨːllar</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_2275" n="HIAT:w" s="T207">tɨ͡aga</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_2278" n="HIAT:w" s="T208">hɨldʼannar</ts>
                  <nts id="Seg_2279" n="HIAT:ip">,</nts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_2282" n="HIAT:w" s="T209">kannɨk</ts>
                  <nts id="Seg_2283" n="HIAT:ip">…</nts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_2286" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_2288" n="HIAT:w" s="T210">Kannɨk</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_2291" n="HIAT:w" s="T211">ogo</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_2294" n="HIAT:w" s="T212">kihi</ts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2296" n="HIAT:ip">"</nts>
                  <ts e="T214" id="Seg_2298" n="HIAT:w" s="T213">barɨ͡am</ts>
                  <nts id="Seg_2299" n="HIAT:ip">"</nts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_2302" n="HIAT:w" s="T214">di͡ej</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2304" n="HIAT:ip">"</nts>
                  <ts e="T216" id="Seg_2306" n="HIAT:w" s="T215">itiː</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_2309" n="HIAT:w" s="T216">dʼi͡etten</ts>
                  <nts id="Seg_2310" n="HIAT:ip">,</nts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_2313" n="HIAT:w" s="T217">itiː</ts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_2316" n="HIAT:w" s="T218">u͡otton</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_2319" n="HIAT:w" s="T219">taksan</ts>
                  <nts id="Seg_2320" n="HIAT:ip">,</nts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_2323" n="HIAT:w" s="T220">tɨmnɨːga</ts>
                  <nts id="Seg_2324" n="HIAT:ip">,</nts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_2327" n="HIAT:w" s="T221">tɨ͡alga</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_2330" n="HIAT:w" s="T222">taksan</ts>
                  <nts id="Seg_2331" n="HIAT:ip">,</nts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_2334" n="HIAT:w" s="T223">tabanɨ</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_2337" n="HIAT:w" s="T224">ketiː</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_2340" n="HIAT:w" s="T225">barɨ͡am</ts>
                  <nts id="Seg_2341" n="HIAT:ip">"</nts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_2344" n="HIAT:w" s="T226">di͡en</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_2347" n="HIAT:w" s="T227">du͡o</ts>
                  <nts id="Seg_2348" n="HIAT:ip">?</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_2351" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_2353" n="HIAT:w" s="T228">Tölöːböttör</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_2356" n="HIAT:w" s="T229">daːganɨ</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_2359" n="HIAT:w" s="T230">ol</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_2362" n="HIAT:w" s="T231">oččo</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_2365" n="HIAT:w" s="T232">bu͡olla</ts>
                  <nts id="Seg_2366" n="HIAT:ip">.</nts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_2369" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_2371" n="HIAT:w" s="T233">Karčɨnɨ</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_2374" n="HIAT:w" s="T234">körböttör</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_2377" n="HIAT:w" s="T235">tɨ͡ataːgɨlar</ts>
                  <nts id="Seg_2378" n="HIAT:ip">.</nts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_2381" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_2383" n="HIAT:w" s="T236">Hanɨ</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_2386" n="HIAT:w" s="T237">pɨrsɨ͡an</ts>
                  <nts id="Seg_2387" n="HIAT:ip">,</nts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_2390" n="HIAT:w" s="T238">tu͡ok</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_2393" n="HIAT:w" s="T239">egeleller</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2395" n="HIAT:ip">(</nts>
                  <ts e="T241" id="Seg_2397" n="HIAT:w" s="T240">kamʼe-</ts>
                  <nts id="Seg_2398" n="HIAT:ip">)</nts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_2401" n="HIAT:w" s="T241">kamʼersantar</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_2404" n="HIAT:w" s="T242">atɨːlɨːllar</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_2407" n="HIAT:w" s="T243">hürdeːk</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_2410" n="HIAT:w" s="T244">henalaːkka</ts>
                  <nts id="Seg_2411" n="HIAT:ip">.</nts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_2414" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_2416" n="HIAT:w" s="T245">Ontuŋ</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_2419" n="HIAT:w" s="T246">tu͡ok</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_2422" n="HIAT:w" s="T247">kihitej</ts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_2425" n="HIAT:w" s="T248">ɨlɨnan</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_2428" n="HIAT:w" s="T249">ɨlɨ͡aj</ts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_2431" n="HIAT:w" s="T250">duː</ts>
                  <nts id="Seg_2432" n="HIAT:ip">,</nts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_2435" n="HIAT:w" s="T251">eː</ts>
                  <nts id="Seg_2436" n="HIAT:ip">,</nts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_2439" n="HIAT:w" s="T252">ahɨ</ts>
                  <nts id="Seg_2440" n="HIAT:ip">,</nts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_2443" n="HIAT:w" s="T253">ahɨ</ts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_2446" n="HIAT:w" s="T254">ɨlɨnɨ͡aj</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_2449" n="HIAT:w" s="T255">duː</ts>
                  <nts id="Seg_2450" n="HIAT:ip">?</nts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_2453" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_2455" n="HIAT:w" s="T256">Ahɨ</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_2458" n="HIAT:w" s="T257">ɨlɨnɨ͡aŋ</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_2461" n="HIAT:w" s="T258">duː</ts>
                  <nts id="Seg_2462" n="HIAT:ip">,</nts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_2465" n="HIAT:w" s="T259">pɨrsɨ͡an</ts>
                  <nts id="Seg_2466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_2468" n="HIAT:w" s="T260">ɨlɨnɨ͡aŋ</ts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_2471" n="HIAT:w" s="T261">duː</ts>
                  <nts id="Seg_2472" n="HIAT:ip">,</nts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_2475" n="HIAT:w" s="T262">taŋas</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_2478" n="HIAT:w" s="T263">ɨlɨnɨ͡aŋ</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_2481" n="HIAT:w" s="T264">duː</ts>
                  <nts id="Seg_2482" n="HIAT:ip">,</nts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_2485" n="HIAT:w" s="T265">ogoloruŋ</ts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_2488" n="HIAT:w" s="T266">össü͡ö</ts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_2491" n="HIAT:w" s="T267">ü͡öreni͡ekterin</ts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_2494" n="HIAT:w" s="T268">naːda</ts>
                  <nts id="Seg_2495" n="HIAT:ip">.</nts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_2498" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_2500" n="HIAT:w" s="T269">Tu͡ok</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_2503" n="HIAT:w" s="T270">daːganɨ</ts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_2506" n="HIAT:w" s="T271">anɨ</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_2509" n="HIAT:w" s="T272">bu͡ola</ts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_2512" n="HIAT:w" s="T273">urut</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_2515" n="HIAT:w" s="T274">studentarɨ</ts>
                  <nts id="Seg_2516" n="HIAT:ip">,</nts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_2519" n="HIAT:w" s="T275">tu͡oktarɨ</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_2522" n="HIAT:w" s="T276">haːtar</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_2525" n="HIAT:w" s="T277">bi͡ereːčči</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_2528" n="HIAT:w" s="T278">etilere</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_2531" n="HIAT:w" s="T279">ginilerge</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_2534" n="HIAT:w" s="T280">tu͡ok</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_2537" n="HIAT:w" s="T281">ere</ts>
                  <nts id="Seg_2538" n="HIAT:ip">,</nts>
                  <nts id="Seg_2539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_2541" n="HIAT:w" s="T282">datacɨja</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_2544" n="HIAT:w" s="T283">bu͡ol</ts>
                  <nts id="Seg_2545" n="HIAT:ip">,</nts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_2548" n="HIAT:w" s="T284">hapku͡ostan</ts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_2551" n="HIAT:w" s="T285">napravlʼajdaːččɨ</ts>
                  <nts id="Seg_2552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_2554" n="HIAT:w" s="T286">etilere</ts>
                  <nts id="Seg_2555" n="HIAT:ip">.</nts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_2558" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_2560" n="HIAT:w" s="T287">Anɨ</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_2563" n="HIAT:w" s="T288">ol</ts>
                  <nts id="Seg_2564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_2566" n="HIAT:w" s="T289">onnuk</ts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_2569" n="HIAT:w" s="T290">tu͡ok</ts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_2572" n="HIAT:w" s="T291">da</ts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_2575" n="HIAT:w" s="T292">hu͡ok</ts>
                  <nts id="Seg_2576" n="HIAT:ip">.</nts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_2579" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_2581" n="HIAT:w" s="T293">Barɨta</ts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_2584" n="HIAT:w" s="T294">kihi</ts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_2587" n="HIAT:w" s="T295">bejetin</ts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_2590" n="HIAT:w" s="T296">karčɨtɨnan</ts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_2593" n="HIAT:w" s="T297">hɨldʼar</ts>
                  <nts id="Seg_2594" n="HIAT:ip">.</nts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_2597" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_2599" n="HIAT:w" s="T298">Kim</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_2602" n="HIAT:w" s="T299">kɨ͡aga</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_2605" n="HIAT:w" s="T300">hu͡ok</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_2608" n="HIAT:w" s="T301">kihi</ts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_2611" n="HIAT:w" s="T302">hapsi͡em</ts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_2614" n="HIAT:w" s="T303">ölör</ts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_2617" n="HIAT:w" s="T304">di͡ebikke</ts>
                  <nts id="Seg_2618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_2620" n="HIAT:w" s="T305">di͡eri</ts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_2623" n="HIAT:w" s="T306">hiti</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_2626" n="HIAT:w" s="T307">hanaːn</ts>
                  <nts id="Seg_2627" n="HIAT:ip">.</nts>
                  <nts id="Seg_2628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_2630" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_2632" n="HIAT:w" s="T308">A</ts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_2635" n="HIAT:w" s="T309">kim</ts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_2638" n="HIAT:w" s="T310">kɨ͡aktaːgɨŋ</ts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_2641" n="HIAT:w" s="T311">hi͡ese</ts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_2644" n="HIAT:w" s="T312">ü͡öreteller</ts>
                  <nts id="Seg_2645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_2647" n="HIAT:w" s="T313">ogolorun</ts>
                  <nts id="Seg_2648" n="HIAT:ip">,</nts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_2651" n="HIAT:w" s="T314">hin</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_2654" n="HIAT:w" s="T315">biːr</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_2657" n="HIAT:w" s="T316">na</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_2660" n="HIAT:w" s="T317">granʼi</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_2663" n="HIAT:w" s="T318">nʼišʼetɨ</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_2666" n="HIAT:w" s="T319">olorollor</ts>
                  <nts id="Seg_2667" n="HIAT:ip">.</nts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T350" id="Seg_2669" n="sc" s="T335">
               <ts e="T343" id="Seg_2671" n="HIAT:u" s="T335">
                  <nts id="Seg_2672" n="HIAT:ip">–</nts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_2675" n="HIAT:w" s="T335">Naj</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_2678" n="HIAT:w" s="T336">tojonnor</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_2681" n="HIAT:w" s="T337">ere</ts>
                  <nts id="Seg_2682" n="HIAT:ip">,</nts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_2685" n="HIAT:w" s="T338">tojonnor</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_2688" n="HIAT:w" s="T339">ere</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_2691" n="HIAT:w" s="T340">kömölöhü͡öktere</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_2694" n="HIAT:w" s="T341">bihigi</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_2697" n="HIAT:w" s="T342">hirbitiger</ts>
                  <nts id="Seg_2698" n="HIAT:ip">.</nts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_2701" n="HIAT:u" s="T343">
                  <ts e="T344" id="Seg_2703" n="HIAT:w" s="T343">Olopput</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_2706" n="HIAT:w" s="T344">tuksara</ts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_2709" n="HIAT:w" s="T345">barɨta</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_2712" n="HIAT:w" s="T346">ginilerten</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_2715" n="HIAT:w" s="T347">ere</ts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_2718" n="HIAT:w" s="T348">taksɨ͡aga</ts>
                  <nts id="Seg_2719" n="HIAT:ip">.</nts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T449" id="Seg_2721" n="sc" s="T360">
               <ts e="T381" id="Seg_2723" n="HIAT:u" s="T360">
                  <nts id="Seg_2724" n="HIAT:ip">–</nts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2726" n="HIAT:ip">(</nts>
                  <ts e="T362" id="Seg_2728" n="HIAT:w" s="T360">Beje-</ts>
                  <nts id="Seg_2729" n="HIAT:ip">)</nts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_2732" n="HIAT:w" s="T362">bejebit</ts>
                  <nts id="Seg_2733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_2735" n="HIAT:w" s="T365">anɨ</ts>
                  <nts id="Seg_2736" n="HIAT:ip">,</nts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_2739" n="HIAT:w" s="T368">kihibit</ts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_2742" n="HIAT:w" s="T369">anɨ</ts>
                  <nts id="Seg_2743" n="HIAT:ip">,</nts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_2746" n="HIAT:w" s="T370">kihilerbit</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_2749" n="HIAT:w" s="T371">bu͡olla</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372.tx-UkOA.1" id="Seg_2752" n="HIAT:w" s="T372">v</ts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_2755" n="HIAT:w" s="T372.tx-UkOA.1">asnavnom</ts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_2758" n="HIAT:w" s="T373">elbek</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_2761" n="HIAT:w" s="T374">kihi</ts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_2764" n="HIAT:w" s="T375">eː</ts>
                  <nts id="Seg_2765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_2767" n="HIAT:w" s="T376">kiːrbite</ts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_2770" n="HIAT:w" s="T377">tɨ͡attan</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_2773" n="HIAT:w" s="T378">kɨ͡aga</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_2776" n="HIAT:w" s="T379">hu͡ok</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_2779" n="HIAT:w" s="T380">bu͡olannar</ts>
                  <nts id="Seg_2780" n="HIAT:ip">.</nts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_2783" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_2785" n="HIAT:w" s="T381">Barɨlara</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_2788" n="HIAT:w" s="T382">ulakan</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_2791" n="HIAT:w" s="T383">opɨttaːktar</ts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_2794" n="HIAT:w" s="T384">tabaga</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_2797" n="HIAT:w" s="T385">hɨldʼɨːtɨgar</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_2800" n="HIAT:w" s="T386">daːganɨ</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_2803" n="HIAT:w" s="T387">eː</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_2806" n="HIAT:w" s="T388">ol</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_2809" n="HIAT:w" s="T389">barɨta</ts>
                  <nts id="Seg_2810" n="HIAT:ip">,</nts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_2813" n="HIAT:w" s="T390">ol</ts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_2816" n="HIAT:w" s="T391">barɨta</ts>
                  <nts id="Seg_2817" n="HIAT:ip">,</nts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_2820" n="HIAT:w" s="T392">ol</ts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_2823" n="HIAT:w" s="T393">barɨta</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_2826" n="HIAT:w" s="T394">ölör</ts>
                  <nts id="Seg_2827" n="HIAT:ip">.</nts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_2830" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_2832" n="HIAT:w" s="T395">Kihi</ts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_2835" n="HIAT:w" s="T396">barɨta</ts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_2838" n="HIAT:w" s="T397">eː</ts>
                  <nts id="Seg_2839" n="HIAT:ip">,</nts>
                  <nts id="Seg_2840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_2842" n="HIAT:w" s="T398">nu</ts>
                  <nts id="Seg_2843" n="HIAT:ip">,</nts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_2846" n="HIAT:w" s="T399">hirge</ts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_2849" n="HIAT:w" s="T400">kaːlar</ts>
                  <nts id="Seg_2850" n="HIAT:ip">,</nts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_2853" n="HIAT:w" s="T401">skažem</ts>
                  <nts id="Seg_2854" n="HIAT:ip">,</nts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_2857" n="HIAT:w" s="T402">hirge</ts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_2860" n="HIAT:w" s="T403">kaːlar</ts>
                  <nts id="Seg_2861" n="HIAT:ip">.</nts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_2864" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_2866" n="HIAT:w" s="T404">Kihi</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_2869" n="HIAT:w" s="T405">barɨta</ts>
                  <nts id="Seg_2870" n="HIAT:ip">,</nts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_2873" n="HIAT:w" s="T406">anɨ</ts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_2876" n="HIAT:w" s="T407">hɨldʼar</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_2879" n="HIAT:w" s="T408">da</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_2882" n="HIAT:w" s="T409">kihiŋ</ts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_2885" n="HIAT:w" s="T410">olus</ts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_2888" n="HIAT:w" s="T411">hiri</ts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_2891" n="HIAT:w" s="T412">bilbetter</ts>
                  <nts id="Seg_2892" n="HIAT:ip">,</nts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_2895" n="HIAT:w" s="T413">ogo</ts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_2898" n="HIAT:w" s="T414">kihileriŋ</ts>
                  <nts id="Seg_2899" n="HIAT:ip">,</nts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_2902" n="HIAT:w" s="T415">anɨ</ts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_2905" n="HIAT:w" s="T416">hɨldʼannar</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_2908" n="HIAT:w" s="T417">da</ts>
                  <nts id="Seg_2909" n="HIAT:ip">,</nts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_2912" n="HIAT:w" s="T418">taksannar</ts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_2915" n="HIAT:w" s="T419">da</ts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_2918" n="HIAT:w" s="T420">öskötün</ts>
                  <nts id="Seg_2919" n="HIAT:ip">.</nts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_2922" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_2924" n="HIAT:w" s="T421">Ahɨlɨktaːk</ts>
                  <nts id="Seg_2925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_2927" n="HIAT:w" s="T422">hirder</ts>
                  <nts id="Seg_2928" n="HIAT:ip">,</nts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_2931" n="HIAT:w" s="T423">anɨ</ts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_2934" n="HIAT:w" s="T424">kɨːlbɨt</ts>
                  <nts id="Seg_2935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_2937" n="HIAT:w" s="T425">anɨ</ts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_2940" n="HIAT:w" s="T426">hirin</ts>
                  <nts id="Seg_2941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_2943" n="HIAT:w" s="T427">ularɨtan</ts>
                  <nts id="Seg_2944" n="HIAT:ip">,</nts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_2947" n="HIAT:w" s="T428">bihiginen</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_2950" n="HIAT:w" s="T429">elbegi</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_2953" n="HIAT:w" s="T430">tüher</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_2956" n="HIAT:w" s="T431">bu͡olan</ts>
                  <nts id="Seg_2957" n="HIAT:ip">,</nts>
                  <nts id="Seg_2958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_2960" n="HIAT:w" s="T432">ahɨlɨkpɨtɨn</ts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_2963" n="HIAT:w" s="T433">baratar</ts>
                  <nts id="Seg_2964" n="HIAT:ip">.</nts>
                  <nts id="Seg_2965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_2967" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_2969" n="HIAT:w" s="T434">Ol</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_2972" n="HIAT:w" s="T435">ihin</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_2975" n="HIAT:w" s="T436">hɨldʼar</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_2978" n="HIAT:w" s="T437">hirderbit</ts>
                  <nts id="Seg_2979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_2981" n="HIAT:w" s="T438">kɨ͡araːbɨttar</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2983" n="HIAT:ip">–</nts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_2986" n="HIAT:w" s="T439">tabaga</ts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_2989" n="HIAT:w" s="T440">daːganɨ</ts>
                  <nts id="Seg_2990" n="HIAT:ip">,</nts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_2993" n="HIAT:w" s="T441">kak</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_2996" n="HIAT:w" s="T442">tabaga</ts>
                  <nts id="Seg_2997" n="HIAT:ip">,</nts>
                  <nts id="Seg_2998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_3000" n="HIAT:w" s="T443">tak</ts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_3003" n="HIAT:w" s="T444">kihige</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_3006" n="HIAT:w" s="T445">daːganɨ</ts>
                  <nts id="Seg_3007" n="HIAT:ip">.</nts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_3010" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_3012" n="HIAT:w" s="T446">Kihilerbit</ts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_3015" n="HIAT:w" s="T447">eː</ts>
                  <nts id="Seg_3016" n="HIAT:ip">…</nts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T456" id="Seg_3018" n="sc" s="T451">
               <ts e="T456" id="Seg_3020" n="HIAT:u" s="T451">
                  <nts id="Seg_3021" n="HIAT:ip">–</nts>
                  <nts id="Seg_3022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_3024" n="HIAT:w" s="T451">Atɨn</ts>
                  <nts id="Seg_3025" n="HIAT:ip">,</nts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_3028" n="HIAT:w" s="T453">atɨn</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_3031" n="HIAT:w" s="T454">ologu</ts>
                  <nts id="Seg_3032" n="HIAT:ip">…</nts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T630" id="Seg_3034" n="sc" s="T458">
               <ts e="T468" id="Seg_3036" n="HIAT:u" s="T458">
                  <nts id="Seg_3037" n="HIAT:ip">–</nts>
                  <nts id="Seg_3038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_3040" n="HIAT:w" s="T458">Atɨn</ts>
                  <nts id="Seg_3041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_3043" n="HIAT:w" s="T460">ologu</ts>
                  <nts id="Seg_3044" n="HIAT:ip">,</nts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_3047" n="HIAT:w" s="T461">hepsekiː</ts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_3050" n="HIAT:w" s="T462">ologu</ts>
                  <nts id="Seg_3051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_3053" n="HIAT:w" s="T463">batannar</ts>
                  <nts id="Seg_3054" n="HIAT:ip">,</nts>
                  <nts id="Seg_3055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_3057" n="HIAT:w" s="T464">hepsekiː</ts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_3060" n="HIAT:w" s="T465">onton</ts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_3063" n="HIAT:w" s="T466">hepsekiː</ts>
                  <nts id="Seg_3064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_3066" n="HIAT:w" s="T467">bu͡olu͡o</ts>
                  <nts id="Seg_3067" n="HIAT:ip">.</nts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_3070" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_3072" n="HIAT:w" s="T468">Rɨbnajga</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3074" n="HIAT:ip">(</nts>
                  <ts e="T470" id="Seg_3076" n="HIAT:w" s="T469">olor-</ts>
                  <nts id="Seg_3077" n="HIAT:ip">)</nts>
                  <nts id="Seg_3078" n="HIAT:ip">,</nts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_3081" n="HIAT:w" s="T470">eː</ts>
                  <nts id="Seg_3082" n="HIAT:ip">,</nts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_3085" n="HIAT:w" s="T471">patom</ts>
                  <nts id="Seg_3086" n="HIAT:ip">,</nts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_3089" n="HIAT:w" s="T472">v</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_3092" n="HIAT:w" s="T473">čʼastnastʼi</ts>
                  <nts id="Seg_3093" n="HIAT:ip">,</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_3096" n="HIAT:w" s="T474">Rɨbnaj</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_3099" n="HIAT:w" s="T475">tuhunan</ts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_3102" n="HIAT:w" s="T476">kepsiːbin</ts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_3105" n="HIAT:w" s="T477">onton</ts>
                  <nts id="Seg_3106" n="HIAT:ip">.</nts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T495" id="Seg_3109" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_3111" n="HIAT:w" s="T478">Rɨbnajga</ts>
                  <nts id="Seg_3112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_3114" n="HIAT:w" s="T479">hi͡ese</ts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_3117" n="HIAT:w" s="T480">bu͡o</ts>
                  <nts id="Seg_3118" n="HIAT:ip">,</nts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_3121" n="HIAT:w" s="T481">hibi͡etteːkter</ts>
                  <nts id="Seg_3122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_3124" n="HIAT:w" s="T482">daːganɨ</ts>
                  <nts id="Seg_3125" n="HIAT:ip">,</nts>
                  <nts id="Seg_3126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_3128" n="HIAT:w" s="T483">ičiges</ts>
                  <nts id="Seg_3129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_3131" n="HIAT:w" s="T484">daːganɨ</ts>
                  <nts id="Seg_3132" n="HIAT:ip">,</nts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_3135" n="HIAT:w" s="T485">ol</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_3138" n="HIAT:w" s="T486">ihin</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3140" n="HIAT:ip">(</nts>
                  <ts e="T488" id="Seg_3142" n="HIAT:w" s="T487">tɨ͡a-</ts>
                  <nts id="Seg_3143" n="HIAT:ip">)</nts>
                  <nts id="Seg_3144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_3146" n="HIAT:w" s="T488">tɨ͡ataːgɨ</ts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_3149" n="HIAT:w" s="T489">kihiler</ts>
                  <nts id="Seg_3150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_3152" n="HIAT:w" s="T490">tɨ͡aga</ts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_3155" n="HIAT:w" s="T491">hɨldʼɨ͡aktarɨn</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_3158" n="HIAT:w" s="T492">olus</ts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_3161" n="HIAT:w" s="T493">da</ts>
                  <nts id="Seg_3162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_3164" n="HIAT:w" s="T494">tartarbattar</ts>
                  <nts id="Seg_3165" n="HIAT:ip">.</nts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_3168" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_3170" n="HIAT:w" s="T495">Karčɨ</ts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_3173" n="HIAT:w" s="T496">olus</ts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_3176" n="HIAT:w" s="T497">eː</ts>
                  <nts id="Seg_3177" n="HIAT:ip">,</nts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_3180" n="HIAT:w" s="T498">tabattan</ts>
                  <nts id="Seg_3181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_3183" n="HIAT:w" s="T499">tugu</ts>
                  <nts id="Seg_3184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_3186" n="HIAT:w" s="T500">ɨlɨ͡aŋɨj</ts>
                  <nts id="Seg_3187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_3189" n="HIAT:w" s="T501">di͡e</ts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_3192" n="HIAT:w" s="T502">anɨ</ts>
                  <nts id="Seg_3193" n="HIAT:ip">,</nts>
                  <nts id="Seg_3194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_3196" n="HIAT:w" s="T503">ölör</ts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_3199" n="HIAT:w" s="T504">daː</ts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_3202" n="HIAT:w" s="T505">ol</ts>
                  <nts id="Seg_3203" n="HIAT:ip">,</nts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_3206" n="HIAT:w" s="T506">ka</ts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_3209" n="HIAT:w" s="T507">ahɨjak</ts>
                  <nts id="Seg_3210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_3212" n="HIAT:w" s="T508">bi͡ehu͡onča</ts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_3215" n="HIAT:w" s="T509">tabanɨ</ts>
                  <nts id="Seg_3216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_3218" n="HIAT:w" s="T510">daːganɨ</ts>
                  <nts id="Seg_3219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_3221" n="HIAT:w" s="T511">ölördökküne</ts>
                  <nts id="Seg_3222" n="HIAT:ip">,</nts>
                  <nts id="Seg_3223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_3225" n="HIAT:w" s="T512">olus</ts>
                  <nts id="Seg_3226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_3228" n="HIAT:w" s="T513">da</ts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_3231" n="HIAT:w" s="T514">elbek</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_3234" n="HIAT:w" s="T515">karčɨnɨ</ts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_3237" n="HIAT:w" s="T516">ölörü͡öŋ</ts>
                  <nts id="Seg_3238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_3240" n="HIAT:w" s="T517">hu͡oga</ts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_3243" n="HIAT:w" s="T518">ol</ts>
                  <nts id="Seg_3244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_3246" n="HIAT:w" s="T519">tuhugar</ts>
                  <nts id="Seg_3247" n="HIAT:ip">,</nts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_3250" n="HIAT:w" s="T520">ili</ts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_3253" n="HIAT:w" s="T521">tɨ͡aga</ts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_3256" n="HIAT:w" s="T522">hɨldʼaːŋŋɨn</ts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_3259" n="HIAT:w" s="T523">duː</ts>
                  <nts id="Seg_3260" n="HIAT:ip">.</nts>
                  <nts id="Seg_3261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_3263" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_3265" n="HIAT:w" s="T524">Ol</ts>
                  <nts id="Seg_3266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_3268" n="HIAT:w" s="T525">ihin</ts>
                  <nts id="Seg_3269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_3271" n="HIAT:w" s="T526">eteŋŋe</ts>
                  <nts id="Seg_3272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_3274" n="HIAT:w" s="T527">oloru͡okka</ts>
                  <nts id="Seg_3275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_3277" n="HIAT:w" s="T528">i</ts>
                  <nts id="Seg_3278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_3280" n="HIAT:w" s="T529">tɨ͡aga</ts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_3283" n="HIAT:w" s="T530">kihiler</ts>
                  <nts id="Seg_3284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_3286" n="HIAT:w" s="T531">dʼolloːktuk</ts>
                  <nts id="Seg_3287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_3289" n="HIAT:w" s="T532">oloru͡oktarɨn</ts>
                  <nts id="Seg_3290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_3292" n="HIAT:w" s="T533">iti</ts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_3295" n="HIAT:w" s="T534">maŋnajgɨ</ts>
                  <nts id="Seg_3296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_3298" n="HIAT:w" s="T535">ol</ts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_3301" n="HIAT:w" s="T536">tuhugar</ts>
                  <nts id="Seg_3302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_3304" n="HIAT:w" s="T537">pravʼitʼelʼstva</ts>
                  <nts id="Seg_3305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_3307" n="HIAT:w" s="T538">dumajdɨ͡agɨn</ts>
                  <nts id="Seg_3308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_3310" n="HIAT:w" s="T539">naːda</ts>
                  <nts id="Seg_3311" n="HIAT:ip">.</nts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_3314" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_3316" n="HIAT:w" s="T540">Kihilerge</ts>
                  <nts id="Seg_3317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_3319" n="HIAT:w" s="T541">kömö</ts>
                  <nts id="Seg_3320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_3322" n="HIAT:w" s="T542">iː</ts>
                  <nts id="Seg_3323" n="HIAT:ip">…</nts>
                  <nts id="Seg_3324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_3326" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_3328" n="HIAT:w" s="T543">Kihiler</ts>
                  <nts id="Seg_3329" n="HIAT:ip">,</nts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_3332" n="HIAT:w" s="T544">bejelere</ts>
                  <nts id="Seg_3333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_3335" n="HIAT:w" s="T545">kihiler</ts>
                  <nts id="Seg_3336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_3338" n="HIAT:w" s="T546">tugu</ts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_3341" n="HIAT:w" s="T547">da</ts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_3344" n="HIAT:w" s="T548">gɨnɨ͡aktara</ts>
                  <nts id="Seg_3345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_3347" n="HIAT:w" s="T549">hu͡oga</ts>
                  <nts id="Seg_3348" n="HIAT:ip">,</nts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_3351" n="HIAT:w" s="T550">tɨ͡ataːgɨ</ts>
                  <nts id="Seg_3352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_3354" n="HIAT:w" s="T551">kihiler</ts>
                  <nts id="Seg_3355" n="HIAT:ip">,</nts>
                  <nts id="Seg_3356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552.tx-UkOA.1" id="Seg_3358" n="HIAT:w" s="T552">tʼem</ts>
                  <nts id="Seg_3359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_3361" n="HIAT:w" s="T552.tx-UkOA.1">bolʼeje</ts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_3364" n="HIAT:w" s="T553">bejelerin</ts>
                  <nts id="Seg_3365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_3367" n="HIAT:w" s="T554">innilerin</ts>
                  <nts id="Seg_3368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_3370" n="HIAT:w" s="T555">tuhunan</ts>
                  <nts id="Seg_3371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_3373" n="HIAT:w" s="T556">tugu</ts>
                  <nts id="Seg_3374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_3376" n="HIAT:w" s="T557">da</ts>
                  <nts id="Seg_3377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_3379" n="HIAT:w" s="T558">haŋarɨmattar</ts>
                  <nts id="Seg_3380" n="HIAT:ip">.</nts>
                  <nts id="Seg_3381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_3383" n="HIAT:u" s="T559">
                  <ts e="T560" id="Seg_3385" n="HIAT:w" s="T559">Hapku͡ostarbɨt</ts>
                  <nts id="Seg_3386" n="HIAT:ip">,</nts>
                  <nts id="Seg_3387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_3389" n="HIAT:w" s="T560">dʼirʼektarbɨt</ts>
                  <nts id="Seg_3390" n="HIAT:ip">,</nts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_3393" n="HIAT:w" s="T561">dʼirʼektardar</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_3396" n="HIAT:w" s="T562">da</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_3399" n="HIAT:w" s="T563">ularɨjallar</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_3402" n="HIAT:w" s="T564">daːganɨ</ts>
                  <nts id="Seg_3403" n="HIAT:ip">.</nts>
                  <nts id="Seg_3404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T581" id="Seg_3406" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_3408" n="HIAT:w" s="T565">Anɨ</ts>
                  <nts id="Seg_3409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_3411" n="HIAT:w" s="T566">össü͡ö</ts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_3414" n="HIAT:w" s="T567">hi͡ese</ts>
                  <nts id="Seg_3415" n="HIAT:ip">,</nts>
                  <nts id="Seg_3416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_3418" n="HIAT:w" s="T568">bihi͡eke</ts>
                  <nts id="Seg_3419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_3421" n="HIAT:w" s="T569">Rɨbnajga</ts>
                  <nts id="Seg_3422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_3424" n="HIAT:w" s="T570">össü͡ö</ts>
                  <nts id="Seg_3425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_3427" n="HIAT:w" s="T571">tura</ts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_3430" n="HIAT:w" s="T572">tüste</ts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_3433" n="HIAT:w" s="T573">eːt</ts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_3436" n="HIAT:w" s="T574">biːr</ts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_3439" n="HIAT:w" s="T575">dʼirʼektar</ts>
                  <nts id="Seg_3440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3441" n="HIAT:ip">–</nts>
                  <nts id="Seg_3442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_3444" n="HIAT:w" s="T576">Mamonav</ts>
                  <nts id="Seg_3445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_3447" n="HIAT:w" s="T577">Vladʼimʼir</ts>
                  <nts id="Seg_3448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_3450" n="HIAT:w" s="T578">Alʼeksandravʼičʼ</ts>
                  <nts id="Seg_3451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_3453" n="HIAT:w" s="T579">di͡en</ts>
                  <nts id="Seg_3454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_3456" n="HIAT:w" s="T580">kihi</ts>
                  <nts id="Seg_3457" n="HIAT:ip">.</nts>
                  <nts id="Seg_3458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_3460" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_3462" n="HIAT:w" s="T581">Ol</ts>
                  <nts id="Seg_3463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_3465" n="HIAT:w" s="T582">ol</ts>
                  <nts id="Seg_3466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_3468" n="HIAT:w" s="T583">tuhunan</ts>
                  <nts id="Seg_3469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_3471" n="HIAT:w" s="T584">bagas</ts>
                  <nts id="Seg_3472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_3474" n="HIAT:w" s="T585">tu͡ok</ts>
                  <nts id="Seg_3475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_3477" n="HIAT:w" s="T586">da</ts>
                  <nts id="Seg_3478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_3480" n="HIAT:w" s="T587">kuhaganɨ</ts>
                  <nts id="Seg_3481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_3483" n="HIAT:w" s="T588">haŋarbappɨn</ts>
                  <nts id="Seg_3484" n="HIAT:ip">,</nts>
                  <nts id="Seg_3485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_3487" n="HIAT:w" s="T589">berteːk</ts>
                  <nts id="Seg_3488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_3490" n="HIAT:w" s="T590">kihi</ts>
                  <nts id="Seg_3491" n="HIAT:ip">,</nts>
                  <nts id="Seg_3492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_3494" n="HIAT:w" s="T591">eː</ts>
                  <nts id="Seg_3495" n="HIAT:ip">.</nts>
                  <nts id="Seg_3496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T610" id="Seg_3498" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_3500" n="HIAT:w" s="T592">Kihileri</ts>
                  <nts id="Seg_3501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_3503" n="HIAT:w" s="T593">gɨtta</ts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_3506" n="HIAT:w" s="T594">anɨ</ts>
                  <nts id="Seg_3507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_3509" n="HIAT:w" s="T595">hi͡ese</ts>
                  <nts id="Seg_3510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_3512" n="HIAT:w" s="T596">karčɨlaːk</ts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_3515" n="HIAT:w" s="T597">bu͡olbuttar</ts>
                  <nts id="Seg_3516" n="HIAT:ip">,</nts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_3519" n="HIAT:w" s="T598">iti</ts>
                  <nts id="Seg_3520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_3522" n="HIAT:w" s="T599">kim</ts>
                  <nts id="Seg_3523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_3525" n="HIAT:w" s="T600">ete</ts>
                  <nts id="Seg_3526" n="HIAT:ip">,</nts>
                  <nts id="Seg_3527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_3529" n="HIAT:w" s="T601">nolʼ</ts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_3532" n="HIAT:w" s="T602">sʼemʼ</ts>
                  <nts id="Seg_3533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_3535" n="HIAT:w" s="T603">bi͡ereːčči</ts>
                  <nts id="Seg_3536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_3538" n="HIAT:w" s="T604">etilere</ts>
                  <nts id="Seg_3539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_3541" n="HIAT:w" s="T605">pradukcɨja</ts>
                  <nts id="Seg_3542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_3544" n="HIAT:w" s="T606">gi͡enin</ts>
                  <nts id="Seg_3545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_3547" n="HIAT:w" s="T607">ke</ts>
                  <nts id="Seg_3548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_3550" n="HIAT:w" s="T608">bɨjɨl</ts>
                  <nts id="Seg_3551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_3553" n="HIAT:w" s="T609">oŋorbuttara</ts>
                  <nts id="Seg_3554" n="HIAT:ip">.</nts>
                  <nts id="Seg_3555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_3557" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_3559" n="HIAT:w" s="T610">Ontularɨn</ts>
                  <nts id="Seg_3560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_3562" n="HIAT:w" s="T611">da</ts>
                  <nts id="Seg_3563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_3565" n="HIAT:w" s="T612">ubirajdɨ͡appɨt</ts>
                  <nts id="Seg_3566" n="HIAT:ip">.</nts>
                  <nts id="Seg_3567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T630" id="Seg_3569" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_3571" n="HIAT:w" s="T613">Onu</ts>
                  <nts id="Seg_3572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_3574" n="HIAT:w" s="T614">ubiraːjdaːtaktarɨnan</ts>
                  <nts id="Seg_3575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_3577" n="HIAT:w" s="T615">anɨ</ts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_3580" n="HIAT:w" s="T616">tɨ͡ataːgɨ</ts>
                  <nts id="Seg_3581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_3583" n="HIAT:w" s="T617">eː</ts>
                  <nts id="Seg_3584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_3586" n="HIAT:w" s="T618">bulčuttar</ts>
                  <nts id="Seg_3587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_3589" n="HIAT:w" s="T619">da</ts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_3592" n="HIAT:w" s="T620">emi͡e</ts>
                  <nts id="Seg_3593" n="HIAT:ip">,</nts>
                  <nts id="Seg_3594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3595" n="HIAT:ip">(</nts>
                  <ts e="T622" id="Seg_3597" n="HIAT:w" s="T621">ɨtɨ͡a-</ts>
                  <nts id="Seg_3598" n="HIAT:ip">)</nts>
                  <nts id="Seg_3599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3600" n="HIAT:ip">(</nts>
                  <ts e="T623" id="Seg_3602" n="HIAT:w" s="T622">ɨtɨ͡a-</ts>
                  <nts id="Seg_3603" n="HIAT:ip">)</nts>
                  <nts id="Seg_3604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_3606" n="HIAT:w" s="T623">ɨtɨ͡alɨːhɨt</ts>
                  <nts id="Seg_3607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_3609" n="HIAT:w" s="T624">kihiler</ts>
                  <nts id="Seg_3610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_3612" n="HIAT:w" s="T625">da</ts>
                  <nts id="Seg_3613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_3615" n="HIAT:w" s="T626">agɨjagɨ</ts>
                  <nts id="Seg_3616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_3618" n="HIAT:w" s="T627">ɨlɨ͡aktara</ts>
                  <nts id="Seg_3619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_3621" n="HIAT:w" s="T628">hin</ts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_3624" n="HIAT:w" s="T629">biːr</ts>
                  <nts id="Seg_3625" n="HIAT:ip">.</nts>
                  <nts id="Seg_3626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T637" id="Seg_3627" n="sc" s="T631">
               <ts e="T637" id="Seg_3629" n="HIAT:u" s="T631">
                  <nts id="Seg_3630" n="HIAT:ip">–</nts>
                  <nts id="Seg_3631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3632" n="HIAT:ip">(</nts>
                  <ts e="T633" id="Seg_3634" n="HIAT:w" s="T631">Hatan-</ts>
                  <nts id="Seg_3635" n="HIAT:ip">)</nts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_3638" n="HIAT:w" s="T633">hatanɨ͡aktara</ts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_3641" n="HIAT:w" s="T635">hu͡oktara</ts>
                  <nts id="Seg_3642" n="HIAT:ip">,</nts>
                  <nts id="Seg_3643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_3645" n="HIAT:w" s="T636">heː</ts>
                  <nts id="Seg_3646" n="HIAT:ip">.</nts>
                  <nts id="Seg_3647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T725" id="Seg_3648" n="sc" s="T657">
               <ts e="T663" id="Seg_3650" n="HIAT:u" s="T657">
                  <nts id="Seg_3651" n="HIAT:ip">–</nts>
                  <nts id="Seg_3652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_3654" n="HIAT:w" s="T657">Onon</ts>
                  <nts id="Seg_3655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_3657" n="HIAT:w" s="T659">erenebit</ts>
                  <nts id="Seg_3658" n="HIAT:ip">,</nts>
                  <nts id="Seg_3659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_3661" n="HIAT:w" s="T660">eː</ts>
                  <nts id="Seg_3662" n="HIAT:ip">,</nts>
                  <nts id="Seg_3663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_3665" n="HIAT:w" s="T661">onon</ts>
                  <nts id="Seg_3666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_3668" n="HIAT:w" s="T662">erenebit</ts>
                  <nts id="Seg_3669" n="HIAT:ip">.</nts>
                  <nts id="Seg_3670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_3672" n="HIAT:u" s="T663">
                  <ts e="T664" id="Seg_3674" n="HIAT:w" s="T663">A</ts>
                  <nts id="Seg_3675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_3677" n="HIAT:w" s="T664">tak</ts>
                  <nts id="Seg_3678" n="HIAT:ip">,</nts>
                  <nts id="Seg_3679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_3681" n="HIAT:w" s="T665">iti</ts>
                  <nts id="Seg_3682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_3684" n="HIAT:w" s="T666">karaŋa</ts>
                  <nts id="Seg_3685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_3687" n="HIAT:w" s="T667">di͡ek</ts>
                  <nts id="Seg_3688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_3690" n="HIAT:w" s="T668">öttütün</ts>
                  <nts id="Seg_3691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_3693" n="HIAT:w" s="T669">kepsettibit</ts>
                  <nts id="Seg_3694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_3696" n="HIAT:w" s="T670">bihigi</ts>
                  <nts id="Seg_3697" n="HIAT:ip">.</nts>
                  <nts id="Seg_3698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T689" id="Seg_3700" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_3702" n="HIAT:w" s="T671">A</ts>
                  <nts id="Seg_3703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_3705" n="HIAT:w" s="T672">tak</ts>
                  <nts id="Seg_3706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_3708" n="HIAT:w" s="T673">da</ts>
                  <nts id="Seg_3709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_3711" n="HIAT:w" s="T674">tɨ͡aga</ts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_3714" n="HIAT:w" s="T675">hɨrɨttakka</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_3717" n="HIAT:w" s="T676">bert</ts>
                  <nts id="Seg_3718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_3720" n="HIAT:w" s="T677">beseleː</ts>
                  <nts id="Seg_3721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_3723" n="HIAT:w" s="T678">ete</ts>
                  <nts id="Seg_3724" n="HIAT:ip">,</nts>
                  <nts id="Seg_3725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_3727" n="HIAT:w" s="T679">eː</ts>
                  <nts id="Seg_3728" n="HIAT:ip">,</nts>
                  <nts id="Seg_3729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_3731" n="HIAT:w" s="T680">tɨ͡ataːgɨ</ts>
                  <nts id="Seg_3732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_3734" n="HIAT:w" s="T681">kihiler</ts>
                  <nts id="Seg_3735" n="HIAT:ip">,</nts>
                  <nts id="Seg_3736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_3738" n="HIAT:w" s="T682">anɨ</ts>
                  <nts id="Seg_3739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_3741" n="HIAT:w" s="T683">ogolor</ts>
                  <nts id="Seg_3742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_3744" n="HIAT:w" s="T684">hɨldʼɨbat</ts>
                  <nts id="Seg_3745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_3747" n="HIAT:w" s="T685">bu͡olannar</ts>
                  <nts id="Seg_3748" n="HIAT:ip">,</nts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_3751" n="HIAT:w" s="T686">anɨ</ts>
                  <nts id="Seg_3752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_3754" n="HIAT:w" s="T687">bilbetter</ts>
                  <nts id="Seg_3755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_3757" n="HIAT:w" s="T688">bu͡o</ts>
                  <nts id="Seg_3758" n="HIAT:ip">.</nts>
                  <nts id="Seg_3759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_3761" n="HIAT:u" s="T689">
                  <ts e="T690" id="Seg_3763" n="HIAT:w" s="T689">Haka</ts>
                  <nts id="Seg_3764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_3766" n="HIAT:w" s="T690">tɨlɨn</ts>
                  <nts id="Seg_3767" n="HIAT:ip">,</nts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_3770" n="HIAT:w" s="T691">čeber</ts>
                  <nts id="Seg_3771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_3773" n="HIAT:w" s="T692">tɨːnnara</ts>
                  <nts id="Seg_3774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_3776" n="HIAT:w" s="T693">eː</ts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_3779" n="HIAT:w" s="T694">halgɨnɨ</ts>
                  <nts id="Seg_3780" n="HIAT:ip">,</nts>
                  <nts id="Seg_3781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_3783" n="HIAT:w" s="T695">ot</ts>
                  <nts id="Seg_3784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_3786" n="HIAT:w" s="T696">hɨtɨn</ts>
                  <nts id="Seg_3787" n="HIAT:ip">,</nts>
                  <nts id="Seg_3788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_3790" n="HIAT:w" s="T697">di͡ebikke</ts>
                  <nts id="Seg_3791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_3793" n="HIAT:w" s="T698">di͡eri</ts>
                  <nts id="Seg_3794" n="HIAT:ip">,</nts>
                  <nts id="Seg_3795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_3797" n="HIAT:w" s="T699">maŋnaj</ts>
                  <nts id="Seg_3798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_3800" n="HIAT:w" s="T700">kaːs</ts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_3803" n="HIAT:w" s="T701">aːjdaːnɨn</ts>
                  <nts id="Seg_3804" n="HIAT:ip">,</nts>
                  <nts id="Seg_3805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_3807" n="HIAT:w" s="T702">tuguttar</ts>
                  <nts id="Seg_3808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_3810" n="HIAT:w" s="T703">törüːllerin</ts>
                  <nts id="Seg_3811" n="HIAT:ip">,</nts>
                  <nts id="Seg_3812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_3814" n="HIAT:w" s="T704">barɨtɨn</ts>
                  <nts id="Seg_3815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_3817" n="HIAT:w" s="T705">tugu</ts>
                  <nts id="Seg_3818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_3820" n="HIAT:w" s="T706">da</ts>
                  <nts id="Seg_3821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_3823" n="HIAT:w" s="T707">olus</ts>
                  <nts id="Seg_3824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_3826" n="HIAT:w" s="T708">bilbetter</ts>
                  <nts id="Seg_3827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_3829" n="HIAT:w" s="T709">bu͡o</ts>
                  <nts id="Seg_3830" n="HIAT:ip">.</nts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_3833" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_3835" n="HIAT:w" s="T710">Ontuŋ</ts>
                  <nts id="Seg_3836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_3838" n="HIAT:w" s="T711">barɨta</ts>
                  <nts id="Seg_3839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_3841" n="HIAT:w" s="T712">bihige</ts>
                  <nts id="Seg_3842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_3844" n="HIAT:w" s="T713">eː</ts>
                  <nts id="Seg_3845" n="HIAT:ip">,</nts>
                  <nts id="Seg_3846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_3848" n="HIAT:w" s="T714">tɨ͡aga</ts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_3851" n="HIAT:w" s="T715">ü͡öskeːbit</ts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_3854" n="HIAT:w" s="T716">kihilerge</ts>
                  <nts id="Seg_3855" n="HIAT:ip">,</nts>
                  <nts id="Seg_3856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_3858" n="HIAT:w" s="T717">ontuŋ</ts>
                  <nts id="Seg_3859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_3861" n="HIAT:w" s="T718">barɨta</ts>
                  <nts id="Seg_3862" n="HIAT:ip">,</nts>
                  <nts id="Seg_3863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_3865" n="HIAT:w" s="T719">barɨta</ts>
                  <nts id="Seg_3866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_3868" n="HIAT:w" s="T720">ulakan</ts>
                  <nts id="Seg_3869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_3871" n="HIAT:w" s="T721">kim</ts>
                  <nts id="Seg_3872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_3874" n="HIAT:w" s="T722">bu͡o</ts>
                  <nts id="Seg_3875" n="HIAT:ip">,</nts>
                  <nts id="Seg_3876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_3878" n="HIAT:w" s="T723">vpʼečʼatlʼenʼije</ts>
                  <nts id="Seg_3879" n="HIAT:ip">.</nts>
                  <nts id="Seg_3880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T764" id="Seg_3881" n="sc" s="T763">
               <ts e="T764" id="Seg_3883" n="HIAT:u" s="T763">
                  <nts id="Seg_3884" n="HIAT:ip">–</nts>
                  <nts id="Seg_3885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3887" n="HIAT:w" s="T763">Bahɨːba</ts>
                  <nts id="Seg_3888" n="HIAT:ip">.</nts>
                  <nts id="Seg_3889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-UkOA">
            <ts e="T65" id="Seg_3890" n="sc" s="T7">
               <ts e="T8" id="Seg_3892" n="e" s="T7">– Aː, </ts>
               <ts e="T9" id="Seg_3894" n="e" s="T8">bejem </ts>
               <ts e="T10" id="Seg_3896" n="e" s="T9">Nosku͡oga </ts>
               <ts e="T11" id="Seg_3898" n="e" s="T10">dʼi͡eleːk </ts>
               <ts e="T12" id="Seg_3900" n="e" s="T11">bu͡olammɨn, </ts>
               <ts e="T13" id="Seg_3902" n="e" s="T12">eː, </ts>
               <ts e="T14" id="Seg_3904" n="e" s="T13">tɨ͡aga </ts>
               <ts e="T15" id="Seg_3906" n="e" s="T14">agɨjaktɨk </ts>
               <ts e="T16" id="Seg_3908" n="e" s="T15">bu͡olabɨn </ts>
               <ts e="T17" id="Seg_3910" n="e" s="T16">bu͡o, </ts>
               <ts e="T18" id="Seg_3912" n="e" s="T17">hin </ts>
               <ts e="T19" id="Seg_3914" n="e" s="T18">biːr. </ts>
               <ts e="T20" id="Seg_3916" n="e" s="T19">Haːstarɨ, </ts>
               <ts e="T21" id="Seg_3918" n="e" s="T20">kühünneri </ts>
               <ts e="T22" id="Seg_3920" n="e" s="T21">bu͡ol </ts>
               <ts e="T23" id="Seg_3922" n="e" s="T22">bi͡ek </ts>
               <ts e="T24" id="Seg_3924" n="e" s="T23">tɨ͡aga </ts>
               <ts e="T25" id="Seg_3926" n="e" s="T24">hɨldʼabɨn. </ts>
               <ts e="T26" id="Seg_3928" n="e" s="T25">Kas </ts>
               <ts e="T27" id="Seg_3930" n="e" s="T26">dʼɨl </ts>
               <ts e="T28" id="Seg_3932" n="e" s="T27">aːjɨ </ts>
               <ts e="T29" id="Seg_3934" n="e" s="T28">inʼemeːpper, </ts>
               <ts e="T30" id="Seg_3936" n="e" s="T29">teːtemeːpper </ts>
               <ts e="T31" id="Seg_3938" n="e" s="T30">kömölöhö. </ts>
               <ts e="T32" id="Seg_3940" n="e" s="T31">Ol </ts>
               <ts e="T33" id="Seg_3942" n="e" s="T32">di͡ek, </ts>
               <ts e="T34" id="Seg_3944" n="e" s="T33">tɨ͡aga </ts>
               <ts e="T35" id="Seg_3946" n="e" s="T34">hɨldʼa </ts>
               <ts e="T36" id="Seg_3948" n="e" s="T35">bu͡olannar </ts>
               <ts e="T37" id="Seg_3950" n="e" s="T36">(gi͡e-) </ts>
               <ts e="T38" id="Seg_3952" n="e" s="T37">tabalaːktar </ts>
               <ts e="T39" id="Seg_3954" n="e" s="T38">bu͡o </ts>
               <ts e="T40" id="Seg_3956" n="e" s="T39">ol. </ts>
               <ts e="T41" id="Seg_3958" n="e" s="T40">Tabalar, </ts>
               <ts e="T42" id="Seg_3960" n="e" s="T41">tabalarɨn </ts>
               <ts e="T43" id="Seg_3962" n="e" s="T42">da </ts>
               <ts e="T44" id="Seg_3964" n="e" s="T43">körö </ts>
               <ts e="T45" id="Seg_3966" n="e" s="T44">hotoru </ts>
               <ts e="T46" id="Seg_3968" n="e" s="T45">daːganɨ </ts>
               <ts e="T47" id="Seg_3970" n="e" s="T46">onnugu </ts>
               <ts e="T48" id="Seg_3972" n="e" s="T47">bejem </ts>
               <ts e="T49" id="Seg_3974" n="e" s="T48">daːganɨ </ts>
               <ts e="T50" id="Seg_3976" n="e" s="T49">(zaž-) </ts>
               <ts e="T51" id="Seg_3978" n="e" s="T50">kimniːbin, </ts>
               <ts e="T52" id="Seg_3980" n="e" s="T51">iːtillebin </ts>
               <ts e="T53" id="Seg_3982" n="e" s="T52">eː </ts>
               <ts e="T54" id="Seg_3984" n="e" s="T53">kiminen, </ts>
               <ts e="T55" id="Seg_3986" n="e" s="T54">energʼijanan </ts>
               <ts e="T56" id="Seg_3988" n="e" s="T55">di͡ebikke </ts>
               <ts e="T57" id="Seg_3990" n="e" s="T56">di͡eri. </ts>
               <ts e="T58" id="Seg_3992" n="e" s="T57">Svʼežij </ts>
               <ts e="T59" id="Seg_3994" n="e" s="T58">vozdux. </ts>
               <ts e="T60" id="Seg_3996" n="e" s="T59">Nosku͡oga </ts>
               <ts e="T61" id="Seg_3998" n="e" s="T60">hɨtan </ts>
               <ts e="T62" id="Seg_4000" n="e" s="T61">tɨːnɨn </ts>
               <ts e="T63" id="Seg_4002" n="e" s="T62">(kaːjallar) </ts>
               <ts e="T64" id="Seg_4004" n="e" s="T63">hol </ts>
               <ts e="T65" id="Seg_4006" n="e" s="T64">kenne. </ts>
            </ts>
            <ts e="T119" id="Seg_4007" n="sc" s="T84">
               <ts e="T85" id="Seg_4009" n="e" s="T84">– Tɨ͡ataːgɨlarɨŋ </ts>
               <ts e="T86" id="Seg_4011" n="e" s="T85">bejelere </ts>
               <ts e="T87" id="Seg_4013" n="e" s="T86">bileller </ts>
               <ts e="T88" id="Seg_4015" n="e" s="T87">bu͡o </ts>
               <ts e="T89" id="Seg_4017" n="e" s="T88">kajdak </ts>
               <ts e="T90" id="Seg_4019" n="e" s="T89">oloru͡oktarɨn. </ts>
               <ts e="T91" id="Seg_4021" n="e" s="T90">Anɨ </ts>
               <ts e="T92" id="Seg_4023" n="e" s="T91">üčügej </ts>
               <ts e="T93" id="Seg_4025" n="e" s="T92">ologu </ts>
               <ts e="T94" id="Seg_4027" n="e" s="T93">bilbet </ts>
               <ts e="T95" id="Seg_4029" n="e" s="T94">bu͡olannar, </ts>
               <ts e="T96" id="Seg_4031" n="e" s="T95">giller </ts>
               <ts e="T97" id="Seg_4033" n="e" s="T96">eː </ts>
               <ts e="T98" id="Seg_4035" n="e" s="T97">üčügejdik </ts>
               <ts e="T99" id="Seg_4037" n="e" s="T98">oloru͡opput </ts>
               <ts e="T100" id="Seg_4039" n="e" s="T99">di͡en </ts>
               <ts e="T101" id="Seg_4041" n="e" s="T100">ispetter, </ts>
               <ts e="T102" id="Seg_4043" n="e" s="T101">bɨhɨlak, </ts>
               <ts e="T103" id="Seg_4045" n="e" s="T102">olus </ts>
               <ts e="T104" id="Seg_4047" n="e" s="T103">daːganɨ </ts>
               <ts e="T105" id="Seg_4049" n="e" s="T104">bejelere </ts>
               <ts e="T106" id="Seg_4051" n="e" s="T105">daːganɨ. </ts>
               <ts e="T107" id="Seg_4053" n="e" s="T106">Tu͡ok </ts>
               <ts e="T108" id="Seg_4055" n="e" s="T107">baːr </ts>
               <ts e="T109" id="Seg_4057" n="e" s="T108">onon </ts>
               <ts e="T110" id="Seg_4059" n="e" s="T109">ü͡öre </ts>
               <ts e="T111" id="Seg_4061" n="e" s="T110">hɨldʼallar. </ts>
               <ts e="T112" id="Seg_4063" n="e" s="T111">Hu͡ok </ts>
               <ts e="T113" id="Seg_4065" n="e" s="T112">bu͡ollagɨna, </ts>
               <ts e="T114" id="Seg_4067" n="e" s="T113">kajdak </ts>
               <ts e="T115" id="Seg_4069" n="e" s="T114">gɨnɨ͡aj, </ts>
               <ts e="T116" id="Seg_4071" n="e" s="T115">hu͡ok </ts>
               <ts e="T117" id="Seg_4073" n="e" s="T116">da </ts>
               <ts e="T119" id="Seg_4075" n="e" s="T117">hu͡ok. </ts>
            </ts>
            <ts e="T321" id="Seg_4076" n="sc" s="T123">
               <ts e="T124" id="Seg_4078" n="e" s="T123">– Olus </ts>
               <ts e="T125" id="Seg_4080" n="e" s="T124">da </ts>
               <ts e="T126" id="Seg_4082" n="e" s="T125">hanaːrgaːbattar, </ts>
               <ts e="T127" id="Seg_4084" n="e" s="T126">eː. </ts>
               <ts e="T128" id="Seg_4086" n="e" s="T127">A </ts>
               <ts e="T129" id="Seg_4088" n="e" s="T128">jeslʼi </ts>
               <ts e="T130" id="Seg_4090" n="e" s="T129">bu </ts>
               <ts e="T131" id="Seg_4092" n="e" s="T130">(üčü-) </ts>
               <ts e="T132" id="Seg_4094" n="e" s="T131">üčügejdik </ts>
               <ts e="T133" id="Seg_4096" n="e" s="T132">eː </ts>
               <ts e="T134" id="Seg_4098" n="e" s="T133">kihiliː </ts>
               <ts e="T135" id="Seg_4100" n="e" s="T134">bu </ts>
               <ts e="T136" id="Seg_4102" n="e" s="T135">ileliː </ts>
               <ts e="T137" id="Seg_4104" n="e" s="T136">oloror </ts>
               <ts e="T138" id="Seg_4106" n="e" s="T137">kihi </ts>
               <ts e="T139" id="Seg_4108" n="e" s="T138">ba, </ts>
               <ts e="T140" id="Seg_4110" n="e" s="T139">ile </ts>
               <ts e="T141" id="Seg_4112" n="e" s="T140">daːganɨ, </ts>
               <ts e="T142" id="Seg_4114" n="e" s="T141">hepsekiː </ts>
               <ts e="T143" id="Seg_4116" n="e" s="T142">hogus </ts>
               <ts e="T144" id="Seg_4118" n="e" s="T143">boloktoru </ts>
               <ts e="T145" id="Seg_4120" n="e" s="T144">admʼinʼistracɨjattan </ts>
               <ts e="T146" id="Seg_4122" n="e" s="T145">bi͡eri͡ek </ts>
               <ts e="T147" id="Seg_4124" n="e" s="T146">etilere </ts>
               <ts e="T148" id="Seg_4126" n="e" s="T147">bu͡olla, </ts>
               <ts e="T149" id="Seg_4128" n="e" s="T148">bu </ts>
               <ts e="T150" id="Seg_4130" n="e" s="T149">nʼeabxadʼimɨj, </ts>
               <ts e="T151" id="Seg_4132" n="e" s="T150">muŋ </ts>
               <ts e="T152" id="Seg_4134" n="e" s="T151">muŋ </ts>
               <ts e="T153" id="Seg_4136" n="e" s="T152">naːdalaːk </ts>
               <ts e="T154" id="Seg_4138" n="e" s="T153">ebi͡ennʼelere </ts>
               <ts e="T155" id="Seg_4140" n="e" s="T154">tu͡oktara </ts>
               <ts e="T156" id="Seg_4142" n="e" s="T155">(ki-) </ts>
               <ts e="T157" id="Seg_4144" n="e" s="T156">bu </ts>
               <ts e="T158" id="Seg_4146" n="e" s="T157">ke </ts>
               <ts e="T159" id="Seg_4148" n="e" s="T158">pomašʼ, </ts>
               <ts e="T160" id="Seg_4150" n="e" s="T159">datacɨja </ts>
               <ts e="T161" id="Seg_4152" n="e" s="T160">kördük </ts>
               <ts e="T162" id="Seg_4154" n="e" s="T161">ulakannɨk </ts>
               <ts e="T163" id="Seg_4156" n="e" s="T162">kelere </ts>
               <ts e="T164" id="Seg_4158" n="e" s="T163">bu͡ola </ts>
               <ts e="T165" id="Seg_4160" n="e" s="T164">tɨ͡ataːgɨlarga. </ts>
               <ts e="T166" id="Seg_4162" n="e" s="T165">Bejelerin, </ts>
               <ts e="T167" id="Seg_4164" n="e" s="T166">bejelerin </ts>
               <ts e="T168" id="Seg_4166" n="e" s="T167">kɨ͡aktarɨnan </ts>
               <ts e="T169" id="Seg_4168" n="e" s="T168">hɨldʼanaːktɨːllar, </ts>
               <ts e="T170" id="Seg_4170" n="e" s="T169">tɨ͡ataːgɨlarbɨt. </ts>
               <ts e="T171" id="Seg_4172" n="e" s="T170">Ol </ts>
               <ts e="T172" id="Seg_4174" n="e" s="T171">ihin </ts>
               <ts e="T173" id="Seg_4176" n="e" s="T172">anɨ </ts>
               <ts e="T174" id="Seg_4178" n="e" s="T173">össü͡ö </ts>
               <ts e="T175" id="Seg_4180" n="e" s="T174">iti </ts>
               <ts e="T176" id="Seg_4182" n="e" s="T175">tɨːhɨčča </ts>
               <ts e="T177" id="Seg_4184" n="e" s="T176">bi͡es </ts>
               <ts e="T178" id="Seg_4186" n="e" s="T177">hüːs, </ts>
               <ts e="T179" id="Seg_4188" n="e" s="T178">iti </ts>
               <ts e="T180" id="Seg_4190" n="e" s="T179">kim </ts>
               <ts e="T181" id="Seg_4192" n="e" s="T180">kam </ts>
               <ts e="T182" id="Seg_4194" n="e" s="T181">paltaraškanɨ </ts>
               <ts e="T183" id="Seg_4196" n="e" s="T182">bi͡eren </ts>
               <ts e="T184" id="Seg_4198" n="e" s="T183">bu͡olannar, </ts>
               <ts e="T185" id="Seg_4200" n="e" s="T184">ješʼo </ts>
               <ts e="T186" id="Seg_4202" n="e" s="T185">hi͡ese. </ts>
               <ts e="T187" id="Seg_4204" n="e" s="T186">Aː, </ts>
               <ts e="T188" id="Seg_4206" n="e" s="T187">a </ts>
               <ts e="T189" id="Seg_4208" n="e" s="T188">tak </ts>
               <ts e="T190" id="Seg_4210" n="e" s="T189">hapsi͡em </ts>
               <ts e="T191" id="Seg_4212" n="e" s="T190">bu͡o. </ts>
               <ts e="T192" id="Seg_4214" n="e" s="T191">Tu͡ok </ts>
               <ts e="T193" id="Seg_4216" n="e" s="T192">olus </ts>
               <ts e="T194" id="Seg_4218" n="e" s="T193">daːganɨ </ts>
               <ts e="T195" id="Seg_4220" n="e" s="T194">(ast-) </ts>
               <ts e="T196" id="Seg_4222" n="e" s="T195">ahɨnan </ts>
               <ts e="T197" id="Seg_4224" n="e" s="T196">daːganɨ, </ts>
               <ts e="T198" id="Seg_4226" n="e" s="T197">kili͡ebinen </ts>
               <ts e="T199" id="Seg_4228" n="e" s="T198">da, </ts>
               <ts e="T200" id="Seg_4230" n="e" s="T199">tugunan </ts>
               <ts e="T201" id="Seg_4232" n="e" s="T200">da, </ts>
               <ts e="T202" id="Seg_4234" n="e" s="T201">ol </ts>
               <ts e="T203" id="Seg_4236" n="e" s="T202">pʼikarnʼabɨt </ts>
               <ts e="T204" id="Seg_4238" n="e" s="T203">üleleːbet </ts>
               <ts e="T205" id="Seg_4240" n="e" s="T204">bu͡olan, </ts>
               <ts e="T206" id="Seg_4242" n="e" s="T205">bejelere </ts>
               <ts e="T207" id="Seg_4244" n="e" s="T206">buharɨnaːktɨːllar </ts>
               <ts e="T208" id="Seg_4246" n="e" s="T207">tɨ͡aga </ts>
               <ts e="T209" id="Seg_4248" n="e" s="T208">hɨldʼannar, </ts>
               <ts e="T210" id="Seg_4250" n="e" s="T209">kannɨk… </ts>
               <ts e="T211" id="Seg_4252" n="e" s="T210">Kannɨk </ts>
               <ts e="T212" id="Seg_4254" n="e" s="T211">ogo </ts>
               <ts e="T213" id="Seg_4256" n="e" s="T212">kihi </ts>
               <ts e="T214" id="Seg_4258" n="e" s="T213">"barɨ͡am" </ts>
               <ts e="T215" id="Seg_4260" n="e" s="T214">di͡ej </ts>
               <ts e="T216" id="Seg_4262" n="e" s="T215">"itiː </ts>
               <ts e="T217" id="Seg_4264" n="e" s="T216">dʼi͡etten, </ts>
               <ts e="T218" id="Seg_4266" n="e" s="T217">itiː </ts>
               <ts e="T219" id="Seg_4268" n="e" s="T218">u͡otton </ts>
               <ts e="T220" id="Seg_4270" n="e" s="T219">taksan, </ts>
               <ts e="T221" id="Seg_4272" n="e" s="T220">tɨmnɨːga, </ts>
               <ts e="T222" id="Seg_4274" n="e" s="T221">tɨ͡alga </ts>
               <ts e="T223" id="Seg_4276" n="e" s="T222">taksan, </ts>
               <ts e="T224" id="Seg_4278" n="e" s="T223">tabanɨ </ts>
               <ts e="T225" id="Seg_4280" n="e" s="T224">ketiː </ts>
               <ts e="T226" id="Seg_4282" n="e" s="T225">barɨ͡am" </ts>
               <ts e="T227" id="Seg_4284" n="e" s="T226">di͡en </ts>
               <ts e="T228" id="Seg_4286" n="e" s="T227">du͡o? </ts>
               <ts e="T229" id="Seg_4288" n="e" s="T228">Tölöːböttör </ts>
               <ts e="T230" id="Seg_4290" n="e" s="T229">daːganɨ </ts>
               <ts e="T231" id="Seg_4292" n="e" s="T230">ol </ts>
               <ts e="T232" id="Seg_4294" n="e" s="T231">oččo </ts>
               <ts e="T233" id="Seg_4296" n="e" s="T232">bu͡olla. </ts>
               <ts e="T234" id="Seg_4298" n="e" s="T233">Karčɨnɨ </ts>
               <ts e="T235" id="Seg_4300" n="e" s="T234">körböttör </ts>
               <ts e="T236" id="Seg_4302" n="e" s="T235">tɨ͡ataːgɨlar. </ts>
               <ts e="T237" id="Seg_4304" n="e" s="T236">Hanɨ </ts>
               <ts e="T238" id="Seg_4306" n="e" s="T237">pɨrsɨ͡an, </ts>
               <ts e="T239" id="Seg_4308" n="e" s="T238">tu͡ok </ts>
               <ts e="T240" id="Seg_4310" n="e" s="T239">egeleller </ts>
               <ts e="T241" id="Seg_4312" n="e" s="T240">(kamʼe-) </ts>
               <ts e="T242" id="Seg_4314" n="e" s="T241">kamʼersantar </ts>
               <ts e="T243" id="Seg_4316" n="e" s="T242">atɨːlɨːllar </ts>
               <ts e="T244" id="Seg_4318" n="e" s="T243">hürdeːk </ts>
               <ts e="T245" id="Seg_4320" n="e" s="T244">henalaːkka. </ts>
               <ts e="T246" id="Seg_4322" n="e" s="T245">Ontuŋ </ts>
               <ts e="T247" id="Seg_4324" n="e" s="T246">tu͡ok </ts>
               <ts e="T248" id="Seg_4326" n="e" s="T247">kihitej </ts>
               <ts e="T249" id="Seg_4328" n="e" s="T248">ɨlɨnan </ts>
               <ts e="T250" id="Seg_4330" n="e" s="T249">ɨlɨ͡aj </ts>
               <ts e="T251" id="Seg_4332" n="e" s="T250">duː, </ts>
               <ts e="T252" id="Seg_4334" n="e" s="T251">eː, </ts>
               <ts e="T253" id="Seg_4336" n="e" s="T252">ahɨ, </ts>
               <ts e="T254" id="Seg_4338" n="e" s="T253">ahɨ </ts>
               <ts e="T255" id="Seg_4340" n="e" s="T254">ɨlɨnɨ͡aj </ts>
               <ts e="T256" id="Seg_4342" n="e" s="T255">duː? </ts>
               <ts e="T257" id="Seg_4344" n="e" s="T256">Ahɨ </ts>
               <ts e="T258" id="Seg_4346" n="e" s="T257">ɨlɨnɨ͡aŋ </ts>
               <ts e="T259" id="Seg_4348" n="e" s="T258">duː, </ts>
               <ts e="T260" id="Seg_4350" n="e" s="T259">pɨrsɨ͡an </ts>
               <ts e="T261" id="Seg_4352" n="e" s="T260">ɨlɨnɨ͡aŋ </ts>
               <ts e="T262" id="Seg_4354" n="e" s="T261">duː, </ts>
               <ts e="T263" id="Seg_4356" n="e" s="T262">taŋas </ts>
               <ts e="T264" id="Seg_4358" n="e" s="T263">ɨlɨnɨ͡aŋ </ts>
               <ts e="T265" id="Seg_4360" n="e" s="T264">duː, </ts>
               <ts e="T266" id="Seg_4362" n="e" s="T265">ogoloruŋ </ts>
               <ts e="T267" id="Seg_4364" n="e" s="T266">össü͡ö </ts>
               <ts e="T268" id="Seg_4366" n="e" s="T267">ü͡öreni͡ekterin </ts>
               <ts e="T269" id="Seg_4368" n="e" s="T268">naːda. </ts>
               <ts e="T270" id="Seg_4370" n="e" s="T269">Tu͡ok </ts>
               <ts e="T271" id="Seg_4372" n="e" s="T270">daːganɨ </ts>
               <ts e="T272" id="Seg_4374" n="e" s="T271">anɨ </ts>
               <ts e="T273" id="Seg_4376" n="e" s="T272">bu͡ola </ts>
               <ts e="T274" id="Seg_4378" n="e" s="T273">urut </ts>
               <ts e="T275" id="Seg_4380" n="e" s="T274">studentarɨ, </ts>
               <ts e="T276" id="Seg_4382" n="e" s="T275">tu͡oktarɨ </ts>
               <ts e="T277" id="Seg_4384" n="e" s="T276">haːtar </ts>
               <ts e="T278" id="Seg_4386" n="e" s="T277">bi͡ereːčči </ts>
               <ts e="T279" id="Seg_4388" n="e" s="T278">etilere </ts>
               <ts e="T280" id="Seg_4390" n="e" s="T279">ginilerge </ts>
               <ts e="T281" id="Seg_4392" n="e" s="T280">tu͡ok </ts>
               <ts e="T282" id="Seg_4394" n="e" s="T281">ere, </ts>
               <ts e="T283" id="Seg_4396" n="e" s="T282">datacɨja </ts>
               <ts e="T284" id="Seg_4398" n="e" s="T283">bu͡ol, </ts>
               <ts e="T285" id="Seg_4400" n="e" s="T284">hapku͡ostan </ts>
               <ts e="T286" id="Seg_4402" n="e" s="T285">napravlʼajdaːččɨ </ts>
               <ts e="T287" id="Seg_4404" n="e" s="T286">etilere. </ts>
               <ts e="T288" id="Seg_4406" n="e" s="T287">Anɨ </ts>
               <ts e="T289" id="Seg_4408" n="e" s="T288">ol </ts>
               <ts e="T290" id="Seg_4410" n="e" s="T289">onnuk </ts>
               <ts e="T291" id="Seg_4412" n="e" s="T290">tu͡ok </ts>
               <ts e="T292" id="Seg_4414" n="e" s="T291">da </ts>
               <ts e="T293" id="Seg_4416" n="e" s="T292">hu͡ok. </ts>
               <ts e="T294" id="Seg_4418" n="e" s="T293">Barɨta </ts>
               <ts e="T295" id="Seg_4420" n="e" s="T294">kihi </ts>
               <ts e="T296" id="Seg_4422" n="e" s="T295">bejetin </ts>
               <ts e="T297" id="Seg_4424" n="e" s="T296">karčɨtɨnan </ts>
               <ts e="T298" id="Seg_4426" n="e" s="T297">hɨldʼar. </ts>
               <ts e="T299" id="Seg_4428" n="e" s="T298">Kim </ts>
               <ts e="T300" id="Seg_4430" n="e" s="T299">kɨ͡aga </ts>
               <ts e="T301" id="Seg_4432" n="e" s="T300">hu͡ok </ts>
               <ts e="T302" id="Seg_4434" n="e" s="T301">kihi </ts>
               <ts e="T303" id="Seg_4436" n="e" s="T302">hapsi͡em </ts>
               <ts e="T304" id="Seg_4438" n="e" s="T303">ölör </ts>
               <ts e="T305" id="Seg_4440" n="e" s="T304">di͡ebikke </ts>
               <ts e="T306" id="Seg_4442" n="e" s="T305">di͡eri </ts>
               <ts e="T307" id="Seg_4444" n="e" s="T306">hiti </ts>
               <ts e="T308" id="Seg_4446" n="e" s="T307">hanaːn. </ts>
               <ts e="T309" id="Seg_4448" n="e" s="T308">A </ts>
               <ts e="T310" id="Seg_4450" n="e" s="T309">kim </ts>
               <ts e="T311" id="Seg_4452" n="e" s="T310">kɨ͡aktaːgɨŋ </ts>
               <ts e="T312" id="Seg_4454" n="e" s="T311">hi͡ese </ts>
               <ts e="T313" id="Seg_4456" n="e" s="T312">ü͡öreteller </ts>
               <ts e="T314" id="Seg_4458" n="e" s="T313">ogolorun, </ts>
               <ts e="T315" id="Seg_4460" n="e" s="T314">hin </ts>
               <ts e="T316" id="Seg_4462" n="e" s="T315">biːr </ts>
               <ts e="T317" id="Seg_4464" n="e" s="T316">na </ts>
               <ts e="T318" id="Seg_4466" n="e" s="T317">granʼi </ts>
               <ts e="T319" id="Seg_4468" n="e" s="T318">nʼišʼetɨ </ts>
               <ts e="T321" id="Seg_4470" n="e" s="T319">olorollor. </ts>
            </ts>
            <ts e="T350" id="Seg_4471" n="sc" s="T335">
               <ts e="T336" id="Seg_4473" n="e" s="T335">– Naj </ts>
               <ts e="T337" id="Seg_4475" n="e" s="T336">tojonnor </ts>
               <ts e="T338" id="Seg_4477" n="e" s="T337">ere, </ts>
               <ts e="T339" id="Seg_4479" n="e" s="T338">tojonnor </ts>
               <ts e="T340" id="Seg_4481" n="e" s="T339">ere </ts>
               <ts e="T341" id="Seg_4483" n="e" s="T340">kömölöhü͡öktere </ts>
               <ts e="T342" id="Seg_4485" n="e" s="T341">bihigi </ts>
               <ts e="T343" id="Seg_4487" n="e" s="T342">hirbitiger. </ts>
               <ts e="T344" id="Seg_4489" n="e" s="T343">Olopput </ts>
               <ts e="T345" id="Seg_4491" n="e" s="T344">tuksara </ts>
               <ts e="T346" id="Seg_4493" n="e" s="T345">barɨta </ts>
               <ts e="T347" id="Seg_4495" n="e" s="T346">ginilerten </ts>
               <ts e="T348" id="Seg_4497" n="e" s="T347">ere </ts>
               <ts e="T350" id="Seg_4499" n="e" s="T348">taksɨ͡aga. </ts>
            </ts>
            <ts e="T449" id="Seg_4500" n="sc" s="T360">
               <ts e="T362" id="Seg_4502" n="e" s="T360">– (Beje-) </ts>
               <ts e="T365" id="Seg_4504" n="e" s="T362">bejebit </ts>
               <ts e="T368" id="Seg_4506" n="e" s="T365">anɨ, </ts>
               <ts e="T369" id="Seg_4508" n="e" s="T368">kihibit </ts>
               <ts e="T370" id="Seg_4510" n="e" s="T369">anɨ, </ts>
               <ts e="T371" id="Seg_4512" n="e" s="T370">kihilerbit </ts>
               <ts e="T372" id="Seg_4514" n="e" s="T371">bu͡olla </ts>
               <ts e="T373" id="Seg_4516" n="e" s="T372">v asnavnom </ts>
               <ts e="T374" id="Seg_4518" n="e" s="T373">elbek </ts>
               <ts e="T375" id="Seg_4520" n="e" s="T374">kihi </ts>
               <ts e="T376" id="Seg_4522" n="e" s="T375">eː </ts>
               <ts e="T377" id="Seg_4524" n="e" s="T376">kiːrbite </ts>
               <ts e="T378" id="Seg_4526" n="e" s="T377">tɨ͡attan </ts>
               <ts e="T379" id="Seg_4528" n="e" s="T378">kɨ͡aga </ts>
               <ts e="T380" id="Seg_4530" n="e" s="T379">hu͡ok </ts>
               <ts e="T381" id="Seg_4532" n="e" s="T380">bu͡olannar. </ts>
               <ts e="T382" id="Seg_4534" n="e" s="T381">Barɨlara </ts>
               <ts e="T383" id="Seg_4536" n="e" s="T382">ulakan </ts>
               <ts e="T384" id="Seg_4538" n="e" s="T383">opɨttaːktar </ts>
               <ts e="T385" id="Seg_4540" n="e" s="T384">tabaga </ts>
               <ts e="T386" id="Seg_4542" n="e" s="T385">hɨldʼɨːtɨgar </ts>
               <ts e="T387" id="Seg_4544" n="e" s="T386">daːganɨ </ts>
               <ts e="T388" id="Seg_4546" n="e" s="T387">eː </ts>
               <ts e="T389" id="Seg_4548" n="e" s="T388">ol </ts>
               <ts e="T390" id="Seg_4550" n="e" s="T389">barɨta, </ts>
               <ts e="T391" id="Seg_4552" n="e" s="T390">ol </ts>
               <ts e="T392" id="Seg_4554" n="e" s="T391">barɨta, </ts>
               <ts e="T393" id="Seg_4556" n="e" s="T392">ol </ts>
               <ts e="T394" id="Seg_4558" n="e" s="T393">barɨta </ts>
               <ts e="T395" id="Seg_4560" n="e" s="T394">ölör. </ts>
               <ts e="T396" id="Seg_4562" n="e" s="T395">Kihi </ts>
               <ts e="T397" id="Seg_4564" n="e" s="T396">barɨta </ts>
               <ts e="T398" id="Seg_4566" n="e" s="T397">eː, </ts>
               <ts e="T399" id="Seg_4568" n="e" s="T398">nu, </ts>
               <ts e="T400" id="Seg_4570" n="e" s="T399">hirge </ts>
               <ts e="T401" id="Seg_4572" n="e" s="T400">kaːlar, </ts>
               <ts e="T402" id="Seg_4574" n="e" s="T401">skažem, </ts>
               <ts e="T403" id="Seg_4576" n="e" s="T402">hirge </ts>
               <ts e="T404" id="Seg_4578" n="e" s="T403">kaːlar. </ts>
               <ts e="T405" id="Seg_4580" n="e" s="T404">Kihi </ts>
               <ts e="T406" id="Seg_4582" n="e" s="T405">barɨta, </ts>
               <ts e="T407" id="Seg_4584" n="e" s="T406">anɨ </ts>
               <ts e="T408" id="Seg_4586" n="e" s="T407">hɨldʼar </ts>
               <ts e="T409" id="Seg_4588" n="e" s="T408">da </ts>
               <ts e="T410" id="Seg_4590" n="e" s="T409">kihiŋ </ts>
               <ts e="T411" id="Seg_4592" n="e" s="T410">olus </ts>
               <ts e="T412" id="Seg_4594" n="e" s="T411">hiri </ts>
               <ts e="T413" id="Seg_4596" n="e" s="T412">bilbetter, </ts>
               <ts e="T414" id="Seg_4598" n="e" s="T413">ogo </ts>
               <ts e="T415" id="Seg_4600" n="e" s="T414">kihileriŋ, </ts>
               <ts e="T416" id="Seg_4602" n="e" s="T415">anɨ </ts>
               <ts e="T417" id="Seg_4604" n="e" s="T416">hɨldʼannar </ts>
               <ts e="T418" id="Seg_4606" n="e" s="T417">da, </ts>
               <ts e="T419" id="Seg_4608" n="e" s="T418">taksannar </ts>
               <ts e="T420" id="Seg_4610" n="e" s="T419">da </ts>
               <ts e="T421" id="Seg_4612" n="e" s="T420">öskötün. </ts>
               <ts e="T422" id="Seg_4614" n="e" s="T421">Ahɨlɨktaːk </ts>
               <ts e="T423" id="Seg_4616" n="e" s="T422">hirder, </ts>
               <ts e="T424" id="Seg_4618" n="e" s="T423">anɨ </ts>
               <ts e="T425" id="Seg_4620" n="e" s="T424">kɨːlbɨt </ts>
               <ts e="T426" id="Seg_4622" n="e" s="T425">anɨ </ts>
               <ts e="T427" id="Seg_4624" n="e" s="T426">hirin </ts>
               <ts e="T428" id="Seg_4626" n="e" s="T427">ularɨtan, </ts>
               <ts e="T429" id="Seg_4628" n="e" s="T428">bihiginen </ts>
               <ts e="T430" id="Seg_4630" n="e" s="T429">elbegi </ts>
               <ts e="T431" id="Seg_4632" n="e" s="T430">tüher </ts>
               <ts e="T432" id="Seg_4634" n="e" s="T431">bu͡olan, </ts>
               <ts e="T433" id="Seg_4636" n="e" s="T432">ahɨlɨkpɨtɨn </ts>
               <ts e="T434" id="Seg_4638" n="e" s="T433">baratar. </ts>
               <ts e="T435" id="Seg_4640" n="e" s="T434">Ol </ts>
               <ts e="T436" id="Seg_4642" n="e" s="T435">ihin </ts>
               <ts e="T437" id="Seg_4644" n="e" s="T436">hɨldʼar </ts>
               <ts e="T438" id="Seg_4646" n="e" s="T437">hirderbit </ts>
               <ts e="T439" id="Seg_4648" n="e" s="T438">kɨ͡araːbɨttar – </ts>
               <ts e="T440" id="Seg_4650" n="e" s="T439">tabaga </ts>
               <ts e="T441" id="Seg_4652" n="e" s="T440">daːganɨ, </ts>
               <ts e="T442" id="Seg_4654" n="e" s="T441">kak </ts>
               <ts e="T443" id="Seg_4656" n="e" s="T442">tabaga, </ts>
               <ts e="T444" id="Seg_4658" n="e" s="T443">tak </ts>
               <ts e="T445" id="Seg_4660" n="e" s="T444">kihige </ts>
               <ts e="T446" id="Seg_4662" n="e" s="T445">daːganɨ. </ts>
               <ts e="T447" id="Seg_4664" n="e" s="T446">Kihilerbit </ts>
               <ts e="T449" id="Seg_4666" n="e" s="T447">eː… </ts>
            </ts>
            <ts e="T456" id="Seg_4667" n="sc" s="T451">
               <ts e="T453" id="Seg_4669" n="e" s="T451">– Atɨn, </ts>
               <ts e="T454" id="Seg_4671" n="e" s="T453">atɨn </ts>
               <ts e="T456" id="Seg_4673" n="e" s="T454">ologu… </ts>
            </ts>
            <ts e="T630" id="Seg_4674" n="sc" s="T458">
               <ts e="T460" id="Seg_4676" n="e" s="T458">– Atɨn </ts>
               <ts e="T461" id="Seg_4678" n="e" s="T460">ologu, </ts>
               <ts e="T462" id="Seg_4680" n="e" s="T461">hepsekiː </ts>
               <ts e="T463" id="Seg_4682" n="e" s="T462">ologu </ts>
               <ts e="T464" id="Seg_4684" n="e" s="T463">batannar, </ts>
               <ts e="T465" id="Seg_4686" n="e" s="T464">hepsekiː </ts>
               <ts e="T466" id="Seg_4688" n="e" s="T465">onton </ts>
               <ts e="T467" id="Seg_4690" n="e" s="T466">hepsekiː </ts>
               <ts e="T468" id="Seg_4692" n="e" s="T467">bu͡olu͡o. </ts>
               <ts e="T469" id="Seg_4694" n="e" s="T468">Rɨbnajga </ts>
               <ts e="T470" id="Seg_4696" n="e" s="T469">(olor-), </ts>
               <ts e="T471" id="Seg_4698" n="e" s="T470">eː, </ts>
               <ts e="T472" id="Seg_4700" n="e" s="T471">patom, </ts>
               <ts e="T473" id="Seg_4702" n="e" s="T472">v </ts>
               <ts e="T474" id="Seg_4704" n="e" s="T473">čʼastnastʼi, </ts>
               <ts e="T475" id="Seg_4706" n="e" s="T474">Rɨbnaj </ts>
               <ts e="T476" id="Seg_4708" n="e" s="T475">tuhunan </ts>
               <ts e="T477" id="Seg_4710" n="e" s="T476">kepsiːbin </ts>
               <ts e="T478" id="Seg_4712" n="e" s="T477">onton. </ts>
               <ts e="T479" id="Seg_4714" n="e" s="T478">Rɨbnajga </ts>
               <ts e="T480" id="Seg_4716" n="e" s="T479">hi͡ese </ts>
               <ts e="T481" id="Seg_4718" n="e" s="T480">bu͡o, </ts>
               <ts e="T482" id="Seg_4720" n="e" s="T481">hibi͡etteːkter </ts>
               <ts e="T483" id="Seg_4722" n="e" s="T482">daːganɨ, </ts>
               <ts e="T484" id="Seg_4724" n="e" s="T483">ičiges </ts>
               <ts e="T485" id="Seg_4726" n="e" s="T484">daːganɨ, </ts>
               <ts e="T486" id="Seg_4728" n="e" s="T485">ol </ts>
               <ts e="T487" id="Seg_4730" n="e" s="T486">ihin </ts>
               <ts e="T488" id="Seg_4732" n="e" s="T487">(tɨ͡a-) </ts>
               <ts e="T489" id="Seg_4734" n="e" s="T488">tɨ͡ataːgɨ </ts>
               <ts e="T490" id="Seg_4736" n="e" s="T489">kihiler </ts>
               <ts e="T491" id="Seg_4738" n="e" s="T490">tɨ͡aga </ts>
               <ts e="T492" id="Seg_4740" n="e" s="T491">hɨldʼɨ͡aktarɨn </ts>
               <ts e="T493" id="Seg_4742" n="e" s="T492">olus </ts>
               <ts e="T494" id="Seg_4744" n="e" s="T493">da </ts>
               <ts e="T495" id="Seg_4746" n="e" s="T494">tartarbattar. </ts>
               <ts e="T496" id="Seg_4748" n="e" s="T495">Karčɨ </ts>
               <ts e="T497" id="Seg_4750" n="e" s="T496">olus </ts>
               <ts e="T498" id="Seg_4752" n="e" s="T497">eː, </ts>
               <ts e="T499" id="Seg_4754" n="e" s="T498">tabattan </ts>
               <ts e="T500" id="Seg_4756" n="e" s="T499">tugu </ts>
               <ts e="T501" id="Seg_4758" n="e" s="T500">ɨlɨ͡aŋɨj </ts>
               <ts e="T502" id="Seg_4760" n="e" s="T501">di͡e </ts>
               <ts e="T503" id="Seg_4762" n="e" s="T502">anɨ, </ts>
               <ts e="T504" id="Seg_4764" n="e" s="T503">ölör </ts>
               <ts e="T505" id="Seg_4766" n="e" s="T504">daː </ts>
               <ts e="T506" id="Seg_4768" n="e" s="T505">ol, </ts>
               <ts e="T507" id="Seg_4770" n="e" s="T506">ka </ts>
               <ts e="T508" id="Seg_4772" n="e" s="T507">ahɨjak </ts>
               <ts e="T509" id="Seg_4774" n="e" s="T508">bi͡ehu͡onča </ts>
               <ts e="T510" id="Seg_4776" n="e" s="T509">tabanɨ </ts>
               <ts e="T511" id="Seg_4778" n="e" s="T510">daːganɨ </ts>
               <ts e="T512" id="Seg_4780" n="e" s="T511">ölördökküne, </ts>
               <ts e="T513" id="Seg_4782" n="e" s="T512">olus </ts>
               <ts e="T514" id="Seg_4784" n="e" s="T513">da </ts>
               <ts e="T515" id="Seg_4786" n="e" s="T514">elbek </ts>
               <ts e="T516" id="Seg_4788" n="e" s="T515">karčɨnɨ </ts>
               <ts e="T517" id="Seg_4790" n="e" s="T516">ölörü͡öŋ </ts>
               <ts e="T518" id="Seg_4792" n="e" s="T517">hu͡oga </ts>
               <ts e="T519" id="Seg_4794" n="e" s="T518">ol </ts>
               <ts e="T520" id="Seg_4796" n="e" s="T519">tuhugar, </ts>
               <ts e="T521" id="Seg_4798" n="e" s="T520">ili </ts>
               <ts e="T522" id="Seg_4800" n="e" s="T521">tɨ͡aga </ts>
               <ts e="T523" id="Seg_4802" n="e" s="T522">hɨldʼaːŋŋɨn </ts>
               <ts e="T524" id="Seg_4804" n="e" s="T523">duː. </ts>
               <ts e="T525" id="Seg_4806" n="e" s="T524">Ol </ts>
               <ts e="T526" id="Seg_4808" n="e" s="T525">ihin </ts>
               <ts e="T527" id="Seg_4810" n="e" s="T526">eteŋŋe </ts>
               <ts e="T528" id="Seg_4812" n="e" s="T527">oloru͡okka </ts>
               <ts e="T529" id="Seg_4814" n="e" s="T528">i </ts>
               <ts e="T530" id="Seg_4816" n="e" s="T529">tɨ͡aga </ts>
               <ts e="T531" id="Seg_4818" n="e" s="T530">kihiler </ts>
               <ts e="T532" id="Seg_4820" n="e" s="T531">dʼolloːktuk </ts>
               <ts e="T533" id="Seg_4822" n="e" s="T532">oloru͡oktarɨn </ts>
               <ts e="T534" id="Seg_4824" n="e" s="T533">iti </ts>
               <ts e="T535" id="Seg_4826" n="e" s="T534">maŋnajgɨ </ts>
               <ts e="T536" id="Seg_4828" n="e" s="T535">ol </ts>
               <ts e="T537" id="Seg_4830" n="e" s="T536">tuhugar </ts>
               <ts e="T538" id="Seg_4832" n="e" s="T537">pravʼitʼelʼstva </ts>
               <ts e="T539" id="Seg_4834" n="e" s="T538">dumajdɨ͡agɨn </ts>
               <ts e="T540" id="Seg_4836" n="e" s="T539">naːda. </ts>
               <ts e="T541" id="Seg_4838" n="e" s="T540">Kihilerge </ts>
               <ts e="T542" id="Seg_4840" n="e" s="T541">kömö </ts>
               <ts e="T543" id="Seg_4842" n="e" s="T542">iː… </ts>
               <ts e="T544" id="Seg_4844" n="e" s="T543">Kihiler, </ts>
               <ts e="T545" id="Seg_4846" n="e" s="T544">bejelere </ts>
               <ts e="T546" id="Seg_4848" n="e" s="T545">kihiler </ts>
               <ts e="T547" id="Seg_4850" n="e" s="T546">tugu </ts>
               <ts e="T548" id="Seg_4852" n="e" s="T547">da </ts>
               <ts e="T549" id="Seg_4854" n="e" s="T548">gɨnɨ͡aktara </ts>
               <ts e="T550" id="Seg_4856" n="e" s="T549">hu͡oga, </ts>
               <ts e="T551" id="Seg_4858" n="e" s="T550">tɨ͡ataːgɨ </ts>
               <ts e="T552" id="Seg_4860" n="e" s="T551">kihiler, </ts>
               <ts e="T553" id="Seg_4862" n="e" s="T552">tʼem bolʼeje </ts>
               <ts e="T554" id="Seg_4864" n="e" s="T553">bejelerin </ts>
               <ts e="T555" id="Seg_4866" n="e" s="T554">innilerin </ts>
               <ts e="T556" id="Seg_4868" n="e" s="T555">tuhunan </ts>
               <ts e="T557" id="Seg_4870" n="e" s="T556">tugu </ts>
               <ts e="T558" id="Seg_4872" n="e" s="T557">da </ts>
               <ts e="T559" id="Seg_4874" n="e" s="T558">haŋarɨmattar. </ts>
               <ts e="T560" id="Seg_4876" n="e" s="T559">Hapku͡ostarbɨt, </ts>
               <ts e="T561" id="Seg_4878" n="e" s="T560">dʼirʼektarbɨt, </ts>
               <ts e="T562" id="Seg_4880" n="e" s="T561">dʼirʼektardar </ts>
               <ts e="T563" id="Seg_4882" n="e" s="T562">da </ts>
               <ts e="T564" id="Seg_4884" n="e" s="T563">ularɨjallar </ts>
               <ts e="T565" id="Seg_4886" n="e" s="T564">daːganɨ. </ts>
               <ts e="T566" id="Seg_4888" n="e" s="T565">Anɨ </ts>
               <ts e="T567" id="Seg_4890" n="e" s="T566">össü͡ö </ts>
               <ts e="T568" id="Seg_4892" n="e" s="T567">hi͡ese, </ts>
               <ts e="T569" id="Seg_4894" n="e" s="T568">bihi͡eke </ts>
               <ts e="T570" id="Seg_4896" n="e" s="T569">Rɨbnajga </ts>
               <ts e="T571" id="Seg_4898" n="e" s="T570">össü͡ö </ts>
               <ts e="T572" id="Seg_4900" n="e" s="T571">tura </ts>
               <ts e="T573" id="Seg_4902" n="e" s="T572">tüste </ts>
               <ts e="T574" id="Seg_4904" n="e" s="T573">eːt </ts>
               <ts e="T575" id="Seg_4906" n="e" s="T574">biːr </ts>
               <ts e="T576" id="Seg_4908" n="e" s="T575">dʼirʼektar – </ts>
               <ts e="T577" id="Seg_4910" n="e" s="T576">Mamonav </ts>
               <ts e="T578" id="Seg_4912" n="e" s="T577">Vladʼimʼir </ts>
               <ts e="T579" id="Seg_4914" n="e" s="T578">Alʼeksandravʼičʼ </ts>
               <ts e="T580" id="Seg_4916" n="e" s="T579">di͡en </ts>
               <ts e="T581" id="Seg_4918" n="e" s="T580">kihi. </ts>
               <ts e="T582" id="Seg_4920" n="e" s="T581">Ol </ts>
               <ts e="T583" id="Seg_4922" n="e" s="T582">ol </ts>
               <ts e="T584" id="Seg_4924" n="e" s="T583">tuhunan </ts>
               <ts e="T585" id="Seg_4926" n="e" s="T584">bagas </ts>
               <ts e="T586" id="Seg_4928" n="e" s="T585">tu͡ok </ts>
               <ts e="T587" id="Seg_4930" n="e" s="T586">da </ts>
               <ts e="T588" id="Seg_4932" n="e" s="T587">kuhaganɨ </ts>
               <ts e="T589" id="Seg_4934" n="e" s="T588">haŋarbappɨn, </ts>
               <ts e="T590" id="Seg_4936" n="e" s="T589">berteːk </ts>
               <ts e="T591" id="Seg_4938" n="e" s="T590">kihi, </ts>
               <ts e="T592" id="Seg_4940" n="e" s="T591">eː. </ts>
               <ts e="T593" id="Seg_4942" n="e" s="T592">Kihileri </ts>
               <ts e="T594" id="Seg_4944" n="e" s="T593">gɨtta </ts>
               <ts e="T595" id="Seg_4946" n="e" s="T594">anɨ </ts>
               <ts e="T596" id="Seg_4948" n="e" s="T595">hi͡ese </ts>
               <ts e="T597" id="Seg_4950" n="e" s="T596">karčɨlaːk </ts>
               <ts e="T598" id="Seg_4952" n="e" s="T597">bu͡olbuttar, </ts>
               <ts e="T599" id="Seg_4954" n="e" s="T598">iti </ts>
               <ts e="T600" id="Seg_4956" n="e" s="T599">kim </ts>
               <ts e="T601" id="Seg_4958" n="e" s="T600">ete, </ts>
               <ts e="T602" id="Seg_4960" n="e" s="T601">nolʼ </ts>
               <ts e="T603" id="Seg_4962" n="e" s="T602">sʼemʼ </ts>
               <ts e="T604" id="Seg_4964" n="e" s="T603">bi͡ereːčči </ts>
               <ts e="T605" id="Seg_4966" n="e" s="T604">etilere </ts>
               <ts e="T606" id="Seg_4968" n="e" s="T605">pradukcɨja </ts>
               <ts e="T607" id="Seg_4970" n="e" s="T606">gi͡enin </ts>
               <ts e="T608" id="Seg_4972" n="e" s="T607">ke </ts>
               <ts e="T609" id="Seg_4974" n="e" s="T608">bɨjɨl </ts>
               <ts e="T610" id="Seg_4976" n="e" s="T609">oŋorbuttara. </ts>
               <ts e="T611" id="Seg_4978" n="e" s="T610">Ontularɨn </ts>
               <ts e="T612" id="Seg_4980" n="e" s="T611">da </ts>
               <ts e="T613" id="Seg_4982" n="e" s="T612">ubirajdɨ͡appɨt. </ts>
               <ts e="T614" id="Seg_4984" n="e" s="T613">Onu </ts>
               <ts e="T615" id="Seg_4986" n="e" s="T614">ubiraːjdaːtaktarɨnan </ts>
               <ts e="T616" id="Seg_4988" n="e" s="T615">anɨ </ts>
               <ts e="T617" id="Seg_4990" n="e" s="T616">tɨ͡ataːgɨ </ts>
               <ts e="T618" id="Seg_4992" n="e" s="T617">eː </ts>
               <ts e="T619" id="Seg_4994" n="e" s="T618">bulčuttar </ts>
               <ts e="T620" id="Seg_4996" n="e" s="T619">da </ts>
               <ts e="T621" id="Seg_4998" n="e" s="T620">emi͡e, </ts>
               <ts e="T622" id="Seg_5000" n="e" s="T621">(ɨtɨ͡a-) </ts>
               <ts e="T623" id="Seg_5002" n="e" s="T622">(ɨtɨ͡a-) </ts>
               <ts e="T624" id="Seg_5004" n="e" s="T623">ɨtɨ͡alɨːhɨt </ts>
               <ts e="T625" id="Seg_5006" n="e" s="T624">kihiler </ts>
               <ts e="T626" id="Seg_5008" n="e" s="T625">da </ts>
               <ts e="T627" id="Seg_5010" n="e" s="T626">agɨjagɨ </ts>
               <ts e="T628" id="Seg_5012" n="e" s="T627">ɨlɨ͡aktara </ts>
               <ts e="T629" id="Seg_5014" n="e" s="T628">hin </ts>
               <ts e="T630" id="Seg_5016" n="e" s="T629">biːr. </ts>
            </ts>
            <ts e="T637" id="Seg_5017" n="sc" s="T631">
               <ts e="T633" id="Seg_5019" n="e" s="T631">– (Hatan-) </ts>
               <ts e="T635" id="Seg_5021" n="e" s="T633">hatanɨ͡aktara </ts>
               <ts e="T636" id="Seg_5023" n="e" s="T635">hu͡oktara, </ts>
               <ts e="T637" id="Seg_5025" n="e" s="T636">heː. </ts>
            </ts>
            <ts e="T725" id="Seg_5026" n="sc" s="T657">
               <ts e="T659" id="Seg_5028" n="e" s="T657">– Onon </ts>
               <ts e="T660" id="Seg_5030" n="e" s="T659">erenebit, </ts>
               <ts e="T661" id="Seg_5032" n="e" s="T660">eː, </ts>
               <ts e="T662" id="Seg_5034" n="e" s="T661">onon </ts>
               <ts e="T663" id="Seg_5036" n="e" s="T662">erenebit. </ts>
               <ts e="T664" id="Seg_5038" n="e" s="T663">A </ts>
               <ts e="T665" id="Seg_5040" n="e" s="T664">tak, </ts>
               <ts e="T666" id="Seg_5042" n="e" s="T665">iti </ts>
               <ts e="T667" id="Seg_5044" n="e" s="T666">karaŋa </ts>
               <ts e="T668" id="Seg_5046" n="e" s="T667">di͡ek </ts>
               <ts e="T669" id="Seg_5048" n="e" s="T668">öttütün </ts>
               <ts e="T670" id="Seg_5050" n="e" s="T669">kepsettibit </ts>
               <ts e="T671" id="Seg_5052" n="e" s="T670">bihigi. </ts>
               <ts e="T672" id="Seg_5054" n="e" s="T671">A </ts>
               <ts e="T673" id="Seg_5056" n="e" s="T672">tak </ts>
               <ts e="T674" id="Seg_5058" n="e" s="T673">da </ts>
               <ts e="T675" id="Seg_5060" n="e" s="T674">tɨ͡aga </ts>
               <ts e="T676" id="Seg_5062" n="e" s="T675">hɨrɨttakka </ts>
               <ts e="T677" id="Seg_5064" n="e" s="T676">bert </ts>
               <ts e="T678" id="Seg_5066" n="e" s="T677">beseleː </ts>
               <ts e="T679" id="Seg_5068" n="e" s="T678">ete, </ts>
               <ts e="T680" id="Seg_5070" n="e" s="T679">eː, </ts>
               <ts e="T681" id="Seg_5072" n="e" s="T680">tɨ͡ataːgɨ </ts>
               <ts e="T682" id="Seg_5074" n="e" s="T681">kihiler, </ts>
               <ts e="T683" id="Seg_5076" n="e" s="T682">anɨ </ts>
               <ts e="T684" id="Seg_5078" n="e" s="T683">ogolor </ts>
               <ts e="T685" id="Seg_5080" n="e" s="T684">hɨldʼɨbat </ts>
               <ts e="T686" id="Seg_5082" n="e" s="T685">bu͡olannar, </ts>
               <ts e="T687" id="Seg_5084" n="e" s="T686">anɨ </ts>
               <ts e="T688" id="Seg_5086" n="e" s="T687">bilbetter </ts>
               <ts e="T689" id="Seg_5088" n="e" s="T688">bu͡o. </ts>
               <ts e="T690" id="Seg_5090" n="e" s="T689">Haka </ts>
               <ts e="T691" id="Seg_5092" n="e" s="T690">tɨlɨn, </ts>
               <ts e="T692" id="Seg_5094" n="e" s="T691">čeber </ts>
               <ts e="T693" id="Seg_5096" n="e" s="T692">tɨːnnara </ts>
               <ts e="T694" id="Seg_5098" n="e" s="T693">eː </ts>
               <ts e="T695" id="Seg_5100" n="e" s="T694">halgɨnɨ, </ts>
               <ts e="T696" id="Seg_5102" n="e" s="T695">ot </ts>
               <ts e="T697" id="Seg_5104" n="e" s="T696">hɨtɨn, </ts>
               <ts e="T698" id="Seg_5106" n="e" s="T697">di͡ebikke </ts>
               <ts e="T699" id="Seg_5108" n="e" s="T698">di͡eri, </ts>
               <ts e="T700" id="Seg_5110" n="e" s="T699">maŋnaj </ts>
               <ts e="T701" id="Seg_5112" n="e" s="T700">kaːs </ts>
               <ts e="T702" id="Seg_5114" n="e" s="T701">aːjdaːnɨn, </ts>
               <ts e="T703" id="Seg_5116" n="e" s="T702">tuguttar </ts>
               <ts e="T704" id="Seg_5118" n="e" s="T703">törüːllerin, </ts>
               <ts e="T705" id="Seg_5120" n="e" s="T704">barɨtɨn </ts>
               <ts e="T706" id="Seg_5122" n="e" s="T705">tugu </ts>
               <ts e="T707" id="Seg_5124" n="e" s="T706">da </ts>
               <ts e="T708" id="Seg_5126" n="e" s="T707">olus </ts>
               <ts e="T709" id="Seg_5128" n="e" s="T708">bilbetter </ts>
               <ts e="T710" id="Seg_5130" n="e" s="T709">bu͡o. </ts>
               <ts e="T711" id="Seg_5132" n="e" s="T710">Ontuŋ </ts>
               <ts e="T712" id="Seg_5134" n="e" s="T711">barɨta </ts>
               <ts e="T713" id="Seg_5136" n="e" s="T712">bihige </ts>
               <ts e="T714" id="Seg_5138" n="e" s="T713">eː, </ts>
               <ts e="T715" id="Seg_5140" n="e" s="T714">tɨ͡aga </ts>
               <ts e="T716" id="Seg_5142" n="e" s="T715">ü͡öskeːbit </ts>
               <ts e="T717" id="Seg_5144" n="e" s="T716">kihilerge, </ts>
               <ts e="T718" id="Seg_5146" n="e" s="T717">ontuŋ </ts>
               <ts e="T719" id="Seg_5148" n="e" s="T718">barɨta, </ts>
               <ts e="T720" id="Seg_5150" n="e" s="T719">barɨta </ts>
               <ts e="T721" id="Seg_5152" n="e" s="T720">ulakan </ts>
               <ts e="T722" id="Seg_5154" n="e" s="T721">kim </ts>
               <ts e="T723" id="Seg_5156" n="e" s="T722">bu͡o, </ts>
               <ts e="T725" id="Seg_5158" n="e" s="T723">vpʼečʼatlʼenʼije. </ts>
            </ts>
            <ts e="T764" id="Seg_5159" n="sc" s="T763">
               <ts e="T764" id="Seg_5161" n="e" s="T763">– Bahɨːba. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-UkOA">
            <ta e="T19" id="Seg_5162" s="T7">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.001 (001.002)</ta>
            <ta e="T25" id="Seg_5163" s="T19">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.002 (001.003)</ta>
            <ta e="T31" id="Seg_5164" s="T25">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.003 (001.004)</ta>
            <ta e="T40" id="Seg_5165" s="T31">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.004 (001.005)</ta>
            <ta e="T57" id="Seg_5166" s="T40">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.005 (001.006)</ta>
            <ta e="T59" id="Seg_5167" s="T57">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.006 (001.007)</ta>
            <ta e="T65" id="Seg_5168" s="T59">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.007 (001.008)</ta>
            <ta e="T90" id="Seg_5169" s="T84">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.008 (001.011)</ta>
            <ta e="T106" id="Seg_5170" s="T90">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.009 (001.012)</ta>
            <ta e="T111" id="Seg_5171" s="T106">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.010 (001.013)</ta>
            <ta e="T119" id="Seg_5172" s="T111">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.011 (001.014)</ta>
            <ta e="T127" id="Seg_5173" s="T123">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.012 (001.016)</ta>
            <ta e="T165" id="Seg_5174" s="T127">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.013 (001.017)</ta>
            <ta e="T170" id="Seg_5175" s="T165">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.014 (001.018)</ta>
            <ta e="T186" id="Seg_5176" s="T170">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.015 (001.019)</ta>
            <ta e="T191" id="Seg_5177" s="T186">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.016 (001.020)</ta>
            <ta e="T210" id="Seg_5178" s="T191">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.017 (001.021)</ta>
            <ta e="T228" id="Seg_5179" s="T210">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.018 (001.022)</ta>
            <ta e="T233" id="Seg_5180" s="T228">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.019 (001.023)</ta>
            <ta e="T236" id="Seg_5181" s="T233">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.020 (001.024)</ta>
            <ta e="T245" id="Seg_5182" s="T236">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.021 (001.025)</ta>
            <ta e="T256" id="Seg_5183" s="T245">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.022 (001.026)</ta>
            <ta e="T269" id="Seg_5184" s="T256">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.023 (001.027)</ta>
            <ta e="T287" id="Seg_5185" s="T269">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.024 (001.028)</ta>
            <ta e="T293" id="Seg_5186" s="T287">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.025 (001.029)</ta>
            <ta e="T298" id="Seg_5187" s="T293">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.026 (001.030)</ta>
            <ta e="T308" id="Seg_5188" s="T298">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.027 (001.031)</ta>
            <ta e="T321" id="Seg_5189" s="T308">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.028 (001.032)</ta>
            <ta e="T343" id="Seg_5190" s="T335">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.029 (001.035)</ta>
            <ta e="T350" id="Seg_5191" s="T343">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.030 (001.036)</ta>
            <ta e="T381" id="Seg_5192" s="T360">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.031 (001.038)</ta>
            <ta e="T395" id="Seg_5193" s="T381">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.032 (001.039)</ta>
            <ta e="T404" id="Seg_5194" s="T395">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.033 (001.040)</ta>
            <ta e="T421" id="Seg_5195" s="T404">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.034 (001.041)</ta>
            <ta e="T434" id="Seg_5196" s="T421">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.035 (001.042)</ta>
            <ta e="T446" id="Seg_5197" s="T434">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.036 (001.043)</ta>
            <ta e="T449" id="Seg_5198" s="T446">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.037 (001.044)</ta>
            <ta e="T456" id="Seg_5199" s="T451">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.038 (001.046)</ta>
            <ta e="T468" id="Seg_5200" s="T458">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.039 (001.048)</ta>
            <ta e="T478" id="Seg_5201" s="T468">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.040 (001.049)</ta>
            <ta e="T495" id="Seg_5202" s="T478">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.041 (001.050)</ta>
            <ta e="T524" id="Seg_5203" s="T495">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.042 (001.051)</ta>
            <ta e="T540" id="Seg_5204" s="T524">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.043 (001.052)</ta>
            <ta e="T543" id="Seg_5205" s="T540">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.044 (001.053)</ta>
            <ta e="T559" id="Seg_5206" s="T543">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.045 (001.054)</ta>
            <ta e="T565" id="Seg_5207" s="T559">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.046 (001.055)</ta>
            <ta e="T581" id="Seg_5208" s="T565">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.047 (001.056)</ta>
            <ta e="T592" id="Seg_5209" s="T581">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.048 (001.057)</ta>
            <ta e="T610" id="Seg_5210" s="T592">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.049 (001.058)</ta>
            <ta e="T613" id="Seg_5211" s="T610">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.050 (001.059)</ta>
            <ta e="T630" id="Seg_5212" s="T613">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.051 (001.060)</ta>
            <ta e="T637" id="Seg_5213" s="T631">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.052 (001.062)</ta>
            <ta e="T663" id="Seg_5214" s="T657">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.053 (001.064)</ta>
            <ta e="T671" id="Seg_5215" s="T663">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.054 (001.065)</ta>
            <ta e="T689" id="Seg_5216" s="T671">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.055 (001.066)</ta>
            <ta e="T710" id="Seg_5217" s="T689">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.056 (001.067)</ta>
            <ta e="T725" id="Seg_5218" s="T710">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.057 (001.068)</ta>
            <ta e="T764" id="Seg_5219" s="T763">UkOA_KuNS_20XX_LifeNovorybnoe_conv.UkOA.058 (001.072)</ta>
         </annotation>
         <annotation name="st" tierref="st-UkOA">
            <ta e="T19" id="Seg_5220" s="T7">– Аа, бэйэм Носкуога дьиэлэк буолабын, ээ,тыага агыйатык буолабын буо, һин биир.</ta>
            <ta e="T25" id="Seg_5221" s="T19">Һаастары, күһүннэри буол биэк тыага һылдьабын.</ta>
            <ta e="T31" id="Seg_5222" s="T25">Кас дьыл аайы иньэмээппэр, тээтэмээппэр көмөлөһө. </ta>
            <ta e="T40" id="Seg_5223" s="T31">Ол диэк, тыага һыльдар буоланнар гиэ табалактар буо ол.</ta>
            <ta e="T57" id="Seg_5224" s="T40">Табалары, табаларын да көрө һотору дааганый онугу бэйэм дааганы заж.. кимниибин, иитиллэбин ээ киминэн, энергийанан диэбиккэ диэри.</ta>
            <ta e="T59" id="Seg_5225" s="T57">Свежий воздух. </ta>
            <ta e="T65" id="Seg_5226" s="T59">Носкуога һытан тыынын кайалар һол (һорок) кэнтэ.</ta>
            <ta e="T90" id="Seg_5227" s="T84">– Тыатаагылар бэйлэрэ билэллэр буо кайдак олоруоктарын. </ta>
            <ta e="T106" id="Seg_5228" s="T90">Аны үчүгэй ологу билбэт буоланар, гиллэр (гинилэр) ээ үчүгуойдык(үчүгэйдие) олоруоппут диэн испэтэр, быһылак, олус дааганы бэйлэрэ даа аны. </ta>
            <ta e="T111" id="Seg_5229" s="T106">Туок баар онон үөрэ һылдьаллар.</ta>
            <ta e="T119" id="Seg_5230" s="T111">Һуок буолагына, кайдак гыныай һуок да һуок. </ta>
            <ta e="T127" id="Seg_5231" s="T123">– Олус да һанааргаабаттар, һээ.</ta>
            <ta e="T165" id="Seg_5232" s="T127">А если бу учу учугуойдык (үчүгэйдик) оо киһилии бу илэлии олорор киһи ба, илэ дааганы – һэпсэкии һогус болоктору администрацияттан биэриэк этилэрэ буолла, бу необходимый, муӈ наадалаак эбиэнньэлэрэ туоктара ки.. бу киэ помощь, дотация көрдүк улаканнык кэлэрэ буола тыатаагыларга. </ta>
            <ta e="T170" id="Seg_5233" s="T165">Бэйэлэрин, бэйэлэрин кыактарынан һылдьынаактыыллар тыатаагыларбыт. </ta>
            <ta e="T186" id="Seg_5234" s="T170">Ол иһин аны өссүө ити тыыһычча биэс һүүс, ити ким кам полторашканы биэрэн буоланар, ещё һиэсэ.</ta>
            <ta e="T191" id="Seg_5235" s="T186">Аа, а так һопсиэм буо. </ta>
            <ta e="T210" id="Seg_5236" s="T191">Туок олус дааганы аст.. аһынан дааганы, килиэбинэн да.., тугунан даа, ол пикарньабыт үлэлээбэт буолан, бэйээрэ буһарынаактыыллар тыага һылдьаннар, каннык..</ta>
            <ta e="T228" id="Seg_5237" s="T210">Каннык ого киһи багарыам (барыам) диэй итии диэттэн, итии уоттон таксан, тымныыга, тыалга таксан, табаны кэтии барыам диэн дуо? </ta>
            <ta e="T233" id="Seg_5238" s="T228">Төлөөбөттөр дааганы ол оччо буола.</ta>
            <ta e="T236" id="Seg_5239" s="T233">Карчыны көрбөттөр тыатаагылар. </ta>
            <ta e="T245" id="Seg_5240" s="T236">Һаны (аны)ы пырсыан, туок эгэлэллэр коми комерсантар атыылыылар һүрдэк һэналаакка.</ta>
            <ta e="T256" id="Seg_5241" s="T245">Онтуӈ туок киһитэй ылынан ылыай дуу, и ээ, аһы, аһы ылыныай дуу?</ta>
            <ta e="T269" id="Seg_5242" s="T256">–Аһы ылыныаӈ дуу, пырысыан ылыныаӈ дуу, таӈас ылыныаӈ дуу, оголоруӈ өссүө үөрэниэктэрин наада. </ta>
            <ta e="T287" id="Seg_5243" s="T269">Туок дааганы буола урут студентары, туоктары һаатар биэрээччи этилэрэ гинилэргэ туок эрэ, дотация буол, һопкуостан напраляйдааччы этилэрэ. </ta>
            <ta e="T293" id="Seg_5244" s="T287">Аны ол оннук туок да һуок. </ta>
            <ta e="T298" id="Seg_5245" s="T293">Барыта киһи бэйтин карчытынан һылдьар. </ta>
            <ta e="T308" id="Seg_5246" s="T298">Ким кыага һуок киһи һопсиэм өлөр диэбиккэ диэри һити һанаан. </ta>
            <ta e="T321" id="Seg_5247" s="T308">А ким кыактаагыӈ һиэсэ үөрэтэллэр оголорун, һин биир на грани нищеты олороллор. </ta>
            <ta e="T343" id="Seg_5248" s="T335">– Най тойоннор эрэ, тойоннор эрэ көмөлөһүөктэрэ биһиги һирбитигэр. </ta>
            <ta e="T350" id="Seg_5249" s="T343">Олокпут туксара барыта гинилэртэн эрэ таксыага. </ta>
            <ta e="T381" id="Seg_5250" s="T360">– Бэйэбит .. Бэйэбин аны, киһибит аны, киһилэрбит буо в основном элбэк киһи ээ киирбитэ тыаттан кыага һуок буоланар. </ta>
            <ta e="T395" id="Seg_5251" s="T381">Барылара улакан опыттаактар табага һылдьыытыгар дааганы ээ ол барыта, ол барыта, ол барыта өлөр. </ta>
            <ta e="T404" id="Seg_5252" s="T395">Киһи барыта ээ, ну, һиргэ каалар, скажем, һиргэ калар. </ta>
            <ta e="T421" id="Seg_5253" s="T404">Киһи барыта.., аны һылдьар да киһиӈ олус һири билбэттэр, ого киһилэриӈ, аны һылдьаннар да, таксаннар да өскөтүн.</ta>
            <ta e="T434" id="Seg_5254" s="T421">Аһылыктаак һирдэр .. аны кыылбыт аны һирин уларытан, биһигинэн элбэги түһэр буолан, аһылыкпытын баратар.</ta>
            <ta e="T446" id="Seg_5255" s="T434">Ол иһин һылдьар һирдэрбит кыараабыттар – табага дааганы.. как табага, так киһэгэ даагаыы.</ta>
            <ta e="T449" id="Seg_5256" s="T446">Киһилэрбит ээ.. </ta>
            <ta e="T456" id="Seg_5257" s="T451">– Атын, атын ологу ..</ta>
            <ta e="T468" id="Seg_5258" s="T458">– Атын ологу, һэпсэкии ологу батаннар… Һэпсэкии онтон һэпсэкии буолуо ..</ta>
            <ta e="T478" id="Seg_5259" s="T468">Рыбнайга олор .., ээ, потом, в частности, Рыбнай туһунан кэпсиибин онтон. </ta>
            <ta e="T495" id="Seg_5260" s="T478">Рыбнайга һиэсэ буо – һибиэттээктэр дааганы, ичигэс дааганы, ол иһин тыа .. тыатаагы киһилэр тыага һылдьыактарын олус да тартарбаттар. </ta>
            <ta e="T524" id="Seg_5261" s="T495">Карчи олус ээ.. Табаттан тугу ылыаӈый диэ аны: өлөр даа ол, һү.. каһыйак биэһуонча табаны дааганы өлөрдөккүнэ, олус да элбэк карчыны өлөрүөӈ һуога ол туһугар, или тыага һылдьааӈӈын дуу. </ta>
            <ta e="T540" id="Seg_5262" s="T524">Ол иһин этэӈӈэ олоруокка и тыага киһилэр дьоллоктук олоруоктарын ити маӈнайгы ол туһугар правительство думайдыагын наада – </ta>
            <ta e="T543" id="Seg_5263" s="T540">киһилэргэ көмө ии</ta>
            <ta e="T559" id="Seg_5264" s="T543">киһилэр .. бэйлэрэ киһилэр тугу да гыныактара һуога, тыатаагы киһилэр, тем более бэйлэрин иннилэрин туһунан тугу да һаӈарыматтар.</ta>
            <ta e="T565" id="Seg_5265" s="T559">Һопкуостарбын, директорбыт .. Директордар уларыйаллар дааганы. </ta>
            <ta e="T581" id="Seg_5266" s="T565">Аны өссүө һиэсэ, биһикэ Рымнайга өссүө тура түстэ ээт биир директор – Момонов Владимир Александрович диэн киһи.</ta>
            <ta e="T592" id="Seg_5267" s="T581">О ол туһунан багас туок да куһаганы һаӈарбаппын, бэртээк киһи. Ммм.</ta>
            <ta e="T610" id="Seg_5268" s="T592">Киһилэри гытта аны .. һиэсэ карчылаак буолбуттар – ити ким этэ: ноль семь биэрээччи этилэрэ продукция гиэнин киэ быйыл оӈорбуттара. </ta>
            <ta e="T613" id="Seg_5269" s="T610">Онтуларын да убирайдыаппыт, диэн аны.</ta>
            <ta e="T630" id="Seg_5270" s="T613">Ону убираайдаатактарынан аны Ону убирайдаатактарынан эмиэ аны тыатаагы ээ булчуттар да эмиэ, ытыа ытыалыһыт киһилэр да агыйагы ылыактара һин биир.</ta>
            <ta e="T637" id="Seg_5271" s="T631">– Һатаныактара һуога, һээ. </ta>
            <ta e="T663" id="Seg_5272" s="T657">– Онон эрэ эрэнэбит, ээ, онон эрэнэбит. </ta>
            <ta e="T671" id="Seg_5273" s="T663">А так .. –Ити караӈа диэк өттүтүн кэпсэттитбит биһиги. </ta>
            <ta e="T689" id="Seg_5274" s="T671">А так да тыага һырыттакка бэрт бэсэлээтэ, ээ, тыатаагы киһилэр.. аны оголор һылдьыбат буоланнар, аны билбэттэр буо </ta>
            <ta e="T710" id="Seg_5275" s="T689">– Һака тылын, чэбэр тыынара ээ һалгыны, от һытын,диэбиккэ диэри, маӈнай каас аайдаанын, тугуттар төрүүллэрин, – барытын тугу да олус билбэттэр буо. </ta>
            <ta e="T725" id="Seg_5276" s="T710">Онтуӈ барыта биһигэ (биһиэкэ) ээ, тыага үөскээбит киһилэргэ, онтуӈ барыта, барыта улакан ким буо, впечатление. </ta>
            <ta e="T764" id="Seg_5277" s="T763">– Баһыыба.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-UkOA">
            <ta e="T19" id="Seg_5278" s="T7">– Aː, bejem Nosku͡oga dʼi͡eleːk bu͡olammɨn, eː, tɨ͡aga agɨjaktɨk bu͡olabɨn bu͡o, hin biːr. </ta>
            <ta e="T25" id="Seg_5279" s="T19">Haːstarɨ, kühünneri bu͡ol bi͡ek tɨ͡aga hɨldʼabɨn. </ta>
            <ta e="T31" id="Seg_5280" s="T25">Kas dʼɨl aːjɨ inʼemeːpper, teːtemeːpper kömölöhö. </ta>
            <ta e="T40" id="Seg_5281" s="T31">Ol di͡ek, tɨ͡aga hɨldʼa bu͡olannar (gi͡e-) tabalaːktar bu͡o ol. </ta>
            <ta e="T57" id="Seg_5282" s="T40">Tabalar, tabalarɨn da körö hotoru daːganɨ onnugu bejem daːganɨ (zaž-) kimniːbin, iːtillebin eː kiminen, energʼijanan di͡ebikke di͡eri. </ta>
            <ta e="T59" id="Seg_5283" s="T57">Svʼežij vozdux. </ta>
            <ta e="T65" id="Seg_5284" s="T59">Nosku͡oga hɨtan tɨːnɨn (kaːjallar) hol kenne. </ta>
            <ta e="T90" id="Seg_5285" s="T84">– Tɨ͡ataːgɨlarɨŋ bejelere bileller bu͡o kajdak oloru͡oktarɨn. </ta>
            <ta e="T106" id="Seg_5286" s="T90">Anɨ üčügej ologu bilbet bu͡olannar, giller eː üčügejdik oloru͡opput di͡en ispetter, bɨhɨlak, olus daːganɨ bejelere daːganɨ. </ta>
            <ta e="T111" id="Seg_5287" s="T106">Tu͡ok baːr onon ü͡öre hɨldʼallar. </ta>
            <ta e="T119" id="Seg_5288" s="T111">Hu͡ok bu͡ollagɨna, kajdak gɨnɨ͡aj, hu͡ok da hu͡ok. </ta>
            <ta e="T127" id="Seg_5289" s="T123">– Olus da hanaːrgaːbattar, eː. </ta>
            <ta e="T165" id="Seg_5290" s="T127">A jeslʼi bu (üčü-) üčügejdik eː kihiliː bu ileliː oloror kihi ba, ile daːganɨ, hepsekiː hogus boloktoru admʼinʼistracɨjattan bi͡eri͡ek etilere bu͡olla, bu nʼeabxadʼimɨj, muŋ muŋ naːdalaːk ebi͡ennʼelere tu͡oktara (ki-) bu ke pomašʼ, datacɨja kördük ulakannɨk kelere bu͡ola tɨ͡ataːgɨlarga. </ta>
            <ta e="T170" id="Seg_5291" s="T165">Bejelerin, bejelerin kɨ͡aktarɨnan hɨldʼanaːktɨːllar, tɨ͡ataːgɨlarbɨt. </ta>
            <ta e="T186" id="Seg_5292" s="T170">Ol ihin anɨ össü͡ö iti tɨːhɨčča bi͡es hüːs, iti kim kam paltaraškanɨ bi͡eren bu͡olannar, ješʼo hi͡ese. </ta>
            <ta e="T191" id="Seg_5293" s="T186">Aː, a tak hapsi͡em bu͡o. </ta>
            <ta e="T210" id="Seg_5294" s="T191">Tu͡ok olus daːganɨ (ast-) ahɨnan daːganɨ, kili͡ebinen da, tugunan da, ol pʼikarnʼabɨt üleleːbet bu͡olan, bejelere buharɨnaːktɨːllar tɨ͡aga hɨldʼannar, kannɨk… </ta>
            <ta e="T228" id="Seg_5295" s="T210">Kannɨk ogo kihi "barɨ͡am" di͡ej "itiː dʼi͡etten, itiː u͡otton taksan, tɨmnɨːga, tɨ͡alga taksan, tabanɨ ketiː barɨ͡am" di͡en du͡o? </ta>
            <ta e="T233" id="Seg_5296" s="T228">Tölöːböttör daːganɨ ol oččo bu͡olla. </ta>
            <ta e="T236" id="Seg_5297" s="T233">Karčɨnɨ körböttör tɨ͡ataːgɨlar. </ta>
            <ta e="T245" id="Seg_5298" s="T236">Hanɨ pɨrsɨ͡an, tu͡ok egeleller (kamʼe-) kamʼersantar atɨːlɨːllar hürdeːk henalaːkka. </ta>
            <ta e="T256" id="Seg_5299" s="T245">Ontuŋ tu͡ok kihitej ɨlɨnan ɨlɨ͡aj duː, eː, ahɨ, ahɨ ɨlɨnɨ͡aj duː? </ta>
            <ta e="T269" id="Seg_5300" s="T256">Ahɨ ɨlɨnɨ͡aŋ duː, pɨrsɨ͡an ɨlɨnɨ͡aŋ duː, taŋas ɨlɨnɨ͡aŋ duː, ogoloruŋ össü͡ö ü͡öreni͡ekterin naːda. </ta>
            <ta e="T287" id="Seg_5301" s="T269">Tu͡ok daːganɨ anɨ bu͡ola urut studentarɨ, tu͡oktarɨ haːtar bi͡ereːčči etilere ginilerge tu͡ok ere, datacɨja bu͡ol, hapku͡ostan napravlʼajdaːččɨ etilere. </ta>
            <ta e="T293" id="Seg_5302" s="T287">Anɨ ol onnuk tu͡ok da hu͡ok. </ta>
            <ta e="T298" id="Seg_5303" s="T293">Barɨta kihi bejetin karčɨtɨnan hɨldʼar. </ta>
            <ta e="T308" id="Seg_5304" s="T298">Kim kɨ͡aga hu͡ok kihi hapsi͡em ölör di͡ebikke di͡eri hiti hanaːn. </ta>
            <ta e="T321" id="Seg_5305" s="T308">A kim kɨ͡aktaːgɨŋ hi͡ese ü͡öreteller ogolorun, hin biːr na granʼi nʼišʼetɨ olorollor. </ta>
            <ta e="T343" id="Seg_5306" s="T335">– Naj tojonnor ere, tojonnor ere kömölöhü͡öktere bihigi hirbitiger. </ta>
            <ta e="T350" id="Seg_5307" s="T343">Olopput tuksara barɨta ginilerten ere taksɨ͡aga. </ta>
            <ta e="T381" id="Seg_5308" s="T360">– (Beje-) bejebit anɨ, kihibit anɨ, kihilerbit bu͡olla v asnavnom elbek kihi eː kiːrbite tɨ͡attan kɨ͡aga hu͡ok bu͡olannar. </ta>
            <ta e="T395" id="Seg_5309" s="T381">Barɨlara ulakan opɨttaːktar tabaga hɨldʼɨːtɨgar daːganɨ eː ol barɨta, ol barɨta, ol barɨta ölör. </ta>
            <ta e="T404" id="Seg_5310" s="T395">Kihi barɨta eː, nu, hirge kaːlar, skažem, hirge kaːlar. </ta>
            <ta e="T421" id="Seg_5311" s="T404">Kihi barɨta, anɨ hɨldʼar da kihiŋ olus hiri bilbetter, ogo kihileriŋ, anɨ hɨldʼannar da, taksannar da öskötün. </ta>
            <ta e="T434" id="Seg_5312" s="T421">Ahɨlɨktaːk hirder, anɨ kɨːlbɨt anɨ hirin ularɨtan, bihiginen elbegi tüher bu͡olan, ahɨlɨkpɨtɨn baratar. </ta>
            <ta e="T446" id="Seg_5313" s="T434">Ol ihin hɨldʼar hirderbit kɨ͡araːbɨttar – tabaga daːganɨ, kak tabaga, tak kihige daːganɨ. </ta>
            <ta e="T449" id="Seg_5314" s="T446">Kihilerbit eː… </ta>
            <ta e="T456" id="Seg_5315" s="T451">– Atɨn, atɨn ologu… </ta>
            <ta e="T468" id="Seg_5316" s="T458">– Atɨn ologu, hepsekiː ologu batannar, hepsekiː onton hepsekiː bu͡olu͡o. </ta>
            <ta e="T478" id="Seg_5317" s="T468">Rɨbnajga (olor-), eː, patom, v čʼastnastʼi, Rɨbnaj tuhunan kepsiːbin onton. </ta>
            <ta e="T495" id="Seg_5318" s="T478">Rɨbnajga hi͡ese bu͡o, hibi͡etteːkter daːganɨ, ičiges daːganɨ, ol ihin (tɨ͡a-) tɨ͡ataːgɨ kihiler tɨ͡aga hɨldʼɨ͡aktarɨn olus da tartarbattar. </ta>
            <ta e="T524" id="Seg_5319" s="T495">Karčɨ olus eː, tabattan tugu ɨlɨ͡aŋɨj di͡e anɨ, ölör daː ol, ka ahɨjak bi͡ehu͡onča tabanɨ daːganɨ ölördökküne, olus da elbek karčɨnɨ ölörü͡öŋ hu͡oga ol tuhugar, ili tɨ͡aga hɨldʼaːŋŋɨn duː. </ta>
            <ta e="T540" id="Seg_5320" s="T524">Ol ihin eteŋŋe oloru͡okka i tɨ͡aga kihiler dʼolloːktuk oloru͡oktarɨn iti maŋnajgɨ ol tuhugar pravʼitʼelʼstva dumajdɨ͡agɨn naːda. </ta>
            <ta e="T543" id="Seg_5321" s="T540">Kihilerge kömö iː… </ta>
            <ta e="T559" id="Seg_5322" s="T543">Kihiler, bejelere kihiler tugu da gɨnɨ͡aktara hu͡oga, tɨ͡ataːgɨ kihiler, tʼem bolʼeje bejelerin innilerin tuhunan tugu da haŋarɨmattar. </ta>
            <ta e="T565" id="Seg_5323" s="T559">Hapku͡ostarbɨt, dʼirʼektarbɨt, dʼirʼektardar da ularɨjallar daːganɨ. </ta>
            <ta e="T581" id="Seg_5324" s="T565">Anɨ össü͡ö hi͡ese, bihi͡eke Rɨbnajga össü͡ö tura tüste eːt biːr dʼirʼektar – Mamonav Vladʼimʼir Alʼeksandravʼičʼ di͡en kihi. </ta>
            <ta e="T592" id="Seg_5325" s="T581">Ol ol tuhunan bagas tu͡ok da kuhaganɨ haŋarbappɨn, berteːk kihi, eː. </ta>
            <ta e="T610" id="Seg_5326" s="T592">Kihileri gɨtta anɨ hi͡ese karčɨlaːk bu͡olbuttar, iti kim ete, nolʼ sʼemʼ bi͡ereːčči etilere pradukcɨja gi͡enin ke bɨjɨl oŋorbuttara. </ta>
            <ta e="T613" id="Seg_5327" s="T610">Ontularɨn da ubirajdɨ͡appɨt. </ta>
            <ta e="T630" id="Seg_5328" s="T613">Onu ubiraːjdaːtaktarɨnan anɨ tɨ͡ataːgɨ eː bulčuttar da emi͡e, (ɨtɨ͡a-) (ɨtɨ͡a-) ɨtɨ͡alɨːhɨt kihiler da agɨjagɨ ɨlɨ͡aktara hin biːr. </ta>
            <ta e="T637" id="Seg_5329" s="T631">– (Hatan-) hatanɨ͡aktara hu͡oktara, heː. </ta>
            <ta e="T663" id="Seg_5330" s="T657">– Onon erenebit, eː, onon erenebit. </ta>
            <ta e="T671" id="Seg_5331" s="T663">A tak, iti karaŋa di͡ek öttütün kepsettibit bihigi. </ta>
            <ta e="T689" id="Seg_5332" s="T671">A tak da tɨ͡aga hɨrɨttakka bert beseleː ete, eː, tɨ͡ataːgɨ kihiler, anɨ ogolor hɨldʼɨbat bu͡olannar, anɨ bilbetter bu͡o. </ta>
            <ta e="T710" id="Seg_5333" s="T689">Haka tɨlɨn, čeber tɨːnnara eː halgɨnɨ, ot hɨtɨn, di͡ebikke di͡eri, maŋnaj kaːs aːjdaːnɨn, tuguttar törüːllerin, barɨtɨn tugu da olus bilbetter bu͡o. </ta>
            <ta e="T725" id="Seg_5334" s="T710">Ontuŋ barɨta bihige eː, tɨ͡aga ü͡öskeːbit kihilerge, ontuŋ barɨta, barɨta ulakan kim bu͡o, vpʼečʼatlʼenʼije. </ta>
            <ta e="T764" id="Seg_5335" s="T763">– Bahɨːba. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-UkOA">
            <ta e="T8" id="Seg_5336" s="T7">aː</ta>
            <ta e="T9" id="Seg_5337" s="T8">beje-m</ta>
            <ta e="T10" id="Seg_5338" s="T9">Nosku͡o-ga</ta>
            <ta e="T11" id="Seg_5339" s="T10">dʼi͡e-leːk</ta>
            <ta e="T12" id="Seg_5340" s="T11">bu͡ol-am-mɨn</ta>
            <ta e="T13" id="Seg_5341" s="T12">eː</ta>
            <ta e="T14" id="Seg_5342" s="T13">tɨ͡a-ga</ta>
            <ta e="T15" id="Seg_5343" s="T14">agɨjak-tɨk</ta>
            <ta e="T16" id="Seg_5344" s="T15">bu͡ol-a-bɨn</ta>
            <ta e="T17" id="Seg_5345" s="T16">bu͡o</ta>
            <ta e="T18" id="Seg_5346" s="T17">hin</ta>
            <ta e="T19" id="Seg_5347" s="T18">biːr</ta>
            <ta e="T20" id="Seg_5348" s="T19">haːs-tar-ɨ</ta>
            <ta e="T21" id="Seg_5349" s="T20">kühün-ner-i</ta>
            <ta e="T22" id="Seg_5350" s="T21">bu͡ol</ta>
            <ta e="T23" id="Seg_5351" s="T22">bi͡ek</ta>
            <ta e="T24" id="Seg_5352" s="T23">tɨ͡a-ga</ta>
            <ta e="T25" id="Seg_5353" s="T24">hɨldʼ-a-bɨn</ta>
            <ta e="T26" id="Seg_5354" s="T25">kas</ta>
            <ta e="T27" id="Seg_5355" s="T26">dʼɨl</ta>
            <ta e="T28" id="Seg_5356" s="T27">aːjɨ</ta>
            <ta e="T29" id="Seg_5357" s="T28">inʼe-m-eːp-pe-r</ta>
            <ta e="T30" id="Seg_5358" s="T29">teːte-m-eːp-pe-r</ta>
            <ta e="T31" id="Seg_5359" s="T30">kömölöh-ö</ta>
            <ta e="T32" id="Seg_5360" s="T31">ol</ta>
            <ta e="T33" id="Seg_5361" s="T32">di͡ek</ta>
            <ta e="T34" id="Seg_5362" s="T33">tɨ͡a-ga</ta>
            <ta e="T35" id="Seg_5363" s="T34">hɨldʼ-a</ta>
            <ta e="T36" id="Seg_5364" s="T35">bu͡ol-an-nar</ta>
            <ta e="T38" id="Seg_5365" s="T37">taba-laːk-tar</ta>
            <ta e="T39" id="Seg_5366" s="T38">bu͡o</ta>
            <ta e="T40" id="Seg_5367" s="T39">ol</ta>
            <ta e="T41" id="Seg_5368" s="T40">taba-lar</ta>
            <ta e="T42" id="Seg_5369" s="T41">taba-larɨ-n</ta>
            <ta e="T43" id="Seg_5370" s="T42">da</ta>
            <ta e="T44" id="Seg_5371" s="T43">kör-ö</ta>
            <ta e="T45" id="Seg_5372" s="T44">hotoru</ta>
            <ta e="T46" id="Seg_5373" s="T45">daːganɨ</ta>
            <ta e="T47" id="Seg_5374" s="T46">onnug-u</ta>
            <ta e="T48" id="Seg_5375" s="T47">beje-m</ta>
            <ta e="T49" id="Seg_5376" s="T48">daːganɨ</ta>
            <ta e="T51" id="Seg_5377" s="T50">kim-n-iː-bin</ta>
            <ta e="T52" id="Seg_5378" s="T51">iːt-i-ll-e-bin</ta>
            <ta e="T53" id="Seg_5379" s="T52">eː</ta>
            <ta e="T54" id="Seg_5380" s="T53">kim-i-nen</ta>
            <ta e="T55" id="Seg_5381" s="T54">energʼija-nan</ta>
            <ta e="T56" id="Seg_5382" s="T55">di͡e-bik-ke</ta>
            <ta e="T57" id="Seg_5383" s="T56">di͡e-ri</ta>
            <ta e="T60" id="Seg_5384" s="T59">Nosku͡o-ga</ta>
            <ta e="T61" id="Seg_5385" s="T60">hɨt-an</ta>
            <ta e="T62" id="Seg_5386" s="T61">tɨːn-ɨ-n</ta>
            <ta e="T63" id="Seg_5387" s="T62">kaːj-al-lar</ta>
            <ta e="T64" id="Seg_5388" s="T63">hol</ta>
            <ta e="T65" id="Seg_5389" s="T64">kenne</ta>
            <ta e="T85" id="Seg_5390" s="T84">tɨ͡a-taːgɨ-lar-ɨ-ŋ</ta>
            <ta e="T86" id="Seg_5391" s="T85">beje-lere</ta>
            <ta e="T87" id="Seg_5392" s="T86">bil-el-ler</ta>
            <ta e="T88" id="Seg_5393" s="T87">bu͡o</ta>
            <ta e="T89" id="Seg_5394" s="T88">kajdak</ta>
            <ta e="T90" id="Seg_5395" s="T89">olor-u͡ok-tarɨ-n</ta>
            <ta e="T91" id="Seg_5396" s="T90">anɨ</ta>
            <ta e="T92" id="Seg_5397" s="T91">üčügej</ta>
            <ta e="T93" id="Seg_5398" s="T92">olog-u</ta>
            <ta e="T94" id="Seg_5399" s="T93">bil-bet</ta>
            <ta e="T95" id="Seg_5400" s="T94">bu͡ol-an-nar</ta>
            <ta e="T96" id="Seg_5401" s="T95">giller</ta>
            <ta e="T97" id="Seg_5402" s="T96">eː</ta>
            <ta e="T98" id="Seg_5403" s="T97">üčügej-dik</ta>
            <ta e="T99" id="Seg_5404" s="T98">olor-u͡op-put</ta>
            <ta e="T100" id="Seg_5405" s="T99">di͡e-n</ta>
            <ta e="T101" id="Seg_5406" s="T100">is-pet-ter</ta>
            <ta e="T102" id="Seg_5407" s="T101">bɨhɨlaːk</ta>
            <ta e="T103" id="Seg_5408" s="T102">olus</ta>
            <ta e="T104" id="Seg_5409" s="T103">daːganɨ</ta>
            <ta e="T105" id="Seg_5410" s="T104">beje-lere</ta>
            <ta e="T106" id="Seg_5411" s="T105">daːganɨ</ta>
            <ta e="T107" id="Seg_5412" s="T106">tu͡ok</ta>
            <ta e="T108" id="Seg_5413" s="T107">baːr</ta>
            <ta e="T109" id="Seg_5414" s="T108">o-non</ta>
            <ta e="T110" id="Seg_5415" s="T109">ü͡ör-e</ta>
            <ta e="T111" id="Seg_5416" s="T110">hɨldʼ-al-lar</ta>
            <ta e="T112" id="Seg_5417" s="T111">hu͡ok</ta>
            <ta e="T113" id="Seg_5418" s="T112">bu͡ol-lag-ɨna</ta>
            <ta e="T114" id="Seg_5419" s="T113">kajdak</ta>
            <ta e="T115" id="Seg_5420" s="T114">gɨn-ɨ͡a=j</ta>
            <ta e="T116" id="Seg_5421" s="T115">hu͡ok</ta>
            <ta e="T117" id="Seg_5422" s="T116">da</ta>
            <ta e="T119" id="Seg_5423" s="T117">hu͡ok</ta>
            <ta e="T124" id="Seg_5424" s="T123">olus</ta>
            <ta e="T125" id="Seg_5425" s="T124">da</ta>
            <ta e="T126" id="Seg_5426" s="T125">hanaːrgaː-bat-tar</ta>
            <ta e="T127" id="Seg_5427" s="T126">eː</ta>
            <ta e="T128" id="Seg_5428" s="T127">a</ta>
            <ta e="T129" id="Seg_5429" s="T128">jeslʼi</ta>
            <ta e="T130" id="Seg_5430" s="T129">bu</ta>
            <ta e="T132" id="Seg_5431" s="T131">üčügej-dik</ta>
            <ta e="T133" id="Seg_5432" s="T132">eː</ta>
            <ta e="T134" id="Seg_5433" s="T133">kihi-liː</ta>
            <ta e="T135" id="Seg_5434" s="T134">bu</ta>
            <ta e="T136" id="Seg_5435" s="T135">ile-liː</ta>
            <ta e="T137" id="Seg_5436" s="T136">olor-or</ta>
            <ta e="T138" id="Seg_5437" s="T137">kihi</ta>
            <ta e="T139" id="Seg_5438" s="T138">ba</ta>
            <ta e="T140" id="Seg_5439" s="T139">ile</ta>
            <ta e="T141" id="Seg_5440" s="T140">daːganɨ</ta>
            <ta e="T142" id="Seg_5441" s="T141">hepsekiː</ta>
            <ta e="T143" id="Seg_5442" s="T142">hogus</ta>
            <ta e="T144" id="Seg_5443" s="T143">bolok-tor-u</ta>
            <ta e="T145" id="Seg_5444" s="T144">admʼinʼistracɨja-ttan</ta>
            <ta e="T146" id="Seg_5445" s="T145">bi͡er-i͡ek</ta>
            <ta e="T147" id="Seg_5446" s="T146">e-ti-lere</ta>
            <ta e="T148" id="Seg_5447" s="T147">bu͡olla</ta>
            <ta e="T149" id="Seg_5448" s="T148">bu</ta>
            <ta e="T150" id="Seg_5449" s="T149">nʼeabxadʼimɨj</ta>
            <ta e="T151" id="Seg_5450" s="T150">muŋ</ta>
            <ta e="T152" id="Seg_5451" s="T151">muŋ</ta>
            <ta e="T153" id="Seg_5452" s="T152">naːda-laːk</ta>
            <ta e="T154" id="Seg_5453" s="T153">ebi͡ennʼe-lere</ta>
            <ta e="T155" id="Seg_5454" s="T154">tu͡ok-tara</ta>
            <ta e="T157" id="Seg_5455" s="T156">bu</ta>
            <ta e="T158" id="Seg_5456" s="T157">ke</ta>
            <ta e="T159" id="Seg_5457" s="T158">pomašʼ</ta>
            <ta e="T160" id="Seg_5458" s="T159">datacɨja</ta>
            <ta e="T161" id="Seg_5459" s="T160">kördük</ta>
            <ta e="T162" id="Seg_5460" s="T161">ulakan-nɨk</ta>
            <ta e="T163" id="Seg_5461" s="T162">kel-er-e</ta>
            <ta e="T164" id="Seg_5462" s="T163">bu͡ol-a</ta>
            <ta e="T165" id="Seg_5463" s="T164">tɨ͡a-taːgɨ-lar-ga</ta>
            <ta e="T166" id="Seg_5464" s="T165">beje-leri-n</ta>
            <ta e="T167" id="Seg_5465" s="T166">beje-leri-n</ta>
            <ta e="T168" id="Seg_5466" s="T167">kɨ͡ak-tarɨ-nan</ta>
            <ta e="T169" id="Seg_5467" s="T168">hɨldʼ-a-n-aːktɨː-l-lar</ta>
            <ta e="T170" id="Seg_5468" s="T169">tɨ͡a-taːgɨ-lar-bɨt</ta>
            <ta e="T171" id="Seg_5469" s="T170">ol</ta>
            <ta e="T172" id="Seg_5470" s="T171">ihin</ta>
            <ta e="T173" id="Seg_5471" s="T172">anɨ</ta>
            <ta e="T174" id="Seg_5472" s="T173">össü͡ö</ta>
            <ta e="T175" id="Seg_5473" s="T174">iti</ta>
            <ta e="T176" id="Seg_5474" s="T175">tɨːhɨčča</ta>
            <ta e="T177" id="Seg_5475" s="T176">bi͡es</ta>
            <ta e="T178" id="Seg_5476" s="T177">hüːs</ta>
            <ta e="T179" id="Seg_5477" s="T178">iti</ta>
            <ta e="T180" id="Seg_5478" s="T179">kim</ta>
            <ta e="T181" id="Seg_5479" s="T180">kam</ta>
            <ta e="T182" id="Seg_5480" s="T181">paltaraška-nɨ</ta>
            <ta e="T183" id="Seg_5481" s="T182">bi͡er-en</ta>
            <ta e="T184" id="Seg_5482" s="T183">bu͡ol-an-nar</ta>
            <ta e="T185" id="Seg_5483" s="T184">ješʼo</ta>
            <ta e="T186" id="Seg_5484" s="T185">hi͡ese</ta>
            <ta e="T187" id="Seg_5485" s="T186">aː</ta>
            <ta e="T188" id="Seg_5486" s="T187">a</ta>
            <ta e="T189" id="Seg_5487" s="T188">tak</ta>
            <ta e="T190" id="Seg_5488" s="T189">hapsi͡em</ta>
            <ta e="T191" id="Seg_5489" s="T190">bu͡o</ta>
            <ta e="T192" id="Seg_5490" s="T191">tu͡ok</ta>
            <ta e="T193" id="Seg_5491" s="T192">olus</ta>
            <ta e="T194" id="Seg_5492" s="T193">daːganɨ</ta>
            <ta e="T196" id="Seg_5493" s="T195">ah-ɨ-nan</ta>
            <ta e="T197" id="Seg_5494" s="T196">daːganɨ</ta>
            <ta e="T198" id="Seg_5495" s="T197">kili͡eb-i-nen</ta>
            <ta e="T199" id="Seg_5496" s="T198">da</ta>
            <ta e="T200" id="Seg_5497" s="T199">tug-u-nan</ta>
            <ta e="T201" id="Seg_5498" s="T200">da</ta>
            <ta e="T202" id="Seg_5499" s="T201">ol</ta>
            <ta e="T203" id="Seg_5500" s="T202">pʼikarnʼa-bɨt</ta>
            <ta e="T204" id="Seg_5501" s="T203">üleleː-bet</ta>
            <ta e="T205" id="Seg_5502" s="T204">bu͡ol-an</ta>
            <ta e="T206" id="Seg_5503" s="T205">beje-lere</ta>
            <ta e="T207" id="Seg_5504" s="T206">buh-a-r-ɨ-n-aːktɨː-l-lar</ta>
            <ta e="T208" id="Seg_5505" s="T207">tɨ͡a-ga</ta>
            <ta e="T209" id="Seg_5506" s="T208">hɨldʼ-an-nar</ta>
            <ta e="T210" id="Seg_5507" s="T209">kannɨk</ta>
            <ta e="T211" id="Seg_5508" s="T210">kannɨk</ta>
            <ta e="T212" id="Seg_5509" s="T211">ogo</ta>
            <ta e="T213" id="Seg_5510" s="T212">kihi</ta>
            <ta e="T214" id="Seg_5511" s="T213">bar-ɨ͡a-m</ta>
            <ta e="T215" id="Seg_5512" s="T214">d-i͡e=j</ta>
            <ta e="T216" id="Seg_5513" s="T215">itiː</ta>
            <ta e="T217" id="Seg_5514" s="T216">dʼi͡e-tten</ta>
            <ta e="T218" id="Seg_5515" s="T217">itiː</ta>
            <ta e="T219" id="Seg_5516" s="T218">u͡ot-ton</ta>
            <ta e="T220" id="Seg_5517" s="T219">taks-an</ta>
            <ta e="T221" id="Seg_5518" s="T220">tɨmnɨː-ga</ta>
            <ta e="T222" id="Seg_5519" s="T221">tɨ͡al-ga</ta>
            <ta e="T223" id="Seg_5520" s="T222">taks-an</ta>
            <ta e="T224" id="Seg_5521" s="T223">taba-nɨ</ta>
            <ta e="T225" id="Seg_5522" s="T224">ket-iː</ta>
            <ta e="T226" id="Seg_5523" s="T225">bar-ɨ͡a-m</ta>
            <ta e="T227" id="Seg_5524" s="T226">di͡e-n</ta>
            <ta e="T228" id="Seg_5525" s="T227">du͡o</ta>
            <ta e="T229" id="Seg_5526" s="T228">tölöː-böt-tör</ta>
            <ta e="T230" id="Seg_5527" s="T229">daːganɨ</ta>
            <ta e="T231" id="Seg_5528" s="T230">ol</ta>
            <ta e="T232" id="Seg_5529" s="T231">oččo</ta>
            <ta e="T233" id="Seg_5530" s="T232">bu͡olla</ta>
            <ta e="T234" id="Seg_5531" s="T233">karčɨ-nɨ</ta>
            <ta e="T235" id="Seg_5532" s="T234">kör-böt-tör</ta>
            <ta e="T236" id="Seg_5533" s="T235">tɨ͡a-taːgɨ-lar</ta>
            <ta e="T237" id="Seg_5534" s="T236">hanɨ</ta>
            <ta e="T238" id="Seg_5535" s="T237">pɨrsɨ͡an</ta>
            <ta e="T239" id="Seg_5536" s="T238">tu͡ok</ta>
            <ta e="T240" id="Seg_5537" s="T239">egel-el-ler</ta>
            <ta e="T242" id="Seg_5538" s="T241">kamʼersan-tar</ta>
            <ta e="T243" id="Seg_5539" s="T242">atɨːlɨː-l-lar</ta>
            <ta e="T244" id="Seg_5540" s="T243">hürdeːk</ta>
            <ta e="T245" id="Seg_5541" s="T244">henalaːk-ka</ta>
            <ta e="T246" id="Seg_5542" s="T245">on-tu-ŋ</ta>
            <ta e="T247" id="Seg_5543" s="T246">tu͡ok</ta>
            <ta e="T248" id="Seg_5544" s="T247">kihi-te=j</ta>
            <ta e="T249" id="Seg_5545" s="T248">ɨlɨn-an</ta>
            <ta e="T250" id="Seg_5546" s="T249">ɨl-ɨ͡a=j</ta>
            <ta e="T251" id="Seg_5547" s="T250">duː</ta>
            <ta e="T252" id="Seg_5548" s="T251">eː</ta>
            <ta e="T253" id="Seg_5549" s="T252">ah-ɨ</ta>
            <ta e="T254" id="Seg_5550" s="T253">ah-ɨ</ta>
            <ta e="T255" id="Seg_5551" s="T254">ɨlɨn-ɨ͡a=j</ta>
            <ta e="T256" id="Seg_5552" s="T255">duː</ta>
            <ta e="T257" id="Seg_5553" s="T256">ah-ɨ</ta>
            <ta e="T258" id="Seg_5554" s="T257">ɨlɨn-ɨ͡a-ŋ</ta>
            <ta e="T259" id="Seg_5555" s="T258">duː</ta>
            <ta e="T260" id="Seg_5556" s="T259">pɨrsɨ͡an</ta>
            <ta e="T261" id="Seg_5557" s="T260">ɨlɨn-ɨ͡a-ŋ</ta>
            <ta e="T262" id="Seg_5558" s="T261">duː</ta>
            <ta e="T263" id="Seg_5559" s="T262">taŋas</ta>
            <ta e="T264" id="Seg_5560" s="T263">ɨlɨn-ɨ͡a-ŋ</ta>
            <ta e="T265" id="Seg_5561" s="T264">duː</ta>
            <ta e="T266" id="Seg_5562" s="T265">ogo-lor-u-ŋ</ta>
            <ta e="T267" id="Seg_5563" s="T266">össü͡ö</ta>
            <ta e="T268" id="Seg_5564" s="T267">ü͡ören-i͡ek-teri-n</ta>
            <ta e="T269" id="Seg_5565" s="T268">naːda</ta>
            <ta e="T270" id="Seg_5566" s="T269">tu͡ok</ta>
            <ta e="T271" id="Seg_5567" s="T270">daːganɨ</ta>
            <ta e="T272" id="Seg_5568" s="T271">anɨ</ta>
            <ta e="T273" id="Seg_5569" s="T272">bu͡ol-a</ta>
            <ta e="T274" id="Seg_5570" s="T273">urut</ta>
            <ta e="T275" id="Seg_5571" s="T274">studen-tar-ɨ</ta>
            <ta e="T276" id="Seg_5572" s="T275">tu͡ok-tar-ɨ</ta>
            <ta e="T277" id="Seg_5573" s="T276">haːtar</ta>
            <ta e="T278" id="Seg_5574" s="T277">bi͡er-eːčči</ta>
            <ta e="T279" id="Seg_5575" s="T278">e-ti-lere</ta>
            <ta e="T280" id="Seg_5576" s="T279">giniler-ge</ta>
            <ta e="T281" id="Seg_5577" s="T280">tu͡ok</ta>
            <ta e="T282" id="Seg_5578" s="T281">ere</ta>
            <ta e="T283" id="Seg_5579" s="T282">datacɨja</ta>
            <ta e="T284" id="Seg_5580" s="T283">bu͡ol</ta>
            <ta e="T285" id="Seg_5581" s="T284">hapku͡os-tan</ta>
            <ta e="T286" id="Seg_5582" s="T285">napravlʼaj-daː-ččɨ</ta>
            <ta e="T287" id="Seg_5583" s="T286">e-ti-lere</ta>
            <ta e="T288" id="Seg_5584" s="T287">anɨ</ta>
            <ta e="T289" id="Seg_5585" s="T288">ol</ta>
            <ta e="T290" id="Seg_5586" s="T289">onnuk</ta>
            <ta e="T291" id="Seg_5587" s="T290">tu͡ok</ta>
            <ta e="T292" id="Seg_5588" s="T291">da</ta>
            <ta e="T293" id="Seg_5589" s="T292">hu͡ok</ta>
            <ta e="T294" id="Seg_5590" s="T293">barɨ-ta</ta>
            <ta e="T295" id="Seg_5591" s="T294">kihi</ta>
            <ta e="T296" id="Seg_5592" s="T295">beje-ti-n</ta>
            <ta e="T297" id="Seg_5593" s="T296">karčɨ-tɨ-nan</ta>
            <ta e="T298" id="Seg_5594" s="T297">hɨldʼ-ar</ta>
            <ta e="T299" id="Seg_5595" s="T298">kim</ta>
            <ta e="T300" id="Seg_5596" s="T299">kɨ͡ag-a</ta>
            <ta e="T301" id="Seg_5597" s="T300">hu͡ok</ta>
            <ta e="T302" id="Seg_5598" s="T301">kihi</ta>
            <ta e="T303" id="Seg_5599" s="T302">hapsi͡em</ta>
            <ta e="T304" id="Seg_5600" s="T303">öl-ör</ta>
            <ta e="T305" id="Seg_5601" s="T304">di͡e-bik-ke</ta>
            <ta e="T306" id="Seg_5602" s="T305">di͡e-ri</ta>
            <ta e="T307" id="Seg_5603" s="T306">hiti</ta>
            <ta e="T308" id="Seg_5604" s="T307">hanaː-n</ta>
            <ta e="T309" id="Seg_5605" s="T308">a</ta>
            <ta e="T310" id="Seg_5606" s="T309">kim</ta>
            <ta e="T311" id="Seg_5607" s="T310">kɨ͡ak-taːg-ɨ-ŋ</ta>
            <ta e="T312" id="Seg_5608" s="T311">hi͡ese</ta>
            <ta e="T313" id="Seg_5609" s="T312">ü͡öret-el-ler</ta>
            <ta e="T314" id="Seg_5610" s="T313">ogo-lor-u-n</ta>
            <ta e="T315" id="Seg_5611" s="T314">hin</ta>
            <ta e="T316" id="Seg_5612" s="T315">biːr</ta>
            <ta e="T321" id="Seg_5613" s="T319">olor-ol-lor</ta>
            <ta e="T336" id="Seg_5614" s="T335">naj</ta>
            <ta e="T337" id="Seg_5615" s="T336">tojon-nor</ta>
            <ta e="T338" id="Seg_5616" s="T337">ere</ta>
            <ta e="T339" id="Seg_5617" s="T338">tojon-nor</ta>
            <ta e="T340" id="Seg_5618" s="T339">ere</ta>
            <ta e="T341" id="Seg_5619" s="T340">kömölöh-ü͡ök-tere</ta>
            <ta e="T342" id="Seg_5620" s="T341">bihigi</ta>
            <ta e="T343" id="Seg_5621" s="T342">hir-biti-ger</ta>
            <ta e="T344" id="Seg_5622" s="T343">olop-put</ta>
            <ta e="T345" id="Seg_5623" s="T344">tuks-ar-a</ta>
            <ta e="T346" id="Seg_5624" s="T345">barɨta</ta>
            <ta e="T347" id="Seg_5625" s="T346">giniler-ten</ta>
            <ta e="T348" id="Seg_5626" s="T347">ere</ta>
            <ta e="T350" id="Seg_5627" s="T348">taks-ɨ͡ag-a</ta>
            <ta e="T362" id="Seg_5628" s="T360">beje</ta>
            <ta e="T365" id="Seg_5629" s="T362">beje-bit</ta>
            <ta e="T368" id="Seg_5630" s="T365">anɨ</ta>
            <ta e="T369" id="Seg_5631" s="T368">kihi-bit</ta>
            <ta e="T370" id="Seg_5632" s="T369">anɨ</ta>
            <ta e="T371" id="Seg_5633" s="T370">kihi-ler-bit</ta>
            <ta e="T372" id="Seg_5634" s="T371">bu͡olla</ta>
            <ta e="T373" id="Seg_5635" s="T372">v asnavnom</ta>
            <ta e="T374" id="Seg_5636" s="T373">elbek</ta>
            <ta e="T375" id="Seg_5637" s="T374">kihi</ta>
            <ta e="T376" id="Seg_5638" s="T375">eː</ta>
            <ta e="T377" id="Seg_5639" s="T376">kiːr-bit-e</ta>
            <ta e="T378" id="Seg_5640" s="T377">tɨ͡a-ttan</ta>
            <ta e="T379" id="Seg_5641" s="T378">kɨ͡ag-a</ta>
            <ta e="T380" id="Seg_5642" s="T379">hu͡ok</ta>
            <ta e="T381" id="Seg_5643" s="T380">bu͡ol-an-nar</ta>
            <ta e="T382" id="Seg_5644" s="T381">barɨ-lara</ta>
            <ta e="T383" id="Seg_5645" s="T382">ulakan</ta>
            <ta e="T384" id="Seg_5646" s="T383">opɨt-taːk-tar</ta>
            <ta e="T385" id="Seg_5647" s="T384">taba-ga</ta>
            <ta e="T386" id="Seg_5648" s="T385">hɨldʼ-ɨː-tɨ-gar</ta>
            <ta e="T387" id="Seg_5649" s="T386">daːganɨ</ta>
            <ta e="T388" id="Seg_5650" s="T387">eː</ta>
            <ta e="T389" id="Seg_5651" s="T388">ol</ta>
            <ta e="T390" id="Seg_5652" s="T389">barɨ-ta</ta>
            <ta e="T391" id="Seg_5653" s="T390">ol</ta>
            <ta e="T392" id="Seg_5654" s="T391">barɨ-ta</ta>
            <ta e="T393" id="Seg_5655" s="T392">ol</ta>
            <ta e="T394" id="Seg_5656" s="T393">barɨ-ta</ta>
            <ta e="T395" id="Seg_5657" s="T394">öl-ör</ta>
            <ta e="T396" id="Seg_5658" s="T395">kihi</ta>
            <ta e="T397" id="Seg_5659" s="T396">barɨ-ta</ta>
            <ta e="T398" id="Seg_5660" s="T397">eː</ta>
            <ta e="T399" id="Seg_5661" s="T398">nu</ta>
            <ta e="T400" id="Seg_5662" s="T399">hir-ge</ta>
            <ta e="T401" id="Seg_5663" s="T400">kaːl-ar</ta>
            <ta e="T403" id="Seg_5664" s="T402">hir-ge</ta>
            <ta e="T404" id="Seg_5665" s="T403">kaːl-ar</ta>
            <ta e="T405" id="Seg_5666" s="T404">kihi</ta>
            <ta e="T406" id="Seg_5667" s="T405">barɨ-ta</ta>
            <ta e="T407" id="Seg_5668" s="T406">anɨ</ta>
            <ta e="T408" id="Seg_5669" s="T407">hɨldʼ-ar</ta>
            <ta e="T409" id="Seg_5670" s="T408">da</ta>
            <ta e="T410" id="Seg_5671" s="T409">kihi-ŋ</ta>
            <ta e="T411" id="Seg_5672" s="T410">olus</ta>
            <ta e="T412" id="Seg_5673" s="T411">hir-i</ta>
            <ta e="T413" id="Seg_5674" s="T412">bil-bet-ter</ta>
            <ta e="T414" id="Seg_5675" s="T413">ogo</ta>
            <ta e="T415" id="Seg_5676" s="T414">kihi-ler-i-ŋ</ta>
            <ta e="T416" id="Seg_5677" s="T415">anɨ</ta>
            <ta e="T417" id="Seg_5678" s="T416">hɨldʼ-an-nar</ta>
            <ta e="T418" id="Seg_5679" s="T417">da</ta>
            <ta e="T419" id="Seg_5680" s="T418">taks-an-nar</ta>
            <ta e="T420" id="Seg_5681" s="T419">da</ta>
            <ta e="T421" id="Seg_5682" s="T420">öskötün</ta>
            <ta e="T422" id="Seg_5683" s="T421">ahɨlɨk-taːk</ta>
            <ta e="T423" id="Seg_5684" s="T422">hir-der</ta>
            <ta e="T424" id="Seg_5685" s="T423">anɨ</ta>
            <ta e="T425" id="Seg_5686" s="T424">kɨːl-bɨt</ta>
            <ta e="T426" id="Seg_5687" s="T425">anɨ</ta>
            <ta e="T427" id="Seg_5688" s="T426">hir-i-n</ta>
            <ta e="T428" id="Seg_5689" s="T427">ularɨt-an</ta>
            <ta e="T429" id="Seg_5690" s="T428">bihigi-nen</ta>
            <ta e="T430" id="Seg_5691" s="T429">elbeg-i</ta>
            <ta e="T431" id="Seg_5692" s="T430">tüh-er</ta>
            <ta e="T432" id="Seg_5693" s="T431">bu͡ol-an</ta>
            <ta e="T433" id="Seg_5694" s="T432">ahɨlɨk-pɨtɨ-n</ta>
            <ta e="T434" id="Seg_5695" s="T433">bara-t-ar</ta>
            <ta e="T435" id="Seg_5696" s="T434">ol</ta>
            <ta e="T436" id="Seg_5697" s="T435">ihin</ta>
            <ta e="T437" id="Seg_5698" s="T436">hɨldʼ-ar</ta>
            <ta e="T438" id="Seg_5699" s="T437">hir-der-bit</ta>
            <ta e="T439" id="Seg_5700" s="T438">kɨ͡araː-bɨt-tar</ta>
            <ta e="T440" id="Seg_5701" s="T439">taba-ga</ta>
            <ta e="T441" id="Seg_5702" s="T440">daːganɨ</ta>
            <ta e="T442" id="Seg_5703" s="T441">kak</ta>
            <ta e="T443" id="Seg_5704" s="T442">taba-ga</ta>
            <ta e="T444" id="Seg_5705" s="T443">tak</ta>
            <ta e="T445" id="Seg_5706" s="T444">kihi-ge</ta>
            <ta e="T446" id="Seg_5707" s="T445">daːganɨ</ta>
            <ta e="T447" id="Seg_5708" s="T446">kihi-ler-bit</ta>
            <ta e="T449" id="Seg_5709" s="T447">eː</ta>
            <ta e="T453" id="Seg_5710" s="T451">atɨn</ta>
            <ta e="T454" id="Seg_5711" s="T453">atɨn</ta>
            <ta e="T456" id="Seg_5712" s="T454">olog-u</ta>
            <ta e="T460" id="Seg_5713" s="T458">atɨn</ta>
            <ta e="T461" id="Seg_5714" s="T460">olog-u</ta>
            <ta e="T462" id="Seg_5715" s="T461">hepsekiː</ta>
            <ta e="T463" id="Seg_5716" s="T462">olog-u</ta>
            <ta e="T464" id="Seg_5717" s="T463">bat-an-nar</ta>
            <ta e="T465" id="Seg_5718" s="T464">hepsekiː</ta>
            <ta e="T466" id="Seg_5719" s="T465">onton</ta>
            <ta e="T467" id="Seg_5720" s="T466">hepsekiː</ta>
            <ta e="T468" id="Seg_5721" s="T467">bu͡ol-u͡o</ta>
            <ta e="T469" id="Seg_5722" s="T468">Rɨbnaj-ga</ta>
            <ta e="T470" id="Seg_5723" s="T469">olor</ta>
            <ta e="T471" id="Seg_5724" s="T470">eː</ta>
            <ta e="T472" id="Seg_5725" s="T471">patom</ta>
            <ta e="T475" id="Seg_5726" s="T474">Rɨbnaj</ta>
            <ta e="T476" id="Seg_5727" s="T475">tuh-u-nan</ta>
            <ta e="T477" id="Seg_5728" s="T476">keps-iː-bin</ta>
            <ta e="T478" id="Seg_5729" s="T477">onton</ta>
            <ta e="T479" id="Seg_5730" s="T478">Rɨbnaj-ga</ta>
            <ta e="T480" id="Seg_5731" s="T479">hi͡ese</ta>
            <ta e="T481" id="Seg_5732" s="T480">bu͡o</ta>
            <ta e="T482" id="Seg_5733" s="T481">hibi͡et-teːk-ter</ta>
            <ta e="T483" id="Seg_5734" s="T482">daːganɨ</ta>
            <ta e="T484" id="Seg_5735" s="T483">ičiges</ta>
            <ta e="T485" id="Seg_5736" s="T484">daːganɨ</ta>
            <ta e="T486" id="Seg_5737" s="T485">ol</ta>
            <ta e="T487" id="Seg_5738" s="T486">ihin</ta>
            <ta e="T488" id="Seg_5739" s="T487">tɨ͡a</ta>
            <ta e="T489" id="Seg_5740" s="T488">tɨ͡a-taːgɨ</ta>
            <ta e="T490" id="Seg_5741" s="T489">kihi-ler</ta>
            <ta e="T491" id="Seg_5742" s="T490">tɨ͡a-ga</ta>
            <ta e="T492" id="Seg_5743" s="T491">hɨldʼ-ɨ͡ak-tarɨ-n</ta>
            <ta e="T493" id="Seg_5744" s="T492">olus</ta>
            <ta e="T494" id="Seg_5745" s="T493">da</ta>
            <ta e="T495" id="Seg_5746" s="T494">tart-a-r-bat-tar</ta>
            <ta e="T496" id="Seg_5747" s="T495">karčɨ</ta>
            <ta e="T497" id="Seg_5748" s="T496">olus</ta>
            <ta e="T498" id="Seg_5749" s="T497">eː</ta>
            <ta e="T499" id="Seg_5750" s="T498">taba-ttan</ta>
            <ta e="T500" id="Seg_5751" s="T499">tug-u</ta>
            <ta e="T501" id="Seg_5752" s="T500">ɨl-ɨ͡a-ŋ=ɨj</ta>
            <ta e="T502" id="Seg_5753" s="T501">di͡e</ta>
            <ta e="T503" id="Seg_5754" s="T502">anɨ</ta>
            <ta e="T504" id="Seg_5755" s="T503">ölör</ta>
            <ta e="T505" id="Seg_5756" s="T504">daː</ta>
            <ta e="T506" id="Seg_5757" s="T505">ol</ta>
            <ta e="T507" id="Seg_5758" s="T506">ka</ta>
            <ta e="T508" id="Seg_5759" s="T507">ahɨjak</ta>
            <ta e="T509" id="Seg_5760" s="T508">bi͡eh-u͡on-ča</ta>
            <ta e="T510" id="Seg_5761" s="T509">taba-nɨ</ta>
            <ta e="T511" id="Seg_5762" s="T510">daːganɨ</ta>
            <ta e="T512" id="Seg_5763" s="T511">ölör-dök-küne</ta>
            <ta e="T513" id="Seg_5764" s="T512">olus</ta>
            <ta e="T514" id="Seg_5765" s="T513">da</ta>
            <ta e="T515" id="Seg_5766" s="T514">elbek</ta>
            <ta e="T516" id="Seg_5767" s="T515">karčɨ-nɨ</ta>
            <ta e="T517" id="Seg_5768" s="T516">ölör-ü͡ö-ŋ</ta>
            <ta e="T518" id="Seg_5769" s="T517">hu͡og-a</ta>
            <ta e="T519" id="Seg_5770" s="T518">ol</ta>
            <ta e="T520" id="Seg_5771" s="T519">tuh-u-gar</ta>
            <ta e="T521" id="Seg_5772" s="T520">ili</ta>
            <ta e="T522" id="Seg_5773" s="T521">tɨ͡a-ga</ta>
            <ta e="T523" id="Seg_5774" s="T522">hɨldʼ-aːŋ-ŋɨn</ta>
            <ta e="T524" id="Seg_5775" s="T523">duː</ta>
            <ta e="T525" id="Seg_5776" s="T524">ol</ta>
            <ta e="T526" id="Seg_5777" s="T525">ihin</ta>
            <ta e="T527" id="Seg_5778" s="T526">eteŋŋe</ta>
            <ta e="T528" id="Seg_5779" s="T527">olor-u͡ok-ka</ta>
            <ta e="T529" id="Seg_5780" s="T528">i</ta>
            <ta e="T530" id="Seg_5781" s="T529">tɨ͡a-ga</ta>
            <ta e="T531" id="Seg_5782" s="T530">kihi-ler</ta>
            <ta e="T532" id="Seg_5783" s="T531">dʼolloːk-tuk</ta>
            <ta e="T533" id="Seg_5784" s="T532">olor-u͡ok-tarɨ-n</ta>
            <ta e="T534" id="Seg_5785" s="T533">iti</ta>
            <ta e="T535" id="Seg_5786" s="T534">maŋnajgɨ</ta>
            <ta e="T536" id="Seg_5787" s="T535">ol</ta>
            <ta e="T537" id="Seg_5788" s="T536">tuh-u-gar</ta>
            <ta e="T538" id="Seg_5789" s="T537">pravʼitʼelʼstva</ta>
            <ta e="T539" id="Seg_5790" s="T538">dumaj-d-ɨ͡ag-ɨ-n</ta>
            <ta e="T540" id="Seg_5791" s="T539">naːda</ta>
            <ta e="T541" id="Seg_5792" s="T540">kihi-ler-ge</ta>
            <ta e="T542" id="Seg_5793" s="T541">kömö</ta>
            <ta e="T543" id="Seg_5794" s="T542">iː</ta>
            <ta e="T544" id="Seg_5795" s="T543">kihi-ler</ta>
            <ta e="T545" id="Seg_5796" s="T544">beje-lere</ta>
            <ta e="T546" id="Seg_5797" s="T545">kihi-ler</ta>
            <ta e="T547" id="Seg_5798" s="T546">tug-u</ta>
            <ta e="T548" id="Seg_5799" s="T547">da</ta>
            <ta e="T549" id="Seg_5800" s="T548">gɨn-ɨ͡ak-tara</ta>
            <ta e="T550" id="Seg_5801" s="T549">hu͡og-a</ta>
            <ta e="T551" id="Seg_5802" s="T550">tɨ͡a-taːgɨ</ta>
            <ta e="T552" id="Seg_5803" s="T551">kihi-ler</ta>
            <ta e="T553" id="Seg_5804" s="T552">tʼem bolʼeje</ta>
            <ta e="T554" id="Seg_5805" s="T553">beje-leri-n</ta>
            <ta e="T555" id="Seg_5806" s="T554">inni-leri-n</ta>
            <ta e="T556" id="Seg_5807" s="T555">tuh-u-nan</ta>
            <ta e="T557" id="Seg_5808" s="T556">tug-u</ta>
            <ta e="T558" id="Seg_5809" s="T557">da</ta>
            <ta e="T559" id="Seg_5810" s="T558">haŋar-ɨ-mat-tar</ta>
            <ta e="T560" id="Seg_5811" s="T559">hapku͡os-tar-bɨt</ta>
            <ta e="T561" id="Seg_5812" s="T560">dʼirʼektar-bɨt</ta>
            <ta e="T562" id="Seg_5813" s="T561">dʼirʼektar-dar</ta>
            <ta e="T563" id="Seg_5814" s="T562">da</ta>
            <ta e="T564" id="Seg_5815" s="T563">ularɨj-al-lar</ta>
            <ta e="T565" id="Seg_5816" s="T564">daːganɨ</ta>
            <ta e="T566" id="Seg_5817" s="T565">anɨ</ta>
            <ta e="T567" id="Seg_5818" s="T566">össü͡ö</ta>
            <ta e="T568" id="Seg_5819" s="T567">hi͡ese</ta>
            <ta e="T569" id="Seg_5820" s="T568">bihi͡e-ke</ta>
            <ta e="T570" id="Seg_5821" s="T569">Rɨbnaj-ga</ta>
            <ta e="T571" id="Seg_5822" s="T570">össü͡ö</ta>
            <ta e="T572" id="Seg_5823" s="T571">tur-a</ta>
            <ta e="T573" id="Seg_5824" s="T572">tüs-t-e</ta>
            <ta e="T574" id="Seg_5825" s="T573">eːt</ta>
            <ta e="T575" id="Seg_5826" s="T574">biːr</ta>
            <ta e="T576" id="Seg_5827" s="T575">dʼirʼektar</ta>
            <ta e="T577" id="Seg_5828" s="T576">Mamonav</ta>
            <ta e="T578" id="Seg_5829" s="T577">Vladʼimʼir</ta>
            <ta e="T579" id="Seg_5830" s="T578">Alʼeksandravʼičʼ</ta>
            <ta e="T580" id="Seg_5831" s="T579">di͡e-n</ta>
            <ta e="T581" id="Seg_5832" s="T580">kihi</ta>
            <ta e="T582" id="Seg_5833" s="T581">ol</ta>
            <ta e="T583" id="Seg_5834" s="T582">ol</ta>
            <ta e="T584" id="Seg_5835" s="T583">tuh-u-nan</ta>
            <ta e="T585" id="Seg_5836" s="T584">bagas</ta>
            <ta e="T586" id="Seg_5837" s="T585">tu͡ok</ta>
            <ta e="T587" id="Seg_5838" s="T586">da</ta>
            <ta e="T588" id="Seg_5839" s="T587">kuhagan-ɨ</ta>
            <ta e="T589" id="Seg_5840" s="T588">haŋar-bap-pɨn</ta>
            <ta e="T590" id="Seg_5841" s="T589">ber-teːk</ta>
            <ta e="T591" id="Seg_5842" s="T590">kihi</ta>
            <ta e="T592" id="Seg_5843" s="T591">eː</ta>
            <ta e="T593" id="Seg_5844" s="T592">kihi-ler-i</ta>
            <ta e="T594" id="Seg_5845" s="T593">gɨtta</ta>
            <ta e="T595" id="Seg_5846" s="T594">anɨ</ta>
            <ta e="T596" id="Seg_5847" s="T595">hi͡ese</ta>
            <ta e="T597" id="Seg_5848" s="T596">karčɨ-laːk</ta>
            <ta e="T598" id="Seg_5849" s="T597">bu͡ol-but-tar</ta>
            <ta e="T599" id="Seg_5850" s="T598">iti</ta>
            <ta e="T600" id="Seg_5851" s="T599">kim</ta>
            <ta e="T601" id="Seg_5852" s="T600">e-t-e</ta>
            <ta e="T604" id="Seg_5853" s="T603">bi͡er-eːčči</ta>
            <ta e="T605" id="Seg_5854" s="T604">e-ti-lere</ta>
            <ta e="T606" id="Seg_5855" s="T605">pradukcɨja</ta>
            <ta e="T607" id="Seg_5856" s="T606">gi͡en-i-n</ta>
            <ta e="T608" id="Seg_5857" s="T607">ke</ta>
            <ta e="T609" id="Seg_5858" s="T608">bɨjɨl</ta>
            <ta e="T610" id="Seg_5859" s="T609">oŋor-but-tara</ta>
            <ta e="T611" id="Seg_5860" s="T610">on-tu-larɨ-n</ta>
            <ta e="T612" id="Seg_5861" s="T611">da</ta>
            <ta e="T613" id="Seg_5862" s="T612">ubiraj-d-ɨ͡ap-pɨt</ta>
            <ta e="T614" id="Seg_5863" s="T613">o-nu</ta>
            <ta e="T615" id="Seg_5864" s="T614">ubiraːj-daː-tak-tarɨnan</ta>
            <ta e="T616" id="Seg_5865" s="T615">anɨ</ta>
            <ta e="T617" id="Seg_5866" s="T616">tɨ͡a-taːgɨ</ta>
            <ta e="T618" id="Seg_5867" s="T617">eː</ta>
            <ta e="T619" id="Seg_5868" s="T618">bulčut-tar</ta>
            <ta e="T620" id="Seg_5869" s="T619">da</ta>
            <ta e="T621" id="Seg_5870" s="T620">emi͡e</ta>
            <ta e="T624" id="Seg_5871" s="T623">ɨt-ɨ͡al-ɨː-hɨt</ta>
            <ta e="T625" id="Seg_5872" s="T624">kihi-ler</ta>
            <ta e="T626" id="Seg_5873" s="T625">da</ta>
            <ta e="T627" id="Seg_5874" s="T626">agɨjag-ɨ</ta>
            <ta e="T628" id="Seg_5875" s="T627">ɨl-ɨ͡ak-tara</ta>
            <ta e="T629" id="Seg_5876" s="T628">hin</ta>
            <ta e="T630" id="Seg_5877" s="T629">biːr</ta>
            <ta e="T633" id="Seg_5878" s="T631">hatan</ta>
            <ta e="T635" id="Seg_5879" s="T633">hatan-ɨ͡ak-tara</ta>
            <ta e="T636" id="Seg_5880" s="T635">hu͡ok-tara</ta>
            <ta e="T637" id="Seg_5881" s="T636">heː</ta>
            <ta e="T659" id="Seg_5882" s="T657">o-non</ta>
            <ta e="T660" id="Seg_5883" s="T659">eren-e-bit</ta>
            <ta e="T661" id="Seg_5884" s="T660">eː</ta>
            <ta e="T662" id="Seg_5885" s="T661">o-non</ta>
            <ta e="T663" id="Seg_5886" s="T662">eren-e-bit</ta>
            <ta e="T664" id="Seg_5887" s="T663">a</ta>
            <ta e="T665" id="Seg_5888" s="T664">tak</ta>
            <ta e="T666" id="Seg_5889" s="T665">iti</ta>
            <ta e="T667" id="Seg_5890" s="T666">karaŋa</ta>
            <ta e="T668" id="Seg_5891" s="T667">di͡ek</ta>
            <ta e="T669" id="Seg_5892" s="T668">öttü-tü-n</ta>
            <ta e="T670" id="Seg_5893" s="T669">kepset-ti-bit</ta>
            <ta e="T671" id="Seg_5894" s="T670">bihigi</ta>
            <ta e="T672" id="Seg_5895" s="T671">a</ta>
            <ta e="T673" id="Seg_5896" s="T672">tak</ta>
            <ta e="T674" id="Seg_5897" s="T673">da</ta>
            <ta e="T675" id="Seg_5898" s="T674">tɨ͡a-ga</ta>
            <ta e="T676" id="Seg_5899" s="T675">hɨrɨt-tak-ka</ta>
            <ta e="T677" id="Seg_5900" s="T676">bert</ta>
            <ta e="T678" id="Seg_5901" s="T677">beseleː</ta>
            <ta e="T679" id="Seg_5902" s="T678">e-t-e</ta>
            <ta e="T680" id="Seg_5903" s="T679">eː</ta>
            <ta e="T681" id="Seg_5904" s="T680">tɨ͡a-taːgɨ</ta>
            <ta e="T682" id="Seg_5905" s="T681">kihi-ler</ta>
            <ta e="T683" id="Seg_5906" s="T682">anɨ</ta>
            <ta e="T684" id="Seg_5907" s="T683">ogo-lor</ta>
            <ta e="T685" id="Seg_5908" s="T684">hɨldʼ-ɨ-bat</ta>
            <ta e="T686" id="Seg_5909" s="T685">bu͡ol-an-nar</ta>
            <ta e="T687" id="Seg_5910" s="T686">anɨ</ta>
            <ta e="T688" id="Seg_5911" s="T687">bil-bet-ter</ta>
            <ta e="T689" id="Seg_5912" s="T688">bu͡o</ta>
            <ta e="T690" id="Seg_5913" s="T689">haka</ta>
            <ta e="T691" id="Seg_5914" s="T690">tɨl-ɨ-n</ta>
            <ta e="T692" id="Seg_5915" s="T691">čeber</ta>
            <ta e="T693" id="Seg_5916" s="T692">tɨːn-nara</ta>
            <ta e="T694" id="Seg_5917" s="T693">eː</ta>
            <ta e="T695" id="Seg_5918" s="T694">halgɨn-ɨ</ta>
            <ta e="T696" id="Seg_5919" s="T695">ot</ta>
            <ta e="T697" id="Seg_5920" s="T696">hɨt-ɨ-n</ta>
            <ta e="T698" id="Seg_5921" s="T697">di͡e-bik-ke</ta>
            <ta e="T699" id="Seg_5922" s="T698">di͡e-ri</ta>
            <ta e="T700" id="Seg_5923" s="T699">maŋnaj</ta>
            <ta e="T701" id="Seg_5924" s="T700">kaːs</ta>
            <ta e="T702" id="Seg_5925" s="T701">aːjdaːn-ɨ-n</ta>
            <ta e="T703" id="Seg_5926" s="T702">tugut-tar</ta>
            <ta e="T704" id="Seg_5927" s="T703">törüː-l-leri-n</ta>
            <ta e="T705" id="Seg_5928" s="T704">barɨ-tɨ-n</ta>
            <ta e="T706" id="Seg_5929" s="T705">tug-u</ta>
            <ta e="T707" id="Seg_5930" s="T706">da</ta>
            <ta e="T708" id="Seg_5931" s="T707">olus</ta>
            <ta e="T709" id="Seg_5932" s="T708">bil-bet-ter</ta>
            <ta e="T710" id="Seg_5933" s="T709">bu͡o</ta>
            <ta e="T711" id="Seg_5934" s="T710">on-tu-ŋ</ta>
            <ta e="T712" id="Seg_5935" s="T711">barɨ-ta</ta>
            <ta e="T713" id="Seg_5936" s="T712">bihi-ge</ta>
            <ta e="T714" id="Seg_5937" s="T713">eː</ta>
            <ta e="T715" id="Seg_5938" s="T714">tɨ͡a-ga</ta>
            <ta e="T716" id="Seg_5939" s="T715">ü͡öskeː-bit</ta>
            <ta e="T717" id="Seg_5940" s="T716">kihi-ler-ge</ta>
            <ta e="T718" id="Seg_5941" s="T717">on-tu-ŋ</ta>
            <ta e="T719" id="Seg_5942" s="T718">barɨ-ta</ta>
            <ta e="T720" id="Seg_5943" s="T719">barɨ-ta</ta>
            <ta e="T721" id="Seg_5944" s="T720">ulakan</ta>
            <ta e="T722" id="Seg_5945" s="T721">kim</ta>
            <ta e="T723" id="Seg_5946" s="T722">bu͡o</ta>
            <ta e="T725" id="Seg_5947" s="T723">vpʼečʼatlʼenʼije</ta>
            <ta e="T764" id="Seg_5948" s="T763">bahɨːba</ta>
         </annotation>
         <annotation name="mp" tierref="mp-UkOA">
            <ta e="T8" id="Seg_5949" s="T7">aː</ta>
            <ta e="T9" id="Seg_5950" s="T8">beje-m</ta>
            <ta e="T10" id="Seg_5951" s="T9">Nosku͡o-GA</ta>
            <ta e="T11" id="Seg_5952" s="T10">dʼi͡e-LAːK</ta>
            <ta e="T12" id="Seg_5953" s="T11">bu͡ol-An-BIn</ta>
            <ta e="T13" id="Seg_5954" s="T12">eː</ta>
            <ta e="T14" id="Seg_5955" s="T13">tɨ͡a-GA</ta>
            <ta e="T15" id="Seg_5956" s="T14">agɨjak-LIk</ta>
            <ta e="T16" id="Seg_5957" s="T15">bu͡ol-A-BIn</ta>
            <ta e="T17" id="Seg_5958" s="T16">bu͡o</ta>
            <ta e="T18" id="Seg_5959" s="T17">hin</ta>
            <ta e="T19" id="Seg_5960" s="T18">biːr</ta>
            <ta e="T20" id="Seg_5961" s="T19">haːs-LAr-nI</ta>
            <ta e="T21" id="Seg_5962" s="T20">kühün-LAr-nI</ta>
            <ta e="T22" id="Seg_5963" s="T21">bu͡ol</ta>
            <ta e="T23" id="Seg_5964" s="T22">bi͡ek</ta>
            <ta e="T24" id="Seg_5965" s="T23">tɨ͡a-GA</ta>
            <ta e="T25" id="Seg_5966" s="T24">hɨrɨt-A-BIn</ta>
            <ta e="T26" id="Seg_5967" s="T25">kas</ta>
            <ta e="T27" id="Seg_5968" s="T26">dʼɨl</ta>
            <ta e="T28" id="Seg_5969" s="T27">aːjɨ</ta>
            <ta e="T29" id="Seg_5970" s="T28">inʼe-m-LAːK-BA-r</ta>
            <ta e="T30" id="Seg_5971" s="T29">teːte-m-LAːK-BA-r</ta>
            <ta e="T31" id="Seg_5972" s="T30">kömölös-A</ta>
            <ta e="T32" id="Seg_5973" s="T31">ol</ta>
            <ta e="T33" id="Seg_5974" s="T32">dek</ta>
            <ta e="T34" id="Seg_5975" s="T33">tɨ͡a-GA</ta>
            <ta e="T35" id="Seg_5976" s="T34">hɨrɨt-A</ta>
            <ta e="T36" id="Seg_5977" s="T35">bu͡ol-An-LAr</ta>
            <ta e="T38" id="Seg_5978" s="T37">taba-LAːK-LAr</ta>
            <ta e="T39" id="Seg_5979" s="T38">bu͡o</ta>
            <ta e="T40" id="Seg_5980" s="T39">ol</ta>
            <ta e="T41" id="Seg_5981" s="T40">taba-LAr</ta>
            <ta e="T42" id="Seg_5982" s="T41">taba-LArI-n</ta>
            <ta e="T43" id="Seg_5983" s="T42">da</ta>
            <ta e="T44" id="Seg_5984" s="T43">kör-A</ta>
            <ta e="T45" id="Seg_5985" s="T44">hotoru</ta>
            <ta e="T46" id="Seg_5986" s="T45">daːganɨ</ta>
            <ta e="T47" id="Seg_5987" s="T46">onnuk-nI</ta>
            <ta e="T48" id="Seg_5988" s="T47">beje-m</ta>
            <ta e="T49" id="Seg_5989" s="T48">daːganɨ</ta>
            <ta e="T51" id="Seg_5990" s="T50">kim-LAː-A-BIn</ta>
            <ta e="T52" id="Seg_5991" s="T51">iːt-I-LIN-A-BIn</ta>
            <ta e="T53" id="Seg_5992" s="T52">eː</ta>
            <ta e="T54" id="Seg_5993" s="T53">kim-I-nAn</ta>
            <ta e="T55" id="Seg_5994" s="T54">energʼija-nAn</ta>
            <ta e="T56" id="Seg_5995" s="T55">di͡e-BIT-GA</ta>
            <ta e="T57" id="Seg_5996" s="T56">di͡e-AːrI</ta>
            <ta e="T60" id="Seg_5997" s="T59">Nosku͡o-GA</ta>
            <ta e="T61" id="Seg_5998" s="T60">hɨt-An</ta>
            <ta e="T62" id="Seg_5999" s="T61">tɨːn-tI-n</ta>
            <ta e="T63" id="Seg_6000" s="T62">kaːj-Ar-LAr</ta>
            <ta e="T64" id="Seg_6001" s="T63">hol</ta>
            <ta e="T65" id="Seg_6002" s="T64">genne</ta>
            <ta e="T85" id="Seg_6003" s="T84">tɨ͡a-LAːgI-LAr-I-ŋ</ta>
            <ta e="T86" id="Seg_6004" s="T85">beje-LArA</ta>
            <ta e="T87" id="Seg_6005" s="T86">bil-Ar-LAr</ta>
            <ta e="T88" id="Seg_6006" s="T87">bu͡o</ta>
            <ta e="T89" id="Seg_6007" s="T88">kajdak</ta>
            <ta e="T90" id="Seg_6008" s="T89">olor-IAK-LArI-n</ta>
            <ta e="T91" id="Seg_6009" s="T90">anɨ</ta>
            <ta e="T92" id="Seg_6010" s="T91">üčügej</ta>
            <ta e="T93" id="Seg_6011" s="T92">olok-nI</ta>
            <ta e="T94" id="Seg_6012" s="T93">bil-BAT</ta>
            <ta e="T95" id="Seg_6013" s="T94">bu͡ol-An-LAr</ta>
            <ta e="T96" id="Seg_6014" s="T95">giniler</ta>
            <ta e="T97" id="Seg_6015" s="T96">eː</ta>
            <ta e="T98" id="Seg_6016" s="T97">üčügej-LIk</ta>
            <ta e="T99" id="Seg_6017" s="T98">olor-IAK-BIt</ta>
            <ta e="T100" id="Seg_6018" s="T99">di͡e-An</ta>
            <ta e="T101" id="Seg_6019" s="T100">is-BAT-LAr</ta>
            <ta e="T102" id="Seg_6020" s="T101">bɨhɨːlaːk</ta>
            <ta e="T103" id="Seg_6021" s="T102">olus</ta>
            <ta e="T104" id="Seg_6022" s="T103">daːganɨ</ta>
            <ta e="T105" id="Seg_6023" s="T104">beje-LArA</ta>
            <ta e="T106" id="Seg_6024" s="T105">daːganɨ</ta>
            <ta e="T107" id="Seg_6025" s="T106">tu͡ok</ta>
            <ta e="T108" id="Seg_6026" s="T107">baːr</ta>
            <ta e="T109" id="Seg_6027" s="T108">ol-nAn</ta>
            <ta e="T110" id="Seg_6028" s="T109">ü͡ör-A</ta>
            <ta e="T111" id="Seg_6029" s="T110">hɨrɨt-Ar-LAr</ta>
            <ta e="T112" id="Seg_6030" s="T111">hu͡ok</ta>
            <ta e="T113" id="Seg_6031" s="T112">bu͡ol-TAK-InA</ta>
            <ta e="T114" id="Seg_6032" s="T113">kajdak</ta>
            <ta e="T115" id="Seg_6033" s="T114">gɨn-IAK.[tA]=Ij</ta>
            <ta e="T116" id="Seg_6034" s="T115">hu͡ok</ta>
            <ta e="T117" id="Seg_6035" s="T116">da</ta>
            <ta e="T119" id="Seg_6036" s="T117">hu͡ok</ta>
            <ta e="T124" id="Seg_6037" s="T123">olus</ta>
            <ta e="T125" id="Seg_6038" s="T124">da</ta>
            <ta e="T126" id="Seg_6039" s="T125">hanaːrgaː-BAT-LAr</ta>
            <ta e="T127" id="Seg_6040" s="T126">eː</ta>
            <ta e="T128" id="Seg_6041" s="T127">a</ta>
            <ta e="T129" id="Seg_6042" s="T128">jeslʼi</ta>
            <ta e="T130" id="Seg_6043" s="T129">bu</ta>
            <ta e="T132" id="Seg_6044" s="T131">üčügej-LIk</ta>
            <ta e="T133" id="Seg_6045" s="T132">eː</ta>
            <ta e="T134" id="Seg_6046" s="T133">kihi-LIː</ta>
            <ta e="T135" id="Seg_6047" s="T134">bu</ta>
            <ta e="T136" id="Seg_6048" s="T135">ile-LIː</ta>
            <ta e="T137" id="Seg_6049" s="T136">olor-Ar</ta>
            <ta e="T138" id="Seg_6050" s="T137">kihi</ta>
            <ta e="T139" id="Seg_6051" s="T138">bu</ta>
            <ta e="T140" id="Seg_6052" s="T139">ile</ta>
            <ta e="T141" id="Seg_6053" s="T140">daːganɨ</ta>
            <ta e="T142" id="Seg_6054" s="T141">hepsekiː</ta>
            <ta e="T143" id="Seg_6055" s="T142">hogus</ta>
            <ta e="T144" id="Seg_6056" s="T143">balok-LAr-nI</ta>
            <ta e="T145" id="Seg_6057" s="T144">admʼinʼistracɨja-ttAn</ta>
            <ta e="T146" id="Seg_6058" s="T145">bi͡er-IAK</ta>
            <ta e="T147" id="Seg_6059" s="T146">e-TI-LArA</ta>
            <ta e="T148" id="Seg_6060" s="T147">bu͡olla</ta>
            <ta e="T149" id="Seg_6061" s="T148">bu</ta>
            <ta e="T150" id="Seg_6062" s="T149">nʼeabxadʼimɨj</ta>
            <ta e="T151" id="Seg_6063" s="T150">muŋ</ta>
            <ta e="T152" id="Seg_6064" s="T151">muŋ</ta>
            <ta e="T153" id="Seg_6065" s="T152">naːda-LAːK</ta>
            <ta e="T154" id="Seg_6066" s="T153">ebi͡ennʼe-LArA</ta>
            <ta e="T155" id="Seg_6067" s="T154">tu͡ok-LArA</ta>
            <ta e="T157" id="Seg_6068" s="T156">bu</ta>
            <ta e="T158" id="Seg_6069" s="T157">ka</ta>
            <ta e="T159" id="Seg_6070" s="T158">pomašʼ</ta>
            <ta e="T160" id="Seg_6071" s="T159">datacɨja</ta>
            <ta e="T161" id="Seg_6072" s="T160">kördük</ta>
            <ta e="T162" id="Seg_6073" s="T161">ulakan-LIk</ta>
            <ta e="T163" id="Seg_6074" s="T162">kel-Ar-tA</ta>
            <ta e="T164" id="Seg_6075" s="T163">bu͡ol-A</ta>
            <ta e="T165" id="Seg_6076" s="T164">tɨ͡a-LAːgI-LAr-GA</ta>
            <ta e="T166" id="Seg_6077" s="T165">beje-LArI-n</ta>
            <ta e="T167" id="Seg_6078" s="T166">beje-LArI-n</ta>
            <ta e="T168" id="Seg_6079" s="T167">kɨ͡ak-LArI-nAn</ta>
            <ta e="T169" id="Seg_6080" s="T168">hɨrɨt-A-n-AːktAː-Ar-LAr</ta>
            <ta e="T170" id="Seg_6081" s="T169">tɨ͡a-LAːgI-LAr-BIt</ta>
            <ta e="T171" id="Seg_6082" s="T170">ol</ta>
            <ta e="T172" id="Seg_6083" s="T171">ihin</ta>
            <ta e="T173" id="Seg_6084" s="T172">anɨ</ta>
            <ta e="T174" id="Seg_6085" s="T173">össü͡ö</ta>
            <ta e="T175" id="Seg_6086" s="T174">iti</ta>
            <ta e="T176" id="Seg_6087" s="T175">tɨːhɨčča</ta>
            <ta e="T177" id="Seg_6088" s="T176">bi͡es</ta>
            <ta e="T178" id="Seg_6089" s="T177">hüːs</ta>
            <ta e="T179" id="Seg_6090" s="T178">iti</ta>
            <ta e="T180" id="Seg_6091" s="T179">kim</ta>
            <ta e="T181" id="Seg_6092" s="T180">kam</ta>
            <ta e="T182" id="Seg_6093" s="T181">paltaraška-nI</ta>
            <ta e="T183" id="Seg_6094" s="T182">bi͡er-An</ta>
            <ta e="T184" id="Seg_6095" s="T183">bu͡ol-An-LAr</ta>
            <ta e="T185" id="Seg_6096" s="T184">össü͡ö</ta>
            <ta e="T186" id="Seg_6097" s="T185">hi͡ese</ta>
            <ta e="T187" id="Seg_6098" s="T186">aː</ta>
            <ta e="T188" id="Seg_6099" s="T187">a</ta>
            <ta e="T189" id="Seg_6100" s="T188">taːk</ta>
            <ta e="T190" id="Seg_6101" s="T189">hapsi͡em</ta>
            <ta e="T191" id="Seg_6102" s="T190">bu͡o</ta>
            <ta e="T192" id="Seg_6103" s="T191">tu͡ok</ta>
            <ta e="T193" id="Seg_6104" s="T192">olus</ta>
            <ta e="T194" id="Seg_6105" s="T193">daːganɨ</ta>
            <ta e="T196" id="Seg_6106" s="T195">as-I-nAn</ta>
            <ta e="T197" id="Seg_6107" s="T196">daːganɨ</ta>
            <ta e="T198" id="Seg_6108" s="T197">keli͡ep-I-nAn</ta>
            <ta e="T199" id="Seg_6109" s="T198">da</ta>
            <ta e="T200" id="Seg_6110" s="T199">tu͡ok-I-nAn</ta>
            <ta e="T201" id="Seg_6111" s="T200">da</ta>
            <ta e="T202" id="Seg_6112" s="T201">ol</ta>
            <ta e="T203" id="Seg_6113" s="T202">pʼikarnʼa-BIt</ta>
            <ta e="T204" id="Seg_6114" s="T203">üleleː-BAT</ta>
            <ta e="T205" id="Seg_6115" s="T204">bu͡ol-An</ta>
            <ta e="T206" id="Seg_6116" s="T205">beje-LArA</ta>
            <ta e="T207" id="Seg_6117" s="T206">bus-A-r-I-n-AːktAː-Ar-LAr</ta>
            <ta e="T208" id="Seg_6118" s="T207">tɨ͡a-GA</ta>
            <ta e="T209" id="Seg_6119" s="T208">hɨrɨt-An-LAr</ta>
            <ta e="T210" id="Seg_6120" s="T209">kannɨk</ta>
            <ta e="T211" id="Seg_6121" s="T210">kannɨk</ta>
            <ta e="T212" id="Seg_6122" s="T211">ogo</ta>
            <ta e="T213" id="Seg_6123" s="T212">kihi</ta>
            <ta e="T214" id="Seg_6124" s="T213">bar-IAK-m</ta>
            <ta e="T215" id="Seg_6125" s="T214">di͡e-IAK.[tA]=Ij</ta>
            <ta e="T216" id="Seg_6126" s="T215">itiː</ta>
            <ta e="T217" id="Seg_6127" s="T216">dʼi͡e-ttAn</ta>
            <ta e="T218" id="Seg_6128" s="T217">itiː</ta>
            <ta e="T219" id="Seg_6129" s="T218">u͡ot-ttAn</ta>
            <ta e="T220" id="Seg_6130" s="T219">tagɨs-An</ta>
            <ta e="T221" id="Seg_6131" s="T220">tɨmnɨː-GA</ta>
            <ta e="T222" id="Seg_6132" s="T221">tɨ͡al-GA</ta>
            <ta e="T223" id="Seg_6133" s="T222">tagɨs-An</ta>
            <ta e="T224" id="Seg_6134" s="T223">taba-nI</ta>
            <ta e="T225" id="Seg_6135" s="T224">keteː-A</ta>
            <ta e="T226" id="Seg_6136" s="T225">bar-IAK-m</ta>
            <ta e="T227" id="Seg_6137" s="T226">di͡e-An</ta>
            <ta e="T228" id="Seg_6138" s="T227">du͡o</ta>
            <ta e="T229" id="Seg_6139" s="T228">tölöː-BAT-LAr</ta>
            <ta e="T230" id="Seg_6140" s="T229">daːganɨ</ta>
            <ta e="T231" id="Seg_6141" s="T230">ol</ta>
            <ta e="T232" id="Seg_6142" s="T231">oččogo</ta>
            <ta e="T233" id="Seg_6143" s="T232">bu͡olla</ta>
            <ta e="T234" id="Seg_6144" s="T233">karčɨ-nI</ta>
            <ta e="T235" id="Seg_6145" s="T234">kör-BAT-LAr</ta>
            <ta e="T236" id="Seg_6146" s="T235">tɨ͡a-LAːgI-LAr</ta>
            <ta e="T237" id="Seg_6147" s="T236">hannɨ</ta>
            <ta e="T238" id="Seg_6148" s="T237">pɨrsɨ͡an</ta>
            <ta e="T239" id="Seg_6149" s="T238">tu͡ok</ta>
            <ta e="T240" id="Seg_6150" s="T239">egel-Ar-LAr</ta>
            <ta e="T242" id="Seg_6151" s="T241">kamʼersant-LAr</ta>
            <ta e="T243" id="Seg_6152" s="T242">atɨːlaː-Ar-LAr</ta>
            <ta e="T244" id="Seg_6153" s="T243">hürdeːk</ta>
            <ta e="T245" id="Seg_6154" s="T244">hɨ͡analaːk-GA</ta>
            <ta e="T246" id="Seg_6155" s="T245">ol-tI-ŋ</ta>
            <ta e="T247" id="Seg_6156" s="T246">tu͡ok</ta>
            <ta e="T248" id="Seg_6157" s="T247">kihi-tA=Ij</ta>
            <ta e="T249" id="Seg_6158" s="T248">ɨlɨn-An</ta>
            <ta e="T250" id="Seg_6159" s="T249">ɨl-IAK.[tA]=Ij</ta>
            <ta e="T251" id="Seg_6160" s="T250">du͡o</ta>
            <ta e="T252" id="Seg_6161" s="T251">eː</ta>
            <ta e="T253" id="Seg_6162" s="T252">as-nI</ta>
            <ta e="T254" id="Seg_6163" s="T253">as-nI</ta>
            <ta e="T255" id="Seg_6164" s="T254">ɨlɨn-IAK.[tA]=Ij</ta>
            <ta e="T256" id="Seg_6165" s="T255">du͡o</ta>
            <ta e="T257" id="Seg_6166" s="T256">as-nI</ta>
            <ta e="T258" id="Seg_6167" s="T257">ɨlɨn-IAK-ŋ</ta>
            <ta e="T259" id="Seg_6168" s="T258">du͡o</ta>
            <ta e="T260" id="Seg_6169" s="T259">pɨrsɨ͡an</ta>
            <ta e="T261" id="Seg_6170" s="T260">ɨlɨn-IAK-ŋ</ta>
            <ta e="T262" id="Seg_6171" s="T261">du͡o</ta>
            <ta e="T263" id="Seg_6172" s="T262">taŋas</ta>
            <ta e="T264" id="Seg_6173" s="T263">ɨlɨn-IAK-ŋ</ta>
            <ta e="T265" id="Seg_6174" s="T264">du͡o</ta>
            <ta e="T266" id="Seg_6175" s="T265">ogo-LAr-I-ŋ</ta>
            <ta e="T267" id="Seg_6176" s="T266">össü͡ö</ta>
            <ta e="T268" id="Seg_6177" s="T267">ü͡ören-IAK-LArI-n</ta>
            <ta e="T269" id="Seg_6178" s="T268">naːda</ta>
            <ta e="T270" id="Seg_6179" s="T269">tu͡ok</ta>
            <ta e="T271" id="Seg_6180" s="T270">daːganɨ</ta>
            <ta e="T272" id="Seg_6181" s="T271">anɨ</ta>
            <ta e="T273" id="Seg_6182" s="T272">bu͡ol-A</ta>
            <ta e="T274" id="Seg_6183" s="T273">urut</ta>
            <ta e="T275" id="Seg_6184" s="T274">student-LAr-nI</ta>
            <ta e="T276" id="Seg_6185" s="T275">tu͡ok-LAr-nI</ta>
            <ta e="T277" id="Seg_6186" s="T276">haːtar</ta>
            <ta e="T278" id="Seg_6187" s="T277">bi͡er-AːččI</ta>
            <ta e="T279" id="Seg_6188" s="T278">e-TI-LArA</ta>
            <ta e="T280" id="Seg_6189" s="T279">giniler-GA</ta>
            <ta e="T281" id="Seg_6190" s="T280">tu͡ok</ta>
            <ta e="T282" id="Seg_6191" s="T281">ere</ta>
            <ta e="T283" id="Seg_6192" s="T282">datacɨja</ta>
            <ta e="T284" id="Seg_6193" s="T283">bu͡ol</ta>
            <ta e="T285" id="Seg_6194" s="T284">hopku͡os-ttAn</ta>
            <ta e="T286" id="Seg_6195" s="T285">napravlʼaj-LAː-AːččI</ta>
            <ta e="T287" id="Seg_6196" s="T286">e-TI-LArA</ta>
            <ta e="T288" id="Seg_6197" s="T287">anɨ</ta>
            <ta e="T289" id="Seg_6198" s="T288">ol</ta>
            <ta e="T290" id="Seg_6199" s="T289">onnuk</ta>
            <ta e="T291" id="Seg_6200" s="T290">tu͡ok</ta>
            <ta e="T292" id="Seg_6201" s="T291">da</ta>
            <ta e="T293" id="Seg_6202" s="T292">hu͡ok</ta>
            <ta e="T294" id="Seg_6203" s="T293">barɨ-tA</ta>
            <ta e="T295" id="Seg_6204" s="T294">kihi</ta>
            <ta e="T296" id="Seg_6205" s="T295">beje-tI-n</ta>
            <ta e="T297" id="Seg_6206" s="T296">karčɨ-tI-nAn</ta>
            <ta e="T298" id="Seg_6207" s="T297">hɨrɨt-Ar</ta>
            <ta e="T299" id="Seg_6208" s="T298">kim</ta>
            <ta e="T300" id="Seg_6209" s="T299">kɨ͡ak-tA</ta>
            <ta e="T301" id="Seg_6210" s="T300">hu͡ok</ta>
            <ta e="T302" id="Seg_6211" s="T301">kihi</ta>
            <ta e="T303" id="Seg_6212" s="T302">hapsi͡em</ta>
            <ta e="T304" id="Seg_6213" s="T303">öl-Ar</ta>
            <ta e="T305" id="Seg_6214" s="T304">di͡e-BIT-GA</ta>
            <ta e="T306" id="Seg_6215" s="T305">di͡e-AːrI</ta>
            <ta e="T307" id="Seg_6216" s="T306">hiti</ta>
            <ta e="T308" id="Seg_6217" s="T307">hanaː-An</ta>
            <ta e="T309" id="Seg_6218" s="T308">a</ta>
            <ta e="T310" id="Seg_6219" s="T309">kim</ta>
            <ta e="T311" id="Seg_6220" s="T310">kɨ͡ak-LAːK-I-ŋ</ta>
            <ta e="T312" id="Seg_6221" s="T311">hi͡ese</ta>
            <ta e="T313" id="Seg_6222" s="T312">ü͡öret-Ar-LAr</ta>
            <ta e="T314" id="Seg_6223" s="T313">ogo-LAr-tI-n</ta>
            <ta e="T315" id="Seg_6224" s="T314">hin</ta>
            <ta e="T316" id="Seg_6225" s="T315">biːr</ta>
            <ta e="T321" id="Seg_6226" s="T319">olor-Ar-LAr</ta>
            <ta e="T336" id="Seg_6227" s="T335">na</ta>
            <ta e="T337" id="Seg_6228" s="T336">tojon-LAr</ta>
            <ta e="T338" id="Seg_6229" s="T337">ere</ta>
            <ta e="T339" id="Seg_6230" s="T338">tojon-LAr</ta>
            <ta e="T340" id="Seg_6231" s="T339">ere</ta>
            <ta e="T341" id="Seg_6232" s="T340">kömölös-IAK-LArA</ta>
            <ta e="T342" id="Seg_6233" s="T341">bihigi</ta>
            <ta e="T343" id="Seg_6234" s="T342">hir-BItI-GAr</ta>
            <ta e="T344" id="Seg_6235" s="T343">olok-BIt</ta>
            <ta e="T345" id="Seg_6236" s="T344">tugus-Ar-tA</ta>
            <ta e="T346" id="Seg_6237" s="T345">barɨta</ta>
            <ta e="T347" id="Seg_6238" s="T346">giniler-ttAn</ta>
            <ta e="T348" id="Seg_6239" s="T347">ere</ta>
            <ta e="T350" id="Seg_6240" s="T348">tagɨs-IAK-tA</ta>
            <ta e="T362" id="Seg_6241" s="T360">beje</ta>
            <ta e="T365" id="Seg_6242" s="T362">beje-BIt</ta>
            <ta e="T368" id="Seg_6243" s="T365">anɨ</ta>
            <ta e="T369" id="Seg_6244" s="T368">kihi-BIt</ta>
            <ta e="T370" id="Seg_6245" s="T369">anɨ</ta>
            <ta e="T371" id="Seg_6246" s="T370">kihi-LAr-BIt</ta>
            <ta e="T372" id="Seg_6247" s="T371">bu͡olla</ta>
            <ta e="T373" id="Seg_6248" s="T372">v asnavnom</ta>
            <ta e="T374" id="Seg_6249" s="T373">elbek</ta>
            <ta e="T375" id="Seg_6250" s="T374">kihi</ta>
            <ta e="T376" id="Seg_6251" s="T375">eː</ta>
            <ta e="T377" id="Seg_6252" s="T376">kiːr-BIT-tA</ta>
            <ta e="T378" id="Seg_6253" s="T377">tɨ͡a-ttAn</ta>
            <ta e="T379" id="Seg_6254" s="T378">kɨ͡ak-tA</ta>
            <ta e="T380" id="Seg_6255" s="T379">hu͡ok</ta>
            <ta e="T381" id="Seg_6256" s="T380">bu͡ol-An-LAr</ta>
            <ta e="T382" id="Seg_6257" s="T381">barɨ-LArA</ta>
            <ta e="T383" id="Seg_6258" s="T382">ulakan</ta>
            <ta e="T384" id="Seg_6259" s="T383">opɨt-LAːK-LAr</ta>
            <ta e="T385" id="Seg_6260" s="T384">taba-GA</ta>
            <ta e="T386" id="Seg_6261" s="T385">hɨrɨt-Iː-tI-GAr</ta>
            <ta e="T387" id="Seg_6262" s="T386">daːganɨ</ta>
            <ta e="T388" id="Seg_6263" s="T387">eː</ta>
            <ta e="T389" id="Seg_6264" s="T388">ol</ta>
            <ta e="T390" id="Seg_6265" s="T389">barɨ-tA</ta>
            <ta e="T391" id="Seg_6266" s="T390">ol</ta>
            <ta e="T392" id="Seg_6267" s="T391">barɨ-tA</ta>
            <ta e="T393" id="Seg_6268" s="T392">ol</ta>
            <ta e="T394" id="Seg_6269" s="T393">barɨ-tA</ta>
            <ta e="T395" id="Seg_6270" s="T394">öl-Ar</ta>
            <ta e="T396" id="Seg_6271" s="T395">kihi</ta>
            <ta e="T397" id="Seg_6272" s="T396">barɨ-tA</ta>
            <ta e="T398" id="Seg_6273" s="T397">eː</ta>
            <ta e="T399" id="Seg_6274" s="T398">nu</ta>
            <ta e="T400" id="Seg_6275" s="T399">hir-GA</ta>
            <ta e="T401" id="Seg_6276" s="T400">kaːl-Ar</ta>
            <ta e="T403" id="Seg_6277" s="T402">hir-GA</ta>
            <ta e="T404" id="Seg_6278" s="T403">kaːl-Ar</ta>
            <ta e="T405" id="Seg_6279" s="T404">kihi</ta>
            <ta e="T406" id="Seg_6280" s="T405">barɨ-tA</ta>
            <ta e="T407" id="Seg_6281" s="T406">anɨ</ta>
            <ta e="T408" id="Seg_6282" s="T407">hɨrɨt-Ar</ta>
            <ta e="T409" id="Seg_6283" s="T408">da</ta>
            <ta e="T410" id="Seg_6284" s="T409">kihi-ŋ</ta>
            <ta e="T411" id="Seg_6285" s="T410">olus</ta>
            <ta e="T412" id="Seg_6286" s="T411">hir-nI</ta>
            <ta e="T413" id="Seg_6287" s="T412">bil-BAT-LAr</ta>
            <ta e="T414" id="Seg_6288" s="T413">ogo</ta>
            <ta e="T415" id="Seg_6289" s="T414">kihi-LAr-I-ŋ</ta>
            <ta e="T416" id="Seg_6290" s="T415">anɨ</ta>
            <ta e="T417" id="Seg_6291" s="T416">hɨrɨt-An-LAr</ta>
            <ta e="T418" id="Seg_6292" s="T417">da</ta>
            <ta e="T419" id="Seg_6293" s="T418">tagɨs-An-LAr</ta>
            <ta e="T420" id="Seg_6294" s="T419">da</ta>
            <ta e="T421" id="Seg_6295" s="T420">öskötün</ta>
            <ta e="T422" id="Seg_6296" s="T421">ahɨlɨk-LAːK</ta>
            <ta e="T423" id="Seg_6297" s="T422">hir-LAr</ta>
            <ta e="T424" id="Seg_6298" s="T423">anɨ</ta>
            <ta e="T425" id="Seg_6299" s="T424">kɨːl-BIt</ta>
            <ta e="T426" id="Seg_6300" s="T425">anɨ</ta>
            <ta e="T427" id="Seg_6301" s="T426">hir-tI-n</ta>
            <ta e="T428" id="Seg_6302" s="T427">ularɨt-An</ta>
            <ta e="T429" id="Seg_6303" s="T428">bihigi-nAn</ta>
            <ta e="T430" id="Seg_6304" s="T429">elbek-nI</ta>
            <ta e="T431" id="Seg_6305" s="T430">tüs-Ar</ta>
            <ta e="T432" id="Seg_6306" s="T431">bu͡ol-An</ta>
            <ta e="T433" id="Seg_6307" s="T432">ahɨlɨk-BItI-n</ta>
            <ta e="T434" id="Seg_6308" s="T433">baraː-t-Ar</ta>
            <ta e="T435" id="Seg_6309" s="T434">ol</ta>
            <ta e="T436" id="Seg_6310" s="T435">ihin</ta>
            <ta e="T437" id="Seg_6311" s="T436">hɨrɨt-Ar</ta>
            <ta e="T438" id="Seg_6312" s="T437">hir-LAr-BIt</ta>
            <ta e="T439" id="Seg_6313" s="T438">kɨ͡araː-BIT-LAr</ta>
            <ta e="T440" id="Seg_6314" s="T439">taba-GA</ta>
            <ta e="T441" id="Seg_6315" s="T440">daːganɨ</ta>
            <ta e="T442" id="Seg_6316" s="T441">kaːk</ta>
            <ta e="T443" id="Seg_6317" s="T442">taba-GA</ta>
            <ta e="T444" id="Seg_6318" s="T443">taːk</ta>
            <ta e="T445" id="Seg_6319" s="T444">kihi-GA</ta>
            <ta e="T446" id="Seg_6320" s="T445">daːganɨ</ta>
            <ta e="T447" id="Seg_6321" s="T446">kihi-LAr-BIt</ta>
            <ta e="T449" id="Seg_6322" s="T447">eː</ta>
            <ta e="T453" id="Seg_6323" s="T451">atɨn</ta>
            <ta e="T454" id="Seg_6324" s="T453">atɨn</ta>
            <ta e="T456" id="Seg_6325" s="T454">olok-nI</ta>
            <ta e="T460" id="Seg_6326" s="T458">atɨn</ta>
            <ta e="T461" id="Seg_6327" s="T460">olok-nI</ta>
            <ta e="T462" id="Seg_6328" s="T461">hepsekiː</ta>
            <ta e="T463" id="Seg_6329" s="T462">olok-nI</ta>
            <ta e="T464" id="Seg_6330" s="T463">bat-An-LAr</ta>
            <ta e="T465" id="Seg_6331" s="T464">hepsekiː</ta>
            <ta e="T466" id="Seg_6332" s="T465">onton</ta>
            <ta e="T467" id="Seg_6333" s="T466">hepsekiː</ta>
            <ta e="T468" id="Seg_6334" s="T467">bu͡ol-IAK.[tA]</ta>
            <ta e="T469" id="Seg_6335" s="T468">Rɨmnaj-GA</ta>
            <ta e="T470" id="Seg_6336" s="T469">olor</ta>
            <ta e="T471" id="Seg_6337" s="T470">eː</ta>
            <ta e="T472" id="Seg_6338" s="T471">patom</ta>
            <ta e="T475" id="Seg_6339" s="T474">Rɨmnaj</ta>
            <ta e="T476" id="Seg_6340" s="T475">tus-tI-nAn</ta>
            <ta e="T477" id="Seg_6341" s="T476">kepseː-A-BIn</ta>
            <ta e="T478" id="Seg_6342" s="T477">onton</ta>
            <ta e="T479" id="Seg_6343" s="T478">Rɨmnaj-GA</ta>
            <ta e="T480" id="Seg_6344" s="T479">hi͡ese</ta>
            <ta e="T481" id="Seg_6345" s="T480">bu͡o</ta>
            <ta e="T482" id="Seg_6346" s="T481">hibi͡et-LAːK-LAr</ta>
            <ta e="T483" id="Seg_6347" s="T482">daːganɨ</ta>
            <ta e="T484" id="Seg_6348" s="T483">ičiges</ta>
            <ta e="T485" id="Seg_6349" s="T484">daːganɨ</ta>
            <ta e="T486" id="Seg_6350" s="T485">ol</ta>
            <ta e="T487" id="Seg_6351" s="T486">ihin</ta>
            <ta e="T488" id="Seg_6352" s="T487">tɨ͡a</ta>
            <ta e="T489" id="Seg_6353" s="T488">tɨ͡a-LAːgI</ta>
            <ta e="T490" id="Seg_6354" s="T489">kihi-LAr</ta>
            <ta e="T491" id="Seg_6355" s="T490">tɨ͡a-GA</ta>
            <ta e="T492" id="Seg_6356" s="T491">hɨrɨt-IAK-LArI-n</ta>
            <ta e="T493" id="Seg_6357" s="T492">olus</ta>
            <ta e="T494" id="Seg_6358" s="T493">da</ta>
            <ta e="T495" id="Seg_6359" s="T494">tart-A-r-BAT-LAr</ta>
            <ta e="T496" id="Seg_6360" s="T495">karčɨ</ta>
            <ta e="T497" id="Seg_6361" s="T496">olus</ta>
            <ta e="T498" id="Seg_6362" s="T497">eː</ta>
            <ta e="T499" id="Seg_6363" s="T498">taba-ttAn</ta>
            <ta e="T500" id="Seg_6364" s="T499">tu͡ok-nI</ta>
            <ta e="T501" id="Seg_6365" s="T500">ɨl-IAK-ŋ=Ij</ta>
            <ta e="T502" id="Seg_6366" s="T501">dʼe</ta>
            <ta e="T503" id="Seg_6367" s="T502">anɨ</ta>
            <ta e="T504" id="Seg_6368" s="T503">ölör</ta>
            <ta e="T505" id="Seg_6369" s="T504">da</ta>
            <ta e="T506" id="Seg_6370" s="T505">ol</ta>
            <ta e="T507" id="Seg_6371" s="T506">ka</ta>
            <ta e="T508" id="Seg_6372" s="T507">agɨjak</ta>
            <ta e="T509" id="Seg_6373" s="T508">bi͡es-u͡on-ččA</ta>
            <ta e="T510" id="Seg_6374" s="T509">taba-nI</ta>
            <ta e="T511" id="Seg_6375" s="T510">daːganɨ</ta>
            <ta e="T512" id="Seg_6376" s="T511">ölör-TAK-GInA</ta>
            <ta e="T513" id="Seg_6377" s="T512">olus</ta>
            <ta e="T514" id="Seg_6378" s="T513">da</ta>
            <ta e="T515" id="Seg_6379" s="T514">elbek</ta>
            <ta e="T516" id="Seg_6380" s="T515">karčɨ-nI</ta>
            <ta e="T517" id="Seg_6381" s="T516">ölör-IAK-ŋ</ta>
            <ta e="T518" id="Seg_6382" s="T517">hu͡ok-tA</ta>
            <ta e="T519" id="Seg_6383" s="T518">ol</ta>
            <ta e="T520" id="Seg_6384" s="T519">tus-tI-GAr</ta>
            <ta e="T521" id="Seg_6385" s="T520">ili</ta>
            <ta e="T522" id="Seg_6386" s="T521">tɨ͡a-GA</ta>
            <ta e="T523" id="Seg_6387" s="T522">hɨrɨt-An-GIn</ta>
            <ta e="T524" id="Seg_6388" s="T523">du͡o</ta>
            <ta e="T525" id="Seg_6389" s="T524">ol</ta>
            <ta e="T526" id="Seg_6390" s="T525">ihin</ta>
            <ta e="T527" id="Seg_6391" s="T526">eteŋŋe</ta>
            <ta e="T528" id="Seg_6392" s="T527">olor-IAK-GA</ta>
            <ta e="T529" id="Seg_6393" s="T528">i</ta>
            <ta e="T530" id="Seg_6394" s="T529">tɨ͡a-GA</ta>
            <ta e="T531" id="Seg_6395" s="T530">kihi-LAr</ta>
            <ta e="T532" id="Seg_6396" s="T531">dʼolloːk-LIk</ta>
            <ta e="T533" id="Seg_6397" s="T532">olor-IAK-LArI-n</ta>
            <ta e="T534" id="Seg_6398" s="T533">iti</ta>
            <ta e="T535" id="Seg_6399" s="T534">maŋnajgɨ</ta>
            <ta e="T536" id="Seg_6400" s="T535">ol</ta>
            <ta e="T537" id="Seg_6401" s="T536">tus-tI-GAr</ta>
            <ta e="T538" id="Seg_6402" s="T537">pravʼitʼelʼstva</ta>
            <ta e="T539" id="Seg_6403" s="T538">dumaj-LAː-IAK-tI-n</ta>
            <ta e="T540" id="Seg_6404" s="T539">naːda</ta>
            <ta e="T541" id="Seg_6405" s="T540">kihi-LAr-GA</ta>
            <ta e="T542" id="Seg_6406" s="T541">kömö</ta>
            <ta e="T543" id="Seg_6407" s="T542">i</ta>
            <ta e="T544" id="Seg_6408" s="T543">kihi-LAr</ta>
            <ta e="T545" id="Seg_6409" s="T544">beje-LArA</ta>
            <ta e="T546" id="Seg_6410" s="T545">kihi-LAr</ta>
            <ta e="T547" id="Seg_6411" s="T546">tu͡ok-nI</ta>
            <ta e="T548" id="Seg_6412" s="T547">da</ta>
            <ta e="T549" id="Seg_6413" s="T548">gɨn-IAK-LArA</ta>
            <ta e="T550" id="Seg_6414" s="T549">hu͡ok-tA</ta>
            <ta e="T551" id="Seg_6415" s="T550">tɨ͡a-LAːgI</ta>
            <ta e="T552" id="Seg_6416" s="T551">kihi-LAr</ta>
            <ta e="T553" id="Seg_6417" s="T552">tʼem bolʼeje</ta>
            <ta e="T554" id="Seg_6418" s="T553">beje-LArI-n</ta>
            <ta e="T555" id="Seg_6419" s="T554">ilin-LArI-n</ta>
            <ta e="T556" id="Seg_6420" s="T555">tus-tI-nAn</ta>
            <ta e="T557" id="Seg_6421" s="T556">tu͡ok-nI</ta>
            <ta e="T558" id="Seg_6422" s="T557">da</ta>
            <ta e="T559" id="Seg_6423" s="T558">haŋar-I-BAT-LAr</ta>
            <ta e="T560" id="Seg_6424" s="T559">hopku͡os-LAr-BIt</ta>
            <ta e="T561" id="Seg_6425" s="T560">dʼirʼektar-BIt</ta>
            <ta e="T562" id="Seg_6426" s="T561">dʼirʼektar-LAr</ta>
            <ta e="T563" id="Seg_6427" s="T562">da</ta>
            <ta e="T564" id="Seg_6428" s="T563">ularɨj-Ar-LAr</ta>
            <ta e="T565" id="Seg_6429" s="T564">daːganɨ</ta>
            <ta e="T566" id="Seg_6430" s="T565">anɨ</ta>
            <ta e="T567" id="Seg_6431" s="T566">össü͡ö</ta>
            <ta e="T568" id="Seg_6432" s="T567">hi͡ese</ta>
            <ta e="T569" id="Seg_6433" s="T568">bihigi-GA</ta>
            <ta e="T570" id="Seg_6434" s="T569">Rɨmnaj-GA</ta>
            <ta e="T571" id="Seg_6435" s="T570">össü͡ö</ta>
            <ta e="T572" id="Seg_6436" s="T571">tur-A</ta>
            <ta e="T573" id="Seg_6437" s="T572">tüs-TI-tA</ta>
            <ta e="T574" id="Seg_6438" s="T573">eːt</ta>
            <ta e="T575" id="Seg_6439" s="T574">biːr</ta>
            <ta e="T576" id="Seg_6440" s="T575">dʼirʼektar</ta>
            <ta e="T577" id="Seg_6441" s="T576">Mamonav</ta>
            <ta e="T578" id="Seg_6442" s="T577">Vladʼimʼir</ta>
            <ta e="T579" id="Seg_6443" s="T578">Alʼeksandravʼičʼ</ta>
            <ta e="T580" id="Seg_6444" s="T579">di͡e-An</ta>
            <ta e="T581" id="Seg_6445" s="T580">kihi</ta>
            <ta e="T582" id="Seg_6446" s="T581">ol</ta>
            <ta e="T583" id="Seg_6447" s="T582">ol</ta>
            <ta e="T584" id="Seg_6448" s="T583">tus-tI-nAn</ta>
            <ta e="T585" id="Seg_6449" s="T584">bagas</ta>
            <ta e="T586" id="Seg_6450" s="T585">tu͡ok</ta>
            <ta e="T587" id="Seg_6451" s="T586">da</ta>
            <ta e="T588" id="Seg_6452" s="T587">kuhagan-nI</ta>
            <ta e="T589" id="Seg_6453" s="T588">haŋar-BAT-BIn</ta>
            <ta e="T590" id="Seg_6454" s="T589">bert-LAːK</ta>
            <ta e="T591" id="Seg_6455" s="T590">kihi</ta>
            <ta e="T592" id="Seg_6456" s="T591">eː</ta>
            <ta e="T593" id="Seg_6457" s="T592">kihi-LAr-nI</ta>
            <ta e="T594" id="Seg_6458" s="T593">kɨtta</ta>
            <ta e="T595" id="Seg_6459" s="T594">anɨ</ta>
            <ta e="T596" id="Seg_6460" s="T595">hi͡ese</ta>
            <ta e="T597" id="Seg_6461" s="T596">karčɨ-LAːK</ta>
            <ta e="T598" id="Seg_6462" s="T597">bu͡ol-BIT-LAr</ta>
            <ta e="T599" id="Seg_6463" s="T598">iti</ta>
            <ta e="T600" id="Seg_6464" s="T599">kim</ta>
            <ta e="T601" id="Seg_6465" s="T600">e-TI-tA</ta>
            <ta e="T604" id="Seg_6466" s="T603">bi͡er-AːččI</ta>
            <ta e="T605" id="Seg_6467" s="T604">e-TI-LArA</ta>
            <ta e="T606" id="Seg_6468" s="T605">pradukcɨja</ta>
            <ta e="T607" id="Seg_6469" s="T606">gi͡en-tI-n</ta>
            <ta e="T608" id="Seg_6470" s="T607">ka</ta>
            <ta e="T609" id="Seg_6471" s="T608">bɨjɨl</ta>
            <ta e="T610" id="Seg_6472" s="T609">oŋor-BIT-LArA</ta>
            <ta e="T611" id="Seg_6473" s="T610">ol-tI-LArI-n</ta>
            <ta e="T612" id="Seg_6474" s="T611">da</ta>
            <ta e="T613" id="Seg_6475" s="T612">ubiraj-LAː-IAK-BIt</ta>
            <ta e="T614" id="Seg_6476" s="T613">ol-nI</ta>
            <ta e="T615" id="Seg_6477" s="T614">ubiraj-LAː-TAK-TArInA</ta>
            <ta e="T616" id="Seg_6478" s="T615">anɨ</ta>
            <ta e="T617" id="Seg_6479" s="T616">tɨ͡a-LAːgI</ta>
            <ta e="T618" id="Seg_6480" s="T617">eː</ta>
            <ta e="T619" id="Seg_6481" s="T618">bulčut-LAr</ta>
            <ta e="T620" id="Seg_6482" s="T619">da</ta>
            <ta e="T621" id="Seg_6483" s="T620">emi͡e</ta>
            <ta e="T624" id="Seg_6484" s="T623">ɨt-IAlAː-Iː-ČIt</ta>
            <ta e="T625" id="Seg_6485" s="T624">kihi-LAr</ta>
            <ta e="T626" id="Seg_6486" s="T625">da</ta>
            <ta e="T627" id="Seg_6487" s="T626">agɨjak-nI</ta>
            <ta e="T628" id="Seg_6488" s="T627">ɨl-IAK-LArA</ta>
            <ta e="T629" id="Seg_6489" s="T628">hin</ta>
            <ta e="T630" id="Seg_6490" s="T629">biːr</ta>
            <ta e="T633" id="Seg_6491" s="T631">hatan</ta>
            <ta e="T635" id="Seg_6492" s="T633">hatan-IAK-LArA</ta>
            <ta e="T636" id="Seg_6493" s="T635">hu͡ok-LArA</ta>
            <ta e="T637" id="Seg_6494" s="T636">eː</ta>
            <ta e="T659" id="Seg_6495" s="T657">ol-nAn</ta>
            <ta e="T660" id="Seg_6496" s="T659">eren-A-BIt</ta>
            <ta e="T661" id="Seg_6497" s="T660">eː</ta>
            <ta e="T662" id="Seg_6498" s="T661">ol-nAn</ta>
            <ta e="T663" id="Seg_6499" s="T662">eren-A-BIt</ta>
            <ta e="T664" id="Seg_6500" s="T663">a</ta>
            <ta e="T665" id="Seg_6501" s="T664">taːk</ta>
            <ta e="T666" id="Seg_6502" s="T665">iti</ta>
            <ta e="T667" id="Seg_6503" s="T666">karaŋa</ta>
            <ta e="T668" id="Seg_6504" s="T667">dek</ta>
            <ta e="T669" id="Seg_6505" s="T668">öttü-tI-n</ta>
            <ta e="T670" id="Seg_6506" s="T669">kepset-TI-BIt</ta>
            <ta e="T671" id="Seg_6507" s="T670">bihigi</ta>
            <ta e="T672" id="Seg_6508" s="T671">a</ta>
            <ta e="T673" id="Seg_6509" s="T672">taːk</ta>
            <ta e="T674" id="Seg_6510" s="T673">da</ta>
            <ta e="T675" id="Seg_6511" s="T674">tɨ͡a-GA</ta>
            <ta e="T676" id="Seg_6512" s="T675">hɨrɨt-TAK-GA</ta>
            <ta e="T677" id="Seg_6513" s="T676">bert</ta>
            <ta e="T678" id="Seg_6514" s="T677">beseleː</ta>
            <ta e="T679" id="Seg_6515" s="T678">e-TI-tA</ta>
            <ta e="T680" id="Seg_6516" s="T679">eː</ta>
            <ta e="T681" id="Seg_6517" s="T680">tɨ͡a-LAːgI</ta>
            <ta e="T682" id="Seg_6518" s="T681">kihi-LAr</ta>
            <ta e="T683" id="Seg_6519" s="T682">anɨ</ta>
            <ta e="T684" id="Seg_6520" s="T683">ogo-LAr</ta>
            <ta e="T685" id="Seg_6521" s="T684">hɨrɨt-I-BAT</ta>
            <ta e="T686" id="Seg_6522" s="T685">bu͡ol-An-LAr</ta>
            <ta e="T687" id="Seg_6523" s="T686">anɨ</ta>
            <ta e="T688" id="Seg_6524" s="T687">bil-BAT-LAr</ta>
            <ta e="T689" id="Seg_6525" s="T688">bu͡o</ta>
            <ta e="T690" id="Seg_6526" s="T689">haka</ta>
            <ta e="T691" id="Seg_6527" s="T690">tɨl-tI-n</ta>
            <ta e="T692" id="Seg_6528" s="T691">čeber</ta>
            <ta e="T693" id="Seg_6529" s="T692">tɨːn-LArA</ta>
            <ta e="T694" id="Seg_6530" s="T693">eː</ta>
            <ta e="T695" id="Seg_6531" s="T694">halgɨn-nI</ta>
            <ta e="T696" id="Seg_6532" s="T695">ot</ta>
            <ta e="T697" id="Seg_6533" s="T696">hɨt-tI-n</ta>
            <ta e="T698" id="Seg_6534" s="T697">di͡e-BIT-GA</ta>
            <ta e="T699" id="Seg_6535" s="T698">di͡e-AːrI</ta>
            <ta e="T700" id="Seg_6536" s="T699">maŋnaj</ta>
            <ta e="T701" id="Seg_6537" s="T700">kaːs</ta>
            <ta e="T702" id="Seg_6538" s="T701">ajdaːn-tI-n</ta>
            <ta e="T703" id="Seg_6539" s="T702">tugut-LAr</ta>
            <ta e="T704" id="Seg_6540" s="T703">töröː-Ar-LArI-n</ta>
            <ta e="T705" id="Seg_6541" s="T704">barɨ-tI-n</ta>
            <ta e="T706" id="Seg_6542" s="T705">tu͡ok-nI</ta>
            <ta e="T707" id="Seg_6543" s="T706">da</ta>
            <ta e="T708" id="Seg_6544" s="T707">olus</ta>
            <ta e="T709" id="Seg_6545" s="T708">bil-BAT-LAr</ta>
            <ta e="T710" id="Seg_6546" s="T709">bu͡o</ta>
            <ta e="T711" id="Seg_6547" s="T710">ol-tI-ŋ</ta>
            <ta e="T712" id="Seg_6548" s="T711">barɨ-tA</ta>
            <ta e="T713" id="Seg_6549" s="T712">bihigi-GA</ta>
            <ta e="T714" id="Seg_6550" s="T713">eː</ta>
            <ta e="T715" id="Seg_6551" s="T714">tɨ͡a-GA</ta>
            <ta e="T716" id="Seg_6552" s="T715">ü͡öskeː-BIT</ta>
            <ta e="T717" id="Seg_6553" s="T716">kihi-LAr-GA</ta>
            <ta e="T718" id="Seg_6554" s="T717">ol-tI-ŋ</ta>
            <ta e="T719" id="Seg_6555" s="T718">barɨ-tA</ta>
            <ta e="T720" id="Seg_6556" s="T719">barɨ-tA</ta>
            <ta e="T721" id="Seg_6557" s="T720">ulakan</ta>
            <ta e="T722" id="Seg_6558" s="T721">kim</ta>
            <ta e="T723" id="Seg_6559" s="T722">bu͡o</ta>
            <ta e="T725" id="Seg_6560" s="T723">vpʼečʼatlʼenʼije</ta>
            <ta e="T764" id="Seg_6561" s="T763">pasiːba</ta>
         </annotation>
         <annotation name="ge" tierref="ge-UkOA">
            <ta e="T8" id="Seg_6562" s="T7">ah</ta>
            <ta e="T9" id="Seg_6563" s="T8">self-1SG.[NOM]</ta>
            <ta e="T10" id="Seg_6564" s="T9">Khatanga-DAT/LOC</ta>
            <ta e="T11" id="Seg_6565" s="T10">house-PROPR.[NOM]</ta>
            <ta e="T12" id="Seg_6566" s="T11">be-CVB.SEQ-1SG</ta>
            <ta e="T13" id="Seg_6567" s="T12">eh</ta>
            <ta e="T14" id="Seg_6568" s="T13">tundra-DAT/LOC</ta>
            <ta e="T15" id="Seg_6569" s="T14">little-ADVZ</ta>
            <ta e="T16" id="Seg_6570" s="T15">be-PRS-1SG</ta>
            <ta e="T17" id="Seg_6571" s="T16">EMPH</ta>
            <ta e="T18" id="Seg_6572" s="T17">however</ta>
            <ta e="T19" id="Seg_6573" s="T18">one</ta>
            <ta e="T20" id="Seg_6574" s="T19">spring-PL-ACC</ta>
            <ta e="T21" id="Seg_6575" s="T20">autumn-PL-ACC</ta>
            <ta e="T22" id="Seg_6576" s="T21">maybe</ta>
            <ta e="T23" id="Seg_6577" s="T22">always</ta>
            <ta e="T24" id="Seg_6578" s="T23">tundra-DAT/LOC</ta>
            <ta e="T25" id="Seg_6579" s="T24">go-PRS-1SG</ta>
            <ta e="T26" id="Seg_6580" s="T25">how.much</ta>
            <ta e="T27" id="Seg_6581" s="T26">year.[NOM]</ta>
            <ta e="T28" id="Seg_6582" s="T27">every</ta>
            <ta e="T29" id="Seg_6583" s="T28">mother-1SG-PROPR-1SG-DAT/LOC</ta>
            <ta e="T30" id="Seg_6584" s="T29">father-1SG-PROPR-1SG-DAT/LOC</ta>
            <ta e="T31" id="Seg_6585" s="T30">help-CVB.SIM</ta>
            <ta e="T32" id="Seg_6586" s="T31">that.[NOM]</ta>
            <ta e="T33" id="Seg_6587" s="T32">to</ta>
            <ta e="T34" id="Seg_6588" s="T33">tundra-DAT/LOC</ta>
            <ta e="T35" id="Seg_6589" s="T34">go-CVB.SIM</ta>
            <ta e="T36" id="Seg_6590" s="T35">be-CVB.SEQ-3PL</ta>
            <ta e="T38" id="Seg_6591" s="T37">reindeer-PROPR-3PL</ta>
            <ta e="T39" id="Seg_6592" s="T38">EMPH</ta>
            <ta e="T40" id="Seg_6593" s="T39">that</ta>
            <ta e="T41" id="Seg_6594" s="T40">reindeer-PL.[NOM]</ta>
            <ta e="T42" id="Seg_6595" s="T41">reindeer-3PL-ACC</ta>
            <ta e="T43" id="Seg_6596" s="T42">EMPH</ta>
            <ta e="T44" id="Seg_6597" s="T43">see-CVB.SIM</ta>
            <ta e="T45" id="Seg_6598" s="T44">soon</ta>
            <ta e="T46" id="Seg_6599" s="T45">EMPH</ta>
            <ta e="T47" id="Seg_6600" s="T46">such-ACC</ta>
            <ta e="T48" id="Seg_6601" s="T47">self-1SG.[NOM]</ta>
            <ta e="T49" id="Seg_6602" s="T48">EMPH</ta>
            <ta e="T51" id="Seg_6603" s="T50">who-VBZ-PRS-1SG</ta>
            <ta e="T52" id="Seg_6604" s="T51">load-EP-PASS/REFL-PRS-1SG</ta>
            <ta e="T53" id="Seg_6605" s="T52">eh</ta>
            <ta e="T54" id="Seg_6606" s="T53">who-EP-INSTR</ta>
            <ta e="T55" id="Seg_6607" s="T54">energy-INSTR</ta>
            <ta e="T56" id="Seg_6608" s="T55">say-PTCP.PST-DAT/LOC</ta>
            <ta e="T57" id="Seg_6609" s="T56">say-CVB.PURP</ta>
            <ta e="T60" id="Seg_6610" s="T59">Khatanga-DAT/LOC</ta>
            <ta e="T61" id="Seg_6611" s="T60">lie-CVB.SEQ</ta>
            <ta e="T62" id="Seg_6612" s="T61">breath-3SG-ACC</ta>
            <ta e="T63" id="Seg_6613" s="T62">block-PRS-3PL</ta>
            <ta e="T64" id="Seg_6614" s="T63">that.EMPH.[NOM]</ta>
            <ta e="T65" id="Seg_6615" s="T64">after</ta>
            <ta e="T85" id="Seg_6616" s="T84">tundra-ADJZ-PL-EP-2SG.[NOM]</ta>
            <ta e="T86" id="Seg_6617" s="T85">self-3PL.[NOM]</ta>
            <ta e="T87" id="Seg_6618" s="T86">know-PRS-3PL</ta>
            <ta e="T88" id="Seg_6619" s="T87">EMPH</ta>
            <ta e="T89" id="Seg_6620" s="T88">how</ta>
            <ta e="T90" id="Seg_6621" s="T89">live-PTCP.FUT-3PL-ACC</ta>
            <ta e="T91" id="Seg_6622" s="T90">now</ta>
            <ta e="T92" id="Seg_6623" s="T91">good.[NOM]</ta>
            <ta e="T93" id="Seg_6624" s="T92">life-ACC</ta>
            <ta e="T94" id="Seg_6625" s="T93">know-NEG.PTCP</ta>
            <ta e="T95" id="Seg_6626" s="T94">be-CVB.SEQ-3PL</ta>
            <ta e="T96" id="Seg_6627" s="T95">3PL.[NOM]</ta>
            <ta e="T97" id="Seg_6628" s="T96">eh</ta>
            <ta e="T98" id="Seg_6629" s="T97">good-ADVZ</ta>
            <ta e="T99" id="Seg_6630" s="T98">live-FUT-1PL</ta>
            <ta e="T100" id="Seg_6631" s="T99">say-CVB.SEQ</ta>
            <ta e="T101" id="Seg_6632" s="T100">go-NEG-3PL</ta>
            <ta e="T102" id="Seg_6633" s="T101">apparently</ta>
            <ta e="T103" id="Seg_6634" s="T102">very</ta>
            <ta e="T104" id="Seg_6635" s="T103">EMPH</ta>
            <ta e="T105" id="Seg_6636" s="T104">self-3PL.[NOM]</ta>
            <ta e="T106" id="Seg_6637" s="T105">EMPH</ta>
            <ta e="T107" id="Seg_6638" s="T106">what.[NOM]</ta>
            <ta e="T108" id="Seg_6639" s="T107">there.is</ta>
            <ta e="T109" id="Seg_6640" s="T108">that-INSTR</ta>
            <ta e="T110" id="Seg_6641" s="T109">be.happy-CVB.SIM</ta>
            <ta e="T111" id="Seg_6642" s="T110">go-PRS-3PL</ta>
            <ta e="T112" id="Seg_6643" s="T111">NEG.EX</ta>
            <ta e="T113" id="Seg_6644" s="T112">be-TEMP-3SG</ta>
            <ta e="T114" id="Seg_6645" s="T113">how</ta>
            <ta e="T115" id="Seg_6646" s="T114">make-FUT.[3SG]=Q</ta>
            <ta e="T116" id="Seg_6647" s="T115">NEG.EX</ta>
            <ta e="T117" id="Seg_6648" s="T116">EMPH</ta>
            <ta e="T119" id="Seg_6649" s="T117">NEG.EX</ta>
            <ta e="T124" id="Seg_6650" s="T123">very</ta>
            <ta e="T125" id="Seg_6651" s="T124">EMPH</ta>
            <ta e="T126" id="Seg_6652" s="T125">be.sad-NEG-3PL</ta>
            <ta e="T127" id="Seg_6653" s="T126">AFFIRM</ta>
            <ta e="T128" id="Seg_6654" s="T127">and</ta>
            <ta e="T129" id="Seg_6655" s="T128">if</ta>
            <ta e="T130" id="Seg_6656" s="T129">this</ta>
            <ta e="T132" id="Seg_6657" s="T131">good-ADVZ</ta>
            <ta e="T133" id="Seg_6658" s="T132">eh</ta>
            <ta e="T134" id="Seg_6659" s="T133">human.being-SIM</ta>
            <ta e="T135" id="Seg_6660" s="T134">this</ta>
            <ta e="T136" id="Seg_6661" s="T135">real-SIM</ta>
            <ta e="T137" id="Seg_6662" s="T136">live-PTCP.PRS</ta>
            <ta e="T138" id="Seg_6663" s="T137">human.being.[NOM]</ta>
            <ta e="T139" id="Seg_6664" s="T138">this.[NOM]</ta>
            <ta e="T140" id="Seg_6665" s="T139">indeed</ta>
            <ta e="T141" id="Seg_6666" s="T140">EMPH</ta>
            <ta e="T142" id="Seg_6667" s="T141">accessible</ta>
            <ta e="T143" id="Seg_6668" s="T142">EMPH</ta>
            <ta e="T144" id="Seg_6669" s="T143">balok-PL-ACC</ta>
            <ta e="T145" id="Seg_6670" s="T144">administration-ABL</ta>
            <ta e="T146" id="Seg_6671" s="T145">give-PTCP.FUT</ta>
            <ta e="T147" id="Seg_6672" s="T146">be-PST1-3PL</ta>
            <ta e="T148" id="Seg_6673" s="T147">MOD</ta>
            <ta e="T149" id="Seg_6674" s="T148">this</ta>
            <ta e="T150" id="Seg_6675" s="T149">indispensable.[NOM]</ta>
            <ta e="T151" id="Seg_6676" s="T150">most</ta>
            <ta e="T152" id="Seg_6677" s="T151">most</ta>
            <ta e="T153" id="Seg_6678" s="T152">need-PROPR.[NOM]</ta>
            <ta e="T154" id="Seg_6679" s="T153">thing-3PL.[NOM]</ta>
            <ta e="T155" id="Seg_6680" s="T154">what-3PL.[NOM]</ta>
            <ta e="T157" id="Seg_6681" s="T156">this</ta>
            <ta e="T158" id="Seg_6682" s="T157">well</ta>
            <ta e="T159" id="Seg_6683" s="T158">help.[NOM]</ta>
            <ta e="T160" id="Seg_6684" s="T159">subsidy.[NOM]</ta>
            <ta e="T161" id="Seg_6685" s="T160">similar</ta>
            <ta e="T162" id="Seg_6686" s="T161">big-ADVZ</ta>
            <ta e="T163" id="Seg_6687" s="T162">come-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T164" id="Seg_6688" s="T163">be-CVB.SIM</ta>
            <ta e="T165" id="Seg_6689" s="T164">tundra-ADJZ-PL-DAT/LOC</ta>
            <ta e="T166" id="Seg_6690" s="T165">self-3PL-GEN</ta>
            <ta e="T167" id="Seg_6691" s="T166">self-3PL-GEN</ta>
            <ta e="T168" id="Seg_6692" s="T167">ability-3PL-INSTR</ta>
            <ta e="T169" id="Seg_6693" s="T168">go-EP-MED-EMOT-PRS-3PL</ta>
            <ta e="T170" id="Seg_6694" s="T169">tundra-ADJZ-PL-1PL.[NOM]</ta>
            <ta e="T171" id="Seg_6695" s="T170">that.[NOM]</ta>
            <ta e="T172" id="Seg_6696" s="T171">because.of</ta>
            <ta e="T173" id="Seg_6697" s="T172">now</ta>
            <ta e="T174" id="Seg_6698" s="T173">still</ta>
            <ta e="T175" id="Seg_6699" s="T174">that.[NOM]</ta>
            <ta e="T176" id="Seg_6700" s="T175">thousand</ta>
            <ta e="T177" id="Seg_6701" s="T176">five</ta>
            <ta e="T178" id="Seg_6702" s="T177">hundred</ta>
            <ta e="T179" id="Seg_6703" s="T178">that.[NOM]</ta>
            <ta e="T180" id="Seg_6704" s="T179">who.[NOM]</ta>
            <ta e="T181" id="Seg_6705" s="T180">strongly</ta>
            <ta e="T182" id="Seg_6706" s="T181">1500.rubles-ACC</ta>
            <ta e="T183" id="Seg_6707" s="T182">give-CVB.SEQ</ta>
            <ta e="T184" id="Seg_6708" s="T183">be-CVB.SEQ-3PL</ta>
            <ta e="T185" id="Seg_6709" s="T184">still</ta>
            <ta e="T186" id="Seg_6710" s="T185">a.little</ta>
            <ta e="T187" id="Seg_6711" s="T186">ah</ta>
            <ta e="T188" id="Seg_6712" s="T187">and</ta>
            <ta e="T189" id="Seg_6713" s="T188">so</ta>
            <ta e="T190" id="Seg_6714" s="T189">at.all</ta>
            <ta e="T191" id="Seg_6715" s="T190">EMPH</ta>
            <ta e="T192" id="Seg_6716" s="T191">what.[NOM]</ta>
            <ta e="T193" id="Seg_6717" s="T192">very</ta>
            <ta e="T194" id="Seg_6718" s="T193">EMPH</ta>
            <ta e="T196" id="Seg_6719" s="T195">food-EP-INSTR</ta>
            <ta e="T197" id="Seg_6720" s="T196">EMPH</ta>
            <ta e="T198" id="Seg_6721" s="T197">bread-EP-INSTR</ta>
            <ta e="T199" id="Seg_6722" s="T198">NEG</ta>
            <ta e="T200" id="Seg_6723" s="T199">what-EP-INSTR</ta>
            <ta e="T201" id="Seg_6724" s="T200">NEG</ta>
            <ta e="T202" id="Seg_6725" s="T201">that</ta>
            <ta e="T203" id="Seg_6726" s="T202">bakery-1PL.[NOM]</ta>
            <ta e="T204" id="Seg_6727" s="T203">work-PTCP.NEG</ta>
            <ta e="T205" id="Seg_6728" s="T204">be-CVB.SEQ</ta>
            <ta e="T206" id="Seg_6729" s="T205">self-3PL.[NOM]</ta>
            <ta e="T207" id="Seg_6730" s="T206">boil-EP-CAUS-EP-MED-EMOT-PRS-3PL</ta>
            <ta e="T208" id="Seg_6731" s="T207">tundra-DAT/LOC</ta>
            <ta e="T209" id="Seg_6732" s="T208">go-CVB.SEQ-3PL</ta>
            <ta e="T210" id="Seg_6733" s="T209">what.kind.of</ta>
            <ta e="T211" id="Seg_6734" s="T210">what.kind.of</ta>
            <ta e="T212" id="Seg_6735" s="T211">young.[NOM]</ta>
            <ta e="T213" id="Seg_6736" s="T212">human.being.[NOM]</ta>
            <ta e="T214" id="Seg_6737" s="T213">go-FUT-1SG</ta>
            <ta e="T215" id="Seg_6738" s="T214">say-FUT.[3SG]=Q</ta>
            <ta e="T216" id="Seg_6739" s="T215">warm.[NOM]</ta>
            <ta e="T217" id="Seg_6740" s="T216">house-ABL</ta>
            <ta e="T218" id="Seg_6741" s="T217">warm</ta>
            <ta e="T219" id="Seg_6742" s="T218">fire-ABL</ta>
            <ta e="T220" id="Seg_6743" s="T219">go.out-CVB.SEQ</ta>
            <ta e="T221" id="Seg_6744" s="T220">cold-DAT/LOC</ta>
            <ta e="T222" id="Seg_6745" s="T221">wind-DAT/LOC</ta>
            <ta e="T223" id="Seg_6746" s="T222">go.out-CVB.SEQ</ta>
            <ta e="T224" id="Seg_6747" s="T223">reindeer-ACC</ta>
            <ta e="T225" id="Seg_6748" s="T224">guard-CVB.SIM</ta>
            <ta e="T226" id="Seg_6749" s="T225">go-FUT-1SG</ta>
            <ta e="T227" id="Seg_6750" s="T226">say-CVB.SEQ</ta>
            <ta e="T228" id="Seg_6751" s="T227">Q</ta>
            <ta e="T229" id="Seg_6752" s="T228">pay-NEG-3PL</ta>
            <ta e="T230" id="Seg_6753" s="T229">EMPH</ta>
            <ta e="T231" id="Seg_6754" s="T230">that</ta>
            <ta e="T232" id="Seg_6755" s="T231">then</ta>
            <ta e="T233" id="Seg_6756" s="T232">MOD</ta>
            <ta e="T234" id="Seg_6757" s="T233">money-ACC</ta>
            <ta e="T235" id="Seg_6758" s="T234">see-NEG-3PL</ta>
            <ta e="T236" id="Seg_6759" s="T235">tundra-ADJZ-PL.[NOM]</ta>
            <ta e="T237" id="Seg_6760" s="T236">recently</ta>
            <ta e="T238" id="Seg_6761" s="T237">tarpaulin.[NOM]</ta>
            <ta e="T239" id="Seg_6762" s="T238">what.[NOM]</ta>
            <ta e="T240" id="Seg_6763" s="T239">bring-PRS-3PL</ta>
            <ta e="T242" id="Seg_6764" s="T241">business.man-PL.[NOM]</ta>
            <ta e="T243" id="Seg_6765" s="T242">sell-PRS-3PL</ta>
            <ta e="T244" id="Seg_6766" s="T243">very</ta>
            <ta e="T245" id="Seg_6767" s="T244">expensive-DAT/LOC</ta>
            <ta e="T246" id="Seg_6768" s="T245">that-3SG-2SG.[NOM]</ta>
            <ta e="T247" id="Seg_6769" s="T246">what.[NOM]</ta>
            <ta e="T248" id="Seg_6770" s="T247">human.being-3SG.[NOM]=Q</ta>
            <ta e="T249" id="Seg_6771" s="T248">buy-CVB.SEQ</ta>
            <ta e="T250" id="Seg_6772" s="T249">take-FUT.[3SG]=Q</ta>
            <ta e="T251" id="Seg_6773" s="T250">Q</ta>
            <ta e="T252" id="Seg_6774" s="T251">eh</ta>
            <ta e="T253" id="Seg_6775" s="T252">food-ACC</ta>
            <ta e="T254" id="Seg_6776" s="T253">food-ACC</ta>
            <ta e="T255" id="Seg_6777" s="T254">buy-FUT.[3SG]=Q</ta>
            <ta e="T256" id="Seg_6778" s="T255">Q</ta>
            <ta e="T257" id="Seg_6779" s="T256">food-ACC</ta>
            <ta e="T258" id="Seg_6780" s="T257">buy-FUT-2SG</ta>
            <ta e="T259" id="Seg_6781" s="T258">Q</ta>
            <ta e="T260" id="Seg_6782" s="T259">tarpaulin.[NOM]</ta>
            <ta e="T261" id="Seg_6783" s="T260">buy-FUT-2SG</ta>
            <ta e="T262" id="Seg_6784" s="T261">Q</ta>
            <ta e="T263" id="Seg_6785" s="T262">clothes.[NOM]</ta>
            <ta e="T264" id="Seg_6786" s="T263">buy-FUT-2SG</ta>
            <ta e="T265" id="Seg_6787" s="T264">Q</ta>
            <ta e="T266" id="Seg_6788" s="T265">child-PL-EP-2SG.[NOM]</ta>
            <ta e="T267" id="Seg_6789" s="T266">still</ta>
            <ta e="T268" id="Seg_6790" s="T267">learn-PTCP.FUT-3PL-ACC</ta>
            <ta e="T269" id="Seg_6791" s="T268">need.to</ta>
            <ta e="T270" id="Seg_6792" s="T269">what.[NOM]</ta>
            <ta e="T271" id="Seg_6793" s="T270">EMPH</ta>
            <ta e="T272" id="Seg_6794" s="T271">now</ta>
            <ta e="T273" id="Seg_6795" s="T272">be-CVB.SIM</ta>
            <ta e="T274" id="Seg_6796" s="T273">before</ta>
            <ta e="T275" id="Seg_6797" s="T274">student-PL-ACC</ta>
            <ta e="T276" id="Seg_6798" s="T275">what-PL-ACC</ta>
            <ta e="T277" id="Seg_6799" s="T276">though</ta>
            <ta e="T278" id="Seg_6800" s="T277">give-PTCP.HAB</ta>
            <ta e="T279" id="Seg_6801" s="T278">be-PST1-3PL</ta>
            <ta e="T280" id="Seg_6802" s="T279">3PL-DAT/LOC</ta>
            <ta e="T281" id="Seg_6803" s="T280">what.[NOM]</ta>
            <ta e="T282" id="Seg_6804" s="T281">INDEF</ta>
            <ta e="T283" id="Seg_6805" s="T282">subsidy.[NOM]</ta>
            <ta e="T284" id="Seg_6806" s="T283">maybe</ta>
            <ta e="T285" id="Seg_6807" s="T284">sovkhoz-ABL</ta>
            <ta e="T286" id="Seg_6808" s="T285">relocate-VBZ-PTCP.HAB</ta>
            <ta e="T287" id="Seg_6809" s="T286">be-PST1-3PL</ta>
            <ta e="T288" id="Seg_6810" s="T287">now</ta>
            <ta e="T289" id="Seg_6811" s="T288">that</ta>
            <ta e="T290" id="Seg_6812" s="T289">such</ta>
            <ta e="T291" id="Seg_6813" s="T290">what.[NOM]</ta>
            <ta e="T292" id="Seg_6814" s="T291">NEG</ta>
            <ta e="T293" id="Seg_6815" s="T292">NEG.EX</ta>
            <ta e="T294" id="Seg_6816" s="T293">whole-3SG.[NOM]</ta>
            <ta e="T295" id="Seg_6817" s="T294">human.being.[NOM]</ta>
            <ta e="T296" id="Seg_6818" s="T295">self-3SG-GEN</ta>
            <ta e="T297" id="Seg_6819" s="T296">money-3SG-INSTR</ta>
            <ta e="T298" id="Seg_6820" s="T297">go-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_6821" s="T298">who.[NOM]</ta>
            <ta e="T300" id="Seg_6822" s="T299">ability-POSS</ta>
            <ta e="T301" id="Seg_6823" s="T300">NEG</ta>
            <ta e="T302" id="Seg_6824" s="T301">human.being.[NOM]</ta>
            <ta e="T303" id="Seg_6825" s="T302">at.all</ta>
            <ta e="T304" id="Seg_6826" s="T303">die-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_6827" s="T304">say-PTCP.PST-DAT/LOC</ta>
            <ta e="T306" id="Seg_6828" s="T305">say-CVB.PURP</ta>
            <ta e="T307" id="Seg_6829" s="T306">that.EMPH</ta>
            <ta e="T308" id="Seg_6830" s="T307">think-CVB.SEQ</ta>
            <ta e="T309" id="Seg_6831" s="T308">and</ta>
            <ta e="T310" id="Seg_6832" s="T309">who.[NOM]</ta>
            <ta e="T311" id="Seg_6833" s="T310">ability-PROPR-EP-2SG.[NOM]</ta>
            <ta e="T312" id="Seg_6834" s="T311">a.little</ta>
            <ta e="T313" id="Seg_6835" s="T312">teach-PRS-3PL</ta>
            <ta e="T314" id="Seg_6836" s="T313">child-PL-3SG-ACC</ta>
            <ta e="T315" id="Seg_6837" s="T314">however</ta>
            <ta e="T316" id="Seg_6838" s="T315">one</ta>
            <ta e="T321" id="Seg_6839" s="T319">live-PRS-3PL</ta>
            <ta e="T336" id="Seg_6840" s="T335">na</ta>
            <ta e="T337" id="Seg_6841" s="T336">lord-PL.[NOM]</ta>
            <ta e="T338" id="Seg_6842" s="T337">just</ta>
            <ta e="T339" id="Seg_6843" s="T338">lord-PL.[NOM]</ta>
            <ta e="T340" id="Seg_6844" s="T339">just</ta>
            <ta e="T341" id="Seg_6845" s="T340">help-FUT-3PL</ta>
            <ta e="T342" id="Seg_6846" s="T341">1PL.[NOM]</ta>
            <ta e="T343" id="Seg_6847" s="T342">earth-1PL-DAT/LOC</ta>
            <ta e="T344" id="Seg_6848" s="T343">life-1PL.[NOM]</ta>
            <ta e="T345" id="Seg_6849" s="T344">improve-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T346" id="Seg_6850" s="T345">completely</ta>
            <ta e="T347" id="Seg_6851" s="T346">3PL-ABL</ta>
            <ta e="T348" id="Seg_6852" s="T347">just</ta>
            <ta e="T350" id="Seg_6853" s="T348">go.out-FUT-3SG</ta>
            <ta e="T362" id="Seg_6854" s="T360">self</ta>
            <ta e="T365" id="Seg_6855" s="T362">self-1PL.[NOM]</ta>
            <ta e="T368" id="Seg_6856" s="T365">now</ta>
            <ta e="T369" id="Seg_6857" s="T368">human.being-1PL.[NOM]</ta>
            <ta e="T370" id="Seg_6858" s="T369">now</ta>
            <ta e="T371" id="Seg_6859" s="T370">human.being-PL-1PL.[NOM]</ta>
            <ta e="T372" id="Seg_6860" s="T371">MOD</ta>
            <ta e="T373" id="Seg_6861" s="T372">basically</ta>
            <ta e="T374" id="Seg_6862" s="T373">many</ta>
            <ta e="T375" id="Seg_6863" s="T374">human.being.[NOM]</ta>
            <ta e="T376" id="Seg_6864" s="T375">eh</ta>
            <ta e="T377" id="Seg_6865" s="T376">go.in-PST2-3SG</ta>
            <ta e="T378" id="Seg_6866" s="T377">tundra-ABL</ta>
            <ta e="T379" id="Seg_6867" s="T378">ability-POSS</ta>
            <ta e="T380" id="Seg_6868" s="T379">NEG</ta>
            <ta e="T381" id="Seg_6869" s="T380">be-CVB.SEQ-3PL</ta>
            <ta e="T382" id="Seg_6870" s="T381">every-3PL.[NOM]</ta>
            <ta e="T383" id="Seg_6871" s="T382">big.[NOM]</ta>
            <ta e="T384" id="Seg_6872" s="T383">experience-PROPR-3PL.[NOM]</ta>
            <ta e="T385" id="Seg_6873" s="T384">reindeer-DAT/LOC</ta>
            <ta e="T386" id="Seg_6874" s="T385">go-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T387" id="Seg_6875" s="T386">EMPH</ta>
            <ta e="T388" id="Seg_6876" s="T387">eh</ta>
            <ta e="T389" id="Seg_6877" s="T388">that</ta>
            <ta e="T390" id="Seg_6878" s="T389">every-3SG.[NOM]</ta>
            <ta e="T391" id="Seg_6879" s="T390">that</ta>
            <ta e="T392" id="Seg_6880" s="T391">every-3SG.[NOM]</ta>
            <ta e="T393" id="Seg_6881" s="T392">that</ta>
            <ta e="T394" id="Seg_6882" s="T393">every-3SG.[NOM]</ta>
            <ta e="T395" id="Seg_6883" s="T394">die-PRS.[3SG]</ta>
            <ta e="T396" id="Seg_6884" s="T395">human.being.[NOM]</ta>
            <ta e="T397" id="Seg_6885" s="T396">every-3SG.[NOM]</ta>
            <ta e="T398" id="Seg_6886" s="T397">eh</ta>
            <ta e="T399" id="Seg_6887" s="T398">so</ta>
            <ta e="T400" id="Seg_6888" s="T399">place-DAT/LOC</ta>
            <ta e="T401" id="Seg_6889" s="T400">stay-PRS.[3SG]</ta>
            <ta e="T403" id="Seg_6890" s="T402">place-DAT/LOC</ta>
            <ta e="T404" id="Seg_6891" s="T403">stay-PRS.[3SG]</ta>
            <ta e="T405" id="Seg_6892" s="T404">human.being.[NOM]</ta>
            <ta e="T406" id="Seg_6893" s="T405">every-3SG.[NOM]</ta>
            <ta e="T407" id="Seg_6894" s="T406">now</ta>
            <ta e="T408" id="Seg_6895" s="T407">go-PTCP.PRS</ta>
            <ta e="T409" id="Seg_6896" s="T408">EMPH</ta>
            <ta e="T410" id="Seg_6897" s="T409">human.being-2SG.[NOM]</ta>
            <ta e="T411" id="Seg_6898" s="T410">very</ta>
            <ta e="T412" id="Seg_6899" s="T411">earth-ACC</ta>
            <ta e="T413" id="Seg_6900" s="T412">know-NEG-3PL</ta>
            <ta e="T414" id="Seg_6901" s="T413">young.[NOM]</ta>
            <ta e="T415" id="Seg_6902" s="T414">human.being-PL-EP-2SG.[NOM]</ta>
            <ta e="T416" id="Seg_6903" s="T415">now</ta>
            <ta e="T417" id="Seg_6904" s="T416">go-CVB.SEQ-3PL</ta>
            <ta e="T418" id="Seg_6905" s="T417">EMPH</ta>
            <ta e="T419" id="Seg_6906" s="T418">go.out-CVB.SEQ-3PL</ta>
            <ta e="T420" id="Seg_6907" s="T419">EMPH</ta>
            <ta e="T421" id="Seg_6908" s="T420">for.example</ta>
            <ta e="T422" id="Seg_6909" s="T421">reindeer.moss-PROPR.[NOM]</ta>
            <ta e="T423" id="Seg_6910" s="T422">place-PL.[NOM]</ta>
            <ta e="T424" id="Seg_6911" s="T423">now</ta>
            <ta e="T425" id="Seg_6912" s="T424">wild.reindeer-1PL.[NOM]</ta>
            <ta e="T426" id="Seg_6913" s="T425">now</ta>
            <ta e="T427" id="Seg_6914" s="T426">place-3SG-ACC</ta>
            <ta e="T428" id="Seg_6915" s="T427">change-CVB.SEQ</ta>
            <ta e="T429" id="Seg_6916" s="T428">1PL-INSTR</ta>
            <ta e="T430" id="Seg_6917" s="T429">many-ACC</ta>
            <ta e="T431" id="Seg_6918" s="T430">fall-PTCP.PRS</ta>
            <ta e="T432" id="Seg_6919" s="T431">be-CVB.SEQ</ta>
            <ta e="T433" id="Seg_6920" s="T432">reindeer.moss-1PL-ACC</ta>
            <ta e="T434" id="Seg_6921" s="T433">spend-CAUS-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_6922" s="T434">that.[NOM]</ta>
            <ta e="T436" id="Seg_6923" s="T435">because.of</ta>
            <ta e="T437" id="Seg_6924" s="T436">go-PTCP.PRS</ta>
            <ta e="T438" id="Seg_6925" s="T437">place-PL-1PL.[NOM]</ta>
            <ta e="T439" id="Seg_6926" s="T438">narrow-PST2-3PL</ta>
            <ta e="T440" id="Seg_6927" s="T439">reindeer-DAT/LOC</ta>
            <ta e="T441" id="Seg_6928" s="T440">EMPH</ta>
            <ta e="T442" id="Seg_6929" s="T441">how</ta>
            <ta e="T443" id="Seg_6930" s="T442">reindeer-DAT/LOC</ta>
            <ta e="T444" id="Seg_6931" s="T443">so</ta>
            <ta e="T445" id="Seg_6932" s="T444">human.being-DAT/LOC</ta>
            <ta e="T446" id="Seg_6933" s="T445">EMPH</ta>
            <ta e="T447" id="Seg_6934" s="T446">human.being-PL-1PL.[NOM]</ta>
            <ta e="T449" id="Seg_6935" s="T447">eh</ta>
            <ta e="T453" id="Seg_6936" s="T451">different.[NOM]</ta>
            <ta e="T454" id="Seg_6937" s="T453">different.[NOM]</ta>
            <ta e="T456" id="Seg_6938" s="T454">life-ACC</ta>
            <ta e="T460" id="Seg_6939" s="T458">different.[NOM]</ta>
            <ta e="T461" id="Seg_6940" s="T460">life-ACC</ta>
            <ta e="T462" id="Seg_6941" s="T461">accessible.[NOM]</ta>
            <ta e="T463" id="Seg_6942" s="T462">life-ACC</ta>
            <ta e="T464" id="Seg_6943" s="T463">follow-CVB.SEQ-3PL</ta>
            <ta e="T465" id="Seg_6944" s="T464">accessible.[NOM]</ta>
            <ta e="T466" id="Seg_6945" s="T465">then</ta>
            <ta e="T467" id="Seg_6946" s="T466">accessible.[NOM]</ta>
            <ta e="T468" id="Seg_6947" s="T467">be-FUT.[3SG]</ta>
            <ta e="T469" id="Seg_6948" s="T468">Novorybnoe-DAT/LOC</ta>
            <ta e="T470" id="Seg_6949" s="T469">live</ta>
            <ta e="T471" id="Seg_6950" s="T470">eh</ta>
            <ta e="T472" id="Seg_6951" s="T471">then</ta>
            <ta e="T475" id="Seg_6952" s="T474">Novorybnoe.[NOM]</ta>
            <ta e="T476" id="Seg_6953" s="T475">side-3SG-INSTR</ta>
            <ta e="T477" id="Seg_6954" s="T476">tell-PRS-1SG</ta>
            <ta e="T478" id="Seg_6955" s="T477">then</ta>
            <ta e="T479" id="Seg_6956" s="T478">Novorybnoe-DAT/LOC</ta>
            <ta e="T480" id="Seg_6957" s="T479">better</ta>
            <ta e="T481" id="Seg_6958" s="T480">EMPH</ta>
            <ta e="T482" id="Seg_6959" s="T481">light-PROPR-3PL</ta>
            <ta e="T483" id="Seg_6960" s="T482">EMPH</ta>
            <ta e="T484" id="Seg_6961" s="T483">warm.[NOM]</ta>
            <ta e="T485" id="Seg_6962" s="T484">EMPH</ta>
            <ta e="T486" id="Seg_6963" s="T485">that.[NOM]</ta>
            <ta e="T487" id="Seg_6964" s="T486">because.of</ta>
            <ta e="T488" id="Seg_6965" s="T487">tundra</ta>
            <ta e="T489" id="Seg_6966" s="T488">tundra-ADJZ.[NOM]</ta>
            <ta e="T490" id="Seg_6967" s="T489">human.being-PL.[NOM]</ta>
            <ta e="T491" id="Seg_6968" s="T490">tundra-DAT/LOC</ta>
            <ta e="T492" id="Seg_6969" s="T491">go-PTCP.FUT-3PL-ACC</ta>
            <ta e="T493" id="Seg_6970" s="T492">very</ta>
            <ta e="T494" id="Seg_6971" s="T493">NEG</ta>
            <ta e="T495" id="Seg_6972" s="T494">pull-EP-CAUS-NEG-3PL</ta>
            <ta e="T496" id="Seg_6973" s="T495">money.[NOM]</ta>
            <ta e="T497" id="Seg_6974" s="T496">very</ta>
            <ta e="T498" id="Seg_6975" s="T497">eh</ta>
            <ta e="T499" id="Seg_6976" s="T498">reindeer-ABL</ta>
            <ta e="T500" id="Seg_6977" s="T499">what-ACC</ta>
            <ta e="T501" id="Seg_6978" s="T500">get-FUT-2SG=Q</ta>
            <ta e="T502" id="Seg_6979" s="T501">well</ta>
            <ta e="T503" id="Seg_6980" s="T502">now</ta>
            <ta e="T504" id="Seg_6981" s="T503">kill.[IMP.2SG]</ta>
            <ta e="T505" id="Seg_6982" s="T504">and</ta>
            <ta e="T506" id="Seg_6983" s="T505">that</ta>
            <ta e="T507" id="Seg_6984" s="T506">well</ta>
            <ta e="T508" id="Seg_6985" s="T507">little</ta>
            <ta e="T509" id="Seg_6986" s="T508">five-ten-APRX</ta>
            <ta e="T510" id="Seg_6987" s="T509">reindeer-ACC</ta>
            <ta e="T511" id="Seg_6988" s="T510">EMPH</ta>
            <ta e="T512" id="Seg_6989" s="T511">kill-TEMP-2SG</ta>
            <ta e="T513" id="Seg_6990" s="T512">very</ta>
            <ta e="T514" id="Seg_6991" s="T513">NEG</ta>
            <ta e="T515" id="Seg_6992" s="T514">many</ta>
            <ta e="T516" id="Seg_6993" s="T515">money-ACC</ta>
            <ta e="T517" id="Seg_6994" s="T516">gain-FUT-2SG</ta>
            <ta e="T518" id="Seg_6995" s="T517">NEG-3SG</ta>
            <ta e="T519" id="Seg_6996" s="T518">that</ta>
            <ta e="T520" id="Seg_6997" s="T519">side-3SG-DAT/LOC</ta>
            <ta e="T521" id="Seg_6998" s="T520">or</ta>
            <ta e="T522" id="Seg_6999" s="T521">tundra-DAT/LOC</ta>
            <ta e="T523" id="Seg_7000" s="T522">go-CVB.SEQ-2SG</ta>
            <ta e="T524" id="Seg_7001" s="T523">MOD</ta>
            <ta e="T525" id="Seg_7002" s="T524">that.[NOM]</ta>
            <ta e="T526" id="Seg_7003" s="T525">because.of</ta>
            <ta e="T527" id="Seg_7004" s="T526">happy</ta>
            <ta e="T528" id="Seg_7005" s="T527">live-PTCP.FUT-DAT/LOC</ta>
            <ta e="T529" id="Seg_7006" s="T528">and</ta>
            <ta e="T530" id="Seg_7007" s="T529">tundra-DAT/LOC</ta>
            <ta e="T531" id="Seg_7008" s="T530">human.being-PL.[NOM]</ta>
            <ta e="T532" id="Seg_7009" s="T531">happy-ADVZ</ta>
            <ta e="T533" id="Seg_7010" s="T532">live-PTCP.FUT-3PL-ACC</ta>
            <ta e="T534" id="Seg_7011" s="T533">that.[NOM]</ta>
            <ta e="T535" id="Seg_7012" s="T534">first</ta>
            <ta e="T536" id="Seg_7013" s="T535">that</ta>
            <ta e="T537" id="Seg_7014" s="T536">side-3SG-DAT/LOC</ta>
            <ta e="T538" id="Seg_7015" s="T537">government.[NOM]</ta>
            <ta e="T539" id="Seg_7016" s="T538">think-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T540" id="Seg_7017" s="T539">need.to</ta>
            <ta e="T541" id="Seg_7018" s="T540">human.being-PL-DAT/LOC</ta>
            <ta e="T542" id="Seg_7019" s="T541">help.[NOM]</ta>
            <ta e="T543" id="Seg_7020" s="T542">and</ta>
            <ta e="T544" id="Seg_7021" s="T543">human.being-PL.[NOM]</ta>
            <ta e="T545" id="Seg_7022" s="T544">self-3PL.[NOM]</ta>
            <ta e="T546" id="Seg_7023" s="T545">human.being-PL.[NOM]</ta>
            <ta e="T547" id="Seg_7024" s="T546">what-ACC</ta>
            <ta e="T548" id="Seg_7025" s="T547">NEG</ta>
            <ta e="T549" id="Seg_7026" s="T548">make-FUT-3PL</ta>
            <ta e="T550" id="Seg_7027" s="T549">NEG-3SG</ta>
            <ta e="T551" id="Seg_7028" s="T550">tundra-ADJZ.[NOM]</ta>
            <ta e="T552" id="Seg_7029" s="T551">human.being-PL.[NOM]</ta>
            <ta e="T553" id="Seg_7030" s="T552">even.more</ta>
            <ta e="T554" id="Seg_7031" s="T553">self-3PL-GEN</ta>
            <ta e="T555" id="Seg_7032" s="T554">front-3PL-GEN</ta>
            <ta e="T556" id="Seg_7033" s="T555">side-3SG-INSTR</ta>
            <ta e="T557" id="Seg_7034" s="T556">what-ACC</ta>
            <ta e="T558" id="Seg_7035" s="T557">NEG</ta>
            <ta e="T559" id="Seg_7036" s="T558">speak-EP-NEG-3PL</ta>
            <ta e="T560" id="Seg_7037" s="T559">sovkhoz-PL-1PL.[NOM]</ta>
            <ta e="T561" id="Seg_7038" s="T560">director-1PL.[NOM]</ta>
            <ta e="T562" id="Seg_7039" s="T561">director-PL.[NOM]</ta>
            <ta e="T563" id="Seg_7040" s="T562">EMPH</ta>
            <ta e="T564" id="Seg_7041" s="T563">change-PRS-3PL</ta>
            <ta e="T565" id="Seg_7042" s="T564">EMPH</ta>
            <ta e="T566" id="Seg_7043" s="T565">now</ta>
            <ta e="T567" id="Seg_7044" s="T566">still</ta>
            <ta e="T568" id="Seg_7045" s="T567">a.little</ta>
            <ta e="T569" id="Seg_7046" s="T568">1PL-DAT/LOC</ta>
            <ta e="T570" id="Seg_7047" s="T569">Novorybnoe-DAT/LOC</ta>
            <ta e="T571" id="Seg_7048" s="T570">still</ta>
            <ta e="T572" id="Seg_7049" s="T571">stand-CVB.SIM</ta>
            <ta e="T573" id="Seg_7050" s="T572">fall-PST1-3SG</ta>
            <ta e="T574" id="Seg_7051" s="T573">EVID</ta>
            <ta e="T575" id="Seg_7052" s="T574">one</ta>
            <ta e="T576" id="Seg_7053" s="T575">director.[NOM]</ta>
            <ta e="T577" id="Seg_7054" s="T576">Momonov</ta>
            <ta e="T578" id="Seg_7055" s="T577">Vladimir</ta>
            <ta e="T579" id="Seg_7056" s="T578">Aleksandrovich.[NOM]</ta>
            <ta e="T580" id="Seg_7057" s="T579">say-CVB.SEQ</ta>
            <ta e="T581" id="Seg_7058" s="T580">human.being.[NOM]</ta>
            <ta e="T582" id="Seg_7059" s="T581">that.[NOM]</ta>
            <ta e="T583" id="Seg_7060" s="T582">that.[NOM]</ta>
            <ta e="T584" id="Seg_7061" s="T583">side-3SG-INSTR</ta>
            <ta e="T585" id="Seg_7062" s="T584">EMPH</ta>
            <ta e="T586" id="Seg_7063" s="T585">what.[NOM]</ta>
            <ta e="T587" id="Seg_7064" s="T586">NEG</ta>
            <ta e="T588" id="Seg_7065" s="T587">bad-ACC</ta>
            <ta e="T589" id="Seg_7066" s="T588">say-NEG-1SG</ta>
            <ta e="T590" id="Seg_7067" s="T589">outstanding-PROPR.[NOM]</ta>
            <ta e="T591" id="Seg_7068" s="T590">human.being.[NOM]</ta>
            <ta e="T592" id="Seg_7069" s="T591">eh</ta>
            <ta e="T593" id="Seg_7070" s="T592">human.being-PL-ACC</ta>
            <ta e="T594" id="Seg_7071" s="T593">with</ta>
            <ta e="T595" id="Seg_7072" s="T594">now</ta>
            <ta e="T596" id="Seg_7073" s="T595">a.little</ta>
            <ta e="T597" id="Seg_7074" s="T596">money-PROPR.[NOM]</ta>
            <ta e="T598" id="Seg_7075" s="T597">become-PST2-3PL</ta>
            <ta e="T599" id="Seg_7076" s="T598">that.[NOM]</ta>
            <ta e="T600" id="Seg_7077" s="T599">who.[NOM]</ta>
            <ta e="T601" id="Seg_7078" s="T600">be-PST1-3SG</ta>
            <ta e="T604" id="Seg_7079" s="T603">give-PTCP.HAB</ta>
            <ta e="T605" id="Seg_7080" s="T604">be-PST1-3PL</ta>
            <ta e="T606" id="Seg_7081" s="T605">production.[NOM]</ta>
            <ta e="T607" id="Seg_7082" s="T606">own-3SG-ACC</ta>
            <ta e="T608" id="Seg_7083" s="T607">well</ta>
            <ta e="T609" id="Seg_7084" s="T608">this.year</ta>
            <ta e="T610" id="Seg_7085" s="T609">make-PST2-3PL</ta>
            <ta e="T611" id="Seg_7086" s="T610">that-3SG-3PL-ACC</ta>
            <ta e="T612" id="Seg_7087" s="T611">EMPH</ta>
            <ta e="T613" id="Seg_7088" s="T612">take.away-VBZ-FUT-1PL</ta>
            <ta e="T614" id="Seg_7089" s="T613">that-ACC</ta>
            <ta e="T615" id="Seg_7090" s="T614">take.away-VBZ-TEMP-3PL</ta>
            <ta e="T616" id="Seg_7091" s="T615">now</ta>
            <ta e="T617" id="Seg_7092" s="T616">tundra-ADJZ.[NOM]</ta>
            <ta e="T618" id="Seg_7093" s="T617">eh</ta>
            <ta e="T619" id="Seg_7094" s="T618">hunter-PL.[NOM]</ta>
            <ta e="T620" id="Seg_7095" s="T619">EMPH</ta>
            <ta e="T621" id="Seg_7096" s="T620">also</ta>
            <ta e="T624" id="Seg_7097" s="T623">shoot-FREQ-NMNZ-AG.[NOM]</ta>
            <ta e="T625" id="Seg_7098" s="T624">human.being-PL.[NOM]</ta>
            <ta e="T626" id="Seg_7099" s="T625">EMPH</ta>
            <ta e="T627" id="Seg_7100" s="T626">little-ACC</ta>
            <ta e="T628" id="Seg_7101" s="T627">get-FUT-3PL</ta>
            <ta e="T629" id="Seg_7102" s="T628">however</ta>
            <ta e="T630" id="Seg_7103" s="T629">one</ta>
            <ta e="T633" id="Seg_7104" s="T631">be.able</ta>
            <ta e="T635" id="Seg_7105" s="T633">be.able-FUT-3PL</ta>
            <ta e="T636" id="Seg_7106" s="T635">NEG-3PL</ta>
            <ta e="T637" id="Seg_7107" s="T636">AFFIRM</ta>
            <ta e="T659" id="Seg_7108" s="T657">that-INSTR</ta>
            <ta e="T660" id="Seg_7109" s="T659">hope-PRS-1PL</ta>
            <ta e="T661" id="Seg_7110" s="T660">AFFIRM</ta>
            <ta e="T662" id="Seg_7111" s="T661">that-INSTR</ta>
            <ta e="T663" id="Seg_7112" s="T662">hope-PRS-1PL</ta>
            <ta e="T664" id="Seg_7113" s="T663">and</ta>
            <ta e="T665" id="Seg_7114" s="T664">so</ta>
            <ta e="T666" id="Seg_7115" s="T665">that.[NOM]</ta>
            <ta e="T667" id="Seg_7116" s="T666">dark</ta>
            <ta e="T668" id="Seg_7117" s="T667">side.[NOM]</ta>
            <ta e="T669" id="Seg_7118" s="T668">side-3SG-ACC</ta>
            <ta e="T670" id="Seg_7119" s="T669">chat-PST1-1PL</ta>
            <ta e="T671" id="Seg_7120" s="T670">1PL.[NOM]</ta>
            <ta e="T672" id="Seg_7121" s="T671">and</ta>
            <ta e="T673" id="Seg_7122" s="T672">so</ta>
            <ta e="T674" id="Seg_7123" s="T673">EMPH</ta>
            <ta e="T675" id="Seg_7124" s="T674">tundra-DAT/LOC</ta>
            <ta e="T676" id="Seg_7125" s="T675">live-PTCP.COND-DAT/LOC</ta>
            <ta e="T677" id="Seg_7126" s="T676">very</ta>
            <ta e="T678" id="Seg_7127" s="T677">happy.[NOM]</ta>
            <ta e="T679" id="Seg_7128" s="T678">be-PST1-3SG</ta>
            <ta e="T680" id="Seg_7129" s="T679">eh</ta>
            <ta e="T681" id="Seg_7130" s="T680">tundra-ADJZ.[NOM]</ta>
            <ta e="T682" id="Seg_7131" s="T681">human.being-PL.[NOM]</ta>
            <ta e="T683" id="Seg_7132" s="T682">now</ta>
            <ta e="T684" id="Seg_7133" s="T683">child-PL.[NOM]</ta>
            <ta e="T685" id="Seg_7134" s="T684">go-EP-NEG.PTCP</ta>
            <ta e="T686" id="Seg_7135" s="T685">be-CVB.SEQ-3PL</ta>
            <ta e="T687" id="Seg_7136" s="T686">now</ta>
            <ta e="T688" id="Seg_7137" s="T687">know-NEG-3PL</ta>
            <ta e="T689" id="Seg_7138" s="T688">EMPH</ta>
            <ta e="T690" id="Seg_7139" s="T689">Dolgan.[NOM]</ta>
            <ta e="T691" id="Seg_7140" s="T690">language-3SG-ACC</ta>
            <ta e="T692" id="Seg_7141" s="T691">clean.[NOM]</ta>
            <ta e="T693" id="Seg_7142" s="T692">breath-3PL.[NOM]</ta>
            <ta e="T694" id="Seg_7143" s="T693">eh</ta>
            <ta e="T695" id="Seg_7144" s="T694">air-ACC</ta>
            <ta e="T696" id="Seg_7145" s="T695">grass.[NOM]</ta>
            <ta e="T697" id="Seg_7146" s="T696">smell-3SG-ACC</ta>
            <ta e="T698" id="Seg_7147" s="T697">say-PTCP.PST-DAT/LOC</ta>
            <ta e="T699" id="Seg_7148" s="T698">say-CVB.PURP</ta>
            <ta e="T700" id="Seg_7149" s="T699">first</ta>
            <ta e="T701" id="Seg_7150" s="T700">goose.[NOM]</ta>
            <ta e="T702" id="Seg_7151" s="T701">noise-3SG-ACC</ta>
            <ta e="T703" id="Seg_7152" s="T702">calf-PL.[NOM]</ta>
            <ta e="T704" id="Seg_7153" s="T703">be.born-PTCP.PRS-3PL-ACC</ta>
            <ta e="T705" id="Seg_7154" s="T704">whole-3SG-ACC</ta>
            <ta e="T706" id="Seg_7155" s="T705">what-ACC</ta>
            <ta e="T707" id="Seg_7156" s="T706">NEG</ta>
            <ta e="T708" id="Seg_7157" s="T707">very</ta>
            <ta e="T709" id="Seg_7158" s="T708">know-NEG-3PL</ta>
            <ta e="T710" id="Seg_7159" s="T709">EMPH</ta>
            <ta e="T711" id="Seg_7160" s="T710">that-3SG-2SG.[NOM]</ta>
            <ta e="T712" id="Seg_7161" s="T711">every-3SG.[NOM]</ta>
            <ta e="T713" id="Seg_7162" s="T712">1PL-DAT/LOC</ta>
            <ta e="T714" id="Seg_7163" s="T713">eh</ta>
            <ta e="T715" id="Seg_7164" s="T714">tundra-DAT/LOC</ta>
            <ta e="T716" id="Seg_7165" s="T715">be.born-PTCP.PST</ta>
            <ta e="T717" id="Seg_7166" s="T716">human.being-PL-DAT/LOC</ta>
            <ta e="T718" id="Seg_7167" s="T717">that-3SG-2SG.[NOM]</ta>
            <ta e="T719" id="Seg_7168" s="T718">every-3SG.[NOM]</ta>
            <ta e="T720" id="Seg_7169" s="T719">every-3SG.[NOM]</ta>
            <ta e="T721" id="Seg_7170" s="T720">big.[NOM]</ta>
            <ta e="T722" id="Seg_7171" s="T721">who.[NOM]</ta>
            <ta e="T723" id="Seg_7172" s="T722">EMPH</ta>
            <ta e="T725" id="Seg_7173" s="T723">impression.[NOM]</ta>
            <ta e="T764" id="Seg_7174" s="T763">thanks</ta>
         </annotation>
         <annotation name="gg" tierref="gg-UkOA">
            <ta e="T8" id="Seg_7175" s="T7">ah</ta>
            <ta e="T9" id="Seg_7176" s="T8">selbst-1SG.[NOM]</ta>
            <ta e="T10" id="Seg_7177" s="T9">Chatanga-DAT/LOC</ta>
            <ta e="T11" id="Seg_7178" s="T10">Haus-PROPR.[NOM]</ta>
            <ta e="T12" id="Seg_7179" s="T11">sein-CVB.SEQ-1SG</ta>
            <ta e="T13" id="Seg_7180" s="T12">äh</ta>
            <ta e="T14" id="Seg_7181" s="T13">Tundra-DAT/LOC</ta>
            <ta e="T15" id="Seg_7182" s="T14">wenig-ADVZ</ta>
            <ta e="T16" id="Seg_7183" s="T15">sein-PRS-1SG</ta>
            <ta e="T17" id="Seg_7184" s="T16">EMPH</ta>
            <ta e="T18" id="Seg_7185" s="T17">doch</ta>
            <ta e="T19" id="Seg_7186" s="T18">eins</ta>
            <ta e="T20" id="Seg_7187" s="T19">Frühling-PL-ACC</ta>
            <ta e="T21" id="Seg_7188" s="T20">Herbst-PL-ACC</ta>
            <ta e="T22" id="Seg_7189" s="T21">vielleicht</ta>
            <ta e="T23" id="Seg_7190" s="T22">immer</ta>
            <ta e="T24" id="Seg_7191" s="T23">Tundra-DAT/LOC</ta>
            <ta e="T25" id="Seg_7192" s="T24">gehen-PRS-1SG</ta>
            <ta e="T26" id="Seg_7193" s="T25">wie.viel</ta>
            <ta e="T27" id="Seg_7194" s="T26">Jahr.[NOM]</ta>
            <ta e="T28" id="Seg_7195" s="T27">jeder</ta>
            <ta e="T29" id="Seg_7196" s="T28">Mutter-1SG-PROPR-1SG-DAT/LOC</ta>
            <ta e="T30" id="Seg_7197" s="T29">Vater-1SG-PROPR-1SG-DAT/LOC</ta>
            <ta e="T31" id="Seg_7198" s="T30">helfen-CVB.SIM</ta>
            <ta e="T32" id="Seg_7199" s="T31">jenes.[NOM]</ta>
            <ta e="T33" id="Seg_7200" s="T32">zu</ta>
            <ta e="T34" id="Seg_7201" s="T33">Tundra-DAT/LOC</ta>
            <ta e="T35" id="Seg_7202" s="T34">gehen-CVB.SIM</ta>
            <ta e="T36" id="Seg_7203" s="T35">sein-CVB.SEQ-3PL</ta>
            <ta e="T38" id="Seg_7204" s="T37">Rentier-PROPR-3PL</ta>
            <ta e="T39" id="Seg_7205" s="T38">EMPH</ta>
            <ta e="T40" id="Seg_7206" s="T39">jenes</ta>
            <ta e="T41" id="Seg_7207" s="T40">Rentier-PL.[NOM]</ta>
            <ta e="T42" id="Seg_7208" s="T41">Rentier-3PL-ACC</ta>
            <ta e="T43" id="Seg_7209" s="T42">EMPH</ta>
            <ta e="T44" id="Seg_7210" s="T43">sehen-CVB.SIM</ta>
            <ta e="T45" id="Seg_7211" s="T44">bald</ta>
            <ta e="T46" id="Seg_7212" s="T45">EMPH</ta>
            <ta e="T47" id="Seg_7213" s="T46">solch-ACC</ta>
            <ta e="T48" id="Seg_7214" s="T47">selbst-1SG.[NOM]</ta>
            <ta e="T49" id="Seg_7215" s="T48">EMPH</ta>
            <ta e="T51" id="Seg_7216" s="T50">wer-VBZ-PRS-1SG</ta>
            <ta e="T52" id="Seg_7217" s="T51">laden-EP-PASS/REFL-PRS-1SG</ta>
            <ta e="T53" id="Seg_7218" s="T52">äh</ta>
            <ta e="T54" id="Seg_7219" s="T53">wer-EP-INSTR</ta>
            <ta e="T55" id="Seg_7220" s="T54">Energie-INSTR</ta>
            <ta e="T56" id="Seg_7221" s="T55">sagen-PTCP.PST-DAT/LOC</ta>
            <ta e="T57" id="Seg_7222" s="T56">sagen-CVB.PURP</ta>
            <ta e="T60" id="Seg_7223" s="T59">Chatanga-DAT/LOC</ta>
            <ta e="T61" id="Seg_7224" s="T60">liegen-CVB.SEQ</ta>
            <ta e="T62" id="Seg_7225" s="T61">Atem-3SG-ACC</ta>
            <ta e="T63" id="Seg_7226" s="T62">versperren-PRS-3PL</ta>
            <ta e="T64" id="Seg_7227" s="T63">jenes.EMPH.[NOM]</ta>
            <ta e="T65" id="Seg_7228" s="T64">nachdem</ta>
            <ta e="T85" id="Seg_7229" s="T84">Tundra-ADJZ-PL-EP-2SG.[NOM]</ta>
            <ta e="T86" id="Seg_7230" s="T85">selbst-3PL.[NOM]</ta>
            <ta e="T87" id="Seg_7231" s="T86">wissen-PRS-3PL</ta>
            <ta e="T88" id="Seg_7232" s="T87">EMPH</ta>
            <ta e="T89" id="Seg_7233" s="T88">wie</ta>
            <ta e="T90" id="Seg_7234" s="T89">leben-PTCP.FUT-3PL-ACC</ta>
            <ta e="T91" id="Seg_7235" s="T90">jetzt</ta>
            <ta e="T92" id="Seg_7236" s="T91">gut.[NOM]</ta>
            <ta e="T93" id="Seg_7237" s="T92">Leben-ACC</ta>
            <ta e="T94" id="Seg_7238" s="T93">wissen-NEG.PTCP</ta>
            <ta e="T95" id="Seg_7239" s="T94">sein-CVB.SEQ-3PL</ta>
            <ta e="T96" id="Seg_7240" s="T95">3PL.[NOM]</ta>
            <ta e="T97" id="Seg_7241" s="T96">äh</ta>
            <ta e="T98" id="Seg_7242" s="T97">gut-ADVZ</ta>
            <ta e="T99" id="Seg_7243" s="T98">leben-FUT-1PL</ta>
            <ta e="T100" id="Seg_7244" s="T99">sagen-CVB.SEQ</ta>
            <ta e="T101" id="Seg_7245" s="T100">gehen-NEG-3PL</ta>
            <ta e="T102" id="Seg_7246" s="T101">offenbar</ta>
            <ta e="T103" id="Seg_7247" s="T102">sehr</ta>
            <ta e="T104" id="Seg_7248" s="T103">EMPH</ta>
            <ta e="T105" id="Seg_7249" s="T104">selbst-3PL.[NOM]</ta>
            <ta e="T106" id="Seg_7250" s="T105">EMPH</ta>
            <ta e="T107" id="Seg_7251" s="T106">was.[NOM]</ta>
            <ta e="T108" id="Seg_7252" s="T107">es.gibt</ta>
            <ta e="T109" id="Seg_7253" s="T108">jenes-INSTR</ta>
            <ta e="T110" id="Seg_7254" s="T109">sich.freuen-CVB.SIM</ta>
            <ta e="T111" id="Seg_7255" s="T110">gehen-PRS-3PL</ta>
            <ta e="T112" id="Seg_7256" s="T111">NEG.EX</ta>
            <ta e="T113" id="Seg_7257" s="T112">sein-TEMP-3SG</ta>
            <ta e="T114" id="Seg_7258" s="T113">wie</ta>
            <ta e="T115" id="Seg_7259" s="T114">machen-FUT.[3SG]=Q</ta>
            <ta e="T116" id="Seg_7260" s="T115">NEG.EX</ta>
            <ta e="T117" id="Seg_7261" s="T116">EMPH</ta>
            <ta e="T119" id="Seg_7262" s="T117">NEG.EX</ta>
            <ta e="T124" id="Seg_7263" s="T123">sehr</ta>
            <ta e="T125" id="Seg_7264" s="T124">EMPH</ta>
            <ta e="T126" id="Seg_7265" s="T125">traurig.sein-NEG-3PL</ta>
            <ta e="T127" id="Seg_7266" s="T126">AFFIRM</ta>
            <ta e="T128" id="Seg_7267" s="T127">und</ta>
            <ta e="T129" id="Seg_7268" s="T128">wenn</ta>
            <ta e="T130" id="Seg_7269" s="T129">dieses</ta>
            <ta e="T132" id="Seg_7270" s="T131">gut-ADVZ</ta>
            <ta e="T133" id="Seg_7271" s="T132">äh</ta>
            <ta e="T134" id="Seg_7272" s="T133">Mensch-SIM</ta>
            <ta e="T135" id="Seg_7273" s="T134">dieses</ta>
            <ta e="T136" id="Seg_7274" s="T135">echt-SIM</ta>
            <ta e="T137" id="Seg_7275" s="T136">leben-PTCP.PRS</ta>
            <ta e="T138" id="Seg_7276" s="T137">Mensch.[NOM]</ta>
            <ta e="T139" id="Seg_7277" s="T138">dieses.[NOM]</ta>
            <ta e="T140" id="Seg_7278" s="T139">tatsächlich</ta>
            <ta e="T141" id="Seg_7279" s="T140">EMPH</ta>
            <ta e="T142" id="Seg_7280" s="T141">zugänglich</ta>
            <ta e="T143" id="Seg_7281" s="T142">EMPH</ta>
            <ta e="T144" id="Seg_7282" s="T143">Balok-PL-ACC</ta>
            <ta e="T145" id="Seg_7283" s="T144">Verwaltung-ABL</ta>
            <ta e="T146" id="Seg_7284" s="T145">geben-PTCP.FUT</ta>
            <ta e="T147" id="Seg_7285" s="T146">sein-PST1-3PL</ta>
            <ta e="T148" id="Seg_7286" s="T147">MOD</ta>
            <ta e="T149" id="Seg_7287" s="T148">dieses</ta>
            <ta e="T150" id="Seg_7288" s="T149">unverzichtbar.[NOM]</ta>
            <ta e="T151" id="Seg_7289" s="T150">meist</ta>
            <ta e="T152" id="Seg_7290" s="T151">meist</ta>
            <ta e="T153" id="Seg_7291" s="T152">Bedarf-PROPR.[NOM]</ta>
            <ta e="T154" id="Seg_7292" s="T153">Ding-3PL.[NOM]</ta>
            <ta e="T155" id="Seg_7293" s="T154">was-3PL.[NOM]</ta>
            <ta e="T157" id="Seg_7294" s="T156">dieses</ta>
            <ta e="T158" id="Seg_7295" s="T157">nun</ta>
            <ta e="T159" id="Seg_7296" s="T158">Hilfe.[NOM]</ta>
            <ta e="T160" id="Seg_7297" s="T159">Zuschuss.[NOM]</ta>
            <ta e="T161" id="Seg_7298" s="T160">ähnlich</ta>
            <ta e="T162" id="Seg_7299" s="T161">groß-ADVZ</ta>
            <ta e="T163" id="Seg_7300" s="T162">kommen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T164" id="Seg_7301" s="T163">sein-CVB.SIM</ta>
            <ta e="T165" id="Seg_7302" s="T164">Tundra-ADJZ-PL-DAT/LOC</ta>
            <ta e="T166" id="Seg_7303" s="T165">selbst-3PL-GEN</ta>
            <ta e="T167" id="Seg_7304" s="T166">selbst-3PL-GEN</ta>
            <ta e="T168" id="Seg_7305" s="T167">Fähigkeit-3PL-INSTR</ta>
            <ta e="T169" id="Seg_7306" s="T168">gehen-EP-MED-EMOT-PRS-3PL</ta>
            <ta e="T170" id="Seg_7307" s="T169">Tundra-ADJZ-PL-1PL.[NOM]</ta>
            <ta e="T171" id="Seg_7308" s="T170">jenes.[NOM]</ta>
            <ta e="T172" id="Seg_7309" s="T171">wegen</ta>
            <ta e="T173" id="Seg_7310" s="T172">jetzt</ta>
            <ta e="T174" id="Seg_7311" s="T173">noch</ta>
            <ta e="T175" id="Seg_7312" s="T174">dieses.[NOM]</ta>
            <ta e="T176" id="Seg_7313" s="T175">tausend</ta>
            <ta e="T177" id="Seg_7314" s="T176">fünf</ta>
            <ta e="T178" id="Seg_7315" s="T177">hundert</ta>
            <ta e="T179" id="Seg_7316" s="T178">dieses.[NOM]</ta>
            <ta e="T180" id="Seg_7317" s="T179">wer.[NOM]</ta>
            <ta e="T181" id="Seg_7318" s="T180">heftig</ta>
            <ta e="T182" id="Seg_7319" s="T181">1500.Rubel-ACC</ta>
            <ta e="T183" id="Seg_7320" s="T182">geben-CVB.SEQ</ta>
            <ta e="T184" id="Seg_7321" s="T183">sein-CVB.SEQ-3PL</ta>
            <ta e="T185" id="Seg_7322" s="T184">noch</ta>
            <ta e="T186" id="Seg_7323" s="T185">ein.Bisschen</ta>
            <ta e="T187" id="Seg_7324" s="T186">ah</ta>
            <ta e="T188" id="Seg_7325" s="T187">und</ta>
            <ta e="T189" id="Seg_7326" s="T188">so</ta>
            <ta e="T190" id="Seg_7327" s="T189">ganz</ta>
            <ta e="T191" id="Seg_7328" s="T190">EMPH</ta>
            <ta e="T192" id="Seg_7329" s="T191">was.[NOM]</ta>
            <ta e="T193" id="Seg_7330" s="T192">sehr</ta>
            <ta e="T194" id="Seg_7331" s="T193">EMPH</ta>
            <ta e="T196" id="Seg_7332" s="T195">Nahrung-EP-INSTR</ta>
            <ta e="T197" id="Seg_7333" s="T196">EMPH</ta>
            <ta e="T198" id="Seg_7334" s="T197">Brot-EP-INSTR</ta>
            <ta e="T199" id="Seg_7335" s="T198">NEG</ta>
            <ta e="T200" id="Seg_7336" s="T199">was-EP-INSTR</ta>
            <ta e="T201" id="Seg_7337" s="T200">NEG</ta>
            <ta e="T202" id="Seg_7338" s="T201">jenes</ta>
            <ta e="T203" id="Seg_7339" s="T202">Bäckerei-1PL.[NOM]</ta>
            <ta e="T204" id="Seg_7340" s="T203">arbeiten-PTCP.NEG</ta>
            <ta e="T205" id="Seg_7341" s="T204">sein-CVB.SEQ</ta>
            <ta e="T206" id="Seg_7342" s="T205">selbst-3PL.[NOM]</ta>
            <ta e="T207" id="Seg_7343" s="T206">kochen-EP-CAUS-EP-MED-EMOT-PRS-3PL</ta>
            <ta e="T208" id="Seg_7344" s="T207">Tundra-DAT/LOC</ta>
            <ta e="T209" id="Seg_7345" s="T208">gehen-CVB.SEQ-3PL</ta>
            <ta e="T210" id="Seg_7346" s="T209">was.für.ein</ta>
            <ta e="T211" id="Seg_7347" s="T210">was.für.ein</ta>
            <ta e="T212" id="Seg_7348" s="T211">jung.[NOM]</ta>
            <ta e="T213" id="Seg_7349" s="T212">Mensch.[NOM]</ta>
            <ta e="T214" id="Seg_7350" s="T213">gehen-FUT-1SG</ta>
            <ta e="T215" id="Seg_7351" s="T214">sagen-FUT.[3SG]=Q</ta>
            <ta e="T216" id="Seg_7352" s="T215">warm.[NOM]</ta>
            <ta e="T217" id="Seg_7353" s="T216">Haus-ABL</ta>
            <ta e="T218" id="Seg_7354" s="T217">warm</ta>
            <ta e="T219" id="Seg_7355" s="T218">Feuer-ABL</ta>
            <ta e="T220" id="Seg_7356" s="T219">hinausgehen-CVB.SEQ</ta>
            <ta e="T221" id="Seg_7357" s="T220">Kälte-DAT/LOC</ta>
            <ta e="T222" id="Seg_7358" s="T221">Wind-DAT/LOC</ta>
            <ta e="T223" id="Seg_7359" s="T222">hinausgehen-CVB.SEQ</ta>
            <ta e="T224" id="Seg_7360" s="T223">Rentier-ACC</ta>
            <ta e="T225" id="Seg_7361" s="T224">hüten-CVB.SIM</ta>
            <ta e="T226" id="Seg_7362" s="T225">gehen-FUT-1SG</ta>
            <ta e="T227" id="Seg_7363" s="T226">sagen-CVB.SEQ</ta>
            <ta e="T228" id="Seg_7364" s="T227">Q</ta>
            <ta e="T229" id="Seg_7365" s="T228">bezahlen-NEG-3PL</ta>
            <ta e="T230" id="Seg_7366" s="T229">EMPH</ta>
            <ta e="T231" id="Seg_7367" s="T230">jenes</ta>
            <ta e="T232" id="Seg_7368" s="T231">dann</ta>
            <ta e="T233" id="Seg_7369" s="T232">MOD</ta>
            <ta e="T234" id="Seg_7370" s="T233">Geld-ACC</ta>
            <ta e="T235" id="Seg_7371" s="T234">sehen-NEG-3PL</ta>
            <ta e="T236" id="Seg_7372" s="T235">Tundra-ADJZ-PL.[NOM]</ta>
            <ta e="T237" id="Seg_7373" s="T236">vor.Kurzem</ta>
            <ta e="T238" id="Seg_7374" s="T237">Plane.[NOM]</ta>
            <ta e="T239" id="Seg_7375" s="T238">was.[NOM]</ta>
            <ta e="T240" id="Seg_7376" s="T239">bringen-PRS-3PL</ta>
            <ta e="T242" id="Seg_7377" s="T241">Geschäftsmann-PL.[NOM]</ta>
            <ta e="T243" id="Seg_7378" s="T242">verkaufen-PRS-3PL</ta>
            <ta e="T244" id="Seg_7379" s="T243">sehr</ta>
            <ta e="T245" id="Seg_7380" s="T244">teuer-DAT/LOC</ta>
            <ta e="T246" id="Seg_7381" s="T245">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T247" id="Seg_7382" s="T246">was.[NOM]</ta>
            <ta e="T248" id="Seg_7383" s="T247">Mensch-3SG.[NOM]=Q</ta>
            <ta e="T249" id="Seg_7384" s="T248">kaufen-CVB.SEQ</ta>
            <ta e="T250" id="Seg_7385" s="T249">nehmen-FUT.[3SG]=Q</ta>
            <ta e="T251" id="Seg_7386" s="T250">Q</ta>
            <ta e="T252" id="Seg_7387" s="T251">äh</ta>
            <ta e="T253" id="Seg_7388" s="T252">Nahrung-ACC</ta>
            <ta e="T254" id="Seg_7389" s="T253">Nahrung-ACC</ta>
            <ta e="T255" id="Seg_7390" s="T254">kaufen-FUT.[3SG]=Q</ta>
            <ta e="T256" id="Seg_7391" s="T255">Q</ta>
            <ta e="T257" id="Seg_7392" s="T256">Nahrung-ACC</ta>
            <ta e="T258" id="Seg_7393" s="T257">kaufen-FUT-2SG</ta>
            <ta e="T259" id="Seg_7394" s="T258">Q</ta>
            <ta e="T260" id="Seg_7395" s="T259">Plane.[NOM]</ta>
            <ta e="T261" id="Seg_7396" s="T260">kaufen-FUT-2SG</ta>
            <ta e="T262" id="Seg_7397" s="T261">Q</ta>
            <ta e="T263" id="Seg_7398" s="T262">Kleidung.[NOM]</ta>
            <ta e="T264" id="Seg_7399" s="T263">kaufen-FUT-2SG</ta>
            <ta e="T265" id="Seg_7400" s="T264">Q</ta>
            <ta e="T266" id="Seg_7401" s="T265">Kind-PL-EP-2SG.[NOM]</ta>
            <ta e="T267" id="Seg_7402" s="T266">noch</ta>
            <ta e="T268" id="Seg_7403" s="T267">lernen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T269" id="Seg_7404" s="T268">man.muss</ta>
            <ta e="T270" id="Seg_7405" s="T269">was.[NOM]</ta>
            <ta e="T271" id="Seg_7406" s="T270">EMPH</ta>
            <ta e="T272" id="Seg_7407" s="T271">jetzt</ta>
            <ta e="T273" id="Seg_7408" s="T272">sein-CVB.SIM</ta>
            <ta e="T274" id="Seg_7409" s="T273">früher</ta>
            <ta e="T275" id="Seg_7410" s="T274">Student-PL-ACC</ta>
            <ta e="T276" id="Seg_7411" s="T275">was-PL-ACC</ta>
            <ta e="T277" id="Seg_7412" s="T276">wenn.auch</ta>
            <ta e="T278" id="Seg_7413" s="T277">geben-PTCP.HAB</ta>
            <ta e="T279" id="Seg_7414" s="T278">sein-PST1-3PL</ta>
            <ta e="T280" id="Seg_7415" s="T279">3PL-DAT/LOC</ta>
            <ta e="T281" id="Seg_7416" s="T280">was.[NOM]</ta>
            <ta e="T282" id="Seg_7417" s="T281">INDEF</ta>
            <ta e="T283" id="Seg_7418" s="T282">Zuschuss.[NOM]</ta>
            <ta e="T284" id="Seg_7419" s="T283">vielleicht</ta>
            <ta e="T285" id="Seg_7420" s="T284">Sowchose-ABL</ta>
            <ta e="T286" id="Seg_7421" s="T285">abordnen-VBZ-PTCP.HAB</ta>
            <ta e="T287" id="Seg_7422" s="T286">sein-PST1-3PL</ta>
            <ta e="T288" id="Seg_7423" s="T287">jetzt</ta>
            <ta e="T289" id="Seg_7424" s="T288">jenes</ta>
            <ta e="T290" id="Seg_7425" s="T289">solch</ta>
            <ta e="T291" id="Seg_7426" s="T290">was.[NOM]</ta>
            <ta e="T292" id="Seg_7427" s="T291">NEG</ta>
            <ta e="T293" id="Seg_7428" s="T292">NEG.EX</ta>
            <ta e="T294" id="Seg_7429" s="T293">ganz-3SG.[NOM]</ta>
            <ta e="T295" id="Seg_7430" s="T294">Mensch.[NOM]</ta>
            <ta e="T296" id="Seg_7431" s="T295">selbst-3SG-GEN</ta>
            <ta e="T297" id="Seg_7432" s="T296">Geld-3SG-INSTR</ta>
            <ta e="T298" id="Seg_7433" s="T297">gehen-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_7434" s="T298">wer.[NOM]</ta>
            <ta e="T300" id="Seg_7435" s="T299">Fähigkeit-POSS</ta>
            <ta e="T301" id="Seg_7436" s="T300">NEG</ta>
            <ta e="T302" id="Seg_7437" s="T301">Mensch.[NOM]</ta>
            <ta e="T303" id="Seg_7438" s="T302">ganz</ta>
            <ta e="T304" id="Seg_7439" s="T303">sterben-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_7440" s="T304">sagen-PTCP.PST-DAT/LOC</ta>
            <ta e="T306" id="Seg_7441" s="T305">sagen-CVB.PURP</ta>
            <ta e="T307" id="Seg_7442" s="T306">dieses.EMPH</ta>
            <ta e="T308" id="Seg_7443" s="T307">denken-CVB.SEQ</ta>
            <ta e="T309" id="Seg_7444" s="T308">und</ta>
            <ta e="T310" id="Seg_7445" s="T309">wer.[NOM]</ta>
            <ta e="T311" id="Seg_7446" s="T310">Fähigkeit-PROPR-EP-2SG.[NOM]</ta>
            <ta e="T312" id="Seg_7447" s="T311">ein.Bisschen</ta>
            <ta e="T313" id="Seg_7448" s="T312">lehren-PRS-3PL</ta>
            <ta e="T314" id="Seg_7449" s="T313">Kind-PL-3SG-ACC</ta>
            <ta e="T315" id="Seg_7450" s="T314">doch</ta>
            <ta e="T316" id="Seg_7451" s="T315">eins</ta>
            <ta e="T321" id="Seg_7452" s="T319">leben-PRS-3PL</ta>
            <ta e="T336" id="Seg_7453" s="T335">na</ta>
            <ta e="T337" id="Seg_7454" s="T336">Herr-PL.[NOM]</ta>
            <ta e="T338" id="Seg_7455" s="T337">nur</ta>
            <ta e="T339" id="Seg_7456" s="T338">Herr-PL.[NOM]</ta>
            <ta e="T340" id="Seg_7457" s="T339">nur</ta>
            <ta e="T341" id="Seg_7458" s="T340">helfen-FUT-3PL</ta>
            <ta e="T342" id="Seg_7459" s="T341">1PL.[NOM]</ta>
            <ta e="T343" id="Seg_7460" s="T342">Erde-1PL-DAT/LOC</ta>
            <ta e="T344" id="Seg_7461" s="T343">Leben-1PL.[NOM]</ta>
            <ta e="T345" id="Seg_7462" s="T344">verbessern-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T346" id="Seg_7463" s="T345">ganz</ta>
            <ta e="T347" id="Seg_7464" s="T346">3PL-ABL</ta>
            <ta e="T348" id="Seg_7465" s="T347">nur</ta>
            <ta e="T350" id="Seg_7466" s="T348">hinausgehen-FUT-3SG</ta>
            <ta e="T362" id="Seg_7467" s="T360">selbst</ta>
            <ta e="T365" id="Seg_7468" s="T362">selbst-1PL.[NOM]</ta>
            <ta e="T368" id="Seg_7469" s="T365">jetzt</ta>
            <ta e="T369" id="Seg_7470" s="T368">Mensch-1PL.[NOM]</ta>
            <ta e="T370" id="Seg_7471" s="T369">jetzt</ta>
            <ta e="T371" id="Seg_7472" s="T370">Mensch-PL-1PL.[NOM]</ta>
            <ta e="T372" id="Seg_7473" s="T371">MOD</ta>
            <ta e="T373" id="Seg_7474" s="T372">im.Grunde</ta>
            <ta e="T374" id="Seg_7475" s="T373">viel</ta>
            <ta e="T375" id="Seg_7476" s="T374">Mensch.[NOM]</ta>
            <ta e="T376" id="Seg_7477" s="T375">äh</ta>
            <ta e="T377" id="Seg_7478" s="T376">hineingehen-PST2-3SG</ta>
            <ta e="T378" id="Seg_7479" s="T377">Tundra-ABL</ta>
            <ta e="T379" id="Seg_7480" s="T378">Fähigkeit-POSS</ta>
            <ta e="T380" id="Seg_7481" s="T379">NEG</ta>
            <ta e="T381" id="Seg_7482" s="T380">sein-CVB.SEQ-3PL</ta>
            <ta e="T382" id="Seg_7483" s="T381">jeder-3PL.[NOM]</ta>
            <ta e="T383" id="Seg_7484" s="T382">groß.[NOM]</ta>
            <ta e="T384" id="Seg_7485" s="T383">Erfahrung-PROPR-3PL.[NOM]</ta>
            <ta e="T385" id="Seg_7486" s="T384">Rentier-DAT/LOC</ta>
            <ta e="T386" id="Seg_7487" s="T385">gehen-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T387" id="Seg_7488" s="T386">EMPH</ta>
            <ta e="T388" id="Seg_7489" s="T387">äh</ta>
            <ta e="T389" id="Seg_7490" s="T388">jenes</ta>
            <ta e="T390" id="Seg_7491" s="T389">jeder-3SG.[NOM]</ta>
            <ta e="T391" id="Seg_7492" s="T390">jenes</ta>
            <ta e="T392" id="Seg_7493" s="T391">jeder-3SG.[NOM]</ta>
            <ta e="T393" id="Seg_7494" s="T392">jenes</ta>
            <ta e="T394" id="Seg_7495" s="T393">jeder-3SG.[NOM]</ta>
            <ta e="T395" id="Seg_7496" s="T394">sterben-PRS.[3SG]</ta>
            <ta e="T396" id="Seg_7497" s="T395">Mensch.[NOM]</ta>
            <ta e="T397" id="Seg_7498" s="T396">jeder-3SG.[NOM]</ta>
            <ta e="T398" id="Seg_7499" s="T397">äh</ta>
            <ta e="T399" id="Seg_7500" s="T398">also</ta>
            <ta e="T400" id="Seg_7501" s="T399">Ort-DAT/LOC</ta>
            <ta e="T401" id="Seg_7502" s="T400">bleiben-PRS.[3SG]</ta>
            <ta e="T403" id="Seg_7503" s="T402">Ort-DAT/LOC</ta>
            <ta e="T404" id="Seg_7504" s="T403">bleiben-PRS.[3SG]</ta>
            <ta e="T405" id="Seg_7505" s="T404">Mensch.[NOM]</ta>
            <ta e="T406" id="Seg_7506" s="T405">jeder-3SG.[NOM]</ta>
            <ta e="T407" id="Seg_7507" s="T406">jetzt</ta>
            <ta e="T408" id="Seg_7508" s="T407">gehen-PTCP.PRS</ta>
            <ta e="T409" id="Seg_7509" s="T408">EMPH</ta>
            <ta e="T410" id="Seg_7510" s="T409">Mensch-2SG.[NOM]</ta>
            <ta e="T411" id="Seg_7511" s="T410">sehr</ta>
            <ta e="T412" id="Seg_7512" s="T411">Erde-ACC</ta>
            <ta e="T413" id="Seg_7513" s="T412">wissen-NEG-3PL</ta>
            <ta e="T414" id="Seg_7514" s="T413">jung.[NOM]</ta>
            <ta e="T415" id="Seg_7515" s="T414">Mensch-PL-EP-2SG.[NOM]</ta>
            <ta e="T416" id="Seg_7516" s="T415">jetzt</ta>
            <ta e="T417" id="Seg_7517" s="T416">gehen-CVB.SEQ-3PL</ta>
            <ta e="T418" id="Seg_7518" s="T417">EMPH</ta>
            <ta e="T419" id="Seg_7519" s="T418">hinausgehen-CVB.SEQ-3PL</ta>
            <ta e="T420" id="Seg_7520" s="T419">EMPH</ta>
            <ta e="T421" id="Seg_7521" s="T420">zum.Beispiel</ta>
            <ta e="T422" id="Seg_7522" s="T421">Rentierflechte-PROPR.[NOM]</ta>
            <ta e="T423" id="Seg_7523" s="T422">Ort-PL.[NOM]</ta>
            <ta e="T424" id="Seg_7524" s="T423">jetzt</ta>
            <ta e="T425" id="Seg_7525" s="T424">wildes.Rentier-1PL.[NOM]</ta>
            <ta e="T426" id="Seg_7526" s="T425">jetzt</ta>
            <ta e="T427" id="Seg_7527" s="T426">Ort-3SG-ACC</ta>
            <ta e="T428" id="Seg_7528" s="T427">ändern-CVB.SEQ</ta>
            <ta e="T429" id="Seg_7529" s="T428">1PL-INSTR</ta>
            <ta e="T430" id="Seg_7530" s="T429">viel-ACC</ta>
            <ta e="T431" id="Seg_7531" s="T430">fallen-PTCP.PRS</ta>
            <ta e="T432" id="Seg_7532" s="T431">sein-CVB.SEQ</ta>
            <ta e="T433" id="Seg_7533" s="T432">Rentierflechte-1PL-ACC</ta>
            <ta e="T434" id="Seg_7534" s="T433">verbrauchen-CAUS-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_7535" s="T434">jenes.[NOM]</ta>
            <ta e="T436" id="Seg_7536" s="T435">wegen</ta>
            <ta e="T437" id="Seg_7537" s="T436">gehen-PTCP.PRS</ta>
            <ta e="T438" id="Seg_7538" s="T437">Ort-PL-1PL.[NOM]</ta>
            <ta e="T439" id="Seg_7539" s="T438">enger.werden-PST2-3PL</ta>
            <ta e="T440" id="Seg_7540" s="T439">Rentier-DAT/LOC</ta>
            <ta e="T441" id="Seg_7541" s="T440">EMPH</ta>
            <ta e="T442" id="Seg_7542" s="T441">wie</ta>
            <ta e="T443" id="Seg_7543" s="T442">Rentier-DAT/LOC</ta>
            <ta e="T444" id="Seg_7544" s="T443">so</ta>
            <ta e="T445" id="Seg_7545" s="T444">Mensch-DAT/LOC</ta>
            <ta e="T446" id="Seg_7546" s="T445">EMPH</ta>
            <ta e="T447" id="Seg_7547" s="T446">Mensch-PL-1PL.[NOM]</ta>
            <ta e="T449" id="Seg_7548" s="T447">äh</ta>
            <ta e="T453" id="Seg_7549" s="T451">anders.[NOM]</ta>
            <ta e="T454" id="Seg_7550" s="T453">anders.[NOM]</ta>
            <ta e="T456" id="Seg_7551" s="T454">Leben-ACC</ta>
            <ta e="T460" id="Seg_7552" s="T458">anders.[NOM]</ta>
            <ta e="T461" id="Seg_7553" s="T460">Leben-ACC</ta>
            <ta e="T462" id="Seg_7554" s="T461">zugänglich.[NOM]</ta>
            <ta e="T463" id="Seg_7555" s="T462">Leben-ACC</ta>
            <ta e="T464" id="Seg_7556" s="T463">folgen-CVB.SEQ-3PL</ta>
            <ta e="T465" id="Seg_7557" s="T464">zugänglich.[NOM]</ta>
            <ta e="T466" id="Seg_7558" s="T465">dann</ta>
            <ta e="T467" id="Seg_7559" s="T466">zugänglich.[NOM]</ta>
            <ta e="T468" id="Seg_7560" s="T467">sein-FUT.[3SG]</ta>
            <ta e="T469" id="Seg_7561" s="T468">Novorybnoe-DAT/LOC</ta>
            <ta e="T470" id="Seg_7562" s="T469">leben</ta>
            <ta e="T471" id="Seg_7563" s="T470">äh</ta>
            <ta e="T472" id="Seg_7564" s="T471">dann</ta>
            <ta e="T475" id="Seg_7565" s="T474">Novorybnoe.[NOM]</ta>
            <ta e="T476" id="Seg_7566" s="T475">Seite-3SG-INSTR</ta>
            <ta e="T477" id="Seg_7567" s="T476">erzählen-PRS-1SG</ta>
            <ta e="T478" id="Seg_7568" s="T477">dann</ta>
            <ta e="T479" id="Seg_7569" s="T478">Novorybnoe-DAT/LOC</ta>
            <ta e="T480" id="Seg_7570" s="T479">besser</ta>
            <ta e="T481" id="Seg_7571" s="T480">EMPH</ta>
            <ta e="T482" id="Seg_7572" s="T481">Licht-PROPR-3PL</ta>
            <ta e="T483" id="Seg_7573" s="T482">EMPH</ta>
            <ta e="T484" id="Seg_7574" s="T483">warm.[NOM]</ta>
            <ta e="T485" id="Seg_7575" s="T484">EMPH</ta>
            <ta e="T486" id="Seg_7576" s="T485">jenes.[NOM]</ta>
            <ta e="T487" id="Seg_7577" s="T486">wegen</ta>
            <ta e="T488" id="Seg_7578" s="T487">Tundra</ta>
            <ta e="T489" id="Seg_7579" s="T488">Tundra-ADJZ.[NOM]</ta>
            <ta e="T490" id="Seg_7580" s="T489">Mensch-PL.[NOM]</ta>
            <ta e="T491" id="Seg_7581" s="T490">Tundra-DAT/LOC</ta>
            <ta e="T492" id="Seg_7582" s="T491">gehen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T493" id="Seg_7583" s="T492">sehr</ta>
            <ta e="T494" id="Seg_7584" s="T493">NEG</ta>
            <ta e="T495" id="Seg_7585" s="T494">ziehen-EP-CAUS-NEG-3PL</ta>
            <ta e="T496" id="Seg_7586" s="T495">Geld.[NOM]</ta>
            <ta e="T497" id="Seg_7587" s="T496">sehr</ta>
            <ta e="T498" id="Seg_7588" s="T497">äh</ta>
            <ta e="T499" id="Seg_7589" s="T498">Rentier-ABL</ta>
            <ta e="T500" id="Seg_7590" s="T499">was-ACC</ta>
            <ta e="T501" id="Seg_7591" s="T500">bekommen-FUT-2SG=Q</ta>
            <ta e="T502" id="Seg_7592" s="T501">doch</ta>
            <ta e="T503" id="Seg_7593" s="T502">jetzt</ta>
            <ta e="T504" id="Seg_7594" s="T503">töten.[IMP.2SG]</ta>
            <ta e="T505" id="Seg_7595" s="T504">und</ta>
            <ta e="T506" id="Seg_7596" s="T505">jenes</ta>
            <ta e="T507" id="Seg_7597" s="T506">nun</ta>
            <ta e="T508" id="Seg_7598" s="T507">wenig</ta>
            <ta e="T509" id="Seg_7599" s="T508">fünf-zehn-APRX</ta>
            <ta e="T510" id="Seg_7600" s="T509">Rentier-ACC</ta>
            <ta e="T511" id="Seg_7601" s="T510">EMPH</ta>
            <ta e="T512" id="Seg_7602" s="T511">töten-TEMP-2SG</ta>
            <ta e="T513" id="Seg_7603" s="T512">sehr</ta>
            <ta e="T514" id="Seg_7604" s="T513">NEG</ta>
            <ta e="T515" id="Seg_7605" s="T514">viel</ta>
            <ta e="T516" id="Seg_7606" s="T515">Geld-ACC</ta>
            <ta e="T517" id="Seg_7607" s="T516">verdienen-FUT-2SG</ta>
            <ta e="T518" id="Seg_7608" s="T517">NEG-3SG</ta>
            <ta e="T519" id="Seg_7609" s="T518">jenes</ta>
            <ta e="T520" id="Seg_7610" s="T519">Seite-3SG-DAT/LOC</ta>
            <ta e="T521" id="Seg_7611" s="T520">oder</ta>
            <ta e="T522" id="Seg_7612" s="T521">Tundra-DAT/LOC</ta>
            <ta e="T523" id="Seg_7613" s="T522">gehen-CVB.SEQ-2SG</ta>
            <ta e="T524" id="Seg_7614" s="T523">MOD</ta>
            <ta e="T525" id="Seg_7615" s="T524">jenes.[NOM]</ta>
            <ta e="T526" id="Seg_7616" s="T525">wegen</ta>
            <ta e="T527" id="Seg_7617" s="T526">glücklich</ta>
            <ta e="T528" id="Seg_7618" s="T527">leben-PTCP.FUT-DAT/LOC</ta>
            <ta e="T529" id="Seg_7619" s="T528">und</ta>
            <ta e="T530" id="Seg_7620" s="T529">Tundra-DAT/LOC</ta>
            <ta e="T531" id="Seg_7621" s="T530">Mensch-PL.[NOM]</ta>
            <ta e="T532" id="Seg_7622" s="T531">glücklich-ADVZ</ta>
            <ta e="T533" id="Seg_7623" s="T532">leben-PTCP.FUT-3PL-ACC</ta>
            <ta e="T534" id="Seg_7624" s="T533">dieses.[NOM]</ta>
            <ta e="T535" id="Seg_7625" s="T534">erster</ta>
            <ta e="T536" id="Seg_7626" s="T535">jenes</ta>
            <ta e="T537" id="Seg_7627" s="T536">Seite-3SG-DAT/LOC</ta>
            <ta e="T538" id="Seg_7628" s="T537">Regierung.[NOM]</ta>
            <ta e="T539" id="Seg_7629" s="T538">denken-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T540" id="Seg_7630" s="T539">man.muss</ta>
            <ta e="T541" id="Seg_7631" s="T540">Mensch-PL-DAT/LOC</ta>
            <ta e="T542" id="Seg_7632" s="T541">Hilfe.[NOM]</ta>
            <ta e="T543" id="Seg_7633" s="T542">und</ta>
            <ta e="T544" id="Seg_7634" s="T543">Mensch-PL.[NOM]</ta>
            <ta e="T545" id="Seg_7635" s="T544">selbst-3PL.[NOM]</ta>
            <ta e="T546" id="Seg_7636" s="T545">Mensch-PL.[NOM]</ta>
            <ta e="T547" id="Seg_7637" s="T546">was-ACC</ta>
            <ta e="T548" id="Seg_7638" s="T547">NEG</ta>
            <ta e="T549" id="Seg_7639" s="T548">machen-FUT-3PL</ta>
            <ta e="T550" id="Seg_7640" s="T549">NEG-3SG</ta>
            <ta e="T551" id="Seg_7641" s="T550">Tundra-ADJZ.[NOM]</ta>
            <ta e="T552" id="Seg_7642" s="T551">Mensch-PL.[NOM]</ta>
            <ta e="T553" id="Seg_7643" s="T552">umso.mehr</ta>
            <ta e="T554" id="Seg_7644" s="T553">selbst-3PL-GEN</ta>
            <ta e="T555" id="Seg_7645" s="T554">Vorderteil-3PL-GEN</ta>
            <ta e="T556" id="Seg_7646" s="T555">Seite-3SG-INSTR</ta>
            <ta e="T557" id="Seg_7647" s="T556">was-ACC</ta>
            <ta e="T558" id="Seg_7648" s="T557">NEG</ta>
            <ta e="T559" id="Seg_7649" s="T558">sprechen-EP-NEG-3PL</ta>
            <ta e="T560" id="Seg_7650" s="T559">Sowchose-PL-1PL.[NOM]</ta>
            <ta e="T561" id="Seg_7651" s="T560">Direktor-1PL.[NOM]</ta>
            <ta e="T562" id="Seg_7652" s="T561">Direktor-PL.[NOM]</ta>
            <ta e="T563" id="Seg_7653" s="T562">EMPH</ta>
            <ta e="T564" id="Seg_7654" s="T563">sich.ändern-PRS-3PL</ta>
            <ta e="T565" id="Seg_7655" s="T564">EMPH</ta>
            <ta e="T566" id="Seg_7656" s="T565">jetzt</ta>
            <ta e="T567" id="Seg_7657" s="T566">noch</ta>
            <ta e="T568" id="Seg_7658" s="T567">ein.Bisschen</ta>
            <ta e="T569" id="Seg_7659" s="T568">1PL-DAT/LOC</ta>
            <ta e="T570" id="Seg_7660" s="T569">Novorybnoe-DAT/LOC</ta>
            <ta e="T571" id="Seg_7661" s="T570">noch</ta>
            <ta e="T572" id="Seg_7662" s="T571">stehen-CVB.SIM</ta>
            <ta e="T573" id="Seg_7663" s="T572">fallen-PST1-3SG</ta>
            <ta e="T574" id="Seg_7664" s="T573">EVID</ta>
            <ta e="T575" id="Seg_7665" s="T574">eins</ta>
            <ta e="T576" id="Seg_7666" s="T575">Direktor.[NOM]</ta>
            <ta e="T577" id="Seg_7667" s="T576">Momonov</ta>
            <ta e="T578" id="Seg_7668" s="T577">Vladimir</ta>
            <ta e="T579" id="Seg_7669" s="T578">Aleksandrowitsch.[NOM]</ta>
            <ta e="T580" id="Seg_7670" s="T579">sagen-CVB.SEQ</ta>
            <ta e="T581" id="Seg_7671" s="T580">Mensch.[NOM]</ta>
            <ta e="T582" id="Seg_7672" s="T581">jenes.[NOM]</ta>
            <ta e="T583" id="Seg_7673" s="T582">jenes.[NOM]</ta>
            <ta e="T584" id="Seg_7674" s="T583">Seite-3SG-INSTR</ta>
            <ta e="T585" id="Seg_7675" s="T584">EMPH</ta>
            <ta e="T586" id="Seg_7676" s="T585">was.[NOM]</ta>
            <ta e="T587" id="Seg_7677" s="T586">NEG</ta>
            <ta e="T588" id="Seg_7678" s="T587">schlecht-ACC</ta>
            <ta e="T589" id="Seg_7679" s="T588">sagen-NEG-1SG</ta>
            <ta e="T590" id="Seg_7680" s="T589">hervorragend-PROPR.[NOM]</ta>
            <ta e="T591" id="Seg_7681" s="T590">Mensch.[NOM]</ta>
            <ta e="T592" id="Seg_7682" s="T591">äh</ta>
            <ta e="T593" id="Seg_7683" s="T592">Mensch-PL-ACC</ta>
            <ta e="T594" id="Seg_7684" s="T593">mit</ta>
            <ta e="T595" id="Seg_7685" s="T594">jetzt</ta>
            <ta e="T596" id="Seg_7686" s="T595">ein.Bisschen</ta>
            <ta e="T597" id="Seg_7687" s="T596">Geld-PROPR.[NOM]</ta>
            <ta e="T598" id="Seg_7688" s="T597">werden-PST2-3PL</ta>
            <ta e="T599" id="Seg_7689" s="T598">dieses.[NOM]</ta>
            <ta e="T600" id="Seg_7690" s="T599">wer.[NOM]</ta>
            <ta e="T601" id="Seg_7691" s="T600">sein-PST1-3SG</ta>
            <ta e="T604" id="Seg_7692" s="T603">geben-PTCP.HAB</ta>
            <ta e="T605" id="Seg_7693" s="T604">sein-PST1-3PL</ta>
            <ta e="T606" id="Seg_7694" s="T605">Produktion.[NOM]</ta>
            <ta e="T607" id="Seg_7695" s="T606">eigen-3SG-ACC</ta>
            <ta e="T608" id="Seg_7696" s="T607">nun</ta>
            <ta e="T609" id="Seg_7697" s="T608">in.diesem.Jahr</ta>
            <ta e="T610" id="Seg_7698" s="T609">machen-PST2-3PL</ta>
            <ta e="T611" id="Seg_7699" s="T610">jenes-3SG-3PL-ACC</ta>
            <ta e="T612" id="Seg_7700" s="T611">EMPH</ta>
            <ta e="T613" id="Seg_7701" s="T612">wegnehmen-VBZ-FUT-1PL</ta>
            <ta e="T614" id="Seg_7702" s="T613">jenes-ACC</ta>
            <ta e="T615" id="Seg_7703" s="T614">wegnehmen-VBZ-TEMP-3PL</ta>
            <ta e="T616" id="Seg_7704" s="T615">jetzt</ta>
            <ta e="T617" id="Seg_7705" s="T616">Tundra-ADJZ.[NOM]</ta>
            <ta e="T618" id="Seg_7706" s="T617">äh</ta>
            <ta e="T619" id="Seg_7707" s="T618">Jäger-PL.[NOM]</ta>
            <ta e="T620" id="Seg_7708" s="T619">EMPH</ta>
            <ta e="T621" id="Seg_7709" s="T620">auch</ta>
            <ta e="T624" id="Seg_7710" s="T623">schießen-FREQ-NMNZ-AG.[NOM]</ta>
            <ta e="T625" id="Seg_7711" s="T624">Mensch-PL.[NOM]</ta>
            <ta e="T626" id="Seg_7712" s="T625">EMPH</ta>
            <ta e="T627" id="Seg_7713" s="T626">wenig-ACC</ta>
            <ta e="T628" id="Seg_7714" s="T627">bekommen-FUT-3PL</ta>
            <ta e="T629" id="Seg_7715" s="T628">doch</ta>
            <ta e="T630" id="Seg_7716" s="T629">eins</ta>
            <ta e="T633" id="Seg_7717" s="T631">vermögen</ta>
            <ta e="T635" id="Seg_7718" s="T633">vermögen-FUT-3PL</ta>
            <ta e="T636" id="Seg_7719" s="T635">NEG-3PL</ta>
            <ta e="T637" id="Seg_7720" s="T636">AFFIRM</ta>
            <ta e="T659" id="Seg_7721" s="T657">jenes-INSTR</ta>
            <ta e="T660" id="Seg_7722" s="T659">hoffen-PRS-1PL</ta>
            <ta e="T661" id="Seg_7723" s="T660">AFFIRM</ta>
            <ta e="T662" id="Seg_7724" s="T661">jenes-INSTR</ta>
            <ta e="T663" id="Seg_7725" s="T662">hoffen-PRS-1PL</ta>
            <ta e="T664" id="Seg_7726" s="T663">und</ta>
            <ta e="T665" id="Seg_7727" s="T664">so</ta>
            <ta e="T666" id="Seg_7728" s="T665">dieses.[NOM]</ta>
            <ta e="T667" id="Seg_7729" s="T666">dunkel</ta>
            <ta e="T668" id="Seg_7730" s="T667">Seite.[NOM]</ta>
            <ta e="T669" id="Seg_7731" s="T668">Seite-3SG-ACC</ta>
            <ta e="T670" id="Seg_7732" s="T669">sich.unterhalten-PST1-1PL</ta>
            <ta e="T671" id="Seg_7733" s="T670">1PL.[NOM]</ta>
            <ta e="T672" id="Seg_7734" s="T671">und</ta>
            <ta e="T673" id="Seg_7735" s="T672">so</ta>
            <ta e="T674" id="Seg_7736" s="T673">EMPH</ta>
            <ta e="T675" id="Seg_7737" s="T674">Tundra-DAT/LOC</ta>
            <ta e="T676" id="Seg_7738" s="T675">leben-PTCP.COND-DAT/LOC</ta>
            <ta e="T677" id="Seg_7739" s="T676">sehr</ta>
            <ta e="T678" id="Seg_7740" s="T677">fröhlich.[NOM]</ta>
            <ta e="T679" id="Seg_7741" s="T678">sein-PST1-3SG</ta>
            <ta e="T680" id="Seg_7742" s="T679">äh</ta>
            <ta e="T681" id="Seg_7743" s="T680">Tundra-ADJZ.[NOM]</ta>
            <ta e="T682" id="Seg_7744" s="T681">Mensch-PL.[NOM]</ta>
            <ta e="T683" id="Seg_7745" s="T682">jetzt</ta>
            <ta e="T684" id="Seg_7746" s="T683">Kind-PL.[NOM]</ta>
            <ta e="T685" id="Seg_7747" s="T684">gehen-EP-NEG.PTCP</ta>
            <ta e="T686" id="Seg_7748" s="T685">sein-CVB.SEQ-3PL</ta>
            <ta e="T687" id="Seg_7749" s="T686">jetzt</ta>
            <ta e="T688" id="Seg_7750" s="T687">wissen-NEG-3PL</ta>
            <ta e="T689" id="Seg_7751" s="T688">EMPH</ta>
            <ta e="T690" id="Seg_7752" s="T689">dolganisch.[NOM]</ta>
            <ta e="T691" id="Seg_7753" s="T690">Sprache-3SG-ACC</ta>
            <ta e="T692" id="Seg_7754" s="T691">sauber.[NOM]</ta>
            <ta e="T693" id="Seg_7755" s="T692">Atem-3PL.[NOM]</ta>
            <ta e="T694" id="Seg_7756" s="T693">äh</ta>
            <ta e="T695" id="Seg_7757" s="T694">Luft-ACC</ta>
            <ta e="T696" id="Seg_7758" s="T695">Gras.[NOM]</ta>
            <ta e="T697" id="Seg_7759" s="T696">Geruch-3SG-ACC</ta>
            <ta e="T698" id="Seg_7760" s="T697">sagen-PTCP.PST-DAT/LOC</ta>
            <ta e="T699" id="Seg_7761" s="T698">sagen-CVB.PURP</ta>
            <ta e="T700" id="Seg_7762" s="T699">erster</ta>
            <ta e="T701" id="Seg_7763" s="T700">Gans.[NOM]</ta>
            <ta e="T702" id="Seg_7764" s="T701">Lärm-3SG-ACC</ta>
            <ta e="T703" id="Seg_7765" s="T702">Kalb-PL.[NOM]</ta>
            <ta e="T704" id="Seg_7766" s="T703">geboren.werden-PTCP.PRS-3PL-ACC</ta>
            <ta e="T705" id="Seg_7767" s="T704">ganz-3SG-ACC</ta>
            <ta e="T706" id="Seg_7768" s="T705">was-ACC</ta>
            <ta e="T707" id="Seg_7769" s="T706">NEG</ta>
            <ta e="T708" id="Seg_7770" s="T707">sehr</ta>
            <ta e="T709" id="Seg_7771" s="T708">wissen-NEG-3PL</ta>
            <ta e="T710" id="Seg_7772" s="T709">EMPH</ta>
            <ta e="T711" id="Seg_7773" s="T710">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T712" id="Seg_7774" s="T711">jeder-3SG.[NOM]</ta>
            <ta e="T713" id="Seg_7775" s="T712">1PL-DAT/LOC</ta>
            <ta e="T714" id="Seg_7776" s="T713">äh</ta>
            <ta e="T715" id="Seg_7777" s="T714">Tundra-DAT/LOC</ta>
            <ta e="T716" id="Seg_7778" s="T715">geboren.werden-PTCP.PST</ta>
            <ta e="T717" id="Seg_7779" s="T716">Mensch-PL-DAT/LOC</ta>
            <ta e="T718" id="Seg_7780" s="T717">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T719" id="Seg_7781" s="T718">jeder-3SG.[NOM]</ta>
            <ta e="T720" id="Seg_7782" s="T719">jeder-3SG.[NOM]</ta>
            <ta e="T721" id="Seg_7783" s="T720">groß.[NOM]</ta>
            <ta e="T722" id="Seg_7784" s="T721">wer.[NOM]</ta>
            <ta e="T723" id="Seg_7785" s="T722">EMPH</ta>
            <ta e="T725" id="Seg_7786" s="T723">Eindruck.[NOM]</ta>
            <ta e="T764" id="Seg_7787" s="T763">danke</ta>
         </annotation>
         <annotation name="gr" tierref="gr-UkOA">
            <ta e="T8" id="Seg_7788" s="T7">ах</ta>
            <ta e="T9" id="Seg_7789" s="T8">сам-1SG.[NOM]</ta>
            <ta e="T10" id="Seg_7790" s="T9">Хатанга-DAT/LOC</ta>
            <ta e="T11" id="Seg_7791" s="T10">дом-PROPR.[NOM]</ta>
            <ta e="T12" id="Seg_7792" s="T11">быть-CVB.SEQ-1SG</ta>
            <ta e="T13" id="Seg_7793" s="T12">ээ</ta>
            <ta e="T14" id="Seg_7794" s="T13">тундра-DAT/LOC</ta>
            <ta e="T15" id="Seg_7795" s="T14">мало-ADVZ</ta>
            <ta e="T16" id="Seg_7796" s="T15">быть-PRS-1SG</ta>
            <ta e="T17" id="Seg_7797" s="T16">EMPH</ta>
            <ta e="T18" id="Seg_7798" s="T17">ведь</ta>
            <ta e="T19" id="Seg_7799" s="T18">один</ta>
            <ta e="T20" id="Seg_7800" s="T19">весна-PL-ACC</ta>
            <ta e="T21" id="Seg_7801" s="T20">осень-PL-ACC</ta>
            <ta e="T22" id="Seg_7802" s="T21">может</ta>
            <ta e="T23" id="Seg_7803" s="T22">всегда</ta>
            <ta e="T24" id="Seg_7804" s="T23">тундра-DAT/LOC</ta>
            <ta e="T25" id="Seg_7805" s="T24">идти-PRS-1SG</ta>
            <ta e="T26" id="Seg_7806" s="T25">сколько</ta>
            <ta e="T27" id="Seg_7807" s="T26">год.[NOM]</ta>
            <ta e="T28" id="Seg_7808" s="T27">каждый</ta>
            <ta e="T29" id="Seg_7809" s="T28">мать-1SG-PROPR-1SG-DAT/LOC</ta>
            <ta e="T30" id="Seg_7810" s="T29">отец-1SG-PROPR-1SG-DAT/LOC</ta>
            <ta e="T31" id="Seg_7811" s="T30">помогать-CVB.SIM</ta>
            <ta e="T32" id="Seg_7812" s="T31">тот.[NOM]</ta>
            <ta e="T33" id="Seg_7813" s="T32">к</ta>
            <ta e="T34" id="Seg_7814" s="T33">тундра-DAT/LOC</ta>
            <ta e="T35" id="Seg_7815" s="T34">идти-CVB.SIM</ta>
            <ta e="T36" id="Seg_7816" s="T35">быть-CVB.SEQ-3PL</ta>
            <ta e="T38" id="Seg_7817" s="T37">олень-PROPR-3PL</ta>
            <ta e="T39" id="Seg_7818" s="T38">EMPH</ta>
            <ta e="T40" id="Seg_7819" s="T39">тот</ta>
            <ta e="T41" id="Seg_7820" s="T40">олень-PL.[NOM]</ta>
            <ta e="T42" id="Seg_7821" s="T41">олень-3PL-ACC</ta>
            <ta e="T43" id="Seg_7822" s="T42">EMPH</ta>
            <ta e="T44" id="Seg_7823" s="T43">видеть-CVB.SIM</ta>
            <ta e="T45" id="Seg_7824" s="T44">скоро</ta>
            <ta e="T46" id="Seg_7825" s="T45">EMPH</ta>
            <ta e="T47" id="Seg_7826" s="T46">такой-ACC</ta>
            <ta e="T48" id="Seg_7827" s="T47">сам-1SG.[NOM]</ta>
            <ta e="T49" id="Seg_7828" s="T48">EMPH</ta>
            <ta e="T51" id="Seg_7829" s="T50">кто-VBZ-PRS-1SG</ta>
            <ta e="T52" id="Seg_7830" s="T51">зарядить-EP-PASS/REFL-PRS-1SG</ta>
            <ta e="T53" id="Seg_7831" s="T52">ээ</ta>
            <ta e="T54" id="Seg_7832" s="T53">кто-EP-INSTR</ta>
            <ta e="T55" id="Seg_7833" s="T54">энергия-INSTR</ta>
            <ta e="T56" id="Seg_7834" s="T55">говорить-PTCP.PST-DAT/LOC</ta>
            <ta e="T57" id="Seg_7835" s="T56">говорить-CVB.PURP</ta>
            <ta e="T60" id="Seg_7836" s="T59">Хатанга-DAT/LOC</ta>
            <ta e="T61" id="Seg_7837" s="T60">лежать-CVB.SEQ</ta>
            <ta e="T62" id="Seg_7838" s="T61">дыхание-3SG-ACC</ta>
            <ta e="T63" id="Seg_7839" s="T62">заложить-PRS-3PL</ta>
            <ta e="T64" id="Seg_7840" s="T63">тот.EMPH.[NOM]</ta>
            <ta e="T65" id="Seg_7841" s="T64">после.того</ta>
            <ta e="T85" id="Seg_7842" s="T84">тундра-ADJZ-PL-EP-2SG.[NOM]</ta>
            <ta e="T86" id="Seg_7843" s="T85">сам-3PL.[NOM]</ta>
            <ta e="T87" id="Seg_7844" s="T86">знать-PRS-3PL</ta>
            <ta e="T88" id="Seg_7845" s="T87">EMPH</ta>
            <ta e="T89" id="Seg_7846" s="T88">как</ta>
            <ta e="T90" id="Seg_7847" s="T89">жить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T91" id="Seg_7848" s="T90">теперь</ta>
            <ta e="T92" id="Seg_7849" s="T91">хороший.[NOM]</ta>
            <ta e="T93" id="Seg_7850" s="T92">жизнь-ACC</ta>
            <ta e="T94" id="Seg_7851" s="T93">знать-NEG.PTCP</ta>
            <ta e="T95" id="Seg_7852" s="T94">быть-CVB.SEQ-3PL</ta>
            <ta e="T96" id="Seg_7853" s="T95">3PL.[NOM]</ta>
            <ta e="T97" id="Seg_7854" s="T96">ээ</ta>
            <ta e="T98" id="Seg_7855" s="T97">хороший-ADVZ</ta>
            <ta e="T99" id="Seg_7856" s="T98">жить-FUT-1PL</ta>
            <ta e="T100" id="Seg_7857" s="T99">говорить-CVB.SEQ</ta>
            <ta e="T101" id="Seg_7858" s="T100">идти-NEG-3PL</ta>
            <ta e="T102" id="Seg_7859" s="T101">наверное</ta>
            <ta e="T103" id="Seg_7860" s="T102">очень</ta>
            <ta e="T104" id="Seg_7861" s="T103">EMPH</ta>
            <ta e="T105" id="Seg_7862" s="T104">сам-3PL.[NOM]</ta>
            <ta e="T106" id="Seg_7863" s="T105">EMPH</ta>
            <ta e="T107" id="Seg_7864" s="T106">что.[NOM]</ta>
            <ta e="T108" id="Seg_7865" s="T107">есть</ta>
            <ta e="T109" id="Seg_7866" s="T108">тот-INSTR</ta>
            <ta e="T110" id="Seg_7867" s="T109">радоваться-CVB.SIM</ta>
            <ta e="T111" id="Seg_7868" s="T110">идти-PRS-3PL</ta>
            <ta e="T112" id="Seg_7869" s="T111">NEG.EX</ta>
            <ta e="T113" id="Seg_7870" s="T112">быть-TEMP-3SG</ta>
            <ta e="T114" id="Seg_7871" s="T113">как</ta>
            <ta e="T115" id="Seg_7872" s="T114">делать-FUT.[3SG]=Q</ta>
            <ta e="T116" id="Seg_7873" s="T115">NEG.EX</ta>
            <ta e="T117" id="Seg_7874" s="T116">EMPH</ta>
            <ta e="T119" id="Seg_7875" s="T117">NEG.EX</ta>
            <ta e="T124" id="Seg_7876" s="T123">очень</ta>
            <ta e="T125" id="Seg_7877" s="T124">EMPH</ta>
            <ta e="T126" id="Seg_7878" s="T125">грустить-NEG-3PL</ta>
            <ta e="T127" id="Seg_7879" s="T126">AFFIRM</ta>
            <ta e="T128" id="Seg_7880" s="T127">а</ta>
            <ta e="T129" id="Seg_7881" s="T128">если</ta>
            <ta e="T130" id="Seg_7882" s="T129">этот</ta>
            <ta e="T132" id="Seg_7883" s="T131">хороший-ADVZ</ta>
            <ta e="T133" id="Seg_7884" s="T132">ээ</ta>
            <ta e="T134" id="Seg_7885" s="T133">человек-SIM</ta>
            <ta e="T135" id="Seg_7886" s="T134">этот</ta>
            <ta e="T136" id="Seg_7887" s="T135">настоящий-SIM</ta>
            <ta e="T137" id="Seg_7888" s="T136">жить-PTCP.PRS</ta>
            <ta e="T138" id="Seg_7889" s="T137">человек.[NOM]</ta>
            <ta e="T139" id="Seg_7890" s="T138">этот.[NOM]</ta>
            <ta e="T140" id="Seg_7891" s="T139">вправду</ta>
            <ta e="T141" id="Seg_7892" s="T140">EMPH</ta>
            <ta e="T142" id="Seg_7893" s="T141">доступный</ta>
            <ta e="T143" id="Seg_7894" s="T142">EMPH</ta>
            <ta e="T144" id="Seg_7895" s="T143">балок-PL-ACC</ta>
            <ta e="T145" id="Seg_7896" s="T144">администрация-ABL</ta>
            <ta e="T146" id="Seg_7897" s="T145">давать-PTCP.FUT</ta>
            <ta e="T147" id="Seg_7898" s="T146">быть-PST1-3PL</ta>
            <ta e="T148" id="Seg_7899" s="T147">MOD</ta>
            <ta e="T149" id="Seg_7900" s="T148">этот</ta>
            <ta e="T150" id="Seg_7901" s="T149">необходимый.[NOM]</ta>
            <ta e="T151" id="Seg_7902" s="T150">самый</ta>
            <ta e="T152" id="Seg_7903" s="T151">самый</ta>
            <ta e="T153" id="Seg_7904" s="T152">потребность-PROPR.[NOM]</ta>
            <ta e="T154" id="Seg_7905" s="T153">вещь-3PL.[NOM]</ta>
            <ta e="T155" id="Seg_7906" s="T154">что-3PL.[NOM]</ta>
            <ta e="T157" id="Seg_7907" s="T156">этот</ta>
            <ta e="T158" id="Seg_7908" s="T157">вот</ta>
            <ta e="T159" id="Seg_7909" s="T158">помощь.[NOM]</ta>
            <ta e="T160" id="Seg_7910" s="T159">дотация.[NOM]</ta>
            <ta e="T161" id="Seg_7911" s="T160">подобно</ta>
            <ta e="T162" id="Seg_7912" s="T161">большой-ADVZ</ta>
            <ta e="T163" id="Seg_7913" s="T162">приходить-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T164" id="Seg_7914" s="T163">быть-CVB.SIM</ta>
            <ta e="T165" id="Seg_7915" s="T164">тундра-ADJZ-PL-DAT/LOC</ta>
            <ta e="T166" id="Seg_7916" s="T165">сам-3PL-GEN</ta>
            <ta e="T167" id="Seg_7917" s="T166">сам-3PL-GEN</ta>
            <ta e="T168" id="Seg_7918" s="T167">способность-3PL-INSTR</ta>
            <ta e="T169" id="Seg_7919" s="T168">идти-EP-MED-EMOT-PRS-3PL</ta>
            <ta e="T170" id="Seg_7920" s="T169">тундра-ADJZ-PL-1PL.[NOM]</ta>
            <ta e="T171" id="Seg_7921" s="T170">тот.[NOM]</ta>
            <ta e="T172" id="Seg_7922" s="T171">из_за</ta>
            <ta e="T173" id="Seg_7923" s="T172">теперь</ta>
            <ta e="T174" id="Seg_7924" s="T173">еще</ta>
            <ta e="T175" id="Seg_7925" s="T174">тот.[NOM]</ta>
            <ta e="T176" id="Seg_7926" s="T175">тысяча</ta>
            <ta e="T177" id="Seg_7927" s="T176">пять</ta>
            <ta e="T178" id="Seg_7928" s="T177">сто</ta>
            <ta e="T179" id="Seg_7929" s="T178">тот.[NOM]</ta>
            <ta e="T180" id="Seg_7930" s="T179">кто.[NOM]</ta>
            <ta e="T181" id="Seg_7931" s="T180">крепко</ta>
            <ta e="T182" id="Seg_7932" s="T181">полторашка-ACC</ta>
            <ta e="T183" id="Seg_7933" s="T182">давать-CVB.SEQ</ta>
            <ta e="T184" id="Seg_7934" s="T183">быть-CVB.SEQ-3PL</ta>
            <ta e="T185" id="Seg_7935" s="T184">еще</ta>
            <ta e="T186" id="Seg_7936" s="T185">немного</ta>
            <ta e="T187" id="Seg_7937" s="T186">ах</ta>
            <ta e="T188" id="Seg_7938" s="T187">а</ta>
            <ta e="T189" id="Seg_7939" s="T188">так</ta>
            <ta e="T190" id="Seg_7940" s="T189">совсем</ta>
            <ta e="T191" id="Seg_7941" s="T190">EMPH</ta>
            <ta e="T192" id="Seg_7942" s="T191">что.[NOM]</ta>
            <ta e="T193" id="Seg_7943" s="T192">очень</ta>
            <ta e="T194" id="Seg_7944" s="T193">EMPH</ta>
            <ta e="T196" id="Seg_7945" s="T195">пища-EP-INSTR</ta>
            <ta e="T197" id="Seg_7946" s="T196">EMPH</ta>
            <ta e="T198" id="Seg_7947" s="T197">хлеб-EP-INSTR</ta>
            <ta e="T199" id="Seg_7948" s="T198">NEG</ta>
            <ta e="T200" id="Seg_7949" s="T199">что-EP-INSTR</ta>
            <ta e="T201" id="Seg_7950" s="T200">NEG</ta>
            <ta e="T202" id="Seg_7951" s="T201">тот</ta>
            <ta e="T203" id="Seg_7952" s="T202">пекарня-1PL.[NOM]</ta>
            <ta e="T204" id="Seg_7953" s="T203">работать-PTCP.NEG</ta>
            <ta e="T205" id="Seg_7954" s="T204">быть-CVB.SEQ</ta>
            <ta e="T206" id="Seg_7955" s="T205">сам-3PL.[NOM]</ta>
            <ta e="T207" id="Seg_7956" s="T206">вариться-EP-CAUS-EP-MED-EMOT-PRS-3PL</ta>
            <ta e="T208" id="Seg_7957" s="T207">тундра-DAT/LOC</ta>
            <ta e="T209" id="Seg_7958" s="T208">идти-CVB.SEQ-3PL</ta>
            <ta e="T210" id="Seg_7959" s="T209">какой</ta>
            <ta e="T211" id="Seg_7960" s="T210">какой</ta>
            <ta e="T212" id="Seg_7961" s="T211">молодой.[NOM]</ta>
            <ta e="T213" id="Seg_7962" s="T212">человек.[NOM]</ta>
            <ta e="T214" id="Seg_7963" s="T213">идти-FUT-1SG</ta>
            <ta e="T215" id="Seg_7964" s="T214">говорить-FUT.[3SG]=Q</ta>
            <ta e="T216" id="Seg_7965" s="T215">теплый.[NOM]</ta>
            <ta e="T217" id="Seg_7966" s="T216">дом-ABL</ta>
            <ta e="T218" id="Seg_7967" s="T217">теплый</ta>
            <ta e="T219" id="Seg_7968" s="T218">огонь-ABL</ta>
            <ta e="T220" id="Seg_7969" s="T219">выйти-CVB.SEQ</ta>
            <ta e="T221" id="Seg_7970" s="T220">холод-DAT/LOC</ta>
            <ta e="T222" id="Seg_7971" s="T221">ветер-DAT/LOC</ta>
            <ta e="T223" id="Seg_7972" s="T222">выйти-CVB.SEQ</ta>
            <ta e="T224" id="Seg_7973" s="T223">олень-ACC</ta>
            <ta e="T225" id="Seg_7974" s="T224">охранять-CVB.SIM</ta>
            <ta e="T226" id="Seg_7975" s="T225">идти-FUT-1SG</ta>
            <ta e="T227" id="Seg_7976" s="T226">говорить-CVB.SEQ</ta>
            <ta e="T228" id="Seg_7977" s="T227">Q</ta>
            <ta e="T229" id="Seg_7978" s="T228">платить-NEG-3PL</ta>
            <ta e="T230" id="Seg_7979" s="T229">EMPH</ta>
            <ta e="T231" id="Seg_7980" s="T230">тот</ta>
            <ta e="T232" id="Seg_7981" s="T231">тогда</ta>
            <ta e="T233" id="Seg_7982" s="T232">MOD</ta>
            <ta e="T234" id="Seg_7983" s="T233">деньги-ACC</ta>
            <ta e="T235" id="Seg_7984" s="T234">видеть-NEG-3PL</ta>
            <ta e="T236" id="Seg_7985" s="T235">тундра-ADJZ-PL.[NOM]</ta>
            <ta e="T237" id="Seg_7986" s="T236">недавно</ta>
            <ta e="T238" id="Seg_7987" s="T237">брезент.[NOM]</ta>
            <ta e="T239" id="Seg_7988" s="T238">что.[NOM]</ta>
            <ta e="T240" id="Seg_7989" s="T239">принести-PRS-3PL</ta>
            <ta e="T242" id="Seg_7990" s="T241">коммерсант-PL.[NOM]</ta>
            <ta e="T243" id="Seg_7991" s="T242">продавать-PRS-3PL</ta>
            <ta e="T244" id="Seg_7992" s="T243">очень</ta>
            <ta e="T245" id="Seg_7993" s="T244">дорогой-DAT/LOC</ta>
            <ta e="T246" id="Seg_7994" s="T245">тот-3SG-2SG.[NOM]</ta>
            <ta e="T247" id="Seg_7995" s="T246">что.[NOM]</ta>
            <ta e="T248" id="Seg_7996" s="T247">человек-3SG.[NOM]=Q</ta>
            <ta e="T249" id="Seg_7997" s="T248">покупать-CVB.SEQ</ta>
            <ta e="T250" id="Seg_7998" s="T249">взять-FUT.[3SG]=Q</ta>
            <ta e="T251" id="Seg_7999" s="T250">Q</ta>
            <ta e="T252" id="Seg_8000" s="T251">ээ</ta>
            <ta e="T253" id="Seg_8001" s="T252">пища-ACC</ta>
            <ta e="T254" id="Seg_8002" s="T253">пища-ACC</ta>
            <ta e="T255" id="Seg_8003" s="T254">покупать-FUT.[3SG]=Q</ta>
            <ta e="T256" id="Seg_8004" s="T255">Q</ta>
            <ta e="T257" id="Seg_8005" s="T256">пища-ACC</ta>
            <ta e="T258" id="Seg_8006" s="T257">покупать-FUT-2SG</ta>
            <ta e="T259" id="Seg_8007" s="T258">Q</ta>
            <ta e="T260" id="Seg_8008" s="T259">брезент.[NOM]</ta>
            <ta e="T261" id="Seg_8009" s="T260">покупать-FUT-2SG</ta>
            <ta e="T262" id="Seg_8010" s="T261">Q</ta>
            <ta e="T263" id="Seg_8011" s="T262">одежда.[NOM]</ta>
            <ta e="T264" id="Seg_8012" s="T263">покупать-FUT-2SG</ta>
            <ta e="T265" id="Seg_8013" s="T264">Q</ta>
            <ta e="T266" id="Seg_8014" s="T265">ребенок-PL-EP-2SG.[NOM]</ta>
            <ta e="T267" id="Seg_8015" s="T266">еще</ta>
            <ta e="T268" id="Seg_8016" s="T267">учиться-PTCP.FUT-3PL-ACC</ta>
            <ta e="T269" id="Seg_8017" s="T268">надо</ta>
            <ta e="T270" id="Seg_8018" s="T269">что.[NOM]</ta>
            <ta e="T271" id="Seg_8019" s="T270">EMPH</ta>
            <ta e="T272" id="Seg_8020" s="T271">теперь</ta>
            <ta e="T273" id="Seg_8021" s="T272">быть-CVB.SIM</ta>
            <ta e="T274" id="Seg_8022" s="T273">раньше</ta>
            <ta e="T275" id="Seg_8023" s="T274">студент-PL-ACC</ta>
            <ta e="T276" id="Seg_8024" s="T275">что-PL-ACC</ta>
            <ta e="T277" id="Seg_8025" s="T276">хоть</ta>
            <ta e="T278" id="Seg_8026" s="T277">давать-PTCP.HAB</ta>
            <ta e="T279" id="Seg_8027" s="T278">быть-PST1-3PL</ta>
            <ta e="T280" id="Seg_8028" s="T279">3PL-DAT/LOC</ta>
            <ta e="T281" id="Seg_8029" s="T280">что.[NOM]</ta>
            <ta e="T282" id="Seg_8030" s="T281">INDEF</ta>
            <ta e="T283" id="Seg_8031" s="T282">дотация.[NOM]</ta>
            <ta e="T284" id="Seg_8032" s="T283">может</ta>
            <ta e="T285" id="Seg_8033" s="T284">совхоз-ABL</ta>
            <ta e="T286" id="Seg_8034" s="T285">направлять-VBZ-PTCP.HAB</ta>
            <ta e="T287" id="Seg_8035" s="T286">быть-PST1-3PL</ta>
            <ta e="T288" id="Seg_8036" s="T287">теперь</ta>
            <ta e="T289" id="Seg_8037" s="T288">тот</ta>
            <ta e="T290" id="Seg_8038" s="T289">такой</ta>
            <ta e="T291" id="Seg_8039" s="T290">что.[NOM]</ta>
            <ta e="T292" id="Seg_8040" s="T291">NEG</ta>
            <ta e="T293" id="Seg_8041" s="T292">NEG.EX</ta>
            <ta e="T294" id="Seg_8042" s="T293">целый-3SG.[NOM]</ta>
            <ta e="T295" id="Seg_8043" s="T294">человек.[NOM]</ta>
            <ta e="T296" id="Seg_8044" s="T295">сам-3SG-GEN</ta>
            <ta e="T297" id="Seg_8045" s="T296">деньги-3SG-INSTR</ta>
            <ta e="T298" id="Seg_8046" s="T297">идти-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_8047" s="T298">кто.[NOM]</ta>
            <ta e="T300" id="Seg_8048" s="T299">способность-POSS</ta>
            <ta e="T301" id="Seg_8049" s="T300">NEG</ta>
            <ta e="T302" id="Seg_8050" s="T301">человек.[NOM]</ta>
            <ta e="T303" id="Seg_8051" s="T302">совсем</ta>
            <ta e="T304" id="Seg_8052" s="T303">умирать-PRS.[3SG]</ta>
            <ta e="T305" id="Seg_8053" s="T304">говорить-PTCP.PST-DAT/LOC</ta>
            <ta e="T306" id="Seg_8054" s="T305">говорить-CVB.PURP</ta>
            <ta e="T307" id="Seg_8055" s="T306">тот.EMPH</ta>
            <ta e="T308" id="Seg_8056" s="T307">думать-CVB.SEQ</ta>
            <ta e="T309" id="Seg_8057" s="T308">а</ta>
            <ta e="T310" id="Seg_8058" s="T309">кто.[NOM]</ta>
            <ta e="T311" id="Seg_8059" s="T310">способность-PROPR-EP-2SG.[NOM]</ta>
            <ta e="T312" id="Seg_8060" s="T311">немного</ta>
            <ta e="T313" id="Seg_8061" s="T312">учить-PRS-3PL</ta>
            <ta e="T314" id="Seg_8062" s="T313">ребенок-PL-3SG-ACC</ta>
            <ta e="T315" id="Seg_8063" s="T314">ведь</ta>
            <ta e="T316" id="Seg_8064" s="T315">один</ta>
            <ta e="T321" id="Seg_8065" s="T319">жить-PRS-3PL</ta>
            <ta e="T336" id="Seg_8066" s="T335">на</ta>
            <ta e="T337" id="Seg_8067" s="T336">господин-PL.[NOM]</ta>
            <ta e="T338" id="Seg_8068" s="T337">только</ta>
            <ta e="T339" id="Seg_8069" s="T338">господин-PL.[NOM]</ta>
            <ta e="T340" id="Seg_8070" s="T339">только</ta>
            <ta e="T341" id="Seg_8071" s="T340">помогать-FUT-3PL</ta>
            <ta e="T342" id="Seg_8072" s="T341">1PL.[NOM]</ta>
            <ta e="T343" id="Seg_8073" s="T342">земля-1PL-DAT/LOC</ta>
            <ta e="T344" id="Seg_8074" s="T343">жизнь-1PL.[NOM]</ta>
            <ta e="T345" id="Seg_8075" s="T344">улучшать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T346" id="Seg_8076" s="T345">полностью</ta>
            <ta e="T347" id="Seg_8077" s="T346">3PL-ABL</ta>
            <ta e="T348" id="Seg_8078" s="T347">только</ta>
            <ta e="T350" id="Seg_8079" s="T348">выйти-FUT-3SG</ta>
            <ta e="T362" id="Seg_8080" s="T360">сам</ta>
            <ta e="T365" id="Seg_8081" s="T362">сам-1PL.[NOM]</ta>
            <ta e="T368" id="Seg_8082" s="T365">теперь</ta>
            <ta e="T369" id="Seg_8083" s="T368">человек-1PL.[NOM]</ta>
            <ta e="T370" id="Seg_8084" s="T369">теперь</ta>
            <ta e="T371" id="Seg_8085" s="T370">человек-PL-1PL.[NOM]</ta>
            <ta e="T372" id="Seg_8086" s="T371">MOD</ta>
            <ta e="T373" id="Seg_8087" s="T372">в.основном</ta>
            <ta e="T374" id="Seg_8088" s="T373">много</ta>
            <ta e="T375" id="Seg_8089" s="T374">человек.[NOM]</ta>
            <ta e="T376" id="Seg_8090" s="T375">ээ</ta>
            <ta e="T377" id="Seg_8091" s="T376">входить-PST2-3SG</ta>
            <ta e="T378" id="Seg_8092" s="T377">тундра-ABL</ta>
            <ta e="T379" id="Seg_8093" s="T378">способность-POSS</ta>
            <ta e="T380" id="Seg_8094" s="T379">NEG</ta>
            <ta e="T381" id="Seg_8095" s="T380">быть-CVB.SEQ-3PL</ta>
            <ta e="T382" id="Seg_8096" s="T381">каждый-3PL.[NOM]</ta>
            <ta e="T383" id="Seg_8097" s="T382">большой.[NOM]</ta>
            <ta e="T384" id="Seg_8098" s="T383">опыт-PROPR-3PL.[NOM]</ta>
            <ta e="T385" id="Seg_8099" s="T384">олень-DAT/LOC</ta>
            <ta e="T386" id="Seg_8100" s="T385">идти-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T387" id="Seg_8101" s="T386">EMPH</ta>
            <ta e="T388" id="Seg_8102" s="T387">ээ</ta>
            <ta e="T389" id="Seg_8103" s="T388">тот</ta>
            <ta e="T390" id="Seg_8104" s="T389">каждый-3SG.[NOM]</ta>
            <ta e="T391" id="Seg_8105" s="T390">тот</ta>
            <ta e="T392" id="Seg_8106" s="T391">каждый-3SG.[NOM]</ta>
            <ta e="T393" id="Seg_8107" s="T392">тот</ta>
            <ta e="T394" id="Seg_8108" s="T393">каждый-3SG.[NOM]</ta>
            <ta e="T395" id="Seg_8109" s="T394">умирать-PRS.[3SG]</ta>
            <ta e="T396" id="Seg_8110" s="T395">человек.[NOM]</ta>
            <ta e="T397" id="Seg_8111" s="T396">каждый-3SG.[NOM]</ta>
            <ta e="T398" id="Seg_8112" s="T397">ээ</ta>
            <ta e="T399" id="Seg_8113" s="T398">ну</ta>
            <ta e="T400" id="Seg_8114" s="T399">место-DAT/LOC</ta>
            <ta e="T401" id="Seg_8115" s="T400">оставаться-PRS.[3SG]</ta>
            <ta e="T403" id="Seg_8116" s="T402">место-DAT/LOC</ta>
            <ta e="T404" id="Seg_8117" s="T403">оставаться-PRS.[3SG]</ta>
            <ta e="T405" id="Seg_8118" s="T404">человек.[NOM]</ta>
            <ta e="T406" id="Seg_8119" s="T405">каждый-3SG.[NOM]</ta>
            <ta e="T407" id="Seg_8120" s="T406">теперь</ta>
            <ta e="T408" id="Seg_8121" s="T407">идти-PTCP.PRS</ta>
            <ta e="T409" id="Seg_8122" s="T408">EMPH</ta>
            <ta e="T410" id="Seg_8123" s="T409">человек-2SG.[NOM]</ta>
            <ta e="T411" id="Seg_8124" s="T410">очень</ta>
            <ta e="T412" id="Seg_8125" s="T411">земля-ACC</ta>
            <ta e="T413" id="Seg_8126" s="T412">знать-NEG-3PL</ta>
            <ta e="T414" id="Seg_8127" s="T413">молодой.[NOM]</ta>
            <ta e="T415" id="Seg_8128" s="T414">человек-PL-EP-2SG.[NOM]</ta>
            <ta e="T416" id="Seg_8129" s="T415">теперь</ta>
            <ta e="T417" id="Seg_8130" s="T416">идти-CVB.SEQ-3PL</ta>
            <ta e="T418" id="Seg_8131" s="T417">EMPH</ta>
            <ta e="T419" id="Seg_8132" s="T418">выйти-CVB.SEQ-3PL</ta>
            <ta e="T420" id="Seg_8133" s="T419">EMPH</ta>
            <ta e="T421" id="Seg_8134" s="T420">например</ta>
            <ta e="T422" id="Seg_8135" s="T421">ягель-PROPR.[NOM]</ta>
            <ta e="T423" id="Seg_8136" s="T422">место-PL.[NOM]</ta>
            <ta e="T424" id="Seg_8137" s="T423">теперь</ta>
            <ta e="T425" id="Seg_8138" s="T424">дикий.олень-1PL.[NOM]</ta>
            <ta e="T426" id="Seg_8139" s="T425">теперь</ta>
            <ta e="T427" id="Seg_8140" s="T426">место-3SG-ACC</ta>
            <ta e="T428" id="Seg_8141" s="T427">сменить-CVB.SEQ</ta>
            <ta e="T429" id="Seg_8142" s="T428">1PL-INSTR</ta>
            <ta e="T430" id="Seg_8143" s="T429">много-ACC</ta>
            <ta e="T431" id="Seg_8144" s="T430">падать-PTCP.PRS</ta>
            <ta e="T432" id="Seg_8145" s="T431">быть-CVB.SEQ</ta>
            <ta e="T433" id="Seg_8146" s="T432">ягель-1PL-ACC</ta>
            <ta e="T434" id="Seg_8147" s="T433">истратить-CAUS-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_8148" s="T434">тот.[NOM]</ta>
            <ta e="T436" id="Seg_8149" s="T435">из_за</ta>
            <ta e="T437" id="Seg_8150" s="T436">идти-PTCP.PRS</ta>
            <ta e="T438" id="Seg_8151" s="T437">место-PL-1PL.[NOM]</ta>
            <ta e="T439" id="Seg_8152" s="T438">сузиться-PST2-3PL</ta>
            <ta e="T440" id="Seg_8153" s="T439">олень-DAT/LOC</ta>
            <ta e="T441" id="Seg_8154" s="T440">EMPH</ta>
            <ta e="T442" id="Seg_8155" s="T441">как</ta>
            <ta e="T443" id="Seg_8156" s="T442">олень-DAT/LOC</ta>
            <ta e="T444" id="Seg_8157" s="T443">так</ta>
            <ta e="T445" id="Seg_8158" s="T444">человек-DAT/LOC</ta>
            <ta e="T446" id="Seg_8159" s="T445">EMPH</ta>
            <ta e="T447" id="Seg_8160" s="T446">человек-PL-1PL.[NOM]</ta>
            <ta e="T449" id="Seg_8161" s="T447">ээ</ta>
            <ta e="T453" id="Seg_8162" s="T451">другой.[NOM]</ta>
            <ta e="T454" id="Seg_8163" s="T453">другой.[NOM]</ta>
            <ta e="T456" id="Seg_8164" s="T454">жизнь-ACC</ta>
            <ta e="T460" id="Seg_8165" s="T458">другой.[NOM]</ta>
            <ta e="T461" id="Seg_8166" s="T460">жизнь-ACC</ta>
            <ta e="T462" id="Seg_8167" s="T461">доступный.[NOM]</ta>
            <ta e="T463" id="Seg_8168" s="T462">жизнь-ACC</ta>
            <ta e="T464" id="Seg_8169" s="T463">следовать-CVB.SEQ-3PL</ta>
            <ta e="T465" id="Seg_8170" s="T464">доступный.[NOM]</ta>
            <ta e="T466" id="Seg_8171" s="T465">потом</ta>
            <ta e="T467" id="Seg_8172" s="T466">доступный.[NOM]</ta>
            <ta e="T468" id="Seg_8173" s="T467">быть-FUT.[3SG]</ta>
            <ta e="T469" id="Seg_8174" s="T468">Новорыбное-DAT/LOC</ta>
            <ta e="T470" id="Seg_8175" s="T469">жить</ta>
            <ta e="T471" id="Seg_8176" s="T470">ээ</ta>
            <ta e="T472" id="Seg_8177" s="T471">потом</ta>
            <ta e="T475" id="Seg_8178" s="T474">Новорыбное.[NOM]</ta>
            <ta e="T476" id="Seg_8179" s="T475">сторона-3SG-INSTR</ta>
            <ta e="T477" id="Seg_8180" s="T476">рассказывать-PRS-1SG</ta>
            <ta e="T478" id="Seg_8181" s="T477">потом</ta>
            <ta e="T479" id="Seg_8182" s="T478">Новорыбное-DAT/LOC</ta>
            <ta e="T480" id="Seg_8183" s="T479">лучше</ta>
            <ta e="T481" id="Seg_8184" s="T480">EMPH</ta>
            <ta e="T482" id="Seg_8185" s="T481">свет-PROPR-3PL</ta>
            <ta e="T483" id="Seg_8186" s="T482">EMPH</ta>
            <ta e="T484" id="Seg_8187" s="T483">теплый.[NOM]</ta>
            <ta e="T485" id="Seg_8188" s="T484">EMPH</ta>
            <ta e="T486" id="Seg_8189" s="T485">тот.[NOM]</ta>
            <ta e="T487" id="Seg_8190" s="T486">из_за</ta>
            <ta e="T488" id="Seg_8191" s="T487">тундра</ta>
            <ta e="T489" id="Seg_8192" s="T488">тундра-ADJZ.[NOM]</ta>
            <ta e="T490" id="Seg_8193" s="T489">человек-PL.[NOM]</ta>
            <ta e="T491" id="Seg_8194" s="T490">тундра-DAT/LOC</ta>
            <ta e="T492" id="Seg_8195" s="T491">идти-PTCP.FUT-3PL-ACC</ta>
            <ta e="T493" id="Seg_8196" s="T492">очень</ta>
            <ta e="T494" id="Seg_8197" s="T493">NEG</ta>
            <ta e="T495" id="Seg_8198" s="T494">тянуть-EP-CAUS-NEG-3PL</ta>
            <ta e="T496" id="Seg_8199" s="T495">деньги.[NOM]</ta>
            <ta e="T497" id="Seg_8200" s="T496">очень</ta>
            <ta e="T498" id="Seg_8201" s="T497">ээ</ta>
            <ta e="T499" id="Seg_8202" s="T498">олень-ABL</ta>
            <ta e="T500" id="Seg_8203" s="T499">что-ACC</ta>
            <ta e="T501" id="Seg_8204" s="T500">получить-FUT-2SG=Q</ta>
            <ta e="T502" id="Seg_8205" s="T501">вот</ta>
            <ta e="T503" id="Seg_8206" s="T502">теперь</ta>
            <ta e="T504" id="Seg_8207" s="T503">убить.[IMP.2SG]</ta>
            <ta e="T505" id="Seg_8208" s="T504">да</ta>
            <ta e="T506" id="Seg_8209" s="T505">тот</ta>
            <ta e="T507" id="Seg_8210" s="T506">вот</ta>
            <ta e="T508" id="Seg_8211" s="T507">мало</ta>
            <ta e="T509" id="Seg_8212" s="T508">пять-десять-APRX</ta>
            <ta e="T510" id="Seg_8213" s="T509">олень-ACC</ta>
            <ta e="T511" id="Seg_8214" s="T510">EMPH</ta>
            <ta e="T512" id="Seg_8215" s="T511">убить-TEMP-2SG</ta>
            <ta e="T513" id="Seg_8216" s="T512">очень</ta>
            <ta e="T514" id="Seg_8217" s="T513">NEG</ta>
            <ta e="T515" id="Seg_8218" s="T514">много</ta>
            <ta e="T516" id="Seg_8219" s="T515">деньги-ACC</ta>
            <ta e="T517" id="Seg_8220" s="T516">зарабатывать-FUT-2SG</ta>
            <ta e="T518" id="Seg_8221" s="T517">NEG-3SG</ta>
            <ta e="T519" id="Seg_8222" s="T518">тот</ta>
            <ta e="T520" id="Seg_8223" s="T519">сторона-3SG-DAT/LOC</ta>
            <ta e="T521" id="Seg_8224" s="T520">или</ta>
            <ta e="T522" id="Seg_8225" s="T521">тундра-DAT/LOC</ta>
            <ta e="T523" id="Seg_8226" s="T522">идти-CVB.SEQ-2SG</ta>
            <ta e="T524" id="Seg_8227" s="T523">MOD</ta>
            <ta e="T525" id="Seg_8228" s="T524">тот.[NOM]</ta>
            <ta e="T526" id="Seg_8229" s="T525">из_за</ta>
            <ta e="T527" id="Seg_8230" s="T526">счастливый</ta>
            <ta e="T528" id="Seg_8231" s="T527">жить-PTCP.FUT-DAT/LOC</ta>
            <ta e="T529" id="Seg_8232" s="T528">и</ta>
            <ta e="T530" id="Seg_8233" s="T529">тундра-DAT/LOC</ta>
            <ta e="T531" id="Seg_8234" s="T530">человек-PL.[NOM]</ta>
            <ta e="T532" id="Seg_8235" s="T531">счастливый-ADVZ</ta>
            <ta e="T533" id="Seg_8236" s="T532">жить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T534" id="Seg_8237" s="T533">тот.[NOM]</ta>
            <ta e="T535" id="Seg_8238" s="T534">первый</ta>
            <ta e="T536" id="Seg_8239" s="T535">тот</ta>
            <ta e="T537" id="Seg_8240" s="T536">сторона-3SG-DAT/LOC</ta>
            <ta e="T538" id="Seg_8241" s="T537">правительство.[NOM]</ta>
            <ta e="T539" id="Seg_8242" s="T538">думать-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T540" id="Seg_8243" s="T539">надо</ta>
            <ta e="T541" id="Seg_8244" s="T540">человек-PL-DAT/LOC</ta>
            <ta e="T542" id="Seg_8245" s="T541">помощь.[NOM]</ta>
            <ta e="T543" id="Seg_8246" s="T542">и</ta>
            <ta e="T544" id="Seg_8247" s="T543">человек-PL.[NOM]</ta>
            <ta e="T545" id="Seg_8248" s="T544">сам-3PL.[NOM]</ta>
            <ta e="T546" id="Seg_8249" s="T545">человек-PL.[NOM]</ta>
            <ta e="T547" id="Seg_8250" s="T546">что-ACC</ta>
            <ta e="T548" id="Seg_8251" s="T547">NEG</ta>
            <ta e="T549" id="Seg_8252" s="T548">делать-FUT-3PL</ta>
            <ta e="T550" id="Seg_8253" s="T549">NEG-3SG</ta>
            <ta e="T551" id="Seg_8254" s="T550">тундра-ADJZ.[NOM]</ta>
            <ta e="T552" id="Seg_8255" s="T551">человек-PL.[NOM]</ta>
            <ta e="T553" id="Seg_8256" s="T552">тем.более</ta>
            <ta e="T554" id="Seg_8257" s="T553">сам-3PL-GEN</ta>
            <ta e="T555" id="Seg_8258" s="T554">передняя.часть-3PL-GEN</ta>
            <ta e="T556" id="Seg_8259" s="T555">сторона-3SG-INSTR</ta>
            <ta e="T557" id="Seg_8260" s="T556">что-ACC</ta>
            <ta e="T558" id="Seg_8261" s="T557">NEG</ta>
            <ta e="T559" id="Seg_8262" s="T558">говорить-EP-NEG-3PL</ta>
            <ta e="T560" id="Seg_8263" s="T559">совхоз-PL-1PL.[NOM]</ta>
            <ta e="T561" id="Seg_8264" s="T560">директор-1PL.[NOM]</ta>
            <ta e="T562" id="Seg_8265" s="T561">директор-PL.[NOM]</ta>
            <ta e="T563" id="Seg_8266" s="T562">EMPH</ta>
            <ta e="T564" id="Seg_8267" s="T563">измениться-PRS-3PL</ta>
            <ta e="T565" id="Seg_8268" s="T564">EMPH</ta>
            <ta e="T566" id="Seg_8269" s="T565">теперь</ta>
            <ta e="T567" id="Seg_8270" s="T566">еще</ta>
            <ta e="T568" id="Seg_8271" s="T567">немного</ta>
            <ta e="T569" id="Seg_8272" s="T568">1PL-DAT/LOC</ta>
            <ta e="T570" id="Seg_8273" s="T569">Новорыбное-DAT/LOC</ta>
            <ta e="T571" id="Seg_8274" s="T570">еще</ta>
            <ta e="T572" id="Seg_8275" s="T571">стоять-CVB.SIM</ta>
            <ta e="T573" id="Seg_8276" s="T572">падать-PST1-3SG</ta>
            <ta e="T574" id="Seg_8277" s="T573">EVID</ta>
            <ta e="T575" id="Seg_8278" s="T574">один</ta>
            <ta e="T576" id="Seg_8279" s="T575">директор.[NOM]</ta>
            <ta e="T577" id="Seg_8280" s="T576">Момонов</ta>
            <ta e="T578" id="Seg_8281" s="T577">Владимир</ta>
            <ta e="T579" id="Seg_8282" s="T578">Александрович.[NOM]</ta>
            <ta e="T580" id="Seg_8283" s="T579">говорить-CVB.SEQ</ta>
            <ta e="T581" id="Seg_8284" s="T580">человек.[NOM]</ta>
            <ta e="T582" id="Seg_8285" s="T581">тот.[NOM]</ta>
            <ta e="T583" id="Seg_8286" s="T582">тот.[NOM]</ta>
            <ta e="T584" id="Seg_8287" s="T583">сторона-3SG-INSTR</ta>
            <ta e="T585" id="Seg_8288" s="T584">EMPH</ta>
            <ta e="T586" id="Seg_8289" s="T585">что.[NOM]</ta>
            <ta e="T587" id="Seg_8290" s="T586">NEG</ta>
            <ta e="T588" id="Seg_8291" s="T587">плохой-ACC</ta>
            <ta e="T589" id="Seg_8292" s="T588">говорить-NEG-1SG</ta>
            <ta e="T590" id="Seg_8293" s="T589">замечательный-PROPR.[NOM]</ta>
            <ta e="T591" id="Seg_8294" s="T590">человек.[NOM]</ta>
            <ta e="T592" id="Seg_8295" s="T591">ээ</ta>
            <ta e="T593" id="Seg_8296" s="T592">человек-PL-ACC</ta>
            <ta e="T594" id="Seg_8297" s="T593">с</ta>
            <ta e="T595" id="Seg_8298" s="T594">теперь</ta>
            <ta e="T596" id="Seg_8299" s="T595">немного</ta>
            <ta e="T597" id="Seg_8300" s="T596">деньги-PROPR.[NOM]</ta>
            <ta e="T598" id="Seg_8301" s="T597">становиться-PST2-3PL</ta>
            <ta e="T599" id="Seg_8302" s="T598">тот.[NOM]</ta>
            <ta e="T600" id="Seg_8303" s="T599">кто.[NOM]</ta>
            <ta e="T601" id="Seg_8304" s="T600">быть-PST1-3SG</ta>
            <ta e="T604" id="Seg_8305" s="T603">давать-PTCP.HAB</ta>
            <ta e="T605" id="Seg_8306" s="T604">быть-PST1-3PL</ta>
            <ta e="T606" id="Seg_8307" s="T605">продукция.[NOM]</ta>
            <ta e="T607" id="Seg_8308" s="T606">собственный-3SG-ACC</ta>
            <ta e="T608" id="Seg_8309" s="T607">вот</ta>
            <ta e="T609" id="Seg_8310" s="T608">в.этом.году</ta>
            <ta e="T610" id="Seg_8311" s="T609">делать-PST2-3PL</ta>
            <ta e="T611" id="Seg_8312" s="T610">тот-3SG-3PL-ACC</ta>
            <ta e="T612" id="Seg_8313" s="T611">EMPH</ta>
            <ta e="T613" id="Seg_8314" s="T612">убирать-VBZ-FUT-1PL</ta>
            <ta e="T614" id="Seg_8315" s="T613">тот-ACC</ta>
            <ta e="T615" id="Seg_8316" s="T614">убирать-VBZ-TEMP-3PL</ta>
            <ta e="T616" id="Seg_8317" s="T615">теперь</ta>
            <ta e="T617" id="Seg_8318" s="T616">тундра-ADJZ.[NOM]</ta>
            <ta e="T618" id="Seg_8319" s="T617">ээ</ta>
            <ta e="T619" id="Seg_8320" s="T618">охотник-PL.[NOM]</ta>
            <ta e="T620" id="Seg_8321" s="T619">EMPH</ta>
            <ta e="T621" id="Seg_8322" s="T620">тоже</ta>
            <ta e="T624" id="Seg_8323" s="T623">стрелять-FREQ-NMNZ-AG.[NOM]</ta>
            <ta e="T625" id="Seg_8324" s="T624">человек-PL.[NOM]</ta>
            <ta e="T626" id="Seg_8325" s="T625">EMPH</ta>
            <ta e="T627" id="Seg_8326" s="T626">мало-ACC</ta>
            <ta e="T628" id="Seg_8327" s="T627">получить-FUT-3PL</ta>
            <ta e="T629" id="Seg_8328" s="T628">ведь</ta>
            <ta e="T630" id="Seg_8329" s="T629">один</ta>
            <ta e="T633" id="Seg_8330" s="T631">уметь</ta>
            <ta e="T635" id="Seg_8331" s="T633">уметь-FUT-3PL</ta>
            <ta e="T636" id="Seg_8332" s="T635">NEG-3PL</ta>
            <ta e="T637" id="Seg_8333" s="T636">AFFIRM</ta>
            <ta e="T659" id="Seg_8334" s="T657">тот-INSTR</ta>
            <ta e="T660" id="Seg_8335" s="T659">надеяться-PRS-1PL</ta>
            <ta e="T661" id="Seg_8336" s="T660">AFFIRM</ta>
            <ta e="T662" id="Seg_8337" s="T661">тот-INSTR</ta>
            <ta e="T663" id="Seg_8338" s="T662">надеяться-PRS-1PL</ta>
            <ta e="T664" id="Seg_8339" s="T663">а</ta>
            <ta e="T665" id="Seg_8340" s="T664">так</ta>
            <ta e="T666" id="Seg_8341" s="T665">тот.[NOM]</ta>
            <ta e="T667" id="Seg_8342" s="T666">темный</ta>
            <ta e="T668" id="Seg_8343" s="T667">сторона.[NOM]</ta>
            <ta e="T669" id="Seg_8344" s="T668">сторона-3SG-ACC</ta>
            <ta e="T670" id="Seg_8345" s="T669">разговаривать-PST1-1PL</ta>
            <ta e="T671" id="Seg_8346" s="T670">1PL.[NOM]</ta>
            <ta e="T672" id="Seg_8347" s="T671">а</ta>
            <ta e="T673" id="Seg_8348" s="T672">так</ta>
            <ta e="T674" id="Seg_8349" s="T673">EMPH</ta>
            <ta e="T675" id="Seg_8350" s="T674">тундра-DAT/LOC</ta>
            <ta e="T676" id="Seg_8351" s="T675">жить-PTCP.COND-DAT/LOC</ta>
            <ta e="T677" id="Seg_8352" s="T676">очень</ta>
            <ta e="T678" id="Seg_8353" s="T677">веселый.[NOM]</ta>
            <ta e="T679" id="Seg_8354" s="T678">быть-PST1-3SG</ta>
            <ta e="T680" id="Seg_8355" s="T679">ээ</ta>
            <ta e="T681" id="Seg_8356" s="T680">тундра-ADJZ.[NOM]</ta>
            <ta e="T682" id="Seg_8357" s="T681">человек-PL.[NOM]</ta>
            <ta e="T683" id="Seg_8358" s="T682">теперь</ta>
            <ta e="T684" id="Seg_8359" s="T683">ребенок-PL.[NOM]</ta>
            <ta e="T685" id="Seg_8360" s="T684">идти-EP-NEG.PTCP</ta>
            <ta e="T686" id="Seg_8361" s="T685">быть-CVB.SEQ-3PL</ta>
            <ta e="T687" id="Seg_8362" s="T686">теперь</ta>
            <ta e="T688" id="Seg_8363" s="T687">знать-NEG-3PL</ta>
            <ta e="T689" id="Seg_8364" s="T688">EMPH</ta>
            <ta e="T690" id="Seg_8365" s="T689">долганский.[NOM]</ta>
            <ta e="T691" id="Seg_8366" s="T690">язык-3SG-ACC</ta>
            <ta e="T692" id="Seg_8367" s="T691">чистый.[NOM]</ta>
            <ta e="T693" id="Seg_8368" s="T692">дыхание-3PL.[NOM]</ta>
            <ta e="T694" id="Seg_8369" s="T693">ээ</ta>
            <ta e="T695" id="Seg_8370" s="T694">воздух-ACC</ta>
            <ta e="T696" id="Seg_8371" s="T695">трава.[NOM]</ta>
            <ta e="T697" id="Seg_8372" s="T696">запах-3SG-ACC</ta>
            <ta e="T698" id="Seg_8373" s="T697">говорить-PTCP.PST-DAT/LOC</ta>
            <ta e="T699" id="Seg_8374" s="T698">говорить-CVB.PURP</ta>
            <ta e="T700" id="Seg_8375" s="T699">первый</ta>
            <ta e="T701" id="Seg_8376" s="T700">гусь.[NOM]</ta>
            <ta e="T702" id="Seg_8377" s="T701">шум-3SG-ACC</ta>
            <ta e="T703" id="Seg_8378" s="T702">теленок-PL.[NOM]</ta>
            <ta e="T704" id="Seg_8379" s="T703">родиться-PTCP.PRS-3PL-ACC</ta>
            <ta e="T705" id="Seg_8380" s="T704">целый-3SG-ACC</ta>
            <ta e="T706" id="Seg_8381" s="T705">что-ACC</ta>
            <ta e="T707" id="Seg_8382" s="T706">NEG</ta>
            <ta e="T708" id="Seg_8383" s="T707">очень</ta>
            <ta e="T709" id="Seg_8384" s="T708">знать-NEG-3PL</ta>
            <ta e="T710" id="Seg_8385" s="T709">EMPH</ta>
            <ta e="T711" id="Seg_8386" s="T710">тот-3SG-2SG.[NOM]</ta>
            <ta e="T712" id="Seg_8387" s="T711">каждый-3SG.[NOM]</ta>
            <ta e="T713" id="Seg_8388" s="T712">1PL-DAT/LOC</ta>
            <ta e="T714" id="Seg_8389" s="T713">ээ</ta>
            <ta e="T715" id="Seg_8390" s="T714">тундра-DAT/LOC</ta>
            <ta e="T716" id="Seg_8391" s="T715">рождаться-PTCP.PST</ta>
            <ta e="T717" id="Seg_8392" s="T716">человек-PL-DAT/LOC</ta>
            <ta e="T718" id="Seg_8393" s="T717">тот-3SG-2SG.[NOM]</ta>
            <ta e="T719" id="Seg_8394" s="T718">каждый-3SG.[NOM]</ta>
            <ta e="T720" id="Seg_8395" s="T719">каждый-3SG.[NOM]</ta>
            <ta e="T721" id="Seg_8396" s="T720">большой.[NOM]</ta>
            <ta e="T722" id="Seg_8397" s="T721">кто.[NOM]</ta>
            <ta e="T723" id="Seg_8398" s="T722">EMPH</ta>
            <ta e="T725" id="Seg_8399" s="T723">впечатление.[NOM]</ta>
            <ta e="T764" id="Seg_8400" s="T763">спасибо</ta>
         </annotation>
         <annotation name="mc" tierref="mc-UkOA">
            <ta e="T8" id="Seg_8401" s="T7">interj</ta>
            <ta e="T9" id="Seg_8402" s="T8">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T10" id="Seg_8403" s="T9">propr-n:case</ta>
            <ta e="T11" id="Seg_8404" s="T10">n-n&gt;adj.[n:case]</ta>
            <ta e="T12" id="Seg_8405" s="T11">v-v:cvb-v:pred.pn</ta>
            <ta e="T13" id="Seg_8406" s="T12">interj</ta>
            <ta e="T14" id="Seg_8407" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_8408" s="T14">quant-quant&gt;adv</ta>
            <ta e="T16" id="Seg_8409" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_8410" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_8411" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_8412" s="T18">cardnum</ta>
            <ta e="T20" id="Seg_8413" s="T19">n-n:(num)-n:case</ta>
            <ta e="T21" id="Seg_8414" s="T20">n-n:(num)-n:case</ta>
            <ta e="T22" id="Seg_8415" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_8416" s="T22">adv</ta>
            <ta e="T24" id="Seg_8417" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_8418" s="T24">v-v:tense-v:pred.pn</ta>
            <ta e="T26" id="Seg_8419" s="T25">que</ta>
            <ta e="T27" id="Seg_8420" s="T26">n.[n:case]</ta>
            <ta e="T28" id="Seg_8421" s="T27">adj</ta>
            <ta e="T29" id="Seg_8422" s="T28">n-n:(poss)-n&gt;adj-n:poss-n:case</ta>
            <ta e="T30" id="Seg_8423" s="T29">n-n:(poss)-n&gt;adj-n:poss-n:case</ta>
            <ta e="T31" id="Seg_8424" s="T30">v-v:cvb</ta>
            <ta e="T32" id="Seg_8425" s="T31">dempro.[pro:case]</ta>
            <ta e="T33" id="Seg_8426" s="T32">post</ta>
            <ta e="T34" id="Seg_8427" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_8428" s="T34">v-v:cvb</ta>
            <ta e="T36" id="Seg_8429" s="T35">v-v:cvb-v:pred.pn</ta>
            <ta e="T38" id="Seg_8430" s="T37">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T39" id="Seg_8431" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_8432" s="T39">dempro</ta>
            <ta e="T41" id="Seg_8433" s="T40">n-n:(num).[n:case]</ta>
            <ta e="T42" id="Seg_8434" s="T41">n-n:poss-n:case</ta>
            <ta e="T43" id="Seg_8435" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_8436" s="T43">v-v:cvb</ta>
            <ta e="T45" id="Seg_8437" s="T44">adv</ta>
            <ta e="T46" id="Seg_8438" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_8439" s="T46">dempro-pro:case</ta>
            <ta e="T48" id="Seg_8440" s="T47">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T49" id="Seg_8441" s="T48">ptcl</ta>
            <ta e="T51" id="Seg_8442" s="T50">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_8443" s="T51">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T53" id="Seg_8444" s="T52">interj</ta>
            <ta e="T54" id="Seg_8445" s="T53">que-pro:(ins)-pro:case</ta>
            <ta e="T55" id="Seg_8446" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_8447" s="T55">v-v:ptcp-v:(case)</ta>
            <ta e="T57" id="Seg_8448" s="T56">v-v:cvb</ta>
            <ta e="T60" id="Seg_8449" s="T59">propr-n:case</ta>
            <ta e="T61" id="Seg_8450" s="T60">v-v:cvb</ta>
            <ta e="T62" id="Seg_8451" s="T61">n-n:poss-n:case</ta>
            <ta e="T63" id="Seg_8452" s="T62">v-v:tense-v:pred.pn</ta>
            <ta e="T64" id="Seg_8453" s="T63">dempro.[pro:case]</ta>
            <ta e="T65" id="Seg_8454" s="T64">post</ta>
            <ta e="T85" id="Seg_8455" s="T84">n-n&gt;adj-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T86" id="Seg_8456" s="T85">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T87" id="Seg_8457" s="T86">v-v:tense-v:pred.pn</ta>
            <ta e="T88" id="Seg_8458" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_8459" s="T88">que</ta>
            <ta e="T90" id="Seg_8460" s="T89">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T91" id="Seg_8461" s="T90">adv</ta>
            <ta e="T92" id="Seg_8462" s="T91">adj.[n:case]</ta>
            <ta e="T93" id="Seg_8463" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_8464" s="T93">v-v:ptcp</ta>
            <ta e="T95" id="Seg_8465" s="T94">v-v:cvb-v:pred.pn</ta>
            <ta e="T96" id="Seg_8466" s="T95">pers.[pro:case]</ta>
            <ta e="T97" id="Seg_8467" s="T96">interj</ta>
            <ta e="T98" id="Seg_8468" s="T97">adj-adj&gt;adv</ta>
            <ta e="T99" id="Seg_8469" s="T98">v-v:tense-v:poss.pn</ta>
            <ta e="T100" id="Seg_8470" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_8471" s="T100">v-v:(neg)-v:pred.pn</ta>
            <ta e="T102" id="Seg_8472" s="T101">adv</ta>
            <ta e="T103" id="Seg_8473" s="T102">adv</ta>
            <ta e="T104" id="Seg_8474" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_8475" s="T104">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T106" id="Seg_8476" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_8477" s="T106">que.[pro:case]</ta>
            <ta e="T108" id="Seg_8478" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_8479" s="T108">dempro-pro:case</ta>
            <ta e="T110" id="Seg_8480" s="T109">v-v:cvb</ta>
            <ta e="T111" id="Seg_8481" s="T110">v-v:tense-v:pred.pn</ta>
            <ta e="T112" id="Seg_8482" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_8483" s="T112">v-v:mood-v:temp.pn</ta>
            <ta e="T114" id="Seg_8484" s="T113">que</ta>
            <ta e="T115" id="Seg_8485" s="T114">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T116" id="Seg_8486" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_8487" s="T116">ptcl</ta>
            <ta e="T119" id="Seg_8488" s="T117">ptcl</ta>
            <ta e="T124" id="Seg_8489" s="T123">adv</ta>
            <ta e="T125" id="Seg_8490" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_8491" s="T125">v-v:(neg)-v:pred.pn</ta>
            <ta e="T127" id="Seg_8492" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_8493" s="T127">conj</ta>
            <ta e="T129" id="Seg_8494" s="T128">conj</ta>
            <ta e="T130" id="Seg_8495" s="T129">dempro</ta>
            <ta e="T132" id="Seg_8496" s="T131">adj-adj&gt;adv</ta>
            <ta e="T133" id="Seg_8497" s="T132">interj</ta>
            <ta e="T134" id="Seg_8498" s="T133">n-n&gt;adv</ta>
            <ta e="T135" id="Seg_8499" s="T134">dempro</ta>
            <ta e="T136" id="Seg_8500" s="T135">adj-adj&gt;adv</ta>
            <ta e="T137" id="Seg_8501" s="T136">v-v:ptcp</ta>
            <ta e="T138" id="Seg_8502" s="T137">n.[n:case]</ta>
            <ta e="T139" id="Seg_8503" s="T138">dempro.[pro:case]</ta>
            <ta e="T140" id="Seg_8504" s="T139">adv</ta>
            <ta e="T141" id="Seg_8505" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_8506" s="T141">adj</ta>
            <ta e="T143" id="Seg_8507" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_8508" s="T143">n-n:(num)-n:case</ta>
            <ta e="T145" id="Seg_8509" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_8510" s="T145">v-v:ptcp</ta>
            <ta e="T147" id="Seg_8511" s="T146">v-v:tense-v:poss.pn</ta>
            <ta e="T148" id="Seg_8512" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_8513" s="T148">dempro</ta>
            <ta e="T150" id="Seg_8514" s="T149">adj.[n:case]</ta>
            <ta e="T151" id="Seg_8515" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_8516" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_8517" s="T152">n-n&gt;adj.[n:case]</ta>
            <ta e="T154" id="Seg_8518" s="T153">n-n:(poss).[n:case]</ta>
            <ta e="T155" id="Seg_8519" s="T154">que-pro:(poss).[pro:case]</ta>
            <ta e="T157" id="Seg_8520" s="T156">dempro</ta>
            <ta e="T158" id="Seg_8521" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_8522" s="T158">n.[n:case]</ta>
            <ta e="T160" id="Seg_8523" s="T159">n.[n:case]</ta>
            <ta e="T161" id="Seg_8524" s="T160">post</ta>
            <ta e="T162" id="Seg_8525" s="T161">adj-adj&gt;adv</ta>
            <ta e="T163" id="Seg_8526" s="T162">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T164" id="Seg_8527" s="T163">v-v:cvb</ta>
            <ta e="T165" id="Seg_8528" s="T164">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T166" id="Seg_8529" s="T165">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T167" id="Seg_8530" s="T166">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T168" id="Seg_8531" s="T167">n-n:poss-n:case</ta>
            <ta e="T169" id="Seg_8532" s="T168">v-v:(ins)-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T170" id="Seg_8533" s="T169">n-n&gt;adj-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T171" id="Seg_8534" s="T170">dempro.[pro:case]</ta>
            <ta e="T172" id="Seg_8535" s="T171">post</ta>
            <ta e="T173" id="Seg_8536" s="T172">adv</ta>
            <ta e="T174" id="Seg_8537" s="T173">adv</ta>
            <ta e="T175" id="Seg_8538" s="T174">dempro.[pro:case]</ta>
            <ta e="T176" id="Seg_8539" s="T175">cardnum</ta>
            <ta e="T177" id="Seg_8540" s="T176">cardnum</ta>
            <ta e="T178" id="Seg_8541" s="T177">cardnum</ta>
            <ta e="T179" id="Seg_8542" s="T178">dempro.[pro:case]</ta>
            <ta e="T180" id="Seg_8543" s="T179">que.[pro:case]</ta>
            <ta e="T181" id="Seg_8544" s="T180">adv</ta>
            <ta e="T182" id="Seg_8545" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_8546" s="T182">v-v:cvb</ta>
            <ta e="T184" id="Seg_8547" s="T183">v-v:cvb-v:pred.pn</ta>
            <ta e="T185" id="Seg_8548" s="T184">adv</ta>
            <ta e="T186" id="Seg_8549" s="T185">quant</ta>
            <ta e="T187" id="Seg_8550" s="T186">interj</ta>
            <ta e="T188" id="Seg_8551" s="T187">conj</ta>
            <ta e="T189" id="Seg_8552" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_8553" s="T189">adv</ta>
            <ta e="T191" id="Seg_8554" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_8555" s="T191">que.[pro:case]</ta>
            <ta e="T193" id="Seg_8556" s="T192">adv</ta>
            <ta e="T194" id="Seg_8557" s="T193">ptcl</ta>
            <ta e="T196" id="Seg_8558" s="T195">n-n:(ins)-n:case</ta>
            <ta e="T197" id="Seg_8559" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_8560" s="T197">n-n:(ins)-n:case</ta>
            <ta e="T199" id="Seg_8561" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_8562" s="T199">que-pro:(ins)-pro:case</ta>
            <ta e="T201" id="Seg_8563" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_8564" s="T201">dempro</ta>
            <ta e="T203" id="Seg_8565" s="T202">n-n:(poss).[n:case]</ta>
            <ta e="T204" id="Seg_8566" s="T203">v-v:ptcp</ta>
            <ta e="T205" id="Seg_8567" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_8568" s="T205">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T207" id="Seg_8569" s="T206">v-v:(ins)-v&gt;v-v:(ins)-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T208" id="Seg_8570" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_8571" s="T208">v-v:cvb-v:pred.pn</ta>
            <ta e="T210" id="Seg_8572" s="T209">que</ta>
            <ta e="T211" id="Seg_8573" s="T210">que</ta>
            <ta e="T212" id="Seg_8574" s="T211">adj.[n:case]</ta>
            <ta e="T213" id="Seg_8575" s="T212">n.[n:case]</ta>
            <ta e="T214" id="Seg_8576" s="T213">v-v:tense-v:poss.pn</ta>
            <ta e="T215" id="Seg_8577" s="T214">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T216" id="Seg_8578" s="T215">adj.[n:case]</ta>
            <ta e="T217" id="Seg_8579" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_8580" s="T217">adj</ta>
            <ta e="T219" id="Seg_8581" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_8582" s="T219">v-v:cvb</ta>
            <ta e="T221" id="Seg_8583" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_8584" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_8585" s="T222">v-v:cvb</ta>
            <ta e="T224" id="Seg_8586" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_8587" s="T224">v-v:cvb</ta>
            <ta e="T226" id="Seg_8588" s="T225">v-v:tense-v:poss.pn</ta>
            <ta e="T227" id="Seg_8589" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_8590" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_8591" s="T228">v-v:(neg)-v:pred.pn</ta>
            <ta e="T230" id="Seg_8592" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_8593" s="T230">dempro</ta>
            <ta e="T232" id="Seg_8594" s="T231">adv</ta>
            <ta e="T233" id="Seg_8595" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_8596" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_8597" s="T234">v-v:(neg)-v:pred.pn</ta>
            <ta e="T236" id="Seg_8598" s="T235">n-n&gt;adj-n:(num).[n:case]</ta>
            <ta e="T237" id="Seg_8599" s="T236">adv</ta>
            <ta e="T238" id="Seg_8600" s="T237">n.[n:case]</ta>
            <ta e="T239" id="Seg_8601" s="T238">que.[pro:case]</ta>
            <ta e="T240" id="Seg_8602" s="T239">v-v:tense-v:pred.pn</ta>
            <ta e="T242" id="Seg_8603" s="T241">n-n:(num).[n:case]</ta>
            <ta e="T243" id="Seg_8604" s="T242">v-v:tense-v:pred.pn</ta>
            <ta e="T244" id="Seg_8605" s="T243">adv</ta>
            <ta e="T245" id="Seg_8606" s="T244">adj-n:case</ta>
            <ta e="T246" id="Seg_8607" s="T245">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T247" id="Seg_8608" s="T246">que.[pro:case]</ta>
            <ta e="T248" id="Seg_8609" s="T247">n-n:(poss).[n:case]=ptcl</ta>
            <ta e="T249" id="Seg_8610" s="T248">v-v:cvb</ta>
            <ta e="T250" id="Seg_8611" s="T249">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T251" id="Seg_8612" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_8613" s="T251">interj</ta>
            <ta e="T253" id="Seg_8614" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_8615" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_8616" s="T254">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T256" id="Seg_8617" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_8618" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_8619" s="T257">v-v:tense-v:poss.pn</ta>
            <ta e="T259" id="Seg_8620" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_8621" s="T259">n.[n:case]</ta>
            <ta e="T261" id="Seg_8622" s="T260">v-v:tense-v:poss.pn</ta>
            <ta e="T262" id="Seg_8623" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_8624" s="T262">n.[n:case]</ta>
            <ta e="T264" id="Seg_8625" s="T263">v-v:tense-v:poss.pn</ta>
            <ta e="T265" id="Seg_8626" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_8627" s="T265">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T267" id="Seg_8628" s="T266">adv</ta>
            <ta e="T268" id="Seg_8629" s="T267">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T269" id="Seg_8630" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_8631" s="T269">que.[pro:case]</ta>
            <ta e="T271" id="Seg_8632" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_8633" s="T271">adv</ta>
            <ta e="T273" id="Seg_8634" s="T272">v-v:cvb</ta>
            <ta e="T274" id="Seg_8635" s="T273">adv</ta>
            <ta e="T275" id="Seg_8636" s="T274">n-n:(num)-n:case</ta>
            <ta e="T276" id="Seg_8637" s="T275">que-pro:(num)-pro:case</ta>
            <ta e="T277" id="Seg_8638" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_8639" s="T277">v-v:ptcp</ta>
            <ta e="T279" id="Seg_8640" s="T278">v-v:tense-v:poss.pn</ta>
            <ta e="T280" id="Seg_8641" s="T279">pers-pro:case</ta>
            <ta e="T281" id="Seg_8642" s="T280">que.[pro:case]</ta>
            <ta e="T282" id="Seg_8643" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_8644" s="T282">n.[n:case]</ta>
            <ta e="T284" id="Seg_8645" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_8646" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_8647" s="T285">v-v&gt;v-v:ptcp</ta>
            <ta e="T287" id="Seg_8648" s="T286">v-v:tense-v:poss.pn</ta>
            <ta e="T288" id="Seg_8649" s="T287">adv</ta>
            <ta e="T289" id="Seg_8650" s="T288">dempro</ta>
            <ta e="T290" id="Seg_8651" s="T289">dempro</ta>
            <ta e="T291" id="Seg_8652" s="T290">que.[pro:case]</ta>
            <ta e="T292" id="Seg_8653" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_8654" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_8655" s="T293">adj-n:(poss).[n:case]</ta>
            <ta e="T295" id="Seg_8656" s="T294">n.[n:case]</ta>
            <ta e="T296" id="Seg_8657" s="T295">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T297" id="Seg_8658" s="T296">n-n:poss-n:case</ta>
            <ta e="T298" id="Seg_8659" s="T297">v-v:tense.[v:pred.pn]</ta>
            <ta e="T299" id="Seg_8660" s="T298">que.[pro:case]</ta>
            <ta e="T300" id="Seg_8661" s="T299">n-n:(poss)</ta>
            <ta e="T301" id="Seg_8662" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_8663" s="T301">n.[n:case]</ta>
            <ta e="T303" id="Seg_8664" s="T302">adv</ta>
            <ta e="T304" id="Seg_8665" s="T303">v-v:tense.[v:pred.pn]</ta>
            <ta e="T305" id="Seg_8666" s="T304">v-v:ptcp-v:(case)</ta>
            <ta e="T306" id="Seg_8667" s="T305">v-v:cvb</ta>
            <ta e="T307" id="Seg_8668" s="T306">dempro</ta>
            <ta e="T308" id="Seg_8669" s="T307">v-v:cvb</ta>
            <ta e="T309" id="Seg_8670" s="T308">conj</ta>
            <ta e="T310" id="Seg_8671" s="T309">que.[pro:case]</ta>
            <ta e="T311" id="Seg_8672" s="T310">n-n&gt;adj-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T312" id="Seg_8673" s="T311">quant</ta>
            <ta e="T313" id="Seg_8674" s="T312">v-v:tense-v:pred.pn</ta>
            <ta e="T314" id="Seg_8675" s="T313">n-n:(num)-n:poss-n:case</ta>
            <ta e="T315" id="Seg_8676" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_8677" s="T315">cardnum</ta>
            <ta e="T321" id="Seg_8678" s="T319">v-v:tense-v:pred.pn</ta>
            <ta e="T336" id="Seg_8679" s="T335">interj</ta>
            <ta e="T337" id="Seg_8680" s="T336">n-n:(num).[n:case]</ta>
            <ta e="T338" id="Seg_8681" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_8682" s="T338">n-n:(num).[n:case]</ta>
            <ta e="T340" id="Seg_8683" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_8684" s="T340">v-v:tense-v:poss.pn</ta>
            <ta e="T342" id="Seg_8685" s="T341">pers.[pro:case]</ta>
            <ta e="T343" id="Seg_8686" s="T342">n-n:poss-n:case</ta>
            <ta e="T344" id="Seg_8687" s="T343">n-n:(poss).[n:case]</ta>
            <ta e="T345" id="Seg_8688" s="T344">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T346" id="Seg_8689" s="T345">adv</ta>
            <ta e="T347" id="Seg_8690" s="T346">pers-pro:case</ta>
            <ta e="T348" id="Seg_8691" s="T347">ptcl</ta>
            <ta e="T350" id="Seg_8692" s="T348">v-v:tense-v:poss.pn</ta>
            <ta e="T362" id="Seg_8693" s="T360">emphpro</ta>
            <ta e="T365" id="Seg_8694" s="T362">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T368" id="Seg_8695" s="T365">adv</ta>
            <ta e="T369" id="Seg_8696" s="T368">n-n:(poss).[n:case]</ta>
            <ta e="T370" id="Seg_8697" s="T369">adv</ta>
            <ta e="T371" id="Seg_8698" s="T370">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T372" id="Seg_8699" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_8700" s="T372">adv</ta>
            <ta e="T374" id="Seg_8701" s="T373">quant</ta>
            <ta e="T375" id="Seg_8702" s="T374">n.[n:case]</ta>
            <ta e="T376" id="Seg_8703" s="T375">interj</ta>
            <ta e="T377" id="Seg_8704" s="T376">v-v:tense-v:poss.pn</ta>
            <ta e="T378" id="Seg_8705" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_8706" s="T378">n-n:(poss)</ta>
            <ta e="T380" id="Seg_8707" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_8708" s="T380">v-v:cvb-v:pred.pn</ta>
            <ta e="T382" id="Seg_8709" s="T381">adj-n:(poss).[n:case]</ta>
            <ta e="T383" id="Seg_8710" s="T382">adj.[n:case]</ta>
            <ta e="T384" id="Seg_8711" s="T383">n-n&gt;adj-n:(pred.pn).[n:case]</ta>
            <ta e="T385" id="Seg_8712" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_8713" s="T385">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T387" id="Seg_8714" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_8715" s="T387">interj</ta>
            <ta e="T389" id="Seg_8716" s="T388">dempro</ta>
            <ta e="T390" id="Seg_8717" s="T389">adj-n:(poss).[n:case]</ta>
            <ta e="T391" id="Seg_8718" s="T390">dempro</ta>
            <ta e="T392" id="Seg_8719" s="T391">adj-n:(poss).[n:case]</ta>
            <ta e="T393" id="Seg_8720" s="T392">dempro</ta>
            <ta e="T394" id="Seg_8721" s="T393">adj-n:(poss).[n:case]</ta>
            <ta e="T395" id="Seg_8722" s="T394">v-v:tense.[v:pred.pn]</ta>
            <ta e="T396" id="Seg_8723" s="T395">n.[n:case]</ta>
            <ta e="T397" id="Seg_8724" s="T396">adj-n:(poss).[n:case]</ta>
            <ta e="T398" id="Seg_8725" s="T397">interj</ta>
            <ta e="T399" id="Seg_8726" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_8727" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_8728" s="T400">v-v:tense.[v:pred.pn]</ta>
            <ta e="T403" id="Seg_8729" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_8730" s="T403">v-v:tense.[v:pred.pn]</ta>
            <ta e="T405" id="Seg_8731" s="T404">n.[n:case]</ta>
            <ta e="T406" id="Seg_8732" s="T405">adj-n:(poss).[n:case]</ta>
            <ta e="T407" id="Seg_8733" s="T406">adv</ta>
            <ta e="T408" id="Seg_8734" s="T407">v-v:ptcp</ta>
            <ta e="T409" id="Seg_8735" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_8736" s="T409">n-n:(poss).[n:case]</ta>
            <ta e="T411" id="Seg_8737" s="T410">adv</ta>
            <ta e="T412" id="Seg_8738" s="T411">n-n:case</ta>
            <ta e="T413" id="Seg_8739" s="T412">v-v:(neg)-v:pred.pn</ta>
            <ta e="T414" id="Seg_8740" s="T413">adj.[n:case]</ta>
            <ta e="T415" id="Seg_8741" s="T414">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T416" id="Seg_8742" s="T415">adv</ta>
            <ta e="T417" id="Seg_8743" s="T416">v-v:cvb-v:pred.pn</ta>
            <ta e="T418" id="Seg_8744" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_8745" s="T418">v-v:cvb-v:pred.pn</ta>
            <ta e="T420" id="Seg_8746" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_8747" s="T420">adv</ta>
            <ta e="T422" id="Seg_8748" s="T421">n-n&gt;adj.[n:case]</ta>
            <ta e="T423" id="Seg_8749" s="T422">n-n:(num).[n:case]</ta>
            <ta e="T424" id="Seg_8750" s="T423">adv</ta>
            <ta e="T425" id="Seg_8751" s="T424">n-n:(poss).[n:case]</ta>
            <ta e="T426" id="Seg_8752" s="T425">adv</ta>
            <ta e="T427" id="Seg_8753" s="T426">n-n:poss-n:case</ta>
            <ta e="T428" id="Seg_8754" s="T427">v-v:cvb</ta>
            <ta e="T429" id="Seg_8755" s="T428">pers-pro:case</ta>
            <ta e="T430" id="Seg_8756" s="T429">quant-n:case</ta>
            <ta e="T431" id="Seg_8757" s="T430">v-v:ptcp</ta>
            <ta e="T432" id="Seg_8758" s="T431">v-v:cvb</ta>
            <ta e="T433" id="Seg_8759" s="T432">n-n:poss-n:case</ta>
            <ta e="T434" id="Seg_8760" s="T433">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T435" id="Seg_8761" s="T434">dempro.[pro:case]</ta>
            <ta e="T436" id="Seg_8762" s="T435">post</ta>
            <ta e="T437" id="Seg_8763" s="T436">v-v:ptcp</ta>
            <ta e="T438" id="Seg_8764" s="T437">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T439" id="Seg_8765" s="T438">v-v:tense-v:pred.pn</ta>
            <ta e="T440" id="Seg_8766" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_8767" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_8768" s="T441">que</ta>
            <ta e="T443" id="Seg_8769" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_8770" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_8771" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_8772" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_8773" s="T446">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T449" id="Seg_8774" s="T447">interj</ta>
            <ta e="T453" id="Seg_8775" s="T451">adj.[n:case]</ta>
            <ta e="T454" id="Seg_8776" s="T453">adj.[n:case]</ta>
            <ta e="T456" id="Seg_8777" s="T454">n-n:case</ta>
            <ta e="T460" id="Seg_8778" s="T458">adj.[n:case]</ta>
            <ta e="T461" id="Seg_8779" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_8780" s="T461">adj.[n:case]</ta>
            <ta e="T463" id="Seg_8781" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_8782" s="T463">v-v:cvb-v:pred.pn</ta>
            <ta e="T465" id="Seg_8783" s="T464">adj.[n:case]</ta>
            <ta e="T466" id="Seg_8784" s="T465">adv</ta>
            <ta e="T467" id="Seg_8785" s="T466">adj.[n:case]</ta>
            <ta e="T468" id="Seg_8786" s="T467">v-v:tense.[v:poss.pn]</ta>
            <ta e="T469" id="Seg_8787" s="T468">propr-n:case</ta>
            <ta e="T470" id="Seg_8788" s="T469">v</ta>
            <ta e="T471" id="Seg_8789" s="T470">interj</ta>
            <ta e="T472" id="Seg_8790" s="T471">adv</ta>
            <ta e="T475" id="Seg_8791" s="T474">propr.[n:case]</ta>
            <ta e="T476" id="Seg_8792" s="T475">n-n:poss-n:case</ta>
            <ta e="T477" id="Seg_8793" s="T476">v-v:tense-v:pred.pn</ta>
            <ta e="T478" id="Seg_8794" s="T477">adv</ta>
            <ta e="T479" id="Seg_8795" s="T478">propr-n:case</ta>
            <ta e="T480" id="Seg_8796" s="T479">adv</ta>
            <ta e="T481" id="Seg_8797" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_8798" s="T481">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T483" id="Seg_8799" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_8800" s="T483">adj.[n:case]</ta>
            <ta e="T485" id="Seg_8801" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_8802" s="T485">dempro.[pro:case]</ta>
            <ta e="T487" id="Seg_8803" s="T486">post</ta>
            <ta e="T488" id="Seg_8804" s="T487">n</ta>
            <ta e="T489" id="Seg_8805" s="T488">n-n&gt;adj.[n:case]</ta>
            <ta e="T490" id="Seg_8806" s="T489">n-n:(num).[n:case]</ta>
            <ta e="T491" id="Seg_8807" s="T490">n-n:case</ta>
            <ta e="T492" id="Seg_8808" s="T491">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T493" id="Seg_8809" s="T492">adv</ta>
            <ta e="T494" id="Seg_8810" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_8811" s="T494">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T496" id="Seg_8812" s="T495">n.[n:case]</ta>
            <ta e="T497" id="Seg_8813" s="T496">adv</ta>
            <ta e="T498" id="Seg_8814" s="T497">interj</ta>
            <ta e="T499" id="Seg_8815" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_8816" s="T499">que-pro:case</ta>
            <ta e="T501" id="Seg_8817" s="T500">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T502" id="Seg_8818" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_8819" s="T502">adv</ta>
            <ta e="T504" id="Seg_8820" s="T503">v.[v:mood.pn]</ta>
            <ta e="T505" id="Seg_8821" s="T504">conj</ta>
            <ta e="T506" id="Seg_8822" s="T505">dempro</ta>
            <ta e="T507" id="Seg_8823" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_8824" s="T507">quant</ta>
            <ta e="T509" id="Seg_8825" s="T508">cardnum-cardnum-cardnum&gt;cardnum</ta>
            <ta e="T510" id="Seg_8826" s="T509">n-n:case</ta>
            <ta e="T511" id="Seg_8827" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_8828" s="T511">v-v:mood-v:temp.pn</ta>
            <ta e="T513" id="Seg_8829" s="T512">adv</ta>
            <ta e="T514" id="Seg_8830" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_8831" s="T514">quant</ta>
            <ta e="T516" id="Seg_8832" s="T515">n-n:case</ta>
            <ta e="T517" id="Seg_8833" s="T516">v-v:tense-v:poss.pn</ta>
            <ta e="T518" id="Seg_8834" s="T517">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T519" id="Seg_8835" s="T518">dempro</ta>
            <ta e="T520" id="Seg_8836" s="T519">n-n:poss-n:case</ta>
            <ta e="T521" id="Seg_8837" s="T520">conj</ta>
            <ta e="T522" id="Seg_8838" s="T521">n-n:case</ta>
            <ta e="T523" id="Seg_8839" s="T522">v-v:cvb-v:pred.pn</ta>
            <ta e="T524" id="Seg_8840" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_8841" s="T524">dempro.[pro:case]</ta>
            <ta e="T526" id="Seg_8842" s="T525">post</ta>
            <ta e="T527" id="Seg_8843" s="T526">adj</ta>
            <ta e="T528" id="Seg_8844" s="T527">v-v:ptcp-v:(case)</ta>
            <ta e="T529" id="Seg_8845" s="T528">conj</ta>
            <ta e="T530" id="Seg_8846" s="T529">n-n:case</ta>
            <ta e="T531" id="Seg_8847" s="T530">n-n:(num).[n:case]</ta>
            <ta e="T532" id="Seg_8848" s="T531">adj-adj&gt;adv</ta>
            <ta e="T533" id="Seg_8849" s="T532">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T534" id="Seg_8850" s="T533">dempro.[pro:case]</ta>
            <ta e="T535" id="Seg_8851" s="T534">ordnum</ta>
            <ta e="T536" id="Seg_8852" s="T535">dempro</ta>
            <ta e="T537" id="Seg_8853" s="T536">n-n:poss-n:case</ta>
            <ta e="T538" id="Seg_8854" s="T537">n.[n:case]</ta>
            <ta e="T539" id="Seg_8855" s="T538">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T540" id="Seg_8856" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_8857" s="T540">n-n:(num)-n:case</ta>
            <ta e="T542" id="Seg_8858" s="T541">n.[n:case]</ta>
            <ta e="T543" id="Seg_8859" s="T542">conj</ta>
            <ta e="T544" id="Seg_8860" s="T543">n-n:(num).[n:case]</ta>
            <ta e="T545" id="Seg_8861" s="T544">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T546" id="Seg_8862" s="T545">n-n:(num).[n:case]</ta>
            <ta e="T547" id="Seg_8863" s="T546">que-pro:case</ta>
            <ta e="T548" id="Seg_8864" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_8865" s="T548">v-v:tense-v:poss.pn</ta>
            <ta e="T550" id="Seg_8866" s="T549">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T551" id="Seg_8867" s="T550">n-n&gt;adj.[n:case]</ta>
            <ta e="T552" id="Seg_8868" s="T551">n-n:(num).[n:case]</ta>
            <ta e="T553" id="Seg_8869" s="T552">adv</ta>
            <ta e="T554" id="Seg_8870" s="T553">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T555" id="Seg_8871" s="T554">n-n:poss-n:case</ta>
            <ta e="T556" id="Seg_8872" s="T555">n-n:poss-n:case</ta>
            <ta e="T557" id="Seg_8873" s="T556">que-pro:case</ta>
            <ta e="T558" id="Seg_8874" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_8875" s="T558">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T560" id="Seg_8876" s="T559">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T561" id="Seg_8877" s="T560">n-n:(poss).[n:case]</ta>
            <ta e="T562" id="Seg_8878" s="T561">n-n:(num).[n:case]</ta>
            <ta e="T563" id="Seg_8879" s="T562">ptcl</ta>
            <ta e="T564" id="Seg_8880" s="T563">v-v:tense-v:pred.pn</ta>
            <ta e="T565" id="Seg_8881" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_8882" s="T565">adv</ta>
            <ta e="T567" id="Seg_8883" s="T566">adv</ta>
            <ta e="T568" id="Seg_8884" s="T567">quant</ta>
            <ta e="T569" id="Seg_8885" s="T568">pers-pro:case</ta>
            <ta e="T570" id="Seg_8886" s="T569">propr-n:case</ta>
            <ta e="T571" id="Seg_8887" s="T570">adv</ta>
            <ta e="T572" id="Seg_8888" s="T571">v-v:cvb</ta>
            <ta e="T573" id="Seg_8889" s="T572">v-v:tense-v:poss.pn</ta>
            <ta e="T574" id="Seg_8890" s="T573">ptcl</ta>
            <ta e="T575" id="Seg_8891" s="T574">cardnum</ta>
            <ta e="T576" id="Seg_8892" s="T575">n.[n:case]</ta>
            <ta e="T577" id="Seg_8893" s="T576">propr</ta>
            <ta e="T578" id="Seg_8894" s="T577">propr</ta>
            <ta e="T579" id="Seg_8895" s="T578">propr.[n:case]</ta>
            <ta e="T580" id="Seg_8896" s="T579">v-v:cvb</ta>
            <ta e="T581" id="Seg_8897" s="T580">n.[n:case]</ta>
            <ta e="T582" id="Seg_8898" s="T581">dempro.[pro:case]</ta>
            <ta e="T583" id="Seg_8899" s="T582">dempro.[pro:case]</ta>
            <ta e="T584" id="Seg_8900" s="T583">n-n:poss-n:case</ta>
            <ta e="T585" id="Seg_8901" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_8902" s="T585">que.[pro:case]</ta>
            <ta e="T587" id="Seg_8903" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_8904" s="T587">adj-n:case</ta>
            <ta e="T589" id="Seg_8905" s="T588">v-v:(neg)-v:pred.pn</ta>
            <ta e="T590" id="Seg_8906" s="T589">adj-adj&gt;adj.[n:case]</ta>
            <ta e="T591" id="Seg_8907" s="T590">n.[n:case]</ta>
            <ta e="T592" id="Seg_8908" s="T591">interj</ta>
            <ta e="T593" id="Seg_8909" s="T592">n-n:(num)-n:case</ta>
            <ta e="T594" id="Seg_8910" s="T593">post</ta>
            <ta e="T595" id="Seg_8911" s="T594">adv</ta>
            <ta e="T596" id="Seg_8912" s="T595">quant</ta>
            <ta e="T597" id="Seg_8913" s="T596">n-n&gt;adj.[n:case]</ta>
            <ta e="T598" id="Seg_8914" s="T597">v-v:tense-v:pred.pn</ta>
            <ta e="T599" id="Seg_8915" s="T598">dempro.[pro:case]</ta>
            <ta e="T600" id="Seg_8916" s="T599">que.[pro:case]</ta>
            <ta e="T601" id="Seg_8917" s="T600">v-v:tense-v:poss.pn</ta>
            <ta e="T604" id="Seg_8918" s="T603">v-v:ptcp</ta>
            <ta e="T605" id="Seg_8919" s="T604">v-v:tense-v:poss.pn</ta>
            <ta e="T606" id="Seg_8920" s="T605">n.[n:case]</ta>
            <ta e="T607" id="Seg_8921" s="T606">adj-n:poss-n:case</ta>
            <ta e="T608" id="Seg_8922" s="T607">ptcl</ta>
            <ta e="T609" id="Seg_8923" s="T608">adv</ta>
            <ta e="T610" id="Seg_8924" s="T609">v-v:tense-v:poss.pn</ta>
            <ta e="T611" id="Seg_8925" s="T610">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T612" id="Seg_8926" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_8927" s="T612">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T614" id="Seg_8928" s="T613">dempro-pro:case</ta>
            <ta e="T615" id="Seg_8929" s="T614">v-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T616" id="Seg_8930" s="T615">adv</ta>
            <ta e="T617" id="Seg_8931" s="T616">n-n&gt;adj.[n:case]</ta>
            <ta e="T618" id="Seg_8932" s="T617">interj</ta>
            <ta e="T619" id="Seg_8933" s="T618">n-n:(num).[n:case]</ta>
            <ta e="T620" id="Seg_8934" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_8935" s="T620">ptcl</ta>
            <ta e="T624" id="Seg_8936" s="T623">v-v&gt;v-v&gt;n-n&gt;n.[n:case]</ta>
            <ta e="T625" id="Seg_8937" s="T624">n-n:(num).[n:case]</ta>
            <ta e="T626" id="Seg_8938" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_8939" s="T626">quant-n:case</ta>
            <ta e="T628" id="Seg_8940" s="T627">v-v:tense-v:poss.pn</ta>
            <ta e="T629" id="Seg_8941" s="T628">ptcl</ta>
            <ta e="T630" id="Seg_8942" s="T629">cardnum</ta>
            <ta e="T633" id="Seg_8943" s="T631">v</ta>
            <ta e="T635" id="Seg_8944" s="T633">v-v:tense-v:poss.pn</ta>
            <ta e="T636" id="Seg_8945" s="T635">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T637" id="Seg_8946" s="T636">ptcl</ta>
            <ta e="T659" id="Seg_8947" s="T657">dempro-pro:case</ta>
            <ta e="T660" id="Seg_8948" s="T659">v-v:tense-v:pred.pn</ta>
            <ta e="T661" id="Seg_8949" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_8950" s="T661">dempro-pro:case</ta>
            <ta e="T663" id="Seg_8951" s="T662">v-v:tense-v:pred.pn</ta>
            <ta e="T664" id="Seg_8952" s="T663">conj</ta>
            <ta e="T665" id="Seg_8953" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_8954" s="T665">dempro.[pro:case]</ta>
            <ta e="T667" id="Seg_8955" s="T666">adj</ta>
            <ta e="T668" id="Seg_8956" s="T667">n.[n:case]</ta>
            <ta e="T669" id="Seg_8957" s="T668">n-n:poss-n:case</ta>
            <ta e="T670" id="Seg_8958" s="T669">v-v:tense-v:poss.pn</ta>
            <ta e="T671" id="Seg_8959" s="T670">pers.[pro:case]</ta>
            <ta e="T672" id="Seg_8960" s="T671">conj</ta>
            <ta e="T673" id="Seg_8961" s="T672">ptcl</ta>
            <ta e="T674" id="Seg_8962" s="T673">ptcl</ta>
            <ta e="T675" id="Seg_8963" s="T674">n-n:case</ta>
            <ta e="T676" id="Seg_8964" s="T675">v-v:ptcp-v:(case)</ta>
            <ta e="T677" id="Seg_8965" s="T676">adv</ta>
            <ta e="T678" id="Seg_8966" s="T677">adj.[n:case]</ta>
            <ta e="T679" id="Seg_8967" s="T678">v-v:tense-v:poss.pn</ta>
            <ta e="T680" id="Seg_8968" s="T679">interj</ta>
            <ta e="T681" id="Seg_8969" s="T680">n-n&gt;adj.[n:case]</ta>
            <ta e="T682" id="Seg_8970" s="T681">n-n:(num).[n:case]</ta>
            <ta e="T683" id="Seg_8971" s="T682">adv</ta>
            <ta e="T684" id="Seg_8972" s="T683">n-n:(num).[n:case]</ta>
            <ta e="T685" id="Seg_8973" s="T684">v-v:(ins)-v:ptcp</ta>
            <ta e="T686" id="Seg_8974" s="T685">v-v:cvb-v:pred.pn</ta>
            <ta e="T687" id="Seg_8975" s="T686">adv</ta>
            <ta e="T688" id="Seg_8976" s="T687">v-v:(neg)-v:pred.pn</ta>
            <ta e="T689" id="Seg_8977" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_8978" s="T689">adj.[n:case]</ta>
            <ta e="T691" id="Seg_8979" s="T690">n-n:poss-n:case</ta>
            <ta e="T692" id="Seg_8980" s="T691">adj.[n:case]</ta>
            <ta e="T693" id="Seg_8981" s="T692">n-n:(poss).[n:case]</ta>
            <ta e="T694" id="Seg_8982" s="T693">interj</ta>
            <ta e="T695" id="Seg_8983" s="T694">n-n:case</ta>
            <ta e="T696" id="Seg_8984" s="T695">n.[n:case]</ta>
            <ta e="T697" id="Seg_8985" s="T696">n-n:poss-n:case</ta>
            <ta e="T698" id="Seg_8986" s="T697">v-v:ptcp-v:(case)</ta>
            <ta e="T699" id="Seg_8987" s="T698">v-v:cvb</ta>
            <ta e="T700" id="Seg_8988" s="T699">ordnum</ta>
            <ta e="T701" id="Seg_8989" s="T700">n.[n:case]</ta>
            <ta e="T702" id="Seg_8990" s="T701">n-n:poss-n:case</ta>
            <ta e="T703" id="Seg_8991" s="T702">n-n:(num).[n:case]</ta>
            <ta e="T704" id="Seg_8992" s="T703">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T705" id="Seg_8993" s="T704">adj-n:poss-n:case</ta>
            <ta e="T706" id="Seg_8994" s="T705">que-pro:case</ta>
            <ta e="T707" id="Seg_8995" s="T706">ptcl</ta>
            <ta e="T708" id="Seg_8996" s="T707">adv</ta>
            <ta e="T709" id="Seg_8997" s="T708">v-v:(neg)-v:pred.pn</ta>
            <ta e="T710" id="Seg_8998" s="T709">ptcl</ta>
            <ta e="T711" id="Seg_8999" s="T710">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T712" id="Seg_9000" s="T711">adj-n:(poss).[n:case]</ta>
            <ta e="T713" id="Seg_9001" s="T712">pers-pro:case</ta>
            <ta e="T714" id="Seg_9002" s="T713">interj</ta>
            <ta e="T715" id="Seg_9003" s="T714">n-n:case</ta>
            <ta e="T716" id="Seg_9004" s="T715">v-v:ptcp</ta>
            <ta e="T717" id="Seg_9005" s="T716">n-n:(num)-n:case</ta>
            <ta e="T718" id="Seg_9006" s="T717">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T719" id="Seg_9007" s="T718">adj-n:(poss).[n:case]</ta>
            <ta e="T720" id="Seg_9008" s="T719">adj-n:(poss).[n:case]</ta>
            <ta e="T721" id="Seg_9009" s="T720">adj.[n:case]</ta>
            <ta e="T722" id="Seg_9010" s="T721">que.[pro:case]</ta>
            <ta e="T723" id="Seg_9011" s="T722">ptcl</ta>
            <ta e="T725" id="Seg_9012" s="T723">n.[n:case]</ta>
            <ta e="T764" id="Seg_9013" s="T763">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-UkOA">
            <ta e="T8" id="Seg_9014" s="T7">interj</ta>
            <ta e="T9" id="Seg_9015" s="T8">emphpro</ta>
            <ta e="T10" id="Seg_9016" s="T9">propr</ta>
            <ta e="T11" id="Seg_9017" s="T10">adj</ta>
            <ta e="T12" id="Seg_9018" s="T11">cop</ta>
            <ta e="T13" id="Seg_9019" s="T12">interj</ta>
            <ta e="T14" id="Seg_9020" s="T13">n</ta>
            <ta e="T15" id="Seg_9021" s="T14">adv</ta>
            <ta e="T16" id="Seg_9022" s="T15">cop</ta>
            <ta e="T17" id="Seg_9023" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_9024" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_9025" s="T18">cardnum</ta>
            <ta e="T20" id="Seg_9026" s="T19">n</ta>
            <ta e="T21" id="Seg_9027" s="T20">n</ta>
            <ta e="T22" id="Seg_9028" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_9029" s="T22">adv</ta>
            <ta e="T24" id="Seg_9030" s="T23">n</ta>
            <ta e="T25" id="Seg_9031" s="T24">v</ta>
            <ta e="T26" id="Seg_9032" s="T25">que</ta>
            <ta e="T27" id="Seg_9033" s="T26">n</ta>
            <ta e="T28" id="Seg_9034" s="T27">adj</ta>
            <ta e="T29" id="Seg_9035" s="T28">n</ta>
            <ta e="T30" id="Seg_9036" s="T29">n</ta>
            <ta e="T31" id="Seg_9037" s="T30">v</ta>
            <ta e="T32" id="Seg_9038" s="T31">dempro</ta>
            <ta e="T33" id="Seg_9039" s="T32">post</ta>
            <ta e="T34" id="Seg_9040" s="T33">n</ta>
            <ta e="T35" id="Seg_9041" s="T34">v</ta>
            <ta e="T36" id="Seg_9042" s="T35">cop</ta>
            <ta e="T38" id="Seg_9043" s="T37">adj</ta>
            <ta e="T39" id="Seg_9044" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_9045" s="T39">dempro</ta>
            <ta e="T41" id="Seg_9046" s="T40">n</ta>
            <ta e="T42" id="Seg_9047" s="T41">n</ta>
            <ta e="T43" id="Seg_9048" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_9049" s="T43">v</ta>
            <ta e="T45" id="Seg_9050" s="T44">adv</ta>
            <ta e="T46" id="Seg_9051" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_9052" s="T46">dempro</ta>
            <ta e="T48" id="Seg_9053" s="T47">emphpro</ta>
            <ta e="T49" id="Seg_9054" s="T48">ptcl</ta>
            <ta e="T51" id="Seg_9055" s="T50">v</ta>
            <ta e="T52" id="Seg_9056" s="T51">v</ta>
            <ta e="T53" id="Seg_9057" s="T52">interj</ta>
            <ta e="T54" id="Seg_9058" s="T53">que</ta>
            <ta e="T55" id="Seg_9059" s="T54">n</ta>
            <ta e="T56" id="Seg_9060" s="T55">v</ta>
            <ta e="T57" id="Seg_9061" s="T56">v</ta>
            <ta e="T60" id="Seg_9062" s="T59">propr</ta>
            <ta e="T61" id="Seg_9063" s="T60">v</ta>
            <ta e="T62" id="Seg_9064" s="T61">n</ta>
            <ta e="T63" id="Seg_9065" s="T62">v</ta>
            <ta e="T64" id="Seg_9066" s="T63">dempro</ta>
            <ta e="T65" id="Seg_9067" s="T64">post</ta>
            <ta e="T85" id="Seg_9068" s="T84">n</ta>
            <ta e="T86" id="Seg_9069" s="T85">emphpro</ta>
            <ta e="T87" id="Seg_9070" s="T86">v</ta>
            <ta e="T88" id="Seg_9071" s="T87">ptcl</ta>
            <ta e="T89" id="Seg_9072" s="T88">que</ta>
            <ta e="T90" id="Seg_9073" s="T89">v</ta>
            <ta e="T91" id="Seg_9074" s="T90">adv</ta>
            <ta e="T92" id="Seg_9075" s="T91">adj</ta>
            <ta e="T93" id="Seg_9076" s="T92">n</ta>
            <ta e="T94" id="Seg_9077" s="T93">v</ta>
            <ta e="T95" id="Seg_9078" s="T94">aux</ta>
            <ta e="T96" id="Seg_9079" s="T95">pers</ta>
            <ta e="T97" id="Seg_9080" s="T96">interj</ta>
            <ta e="T98" id="Seg_9081" s="T97">adv</ta>
            <ta e="T99" id="Seg_9082" s="T98">v</ta>
            <ta e="T100" id="Seg_9083" s="T99">v</ta>
            <ta e="T101" id="Seg_9084" s="T100">aux</ta>
            <ta e="T102" id="Seg_9085" s="T101">adv</ta>
            <ta e="T103" id="Seg_9086" s="T102">adv</ta>
            <ta e="T104" id="Seg_9087" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_9088" s="T104">emphpro</ta>
            <ta e="T106" id="Seg_9089" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_9090" s="T106">que</ta>
            <ta e="T108" id="Seg_9091" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_9092" s="T108">dempro</ta>
            <ta e="T110" id="Seg_9093" s="T109">v</ta>
            <ta e="T111" id="Seg_9094" s="T110">aux</ta>
            <ta e="T112" id="Seg_9095" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_9096" s="T112">cop</ta>
            <ta e="T114" id="Seg_9097" s="T113">que</ta>
            <ta e="T115" id="Seg_9098" s="T114">v</ta>
            <ta e="T116" id="Seg_9099" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_9100" s="T116">ptcl</ta>
            <ta e="T119" id="Seg_9101" s="T117">ptcl</ta>
            <ta e="T124" id="Seg_9102" s="T123">adv</ta>
            <ta e="T125" id="Seg_9103" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_9104" s="T125">v</ta>
            <ta e="T127" id="Seg_9105" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_9106" s="T127">conj</ta>
            <ta e="T129" id="Seg_9107" s="T128">conj</ta>
            <ta e="T130" id="Seg_9108" s="T129">dempro</ta>
            <ta e="T132" id="Seg_9109" s="T131">adv</ta>
            <ta e="T133" id="Seg_9110" s="T132">interj</ta>
            <ta e="T134" id="Seg_9111" s="T133">adv</ta>
            <ta e="T135" id="Seg_9112" s="T134">dempro</ta>
            <ta e="T136" id="Seg_9113" s="T135">adv</ta>
            <ta e="T137" id="Seg_9114" s="T136">v</ta>
            <ta e="T138" id="Seg_9115" s="T137">n</ta>
            <ta e="T139" id="Seg_9116" s="T138">dempro</ta>
            <ta e="T140" id="Seg_9117" s="T139">adv</ta>
            <ta e="T141" id="Seg_9118" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_9119" s="T141">adj</ta>
            <ta e="T143" id="Seg_9120" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_9121" s="T143">n</ta>
            <ta e="T145" id="Seg_9122" s="T144">n</ta>
            <ta e="T146" id="Seg_9123" s="T145">v</ta>
            <ta e="T147" id="Seg_9124" s="T146">aux</ta>
            <ta e="T148" id="Seg_9125" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_9126" s="T148">dempro</ta>
            <ta e="T150" id="Seg_9127" s="T149">n</ta>
            <ta e="T151" id="Seg_9128" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_9129" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_9130" s="T152">adj</ta>
            <ta e="T154" id="Seg_9131" s="T153">n</ta>
            <ta e="T155" id="Seg_9132" s="T154">que</ta>
            <ta e="T157" id="Seg_9133" s="T156">dempro</ta>
            <ta e="T158" id="Seg_9134" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_9135" s="T158">n</ta>
            <ta e="T160" id="Seg_9136" s="T159">n</ta>
            <ta e="T161" id="Seg_9137" s="T160">post</ta>
            <ta e="T162" id="Seg_9138" s="T161">adv</ta>
            <ta e="T163" id="Seg_9139" s="T162">v</ta>
            <ta e="T164" id="Seg_9140" s="T163">cop</ta>
            <ta e="T165" id="Seg_9141" s="T164">n</ta>
            <ta e="T166" id="Seg_9142" s="T165">emphpro</ta>
            <ta e="T167" id="Seg_9143" s="T166">emphpro</ta>
            <ta e="T168" id="Seg_9144" s="T167">n</ta>
            <ta e="T169" id="Seg_9145" s="T168">v</ta>
            <ta e="T170" id="Seg_9146" s="T169">n</ta>
            <ta e="T171" id="Seg_9147" s="T170">dempro</ta>
            <ta e="T172" id="Seg_9148" s="T171">post</ta>
            <ta e="T173" id="Seg_9149" s="T172">adv</ta>
            <ta e="T174" id="Seg_9150" s="T173">adv</ta>
            <ta e="T175" id="Seg_9151" s="T174">dempro</ta>
            <ta e="T176" id="Seg_9152" s="T175">cardnum</ta>
            <ta e="T177" id="Seg_9153" s="T176">cardnum</ta>
            <ta e="T178" id="Seg_9154" s="T177">cardnum</ta>
            <ta e="T179" id="Seg_9155" s="T178">dempro</ta>
            <ta e="T180" id="Seg_9156" s="T179">que</ta>
            <ta e="T181" id="Seg_9157" s="T180">adv</ta>
            <ta e="T182" id="Seg_9158" s="T181">n</ta>
            <ta e="T183" id="Seg_9159" s="T182">v</ta>
            <ta e="T184" id="Seg_9160" s="T183">aux</ta>
            <ta e="T185" id="Seg_9161" s="T184">adv</ta>
            <ta e="T186" id="Seg_9162" s="T185">quant</ta>
            <ta e="T187" id="Seg_9163" s="T186">interj</ta>
            <ta e="T188" id="Seg_9164" s="T187">conj</ta>
            <ta e="T189" id="Seg_9165" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_9166" s="T189">adv</ta>
            <ta e="T191" id="Seg_9167" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_9168" s="T191">que</ta>
            <ta e="T193" id="Seg_9169" s="T192">adv</ta>
            <ta e="T194" id="Seg_9170" s="T193">ptcl</ta>
            <ta e="T196" id="Seg_9171" s="T195">n</ta>
            <ta e="T197" id="Seg_9172" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_9173" s="T197">n</ta>
            <ta e="T199" id="Seg_9174" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_9175" s="T199">que</ta>
            <ta e="T201" id="Seg_9176" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_9177" s="T201">dempro</ta>
            <ta e="T203" id="Seg_9178" s="T202">n</ta>
            <ta e="T204" id="Seg_9179" s="T203">v</ta>
            <ta e="T205" id="Seg_9180" s="T204">aux</ta>
            <ta e="T206" id="Seg_9181" s="T205">emphpro</ta>
            <ta e="T207" id="Seg_9182" s="T206">v</ta>
            <ta e="T208" id="Seg_9183" s="T207">n</ta>
            <ta e="T209" id="Seg_9184" s="T208">v</ta>
            <ta e="T210" id="Seg_9185" s="T209">que</ta>
            <ta e="T211" id="Seg_9186" s="T210">que</ta>
            <ta e="T212" id="Seg_9187" s="T211">adj</ta>
            <ta e="T213" id="Seg_9188" s="T212">n</ta>
            <ta e="T214" id="Seg_9189" s="T213">v</ta>
            <ta e="T215" id="Seg_9190" s="T214">v</ta>
            <ta e="T216" id="Seg_9191" s="T215">adj</ta>
            <ta e="T217" id="Seg_9192" s="T216">n</ta>
            <ta e="T218" id="Seg_9193" s="T217">adj</ta>
            <ta e="T219" id="Seg_9194" s="T218">n</ta>
            <ta e="T220" id="Seg_9195" s="T219">v</ta>
            <ta e="T221" id="Seg_9196" s="T220">n</ta>
            <ta e="T222" id="Seg_9197" s="T221">n</ta>
            <ta e="T223" id="Seg_9198" s="T222">v</ta>
            <ta e="T224" id="Seg_9199" s="T223">n</ta>
            <ta e="T225" id="Seg_9200" s="T224">v</ta>
            <ta e="T226" id="Seg_9201" s="T225">v</ta>
            <ta e="T227" id="Seg_9202" s="T226">v</ta>
            <ta e="T228" id="Seg_9203" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_9204" s="T228">v</ta>
            <ta e="T230" id="Seg_9205" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_9206" s="T230">dempro</ta>
            <ta e="T232" id="Seg_9207" s="T231">adv</ta>
            <ta e="T233" id="Seg_9208" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_9209" s="T233">n</ta>
            <ta e="T235" id="Seg_9210" s="T234">v</ta>
            <ta e="T236" id="Seg_9211" s="T235">n</ta>
            <ta e="T237" id="Seg_9212" s="T236">adv</ta>
            <ta e="T238" id="Seg_9213" s="T237">n</ta>
            <ta e="T239" id="Seg_9214" s="T238">que</ta>
            <ta e="T240" id="Seg_9215" s="T239">v</ta>
            <ta e="T242" id="Seg_9216" s="T241">n</ta>
            <ta e="T243" id="Seg_9217" s="T242">v</ta>
            <ta e="T244" id="Seg_9218" s="T243">adv</ta>
            <ta e="T245" id="Seg_9219" s="T244">adj</ta>
            <ta e="T246" id="Seg_9220" s="T245">dempro</ta>
            <ta e="T247" id="Seg_9221" s="T246">que</ta>
            <ta e="T248" id="Seg_9222" s="T247">n</ta>
            <ta e="T249" id="Seg_9223" s="T248">v</ta>
            <ta e="T250" id="Seg_9224" s="T249">aux</ta>
            <ta e="T251" id="Seg_9225" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_9226" s="T251">interj</ta>
            <ta e="T253" id="Seg_9227" s="T252">n</ta>
            <ta e="T254" id="Seg_9228" s="T253">n</ta>
            <ta e="T255" id="Seg_9229" s="T254">v</ta>
            <ta e="T256" id="Seg_9230" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_9231" s="T256">n</ta>
            <ta e="T258" id="Seg_9232" s="T257">v</ta>
            <ta e="T259" id="Seg_9233" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_9234" s="T259">n</ta>
            <ta e="T261" id="Seg_9235" s="T260">v</ta>
            <ta e="T262" id="Seg_9236" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_9237" s="T262">n</ta>
            <ta e="T264" id="Seg_9238" s="T263">v</ta>
            <ta e="T265" id="Seg_9239" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_9240" s="T265">n</ta>
            <ta e="T267" id="Seg_9241" s="T266">adv</ta>
            <ta e="T268" id="Seg_9242" s="T267">v</ta>
            <ta e="T269" id="Seg_9243" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_9244" s="T269">que</ta>
            <ta e="T271" id="Seg_9245" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_9246" s="T271">adv</ta>
            <ta e="T273" id="Seg_9247" s="T272">cop</ta>
            <ta e="T274" id="Seg_9248" s="T273">adv</ta>
            <ta e="T275" id="Seg_9249" s="T274">n</ta>
            <ta e="T276" id="Seg_9250" s="T275">que</ta>
            <ta e="T277" id="Seg_9251" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_9252" s="T277">v</ta>
            <ta e="T279" id="Seg_9253" s="T278">aux</ta>
            <ta e="T280" id="Seg_9254" s="T279">pers</ta>
            <ta e="T281" id="Seg_9255" s="T280">que</ta>
            <ta e="T282" id="Seg_9256" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_9257" s="T282">n</ta>
            <ta e="T284" id="Seg_9258" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_9259" s="T284">n</ta>
            <ta e="T286" id="Seg_9260" s="T285">v</ta>
            <ta e="T287" id="Seg_9261" s="T286">aux</ta>
            <ta e="T288" id="Seg_9262" s="T287">adv</ta>
            <ta e="T289" id="Seg_9263" s="T288">dempro</ta>
            <ta e="T290" id="Seg_9264" s="T289">dempro</ta>
            <ta e="T291" id="Seg_9265" s="T290">que</ta>
            <ta e="T292" id="Seg_9266" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_9267" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_9268" s="T293">adj</ta>
            <ta e="T295" id="Seg_9269" s="T294">n</ta>
            <ta e="T296" id="Seg_9270" s="T295">emphpro</ta>
            <ta e="T297" id="Seg_9271" s="T296">n</ta>
            <ta e="T298" id="Seg_9272" s="T297">v</ta>
            <ta e="T299" id="Seg_9273" s="T298">que</ta>
            <ta e="T300" id="Seg_9274" s="T299">n</ta>
            <ta e="T301" id="Seg_9275" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_9276" s="T301">n</ta>
            <ta e="T303" id="Seg_9277" s="T302">adv</ta>
            <ta e="T304" id="Seg_9278" s="T303">v</ta>
            <ta e="T305" id="Seg_9279" s="T304">v</ta>
            <ta e="T306" id="Seg_9280" s="T305">v</ta>
            <ta e="T307" id="Seg_9281" s="T306">dempro</ta>
            <ta e="T308" id="Seg_9282" s="T307">v</ta>
            <ta e="T309" id="Seg_9283" s="T308">conj</ta>
            <ta e="T310" id="Seg_9284" s="T309">que</ta>
            <ta e="T311" id="Seg_9285" s="T310">n</ta>
            <ta e="T312" id="Seg_9286" s="T311">quant</ta>
            <ta e="T313" id="Seg_9287" s="T312">v</ta>
            <ta e="T314" id="Seg_9288" s="T313">n</ta>
            <ta e="T315" id="Seg_9289" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_9290" s="T315">cardnum</ta>
            <ta e="T321" id="Seg_9291" s="T319">v</ta>
            <ta e="T336" id="Seg_9292" s="T335">interj</ta>
            <ta e="T337" id="Seg_9293" s="T336">n</ta>
            <ta e="T338" id="Seg_9294" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_9295" s="T338">n</ta>
            <ta e="T340" id="Seg_9296" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_9297" s="T340">v</ta>
            <ta e="T342" id="Seg_9298" s="T341">pers</ta>
            <ta e="T343" id="Seg_9299" s="T342">n</ta>
            <ta e="T344" id="Seg_9300" s="T343">n</ta>
            <ta e="T345" id="Seg_9301" s="T344">v</ta>
            <ta e="T346" id="Seg_9302" s="T345">adv</ta>
            <ta e="T347" id="Seg_9303" s="T346">pers</ta>
            <ta e="T348" id="Seg_9304" s="T347">ptcl</ta>
            <ta e="T350" id="Seg_9305" s="T348">v</ta>
            <ta e="T362" id="Seg_9306" s="T360">emphpro</ta>
            <ta e="T365" id="Seg_9307" s="T362">emphpro</ta>
            <ta e="T368" id="Seg_9308" s="T365">adv</ta>
            <ta e="T369" id="Seg_9309" s="T368">n</ta>
            <ta e="T370" id="Seg_9310" s="T369">adv</ta>
            <ta e="T371" id="Seg_9311" s="T370">n</ta>
            <ta e="T372" id="Seg_9312" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_9313" s="T372">adv</ta>
            <ta e="T374" id="Seg_9314" s="T373">quant</ta>
            <ta e="T375" id="Seg_9315" s="T374">n</ta>
            <ta e="T376" id="Seg_9316" s="T375">interj</ta>
            <ta e="T377" id="Seg_9317" s="T376">v</ta>
            <ta e="T378" id="Seg_9318" s="T377">n</ta>
            <ta e="T379" id="Seg_9319" s="T378">n</ta>
            <ta e="T380" id="Seg_9320" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_9321" s="T380">cop</ta>
            <ta e="T382" id="Seg_9322" s="T381">adj</ta>
            <ta e="T383" id="Seg_9323" s="T382">adj</ta>
            <ta e="T384" id="Seg_9324" s="T383">adj</ta>
            <ta e="T385" id="Seg_9325" s="T384">n</ta>
            <ta e="T386" id="Seg_9326" s="T385">n</ta>
            <ta e="T387" id="Seg_9327" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_9328" s="T387">interj</ta>
            <ta e="T389" id="Seg_9329" s="T388">dempro</ta>
            <ta e="T390" id="Seg_9330" s="T389">adj</ta>
            <ta e="T391" id="Seg_9331" s="T390">dempro</ta>
            <ta e="T392" id="Seg_9332" s="T391">adj</ta>
            <ta e="T393" id="Seg_9333" s="T392">dempro</ta>
            <ta e="T394" id="Seg_9334" s="T393">adj</ta>
            <ta e="T395" id="Seg_9335" s="T394">v</ta>
            <ta e="T396" id="Seg_9336" s="T395">n</ta>
            <ta e="T397" id="Seg_9337" s="T396">adj</ta>
            <ta e="T398" id="Seg_9338" s="T397">interj</ta>
            <ta e="T399" id="Seg_9339" s="T398">ptcl</ta>
            <ta e="T400" id="Seg_9340" s="T399">n</ta>
            <ta e="T401" id="Seg_9341" s="T400">v</ta>
            <ta e="T403" id="Seg_9342" s="T402">n</ta>
            <ta e="T404" id="Seg_9343" s="T403">v</ta>
            <ta e="T405" id="Seg_9344" s="T404">n</ta>
            <ta e="T406" id="Seg_9345" s="T405">adj</ta>
            <ta e="T407" id="Seg_9346" s="T406">adv</ta>
            <ta e="T408" id="Seg_9347" s="T407">v</ta>
            <ta e="T409" id="Seg_9348" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_9349" s="T409">n</ta>
            <ta e="T411" id="Seg_9350" s="T410">adv</ta>
            <ta e="T412" id="Seg_9351" s="T411">n</ta>
            <ta e="T413" id="Seg_9352" s="T412">v</ta>
            <ta e="T414" id="Seg_9353" s="T413">adj</ta>
            <ta e="T415" id="Seg_9354" s="T414">n</ta>
            <ta e="T416" id="Seg_9355" s="T415">adv</ta>
            <ta e="T417" id="Seg_9356" s="T416">v</ta>
            <ta e="T418" id="Seg_9357" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_9358" s="T418">v</ta>
            <ta e="T420" id="Seg_9359" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_9360" s="T420">adv</ta>
            <ta e="T422" id="Seg_9361" s="T421">adj</ta>
            <ta e="T423" id="Seg_9362" s="T422">n</ta>
            <ta e="T424" id="Seg_9363" s="T423">adv</ta>
            <ta e="T425" id="Seg_9364" s="T424">n</ta>
            <ta e="T426" id="Seg_9365" s="T425">adv</ta>
            <ta e="T427" id="Seg_9366" s="T426">n</ta>
            <ta e="T428" id="Seg_9367" s="T427">v</ta>
            <ta e="T429" id="Seg_9368" s="T428">pers</ta>
            <ta e="T430" id="Seg_9369" s="T429">quant</ta>
            <ta e="T431" id="Seg_9370" s="T430">v</ta>
            <ta e="T432" id="Seg_9371" s="T431">aux</ta>
            <ta e="T433" id="Seg_9372" s="T432">n</ta>
            <ta e="T434" id="Seg_9373" s="T433">v</ta>
            <ta e="T435" id="Seg_9374" s="T434">dempro</ta>
            <ta e="T436" id="Seg_9375" s="T435">post</ta>
            <ta e="T437" id="Seg_9376" s="T436">v</ta>
            <ta e="T438" id="Seg_9377" s="T437">n</ta>
            <ta e="T439" id="Seg_9378" s="T438">v</ta>
            <ta e="T440" id="Seg_9379" s="T439">n</ta>
            <ta e="T441" id="Seg_9380" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_9381" s="T441">que</ta>
            <ta e="T443" id="Seg_9382" s="T442">n</ta>
            <ta e="T444" id="Seg_9383" s="T443">ptcl</ta>
            <ta e="T445" id="Seg_9384" s="T444">n</ta>
            <ta e="T446" id="Seg_9385" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_9386" s="T446">n</ta>
            <ta e="T449" id="Seg_9387" s="T447">interj</ta>
            <ta e="T453" id="Seg_9388" s="T451">adj</ta>
            <ta e="T454" id="Seg_9389" s="T453">adj</ta>
            <ta e="T456" id="Seg_9390" s="T454">n</ta>
            <ta e="T460" id="Seg_9391" s="T458">adj</ta>
            <ta e="T461" id="Seg_9392" s="T460">n</ta>
            <ta e="T462" id="Seg_9393" s="T461">adj</ta>
            <ta e="T463" id="Seg_9394" s="T462">n</ta>
            <ta e="T464" id="Seg_9395" s="T463">v</ta>
            <ta e="T465" id="Seg_9396" s="T464">adj</ta>
            <ta e="T466" id="Seg_9397" s="T465">adv</ta>
            <ta e="T467" id="Seg_9398" s="T466">adj</ta>
            <ta e="T468" id="Seg_9399" s="T467">cop</ta>
            <ta e="T469" id="Seg_9400" s="T468">propr</ta>
            <ta e="T470" id="Seg_9401" s="T469">v</ta>
            <ta e="T471" id="Seg_9402" s="T470">interj</ta>
            <ta e="T472" id="Seg_9403" s="T471">adv</ta>
            <ta e="T475" id="Seg_9404" s="T474">propr</ta>
            <ta e="T476" id="Seg_9405" s="T475">n</ta>
            <ta e="T477" id="Seg_9406" s="T476">v</ta>
            <ta e="T478" id="Seg_9407" s="T477">adv</ta>
            <ta e="T479" id="Seg_9408" s="T478">propr</ta>
            <ta e="T480" id="Seg_9409" s="T479">adv</ta>
            <ta e="T481" id="Seg_9410" s="T480">ptcl</ta>
            <ta e="T482" id="Seg_9411" s="T481">adj</ta>
            <ta e="T483" id="Seg_9412" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_9413" s="T483">adj</ta>
            <ta e="T485" id="Seg_9414" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_9415" s="T485">dempro</ta>
            <ta e="T487" id="Seg_9416" s="T486">post</ta>
            <ta e="T488" id="Seg_9417" s="T487">n</ta>
            <ta e="T489" id="Seg_9418" s="T488">adj</ta>
            <ta e="T490" id="Seg_9419" s="T489">n</ta>
            <ta e="T491" id="Seg_9420" s="T490">n</ta>
            <ta e="T492" id="Seg_9421" s="T491">v</ta>
            <ta e="T493" id="Seg_9422" s="T492">adv</ta>
            <ta e="T494" id="Seg_9423" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_9424" s="T494">v</ta>
            <ta e="T496" id="Seg_9425" s="T495">n</ta>
            <ta e="T497" id="Seg_9426" s="T496">adv</ta>
            <ta e="T498" id="Seg_9427" s="T497">interj</ta>
            <ta e="T499" id="Seg_9428" s="T498">n</ta>
            <ta e="T500" id="Seg_9429" s="T499">que</ta>
            <ta e="T501" id="Seg_9430" s="T500">v</ta>
            <ta e="T502" id="Seg_9431" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_9432" s="T502">adv</ta>
            <ta e="T504" id="Seg_9433" s="T503">v</ta>
            <ta e="T505" id="Seg_9434" s="T504">conj</ta>
            <ta e="T506" id="Seg_9435" s="T505">dempro</ta>
            <ta e="T507" id="Seg_9436" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_9437" s="T507">quant</ta>
            <ta e="T509" id="Seg_9438" s="T508">cardnum</ta>
            <ta e="T510" id="Seg_9439" s="T509">n</ta>
            <ta e="T511" id="Seg_9440" s="T510">ptcl</ta>
            <ta e="T512" id="Seg_9441" s="T511">v</ta>
            <ta e="T513" id="Seg_9442" s="T512">adv</ta>
            <ta e="T514" id="Seg_9443" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_9444" s="T514">quant</ta>
            <ta e="T516" id="Seg_9445" s="T515">n</ta>
            <ta e="T517" id="Seg_9446" s="T516">v</ta>
            <ta e="T518" id="Seg_9447" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_9448" s="T518">dempro</ta>
            <ta e="T520" id="Seg_9449" s="T519">n</ta>
            <ta e="T521" id="Seg_9450" s="T520">conj</ta>
            <ta e="T522" id="Seg_9451" s="T521">n</ta>
            <ta e="T523" id="Seg_9452" s="T522">v</ta>
            <ta e="T524" id="Seg_9453" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_9454" s="T524">dempro</ta>
            <ta e="T526" id="Seg_9455" s="T525">post</ta>
            <ta e="T527" id="Seg_9456" s="T526">adv</ta>
            <ta e="T528" id="Seg_9457" s="T527">v</ta>
            <ta e="T529" id="Seg_9458" s="T528">conj</ta>
            <ta e="T530" id="Seg_9459" s="T529">n</ta>
            <ta e="T531" id="Seg_9460" s="T530">n</ta>
            <ta e="T532" id="Seg_9461" s="T531">adv</ta>
            <ta e="T533" id="Seg_9462" s="T532">v</ta>
            <ta e="T534" id="Seg_9463" s="T533">dempro</ta>
            <ta e="T535" id="Seg_9464" s="T534">ordnum</ta>
            <ta e="T536" id="Seg_9465" s="T535">dempro</ta>
            <ta e="T537" id="Seg_9466" s="T536">n</ta>
            <ta e="T538" id="Seg_9467" s="T537">n</ta>
            <ta e="T539" id="Seg_9468" s="T538">v</ta>
            <ta e="T540" id="Seg_9469" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_9470" s="T540">n</ta>
            <ta e="T542" id="Seg_9471" s="T541">n</ta>
            <ta e="T543" id="Seg_9472" s="T542">conj</ta>
            <ta e="T544" id="Seg_9473" s="T543">n</ta>
            <ta e="T545" id="Seg_9474" s="T544">emphpro</ta>
            <ta e="T546" id="Seg_9475" s="T545">n</ta>
            <ta e="T547" id="Seg_9476" s="T546">que</ta>
            <ta e="T548" id="Seg_9477" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_9478" s="T548">v</ta>
            <ta e="T550" id="Seg_9479" s="T549">ptcl</ta>
            <ta e="T551" id="Seg_9480" s="T550">adj</ta>
            <ta e="T552" id="Seg_9481" s="T551">n</ta>
            <ta e="T553" id="Seg_9482" s="T552">adv</ta>
            <ta e="T554" id="Seg_9483" s="T553">emphpro</ta>
            <ta e="T555" id="Seg_9484" s="T554">n</ta>
            <ta e="T556" id="Seg_9485" s="T555">n</ta>
            <ta e="T557" id="Seg_9486" s="T556">que</ta>
            <ta e="T558" id="Seg_9487" s="T557">ptcl</ta>
            <ta e="T559" id="Seg_9488" s="T558">v</ta>
            <ta e="T560" id="Seg_9489" s="T559">n</ta>
            <ta e="T561" id="Seg_9490" s="T560">n</ta>
            <ta e="T562" id="Seg_9491" s="T561">n</ta>
            <ta e="T563" id="Seg_9492" s="T562">ptcl</ta>
            <ta e="T564" id="Seg_9493" s="T563">v</ta>
            <ta e="T565" id="Seg_9494" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_9495" s="T565">adv</ta>
            <ta e="T567" id="Seg_9496" s="T566">adv</ta>
            <ta e="T568" id="Seg_9497" s="T567">quant</ta>
            <ta e="T569" id="Seg_9498" s="T568">pers</ta>
            <ta e="T570" id="Seg_9499" s="T569">propr</ta>
            <ta e="T571" id="Seg_9500" s="T570">adv</ta>
            <ta e="T572" id="Seg_9501" s="T571">v</ta>
            <ta e="T573" id="Seg_9502" s="T572">aux</ta>
            <ta e="T574" id="Seg_9503" s="T573">ptcl</ta>
            <ta e="T575" id="Seg_9504" s="T574">cardnum</ta>
            <ta e="T576" id="Seg_9505" s="T575">n</ta>
            <ta e="T577" id="Seg_9506" s="T576">propr</ta>
            <ta e="T578" id="Seg_9507" s="T577">propr</ta>
            <ta e="T579" id="Seg_9508" s="T578">propr</ta>
            <ta e="T580" id="Seg_9509" s="T579">v</ta>
            <ta e="T581" id="Seg_9510" s="T580">n</ta>
            <ta e="T582" id="Seg_9511" s="T581">dempro</ta>
            <ta e="T583" id="Seg_9512" s="T582">dempro</ta>
            <ta e="T584" id="Seg_9513" s="T583">n</ta>
            <ta e="T585" id="Seg_9514" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_9515" s="T585">que</ta>
            <ta e="T587" id="Seg_9516" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_9517" s="T587">n</ta>
            <ta e="T589" id="Seg_9518" s="T588">v</ta>
            <ta e="T590" id="Seg_9519" s="T589">adj</ta>
            <ta e="T591" id="Seg_9520" s="T590">n</ta>
            <ta e="T592" id="Seg_9521" s="T591">interj</ta>
            <ta e="T593" id="Seg_9522" s="T592">n</ta>
            <ta e="T594" id="Seg_9523" s="T593">post</ta>
            <ta e="T595" id="Seg_9524" s="T594">adv</ta>
            <ta e="T596" id="Seg_9525" s="T595">quant</ta>
            <ta e="T597" id="Seg_9526" s="T596">adj</ta>
            <ta e="T598" id="Seg_9527" s="T597">cop</ta>
            <ta e="T599" id="Seg_9528" s="T598">dempro</ta>
            <ta e="T600" id="Seg_9529" s="T599">que</ta>
            <ta e="T601" id="Seg_9530" s="T600">cop</ta>
            <ta e="T604" id="Seg_9531" s="T603">v</ta>
            <ta e="T605" id="Seg_9532" s="T604">aux</ta>
            <ta e="T606" id="Seg_9533" s="T605">n</ta>
            <ta e="T607" id="Seg_9534" s="T606">adj</ta>
            <ta e="T608" id="Seg_9535" s="T607">ptcl</ta>
            <ta e="T609" id="Seg_9536" s="T608">adv</ta>
            <ta e="T610" id="Seg_9537" s="T609">v</ta>
            <ta e="T611" id="Seg_9538" s="T610">dempro</ta>
            <ta e="T612" id="Seg_9539" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_9540" s="T612">v</ta>
            <ta e="T614" id="Seg_9541" s="T613">dempro</ta>
            <ta e="T615" id="Seg_9542" s="T614">v</ta>
            <ta e="T616" id="Seg_9543" s="T615">adv</ta>
            <ta e="T617" id="Seg_9544" s="T616">adj</ta>
            <ta e="T618" id="Seg_9545" s="T617">interj</ta>
            <ta e="T619" id="Seg_9546" s="T618">n</ta>
            <ta e="T620" id="Seg_9547" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_9548" s="T620">ptcl</ta>
            <ta e="T624" id="Seg_9549" s="T623">n</ta>
            <ta e="T625" id="Seg_9550" s="T624">n</ta>
            <ta e="T626" id="Seg_9551" s="T625">ptcl</ta>
            <ta e="T627" id="Seg_9552" s="T626">quant</ta>
            <ta e="T628" id="Seg_9553" s="T627">v</ta>
            <ta e="T629" id="Seg_9554" s="T628">ptcl</ta>
            <ta e="T630" id="Seg_9555" s="T629">cardnum</ta>
            <ta e="T633" id="Seg_9556" s="T631">v</ta>
            <ta e="T635" id="Seg_9557" s="T633">v</ta>
            <ta e="T636" id="Seg_9558" s="T635">ptcl</ta>
            <ta e="T637" id="Seg_9559" s="T636">ptcl</ta>
            <ta e="T659" id="Seg_9560" s="T657">dempro</ta>
            <ta e="T660" id="Seg_9561" s="T659">v</ta>
            <ta e="T661" id="Seg_9562" s="T660">ptcl</ta>
            <ta e="T662" id="Seg_9563" s="T661">dempro</ta>
            <ta e="T663" id="Seg_9564" s="T662">v</ta>
            <ta e="T664" id="Seg_9565" s="T663">conj</ta>
            <ta e="T665" id="Seg_9566" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_9567" s="T665">dempro</ta>
            <ta e="T667" id="Seg_9568" s="T666">adj</ta>
            <ta e="T668" id="Seg_9569" s="T667">n</ta>
            <ta e="T669" id="Seg_9570" s="T668">n</ta>
            <ta e="T670" id="Seg_9571" s="T669">v</ta>
            <ta e="T671" id="Seg_9572" s="T670">pers</ta>
            <ta e="T672" id="Seg_9573" s="T671">conj</ta>
            <ta e="T673" id="Seg_9574" s="T672">ptcl</ta>
            <ta e="T674" id="Seg_9575" s="T673">ptcl</ta>
            <ta e="T675" id="Seg_9576" s="T674">n</ta>
            <ta e="T676" id="Seg_9577" s="T675">v</ta>
            <ta e="T677" id="Seg_9578" s="T676">adv</ta>
            <ta e="T678" id="Seg_9579" s="T677">adj</ta>
            <ta e="T679" id="Seg_9580" s="T678">cop</ta>
            <ta e="T680" id="Seg_9581" s="T679">interj</ta>
            <ta e="T681" id="Seg_9582" s="T680">adj</ta>
            <ta e="T682" id="Seg_9583" s="T681">n</ta>
            <ta e="T683" id="Seg_9584" s="T682">adv</ta>
            <ta e="T684" id="Seg_9585" s="T683">n</ta>
            <ta e="T685" id="Seg_9586" s="T684">v</ta>
            <ta e="T686" id="Seg_9587" s="T685">aux</ta>
            <ta e="T687" id="Seg_9588" s="T686">adv</ta>
            <ta e="T688" id="Seg_9589" s="T687">v</ta>
            <ta e="T689" id="Seg_9590" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_9591" s="T689">adj</ta>
            <ta e="T691" id="Seg_9592" s="T690">n</ta>
            <ta e="T692" id="Seg_9593" s="T691">adj</ta>
            <ta e="T693" id="Seg_9594" s="T692">n</ta>
            <ta e="T694" id="Seg_9595" s="T693">interj</ta>
            <ta e="T695" id="Seg_9596" s="T694">n</ta>
            <ta e="T696" id="Seg_9597" s="T695">n</ta>
            <ta e="T697" id="Seg_9598" s="T696">n</ta>
            <ta e="T698" id="Seg_9599" s="T697">v</ta>
            <ta e="T699" id="Seg_9600" s="T698">v</ta>
            <ta e="T700" id="Seg_9601" s="T699">ordnum</ta>
            <ta e="T701" id="Seg_9602" s="T700">n</ta>
            <ta e="T702" id="Seg_9603" s="T701">n</ta>
            <ta e="T703" id="Seg_9604" s="T702">n</ta>
            <ta e="T704" id="Seg_9605" s="T703">n</ta>
            <ta e="T705" id="Seg_9606" s="T704">adj</ta>
            <ta e="T706" id="Seg_9607" s="T705">que</ta>
            <ta e="T707" id="Seg_9608" s="T706">ptcl</ta>
            <ta e="T708" id="Seg_9609" s="T707">adv</ta>
            <ta e="T709" id="Seg_9610" s="T708">v</ta>
            <ta e="T710" id="Seg_9611" s="T709">ptcl</ta>
            <ta e="T711" id="Seg_9612" s="T710">dempro</ta>
            <ta e="T712" id="Seg_9613" s="T711">adj</ta>
            <ta e="T713" id="Seg_9614" s="T712">pers</ta>
            <ta e="T714" id="Seg_9615" s="T713">interj</ta>
            <ta e="T715" id="Seg_9616" s="T714">n</ta>
            <ta e="T716" id="Seg_9617" s="T715">v</ta>
            <ta e="T717" id="Seg_9618" s="T716">n</ta>
            <ta e="T718" id="Seg_9619" s="T717">dempro</ta>
            <ta e="T719" id="Seg_9620" s="T718">adj</ta>
            <ta e="T720" id="Seg_9621" s="T719">adj</ta>
            <ta e="T721" id="Seg_9622" s="T720">adj</ta>
            <ta e="T722" id="Seg_9623" s="T721">que</ta>
            <ta e="T723" id="Seg_9624" s="T722">ptcl</ta>
            <ta e="T725" id="Seg_9625" s="T723">n</ta>
            <ta e="T764" id="Seg_9626" s="T763">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-UkOA" />
         <annotation name="SyF" tierref="SyF-UkOA" />
         <annotation name="IST" tierref="IST-UkOA" />
         <annotation name="Top" tierref="Top-UkOA" />
         <annotation name="Foc" tierref="Foc-UkOA" />
         <annotation name="BOR" tierref="BOR-UkOA">
            <ta e="T23" id="Seg_9627" s="T22">RUS:core</ta>
            <ta e="T55" id="Seg_9628" s="T54">RUS:cult</ta>
            <ta e="T128" id="Seg_9629" s="T127">RUS:gram</ta>
            <ta e="T129" id="Seg_9630" s="T128">RUS:gram</ta>
            <ta e="T144" id="Seg_9631" s="T143">RUS:cult</ta>
            <ta e="T145" id="Seg_9632" s="T144">RUS:cult</ta>
            <ta e="T150" id="Seg_9633" s="T149">RUS:cult</ta>
            <ta e="T153" id="Seg_9634" s="T152">RUS:cult</ta>
            <ta e="T159" id="Seg_9635" s="T158">RUS:core</ta>
            <ta e="T160" id="Seg_9636" s="T159">RUS:cult</ta>
            <ta e="T174" id="Seg_9637" s="T173">RUS:mod</ta>
            <ta e="T176" id="Seg_9638" s="T175">RUS:cult</ta>
            <ta e="T182" id="Seg_9639" s="T181">RUS:cult</ta>
            <ta e="T185" id="Seg_9640" s="T184">RUS:mod</ta>
            <ta e="T188" id="Seg_9641" s="T187">RUS:gram</ta>
            <ta e="T189" id="Seg_9642" s="T188">RUS:disc</ta>
            <ta e="T190" id="Seg_9643" s="T189">RUS:mod</ta>
            <ta e="T198" id="Seg_9644" s="T197">RUS:cult</ta>
            <ta e="T203" id="Seg_9645" s="T202">RUS:cult</ta>
            <ta e="T238" id="Seg_9646" s="T237">RUS:cult</ta>
            <ta e="T242" id="Seg_9647" s="T241">RUS:cult</ta>
            <ta e="T260" id="Seg_9648" s="T259">RUS:cult</ta>
            <ta e="T267" id="Seg_9649" s="T266">RUS:mod</ta>
            <ta e="T269" id="Seg_9650" s="T268">RUS:mod</ta>
            <ta e="T275" id="Seg_9651" s="T274">RUS:cult</ta>
            <ta e="T283" id="Seg_9652" s="T282">RUS:cult</ta>
            <ta e="T285" id="Seg_9653" s="T284">RUS:cult</ta>
            <ta e="T286" id="Seg_9654" s="T285">RUS:cult</ta>
            <ta e="T303" id="Seg_9655" s="T302">RUS:mod</ta>
            <ta e="T309" id="Seg_9656" s="T308">RUS:gram</ta>
            <ta e="T373" id="Seg_9657" s="T372">RUS:mod</ta>
            <ta e="T384" id="Seg_9658" s="T383">RUS:cult</ta>
            <ta e="T399" id="Seg_9659" s="T398">RUS:disc</ta>
            <ta e="T442" id="Seg_9660" s="T441">RUS:mod</ta>
            <ta e="T444" id="Seg_9661" s="T443">RUS:disc</ta>
            <ta e="T469" id="Seg_9662" s="T468">RUS:cult</ta>
            <ta e="T472" id="Seg_9663" s="T471">RUS:mod</ta>
            <ta e="T475" id="Seg_9664" s="T474">RUS:cult</ta>
            <ta e="T479" id="Seg_9665" s="T478">RUS:cult</ta>
            <ta e="T482" id="Seg_9666" s="T481">RUS:cult</ta>
            <ta e="T505" id="Seg_9667" s="T504">RUS:gram</ta>
            <ta e="T521" id="Seg_9668" s="T520">RUS:gram</ta>
            <ta e="T529" id="Seg_9669" s="T528">RUS:gram</ta>
            <ta e="T538" id="Seg_9670" s="T537">RUS:cult</ta>
            <ta e="T539" id="Seg_9671" s="T538">RUS:core</ta>
            <ta e="T540" id="Seg_9672" s="T539">RUS:mod</ta>
            <ta e="T543" id="Seg_9673" s="T542">RUS:gram</ta>
            <ta e="T553" id="Seg_9674" s="T552">RUS:mod</ta>
            <ta e="T560" id="Seg_9675" s="T559">RUS:cult</ta>
            <ta e="T561" id="Seg_9676" s="T560">RUS:cult</ta>
            <ta e="T562" id="Seg_9677" s="T561">RUS:cult</ta>
            <ta e="T567" id="Seg_9678" s="T566">RUS:mod</ta>
            <ta e="T570" id="Seg_9679" s="T569">RUS:cult</ta>
            <ta e="T571" id="Seg_9680" s="T570">RUS:mod</ta>
            <ta e="T576" id="Seg_9681" s="T575">RUS:cult</ta>
            <ta e="T577" id="Seg_9682" s="T576">RUS:cult</ta>
            <ta e="T578" id="Seg_9683" s="T577">RUS:cult</ta>
            <ta e="T579" id="Seg_9684" s="T578">RUS:cult</ta>
            <ta e="T606" id="Seg_9685" s="T605">RUS:cult</ta>
            <ta e="T613" id="Seg_9686" s="T612">RUS:cult</ta>
            <ta e="T615" id="Seg_9687" s="T614">RUS:cult</ta>
            <ta e="T664" id="Seg_9688" s="T663">RUS:gram</ta>
            <ta e="T665" id="Seg_9689" s="T664">RUS:disc</ta>
            <ta e="T672" id="Seg_9690" s="T671">RUS:gram</ta>
            <ta e="T673" id="Seg_9691" s="T672">RUS:disc</ta>
            <ta e="T678" id="Seg_9692" s="T677">RUS:core</ta>
            <ta e="T725" id="Seg_9693" s="T723">RUS:cult</ta>
            <ta e="T764" id="Seg_9694" s="T763">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-UkOA">
            <ta e="T23" id="Seg_9695" s="T22">fortition Vsub</ta>
            <ta e="T144" id="Seg_9696" s="T143">Vsub</ta>
            <ta e="T174" id="Seg_9697" s="T173">Vsub Csub Vsub</ta>
            <ta e="T176" id="Seg_9698" s="T175">Csub</ta>
            <ta e="T190" id="Seg_9699" s="T189">Csub fortition Vsub</ta>
            <ta e="T198" id="Seg_9700" s="T197">fortition medVins Vsub</ta>
            <ta e="T238" id="Seg_9701" s="T237">fortition medVins fortition Vsub finCdel</ta>
            <ta e="T260" id="Seg_9702" s="T259">fortition medVins fortition Vsub finCdel</ta>
            <ta e="T267" id="Seg_9703" s="T266">Vsub Csub Vsub</ta>
            <ta e="T285" id="Seg_9704" s="T284">Csub fortition fortition Vsub</ta>
            <ta e="T303" id="Seg_9705" s="T302">Csub fortition Vsub</ta>
            <ta e="T482" id="Seg_9706" s="T481">Csub medVins fortition Vsub</ta>
            <ta e="T560" id="Seg_9707" s="T559">Csub fortition fortition Vsub</ta>
            <ta e="T567" id="Seg_9708" s="T566">Vsub Csub Vsub</ta>
            <ta e="T571" id="Seg_9709" s="T570">Vsub Csub Vsub</ta>
            <ta e="T678" id="Seg_9710" s="T677">fortition Vsub</ta>
            <ta e="T764" id="Seg_9711" s="T763">inCdel lenition Csub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-UkOA">
            <ta e="T23" id="Seg_9712" s="T22">dir:bare</ta>
            <ta e="T55" id="Seg_9713" s="T54">dir:infl</ta>
            <ta e="T128" id="Seg_9714" s="T127">dir:bare</ta>
            <ta e="T129" id="Seg_9715" s="T128">dir:bare</ta>
            <ta e="T144" id="Seg_9716" s="T143">dir:infl</ta>
            <ta e="T145" id="Seg_9717" s="T144">dir:infl</ta>
            <ta e="T150" id="Seg_9718" s="T149">dir:bare</ta>
            <ta e="T153" id="Seg_9719" s="T152">dir:infl</ta>
            <ta e="T159" id="Seg_9720" s="T158">dir:bare</ta>
            <ta e="T160" id="Seg_9721" s="T159">dir:bare</ta>
            <ta e="T174" id="Seg_9722" s="T173">dir:bare</ta>
            <ta e="T176" id="Seg_9723" s="T175">dir:bare</ta>
            <ta e="T182" id="Seg_9724" s="T181">dir:infl</ta>
            <ta e="T185" id="Seg_9725" s="T184">dir:bare</ta>
            <ta e="T188" id="Seg_9726" s="T187">dir:bare</ta>
            <ta e="T189" id="Seg_9727" s="T188">dir:bare</ta>
            <ta e="T190" id="Seg_9728" s="T189">dir:bare</ta>
            <ta e="T198" id="Seg_9729" s="T197">dir:infl</ta>
            <ta e="T203" id="Seg_9730" s="T202">dir:infl</ta>
            <ta e="T238" id="Seg_9731" s="T237">dir:bare</ta>
            <ta e="T242" id="Seg_9732" s="T241">dir:infl</ta>
            <ta e="T260" id="Seg_9733" s="T259">dir:bare</ta>
            <ta e="T267" id="Seg_9734" s="T266">dir:bare</ta>
            <ta e="T269" id="Seg_9735" s="T268">dir:bare</ta>
            <ta e="T275" id="Seg_9736" s="T274">dir:infl</ta>
            <ta e="T283" id="Seg_9737" s="T282">dir:bare</ta>
            <ta e="T285" id="Seg_9738" s="T284">dir:infl</ta>
            <ta e="T286" id="Seg_9739" s="T285">indir:infl</ta>
            <ta e="T303" id="Seg_9740" s="T302">dir:bare</ta>
            <ta e="T309" id="Seg_9741" s="T308">dir:bare</ta>
            <ta e="T373" id="Seg_9742" s="T372">dir:bare</ta>
            <ta e="T384" id="Seg_9743" s="T383">dir:infl</ta>
            <ta e="T399" id="Seg_9744" s="T398">dir:bare</ta>
            <ta e="T442" id="Seg_9745" s="T441">dir:bare</ta>
            <ta e="T444" id="Seg_9746" s="T443">dir:bare</ta>
            <ta e="T469" id="Seg_9747" s="T468">indir:infl</ta>
            <ta e="T472" id="Seg_9748" s="T471">dir:bare</ta>
            <ta e="T475" id="Seg_9749" s="T474">indir:bare</ta>
            <ta e="T479" id="Seg_9750" s="T478">indir:infl</ta>
            <ta e="T482" id="Seg_9751" s="T481">dir:infl</ta>
            <ta e="T505" id="Seg_9752" s="T504">dir:bare</ta>
            <ta e="T521" id="Seg_9753" s="T520">dir:bare</ta>
            <ta e="T529" id="Seg_9754" s="T528">dir:bare</ta>
            <ta e="T538" id="Seg_9755" s="T537">dir:bare</ta>
            <ta e="T539" id="Seg_9756" s="T538">indir:infl</ta>
            <ta e="T540" id="Seg_9757" s="T539">dir:bare</ta>
            <ta e="T543" id="Seg_9758" s="T542">dir:bare</ta>
            <ta e="T553" id="Seg_9759" s="T552">dir:bare</ta>
            <ta e="T560" id="Seg_9760" s="T559">dir:infl</ta>
            <ta e="T561" id="Seg_9761" s="T560">dir:infl</ta>
            <ta e="T562" id="Seg_9762" s="T561">dir:infl</ta>
            <ta e="T567" id="Seg_9763" s="T566">dir:bare</ta>
            <ta e="T570" id="Seg_9764" s="T569">indir:infl</ta>
            <ta e="T571" id="Seg_9765" s="T570">dir:bare</ta>
            <ta e="T576" id="Seg_9766" s="T575">dir:bare</ta>
            <ta e="T577" id="Seg_9767" s="T576">dir:bare</ta>
            <ta e="T578" id="Seg_9768" s="T577">dir:bare</ta>
            <ta e="T579" id="Seg_9769" s="T578">dir:bare</ta>
            <ta e="T606" id="Seg_9770" s="T605">dir:bare</ta>
            <ta e="T613" id="Seg_9771" s="T612">indir:infl</ta>
            <ta e="T615" id="Seg_9772" s="T614">indir:infl</ta>
            <ta e="T664" id="Seg_9773" s="T663">dir:bare</ta>
            <ta e="T665" id="Seg_9774" s="T664">dir:bare</ta>
            <ta e="T672" id="Seg_9775" s="T671">dir:bare</ta>
            <ta e="T673" id="Seg_9776" s="T672">dir:bare</ta>
            <ta e="T678" id="Seg_9777" s="T677">dir:bare</ta>
            <ta e="T725" id="Seg_9778" s="T723">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-UkOA">
            <ta e="T59" id="Seg_9779" s="T57">RUS:ext</ta>
            <ta e="T319" id="Seg_9780" s="T316">RUS:int.ins</ta>
            <ta e="T402" id="Seg_9781" s="T401">RUS:int.ins</ta>
            <ta e="T474" id="Seg_9782" s="T472">RUS:int.ins</ta>
         </annotation>
         <annotation name="fe" tierref="fe-UkOA">
            <ta e="T19" id="Seg_9783" s="T7">– Ah, I myself live in Khatanga, eh, I am rarely in the tundra, however.</ta>
            <ta e="T25" id="Seg_9784" s="T19">In spring and in autumn I am probably always in the tundra.</ta>
            <ta e="T31" id="Seg_9785" s="T25">How many years in a row helping my mother and her people, my father and his people.</ta>
            <ta e="T40" id="Seg_9786" s="T31">There, nomadizing in the tundra, they have their reindeer.</ta>
            <ta e="T57" id="Seg_9787" s="T40">The reindeer, looking for their reindeer (?) and I myself even whatchamacallit, I'm loaded with whatchamacallit, with energy, one could say.</ta>
            <ta e="T59" id="Seg_9788" s="T57">Fresh air.</ta>
            <ta e="T65" id="Seg_9789" s="T59">Being in Khatanga breathing is harder afterwards (?).</ta>
            <ta e="T90" id="Seg_9790" s="T84">– The tundra people know themselves how they will live.</ta>
            <ta e="T106" id="Seg_9791" s="T90">As they don't know a good life now, they don't say "we'll live well", apparently, they themselves.</ta>
            <ta e="T111" id="Seg_9792" s="T106">They enjoy what is there.</ta>
            <ta e="T119" id="Seg_9793" s="T111">If [something] isn't there, what will you do, what isn't there, isn't there.</ta>
            <ta e="T127" id="Seg_9794" s="T123">– They are not very sad, no.</ta>
            <ta e="T165" id="Seg_9795" s="T127">And if a properly living human, if the administration would give baloks more easily, the indispensable, that what is most needed and so on, it would come like a support, like a subsidy for the tundra people.</ta>
            <ta e="T170" id="Seg_9796" s="T165">Only with their own abilities they are going [=nomadizing], our tundra people.</ta>
            <ta e="T186" id="Seg_9797" s="T170">Therefore now these 1500, giving these one and a half thousand rubles, still a bit.</ta>
            <ta e="T191" id="Seg_9798" s="T186">Ah and so at all.</ta>
            <ta e="T210" id="Seg_9799" s="T191">Not very with food, not with bread, with nothing, because our bakery isn't working, they bake [lit. cook] themselves, when they are in the tundra, what kind of...</ta>
            <ta e="T228" id="Seg_9800" s="T210">Which young human says "I'll go, out of the warm house, away from the warm fire, into the cold, into the wind, I'll go reindeer herding"?</ta>
            <ta e="T233" id="Seg_9801" s="T228">They don't pay very well.</ta>
            <ta e="T236" id="Seg_9802" s="T233">They don't see money, the tundra people.</ta>
            <ta e="T245" id="Seg_9803" s="T236">Recently they brought tarpaulin and so on, the merchants sell it at a very high price.</ta>
            <ta e="T256" id="Seg_9804" s="T245">That, which human does buy it, eh, or does he buy food?</ta>
            <ta e="T269" id="Seg_9805" s="T256">You'll buy food or you'll buy tarpaulin or you'll buy clothes, your children also have to learn.</ta>
            <ta e="T287" id="Seg_9806" s="T269">Nevertheless, earlier the students and so on, they were given something, maybe a subsidy, it was sent from the sovkhoz.</ta>
            <ta e="T293" id="Seg_9807" s="T287">Now there isn't such thing.</ta>
            <ta e="T298" id="Seg_9808" s="T293">One does everything with one's own money.</ta>
            <ta e="T308" id="Seg_9809" s="T298">A person, who can't do it, dies at all, one can say, thinking.</ta>
            <ta e="T321" id="Seg_9810" s="T308">And [a person], who can [do it], gives his/her children some education, nevertheless they live at the poverly line.</ta>
            <ta e="T343" id="Seg_9811" s="T335">– Well, only the politicians, only the politicans help in our country.</ta>
            <ta e="T350" id="Seg_9812" s="T343">Improving our life comes only from them.</ta>
            <ta e="T381" id="Seg_9813" s="T360">– We ourselves now, our people now, many of our people came in from the tundra now, because they don't have anymore resources.</ta>
            <ta e="T395" id="Seg_9814" s="T381">They are all very experienced in reindeer herding, all this, all this, all this is dying out.</ta>
            <ta e="T404" id="Seg_9815" s="T395">All people, well, stay at one place, we say, stay at one place.</ta>
            <ta e="T421" id="Seg_9816" s="T404">All people, the people which are nomadizing now, they don't know the land very well, young people, when for example they are nomadizing now, going out.</ta>
            <ta e="T434" id="Seg_9817" s="T421">The places with reindeer moss, now the wild reindeer change their places and often come to our [places], they are eating our reindeer moss.</ta>
            <ta e="T446" id="Seg_9818" s="T434">Therefore the places for nomadizing have narrowed, both for the reindeer and for the people.</ta>
            <ta e="T449" id="Seg_9819" s="T446">Our people, eh...</ta>
            <ta e="T456" id="Seg_9820" s="T451">– A different, a different life...</ta>
            <ta e="T468" id="Seg_9821" s="T458">– For another life, they are striving for an easier life, it'll be easier.</ta>
            <ta e="T478" id="Seg_9822" s="T468">In Novorybnoe, eh, then, I'm telling now especially about Novorybnoe.</ta>
            <ta e="T495" id="Seg_9823" s="T478">In Novorybnoe it is better, they have light [=electricity], it is even warm, therefore the tundra people are not drawn to the tundra.</ta>
            <ta e="T524" id="Seg_9824" s="T495">Money very, eh, what do you get for a reindeer now: Kill it, few, even if you kill fifty reindeer, you won't gain a lot of money, or if you nomadize in the tundra.</ta>
            <ta e="T540" id="Seg_9825" s="T524">Therefore to live happily, for that also the tundra people live happily, the government has to think about that.</ta>
            <ta e="T543" id="Seg_9826" s="T540">Help for the people and...</ta>
            <ta e="T559" id="Seg_9827" s="T543">The people, the people won't do anything themselves, the tundra people, even more, as they don't say anything themselves.</ta>
            <ta e="T565" id="Seg_9828" s="T559">Our sovkhozes, our director, even the directors are changing.</ta>
            <ta e="T581" id="Seg_9829" s="T565">Still a bit now, in Novorybnoe one director has stayed – a man who is called Vladimir Aleksandrovich Momonov.</ta>
            <ta e="T592" id="Seg_9830" s="T581">I don't tell anything bad about him, a good man, mhm.</ta>
            <ta e="T610" id="Seg_9831" s="T592">With the people now, they got a little money, there was this whatchamacallit, zero seven they gave for their own production (?), they made this year.</ta>
            <ta e="T613" id="Seg_9832" s="T610">And we take (away) those ones.</ta>
            <ta e="T629" id="Seg_9833" s="T613">When the tundra people now, eh, the hunters also take it (away), the shooting people also get few.</ta>
            <ta e="T637" id="Seg_9834" s="T631">– They won't survive it, no.</ta>
            <ta e="T663" id="Seg_9835" s="T657">– We hope for it, yes, we hope for it.</ta>
            <ta e="T671" id="Seg_9836" s="T663">And so, we talked about this dark side.</ta>
            <ta e="T689" id="Seg_9837" s="T671">But living so in the tundra was also very happy, eh, the tundra people, as the children aren't nomadizing anymore, they don't know it.</ta>
            <ta e="T710" id="Seg_9838" s="T689">The Dolgan language, the clean breath, eh, the air, the smell of grass, one can say, the first noise of geese, the birth of calves, they don't know all that really.</ta>
            <ta e="T725" id="Seg_9839" s="T710">All this is for us, eh, for the poeple which were born in the tundra, all this are great things, impressions.</ta>
            <ta e="T764" id="Seg_9840" s="T763">– Thank you.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-UkOA">
            <ta e="T19" id="Seg_9841" s="T7">– Ah, ich selbst wohne in Chatanga, äh, ich bin wenig in der Tundra jedenfalls.</ta>
            <ta e="T25" id="Seg_9842" s="T19">Im Frühling und Herbst bin ich vielleicht immer in der Tundra.</ta>
            <ta e="T31" id="Seg_9843" s="T25">Wie viele Jahre nacheinander meiner Mutter und den Ihrigen, meinem Vater und den Seinigen helfend.</ta>
            <ta e="T40" id="Seg_9844" s="T31">Dort, in der Tundra nomadisierend haben sie ihre Rentiere.</ta>
            <ta e="T57" id="Seg_9845" s="T40">Die Rentiere, nach ihren Rentieren schauend (?) und ich selbst sogar dingse, werde geladen mit dings, mit Energie, könnte man sagen.</ta>
            <ta e="T59" id="Seg_9846" s="T57">Frische Luft.</ta>
            <ta e="T65" id="Seg_9847" s="T59">In Chatanga seiend wird das Atmen erschwert danach (?).</ta>
            <ta e="T90" id="Seg_9848" s="T84">– Die Tundraleute wissen selbst, wie sie leben werden.</ta>
            <ta e="T106" id="Seg_9849" s="T90">Da sie jetzt kein gutes Leben kennen, sagen sie nicht "wir werden gut leben", offenbar, sie selbst.</ta>
            <ta e="T111" id="Seg_9850" s="T106">Was es gibt, daran freuen sie sich.</ta>
            <ta e="T119" id="Seg_9851" s="T111">Wenn es [etwas] nicht gibt, was willst du machen, was nicht ist, ist nicht.</ta>
            <ta e="T127" id="Seg_9852" s="T123">– Sie sind nicht sehr traurig, nein.</ta>
            <ta e="T165" id="Seg_9853" s="T127">Und wenn ein gut, ein richtig lebender Mensch, wenn man leichter Baloks von der Verwaltung geben würde, das Unverzichtbare, das am meisten Benötigte und so, es würde wie eine Unterstützung, wie ein Zuschuss für die Leute der Tundra kommen. </ta>
            <ta e="T170" id="Seg_9854" s="T165">Nur mit ihren eigenen Fähigkeiten gehen [=nomadisieren] sie, unsere Leute der Tundra.</ta>
            <ta e="T186" id="Seg_9855" s="T170">Deshalb jetzt diese 1500, diese anderthalb tausend Rubel gebend, noch ein Bisschen.</ta>
            <ta e="T191" id="Seg_9856" s="T186">Ah, und überhaupt so.</ta>
            <ta e="T210" id="Seg_9857" s="T191">Nicht sehr mit Nahrung, nicht mit Brot, mit nichts, da unsere Bäckerei nicht arbeitet, backen [wörtl. kochen] sie selbst, wenn sie in der Tundra sind, was für ein…</ta>
            <ta e="T228" id="Seg_9858" s="T210">Welcher junge Mensch sagt "ich gehe, aus dem warmen Haus hinaus, vom warmen Feuer weg, in die Kälte, in den Wind, ich gehe Rentiere hüten"?</ta>
            <ta e="T233" id="Seg_9859" s="T228">Sie bezahlen dann nicht besonders.</ta>
            <ta e="T236" id="Seg_9860" s="T233">Sie sehen kein Geld, die Leute der Tundra.</ta>
            <ta e="T245" id="Seg_9861" s="T236">Vor Kurzem haben sie Plane und so gebracht, die Kaufleute verkaufen es zu einem sehr hohen Preis.</ta>
            <ta e="T256" id="Seg_9862" s="T245">Das, welcher Mensch kauft das, äh, oder kauft er Nahrung?</ta>
            <ta e="T269" id="Seg_9863" s="T256">Du kaufst Nahrung oder du kaufst Plane oder du kaufst Kleidung, deine Kinder müssen auch noch lernen.</ta>
            <ta e="T287" id="Seg_9864" s="T269">Und dennoch früher die Studenten und so, denen gab man etwas, vielleicht einen Zuschuss, von der Sowchose wurde das geschickt.</ta>
            <ta e="T293" id="Seg_9865" s="T287">Jetzt gibt es sowas nicht.</ta>
            <ta e="T298" id="Seg_9866" s="T293">Alles macht der Mensch mit seinem eigenen Geld.</ta>
            <ta e="T308" id="Seg_9867" s="T298">Wer ein Mensch ist, der das nicht kann, der stirbt ganz, kann man sagen, denkend.</ta>
            <ta e="T321" id="Seg_9868" s="T308">Und wer kann, der gibt seinen Kindern etwas Bildung, dennoch leben sie am Rande der Armut. </ta>
            <ta e="T343" id="Seg_9869" s="T335">– Na, nur die Politiker, nur die Politiker helfen in unserem Land.</ta>
            <ta e="T350" id="Seg_9870" s="T343">Dass unser Leben besser wird, geht nur von ihnen aus.</ta>
            <ta e="T381" id="Seg_9871" s="T360">– Wir selbst jetzt, unsere Leute jetzt, viele unserer Leute sind jetzt aus der Tundra hereingekommen, weil sie keine Mittel mehr haben.</ta>
            <ta e="T395" id="Seg_9872" s="T381">Sie alle haben viel Erfahrung in der Rentierzucht, das alles, das alles, das alles stirbt.</ta>
            <ta e="T404" id="Seg_9873" s="T395">Alle Leute, nun, bleiben an einem Ort, sagen wir, bleiben an einem Ort.</ta>
            <ta e="T421" id="Seg_9874" s="T404">Alle Leute, die Leute, die jetzt nomadisieren, kennen das Land nicht sehr gut, junge Leute, wenn die jetzt zum Beispiel nomadisieren, hinausgehen.</ta>
            <ta e="T434" id="Seg_9875" s="T421">Die Plätze mit Flechten, jetzt ändern die wilden Rentiere ihre Plätze und kommen viel zu unseren [Plätzen], sie fressen unsere Flechten auf.</ta>
            <ta e="T446" id="Seg_9876" s="T434">Deshalb sind die Orte zum Nomadisieren enger geworden, wie für die Rentiere, so für den Menschen.</ta>
            <ta e="T449" id="Seg_9877" s="T446">Unsere Leute, äh…</ta>
            <ta e="T456" id="Seg_9878" s="T451">– Ein anderes, anderes Leben…</ta>
            <ta e="T468" id="Seg_9879" s="T458">– Nach einem anderen Leben, nach einem leichteren Leben streben sie, es wird leichter sein.</ta>
            <ta e="T478" id="Seg_9880" s="T468">In Novorybnoe, äh, dann, ich erzähle jetzt vor allem über Novorybnoe.</ta>
            <ta e="T495" id="Seg_9881" s="T478">In Novorybnoe ist es besser, sie haben sogar Licht [=Elektrizität], es ist sogar warm, deshalb drängt es die Leute der Tundra nicht dazu in die Tundra zu gehen.</ta>
            <ta e="T524" id="Seg_9882" s="T495">Geld ziemlich, äh, was bekommst du jetzt für ein Rentier: Töte es, wenig, sogar wenn du fünfzig Rentiere tötest, verdienst du nicht viel Geld daran, oder wenn du in der Tundra nomadisierst.</ta>
            <ta e="T540" id="Seg_9883" s="T524">Deshalb gut zu leben, damit auch die Leute in der Tundra glücklich leben, muss die Regierung zuerst darüber nachdenken.</ta>
            <ta e="T543" id="Seg_9884" s="T540">Hilfe für die Leute und…</ta>
            <ta e="T559" id="Seg_9885" s="T543">Die Leute, von selbst werden die Leute nichts machen, die Leute der Tundra, umso mehr, da sie von sich aus nichts sagen.</ta>
            <ta e="T565" id="Seg_9886" s="T559">Unsere Sowchosen, unser Direktor, die Direktoren ändern sich sogar.</ta>
            <ta e="T581" id="Seg_9887" s="T565">Jetzt noch ein Bisschen, bei uns in Novorybnoe ist ein Direktor noch geblieben – ein Mensch, der Vladimir Aleksandrovich Momonov heißt.</ta>
            <ta e="T592" id="Seg_9888" s="T581">Über ihn sage ich nichts Schlechtes, ein guter Mensch, mhm.</ta>
            <ta e="T610" id="Seg_9889" s="T592">Mit den Leuten jetzt, sie haben ein Bisschen Geld bekommen, dieses Dings war, null sieben gaben sie für ihre eigene Produktion (?), haben sie in diesem Jahr gemacht.</ta>
            <ta e="T613" id="Seg_9890" s="T610">Und jene nehmen wir (weg).</ta>
            <ta e="T630" id="Seg_9891" s="T613">Wenn jetzt die Tundraleute, äh, die Jäger das auch (weg)nehmen, die schießenden Leute bekommen doch auch wenig.</ta>
            <ta e="T637" id="Seg_9892" s="T631">– Sie überleben es nicht, nein.</ta>
            <ta e="T663" id="Seg_9893" s="T657">– Darauf hoffen wir, ja, darauf hoffen wir. </ta>
            <ta e="T671" id="Seg_9894" s="T663">Und so, über diese dunkle Seite haben wir uns unterhalten.</ta>
            <ta e="T689" id="Seg_9895" s="T671">Und so in der Tundra zu leben war doch sehr fröhlich, eh, die Leute der Tundra, da die Kinder jetzt nicht mehr nomadisieren, wissen sie es jetzt nicht.</ta>
            <ta e="T710" id="Seg_9896" s="T689">Die dolganische Sprache, den sauberen Atem, äh, die Luft, den Geruch von Gras, kann man sagen, den ersten Lärm der Gänse, die Geburt der Kälber, das alles kennen sie nicht wirklich.</ta>
            <ta e="T725" id="Seg_9897" s="T710">Das alles ist für uns, äh, für die Menschen, die in der Tundra geboren sind, das alles sind für uns großartige Dings, Eindrücke.</ta>
            <ta e="T764" id="Seg_9898" s="T763">– Danke. </ta>
         </annotation>
         <annotation name="fr" tierref="fr-UkOA" />
         <annotation name="ltr" tierref="ltr-UkOA">
            <ta e="T19" id="Seg_9899" s="T7">– Аа, сам в Хатанге дом имеяя, ээ, в тундре мало бываю, всё равно.</ta>
            <ta e="T25" id="Seg_9900" s="T19">Весной, осенью постаянно в тундру езжу.</ta>
            <ta e="T31" id="Seg_9901" s="T25">Сколько лет подряд маме, папе помогая. </ta>
            <ta e="T40" id="Seg_9902" s="T31">Там, в тундре качуя то с оленями ведь.</ta>
            <ta e="T57" id="Seg_9903" s="T40">Олени, олиней своих наблюдают и сам даже заж.. это, грею ээ это, энергией так сказать.</ta>
            <ta e="T59" id="Seg_9904" s="T57">Свежий воздух. </ta>
            <ta e="T65" id="Seg_9905" s="T59">В Хатанге находясь задыхаться стал.</ta>
            <ta e="T90" id="Seg_9906" s="T84">– Тундровики сами знают ведь, как будут жить. </ta>
            <ta e="T106" id="Seg_9907" s="T90">Сейчас хорошую жизнь не зная, они хорошо жить будут, думая, не поговаривают, вроде, сильно теперь. </ta>
            <ta e="T111" id="Seg_9908" s="T106">Что есть тому и радуюются. </ta>
            <ta e="T119" id="Seg_9909" s="T111">Если не будет, что поделаешь, нет так нет. </ta>
            <ta e="T127" id="Seg_9910" s="T123">– Сильно не печалятся, ага.</ta>
            <ta e="T165" id="Seg_9911" s="T127">А если бы хоро хорошо оо по-человечески вот по-настоящему живущий человек это, и вправду – полегче балки, от администрации если выделили бы, вот необходимое, самое нужное приспособления и так далее .. вот ведь помощь, как дотация по-серьёзному приходила бы тундровикам.</ta>
            <ta e="T170" id="Seg_9912" s="T165">Своими, своими силами только кочуют (покочёвывают) тундровики наши.</ta>
            <ta e="T186" id="Seg_9913" s="T170">Поэтому сейчас ещё эту тысячу пятьсот, вот это стабильно полторашку выплачивая, ещё ничего.</ta>
            <ta e="T191" id="Seg_9914" s="T186">Аа, а так совсем ведь.</ta>
            <ta e="T210" id="Seg_9915" s="T191">Вот сильно ни едой, ни хлебом да.., ничем вот, и эта пеканрня наша не работая, сами пекут как могут, в тундре находясь, какой..</ta>
            <ta e="T228" id="Seg_9916" s="T210">Какой молодой человек, пойду скажет от тёплого дома, от тёплого очага выйдя, на холод, на ветер выйдя, оленя стеречь пойду, думая даже?</ta>
            <ta e="T233" id="Seg_9917" s="T228">Не платят даже вот сильно-то.</ta>
            <ta e="T236" id="Seg_9918" s="T233">Деньги не видят тундровики. </ta>
            <ta e="T245" id="Seg_9919" s="T236">Сейчас брезент, такое привозят коммерсанты продают очень дорого.</ta>
            <ta e="T256" id="Seg_9920" s="T245">То какой человек покупая купит или, и ээ, еду, еду купит ли ?</ta>
            <ta e="T269" id="Seg_9921" s="T256">– ду купишь ли, брезент купишь ли, одежду купишь ли, дети твои ещё должны учиться. </ta>
            <ta e="T287" id="Seg_9922" s="T269">И тем не менее раньше студентов, таковых даже давали им что-то, и дотацию, из совхоза направляли ведь. </ta>
            <ta e="T293" id="Seg_9923" s="T287">Сейчас такого вот ничего нет.</ta>
            <ta e="T298" id="Seg_9924" s="T293">Всё человек своими деньгами снабжается. </ta>
            <ta e="T308" id="Seg_9925" s="T298">Кто немощный человек, совсем умирает, так сказать, думая.</ta>
            <ta e="T321" id="Seg_9926" s="T308">А кто сможет, по-тихоньку обучают детей, всё равно на грани нищеты живут. </ta>
            <ta e="T343" id="Seg_9927" s="T335">И только руководители, руководители только помогут на нашей земле. </ta>
            <ta e="T350" id="Seg_9928" s="T343">Нашей жизни улучшение, всё от них только выйдет. </ta>
            <ta e="T381" id="Seg_9929" s="T360">– Сами мы .. Сами мы, люди наши сейчас, люди наши ведь в основном многие ээ заехали (переехали) из тундры, немощными ставши. </ta>
            <ta e="T395" id="Seg_9930" s="T381">Все с большим опытом в оленеводстве даже ээ и всё такое, всё такое, всё такое умирает. </ta>
            <ta e="T404" id="Seg_9931" s="T395">Весь люд ээ, ну, на земле остаётся, скажем, на земле остаётся. </ta>
            <ta e="T421" id="Seg_9932" s="T404">Весь люд .., сейчас кочующие даже людей хорошо земли не знают, молодёжь, даже кочуя, выезжая, допустим.</ta>
            <ta e="T434" id="Seg_9933" s="T421">Ягельные места .. сейчас дикий олень сейчас место миграции меняя, по нам много спускаясь, ягель всесь съедает.</ta>
            <ta e="T446" id="Seg_9934" s="T434">Поэтому места кочевья сузились – для оленя даже.. как для оленя, так и для человека даже.</ta>
            <ta e="T449" id="Seg_9935" s="T446">Люди наши ээ.. </ta>
            <ta e="T456" id="Seg_9936" s="T451">– Другую, Другую жизнь .. </ta>
            <ta e="T468" id="Seg_9937" s="T458">– Другую жизнь, за лёгкой жизнью следуют… Лёгкая вот лёгкая будет ..</ta>
            <ta e="T478" id="Seg_9938" s="T468">В Новорыбной живи .., ээ, потом, в частности, про Новорыбную рассказываю вот.</ta>
            <ta e="T495" id="Seg_9939" s="T478">В Новорыбной лучше ведь – и свет у них есть, и тепло, поэтому тундры … тундровики в тундре жить не сильно тянутся.</ta>
            <ta e="T524" id="Seg_9940" s="T495">Денег сильно ээ.. От оленя что возьмёшь вот сейчас: и забей даже .. немножко пятьдесят оленей даже, если забьёшь, сильно-то много денег не заработаешь тем не менее, или по тундре кочуя даже. </ta>
            <ta e="T540" id="Seg_9941" s="T524">Поэтому счастливо жить и в тундре люди чтобы счастливо жили это первой об этом правительство должно думать – </ta>
            <ta e="T543" id="Seg_9942" s="T540">людям помощь ии</ta>
            <ta e="T559" id="Seg_9943" s="T543">люди .. сами люди ничего не сделают, тундровые люди, тем более за себя ничего не говорят(не просят). </ta>
            <ta e="T565" id="Seg_9944" s="T559">Совзозы наши, директор наш .. Директора меняются даже. </ta>
            <ta e="T581" id="Seg_9945" s="T565">Сечас ещё ничего, у нас в Новорыбной ещё подзадержался малость вот один директор – Мамонов Владимир Александрович – под таким именем человек.</ta>
            <ta e="T592" id="Seg_9946" s="T581">О нём вот ничего плохого не говорю, хороший человек. Ммм.</ta>
            <ta e="T610" id="Seg_9947" s="T592">Вместе с людьми сейчас.. немного с деньгами стали – вот этот было: ноль семь давали от продукции-то, в этом году сделали. </ta>
            <ta e="T613" id="Seg_9948" s="T610">И те убирём, говоря сейчас.</ta>
            <ta e="T630" id="Seg_9949" s="T613">То если уберут сейчас. То если уберут опять сейчас тундровики ээ охотники также, стрелять стреляющие люди даже мало получать будут всё равно.</ta>
            <ta e="T637" id="Seg_9950" s="T631">– Не выживут, да </ta>
            <ta e="T663" id="Seg_9951" s="T657">– На то только надеемся, ээ, на то надеемся. </ta>
            <ta e="T671" id="Seg_9952" s="T663">А так .. – Это тёмную сторону обсудили мы. </ta>
            <ta e="T689" id="Seg_9953" s="T671">А так ведь в тундре если жить, очень весело, ээ, тундровики.. теререшняя молодёжь не кочуя, сейчасне знают ведь.</ta>
            <ta e="T710" id="Seg_9954" s="T689">Долганский язык, чистое дыхание ээ воздух, травы запах, – скажем так, первый гусей гомон, телят рождение, – всё сильно-то не знают. </ta>
            <ta e="T725" id="Seg_9955" s="T710">Всё это нам ээ, в тундре рождённым, это всё, всё большое ведь, впечатление. </ta>
            <ta e="T764" id="Seg_9956" s="T763">– Спасибо.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-UkOA" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KuNS"
                          name="ref"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KuNS"
                          name="st"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KuNS"
                          name="ts"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KuNS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KuNS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KuNS"
                          name="mb"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KuNS"
                          name="mp"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KuNS"
                          name="ge"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KuNS"
                          name="gg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KuNS"
                          name="gr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KuNS"
                          name="mc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KuNS"
                          name="ps"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KuNS"
                          name="SeR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KuNS"
                          name="SyF"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KuNS"
                          name="IST"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KuNS"
                          name="Top"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KuNS"
                          name="Foc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KuNS"
                          name="BOR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KuNS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KuNS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KuNS"
                          name="CS"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KuNS"
                          name="fe"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KuNS"
                          name="fg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KuNS"
                          name="fr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KuNS"
                          name="ltr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KuNS"
                          name="nt"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-UkOA"
                          name="ref"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-UkOA"
                          name="st"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-UkOA"
                          name="ts"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-UkOA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-UkOA"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-UkOA"
                          name="mb"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-UkOA"
                          name="mp"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-UkOA"
                          name="ge"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-UkOA"
                          name="gg"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-UkOA"
                          name="gr"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-UkOA"
                          name="mc"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-UkOA"
                          name="ps"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-UkOA"
                          name="SeR"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-UkOA"
                          name="SyF"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-UkOA"
                          name="IST"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-UkOA"
                          name="Top"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-UkOA"
                          name="Foc"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-UkOA"
                          name="BOR"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-UkOA"
                          name="BOR-Phon"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-UkOA"
                          name="BOR-Morph"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-UkOA"
                          name="CS"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-UkOA"
                          name="fe"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-UkOA"
                          name="fg"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-UkOA"
                          name="fr"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-UkOA"
                          name="ltr"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-UkOA"
                          name="nt"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
