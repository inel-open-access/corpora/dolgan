<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID353D31D5-9B27-4ABF-FEE8-57A1DC478B01">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnMSp_2009_Holiday_nar</transcription-name>
         <referenced-file url="AnIM_AnMSp_2009_Holiday_conv.wav" />
         <referenced-file url="AnIM_AnMSp_2009_Holiday_conv.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\conv\AnMSp_AnIM_2009_Holiday_conv\AnIM_AnMSp_2009_Holiday_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">290</ud-information>
            <ud-information attribute-name="# HIAT:w">211</ud-information>
            <ud-information attribute-name="# e">210</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">58</ud-information>
            <ud-information attribute-name="# sc">90</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnIM">
            <abbreviation>AnIM</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="AnMSp">
            <abbreviation>AnMSp</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="5.793333619698" />
         <tli id="T1" time="6.248" type="appl" />
         <tli id="T2" time="6.696" type="appl" />
         <tli id="T3" time="7.143" type="appl" />
         <tli id="T4" time="7.591" type="appl" />
         <tli id="T5" time="8.093185940630121" />
         <tli id="T6" time="12.37977454015662" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T90" />
         <tli id="T194" />
         <tli id="T211" />
         <tli id="T10" time="16.626363868147877" />
         <tli id="T11" time="20.532959388089598" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" time="20.619624476415954" />
         <tli id="T17" time="21.293" type="appl" />
         <tli id="T18" time="21.787" type="appl" />
         <tli id="T19" time="22.28" type="appl" />
         <tli id="T20" time="22.532" type="appl" />
         <tli id="T21" time="22.764" type="appl" />
         <tli id="T22" time="22.996" type="appl" />
         <tli id="T23" time="23.228" type="appl" />
         <tli id="T24" time="23.46" type="appl" />
         <tli id="T25" time="24.02" type="appl" />
         <tli id="T26" time="24.56" type="appl" />
         <tli id="T27" time="25.1" type="appl" />
         <tli id="T28" time="25.54" type="appl" />
         <tli id="T29" time="25.96" type="appl" />
         <tli id="T30" time="26.653" type="appl" />
         <tli id="T31" time="27.327" type="appl" />
         <tli id="T32" time="28.0" type="appl" />
         <tli id="T33" time="28.707" type="appl" />
         <tli id="T34" time="29.344" type="appl" />
         <tli id="T35" time="29.981" type="appl" />
         <tli id="T36" time="30.619" type="appl" />
         <tli id="T37" time="31.256" type="appl" />
         <tli id="T38" time="31.893" type="appl" />
         <tli id="T39" time="32.53" type="appl" />
         <tli id="T40" time="33.094" type="appl" />
         <tli id="T41" time="33.64" type="appl" />
         <tli id="T42" time="34.407" type="appl" />
         <tli id="T43" time="35.023" type="appl" />
         <tli id="T44" time="35.64" type="appl" />
         <tli id="T45" time="36.232" type="appl" />
         <tli id="T46" time="36.804" type="appl" />
         <tli id="T47" time="37.377" type="appl" />
         <tli id="T48" time="37.949" type="appl" />
         <tli id="T49" time="38.521" type="appl" />
         <tli id="T50" time="39.093" type="appl" />
         <tli id="T51" time="39.666" type="appl" />
         <tli id="T52" time="40.238" type="appl" />
         <tli id="T53" time="40.81" type="appl" />
         <tli id="T54" time="41.13" type="appl" />
         <tli id="T55" time="41.43" type="appl" />
         <tli id="T56" time="41.73" type="appl" />
         <tli id="T57" time="42.622" type="appl" />
         <tli id="T58" time="43.475" type="appl" />
         <tli id="T59" time="44.328" type="appl" />
         <tli id="T60" time="45.18" type="appl" />
         <tli id="T61" time="46.14" type="appl" />
         <tli id="T62" time="46.6" type="appl" />
         <tli id="T63" time="47.06" type="appl" />
         <tli id="T64" time="48.266" type="appl" />
         <tli id="T65" time="48.951" type="appl" />
         <tli id="T66" time="49.637" type="appl" />
         <tli id="T67" time="50.323" type="appl" />
         <tli id="T68" time="51.009" type="appl" />
         <tli id="T69" time="51.694" type="appl" />
         <tli id="T70" time="52.38" type="appl" />
         <tli id="T71" time="53.763" type="appl" />
         <tli id="T72" time="54.297" type="appl" />
         <tli id="T73" time="54.83" type="appl" />
         <tli id="T74" time="55.192" type="appl" />
         <tli id="T75" time="55.505" type="appl" />
         <tli id="T76" time="55.818" type="appl" />
         <tli id="T77" time="56.13" type="appl" />
         <tli id="T78" time="56.64563503915498" />
         <tli id="T79" time="57.332289200509926" />
         <tli id="T80" />
         <tli id="T81" time="57.745" type="appl" />
         <tli id="T82" time="58.232" type="appl" />
         <tli id="T83" time="58.72" type="appl" />
         <tli id="T84" time="60.602" type="appl" />
         <tli id="T85" time="62.315" type="appl" />
         <tli id="T212" time="63.9655017301038" />
         <tli id="T86" time="64.5388246221089" />
         <tli id="T213" time="64.638822800947" />
         <tli id="T87" time="65.74" type="appl" />
         <tli id="T210" time="65.804" type="intp" />
         <tli id="T88" time="66.092" type="appl" />
         <tli id="T89" time="66.405" type="appl" />
         <tli id="T91" time="67.01211291203788" />
         <tli id="T92" time="67.35" type="appl" />
         <tli id="T93" time="67.66" type="appl" />
         <tli id="T94" time="67.97" type="appl" />
         <tli id="T95" time="68.54" type="appl" />
         <tli id="T96" time="69.07" type="appl" />
         <tli id="T97" time="69.6" type="appl" />
         <tli id="T98" time="70.393" type="appl" />
         <tli id="T99" time="71.027" type="appl" />
         <tli id="T100" time="71.66" type="appl" />
         <tli id="T101" time="72.293" type="appl" />
         <tli id="T102" time="72.927" type="appl" />
         <tli id="T103" time="73.56" type="appl" />
         <tli id="T104" time="74.765" type="appl" />
         <tli id="T105" time="75.75" type="appl" />
         <tli id="T106" time="76.735" type="appl" />
         <tli id="T107" time="77.72" type="appl" />
         <tli id="T108" time="78.705" type="appl" />
         <tli id="T109" time="79.81854634857038" />
         <tli id="T110" time="80.635" type="appl" />
         <tli id="T111" time="81.52" type="appl" />
         <tli id="T112" time="82.18" type="appl" />
         <tli id="T113" time="82.81" type="appl" />
         <tli id="T114" time="83.44" type="appl" />
         <tli id="T115" time="85.45177708978328" />
         <tli id="T116" time="85.81177053360044" />
         <tli id="T117" time="86.007" type="appl" />
         <tli id="T118" time="86.41" type="appl" />
         <tli id="T119" time="86.813" type="appl" />
         <tli id="T120" time="87.217" type="appl" />
         <tli id="T121" time="87.62" type="appl" />
         <tli id="T122" time="88.287" type="appl" />
         <tli id="T123" time="88.713" type="appl" />
         <tli id="T124" time="89.14" type="appl" />
         <tli id="T125" time="89.452" type="appl" />
         <tli id="T126" time="89.735" type="appl" />
         <tli id="T127" time="90.018" type="appl" />
         <tli id="T128" time="90.3" type="appl" />
         <tli id="T129" time="90.84" type="appl" />
         <tli id="T130" time="91.214" type="appl" />
         <tli id="T131" time="91.538" type="appl" />
         <tli id="T132" time="91.861" type="appl" />
         <tli id="T133" time="92.185" type="appl" />
         <tli id="T134" time="92.509" type="appl" />
         <tli id="T135" time="93.33" type="appl" />
         <tli id="T136" time="94.02" type="appl" />
         <tli id="T137" time="94.474" type="appl" />
         <tli id="T138" time="94.768" type="appl" />
         <tli id="T139" time="95.062" type="appl" />
         <tli id="T140" time="95.356" type="appl" />
         <tli id="T141" time="95.65" type="appl" />
         <tli id="T142" time="96.24" type="appl" />
         <tli id="T143" time="96.7" type="appl" />
         <tli id="T144" time="97.16" type="appl" />
         <tli id="T145" time="97.62" type="appl" />
         <tli id="T146" time="98.08" type="appl" />
         <tli id="T147" time="98.435" type="appl" />
         <tli id="T148" time="98.74" type="appl" />
         <tli id="T149" time="99.401" type="appl" />
         <tli id="T150" time="99.953" type="appl" />
         <tli id="T151" time="100.504" type="appl" />
         <tli id="T152" time="101.056" type="appl" />
         <tli id="T153" time="101.607" type="appl" />
         <tli id="T154" time="102.159" type="appl" />
         <tli id="T155" time="103.4114500091058" />
         <tli id="T156" time="104.145" type="appl" />
         <tli id="T157" time="104.85" type="appl" />
         <tli id="T158" time="105.277" type="appl" />
         <tli id="T159" time="105.663" type="appl" />
         <tli id="T160" time="107.25804662174467" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T161" time="108.982" type="appl" />
         <tli id="T162" time="109.495" type="appl" />
         <tli id="T163" time="110.008" type="appl" />
         <tli id="T164" time="110.52" type="appl" />
         <tli id="T165" time="111.032" type="appl" />
         <tli id="T166" time="111.545" type="appl" />
         <tli id="T167" time="112.057" type="appl" />
         <tli id="T168" time="114.79124276088145" />
         <tli id="T169" time="115.19" type="appl" />
         <tli id="T170" time="116.42" type="appl" />
         <tli id="T171" time="117.65" type="appl" />
         <tli id="T172" time="118.186" type="appl" />
         <tli id="T173" time="118.682" type="appl" />
         <tli id="T174" time="119.178" type="appl" />
         <tli id="T175" time="119.674" type="appl" />
         <tli id="T176" time="120.17" type="appl" />
         <tli id="T177" time="121.034" type="appl" />
         <tli id="T178" time="121.709" type="appl" />
         <tli id="T179" time="122.383" type="appl" />
         <tli id="T180" time="123.057" type="appl" />
         <tli id="T181" time="123.731" type="appl" />
         <tli id="T182" time="124.406" type="appl" />
         <tli id="T183" time="125.08" type="appl" />
         <tli id="T184" time="125.78" type="appl" />
         <tli id="T185" time="126.36" type="appl" />
         <tli id="T186" time="127.04" type="appl" />
         <tli id="T187" time="127.68" type="appl" />
         <tli id="T188" time="128.655" type="appl" />
         <tli id="T189" time="129.6" type="appl" />
         <tli id="T190" time="130.46" type="appl" />
         <tli id="T191" time="130.97" type="appl" />
         <tli id="T192" time="131.45" type="appl" />
         <tli id="T193" time="134.683" type="appl" />
         <tli id="T195" time="135.43" type="appl" />
         <tli id="T196" time="135.787" type="appl" />
         <tli id="T197" time="136.134" type="appl" />
         <tli id="T198" time="136.482" type="appl" />
         <tli id="T199" time="136.829" type="appl" />
         <tli id="T200" time="137.73" type="appl" />
         <tli id="T201" time="138.51" type="appl" />
         <tli id="T202" time="140.19" type="appl" />
         <tli id="T203" time="140.74" type="appl" />
         <tli id="T204" time="141.85" type="appl" />
         <tli id="T205" time="142.93" type="appl" />
         <tli id="T206" time="143.385" type="appl" />
         <tli id="T207" time="143.8" type="appl" />
         <tli id="T208" time="144.56" type="appl" />
         <tli id="T209" time="146.424" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-AnIM"
                      id="tx-AnIM"
                      speaker="AnIM"
                      type="t">
         <timeline-fork end="T194" start="T90">
            <tli id="T90.tx-AnIM.1" />
            <tli id="T90.tx-AnIM.2" />
            <tli id="T90.tx-AnIM.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-AnIM">
            <ts e="T10" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Meːne</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kepsi͡eŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ös</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">kepsi͡eŋ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">meːne</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Bihi͡eke</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kepsiːgin</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">meːne</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">haːppakka</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_34" n="HIAT:w" s="T9">kimneːbekkegin</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90.tx-AnIM.1" id="Seg_38" n="HIAT:w" s="T90">vsjo</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90.tx-AnIM.2" id="Seg_41" n="HIAT:w" s="T90.tx-AnIM.1">eto</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_43" n="HIAT:ip">(</nts>
                  <nts id="Seg_44" n="HIAT:ip">(</nts>
                  <ats e="T90.tx-AnIM.3"
                       id="Seg_45"
                       n="HIAT:non-pho"
                       s="T90.tx-AnIM.2">…</ats>
                  <nts id="Seg_46" n="HIAT:ip">)</nts>
                  <nts id="Seg_47" n="HIAT:ip">)</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_50" n="HIAT:w" s="T90.tx-AnIM.3">a</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_53" n="HIAT:w" s="T194">dʼe</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_56" n="HIAT:w" s="T211">kepseː</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T24" id="Seg_59" n="sc" s="T19">
               <ts e="T24" id="Seg_61" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_63" n="HIAT:w" s="T19">Kimi</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_66" n="HIAT:w" s="T20">gɨtta</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_69" n="HIAT:w" s="T21">baragɨn</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_72" n="HIAT:w" s="T22">ke</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_75" n="HIAT:w" s="T23">tɨ͡aga</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T29" id="Seg_78" n="sc" s="T27">
               <ts e="T29" id="Seg_80" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_82" n="HIAT:w" s="T27">Heee</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_85" n="HIAT:w" s="T28">de</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T41" id="Seg_88" n="sc" s="T39">
               <ts e="T41" id="Seg_90" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_92" n="HIAT:w" s="T39">Körtügüt</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_95" n="HIAT:w" s="T40">du͡o</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T56" id="Seg_98" n="sc" s="T53">
               <ts e="T56" id="Seg_100" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_102" n="HIAT:w" s="T53">Heee</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_105" n="HIAT:w" s="T54">onton</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_108" n="HIAT:w" s="T55">ke</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T77" id="Seg_111" n="sc" s="T73">
               <ts e="T77" id="Seg_113" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_115" n="HIAT:w" s="T73">Kanna</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_118" n="HIAT:w" s="T74">tiːjtigitij</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_121" n="HIAT:w" s="T75">bartɨgɨtɨj</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_124" n="HIAT:w" s="T76">ke</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T83" id="Seg_127" n="sc" s="T78">
               <ts e="T83" id="Seg_129" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_131" n="HIAT:w" s="T78">Heː</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_135" n="HIAT:w" s="T79">kimneːk</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_138" n="HIAT:w" s="T80">baːllar</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_141" n="HIAT:w" s="T81">ke</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_144" n="HIAT:w" s="T82">tɨ͡aga</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T213" id="Seg_147" n="sc" s="T212">
               <ts e="T213" id="Seg_149" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_151" n="HIAT:w" s="T212">Natalaːk</ts>
                  <nts id="Seg_152" n="HIAT:ip">.</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T94" id="Seg_154" n="sc" s="T91">
               <ts e="T94" id="Seg_156" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_158" n="HIAT:w" s="T91">Hu͡ok</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_161" n="HIAT:w" s="T92">etiler</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_164" n="HIAT:w" s="T93">Natalaːk</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T111" id="Seg_167" n="sc" s="T109">
               <ts e="T111" id="Seg_169" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_171" n="HIAT:w" s="T109">Ilim</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_174" n="HIAT:w" s="T110">körüne</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T121" id="Seg_177" n="sc" s="T115">
               <ts e="T121" id="Seg_179" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_181" n="HIAT:w" s="T115">Onton</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_184" n="HIAT:w" s="T116">ke</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_187" n="HIAT:w" s="T117">Diːma</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_190" n="HIAT:w" s="T118">ölörtö</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_193" n="HIAT:w" s="T119">du͡o</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_196" n="HIAT:w" s="T120">kɨːlɨ</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T128" id="Seg_199" n="sc" s="T124">
               <ts e="T128" id="Seg_201" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_203" n="HIAT:w" s="T124">Ölörbüte</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_206" n="HIAT:w" s="T125">di͡en</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_209" n="HIAT:w" s="T126">du</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_212" n="HIAT:w" s="T127">kas</ts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T134" id="Seg_215" n="sc" s="T129">
               <ts e="T134" id="Seg_217" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_219" n="HIAT:w" s="T129">Kas</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_222" n="HIAT:w" s="T130">ere</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_225" n="HIAT:w" s="T131">kɨːlɨ</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_228" n="HIAT:w" s="T132">körön</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_231" n="HIAT:w" s="T133">ke</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_234" n="sc" s="T136">
               <ts e="T141" id="Seg_236" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_238" n="HIAT:w" s="T136">De</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_241" n="HIAT:w" s="T137">üs</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_244" n="HIAT:w" s="T138">kɨːlɨ</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_247" n="HIAT:w" s="T139">körön</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_250" n="HIAT:w" s="T140">ke</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T148" id="Seg_253" n="sc" s="T146">
               <ts e="T148" id="Seg_255" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_257" n="HIAT:w" s="T146">De</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_260" n="HIAT:w" s="T147">heee</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T168" id="Seg_263" n="sc" s="T157">
               <ts e="T160" id="Seg_265" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_267" n="HIAT:w" s="T157">Dʼi͡etiger</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_270" n="HIAT:w" s="T158">kelte</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_273" n="HIAT:w" s="T159">du͡o</ts>
                  <nts id="Seg_274" n="HIAT:ip">?</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_277" n="HIAT:u" s="T160">
                  <ts e="T214" id="Seg_279" n="HIAT:w" s="T160">Onton</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_282" n="HIAT:w" s="T214">ke</ts>
                  <nts id="Seg_283" n="HIAT:ip">,</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_286" n="HIAT:w" s="T215">en</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_289" n="HIAT:w" s="T161">ke</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_292" n="HIAT:w" s="T162">bardɨŋ</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_295" n="HIAT:w" s="T163">hurka</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_298" n="HIAT:w" s="T164">bardɨŋ</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_301" n="HIAT:w" s="T165">onton</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_304" n="HIAT:w" s="T166">iher</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_307" n="HIAT:w" s="T167">buranɨnan</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T176" id="Seg_310" n="sc" s="T171">
               <ts e="T176" id="Seg_312" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_314" n="HIAT:w" s="T171">De</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_317" n="HIAT:w" s="T172">üčügejdik</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_320" n="HIAT:w" s="T173">haŋar</ts>
                  <nts id="Seg_321" n="HIAT:ip">,</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_324" n="HIAT:w" s="T174">Sɨndaːssko</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_327" n="HIAT:w" s="T175">kiːreːribit</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T185" id="Seg_330" n="sc" s="T183">
               <ts e="T185" id="Seg_332" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_334" n="HIAT:w" s="T183">Kanna</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_337" n="HIAT:w" s="T184">bartɨgɨtɨj</ts>
                  <nts id="Seg_338" n="HIAT:ip">?</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T189" id="Seg_340" n="sc" s="T187">
               <ts e="T189" id="Seg_342" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_344" n="HIAT:w" s="T187">Tugu</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_347" n="HIAT:w" s="T188">egeltigitij</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T192" id="Seg_350" n="sc" s="T190">
               <ts e="T192" id="Seg_352" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_354" n="HIAT:w" s="T190">Egeltigit</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_357" n="HIAT:w" s="T191">du͡o</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T199" id="Seg_360" n="sc" s="T195">
               <ts e="T199" id="Seg_362" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_364" n="HIAT:w" s="T195">En</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_367" n="HIAT:w" s="T196">tugu</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_370" n="HIAT:w" s="T197">gɨntɨgɨnɨj</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_373" n="HIAT:w" s="T198">ke</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T203" id="Seg_376" n="sc" s="T201">
               <ts e="T203" id="Seg_378" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_380" n="HIAT:w" s="T201">Onton</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_383" n="HIAT:w" s="T202">Hɨndaːsskajga</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T208" id="Seg_386" n="sc" s="T205">
               <ts e="T207" id="Seg_388" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_390" n="HIAT:w" s="T205">Elete</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_393" n="HIAT:w" s="T206">du͡o</ts>
                  <nts id="Seg_394" n="HIAT:ip">.</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_397" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_399" n="HIAT:w" s="T207">Vse</ts>
                  <nts id="Seg_400" n="HIAT:ip">.</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-AnIM">
            <ts e="T10" id="Seg_402" n="sc" s="T0">
               <ts e="T1" id="Seg_404" n="e" s="T0">Meːne </ts>
               <ts e="T2" id="Seg_406" n="e" s="T1">kepsi͡eŋ </ts>
               <ts e="T3" id="Seg_408" n="e" s="T2">ös </ts>
               <ts e="T4" id="Seg_410" n="e" s="T3">kepsi͡eŋ </ts>
               <ts e="T5" id="Seg_412" n="e" s="T4">meːne. </ts>
               <ts e="T6" id="Seg_414" n="e" s="T5">Bihi͡eke </ts>
               <ts e="T7" id="Seg_416" n="e" s="T6">kepsiːgin </ts>
               <ts e="T8" id="Seg_418" n="e" s="T7">meːne </ts>
               <ts e="T9" id="Seg_420" n="e" s="T8">haːppakka </ts>
               <ts e="T90" id="Seg_422" n="e" s="T9">kimneːbekkegin, </ts>
               <ts e="T194" id="Seg_424" n="e" s="T90">vsjo eto ((…)) a </ts>
               <ts e="T211" id="Seg_426" n="e" s="T194">dʼe </ts>
               <ts e="T10" id="Seg_428" n="e" s="T211">kepseː. </ts>
            </ts>
            <ts e="T24" id="Seg_429" n="sc" s="T19">
               <ts e="T20" id="Seg_431" n="e" s="T19">Kimi </ts>
               <ts e="T21" id="Seg_433" n="e" s="T20">gɨtta </ts>
               <ts e="T22" id="Seg_435" n="e" s="T21">baragɨn </ts>
               <ts e="T23" id="Seg_437" n="e" s="T22">ke </ts>
               <ts e="T24" id="Seg_439" n="e" s="T23">tɨ͡aga. </ts>
            </ts>
            <ts e="T29" id="Seg_440" n="sc" s="T27">
               <ts e="T28" id="Seg_442" n="e" s="T27">Heee </ts>
               <ts e="T29" id="Seg_444" n="e" s="T28">de. </ts>
            </ts>
            <ts e="T41" id="Seg_445" n="sc" s="T39">
               <ts e="T40" id="Seg_447" n="e" s="T39">Körtügüt </ts>
               <ts e="T41" id="Seg_449" n="e" s="T40">du͡o. </ts>
            </ts>
            <ts e="T56" id="Seg_450" n="sc" s="T53">
               <ts e="T54" id="Seg_452" n="e" s="T53">Heee </ts>
               <ts e="T55" id="Seg_454" n="e" s="T54">onton </ts>
               <ts e="T56" id="Seg_456" n="e" s="T55">ke. </ts>
            </ts>
            <ts e="T77" id="Seg_457" n="sc" s="T73">
               <ts e="T74" id="Seg_459" n="e" s="T73">Kanna </ts>
               <ts e="T75" id="Seg_461" n="e" s="T74">tiːjtigitij </ts>
               <ts e="T76" id="Seg_463" n="e" s="T75">bartɨgɨtɨj </ts>
               <ts e="T77" id="Seg_465" n="e" s="T76">ke. </ts>
            </ts>
            <ts e="T83" id="Seg_466" n="sc" s="T78">
               <ts e="T79" id="Seg_468" n="e" s="T78">Heː, </ts>
               <ts e="T80" id="Seg_470" n="e" s="T79">kimneːk </ts>
               <ts e="T81" id="Seg_472" n="e" s="T80">baːllar </ts>
               <ts e="T82" id="Seg_474" n="e" s="T81">ke </ts>
               <ts e="T83" id="Seg_476" n="e" s="T82">tɨ͡aga. </ts>
            </ts>
            <ts e="T213" id="Seg_477" n="sc" s="T212">
               <ts e="T213" id="Seg_479" n="e" s="T212">Natalaːk. </ts>
            </ts>
            <ts e="T94" id="Seg_480" n="sc" s="T91">
               <ts e="T92" id="Seg_482" n="e" s="T91">Hu͡ok </ts>
               <ts e="T93" id="Seg_484" n="e" s="T92">etiler </ts>
               <ts e="T94" id="Seg_486" n="e" s="T93">Natalaːk. </ts>
            </ts>
            <ts e="T111" id="Seg_487" n="sc" s="T109">
               <ts e="T110" id="Seg_489" n="e" s="T109">Ilim </ts>
               <ts e="T111" id="Seg_491" n="e" s="T110">körüne. </ts>
            </ts>
            <ts e="T121" id="Seg_492" n="sc" s="T115">
               <ts e="T116" id="Seg_494" n="e" s="T115">Onton </ts>
               <ts e="T117" id="Seg_496" n="e" s="T116">ke </ts>
               <ts e="T118" id="Seg_498" n="e" s="T117">Diːma </ts>
               <ts e="T119" id="Seg_500" n="e" s="T118">ölörtö </ts>
               <ts e="T120" id="Seg_502" n="e" s="T119">du͡o </ts>
               <ts e="T121" id="Seg_504" n="e" s="T120">kɨːlɨ. </ts>
            </ts>
            <ts e="T128" id="Seg_505" n="sc" s="T124">
               <ts e="T125" id="Seg_507" n="e" s="T124">Ölörbüte </ts>
               <ts e="T126" id="Seg_509" n="e" s="T125">di͡en </ts>
               <ts e="T127" id="Seg_511" n="e" s="T126">du </ts>
               <ts e="T128" id="Seg_513" n="e" s="T127">kas. </ts>
            </ts>
            <ts e="T134" id="Seg_514" n="sc" s="T129">
               <ts e="T130" id="Seg_516" n="e" s="T129">Kas </ts>
               <ts e="T131" id="Seg_518" n="e" s="T130">ere </ts>
               <ts e="T132" id="Seg_520" n="e" s="T131">kɨːlɨ </ts>
               <ts e="T133" id="Seg_522" n="e" s="T132">körön </ts>
               <ts e="T134" id="Seg_524" n="e" s="T133">ke. </ts>
            </ts>
            <ts e="T141" id="Seg_525" n="sc" s="T136">
               <ts e="T137" id="Seg_527" n="e" s="T136">De </ts>
               <ts e="T138" id="Seg_529" n="e" s="T137">üs </ts>
               <ts e="T139" id="Seg_531" n="e" s="T138">kɨːlɨ </ts>
               <ts e="T140" id="Seg_533" n="e" s="T139">körön </ts>
               <ts e="T141" id="Seg_535" n="e" s="T140">ke. </ts>
            </ts>
            <ts e="T148" id="Seg_536" n="sc" s="T146">
               <ts e="T147" id="Seg_538" n="e" s="T146">De </ts>
               <ts e="T148" id="Seg_540" n="e" s="T147">heee. </ts>
            </ts>
            <ts e="T168" id="Seg_541" n="sc" s="T157">
               <ts e="T158" id="Seg_543" n="e" s="T157">Dʼi͡etiger </ts>
               <ts e="T159" id="Seg_545" n="e" s="T158">kelte </ts>
               <ts e="T160" id="Seg_547" n="e" s="T159">du͡o? </ts>
               <ts e="T214" id="Seg_549" n="e" s="T160">Onton </ts>
               <ts e="T215" id="Seg_551" n="e" s="T214">ke, </ts>
               <ts e="T161" id="Seg_553" n="e" s="T215">en </ts>
               <ts e="T162" id="Seg_555" n="e" s="T161">ke </ts>
               <ts e="T163" id="Seg_557" n="e" s="T162">bardɨŋ </ts>
               <ts e="T164" id="Seg_559" n="e" s="T163">hurka </ts>
               <ts e="T165" id="Seg_561" n="e" s="T164">bardɨŋ </ts>
               <ts e="T166" id="Seg_563" n="e" s="T165">onton </ts>
               <ts e="T167" id="Seg_565" n="e" s="T166">iher </ts>
               <ts e="T168" id="Seg_567" n="e" s="T167">buranɨnan. </ts>
            </ts>
            <ts e="T176" id="Seg_568" n="sc" s="T171">
               <ts e="T172" id="Seg_570" n="e" s="T171">De </ts>
               <ts e="T173" id="Seg_572" n="e" s="T172">üčügejdik </ts>
               <ts e="T174" id="Seg_574" n="e" s="T173">haŋar, </ts>
               <ts e="T175" id="Seg_576" n="e" s="T174">Sɨndaːssko </ts>
               <ts e="T176" id="Seg_578" n="e" s="T175">kiːreːribit. </ts>
            </ts>
            <ts e="T185" id="Seg_579" n="sc" s="T183">
               <ts e="T184" id="Seg_581" n="e" s="T183">Kanna </ts>
               <ts e="T185" id="Seg_583" n="e" s="T184">bartɨgɨtɨj? </ts>
            </ts>
            <ts e="T189" id="Seg_584" n="sc" s="T187">
               <ts e="T188" id="Seg_586" n="e" s="T187">Tugu </ts>
               <ts e="T189" id="Seg_588" n="e" s="T188">egeltigitij. </ts>
            </ts>
            <ts e="T192" id="Seg_589" n="sc" s="T190">
               <ts e="T191" id="Seg_591" n="e" s="T190">Egeltigit </ts>
               <ts e="T192" id="Seg_593" n="e" s="T191">du͡o. </ts>
            </ts>
            <ts e="T199" id="Seg_594" n="sc" s="T195">
               <ts e="T196" id="Seg_596" n="e" s="T195">En </ts>
               <ts e="T197" id="Seg_598" n="e" s="T196">tugu </ts>
               <ts e="T198" id="Seg_600" n="e" s="T197">gɨntɨgɨnɨj </ts>
               <ts e="T199" id="Seg_602" n="e" s="T198">ke. </ts>
            </ts>
            <ts e="T203" id="Seg_603" n="sc" s="T201">
               <ts e="T202" id="Seg_605" n="e" s="T201">Onton </ts>
               <ts e="T203" id="Seg_607" n="e" s="T202">Hɨndaːsskajga. </ts>
            </ts>
            <ts e="T208" id="Seg_608" n="sc" s="T205">
               <ts e="T206" id="Seg_610" n="e" s="T205">Elete </ts>
               <ts e="T207" id="Seg_612" n="e" s="T206">du͡o. </ts>
               <ts e="T208" id="Seg_614" n="e" s="T207">Vse. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-AnIM">
            <ta e="T5" id="Seg_615" s="T0">AnIM_AnMSp_2009_Holiday_conv.AnIM.001 (001.001)</ta>
            <ta e="T10" id="Seg_616" s="T5">AnIM_AnMSp_2009_Holiday_conv.AnIM.002 (001.002)</ta>
            <ta e="T24" id="Seg_617" s="T19">AnIM_AnMSp_2009_Holiday_conv.AnIM.003 (001.005)</ta>
            <ta e="T29" id="Seg_618" s="T27">AnIM_AnMSp_2009_Holiday_conv.AnIM.004 (001.007)</ta>
            <ta e="T41" id="Seg_619" s="T39">AnIM_AnMSp_2009_Holiday_conv.AnIM.005 (001.010)</ta>
            <ta e="T56" id="Seg_620" s="T53">AnIM_AnMSp_2009_Holiday_conv.AnIM.006 (001.013)</ta>
            <ta e="T77" id="Seg_621" s="T73">AnIM_AnMSp_2009_Holiday_conv.AnIM.007 (001.018)</ta>
            <ta e="T83" id="Seg_622" s="T78">AnIM_AnMSp_2009_Holiday_conv.AnIM.008 (001.021)</ta>
            <ta e="T213" id="Seg_623" s="T212">AnIM_AnMSp_2009_Holiday_conv.AnIM.009 (001.021)</ta>
            <ta e="T94" id="Seg_624" s="T91">AnIM_AnMSp_2009_Holiday_conv.AnIM.010 (001.024)</ta>
            <ta e="T111" id="Seg_625" s="T109">AnIM_AnMSp_2009_Holiday_conv.AnIM.011 (001.028)</ta>
            <ta e="T121" id="Seg_626" s="T115">AnIM_AnMSp_2009_Holiday_conv.AnIM.012 (001.030)</ta>
            <ta e="T128" id="Seg_627" s="T124">AnIM_AnMSp_2009_Holiday_conv.AnIM.013 (001.032)</ta>
            <ta e="T134" id="Seg_628" s="T129">AnIM_AnMSp_2009_Holiday_conv.AnIM.014 (001.034)</ta>
            <ta e="T141" id="Seg_629" s="T136">AnIM_AnMSp_2009_Holiday_conv.AnIM.015 (001.036)</ta>
            <ta e="T148" id="Seg_630" s="T146">AnIM_AnMSp_2009_Holiday_conv.AnIM.016 (001.038)</ta>
            <ta e="T160" id="Seg_631" s="T157">AnIM_AnMSp_2009_Holiday_conv.AnIM.017 (001.041)</ta>
            <ta e="T168" id="Seg_632" s="T160">AnIM_AnMSp_2009_Holiday_conv.AnIM.018 (001.042)</ta>
            <ta e="T176" id="Seg_633" s="T171">AnIM_AnMSp_2009_Holiday_conv.AnIM.019 (001.044)</ta>
            <ta e="T185" id="Seg_634" s="T183">AnIM_AnMSp_2009_Holiday_conv.AnIM.020 (001.046)</ta>
            <ta e="T189" id="Seg_635" s="T187">AnIM_AnMSp_2009_Holiday_conv.AnIM.021 (001.048)</ta>
            <ta e="T192" id="Seg_636" s="T190">AnIM_AnMSp_2009_Holiday_conv.AnIM.022 (001.050)</ta>
            <ta e="T199" id="Seg_637" s="T195">AnIM_AnMSp_2009_Holiday_conv.AnIM.023 (001.052)</ta>
            <ta e="T203" id="Seg_638" s="T201">AnIM_AnMSp_2009_Holiday_conv.AnIM.024 (001.054)</ta>
            <ta e="T207" id="Seg_639" s="T205">AnIM_AnMSp_2009_Holiday_conv.AnIM.025 (001.056)</ta>
            <ta e="T208" id="Seg_640" s="T207">AnIM_AnMSp_2009_Holiday_conv.AnIM.026 (001.057)</ta>
         </annotation>
         <annotation name="st" tierref="st-AnIM" />
         <annotation name="ts" tierref="ts-AnIM">
            <ta e="T5" id="Seg_641" s="T0">Meːne kepsi͡eŋ ös kepsi͡eŋ meːne. </ta>
            <ta e="T10" id="Seg_642" s="T5">Bihi͡eke kepsiːgin meːne haːppakka kimneːbekkegin, vsjo eto ((…)) a dʼe kepseː.</ta>
            <ta e="T24" id="Seg_643" s="T19">Kimi gɨtta baragɨn ke tɨ͡aga. </ta>
            <ta e="T29" id="Seg_644" s="T27">Heee de. </ta>
            <ta e="T41" id="Seg_645" s="T39">Körtügüt du͡o. </ta>
            <ta e="T56" id="Seg_646" s="T53">Heee onton ke? </ta>
            <ta e="T77" id="Seg_647" s="T73">Kanna tiːjtigitij bartɨgɨtɨj ke. </ta>
            <ta e="T83" id="Seg_648" s="T78">Heː, kimneːk baːllar ke tɨ͡aga. </ta>
            <ta e="T213" id="Seg_649" s="T212">Natalaːk. </ta>
            <ta e="T94" id="Seg_650" s="T91">Hu͡ok etiler Natalaːk. </ta>
            <ta e="T111" id="Seg_651" s="T109">Ilim körüne. </ta>
            <ta e="T121" id="Seg_652" s="T115">Onton ke Diːma ölörtö du͡o kɨːlɨ. </ta>
            <ta e="T128" id="Seg_653" s="T124">Ölörbüte di͡en du kas. </ta>
            <ta e="T134" id="Seg_654" s="T129">Kas ere kɨːlɨ körön ke. </ta>
            <ta e="T141" id="Seg_655" s="T136">De üs kɨːlɨ körön ke. </ta>
            <ta e="T148" id="Seg_656" s="T146">De heee. </ta>
            <ta e="T160" id="Seg_657" s="T157">Dʼi͡etiger kelte du͡o? </ta>
            <ta e="T168" id="Seg_658" s="T160">Onton ke, en ke bardɨŋ hurka bardɨŋ onton iher buranɨnan. </ta>
            <ta e="T176" id="Seg_659" s="T171">De üčügejdik haŋar, Sɨndaːssko kiːreːribit. </ta>
            <ta e="T185" id="Seg_660" s="T183">Kanna bartɨgɨtɨj? </ta>
            <ta e="T189" id="Seg_661" s="T187">Tugu egeltigitij? </ta>
            <ta e="T192" id="Seg_662" s="T190">Egeltigit du͡o? </ta>
            <ta e="T199" id="Seg_663" s="T195">En tugu gɨntɨgɨnɨj ke. </ta>
            <ta e="T203" id="Seg_664" s="T201">Onton Hɨndaːsskajga. </ta>
            <ta e="T207" id="Seg_665" s="T205">Elete du͡o? </ta>
            <ta e="T208" id="Seg_666" s="T207">Vse. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-AnIM">
            <ta e="T1" id="Seg_667" s="T0">meːne</ta>
            <ta e="T2" id="Seg_668" s="T1">keps-i͡e-ŋ</ta>
            <ta e="T3" id="Seg_669" s="T2">ös</ta>
            <ta e="T4" id="Seg_670" s="T3">keps-i͡e-ŋ</ta>
            <ta e="T5" id="Seg_671" s="T4">meːne</ta>
            <ta e="T6" id="Seg_672" s="T5">bihi͡e-ke</ta>
            <ta e="T7" id="Seg_673" s="T6">keps-iː-gin</ta>
            <ta e="T8" id="Seg_674" s="T7">meːne</ta>
            <ta e="T9" id="Seg_675" s="T8">haːp-pakka</ta>
            <ta e="T90" id="Seg_676" s="T9">kim-neː-bekke-gin</ta>
            <ta e="T211" id="Seg_677" s="T194">dʼe</ta>
            <ta e="T10" id="Seg_678" s="T211">kepseː</ta>
            <ta e="T20" id="Seg_679" s="T19">kim-i</ta>
            <ta e="T21" id="Seg_680" s="T20">gɨtta</ta>
            <ta e="T22" id="Seg_681" s="T21">bar-a-gɨn</ta>
            <ta e="T23" id="Seg_682" s="T22">ke</ta>
            <ta e="T24" id="Seg_683" s="T23">tɨ͡a-ga</ta>
            <ta e="T28" id="Seg_684" s="T27">heː</ta>
            <ta e="T29" id="Seg_685" s="T28">de</ta>
            <ta e="T40" id="Seg_686" s="T39">kör-tü-güt</ta>
            <ta e="T41" id="Seg_687" s="T40">du͡o</ta>
            <ta e="T54" id="Seg_688" s="T53">heː</ta>
            <ta e="T55" id="Seg_689" s="T54">onton</ta>
            <ta e="T56" id="Seg_690" s="T55">ke</ta>
            <ta e="T74" id="Seg_691" s="T73">kanna</ta>
            <ta e="T75" id="Seg_692" s="T74">tiːj-ti-git=ij</ta>
            <ta e="T76" id="Seg_693" s="T75">bar-tɨ-gɨt=ɨj</ta>
            <ta e="T77" id="Seg_694" s="T76">ke</ta>
            <ta e="T79" id="Seg_695" s="T78">heː</ta>
            <ta e="T80" id="Seg_696" s="T79">kim-neːk</ta>
            <ta e="T81" id="Seg_697" s="T80">baːl-lar</ta>
            <ta e="T82" id="Seg_698" s="T81">ke</ta>
            <ta e="T83" id="Seg_699" s="T82">tɨ͡a-ga</ta>
            <ta e="T213" id="Seg_700" s="T212">Nata-laːk</ta>
            <ta e="T92" id="Seg_701" s="T91">hu͡ok</ta>
            <ta e="T93" id="Seg_702" s="T92">e-ti-ler</ta>
            <ta e="T94" id="Seg_703" s="T93">Nata-laːk</ta>
            <ta e="T110" id="Seg_704" s="T109">ilim</ta>
            <ta e="T111" id="Seg_705" s="T110">kör-ü-n-e</ta>
            <ta e="T116" id="Seg_706" s="T115">onton</ta>
            <ta e="T117" id="Seg_707" s="T116">ke</ta>
            <ta e="T118" id="Seg_708" s="T117">Diːma</ta>
            <ta e="T119" id="Seg_709" s="T118">ölör-t-ö</ta>
            <ta e="T120" id="Seg_710" s="T119">du͡o</ta>
            <ta e="T121" id="Seg_711" s="T120">kɨːl-ɨ</ta>
            <ta e="T125" id="Seg_712" s="T124">ölör-büt-e</ta>
            <ta e="T126" id="Seg_713" s="T125">di͡e-n</ta>
            <ta e="T127" id="Seg_714" s="T126">du</ta>
            <ta e="T128" id="Seg_715" s="T127">kas</ta>
            <ta e="T130" id="Seg_716" s="T129">kas</ta>
            <ta e="T131" id="Seg_717" s="T130">ere</ta>
            <ta e="T132" id="Seg_718" s="T131">kɨːl-ɨ</ta>
            <ta e="T133" id="Seg_719" s="T132">kör-ön</ta>
            <ta e="T134" id="Seg_720" s="T133">ke</ta>
            <ta e="T137" id="Seg_721" s="T136">de</ta>
            <ta e="T138" id="Seg_722" s="T137">üs</ta>
            <ta e="T139" id="Seg_723" s="T138">kɨːl-ɨ</ta>
            <ta e="T140" id="Seg_724" s="T139">kör-ön</ta>
            <ta e="T141" id="Seg_725" s="T140">ke</ta>
            <ta e="T147" id="Seg_726" s="T146">de</ta>
            <ta e="T148" id="Seg_727" s="T147">heː</ta>
            <ta e="T158" id="Seg_728" s="T157">dʼi͡e-ti-ger</ta>
            <ta e="T159" id="Seg_729" s="T158">kel-t-e</ta>
            <ta e="T160" id="Seg_730" s="T159">du͡o</ta>
            <ta e="T214" id="Seg_731" s="T160">onton</ta>
            <ta e="T215" id="Seg_732" s="T214">ke</ta>
            <ta e="T161" id="Seg_733" s="T215">en</ta>
            <ta e="T162" id="Seg_734" s="T161">ke</ta>
            <ta e="T163" id="Seg_735" s="T162">bar-dɨ-ŋ</ta>
            <ta e="T164" id="Seg_736" s="T163">hur-ka</ta>
            <ta e="T165" id="Seg_737" s="T164">bar-dɨ-ŋ</ta>
            <ta e="T166" id="Seg_738" s="T165">onton</ta>
            <ta e="T167" id="Seg_739" s="T166">ih-er</ta>
            <ta e="T168" id="Seg_740" s="T167">buran-ɨ-nan</ta>
            <ta e="T172" id="Seg_741" s="T171">de</ta>
            <ta e="T173" id="Seg_742" s="T172">üčügej-dik</ta>
            <ta e="T174" id="Seg_743" s="T173">haŋar</ta>
            <ta e="T175" id="Seg_744" s="T174">Sɨndaːssko</ta>
            <ta e="T176" id="Seg_745" s="T175">kiːr-eːri-bit</ta>
            <ta e="T184" id="Seg_746" s="T183">kanna</ta>
            <ta e="T185" id="Seg_747" s="T184">bar-tɨ-gɨt=ɨj</ta>
            <ta e="T188" id="Seg_748" s="T187">tug-u</ta>
            <ta e="T189" id="Seg_749" s="T188">egel-ti-git=ij</ta>
            <ta e="T191" id="Seg_750" s="T190">egel-ti-git</ta>
            <ta e="T192" id="Seg_751" s="T191">du͡o</ta>
            <ta e="T196" id="Seg_752" s="T195">en</ta>
            <ta e="T197" id="Seg_753" s="T196">tug-u</ta>
            <ta e="T198" id="Seg_754" s="T197">gɨn-tɨ-gɨn=ɨj</ta>
            <ta e="T199" id="Seg_755" s="T198">ke</ta>
            <ta e="T202" id="Seg_756" s="T201">onton</ta>
            <ta e="T203" id="Seg_757" s="T202">Hɨndaːsskaj-ga</ta>
            <ta e="T206" id="Seg_758" s="T205">ele-te</ta>
            <ta e="T207" id="Seg_759" s="T206">du͡o</ta>
         </annotation>
         <annotation name="mp" tierref="mp-AnIM">
            <ta e="T1" id="Seg_760" s="T0">meːne</ta>
            <ta e="T2" id="Seg_761" s="T1">kepseː-IAK-ŋ</ta>
            <ta e="T3" id="Seg_762" s="T2">ös</ta>
            <ta e="T4" id="Seg_763" s="T3">kepseː-IAK-ŋ</ta>
            <ta e="T5" id="Seg_764" s="T4">meːne</ta>
            <ta e="T6" id="Seg_765" s="T5">bihigi-GA</ta>
            <ta e="T7" id="Seg_766" s="T6">kepseː-A-GIn</ta>
            <ta e="T8" id="Seg_767" s="T7">meːne</ta>
            <ta e="T9" id="Seg_768" s="T8">haːt-BAkkA</ta>
            <ta e="T90" id="Seg_769" s="T9">kim-LAː-BAkkA-GIn</ta>
            <ta e="T211" id="Seg_770" s="T194">dʼe</ta>
            <ta e="T10" id="Seg_771" s="T211">kepseː</ta>
            <ta e="T20" id="Seg_772" s="T19">kim-nI</ta>
            <ta e="T21" id="Seg_773" s="T20">kɨtta</ta>
            <ta e="T22" id="Seg_774" s="T21">bar-A-GIn</ta>
            <ta e="T23" id="Seg_775" s="T22">ka</ta>
            <ta e="T24" id="Seg_776" s="T23">tɨ͡a-GA</ta>
            <ta e="T28" id="Seg_777" s="T27">eː</ta>
            <ta e="T29" id="Seg_778" s="T28">dʼe</ta>
            <ta e="T40" id="Seg_779" s="T39">kör-TI-GIt</ta>
            <ta e="T41" id="Seg_780" s="T40">du͡o</ta>
            <ta e="T54" id="Seg_781" s="T53">eː</ta>
            <ta e="T55" id="Seg_782" s="T54">onton</ta>
            <ta e="T56" id="Seg_783" s="T55">ka</ta>
            <ta e="T74" id="Seg_784" s="T73">kanna</ta>
            <ta e="T75" id="Seg_785" s="T74">tij-TI-GIt=Ij</ta>
            <ta e="T76" id="Seg_786" s="T75">bar-TI-GIt=Ij</ta>
            <ta e="T77" id="Seg_787" s="T76">ka</ta>
            <ta e="T79" id="Seg_788" s="T78">eː</ta>
            <ta e="T80" id="Seg_789" s="T79">kim-LAːK</ta>
            <ta e="T81" id="Seg_790" s="T80">baːr-LAr</ta>
            <ta e="T82" id="Seg_791" s="T81">ka</ta>
            <ta e="T83" id="Seg_792" s="T82">tɨ͡a-GA</ta>
            <ta e="T213" id="Seg_793" s="T212">Nata-LAːk</ta>
            <ta e="T92" id="Seg_794" s="T91">hu͡ok</ta>
            <ta e="T93" id="Seg_795" s="T92">e-TI-LAr</ta>
            <ta e="T94" id="Seg_796" s="T93">Nata-LAːK</ta>
            <ta e="T110" id="Seg_797" s="T109">ilim</ta>
            <ta e="T111" id="Seg_798" s="T110">kör-I-n-A</ta>
            <ta e="T116" id="Seg_799" s="T115">onton</ta>
            <ta e="T117" id="Seg_800" s="T116">ka</ta>
            <ta e="T118" id="Seg_801" s="T117">Diːma</ta>
            <ta e="T119" id="Seg_802" s="T118">ölör-BIT-tA</ta>
            <ta e="T120" id="Seg_803" s="T119">du͡o</ta>
            <ta e="T121" id="Seg_804" s="T120">kɨːl-nI</ta>
            <ta e="T125" id="Seg_805" s="T124">ölör-BIT-tA</ta>
            <ta e="T126" id="Seg_806" s="T125">di͡e-An</ta>
            <ta e="T127" id="Seg_807" s="T126">du͡o</ta>
            <ta e="T128" id="Seg_808" s="T127">kas</ta>
            <ta e="T130" id="Seg_809" s="T129">kas</ta>
            <ta e="T131" id="Seg_810" s="T130">ere</ta>
            <ta e="T132" id="Seg_811" s="T131">kɨːl-nI</ta>
            <ta e="T133" id="Seg_812" s="T132">kör-An</ta>
            <ta e="T134" id="Seg_813" s="T133">ka</ta>
            <ta e="T137" id="Seg_814" s="T136">dʼe</ta>
            <ta e="T138" id="Seg_815" s="T137">üs</ta>
            <ta e="T139" id="Seg_816" s="T138">kɨːl-nI</ta>
            <ta e="T140" id="Seg_817" s="T139">kör-An</ta>
            <ta e="T141" id="Seg_818" s="T140">ka</ta>
            <ta e="T147" id="Seg_819" s="T146">dʼe</ta>
            <ta e="T148" id="Seg_820" s="T147">eː</ta>
            <ta e="T158" id="Seg_821" s="T157">dʼi͡e-tI-GAr</ta>
            <ta e="T159" id="Seg_822" s="T158">kel-BIT-tA</ta>
            <ta e="T160" id="Seg_823" s="T159">du͡o</ta>
            <ta e="T214" id="Seg_824" s="T160">onton</ta>
            <ta e="T215" id="Seg_825" s="T214">ka</ta>
            <ta e="T161" id="Seg_826" s="T215">en</ta>
            <ta e="T162" id="Seg_827" s="T161">ka</ta>
            <ta e="T163" id="Seg_828" s="T162">bar-TI-ŋ</ta>
            <ta e="T164" id="Seg_829" s="T163">huːrt-GA</ta>
            <ta e="T165" id="Seg_830" s="T164">bar-TI-ŋ</ta>
            <ta e="T166" id="Seg_831" s="T165">onton</ta>
            <ta e="T167" id="Seg_832" s="T166">is-Ar</ta>
            <ta e="T168" id="Seg_833" s="T167">buran-tI-nAn</ta>
            <ta e="T172" id="Seg_834" s="T171">dʼe</ta>
            <ta e="T173" id="Seg_835" s="T172">üčügej-LIk</ta>
            <ta e="T174" id="Seg_836" s="T173">haŋar</ta>
            <ta e="T175" id="Seg_837" s="T174">Hɨndaːska</ta>
            <ta e="T176" id="Seg_838" s="T175">kiːr-AːrI-BIt</ta>
            <ta e="T184" id="Seg_839" s="T183">kanna</ta>
            <ta e="T185" id="Seg_840" s="T184">bar-TI-GIt=Ij</ta>
            <ta e="T188" id="Seg_841" s="T187">tu͡ok-nI</ta>
            <ta e="T189" id="Seg_842" s="T188">egel-TI-GIt=Ij</ta>
            <ta e="T191" id="Seg_843" s="T190">egel-TI-GIt</ta>
            <ta e="T192" id="Seg_844" s="T191">du͡o</ta>
            <ta e="T196" id="Seg_845" s="T195">en</ta>
            <ta e="T197" id="Seg_846" s="T196">tu͡ok-nI</ta>
            <ta e="T198" id="Seg_847" s="T197">gɨn-TI-GIn=Ij</ta>
            <ta e="T199" id="Seg_848" s="T198">ka</ta>
            <ta e="T202" id="Seg_849" s="T201">onton</ta>
            <ta e="T203" id="Seg_850" s="T202">Hɨndaːska-GA</ta>
            <ta e="T206" id="Seg_851" s="T205">ele-tA</ta>
            <ta e="T207" id="Seg_852" s="T206">du͡o</ta>
         </annotation>
         <annotation name="ge" tierref="ge-AnIM">
            <ta e="T1" id="Seg_853" s="T0">simply</ta>
            <ta e="T2" id="Seg_854" s="T1">tell-FUT-2SG</ta>
            <ta e="T3" id="Seg_855" s="T2">story.[NOM]</ta>
            <ta e="T4" id="Seg_856" s="T3">tell-FUT-2SG</ta>
            <ta e="T5" id="Seg_857" s="T4">simply</ta>
            <ta e="T6" id="Seg_858" s="T5">1PL-DAT/LOC</ta>
            <ta e="T7" id="Seg_859" s="T6">tell-PRS-2SG</ta>
            <ta e="T8" id="Seg_860" s="T7">simply</ta>
            <ta e="T9" id="Seg_861" s="T8">feel.ashamed-NEG.CVB.SIM</ta>
            <ta e="T90" id="Seg_862" s="T9">who-VBZ-NEG.CVB.SIM-2SG</ta>
            <ta e="T211" id="Seg_863" s="T194">well</ta>
            <ta e="T10" id="Seg_864" s="T211">tell.[IMP.2SG]</ta>
            <ta e="T20" id="Seg_865" s="T19">who-ACC</ta>
            <ta e="T21" id="Seg_866" s="T20">with</ta>
            <ta e="T22" id="Seg_867" s="T21">go-PRS-2SG</ta>
            <ta e="T23" id="Seg_868" s="T22">well</ta>
            <ta e="T24" id="Seg_869" s="T23">tundra-DAT/LOC</ta>
            <ta e="T28" id="Seg_870" s="T27">AFFIRM</ta>
            <ta e="T29" id="Seg_871" s="T28">well</ta>
            <ta e="T40" id="Seg_872" s="T39">see-PST1-2PL</ta>
            <ta e="T41" id="Seg_873" s="T40">Q</ta>
            <ta e="T54" id="Seg_874" s="T53">AFFIRM</ta>
            <ta e="T55" id="Seg_875" s="T54">then</ta>
            <ta e="T56" id="Seg_876" s="T55">well</ta>
            <ta e="T74" id="Seg_877" s="T73">where</ta>
            <ta e="T75" id="Seg_878" s="T74">reach-PST1-2PL=Q</ta>
            <ta e="T76" id="Seg_879" s="T75">go-PST1-2PL=Q</ta>
            <ta e="T77" id="Seg_880" s="T76">well</ta>
            <ta e="T79" id="Seg_881" s="T78">AFFIRM</ta>
            <ta e="T80" id="Seg_882" s="T79">who-PROPR.[NOM]</ta>
            <ta e="T81" id="Seg_883" s="T80">there.is-3PL</ta>
            <ta e="T82" id="Seg_884" s="T81">well</ta>
            <ta e="T83" id="Seg_885" s="T82">tundra-DAT/LOC</ta>
            <ta e="T213" id="Seg_886" s="T212">Nata-PROPR.[NOM]</ta>
            <ta e="T92" id="Seg_887" s="T91">NEG.EX</ta>
            <ta e="T93" id="Seg_888" s="T92">be-PST1-3PL</ta>
            <ta e="T94" id="Seg_889" s="T93">Nata-PROPR.[NOM]</ta>
            <ta e="T110" id="Seg_890" s="T109">net.[NOM]</ta>
            <ta e="T111" id="Seg_891" s="T110">see-EP-MED-CVB.SIM</ta>
            <ta e="T116" id="Seg_892" s="T115">then</ta>
            <ta e="T117" id="Seg_893" s="T116">well</ta>
            <ta e="T118" id="Seg_894" s="T117">Dima.[NOM]</ta>
            <ta e="T119" id="Seg_895" s="T118">kill-PST2-3SG</ta>
            <ta e="T120" id="Seg_896" s="T119">Q</ta>
            <ta e="T121" id="Seg_897" s="T120">wild.reindeer-ACC</ta>
            <ta e="T125" id="Seg_898" s="T124">kill-PST2-3SG</ta>
            <ta e="T126" id="Seg_899" s="T125">say-CVB.SEQ</ta>
            <ta e="T127" id="Seg_900" s="T126">Q</ta>
            <ta e="T128" id="Seg_901" s="T127">how.much</ta>
            <ta e="T130" id="Seg_902" s="T129">how.much</ta>
            <ta e="T131" id="Seg_903" s="T130">just</ta>
            <ta e="T132" id="Seg_904" s="T131">wild.reindeer-ACC</ta>
            <ta e="T133" id="Seg_905" s="T132">see-CVB.SEQ</ta>
            <ta e="T134" id="Seg_906" s="T133">well</ta>
            <ta e="T137" id="Seg_907" s="T136">well</ta>
            <ta e="T138" id="Seg_908" s="T137">three</ta>
            <ta e="T139" id="Seg_909" s="T138">wild.reindeer-ACC</ta>
            <ta e="T140" id="Seg_910" s="T139">see-CVB.SEQ</ta>
            <ta e="T141" id="Seg_911" s="T140">well</ta>
            <ta e="T147" id="Seg_912" s="T146">well</ta>
            <ta e="T148" id="Seg_913" s="T147">AFFIRM</ta>
            <ta e="T158" id="Seg_914" s="T157">house-3SG-DAT/LOC</ta>
            <ta e="T159" id="Seg_915" s="T158">come-PST2-3SG</ta>
            <ta e="T160" id="Seg_916" s="T159">Q</ta>
            <ta e="T214" id="Seg_917" s="T160">then</ta>
            <ta e="T215" id="Seg_918" s="T214">well</ta>
            <ta e="T161" id="Seg_919" s="T215">2SG.[NOM]</ta>
            <ta e="T162" id="Seg_920" s="T161">well</ta>
            <ta e="T163" id="Seg_921" s="T162">go-PST1-2SG</ta>
            <ta e="T164" id="Seg_922" s="T163">temporary.settlement-DAT/LOC</ta>
            <ta e="T165" id="Seg_923" s="T164">go-PST1-2SG</ta>
            <ta e="T166" id="Seg_924" s="T165">then</ta>
            <ta e="T167" id="Seg_925" s="T166">go-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_926" s="T167">snow.scooter-3SG-INSTR</ta>
            <ta e="T172" id="Seg_927" s="T171">well</ta>
            <ta e="T173" id="Seg_928" s="T172">good-ADVZ</ta>
            <ta e="T174" id="Seg_929" s="T173">speak.[IMP.2SG]</ta>
            <ta e="T175" id="Seg_930" s="T174">Syndassko</ta>
            <ta e="T176" id="Seg_931" s="T175">go.in-CVB.PURP-1PL</ta>
            <ta e="T184" id="Seg_932" s="T183">where</ta>
            <ta e="T185" id="Seg_933" s="T184">go-PST1-2PL=Q</ta>
            <ta e="T188" id="Seg_934" s="T187">what-ACC</ta>
            <ta e="T189" id="Seg_935" s="T188">bring-PST1-2PL=Q</ta>
            <ta e="T191" id="Seg_936" s="T190">bring-PST1-2PL</ta>
            <ta e="T192" id="Seg_937" s="T191">Q</ta>
            <ta e="T196" id="Seg_938" s="T195">2SG.[NOM]</ta>
            <ta e="T197" id="Seg_939" s="T196">what-ACC</ta>
            <ta e="T198" id="Seg_940" s="T197">make-PST1-2SG=Q</ta>
            <ta e="T199" id="Seg_941" s="T198">well</ta>
            <ta e="T202" id="Seg_942" s="T201">then</ta>
            <ta e="T203" id="Seg_943" s="T202">Syndassko-DAT/LOC</ta>
            <ta e="T206" id="Seg_944" s="T205">last-3SG.[NOM]</ta>
            <ta e="T207" id="Seg_945" s="T206">Q</ta>
         </annotation>
         <annotation name="gg" tierref="gg-AnIM">
            <ta e="T1" id="Seg_946" s="T0">einfach</ta>
            <ta e="T2" id="Seg_947" s="T1">erzählen-FUT-2SG</ta>
            <ta e="T3" id="Seg_948" s="T2">Erzählung.[NOM]</ta>
            <ta e="T4" id="Seg_949" s="T3">erzählen-FUT-2SG</ta>
            <ta e="T5" id="Seg_950" s="T4">einfach</ta>
            <ta e="T6" id="Seg_951" s="T5">1PL-DAT/LOC</ta>
            <ta e="T7" id="Seg_952" s="T6">erzählen-PRS-2SG</ta>
            <ta e="T8" id="Seg_953" s="T7">einfach</ta>
            <ta e="T9" id="Seg_954" s="T8">sich.schämen-NEG.CVB.SIM</ta>
            <ta e="T90" id="Seg_955" s="T9">wer-VBZ-NEG.CVB.SIM-2SG</ta>
            <ta e="T211" id="Seg_956" s="T194">doch</ta>
            <ta e="T10" id="Seg_957" s="T211">erzählen.[IMP.2SG]</ta>
            <ta e="T20" id="Seg_958" s="T19">wer-ACC</ta>
            <ta e="T21" id="Seg_959" s="T20">mit</ta>
            <ta e="T22" id="Seg_960" s="T21">gehen-PRS-2SG</ta>
            <ta e="T23" id="Seg_961" s="T22">nun</ta>
            <ta e="T24" id="Seg_962" s="T23">Tundra-DAT/LOC</ta>
            <ta e="T28" id="Seg_963" s="T27">AFFIRM</ta>
            <ta e="T29" id="Seg_964" s="T28">doch</ta>
            <ta e="T40" id="Seg_965" s="T39">sehen-PST1-2PL</ta>
            <ta e="T41" id="Seg_966" s="T40">Q</ta>
            <ta e="T54" id="Seg_967" s="T53">AFFIRM</ta>
            <ta e="T55" id="Seg_968" s="T54">dann</ta>
            <ta e="T56" id="Seg_969" s="T55">nun</ta>
            <ta e="T74" id="Seg_970" s="T73">wo</ta>
            <ta e="T75" id="Seg_971" s="T74">ankommen-PST1-2PL=Q</ta>
            <ta e="T76" id="Seg_972" s="T75">gehen-PST1-2PL=Q</ta>
            <ta e="T77" id="Seg_973" s="T76">nun</ta>
            <ta e="T79" id="Seg_974" s="T78">AFFIRM</ta>
            <ta e="T80" id="Seg_975" s="T79">wer-PROPR.[NOM]</ta>
            <ta e="T81" id="Seg_976" s="T80">es.gibt-3PL</ta>
            <ta e="T82" id="Seg_977" s="T81">nun</ta>
            <ta e="T83" id="Seg_978" s="T82">Tundra-DAT/LOC</ta>
            <ta e="T213" id="Seg_979" s="T212">Nata-PROPR.[NOM]</ta>
            <ta e="T92" id="Seg_980" s="T91">NEG.EX</ta>
            <ta e="T93" id="Seg_981" s="T92">sein-PST1-3PL</ta>
            <ta e="T94" id="Seg_982" s="T93">Nata-PROPR.[NOM]</ta>
            <ta e="T110" id="Seg_983" s="T109">Netz.[NOM]</ta>
            <ta e="T111" id="Seg_984" s="T110">sehen-EP-MED-CVB.SIM</ta>
            <ta e="T116" id="Seg_985" s="T115">dann</ta>
            <ta e="T117" id="Seg_986" s="T116">nun</ta>
            <ta e="T118" id="Seg_987" s="T117">Dima.[NOM]</ta>
            <ta e="T119" id="Seg_988" s="T118">töten-PST2-3SG</ta>
            <ta e="T120" id="Seg_989" s="T119">Q</ta>
            <ta e="T121" id="Seg_990" s="T120">wildes.Rentier-ACC</ta>
            <ta e="T125" id="Seg_991" s="T124">töten-PST2-3SG</ta>
            <ta e="T126" id="Seg_992" s="T125">sagen-CVB.SEQ</ta>
            <ta e="T127" id="Seg_993" s="T126">Q</ta>
            <ta e="T128" id="Seg_994" s="T127">wie.viel</ta>
            <ta e="T130" id="Seg_995" s="T129">wie.viel</ta>
            <ta e="T131" id="Seg_996" s="T130">nur</ta>
            <ta e="T132" id="Seg_997" s="T131">wildes.Rentier-ACC</ta>
            <ta e="T133" id="Seg_998" s="T132">sehen-CVB.SEQ</ta>
            <ta e="T134" id="Seg_999" s="T133">nun</ta>
            <ta e="T137" id="Seg_1000" s="T136">doch</ta>
            <ta e="T138" id="Seg_1001" s="T137">drei</ta>
            <ta e="T139" id="Seg_1002" s="T138">wildes.Rentier-ACC</ta>
            <ta e="T140" id="Seg_1003" s="T139">sehen-CVB.SEQ</ta>
            <ta e="T141" id="Seg_1004" s="T140">nun</ta>
            <ta e="T147" id="Seg_1005" s="T146">doch</ta>
            <ta e="T148" id="Seg_1006" s="T147">AFFIRM</ta>
            <ta e="T158" id="Seg_1007" s="T157">Haus-3SG-DAT/LOC</ta>
            <ta e="T159" id="Seg_1008" s="T158">kommen-PST2-3SG</ta>
            <ta e="T160" id="Seg_1009" s="T159">Q</ta>
            <ta e="T214" id="Seg_1010" s="T160">dann</ta>
            <ta e="T215" id="Seg_1011" s="T214">nun</ta>
            <ta e="T161" id="Seg_1012" s="T215">2SG.[NOM]</ta>
            <ta e="T162" id="Seg_1013" s="T161">nun</ta>
            <ta e="T163" id="Seg_1014" s="T162">gehen-PST1-2SG</ta>
            <ta e="T164" id="Seg_1015" s="T163">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T165" id="Seg_1016" s="T164">gehen-PST1-2SG</ta>
            <ta e="T166" id="Seg_1017" s="T165">dann</ta>
            <ta e="T167" id="Seg_1018" s="T166">gehen-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_1019" s="T167">Schneemobil-3SG-INSTR</ta>
            <ta e="T172" id="Seg_1020" s="T171">doch</ta>
            <ta e="T173" id="Seg_1021" s="T172">gut-ADVZ</ta>
            <ta e="T174" id="Seg_1022" s="T173">sprechen.[IMP.2SG]</ta>
            <ta e="T175" id="Seg_1023" s="T174">Syndassko</ta>
            <ta e="T176" id="Seg_1024" s="T175">hineingehen-CVB.PURP-1PL</ta>
            <ta e="T184" id="Seg_1025" s="T183">wo</ta>
            <ta e="T185" id="Seg_1026" s="T184">gehen-PST1-2PL=Q</ta>
            <ta e="T188" id="Seg_1027" s="T187">was-ACC</ta>
            <ta e="T189" id="Seg_1028" s="T188">bringen-PST1-2PL=Q</ta>
            <ta e="T191" id="Seg_1029" s="T190">bringen-PST1-2PL</ta>
            <ta e="T192" id="Seg_1030" s="T191">Q</ta>
            <ta e="T196" id="Seg_1031" s="T195">2SG.[NOM]</ta>
            <ta e="T197" id="Seg_1032" s="T196">was-ACC</ta>
            <ta e="T198" id="Seg_1033" s="T197">machen-PST1-2SG=Q</ta>
            <ta e="T199" id="Seg_1034" s="T198">nun</ta>
            <ta e="T202" id="Seg_1035" s="T201">dann</ta>
            <ta e="T203" id="Seg_1036" s="T202">Syndassko-DAT/LOC</ta>
            <ta e="T206" id="Seg_1037" s="T205">letzter-3SG.[NOM]</ta>
            <ta e="T207" id="Seg_1038" s="T206">Q</ta>
         </annotation>
         <annotation name="gr" tierref="gr-AnIM">
            <ta e="T1" id="Seg_1039" s="T0">просто</ta>
            <ta e="T2" id="Seg_1040" s="T1">рассказывать-FUT-2SG</ta>
            <ta e="T3" id="Seg_1041" s="T2">рассказ.[NOM]</ta>
            <ta e="T4" id="Seg_1042" s="T3">рассказывать-FUT-2SG</ta>
            <ta e="T5" id="Seg_1043" s="T4">просто</ta>
            <ta e="T6" id="Seg_1044" s="T5">1PL-DAT/LOC</ta>
            <ta e="T7" id="Seg_1045" s="T6">рассказывать-PRS-2SG</ta>
            <ta e="T8" id="Seg_1046" s="T7">просто</ta>
            <ta e="T9" id="Seg_1047" s="T8">стесняться-NEG.CVB.SIM</ta>
            <ta e="T90" id="Seg_1048" s="T9">кто-VBZ-NEG.CVB.SIM-2SG</ta>
            <ta e="T211" id="Seg_1049" s="T194">вот</ta>
            <ta e="T10" id="Seg_1050" s="T211">рассказывать.[IMP.2SG]</ta>
            <ta e="T20" id="Seg_1051" s="T19">кто-ACC</ta>
            <ta e="T21" id="Seg_1052" s="T20">с</ta>
            <ta e="T22" id="Seg_1053" s="T21">идти-PRS-2SG</ta>
            <ta e="T23" id="Seg_1054" s="T22">вот</ta>
            <ta e="T24" id="Seg_1055" s="T23">тундра-DAT/LOC</ta>
            <ta e="T28" id="Seg_1056" s="T27">AFFIRM</ta>
            <ta e="T29" id="Seg_1057" s="T28">вот</ta>
            <ta e="T40" id="Seg_1058" s="T39">видеть-PST1-2PL</ta>
            <ta e="T41" id="Seg_1059" s="T40">Q</ta>
            <ta e="T54" id="Seg_1060" s="T53">AFFIRM</ta>
            <ta e="T55" id="Seg_1061" s="T54">потом</ta>
            <ta e="T56" id="Seg_1062" s="T55">вот</ta>
            <ta e="T74" id="Seg_1063" s="T73">где</ta>
            <ta e="T75" id="Seg_1064" s="T74">доезжать-PST1-2PL=Q</ta>
            <ta e="T76" id="Seg_1065" s="T75">идти-PST1-2PL=Q</ta>
            <ta e="T77" id="Seg_1066" s="T76">вот</ta>
            <ta e="T79" id="Seg_1067" s="T78">AFFIRM</ta>
            <ta e="T80" id="Seg_1068" s="T79">кто-PROPR.[NOM]</ta>
            <ta e="T81" id="Seg_1069" s="T80">есть-3PL</ta>
            <ta e="T82" id="Seg_1070" s="T81">вот</ta>
            <ta e="T83" id="Seg_1071" s="T82">тундра-DAT/LOC</ta>
            <ta e="T213" id="Seg_1072" s="T212">Ната-PROPR.[NOM]</ta>
            <ta e="T92" id="Seg_1073" s="T91">NEG.EX</ta>
            <ta e="T93" id="Seg_1074" s="T92">быть-PST1-3PL</ta>
            <ta e="T94" id="Seg_1075" s="T93">Ната-PROPR.[NOM]</ta>
            <ta e="T110" id="Seg_1076" s="T109">сеть.[NOM]</ta>
            <ta e="T111" id="Seg_1077" s="T110">видеть-EP-MED-CVB.SIM</ta>
            <ta e="T116" id="Seg_1078" s="T115">потом</ta>
            <ta e="T117" id="Seg_1079" s="T116">вот</ta>
            <ta e="T118" id="Seg_1080" s="T117">Дима.[NOM]</ta>
            <ta e="T119" id="Seg_1081" s="T118">убить-PST2-3SG</ta>
            <ta e="T120" id="Seg_1082" s="T119">Q</ta>
            <ta e="T121" id="Seg_1083" s="T120">дикий.олень-ACC</ta>
            <ta e="T125" id="Seg_1084" s="T124">убить-PST2-3SG</ta>
            <ta e="T126" id="Seg_1085" s="T125">говорить-CVB.SEQ</ta>
            <ta e="T127" id="Seg_1086" s="T126">Q</ta>
            <ta e="T128" id="Seg_1087" s="T127">сколько</ta>
            <ta e="T130" id="Seg_1088" s="T129">сколько</ta>
            <ta e="T131" id="Seg_1089" s="T130">только</ta>
            <ta e="T132" id="Seg_1090" s="T131">дикий.олень-ACC</ta>
            <ta e="T133" id="Seg_1091" s="T132">видеть-CVB.SEQ</ta>
            <ta e="T134" id="Seg_1092" s="T133">вот</ta>
            <ta e="T137" id="Seg_1093" s="T136">вот</ta>
            <ta e="T138" id="Seg_1094" s="T137">три</ta>
            <ta e="T139" id="Seg_1095" s="T138">дикий.олень-ACC</ta>
            <ta e="T140" id="Seg_1096" s="T139">видеть-CVB.SEQ</ta>
            <ta e="T141" id="Seg_1097" s="T140">вот</ta>
            <ta e="T147" id="Seg_1098" s="T146">вот</ta>
            <ta e="T148" id="Seg_1099" s="T147">AFFIRM</ta>
            <ta e="T158" id="Seg_1100" s="T157">дом-3SG-DAT/LOC</ta>
            <ta e="T159" id="Seg_1101" s="T158">приходить-PST2-3SG</ta>
            <ta e="T160" id="Seg_1102" s="T159">Q</ta>
            <ta e="T214" id="Seg_1103" s="T160">потом</ta>
            <ta e="T215" id="Seg_1104" s="T214">вот</ta>
            <ta e="T161" id="Seg_1105" s="T215">2SG.[NOM]</ta>
            <ta e="T162" id="Seg_1106" s="T161">вот</ta>
            <ta e="T163" id="Seg_1107" s="T162">идти-PST1-2SG</ta>
            <ta e="T164" id="Seg_1108" s="T163">стоянка-DAT/LOC</ta>
            <ta e="T165" id="Seg_1109" s="T164">идти-PST1-2SG</ta>
            <ta e="T166" id="Seg_1110" s="T165">потом</ta>
            <ta e="T167" id="Seg_1111" s="T166">идти-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_1112" s="T167">буран-3SG-INSTR</ta>
            <ta e="T172" id="Seg_1113" s="T171">вот</ta>
            <ta e="T173" id="Seg_1114" s="T172">хороший-ADVZ</ta>
            <ta e="T174" id="Seg_1115" s="T173">говорить.[IMP.2SG]</ta>
            <ta e="T175" id="Seg_1116" s="T174">Сындасско</ta>
            <ta e="T176" id="Seg_1117" s="T175">входить-CVB.PURP-1PL</ta>
            <ta e="T184" id="Seg_1118" s="T183">где</ta>
            <ta e="T185" id="Seg_1119" s="T184">идти-PST1-2PL=Q</ta>
            <ta e="T188" id="Seg_1120" s="T187">что-ACC</ta>
            <ta e="T189" id="Seg_1121" s="T188">принести-PST1-2PL=Q</ta>
            <ta e="T191" id="Seg_1122" s="T190">принести-PST1-2PL</ta>
            <ta e="T192" id="Seg_1123" s="T191">Q</ta>
            <ta e="T196" id="Seg_1124" s="T195">2SG.[NOM]</ta>
            <ta e="T197" id="Seg_1125" s="T196">что-ACC</ta>
            <ta e="T198" id="Seg_1126" s="T197">делать-PST1-2SG=Q</ta>
            <ta e="T199" id="Seg_1127" s="T198">вот</ta>
            <ta e="T202" id="Seg_1128" s="T201">потом</ta>
            <ta e="T203" id="Seg_1129" s="T202">Сындасско-DAT/LOC</ta>
            <ta e="T206" id="Seg_1130" s="T205">последний-3SG.[NOM]</ta>
            <ta e="T207" id="Seg_1131" s="T206">Q</ta>
         </annotation>
         <annotation name="mc" tierref="mc-AnIM">
            <ta e="T1" id="Seg_1132" s="T0">adv</ta>
            <ta e="T2" id="Seg_1133" s="T1">v-v:tense-v:poss.pn</ta>
            <ta e="T3" id="Seg_1134" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1135" s="T3">v-v:tense-v:poss.pn</ta>
            <ta e="T5" id="Seg_1136" s="T4">adv</ta>
            <ta e="T6" id="Seg_1137" s="T5">pers-pro:case</ta>
            <ta e="T7" id="Seg_1138" s="T6">v-v:tense-v:pred.pn</ta>
            <ta e="T8" id="Seg_1139" s="T7">adv</ta>
            <ta e="T9" id="Seg_1140" s="T8">v-v:cvb</ta>
            <ta e="T90" id="Seg_1141" s="T9">que-que&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T211" id="Seg_1142" s="T194">ptcl</ta>
            <ta e="T10" id="Seg_1143" s="T211">v-v:mood.pn</ta>
            <ta e="T20" id="Seg_1144" s="T19">que-pro:case</ta>
            <ta e="T21" id="Seg_1145" s="T20">post</ta>
            <ta e="T22" id="Seg_1146" s="T21">v-v:tense-v:pred.pn</ta>
            <ta e="T23" id="Seg_1147" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_1148" s="T23">n-n:case</ta>
            <ta e="T28" id="Seg_1149" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1150" s="T28">ptcl</ta>
            <ta e="T40" id="Seg_1151" s="T39">v-v:tense-v:poss.pn</ta>
            <ta e="T41" id="Seg_1152" s="T40">ptcl</ta>
            <ta e="T54" id="Seg_1153" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1154" s="T54">adv</ta>
            <ta e="T56" id="Seg_1155" s="T55">ptcl</ta>
            <ta e="T74" id="Seg_1156" s="T73">que</ta>
            <ta e="T75" id="Seg_1157" s="T74">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T76" id="Seg_1158" s="T75">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T77" id="Seg_1159" s="T76">ptcl</ta>
            <ta e="T79" id="Seg_1160" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1161" s="T79">que-que&gt;adj.[pro:case]</ta>
            <ta e="T81" id="Seg_1162" s="T80">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T82" id="Seg_1163" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_1164" s="T82">n-n:case</ta>
            <ta e="T213" id="Seg_1165" s="T212">propr-n&gt;adj.[n:case]</ta>
            <ta e="T92" id="Seg_1166" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_1167" s="T92">v-v:tense-v:pred.pn</ta>
            <ta e="T94" id="Seg_1168" s="T93">propr-n&gt;adj.[n:case]</ta>
            <ta e="T110" id="Seg_1169" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1170" s="T110">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T116" id="Seg_1171" s="T115">adv</ta>
            <ta e="T117" id="Seg_1172" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_1173" s="T117">propr-n:case</ta>
            <ta e="T119" id="Seg_1174" s="T118">v-v:tense-v:(poss)</ta>
            <ta e="T120" id="Seg_1175" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1176" s="T120">n-n:case</ta>
            <ta e="T125" id="Seg_1177" s="T124">v-v:tense-v:poss.pn</ta>
            <ta e="T126" id="Seg_1178" s="T125">v-v:cvb</ta>
            <ta e="T127" id="Seg_1179" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_1180" s="T127">que</ta>
            <ta e="T130" id="Seg_1181" s="T129">que</ta>
            <ta e="T131" id="Seg_1182" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_1183" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_1184" s="T132">v-v:cvb</ta>
            <ta e="T134" id="Seg_1185" s="T133">ptcl</ta>
            <ta e="T137" id="Seg_1186" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_1187" s="T137">cardnum</ta>
            <ta e="T139" id="Seg_1188" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_1189" s="T139">v-v:cvb</ta>
            <ta e="T141" id="Seg_1190" s="T140">ptcl</ta>
            <ta e="T147" id="Seg_1191" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_1192" s="T147">ptcl</ta>
            <ta e="T158" id="Seg_1193" s="T157">n-n:poss-n:case</ta>
            <ta e="T159" id="Seg_1194" s="T158">v-v:tense-v:poss.pn</ta>
            <ta e="T160" id="Seg_1195" s="T159">ptcl</ta>
            <ta e="T214" id="Seg_1196" s="T160">adv</ta>
            <ta e="T215" id="Seg_1197" s="T214">ptcl</ta>
            <ta e="T161" id="Seg_1198" s="T215">pers-pro:case</ta>
            <ta e="T162" id="Seg_1199" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_1200" s="T162">v-v:tense-v:poss.pn</ta>
            <ta e="T164" id="Seg_1201" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_1202" s="T164">v-v:tense-v:poss.pn</ta>
            <ta e="T166" id="Seg_1203" s="T165">adv</ta>
            <ta e="T167" id="Seg_1204" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_1205" s="T167">n-n:poss-n:case</ta>
            <ta e="T172" id="Seg_1206" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_1207" s="T172">adj-adj&gt;adv</ta>
            <ta e="T174" id="Seg_1208" s="T173">v-v:mood.pn</ta>
            <ta e="T175" id="Seg_1209" s="T174">propr</ta>
            <ta e="T176" id="Seg_1210" s="T175">v-v:cvb-v:pred.pn</ta>
            <ta e="T184" id="Seg_1211" s="T183">que</ta>
            <ta e="T185" id="Seg_1212" s="T184">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T188" id="Seg_1213" s="T187">que-pro:case</ta>
            <ta e="T189" id="Seg_1214" s="T188">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T191" id="Seg_1215" s="T190">v-v:tense-v:poss.pn</ta>
            <ta e="T192" id="Seg_1216" s="T191">ptcl</ta>
            <ta e="T196" id="Seg_1217" s="T195">pers-pro:case</ta>
            <ta e="T197" id="Seg_1218" s="T196">que-pro:case</ta>
            <ta e="T198" id="Seg_1219" s="T197">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T199" id="Seg_1220" s="T198">ptcl</ta>
            <ta e="T202" id="Seg_1221" s="T201">adv</ta>
            <ta e="T203" id="Seg_1222" s="T202">propr-n:case</ta>
            <ta e="T206" id="Seg_1223" s="T205">adj-n:(poss)-n:case</ta>
            <ta e="T207" id="Seg_1224" s="T206">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-AnIM">
            <ta e="T1" id="Seg_1225" s="T0">adv</ta>
            <ta e="T2" id="Seg_1226" s="T1">v</ta>
            <ta e="T3" id="Seg_1227" s="T2">n</ta>
            <ta e="T4" id="Seg_1228" s="T3">v</ta>
            <ta e="T5" id="Seg_1229" s="T4">adv</ta>
            <ta e="T6" id="Seg_1230" s="T5">pers</ta>
            <ta e="T7" id="Seg_1231" s="T6">v</ta>
            <ta e="T8" id="Seg_1232" s="T7">adv</ta>
            <ta e="T9" id="Seg_1233" s="T8">v</ta>
            <ta e="T90" id="Seg_1234" s="T9">v</ta>
            <ta e="T211" id="Seg_1235" s="T194">ptcl</ta>
            <ta e="T10" id="Seg_1236" s="T211">v</ta>
            <ta e="T20" id="Seg_1237" s="T19">que</ta>
            <ta e="T21" id="Seg_1238" s="T20">post</ta>
            <ta e="T22" id="Seg_1239" s="T21">v</ta>
            <ta e="T23" id="Seg_1240" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_1241" s="T23">n</ta>
            <ta e="T28" id="Seg_1242" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_1243" s="T28">ptcl</ta>
            <ta e="T40" id="Seg_1244" s="T39">v</ta>
            <ta e="T41" id="Seg_1245" s="T40">ptcl</ta>
            <ta e="T54" id="Seg_1246" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1247" s="T54">adv</ta>
            <ta e="T56" id="Seg_1248" s="T55">ptcl</ta>
            <ta e="T74" id="Seg_1249" s="T73">que</ta>
            <ta e="T75" id="Seg_1250" s="T74">v</ta>
            <ta e="T76" id="Seg_1251" s="T75">v</ta>
            <ta e="T77" id="Seg_1252" s="T76">ptcl</ta>
            <ta e="T79" id="Seg_1253" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_1254" s="T79">adj</ta>
            <ta e="T81" id="Seg_1255" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1256" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_1257" s="T82">n</ta>
            <ta e="T213" id="Seg_1258" s="T212">propr</ta>
            <ta e="T92" id="Seg_1259" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_1260" s="T92">cop</ta>
            <ta e="T94" id="Seg_1261" s="T93">propr</ta>
            <ta e="T110" id="Seg_1262" s="T109">n</ta>
            <ta e="T111" id="Seg_1263" s="T110">v</ta>
            <ta e="T116" id="Seg_1264" s="T115">adv</ta>
            <ta e="T117" id="Seg_1265" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_1266" s="T117">propr</ta>
            <ta e="T119" id="Seg_1267" s="T118">v</ta>
            <ta e="T120" id="Seg_1268" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_1269" s="T120">n</ta>
            <ta e="T125" id="Seg_1270" s="T124">v</ta>
            <ta e="T126" id="Seg_1271" s="T125">v</ta>
            <ta e="T127" id="Seg_1272" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_1273" s="T127">que</ta>
            <ta e="T130" id="Seg_1274" s="T129">que</ta>
            <ta e="T131" id="Seg_1275" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_1276" s="T131">n</ta>
            <ta e="T133" id="Seg_1277" s="T132">v</ta>
            <ta e="T134" id="Seg_1278" s="T133">ptcl</ta>
            <ta e="T137" id="Seg_1279" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_1280" s="T137">cardnum</ta>
            <ta e="T139" id="Seg_1281" s="T138">n</ta>
            <ta e="T140" id="Seg_1282" s="T139">v</ta>
            <ta e="T141" id="Seg_1283" s="T140">ptcl</ta>
            <ta e="T147" id="Seg_1284" s="T146">ptcl</ta>
            <ta e="T148" id="Seg_1285" s="T147">ptcl</ta>
            <ta e="T158" id="Seg_1286" s="T157">n</ta>
            <ta e="T159" id="Seg_1287" s="T158">v</ta>
            <ta e="T160" id="Seg_1288" s="T159">ptcl</ta>
            <ta e="T214" id="Seg_1289" s="T160">adv</ta>
            <ta e="T215" id="Seg_1290" s="T214">ptcl</ta>
            <ta e="T161" id="Seg_1291" s="T215">pers</ta>
            <ta e="T162" id="Seg_1292" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_1293" s="T162">v</ta>
            <ta e="T164" id="Seg_1294" s="T163">n</ta>
            <ta e="T165" id="Seg_1295" s="T164">v</ta>
            <ta e="T166" id="Seg_1296" s="T165">adv</ta>
            <ta e="T167" id="Seg_1297" s="T166">v</ta>
            <ta e="T168" id="Seg_1298" s="T167">n</ta>
            <ta e="T172" id="Seg_1299" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_1300" s="T172">adv</ta>
            <ta e="T174" id="Seg_1301" s="T173">v</ta>
            <ta e="T175" id="Seg_1302" s="T174">propr</ta>
            <ta e="T176" id="Seg_1303" s="T175">v</ta>
            <ta e="T184" id="Seg_1304" s="T183">que</ta>
            <ta e="T185" id="Seg_1305" s="T184">v</ta>
            <ta e="T188" id="Seg_1306" s="T187">que</ta>
            <ta e="T189" id="Seg_1307" s="T188">v</ta>
            <ta e="T191" id="Seg_1308" s="T190">v</ta>
            <ta e="T192" id="Seg_1309" s="T191">ptcl</ta>
            <ta e="T196" id="Seg_1310" s="T195">pers</ta>
            <ta e="T197" id="Seg_1311" s="T196">que</ta>
            <ta e="T198" id="Seg_1312" s="T197">v</ta>
            <ta e="T199" id="Seg_1313" s="T198">ptcl</ta>
            <ta e="T202" id="Seg_1314" s="T201">adv</ta>
            <ta e="T203" id="Seg_1315" s="T202">propr</ta>
            <ta e="T206" id="Seg_1316" s="T205">adj</ta>
            <ta e="T207" id="Seg_1317" s="T206">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-AnIM" />
         <annotation name="SyF" tierref="SyF-AnIM" />
         <annotation name="IST" tierref="IST-AnIM" />
         <annotation name="Top" tierref="Top-AnIM" />
         <annotation name="Foc" tierref="Foc-AnIM" />
         <annotation name="BOR" tierref="BOR-AnIM">
            <ta e="T168" id="Seg_1318" s="T167">RUS:cult</ta>
            <ta e="T175" id="Seg_1319" s="T174">RUS:cult</ta>
            <ta e="T203" id="Seg_1320" s="T202">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-AnIM" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-AnIM" />
         <annotation name="CS" tierref="CS-AnIM">
            <ta e="T194" id="Seg_1321" s="T90">RUS:int.ins</ta>
            <ta e="T208" id="Seg_1322" s="T207">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe-AnIM">
            <ta e="T5" id="Seg_1323" s="T0">You just tell, just tell a story.</ta>
            <ta e="T10" id="Seg_1324" s="T5">Just tell us, don't be a shame or whatever, everything (…) well, tell. </ta>
            <ta e="T24" id="Seg_1325" s="T19">With whom do you go to the tundra?</ta>
            <ta e="T29" id="Seg_1326" s="T27">Ok.</ta>
            <ta e="T41" id="Seg_1327" s="T39">Did you see them?</ta>
            <ta e="T56" id="Seg_1328" s="T53">And then?</ta>
            <ta e="T77" id="Seg_1329" s="T73">Where did you arrive?</ta>
            <ta e="T83" id="Seg_1330" s="T78">Yes, [and] who were there in the tundra?</ta>
            <ta e="T213" id="Seg_1331" s="T212">Natasha and her family.</ta>
            <ta e="T94" id="Seg_1332" s="T91">Natasha and family were not there.</ta>
            <ta e="T111" id="Seg_1333" s="T109">Check the nets.</ta>
            <ta e="T121" id="Seg_1334" s="T115">Then did Dima kill a reindeer?</ta>
            <ta e="T128" id="Seg_1335" s="T124">How many did he kill?</ta>
            <ta e="T134" id="Seg_1336" s="T129">How many wild reindeer did he see?</ta>
            <ta e="T141" id="Seg_1337" s="T136">He saw three wild reindeer.</ta>
            <ta e="T148" id="Seg_1338" s="T146">Ah ok.</ta>
            <ta e="T160" id="Seg_1339" s="T157">He came home?</ta>
            <ta e="T168" id="Seg_1340" s="T160">And you, did you go to the settlement by snow scooter?</ta>
            <ta e="T176" id="Seg_1341" s="T171">Speak correctly, so we entered Syndassko.</ta>
            <ta e="T185" id="Seg_1342" s="T183">Where did you go?</ta>
            <ta e="T189" id="Seg_1343" s="T187">What did you bring?</ta>
            <ta e="T192" id="Seg_1344" s="T190">Did you bring them?</ta>
            <ta e="T199" id="Seg_1345" s="T195">What did you do?</ta>
            <ta e="T203" id="Seg_1346" s="T201">And then to Syndassko.</ta>
            <ta e="T207" id="Seg_1347" s="T205">Is that the end?</ta>
            <ta e="T208" id="Seg_1348" s="T207">That's all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-AnIM">
            <ta e="T5" id="Seg_1349" s="T0">Erzähl einfach, erzähl einfach eine Geschichte.</ta>
            <ta e="T10" id="Seg_1350" s="T5">Du erzählst uns einfach ohne dich zu schämen oder so, alles (…) nun erzähl.</ta>
            <ta e="T24" id="Seg_1351" s="T19">Mit wem gehst du in die Tundra?</ta>
            <ta e="T29" id="Seg_1352" s="T27">Ah ja.</ta>
            <ta e="T41" id="Seg_1353" s="T39">Habt ihr sie gesehen?</ta>
            <ta e="T56" id="Seg_1354" s="T53">Ja, und dann?</ta>
            <ta e="T77" id="Seg_1355" s="T73">Wo kamt ihr hin?</ta>
            <ta e="T83" id="Seg_1356" s="T78">Ja, [und] wer war in der Tundra?</ta>
            <ta e="T213" id="Seg_1357" s="T212">Natascha und Familie.</ta>
            <ta e="T94" id="Seg_1358" s="T91">Natascha und Familie war nicht da.</ta>
            <ta e="T111" id="Seg_1359" s="T109">Die Netze kontrollieren.</ta>
            <ta e="T121" id="Seg_1360" s="T115">Und dann, tötete Dima ein Rentier?</ta>
            <ta e="T128" id="Seg_1361" s="T124">Wie viele tötete er?</ta>
            <ta e="T134" id="Seg_1362" s="T129">Wie viele wilde Rentiere sah er?</ta>
            <ta e="T141" id="Seg_1363" s="T136">Er sah doch drei wilde Rentiere.</ta>
            <ta e="T148" id="Seg_1364" s="T146">Ah, ok.</ta>
            <ta e="T160" id="Seg_1365" s="T157">Nach Hause kam er?</ta>
            <ta e="T168" id="Seg_1366" s="T160">Und dann, du, bist du mit dem Schneemobil in die Siedlung gefahren?</ta>
            <ta e="T176" id="Seg_1367" s="T171">Nun, sprich richtig, damit wir nach Syndassko hineinfahren.</ta>
            <ta e="T185" id="Seg_1368" s="T183">Wo seid ihr hingefahren?</ta>
            <ta e="T189" id="Seg_1369" s="T187">Was habt ihr mitgebracht?</ta>
            <ta e="T192" id="Seg_1370" s="T190">Habt ihr sie gebracht?</ta>
            <ta e="T199" id="Seg_1371" s="T195">Was hast du gemacht?</ta>
            <ta e="T203" id="Seg_1372" s="T201">Dann nach Syndassko.</ta>
            <ta e="T207" id="Seg_1373" s="T205">Ist das das Ende?</ta>
            <ta e="T208" id="Seg_1374" s="T207">Das ist alles.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-AnIM">
            <ta e="T5" id="Seg_1375" s="T0">Просто расскажи, просто расскажи историю.</ta>
            <ta e="T10" id="Seg_1376" s="T5">Просто расскажи нам, не стесняйся, не это.</ta>
            <ta e="T24" id="Seg_1377" s="T19">С кем ты ходишь в тундру?</ta>
            <ta e="T29" id="Seg_1378" s="T27">Ага.</ta>
            <ta e="T41" id="Seg_1379" s="T39">Вы их видели?</ta>
            <ta e="T56" id="Seg_1380" s="T53">А потом?</ta>
            <ta e="T77" id="Seg_1381" s="T73">Куда приехали?</ta>
            <ta e="T83" id="Seg_1382" s="T78">Да, [а] кто был в тундре?</ta>
            <ta e="T213" id="Seg_1383" s="T212">Наташины.</ta>
            <ta e="T94" id="Seg_1384" s="T91">Не было Наташиных.</ta>
            <ta e="T111" id="Seg_1385" s="T109">Сети посмотреть.</ta>
            <ta e="T121" id="Seg_1386" s="T115">И тогда Дима оленя убил?</ta>
            <ta e="T128" id="Seg_1387" s="T124">Скольких он убил?</ta>
            <ta e="T134" id="Seg_1388" s="T129">Сколько оленей он видел?</ta>
            <ta e="T141" id="Seg_1389" s="T136">Он же видел трёх оленей.</ta>
            <ta e="T148" id="Seg_1390" s="T146">А-а, ага.</ta>
            <ta e="T160" id="Seg_1391" s="T157">Вернулся?</ta>
            <ta e="T168" id="Seg_1392" s="T160">А ты вот поехал, на стоянку на буране потом поехал?</ta>
            <ta e="T176" id="Seg_1393" s="T171">Ну, говори правильно, чтобы ехать в Сындасско.</ta>
            <ta e="T185" id="Seg_1394" s="T183">Куда вы поехали?</ta>
            <ta e="T189" id="Seg_1395" s="T187">Что вы привезли?</ta>
            <ta e="T192" id="Seg_1396" s="T190">Вы их привезли?</ta>
            <ta e="T199" id="Seg_1397" s="T195">Что вы делали?</ta>
            <ta e="T203" id="Seg_1398" s="T201">А потом в Сындасско.</ta>
            <ta e="T207" id="Seg_1399" s="T205">Это всё?</ta>
            <ta e="T208" id="Seg_1400" s="T207">Всё.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-AnIM" />
         <annotation name="nt" tierref="nt-AnIM" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-AnMSp"
                      id="tx-AnMSp"
                      speaker="AnMSp"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-AnMSp">
            <ts e="T19" id="Seg_1401" n="sc" s="T10">
               <ts e="T16" id="Seg_1403" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_1405" n="HIAT:w" s="T10">Barar</ts>
                  <nts id="Seg_1406" n="HIAT:ip">,</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_1409" n="HIAT:w" s="T11">kim</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_1412" n="HIAT:w" s="T12">Hɨndaːssko</ts>
                  <nts id="Seg_1413" n="HIAT:ip">,</nts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_1416" n="HIAT:w" s="T13">eee</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_1419" n="HIAT:w" s="T14">tɨ͡aga</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_1422" n="HIAT:w" s="T15">barabɨt</ts>
                  <nts id="Seg_1423" n="HIAT:ip">.</nts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_1426" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_1428" n="HIAT:w" s="T16">Onton</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_1431" n="HIAT:w" s="T17">bara</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_1434" n="HIAT:w" s="T18">turabit</ts>
                  <nts id="Seg_1435" n="HIAT:ip">.</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T27" id="Seg_1437" n="sc" s="T24">
               <ts e="T27" id="Seg_1439" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_1441" n="HIAT:w" s="T24">Diːma</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_1444" n="HIAT:w" s="T25">onton</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_1447" n="HIAT:w" s="T26">Mɨkaː</ts>
                  <nts id="Seg_1448" n="HIAT:ip">.</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T39" id="Seg_1450" n="sc" s="T29">
               <ts e="T32" id="Seg_1452" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_1454" n="HIAT:w" s="T29">Onton</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_1457" n="HIAT:w" s="T30">onno</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_1460" n="HIAT:w" s="T31">barabɨt</ts>
                  <nts id="Seg_1461" n="HIAT:ip">.</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_1464" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_1466" n="HIAT:w" s="T32">Kim</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_1469" n="HIAT:w" s="T33">kim</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_1472" n="HIAT:w" s="T34">baːr</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_1475" n="HIAT:w" s="T35">ehm</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_1478" n="HIAT:w" s="T36">ehm</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_1481" n="HIAT:w" s="T37">kɨːllar</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_1484" n="HIAT:w" s="T38">baːllar</ts>
                  <nts id="Seg_1485" n="HIAT:ip">.</nts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_1487" n="sc" s="T41">
               <ts e="T44" id="Seg_1489" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_1491" n="HIAT:w" s="T41">Burammɨt</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_1494" n="HIAT:w" s="T42">katu͡oga</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_1497" n="HIAT:w" s="T43">aldʼːammɨt</ts>
                  <nts id="Seg_1498" n="HIAT:ip">.</nts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_1501" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_1503" n="HIAT:w" s="T44">Onton</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_1506" n="HIAT:w" s="T45">menjajdaːn</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_1509" n="HIAT:w" s="T46">bartɨbɨt</ts>
                  <nts id="Seg_1510" n="HIAT:ip">,</nts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_1513" n="HIAT:w" s="T47">ama</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_1516" n="HIAT:w" s="T48">körsörö</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_1519" n="HIAT:w" s="T49">biːr</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_1522" n="HIAT:w" s="T50">kɨːlɨ</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_1525" n="HIAT:w" s="T51">ölörön</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_1528" n="HIAT:w" s="T52">keːspippit</ts>
                  <nts id="Seg_1529" n="HIAT:ip">.</nts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T73" id="Seg_1531" n="sc" s="T56">
               <ts e="T60" id="Seg_1533" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_1535" n="HIAT:w" s="T56">Onton</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_1538" n="HIAT:w" s="T57">dalše</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_1541" n="HIAT:w" s="T58">barabɨt</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_1544" n="HIAT:w" s="T59">hülümmüppüt</ts>
                  <nts id="Seg_1545" n="HIAT:ip">.</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_1548" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_1550" n="HIAT:w" s="T60">Onton</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_1553" n="HIAT:w" s="T61">dalše</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_1556" n="HIAT:w" s="T62">barbɨppɨt</ts>
                  <nts id="Seg_1557" n="HIAT:ip">.</nts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_1560" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_1562" n="HIAT:w" s="T63">Onno</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_1565" n="HIAT:w" s="T64">emi͡e</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_1568" n="HIAT:w" s="T65">kɨːllar</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_1571" n="HIAT:w" s="T66">baːllar</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_1574" n="HIAT:w" s="T67">onu</ts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_1577" n="HIAT:w" s="T68">gojobuːnnaːbɨppɨt</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_1580" n="HIAT:w" s="T69">bulbatappɨt</ts>
                  <nts id="Seg_1581" n="HIAT:ip">.</nts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_1584" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_1586" n="HIAT:w" s="T70">Onton</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_1589" n="HIAT:w" s="T71">keltibit</ts>
                  <nts id="Seg_1590" n="HIAT:ip">.</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T78" id="Seg_1592" n="sc" s="T77">
               <ts e="T78" id="Seg_1594" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_1596" n="HIAT:w" s="T77">Tɨ͡aga</ts>
                  <nts id="Seg_1597" n="HIAT:ip">.</nts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T212" id="Seg_1599" n="sc" s="T83">
               <ts e="T212" id="Seg_1601" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_1603" n="HIAT:w" s="T83">Maːmam</ts>
                  <nts id="Seg_1604" n="HIAT:ip">,</nts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_1607" n="HIAT:w" s="T84">Ženja</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_1610" n="HIAT:w" s="T85">kim</ts>
                  <nts id="Seg_1611" n="HIAT:ip">.</nts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_1613" n="sc" s="T213">
               <ts e="T87" id="Seg_1615" n="HIAT:u" s="T213">
                  <ts e="T87" id="Seg_1617" n="HIAT:w" s="T213">Natalaːk</ts>
                  <nts id="Seg_1618" n="HIAT:ip">.</nts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_1621" n="HIAT:u" s="T87">
                  <ts e="T210" id="Seg_1623" n="HIAT:w" s="T87">Eh</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_1626" n="HIAT:w" s="T210">Natalaːk</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_1629" n="HIAT:w" s="T88">hu͡ok</ts>
                  <nts id="Seg_1630" n="HIAT:ip">.</nts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T109" id="Seg_1632" n="sc" s="T94">
               <ts e="T97" id="Seg_1634" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_1636" n="HIAT:w" s="T94">Onton</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_1639" n="HIAT:w" s="T95">harsɨ͡arda</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_1642" n="HIAT:w" s="T96">barda</ts>
                  <nts id="Seg_1643" n="HIAT:ip">.</nts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_1646" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_1648" n="HIAT:w" s="T97">A</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_1651" n="HIAT:w" s="T98">Diːma</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_1654" n="HIAT:w" s="T99">barda</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_1657" n="HIAT:w" s="T100">harsɨ͡arda</ts>
                  <nts id="Seg_1658" n="HIAT:ip">,</nts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_1661" n="HIAT:w" s="T101">kɨːlɨ</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_1664" n="HIAT:w" s="T102">kördüː</ts>
                  <nts id="Seg_1665" n="HIAT:ip">.</nts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_1668" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_1670" n="HIAT:w" s="T103">Onton</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_1673" n="HIAT:w" s="T104">onton</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_1676" n="HIAT:w" s="T105">Mɨkaː</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_1679" n="HIAT:w" s="T106">kimŋe</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_1682" n="HIAT:w" s="T107">barda</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_1685" n="HIAT:w" s="T108">balɨktana</ts>
                  <nts id="Seg_1686" n="HIAT:ip">.</nts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T115" id="Seg_1688" n="sc" s="T111">
               <ts e="T115" id="Seg_1690" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_1692" n="HIAT:w" s="T111">Onton</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_1695" n="HIAT:w" s="T112">harsɨŋŋɨtɨn</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_1698" n="HIAT:w" s="T113">Natalaːk</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_1701" n="HIAT:w" s="T114">kelbittere</ts>
                  <nts id="Seg_1702" n="HIAT:ip">.</nts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T124" id="Seg_1704" n="sc" s="T121">
               <ts e="T124" id="Seg_1706" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_1708" n="HIAT:w" s="T121">Hu͡ok</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_1711" n="HIAT:w" s="T122">ölörö</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_1714" n="HIAT:w" s="T123">ilik</ts>
                  <nts id="Seg_1715" n="HIAT:ip">.</nts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T129" id="Seg_1717" n="sc" s="T128">
               <ts e="T129" id="Seg_1719" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_1721" n="HIAT:w" s="T128">Kanna</ts>
                  <nts id="Seg_1722" n="HIAT:ip">?</nts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T136" id="Seg_1724" n="sc" s="T134">
               <ts e="T136" id="Seg_1726" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_1728" n="HIAT:w" s="T134">Biːr</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_1731" n="HIAT:w" s="T135">kɨːlɨ</ts>
                  <nts id="Seg_1732" n="HIAT:ip">.</nts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T146" id="Seg_1734" n="sc" s="T141">
               <ts e="T146" id="Seg_1736" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_1738" n="HIAT:w" s="T141">De</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_1741" n="HIAT:w" s="T142">ikki</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_1744" n="HIAT:w" s="T143">kɨːlɨ</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_1747" n="HIAT:w" s="T144">ete</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_1750" n="HIAT:w" s="T145">di͡en</ts>
                  <nts id="Seg_1751" n="HIAT:ip">.</nts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T157" id="Seg_1753" n="sc" s="T148">
               <ts e="T155" id="Seg_1755" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_1757" n="HIAT:w" s="T148">Onno</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_1760" n="HIAT:w" s="T149">ikki</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1762" n="HIAT:ip">(</nts>
                  <nts id="Seg_1763" n="HIAT:ip">(</nts>
                  <ats e="T151" id="Seg_1764" n="HIAT:non-pho" s="T150">…</ats>
                  <nts id="Seg_1765" n="HIAT:ip">)</nts>
                  <nts id="Seg_1766" n="HIAT:ip">)</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_1769" n="HIAT:w" s="T151">biːr</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_1772" n="HIAT:w" s="T152">biːrin</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_1775" n="HIAT:w" s="T154">ölörbüt</ts>
                  <nts id="Seg_1776" n="HIAT:ip">.</nts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_1779" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_1781" n="HIAT:w" s="T155">Onton</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_1784" n="HIAT:w" s="T156">kelbite</ts>
                  <nts id="Seg_1785" n="HIAT:ip">.</nts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T171" id="Seg_1787" n="sc" s="T168">
               <ts e="T171" id="Seg_1789" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_1791" n="HIAT:w" s="T168">Bararbɨtɨgar</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_1794" n="HIAT:w" s="T169">Sɨndaːssko</ts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_1797" n="HIAT:w" s="T170">kiːrer</ts>
                  <nts id="Seg_1798" n="HIAT:ip">.</nts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T183" id="Seg_1800" n="sc" s="T176">
               <ts e="T183" id="Seg_1802" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_1804" n="HIAT:w" s="T176">Sɨndaːssko</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_1807" n="HIAT:w" s="T177">kiːreːri</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_1810" n="HIAT:w" s="T178">bardɨbɨt</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_1813" n="HIAT:w" s="T179">buranɨnan</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_1816" n="HIAT:w" s="T180">Mɨkaː</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_1819" n="HIAT:w" s="T181">ta</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_1822" n="HIAT:w" s="T182">maːmam</ts>
                  <nts id="Seg_1823" n="HIAT:ip">.</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T187" id="Seg_1825" n="sc" s="T185">
               <ts e="T187" id="Seg_1827" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_1829" n="HIAT:w" s="T185">Palaːkalar</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_1832" n="HIAT:w" s="T186">ke</ts>
                  <nts id="Seg_1833" n="HIAT:ip">.</nts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T190" id="Seg_1835" n="sc" s="T189">
               <ts e="T190" id="Seg_1837" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_1839" n="HIAT:w" s="T189">Tiriːler</ts>
                  <nts id="Seg_1840" n="HIAT:ip">.</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T195" id="Seg_1842" n="sc" s="T192">
               <ts e="T195" id="Seg_1844" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_1846" n="HIAT:w" s="T192">Onton</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1848" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_1850" n="HIAT:w" s="T193">Hɨnda-</ts>
                  <nts id="Seg_1851" n="HIAT:ip">)</nts>
                  <nts id="Seg_1852" n="HIAT:ip">.</nts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T201" id="Seg_1854" n="sc" s="T199">
               <ts e="T201" id="Seg_1856" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_1858" n="HIAT:w" s="T199">Buran</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_1861" n="HIAT:w" s="T200">vodittaːbɨtɨm</ts>
                  <nts id="Seg_1862" n="HIAT:ip">.</nts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T205" id="Seg_1864" n="sc" s="T203">
               <ts e="T205" id="Seg_1866" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_1868" n="HIAT:w" s="T203">Hɨndaːska</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_1871" n="HIAT:w" s="T204">keltibit</ts>
                  <nts id="Seg_1872" n="HIAT:ip">.</nts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-AnMSp">
            <ts e="T19" id="Seg_1874" n="sc" s="T10">
               <ts e="T11" id="Seg_1876" n="e" s="T10">Barar, </ts>
               <ts e="T12" id="Seg_1878" n="e" s="T11">kim </ts>
               <ts e="T13" id="Seg_1880" n="e" s="T12">Hɨndaːssko, </ts>
               <ts e="T14" id="Seg_1882" n="e" s="T13">eee </ts>
               <ts e="T15" id="Seg_1884" n="e" s="T14">tɨ͡aga </ts>
               <ts e="T16" id="Seg_1886" n="e" s="T15">barabɨt. </ts>
               <ts e="T17" id="Seg_1888" n="e" s="T16">Onton </ts>
               <ts e="T18" id="Seg_1890" n="e" s="T17">bara </ts>
               <ts e="T19" id="Seg_1892" n="e" s="T18">turabit. </ts>
            </ts>
            <ts e="T27" id="Seg_1893" n="sc" s="T24">
               <ts e="T25" id="Seg_1895" n="e" s="T24">Diːma </ts>
               <ts e="T26" id="Seg_1897" n="e" s="T25">onton </ts>
               <ts e="T27" id="Seg_1899" n="e" s="T26">Mɨkaː. </ts>
            </ts>
            <ts e="T39" id="Seg_1900" n="sc" s="T29">
               <ts e="T30" id="Seg_1902" n="e" s="T29">Onton </ts>
               <ts e="T31" id="Seg_1904" n="e" s="T30">onno </ts>
               <ts e="T32" id="Seg_1906" n="e" s="T31">barabɨt. </ts>
               <ts e="T33" id="Seg_1908" n="e" s="T32">Kim </ts>
               <ts e="T34" id="Seg_1910" n="e" s="T33">kim </ts>
               <ts e="T35" id="Seg_1912" n="e" s="T34">baːr </ts>
               <ts e="T36" id="Seg_1914" n="e" s="T35">ehm </ts>
               <ts e="T37" id="Seg_1916" n="e" s="T36">ehm </ts>
               <ts e="T38" id="Seg_1918" n="e" s="T37">kɨːllar </ts>
               <ts e="T39" id="Seg_1920" n="e" s="T38">baːllar. </ts>
            </ts>
            <ts e="T53" id="Seg_1921" n="sc" s="T41">
               <ts e="T42" id="Seg_1923" n="e" s="T41">Burammɨt </ts>
               <ts e="T43" id="Seg_1925" n="e" s="T42">katu͡oga </ts>
               <ts e="T44" id="Seg_1927" n="e" s="T43">aldʼːammɨt. </ts>
               <ts e="T45" id="Seg_1929" n="e" s="T44">Onton </ts>
               <ts e="T46" id="Seg_1931" n="e" s="T45">menjajdaːn </ts>
               <ts e="T47" id="Seg_1933" n="e" s="T46">bartɨbɨt, </ts>
               <ts e="T48" id="Seg_1935" n="e" s="T47">ama </ts>
               <ts e="T49" id="Seg_1937" n="e" s="T48">körsörö </ts>
               <ts e="T50" id="Seg_1939" n="e" s="T49">biːr </ts>
               <ts e="T51" id="Seg_1941" n="e" s="T50">kɨːlɨ </ts>
               <ts e="T52" id="Seg_1943" n="e" s="T51">ölörön </ts>
               <ts e="T53" id="Seg_1945" n="e" s="T52">keːspippit. </ts>
            </ts>
            <ts e="T73" id="Seg_1946" n="sc" s="T56">
               <ts e="T57" id="Seg_1948" n="e" s="T56">Onton </ts>
               <ts e="T58" id="Seg_1950" n="e" s="T57">dalše </ts>
               <ts e="T59" id="Seg_1952" n="e" s="T58">barabɨt </ts>
               <ts e="T60" id="Seg_1954" n="e" s="T59">hülümmüppüt. </ts>
               <ts e="T61" id="Seg_1956" n="e" s="T60">Onton </ts>
               <ts e="T62" id="Seg_1958" n="e" s="T61">dalše </ts>
               <ts e="T63" id="Seg_1960" n="e" s="T62">barbɨppɨt. </ts>
               <ts e="T64" id="Seg_1962" n="e" s="T63">Onno </ts>
               <ts e="T65" id="Seg_1964" n="e" s="T64">emi͡e </ts>
               <ts e="T66" id="Seg_1966" n="e" s="T65">kɨːllar </ts>
               <ts e="T67" id="Seg_1968" n="e" s="T66">baːllar </ts>
               <ts e="T68" id="Seg_1970" n="e" s="T67">onu </ts>
               <ts e="T69" id="Seg_1972" n="e" s="T68">gojobuːnnaːbɨppɨt </ts>
               <ts e="T70" id="Seg_1974" n="e" s="T69">bulbatappɨt. </ts>
               <ts e="T71" id="Seg_1976" n="e" s="T70">Onton </ts>
               <ts e="T73" id="Seg_1978" n="e" s="T71">keltibit. </ts>
            </ts>
            <ts e="T78" id="Seg_1979" n="sc" s="T77">
               <ts e="T78" id="Seg_1981" n="e" s="T77">Tɨ͡aga. </ts>
            </ts>
            <ts e="T212" id="Seg_1982" n="sc" s="T83">
               <ts e="T84" id="Seg_1984" n="e" s="T83">Maːmam, </ts>
               <ts e="T85" id="Seg_1986" n="e" s="T84">Ženja </ts>
               <ts e="T212" id="Seg_1988" n="e" s="T85">kim. </ts>
            </ts>
            <ts e="T91" id="Seg_1989" n="sc" s="T213">
               <ts e="T87" id="Seg_1991" n="e" s="T213">Natalaːk. </ts>
               <ts e="T210" id="Seg_1993" n="e" s="T87">Eh </ts>
               <ts e="T88" id="Seg_1995" n="e" s="T210">Natalaːk </ts>
               <ts e="T91" id="Seg_1997" n="e" s="T88">hu͡ok. </ts>
            </ts>
            <ts e="T109" id="Seg_1998" n="sc" s="T94">
               <ts e="T95" id="Seg_2000" n="e" s="T94">Onton </ts>
               <ts e="T96" id="Seg_2002" n="e" s="T95">harsɨ͡arda </ts>
               <ts e="T97" id="Seg_2004" n="e" s="T96">barda. </ts>
               <ts e="T98" id="Seg_2006" n="e" s="T97">A </ts>
               <ts e="T99" id="Seg_2008" n="e" s="T98">Diːma </ts>
               <ts e="T100" id="Seg_2010" n="e" s="T99">barda </ts>
               <ts e="T101" id="Seg_2012" n="e" s="T100">harsɨ͡arda, </ts>
               <ts e="T102" id="Seg_2014" n="e" s="T101">kɨːlɨ </ts>
               <ts e="T103" id="Seg_2016" n="e" s="T102">kördüː. </ts>
               <ts e="T104" id="Seg_2018" n="e" s="T103">Onton </ts>
               <ts e="T105" id="Seg_2020" n="e" s="T104">onton </ts>
               <ts e="T106" id="Seg_2022" n="e" s="T105">Mɨkaː </ts>
               <ts e="T107" id="Seg_2024" n="e" s="T106">kimŋe </ts>
               <ts e="T108" id="Seg_2026" n="e" s="T107">barda </ts>
               <ts e="T109" id="Seg_2028" n="e" s="T108">balɨktana. </ts>
            </ts>
            <ts e="T115" id="Seg_2029" n="sc" s="T111">
               <ts e="T112" id="Seg_2031" n="e" s="T111">Onton </ts>
               <ts e="T113" id="Seg_2033" n="e" s="T112">harsɨŋŋɨtɨn </ts>
               <ts e="T114" id="Seg_2035" n="e" s="T113">Natalaːk </ts>
               <ts e="T115" id="Seg_2037" n="e" s="T114">kelbittere. </ts>
            </ts>
            <ts e="T124" id="Seg_2038" n="sc" s="T121">
               <ts e="T122" id="Seg_2040" n="e" s="T121">Hu͡ok </ts>
               <ts e="T123" id="Seg_2042" n="e" s="T122">ölörö </ts>
               <ts e="T124" id="Seg_2044" n="e" s="T123">ilik. </ts>
            </ts>
            <ts e="T129" id="Seg_2045" n="sc" s="T128">
               <ts e="T129" id="Seg_2047" n="e" s="T128">Kanna? </ts>
            </ts>
            <ts e="T136" id="Seg_2048" n="sc" s="T134">
               <ts e="T135" id="Seg_2050" n="e" s="T134">Biːr </ts>
               <ts e="T136" id="Seg_2052" n="e" s="T135">kɨːlɨ. </ts>
            </ts>
            <ts e="T146" id="Seg_2053" n="sc" s="T141">
               <ts e="T142" id="Seg_2055" n="e" s="T141">De </ts>
               <ts e="T143" id="Seg_2057" n="e" s="T142">ikki </ts>
               <ts e="T144" id="Seg_2059" n="e" s="T143">kɨːlɨ </ts>
               <ts e="T145" id="Seg_2061" n="e" s="T144">ete </ts>
               <ts e="T146" id="Seg_2063" n="e" s="T145">di͡en. </ts>
            </ts>
            <ts e="T157" id="Seg_2064" n="sc" s="T148">
               <ts e="T149" id="Seg_2066" n="e" s="T148">Onno </ts>
               <ts e="T150" id="Seg_2068" n="e" s="T149">ikki </ts>
               <ts e="T151" id="Seg_2070" n="e" s="T150">((…)) </ts>
               <ts e="T152" id="Seg_2072" n="e" s="T151">biːr </ts>
               <ts e="T154" id="Seg_2074" n="e" s="T152">biːrin </ts>
               <ts e="T155" id="Seg_2076" n="e" s="T154">ölörbüt. </ts>
               <ts e="T156" id="Seg_2078" n="e" s="T155">Onton </ts>
               <ts e="T157" id="Seg_2080" n="e" s="T156">kelbite. </ts>
            </ts>
            <ts e="T171" id="Seg_2081" n="sc" s="T168">
               <ts e="T169" id="Seg_2083" n="e" s="T168">Bararbɨtɨgar </ts>
               <ts e="T170" id="Seg_2085" n="e" s="T169">Sɨndaːssko </ts>
               <ts e="T171" id="Seg_2087" n="e" s="T170">kiːrer. </ts>
            </ts>
            <ts e="T183" id="Seg_2088" n="sc" s="T176">
               <ts e="T177" id="Seg_2090" n="e" s="T176">Sɨndaːssko </ts>
               <ts e="T178" id="Seg_2092" n="e" s="T177">kiːreːri </ts>
               <ts e="T179" id="Seg_2094" n="e" s="T178">bardɨbɨt </ts>
               <ts e="T180" id="Seg_2096" n="e" s="T179">buranɨnan </ts>
               <ts e="T181" id="Seg_2098" n="e" s="T180">Mɨkaː </ts>
               <ts e="T182" id="Seg_2100" n="e" s="T181">ta </ts>
               <ts e="T183" id="Seg_2102" n="e" s="T182">maːmam. </ts>
            </ts>
            <ts e="T187" id="Seg_2103" n="sc" s="T185">
               <ts e="T186" id="Seg_2105" n="e" s="T185">Palaːkalar </ts>
               <ts e="T187" id="Seg_2107" n="e" s="T186">ke. </ts>
            </ts>
            <ts e="T190" id="Seg_2108" n="sc" s="T189">
               <ts e="T190" id="Seg_2110" n="e" s="T189">Tiriːler. </ts>
            </ts>
            <ts e="T195" id="Seg_2111" n="sc" s="T192">
               <ts e="T193" id="Seg_2113" n="e" s="T192">Onton </ts>
               <ts e="T195" id="Seg_2115" n="e" s="T193">(Hɨnda-). </ts>
            </ts>
            <ts e="T201" id="Seg_2116" n="sc" s="T199">
               <ts e="T200" id="Seg_2118" n="e" s="T199">Buran </ts>
               <ts e="T201" id="Seg_2120" n="e" s="T200">vodittaːbɨtɨm. </ts>
            </ts>
            <ts e="T205" id="Seg_2121" n="sc" s="T203">
               <ts e="T204" id="Seg_2123" n="e" s="T203">Hɨndaːska </ts>
               <ts e="T205" id="Seg_2125" n="e" s="T204">keltibit. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-AnMSp">
            <ta e="T16" id="Seg_2126" s="T10">AnIM_AnMSp_2009_Holiday_conv.AnMSp.001 (001.003)</ta>
            <ta e="T19" id="Seg_2127" s="T16">AnIM_AnMSp_2009_Holiday_conv.AnMSp.002 (001.004)</ta>
            <ta e="T27" id="Seg_2128" s="T24">AnIM_AnMSp_2009_Holiday_conv.AnMSp.003 (001.006)</ta>
            <ta e="T32" id="Seg_2129" s="T29">AnIM_AnMSp_2009_Holiday_conv.AnMSp.004 (001.008)</ta>
            <ta e="T39" id="Seg_2130" s="T32">AnIM_AnMSp_2009_Holiday_conv.AnMSp.005 (001.009)</ta>
            <ta e="T44" id="Seg_2131" s="T41">AnIM_AnMSp_2009_Holiday_conv.AnMSp.006 (001.011)</ta>
            <ta e="T53" id="Seg_2132" s="T44">AnIM_AnMSp_2009_Holiday_conv.AnMSp.007 (001.012)</ta>
            <ta e="T60" id="Seg_2133" s="T56">AnIM_AnMSp_2009_Holiday_conv.AnMSp.008 (001.014)</ta>
            <ta e="T63" id="Seg_2134" s="T60">AnIM_AnMSp_2009_Holiday_conv.AnMSp.009 (001.015)</ta>
            <ta e="T70" id="Seg_2135" s="T63">AnIM_AnMSp_2009_Holiday_conv.AnMSp.010 (001.016)</ta>
            <ta e="T73" id="Seg_2136" s="T70">AnIM_AnMSp_2009_Holiday_conv.AnMSp.011 (001.017)</ta>
            <ta e="T78" id="Seg_2137" s="T77">AnIM_AnMSp_2009_Holiday_conv.AnMSp.012 (001.019)</ta>
            <ta e="T212" id="Seg_2138" s="T83">AnIM_AnMSp_2009_Holiday_conv.AnMSp.013 (001.022)</ta>
            <ta e="T87" id="Seg_2139" s="T213">AnIM_AnMSp_2009_Holiday_conv.AnMSp.014 (001.022)</ta>
            <ta e="T91" id="Seg_2140" s="T87">AnIM_AnMSp_2009_Holiday_conv.AnMSp.015 (001.023)</ta>
            <ta e="T97" id="Seg_2141" s="T94">AnIM_AnMSp_2009_Holiday_conv.AnMSp.016 (001.025)</ta>
            <ta e="T103" id="Seg_2142" s="T97">AnIM_AnMSp_2009_Holiday_conv.AnMSp.017 (001.026)</ta>
            <ta e="T109" id="Seg_2143" s="T103">AnIM_AnMSp_2009_Holiday_conv.AnMSp.018 (001.027)</ta>
            <ta e="T115" id="Seg_2144" s="T111">AnIM_AnMSp_2009_Holiday_conv.AnMSp.019 (001.029)</ta>
            <ta e="T124" id="Seg_2145" s="T121">AnIM_AnMSp_2009_Holiday_conv.AnMSp.020 (001.031)</ta>
            <ta e="T129" id="Seg_2146" s="T128">AnIM_AnMSp_2009_Holiday_conv.AnMSp.021 (001.033)</ta>
            <ta e="T136" id="Seg_2147" s="T134">AnIM_AnMSp_2009_Holiday_conv.AnMSp.022 (001.035)</ta>
            <ta e="T146" id="Seg_2148" s="T141">AnIM_AnMSp_2009_Holiday_conv.AnMSp.023 (001.037)</ta>
            <ta e="T155" id="Seg_2149" s="T148">AnIM_AnMSp_2009_Holiday_conv.AnMSp.024 (001.039)</ta>
            <ta e="T157" id="Seg_2150" s="T155">AnIM_AnMSp_2009_Holiday_conv.AnMSp.025 (001.040)</ta>
            <ta e="T171" id="Seg_2151" s="T168">AnIM_AnMSp_2009_Holiday_conv.AnMSp.026 (001.043)</ta>
            <ta e="T183" id="Seg_2152" s="T176">AnIM_AnMSp_2009_Holiday_conv.AnMSp.027 (001.045)</ta>
            <ta e="T187" id="Seg_2153" s="T185">AnIM_AnMSp_2009_Holiday_conv.AnMSp.028 (001.047)</ta>
            <ta e="T190" id="Seg_2154" s="T189">AnIM_AnMSp_2009_Holiday_conv.AnMSp.029 (001.049)</ta>
            <ta e="T195" id="Seg_2155" s="T192">AnIM_AnMSp_2009_Holiday_conv.AnMSp.030 (001.051)</ta>
            <ta e="T201" id="Seg_2156" s="T199">AnIM_AnMSp_2009_Holiday_conv.AnMSp.031 (001.053)</ta>
            <ta e="T205" id="Seg_2157" s="T203">AnIM_AnMSp_2009_Holiday_conv.AnMSp.032 (001.055)</ta>
         </annotation>
         <annotation name="st" tierref="st-AnMSp" />
         <annotation name="ts" tierref="ts-AnMSp">
            <ta e="T16" id="Seg_2158" s="T10">Barar, kim Hɨndaːssko, eee tɨ͡aga barabɨt. </ta>
            <ta e="T19" id="Seg_2159" s="T16">Onton bara turabit. </ta>
            <ta e="T27" id="Seg_2160" s="T24">Diːma onton Mɨkaː. </ta>
            <ta e="T32" id="Seg_2161" s="T29">Onton onno barabɨt. </ta>
            <ta e="T39" id="Seg_2162" s="T32">Kim kim baːr ehm ehm kɨːllar baːllar. </ta>
            <ta e="T44" id="Seg_2163" s="T41">Burammɨt katu͡oga aldʼːammɨt. </ta>
            <ta e="T53" id="Seg_2164" s="T44">Onton menjajdaːn bartɨbɨt, ama körsörö biːr kɨːlɨ ölörön keːspippit. </ta>
            <ta e="T60" id="Seg_2165" s="T56">Onton dalše barabɨt hülümmüppüt. </ta>
            <ta e="T63" id="Seg_2166" s="T60">Onton dalše barbɨppɨt. </ta>
            <ta e="T70" id="Seg_2167" s="T63">Onno emi͡e kɨːllar baːllar onu gojobuːnnaːbɨppɨt bulbatappɨt. </ta>
            <ta e="T73" id="Seg_2168" s="T70">Onton keltibit. </ta>
            <ta e="T78" id="Seg_2169" s="T77">Tɨ͡aga. </ta>
            <ta e="T212" id="Seg_2170" s="T83">Maːmam, Ženja kim.</ta>
            <ta e="T87" id="Seg_2171" s="T213">Natalaːk. </ta>
            <ta e="T91" id="Seg_2172" s="T87">Eh Natalaːk hu͡ok. </ta>
            <ta e="T97" id="Seg_2173" s="T94">Onton harsɨ͡arda barda. </ta>
            <ta e="T103" id="Seg_2174" s="T97">A Diːma barda harsɨ͡arda, kɨːlɨ kördüː. </ta>
            <ta e="T109" id="Seg_2175" s="T103">Onton onton Mɨkaː kimŋe barda balɨktana. </ta>
            <ta e="T115" id="Seg_2176" s="T111">Onton harsɨŋŋɨtɨn Natalaːk kelbittere. </ta>
            <ta e="T124" id="Seg_2177" s="T121">Hu͡ok ölörö ilik. </ta>
            <ta e="T129" id="Seg_2178" s="T128">Kanna? </ta>
            <ta e="T136" id="Seg_2179" s="T134">Biːr kɨːlɨ. </ta>
            <ta e="T146" id="Seg_2180" s="T141">De ikki kɨːlɨ ete di͡en. </ta>
            <ta e="T155" id="Seg_2181" s="T148">Onno ikki ((…)) biːr biːrin ölörbüt. </ta>
            <ta e="T157" id="Seg_2182" s="T155">Onton kelbite. </ta>
            <ta e="T171" id="Seg_2183" s="T168">Bararbɨtɨgar Sɨndaːssko kiːrer. </ta>
            <ta e="T183" id="Seg_2184" s="T176">Sɨndaːssko kiːreːri bardɨbɨt buranɨnan Mɨkaː ta maːmam. </ta>
            <ta e="T187" id="Seg_2185" s="T185">Palaːkalar ke. </ta>
            <ta e="T190" id="Seg_2186" s="T189">Tiriːler. </ta>
            <ta e="T195" id="Seg_2187" s="T192">Onton (Hɨnda-). </ta>
            <ta e="T201" id="Seg_2188" s="T199">Buran vodittaːbɨtɨm. </ta>
            <ta e="T205" id="Seg_2189" s="T203">Hɨndaːska keltibit. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-AnMSp">
            <ta e="T11" id="Seg_2190" s="T10">bar-ar</ta>
            <ta e="T12" id="Seg_2191" s="T11">kim</ta>
            <ta e="T13" id="Seg_2192" s="T12">Hɨndaːssko</ta>
            <ta e="T14" id="Seg_2193" s="T13">eː</ta>
            <ta e="T15" id="Seg_2194" s="T14">tɨ͡a-ga</ta>
            <ta e="T16" id="Seg_2195" s="T15">bar-a-bɨt</ta>
            <ta e="T17" id="Seg_2196" s="T16">onton</ta>
            <ta e="T18" id="Seg_2197" s="T17">bar-a</ta>
            <ta e="T19" id="Seg_2198" s="T18">tur-a-bit</ta>
            <ta e="T25" id="Seg_2199" s="T24">Diːma</ta>
            <ta e="T26" id="Seg_2200" s="T25">onton</ta>
            <ta e="T27" id="Seg_2201" s="T26">Mɨkaː</ta>
            <ta e="T30" id="Seg_2202" s="T29">onton</ta>
            <ta e="T31" id="Seg_2203" s="T30">onno</ta>
            <ta e="T32" id="Seg_2204" s="T31">bar-a-bɨt</ta>
            <ta e="T33" id="Seg_2205" s="T32">kim</ta>
            <ta e="T34" id="Seg_2206" s="T33">kim</ta>
            <ta e="T35" id="Seg_2207" s="T34">baːr</ta>
            <ta e="T38" id="Seg_2208" s="T37">kɨːl-lar</ta>
            <ta e="T39" id="Seg_2209" s="T38">baːl-lar</ta>
            <ta e="T42" id="Seg_2210" s="T41">buram-mɨt</ta>
            <ta e="T43" id="Seg_2211" s="T42">katu͡og-a</ta>
            <ta e="T44" id="Seg_2212" s="T43">aldʼam-mɨt</ta>
            <ta e="T45" id="Seg_2213" s="T44">onton</ta>
            <ta e="T46" id="Seg_2214" s="T45">menjaj-daː-n</ta>
            <ta e="T47" id="Seg_2215" s="T46">bar-t-ɨ-bɨt</ta>
            <ta e="T48" id="Seg_2216" s="T47">ama</ta>
            <ta e="T49" id="Seg_2217" s="T48">kör-s-ör-ö</ta>
            <ta e="T50" id="Seg_2218" s="T49">biːr</ta>
            <ta e="T51" id="Seg_2219" s="T50">kɨːl-ɨ</ta>
            <ta e="T52" id="Seg_2220" s="T51">ölör-ön</ta>
            <ta e="T53" id="Seg_2221" s="T52">keːs-pip-pit</ta>
            <ta e="T57" id="Seg_2222" s="T56">onton</ta>
            <ta e="T58" id="Seg_2223" s="T57">dalše</ta>
            <ta e="T59" id="Seg_2224" s="T58">bar-a-bɨt</ta>
            <ta e="T60" id="Seg_2225" s="T59">hül-ü-m-müp-püt</ta>
            <ta e="T61" id="Seg_2226" s="T60">onton</ta>
            <ta e="T62" id="Seg_2227" s="T61">dalše </ta>
            <ta e="T63" id="Seg_2228" s="T62">bar-bɨp-pɨt</ta>
            <ta e="T64" id="Seg_2229" s="T63">onno</ta>
            <ta e="T65" id="Seg_2230" s="T64">emi͡e</ta>
            <ta e="T66" id="Seg_2231" s="T65">kɨːl-lar</ta>
            <ta e="T67" id="Seg_2232" s="T66">baːl-lar</ta>
            <ta e="T68" id="Seg_2233" s="T67">o-nu</ta>
            <ta e="T69" id="Seg_2234" s="T68">gojobuːn-naː-bɨp-pɨt</ta>
            <ta e="T70" id="Seg_2235" s="T69">bul-batap-pɨt</ta>
            <ta e="T71" id="Seg_2236" s="T70">onton</ta>
            <ta e="T73" id="Seg_2237" s="T71">kel-ti-bit</ta>
            <ta e="T78" id="Seg_2238" s="T77">tɨ͡a-ga</ta>
            <ta e="T84" id="Seg_2239" s="T83">maːma-m</ta>
            <ta e="T85" id="Seg_2240" s="T84">Ženja</ta>
            <ta e="T212" id="Seg_2241" s="T85">kim</ta>
            <ta e="T87" id="Seg_2242" s="T213">Nata-laːk</ta>
            <ta e="T88" id="Seg_2243" s="T210">Nata-laːk</ta>
            <ta e="T91" id="Seg_2244" s="T88">hu͡ok</ta>
            <ta e="T95" id="Seg_2245" s="T94">onton</ta>
            <ta e="T96" id="Seg_2246" s="T95">harsɨ͡arda</ta>
            <ta e="T97" id="Seg_2247" s="T96">bar-d-a</ta>
            <ta e="T98" id="Seg_2248" s="T97">a</ta>
            <ta e="T99" id="Seg_2249" s="T98">Diːma</ta>
            <ta e="T100" id="Seg_2250" s="T99">bar-d-a</ta>
            <ta e="T101" id="Seg_2251" s="T100">harsɨ͡arda</ta>
            <ta e="T102" id="Seg_2252" s="T101">kɨːl-ɨ</ta>
            <ta e="T103" id="Seg_2253" s="T102">körd-üː</ta>
            <ta e="T104" id="Seg_2254" s="T103">onton</ta>
            <ta e="T105" id="Seg_2255" s="T104">onton</ta>
            <ta e="T106" id="Seg_2256" s="T105">Mɨkaː</ta>
            <ta e="T107" id="Seg_2257" s="T106">kim-ŋe</ta>
            <ta e="T108" id="Seg_2258" s="T107">bar-d-a</ta>
            <ta e="T109" id="Seg_2259" s="T108">balɨk-tan-a</ta>
            <ta e="T112" id="Seg_2260" s="T111">onton</ta>
            <ta e="T113" id="Seg_2261" s="T112">harsɨŋŋɨ-tɨ-n</ta>
            <ta e="T114" id="Seg_2262" s="T113">Nata-laːk</ta>
            <ta e="T115" id="Seg_2263" s="T114">kel-bit-tere</ta>
            <ta e="T122" id="Seg_2264" s="T121">hu͡ok</ta>
            <ta e="T123" id="Seg_2265" s="T122">ölör-ö</ta>
            <ta e="T124" id="Seg_2266" s="T123">ilik</ta>
            <ta e="T129" id="Seg_2267" s="T128">kanna</ta>
            <ta e="T135" id="Seg_2268" s="T134">biːr</ta>
            <ta e="T136" id="Seg_2269" s="T135">kɨːl-ɨ</ta>
            <ta e="T142" id="Seg_2270" s="T141">de</ta>
            <ta e="T143" id="Seg_2271" s="T142">ikki</ta>
            <ta e="T144" id="Seg_2272" s="T143">kɨːl-ɨ</ta>
            <ta e="T145" id="Seg_2273" s="T144">e-t-e</ta>
            <ta e="T146" id="Seg_2274" s="T145">di͡e-n</ta>
            <ta e="T149" id="Seg_2275" s="T148">onno</ta>
            <ta e="T150" id="Seg_2276" s="T149">ikki</ta>
            <ta e="T152" id="Seg_2277" s="T151">biːr</ta>
            <ta e="T154" id="Seg_2278" s="T152">biːr-i-n</ta>
            <ta e="T155" id="Seg_2279" s="T154">ölör-büt</ta>
            <ta e="T156" id="Seg_2280" s="T155">onton</ta>
            <ta e="T157" id="Seg_2281" s="T156">kel-bit-e</ta>
            <ta e="T169" id="Seg_2282" s="T168">bar-ar-bɨtɨ-gar</ta>
            <ta e="T170" id="Seg_2283" s="T169">Sɨndaːssko</ta>
            <ta e="T171" id="Seg_2284" s="T170">kiːr-er</ta>
            <ta e="T177" id="Seg_2285" s="T176">Sɨndaːssko</ta>
            <ta e="T178" id="Seg_2286" s="T177">kiːr-eːri</ta>
            <ta e="T179" id="Seg_2287" s="T178">bar-dɨ-bɨt</ta>
            <ta e="T180" id="Seg_2288" s="T179">buran-ɨ-nan</ta>
            <ta e="T181" id="Seg_2289" s="T180">Mɨkaː</ta>
            <ta e="T182" id="Seg_2290" s="T181">ta</ta>
            <ta e="T183" id="Seg_2291" s="T182">maːma-m</ta>
            <ta e="T186" id="Seg_2292" s="T185">palaːka-lar</ta>
            <ta e="T187" id="Seg_2293" s="T186">ke</ta>
            <ta e="T190" id="Seg_2294" s="T189">tiriː-ler</ta>
            <ta e="T193" id="Seg_2295" s="T192">onton</ta>
            <ta e="T200" id="Seg_2296" s="T199">buran</ta>
            <ta e="T201" id="Seg_2297" s="T200">vodit-taː-bɨt-ɨ-m</ta>
            <ta e="T204" id="Seg_2298" s="T203">Hɨndaːska</ta>
            <ta e="T205" id="Seg_2299" s="T204">kel-ti-bit</ta>
         </annotation>
         <annotation name="mp" tierref="mp-AnMSp">
            <ta e="T11" id="Seg_2300" s="T10">bar-Ar</ta>
            <ta e="T12" id="Seg_2301" s="T11">kim</ta>
            <ta e="T13" id="Seg_2302" s="T12">Hɨndaːska</ta>
            <ta e="T14" id="Seg_2303" s="T13">eː</ta>
            <ta e="T15" id="Seg_2304" s="T14">tɨ͡a-GA</ta>
            <ta e="T16" id="Seg_2305" s="T15">bar-A-BIt</ta>
            <ta e="T17" id="Seg_2306" s="T16">onton</ta>
            <ta e="T18" id="Seg_2307" s="T17">bar-A</ta>
            <ta e="T19" id="Seg_2308" s="T18">tur-A-BIt</ta>
            <ta e="T25" id="Seg_2309" s="T24">Diːma</ta>
            <ta e="T26" id="Seg_2310" s="T25">onton</ta>
            <ta e="T27" id="Seg_2311" s="T26">Mikaː</ta>
            <ta e="T30" id="Seg_2312" s="T29">onton</ta>
            <ta e="T31" id="Seg_2313" s="T30">onno</ta>
            <ta e="T32" id="Seg_2314" s="T31">bar-A-BIt</ta>
            <ta e="T33" id="Seg_2315" s="T32">kim</ta>
            <ta e="T34" id="Seg_2316" s="T33">kim</ta>
            <ta e="T35" id="Seg_2317" s="T34">baːr</ta>
            <ta e="T38" id="Seg_2318" s="T37">kɨːl-LAr</ta>
            <ta e="T39" id="Seg_2319" s="T38">baːr-LAr</ta>
            <ta e="T42" id="Seg_2320" s="T41">buran-BIt</ta>
            <ta e="T43" id="Seg_2321" s="T42">katu͡ok-tA</ta>
            <ta e="T44" id="Seg_2322" s="T43">aldʼat-BIT</ta>
            <ta e="T45" id="Seg_2323" s="T44">onton</ta>
            <ta e="T46" id="Seg_2324" s="T45">menjaj-LAː-An</ta>
            <ta e="T47" id="Seg_2325" s="T46">bar-BIT-I-BIt</ta>
            <ta e="T48" id="Seg_2326" s="T47">ama</ta>
            <ta e="T49" id="Seg_2327" s="T48">kör-s-IAr-A</ta>
            <ta e="T50" id="Seg_2328" s="T49">biːr</ta>
            <ta e="T51" id="Seg_2329" s="T50">kɨːl-nI</ta>
            <ta e="T52" id="Seg_2330" s="T51">ölör-An</ta>
            <ta e="T53" id="Seg_2331" s="T52">keːs-BIT-BIt</ta>
            <ta e="T57" id="Seg_2332" s="T56">onton</ta>
            <ta e="T58" id="Seg_2333" s="T57">dalše</ta>
            <ta e="T59" id="Seg_2334" s="T58">bar-A-BIt</ta>
            <ta e="T60" id="Seg_2335" s="T59">hül-I-n-BIT-BIt</ta>
            <ta e="T61" id="Seg_2336" s="T60">onton</ta>
            <ta e="T62" id="Seg_2337" s="T61">dalše</ta>
            <ta e="T63" id="Seg_2338" s="T62">bar-BIT-BIt</ta>
            <ta e="T64" id="Seg_2339" s="T63">onno</ta>
            <ta e="T65" id="Seg_2340" s="T64">emi͡e</ta>
            <ta e="T66" id="Seg_2341" s="T65">kɨːl-LAr</ta>
            <ta e="T67" id="Seg_2342" s="T66">baːr-LAr</ta>
            <ta e="T68" id="Seg_2343" s="T67">ol-nI</ta>
            <ta e="T69" id="Seg_2344" s="T68">gojobuːn-LAː-BIT-BIt</ta>
            <ta e="T70" id="Seg_2345" s="T69">bul-BAtAK-BIt</ta>
            <ta e="T71" id="Seg_2346" s="T70">onton</ta>
            <ta e="T73" id="Seg_2347" s="T71">kel-TI-BIt</ta>
            <ta e="T78" id="Seg_2348" s="T77">tɨ͡a-GA</ta>
            <ta e="T84" id="Seg_2349" s="T83">maːma-m</ta>
            <ta e="T85" id="Seg_2350" s="T84">Ženja</ta>
            <ta e="T212" id="Seg_2351" s="T85">kim</ta>
            <ta e="T87" id="Seg_2352" s="T213">Nata-LAːK</ta>
            <ta e="T88" id="Seg_2353" s="T210">Nata-LAːK</ta>
            <ta e="T91" id="Seg_2354" s="T88">hu͡ok</ta>
            <ta e="T95" id="Seg_2355" s="T94">onton</ta>
            <ta e="T96" id="Seg_2356" s="T95">harsi͡erda</ta>
            <ta e="T97" id="Seg_2357" s="T96">bar-TI-tA</ta>
            <ta e="T98" id="Seg_2358" s="T97">a</ta>
            <ta e="T99" id="Seg_2359" s="T98">Diːma</ta>
            <ta e="T100" id="Seg_2360" s="T99">bar-TI-tA</ta>
            <ta e="T101" id="Seg_2361" s="T100">harsi͡erda</ta>
            <ta e="T102" id="Seg_2362" s="T101">kɨːl-nI</ta>
            <ta e="T103" id="Seg_2363" s="T102">kördöː-A</ta>
            <ta e="T104" id="Seg_2364" s="T103">onton</ta>
            <ta e="T105" id="Seg_2365" s="T104">onton</ta>
            <ta e="T106" id="Seg_2366" s="T105">Mikaː</ta>
            <ta e="T107" id="Seg_2367" s="T106">kim-GA</ta>
            <ta e="T108" id="Seg_2368" s="T107">bar-TI-tA</ta>
            <ta e="T109" id="Seg_2369" s="T108">balɨk-LAN-A</ta>
            <ta e="T112" id="Seg_2370" s="T111">onton</ta>
            <ta e="T113" id="Seg_2371" s="T112">harsɨŋŋɨ-tI-n</ta>
            <ta e="T114" id="Seg_2372" s="T113">Nata-LAːK</ta>
            <ta e="T115" id="Seg_2373" s="T114">kel-BIT-LArA</ta>
            <ta e="T122" id="Seg_2374" s="T121">hu͡ok</ta>
            <ta e="T123" id="Seg_2375" s="T122">ölör-A</ta>
            <ta e="T124" id="Seg_2376" s="T123">ilik</ta>
            <ta e="T129" id="Seg_2377" s="T128">kanna</ta>
            <ta e="T135" id="Seg_2378" s="T134">biːr</ta>
            <ta e="T136" id="Seg_2379" s="T135">kɨːl-nI</ta>
            <ta e="T142" id="Seg_2380" s="T141">dʼe</ta>
            <ta e="T143" id="Seg_2381" s="T142">ikki</ta>
            <ta e="T144" id="Seg_2382" s="T143">kɨːl-nI</ta>
            <ta e="T145" id="Seg_2383" s="T144">e-TI-tA</ta>
            <ta e="T146" id="Seg_2384" s="T145">di͡e-An</ta>
            <ta e="T149" id="Seg_2385" s="T148">onno</ta>
            <ta e="T150" id="Seg_2386" s="T149">ikki</ta>
            <ta e="T152" id="Seg_2387" s="T151">biːr</ta>
            <ta e="T154" id="Seg_2388" s="T152">biːr-tI-n</ta>
            <ta e="T155" id="Seg_2389" s="T154">ölör-BIT</ta>
            <ta e="T156" id="Seg_2390" s="T155">onton</ta>
            <ta e="T157" id="Seg_2391" s="T156">kel-BIT-tA</ta>
            <ta e="T169" id="Seg_2392" s="T168">bar-Ar-BItI-GAr</ta>
            <ta e="T170" id="Seg_2393" s="T169">Hɨndaːska</ta>
            <ta e="T171" id="Seg_2394" s="T170">kiːr-Ar</ta>
            <ta e="T177" id="Seg_2395" s="T176">Hɨndaːska</ta>
            <ta e="T178" id="Seg_2396" s="T177">kiːr-AːrI</ta>
            <ta e="T179" id="Seg_2397" s="T178">bar-TI-BIt</ta>
            <ta e="T180" id="Seg_2398" s="T179">buran-tI-nAn</ta>
            <ta e="T181" id="Seg_2399" s="T180">Mikaː</ta>
            <ta e="T182" id="Seg_2400" s="T181">da</ta>
            <ta e="T183" id="Seg_2401" s="T182">maːma-m</ta>
            <ta e="T186" id="Seg_2402" s="T185">palaːka-LAr</ta>
            <ta e="T187" id="Seg_2403" s="T186">ka</ta>
            <ta e="T190" id="Seg_2404" s="T189">tiriː-LAr</ta>
            <ta e="T193" id="Seg_2405" s="T192">onton</ta>
            <ta e="T200" id="Seg_2406" s="T199">buran</ta>
            <ta e="T201" id="Seg_2407" s="T200">vodit-LAː-BIT-I-m</ta>
            <ta e="T204" id="Seg_2408" s="T203">Hɨndaːska</ta>
            <ta e="T205" id="Seg_2409" s="T204">kel-TI-BIt</ta>
         </annotation>
         <annotation name="ge" tierref="ge-AnMSp">
            <ta e="T11" id="Seg_2410" s="T10">go-PRS.[3SG]</ta>
            <ta e="T12" id="Seg_2411" s="T11">who.[NOM]</ta>
            <ta e="T13" id="Seg_2412" s="T12">Syndassko.[NOM]</ta>
            <ta e="T14" id="Seg_2413" s="T13">AFFIRM</ta>
            <ta e="T15" id="Seg_2414" s="T14">tundra-DAT/LOC</ta>
            <ta e="T16" id="Seg_2415" s="T15">go-PRS-1PL</ta>
            <ta e="T17" id="Seg_2416" s="T16">then</ta>
            <ta e="T18" id="Seg_2417" s="T17">go-CVB.SIM</ta>
            <ta e="T19" id="Seg_2418" s="T18">stand-PRS-1PL</ta>
            <ta e="T25" id="Seg_2419" s="T24">Dima.[NOM]</ta>
            <ta e="T26" id="Seg_2420" s="T25">then</ta>
            <ta e="T27" id="Seg_2421" s="T26">Mika.[NOM]</ta>
            <ta e="T30" id="Seg_2422" s="T29">then</ta>
            <ta e="T31" id="Seg_2423" s="T30">thither</ta>
            <ta e="T32" id="Seg_2424" s="T31">go-PRS-1PL</ta>
            <ta e="T33" id="Seg_2425" s="T32">who.[NOM]</ta>
            <ta e="T34" id="Seg_2426" s="T33">who.[NOM]</ta>
            <ta e="T35" id="Seg_2427" s="T34">there.is</ta>
            <ta e="T38" id="Seg_2428" s="T37">wild.reindeer-PL.[NOM]</ta>
            <ta e="T39" id="Seg_2429" s="T38">there.is-3PL</ta>
            <ta e="T42" id="Seg_2430" s="T41">snow.scooter-1PL.[NOM]</ta>
            <ta e="T43" id="Seg_2431" s="T42">runner-3SG.[NOM]</ta>
            <ta e="T44" id="Seg_2432" s="T43">break-PST2.[3SG]</ta>
            <ta e="T45" id="Seg_2433" s="T44">then</ta>
            <ta e="T46" id="Seg_2434" s="T45">change-VBZ-CVB.SEQ</ta>
            <ta e="T47" id="Seg_2435" s="T46">go-PST2-EP-1PL</ta>
            <ta e="T48" id="Seg_2436" s="T47">EMPH</ta>
            <ta e="T49" id="Seg_2437" s="T48">see-RECP/COLL-CAUS-CVB.SIM</ta>
            <ta e="T50" id="Seg_2438" s="T49">one</ta>
            <ta e="T51" id="Seg_2439" s="T50">wild.reindeer-ACC</ta>
            <ta e="T52" id="Seg_2440" s="T51">kill-CVB.SEQ</ta>
            <ta e="T53" id="Seg_2441" s="T52">throw-PST2-1PL</ta>
            <ta e="T57" id="Seg_2442" s="T56">then</ta>
            <ta e="T58" id="Seg_2443" s="T57">further</ta>
            <ta e="T59" id="Seg_2444" s="T58">go-PRS-1PL</ta>
            <ta e="T60" id="Seg_2445" s="T59">skin-EP-REFL-PST2-1PL</ta>
            <ta e="T61" id="Seg_2446" s="T60">then</ta>
            <ta e="T62" id="Seg_2447" s="T61">further</ta>
            <ta e="T63" id="Seg_2448" s="T62">go-PST2-1PL</ta>
            <ta e="T64" id="Seg_2449" s="T63">there</ta>
            <ta e="T65" id="Seg_2450" s="T64">again</ta>
            <ta e="T66" id="Seg_2451" s="T65">wild.reindeer-PL.[NOM]</ta>
            <ta e="T67" id="Seg_2452" s="T66">there.is-3PL</ta>
            <ta e="T68" id="Seg_2453" s="T67">that-ACC</ta>
            <ta e="T69" id="Seg_2454" s="T68">wound-VBZ-PST2-1PL</ta>
            <ta e="T70" id="Seg_2455" s="T69">find-PST2.NEG-1PL</ta>
            <ta e="T71" id="Seg_2456" s="T70">then</ta>
            <ta e="T73" id="Seg_2457" s="T71">come-PST1-1PL</ta>
            <ta e="T78" id="Seg_2458" s="T77">tundra-DAT/LOC</ta>
            <ta e="T84" id="Seg_2459" s="T83">mum-1SG.[NOM]</ta>
            <ta e="T85" id="Seg_2460" s="T84">Zhenja.[NOM]</ta>
            <ta e="T212" id="Seg_2461" s="T85">who.[NOM]</ta>
            <ta e="T87" id="Seg_2462" s="T213">Nata-PROPR.[NOM]</ta>
            <ta e="T88" id="Seg_2463" s="T210">Nata-PROPR.[NOM]</ta>
            <ta e="T91" id="Seg_2464" s="T88">NEG.EX</ta>
            <ta e="T95" id="Seg_2465" s="T94">then</ta>
            <ta e="T96" id="Seg_2466" s="T95">in.the.morning</ta>
            <ta e="T97" id="Seg_2467" s="T96">go-PST1-3SG</ta>
            <ta e="T98" id="Seg_2468" s="T97">and</ta>
            <ta e="T99" id="Seg_2469" s="T98">Dima.[NOM]</ta>
            <ta e="T100" id="Seg_2470" s="T99">go-PST1-3SG</ta>
            <ta e="T101" id="Seg_2471" s="T100">in.the.morning</ta>
            <ta e="T102" id="Seg_2472" s="T101">wild.reindeer-ACC</ta>
            <ta e="T103" id="Seg_2473" s="T102">search-CVB.SIM</ta>
            <ta e="T104" id="Seg_2474" s="T103">then</ta>
            <ta e="T105" id="Seg_2475" s="T104">then</ta>
            <ta e="T106" id="Seg_2476" s="T105">Mika</ta>
            <ta e="T107" id="Seg_2477" s="T106">who-DAT/LOC</ta>
            <ta e="T108" id="Seg_2478" s="T107">go-PST1-3SG</ta>
            <ta e="T109" id="Seg_2479" s="T108">fish-VBZ-CVB.SIM</ta>
            <ta e="T112" id="Seg_2480" s="T111">then</ta>
            <ta e="T113" id="Seg_2481" s="T112">next.morning-3SG-ACC</ta>
            <ta e="T114" id="Seg_2482" s="T113">Nata-PROPR.[NOM]</ta>
            <ta e="T115" id="Seg_2483" s="T114">come-PST2-3PL</ta>
            <ta e="T122" id="Seg_2484" s="T121">no</ta>
            <ta e="T123" id="Seg_2485" s="T122">kill-CVB.SIM</ta>
            <ta e="T124" id="Seg_2486" s="T123">not.yet.[3SG]</ta>
            <ta e="T129" id="Seg_2487" s="T128">where</ta>
            <ta e="T135" id="Seg_2488" s="T134">one</ta>
            <ta e="T136" id="Seg_2489" s="T135">wild.reindeer-ACC</ta>
            <ta e="T142" id="Seg_2490" s="T141">well</ta>
            <ta e="T143" id="Seg_2491" s="T142">two</ta>
            <ta e="T144" id="Seg_2492" s="T143">wild.reindeer-ACC</ta>
            <ta e="T145" id="Seg_2493" s="T144">be-PST1-3SG</ta>
            <ta e="T146" id="Seg_2494" s="T145">say-CVB.SEQ</ta>
            <ta e="T149" id="Seg_2495" s="T148">there</ta>
            <ta e="T150" id="Seg_2496" s="T149">two</ta>
            <ta e="T152" id="Seg_2497" s="T151">one</ta>
            <ta e="T154" id="Seg_2498" s="T152">one-3SG-ACC</ta>
            <ta e="T155" id="Seg_2499" s="T154">kill-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_2500" s="T155">then</ta>
            <ta e="T157" id="Seg_2501" s="T156">come-PST2-3SG</ta>
            <ta e="T169" id="Seg_2502" s="T168">go-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T170" id="Seg_2503" s="T169">Syndassko.[NOM]</ta>
            <ta e="T171" id="Seg_2504" s="T170">go.in-PRS.[3SG]</ta>
            <ta e="T177" id="Seg_2505" s="T176">Syndassko</ta>
            <ta e="T178" id="Seg_2506" s="T177">go.in-CVB.PURP</ta>
            <ta e="T179" id="Seg_2507" s="T178">go-PST1-1PL</ta>
            <ta e="T180" id="Seg_2508" s="T179">snow.scooter-3SG-INSTR</ta>
            <ta e="T181" id="Seg_2509" s="T180">Mika</ta>
            <ta e="T182" id="Seg_2510" s="T181">and</ta>
            <ta e="T183" id="Seg_2511" s="T182">mum-1SG.[NOM]</ta>
            <ta e="T186" id="Seg_2512" s="T185">tent-PL</ta>
            <ta e="T187" id="Seg_2513" s="T186">well</ta>
            <ta e="T190" id="Seg_2514" s="T189">fur-PL.[NOM]</ta>
            <ta e="T193" id="Seg_2515" s="T192">then</ta>
            <ta e="T200" id="Seg_2516" s="T199">snow.scooter.[NOM]</ta>
            <ta e="T201" id="Seg_2517" s="T200">drive-VBZ-PST2-EP-1SG</ta>
            <ta e="T204" id="Seg_2518" s="T203">Syndassko.[NOM]</ta>
            <ta e="T205" id="Seg_2519" s="T204">come-PST1-1PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg-AnMSp">
            <ta e="T11" id="Seg_2520" s="T10">gehen-PRS.[3SG]</ta>
            <ta e="T12" id="Seg_2521" s="T11">wer.[NOM]</ta>
            <ta e="T13" id="Seg_2522" s="T12">Syndassko.[NOM]</ta>
            <ta e="T14" id="Seg_2523" s="T13">AFFIRM</ta>
            <ta e="T15" id="Seg_2524" s="T14">Tundra-DAT/LOC</ta>
            <ta e="T16" id="Seg_2525" s="T15">gehen-PRS-1PL</ta>
            <ta e="T17" id="Seg_2526" s="T16">dann</ta>
            <ta e="T18" id="Seg_2527" s="T17">gehen-CVB.SIM</ta>
            <ta e="T19" id="Seg_2528" s="T18">stehen-PRS-1PL</ta>
            <ta e="T25" id="Seg_2529" s="T24">Dima.[NOM]</ta>
            <ta e="T26" id="Seg_2530" s="T25">dann</ta>
            <ta e="T27" id="Seg_2531" s="T26">Mika.[NOM]</ta>
            <ta e="T30" id="Seg_2532" s="T29">dann</ta>
            <ta e="T31" id="Seg_2533" s="T30">dorthin</ta>
            <ta e="T32" id="Seg_2534" s="T31">gehen-PRS-1PL</ta>
            <ta e="T33" id="Seg_2535" s="T32">wer.[NOM]</ta>
            <ta e="T34" id="Seg_2536" s="T33">wer.[NOM]</ta>
            <ta e="T35" id="Seg_2537" s="T34">es.gibt</ta>
            <ta e="T38" id="Seg_2538" s="T37">wildes.Rentier-PL.[NOM]</ta>
            <ta e="T39" id="Seg_2539" s="T38">es.gibt-3PL</ta>
            <ta e="T42" id="Seg_2540" s="T41">Schneemobil-1PL.[NOM]</ta>
            <ta e="T43" id="Seg_2541" s="T42">Kufe-3SG.[NOM]</ta>
            <ta e="T44" id="Seg_2542" s="T43">zerbrechen-PST2.[3SG]</ta>
            <ta e="T45" id="Seg_2543" s="T44">dann</ta>
            <ta e="T46" id="Seg_2544" s="T45">wechseln-VBZ-CVB.SEQ</ta>
            <ta e="T47" id="Seg_2545" s="T46">gehen-PST2-EP-1PL</ta>
            <ta e="T48" id="Seg_2546" s="T47">EMPH</ta>
            <ta e="T49" id="Seg_2547" s="T48">sehen-RECP/COLL-CAUS-CVB.SIM</ta>
            <ta e="T50" id="Seg_2548" s="T49">eins</ta>
            <ta e="T51" id="Seg_2549" s="T50">wildes.Rentier-ACC</ta>
            <ta e="T52" id="Seg_2550" s="T51">töten-CVB.SEQ</ta>
            <ta e="T53" id="Seg_2551" s="T52">werfen-PST2-1PL</ta>
            <ta e="T57" id="Seg_2552" s="T56">dann</ta>
            <ta e="T58" id="Seg_2553" s="T57">weiter</ta>
            <ta e="T59" id="Seg_2554" s="T58">gehen-PRS-1PL</ta>
            <ta e="T60" id="Seg_2555" s="T59">Haut.abziehen-EP-REFL-PST2-1PL</ta>
            <ta e="T61" id="Seg_2556" s="T60">dann</ta>
            <ta e="T62" id="Seg_2557" s="T61">weiter</ta>
            <ta e="T63" id="Seg_2558" s="T62">gehen-PST2-1PL</ta>
            <ta e="T64" id="Seg_2559" s="T63">dort</ta>
            <ta e="T65" id="Seg_2560" s="T64">wieder</ta>
            <ta e="T66" id="Seg_2561" s="T65">wildes.Rentier-PL.[NOM]</ta>
            <ta e="T67" id="Seg_2562" s="T66">es.gibt-3PL</ta>
            <ta e="T68" id="Seg_2563" s="T67">jenes-ACC</ta>
            <ta e="T69" id="Seg_2564" s="T68">Wunde-VBZ-PST2-1PL</ta>
            <ta e="T70" id="Seg_2565" s="T69">finden-PST2.NEG-1PL</ta>
            <ta e="T71" id="Seg_2566" s="T70">dann</ta>
            <ta e="T73" id="Seg_2567" s="T71">kommen-PST1-1PL</ta>
            <ta e="T78" id="Seg_2568" s="T77">Tundra-DAT/LOC</ta>
            <ta e="T84" id="Seg_2569" s="T83">Mama-1SG.[NOM]</ta>
            <ta e="T85" id="Seg_2570" s="T84">Zhenja.[NOM]</ta>
            <ta e="T212" id="Seg_2571" s="T85">wer.[NOM]</ta>
            <ta e="T87" id="Seg_2572" s="T213">Nata-PROPR.[NOM]</ta>
            <ta e="T88" id="Seg_2573" s="T210">Nata-PROPR.[NOM]</ta>
            <ta e="T91" id="Seg_2574" s="T88">NEG.EX</ta>
            <ta e="T95" id="Seg_2575" s="T94">dann</ta>
            <ta e="T96" id="Seg_2576" s="T95">am.Morgen</ta>
            <ta e="T97" id="Seg_2577" s="T96">gehen-PST1-3SG</ta>
            <ta e="T98" id="Seg_2578" s="T97">und</ta>
            <ta e="T99" id="Seg_2579" s="T98">Dima.[NOM]</ta>
            <ta e="T100" id="Seg_2580" s="T99">gehen-PST1-3SG</ta>
            <ta e="T101" id="Seg_2581" s="T100">am.Morgen</ta>
            <ta e="T102" id="Seg_2582" s="T101">wildes.Rentier-ACC</ta>
            <ta e="T103" id="Seg_2583" s="T102">suchen-CVB.SIM</ta>
            <ta e="T104" id="Seg_2584" s="T103">dann</ta>
            <ta e="T105" id="Seg_2585" s="T104">dann</ta>
            <ta e="T106" id="Seg_2586" s="T105">Mika</ta>
            <ta e="T107" id="Seg_2587" s="T106">wer-DAT/LOC</ta>
            <ta e="T108" id="Seg_2588" s="T107">gehen-PST1-3SG</ta>
            <ta e="T109" id="Seg_2589" s="T108">Fisch-VBZ-CVB.SIM</ta>
            <ta e="T112" id="Seg_2590" s="T111">dann</ta>
            <ta e="T113" id="Seg_2591" s="T112">nächster.Morgen-3SG-ACC</ta>
            <ta e="T114" id="Seg_2592" s="T113">Nata-PROPR.[NOM]</ta>
            <ta e="T115" id="Seg_2593" s="T114">kommen-PST2-3PL</ta>
            <ta e="T122" id="Seg_2594" s="T121">nein</ta>
            <ta e="T123" id="Seg_2595" s="T122">töten-CVB.SIM</ta>
            <ta e="T124" id="Seg_2596" s="T123">noch.nicht.[3SG]</ta>
            <ta e="T129" id="Seg_2597" s="T128">wo</ta>
            <ta e="T135" id="Seg_2598" s="T134">eins</ta>
            <ta e="T136" id="Seg_2599" s="T135">wildes.Rentier-ACC</ta>
            <ta e="T142" id="Seg_2600" s="T141">doch</ta>
            <ta e="T143" id="Seg_2601" s="T142">zwei</ta>
            <ta e="T144" id="Seg_2602" s="T143">wildes.Rentier-ACC</ta>
            <ta e="T145" id="Seg_2603" s="T144">sein-PST1-3SG</ta>
            <ta e="T146" id="Seg_2604" s="T145">sagen-CVB.SEQ</ta>
            <ta e="T149" id="Seg_2605" s="T148">dort</ta>
            <ta e="T150" id="Seg_2606" s="T149">zwei</ta>
            <ta e="T152" id="Seg_2607" s="T151">eins</ta>
            <ta e="T154" id="Seg_2608" s="T152">eins-3SG-ACC</ta>
            <ta e="T155" id="Seg_2609" s="T154">töten-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_2610" s="T155">dann</ta>
            <ta e="T157" id="Seg_2611" s="T156">kommen-PST2-3SG</ta>
            <ta e="T169" id="Seg_2612" s="T168">gehen-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T170" id="Seg_2613" s="T169">Syndassko.[NOM]</ta>
            <ta e="T171" id="Seg_2614" s="T170">hineingehen-PRS.[3SG]</ta>
            <ta e="T177" id="Seg_2615" s="T176">Syndassko</ta>
            <ta e="T178" id="Seg_2616" s="T177">hineingehen-CVB.PURP</ta>
            <ta e="T179" id="Seg_2617" s="T178">gehen-PST1-1PL</ta>
            <ta e="T180" id="Seg_2618" s="T179">Schneemobil-3SG-INSTR</ta>
            <ta e="T181" id="Seg_2619" s="T180">Mika</ta>
            <ta e="T182" id="Seg_2620" s="T181">und</ta>
            <ta e="T183" id="Seg_2621" s="T182">Mama-1SG.[NOM]</ta>
            <ta e="T186" id="Seg_2622" s="T185">Zelt-PL</ta>
            <ta e="T187" id="Seg_2623" s="T186">nun</ta>
            <ta e="T190" id="Seg_2624" s="T189">Fell-PL.[NOM]</ta>
            <ta e="T193" id="Seg_2625" s="T192">dann</ta>
            <ta e="T200" id="Seg_2626" s="T199">Schneemobil.[NOM]</ta>
            <ta e="T201" id="Seg_2627" s="T200">fahren-VBZ-PST2-EP-1SG</ta>
            <ta e="T204" id="Seg_2628" s="T203">Syndassko.[NOM]</ta>
            <ta e="T205" id="Seg_2629" s="T204">kommen-PST1-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr-AnMSp">
            <ta e="T11" id="Seg_2630" s="T10">идти-PRS.[3SG]</ta>
            <ta e="T12" id="Seg_2631" s="T11">кто.[NOM]</ta>
            <ta e="T13" id="Seg_2632" s="T12">Сындасско.[NOM]</ta>
            <ta e="T14" id="Seg_2633" s="T13">AFFIRM</ta>
            <ta e="T15" id="Seg_2634" s="T14">тундра-DAT/LOC</ta>
            <ta e="T16" id="Seg_2635" s="T15">идти-PRS-1PL</ta>
            <ta e="T17" id="Seg_2636" s="T16">потом</ta>
            <ta e="T18" id="Seg_2637" s="T17">идти-CVB.SIM</ta>
            <ta e="T19" id="Seg_2638" s="T18">стоять-PRS-1PL</ta>
            <ta e="T25" id="Seg_2639" s="T24">Дима.[NOM]</ta>
            <ta e="T26" id="Seg_2640" s="T25">потом</ta>
            <ta e="T27" id="Seg_2641" s="T26">Мика.[NOM]</ta>
            <ta e="T30" id="Seg_2642" s="T29">потом</ta>
            <ta e="T31" id="Seg_2643" s="T30">туда</ta>
            <ta e="T32" id="Seg_2644" s="T31">идти-PRS-1PL</ta>
            <ta e="T33" id="Seg_2645" s="T32">кто.[NOM]</ta>
            <ta e="T34" id="Seg_2646" s="T33">кто.[NOM]</ta>
            <ta e="T35" id="Seg_2647" s="T34">есть</ta>
            <ta e="T38" id="Seg_2648" s="T37">дикий.олень-PL.[NOM]</ta>
            <ta e="T39" id="Seg_2649" s="T38">есть-3PL</ta>
            <ta e="T42" id="Seg_2650" s="T41">буран-1PL.[NOM]</ta>
            <ta e="T43" id="Seg_2651" s="T42">каток-3SG.[NOM]</ta>
            <ta e="T44" id="Seg_2652" s="T43">разбивать-PST2.[3SG]</ta>
            <ta e="T45" id="Seg_2653" s="T44">потом</ta>
            <ta e="T46" id="Seg_2654" s="T45">менять-VBZ-CVB.SEQ</ta>
            <ta e="T47" id="Seg_2655" s="T46">идти-PST2-EP-1PL</ta>
            <ta e="T48" id="Seg_2656" s="T47">EMPH</ta>
            <ta e="T49" id="Seg_2657" s="T48">видеть-RECP/COLL-CAUS-CVB.SIM</ta>
            <ta e="T50" id="Seg_2658" s="T49">один</ta>
            <ta e="T51" id="Seg_2659" s="T50">дикий.олень-ACC</ta>
            <ta e="T52" id="Seg_2660" s="T51">убить-CVB.SEQ</ta>
            <ta e="T53" id="Seg_2661" s="T52">бросать-PST2-1PL</ta>
            <ta e="T57" id="Seg_2662" s="T56">потом</ta>
            <ta e="T58" id="Seg_2663" s="T57">дальше</ta>
            <ta e="T59" id="Seg_2664" s="T58">идти-PRS-1PL</ta>
            <ta e="T60" id="Seg_2665" s="T59">сдирать.кожу-EP-REFL-PST2-1PL</ta>
            <ta e="T61" id="Seg_2666" s="T60">потом</ta>
            <ta e="T62" id="Seg_2667" s="T61">дальше</ta>
            <ta e="T63" id="Seg_2668" s="T62">идти-PST2-1PL</ta>
            <ta e="T64" id="Seg_2669" s="T63">там</ta>
            <ta e="T65" id="Seg_2670" s="T64">опять</ta>
            <ta e="T66" id="Seg_2671" s="T65">дикий.олень-PL.[NOM]</ta>
            <ta e="T67" id="Seg_2672" s="T66">есть-3PL</ta>
            <ta e="T68" id="Seg_2673" s="T67">тот-ACC</ta>
            <ta e="T69" id="Seg_2674" s="T68">рана-VBZ-PST2-1PL</ta>
            <ta e="T70" id="Seg_2675" s="T69">найти-PST2.NEG-1PL</ta>
            <ta e="T71" id="Seg_2676" s="T70">потом</ta>
            <ta e="T73" id="Seg_2677" s="T71">приходить-PST1-1PL</ta>
            <ta e="T78" id="Seg_2678" s="T77">тундра-DAT/LOC</ta>
            <ta e="T84" id="Seg_2679" s="T83">мама-1SG.[NOM]</ta>
            <ta e="T85" id="Seg_2680" s="T84">Женя.[NOM]</ta>
            <ta e="T212" id="Seg_2681" s="T85">кто.[NOM]</ta>
            <ta e="T87" id="Seg_2682" s="T213">Ната-PROPR.[NOM]</ta>
            <ta e="T88" id="Seg_2683" s="T210">Ната-PROPR.[NOM]</ta>
            <ta e="T91" id="Seg_2684" s="T88">NEG.EX</ta>
            <ta e="T95" id="Seg_2685" s="T94">потом</ta>
            <ta e="T96" id="Seg_2686" s="T95">утром</ta>
            <ta e="T97" id="Seg_2687" s="T96">идти-PST1-3SG</ta>
            <ta e="T98" id="Seg_2688" s="T97">а</ta>
            <ta e="T99" id="Seg_2689" s="T98">Дима.[NOM]</ta>
            <ta e="T100" id="Seg_2690" s="T99">идти-PST1-3SG</ta>
            <ta e="T101" id="Seg_2691" s="T100">утром</ta>
            <ta e="T102" id="Seg_2692" s="T101">дикий.олень-ACC</ta>
            <ta e="T103" id="Seg_2693" s="T102">искать-CVB.SIM</ta>
            <ta e="T104" id="Seg_2694" s="T103">потом</ta>
            <ta e="T105" id="Seg_2695" s="T104">потом</ta>
            <ta e="T106" id="Seg_2696" s="T105">Мика</ta>
            <ta e="T107" id="Seg_2697" s="T106">кто-DAT/LOC</ta>
            <ta e="T108" id="Seg_2698" s="T107">идти-PST1-3SG</ta>
            <ta e="T109" id="Seg_2699" s="T108">рыба-VBZ-CVB.SIM</ta>
            <ta e="T112" id="Seg_2700" s="T111">потом</ta>
            <ta e="T113" id="Seg_2701" s="T112">следующее.утро-3SG-ACC</ta>
            <ta e="T114" id="Seg_2702" s="T113">Ната-PROPR.[NOM]</ta>
            <ta e="T115" id="Seg_2703" s="T114">приходить-PST2-3PL</ta>
            <ta e="T122" id="Seg_2704" s="T121">нет</ta>
            <ta e="T123" id="Seg_2705" s="T122">убить-CVB.SIM</ta>
            <ta e="T124" id="Seg_2706" s="T123">еще.не.[3SG]</ta>
            <ta e="T129" id="Seg_2707" s="T128">где</ta>
            <ta e="T135" id="Seg_2708" s="T134">один</ta>
            <ta e="T136" id="Seg_2709" s="T135">дикий.олень-ACC</ta>
            <ta e="T142" id="Seg_2710" s="T141">вот</ta>
            <ta e="T143" id="Seg_2711" s="T142">два</ta>
            <ta e="T144" id="Seg_2712" s="T143">дикий.олень-ACC</ta>
            <ta e="T145" id="Seg_2713" s="T144">быть-PST1-3SG</ta>
            <ta e="T146" id="Seg_2714" s="T145">говорить-CVB.SEQ</ta>
            <ta e="T149" id="Seg_2715" s="T148">там</ta>
            <ta e="T150" id="Seg_2716" s="T149">два</ta>
            <ta e="T152" id="Seg_2717" s="T151">один</ta>
            <ta e="T154" id="Seg_2718" s="T152">один-3SG-ACC</ta>
            <ta e="T155" id="Seg_2719" s="T154">убить-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_2720" s="T155">потом</ta>
            <ta e="T157" id="Seg_2721" s="T156">приходить-PST2-3SG</ta>
            <ta e="T169" id="Seg_2722" s="T168">идти-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T170" id="Seg_2723" s="T169">Сындасско.[NOM]</ta>
            <ta e="T171" id="Seg_2724" s="T170">входить-PRS.[3SG]</ta>
            <ta e="T177" id="Seg_2725" s="T176">Сындасско</ta>
            <ta e="T178" id="Seg_2726" s="T177">входить-CVB.PURP</ta>
            <ta e="T179" id="Seg_2727" s="T178">идти-PST1-1PL</ta>
            <ta e="T180" id="Seg_2728" s="T179">буран-3SG-INSTR</ta>
            <ta e="T181" id="Seg_2729" s="T180">Мика</ta>
            <ta e="T182" id="Seg_2730" s="T181">да</ta>
            <ta e="T183" id="Seg_2731" s="T182">мама-1SG.[NOM]</ta>
            <ta e="T186" id="Seg_2732" s="T185">палатка-PL</ta>
            <ta e="T187" id="Seg_2733" s="T186">вот</ta>
            <ta e="T190" id="Seg_2734" s="T189">шкура-PL.[NOM]</ta>
            <ta e="T193" id="Seg_2735" s="T192">потом</ta>
            <ta e="T200" id="Seg_2736" s="T199">буран.[NOM]</ta>
            <ta e="T201" id="Seg_2737" s="T200">водить-VBZ-PST2-EP-1SG</ta>
            <ta e="T204" id="Seg_2738" s="T203">Сындасско.[NOM]</ta>
            <ta e="T205" id="Seg_2739" s="T204">приходить-PST1-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc-AnMSp">
            <ta e="T11" id="Seg_2740" s="T10">v-v:tense-v:pred.pn</ta>
            <ta e="T12" id="Seg_2741" s="T11">que-pro:case</ta>
            <ta e="T13" id="Seg_2742" s="T12">propr-n:case</ta>
            <ta e="T14" id="Seg_2743" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_2744" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_2745" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_2746" s="T16">adv</ta>
            <ta e="T18" id="Seg_2747" s="T17">v-v:cvb</ta>
            <ta e="T19" id="Seg_2748" s="T18">aux-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_2749" s="T24">propr.[n:case]</ta>
            <ta e="T26" id="Seg_2750" s="T25">adv</ta>
            <ta e="T27" id="Seg_2751" s="T26">propr-n:case</ta>
            <ta e="T30" id="Seg_2752" s="T29">adv</ta>
            <ta e="T31" id="Seg_2753" s="T30">adv</ta>
            <ta e="T32" id="Seg_2754" s="T31">v-v:tense-v:pred.pn</ta>
            <ta e="T33" id="Seg_2755" s="T32">que-pro:case</ta>
            <ta e="T34" id="Seg_2756" s="T33">que-pro:case</ta>
            <ta e="T35" id="Seg_2757" s="T34">ptcl</ta>
            <ta e="T38" id="Seg_2758" s="T37">n-n:(num)-n:case</ta>
            <ta e="T39" id="Seg_2759" s="T38">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T42" id="Seg_2760" s="T41">n-n:(poss).[n:case]</ta>
            <ta e="T43" id="Seg_2761" s="T42">n-n:(poss)-n:case</ta>
            <ta e="T44" id="Seg_2762" s="T43">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_2763" s="T44">adv</ta>
            <ta e="T46" id="Seg_2764" s="T45">v-v&gt;v-v:cvb</ta>
            <ta e="T47" id="Seg_2765" s="T46">v-v:tense-v:(ins)-v:pred.pn</ta>
            <ta e="T48" id="Seg_2766" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_2767" s="T48">v-v&gt;v-v&gt;v-v:cvb</ta>
            <ta e="T50" id="Seg_2768" s="T49">cardnum</ta>
            <ta e="T51" id="Seg_2769" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_2770" s="T51">v-v:cvb</ta>
            <ta e="T53" id="Seg_2771" s="T52">v-v:tense-v:pred.pn</ta>
            <ta e="T57" id="Seg_2772" s="T56">adv</ta>
            <ta e="T58" id="Seg_2773" s="T57">adv</ta>
            <ta e="T59" id="Seg_2774" s="T58">v-v:tense-v:pred.pn</ta>
            <ta e="T60" id="Seg_2775" s="T59">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T61" id="Seg_2776" s="T60">adv</ta>
            <ta e="T62" id="Seg_2777" s="T61">adv</ta>
            <ta e="T63" id="Seg_2778" s="T62">v-v:tense-v:pred.pn</ta>
            <ta e="T64" id="Seg_2779" s="T63">adv</ta>
            <ta e="T65" id="Seg_2780" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2781" s="T65">n-n:(num)-n:case</ta>
            <ta e="T67" id="Seg_2782" s="T66">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T68" id="Seg_2783" s="T67">dempro-pro:case</ta>
            <ta e="T69" id="Seg_2784" s="T68">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T70" id="Seg_2785" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_2786" s="T70">adv</ta>
            <ta e="T73" id="Seg_2787" s="T71">v-v:tense-v:pred.pn</ta>
            <ta e="T78" id="Seg_2788" s="T77">n-n:case</ta>
            <ta e="T84" id="Seg_2789" s="T83">n-n:(poss)-n:case</ta>
            <ta e="T85" id="Seg_2790" s="T84">propr-n:case</ta>
            <ta e="T212" id="Seg_2791" s="T85">que-pro:case</ta>
            <ta e="T87" id="Seg_2792" s="T213">propr-n&gt;adj.[n:case]</ta>
            <ta e="T88" id="Seg_2793" s="T210">propr-n&gt;adj.[n:case]</ta>
            <ta e="T91" id="Seg_2794" s="T88">ptcl</ta>
            <ta e="T95" id="Seg_2795" s="T94">adv</ta>
            <ta e="T96" id="Seg_2796" s="T95">adv</ta>
            <ta e="T97" id="Seg_2797" s="T96">v-v:tense-v:poss.pn</ta>
            <ta e="T98" id="Seg_2798" s="T97">conj</ta>
            <ta e="T99" id="Seg_2799" s="T98">propr-n:case</ta>
            <ta e="T100" id="Seg_2800" s="T99">v-v:tense-v:poss.pn</ta>
            <ta e="T101" id="Seg_2801" s="T100">adv</ta>
            <ta e="T102" id="Seg_2802" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_2803" s="T102">v-v:cvb</ta>
            <ta e="T104" id="Seg_2804" s="T103">adv</ta>
            <ta e="T105" id="Seg_2805" s="T104">adv</ta>
            <ta e="T106" id="Seg_2806" s="T105">propr</ta>
            <ta e="T107" id="Seg_2807" s="T106">que-pro:case</ta>
            <ta e="T108" id="Seg_2808" s="T107">v-v:tense-v:poss.pn</ta>
            <ta e="T109" id="Seg_2809" s="T108">n-n&gt;v-v:cvb</ta>
            <ta e="T112" id="Seg_2810" s="T111">adv</ta>
            <ta e="T113" id="Seg_2811" s="T112">n-n:poss-n:case</ta>
            <ta e="T114" id="Seg_2812" s="T113">propr-n&gt;adj.[n:case]</ta>
            <ta e="T115" id="Seg_2813" s="T114">v-v:tense-v:poss.pn</ta>
            <ta e="T122" id="Seg_2814" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_2815" s="T122">v-v:cvb</ta>
            <ta e="T124" id="Seg_2816" s="T123">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T129" id="Seg_2817" s="T128">que</ta>
            <ta e="T135" id="Seg_2818" s="T134">cardnum</ta>
            <ta e="T136" id="Seg_2819" s="T135">n-n:case</ta>
            <ta e="T142" id="Seg_2820" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_2821" s="T142">cardnum</ta>
            <ta e="T144" id="Seg_2822" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_2823" s="T144">v-v:tense-v:poss.pn</ta>
            <ta e="T146" id="Seg_2824" s="T145">v-v:cvb</ta>
            <ta e="T149" id="Seg_2825" s="T148">adv</ta>
            <ta e="T150" id="Seg_2826" s="T149">cardnum</ta>
            <ta e="T152" id="Seg_2827" s="T151">cardnum</ta>
            <ta e="T154" id="Seg_2828" s="T152">cardnum-n:(poss)-n:case</ta>
            <ta e="T155" id="Seg_2829" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_2830" s="T155">adv</ta>
            <ta e="T157" id="Seg_2831" s="T156">v-v:tense-v:poss.pn</ta>
            <ta e="T169" id="Seg_2832" s="T168">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T170" id="Seg_2833" s="T169">propr.[n:case]</ta>
            <ta e="T171" id="Seg_2834" s="T170">v-v:tense-v:pred.pn</ta>
            <ta e="T177" id="Seg_2835" s="T176">propr</ta>
            <ta e="T178" id="Seg_2836" s="T177">v-v:cvb</ta>
            <ta e="T179" id="Seg_2837" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_2838" s="T179">n-n:poss-n:case</ta>
            <ta e="T181" id="Seg_2839" s="T180">propr</ta>
            <ta e="T182" id="Seg_2840" s="T181">conj</ta>
            <ta e="T183" id="Seg_2841" s="T182">n-n:(poss)-n:case</ta>
            <ta e="T186" id="Seg_2842" s="T185">n-n:(num)</ta>
            <ta e="T187" id="Seg_2843" s="T186">ptcl</ta>
            <ta e="T190" id="Seg_2844" s="T189">n-n:(num)-n:case</ta>
            <ta e="T193" id="Seg_2845" s="T192">adv</ta>
            <ta e="T200" id="Seg_2846" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_2847" s="T200">v-v&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T204" id="Seg_2848" s="T203">propr.[n:case]</ta>
            <ta e="T205" id="Seg_2849" s="T204">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-AnMSp">
            <ta e="T11" id="Seg_2850" s="T10">v</ta>
            <ta e="T12" id="Seg_2851" s="T11">que</ta>
            <ta e="T13" id="Seg_2852" s="T12">propr</ta>
            <ta e="T14" id="Seg_2853" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_2854" s="T14">n</ta>
            <ta e="T16" id="Seg_2855" s="T15">v</ta>
            <ta e="T17" id="Seg_2856" s="T16">adv</ta>
            <ta e="T18" id="Seg_2857" s="T17">v</ta>
            <ta e="T19" id="Seg_2858" s="T18">aux</ta>
            <ta e="T25" id="Seg_2859" s="T24">propr</ta>
            <ta e="T26" id="Seg_2860" s="T25">adv</ta>
            <ta e="T27" id="Seg_2861" s="T26">propr</ta>
            <ta e="T30" id="Seg_2862" s="T29">adv</ta>
            <ta e="T31" id="Seg_2863" s="T30">adv</ta>
            <ta e="T32" id="Seg_2864" s="T31">v</ta>
            <ta e="T33" id="Seg_2865" s="T32">que</ta>
            <ta e="T34" id="Seg_2866" s="T33">que</ta>
            <ta e="T35" id="Seg_2867" s="T34">ptcl</ta>
            <ta e="T38" id="Seg_2868" s="T37">n</ta>
            <ta e="T39" id="Seg_2869" s="T38">ptcl</ta>
            <ta e="T42" id="Seg_2870" s="T41">n</ta>
            <ta e="T43" id="Seg_2871" s="T42">n</ta>
            <ta e="T44" id="Seg_2872" s="T43">v</ta>
            <ta e="T45" id="Seg_2873" s="T44">adv</ta>
            <ta e="T46" id="Seg_2874" s="T45">v</ta>
            <ta e="T47" id="Seg_2875" s="T46">v</ta>
            <ta e="T48" id="Seg_2876" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_2877" s="T48">v</ta>
            <ta e="T50" id="Seg_2878" s="T49">cardnum</ta>
            <ta e="T51" id="Seg_2879" s="T50">n</ta>
            <ta e="T52" id="Seg_2880" s="T51">v</ta>
            <ta e="T53" id="Seg_2881" s="T52">aux</ta>
            <ta e="T57" id="Seg_2882" s="T56">adv</ta>
            <ta e="T58" id="Seg_2883" s="T57">adv</ta>
            <ta e="T59" id="Seg_2884" s="T58">v</ta>
            <ta e="T60" id="Seg_2885" s="T59">v</ta>
            <ta e="T61" id="Seg_2886" s="T60">adv</ta>
            <ta e="T62" id="Seg_2887" s="T61">adv</ta>
            <ta e="T63" id="Seg_2888" s="T62">v</ta>
            <ta e="T64" id="Seg_2889" s="T63">adv</ta>
            <ta e="T65" id="Seg_2890" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2891" s="T65">n</ta>
            <ta e="T67" id="Seg_2892" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2893" s="T67">dempro</ta>
            <ta e="T69" id="Seg_2894" s="T68">v</ta>
            <ta e="T70" id="Seg_2895" s="T69">v</ta>
            <ta e="T71" id="Seg_2896" s="T70">adv</ta>
            <ta e="T73" id="Seg_2897" s="T71">v</ta>
            <ta e="T78" id="Seg_2898" s="T77">n</ta>
            <ta e="T84" id="Seg_2899" s="T83">n</ta>
            <ta e="T85" id="Seg_2900" s="T84">propr</ta>
            <ta e="T212" id="Seg_2901" s="T85">que</ta>
            <ta e="T87" id="Seg_2902" s="T213">propr</ta>
            <ta e="T88" id="Seg_2903" s="T210">propr</ta>
            <ta e="T91" id="Seg_2904" s="T88">ptcl</ta>
            <ta e="T95" id="Seg_2905" s="T94">adv</ta>
            <ta e="T96" id="Seg_2906" s="T95">adv</ta>
            <ta e="T97" id="Seg_2907" s="T96">v</ta>
            <ta e="T98" id="Seg_2908" s="T97">conj</ta>
            <ta e="T99" id="Seg_2909" s="T98">propr</ta>
            <ta e="T100" id="Seg_2910" s="T99">v</ta>
            <ta e="T101" id="Seg_2911" s="T100">adv</ta>
            <ta e="T102" id="Seg_2912" s="T101">n</ta>
            <ta e="T103" id="Seg_2913" s="T102">v</ta>
            <ta e="T104" id="Seg_2914" s="T103">adv</ta>
            <ta e="T105" id="Seg_2915" s="T104">adv</ta>
            <ta e="T106" id="Seg_2916" s="T105">propr</ta>
            <ta e="T107" id="Seg_2917" s="T106">que</ta>
            <ta e="T108" id="Seg_2918" s="T107">v</ta>
            <ta e="T109" id="Seg_2919" s="T108">v</ta>
            <ta e="T112" id="Seg_2920" s="T111">adv</ta>
            <ta e="T113" id="Seg_2921" s="T112">n</ta>
            <ta e="T114" id="Seg_2922" s="T113">propr</ta>
            <ta e="T115" id="Seg_2923" s="T114">v</ta>
            <ta e="T122" id="Seg_2924" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_2925" s="T122">v</ta>
            <ta e="T124" id="Seg_2926" s="T123">ptcl</ta>
            <ta e="T129" id="Seg_2927" s="T128">que</ta>
            <ta e="T135" id="Seg_2928" s="T134">cardnum</ta>
            <ta e="T136" id="Seg_2929" s="T135">n</ta>
            <ta e="T142" id="Seg_2930" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_2931" s="T142">cardnum</ta>
            <ta e="T144" id="Seg_2932" s="T143">n</ta>
            <ta e="T145" id="Seg_2933" s="T144">cop</ta>
            <ta e="T146" id="Seg_2934" s="T145">v</ta>
            <ta e="T149" id="Seg_2935" s="T148">adv</ta>
            <ta e="T150" id="Seg_2936" s="T149">cardnum</ta>
            <ta e="T152" id="Seg_2937" s="T151">cardnum</ta>
            <ta e="T154" id="Seg_2938" s="T152">cardnum</ta>
            <ta e="T155" id="Seg_2939" s="T154">v</ta>
            <ta e="T156" id="Seg_2940" s="T155">adv</ta>
            <ta e="T157" id="Seg_2941" s="T156">v</ta>
            <ta e="T169" id="Seg_2942" s="T168">v</ta>
            <ta e="T170" id="Seg_2943" s="T169">propr</ta>
            <ta e="T171" id="Seg_2944" s="T170">v</ta>
            <ta e="T177" id="Seg_2945" s="T176">propr</ta>
            <ta e="T178" id="Seg_2946" s="T177">v</ta>
            <ta e="T179" id="Seg_2947" s="T178">v</ta>
            <ta e="T180" id="Seg_2948" s="T179">n</ta>
            <ta e="T181" id="Seg_2949" s="T180">propr</ta>
            <ta e="T182" id="Seg_2950" s="T181">conj</ta>
            <ta e="T183" id="Seg_2951" s="T182">n</ta>
            <ta e="T186" id="Seg_2952" s="T185">n</ta>
            <ta e="T187" id="Seg_2953" s="T186">ptcl</ta>
            <ta e="T190" id="Seg_2954" s="T189">n</ta>
            <ta e="T193" id="Seg_2955" s="T192">adv</ta>
            <ta e="T200" id="Seg_2956" s="T199">n</ta>
            <ta e="T201" id="Seg_2957" s="T200">v</ta>
            <ta e="T204" id="Seg_2958" s="T203">propr</ta>
            <ta e="T205" id="Seg_2959" s="T204">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-AnMSp" />
         <annotation name="SyF" tierref="SyF-AnMSp" />
         <annotation name="IST" tierref="IST-AnMSp" />
         <annotation name="Top" tierref="Top-AnMSp" />
         <annotation name="Foc" tierref="Foc-AnMSp" />
         <annotation name="BOR" tierref="BOR-AnMSp">
            <ta e="T13" id="Seg_2960" s="T12">RUS:cult</ta>
            <ta e="T25" id="Seg_2961" s="T24">RUS:cult</ta>
            <ta e="T27" id="Seg_2962" s="T26">RUS:cult</ta>
            <ta e="T42" id="Seg_2963" s="T41">RUS:cult</ta>
            <ta e="T43" id="Seg_2964" s="T42">RUS:cult</ta>
            <ta e="T58" id="Seg_2965" s="T57">RUS:disc</ta>
            <ta e="T62" id="Seg_2966" s="T61">RUS:disc</ta>
            <ta e="T69" id="Seg_2967" s="T68">EV:core</ta>
            <ta e="T84" id="Seg_2968" s="T83">RUS:cult</ta>
            <ta e="T85" id="Seg_2969" s="T84">RUS:cult</ta>
            <ta e="T87" id="Seg_2970" s="T213">RUS:cult</ta>
            <ta e="T98" id="Seg_2971" s="T97">RUS:gram</ta>
            <ta e="T99" id="Seg_2972" s="T98">RUS:cult</ta>
            <ta e="T106" id="Seg_2973" s="T105">RUS:cult</ta>
            <ta e="T170" id="Seg_2974" s="T169">RUS:cult</ta>
            <ta e="T177" id="Seg_2975" s="T176">RUS:cult</ta>
            <ta e="T180" id="Seg_2976" s="T179">RUS:cult</ta>
            <ta e="T181" id="Seg_2977" s="T180">RUS:cult</ta>
            <ta e="T182" id="Seg_2978" s="T181">RUS:gram</ta>
            <ta e="T183" id="Seg_2979" s="T182">RUS:cult</ta>
            <ta e="T200" id="Seg_2980" s="T199">RUS:cult</ta>
            <ta e="T201" id="Seg_2981" s="T200">RUS:cult</ta>
            <ta e="T204" id="Seg_2982" s="T203">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-AnMSp" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-AnMSp" />
         <annotation name="CS" tierref="CS-AnMSp" />
         <annotation name="fe" tierref="fe-AnMSp">
            <ta e="T16" id="Seg_2983" s="T10">Go… this, Syndassko, ehm, we go to the tundra.</ta>
            <ta e="T19" id="Seg_2984" s="T16">Then we keep going.</ta>
            <ta e="T27" id="Seg_2985" s="T24">[With] Dima and Mika.</ta>
            <ta e="T32" id="Seg_2986" s="T29">Then we go [=went] there.</ta>
            <ta e="T39" id="Seg_2987" s="T32">There is whatchamacallit, there are reindeer.</ta>
            <ta e="T44" id="Seg_2988" s="T41">The runner of our snowscooter broke.</ta>
            <ta e="T53" id="Seg_2989" s="T44">Then we changed it and went on, we came across one reindeer and killed it.</ta>
            <ta e="T60" id="Seg_2990" s="T56">Then we go on, we skinned (it).</ta>
            <ta e="T63" id="Seg_2991" s="T60">Then we went on.</ta>
            <ta e="T70" id="Seg_2992" s="T63">There are again wild reindeer, we wounded one but did not find it.</ta>
            <ta e="T73" id="Seg_2993" s="T70">Then we arrived.</ta>
            <ta e="T78" id="Seg_2994" s="T77">To the tundra.</ta>
            <ta e="T212" id="Seg_2995" s="T83">My mommy, Zhenja and that.</ta>
            <ta e="T87" id="Seg_2996" s="T213">Natasha and family.</ta>
            <ta e="T91" id="Seg_2997" s="T87">Ehm, Natasha and family were not there.</ta>
            <ta e="T97" id="Seg_2998" s="T94">Then in the morning he went.</ta>
            <ta e="T103" id="Seg_2999" s="T97">And Dima went looking for wild reindeer in the morning.</ta>
            <ta e="T109" id="Seg_3000" s="T103">Then Mika went fishing.</ta>
            <ta e="T115" id="Seg_3001" s="T111">Then in the morning Natasha and family came.</ta>
            <ta e="T124" id="Seg_3002" s="T121">No he did not yet kill it.</ta>
            <ta e="T129" id="Seg_3003" s="T128">Where?</ta>
            <ta e="T136" id="Seg_3004" s="T134">One wild reindeer.</ta>
            <ta e="T146" id="Seg_3005" s="T141">Well there were two wild reindeer.</ta>
            <ta e="T155" id="Seg_3006" s="T148">There two (…) he killed one.</ta>
            <ta e="T157" id="Seg_3007" s="T155">Then he came back.</ta>
            <ta e="T171" id="Seg_3008" s="T168">On the way s/he entered Syndassko.</ta>
            <ta e="T183" id="Seg_3009" s="T176">In order to enter Syndassko we went by snowscooter, Mika and mommy.</ta>
            <ta e="T187" id="Seg_3010" s="T185">[To the] tents [=to those who live in tents].</ta>
            <ta e="T190" id="Seg_3011" s="T189">Skins.</ta>
            <ta e="T195" id="Seg_3012" s="T192">Then to Synda…</ta>
            <ta e="T201" id="Seg_3013" s="T199">I drove the snow scooter.</ta>
            <ta e="T205" id="Seg_3014" s="T203">We came to Syndassko.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-AnMSp">
            <ta e="T16" id="Seg_3015" s="T10">Er/sie geht, dings, Syndassko, äh, wir gehen in die Tundra.</ta>
            <ta e="T19" id="Seg_3016" s="T16">Dann gehen wir weiter.</ta>
            <ta e="T27" id="Seg_3017" s="T24">Dima und Mika.</ta>
            <ta e="T32" id="Seg_3018" s="T29">Dann gehen [=gingen] wir dorthin.</ta>
            <ta e="T39" id="Seg_3019" s="T32">Dort gibt es dings, dort sind wilde Rentiere.</ta>
            <ta e="T44" id="Seg_3020" s="T41">Eine Kufe unseres Schneemobils ging kaputt.</ta>
            <ta e="T53" id="Seg_3021" s="T44">Dann wechselten wir sie und fuhren weiter, wir trafen auf ein wildes Rentier und töteten es.</ta>
            <ta e="T60" id="Seg_3022" s="T56">Dann gehen wir weiter, wir häuteten es.</ta>
            <ta e="T63" id="Seg_3023" s="T60">Dann fuhren wir weiter.</ta>
            <ta e="T70" id="Seg_3024" s="T63">Dort sind wieder wilde Rentiere, wir verwundeten [eins], fanden es nicht.</ta>
            <ta e="T73" id="Seg_3025" s="T70">Dann kamen wir.</ta>
            <ta e="T78" id="Seg_3026" s="T77">In die Tundra.</ta>
            <ta e="T212" id="Seg_3027" s="T83">Meine Mama, Zhenja und dings.</ta>
            <ta e="T87" id="Seg_3028" s="T213">Natascha und Familie.</ta>
            <ta e="T91" id="Seg_3029" s="T87">Äh, Natascha und Familie ist nicht da.</ta>
            <ta e="T97" id="Seg_3030" s="T94">Dann am Morgen ging er.</ta>
            <ta e="T103" id="Seg_3031" s="T97">Und Dima ging am Morgen, um wilde Rentiere zu suchen.</ta>
            <ta e="T109" id="Seg_3032" s="T103">Dann ging Mika zum Fischen.</ta>
            <ta e="T115" id="Seg_3033" s="T111">Dann am nächsten Morgen kam Natascha und Familie.</ta>
            <ta e="T124" id="Seg_3034" s="T121">Nein, er tötete noch nicht.</ta>
            <ta e="T129" id="Seg_3035" s="T128">Wo?</ta>
            <ta e="T136" id="Seg_3036" s="T134">Ein wildes Rentier.</ta>
            <ta e="T146" id="Seg_3037" s="T141">Nun, dort waren zwei wilde Rentiere.</ta>
            <ta e="T155" id="Seg_3038" s="T148">Dort zwei (…) eins tötete er.</ta>
            <ta e="T157" id="Seg_3039" s="T155">Dann kam er.</ta>
            <ta e="T171" id="Seg_3040" s="T168">Auf dem Weg fuhr er/sie nach Syndassko hinein.</ta>
            <ta e="T183" id="Seg_3041" s="T176">Um nach Syndassko hineinzufahren, fuhren wir mit dem Schneemobil, Mika und meine Mama. </ta>
            <ta e="T187" id="Seg_3042" s="T185">[Zu den] Zelten [=zu denen, die in Zelten wohnen].</ta>
            <ta e="T190" id="Seg_3043" s="T189">Felle.</ta>
            <ta e="T195" id="Seg_3044" s="T192">Dann Synda…</ta>
            <ta e="T201" id="Seg_3045" s="T199">Ich habe das Schneemobil gefahren.</ta>
            <ta e="T205" id="Seg_3046" s="T203">Wir kamen nach Syndassko.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-AnMSp">
            <ta e="T16" id="Seg_3047" s="T10">Пошёл… как это, Сындасско, мы ходим в тундру.</ta>
            <ta e="T19" id="Seg_3048" s="T16">Продолжаем ходить.</ta>
            <ta e="T27" id="Seg_3049" s="T24">С Димой и Микой.</ta>
            <ta e="T32" id="Seg_3050" s="T29">Потом едем [=поехали] туда.</ta>
            <ta e="T39" id="Seg_3051" s="T32">Там эти есть, олени.</ta>
            <ta e="T44" id="Seg_3052" s="T41">У бурана каток сломался.</ta>
            <ta e="T53" id="Seg_3053" s="T44">Потом поменяли его, поехали, увидели одного оленя и убили.</ta>
            <ta e="T60" id="Seg_3054" s="T56">Потом дальше едем, освежевали его.</ta>
            <ta e="T63" id="Seg_3055" s="T60">Потом дальше поехали.</ta>
            <ta e="T70" id="Seg_3056" s="T63">Там опять олени, ранили одного, но не нашли.</ta>
            <ta e="T73" id="Seg_3057" s="T70">Потом приехали.</ta>
            <ta e="T78" id="Seg_3058" s="T77">В тундру.</ta>
            <ta e="T212" id="Seg_3059" s="T83">Мама, Женя и это.</ta>
            <ta e="T87" id="Seg_3060" s="T213">Наташины [=Наташа с семьёй].</ta>
            <ta e="T91" id="Seg_3061" s="T87">А, Наташиных не было.</ta>
            <ta e="T97" id="Seg_3062" s="T94">Потом утром пошёл.</ta>
            <ta e="T103" id="Seg_3063" s="T97">А Дима пошёл утром диких оленей искать.</ta>
            <ta e="T109" id="Seg_3064" s="T103">Потом Мика пошёл за рыбой.</ta>
            <ta e="T115" id="Seg_3065" s="T111">Потом наутро Наташины приехали.</ta>
            <ta e="T124" id="Seg_3066" s="T121">Нет, ещё не убил.</ta>
            <ta e="T129" id="Seg_3067" s="T128">Где?</ta>
            <ta e="T136" id="Seg_3068" s="T134">Одного оленя.</ta>
            <ta e="T146" id="Seg_3069" s="T141">Ну, там было два оленя.</ta>
            <ta e="T155" id="Seg_3070" s="T148">Там два (…) одного убил.</ta>
            <ta e="T157" id="Seg_3071" s="T155">Потом вернулся.</ta>
            <ta e="T171" id="Seg_3072" s="T168">Как ехать в Сындасско.</ta>
            <ta e="T183" id="Seg_3073" s="T176">Чтобы добраться до Сындасско, мы поехали на буране, Мика и мама.</ta>
            <ta e="T187" id="Seg_3074" s="T185">К палаткам [=к тем, которые в палатках].</ta>
            <ta e="T190" id="Seg_3075" s="T189">Шкуры.</ta>
            <ta e="T195" id="Seg_3076" s="T192">Потом в (Сындасско)…</ta>
            <ta e="T201" id="Seg_3077" s="T199">Я буран водил.</ta>
            <ta e="T205" id="Seg_3078" s="T203">Приехали в Сындасско.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-AnMSp" />
         <annotation name="nt" tierref="nt-AnMSp" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T90" />
            <conversion-tli id="T194" />
            <conversion-tli id="T211" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T212" />
            <conversion-tli id="T86" />
            <conversion-tli id="T213" />
            <conversion-tli id="T87" />
            <conversion-tli id="T210" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-AnIM"
                          name="ref"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-AnIM"
                          name="st"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-AnIM"
                          name="ts"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-AnIM"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-AnIM"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-AnIM"
                          name="mb"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-AnIM"
                          name="mp"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-AnIM"
                          name="ge"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-AnIM"
                          name="gg"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-AnIM"
                          name="gr"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-AnIM"
                          name="mc"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-AnIM"
                          name="ps"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-AnIM"
                          name="SeR"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-AnIM"
                          name="SyF"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-AnIM"
                          name="IST"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-AnIM"
                          name="Top"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-AnIM"
                          name="Foc"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-AnIM"
                          name="BOR"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-AnIM"
                          name="BOR-Phon"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-AnIM"
                          name="BOR-Morph"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-AnIM"
                          name="CS"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-AnIM"
                          name="fe"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-AnIM"
                          name="fg"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-AnIM"
                          name="fr"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-AnIM"
                          name="ltr"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-AnIM"
                          name="nt"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-AnMSp"
                          name="ref"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-AnMSp"
                          name="st"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-AnMSp"
                          name="ts"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-AnMSp"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-AnMSp"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-AnMSp"
                          name="mb"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-AnMSp"
                          name="mp"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-AnMSp"
                          name="ge"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-AnMSp"
                          name="gg"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-AnMSp"
                          name="gr"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-AnMSp"
                          name="mc"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-AnMSp"
                          name="ps"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-AnMSp"
                          name="SeR"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-AnMSp"
                          name="SyF"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-AnMSp"
                          name="IST"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-AnMSp"
                          name="Top"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-AnMSp"
                          name="Foc"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-AnMSp"
                          name="BOR"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-AnMSp"
                          name="BOR-Phon"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-AnMSp"
                          name="BOR-Morph"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-AnMSp"
                          name="CS"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-AnMSp"
                          name="fe"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-AnMSp"
                          name="fg"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-AnMSp"
                          name="fr"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-AnMSp"
                          name="ltr"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-AnMSp"
                          name="nt"
                          segmented-tier-id="tx-AnMSp"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
