<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID9A5CE084-2309-D609-AA63-132045474C54">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>MaPX_KuNS_200X_YakutsOfEssej_conv</transcription-name>
         <referenced-file url="MaPX_KuNS_200X_YakutsOfEssej_conv.wav" />
         <referenced-file url="MaPX_KuNS_200X_YakutsOfEssej_conv.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\conv\MaPX_KuNS_200X_YakutsOfEssej_conv\MaPX_KuNS_200X_YakutsOfEssej_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">684</ud-information>
            <ud-information attribute-name="# HIAT:w">508</ud-information>
            <ud-information attribute-name="# e">503</ud-information>
            <ud-information attribute-name="# HIAT:u">55</ud-information>
            <ud-information attribute-name="# sc">38</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MaPX">
            <abbreviation>MaPX</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KuNS">
            <abbreviation>KuNS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.89" type="appl" />
         <tli id="T2" time="1.78" type="appl" />
         <tli id="T3" time="2.67" type="appl" />
         <tli id="T4" time="3.56" type="appl" />
         <tli id="T5" time="4.45" type="appl" />
         <tli id="T6" time="5.131" type="appl" />
         <tli id="T7" time="5.813" type="appl" />
         <tli id="T8" time="6.494" type="appl" />
         <tli id="T9" time="7.175" type="appl" />
         <tli id="T10" time="7.857" type="appl" />
         <tli id="T11" time="8.538" type="appl" />
         <tli id="T12" time="9.219" type="appl" />
         <tli id="T13" time="9.9" type="appl" />
         <tli id="T14" time="10.582" type="appl" />
         <tli id="T15" time="11.263" type="appl" />
         <tli id="T16" time="11.874" type="appl" />
         <tli id="T17" time="12.484" type="appl" />
         <tli id="T18" time="13.094" type="appl" />
         <tli id="T19" time="13.705" type="appl" />
         <tli id="T20" time="14.316" type="appl" />
         <tli id="T21" time="14.926" type="appl" />
         <tli id="T22" time="15.536" type="appl" />
         <tli id="T23" time="16.147" type="appl" />
         <tli id="T24" time="16.758" type="appl" />
         <tli id="T25" time="17.368" type="appl" />
         <tli id="T26" time="17.978" type="appl" />
         <tli id="T27" time="18.589" type="appl" />
         <tli id="T28" time="19.2" type="appl" />
         <tli id="T29" time="19.68633041711866" />
         <tli id="T30" time="20.578" type="appl" />
         <tli id="T31" time="21.346" type="appl" />
         <tli id="T32" time="22.113" type="appl" />
         <tli id="T33" time="22.881" type="appl" />
         <tli id="T34" time="23.649" type="appl" />
         <tli id="T35" time="24.417" type="appl" />
         <tli id="T36" time="25.185" type="appl" />
         <tli id="T37" time="25.952" type="appl" />
         <tli id="T38" time="26.72" type="appl" />
         <tli id="T39" time="27.488" type="appl" />
         <tli id="T40" time="28.15" type="appl" />
         <tli id="T41" time="28.813" type="appl" />
         <tli id="T42" time="29.475" type="appl" />
         <tli id="T43" time="30.138" type="appl" />
         <tli id="T44" time="30.8" type="appl" />
         <tli id="T45" time="31.463" type="appl" />
         <tli id="T46" time="32.125" type="appl" />
         <tli id="T47" time="32.787" type="appl" />
         <tli id="T48" time="33.45" type="appl" />
         <tli id="T50" time="34.775" type="appl" />
         <tli id="T51" time="35.437" type="appl" />
         <tli id="T52" time="36.1" type="appl" />
         <tli id="T53" time="36.762" type="appl" />
         <tli id="T54" time="37.863" type="appl" />
         <tli id="T55" time="38.964" type="appl" />
         <tli id="T56" time="40.065" type="appl" />
         <tli id="T57" time="41.166" type="appl" />
         <tli id="T58" time="42.267" type="appl" />
         <tli id="T59" time="43.369" type="appl" />
         <tli id="T60" time="44.47" type="appl" />
         <tli id="T61" time="45.571" type="appl" />
         <tli id="T62" time="46.672" type="appl" />
         <tli id="T63" time="47.773" type="appl" />
         <tli id="T64" time="48.874" type="appl" />
         <tli id="T65" time="49.509" type="appl" />
         <tli id="T66" time="50.145" type="appl" />
         <tli id="T67" time="50.78" type="appl" />
         <tli id="T68" time="51.415" type="appl" />
         <tli id="T69" time="52.05" type="appl" />
         <tli id="T70" time="52.686" type="appl" />
         <tli id="T71" time="53.321" type="appl" />
         <tli id="T72" time="53.956" type="appl" />
         <tli id="T73" time="54.591" type="appl" />
         <tli id="T74" time="55.227" type="appl" />
         <tli id="T75" time="55.862" type="appl" />
         <tli id="T76" time="56.497" type="appl" />
         <tli id="T77" time="57.132" type="appl" />
         <tli id="T78" time="57.768" type="appl" />
         <tli id="T79" time="58.403" type="appl" />
         <tli id="T80" time="59.132" type="appl" />
         <tli id="T81" time="59.861" type="appl" />
         <tli id="T82" time="60.59" type="appl" />
         <tli id="T83" time="61.319" type="appl" />
         <tli id="T84" time="62.048" type="appl" />
         <tli id="T85" time="62.777" type="appl" />
         <tli id="T86" time="63.506" type="appl" />
         <tli id="T87" time="64.236" type="appl" />
         <tli id="T88" time="64.965" type="appl" />
         <tli id="T89" time="65.694" type="appl" />
         <tli id="T90" time="66.423" type="appl" />
         <tli id="T91" time="67.152" type="appl" />
         <tli id="T92" time="67.881" type="appl" />
         <tli id="T93" time="68.61" type="appl" />
         <tli id="T94" time="69.288" type="appl" />
         <tli id="T95" time="69.966" type="appl" />
         <tli id="T96" time="70.644" type="appl" />
         <tli id="T97" time="71.321" type="appl" />
         <tli id="T98" time="71.999" type="appl" />
         <tli id="T99" time="72.677" type="appl" />
         <tli id="T100" time="73.355" type="appl" />
         <tli id="T101" time="74.051" type="appl" />
         <tli id="T102" time="74.748" type="appl" />
         <tli id="T103" time="75.444" type="appl" />
         <tli id="T104" time="76.14" type="appl" />
         <tli id="T105" time="76.837" type="appl" />
         <tli id="T106" time="77.533" type="appl" />
         <tli id="T107" time="78.229" type="appl" />
         <tli id="T108" time="78.926" type="appl" />
         <tli id="T109" time="79.622" type="appl" />
         <tli id="T110" time="80.404" type="appl" />
         <tli id="T111" time="81.186" type="appl" />
         <tli id="T112" time="81.969" type="appl" />
         <tli id="T113" time="82.751" type="appl" />
         <tli id="T114" time="83.533" type="appl" />
         <tli id="T115" time="84.315" type="appl" />
         <tli id="T116" time="85.098" type="appl" />
         <tli id="T117" time="85.88" type="appl" />
         <tli id="T118" time="86.662" type="appl" />
         <tli id="T119" time="87.29" type="appl" />
         <tli id="T120" time="87.918" type="appl" />
         <tli id="T121" time="88.546" type="appl" />
         <tli id="T122" time="89.174" type="appl" />
         <tli id="T123" time="89.802" type="appl" />
         <tli id="T124" time="90.43" type="appl" />
         <tli id="T125" time="91.028" type="appl" />
         <tli id="T126" time="91.627" type="appl" />
         <tli id="T127" time="92.225" type="appl" />
         <tli id="T128" time="92.824" type="appl" />
         <tli id="T129" time="93.422" type="appl" />
         <tli id="T130" time="94.02" type="appl" />
         <tli id="T131" time="94.619" type="appl" />
         <tli id="T132" time="95.217" type="appl" />
         <tli id="T133" time="95.816" type="appl" />
         <tli id="T134" time="96.414" type="appl" />
         <tli id="T135" time="97.002" type="appl" />
         <tli id="T136" time="97.591" type="appl" />
         <tli id="T137" time="98.179" type="appl" />
         <tli id="T138" time="98.767" type="appl" />
         <tli id="T139" time="99.355" type="appl" />
         <tli id="T140" time="99.944" type="appl" />
         <tli id="T141" time="100.532" type="appl" />
         <tli id="T142" time="101.12" type="appl" />
         <tli id="T143" time="101.708" type="appl" />
         <tli id="T144" time="102.297" type="appl" />
         <tli id="T145" time="102.885" type="appl" />
         <tli id="T146" time="103.473" type="appl" />
         <tli id="T147" time="104.061" type="appl" />
         <tli id="T148" time="104.65" type="appl" />
         <tli id="T149" time="105.238" type="appl" />
         <tli id="T150" time="106.025" type="appl" />
         <tli id="T151" time="106.18485299148864" />
         <tli id="T152" time="107.6" type="appl" />
         <tli id="T153" />
         <tli id="T154" time="108.11" type="appl" />
         <tli id="T155" time="109.13" type="appl" />
         <tli id="T156" time="109.598" type="appl" />
         <tli id="T157" time="110.065" type="appl" />
         <tli id="T158" time="110.532" type="appl" />
         <tli id="T159" time="111.0" type="appl" />
         <tli id="T160" time="111.468" type="appl" />
         <tli id="T161" time="111.935" type="appl" />
         <tli id="T162" time="112.402" type="appl" />
         <tli id="T163" time="112.87" type="appl" />
         <tli id="T164" time="113.526" type="appl" />
         <tli id="T165" time="114.182" type="appl" />
         <tli id="T166" time="114.838" type="appl" />
         <tli id="T167" time="115.495" type="appl" />
         <tli id="T168" time="116.151" type="appl" />
         <tli id="T169" time="116.807" type="appl" />
         <tli id="T170" time="117.463" type="appl" />
         <tli id="T171" time="118.097" type="appl" />
         <tli id="T172" time="118.732" type="appl" />
         <tli id="T173" time="119.366" type="appl" />
         <tli id="T174" time="120.0" type="appl" />
         <tli id="T175" time="120.634" type="appl" />
         <tli id="T176" time="121.269" type="appl" />
         <tli id="T177" time="121.903" type="appl" />
         <tli id="T178" time="122.537" type="appl" />
         <tli id="T179" time="123.171" type="appl" />
         <tli id="T180" time="123.806" type="appl" />
         <tli id="T181" time="124.44" type="appl" />
         <tli id="T182" time="125.084" type="appl" />
         <tli id="T183" time="125.727" type="appl" />
         <tli id="T184" time="126.371" type="appl" />
         <tli id="T185" time="127.015" type="appl" />
         <tli id="T186" time="127.658" type="appl" />
         <tli id="T187" time="128.302" type="appl" />
         <tli id="T188" time="128.946" type="appl" />
         <tli id="T189" time="129.589" type="appl" />
         <tli id="T190" time="130.233" type="appl" />
         <tli id="T191" time="130.821" type="appl" />
         <tli id="T192" time="131.408" type="appl" />
         <tli id="T193" time="131.996" type="appl" />
         <tli id="T194" time="132.584" type="appl" />
         <tli id="T195" time="133.171" type="appl" />
         <tli id="T196" time="133.759" type="appl" />
         <tli id="T197" time="134.346" type="appl" />
         <tli id="T198" time="134.934" type="appl" />
         <tli id="T199" time="135.522" type="appl" />
         <tli id="T201" time="136.697" type="appl" />
         <tli id="T202" time="137.272" type="appl" />
         <tli id="T203" time="137.847" type="appl" />
         <tli id="T204" time="138.422" type="appl" />
         <tli id="T205" time="138.997" type="appl" />
         <tli id="T206" time="139.572" type="appl" />
         <tli id="T207" time="140.147" type="appl" />
         <tli id="T208" time="140.722" type="appl" />
         <tli id="T209" time="141.298" type="appl" />
         <tli id="T210" time="141.873" type="appl" />
         <tli id="T211" time="142.448" type="appl" />
         <tli id="T212" time="143.023" type="appl" />
         <tli id="T213" time="143.598" type="appl" />
         <tli id="T214" time="144.173" type="appl" />
         <tli id="T215" time="144.748" type="appl" />
         <tli id="T216" time="145.323" type="appl" />
         <tli id="T217" time="145.898" type="appl" />
         <tli id="T218" time="146.431" type="appl" />
         <tli id="T219" time="146.965" type="appl" />
         <tli id="T220" time="147.498" type="appl" />
         <tli id="T221" time="148.031" type="appl" />
         <tli id="T222" time="148.565" type="appl" />
         <tli id="T223" time="149.098" type="appl" />
         <tli id="T224" time="149.631" type="appl" />
         <tli id="T225" time="150.165" type="appl" />
         <tli id="T226" time="150.698" type="appl" />
         <tli id="T227" time="151.231" type="appl" />
         <tli id="T228" time="151.765" type="appl" />
         <tli id="T229" time="152.298" type="appl" />
         <tli id="T230" time="153.034" type="appl" />
         <tli id="T231" time="153.769" type="appl" />
         <tli id="T232" time="154.505" type="appl" />
         <tli id="T233" time="155.241" type="appl" />
         <tli id="T234" time="155.976" type="appl" />
         <tli id="T235" time="156.712" type="appl" />
         <tli id="T236" time="157.448" type="appl" />
         <tli id="T237" time="158.183" type="appl" />
         <tli id="T238" time="158.919" type="appl" />
         <tli id="T239" time="159.654" type="appl" />
         <tli id="T240" time="160.39" type="appl" />
         <tli id="T241" time="161.126" type="appl" />
         <tli id="T242" time="161.861" type="appl" />
         <tli id="T243" time="162.597" type="appl" />
         <tli id="T244" time="163.164" type="appl" />
         <tli id="T245" time="163.731" type="appl" />
         <tli id="T246" time="164.298" type="appl" />
         <tli id="T247" time="164.864" type="appl" />
         <tli id="T248" time="165.431" type="appl" />
         <tli id="T249" time="165.998" type="appl" />
         <tli id="T250" time="166.73" type="appl" />
         <tli id="T251" time="167.462" type="appl" />
         <tli id="T252" time="168.193" type="appl" />
         <tli id="T253" time="168.925" type="appl" />
         <tli id="T254" time="169.657" type="appl" />
         <tli id="T255" time="170.389" type="appl" />
         <tli id="T256" time="171.12" type="appl" />
         <tli id="T257" time="171.852" type="appl" />
         <tli id="T258" time="172.584" type="appl" />
         <tli id="T259" time="173.316" type="appl" />
         <tli id="T260" time="174.048" type="appl" />
         <tli id="T261" time="174.779" type="appl" />
         <tli id="T262" time="175.511" type="appl" />
         <tli id="T263" time="176.243" type="appl" />
         <tli id="T264" time="176.854" type="appl" />
         <tli id="T265" time="177.465" type="appl" />
         <tli id="T266" time="178.076" type="appl" />
         <tli id="T267" time="178.687" type="appl" />
         <tli id="T268" time="179.299" type="appl" />
         <tli id="T269" time="179.91" type="appl" />
         <tli id="T270" time="180.521" type="appl" />
         <tli id="T271" time="181.132" type="appl" />
         <tli id="T272" time="181.743" type="appl" />
         <tli id="T273" time="182.4" type="appl" />
         <tli id="T274" time="183.058" type="appl" />
         <tli id="T275" time="183.715" type="appl" />
         <tli id="T276" time="184.25685282720713" />
         <tli id="T277" time="185.266" type="appl" />
         <tli id="T278" time="186.16" type="appl" />
         <tli id="T279" time="186.681" type="appl" />
         <tli id="T280" time="187.202" type="appl" />
         <tli id="T281" time="187.723" type="appl" />
         <tli id="T282" time="188.244" type="appl" />
         <tli id="T283" time="188.765" type="appl" />
         <tli id="T284" time="189.286" type="appl" />
         <tli id="T285" time="189.807" type="appl" />
         <tli id="T286" time="190.328" type="appl" />
         <tli id="T287" time="190.849" type="appl" />
         <tli id="T288" time="191.486" type="appl" />
         <tli id="T289" time="192.123" type="appl" />
         <tli id="T290" time="192.76" type="appl" />
         <tli id="T291" time="193.398" type="appl" />
         <tli id="T292" time="194.035" type="appl" />
         <tli id="T293" time="194.672" type="appl" />
         <tli id="T294" time="195.198" type="appl" />
         <tli id="T295" time="195.724" type="appl" />
         <tli id="T296" time="196.25" type="appl" />
         <tli id="T297" time="196.775" type="appl" />
         <tli id="T298" time="197.301" type="appl" />
         <tli id="T299" time="197.827" type="appl" />
         <tli id="T300" time="198.353" type="appl" />
         <tli id="T301" time="198.879" type="appl" />
         <tli id="T302" time="199.405" type="appl" />
         <tli id="T303" time="199.93" type="appl" />
         <tli id="T304" time="200.456" type="appl" />
         <tli id="T305" time="200.982" type="appl" />
         <tli id="T306" time="201.508" type="appl" />
         <tli id="T307" time="202.034" type="appl" />
         <tli id="T308" time="202.56" type="appl" />
         <tli id="T309" time="203.086" type="appl" />
         <tli id="T311" time="204.137" type="appl" />
         <tli id="T312" time="204.663" type="appl" />
         <tli id="T313" time="205.189" type="appl" />
         <tli id="T314" time="205.715" type="appl" />
         <tli id="T315" time="206.241" type="appl" />
         <tli id="T316" time="206.766" type="appl" />
         <tli id="T317" time="207.292" type="appl" />
         <tli id="T318" time="207.818" type="appl" />
         <tli id="T319" time="208.344" type="appl" />
         <tli id="T320" time="209.256" type="appl" />
         <tli id="T321" time="210.168" type="appl" />
         <tli id="T322" time="210.86973160307494" />
         <tli id="T323" time="211.859" type="appl" />
         <tli id="T324" time="212.637" type="appl" />
         <tli id="T325" time="213.416" type="appl" />
         <tli id="T326" time="214.194" type="appl" />
         <tli id="T327" time="214.973" type="appl" />
         <tli id="T328" time="215.751" type="appl" />
         <tli id="T329" time="216.53" type="appl" />
         <tli id="T330" time="217.308" type="appl" />
         <tli id="T331" time="218.087" type="appl" />
         <tli id="T332" time="218.682" type="appl" />
         <tli id="T333" time="219.277" type="appl" />
         <tli id="T334" time="219.872" type="appl" />
         <tli id="T335" time="220.578" type="appl" />
         <tli id="T336" time="221.284" type="appl" />
         <tli id="T337" time="221.72287956076852" />
         <tli id="T338" time="222.592" type="appl" />
         <tli id="T339" time="223.194" type="appl" />
         <tli id="T340" time="223.795" type="appl" />
         <tli id="T341" time="224.397" type="appl" />
         <tli id="T342" time="224.999" type="appl" />
         <tli id="T343" time="225.601" type="appl" />
         <tli id="T344" time="226.203" type="appl" />
         <tli id="T345" time="226.805" type="appl" />
         <tli id="T346" time="227.406" type="appl" />
         <tli id="T347" time="228.008" type="appl" />
         <tli id="T348" time="228.61" type="appl" />
         <tli id="T350" time="229.505" type="appl" />
         <tli id="T351" time="229.952" type="appl" />
         <tli id="T352" time="230.4" type="appl" />
         <tli id="T353" time="230.847" type="appl" />
         <tli id="T354" time="231.295" type="appl" />
         <tli id="T355" time="231.742" type="appl" />
         <tli id="T356" time="232.19" type="appl" />
         <tli id="T357" time="232.637" type="appl" />
         <tli id="T358" time="233.084" type="appl" />
         <tli id="T359" time="233.532" type="appl" />
         <tli id="T360" time="233.979" type="appl" />
         <tli id="T361" time="234.427" type="appl" />
         <tli id="T362" time="234.874" type="appl" />
         <tli id="T363" time="235.322" type="appl" />
         <tli id="T364" time="235.769" type="appl" />
         <tli id="T365" time="236.217" type="appl" />
         <tli id="T366" time="236.664" type="appl" />
         <tli id="T367" time="237.112" type="appl" />
         <tli id="T368" time="237.559" type="appl" />
         <tli id="T369" time="238.096" type="appl" />
         <tli id="T370" time="238.633" type="appl" />
         <tli id="T371" time="239.17" type="appl" />
         <tli id="T372" time="239.686" type="appl" />
         <tli id="T373" time="240.202" type="appl" />
         <tli id="T374" time="240.718" type="appl" />
         <tli id="T375" time="241.234" type="appl" />
         <tli id="T376" time="241.75" type="appl" />
         <tli id="T377" time="242.266" type="appl" />
         <tli id="T379" time="243.298" type="appl" />
         <tli id="T381" time="244.33" type="appl" />
         <tli id="T382" time="244.846" type="appl" />
         <tli id="T383" time="245.665" type="appl" />
         <tli id="T384" time="246.484" type="appl" />
         <tli id="T385" time="247.303" type="appl" />
         <tli id="T386" time="248.123" type="appl" />
         <tli id="T387" time="248.942" type="appl" />
         <tli id="T388" time="249.761" type="appl" />
         <tli id="T389" time="250.4557221268984" />
         <tli id="T390" time="251.095" type="appl" />
         <tli id="T391" time="251.24237535724717" />
         <tli id="T392" time="251.976" type="appl" />
         <tli id="T393" time="252.692" type="appl" />
         <tli id="T394" time="253.408" type="appl" />
         <tli id="T395" time="254.036" type="appl" />
         <tli id="T396" time="254.664" type="appl" />
         <tli id="T397" time="255.292" type="appl" />
         <tli id="T398" time="255.919" type="appl" />
         <tli id="T399" time="256.547" type="appl" />
         <tli id="T400" time="257.175" type="appl" />
         <tli id="T401" time="257.803" type="appl" />
         <tli id="T402" time="258.431" type="appl" />
         <tli id="T403" time="259.059" type="appl" />
         <tli id="T404" time="259.687" type="appl" />
         <tli id="T405" time="260.314" type="appl" />
         <tli id="T406" time="260.942" type="appl" />
         <tli id="T407" time="261.57" type="appl" />
         <tli id="T408" time="262.198" type="appl" />
         <tli id="T409" time="262.826" type="appl" />
         <tli id="T410" time="263.454" type="appl" />
         <tli id="T411" time="264.082" type="appl" />
         <tli id="T412" time="264.71" type="appl" />
         <tli id="T413" time="265.337" type="appl" />
         <tli id="T414" time="265.965" type="appl" />
         <tli id="T415" time="266.593" type="appl" />
         <tli id="T416" time="267.221" type="appl" />
         <tli id="T417" time="267.863" type="appl" />
         <tli id="T418" time="268.506" type="appl" />
         <tli id="T419" time="269.148" type="appl" />
         <tli id="T420" time="269.791" type="appl" />
         <tli id="T421" time="270.433" type="appl" />
         <tli id="T422" time="271.076" type="appl" />
         <tli id="T423" time="271.718" type="appl" />
         <tli id="T424" time="272.36" type="appl" />
         <tli id="T425" time="273.003" type="appl" />
         <tli id="T426" time="273.645" type="appl" />
         <tli id="T427" time="274.288" type="appl" />
         <tli id="T428" time="274.93" type="appl" />
         <tli id="T429" time="275.74" type="appl" />
         <tli id="T430" time="276.6" type="appl" />
         <tli id="T431" time="277.2419312755549" />
         <tli id="T432" time="277.927" type="appl" />
         <tli id="T433" time="278.485" type="appl" />
         <tli id="T434" time="279.042" type="appl" />
         <tli id="T435" time="279.453" type="appl" />
         <tli id="T436" time="279.864" type="appl" />
         <tli id="T437" time="280.274" type="appl" />
         <tli id="T438" time="280.685" type="appl" />
         <tli id="T439" time="281.096" type="appl" />
         <tli id="T440" time="281.507" type="appl" />
         <tli id="T441" time="281.917" type="appl" />
         <tli id="T443" time="282.739" type="appl" />
         <tli id="T444" time="283.15" type="appl" />
         <tli id="T445" time="283.56" type="appl" />
         <tli id="T446" time="283.971" type="appl" />
         <tli id="T447" time="284.382" type="appl" />
         <tli id="T448" time="284.793" type="appl" />
         <tli id="T450" time="285.614" type="appl" />
         <tli id="T451" time="286.025" type="appl" />
         <tli id="T452" time="286.436" type="appl" />
         <tli id="T453" time="286.847" type="appl" />
         <tli id="T454" time="287.257" type="appl" />
         <tli id="T455" time="287.668" type="appl" />
         <tli id="T456" time="288.079" type="appl" />
         <tli id="T457" time="288.49" type="appl" />
         <tli id="T458" time="288.9" type="appl" />
         <tli id="T459" time="289.311" type="appl" />
         <tli id="T460" time="289.722" type="appl" />
         <tli id="T461" time="290.285" type="appl" />
         <tli id="T462" time="290.849" type="appl" />
         <tli id="T463" time="291.412" type="appl" />
         <tli id="T464" time="291.976" type="appl" />
         <tli id="T465" time="292.539" type="appl" />
         <tli id="T466" time="293.103" type="appl" />
         <tli id="T467" time="293.666" type="appl" />
         <tli id="T468" time="294.23" type="appl" />
         <tli id="T469" time="294.793" type="appl" />
         <tli id="T470" time="295.357" type="appl" />
         <tli id="T471" time="295.92" type="appl" />
         <tli id="T472" time="296.484" type="appl" />
         <tli id="T473" time="297.047" type="appl" />
         <tli id="T474" time="297.611" type="appl" />
         <tli id="T475" time="298.174" type="appl" />
         <tli id="T476" time="298.589" type="appl" />
         <tli id="T477" time="299.005" type="appl" />
         <tli id="T478" time="299.42" type="appl" />
         <tli id="T479" time="300.04" type="appl" />
         <tli id="T480" time="300.66" type="appl" />
         <tli id="T481" time="301.28" type="appl" />
         <tli id="T482" time="301.9" type="appl" />
         <tli id="T483" time="302.419" type="appl" />
         <tli id="T484" time="302.939" type="appl" />
         <tli id="T485" time="303.458" type="appl" />
         <tli id="T486" time="303.978" type="appl" />
         <tli id="T487" time="304.497" type="appl" />
         <tli id="T488" time="305.016" type="appl" />
         <tli id="T489" time="305.536" type="appl" />
         <tli id="T490" time="306.055" type="appl" />
         <tli id="T491" time="306.575" type="appl" />
         <tli id="T492" time="307.094" type="appl" />
         <tli id="T493" time="307.555" type="appl" />
         <tli id="T494" time="308.016" type="appl" />
         <tli id="T495" time="308.478" type="appl" />
         <tli id="T496" time="308.939" type="appl" />
         <tli id="T497" time="309.01472191828685" />
         <tli id="T498" time="309.89" type="appl" />
         <tli id="T499" time="310.853" type="appl" />
         <tli id="T500" time="311.816" type="appl" />
         <tli id="T501" time="312.6879925108478" />
         <tli id="T502" time="313.46" type="appl" />
         <tli id="T503" time="314.14" type="appl" />
         <tli id="T504" time="314.821" type="appl" />
         <tli id="T505" time="315.501" type="appl" />
         <tli id="T506" time="316.181" type="appl" />
         <tli id="T507" time="316.861" type="appl" />
         <tli id="T508" time="317.541" type="appl" />
         <tli id="T509" time="318.222" type="appl" />
         <tli id="T510" time="318.902" type="appl" />
         <tli id="T511" time="319.582" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-MaPX"
                      id="tx-MaPX"
                      speaker="MaPX"
                      type="t">
         <timeline-fork end="T61" start="T60">
            <tli id="T60.tx-MaPX.1" />
         </timeline-fork>
         <timeline-fork end="T136" start="T135">
            <tli id="T135.tx-MaPX.1" />
         </timeline-fork>
         <timeline-fork end="T197" start="T196">
            <tli id="T196.tx-MaPX.1" />
         </timeline-fork>
         <timeline-fork end="T234" start="T233">
            <tli id="T233.tx-MaPX.1" />
         </timeline-fork>
         <timeline-fork end="T505" start="T504">
            <tli id="T504.tx-MaPX.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-MaPX">
            <ts e="T15" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Min</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">Krasnajarskaj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">pʼedagagʼičʼeskaj</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">instʼitutɨ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">büppütüm</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Istorʼijaːnɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">školaga</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ogoloru</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">ü͡örete</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">hɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">istorʼija</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">i</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">abšʼestvaznanʼije</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_46" n="HIAT:w" s="T13">di͡en</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_49" n="HIAT:w" s="T14">prʼedmʼetteri</ts>
                  <nts id="Seg_50" n="HIAT:ip">.</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T149" id="Seg_52" n="sc" s="T39">
               <ts e="T53" id="Seg_54" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_56" n="HIAT:w" s="T39">U͡on</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_59" n="HIAT:w" s="T40">hette</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_62" n="HIAT:w" s="T41">bötürü͡öpteːkke</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_65" n="HIAT:w" s="T42">haka</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_68" n="HIAT:w" s="T43">hiritten</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_72" n="HIAT:w" s="T44">Lʼena</ts>
                  <nts id="Seg_73" n="HIAT:ip">,</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_76" n="HIAT:w" s="T45">orto</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_79" n="HIAT:w" s="T46">Lʼenattan</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_82" n="HIAT:ip">(</nts>
                  <ts e="T48" id="Seg_84" n="HIAT:w" s="T47">u͡on</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_87" n="HIAT:w" s="T48">het-</ts>
                  <nts id="Seg_88" n="HIAT:ip">)</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_91" n="HIAT:w" s="T50">kelbitter</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_94" n="HIAT:w" s="T51">bastɨkɨː</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_97" n="HIAT:w" s="T52">hakalar</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_101" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_103" n="HIAT:w" s="T53">Nʼuːčča</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_106" n="HIAT:w" s="T54">hirdemmitiger</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_109" n="HIAT:w" s="T55">Lʼenaga</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_112" n="HIAT:w" s="T56">epiʼdʼeːmʼijeler</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_115" n="HIAT:w" s="T57">bu͡olbuttara</ts>
                  <nts id="Seg_116" n="HIAT:ip">,</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_119" n="HIAT:w" s="T58">u͡ospa</ts>
                  <nts id="Seg_120" n="HIAT:ip">,</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_123" n="HIAT:w" s="T59">čʼuma</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60.tx-MaPX.1" id="Seg_127" n="HIAT:w" s="T60">sʼibʼirskaj</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_130" n="HIAT:w" s="T60.tx-MaPX.1">jazva</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_133" n="HIAT:w" s="T61">di͡en</ts>
                  <nts id="Seg_134" n="HIAT:ip">,</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_137" n="HIAT:w" s="T62">kihiler</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_141" n="HIAT:w" s="T63">dʼizʼentʼerʼija</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_145" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_147" n="HIAT:w" s="T64">Onno</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_150" n="HIAT:w" s="T65">tɨːnnaːk</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_153" n="HIAT:w" s="T66">kaːlbɨt</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_156" n="HIAT:w" s="T67">kihileriŋ</ts>
                  <nts id="Seg_157" n="HIAT:ip">,</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_160" n="HIAT:w" s="T68">kotuː</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_163" n="HIAT:w" s="T69">hir</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_166" n="HIAT:w" s="T70">di͡ek</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_170" n="HIAT:w" s="T71">eː</ts>
                  <nts id="Seg_171" n="HIAT:ip">,</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_174" n="HIAT:w" s="T72">baːj</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_178" n="HIAT:w" s="T73">ki͡eŋ</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_181" n="HIAT:w" s="T74">kü͡öl</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_184" n="HIAT:w" s="T75">baːr</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_187" n="HIAT:w" s="T76">di͡en</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_190" n="HIAT:w" s="T77">hurakka</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_193" n="HIAT:w" s="T78">kelbitter</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_197" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_199" n="HIAT:w" s="T79">Onon</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_202" n="HIAT:w" s="T80">Dʼehi͡ej</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_205" n="HIAT:w" s="T81">ebe</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_208" n="HIAT:w" s="T82">kɨtɨlɨgar</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_211" n="HIAT:w" s="T83">oloron</ts>
                  <nts id="Seg_212" n="HIAT:ip">,</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_215" n="HIAT:w" s="T84">ol</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_218" n="HIAT:w" s="T85">kelelleriger</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_222" n="HIAT:w" s="T86">Dʼehi͡ej</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_225" n="HIAT:w" s="T87">kü͡ölüŋ</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_228" n="HIAT:w" s="T88">kɨtɨllara</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_231" n="HIAT:w" s="T89">iččitek</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_235" n="HIAT:w" s="T90">ühü</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_239" n="HIAT:w" s="T91">kihi</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_242" n="HIAT:w" s="T92">hu͡ok</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_246" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_248" n="HIAT:w" s="T93">Urahalar</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_251" n="HIAT:w" s="T94">ire</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_253" n="HIAT:ip">(</nts>
                  <ts e="T96" id="Seg_255" n="HIAT:w" s="T95">turbut</ts>
                  <nts id="Seg_256" n="HIAT:ip">)</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_259" n="HIAT:w" s="T96">turallar</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_263" n="HIAT:w" s="T97">dʼi͡eleriŋ</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_266" n="HIAT:w" s="T98">tu͡ostara</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_269" n="HIAT:w" s="T99">hɨtɨjbɨttar</ts>
                  <nts id="Seg_270" n="HIAT:ip">.</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_273" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_275" n="HIAT:w" s="T100">Kihiler</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_278" n="HIAT:w" s="T101">uŋu͡oktara</ts>
                  <nts id="Seg_279" n="HIAT:ip">,</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_282" n="HIAT:w" s="T102">tabalar</ts>
                  <nts id="Seg_283" n="HIAT:ip">,</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_286" n="HIAT:w" s="T103">ɨttar</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_289" n="HIAT:w" s="T104">uŋu͡oktara</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_292" n="HIAT:w" s="T105">hɨtallar</ts>
                  <nts id="Seg_293" n="HIAT:ip">,</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_296" n="HIAT:w" s="T106">ühü</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_300" n="HIAT:w" s="T107">tögürüččü</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_303" n="HIAT:w" s="T108">Dʼeheji</ts>
                  <nts id="Seg_304" n="HIAT:ip">.</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_307" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_309" n="HIAT:w" s="T109">Onno</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_312" n="HIAT:w" s="T110">u͡ospattan</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_315" n="HIAT:w" s="T111">ölbütter</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_318" n="HIAT:w" s="T112">toŋustar</ts>
                  <nts id="Seg_319" n="HIAT:ip">,</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_322" n="HIAT:w" s="T113">dʼuraːktar</ts>
                  <nts id="Seg_323" n="HIAT:ip">,</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_326" n="HIAT:w" s="T114">samajedɨ</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_330" n="HIAT:w" s="T115">plʼemʼena</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_333" n="HIAT:w" s="T116">tam</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_336" n="HIAT:w" s="T117">žɨlʼi</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_340" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_342" n="HIAT:w" s="T118">Oloru</ts>
                  <nts id="Seg_343" n="HIAT:ip">,</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_346" n="HIAT:w" s="T119">ojun</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_349" n="HIAT:w" s="T120">kɨraːbɨt</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_352" n="HIAT:w" s="T121">hire</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_355" n="HIAT:w" s="T122">di͡en</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_358" n="HIAT:w" s="T123">küreteletteːbitter</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_362" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_364" n="HIAT:w" s="T124">Onu</ts>
                  <nts id="Seg_365" n="HIAT:ip">,</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_368" n="HIAT:w" s="T125">ol</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_371" n="HIAT:w" s="T126">iččitek</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_374" n="HIAT:w" s="T127">hirge</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_377" n="HIAT:w" s="T128">kelenner</ts>
                  <nts id="Seg_378" n="HIAT:ip">,</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_381" n="HIAT:w" s="T129">hakalarɨŋ</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_384" n="HIAT:w" s="T130">törüt</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_387" n="HIAT:w" s="T131">hir</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_390" n="HIAT:w" s="T132">oŋostubuttar</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_393" n="HIAT:w" s="T133">Dʼehi͡eji</ts>
                  <nts id="Seg_394" n="HIAT:ip">.</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_397" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_399" n="HIAT:w" s="T134">Onton</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135.tx-MaPX.1" id="Seg_402" n="HIAT:w" s="T135">gdʼe</ts>
                  <nts id="Seg_403" n="HIAT:ip">_</nts>
                  <ts e="T136" id="Seg_405" n="HIAT:w" s="T135.tx-MaPX.1">tа</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_408" n="HIAT:w" s="T136">u͡on</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_411" n="HIAT:w" s="T137">togus</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_414" n="HIAT:w" s="T138">bötürü͡ökke</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_417" n="HIAT:w" s="T139">Tajmɨr</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_420" n="HIAT:w" s="T140">di͡ek</ts>
                  <nts id="Seg_421" n="HIAT:ip">,</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_424" n="HIAT:w" s="T141">horok</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_427" n="HIAT:w" s="T142">kihiler</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_430" n="HIAT:w" s="T143">Tajmɨr</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_433" n="HIAT:w" s="T144">di͡ek</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_436" n="HIAT:w" s="T145">uže</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_439" n="HIAT:w" s="T146">köhö</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_442" n="HIAT:w" s="T147">hɨldʼar</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_445" n="HIAT:w" s="T148">bu͡olbuttar</ts>
                  <nts id="Seg_446" n="HIAT:ip">.</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T170" id="Seg_448" n="sc" s="T151">
               <ts e="T155" id="Seg_450" n="HIAT:u" s="T151">
                  <nts id="Seg_451" n="HIAT:ip">(</nts>
                  <ts e="T153" id="Seg_453" n="HIAT:w" s="T151">Tabalannan</ts>
                  <nts id="Seg_454" n="HIAT:ip">)</nts>
                  <nts id="Seg_455" n="HIAT:ip">,</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_458" n="HIAT:w" s="T153">tabannan</ts>
                  <nts id="Seg_459" n="HIAT:ip">,</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_462" n="HIAT:w" s="T154">tabannan</ts>
                  <nts id="Seg_463" n="HIAT:ip">.</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_466" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_468" n="HIAT:w" s="T155">Uže</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_471" n="HIAT:w" s="T156">ol</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_474" n="HIAT:w" s="T157">tabanɨ</ts>
                  <nts id="Seg_475" n="HIAT:ip">,</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_478" n="HIAT:w" s="T158">uže</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_481" n="HIAT:w" s="T159">iːter</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_484" n="HIAT:w" s="T160">kihi</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_487" n="HIAT:w" s="T161">bu͡olbuttar</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_490" n="HIAT:w" s="T162">bu͡o</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_494" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_496" n="HIAT:w" s="T163">Mm</ts>
                  <nts id="Seg_497" n="HIAT:ip">,</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_500" n="HIAT:w" s="T164">bettek</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_503" n="HIAT:w" s="T165">kotuː</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_506" n="HIAT:w" s="T166">hir</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_509" n="HIAT:w" s="T167">di͡ek</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_512" n="HIAT:w" s="T168">kelen</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_515" n="HIAT:w" s="T169">baraːnnar</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T272" id="Seg_518" n="sc" s="T181">
               <ts e="T190" id="Seg_520" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_522" n="HIAT:w" s="T181">Dʼehi͡ejderiŋ</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_525" n="HIAT:w" s="T182">eː</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_528" n="HIAT:w" s="T183">barɨlara</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_531" n="HIAT:w" s="T184">da</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_534" n="HIAT:w" s="T185">bu͡olbattar</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_537" n="HIAT:w" s="T186">baːj</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_540" n="HIAT:w" s="T187">kihiler</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_544" n="HIAT:w" s="T188">baːj</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_547" n="HIAT:w" s="T189">ɨ͡allar</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_551" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_553" n="HIAT:w" s="T190">Hebi͡eskej</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_556" n="HIAT:w" s="T191">bɨlaːs</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_559" n="HIAT:w" s="T192">turarɨgar</ts>
                  <nts id="Seg_560" n="HIAT:ip">,</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_563" n="HIAT:w" s="T193">hebi͡eskej</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_566" n="HIAT:w" s="T194">bɨlaːstan</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_569" n="HIAT:w" s="T195">küreːnner</ts>
                  <nts id="Seg_570" n="HIAT:ip">,</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196.tx-MaPX.1" id="Seg_573" n="HIAT:w" s="T196">v</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_576" n="HIAT:w" s="T196.tx-MaPX.1">asnavnom</ts>
                  <nts id="Seg_577" n="HIAT:ip">,</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_580" n="HIAT:w" s="T197">urut</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_583" n="HIAT:w" s="T198">otto</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_586" n="HIAT:w" s="T199">taːk</ts>
                  <nts id="Seg_587" n="HIAT:ip">…</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_590" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_592" n="HIAT:w" s="T201">Atɨː</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_595" n="HIAT:w" s="T202">hɨldʼallar</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_598" n="HIAT:w" s="T203">bu͡o</ts>
                  <nts id="Seg_599" n="HIAT:ip">,</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_602" n="HIAT:w" s="T204">ɨ͡allana</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_605" n="HIAT:w" s="T205">hɨldʼallar</ts>
                  <nts id="Seg_606" n="HIAT:ip">,</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_609" n="HIAT:w" s="T206">horoktoro</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_612" n="HIAT:w" s="T207">kaːlallar</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_615" n="HIAT:w" s="T208">eː</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_618" n="HIAT:w" s="T209">mannaːgɨ</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_621" n="HIAT:w" s="T210">kihi</ts>
                  <nts id="Seg_622" n="HIAT:ip">,</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_625" n="HIAT:w" s="T211">Tajmu͡orga</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_628" n="HIAT:w" s="T212">baːr</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_631" n="HIAT:w" s="T213">kergen</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_634" n="HIAT:w" s="T214">bulunannar</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_637" n="HIAT:w" s="T215">eŋin</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_640" n="HIAT:w" s="T216">kaːlallar</ts>
                  <nts id="Seg_641" n="HIAT:ip">.</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_644" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_646" n="HIAT:w" s="T217">Aː</ts>
                  <nts id="Seg_647" n="HIAT:ip">,</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_650" n="HIAT:w" s="T218">no</ts>
                  <nts id="Seg_651" n="HIAT:ip">,</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_654" n="HIAT:w" s="T219">savʼetskaj</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_657" n="HIAT:w" s="T220">bɨlaːs</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_660" n="HIAT:w" s="T221">turu͡or</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_663" n="HIAT:w" s="T222">di͡eri</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_666" n="HIAT:w" s="T223">Dʼehi͡ej</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_669" n="HIAT:w" s="T224">hakalara</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_672" n="HIAT:w" s="T225">biːr</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_675" n="HIAT:w" s="T226">hirge</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_678" n="HIAT:w" s="T227">oloror</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_681" n="HIAT:w" s="T228">etiler</ts>
                  <nts id="Seg_682" n="HIAT:ip">.</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_685" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_687" n="HIAT:w" s="T229">Onton</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_690" n="HIAT:w" s="T230">savʼetskaj</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_693" n="HIAT:w" s="T231">vlaːhɨttan</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_696" n="HIAT:w" s="T232">küreːn</ts>
                  <nts id="Seg_697" n="HIAT:ip">,</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233.tx-MaPX.1" id="Seg_700" n="HIAT:w" s="T233">v</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_703" n="HIAT:w" s="T233.tx-MaPX.1">asnavnom</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_706" n="HIAT:w" s="T234">baːj</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_709" n="HIAT:w" s="T235">ɨ͡allar</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_712" n="HIAT:w" s="T236">bettek</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_715" n="HIAT:w" s="T237">keletteːbitter</ts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_719" n="HIAT:w" s="T238">munna</ts>
                  <nts id="Seg_720" n="HIAT:ip">,</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_723" n="HIAT:w" s="T239">Tajmu͡orga</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_726" n="HIAT:w" s="T240">baːr</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_729" n="HIAT:w" s="T241">uruːlarɨgar</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_732" n="HIAT:w" s="T242">čugas</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_736" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_738" n="HIAT:w" s="T243">Könö</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_741" n="HIAT:w" s="T244">biler</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_744" n="HIAT:w" s="T245">hirderiger</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_747" n="HIAT:w" s="T246">eː</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_750" n="HIAT:w" s="T247">kelbitter</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_753" n="HIAT:w" s="T248">köhöttöːn</ts>
                  <nts id="Seg_754" n="HIAT:ip">.</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_757" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_759" n="HIAT:w" s="T249">Onton</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_762" n="HIAT:w" s="T250">hüːrbe</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_765" n="HIAT:w" s="T251">dʼɨllar</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_768" n="HIAT:w" s="T252">bütelleriger</ts>
                  <nts id="Seg_769" n="HIAT:ip">,</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_772" n="HIAT:w" s="T253">otut</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_775" n="HIAT:w" s="T254">dʼɨllar</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_778" n="HIAT:w" s="T255">taksallarɨgar</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_781" n="HIAT:w" s="T256">Tajmu͡orga</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_784" n="HIAT:w" s="T257">emi͡e</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_787" n="HIAT:w" s="T258">savʼetskaj</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_790" n="HIAT:w" s="T259">bɨlaːha</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_793" n="HIAT:w" s="T260">palʼitʼikata</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_796" n="HIAT:w" s="T261">kɨtaːtan</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_799" n="HIAT:w" s="T262">ispit</ts>
                  <nts id="Seg_800" n="HIAT:ip">.</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_803" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_805" n="HIAT:w" s="T263">Emi͡e</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_808" n="HIAT:w" s="T264">dʼe</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_811" n="HIAT:w" s="T265">baːj</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_814" n="HIAT:w" s="T266">kihileri</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_817" n="HIAT:w" s="T267">baːjdarɨn</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_820" n="HIAT:w" s="T268">talaːn</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_823" n="HIAT:w" s="T269">üllesti͡ekke</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_826" n="HIAT:w" s="T270">di͡en</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_829" n="HIAT:w" s="T271">bu͡olbut</ts>
                  <nts id="Seg_830" n="HIAT:ip">.</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T331" id="Seg_832" n="sc" s="T276">
               <ts e="T278" id="Seg_834" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_836" n="HIAT:w" s="T276">Tabalarɨn</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_839" n="HIAT:w" s="T277">bɨldʼaːn</ts>
                  <nts id="Seg_840" n="HIAT:ip">.</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_843" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_845" n="HIAT:w" s="T278">Onton</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_848" n="HIAT:w" s="T279">tabalarɨn</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_851" n="HIAT:w" s="T280">ere</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_854" n="HIAT:w" s="T281">bu͡olbat</ts>
                  <nts id="Seg_855" n="HIAT:ip">,</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_858" n="HIAT:w" s="T282">dʼi͡e</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_861" n="HIAT:w" s="T283">üp</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_864" n="HIAT:w" s="T284">tüːnün</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_866" n="HIAT:ip">(</nts>
                  <ts e="T286" id="Seg_868" n="HIAT:w" s="T285">talɨːllar</ts>
                  <nts id="Seg_869" n="HIAT:ip">)</nts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_872" n="HIAT:w" s="T286">bu͡o</ts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_876" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_878" n="HIAT:w" s="T287">Onu</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_881" n="HIAT:w" s="T288">barɨtɨn</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_884" n="HIAT:w" s="T289">tügeti͡ekke</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_887" n="HIAT:w" s="T290">naːda</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_890" n="HIAT:w" s="T291">di͡en</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_893" n="HIAT:w" s="T292">dʼadaŋɨlarga</ts>
                  <nts id="Seg_894" n="HIAT:ip">.</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_897" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_899" n="HIAT:w" s="T293">Urut</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_902" n="HIAT:w" s="T294">daː</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_905" n="HIAT:w" s="T295">otto</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_908" n="HIAT:w" s="T296">baːj</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_911" n="HIAT:w" s="T297">kihiŋ</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_914" n="HIAT:w" s="T298">bɨlɨrgɨ</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_917" n="HIAT:w" s="T299">haku͡onɨnan</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_920" n="HIAT:w" s="T300">kimi</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_923" n="HIAT:w" s="T301">da</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_926" n="HIAT:w" s="T302">atagastaːbakka</ts>
                  <nts id="Seg_927" n="HIAT:ip">,</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_930" n="HIAT:w" s="T303">ülehitin</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_933" n="HIAT:w" s="T304">da</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_936" n="HIAT:w" s="T305">atagastaːbakka</ts>
                  <nts id="Seg_937" n="HIAT:ip">,</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_940" n="HIAT:w" s="T306">bejetin</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_943" n="HIAT:w" s="T307">uruːtun</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_946" n="HIAT:w" s="T308">da</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_948" n="HIAT:ip">(</nts>
                  <ts e="T311" id="Seg_950" n="HIAT:w" s="T309">atagas-</ts>
                  <nts id="Seg_951" n="HIAT:ip">)</nts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_955" n="HIAT:w" s="T311">dʼadaŋɨ</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_958" n="HIAT:w" s="T312">uruːtun</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_961" n="HIAT:w" s="T313">da</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_964" n="HIAT:w" s="T314">atagastaːbakka</ts>
                  <nts id="Seg_965" n="HIAT:ip">,</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_968" n="HIAT:w" s="T315">olorbuttar</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_971" n="HIAT:w" s="T316">bu͡o</ts>
                  <nts id="Seg_972" n="HIAT:ip">,</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_975" n="HIAT:w" s="T317">abɨčajdara</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_978" n="HIAT:w" s="T318">onnuk</ts>
                  <nts id="Seg_979" n="HIAT:ip">.</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_982" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_984" n="HIAT:w" s="T319">Kömölöhön</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_987" n="HIAT:w" s="T320">olorbuttar</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_990" n="HIAT:w" s="T321">bu͡o</ts>
                  <nts id="Seg_991" n="HIAT:ip">.</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_994" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_996" n="HIAT:w" s="T322">Savʼetskaj</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_999" n="HIAT:w" s="T323">bɨlaːs</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1002" n="HIAT:w" s="T324">palʼitʼikatɨn</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1005" n="HIAT:w" s="T325">gɨtta</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1008" n="HIAT:w" s="T326">höbülespetek</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1011" n="HIAT:w" s="T327">kihileriŋ</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1014" n="HIAT:w" s="T328">manna</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1017" n="HIAT:w" s="T329">bunt</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1020" n="HIAT:w" s="T330">tahaːrbɨttara</ts>
                  <nts id="Seg_1021" n="HIAT:ip">.</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T389" id="Seg_1023" n="sc" s="T334">
               <ts e="T337" id="Seg_1025" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_1027" n="HIAT:w" s="T334">Heː</ts>
                  <nts id="Seg_1028" n="HIAT:ip">,</nts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1031" n="HIAT:w" s="T335">aːjdahan</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1034" n="HIAT:w" s="T336">turbut</ts>
                  <nts id="Seg_1035" n="HIAT:ip">.</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1038" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1040" n="HIAT:w" s="T337">Dʼe</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1043" n="HIAT:w" s="T338">onno</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1046" n="HIAT:w" s="T339">ol</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1049" n="HIAT:w" s="T340">ulakan</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1052" n="HIAT:w" s="T341">ajdaːŋŋa</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1055" n="HIAT:w" s="T342">kihini</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1058" n="HIAT:w" s="T343">eŋin</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1061" n="HIAT:w" s="T344">ölörbütter</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1063" n="HIAT:ip">–</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1066" n="HIAT:w" s="T345">manna</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1069" n="HIAT:w" s="T346">tu͡oktarɨ</ts>
                  <nts id="Seg_1070" n="HIAT:ip">,</nts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1073" n="HIAT:w" s="T347">upalnamočennɨjdarɨ</ts>
                  <nts id="Seg_1074" n="HIAT:ip">.</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1077" n="HIAT:u" s="T348">
                  <nts id="Seg_1078" n="HIAT:ip">(</nts>
                  <ts e="T350" id="Seg_1080" n="HIAT:w" s="T348">Olu-</ts>
                  <nts id="Seg_1081" n="HIAT:ip">)</nts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1084" n="HIAT:w" s="T350">tʼe</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1087" n="HIAT:w" s="T351">ol</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1090" n="HIAT:w" s="T352">tɨ͡a</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1093" n="HIAT:w" s="T353">kihite</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1096" n="HIAT:w" s="T354">ölörbüte</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1099" n="HIAT:w" s="T355">billibet</ts>
                  <nts id="Seg_1100" n="HIAT:ip">,</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1103" n="HIAT:w" s="T356">ili</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1106" n="HIAT:w" s="T357">že</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1109" n="HIAT:w" s="T358">atɨn</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1112" n="HIAT:w" s="T359">tu͡ok</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1115" n="HIAT:w" s="T360">eme</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1118" n="HIAT:w" s="T361">kihite</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1121" n="HIAT:w" s="T362">anaːn</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1125" n="HIAT:w" s="T363">onnuk</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1128" n="HIAT:w" s="T364">aːjdaːnɨ</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1131" n="HIAT:w" s="T365">tahaːraːrɨ</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1134" n="HIAT:w" s="T366">ölörbüte</ts>
                  <nts id="Seg_1135" n="HIAT:ip">,</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1138" n="HIAT:w" s="T367">billibet</ts>
                  <nts id="Seg_1139" n="HIAT:ip">.</nts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1142" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1144" n="HIAT:w" s="T368">Anɨga</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1147" n="HIAT:w" s="T369">daː</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1150" n="HIAT:w" s="T370">di͡eri</ts>
                  <nts id="Seg_1151" n="HIAT:ip">.</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1154" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1156" n="HIAT:w" s="T371">Onno</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1159" n="HIAT:w" s="T372">aktʼivnaj</ts>
                  <nts id="Seg_1160" n="HIAT:ip">,</nts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1163" n="HIAT:w" s="T373">dʼe</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1166" n="HIAT:w" s="T374">otto</ts>
                  <nts id="Seg_1167" n="HIAT:ip">,</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1170" n="HIAT:w" s="T375">aristuːjdaːbɨttar</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1173" n="HIAT:w" s="T376">bu͡o</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1175" n="HIAT:ip">(</nts>
                  <ts e="T379" id="Seg_1177" n="HIAT:w" s="T377">aːrʼes-</ts>
                  <nts id="Seg_1178" n="HIAT:ip">)</nts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1180" n="HIAT:ip">(</nts>
                  <ts e="T381" id="Seg_1182" n="HIAT:w" s="T379">a-</ts>
                  <nts id="Seg_1183" n="HIAT:ip">)</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1186" n="HIAT:w" s="T381">aːrʼestɨ</ts>
                  <nts id="Seg_1187" n="HIAT:ip">.</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1190" n="HIAT:u" s="T382">
                  <nts id="Seg_1191" n="HIAT:ip">"</nts>
                  <ts e="T383" id="Seg_1193" n="HIAT:w" s="T382">Atrʼad</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1196" n="HIAT:w" s="T383">čonovcev</ts>
                  <nts id="Seg_1197" n="HIAT:ip">"</nts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1200" n="HIAT:w" s="T384">di͡en</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1203" n="HIAT:w" s="T385">kihi</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1206" n="HIAT:w" s="T386">kelbitter</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1209" n="HIAT:w" s="T387">Xaːtangattan</ts>
                  <nts id="Seg_1210" n="HIAT:ip">,</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1213" n="HIAT:w" s="T388">Jenʼisʼejskʼejten</ts>
                  <nts id="Seg_1214" n="HIAT:ip">.</nts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T428" id="Seg_1216" n="sc" s="T391">
               <ts e="T394" id="Seg_1218" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1220" n="HIAT:w" s="T391">Nʼuːččalar</ts>
                  <nts id="Seg_1221" n="HIAT:ip">,</nts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1224" n="HIAT:w" s="T392">heː</ts>
                  <nts id="Seg_1225" n="HIAT:ip">,</nts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1228" n="HIAT:w" s="T393">haːlaːk</ts>
                  <nts id="Seg_1229" n="HIAT:ip">.</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1232" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1234" n="HIAT:w" s="T394">Dʼe</ts>
                  <nts id="Seg_1235" n="HIAT:ip">,</nts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1237" n="HIAT:ip">(</nts>
                  <ts e="T396" id="Seg_1239" n="HIAT:w" s="T395">on-</ts>
                  <nts id="Seg_1240" n="HIAT:ip">)</nts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1243" n="HIAT:w" s="T396">ol</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1246" n="HIAT:w" s="T397">huraktan</ts>
                  <nts id="Seg_1247" n="HIAT:ip">,</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1250" n="HIAT:w" s="T398">kihileri</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1253" n="HIAT:w" s="T399">kaːjɨːga</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1256" n="HIAT:w" s="T400">olordollor</ts>
                  <nts id="Seg_1257" n="HIAT:ip">,</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1260" n="HIAT:w" s="T401">ühü</ts>
                  <nts id="Seg_1261" n="HIAT:ip">,</nts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1264" n="HIAT:w" s="T402">di͡en</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1267" n="HIAT:w" s="T403">huraktan</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1270" n="HIAT:w" s="T404">min</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1273" n="HIAT:w" s="T405">inʼem</ts>
                  <nts id="Seg_1274" n="HIAT:ip">,</nts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1277" n="HIAT:w" s="T406">agam</ts>
                  <nts id="Seg_1278" n="HIAT:ip">,</nts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1281" n="HIAT:w" s="T407">ehem</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1284" n="HIAT:w" s="T408">bu͡olannar</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1287" n="HIAT:w" s="T409">dʼe</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1290" n="HIAT:w" s="T410">kürüːrge</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1293" n="HIAT:w" s="T411">turbuttar</ts>
                  <nts id="Seg_1294" n="HIAT:ip">,</nts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1297" n="HIAT:w" s="T412">i</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1300" n="HIAT:w" s="T413">töttörü</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1303" n="HIAT:w" s="T414">Dʼehi͡ejderiger</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1306" n="HIAT:w" s="T415">tönnübütter</ts>
                  <nts id="Seg_1307" n="HIAT:ip">.</nts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1310" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1312" n="HIAT:w" s="T416">Agam</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1315" n="HIAT:w" s="T417">braːta</ts>
                  <nts id="Seg_1316" n="HIAT:ip">,</nts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1319" n="HIAT:w" s="T418">Ignatʼij</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1322" n="HIAT:w" s="T419">di͡en</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1325" n="HIAT:w" s="T420">kihini</ts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1328" n="HIAT:w" s="T421">ɨlbɨttar</ts>
                  <nts id="Seg_1329" n="HIAT:ip">,</nts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1332" n="HIAT:w" s="T422">kaːjɨːga</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1335" n="HIAT:w" s="T423">olordon</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1338" n="HIAT:w" s="T424">baraːn</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1340" n="HIAT:ip">(</nts>
                  <ts e="T426" id="Seg_1342" n="HIAT:w" s="T425">ɨppɨt-</ts>
                  <nts id="Seg_1343" n="HIAT:ip">)</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1346" n="HIAT:w" s="T426">ölörbütter</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1349" n="HIAT:w" s="T427">če</ts>
                  <nts id="Seg_1350" n="HIAT:ip">.</nts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T475" id="Seg_1352" n="sc" s="T431">
               <ts e="T434" id="Seg_1354" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1356" n="HIAT:w" s="T431">Heː</ts>
                  <nts id="Seg_1357" n="HIAT:ip">,</nts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1360" n="HIAT:w" s="T432">Majmagolar</ts>
                  <nts id="Seg_1361" n="HIAT:ip">,</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1364" n="HIAT:w" s="T433">aga</ts>
                  <nts id="Seg_1365" n="HIAT:ip">.</nts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1368" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1370" n="HIAT:w" s="T434">Onton</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1373" n="HIAT:w" s="T435">agam</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1376" n="HIAT:w" s="T436">kɨra</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1379" n="HIAT:w" s="T437">bɨraːta</ts>
                  <nts id="Seg_1380" n="HIAT:ip">,</nts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1383" n="HIAT:w" s="T438">Danʼil</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1386" n="HIAT:w" s="T439">di͡en</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1389" n="HIAT:w" s="T440">kihi</ts>
                  <nts id="Seg_1390" n="HIAT:ip">,</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1392" n="HIAT:ip">(</nts>
                  <ts e="T443" id="Seg_1394" n="HIAT:w" s="T441">ɨ͡alla-</ts>
                  <nts id="Seg_1395" n="HIAT:ip">)</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1398" n="HIAT:w" s="T443">uruːlarɨgar</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1401" n="HIAT:w" s="T444">ɨ͡allana</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1404" n="HIAT:w" s="T445">barbɨt</ts>
                  <nts id="Seg_1405" n="HIAT:ip">,</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1408" n="HIAT:w" s="T446">keːhe</ts>
                  <nts id="Seg_1409" n="HIAT:ip">,</nts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1412" n="HIAT:w" s="T447">kürüːr</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1414" n="HIAT:ip">(</nts>
                  <ts e="T450" id="Seg_1416" n="HIAT:w" s="T448">ke-</ts>
                  <nts id="Seg_1417" n="HIAT:ip">)</nts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1420" n="HIAT:w" s="T450">küreːbitter</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1423" n="HIAT:w" s="T451">bu͡o</ts>
                  <nts id="Seg_1424" n="HIAT:ip">,</nts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1427" n="HIAT:w" s="T452">ol</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1430" n="HIAT:w" s="T453">ihin</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1433" n="HIAT:w" s="T454">ontuŋ</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1436" n="HIAT:w" s="T455">ol</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1439" n="HIAT:w" s="T456">ogolorun</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1442" n="HIAT:w" s="T457">daː</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1445" n="HIAT:w" s="T458">keːhe</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1448" n="HIAT:w" s="T459">barbɨttar</ts>
                  <nts id="Seg_1449" n="HIAT:ip">.</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1452" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_1454" n="HIAT:w" s="T460">I</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1457" n="HIAT:w" s="T461">ol</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1460" n="HIAT:w" s="T462">Danʼil</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1463" n="HIAT:w" s="T463">bu͡o</ts>
                  <nts id="Seg_1464" n="HIAT:ip">,</nts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1467" n="HIAT:w" s="T464">manna</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1470" n="HIAT:w" s="T465">kamkɨ</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1473" n="HIAT:w" s="T466">kɨrdʼɨ͡ar</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1476" n="HIAT:w" s="T467">di͡eri</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1479" n="HIAT:w" s="T468">Tajmɨrga</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1482" n="HIAT:w" s="T469">üleleːn</ts>
                  <nts id="Seg_1483" n="HIAT:ip">,</nts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1486" n="HIAT:w" s="T470">Ordʼen</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1489" n="HIAT:w" s="T471">Trudavoːva</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1492" n="HIAT:w" s="T472">Krasnava</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1495" n="HIAT:w" s="T473">znamʼenʼi</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1498" n="HIAT:w" s="T474">zarabotal</ts>
                  <nts id="Seg_1499" n="HIAT:ip">.</nts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T492" id="Seg_1501" n="sc" s="T478">
               <ts e="T482" id="Seg_1503" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1505" n="HIAT:w" s="T478">Majmago</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1508" n="HIAT:w" s="T479">Danʼil</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1511" n="HIAT:w" s="T480">Stʼepanavʼičʼ</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1514" n="HIAT:w" s="T481">di͡en</ts>
                  <nts id="Seg_1515" n="HIAT:ip">.</nts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T492" id="Seg_1518" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1520" n="HIAT:w" s="T482">Ol</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1523" n="HIAT:w" s="T483">kihi</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1526" n="HIAT:w" s="T484">taba</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1529" n="HIAT:w" s="T485">iːtiːtiger</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1532" n="HIAT:w" s="T486">kamkɨ</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1535" n="HIAT:w" s="T487">kɨrdʼɨ͡ar</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1538" n="HIAT:w" s="T488">di͡eri</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1541" n="HIAT:w" s="T489">üleleːbit</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1544" n="HIAT:w" s="T490">ogo</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1547" n="HIAT:w" s="T491">haːhɨttan</ts>
                  <nts id="Seg_1548" n="HIAT:ip">.</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T511" id="Seg_1550" n="sc" s="T497">
               <ts e="T501" id="Seg_1552" n="HIAT:u" s="T497">
                  <ts e="T498" id="Seg_1554" n="HIAT:w" s="T497">Valačanskaj</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1557" n="HIAT:w" s="T498">rajon</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1560" n="HIAT:w" s="T499">Kamʼeŋŋa</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1563" n="HIAT:w" s="T500">ete</ts>
                  <nts id="Seg_1564" n="HIAT:ip">.</nts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T511" id="Seg_1567" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1569" n="HIAT:w" s="T501">Onton</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1572" n="HIAT:w" s="T502">kennikiː</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1575" n="HIAT:w" s="T503">dʼɨllarɨn</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504.tx-MaPX.1" id="Seg_1578" n="HIAT:w" s="T504">Xantajskaj</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1581" n="HIAT:w" s="T504.tx-MaPX.1">Oːzʼera</ts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1584" n="HIAT:w" s="T505">di͡en</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1587" n="HIAT:w" s="T506">pasʼolakka</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1590" n="HIAT:w" s="T507">olorbuta</ts>
                  <nts id="Seg_1591" n="HIAT:ip">,</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1594" n="HIAT:w" s="T508">onno</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1597" n="HIAT:w" s="T509">da</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1600" n="HIAT:w" s="T510">ölbüte</ts>
                  <nts id="Seg_1601" n="HIAT:ip">.</nts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-MaPX">
            <ts e="T15" id="Seg_1603" n="sc" s="T0">
               <ts e="T1" id="Seg_1605" n="e" s="T0">Min </ts>
               <ts e="T2" id="Seg_1607" n="e" s="T1">Krasnajarskaj </ts>
               <ts e="T3" id="Seg_1609" n="e" s="T2">pʼedagagʼičʼeskaj </ts>
               <ts e="T4" id="Seg_1611" n="e" s="T3">instʼitutɨ </ts>
               <ts e="T5" id="Seg_1613" n="e" s="T4">büppütüm. </ts>
               <ts e="T6" id="Seg_1615" n="e" s="T5">Istorʼijaːnɨ </ts>
               <ts e="T7" id="Seg_1617" n="e" s="T6">školaga </ts>
               <ts e="T8" id="Seg_1619" n="e" s="T7">ogoloru </ts>
               <ts e="T9" id="Seg_1621" n="e" s="T8">ü͡örete </ts>
               <ts e="T10" id="Seg_1623" n="e" s="T9">hɨldʼɨbɨtɨm </ts>
               <ts e="T11" id="Seg_1625" n="e" s="T10">istorʼija </ts>
               <ts e="T12" id="Seg_1627" n="e" s="T11">i </ts>
               <ts e="T13" id="Seg_1629" n="e" s="T12">abšʼestvaznanʼije </ts>
               <ts e="T14" id="Seg_1631" n="e" s="T13">di͡en </ts>
               <ts e="T15" id="Seg_1633" n="e" s="T14">prʼedmʼetteri. </ts>
            </ts>
            <ts e="T149" id="Seg_1634" n="sc" s="T39">
               <ts e="T40" id="Seg_1636" n="e" s="T39">U͡on </ts>
               <ts e="T41" id="Seg_1638" n="e" s="T40">hette </ts>
               <ts e="T42" id="Seg_1640" n="e" s="T41">bötürü͡öpteːkke </ts>
               <ts e="T43" id="Seg_1642" n="e" s="T42">haka </ts>
               <ts e="T44" id="Seg_1644" n="e" s="T43">hiritten, </ts>
               <ts e="T45" id="Seg_1646" n="e" s="T44">Lʼena, </ts>
               <ts e="T46" id="Seg_1648" n="e" s="T45">orto </ts>
               <ts e="T47" id="Seg_1650" n="e" s="T46">Lʼenattan, </ts>
               <ts e="T48" id="Seg_1652" n="e" s="T47">(u͡on </ts>
               <ts e="T50" id="Seg_1654" n="e" s="T48">het-) </ts>
               <ts e="T51" id="Seg_1656" n="e" s="T50">kelbitter </ts>
               <ts e="T52" id="Seg_1658" n="e" s="T51">bastɨkɨː </ts>
               <ts e="T53" id="Seg_1660" n="e" s="T52">hakalar. </ts>
               <ts e="T54" id="Seg_1662" n="e" s="T53">Nʼuːčča </ts>
               <ts e="T55" id="Seg_1664" n="e" s="T54">hirdemmitiger </ts>
               <ts e="T56" id="Seg_1666" n="e" s="T55">Lʼenaga </ts>
               <ts e="T57" id="Seg_1668" n="e" s="T56">epiʼdʼeːmʼijeler </ts>
               <ts e="T58" id="Seg_1670" n="e" s="T57">bu͡olbuttara, </ts>
               <ts e="T59" id="Seg_1672" n="e" s="T58">u͡ospa, </ts>
               <ts e="T60" id="Seg_1674" n="e" s="T59">čʼuma, </ts>
               <ts e="T61" id="Seg_1676" n="e" s="T60">sʼibʼirskaj jazva </ts>
               <ts e="T62" id="Seg_1678" n="e" s="T61">di͡en, </ts>
               <ts e="T63" id="Seg_1680" n="e" s="T62">kihiler, </ts>
               <ts e="T64" id="Seg_1682" n="e" s="T63">dʼizʼentʼerʼija. </ts>
               <ts e="T65" id="Seg_1684" n="e" s="T64">Onno </ts>
               <ts e="T66" id="Seg_1686" n="e" s="T65">tɨːnnaːk </ts>
               <ts e="T67" id="Seg_1688" n="e" s="T66">kaːlbɨt </ts>
               <ts e="T68" id="Seg_1690" n="e" s="T67">kihileriŋ, </ts>
               <ts e="T69" id="Seg_1692" n="e" s="T68">kotuː </ts>
               <ts e="T70" id="Seg_1694" n="e" s="T69">hir </ts>
               <ts e="T71" id="Seg_1696" n="e" s="T70">di͡ek, </ts>
               <ts e="T72" id="Seg_1698" n="e" s="T71">eː, </ts>
               <ts e="T73" id="Seg_1700" n="e" s="T72">baːj, </ts>
               <ts e="T74" id="Seg_1702" n="e" s="T73">ki͡eŋ </ts>
               <ts e="T75" id="Seg_1704" n="e" s="T74">kü͡öl </ts>
               <ts e="T76" id="Seg_1706" n="e" s="T75">baːr </ts>
               <ts e="T77" id="Seg_1708" n="e" s="T76">di͡en </ts>
               <ts e="T78" id="Seg_1710" n="e" s="T77">hurakka </ts>
               <ts e="T79" id="Seg_1712" n="e" s="T78">kelbitter. </ts>
               <ts e="T80" id="Seg_1714" n="e" s="T79">Onon </ts>
               <ts e="T81" id="Seg_1716" n="e" s="T80">Dʼehi͡ej </ts>
               <ts e="T82" id="Seg_1718" n="e" s="T81">ebe </ts>
               <ts e="T83" id="Seg_1720" n="e" s="T82">kɨtɨlɨgar </ts>
               <ts e="T84" id="Seg_1722" n="e" s="T83">oloron, </ts>
               <ts e="T85" id="Seg_1724" n="e" s="T84">ol </ts>
               <ts e="T86" id="Seg_1726" n="e" s="T85">kelelleriger, </ts>
               <ts e="T87" id="Seg_1728" n="e" s="T86">Dʼehi͡ej </ts>
               <ts e="T88" id="Seg_1730" n="e" s="T87">kü͡ölüŋ </ts>
               <ts e="T89" id="Seg_1732" n="e" s="T88">kɨtɨllara </ts>
               <ts e="T90" id="Seg_1734" n="e" s="T89">iččitek, </ts>
               <ts e="T91" id="Seg_1736" n="e" s="T90">ühü, </ts>
               <ts e="T92" id="Seg_1738" n="e" s="T91">kihi </ts>
               <ts e="T93" id="Seg_1740" n="e" s="T92">hu͡ok. </ts>
               <ts e="T94" id="Seg_1742" n="e" s="T93">Urahalar </ts>
               <ts e="T95" id="Seg_1744" n="e" s="T94">ire </ts>
               <ts e="T96" id="Seg_1746" n="e" s="T95">(turbut) </ts>
               <ts e="T97" id="Seg_1748" n="e" s="T96">turallar, </ts>
               <ts e="T98" id="Seg_1750" n="e" s="T97">dʼi͡eleriŋ </ts>
               <ts e="T99" id="Seg_1752" n="e" s="T98">tu͡ostara </ts>
               <ts e="T100" id="Seg_1754" n="e" s="T99">hɨtɨjbɨttar. </ts>
               <ts e="T101" id="Seg_1756" n="e" s="T100">Kihiler </ts>
               <ts e="T102" id="Seg_1758" n="e" s="T101">uŋu͡oktara, </ts>
               <ts e="T103" id="Seg_1760" n="e" s="T102">tabalar, </ts>
               <ts e="T104" id="Seg_1762" n="e" s="T103">ɨttar </ts>
               <ts e="T105" id="Seg_1764" n="e" s="T104">uŋu͡oktara </ts>
               <ts e="T106" id="Seg_1766" n="e" s="T105">hɨtallar, </ts>
               <ts e="T107" id="Seg_1768" n="e" s="T106">ühü, </ts>
               <ts e="T108" id="Seg_1770" n="e" s="T107">tögürüččü </ts>
               <ts e="T109" id="Seg_1772" n="e" s="T108">Dʼeheji. </ts>
               <ts e="T110" id="Seg_1774" n="e" s="T109">Onno </ts>
               <ts e="T111" id="Seg_1776" n="e" s="T110">u͡ospattan </ts>
               <ts e="T112" id="Seg_1778" n="e" s="T111">ölbütter </ts>
               <ts e="T113" id="Seg_1780" n="e" s="T112">toŋustar, </ts>
               <ts e="T114" id="Seg_1782" n="e" s="T113">dʼuraːktar, </ts>
               <ts e="T115" id="Seg_1784" n="e" s="T114">samajedɨ, </ts>
               <ts e="T116" id="Seg_1786" n="e" s="T115">plʼemʼena </ts>
               <ts e="T117" id="Seg_1788" n="e" s="T116">tam </ts>
               <ts e="T118" id="Seg_1790" n="e" s="T117">žɨlʼi. </ts>
               <ts e="T119" id="Seg_1792" n="e" s="T118">Oloru, </ts>
               <ts e="T120" id="Seg_1794" n="e" s="T119">ojun </ts>
               <ts e="T121" id="Seg_1796" n="e" s="T120">kɨraːbɨt </ts>
               <ts e="T122" id="Seg_1798" n="e" s="T121">hire </ts>
               <ts e="T123" id="Seg_1800" n="e" s="T122">di͡en </ts>
               <ts e="T124" id="Seg_1802" n="e" s="T123">küreteletteːbitter. </ts>
               <ts e="T125" id="Seg_1804" n="e" s="T124">Onu, </ts>
               <ts e="T126" id="Seg_1806" n="e" s="T125">ol </ts>
               <ts e="T127" id="Seg_1808" n="e" s="T126">iččitek </ts>
               <ts e="T128" id="Seg_1810" n="e" s="T127">hirge </ts>
               <ts e="T129" id="Seg_1812" n="e" s="T128">kelenner, </ts>
               <ts e="T130" id="Seg_1814" n="e" s="T129">hakalarɨŋ </ts>
               <ts e="T131" id="Seg_1816" n="e" s="T130">törüt </ts>
               <ts e="T132" id="Seg_1818" n="e" s="T131">hir </ts>
               <ts e="T133" id="Seg_1820" n="e" s="T132">oŋostubuttar </ts>
               <ts e="T134" id="Seg_1822" n="e" s="T133">Dʼehi͡eji. </ts>
               <ts e="T135" id="Seg_1824" n="e" s="T134">Onton </ts>
               <ts e="T136" id="Seg_1826" n="e" s="T135">gdʼe_tа </ts>
               <ts e="T137" id="Seg_1828" n="e" s="T136">u͡on </ts>
               <ts e="T138" id="Seg_1830" n="e" s="T137">togus </ts>
               <ts e="T139" id="Seg_1832" n="e" s="T138">bötürü͡ökke </ts>
               <ts e="T140" id="Seg_1834" n="e" s="T139">Tajmɨr </ts>
               <ts e="T141" id="Seg_1836" n="e" s="T140">di͡ek, </ts>
               <ts e="T142" id="Seg_1838" n="e" s="T141">horok </ts>
               <ts e="T143" id="Seg_1840" n="e" s="T142">kihiler </ts>
               <ts e="T144" id="Seg_1842" n="e" s="T143">Tajmɨr </ts>
               <ts e="T145" id="Seg_1844" n="e" s="T144">di͡ek </ts>
               <ts e="T146" id="Seg_1846" n="e" s="T145">uže </ts>
               <ts e="T147" id="Seg_1848" n="e" s="T146">köhö </ts>
               <ts e="T148" id="Seg_1850" n="e" s="T147">hɨldʼar </ts>
               <ts e="T149" id="Seg_1852" n="e" s="T148">bu͡olbuttar. </ts>
            </ts>
            <ts e="T170" id="Seg_1853" n="sc" s="T151">
               <ts e="T153" id="Seg_1855" n="e" s="T151">(Tabalannan), </ts>
               <ts e="T154" id="Seg_1857" n="e" s="T153">tabannan, </ts>
               <ts e="T155" id="Seg_1859" n="e" s="T154">tabannan. </ts>
               <ts e="T156" id="Seg_1861" n="e" s="T155">Uže </ts>
               <ts e="T157" id="Seg_1863" n="e" s="T156">ol </ts>
               <ts e="T158" id="Seg_1865" n="e" s="T157">tabanɨ, </ts>
               <ts e="T159" id="Seg_1867" n="e" s="T158">uže </ts>
               <ts e="T160" id="Seg_1869" n="e" s="T159">iːter </ts>
               <ts e="T161" id="Seg_1871" n="e" s="T160">kihi </ts>
               <ts e="T162" id="Seg_1873" n="e" s="T161">bu͡olbuttar </ts>
               <ts e="T163" id="Seg_1875" n="e" s="T162">bu͡o. </ts>
               <ts e="T164" id="Seg_1877" n="e" s="T163">Mm, </ts>
               <ts e="T165" id="Seg_1879" n="e" s="T164">bettek </ts>
               <ts e="T166" id="Seg_1881" n="e" s="T165">kotuː </ts>
               <ts e="T167" id="Seg_1883" n="e" s="T166">hir </ts>
               <ts e="T168" id="Seg_1885" n="e" s="T167">di͡ek </ts>
               <ts e="T169" id="Seg_1887" n="e" s="T168">kelen </ts>
               <ts e="T170" id="Seg_1889" n="e" s="T169">baraːnnar. </ts>
            </ts>
            <ts e="T272" id="Seg_1890" n="sc" s="T181">
               <ts e="T182" id="Seg_1892" n="e" s="T181">Dʼehi͡ejderiŋ </ts>
               <ts e="T183" id="Seg_1894" n="e" s="T182">eː </ts>
               <ts e="T184" id="Seg_1896" n="e" s="T183">barɨlara </ts>
               <ts e="T185" id="Seg_1898" n="e" s="T184">da </ts>
               <ts e="T186" id="Seg_1900" n="e" s="T185">bu͡olbattar </ts>
               <ts e="T187" id="Seg_1902" n="e" s="T186">baːj </ts>
               <ts e="T188" id="Seg_1904" n="e" s="T187">kihiler, </ts>
               <ts e="T189" id="Seg_1906" n="e" s="T188">baːj </ts>
               <ts e="T190" id="Seg_1908" n="e" s="T189">ɨ͡allar. </ts>
               <ts e="T191" id="Seg_1910" n="e" s="T190">Hebi͡eskej </ts>
               <ts e="T192" id="Seg_1912" n="e" s="T191">bɨlaːs </ts>
               <ts e="T193" id="Seg_1914" n="e" s="T192">turarɨgar, </ts>
               <ts e="T194" id="Seg_1916" n="e" s="T193">hebi͡eskej </ts>
               <ts e="T195" id="Seg_1918" n="e" s="T194">bɨlaːstan </ts>
               <ts e="T196" id="Seg_1920" n="e" s="T195">küreːnner, </ts>
               <ts e="T197" id="Seg_1922" n="e" s="T196">v asnavnom, </ts>
               <ts e="T198" id="Seg_1924" n="e" s="T197">urut </ts>
               <ts e="T199" id="Seg_1926" n="e" s="T198">otto </ts>
               <ts e="T201" id="Seg_1928" n="e" s="T199">taːk… </ts>
               <ts e="T202" id="Seg_1930" n="e" s="T201">Atɨː </ts>
               <ts e="T203" id="Seg_1932" n="e" s="T202">hɨldʼallar </ts>
               <ts e="T204" id="Seg_1934" n="e" s="T203">bu͡o, </ts>
               <ts e="T205" id="Seg_1936" n="e" s="T204">ɨ͡allana </ts>
               <ts e="T206" id="Seg_1938" n="e" s="T205">hɨldʼallar, </ts>
               <ts e="T207" id="Seg_1940" n="e" s="T206">horoktoro </ts>
               <ts e="T208" id="Seg_1942" n="e" s="T207">kaːlallar </ts>
               <ts e="T209" id="Seg_1944" n="e" s="T208">eː </ts>
               <ts e="T210" id="Seg_1946" n="e" s="T209">mannaːgɨ </ts>
               <ts e="T211" id="Seg_1948" n="e" s="T210">kihi, </ts>
               <ts e="T212" id="Seg_1950" n="e" s="T211">Tajmu͡orga </ts>
               <ts e="T213" id="Seg_1952" n="e" s="T212">baːr </ts>
               <ts e="T214" id="Seg_1954" n="e" s="T213">kergen </ts>
               <ts e="T215" id="Seg_1956" n="e" s="T214">bulunannar </ts>
               <ts e="T216" id="Seg_1958" n="e" s="T215">eŋin </ts>
               <ts e="T217" id="Seg_1960" n="e" s="T216">kaːlallar. </ts>
               <ts e="T218" id="Seg_1962" n="e" s="T217">Aː, </ts>
               <ts e="T219" id="Seg_1964" n="e" s="T218">no, </ts>
               <ts e="T220" id="Seg_1966" n="e" s="T219">savʼetskaj </ts>
               <ts e="T221" id="Seg_1968" n="e" s="T220">bɨlaːs </ts>
               <ts e="T222" id="Seg_1970" n="e" s="T221">turu͡or </ts>
               <ts e="T223" id="Seg_1972" n="e" s="T222">di͡eri </ts>
               <ts e="T224" id="Seg_1974" n="e" s="T223">Dʼehi͡ej </ts>
               <ts e="T225" id="Seg_1976" n="e" s="T224">hakalara </ts>
               <ts e="T226" id="Seg_1978" n="e" s="T225">biːr </ts>
               <ts e="T227" id="Seg_1980" n="e" s="T226">hirge </ts>
               <ts e="T228" id="Seg_1982" n="e" s="T227">oloror </ts>
               <ts e="T229" id="Seg_1984" n="e" s="T228">etiler. </ts>
               <ts e="T230" id="Seg_1986" n="e" s="T229">Onton </ts>
               <ts e="T231" id="Seg_1988" n="e" s="T230">savʼetskaj </ts>
               <ts e="T232" id="Seg_1990" n="e" s="T231">vlaːhɨttan </ts>
               <ts e="T233" id="Seg_1992" n="e" s="T232">küreːn, </ts>
               <ts e="T234" id="Seg_1994" n="e" s="T233">v asnavnom </ts>
               <ts e="T235" id="Seg_1996" n="e" s="T234">baːj </ts>
               <ts e="T236" id="Seg_1998" n="e" s="T235">ɨ͡allar </ts>
               <ts e="T237" id="Seg_2000" n="e" s="T236">bettek </ts>
               <ts e="T238" id="Seg_2002" n="e" s="T237">keletteːbitter, </ts>
               <ts e="T239" id="Seg_2004" n="e" s="T238">munna, </ts>
               <ts e="T240" id="Seg_2006" n="e" s="T239">Tajmu͡orga </ts>
               <ts e="T241" id="Seg_2008" n="e" s="T240">baːr </ts>
               <ts e="T242" id="Seg_2010" n="e" s="T241">uruːlarɨgar </ts>
               <ts e="T243" id="Seg_2012" n="e" s="T242">čugas. </ts>
               <ts e="T244" id="Seg_2014" n="e" s="T243">Könö </ts>
               <ts e="T245" id="Seg_2016" n="e" s="T244">biler </ts>
               <ts e="T246" id="Seg_2018" n="e" s="T245">hirderiger </ts>
               <ts e="T247" id="Seg_2020" n="e" s="T246">eː </ts>
               <ts e="T248" id="Seg_2022" n="e" s="T247">kelbitter </ts>
               <ts e="T249" id="Seg_2024" n="e" s="T248">köhöttöːn. </ts>
               <ts e="T250" id="Seg_2026" n="e" s="T249">Onton </ts>
               <ts e="T251" id="Seg_2028" n="e" s="T250">hüːrbe </ts>
               <ts e="T252" id="Seg_2030" n="e" s="T251">dʼɨllar </ts>
               <ts e="T253" id="Seg_2032" n="e" s="T252">bütelleriger, </ts>
               <ts e="T254" id="Seg_2034" n="e" s="T253">otut </ts>
               <ts e="T255" id="Seg_2036" n="e" s="T254">dʼɨllar </ts>
               <ts e="T256" id="Seg_2038" n="e" s="T255">taksallarɨgar </ts>
               <ts e="T257" id="Seg_2040" n="e" s="T256">Tajmu͡orga </ts>
               <ts e="T258" id="Seg_2042" n="e" s="T257">emi͡e </ts>
               <ts e="T259" id="Seg_2044" n="e" s="T258">savʼetskaj </ts>
               <ts e="T260" id="Seg_2046" n="e" s="T259">bɨlaːha </ts>
               <ts e="T261" id="Seg_2048" n="e" s="T260">palʼitʼikata </ts>
               <ts e="T262" id="Seg_2050" n="e" s="T261">kɨtaːtan </ts>
               <ts e="T263" id="Seg_2052" n="e" s="T262">ispit. </ts>
               <ts e="T264" id="Seg_2054" n="e" s="T263">Emi͡e </ts>
               <ts e="T265" id="Seg_2056" n="e" s="T264">dʼe </ts>
               <ts e="T266" id="Seg_2058" n="e" s="T265">baːj </ts>
               <ts e="T267" id="Seg_2060" n="e" s="T266">kihileri </ts>
               <ts e="T268" id="Seg_2062" n="e" s="T267">baːjdarɨn </ts>
               <ts e="T269" id="Seg_2064" n="e" s="T268">talaːn </ts>
               <ts e="T270" id="Seg_2066" n="e" s="T269">üllesti͡ekke </ts>
               <ts e="T271" id="Seg_2068" n="e" s="T270">di͡en </ts>
               <ts e="T272" id="Seg_2070" n="e" s="T271">bu͡olbut. </ts>
            </ts>
            <ts e="T331" id="Seg_2071" n="sc" s="T276">
               <ts e="T277" id="Seg_2073" n="e" s="T276">Tabalarɨn </ts>
               <ts e="T278" id="Seg_2075" n="e" s="T277">bɨldʼaːn. </ts>
               <ts e="T279" id="Seg_2077" n="e" s="T278">Onton </ts>
               <ts e="T280" id="Seg_2079" n="e" s="T279">tabalarɨn </ts>
               <ts e="T281" id="Seg_2081" n="e" s="T280">ere </ts>
               <ts e="T282" id="Seg_2083" n="e" s="T281">bu͡olbat, </ts>
               <ts e="T283" id="Seg_2085" n="e" s="T282">dʼi͡e </ts>
               <ts e="T284" id="Seg_2087" n="e" s="T283">üp </ts>
               <ts e="T285" id="Seg_2089" n="e" s="T284">tüːnün </ts>
               <ts e="T286" id="Seg_2091" n="e" s="T285">(talɨːllar) </ts>
               <ts e="T287" id="Seg_2093" n="e" s="T286">bu͡o. </ts>
               <ts e="T288" id="Seg_2095" n="e" s="T287">Onu </ts>
               <ts e="T289" id="Seg_2097" n="e" s="T288">barɨtɨn </ts>
               <ts e="T290" id="Seg_2099" n="e" s="T289">tügeti͡ekke </ts>
               <ts e="T291" id="Seg_2101" n="e" s="T290">naːda </ts>
               <ts e="T292" id="Seg_2103" n="e" s="T291">di͡en </ts>
               <ts e="T293" id="Seg_2105" n="e" s="T292">dʼadaŋɨlarga. </ts>
               <ts e="T294" id="Seg_2107" n="e" s="T293">Urut </ts>
               <ts e="T295" id="Seg_2109" n="e" s="T294">daː </ts>
               <ts e="T296" id="Seg_2111" n="e" s="T295">otto </ts>
               <ts e="T297" id="Seg_2113" n="e" s="T296">baːj </ts>
               <ts e="T298" id="Seg_2115" n="e" s="T297">kihiŋ </ts>
               <ts e="T299" id="Seg_2117" n="e" s="T298">bɨlɨrgɨ </ts>
               <ts e="T300" id="Seg_2119" n="e" s="T299">haku͡onɨnan </ts>
               <ts e="T301" id="Seg_2121" n="e" s="T300">kimi </ts>
               <ts e="T302" id="Seg_2123" n="e" s="T301">da </ts>
               <ts e="T303" id="Seg_2125" n="e" s="T302">atagastaːbakka, </ts>
               <ts e="T304" id="Seg_2127" n="e" s="T303">ülehitin </ts>
               <ts e="T305" id="Seg_2129" n="e" s="T304">da </ts>
               <ts e="T306" id="Seg_2131" n="e" s="T305">atagastaːbakka, </ts>
               <ts e="T307" id="Seg_2133" n="e" s="T306">bejetin </ts>
               <ts e="T308" id="Seg_2135" n="e" s="T307">uruːtun </ts>
               <ts e="T309" id="Seg_2137" n="e" s="T308">da </ts>
               <ts e="T311" id="Seg_2139" n="e" s="T309">(atagas-), </ts>
               <ts e="T312" id="Seg_2141" n="e" s="T311">dʼadaŋɨ </ts>
               <ts e="T313" id="Seg_2143" n="e" s="T312">uruːtun </ts>
               <ts e="T314" id="Seg_2145" n="e" s="T313">da </ts>
               <ts e="T315" id="Seg_2147" n="e" s="T314">atagastaːbakka, </ts>
               <ts e="T316" id="Seg_2149" n="e" s="T315">olorbuttar </ts>
               <ts e="T317" id="Seg_2151" n="e" s="T316">bu͡o, </ts>
               <ts e="T318" id="Seg_2153" n="e" s="T317">abɨčajdara </ts>
               <ts e="T319" id="Seg_2155" n="e" s="T318">onnuk. </ts>
               <ts e="T320" id="Seg_2157" n="e" s="T319">Kömölöhön </ts>
               <ts e="T321" id="Seg_2159" n="e" s="T320">olorbuttar </ts>
               <ts e="T322" id="Seg_2161" n="e" s="T321">bu͡o. </ts>
               <ts e="T323" id="Seg_2163" n="e" s="T322">Savʼetskaj </ts>
               <ts e="T324" id="Seg_2165" n="e" s="T323">bɨlaːs </ts>
               <ts e="T325" id="Seg_2167" n="e" s="T324">palʼitʼikatɨn </ts>
               <ts e="T326" id="Seg_2169" n="e" s="T325">gɨtta </ts>
               <ts e="T327" id="Seg_2171" n="e" s="T326">höbülespetek </ts>
               <ts e="T328" id="Seg_2173" n="e" s="T327">kihileriŋ </ts>
               <ts e="T329" id="Seg_2175" n="e" s="T328">manna </ts>
               <ts e="T330" id="Seg_2177" n="e" s="T329">bunt </ts>
               <ts e="T331" id="Seg_2179" n="e" s="T330">tahaːrbɨttara. </ts>
            </ts>
            <ts e="T389" id="Seg_2180" n="sc" s="T334">
               <ts e="T335" id="Seg_2182" n="e" s="T334">Heː, </ts>
               <ts e="T336" id="Seg_2184" n="e" s="T335">aːjdahan </ts>
               <ts e="T337" id="Seg_2186" n="e" s="T336">turbut. </ts>
               <ts e="T338" id="Seg_2188" n="e" s="T337">Dʼe </ts>
               <ts e="T339" id="Seg_2190" n="e" s="T338">onno </ts>
               <ts e="T340" id="Seg_2192" n="e" s="T339">ol </ts>
               <ts e="T341" id="Seg_2194" n="e" s="T340">ulakan </ts>
               <ts e="T342" id="Seg_2196" n="e" s="T341">ajdaːŋŋa </ts>
               <ts e="T343" id="Seg_2198" n="e" s="T342">kihini </ts>
               <ts e="T344" id="Seg_2200" n="e" s="T343">eŋin </ts>
               <ts e="T345" id="Seg_2202" n="e" s="T344">ölörbütter – </ts>
               <ts e="T346" id="Seg_2204" n="e" s="T345">manna </ts>
               <ts e="T347" id="Seg_2206" n="e" s="T346">tu͡oktarɨ, </ts>
               <ts e="T348" id="Seg_2208" n="e" s="T347">upalnamočennɨjdarɨ. </ts>
               <ts e="T350" id="Seg_2210" n="e" s="T348">(Olu-) </ts>
               <ts e="T351" id="Seg_2212" n="e" s="T350">tʼe </ts>
               <ts e="T352" id="Seg_2214" n="e" s="T351">ol </ts>
               <ts e="T353" id="Seg_2216" n="e" s="T352">tɨ͡a </ts>
               <ts e="T354" id="Seg_2218" n="e" s="T353">kihite </ts>
               <ts e="T355" id="Seg_2220" n="e" s="T354">ölörbüte </ts>
               <ts e="T356" id="Seg_2222" n="e" s="T355">billibet, </ts>
               <ts e="T357" id="Seg_2224" n="e" s="T356">ili </ts>
               <ts e="T358" id="Seg_2226" n="e" s="T357">že </ts>
               <ts e="T359" id="Seg_2228" n="e" s="T358">atɨn </ts>
               <ts e="T360" id="Seg_2230" n="e" s="T359">tu͡ok </ts>
               <ts e="T361" id="Seg_2232" n="e" s="T360">eme </ts>
               <ts e="T362" id="Seg_2234" n="e" s="T361">kihite </ts>
               <ts e="T363" id="Seg_2236" n="e" s="T362">anaːn, </ts>
               <ts e="T364" id="Seg_2238" n="e" s="T363">onnuk </ts>
               <ts e="T365" id="Seg_2240" n="e" s="T364">aːjdaːnɨ </ts>
               <ts e="T366" id="Seg_2242" n="e" s="T365">tahaːraːrɨ </ts>
               <ts e="T367" id="Seg_2244" n="e" s="T366">ölörbüte, </ts>
               <ts e="T368" id="Seg_2246" n="e" s="T367">billibet. </ts>
               <ts e="T369" id="Seg_2248" n="e" s="T368">Anɨga </ts>
               <ts e="T370" id="Seg_2250" n="e" s="T369">daː </ts>
               <ts e="T371" id="Seg_2252" n="e" s="T370">di͡eri. </ts>
               <ts e="T372" id="Seg_2254" n="e" s="T371">Onno </ts>
               <ts e="T373" id="Seg_2256" n="e" s="T372">aktʼivnaj, </ts>
               <ts e="T374" id="Seg_2258" n="e" s="T373">dʼe </ts>
               <ts e="T375" id="Seg_2260" n="e" s="T374">otto, </ts>
               <ts e="T376" id="Seg_2262" n="e" s="T375">aristuːjdaːbɨttar </ts>
               <ts e="T377" id="Seg_2264" n="e" s="T376">bu͡o </ts>
               <ts e="T379" id="Seg_2266" n="e" s="T377">(aːrʼes-) </ts>
               <ts e="T381" id="Seg_2268" n="e" s="T379">(a-) </ts>
               <ts e="T382" id="Seg_2270" n="e" s="T381">aːrʼestɨ. </ts>
               <ts e="T383" id="Seg_2272" n="e" s="T382">"Atrʼad </ts>
               <ts e="T384" id="Seg_2274" n="e" s="T383">čonovcev" </ts>
               <ts e="T385" id="Seg_2276" n="e" s="T384">di͡en </ts>
               <ts e="T386" id="Seg_2278" n="e" s="T385">kihi </ts>
               <ts e="T387" id="Seg_2280" n="e" s="T386">kelbitter </ts>
               <ts e="T388" id="Seg_2282" n="e" s="T387">Xaːtangattan, </ts>
               <ts e="T389" id="Seg_2284" n="e" s="T388">Jenʼisʼejskʼejten. </ts>
            </ts>
            <ts e="T428" id="Seg_2285" n="sc" s="T391">
               <ts e="T392" id="Seg_2287" n="e" s="T391">Nʼuːččalar, </ts>
               <ts e="T393" id="Seg_2289" n="e" s="T392">heː, </ts>
               <ts e="T394" id="Seg_2291" n="e" s="T393">haːlaːk. </ts>
               <ts e="T395" id="Seg_2293" n="e" s="T394">Dʼe, </ts>
               <ts e="T396" id="Seg_2295" n="e" s="T395">(on-) </ts>
               <ts e="T397" id="Seg_2297" n="e" s="T396">ol </ts>
               <ts e="T398" id="Seg_2299" n="e" s="T397">huraktan, </ts>
               <ts e="T399" id="Seg_2301" n="e" s="T398">kihileri </ts>
               <ts e="T400" id="Seg_2303" n="e" s="T399">kaːjɨːga </ts>
               <ts e="T401" id="Seg_2305" n="e" s="T400">olordollor, </ts>
               <ts e="T402" id="Seg_2307" n="e" s="T401">ühü, </ts>
               <ts e="T403" id="Seg_2309" n="e" s="T402">di͡en </ts>
               <ts e="T404" id="Seg_2311" n="e" s="T403">huraktan </ts>
               <ts e="T405" id="Seg_2313" n="e" s="T404">min </ts>
               <ts e="T406" id="Seg_2315" n="e" s="T405">inʼem, </ts>
               <ts e="T407" id="Seg_2317" n="e" s="T406">agam, </ts>
               <ts e="T408" id="Seg_2319" n="e" s="T407">ehem </ts>
               <ts e="T409" id="Seg_2321" n="e" s="T408">bu͡olannar </ts>
               <ts e="T410" id="Seg_2323" n="e" s="T409">dʼe </ts>
               <ts e="T411" id="Seg_2325" n="e" s="T410">kürüːrge </ts>
               <ts e="T412" id="Seg_2327" n="e" s="T411">turbuttar, </ts>
               <ts e="T413" id="Seg_2329" n="e" s="T412">i </ts>
               <ts e="T414" id="Seg_2331" n="e" s="T413">töttörü </ts>
               <ts e="T415" id="Seg_2333" n="e" s="T414">Dʼehi͡ejderiger </ts>
               <ts e="T416" id="Seg_2335" n="e" s="T415">tönnübütter. </ts>
               <ts e="T417" id="Seg_2337" n="e" s="T416">Agam </ts>
               <ts e="T418" id="Seg_2339" n="e" s="T417">braːta, </ts>
               <ts e="T419" id="Seg_2341" n="e" s="T418">Ignatʼij </ts>
               <ts e="T420" id="Seg_2343" n="e" s="T419">di͡en </ts>
               <ts e="T421" id="Seg_2345" n="e" s="T420">kihini </ts>
               <ts e="T422" id="Seg_2347" n="e" s="T421">ɨlbɨttar, </ts>
               <ts e="T423" id="Seg_2349" n="e" s="T422">kaːjɨːga </ts>
               <ts e="T424" id="Seg_2351" n="e" s="T423">olordon </ts>
               <ts e="T425" id="Seg_2353" n="e" s="T424">baraːn </ts>
               <ts e="T426" id="Seg_2355" n="e" s="T425">(ɨppɨt-) </ts>
               <ts e="T427" id="Seg_2357" n="e" s="T426">ölörbütter </ts>
               <ts e="T428" id="Seg_2359" n="e" s="T427">če. </ts>
            </ts>
            <ts e="T475" id="Seg_2360" n="sc" s="T431">
               <ts e="T432" id="Seg_2362" n="e" s="T431">Heː, </ts>
               <ts e="T433" id="Seg_2364" n="e" s="T432">Majmagolar, </ts>
               <ts e="T434" id="Seg_2366" n="e" s="T433">aga. </ts>
               <ts e="T435" id="Seg_2368" n="e" s="T434">Onton </ts>
               <ts e="T436" id="Seg_2370" n="e" s="T435">agam </ts>
               <ts e="T437" id="Seg_2372" n="e" s="T436">kɨra </ts>
               <ts e="T438" id="Seg_2374" n="e" s="T437">bɨraːta, </ts>
               <ts e="T439" id="Seg_2376" n="e" s="T438">Danʼil </ts>
               <ts e="T440" id="Seg_2378" n="e" s="T439">di͡en </ts>
               <ts e="T441" id="Seg_2380" n="e" s="T440">kihi, </ts>
               <ts e="T443" id="Seg_2382" n="e" s="T441">(ɨ͡alla-) </ts>
               <ts e="T444" id="Seg_2384" n="e" s="T443">uruːlarɨgar </ts>
               <ts e="T445" id="Seg_2386" n="e" s="T444">ɨ͡allana </ts>
               <ts e="T446" id="Seg_2388" n="e" s="T445">barbɨt, </ts>
               <ts e="T447" id="Seg_2390" n="e" s="T446">keːhe, </ts>
               <ts e="T448" id="Seg_2392" n="e" s="T447">kürüːr </ts>
               <ts e="T450" id="Seg_2394" n="e" s="T448">(ke-) </ts>
               <ts e="T451" id="Seg_2396" n="e" s="T450">küreːbitter </ts>
               <ts e="T452" id="Seg_2398" n="e" s="T451">bu͡o, </ts>
               <ts e="T453" id="Seg_2400" n="e" s="T452">ol </ts>
               <ts e="T454" id="Seg_2402" n="e" s="T453">ihin </ts>
               <ts e="T455" id="Seg_2404" n="e" s="T454">ontuŋ </ts>
               <ts e="T456" id="Seg_2406" n="e" s="T455">ol </ts>
               <ts e="T457" id="Seg_2408" n="e" s="T456">ogolorun </ts>
               <ts e="T458" id="Seg_2410" n="e" s="T457">daː </ts>
               <ts e="T459" id="Seg_2412" n="e" s="T458">keːhe </ts>
               <ts e="T460" id="Seg_2414" n="e" s="T459">barbɨttar. </ts>
               <ts e="T461" id="Seg_2416" n="e" s="T460">I </ts>
               <ts e="T462" id="Seg_2418" n="e" s="T461">ol </ts>
               <ts e="T463" id="Seg_2420" n="e" s="T462">Danʼil </ts>
               <ts e="T464" id="Seg_2422" n="e" s="T463">bu͡o, </ts>
               <ts e="T465" id="Seg_2424" n="e" s="T464">manna </ts>
               <ts e="T466" id="Seg_2426" n="e" s="T465">kamkɨ </ts>
               <ts e="T467" id="Seg_2428" n="e" s="T466">kɨrdʼɨ͡ar </ts>
               <ts e="T468" id="Seg_2430" n="e" s="T467">di͡eri </ts>
               <ts e="T469" id="Seg_2432" n="e" s="T468">Tajmɨrga </ts>
               <ts e="T470" id="Seg_2434" n="e" s="T469">üleleːn, </ts>
               <ts e="T471" id="Seg_2436" n="e" s="T470">Ordʼen </ts>
               <ts e="T472" id="Seg_2438" n="e" s="T471">Trudavoːva </ts>
               <ts e="T473" id="Seg_2440" n="e" s="T472">Krasnava </ts>
               <ts e="T474" id="Seg_2442" n="e" s="T473">znamʼenʼi </ts>
               <ts e="T475" id="Seg_2444" n="e" s="T474">zarabotal. </ts>
            </ts>
            <ts e="T492" id="Seg_2445" n="sc" s="T478">
               <ts e="T479" id="Seg_2447" n="e" s="T478">Majmago </ts>
               <ts e="T480" id="Seg_2449" n="e" s="T479">Danʼil </ts>
               <ts e="T481" id="Seg_2451" n="e" s="T480">Stʼepanavʼičʼ </ts>
               <ts e="T482" id="Seg_2453" n="e" s="T481">di͡en. </ts>
               <ts e="T483" id="Seg_2455" n="e" s="T482">Ol </ts>
               <ts e="T484" id="Seg_2457" n="e" s="T483">kihi </ts>
               <ts e="T485" id="Seg_2459" n="e" s="T484">taba </ts>
               <ts e="T486" id="Seg_2461" n="e" s="T485">iːtiːtiger </ts>
               <ts e="T487" id="Seg_2463" n="e" s="T486">kamkɨ </ts>
               <ts e="T488" id="Seg_2465" n="e" s="T487">kɨrdʼɨ͡ar </ts>
               <ts e="T489" id="Seg_2467" n="e" s="T488">di͡eri </ts>
               <ts e="T490" id="Seg_2469" n="e" s="T489">üleleːbit </ts>
               <ts e="T491" id="Seg_2471" n="e" s="T490">ogo </ts>
               <ts e="T492" id="Seg_2473" n="e" s="T491">haːhɨttan. </ts>
            </ts>
            <ts e="T511" id="Seg_2474" n="sc" s="T497">
               <ts e="T498" id="Seg_2476" n="e" s="T497">Valačanskaj </ts>
               <ts e="T499" id="Seg_2478" n="e" s="T498">rajon </ts>
               <ts e="T500" id="Seg_2480" n="e" s="T499">Kamʼeŋŋa </ts>
               <ts e="T501" id="Seg_2482" n="e" s="T500">ete. </ts>
               <ts e="T502" id="Seg_2484" n="e" s="T501">Onton </ts>
               <ts e="T503" id="Seg_2486" n="e" s="T502">kennikiː </ts>
               <ts e="T504" id="Seg_2488" n="e" s="T503">dʼɨllarɨn </ts>
               <ts e="T505" id="Seg_2490" n="e" s="T504">Xantajskaj Oːzʼera </ts>
               <ts e="T506" id="Seg_2492" n="e" s="T505">di͡en </ts>
               <ts e="T507" id="Seg_2494" n="e" s="T506">pasʼolakka </ts>
               <ts e="T508" id="Seg_2496" n="e" s="T507">olorbuta, </ts>
               <ts e="T509" id="Seg_2498" n="e" s="T508">onno </ts>
               <ts e="T510" id="Seg_2500" n="e" s="T509">da </ts>
               <ts e="T511" id="Seg_2502" n="e" s="T510">ölbüte. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-MaPX">
            <ta e="T5" id="Seg_2503" s="T0">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.001 (001.001)</ta>
            <ta e="T15" id="Seg_2504" s="T5">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.002 (001.002)</ta>
            <ta e="T53" id="Seg_2505" s="T39">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.003 (001.005)</ta>
            <ta e="T64" id="Seg_2506" s="T53">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.004 (001.006)</ta>
            <ta e="T79" id="Seg_2507" s="T64">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.005 (001.007)</ta>
            <ta e="T93" id="Seg_2508" s="T79">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.006 (001.008)</ta>
            <ta e="T100" id="Seg_2509" s="T93">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.007 (001.009)</ta>
            <ta e="T109" id="Seg_2510" s="T100">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.008 (001.010)</ta>
            <ta e="T118" id="Seg_2511" s="T109">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.009 (001.011)</ta>
            <ta e="T124" id="Seg_2512" s="T118">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.010 (001.012)</ta>
            <ta e="T134" id="Seg_2513" s="T124">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.011 (001.013)</ta>
            <ta e="T149" id="Seg_2514" s="T134">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.012 (001.014)</ta>
            <ta e="T155" id="Seg_2515" s="T151">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.013 (001.016)</ta>
            <ta e="T163" id="Seg_2516" s="T155">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.014 (001.017)</ta>
            <ta e="T170" id="Seg_2517" s="T163">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.015 (001.018)</ta>
            <ta e="T190" id="Seg_2518" s="T181">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.016 (001.020)</ta>
            <ta e="T201" id="Seg_2519" s="T190">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.017 (001.021)</ta>
            <ta e="T217" id="Seg_2520" s="T201">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.018 (001.022)</ta>
            <ta e="T229" id="Seg_2521" s="T217">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.019 (001.023)</ta>
            <ta e="T243" id="Seg_2522" s="T229">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.020 (001.024)</ta>
            <ta e="T249" id="Seg_2523" s="T243">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.021 (001.025)</ta>
            <ta e="T263" id="Seg_2524" s="T249">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.022 (001.026)</ta>
            <ta e="T272" id="Seg_2525" s="T263">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.023 (001.027)</ta>
            <ta e="T278" id="Seg_2526" s="T276">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.024 (001.029)</ta>
            <ta e="T287" id="Seg_2527" s="T278">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.025 (001.030)</ta>
            <ta e="T293" id="Seg_2528" s="T287">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.026 (001.031)</ta>
            <ta e="T319" id="Seg_2529" s="T293">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.027 (001.032)</ta>
            <ta e="T322" id="Seg_2530" s="T319">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.028 (001.033)</ta>
            <ta e="T331" id="Seg_2531" s="T322">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.029 (001.034)</ta>
            <ta e="T337" id="Seg_2532" s="T334">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.030 (001.036)</ta>
            <ta e="T348" id="Seg_2533" s="T337">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.031 (001.037)</ta>
            <ta e="T368" id="Seg_2534" s="T348">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.032 (001.038)</ta>
            <ta e="T371" id="Seg_2535" s="T368">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.033 (001.039)</ta>
            <ta e="T382" id="Seg_2536" s="T371">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.034 (001.040)</ta>
            <ta e="T389" id="Seg_2537" s="T382">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.035 (001.041)</ta>
            <ta e="T394" id="Seg_2538" s="T391">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.036 (001.043)</ta>
            <ta e="T416" id="Seg_2539" s="T394">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.037 (001.044)</ta>
            <ta e="T428" id="Seg_2540" s="T416">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.038 (001.045)</ta>
            <ta e="T434" id="Seg_2541" s="T431">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.039 (001.047)</ta>
            <ta e="T460" id="Seg_2542" s="T434">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.040 (001.048)</ta>
            <ta e="T475" id="Seg_2543" s="T460">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.041 (001.049)</ta>
            <ta e="T482" id="Seg_2544" s="T478">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.042 (001.051)</ta>
            <ta e="T492" id="Seg_2545" s="T482">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.043 (001.052)</ta>
            <ta e="T501" id="Seg_2546" s="T497">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.044 (001.054)</ta>
            <ta e="T511" id="Seg_2547" s="T501">MaPX_KuNS_200X_YakutsOfEssej_conv.MaPX.045 (001.055)</ta>
         </annotation>
         <annotation name="st" tierref="st-MaPX">
            <ta e="T5" id="Seg_2548" s="T0">M: Мин Красноярскай педагогическай институты бүппүтүм.</ta>
            <ta e="T15" id="Seg_2549" s="T5">Историяны школага оголору үөрэтэ һылдьыбытым история и обществознание диэн предметтэри.</ta>
            <ta e="T53" id="Seg_2550" s="T39">М: Уон һэттэ бөтүрүөптээккэ Һака һириттэн, Лена, орто Ленаттан, (уон һэт-) кэлбиттэр бастыкыы һакалар.</ta>
            <ta e="T64" id="Seg_2551" s="T53">Ньуучча һирдэммитигэр Ленага эпидээмийэлэр буолбуттара: уоспа, чума, сибирскай язва диэн, киһилэр, дизентерия. </ta>
            <ta e="T79" id="Seg_2552" s="T64">Онно тыыннаак каалбыт киһилэриӈ, котуу һир диэк, ээ, баай, киэӈ күөл баар диэн һуракка кэлбиттэр.</ta>
            <ta e="T93" id="Seg_2553" s="T79">Онон Дьэһиэй эбэ кытылыгар олорон, ол… кэлэллэригэр, Дьэһиэй күөлүӈ кытыллара иччитэк, үһү, киһи һуок.</ta>
            <ta e="T100" id="Seg_2554" s="T93">Ураһалар ирэ (эрэ) (турбут) тураллар, дьиэлэриӈ туостара һытыйбыттар.</ta>
            <ta e="T109" id="Seg_2555" s="T100">Киһилэр уӈуоктара (оӈуоктара), табалар, ыттaр уӈуоктара (оӈуоктара) һыталлар, үһү, төгүрүччү Дьэһэйи.</ta>
            <ta e="T118" id="Seg_2556" s="T109">Онно уоспаттан өлбүттэр тоӈустар, дьураактар, самоеды (племена там жили).</ta>
            <ta e="T124" id="Seg_2557" s="T118">Олору, ойун кыраабыт һирэ диэн күрэтэлэттээбиттэр.</ta>
            <ta e="T134" id="Seg_2558" s="T124">Ону, ол иччитэк һиргэ кэлэннэр, һакаларыӈ төрүт һир оӈостубуттар Дьэһиэйи. </ta>
            <ta e="T149" id="Seg_2559" s="T134">Онтон где-то уон тогус бөтүрүөпкэ Таймыр диэк, һорок киһилэр Таймыр диэк уже көһө һылдьар буолбуттар. </ta>
            <ta e="T155" id="Seg_2560" s="T151">M: (Табаланнан), табаннан, табаннан.</ta>
            <ta e="T163" id="Seg_2561" s="T155">Уже ол табаны, уже иитэр киһи буолбуттар буо.</ta>
            <ta e="T170" id="Seg_2562" s="T163">Мм… бэттэк котуу һир диэк кэлэн барааннар.</ta>
            <ta e="T190" id="Seg_2563" s="T181">М: Дьэһиэйдэриӈ ээ барылара да буолбаттар баай киһилэр, баай ыаллар.</ta>
            <ta e="T201" id="Seg_2564" s="T190">Һэбиэскэй былаас турарыгар, һэбиэскэй былаастан күрээннэр, в основном, урут отто таак…</ta>
            <ta e="T217" id="Seg_2565" s="T201">Атыы һылдьаллар буо, ыаллана һылдьаллар, һорокторо каалаллар маннаагы киһи… Таймуорга баар кэргэн булунаннан эӈин каалаллар. </ta>
            <ta e="T229" id="Seg_2566" s="T217">Аа, но, советскай былаас туруор диэри Дьэһиэй һакалара биир һиргэ олорор этилэр.</ta>
            <ta e="T243" id="Seg_2567" s="T229">Онтон советскай влааһы..ттан күрээн, в основном баай ыаллар бэттэк кэлэттээбиттэр, мунна, Таймуорга баар урууларыгар чугас.</ta>
            <ta e="T249" id="Seg_2568" s="T243">Көнө билэр һирдэригэр ээ кэлбиттэр көһөттөөн.</ta>
            <ta e="T263" id="Seg_2569" s="T249">Онтон һүүрбэ дьыллар бүтэллэригэр, отут дьыллар таксалларыгар Таймуорга эмиэ советскай былааһа политиката кытаатан испит.</ta>
            <ta e="T272" id="Seg_2570" s="T263">Эмиэ дьэ баай киһилэри баайдарын талаан үллэстиэккэ диэн буолбут.</ta>
            <ta e="T278" id="Seg_2571" s="T276">М: Табаларын былдьаан.</ta>
            <ta e="T287" id="Seg_2572" s="T278">Онтон табаларын эрэ буолбат, дьиэ үптүүнэн талыыллар буо.</ta>
            <ta e="T293" id="Seg_2573" s="T287">Ону барытын түгэтиэккэ наада диэн дьадаӈыларга.</ta>
            <ta e="T319" id="Seg_2574" s="T293">Урут даа отто баай киһиӈ былыргы һакуонынан кими да атагастаабакка: үлэһитин да атагастаабакка, бэйэтин уруутун да (атагас-), дьадаӈӈы уруутун да атагастаабакка, олорбуттар буо, обычайдара оннук.</ta>
            <ta e="T322" id="Seg_2575" s="T319">Көмөлөһөн олорбуттар буо.</ta>
            <ta e="T331" id="Seg_2576" s="T322">Советскай былаас политикатын гытта һөбүлэспэтэк киһилэриӈ манна бунт таһаарбыттарa.</ta>
            <ta e="T337" id="Seg_2577" s="T334">М: Һэ-э, аайдааһан турбут.</ta>
            <ta e="T348" id="Seg_2578" s="T337">Дьэ онно ол улакан айдааӈӈа киһини эӈин өлөрбүттэр – манна туоктары, уполномоченныйдары.</ta>
            <ta e="T368" id="Seg_2579" s="T348">Олу (Ону)… тэ ол тыа киһитэ өлөрбүтэ биллибэт, или же атын туок эмэ киһитэ анаан, оннук аайдааны таһараары өлөрбүтэ, биллибэт.</ta>
            <ta e="T371" id="Seg_2580" s="T368">Аныга даа диэри.</ta>
            <ta e="T382" id="Seg_2581" s="T371">Онно активнай, дьэ отто, аристууйдаабыттар буо (а…) (а…) ааресты (аресты).</ta>
            <ta e="T389" id="Seg_2582" s="T382">"Отряд чоновцев" диэн киһи кэлбиттэр Хатангаттан, Енисейскэйтэн.</ta>
            <ta e="T394" id="Seg_2583" s="T391">М: Ньууччалар, һэ-э һаалаак. </ta>
            <ta e="T416" id="Seg_2584" s="T394">М: Дьэ, (он…) ол һурактан, киһилэри каайыыга олордоллор, үһү, диэн һурактан мин иньэм, агам, эһэм буоланнар дьэ күрүүргэ турбуттар, и төттөрү Дьэһиэйдэригэр төннүбүттэр.</ta>
            <ta e="T428" id="Seg_2585" s="T416">М: Агам брата, Игнатий диэн киһини ылбыттар, каайыыга олордон бараан (ыппыт-), өлөрбүттэр чэ.</ta>
            <ta e="T434" id="Seg_2586" s="T431">М: Һэ-э, Маймогалар, ага.</ta>
            <ta e="T460" id="Seg_2587" s="T434">Онтон агам кыра быраата, Данил диэн киһи, (ыалла…) урууларыгар ыаллана барбыты, кээһэ, күрүүр (кэ…) күрээбиттэр буо, ол иһин онтуӈ оголорун даа кэһэ барбыттар.</ta>
            <ta e="T475" id="Seg_2588" s="T460">И ол Данил буо, манна камкы кырдьыар диэри Таймырга үлэлээн, Орден Трудового Красного знамени заработал.</ta>
            <ta e="T482" id="Seg_2589" s="T478">М: Маймаго Данил Степанович диэн.</ta>
            <ta e="T492" id="Seg_2590" s="T482">Ол киһи таба иитиитигэр камкы кырдьыар диэри үлэлээбит ого һааһыттан.</ta>
            <ta e="T501" id="Seg_2591" s="T497">М: Волочанскай район Каменьӈа этэ.</ta>
            <ta e="T511" id="Seg_2592" s="T501">Онтон кэнникии дьылларын Хантайский Озеро диэн посёлокка олорбута, онно да өлбүтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-MaPX">
            <ta e="T5" id="Seg_2593" s="T0">– Min Krasnajarskaj pʼedagagʼičʼeskaj instʼitutɨ büppütüm. </ta>
            <ta e="T15" id="Seg_2594" s="T5">Istorʼijaːnɨ školaga ogoloru ü͡örete hɨldʼɨbɨtɨm istorʼija i abšʼestvaznanʼije di͡en prʼedmʼetteri. </ta>
            <ta e="T53" id="Seg_2595" s="T39">– U͡on hette bötürü͡öpteːkke haka hiritten, Lʼena, orto Lʼenattan, (u͡on het-) kelbitter bastɨkɨː hakalar. </ta>
            <ta e="T64" id="Seg_2596" s="T53">Nʼuːčča hirdemmitiger Lʼenaga epiʼdʼeːmʼijeler bu͡olbuttara, u͡ospa, čʼuma, sʼibʼirskaj jazva di͡en, kihiler, dʼizʼentʼerʼija. </ta>
            <ta e="T79" id="Seg_2597" s="T64">Onno tɨːnnaːk kaːlbɨt kihileriŋ, kotuː hir di͡ek, eː, baːj, ki͡eŋ kü͡öl baːr di͡en hurakka kelbitter. </ta>
            <ta e="T93" id="Seg_2598" s="T79">Onon Dʼehi͡ej ebe kɨtɨlɨgar oloron, ol kelelleriger, Dʼehi͡ej kü͡ölüŋ kɨtɨllara iččitek, ühü, kihi hu͡ok. </ta>
            <ta e="T100" id="Seg_2599" s="T93">Urahalar ire (turbut) turallar, dʼi͡eleriŋ tu͡ostara hɨtɨjbɨttar. </ta>
            <ta e="T109" id="Seg_2600" s="T100">Kihiler uŋu͡oktara, tabalar, ɨttar uŋu͡oktara hɨtallar, ühü, tögürüččü Dʼeheji. </ta>
            <ta e="T118" id="Seg_2601" s="T109">Onno u͡ospattan ölbütter toŋustar, dʼuraːktar, samajedɨ, plʼemʼena tam žɨlʼi. </ta>
            <ta e="T124" id="Seg_2602" s="T118">Oloru, ojun kɨraːbɨt hire di͡en küreteletteːbitter. </ta>
            <ta e="T134" id="Seg_2603" s="T124">Onu, ol iččitek hirge kelenner, hakalarɨŋ törüt hir oŋostubuttar Dʼehi͡eji. </ta>
            <ta e="T149" id="Seg_2604" s="T134">Onton gdʼe_tа u͡on togus bötürü͡ökke Tajmɨr di͡ek, horok kihiler Tajmɨr di͡ek uže köhö hɨldʼar bu͡olbuttar. </ta>
            <ta e="T155" id="Seg_2605" s="T151">– (Tabalannan), tabannan, tabannan. </ta>
            <ta e="T163" id="Seg_2606" s="T155">Uže ol tabanɨ, uže iːter kihi bu͡olbuttar bu͡o. </ta>
            <ta e="T170" id="Seg_2607" s="T163">Mm, bettek kotuː hir di͡ek kelen baraːnnar. </ta>
            <ta e="T190" id="Seg_2608" s="T181">– Dʼehi͡ejderiŋ eː barɨlara da bu͡olbattar baːj kihiler, baːj ɨ͡allar. </ta>
            <ta e="T201" id="Seg_2609" s="T190">Hebi͡eskej bɨlaːs turarɨgar, hebi͡eskej bɨlaːstan küreːnner, v asnavnom, urut otto taːk… </ta>
            <ta e="T217" id="Seg_2610" s="T201">Atɨː hɨldʼallar bu͡o, ɨ͡allana hɨldʼallar, horoktoro kaːlallar eː mannaːgɨ kihi, Tajmu͡orga baːr kergen bulunannar eŋin kaːlallar. </ta>
            <ta e="T229" id="Seg_2611" s="T217">Aː, no, savʼetskaj bɨlaːs turu͡or di͡eri Dʼehi͡ej hakalara biːr hirge oloror etiler. </ta>
            <ta e="T243" id="Seg_2612" s="T229">Onton savʼetskaj vlaːhɨttan küreːn, v asnavnom baːj ɨ͡allar bettek keletteːbitter, munna, Tajmu͡orga baːr uruːlarɨgar čugas. </ta>
            <ta e="T249" id="Seg_2613" s="T243">Könö biler hirderiger eː kelbitter köhöttöːn. </ta>
            <ta e="T263" id="Seg_2614" s="T249">Onton hüːrbe dʼɨllar bütelleriger, otut dʼɨllar taksallarɨgar Tajmu͡orga emi͡e savʼetskaj bɨlaːha palʼitʼikata kɨtaːtan ispit. </ta>
            <ta e="T272" id="Seg_2615" s="T263">Emi͡e dʼe baːj kihileri baːjdarɨn talaːn üllesti͡ekke di͡en bu͡olbut. </ta>
            <ta e="T278" id="Seg_2616" s="T276">– Tabalarɨn bɨldʼaːn. </ta>
            <ta e="T287" id="Seg_2617" s="T278">Onton tabalarɨn ere bu͡olbat, dʼi͡e üp tüːnün (talɨːllar) bu͡o. </ta>
            <ta e="T293" id="Seg_2618" s="T287">Onu barɨtɨn tügeti͡ekke naːda di͡en dʼadaŋɨlarga. </ta>
            <ta e="T319" id="Seg_2619" s="T293">Urut daː otto baːj kihiŋ bɨlɨrgɨ haku͡onɨnan kimi da atagastaːbakka, ülehitin da atagastaːbakka, bejetin uruːtun da (atagas-), dʼadaŋɨ uruːtun da atagastaːbakka, olorbuttar bu͡o, abɨčajdara onnuk. </ta>
            <ta e="T322" id="Seg_2620" s="T319">Kömölöhön olorbuttar bu͡o. </ta>
            <ta e="T331" id="Seg_2621" s="T322">Savʼetskaj bɨlaːs palʼitʼikatɨn gɨtta höbülespetek kihileriŋ manna bunt tahaːrbɨttara. </ta>
            <ta e="T337" id="Seg_2622" s="T334">– Heː, aːjdahan turbut. </ta>
            <ta e="T348" id="Seg_2623" s="T337">Dʼe onno ol ulakan ajdaːŋŋa kihini eŋin ölörbütter – manna tu͡oktarɨ, upalnamočennɨjdarɨ. </ta>
            <ta e="T368" id="Seg_2624" s="T348">(Olu-) tʼe ol tɨ͡a kihite ölörbüte billibet, ili že atɨn tu͡ok eme kihite anaːn, onnuk aːjdaːnɨ tahaːraːrɨ ölörbüte, billibet. </ta>
            <ta e="T371" id="Seg_2625" s="T368">Anɨga daː di͡eri. </ta>
            <ta e="T382" id="Seg_2626" s="T371">Onno aktʼivnaj, dʼe otto, aristuːjdaːbɨttar bu͡o (aːrʼes-) (a-) aːrʼestɨ. </ta>
            <ta e="T389" id="Seg_2627" s="T382">"Atrʼad čonovcev" di͡en kihi kelbitter Xaːtangattan, Jenʼisʼejskʼejten. </ta>
            <ta e="T394" id="Seg_2628" s="T391">– Nʼuːččalar, heː, haːlaːk. </ta>
            <ta e="T416" id="Seg_2629" s="T394">Dʼe, (on-) ol huraktan, kihileri kaːjɨːga olordollor, ühü, di͡en huraktan min inʼem, agam, ehem bu͡olannar dʼe kürüːrge turbuttar, i töttörü Dʼehi͡ejderiger tönnübütter. </ta>
            <ta e="T428" id="Seg_2630" s="T416">Agam braːta, Ignatʼij di͡en kihini ɨlbɨttar, kaːjɨːga olordon baraːn (ɨppɨt-) ölörbütter če. </ta>
            <ta e="T434" id="Seg_2631" s="T431">– Heː, Majmagolar, aga. </ta>
            <ta e="T460" id="Seg_2632" s="T434">Onton agam kɨra bɨraːta, Danʼil di͡en kihi, (ɨ͡alla-) uruːlarɨgar ɨ͡allana barbɨt, keːhe, kürüːr (ke-) küreːbitter bu͡o, ol ihin ontuŋ ol ogolorun daː keːhe barbɨttar. </ta>
            <ta e="T475" id="Seg_2633" s="T460">I ol Danʼil bu͡o, manna kamkɨ kɨrdʼɨ͡ar di͡eri Tajmɨrga üleleːn, Ordʼen Trudavoːva Krasnava znamʼenʼi zarabotal. </ta>
            <ta e="T482" id="Seg_2634" s="T478">– Majmago Danʼil Stʼepanavʼičʼ di͡en. </ta>
            <ta e="T492" id="Seg_2635" s="T482">Ol kihi taba iːtiːtiger kamkɨ kɨrdʼɨ͡ar di͡eri üleleːbit ogo haːhɨttan. </ta>
            <ta e="T501" id="Seg_2636" s="T497">– Valačanskaj rajon Kamʼeŋŋa ete. </ta>
            <ta e="T511" id="Seg_2637" s="T501">Onton kennikiː dʼɨllarɨn Xantajskaj Oːzʼera di͡en pasʼolakka olorbuta, onno da ölbüte. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-MaPX">
            <ta e="T1" id="Seg_2638" s="T0">min</ta>
            <ta e="T2" id="Seg_2639" s="T1">Krasnajarskaj</ta>
            <ta e="T3" id="Seg_2640" s="T2">pʼedagagʼičʼeskaj</ta>
            <ta e="T4" id="Seg_2641" s="T3">instʼitut-ɨ</ta>
            <ta e="T5" id="Seg_2642" s="T4">büp-püt-ü-m</ta>
            <ta e="T6" id="Seg_2643" s="T5">istorʼijaː-nɨ</ta>
            <ta e="T7" id="Seg_2644" s="T6">škola-ga</ta>
            <ta e="T8" id="Seg_2645" s="T7">ogo-lor-u</ta>
            <ta e="T9" id="Seg_2646" s="T8">ü͡öret-e</ta>
            <ta e="T10" id="Seg_2647" s="T9">hɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T11" id="Seg_2648" s="T10">istorʼija</ta>
            <ta e="T12" id="Seg_2649" s="T11">i</ta>
            <ta e="T13" id="Seg_2650" s="T12">abšʼestvaznanʼije</ta>
            <ta e="T14" id="Seg_2651" s="T13">di͡e-n</ta>
            <ta e="T15" id="Seg_2652" s="T14">prʼedmʼet-ter-i</ta>
            <ta e="T40" id="Seg_2653" s="T39">u͡on</ta>
            <ta e="T41" id="Seg_2654" s="T40">hette</ta>
            <ta e="T42" id="Seg_2655" s="T41">bötürü͡öp-teːk-ke</ta>
            <ta e="T43" id="Seg_2656" s="T42">haka</ta>
            <ta e="T44" id="Seg_2657" s="T43">hir-i-tten</ta>
            <ta e="T45" id="Seg_2658" s="T44">Lʼena</ta>
            <ta e="T46" id="Seg_2659" s="T45">orto</ta>
            <ta e="T47" id="Seg_2660" s="T46">Lʼena-ttan</ta>
            <ta e="T48" id="Seg_2661" s="T47">u͡on</ta>
            <ta e="T51" id="Seg_2662" s="T50">kel-bit-ter</ta>
            <ta e="T52" id="Seg_2663" s="T51">bastɨkɨː</ta>
            <ta e="T53" id="Seg_2664" s="T52">haka-lar</ta>
            <ta e="T54" id="Seg_2665" s="T53">nʼuːčča</ta>
            <ta e="T55" id="Seg_2666" s="T54">hir-dem-mit-i-ger</ta>
            <ta e="T56" id="Seg_2667" s="T55">Lʼena-ga</ta>
            <ta e="T57" id="Seg_2668" s="T56">epiʼdʼeːmʼije-ler</ta>
            <ta e="T58" id="Seg_2669" s="T57">bu͡ol-but-tara</ta>
            <ta e="T59" id="Seg_2670" s="T58">u͡ospa</ta>
            <ta e="T60" id="Seg_2671" s="T59">čʼuma</ta>
            <ta e="T61" id="Seg_2672" s="T60">sʼibʼirskaj jazva</ta>
            <ta e="T62" id="Seg_2673" s="T61">di͡e-n</ta>
            <ta e="T63" id="Seg_2674" s="T62">kihi-ler</ta>
            <ta e="T64" id="Seg_2675" s="T63">dʼizʼentʼerʼija</ta>
            <ta e="T65" id="Seg_2676" s="T64">onno</ta>
            <ta e="T66" id="Seg_2677" s="T65">tɨːnnaːk</ta>
            <ta e="T67" id="Seg_2678" s="T66">kaːl-bɨt</ta>
            <ta e="T68" id="Seg_2679" s="T67">kihi-ler-i-ŋ</ta>
            <ta e="T69" id="Seg_2680" s="T68">kotuː</ta>
            <ta e="T70" id="Seg_2681" s="T69">hir</ta>
            <ta e="T71" id="Seg_2682" s="T70">di͡ek</ta>
            <ta e="T72" id="Seg_2683" s="T71">eː</ta>
            <ta e="T73" id="Seg_2684" s="T72">baːj</ta>
            <ta e="T74" id="Seg_2685" s="T73">ki͡eŋ</ta>
            <ta e="T75" id="Seg_2686" s="T74">kü͡öl</ta>
            <ta e="T76" id="Seg_2687" s="T75">baːr</ta>
            <ta e="T77" id="Seg_2688" s="T76">di͡e-n</ta>
            <ta e="T78" id="Seg_2689" s="T77">hurak-ka</ta>
            <ta e="T79" id="Seg_2690" s="T78">kel-bit-ter</ta>
            <ta e="T80" id="Seg_2691" s="T79">onon</ta>
            <ta e="T81" id="Seg_2692" s="T80">Dʼehi͡ej</ta>
            <ta e="T82" id="Seg_2693" s="T81">ebe</ta>
            <ta e="T83" id="Seg_2694" s="T82">kɨtɨl-ɨ-gar</ta>
            <ta e="T84" id="Seg_2695" s="T83">olor-on</ta>
            <ta e="T85" id="Seg_2696" s="T84">ol</ta>
            <ta e="T86" id="Seg_2697" s="T85">kel-el-leri-ger</ta>
            <ta e="T87" id="Seg_2698" s="T86">Dʼehi͡ej</ta>
            <ta e="T88" id="Seg_2699" s="T87">kü͡öl-ü-ŋ</ta>
            <ta e="T89" id="Seg_2700" s="T88">kɨtɨl-lar-a</ta>
            <ta e="T90" id="Seg_2701" s="T89">iččitek</ta>
            <ta e="T91" id="Seg_2702" s="T90">ühü</ta>
            <ta e="T92" id="Seg_2703" s="T91">kihi</ta>
            <ta e="T93" id="Seg_2704" s="T92">hu͡ok</ta>
            <ta e="T94" id="Seg_2705" s="T93">uraha-lar</ta>
            <ta e="T95" id="Seg_2706" s="T94">ire</ta>
            <ta e="T96" id="Seg_2707" s="T95">tur-but</ta>
            <ta e="T97" id="Seg_2708" s="T96">tur-al-lar</ta>
            <ta e="T98" id="Seg_2709" s="T97">dʼi͡e-ler-i-ŋ</ta>
            <ta e="T99" id="Seg_2710" s="T98">tu͡os-tara</ta>
            <ta e="T100" id="Seg_2711" s="T99">hɨtɨj-bɨt-tar</ta>
            <ta e="T101" id="Seg_2712" s="T100">kihi-ler</ta>
            <ta e="T102" id="Seg_2713" s="T101">uŋu͡ok-tara</ta>
            <ta e="T103" id="Seg_2714" s="T102">taba-lar</ta>
            <ta e="T104" id="Seg_2715" s="T103">ɨt-tar</ta>
            <ta e="T105" id="Seg_2716" s="T104">uŋu͡ok-tara</ta>
            <ta e="T106" id="Seg_2717" s="T105">hɨt-al-lar</ta>
            <ta e="T107" id="Seg_2718" s="T106">ühü</ta>
            <ta e="T108" id="Seg_2719" s="T107">tögürüččü</ta>
            <ta e="T109" id="Seg_2720" s="T108">Dʼehej-i</ta>
            <ta e="T110" id="Seg_2721" s="T109">onno</ta>
            <ta e="T111" id="Seg_2722" s="T110">u͡ospa-ttan</ta>
            <ta e="T112" id="Seg_2723" s="T111">öl-büt-ter</ta>
            <ta e="T113" id="Seg_2724" s="T112">toŋus-tar</ta>
            <ta e="T114" id="Seg_2725" s="T113">dʼuraːk-tar</ta>
            <ta e="T119" id="Seg_2726" s="T118">o-lor-u</ta>
            <ta e="T120" id="Seg_2727" s="T119">ojun</ta>
            <ta e="T121" id="Seg_2728" s="T120">kɨraː-bɨt</ta>
            <ta e="T122" id="Seg_2729" s="T121">hir-e</ta>
            <ta e="T123" id="Seg_2730" s="T122">di͡e-n</ta>
            <ta e="T124" id="Seg_2731" s="T123">kür-e-t-el-etteː-bit-ter</ta>
            <ta e="T125" id="Seg_2732" s="T124">o-nu</ta>
            <ta e="T126" id="Seg_2733" s="T125">ol</ta>
            <ta e="T127" id="Seg_2734" s="T126">iččitek</ta>
            <ta e="T128" id="Seg_2735" s="T127">hir-ge</ta>
            <ta e="T129" id="Seg_2736" s="T128">kel-en-ner</ta>
            <ta e="T130" id="Seg_2737" s="T129">haka-lar-ɨ-ŋ</ta>
            <ta e="T131" id="Seg_2738" s="T130">törüt</ta>
            <ta e="T132" id="Seg_2739" s="T131">hir</ta>
            <ta e="T133" id="Seg_2740" s="T132">oŋost-u-but-tar</ta>
            <ta e="T134" id="Seg_2741" s="T133">Dʼehi͡ej-i</ta>
            <ta e="T135" id="Seg_2742" s="T134">onton</ta>
            <ta e="T136" id="Seg_2743" s="T135">gdʼe_tа</ta>
            <ta e="T137" id="Seg_2744" s="T136">u͡on</ta>
            <ta e="T138" id="Seg_2745" s="T137">togus</ta>
            <ta e="T139" id="Seg_2746" s="T138">bötürü͡ök-ke</ta>
            <ta e="T140" id="Seg_2747" s="T139">Tajmɨr</ta>
            <ta e="T141" id="Seg_2748" s="T140">di͡ek</ta>
            <ta e="T142" id="Seg_2749" s="T141">horok</ta>
            <ta e="T143" id="Seg_2750" s="T142">kihi-ler</ta>
            <ta e="T144" id="Seg_2751" s="T143">Tajmɨr</ta>
            <ta e="T145" id="Seg_2752" s="T144">di͡ek</ta>
            <ta e="T146" id="Seg_2753" s="T145">uže</ta>
            <ta e="T147" id="Seg_2754" s="T146">köh-ö</ta>
            <ta e="T148" id="Seg_2755" s="T147">hɨldʼ-ar</ta>
            <ta e="T149" id="Seg_2756" s="T148">bu͡ol-but-tar</ta>
            <ta e="T154" id="Seg_2757" s="T153">taba-nnan</ta>
            <ta e="T155" id="Seg_2758" s="T154">taba-nnan</ta>
            <ta e="T156" id="Seg_2759" s="T155">uže</ta>
            <ta e="T157" id="Seg_2760" s="T156">ol</ta>
            <ta e="T158" id="Seg_2761" s="T157">taba-nɨ</ta>
            <ta e="T159" id="Seg_2762" s="T158">uže</ta>
            <ta e="T160" id="Seg_2763" s="T159">iːt-er</ta>
            <ta e="T161" id="Seg_2764" s="T160">kihi</ta>
            <ta e="T162" id="Seg_2765" s="T161">bu͡ol-but-tar</ta>
            <ta e="T163" id="Seg_2766" s="T162">bu͡o</ta>
            <ta e="T164" id="Seg_2767" s="T163">mm</ta>
            <ta e="T165" id="Seg_2768" s="T164">bettek</ta>
            <ta e="T166" id="Seg_2769" s="T165">kotuː</ta>
            <ta e="T167" id="Seg_2770" s="T166">hir</ta>
            <ta e="T168" id="Seg_2771" s="T167">di͡ek</ta>
            <ta e="T169" id="Seg_2772" s="T168">kel-en</ta>
            <ta e="T170" id="Seg_2773" s="T169">bar-aːn-nar</ta>
            <ta e="T182" id="Seg_2774" s="T181">Dʼehi͡ej-der-i-ŋ</ta>
            <ta e="T183" id="Seg_2775" s="T182">eː</ta>
            <ta e="T184" id="Seg_2776" s="T183">barɨ-lara</ta>
            <ta e="T185" id="Seg_2777" s="T184">da</ta>
            <ta e="T186" id="Seg_2778" s="T185">bu͡ol-bat-tar</ta>
            <ta e="T187" id="Seg_2779" s="T186">baːj</ta>
            <ta e="T188" id="Seg_2780" s="T187">kihi-ler</ta>
            <ta e="T189" id="Seg_2781" s="T188">baːj</ta>
            <ta e="T190" id="Seg_2782" s="T189">ɨ͡al-lar</ta>
            <ta e="T191" id="Seg_2783" s="T190">hebi͡eskej</ta>
            <ta e="T192" id="Seg_2784" s="T191">bɨlaːs</ta>
            <ta e="T193" id="Seg_2785" s="T192">tur-ar-ɨ-gar</ta>
            <ta e="T194" id="Seg_2786" s="T193">hebi͡eskej</ta>
            <ta e="T195" id="Seg_2787" s="T194">bɨlaːs-tan</ta>
            <ta e="T196" id="Seg_2788" s="T195">küreː-n-ner</ta>
            <ta e="T197" id="Seg_2789" s="T196">v asnavnom</ta>
            <ta e="T198" id="Seg_2790" s="T197">urut</ta>
            <ta e="T199" id="Seg_2791" s="T198">otto</ta>
            <ta e="T201" id="Seg_2792" s="T199">taːk</ta>
            <ta e="T202" id="Seg_2793" s="T201">atɨː</ta>
            <ta e="T203" id="Seg_2794" s="T202">hɨldʼ-al-lar</ta>
            <ta e="T204" id="Seg_2795" s="T203">bu͡o</ta>
            <ta e="T205" id="Seg_2796" s="T204">ɨ͡allan-a</ta>
            <ta e="T206" id="Seg_2797" s="T205">hɨldʼ-al-lar</ta>
            <ta e="T207" id="Seg_2798" s="T206">horok-toro</ta>
            <ta e="T208" id="Seg_2799" s="T207">kaːl-al-lar</ta>
            <ta e="T209" id="Seg_2800" s="T208">eː</ta>
            <ta e="T210" id="Seg_2801" s="T209">mannaː-gɨ</ta>
            <ta e="T211" id="Seg_2802" s="T210">kihi</ta>
            <ta e="T212" id="Seg_2803" s="T211">Tajmu͡or-ga</ta>
            <ta e="T213" id="Seg_2804" s="T212">baːr</ta>
            <ta e="T214" id="Seg_2805" s="T213">kergen</ta>
            <ta e="T215" id="Seg_2806" s="T214">bul-u-n-an-nar</ta>
            <ta e="T216" id="Seg_2807" s="T215">eŋin</ta>
            <ta e="T217" id="Seg_2808" s="T216">kaːl-al-lar</ta>
            <ta e="T218" id="Seg_2809" s="T217">aː</ta>
            <ta e="T219" id="Seg_2810" s="T218">no</ta>
            <ta e="T220" id="Seg_2811" s="T219">savʼetskaj</ta>
            <ta e="T221" id="Seg_2812" s="T220">bɨlaːs</ta>
            <ta e="T222" id="Seg_2813" s="T221">tur-u͡o-r</ta>
            <ta e="T223" id="Seg_2814" s="T222">di͡eri</ta>
            <ta e="T224" id="Seg_2815" s="T223">Dʼehi͡ej</ta>
            <ta e="T225" id="Seg_2816" s="T224">haka-lar-a</ta>
            <ta e="T226" id="Seg_2817" s="T225">biːr</ta>
            <ta e="T227" id="Seg_2818" s="T226">hir-ge</ta>
            <ta e="T228" id="Seg_2819" s="T227">olor-or</ta>
            <ta e="T229" id="Seg_2820" s="T228">e-ti-ler</ta>
            <ta e="T230" id="Seg_2821" s="T229">onton</ta>
            <ta e="T231" id="Seg_2822" s="T230">savʼetskaj</ta>
            <ta e="T232" id="Seg_2823" s="T231">vlaːh-ɨ-ttan</ta>
            <ta e="T233" id="Seg_2824" s="T232">küreː-n</ta>
            <ta e="T234" id="Seg_2825" s="T233">v asnavnom</ta>
            <ta e="T235" id="Seg_2826" s="T234">baːj</ta>
            <ta e="T236" id="Seg_2827" s="T235">ɨ͡al-lar</ta>
            <ta e="T237" id="Seg_2828" s="T236">bettek</ta>
            <ta e="T238" id="Seg_2829" s="T237">kel-etteː-bit-ter</ta>
            <ta e="T239" id="Seg_2830" s="T238">munna</ta>
            <ta e="T240" id="Seg_2831" s="T239">Tajmu͡or-ga</ta>
            <ta e="T241" id="Seg_2832" s="T240">baːr</ta>
            <ta e="T242" id="Seg_2833" s="T241">uruː-larɨ-gar</ta>
            <ta e="T243" id="Seg_2834" s="T242">čugas</ta>
            <ta e="T244" id="Seg_2835" s="T243">könö</ta>
            <ta e="T245" id="Seg_2836" s="T244">bil-er</ta>
            <ta e="T246" id="Seg_2837" s="T245">hir-deri-ger</ta>
            <ta e="T247" id="Seg_2838" s="T246">eː</ta>
            <ta e="T248" id="Seg_2839" s="T247">kel-bit-ter</ta>
            <ta e="T249" id="Seg_2840" s="T248">köh-öttöː-n</ta>
            <ta e="T250" id="Seg_2841" s="T249">onton</ta>
            <ta e="T251" id="Seg_2842" s="T250">hüːrbe</ta>
            <ta e="T252" id="Seg_2843" s="T251">dʼɨl-lar</ta>
            <ta e="T253" id="Seg_2844" s="T252">büt-el-leri-ger</ta>
            <ta e="T254" id="Seg_2845" s="T253">otut</ta>
            <ta e="T255" id="Seg_2846" s="T254">dʼɨl-lar</ta>
            <ta e="T256" id="Seg_2847" s="T255">taks-al-larɨ-gar</ta>
            <ta e="T257" id="Seg_2848" s="T256">Tajmu͡or-ga</ta>
            <ta e="T258" id="Seg_2849" s="T257">emi͡e</ta>
            <ta e="T259" id="Seg_2850" s="T258">savʼetskaj</ta>
            <ta e="T260" id="Seg_2851" s="T259">bɨlaːh-a</ta>
            <ta e="T261" id="Seg_2852" s="T260">palʼitʼika-ta</ta>
            <ta e="T262" id="Seg_2853" s="T261">kɨtaːt-an</ta>
            <ta e="T263" id="Seg_2854" s="T262">is-pit</ta>
            <ta e="T264" id="Seg_2855" s="T263">emi͡e</ta>
            <ta e="T265" id="Seg_2856" s="T264">dʼe</ta>
            <ta e="T266" id="Seg_2857" s="T265">baːj</ta>
            <ta e="T267" id="Seg_2858" s="T266">kihi-ler-i</ta>
            <ta e="T268" id="Seg_2859" s="T267">baːj-darɨ-n</ta>
            <ta e="T269" id="Seg_2860" s="T268">tal-aːn</ta>
            <ta e="T270" id="Seg_2861" s="T269">üllest-i͡ek-ke</ta>
            <ta e="T271" id="Seg_2862" s="T270">di͡e-n</ta>
            <ta e="T272" id="Seg_2863" s="T271">bu͡ol-but</ta>
            <ta e="T277" id="Seg_2864" s="T276">taba-larɨ-n</ta>
            <ta e="T278" id="Seg_2865" s="T277">bɨldʼaː-n</ta>
            <ta e="T279" id="Seg_2866" s="T278">onton</ta>
            <ta e="T280" id="Seg_2867" s="T279">taba-larɨ-n</ta>
            <ta e="T281" id="Seg_2868" s="T280">ere</ta>
            <ta e="T282" id="Seg_2869" s="T281">bu͡ol-bat</ta>
            <ta e="T283" id="Seg_2870" s="T282">dʼi͡e</ta>
            <ta e="T284" id="Seg_2871" s="T283">üp</ta>
            <ta e="T285" id="Seg_2872" s="T284">tüːnün</ta>
            <ta e="T286" id="Seg_2873" s="T285">tal-ɨːl-lar</ta>
            <ta e="T287" id="Seg_2874" s="T286">bu͡o</ta>
            <ta e="T288" id="Seg_2875" s="T287">o-nu</ta>
            <ta e="T289" id="Seg_2876" s="T288">barɨ-tɨ-n</ta>
            <ta e="T290" id="Seg_2877" s="T289">tüget-i͡ek-ke</ta>
            <ta e="T291" id="Seg_2878" s="T290">naːda</ta>
            <ta e="T292" id="Seg_2879" s="T291">di͡e-n</ta>
            <ta e="T293" id="Seg_2880" s="T292">dʼadaŋɨ-lar-ga</ta>
            <ta e="T294" id="Seg_2881" s="T293">urut</ta>
            <ta e="T295" id="Seg_2882" s="T294">daː</ta>
            <ta e="T296" id="Seg_2883" s="T295">otto</ta>
            <ta e="T297" id="Seg_2884" s="T296">baːj</ta>
            <ta e="T298" id="Seg_2885" s="T297">kihi-ŋ</ta>
            <ta e="T299" id="Seg_2886" s="T298">bɨlɨr-gɨ</ta>
            <ta e="T300" id="Seg_2887" s="T299">haku͡on-ɨ-nan</ta>
            <ta e="T301" id="Seg_2888" s="T300">kim-i</ta>
            <ta e="T302" id="Seg_2889" s="T301">da</ta>
            <ta e="T303" id="Seg_2890" s="T302">atagastaː-bakka</ta>
            <ta e="T304" id="Seg_2891" s="T303">ülehit-i-n</ta>
            <ta e="T305" id="Seg_2892" s="T304">da</ta>
            <ta e="T306" id="Seg_2893" s="T305">atagastaː-bakka</ta>
            <ta e="T307" id="Seg_2894" s="T306">beje-ti-n</ta>
            <ta e="T308" id="Seg_2895" s="T307">uruː-tu-n</ta>
            <ta e="T309" id="Seg_2896" s="T308">da</ta>
            <ta e="T312" id="Seg_2897" s="T311">dʼadaŋɨ</ta>
            <ta e="T313" id="Seg_2898" s="T312">uruː-tu-n</ta>
            <ta e="T314" id="Seg_2899" s="T313">da</ta>
            <ta e="T315" id="Seg_2900" s="T314">atagastaː-bakka</ta>
            <ta e="T316" id="Seg_2901" s="T315">olor-but-tar</ta>
            <ta e="T317" id="Seg_2902" s="T316">bu͡o</ta>
            <ta e="T318" id="Seg_2903" s="T317">abɨčaj-dara</ta>
            <ta e="T319" id="Seg_2904" s="T318">onnuk</ta>
            <ta e="T320" id="Seg_2905" s="T319">kömölöh-ön</ta>
            <ta e="T321" id="Seg_2906" s="T320">olor-but-tar</ta>
            <ta e="T322" id="Seg_2907" s="T321">bu͡o</ta>
            <ta e="T323" id="Seg_2908" s="T322">savʼetskaj</ta>
            <ta e="T324" id="Seg_2909" s="T323">bɨlaːs</ta>
            <ta e="T325" id="Seg_2910" s="T324">palʼitʼika-tɨ-n</ta>
            <ta e="T326" id="Seg_2911" s="T325">gɨtta</ta>
            <ta e="T327" id="Seg_2912" s="T326">höbül-e-s-petek</ta>
            <ta e="T328" id="Seg_2913" s="T327">kihi-ler-i-ŋ</ta>
            <ta e="T329" id="Seg_2914" s="T328">manna</ta>
            <ta e="T330" id="Seg_2915" s="T329">bunt</ta>
            <ta e="T331" id="Seg_2916" s="T330">tahaːr-bɨt-tara</ta>
            <ta e="T335" id="Seg_2917" s="T334">heː</ta>
            <ta e="T336" id="Seg_2918" s="T335">aːjdahan</ta>
            <ta e="T337" id="Seg_2919" s="T336">tur-but</ta>
            <ta e="T338" id="Seg_2920" s="T337">dʼe</ta>
            <ta e="T339" id="Seg_2921" s="T338">onno</ta>
            <ta e="T340" id="Seg_2922" s="T339">ol</ta>
            <ta e="T341" id="Seg_2923" s="T340">ulakan</ta>
            <ta e="T342" id="Seg_2924" s="T341">ajdaːŋ-ŋa</ta>
            <ta e="T343" id="Seg_2925" s="T342">kihi-ni</ta>
            <ta e="T344" id="Seg_2926" s="T343">eŋin</ta>
            <ta e="T345" id="Seg_2927" s="T344">ölör-büt-ter</ta>
            <ta e="T346" id="Seg_2928" s="T345">manna</ta>
            <ta e="T347" id="Seg_2929" s="T346">tu͡ok-tar-ɨ</ta>
            <ta e="T348" id="Seg_2930" s="T347">upalnamočennɨj-dar-ɨ</ta>
            <ta e="T351" id="Seg_2931" s="T350">tʼe</ta>
            <ta e="T352" id="Seg_2932" s="T351">ol</ta>
            <ta e="T353" id="Seg_2933" s="T352">tɨ͡a</ta>
            <ta e="T354" id="Seg_2934" s="T353">kihi-te</ta>
            <ta e="T355" id="Seg_2935" s="T354">ölör-büt-e</ta>
            <ta e="T356" id="Seg_2936" s="T355">bil-l-i-bet</ta>
            <ta e="T357" id="Seg_2937" s="T356">ili</ta>
            <ta e="T358" id="Seg_2938" s="T357">že</ta>
            <ta e="T359" id="Seg_2939" s="T358">atɨn</ta>
            <ta e="T360" id="Seg_2940" s="T359">tu͡ok</ta>
            <ta e="T361" id="Seg_2941" s="T360">eme</ta>
            <ta e="T362" id="Seg_2942" s="T361">kihi-te</ta>
            <ta e="T363" id="Seg_2943" s="T362">anaːn</ta>
            <ta e="T364" id="Seg_2944" s="T363">onnuk</ta>
            <ta e="T365" id="Seg_2945" s="T364">aːjdaːn-ɨ</ta>
            <ta e="T366" id="Seg_2946" s="T365">tahaːr-aːrɨ</ta>
            <ta e="T367" id="Seg_2947" s="T366">ölör-büt-e</ta>
            <ta e="T368" id="Seg_2948" s="T367">bil-l-i-bet</ta>
            <ta e="T369" id="Seg_2949" s="T368">anɨ-ga</ta>
            <ta e="T370" id="Seg_2950" s="T369">daː</ta>
            <ta e="T371" id="Seg_2951" s="T370">di͡eri</ta>
            <ta e="T372" id="Seg_2952" s="T371">onno</ta>
            <ta e="T373" id="Seg_2953" s="T372">aktʼivnaj</ta>
            <ta e="T374" id="Seg_2954" s="T373">dʼe</ta>
            <ta e="T375" id="Seg_2955" s="T374">otto</ta>
            <ta e="T376" id="Seg_2956" s="T375">aristuːj-daː-bɨt-tar</ta>
            <ta e="T377" id="Seg_2957" s="T376">bu͡o</ta>
            <ta e="T382" id="Seg_2958" s="T381">aːrʼest-ɨ</ta>
            <ta e="T385" id="Seg_2959" s="T384">di͡e-n</ta>
            <ta e="T386" id="Seg_2960" s="T385">kihi</ta>
            <ta e="T387" id="Seg_2961" s="T386">kel-bit-ter</ta>
            <ta e="T388" id="Seg_2962" s="T387">Xaːtanga-ttan</ta>
            <ta e="T389" id="Seg_2963" s="T388">Jenʼisʼejskʼej-ten</ta>
            <ta e="T392" id="Seg_2964" s="T391">nʼuːčča-lar</ta>
            <ta e="T393" id="Seg_2965" s="T392">heː</ta>
            <ta e="T394" id="Seg_2966" s="T393">haː-laːk</ta>
            <ta e="T395" id="Seg_2967" s="T394">dʼe</ta>
            <ta e="T396" id="Seg_2968" s="T395">on</ta>
            <ta e="T397" id="Seg_2969" s="T396">ol</ta>
            <ta e="T398" id="Seg_2970" s="T397">hurak-tan</ta>
            <ta e="T399" id="Seg_2971" s="T398">kihi-ler-i</ta>
            <ta e="T400" id="Seg_2972" s="T399">kaːjɨː-ga</ta>
            <ta e="T401" id="Seg_2973" s="T400">olord-ol-lor</ta>
            <ta e="T402" id="Seg_2974" s="T401">ühü</ta>
            <ta e="T403" id="Seg_2975" s="T402">di͡e-n</ta>
            <ta e="T404" id="Seg_2976" s="T403">hurak-tan</ta>
            <ta e="T405" id="Seg_2977" s="T404">min</ta>
            <ta e="T406" id="Seg_2978" s="T405">inʼe-m</ta>
            <ta e="T407" id="Seg_2979" s="T406">aga-m</ta>
            <ta e="T408" id="Seg_2980" s="T407">ehe-m</ta>
            <ta e="T409" id="Seg_2981" s="T408">bu͡ol-an-nar</ta>
            <ta e="T410" id="Seg_2982" s="T409">dʼe</ta>
            <ta e="T411" id="Seg_2983" s="T410">kürüː-r-ge</ta>
            <ta e="T412" id="Seg_2984" s="T411">tur-but-tar</ta>
            <ta e="T413" id="Seg_2985" s="T412">i</ta>
            <ta e="T414" id="Seg_2986" s="T413">töttörü</ta>
            <ta e="T415" id="Seg_2987" s="T414">Dʼehi͡ej-deri-ger</ta>
            <ta e="T416" id="Seg_2988" s="T415">tönn-ü-büt-ter</ta>
            <ta e="T417" id="Seg_2989" s="T416">aga-m</ta>
            <ta e="T418" id="Seg_2990" s="T417">braːt-a</ta>
            <ta e="T419" id="Seg_2991" s="T418">Ignatʼij</ta>
            <ta e="T420" id="Seg_2992" s="T419">di͡e-n</ta>
            <ta e="T421" id="Seg_2993" s="T420">kihi-ni</ta>
            <ta e="T422" id="Seg_2994" s="T421">ɨl-bɨt-tar</ta>
            <ta e="T423" id="Seg_2995" s="T422">kaːjɨː-ga</ta>
            <ta e="T424" id="Seg_2996" s="T423">olord-on</ta>
            <ta e="T425" id="Seg_2997" s="T424">baraːn</ta>
            <ta e="T426" id="Seg_2998" s="T425">ɨp-pɨt</ta>
            <ta e="T427" id="Seg_2999" s="T426">ölör-büt-ter</ta>
            <ta e="T428" id="Seg_3000" s="T427">če</ta>
            <ta e="T432" id="Seg_3001" s="T431">heː</ta>
            <ta e="T433" id="Seg_3002" s="T432">Majmago-lar</ta>
            <ta e="T434" id="Seg_3003" s="T433">aga</ta>
            <ta e="T435" id="Seg_3004" s="T434">onton</ta>
            <ta e="T436" id="Seg_3005" s="T435">aga-m</ta>
            <ta e="T437" id="Seg_3006" s="T436">kɨra</ta>
            <ta e="T438" id="Seg_3007" s="T437">bɨraːt-a</ta>
            <ta e="T439" id="Seg_3008" s="T438">Danʼil</ta>
            <ta e="T440" id="Seg_3009" s="T439">di͡e-n</ta>
            <ta e="T441" id="Seg_3010" s="T440">kihi</ta>
            <ta e="T444" id="Seg_3011" s="T443">uruː-larɨ-gar</ta>
            <ta e="T445" id="Seg_3012" s="T444">ɨ͡allan-a</ta>
            <ta e="T446" id="Seg_3013" s="T445">bar-bɨt</ta>
            <ta e="T447" id="Seg_3014" s="T446">keːh-e</ta>
            <ta e="T448" id="Seg_3015" s="T447">kürüː-r</ta>
            <ta e="T451" id="Seg_3016" s="T450">küreː-bit-ter</ta>
            <ta e="T452" id="Seg_3017" s="T451">bu͡o</ta>
            <ta e="T453" id="Seg_3018" s="T452">ol</ta>
            <ta e="T454" id="Seg_3019" s="T453">ihin</ta>
            <ta e="T455" id="Seg_3020" s="T454">on-tu-ŋ</ta>
            <ta e="T456" id="Seg_3021" s="T455">ol</ta>
            <ta e="T457" id="Seg_3022" s="T456">ogo-loru-n</ta>
            <ta e="T458" id="Seg_3023" s="T457">daː</ta>
            <ta e="T459" id="Seg_3024" s="T458">keːh-e</ta>
            <ta e="T460" id="Seg_3025" s="T459">bar-bɨt-tar</ta>
            <ta e="T461" id="Seg_3026" s="T460">i</ta>
            <ta e="T462" id="Seg_3027" s="T461">ol</ta>
            <ta e="T463" id="Seg_3028" s="T462">Danʼil</ta>
            <ta e="T464" id="Seg_3029" s="T463">bu͡o</ta>
            <ta e="T465" id="Seg_3030" s="T464">manna</ta>
            <ta e="T466" id="Seg_3031" s="T465">kamkɨ</ta>
            <ta e="T467" id="Seg_3032" s="T466">kɨrdʼ-ɨ͡a-r</ta>
            <ta e="T468" id="Seg_3033" s="T467">di͡eri</ta>
            <ta e="T469" id="Seg_3034" s="T468">Tajmɨr-ga</ta>
            <ta e="T470" id="Seg_3035" s="T469">üleleː-n</ta>
            <ta e="T479" id="Seg_3036" s="T478">Majmago</ta>
            <ta e="T480" id="Seg_3037" s="T479">Danʼil</ta>
            <ta e="T481" id="Seg_3038" s="T480">Stʼepanavʼičʼ</ta>
            <ta e="T482" id="Seg_3039" s="T481">di͡e-n</ta>
            <ta e="T483" id="Seg_3040" s="T482">ol</ta>
            <ta e="T484" id="Seg_3041" s="T483">kihi</ta>
            <ta e="T485" id="Seg_3042" s="T484">taba</ta>
            <ta e="T486" id="Seg_3043" s="T485">iːt-iː-ti-ger</ta>
            <ta e="T487" id="Seg_3044" s="T486">kamkɨ</ta>
            <ta e="T488" id="Seg_3045" s="T487">kɨrdʼ-ɨ͡a-r</ta>
            <ta e="T489" id="Seg_3046" s="T488">di͡eri</ta>
            <ta e="T490" id="Seg_3047" s="T489">üleleː-bit</ta>
            <ta e="T491" id="Seg_3048" s="T490">ogo</ta>
            <ta e="T492" id="Seg_3049" s="T491">haːh-ɨ-ttan</ta>
            <ta e="T498" id="Seg_3050" s="T497">Valačanskaj</ta>
            <ta e="T499" id="Seg_3051" s="T498">rajon</ta>
            <ta e="T500" id="Seg_3052" s="T499">Kamʼeŋ-ŋa</ta>
            <ta e="T501" id="Seg_3053" s="T500">e-t-e</ta>
            <ta e="T502" id="Seg_3054" s="T501">onton</ta>
            <ta e="T503" id="Seg_3055" s="T502">kenni-kiː</ta>
            <ta e="T504" id="Seg_3056" s="T503">dʼɨl-larɨ-n</ta>
            <ta e="T505" id="Seg_3057" s="T504">Xantajskaj Oːzʼera</ta>
            <ta e="T506" id="Seg_3058" s="T505">di͡e-n</ta>
            <ta e="T507" id="Seg_3059" s="T506">pasʼolak-ka</ta>
            <ta e="T508" id="Seg_3060" s="T507">olor-but-a</ta>
            <ta e="T509" id="Seg_3061" s="T508">onno</ta>
            <ta e="T510" id="Seg_3062" s="T509">da</ta>
            <ta e="T511" id="Seg_3063" s="T510">öl-büt-e</ta>
         </annotation>
         <annotation name="mp" tierref="mp-MaPX">
            <ta e="T1" id="Seg_3064" s="T0">min</ta>
            <ta e="T2" id="Seg_3065" s="T1">Krasnajarskaj</ta>
            <ta e="T3" id="Seg_3066" s="T2">pʼedagagʼičʼeskaj</ta>
            <ta e="T4" id="Seg_3067" s="T3">instʼitut-nI</ta>
            <ta e="T5" id="Seg_3068" s="T4">büt-BIT-I-m</ta>
            <ta e="T6" id="Seg_3069" s="T5">istorʼija-nI</ta>
            <ta e="T7" id="Seg_3070" s="T6">usku͡ola-GA</ta>
            <ta e="T8" id="Seg_3071" s="T7">ogo-LAr-nI</ta>
            <ta e="T9" id="Seg_3072" s="T8">ü͡öret-A</ta>
            <ta e="T10" id="Seg_3073" s="T9">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T11" id="Seg_3074" s="T10">istorʼija</ta>
            <ta e="T12" id="Seg_3075" s="T11">i</ta>
            <ta e="T13" id="Seg_3076" s="T12">abšʼestvaznanʼije</ta>
            <ta e="T14" id="Seg_3077" s="T13">di͡e-An</ta>
            <ta e="T15" id="Seg_3078" s="T14">prʼedmʼet-LAr-nI</ta>
            <ta e="T40" id="Seg_3079" s="T39">u͡on</ta>
            <ta e="T41" id="Seg_3080" s="T40">hette</ta>
            <ta e="T42" id="Seg_3081" s="T41">bötürü͡öp-LAːK-GA</ta>
            <ta e="T43" id="Seg_3082" s="T42">haka</ta>
            <ta e="T44" id="Seg_3083" s="T43">hir-tI-ttAn</ta>
            <ta e="T45" id="Seg_3084" s="T44">Lʼena</ta>
            <ta e="T46" id="Seg_3085" s="T45">orto</ta>
            <ta e="T47" id="Seg_3086" s="T46">Lʼena-ttAn</ta>
            <ta e="T48" id="Seg_3087" s="T47">u͡on</ta>
            <ta e="T51" id="Seg_3088" s="T50">kel-BIT-LAr</ta>
            <ta e="T52" id="Seg_3089" s="T51">bastakɨ</ta>
            <ta e="T53" id="Seg_3090" s="T52">haka-LAr</ta>
            <ta e="T54" id="Seg_3091" s="T53">nuːčča</ta>
            <ta e="T55" id="Seg_3092" s="T54">hir-LAN-BIT-tI-GAr</ta>
            <ta e="T56" id="Seg_3093" s="T55">Lʼena-GA</ta>
            <ta e="T57" id="Seg_3094" s="T56">epiʼdʼeːmʼije-LAr</ta>
            <ta e="T58" id="Seg_3095" s="T57">bu͡ol-BIT-LArA</ta>
            <ta e="T59" id="Seg_3096" s="T58">bu͡ospa</ta>
            <ta e="T60" id="Seg_3097" s="T59">čʼuma</ta>
            <ta e="T61" id="Seg_3098" s="T60">sʼibʼirskaj jazva</ta>
            <ta e="T62" id="Seg_3099" s="T61">di͡e-An</ta>
            <ta e="T63" id="Seg_3100" s="T62">kihi-LAr</ta>
            <ta e="T64" id="Seg_3101" s="T63">dʼizʼentʼerʼija</ta>
            <ta e="T65" id="Seg_3102" s="T64">onno</ta>
            <ta e="T66" id="Seg_3103" s="T65">tɨːnnaːk</ta>
            <ta e="T67" id="Seg_3104" s="T66">kaːl-BIT</ta>
            <ta e="T68" id="Seg_3105" s="T67">kihi-LAr-I-ŋ</ta>
            <ta e="T69" id="Seg_3106" s="T68">kotuː</ta>
            <ta e="T70" id="Seg_3107" s="T69">hir</ta>
            <ta e="T71" id="Seg_3108" s="T70">dek</ta>
            <ta e="T72" id="Seg_3109" s="T71">eː</ta>
            <ta e="T73" id="Seg_3110" s="T72">baːj</ta>
            <ta e="T74" id="Seg_3111" s="T73">ki͡eŋ</ta>
            <ta e="T75" id="Seg_3112" s="T74">kü͡öl</ta>
            <ta e="T76" id="Seg_3113" s="T75">baːr</ta>
            <ta e="T77" id="Seg_3114" s="T76">di͡e-An</ta>
            <ta e="T78" id="Seg_3115" s="T77">hurak-GA</ta>
            <ta e="T79" id="Seg_3116" s="T78">kel-BIT-LAr</ta>
            <ta e="T80" id="Seg_3117" s="T79">onon</ta>
            <ta e="T81" id="Seg_3118" s="T80">Essej</ta>
            <ta e="T82" id="Seg_3119" s="T81">ebe</ta>
            <ta e="T83" id="Seg_3120" s="T82">kɨtɨl-tI-GAr</ta>
            <ta e="T84" id="Seg_3121" s="T83">olor-An</ta>
            <ta e="T85" id="Seg_3122" s="T84">ol</ta>
            <ta e="T86" id="Seg_3123" s="T85">kel-Ar-LArI-GAr</ta>
            <ta e="T87" id="Seg_3124" s="T86">Essej</ta>
            <ta e="T88" id="Seg_3125" s="T87">kü͡öl-I-ŋ</ta>
            <ta e="T89" id="Seg_3126" s="T88">kɨtɨl-LAr-tA</ta>
            <ta e="T90" id="Seg_3127" s="T89">iččitek</ta>
            <ta e="T91" id="Seg_3128" s="T90">ühü</ta>
            <ta e="T92" id="Seg_3129" s="T91">kihi</ta>
            <ta e="T93" id="Seg_3130" s="T92">hu͡ok</ta>
            <ta e="T94" id="Seg_3131" s="T93">uraha-LAr</ta>
            <ta e="T95" id="Seg_3132" s="T94">ere</ta>
            <ta e="T96" id="Seg_3133" s="T95">tur-BIT</ta>
            <ta e="T97" id="Seg_3134" s="T96">tur-Ar-LAr</ta>
            <ta e="T98" id="Seg_3135" s="T97">dʼi͡e-LAr-I-ŋ</ta>
            <ta e="T99" id="Seg_3136" s="T98">tu͡os-LArA</ta>
            <ta e="T100" id="Seg_3137" s="T99">hɨtɨj-BIT-LAr</ta>
            <ta e="T101" id="Seg_3138" s="T100">kihi-LAr</ta>
            <ta e="T102" id="Seg_3139" s="T101">oŋu͡ok-LArA</ta>
            <ta e="T103" id="Seg_3140" s="T102">taba-LAr</ta>
            <ta e="T104" id="Seg_3141" s="T103">ɨt-LAr</ta>
            <ta e="T105" id="Seg_3142" s="T104">oŋu͡ok-LArA</ta>
            <ta e="T106" id="Seg_3143" s="T105">hɨt-Ar-LAr</ta>
            <ta e="T107" id="Seg_3144" s="T106">ühü</ta>
            <ta e="T108" id="Seg_3145" s="T107">tögürüččü</ta>
            <ta e="T109" id="Seg_3146" s="T108">Dʼehej-nI</ta>
            <ta e="T110" id="Seg_3147" s="T109">onno</ta>
            <ta e="T111" id="Seg_3148" s="T110">bu͡ospa-ttAn</ta>
            <ta e="T112" id="Seg_3149" s="T111">öl-BIT-LAr</ta>
            <ta e="T113" id="Seg_3150" s="T112">toŋus-LAr</ta>
            <ta e="T114" id="Seg_3151" s="T113">dʼuraːk-LAr</ta>
            <ta e="T119" id="Seg_3152" s="T118">ol-LAr-nI</ta>
            <ta e="T120" id="Seg_3153" s="T119">ojun</ta>
            <ta e="T121" id="Seg_3154" s="T120">kɨraː-BIT</ta>
            <ta e="T122" id="Seg_3155" s="T121">hir-tA</ta>
            <ta e="T123" id="Seg_3156" s="T122">di͡e-An</ta>
            <ta e="T124" id="Seg_3157" s="T123">küreː-A-t-AlAː-AttAː-BIT-LAr</ta>
            <ta e="T125" id="Seg_3158" s="T124">ol-nI</ta>
            <ta e="T126" id="Seg_3159" s="T125">ol</ta>
            <ta e="T127" id="Seg_3160" s="T126">iččitek</ta>
            <ta e="T128" id="Seg_3161" s="T127">hir-GA</ta>
            <ta e="T129" id="Seg_3162" s="T128">kel-An-LAr</ta>
            <ta e="T130" id="Seg_3163" s="T129">haka-LAr-I-ŋ</ta>
            <ta e="T131" id="Seg_3164" s="T130">törüt</ta>
            <ta e="T132" id="Seg_3165" s="T131">hir</ta>
            <ta e="T133" id="Seg_3166" s="T132">oŋohun-I-BIT-LAr</ta>
            <ta e="T134" id="Seg_3167" s="T133">Essej-nI</ta>
            <ta e="T135" id="Seg_3168" s="T134">onton</ta>
            <ta e="T136" id="Seg_3169" s="T135">gdʼe_tа</ta>
            <ta e="T137" id="Seg_3170" s="T136">u͡on</ta>
            <ta e="T138" id="Seg_3171" s="T137">togus</ta>
            <ta e="T139" id="Seg_3172" s="T138">bötürü͡öp-GA</ta>
            <ta e="T140" id="Seg_3173" s="T139">Tajmɨr</ta>
            <ta e="T141" id="Seg_3174" s="T140">dek</ta>
            <ta e="T142" id="Seg_3175" s="T141">horok</ta>
            <ta e="T143" id="Seg_3176" s="T142">kihi-LAr</ta>
            <ta e="T144" id="Seg_3177" s="T143">Tajmɨr</ta>
            <ta e="T145" id="Seg_3178" s="T144">dek</ta>
            <ta e="T146" id="Seg_3179" s="T145">uže</ta>
            <ta e="T147" id="Seg_3180" s="T146">kös-A</ta>
            <ta e="T148" id="Seg_3181" s="T147">hɨrɨt-Ar</ta>
            <ta e="T149" id="Seg_3182" s="T148">bu͡ol-BIT-LAr</ta>
            <ta e="T154" id="Seg_3183" s="T153">taba-nAn</ta>
            <ta e="T155" id="Seg_3184" s="T154">taba-nAn</ta>
            <ta e="T156" id="Seg_3185" s="T155">uže</ta>
            <ta e="T157" id="Seg_3186" s="T156">ol</ta>
            <ta e="T158" id="Seg_3187" s="T157">taba-nI</ta>
            <ta e="T159" id="Seg_3188" s="T158">uže</ta>
            <ta e="T160" id="Seg_3189" s="T159">iːt-Ar</ta>
            <ta e="T161" id="Seg_3190" s="T160">kihi</ta>
            <ta e="T162" id="Seg_3191" s="T161">bu͡ol-BIT-LAr</ta>
            <ta e="T163" id="Seg_3192" s="T162">bu͡o</ta>
            <ta e="T164" id="Seg_3193" s="T163">mm</ta>
            <ta e="T165" id="Seg_3194" s="T164">bettek</ta>
            <ta e="T166" id="Seg_3195" s="T165">kotuː</ta>
            <ta e="T167" id="Seg_3196" s="T166">hir</ta>
            <ta e="T168" id="Seg_3197" s="T167">dek</ta>
            <ta e="T169" id="Seg_3198" s="T168">kel-An</ta>
            <ta e="T170" id="Seg_3199" s="T169">bar-An-LAr</ta>
            <ta e="T182" id="Seg_3200" s="T181">Essej-LAr-I-ŋ</ta>
            <ta e="T183" id="Seg_3201" s="T182">eː</ta>
            <ta e="T184" id="Seg_3202" s="T183">barɨ-LArA</ta>
            <ta e="T185" id="Seg_3203" s="T184">da</ta>
            <ta e="T186" id="Seg_3204" s="T185">bu͡ol-BAT-LAr</ta>
            <ta e="T187" id="Seg_3205" s="T186">baːj</ta>
            <ta e="T188" id="Seg_3206" s="T187">kihi-LAr</ta>
            <ta e="T189" id="Seg_3207" s="T188">baːj</ta>
            <ta e="T190" id="Seg_3208" s="T189">ɨ͡al-LAr</ta>
            <ta e="T191" id="Seg_3209" s="T190">habi͡eskaj</ta>
            <ta e="T192" id="Seg_3210" s="T191">bɨlaːs</ta>
            <ta e="T193" id="Seg_3211" s="T192">tur-Ar-tI-GAr</ta>
            <ta e="T194" id="Seg_3212" s="T193">habi͡eskaj</ta>
            <ta e="T195" id="Seg_3213" s="T194">bɨlaːs-ttAn</ta>
            <ta e="T196" id="Seg_3214" s="T195">küreː-An-LAr</ta>
            <ta e="T197" id="Seg_3215" s="T196">v asnavnom</ta>
            <ta e="T198" id="Seg_3216" s="T197">urut</ta>
            <ta e="T199" id="Seg_3217" s="T198">onton</ta>
            <ta e="T201" id="Seg_3218" s="T199">taːk</ta>
            <ta e="T202" id="Seg_3219" s="T201">atɨː</ta>
            <ta e="T203" id="Seg_3220" s="T202">hɨrɨt-Ar-LAr</ta>
            <ta e="T204" id="Seg_3221" s="T203">bu͡o</ta>
            <ta e="T205" id="Seg_3222" s="T204">ɨ͡allan-A</ta>
            <ta e="T206" id="Seg_3223" s="T205">hɨrɨt-Ar-LAr</ta>
            <ta e="T207" id="Seg_3224" s="T206">horok-LArA</ta>
            <ta e="T208" id="Seg_3225" s="T207">kaːl-Ar-LAr</ta>
            <ta e="T209" id="Seg_3226" s="T208">eː</ta>
            <ta e="T210" id="Seg_3227" s="T209">manna-GI</ta>
            <ta e="T211" id="Seg_3228" s="T210">kihi</ta>
            <ta e="T212" id="Seg_3229" s="T211">Tajmɨr-GA</ta>
            <ta e="T213" id="Seg_3230" s="T212">baːr</ta>
            <ta e="T214" id="Seg_3231" s="T213">kergen</ta>
            <ta e="T215" id="Seg_3232" s="T214">bul-I-n-An-LAr</ta>
            <ta e="T216" id="Seg_3233" s="T215">eŋin</ta>
            <ta e="T217" id="Seg_3234" s="T216">kaːl-Ar-LAr</ta>
            <ta e="T218" id="Seg_3235" s="T217">aː</ta>
            <ta e="T219" id="Seg_3236" s="T218">no</ta>
            <ta e="T220" id="Seg_3237" s="T219">habi͡eskaj</ta>
            <ta e="T221" id="Seg_3238" s="T220">bɨlaːs</ta>
            <ta e="T222" id="Seg_3239" s="T221">tur-IAK.[tI]-r</ta>
            <ta e="T223" id="Seg_3240" s="T222">di͡eri</ta>
            <ta e="T224" id="Seg_3241" s="T223">Essej</ta>
            <ta e="T225" id="Seg_3242" s="T224">haka-LAr-tA</ta>
            <ta e="T226" id="Seg_3243" s="T225">biːr</ta>
            <ta e="T227" id="Seg_3244" s="T226">hir-GA</ta>
            <ta e="T228" id="Seg_3245" s="T227">olor-Ar</ta>
            <ta e="T229" id="Seg_3246" s="T228">e-TI-LAr</ta>
            <ta e="T230" id="Seg_3247" s="T229">onton</ta>
            <ta e="T231" id="Seg_3248" s="T230">habi͡eskaj</ta>
            <ta e="T232" id="Seg_3249" s="T231">bɨlaːs-tI-ttAn</ta>
            <ta e="T233" id="Seg_3250" s="T232">küreː-An</ta>
            <ta e="T234" id="Seg_3251" s="T233">v asnavnom</ta>
            <ta e="T235" id="Seg_3252" s="T234">baːj</ta>
            <ta e="T236" id="Seg_3253" s="T235">ɨ͡al-LAr</ta>
            <ta e="T237" id="Seg_3254" s="T236">bettek</ta>
            <ta e="T238" id="Seg_3255" s="T237">kel-AttAː-BIT-LAr</ta>
            <ta e="T239" id="Seg_3256" s="T238">manna</ta>
            <ta e="T240" id="Seg_3257" s="T239">Tajmɨr-GA</ta>
            <ta e="T241" id="Seg_3258" s="T240">baːr</ta>
            <ta e="T242" id="Seg_3259" s="T241">uruː-LArI-GAr</ta>
            <ta e="T243" id="Seg_3260" s="T242">hugas</ta>
            <ta e="T244" id="Seg_3261" s="T243">könö</ta>
            <ta e="T245" id="Seg_3262" s="T244">bil-Ar</ta>
            <ta e="T246" id="Seg_3263" s="T245">hir-LArI-GAr</ta>
            <ta e="T247" id="Seg_3264" s="T246">eː</ta>
            <ta e="T248" id="Seg_3265" s="T247">kel-BIT-LAr</ta>
            <ta e="T249" id="Seg_3266" s="T248">kös-AttAː-An</ta>
            <ta e="T250" id="Seg_3267" s="T249">onton</ta>
            <ta e="T251" id="Seg_3268" s="T250">hüːrbe</ta>
            <ta e="T252" id="Seg_3269" s="T251">dʼɨl-LAr</ta>
            <ta e="T253" id="Seg_3270" s="T252">büt-Ar-LArI-GAr</ta>
            <ta e="T254" id="Seg_3271" s="T253">otut</ta>
            <ta e="T255" id="Seg_3272" s="T254">dʼɨl-LAr</ta>
            <ta e="T256" id="Seg_3273" s="T255">tagɨs-Ar-LArI-GAr</ta>
            <ta e="T257" id="Seg_3274" s="T256">Tajmɨr-GA</ta>
            <ta e="T258" id="Seg_3275" s="T257">emi͡e</ta>
            <ta e="T259" id="Seg_3276" s="T258">habi͡eskaj</ta>
            <ta e="T260" id="Seg_3277" s="T259">bɨlaːs-tA</ta>
            <ta e="T261" id="Seg_3278" s="T260">palʼitʼika-tA</ta>
            <ta e="T262" id="Seg_3279" s="T261">kɨtaːt-An</ta>
            <ta e="T263" id="Seg_3280" s="T262">is-BIT</ta>
            <ta e="T264" id="Seg_3281" s="T263">emi͡e</ta>
            <ta e="T265" id="Seg_3282" s="T264">dʼe</ta>
            <ta e="T266" id="Seg_3283" s="T265">baːj</ta>
            <ta e="T267" id="Seg_3284" s="T266">kihi-LAr-nI</ta>
            <ta e="T268" id="Seg_3285" s="T267">baːj-LArI-n</ta>
            <ta e="T269" id="Seg_3286" s="T268">tal-An</ta>
            <ta e="T270" id="Seg_3287" s="T269">üllehin-IAK-GA</ta>
            <ta e="T271" id="Seg_3288" s="T270">di͡e-An</ta>
            <ta e="T272" id="Seg_3289" s="T271">bu͡ol-BIT</ta>
            <ta e="T277" id="Seg_3290" s="T276">taba-LArI-n</ta>
            <ta e="T278" id="Seg_3291" s="T277">bɨldʼaː-An</ta>
            <ta e="T279" id="Seg_3292" s="T278">onton</ta>
            <ta e="T280" id="Seg_3293" s="T279">taba-LArI-n</ta>
            <ta e="T281" id="Seg_3294" s="T280">ere</ta>
            <ta e="T282" id="Seg_3295" s="T281">bu͡ol-BAT</ta>
            <ta e="T283" id="Seg_3296" s="T282">dʼi͡e</ta>
            <ta e="T284" id="Seg_3297" s="T283">üp</ta>
            <ta e="T285" id="Seg_3298" s="T284">tüːnün</ta>
            <ta e="T286" id="Seg_3299" s="T285">tal-Ar-LAr</ta>
            <ta e="T287" id="Seg_3300" s="T286">bu͡o</ta>
            <ta e="T288" id="Seg_3301" s="T287">ol-nI</ta>
            <ta e="T289" id="Seg_3302" s="T288">barɨ-tI-n</ta>
            <ta e="T290" id="Seg_3303" s="T289">tüŋet-IAK-GA</ta>
            <ta e="T291" id="Seg_3304" s="T290">naːda</ta>
            <ta e="T292" id="Seg_3305" s="T291">di͡e-An</ta>
            <ta e="T293" id="Seg_3306" s="T292">dʼadaŋɨ-LAr-GA</ta>
            <ta e="T294" id="Seg_3307" s="T293">urut</ta>
            <ta e="T295" id="Seg_3308" s="T294">da</ta>
            <ta e="T296" id="Seg_3309" s="T295">onton</ta>
            <ta e="T297" id="Seg_3310" s="T296">baːj</ta>
            <ta e="T298" id="Seg_3311" s="T297">kihi-ŋ</ta>
            <ta e="T299" id="Seg_3312" s="T298">bɨlɨr-GI</ta>
            <ta e="T300" id="Seg_3313" s="T299">hoku͡on-tI-nAn</ta>
            <ta e="T301" id="Seg_3314" s="T300">kim-nI</ta>
            <ta e="T302" id="Seg_3315" s="T301">da</ta>
            <ta e="T303" id="Seg_3316" s="T302">atagastaː-BAkkA</ta>
            <ta e="T304" id="Seg_3317" s="T303">ülehit-tI-n</ta>
            <ta e="T305" id="Seg_3318" s="T304">da</ta>
            <ta e="T306" id="Seg_3319" s="T305">atagastaː-BAkkA</ta>
            <ta e="T307" id="Seg_3320" s="T306">beje-tI-n</ta>
            <ta e="T308" id="Seg_3321" s="T307">uruː-tI-n</ta>
            <ta e="T309" id="Seg_3322" s="T308">da</ta>
            <ta e="T312" id="Seg_3323" s="T311">dʼadaŋɨ</ta>
            <ta e="T313" id="Seg_3324" s="T312">uruː-tI-n</ta>
            <ta e="T314" id="Seg_3325" s="T313">da</ta>
            <ta e="T315" id="Seg_3326" s="T314">atagastaː-BAkkA</ta>
            <ta e="T316" id="Seg_3327" s="T315">olor-BIT-LAr</ta>
            <ta e="T317" id="Seg_3328" s="T316">bu͡o</ta>
            <ta e="T318" id="Seg_3329" s="T317">obɨčaj-LArA</ta>
            <ta e="T319" id="Seg_3330" s="T318">onnuk</ta>
            <ta e="T320" id="Seg_3331" s="T319">kömölös-An</ta>
            <ta e="T321" id="Seg_3332" s="T320">olor-BIT-LAr</ta>
            <ta e="T322" id="Seg_3333" s="T321">bu͡o</ta>
            <ta e="T323" id="Seg_3334" s="T322">habi͡eskaj</ta>
            <ta e="T324" id="Seg_3335" s="T323">bɨlaːs</ta>
            <ta e="T325" id="Seg_3336" s="T324">palʼitʼika-tI-n</ta>
            <ta e="T326" id="Seg_3337" s="T325">kɨtta</ta>
            <ta e="T327" id="Seg_3338" s="T326">höbüleː-A-s-BAtAK</ta>
            <ta e="T328" id="Seg_3339" s="T327">kihi-LAr-I-ŋ</ta>
            <ta e="T329" id="Seg_3340" s="T328">manna</ta>
            <ta e="T330" id="Seg_3341" s="T329">bunt</ta>
            <ta e="T331" id="Seg_3342" s="T330">tahaːr-BIT-LArA</ta>
            <ta e="T335" id="Seg_3343" s="T334">eː</ta>
            <ta e="T336" id="Seg_3344" s="T335">ajdaːn</ta>
            <ta e="T337" id="Seg_3345" s="T336">tur-BIT</ta>
            <ta e="T338" id="Seg_3346" s="T337">dʼe</ta>
            <ta e="T339" id="Seg_3347" s="T338">onno</ta>
            <ta e="T340" id="Seg_3348" s="T339">ol</ta>
            <ta e="T341" id="Seg_3349" s="T340">ulakan</ta>
            <ta e="T342" id="Seg_3350" s="T341">ajdaːn-GA</ta>
            <ta e="T343" id="Seg_3351" s="T342">kihi-nI</ta>
            <ta e="T344" id="Seg_3352" s="T343">eŋin</ta>
            <ta e="T345" id="Seg_3353" s="T344">ölör-BIT-LAr</ta>
            <ta e="T346" id="Seg_3354" s="T345">manna</ta>
            <ta e="T347" id="Seg_3355" s="T346">tu͡ok-LAr-nI</ta>
            <ta e="T348" id="Seg_3356" s="T347">upalnamočennɨj-LAr-nI</ta>
            <ta e="T351" id="Seg_3357" s="T350">dʼe</ta>
            <ta e="T352" id="Seg_3358" s="T351">ol</ta>
            <ta e="T353" id="Seg_3359" s="T352">tɨ͡a</ta>
            <ta e="T354" id="Seg_3360" s="T353">kihi-tA</ta>
            <ta e="T355" id="Seg_3361" s="T354">ölör-BIT-tA</ta>
            <ta e="T356" id="Seg_3362" s="T355">bil-LIN-I-BAT</ta>
            <ta e="T357" id="Seg_3363" s="T356">ili</ta>
            <ta e="T358" id="Seg_3364" s="T357">že</ta>
            <ta e="T359" id="Seg_3365" s="T358">atɨn</ta>
            <ta e="T360" id="Seg_3366" s="T359">tu͡ok</ta>
            <ta e="T361" id="Seg_3367" s="T360">eme</ta>
            <ta e="T362" id="Seg_3368" s="T361">kihi-tA</ta>
            <ta e="T363" id="Seg_3369" s="T362">anaːn</ta>
            <ta e="T364" id="Seg_3370" s="T363">onnuk</ta>
            <ta e="T365" id="Seg_3371" s="T364">ajdaːn-nI</ta>
            <ta e="T366" id="Seg_3372" s="T365">tahaːr-AːrI</ta>
            <ta e="T367" id="Seg_3373" s="T366">ölör-BIT-tA</ta>
            <ta e="T368" id="Seg_3374" s="T367">bil-LIN-I-BAT</ta>
            <ta e="T369" id="Seg_3375" s="T368">anɨ-GA</ta>
            <ta e="T370" id="Seg_3376" s="T369">da</ta>
            <ta e="T371" id="Seg_3377" s="T370">di͡eri</ta>
            <ta e="T372" id="Seg_3378" s="T371">onno</ta>
            <ta e="T373" id="Seg_3379" s="T372">aktʼivnɨj</ta>
            <ta e="T374" id="Seg_3380" s="T373">dʼe</ta>
            <ta e="T375" id="Seg_3381" s="T374">onton</ta>
            <ta e="T376" id="Seg_3382" s="T375">aristuːj-LAː-BIT-LAr</ta>
            <ta e="T377" id="Seg_3383" s="T376">bu͡o</ta>
            <ta e="T382" id="Seg_3384" s="T381">aːrʼest-nI</ta>
            <ta e="T385" id="Seg_3385" s="T384">di͡e-An</ta>
            <ta e="T386" id="Seg_3386" s="T385">kihi</ta>
            <ta e="T387" id="Seg_3387" s="T386">kel-BIT-LAr</ta>
            <ta e="T388" id="Seg_3388" s="T387">Katanga-ttAn</ta>
            <ta e="T389" id="Seg_3389" s="T388">Jenʼisʼejskʼej-ttAn</ta>
            <ta e="T392" id="Seg_3390" s="T391">nuːčča-LAr</ta>
            <ta e="T393" id="Seg_3391" s="T392">eː</ta>
            <ta e="T394" id="Seg_3392" s="T393">haː-LAːK</ta>
            <ta e="T395" id="Seg_3393" s="T394">dʼe</ta>
            <ta e="T396" id="Seg_3394" s="T395">ol</ta>
            <ta e="T397" id="Seg_3395" s="T396">ol</ta>
            <ta e="T398" id="Seg_3396" s="T397">hurak-ttAn</ta>
            <ta e="T399" id="Seg_3397" s="T398">kihi-LAr-nI</ta>
            <ta e="T400" id="Seg_3398" s="T399">kaːjɨː-GA</ta>
            <ta e="T401" id="Seg_3399" s="T400">olort-Ar-LAr</ta>
            <ta e="T402" id="Seg_3400" s="T401">ühü</ta>
            <ta e="T403" id="Seg_3401" s="T402">di͡e-An</ta>
            <ta e="T404" id="Seg_3402" s="T403">hurak-ttAn</ta>
            <ta e="T405" id="Seg_3403" s="T404">min</ta>
            <ta e="T406" id="Seg_3404" s="T405">inʼe-m</ta>
            <ta e="T407" id="Seg_3405" s="T406">aga-m</ta>
            <ta e="T408" id="Seg_3406" s="T407">ehe-m</ta>
            <ta e="T409" id="Seg_3407" s="T408">bu͡ol-An-LAr</ta>
            <ta e="T410" id="Seg_3408" s="T409">dʼe</ta>
            <ta e="T411" id="Seg_3409" s="T410">küreː-Ar-GA</ta>
            <ta e="T412" id="Seg_3410" s="T411">tur-BIT-LAr</ta>
            <ta e="T413" id="Seg_3411" s="T412">i</ta>
            <ta e="T414" id="Seg_3412" s="T413">töttörü</ta>
            <ta e="T415" id="Seg_3413" s="T414">Essej-LArI-GAr</ta>
            <ta e="T416" id="Seg_3414" s="T415">tönün-I-BIT-LAr</ta>
            <ta e="T417" id="Seg_3415" s="T416">aga-m</ta>
            <ta e="T418" id="Seg_3416" s="T417">bɨraːt-tA</ta>
            <ta e="T419" id="Seg_3417" s="T418">Ignatʼij</ta>
            <ta e="T420" id="Seg_3418" s="T419">di͡e-An</ta>
            <ta e="T421" id="Seg_3419" s="T420">kihi-nI</ta>
            <ta e="T422" id="Seg_3420" s="T421">ɨl-BIT-LAr</ta>
            <ta e="T423" id="Seg_3421" s="T422">kaːjɨː-GA</ta>
            <ta e="T424" id="Seg_3422" s="T423">olort-An</ta>
            <ta e="T425" id="Seg_3423" s="T424">baran</ta>
            <ta e="T426" id="Seg_3424" s="T425">ɨt-BIT</ta>
            <ta e="T427" id="Seg_3425" s="T426">ölör-BIT-LAr</ta>
            <ta e="T428" id="Seg_3426" s="T427">dʼe</ta>
            <ta e="T432" id="Seg_3427" s="T431">eː</ta>
            <ta e="T433" id="Seg_3428" s="T432">Majmago-LAr</ta>
            <ta e="T434" id="Seg_3429" s="T433">aga</ta>
            <ta e="T435" id="Seg_3430" s="T434">onton</ta>
            <ta e="T436" id="Seg_3431" s="T435">aga-m</ta>
            <ta e="T437" id="Seg_3432" s="T436">kɨra</ta>
            <ta e="T438" id="Seg_3433" s="T437">bɨraːt-tA</ta>
            <ta e="T439" id="Seg_3434" s="T438">Danʼil</ta>
            <ta e="T440" id="Seg_3435" s="T439">di͡e-An</ta>
            <ta e="T441" id="Seg_3436" s="T440">kihi</ta>
            <ta e="T444" id="Seg_3437" s="T443">uruː-LArI-GAr</ta>
            <ta e="T445" id="Seg_3438" s="T444">ɨ͡allan-A</ta>
            <ta e="T446" id="Seg_3439" s="T445">bar-BIT</ta>
            <ta e="T447" id="Seg_3440" s="T446">keːs-A</ta>
            <ta e="T448" id="Seg_3441" s="T447">küreː-Ar</ta>
            <ta e="T451" id="Seg_3442" s="T450">küreː-BIT-LAr</ta>
            <ta e="T452" id="Seg_3443" s="T451">bu͡o</ta>
            <ta e="T453" id="Seg_3444" s="T452">ol</ta>
            <ta e="T454" id="Seg_3445" s="T453">ihin</ta>
            <ta e="T455" id="Seg_3446" s="T454">ol-tI-ŋ</ta>
            <ta e="T456" id="Seg_3447" s="T455">ol</ta>
            <ta e="T457" id="Seg_3448" s="T456">ogo-LArI-n</ta>
            <ta e="T458" id="Seg_3449" s="T457">da</ta>
            <ta e="T459" id="Seg_3450" s="T458">keːs-A</ta>
            <ta e="T460" id="Seg_3451" s="T459">bar-BIT-LAr</ta>
            <ta e="T461" id="Seg_3452" s="T460">i</ta>
            <ta e="T462" id="Seg_3453" s="T461">ol</ta>
            <ta e="T463" id="Seg_3454" s="T462">Danʼil</ta>
            <ta e="T464" id="Seg_3455" s="T463">bu͡o</ta>
            <ta e="T465" id="Seg_3456" s="T464">manna</ta>
            <ta e="T466" id="Seg_3457" s="T465">kamkɨ</ta>
            <ta e="T467" id="Seg_3458" s="T466">kɨrɨj-IAK.[tI]-r</ta>
            <ta e="T468" id="Seg_3459" s="T467">di͡eri</ta>
            <ta e="T469" id="Seg_3460" s="T468">Tajmɨr-GA</ta>
            <ta e="T470" id="Seg_3461" s="T469">üleleː-An</ta>
            <ta e="T479" id="Seg_3462" s="T478">Majmago</ta>
            <ta e="T480" id="Seg_3463" s="T479">Danʼil</ta>
            <ta e="T481" id="Seg_3464" s="T480">Stʼepanavʼičʼ</ta>
            <ta e="T482" id="Seg_3465" s="T481">di͡e-An</ta>
            <ta e="T483" id="Seg_3466" s="T482">ol</ta>
            <ta e="T484" id="Seg_3467" s="T483">kihi</ta>
            <ta e="T485" id="Seg_3468" s="T484">taba</ta>
            <ta e="T486" id="Seg_3469" s="T485">iːt-Iː-tI-GAr</ta>
            <ta e="T487" id="Seg_3470" s="T486">kamkɨ</ta>
            <ta e="T488" id="Seg_3471" s="T487">kɨrɨj-IAK.[tI]-r</ta>
            <ta e="T489" id="Seg_3472" s="T488">di͡eri</ta>
            <ta e="T490" id="Seg_3473" s="T489">üleleː-BIT</ta>
            <ta e="T491" id="Seg_3474" s="T490">ogo</ta>
            <ta e="T492" id="Seg_3475" s="T491">haːs-tI-ttAn</ta>
            <ta e="T498" id="Seg_3476" s="T497">Valačanskaj</ta>
            <ta e="T499" id="Seg_3477" s="T498">rajon</ta>
            <ta e="T500" id="Seg_3478" s="T499">Kamenʼ-GA</ta>
            <ta e="T501" id="Seg_3479" s="T500">e-TI-tA</ta>
            <ta e="T502" id="Seg_3480" s="T501">onton</ta>
            <ta e="T503" id="Seg_3481" s="T502">kelin-GI</ta>
            <ta e="T504" id="Seg_3482" s="T503">dʼɨl-LArI-n</ta>
            <ta e="T505" id="Seg_3483" s="T504">Xantajskaj Oːzʼera</ta>
            <ta e="T506" id="Seg_3484" s="T505">di͡e-An</ta>
            <ta e="T507" id="Seg_3485" s="T506">pasʼolak-GA</ta>
            <ta e="T508" id="Seg_3486" s="T507">olor-BIT-tA</ta>
            <ta e="T509" id="Seg_3487" s="T508">onno</ta>
            <ta e="T510" id="Seg_3488" s="T509">da</ta>
            <ta e="T511" id="Seg_3489" s="T510">öl-BIT-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge-MaPX">
            <ta e="T1" id="Seg_3490" s="T0">1SG.[NOM]</ta>
            <ta e="T2" id="Seg_3491" s="T1">Krasnoyarsk.[NOM]</ta>
            <ta e="T3" id="Seg_3492" s="T2">pedagogical</ta>
            <ta e="T4" id="Seg_3493" s="T3">institute-ACC</ta>
            <ta e="T5" id="Seg_3494" s="T4">stop-PST2-EP-1SG</ta>
            <ta e="T6" id="Seg_3495" s="T5">history-ACC</ta>
            <ta e="T7" id="Seg_3496" s="T6">school-DAT/LOC</ta>
            <ta e="T8" id="Seg_3497" s="T7">child-PL-ACC</ta>
            <ta e="T9" id="Seg_3498" s="T8">teach-CVB.SIM</ta>
            <ta e="T10" id="Seg_3499" s="T9">go-EP-PST2-EP-1SG</ta>
            <ta e="T11" id="Seg_3500" s="T10">history</ta>
            <ta e="T12" id="Seg_3501" s="T11">and</ta>
            <ta e="T13" id="Seg_3502" s="T12">social.science</ta>
            <ta e="T14" id="Seg_3503" s="T13">say-CVB.SEQ</ta>
            <ta e="T15" id="Seg_3504" s="T14">subject-PL-ACC</ta>
            <ta e="T40" id="Seg_3505" s="T39">ten</ta>
            <ta e="T41" id="Seg_3506" s="T40">seven</ta>
            <ta e="T42" id="Seg_3507" s="T41">century-PROPR-DAT/LOC</ta>
            <ta e="T43" id="Seg_3508" s="T42">Yakut</ta>
            <ta e="T44" id="Seg_3509" s="T43">earth-3SG-ABL</ta>
            <ta e="T45" id="Seg_3510" s="T44">Lena</ta>
            <ta e="T46" id="Seg_3511" s="T45">middle</ta>
            <ta e="T47" id="Seg_3512" s="T46">Lena-ABL</ta>
            <ta e="T48" id="Seg_3513" s="T47">ten</ta>
            <ta e="T51" id="Seg_3514" s="T50">come-PST2-3PL</ta>
            <ta e="T52" id="Seg_3515" s="T51">front</ta>
            <ta e="T53" id="Seg_3516" s="T52">Yakut-PL.[NOM]</ta>
            <ta e="T54" id="Seg_3517" s="T53">Russian.[NOM]</ta>
            <ta e="T55" id="Seg_3518" s="T54">earth-VBZ-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T56" id="Seg_3519" s="T55">Lena-DAT/LOC</ta>
            <ta e="T57" id="Seg_3520" s="T56">epidemic-PL.[NOM]</ta>
            <ta e="T58" id="Seg_3521" s="T57">become-PST2-3PL</ta>
            <ta e="T59" id="Seg_3522" s="T58">pock</ta>
            <ta e="T60" id="Seg_3523" s="T59">plague</ta>
            <ta e="T61" id="Seg_3524" s="T60">anthrax</ta>
            <ta e="T62" id="Seg_3525" s="T61">say-CVB.SEQ</ta>
            <ta e="T63" id="Seg_3526" s="T62">human.being-PL.[NOM]</ta>
            <ta e="T64" id="Seg_3527" s="T63">dysentery</ta>
            <ta e="T65" id="Seg_3528" s="T64">there</ta>
            <ta e="T66" id="Seg_3529" s="T65">alive.[NOM]</ta>
            <ta e="T67" id="Seg_3530" s="T66">stay-PTCP.PST</ta>
            <ta e="T68" id="Seg_3531" s="T67">human.being-PL-EP-2SG.[NOM]</ta>
            <ta e="T69" id="Seg_3532" s="T68">powerful</ta>
            <ta e="T70" id="Seg_3533" s="T69">earth.[NOM]</ta>
            <ta e="T71" id="Seg_3534" s="T70">to</ta>
            <ta e="T72" id="Seg_3535" s="T71">eh</ta>
            <ta e="T73" id="Seg_3536" s="T72">rich</ta>
            <ta e="T74" id="Seg_3537" s="T73">broad</ta>
            <ta e="T75" id="Seg_3538" s="T74">lake.[NOM]</ta>
            <ta e="T76" id="Seg_3539" s="T75">there.is</ta>
            <ta e="T77" id="Seg_3540" s="T76">say-CVB.SEQ</ta>
            <ta e="T78" id="Seg_3541" s="T77">message-DAT/LOC</ta>
            <ta e="T79" id="Seg_3542" s="T78">come-PST2-3PL</ta>
            <ta e="T80" id="Seg_3543" s="T79">so</ta>
            <ta e="T81" id="Seg_3544" s="T80">Essej</ta>
            <ta e="T82" id="Seg_3545" s="T81">lake.[NOM]</ta>
            <ta e="T83" id="Seg_3546" s="T82">shore-3SG-DAT/LOC</ta>
            <ta e="T84" id="Seg_3547" s="T83">live-CVB.SEQ</ta>
            <ta e="T85" id="Seg_3548" s="T84">that</ta>
            <ta e="T86" id="Seg_3549" s="T85">come-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T87" id="Seg_3550" s="T86">Essej</ta>
            <ta e="T88" id="Seg_3551" s="T87">lake-EP-2SG.[NOM]</ta>
            <ta e="T89" id="Seg_3552" s="T88">shore-PL-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_3553" s="T89">empty.[NOM]</ta>
            <ta e="T91" id="Seg_3554" s="T90">it.is.said</ta>
            <ta e="T92" id="Seg_3555" s="T91">human.being.[NOM]</ta>
            <ta e="T93" id="Seg_3556" s="T92">NEG.EX</ta>
            <ta e="T94" id="Seg_3557" s="T93">pole-PL.[NOM]</ta>
            <ta e="T95" id="Seg_3558" s="T94">just</ta>
            <ta e="T96" id="Seg_3559" s="T95">stand-PTCP.PST</ta>
            <ta e="T97" id="Seg_3560" s="T96">stand-PRS-3PL</ta>
            <ta e="T98" id="Seg_3561" s="T97">tent-PL-EP-2SG.[NOM]</ta>
            <ta e="T99" id="Seg_3562" s="T98">foundation-3PL.[NOM]</ta>
            <ta e="T100" id="Seg_3563" s="T99">rot-PST2-3PL</ta>
            <ta e="T101" id="Seg_3564" s="T100">human.being-PL.[NOM]</ta>
            <ta e="T102" id="Seg_3565" s="T101">bone-3PL.[NOM]</ta>
            <ta e="T103" id="Seg_3566" s="T102">reindeer-PL.[NOM]</ta>
            <ta e="T104" id="Seg_3567" s="T103">dog-PL.[NOM]</ta>
            <ta e="T105" id="Seg_3568" s="T104">bone-3PL.[NOM]</ta>
            <ta e="T106" id="Seg_3569" s="T105">lie-PRS-3PL</ta>
            <ta e="T107" id="Seg_3570" s="T106">it.is.said</ta>
            <ta e="T108" id="Seg_3571" s="T107">around</ta>
            <ta e="T109" id="Seg_3572" s="T108">Yessej-ACC</ta>
            <ta e="T110" id="Seg_3573" s="T109">there</ta>
            <ta e="T111" id="Seg_3574" s="T110">pock-ABL</ta>
            <ta e="T112" id="Seg_3575" s="T111">die-PST2-3PL</ta>
            <ta e="T113" id="Seg_3576" s="T112">Evenki-PL.[NOM]</ta>
            <ta e="T114" id="Seg_3577" s="T113">Nenets-PL.[NOM]</ta>
            <ta e="T119" id="Seg_3578" s="T118">that-PL-ACC</ta>
            <ta e="T120" id="Seg_3579" s="T119">shaman.[NOM]</ta>
            <ta e="T121" id="Seg_3580" s="T120">curse-PTCP.PST</ta>
            <ta e="T122" id="Seg_3581" s="T121">place-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_3582" s="T122">say-CVB.SEQ</ta>
            <ta e="T124" id="Seg_3583" s="T123">escape-EP-CAUS-FREQ-MULT-PST2-3PL</ta>
            <ta e="T125" id="Seg_3584" s="T124">that-ACC</ta>
            <ta e="T126" id="Seg_3585" s="T125">that</ta>
            <ta e="T127" id="Seg_3586" s="T126">empty.[NOM]</ta>
            <ta e="T128" id="Seg_3587" s="T127">place-DAT/LOC</ta>
            <ta e="T129" id="Seg_3588" s="T128">come-CVB.SEQ-3PL</ta>
            <ta e="T130" id="Seg_3589" s="T129">Yakut-PL-EP-2SG.[NOM]</ta>
            <ta e="T131" id="Seg_3590" s="T130">root</ta>
            <ta e="T132" id="Seg_3591" s="T131">earth.[NOM]</ta>
            <ta e="T133" id="Seg_3592" s="T132">make-EP-PST2-3PL</ta>
            <ta e="T134" id="Seg_3593" s="T133">Essej-ACC</ta>
            <ta e="T135" id="Seg_3594" s="T134">then</ta>
            <ta e="T136" id="Seg_3595" s="T135">somewhere</ta>
            <ta e="T137" id="Seg_3596" s="T136">ten</ta>
            <ta e="T138" id="Seg_3597" s="T137">nine</ta>
            <ta e="T139" id="Seg_3598" s="T138">century-DAT/LOC</ta>
            <ta e="T140" id="Seg_3599" s="T139">Taymyr.[NOM]</ta>
            <ta e="T141" id="Seg_3600" s="T140">to</ta>
            <ta e="T142" id="Seg_3601" s="T141">some</ta>
            <ta e="T143" id="Seg_3602" s="T142">human.being-PL.[NOM]</ta>
            <ta e="T144" id="Seg_3603" s="T143">Taymyr.[NOM]</ta>
            <ta e="T145" id="Seg_3604" s="T144">to</ta>
            <ta e="T146" id="Seg_3605" s="T145">already</ta>
            <ta e="T147" id="Seg_3606" s="T146">nomadize-CVB.SIM</ta>
            <ta e="T148" id="Seg_3607" s="T147">go-PTCP.PRS</ta>
            <ta e="T149" id="Seg_3608" s="T148">become-PST2-3PL</ta>
            <ta e="T154" id="Seg_3609" s="T153">reindeer-INSTR</ta>
            <ta e="T155" id="Seg_3610" s="T154">reindeer-INSTR</ta>
            <ta e="T156" id="Seg_3611" s="T155">already</ta>
            <ta e="T157" id="Seg_3612" s="T156">that</ta>
            <ta e="T158" id="Seg_3613" s="T157">reindeer-ACC</ta>
            <ta e="T159" id="Seg_3614" s="T158">already</ta>
            <ta e="T160" id="Seg_3615" s="T159">bring.up-PTCP.PRS</ta>
            <ta e="T161" id="Seg_3616" s="T160">human.being.[NOM]</ta>
            <ta e="T162" id="Seg_3617" s="T161">become-PST2-3PL</ta>
            <ta e="T163" id="Seg_3618" s="T162">EMPH</ta>
            <ta e="T164" id="Seg_3619" s="T163">mm</ta>
            <ta e="T165" id="Seg_3620" s="T164">closer</ta>
            <ta e="T166" id="Seg_3621" s="T165">powerful</ta>
            <ta e="T167" id="Seg_3622" s="T166">earth.[NOM]</ta>
            <ta e="T168" id="Seg_3623" s="T167">to</ta>
            <ta e="T169" id="Seg_3624" s="T168">come-CVB.SEQ</ta>
            <ta e="T170" id="Seg_3625" s="T169">go-CVB.SEQ-3PL</ta>
            <ta e="T182" id="Seg_3626" s="T181">Essej-PL-EP-2SG.[NOM]</ta>
            <ta e="T183" id="Seg_3627" s="T182">eh</ta>
            <ta e="T184" id="Seg_3628" s="T183">every-3PL.[NOM]</ta>
            <ta e="T185" id="Seg_3629" s="T184">NEG</ta>
            <ta e="T186" id="Seg_3630" s="T185">be-NEG-3PL</ta>
            <ta e="T187" id="Seg_3631" s="T186">rich</ta>
            <ta e="T188" id="Seg_3632" s="T187">human.being-PL.[NOM]</ta>
            <ta e="T189" id="Seg_3633" s="T188">rich</ta>
            <ta e="T190" id="Seg_3634" s="T189">family-PL.[NOM]</ta>
            <ta e="T191" id="Seg_3635" s="T190">Soviet</ta>
            <ta e="T192" id="Seg_3636" s="T191">power.[NOM]</ta>
            <ta e="T193" id="Seg_3637" s="T192">stand.up-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T194" id="Seg_3638" s="T193">Soviet</ta>
            <ta e="T195" id="Seg_3639" s="T194">power-ABL</ta>
            <ta e="T196" id="Seg_3640" s="T195">escape-CVB.SEQ-3PL</ta>
            <ta e="T197" id="Seg_3641" s="T196">basically</ta>
            <ta e="T198" id="Seg_3642" s="T197">before</ta>
            <ta e="T199" id="Seg_3643" s="T198">then</ta>
            <ta e="T201" id="Seg_3644" s="T199">so</ta>
            <ta e="T202" id="Seg_3645" s="T201">purchase.[NOM]</ta>
            <ta e="T203" id="Seg_3646" s="T202">go-PRS-3PL</ta>
            <ta e="T204" id="Seg_3647" s="T203">EMPH</ta>
            <ta e="T205" id="Seg_3648" s="T204">visit-CVB.SIM</ta>
            <ta e="T206" id="Seg_3649" s="T205">go-PRS-3PL</ta>
            <ta e="T207" id="Seg_3650" s="T206">some-3PL.[NOM]</ta>
            <ta e="T208" id="Seg_3651" s="T207">stay-PRS-3PL</ta>
            <ta e="T209" id="Seg_3652" s="T208">eh</ta>
            <ta e="T210" id="Seg_3653" s="T209">here-ADJZ</ta>
            <ta e="T211" id="Seg_3654" s="T210">human.being.[NOM]</ta>
            <ta e="T212" id="Seg_3655" s="T211">Taymyr-DAT/LOC</ta>
            <ta e="T213" id="Seg_3656" s="T212">there.is</ta>
            <ta e="T214" id="Seg_3657" s="T213">family.[NOM]</ta>
            <ta e="T215" id="Seg_3658" s="T214">find-EP-MED-CVB.SEQ-3PL</ta>
            <ta e="T216" id="Seg_3659" s="T215">different</ta>
            <ta e="T217" id="Seg_3660" s="T216">stay-PRS-3PL</ta>
            <ta e="T218" id="Seg_3661" s="T217">ah</ta>
            <ta e="T219" id="Seg_3662" s="T218">but</ta>
            <ta e="T220" id="Seg_3663" s="T219">Soviet</ta>
            <ta e="T221" id="Seg_3664" s="T220">power.[NOM]</ta>
            <ta e="T222" id="Seg_3665" s="T221">stand.up-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T223" id="Seg_3666" s="T222">until</ta>
            <ta e="T224" id="Seg_3667" s="T223">Essej.[NOM]</ta>
            <ta e="T225" id="Seg_3668" s="T224">Yakut-PL-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_3669" s="T225">one</ta>
            <ta e="T227" id="Seg_3670" s="T226">place-DAT/LOC</ta>
            <ta e="T228" id="Seg_3671" s="T227">live-PTCP.PRS</ta>
            <ta e="T229" id="Seg_3672" s="T228">be-PST1-3PL</ta>
            <ta e="T230" id="Seg_3673" s="T229">then</ta>
            <ta e="T231" id="Seg_3674" s="T230">Soviet</ta>
            <ta e="T232" id="Seg_3675" s="T231">power-3SG-ABL</ta>
            <ta e="T233" id="Seg_3676" s="T232">escape-CVB.SEQ</ta>
            <ta e="T234" id="Seg_3677" s="T233">basically</ta>
            <ta e="T235" id="Seg_3678" s="T234">rich</ta>
            <ta e="T236" id="Seg_3679" s="T235">family-PL.[NOM]</ta>
            <ta e="T237" id="Seg_3680" s="T236">closer</ta>
            <ta e="T238" id="Seg_3681" s="T237">come-MULT-PST2-3PL</ta>
            <ta e="T239" id="Seg_3682" s="T238">hither</ta>
            <ta e="T240" id="Seg_3683" s="T239">Taymyr-DAT/LOC</ta>
            <ta e="T241" id="Seg_3684" s="T240">there.is</ta>
            <ta e="T242" id="Seg_3685" s="T241">sibling-3PL-DAT/LOC</ta>
            <ta e="T243" id="Seg_3686" s="T242">close.[NOM]</ta>
            <ta e="T244" id="Seg_3687" s="T243">direct</ta>
            <ta e="T245" id="Seg_3688" s="T244">know-PTCP.PRS</ta>
            <ta e="T246" id="Seg_3689" s="T245">place-3PL-DAT/LOC</ta>
            <ta e="T247" id="Seg_3690" s="T246">eh</ta>
            <ta e="T248" id="Seg_3691" s="T247">come-PST2-3PL</ta>
            <ta e="T249" id="Seg_3692" s="T248">start-MULT-CVB.SEQ</ta>
            <ta e="T250" id="Seg_3693" s="T249">then</ta>
            <ta e="T251" id="Seg_3694" s="T250">twenty</ta>
            <ta e="T252" id="Seg_3695" s="T251">year-PL.[NOM]</ta>
            <ta e="T253" id="Seg_3696" s="T252">stop-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T254" id="Seg_3697" s="T253">thirty</ta>
            <ta e="T255" id="Seg_3698" s="T254">year-PL.[NOM]</ta>
            <ta e="T256" id="Seg_3699" s="T255">go.out-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T257" id="Seg_3700" s="T256">Taymyr-DAT/LOC</ta>
            <ta e="T258" id="Seg_3701" s="T257">also</ta>
            <ta e="T259" id="Seg_3702" s="T258">Soviet</ta>
            <ta e="T260" id="Seg_3703" s="T259">power-3SG.[NOM]</ta>
            <ta e="T261" id="Seg_3704" s="T260">policy-3SG.[NOM]</ta>
            <ta e="T262" id="Seg_3705" s="T261">attempt-CVB.SEQ</ta>
            <ta e="T263" id="Seg_3706" s="T262">go-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_3707" s="T263">again</ta>
            <ta e="T265" id="Seg_3708" s="T264">well</ta>
            <ta e="T266" id="Seg_3709" s="T265">rich</ta>
            <ta e="T267" id="Seg_3710" s="T266">human.being-PL-ACC</ta>
            <ta e="T268" id="Seg_3711" s="T267">wealth-3PL-ACC</ta>
            <ta e="T269" id="Seg_3712" s="T268">take.away-CVB.SEQ</ta>
            <ta e="T270" id="Seg_3713" s="T269">divide-PTCP.FUT-DAT/LOC</ta>
            <ta e="T271" id="Seg_3714" s="T270">say-CVB.SEQ</ta>
            <ta e="T272" id="Seg_3715" s="T271">become-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_3716" s="T276">reindeer-3PL-ACC</ta>
            <ta e="T278" id="Seg_3717" s="T277">take.away-CVB.SEQ</ta>
            <ta e="T279" id="Seg_3718" s="T278">then</ta>
            <ta e="T280" id="Seg_3719" s="T279">reindeer-3PL-ACC</ta>
            <ta e="T281" id="Seg_3720" s="T280">just</ta>
            <ta e="T282" id="Seg_3721" s="T281">be-NEG.[3SG]</ta>
            <ta e="T283" id="Seg_3722" s="T282">house.[NOM]</ta>
            <ta e="T284" id="Seg_3723" s="T283">resources.[NOM]</ta>
            <ta e="T285" id="Seg_3724" s="T284">at.night</ta>
            <ta e="T286" id="Seg_3725" s="T285">choose-PRS-3PL</ta>
            <ta e="T287" id="Seg_3726" s="T286">EMPH</ta>
            <ta e="T288" id="Seg_3727" s="T287">that-ACC</ta>
            <ta e="T289" id="Seg_3728" s="T288">whole-3SG-ACC</ta>
            <ta e="T290" id="Seg_3729" s="T289">share-PTCP.FUT-DAT/LOC</ta>
            <ta e="T291" id="Seg_3730" s="T290">need.to</ta>
            <ta e="T292" id="Seg_3731" s="T291">say-CVB.SEQ</ta>
            <ta e="T293" id="Seg_3732" s="T292">poor-PL-DAT/LOC</ta>
            <ta e="T294" id="Seg_3733" s="T293">before</ta>
            <ta e="T295" id="Seg_3734" s="T294">EMPH</ta>
            <ta e="T296" id="Seg_3735" s="T295">then</ta>
            <ta e="T297" id="Seg_3736" s="T296">rich</ta>
            <ta e="T298" id="Seg_3737" s="T297">human.being-2SG.[NOM]</ta>
            <ta e="T299" id="Seg_3738" s="T298">long.ago-ADJZ</ta>
            <ta e="T300" id="Seg_3739" s="T299">law-3SG-INSTR</ta>
            <ta e="T301" id="Seg_3740" s="T300">who-ACC</ta>
            <ta e="T302" id="Seg_3741" s="T301">NEG</ta>
            <ta e="T303" id="Seg_3742" s="T302">offend-NEG.CVB.SIM</ta>
            <ta e="T304" id="Seg_3743" s="T303">worker-3SG-ACC</ta>
            <ta e="T305" id="Seg_3744" s="T304">NEG</ta>
            <ta e="T306" id="Seg_3745" s="T305">offend-NEG.CVB.SIM</ta>
            <ta e="T307" id="Seg_3746" s="T306">self-3SG-GEN</ta>
            <ta e="T308" id="Seg_3747" s="T307">sibling-3SG-ACC</ta>
            <ta e="T309" id="Seg_3748" s="T308">NEG</ta>
            <ta e="T312" id="Seg_3749" s="T311">poor</ta>
            <ta e="T313" id="Seg_3750" s="T312">sibling-3SG-ACC</ta>
            <ta e="T314" id="Seg_3751" s="T313">NEG</ta>
            <ta e="T315" id="Seg_3752" s="T314">offend-NEG.CVB.SIM</ta>
            <ta e="T316" id="Seg_3753" s="T315">live-PST2-3PL</ta>
            <ta e="T317" id="Seg_3754" s="T316">EMPH</ta>
            <ta e="T318" id="Seg_3755" s="T317">custom-3PL.[NOM]</ta>
            <ta e="T319" id="Seg_3756" s="T318">such</ta>
            <ta e="T320" id="Seg_3757" s="T319">help-CVB.SEQ</ta>
            <ta e="T321" id="Seg_3758" s="T320">live-PST2-3PL</ta>
            <ta e="T322" id="Seg_3759" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_3760" s="T322">Soviet</ta>
            <ta e="T324" id="Seg_3761" s="T323">power.[NOM]</ta>
            <ta e="T325" id="Seg_3762" s="T324">policy-3SG-ACC</ta>
            <ta e="T326" id="Seg_3763" s="T325">with</ta>
            <ta e="T327" id="Seg_3764" s="T326">agree-EP-RECP/COLL-NEG.PTCP.PST</ta>
            <ta e="T328" id="Seg_3765" s="T327">human.being-PL-EP-2SG.[NOM]</ta>
            <ta e="T329" id="Seg_3766" s="T328">here</ta>
            <ta e="T330" id="Seg_3767" s="T329">riot.[NOM]</ta>
            <ta e="T331" id="Seg_3768" s="T330">take.out-PST2-3PL</ta>
            <ta e="T335" id="Seg_3769" s="T334">AFFIRM</ta>
            <ta e="T336" id="Seg_3770" s="T335">noise.[NOM]</ta>
            <ta e="T337" id="Seg_3771" s="T336">stand.up-PST2.[3SG]</ta>
            <ta e="T338" id="Seg_3772" s="T337">well</ta>
            <ta e="T339" id="Seg_3773" s="T338">there</ta>
            <ta e="T340" id="Seg_3774" s="T339">that</ta>
            <ta e="T341" id="Seg_3775" s="T340">big</ta>
            <ta e="T342" id="Seg_3776" s="T341">noise-DAT/LOC</ta>
            <ta e="T343" id="Seg_3777" s="T342">human.being-ACC</ta>
            <ta e="T344" id="Seg_3778" s="T343">different</ta>
            <ta e="T345" id="Seg_3779" s="T344">kill-PST2-3PL</ta>
            <ta e="T346" id="Seg_3780" s="T345">here</ta>
            <ta e="T347" id="Seg_3781" s="T346">what-PL-ACC</ta>
            <ta e="T348" id="Seg_3782" s="T347">deputy-PL-ACC</ta>
            <ta e="T351" id="Seg_3783" s="T350">well</ta>
            <ta e="T352" id="Seg_3784" s="T351">that</ta>
            <ta e="T353" id="Seg_3785" s="T352">Dolgan</ta>
            <ta e="T354" id="Seg_3786" s="T353">human.being-3SG.[NOM]</ta>
            <ta e="T355" id="Seg_3787" s="T354">kill-PST2-3SG</ta>
            <ta e="T356" id="Seg_3788" s="T355">know-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T357" id="Seg_3789" s="T356">or</ta>
            <ta e="T358" id="Seg_3790" s="T357">EMPH</ta>
            <ta e="T359" id="Seg_3791" s="T358">different</ta>
            <ta e="T360" id="Seg_3792" s="T359">what.[NOM]</ta>
            <ta e="T361" id="Seg_3793" s="T360">INDEF</ta>
            <ta e="T362" id="Seg_3794" s="T361">human.being-3SG.[NOM]</ta>
            <ta e="T363" id="Seg_3795" s="T362">especially</ta>
            <ta e="T364" id="Seg_3796" s="T363">such</ta>
            <ta e="T365" id="Seg_3797" s="T364">noise-ACC</ta>
            <ta e="T366" id="Seg_3798" s="T365">take.out-CVB.PURP</ta>
            <ta e="T367" id="Seg_3799" s="T366">kill-PST2-3SG</ta>
            <ta e="T368" id="Seg_3800" s="T367">know-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T369" id="Seg_3801" s="T368">now-DAT/LOC</ta>
            <ta e="T370" id="Seg_3802" s="T369">and</ta>
            <ta e="T371" id="Seg_3803" s="T370">until</ta>
            <ta e="T372" id="Seg_3804" s="T371">there</ta>
            <ta e="T373" id="Seg_3805" s="T372">active.[NOM]</ta>
            <ta e="T374" id="Seg_3806" s="T373">well</ta>
            <ta e="T375" id="Seg_3807" s="T374">then</ta>
            <ta e="T376" id="Seg_3808" s="T375">arrest-VBZ-PST2-3PL</ta>
            <ta e="T377" id="Seg_3809" s="T376">EMPH</ta>
            <ta e="T382" id="Seg_3810" s="T381">arrest-ACC</ta>
            <ta e="T385" id="Seg_3811" s="T384">say-CVB.SEQ</ta>
            <ta e="T386" id="Seg_3812" s="T385">human.being.[NOM]</ta>
            <ta e="T387" id="Seg_3813" s="T386">come-PST2-3PL</ta>
            <ta e="T388" id="Seg_3814" s="T387">Khatanga-ABL</ta>
            <ta e="T389" id="Seg_3815" s="T388">Yeniseysk-ABL</ta>
            <ta e="T392" id="Seg_3816" s="T391">Russian-PL.[NOM]</ta>
            <ta e="T393" id="Seg_3817" s="T392">AFFIRM</ta>
            <ta e="T394" id="Seg_3818" s="T393">weapon-PROPR</ta>
            <ta e="T395" id="Seg_3819" s="T394">well</ta>
            <ta e="T396" id="Seg_3820" s="T395">that</ta>
            <ta e="T397" id="Seg_3821" s="T396">that</ta>
            <ta e="T398" id="Seg_3822" s="T397">message-ABL</ta>
            <ta e="T399" id="Seg_3823" s="T398">human.being-PL-ACC</ta>
            <ta e="T400" id="Seg_3824" s="T399">prison-DAT/LOC</ta>
            <ta e="T401" id="Seg_3825" s="T400">seat-PRS-3PL</ta>
            <ta e="T402" id="Seg_3826" s="T401">it.is.said</ta>
            <ta e="T403" id="Seg_3827" s="T402">say-CVB.SEQ</ta>
            <ta e="T404" id="Seg_3828" s="T403">message-ABL</ta>
            <ta e="T405" id="Seg_3829" s="T404">1SG.[NOM]</ta>
            <ta e="T406" id="Seg_3830" s="T405">mother-1SG.[NOM]</ta>
            <ta e="T407" id="Seg_3831" s="T406">father-1SG.[NOM]</ta>
            <ta e="T408" id="Seg_3832" s="T407">grandfather-1SG.[NOM]</ta>
            <ta e="T409" id="Seg_3833" s="T408">be-CVB.SEQ-3PL</ta>
            <ta e="T410" id="Seg_3834" s="T409">well</ta>
            <ta e="T411" id="Seg_3835" s="T410">escape-PTCP.PRS-DAT/LOC</ta>
            <ta e="T412" id="Seg_3836" s="T411">stand-PST2-3PL</ta>
            <ta e="T413" id="Seg_3837" s="T412">and</ta>
            <ta e="T414" id="Seg_3838" s="T413">back</ta>
            <ta e="T415" id="Seg_3839" s="T414">Essej-3PL-DAT/LOC</ta>
            <ta e="T416" id="Seg_3840" s="T415">come.back-EP-PST2-3PL</ta>
            <ta e="T417" id="Seg_3841" s="T416">father-1SG.[NOM]</ta>
            <ta e="T418" id="Seg_3842" s="T417">brother-3SG.[NOM]</ta>
            <ta e="T419" id="Seg_3843" s="T418">Ignatiy</ta>
            <ta e="T420" id="Seg_3844" s="T419">say-CVB.SEQ</ta>
            <ta e="T421" id="Seg_3845" s="T420">human.being-ACC</ta>
            <ta e="T422" id="Seg_3846" s="T421">take-PST2-3PL</ta>
            <ta e="T423" id="Seg_3847" s="T422">prison-DAT/LOC</ta>
            <ta e="T424" id="Seg_3848" s="T423">seat-CVB.SEQ</ta>
            <ta e="T425" id="Seg_3849" s="T424">after</ta>
            <ta e="T426" id="Seg_3850" s="T425">shoot-PST2</ta>
            <ta e="T427" id="Seg_3851" s="T426">kill-PST2-3PL</ta>
            <ta e="T428" id="Seg_3852" s="T427">well</ta>
            <ta e="T432" id="Seg_3853" s="T431">AFFIRM</ta>
            <ta e="T433" id="Seg_3854" s="T432">Majmago-PL.[NOM]</ta>
            <ta e="T434" id="Seg_3855" s="T433">mhm</ta>
            <ta e="T435" id="Seg_3856" s="T434">then</ta>
            <ta e="T436" id="Seg_3857" s="T435">father-1SG.[NOM]</ta>
            <ta e="T437" id="Seg_3858" s="T436">small</ta>
            <ta e="T438" id="Seg_3859" s="T437">brother-3SG.[NOM]</ta>
            <ta e="T439" id="Seg_3860" s="T438">Daniel</ta>
            <ta e="T440" id="Seg_3861" s="T439">say-CVB.SEQ</ta>
            <ta e="T441" id="Seg_3862" s="T440">human.being.[NOM]</ta>
            <ta e="T444" id="Seg_3863" s="T443">sibling-3PL-DAT/LOC</ta>
            <ta e="T445" id="Seg_3864" s="T444">visit-CVB.SIM</ta>
            <ta e="T446" id="Seg_3865" s="T445">go-PST2.[3SG]</ta>
            <ta e="T447" id="Seg_3866" s="T446">let-CVB.SIM</ta>
            <ta e="T448" id="Seg_3867" s="T447">escape-PTCP.PRS</ta>
            <ta e="T451" id="Seg_3868" s="T450">escape-PST2-3PL</ta>
            <ta e="T452" id="Seg_3869" s="T451">EMPH</ta>
            <ta e="T453" id="Seg_3870" s="T452">that.[NOM]</ta>
            <ta e="T454" id="Seg_3871" s="T453">because.of</ta>
            <ta e="T455" id="Seg_3872" s="T454">that-3SG-2SG.[NOM]</ta>
            <ta e="T456" id="Seg_3873" s="T455">that</ta>
            <ta e="T457" id="Seg_3874" s="T456">child-3PL-ACC</ta>
            <ta e="T458" id="Seg_3875" s="T457">and</ta>
            <ta e="T459" id="Seg_3876" s="T458">let-CVB.SIM</ta>
            <ta e="T460" id="Seg_3877" s="T459">go-PST2-3PL</ta>
            <ta e="T461" id="Seg_3878" s="T460">and</ta>
            <ta e="T462" id="Seg_3879" s="T461">that</ta>
            <ta e="T463" id="Seg_3880" s="T462">Daniel.[NOM]</ta>
            <ta e="T464" id="Seg_3881" s="T463">EMPH</ta>
            <ta e="T465" id="Seg_3882" s="T464">here</ta>
            <ta e="T466" id="Seg_3883" s="T465">firmly</ta>
            <ta e="T467" id="Seg_3884" s="T466">age-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T468" id="Seg_3885" s="T467">until</ta>
            <ta e="T469" id="Seg_3886" s="T468">Taymyr-DAT/LOC</ta>
            <ta e="T470" id="Seg_3887" s="T469">work-CVB.SEQ</ta>
            <ta e="T479" id="Seg_3888" s="T478">Majmago</ta>
            <ta e="T480" id="Seg_3889" s="T479">Daniel</ta>
            <ta e="T481" id="Seg_3890" s="T480">Stepanovich</ta>
            <ta e="T482" id="Seg_3891" s="T481">say-CVB.SEQ</ta>
            <ta e="T483" id="Seg_3892" s="T482">that</ta>
            <ta e="T484" id="Seg_3893" s="T483">human.being.[NOM]</ta>
            <ta e="T485" id="Seg_3894" s="T484">reindeer.[NOM]</ta>
            <ta e="T486" id="Seg_3895" s="T485">bring.up-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T487" id="Seg_3896" s="T486">firmly</ta>
            <ta e="T488" id="Seg_3897" s="T487">age-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T489" id="Seg_3898" s="T488">until</ta>
            <ta e="T490" id="Seg_3899" s="T489">work-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_3900" s="T490">child.[NOM]</ta>
            <ta e="T492" id="Seg_3901" s="T491">age-3SG-ABL</ta>
            <ta e="T498" id="Seg_3902" s="T497">Volochanka</ta>
            <ta e="T499" id="Seg_3903" s="T498">rayon.[NOM]</ta>
            <ta e="T500" id="Seg_3904" s="T499">Kamen-DAT/LOC</ta>
            <ta e="T501" id="Seg_3905" s="T500">be-PST1-3SG</ta>
            <ta e="T502" id="Seg_3906" s="T501">then</ta>
            <ta e="T503" id="Seg_3907" s="T502">back-ADJZ</ta>
            <ta e="T504" id="Seg_3908" s="T503">year-3PL-ACC</ta>
            <ta e="T505" id="Seg_3909" s="T504">Khantayskoe.Ozero</ta>
            <ta e="T506" id="Seg_3910" s="T505">say-CVB.SEQ</ta>
            <ta e="T507" id="Seg_3911" s="T506">village-DAT/LOC</ta>
            <ta e="T508" id="Seg_3912" s="T507">live-PST2-3SG</ta>
            <ta e="T509" id="Seg_3913" s="T508">there</ta>
            <ta e="T510" id="Seg_3914" s="T509">and</ta>
            <ta e="T511" id="Seg_3915" s="T510">die-PST2-3SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg-MaPX">
            <ta e="T1" id="Seg_3916" s="T0">1SG.[NOM]</ta>
            <ta e="T2" id="Seg_3917" s="T1">Krasnojarsk.[NOM]</ta>
            <ta e="T3" id="Seg_3918" s="T2">pädagogisch</ta>
            <ta e="T4" id="Seg_3919" s="T3">Institut-ACC</ta>
            <ta e="T5" id="Seg_3920" s="T4">aufhören-PST2-EP-1SG</ta>
            <ta e="T6" id="Seg_3921" s="T5">Geschichte-ACC</ta>
            <ta e="T7" id="Seg_3922" s="T6">Schule-DAT/LOC</ta>
            <ta e="T8" id="Seg_3923" s="T7">Kind-PL-ACC</ta>
            <ta e="T9" id="Seg_3924" s="T8">lehren-CVB.SIM</ta>
            <ta e="T10" id="Seg_3925" s="T9">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T11" id="Seg_3926" s="T10">Geschichte</ta>
            <ta e="T12" id="Seg_3927" s="T11">und</ta>
            <ta e="T13" id="Seg_3928" s="T12">Gesellschaftswissenschaft</ta>
            <ta e="T14" id="Seg_3929" s="T13">sagen-CVB.SEQ</ta>
            <ta e="T15" id="Seg_3930" s="T14">Fach-PL-ACC</ta>
            <ta e="T40" id="Seg_3931" s="T39">zehn</ta>
            <ta e="T41" id="Seg_3932" s="T40">sieben</ta>
            <ta e="T42" id="Seg_3933" s="T41">Jahrhundert-PROPR-DAT/LOC</ta>
            <ta e="T43" id="Seg_3934" s="T42">jakutisch</ta>
            <ta e="T44" id="Seg_3935" s="T43">Erde-3SG-ABL</ta>
            <ta e="T45" id="Seg_3936" s="T44">Lena</ta>
            <ta e="T46" id="Seg_3937" s="T45">mittlerer</ta>
            <ta e="T47" id="Seg_3938" s="T46">Lena-ABL</ta>
            <ta e="T48" id="Seg_3939" s="T47">zehn</ta>
            <ta e="T51" id="Seg_3940" s="T50">kommen-PST2-3PL</ta>
            <ta e="T52" id="Seg_3941" s="T51">vorderster</ta>
            <ta e="T53" id="Seg_3942" s="T52">Jakute-PL.[NOM]</ta>
            <ta e="T54" id="Seg_3943" s="T53">Russe.[NOM]</ta>
            <ta e="T55" id="Seg_3944" s="T54">Erde-VBZ-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T56" id="Seg_3945" s="T55">Lena-DAT/LOC</ta>
            <ta e="T57" id="Seg_3946" s="T56">Epidemie-PL.[NOM]</ta>
            <ta e="T58" id="Seg_3947" s="T57">werden-PST2-3PL</ta>
            <ta e="T59" id="Seg_3948" s="T58">Pocke</ta>
            <ta e="T60" id="Seg_3949" s="T59">Pest</ta>
            <ta e="T61" id="Seg_3950" s="T60">Milzbrand</ta>
            <ta e="T62" id="Seg_3951" s="T61">sagen-CVB.SEQ</ta>
            <ta e="T63" id="Seg_3952" s="T62">Mensch-PL.[NOM]</ta>
            <ta e="T64" id="Seg_3953" s="T63">Ruhr</ta>
            <ta e="T65" id="Seg_3954" s="T64">dort</ta>
            <ta e="T66" id="Seg_3955" s="T65">lebendig.[NOM]</ta>
            <ta e="T67" id="Seg_3956" s="T66">bleiben-PTCP.PST</ta>
            <ta e="T68" id="Seg_3957" s="T67">Mensch-PL-EP-2SG.[NOM]</ta>
            <ta e="T69" id="Seg_3958" s="T68">stark</ta>
            <ta e="T70" id="Seg_3959" s="T69">Erde.[NOM]</ta>
            <ta e="T71" id="Seg_3960" s="T70">zu</ta>
            <ta e="T72" id="Seg_3961" s="T71">äh</ta>
            <ta e="T73" id="Seg_3962" s="T72">reich</ta>
            <ta e="T74" id="Seg_3963" s="T73">breit</ta>
            <ta e="T75" id="Seg_3964" s="T74">See.[NOM]</ta>
            <ta e="T76" id="Seg_3965" s="T75">es.gibt</ta>
            <ta e="T77" id="Seg_3966" s="T76">sagen-CVB.SEQ</ta>
            <ta e="T78" id="Seg_3967" s="T77">Nachricht-DAT/LOC</ta>
            <ta e="T79" id="Seg_3968" s="T78">kommen-PST2-3PL</ta>
            <ta e="T80" id="Seg_3969" s="T79">so</ta>
            <ta e="T81" id="Seg_3970" s="T80">Essej</ta>
            <ta e="T82" id="Seg_3971" s="T81">See.[NOM]</ta>
            <ta e="T83" id="Seg_3972" s="T82">Ufer-3SG-DAT/LOC</ta>
            <ta e="T84" id="Seg_3973" s="T83">leben-CVB.SEQ</ta>
            <ta e="T85" id="Seg_3974" s="T84">jenes</ta>
            <ta e="T86" id="Seg_3975" s="T85">kommen-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T87" id="Seg_3976" s="T86">Essej</ta>
            <ta e="T88" id="Seg_3977" s="T87">See-EP-2SG.[NOM]</ta>
            <ta e="T89" id="Seg_3978" s="T88">Ufer-PL-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_3979" s="T89">leer.[NOM]</ta>
            <ta e="T91" id="Seg_3980" s="T90">man.sagt</ta>
            <ta e="T92" id="Seg_3981" s="T91">Mensch.[NOM]</ta>
            <ta e="T93" id="Seg_3982" s="T92">NEG.EX</ta>
            <ta e="T94" id="Seg_3983" s="T93">Stange-PL.[NOM]</ta>
            <ta e="T95" id="Seg_3984" s="T94">nur</ta>
            <ta e="T96" id="Seg_3985" s="T95">stehen-PTCP.PST</ta>
            <ta e="T97" id="Seg_3986" s="T96">stehen-PRS-3PL</ta>
            <ta e="T98" id="Seg_3987" s="T97">Zelt-PL-EP-2SG.[NOM]</ta>
            <ta e="T99" id="Seg_3988" s="T98">Fundament-3PL.[NOM]</ta>
            <ta e="T100" id="Seg_3989" s="T99">faulen-PST2-3PL</ta>
            <ta e="T101" id="Seg_3990" s="T100">Mensch-PL.[NOM]</ta>
            <ta e="T102" id="Seg_3991" s="T101">Knochen-3PL.[NOM]</ta>
            <ta e="T103" id="Seg_3992" s="T102">Rentier-PL.[NOM]</ta>
            <ta e="T104" id="Seg_3993" s="T103">Hund-PL.[NOM]</ta>
            <ta e="T105" id="Seg_3994" s="T104">Knochen-3PL.[NOM]</ta>
            <ta e="T106" id="Seg_3995" s="T105">liegen-PRS-3PL</ta>
            <ta e="T107" id="Seg_3996" s="T106">man.sagt</ta>
            <ta e="T108" id="Seg_3997" s="T107">um.herum</ta>
            <ta e="T109" id="Seg_3998" s="T108">Jessej-ACC</ta>
            <ta e="T110" id="Seg_3999" s="T109">dort</ta>
            <ta e="T111" id="Seg_4000" s="T110">Pocke-ABL</ta>
            <ta e="T112" id="Seg_4001" s="T111">sterben-PST2-3PL</ta>
            <ta e="T113" id="Seg_4002" s="T112">Ewenke-PL.[NOM]</ta>
            <ta e="T114" id="Seg_4003" s="T113">Nenze-PL.[NOM]</ta>
            <ta e="T119" id="Seg_4004" s="T118">jenes-PL-ACC</ta>
            <ta e="T120" id="Seg_4005" s="T119">Schamane.[NOM]</ta>
            <ta e="T121" id="Seg_4006" s="T120">verfluchen-PTCP.PST</ta>
            <ta e="T122" id="Seg_4007" s="T121">Ort-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_4008" s="T122">sagen-CVB.SEQ</ta>
            <ta e="T124" id="Seg_4009" s="T123">entfliehen-EP-CAUS-FREQ-MULT-PST2-3PL</ta>
            <ta e="T125" id="Seg_4010" s="T124">jenes-ACC</ta>
            <ta e="T126" id="Seg_4011" s="T125">jenes</ta>
            <ta e="T127" id="Seg_4012" s="T126">leer.[NOM]</ta>
            <ta e="T128" id="Seg_4013" s="T127">Ort-DAT/LOC</ta>
            <ta e="T129" id="Seg_4014" s="T128">kommen-CVB.SEQ-3PL</ta>
            <ta e="T130" id="Seg_4015" s="T129">Jakute-PL-EP-2SG.[NOM]</ta>
            <ta e="T131" id="Seg_4016" s="T130">Wurzel</ta>
            <ta e="T132" id="Seg_4017" s="T131">Erde.[NOM]</ta>
            <ta e="T133" id="Seg_4018" s="T132">machen-EP-PST2-3PL</ta>
            <ta e="T134" id="Seg_4019" s="T133">Essej-ACC</ta>
            <ta e="T135" id="Seg_4020" s="T134">dann</ta>
            <ta e="T136" id="Seg_4021" s="T135">irgendwo</ta>
            <ta e="T137" id="Seg_4022" s="T136">zehn</ta>
            <ta e="T138" id="Seg_4023" s="T137">neun</ta>
            <ta e="T139" id="Seg_4024" s="T138">Jahrhundert-DAT/LOC</ta>
            <ta e="T140" id="Seg_4025" s="T139">Taimyr.[NOM]</ta>
            <ta e="T141" id="Seg_4026" s="T140">zu</ta>
            <ta e="T142" id="Seg_4027" s="T141">mancher</ta>
            <ta e="T143" id="Seg_4028" s="T142">Mensch-PL.[NOM]</ta>
            <ta e="T144" id="Seg_4029" s="T143">Taimyr.[NOM]</ta>
            <ta e="T145" id="Seg_4030" s="T144">zu</ta>
            <ta e="T146" id="Seg_4031" s="T145">schon</ta>
            <ta e="T147" id="Seg_4032" s="T146">nomadisieren-CVB.SIM</ta>
            <ta e="T148" id="Seg_4033" s="T147">gehen-PTCP.PRS</ta>
            <ta e="T149" id="Seg_4034" s="T148">werden-PST2-3PL</ta>
            <ta e="T154" id="Seg_4035" s="T153">Rentier-INSTR</ta>
            <ta e="T155" id="Seg_4036" s="T154">Rentier-INSTR</ta>
            <ta e="T156" id="Seg_4037" s="T155">schon</ta>
            <ta e="T157" id="Seg_4038" s="T156">jenes</ta>
            <ta e="T158" id="Seg_4039" s="T157">Rentier-ACC</ta>
            <ta e="T159" id="Seg_4040" s="T158">schon</ta>
            <ta e="T160" id="Seg_4041" s="T159">aufziehen-PTCP.PRS</ta>
            <ta e="T161" id="Seg_4042" s="T160">Mensch.[NOM]</ta>
            <ta e="T162" id="Seg_4043" s="T161">werden-PST2-3PL</ta>
            <ta e="T163" id="Seg_4044" s="T162">EMPH</ta>
            <ta e="T164" id="Seg_4045" s="T163">mm</ta>
            <ta e="T165" id="Seg_4046" s="T164">näher</ta>
            <ta e="T166" id="Seg_4047" s="T165">stark</ta>
            <ta e="T167" id="Seg_4048" s="T166">Erde.[NOM]</ta>
            <ta e="T168" id="Seg_4049" s="T167">zu</ta>
            <ta e="T169" id="Seg_4050" s="T168">kommen-CVB.SEQ</ta>
            <ta e="T170" id="Seg_4051" s="T169">gehen-CVB.SEQ-3PL</ta>
            <ta e="T182" id="Seg_4052" s="T181">Essej-PL-EP-2SG.[NOM]</ta>
            <ta e="T183" id="Seg_4053" s="T182">äh</ta>
            <ta e="T184" id="Seg_4054" s="T183">jeder-3PL.[NOM]</ta>
            <ta e="T185" id="Seg_4055" s="T184">NEG</ta>
            <ta e="T186" id="Seg_4056" s="T185">sein-NEG-3PL</ta>
            <ta e="T187" id="Seg_4057" s="T186">reich</ta>
            <ta e="T188" id="Seg_4058" s="T187">Mensch-PL.[NOM]</ta>
            <ta e="T189" id="Seg_4059" s="T188">reich</ta>
            <ta e="T190" id="Seg_4060" s="T189">Familie-PL.[NOM]</ta>
            <ta e="T191" id="Seg_4061" s="T190">sowjetisch</ta>
            <ta e="T192" id="Seg_4062" s="T191">Macht.[NOM]</ta>
            <ta e="T193" id="Seg_4063" s="T192">aufstehen-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T194" id="Seg_4064" s="T193">sowjetisch</ta>
            <ta e="T195" id="Seg_4065" s="T194">Macht-ABL</ta>
            <ta e="T196" id="Seg_4066" s="T195">entfliehen-CVB.SEQ-3PL</ta>
            <ta e="T197" id="Seg_4067" s="T196">im.Grunde</ta>
            <ta e="T198" id="Seg_4068" s="T197">früher</ta>
            <ta e="T199" id="Seg_4069" s="T198">dann</ta>
            <ta e="T201" id="Seg_4070" s="T199">so</ta>
            <ta e="T202" id="Seg_4071" s="T201">Kauf.[NOM]</ta>
            <ta e="T203" id="Seg_4072" s="T202">gehen-PRS-3PL</ta>
            <ta e="T204" id="Seg_4073" s="T203">EMPH</ta>
            <ta e="T205" id="Seg_4074" s="T204">besuchen-CVB.SIM</ta>
            <ta e="T206" id="Seg_4075" s="T205">gehen-PRS-3PL</ta>
            <ta e="T207" id="Seg_4076" s="T206">mancher-3PL.[NOM]</ta>
            <ta e="T208" id="Seg_4077" s="T207">bleiben-PRS-3PL</ta>
            <ta e="T209" id="Seg_4078" s="T208">äh</ta>
            <ta e="T210" id="Seg_4079" s="T209">hier-ADJZ</ta>
            <ta e="T211" id="Seg_4080" s="T210">Mensch.[NOM]</ta>
            <ta e="T212" id="Seg_4081" s="T211">Taimyr-DAT/LOC</ta>
            <ta e="T213" id="Seg_4082" s="T212">es.gibt</ta>
            <ta e="T214" id="Seg_4083" s="T213">Familie.[NOM]</ta>
            <ta e="T215" id="Seg_4084" s="T214">finden-EP-MED-CVB.SEQ-3PL</ta>
            <ta e="T216" id="Seg_4085" s="T215">unterschiedlich</ta>
            <ta e="T217" id="Seg_4086" s="T216">bleiben-PRS-3PL</ta>
            <ta e="T218" id="Seg_4087" s="T217">ah</ta>
            <ta e="T219" id="Seg_4088" s="T218">aber</ta>
            <ta e="T220" id="Seg_4089" s="T219">sowjetisch</ta>
            <ta e="T221" id="Seg_4090" s="T220">Macht.[NOM]</ta>
            <ta e="T222" id="Seg_4091" s="T221">aufstehen-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T223" id="Seg_4092" s="T222">bis.zu</ta>
            <ta e="T224" id="Seg_4093" s="T223">Essej.[NOM]</ta>
            <ta e="T225" id="Seg_4094" s="T224">Jakute-PL-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_4095" s="T225">eins</ta>
            <ta e="T227" id="Seg_4096" s="T226">Ort-DAT/LOC</ta>
            <ta e="T228" id="Seg_4097" s="T227">leben-PTCP.PRS</ta>
            <ta e="T229" id="Seg_4098" s="T228">sein-PST1-3PL</ta>
            <ta e="T230" id="Seg_4099" s="T229">dann</ta>
            <ta e="T231" id="Seg_4100" s="T230">sowjetisch</ta>
            <ta e="T232" id="Seg_4101" s="T231">Macht-3SG-ABL</ta>
            <ta e="T233" id="Seg_4102" s="T232">entfliehen-CVB.SEQ</ta>
            <ta e="T234" id="Seg_4103" s="T233">im.Grunde</ta>
            <ta e="T235" id="Seg_4104" s="T234">reich</ta>
            <ta e="T236" id="Seg_4105" s="T235">Familie-PL.[NOM]</ta>
            <ta e="T237" id="Seg_4106" s="T236">näher</ta>
            <ta e="T238" id="Seg_4107" s="T237">kommen-MULT-PST2-3PL</ta>
            <ta e="T239" id="Seg_4108" s="T238">hierher</ta>
            <ta e="T240" id="Seg_4109" s="T239">Taimyr-DAT/LOC</ta>
            <ta e="T241" id="Seg_4110" s="T240">es.gibt</ta>
            <ta e="T242" id="Seg_4111" s="T241">Verwandter-3PL-DAT/LOC</ta>
            <ta e="T243" id="Seg_4112" s="T242">nah.[NOM]</ta>
            <ta e="T244" id="Seg_4113" s="T243">direkt</ta>
            <ta e="T245" id="Seg_4114" s="T244">wissen-PTCP.PRS</ta>
            <ta e="T246" id="Seg_4115" s="T245">Ort-3PL-DAT/LOC</ta>
            <ta e="T247" id="Seg_4116" s="T246">äh</ta>
            <ta e="T248" id="Seg_4117" s="T247">kommen-PST2-3PL</ta>
            <ta e="T249" id="Seg_4118" s="T248">wegfahren-MULT-CVB.SEQ</ta>
            <ta e="T250" id="Seg_4119" s="T249">dann</ta>
            <ta e="T251" id="Seg_4120" s="T250">zwanzig</ta>
            <ta e="T252" id="Seg_4121" s="T251">Jahr-PL.[NOM]</ta>
            <ta e="T253" id="Seg_4122" s="T252">aufhören-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T254" id="Seg_4123" s="T253">dreißig</ta>
            <ta e="T255" id="Seg_4124" s="T254">Jahr-PL.[NOM]</ta>
            <ta e="T256" id="Seg_4125" s="T255">hinausgehen-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T257" id="Seg_4126" s="T256">Taimyr-DAT/LOC</ta>
            <ta e="T258" id="Seg_4127" s="T257">auch</ta>
            <ta e="T259" id="Seg_4128" s="T258">sowjetisch</ta>
            <ta e="T260" id="Seg_4129" s="T259">Macht-3SG.[NOM]</ta>
            <ta e="T261" id="Seg_4130" s="T260">Politik-3SG.[NOM]</ta>
            <ta e="T262" id="Seg_4131" s="T261">sich.bemühen-CVB.SEQ</ta>
            <ta e="T263" id="Seg_4132" s="T262">gehen-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_4133" s="T263">wieder</ta>
            <ta e="T265" id="Seg_4134" s="T264">doch</ta>
            <ta e="T266" id="Seg_4135" s="T265">reich</ta>
            <ta e="T267" id="Seg_4136" s="T266">Mensch-PL-ACC</ta>
            <ta e="T268" id="Seg_4137" s="T267">Reichtum-3PL-ACC</ta>
            <ta e="T269" id="Seg_4138" s="T268">wegnehmen-CVB.SEQ</ta>
            <ta e="T270" id="Seg_4139" s="T269">aufteilen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T271" id="Seg_4140" s="T270">sagen-CVB.SEQ</ta>
            <ta e="T272" id="Seg_4141" s="T271">werden-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_4142" s="T276">Rentier-3PL-ACC</ta>
            <ta e="T278" id="Seg_4143" s="T277">wegnehmen-CVB.SEQ</ta>
            <ta e="T279" id="Seg_4144" s="T278">dann</ta>
            <ta e="T280" id="Seg_4145" s="T279">Rentier-3PL-ACC</ta>
            <ta e="T281" id="Seg_4146" s="T280">nur</ta>
            <ta e="T282" id="Seg_4147" s="T281">sein-NEG.[3SG]</ta>
            <ta e="T283" id="Seg_4148" s="T282">Haus.[NOM]</ta>
            <ta e="T284" id="Seg_4149" s="T283">Mittel.[NOM]</ta>
            <ta e="T285" id="Seg_4150" s="T284">nachts</ta>
            <ta e="T286" id="Seg_4151" s="T285">wählen-PRS-3PL</ta>
            <ta e="T287" id="Seg_4152" s="T286">EMPH</ta>
            <ta e="T288" id="Seg_4153" s="T287">jenes-ACC</ta>
            <ta e="T289" id="Seg_4154" s="T288">ganz-3SG-ACC</ta>
            <ta e="T290" id="Seg_4155" s="T289">teilen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T291" id="Seg_4156" s="T290">man.muss</ta>
            <ta e="T292" id="Seg_4157" s="T291">sagen-CVB.SEQ</ta>
            <ta e="T293" id="Seg_4158" s="T292">arm-PL-DAT/LOC</ta>
            <ta e="T294" id="Seg_4159" s="T293">früher</ta>
            <ta e="T295" id="Seg_4160" s="T294">EMPH</ta>
            <ta e="T296" id="Seg_4161" s="T295">dann</ta>
            <ta e="T297" id="Seg_4162" s="T296">reich</ta>
            <ta e="T298" id="Seg_4163" s="T297">Mensch-2SG.[NOM]</ta>
            <ta e="T299" id="Seg_4164" s="T298">vor.langer.Zeit-ADJZ</ta>
            <ta e="T300" id="Seg_4165" s="T299">Gesetz-3SG-INSTR</ta>
            <ta e="T301" id="Seg_4166" s="T300">wer-ACC</ta>
            <ta e="T302" id="Seg_4167" s="T301">NEG</ta>
            <ta e="T303" id="Seg_4168" s="T302">kränken-NEG.CVB.SIM</ta>
            <ta e="T304" id="Seg_4169" s="T303">Arbeiter-3SG-ACC</ta>
            <ta e="T305" id="Seg_4170" s="T304">NEG</ta>
            <ta e="T306" id="Seg_4171" s="T305">kränken-NEG.CVB.SIM</ta>
            <ta e="T307" id="Seg_4172" s="T306">selbst-3SG-GEN</ta>
            <ta e="T308" id="Seg_4173" s="T307">Verwandter-3SG-ACC</ta>
            <ta e="T309" id="Seg_4174" s="T308">NEG</ta>
            <ta e="T312" id="Seg_4175" s="T311">arm</ta>
            <ta e="T313" id="Seg_4176" s="T312">Verwandter-3SG-ACC</ta>
            <ta e="T314" id="Seg_4177" s="T313">NEG</ta>
            <ta e="T315" id="Seg_4178" s="T314">kränken-NEG.CVB.SIM</ta>
            <ta e="T316" id="Seg_4179" s="T315">leben-PST2-3PL</ta>
            <ta e="T317" id="Seg_4180" s="T316">EMPH</ta>
            <ta e="T318" id="Seg_4181" s="T317">Gebrauch-3PL.[NOM]</ta>
            <ta e="T319" id="Seg_4182" s="T318">solch</ta>
            <ta e="T320" id="Seg_4183" s="T319">helfen-CVB.SEQ</ta>
            <ta e="T321" id="Seg_4184" s="T320">leben-PST2-3PL</ta>
            <ta e="T322" id="Seg_4185" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_4186" s="T322">sowjetisch</ta>
            <ta e="T324" id="Seg_4187" s="T323">Macht.[NOM]</ta>
            <ta e="T325" id="Seg_4188" s="T324">Politik-3SG-ACC</ta>
            <ta e="T326" id="Seg_4189" s="T325">mit</ta>
            <ta e="T327" id="Seg_4190" s="T326">einverstanden.sein-EP-RECP/COLL-NEG.PTCP.PST</ta>
            <ta e="T328" id="Seg_4191" s="T327">Mensch-PL-EP-2SG.[NOM]</ta>
            <ta e="T329" id="Seg_4192" s="T328">hier</ta>
            <ta e="T330" id="Seg_4193" s="T329">Aufstand.[NOM]</ta>
            <ta e="T331" id="Seg_4194" s="T330">herausnehmen-PST2-3PL</ta>
            <ta e="T335" id="Seg_4195" s="T334">AFFIRM</ta>
            <ta e="T336" id="Seg_4196" s="T335">Lärm.[NOM]</ta>
            <ta e="T337" id="Seg_4197" s="T336">aufstehen-PST2.[3SG]</ta>
            <ta e="T338" id="Seg_4198" s="T337">doch</ta>
            <ta e="T339" id="Seg_4199" s="T338">dort</ta>
            <ta e="T340" id="Seg_4200" s="T339">jenes</ta>
            <ta e="T341" id="Seg_4201" s="T340">groß</ta>
            <ta e="T342" id="Seg_4202" s="T341">Lärm-DAT/LOC</ta>
            <ta e="T343" id="Seg_4203" s="T342">Mensch-ACC</ta>
            <ta e="T344" id="Seg_4204" s="T343">unterschiedlich</ta>
            <ta e="T345" id="Seg_4205" s="T344">töten-PST2-3PL</ta>
            <ta e="T346" id="Seg_4206" s="T345">hier</ta>
            <ta e="T347" id="Seg_4207" s="T346">was-PL-ACC</ta>
            <ta e="T348" id="Seg_4208" s="T347">Vertreter-PL-ACC</ta>
            <ta e="T351" id="Seg_4209" s="T350">doch</ta>
            <ta e="T352" id="Seg_4210" s="T351">jenes</ta>
            <ta e="T353" id="Seg_4211" s="T352">dolganisch</ta>
            <ta e="T354" id="Seg_4212" s="T353">Mensch-3SG.[NOM]</ta>
            <ta e="T355" id="Seg_4213" s="T354">töten-PST2-3SG</ta>
            <ta e="T356" id="Seg_4214" s="T355">wissen-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T357" id="Seg_4215" s="T356">oder</ta>
            <ta e="T358" id="Seg_4216" s="T357">EMPH</ta>
            <ta e="T359" id="Seg_4217" s="T358">anders</ta>
            <ta e="T360" id="Seg_4218" s="T359">was.[NOM]</ta>
            <ta e="T361" id="Seg_4219" s="T360">INDEF</ta>
            <ta e="T362" id="Seg_4220" s="T361">Mensch-3SG.[NOM]</ta>
            <ta e="T363" id="Seg_4221" s="T362">besonders</ta>
            <ta e="T364" id="Seg_4222" s="T363">solch</ta>
            <ta e="T365" id="Seg_4223" s="T364">Lärm-ACC</ta>
            <ta e="T366" id="Seg_4224" s="T365">herausnehmen-CVB.PURP</ta>
            <ta e="T367" id="Seg_4225" s="T366">töten-PST2-3SG</ta>
            <ta e="T368" id="Seg_4226" s="T367">wissen-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T369" id="Seg_4227" s="T368">jetzt-DAT/LOC</ta>
            <ta e="T370" id="Seg_4228" s="T369">und</ta>
            <ta e="T371" id="Seg_4229" s="T370">bis.zu</ta>
            <ta e="T372" id="Seg_4230" s="T371">dort</ta>
            <ta e="T373" id="Seg_4231" s="T372">aktiv.[NOM]</ta>
            <ta e="T374" id="Seg_4232" s="T373">doch</ta>
            <ta e="T375" id="Seg_4233" s="T374">dann</ta>
            <ta e="T376" id="Seg_4234" s="T375">festnehmen-VBZ-PST2-3PL</ta>
            <ta e="T377" id="Seg_4235" s="T376">EMPH</ta>
            <ta e="T382" id="Seg_4236" s="T381">Haft-ACC</ta>
            <ta e="T385" id="Seg_4237" s="T384">sagen-CVB.SEQ</ta>
            <ta e="T386" id="Seg_4238" s="T385">Mensch.[NOM]</ta>
            <ta e="T387" id="Seg_4239" s="T386">kommen-PST2-3PL</ta>
            <ta e="T388" id="Seg_4240" s="T387">Chatanga-ABL</ta>
            <ta e="T389" id="Seg_4241" s="T388">Jenisseisk-ABL</ta>
            <ta e="T392" id="Seg_4242" s="T391">Russe-PL.[NOM]</ta>
            <ta e="T393" id="Seg_4243" s="T392">AFFIRM</ta>
            <ta e="T394" id="Seg_4244" s="T393">Waffe-PROPR</ta>
            <ta e="T395" id="Seg_4245" s="T394">doch</ta>
            <ta e="T396" id="Seg_4246" s="T395">jenes</ta>
            <ta e="T397" id="Seg_4247" s="T396">jenes</ta>
            <ta e="T398" id="Seg_4248" s="T397">Nachricht-ABL</ta>
            <ta e="T399" id="Seg_4249" s="T398">Mensch-PL-ACC</ta>
            <ta e="T400" id="Seg_4250" s="T399">Gefängnis-DAT/LOC</ta>
            <ta e="T401" id="Seg_4251" s="T400">setzen-PRS-3PL</ta>
            <ta e="T402" id="Seg_4252" s="T401">man.sagt</ta>
            <ta e="T403" id="Seg_4253" s="T402">sagen-CVB.SEQ</ta>
            <ta e="T404" id="Seg_4254" s="T403">Nachricht-ABL</ta>
            <ta e="T405" id="Seg_4255" s="T404">1SG.[NOM]</ta>
            <ta e="T406" id="Seg_4256" s="T405">Mutter-1SG.[NOM]</ta>
            <ta e="T407" id="Seg_4257" s="T406">Vater-1SG.[NOM]</ta>
            <ta e="T408" id="Seg_4258" s="T407">Großvater-1SG.[NOM]</ta>
            <ta e="T409" id="Seg_4259" s="T408">sein-CVB.SEQ-3PL</ta>
            <ta e="T410" id="Seg_4260" s="T409">doch</ta>
            <ta e="T411" id="Seg_4261" s="T410">entfliehen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T412" id="Seg_4262" s="T411">stehen-PST2-3PL</ta>
            <ta e="T413" id="Seg_4263" s="T412">und</ta>
            <ta e="T414" id="Seg_4264" s="T413">zurück</ta>
            <ta e="T415" id="Seg_4265" s="T414">Essej-3PL-DAT/LOC</ta>
            <ta e="T416" id="Seg_4266" s="T415">zurückkommen-EP-PST2-3PL</ta>
            <ta e="T417" id="Seg_4267" s="T416">Vater-1SG.[NOM]</ta>
            <ta e="T418" id="Seg_4268" s="T417">Bruder-3SG.[NOM]</ta>
            <ta e="T419" id="Seg_4269" s="T418">Ignatij</ta>
            <ta e="T420" id="Seg_4270" s="T419">sagen-CVB.SEQ</ta>
            <ta e="T421" id="Seg_4271" s="T420">Mensch-ACC</ta>
            <ta e="T422" id="Seg_4272" s="T421">nehmen-PST2-3PL</ta>
            <ta e="T423" id="Seg_4273" s="T422">Gefängnis-DAT/LOC</ta>
            <ta e="T424" id="Seg_4274" s="T423">setzen-CVB.SEQ</ta>
            <ta e="T425" id="Seg_4275" s="T424">nachdem</ta>
            <ta e="T426" id="Seg_4276" s="T425">schießen-PST2</ta>
            <ta e="T427" id="Seg_4277" s="T426">töten-PST2-3PL</ta>
            <ta e="T428" id="Seg_4278" s="T427">doch</ta>
            <ta e="T432" id="Seg_4279" s="T431">AFFIRM</ta>
            <ta e="T433" id="Seg_4280" s="T432">Majmago-PL.[NOM]</ta>
            <ta e="T434" id="Seg_4281" s="T433">aha</ta>
            <ta e="T435" id="Seg_4282" s="T434">dann</ta>
            <ta e="T436" id="Seg_4283" s="T435">Vater-1SG.[NOM]</ta>
            <ta e="T437" id="Seg_4284" s="T436">klein</ta>
            <ta e="T438" id="Seg_4285" s="T437">Bruder-3SG.[NOM]</ta>
            <ta e="T439" id="Seg_4286" s="T438">Daniel</ta>
            <ta e="T440" id="Seg_4287" s="T439">sagen-CVB.SEQ</ta>
            <ta e="T441" id="Seg_4288" s="T440">Mensch.[NOM]</ta>
            <ta e="T444" id="Seg_4289" s="T443">Verwandter-3PL-DAT/LOC</ta>
            <ta e="T445" id="Seg_4290" s="T444">besuchen-CVB.SIM</ta>
            <ta e="T446" id="Seg_4291" s="T445">gehen-PST2.[3SG]</ta>
            <ta e="T447" id="Seg_4292" s="T446">lassen-CVB.SIM</ta>
            <ta e="T448" id="Seg_4293" s="T447">entfliehen-PTCP.PRS</ta>
            <ta e="T451" id="Seg_4294" s="T450">entfliehen-PST2-3PL</ta>
            <ta e="T452" id="Seg_4295" s="T451">EMPH</ta>
            <ta e="T453" id="Seg_4296" s="T452">jenes.[NOM]</ta>
            <ta e="T454" id="Seg_4297" s="T453">wegen</ta>
            <ta e="T455" id="Seg_4298" s="T454">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T456" id="Seg_4299" s="T455">jenes</ta>
            <ta e="T457" id="Seg_4300" s="T456">Kind-3PL-ACC</ta>
            <ta e="T458" id="Seg_4301" s="T457">und</ta>
            <ta e="T459" id="Seg_4302" s="T458">lassen-CVB.SIM</ta>
            <ta e="T460" id="Seg_4303" s="T459">gehen-PST2-3PL</ta>
            <ta e="T461" id="Seg_4304" s="T460">und</ta>
            <ta e="T462" id="Seg_4305" s="T461">jenes</ta>
            <ta e="T463" id="Seg_4306" s="T462">Daniel.[NOM]</ta>
            <ta e="T464" id="Seg_4307" s="T463">EMPH</ta>
            <ta e="T465" id="Seg_4308" s="T464">hier</ta>
            <ta e="T466" id="Seg_4309" s="T465">fest</ta>
            <ta e="T467" id="Seg_4310" s="T466">altern-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T468" id="Seg_4311" s="T467">bis.zu</ta>
            <ta e="T469" id="Seg_4312" s="T468">Taimyr-DAT/LOC</ta>
            <ta e="T470" id="Seg_4313" s="T469">arbeiten-CVB.SEQ</ta>
            <ta e="T479" id="Seg_4314" s="T478">Majmago</ta>
            <ta e="T480" id="Seg_4315" s="T479">Daniel</ta>
            <ta e="T481" id="Seg_4316" s="T480">Stepanowitsch</ta>
            <ta e="T482" id="Seg_4317" s="T481">sagen-CVB.SEQ</ta>
            <ta e="T483" id="Seg_4318" s="T482">jenes</ta>
            <ta e="T484" id="Seg_4319" s="T483">Mensch.[NOM]</ta>
            <ta e="T485" id="Seg_4320" s="T484">Rentier.[NOM]</ta>
            <ta e="T486" id="Seg_4321" s="T485">aufziehen-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T487" id="Seg_4322" s="T486">fest</ta>
            <ta e="T488" id="Seg_4323" s="T487">altern-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T489" id="Seg_4324" s="T488">bis.zu</ta>
            <ta e="T490" id="Seg_4325" s="T489">arbeiten-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_4326" s="T490">Kind.[NOM]</ta>
            <ta e="T492" id="Seg_4327" s="T491">Alter-3SG-ABL</ta>
            <ta e="T498" id="Seg_4328" s="T497">Volochankaer</ta>
            <ta e="T499" id="Seg_4329" s="T498">Rajon.[NOM]</ta>
            <ta e="T500" id="Seg_4330" s="T499">Kamen-DAT/LOC</ta>
            <ta e="T501" id="Seg_4331" s="T500">sein-PST1-3SG</ta>
            <ta e="T502" id="Seg_4332" s="T501">dann</ta>
            <ta e="T503" id="Seg_4333" s="T502">Hinterteil-ADJZ</ta>
            <ta e="T504" id="Seg_4334" s="T503">Jahr-3PL-ACC</ta>
            <ta e="T505" id="Seg_4335" s="T504">Chantajskoe.Ozero</ta>
            <ta e="T506" id="Seg_4336" s="T505">sagen-CVB.SEQ</ta>
            <ta e="T507" id="Seg_4337" s="T506">Dorf-DAT/LOC</ta>
            <ta e="T508" id="Seg_4338" s="T507">leben-PST2-3SG</ta>
            <ta e="T509" id="Seg_4339" s="T508">dort</ta>
            <ta e="T510" id="Seg_4340" s="T509">und</ta>
            <ta e="T511" id="Seg_4341" s="T510">sterben-PST2-3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr-MaPX">
            <ta e="T1" id="Seg_4342" s="T0">1SG.[NOM]</ta>
            <ta e="T2" id="Seg_4343" s="T1">Красноярск.[NOM]</ta>
            <ta e="T3" id="Seg_4344" s="T2">педагогический</ta>
            <ta e="T4" id="Seg_4345" s="T3">институт-ACC</ta>
            <ta e="T5" id="Seg_4346" s="T4">кончать-PST2-EP-1SG</ta>
            <ta e="T6" id="Seg_4347" s="T5">история-ACC</ta>
            <ta e="T7" id="Seg_4348" s="T6">школа-DAT/LOC</ta>
            <ta e="T8" id="Seg_4349" s="T7">ребенок-PL-ACC</ta>
            <ta e="T9" id="Seg_4350" s="T8">учить-CVB.SIM</ta>
            <ta e="T10" id="Seg_4351" s="T9">идти-EP-PST2-EP-1SG</ta>
            <ta e="T11" id="Seg_4352" s="T10">история</ta>
            <ta e="T12" id="Seg_4353" s="T11">и</ta>
            <ta e="T13" id="Seg_4354" s="T12">обществознание</ta>
            <ta e="T14" id="Seg_4355" s="T13">говорить-CVB.SEQ</ta>
            <ta e="T15" id="Seg_4356" s="T14">предмет-PL-ACC</ta>
            <ta e="T40" id="Seg_4357" s="T39">десять</ta>
            <ta e="T41" id="Seg_4358" s="T40">семь</ta>
            <ta e="T42" id="Seg_4359" s="T41">век-PROPR-DAT/LOC</ta>
            <ta e="T43" id="Seg_4360" s="T42">якутский</ta>
            <ta e="T44" id="Seg_4361" s="T43">земля-3SG-ABL</ta>
            <ta e="T45" id="Seg_4362" s="T44">Лена</ta>
            <ta e="T46" id="Seg_4363" s="T45">средний</ta>
            <ta e="T47" id="Seg_4364" s="T46">Лена-ABL</ta>
            <ta e="T48" id="Seg_4365" s="T47">десять</ta>
            <ta e="T51" id="Seg_4366" s="T50">приходить-PST2-3PL</ta>
            <ta e="T52" id="Seg_4367" s="T51">передовой</ta>
            <ta e="T53" id="Seg_4368" s="T52">якут-PL.[NOM]</ta>
            <ta e="T54" id="Seg_4369" s="T53">русский.[NOM]</ta>
            <ta e="T55" id="Seg_4370" s="T54">земля-VBZ-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T56" id="Seg_4371" s="T55">Лена-DAT/LOC</ta>
            <ta e="T57" id="Seg_4372" s="T56">эпидемия-PL.[NOM]</ta>
            <ta e="T58" id="Seg_4373" s="T57">становиться-PST2-3PL</ta>
            <ta e="T59" id="Seg_4374" s="T58">оспа</ta>
            <ta e="T60" id="Seg_4375" s="T59">чума</ta>
            <ta e="T61" id="Seg_4376" s="T60">сибирская.язва</ta>
            <ta e="T62" id="Seg_4377" s="T61">говорить-CVB.SEQ</ta>
            <ta e="T63" id="Seg_4378" s="T62">человек-PL.[NOM]</ta>
            <ta e="T64" id="Seg_4379" s="T63">дизентерия</ta>
            <ta e="T65" id="Seg_4380" s="T64">там</ta>
            <ta e="T66" id="Seg_4381" s="T65">живой.[NOM]</ta>
            <ta e="T67" id="Seg_4382" s="T66">оставаться-PTCP.PST</ta>
            <ta e="T68" id="Seg_4383" s="T67">человек-PL-EP-2SG.[NOM]</ta>
            <ta e="T69" id="Seg_4384" s="T68">силный</ta>
            <ta e="T70" id="Seg_4385" s="T69">земля.[NOM]</ta>
            <ta e="T71" id="Seg_4386" s="T70">к</ta>
            <ta e="T72" id="Seg_4387" s="T71">ээ</ta>
            <ta e="T73" id="Seg_4388" s="T72">богатый</ta>
            <ta e="T74" id="Seg_4389" s="T73">широкий</ta>
            <ta e="T75" id="Seg_4390" s="T74">озеро.[NOM]</ta>
            <ta e="T76" id="Seg_4391" s="T75">есть</ta>
            <ta e="T77" id="Seg_4392" s="T76">говорить-CVB.SEQ</ta>
            <ta e="T78" id="Seg_4393" s="T77">весть-DAT/LOC</ta>
            <ta e="T79" id="Seg_4394" s="T78">приходить-PST2-3PL</ta>
            <ta e="T80" id="Seg_4395" s="T79">так</ta>
            <ta e="T81" id="Seg_4396" s="T80">Ессей</ta>
            <ta e="T82" id="Seg_4397" s="T81">озеро.[NOM]</ta>
            <ta e="T83" id="Seg_4398" s="T82">берег-3SG-DAT/LOC</ta>
            <ta e="T84" id="Seg_4399" s="T83">жить-CVB.SEQ</ta>
            <ta e="T85" id="Seg_4400" s="T84">тот</ta>
            <ta e="T86" id="Seg_4401" s="T85">приходить-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T87" id="Seg_4402" s="T86">Ессей</ta>
            <ta e="T88" id="Seg_4403" s="T87">озеро-EP-2SG.[NOM]</ta>
            <ta e="T89" id="Seg_4404" s="T88">берег-PL-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_4405" s="T89">пустой.[NOM]</ta>
            <ta e="T91" id="Seg_4406" s="T90">говорят</ta>
            <ta e="T92" id="Seg_4407" s="T91">человек.[NOM]</ta>
            <ta e="T93" id="Seg_4408" s="T92">NEG.EX</ta>
            <ta e="T94" id="Seg_4409" s="T93">шест-PL.[NOM]</ta>
            <ta e="T95" id="Seg_4410" s="T94">только</ta>
            <ta e="T96" id="Seg_4411" s="T95">стоять-PTCP.PST</ta>
            <ta e="T97" id="Seg_4412" s="T96">стоять-PRS-3PL</ta>
            <ta e="T98" id="Seg_4413" s="T97">чум-PL-EP-2SG.[NOM]</ta>
            <ta e="T99" id="Seg_4414" s="T98">фундамент-3PL.[NOM]</ta>
            <ta e="T100" id="Seg_4415" s="T99">гнить-PST2-3PL</ta>
            <ta e="T101" id="Seg_4416" s="T100">человек-PL.[NOM]</ta>
            <ta e="T102" id="Seg_4417" s="T101">кость-3PL.[NOM]</ta>
            <ta e="T103" id="Seg_4418" s="T102">олень-PL.[NOM]</ta>
            <ta e="T104" id="Seg_4419" s="T103">собака-PL.[NOM]</ta>
            <ta e="T105" id="Seg_4420" s="T104">кость-3PL.[NOM]</ta>
            <ta e="T106" id="Seg_4421" s="T105">лежать-PRS-3PL</ta>
            <ta e="T107" id="Seg_4422" s="T106">говорят</ta>
            <ta e="T108" id="Seg_4423" s="T107">вокруг</ta>
            <ta e="T109" id="Seg_4424" s="T108">Ессей-ACC</ta>
            <ta e="T110" id="Seg_4425" s="T109">там</ta>
            <ta e="T111" id="Seg_4426" s="T110">оспа-ABL</ta>
            <ta e="T112" id="Seg_4427" s="T111">умирать-PST2-3PL</ta>
            <ta e="T113" id="Seg_4428" s="T112">эвенк-PL.[NOM]</ta>
            <ta e="T114" id="Seg_4429" s="T113">ненец-PL.[NOM]</ta>
            <ta e="T119" id="Seg_4430" s="T118">тот-PL-ACC</ta>
            <ta e="T120" id="Seg_4431" s="T119">шаман.[NOM]</ta>
            <ta e="T121" id="Seg_4432" s="T120">проклинать-PTCP.PST</ta>
            <ta e="T122" id="Seg_4433" s="T121">место-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_4434" s="T122">говорить-CVB.SEQ</ta>
            <ta e="T124" id="Seg_4435" s="T123">спасаться-EP-CAUS-FREQ-MULT-PST2-3PL</ta>
            <ta e="T125" id="Seg_4436" s="T124">тот-ACC</ta>
            <ta e="T126" id="Seg_4437" s="T125">тот</ta>
            <ta e="T127" id="Seg_4438" s="T126">пустой.[NOM]</ta>
            <ta e="T128" id="Seg_4439" s="T127">место-DAT/LOC</ta>
            <ta e="T129" id="Seg_4440" s="T128">приходить-CVB.SEQ-3PL</ta>
            <ta e="T130" id="Seg_4441" s="T129">якут-PL-EP-2SG.[NOM]</ta>
            <ta e="T131" id="Seg_4442" s="T130">корень</ta>
            <ta e="T132" id="Seg_4443" s="T131">земля.[NOM]</ta>
            <ta e="T133" id="Seg_4444" s="T132">делать-EP-PST2-3PL</ta>
            <ta e="T134" id="Seg_4445" s="T133">Ессей-ACC</ta>
            <ta e="T135" id="Seg_4446" s="T134">потом</ta>
            <ta e="T136" id="Seg_4447" s="T135">где_то</ta>
            <ta e="T137" id="Seg_4448" s="T136">десять</ta>
            <ta e="T138" id="Seg_4449" s="T137">девять</ta>
            <ta e="T139" id="Seg_4450" s="T138">век-DAT/LOC</ta>
            <ta e="T140" id="Seg_4451" s="T139">Таймыр.[NOM]</ta>
            <ta e="T141" id="Seg_4452" s="T140">к</ta>
            <ta e="T142" id="Seg_4453" s="T141">некоторый</ta>
            <ta e="T143" id="Seg_4454" s="T142">человек-PL.[NOM]</ta>
            <ta e="T144" id="Seg_4455" s="T143">Таймыр.[NOM]</ta>
            <ta e="T145" id="Seg_4456" s="T144">к</ta>
            <ta e="T146" id="Seg_4457" s="T145">уже</ta>
            <ta e="T147" id="Seg_4458" s="T146">кочевать-CVB.SIM</ta>
            <ta e="T148" id="Seg_4459" s="T147">идти-PTCP.PRS</ta>
            <ta e="T149" id="Seg_4460" s="T148">становиться-PST2-3PL</ta>
            <ta e="T154" id="Seg_4461" s="T153">олень-INSTR</ta>
            <ta e="T155" id="Seg_4462" s="T154">олень-INSTR</ta>
            <ta e="T156" id="Seg_4463" s="T155">уже</ta>
            <ta e="T157" id="Seg_4464" s="T156">тот</ta>
            <ta e="T158" id="Seg_4465" s="T157">олень-ACC</ta>
            <ta e="T159" id="Seg_4466" s="T158">уже</ta>
            <ta e="T160" id="Seg_4467" s="T159">воспитывать-PTCP.PRS</ta>
            <ta e="T161" id="Seg_4468" s="T160">человек.[NOM]</ta>
            <ta e="T162" id="Seg_4469" s="T161">становиться-PST2-3PL</ta>
            <ta e="T163" id="Seg_4470" s="T162">EMPH</ta>
            <ta e="T164" id="Seg_4471" s="T163">мм</ta>
            <ta e="T165" id="Seg_4472" s="T164">ближе</ta>
            <ta e="T166" id="Seg_4473" s="T165">силный</ta>
            <ta e="T167" id="Seg_4474" s="T166">земля.[NOM]</ta>
            <ta e="T168" id="Seg_4475" s="T167">к</ta>
            <ta e="T169" id="Seg_4476" s="T168">приходить-CVB.SEQ</ta>
            <ta e="T170" id="Seg_4477" s="T169">идти-CVB.SEQ-3PL</ta>
            <ta e="T182" id="Seg_4478" s="T181">Ессей-PL-EP-2SG.[NOM]</ta>
            <ta e="T183" id="Seg_4479" s="T182">ээ</ta>
            <ta e="T184" id="Seg_4480" s="T183">каждый-3PL.[NOM]</ta>
            <ta e="T185" id="Seg_4481" s="T184">NEG</ta>
            <ta e="T186" id="Seg_4482" s="T185">быть-NEG-3PL</ta>
            <ta e="T187" id="Seg_4483" s="T186">богатый</ta>
            <ta e="T188" id="Seg_4484" s="T187">человек-PL.[NOM]</ta>
            <ta e="T189" id="Seg_4485" s="T188">богатый</ta>
            <ta e="T190" id="Seg_4486" s="T189">семья-PL.[NOM]</ta>
            <ta e="T191" id="Seg_4487" s="T190">советский</ta>
            <ta e="T192" id="Seg_4488" s="T191">власть.[NOM]</ta>
            <ta e="T193" id="Seg_4489" s="T192">вставать-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T194" id="Seg_4490" s="T193">советский</ta>
            <ta e="T195" id="Seg_4491" s="T194">власть-ABL</ta>
            <ta e="T196" id="Seg_4492" s="T195">спасаться-CVB.SEQ-3PL</ta>
            <ta e="T197" id="Seg_4493" s="T196">в.основном</ta>
            <ta e="T198" id="Seg_4494" s="T197">раньше</ta>
            <ta e="T199" id="Seg_4495" s="T198">потом</ta>
            <ta e="T201" id="Seg_4496" s="T199">так</ta>
            <ta e="T202" id="Seg_4497" s="T201">покупка.[NOM]</ta>
            <ta e="T203" id="Seg_4498" s="T202">идти-PRS-3PL</ta>
            <ta e="T204" id="Seg_4499" s="T203">EMPH</ta>
            <ta e="T205" id="Seg_4500" s="T204">посещать-CVB.SIM</ta>
            <ta e="T206" id="Seg_4501" s="T205">идти-PRS-3PL</ta>
            <ta e="T207" id="Seg_4502" s="T206">некоторый-3PL.[NOM]</ta>
            <ta e="T208" id="Seg_4503" s="T207">оставаться-PRS-3PL</ta>
            <ta e="T209" id="Seg_4504" s="T208">ээ</ta>
            <ta e="T210" id="Seg_4505" s="T209">здесь-ADJZ</ta>
            <ta e="T211" id="Seg_4506" s="T210">человек.[NOM]</ta>
            <ta e="T212" id="Seg_4507" s="T211">Таймыр-DAT/LOC</ta>
            <ta e="T213" id="Seg_4508" s="T212">есть</ta>
            <ta e="T214" id="Seg_4509" s="T213">семья.[NOM]</ta>
            <ta e="T215" id="Seg_4510" s="T214">найти-EP-MED-CVB.SEQ-3PL</ta>
            <ta e="T216" id="Seg_4511" s="T215">разный</ta>
            <ta e="T217" id="Seg_4512" s="T216">оставаться-PRS-3PL</ta>
            <ta e="T218" id="Seg_4513" s="T217">ах</ta>
            <ta e="T219" id="Seg_4514" s="T218">но</ta>
            <ta e="T220" id="Seg_4515" s="T219">советский</ta>
            <ta e="T221" id="Seg_4516" s="T220">власть.[NOM]</ta>
            <ta e="T222" id="Seg_4517" s="T221">вставать-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T223" id="Seg_4518" s="T222">пока</ta>
            <ta e="T224" id="Seg_4519" s="T223">Ессей.[NOM]</ta>
            <ta e="T225" id="Seg_4520" s="T224">якут-PL-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_4521" s="T225">один</ta>
            <ta e="T227" id="Seg_4522" s="T226">место-DAT/LOC</ta>
            <ta e="T228" id="Seg_4523" s="T227">жить-PTCP.PRS</ta>
            <ta e="T229" id="Seg_4524" s="T228">быть-PST1-3PL</ta>
            <ta e="T230" id="Seg_4525" s="T229">потом</ta>
            <ta e="T231" id="Seg_4526" s="T230">советский</ta>
            <ta e="T232" id="Seg_4527" s="T231">власть-3SG-ABL</ta>
            <ta e="T233" id="Seg_4528" s="T232">спасаться-CVB.SEQ</ta>
            <ta e="T234" id="Seg_4529" s="T233">в.основном</ta>
            <ta e="T235" id="Seg_4530" s="T234">богатый</ta>
            <ta e="T236" id="Seg_4531" s="T235">семья-PL.[NOM]</ta>
            <ta e="T237" id="Seg_4532" s="T236">ближе</ta>
            <ta e="T238" id="Seg_4533" s="T237">приходить-MULT-PST2-3PL</ta>
            <ta e="T239" id="Seg_4534" s="T238">сюда</ta>
            <ta e="T240" id="Seg_4535" s="T239">Таймыр-DAT/LOC</ta>
            <ta e="T241" id="Seg_4536" s="T240">есть</ta>
            <ta e="T242" id="Seg_4537" s="T241">родственник-3PL-DAT/LOC</ta>
            <ta e="T243" id="Seg_4538" s="T242">близкий.[NOM]</ta>
            <ta e="T244" id="Seg_4539" s="T243">прямой</ta>
            <ta e="T245" id="Seg_4540" s="T244">знать-PTCP.PRS</ta>
            <ta e="T246" id="Seg_4541" s="T245">место-3PL-DAT/LOC</ta>
            <ta e="T247" id="Seg_4542" s="T246">ээ</ta>
            <ta e="T248" id="Seg_4543" s="T247">приходить-PST2-3PL</ta>
            <ta e="T249" id="Seg_4544" s="T248">уезжать-MULT-CVB.SEQ</ta>
            <ta e="T250" id="Seg_4545" s="T249">потом</ta>
            <ta e="T251" id="Seg_4546" s="T250">двадцать</ta>
            <ta e="T252" id="Seg_4547" s="T251">год-PL.[NOM]</ta>
            <ta e="T253" id="Seg_4548" s="T252">кончать-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T254" id="Seg_4549" s="T253">тридцать</ta>
            <ta e="T255" id="Seg_4550" s="T254">год-PL.[NOM]</ta>
            <ta e="T256" id="Seg_4551" s="T255">выйти-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T257" id="Seg_4552" s="T256">Таймыр-DAT/LOC</ta>
            <ta e="T258" id="Seg_4553" s="T257">тоже</ta>
            <ta e="T259" id="Seg_4554" s="T258">советский</ta>
            <ta e="T260" id="Seg_4555" s="T259">власть-3SG.[NOM]</ta>
            <ta e="T261" id="Seg_4556" s="T260">политика-3SG.[NOM]</ta>
            <ta e="T262" id="Seg_4557" s="T261">стараться-CVB.SEQ</ta>
            <ta e="T263" id="Seg_4558" s="T262">идти-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_4559" s="T263">опять</ta>
            <ta e="T265" id="Seg_4560" s="T264">вот</ta>
            <ta e="T266" id="Seg_4561" s="T265">богатый</ta>
            <ta e="T267" id="Seg_4562" s="T266">человек-PL-ACC</ta>
            <ta e="T268" id="Seg_4563" s="T267">богатство-3PL-ACC</ta>
            <ta e="T269" id="Seg_4564" s="T268">отбирать-CVB.SEQ</ta>
            <ta e="T270" id="Seg_4565" s="T269">разделить-PTCP.FUT-DAT/LOC</ta>
            <ta e="T271" id="Seg_4566" s="T270">говорить-CVB.SEQ</ta>
            <ta e="T272" id="Seg_4567" s="T271">становиться-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_4568" s="T276">олень-3PL-ACC</ta>
            <ta e="T278" id="Seg_4569" s="T277">отнимать-CVB.SEQ</ta>
            <ta e="T279" id="Seg_4570" s="T278">потом</ta>
            <ta e="T280" id="Seg_4571" s="T279">олень-3PL-ACC</ta>
            <ta e="T281" id="Seg_4572" s="T280">только</ta>
            <ta e="T282" id="Seg_4573" s="T281">быть-NEG.[3SG]</ta>
            <ta e="T283" id="Seg_4574" s="T282">дом.[NOM]</ta>
            <ta e="T284" id="Seg_4575" s="T283">средства.[NOM]</ta>
            <ta e="T285" id="Seg_4576" s="T284">ночью</ta>
            <ta e="T286" id="Seg_4577" s="T285">выбирать-PRS-3PL</ta>
            <ta e="T287" id="Seg_4578" s="T286">EMPH</ta>
            <ta e="T288" id="Seg_4579" s="T287">тот-ACC</ta>
            <ta e="T289" id="Seg_4580" s="T288">целый-3SG-ACC</ta>
            <ta e="T290" id="Seg_4581" s="T289">делить-PTCP.FUT-DAT/LOC</ta>
            <ta e="T291" id="Seg_4582" s="T290">надо</ta>
            <ta e="T292" id="Seg_4583" s="T291">говорить-CVB.SEQ</ta>
            <ta e="T293" id="Seg_4584" s="T292">бедный-PL-DAT/LOC</ta>
            <ta e="T294" id="Seg_4585" s="T293">раньше</ta>
            <ta e="T295" id="Seg_4586" s="T294">EMPH</ta>
            <ta e="T296" id="Seg_4587" s="T295">потом</ta>
            <ta e="T297" id="Seg_4588" s="T296">богатый</ta>
            <ta e="T298" id="Seg_4589" s="T297">человек-2SG.[NOM]</ta>
            <ta e="T299" id="Seg_4590" s="T298">давно-ADJZ</ta>
            <ta e="T300" id="Seg_4591" s="T299">закон-3SG-INSTR</ta>
            <ta e="T301" id="Seg_4592" s="T300">кто-ACC</ta>
            <ta e="T302" id="Seg_4593" s="T301">NEG</ta>
            <ta e="T303" id="Seg_4594" s="T302">обижать-NEG.CVB.SIM</ta>
            <ta e="T304" id="Seg_4595" s="T303">работник-3SG-ACC</ta>
            <ta e="T305" id="Seg_4596" s="T304">NEG</ta>
            <ta e="T306" id="Seg_4597" s="T305">обижать-NEG.CVB.SIM</ta>
            <ta e="T307" id="Seg_4598" s="T306">сам-3SG-GEN</ta>
            <ta e="T308" id="Seg_4599" s="T307">родственник-3SG-ACC</ta>
            <ta e="T309" id="Seg_4600" s="T308">NEG</ta>
            <ta e="T312" id="Seg_4601" s="T311">бедный</ta>
            <ta e="T313" id="Seg_4602" s="T312">родственник-3SG-ACC</ta>
            <ta e="T314" id="Seg_4603" s="T313">NEG</ta>
            <ta e="T315" id="Seg_4604" s="T314">обижать-NEG.CVB.SIM</ta>
            <ta e="T316" id="Seg_4605" s="T315">жить-PST2-3PL</ta>
            <ta e="T317" id="Seg_4606" s="T316">EMPH</ta>
            <ta e="T318" id="Seg_4607" s="T317">обычай-3PL.[NOM]</ta>
            <ta e="T319" id="Seg_4608" s="T318">такой</ta>
            <ta e="T320" id="Seg_4609" s="T319">помогать-CVB.SEQ</ta>
            <ta e="T321" id="Seg_4610" s="T320">жить-PST2-3PL</ta>
            <ta e="T322" id="Seg_4611" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_4612" s="T322">советский</ta>
            <ta e="T324" id="Seg_4613" s="T323">власть.[NOM]</ta>
            <ta e="T325" id="Seg_4614" s="T324">политика-3SG-ACC</ta>
            <ta e="T326" id="Seg_4615" s="T325">с</ta>
            <ta e="T327" id="Seg_4616" s="T326">согласить-EP-RECP/COLL-NEG.PTCP.PST</ta>
            <ta e="T328" id="Seg_4617" s="T327">человек-PL-EP-2SG.[NOM]</ta>
            <ta e="T329" id="Seg_4618" s="T328">здесь</ta>
            <ta e="T330" id="Seg_4619" s="T329">бунт.[NOM]</ta>
            <ta e="T331" id="Seg_4620" s="T330">вынимать-PST2-3PL</ta>
            <ta e="T335" id="Seg_4621" s="T334">AFFIRM</ta>
            <ta e="T336" id="Seg_4622" s="T335">шум.[NOM]</ta>
            <ta e="T337" id="Seg_4623" s="T336">вставать-PST2.[3SG]</ta>
            <ta e="T338" id="Seg_4624" s="T337">вот</ta>
            <ta e="T339" id="Seg_4625" s="T338">там</ta>
            <ta e="T340" id="Seg_4626" s="T339">тот</ta>
            <ta e="T341" id="Seg_4627" s="T340">большой</ta>
            <ta e="T342" id="Seg_4628" s="T341">шум-DAT/LOC</ta>
            <ta e="T343" id="Seg_4629" s="T342">человек-ACC</ta>
            <ta e="T344" id="Seg_4630" s="T343">разный</ta>
            <ta e="T345" id="Seg_4631" s="T344">убить-PST2-3PL</ta>
            <ta e="T346" id="Seg_4632" s="T345">здесь</ta>
            <ta e="T347" id="Seg_4633" s="T346">что-PL-ACC</ta>
            <ta e="T348" id="Seg_4634" s="T347">уполномоченный-PL-ACC</ta>
            <ta e="T351" id="Seg_4635" s="T350">вот</ta>
            <ta e="T352" id="Seg_4636" s="T351">тот</ta>
            <ta e="T353" id="Seg_4637" s="T352">долганский</ta>
            <ta e="T354" id="Seg_4638" s="T353">человек-3SG.[NOM]</ta>
            <ta e="T355" id="Seg_4639" s="T354">убить-PST2-3SG</ta>
            <ta e="T356" id="Seg_4640" s="T355">знать-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T357" id="Seg_4641" s="T356">или</ta>
            <ta e="T358" id="Seg_4642" s="T357">EMPH</ta>
            <ta e="T359" id="Seg_4643" s="T358">другой</ta>
            <ta e="T360" id="Seg_4644" s="T359">что.[NOM]</ta>
            <ta e="T361" id="Seg_4645" s="T360">INDEF</ta>
            <ta e="T362" id="Seg_4646" s="T361">человек-3SG.[NOM]</ta>
            <ta e="T363" id="Seg_4647" s="T362">специально</ta>
            <ta e="T364" id="Seg_4648" s="T363">такой</ta>
            <ta e="T365" id="Seg_4649" s="T364">шум-ACC</ta>
            <ta e="T366" id="Seg_4650" s="T365">вынимать-CVB.PURP</ta>
            <ta e="T367" id="Seg_4651" s="T366">убить-PST2-3SG</ta>
            <ta e="T368" id="Seg_4652" s="T367">знать-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T369" id="Seg_4653" s="T368">теперь-DAT/LOC</ta>
            <ta e="T370" id="Seg_4654" s="T369">да</ta>
            <ta e="T371" id="Seg_4655" s="T370">пока</ta>
            <ta e="T372" id="Seg_4656" s="T371">там</ta>
            <ta e="T373" id="Seg_4657" s="T372">активный.[NOM]</ta>
            <ta e="T374" id="Seg_4658" s="T373">вот</ta>
            <ta e="T375" id="Seg_4659" s="T374">потом</ta>
            <ta e="T376" id="Seg_4660" s="T375">арестовать-VBZ-PST2-3PL</ta>
            <ta e="T377" id="Seg_4661" s="T376">EMPH</ta>
            <ta e="T382" id="Seg_4662" s="T381">арест-ACC</ta>
            <ta e="T385" id="Seg_4663" s="T384">говорить-CVB.SEQ</ta>
            <ta e="T386" id="Seg_4664" s="T385">человек.[NOM]</ta>
            <ta e="T387" id="Seg_4665" s="T386">приходить-PST2-3PL</ta>
            <ta e="T388" id="Seg_4666" s="T387">Хатанга-ABL</ta>
            <ta e="T389" id="Seg_4667" s="T388">Енисейск-ABL</ta>
            <ta e="T392" id="Seg_4668" s="T391">русский-PL.[NOM]</ta>
            <ta e="T393" id="Seg_4669" s="T392">AFFIRM</ta>
            <ta e="T394" id="Seg_4670" s="T393">оружие-PROPR</ta>
            <ta e="T395" id="Seg_4671" s="T394">вот</ta>
            <ta e="T396" id="Seg_4672" s="T395">тот</ta>
            <ta e="T397" id="Seg_4673" s="T396">тот</ta>
            <ta e="T398" id="Seg_4674" s="T397">весть-ABL</ta>
            <ta e="T399" id="Seg_4675" s="T398">человек-PL-ACC</ta>
            <ta e="T400" id="Seg_4676" s="T399">тюрьма-DAT/LOC</ta>
            <ta e="T401" id="Seg_4677" s="T400">посадить-PRS-3PL</ta>
            <ta e="T402" id="Seg_4678" s="T401">говорят</ta>
            <ta e="T403" id="Seg_4679" s="T402">говорить-CVB.SEQ</ta>
            <ta e="T404" id="Seg_4680" s="T403">весть-ABL</ta>
            <ta e="T405" id="Seg_4681" s="T404">1SG.[NOM]</ta>
            <ta e="T406" id="Seg_4682" s="T405">мать-1SG.[NOM]</ta>
            <ta e="T407" id="Seg_4683" s="T406">отец-1SG.[NOM]</ta>
            <ta e="T408" id="Seg_4684" s="T407">дедушка-1SG.[NOM]</ta>
            <ta e="T409" id="Seg_4685" s="T408">быть-CVB.SEQ-3PL</ta>
            <ta e="T410" id="Seg_4686" s="T409">вот</ta>
            <ta e="T411" id="Seg_4687" s="T410">спасаться-PTCP.PRS-DAT/LOC</ta>
            <ta e="T412" id="Seg_4688" s="T411">стоять-PST2-3PL</ta>
            <ta e="T413" id="Seg_4689" s="T412">и</ta>
            <ta e="T414" id="Seg_4690" s="T413">назад</ta>
            <ta e="T415" id="Seg_4691" s="T414">Ессей-3PL-DAT/LOC</ta>
            <ta e="T416" id="Seg_4692" s="T415">возвращаться-EP-PST2-3PL</ta>
            <ta e="T417" id="Seg_4693" s="T416">отец-1SG.[NOM]</ta>
            <ta e="T418" id="Seg_4694" s="T417">брат-3SG.[NOM]</ta>
            <ta e="T419" id="Seg_4695" s="T418">Игнатий</ta>
            <ta e="T420" id="Seg_4696" s="T419">говорить-CVB.SEQ</ta>
            <ta e="T421" id="Seg_4697" s="T420">человек-ACC</ta>
            <ta e="T422" id="Seg_4698" s="T421">взять-PST2-3PL</ta>
            <ta e="T423" id="Seg_4699" s="T422">тюрьма-DAT/LOC</ta>
            <ta e="T424" id="Seg_4700" s="T423">посадить-CVB.SEQ</ta>
            <ta e="T425" id="Seg_4701" s="T424">после</ta>
            <ta e="T426" id="Seg_4702" s="T425">стрелять-PST2</ta>
            <ta e="T427" id="Seg_4703" s="T426">убить-PST2-3PL</ta>
            <ta e="T428" id="Seg_4704" s="T427">вот</ta>
            <ta e="T432" id="Seg_4705" s="T431">AFFIRM</ta>
            <ta e="T433" id="Seg_4706" s="T432">Маймаго-PL.[NOM]</ta>
            <ta e="T434" id="Seg_4707" s="T433">ага</ta>
            <ta e="T435" id="Seg_4708" s="T434">потом</ta>
            <ta e="T436" id="Seg_4709" s="T435">отец-1SG.[NOM]</ta>
            <ta e="T437" id="Seg_4710" s="T436">маленький</ta>
            <ta e="T438" id="Seg_4711" s="T437">брат-3SG.[NOM]</ta>
            <ta e="T439" id="Seg_4712" s="T438">Данил</ta>
            <ta e="T440" id="Seg_4713" s="T439">говорить-CVB.SEQ</ta>
            <ta e="T441" id="Seg_4714" s="T440">человек.[NOM]</ta>
            <ta e="T444" id="Seg_4715" s="T443">родственник-3PL-DAT/LOC</ta>
            <ta e="T445" id="Seg_4716" s="T444">посещать-CVB.SIM</ta>
            <ta e="T446" id="Seg_4717" s="T445">идти-PST2.[3SG]</ta>
            <ta e="T447" id="Seg_4718" s="T446">оставлять-CVB.SIM</ta>
            <ta e="T448" id="Seg_4719" s="T447">спасаться-PTCP.PRS</ta>
            <ta e="T451" id="Seg_4720" s="T450">спасаться-PST2-3PL</ta>
            <ta e="T452" id="Seg_4721" s="T451">EMPH</ta>
            <ta e="T453" id="Seg_4722" s="T452">тот.[NOM]</ta>
            <ta e="T454" id="Seg_4723" s="T453">из_за</ta>
            <ta e="T455" id="Seg_4724" s="T454">тот-3SG-2SG.[NOM]</ta>
            <ta e="T456" id="Seg_4725" s="T455">тот</ta>
            <ta e="T457" id="Seg_4726" s="T456">ребенок-3PL-ACC</ta>
            <ta e="T458" id="Seg_4727" s="T457">да</ta>
            <ta e="T459" id="Seg_4728" s="T458">оставлять-CVB.SIM</ta>
            <ta e="T460" id="Seg_4729" s="T459">идти-PST2-3PL</ta>
            <ta e="T461" id="Seg_4730" s="T460">и</ta>
            <ta e="T462" id="Seg_4731" s="T461">тот</ta>
            <ta e="T463" id="Seg_4732" s="T462">Данил.[NOM]</ta>
            <ta e="T464" id="Seg_4733" s="T463">EMPH</ta>
            <ta e="T465" id="Seg_4734" s="T464">здесь</ta>
            <ta e="T466" id="Seg_4735" s="T465">крепко</ta>
            <ta e="T467" id="Seg_4736" s="T466">постареть-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T468" id="Seg_4737" s="T467">пока</ta>
            <ta e="T469" id="Seg_4738" s="T468">Таймыр-DAT/LOC</ta>
            <ta e="T470" id="Seg_4739" s="T469">работать-CVB.SEQ</ta>
            <ta e="T479" id="Seg_4740" s="T478">Маймаго</ta>
            <ta e="T480" id="Seg_4741" s="T479">Данил</ta>
            <ta e="T481" id="Seg_4742" s="T480">Степанович</ta>
            <ta e="T482" id="Seg_4743" s="T481">говорить-CVB.SEQ</ta>
            <ta e="T483" id="Seg_4744" s="T482">тот</ta>
            <ta e="T484" id="Seg_4745" s="T483">человек.[NOM]</ta>
            <ta e="T485" id="Seg_4746" s="T484">олень.[NOM]</ta>
            <ta e="T486" id="Seg_4747" s="T485">воспитывать-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T487" id="Seg_4748" s="T486">крепко</ta>
            <ta e="T488" id="Seg_4749" s="T487">постареть-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T489" id="Seg_4750" s="T488">пока</ta>
            <ta e="T490" id="Seg_4751" s="T489">работать-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_4752" s="T490">ребенок.[NOM]</ta>
            <ta e="T492" id="Seg_4753" s="T491">возраст-3SG-ABL</ta>
            <ta e="T498" id="Seg_4754" s="T497">Волочанский</ta>
            <ta e="T499" id="Seg_4755" s="T498">район.[NOM]</ta>
            <ta e="T500" id="Seg_4756" s="T499">Камень-DAT/LOC</ta>
            <ta e="T501" id="Seg_4757" s="T500">быть-PST1-3SG</ta>
            <ta e="T502" id="Seg_4758" s="T501">потом</ta>
            <ta e="T503" id="Seg_4759" s="T502">задняя.часть-ADJZ</ta>
            <ta e="T504" id="Seg_4760" s="T503">год-3PL-ACC</ta>
            <ta e="T505" id="Seg_4761" s="T504">Хантайское.Озеро</ta>
            <ta e="T506" id="Seg_4762" s="T505">говорить-CVB.SEQ</ta>
            <ta e="T507" id="Seg_4763" s="T506">поселок-DAT/LOC</ta>
            <ta e="T508" id="Seg_4764" s="T507">жить-PST2-3SG</ta>
            <ta e="T509" id="Seg_4765" s="T508">там</ta>
            <ta e="T510" id="Seg_4766" s="T509">да</ta>
            <ta e="T511" id="Seg_4767" s="T510">умирать-PST2-3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc-MaPX">
            <ta e="T1" id="Seg_4768" s="T0">pers-pro:case</ta>
            <ta e="T2" id="Seg_4769" s="T1">propr-n:case</ta>
            <ta e="T3" id="Seg_4770" s="T2">adj</ta>
            <ta e="T4" id="Seg_4771" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_4772" s="T4">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T6" id="Seg_4773" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_4774" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_4775" s="T7">n-n:(num)-n:case</ta>
            <ta e="T9" id="Seg_4776" s="T8">v-v:cvb</ta>
            <ta e="T10" id="Seg_4777" s="T9">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T11" id="Seg_4778" s="T10">n</ta>
            <ta e="T12" id="Seg_4779" s="T11">conj</ta>
            <ta e="T13" id="Seg_4780" s="T12">n</ta>
            <ta e="T14" id="Seg_4781" s="T13">v-v:cvb</ta>
            <ta e="T15" id="Seg_4782" s="T14">n-n:(num)-n:case</ta>
            <ta e="T40" id="Seg_4783" s="T39">cardnum</ta>
            <ta e="T41" id="Seg_4784" s="T40">cardnum</ta>
            <ta e="T42" id="Seg_4785" s="T41">n-n&gt;adj-n:case</ta>
            <ta e="T43" id="Seg_4786" s="T42">adj</ta>
            <ta e="T44" id="Seg_4787" s="T43">n-n:poss-n:case</ta>
            <ta e="T45" id="Seg_4788" s="T44">propr</ta>
            <ta e="T46" id="Seg_4789" s="T45">adj</ta>
            <ta e="T47" id="Seg_4790" s="T46">propr-n:case</ta>
            <ta e="T48" id="Seg_4791" s="T47">cardnum</ta>
            <ta e="T51" id="Seg_4792" s="T50">v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_4793" s="T51">adj</ta>
            <ta e="T53" id="Seg_4794" s="T52">n-n:(num)-n:case</ta>
            <ta e="T54" id="Seg_4795" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_4796" s="T54">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T56" id="Seg_4797" s="T55">propr-n:case</ta>
            <ta e="T57" id="Seg_4798" s="T56">n-n:(num)-n:case</ta>
            <ta e="T58" id="Seg_4799" s="T57">v-v:tense-v:poss.pn</ta>
            <ta e="T59" id="Seg_4800" s="T58">n</ta>
            <ta e="T60" id="Seg_4801" s="T59">n</ta>
            <ta e="T61" id="Seg_4802" s="T60">n</ta>
            <ta e="T62" id="Seg_4803" s="T61">v-v:cvb</ta>
            <ta e="T63" id="Seg_4804" s="T62">n-n:(num)-n:case</ta>
            <ta e="T64" id="Seg_4805" s="T63">n</ta>
            <ta e="T65" id="Seg_4806" s="T64">adv</ta>
            <ta e="T66" id="Seg_4807" s="T65">adj-n:case</ta>
            <ta e="T67" id="Seg_4808" s="T66">v-v:ptcp</ta>
            <ta e="T68" id="Seg_4809" s="T67">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T69" id="Seg_4810" s="T68">adj</ta>
            <ta e="T70" id="Seg_4811" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_4812" s="T70">post</ta>
            <ta e="T72" id="Seg_4813" s="T71">interj</ta>
            <ta e="T73" id="Seg_4814" s="T72">adj</ta>
            <ta e="T74" id="Seg_4815" s="T73">adj</ta>
            <ta e="T75" id="Seg_4816" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_4817" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_4818" s="T76">v-v:cvb</ta>
            <ta e="T78" id="Seg_4819" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_4820" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_4821" s="T79">adv</ta>
            <ta e="T81" id="Seg_4822" s="T80">propr</ta>
            <ta e="T82" id="Seg_4823" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_4824" s="T82">n-n:poss-n:case</ta>
            <ta e="T84" id="Seg_4825" s="T83">v-v:cvb</ta>
            <ta e="T85" id="Seg_4826" s="T84">dempro</ta>
            <ta e="T86" id="Seg_4827" s="T85">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T87" id="Seg_4828" s="T86">propr</ta>
            <ta e="T88" id="Seg_4829" s="T87">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T89" id="Seg_4830" s="T88">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T90" id="Seg_4831" s="T89">adj-n:case</ta>
            <ta e="T91" id="Seg_4832" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_4833" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_4834" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_4835" s="T93">n-n:(num)-n:case</ta>
            <ta e="T95" id="Seg_4836" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_4837" s="T95">v-v:ptcp</ta>
            <ta e="T97" id="Seg_4838" s="T96">v-v:tense-v:pred.pn</ta>
            <ta e="T98" id="Seg_4839" s="T97">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T99" id="Seg_4840" s="T98">n-n:(poss)-n:case</ta>
            <ta e="T100" id="Seg_4841" s="T99">v-v:tense-v:pred.pn</ta>
            <ta e="T101" id="Seg_4842" s="T100">n-n:(num)-n:case</ta>
            <ta e="T102" id="Seg_4843" s="T101">n-n:(poss)-n:case</ta>
            <ta e="T103" id="Seg_4844" s="T102">n-n:(num)-n:case</ta>
            <ta e="T104" id="Seg_4845" s="T103">n-n:(num)-n:case</ta>
            <ta e="T105" id="Seg_4846" s="T104">n-n:(poss)-n:case</ta>
            <ta e="T106" id="Seg_4847" s="T105">v-v:tense-v:pred.pn</ta>
            <ta e="T107" id="Seg_4848" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_4849" s="T107">post</ta>
            <ta e="T109" id="Seg_4850" s="T108">propr-n:case</ta>
            <ta e="T110" id="Seg_4851" s="T109">adv</ta>
            <ta e="T111" id="Seg_4852" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_4853" s="T111">v-v:tense-v:pred.pn</ta>
            <ta e="T113" id="Seg_4854" s="T112">n-n:(num)-n:case</ta>
            <ta e="T114" id="Seg_4855" s="T113">n-n:(num)-n:case</ta>
            <ta e="T119" id="Seg_4856" s="T118">dempro-pro:(num)-pro:case</ta>
            <ta e="T120" id="Seg_4857" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_4858" s="T120">v-v:ptcp</ta>
            <ta e="T122" id="Seg_4859" s="T121">n-n:(poss)-n:case</ta>
            <ta e="T123" id="Seg_4860" s="T122">v-v:cvb</ta>
            <ta e="T124" id="Seg_4861" s="T123">v-v:(ins)-v&gt;v-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_4862" s="T124">dempro-pro:case</ta>
            <ta e="T126" id="Seg_4863" s="T125">dempro</ta>
            <ta e="T127" id="Seg_4864" s="T126">adj-n:case</ta>
            <ta e="T128" id="Seg_4865" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_4866" s="T128">v-v:cvb-v:pred.pn</ta>
            <ta e="T130" id="Seg_4867" s="T129">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T131" id="Seg_4868" s="T130">n</ta>
            <ta e="T132" id="Seg_4869" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_4870" s="T132">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_4871" s="T133">propr-n:case</ta>
            <ta e="T135" id="Seg_4872" s="T134">adv</ta>
            <ta e="T136" id="Seg_4873" s="T135">indfpro</ta>
            <ta e="T137" id="Seg_4874" s="T136">cardnum</ta>
            <ta e="T138" id="Seg_4875" s="T137">cardnum</ta>
            <ta e="T139" id="Seg_4876" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_4877" s="T139">propr-n:case</ta>
            <ta e="T141" id="Seg_4878" s="T140">post</ta>
            <ta e="T142" id="Seg_4879" s="T141">indfpro</ta>
            <ta e="T143" id="Seg_4880" s="T142">n-n:(num)-n:case</ta>
            <ta e="T144" id="Seg_4881" s="T143">propr-n:case</ta>
            <ta e="T145" id="Seg_4882" s="T144">post</ta>
            <ta e="T146" id="Seg_4883" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_4884" s="T146">v-v:cvb</ta>
            <ta e="T148" id="Seg_4885" s="T147">v-v:ptcp</ta>
            <ta e="T149" id="Seg_4886" s="T148">v-v:tense-v:pred.pn</ta>
            <ta e="T154" id="Seg_4887" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_4888" s="T154">n-n:case</ta>
            <ta e="T156" id="Seg_4889" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_4890" s="T156">dempro</ta>
            <ta e="T158" id="Seg_4891" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_4892" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_4893" s="T159">v-v:ptcp</ta>
            <ta e="T161" id="Seg_4894" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_4895" s="T161">v-v:tense-v:pred.pn</ta>
            <ta e="T163" id="Seg_4896" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_4897" s="T163">interj</ta>
            <ta e="T165" id="Seg_4898" s="T164">adv</ta>
            <ta e="T166" id="Seg_4899" s="T165">adj</ta>
            <ta e="T167" id="Seg_4900" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_4901" s="T167">post</ta>
            <ta e="T169" id="Seg_4902" s="T168">v-v:cvb</ta>
            <ta e="T170" id="Seg_4903" s="T169">v-v:cvb-v:pred.pn</ta>
            <ta e="T182" id="Seg_4904" s="T181">propr-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T183" id="Seg_4905" s="T182">interj</ta>
            <ta e="T184" id="Seg_4906" s="T183">adj-n:(poss)-n:case</ta>
            <ta e="T185" id="Seg_4907" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_4908" s="T185">v-v:(neg)-v:pred.pn</ta>
            <ta e="T187" id="Seg_4909" s="T186">adj</ta>
            <ta e="T188" id="Seg_4910" s="T187">n-n:(num)-n:case</ta>
            <ta e="T189" id="Seg_4911" s="T188">adj</ta>
            <ta e="T190" id="Seg_4912" s="T189">n-n:(num)-n:case</ta>
            <ta e="T191" id="Seg_4913" s="T190">adj</ta>
            <ta e="T192" id="Seg_4914" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_4915" s="T192">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T194" id="Seg_4916" s="T193">adj</ta>
            <ta e="T195" id="Seg_4917" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_4918" s="T195">v-v:cvb-v:pred.pn</ta>
            <ta e="T197" id="Seg_4919" s="T196">adv</ta>
            <ta e="T198" id="Seg_4920" s="T197">adv</ta>
            <ta e="T199" id="Seg_4921" s="T198">adv</ta>
            <ta e="T201" id="Seg_4922" s="T199">ptcl</ta>
            <ta e="T202" id="Seg_4923" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_4924" s="T202">v-v:tense-v:pred.pn</ta>
            <ta e="T204" id="Seg_4925" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_4926" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_4927" s="T205">v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_4928" s="T206">indfpro-pro:(poss)-pro:case</ta>
            <ta e="T208" id="Seg_4929" s="T207">v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_4930" s="T208">interj</ta>
            <ta e="T210" id="Seg_4931" s="T209">adv-adv&gt;adj</ta>
            <ta e="T211" id="Seg_4932" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_4933" s="T211">propr-n:case</ta>
            <ta e="T213" id="Seg_4934" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_4935" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_4936" s="T214">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T216" id="Seg_4937" s="T215">adj</ta>
            <ta e="T217" id="Seg_4938" s="T216">v-v:tense-v:pred.pn</ta>
            <ta e="T218" id="Seg_4939" s="T217">interj</ta>
            <ta e="T219" id="Seg_4940" s="T218">conj</ta>
            <ta e="T220" id="Seg_4941" s="T219">adj</ta>
            <ta e="T221" id="Seg_4942" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_4943" s="T221">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T223" id="Seg_4944" s="T222">post</ta>
            <ta e="T224" id="Seg_4945" s="T223">propr-n:case</ta>
            <ta e="T225" id="Seg_4946" s="T224">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T226" id="Seg_4947" s="T225">cardnum</ta>
            <ta e="T227" id="Seg_4948" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_4949" s="T227">v-v:ptcp</ta>
            <ta e="T229" id="Seg_4950" s="T228">v-v:tense-v:pred.pn</ta>
            <ta e="T230" id="Seg_4951" s="T229">adv</ta>
            <ta e="T231" id="Seg_4952" s="T230">adj</ta>
            <ta e="T232" id="Seg_4953" s="T231">n-n:poss-n:case</ta>
            <ta e="T233" id="Seg_4954" s="T232">v-v:cvb</ta>
            <ta e="T234" id="Seg_4955" s="T233">adv</ta>
            <ta e="T235" id="Seg_4956" s="T234">adj</ta>
            <ta e="T236" id="Seg_4957" s="T235">n-n:(num)-n:case</ta>
            <ta e="T237" id="Seg_4958" s="T236">adv</ta>
            <ta e="T238" id="Seg_4959" s="T237">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_4960" s="T238">adv</ta>
            <ta e="T240" id="Seg_4961" s="T239">propr-n:case</ta>
            <ta e="T241" id="Seg_4962" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_4963" s="T241">n-n:poss-n:case</ta>
            <ta e="T243" id="Seg_4964" s="T242">adj-n:case</ta>
            <ta e="T244" id="Seg_4965" s="T243">adj</ta>
            <ta e="T245" id="Seg_4966" s="T244">v-v:ptcp</ta>
            <ta e="T246" id="Seg_4967" s="T245">n-n:poss-n:case</ta>
            <ta e="T247" id="Seg_4968" s="T246">interj</ta>
            <ta e="T248" id="Seg_4969" s="T247">v-v:tense-v:pred.pn</ta>
            <ta e="T249" id="Seg_4970" s="T248">v-v&gt;v-v:cvb</ta>
            <ta e="T250" id="Seg_4971" s="T249">adv</ta>
            <ta e="T251" id="Seg_4972" s="T250">cardnum</ta>
            <ta e="T252" id="Seg_4973" s="T251">n-n:(num)-n:case</ta>
            <ta e="T253" id="Seg_4974" s="T252">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T254" id="Seg_4975" s="T253">cardnum</ta>
            <ta e="T255" id="Seg_4976" s="T254">n-n:(num)-n:case</ta>
            <ta e="T256" id="Seg_4977" s="T255">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T257" id="Seg_4978" s="T256">propr-n:case</ta>
            <ta e="T258" id="Seg_4979" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_4980" s="T258">adj</ta>
            <ta e="T260" id="Seg_4981" s="T259">n-n:(poss)-n:case</ta>
            <ta e="T261" id="Seg_4982" s="T260">n-n:(poss)-n:case</ta>
            <ta e="T262" id="Seg_4983" s="T261">v-v:cvb</ta>
            <ta e="T263" id="Seg_4984" s="T262">v-v:tense-v:pred.pn</ta>
            <ta e="T264" id="Seg_4985" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_4986" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_4987" s="T265">adj</ta>
            <ta e="T267" id="Seg_4988" s="T266">n-n:(num)-n:case</ta>
            <ta e="T268" id="Seg_4989" s="T267">n-n:poss-n:case</ta>
            <ta e="T269" id="Seg_4990" s="T268">v-v:cvb</ta>
            <ta e="T270" id="Seg_4991" s="T269">v-v:ptcp-v:(case)</ta>
            <ta e="T271" id="Seg_4992" s="T270">v-v:cvb</ta>
            <ta e="T272" id="Seg_4993" s="T271">v-v:tense-v:pred.pn</ta>
            <ta e="T277" id="Seg_4994" s="T276">n-n:poss-n:case</ta>
            <ta e="T278" id="Seg_4995" s="T277">v-v:cvb</ta>
            <ta e="T279" id="Seg_4996" s="T278">adv</ta>
            <ta e="T280" id="Seg_4997" s="T279">n-n:poss-n:case</ta>
            <ta e="T281" id="Seg_4998" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_4999" s="T281">v-v:(neg)-v:pred.pn</ta>
            <ta e="T283" id="Seg_5000" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_5001" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_5002" s="T284">adv</ta>
            <ta e="T286" id="Seg_5003" s="T285">v-v:tense-v:pred.pn</ta>
            <ta e="T287" id="Seg_5004" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_5005" s="T287">dempro-pro:case</ta>
            <ta e="T289" id="Seg_5006" s="T288">adj-n:poss-n:case</ta>
            <ta e="T290" id="Seg_5007" s="T289">v-v:ptcp-v:(case)</ta>
            <ta e="T291" id="Seg_5008" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_5009" s="T291">v-v:cvb</ta>
            <ta e="T293" id="Seg_5010" s="T292">adj-n:(num)-n:case</ta>
            <ta e="T294" id="Seg_5011" s="T293">adv</ta>
            <ta e="T295" id="Seg_5012" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_5013" s="T295">adv</ta>
            <ta e="T297" id="Seg_5014" s="T296">adj</ta>
            <ta e="T298" id="Seg_5015" s="T297">n-n:(poss)-n:case</ta>
            <ta e="T299" id="Seg_5016" s="T298">adv-adv&gt;adj</ta>
            <ta e="T300" id="Seg_5017" s="T299">n-n:poss-n:case</ta>
            <ta e="T301" id="Seg_5018" s="T300">que-pro:case</ta>
            <ta e="T302" id="Seg_5019" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_5020" s="T302">v-v:cvb</ta>
            <ta e="T304" id="Seg_5021" s="T303">n-n:poss-n:case</ta>
            <ta e="T305" id="Seg_5022" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_5023" s="T305">v-v:cvb</ta>
            <ta e="T307" id="Seg_5024" s="T306">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T308" id="Seg_5025" s="T307">n-n:poss-n:case</ta>
            <ta e="T309" id="Seg_5026" s="T308">ptcl</ta>
            <ta e="T312" id="Seg_5027" s="T311">adj</ta>
            <ta e="T313" id="Seg_5028" s="T312">n-n:poss-n:case</ta>
            <ta e="T314" id="Seg_5029" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_5030" s="T314">v-v:cvb</ta>
            <ta e="T316" id="Seg_5031" s="T315">v-v:tense-v:pred.pn</ta>
            <ta e="T317" id="Seg_5032" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_5033" s="T317">n-n:(poss)-n:case</ta>
            <ta e="T319" id="Seg_5034" s="T318">dempro</ta>
            <ta e="T320" id="Seg_5035" s="T319">v-v:cvb</ta>
            <ta e="T321" id="Seg_5036" s="T320">v-v:tense-v:pred.pn</ta>
            <ta e="T322" id="Seg_5037" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_5038" s="T322">adj</ta>
            <ta e="T324" id="Seg_5039" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_5040" s="T324">n-n:poss-n:case</ta>
            <ta e="T326" id="Seg_5041" s="T325">post</ta>
            <ta e="T327" id="Seg_5042" s="T326">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T328" id="Seg_5043" s="T327">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T329" id="Seg_5044" s="T328">adv</ta>
            <ta e="T330" id="Seg_5045" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_5046" s="T330">v-v:tense-v:poss.pn</ta>
            <ta e="T335" id="Seg_5047" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_5048" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_5049" s="T336">v-v:tense-v:pred.pn</ta>
            <ta e="T338" id="Seg_5050" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_5051" s="T338">adv</ta>
            <ta e="T340" id="Seg_5052" s="T339">dempro</ta>
            <ta e="T341" id="Seg_5053" s="T340">adj</ta>
            <ta e="T342" id="Seg_5054" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_5055" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_5056" s="T343">adj</ta>
            <ta e="T345" id="Seg_5057" s="T344">v-v:tense-v:pred.pn</ta>
            <ta e="T346" id="Seg_5058" s="T345">adv</ta>
            <ta e="T347" id="Seg_5059" s="T346">que-pro:(num)-pro:case</ta>
            <ta e="T348" id="Seg_5060" s="T347">n-n:(num)-n:case</ta>
            <ta e="T351" id="Seg_5061" s="T350">ptcl</ta>
            <ta e="T352" id="Seg_5062" s="T351">dempro</ta>
            <ta e="T353" id="Seg_5063" s="T352">adj</ta>
            <ta e="T354" id="Seg_5064" s="T353">n-n:(poss)-n:case</ta>
            <ta e="T355" id="Seg_5065" s="T354">v-v:tense-v:poss.pn</ta>
            <ta e="T356" id="Seg_5066" s="T355">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T357" id="Seg_5067" s="T356">conj</ta>
            <ta e="T358" id="Seg_5068" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_5069" s="T358">adj</ta>
            <ta e="T360" id="Seg_5070" s="T359">que-pro:case</ta>
            <ta e="T361" id="Seg_5071" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_5072" s="T361">n-n:(poss)-n:case</ta>
            <ta e="T363" id="Seg_5073" s="T362">adv</ta>
            <ta e="T364" id="Seg_5074" s="T363">dempro</ta>
            <ta e="T365" id="Seg_5075" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_5076" s="T365">v-v:cvb</ta>
            <ta e="T367" id="Seg_5077" s="T366">v-v:tense-v:poss.pn</ta>
            <ta e="T368" id="Seg_5078" s="T367">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T369" id="Seg_5079" s="T368">adv-n:case</ta>
            <ta e="T370" id="Seg_5080" s="T369">conj</ta>
            <ta e="T371" id="Seg_5081" s="T370">post</ta>
            <ta e="T372" id="Seg_5082" s="T371">adv</ta>
            <ta e="T373" id="Seg_5083" s="T372">adj-n:case</ta>
            <ta e="T374" id="Seg_5084" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_5085" s="T374">adv</ta>
            <ta e="T376" id="Seg_5086" s="T375">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T377" id="Seg_5087" s="T376">ptcl</ta>
            <ta e="T382" id="Seg_5088" s="T381">n-n:case</ta>
            <ta e="T385" id="Seg_5089" s="T384">v-v:cvb</ta>
            <ta e="T386" id="Seg_5090" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_5091" s="T386">v-v:tense-v:pred.pn</ta>
            <ta e="T388" id="Seg_5092" s="T387">propr-n:case</ta>
            <ta e="T389" id="Seg_5093" s="T388">propr-n:case</ta>
            <ta e="T392" id="Seg_5094" s="T391">n-n:(num)-n:case</ta>
            <ta e="T393" id="Seg_5095" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_5096" s="T393">n-n&gt;adj</ta>
            <ta e="T395" id="Seg_5097" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_5098" s="T395">dempro</ta>
            <ta e="T397" id="Seg_5099" s="T396">dempro</ta>
            <ta e="T398" id="Seg_5100" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_5101" s="T398">n-n:(num)-n:case</ta>
            <ta e="T400" id="Seg_5102" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_5103" s="T400">v-v:tense-v:pred.pn</ta>
            <ta e="T402" id="Seg_5104" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_5105" s="T402">v-v:cvb</ta>
            <ta e="T404" id="Seg_5106" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_5107" s="T404">pers-pro:case</ta>
            <ta e="T406" id="Seg_5108" s="T405">n-n:(poss)-n:case</ta>
            <ta e="T407" id="Seg_5109" s="T406">n-n:(poss)-n:case</ta>
            <ta e="T408" id="Seg_5110" s="T407">n-n:(poss)-n:case</ta>
            <ta e="T409" id="Seg_5111" s="T408">v-v:cvb-v:pred.pn</ta>
            <ta e="T410" id="Seg_5112" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_5113" s="T410">v-v:ptcp-v:(case)</ta>
            <ta e="T412" id="Seg_5114" s="T411">v-v:tense-v:pred.pn</ta>
            <ta e="T413" id="Seg_5115" s="T412">conj</ta>
            <ta e="T414" id="Seg_5116" s="T413">adv</ta>
            <ta e="T415" id="Seg_5117" s="T414">propr-n:poss-n:case</ta>
            <ta e="T416" id="Seg_5118" s="T415">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T417" id="Seg_5119" s="T416">n-n:(poss)-n:case</ta>
            <ta e="T418" id="Seg_5120" s="T417">n-n:(poss)-n:case</ta>
            <ta e="T419" id="Seg_5121" s="T418">propr</ta>
            <ta e="T420" id="Seg_5122" s="T419">v-v:cvb</ta>
            <ta e="T421" id="Seg_5123" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_5124" s="T421">v-v:tense-v:pred.pn</ta>
            <ta e="T423" id="Seg_5125" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_5126" s="T423">v-v:cvb</ta>
            <ta e="T425" id="Seg_5127" s="T424">post</ta>
            <ta e="T426" id="Seg_5128" s="T425">v-v:tense</ta>
            <ta e="T427" id="Seg_5129" s="T426">v-v:tense-v:pred.pn</ta>
            <ta e="T428" id="Seg_5130" s="T427">ptcl</ta>
            <ta e="T432" id="Seg_5131" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_5132" s="T432">propr-n:(num)-n:case</ta>
            <ta e="T434" id="Seg_5133" s="T433">interj</ta>
            <ta e="T435" id="Seg_5134" s="T434">adv</ta>
            <ta e="T436" id="Seg_5135" s="T435">n-n:(poss)-n:case</ta>
            <ta e="T437" id="Seg_5136" s="T436">adj</ta>
            <ta e="T438" id="Seg_5137" s="T437">n-n:(poss)-n:case</ta>
            <ta e="T439" id="Seg_5138" s="T438">propr</ta>
            <ta e="T440" id="Seg_5139" s="T439">v-v:cvb</ta>
            <ta e="T441" id="Seg_5140" s="T440">n-n:case</ta>
            <ta e="T444" id="Seg_5141" s="T443">n-n:poss-n:case</ta>
            <ta e="T445" id="Seg_5142" s="T444">v-v:cvb</ta>
            <ta e="T446" id="Seg_5143" s="T445">v-v:tense-v:pred.pn</ta>
            <ta e="T447" id="Seg_5144" s="T446">v-v:cvb</ta>
            <ta e="T448" id="Seg_5145" s="T447">v-v:ptcp</ta>
            <ta e="T451" id="Seg_5146" s="T450">v-v:tense-v:pred.pn</ta>
            <ta e="T452" id="Seg_5147" s="T451">ptcl</ta>
            <ta e="T453" id="Seg_5148" s="T452">dempro-pro:case</ta>
            <ta e="T454" id="Seg_5149" s="T453">post</ta>
            <ta e="T455" id="Seg_5150" s="T454">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T456" id="Seg_5151" s="T455">dempro</ta>
            <ta e="T457" id="Seg_5152" s="T456">n-n:poss-n:case</ta>
            <ta e="T458" id="Seg_5153" s="T457">conj</ta>
            <ta e="T459" id="Seg_5154" s="T458">v-v:cvb</ta>
            <ta e="T460" id="Seg_5155" s="T459">v-v:tense-v:pred.pn</ta>
            <ta e="T461" id="Seg_5156" s="T460">conj</ta>
            <ta e="T462" id="Seg_5157" s="T461">dempro</ta>
            <ta e="T463" id="Seg_5158" s="T462">propr-n:case</ta>
            <ta e="T464" id="Seg_5159" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_5160" s="T464">adv</ta>
            <ta e="T466" id="Seg_5161" s="T465">adv</ta>
            <ta e="T467" id="Seg_5162" s="T466">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T468" id="Seg_5163" s="T467">post</ta>
            <ta e="T469" id="Seg_5164" s="T468">propr-n:case</ta>
            <ta e="T470" id="Seg_5165" s="T469">v-v:cvb</ta>
            <ta e="T479" id="Seg_5166" s="T478">propr</ta>
            <ta e="T480" id="Seg_5167" s="T479">propr</ta>
            <ta e="T481" id="Seg_5168" s="T480">propr</ta>
            <ta e="T482" id="Seg_5169" s="T481">v-v:cvb</ta>
            <ta e="T483" id="Seg_5170" s="T482">dempro</ta>
            <ta e="T484" id="Seg_5171" s="T483">n-n:case</ta>
            <ta e="T485" id="Seg_5172" s="T484">n-n:case</ta>
            <ta e="T486" id="Seg_5173" s="T485">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T487" id="Seg_5174" s="T486">adv</ta>
            <ta e="T488" id="Seg_5175" s="T487">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T489" id="Seg_5176" s="T488">post</ta>
            <ta e="T490" id="Seg_5177" s="T489">v-v:tense-v:pred.pn</ta>
            <ta e="T491" id="Seg_5178" s="T490">n-n:case</ta>
            <ta e="T492" id="Seg_5179" s="T491">n-n:poss-n:case</ta>
            <ta e="T498" id="Seg_5180" s="T497">adj</ta>
            <ta e="T499" id="Seg_5181" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_5182" s="T499">propr-n:case</ta>
            <ta e="T501" id="Seg_5183" s="T500">v-v:tense-v:poss.pn</ta>
            <ta e="T502" id="Seg_5184" s="T501">adv</ta>
            <ta e="T503" id="Seg_5185" s="T502">n-n&gt;adj</ta>
            <ta e="T504" id="Seg_5186" s="T503">n-n:poss-n:case</ta>
            <ta e="T505" id="Seg_5187" s="T504">propr</ta>
            <ta e="T506" id="Seg_5188" s="T505">v-v:cvb</ta>
            <ta e="T507" id="Seg_5189" s="T506">n-n:case</ta>
            <ta e="T508" id="Seg_5190" s="T507">v-v:tense-v:poss.pn</ta>
            <ta e="T509" id="Seg_5191" s="T508">adv</ta>
            <ta e="T510" id="Seg_5192" s="T509">conj</ta>
            <ta e="T511" id="Seg_5193" s="T510">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-MaPX">
            <ta e="T1" id="Seg_5194" s="T0">pers</ta>
            <ta e="T2" id="Seg_5195" s="T1">propr</ta>
            <ta e="T3" id="Seg_5196" s="T2">adj</ta>
            <ta e="T4" id="Seg_5197" s="T3">n</ta>
            <ta e="T5" id="Seg_5198" s="T4">v</ta>
            <ta e="T6" id="Seg_5199" s="T5">n</ta>
            <ta e="T7" id="Seg_5200" s="T6">n</ta>
            <ta e="T8" id="Seg_5201" s="T7">n</ta>
            <ta e="T9" id="Seg_5202" s="T8">v</ta>
            <ta e="T10" id="Seg_5203" s="T9">aux</ta>
            <ta e="T11" id="Seg_5204" s="T10">n</ta>
            <ta e="T12" id="Seg_5205" s="T11">conj</ta>
            <ta e="T13" id="Seg_5206" s="T12">n</ta>
            <ta e="T14" id="Seg_5207" s="T13">v</ta>
            <ta e="T15" id="Seg_5208" s="T14">n</ta>
            <ta e="T40" id="Seg_5209" s="T39">cardnum</ta>
            <ta e="T41" id="Seg_5210" s="T40">cardnum</ta>
            <ta e="T42" id="Seg_5211" s="T41">adj</ta>
            <ta e="T43" id="Seg_5212" s="T42">adj</ta>
            <ta e="T44" id="Seg_5213" s="T43">n</ta>
            <ta e="T45" id="Seg_5214" s="T44">propr</ta>
            <ta e="T46" id="Seg_5215" s="T45">adj</ta>
            <ta e="T47" id="Seg_5216" s="T46">propr</ta>
            <ta e="T48" id="Seg_5217" s="T47">cardnum</ta>
            <ta e="T51" id="Seg_5218" s="T50">v</ta>
            <ta e="T52" id="Seg_5219" s="T51">adj</ta>
            <ta e="T53" id="Seg_5220" s="T52">n</ta>
            <ta e="T54" id="Seg_5221" s="T53">n</ta>
            <ta e="T55" id="Seg_5222" s="T54">v</ta>
            <ta e="T56" id="Seg_5223" s="T55">propr</ta>
            <ta e="T57" id="Seg_5224" s="T56">n</ta>
            <ta e="T58" id="Seg_5225" s="T57">v</ta>
            <ta e="T59" id="Seg_5226" s="T58">n</ta>
            <ta e="T60" id="Seg_5227" s="T59">n</ta>
            <ta e="T61" id="Seg_5228" s="T60">n</ta>
            <ta e="T62" id="Seg_5229" s="T61">v</ta>
            <ta e="T63" id="Seg_5230" s="T62">n</ta>
            <ta e="T64" id="Seg_5231" s="T63">n</ta>
            <ta e="T65" id="Seg_5232" s="T64">adv</ta>
            <ta e="T66" id="Seg_5233" s="T65">adj</ta>
            <ta e="T67" id="Seg_5234" s="T66">v</ta>
            <ta e="T68" id="Seg_5235" s="T67">n</ta>
            <ta e="T69" id="Seg_5236" s="T68">adj</ta>
            <ta e="T70" id="Seg_5237" s="T69">n</ta>
            <ta e="T71" id="Seg_5238" s="T70">post</ta>
            <ta e="T72" id="Seg_5239" s="T71">interj</ta>
            <ta e="T73" id="Seg_5240" s="T72">adj</ta>
            <ta e="T74" id="Seg_5241" s="T73">adj</ta>
            <ta e="T75" id="Seg_5242" s="T74">n</ta>
            <ta e="T76" id="Seg_5243" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_5244" s="T76">v</ta>
            <ta e="T78" id="Seg_5245" s="T77">n</ta>
            <ta e="T79" id="Seg_5246" s="T78">v</ta>
            <ta e="T80" id="Seg_5247" s="T79">adv</ta>
            <ta e="T81" id="Seg_5248" s="T80">propr</ta>
            <ta e="T82" id="Seg_5249" s="T81">n</ta>
            <ta e="T83" id="Seg_5250" s="T82">n</ta>
            <ta e="T84" id="Seg_5251" s="T83">v</ta>
            <ta e="T85" id="Seg_5252" s="T84">dempro</ta>
            <ta e="T86" id="Seg_5253" s="T85">v</ta>
            <ta e="T87" id="Seg_5254" s="T86">propr</ta>
            <ta e="T88" id="Seg_5255" s="T87">n</ta>
            <ta e="T89" id="Seg_5256" s="T88">n</ta>
            <ta e="T90" id="Seg_5257" s="T89">adj</ta>
            <ta e="T91" id="Seg_5258" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_5259" s="T91">n</ta>
            <ta e="T93" id="Seg_5260" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_5261" s="T93">n</ta>
            <ta e="T95" id="Seg_5262" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_5263" s="T95">v</ta>
            <ta e="T97" id="Seg_5264" s="T96">v</ta>
            <ta e="T98" id="Seg_5265" s="T97">n</ta>
            <ta e="T99" id="Seg_5266" s="T98">n</ta>
            <ta e="T100" id="Seg_5267" s="T99">v</ta>
            <ta e="T101" id="Seg_5268" s="T100">n</ta>
            <ta e="T102" id="Seg_5269" s="T101">n</ta>
            <ta e="T103" id="Seg_5270" s="T102">n</ta>
            <ta e="T104" id="Seg_5271" s="T103">n</ta>
            <ta e="T105" id="Seg_5272" s="T104">n</ta>
            <ta e="T106" id="Seg_5273" s="T105">v</ta>
            <ta e="T107" id="Seg_5274" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_5275" s="T107">post</ta>
            <ta e="T109" id="Seg_5276" s="T108">propr</ta>
            <ta e="T110" id="Seg_5277" s="T109">adv</ta>
            <ta e="T111" id="Seg_5278" s="T110">n</ta>
            <ta e="T112" id="Seg_5279" s="T111">v</ta>
            <ta e="T113" id="Seg_5280" s="T112">n</ta>
            <ta e="T114" id="Seg_5281" s="T113">n</ta>
            <ta e="T119" id="Seg_5282" s="T118">dempro</ta>
            <ta e="T120" id="Seg_5283" s="T119">n</ta>
            <ta e="T121" id="Seg_5284" s="T120">v</ta>
            <ta e="T122" id="Seg_5285" s="T121">n</ta>
            <ta e="T123" id="Seg_5286" s="T122">v</ta>
            <ta e="T124" id="Seg_5287" s="T123">v</ta>
            <ta e="T125" id="Seg_5288" s="T124">dempro</ta>
            <ta e="T126" id="Seg_5289" s="T125">dempro</ta>
            <ta e="T127" id="Seg_5290" s="T126">adj</ta>
            <ta e="T128" id="Seg_5291" s="T127">n</ta>
            <ta e="T129" id="Seg_5292" s="T128">v</ta>
            <ta e="T130" id="Seg_5293" s="T129">n</ta>
            <ta e="T131" id="Seg_5294" s="T130">n</ta>
            <ta e="T132" id="Seg_5295" s="T131">n</ta>
            <ta e="T133" id="Seg_5296" s="T132">v</ta>
            <ta e="T134" id="Seg_5297" s="T133">propr</ta>
            <ta e="T135" id="Seg_5298" s="T134">adv</ta>
            <ta e="T136" id="Seg_5299" s="T135">indfpro</ta>
            <ta e="T137" id="Seg_5300" s="T136">cardnum</ta>
            <ta e="T138" id="Seg_5301" s="T137">cardnum</ta>
            <ta e="T139" id="Seg_5302" s="T138">n</ta>
            <ta e="T140" id="Seg_5303" s="T139">propr</ta>
            <ta e="T141" id="Seg_5304" s="T140">post</ta>
            <ta e="T142" id="Seg_5305" s="T141">indfpro</ta>
            <ta e="T143" id="Seg_5306" s="T142">n</ta>
            <ta e="T144" id="Seg_5307" s="T143">propr</ta>
            <ta e="T145" id="Seg_5308" s="T144">post</ta>
            <ta e="T146" id="Seg_5309" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_5310" s="T146">v</ta>
            <ta e="T148" id="Seg_5311" s="T147">aux</ta>
            <ta e="T149" id="Seg_5312" s="T148">aux</ta>
            <ta e="T154" id="Seg_5313" s="T153">n</ta>
            <ta e="T155" id="Seg_5314" s="T154">n</ta>
            <ta e="T156" id="Seg_5315" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_5316" s="T156">dempro</ta>
            <ta e="T158" id="Seg_5317" s="T157">n</ta>
            <ta e="T159" id="Seg_5318" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_5319" s="T159">v</ta>
            <ta e="T161" id="Seg_5320" s="T160">n</ta>
            <ta e="T162" id="Seg_5321" s="T161">cop</ta>
            <ta e="T163" id="Seg_5322" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_5323" s="T163">interj</ta>
            <ta e="T165" id="Seg_5324" s="T164">adv</ta>
            <ta e="T166" id="Seg_5325" s="T165">adj</ta>
            <ta e="T167" id="Seg_5326" s="T166">n</ta>
            <ta e="T168" id="Seg_5327" s="T167">post</ta>
            <ta e="T169" id="Seg_5328" s="T168">v</ta>
            <ta e="T170" id="Seg_5329" s="T169">aux</ta>
            <ta e="T182" id="Seg_5330" s="T181">propr</ta>
            <ta e="T183" id="Seg_5331" s="T182">interj</ta>
            <ta e="T184" id="Seg_5332" s="T183">adj</ta>
            <ta e="T185" id="Seg_5333" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_5334" s="T185">cop</ta>
            <ta e="T187" id="Seg_5335" s="T186">adj</ta>
            <ta e="T188" id="Seg_5336" s="T187">n</ta>
            <ta e="T189" id="Seg_5337" s="T188">adj</ta>
            <ta e="T190" id="Seg_5338" s="T189">n</ta>
            <ta e="T191" id="Seg_5339" s="T190">adj</ta>
            <ta e="T192" id="Seg_5340" s="T191">n</ta>
            <ta e="T193" id="Seg_5341" s="T192">v</ta>
            <ta e="T194" id="Seg_5342" s="T193">adj</ta>
            <ta e="T195" id="Seg_5343" s="T194">n</ta>
            <ta e="T196" id="Seg_5344" s="T195">v</ta>
            <ta e="T197" id="Seg_5345" s="T196">adv</ta>
            <ta e="T198" id="Seg_5346" s="T197">adv</ta>
            <ta e="T199" id="Seg_5347" s="T198">adv</ta>
            <ta e="T201" id="Seg_5348" s="T199">ptcl</ta>
            <ta e="T202" id="Seg_5349" s="T201">n</ta>
            <ta e="T203" id="Seg_5350" s="T202">v</ta>
            <ta e="T204" id="Seg_5351" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_5352" s="T204">v</ta>
            <ta e="T206" id="Seg_5353" s="T205">aux</ta>
            <ta e="T207" id="Seg_5354" s="T206">indfpro</ta>
            <ta e="T208" id="Seg_5355" s="T207">v</ta>
            <ta e="T209" id="Seg_5356" s="T208">interj</ta>
            <ta e="T210" id="Seg_5357" s="T209">adj</ta>
            <ta e="T211" id="Seg_5358" s="T210">n</ta>
            <ta e="T212" id="Seg_5359" s="T211">propr</ta>
            <ta e="T213" id="Seg_5360" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_5361" s="T213">n</ta>
            <ta e="T215" id="Seg_5362" s="T214">v</ta>
            <ta e="T216" id="Seg_5363" s="T215">adj</ta>
            <ta e="T217" id="Seg_5364" s="T216">v</ta>
            <ta e="T218" id="Seg_5365" s="T217">interj</ta>
            <ta e="T219" id="Seg_5366" s="T218">conj</ta>
            <ta e="T220" id="Seg_5367" s="T219">adj</ta>
            <ta e="T221" id="Seg_5368" s="T220">n</ta>
            <ta e="T222" id="Seg_5369" s="T221">v</ta>
            <ta e="T223" id="Seg_5370" s="T222">post</ta>
            <ta e="T224" id="Seg_5371" s="T223">propr</ta>
            <ta e="T225" id="Seg_5372" s="T224">n</ta>
            <ta e="T226" id="Seg_5373" s="T225">cardnum</ta>
            <ta e="T227" id="Seg_5374" s="T226">n</ta>
            <ta e="T228" id="Seg_5375" s="T227">v</ta>
            <ta e="T229" id="Seg_5376" s="T228">aux</ta>
            <ta e="T230" id="Seg_5377" s="T229">adv</ta>
            <ta e="T231" id="Seg_5378" s="T230">adj</ta>
            <ta e="T232" id="Seg_5379" s="T231">n</ta>
            <ta e="T233" id="Seg_5380" s="T232">v</ta>
            <ta e="T234" id="Seg_5381" s="T233">adv</ta>
            <ta e="T235" id="Seg_5382" s="T234">adj</ta>
            <ta e="T236" id="Seg_5383" s="T235">n</ta>
            <ta e="T237" id="Seg_5384" s="T236">adv</ta>
            <ta e="T238" id="Seg_5385" s="T237">v</ta>
            <ta e="T239" id="Seg_5386" s="T238">adv</ta>
            <ta e="T240" id="Seg_5387" s="T239">propr</ta>
            <ta e="T241" id="Seg_5388" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_5389" s="T241">n</ta>
            <ta e="T243" id="Seg_5390" s="T242">adj</ta>
            <ta e="T244" id="Seg_5391" s="T243">adj</ta>
            <ta e="T245" id="Seg_5392" s="T244">v</ta>
            <ta e="T246" id="Seg_5393" s="T245">n</ta>
            <ta e="T247" id="Seg_5394" s="T246">interj</ta>
            <ta e="T248" id="Seg_5395" s="T247">v</ta>
            <ta e="T249" id="Seg_5396" s="T248">v</ta>
            <ta e="T250" id="Seg_5397" s="T249">adv</ta>
            <ta e="T251" id="Seg_5398" s="T250">cardnum</ta>
            <ta e="T252" id="Seg_5399" s="T251">n</ta>
            <ta e="T253" id="Seg_5400" s="T252">v</ta>
            <ta e="T254" id="Seg_5401" s="T253">cardnum</ta>
            <ta e="T255" id="Seg_5402" s="T254">n</ta>
            <ta e="T256" id="Seg_5403" s="T255">v</ta>
            <ta e="T257" id="Seg_5404" s="T256">propr</ta>
            <ta e="T258" id="Seg_5405" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_5406" s="T258">adj</ta>
            <ta e="T260" id="Seg_5407" s="T259">n</ta>
            <ta e="T261" id="Seg_5408" s="T260">n</ta>
            <ta e="T262" id="Seg_5409" s="T261">v</ta>
            <ta e="T263" id="Seg_5410" s="T262">aux</ta>
            <ta e="T264" id="Seg_5411" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_5412" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_5413" s="T265">adj</ta>
            <ta e="T267" id="Seg_5414" s="T266">n</ta>
            <ta e="T268" id="Seg_5415" s="T267">n</ta>
            <ta e="T269" id="Seg_5416" s="T268">v</ta>
            <ta e="T270" id="Seg_5417" s="T269">v</ta>
            <ta e="T271" id="Seg_5418" s="T270">v</ta>
            <ta e="T272" id="Seg_5419" s="T271">aux</ta>
            <ta e="T277" id="Seg_5420" s="T276">n</ta>
            <ta e="T278" id="Seg_5421" s="T277">v</ta>
            <ta e="T279" id="Seg_5422" s="T278">adv</ta>
            <ta e="T280" id="Seg_5423" s="T279">n</ta>
            <ta e="T281" id="Seg_5424" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_5425" s="T281">cop</ta>
            <ta e="T283" id="Seg_5426" s="T282">n</ta>
            <ta e="T284" id="Seg_5427" s="T283">n</ta>
            <ta e="T285" id="Seg_5428" s="T284">adv</ta>
            <ta e="T286" id="Seg_5429" s="T285">v</ta>
            <ta e="T287" id="Seg_5430" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_5431" s="T287">dempro</ta>
            <ta e="T289" id="Seg_5432" s="T288">adj</ta>
            <ta e="T290" id="Seg_5433" s="T289">v</ta>
            <ta e="T291" id="Seg_5434" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_5435" s="T291">v</ta>
            <ta e="T293" id="Seg_5436" s="T292">n</ta>
            <ta e="T294" id="Seg_5437" s="T293">adv</ta>
            <ta e="T295" id="Seg_5438" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_5439" s="T295">adv</ta>
            <ta e="T297" id="Seg_5440" s="T296">adj</ta>
            <ta e="T298" id="Seg_5441" s="T297">n</ta>
            <ta e="T299" id="Seg_5442" s="T298">adj</ta>
            <ta e="T300" id="Seg_5443" s="T299">n</ta>
            <ta e="T301" id="Seg_5444" s="T300">que</ta>
            <ta e="T302" id="Seg_5445" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_5446" s="T302">v</ta>
            <ta e="T304" id="Seg_5447" s="T303">n</ta>
            <ta e="T305" id="Seg_5448" s="T304">ptcl</ta>
            <ta e="T306" id="Seg_5449" s="T305">v</ta>
            <ta e="T307" id="Seg_5450" s="T306">emphpro</ta>
            <ta e="T308" id="Seg_5451" s="T307">n</ta>
            <ta e="T309" id="Seg_5452" s="T308">ptcl</ta>
            <ta e="T312" id="Seg_5453" s="T311">adj</ta>
            <ta e="T313" id="Seg_5454" s="T312">n</ta>
            <ta e="T314" id="Seg_5455" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_5456" s="T314">v</ta>
            <ta e="T316" id="Seg_5457" s="T315">v</ta>
            <ta e="T317" id="Seg_5458" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_5459" s="T317">n</ta>
            <ta e="T319" id="Seg_5460" s="T318">dempro</ta>
            <ta e="T320" id="Seg_5461" s="T319">v</ta>
            <ta e="T321" id="Seg_5462" s="T320">v</ta>
            <ta e="T322" id="Seg_5463" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_5464" s="T322">adj</ta>
            <ta e="T324" id="Seg_5465" s="T323">n</ta>
            <ta e="T325" id="Seg_5466" s="T324">n</ta>
            <ta e="T326" id="Seg_5467" s="T325">post</ta>
            <ta e="T327" id="Seg_5468" s="T326">v</ta>
            <ta e="T328" id="Seg_5469" s="T327">n</ta>
            <ta e="T329" id="Seg_5470" s="T328">adv</ta>
            <ta e="T330" id="Seg_5471" s="T329">n</ta>
            <ta e="T331" id="Seg_5472" s="T330">v</ta>
            <ta e="T335" id="Seg_5473" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_5474" s="T335">n</ta>
            <ta e="T337" id="Seg_5475" s="T336">v</ta>
            <ta e="T338" id="Seg_5476" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_5477" s="T338">adv</ta>
            <ta e="T340" id="Seg_5478" s="T339">dempro</ta>
            <ta e="T341" id="Seg_5479" s="T340">adj</ta>
            <ta e="T342" id="Seg_5480" s="T341">n</ta>
            <ta e="T343" id="Seg_5481" s="T342">n</ta>
            <ta e="T344" id="Seg_5482" s="T343">adj</ta>
            <ta e="T345" id="Seg_5483" s="T344">v</ta>
            <ta e="T346" id="Seg_5484" s="T345">adv</ta>
            <ta e="T347" id="Seg_5485" s="T346">que</ta>
            <ta e="T348" id="Seg_5486" s="T347">n</ta>
            <ta e="T351" id="Seg_5487" s="T350">ptcl</ta>
            <ta e="T352" id="Seg_5488" s="T351">dempro</ta>
            <ta e="T353" id="Seg_5489" s="T352">adj</ta>
            <ta e="T354" id="Seg_5490" s="T353">n</ta>
            <ta e="T355" id="Seg_5491" s="T354">v</ta>
            <ta e="T356" id="Seg_5492" s="T355">v</ta>
            <ta e="T357" id="Seg_5493" s="T356">conj</ta>
            <ta e="T358" id="Seg_5494" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_5495" s="T358">adj</ta>
            <ta e="T360" id="Seg_5496" s="T359">que</ta>
            <ta e="T361" id="Seg_5497" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_5498" s="T361">n</ta>
            <ta e="T363" id="Seg_5499" s="T362">adv</ta>
            <ta e="T364" id="Seg_5500" s="T363">dempro</ta>
            <ta e="T365" id="Seg_5501" s="T364">n</ta>
            <ta e="T366" id="Seg_5502" s="T365">v</ta>
            <ta e="T367" id="Seg_5503" s="T366">v</ta>
            <ta e="T368" id="Seg_5504" s="T367">v</ta>
            <ta e="T369" id="Seg_5505" s="T368">adv</ta>
            <ta e="T370" id="Seg_5506" s="T369">conj</ta>
            <ta e="T371" id="Seg_5507" s="T370">post</ta>
            <ta e="T372" id="Seg_5508" s="T371">adv</ta>
            <ta e="T373" id="Seg_5509" s="T372">n</ta>
            <ta e="T374" id="Seg_5510" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_5511" s="T374">adv</ta>
            <ta e="T376" id="Seg_5512" s="T375">v</ta>
            <ta e="T377" id="Seg_5513" s="T376">ptcl</ta>
            <ta e="T382" id="Seg_5514" s="T381">n</ta>
            <ta e="T385" id="Seg_5515" s="T384">v</ta>
            <ta e="T386" id="Seg_5516" s="T385">n</ta>
            <ta e="T387" id="Seg_5517" s="T386">v</ta>
            <ta e="T388" id="Seg_5518" s="T387">propr</ta>
            <ta e="T389" id="Seg_5519" s="T388">propr</ta>
            <ta e="T392" id="Seg_5520" s="T391">n</ta>
            <ta e="T393" id="Seg_5521" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_5522" s="T393">adj</ta>
            <ta e="T395" id="Seg_5523" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_5524" s="T395">dempro</ta>
            <ta e="T397" id="Seg_5525" s="T396">dempro</ta>
            <ta e="T398" id="Seg_5526" s="T397">n</ta>
            <ta e="T399" id="Seg_5527" s="T398">n</ta>
            <ta e="T400" id="Seg_5528" s="T399">n</ta>
            <ta e="T401" id="Seg_5529" s="T400">v</ta>
            <ta e="T402" id="Seg_5530" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_5531" s="T402">v</ta>
            <ta e="T404" id="Seg_5532" s="T403">n</ta>
            <ta e="T405" id="Seg_5533" s="T404">pers</ta>
            <ta e="T406" id="Seg_5534" s="T405">n</ta>
            <ta e="T407" id="Seg_5535" s="T406">n</ta>
            <ta e="T408" id="Seg_5536" s="T407">n</ta>
            <ta e="T409" id="Seg_5537" s="T408">cop</ta>
            <ta e="T410" id="Seg_5538" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_5539" s="T410">v</ta>
            <ta e="T412" id="Seg_5540" s="T411">aux</ta>
            <ta e="T413" id="Seg_5541" s="T412">conj</ta>
            <ta e="T414" id="Seg_5542" s="T413">adv</ta>
            <ta e="T415" id="Seg_5543" s="T414">propr</ta>
            <ta e="T416" id="Seg_5544" s="T415">v</ta>
            <ta e="T417" id="Seg_5545" s="T416">n</ta>
            <ta e="T418" id="Seg_5546" s="T417">n</ta>
            <ta e="T419" id="Seg_5547" s="T418">propr</ta>
            <ta e="T420" id="Seg_5548" s="T419">v</ta>
            <ta e="T421" id="Seg_5549" s="T420">n</ta>
            <ta e="T422" id="Seg_5550" s="T421">v</ta>
            <ta e="T423" id="Seg_5551" s="T422">n</ta>
            <ta e="T424" id="Seg_5552" s="T423">v</ta>
            <ta e="T425" id="Seg_5553" s="T424">post</ta>
            <ta e="T426" id="Seg_5554" s="T425">v</ta>
            <ta e="T427" id="Seg_5555" s="T426">v</ta>
            <ta e="T428" id="Seg_5556" s="T427">ptcl</ta>
            <ta e="T432" id="Seg_5557" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_5558" s="T432">propr</ta>
            <ta e="T434" id="Seg_5559" s="T433">interj</ta>
            <ta e="T435" id="Seg_5560" s="T434">adv</ta>
            <ta e="T436" id="Seg_5561" s="T435">n</ta>
            <ta e="T437" id="Seg_5562" s="T436">adj</ta>
            <ta e="T438" id="Seg_5563" s="T437">n</ta>
            <ta e="T439" id="Seg_5564" s="T438">propr</ta>
            <ta e="T440" id="Seg_5565" s="T439">v</ta>
            <ta e="T441" id="Seg_5566" s="T440">n</ta>
            <ta e="T444" id="Seg_5567" s="T443">n</ta>
            <ta e="T445" id="Seg_5568" s="T444">v</ta>
            <ta e="T446" id="Seg_5569" s="T445">v</ta>
            <ta e="T447" id="Seg_5570" s="T446">v</ta>
            <ta e="T448" id="Seg_5571" s="T447">v</ta>
            <ta e="T451" id="Seg_5572" s="T450">v</ta>
            <ta e="T452" id="Seg_5573" s="T451">ptcl</ta>
            <ta e="T453" id="Seg_5574" s="T452">dempro</ta>
            <ta e="T454" id="Seg_5575" s="T453">post</ta>
            <ta e="T455" id="Seg_5576" s="T454">dempro</ta>
            <ta e="T456" id="Seg_5577" s="T455">dempro</ta>
            <ta e="T457" id="Seg_5578" s="T456">n</ta>
            <ta e="T458" id="Seg_5579" s="T457">conj</ta>
            <ta e="T459" id="Seg_5580" s="T458">v</ta>
            <ta e="T460" id="Seg_5581" s="T459">v</ta>
            <ta e="T461" id="Seg_5582" s="T460">conj</ta>
            <ta e="T462" id="Seg_5583" s="T461">dempro</ta>
            <ta e="T463" id="Seg_5584" s="T462">propr</ta>
            <ta e="T464" id="Seg_5585" s="T463">ptcl</ta>
            <ta e="T465" id="Seg_5586" s="T464">adv</ta>
            <ta e="T466" id="Seg_5587" s="T465">adv</ta>
            <ta e="T467" id="Seg_5588" s="T466">v</ta>
            <ta e="T468" id="Seg_5589" s="T467">post</ta>
            <ta e="T469" id="Seg_5590" s="T468">propr</ta>
            <ta e="T470" id="Seg_5591" s="T469">v</ta>
            <ta e="T479" id="Seg_5592" s="T478">propr</ta>
            <ta e="T480" id="Seg_5593" s="T479">propr</ta>
            <ta e="T481" id="Seg_5594" s="T480">propr</ta>
            <ta e="T482" id="Seg_5595" s="T481">v</ta>
            <ta e="T483" id="Seg_5596" s="T482">dempro</ta>
            <ta e="T484" id="Seg_5597" s="T483">n</ta>
            <ta e="T485" id="Seg_5598" s="T484">n</ta>
            <ta e="T486" id="Seg_5599" s="T485">n</ta>
            <ta e="T487" id="Seg_5600" s="T486">adv</ta>
            <ta e="T488" id="Seg_5601" s="T487">v</ta>
            <ta e="T489" id="Seg_5602" s="T488">post</ta>
            <ta e="T490" id="Seg_5603" s="T489">v</ta>
            <ta e="T491" id="Seg_5604" s="T490">n</ta>
            <ta e="T492" id="Seg_5605" s="T491">n</ta>
            <ta e="T498" id="Seg_5606" s="T497">adj</ta>
            <ta e="T499" id="Seg_5607" s="T498">n</ta>
            <ta e="T500" id="Seg_5608" s="T499">propr</ta>
            <ta e="T501" id="Seg_5609" s="T500">cop</ta>
            <ta e="T502" id="Seg_5610" s="T501">adv</ta>
            <ta e="T503" id="Seg_5611" s="T502">adj</ta>
            <ta e="T504" id="Seg_5612" s="T503">n</ta>
            <ta e="T505" id="Seg_5613" s="T504">propr</ta>
            <ta e="T506" id="Seg_5614" s="T505">v</ta>
            <ta e="T507" id="Seg_5615" s="T506">n</ta>
            <ta e="T508" id="Seg_5616" s="T507">v</ta>
            <ta e="T509" id="Seg_5617" s="T508">adv</ta>
            <ta e="T510" id="Seg_5618" s="T509">conj</ta>
            <ta e="T511" id="Seg_5619" s="T510">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-MaPX" />
         <annotation name="SyF" tierref="SyF-MaPX" />
         <annotation name="IST" tierref="IST-MaPX" />
         <annotation name="Top" tierref="Top-MaPX" />
         <annotation name="Foc" tierref="Foc-MaPX" />
         <annotation name="BOR" tierref="BOR-MaPX">
            <ta e="T2" id="Seg_5620" s="T1">RUS:cult</ta>
            <ta e="T3" id="Seg_5621" s="T2">RUS:cult</ta>
            <ta e="T4" id="Seg_5622" s="T3">RUS:cult</ta>
            <ta e="T6" id="Seg_5623" s="T5">RUS:cult</ta>
            <ta e="T7" id="Seg_5624" s="T6">RUS:cult</ta>
            <ta e="T11" id="Seg_5625" s="T10">RUS:cult</ta>
            <ta e="T12" id="Seg_5626" s="T11">RUS:gram</ta>
            <ta e="T13" id="Seg_5627" s="T12">RUS:cult</ta>
            <ta e="T15" id="Seg_5628" s="T14">RUS:cult</ta>
            <ta e="T45" id="Seg_5629" s="T44">RUS:cult</ta>
            <ta e="T47" id="Seg_5630" s="T46">RUS:cult</ta>
            <ta e="T56" id="Seg_5631" s="T55">RUS:cult</ta>
            <ta e="T57" id="Seg_5632" s="T56">RUS:cult</ta>
            <ta e="T59" id="Seg_5633" s="T58">RUS:cult</ta>
            <ta e="T60" id="Seg_5634" s="T59">RUS:cult</ta>
            <ta e="T61" id="Seg_5635" s="T60">RUS:cult</ta>
            <ta e="T64" id="Seg_5636" s="T63">RUS:cult</ta>
            <ta e="T81" id="Seg_5637" s="T80">RUS:cult</ta>
            <ta e="T87" id="Seg_5638" s="T86">RUS:cult</ta>
            <ta e="T111" id="Seg_5639" s="T110">RUS:cult</ta>
            <ta e="T114" id="Seg_5640" s="T113">RUS:cult</ta>
            <ta e="T134" id="Seg_5641" s="T133">RUS:cult</ta>
            <ta e="T136" id="Seg_5642" s="T135">RUS:mod</ta>
            <ta e="T146" id="Seg_5643" s="T145">RUS:mod</ta>
            <ta e="T156" id="Seg_5644" s="T155">RUS:mod</ta>
            <ta e="T159" id="Seg_5645" s="T158">RUS:mod</ta>
            <ta e="T182" id="Seg_5646" s="T181">RUS:cult</ta>
            <ta e="T191" id="Seg_5647" s="T190">RUS:cult</ta>
            <ta e="T192" id="Seg_5648" s="T191">RUS:cult</ta>
            <ta e="T194" id="Seg_5649" s="T193">RUS:cult</ta>
            <ta e="T195" id="Seg_5650" s="T194">RUS:cult</ta>
            <ta e="T197" id="Seg_5651" s="T196">RUS:mod</ta>
            <ta e="T201" id="Seg_5652" s="T199">RUS:disc</ta>
            <ta e="T219" id="Seg_5653" s="T218">RUS:gram</ta>
            <ta e="T220" id="Seg_5654" s="T219">RUS:cult</ta>
            <ta e="T221" id="Seg_5655" s="T220">RUS:cult</ta>
            <ta e="T224" id="Seg_5656" s="T223">RUS:cult</ta>
            <ta e="T231" id="Seg_5657" s="T230">RUS:cult</ta>
            <ta e="T232" id="Seg_5658" s="T231">RUS:cult</ta>
            <ta e="T234" id="Seg_5659" s="T233">RUS:mod</ta>
            <ta e="T259" id="Seg_5660" s="T258">RUS:cult</ta>
            <ta e="T260" id="Seg_5661" s="T259">RUS:cult</ta>
            <ta e="T261" id="Seg_5662" s="T260">RUS:cult</ta>
            <ta e="T291" id="Seg_5663" s="T290">RUS:mod</ta>
            <ta e="T300" id="Seg_5664" s="T299">RUS:cult</ta>
            <ta e="T318" id="Seg_5665" s="T317">RUS:cult</ta>
            <ta e="T323" id="Seg_5666" s="T322">RUS:cult</ta>
            <ta e="T324" id="Seg_5667" s="T323">RUS:cult</ta>
            <ta e="T325" id="Seg_5668" s="T324">RUS:cult</ta>
            <ta e="T330" id="Seg_5669" s="T329">RUS:cult</ta>
            <ta e="T348" id="Seg_5670" s="T347">RUS:cult</ta>
            <ta e="T357" id="Seg_5671" s="T356">RUS:gram</ta>
            <ta e="T358" id="Seg_5672" s="T357">RUS:mod</ta>
            <ta e="T370" id="Seg_5673" s="T369">RUS:gram</ta>
            <ta e="T373" id="Seg_5674" s="T372">RUS:cult</ta>
            <ta e="T376" id="Seg_5675" s="T375">RUS:cult</ta>
            <ta e="T382" id="Seg_5676" s="T381">RUS:cult</ta>
            <ta e="T388" id="Seg_5677" s="T387">RUS:cult</ta>
            <ta e="T389" id="Seg_5678" s="T388">RUS:cult</ta>
            <ta e="T413" id="Seg_5679" s="T412">RUS:gram</ta>
            <ta e="T415" id="Seg_5680" s="T414">RUS:cult</ta>
            <ta e="T418" id="Seg_5681" s="T417">RUS:core</ta>
            <ta e="T419" id="Seg_5682" s="T418">RUS:cult</ta>
            <ta e="T433" id="Seg_5683" s="T432">EV:cult</ta>
            <ta e="T438" id="Seg_5684" s="T437">RUS:core</ta>
            <ta e="T439" id="Seg_5685" s="T438">RUS:cult</ta>
            <ta e="T458" id="Seg_5686" s="T457">RUS:gram</ta>
            <ta e="T461" id="Seg_5687" s="T460">RUS:gram</ta>
            <ta e="T463" id="Seg_5688" s="T462">RUS:cult</ta>
            <ta e="T479" id="Seg_5689" s="T478">EV:cult</ta>
            <ta e="T480" id="Seg_5690" s="T479">RUS:cult</ta>
            <ta e="T481" id="Seg_5691" s="T480">RUS:cult</ta>
            <ta e="T498" id="Seg_5692" s="T497">RUS:cult</ta>
            <ta e="T499" id="Seg_5693" s="T498">RUS:cult</ta>
            <ta e="T500" id="Seg_5694" s="T499">RUS:cult</ta>
            <ta e="T505" id="Seg_5695" s="T504">RUS:cult</ta>
            <ta e="T507" id="Seg_5696" s="T506">RUS:cult</ta>
            <ta e="T510" id="Seg_5697" s="T509">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-MaPX" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-MaPX" />
         <annotation name="CS" tierref="CS-MaPX" />
         <annotation name="fe" tierref="fe-MaPX">
            <ta e="T5" id="Seg_5698" s="T0">– I finished the Krasnoyarsk pedagogical institute.</ta>
            <ta e="T15" id="Seg_5699" s="T5"> ‎I taught the children history at school, subjects which are called "history" and "social science".</ta>
            <ta e="T53" id="Seg_5700" s="T39">– In the 17th century the first Yakuts came from Yakutia, from the middle Lena region.</ta>
            <ta e="T64" id="Seg_5701" s="T53">As the Russians settled there, epidemics came up: pocks, plague, anthrax they are named, the people, dysentery. </ta>
            <ta e="T79" id="Seg_5702" s="T64">The people, which had survived there, came to the good land, eh, after they had got to know that there is a rich, broad lake.</ta>
            <ta e="T93" id="Seg_5703" s="T79">So they lived at the shore of Lake Essey, as they came there, the shores of Lake Essey were empty, it is said, nobody was there.</ta>
            <ta e="T100" id="Seg_5704" s="T93">There are just poles standing, the foundations of the tents are rotten.</ta>
            <ta e="T109" id="Seg_5705" s="T100">Human bones, bones of reindeer and dogs are lying there, it is said, around Essey.</ta>
            <ta e="T118" id="Seg_5706" s="T109">There Evenki and Nenets died of the pocks, Samoyeds, the tribes lived there.</ta>
            <ta e="T134" id="Seg_5707" s="T124">And this, as they came to the empty place, the Yakuts made Essey to their own land.</ta>
            <ta e="T149" id="Seg_5708" s="T134">Then at some point in the 19th century towards Taymyr, some people already started to nomadize towards Taymyr.</ta>
            <ta e="T155" id="Seg_5709" s="T151">– With reindeer, with reindeer.</ta>
            <ta e="T163" id="Seg_5710" s="T155">They had already become reindeer-herding people.</ta>
            <ta e="T170" id="Seg_5711" s="T163">After having come closer to the good land.</ta>
            <ta e="T190" id="Seg_5712" s="T181">– Not all people from Essey were rich people, rich families.</ta>
            <ta e="T201" id="Seg_5713" s="T190">As the Soviet power came, as they were escaping from the Soviet power, after all, earlier so…</ta>
            <ta e="T217" id="Seg_5714" s="T201">They trade, they make visits, some of them stay, eh, if they find people from here, who live on Taymyr, for themselves, they stay.</ta>
            <ta e="T229" id="Seg_5715" s="T217">Well, until the Soviet came, the Yakuts from Essey lived at one place.</ta>
            <ta e="T243" id="Seg_5716" s="T229">As they were escaping from the Soviet power, the rich families came closer, at all, hither, close to their sibling which were living on Taymyr.</ta>
            <ta e="T249" id="Seg_5717" s="T243">They came directly to known place, nomadizing.</ta>
            <ta e="T263" id="Seg_5718" s="T249">Then at the end of the twenties, in the beginning of the thirties the Soviet's policy became stronger also on Taymyr.</ta>
            <ta e="T272" id="Seg_5719" s="T263">Again they started to take away the rich people's wealth and to say "one needs to share".</ta>
            <ta e="T278" id="Seg_5720" s="T276">– Taking away their reindeer.</ta>
            <ta e="T287" id="Seg_5721" s="T278">It was not only reindeer, the household was (taken away?) at night.</ta>
            <ta e="T293" id="Seg_5722" s="T287">It is said "one has to share that and give it to the poor".</ta>
            <ta e="T319" id="Seg_5723" s="T293">Earlier the rich people lived acoording to the old laws not offending anyone, not offending the working people, not offending their own siblings, not offending the poor siblings, their customs are so.</ta>
            <ta e="T322" id="Seg_5724" s="T319">They lived and helped.</ta>
            <ta e="T331" id="Seg_5725" s="T322">People, who didn't agree with the Soviet power's policy, made a riot here.</ta>
            <ta e="T337" id="Seg_5726" s="T334">– Yes, a riot came up.</ta>
            <ta e="T348" id="Seg_5727" s="T337">And during this riot different people were killed, here those whatchamallit, deputies.</ta>
            <ta e="T368" id="Seg_5728" s="T348">It is unknown whether a Dolgan has killed, or another person, whether he has killed in order to provoke a riot, it is unknown.</ta>
            <ta e="T371" id="Seg_5729" s="T368">Until today.</ta>
            <ta e="T382" id="Seg_5730" s="T371">The the active ones were arrested, though, arrest.</ta>
            <ta e="T389" id="Seg_5731" s="T382">People came from Khatanga, from Yeniseysk under the name "unit of chonovetses".</ta>
            <ta e="T394" id="Seg_5732" s="T391">– Russians, yes, with weapons.</ta>
            <ta e="T416" id="Seg_5733" s="T394">Because of this message, that people would be sent to prison, my mother, my father and my grandfather decided to escape, and they went back to Essey.</ta>
            <ta e="T428" id="Seg_5734" s="T416">My father's brother, his name was Ignatiy, was taken, sent to prison and killed.</ta>
            <ta e="T434" id="Seg_5735" s="T431">– Yes, Majmago, mhm.</ta>
            <ta e="T460" id="Seg_5736" s="T434">Then my father's little brother, a man named Danil, he was visiting his siblings, but they were escaping, therefore they left even their child back and went away.</ta>
            <ta e="T475" id="Seg_5737" s="T460">And that Danil was working on Taymyr until he was very old and earned the Orden of the Red Banner of Labour.</ta>
            <ta e="T482" id="Seg_5738" s="T478">– Danil Stepanovich Majmago.</ta>
            <ta e="T492" id="Seg_5739" s="T482">This human worked as a reindeer herder until he was very old, from childhood on.</ta>
            <ta e="T501" id="Seg_5740" s="T497">– It was in Volochanka rayon, in Kamen.</ta>
            <ta e="T511" id="Seg_5741" s="T501">Then he lived in a village named Khantayskoe Ozero, and there he died also.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-MaPX">
            <ta e="T5" id="Seg_5742" s="T0">– Ich habe das Krasnojarsker pädagogische Institut abgeschlossen.</ta>
            <ta e="T15" id="Seg_5743" s="T5">Ich habe Geschichte an der Schule den Kindern gelehrt, Fächer, die "Geschichte" und "Gesellschaftswissenschaft" heißen.</ta>
            <ta e="T53" id="Seg_5744" s="T39">– Im siebzehnten Jahrhundert kamen aus Jakutien, von der mittleren Lena die ersten Jakuten.</ta>
            <ta e="T64" id="Seg_5745" s="T53">Als die Russen sich niederließen, kamen Epidemien an die Lena: Pocken, Pest, Milzbrand heißen die, die Leute, Ruhr.</ta>
            <ta e="T79" id="Seg_5746" s="T64">Die Leute, die dort überlebt hatten, kamen in das gute Land, äh, nachdem sie erfahren hatten, dass es dort einen reichen, breiten See gibt.</ta>
            <ta e="T93" id="Seg_5747" s="T79">So lebten sie am Ufer des Essej-Sees, als sie ankamen, waren die Ufer des Essej-Sees leer, sagt man, dort war niemand.</ta>
            <ta e="T100" id="Seg_5748" s="T93">Es stehen nur Stangen da, die Fundamente der Zelte sind verfault.</ta>
            <ta e="T109" id="Seg_5749" s="T100">Menschliche Knochen, Knochen von Rentieren und Hunden liegen herum, sagt man, um den Essej herum.</ta>
            <ta e="T118" id="Seg_5750" s="T109">Dort starben an den Pocken Ewenken, Nenzen, Samojeden, die Stämme lebten dort.</ta>
            <ta e="T124" id="Seg_5751" s="T118">Jene, "ein von Schamanen verfluchter Ort" sagend verjagte man.</ta>
            <ta e="T134" id="Seg_5752" s="T124">Und dieses, als sie an den leeren Ort kamen, die Jakuten machen sich den Essej zu eigen.</ta>
            <ta e="T149" id="Seg_5753" s="T134">Dann irgendwann im 19. Jahrhundert in Richtung Taimyr, einige Leute fingen schon an, in Richtung Taimyr zu nomadisieren.</ta>
            <ta e="T155" id="Seg_5754" s="T151">– Mit Rentieren, mit Rentieren.</ta>
            <ta e="T163" id="Seg_5755" s="T155">Sie waren schon Rentiere hütende Menschen geworden.</ta>
            <ta e="T170" id="Seg_5756" s="T163">Nachdem sie näher zum guten Land gekommen waren.</ta>
            <ta e="T190" id="Seg_5757" s="T181">– Nicht alle Leute aus Essej waren reiche Leute, reiche Familien.</ta>
            <ta e="T201" id="Seg_5758" s="T190">Als die Sowjetmacht kam, als sie vor der Sowjetmacht flohen, im Grunde, früher so…</ta>
            <ta e="T217" id="Seg_5759" s="T201">Sie handeln, sie gehen auf Besuch, einige bleiben, äh, wenn sie hiesige Menschen, auf der Taimyr seiende Familien für sich finden, bleiben sie.</ta>
            <ta e="T229" id="Seg_5760" s="T217">Nun, bis die Sowjetmacht kam, lebten die Jakuten vom Essej an einem Ort.</ta>
            <ta e="T243" id="Seg_5761" s="T229">Als sie dann vor der Sowjetmacht flohen, kamen die reichen Familien im Grunde näher, hierher, nahe an ihre auf der Taimyr lebenden Verwandten.</ta>
            <ta e="T249" id="Seg_5762" s="T243">Sie kamen direkt zu bekannten Orten, nomadisierend.</ta>
            <ta e="T263" id="Seg_5763" s="T249">Dann am Ende der Zwanzigerjahre, am Anfang der Dreißigerjahre wurde die Politik der Sowjetmacht auch auf der Taimyr strenger.</ta>
            <ta e="T272" id="Seg_5764" s="T263">Wieder fing man an reichen Leuten ihren Reichtum wegzunehmen und zu sagen "man muss teilen".</ta>
            <ta e="T278" id="Seg_5765" s="T276">– Ihre Rentiere wegnehmend.</ta>
            <ta e="T287" id="Seg_5766" s="T278">Es waren nicht nur Rentiere, der Hausstand wurde nachts (weggenommen?).</ta>
            <ta e="T293" id="Seg_5767" s="T287">Es wird gesagt "das muss man alles teilen und den Armen geben".</ta>
            <ta e="T319" id="Seg_5768" s="T293">Früher lebten die reichen Menschen nach den alten Gesetzen ohne irgendjemanden zu kränken, ohne die Arbeiter zu kränken, ohne ihre eigenen Verwandten zu kränken, ohne die armen Verwandten zu kränken, so sind ihre Sitten.</ta>
            <ta e="T322" id="Seg_5769" s="T319">Sie lebten und halfen.</ta>
            <ta e="T331" id="Seg_5770" s="T322">Leute, die mit der Politik der Sowjetmacht nicht einverstanden waren, machten hier einen Aufstand.</ta>
            <ta e="T337" id="Seg_5771" s="T334">– Ja, ein Aufstand brach los.</ta>
            <ta e="T348" id="Seg_5772" s="T337">Und in diesem großen Aufstand wurden unterschiedliche Leute getötet, hier diese da, die Abgeordneten.</ta>
            <ta e="T368" id="Seg_5773" s="T348">Und es ist unbekannt, ob ein Dolgane getötet hat, oder irgendein anderer Mensch, ob er getötet hat, um einen Aufstand zu provozieren, es ist unbekannt.</ta>
            <ta e="T371" id="Seg_5774" s="T368">Bis heute.</ta>
            <ta e="T382" id="Seg_5775" s="T371">Dann wurden die Aktiven, nun, verhaftet, Arrest.</ta>
            <ta e="T389" id="Seg_5776" s="T382">Leute mit der Bezeichnung "Čonovec-Einheit" kamen aus Chatanga, aus Jenisseisk.</ta>
            <ta e="T394" id="Seg_5777" s="T391">– Russen, ja, bewaffnet.</ta>
            <ta e="T416" id="Seg_5778" s="T394">Und wegen dieser Nachricht, dass Leute ins Gefängnis gesteckt würden, entschieden sich meine Mutter, mein Vater und mein Großvater zu fliehen, und sie kehrten nach Essej zurück.</ta>
            <ta e="T428" id="Seg_5779" s="T416">Der Bruder meines Vaters, er hieß Ignatij, wurde mitgenommen, ins Gefängnis gesetzt und getötet.</ta>
            <ta e="T434" id="Seg_5780" s="T431">– Ja, die Majmagos, aha.</ta>
            <ta e="T460" id="Seg_5781" s="T434">Dann der kleine Bruder meines Vaters, ein Mensch mit dem Namen Danil, er besuchte Verwandte, sie flohen aber, deshalb ließen sie sogar ihr Kind züruck und gingen fort.</ta>
            <ta e="T475" id="Seg_5782" s="T460">Und dieser Danil arbeitete bis ins hohe Alter auf der Taimyr und erarbeitete den Orden des Roten Banners der Arbeit.</ta>
            <ta e="T482" id="Seg_5783" s="T478">– Danil Stepanowitsch Majmago.</ta>
            <ta e="T492" id="Seg_5784" s="T482">Dieser Mensch arbeitete in der Rentierzucht bis ins hohe Alter, von Kindesbeinen an.</ta>
            <ta e="T501" id="Seg_5785" s="T497">– Das war im Volochankaer Rajon, in Kamen.</ta>
            <ta e="T511" id="Seg_5786" s="T501">Dann lebte er in den nächsten Jahren in einem Dorf mit dem Namen Chantajskoe Ozero, dort starb er auch.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-MaPX" />
         <annotation name="ltr" tierref="ltr-MaPX">
            <ta e="T5" id="Seg_5787" s="T0">М: Я Красноярский педагогический институт закончила.</ta>
            <ta e="T15" id="Seg_5788" s="T5">Историю в школе детям преподавала, историю и обществознание – как предметы.</ta>
            <ta e="T53" id="Seg_5789" s="T39">М: В семнадцатом веку из Саха (Якутской) земли с середены Лены пришли первые долганы (якуты).</ta>
            <ta e="T64" id="Seg_5790" s="T53">Русские как заселились, на Лене эпидемии начались: оспа, чума, "сибирская язва" – с таким названием, люди, дизентерия.</ta>
            <ta e="T79" id="Seg_5791" s="T64">Там выжившие люди в сторону (спокойной ? благостной?) земли, богатое широкое озеро есть, узнав это, поселились.</ta>
            <ta e="T93" id="Seg_5792" s="T79">Так на берегу Ессейя озера живя, вот когда приехали, Ессейя озера берега пустые, оказывается, людей нет.</ta>
            <ta e="T100" id="Seg_5793" s="T93">Шесты чумов только стоят, дома (до основания?) сгнили. </ta>
            <ta e="T109" id="Seg_5794" s="T100">Человеческие кости, оленьи, собачьи кости лежат, говорят, вокруг Ессейя.</ta>
            <ta e="T118" id="Seg_5795" s="T109">Там от оспы умерли эвенки, ненцы, самоеды (племена там жили).</ta>
            <ta e="T124" id="Seg_5796" s="T118">Вот те, шаман порчу навел, говорят, сбежали в разные стороны.</ta>
            <ta e="T134" id="Seg_5797" s="T124">И то, на пустое место приехав долганы (якуты) своими землями сделали Ессей.</ta>
            <ta e="T149" id="Seg_5798" s="T134">Потом где-то в девятнадцатом веку в стороне Таймыра, некоторые люди возле Таймыра кочевать начали.</ta>
            <ta e="T155" id="Seg_5799" s="T151">М: На оленях, на оленях, на оленях.</ta>
            <ta e="T163" id="Seg_5800" s="T155">Уже оленя выводящими людьми стали.</ta>
            <ta e="T170" id="Seg_5801" s="T163">Ближе к (спокойной ?) земле переехав.</ta>
            <ta e="T190" id="Seg_5802" s="T181">М: Ессейцы хотя и не все, богатые люди, богатые семьи.</ta>
            <ta e="T201" id="Seg_5803" s="T190">Советская власть когда устанавливалась, от советской власти сбежав, в основном, раньше вот так…</ta>
            <ta e="T217" id="Seg_5804" s="T201">(Продавали?) ездили, в гости ездили, некоторые остаются, за здешних людей, за тех, кто на Таймыре живёт семьи находят, так и остаются. </ta>
            <ta e="T229" id="Seg_5805" s="T217">Но, пока советская власть стояла Ессейские долганы (якуты) в одном месте жили.</ta>
            <ta e="T243" id="Seg_5806" s="T229">Потом от советской власти сбежав, в основном, богатые семьи ближе сюда приехали, здесь, на Таймыре находящимся родственниками рядом.</ta>
            <ta e="T249" id="Seg_5807" s="T243">В прямо знакомые места переехали, перекочевали.</ta>
            <ta e="T263" id="Seg_5808" s="T249">Потом в конце дватцатых годов, в начале тридцатых годов на Таймыре опять политика Советской власти окрепла.</ta>
            <ta e="T272" id="Seg_5809" s="T263">Снова у богатых людей богатсво выборочно делить начали.</ta>
            <ta e="T278" id="Seg_5810" s="T276">М: Оленей забрали.</ta>
            <ta e="T287" id="Seg_5811" s="T278">Не только оленей ещё, домашнее имущество отбирали тоже. </ta>
            <ta e="T293" id="Seg_5812" s="T287">Это всё раздать надо, говоря, беднякам.</ta>
            <ta e="T319" id="Seg_5813" s="T293">Раньше богатые люди по старым законам никого не обижая: ни работника своего не обижая, ни своего родственника не обижая, ни родственников бедняка не обижая, жили, обычай их такой.</ta>
            <ta e="T322" id="Seg_5814" s="T319">Помогая жили вот.</ta>
            <ta e="T331" id="Seg_5815" s="T322">С политикой советской власти несогласные люди здесь бунт подняли.</ta>
            <ta e="T337" id="Seg_5816" s="T334">М: Да, взбунтовались (шум поднялся).</ta>
            <ta e="T348" id="Seg_5817" s="T337">И там на большом бунте людей по-разному убили, здесь этих, уполномоченных.</ta>
            <ta e="T368" id="Seg_5818" s="T348">И вот, что тундровик (т.е. местный житель) убил неизвестно или какой-то другой человек специально, чтобы поднять такой бунт (шум), неизвестно.</ta>
            <ta e="T371" id="Seg_5819" s="T368">До сих пор.</ta>
            <ta e="T382" id="Seg_5820" s="T371">Тогда активных, ну, арестовали ведь, аресты.</ta>
            <ta e="T389" id="Seg_5821" s="T382">"Отряд чоновцев" называемые люди прибыли из Хатанги, из Енисейска.</ta>
            <ta e="T394" id="Seg_5822" s="T391">М: Русские, да, вооружённые (с ружьём).</ta>
            <ta e="T416" id="Seg_5823" s="T394">М: И вот из этих вестей, что людей в тюрьму сажают говорят, от этой вести мои мама, папа и дедушка бежать решили, и обратно в Ессей вернулись.</ta>
            <ta e="T428" id="Seg_5824" s="T416">М: Папиного брата, человека с именем Игнатий, забрали, после того, как в тюрьму посадили, (расстреляли), убили вообщем.</ta>
            <ta e="T434" id="Seg_5825" s="T431">М: Да, Маймаго, ага.</ta>
            <ta e="T460" id="Seg_5826" s="T434">Потом папин младший брат, человек с именем Данил, который к родственникам в гости поехал, оставив, сбежали, поэтому даже детей оставили и уехали. </ta>
            <ta e="T475" id="Seg_5827" s="T460">И вот Данил, вот здесь до самой старости на Таймыре проработав, Орден Трудового Красного знамени заработал.</ta>
            <ta e="T482" id="Seg_5828" s="T478">М: Маймаго Данил Степанович.</ta>
            <ta e="T492" id="Seg_5829" s="T482">Тот человек оленеводом (в выращивании оленей) до самой старости проработал с ранних лет.</ta>
            <ta e="T501" id="Seg_5830" s="T497">М: Волочанский район в Камне было.</ta>
            <ta e="T511" id="Seg_5831" s="T501">Потом последующие годы в "Хантайское Озеро" называемом посёлке прожил, там и умер.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-MaPX">
            <ta e="T389" id="Seg_5832" s="T382">[DCh]: The "unit of chonovetses" was a special unit of the army (&lt; russ. ЧОН = часть особого назначения).</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KuNS"
                      id="tx-KuNS"
                      speaker="KuNS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KuNS">
            <ts e="T39" id="Seg_5833" n="sc" s="T15">
               <ts e="T29" id="Seg_5835" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_5837" n="HIAT:w" s="T15">Ehigi</ts>
                  <nts id="Seg_5838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_5840" n="HIAT:w" s="T16">bejegit</ts>
                  <nts id="Seg_5841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_5843" n="HIAT:w" s="T17">törüttergitin</ts>
                  <nts id="Seg_5844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_5846" n="HIAT:w" s="T18">tuhunan</ts>
                  <nts id="Seg_5847" n="HIAT:ip">,</nts>
                  <nts id="Seg_5848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_5850" n="HIAT:w" s="T19">öjdöːtökkö</ts>
                  <nts id="Seg_5851" n="HIAT:ip">,</nts>
                  <nts id="Seg_5852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_5854" n="HIAT:w" s="T20">bert</ts>
                  <nts id="Seg_5855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_5857" n="HIAT:w" s="T21">oduː</ts>
                  <nts id="Seg_5858" n="HIAT:ip">,</nts>
                  <nts id="Seg_5859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_5861" n="HIAT:w" s="T22">kihini</ts>
                  <nts id="Seg_5862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_5864" n="HIAT:w" s="T23">oduːrgaːta</ts>
                  <nts id="Seg_5865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_5867" n="HIAT:w" s="T24">öhü</ts>
                  <nts id="Seg_5868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_5870" n="HIAT:w" s="T25">kepsiːr</ts>
                  <nts id="Seg_5871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_5873" n="HIAT:w" s="T26">kördükküt</ts>
                  <nts id="Seg_5874" n="HIAT:ip">,</nts>
                  <nts id="Seg_5875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_5877" n="HIAT:w" s="T27">bɨhɨlaːk</ts>
                  <nts id="Seg_5878" n="HIAT:ip">,</nts>
                  <nts id="Seg_5879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_5881" n="HIAT:w" s="T28">dʼe</ts>
                  <nts id="Seg_5882" n="HIAT:ip">.</nts>
                  <nts id="Seg_5883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_5885" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_5887" n="HIAT:w" s="T29">Ehi͡ej</ts>
                  <nts id="Seg_5888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_5890" n="HIAT:w" s="T30">hakalara</ts>
                  <nts id="Seg_5891" n="HIAT:ip">,</nts>
                  <nts id="Seg_5892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5893" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_5895" n="HIAT:w" s="T31">Dʼeki͡ej</ts>
                  <nts id="Seg_5896" n="HIAT:ip">)</nts>
                  <nts id="Seg_5897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_5899" n="HIAT:w" s="T32">Ehi͡ej</ts>
                  <nts id="Seg_5900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_5902" n="HIAT:w" s="T33">dʼonnoro</ts>
                  <nts id="Seg_5903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_5905" n="HIAT:w" s="T34">kajdi͡egitten</ts>
                  <nts id="Seg_5906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_5908" n="HIAT:w" s="T35">kelbit</ts>
                  <nts id="Seg_5909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_5911" n="HIAT:w" s="T36">etilerej</ts>
                  <nts id="Seg_5912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_5914" n="HIAT:w" s="T37">iti</ts>
                  <nts id="Seg_5915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_5917" n="HIAT:w" s="T38">hirge</ts>
                  <nts id="Seg_5918" n="HIAT:ip">?</nts>
                  <nts id="Seg_5919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T152" id="Seg_5920" n="sc" s="T149">
               <ts e="T152" id="Seg_5922" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_5924" n="HIAT:w" s="T149">Tabalartaːk</ts>
                  <nts id="Seg_5925" n="HIAT:ip">,</nts>
                  <nts id="Seg_5926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_5928" n="HIAT:w" s="T150">taba</ts>
                  <nts id="Seg_5929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_5931" n="HIAT:w" s="T151">eː</ts>
                  <nts id="Seg_5932" n="HIAT:ip">?</nts>
                  <nts id="Seg_5933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_5934" n="sc" s="T170">
               <ts e="T181" id="Seg_5936" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_5938" n="HIAT:w" s="T170">Onton</ts>
                  <nts id="Seg_5939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_5941" n="HIAT:w" s="T171">Tajmɨːrtan</ts>
                  <nts id="Seg_5942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_5944" n="HIAT:w" s="T172">eː</ts>
                  <nts id="Seg_5945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_5947" n="HIAT:w" s="T173">kajdak</ts>
                  <nts id="Seg_5948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_5950" n="HIAT:w" s="T174">eː</ts>
                  <nts id="Seg_5951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_5953" n="HIAT:w" s="T175">emi͡e</ts>
                  <nts id="Seg_5954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_5956" n="HIAT:w" s="T176">töttörü</ts>
                  <nts id="Seg_5957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_5959" n="HIAT:w" s="T177">iti</ts>
                  <nts id="Seg_5960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_5962" n="HIAT:w" s="T178">Evenkʼija</ts>
                  <nts id="Seg_5963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_5965" n="HIAT:w" s="T179">di͡ek</ts>
                  <nts id="Seg_5966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_5968" n="HIAT:w" s="T180">bardɨlar</ts>
                  <nts id="Seg_5969" n="HIAT:ip">?</nts>
                  <nts id="Seg_5970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T276" id="Seg_5971" n="sc" s="T272">
               <ts e="T276" id="Seg_5973" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_5975" n="HIAT:w" s="T272">Baːjdarɨn</ts>
                  <nts id="Seg_5976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_5978" n="HIAT:w" s="T273">bɨldʼaːtɨlar</ts>
                  <nts id="Seg_5979" n="HIAT:ip">,</nts>
                  <nts id="Seg_5980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_5982" n="HIAT:w" s="T274">tabalarɨn</ts>
                  <nts id="Seg_5983" n="HIAT:ip">,</nts>
                  <nts id="Seg_5984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_5986" n="HIAT:w" s="T275">eː</ts>
                  <nts id="Seg_5987" n="HIAT:ip">?</nts>
                  <nts id="Seg_5988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T334" id="Seg_5989" n="sc" s="T331">
               <ts e="T334" id="Seg_5991" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_5993" n="HIAT:w" s="T331">Ajdaːn</ts>
                  <nts id="Seg_5994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_5996" n="HIAT:w" s="T332">turbuta</ts>
                  <nts id="Seg_5997" n="HIAT:ip">,</nts>
                  <nts id="Seg_5998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_6000" n="HIAT:w" s="T333">eː</ts>
                  <nts id="Seg_6001" n="HIAT:ip">?</nts>
                  <nts id="Seg_6002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T391" id="Seg_6003" n="sc" s="T389">
               <ts e="T391" id="Seg_6005" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_6007" n="HIAT:w" s="T389">Nʼuːččalar</ts>
                  <nts id="Seg_6008" n="HIAT:ip">,</nts>
                  <nts id="Seg_6009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_6011" n="HIAT:w" s="T390">eː</ts>
                  <nts id="Seg_6012" n="HIAT:ip">?</nts>
                  <nts id="Seg_6013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T431" id="Seg_6014" n="sc" s="T428">
               <ts e="T431" id="Seg_6016" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_6018" n="HIAT:w" s="T428">Ehigi</ts>
                  <nts id="Seg_6019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_6021" n="HIAT:w" s="T429">rotkɨt</ts>
                  <nts id="Seg_6022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_6024" n="HIAT:w" s="T430">Majmagolar</ts>
                  <nts id="Seg_6025" n="HIAT:ip">?</nts>
                  <nts id="Seg_6026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T478" id="Seg_6027" n="sc" s="T475">
               <ts e="T478" id="Seg_6029" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_6031" n="HIAT:w" s="T475">Kim</ts>
                  <nts id="Seg_6032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_6034" n="HIAT:w" s="T476">etej</ts>
                  <nts id="Seg_6035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_6037" n="HIAT:w" s="T477">hurullara</ts>
                  <nts id="Seg_6038" n="HIAT:ip">?</nts>
                  <nts id="Seg_6039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T497" id="Seg_6040" n="sc" s="T492">
               <ts e="T497" id="Seg_6042" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_6044" n="HIAT:w" s="T492">Kannɨk</ts>
                  <nts id="Seg_6045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_6047" n="HIAT:w" s="T493">savxozka</ts>
                  <nts id="Seg_6048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_6050" n="HIAT:w" s="T494">ete</ts>
                  <nts id="Seg_6051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_6053" n="HIAT:w" s="T495">ebitej</ts>
                  <nts id="Seg_6054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_6056" n="HIAT:w" s="T496">iti</ts>
                  <nts id="Seg_6057" n="HIAT:ip">?</nts>
                  <nts id="Seg_6058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KuNS">
            <ts e="T39" id="Seg_6059" n="sc" s="T15">
               <ts e="T16" id="Seg_6061" n="e" s="T15">Ehigi </ts>
               <ts e="T17" id="Seg_6063" n="e" s="T16">bejegit </ts>
               <ts e="T18" id="Seg_6065" n="e" s="T17">törüttergitin </ts>
               <ts e="T19" id="Seg_6067" n="e" s="T18">tuhunan, </ts>
               <ts e="T20" id="Seg_6069" n="e" s="T19">öjdöːtökkö, </ts>
               <ts e="T21" id="Seg_6071" n="e" s="T20">bert </ts>
               <ts e="T22" id="Seg_6073" n="e" s="T21">oduː, </ts>
               <ts e="T23" id="Seg_6075" n="e" s="T22">kihini </ts>
               <ts e="T24" id="Seg_6077" n="e" s="T23">oduːrgaːta </ts>
               <ts e="T25" id="Seg_6079" n="e" s="T24">öhü </ts>
               <ts e="T26" id="Seg_6081" n="e" s="T25">kepsiːr </ts>
               <ts e="T27" id="Seg_6083" n="e" s="T26">kördükküt, </ts>
               <ts e="T28" id="Seg_6085" n="e" s="T27">bɨhɨlaːk, </ts>
               <ts e="T29" id="Seg_6087" n="e" s="T28">dʼe. </ts>
               <ts e="T30" id="Seg_6089" n="e" s="T29">Ehi͡ej </ts>
               <ts e="T31" id="Seg_6091" n="e" s="T30">hakalara, </ts>
               <ts e="T32" id="Seg_6093" n="e" s="T31">(Dʼeki͡ej) </ts>
               <ts e="T33" id="Seg_6095" n="e" s="T32">Ehi͡ej </ts>
               <ts e="T34" id="Seg_6097" n="e" s="T33">dʼonnoro </ts>
               <ts e="T35" id="Seg_6099" n="e" s="T34">kajdi͡egitten </ts>
               <ts e="T36" id="Seg_6101" n="e" s="T35">kelbit </ts>
               <ts e="T37" id="Seg_6103" n="e" s="T36">etilerej </ts>
               <ts e="T38" id="Seg_6105" n="e" s="T37">iti </ts>
               <ts e="T39" id="Seg_6107" n="e" s="T38">hirge? </ts>
            </ts>
            <ts e="T152" id="Seg_6108" n="sc" s="T149">
               <ts e="T150" id="Seg_6110" n="e" s="T149">Tabalartaːk, </ts>
               <ts e="T151" id="Seg_6112" n="e" s="T150">taba </ts>
               <ts e="T152" id="Seg_6114" n="e" s="T151">eː? </ts>
            </ts>
            <ts e="T181" id="Seg_6115" n="sc" s="T170">
               <ts e="T171" id="Seg_6117" n="e" s="T170">Onton </ts>
               <ts e="T172" id="Seg_6119" n="e" s="T171">Tajmɨːrtan </ts>
               <ts e="T173" id="Seg_6121" n="e" s="T172">eː </ts>
               <ts e="T174" id="Seg_6123" n="e" s="T173">kajdak </ts>
               <ts e="T175" id="Seg_6125" n="e" s="T174">eː </ts>
               <ts e="T176" id="Seg_6127" n="e" s="T175">emi͡e </ts>
               <ts e="T177" id="Seg_6129" n="e" s="T176">töttörü </ts>
               <ts e="T178" id="Seg_6131" n="e" s="T177">iti </ts>
               <ts e="T179" id="Seg_6133" n="e" s="T178">Evenkʼija </ts>
               <ts e="T180" id="Seg_6135" n="e" s="T179">di͡ek </ts>
               <ts e="T181" id="Seg_6137" n="e" s="T180">bardɨlar? </ts>
            </ts>
            <ts e="T276" id="Seg_6138" n="sc" s="T272">
               <ts e="T273" id="Seg_6140" n="e" s="T272">Baːjdarɨn </ts>
               <ts e="T274" id="Seg_6142" n="e" s="T273">bɨldʼaːtɨlar, </ts>
               <ts e="T275" id="Seg_6144" n="e" s="T274">tabalarɨn, </ts>
               <ts e="T276" id="Seg_6146" n="e" s="T275">eː? </ts>
            </ts>
            <ts e="T334" id="Seg_6147" n="sc" s="T331">
               <ts e="T332" id="Seg_6149" n="e" s="T331">Ajdaːn </ts>
               <ts e="T333" id="Seg_6151" n="e" s="T332">turbuta, </ts>
               <ts e="T334" id="Seg_6153" n="e" s="T333">eː? </ts>
            </ts>
            <ts e="T391" id="Seg_6154" n="sc" s="T389">
               <ts e="T390" id="Seg_6156" n="e" s="T389">Nʼuːččalar, </ts>
               <ts e="T391" id="Seg_6158" n="e" s="T390">eː? </ts>
            </ts>
            <ts e="T431" id="Seg_6159" n="sc" s="T428">
               <ts e="T429" id="Seg_6161" n="e" s="T428">Ehigi </ts>
               <ts e="T430" id="Seg_6163" n="e" s="T429">rotkɨt </ts>
               <ts e="T431" id="Seg_6165" n="e" s="T430">Majmagolar? </ts>
            </ts>
            <ts e="T478" id="Seg_6166" n="sc" s="T475">
               <ts e="T476" id="Seg_6168" n="e" s="T475">Kim </ts>
               <ts e="T477" id="Seg_6170" n="e" s="T476">etej </ts>
               <ts e="T478" id="Seg_6172" n="e" s="T477">hurullara? </ts>
            </ts>
            <ts e="T497" id="Seg_6173" n="sc" s="T492">
               <ts e="T493" id="Seg_6175" n="e" s="T492">Kannɨk </ts>
               <ts e="T494" id="Seg_6177" n="e" s="T493">savxozka </ts>
               <ts e="T495" id="Seg_6179" n="e" s="T494">ete </ts>
               <ts e="T496" id="Seg_6181" n="e" s="T495">ebitej </ts>
               <ts e="T497" id="Seg_6183" n="e" s="T496">iti? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KuNS">
            <ta e="T29" id="Seg_6184" s="T15">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.001 (001.003)</ta>
            <ta e="T39" id="Seg_6185" s="T29">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.002 (001.004)</ta>
            <ta e="T152" id="Seg_6186" s="T149">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.003 (001.015)</ta>
            <ta e="T181" id="Seg_6187" s="T170">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.004 (001.019)</ta>
            <ta e="T276" id="Seg_6188" s="T272">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.005 (001.028)</ta>
            <ta e="T334" id="Seg_6189" s="T331">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.006 (001.035)</ta>
            <ta e="T391" id="Seg_6190" s="T389">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.007 (001.042)</ta>
            <ta e="T431" id="Seg_6191" s="T428">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.008 (001.046)</ta>
            <ta e="T478" id="Seg_6192" s="T475">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.009 (001.050)</ta>
            <ta e="T497" id="Seg_6193" s="T492">MaPX_KuNS_200X_YakutsOfEssej_conv.KuNS.010 (001.053)</ta>
         </annotation>
         <annotation name="st" tierref="st-KuNS">
            <ta e="T29" id="Seg_6194" s="T15">К: Эһиги бэйэгит төрүттэргитин туһунан, өйдөөтөккө, бэрт одуу, киһини одуургаата өһү кэпсиир көрдүккүт, быһылaак, дьэ.</ta>
            <ta e="T39" id="Seg_6195" s="T29">Эһиэй һакалара, Дьэкиэй Эһиэй дьонноро кайдиэгиттэн кэлбит этилэрэй ити һиргэ?</ta>
            <ta e="T152" id="Seg_6196" s="T149">К: Табалактаак, таба ээ…?</ta>
            <ta e="T181" id="Seg_6197" s="T170">К: Онтон Таймыртан ээ кайдак ээ эмиэ төттөрү ити Эвенкия диэк бардылар?</ta>
            <ta e="T276" id="Seg_6198" s="T272">К: Баайдарын былдьаатылар, табаларын, э-э?</ta>
            <ta e="T334" id="Seg_6199" s="T331">К: Айдаан турбута, э-э?</ta>
            <ta e="T391" id="Seg_6200" s="T389">К: Ньууччалар, э-э?</ta>
            <ta e="T431" id="Seg_6201" s="T428">К: Эһиги роткыт Маймаголар?</ta>
            <ta e="T478" id="Seg_6202" s="T475">К: Ким этэй һуруллара?</ta>
            <ta e="T497" id="Seg_6203" s="T492">К: Каннык совхозка этэ эбитэй ити?</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KuNS">
            <ta e="T29" id="Seg_6204" s="T15">– Ehigi bejegit törüttergitin tuhunan, öjdöːtökkö, bert oduː, kihini oduːrgaːta öhü kepsiːr kördükküt, bɨhɨlaːk, dʼe. </ta>
            <ta e="T39" id="Seg_6205" s="T29">Ehi͡ej hakalara, (Dʼeki͡ej) Ehi͡ej dʼonnoro kajdi͡egitten kelbit etilerej iti hirge? </ta>
            <ta e="T152" id="Seg_6206" s="T149">– Tabalartaːk, taba eː? </ta>
            <ta e="T181" id="Seg_6207" s="T170">– Onton Tajmɨːrtan eː kajdak eː emi͡e töttörü iti Evenkʼija di͡ek bardɨlar? </ta>
            <ta e="T276" id="Seg_6208" s="T272">– Baːjdarɨn bɨldʼaːtɨlar, tabalarɨn, eː? </ta>
            <ta e="T334" id="Seg_6209" s="T331">– Ajdaːn turbuta, eː? </ta>
            <ta e="T391" id="Seg_6210" s="T389">– Nʼuːččalar, eː? </ta>
            <ta e="T431" id="Seg_6211" s="T428">– Ehigi rotkɨt Majmagolar? </ta>
            <ta e="T478" id="Seg_6212" s="T475">– Kim etej hurullara? </ta>
            <ta e="T497" id="Seg_6213" s="T492">– Kannɨk savxozka ete ebitej iti? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KuNS">
            <ta e="T16" id="Seg_6214" s="T15">ehigi</ta>
            <ta e="T17" id="Seg_6215" s="T16">beje-git</ta>
            <ta e="T18" id="Seg_6216" s="T17">törüt-ter-giti-n</ta>
            <ta e="T19" id="Seg_6217" s="T18">tuh-u-nan</ta>
            <ta e="T20" id="Seg_6218" s="T19">öjdöː-tök-kö</ta>
            <ta e="T21" id="Seg_6219" s="T20">bert</ta>
            <ta e="T22" id="Seg_6220" s="T21">oduː</ta>
            <ta e="T23" id="Seg_6221" s="T22">kihi-ni</ta>
            <ta e="T24" id="Seg_6222" s="T23">oduːrgaː-t-a</ta>
            <ta e="T25" id="Seg_6223" s="T24">öh-ü</ta>
            <ta e="T26" id="Seg_6224" s="T25">kepsiː-r</ta>
            <ta e="T27" id="Seg_6225" s="T26">kördük-küt</ta>
            <ta e="T28" id="Seg_6226" s="T27">bɨhɨlaːk</ta>
            <ta e="T29" id="Seg_6227" s="T28">dʼe</ta>
            <ta e="T30" id="Seg_6228" s="T29">Ehi͡ej</ta>
            <ta e="T31" id="Seg_6229" s="T30">haka-lar-a</ta>
            <ta e="T32" id="Seg_6230" s="T31">Dʼeki͡ej</ta>
            <ta e="T33" id="Seg_6231" s="T32">Ehi͡ej</ta>
            <ta e="T34" id="Seg_6232" s="T33">dʼon-nor-o</ta>
            <ta e="T35" id="Seg_6233" s="T34">kajdi͡egitten</ta>
            <ta e="T36" id="Seg_6234" s="T35">kel-bit</ta>
            <ta e="T37" id="Seg_6235" s="T36">e-ti-lere=j</ta>
            <ta e="T38" id="Seg_6236" s="T37">iti</ta>
            <ta e="T39" id="Seg_6237" s="T38">hir-ge</ta>
            <ta e="T150" id="Seg_6238" s="T149">taba-lar-taːk</ta>
            <ta e="T151" id="Seg_6239" s="T150">taba</ta>
            <ta e="T152" id="Seg_6240" s="T151">eː</ta>
            <ta e="T171" id="Seg_6241" s="T170">onton</ta>
            <ta e="T172" id="Seg_6242" s="T171">Tajmɨːr-tan</ta>
            <ta e="T173" id="Seg_6243" s="T172">eː</ta>
            <ta e="T174" id="Seg_6244" s="T173">kajdak</ta>
            <ta e="T175" id="Seg_6245" s="T174">eː</ta>
            <ta e="T176" id="Seg_6246" s="T175">emi͡e</ta>
            <ta e="T177" id="Seg_6247" s="T176">töttörü</ta>
            <ta e="T178" id="Seg_6248" s="T177">iti</ta>
            <ta e="T179" id="Seg_6249" s="T178">Evenkʼija</ta>
            <ta e="T180" id="Seg_6250" s="T179">di͡ek</ta>
            <ta e="T181" id="Seg_6251" s="T180">bar-dɨ-lar</ta>
            <ta e="T273" id="Seg_6252" s="T272">baːj-darɨ-n</ta>
            <ta e="T274" id="Seg_6253" s="T273">bɨldʼaː-tɨ-lar</ta>
            <ta e="T275" id="Seg_6254" s="T274">taba-larɨ-n</ta>
            <ta e="T276" id="Seg_6255" s="T275">eː</ta>
            <ta e="T332" id="Seg_6256" s="T331">ajdaːn</ta>
            <ta e="T333" id="Seg_6257" s="T332">tur-but-a</ta>
            <ta e="T334" id="Seg_6258" s="T333">eː</ta>
            <ta e="T390" id="Seg_6259" s="T389">nʼuːčča-lar</ta>
            <ta e="T391" id="Seg_6260" s="T390">eː</ta>
            <ta e="T429" id="Seg_6261" s="T428">ehigi</ta>
            <ta e="T430" id="Seg_6262" s="T429">rot-kɨt</ta>
            <ta e="T431" id="Seg_6263" s="T430">Majmago-lar</ta>
            <ta e="T476" id="Seg_6264" s="T475">kim</ta>
            <ta e="T477" id="Seg_6265" s="T476">e-t-e=j</ta>
            <ta e="T478" id="Seg_6266" s="T477">hurullar-a</ta>
            <ta e="T493" id="Seg_6267" s="T492">kannɨk</ta>
            <ta e="T494" id="Seg_6268" s="T493">savxoz-ka</ta>
            <ta e="T495" id="Seg_6269" s="T494">e-t-e</ta>
            <ta e="T496" id="Seg_6270" s="T495">e-bit-e=j</ta>
            <ta e="T497" id="Seg_6271" s="T496">iti</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KuNS">
            <ta e="T16" id="Seg_6272" s="T15">ehigi</ta>
            <ta e="T17" id="Seg_6273" s="T16">beje-GIt</ta>
            <ta e="T18" id="Seg_6274" s="T17">törüt-LAr-GItI-n</ta>
            <ta e="T19" id="Seg_6275" s="T18">tus-tI-nAn</ta>
            <ta e="T20" id="Seg_6276" s="T19">öjdöː-TAK-GA</ta>
            <ta e="T21" id="Seg_6277" s="T20">bert</ta>
            <ta e="T22" id="Seg_6278" s="T21">oduː</ta>
            <ta e="T23" id="Seg_6279" s="T22">kihi-nI</ta>
            <ta e="T24" id="Seg_6280" s="T23">oduːrgaː-t-A</ta>
            <ta e="T25" id="Seg_6281" s="T24">ös-nI</ta>
            <ta e="T26" id="Seg_6282" s="T25">kepseː-Ar</ta>
            <ta e="T27" id="Seg_6283" s="T26">kördük-GIt</ta>
            <ta e="T28" id="Seg_6284" s="T27">bɨhɨːlaːk</ta>
            <ta e="T29" id="Seg_6285" s="T28">dʼe</ta>
            <ta e="T30" id="Seg_6286" s="T29">Essej</ta>
            <ta e="T31" id="Seg_6287" s="T30">haka-LAr-tA</ta>
            <ta e="T32" id="Seg_6288" s="T31">Essej</ta>
            <ta e="T33" id="Seg_6289" s="T32">Essej</ta>
            <ta e="T34" id="Seg_6290" s="T33">dʼon-LAr-tA</ta>
            <ta e="T35" id="Seg_6291" s="T34">kajdi͡egitten</ta>
            <ta e="T36" id="Seg_6292" s="T35">kel-BIT</ta>
            <ta e="T37" id="Seg_6293" s="T36">e-TI-LArA=Ij</ta>
            <ta e="T38" id="Seg_6294" s="T37">iti</ta>
            <ta e="T39" id="Seg_6295" s="T38">hir-GA</ta>
            <ta e="T150" id="Seg_6296" s="T149">taba-LAr-LAːK</ta>
            <ta e="T151" id="Seg_6297" s="T150">taba</ta>
            <ta e="T152" id="Seg_6298" s="T151">eː</ta>
            <ta e="T171" id="Seg_6299" s="T170">onton</ta>
            <ta e="T172" id="Seg_6300" s="T171">Tajmɨr-ttAn</ta>
            <ta e="T173" id="Seg_6301" s="T172">eː</ta>
            <ta e="T174" id="Seg_6302" s="T173">kajdak</ta>
            <ta e="T175" id="Seg_6303" s="T174">eː</ta>
            <ta e="T176" id="Seg_6304" s="T175">emi͡e</ta>
            <ta e="T177" id="Seg_6305" s="T176">töttörü</ta>
            <ta e="T178" id="Seg_6306" s="T177">iti</ta>
            <ta e="T179" id="Seg_6307" s="T178">Evʼenkʼiːja</ta>
            <ta e="T180" id="Seg_6308" s="T179">dek</ta>
            <ta e="T181" id="Seg_6309" s="T180">bar-TI-LAr</ta>
            <ta e="T273" id="Seg_6310" s="T272">baːj-LArI-n</ta>
            <ta e="T274" id="Seg_6311" s="T273">bɨldʼaː-TI-LAr</ta>
            <ta e="T275" id="Seg_6312" s="T274">taba-LArI-n</ta>
            <ta e="T276" id="Seg_6313" s="T275">eː</ta>
            <ta e="T332" id="Seg_6314" s="T331">ajdaːn</ta>
            <ta e="T333" id="Seg_6315" s="T332">tur-BIT-tA</ta>
            <ta e="T334" id="Seg_6316" s="T333">eː</ta>
            <ta e="T390" id="Seg_6317" s="T389">nuːčča-LAr</ta>
            <ta e="T391" id="Seg_6318" s="T390">eː</ta>
            <ta e="T429" id="Seg_6319" s="T428">ehigi</ta>
            <ta e="T430" id="Seg_6320" s="T429">roːt-GIt</ta>
            <ta e="T431" id="Seg_6321" s="T430">Majmago-LAr</ta>
            <ta e="T476" id="Seg_6322" s="T475">kim</ta>
            <ta e="T477" id="Seg_6323" s="T476">e-TI-tA=Ij</ta>
            <ta e="T478" id="Seg_6324" s="T477">hurullar-tA</ta>
            <ta e="T493" id="Seg_6325" s="T492">kannɨk</ta>
            <ta e="T494" id="Seg_6326" s="T493">hopku͡os-GA</ta>
            <ta e="T495" id="Seg_6327" s="T494">e-TI-tA</ta>
            <ta e="T496" id="Seg_6328" s="T495">e-BIT-tA=Ij</ta>
            <ta e="T497" id="Seg_6329" s="T496">iti</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KuNS">
            <ta e="T16" id="Seg_6330" s="T15">2PL.[NOM]</ta>
            <ta e="T17" id="Seg_6331" s="T16">self-2PL.[NOM]</ta>
            <ta e="T18" id="Seg_6332" s="T17">ancestor-PL-2PL-GEN</ta>
            <ta e="T19" id="Seg_6333" s="T18">side-3SG-INSTR</ta>
            <ta e="T20" id="Seg_6334" s="T19">remember-PTCP.COND-DAT/LOC</ta>
            <ta e="T21" id="Seg_6335" s="T20">very</ta>
            <ta e="T22" id="Seg_6336" s="T21">strange</ta>
            <ta e="T23" id="Seg_6337" s="T22">human.being-ACC</ta>
            <ta e="T24" id="Seg_6338" s="T23">be.surprised-CAUS-CVB.SIM</ta>
            <ta e="T25" id="Seg_6339" s="T24">story-ACC</ta>
            <ta e="T26" id="Seg_6340" s="T25">tell-PTCP.PRS.[NOM]</ta>
            <ta e="T27" id="Seg_6341" s="T26">like-2PL</ta>
            <ta e="T28" id="Seg_6342" s="T27">apparently</ta>
            <ta e="T29" id="Seg_6343" s="T28">well</ta>
            <ta e="T30" id="Seg_6344" s="T29">Essej.[NOM]</ta>
            <ta e="T31" id="Seg_6345" s="T30">Yakut-PL-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_6346" s="T31">Essej</ta>
            <ta e="T33" id="Seg_6347" s="T32">Essej.[NOM]</ta>
            <ta e="T34" id="Seg_6348" s="T33">people-PL-3SG.[NOM]</ta>
            <ta e="T35" id="Seg_6349" s="T34">where.from</ta>
            <ta e="T36" id="Seg_6350" s="T35">come-PTCP.PST</ta>
            <ta e="T37" id="Seg_6351" s="T36">be-PST1-3PL=Q</ta>
            <ta e="T38" id="Seg_6352" s="T37">that</ta>
            <ta e="T39" id="Seg_6353" s="T38">place-DAT/LOC</ta>
            <ta e="T150" id="Seg_6354" s="T149">reindeer-PL-PROPR</ta>
            <ta e="T151" id="Seg_6355" s="T150">reindeer.[NOM]</ta>
            <ta e="T152" id="Seg_6356" s="T151">hey</ta>
            <ta e="T171" id="Seg_6357" s="T170">then</ta>
            <ta e="T172" id="Seg_6358" s="T171">Taymyr-ABL</ta>
            <ta e="T173" id="Seg_6359" s="T172">eh</ta>
            <ta e="T174" id="Seg_6360" s="T173">how</ta>
            <ta e="T175" id="Seg_6361" s="T174">eh</ta>
            <ta e="T176" id="Seg_6362" s="T175">again</ta>
            <ta e="T177" id="Seg_6363" s="T176">back</ta>
            <ta e="T178" id="Seg_6364" s="T177">that</ta>
            <ta e="T179" id="Seg_6365" s="T178">Evenkia.[NOM]</ta>
            <ta e="T180" id="Seg_6366" s="T179">to</ta>
            <ta e="T181" id="Seg_6367" s="T180">go-PST1-3PL</ta>
            <ta e="T273" id="Seg_6368" s="T272">wealth-3PL-ACC</ta>
            <ta e="T274" id="Seg_6369" s="T273">take.away-PST1-3PL</ta>
            <ta e="T275" id="Seg_6370" s="T274">reindeer-3PL-ACC</ta>
            <ta e="T276" id="Seg_6371" s="T275">AFFIRM</ta>
            <ta e="T332" id="Seg_6372" s="T331">noise.[NOM]</ta>
            <ta e="T333" id="Seg_6373" s="T332">stand.up-PST2-3SG</ta>
            <ta e="T334" id="Seg_6374" s="T333">AFFIRM</ta>
            <ta e="T390" id="Seg_6375" s="T389">Russian-PL.[NOM]</ta>
            <ta e="T391" id="Seg_6376" s="T390">AFFIRM</ta>
            <ta e="T429" id="Seg_6377" s="T428">2PL.[NOM]</ta>
            <ta e="T430" id="Seg_6378" s="T429">tribe-2PL.[NOM]</ta>
            <ta e="T431" id="Seg_6379" s="T430">Majmago-PL.[NOM]</ta>
            <ta e="T476" id="Seg_6380" s="T475">who.[NOM]</ta>
            <ta e="T477" id="Seg_6381" s="T476">be-PST1-3SG=Q</ta>
            <ta e="T478" id="Seg_6382" s="T477">surname-3SG.[NOM]</ta>
            <ta e="T493" id="Seg_6383" s="T492">what.kind.of</ta>
            <ta e="T494" id="Seg_6384" s="T493">sovkhoz-DAT/LOC</ta>
            <ta e="T495" id="Seg_6385" s="T494">be-PST1-3SG</ta>
            <ta e="T496" id="Seg_6386" s="T495">be-PST2-3SG=Q</ta>
            <ta e="T497" id="Seg_6387" s="T496">that.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KuNS">
            <ta e="T16" id="Seg_6388" s="T15">2PL.[NOM]</ta>
            <ta e="T17" id="Seg_6389" s="T16">selbst-2PL.[NOM]</ta>
            <ta e="T18" id="Seg_6390" s="T17">Vorfahr-PL-2PL-GEN</ta>
            <ta e="T19" id="Seg_6391" s="T18">Seite-3SG-INSTR</ta>
            <ta e="T20" id="Seg_6392" s="T19">erinnern-PTCP.COND-DAT/LOC</ta>
            <ta e="T21" id="Seg_6393" s="T20">sehr</ta>
            <ta e="T22" id="Seg_6394" s="T21">merkwürdig</ta>
            <ta e="T23" id="Seg_6395" s="T22">Mensch-ACC</ta>
            <ta e="T24" id="Seg_6396" s="T23">sich.wundern-CAUS-CVB.SIM</ta>
            <ta e="T25" id="Seg_6397" s="T24">Erzählung-ACC</ta>
            <ta e="T26" id="Seg_6398" s="T25">erzählen-PTCP.PRS.[NOM]</ta>
            <ta e="T27" id="Seg_6399" s="T26">wie-2PL</ta>
            <ta e="T28" id="Seg_6400" s="T27">offenbar</ta>
            <ta e="T29" id="Seg_6401" s="T28">doch</ta>
            <ta e="T30" id="Seg_6402" s="T29">Essej.[NOM]</ta>
            <ta e="T31" id="Seg_6403" s="T30">Jakute-PL-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_6404" s="T31">Essej</ta>
            <ta e="T33" id="Seg_6405" s="T32">Essej.[NOM]</ta>
            <ta e="T34" id="Seg_6406" s="T33">Leute-PL-3SG.[NOM]</ta>
            <ta e="T35" id="Seg_6407" s="T34">woher</ta>
            <ta e="T36" id="Seg_6408" s="T35">kommen-PTCP.PST</ta>
            <ta e="T37" id="Seg_6409" s="T36">sein-PST1-3PL=Q</ta>
            <ta e="T38" id="Seg_6410" s="T37">dieses</ta>
            <ta e="T39" id="Seg_6411" s="T38">Ort-DAT/LOC</ta>
            <ta e="T150" id="Seg_6412" s="T149">Rentier-PL-PROPR</ta>
            <ta e="T151" id="Seg_6413" s="T150">Rentier.[NOM]</ta>
            <ta e="T152" id="Seg_6414" s="T151">hey</ta>
            <ta e="T171" id="Seg_6415" s="T170">dann</ta>
            <ta e="T172" id="Seg_6416" s="T171">Taimyr-ABL</ta>
            <ta e="T173" id="Seg_6417" s="T172">äh</ta>
            <ta e="T174" id="Seg_6418" s="T173">wie</ta>
            <ta e="T175" id="Seg_6419" s="T174">äh</ta>
            <ta e="T176" id="Seg_6420" s="T175">wieder</ta>
            <ta e="T177" id="Seg_6421" s="T176">zurück</ta>
            <ta e="T178" id="Seg_6422" s="T177">dieses</ta>
            <ta e="T179" id="Seg_6423" s="T178">Ewenkien.[NOM]</ta>
            <ta e="T180" id="Seg_6424" s="T179">zu</ta>
            <ta e="T181" id="Seg_6425" s="T180">gehen-PST1-3PL</ta>
            <ta e="T273" id="Seg_6426" s="T272">Reichtum-3PL-ACC</ta>
            <ta e="T274" id="Seg_6427" s="T273">wegnehmen-PST1-3PL</ta>
            <ta e="T275" id="Seg_6428" s="T274">Rentier-3PL-ACC</ta>
            <ta e="T276" id="Seg_6429" s="T275">AFFIRM</ta>
            <ta e="T332" id="Seg_6430" s="T331">Lärm.[NOM]</ta>
            <ta e="T333" id="Seg_6431" s="T332">aufstehen-PST2-3SG</ta>
            <ta e="T334" id="Seg_6432" s="T333">AFFIRM</ta>
            <ta e="T390" id="Seg_6433" s="T389">Russe-PL.[NOM]</ta>
            <ta e="T391" id="Seg_6434" s="T390">AFFIRM</ta>
            <ta e="T429" id="Seg_6435" s="T428">2PL.[NOM]</ta>
            <ta e="T430" id="Seg_6436" s="T429">Stamm-2PL.[NOM]</ta>
            <ta e="T431" id="Seg_6437" s="T430">Majmago-PL.[NOM]</ta>
            <ta e="T476" id="Seg_6438" s="T475">wer.[NOM]</ta>
            <ta e="T477" id="Seg_6439" s="T476">sein-PST1-3SG=Q</ta>
            <ta e="T478" id="Seg_6440" s="T477">Nachname-3SG.[NOM]</ta>
            <ta e="T493" id="Seg_6441" s="T492">was.für.ein</ta>
            <ta e="T494" id="Seg_6442" s="T493">Sowchose-DAT/LOC</ta>
            <ta e="T495" id="Seg_6443" s="T494">sein-PST1-3SG</ta>
            <ta e="T496" id="Seg_6444" s="T495">sein-PST2-3SG=Q</ta>
            <ta e="T497" id="Seg_6445" s="T496">dieses.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KuNS">
            <ta e="T16" id="Seg_6446" s="T15">2PL.[NOM]</ta>
            <ta e="T17" id="Seg_6447" s="T16">сам-2PL.[NOM]</ta>
            <ta e="T18" id="Seg_6448" s="T17">предок-PL-2PL-GEN</ta>
            <ta e="T19" id="Seg_6449" s="T18">сторона-3SG-INSTR</ta>
            <ta e="T20" id="Seg_6450" s="T19">помнить-PTCP.COND-DAT/LOC</ta>
            <ta e="T21" id="Seg_6451" s="T20">очень</ta>
            <ta e="T22" id="Seg_6452" s="T21">странный</ta>
            <ta e="T23" id="Seg_6453" s="T22">человек-ACC</ta>
            <ta e="T24" id="Seg_6454" s="T23">удивляться-CAUS-CVB.SIM</ta>
            <ta e="T25" id="Seg_6455" s="T24">рассказ-ACC</ta>
            <ta e="T26" id="Seg_6456" s="T25">рассказывать-PTCP.PRS.[NOM]</ta>
            <ta e="T27" id="Seg_6457" s="T26">как-2PL</ta>
            <ta e="T28" id="Seg_6458" s="T27">наверное</ta>
            <ta e="T29" id="Seg_6459" s="T28">вот</ta>
            <ta e="T30" id="Seg_6460" s="T29">Ессей.[NOM]</ta>
            <ta e="T31" id="Seg_6461" s="T30">якут-PL-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_6462" s="T31">Ессей</ta>
            <ta e="T33" id="Seg_6463" s="T32">Ессей.[NOM]</ta>
            <ta e="T34" id="Seg_6464" s="T33">люди-PL-3SG.[NOM]</ta>
            <ta e="T35" id="Seg_6465" s="T34">откуда</ta>
            <ta e="T36" id="Seg_6466" s="T35">приходить-PTCP.PST</ta>
            <ta e="T37" id="Seg_6467" s="T36">быть-PST1-3PL=Q</ta>
            <ta e="T38" id="Seg_6468" s="T37">тот</ta>
            <ta e="T39" id="Seg_6469" s="T38">место-DAT/LOC</ta>
            <ta e="T150" id="Seg_6470" s="T149">олень-PL-PROPR</ta>
            <ta e="T151" id="Seg_6471" s="T150">олень.[NOM]</ta>
            <ta e="T152" id="Seg_6472" s="T151">ээ</ta>
            <ta e="T171" id="Seg_6473" s="T170">потом</ta>
            <ta e="T172" id="Seg_6474" s="T171">Таймыр-ABL</ta>
            <ta e="T173" id="Seg_6475" s="T172">ээ</ta>
            <ta e="T174" id="Seg_6476" s="T173">как</ta>
            <ta e="T175" id="Seg_6477" s="T174">ээ</ta>
            <ta e="T176" id="Seg_6478" s="T175">опять</ta>
            <ta e="T177" id="Seg_6479" s="T176">назад</ta>
            <ta e="T178" id="Seg_6480" s="T177">тот</ta>
            <ta e="T179" id="Seg_6481" s="T178">Эвенкия.[NOM]</ta>
            <ta e="T180" id="Seg_6482" s="T179">к</ta>
            <ta e="T181" id="Seg_6483" s="T180">идти-PST1-3PL</ta>
            <ta e="T273" id="Seg_6484" s="T272">богатство-3PL-ACC</ta>
            <ta e="T274" id="Seg_6485" s="T273">отнимать-PST1-3PL</ta>
            <ta e="T275" id="Seg_6486" s="T274">олень-3PL-ACC</ta>
            <ta e="T276" id="Seg_6487" s="T275">AFFIRM</ta>
            <ta e="T332" id="Seg_6488" s="T331">шум.[NOM]</ta>
            <ta e="T333" id="Seg_6489" s="T332">вставать-PST2-3SG</ta>
            <ta e="T334" id="Seg_6490" s="T333">AFFIRM</ta>
            <ta e="T390" id="Seg_6491" s="T389">русский-PL.[NOM]</ta>
            <ta e="T391" id="Seg_6492" s="T390">AFFIRM</ta>
            <ta e="T429" id="Seg_6493" s="T428">2PL.[NOM]</ta>
            <ta e="T430" id="Seg_6494" s="T429">род-2PL.[NOM]</ta>
            <ta e="T431" id="Seg_6495" s="T430">Маймаго-PL.[NOM]</ta>
            <ta e="T476" id="Seg_6496" s="T475">кто.[NOM]</ta>
            <ta e="T477" id="Seg_6497" s="T476">быть-PST1-3SG=Q</ta>
            <ta e="T478" id="Seg_6498" s="T477">фамилия-3SG.[NOM]</ta>
            <ta e="T493" id="Seg_6499" s="T492">какой</ta>
            <ta e="T494" id="Seg_6500" s="T493">совхоз-DAT/LOC</ta>
            <ta e="T495" id="Seg_6501" s="T494">быть-PST1-3SG</ta>
            <ta e="T496" id="Seg_6502" s="T495">быть-PST2-3SG=Q</ta>
            <ta e="T497" id="Seg_6503" s="T496">тот.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KuNS">
            <ta e="T16" id="Seg_6504" s="T15">pers-pro:case</ta>
            <ta e="T17" id="Seg_6505" s="T16">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T18" id="Seg_6506" s="T17">n-n:(num)-n:poss-n:case</ta>
            <ta e="T19" id="Seg_6507" s="T18">n-n:poss-n:case</ta>
            <ta e="T20" id="Seg_6508" s="T19">v-v:ptcp-v:(case)</ta>
            <ta e="T21" id="Seg_6509" s="T20">adv</ta>
            <ta e="T22" id="Seg_6510" s="T21">adj</ta>
            <ta e="T23" id="Seg_6511" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_6512" s="T23">v-v&gt;v-v:cvb</ta>
            <ta e="T25" id="Seg_6513" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_6514" s="T25">v-v:ptcp-v:(case)</ta>
            <ta e="T27" id="Seg_6515" s="T26">post-n:(pred.pn)</ta>
            <ta e="T28" id="Seg_6516" s="T27">adv</ta>
            <ta e="T29" id="Seg_6517" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_6518" s="T29">propr-n:case</ta>
            <ta e="T31" id="Seg_6519" s="T30">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T32" id="Seg_6520" s="T31">propr</ta>
            <ta e="T33" id="Seg_6521" s="T32">propr-n:case</ta>
            <ta e="T34" id="Seg_6522" s="T33">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T35" id="Seg_6523" s="T34">que</ta>
            <ta e="T36" id="Seg_6524" s="T35">v-v:ptcp</ta>
            <ta e="T37" id="Seg_6525" s="T36">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T38" id="Seg_6526" s="T37">dempro</ta>
            <ta e="T39" id="Seg_6527" s="T38">n-n:case</ta>
            <ta e="T150" id="Seg_6528" s="T149">n-n:(num)-n&gt;adj</ta>
            <ta e="T151" id="Seg_6529" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_6530" s="T151">interj</ta>
            <ta e="T171" id="Seg_6531" s="T170">adv</ta>
            <ta e="T172" id="Seg_6532" s="T171">propr-n:case</ta>
            <ta e="T173" id="Seg_6533" s="T172">interj</ta>
            <ta e="T174" id="Seg_6534" s="T173">que</ta>
            <ta e="T175" id="Seg_6535" s="T174">interj</ta>
            <ta e="T176" id="Seg_6536" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_6537" s="T176">adv</ta>
            <ta e="T178" id="Seg_6538" s="T177">dempro</ta>
            <ta e="T179" id="Seg_6539" s="T178">propr-n:case</ta>
            <ta e="T180" id="Seg_6540" s="T179">post</ta>
            <ta e="T181" id="Seg_6541" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_6542" s="T272">n-n:poss-n:case</ta>
            <ta e="T274" id="Seg_6543" s="T273">v-v:tense-v:pred.pn</ta>
            <ta e="T275" id="Seg_6544" s="T274">n-n:poss-n:case</ta>
            <ta e="T276" id="Seg_6545" s="T275">ptcl</ta>
            <ta e="T332" id="Seg_6546" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_6547" s="T332">v-v:tense-v:poss.pn</ta>
            <ta e="T334" id="Seg_6548" s="T333">ptcl</ta>
            <ta e="T390" id="Seg_6549" s="T389">n-n:(num)-n:case</ta>
            <ta e="T391" id="Seg_6550" s="T390">ptcl</ta>
            <ta e="T429" id="Seg_6551" s="T428">pers-pro:case</ta>
            <ta e="T430" id="Seg_6552" s="T429">n-n:(poss)-n:case</ta>
            <ta e="T431" id="Seg_6553" s="T430">propr-n:(num)-n:case</ta>
            <ta e="T476" id="Seg_6554" s="T475">que-pro:case</ta>
            <ta e="T477" id="Seg_6555" s="T476">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T478" id="Seg_6556" s="T477">n-n:(poss)-n:case</ta>
            <ta e="T493" id="Seg_6557" s="T492">que</ta>
            <ta e="T494" id="Seg_6558" s="T493">n-n:case</ta>
            <ta e="T495" id="Seg_6559" s="T494">v-v:tense-v:poss.pn</ta>
            <ta e="T496" id="Seg_6560" s="T495">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T497" id="Seg_6561" s="T496">dempro-pro:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KuNS">
            <ta e="T16" id="Seg_6562" s="T15">pers</ta>
            <ta e="T17" id="Seg_6563" s="T16">emphpro</ta>
            <ta e="T18" id="Seg_6564" s="T17">n</ta>
            <ta e="T19" id="Seg_6565" s="T18">n</ta>
            <ta e="T20" id="Seg_6566" s="T19">v</ta>
            <ta e="T21" id="Seg_6567" s="T20">adv</ta>
            <ta e="T22" id="Seg_6568" s="T21">adj</ta>
            <ta e="T23" id="Seg_6569" s="T22">n</ta>
            <ta e="T24" id="Seg_6570" s="T23">v</ta>
            <ta e="T25" id="Seg_6571" s="T24">n</ta>
            <ta e="T26" id="Seg_6572" s="T25">v</ta>
            <ta e="T27" id="Seg_6573" s="T26">post</ta>
            <ta e="T28" id="Seg_6574" s="T27">adv</ta>
            <ta e="T29" id="Seg_6575" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_6576" s="T29">propr</ta>
            <ta e="T31" id="Seg_6577" s="T30">n</ta>
            <ta e="T32" id="Seg_6578" s="T31">propr</ta>
            <ta e="T33" id="Seg_6579" s="T32">propr</ta>
            <ta e="T34" id="Seg_6580" s="T33">n</ta>
            <ta e="T35" id="Seg_6581" s="T34">que</ta>
            <ta e="T36" id="Seg_6582" s="T35">v</ta>
            <ta e="T37" id="Seg_6583" s="T36">aux</ta>
            <ta e="T38" id="Seg_6584" s="T37">dempro</ta>
            <ta e="T39" id="Seg_6585" s="T38">n</ta>
            <ta e="T150" id="Seg_6586" s="T149">adj</ta>
            <ta e="T151" id="Seg_6587" s="T150">n</ta>
            <ta e="T152" id="Seg_6588" s="T151">interj</ta>
            <ta e="T171" id="Seg_6589" s="T170">adv</ta>
            <ta e="T172" id="Seg_6590" s="T171">propr</ta>
            <ta e="T173" id="Seg_6591" s="T172">interj</ta>
            <ta e="T174" id="Seg_6592" s="T173">que</ta>
            <ta e="T175" id="Seg_6593" s="T174">interj</ta>
            <ta e="T176" id="Seg_6594" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_6595" s="T176">adv</ta>
            <ta e="T178" id="Seg_6596" s="T177">dempro</ta>
            <ta e="T179" id="Seg_6597" s="T178">propr</ta>
            <ta e="T180" id="Seg_6598" s="T179">post</ta>
            <ta e="T181" id="Seg_6599" s="T180">v</ta>
            <ta e="T273" id="Seg_6600" s="T272">n</ta>
            <ta e="T274" id="Seg_6601" s="T273">v</ta>
            <ta e="T275" id="Seg_6602" s="T274">n</ta>
            <ta e="T276" id="Seg_6603" s="T275">ptcl</ta>
            <ta e="T332" id="Seg_6604" s="T331">n</ta>
            <ta e="T333" id="Seg_6605" s="T332">v</ta>
            <ta e="T334" id="Seg_6606" s="T333">ptcl</ta>
            <ta e="T390" id="Seg_6607" s="T389">n</ta>
            <ta e="T391" id="Seg_6608" s="T390">ptcl</ta>
            <ta e="T429" id="Seg_6609" s="T428">pers</ta>
            <ta e="T430" id="Seg_6610" s="T429">n</ta>
            <ta e="T431" id="Seg_6611" s="T430">propr</ta>
            <ta e="T476" id="Seg_6612" s="T475">que</ta>
            <ta e="T477" id="Seg_6613" s="T476">cop</ta>
            <ta e="T478" id="Seg_6614" s="T477">n</ta>
            <ta e="T493" id="Seg_6615" s="T492">que</ta>
            <ta e="T494" id="Seg_6616" s="T493">n</ta>
            <ta e="T495" id="Seg_6617" s="T494">cop</ta>
            <ta e="T496" id="Seg_6618" s="T495">aux</ta>
            <ta e="T497" id="Seg_6619" s="T496">dempro</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KuNS" />
         <annotation name="SyF" tierref="SyF-KuNS" />
         <annotation name="IST" tierref="IST-KuNS" />
         <annotation name="Top" tierref="Top-KuNS" />
         <annotation name="Foc" tierref="Foc-KuNS" />
         <annotation name="BOR" tierref="BOR-KuNS">
            <ta e="T30" id="Seg_6620" s="T29">RUS:cult</ta>
            <ta e="T32" id="Seg_6621" s="T31">RUS:cult</ta>
            <ta e="T33" id="Seg_6622" s="T32">RUS:cult</ta>
            <ta e="T430" id="Seg_6623" s="T429">RUS:cult</ta>
            <ta e="T431" id="Seg_6624" s="T430">EV:cult</ta>
            <ta e="T494" id="Seg_6625" s="T493">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KuNS" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KuNS" />
         <annotation name="CS" tierref="CS-KuNS" />
         <annotation name="fe" tierref="fe-KuNS">
            <ta e="T29" id="Seg_6626" s="T15">– About Your own ancestors, if one reminds them, you can tell very interesting stories which astonish the people, apparently.</ta>
            <ta e="T39" id="Seg_6627" s="T29">The Essey Yakuts, where did the Essey people come from to this place?</ta>
            <ta e="T152" id="Seg_6628" s="T149">– With reindeer, reindeer, eh?</ta>
            <ta e="T181" id="Seg_6629" s="T170">– And then they went back from Taymyr to Evenkia?</ta>
            <ta e="T276" id="Seg_6630" s="T272">– They took away the wealth, the reindeer, right?</ta>
            <ta e="T334" id="Seg_6631" s="T331">– A riot came up, right?</ta>
            <ta e="T391" id="Seg_6632" s="T389">– Russians, right?</ta>
            <ta e="T431" id="Seg_6633" s="T428">– Your tribe is Majmago, right?</ta>
            <ta e="T478" id="Seg_6634" s="T475">– What was his surname?</ta>
            <ta e="T497" id="Seg_6635" s="T492">– In which Sovkhoz was it?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KuNS">
            <ta e="T29" id="Seg_6636" s="T15">– Über Ihre eigenen Vorfahren, wenn man sich erinnert, können sie sehr interessante, die Leute verwundernde Geschichten erzählen, wahrscheinlich.</ta>
            <ta e="T39" id="Seg_6637" s="T29">Die Jakuten von Essej, woher kamen die Leute von Essej an diesen Ort?</ta>
            <ta e="T152" id="Seg_6638" s="T149">– Mit Rentieren, Rentier, äh?</ta>
            <ta e="T181" id="Seg_6639" s="T170">– Und von der Taimyr sind sie dann wieder zurück nach Ewenkien gegangen?</ta>
            <ta e="T276" id="Seg_6640" s="T272">– Sie nehmen den Reichtum weg, die Rentiere, ja?</ta>
            <ta e="T334" id="Seg_6641" s="T331">– Ein Aufstand brach los, ja?</ta>
            <ta e="T391" id="Seg_6642" s="T389">– Russen, ja?</ta>
            <ta e="T431" id="Seg_6643" s="T428">– Ihr Stamm sind die Majmagos, ja?</ta>
            <ta e="T478" id="Seg_6644" s="T475">– Wie war sein Nachname?</ta>
            <ta e="T497" id="Seg_6645" s="T492">– Auf welcher Sowchose war das?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KuNS" />
         <annotation name="ltr" tierref="ltr-KuNS">
            <ta e="T29" id="Seg_6646" s="T15">К: О ваших своих предках, если вспомнить, очень удивительную, интересную для людей историю можете рассказать, наверное.</ta>
            <ta e="T39" id="Seg_6647" s="T29">Ессейские долганы (якуты), ессейские люди откуда пришли на эту землю?</ta>
            <ta e="T152" id="Seg_6648" s="T149">К: На оленях?</ta>
            <ta e="T181" id="Seg_6649" s="T170">К: А потом из Таймыра как опять обратно в Эвенкию уехали?</ta>
            <ta e="T276" id="Seg_6650" s="T272">К: Богатство изъяли(раскулачили) и оленей тоже.?</ta>
            <ta e="T334" id="Seg_6651" s="T331">К: Взбунтовались (Шум поднялся), да?</ta>
            <ta e="T391" id="Seg_6652" s="T389">К: Русские, да?</ta>
            <ta e="T431" id="Seg_6653" s="T428">К: Ваш род Маймаго?</ta>
            <ta e="T478" id="Seg_6654" s="T475">К: Какая у него была фамилия?</ta>
            <ta e="T497" id="Seg_6655" s="T492">К: В каком совхозе это было?</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KuNS" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T379" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-MaPX"
                          name="ref"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-MaPX"
                          name="st"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-MaPX"
                          name="ts"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-MaPX"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-MaPX"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-MaPX"
                          name="mb"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-MaPX"
                          name="mp"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-MaPX"
                          name="ge"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-MaPX"
                          name="gg"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-MaPX"
                          name="gr"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-MaPX"
                          name="mc"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-MaPX"
                          name="ps"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-MaPX"
                          name="SeR"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-MaPX"
                          name="SyF"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-MaPX"
                          name="IST"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-MaPX"
                          name="Top"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-MaPX"
                          name="Foc"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-MaPX"
                          name="BOR"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-MaPX"
                          name="BOR-Phon"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-MaPX"
                          name="BOR-Morph"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-MaPX"
                          name="CS"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-MaPX"
                          name="fe"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-MaPX"
                          name="fg"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-MaPX"
                          name="fr"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-MaPX"
                          name="ltr"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-MaPX"
                          name="nt"
                          segmented-tier-id="tx-MaPX"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KuNS"
                          name="ref"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KuNS"
                          name="st"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KuNS"
                          name="ts"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KuNS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KuNS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KuNS"
                          name="mb"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KuNS"
                          name="mp"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KuNS"
                          name="ge"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KuNS"
                          name="gg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KuNS"
                          name="gr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KuNS"
                          name="mc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KuNS"
                          name="ps"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KuNS"
                          name="SeR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KuNS"
                          name="SyF"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KuNS"
                          name="IST"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KuNS"
                          name="Top"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KuNS"
                          name="Foc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KuNS"
                          name="BOR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KuNS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KuNS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KuNS"
                          name="CS"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KuNS"
                          name="fe"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KuNS"
                          name="fg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KuNS"
                          name="fr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KuNS"
                          name="ltr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KuNS"
                          name="nt"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
