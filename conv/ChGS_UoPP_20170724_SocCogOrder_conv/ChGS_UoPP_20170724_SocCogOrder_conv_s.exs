<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA93D8857-2D1A-C476-6C0A-45ACDDD7A366">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>ChGS_UoPP_20170724_SocCogRightOrder_conv</transcription-name>
         <referenced-file url="ChGS_UoPP_20170724_SocCogOrder_conv.wav" />
         <referenced-file url="ChGS_UoPP_20170724_SocCogOrder_conv.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\conv\ChGS_UoPP_20170724_SocCogOrder_conv\ChGS_UoPP_20170724_SocCogOrder_conv.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1142</ud-information>
            <ud-information attribute-name="# HIAT:w">620</ud-information>
            <ud-information attribute-name="# e">632</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">12</ud-information>
            <ud-information attribute-name="# HIAT:u">142</ud-information>
            <ud-information attribute-name="# sc">272</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="DCh">
            <abbreviation>DCh</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="ChGS">
            <abbreviation>ChGS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="UoPP">
            <abbreviation>UoPP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.6666000000000001" type="appl" />
         <tli id="T3" time="1.3332000000000002" type="appl" />
         <tli id="T4" time="1.9998000000000002" type="appl" />
         <tli id="T5" time="2.6664000000000003" type="appl" />
         <tli id="T6" time="3.028" type="appl" />
         <tli id="T7" time="3.333" type="appl" />
         <tli id="T8" time="3.416" type="appl" />
         <tli id="T9" time="3.952" type="appl" />
         <tli id="T10" time="4.146647058823529" type="appl" />
         <tli id="T11" time="4.877294117647059" type="appl" />
         <tli id="T12" time="5.607941176470588" type="appl" />
         <tli id="T13" time="6.338588235294118" type="appl" />
         <tli id="T14" time="7.069235294117647" type="appl" />
         <tli id="T15" time="7.799882352941177" type="appl" />
         <tli id="T16" time="8.530529411764705" type="appl" />
         <tli id="T17" time="9.261176470588236" type="appl" />
         <tli id="T18" time="9.991823529411764" type="appl" />
         <tli id="T19" time="10.722470588235295" type="appl" />
         <tli id="T20" time="11.453117647058825" type="appl" />
         <tli id="T21" time="11.868" type="appl" />
         <tli id="T22" time="12.183764705882353" type="appl" />
         <tli id="T23" time="12.914411764705882" type="appl" />
         <tli id="T24" time="13.645058823529412" type="appl" />
         <tli id="T25" time="13.842" type="appl" />
         <tli id="T26" time="13.846" type="appl" />
         <tli id="T27" time="14.375705882352943" type="appl" />
         <tli id="T28" time="14.57" type="appl" />
         <tli id="T29" time="15.106352941176471" type="appl" />
         <tli id="T30" time="15.298" type="appl" />
         <tli id="T31" time="15.837" type="appl" />
         <tli id="T32" time="15.84" type="appl" />
         <tli id="T33" time="16.3917" type="appl" />
         <tli id="T34" time="16.9434" type="appl" />
         <tli id="T35" time="17.4951" type="appl" />
         <tli id="T36" time="18.0468" type="appl" />
         <tli id="T37" time="18.5985" type="appl" />
         <tli id="T38" time="19.074" type="appl" />
         <tli id="T39" time="19.150199999999998" type="appl" />
         <tli id="T40" time="19.67057142857143" type="appl" />
         <tli id="T41" time="19.7019" type="appl" />
         <tli id="T42" time="20.2536" type="appl" />
         <tli id="T43" time="20.267142857142858" type="appl" />
         <tli id="T44" time="20.8053" type="appl" />
         <tli id="T45" time="20.863714285714288" type="appl" />
         <tli id="T46" time="21.357" type="appl" />
         <tli id="T47" time="21.371" type="appl" />
         <tli id="T48" time="21.460285714285714" type="appl" />
         <tli id="T49" time="21.913" type="appl" />
         <tli id="T50" time="22.056857142857144" type="appl" />
         <tli id="T51" time="22.302" type="appl" />
         <tli id="T52" time="22.455" type="appl" />
         <tli id="T53" time="22.65342857142857" type="appl" />
         <tli id="T54" time="22.997" type="appl" />
         <tli id="T55" time="23.25" type="appl" />
         <tli id="T56" time="23.539" type="appl" />
         <tli id="T57" time="23.551" type="appl" />
         <tli id="T58" time="23.942" type="appl" />
         <tli id="T59" time="24.2165" type="appl" />
         <tli id="T60" time="24.881999999999998" type="appl" />
         <tli id="T61" time="25.291" type="appl" />
         <tli id="T62" time="25.5475" type="appl" />
         <tli id="T63" time="26.078" type="appl" />
         <tli id="T64" time="26.213" type="appl" />
         <tli id="T65" time="26.865" type="appl" />
         <tli id="T66" time="28.78" type="appl" />
         <tli id="T67" time="29.015" type="appl" />
         <tli id="T68" time="29.392666666666667" type="appl" />
         <tli id="T69" time="29.478333333333335" type="appl" />
         <tli id="T70" time="29.941666666666666" type="appl" />
         <tli id="T71" time="30.005333333333333" type="appl" />
         <tli id="T72" time="30.405" type="appl" />
         <tli id="T73" time="30.618" type="appl" />
         <tli id="T74" time="30.868333333333336" type="appl" />
         <tli id="T75" time="31.331666666666667" type="appl" />
         <tli id="T76" time="31.795" type="appl" />
         <tli id="T77" time="32.4015" type="appl" />
         <tli id="T78" time="33.008" type="appl" />
         <tli id="T79" time="33.67342857142857" type="appl" />
         <tli id="T80" time="34.338857142857144" type="appl" />
         <tli id="T81" time="35.004285714285714" type="appl" />
         <tli id="T82" time="35.153" type="appl" />
         <tli id="T83" time="35.596" type="appl" />
         <tli id="T84" time="35.669714285714285" type="appl" />
         <tli id="T85" time="36.039" type="appl" />
         <tli id="T86" time="36.335142857142856" type="appl" />
         <tli id="T87" time="36.482" type="appl" />
         <tli id="T88" time="36.925" type="appl" />
         <tli id="T89" time="37.000571428571426" type="appl" />
         <tli id="T90" time="37.666" type="appl" />
         <tli id="T91" time="37.684" type="appl" />
         <tli id="T92" time="38.47" type="appl" />
         <tli id="T93" time="38.483" type="appl" />
         <tli id="T94" time="39.205333333333336" type="appl" />
         <tli id="T95" time="39.282000000000004" type="appl" />
         <tli id="T96" time="39.940666666666665" type="appl" />
         <tli id="T97" time="40.081" type="appl" />
         <tli id="T98" time="40.453" type="appl" />
         <tli id="T99" time="40.676" type="appl" />
         <tli id="T100" time="40.825" type="appl" />
         <tli id="T101" time="41.197" type="appl" />
         <tli id="T102" time="41.569" type="appl" />
         <tli id="T103" time="41.941" type="appl" />
         <tli id="T104" time="41.945" type="appl" />
         <tli id="T105" time="42.287" type="appl" />
         <tli id="T106" time="42.397" type="appl" />
         <tli id="T107" time="42.849" type="appl" />
         <tli id="T108" time="42.853" type="appl" />
         <tli id="T109" time="43.773" type="appl" />
         <tli id="T110" time="44.32709090909091" type="appl" />
         <tli id="T111" time="44.88118181818182" type="appl" />
         <tli id="T112" time="45.43527272727273" type="appl" />
         <tli id="T113" time="45.6" type="appl" />
         <tli id="T114" time="45.98936363636364" type="appl" />
         <tli id="T115" time="46.49875" type="appl" />
         <tli id="T116" time="46.54345454545455" type="appl" />
         <tli id="T117" time="47.097545454545454" type="appl" />
         <tli id="T118" time="47.3975" type="appl" />
         <tli id="T119" time="47.651636363636364" type="appl" />
         <tli id="T120" time="48.20572727272727" type="appl" />
         <tli id="T121" time="48.29625" type="appl" />
         <tli id="T122" time="48.75981818181818" type="appl" />
         <tli id="T123" time="49.195" type="appl" />
         <tli id="T124" time="49.199" type="appl" />
         <tli id="T125" time="49.31390909090909" type="appl" />
         <tli id="T126" time="49.617999999999995" type="appl" />
         <tli id="T127" time="49.868" type="appl" />
         <tli id="T128" time="50.037" type="appl" />
         <tli id="T129" time="50.456" type="appl" />
         <tli id="T130" time="50.875" type="appl" />
         <tli id="T131" time="51.294" type="appl" />
         <tli id="T132" time="51.713" type="appl" />
         <tli id="T133" time="51.714" type="appl" />
         <tli id="T134" time="52.08325" type="appl" />
         <tli id="T135" time="52.4525" type="appl" />
         <tli id="T136" time="52.82175" type="appl" />
         <tli id="T137" time="53.191" type="appl" />
         <tli id="T138" time="54.114" type="appl" />
         <tli id="T139" time="54.86633333333333" type="appl" />
         <tli id="T140" time="55.61866666666667" type="appl" />
         <tli id="T141" time="56.371" type="appl" />
         <tli id="T142" time="56.389" type="appl" />
         <tli id="T143" time="56.9082" type="appl" />
         <tli id="T144" time="57.4274" type="appl" />
         <tli id="T145" time="57.946600000000004" type="appl" />
         <tli id="T146" time="58.4658" type="appl" />
         <tli id="T147" time="58.985" type="appl" />
         <tli id="T148" time="58.996" type="appl" />
         <tli id="T149" time="59.4775" type="appl" />
         <tli id="T150" time="59.959" type="appl" />
         <tli id="T151" time="60.4405" type="appl" />
         <tli id="T152" time="60.922" type="appl" />
         <tli id="T153" time="60.938" type="appl" />
         <tli id="T154" time="61.455285714285715" type="appl" />
         <tli id="T155" time="61.97257142857143" type="appl" />
         <tli id="T156" time="62.48985714285714" type="appl" />
         <tli id="T157" time="62.932" type="appl" />
         <tli id="T158" time="63.00714285714286" type="appl" />
         <tli id="T159" time="63.5172" type="appl" />
         <tli id="T160" time="63.52442857142857" type="appl" />
         <tli id="T161" time="64.04171428571428" type="appl" />
         <tli id="T162" time="64.1024" type="appl" />
         <tli id="T163" time="64.559" type="appl" />
         <tli id="T164" time="64.6876" type="appl" />
         <tli id="T165" time="65.2728" type="appl" />
         <tli id="T166" time="65.858" type="appl" />
         <tli id="T167" time="66.31800000000001" type="appl" />
         <tli id="T168" time="66.451" type="appl" />
         <tli id="T169" time="66.778" type="appl" />
         <tli id="T170" time="67.37" type="appl" />
         <tli id="T171" time="67.381" type="appl" />
         <tli id="T172" time="68.376" type="appl" />
         <tli id="T173" time="69.371" type="appl" />
         <tli id="T174" time="70.366" type="appl" />
         <tli id="T175" time="70.373" type="appl" />
         <tli id="T176" time="70.8216" type="appl" />
         <tli id="T177" time="71.2702" type="appl" />
         <tli id="T178" time="71.7188" type="appl" />
         <tli id="T179" time="72.1674" type="appl" />
         <tli id="T180" time="72.616" type="appl" />
         <tli id="T181" time="72.632" type="appl" />
         <tli id="T182" time="73.088" type="appl" />
         <tli id="T183" time="73.17620000000001" type="appl" />
         <tli id="T184" time="73.588" type="appl" />
         <tli id="T185" time="73.7204" type="appl" />
         <tli id="T186" time="74.088" type="appl" />
         <tli id="T187" time="74.2646" type="appl" />
         <tli id="T188" time="74.588" type="appl" />
         <tli id="T189" time="74.8088" type="appl" />
         <tli id="T190" time="75.088" type="appl" />
         <tli id="T191" time="75.35300000000001" type="appl" />
         <tli id="T192" time="75.588" type="appl" />
         <tli id="T193" time="75.8972" type="appl" />
         <tli id="T194" time="76.088" type="appl" />
         <tli id="T195" time="76.4414" type="appl" />
         <tli id="T196" time="76.588" type="appl" />
         <tli id="T197" time="76.9856" type="appl" />
         <tli id="T198" time="77.088" type="appl" />
         <tli id="T199" time="77.5298" type="appl" />
         <tli id="T200" time="77.588" type="appl" />
         <tli id="T201" time="78.074" type="appl" />
         <tli id="T202" time="78.081" type="appl" />
         <tli id="T203" time="78.088" type="appl" />
         <tli id="T204" time="78.364" type="appl" />
         <tli id="T205" time="78.43133333333334" type="appl" />
         <tli id="T206" time="78.78166666666667" type="appl" />
         <tli id="T207" time="79.055" type="appl" />
         <tli id="T208" time="79.132" type="appl" />
         <tli id="T209" time="79.732" type="appl" />
         <tli id="T210" time="79.746" type="appl" />
         <tli id="T211" time="80.967" type="appl" />
         <tli id="T212" time="81.559" type="appl" />
         <tli id="T213" time="82.151" type="appl" />
         <tli id="T214" time="83.449" type="appl" />
         <tli id="T215" time="83.94149999999999" type="appl" />
         <tli id="T216" time="84.434" type="appl" />
         <tli id="T217" time="84.9265" type="appl" />
         <tli id="T218" time="85.419" type="appl" />
         <tli id="T219" time="85.427" type="appl" />
         <tli id="T220" time="85.629" type="appl" />
         <tli id="T221" time="85.896625" type="appl" />
         <tli id="T222" time="86.36625000000001" type="appl" />
         <tli id="T223" time="86.39725" type="appl" />
         <tli id="T224" time="86.835875" type="appl" />
         <tli id="T225" time="87.16550000000001" type="appl" />
         <tli id="T226" time="87.3055" type="appl" />
         <tli id="T227" time="87.775125" type="appl" />
         <tli id="T228" time="87.93375" type="appl" />
         <tli id="T229" time="88.24475" type="appl" />
         <tli id="T230" time="88.702" type="appl" />
         <tli id="T231" time="88.714375" type="appl" />
         <tli id="T232" time="89.184" type="appl" />
         <tli id="T233" time="89.185" type="appl" />
         <tli id="T234" time="89.92766666666667" type="appl" />
         <tli id="T235" time="90.67033333333333" type="appl" />
         <tli id="T236" time="91.413" type="appl" />
         <tli id="T237" time="91.516" type="appl" />
         <tli id="T238" time="92.134" type="appl" />
         <tli id="T239" time="92.399" type="appl" />
         <tli id="T240" time="92.74166666666666" type="appl" />
         <tli id="T241" time="93.34933333333333" type="appl" />
         <tli id="T242" time="93.957" type="appl" />
         <tli id="T243" time="93.96" type="appl" />
         <tli id="T244" time="94.54771428571428" type="appl" />
         <tli id="T245" time="95.13542857142856" type="appl" />
         <tli id="T246" time="95.72314285714285" type="appl" />
         <tli id="T247" time="95.804" type="appl" />
         <tli id="T248" time="96.09933333333333" type="appl" />
         <tli id="T249" time="96.31085714285715" type="appl" />
         <tli id="T250" time="96.39466666666667" type="appl" />
         <tli id="T251" time="96.69" type="appl" />
         <tli id="T252" time="96.89857142857143" type="appl" />
         <tli id="T253" time="96.98533333333333" type="appl" />
         <tli id="T254" time="97.28066666666666" type="appl" />
         <tli id="T255" time="97.48628571428571" type="appl" />
         <tli id="T256" time="97.576" type="appl" />
         <tli id="T257" time="98.074" type="appl" />
         <tli id="T258" time="98.087" type="appl" />
         <tli id="T259" time="98.56875" type="appl" />
         <tli id="T260" time="99.0505" type="appl" />
         <tli id="T261" time="99.53225" type="appl" />
         <tli id="T262" time="100.014" type="appl" />
         <tli id="T263" time="100.022" type="appl" />
         <tli id="T264" time="100.294" type="appl" />
         <tli id="T265" time="100.34222222222223" type="appl" />
         <tli id="T266" time="100.66244444444445" type="appl" />
         <tli id="T267" time="100.98266666666667" type="appl" />
         <tli id="T268" time="101.30288888888889" type="appl" />
         <tli id="T269" time="101.353" type="appl" />
         <tli id="T270" time="101.62311111111111" type="appl" />
         <tli id="T271" time="101.801" type="appl" />
         <tli id="T272" time="101.94333333333333" type="appl" />
         <tli id="T273" time="102.26355555555556" type="appl" />
         <tli id="T274" time="102.58377777777777" type="appl" />
         <tli id="T275" time="102.888" type="appl" />
         <tli id="T276" time="102.904" type="appl" />
         <tli id="T277" time="103.975" type="appl" />
         <tli id="T278" time="103.99" type="appl" />
         <tli id="T279" time="104.3694" type="appl" />
         <tli id="T280" time="104.7488" type="appl" />
         <tli id="T281" time="105.062" type="appl" />
         <tli id="T282" time="105.12819999999999" type="appl" />
         <tli id="T283" time="105.5" type="appl" />
         <tli id="T284" time="105.5076" type="appl" />
         <tli id="T285" time="105.887" type="appl" />
         <tli id="T286" time="105.894" type="appl" />
         <tli id="T287" time="106.0162" type="appl" />
         <tli id="T288" time="106.5324" type="appl" />
         <tli id="T289" time="106.5595" type="appl" />
         <tli id="T290" time="107.04860000000001" type="appl" />
         <tli id="T291" time="107.225" type="appl" />
         <tli id="T292" time="107.5648" type="appl" />
         <tli id="T293" time="108.081" type="appl" />
         <tli id="T294" time="109.371" type="appl" />
         <tli id="T295" time="109.95433333333332" type="appl" />
         <tli id="T296" time="110.53766666666667" type="appl" />
         <tli id="T297" time="111.121" type="appl" />
         <tli id="T298" time="111.70433333333332" type="appl" />
         <tli id="T299" time="112.28766666666667" type="appl" />
         <tli id="T300" time="112.871" type="appl" />
         <tli id="T301" time="112.883" type="appl" />
         <tli id="T302" time="113.6262" type="appl" />
         <tli id="T303" time="114.3694" type="appl" />
         <tli id="T304" time="115.1126" type="appl" />
         <tli id="T305" time="115.8558" type="appl" />
         <tli id="T306" time="116.599" type="appl" />
         <tli id="T307" time="116.604" type="appl" />
         <tli id="T308" time="117.44525" type="appl" />
         <tli id="T309" time="118.28649999999999" type="appl" />
         <tli id="T310" time="119.12774999999999" type="appl" />
         <tli id="T311" time="119.969" type="appl" />
         <tli id="T312" time="119.986" type="appl" />
         <tli id="T313" time="120.586875" type="appl" />
         <tli id="T314" time="121.18775000000001" type="appl" />
         <tli id="T315" time="121.78862500000001" type="appl" />
         <tli id="T316" time="122.3895" type="appl" />
         <tli id="T317" time="122.990375" type="appl" />
         <tli id="T318" time="123.59125" type="appl" />
         <tli id="T319" time="124.192125" type="appl" />
         <tli id="T320" time="124.793" type="appl" />
         <tli id="T321" time="128.301" type="appl" />
         <tli id="T322" time="129.2385" type="appl" />
         <tli id="T323" time="130.176" type="appl" />
         <tli id="T324" time="131.1135" type="appl" />
         <tli id="T325" time="132.051" type="appl" />
         <tli id="T326" time="132.059" type="appl" />
         <tli id="T327" time="132.323" type="appl" />
         <tli id="T328" time="132.44442105263158" type="appl" />
         <tli id="T329" time="132.82984210526317" type="appl" />
         <tli id="T330" time="133.16366666666667" type="appl" />
         <tli id="T331" time="133.21526315789473" type="appl" />
         <tli id="T332" time="133.6006842105263" type="appl" />
         <tli id="T333" time="133.9861052631579" type="appl" />
         <tli id="T334" time="134.00433333333334" type="appl" />
         <tli id="T335" time="134.37152631578948" type="appl" />
         <tli id="T336" time="134.75694736842107" type="appl" />
         <tli id="T337" time="134.845" type="appl" />
         <tli id="T338" time="135.14236842105262" type="appl" />
         <tli id="T339" time="135.5277894736842" type="appl" />
         <tli id="T340" time="135.9132105263158" type="appl" />
         <tli id="T341" time="136.29863157894738" type="appl" />
         <tli id="T342" time="136.68405263157894" type="appl" />
         <tli id="T343" time="137.06947368421052" type="appl" />
         <tli id="T344" time="137.4548947368421" type="appl" />
         <tli id="T345" time="137.8403157894737" type="appl" />
         <tli id="T346" time="138.22573684210528" type="appl" />
         <tli id="T347" time="138.61115789473683" type="appl" />
         <tli id="T348" time="138.99657894736842" type="appl" />
         <tli id="T349" time="139.382" type="appl" />
         <tli id="T350" time="139.393" type="appl" />
         <tli id="T351" time="139.937" type="appl" />
         <tli id="T352" time="140.481" type="appl" />
         <tli id="T353" time="141.025" type="appl" />
         <tli id="T354" time="141.038" type="appl" />
         <tli id="T355" time="141.2" type="appl" />
         <tli id="T356" time="141.47422222222224" type="appl" />
         <tli id="T357" time="141.65279999999998" type="appl" />
         <tli id="T358" time="141.91044444444447" type="appl" />
         <tli id="T359" time="142.10559999999998" type="appl" />
         <tli id="T360" time="142.34666666666666" type="appl" />
         <tli id="T361" time="142.5584" type="appl" />
         <tli id="T362" time="142.7828888888889" type="appl" />
         <tli id="T363" time="143.0112" type="appl" />
         <tli id="T364" time="143.21911111111112" type="appl" />
         <tli id="T365" time="143.464" type="appl" />
         <tli id="T366" time="143.65533333333335" type="appl" />
         <tli id="T367" time="144.09155555555554" type="appl" />
         <tli id="T368" time="144.52777777777777" type="appl" />
         <tli id="T369" time="144.964" type="appl" />
         <tli id="T370" time="145.3795" type="appl" />
         <tli id="T371" time="145.79500000000002" type="appl" />
         <tli id="T372" time="146.2105" type="appl" />
         <tli id="T373" time="146.626" type="appl" />
         <tli id="T374" time="147.045" type="appl" />
         <tli id="T375" time="147.056" type="appl" />
         <tli id="T376" time="147.46033333333335" type="appl" />
         <tli id="T377" time="147.86466666666666" type="appl" />
         <tli id="T378" time="148.269" type="appl" />
         <tli id="T379" time="148.67333333333335" type="appl" />
         <tli id="T380" time="149.07766666666666" type="appl" />
         <tli id="T381" time="149.482" type="appl" />
         <tli id="T382" time="149.49" type="appl" />
         <tli id="T383" time="150.379" type="appl" />
         <tli id="T384" time="150.397" type="appl" />
         <tli id="T385" time="151.42" type="appl" />
         <tli id="T386" time="151.7545" type="appl" />
         <tli id="T387" time="152.089" type="appl" />
         <tli id="T388" time="152.11399999999998" type="appl" />
         <tli id="T389" time="152.4235" type="appl" />
         <tli id="T390" time="152.758" type="appl" />
         <tli id="T391" time="153.813" type="appl" />
         <tli id="T392" time="153.831" type="appl" />
         <tli id="T393" time="154.3715" type="appl" />
         <tli id="T394" time="154.923" type="appl" />
         <tli id="T395" time="154.93" type="appl" />
         <tli id="T396" time="156.048" type="appl" />
         <tli id="T397" time="156.066" type="appl" />
         <tli id="T398" time="156.76233333333334" type="appl" />
         <tli id="T399" time="157.45866666666666" type="appl" />
         <tli id="T400" time="157.595" type="appl" />
         <tli id="T401" time="158.155" type="appl" />
         <tli id="T402" time="158.169" type="appl" />
         <tli id="T403" time="158.257" type="appl" />
         <tli id="T404" time="158.44850000000002" type="appl" />
         <tli id="T405" time="158.728" type="appl" />
         <tli id="T406" time="158.91899999999998" type="appl" />
         <tli id="T407" time="159.0075" type="appl" />
         <tli id="T408" time="159.287" type="appl" />
         <tli id="T409" time="159.56650000000002" type="appl" />
         <tli id="T410" time="159.581" type="appl" />
         <tli id="T411" time="159.846" type="appl" />
         <tli id="T412" time="159.853" type="appl" />
         <tli id="T413" time="160.41916666666668" type="appl" />
         <tli id="T414" time="160.98533333333333" type="appl" />
         <tli id="T415" time="161.5515" type="appl" />
         <tli id="T416" time="162.11766666666668" type="appl" />
         <tli id="T417" time="162.68383333333333" type="appl" />
         <tli id="T418" time="163.25" type="appl" />
         <tli id="T419" time="163.265" type="appl" />
         <tli id="T420" time="163.63174999999998" type="appl" />
         <tli id="T421" time="163.99849999999998" type="appl" />
         <tli id="T422" time="164.36525" type="appl" />
         <tli id="T423" time="164.732" type="appl" />
         <tli id="T424" time="165.09875" type="appl" />
         <tli id="T425" time="165.46550000000002" type="appl" />
         <tli id="T426" time="165.83225000000002" type="appl" />
         <tli id="T427" time="166.196" type="appl" />
         <tli id="T428" time="166.199" type="appl" />
         <tli id="T429" time="166.51470588235293" type="appl" />
         <tli id="T430" time="166.8334117647059" type="appl" />
         <tli id="T431" time="167.15211764705882" type="appl" />
         <tli id="T432" time="167.47082352941177" type="appl" />
         <tli id="T433" time="167.7895294117647" type="appl" />
         <tli id="T434" time="168.10823529411763" type="appl" />
         <tli id="T435" time="168.4269411764706" type="appl" />
         <tli id="T436" time="168.74564705882352" type="appl" />
         <tli id="T437" time="169.06435294117648" type="appl" />
         <tli id="T438" time="169.3830588235294" type="appl" />
         <tli id="T439" time="169.70176470588237" type="appl" />
         <tli id="T440" time="169.772" type="appl" />
         <tli id="T441" time="170.0204705882353" type="appl" />
         <tli id="T442" time="170.33917647058823" type="appl" />
         <tli id="T443" time="170.6578823529412" type="appl" />
         <tli id="T444" time="170.86" type="appl" />
         <tli id="T445" time="170.97658823529412" type="appl" />
         <tli id="T446" time="171.29529411764707" type="appl" />
         <tli id="T447" time="171.614" type="appl" />
         <tli id="T448" time="171.618" type="appl" />
         <tli id="T449" time="172.883" type="appl" />
         <tli id="T450" time="172.898" type="appl" />
         <tli id="T451" time="172.939" type="appl" />
         <tli id="T452" time="173.696" type="appl" />
         <tli id="T453" time="173.93099999999998" type="appl" />
         <tli id="T454" time="174.964" type="appl" />
         <tli id="T455" time="174.967" type="appl" />
         <tli id="T456" time="175.5596666666667" type="appl" />
         <tli id="T457" time="176.15233333333333" type="appl" />
         <tli id="T458" time="176.745" type="appl" />
         <tli id="T459" time="177.33766666666668" type="appl" />
         <tli id="T460" time="177.93033333333332" type="appl" />
         <tli id="T461" time="178.521" type="appl" />
         <tli id="T462" time="178.523" type="appl" />
         <tli id="T463" time="178.827" type="appl" />
         <tli id="T464" time="179.0091111111111" type="appl" />
         <tli id="T465" time="179.3835" type="appl" />
         <tli id="T466" time="179.4972222222222" type="appl" />
         <tli id="T467" time="179.94" type="appl" />
         <tli id="T468" time="179.98533333333333" type="appl" />
         <tli id="T469" time="180.47344444444443" type="appl" />
         <tli id="T470" time="180.4965" type="appl" />
         <tli id="T471" time="180.96155555555555" type="appl" />
         <tli id="T472" time="181.053" type="appl" />
         <tli id="T473" time="181.44966666666664" type="appl" />
         <tli id="T474" time="181.6095" type="appl" />
         <tli id="T475" time="181.93777777777777" type="appl" />
         <tli id="T476" time="182.166" type="appl" />
         <tli id="T477" time="182.42588888888886" type="appl" />
         <tli id="T478" time="182.914" type="appl" />
         <tli id="T479" time="182.93" type="appl" />
         <tli id="T480" time="183.4115" type="appl" />
         <tli id="T481" time="183.893" type="appl" />
         <tli id="T482" time="184.3745" type="appl" />
         <tli id="T483" time="184.856" type="appl" />
         <tli id="T484" time="184.874" type="appl" />
         <tli id="T485" time="185.2748" type="appl" />
         <tli id="T486" time="185.6756" type="appl" />
         <tli id="T487" time="186.0764" type="appl" />
         <tli id="T488" time="186.47719999999998" type="appl" />
         <tli id="T489" time="186.878" type="appl" />
         <tli id="T490" time="186.983" type="appl" />
         <tli id="T491" time="187.2788" type="appl" />
         <tli id="T492" time="187.314" type="appl" />
         <tli id="T493" time="187.64499999999998" type="appl" />
         <tli id="T494" time="187.6796" type="appl" />
         <tli id="T495" time="187.976" type="appl" />
         <tli id="T496" time="188.0804" type="appl" />
         <tli id="T497" time="188.307" type="appl" />
         <tli id="T498" time="188.4812" type="appl" />
         <tli id="T499" time="188.882" type="appl" />
         <tli id="T500" time="189.175" type="appl" />
         <tli id="T501" time="189.2828" type="appl" />
         <tli id="T502" time="189.68359999999998" type="appl" />
         <tli id="T503" time="190.0844" type="appl" />
         <tli id="T504" time="190.395" type="appl" />
         <tli id="T505" time="190.4852" type="appl" />
         <tli id="T506" time="190.886" type="appl" />
         <tli id="T507" time="190.896" type="appl" />
         <tli id="T508" time="191.5208888888889" type="appl" />
         <tli id="T509" time="192.14577777777777" type="appl" />
         <tli id="T510" time="192.77066666666667" type="appl" />
         <tli id="T511" time="193.39555555555555" type="appl" />
         <tli id="T512" time="194.02044444444445" type="appl" />
         <tli id="T513" time="194.64533333333333" type="appl" />
         <tli id="T514" time="195.27022222222223" type="appl" />
         <tli id="T515" time="195.8951111111111" type="appl" />
         <tli id="T516" time="196.52" type="appl" />
         <tli id="T517" time="198.018" type="appl" />
         <tli id="T518" time="198.37633333333332" type="appl" />
         <tli id="T519" time="198.73466666666667" type="appl" />
         <tli id="T520" time="199.09300000000002" type="appl" />
         <tli id="T521" time="199.45133333333334" type="appl" />
         <tli id="T522" time="199.80966666666666" type="appl" />
         <tli id="T523" time="200.168" type="appl" />
         <tli id="T524" time="200.52633333333335" type="appl" />
         <tli id="T525" time="200.88466666666667" type="appl" />
         <tli id="T526" time="201.243" type="appl" />
         <tli id="T527" time="201.60133333333334" type="appl" />
         <tli id="T528" time="201.9596666666667" type="appl" />
         <tli id="T529" time="202.318" type="appl" />
         <tli id="T530" time="202.33" type="appl" />
         <tli id="T531" time="202.896" type="appl" />
         <tli id="T532" time="202.903" type="appl" />
         <tli id="T533" time="203.23999999999998" type="appl" />
         <tli id="T534" time="203.584" type="appl" />
         <tli id="T535" time="203.928" type="appl" />
         <tli id="T536" time="204.27200000000002" type="appl" />
         <tli id="T537" time="204.616" type="appl" />
         <tli id="T538" time="204.618" type="appl" />
         <tli id="T539" time="205.6875" type="appl" />
         <tli id="T540" time="206.757" type="appl" />
         <tli id="T541" time="207.82649999999998" type="appl" />
         <tli id="T542" time="208.896" type="appl" />
         <tli id="T543" time="208.899" type="appl" />
         <tli id="T544" time="209.41063636363637" type="appl" />
         <tli id="T545" time="209.9222727272727" type="appl" />
         <tli id="T546" time="210.43390909090908" type="appl" />
         <tli id="T547" time="210.94554545454545" type="appl" />
         <tli id="T548" time="211.45718181818182" type="appl" />
         <tli id="T549" time="211.96881818181816" type="appl" />
         <tli id="T550" time="212.48045454545453" type="appl" />
         <tli id="T551" time="212.9920909090909" type="appl" />
         <tli id="T552" time="213.50372727272728" type="appl" />
         <tli id="T553" time="214.01536363636362" type="appl" />
         <tli id="T554" time="214.527" type="appl" />
         <tli id="T555" time="214.533" type="appl" />
         <tli id="T556" time="215.07142857142856" type="appl" />
         <tli id="T557" time="215.60985714285712" type="appl" />
         <tli id="T558" time="216.1482857142857" type="appl" />
         <tli id="T559" time="216.197" type="appl" />
         <tli id="T560" time="216.6867142857143" type="appl" />
         <tli id="T561" time="217.22514285714286" type="appl" />
         <tli id="T562" time="217.76357142857142" type="appl" />
         <tli id="T563" time="218.302" type="appl" />
         <tli id="T564" time="218.309" type="appl" />
         <tli id="T565" time="218.52975" type="appl" />
         <tli id="T566" time="218.7505" type="appl" />
         <tli id="T567" time="218.97125" type="appl" />
         <tli id="T568" time="219.192" type="appl" />
         <tli id="T569" time="219.199" type="appl" />
         <tli id="T570" time="219.474" type="appl" />
         <tli id="T571" time="219.60333333333335" type="appl" />
         <tli id="T572" time="219.85166666666666" type="appl" />
         <tli id="T573" time="220.00766666666667" type="appl" />
         <tli id="T574" time="220.22933333333333" type="appl" />
         <tli id="T575" time="220.412" type="appl" />
         <tli id="T576" time="220.607" type="appl" />
         <tli id="T577" time="220.625" type="appl" />
         <tli id="T578" time="221.04514285714285" type="appl" />
         <tli id="T579" time="221.46528571428573" type="appl" />
         <tli id="T580" time="221.88542857142858" type="appl" />
         <tli id="T581" time="222.30557142857143" type="appl" />
         <tli id="T582" time="222.72571428571428" type="appl" />
         <tli id="T583" time="223.14585714285715" type="appl" />
         <tli id="T584" time="223.566" type="appl" />
         <tli id="T585" time="223.573" type="appl" />
         <tli id="T586" time="223.92962500000002" type="appl" />
         <tli id="T587" time="224.28625" type="appl" />
         <tli id="T588" time="224.642875" type="appl" />
         <tli id="T589" time="224.9995" type="appl" />
         <tli id="T590" time="225.356125" type="appl" />
         <tli id="T591" time="225.71275" type="appl" />
         <tli id="T592" time="226.06937499999998" type="appl" />
         <tli id="T593" time="226.426" type="appl" />
         <tli id="T594" time="226.433" type="appl" />
         <tli id="T595" time="227.2405" type="appl" />
         <tli id="T596" time="228.048" type="appl" />
         <tli id="T597" time="228.8555" type="appl" />
         <tli id="T598" time="229.66299999999998" type="appl" />
         <tli id="T599" time="230.4705" type="appl" />
         <tli id="T600" time="231.278" type="appl" />
         <tli id="T601" time="234.987" type="appl" />
         <tli id="T602" time="235.789" type="appl" />
         <tli id="T603" time="236.23742857142855" type="appl" />
         <tli id="T604" time="236.487" type="appl" />
         <tli id="T605" time="236.68585714285715" type="appl" />
         <tli id="T606" time="237.1342857142857" type="appl" />
         <tli id="T607" time="237.58271428571427" type="appl" />
         <tli id="T608" time="237.987" type="appl" />
         <tli id="T609" time="238.03114285714284" type="appl" />
         <tli id="T610" time="238.218" type="appl" />
         <tli id="T611" time="238.47957142857143" type="appl" />
         <tli id="T612" time="238.5196" type="appl" />
         <tli id="T613" time="238.8212" type="appl" />
         <tli id="T614" time="238.928" type="appl" />
         <tli id="T615" time="239.12279999999998" type="appl" />
         <tli id="T616" time="239.4244" type="appl" />
         <tli id="T617" time="239.499" type="appl" />
         <tli id="T618" time="239.726" type="appl" />
         <tli id="T619" time="240.4762" type="appl" />
         <tli id="T620" time="241.4534" type="appl" />
         <tli id="T621" time="242.319" type="appl" />
         <tli id="T622" time="242.4306" type="appl" />
         <tli id="T623" time="242.7387692307692" type="appl" />
         <tli id="T624" time="243.15853846153846" type="appl" />
         <tli id="T625" time="243.40779999999998" type="appl" />
         <tli id="T626" time="243.57830769230767" type="appl" />
         <tli id="T627" time="243.99807692307692" type="appl" />
         <tli id="T628" time="244.385" type="appl" />
         <tli id="T629" time="244.41784615384614" type="appl" />
         <tli id="T630" time="244.8376153846154" type="appl" />
         <tli id="T631" time="245.2573846153846" type="appl" />
         <tli id="T632" time="245.547" type="appl" />
         <tli id="T633" time="245.67715384615386" type="appl" />
         <tli id="T634" time="246.09692307692308" type="appl" />
         <tli id="T635" time="246.246" type="appl" />
         <tli id="T636" time="246.51669230769232" type="appl" />
         <tli id="T637" time="246.93646153846154" type="appl" />
         <tli id="T638" time="247.3562307692308" type="appl" />
         <tli id="T639" time="247.518" type="appl" />
         <tli id="T640" time="247.776" type="appl" />
         <tli id="T641" time="250.465" type="appl" />
         <tli id="T642" time="251.17000000000002" type="appl" />
         <tli id="T643" time="251.875" type="appl" />
         <tli id="T644" time="251.883" type="appl" />
         <tli id="T645" time="252.43460000000002" type="appl" />
         <tli id="T646" time="252.9862" type="appl" />
         <tli id="T647" time="253.314" type="appl" />
         <tli id="T648" time="253.5378" type="appl" />
         <tli id="T649" time="253.917" type="appl" />
         <tli id="T650" time="254.08939999999998" type="appl" />
         <tli id="T651" time="254.52" type="appl" />
         <tli id="T652" time="254.641" type="appl" />
         <tli id="T653" time="257.294" type="appl" />
         <tli id="T654" time="257.65" type="appl" />
         <tli id="T655" time="257.82099999999997" type="appl" />
         <tli id="T656" time="258.1448571428571" type="appl" />
         <tli id="T657" time="258.348" type="appl" />
         <tli id="T658" time="258.63971428571426" type="appl" />
         <tli id="T659" time="258.875" type="appl" />
         <tli id="T660" time="259.1345714285714" type="appl" />
         <tli id="T661" time="259.184" type="appl" />
         <tli id="T662" time="259.62942857142855" type="appl" />
         <tli id="T663" time="260.0366666666667" type="appl" />
         <tli id="T664" time="260.1242857142857" type="appl" />
         <tli id="T665" time="260.61914285714283" type="appl" />
         <tli id="T666" time="260.88933333333335" type="appl" />
         <tli id="T667" time="261.114" type="appl" />
         <tli id="T668" time="261.742" type="appl" />
         <tli id="T669" time="261.743" type="appl" />
         <tli id="T670" time="262.569" type="appl" />
         <tli id="T671" time="263.395" type="appl" />
         <tli id="T672" time="264.221" type="appl" />
         <tli id="T673" time="264.225" type="appl" />
         <tli id="T674" time="264.258" type="appl" />
         <tli id="T675" time="265.016" type="appl" />
         <tli id="T676" time="265.192" type="appl" />
         <tli id="T677" time="266.159" type="appl" />
         <tli id="T678" time="266.164" type="appl" />
         <tli id="T679" time="267.06525" type="appl" />
         <tli id="T680" time="267.9665" type="appl" />
         <tli id="T681" time="268.86775" type="appl" />
         <tli id="T682" time="269.769" type="appl" />
         <tli id="T683" time="269.775" type="appl" />
         <tli id="T684" time="270.2824" type="appl" />
         <tli id="T685" time="270.7898" type="appl" />
         <tli id="T686" time="271.2972" type="appl" />
         <tli id="T687" time="271.8046" type="appl" />
         <tli id="T688" time="272.312" type="appl" />
         <tli id="T689" time="272.332" type="appl" />
         <tli id="T690" time="272.914375" type="appl" />
         <tli id="T691" time="273.49675" type="appl" />
         <tli id="T692" time="274.079125" type="appl" />
         <tli id="T693" time="274.6615" type="appl" />
         <tli id="T694" time="275.243875" type="appl" />
         <tli id="T695" time="275.82624999999996" type="appl" />
         <tli id="T696" time="276.408625" type="appl" />
         <tli id="T697" time="276.991" type="appl" />
         <tli id="T698" time="276.999" type="appl" />
         <tli id="T699" time="278.262" type="appl" />
         <tli id="T700" time="279.525" type="appl" />
         <tli id="T701" time="280.788" type="appl" />
         <tli id="T702" time="282.047" type="appl" />
         <tli id="T703" time="282.051" type="appl" />
         <tli id="T704" time="282.45500000000004" type="appl" />
         <tli id="T705" time="282.863" type="appl" />
         <tli id="T706" time="282.882" type="appl" />
         <tli id="T707" time="283.69" type="appl" />
         <tli id="T708" time="283.705" type="appl" />
         <tli id="T709" time="284.48016666666666" type="appl" />
         <tli id="T710" time="284.628" type="appl" />
         <tli id="T711" time="285.25533333333334" type="appl" />
         <tli id="T712" time="285.554" type="appl" />
         <tli id="T713" time="286.03049999999996" type="appl" />
         <tli id="T714" time="286.80566666666664" type="appl" />
         <tli id="T715" time="287.5808333333333" type="appl" />
         <tli id="T716" time="287.852" type="appl" />
         <tli id="T717" time="288.356" type="appl" />
         <tli id="T718" time="288.35949999999997" type="appl" />
         <tli id="T719" time="288.86699999999996" type="appl" />
         <tli id="T720" time="289.3745" type="appl" />
         <tli id="T721" time="289.882" type="appl" />
         <tli id="T722" time="289.889" type="appl" />
         <tli id="T723" time="290.083" type="appl" />
         <tli id="T724" time="290.308" type="appl" />
         <tli id="T725" time="290.5683333333333" type="appl" />
         <tli id="T726" time="290.892" type="appl" />
         <tli id="T727" time="291.0536666666667" type="appl" />
         <tli id="T728" time="291.3645" type="appl" />
         <tli id="T729" time="291.539" type="appl" />
         <tli id="T730" time="291.837" type="appl" />
         <tli id="T731" time="292.8555" type="appl" />
         <tli id="T732" time="293.874" type="appl" />
         <tli id="T733" time="293.88" type="appl" />
         <tli id="T734" time="294.616" type="appl" />
         <tli id="T735" time="295.469" type="appl" />
         <tli id="T736" time="295.476" type="appl" />
         <tli id="T737" time="296.8215" type="appl" />
         <tli id="T738" time="298.167" type="appl" />
         <tli id="T739" time="298.184" type="appl" />
         <tli id="T740" time="299.48850000000004" type="appl" />
         <tli id="T741" time="300.793" type="appl" />
         <tli id="T742" time="302.09749999999997" type="appl" />
         <tli id="T743" time="303.402" type="appl" />
         <tli id="T744" time="303.405" type="appl" />
         <tli id="T745" time="304.18666666666667" type="appl" />
         <tli id="T746" time="304.9683333333333" type="appl" />
         <tli id="T747" time="305.75" type="appl" />
         <tli id="T748" time="305.765" type="appl" />
         <tli id="T749" time="306.4765" type="appl" />
         <tli id="T750" time="306.673" type="appl" />
         <tli id="T751" time="307.0063333333333" type="appl" />
         <tli id="T752" time="307.188" type="appl" />
         <tli id="T753" time="307.3396666666667" type="appl" />
         <tli id="T754" time="307.673" type="appl" />
         <tli id="T755" time="307.681" type="appl" />
         <tli id="T756" time="307.8995" type="appl" />
         <tli id="T757" time="308.30833333333334" type="appl" />
         <tli id="T758" time="308.611" type="appl" />
         <tli id="T759" time="308.93566666666663" type="appl" />
         <tli id="T760" time="309.563" type="appl" />
         <tli id="T761" time="311.351" type="appl" />
         <tli id="T762" time="312.0813333333333" type="appl" />
         <tli id="T763" time="312.81166666666667" type="appl" />
         <tli id="T764" time="313.542" type="appl" />
         <tli id="T0" time="313.666" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-DCh"
                      id="tx-DCh"
                      speaker="DCh"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-DCh">
            <ts e="T7" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Pasʼiba</ts>
                  <nts id="Seg_7" n="HIAT:ip">,</nts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">eta</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">bɨla</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">pʼervaja</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T5">zadačʼa</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T31" id="Seg_22" n="sc" s="T8">
               <ts e="T31" id="Seg_24" n="HIAT:u" s="T8">
                  <nts id="Seg_25" n="HIAT:ip">–</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_28" n="HIAT:w" s="T8">Ftaraja</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_31" n="HIAT:w" s="T10">zadačʼa</ts>
                  <nts id="Seg_32" n="HIAT:ip">,</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_35" n="HIAT:w" s="T11">ja</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_38" n="HIAT:w" s="T12">Vam</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_41" n="HIAT:w" s="T13">daju</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_44" n="HIAT:w" s="T14">kartʼinkʼi</ts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_48" n="HIAT:w" s="T15">i</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_51" n="HIAT:w" s="T16">Vɨ</ts>
                  <nts id="Seg_52" n="HIAT:ip">,</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_55" n="HIAT:w" s="T17">požalujsta</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_59" n="HIAT:w" s="T18">pastavʼtʼe</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_62" n="HIAT:w" s="T19">v</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_65" n="HIAT:w" s="T20">takoj</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_68" n="HIAT:w" s="T22">parʼadok</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_72" n="HIAT:w" s="T23">katorɨj</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_75" n="HIAT:w" s="T24">Vam</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_78" n="HIAT:w" s="T27">kažetsa</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_81" n="HIAT:w" s="T29">pravʼilʼnɨj</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T46" id="Seg_84" n="sc" s="T32">
               <ts e="T46" id="Seg_86" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_88" n="HIAT:w" s="T32">Nʼet</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_91" n="HIAT:w" s="T33">pravʼilʼna</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_94" n="HIAT:w" s="T34">ilʼi</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_97" n="HIAT:w" s="T35">nʼe</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_100" n="HIAT:w" s="T36">pravʼilʼna</ts>
                  <nts id="Seg_101" n="HIAT:ip">,</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_104" n="HIAT:w" s="T37">no</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_107" n="HIAT:w" s="T39">takoj</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_110" n="HIAT:w" s="T41">parʼadak</ts>
                  <nts id="Seg_111" n="HIAT:ip">,</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_114" n="HIAT:w" s="T42">katorɨj</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_116" n="HIAT:ip">(</nts>
                  <nts id="Seg_117" n="HIAT:ip">(</nts>
                  <ats e="T46" id="Seg_118" n="HIAT:non-pho" s="T44">…</ats>
                  <nts id="Seg_119" n="HIAT:ip">)</nts>
                  <nts id="Seg_120" n="HIAT:ip">)</nts>
                  <nts id="Seg_121" n="HIAT:ip">.</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T56" id="Seg_123" n="sc" s="T47">
               <ts e="T56" id="Seg_125" n="HIAT:u" s="T47">
                  <nts id="Seg_126" n="HIAT:ip">–</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_129" n="HIAT:w" s="T47">Da</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_133" n="HIAT:w" s="T49">pa</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_136" n="HIAT:w" s="T52">očʼerʼedʼi</ts>
                  <nts id="Seg_137" n="HIAT:ip">,</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_140" n="HIAT:w" s="T54">da</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T64" id="Seg_143" n="sc" s="T57">
               <ts e="T64" id="Seg_145" n="HIAT:u" s="T57">
                  <ts e="T59" id="Seg_147" n="HIAT:w" s="T57">Kakoj</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_150" n="HIAT:w" s="T59">Vam</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_153" n="HIAT:w" s="T60">kažetsa</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_156" n="HIAT:w" s="T62">pravʼilʼnɨj</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T107" id="Seg_159" n="sc" s="T104">
               <ts e="T107" id="Seg_161" n="HIAT:u" s="T104">
                  <nts id="Seg_162" n="HIAT:ip">–</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_165" n="HIAT:w" s="T104">A</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_168" n="HIAT:w" s="T106">hakalɨː</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T532" id="Seg_171" n="sc" s="T530">
               <ts e="T532" id="Seg_173" n="HIAT:u" s="T530">
                  <nts id="Seg_174" n="HIAT:ip">–</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_177" n="HIAT:w" s="T530">Nʼet</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T764" id="Seg_180" n="sc" s="T761">
               <ts e="T764" id="Seg_182" n="HIAT:u" s="T761">
                  <nts id="Seg_183" n="HIAT:ip">–</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_186" n="HIAT:w" s="T761">Tak</ts>
                  <nts id="Seg_187" n="HIAT:ip">,</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_190" n="HIAT:w" s="T762">elete</ts>
                  <nts id="Seg_191" n="HIAT:ip">,</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_194" n="HIAT:w" s="T763">pasiːba</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-DCh">
            <ts e="T7" id="Seg_197" n="sc" s="T1">
               <ts e="T2" id="Seg_199" n="e" s="T1">– Pasʼiba, </ts>
               <ts e="T3" id="Seg_201" n="e" s="T2">eta </ts>
               <ts e="T4" id="Seg_203" n="e" s="T3">bɨla </ts>
               <ts e="T5" id="Seg_205" n="e" s="T4">pʼervaja </ts>
               <ts e="T7" id="Seg_207" n="e" s="T5">zadačʼa. </ts>
            </ts>
            <ts e="T31" id="Seg_208" n="sc" s="T8">
               <ts e="T10" id="Seg_210" n="e" s="T8">– Ftaraja </ts>
               <ts e="T11" id="Seg_212" n="e" s="T10">zadačʼa, </ts>
               <ts e="T12" id="Seg_214" n="e" s="T11">ja </ts>
               <ts e="T13" id="Seg_216" n="e" s="T12">Vam </ts>
               <ts e="T14" id="Seg_218" n="e" s="T13">daju </ts>
               <ts e="T15" id="Seg_220" n="e" s="T14">kartʼinkʼi, </ts>
               <ts e="T16" id="Seg_222" n="e" s="T15">i </ts>
               <ts e="T17" id="Seg_224" n="e" s="T16">Vɨ, </ts>
               <ts e="T18" id="Seg_226" n="e" s="T17">požalujsta, </ts>
               <ts e="T19" id="Seg_228" n="e" s="T18">pastavʼtʼe </ts>
               <ts e="T20" id="Seg_230" n="e" s="T19">v </ts>
               <ts e="T22" id="Seg_232" n="e" s="T20">takoj </ts>
               <ts e="T23" id="Seg_234" n="e" s="T22">parʼadok, </ts>
               <ts e="T24" id="Seg_236" n="e" s="T23">katorɨj </ts>
               <ts e="T27" id="Seg_238" n="e" s="T24">Vam </ts>
               <ts e="T29" id="Seg_240" n="e" s="T27">kažetsa </ts>
               <ts e="T31" id="Seg_242" n="e" s="T29">pravʼilʼnɨj. </ts>
            </ts>
            <ts e="T46" id="Seg_243" n="sc" s="T32">
               <ts e="T33" id="Seg_245" n="e" s="T32">Nʼet </ts>
               <ts e="T34" id="Seg_247" n="e" s="T33">pravʼilʼna </ts>
               <ts e="T35" id="Seg_249" n="e" s="T34">ilʼi </ts>
               <ts e="T36" id="Seg_251" n="e" s="T35">nʼe </ts>
               <ts e="T37" id="Seg_253" n="e" s="T36">pravʼilʼna, </ts>
               <ts e="T39" id="Seg_255" n="e" s="T37">no </ts>
               <ts e="T41" id="Seg_257" n="e" s="T39">takoj </ts>
               <ts e="T42" id="Seg_259" n="e" s="T41">parʼadak, </ts>
               <ts e="T44" id="Seg_261" n="e" s="T42">katorɨj </ts>
               <ts e="T46" id="Seg_263" n="e" s="T44">((…)). </ts>
            </ts>
            <ts e="T56" id="Seg_264" n="sc" s="T47">
               <ts e="T49" id="Seg_266" n="e" s="T47">– Da, </ts>
               <ts e="T52" id="Seg_268" n="e" s="T49">pa </ts>
               <ts e="T54" id="Seg_270" n="e" s="T52">očʼerʼedʼi, </ts>
               <ts e="T56" id="Seg_272" n="e" s="T54">da. </ts>
            </ts>
            <ts e="T64" id="Seg_273" n="sc" s="T57">
               <ts e="T59" id="Seg_275" n="e" s="T57">Kakoj </ts>
               <ts e="T60" id="Seg_277" n="e" s="T59">Vam </ts>
               <ts e="T62" id="Seg_279" n="e" s="T60">kažetsa </ts>
               <ts e="T64" id="Seg_281" n="e" s="T62">pravʼilʼnɨj. </ts>
            </ts>
            <ts e="T107" id="Seg_282" n="sc" s="T104">
               <ts e="T106" id="Seg_284" n="e" s="T104">– A </ts>
               <ts e="T107" id="Seg_286" n="e" s="T106">hakalɨː. </ts>
            </ts>
            <ts e="T532" id="Seg_287" n="sc" s="T530">
               <ts e="T532" id="Seg_289" n="e" s="T530">– Nʼet. </ts>
            </ts>
            <ts e="T764" id="Seg_290" n="sc" s="T761">
               <ts e="T762" id="Seg_292" n="e" s="T761">– Tak, </ts>
               <ts e="T763" id="Seg_294" n="e" s="T762">elete, </ts>
               <ts e="T764" id="Seg_296" n="e" s="T763">pasiːba. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-DCh">
            <ta e="T7" id="Seg_297" s="T1">ChGS_UoPP_20170724_SocCogOrder_conv.DCh.001 (001.001)</ta>
            <ta e="T31" id="Seg_298" s="T8">ChGS_UoPP_20170724_SocCogOrder_conv.DCh.002 (001.003)</ta>
            <ta e="T46" id="Seg_299" s="T32">ChGS_UoPP_20170724_SocCogOrder_conv.DCh.003 (001.006)</ta>
            <ta e="T56" id="Seg_300" s="T47">ChGS_UoPP_20170724_SocCogOrder_conv.DCh.004 (001.008)</ta>
            <ta e="T64" id="Seg_301" s="T57">ChGS_UoPP_20170724_SocCogOrder_conv.DCh.005 (001.010)</ta>
            <ta e="T107" id="Seg_302" s="T104">ChGS_UoPP_20170724_SocCogOrder_conv.DCh.006 (001.020)</ta>
            <ta e="T532" id="Seg_303" s="T530">ChGS_UoPP_20170724_SocCogOrder_conv.DCh.007 (001.093)</ta>
            <ta e="T764" id="Seg_304" s="T761">ChGS_UoPP_20170724_SocCogOrder_conv.DCh.008 (001.142)</ta>
         </annotation>
         <annotation name="st" tierref="st-DCh">
            <ta e="T7" id="Seg_305" s="T1">Пасииба. Это была первая задача.</ta>
            <ta e="T31" id="Seg_306" s="T8">Вторая задача: Я Вам даю картинки, и Вы, пожалуйста, поставьте в такой порядок, который Вам кажется правильный.</ta>
            <ta e="T46" id="Seg_307" s="T32">Нет правильно или неправильно, но такой порядок, который ((…))</ta>
            <ta e="T56" id="Seg_308" s="T47">Да, по очереди, да.</ta>
            <ta e="T64" id="Seg_309" s="T57">Какой Вам кажется правильный.</ta>
            <ta e="T107" id="Seg_310" s="T104">А һакалыы.</ta>
            <ta e="T532" id="Seg_311" s="T530">Нет.</ta>
            <ta e="T764" id="Seg_312" s="T761">Так, элэтэ, пасииба.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-DCh">
            <ta e="T7" id="Seg_313" s="T1">– Pasʼiba, eta bɨla pʼervaja zadačʼa. </ta>
            <ta e="T31" id="Seg_314" s="T8">– Ftaraja zadačʼa, ja Vam daju kartʼinkʼi, i Vɨ, požalujsta, pastavʼtʼe v takoj parʼadok, katorɨj Vam kažetsa pravʼilʼnɨj. </ta>
            <ta e="T46" id="Seg_315" s="T32">Nʼet pravʼilʼna ilʼi nʼe pravʼilʼna, no takoj parʼadak, katorɨj ((…)). </ta>
            <ta e="T56" id="Seg_316" s="T47">– Da, pa očʼerʼedʼi, da. </ta>
            <ta e="T64" id="Seg_317" s="T57">Kakoj Vam kažetsa pravʼilʼnɨj. </ta>
            <ta e="T107" id="Seg_318" s="T104">– A hakalɨː. </ta>
            <ta e="T532" id="Seg_319" s="T530">– Nʼet. </ta>
            <ta e="T764" id="Seg_320" s="T761">– Tak, elete, pasiːba. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-DCh">
            <ta e="T106" id="Seg_321" s="T104">a</ta>
            <ta e="T107" id="Seg_322" s="T106">haka-lɨː</ta>
         </annotation>
         <annotation name="mp" tierref="mp-DCh">
            <ta e="T106" id="Seg_323" s="T104">a</ta>
            <ta e="T107" id="Seg_324" s="T106">haka-LIː</ta>
         </annotation>
         <annotation name="ge" tierref="ge-DCh">
            <ta e="T106" id="Seg_325" s="T104">and</ta>
            <ta e="T107" id="Seg_326" s="T106">Dolgan-SIM</ta>
         </annotation>
         <annotation name="gg" tierref="gg-DCh">
            <ta e="T106" id="Seg_327" s="T104">und</ta>
            <ta e="T107" id="Seg_328" s="T106">dolganisch-SIM</ta>
         </annotation>
         <annotation name="gr" tierref="gr-DCh">
            <ta e="T106" id="Seg_329" s="T104">а</ta>
            <ta e="T107" id="Seg_330" s="T106">долганский-SIM</ta>
         </annotation>
         <annotation name="mc" tierref="mc-DCh">
            <ta e="T106" id="Seg_331" s="T104">conj</ta>
            <ta e="T107" id="Seg_332" s="T106">adj-adj&gt;adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps-DCh">
            <ta e="T106" id="Seg_333" s="T104">conj</ta>
            <ta e="T107" id="Seg_334" s="T106">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-DCh" />
         <annotation name="SyF" tierref="SyF-DCh" />
         <annotation name="IST" tierref="IST-DCh" />
         <annotation name="Top" tierref="Top-DCh" />
         <annotation name="Foc" tierref="Foc-DCh" />
         <annotation name="BOR" tierref="BOR-DCh">
            <ta e="T106" id="Seg_335" s="T104">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-DCh" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-DCh" />
         <annotation name="CS" tierref="CS-DCh" />
         <annotation name="fe" tierref="fe-DCh">
            <ta e="T7" id="Seg_336" s="T1">– Thank You, that was the first task.</ta>
            <ta e="T31" id="Seg_337" s="T8">– Second task: I'll give You the pictures and You put them into an order which seems to be right for You.</ta>
            <ta e="T46" id="Seg_338" s="T32">– There is no "right" or "wrong", but that order which (…).</ta>
            <ta e="T56" id="Seg_339" s="T47">– Yes, consecutively, exactly.</ta>
            <ta e="T64" id="Seg_340" s="T57">– Which seems to be right for You.</ta>
            <ta e="T107" id="Seg_341" s="T104">– And in Dolgan.</ta>
            <ta e="T532" id="Seg_342" s="T530">– No.</ta>
            <ta e="T764" id="Seg_343" s="T761">– So, that's it, thank You.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-DCh">
            <ta e="T7" id="Seg_344" s="T1">– Danke, das war die erste Aufgabe.</ta>
            <ta e="T31" id="Seg_345" s="T8">– Die zweite Aufgabe: Ich gebe Ihnen die Bilder, und Sie bringen sie bitte in die Reihenfolge, die Ihnen richtig erscheint.</ta>
            <ta e="T46" id="Seg_346" s="T32">– Es gibt nicht richtig oder falsch, sondern die Reihenfolge, die (…).</ta>
            <ta e="T56" id="Seg_347" s="T47">– Ja, nacheinander, genau.</ta>
            <ta e="T64" id="Seg_348" s="T57">– Die Ihnen richtig erscheint.</ta>
            <ta e="T107" id="Seg_349" s="T104">– Und auf Dolganisch.</ta>
            <ta e="T532" id="Seg_350" s="T530">– Nein.</ta>
            <ta e="T764" id="Seg_351" s="T761">– So, das war's, danke.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-DCh" />
         <annotation name="ltr" tierref="ltr-DCh">
            <ta e="T107" id="Seg_352" s="T104">А по-долгански.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-DCh" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-ChGS"
                      id="tx-ChGS"
                      speaker="ChGS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-ChGS">
            <ts e="T9" id="Seg_353" n="sc" s="T6">
               <ts e="T9" id="Seg_355" n="HIAT:u" s="T6">
                  <nts id="Seg_356" n="HIAT:ip">–</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_359" n="HIAT:w" s="T6">Xarašo</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T30" id="Seg_362" n="sc" s="T25">
               <ts e="T30" id="Seg_364" n="HIAT:u" s="T25">
                  <nts id="Seg_365" n="HIAT:ip">–</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_368" n="HIAT:w" s="T25">Kažetsa</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_370" n="HIAT:ip">(</nts>
                  <nts id="Seg_371" n="HIAT:ip">(</nts>
                  <ats e="T30" id="Seg_372" n="HIAT:non-pho" s="T28">…</ats>
                  <nts id="Seg_373" n="HIAT:ip">)</nts>
                  <nts id="Seg_374" n="HIAT:ip">)</nts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T58" id="Seg_377" n="sc" s="T51">
               <ts e="T58" id="Seg_379" n="HIAT:u" s="T51">
                  <nts id="Seg_380" n="HIAT:ip">–</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_383" n="HIAT:w" s="T51">Paslʼedavatʼelʼna</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T65" id="Seg_386" n="sc" s="T61">
               <ts e="T65" id="Seg_388" n="HIAT:u" s="T61">
                  <nts id="Seg_389" n="HIAT:ip">–</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_392" n="HIAT:w" s="T61">Kannanɨj</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_395" n="HIAT:w" s="T63">maŋnaj</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T78" id="Seg_398" n="sc" s="T67">
               <ts e="T76" id="Seg_400" n="HIAT:u" s="T67">
                  <nts id="Seg_401" n="HIAT:ip">–</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_403" n="HIAT:ip">(</nts>
                  <nts id="Seg_404" n="HIAT:ip">(</nts>
                  <ats e="T69" id="Seg_405" n="HIAT:non-pho" s="T67">…</ats>
                  <nts id="Seg_406" n="HIAT:ip">)</nts>
                  <nts id="Seg_407" n="HIAT:ip">)</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_410" n="HIAT:w" s="T69">kanʼec</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_413" n="HIAT:w" s="T70">diː</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_416" n="HIAT:w" s="T72">bu͡o</ts>
                  <nts id="Seg_417" n="HIAT:ip">,</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_420" n="HIAT:w" s="T74">mnʼe</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_423" n="HIAT:w" s="T75">kažetsa</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_427" n="HIAT:u" s="T76">
                  <nts id="Seg_428" n="HIAT:ip">(</nts>
                  <nts id="Seg_429" n="HIAT:ip">(</nts>
                  <ats e="T77" id="Seg_430" n="HIAT:non-pho" s="T76">…</ats>
                  <nts id="Seg_431" n="HIAT:ip">)</nts>
                  <nts id="Seg_432" n="HIAT:ip">)</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_435" n="HIAT:w" s="T77">kelbit</ts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T88" id="Seg_438" n="sc" s="T82">
               <ts e="T88" id="Seg_440" n="HIAT:u" s="T82">
                  <nts id="Seg_441" n="HIAT:ip">–</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_444" n="HIAT:w" s="T82">Iti</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_447" n="HIAT:w" s="T83">emi͡e</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_450" n="HIAT:w" s="T85">onno</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_453" n="HIAT:w" s="T87">oloror</ts>
                  <nts id="Seg_454" n="HIAT:ip">.</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T99" id="Seg_456" n="sc" s="T92">
               <ts e="T99" id="Seg_458" n="HIAT:u" s="T92">
                  <nts id="Seg_459" n="HIAT:ip">–</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_462" n="HIAT:w" s="T92">Manna</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_465" n="HIAT:w" s="T94">navʼerna</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_468" n="HIAT:w" s="T96">barɨta</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T108" id="Seg_471" n="sc" s="T105">
               <ts e="T108" id="Seg_473" n="HIAT:u" s="T105">
                  <nts id="Seg_474" n="HIAT:ip">–</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_477" n="HIAT:w" s="T105">Heː</ts>
                  <nts id="Seg_478" n="HIAT:ip">.</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T127" id="Seg_480" n="sc" s="T109">
               <ts e="T127" id="Seg_482" n="HIAT:u" s="T109">
                  <nts id="Seg_483" n="HIAT:ip">–</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_486" n="HIAT:w" s="T109">Beːbe</ts>
                  <nts id="Seg_487" n="HIAT:ip">,</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_490" n="HIAT:w" s="T110">iti</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_493" n="HIAT:w" s="T111">turar</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_496" n="HIAT:w" s="T112">tüstün</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_499" n="HIAT:w" s="T114">körü͡ök</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_502" n="HIAT:w" s="T116">bularɨ</ts>
                  <nts id="Seg_503" n="HIAT:ip">,</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_506" n="HIAT:w" s="T117">uːratalaː</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_509" n="HIAT:w" s="T119">dʼe</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_512" n="HIAT:w" s="T120">barɨtɨn</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_515" n="HIAT:w" s="T122">aragiːlaːbɨttarɨn</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_518" n="HIAT:w" s="T125">kanʼaːbɨttarɨn</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_521" n="sc" s="T133">
               <ts e="T137" id="Seg_523" n="HIAT:u" s="T133">
                  <nts id="Seg_524" n="HIAT:ip">–</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_527" n="HIAT:w" s="T133">Bu</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_530" n="HIAT:w" s="T134">itte</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_533" n="HIAT:w" s="T135">baːr</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_536" n="HIAT:w" s="T136">diː</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T163" id="Seg_539" n="sc" s="T153">
               <ts e="T163" id="Seg_541" n="HIAT:u" s="T153">
                  <nts id="Seg_542" n="HIAT:ip">–</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_545" n="HIAT:w" s="T153">Onton</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_548" n="HIAT:w" s="T154">kelen</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_551" n="HIAT:w" s="T155">bu</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_554" n="HIAT:w" s="T156">karčɨtɨn</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_557" n="HIAT:w" s="T158">barɨtɨn</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_560" n="HIAT:w" s="T160">aragiː</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_563" n="HIAT:w" s="T161">gɨmmɨta</ts>
                  <nts id="Seg_564" n="HIAT:ip">.</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_566" n="sc" s="T166">
               <ts e="T169" id="Seg_568" n="HIAT:u" s="T166">
                  <nts id="Seg_569" n="HIAT:ip">–</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_572" n="HIAT:w" s="T166">Diːgin</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_575" n="HIAT:w" s="T167">du͡o</ts>
                  <nts id="Seg_576" n="HIAT:ip">?</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T180" id="Seg_578" n="sc" s="T175">
               <ts e="T180" id="Seg_580" n="HIAT:u" s="T175">
                  <nts id="Seg_581" n="HIAT:ip">–</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_584" n="HIAT:w" s="T175">Maŋnaj</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_587" n="HIAT:w" s="T176">ispet</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_590" n="HIAT:w" s="T177">ete</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_593" n="HIAT:w" s="T178">bu͡o</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_596" n="HIAT:w" s="T179">du</ts>
                  <nts id="Seg_597" n="HIAT:ip">?</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T203" id="Seg_599" n="sc" s="T182">
               <ts e="T203" id="Seg_601" n="HIAT:u" s="T182">
                  <nts id="Seg_602" n="HIAT:ip">–</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_605" n="HIAT:w" s="T182">Kelen</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_608" n="HIAT:w" s="T184">baran</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_611" n="HIAT:w" s="T186">ispet</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_614" n="HIAT:w" s="T188">du</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_617" n="HIAT:w" s="T190">manna</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_620" n="HIAT:w" s="T192">uže</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_623" n="HIAT:w" s="T194">ogoto</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_626" n="HIAT:w" s="T196">bu</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_629" n="HIAT:w" s="T198">ulaːppɨtɨn</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_632" n="HIAT:w" s="T200">kenne</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T210" id="Seg_635" n="sc" s="T204">
               <ts e="T210" id="Seg_637" n="HIAT:u" s="T204">
                  <nts id="Seg_638" n="HIAT:ip">–</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_641" n="HIAT:w" s="T204">Itinne</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_644" n="HIAT:w" s="T207">kɨra</ts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T213" id="Seg_647" n="sc" s="T211">
               <ts e="T213" id="Seg_649" n="HIAT:u" s="T211">
                  <nts id="Seg_650" n="HIAT:ip">–</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_653" n="HIAT:w" s="T211">Ogoto</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_656" n="HIAT:w" s="T212">kɨra</ts>
                  <nts id="Seg_657" n="HIAT:ip">.</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T218" id="Seg_659" n="sc" s="T214">
               <ts e="T218" id="Seg_661" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_663" n="HIAT:w" s="T214">Kelen</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_666" n="HIAT:w" s="T215">baran</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_669" n="HIAT:w" s="T216">kepsiːr</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_672" n="HIAT:w" s="T217">du</ts>
                  <nts id="Seg_673" n="HIAT:ip">?</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T232" id="Seg_675" n="sc" s="T219">
               <ts e="T232" id="Seg_677" n="HIAT:u" s="T219">
                  <ts e="T221" id="Seg_679" n="HIAT:w" s="T219">Bu</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_682" n="HIAT:w" s="T221">kelen</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_685" n="HIAT:w" s="T222">baran</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_688" n="HIAT:w" s="T224">kepsiːr</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_691" n="HIAT:w" s="T226">eni</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_694" n="HIAT:w" s="T227">kajdak</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_696" n="HIAT:ip">(</nts>
                  <ts e="T231" id="Seg_698" n="HIAT:w" s="T229">oloro-</ts>
                  <nts id="Seg_699" n="HIAT:ip">)</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_702" n="HIAT:w" s="T231">olorbutun</ts>
                  <nts id="Seg_703" n="HIAT:ip">.</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T236" id="Seg_705" n="sc" s="T233">
               <ts e="T236" id="Seg_707" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_709" n="HIAT:w" s="T233">Ihilliː</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_712" n="HIAT:w" s="T234">olorollor</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_715" n="HIAT:w" s="T235">du</ts>
                  <nts id="Seg_716" n="HIAT:ip">?</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T239" id="Seg_718" n="sc" s="T237">
               <ts e="T239" id="Seg_720" n="HIAT:u" s="T237">
                  <ts e="T239" id="Seg_722" n="HIAT:w" s="T237">Beːbe</ts>
                  <nts id="Seg_723" n="HIAT:ip">.</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_725" n="sc" s="T247">
               <ts e="T256" id="Seg_727" n="HIAT:u" s="T247">
                  <nts id="Seg_728" n="HIAT:ip">–</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_731" n="HIAT:w" s="T247">Bu</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_734" n="HIAT:w" s="T248">baːr</ts>
                  <nts id="Seg_735" n="HIAT:ip">,</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_738" n="HIAT:w" s="T250">bu</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_741" n="HIAT:w" s="T251">baːr</ts>
                  <nts id="Seg_742" n="HIAT:ip">,</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_745" n="HIAT:w" s="T253">bu</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_748" n="HIAT:w" s="T254">baːr</ts>
                  <nts id="Seg_749" n="HIAT:ip">.</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T262" id="Seg_751" n="sc" s="T258">
               <ts e="T262" id="Seg_753" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_755" n="HIAT:w" s="T258">Beːbe</ts>
                  <nts id="Seg_756" n="HIAT:ip">,</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_759" n="HIAT:w" s="T259">maŋnajbɨtɨn</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_762" n="HIAT:w" s="T260">bulu͡okputun</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_765" n="HIAT:w" s="T261">naːda</ts>
                  <nts id="Seg_766" n="HIAT:ip">.</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T276" id="Seg_768" n="sc" s="T263">
               <ts e="T276" id="Seg_770" n="HIAT:u" s="T263">
                  <ts e="T265" id="Seg_772" n="HIAT:w" s="T263">Bu</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_775" n="HIAT:w" s="T265">ke</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_778" n="HIAT:w" s="T266">kata</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_781" n="HIAT:w" s="T267">kaːjɨːttan</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_784" n="HIAT:w" s="T268">taksɨbɨt</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_787" n="HIAT:w" s="T270">bɨhɨlaːk</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_790" n="HIAT:w" s="T272">bu͡o</ts>
                  <nts id="Seg_791" n="HIAT:ip">,</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_794" n="HIAT:w" s="T273">bu</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_797" n="HIAT:w" s="T274">ü͡örer</ts>
                  <nts id="Seg_798" n="HIAT:ip">.</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T285" id="Seg_800" n="sc" s="T278">
               <ts e="T285" id="Seg_802" n="HIAT:u" s="T278">
                  <nts id="Seg_803" n="HIAT:ip">–</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_806" n="HIAT:w" s="T278">Kaːjɨːga</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_809" n="HIAT:w" s="T279">diː</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_812" n="HIAT:w" s="T280">gini</ts>
                  <nts id="Seg_813" n="HIAT:ip">,</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_816" n="HIAT:w" s="T282">kažetsa</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_819" n="HIAT:w" s="T284">du</ts>
                  <nts id="Seg_820" n="HIAT:ip">.</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T291" id="Seg_822" n="sc" s="T286">
               <ts e="T291" id="Seg_824" n="HIAT:u" s="T286">
                  <nts id="Seg_825" n="HIAT:ip">–</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_828" n="HIAT:w" s="T286">Üleliːr</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_831" n="HIAT:w" s="T289">du</ts>
                  <nts id="Seg_832" n="HIAT:ip">?</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T320" id="Seg_834" n="sc" s="T312">
               <ts e="T320" id="Seg_836" n="HIAT:u" s="T312">
                  <nts id="Seg_837" n="HIAT:ip">–</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_840" n="HIAT:w" s="T312">Bagarar</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_843" n="HIAT:w" s="T313">ogoto</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_846" n="HIAT:w" s="T314">iti</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_849" n="HIAT:w" s="T315">kördük</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_852" n="HIAT:w" s="T316">hüːre-hüːre</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_855" n="HIAT:w" s="T317">keli͡ek</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_858" n="HIAT:w" s="T318">ete</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_861" n="HIAT:w" s="T319">gini͡eke</ts>
                  <nts id="Seg_862" n="HIAT:ip">.</nts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T349" id="Seg_864" n="sc" s="T326">
               <ts e="T349" id="Seg_866" n="HIAT:u" s="T326">
                  <nts id="Seg_867" n="HIAT:ip">–</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_870" n="HIAT:w" s="T326">Manna</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_873" n="HIAT:w" s="T328">navʼerna</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_876" n="HIAT:w" s="T329">kör</ts>
                  <nts id="Seg_877" n="HIAT:ip">,</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_880" n="HIAT:w" s="T331">bu</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_883" n="HIAT:w" s="T332">aragiːlɨː</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_886" n="HIAT:w" s="T333">oloron</ts>
                  <nts id="Seg_887" n="HIAT:ip">,</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_890" n="HIAT:w" s="T335">bu</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_893" n="HIAT:w" s="T336">aragiːlɨː</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_896" n="HIAT:w" s="T338">oloronnor</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_899" n="HIAT:w" s="T339">kepsete</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_901" n="HIAT:ip">(</nts>
                  <ts e="T341" id="Seg_903" n="HIAT:w" s="T340">ke-</ts>
                  <nts id="Seg_904" n="HIAT:ip">)</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_907" n="HIAT:w" s="T341">bu</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_910" n="HIAT:w" s="T342">kihi</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_913" n="HIAT:w" s="T343">kepsiːr</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_916" n="HIAT:w" s="T344">diː</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_918" n="HIAT:ip">"</nts>
                  <ts e="T346" id="Seg_920" n="HIAT:w" s="T345">dʼaktargɨn</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_923" n="HIAT:w" s="T346">körbütüm</ts>
                  <nts id="Seg_924" n="HIAT:ip">"</nts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_927" n="HIAT:w" s="T347">diːr</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_930" n="HIAT:w" s="T348">bu͡o</ts>
                  <nts id="Seg_931" n="HIAT:ip">.</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T353" id="Seg_933" n="sc" s="T350">
               <ts e="T353" id="Seg_935" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_937" n="HIAT:w" s="T350">Kɨjŋammɨt</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_940" n="HIAT:w" s="T351">uže</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_943" n="HIAT:w" s="T352">bu͡o</ts>
                  <nts id="Seg_944" n="HIAT:ip">.</nts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T373" id="Seg_946" n="sc" s="T354">
               <ts e="T369" id="Seg_948" n="HIAT:u" s="T354">
                  <ts e="T356" id="Seg_950" n="HIAT:w" s="T354">Onton</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_953" n="HIAT:w" s="T356">kelen</ts>
                  <nts id="Seg_954" n="HIAT:ip">,</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_957" n="HIAT:w" s="T358">dʼe</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_960" n="HIAT:w" s="T360">onton</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_963" n="HIAT:w" s="T362">kelen</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_966" n="HIAT:w" s="T364">baran</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_969" n="HIAT:w" s="T366">atnašenʼije</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_972" n="HIAT:w" s="T367">vɨjasnʼajdɨːr</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_975" n="HIAT:w" s="T368">bu͡o</ts>
                  <nts id="Seg_976" n="HIAT:ip">.</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_979" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_981" n="HIAT:w" s="T369">Kɨrbaːrɨ</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_984" n="HIAT:w" s="T370">gɨnar</ts>
                  <nts id="Seg_985" n="HIAT:ip">,</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_988" n="HIAT:w" s="T371">ol</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_991" n="HIAT:w" s="T372">baːr</ts>
                  <nts id="Seg_992" n="HIAT:ip">.</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T381" id="Seg_994" n="sc" s="T375">
               <ts e="T381" id="Seg_996" n="HIAT:u" s="T375">
                  <nts id="Seg_997" n="HIAT:ip">–</nts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1000" n="HIAT:w" s="T375">Onton</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1003" n="HIAT:w" s="T376">bu</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1006" n="HIAT:w" s="T377">uže</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1009" n="HIAT:w" s="T378">kɨrbaːbɨt</ts>
                  <nts id="Seg_1010" n="HIAT:ip">,</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1013" n="HIAT:w" s="T379">oksubut</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1016" n="HIAT:w" s="T380">uže</ts>
                  <nts id="Seg_1017" n="HIAT:ip">.</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T390" id="Seg_1019" n="sc" s="T385">
               <ts e="T390" id="Seg_1021" n="HIAT:u" s="T385">
                  <nts id="Seg_1022" n="HIAT:ip">–</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1025" n="HIAT:w" s="T385">Bu</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1028" n="HIAT:w" s="T386">kördük</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1031" n="HIAT:w" s="T387">možet</ts>
                  <nts id="Seg_1032" n="HIAT:ip">,</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1035" n="HIAT:w" s="T389">da</ts>
                  <nts id="Seg_1036" n="HIAT:ip">?</nts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T395" id="Seg_1038" n="sc" s="T391">
               <ts e="T395" id="Seg_1040" n="HIAT:u" s="T391">
                  <nts id="Seg_1041" n="HIAT:ip">–</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1044" n="HIAT:w" s="T391">Egel</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1047" n="HIAT:w" s="T393">bettek</ts>
                  <nts id="Seg_1048" n="HIAT:ip">.</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T401" id="Seg_1050" n="sc" s="T397">
               <ts e="T401" id="Seg_1052" n="HIAT:u" s="T397">
                  <nts id="Seg_1053" n="HIAT:ip">–</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1056" n="HIAT:w" s="T397">Egel</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1059" n="HIAT:w" s="T398">biːr</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1061" n="HIAT:ip">(</nts>
                  <nts id="Seg_1062" n="HIAT:ip">(</nts>
                  <ats e="T401" id="Seg_1063" n="HIAT:non-pho" s="T399">…</ats>
                  <nts id="Seg_1064" n="HIAT:ip">)</nts>
                  <nts id="Seg_1065" n="HIAT:ip">)</nts>
                  <nts id="Seg_1066" n="HIAT:ip">.</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T411" id="Seg_1068" n="sc" s="T402">
               <ts e="T411" id="Seg_1070" n="HIAT:u" s="T402">
                  <nts id="Seg_1071" n="HIAT:ip">–</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1074" n="HIAT:w" s="T402">Bu</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1077" n="HIAT:w" s="T404">baːr</ts>
                  <nts id="Seg_1078" n="HIAT:ip">,</nts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1081" n="HIAT:w" s="T405">maŋnaj</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1084" n="HIAT:w" s="T407">bu</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1087" n="HIAT:w" s="T408">baːr</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1090" n="HIAT:w" s="T409">ke</ts>
                  <nts id="Seg_1091" n="HIAT:ip">.</nts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T447" id="Seg_1093" n="sc" s="T427">
               <ts e="T447" id="Seg_1095" n="HIAT:u" s="T427">
                  <nts id="Seg_1096" n="HIAT:ip">–</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1099" n="HIAT:w" s="T427">Hu͡ok</ts>
                  <nts id="Seg_1100" n="HIAT:ip">,</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1103" n="HIAT:w" s="T429">bu</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1106" n="HIAT:w" s="T430">uže</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1109" n="HIAT:w" s="T431">kanʼɨhɨgar</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1111" n="HIAT:ip">"</nts>
                  <ts e="T433" id="Seg_1113" n="HIAT:w" s="T432">ispeppin</ts>
                  <nts id="Seg_1114" n="HIAT:ip">"</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1117" n="HIAT:w" s="T433">diːr</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1120" n="HIAT:w" s="T434">uže</ts>
                  <nts id="Seg_1121" n="HIAT:ip">,</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1124" n="HIAT:w" s="T435">ikki</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1127" n="HIAT:w" s="T436">paslʼednʼij</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1130" n="HIAT:w" s="T437">kelen</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1133" n="HIAT:w" s="T438">barar</ts>
                  <nts id="Seg_1134" n="HIAT:ip">,</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1137" n="HIAT:w" s="T439">kaːjɨːttan</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1140" n="HIAT:w" s="T441">kelen</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1143" n="HIAT:w" s="T442">baran</ts>
                  <nts id="Seg_1144" n="HIAT:ip">,</nts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1147" n="HIAT:w" s="T443">ja</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1150" n="HIAT:w" s="T445">tak</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1153" n="HIAT:w" s="T446">dumaju</ts>
                  <nts id="Seg_1154" n="HIAT:ip">.</nts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T449" id="Seg_1156" n="sc" s="T448">
               <ts e="T449" id="Seg_1158" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1160" n="HIAT:w" s="T448">Eː</ts>
                  <nts id="Seg_1161" n="HIAT:ip">?</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T454" id="Seg_1163" n="sc" s="T450">
               <ts e="T454" id="Seg_1165" n="HIAT:u" s="T450">
                  <nts id="Seg_1166" n="HIAT:ip">–</nts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1168" n="HIAT:ip">(</nts>
                  <nts id="Seg_1169" n="HIAT:ip">(</nts>
                  <ats e="T453" id="Seg_1170" n="HIAT:non-pho" s="T450">…</ats>
                  <nts id="Seg_1171" n="HIAT:ip">)</nts>
                  <nts id="Seg_1172" n="HIAT:ip">)</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1175" n="HIAT:w" s="T453">ispet</ts>
                  <nts id="Seg_1176" n="HIAT:ip">.</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T478" id="Seg_1178" n="sc" s="T461">
               <ts e="T478" id="Seg_1180" n="HIAT:u" s="T461">
                  <nts id="Seg_1181" n="HIAT:ip">–</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1184" n="HIAT:w" s="T461">Üčügej</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1187" n="HIAT:w" s="T464">bagajɨ</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1190" n="HIAT:w" s="T466">oloror</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1193" n="HIAT:w" s="T468">etiler</ts>
                  <nts id="Seg_1194" n="HIAT:ip">,</nts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1197" n="HIAT:w" s="T469">oloror</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1200" n="HIAT:w" s="T471">etiler</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1203" n="HIAT:w" s="T473">eni</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1206" n="HIAT:w" s="T475">üleliː</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1209" n="HIAT:w" s="T477">hɨldʼallar</ts>
                  <nts id="Seg_1210" n="HIAT:ip">.</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T506" id="Seg_1212" n="sc" s="T484">
               <ts e="T506" id="Seg_1214" n="HIAT:u" s="T484">
                  <nts id="Seg_1215" n="HIAT:ip">–</nts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1218" n="HIAT:w" s="T484">Če</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1221" n="HIAT:w" s="T485">iti</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1224" n="HIAT:w" s="T486">uže</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1227" n="HIAT:w" s="T487">kaːjɨːtɨn</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1230" n="HIAT:w" s="T488">kepsiːr</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1233" n="HIAT:w" s="T489">diː</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1236" n="HIAT:w" s="T491">gini</ts>
                  <nts id="Seg_1237" n="HIAT:ip">,</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1240" n="HIAT:w" s="T494">kojukkaːn</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1243" n="HIAT:w" s="T496">eni</ts>
                  <nts id="Seg_1244" n="HIAT:ip">,</nts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1247" n="HIAT:w" s="T498">bu</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1250" n="HIAT:w" s="T499">bu</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1253" n="HIAT:w" s="T501">kennitten</ts>
                  <nts id="Seg_1254" n="HIAT:ip">,</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1257" n="HIAT:w" s="T502">bu</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1260" n="HIAT:w" s="T503">kördük</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1263" n="HIAT:w" s="T505">bu͡olu͡oga</ts>
                  <nts id="Seg_1264" n="HIAT:ip">.</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T516" id="Seg_1266" n="sc" s="T507">
               <ts e="T516" id="Seg_1268" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_1270" n="HIAT:w" s="T507">Vot</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1273" n="HIAT:w" s="T508">tak</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1276" n="HIAT:w" s="T509">vot</ts>
                  <nts id="Seg_1277" n="HIAT:ip">,</nts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1280" n="HIAT:w" s="T510">i͡e</ts>
                  <nts id="Seg_1281" n="HIAT:ip">,</nts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1284" n="HIAT:w" s="T511">dapustiʼm</ts>
                  <nts id="Seg_1285" n="HIAT:ip">,</nts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1288" n="HIAT:w" s="T512">pustʼ</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1291" n="HIAT:w" s="T513">budʼet</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1294" n="HIAT:w" s="T514">iti</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1297" n="HIAT:w" s="T515">kördük</ts>
                  <nts id="Seg_1298" n="HIAT:ip">.</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T529" id="Seg_1300" n="sc" s="T517">
               <ts e="T529" id="Seg_1302" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1304" n="HIAT:w" s="T517">A</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1307" n="HIAT:w" s="T518">tut</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1310" n="HIAT:w" s="T519">uvʼidʼela</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1313" n="HIAT:w" s="T520">ja</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1316" n="HIAT:w" s="T521">etʼi</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1319" n="HIAT:w" s="T522">sʼifrɨ</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1322" n="HIAT:w" s="T523">prastavlenɨ</ts>
                  <nts id="Seg_1323" n="HIAT:ip">,</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1326" n="HIAT:w" s="T524">eta</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1329" n="HIAT:w" s="T525">v</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1332" n="HIAT:w" s="T526">pravʼilʼnam</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1335" n="HIAT:w" s="T527">parʼadke</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1338" n="HIAT:w" s="T528">prastavlʼena</ts>
                  <nts id="Seg_1339" n="HIAT:ip">?</nts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T537" id="Seg_1341" n="sc" s="T531">
               <ts e="T537" id="Seg_1343" n="HIAT:u" s="T531">
                  <nts id="Seg_1344" n="HIAT:ip">–</nts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1347" n="HIAT:w" s="T531">Nʼe</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1350" n="HIAT:w" s="T533">nada</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1353" n="HIAT:w" s="T534">na</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1356" n="HIAT:w" s="T535">nʼix</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1359" n="HIAT:w" s="T536">smatrʼetʼ</ts>
                  <nts id="Seg_1360" n="HIAT:ip">.</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T542" id="Seg_1362" n="sc" s="T538">
               <ts e="T542" id="Seg_1364" n="HIAT:u" s="T538">
                  <ts e="T539" id="Seg_1366" n="HIAT:w" s="T538">Ja</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1369" n="HIAT:w" s="T539">xʼitraja</ts>
                  <nts id="Seg_1370" n="HIAT:ip">,</nts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1373" n="HIAT:w" s="T540">to</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1376" n="HIAT:w" s="T541">uvižu</ts>
                  <nts id="Seg_1377" n="HIAT:ip">.</nts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T559" id="Seg_1379" n="sc" s="T543">
               <ts e="T554" id="Seg_1381" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_1383" n="HIAT:w" s="T543">Dʼe</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1386" n="HIAT:w" s="T544">bu</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1389" n="HIAT:w" s="T545">baːllar</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1392" n="HIAT:w" s="T546">diːr</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1395" n="HIAT:w" s="T547">maŋnaj</ts>
                  <nts id="Seg_1396" n="HIAT:ip">,</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1399" n="HIAT:w" s="T548">üleliː</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1402" n="HIAT:w" s="T549">hɨldʼan</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1405" n="HIAT:w" s="T550">bu͡o</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1408" n="HIAT:w" s="T551">bu</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1411" n="HIAT:w" s="T552">aragiːmsaktara</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1414" n="HIAT:w" s="T553">kimneːbit</ts>
                  <nts id="Seg_1415" n="HIAT:ip">.</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T559" id="Seg_1418" n="HIAT:u" s="T554">
                  <ts e="T559" id="Seg_1420" n="HIAT:w" s="T554">Tübespit</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T568" id="Seg_1423" n="sc" s="T564">
               <ts e="T568" id="Seg_1425" n="HIAT:u" s="T564">
                  <nts id="Seg_1426" n="HIAT:ip">–</nts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1429" n="HIAT:w" s="T564">Dʼe</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1432" n="HIAT:w" s="T565">bu</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1435" n="HIAT:w" s="T566">baːllar</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1438" n="HIAT:w" s="T567">diː</ts>
                  <nts id="Seg_1439" n="HIAT:ip">.</nts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T576" id="Seg_1441" n="sc" s="T570">
               <ts e="T576" id="Seg_1443" n="HIAT:u" s="T570">
                  <nts id="Seg_1444" n="HIAT:ip">–</nts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1447" n="HIAT:w" s="T570">Bu</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1450" n="HIAT:w" s="T572">iher</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1453" n="HIAT:w" s="T574">olorollor</ts>
                  <nts id="Seg_1454" n="HIAT:ip">.</nts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T584" id="Seg_1456" n="sc" s="T577">
               <ts e="T584" id="Seg_1458" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_1460" n="HIAT:w" s="T577">Ihe</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1463" n="HIAT:w" s="T578">oloror</ts>
                  <nts id="Seg_1464" n="HIAT:ip">,</nts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1467" n="HIAT:w" s="T579">onton</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1470" n="HIAT:w" s="T580">bu</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1473" n="HIAT:w" s="T581">kepsiːller</ts>
                  <nts id="Seg_1474" n="HIAT:ip">,</nts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1477" n="HIAT:w" s="T582">gini</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_1480" n="HIAT:w" s="T583">kɨjŋanar</ts>
                  <nts id="Seg_1481" n="HIAT:ip">.</nts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T593" id="Seg_1483" n="sc" s="T585">
               <ts e="T593" id="Seg_1485" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_1487" n="HIAT:w" s="T585">Kɨjŋanan</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1490" n="HIAT:w" s="T586">baran</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1493" n="HIAT:w" s="T587">dʼaktarga</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_1496" n="HIAT:w" s="T588">barar</ts>
                  <nts id="Seg_1497" n="HIAT:ip">,</nts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_1500" n="HIAT:w" s="T589">ol</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_1503" n="HIAT:w" s="T590">baːr</ts>
                  <nts id="Seg_1504" n="HIAT:ip">,</nts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_1507" n="HIAT:w" s="T591">egel</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_1510" n="HIAT:w" s="T592">onu</ts>
                  <nts id="Seg_1511" n="HIAT:ip">.</nts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T608" id="Seg_1513" n="sc" s="T601">
               <ts e="T608" id="Seg_1515" n="HIAT:u" s="T601">
                  <nts id="Seg_1516" n="HIAT:ip">–</nts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_1519" n="HIAT:w" s="T601">Kulaːkɨtɨn</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_1522" n="HIAT:w" s="T604">belemnemmit</ts>
                  <nts id="Seg_1523" n="HIAT:ip">.</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T618" id="Seg_1525" n="sc" s="T610">
               <ts e="T618" id="Seg_1527" n="HIAT:u" s="T610">
                  <nts id="Seg_1528" n="HIAT:ip">–</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_1531" n="HIAT:w" s="T610">Da</ts>
                  <nts id="Seg_1532" n="HIAT:ip">,</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_1535" n="HIAT:w" s="T612">da</ts>
                  <nts id="Seg_1536" n="HIAT:ip">,</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_1539" n="HIAT:w" s="T613">da</ts>
                  <nts id="Seg_1540" n="HIAT:ip">,</nts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_1543" n="HIAT:w" s="T615">da</ts>
                  <nts id="Seg_1544" n="HIAT:ip">,</nts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_1547" n="HIAT:w" s="T616">da</ts>
                  <nts id="Seg_1548" n="HIAT:ip">.</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T640" id="Seg_1550" n="sc" s="T621">
               <ts e="T640" id="Seg_1552" n="HIAT:u" s="T621">
                  <nts id="Seg_1553" n="HIAT:ip">–</nts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_1556" n="HIAT:w" s="T621">A</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_1559" n="HIAT:w" s="T623">tam</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_1562" n="HIAT:w" s="T624">üčügej</ts>
                  <nts id="Seg_1563" n="HIAT:ip">,</nts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_1566" n="HIAT:w" s="T626">bu</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_1569" n="HIAT:w" s="T627">baːr</ts>
                  <nts id="Seg_1570" n="HIAT:ip">,</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_1573" n="HIAT:w" s="T629">üleliː</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_1576" n="HIAT:w" s="T630">hɨldʼallar</ts>
                  <nts id="Seg_1577" n="HIAT:ip">,</nts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_1580" n="HIAT:w" s="T631">oloror</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_1583" n="HIAT:w" s="T633">etilere</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_1586" n="HIAT:w" s="T634">eni</ts>
                  <nts id="Seg_1587" n="HIAT:ip">,</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_1590" n="HIAT:w" s="T636">kör</ts>
                  <nts id="Seg_1591" n="HIAT:ip">,</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_1594" n="HIAT:w" s="T637">bu</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_1597" n="HIAT:w" s="T638">vmʼestʼe</ts>
                  <nts id="Seg_1598" n="HIAT:ip">.</nts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T651" id="Seg_1600" n="sc" s="T647">
               <ts e="T651" id="Seg_1602" n="HIAT:u" s="T647">
                  <nts id="Seg_1603" n="HIAT:ip">–</nts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_1606" n="HIAT:w" s="T647">Bul</ts>
                  <nts id="Seg_1607" n="HIAT:ip">,</nts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_1610" n="HIAT:w" s="T649">beːbe</ts>
                  <nts id="Seg_1611" n="HIAT:ip">.</nts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T667" id="Seg_1613" n="sc" s="T654">
               <ts e="T667" id="Seg_1615" n="HIAT:u" s="T654">
                  <nts id="Seg_1616" n="HIAT:ip">–</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_1619" n="HIAT:w" s="T654">Bu</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_1622" n="HIAT:w" s="T656">baːr</ts>
                  <nts id="Seg_1623" n="HIAT:ip">,</nts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_1626" n="HIAT:w" s="T658">bu</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_1629" n="HIAT:w" s="T660">baːr</ts>
                  <nts id="Seg_1630" n="HIAT:ip">,</nts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_1633" n="HIAT:w" s="T662">munu</ts>
                  <nts id="Seg_1634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_1636" n="HIAT:w" s="T664">munu</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_1639" n="HIAT:w" s="T665">daprašɨvajdɨːllar</ts>
                  <nts id="Seg_1640" n="HIAT:ip">.</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T672" id="Seg_1642" n="sc" s="T669">
               <ts e="T672" id="Seg_1644" n="HIAT:u" s="T669">
                  <nts id="Seg_1645" n="HIAT:ip">–</nts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_1648" n="HIAT:w" s="T669">Bu</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_1651" n="HIAT:w" s="T670">baːllar</ts>
                  <nts id="Seg_1652" n="HIAT:ip">,</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_1655" n="HIAT:w" s="T671">daprašɨvajdɨːllar</ts>
                  <nts id="Seg_1656" n="HIAT:ip">.</nts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T677" id="Seg_1658" n="sc" s="T673">
               <ts e="T677" id="Seg_1660" n="HIAT:u" s="T673">
                  <nts id="Seg_1661" n="HIAT:ip">–</nts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_1664" n="HIAT:w" s="T673">Taŋas</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_1667" n="HIAT:w" s="T676">bi͡ereller</ts>
                  <nts id="Seg_1668" n="HIAT:ip">.</nts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T682" id="Seg_1670" n="sc" s="T678">
               <ts e="T682" id="Seg_1672" n="HIAT:u" s="T678">
                  <ts e="T679" id="Seg_1674" n="HIAT:w" s="T678">Kaːjɨllɨbɨt</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_1677" n="HIAT:w" s="T679">oloror</ts>
                  <nts id="Seg_1678" n="HIAT:ip">,</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_1681" n="HIAT:w" s="T680">hanaːga</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_1684" n="HIAT:w" s="T681">battappɨt</ts>
                  <nts id="Seg_1685" n="HIAT:ip">.</nts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T688" id="Seg_1687" n="sc" s="T683">
               <ts e="T688" id="Seg_1689" n="HIAT:u" s="T683">
                  <ts e="T684" id="Seg_1691" n="HIAT:w" s="T683">Bu</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_1694" n="HIAT:w" s="T684">emi͡e</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_1697" n="HIAT:w" s="T685">hanaːga</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_1700" n="HIAT:w" s="T686">battappɨt</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_1703" n="HIAT:w" s="T687">oloror</ts>
                  <nts id="Seg_1704" n="HIAT:ip">.</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T697" id="Seg_1706" n="sc" s="T689">
               <ts e="T697" id="Seg_1708" n="HIAT:u" s="T689">
                  <ts e="T690" id="Seg_1710" n="HIAT:w" s="T689">Bu</ts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_1713" n="HIAT:w" s="T690">bu͡o</ts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_1716" n="HIAT:w" s="T691">uže</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_1719" n="HIAT:w" s="T692">mʼečʼtajdɨː</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_1722" n="HIAT:w" s="T693">oloror</ts>
                  <nts id="Seg_1723" n="HIAT:ip">,</nts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_1726" n="HIAT:w" s="T694">navʼernaje</ts>
                  <nts id="Seg_1727" n="HIAT:ip">,</nts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_1730" n="HIAT:w" s="T695">ogoto</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_1733" n="HIAT:w" s="T696">fstrʼečʼajdɨːrɨn</ts>
                  <nts id="Seg_1734" n="HIAT:ip">.</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T705" id="Seg_1736" n="sc" s="T702">
               <ts e="T705" id="Seg_1738" n="HIAT:u" s="T702">
                  <nts id="Seg_1739" n="HIAT:ip">–</nts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_1742" n="HIAT:w" s="T702">Iti</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_1745" n="HIAT:w" s="T704">kördük</ts>
                  <nts id="Seg_1746" n="HIAT:ip">.</nts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T717" id="Seg_1748" n="sc" s="T708">
               <ts e="T717" id="Seg_1750" n="HIAT:u" s="T708">
                  <nts id="Seg_1751" n="HIAT:ip">–</nts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_1754" n="HIAT:w" s="T708">Da</ts>
                  <nts id="Seg_1755" n="HIAT:ip">,</nts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_1758" n="HIAT:w" s="T709">uže</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_1761" n="HIAT:w" s="T711">prʼedstavlʼajdɨːr</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_1764" n="HIAT:w" s="T713">dʼi͡etiger</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_1767" n="HIAT:w" s="T714">kelerin</ts>
                  <nts id="Seg_1768" n="HIAT:ip">,</nts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_1771" n="HIAT:w" s="T715">navʼerna</ts>
                  <nts id="Seg_1772" n="HIAT:ip">.</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T729" id="Seg_1774" n="sc" s="T723">
               <ts e="T729" id="Seg_1776" n="HIAT:u" s="T723">
                  <nts id="Seg_1777" n="HIAT:ip">–</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_1780" n="HIAT:w" s="T723">Onton</ts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_1783" n="HIAT:w" s="T725">ol</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_1786" n="HIAT:w" s="T727">taksɨbɨt</ts>
                  <nts id="Seg_1787" n="HIAT:ip">.</nts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T735" id="Seg_1789" n="sc" s="T734">
               <ts e="T735" id="Seg_1791" n="HIAT:u" s="T734">
                  <nts id="Seg_1792" n="HIAT:ip">–</nts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_1795" n="HIAT:w" s="T734">Taksɨbɨt</ts>
                  <nts id="Seg_1796" n="HIAT:ip">.</nts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T743" id="Seg_1798" n="sc" s="T739">
               <ts e="T743" id="Seg_1800" n="HIAT:u" s="T739">
                  <nts id="Seg_1801" n="HIAT:ip">–</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_1804" n="HIAT:w" s="T739">Taksɨbɨt</ts>
                  <nts id="Seg_1805" n="HIAT:ip">,</nts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_1808" n="HIAT:w" s="T740">kelbit</ts>
                  <nts id="Seg_1809" n="HIAT:ip">,</nts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_1812" n="HIAT:w" s="T741">ü͡öre-ü͡öre</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_1815" n="HIAT:w" s="T742">radastnɨj</ts>
                  <nts id="Seg_1816" n="HIAT:ip">.</nts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T747" id="Seg_1818" n="sc" s="T744">
               <ts e="T747" id="Seg_1820" n="HIAT:u" s="T744">
                  <ts e="T745" id="Seg_1822" n="HIAT:w" s="T744">Onton</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_1825" n="HIAT:w" s="T745">kepsiː</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_1828" n="HIAT:w" s="T746">oloror</ts>
                  <nts id="Seg_1829" n="HIAT:ip">.</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T758" id="Seg_1831" n="sc" s="T748">
               <ts e="T758" id="Seg_1833" n="HIAT:u" s="T748">
                  <ts e="T749" id="Seg_1835" n="HIAT:w" s="T748">Onton</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1837" n="HIAT:ip">(</nts>
                  <nts id="Seg_1838" n="HIAT:ip">(</nts>
                  <ats e="T752" id="Seg_1839" n="HIAT:non-pho" s="T749">…</ats>
                  <nts id="Seg_1840" n="HIAT:ip">)</nts>
                  <nts id="Seg_1841" n="HIAT:ip">)</nts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_1844" n="HIAT:w" s="T752">ispet</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_1847" n="HIAT:w" s="T756">bu͡olbut</ts>
                  <nts id="Seg_1848" n="HIAT:ip">.</nts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-ChGS">
            <ts e="T9" id="Seg_1850" n="sc" s="T6">
               <ts e="T9" id="Seg_1852" n="e" s="T6">– Xarašo. </ts>
            </ts>
            <ts e="T30" id="Seg_1853" n="sc" s="T25">
               <ts e="T28" id="Seg_1855" n="e" s="T25">– Kažetsa </ts>
               <ts e="T30" id="Seg_1857" n="e" s="T28">((…)). </ts>
            </ts>
            <ts e="T58" id="Seg_1858" n="sc" s="T51">
               <ts e="T58" id="Seg_1860" n="e" s="T51">– Paslʼedavatʼelʼna. </ts>
            </ts>
            <ts e="T65" id="Seg_1861" n="sc" s="T61">
               <ts e="T63" id="Seg_1863" n="e" s="T61">– Kannanɨj </ts>
               <ts e="T65" id="Seg_1865" n="e" s="T63">maŋnaj. </ts>
            </ts>
            <ts e="T78" id="Seg_1866" n="sc" s="T67">
               <ts e="T69" id="Seg_1868" n="e" s="T67">– ((…)) </ts>
               <ts e="T70" id="Seg_1870" n="e" s="T69">kanʼec </ts>
               <ts e="T72" id="Seg_1872" n="e" s="T70">diː </ts>
               <ts e="T74" id="Seg_1874" n="e" s="T72">bu͡o, </ts>
               <ts e="T75" id="Seg_1876" n="e" s="T74">mnʼe </ts>
               <ts e="T76" id="Seg_1878" n="e" s="T75">kažetsa. </ts>
               <ts e="T77" id="Seg_1880" n="e" s="T76">((…)) </ts>
               <ts e="T78" id="Seg_1882" n="e" s="T77">kelbit. </ts>
            </ts>
            <ts e="T88" id="Seg_1883" n="sc" s="T82">
               <ts e="T83" id="Seg_1885" n="e" s="T82">– Iti </ts>
               <ts e="T85" id="Seg_1887" n="e" s="T83">emi͡e </ts>
               <ts e="T87" id="Seg_1889" n="e" s="T85">onno </ts>
               <ts e="T88" id="Seg_1891" n="e" s="T87">oloror. </ts>
            </ts>
            <ts e="T99" id="Seg_1892" n="sc" s="T92">
               <ts e="T94" id="Seg_1894" n="e" s="T92">– Manna </ts>
               <ts e="T96" id="Seg_1896" n="e" s="T94">navʼerna </ts>
               <ts e="T99" id="Seg_1898" n="e" s="T96">barɨta. </ts>
            </ts>
            <ts e="T108" id="Seg_1899" n="sc" s="T105">
               <ts e="T108" id="Seg_1901" n="e" s="T105">– Heː. </ts>
            </ts>
            <ts e="T127" id="Seg_1902" n="sc" s="T109">
               <ts e="T110" id="Seg_1904" n="e" s="T109">– Beːbe, </ts>
               <ts e="T111" id="Seg_1906" n="e" s="T110">iti </ts>
               <ts e="T112" id="Seg_1908" n="e" s="T111">turar </ts>
               <ts e="T114" id="Seg_1910" n="e" s="T112">tüstün </ts>
               <ts e="T116" id="Seg_1912" n="e" s="T114">körü͡ök </ts>
               <ts e="T117" id="Seg_1914" n="e" s="T116">bularɨ, </ts>
               <ts e="T119" id="Seg_1916" n="e" s="T117">uːratalaː </ts>
               <ts e="T120" id="Seg_1918" n="e" s="T119">dʼe </ts>
               <ts e="T122" id="Seg_1920" n="e" s="T120">barɨtɨn </ts>
               <ts e="T125" id="Seg_1922" n="e" s="T122">aragiːlaːbɨttarɨn </ts>
               <ts e="T127" id="Seg_1924" n="e" s="T125">kanʼaːbɨttarɨn. </ts>
            </ts>
            <ts e="T137" id="Seg_1925" n="sc" s="T133">
               <ts e="T134" id="Seg_1927" n="e" s="T133">– Bu </ts>
               <ts e="T135" id="Seg_1929" n="e" s="T134">itte </ts>
               <ts e="T136" id="Seg_1931" n="e" s="T135">baːr </ts>
               <ts e="T137" id="Seg_1933" n="e" s="T136">diː. </ts>
            </ts>
            <ts e="T163" id="Seg_1934" n="sc" s="T153">
               <ts e="T154" id="Seg_1936" n="e" s="T153">– Onton </ts>
               <ts e="T155" id="Seg_1938" n="e" s="T154">kelen </ts>
               <ts e="T156" id="Seg_1940" n="e" s="T155">bu </ts>
               <ts e="T158" id="Seg_1942" n="e" s="T156">karčɨtɨn </ts>
               <ts e="T160" id="Seg_1944" n="e" s="T158">barɨtɨn </ts>
               <ts e="T161" id="Seg_1946" n="e" s="T160">aragiː </ts>
               <ts e="T163" id="Seg_1948" n="e" s="T161">gɨmmɨta. </ts>
            </ts>
            <ts e="T169" id="Seg_1949" n="sc" s="T166">
               <ts e="T167" id="Seg_1951" n="e" s="T166">– Diːgin </ts>
               <ts e="T169" id="Seg_1953" n="e" s="T167">du͡o? </ts>
            </ts>
            <ts e="T180" id="Seg_1954" n="sc" s="T175">
               <ts e="T176" id="Seg_1956" n="e" s="T175">– Maŋnaj </ts>
               <ts e="T177" id="Seg_1958" n="e" s="T176">ispet </ts>
               <ts e="T178" id="Seg_1960" n="e" s="T177">ete </ts>
               <ts e="T179" id="Seg_1962" n="e" s="T178">bu͡o </ts>
               <ts e="T180" id="Seg_1964" n="e" s="T179">du? </ts>
            </ts>
            <ts e="T203" id="Seg_1965" n="sc" s="T182">
               <ts e="T184" id="Seg_1967" n="e" s="T182">– Kelen </ts>
               <ts e="T186" id="Seg_1969" n="e" s="T184">baran </ts>
               <ts e="T188" id="Seg_1971" n="e" s="T186">ispet </ts>
               <ts e="T190" id="Seg_1973" n="e" s="T188">du </ts>
               <ts e="T192" id="Seg_1975" n="e" s="T190">manna </ts>
               <ts e="T194" id="Seg_1977" n="e" s="T192">uže </ts>
               <ts e="T196" id="Seg_1979" n="e" s="T194">ogoto </ts>
               <ts e="T198" id="Seg_1981" n="e" s="T196">bu </ts>
               <ts e="T200" id="Seg_1983" n="e" s="T198">ulaːppɨtɨn </ts>
               <ts e="T203" id="Seg_1985" n="e" s="T200">kenne. </ts>
            </ts>
            <ts e="T210" id="Seg_1986" n="sc" s="T204">
               <ts e="T207" id="Seg_1988" n="e" s="T204">– Itinne </ts>
               <ts e="T210" id="Seg_1990" n="e" s="T207">kɨra. </ts>
            </ts>
            <ts e="T213" id="Seg_1991" n="sc" s="T211">
               <ts e="T212" id="Seg_1993" n="e" s="T211">– Ogoto </ts>
               <ts e="T213" id="Seg_1995" n="e" s="T212">kɨra. </ts>
            </ts>
            <ts e="T218" id="Seg_1996" n="sc" s="T214">
               <ts e="T215" id="Seg_1998" n="e" s="T214">Kelen </ts>
               <ts e="T216" id="Seg_2000" n="e" s="T215">baran </ts>
               <ts e="T217" id="Seg_2002" n="e" s="T216">kepsiːr </ts>
               <ts e="T218" id="Seg_2004" n="e" s="T217">du? </ts>
            </ts>
            <ts e="T232" id="Seg_2005" n="sc" s="T219">
               <ts e="T221" id="Seg_2007" n="e" s="T219">Bu </ts>
               <ts e="T222" id="Seg_2009" n="e" s="T221">kelen </ts>
               <ts e="T224" id="Seg_2011" n="e" s="T222">baran </ts>
               <ts e="T226" id="Seg_2013" n="e" s="T224">kepsiːr </ts>
               <ts e="T227" id="Seg_2015" n="e" s="T226">eni </ts>
               <ts e="T229" id="Seg_2017" n="e" s="T227">kajdak </ts>
               <ts e="T231" id="Seg_2019" n="e" s="T229">(oloro-) </ts>
               <ts e="T232" id="Seg_2021" n="e" s="T231">olorbutun. </ts>
            </ts>
            <ts e="T236" id="Seg_2022" n="sc" s="T233">
               <ts e="T234" id="Seg_2024" n="e" s="T233">Ihilliː </ts>
               <ts e="T235" id="Seg_2026" n="e" s="T234">olorollor </ts>
               <ts e="T236" id="Seg_2028" n="e" s="T235">du? </ts>
            </ts>
            <ts e="T239" id="Seg_2029" n="sc" s="T237">
               <ts e="T239" id="Seg_2031" n="e" s="T237">Beːbe. </ts>
            </ts>
            <ts e="T256" id="Seg_2032" n="sc" s="T247">
               <ts e="T248" id="Seg_2034" n="e" s="T247">– Bu </ts>
               <ts e="T250" id="Seg_2036" n="e" s="T248">baːr, </ts>
               <ts e="T251" id="Seg_2038" n="e" s="T250">bu </ts>
               <ts e="T253" id="Seg_2040" n="e" s="T251">baːr, </ts>
               <ts e="T254" id="Seg_2042" n="e" s="T253">bu </ts>
               <ts e="T256" id="Seg_2044" n="e" s="T254">baːr. </ts>
            </ts>
            <ts e="T262" id="Seg_2045" n="sc" s="T258">
               <ts e="T259" id="Seg_2047" n="e" s="T258">Beːbe, </ts>
               <ts e="T260" id="Seg_2049" n="e" s="T259">maŋnajbɨtɨn </ts>
               <ts e="T261" id="Seg_2051" n="e" s="T260">bulu͡okputun </ts>
               <ts e="T262" id="Seg_2053" n="e" s="T261">naːda. </ts>
            </ts>
            <ts e="T276" id="Seg_2054" n="sc" s="T263">
               <ts e="T265" id="Seg_2056" n="e" s="T263">Bu </ts>
               <ts e="T266" id="Seg_2058" n="e" s="T265">ke </ts>
               <ts e="T267" id="Seg_2060" n="e" s="T266">kata </ts>
               <ts e="T268" id="Seg_2062" n="e" s="T267">kaːjɨːttan </ts>
               <ts e="T270" id="Seg_2064" n="e" s="T268">taksɨbɨt </ts>
               <ts e="T272" id="Seg_2066" n="e" s="T270">bɨhɨlaːk </ts>
               <ts e="T273" id="Seg_2068" n="e" s="T272">bu͡o, </ts>
               <ts e="T274" id="Seg_2070" n="e" s="T273">bu </ts>
               <ts e="T276" id="Seg_2072" n="e" s="T274">ü͡örer. </ts>
            </ts>
            <ts e="T285" id="Seg_2073" n="sc" s="T278">
               <ts e="T279" id="Seg_2075" n="e" s="T278">– Kaːjɨːga </ts>
               <ts e="T280" id="Seg_2077" n="e" s="T279">diː </ts>
               <ts e="T282" id="Seg_2079" n="e" s="T280">gini, </ts>
               <ts e="T284" id="Seg_2081" n="e" s="T282">kažetsa </ts>
               <ts e="T285" id="Seg_2083" n="e" s="T284">du. </ts>
            </ts>
            <ts e="T291" id="Seg_2084" n="sc" s="T286">
               <ts e="T289" id="Seg_2086" n="e" s="T286">– Üleliːr </ts>
               <ts e="T291" id="Seg_2088" n="e" s="T289">du? </ts>
            </ts>
            <ts e="T320" id="Seg_2089" n="sc" s="T312">
               <ts e="T313" id="Seg_2091" n="e" s="T312">– Bagarar </ts>
               <ts e="T314" id="Seg_2093" n="e" s="T313">ogoto </ts>
               <ts e="T315" id="Seg_2095" n="e" s="T314">iti </ts>
               <ts e="T316" id="Seg_2097" n="e" s="T315">kördük </ts>
               <ts e="T317" id="Seg_2099" n="e" s="T316">hüːre-hüːre </ts>
               <ts e="T318" id="Seg_2101" n="e" s="T317">keli͡ek </ts>
               <ts e="T319" id="Seg_2103" n="e" s="T318">ete </ts>
               <ts e="T320" id="Seg_2105" n="e" s="T319">gini͡eke. </ts>
            </ts>
            <ts e="T349" id="Seg_2106" n="sc" s="T326">
               <ts e="T328" id="Seg_2108" n="e" s="T326">– Manna </ts>
               <ts e="T329" id="Seg_2110" n="e" s="T328">navʼerna </ts>
               <ts e="T331" id="Seg_2112" n="e" s="T329">kör, </ts>
               <ts e="T332" id="Seg_2114" n="e" s="T331">bu </ts>
               <ts e="T333" id="Seg_2116" n="e" s="T332">aragiːlɨː </ts>
               <ts e="T335" id="Seg_2118" n="e" s="T333">oloron, </ts>
               <ts e="T336" id="Seg_2120" n="e" s="T335">bu </ts>
               <ts e="T338" id="Seg_2122" n="e" s="T336">aragiːlɨː </ts>
               <ts e="T339" id="Seg_2124" n="e" s="T338">oloronnor </ts>
               <ts e="T340" id="Seg_2126" n="e" s="T339">kepsete </ts>
               <ts e="T341" id="Seg_2128" n="e" s="T340">(ke-) </ts>
               <ts e="T342" id="Seg_2130" n="e" s="T341">bu </ts>
               <ts e="T343" id="Seg_2132" n="e" s="T342">kihi </ts>
               <ts e="T344" id="Seg_2134" n="e" s="T343">kepsiːr </ts>
               <ts e="T345" id="Seg_2136" n="e" s="T344">diː </ts>
               <ts e="T346" id="Seg_2138" n="e" s="T345">"dʼaktargɨn </ts>
               <ts e="T347" id="Seg_2140" n="e" s="T346">körbütüm" </ts>
               <ts e="T348" id="Seg_2142" n="e" s="T347">diːr </ts>
               <ts e="T349" id="Seg_2144" n="e" s="T348">bu͡o. </ts>
            </ts>
            <ts e="T353" id="Seg_2145" n="sc" s="T350">
               <ts e="T351" id="Seg_2147" n="e" s="T350">Kɨjŋammɨt </ts>
               <ts e="T352" id="Seg_2149" n="e" s="T351">uže </ts>
               <ts e="T353" id="Seg_2151" n="e" s="T352">bu͡o. </ts>
            </ts>
            <ts e="T373" id="Seg_2152" n="sc" s="T354">
               <ts e="T356" id="Seg_2154" n="e" s="T354">Onton </ts>
               <ts e="T358" id="Seg_2156" n="e" s="T356">kelen, </ts>
               <ts e="T360" id="Seg_2158" n="e" s="T358">dʼe </ts>
               <ts e="T362" id="Seg_2160" n="e" s="T360">onton </ts>
               <ts e="T364" id="Seg_2162" n="e" s="T362">kelen </ts>
               <ts e="T366" id="Seg_2164" n="e" s="T364">baran </ts>
               <ts e="T367" id="Seg_2166" n="e" s="T366">atnašenʼije </ts>
               <ts e="T368" id="Seg_2168" n="e" s="T367">vɨjasnʼajdɨːr </ts>
               <ts e="T369" id="Seg_2170" n="e" s="T368">bu͡o. </ts>
               <ts e="T370" id="Seg_2172" n="e" s="T369">Kɨrbaːrɨ </ts>
               <ts e="T371" id="Seg_2174" n="e" s="T370">gɨnar, </ts>
               <ts e="T372" id="Seg_2176" n="e" s="T371">ol </ts>
               <ts e="T373" id="Seg_2178" n="e" s="T372">baːr. </ts>
            </ts>
            <ts e="T381" id="Seg_2179" n="sc" s="T375">
               <ts e="T376" id="Seg_2181" n="e" s="T375">– Onton </ts>
               <ts e="T377" id="Seg_2183" n="e" s="T376">bu </ts>
               <ts e="T378" id="Seg_2185" n="e" s="T377">uže </ts>
               <ts e="T379" id="Seg_2187" n="e" s="T378">kɨrbaːbɨt, </ts>
               <ts e="T380" id="Seg_2189" n="e" s="T379">oksubut </ts>
               <ts e="T381" id="Seg_2191" n="e" s="T380">uže. </ts>
            </ts>
            <ts e="T390" id="Seg_2192" n="sc" s="T385">
               <ts e="T386" id="Seg_2194" n="e" s="T385">– Bu </ts>
               <ts e="T387" id="Seg_2196" n="e" s="T386">kördük </ts>
               <ts e="T389" id="Seg_2198" n="e" s="T387">možet, </ts>
               <ts e="T390" id="Seg_2200" n="e" s="T389">da? </ts>
            </ts>
            <ts e="T395" id="Seg_2201" n="sc" s="T391">
               <ts e="T393" id="Seg_2203" n="e" s="T391">– Egel </ts>
               <ts e="T395" id="Seg_2205" n="e" s="T393">bettek. </ts>
            </ts>
            <ts e="T401" id="Seg_2206" n="sc" s="T397">
               <ts e="T398" id="Seg_2208" n="e" s="T397">– Egel </ts>
               <ts e="T399" id="Seg_2210" n="e" s="T398">biːr </ts>
               <ts e="T401" id="Seg_2212" n="e" s="T399">((…)). </ts>
            </ts>
            <ts e="T411" id="Seg_2213" n="sc" s="T402">
               <ts e="T404" id="Seg_2215" n="e" s="T402">– Bu </ts>
               <ts e="T405" id="Seg_2217" n="e" s="T404">baːr, </ts>
               <ts e="T407" id="Seg_2219" n="e" s="T405">maŋnaj </ts>
               <ts e="T408" id="Seg_2221" n="e" s="T407">bu </ts>
               <ts e="T409" id="Seg_2223" n="e" s="T408">baːr </ts>
               <ts e="T411" id="Seg_2225" n="e" s="T409">ke. </ts>
            </ts>
            <ts e="T447" id="Seg_2226" n="sc" s="T427">
               <ts e="T429" id="Seg_2228" n="e" s="T427">– Hu͡ok, </ts>
               <ts e="T430" id="Seg_2230" n="e" s="T429">bu </ts>
               <ts e="T431" id="Seg_2232" n="e" s="T430">uže </ts>
               <ts e="T432" id="Seg_2234" n="e" s="T431">kanʼɨhɨgar </ts>
               <ts e="T433" id="Seg_2236" n="e" s="T432">"ispeppin" </ts>
               <ts e="T434" id="Seg_2238" n="e" s="T433">diːr </ts>
               <ts e="T435" id="Seg_2240" n="e" s="T434">uže, </ts>
               <ts e="T436" id="Seg_2242" n="e" s="T435">ikki </ts>
               <ts e="T437" id="Seg_2244" n="e" s="T436">paslʼednʼij </ts>
               <ts e="T438" id="Seg_2246" n="e" s="T437">kelen </ts>
               <ts e="T439" id="Seg_2248" n="e" s="T438">barar, </ts>
               <ts e="T441" id="Seg_2250" n="e" s="T439">kaːjɨːttan </ts>
               <ts e="T442" id="Seg_2252" n="e" s="T441">kelen </ts>
               <ts e="T443" id="Seg_2254" n="e" s="T442">baran, </ts>
               <ts e="T445" id="Seg_2256" n="e" s="T443">ja </ts>
               <ts e="T446" id="Seg_2258" n="e" s="T445">tak </ts>
               <ts e="T447" id="Seg_2260" n="e" s="T446">dumaju. </ts>
            </ts>
            <ts e="T449" id="Seg_2261" n="sc" s="T448">
               <ts e="T449" id="Seg_2263" n="e" s="T448">Eː? </ts>
            </ts>
            <ts e="T454" id="Seg_2264" n="sc" s="T450">
               <ts e="T453" id="Seg_2266" n="e" s="T450">– ((…)) </ts>
               <ts e="T454" id="Seg_2268" n="e" s="T453">ispet. </ts>
            </ts>
            <ts e="T478" id="Seg_2269" n="sc" s="T461">
               <ts e="T464" id="Seg_2271" n="e" s="T461">– Üčügej </ts>
               <ts e="T466" id="Seg_2273" n="e" s="T464">bagajɨ </ts>
               <ts e="T468" id="Seg_2275" n="e" s="T466">oloror </ts>
               <ts e="T469" id="Seg_2277" n="e" s="T468">etiler, </ts>
               <ts e="T471" id="Seg_2279" n="e" s="T469">oloror </ts>
               <ts e="T473" id="Seg_2281" n="e" s="T471">etiler </ts>
               <ts e="T475" id="Seg_2283" n="e" s="T473">eni </ts>
               <ts e="T477" id="Seg_2285" n="e" s="T475">üleliː </ts>
               <ts e="T478" id="Seg_2287" n="e" s="T477">hɨldʼallar. </ts>
            </ts>
            <ts e="T506" id="Seg_2288" n="sc" s="T484">
               <ts e="T485" id="Seg_2290" n="e" s="T484">– Če </ts>
               <ts e="T486" id="Seg_2292" n="e" s="T485">iti </ts>
               <ts e="T487" id="Seg_2294" n="e" s="T486">uže </ts>
               <ts e="T488" id="Seg_2296" n="e" s="T487">kaːjɨːtɨn </ts>
               <ts e="T489" id="Seg_2298" n="e" s="T488">kepsiːr </ts>
               <ts e="T491" id="Seg_2300" n="e" s="T489">diː </ts>
               <ts e="T494" id="Seg_2302" n="e" s="T491">gini, </ts>
               <ts e="T496" id="Seg_2304" n="e" s="T494">kojukkaːn </ts>
               <ts e="T498" id="Seg_2306" n="e" s="T496">eni, </ts>
               <ts e="T499" id="Seg_2308" n="e" s="T498">bu </ts>
               <ts e="T501" id="Seg_2310" n="e" s="T499">bu </ts>
               <ts e="T502" id="Seg_2312" n="e" s="T501">kennitten, </ts>
               <ts e="T503" id="Seg_2314" n="e" s="T502">bu </ts>
               <ts e="T505" id="Seg_2316" n="e" s="T503">kördük </ts>
               <ts e="T506" id="Seg_2318" n="e" s="T505">bu͡olu͡oga. </ts>
            </ts>
            <ts e="T516" id="Seg_2319" n="sc" s="T507">
               <ts e="T508" id="Seg_2321" n="e" s="T507">Vot </ts>
               <ts e="T509" id="Seg_2323" n="e" s="T508">tak </ts>
               <ts e="T510" id="Seg_2325" n="e" s="T509">vot, </ts>
               <ts e="T511" id="Seg_2327" n="e" s="T510">i͡e, </ts>
               <ts e="T512" id="Seg_2329" n="e" s="T511">dapustiʼm, </ts>
               <ts e="T513" id="Seg_2331" n="e" s="T512">pustʼ </ts>
               <ts e="T514" id="Seg_2333" n="e" s="T513">budʼet </ts>
               <ts e="T515" id="Seg_2335" n="e" s="T514">iti </ts>
               <ts e="T516" id="Seg_2337" n="e" s="T515">kördük. </ts>
            </ts>
            <ts e="T529" id="Seg_2338" n="sc" s="T517">
               <ts e="T518" id="Seg_2340" n="e" s="T517">A </ts>
               <ts e="T519" id="Seg_2342" n="e" s="T518">tut </ts>
               <ts e="T520" id="Seg_2344" n="e" s="T519">uvʼidʼela </ts>
               <ts e="T521" id="Seg_2346" n="e" s="T520">ja </ts>
               <ts e="T522" id="Seg_2348" n="e" s="T521">etʼi </ts>
               <ts e="T523" id="Seg_2350" n="e" s="T522">sʼifrɨ </ts>
               <ts e="T524" id="Seg_2352" n="e" s="T523">prastavlenɨ, </ts>
               <ts e="T525" id="Seg_2354" n="e" s="T524">eta </ts>
               <ts e="T526" id="Seg_2356" n="e" s="T525">v </ts>
               <ts e="T527" id="Seg_2358" n="e" s="T526">pravʼilʼnam </ts>
               <ts e="T528" id="Seg_2360" n="e" s="T527">parʼadke </ts>
               <ts e="T529" id="Seg_2362" n="e" s="T528">prastavlʼena? </ts>
            </ts>
            <ts e="T537" id="Seg_2363" n="sc" s="T531">
               <ts e="T533" id="Seg_2365" n="e" s="T531">– Nʼe </ts>
               <ts e="T534" id="Seg_2367" n="e" s="T533">nada </ts>
               <ts e="T535" id="Seg_2369" n="e" s="T534">na </ts>
               <ts e="T536" id="Seg_2371" n="e" s="T535">nʼix </ts>
               <ts e="T537" id="Seg_2373" n="e" s="T536">smatrʼetʼ. </ts>
            </ts>
            <ts e="T542" id="Seg_2374" n="sc" s="T538">
               <ts e="T539" id="Seg_2376" n="e" s="T538">Ja </ts>
               <ts e="T540" id="Seg_2378" n="e" s="T539">xʼitraja, </ts>
               <ts e="T541" id="Seg_2380" n="e" s="T540">to </ts>
               <ts e="T542" id="Seg_2382" n="e" s="T541">uvižu. </ts>
            </ts>
            <ts e="T559" id="Seg_2383" n="sc" s="T543">
               <ts e="T544" id="Seg_2385" n="e" s="T543">Dʼe </ts>
               <ts e="T545" id="Seg_2387" n="e" s="T544">bu </ts>
               <ts e="T546" id="Seg_2389" n="e" s="T545">baːllar </ts>
               <ts e="T547" id="Seg_2391" n="e" s="T546">diːr </ts>
               <ts e="T548" id="Seg_2393" n="e" s="T547">maŋnaj, </ts>
               <ts e="T549" id="Seg_2395" n="e" s="T548">üleliː </ts>
               <ts e="T550" id="Seg_2397" n="e" s="T549">hɨldʼan </ts>
               <ts e="T551" id="Seg_2399" n="e" s="T550">bu͡o </ts>
               <ts e="T552" id="Seg_2401" n="e" s="T551">bu </ts>
               <ts e="T553" id="Seg_2403" n="e" s="T552">aragiːmsaktara </ts>
               <ts e="T554" id="Seg_2405" n="e" s="T553">kimneːbit. </ts>
               <ts e="T559" id="Seg_2407" n="e" s="T554">Tübespit. </ts>
            </ts>
            <ts e="T568" id="Seg_2408" n="sc" s="T564">
               <ts e="T565" id="Seg_2410" n="e" s="T564">– Dʼe </ts>
               <ts e="T566" id="Seg_2412" n="e" s="T565">bu </ts>
               <ts e="T567" id="Seg_2414" n="e" s="T566">baːllar </ts>
               <ts e="T568" id="Seg_2416" n="e" s="T567">diː. </ts>
            </ts>
            <ts e="T576" id="Seg_2417" n="sc" s="T570">
               <ts e="T572" id="Seg_2419" n="e" s="T570">– Bu </ts>
               <ts e="T574" id="Seg_2421" n="e" s="T572">iher </ts>
               <ts e="T576" id="Seg_2423" n="e" s="T574">olorollor. </ts>
            </ts>
            <ts e="T584" id="Seg_2424" n="sc" s="T577">
               <ts e="T578" id="Seg_2426" n="e" s="T577">Ihe </ts>
               <ts e="T579" id="Seg_2428" n="e" s="T578">oloror, </ts>
               <ts e="T580" id="Seg_2430" n="e" s="T579">onton </ts>
               <ts e="T581" id="Seg_2432" n="e" s="T580">bu </ts>
               <ts e="T582" id="Seg_2434" n="e" s="T581">kepsiːller, </ts>
               <ts e="T583" id="Seg_2436" n="e" s="T582">gini </ts>
               <ts e="T584" id="Seg_2438" n="e" s="T583">kɨjŋanar. </ts>
            </ts>
            <ts e="T593" id="Seg_2439" n="sc" s="T585">
               <ts e="T586" id="Seg_2441" n="e" s="T585">Kɨjŋanan </ts>
               <ts e="T587" id="Seg_2443" n="e" s="T586">baran </ts>
               <ts e="T588" id="Seg_2445" n="e" s="T587">dʼaktarga </ts>
               <ts e="T589" id="Seg_2447" n="e" s="T588">barar, </ts>
               <ts e="T590" id="Seg_2449" n="e" s="T589">ol </ts>
               <ts e="T591" id="Seg_2451" n="e" s="T590">baːr, </ts>
               <ts e="T592" id="Seg_2453" n="e" s="T591">egel </ts>
               <ts e="T593" id="Seg_2455" n="e" s="T592">onu. </ts>
            </ts>
            <ts e="T608" id="Seg_2456" n="sc" s="T601">
               <ts e="T604" id="Seg_2458" n="e" s="T601">– Kulaːkɨtɨn </ts>
               <ts e="T608" id="Seg_2460" n="e" s="T604">belemnemmit. </ts>
            </ts>
            <ts e="T618" id="Seg_2461" n="sc" s="T610">
               <ts e="T612" id="Seg_2463" n="e" s="T610">– Da, </ts>
               <ts e="T613" id="Seg_2465" n="e" s="T612">da, </ts>
               <ts e="T615" id="Seg_2467" n="e" s="T613">da, </ts>
               <ts e="T616" id="Seg_2469" n="e" s="T615">da, </ts>
               <ts e="T618" id="Seg_2471" n="e" s="T616">da. </ts>
            </ts>
            <ts e="T640" id="Seg_2472" n="sc" s="T621">
               <ts e="T623" id="Seg_2474" n="e" s="T621">– A </ts>
               <ts e="T624" id="Seg_2476" n="e" s="T623">tam </ts>
               <ts e="T626" id="Seg_2478" n="e" s="T624">üčügej, </ts>
               <ts e="T627" id="Seg_2480" n="e" s="T626">bu </ts>
               <ts e="T629" id="Seg_2482" n="e" s="T627">baːr, </ts>
               <ts e="T630" id="Seg_2484" n="e" s="T629">üleliː </ts>
               <ts e="T631" id="Seg_2486" n="e" s="T630">hɨldʼallar, </ts>
               <ts e="T633" id="Seg_2488" n="e" s="T631">oloror </ts>
               <ts e="T634" id="Seg_2490" n="e" s="T633">etilere </ts>
               <ts e="T636" id="Seg_2492" n="e" s="T634">eni, </ts>
               <ts e="T637" id="Seg_2494" n="e" s="T636">kör, </ts>
               <ts e="T638" id="Seg_2496" n="e" s="T637">bu </ts>
               <ts e="T640" id="Seg_2498" n="e" s="T638">vmʼestʼe. </ts>
            </ts>
            <ts e="T651" id="Seg_2499" n="sc" s="T647">
               <ts e="T649" id="Seg_2501" n="e" s="T647">– Bul, </ts>
               <ts e="T651" id="Seg_2503" n="e" s="T649">beːbe. </ts>
            </ts>
            <ts e="T667" id="Seg_2504" n="sc" s="T654">
               <ts e="T656" id="Seg_2506" n="e" s="T654">– Bu </ts>
               <ts e="T658" id="Seg_2508" n="e" s="T656">baːr, </ts>
               <ts e="T660" id="Seg_2510" n="e" s="T658">bu </ts>
               <ts e="T662" id="Seg_2512" n="e" s="T660">baːr, </ts>
               <ts e="T664" id="Seg_2514" n="e" s="T662">munu </ts>
               <ts e="T665" id="Seg_2516" n="e" s="T664">munu </ts>
               <ts e="T667" id="Seg_2518" n="e" s="T665">daprašɨvajdɨːllar. </ts>
            </ts>
            <ts e="T672" id="Seg_2519" n="sc" s="T669">
               <ts e="T670" id="Seg_2521" n="e" s="T669">– Bu </ts>
               <ts e="T671" id="Seg_2523" n="e" s="T670">baːllar, </ts>
               <ts e="T672" id="Seg_2525" n="e" s="T671">daprašɨvajdɨːllar. </ts>
            </ts>
            <ts e="T677" id="Seg_2526" n="sc" s="T673">
               <ts e="T676" id="Seg_2528" n="e" s="T673">– Taŋas </ts>
               <ts e="T677" id="Seg_2530" n="e" s="T676">bi͡ereller. </ts>
            </ts>
            <ts e="T682" id="Seg_2531" n="sc" s="T678">
               <ts e="T679" id="Seg_2533" n="e" s="T678">Kaːjɨllɨbɨt </ts>
               <ts e="T680" id="Seg_2535" n="e" s="T679">oloror, </ts>
               <ts e="T681" id="Seg_2537" n="e" s="T680">hanaːga </ts>
               <ts e="T682" id="Seg_2539" n="e" s="T681">battappɨt. </ts>
            </ts>
            <ts e="T688" id="Seg_2540" n="sc" s="T683">
               <ts e="T684" id="Seg_2542" n="e" s="T683">Bu </ts>
               <ts e="T685" id="Seg_2544" n="e" s="T684">emi͡e </ts>
               <ts e="T686" id="Seg_2546" n="e" s="T685">hanaːga </ts>
               <ts e="T687" id="Seg_2548" n="e" s="T686">battappɨt </ts>
               <ts e="T688" id="Seg_2550" n="e" s="T687">oloror. </ts>
            </ts>
            <ts e="T697" id="Seg_2551" n="sc" s="T689">
               <ts e="T690" id="Seg_2553" n="e" s="T689">Bu </ts>
               <ts e="T691" id="Seg_2555" n="e" s="T690">bu͡o </ts>
               <ts e="T692" id="Seg_2557" n="e" s="T691">uže </ts>
               <ts e="T693" id="Seg_2559" n="e" s="T692">mʼečʼtajdɨː </ts>
               <ts e="T694" id="Seg_2561" n="e" s="T693">oloror, </ts>
               <ts e="T695" id="Seg_2563" n="e" s="T694">navʼernaje, </ts>
               <ts e="T696" id="Seg_2565" n="e" s="T695">ogoto </ts>
               <ts e="T697" id="Seg_2567" n="e" s="T696">fstrʼečʼajdɨːrɨn. </ts>
            </ts>
            <ts e="T705" id="Seg_2568" n="sc" s="T702">
               <ts e="T704" id="Seg_2570" n="e" s="T702">– Iti </ts>
               <ts e="T705" id="Seg_2572" n="e" s="T704">kördük. </ts>
            </ts>
            <ts e="T717" id="Seg_2573" n="sc" s="T708">
               <ts e="T709" id="Seg_2575" n="e" s="T708">– Da, </ts>
               <ts e="T711" id="Seg_2577" n="e" s="T709">uže </ts>
               <ts e="T713" id="Seg_2579" n="e" s="T711">prʼedstavlʼajdɨːr </ts>
               <ts e="T714" id="Seg_2581" n="e" s="T713">dʼi͡etiger </ts>
               <ts e="T715" id="Seg_2583" n="e" s="T714">kelerin, </ts>
               <ts e="T717" id="Seg_2585" n="e" s="T715">navʼerna. </ts>
            </ts>
            <ts e="T729" id="Seg_2586" n="sc" s="T723">
               <ts e="T725" id="Seg_2588" n="e" s="T723">– Onton </ts>
               <ts e="T727" id="Seg_2590" n="e" s="T725">ol </ts>
               <ts e="T729" id="Seg_2592" n="e" s="T727">taksɨbɨt. </ts>
            </ts>
            <ts e="T735" id="Seg_2593" n="sc" s="T734">
               <ts e="T735" id="Seg_2595" n="e" s="T734">– Taksɨbɨt. </ts>
            </ts>
            <ts e="T743" id="Seg_2596" n="sc" s="T739">
               <ts e="T740" id="Seg_2598" n="e" s="T739">– Taksɨbɨt, </ts>
               <ts e="T741" id="Seg_2600" n="e" s="T740">kelbit, </ts>
               <ts e="T742" id="Seg_2602" n="e" s="T741">ü͡öre-ü͡öre </ts>
               <ts e="T743" id="Seg_2604" n="e" s="T742">radastnɨj. </ts>
            </ts>
            <ts e="T747" id="Seg_2605" n="sc" s="T744">
               <ts e="T745" id="Seg_2607" n="e" s="T744">Onton </ts>
               <ts e="T746" id="Seg_2609" n="e" s="T745">kepsiː </ts>
               <ts e="T747" id="Seg_2611" n="e" s="T746">oloror. </ts>
            </ts>
            <ts e="T758" id="Seg_2612" n="sc" s="T748">
               <ts e="T749" id="Seg_2614" n="e" s="T748">Onton </ts>
               <ts e="T752" id="Seg_2616" n="e" s="T749">((…)) </ts>
               <ts e="T756" id="Seg_2618" n="e" s="T752">ispet </ts>
               <ts e="T758" id="Seg_2620" n="e" s="T756">bu͡olbut. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-ChGS">
            <ta e="T9" id="Seg_2621" s="T6">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.001 (001.002)</ta>
            <ta e="T30" id="Seg_2622" s="T25">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.002 (001.005)</ta>
            <ta e="T58" id="Seg_2623" s="T51">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.003 (001.009)</ta>
            <ta e="T65" id="Seg_2624" s="T61">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.004 (001.011)</ta>
            <ta e="T76" id="Seg_2625" s="T67">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.005 (001.013)</ta>
            <ta e="T78" id="Seg_2626" s="T76">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.006 (001.014)</ta>
            <ta e="T88" id="Seg_2627" s="T82">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.007 (001.016)</ta>
            <ta e="T99" id="Seg_2628" s="T92">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.008 (001.018)</ta>
            <ta e="T108" id="Seg_2629" s="T105">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.009 (001.021)</ta>
            <ta e="T127" id="Seg_2630" s="T109">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.010 (001.022)</ta>
            <ta e="T137" id="Seg_2631" s="T133">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.011 (001.025)</ta>
            <ta e="T163" id="Seg_2632" s="T153">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.012 (001.029)</ta>
            <ta e="T169" id="Seg_2633" s="T166">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.013 (001.031)</ta>
            <ta e="T180" id="Seg_2634" s="T175">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.014 (001.034)</ta>
            <ta e="T203" id="Seg_2635" s="T182">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.015 (001.036)</ta>
            <ta e="T210" id="Seg_2636" s="T204">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.016 (001.038)</ta>
            <ta e="T213" id="Seg_2637" s="T211">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.017 (001.040)</ta>
            <ta e="T218" id="Seg_2638" s="T214">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.018 (001.041)</ta>
            <ta e="T232" id="Seg_2639" s="T219">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.019 (001.042)</ta>
            <ta e="T236" id="Seg_2640" s="T233">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.020 (001.044)</ta>
            <ta e="T239" id="Seg_2641" s="T237">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.021 (001.045)</ta>
            <ta e="T256" id="Seg_2642" s="T247">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.022 (001.048)</ta>
            <ta e="T262" id="Seg_2643" s="T258">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.023 (001.049)</ta>
            <ta e="T276" id="Seg_2644" s="T263">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.024 (001.050)</ta>
            <ta e="T285" id="Seg_2645" s="T278">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.025 (001.053)</ta>
            <ta e="T291" id="Seg_2646" s="T286">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.026 (001.055)</ta>
            <ta e="T320" id="Seg_2647" s="T312">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.027 (001.059)</ta>
            <ta e="T349" id="Seg_2648" s="T326">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.028 (001.061)</ta>
            <ta e="T353" id="Seg_2649" s="T350">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.029 (001.063)</ta>
            <ta e="T369" id="Seg_2650" s="T354">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.030 (001.064)</ta>
            <ta e="T373" id="Seg_2651" s="T369">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.031 (001.066)</ta>
            <ta e="T381" id="Seg_2652" s="T375">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.032 (001.068)</ta>
            <ta e="T390" id="Seg_2653" s="T385">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.033 (001.071)</ta>
            <ta e="T395" id="Seg_2654" s="T391">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.034 (001.072)</ta>
            <ta e="T401" id="Seg_2655" s="T397">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.035 (001.074)</ta>
            <ta e="T411" id="Seg_2656" s="T402">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.036 (001.076)</ta>
            <ta e="T447" id="Seg_2657" s="T427">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.037 (001.079)</ta>
            <ta e="T449" id="Seg_2658" s="T448">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.038 (001.081)</ta>
            <ta e="T454" id="Seg_2659" s="T450">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.039 (001.082)</ta>
            <ta e="T478" id="Seg_2660" s="T461">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.040 (001.085)</ta>
            <ta e="T506" id="Seg_2661" s="T484">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.041 (001.088)</ta>
            <ta e="T516" id="Seg_2662" s="T507">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.042 (001.091)</ta>
            <ta e="T529" id="Seg_2663" s="T517">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.043 (001.092)</ta>
            <ta e="T537" id="Seg_2664" s="T531">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.044 (001.094)</ta>
            <ta e="T542" id="Seg_2665" s="T538">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.045 (001.095)</ta>
            <ta e="T554" id="Seg_2666" s="T543">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.046 (001.096)</ta>
            <ta e="T559" id="Seg_2667" s="T554">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.047 (001.097)</ta>
            <ta e="T568" id="Seg_2668" s="T564">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.048 (001.099)</ta>
            <ta e="T576" id="Seg_2669" s="T570">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.049 (001.101)</ta>
            <ta e="T584" id="Seg_2670" s="T577">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.050 (001.102)</ta>
            <ta e="T593" id="Seg_2671" s="T585">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.051 (001.103)</ta>
            <ta e="T608" id="Seg_2672" s="T601">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.052 (001.105)</ta>
            <ta e="T618" id="Seg_2673" s="T610">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.053 (001.107)</ta>
            <ta e="T640" id="Seg_2674" s="T621">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.054 (001.109)</ta>
            <ta e="T651" id="Seg_2675" s="T647">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.055 (001.114)</ta>
            <ta e="T667" id="Seg_2676" s="T654">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.056 (001.116)</ta>
            <ta e="T672" id="Seg_2677" s="T669">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.057 (001.118)</ta>
            <ta e="T677" id="Seg_2678" s="T673">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.058 (001.119)</ta>
            <ta e="T682" id="Seg_2679" s="T678">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.059 (001.121)</ta>
            <ta e="T688" id="Seg_2680" s="T683">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.060 (001.122)</ta>
            <ta e="T697" id="Seg_2681" s="T689">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.061 (001.123)</ta>
            <ta e="T705" id="Seg_2682" s="T702">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.062 (001.125)</ta>
            <ta e="T717" id="Seg_2683" s="T708">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.063 (001.127)</ta>
            <ta e="T729" id="Seg_2684" s="T723">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.064 (001.131)</ta>
            <ta e="T735" id="Seg_2685" s="T734">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.065 (001.135)</ta>
            <ta e="T743" id="Seg_2686" s="T739">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.066 (001.137)</ta>
            <ta e="T747" id="Seg_2687" s="T744">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.067 (001.138)</ta>
            <ta e="T758" id="Seg_2688" s="T748">ChGS_UoPP_20170724_SocCogOrder_conv.ChGS.068 (001.139)</ta>
         </annotation>
         <annotation name="st" tierref="st-ChGS">
            <ta e="T9" id="Seg_2689" s="T6">Хорошо.</ta>
            <ta e="T30" id="Seg_2690" s="T25">Кажется ((…)).</ta>
            <ta e="T58" id="Seg_2691" s="T51">Последовательно.</ta>
            <ta e="T65" id="Seg_2692" s="T61">Каннаный маӈнайгы?</ta>
            <ta e="T76" id="Seg_2693" s="T67">Бу наверно конец дии буо, мне кажется.</ta>
            <ta e="T78" id="Seg_2694" s="T76">Куһаган(?) кэлбит.</ta>
            <ta e="T88" id="Seg_2695" s="T82">Ити эмиэ онно олорор.</ta>
            <ta e="T99" id="Seg_2696" s="T92">Манна наверно барыта.</ta>
            <ta e="T108" id="Seg_2697" s="T105">Һэ-э.</ta>
            <ta e="T127" id="Seg_2698" s="T109">Бээбэ, ити турар түстүн көрүөк булары уратала дьэ барытын арагилаабыттарын каньаабытарын.</ta>
            <ta e="T137" id="Seg_2699" s="T133">Бу иттэ баар дии.</ta>
            <ta e="T163" id="Seg_2700" s="T153">Онтон кэлэн бу карчытын барытын араги гыммыт.</ta>
            <ta e="T169" id="Seg_2701" s="T166">Диигин дуо?</ta>
            <ta e="T180" id="Seg_2702" s="T175">Маӈнай испет этэ эбит буо ду.</ta>
            <ta e="T203" id="Seg_2703" s="T182">Кэлэн баран испэт ду манна уже огото улааппытын кэннэ.</ta>
            <ta e="T210" id="Seg_2704" s="T204">Итиннэ кыра.</ta>
            <ta e="T213" id="Seg_2705" s="T211">Огото кыра.</ta>
            <ta e="T218" id="Seg_2706" s="T214">Кэлэн баран кэпсиир дуо?</ta>
            <ta e="T232" id="Seg_2707" s="T219">Бу кэлэн баран кепсиир эни кайтак олорбутун.</ta>
            <ta e="T236" id="Seg_2708" s="T233">Иһиллии олорор ду? ((…))</ta>
            <ta e="T239" id="Seg_2709" s="T237">Бээбэ.</ta>
            <ta e="T256" id="Seg_2710" s="T247">Бу баар, бу баар, бу баар.</ta>
            <ta e="T262" id="Seg_2711" s="T258">Бээбэ, маӈнайбытын булуокпутун наада.</ta>
            <ta e="T276" id="Seg_2712" s="T263">Бу кэ ката кайыттан таксыбыт быһылаак буо, бу үөрэр.</ta>
            <ta e="T285" id="Seg_2713" s="T278">Кайыыга дии гини, кажется ду.</ta>
            <ta e="T291" id="Seg_2714" s="T286">Үлэлиир ду?</ta>
            <ta e="T320" id="Seg_2715" s="T312">Багарар огото итигирдик һүүрэ-һүүрэ кэлиэк этэ гиниэкэ.</ta>
            <ta e="T349" id="Seg_2716" s="T326">Манна наверно көр бу арагилыы олороннор, бу арагилыы олороннор кэпсэтэ (кэ-) бу киһи кэпсиир дии дьактаргын көрбүтүм диир буо.</ta>
            <ta e="T353" id="Seg_2717" s="T350">Кыйӈаммыт уже бу.</ta>
            <ta e="T369" id="Seg_2718" s="T354">Онтон кэлэн дьэ онтон кэлэн баран отношение выясняйдыыр буо.</ta>
            <ta e="T373" id="Seg_2719" s="T369">Кырбаары гынар, ол ол баар.</ta>
            <ta e="T381" id="Seg_2720" s="T375">Онтон бу уже кырбаабыт оксубут буо.</ta>
            <ta e="T390" id="Seg_2721" s="T385">Бу көрдүк может, да?</ta>
            <ta e="T395" id="Seg_2722" s="T391">Эгэл бэттэк.</ta>
            <ta e="T401" id="Seg_2723" s="T397">Эгэл биир ((…))</ta>
            <ta e="T411" id="Seg_2724" s="T402">Бу баар, маӈнай бу баар ((…)).</ta>
            <ta e="T447" id="Seg_2725" s="T427">Һуок, бу уже каньыһыгар "испэппин" диир уже икки последний кайыыттан кэлэн баран, я так думаю.</ta>
            <ta e="T449" id="Seg_2726" s="T448">Ээ?</ta>
            <ta e="T454" id="Seg_2727" s="T450">((…)) испэт.</ta>
            <ta e="T478" id="Seg_2728" s="T461">Үчүгэй багайы олорор этилэр, олорор этилэр эни үлэлии һылдьаллар.</ta>
            <ta e="T506" id="Seg_2729" s="T484">Че ити уже кайыытын кэпсиир дии гини, койуккаан эни, бу бу кэнниттэн, бу көрдүк буолуок этэ.</ta>
            <ta e="T516" id="Seg_2730" s="T507">Вот так вот, иэ, допустим, пусть будет ити көрдүк.</ta>
            <ta e="T529" id="Seg_2731" s="T517">А тут увидела я эти цифры праставлены, это в правильном порядке праставлено?</ta>
            <ta e="T537" id="Seg_2732" s="T531">Не надо на них смотреть.</ta>
            <ta e="T542" id="Seg_2733" s="T538">Я хитрая-то увижу.</ta>
            <ta e="T554" id="Seg_2734" s="T543">Дье бу бааллар дии маӈнай үлэлии һылдьан бу арагимсактарга кимнээбит.</ta>
            <ta e="T559" id="Seg_2735" s="T554">Түбэспит.</ta>
            <ta e="T568" id="Seg_2736" s="T564">Дьэ бу бааллар дии.</ta>
            <ta e="T576" id="Seg_2737" s="T570">Бу иһэр олороллор уже.</ta>
            <ta e="T584" id="Seg_2738" s="T577">Иһэ олорор онтон бу кэпсииллэр гини кыйӈанар.</ta>
            <ta e="T593" id="Seg_2739" s="T585">Кыйӈанан баран дьактарга барар, ол баар, эгэл ону.</ta>
            <ta e="T608" id="Seg_2740" s="T601">Кулаакытын бэлэмнэммит ((…)).</ta>
            <ta e="T618" id="Seg_2741" s="T610">Да, да, да, да, да.</ta>
            <ta e="T640" id="Seg_2742" s="T621">А там үчүгэй, бу баар, үлэлии һылдьаллар, олорор этилэрэ эни көр бу вместе.</ta>
            <ta e="T651" id="Seg_2743" s="T647">Бул, бээбе.</ta>
            <ta e="T667" id="Seg_2744" s="T654">Бу баар, бу баар, муну муну допрашивайдыыллар.</ta>
            <ta e="T672" id="Seg_2745" s="T669">Бу бааллар, допрашивайдыыллар.</ta>
            <ta e="T677" id="Seg_2746" s="T673">Таӈас биэрэллэр.</ta>
            <ta e="T682" id="Seg_2747" s="T678">Кайыллыбыт олорор һанаага батаппыт.</ta>
            <ta e="T688" id="Seg_2748" s="T683">Бу эмиэ һанаага батаппыт олорор.</ta>
            <ta e="T697" id="Seg_2749" s="T689">Бу буо уже мечтайдыы олорор наверное огото встречайдыырын.</ta>
            <ta e="T705" id="Seg_2750" s="T702">Ити көрдүк.</ta>
            <ta e="T717" id="Seg_2751" s="T708">Да, уже представляйдыыр дьиэтигэр кэлэрин, наверно.</ta>
            <ta e="T729" id="Seg_2752" s="T723">Онтон ол таксыбыт.</ta>
            <ta e="T735" id="Seg_2753" s="T734">Таксыбыт.</ta>
            <ta e="T743" id="Seg_2754" s="T739">Таксыбыт, кэлбит, үөрэ-үөрэ радостный.</ta>
            <ta e="T747" id="Seg_2755" s="T744">Онтон кэпсии олорор.</ta>
            <ta e="T758" id="Seg_2756" s="T748">Онтон ((…)) испэт буолбут.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-ChGS">
            <ta e="T9" id="Seg_2757" s="T6">– Xarašo. </ta>
            <ta e="T30" id="Seg_2758" s="T25">– Kažetsa ((…)). </ta>
            <ta e="T58" id="Seg_2759" s="T51">– Paslʼedavatʼelʼna. </ta>
            <ta e="T65" id="Seg_2760" s="T61">– Kannanɨj maŋnaj. </ta>
            <ta e="T76" id="Seg_2761" s="T67">– ((…)) kanʼec diː bu͡o, mnʼe kažetsa. </ta>
            <ta e="T78" id="Seg_2762" s="T76">((…)) kelbit. </ta>
            <ta e="T88" id="Seg_2763" s="T82">– Iti emi͡e onno oloror. </ta>
            <ta e="T99" id="Seg_2764" s="T92">– Manna navʼerna barɨta. </ta>
            <ta e="T108" id="Seg_2765" s="T105">– Heː. </ta>
            <ta e="T127" id="Seg_2766" s="T109">– Beːbe, iti turar tüstün körü͡ök bularɨ, uːratalaː dʼe barɨtɨn aragiːlaːbɨttarɨn kanʼaːbɨttarɨn. </ta>
            <ta e="T137" id="Seg_2767" s="T133">– Bu itte baːr diː. </ta>
            <ta e="T163" id="Seg_2768" s="T153">– Onton kelen bu karčɨtɨn barɨtɨn aragiː gɨmmɨta. </ta>
            <ta e="T169" id="Seg_2769" s="T166">– Diːgin du͡o? </ta>
            <ta e="T180" id="Seg_2770" s="T175">– Maŋnaj ispet ete bu͡o du? </ta>
            <ta e="T203" id="Seg_2771" s="T182">– Kelen baran ispet du manna uže ogoto bu ulaːppɨtɨn kenne. </ta>
            <ta e="T210" id="Seg_2772" s="T204">– Itinne kɨra. </ta>
            <ta e="T213" id="Seg_2773" s="T211">– Ogoto kɨra. </ta>
            <ta e="T218" id="Seg_2774" s="T214">Kelen baran kepsiːr du? </ta>
            <ta e="T232" id="Seg_2775" s="T219">Bu kelen baran kepsiːr eni kajdak (oloro-) olorbutun. </ta>
            <ta e="T236" id="Seg_2776" s="T233">Ihilliː olorollor du? </ta>
            <ta e="T239" id="Seg_2777" s="T237">Beːbe. </ta>
            <ta e="T256" id="Seg_2778" s="T247">– Bu baːr, bu baːr, bu baːr. </ta>
            <ta e="T262" id="Seg_2779" s="T258">Beːbe, maŋnajbɨtɨn bulu͡okputun naːda. </ta>
            <ta e="T276" id="Seg_2780" s="T263">Bu ke kata kaːjɨːttan taksɨbɨt bɨhɨlaːk bu͡o, bu ü͡örer. </ta>
            <ta e="T285" id="Seg_2781" s="T278">– Kaːjɨːga diː gini, kažetsa du. </ta>
            <ta e="T291" id="Seg_2782" s="T286">– Üleliːr du? </ta>
            <ta e="T320" id="Seg_2783" s="T312">– Bagarar ogoto iti kördük hüːre-hüːre keli͡ek ete gini͡eke. </ta>
            <ta e="T349" id="Seg_2784" s="T326">– Manna navʼerna kör, bu aragiːlɨː oloron, bu aragiːlɨː oloronnor kepsete (ke-) bu kihi kepsiːr diː "dʼaktargɨn körbütüm" diːr bu͡o. </ta>
            <ta e="T353" id="Seg_2785" s="T350">Kɨjŋammɨt uže bu͡o. </ta>
            <ta e="T369" id="Seg_2786" s="T354">Onton kelen, dʼe onton kelen baran atnašenʼije vɨjasnʼajdɨːr bu͡o. </ta>
            <ta e="T373" id="Seg_2787" s="T369">Kɨrbaːrɨ gɨnar, ol baːr. </ta>
            <ta e="T381" id="Seg_2788" s="T375">– Onton bu uže kɨrbaːbɨt, oksubut uže. </ta>
            <ta e="T390" id="Seg_2789" s="T385">– Bu kördük možet, da? </ta>
            <ta e="T395" id="Seg_2790" s="T391">– Egel bettek. </ta>
            <ta e="T401" id="Seg_2791" s="T397">– Egel biːr ((…)). </ta>
            <ta e="T411" id="Seg_2792" s="T402">– Bu baːr, maŋnaj bu baːr ke. </ta>
            <ta e="T447" id="Seg_2793" s="T427">– Hu͡ok, bu uže kanʼɨhɨgar "ispeppin" diːr uže, ikki paslʼednʼij kelen barar, kaːjɨːttan kelen baran, ja tak dumaju. </ta>
            <ta e="T449" id="Seg_2794" s="T448">Eː? </ta>
            <ta e="T454" id="Seg_2795" s="T450">– ((…)) ispet. </ta>
            <ta e="T478" id="Seg_2796" s="T461">– Üčügej bagajɨ oloror etiler, oloror etiler eni üleliː hɨldʼallar. </ta>
            <ta e="T506" id="Seg_2797" s="T484">– Če iti uže kaːjɨːtɨn kepsiːr diː gini, kojukkaːn eni, bu bu kennitten, bu kördük bu͡olu͡oga. </ta>
            <ta e="T516" id="Seg_2798" s="T507">Vot tak vot, i͡e, dapustiʼm, pustʼ budʼet iti kördük. </ta>
            <ta e="T529" id="Seg_2799" s="T517">A tut uvʼidʼela ja etʼi sʼifrɨ prastavlenɨ, eta v pravʼilʼnam parʼadke prastavlʼena? </ta>
            <ta e="T537" id="Seg_2800" s="T531">– Nʼe nada na nʼix smatrʼetʼ. </ta>
            <ta e="T542" id="Seg_2801" s="T538">Ja xʼitraja, to uvižu. </ta>
            <ta e="T554" id="Seg_2802" s="T543">Dʼe bu baːllar diːr maŋnaj, üleliː hɨldʼan bu͡o bu aragiːmsaktara kimneːbit. </ta>
            <ta e="T559" id="Seg_2803" s="T554">Tübespit. </ta>
            <ta e="T568" id="Seg_2804" s="T564">– Dʼe bu baːllar diː. </ta>
            <ta e="T576" id="Seg_2805" s="T570">– Bu iher olorollor. </ta>
            <ta e="T584" id="Seg_2806" s="T577">Ihe oloror, onton bu kepsiːller, gini kɨjŋanar. </ta>
            <ta e="T593" id="Seg_2807" s="T585">Kɨjŋanan baran dʼaktarga barar, ol baːr, egel onu. </ta>
            <ta e="T608" id="Seg_2808" s="T601">– Kulaːkɨtɨn belemnemmit. </ta>
            <ta e="T618" id="Seg_2809" s="T610">– Da, da, da, da, da. </ta>
            <ta e="T640" id="Seg_2810" s="T621">– A tam üčügej, bu baːr, üleliː hɨldʼallar, oloror etilere eni, kör, bu vmʼestʼe. </ta>
            <ta e="T651" id="Seg_2811" s="T647">– Bul, beːbe. </ta>
            <ta e="T667" id="Seg_2812" s="T654">– Bu baːr, bu baːr, munu munu daprašɨvajdɨːllar. </ta>
            <ta e="T672" id="Seg_2813" s="T669">– Bu baːllar, daprašɨvajdɨːllar. </ta>
            <ta e="T677" id="Seg_2814" s="T673">– Taŋas bi͡ereller. </ta>
            <ta e="T682" id="Seg_2815" s="T678">Kaːjɨllɨbɨt oloror, hanaːga battappɨt. </ta>
            <ta e="T688" id="Seg_2816" s="T683">Bu emi͡e hanaːga battappɨt oloror. </ta>
            <ta e="T697" id="Seg_2817" s="T689">Bu bu͡o uže mʼečʼtajdɨː oloror, navʼernaje, ogoto fstrʼečʼajdɨːrɨn. </ta>
            <ta e="T705" id="Seg_2818" s="T702">– Iti kördük. </ta>
            <ta e="T717" id="Seg_2819" s="T708">– Da, uže prʼedstavlʼajdɨːr dʼi͡etiger kelerin, navʼerna. </ta>
            <ta e="T729" id="Seg_2820" s="T723">– Onton ol taksɨbɨt. </ta>
            <ta e="T735" id="Seg_2821" s="T734">– Taksɨbɨt. </ta>
            <ta e="T743" id="Seg_2822" s="T739">– Taksɨbɨt, kelbit, ü͡öre-ü͡öre radastnɨj. </ta>
            <ta e="T747" id="Seg_2823" s="T744">Onton kepsiː oloror. </ta>
            <ta e="T758" id="Seg_2824" s="T748">Onton ((…)) ispet bu͡olbut. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-ChGS">
            <ta e="T63" id="Seg_2825" s="T61">kannan=ɨj</ta>
            <ta e="T65" id="Seg_2826" s="T63">maŋnaj</ta>
            <ta e="T70" id="Seg_2827" s="T69">kanʼec</ta>
            <ta e="T72" id="Seg_2828" s="T70">diː</ta>
            <ta e="T74" id="Seg_2829" s="T72">bu͡o</ta>
            <ta e="T78" id="Seg_2830" s="T77">kel-bit</ta>
            <ta e="T83" id="Seg_2831" s="T82">iti</ta>
            <ta e="T85" id="Seg_2832" s="T83">emi͡e</ta>
            <ta e="T87" id="Seg_2833" s="T85">onno</ta>
            <ta e="T88" id="Seg_2834" s="T87">olor-or</ta>
            <ta e="T94" id="Seg_2835" s="T92">manna</ta>
            <ta e="T96" id="Seg_2836" s="T94">navʼerna</ta>
            <ta e="T99" id="Seg_2837" s="T96">barɨ-ta</ta>
            <ta e="T108" id="Seg_2838" s="T105">heː</ta>
            <ta e="T110" id="Seg_2839" s="T109">beːbe</ta>
            <ta e="T111" id="Seg_2840" s="T110">iti</ta>
            <ta e="T112" id="Seg_2841" s="T111">tur-ar</ta>
            <ta e="T114" id="Seg_2842" s="T112">tüs-tün</ta>
            <ta e="T116" id="Seg_2843" s="T114">kör-ü͡ök</ta>
            <ta e="T117" id="Seg_2844" s="T116">bu-lar-ɨ</ta>
            <ta e="T119" id="Seg_2845" s="T117">uːr-atalaː</ta>
            <ta e="T120" id="Seg_2846" s="T119">dʼe</ta>
            <ta e="T122" id="Seg_2847" s="T120">barɨ-tɨ-n</ta>
            <ta e="T125" id="Seg_2848" s="T122">aragiː-laː-bɨt-tarɨ-n</ta>
            <ta e="T127" id="Seg_2849" s="T125">kanʼaː-bɨt-tarɨ-n</ta>
            <ta e="T134" id="Seg_2850" s="T133">bu</ta>
            <ta e="T135" id="Seg_2851" s="T134">itte</ta>
            <ta e="T136" id="Seg_2852" s="T135">baːr</ta>
            <ta e="T137" id="Seg_2853" s="T136">diː</ta>
            <ta e="T154" id="Seg_2854" s="T153">onton</ta>
            <ta e="T155" id="Seg_2855" s="T154">kel-en</ta>
            <ta e="T156" id="Seg_2856" s="T155">bu</ta>
            <ta e="T158" id="Seg_2857" s="T156">karčɨ-tɨ-n</ta>
            <ta e="T160" id="Seg_2858" s="T158">barɨ-tɨ-n</ta>
            <ta e="T161" id="Seg_2859" s="T160">aragiː</ta>
            <ta e="T163" id="Seg_2860" s="T161">gɨm-mɨt-a</ta>
            <ta e="T167" id="Seg_2861" s="T166">d-iː-gin</ta>
            <ta e="T169" id="Seg_2862" s="T167">du͡o</ta>
            <ta e="T176" id="Seg_2863" s="T175">maŋnaj</ta>
            <ta e="T177" id="Seg_2864" s="T176">is-pet</ta>
            <ta e="T178" id="Seg_2865" s="T177">e-t-e</ta>
            <ta e="T179" id="Seg_2866" s="T178">bu͡o</ta>
            <ta e="T180" id="Seg_2867" s="T179">du</ta>
            <ta e="T184" id="Seg_2868" s="T182">kel-en</ta>
            <ta e="T186" id="Seg_2869" s="T184">bar-an</ta>
            <ta e="T188" id="Seg_2870" s="T186">is-pet</ta>
            <ta e="T190" id="Seg_2871" s="T188">du</ta>
            <ta e="T192" id="Seg_2872" s="T190">manna</ta>
            <ta e="T194" id="Seg_2873" s="T192">uže</ta>
            <ta e="T196" id="Seg_2874" s="T194">ogo-to</ta>
            <ta e="T198" id="Seg_2875" s="T196">bu</ta>
            <ta e="T200" id="Seg_2876" s="T198">ulaːp-pɨt-ɨ-n</ta>
            <ta e="T203" id="Seg_2877" s="T200">kenne</ta>
            <ta e="T207" id="Seg_2878" s="T204">itinne</ta>
            <ta e="T210" id="Seg_2879" s="T207">kɨra</ta>
            <ta e="T212" id="Seg_2880" s="T211">ogo-to</ta>
            <ta e="T213" id="Seg_2881" s="T212">kɨra</ta>
            <ta e="T215" id="Seg_2882" s="T214">kel-en</ta>
            <ta e="T216" id="Seg_2883" s="T215">baran</ta>
            <ta e="T217" id="Seg_2884" s="T216">kepsiː-r</ta>
            <ta e="T218" id="Seg_2885" s="T217">du</ta>
            <ta e="T221" id="Seg_2886" s="T219">bu</ta>
            <ta e="T222" id="Seg_2887" s="T221">kel-en</ta>
            <ta e="T224" id="Seg_2888" s="T222">baran</ta>
            <ta e="T226" id="Seg_2889" s="T224">kepsiː-r</ta>
            <ta e="T227" id="Seg_2890" s="T226">eni</ta>
            <ta e="T229" id="Seg_2891" s="T227">kajdak</ta>
            <ta e="T231" id="Seg_2892" s="T229">olor-o</ta>
            <ta e="T232" id="Seg_2893" s="T231">olor-but-u-n</ta>
            <ta e="T234" id="Seg_2894" s="T233">ihill-iː</ta>
            <ta e="T235" id="Seg_2895" s="T234">olor-ol-lor</ta>
            <ta e="T236" id="Seg_2896" s="T235">du</ta>
            <ta e="T239" id="Seg_2897" s="T237">beːbe</ta>
            <ta e="T248" id="Seg_2898" s="T247">bu</ta>
            <ta e="T250" id="Seg_2899" s="T248">baːr</ta>
            <ta e="T251" id="Seg_2900" s="T250">bu</ta>
            <ta e="T253" id="Seg_2901" s="T251">baːr</ta>
            <ta e="T254" id="Seg_2902" s="T253">bu</ta>
            <ta e="T256" id="Seg_2903" s="T254">baːr</ta>
            <ta e="T259" id="Seg_2904" s="T258">beːbe</ta>
            <ta e="T260" id="Seg_2905" s="T259">maŋnaj-bɨtɨ-n</ta>
            <ta e="T261" id="Seg_2906" s="T260">bul-u͡ok-putu-n</ta>
            <ta e="T262" id="Seg_2907" s="T261">naːda</ta>
            <ta e="T265" id="Seg_2908" s="T263">bu</ta>
            <ta e="T266" id="Seg_2909" s="T265">ke</ta>
            <ta e="T267" id="Seg_2910" s="T266">kata</ta>
            <ta e="T268" id="Seg_2911" s="T267">kaːjɨː-ttan</ta>
            <ta e="T270" id="Seg_2912" s="T268">taks-ɨ-bɨt</ta>
            <ta e="T272" id="Seg_2913" s="T270">bɨhɨlaːk</ta>
            <ta e="T273" id="Seg_2914" s="T272">bu͡o</ta>
            <ta e="T274" id="Seg_2915" s="T273">bu</ta>
            <ta e="T276" id="Seg_2916" s="T274">ü͡ör-er</ta>
            <ta e="T279" id="Seg_2917" s="T278">kaːjɨː-ga</ta>
            <ta e="T280" id="Seg_2918" s="T279">diː</ta>
            <ta e="T282" id="Seg_2919" s="T280">gini</ta>
            <ta e="T284" id="Seg_2920" s="T282">kažetsa</ta>
            <ta e="T285" id="Seg_2921" s="T284">du</ta>
            <ta e="T289" id="Seg_2922" s="T286">üleliː-r</ta>
            <ta e="T291" id="Seg_2923" s="T289">du</ta>
            <ta e="T313" id="Seg_2924" s="T312">bagar-ar</ta>
            <ta e="T314" id="Seg_2925" s="T313">ogo-to</ta>
            <ta e="T315" id="Seg_2926" s="T314">iti</ta>
            <ta e="T316" id="Seg_2927" s="T315">kördük</ta>
            <ta e="T317" id="Seg_2928" s="T316">hüːr-e-hüːr-e</ta>
            <ta e="T318" id="Seg_2929" s="T317">kel-i͡ek</ta>
            <ta e="T319" id="Seg_2930" s="T318">e-t-e</ta>
            <ta e="T320" id="Seg_2931" s="T319">gini͡e-ke</ta>
            <ta e="T328" id="Seg_2932" s="T326">manna</ta>
            <ta e="T329" id="Seg_2933" s="T328">navʼerna</ta>
            <ta e="T331" id="Seg_2934" s="T329">kör</ta>
            <ta e="T332" id="Seg_2935" s="T331">bu</ta>
            <ta e="T333" id="Seg_2936" s="T332">aragiː-l-ɨː</ta>
            <ta e="T335" id="Seg_2937" s="T333">olor-on</ta>
            <ta e="T336" id="Seg_2938" s="T335">bu</ta>
            <ta e="T338" id="Seg_2939" s="T336">aragiː-l-ɨː</ta>
            <ta e="T339" id="Seg_2940" s="T338">olor-on-nor</ta>
            <ta e="T340" id="Seg_2941" s="T339">kepset-e</ta>
            <ta e="T342" id="Seg_2942" s="T341">bu</ta>
            <ta e="T343" id="Seg_2943" s="T342">kihi</ta>
            <ta e="T344" id="Seg_2944" s="T343">kepsiː-r</ta>
            <ta e="T345" id="Seg_2945" s="T344">d-iː</ta>
            <ta e="T346" id="Seg_2946" s="T345">dʼaktar-gɨ-n</ta>
            <ta e="T347" id="Seg_2947" s="T346">kör-büt-ü-m</ta>
            <ta e="T348" id="Seg_2948" s="T347">diː-r</ta>
            <ta e="T349" id="Seg_2949" s="T348">bu͡o</ta>
            <ta e="T351" id="Seg_2950" s="T350">kɨjŋam-mɨt</ta>
            <ta e="T352" id="Seg_2951" s="T351">uže</ta>
            <ta e="T353" id="Seg_2952" s="T352">bu͡o</ta>
            <ta e="T356" id="Seg_2953" s="T354">onton</ta>
            <ta e="T358" id="Seg_2954" s="T356">kel-en</ta>
            <ta e="T360" id="Seg_2955" s="T358">dʼe</ta>
            <ta e="T362" id="Seg_2956" s="T360">onton</ta>
            <ta e="T364" id="Seg_2957" s="T362">kel-en</ta>
            <ta e="T366" id="Seg_2958" s="T364">baran</ta>
            <ta e="T367" id="Seg_2959" s="T366">atnašenʼije</ta>
            <ta e="T368" id="Seg_2960" s="T367">vɨjasnʼaj-dɨː-r</ta>
            <ta e="T369" id="Seg_2961" s="T368">bu͡o</ta>
            <ta e="T370" id="Seg_2962" s="T369">kɨrb-aːrɨ</ta>
            <ta e="T371" id="Seg_2963" s="T370">gɨn-ar</ta>
            <ta e="T372" id="Seg_2964" s="T371">ol</ta>
            <ta e="T373" id="Seg_2965" s="T372">baːr</ta>
            <ta e="T376" id="Seg_2966" s="T375">onton</ta>
            <ta e="T377" id="Seg_2967" s="T376">bu</ta>
            <ta e="T378" id="Seg_2968" s="T377">uže</ta>
            <ta e="T379" id="Seg_2969" s="T378">kɨrbaː-bɨt</ta>
            <ta e="T380" id="Seg_2970" s="T379">oks-u-but</ta>
            <ta e="T381" id="Seg_2971" s="T380">uže</ta>
            <ta e="T386" id="Seg_2972" s="T385">bu</ta>
            <ta e="T387" id="Seg_2973" s="T386">kördük</ta>
            <ta e="T389" id="Seg_2974" s="T387">možet</ta>
            <ta e="T390" id="Seg_2975" s="T389">da</ta>
            <ta e="T393" id="Seg_2976" s="T391">egel</ta>
            <ta e="T395" id="Seg_2977" s="T393">bettek</ta>
            <ta e="T398" id="Seg_2978" s="T397">egel</ta>
            <ta e="T399" id="Seg_2979" s="T398">biːr</ta>
            <ta e="T404" id="Seg_2980" s="T402">bu</ta>
            <ta e="T405" id="Seg_2981" s="T404">baːr</ta>
            <ta e="T407" id="Seg_2982" s="T405">maŋnaj</ta>
            <ta e="T408" id="Seg_2983" s="T407">bu</ta>
            <ta e="T409" id="Seg_2984" s="T408">baːr</ta>
            <ta e="T411" id="Seg_2985" s="T409">ke</ta>
            <ta e="T429" id="Seg_2986" s="T427">hu͡ok</ta>
            <ta e="T430" id="Seg_2987" s="T429">bu</ta>
            <ta e="T431" id="Seg_2988" s="T430">uže</ta>
            <ta e="T432" id="Seg_2989" s="T431">kanʼɨh-ɨ-gar</ta>
            <ta e="T433" id="Seg_2990" s="T432">is-pep-pin</ta>
            <ta e="T434" id="Seg_2991" s="T433">diː-r</ta>
            <ta e="T435" id="Seg_2992" s="T434">uže</ta>
            <ta e="T436" id="Seg_2993" s="T435">ikki</ta>
            <ta e="T437" id="Seg_2994" s="T436">paslʼednʼij</ta>
            <ta e="T438" id="Seg_2995" s="T437">kel-en</ta>
            <ta e="T439" id="Seg_2996" s="T438">bar-ar</ta>
            <ta e="T441" id="Seg_2997" s="T439">kaːjɨː-ttan</ta>
            <ta e="T442" id="Seg_2998" s="T441">kel-en</ta>
            <ta e="T443" id="Seg_2999" s="T442">baran</ta>
            <ta e="T449" id="Seg_3000" s="T448">eː</ta>
            <ta e="T454" id="Seg_3001" s="T453">is-pet</ta>
            <ta e="T464" id="Seg_3002" s="T461">üčügej</ta>
            <ta e="T466" id="Seg_3003" s="T464">bagajɨ</ta>
            <ta e="T468" id="Seg_3004" s="T466">olor-or</ta>
            <ta e="T469" id="Seg_3005" s="T468">e-ti-ler</ta>
            <ta e="T471" id="Seg_3006" s="T469">olor-or</ta>
            <ta e="T473" id="Seg_3007" s="T471">e-ti-ler</ta>
            <ta e="T475" id="Seg_3008" s="T473">eni</ta>
            <ta e="T477" id="Seg_3009" s="T475">ülel-iː</ta>
            <ta e="T478" id="Seg_3010" s="T477">hɨldʼ-al-lar</ta>
            <ta e="T485" id="Seg_3011" s="T484">če</ta>
            <ta e="T486" id="Seg_3012" s="T485">iti</ta>
            <ta e="T487" id="Seg_3013" s="T486">uže</ta>
            <ta e="T488" id="Seg_3014" s="T487">kaːjɨː-tɨ-n</ta>
            <ta e="T489" id="Seg_3015" s="T488">kepsiː-r</ta>
            <ta e="T491" id="Seg_3016" s="T489">diː</ta>
            <ta e="T494" id="Seg_3017" s="T491">gini</ta>
            <ta e="T496" id="Seg_3018" s="T494">kojuk-kaːn</ta>
            <ta e="T498" id="Seg_3019" s="T496">eni</ta>
            <ta e="T499" id="Seg_3020" s="T498">bu</ta>
            <ta e="T501" id="Seg_3021" s="T499">bu</ta>
            <ta e="T502" id="Seg_3022" s="T501">kenn-i-tten</ta>
            <ta e="T503" id="Seg_3023" s="T502">bu</ta>
            <ta e="T505" id="Seg_3024" s="T503">kördük</ta>
            <ta e="T506" id="Seg_3025" s="T505">bu͡ol-u͡og-a</ta>
            <ta e="T511" id="Seg_3026" s="T510">i͡e</ta>
            <ta e="T515" id="Seg_3027" s="T514">iti</ta>
            <ta e="T516" id="Seg_3028" s="T515">kördük</ta>
            <ta e="T544" id="Seg_3029" s="T543">dʼe</ta>
            <ta e="T545" id="Seg_3030" s="T544">bu</ta>
            <ta e="T546" id="Seg_3031" s="T545">baːl-lar</ta>
            <ta e="T547" id="Seg_3032" s="T546">diː-r</ta>
            <ta e="T548" id="Seg_3033" s="T547">maŋnaj</ta>
            <ta e="T549" id="Seg_3034" s="T548">ülel-iː</ta>
            <ta e="T550" id="Seg_3035" s="T549">hɨldʼ-an</ta>
            <ta e="T551" id="Seg_3036" s="T550">bu͡o</ta>
            <ta e="T552" id="Seg_3037" s="T551">bu</ta>
            <ta e="T553" id="Seg_3038" s="T552">aragiː-msak-tara</ta>
            <ta e="T554" id="Seg_3039" s="T553">kim-neː-bit</ta>
            <ta e="T559" id="Seg_3040" s="T554">tübes-pit</ta>
            <ta e="T565" id="Seg_3041" s="T564">dʼe</ta>
            <ta e="T566" id="Seg_3042" s="T565">bu</ta>
            <ta e="T567" id="Seg_3043" s="T566">baːl-lar</ta>
            <ta e="T568" id="Seg_3044" s="T567">diː</ta>
            <ta e="T572" id="Seg_3045" s="T570">bu</ta>
            <ta e="T574" id="Seg_3046" s="T572">ih-er</ta>
            <ta e="T576" id="Seg_3047" s="T574">olor-ol-lor</ta>
            <ta e="T578" id="Seg_3048" s="T577">ih-e</ta>
            <ta e="T579" id="Seg_3049" s="T578">olor-or</ta>
            <ta e="T580" id="Seg_3050" s="T579">onton</ta>
            <ta e="T581" id="Seg_3051" s="T580">bu</ta>
            <ta e="T582" id="Seg_3052" s="T581">kepsiː-l-ler</ta>
            <ta e="T583" id="Seg_3053" s="T582">gini</ta>
            <ta e="T584" id="Seg_3054" s="T583">kɨjŋan-ar</ta>
            <ta e="T586" id="Seg_3055" s="T585">kɨjŋan-an</ta>
            <ta e="T587" id="Seg_3056" s="T586">baran</ta>
            <ta e="T588" id="Seg_3057" s="T587">dʼaktar-ga</ta>
            <ta e="T589" id="Seg_3058" s="T588">bar-ar</ta>
            <ta e="T590" id="Seg_3059" s="T589">ol</ta>
            <ta e="T591" id="Seg_3060" s="T590">baːr</ta>
            <ta e="T592" id="Seg_3061" s="T591">egel</ta>
            <ta e="T593" id="Seg_3062" s="T592">o-nu</ta>
            <ta e="T604" id="Seg_3063" s="T601">kulaːkɨ-tɨ-n</ta>
            <ta e="T608" id="Seg_3064" s="T604">belemn-e-m-mit</ta>
            <ta e="T612" id="Seg_3065" s="T610">da</ta>
            <ta e="T613" id="Seg_3066" s="T612">da</ta>
            <ta e="T615" id="Seg_3067" s="T613">da</ta>
            <ta e="T616" id="Seg_3068" s="T615">da</ta>
            <ta e="T618" id="Seg_3069" s="T616">da</ta>
            <ta e="T626" id="Seg_3070" s="T624">üčügej</ta>
            <ta e="T627" id="Seg_3071" s="T626">bu</ta>
            <ta e="T629" id="Seg_3072" s="T627">baːr</ta>
            <ta e="T630" id="Seg_3073" s="T629">ülel-iː</ta>
            <ta e="T631" id="Seg_3074" s="T630">hɨldʼ-al-lar</ta>
            <ta e="T633" id="Seg_3075" s="T631">olor-or</ta>
            <ta e="T634" id="Seg_3076" s="T633">e-ti-lere</ta>
            <ta e="T636" id="Seg_3077" s="T634">eni</ta>
            <ta e="T637" id="Seg_3078" s="T636">kör</ta>
            <ta e="T638" id="Seg_3079" s="T637">bu</ta>
            <ta e="T640" id="Seg_3080" s="T638">vmʼestʼe</ta>
            <ta e="T649" id="Seg_3081" s="T647">bul</ta>
            <ta e="T651" id="Seg_3082" s="T649">beːbe</ta>
            <ta e="T656" id="Seg_3083" s="T654">bu</ta>
            <ta e="T658" id="Seg_3084" s="T656">baːr</ta>
            <ta e="T660" id="Seg_3085" s="T658">bu</ta>
            <ta e="T662" id="Seg_3086" s="T660">baːr</ta>
            <ta e="T664" id="Seg_3087" s="T662">mu-nu</ta>
            <ta e="T665" id="Seg_3088" s="T664">mu-nu</ta>
            <ta e="T667" id="Seg_3089" s="T665">daprašɨvaj-dɨː-l-lar</ta>
            <ta e="T670" id="Seg_3090" s="T669">bu</ta>
            <ta e="T671" id="Seg_3091" s="T670">baːl-lar</ta>
            <ta e="T672" id="Seg_3092" s="T671">daprašɨvaj-dɨː-l-lar</ta>
            <ta e="T676" id="Seg_3093" s="T673">taŋas</ta>
            <ta e="T677" id="Seg_3094" s="T676">bi͡er-el-ler</ta>
            <ta e="T679" id="Seg_3095" s="T678">kaːj-ɨ-ll-ɨ-bɨt</ta>
            <ta e="T680" id="Seg_3096" s="T679">olor-or</ta>
            <ta e="T681" id="Seg_3097" s="T680">hanaː-ga</ta>
            <ta e="T682" id="Seg_3098" s="T681">batt-a-p-pɨt</ta>
            <ta e="T684" id="Seg_3099" s="T683">bu</ta>
            <ta e="T685" id="Seg_3100" s="T684">emi͡e</ta>
            <ta e="T686" id="Seg_3101" s="T685">hanaː-ga</ta>
            <ta e="T687" id="Seg_3102" s="T686">batt-a-p-pɨt</ta>
            <ta e="T688" id="Seg_3103" s="T687">olor-or</ta>
            <ta e="T690" id="Seg_3104" s="T689">bu</ta>
            <ta e="T691" id="Seg_3105" s="T690">bu͡o</ta>
            <ta e="T692" id="Seg_3106" s="T691">uže</ta>
            <ta e="T693" id="Seg_3107" s="T692">mʼečʼtaj-d-ɨː</ta>
            <ta e="T694" id="Seg_3108" s="T693">olor-or</ta>
            <ta e="T695" id="Seg_3109" s="T694">navʼernaje</ta>
            <ta e="T696" id="Seg_3110" s="T695">ogo-to</ta>
            <ta e="T697" id="Seg_3111" s="T696">fstrʼečʼaj-dɨː-r-ɨ-n</ta>
            <ta e="T704" id="Seg_3112" s="T702">iti</ta>
            <ta e="T705" id="Seg_3113" s="T704">kördük</ta>
            <ta e="T709" id="Seg_3114" s="T708">da</ta>
            <ta e="T711" id="Seg_3115" s="T709">uže</ta>
            <ta e="T713" id="Seg_3116" s="T711">prʼedstavlʼaj-dɨː-r</ta>
            <ta e="T714" id="Seg_3117" s="T713">dʼi͡e-ti-ger</ta>
            <ta e="T715" id="Seg_3118" s="T714">kel-er-i-n</ta>
            <ta e="T717" id="Seg_3119" s="T715">navʼerna</ta>
            <ta e="T725" id="Seg_3120" s="T723">onton</ta>
            <ta e="T727" id="Seg_3121" s="T725">ol</ta>
            <ta e="T729" id="Seg_3122" s="T727">taks-ɨ-bɨt</ta>
            <ta e="T735" id="Seg_3123" s="T734">taks-ɨ-bɨt</ta>
            <ta e="T740" id="Seg_3124" s="T739">taks-ɨ-bɨt</ta>
            <ta e="T741" id="Seg_3125" s="T740">kel-bit</ta>
            <ta e="T742" id="Seg_3126" s="T741">ü͡ör-e-ü͡ör-e</ta>
            <ta e="T743" id="Seg_3127" s="T742">radastnɨj</ta>
            <ta e="T745" id="Seg_3128" s="T744">onton</ta>
            <ta e="T746" id="Seg_3129" s="T745">keps-iː</ta>
            <ta e="T747" id="Seg_3130" s="T746">olor-or</ta>
            <ta e="T749" id="Seg_3131" s="T748">onton</ta>
            <ta e="T756" id="Seg_3132" s="T752">is-pet</ta>
            <ta e="T758" id="Seg_3133" s="T756">bu͡ol-but</ta>
         </annotation>
         <annotation name="mp" tierref="mp-ChGS">
            <ta e="T63" id="Seg_3134" s="T61">kanna=Ij</ta>
            <ta e="T65" id="Seg_3135" s="T63">maŋnaj</ta>
            <ta e="T70" id="Seg_3136" s="T69">kanʼec</ta>
            <ta e="T72" id="Seg_3137" s="T70">diː</ta>
            <ta e="T74" id="Seg_3138" s="T72">bu͡o</ta>
            <ta e="T78" id="Seg_3139" s="T77">kel-BIT</ta>
            <ta e="T83" id="Seg_3140" s="T82">iti</ta>
            <ta e="T85" id="Seg_3141" s="T83">emi͡e</ta>
            <ta e="T87" id="Seg_3142" s="T85">onno</ta>
            <ta e="T88" id="Seg_3143" s="T87">olor-Ar</ta>
            <ta e="T94" id="Seg_3144" s="T92">manna</ta>
            <ta e="T96" id="Seg_3145" s="T94">navʼernaje</ta>
            <ta e="T99" id="Seg_3146" s="T96">barɨ-tA</ta>
            <ta e="T108" id="Seg_3147" s="T105">eː</ta>
            <ta e="T110" id="Seg_3148" s="T109">beːbe</ta>
            <ta e="T111" id="Seg_3149" s="T110">iti</ta>
            <ta e="T112" id="Seg_3150" s="T111">tur-Ar</ta>
            <ta e="T114" id="Seg_3151" s="T112">tüs-TIn</ta>
            <ta e="T116" id="Seg_3152" s="T114">kör-IAk</ta>
            <ta e="T117" id="Seg_3153" s="T116">bu-LAr-nI</ta>
            <ta e="T119" id="Seg_3154" s="T117">uːr-ItAlAː</ta>
            <ta e="T120" id="Seg_3155" s="T119">dʼe</ta>
            <ta e="T122" id="Seg_3156" s="T120">barɨ-tI-n</ta>
            <ta e="T125" id="Seg_3157" s="T122">aragiː-LAː-BIT-LArI-n</ta>
            <ta e="T127" id="Seg_3158" s="T125">kanʼaː-BIT-LArI-n</ta>
            <ta e="T134" id="Seg_3159" s="T133">bu</ta>
            <ta e="T135" id="Seg_3160" s="T134">itte</ta>
            <ta e="T136" id="Seg_3161" s="T135">baːr</ta>
            <ta e="T137" id="Seg_3162" s="T136">diː</ta>
            <ta e="T154" id="Seg_3163" s="T153">onton</ta>
            <ta e="T155" id="Seg_3164" s="T154">kel-An</ta>
            <ta e="T156" id="Seg_3165" s="T155">bu</ta>
            <ta e="T158" id="Seg_3166" s="T156">karčɨ-tI-n</ta>
            <ta e="T160" id="Seg_3167" s="T158">barɨ-tI-n</ta>
            <ta e="T161" id="Seg_3168" s="T160">aragiː</ta>
            <ta e="T163" id="Seg_3169" s="T161">gɨn-BIT-tA</ta>
            <ta e="T167" id="Seg_3170" s="T166">di͡e-A-GIn</ta>
            <ta e="T169" id="Seg_3171" s="T167">du͡o</ta>
            <ta e="T176" id="Seg_3172" s="T175">maŋnaj</ta>
            <ta e="T177" id="Seg_3173" s="T176">is-BAT</ta>
            <ta e="T178" id="Seg_3174" s="T177">e-TI-tA</ta>
            <ta e="T179" id="Seg_3175" s="T178">bu͡o</ta>
            <ta e="T180" id="Seg_3176" s="T179">du͡o</ta>
            <ta e="T184" id="Seg_3177" s="T182">kel-An</ta>
            <ta e="T186" id="Seg_3178" s="T184">bar-An</ta>
            <ta e="T188" id="Seg_3179" s="T186">is-BAT</ta>
            <ta e="T190" id="Seg_3180" s="T188">du͡o</ta>
            <ta e="T192" id="Seg_3181" s="T190">manna</ta>
            <ta e="T194" id="Seg_3182" s="T192">uže</ta>
            <ta e="T196" id="Seg_3183" s="T194">ogo-tA</ta>
            <ta e="T198" id="Seg_3184" s="T196">bu</ta>
            <ta e="T200" id="Seg_3185" s="T198">ulaːt-BIT-tI-n</ta>
            <ta e="T203" id="Seg_3186" s="T200">genne</ta>
            <ta e="T207" id="Seg_3187" s="T204">itinne</ta>
            <ta e="T210" id="Seg_3188" s="T207">kɨra</ta>
            <ta e="T212" id="Seg_3189" s="T211">ogo-tA</ta>
            <ta e="T213" id="Seg_3190" s="T212">kɨra</ta>
            <ta e="T215" id="Seg_3191" s="T214">kel-An</ta>
            <ta e="T216" id="Seg_3192" s="T215">baran</ta>
            <ta e="T217" id="Seg_3193" s="T216">kepseː-Ar</ta>
            <ta e="T218" id="Seg_3194" s="T217">du͡o</ta>
            <ta e="T221" id="Seg_3195" s="T219">bu</ta>
            <ta e="T222" id="Seg_3196" s="T221">kel-An</ta>
            <ta e="T224" id="Seg_3197" s="T222">baran</ta>
            <ta e="T226" id="Seg_3198" s="T224">kepseː-Ar</ta>
            <ta e="T227" id="Seg_3199" s="T226">eni</ta>
            <ta e="T229" id="Seg_3200" s="T227">kajdak</ta>
            <ta e="T231" id="Seg_3201" s="T229">olor-A</ta>
            <ta e="T232" id="Seg_3202" s="T231">olor-BIT-tI-n</ta>
            <ta e="T234" id="Seg_3203" s="T233">ihilleː-A</ta>
            <ta e="T235" id="Seg_3204" s="T234">olor-Ar-LAr</ta>
            <ta e="T236" id="Seg_3205" s="T235">du͡o</ta>
            <ta e="T239" id="Seg_3206" s="T237">beːbe</ta>
            <ta e="T248" id="Seg_3207" s="T247">bu</ta>
            <ta e="T250" id="Seg_3208" s="T248">baːr</ta>
            <ta e="T251" id="Seg_3209" s="T250">bu</ta>
            <ta e="T253" id="Seg_3210" s="T251">baːr</ta>
            <ta e="T254" id="Seg_3211" s="T253">bu</ta>
            <ta e="T256" id="Seg_3212" s="T254">baːr</ta>
            <ta e="T259" id="Seg_3213" s="T258">beːbe</ta>
            <ta e="T260" id="Seg_3214" s="T259">maŋnaj-BItI-n</ta>
            <ta e="T261" id="Seg_3215" s="T260">bul-IAK-BItI-n</ta>
            <ta e="T262" id="Seg_3216" s="T261">naːda</ta>
            <ta e="T265" id="Seg_3217" s="T263">bu</ta>
            <ta e="T266" id="Seg_3218" s="T265">ka</ta>
            <ta e="T267" id="Seg_3219" s="T266">kata</ta>
            <ta e="T268" id="Seg_3220" s="T267">kaːjɨː-ttAn</ta>
            <ta e="T270" id="Seg_3221" s="T268">tagɨs-I-BIT</ta>
            <ta e="T272" id="Seg_3222" s="T270">bɨhɨːlaːk</ta>
            <ta e="T273" id="Seg_3223" s="T272">bu͡o</ta>
            <ta e="T274" id="Seg_3224" s="T273">bu</ta>
            <ta e="T276" id="Seg_3225" s="T274">ü͡ör-Ar</ta>
            <ta e="T279" id="Seg_3226" s="T278">kaːjɨː-GA</ta>
            <ta e="T280" id="Seg_3227" s="T279">diː</ta>
            <ta e="T282" id="Seg_3228" s="T280">gini</ta>
            <ta e="T284" id="Seg_3229" s="T282">kažetsa</ta>
            <ta e="T285" id="Seg_3230" s="T284">du͡o</ta>
            <ta e="T289" id="Seg_3231" s="T286">üleleː-Ar</ta>
            <ta e="T291" id="Seg_3232" s="T289">du͡o</ta>
            <ta e="T313" id="Seg_3233" s="T312">bagar-Ar</ta>
            <ta e="T314" id="Seg_3234" s="T313">ogo-tA</ta>
            <ta e="T315" id="Seg_3235" s="T314">iti</ta>
            <ta e="T316" id="Seg_3236" s="T315">kördük</ta>
            <ta e="T317" id="Seg_3237" s="T316">hüːr-A-hüːr-A</ta>
            <ta e="T318" id="Seg_3238" s="T317">kel-IAK</ta>
            <ta e="T319" id="Seg_3239" s="T318">e-TI-tA</ta>
            <ta e="T320" id="Seg_3240" s="T319">gini-GA</ta>
            <ta e="T328" id="Seg_3241" s="T326">manna</ta>
            <ta e="T329" id="Seg_3242" s="T328">navʼernaje</ta>
            <ta e="T331" id="Seg_3243" s="T329">kör</ta>
            <ta e="T332" id="Seg_3244" s="T331">bu</ta>
            <ta e="T333" id="Seg_3245" s="T332">aragiː-LAː-A</ta>
            <ta e="T335" id="Seg_3246" s="T333">olor-An</ta>
            <ta e="T336" id="Seg_3247" s="T335">bu</ta>
            <ta e="T338" id="Seg_3248" s="T336">aragiː-LAː-A</ta>
            <ta e="T339" id="Seg_3249" s="T338">olor-An-LAr</ta>
            <ta e="T340" id="Seg_3250" s="T339">kepset-A</ta>
            <ta e="T342" id="Seg_3251" s="T341">bu</ta>
            <ta e="T343" id="Seg_3252" s="T342">kihi</ta>
            <ta e="T344" id="Seg_3253" s="T343">kepseː-Ar</ta>
            <ta e="T345" id="Seg_3254" s="T344">di͡e-A</ta>
            <ta e="T346" id="Seg_3255" s="T345">dʼaktar-GI-n</ta>
            <ta e="T347" id="Seg_3256" s="T346">kör-BIT-I-m</ta>
            <ta e="T348" id="Seg_3257" s="T347">di͡e-Ar</ta>
            <ta e="T349" id="Seg_3258" s="T348">bu͡o</ta>
            <ta e="T351" id="Seg_3259" s="T350">kɨjgan-BIT</ta>
            <ta e="T352" id="Seg_3260" s="T351">uže</ta>
            <ta e="T353" id="Seg_3261" s="T352">bu͡o</ta>
            <ta e="T356" id="Seg_3262" s="T354">onton</ta>
            <ta e="T358" id="Seg_3263" s="T356">kel-An</ta>
            <ta e="T360" id="Seg_3264" s="T358">dʼe</ta>
            <ta e="T362" id="Seg_3265" s="T360">onton</ta>
            <ta e="T364" id="Seg_3266" s="T362">kel-An</ta>
            <ta e="T366" id="Seg_3267" s="T364">baran</ta>
            <ta e="T367" id="Seg_3268" s="T366">atnašenʼije</ta>
            <ta e="T368" id="Seg_3269" s="T367">vɨjasnʼaj-LAː-Ar</ta>
            <ta e="T369" id="Seg_3270" s="T368">bu͡o</ta>
            <ta e="T370" id="Seg_3271" s="T369">kɨrbaː-AːrI</ta>
            <ta e="T371" id="Seg_3272" s="T370">gɨn-Ar</ta>
            <ta e="T372" id="Seg_3273" s="T371">ol</ta>
            <ta e="T373" id="Seg_3274" s="T372">baːr</ta>
            <ta e="T376" id="Seg_3275" s="T375">onton</ta>
            <ta e="T377" id="Seg_3276" s="T376">bu</ta>
            <ta e="T378" id="Seg_3277" s="T377">uže</ta>
            <ta e="T379" id="Seg_3278" s="T378">kɨrbaː-BIT</ta>
            <ta e="T380" id="Seg_3279" s="T379">ogus-I-BIT</ta>
            <ta e="T381" id="Seg_3280" s="T380">uže</ta>
            <ta e="T386" id="Seg_3281" s="T385">bu</ta>
            <ta e="T387" id="Seg_3282" s="T386">kördük</ta>
            <ta e="T389" id="Seg_3283" s="T387">mozet</ta>
            <ta e="T390" id="Seg_3284" s="T389">da</ta>
            <ta e="T393" id="Seg_3285" s="T391">egel</ta>
            <ta e="T395" id="Seg_3286" s="T393">bettek</ta>
            <ta e="T398" id="Seg_3287" s="T397">egel</ta>
            <ta e="T399" id="Seg_3288" s="T398">biːr</ta>
            <ta e="T404" id="Seg_3289" s="T402">bu</ta>
            <ta e="T405" id="Seg_3290" s="T404">baːr</ta>
            <ta e="T407" id="Seg_3291" s="T405">maŋnaj</ta>
            <ta e="T408" id="Seg_3292" s="T407">bu</ta>
            <ta e="T409" id="Seg_3293" s="T408">baːr</ta>
            <ta e="T411" id="Seg_3294" s="T409">ka</ta>
            <ta e="T429" id="Seg_3295" s="T427">hu͡ok</ta>
            <ta e="T430" id="Seg_3296" s="T429">bu</ta>
            <ta e="T431" id="Seg_3297" s="T430">uže</ta>
            <ta e="T432" id="Seg_3298" s="T431">kanʼec-tI-GAr</ta>
            <ta e="T433" id="Seg_3299" s="T432">is-BAT-BIn</ta>
            <ta e="T434" id="Seg_3300" s="T433">di͡e-Ar</ta>
            <ta e="T435" id="Seg_3301" s="T434">uže</ta>
            <ta e="T436" id="Seg_3302" s="T435">ikki</ta>
            <ta e="T437" id="Seg_3303" s="T436">pusli͡ednej</ta>
            <ta e="T438" id="Seg_3304" s="T437">kel-An</ta>
            <ta e="T439" id="Seg_3305" s="T438">bar-Ar</ta>
            <ta e="T441" id="Seg_3306" s="T439">kaːjɨː-ttAn</ta>
            <ta e="T442" id="Seg_3307" s="T441">kel-An</ta>
            <ta e="T443" id="Seg_3308" s="T442">baran</ta>
            <ta e="T449" id="Seg_3309" s="T448">eː</ta>
            <ta e="T454" id="Seg_3310" s="T453">is-BAT</ta>
            <ta e="T464" id="Seg_3311" s="T461">üčügej</ta>
            <ta e="T466" id="Seg_3312" s="T464">bagajɨ</ta>
            <ta e="T468" id="Seg_3313" s="T466">olor-Ar</ta>
            <ta e="T469" id="Seg_3314" s="T468">e-TI-LAr</ta>
            <ta e="T471" id="Seg_3315" s="T469">olor-Ar</ta>
            <ta e="T473" id="Seg_3316" s="T471">e-TI-LAr</ta>
            <ta e="T475" id="Seg_3317" s="T473">eni</ta>
            <ta e="T477" id="Seg_3318" s="T475">üleleː-A</ta>
            <ta e="T478" id="Seg_3319" s="T477">hɨrɨt-Ar-LAr</ta>
            <ta e="T485" id="Seg_3320" s="T484">dʼe</ta>
            <ta e="T486" id="Seg_3321" s="T485">iti</ta>
            <ta e="T487" id="Seg_3322" s="T486">uže</ta>
            <ta e="T488" id="Seg_3323" s="T487">kaːjɨː-tI-n</ta>
            <ta e="T489" id="Seg_3324" s="T488">kepseː-Ar</ta>
            <ta e="T491" id="Seg_3325" s="T489">diː</ta>
            <ta e="T494" id="Seg_3326" s="T491">gini</ta>
            <ta e="T496" id="Seg_3327" s="T494">kojut-kAːN</ta>
            <ta e="T498" id="Seg_3328" s="T496">eni</ta>
            <ta e="T499" id="Seg_3329" s="T498">bu</ta>
            <ta e="T501" id="Seg_3330" s="T499">bu</ta>
            <ta e="T502" id="Seg_3331" s="T501">kelin-tI-ttAn</ta>
            <ta e="T503" id="Seg_3332" s="T502">bu</ta>
            <ta e="T505" id="Seg_3333" s="T503">kördük</ta>
            <ta e="T506" id="Seg_3334" s="T505">bu͡ol-IAK-tA</ta>
            <ta e="T511" id="Seg_3335" s="T510">eː</ta>
            <ta e="T515" id="Seg_3336" s="T514">iti</ta>
            <ta e="T516" id="Seg_3337" s="T515">kördük</ta>
            <ta e="T544" id="Seg_3338" s="T543">dʼe</ta>
            <ta e="T545" id="Seg_3339" s="T544">bu</ta>
            <ta e="T546" id="Seg_3340" s="T545">baːr-LAr</ta>
            <ta e="T547" id="Seg_3341" s="T546">di͡e-Ar</ta>
            <ta e="T548" id="Seg_3342" s="T547">maŋnaj</ta>
            <ta e="T549" id="Seg_3343" s="T548">üleleː-A</ta>
            <ta e="T550" id="Seg_3344" s="T549">hɨrɨt-An</ta>
            <ta e="T551" id="Seg_3345" s="T550">bu͡o</ta>
            <ta e="T552" id="Seg_3346" s="T551">bu</ta>
            <ta e="T553" id="Seg_3347" s="T552">aragiː-msAk-LArA</ta>
            <ta e="T554" id="Seg_3348" s="T553">kim-LAː-BIT</ta>
            <ta e="T559" id="Seg_3349" s="T554">tübes-BIT</ta>
            <ta e="T565" id="Seg_3350" s="T564">dʼe</ta>
            <ta e="T566" id="Seg_3351" s="T565">bu</ta>
            <ta e="T567" id="Seg_3352" s="T566">baːr-LAr</ta>
            <ta e="T568" id="Seg_3353" s="T567">diː</ta>
            <ta e="T572" id="Seg_3354" s="T570">bu</ta>
            <ta e="T574" id="Seg_3355" s="T572">is-Ar</ta>
            <ta e="T576" id="Seg_3356" s="T574">olor-Ar-LAr</ta>
            <ta e="T578" id="Seg_3357" s="T577">is-A</ta>
            <ta e="T579" id="Seg_3358" s="T578">olor-Ar</ta>
            <ta e="T580" id="Seg_3359" s="T579">onton</ta>
            <ta e="T581" id="Seg_3360" s="T580">bu</ta>
            <ta e="T582" id="Seg_3361" s="T581">kepseː-Ar-LAr</ta>
            <ta e="T583" id="Seg_3362" s="T582">gini</ta>
            <ta e="T584" id="Seg_3363" s="T583">kɨjgan-Ar</ta>
            <ta e="T586" id="Seg_3364" s="T585">kɨjgan-An</ta>
            <ta e="T587" id="Seg_3365" s="T586">baran</ta>
            <ta e="T588" id="Seg_3366" s="T587">dʼaktar-GA</ta>
            <ta e="T589" id="Seg_3367" s="T588">bar-Ar</ta>
            <ta e="T590" id="Seg_3368" s="T589">ol</ta>
            <ta e="T591" id="Seg_3369" s="T590">baːr</ta>
            <ta e="T592" id="Seg_3370" s="T591">egel</ta>
            <ta e="T593" id="Seg_3371" s="T592">ol-nI</ta>
            <ta e="T604" id="Seg_3372" s="T601">kulaːkɨ-tI-n</ta>
            <ta e="T608" id="Seg_3373" s="T604">belemneː-A-n-BIT</ta>
            <ta e="T612" id="Seg_3374" s="T610">da</ta>
            <ta e="T613" id="Seg_3375" s="T612">da</ta>
            <ta e="T615" id="Seg_3376" s="T613">da</ta>
            <ta e="T616" id="Seg_3377" s="T615">da</ta>
            <ta e="T618" id="Seg_3378" s="T616">da</ta>
            <ta e="T626" id="Seg_3379" s="T624">üčügej</ta>
            <ta e="T627" id="Seg_3380" s="T626">bu</ta>
            <ta e="T629" id="Seg_3381" s="T627">baːr</ta>
            <ta e="T630" id="Seg_3382" s="T629">üleleː-A</ta>
            <ta e="T631" id="Seg_3383" s="T630">hɨrɨt-Ar-LAr</ta>
            <ta e="T633" id="Seg_3384" s="T631">olor-Ar</ta>
            <ta e="T634" id="Seg_3385" s="T633">e-TI-LArA</ta>
            <ta e="T636" id="Seg_3386" s="T634">eni</ta>
            <ta e="T637" id="Seg_3387" s="T636">kör</ta>
            <ta e="T638" id="Seg_3388" s="T637">bu</ta>
            <ta e="T640" id="Seg_3389" s="T638">vmʼestʼe</ta>
            <ta e="T649" id="Seg_3390" s="T647">bul</ta>
            <ta e="T651" id="Seg_3391" s="T649">beːbe</ta>
            <ta e="T656" id="Seg_3392" s="T654">bu</ta>
            <ta e="T658" id="Seg_3393" s="T656">baːr</ta>
            <ta e="T660" id="Seg_3394" s="T658">bu</ta>
            <ta e="T662" id="Seg_3395" s="T660">baːr</ta>
            <ta e="T664" id="Seg_3396" s="T662">bu-nI</ta>
            <ta e="T665" id="Seg_3397" s="T664">bu-nI</ta>
            <ta e="T667" id="Seg_3398" s="T665">daprašɨvaj-LAː-Ar-LAr</ta>
            <ta e="T670" id="Seg_3399" s="T669">bu</ta>
            <ta e="T671" id="Seg_3400" s="T670">baːr-LAr</ta>
            <ta e="T672" id="Seg_3401" s="T671">daprašɨvaj-LAː-Ar-LAr</ta>
            <ta e="T676" id="Seg_3402" s="T673">taŋas</ta>
            <ta e="T677" id="Seg_3403" s="T676">bi͡er-Ar-LAr</ta>
            <ta e="T679" id="Seg_3404" s="T678">kaːj-I-LIN-I-BIT</ta>
            <ta e="T680" id="Seg_3405" s="T679">olor-Ar</ta>
            <ta e="T681" id="Seg_3406" s="T680">hanaː-GA</ta>
            <ta e="T682" id="Seg_3407" s="T681">battaː-A-t-BIT</ta>
            <ta e="T684" id="Seg_3408" s="T683">bu</ta>
            <ta e="T685" id="Seg_3409" s="T684">emi͡e</ta>
            <ta e="T686" id="Seg_3410" s="T685">hanaː-GA</ta>
            <ta e="T687" id="Seg_3411" s="T686">battaː-A-t-BIT</ta>
            <ta e="T688" id="Seg_3412" s="T687">olor-Ar</ta>
            <ta e="T690" id="Seg_3413" s="T689">bu</ta>
            <ta e="T691" id="Seg_3414" s="T690">bu͡o</ta>
            <ta e="T692" id="Seg_3415" s="T691">uže</ta>
            <ta e="T693" id="Seg_3416" s="T692">mʼečʼtaj-LAː-A</ta>
            <ta e="T694" id="Seg_3417" s="T693">olor-Ar</ta>
            <ta e="T695" id="Seg_3418" s="T694">navʼernaje</ta>
            <ta e="T696" id="Seg_3419" s="T695">ogo-tA</ta>
            <ta e="T697" id="Seg_3420" s="T696">fstrʼečʼaj-LAː-Ar-tI-n</ta>
            <ta e="T704" id="Seg_3421" s="T702">iti</ta>
            <ta e="T705" id="Seg_3422" s="T704">kördük</ta>
            <ta e="T709" id="Seg_3423" s="T708">da</ta>
            <ta e="T711" id="Seg_3424" s="T709">uže</ta>
            <ta e="T713" id="Seg_3425" s="T711">prʼedstavlʼaj-LAː-Ar</ta>
            <ta e="T714" id="Seg_3426" s="T713">dʼi͡e-tI-GAr</ta>
            <ta e="T715" id="Seg_3427" s="T714">kel-Ar-tI-n</ta>
            <ta e="T717" id="Seg_3428" s="T715">navʼernaje</ta>
            <ta e="T725" id="Seg_3429" s="T723">onton</ta>
            <ta e="T727" id="Seg_3430" s="T725">ol</ta>
            <ta e="T729" id="Seg_3431" s="T727">tagɨs-I-BIT</ta>
            <ta e="T735" id="Seg_3432" s="T734">tagɨs-I-BIT</ta>
            <ta e="T740" id="Seg_3433" s="T739">tagɨs-I-BIT</ta>
            <ta e="T741" id="Seg_3434" s="T740">kel-BIT</ta>
            <ta e="T742" id="Seg_3435" s="T741">ü͡ör-A-ü͡ör-A</ta>
            <ta e="T743" id="Seg_3436" s="T742">radastnɨj</ta>
            <ta e="T745" id="Seg_3437" s="T744">onton</ta>
            <ta e="T746" id="Seg_3438" s="T745">kepseː-A</ta>
            <ta e="T747" id="Seg_3439" s="T746">olor-Ar</ta>
            <ta e="T749" id="Seg_3440" s="T748">onton</ta>
            <ta e="T756" id="Seg_3441" s="T752">is-BAT</ta>
            <ta e="T758" id="Seg_3442" s="T756">bu͡ol-BIT</ta>
         </annotation>
         <annotation name="ge" tierref="ge-ChGS">
            <ta e="T63" id="Seg_3443" s="T61">where=Q</ta>
            <ta e="T65" id="Seg_3444" s="T63">first.[NOM]</ta>
            <ta e="T70" id="Seg_3445" s="T69">end.[NOM]</ta>
            <ta e="T72" id="Seg_3446" s="T70">EMPH</ta>
            <ta e="T74" id="Seg_3447" s="T72">EMPH</ta>
            <ta e="T78" id="Seg_3448" s="T77">come-PST2.[3SG]</ta>
            <ta e="T83" id="Seg_3449" s="T82">that.[NOM]</ta>
            <ta e="T85" id="Seg_3450" s="T83">also</ta>
            <ta e="T87" id="Seg_3451" s="T85">there</ta>
            <ta e="T88" id="Seg_3452" s="T87">sit-PRS.[3SG]</ta>
            <ta e="T94" id="Seg_3453" s="T92">here</ta>
            <ta e="T96" id="Seg_3454" s="T94">probably</ta>
            <ta e="T99" id="Seg_3455" s="T96">every-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_3456" s="T105">AFFIRM</ta>
            <ta e="T110" id="Seg_3457" s="T109">wait</ta>
            <ta e="T111" id="Seg_3458" s="T110">that.[NOM]</ta>
            <ta e="T112" id="Seg_3459" s="T111">stand-PTCP.PRS</ta>
            <ta e="T114" id="Seg_3460" s="T112">fall-IMP.3SG</ta>
            <ta e="T116" id="Seg_3461" s="T114">see-IMP.1DU</ta>
            <ta e="T117" id="Seg_3462" s="T116">this-PL-ACC</ta>
            <ta e="T119" id="Seg_3463" s="T117">lay-FREQ.[IMP.2SG]</ta>
            <ta e="T120" id="Seg_3464" s="T119">well</ta>
            <ta e="T122" id="Seg_3465" s="T120">whole-3SG-ACC</ta>
            <ta e="T125" id="Seg_3466" s="T122">spirit-VBZ-PTCP.PST-3PL-ACC</ta>
            <ta e="T127" id="Seg_3467" s="T125">and.so.on-PTCP.PST-3PL-ACC</ta>
            <ta e="T134" id="Seg_3468" s="T133">this</ta>
            <ta e="T135" id="Seg_3469" s="T134">EMPH</ta>
            <ta e="T136" id="Seg_3470" s="T135">there.is</ta>
            <ta e="T137" id="Seg_3471" s="T136">EMPH</ta>
            <ta e="T154" id="Seg_3472" s="T153">then</ta>
            <ta e="T155" id="Seg_3473" s="T154">come-CVB.SEQ</ta>
            <ta e="T156" id="Seg_3474" s="T155">this</ta>
            <ta e="T158" id="Seg_3475" s="T156">money-3SG-ACC</ta>
            <ta e="T160" id="Seg_3476" s="T158">whole-3SG-ACC</ta>
            <ta e="T161" id="Seg_3477" s="T160">spirit.[NOM]</ta>
            <ta e="T163" id="Seg_3478" s="T161">make-PST2-3SG</ta>
            <ta e="T167" id="Seg_3479" s="T166">say-PRS-2SG</ta>
            <ta e="T169" id="Seg_3480" s="T167">Q</ta>
            <ta e="T176" id="Seg_3481" s="T175">at.first</ta>
            <ta e="T177" id="Seg_3482" s="T176">drink-NEG.PTCP</ta>
            <ta e="T178" id="Seg_3483" s="T177">be-PST1-3SG</ta>
            <ta e="T179" id="Seg_3484" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_3485" s="T179">Q</ta>
            <ta e="T184" id="Seg_3486" s="T182">come-CVB.SEQ</ta>
            <ta e="T186" id="Seg_3487" s="T184">go-CVB.SEQ</ta>
            <ta e="T188" id="Seg_3488" s="T186">drink-NEG.[3SG]</ta>
            <ta e="T190" id="Seg_3489" s="T188">MOD</ta>
            <ta e="T192" id="Seg_3490" s="T190">here</ta>
            <ta e="T194" id="Seg_3491" s="T192">already</ta>
            <ta e="T196" id="Seg_3492" s="T194">child-3SG.[NOM]</ta>
            <ta e="T198" id="Seg_3493" s="T196">this</ta>
            <ta e="T200" id="Seg_3494" s="T198">grow-PTCP.PST-3SG-ACC</ta>
            <ta e="T203" id="Seg_3495" s="T200">after</ta>
            <ta e="T207" id="Seg_3496" s="T204">there</ta>
            <ta e="T210" id="Seg_3497" s="T207">small.[NOM]</ta>
            <ta e="T212" id="Seg_3498" s="T211">child-3SG.[NOM]</ta>
            <ta e="T213" id="Seg_3499" s="T212">small.[NOM]</ta>
            <ta e="T215" id="Seg_3500" s="T214">come-CVB.SEQ</ta>
            <ta e="T216" id="Seg_3501" s="T215">after</ta>
            <ta e="T217" id="Seg_3502" s="T216">tell-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_3503" s="T217">Q</ta>
            <ta e="T221" id="Seg_3504" s="T219">this</ta>
            <ta e="T222" id="Seg_3505" s="T221">come-CVB.SEQ</ta>
            <ta e="T224" id="Seg_3506" s="T222">after</ta>
            <ta e="T226" id="Seg_3507" s="T224">tell-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_3508" s="T226">apparently</ta>
            <ta e="T229" id="Seg_3509" s="T227">how</ta>
            <ta e="T231" id="Seg_3510" s="T229">sit-CVB.SIM</ta>
            <ta e="T232" id="Seg_3511" s="T231">sit-PTCP.PST-3SG-ACC</ta>
            <ta e="T234" id="Seg_3512" s="T233">listen-CVB.SIM</ta>
            <ta e="T235" id="Seg_3513" s="T234">live-PRS-3PL</ta>
            <ta e="T236" id="Seg_3514" s="T235">Q</ta>
            <ta e="T239" id="Seg_3515" s="T237">wait</ta>
            <ta e="T248" id="Seg_3516" s="T247">this</ta>
            <ta e="T250" id="Seg_3517" s="T248">there.is</ta>
            <ta e="T251" id="Seg_3518" s="T250">this</ta>
            <ta e="T253" id="Seg_3519" s="T251">there.is</ta>
            <ta e="T254" id="Seg_3520" s="T253">this</ta>
            <ta e="T256" id="Seg_3521" s="T254">there.is</ta>
            <ta e="T259" id="Seg_3522" s="T258">wait</ta>
            <ta e="T260" id="Seg_3523" s="T259">first-1PL-ACC</ta>
            <ta e="T261" id="Seg_3524" s="T260">find-PTCP.FUT-1PL-ACC</ta>
            <ta e="T262" id="Seg_3525" s="T261">need.to</ta>
            <ta e="T265" id="Seg_3526" s="T263">this</ta>
            <ta e="T266" id="Seg_3527" s="T265">well</ta>
            <ta e="T267" id="Seg_3528" s="T266">just</ta>
            <ta e="T268" id="Seg_3529" s="T267">prison-ABL</ta>
            <ta e="T270" id="Seg_3530" s="T268">go.out-EP-PST2.[3SG]</ta>
            <ta e="T272" id="Seg_3531" s="T270">apparently</ta>
            <ta e="T273" id="Seg_3532" s="T272">EMPH</ta>
            <ta e="T274" id="Seg_3533" s="T273">this</ta>
            <ta e="T276" id="Seg_3534" s="T274">be.happy-PRS.[3SG]</ta>
            <ta e="T279" id="Seg_3535" s="T278">prison-DAT/LOC</ta>
            <ta e="T280" id="Seg_3536" s="T279">EMPH</ta>
            <ta e="T282" id="Seg_3537" s="T280">3SG.[NOM]</ta>
            <ta e="T284" id="Seg_3538" s="T282">it.seems</ta>
            <ta e="T285" id="Seg_3539" s="T284">MOD</ta>
            <ta e="T289" id="Seg_3540" s="T286">work-PRS.[3SG]</ta>
            <ta e="T291" id="Seg_3541" s="T289">Q</ta>
            <ta e="T313" id="Seg_3542" s="T312">love-PTCP.PRS</ta>
            <ta e="T314" id="Seg_3543" s="T313">child-3SG.[NOM]</ta>
            <ta e="T315" id="Seg_3544" s="T314">that.[NOM]</ta>
            <ta e="T316" id="Seg_3545" s="T315">similar</ta>
            <ta e="T317" id="Seg_3546" s="T316">run-CVB.SIM-run-CVB.SIM</ta>
            <ta e="T318" id="Seg_3547" s="T317">come-PTCP.FUT</ta>
            <ta e="T319" id="Seg_3548" s="T318">be-PST1-3SG</ta>
            <ta e="T320" id="Seg_3549" s="T319">3SG-DAT/LOC</ta>
            <ta e="T328" id="Seg_3550" s="T326">here</ta>
            <ta e="T329" id="Seg_3551" s="T328">probably</ta>
            <ta e="T331" id="Seg_3552" s="T329">see.[IMP.2SG]</ta>
            <ta e="T332" id="Seg_3553" s="T331">this</ta>
            <ta e="T333" id="Seg_3554" s="T332">spirit-VBZ-CVB.SIM</ta>
            <ta e="T335" id="Seg_3555" s="T333">sit-CVB.SEQ</ta>
            <ta e="T336" id="Seg_3556" s="T335">this</ta>
            <ta e="T338" id="Seg_3557" s="T336">spirit-VBZ-CVB.SIM</ta>
            <ta e="T339" id="Seg_3558" s="T338">sit-CVB.SEQ-3PL</ta>
            <ta e="T340" id="Seg_3559" s="T339">chat-CVB.SIM</ta>
            <ta e="T342" id="Seg_3560" s="T341">this</ta>
            <ta e="T343" id="Seg_3561" s="T342">human.being.[NOM]</ta>
            <ta e="T344" id="Seg_3562" s="T343">tell-PRS.[3SG]</ta>
            <ta e="T345" id="Seg_3563" s="T344">say-CVB.SIM</ta>
            <ta e="T346" id="Seg_3564" s="T345">woman-2SG-ACC</ta>
            <ta e="T347" id="Seg_3565" s="T346">see-PST2-EP-1SG</ta>
            <ta e="T348" id="Seg_3566" s="T347">say-PRS.[3SG]</ta>
            <ta e="T349" id="Seg_3567" s="T348">EMPH</ta>
            <ta e="T351" id="Seg_3568" s="T350">get.angry-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_3569" s="T351">already</ta>
            <ta e="T353" id="Seg_3570" s="T352">EMPH</ta>
            <ta e="T356" id="Seg_3571" s="T354">from.there</ta>
            <ta e="T358" id="Seg_3572" s="T356">come-CVB.SEQ</ta>
            <ta e="T360" id="Seg_3573" s="T358">well</ta>
            <ta e="T362" id="Seg_3574" s="T360">from.there</ta>
            <ta e="T364" id="Seg_3575" s="T362">come-CVB.SEQ</ta>
            <ta e="T366" id="Seg_3576" s="T364">after</ta>
            <ta e="T367" id="Seg_3577" s="T366">relationship.[NOM]</ta>
            <ta e="T368" id="Seg_3578" s="T367">clarify-VBZ-PRS.[3SG]</ta>
            <ta e="T369" id="Seg_3579" s="T368">EMPH</ta>
            <ta e="T370" id="Seg_3580" s="T369">thrash-CVB.PURP</ta>
            <ta e="T371" id="Seg_3581" s="T370">want-PRS.[3SG]</ta>
            <ta e="T372" id="Seg_3582" s="T371">that</ta>
            <ta e="T373" id="Seg_3583" s="T372">there.is</ta>
            <ta e="T376" id="Seg_3584" s="T375">then</ta>
            <ta e="T377" id="Seg_3585" s="T376">this</ta>
            <ta e="T378" id="Seg_3586" s="T377">already</ta>
            <ta e="T379" id="Seg_3587" s="T378">thrash-PST2.[3SG]</ta>
            <ta e="T380" id="Seg_3588" s="T379">beat-EP-PST2.[3SG]</ta>
            <ta e="T381" id="Seg_3589" s="T380">already</ta>
            <ta e="T386" id="Seg_3590" s="T385">this.[NOM]</ta>
            <ta e="T387" id="Seg_3591" s="T386">similar</ta>
            <ta e="T389" id="Seg_3592" s="T387">maybe</ta>
            <ta e="T390" id="Seg_3593" s="T389">yes</ta>
            <ta e="T393" id="Seg_3594" s="T391">bring.[IMP.2SG]</ta>
            <ta e="T395" id="Seg_3595" s="T393">closer</ta>
            <ta e="T398" id="Seg_3596" s="T397">bring.[IMP.2SG]</ta>
            <ta e="T399" id="Seg_3597" s="T398">one</ta>
            <ta e="T404" id="Seg_3598" s="T402">this</ta>
            <ta e="T405" id="Seg_3599" s="T404">there.is</ta>
            <ta e="T407" id="Seg_3600" s="T405">first.[NOM]</ta>
            <ta e="T408" id="Seg_3601" s="T407">this</ta>
            <ta e="T409" id="Seg_3602" s="T408">there.is</ta>
            <ta e="T411" id="Seg_3603" s="T409">well</ta>
            <ta e="T429" id="Seg_3604" s="T427">no</ta>
            <ta e="T430" id="Seg_3605" s="T429">this</ta>
            <ta e="T431" id="Seg_3606" s="T430">already</ta>
            <ta e="T432" id="Seg_3607" s="T431">end-3SG-DAT/LOC</ta>
            <ta e="T433" id="Seg_3608" s="T432">drink-NEG-1SG</ta>
            <ta e="T434" id="Seg_3609" s="T433">say-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_3610" s="T434">already</ta>
            <ta e="T436" id="Seg_3611" s="T435">two</ta>
            <ta e="T437" id="Seg_3612" s="T436">last.[NOM]</ta>
            <ta e="T438" id="Seg_3613" s="T437">come-CVB.SEQ</ta>
            <ta e="T439" id="Seg_3614" s="T438">go-PRS.[3SG]</ta>
            <ta e="T441" id="Seg_3615" s="T439">prison-ABL</ta>
            <ta e="T442" id="Seg_3616" s="T441">come-CVB.SEQ</ta>
            <ta e="T443" id="Seg_3617" s="T442">after</ta>
            <ta e="T449" id="Seg_3618" s="T448">AFFIRM</ta>
            <ta e="T454" id="Seg_3619" s="T453">drink-NEG.[3SG]</ta>
            <ta e="T464" id="Seg_3620" s="T461">good.[NOM]</ta>
            <ta e="T466" id="Seg_3621" s="T464">very</ta>
            <ta e="T468" id="Seg_3622" s="T466">live-PTCP.PRS</ta>
            <ta e="T469" id="Seg_3623" s="T468">be-PST1-3PL</ta>
            <ta e="T471" id="Seg_3624" s="T469">live-PTCP.PRS</ta>
            <ta e="T473" id="Seg_3625" s="T471">be-PST1-3PL</ta>
            <ta e="T475" id="Seg_3626" s="T473">apparently</ta>
            <ta e="T477" id="Seg_3627" s="T475">work-CVB.SIM</ta>
            <ta e="T478" id="Seg_3628" s="T477">go-PRS-3PL</ta>
            <ta e="T485" id="Seg_3629" s="T484">well</ta>
            <ta e="T486" id="Seg_3630" s="T485">that.[NOM]</ta>
            <ta e="T487" id="Seg_3631" s="T486">already</ta>
            <ta e="T488" id="Seg_3632" s="T487">prison-3SG-ACC</ta>
            <ta e="T489" id="Seg_3633" s="T488">tell-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_3634" s="T489">EMPH</ta>
            <ta e="T494" id="Seg_3635" s="T491">3SG.[NOM]</ta>
            <ta e="T496" id="Seg_3636" s="T494">later-INTNS</ta>
            <ta e="T498" id="Seg_3637" s="T496">apparently</ta>
            <ta e="T499" id="Seg_3638" s="T498">this</ta>
            <ta e="T501" id="Seg_3639" s="T499">this</ta>
            <ta e="T502" id="Seg_3640" s="T501">back-3SG-ABL</ta>
            <ta e="T503" id="Seg_3641" s="T502">this.[NOM]</ta>
            <ta e="T505" id="Seg_3642" s="T503">similar</ta>
            <ta e="T506" id="Seg_3643" s="T505">be-FUT-3SG</ta>
            <ta e="T511" id="Seg_3644" s="T510">AFFIRM</ta>
            <ta e="T515" id="Seg_3645" s="T514">that.[NOM]</ta>
            <ta e="T516" id="Seg_3646" s="T515">similar</ta>
            <ta e="T544" id="Seg_3647" s="T543">well</ta>
            <ta e="T545" id="Seg_3648" s="T544">this</ta>
            <ta e="T546" id="Seg_3649" s="T545">there.is-3PL</ta>
            <ta e="T547" id="Seg_3650" s="T546">say-PRS.[3SG]</ta>
            <ta e="T548" id="Seg_3651" s="T547">at.first</ta>
            <ta e="T549" id="Seg_3652" s="T548">work-CVB.SIM</ta>
            <ta e="T550" id="Seg_3653" s="T549">go-CVB.SEQ</ta>
            <ta e="T551" id="Seg_3654" s="T550">EMPH</ta>
            <ta e="T552" id="Seg_3655" s="T551">this</ta>
            <ta e="T553" id="Seg_3656" s="T552">spirit-PHIL-3PL.[NOM]</ta>
            <ta e="T554" id="Seg_3657" s="T553">who-VBZ-PST2.[3SG]</ta>
            <ta e="T559" id="Seg_3658" s="T554">get.into-PST2.[3SG]</ta>
            <ta e="T565" id="Seg_3659" s="T564">well</ta>
            <ta e="T566" id="Seg_3660" s="T565">this</ta>
            <ta e="T567" id="Seg_3661" s="T566">there.is-3PL</ta>
            <ta e="T568" id="Seg_3662" s="T567">EMPH</ta>
            <ta e="T572" id="Seg_3663" s="T570">this</ta>
            <ta e="T574" id="Seg_3664" s="T572">drink-PTCP.PRS</ta>
            <ta e="T576" id="Seg_3665" s="T574">sit-PRS-3PL</ta>
            <ta e="T578" id="Seg_3666" s="T577">drink-CVB.SIM</ta>
            <ta e="T579" id="Seg_3667" s="T578">sit-PRS.[3SG]</ta>
            <ta e="T580" id="Seg_3668" s="T579">then</ta>
            <ta e="T581" id="Seg_3669" s="T580">this</ta>
            <ta e="T582" id="Seg_3670" s="T581">tell-PRS-3PL</ta>
            <ta e="T583" id="Seg_3671" s="T582">3SG.[NOM]</ta>
            <ta e="T584" id="Seg_3672" s="T583">get.angry-PRS.[3SG]</ta>
            <ta e="T586" id="Seg_3673" s="T585">get.angry-CVB.SEQ</ta>
            <ta e="T587" id="Seg_3674" s="T586">after</ta>
            <ta e="T588" id="Seg_3675" s="T587">woman-DAT/LOC</ta>
            <ta e="T589" id="Seg_3676" s="T588">go-PRS.[3SG]</ta>
            <ta e="T590" id="Seg_3677" s="T589">that</ta>
            <ta e="T591" id="Seg_3678" s="T590">there.is</ta>
            <ta e="T592" id="Seg_3679" s="T591">bring.[IMP.2SG]</ta>
            <ta e="T593" id="Seg_3680" s="T592">that-ACC</ta>
            <ta e="T604" id="Seg_3681" s="T601">fists-3SG-ACC</ta>
            <ta e="T608" id="Seg_3682" s="T604">prepare-EP-MED-PST2.[3SG]</ta>
            <ta e="T612" id="Seg_3683" s="T610">yes</ta>
            <ta e="T613" id="Seg_3684" s="T612">yes</ta>
            <ta e="T615" id="Seg_3685" s="T613">yes</ta>
            <ta e="T616" id="Seg_3686" s="T615">yes</ta>
            <ta e="T618" id="Seg_3687" s="T616">yes</ta>
            <ta e="T626" id="Seg_3688" s="T624">good.[NOM]</ta>
            <ta e="T627" id="Seg_3689" s="T626">this</ta>
            <ta e="T629" id="Seg_3690" s="T627">there.is</ta>
            <ta e="T630" id="Seg_3691" s="T629">work-CVB.SIM</ta>
            <ta e="T631" id="Seg_3692" s="T630">go-PRS-3PL</ta>
            <ta e="T633" id="Seg_3693" s="T631">live-PTCP.PRS</ta>
            <ta e="T634" id="Seg_3694" s="T633">be-PST1-3PL</ta>
            <ta e="T636" id="Seg_3695" s="T634">apparently</ta>
            <ta e="T637" id="Seg_3696" s="T636">see.[IMP.2SG]</ta>
            <ta e="T638" id="Seg_3697" s="T637">this</ta>
            <ta e="T640" id="Seg_3698" s="T638">together</ta>
            <ta e="T649" id="Seg_3699" s="T647">find.[IMP.2SG]</ta>
            <ta e="T651" id="Seg_3700" s="T649">wait</ta>
            <ta e="T656" id="Seg_3701" s="T654">this</ta>
            <ta e="T658" id="Seg_3702" s="T656">there.is</ta>
            <ta e="T660" id="Seg_3703" s="T658">this</ta>
            <ta e="T662" id="Seg_3704" s="T660">there.is</ta>
            <ta e="T664" id="Seg_3705" s="T662">this-ACC</ta>
            <ta e="T665" id="Seg_3706" s="T664">this-ACC</ta>
            <ta e="T667" id="Seg_3707" s="T665">question-VBZ-PRS-3PL</ta>
            <ta e="T670" id="Seg_3708" s="T669">this</ta>
            <ta e="T671" id="Seg_3709" s="T670">there.is-3PL</ta>
            <ta e="T672" id="Seg_3710" s="T671">question-VBZ-PRS-3PL</ta>
            <ta e="T676" id="Seg_3711" s="T673">clothes.[NOM]</ta>
            <ta e="T677" id="Seg_3712" s="T676">give-PRS-3PL</ta>
            <ta e="T679" id="Seg_3713" s="T678">block-EP-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T680" id="Seg_3714" s="T679">sit-PRS.[3SG]</ta>
            <ta e="T681" id="Seg_3715" s="T680">thought-DAT/LOC</ta>
            <ta e="T682" id="Seg_3716" s="T681">weigh-EP-PASS-PTCP.PST.[NOM]</ta>
            <ta e="T684" id="Seg_3717" s="T683">this</ta>
            <ta e="T685" id="Seg_3718" s="T684">also</ta>
            <ta e="T686" id="Seg_3719" s="T685">thought-DAT/LOC</ta>
            <ta e="T687" id="Seg_3720" s="T686">weigh-EP-PASS-PTCP.PST.[NOM]</ta>
            <ta e="T688" id="Seg_3721" s="T687">sit-PRS.[3SG]</ta>
            <ta e="T690" id="Seg_3722" s="T689">this</ta>
            <ta e="T691" id="Seg_3723" s="T690">EMPH</ta>
            <ta e="T692" id="Seg_3724" s="T691">already</ta>
            <ta e="T693" id="Seg_3725" s="T692">dream-VBZ-CVB.SIM</ta>
            <ta e="T694" id="Seg_3726" s="T693">sit-PRS.[3SG]</ta>
            <ta e="T695" id="Seg_3727" s="T694">probably</ta>
            <ta e="T696" id="Seg_3728" s="T695">child-3SG.[NOM]</ta>
            <ta e="T697" id="Seg_3729" s="T696">meet-VBZ-PTCP.PRS-3SG-ACC</ta>
            <ta e="T704" id="Seg_3730" s="T702">that.[NOM]</ta>
            <ta e="T705" id="Seg_3731" s="T704">similar</ta>
            <ta e="T709" id="Seg_3732" s="T708">yes</ta>
            <ta e="T711" id="Seg_3733" s="T709">already</ta>
            <ta e="T713" id="Seg_3734" s="T711">imagine-VBZ-PRS.[3SG]</ta>
            <ta e="T714" id="Seg_3735" s="T713">house-3SG-DAT/LOC</ta>
            <ta e="T715" id="Seg_3736" s="T714">come-PTCP.PRS-3SG-ACC</ta>
            <ta e="T717" id="Seg_3737" s="T715">probably</ta>
            <ta e="T725" id="Seg_3738" s="T723">then</ta>
            <ta e="T727" id="Seg_3739" s="T725">that</ta>
            <ta e="T729" id="Seg_3740" s="T727">go.out-EP-PST2.[3SG]</ta>
            <ta e="T735" id="Seg_3741" s="T734">go.out-EP-PST2.[3SG]</ta>
            <ta e="T740" id="Seg_3742" s="T739">go.out-EP-PST2.[3SG]</ta>
            <ta e="T741" id="Seg_3743" s="T740">come-PST2.[3SG]</ta>
            <ta e="T742" id="Seg_3744" s="T741">be.happy-CVB.SIM-be.happy-CVB.SIM</ta>
            <ta e="T743" id="Seg_3745" s="T742">happy.[NOM]</ta>
            <ta e="T745" id="Seg_3746" s="T744">then</ta>
            <ta e="T746" id="Seg_3747" s="T745">tell-CVB.SIM</ta>
            <ta e="T747" id="Seg_3748" s="T746">sit-PRS.[3SG]</ta>
            <ta e="T749" id="Seg_3749" s="T748">then</ta>
            <ta e="T756" id="Seg_3750" s="T752">drink-NEG.PTCP</ta>
            <ta e="T758" id="Seg_3751" s="T756">become-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-ChGS">
            <ta e="T63" id="Seg_3752" s="T61">wo=Q</ta>
            <ta e="T65" id="Seg_3753" s="T63">erster.[NOM]</ta>
            <ta e="T70" id="Seg_3754" s="T69">Ende.[NOM]</ta>
            <ta e="T72" id="Seg_3755" s="T70">EMPH</ta>
            <ta e="T74" id="Seg_3756" s="T72">EMPH</ta>
            <ta e="T78" id="Seg_3757" s="T77">kommen-PST2.[3SG]</ta>
            <ta e="T83" id="Seg_3758" s="T82">dieses.[NOM]</ta>
            <ta e="T85" id="Seg_3759" s="T83">auch</ta>
            <ta e="T87" id="Seg_3760" s="T85">dort</ta>
            <ta e="T88" id="Seg_3761" s="T87">sitzen-PRS.[3SG]</ta>
            <ta e="T94" id="Seg_3762" s="T92">hier</ta>
            <ta e="T96" id="Seg_3763" s="T94">wahrscheinlich</ta>
            <ta e="T99" id="Seg_3764" s="T96">jeder-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_3765" s="T105">AFFIRM</ta>
            <ta e="T110" id="Seg_3766" s="T109">warte</ta>
            <ta e="T111" id="Seg_3767" s="T110">dieses.[NOM]</ta>
            <ta e="T112" id="Seg_3768" s="T111">stehen-PTCP.PRS</ta>
            <ta e="T114" id="Seg_3769" s="T112">fallen-IMP.3SG</ta>
            <ta e="T116" id="Seg_3770" s="T114">sehen-IMP.1DU</ta>
            <ta e="T117" id="Seg_3771" s="T116">dieses-PL-ACC</ta>
            <ta e="T119" id="Seg_3772" s="T117">legen-FREQ.[IMP.2SG]</ta>
            <ta e="T120" id="Seg_3773" s="T119">doch</ta>
            <ta e="T122" id="Seg_3774" s="T120">ganz-3SG-ACC</ta>
            <ta e="T125" id="Seg_3775" s="T122">Schnaps-VBZ-PTCP.PST-3PL-ACC</ta>
            <ta e="T127" id="Seg_3776" s="T125">und.so.weiter-PTCP.PST-3PL-ACC</ta>
            <ta e="T134" id="Seg_3777" s="T133">dieses</ta>
            <ta e="T135" id="Seg_3778" s="T134">EMPH</ta>
            <ta e="T136" id="Seg_3779" s="T135">es.gibt</ta>
            <ta e="T137" id="Seg_3780" s="T136">EMPH</ta>
            <ta e="T154" id="Seg_3781" s="T153">dann</ta>
            <ta e="T155" id="Seg_3782" s="T154">kommen-CVB.SEQ</ta>
            <ta e="T156" id="Seg_3783" s="T155">dieses</ta>
            <ta e="T158" id="Seg_3784" s="T156">Geld-3SG-ACC</ta>
            <ta e="T160" id="Seg_3785" s="T158">ganz-3SG-ACC</ta>
            <ta e="T161" id="Seg_3786" s="T160">Schnaps.[NOM]</ta>
            <ta e="T163" id="Seg_3787" s="T161">machen-PST2-3SG</ta>
            <ta e="T167" id="Seg_3788" s="T166">sagen-PRS-2SG</ta>
            <ta e="T169" id="Seg_3789" s="T167">Q</ta>
            <ta e="T176" id="Seg_3790" s="T175">zuerst</ta>
            <ta e="T177" id="Seg_3791" s="T176">trinken-NEG.PTCP</ta>
            <ta e="T178" id="Seg_3792" s="T177">sein-PST1-3SG</ta>
            <ta e="T179" id="Seg_3793" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_3794" s="T179">Q</ta>
            <ta e="T184" id="Seg_3795" s="T182">kommen-CVB.SEQ</ta>
            <ta e="T186" id="Seg_3796" s="T184">gehen-CVB.SEQ</ta>
            <ta e="T188" id="Seg_3797" s="T186">trinken-NEG.[3SG]</ta>
            <ta e="T190" id="Seg_3798" s="T188">MOD</ta>
            <ta e="T192" id="Seg_3799" s="T190">hier</ta>
            <ta e="T194" id="Seg_3800" s="T192">schon</ta>
            <ta e="T196" id="Seg_3801" s="T194">Kind-3SG.[NOM]</ta>
            <ta e="T198" id="Seg_3802" s="T196">dieses</ta>
            <ta e="T200" id="Seg_3803" s="T198">wachsen-PTCP.PST-3SG-ACC</ta>
            <ta e="T203" id="Seg_3804" s="T200">nachdem</ta>
            <ta e="T207" id="Seg_3805" s="T204">dort</ta>
            <ta e="T210" id="Seg_3806" s="T207">klein.[NOM]</ta>
            <ta e="T212" id="Seg_3807" s="T211">Kind-3SG.[NOM]</ta>
            <ta e="T213" id="Seg_3808" s="T212">klein.[NOM]</ta>
            <ta e="T215" id="Seg_3809" s="T214">kommen-CVB.SEQ</ta>
            <ta e="T216" id="Seg_3810" s="T215">nachdem</ta>
            <ta e="T217" id="Seg_3811" s="T216">erzählen-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_3812" s="T217">Q</ta>
            <ta e="T221" id="Seg_3813" s="T219">dieses</ta>
            <ta e="T222" id="Seg_3814" s="T221">kommen-CVB.SEQ</ta>
            <ta e="T224" id="Seg_3815" s="T222">nachdem</ta>
            <ta e="T226" id="Seg_3816" s="T224">erzählen-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_3817" s="T226">offenbar</ta>
            <ta e="T229" id="Seg_3818" s="T227">wie</ta>
            <ta e="T231" id="Seg_3819" s="T229">sitzen-CVB.SIM</ta>
            <ta e="T232" id="Seg_3820" s="T231">sitzen-PTCP.PST-3SG-ACC</ta>
            <ta e="T234" id="Seg_3821" s="T233">zuhören-CVB.SIM</ta>
            <ta e="T235" id="Seg_3822" s="T234">leben-PRS-3PL</ta>
            <ta e="T236" id="Seg_3823" s="T235">Q</ta>
            <ta e="T239" id="Seg_3824" s="T237">warte</ta>
            <ta e="T248" id="Seg_3825" s="T247">dieses</ta>
            <ta e="T250" id="Seg_3826" s="T248">es.gibt</ta>
            <ta e="T251" id="Seg_3827" s="T250">dieses</ta>
            <ta e="T253" id="Seg_3828" s="T251">es.gibt</ta>
            <ta e="T254" id="Seg_3829" s="T253">dieses</ta>
            <ta e="T256" id="Seg_3830" s="T254">es.gibt</ta>
            <ta e="T259" id="Seg_3831" s="T258">warte</ta>
            <ta e="T260" id="Seg_3832" s="T259">erster-1PL-ACC</ta>
            <ta e="T261" id="Seg_3833" s="T260">finden-PTCP.FUT-1PL-ACC</ta>
            <ta e="T262" id="Seg_3834" s="T261">man.muss</ta>
            <ta e="T265" id="Seg_3835" s="T263">dieses</ta>
            <ta e="T266" id="Seg_3836" s="T265">nun</ta>
            <ta e="T267" id="Seg_3837" s="T266">gerade</ta>
            <ta e="T268" id="Seg_3838" s="T267">Gefängnis-ABL</ta>
            <ta e="T270" id="Seg_3839" s="T268">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T272" id="Seg_3840" s="T270">offenbar</ta>
            <ta e="T273" id="Seg_3841" s="T272">EMPH</ta>
            <ta e="T274" id="Seg_3842" s="T273">dieses</ta>
            <ta e="T276" id="Seg_3843" s="T274">sich.freuen-PRS.[3SG]</ta>
            <ta e="T279" id="Seg_3844" s="T278">Gefängnis-DAT/LOC</ta>
            <ta e="T280" id="Seg_3845" s="T279">EMPH</ta>
            <ta e="T282" id="Seg_3846" s="T280">3SG.[NOM]</ta>
            <ta e="T284" id="Seg_3847" s="T282">es.scheint</ta>
            <ta e="T285" id="Seg_3848" s="T284">MOD</ta>
            <ta e="T289" id="Seg_3849" s="T286">arbeiten-PRS.[3SG]</ta>
            <ta e="T291" id="Seg_3850" s="T289">Q</ta>
            <ta e="T313" id="Seg_3851" s="T312">lieben-PTCP.PRS</ta>
            <ta e="T314" id="Seg_3852" s="T313">Kind-3SG.[NOM]</ta>
            <ta e="T315" id="Seg_3853" s="T314">dieses.[NOM]</ta>
            <ta e="T316" id="Seg_3854" s="T315">ähnlich</ta>
            <ta e="T317" id="Seg_3855" s="T316">laufen-CVB.SIM-laufen-CVB.SIM</ta>
            <ta e="T318" id="Seg_3856" s="T317">kommen-PTCP.FUT</ta>
            <ta e="T319" id="Seg_3857" s="T318">sein-PST1-3SG</ta>
            <ta e="T320" id="Seg_3858" s="T319">3SG-DAT/LOC</ta>
            <ta e="T328" id="Seg_3859" s="T326">hier</ta>
            <ta e="T329" id="Seg_3860" s="T328">wahrscheinlich</ta>
            <ta e="T331" id="Seg_3861" s="T329">sehen.[IMP.2SG]</ta>
            <ta e="T332" id="Seg_3862" s="T331">dieses</ta>
            <ta e="T333" id="Seg_3863" s="T332">Schnaps-VBZ-CVB.SIM</ta>
            <ta e="T335" id="Seg_3864" s="T333">sitzen-CVB.SEQ</ta>
            <ta e="T336" id="Seg_3865" s="T335">dieses</ta>
            <ta e="T338" id="Seg_3866" s="T336">Schnaps-VBZ-CVB.SIM</ta>
            <ta e="T339" id="Seg_3867" s="T338">sitzen-CVB.SEQ-3PL</ta>
            <ta e="T340" id="Seg_3868" s="T339">sich.unterhalten-CVB.SIM</ta>
            <ta e="T342" id="Seg_3869" s="T341">dieses</ta>
            <ta e="T343" id="Seg_3870" s="T342">Mensch.[NOM]</ta>
            <ta e="T344" id="Seg_3871" s="T343">erzählen-PRS.[3SG]</ta>
            <ta e="T345" id="Seg_3872" s="T344">sagen-CVB.SIM</ta>
            <ta e="T346" id="Seg_3873" s="T345">Frau-2SG-ACC</ta>
            <ta e="T347" id="Seg_3874" s="T346">sehen-PST2-EP-1SG</ta>
            <ta e="T348" id="Seg_3875" s="T347">sagen-PRS.[3SG]</ta>
            <ta e="T349" id="Seg_3876" s="T348">EMPH</ta>
            <ta e="T351" id="Seg_3877" s="T350">böse.werden-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_3878" s="T351">schon</ta>
            <ta e="T353" id="Seg_3879" s="T352">EMPH</ta>
            <ta e="T356" id="Seg_3880" s="T354">von.dort</ta>
            <ta e="T358" id="Seg_3881" s="T356">kommen-CVB.SEQ</ta>
            <ta e="T360" id="Seg_3882" s="T358">doch</ta>
            <ta e="T362" id="Seg_3883" s="T360">von.dort</ta>
            <ta e="T364" id="Seg_3884" s="T362">kommen-CVB.SEQ</ta>
            <ta e="T366" id="Seg_3885" s="T364">nachdem</ta>
            <ta e="T367" id="Seg_3886" s="T366">Beziehung.[NOM]</ta>
            <ta e="T368" id="Seg_3887" s="T367">klären-VBZ-PRS.[3SG]</ta>
            <ta e="T369" id="Seg_3888" s="T368">EMPH</ta>
            <ta e="T370" id="Seg_3889" s="T369">prügeln-CVB.PURP</ta>
            <ta e="T371" id="Seg_3890" s="T370">wollen-PRS.[3SG]</ta>
            <ta e="T372" id="Seg_3891" s="T371">jenes</ta>
            <ta e="T373" id="Seg_3892" s="T372">es.gibt</ta>
            <ta e="T376" id="Seg_3893" s="T375">dann</ta>
            <ta e="T377" id="Seg_3894" s="T376">dieses</ta>
            <ta e="T378" id="Seg_3895" s="T377">schon</ta>
            <ta e="T379" id="Seg_3896" s="T378">prügeln-PST2.[3SG]</ta>
            <ta e="T380" id="Seg_3897" s="T379">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T381" id="Seg_3898" s="T380">schon</ta>
            <ta e="T386" id="Seg_3899" s="T385">dieses.[NOM]</ta>
            <ta e="T387" id="Seg_3900" s="T386">ähnlich</ta>
            <ta e="T389" id="Seg_3901" s="T387">vielleicht</ta>
            <ta e="T390" id="Seg_3902" s="T389">ja</ta>
            <ta e="T393" id="Seg_3903" s="T391">bringen.[IMP.2SG]</ta>
            <ta e="T395" id="Seg_3904" s="T393">näher</ta>
            <ta e="T398" id="Seg_3905" s="T397">bringen.[IMP.2SG]</ta>
            <ta e="T399" id="Seg_3906" s="T398">eins</ta>
            <ta e="T404" id="Seg_3907" s="T402">dieses</ta>
            <ta e="T405" id="Seg_3908" s="T404">es.gibt</ta>
            <ta e="T407" id="Seg_3909" s="T405">erster.[NOM]</ta>
            <ta e="T408" id="Seg_3910" s="T407">dieses</ta>
            <ta e="T409" id="Seg_3911" s="T408">es.gibt</ta>
            <ta e="T411" id="Seg_3912" s="T409">nun</ta>
            <ta e="T429" id="Seg_3913" s="T427">nein</ta>
            <ta e="T430" id="Seg_3914" s="T429">dieses</ta>
            <ta e="T431" id="Seg_3915" s="T430">schon</ta>
            <ta e="T432" id="Seg_3916" s="T431">Ende-3SG-DAT/LOC</ta>
            <ta e="T433" id="Seg_3917" s="T432">trinken-NEG-1SG</ta>
            <ta e="T434" id="Seg_3918" s="T433">sagen-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_3919" s="T434">schon</ta>
            <ta e="T436" id="Seg_3920" s="T435">zwei</ta>
            <ta e="T437" id="Seg_3921" s="T436">letzter.[NOM]</ta>
            <ta e="T438" id="Seg_3922" s="T437">kommen-CVB.SEQ</ta>
            <ta e="T439" id="Seg_3923" s="T438">gehen-PRS.[3SG]</ta>
            <ta e="T441" id="Seg_3924" s="T439">Gefängnis-ABL</ta>
            <ta e="T442" id="Seg_3925" s="T441">kommen-CVB.SEQ</ta>
            <ta e="T443" id="Seg_3926" s="T442">nachdem</ta>
            <ta e="T449" id="Seg_3927" s="T448">AFFIRM</ta>
            <ta e="T454" id="Seg_3928" s="T453">trinken-NEG.[3SG]</ta>
            <ta e="T464" id="Seg_3929" s="T461">gut.[NOM]</ta>
            <ta e="T466" id="Seg_3930" s="T464">sehr</ta>
            <ta e="T468" id="Seg_3931" s="T466">leben-PTCP.PRS</ta>
            <ta e="T469" id="Seg_3932" s="T468">sein-PST1-3PL</ta>
            <ta e="T471" id="Seg_3933" s="T469">leben-PTCP.PRS</ta>
            <ta e="T473" id="Seg_3934" s="T471">sein-PST1-3PL</ta>
            <ta e="T475" id="Seg_3935" s="T473">offenbar</ta>
            <ta e="T477" id="Seg_3936" s="T475">arbeiten-CVB.SIM</ta>
            <ta e="T478" id="Seg_3937" s="T477">gehen-PRS-3PL</ta>
            <ta e="T485" id="Seg_3938" s="T484">doch</ta>
            <ta e="T486" id="Seg_3939" s="T485">dieses.[NOM]</ta>
            <ta e="T487" id="Seg_3940" s="T486">schon</ta>
            <ta e="T488" id="Seg_3941" s="T487">Gefängnis-3SG-ACC</ta>
            <ta e="T489" id="Seg_3942" s="T488">erzählen-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_3943" s="T489">EMPH</ta>
            <ta e="T494" id="Seg_3944" s="T491">3SG.[NOM]</ta>
            <ta e="T496" id="Seg_3945" s="T494">später-INTNS</ta>
            <ta e="T498" id="Seg_3946" s="T496">offenbar</ta>
            <ta e="T499" id="Seg_3947" s="T498">dieses</ta>
            <ta e="T501" id="Seg_3948" s="T499">dieses</ta>
            <ta e="T502" id="Seg_3949" s="T501">Hinterteil-3SG-ABL</ta>
            <ta e="T503" id="Seg_3950" s="T502">dieses.[NOM]</ta>
            <ta e="T505" id="Seg_3951" s="T503">ähnlich</ta>
            <ta e="T506" id="Seg_3952" s="T505">sein-FUT-3SG</ta>
            <ta e="T511" id="Seg_3953" s="T510">AFFIRM</ta>
            <ta e="T515" id="Seg_3954" s="T514">dieses.[NOM]</ta>
            <ta e="T516" id="Seg_3955" s="T515">ähnlich</ta>
            <ta e="T544" id="Seg_3956" s="T543">doch</ta>
            <ta e="T545" id="Seg_3957" s="T544">dieses</ta>
            <ta e="T546" id="Seg_3958" s="T545">es.gibt-3PL</ta>
            <ta e="T547" id="Seg_3959" s="T546">sagen-PRS.[3SG]</ta>
            <ta e="T548" id="Seg_3960" s="T547">zuerst</ta>
            <ta e="T549" id="Seg_3961" s="T548">arbeiten-CVB.SIM</ta>
            <ta e="T550" id="Seg_3962" s="T549">gehen-CVB.SEQ</ta>
            <ta e="T551" id="Seg_3963" s="T550">EMPH</ta>
            <ta e="T552" id="Seg_3964" s="T551">dieses</ta>
            <ta e="T553" id="Seg_3965" s="T552">Schnaps-PHIL-3PL.[NOM]</ta>
            <ta e="T554" id="Seg_3966" s="T553">wer-VBZ-PST2.[3SG]</ta>
            <ta e="T559" id="Seg_3967" s="T554">geraten-PST2.[3SG]</ta>
            <ta e="T565" id="Seg_3968" s="T564">doch</ta>
            <ta e="T566" id="Seg_3969" s="T565">dieses</ta>
            <ta e="T567" id="Seg_3970" s="T566">es.gibt-3PL</ta>
            <ta e="T568" id="Seg_3971" s="T567">EMPH</ta>
            <ta e="T572" id="Seg_3972" s="T570">dieses</ta>
            <ta e="T574" id="Seg_3973" s="T572">trinken-PTCP.PRS</ta>
            <ta e="T576" id="Seg_3974" s="T574">sitzen-PRS-3PL</ta>
            <ta e="T578" id="Seg_3975" s="T577">trinken-CVB.SIM</ta>
            <ta e="T579" id="Seg_3976" s="T578">sitzen-PRS.[3SG]</ta>
            <ta e="T580" id="Seg_3977" s="T579">dann</ta>
            <ta e="T581" id="Seg_3978" s="T580">dieses</ta>
            <ta e="T582" id="Seg_3979" s="T581">erzählen-PRS-3PL</ta>
            <ta e="T583" id="Seg_3980" s="T582">3SG.[NOM]</ta>
            <ta e="T584" id="Seg_3981" s="T583">böse.werden-PRS.[3SG]</ta>
            <ta e="T586" id="Seg_3982" s="T585">böse.werden-CVB.SEQ</ta>
            <ta e="T587" id="Seg_3983" s="T586">nachdem</ta>
            <ta e="T588" id="Seg_3984" s="T587">Frau-DAT/LOC</ta>
            <ta e="T589" id="Seg_3985" s="T588">gehen-PRS.[3SG]</ta>
            <ta e="T590" id="Seg_3986" s="T589">jenes</ta>
            <ta e="T591" id="Seg_3987" s="T590">es.gibt</ta>
            <ta e="T592" id="Seg_3988" s="T591">bringen.[IMP.2SG]</ta>
            <ta e="T593" id="Seg_3989" s="T592">jenes-ACC</ta>
            <ta e="T604" id="Seg_3990" s="T601">Fäuste-3SG-ACC</ta>
            <ta e="T608" id="Seg_3991" s="T604">vorbereiten-EP-MED-PST2.[3SG]</ta>
            <ta e="T612" id="Seg_3992" s="T610">ja</ta>
            <ta e="T613" id="Seg_3993" s="T612">ja</ta>
            <ta e="T615" id="Seg_3994" s="T613">ja</ta>
            <ta e="T616" id="Seg_3995" s="T615">ja</ta>
            <ta e="T618" id="Seg_3996" s="T616">ja</ta>
            <ta e="T626" id="Seg_3997" s="T624">gut.[NOM]</ta>
            <ta e="T627" id="Seg_3998" s="T626">dieses</ta>
            <ta e="T629" id="Seg_3999" s="T627">es.gibt</ta>
            <ta e="T630" id="Seg_4000" s="T629">arbeiten-CVB.SIM</ta>
            <ta e="T631" id="Seg_4001" s="T630">gehen-PRS-3PL</ta>
            <ta e="T633" id="Seg_4002" s="T631">leben-PTCP.PRS</ta>
            <ta e="T634" id="Seg_4003" s="T633">sein-PST1-3PL</ta>
            <ta e="T636" id="Seg_4004" s="T634">offenbar</ta>
            <ta e="T637" id="Seg_4005" s="T636">sehen.[IMP.2SG]</ta>
            <ta e="T638" id="Seg_4006" s="T637">dieses</ta>
            <ta e="T640" id="Seg_4007" s="T638">zusammen</ta>
            <ta e="T649" id="Seg_4008" s="T647">finden.[IMP.2SG]</ta>
            <ta e="T651" id="Seg_4009" s="T649">warte</ta>
            <ta e="T656" id="Seg_4010" s="T654">dieses</ta>
            <ta e="T658" id="Seg_4011" s="T656">es.gibt</ta>
            <ta e="T660" id="Seg_4012" s="T658">dieses</ta>
            <ta e="T662" id="Seg_4013" s="T660">es.gibt</ta>
            <ta e="T664" id="Seg_4014" s="T662">dieses-ACC</ta>
            <ta e="T665" id="Seg_4015" s="T664">dieses-ACC</ta>
            <ta e="T667" id="Seg_4016" s="T665">verhören-VBZ-PRS-3PL</ta>
            <ta e="T670" id="Seg_4017" s="T669">dieses</ta>
            <ta e="T671" id="Seg_4018" s="T670">es.gibt-3PL</ta>
            <ta e="T672" id="Seg_4019" s="T671">verhören-VBZ-PRS-3PL</ta>
            <ta e="T676" id="Seg_4020" s="T673">Kleidung.[NOM]</ta>
            <ta e="T677" id="Seg_4021" s="T676">geben-PRS-3PL</ta>
            <ta e="T679" id="Seg_4022" s="T678">versperren-EP-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T680" id="Seg_4023" s="T679">sitzen-PRS.[3SG]</ta>
            <ta e="T681" id="Seg_4024" s="T680">Gedanke-DAT/LOC</ta>
            <ta e="T682" id="Seg_4025" s="T681">lasten-EP-PASS-PTCP.PST.[NOM]</ta>
            <ta e="T684" id="Seg_4026" s="T683">dieses</ta>
            <ta e="T685" id="Seg_4027" s="T684">auch</ta>
            <ta e="T686" id="Seg_4028" s="T685">Gedanke-DAT/LOC</ta>
            <ta e="T687" id="Seg_4029" s="T686">lasten-EP-PASS-PTCP.PST.[NOM]</ta>
            <ta e="T688" id="Seg_4030" s="T687">sitzen-PRS.[3SG]</ta>
            <ta e="T690" id="Seg_4031" s="T689">dieses</ta>
            <ta e="T691" id="Seg_4032" s="T690">EMPH</ta>
            <ta e="T692" id="Seg_4033" s="T691">schon</ta>
            <ta e="T693" id="Seg_4034" s="T692">träumen-VBZ-CVB.SIM</ta>
            <ta e="T694" id="Seg_4035" s="T693">sitzen-PRS.[3SG]</ta>
            <ta e="T695" id="Seg_4036" s="T694">wahrscheinlich</ta>
            <ta e="T696" id="Seg_4037" s="T695">Kind-3SG.[NOM]</ta>
            <ta e="T697" id="Seg_4038" s="T696">treffen-VBZ-PTCP.PRS-3SG-ACC</ta>
            <ta e="T704" id="Seg_4039" s="T702">dieses.[NOM]</ta>
            <ta e="T705" id="Seg_4040" s="T704">ähnlich</ta>
            <ta e="T709" id="Seg_4041" s="T708">ja</ta>
            <ta e="T711" id="Seg_4042" s="T709">schon</ta>
            <ta e="T713" id="Seg_4043" s="T711">sich.vorstellen-VBZ-PRS.[3SG]</ta>
            <ta e="T714" id="Seg_4044" s="T713">Haus-3SG-DAT/LOC</ta>
            <ta e="T715" id="Seg_4045" s="T714">kommen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T717" id="Seg_4046" s="T715">wahrscheinlich</ta>
            <ta e="T725" id="Seg_4047" s="T723">dann</ta>
            <ta e="T727" id="Seg_4048" s="T725">jenes</ta>
            <ta e="T729" id="Seg_4049" s="T727">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T735" id="Seg_4050" s="T734">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T740" id="Seg_4051" s="T739">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T741" id="Seg_4052" s="T740">kommen-PST2.[3SG]</ta>
            <ta e="T742" id="Seg_4053" s="T741">sich.freuen-CVB.SIM-sich.freuen-CVB.SIM</ta>
            <ta e="T743" id="Seg_4054" s="T742">freudig.[NOM]</ta>
            <ta e="T745" id="Seg_4055" s="T744">dann</ta>
            <ta e="T746" id="Seg_4056" s="T745">erzählen-CVB.SIM</ta>
            <ta e="T747" id="Seg_4057" s="T746">sitzen-PRS.[3SG]</ta>
            <ta e="T749" id="Seg_4058" s="T748">dann</ta>
            <ta e="T756" id="Seg_4059" s="T752">trinken-NEG.PTCP</ta>
            <ta e="T758" id="Seg_4060" s="T756">werden-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-ChGS">
            <ta e="T63" id="Seg_4061" s="T61">где=Q</ta>
            <ta e="T65" id="Seg_4062" s="T63">первый.[NOM]</ta>
            <ta e="T70" id="Seg_4063" s="T69">конец.[NOM]</ta>
            <ta e="T72" id="Seg_4064" s="T70">EMPH</ta>
            <ta e="T74" id="Seg_4065" s="T72">EMPH</ta>
            <ta e="T78" id="Seg_4066" s="T77">приходить-PST2.[3SG]</ta>
            <ta e="T83" id="Seg_4067" s="T82">тот.[NOM]</ta>
            <ta e="T85" id="Seg_4068" s="T83">тоже</ta>
            <ta e="T87" id="Seg_4069" s="T85">там</ta>
            <ta e="T88" id="Seg_4070" s="T87">сидеть-PRS.[3SG]</ta>
            <ta e="T94" id="Seg_4071" s="T92">здесь</ta>
            <ta e="T96" id="Seg_4072" s="T94">наверное</ta>
            <ta e="T99" id="Seg_4073" s="T96">каждый-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_4074" s="T105">AFFIRM</ta>
            <ta e="T110" id="Seg_4075" s="T109">подожди</ta>
            <ta e="T111" id="Seg_4076" s="T110">тот.[NOM]</ta>
            <ta e="T112" id="Seg_4077" s="T111">стоять-PTCP.PRS</ta>
            <ta e="T114" id="Seg_4078" s="T112">падать-IMP.3SG</ta>
            <ta e="T116" id="Seg_4079" s="T114">видеть-IMP.1DU</ta>
            <ta e="T117" id="Seg_4080" s="T116">этот-PL-ACC</ta>
            <ta e="T119" id="Seg_4081" s="T117">класть-FREQ.[IMP.2SG]</ta>
            <ta e="T120" id="Seg_4082" s="T119">вот</ta>
            <ta e="T122" id="Seg_4083" s="T120">целый-3SG-ACC</ta>
            <ta e="T125" id="Seg_4084" s="T122">спиртное-VBZ-PTCP.PST-3PL-ACC</ta>
            <ta e="T127" id="Seg_4085" s="T125">и.так.далее-PTCP.PST-3PL-ACC</ta>
            <ta e="T134" id="Seg_4086" s="T133">этот</ta>
            <ta e="T135" id="Seg_4087" s="T134">EMPH</ta>
            <ta e="T136" id="Seg_4088" s="T135">есть</ta>
            <ta e="T137" id="Seg_4089" s="T136">EMPH</ta>
            <ta e="T154" id="Seg_4090" s="T153">потом</ta>
            <ta e="T155" id="Seg_4091" s="T154">приходить-CVB.SEQ</ta>
            <ta e="T156" id="Seg_4092" s="T155">этот</ta>
            <ta e="T158" id="Seg_4093" s="T156">деньги-3SG-ACC</ta>
            <ta e="T160" id="Seg_4094" s="T158">целый-3SG-ACC</ta>
            <ta e="T161" id="Seg_4095" s="T160">спиртное.[NOM]</ta>
            <ta e="T163" id="Seg_4096" s="T161">делать-PST2-3SG</ta>
            <ta e="T167" id="Seg_4097" s="T166">говорить-PRS-2SG</ta>
            <ta e="T169" id="Seg_4098" s="T167">Q</ta>
            <ta e="T176" id="Seg_4099" s="T175">сначала</ta>
            <ta e="T177" id="Seg_4100" s="T176">пить-NEG.PTCP</ta>
            <ta e="T178" id="Seg_4101" s="T177">быть-PST1-3SG</ta>
            <ta e="T179" id="Seg_4102" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_4103" s="T179">Q</ta>
            <ta e="T184" id="Seg_4104" s="T182">приходить-CVB.SEQ</ta>
            <ta e="T186" id="Seg_4105" s="T184">идти-CVB.SEQ</ta>
            <ta e="T188" id="Seg_4106" s="T186">пить-NEG.[3SG]</ta>
            <ta e="T190" id="Seg_4107" s="T188">MOD</ta>
            <ta e="T192" id="Seg_4108" s="T190">здесь</ta>
            <ta e="T194" id="Seg_4109" s="T192">уже</ta>
            <ta e="T196" id="Seg_4110" s="T194">ребенок-3SG.[NOM]</ta>
            <ta e="T198" id="Seg_4111" s="T196">этот</ta>
            <ta e="T200" id="Seg_4112" s="T198">расти-PTCP.PST-3SG-ACC</ta>
            <ta e="T203" id="Seg_4113" s="T200">после.того</ta>
            <ta e="T207" id="Seg_4114" s="T204">там</ta>
            <ta e="T210" id="Seg_4115" s="T207">маленький.[NOM]</ta>
            <ta e="T212" id="Seg_4116" s="T211">ребенок-3SG.[NOM]</ta>
            <ta e="T213" id="Seg_4117" s="T212">маленький.[NOM]</ta>
            <ta e="T215" id="Seg_4118" s="T214">приходить-CVB.SEQ</ta>
            <ta e="T216" id="Seg_4119" s="T215">после</ta>
            <ta e="T217" id="Seg_4120" s="T216">рассказывать-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_4121" s="T217">Q</ta>
            <ta e="T221" id="Seg_4122" s="T219">этот</ta>
            <ta e="T222" id="Seg_4123" s="T221">приходить-CVB.SEQ</ta>
            <ta e="T224" id="Seg_4124" s="T222">после</ta>
            <ta e="T226" id="Seg_4125" s="T224">рассказывать-PRS.[3SG]</ta>
            <ta e="T227" id="Seg_4126" s="T226">очевидно</ta>
            <ta e="T229" id="Seg_4127" s="T227">как</ta>
            <ta e="T231" id="Seg_4128" s="T229">сидеть-CVB.SIM</ta>
            <ta e="T232" id="Seg_4129" s="T231">сидеть-PTCP.PST-3SG-ACC</ta>
            <ta e="T234" id="Seg_4130" s="T233">слушать-CVB.SIM</ta>
            <ta e="T235" id="Seg_4131" s="T234">жить-PRS-3PL</ta>
            <ta e="T236" id="Seg_4132" s="T235">Q</ta>
            <ta e="T239" id="Seg_4133" s="T237">подожди</ta>
            <ta e="T248" id="Seg_4134" s="T247">этот</ta>
            <ta e="T250" id="Seg_4135" s="T248">есть</ta>
            <ta e="T251" id="Seg_4136" s="T250">этот</ta>
            <ta e="T253" id="Seg_4137" s="T251">есть</ta>
            <ta e="T254" id="Seg_4138" s="T253">этот</ta>
            <ta e="T256" id="Seg_4139" s="T254">есть</ta>
            <ta e="T259" id="Seg_4140" s="T258">подожди</ta>
            <ta e="T260" id="Seg_4141" s="T259">первый-1PL-ACC</ta>
            <ta e="T261" id="Seg_4142" s="T260">найти-PTCP.FUT-1PL-ACC</ta>
            <ta e="T262" id="Seg_4143" s="T261">надо</ta>
            <ta e="T265" id="Seg_4144" s="T263">этот</ta>
            <ta e="T266" id="Seg_4145" s="T265">вот</ta>
            <ta e="T267" id="Seg_4146" s="T266">как.раз</ta>
            <ta e="T268" id="Seg_4147" s="T267">тюрьма-ABL</ta>
            <ta e="T270" id="Seg_4148" s="T268">выйти-EP-PST2.[3SG]</ta>
            <ta e="T272" id="Seg_4149" s="T270">наверное</ta>
            <ta e="T273" id="Seg_4150" s="T272">EMPH</ta>
            <ta e="T274" id="Seg_4151" s="T273">этот</ta>
            <ta e="T276" id="Seg_4152" s="T274">радоваться-PRS.[3SG]</ta>
            <ta e="T279" id="Seg_4153" s="T278">тюрьма-DAT/LOC</ta>
            <ta e="T280" id="Seg_4154" s="T279">EMPH</ta>
            <ta e="T282" id="Seg_4155" s="T280">3SG.[NOM]</ta>
            <ta e="T284" id="Seg_4156" s="T282">кажется</ta>
            <ta e="T285" id="Seg_4157" s="T284">MOD</ta>
            <ta e="T289" id="Seg_4158" s="T286">работать-PRS.[3SG]</ta>
            <ta e="T291" id="Seg_4159" s="T289">Q</ta>
            <ta e="T313" id="Seg_4160" s="T312">любить-PTCP.PRS</ta>
            <ta e="T314" id="Seg_4161" s="T313">ребенок-3SG.[NOM]</ta>
            <ta e="T315" id="Seg_4162" s="T314">тот.[NOM]</ta>
            <ta e="T316" id="Seg_4163" s="T315">подобно</ta>
            <ta e="T317" id="Seg_4164" s="T316">бегать-CVB.SIM-бегать-CVB.SIM</ta>
            <ta e="T318" id="Seg_4165" s="T317">приходить-PTCP.FUT</ta>
            <ta e="T319" id="Seg_4166" s="T318">быть-PST1-3SG</ta>
            <ta e="T320" id="Seg_4167" s="T319">3SG-DAT/LOC</ta>
            <ta e="T328" id="Seg_4168" s="T326">здесь</ta>
            <ta e="T329" id="Seg_4169" s="T328">наверное</ta>
            <ta e="T331" id="Seg_4170" s="T329">видеть.[IMP.2SG]</ta>
            <ta e="T332" id="Seg_4171" s="T331">этот</ta>
            <ta e="T333" id="Seg_4172" s="T332">спиртное-VBZ-CVB.SIM</ta>
            <ta e="T335" id="Seg_4173" s="T333">сидеть-CVB.SEQ</ta>
            <ta e="T336" id="Seg_4174" s="T335">этот</ta>
            <ta e="T338" id="Seg_4175" s="T336">спиртное-VBZ-CVB.SIM</ta>
            <ta e="T339" id="Seg_4176" s="T338">сидеть-CVB.SEQ-3PL</ta>
            <ta e="T340" id="Seg_4177" s="T339">разговаривать-CVB.SIM</ta>
            <ta e="T342" id="Seg_4178" s="T341">этот</ta>
            <ta e="T343" id="Seg_4179" s="T342">человек.[NOM]</ta>
            <ta e="T344" id="Seg_4180" s="T343">рассказывать-PRS.[3SG]</ta>
            <ta e="T345" id="Seg_4181" s="T344">говорить-CVB.SIM</ta>
            <ta e="T346" id="Seg_4182" s="T345">жена-2SG-ACC</ta>
            <ta e="T347" id="Seg_4183" s="T346">видеть-PST2-EP-1SG</ta>
            <ta e="T348" id="Seg_4184" s="T347">говорить-PRS.[3SG]</ta>
            <ta e="T349" id="Seg_4185" s="T348">EMPH</ta>
            <ta e="T351" id="Seg_4186" s="T350">рассердиться-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_4187" s="T351">уже</ta>
            <ta e="T353" id="Seg_4188" s="T352">EMPH</ta>
            <ta e="T356" id="Seg_4189" s="T354">оттуда</ta>
            <ta e="T358" id="Seg_4190" s="T356">приходить-CVB.SEQ</ta>
            <ta e="T360" id="Seg_4191" s="T358">вот</ta>
            <ta e="T362" id="Seg_4192" s="T360">оттуда</ta>
            <ta e="T364" id="Seg_4193" s="T362">приходить-CVB.SEQ</ta>
            <ta e="T366" id="Seg_4194" s="T364">после</ta>
            <ta e="T367" id="Seg_4195" s="T366">отношение.[NOM]</ta>
            <ta e="T368" id="Seg_4196" s="T367">выяснять-VBZ-PRS.[3SG]</ta>
            <ta e="T369" id="Seg_4197" s="T368">EMPH</ta>
            <ta e="T370" id="Seg_4198" s="T369">бить-CVB.PURP</ta>
            <ta e="T371" id="Seg_4199" s="T370">хотеть-PRS.[3SG]</ta>
            <ta e="T372" id="Seg_4200" s="T371">тот</ta>
            <ta e="T373" id="Seg_4201" s="T372">есть</ta>
            <ta e="T376" id="Seg_4202" s="T375">потом</ta>
            <ta e="T377" id="Seg_4203" s="T376">этот</ta>
            <ta e="T378" id="Seg_4204" s="T377">уже</ta>
            <ta e="T379" id="Seg_4205" s="T378">бить-PST2.[3SG]</ta>
            <ta e="T380" id="Seg_4206" s="T379">бить-EP-PST2.[3SG]</ta>
            <ta e="T381" id="Seg_4207" s="T380">уже</ta>
            <ta e="T386" id="Seg_4208" s="T385">этот.[NOM]</ta>
            <ta e="T387" id="Seg_4209" s="T386">подобно</ta>
            <ta e="T389" id="Seg_4210" s="T387">может</ta>
            <ta e="T390" id="Seg_4211" s="T389">да</ta>
            <ta e="T393" id="Seg_4212" s="T391">принести.[IMP.2SG]</ta>
            <ta e="T395" id="Seg_4213" s="T393">ближе</ta>
            <ta e="T398" id="Seg_4214" s="T397">принести.[IMP.2SG]</ta>
            <ta e="T399" id="Seg_4215" s="T398">один</ta>
            <ta e="T404" id="Seg_4216" s="T402">этот</ta>
            <ta e="T405" id="Seg_4217" s="T404">есть</ta>
            <ta e="T407" id="Seg_4218" s="T405">первый.[NOM]</ta>
            <ta e="T408" id="Seg_4219" s="T407">этот</ta>
            <ta e="T409" id="Seg_4220" s="T408">есть</ta>
            <ta e="T411" id="Seg_4221" s="T409">вот</ta>
            <ta e="T429" id="Seg_4222" s="T427">нет</ta>
            <ta e="T430" id="Seg_4223" s="T429">этот</ta>
            <ta e="T431" id="Seg_4224" s="T430">уже</ta>
            <ta e="T432" id="Seg_4225" s="T431">конец-3SG-DAT/LOC</ta>
            <ta e="T433" id="Seg_4226" s="T432">пить-NEG-1SG</ta>
            <ta e="T434" id="Seg_4227" s="T433">говорить-PRS.[3SG]</ta>
            <ta e="T435" id="Seg_4228" s="T434">уже</ta>
            <ta e="T436" id="Seg_4229" s="T435">два</ta>
            <ta e="T437" id="Seg_4230" s="T436">последний.[NOM]</ta>
            <ta e="T438" id="Seg_4231" s="T437">приходить-CVB.SEQ</ta>
            <ta e="T439" id="Seg_4232" s="T438">идти-PRS.[3SG]</ta>
            <ta e="T441" id="Seg_4233" s="T439">тюрьма-ABL</ta>
            <ta e="T442" id="Seg_4234" s="T441">приходить-CVB.SEQ</ta>
            <ta e="T443" id="Seg_4235" s="T442">после</ta>
            <ta e="T449" id="Seg_4236" s="T448">AFFIRM</ta>
            <ta e="T454" id="Seg_4237" s="T453">пить-NEG.[3SG]</ta>
            <ta e="T464" id="Seg_4238" s="T461">хороший.[NOM]</ta>
            <ta e="T466" id="Seg_4239" s="T464">очень</ta>
            <ta e="T468" id="Seg_4240" s="T466">жить-PTCP.PRS</ta>
            <ta e="T469" id="Seg_4241" s="T468">быть-PST1-3PL</ta>
            <ta e="T471" id="Seg_4242" s="T469">жить-PTCP.PRS</ta>
            <ta e="T473" id="Seg_4243" s="T471">быть-PST1-3PL</ta>
            <ta e="T475" id="Seg_4244" s="T473">очевидно</ta>
            <ta e="T477" id="Seg_4245" s="T475">работать-CVB.SIM</ta>
            <ta e="T478" id="Seg_4246" s="T477">идти-PRS-3PL</ta>
            <ta e="T485" id="Seg_4247" s="T484">вот</ta>
            <ta e="T486" id="Seg_4248" s="T485">тот.[NOM]</ta>
            <ta e="T487" id="Seg_4249" s="T486">уже</ta>
            <ta e="T488" id="Seg_4250" s="T487">тюрьма-3SG-ACC</ta>
            <ta e="T489" id="Seg_4251" s="T488">рассказывать-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_4252" s="T489">EMPH</ta>
            <ta e="T494" id="Seg_4253" s="T491">3SG.[NOM]</ta>
            <ta e="T496" id="Seg_4254" s="T494">позже-INTNS</ta>
            <ta e="T498" id="Seg_4255" s="T496">очевидно</ta>
            <ta e="T499" id="Seg_4256" s="T498">этот</ta>
            <ta e="T501" id="Seg_4257" s="T499">этот</ta>
            <ta e="T502" id="Seg_4258" s="T501">задняя.часть-3SG-ABL</ta>
            <ta e="T503" id="Seg_4259" s="T502">этот.[NOM]</ta>
            <ta e="T505" id="Seg_4260" s="T503">подобно</ta>
            <ta e="T506" id="Seg_4261" s="T505">быть-FUT-3SG</ta>
            <ta e="T511" id="Seg_4262" s="T510">AFFIRM</ta>
            <ta e="T515" id="Seg_4263" s="T514">тот.[NOM]</ta>
            <ta e="T516" id="Seg_4264" s="T515">подобно</ta>
            <ta e="T544" id="Seg_4265" s="T543">вот</ta>
            <ta e="T545" id="Seg_4266" s="T544">этот</ta>
            <ta e="T546" id="Seg_4267" s="T545">есть-3PL</ta>
            <ta e="T547" id="Seg_4268" s="T546">говорить-PRS.[3SG]</ta>
            <ta e="T548" id="Seg_4269" s="T547">сначала</ta>
            <ta e="T549" id="Seg_4270" s="T548">работать-CVB.SIM</ta>
            <ta e="T550" id="Seg_4271" s="T549">идти-CVB.SEQ</ta>
            <ta e="T551" id="Seg_4272" s="T550">EMPH</ta>
            <ta e="T552" id="Seg_4273" s="T551">этот</ta>
            <ta e="T553" id="Seg_4274" s="T552">спиртное-PHIL-3PL.[NOM]</ta>
            <ta e="T554" id="Seg_4275" s="T553">кто-VBZ-PST2.[3SG]</ta>
            <ta e="T559" id="Seg_4276" s="T554">попадать-PST2.[3SG]</ta>
            <ta e="T565" id="Seg_4277" s="T564">вот</ta>
            <ta e="T566" id="Seg_4278" s="T565">этот</ta>
            <ta e="T567" id="Seg_4279" s="T566">есть-3PL</ta>
            <ta e="T568" id="Seg_4280" s="T567">EMPH</ta>
            <ta e="T572" id="Seg_4281" s="T570">этот</ta>
            <ta e="T574" id="Seg_4282" s="T572">пить-PTCP.PRS</ta>
            <ta e="T576" id="Seg_4283" s="T574">сидеть-PRS-3PL</ta>
            <ta e="T578" id="Seg_4284" s="T577">пить-CVB.SIM</ta>
            <ta e="T579" id="Seg_4285" s="T578">сидеть-PRS.[3SG]</ta>
            <ta e="T580" id="Seg_4286" s="T579">потом</ta>
            <ta e="T581" id="Seg_4287" s="T580">этот</ta>
            <ta e="T582" id="Seg_4288" s="T581">рассказывать-PRS-3PL</ta>
            <ta e="T583" id="Seg_4289" s="T582">3SG.[NOM]</ta>
            <ta e="T584" id="Seg_4290" s="T583">рассердиться-PRS.[3SG]</ta>
            <ta e="T586" id="Seg_4291" s="T585">рассердиться-CVB.SEQ</ta>
            <ta e="T587" id="Seg_4292" s="T586">после</ta>
            <ta e="T588" id="Seg_4293" s="T587">жена-DAT/LOC</ta>
            <ta e="T589" id="Seg_4294" s="T588">идти-PRS.[3SG]</ta>
            <ta e="T590" id="Seg_4295" s="T589">тот</ta>
            <ta e="T591" id="Seg_4296" s="T590">есть</ta>
            <ta e="T592" id="Seg_4297" s="T591">принести.[IMP.2SG]</ta>
            <ta e="T593" id="Seg_4298" s="T592">тот-ACC</ta>
            <ta e="T604" id="Seg_4299" s="T601">кулаки-3SG-ACC</ta>
            <ta e="T608" id="Seg_4300" s="T604">готовить-EP-MED-PST2.[3SG]</ta>
            <ta e="T612" id="Seg_4301" s="T610">да</ta>
            <ta e="T613" id="Seg_4302" s="T612">да</ta>
            <ta e="T615" id="Seg_4303" s="T613">да</ta>
            <ta e="T616" id="Seg_4304" s="T615">да</ta>
            <ta e="T618" id="Seg_4305" s="T616">да</ta>
            <ta e="T626" id="Seg_4306" s="T624">хороший.[NOM]</ta>
            <ta e="T627" id="Seg_4307" s="T626">этот</ta>
            <ta e="T629" id="Seg_4308" s="T627">есть</ta>
            <ta e="T630" id="Seg_4309" s="T629">работать-CVB.SIM</ta>
            <ta e="T631" id="Seg_4310" s="T630">идти-PRS-3PL</ta>
            <ta e="T633" id="Seg_4311" s="T631">жить-PTCP.PRS</ta>
            <ta e="T634" id="Seg_4312" s="T633">быть-PST1-3PL</ta>
            <ta e="T636" id="Seg_4313" s="T634">очевидно</ta>
            <ta e="T637" id="Seg_4314" s="T636">видеть.[IMP.2SG]</ta>
            <ta e="T638" id="Seg_4315" s="T637">этот</ta>
            <ta e="T640" id="Seg_4316" s="T638">вместе</ta>
            <ta e="T649" id="Seg_4317" s="T647">найти.[IMP.2SG]</ta>
            <ta e="T651" id="Seg_4318" s="T649">подожди</ta>
            <ta e="T656" id="Seg_4319" s="T654">этот</ta>
            <ta e="T658" id="Seg_4320" s="T656">есть</ta>
            <ta e="T660" id="Seg_4321" s="T658">этот</ta>
            <ta e="T662" id="Seg_4322" s="T660">есть</ta>
            <ta e="T664" id="Seg_4323" s="T662">этот-ACC</ta>
            <ta e="T665" id="Seg_4324" s="T664">этот-ACC</ta>
            <ta e="T667" id="Seg_4325" s="T665">допрашивать-VBZ-PRS-3PL</ta>
            <ta e="T670" id="Seg_4326" s="T669">этот</ta>
            <ta e="T671" id="Seg_4327" s="T670">есть-3PL</ta>
            <ta e="T672" id="Seg_4328" s="T671">допрашивать-VBZ-PRS-3PL</ta>
            <ta e="T676" id="Seg_4329" s="T673">одежда.[NOM]</ta>
            <ta e="T677" id="Seg_4330" s="T676">давать-PRS-3PL</ta>
            <ta e="T679" id="Seg_4331" s="T678">заложить-EP-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T680" id="Seg_4332" s="T679">сидеть-PRS.[3SG]</ta>
            <ta e="T681" id="Seg_4333" s="T680">мысль-DAT/LOC</ta>
            <ta e="T682" id="Seg_4334" s="T681">давить-EP-PASS-PTCP.PST.[NOM]</ta>
            <ta e="T684" id="Seg_4335" s="T683">этот</ta>
            <ta e="T685" id="Seg_4336" s="T684">тоже</ta>
            <ta e="T686" id="Seg_4337" s="T685">мысль-DAT/LOC</ta>
            <ta e="T687" id="Seg_4338" s="T686">давить-EP-PASS-PTCP.PST.[NOM]</ta>
            <ta e="T688" id="Seg_4339" s="T687">сидеть-PRS.[3SG]</ta>
            <ta e="T690" id="Seg_4340" s="T689">этот</ta>
            <ta e="T691" id="Seg_4341" s="T690">EMPH</ta>
            <ta e="T692" id="Seg_4342" s="T691">уже</ta>
            <ta e="T693" id="Seg_4343" s="T692">мечтать-VBZ-CVB.SIM</ta>
            <ta e="T694" id="Seg_4344" s="T693">сидеть-PRS.[3SG]</ta>
            <ta e="T695" id="Seg_4345" s="T694">наверное</ta>
            <ta e="T696" id="Seg_4346" s="T695">ребенок-3SG.[NOM]</ta>
            <ta e="T697" id="Seg_4347" s="T696">встречать-VBZ-PTCP.PRS-3SG-ACC</ta>
            <ta e="T704" id="Seg_4348" s="T702">тот.[NOM]</ta>
            <ta e="T705" id="Seg_4349" s="T704">подобно</ta>
            <ta e="T709" id="Seg_4350" s="T708">да</ta>
            <ta e="T711" id="Seg_4351" s="T709">уже</ta>
            <ta e="T713" id="Seg_4352" s="T711">представлять-VBZ-PRS.[3SG]</ta>
            <ta e="T714" id="Seg_4353" s="T713">дом-3SG-DAT/LOC</ta>
            <ta e="T715" id="Seg_4354" s="T714">приходить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T717" id="Seg_4355" s="T715">наверное</ta>
            <ta e="T725" id="Seg_4356" s="T723">потом</ta>
            <ta e="T727" id="Seg_4357" s="T725">тот</ta>
            <ta e="T729" id="Seg_4358" s="T727">выйти-EP-PST2.[3SG]</ta>
            <ta e="T735" id="Seg_4359" s="T734">выйти-EP-PST2.[3SG]</ta>
            <ta e="T740" id="Seg_4360" s="T739">выйти-EP-PST2.[3SG]</ta>
            <ta e="T741" id="Seg_4361" s="T740">приходить-PST2.[3SG]</ta>
            <ta e="T742" id="Seg_4362" s="T741">радоваться-CVB.SIM-радоваться-CVB.SIM</ta>
            <ta e="T743" id="Seg_4363" s="T742">радостный.[NOM]</ta>
            <ta e="T745" id="Seg_4364" s="T744">потом</ta>
            <ta e="T746" id="Seg_4365" s="T745">рассказывать-CVB.SIM</ta>
            <ta e="T747" id="Seg_4366" s="T746">сидеть-PRS.[3SG]</ta>
            <ta e="T749" id="Seg_4367" s="T748">потом</ta>
            <ta e="T756" id="Seg_4368" s="T752">пить-NEG.PTCP</ta>
            <ta e="T758" id="Seg_4369" s="T756">становиться-PST2.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-ChGS">
            <ta e="T63" id="Seg_4370" s="T61">que=ptcl</ta>
            <ta e="T65" id="Seg_4371" s="T63">ordnum.[n:case]</ta>
            <ta e="T70" id="Seg_4372" s="T69">n.[n:case]</ta>
            <ta e="T72" id="Seg_4373" s="T70">ptcl</ta>
            <ta e="T74" id="Seg_4374" s="T72">ptcl</ta>
            <ta e="T78" id="Seg_4375" s="T77">v-v:tense.[v:pred.pn]</ta>
            <ta e="T83" id="Seg_4376" s="T82">dempro.[pro:case]</ta>
            <ta e="T85" id="Seg_4377" s="T83">ptcl</ta>
            <ta e="T87" id="Seg_4378" s="T85">adv</ta>
            <ta e="T88" id="Seg_4379" s="T87">v-v:tense.[v:pred.pn]</ta>
            <ta e="T94" id="Seg_4380" s="T92">adv</ta>
            <ta e="T96" id="Seg_4381" s="T94">adv</ta>
            <ta e="T99" id="Seg_4382" s="T96">adj-n:(poss).[n:case]</ta>
            <ta e="T108" id="Seg_4383" s="T105">ptcl</ta>
            <ta e="T110" id="Seg_4384" s="T109">interj</ta>
            <ta e="T111" id="Seg_4385" s="T110">dempro.[pro:case]</ta>
            <ta e="T112" id="Seg_4386" s="T111">v-v:ptcp</ta>
            <ta e="T114" id="Seg_4387" s="T112">v-v:mood.pn</ta>
            <ta e="T116" id="Seg_4388" s="T114">v-v:mood.pn</ta>
            <ta e="T117" id="Seg_4389" s="T116">dempro-pro:(num)-pro:case</ta>
            <ta e="T119" id="Seg_4390" s="T117">v-v&gt;v.[v:mood.pn]</ta>
            <ta e="T120" id="Seg_4391" s="T119">ptcl</ta>
            <ta e="T122" id="Seg_4392" s="T120">adj-n:poss-n:case</ta>
            <ta e="T125" id="Seg_4393" s="T122">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T127" id="Seg_4394" s="T125">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T134" id="Seg_4395" s="T133">dempro</ta>
            <ta e="T135" id="Seg_4396" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_4397" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_4398" s="T136">ptcl</ta>
            <ta e="T154" id="Seg_4399" s="T153">adv</ta>
            <ta e="T155" id="Seg_4400" s="T154">v-v:cvb</ta>
            <ta e="T156" id="Seg_4401" s="T155">dempro</ta>
            <ta e="T158" id="Seg_4402" s="T156">n-n:poss-n:case</ta>
            <ta e="T160" id="Seg_4403" s="T158">adj-n:poss-n:case</ta>
            <ta e="T161" id="Seg_4404" s="T160">n.[n:case]</ta>
            <ta e="T163" id="Seg_4405" s="T161">v-v:tense-v:poss.pn</ta>
            <ta e="T167" id="Seg_4406" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_4407" s="T167">ptcl</ta>
            <ta e="T176" id="Seg_4408" s="T175">adv</ta>
            <ta e="T177" id="Seg_4409" s="T176">v-v:ptcp</ta>
            <ta e="T178" id="Seg_4410" s="T177">v-v:tense-v:poss.pn</ta>
            <ta e="T179" id="Seg_4411" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_4412" s="T179">ptcl</ta>
            <ta e="T184" id="Seg_4413" s="T182">v-v:cvb</ta>
            <ta e="T186" id="Seg_4414" s="T184">v-v:cvb</ta>
            <ta e="T188" id="Seg_4415" s="T186">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T190" id="Seg_4416" s="T188">ptcl</ta>
            <ta e="T192" id="Seg_4417" s="T190">adv</ta>
            <ta e="T194" id="Seg_4418" s="T192">ptcl</ta>
            <ta e="T196" id="Seg_4419" s="T194">n-n:(poss).[n:case]</ta>
            <ta e="T198" id="Seg_4420" s="T196">dempro</ta>
            <ta e="T200" id="Seg_4421" s="T198">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T203" id="Seg_4422" s="T200">post</ta>
            <ta e="T207" id="Seg_4423" s="T204">adv</ta>
            <ta e="T210" id="Seg_4424" s="T207">adj.[n:case]</ta>
            <ta e="T212" id="Seg_4425" s="T211">n-n:(poss).[n:case]</ta>
            <ta e="T213" id="Seg_4426" s="T212">adj.[n:case]</ta>
            <ta e="T215" id="Seg_4427" s="T214">v-v:cvb</ta>
            <ta e="T216" id="Seg_4428" s="T215">post</ta>
            <ta e="T217" id="Seg_4429" s="T216">v-v:tense.[v:pred.pn]</ta>
            <ta e="T218" id="Seg_4430" s="T217">ptcl</ta>
            <ta e="T221" id="Seg_4431" s="T219">dempro</ta>
            <ta e="T222" id="Seg_4432" s="T221">v-v:cvb</ta>
            <ta e="T224" id="Seg_4433" s="T222">post</ta>
            <ta e="T226" id="Seg_4434" s="T224">v-v:tense.[v:pred.pn]</ta>
            <ta e="T227" id="Seg_4435" s="T226">ptcl</ta>
            <ta e="T229" id="Seg_4436" s="T227">que</ta>
            <ta e="T231" id="Seg_4437" s="T229">v-v:cvb</ta>
            <ta e="T232" id="Seg_4438" s="T231">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T234" id="Seg_4439" s="T233">v-v:cvb</ta>
            <ta e="T235" id="Seg_4440" s="T234">v-v:tense-v:pred.pn</ta>
            <ta e="T236" id="Seg_4441" s="T235">ptcl</ta>
            <ta e="T239" id="Seg_4442" s="T237">interj</ta>
            <ta e="T248" id="Seg_4443" s="T247">dempro</ta>
            <ta e="T250" id="Seg_4444" s="T248">ptcl</ta>
            <ta e="T251" id="Seg_4445" s="T250">dempro</ta>
            <ta e="T253" id="Seg_4446" s="T251">ptcl</ta>
            <ta e="T254" id="Seg_4447" s="T253">dempro</ta>
            <ta e="T256" id="Seg_4448" s="T254">ptcl</ta>
            <ta e="T259" id="Seg_4449" s="T258">interj</ta>
            <ta e="T260" id="Seg_4450" s="T259">ordnum-n:poss-n:case</ta>
            <ta e="T261" id="Seg_4451" s="T260">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T262" id="Seg_4452" s="T261">ptcl</ta>
            <ta e="T265" id="Seg_4453" s="T263">dempro</ta>
            <ta e="T266" id="Seg_4454" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_4455" s="T266">adv</ta>
            <ta e="T268" id="Seg_4456" s="T267">n-n:case</ta>
            <ta e="T270" id="Seg_4457" s="T268">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T272" id="Seg_4458" s="T270">adv</ta>
            <ta e="T273" id="Seg_4459" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_4460" s="T273">dempro</ta>
            <ta e="T276" id="Seg_4461" s="T274">v-v:tense.[v:pred.pn]</ta>
            <ta e="T279" id="Seg_4462" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_4463" s="T279">ptcl</ta>
            <ta e="T282" id="Seg_4464" s="T280">pers.[pro:case]</ta>
            <ta e="T284" id="Seg_4465" s="T282">ptcl</ta>
            <ta e="T285" id="Seg_4466" s="T284">ptcl</ta>
            <ta e="T289" id="Seg_4467" s="T286">v-v:tense.[v:pred.pn]</ta>
            <ta e="T291" id="Seg_4468" s="T289">ptcl</ta>
            <ta e="T313" id="Seg_4469" s="T312">v-v:ptcp</ta>
            <ta e="T314" id="Seg_4470" s="T313">n-n:(poss).[n:case]</ta>
            <ta e="T315" id="Seg_4471" s="T314">dempro.[pro:case]</ta>
            <ta e="T316" id="Seg_4472" s="T315">post</ta>
            <ta e="T317" id="Seg_4473" s="T316">v-v:cvb-v-v:cvb</ta>
            <ta e="T318" id="Seg_4474" s="T317">v-v:ptcp</ta>
            <ta e="T319" id="Seg_4475" s="T318">v-v:tense-v:poss.pn</ta>
            <ta e="T320" id="Seg_4476" s="T319">pers-pro:case</ta>
            <ta e="T328" id="Seg_4477" s="T326">adv</ta>
            <ta e="T329" id="Seg_4478" s="T328">adv</ta>
            <ta e="T331" id="Seg_4479" s="T329">v.[v:mood.pn]</ta>
            <ta e="T332" id="Seg_4480" s="T331">dempro</ta>
            <ta e="T333" id="Seg_4481" s="T332">n-n&gt;v-v:cvb</ta>
            <ta e="T335" id="Seg_4482" s="T333">v-v:cvb</ta>
            <ta e="T336" id="Seg_4483" s="T335">dempro</ta>
            <ta e="T338" id="Seg_4484" s="T336">n-n&gt;v-v:cvb</ta>
            <ta e="T339" id="Seg_4485" s="T338">v-v:cvb-v:pred.pn</ta>
            <ta e="T340" id="Seg_4486" s="T339">v-v:cvb</ta>
            <ta e="T342" id="Seg_4487" s="T341">dempro</ta>
            <ta e="T343" id="Seg_4488" s="T342">n.[n:case]</ta>
            <ta e="T344" id="Seg_4489" s="T343">v-v:tense.[v:pred.pn]</ta>
            <ta e="T345" id="Seg_4490" s="T344">v-v:cvb</ta>
            <ta e="T346" id="Seg_4491" s="T345">n-n:poss-n:case</ta>
            <ta e="T347" id="Seg_4492" s="T346">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T348" id="Seg_4493" s="T347">v-v:tense.[v:pred.pn]</ta>
            <ta e="T349" id="Seg_4494" s="T348">ptcl</ta>
            <ta e="T351" id="Seg_4495" s="T350">v-v:tense.[v:pred.pn]</ta>
            <ta e="T352" id="Seg_4496" s="T351">ptcl</ta>
            <ta e="T353" id="Seg_4497" s="T352">ptcl</ta>
            <ta e="T356" id="Seg_4498" s="T354">adv</ta>
            <ta e="T358" id="Seg_4499" s="T356">v-v:cvb</ta>
            <ta e="T360" id="Seg_4500" s="T358">ptcl</ta>
            <ta e="T362" id="Seg_4501" s="T360">adv</ta>
            <ta e="T364" id="Seg_4502" s="T362">v-v:cvb</ta>
            <ta e="T366" id="Seg_4503" s="T364">post</ta>
            <ta e="T367" id="Seg_4504" s="T366">n.[n:case]</ta>
            <ta e="T368" id="Seg_4505" s="T367">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T369" id="Seg_4506" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_4507" s="T369">v-v:cvb</ta>
            <ta e="T371" id="Seg_4508" s="T370">v-v:tense.[v:pred.pn]</ta>
            <ta e="T372" id="Seg_4509" s="T371">dempro</ta>
            <ta e="T373" id="Seg_4510" s="T372">ptcl</ta>
            <ta e="T376" id="Seg_4511" s="T375">adv</ta>
            <ta e="T377" id="Seg_4512" s="T376">dempro</ta>
            <ta e="T378" id="Seg_4513" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_4514" s="T378">v-v:tense.[v:pred.pn]</ta>
            <ta e="T380" id="Seg_4515" s="T379">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T381" id="Seg_4516" s="T380">ptcl</ta>
            <ta e="T386" id="Seg_4517" s="T385">dempro.[pro:case]</ta>
            <ta e="T387" id="Seg_4518" s="T386">post</ta>
            <ta e="T389" id="Seg_4519" s="T387">ptcl</ta>
            <ta e="T390" id="Seg_4520" s="T389">ptcl</ta>
            <ta e="T393" id="Seg_4521" s="T391">v.[v:mood.pn]</ta>
            <ta e="T395" id="Seg_4522" s="T393">adv</ta>
            <ta e="T398" id="Seg_4523" s="T397">v.[v:mood.pn]</ta>
            <ta e="T399" id="Seg_4524" s="T398">cardnum</ta>
            <ta e="T404" id="Seg_4525" s="T402">dempro</ta>
            <ta e="T405" id="Seg_4526" s="T404">ptcl</ta>
            <ta e="T407" id="Seg_4527" s="T405">ordnum.[n:case]</ta>
            <ta e="T408" id="Seg_4528" s="T407">dempro</ta>
            <ta e="T409" id="Seg_4529" s="T408">ptcl</ta>
            <ta e="T411" id="Seg_4530" s="T409">ptcl</ta>
            <ta e="T429" id="Seg_4531" s="T427">ptcl</ta>
            <ta e="T430" id="Seg_4532" s="T429">dempro</ta>
            <ta e="T431" id="Seg_4533" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_4534" s="T431">n-n:poss-n:case</ta>
            <ta e="T433" id="Seg_4535" s="T432">v-v:(neg)-v:pred.pn</ta>
            <ta e="T434" id="Seg_4536" s="T433">v-v:tense.[v:pred.pn]</ta>
            <ta e="T435" id="Seg_4537" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_4538" s="T435">cardnum</ta>
            <ta e="T437" id="Seg_4539" s="T436">adj.[n:case]</ta>
            <ta e="T438" id="Seg_4540" s="T437">v-v:cvb</ta>
            <ta e="T439" id="Seg_4541" s="T438">v-v:tense.[v:pred.pn]</ta>
            <ta e="T441" id="Seg_4542" s="T439">n-n:case</ta>
            <ta e="T442" id="Seg_4543" s="T441">v-v:cvb</ta>
            <ta e="T443" id="Seg_4544" s="T442">post</ta>
            <ta e="T449" id="Seg_4545" s="T448">ptcl</ta>
            <ta e="T454" id="Seg_4546" s="T453">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T464" id="Seg_4547" s="T461">adj.[n:case]</ta>
            <ta e="T466" id="Seg_4548" s="T464">ptcl</ta>
            <ta e="T468" id="Seg_4549" s="T466">v-v:ptcp</ta>
            <ta e="T469" id="Seg_4550" s="T468">v-v:tense-v:pred.pn</ta>
            <ta e="T471" id="Seg_4551" s="T469">v-v:ptcp</ta>
            <ta e="T473" id="Seg_4552" s="T471">v-v:tense-v:pred.pn</ta>
            <ta e="T475" id="Seg_4553" s="T473">ptcl</ta>
            <ta e="T477" id="Seg_4554" s="T475">v-v:cvb</ta>
            <ta e="T478" id="Seg_4555" s="T477">v-v:tense-v:pred.pn</ta>
            <ta e="T485" id="Seg_4556" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_4557" s="T485">dempro.[pro:case]</ta>
            <ta e="T487" id="Seg_4558" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_4559" s="T487">n-n:poss-n:case</ta>
            <ta e="T489" id="Seg_4560" s="T488">v-v:tense.[v:pred.pn]</ta>
            <ta e="T491" id="Seg_4561" s="T489">ptcl</ta>
            <ta e="T494" id="Seg_4562" s="T491">pers.[pro:case]</ta>
            <ta e="T496" id="Seg_4563" s="T494">adv-adv&gt;adv</ta>
            <ta e="T498" id="Seg_4564" s="T496">ptcl</ta>
            <ta e="T499" id="Seg_4565" s="T498">dempro</ta>
            <ta e="T501" id="Seg_4566" s="T499">dempro</ta>
            <ta e="T502" id="Seg_4567" s="T501">n-n:poss-n:case</ta>
            <ta e="T503" id="Seg_4568" s="T502">dempro.[pro:case]</ta>
            <ta e="T505" id="Seg_4569" s="T503">post</ta>
            <ta e="T506" id="Seg_4570" s="T505">v-v:tense-v:poss.pn</ta>
            <ta e="T511" id="Seg_4571" s="T510">ptcl</ta>
            <ta e="T515" id="Seg_4572" s="T514">dempro.[pro:case]</ta>
            <ta e="T516" id="Seg_4573" s="T515">post</ta>
            <ta e="T544" id="Seg_4574" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_4575" s="T544">dempro</ta>
            <ta e="T546" id="Seg_4576" s="T545">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T547" id="Seg_4577" s="T546">v-v:tense.[v:pred.pn]</ta>
            <ta e="T548" id="Seg_4578" s="T547">adv</ta>
            <ta e="T549" id="Seg_4579" s="T548">v-v:cvb</ta>
            <ta e="T550" id="Seg_4580" s="T549">v-v:cvb</ta>
            <ta e="T551" id="Seg_4581" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_4582" s="T551">dempro</ta>
            <ta e="T553" id="Seg_4583" s="T552">n-n&gt;n-n:(poss).[n:case]</ta>
            <ta e="T554" id="Seg_4584" s="T553">que-que&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T559" id="Seg_4585" s="T554">v-v:tense.[v:pred.pn]</ta>
            <ta e="T565" id="Seg_4586" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_4587" s="T565">dempro</ta>
            <ta e="T567" id="Seg_4588" s="T566">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T568" id="Seg_4589" s="T567">ptcl</ta>
            <ta e="T572" id="Seg_4590" s="T570">dempro</ta>
            <ta e="T574" id="Seg_4591" s="T572">v-v:ptcp</ta>
            <ta e="T576" id="Seg_4592" s="T574">v-v:tense-v:pred.pn</ta>
            <ta e="T578" id="Seg_4593" s="T577">v-v:cvb</ta>
            <ta e="T579" id="Seg_4594" s="T578">v-v:tense.[v:pred.pn]</ta>
            <ta e="T580" id="Seg_4595" s="T579">adv</ta>
            <ta e="T581" id="Seg_4596" s="T580">dempro</ta>
            <ta e="T582" id="Seg_4597" s="T581">v-v:tense-v:pred.pn</ta>
            <ta e="T583" id="Seg_4598" s="T582">pers.[pro:case]</ta>
            <ta e="T584" id="Seg_4599" s="T583">v-v:tense.[v:pred.pn]</ta>
            <ta e="T586" id="Seg_4600" s="T585">v-v:cvb</ta>
            <ta e="T587" id="Seg_4601" s="T586">post</ta>
            <ta e="T588" id="Seg_4602" s="T587">n-n:case</ta>
            <ta e="T589" id="Seg_4603" s="T588">v-v:tense.[v:pred.pn]</ta>
            <ta e="T590" id="Seg_4604" s="T589">dempro</ta>
            <ta e="T591" id="Seg_4605" s="T590">ptcl</ta>
            <ta e="T592" id="Seg_4606" s="T591">v.[v:mood.pn]</ta>
            <ta e="T593" id="Seg_4607" s="T592">dempro-pro:case</ta>
            <ta e="T604" id="Seg_4608" s="T601">n-n:poss-n:case</ta>
            <ta e="T608" id="Seg_4609" s="T604">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T612" id="Seg_4610" s="T610">ptcl</ta>
            <ta e="T613" id="Seg_4611" s="T612">ptcl</ta>
            <ta e="T615" id="Seg_4612" s="T613">ptcl</ta>
            <ta e="T616" id="Seg_4613" s="T615">ptcl</ta>
            <ta e="T618" id="Seg_4614" s="T616">ptcl</ta>
            <ta e="T626" id="Seg_4615" s="T624">adj.[n:case]</ta>
            <ta e="T627" id="Seg_4616" s="T626">dempro</ta>
            <ta e="T629" id="Seg_4617" s="T627">ptcl</ta>
            <ta e="T630" id="Seg_4618" s="T629">v-v:cvb</ta>
            <ta e="T631" id="Seg_4619" s="T630">v-v:tense-v:pred.pn</ta>
            <ta e="T633" id="Seg_4620" s="T631">v-v:ptcp</ta>
            <ta e="T634" id="Seg_4621" s="T633">v-v:tense-v:poss.pn</ta>
            <ta e="T636" id="Seg_4622" s="T634">ptcl</ta>
            <ta e="T637" id="Seg_4623" s="T636">v.[v:mood.pn]</ta>
            <ta e="T638" id="Seg_4624" s="T637">dempro</ta>
            <ta e="T640" id="Seg_4625" s="T638">adv</ta>
            <ta e="T649" id="Seg_4626" s="T647">v.[v:mood.pn]</ta>
            <ta e="T651" id="Seg_4627" s="T649">interj</ta>
            <ta e="T656" id="Seg_4628" s="T654">dempro</ta>
            <ta e="T658" id="Seg_4629" s="T656">ptcl</ta>
            <ta e="T660" id="Seg_4630" s="T658">dempro</ta>
            <ta e="T662" id="Seg_4631" s="T660">ptcl</ta>
            <ta e="T664" id="Seg_4632" s="T662">dempro-pro:case</ta>
            <ta e="T665" id="Seg_4633" s="T664">dempro-pro:case</ta>
            <ta e="T667" id="Seg_4634" s="T665">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T670" id="Seg_4635" s="T669">dempro</ta>
            <ta e="T671" id="Seg_4636" s="T670">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T672" id="Seg_4637" s="T671">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T676" id="Seg_4638" s="T673">n.[n:case]</ta>
            <ta e="T677" id="Seg_4639" s="T676">v-v:tense-v:pred.pn</ta>
            <ta e="T679" id="Seg_4640" s="T678">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp.[v:(case)]</ta>
            <ta e="T680" id="Seg_4641" s="T679">v-v:tense.[v:pred.pn]</ta>
            <ta e="T681" id="Seg_4642" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_4643" s="T681">v-v:(ins)-v&gt;v-v:ptcp.[v:(case)]</ta>
            <ta e="T684" id="Seg_4644" s="T683">dempro</ta>
            <ta e="T685" id="Seg_4645" s="T684">ptcl</ta>
            <ta e="T686" id="Seg_4646" s="T685">n-n:case</ta>
            <ta e="T687" id="Seg_4647" s="T686">v-v:(ins)-v&gt;v-v:ptcp.[v:(case)]</ta>
            <ta e="T688" id="Seg_4648" s="T687">v-v:tense.[v:pred.pn]</ta>
            <ta e="T690" id="Seg_4649" s="T689">dempro</ta>
            <ta e="T691" id="Seg_4650" s="T690">ptcl</ta>
            <ta e="T692" id="Seg_4651" s="T691">ptcl</ta>
            <ta e="T693" id="Seg_4652" s="T692">v-v&gt;v-v:cvb</ta>
            <ta e="T694" id="Seg_4653" s="T693">v-v:tense.[v:pred.pn]</ta>
            <ta e="T695" id="Seg_4654" s="T694">adv</ta>
            <ta e="T696" id="Seg_4655" s="T695">n-n:(poss).[n:case]</ta>
            <ta e="T697" id="Seg_4656" s="T696">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T704" id="Seg_4657" s="T702">dempro.[pro:case]</ta>
            <ta e="T705" id="Seg_4658" s="T704">post</ta>
            <ta e="T709" id="Seg_4659" s="T708">ptcl</ta>
            <ta e="T711" id="Seg_4660" s="T709">ptcl</ta>
            <ta e="T713" id="Seg_4661" s="T711">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T714" id="Seg_4662" s="T713">n-n:poss-n:case</ta>
            <ta e="T715" id="Seg_4663" s="T714">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T717" id="Seg_4664" s="T715">adv</ta>
            <ta e="T725" id="Seg_4665" s="T723">adv</ta>
            <ta e="T727" id="Seg_4666" s="T725">dempro</ta>
            <ta e="T729" id="Seg_4667" s="T727">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T735" id="Seg_4668" s="T734">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T740" id="Seg_4669" s="T739">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T741" id="Seg_4670" s="T740">v-v:tense.[v:pred.pn]</ta>
            <ta e="T742" id="Seg_4671" s="T741">v-v:cvb-v-v:cvb</ta>
            <ta e="T743" id="Seg_4672" s="T742">adj.[n:case]</ta>
            <ta e="T745" id="Seg_4673" s="T744">adv</ta>
            <ta e="T746" id="Seg_4674" s="T745">v-v:cvb</ta>
            <ta e="T747" id="Seg_4675" s="T746">v-v:tense.[v:pred.pn]</ta>
            <ta e="T749" id="Seg_4676" s="T748">adv</ta>
            <ta e="T756" id="Seg_4677" s="T752">v-v:ptcp</ta>
            <ta e="T758" id="Seg_4678" s="T756">v-v:tense.[v:pred.pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-ChGS">
            <ta e="T63" id="Seg_4679" s="T61">que</ta>
            <ta e="T65" id="Seg_4680" s="T63">ordnum</ta>
            <ta e="T70" id="Seg_4681" s="T69">n</ta>
            <ta e="T72" id="Seg_4682" s="T70">ptcl</ta>
            <ta e="T74" id="Seg_4683" s="T72">ptcl</ta>
            <ta e="T78" id="Seg_4684" s="T77">v</ta>
            <ta e="T83" id="Seg_4685" s="T82">dempro</ta>
            <ta e="T85" id="Seg_4686" s="T83">ptcl</ta>
            <ta e="T87" id="Seg_4687" s="T85">adv</ta>
            <ta e="T88" id="Seg_4688" s="T87">v</ta>
            <ta e="T94" id="Seg_4689" s="T92">adv</ta>
            <ta e="T96" id="Seg_4690" s="T94">adv</ta>
            <ta e="T99" id="Seg_4691" s="T96">adj</ta>
            <ta e="T108" id="Seg_4692" s="T105">ptcl</ta>
            <ta e="T110" id="Seg_4693" s="T109">interj</ta>
            <ta e="T111" id="Seg_4694" s="T110">dempro</ta>
            <ta e="T112" id="Seg_4695" s="T111">v</ta>
            <ta e="T114" id="Seg_4696" s="T112">aux</ta>
            <ta e="T116" id="Seg_4697" s="T114">v</ta>
            <ta e="T117" id="Seg_4698" s="T116">dempro</ta>
            <ta e="T119" id="Seg_4699" s="T117">v</ta>
            <ta e="T120" id="Seg_4700" s="T119">ptcl</ta>
            <ta e="T122" id="Seg_4701" s="T120">adj</ta>
            <ta e="T125" id="Seg_4702" s="T122">v</ta>
            <ta e="T127" id="Seg_4703" s="T125">v</ta>
            <ta e="T134" id="Seg_4704" s="T133">dempro</ta>
            <ta e="T135" id="Seg_4705" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_4706" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_4707" s="T136">ptcl</ta>
            <ta e="T154" id="Seg_4708" s="T153">adv</ta>
            <ta e="T155" id="Seg_4709" s="T154">v</ta>
            <ta e="T156" id="Seg_4710" s="T155">dempro</ta>
            <ta e="T158" id="Seg_4711" s="T156">n</ta>
            <ta e="T160" id="Seg_4712" s="T158">adj</ta>
            <ta e="T161" id="Seg_4713" s="T160">n</ta>
            <ta e="T163" id="Seg_4714" s="T161">v</ta>
            <ta e="T167" id="Seg_4715" s="T166">v</ta>
            <ta e="T169" id="Seg_4716" s="T167">ptcl</ta>
            <ta e="T176" id="Seg_4717" s="T175">adv</ta>
            <ta e="T177" id="Seg_4718" s="T176">v</ta>
            <ta e="T178" id="Seg_4719" s="T177">aux</ta>
            <ta e="T179" id="Seg_4720" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_4721" s="T179">ptcl</ta>
            <ta e="T184" id="Seg_4722" s="T182">v</ta>
            <ta e="T186" id="Seg_4723" s="T184">v</ta>
            <ta e="T188" id="Seg_4724" s="T186">v</ta>
            <ta e="T190" id="Seg_4725" s="T188">ptcl</ta>
            <ta e="T192" id="Seg_4726" s="T190">adv</ta>
            <ta e="T194" id="Seg_4727" s="T192">ptcl</ta>
            <ta e="T196" id="Seg_4728" s="T194">n</ta>
            <ta e="T198" id="Seg_4729" s="T196">dempro</ta>
            <ta e="T200" id="Seg_4730" s="T198">v</ta>
            <ta e="T203" id="Seg_4731" s="T200">post</ta>
            <ta e="T207" id="Seg_4732" s="T204">adv</ta>
            <ta e="T210" id="Seg_4733" s="T207">adj</ta>
            <ta e="T212" id="Seg_4734" s="T211">n</ta>
            <ta e="T213" id="Seg_4735" s="T212">adj</ta>
            <ta e="T215" id="Seg_4736" s="T214">v</ta>
            <ta e="T216" id="Seg_4737" s="T215">post</ta>
            <ta e="T217" id="Seg_4738" s="T216">v</ta>
            <ta e="T218" id="Seg_4739" s="T217">ptcl</ta>
            <ta e="T221" id="Seg_4740" s="T219">dempro</ta>
            <ta e="T222" id="Seg_4741" s="T221">v</ta>
            <ta e="T224" id="Seg_4742" s="T222">post</ta>
            <ta e="T226" id="Seg_4743" s="T224">v</ta>
            <ta e="T227" id="Seg_4744" s="T226">ptcl</ta>
            <ta e="T229" id="Seg_4745" s="T227">que</ta>
            <ta e="T231" id="Seg_4746" s="T229">v</ta>
            <ta e="T232" id="Seg_4747" s="T231">v</ta>
            <ta e="T234" id="Seg_4748" s="T233">v</ta>
            <ta e="T235" id="Seg_4749" s="T234">aux</ta>
            <ta e="T236" id="Seg_4750" s="T235">ptcl</ta>
            <ta e="T239" id="Seg_4751" s="T237">interj</ta>
            <ta e="T248" id="Seg_4752" s="T247">dempro</ta>
            <ta e="T250" id="Seg_4753" s="T248">ptcl</ta>
            <ta e="T251" id="Seg_4754" s="T250">dempro</ta>
            <ta e="T253" id="Seg_4755" s="T251">ptcl</ta>
            <ta e="T254" id="Seg_4756" s="T253">dempro</ta>
            <ta e="T256" id="Seg_4757" s="T254">ptcl</ta>
            <ta e="T259" id="Seg_4758" s="T258">interj</ta>
            <ta e="T260" id="Seg_4759" s="T259">ordnum</ta>
            <ta e="T261" id="Seg_4760" s="T260">v</ta>
            <ta e="T262" id="Seg_4761" s="T261">ptcl</ta>
            <ta e="T265" id="Seg_4762" s="T263">dempro</ta>
            <ta e="T266" id="Seg_4763" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_4764" s="T266">adv</ta>
            <ta e="T268" id="Seg_4765" s="T267">n</ta>
            <ta e="T270" id="Seg_4766" s="T268">v</ta>
            <ta e="T272" id="Seg_4767" s="T270">adv</ta>
            <ta e="T273" id="Seg_4768" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_4769" s="T273">dempro</ta>
            <ta e="T276" id="Seg_4770" s="T274">v</ta>
            <ta e="T279" id="Seg_4771" s="T278">n</ta>
            <ta e="T280" id="Seg_4772" s="T279">ptcl</ta>
            <ta e="T282" id="Seg_4773" s="T280">pers</ta>
            <ta e="T284" id="Seg_4774" s="T282">ptcl</ta>
            <ta e="T285" id="Seg_4775" s="T284">ptcl</ta>
            <ta e="T289" id="Seg_4776" s="T286">v</ta>
            <ta e="T291" id="Seg_4777" s="T289">ptcl</ta>
            <ta e="T313" id="Seg_4778" s="T312">v</ta>
            <ta e="T314" id="Seg_4779" s="T313">n</ta>
            <ta e="T315" id="Seg_4780" s="T314">dempro</ta>
            <ta e="T316" id="Seg_4781" s="T315">post</ta>
            <ta e="T317" id="Seg_4782" s="T316">v</ta>
            <ta e="T318" id="Seg_4783" s="T317">v</ta>
            <ta e="T319" id="Seg_4784" s="T318">aux</ta>
            <ta e="T320" id="Seg_4785" s="T319">pers</ta>
            <ta e="T328" id="Seg_4786" s="T326">adv</ta>
            <ta e="T329" id="Seg_4787" s="T328">adv</ta>
            <ta e="T331" id="Seg_4788" s="T329">v</ta>
            <ta e="T332" id="Seg_4789" s="T331">dempro</ta>
            <ta e="T333" id="Seg_4790" s="T332">v</ta>
            <ta e="T335" id="Seg_4791" s="T333">aux</ta>
            <ta e="T336" id="Seg_4792" s="T335">dempro</ta>
            <ta e="T338" id="Seg_4793" s="T336">v</ta>
            <ta e="T339" id="Seg_4794" s="T338">aux</ta>
            <ta e="T340" id="Seg_4795" s="T339">v</ta>
            <ta e="T342" id="Seg_4796" s="T341">dempro</ta>
            <ta e="T343" id="Seg_4797" s="T342">n</ta>
            <ta e="T344" id="Seg_4798" s="T343">v</ta>
            <ta e="T345" id="Seg_4799" s="T344">v</ta>
            <ta e="T346" id="Seg_4800" s="T345">n</ta>
            <ta e="T347" id="Seg_4801" s="T346">v</ta>
            <ta e="T348" id="Seg_4802" s="T347">v</ta>
            <ta e="T349" id="Seg_4803" s="T348">ptcl</ta>
            <ta e="T351" id="Seg_4804" s="T350">v</ta>
            <ta e="T352" id="Seg_4805" s="T351">ptcl</ta>
            <ta e="T353" id="Seg_4806" s="T352">ptcl</ta>
            <ta e="T356" id="Seg_4807" s="T354">adv</ta>
            <ta e="T358" id="Seg_4808" s="T356">v</ta>
            <ta e="T360" id="Seg_4809" s="T358">ptcl</ta>
            <ta e="T362" id="Seg_4810" s="T360">adv</ta>
            <ta e="T364" id="Seg_4811" s="T362">v</ta>
            <ta e="T366" id="Seg_4812" s="T364">post</ta>
            <ta e="T367" id="Seg_4813" s="T366">n</ta>
            <ta e="T368" id="Seg_4814" s="T367">v</ta>
            <ta e="T369" id="Seg_4815" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_4816" s="T369">v</ta>
            <ta e="T371" id="Seg_4817" s="T370">v</ta>
            <ta e="T372" id="Seg_4818" s="T371">dempro</ta>
            <ta e="T373" id="Seg_4819" s="T372">ptcl</ta>
            <ta e="T376" id="Seg_4820" s="T375">adv</ta>
            <ta e="T377" id="Seg_4821" s="T376">dempro</ta>
            <ta e="T378" id="Seg_4822" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_4823" s="T378">v</ta>
            <ta e="T380" id="Seg_4824" s="T379">v</ta>
            <ta e="T381" id="Seg_4825" s="T380">ptcl</ta>
            <ta e="T386" id="Seg_4826" s="T385">dempro</ta>
            <ta e="T387" id="Seg_4827" s="T386">post</ta>
            <ta e="T389" id="Seg_4828" s="T387">ptcl</ta>
            <ta e="T390" id="Seg_4829" s="T389">ptcl</ta>
            <ta e="T393" id="Seg_4830" s="T391">v</ta>
            <ta e="T395" id="Seg_4831" s="T393">adv</ta>
            <ta e="T398" id="Seg_4832" s="T397">v</ta>
            <ta e="T399" id="Seg_4833" s="T398">cardnum</ta>
            <ta e="T404" id="Seg_4834" s="T402">dempro</ta>
            <ta e="T405" id="Seg_4835" s="T404">ptcl</ta>
            <ta e="T407" id="Seg_4836" s="T405">ordnum</ta>
            <ta e="T408" id="Seg_4837" s="T407">dempro</ta>
            <ta e="T409" id="Seg_4838" s="T408">ptcl</ta>
            <ta e="T411" id="Seg_4839" s="T409">ptcl</ta>
            <ta e="T429" id="Seg_4840" s="T427">ptcl</ta>
            <ta e="T430" id="Seg_4841" s="T429">dempro</ta>
            <ta e="T431" id="Seg_4842" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_4843" s="T431">n</ta>
            <ta e="T433" id="Seg_4844" s="T432">v</ta>
            <ta e="T434" id="Seg_4845" s="T433">v</ta>
            <ta e="T435" id="Seg_4846" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_4847" s="T435">cardnum</ta>
            <ta e="T437" id="Seg_4848" s="T436">n</ta>
            <ta e="T438" id="Seg_4849" s="T437">v</ta>
            <ta e="T439" id="Seg_4850" s="T438">aux</ta>
            <ta e="T441" id="Seg_4851" s="T439">n</ta>
            <ta e="T442" id="Seg_4852" s="T441">v</ta>
            <ta e="T443" id="Seg_4853" s="T442">post</ta>
            <ta e="T449" id="Seg_4854" s="T448">ptcl</ta>
            <ta e="T454" id="Seg_4855" s="T453">v</ta>
            <ta e="T464" id="Seg_4856" s="T461">adj</ta>
            <ta e="T466" id="Seg_4857" s="T464">ptcl</ta>
            <ta e="T468" id="Seg_4858" s="T466">v</ta>
            <ta e="T469" id="Seg_4859" s="T468">aux</ta>
            <ta e="T471" id="Seg_4860" s="T469">v</ta>
            <ta e="T473" id="Seg_4861" s="T471">aux</ta>
            <ta e="T475" id="Seg_4862" s="T473">ptcl</ta>
            <ta e="T477" id="Seg_4863" s="T475">v</ta>
            <ta e="T478" id="Seg_4864" s="T477">aux</ta>
            <ta e="T485" id="Seg_4865" s="T484">ptcl</ta>
            <ta e="T486" id="Seg_4866" s="T485">dempro</ta>
            <ta e="T487" id="Seg_4867" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_4868" s="T487">n</ta>
            <ta e="T489" id="Seg_4869" s="T488">v</ta>
            <ta e="T491" id="Seg_4870" s="T489">ptcl</ta>
            <ta e="T494" id="Seg_4871" s="T491">pers</ta>
            <ta e="T496" id="Seg_4872" s="T494">adv</ta>
            <ta e="T498" id="Seg_4873" s="T496">ptcl</ta>
            <ta e="T499" id="Seg_4874" s="T498">dempro</ta>
            <ta e="T501" id="Seg_4875" s="T499">dempro</ta>
            <ta e="T502" id="Seg_4876" s="T501">n</ta>
            <ta e="T503" id="Seg_4877" s="T502">dempro</ta>
            <ta e="T505" id="Seg_4878" s="T503">post</ta>
            <ta e="T506" id="Seg_4879" s="T505">cop</ta>
            <ta e="T511" id="Seg_4880" s="T510">ptcl</ta>
            <ta e="T515" id="Seg_4881" s="T514">dempro</ta>
            <ta e="T516" id="Seg_4882" s="T515">post</ta>
            <ta e="T544" id="Seg_4883" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_4884" s="T544">dempro</ta>
            <ta e="T546" id="Seg_4885" s="T545">ptcl</ta>
            <ta e="T547" id="Seg_4886" s="T546">v</ta>
            <ta e="T548" id="Seg_4887" s="T547">adv</ta>
            <ta e="T549" id="Seg_4888" s="T548">v</ta>
            <ta e="T550" id="Seg_4889" s="T549">v</ta>
            <ta e="T551" id="Seg_4890" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_4891" s="T551">dempro</ta>
            <ta e="T553" id="Seg_4892" s="T552">n</ta>
            <ta e="T554" id="Seg_4893" s="T553">v</ta>
            <ta e="T559" id="Seg_4894" s="T554">v</ta>
            <ta e="T565" id="Seg_4895" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_4896" s="T565">dempro</ta>
            <ta e="T567" id="Seg_4897" s="T566">ptcl</ta>
            <ta e="T568" id="Seg_4898" s="T567">ptcl</ta>
            <ta e="T572" id="Seg_4899" s="T570">dempro</ta>
            <ta e="T574" id="Seg_4900" s="T572">v</ta>
            <ta e="T576" id="Seg_4901" s="T574">v</ta>
            <ta e="T578" id="Seg_4902" s="T577">v</ta>
            <ta e="T579" id="Seg_4903" s="T578">v</ta>
            <ta e="T580" id="Seg_4904" s="T579">adv</ta>
            <ta e="T581" id="Seg_4905" s="T580">dempro</ta>
            <ta e="T582" id="Seg_4906" s="T581">v</ta>
            <ta e="T583" id="Seg_4907" s="T582">pers</ta>
            <ta e="T584" id="Seg_4908" s="T583">v</ta>
            <ta e="T586" id="Seg_4909" s="T585">v</ta>
            <ta e="T587" id="Seg_4910" s="T586">post</ta>
            <ta e="T588" id="Seg_4911" s="T587">n</ta>
            <ta e="T589" id="Seg_4912" s="T588">v</ta>
            <ta e="T590" id="Seg_4913" s="T589">dempro</ta>
            <ta e="T591" id="Seg_4914" s="T590">ptcl</ta>
            <ta e="T592" id="Seg_4915" s="T591">v</ta>
            <ta e="T593" id="Seg_4916" s="T592">dempro</ta>
            <ta e="T604" id="Seg_4917" s="T601">n</ta>
            <ta e="T608" id="Seg_4918" s="T604">v</ta>
            <ta e="T612" id="Seg_4919" s="T610">ptcl</ta>
            <ta e="T613" id="Seg_4920" s="T612">ptcl</ta>
            <ta e="T615" id="Seg_4921" s="T613">ptcl</ta>
            <ta e="T616" id="Seg_4922" s="T615">ptcl</ta>
            <ta e="T618" id="Seg_4923" s="T616">ptcl</ta>
            <ta e="T626" id="Seg_4924" s="T624">adj</ta>
            <ta e="T627" id="Seg_4925" s="T626">dempro</ta>
            <ta e="T629" id="Seg_4926" s="T627">ptcl</ta>
            <ta e="T630" id="Seg_4927" s="T629">v</ta>
            <ta e="T631" id="Seg_4928" s="T630">aux</ta>
            <ta e="T633" id="Seg_4929" s="T631">v</ta>
            <ta e="T634" id="Seg_4930" s="T633">aux</ta>
            <ta e="T636" id="Seg_4931" s="T634">ptcl</ta>
            <ta e="T637" id="Seg_4932" s="T636">v</ta>
            <ta e="T638" id="Seg_4933" s="T637">dempro</ta>
            <ta e="T640" id="Seg_4934" s="T638">adv</ta>
            <ta e="T649" id="Seg_4935" s="T647">v</ta>
            <ta e="T651" id="Seg_4936" s="T649">interj</ta>
            <ta e="T656" id="Seg_4937" s="T654">dempro</ta>
            <ta e="T658" id="Seg_4938" s="T656">ptcl</ta>
            <ta e="T660" id="Seg_4939" s="T658">dempro</ta>
            <ta e="T662" id="Seg_4940" s="T660">ptcl</ta>
            <ta e="T664" id="Seg_4941" s="T662">dempro</ta>
            <ta e="T665" id="Seg_4942" s="T664">dempro</ta>
            <ta e="T667" id="Seg_4943" s="T665">v</ta>
            <ta e="T670" id="Seg_4944" s="T669">dempro</ta>
            <ta e="T671" id="Seg_4945" s="T670">ptcl</ta>
            <ta e="T672" id="Seg_4946" s="T671">v</ta>
            <ta e="T676" id="Seg_4947" s="T673">n</ta>
            <ta e="T677" id="Seg_4948" s="T676">v</ta>
            <ta e="T679" id="Seg_4949" s="T678">adj</ta>
            <ta e="T680" id="Seg_4950" s="T679">v</ta>
            <ta e="T681" id="Seg_4951" s="T680">n</ta>
            <ta e="T682" id="Seg_4952" s="T681">adj</ta>
            <ta e="T684" id="Seg_4953" s="T683">dempro</ta>
            <ta e="T685" id="Seg_4954" s="T684">ptcl</ta>
            <ta e="T686" id="Seg_4955" s="T685">n</ta>
            <ta e="T687" id="Seg_4956" s="T686">adj</ta>
            <ta e="T688" id="Seg_4957" s="T687">v</ta>
            <ta e="T690" id="Seg_4958" s="T689">dempro</ta>
            <ta e="T691" id="Seg_4959" s="T690">ptcl</ta>
            <ta e="T692" id="Seg_4960" s="T691">ptcl</ta>
            <ta e="T693" id="Seg_4961" s="T692">v</ta>
            <ta e="T694" id="Seg_4962" s="T693">aux</ta>
            <ta e="T695" id="Seg_4963" s="T694">adv</ta>
            <ta e="T696" id="Seg_4964" s="T695">n</ta>
            <ta e="T697" id="Seg_4965" s="T696">v</ta>
            <ta e="T704" id="Seg_4966" s="T702">dempro</ta>
            <ta e="T705" id="Seg_4967" s="T704">post</ta>
            <ta e="T709" id="Seg_4968" s="T708">ptcl</ta>
            <ta e="T711" id="Seg_4969" s="T709">ptcl</ta>
            <ta e="T713" id="Seg_4970" s="T711">v</ta>
            <ta e="T714" id="Seg_4971" s="T713">n</ta>
            <ta e="T715" id="Seg_4972" s="T714">v</ta>
            <ta e="T717" id="Seg_4973" s="T715">adv</ta>
            <ta e="T725" id="Seg_4974" s="T723">adv</ta>
            <ta e="T727" id="Seg_4975" s="T725">dempro</ta>
            <ta e="T729" id="Seg_4976" s="T727">v</ta>
            <ta e="T735" id="Seg_4977" s="T734">v</ta>
            <ta e="T740" id="Seg_4978" s="T739">v</ta>
            <ta e="T741" id="Seg_4979" s="T740">v</ta>
            <ta e="T742" id="Seg_4980" s="T741">v</ta>
            <ta e="T743" id="Seg_4981" s="T742">adj</ta>
            <ta e="T745" id="Seg_4982" s="T744">adv</ta>
            <ta e="T746" id="Seg_4983" s="T745">v</ta>
            <ta e="T747" id="Seg_4984" s="T746">aux</ta>
            <ta e="T749" id="Seg_4985" s="T748">adv</ta>
            <ta e="T756" id="Seg_4986" s="T752">v</ta>
            <ta e="T758" id="Seg_4987" s="T756">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-ChGS">
            <ta e="T65" id="Seg_4988" s="T63">np:Th</ta>
            <ta e="T87" id="Seg_4989" s="T85">adv:L</ta>
            <ta e="T88" id="Seg_4990" s="T87">0.3.h:Th</ta>
            <ta e="T94" id="Seg_4991" s="T92">adv:L</ta>
            <ta e="T111" id="Seg_4992" s="T110">pro:Th</ta>
            <ta e="T116" id="Seg_4993" s="T114">0.1.h:A</ta>
            <ta e="T117" id="Seg_4994" s="T116">pro:Th</ta>
            <ta e="T119" id="Seg_4995" s="T117">0.2.h:A</ta>
            <ta e="T122" id="Seg_4996" s="T120">np:Th</ta>
            <ta e="T158" id="Seg_4997" s="T156">np:Th</ta>
            <ta e="T163" id="Seg_4998" s="T161">0.3.h:A</ta>
            <ta e="T167" id="Seg_4999" s="T166">0.2.h:A</ta>
            <ta e="T176" id="Seg_5000" s="T175">adv:Time</ta>
            <ta e="T178" id="Seg_5001" s="T176">0.3.h:A</ta>
            <ta e="T188" id="Seg_5002" s="T186">0.3.h:A</ta>
            <ta e="T196" id="Seg_5003" s="T194">np.h:P</ta>
            <ta e="T210" id="Seg_5004" s="T207">0.3.h:Th</ta>
            <ta e="T212" id="Seg_5005" s="T211">np.h:Th</ta>
            <ta e="T217" id="Seg_5006" s="T216">0.3.h:A</ta>
            <ta e="T226" id="Seg_5007" s="T224">0.3.h:A</ta>
            <ta e="T235" id="Seg_5008" s="T233">0.3.h:A</ta>
            <ta e="T260" id="Seg_5009" s="T259">np:Th</ta>
            <ta e="T261" id="Seg_5010" s="T260">0.1.h:A</ta>
            <ta e="T268" id="Seg_5011" s="T267">np:So</ta>
            <ta e="T270" id="Seg_5012" s="T268">0.3.h:A</ta>
            <ta e="T276" id="Seg_5013" s="T274">0.3.h:E</ta>
            <ta e="T282" id="Seg_5014" s="T280">pro.h:Th</ta>
            <ta e="T289" id="Seg_5015" s="T286">0.3.h:A</ta>
            <ta e="T314" id="Seg_5016" s="T313">np.h:A</ta>
            <ta e="T320" id="Seg_5017" s="T319">pro:G</ta>
            <ta e="T331" id="Seg_5018" s="T329">0.2.h:A</ta>
            <ta e="T343" id="Seg_5019" s="T342">np.h:A</ta>
            <ta e="T346" id="Seg_5020" s="T345">0.2.h:Poss np.h:St</ta>
            <ta e="T347" id="Seg_5021" s="T346">0.1.h:E</ta>
            <ta e="T348" id="Seg_5022" s="T347">0.3.h:A</ta>
            <ta e="T351" id="Seg_5023" s="T350">0.3.h:E</ta>
            <ta e="T367" id="Seg_5024" s="T366">np:Th</ta>
            <ta e="T368" id="Seg_5025" s="T367">0.3.h:A</ta>
            <ta e="T371" id="Seg_5026" s="T370">0.3.h:E</ta>
            <ta e="T379" id="Seg_5027" s="T378">0.3.h:A</ta>
            <ta e="T380" id="Seg_5028" s="T379">0.3.h:A</ta>
            <ta e="T393" id="Seg_5029" s="T391">0.2.h:A</ta>
            <ta e="T395" id="Seg_5030" s="T393">adv:G</ta>
            <ta e="T398" id="Seg_5031" s="T397">0.2.h:A</ta>
            <ta e="T432" id="Seg_5032" s="T431">n:Time</ta>
            <ta e="T433" id="Seg_5033" s="T432">0.1.h:A</ta>
            <ta e="T434" id="Seg_5034" s="T433">0.3.h:A</ta>
            <ta e="T437" id="Seg_5035" s="T436">np:Th</ta>
            <ta e="T441" id="Seg_5036" s="T439">np:So</ta>
            <ta e="T454" id="Seg_5037" s="T453">0.3.h:A</ta>
            <ta e="T469" id="Seg_5038" s="T466">0.3.h:Th</ta>
            <ta e="T473" id="Seg_5039" s="T469">0.3.h:Th</ta>
            <ta e="T478" id="Seg_5040" s="T475">0.3.h:A</ta>
            <ta e="T488" id="Seg_5041" s="T487">np:Th</ta>
            <ta e="T494" id="Seg_5042" s="T491">pro.h:A</ta>
            <ta e="T496" id="Seg_5043" s="T494">adv:Time</ta>
            <ta e="T502" id="Seg_5044" s="T501">n:Time</ta>
            <ta e="T559" id="Seg_5045" s="T554">0.3.h:P</ta>
            <ta e="T576" id="Seg_5046" s="T574">0.3.h:Th</ta>
            <ta e="T579" id="Seg_5047" s="T578">0.3.h:Th</ta>
            <ta e="T582" id="Seg_5048" s="T581">0.3.h:A</ta>
            <ta e="T583" id="Seg_5049" s="T582">pro.h:E</ta>
            <ta e="T588" id="Seg_5050" s="T587">np:G</ta>
            <ta e="T589" id="Seg_5051" s="T588">0.3.h:A</ta>
            <ta e="T592" id="Seg_5052" s="T591">0.2.h:A</ta>
            <ta e="T593" id="Seg_5053" s="T592">pro:Th</ta>
            <ta e="T604" id="Seg_5054" s="T601">0.3.h:Poss np:Th</ta>
            <ta e="T608" id="Seg_5055" s="T604">0.3.h:A</ta>
            <ta e="T631" id="Seg_5056" s="T629">0.3.h:A</ta>
            <ta e="T634" id="Seg_5057" s="T631">0.3.h:Th</ta>
            <ta e="T637" id="Seg_5058" s="T636">0.2.h:A</ta>
            <ta e="T649" id="Seg_5059" s="T647">0.2.h:A</ta>
            <ta e="T664" id="Seg_5060" s="T662">pro.h:Th</ta>
            <ta e="T665" id="Seg_5061" s="T664">pro.h:Th</ta>
            <ta e="T667" id="Seg_5062" s="T665">0.3.h:A</ta>
            <ta e="T672" id="Seg_5063" s="T671">0.3.h:A</ta>
            <ta e="T676" id="Seg_5064" s="T673">np:Th</ta>
            <ta e="T677" id="Seg_5065" s="T676">0.3.h:A</ta>
            <ta e="T680" id="Seg_5066" s="T679">0.3.h:Th</ta>
            <ta e="T682" id="Seg_5067" s="T681">0.3.h:Th</ta>
            <ta e="T688" id="Seg_5068" s="T687">0.3.h:Th</ta>
            <ta e="T694" id="Seg_5069" s="T692">0.3.h:E</ta>
            <ta e="T696" id="Seg_5070" s="T695">np.h:Th</ta>
            <ta e="T697" id="Seg_5071" s="T696">0.3.h:A</ta>
            <ta e="T713" id="Seg_5072" s="T711">0.3.h:E</ta>
            <ta e="T729" id="Seg_5073" s="T727">0.3.h:A</ta>
            <ta e="T735" id="Seg_5074" s="T734">0.3.h:A</ta>
            <ta e="T740" id="Seg_5075" s="T739">0.3.h:A</ta>
            <ta e="T741" id="Seg_5076" s="T740">0.3.h:A</ta>
            <ta e="T747" id="Seg_5077" s="T745">0.3.h:A</ta>
            <ta e="T758" id="Seg_5078" s="T752">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-ChGS">
            <ta e="T63" id="Seg_5079" s="T61">pro:pred</ta>
            <ta e="T65" id="Seg_5080" s="T63">np:S</ta>
            <ta e="T78" id="Seg_5081" s="T77">v:pred</ta>
            <ta e="T88" id="Seg_5082" s="T87">0.3.h:S v:pred</ta>
            <ta e="T111" id="Seg_5083" s="T110">pro:S</ta>
            <ta e="T114" id="Seg_5084" s="T111">v:pred</ta>
            <ta e="T116" id="Seg_5085" s="T114">0.1.h:S v:pred</ta>
            <ta e="T117" id="Seg_5086" s="T116">pro:O</ta>
            <ta e="T119" id="Seg_5087" s="T117">0.2.h:S v:pred</ta>
            <ta e="T122" id="Seg_5088" s="T120">np:O</ta>
            <ta e="T127" id="Seg_5089" s="T122">s:rel</ta>
            <ta e="T155" id="Seg_5090" s="T153">s:temp</ta>
            <ta e="T158" id="Seg_5091" s="T156">np:O</ta>
            <ta e="T163" id="Seg_5092" s="T161">0.3.h:S v:pred</ta>
            <ta e="T167" id="Seg_5093" s="T166">0.2.h:S v:pred</ta>
            <ta e="T178" id="Seg_5094" s="T176">0.3.h:S v:pred</ta>
            <ta e="T186" id="Seg_5095" s="T182">s:temp</ta>
            <ta e="T188" id="Seg_5096" s="T186">0.3.h:S v:pred</ta>
            <ta e="T203" id="Seg_5097" s="T194">s:temp</ta>
            <ta e="T210" id="Seg_5098" s="T207">0.3.h:S adj:pred</ta>
            <ta e="T212" id="Seg_5099" s="T211">np.h:S</ta>
            <ta e="T213" id="Seg_5100" s="T212">adj:pred</ta>
            <ta e="T216" id="Seg_5101" s="T214">s:temp</ta>
            <ta e="T217" id="Seg_5102" s="T216">0.3.h:S v:pred</ta>
            <ta e="T224" id="Seg_5103" s="T219">s:temp</ta>
            <ta e="T226" id="Seg_5104" s="T224">0.3.h:S v:pred</ta>
            <ta e="T232" id="Seg_5105" s="T227">s:adv</ta>
            <ta e="T235" id="Seg_5106" s="T233">0.3.h:S v:pred</ta>
            <ta e="T250" id="Seg_5107" s="T248">ptcl:pred</ta>
            <ta e="T253" id="Seg_5108" s="T251">ptcl:pred</ta>
            <ta e="T256" id="Seg_5109" s="T254">ptcl:pred</ta>
            <ta e="T261" id="Seg_5110" s="T259">s:comp</ta>
            <ta e="T270" id="Seg_5111" s="T268">0.3.h:S v:pred</ta>
            <ta e="T276" id="Seg_5112" s="T274">0.3.h:S v:pred</ta>
            <ta e="T279" id="Seg_5113" s="T278">n:pred</ta>
            <ta e="T282" id="Seg_5114" s="T280">pro.h:S</ta>
            <ta e="T289" id="Seg_5115" s="T286">0.3.h:S v:pred</ta>
            <ta e="T314" id="Seg_5116" s="T313">np.h:S</ta>
            <ta e="T319" id="Seg_5117" s="T317">v:pred</ta>
            <ta e="T331" id="Seg_5118" s="T329">0.2.h:S v:pred</ta>
            <ta e="T335" id="Seg_5119" s="T331">s:adv</ta>
            <ta e="T339" id="Seg_5120" s="T335">s:adv</ta>
            <ta e="T340" id="Seg_5121" s="T339">s:adv</ta>
            <ta e="T343" id="Seg_5122" s="T342">np.h:S</ta>
            <ta e="T344" id="Seg_5123" s="T343">v:pred</ta>
            <ta e="T346" id="Seg_5124" s="T345">np.h:O</ta>
            <ta e="T347" id="Seg_5125" s="T346">0.1.h:S v:pred</ta>
            <ta e="T348" id="Seg_5126" s="T347">0.3.h:S v:pred</ta>
            <ta e="T351" id="Seg_5127" s="T350">0.3.h:S v:pred</ta>
            <ta e="T358" id="Seg_5128" s="T354">s:temp</ta>
            <ta e="T366" id="Seg_5129" s="T358">s:temp</ta>
            <ta e="T367" id="Seg_5130" s="T366">np:O</ta>
            <ta e="T368" id="Seg_5131" s="T367">0.3.h:S v:pred</ta>
            <ta e="T371" id="Seg_5132" s="T370">0.3.h:S v:pred</ta>
            <ta e="T373" id="Seg_5133" s="T372">ptcl:pred</ta>
            <ta e="T379" id="Seg_5134" s="T378">0.3.h:S v:pred</ta>
            <ta e="T380" id="Seg_5135" s="T379">0.3.h:S v:pred</ta>
            <ta e="T393" id="Seg_5136" s="T391">0.2.h:S v:pred</ta>
            <ta e="T398" id="Seg_5137" s="T397">0.2.h:S v:pred</ta>
            <ta e="T433" id="Seg_5138" s="T432">0.1.h:S</ta>
            <ta e="T434" id="Seg_5139" s="T433">0.3.h:S v:pred</ta>
            <ta e="T437" id="Seg_5140" s="T436">np:S</ta>
            <ta e="T439" id="Seg_5141" s="T437">v:pred</ta>
            <ta e="T443" id="Seg_5142" s="T439">s:temp</ta>
            <ta e="T454" id="Seg_5143" s="T453">0.3.h:S v:pred</ta>
            <ta e="T469" id="Seg_5144" s="T466">0.3.h:S v:pred</ta>
            <ta e="T473" id="Seg_5145" s="T469">0.3.h:S v:pred</ta>
            <ta e="T478" id="Seg_5146" s="T475">0.3.h:S v:pred</ta>
            <ta e="T488" id="Seg_5147" s="T487">np:O</ta>
            <ta e="T489" id="Seg_5148" s="T488">v:pred</ta>
            <ta e="T494" id="Seg_5149" s="T491">pro.h:S</ta>
            <ta e="T550" id="Seg_5150" s="T548">s:temp</ta>
            <ta e="T553" id="Seg_5151" s="T552">np.h:O</ta>
            <ta e="T554" id="Seg_5152" s="T553">0.3.h:S v:pred</ta>
            <ta e="T559" id="Seg_5153" s="T554">0.3.h:S v:pred</ta>
            <ta e="T567" id="Seg_5154" s="T566">ptcl:pred</ta>
            <ta e="T576" id="Seg_5155" s="T574">0.3.h:S v:pred</ta>
            <ta e="T578" id="Seg_5156" s="T577">s:adv</ta>
            <ta e="T579" id="Seg_5157" s="T578">0.3.h:S v:pred</ta>
            <ta e="T582" id="Seg_5158" s="T581">0.3.h:S v:pred</ta>
            <ta e="T583" id="Seg_5159" s="T582">pro.h:S</ta>
            <ta e="T584" id="Seg_5160" s="T583">v:pred</ta>
            <ta e="T587" id="Seg_5161" s="T585">s:temp</ta>
            <ta e="T589" id="Seg_5162" s="T588">0.3.h:S v:pred</ta>
            <ta e="T591" id="Seg_5163" s="T590">ptcl:pred</ta>
            <ta e="T592" id="Seg_5164" s="T591">0.2.h:S v:pred</ta>
            <ta e="T593" id="Seg_5165" s="T592">pro:O</ta>
            <ta e="T604" id="Seg_5166" s="T601">np:O</ta>
            <ta e="T608" id="Seg_5167" s="T604">0.3.h:S v:pred</ta>
            <ta e="T629" id="Seg_5168" s="T627">ptcl:pred</ta>
            <ta e="T631" id="Seg_5169" s="T629">0.3.h:S v:pred</ta>
            <ta e="T634" id="Seg_5170" s="T631">0.3.h:S v:pred</ta>
            <ta e="T637" id="Seg_5171" s="T636">0.2.h:S v:pred</ta>
            <ta e="T649" id="Seg_5172" s="T647">0.2.h:S v:pred</ta>
            <ta e="T658" id="Seg_5173" s="T656">ptcl:pred</ta>
            <ta e="T662" id="Seg_5174" s="T660">ptcl:pred</ta>
            <ta e="T664" id="Seg_5175" s="T662">pro.h:O</ta>
            <ta e="T665" id="Seg_5176" s="T664">pro.h:O</ta>
            <ta e="T667" id="Seg_5177" s="T665">0.3.h:S v:pred</ta>
            <ta e="T671" id="Seg_5178" s="T670">ptcl:pred</ta>
            <ta e="T672" id="Seg_5179" s="T671">0.3.h:S v:pred</ta>
            <ta e="T676" id="Seg_5180" s="T673">np:O</ta>
            <ta e="T677" id="Seg_5181" s="T676">0.3.h:S v:pred</ta>
            <ta e="T680" id="Seg_5182" s="T679">0.3.h:S v:pred</ta>
            <ta e="T682" id="Seg_5183" s="T681">0.3.h:S adj:pred</ta>
            <ta e="T688" id="Seg_5184" s="T687">0.3.h:S v:pred</ta>
            <ta e="T694" id="Seg_5185" s="T692">0.3.h:S v:pred</ta>
            <ta e="T697" id="Seg_5186" s="T695">s:comp</ta>
            <ta e="T713" id="Seg_5187" s="T711">0.3.h:S v:pred</ta>
            <ta e="T715" id="Seg_5188" s="T713">s:comp</ta>
            <ta e="T729" id="Seg_5189" s="T727">0.3.h:S v:pred</ta>
            <ta e="T735" id="Seg_5190" s="T734">0.3.h:S v:pred</ta>
            <ta e="T740" id="Seg_5191" s="T739">0.3.h:S v:pred</ta>
            <ta e="T741" id="Seg_5192" s="T740">0.3.h:S v:pred</ta>
            <ta e="T742" id="Seg_5193" s="T741">s:adv</ta>
            <ta e="T747" id="Seg_5194" s="T745">0.3.h:S v:pred</ta>
            <ta e="T758" id="Seg_5195" s="T752">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-ChGS" />
         <annotation name="Top" tierref="Top-ChGS" />
         <annotation name="Foc" tierref="Foc-ChGS" />
         <annotation name="BOR" tierref="BOR-ChGS">
            <ta e="T70" id="Seg_5196" s="T69">RUS:core</ta>
            <ta e="T96" id="Seg_5197" s="T94">RUS:mod</ta>
            <ta e="T194" id="Seg_5198" s="T192">RUS:mod</ta>
            <ta e="T262" id="Seg_5199" s="T261">RUS:mod</ta>
            <ta e="T284" id="Seg_5200" s="T282">RUS:mod</ta>
            <ta e="T329" id="Seg_5201" s="T328">RUS:mod</ta>
            <ta e="T352" id="Seg_5202" s="T351">RUS:mod</ta>
            <ta e="T367" id="Seg_5203" s="T366">RUS:cult</ta>
            <ta e="T368" id="Seg_5204" s="T367">RUS:core</ta>
            <ta e="T378" id="Seg_5205" s="T377">RUS:mod</ta>
            <ta e="T381" id="Seg_5206" s="T380">RUS:mod</ta>
            <ta e="T389" id="Seg_5207" s="T387">RUS:mod</ta>
            <ta e="T390" id="Seg_5208" s="T389">RUS:disc</ta>
            <ta e="T431" id="Seg_5209" s="T430">RUS:mod</ta>
            <ta e="T432" id="Seg_5210" s="T431">RUS:core</ta>
            <ta e="T435" id="Seg_5211" s="T434">RUS:mod</ta>
            <ta e="T437" id="Seg_5212" s="T436">RUS:core</ta>
            <ta e="T487" id="Seg_5213" s="T486">RUS:mod</ta>
            <ta e="T496" id="Seg_5214" s="T494">EV:gram (DIM)</ta>
            <ta e="T604" id="Seg_5215" s="T601">RUS:core</ta>
            <ta e="T612" id="Seg_5216" s="T610">RUS:disc</ta>
            <ta e="T613" id="Seg_5217" s="T612">RUS:disc</ta>
            <ta e="T615" id="Seg_5218" s="T613">RUS:disc</ta>
            <ta e="T616" id="Seg_5219" s="T615">RUS:disc</ta>
            <ta e="T618" id="Seg_5220" s="T616">RUS:disc</ta>
            <ta e="T640" id="Seg_5221" s="T638">RUS:core</ta>
            <ta e="T667" id="Seg_5222" s="T665">RUS:cult</ta>
            <ta e="T672" id="Seg_5223" s="T671">RUS:cult</ta>
            <ta e="T692" id="Seg_5224" s="T691">RUS:mod</ta>
            <ta e="T693" id="Seg_5225" s="T692">RUS:core</ta>
            <ta e="T695" id="Seg_5226" s="T694">RUS:mod</ta>
            <ta e="T697" id="Seg_5227" s="T696">RUS:core</ta>
            <ta e="T709" id="Seg_5228" s="T708">RUS:disc</ta>
            <ta e="T711" id="Seg_5229" s="T709">RUS:mod</ta>
            <ta e="T713" id="Seg_5230" s="T711">RUS:cult</ta>
            <ta e="T717" id="Seg_5231" s="T715">RUS:mod</ta>
            <ta e="T743" id="Seg_5232" s="T742">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-ChGS">
            <ta e="T432" id="Seg_5233" s="T431">Vsub lenition</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-ChGS">
            <ta e="T70" id="Seg_5234" s="T69">dir:bare</ta>
            <ta e="T96" id="Seg_5235" s="T94">dir:bare</ta>
            <ta e="T194" id="Seg_5236" s="T192">dir:bare</ta>
            <ta e="T262" id="Seg_5237" s="T261">dir:bare</ta>
            <ta e="T284" id="Seg_5238" s="T282">dir:bare</ta>
            <ta e="T329" id="Seg_5239" s="T328">dir:bare</ta>
            <ta e="T352" id="Seg_5240" s="T351">dir:bare</ta>
            <ta e="T367" id="Seg_5241" s="T366">dir:bare</ta>
            <ta e="T368" id="Seg_5242" s="T367">indir:infl</ta>
            <ta e="T378" id="Seg_5243" s="T377">dir:bare</ta>
            <ta e="T381" id="Seg_5244" s="T380">dir:bare</ta>
            <ta e="T389" id="Seg_5245" s="T387">dir:bare</ta>
            <ta e="T390" id="Seg_5246" s="T389">dir:bare</ta>
            <ta e="T431" id="Seg_5247" s="T430">dir:bare</ta>
            <ta e="T432" id="Seg_5248" s="T431">dir:infl</ta>
            <ta e="T435" id="Seg_5249" s="T434">dir:bare</ta>
            <ta e="T437" id="Seg_5250" s="T436">dir:bare</ta>
            <ta e="T487" id="Seg_5251" s="T486">dir:bare</ta>
            <ta e="T604" id="Seg_5252" s="T601">parad:infl</ta>
            <ta e="T640" id="Seg_5253" s="T638">dir:bare</ta>
            <ta e="T667" id="Seg_5254" s="T665">indir:infl</ta>
            <ta e="T672" id="Seg_5255" s="T671">indir:infl</ta>
            <ta e="T692" id="Seg_5256" s="T691">dir:bare</ta>
            <ta e="T693" id="Seg_5257" s="T692">indir:infl</ta>
            <ta e="T695" id="Seg_5258" s="T694">dir:bare</ta>
            <ta e="T697" id="Seg_5259" s="T696">indir:infl</ta>
            <ta e="T709" id="Seg_5260" s="T708">dir:bare</ta>
            <ta e="T711" id="Seg_5261" s="T709">dir:bare</ta>
            <ta e="T713" id="Seg_5262" s="T711">indir:infl</ta>
            <ta e="T717" id="Seg_5263" s="T715">dir:bare</ta>
            <ta e="T743" id="Seg_5264" s="T742">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-ChGS">
            <ta e="T9" id="Seg_5265" s="T6">RUS:ext</ta>
            <ta e="T30" id="Seg_5266" s="T25">RUS:ext</ta>
            <ta e="T58" id="Seg_5267" s="T51">RUS:ext</ta>
            <ta e="T76" id="Seg_5268" s="T74">RUS:int.ins</ta>
            <ta e="T447" id="Seg_5269" s="T443">RUS:int.ins</ta>
            <ta e="T510" id="Seg_5270" s="T507">RUS:int.alt</ta>
            <ta e="T514" id="Seg_5271" s="T511">RUS:int.alt</ta>
            <ta e="T529" id="Seg_5272" s="T517">RUS:ext</ta>
            <ta e="T537" id="Seg_5273" s="T531">RUS:ext</ta>
            <ta e="T542" id="Seg_5274" s="T538">RUS:ext</ta>
            <ta e="T618" id="Seg_5275" s="T610">RUS:ext</ta>
            <ta e="T624" id="Seg_5276" s="T621">RUS:int.alt</ta>
         </annotation>
         <annotation name="fe" tierref="fe-ChGS">
            <ta e="T9" id="Seg_5277" s="T6">– Well.</ta>
            <ta e="T30" id="Seg_5278" s="T25">– That seems (…).</ta>
            <ta e="T58" id="Seg_5279" s="T51">– Successively.</ta>
            <ta e="T65" id="Seg_5280" s="T61">– Where is the beginning?</ta>
            <ta e="T76" id="Seg_5281" s="T67">– (…) the end, though, seems to me.</ta>
            <ta e="T78" id="Seg_5282" s="T76">(…) came.</ta>
            <ta e="T88" id="Seg_5283" s="T82">There he is also sitting.</ta>
            <ta e="T99" id="Seg_5284" s="T92">– Here is probably everything.</ta>
            <ta e="T108" id="Seg_5285" s="T105">– Yes.</ta>
            <ta e="T127" id="Seg_5286" s="T109">– Wait, let it like this, let's look at these, lay all of them down where they are drinking alcohol and so on.</ta>
            <ta e="T137" id="Seg_5287" s="T133">– Here's one more.</ta>
            <ta e="T163" id="Seg_5288" s="T153">– Then he came and drank his whole money away.</ta>
            <ta e="T169" id="Seg_5289" s="T166">– You say?</ta>
            <ta e="T180" id="Seg_5290" s="T175">– At first he didn't drink, did he?</ta>
            <ta e="T203" id="Seg_5291" s="T182">– Having come he doesn't drink here, after the child had already grown.</ta>
            <ta e="T210" id="Seg_5292" s="T204">– There it is small.</ta>
            <ta e="T213" id="Seg_5293" s="T211">– The child is small.</ta>
            <ta e="T218" id="Seg_5294" s="T214">Having come he tells everything, right?</ta>
            <ta e="T232" id="Seg_5295" s="T219">Having come he tells how he was sitting [in prison].</ta>
            <ta e="T236" id="Seg_5296" s="T233">They are listening, right?</ta>
            <ta e="T239" id="Seg_5297" s="T237">Wait.</ta>
            <ta e="T256" id="Seg_5298" s="T247">– Here it is, here it is, here it is.</ta>
            <ta e="T262" id="Seg_5299" s="T258">Wait, we have to find the beginning.</ta>
            <ta e="T276" id="Seg_5300" s="T263">Here he has just come out from prison, apparently, he is happy.</ta>
            <ta e="T285" id="Seg_5301" s="T278">He is prison, seemingly. </ta>
            <ta e="T291" id="Seg_5302" s="T286">– Does he work?</ta>
            <ta e="T320" id="Seg_5303" s="T312">– The loving child would come running to him.</ta>
            <ta e="T349" id="Seg_5304" s="T326">– Look here probably, drinking alcohol, drinking alcohol they are chatting, this man is telling: "I have seen your wife", he says apparently.</ta>
            <ta e="T353" id="Seg_5305" s="T350">He's already angry here.</ta>
            <ta e="T369" id="Seg_5306" s="T354">Then having come form there, he clarifies the relationship.</ta>
            <ta e="T373" id="Seg_5307" s="T369">He wants to thrash [her], that's here. </ta>
            <ta e="T381" id="Seg_5308" s="T375">– Then he already has thrashed her, has beaten her.</ta>
            <ta e="T390" id="Seg_5309" s="T385">– Like that maybe, yes?</ta>
            <ta e="T395" id="Seg_5310" s="T391">– Put it here.</ta>
            <ta e="T401" id="Seg_5311" s="T397">– Give one (…).</ta>
            <ta e="T411" id="Seg_5312" s="T402">– Here it is, the beginning is here.</ta>
            <ta e="T447" id="Seg_5313" s="T427">– No, here already in the end, "I don't drink", he's saying already, the two last ones come after he has come from prison, I think.</ta>
            <ta e="T449" id="Seg_5314" s="T448">Yes?</ta>
            <ta e="T454" id="Seg_5315" s="T450">– (…) he doesn't drink.</ta>
            <ta e="T478" id="Seg_5316" s="T461">– They apparently were living very well, they are going to work.</ta>
            <ta e="T506" id="Seg_5317" s="T484">– Well, there he is telling already about the prison, later, that's afterwards, like this it is probably.</ta>
            <ta e="T516" id="Seg_5318" s="T507">Well, yes, let's say, may it be like that.</ta>
            <ta e="T529" id="Seg_5319" s="T517">And I have seen, that there are numbers written, are they in the right order?</ta>
            <ta e="T537" id="Seg_5320" s="T531">– One need not to look at them.</ta>
            <ta e="T542" id="Seg_5321" s="T538">I am smart, though, and see it.</ta>
            <ta e="T554" id="Seg_5322" s="T543">Well, here are these in the beginning, after he had gone to work and the drunkards whatchamacallit.</ta>
            <ta e="T559" id="Seg_5323" s="T554">He came across them.</ta>
            <ta e="T568" id="Seg_5324" s="T564">– Well, here it is.</ta>
            <ta e="T576" id="Seg_5325" s="T570">– Here they are sitting and drinking.</ta>
            <ta e="T584" id="Seg_5326" s="T577">He is sitting and drinking, then ther are telling, he is getting angry.</ta>
            <ta e="T593" id="Seg_5327" s="T585">Having gotten angry, he is going to the woman, there it is, give it.</ta>
            <ta e="T608" id="Seg_5328" s="T601">– He prepared his fist.</ta>
            <ta e="T618" id="Seg_5329" s="T610">– Yes, yes, yes, yes, yes.</ta>
            <ta e="T640" id="Seg_5330" s="T621">– And there it is good, here it is, they are working, they are living apparently, look, together.</ta>
            <ta e="T651" id="Seg_5331" s="T647">– Find them, wait.</ta>
            <ta e="T667" id="Seg_5332" s="T654">– Here it is, here, he is being questioned.</ta>
            <ta e="T672" id="Seg_5333" s="T669">– Here they are, they are questioning him.</ta>
            <ta e="T677" id="Seg_5334" s="T673">– They are giving clothes.</ta>
            <ta e="T682" id="Seg_5335" s="T678">He is imprisoned and is sitting [there], he is deep it thought.</ta>
            <ta e="T688" id="Seg_5336" s="T683">Here he is also sitting deep in thought.</ta>
            <ta e="T697" id="Seg_5337" s="T689">Here he is already dreaming, probably, how he will meet his child.</ta>
            <ta e="T705" id="Seg_5338" s="T702">– Like that.</ta>
            <ta e="T717" id="Seg_5339" s="T708">– Yes, he is probably already imagining how he'll come home.</ta>
            <ta e="T729" id="Seg_5340" s="T723">– Then he has come out.</ta>
            <ta e="T735" id="Seg_5341" s="T734">– He has come out.</ta>
            <ta e="T743" id="Seg_5342" s="T739">– He has come out, he has come, he is happy.</ta>
            <ta e="T747" id="Seg_5343" s="T744">Then he is telling.</ta>
            <ta e="T758" id="Seg_5344" s="T748">Then (…) has stopped drinking.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-ChGS">
            <ta e="T9" id="Seg_5345" s="T6">– Gut.</ta>
            <ta e="T30" id="Seg_5346" s="T25">– Das scheint (…).</ta>
            <ta e="T58" id="Seg_5347" s="T51">– Aufeinander folgend.</ta>
            <ta e="T65" id="Seg_5348" s="T61">– Wo ist der Anfang?</ta>
            <ta e="T76" id="Seg_5349" s="T67">– (…) wohl das Ende, scheint mir.</ta>
            <ta e="T78" id="Seg_5350" s="T76">(…) kam.</ta>
            <ta e="T88" id="Seg_5351" s="T82">Dort sitzt er auch.</ta>
            <ta e="T99" id="Seg_5352" s="T92">– Hier ist wahrscheinlich alles.</ta>
            <ta e="T108" id="Seg_5353" s="T105">– Ja.</ta>
            <ta e="T127" id="Seg_5354" s="T109">– Warte, lass es so stehen, lass uns diese hier angucken, leg alle die hin, wo sie Schnaps trinken und so.</ta>
            <ta e="T137" id="Seg_5355" s="T133">– Hier ist noch eins.</ta>
            <ta e="T163" id="Seg_5356" s="T153">– Dann kam er und vertrank das ganze Geld.</ta>
            <ta e="T169" id="Seg_5357" s="T166">– Sagst du?</ta>
            <ta e="T180" id="Seg_5358" s="T175">– Zuerst hat er wohl nicht getrunken, oder?</ta>
            <ta e="T203" id="Seg_5359" s="T182">– Er kam und trinkt nicht hier, nachdem das Kind schon gewachsen ist.</ta>
            <ta e="T210" id="Seg_5360" s="T204">– Dort ist es klein.</ta>
            <ta e="T213" id="Seg_5361" s="T211">– Das Kind ist klein.</ta>
            <ta e="T218" id="Seg_5362" s="T214">Nachdem er gekommen ist, erzählt er, oder?</ta>
            <ta e="T232" id="Seg_5363" s="T219">Nachdem er gekommen ist, erzählt er wohl, wie er gesessen hat.</ta>
            <ta e="T236" id="Seg_5364" s="T233">Sie hören zu, ja?</ta>
            <ta e="T239" id="Seg_5365" s="T237">Warte.</ta>
            <ta e="T256" id="Seg_5366" s="T247">– Hier ist es, hier ist es, hier ist es.</ta>
            <ta e="T262" id="Seg_5367" s="T258">Warte, wir müssen den Anfang finden.</ta>
            <ta e="T276" id="Seg_5368" s="T263">Hier ist er wohl gerade aus dem Gefängnis gekommen, er freut sich.</ta>
            <ta e="T285" id="Seg_5369" s="T278">Im Gefängnis ist er, so scheint es.</ta>
            <ta e="T291" id="Seg_5370" s="T286">– Arbeitet er?</ta>
            <ta e="T320" id="Seg_5371" s="T312">– Das liebende Kind würde so zu ihm laufen.</ta>
            <ta e="T349" id="Seg_5372" s="T326">– Schau hier wahrscheinlich, sie trinken Schnaps, sie trinken Schnaps und unterhalten sich, dieser Mensch erzählt, "ich habe deine Frau gesehen", sagt er wohl.</ta>
            <ta e="T353" id="Seg_5373" s="T350">Er ist schon böse hier.</ta>
            <ta e="T369" id="Seg_5374" s="T354">Nachdem er von dort gekommen ist, klärt er die Beziehung.</ta>
            <ta e="T373" id="Seg_5375" s="T369">Er möchte sie prügeln, das ist hier.</ta>
            <ta e="T381" id="Seg_5376" s="T375">– Dann hat er sie schon verprügelt, er hat sie schon geschlagen.</ta>
            <ta e="T390" id="Seg_5377" s="T385">– So vielleicht, ja?</ta>
            <ta e="T395" id="Seg_5378" s="T391">– Tu das hierher.</ta>
            <ta e="T401" id="Seg_5379" s="T397">– Gib eins (…).</ta>
            <ta e="T411" id="Seg_5380" s="T402">– Hier ist es, der Anfang ist hier.</ta>
            <ta e="T447" id="Seg_5381" s="T427">– Nein, hier schon am Ende, "ich trinke nicht", sagt er schon, die zwei Letzten kommen, nachdem er aus dem Gefängnis gekommen ist, denke ich.</ta>
            <ta e="T449" id="Seg_5382" s="T448">Ja?</ta>
            <ta e="T454" id="Seg_5383" s="T450">– (…) er trinkt nicht.</ta>
            <ta e="T478" id="Seg_5384" s="T461">– Sie haben sehr gut gelebt offensichtlich, sie gehen arbeiten.</ta>
            <ta e="T506" id="Seg_5385" s="T484">– Also, da erzählt er schon vom Gefängnis, später, das ist danach, so ist es wohl.</ta>
            <ta e="T516" id="Seg_5386" s="T507">Nun so, ja, sagen wir, dass es so ist.</ta>
            <ta e="T529" id="Seg_5387" s="T517">Und ich habe gesehen, dass da Zahlen geschrieben sind, sind die in der richtigen Reihenfolge?</ta>
            <ta e="T537" id="Seg_5388" s="T531">– Da muss man nicht draufgucken.</ta>
            <ta e="T542" id="Seg_5389" s="T538">Ich bin clever und sehe das.</ta>
            <ta e="T554" id="Seg_5390" s="T543">Nun hier sind die am Anfang, nachdem er arbeiten gegangen und die Trunkenbolde dingst.</ta>
            <ta e="T559" id="Seg_5391" s="T554">Er ist auf die gestoßen.</ta>
            <ta e="T568" id="Seg_5392" s="T564">– Nun, hier ist es.</ta>
            <ta e="T576" id="Seg_5393" s="T570">– Hier sitzen sie und trinken.</ta>
            <ta e="T584" id="Seg_5394" s="T577">Er sitzt und trinkt, dann erzählen sie, er wird böse.</ta>
            <ta e="T593" id="Seg_5395" s="T585">Nachdem er böse geworden ist, geht er zur Frau, hier ist es, gib das.</ta>
            <ta e="T608" id="Seg_5396" s="T601">– Er bereitete seine Faust vor.</ta>
            <ta e="T618" id="Seg_5397" s="T610">– Ja, ja, ja, ja, ja.</ta>
            <ta e="T640" id="Seg_5398" s="T621">– Und hier ist es gut, hier ist es, sie arbeiten, sie lebten offenbar, schau, hier zusammen.</ta>
            <ta e="T651" id="Seg_5399" s="T647">– Finde die, warte.</ta>
            <ta e="T667" id="Seg_5400" s="T654">– Hier ist es, hier, er wird verhört.</ta>
            <ta e="T672" id="Seg_5401" s="T669">– Hier sind sie, sie verhören ihn.</ta>
            <ta e="T677" id="Seg_5402" s="T673">– Sie geben Kleidung.</ta>
            <ta e="T682" id="Seg_5403" s="T678">Er ist eingesperrt und sitzt, er ist in Gedanken versunken.</ta>
            <ta e="T688" id="Seg_5404" s="T683">Hier sitzt er auch in Gedanken versunken.</ta>
            <ta e="T697" id="Seg_5405" s="T689">Hier träumt er schon, wahrscheinlich, wie er sein Kind trifft.</ta>
            <ta e="T705" id="Seg_5406" s="T702">– So.</ta>
            <ta e="T717" id="Seg_5407" s="T708">– Ja, er stellt sich wahrscheinlich schon vor, wie er nach Hause kommt.</ta>
            <ta e="T729" id="Seg_5408" s="T723">– Dann ist er rausgekommen.</ta>
            <ta e="T735" id="Seg_5409" s="T734">– Er ist rausgekommen.</ta>
            <ta e="T743" id="Seg_5410" s="T739">– Er ist rausgekommen, er ist gekommen, er freut sich.</ta>
            <ta e="T747" id="Seg_5411" s="T744">Dann erzählt er.</ta>
            <ta e="T758" id="Seg_5412" s="T748">Dann (…) hat er aufgehört zu trinken.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-ChGS" />
         <annotation name="ltr" tierref="ltr-ChGS">
            <ta e="T65" id="Seg_5413" s="T61">Где начала?</ta>
            <ta e="T76" id="Seg_5414" s="T67">Это наверно конец, мне кажется.</ta>
            <ta e="T78" id="Seg_5415" s="T76">Плохой пришел.</ta>
            <ta e="T88" id="Seg_5416" s="T82">Это тоже там сидит.</ta>
            <ta e="T99" id="Seg_5417" s="T92">Здесь наверно все.</ta>
            <ta e="T108" id="Seg_5418" s="T105">Аһа.</ta>
            <ta e="T127" id="Seg_5419" s="T109">Подожди, это пусть постоит вот эти посмотрим, разложи все где пьют и прочее.</ta>
            <ta e="T137" id="Seg_5420" s="T133">Вот еще есть.</ta>
            <ta e="T163" id="Seg_5421" s="T153">Оттуда приехал, все деньги пропил.</ta>
            <ta e="T169" id="Seg_5422" s="T166">Говоришь, да?</ta>
            <ta e="T180" id="Seg_5423" s="T175">Сначала он не пил кажется.</ta>
            <ta e="T203" id="Seg_5424" s="T182">Как приехал не пьет кажется здесь ребенок уже вырос.</ta>
            <ta e="T210" id="Seg_5425" s="T204">Здесь маленький.</ta>
            <ta e="T213" id="Seg_5426" s="T211">Ребенок маленький.</ta>
            <ta e="T218" id="Seg_5427" s="T214">После приезда рассказывает, да?</ta>
            <ta e="T232" id="Seg_5428" s="T219">После приезда рассказывает наверно как он сидел.</ta>
            <ta e="T236" id="Seg_5429" s="T233">Сидят слушают да?</ta>
            <ta e="T239" id="Seg_5430" s="T237">Подожди.</ta>
            <ta e="T256" id="Seg_5431" s="T247">Вот есть, вот есть, вот есть.</ta>
            <ta e="T262" id="Seg_5432" s="T258">Подожди, начало найти надо.</ta>
            <ta e="T276" id="Seg_5433" s="T263">Вот кажется с тюрьмы вышел кажется, вот радуется.</ta>
            <ta e="T285" id="Seg_5434" s="T278">А тюрьму же он, кажется.</ta>
            <ta e="T291" id="Seg_5435" s="T286">Работает что-ли?</ta>
            <ta e="T320" id="Seg_5436" s="T312">Любящий ребенок бегом прибежал бы к нему.</ta>
            <ta e="T349" id="Seg_5437" s="T326">Здесь наверно смотри, пьют сидят пьют сидят, разговаривают а этот человек рассказывает, потом говорит "жену видел" говорит.</ta>
            <ta e="T353" id="Seg_5438" s="T350">Рассердился уже здесь.</ta>
            <ta e="T369" id="Seg_5439" s="T354">Оттуда пришел, после того как пришел отношение выясняет.</ta>
            <ta e="T373" id="Seg_5440" s="T369">Бить хочет, вот есть.</ta>
            <ta e="T381" id="Seg_5441" s="T375">Потом он уже избил, ударил вот.</ta>
            <ta e="T390" id="Seg_5442" s="T385">Вот так может, да?</ta>
            <ta e="T395" id="Seg_5443" s="T391">Дай сюда.</ta>
            <ta e="T401" id="Seg_5444" s="T397">Дай один ((…))</ta>
            <ta e="T411" id="Seg_5445" s="T402">Вот это, начало вот есть ((…)).</ta>
            <ta e="T447" id="Seg_5446" s="T427">Нет, вот уже в конце, "не пью" говорит, уже две последние, после тюрьмы, я так думаю.</ta>
            <ta e="T449" id="Seg_5447" s="T448">Да?</ta>
            <ta e="T454" id="Seg_5448" s="T450">((…)) не пьет.</ta>
            <ta e="T478" id="Seg_5449" s="T461">Очень хорошо жили, жили кажется, работают.</ta>
            <ta e="T506" id="Seg_5450" s="T484">Это он уже по тюрьму рассказывает, а потом, а после этой (картинкой) вот так было бы.</ta>
            <ta e="T554" id="Seg_5451" s="T543">Ну вот же здесь когда он работал вот пьяницам присоединился.</ta>
            <ta e="T559" id="Seg_5452" s="T554">Попал.</ta>
            <ta e="T568" id="Seg_5453" s="T564">Ну вот они есть.</ta>
            <ta e="T576" id="Seg_5454" s="T570">Вот они сидят пьют уже.</ta>
            <ta e="T584" id="Seg_5455" s="T577">Пьют сидят потом вот рассказывают, он сердится.</ta>
            <ta e="T593" id="Seg_5456" s="T585">Рассердился к жене пошел, вот есть, дай ее.</ta>
            <ta e="T608" id="Seg_5457" s="T601">Кулаки приготовил ((…))).</ta>
            <ta e="T640" id="Seg_5458" s="T621">А там хорошо, вот, работают, жили, смотри, вместе.</ta>
            <ta e="T651" id="Seg_5459" s="T647">Найди, подожди.</ta>
            <ta e="T667" id="Seg_5460" s="T654">Вот, вот, его его допрашивают.</ta>
            <ta e="T672" id="Seg_5461" s="T669">Вот есть, допрашивают.</ta>
            <ta e="T677" id="Seg_5462" s="T673">Одежду дают.</ta>
            <ta e="T682" id="Seg_5463" s="T678">Заключенно сидит, раздумывает.</ta>
            <ta e="T688" id="Seg_5464" s="T683">Опять раздумывает сидит.</ta>
            <ta e="T697" id="Seg_5465" s="T689">А вот это уже мечтает сидит наверное как ребенок его встречает.</ta>
            <ta e="T705" id="Seg_5466" s="T702">Вот так.</ta>
            <ta e="T717" id="Seg_5467" s="T708">…представляет как он домой приходит, наверно.</ta>
            <ta e="T729" id="Seg_5468" s="T723">Потом он вышел.</ta>
            <ta e="T735" id="Seg_5469" s="T734">Вышел.</ta>
            <ta e="T743" id="Seg_5470" s="T739">Вышел, пришел, радуется радуется радостный.</ta>
            <ta e="T747" id="Seg_5471" s="T744">Потом сидит рассказывает.</ta>
            <ta e="T758" id="Seg_5472" s="T748">Потом ((…)) перестал пить.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-ChGS" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-UoPP"
                      id="tx-UoPP"
                      speaker="UoPP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-UoPP">
            <ts e="T26" id="Seg_5473" n="sc" s="T21">
               <ts e="T26" id="Seg_5475" n="HIAT:u" s="T21">
                  <nts id="Seg_5476" n="HIAT:ip">–</nts>
                  <nts id="Seg_5477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_5479" n="HIAT:w" s="T21">Aː</ts>
                  <nts id="Seg_5480" n="HIAT:ip">.</nts>
                  <nts id="Seg_5481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T55" id="Seg_5482" n="sc" s="T38">
               <ts e="T55" id="Seg_5484" n="HIAT:u" s="T38">
                  <nts id="Seg_5485" n="HIAT:ip">–</nts>
                  <nts id="Seg_5486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_5488" n="HIAT:w" s="T38">A</ts>
                  <nts id="Seg_5489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_5491" n="HIAT:w" s="T40">što</ts>
                  <nts id="Seg_5492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_5494" n="HIAT:w" s="T43">pa</ts>
                  <nts id="Seg_5495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_5497" n="HIAT:w" s="T45">očʼerʼedʼi</ts>
                  <nts id="Seg_5498" n="HIAT:ip">,</nts>
                  <nts id="Seg_5499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_5501" n="HIAT:w" s="T48">kak</ts>
                  <nts id="Seg_5502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_5504" n="HIAT:w" s="T50">eta</ts>
                  <nts id="Seg_5505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_5507" n="HIAT:w" s="T53">načʼinalasʼ</ts>
                  <nts id="Seg_5508" n="HIAT:ip">.</nts>
                  <nts id="Seg_5509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T73" id="Seg_5510" n="sc" s="T66">
               <ts e="T73" id="Seg_5512" n="HIAT:u" s="T66">
                  <nts id="Seg_5513" n="HIAT:ip">–</nts>
                  <nts id="Seg_5514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_5516" n="HIAT:w" s="T66">Bu</ts>
                  <nts id="Seg_5517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_5519" n="HIAT:w" s="T68">navʼernaje</ts>
                  <nts id="Seg_5520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5521" n="HIAT:ip">(</nts>
                  <nts id="Seg_5522" n="HIAT:ip">(</nts>
                  <ats e="T73" id="Seg_5523" n="HIAT:non-pho" s="T71">…</ats>
                  <nts id="Seg_5524" n="HIAT:ip">)</nts>
                  <nts id="Seg_5525" n="HIAT:ip">)</nts>
                  <nts id="Seg_5526" n="HIAT:ip">.</nts>
                  <nts id="Seg_5527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T90" id="Seg_5528" n="sc" s="T78">
               <ts e="T90" id="Seg_5530" n="HIAT:u" s="T78">
                  <nts id="Seg_5531" n="HIAT:ip">–</nts>
                  <nts id="Seg_5532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_5534" n="HIAT:w" s="T78">Eta</ts>
                  <nts id="Seg_5535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_5537" n="HIAT:w" s="T79">v</ts>
                  <nts id="Seg_5538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_5540" n="HIAT:w" s="T80">tʼurmu</ts>
                  <nts id="Seg_5541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_5543" n="HIAT:w" s="T81">on</ts>
                  <nts id="Seg_5544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_5546" n="HIAT:w" s="T84">papal</ts>
                  <nts id="Seg_5547" n="HIAT:ip">,</nts>
                  <nts id="Seg_5548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5549" n="HIAT:ip">(</nts>
                  <nts id="Seg_5550" n="HIAT:ip">(</nts>
                  <ats e="T89" id="Seg_5551" n="HIAT:non-pho" s="T86">…</ats>
                  <nts id="Seg_5552" n="HIAT:ip">)</nts>
                  <nts id="Seg_5553" n="HIAT:ip">)</nts>
                  <nts id="Seg_5554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_5556" n="HIAT:w" s="T89">oloror</ts>
                  <nts id="Seg_5557" n="HIAT:ip">.</nts>
                  <nts id="Seg_5558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T103" id="Seg_5559" n="sc" s="T91">
               <ts e="T97" id="Seg_5561" n="HIAT:u" s="T91">
                  <nts id="Seg_5562" n="HIAT:ip">–</nts>
                  <nts id="Seg_5563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_5565" n="HIAT:w" s="T91">A</ts>
                  <nts id="Seg_5566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_5568" n="HIAT:w" s="T93">onno</ts>
                  <nts id="Seg_5569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5570" n="HIAT:ip">(</nts>
                  <nts id="Seg_5571" n="HIAT:ip">(</nts>
                  <ats e="T97" id="Seg_5572" n="HIAT:non-pho" s="T95">…</ats>
                  <nts id="Seg_5573" n="HIAT:ip">)</nts>
                  <nts id="Seg_5574" n="HIAT:ip">)</nts>
                  <nts id="Seg_5575" n="HIAT:ip">.</nts>
                  <nts id="Seg_5576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_5578" n="HIAT:u" s="T97">
                  <nts id="Seg_5579" n="HIAT:ip">–</nts>
                  <nts id="Seg_5580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_5582" n="HIAT:w" s="T97">Aː</ts>
                  <nts id="Seg_5583" n="HIAT:ip">,</nts>
                  <nts id="Seg_5584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_5586" n="HIAT:w" s="T98">pa-dalganskʼi</ts>
                  <nts id="Seg_5587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_5589" n="HIAT:w" s="T101">že</ts>
                  <nts id="Seg_5590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_5592" n="HIAT:w" s="T102">naːda</ts>
                  <nts id="Seg_5593" n="HIAT:ip">.</nts>
                  <nts id="Seg_5594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T123" id="Seg_5595" n="sc" s="T113">
               <ts e="T123" id="Seg_5597" n="HIAT:u" s="T113">
                  <nts id="Seg_5598" n="HIAT:ip">–</nts>
                  <nts id="Seg_5599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_5601" n="HIAT:w" s="T113">Aragiːta</ts>
                  <nts id="Seg_5602" n="HIAT:ip">,</nts>
                  <nts id="Seg_5603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_5605" n="HIAT:w" s="T115">tak</ts>
                  <nts id="Seg_5606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_5608" n="HIAT:w" s="T118">eta</ts>
                  <nts id="Seg_5609" n="HIAT:ip">,</nts>
                  <nts id="Seg_5610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_5612" n="HIAT:w" s="T121">sʼuda</ts>
                  <nts id="Seg_5613" n="HIAT:ip">.</nts>
                  <nts id="Seg_5614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T132" id="Seg_5615" n="sc" s="T124">
               <ts e="T132" id="Seg_5617" n="HIAT:u" s="T124">
                  <nts id="Seg_5618" n="HIAT:ip">–</nts>
                  <nts id="Seg_5619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_5621" n="HIAT:w" s="T124">Tak</ts>
                  <nts id="Seg_5622" n="HIAT:ip">,</nts>
                  <nts id="Seg_5623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_5625" n="HIAT:w" s="T126">bu</ts>
                  <nts id="Seg_5626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_5628" n="HIAT:w" s="T128">itte</ts>
                  <nts id="Seg_5629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_5631" n="HIAT:w" s="T129">baːr</ts>
                  <nts id="Seg_5632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_5634" n="HIAT:w" s="T130">ebit</ts>
                  <nts id="Seg_5635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_5637" n="HIAT:w" s="T131">načʼala</ts>
                  <nts id="Seg_5638" n="HIAT:ip">.</nts>
                  <nts id="Seg_5639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_5640" n="sc" s="T138">
               <ts e="T141" id="Seg_5642" n="HIAT:u" s="T138">
                  <nts id="Seg_5643" n="HIAT:ip">–</nts>
                  <nts id="Seg_5644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_5646" n="HIAT:w" s="T138">Aragiːlɨː</ts>
                  <nts id="Seg_5647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_5649" n="HIAT:w" s="T139">olorollor</ts>
                  <nts id="Seg_5650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_5652" n="HIAT:w" s="T140">iti</ts>
                  <nts id="Seg_5653" n="HIAT:ip">.</nts>
                  <nts id="Seg_5654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_5655" n="sc" s="T142">
               <ts e="T147" id="Seg_5657" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_5659" n="HIAT:w" s="T142">Aː</ts>
                  <nts id="Seg_5660" n="HIAT:ip">,</nts>
                  <nts id="Seg_5661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_5663" n="HIAT:w" s="T143">bu</ts>
                  <nts id="Seg_5664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_5666" n="HIAT:w" s="T144">üleliː</ts>
                  <nts id="Seg_5667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_5669" n="HIAT:w" s="T145">hɨldʼɨbɨt</ts>
                  <nts id="Seg_5670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_5672" n="HIAT:w" s="T146">že</ts>
                  <nts id="Seg_5673" n="HIAT:ip">.</nts>
                  <nts id="Seg_5674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T152" id="Seg_5675" n="sc" s="T148">
               <ts e="T152" id="Seg_5677" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_5679" n="HIAT:w" s="T148">Üle</ts>
                  <nts id="Seg_5680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_5682" n="HIAT:w" s="T149">karčɨ</ts>
                  <nts id="Seg_5683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_5685" n="HIAT:w" s="T150">oŋosto</ts>
                  <nts id="Seg_5686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_5688" n="HIAT:w" s="T151">hɨldʼɨbɨt</ts>
                  <nts id="Seg_5689" n="HIAT:ip">.</nts>
                  <nts id="Seg_5690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T166" id="Seg_5691" n="sc" s="T157">
               <ts e="T166" id="Seg_5693" n="HIAT:u" s="T157">
                  <nts id="Seg_5694" n="HIAT:ip">–</nts>
                  <nts id="Seg_5695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_5697" n="HIAT:w" s="T157">Tak</ts>
                  <nts id="Seg_5698" n="HIAT:ip">,</nts>
                  <nts id="Seg_5699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_5701" n="HIAT:w" s="T159">eta</ts>
                  <nts id="Seg_5702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_5704" n="HIAT:w" s="T162">ogobut</ts>
                  <nts id="Seg_5705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_5707" n="HIAT:w" s="T164">küččügüj</ts>
                  <nts id="Seg_5708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_5710" n="HIAT:w" s="T165">manna</ts>
                  <nts id="Seg_5711" n="HIAT:ip">.</nts>
                  <nts id="Seg_5712" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T170" id="Seg_5713" n="sc" s="T168">
               <ts e="T170" id="Seg_5715" n="HIAT:u" s="T168">
                  <nts id="Seg_5716" n="HIAT:ip">–</nts>
                  <nts id="Seg_5717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_5719" n="HIAT:w" s="T168">Aha</ts>
                  <nts id="Seg_5720" n="HIAT:ip">.</nts>
                  <nts id="Seg_5721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T174" id="Seg_5722" n="sc" s="T171">
               <ts e="T174" id="Seg_5724" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_5726" n="HIAT:w" s="T171">Küččügüj</ts>
                  <nts id="Seg_5727" n="HIAT:ip">,</nts>
                  <nts id="Seg_5728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_5730" n="HIAT:w" s="T172">türmege</ts>
                  <nts id="Seg_5731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_5733" n="HIAT:w" s="T173">oloror</ts>
                  <nts id="Seg_5734" n="HIAT:ip">.</nts>
                  <nts id="Seg_5735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T201" id="Seg_5736" n="sc" s="T181">
               <ts e="T201" id="Seg_5738" n="HIAT:u" s="T181">
                  <nts id="Seg_5739" n="HIAT:ip">–</nts>
                  <nts id="Seg_5740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_5742" n="HIAT:w" s="T181">Kojut</ts>
                  <nts id="Seg_5743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_5745" n="HIAT:w" s="T183">kelen</ts>
                  <nts id="Seg_5746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_5748" n="HIAT:w" s="T185">barar</ts>
                  <nts id="Seg_5749" n="HIAT:ip">,</nts>
                  <nts id="Seg_5750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_5752" n="HIAT:w" s="T187">eta</ts>
                  <nts id="Seg_5753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_5755" n="HIAT:w" s="T189">že</ts>
                  <nts id="Seg_5756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_5758" n="HIAT:w" s="T191">kanʼec</ts>
                  <nts id="Seg_5759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_5761" n="HIAT:w" s="T193">eta</ts>
                  <nts id="Seg_5762" n="HIAT:ip">,</nts>
                  <nts id="Seg_5763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_5765" n="HIAT:w" s="T195">aː</ts>
                  <nts id="Seg_5766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_5768" n="HIAT:w" s="T197">kennite</ts>
                  <nts id="Seg_5769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_5771" n="HIAT:w" s="T199">iti</ts>
                  <nts id="Seg_5772" n="HIAT:ip">.</nts>
                  <nts id="Seg_5773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T208" id="Seg_5774" n="sc" s="T202">
               <ts e="T208" id="Seg_5776" n="HIAT:u" s="T202">
                  <nts id="Seg_5777" n="HIAT:ip">–</nts>
                  <nts id="Seg_5778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_5780" n="HIAT:w" s="T202">A</ts>
                  <nts id="Seg_5781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_5783" n="HIAT:w" s="T205">eta</ts>
                  <nts id="Seg_5784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_5786" n="HIAT:w" s="T206">što</ts>
                  <nts id="Seg_5787" n="HIAT:ip">?</nts>
                  <nts id="Seg_5788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T211" id="Seg_5789" n="sc" s="T209">
               <ts e="T211" id="Seg_5791" n="HIAT:u" s="T209">
                  <nts id="Seg_5792" n="HIAT:ip">–</nts>
                  <nts id="Seg_5793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_5795" n="HIAT:w" s="T209">Tak</ts>
                  <nts id="Seg_5796" n="HIAT:ip">.</nts>
                  <nts id="Seg_5797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T230" id="Seg_5798" n="sc" s="T220">
               <ts e="T230" id="Seg_5800" n="HIAT:u" s="T220">
                  <nts id="Seg_5801" n="HIAT:ip">–</nts>
                  <nts id="Seg_5802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_5804" n="HIAT:w" s="T220">Kelen</ts>
                  <nts id="Seg_5805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_5807" n="HIAT:w" s="T223">baran</ts>
                  <nts id="Seg_5808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_5810" n="HIAT:w" s="T225">kepsiː</ts>
                  <nts id="Seg_5811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_5813" n="HIAT:w" s="T228">oloror</ts>
                  <nts id="Seg_5814" n="HIAT:ip">.</nts>
                  <nts id="Seg_5815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T242" id="Seg_5816" n="sc" s="T238">
               <ts e="T242" id="Seg_5818" n="HIAT:u" s="T238">
                  <nts id="Seg_5819" n="HIAT:ip">–</nts>
                  <nts id="Seg_5820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_5822" n="HIAT:w" s="T238">Kajdi͡etij</ts>
                  <nts id="Seg_5823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_5825" n="HIAT:w" s="T240">ke</ts>
                  <nts id="Seg_5826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_5828" n="HIAT:w" s="T241">bajdʼɨŋɨbɨt</ts>
                  <nts id="Seg_5829" n="HIAT:ip">?</nts>
                  <nts id="Seg_5830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T257" id="Seg_5831" n="sc" s="T243">
               <ts e="T257" id="Seg_5833" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_5835" n="HIAT:w" s="T243">A</ts>
                  <nts id="Seg_5836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_5838" n="HIAT:w" s="T244">kanna</ts>
                  <nts id="Seg_5839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_5841" n="HIAT:w" s="T245">baːrɨj</ts>
                  <nts id="Seg_5842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_5844" n="HIAT:w" s="T246">bajdʼɨŋ</ts>
                  <nts id="Seg_5845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_5847" n="HIAT:w" s="T249">körsöllör</ts>
                  <nts id="Seg_5848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_5850" n="HIAT:w" s="T252">haŋa</ts>
                  <nts id="Seg_5851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5852" n="HIAT:ip">(</nts>
                  <ts e="T257" id="Seg_5854" n="HIAT:w" s="T255">kiːr-</ts>
                  <nts id="Seg_5855" n="HIAT:ip">)</nts>
                  <nts id="Seg_5856" n="HIAT:ip">.</nts>
                  <nts id="Seg_5857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T269" id="Seg_5858" n="sc" s="T264">
               <ts e="T269" id="Seg_5860" n="HIAT:u" s="T264">
                  <nts id="Seg_5861" n="HIAT:ip">–</nts>
                  <nts id="Seg_5862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_5864" n="HIAT:w" s="T264">Naːda</ts>
                  <nts id="Seg_5865" n="HIAT:ip">.</nts>
                  <nts id="Seg_5866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T281" id="Seg_5867" n="sc" s="T271">
               <ts e="T281" id="Seg_5869" n="HIAT:u" s="T271">
                  <nts id="Seg_5870" n="HIAT:ip">–</nts>
                  <nts id="Seg_5871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_5873" n="HIAT:w" s="T271">Üleliː</ts>
                  <nts id="Seg_5874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_5876" n="HIAT:w" s="T275">barar</ts>
                  <nts id="Seg_5877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_5879" n="HIAT:w" s="T277">bu</ts>
                  <nts id="Seg_5880" n="HIAT:ip">.</nts>
                  <nts id="Seg_5881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T293" id="Seg_5882" n="sc" s="T283">
               <ts e="T293" id="Seg_5884" n="HIAT:u" s="T283">
                  <nts id="Seg_5885" n="HIAT:ip">–</nts>
                  <nts id="Seg_5886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_5888" n="HIAT:w" s="T283">Hu͡ok</ts>
                  <nts id="Seg_5889" n="HIAT:ip">,</nts>
                  <nts id="Seg_5890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_5892" n="HIAT:w" s="T287">hu͡ok</ts>
                  <nts id="Seg_5893" n="HIAT:ip">,</nts>
                  <nts id="Seg_5894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_5896" n="HIAT:w" s="T288">aː</ts>
                  <nts id="Seg_5897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_5899" n="HIAT:w" s="T290">beːbe</ts>
                  <nts id="Seg_5900" n="HIAT:ip">,</nts>
                  <nts id="Seg_5901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_5903" n="HIAT:w" s="T292">körü͡ökpüt</ts>
                  <nts id="Seg_5904" n="HIAT:ip">.</nts>
                  <nts id="Seg_5905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T300" id="Seg_5906" n="sc" s="T294">
               <ts e="T300" id="Seg_5908" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_5910" n="HIAT:w" s="T294">Tʼortʼü</ts>
                  <nts id="Seg_5911" n="HIAT:ip">,</nts>
                  <nts id="Seg_5912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_5914" n="HIAT:w" s="T295">čʼort</ts>
                  <nts id="Seg_5915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_5917" n="HIAT:w" s="T296">jevo</ts>
                  <nts id="Seg_5918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_5920" n="HIAT:w" s="T297">znajet</ts>
                  <nts id="Seg_5921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_5923" n="HIAT:w" s="T298">xatʼela</ts>
                  <nts id="Seg_5924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_5926" n="HIAT:w" s="T299">skazatʼ</ts>
                  <nts id="Seg_5927" n="HIAT:ip">.</nts>
                  <nts id="Seg_5928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T306" id="Seg_5929" n="sc" s="T301">
               <ts e="T306" id="Seg_5931" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_5933" n="HIAT:w" s="T301">Tak</ts>
                  <nts id="Seg_5934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_5936" n="HIAT:w" s="T302">rebʼonok</ts>
                  <nts id="Seg_5937" n="HIAT:ip">,</nts>
                  <nts id="Seg_5938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_5940" n="HIAT:w" s="T303">ogobut</ts>
                  <nts id="Seg_5941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_5943" n="HIAT:w" s="T304">küččügüj</ts>
                  <nts id="Seg_5944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_5946" n="HIAT:w" s="T305">itinne</ts>
                  <nts id="Seg_5947" n="HIAT:ip">.</nts>
                  <nts id="Seg_5948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T311" id="Seg_5949" n="sc" s="T307">
               <ts e="T311" id="Seg_5951" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_5953" n="HIAT:w" s="T307">Tak</ts>
                  <nts id="Seg_5954" n="HIAT:ip">,</nts>
                  <nts id="Seg_5955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_5957" n="HIAT:w" s="T308">bu</ts>
                  <nts id="Seg_5958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_5960" n="HIAT:w" s="T309">kaːjɨːga</ts>
                  <nts id="Seg_5961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_5963" n="HIAT:w" s="T310">oloror</ts>
                  <nts id="Seg_5964" n="HIAT:ip">.</nts>
                  <nts id="Seg_5965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T325" id="Seg_5966" n="sc" s="T321">
               <ts e="T325" id="Seg_5968" n="HIAT:u" s="T321">
                  <nts id="Seg_5969" n="HIAT:ip">–</nts>
                  <nts id="Seg_5970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_5972" n="HIAT:w" s="T321">Bu</ts>
                  <nts id="Seg_5973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_5975" n="HIAT:w" s="T322">künüːleːn</ts>
                  <nts id="Seg_5976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_5977" n="HIAT:ip">(</nts>
                  <ts e="T324" id="Seg_5979" n="HIAT:w" s="T323">dʼak-</ts>
                  <nts id="Seg_5980" n="HIAT:ip">)</nts>
                  <nts id="Seg_5981" n="HIAT:ip">,</nts>
                  <nts id="Seg_5982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_5984" n="HIAT:w" s="T324">padaždʼi</ts>
                  <nts id="Seg_5985" n="HIAT:ip">.</nts>
                  <nts id="Seg_5986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T337" id="Seg_5987" n="sc" s="T327">
               <ts e="T337" id="Seg_5989" n="HIAT:u" s="T327">
                  <nts id="Seg_5990" n="HIAT:ip">–</nts>
                  <nts id="Seg_5991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_5993" n="HIAT:w" s="T327">Dʼaktarɨn</ts>
                  <nts id="Seg_5994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_5996" n="HIAT:w" s="T330">kɨrbɨːr</ts>
                  <nts id="Seg_5997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_5999" n="HIAT:w" s="T334">ete</ts>
                  <nts id="Seg_6000" n="HIAT:ip">.</nts>
                  <nts id="Seg_6001" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T365" id="Seg_6002" n="sc" s="T355">
               <ts e="T365" id="Seg_6004" n="HIAT:u" s="T355">
                  <nts id="Seg_6005" n="HIAT:ip">–</nts>
                  <nts id="Seg_6006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_6008" n="HIAT:w" s="T355">E</ts>
                  <nts id="Seg_6009" n="HIAT:ip">,</nts>
                  <nts id="Seg_6010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_6012" n="HIAT:w" s="T357">onton</ts>
                  <nts id="Seg_6013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_6015" n="HIAT:w" s="T359">kɨrbɨːr</ts>
                  <nts id="Seg_6016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_6018" n="HIAT:w" s="T361">i͡e</ts>
                  <nts id="Seg_6019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_6021" n="HIAT:w" s="T363">diː</ts>
                  <nts id="Seg_6022" n="HIAT:ip">?</nts>
                  <nts id="Seg_6023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T374" id="Seg_6024" n="sc" s="T373">
               <ts e="T374" id="Seg_6026" n="HIAT:u" s="T373">
                  <nts id="Seg_6027" n="HIAT:ip">–</nts>
                  <nts id="Seg_6028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_6030" n="HIAT:w" s="T373">Aha</ts>
                  <nts id="Seg_6031" n="HIAT:ip">.</nts>
                  <nts id="Seg_6032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T383" id="Seg_6033" n="sc" s="T382">
               <ts e="T383" id="Seg_6035" n="HIAT:u" s="T382">
                  <nts id="Seg_6036" n="HIAT:ip">–</nts>
                  <nts id="Seg_6037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_6039" n="HIAT:w" s="T382">Aha</ts>
                  <nts id="Seg_6040" n="HIAT:ip">.</nts>
                  <nts id="Seg_6041" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T392" id="Seg_6042" n="sc" s="T384">
               <ts e="T392" id="Seg_6044" n="HIAT:u" s="T384">
                  <ts e="T388" id="Seg_6046" n="HIAT:w" s="T384">Da</ts>
                  <nts id="Seg_6047" n="HIAT:ip">,</nts>
                  <nts id="Seg_6048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_6050" n="HIAT:w" s="T388">sʼuda</ts>
                  <nts id="Seg_6051" n="HIAT:ip">.</nts>
                  <nts id="Seg_6052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T396" id="Seg_6053" n="sc" s="T394">
               <ts e="T396" id="Seg_6055" n="HIAT:u" s="T394">
                  <nts id="Seg_6056" n="HIAT:ip">–</nts>
                  <nts id="Seg_6057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_6059" n="HIAT:w" s="T394">Tak</ts>
                  <nts id="Seg_6060" n="HIAT:ip">.</nts>
                  <nts id="Seg_6061" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T410" id="Seg_6062" n="sc" s="T400">
               <ts e="T410" id="Seg_6064" n="HIAT:u" s="T400">
                  <nts id="Seg_6065" n="HIAT:ip">–</nts>
                  <nts id="Seg_6066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_6068" n="HIAT:w" s="T400">Mʼilʼisʼijeler</ts>
                  <nts id="Seg_6069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_6071" n="HIAT:w" s="T403">kelen</ts>
                  <nts id="Seg_6072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_6074" n="HIAT:w" s="T406">ilpitter</ts>
                  <nts id="Seg_6075" n="HIAT:ip">.</nts>
                  <nts id="Seg_6076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T418" id="Seg_6077" n="sc" s="T412">
               <ts e="T418" id="Seg_6079" n="HIAT:u" s="T412">
                  <nts id="Seg_6080" n="HIAT:ip">–</nts>
                  <nts id="Seg_6081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_6083" n="HIAT:w" s="T412">Mʼilʼisʼijeler</ts>
                  <nts id="Seg_6084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_6086" n="HIAT:w" s="T413">kelen</ts>
                  <nts id="Seg_6087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_6089" n="HIAT:w" s="T414">ilpitter</ts>
                  <nts id="Seg_6090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_6092" n="HIAT:w" s="T415">onton</ts>
                  <nts id="Seg_6093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_6095" n="HIAT:w" s="T416">muntugun</ts>
                  <nts id="Seg_6096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_6098" n="HIAT:w" s="T417">oksubutugar</ts>
                  <nts id="Seg_6099" n="HIAT:ip">.</nts>
                  <nts id="Seg_6100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T428" id="Seg_6101" n="sc" s="T419">
               <ts e="T428" id="Seg_6103" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_6105" n="HIAT:w" s="T419">Kɨrbaːbat</ts>
                  <nts id="Seg_6106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_6108" n="HIAT:w" s="T420">du͡o</ts>
                  <nts id="Seg_6109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_6111" n="HIAT:w" s="T421">itinne</ts>
                  <nts id="Seg_6112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_6114" n="HIAT:w" s="T422">kajdi͡ek</ts>
                  <nts id="Seg_6115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_6117" n="HIAT:w" s="T423">da</ts>
                  <nts id="Seg_6118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_6120" n="HIAT:w" s="T424">hu͡ok</ts>
                  <nts id="Seg_6121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_6123" n="HIAT:w" s="T425">du͡o</ts>
                  <nts id="Seg_6124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_6126" n="HIAT:w" s="T426">bolʼše</ts>
                  <nts id="Seg_6127" n="HIAT:ip">.</nts>
                  <nts id="Seg_6128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T444" id="Seg_6129" n="sc" s="T440">
               <ts e="T444" id="Seg_6131" n="HIAT:u" s="T440">
                  <nts id="Seg_6132" n="HIAT:ip">–</nts>
                  <nts id="Seg_6133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_6135" n="HIAT:w" s="T440">Aha</ts>
                  <nts id="Seg_6136" n="HIAT:ip">.</nts>
                  <nts id="Seg_6137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T452" id="Seg_6138" n="sc" s="T451">
               <ts e="T452" id="Seg_6140" n="HIAT:u" s="T451">
                  <nts id="Seg_6141" n="HIAT:ip">–</nts>
                  <nts id="Seg_6142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_6144" n="HIAT:w" s="T451">Heː</ts>
                  <nts id="Seg_6145" n="HIAT:ip">.</nts>
                  <nts id="Seg_6146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T462" id="Seg_6147" n="sc" s="T455">
               <ts e="T462" id="Seg_6149" n="HIAT:u" s="T455">
                  <nts id="Seg_6150" n="HIAT:ip">–</nts>
                  <nts id="Seg_6151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_6153" n="HIAT:w" s="T455">Tak</ts>
                  <nts id="Seg_6154" n="HIAT:ip">,</nts>
                  <nts id="Seg_6155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_6157" n="HIAT:w" s="T456">bu</ts>
                  <nts id="Seg_6158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_6160" n="HIAT:w" s="T457">ke</ts>
                  <nts id="Seg_6161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_6163" n="HIAT:w" s="T458">ogoto</ts>
                  <nts id="Seg_6164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_6166" n="HIAT:w" s="T459">küččügüj</ts>
                  <nts id="Seg_6167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_6169" n="HIAT:w" s="T460">erdegine</ts>
                  <nts id="Seg_6170" n="HIAT:ip">.</nts>
                  <nts id="Seg_6171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T476" id="Seg_6172" n="sc" s="T463">
               <ts e="T476" id="Seg_6174" n="HIAT:u" s="T463">
                  <nts id="Seg_6175" n="HIAT:ip">–</nts>
                  <nts id="Seg_6176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_6178" n="HIAT:w" s="T463">Üčügej</ts>
                  <nts id="Seg_6179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_6181" n="HIAT:w" s="T465">bagajdɨk</ts>
                  <nts id="Seg_6182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_6184" n="HIAT:w" s="T467">oloror</ts>
                  <nts id="Seg_6185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_6187" n="HIAT:w" s="T470">etiler</ts>
                  <nts id="Seg_6188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_6190" n="HIAT:w" s="T472">eni</ts>
                  <nts id="Seg_6191" n="HIAT:ip">,</nts>
                  <nts id="Seg_6192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_6194" n="HIAT:w" s="T474">hmm</ts>
                  <nts id="Seg_6195" n="HIAT:ip">.</nts>
                  <nts id="Seg_6196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T483" id="Seg_6197" n="sc" s="T479">
               <ts e="T483" id="Seg_6199" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_6201" n="HIAT:w" s="T479">Üleliːr</ts>
                  <nts id="Seg_6202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_6204" n="HIAT:w" s="T480">hɨldʼallar</ts>
                  <nts id="Seg_6205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_6207" n="HIAT:w" s="T481">tak</ts>
                  <nts id="Seg_6208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_6210" n="HIAT:w" s="T482">da</ts>
                  <nts id="Seg_6211" n="HIAT:ip">.</nts>
                  <nts id="Seg_6212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T497" id="Seg_6213" n="sc" s="T490">
               <ts e="T497" id="Seg_6215" n="HIAT:u" s="T490">
                  <nts id="Seg_6216" n="HIAT:ip">–</nts>
                  <nts id="Seg_6217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_6219" n="HIAT:w" s="T490">E</ts>
                  <nts id="Seg_6220" n="HIAT:ip">,</nts>
                  <nts id="Seg_6221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_6223" n="HIAT:w" s="T492">kelen</ts>
                  <nts id="Seg_6224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_6226" n="HIAT:w" s="T493">baran</ts>
                  <nts id="Seg_6227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_6229" n="HIAT:w" s="T495">du</ts>
                  <nts id="Seg_6230" n="HIAT:ip">?</nts>
                  <nts id="Seg_6231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T504" id="Seg_6232" n="sc" s="T500">
               <ts e="T504" id="Seg_6234" n="HIAT:u" s="T500">
                  <nts id="Seg_6235" n="HIAT:ip">–</nts>
                  <nts id="Seg_6236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_6238" n="HIAT:w" s="T500">Aha</ts>
                  <nts id="Seg_6239" n="HIAT:ip">.</nts>
                  <nts id="Seg_6240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T563" id="Seg_6241" n="sc" s="T555">
               <ts e="T563" id="Seg_6243" n="HIAT:u" s="T555">
                  <nts id="Seg_6244" n="HIAT:ip">–</nts>
                  <nts id="Seg_6245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_6247" n="HIAT:w" s="T555">Tak</ts>
                  <nts id="Seg_6248" n="HIAT:ip">,</nts>
                  <nts id="Seg_6249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_6251" n="HIAT:w" s="T556">onton</ts>
                  <nts id="Seg_6252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_6254" n="HIAT:w" s="T557">načʼala</ts>
                  <nts id="Seg_6255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_6257" n="HIAT:w" s="T558">kajdi͡ek</ts>
                  <nts id="Seg_6258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_6260" n="HIAT:w" s="T560">baːr</ts>
                  <nts id="Seg_6261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_6263" n="HIAT:w" s="T561">aragiːmsak</ts>
                  <nts id="Seg_6264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_6266" n="HIAT:w" s="T562">bu͡olbuta</ts>
                  <nts id="Seg_6267" n="HIAT:ip">.</nts>
                  <nts id="Seg_6268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T575" id="Seg_6269" n="sc" s="T569">
               <ts e="T575" id="Seg_6271" n="HIAT:u" s="T569">
                  <nts id="Seg_6272" n="HIAT:ip">–</nts>
                  <nts id="Seg_6273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_6275" n="HIAT:w" s="T569">Eː</ts>
                  <nts id="Seg_6276" n="HIAT:ip">,</nts>
                  <nts id="Seg_6277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_6279" n="HIAT:w" s="T571">bu</ts>
                  <nts id="Seg_6280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_6282" n="HIAT:w" s="T573">kepsiːr</ts>
                  <nts id="Seg_6283" n="HIAT:ip">.</nts>
                  <nts id="Seg_6284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T600" id="Seg_6285" n="sc" s="T594">
               <ts e="T600" id="Seg_6287" n="HIAT:u" s="T594">
                  <nts id="Seg_6288" n="HIAT:ip">–</nts>
                  <nts id="Seg_6289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_6291" n="HIAT:w" s="T594">Tak</ts>
                  <nts id="Seg_6292" n="HIAT:ip">,</nts>
                  <nts id="Seg_6293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_6295" n="HIAT:w" s="T595">dva</ts>
                  <nts id="Seg_6296" n="HIAT:ip">,</nts>
                  <nts id="Seg_6297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_6299" n="HIAT:w" s="T596">tak</ts>
                  <nts id="Seg_6300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_6302" n="HIAT:w" s="T597">da</ts>
                  <nts id="Seg_6303" n="HIAT:ip">,</nts>
                  <nts id="Seg_6304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_6306" n="HIAT:w" s="T598">dʼaktarga</ts>
                  <nts id="Seg_6307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_6309" n="HIAT:w" s="T599">barbɨt</ts>
                  <nts id="Seg_6310" n="HIAT:ip">.</nts>
                  <nts id="Seg_6311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T614" id="Seg_6312" n="sc" s="T602">
               <ts e="T614" id="Seg_6314" n="HIAT:u" s="T602">
                  <nts id="Seg_6315" n="HIAT:ip">–</nts>
                  <nts id="Seg_6316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6317" n="HIAT:ip">"</nts>
                  <ts e="T603" id="Seg_6319" n="HIAT:w" s="T602">Hɨt</ts>
                  <nts id="Seg_6320" n="HIAT:ip">,</nts>
                  <nts id="Seg_6321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_6323" n="HIAT:w" s="T603">ol</ts>
                  <nts id="Seg_6324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_6326" n="HIAT:w" s="T605">kihini</ts>
                  <nts id="Seg_6327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_6329" n="HIAT:w" s="T606">gɨtta</ts>
                  <nts id="Seg_6330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_6332" n="HIAT:w" s="T607">bugurduk</ts>
                  <nts id="Seg_6333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_6335" n="HIAT:w" s="T609">hɨldʼɨbɨkkɨn</ts>
                  <nts id="Seg_6336" n="HIAT:ip">"</nts>
                  <nts id="Seg_6337" n="HIAT:ip">,</nts>
                  <nts id="Seg_6338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_6340" n="HIAT:w" s="T611">diːr</ts>
                  <nts id="Seg_6341" n="HIAT:ip">.</nts>
                  <nts id="Seg_6342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T632" id="Seg_6343" n="sc" s="T617">
               <ts e="T628" id="Seg_6345" n="HIAT:u" s="T617">
                  <nts id="Seg_6346" n="HIAT:ip">–</nts>
                  <nts id="Seg_6347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_6349" n="HIAT:w" s="T617">Onton</ts>
                  <nts id="Seg_6350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_6352" n="HIAT:w" s="T619">oksubut</ts>
                  <nts id="Seg_6353" n="HIAT:ip">,</nts>
                  <nts id="Seg_6354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_6356" n="HIAT:w" s="T620">onton</ts>
                  <nts id="Seg_6357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_6359" n="HIAT:w" s="T622">mʼilʼisʼijeler</ts>
                  <nts id="Seg_6360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_6362" n="HIAT:w" s="T625">kelbitter</ts>
                  <nts id="Seg_6363" n="HIAT:ip">.</nts>
                  <nts id="Seg_6364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_6366" n="HIAT:u" s="T628">
                  <nts id="Seg_6367" n="HIAT:ip">–</nts>
                  <nts id="Seg_6368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_6370" n="HIAT:w" s="T628">Aha</ts>
                  <nts id="Seg_6371" n="HIAT:ip">.</nts>
                  <nts id="Seg_6372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T639" id="Seg_6373" n="sc" s="T635">
               <ts e="T639" id="Seg_6375" n="HIAT:u" s="T635">
                  <nts id="Seg_6376" n="HIAT:ip">–</nts>
                  <nts id="Seg_6377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_6379" n="HIAT:w" s="T635">Tak</ts>
                  <nts id="Seg_6380" n="HIAT:ip">.</nts>
                  <nts id="Seg_6381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T643" id="Seg_6382" n="sc" s="T641">
               <ts e="T643" id="Seg_6384" n="HIAT:u" s="T641">
                  <nts id="Seg_6385" n="HIAT:ip">–</nts>
                  <nts id="Seg_6386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_6388" n="HIAT:w" s="T641">Tak</ts>
                  <nts id="Seg_6389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_6391" n="HIAT:w" s="T642">ešʼo</ts>
                  <nts id="Seg_6392" n="HIAT:ip">.</nts>
                  <nts id="Seg_6393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T652" id="Seg_6394" n="sc" s="T644">
               <ts e="T652" id="Seg_6396" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_6398" n="HIAT:w" s="T644">A</ts>
                  <nts id="Seg_6399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_6401" n="HIAT:w" s="T645">vot</ts>
                  <nts id="Seg_6402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_6404" n="HIAT:w" s="T646">etʼi</ts>
                  <nts id="Seg_6405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_6407" n="HIAT:w" s="T648">pasmatrʼetʼ</ts>
                  <nts id="Seg_6408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_6410" n="HIAT:w" s="T650">naːda</ts>
                  <nts id="Seg_6411" n="HIAT:ip">.</nts>
                  <nts id="Seg_6412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T659" id="Seg_6413" n="sc" s="T653">
               <ts e="T659" id="Seg_6415" n="HIAT:u" s="T653">
                  <nts id="Seg_6416" n="HIAT:ip">–</nts>
                  <nts id="Seg_6417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_6419" n="HIAT:w" s="T653">Načala</ts>
                  <nts id="Seg_6420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_6422" n="HIAT:w" s="T655">baːr</ts>
                  <nts id="Seg_6423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_6425" n="HIAT:w" s="T657">taŋas</ts>
                  <nts id="Seg_6426" n="HIAT:ip">.</nts>
                  <nts id="Seg_6427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T668" id="Seg_6428" n="sc" s="T661">
               <ts e="T668" id="Seg_6430" n="HIAT:u" s="T661">
                  <nts id="Seg_6431" n="HIAT:ip">–</nts>
                  <nts id="Seg_6432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_6434" n="HIAT:w" s="T661">Aː</ts>
                  <nts id="Seg_6435" n="HIAT:ip">,</nts>
                  <nts id="Seg_6436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_6438" n="HIAT:w" s="T663">vot</ts>
                  <nts id="Seg_6439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_6441" n="HIAT:w" s="T666">ješʼo</ts>
                  <nts id="Seg_6442" n="HIAT:ip">.</nts>
                  <nts id="Seg_6443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T675" id="Seg_6444" n="sc" s="T674">
               <ts e="T675" id="Seg_6446" n="HIAT:u" s="T674">
                  <nts id="Seg_6447" n="HIAT:ip">–</nts>
                  <nts id="Seg_6448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_6450" n="HIAT:w" s="T674">Taŋas</ts>
                  <nts id="Seg_6451" n="HIAT:ip">.</nts>
                  <nts id="Seg_6452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T703" id="Seg_6453" n="sc" s="T698">
               <ts e="T703" id="Seg_6455" n="HIAT:u" s="T698">
                  <nts id="Seg_6456" n="HIAT:ip">–</nts>
                  <nts id="Seg_6457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6458" n="HIAT:ip">(</nts>
                  <nts id="Seg_6459" n="HIAT:ip">(</nts>
                  <ats e="T699" id="Seg_6460" n="HIAT:non-pho" s="T698">…</ats>
                  <nts id="Seg_6461" n="HIAT:ip">)</nts>
                  <nts id="Seg_6462" n="HIAT:ip">)</nts>
                  <nts id="Seg_6463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_6465" n="HIAT:w" s="T699">mʼilʼisʼijeler</ts>
                  <nts id="Seg_6466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_6468" n="HIAT:w" s="T700">kɨrbɨːllar</ts>
                  <nts id="Seg_6469" n="HIAT:ip">,</nts>
                  <nts id="Seg_6470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_6472" n="HIAT:w" s="T701">kanʼɨːllar</ts>
                  <nts id="Seg_6473" n="HIAT:ip">.</nts>
                  <nts id="Seg_6474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T707" id="Seg_6475" n="sc" s="T706">
               <ts e="T707" id="Seg_6477" n="HIAT:u" s="T706">
                  <nts id="Seg_6478" n="HIAT:ip">–</nts>
                  <nts id="Seg_6479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_6481" n="HIAT:w" s="T706">Onton</ts>
                  <nts id="Seg_6482" n="HIAT:ip">.</nts>
                  <nts id="Seg_6483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T712" id="Seg_6484" n="sc" s="T710">
               <ts e="T712" id="Seg_6486" n="HIAT:u" s="T710">
                  <nts id="Seg_6487" n="HIAT:ip">–</nts>
                  <nts id="Seg_6488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_6490" n="HIAT:w" s="T710">Tak</ts>
                  <nts id="Seg_6491" n="HIAT:ip">.</nts>
                  <nts id="Seg_6492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T721" id="Seg_6493" n="sc" s="T716">
               <ts e="T721" id="Seg_6495" n="HIAT:u" s="T716">
                  <nts id="Seg_6496" n="HIAT:ip">–</nts>
                  <nts id="Seg_6497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_6499" n="HIAT:w" s="T716">Aː</ts>
                  <nts id="Seg_6500" n="HIAT:ip">,</nts>
                  <nts id="Seg_6501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_6503" n="HIAT:w" s="T718">dʼi͡etiger</ts>
                  <nts id="Seg_6504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_6506" n="HIAT:w" s="T719">kelerin</ts>
                  <nts id="Seg_6507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_6509" n="HIAT:w" s="T720">prʼedstavlʼajet</ts>
                  <nts id="Seg_6510" n="HIAT:ip">.</nts>
                  <nts id="Seg_6511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T724" id="Seg_6512" n="sc" s="T722">
               <ts e="T724" id="Seg_6514" n="HIAT:u" s="T722">
                  <ts e="T724" id="Seg_6516" n="HIAT:w" s="T722">A</ts>
                  <nts id="Seg_6517" n="HIAT:ip">…</nts>
                  <nts id="Seg_6518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T732" id="Seg_6519" n="sc" s="T726">
               <ts e="T730" id="Seg_6521" n="HIAT:u" s="T726">
                  <nts id="Seg_6522" n="HIAT:ip">–</nts>
                  <nts id="Seg_6523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_6525" n="HIAT:w" s="T726">Kajdi͡ek</ts>
                  <nts id="Seg_6526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_6528" n="HIAT:w" s="T728">etej</ts>
                  <nts id="Seg_6529" n="HIAT:ip">?</nts>
                  <nts id="Seg_6530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T732" id="Seg_6532" n="HIAT:u" s="T730">
                  <ts e="T731" id="Seg_6534" n="HIAT:w" s="T730">Aː</ts>
                  <nts id="Seg_6535" n="HIAT:ip">,</nts>
                  <nts id="Seg_6536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_6538" n="HIAT:w" s="T731">aha</ts>
                  <nts id="Seg_6539" n="HIAT:ip">.</nts>
                  <nts id="Seg_6540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T734" id="Seg_6541" n="sc" s="T733">
               <ts e="T734" id="Seg_6543" n="HIAT:u" s="T733">
                  <ts e="T734" id="Seg_6545" n="HIAT:w" s="T733">Taksɨbɨt</ts>
                  <nts id="Seg_6546" n="HIAT:ip">.</nts>
                  <nts id="Seg_6547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T738" id="Seg_6548" n="sc" s="T736">
               <ts e="T738" id="Seg_6550" n="HIAT:u" s="T736">
                  <nts id="Seg_6551" n="HIAT:ip">–</nts>
                  <nts id="Seg_6552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6553" n="HIAT:ip">(</nts>
                  <nts id="Seg_6554" n="HIAT:ip">(</nts>
                  <ats e="T737" id="Seg_6555" n="HIAT:non-pho" s="T736">…</ats>
                  <nts id="Seg_6556" n="HIAT:ip">)</nts>
                  <nts id="Seg_6557" n="HIAT:ip">)</nts>
                  <nts id="Seg_6558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_6560" n="HIAT:w" s="T737">tak</ts>
                  <nts id="Seg_6561" n="HIAT:ip">.</nts>
                  <nts id="Seg_6562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T754" id="Seg_6563" n="sc" s="T750">
               <ts e="T754" id="Seg_6565" n="HIAT:u" s="T750">
                  <nts id="Seg_6566" n="HIAT:ip">–</nts>
                  <nts id="Seg_6567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_6569" n="HIAT:w" s="T750">Aragiː</ts>
                  <nts id="Seg_6570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_6572" n="HIAT:w" s="T751">ispet</ts>
                  <nts id="Seg_6573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_6575" n="HIAT:w" s="T753">bu͡olbut</ts>
                  <nts id="Seg_6576" n="HIAT:ip">.</nts>
                  <nts id="Seg_6577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T760" id="Seg_6578" n="sc" s="T755">
               <ts e="T760" id="Seg_6580" n="HIAT:u" s="T755">
                  <ts e="T757" id="Seg_6582" n="HIAT:w" s="T755">Tak</ts>
                  <nts id="Seg_6583" n="HIAT:ip">,</nts>
                  <nts id="Seg_6584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_6586" n="HIAT:w" s="T757">da</ts>
                  <nts id="Seg_6587" n="HIAT:ip">,</nts>
                  <nts id="Seg_6588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_6590" n="HIAT:w" s="T759">pravʼilʼna</ts>
                  <nts id="Seg_6591" n="HIAT:ip">?</nts>
                  <nts id="Seg_6592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-UoPP">
            <ts e="T26" id="Seg_6593" n="sc" s="T21">
               <ts e="T26" id="Seg_6595" n="e" s="T21">– Aː. </ts>
            </ts>
            <ts e="T55" id="Seg_6596" n="sc" s="T38">
               <ts e="T40" id="Seg_6598" n="e" s="T38">– A </ts>
               <ts e="T43" id="Seg_6600" n="e" s="T40">što </ts>
               <ts e="T45" id="Seg_6602" n="e" s="T43">pa </ts>
               <ts e="T48" id="Seg_6604" n="e" s="T45">očʼerʼedʼi, </ts>
               <ts e="T50" id="Seg_6606" n="e" s="T48">kak </ts>
               <ts e="T53" id="Seg_6608" n="e" s="T50">eta </ts>
               <ts e="T55" id="Seg_6610" n="e" s="T53">načʼinalasʼ. </ts>
            </ts>
            <ts e="T73" id="Seg_6611" n="sc" s="T66">
               <ts e="T68" id="Seg_6613" n="e" s="T66">– Bu </ts>
               <ts e="T71" id="Seg_6615" n="e" s="T68">navʼernaje </ts>
               <ts e="T73" id="Seg_6617" n="e" s="T71">((…)). </ts>
            </ts>
            <ts e="T90" id="Seg_6618" n="sc" s="T78">
               <ts e="T79" id="Seg_6620" n="e" s="T78">– Eta </ts>
               <ts e="T80" id="Seg_6622" n="e" s="T79">v </ts>
               <ts e="T81" id="Seg_6624" n="e" s="T80">tʼurmu </ts>
               <ts e="T84" id="Seg_6626" n="e" s="T81">on </ts>
               <ts e="T86" id="Seg_6628" n="e" s="T84">papal, </ts>
               <ts e="T89" id="Seg_6630" n="e" s="T86">((…)) </ts>
               <ts e="T90" id="Seg_6632" n="e" s="T89">oloror. </ts>
            </ts>
            <ts e="T103" id="Seg_6633" n="sc" s="T91">
               <ts e="T93" id="Seg_6635" n="e" s="T91">– A </ts>
               <ts e="T95" id="Seg_6637" n="e" s="T93">onno </ts>
               <ts e="T97" id="Seg_6639" n="e" s="T95">((…)). </ts>
               <ts e="T98" id="Seg_6641" n="e" s="T97">– Aː, </ts>
               <ts e="T101" id="Seg_6643" n="e" s="T98">pa-dalganskʼi </ts>
               <ts e="T102" id="Seg_6645" n="e" s="T101">že </ts>
               <ts e="T103" id="Seg_6647" n="e" s="T102">naːda. </ts>
            </ts>
            <ts e="T123" id="Seg_6648" n="sc" s="T113">
               <ts e="T115" id="Seg_6650" n="e" s="T113">– Aragiːta, </ts>
               <ts e="T118" id="Seg_6652" n="e" s="T115">tak </ts>
               <ts e="T121" id="Seg_6654" n="e" s="T118">eta, </ts>
               <ts e="T123" id="Seg_6656" n="e" s="T121">sʼuda. </ts>
            </ts>
            <ts e="T132" id="Seg_6657" n="sc" s="T124">
               <ts e="T126" id="Seg_6659" n="e" s="T124">– Tak, </ts>
               <ts e="T128" id="Seg_6661" n="e" s="T126">bu </ts>
               <ts e="T129" id="Seg_6663" n="e" s="T128">itte </ts>
               <ts e="T130" id="Seg_6665" n="e" s="T129">baːr </ts>
               <ts e="T131" id="Seg_6667" n="e" s="T130">ebit </ts>
               <ts e="T132" id="Seg_6669" n="e" s="T131">načʼala. </ts>
            </ts>
            <ts e="T141" id="Seg_6670" n="sc" s="T138">
               <ts e="T139" id="Seg_6672" n="e" s="T138">– Aragiːlɨː </ts>
               <ts e="T140" id="Seg_6674" n="e" s="T139">olorollor </ts>
               <ts e="T141" id="Seg_6676" n="e" s="T140">iti. </ts>
            </ts>
            <ts e="T147" id="Seg_6677" n="sc" s="T142">
               <ts e="T143" id="Seg_6679" n="e" s="T142">Aː, </ts>
               <ts e="T144" id="Seg_6681" n="e" s="T143">bu </ts>
               <ts e="T145" id="Seg_6683" n="e" s="T144">üleliː </ts>
               <ts e="T146" id="Seg_6685" n="e" s="T145">hɨldʼɨbɨt </ts>
               <ts e="T147" id="Seg_6687" n="e" s="T146">že. </ts>
            </ts>
            <ts e="T152" id="Seg_6688" n="sc" s="T148">
               <ts e="T149" id="Seg_6690" n="e" s="T148">Üle </ts>
               <ts e="T150" id="Seg_6692" n="e" s="T149">karčɨ </ts>
               <ts e="T151" id="Seg_6694" n="e" s="T150">oŋosto </ts>
               <ts e="T152" id="Seg_6696" n="e" s="T151">hɨldʼɨbɨt. </ts>
            </ts>
            <ts e="T166" id="Seg_6697" n="sc" s="T157">
               <ts e="T159" id="Seg_6699" n="e" s="T157">– Tak, </ts>
               <ts e="T162" id="Seg_6701" n="e" s="T159">eta </ts>
               <ts e="T164" id="Seg_6703" n="e" s="T162">ogobut </ts>
               <ts e="T165" id="Seg_6705" n="e" s="T164">küččügüj </ts>
               <ts e="T166" id="Seg_6707" n="e" s="T165">manna. </ts>
            </ts>
            <ts e="T170" id="Seg_6708" n="sc" s="T168">
               <ts e="T170" id="Seg_6710" n="e" s="T168">– Aha. </ts>
            </ts>
            <ts e="T174" id="Seg_6711" n="sc" s="T171">
               <ts e="T172" id="Seg_6713" n="e" s="T171">Küččügüj, </ts>
               <ts e="T173" id="Seg_6715" n="e" s="T172">türmege </ts>
               <ts e="T174" id="Seg_6717" n="e" s="T173">oloror. </ts>
            </ts>
            <ts e="T201" id="Seg_6718" n="sc" s="T181">
               <ts e="T183" id="Seg_6720" n="e" s="T181">– Kojut </ts>
               <ts e="T185" id="Seg_6722" n="e" s="T183">kelen </ts>
               <ts e="T187" id="Seg_6724" n="e" s="T185">barar, </ts>
               <ts e="T189" id="Seg_6726" n="e" s="T187">eta </ts>
               <ts e="T191" id="Seg_6728" n="e" s="T189">že </ts>
               <ts e="T193" id="Seg_6730" n="e" s="T191">kanʼec </ts>
               <ts e="T195" id="Seg_6732" n="e" s="T193">eta, </ts>
               <ts e="T197" id="Seg_6734" n="e" s="T195">aː </ts>
               <ts e="T199" id="Seg_6736" n="e" s="T197">kennite </ts>
               <ts e="T201" id="Seg_6738" n="e" s="T199">iti. </ts>
            </ts>
            <ts e="T208" id="Seg_6739" n="sc" s="T202">
               <ts e="T205" id="Seg_6741" n="e" s="T202">– A </ts>
               <ts e="T206" id="Seg_6743" n="e" s="T205">eta </ts>
               <ts e="T208" id="Seg_6745" n="e" s="T206">što? </ts>
            </ts>
            <ts e="T211" id="Seg_6746" n="sc" s="T209">
               <ts e="T211" id="Seg_6748" n="e" s="T209">– Tak. </ts>
            </ts>
            <ts e="T230" id="Seg_6749" n="sc" s="T220">
               <ts e="T223" id="Seg_6751" n="e" s="T220">– Kelen </ts>
               <ts e="T225" id="Seg_6753" n="e" s="T223">baran </ts>
               <ts e="T228" id="Seg_6755" n="e" s="T225">kepsiː </ts>
               <ts e="T230" id="Seg_6757" n="e" s="T228">oloror. </ts>
            </ts>
            <ts e="T242" id="Seg_6758" n="sc" s="T238">
               <ts e="T240" id="Seg_6760" n="e" s="T238">– Kajdi͡etij </ts>
               <ts e="T241" id="Seg_6762" n="e" s="T240">ke </ts>
               <ts e="T242" id="Seg_6764" n="e" s="T241">bajdʼɨŋɨbɨt? </ts>
            </ts>
            <ts e="T257" id="Seg_6765" n="sc" s="T243">
               <ts e="T244" id="Seg_6767" n="e" s="T243">A </ts>
               <ts e="T245" id="Seg_6769" n="e" s="T244">kanna </ts>
               <ts e="T246" id="Seg_6771" n="e" s="T245">baːrɨj </ts>
               <ts e="T249" id="Seg_6773" n="e" s="T246">bajdʼɨŋ </ts>
               <ts e="T252" id="Seg_6775" n="e" s="T249">körsöllör </ts>
               <ts e="T255" id="Seg_6777" n="e" s="T252">haŋa </ts>
               <ts e="T257" id="Seg_6779" n="e" s="T255">(kiːr-). </ts>
            </ts>
            <ts e="T269" id="Seg_6780" n="sc" s="T264">
               <ts e="T269" id="Seg_6782" n="e" s="T264">– Naːda. </ts>
            </ts>
            <ts e="T281" id="Seg_6783" n="sc" s="T271">
               <ts e="T275" id="Seg_6785" n="e" s="T271">– Üleliː </ts>
               <ts e="T277" id="Seg_6787" n="e" s="T275">barar </ts>
               <ts e="T281" id="Seg_6789" n="e" s="T277">bu. </ts>
            </ts>
            <ts e="T293" id="Seg_6790" n="sc" s="T283">
               <ts e="T287" id="Seg_6792" n="e" s="T283">– Hu͡ok, </ts>
               <ts e="T288" id="Seg_6794" n="e" s="T287">hu͡ok, </ts>
               <ts e="T290" id="Seg_6796" n="e" s="T288">aː </ts>
               <ts e="T292" id="Seg_6798" n="e" s="T290">beːbe, </ts>
               <ts e="T293" id="Seg_6800" n="e" s="T292">körü͡ökpüt. </ts>
            </ts>
            <ts e="T300" id="Seg_6801" n="sc" s="T294">
               <ts e="T295" id="Seg_6803" n="e" s="T294">Tʼortʼü, </ts>
               <ts e="T296" id="Seg_6805" n="e" s="T295">čʼort </ts>
               <ts e="T297" id="Seg_6807" n="e" s="T296">jevo </ts>
               <ts e="T298" id="Seg_6809" n="e" s="T297">znajet </ts>
               <ts e="T299" id="Seg_6811" n="e" s="T298">xatʼela </ts>
               <ts e="T300" id="Seg_6813" n="e" s="T299">skazatʼ. </ts>
            </ts>
            <ts e="T306" id="Seg_6814" n="sc" s="T301">
               <ts e="T302" id="Seg_6816" n="e" s="T301">Tak </ts>
               <ts e="T303" id="Seg_6818" n="e" s="T302">rebʼonok, </ts>
               <ts e="T304" id="Seg_6820" n="e" s="T303">ogobut </ts>
               <ts e="T305" id="Seg_6822" n="e" s="T304">küččügüj </ts>
               <ts e="T306" id="Seg_6824" n="e" s="T305">itinne. </ts>
            </ts>
            <ts e="T311" id="Seg_6825" n="sc" s="T307">
               <ts e="T308" id="Seg_6827" n="e" s="T307">Tak, </ts>
               <ts e="T309" id="Seg_6829" n="e" s="T308">bu </ts>
               <ts e="T310" id="Seg_6831" n="e" s="T309">kaːjɨːga </ts>
               <ts e="T311" id="Seg_6833" n="e" s="T310">oloror. </ts>
            </ts>
            <ts e="T325" id="Seg_6834" n="sc" s="T321">
               <ts e="T322" id="Seg_6836" n="e" s="T321">– Bu </ts>
               <ts e="T323" id="Seg_6838" n="e" s="T322">künüːleːn </ts>
               <ts e="T324" id="Seg_6840" n="e" s="T323">(dʼak-), </ts>
               <ts e="T325" id="Seg_6842" n="e" s="T324">padaždʼi. </ts>
            </ts>
            <ts e="T337" id="Seg_6843" n="sc" s="T327">
               <ts e="T330" id="Seg_6845" n="e" s="T327">– Dʼaktarɨn </ts>
               <ts e="T334" id="Seg_6847" n="e" s="T330">kɨrbɨːr </ts>
               <ts e="T337" id="Seg_6849" n="e" s="T334">ete. </ts>
            </ts>
            <ts e="T365" id="Seg_6850" n="sc" s="T355">
               <ts e="T357" id="Seg_6852" n="e" s="T355">– E, </ts>
               <ts e="T359" id="Seg_6854" n="e" s="T357">onton </ts>
               <ts e="T361" id="Seg_6856" n="e" s="T359">kɨrbɨːr </ts>
               <ts e="T363" id="Seg_6858" n="e" s="T361">i͡e </ts>
               <ts e="T365" id="Seg_6860" n="e" s="T363">diː? </ts>
            </ts>
            <ts e="T374" id="Seg_6861" n="sc" s="T373">
               <ts e="T374" id="Seg_6863" n="e" s="T373">– Aha. </ts>
            </ts>
            <ts e="T383" id="Seg_6864" n="sc" s="T382">
               <ts e="T383" id="Seg_6866" n="e" s="T382">– Aha. </ts>
            </ts>
            <ts e="T392" id="Seg_6867" n="sc" s="T384">
               <ts e="T388" id="Seg_6869" n="e" s="T384">Da, </ts>
               <ts e="T392" id="Seg_6871" n="e" s="T388">sʼuda. </ts>
            </ts>
            <ts e="T396" id="Seg_6872" n="sc" s="T394">
               <ts e="T396" id="Seg_6874" n="e" s="T394">– Tak. </ts>
            </ts>
            <ts e="T410" id="Seg_6875" n="sc" s="T400">
               <ts e="T403" id="Seg_6877" n="e" s="T400">– Mʼilʼisʼijeler </ts>
               <ts e="T406" id="Seg_6879" n="e" s="T403">kelen </ts>
               <ts e="T410" id="Seg_6881" n="e" s="T406">ilpitter. </ts>
            </ts>
            <ts e="T418" id="Seg_6882" n="sc" s="T412">
               <ts e="T413" id="Seg_6884" n="e" s="T412">– Mʼilʼisʼijeler </ts>
               <ts e="T414" id="Seg_6886" n="e" s="T413">kelen </ts>
               <ts e="T415" id="Seg_6888" n="e" s="T414">ilpitter </ts>
               <ts e="T416" id="Seg_6890" n="e" s="T415">onton </ts>
               <ts e="T417" id="Seg_6892" n="e" s="T416">muntugun </ts>
               <ts e="T418" id="Seg_6894" n="e" s="T417">oksubutugar. </ts>
            </ts>
            <ts e="T428" id="Seg_6895" n="sc" s="T419">
               <ts e="T420" id="Seg_6897" n="e" s="T419">Kɨrbaːbat </ts>
               <ts e="T421" id="Seg_6899" n="e" s="T420">du͡o </ts>
               <ts e="T422" id="Seg_6901" n="e" s="T421">itinne </ts>
               <ts e="T423" id="Seg_6903" n="e" s="T422">kajdi͡ek </ts>
               <ts e="T424" id="Seg_6905" n="e" s="T423">da </ts>
               <ts e="T425" id="Seg_6907" n="e" s="T424">hu͡ok </ts>
               <ts e="T426" id="Seg_6909" n="e" s="T425">du͡o </ts>
               <ts e="T428" id="Seg_6911" n="e" s="T426">bolʼše. </ts>
            </ts>
            <ts e="T444" id="Seg_6912" n="sc" s="T440">
               <ts e="T444" id="Seg_6914" n="e" s="T440">– Aha. </ts>
            </ts>
            <ts e="T452" id="Seg_6915" n="sc" s="T451">
               <ts e="T452" id="Seg_6917" n="e" s="T451">– Heː. </ts>
            </ts>
            <ts e="T462" id="Seg_6918" n="sc" s="T455">
               <ts e="T456" id="Seg_6920" n="e" s="T455">– Tak, </ts>
               <ts e="T457" id="Seg_6922" n="e" s="T456">bu </ts>
               <ts e="T458" id="Seg_6924" n="e" s="T457">ke </ts>
               <ts e="T459" id="Seg_6926" n="e" s="T458">ogoto </ts>
               <ts e="T460" id="Seg_6928" n="e" s="T459">küččügüj </ts>
               <ts e="T462" id="Seg_6930" n="e" s="T460">erdegine. </ts>
            </ts>
            <ts e="T476" id="Seg_6931" n="sc" s="T463">
               <ts e="T465" id="Seg_6933" n="e" s="T463">– Üčügej </ts>
               <ts e="T467" id="Seg_6935" n="e" s="T465">bagajdɨk </ts>
               <ts e="T470" id="Seg_6937" n="e" s="T467">oloror </ts>
               <ts e="T472" id="Seg_6939" n="e" s="T470">etiler </ts>
               <ts e="T474" id="Seg_6941" n="e" s="T472">eni, </ts>
               <ts e="T476" id="Seg_6943" n="e" s="T474">hmm. </ts>
            </ts>
            <ts e="T483" id="Seg_6944" n="sc" s="T479">
               <ts e="T480" id="Seg_6946" n="e" s="T479">Üleliːr </ts>
               <ts e="T481" id="Seg_6948" n="e" s="T480">hɨldʼallar </ts>
               <ts e="T482" id="Seg_6950" n="e" s="T481">tak </ts>
               <ts e="T483" id="Seg_6952" n="e" s="T482">da. </ts>
            </ts>
            <ts e="T497" id="Seg_6953" n="sc" s="T490">
               <ts e="T492" id="Seg_6955" n="e" s="T490">– E, </ts>
               <ts e="T493" id="Seg_6957" n="e" s="T492">kelen </ts>
               <ts e="T495" id="Seg_6959" n="e" s="T493">baran </ts>
               <ts e="T497" id="Seg_6961" n="e" s="T495">du? </ts>
            </ts>
            <ts e="T504" id="Seg_6962" n="sc" s="T500">
               <ts e="T504" id="Seg_6964" n="e" s="T500">– Aha. </ts>
            </ts>
            <ts e="T563" id="Seg_6965" n="sc" s="T555">
               <ts e="T556" id="Seg_6967" n="e" s="T555">– Tak, </ts>
               <ts e="T557" id="Seg_6969" n="e" s="T556">onton </ts>
               <ts e="T558" id="Seg_6971" n="e" s="T557">načʼala </ts>
               <ts e="T560" id="Seg_6973" n="e" s="T558">kajdi͡ek </ts>
               <ts e="T561" id="Seg_6975" n="e" s="T560">baːr </ts>
               <ts e="T562" id="Seg_6977" n="e" s="T561">aragiːmsak </ts>
               <ts e="T563" id="Seg_6979" n="e" s="T562">bu͡olbuta. </ts>
            </ts>
            <ts e="T575" id="Seg_6980" n="sc" s="T569">
               <ts e="T571" id="Seg_6982" n="e" s="T569">– Eː, </ts>
               <ts e="T573" id="Seg_6984" n="e" s="T571">bu </ts>
               <ts e="T575" id="Seg_6986" n="e" s="T573">kepsiːr. </ts>
            </ts>
            <ts e="T600" id="Seg_6987" n="sc" s="T594">
               <ts e="T595" id="Seg_6989" n="e" s="T594">– Tak, </ts>
               <ts e="T596" id="Seg_6991" n="e" s="T595">dva, </ts>
               <ts e="T597" id="Seg_6993" n="e" s="T596">tak </ts>
               <ts e="T598" id="Seg_6995" n="e" s="T597">da, </ts>
               <ts e="T599" id="Seg_6997" n="e" s="T598">dʼaktarga </ts>
               <ts e="T600" id="Seg_6999" n="e" s="T599">barbɨt. </ts>
            </ts>
            <ts e="T614" id="Seg_7000" n="sc" s="T602">
               <ts e="T603" id="Seg_7002" n="e" s="T602">– "Hɨt, </ts>
               <ts e="T605" id="Seg_7004" n="e" s="T603">ol </ts>
               <ts e="T606" id="Seg_7006" n="e" s="T605">kihini </ts>
               <ts e="T607" id="Seg_7008" n="e" s="T606">gɨtta </ts>
               <ts e="T609" id="Seg_7010" n="e" s="T607">bugurduk </ts>
               <ts e="T611" id="Seg_7012" n="e" s="T609">hɨldʼɨbɨkkɨn", </ts>
               <ts e="T614" id="Seg_7014" n="e" s="T611">diːr. </ts>
            </ts>
            <ts e="T632" id="Seg_7015" n="sc" s="T617">
               <ts e="T619" id="Seg_7017" n="e" s="T617">– Onton </ts>
               <ts e="T620" id="Seg_7019" n="e" s="T619">oksubut, </ts>
               <ts e="T622" id="Seg_7021" n="e" s="T620">onton </ts>
               <ts e="T625" id="Seg_7023" n="e" s="T622">mʼilʼisʼijeler </ts>
               <ts e="T628" id="Seg_7025" n="e" s="T625">kelbitter. </ts>
               <ts e="T632" id="Seg_7027" n="e" s="T628">– Aha. </ts>
            </ts>
            <ts e="T639" id="Seg_7028" n="sc" s="T635">
               <ts e="T639" id="Seg_7030" n="e" s="T635">– Tak. </ts>
            </ts>
            <ts e="T643" id="Seg_7031" n="sc" s="T641">
               <ts e="T642" id="Seg_7033" n="e" s="T641">– Tak </ts>
               <ts e="T643" id="Seg_7035" n="e" s="T642">ešʼo. </ts>
            </ts>
            <ts e="T652" id="Seg_7036" n="sc" s="T644">
               <ts e="T645" id="Seg_7038" n="e" s="T644">A </ts>
               <ts e="T646" id="Seg_7040" n="e" s="T645">vot </ts>
               <ts e="T648" id="Seg_7042" n="e" s="T646">etʼi </ts>
               <ts e="T650" id="Seg_7044" n="e" s="T648">pasmatrʼetʼ </ts>
               <ts e="T652" id="Seg_7046" n="e" s="T650">naːda. </ts>
            </ts>
            <ts e="T659" id="Seg_7047" n="sc" s="T653">
               <ts e="T655" id="Seg_7049" n="e" s="T653">– Načala </ts>
               <ts e="T657" id="Seg_7051" n="e" s="T655">baːr </ts>
               <ts e="T659" id="Seg_7053" n="e" s="T657">taŋas. </ts>
            </ts>
            <ts e="T668" id="Seg_7054" n="sc" s="T661">
               <ts e="T663" id="Seg_7056" n="e" s="T661">– Aː, </ts>
               <ts e="T666" id="Seg_7058" n="e" s="T663">vot </ts>
               <ts e="T668" id="Seg_7060" n="e" s="T666">ješʼo. </ts>
            </ts>
            <ts e="T675" id="Seg_7061" n="sc" s="T674">
               <ts e="T675" id="Seg_7063" n="e" s="T674">– Taŋas. </ts>
            </ts>
            <ts e="T703" id="Seg_7064" n="sc" s="T698">
               <ts e="T699" id="Seg_7066" n="e" s="T698">– ((…)) </ts>
               <ts e="T700" id="Seg_7068" n="e" s="T699">mʼilʼisʼijeler </ts>
               <ts e="T701" id="Seg_7070" n="e" s="T700">kɨrbɨːllar, </ts>
               <ts e="T703" id="Seg_7072" n="e" s="T701">kanʼɨːllar. </ts>
            </ts>
            <ts e="T707" id="Seg_7073" n="sc" s="T706">
               <ts e="T707" id="Seg_7075" n="e" s="T706">– Onton. </ts>
            </ts>
            <ts e="T712" id="Seg_7076" n="sc" s="T710">
               <ts e="T712" id="Seg_7078" n="e" s="T710">– Tak. </ts>
            </ts>
            <ts e="T721" id="Seg_7079" n="sc" s="T716">
               <ts e="T718" id="Seg_7081" n="e" s="T716">– Aː, </ts>
               <ts e="T719" id="Seg_7083" n="e" s="T718">dʼi͡etiger </ts>
               <ts e="T720" id="Seg_7085" n="e" s="T719">kelerin </ts>
               <ts e="T721" id="Seg_7087" n="e" s="T720">prʼedstavlʼajet. </ts>
            </ts>
            <ts e="T724" id="Seg_7088" n="sc" s="T722">
               <ts e="T724" id="Seg_7090" n="e" s="T722">A… </ts>
            </ts>
            <ts e="T732" id="Seg_7091" n="sc" s="T726">
               <ts e="T728" id="Seg_7093" n="e" s="T726">– Kajdi͡ek </ts>
               <ts e="T730" id="Seg_7095" n="e" s="T728">etej? </ts>
               <ts e="T731" id="Seg_7097" n="e" s="T730">Aː, </ts>
               <ts e="T732" id="Seg_7099" n="e" s="T731">aha. </ts>
            </ts>
            <ts e="T734" id="Seg_7100" n="sc" s="T733">
               <ts e="T734" id="Seg_7102" n="e" s="T733">Taksɨbɨt. </ts>
            </ts>
            <ts e="T738" id="Seg_7103" n="sc" s="T736">
               <ts e="T737" id="Seg_7105" n="e" s="T736">– ((…)) </ts>
               <ts e="T738" id="Seg_7107" n="e" s="T737">tak. </ts>
            </ts>
            <ts e="T754" id="Seg_7108" n="sc" s="T750">
               <ts e="T751" id="Seg_7110" n="e" s="T750">– Aragiː </ts>
               <ts e="T753" id="Seg_7112" n="e" s="T751">ispet </ts>
               <ts e="T754" id="Seg_7114" n="e" s="T753">bu͡olbut. </ts>
            </ts>
            <ts e="T760" id="Seg_7115" n="sc" s="T755">
               <ts e="T757" id="Seg_7117" n="e" s="T755">Tak, </ts>
               <ts e="T759" id="Seg_7119" n="e" s="T757">da, </ts>
               <ts e="T760" id="Seg_7121" n="e" s="T759">pravʼilʼna? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-UoPP">
            <ta e="T26" id="Seg_7122" s="T21">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.001 (001.004)</ta>
            <ta e="T55" id="Seg_7123" s="T38">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.002 (001.007)</ta>
            <ta e="T73" id="Seg_7124" s="T66">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.003 (001.012)</ta>
            <ta e="T90" id="Seg_7125" s="T78">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.004 (001.015)</ta>
            <ta e="T97" id="Seg_7126" s="T91">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.005 (001.017)</ta>
            <ta e="T103" id="Seg_7127" s="T97">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.006 (001.019)</ta>
            <ta e="T123" id="Seg_7128" s="T113">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.007 (001.023)</ta>
            <ta e="T132" id="Seg_7129" s="T124">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.008 (001.024)</ta>
            <ta e="T141" id="Seg_7130" s="T138">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.009 (001.026)</ta>
            <ta e="T147" id="Seg_7131" s="T142">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.010 (001.027)</ta>
            <ta e="T152" id="Seg_7132" s="T148">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.011 (001.028)</ta>
            <ta e="T166" id="Seg_7133" s="T157">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.012 (001.030)</ta>
            <ta e="T170" id="Seg_7134" s="T168">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.013 (001.032)</ta>
            <ta e="T174" id="Seg_7135" s="T171">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.014 (001.033)</ta>
            <ta e="T201" id="Seg_7136" s="T181">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.015 (001.035)</ta>
            <ta e="T208" id="Seg_7137" s="T202">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.016 (001.037)</ta>
            <ta e="T211" id="Seg_7138" s="T209">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.017 (001.039)</ta>
            <ta e="T230" id="Seg_7139" s="T220">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.018 (001.043)</ta>
            <ta e="T242" id="Seg_7140" s="T238">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.019 (001.046)</ta>
            <ta e="T257" id="Seg_7141" s="T243">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.020 (001.047)</ta>
            <ta e="T269" id="Seg_7142" s="T264">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.021 (001.051)</ta>
            <ta e="T281" id="Seg_7143" s="T271">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.022 (001.052)</ta>
            <ta e="T293" id="Seg_7144" s="T283">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.023 (001.054)</ta>
            <ta e="T300" id="Seg_7145" s="T294">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.024 (001.056)</ta>
            <ta e="T306" id="Seg_7146" s="T301">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.025 (001.057)</ta>
            <ta e="T311" id="Seg_7147" s="T307">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.026 (001.058)</ta>
            <ta e="T325" id="Seg_7148" s="T321">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.027 (001.060)</ta>
            <ta e="T337" id="Seg_7149" s="T327">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.028 (001.062)</ta>
            <ta e="T365" id="Seg_7150" s="T355">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.029 (001.065)</ta>
            <ta e="T374" id="Seg_7151" s="T373">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.030 (001.067)</ta>
            <ta e="T383" id="Seg_7152" s="T382">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.031 (001.069)</ta>
            <ta e="T392" id="Seg_7153" s="T384">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.032 (001.070)</ta>
            <ta e="T396" id="Seg_7154" s="T394">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.033 (001.073)</ta>
            <ta e="T410" id="Seg_7155" s="T400">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.034 (001.075)</ta>
            <ta e="T418" id="Seg_7156" s="T412">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.035 (001.077)</ta>
            <ta e="T428" id="Seg_7157" s="T419">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.036 (001.078)</ta>
            <ta e="T444" id="Seg_7158" s="T440">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.037 (001.080)</ta>
            <ta e="T452" id="Seg_7159" s="T451">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.038 (001.083)</ta>
            <ta e="T462" id="Seg_7160" s="T455">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.039 (001.084)</ta>
            <ta e="T476" id="Seg_7161" s="T463">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.040 (001.086)</ta>
            <ta e="T483" id="Seg_7162" s="T479">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.041 (001.087)</ta>
            <ta e="T497" id="Seg_7163" s="T490">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.042 (001.089)</ta>
            <ta e="T504" id="Seg_7164" s="T500">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.043 (001.090)</ta>
            <ta e="T563" id="Seg_7165" s="T555">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.044 (001.098)</ta>
            <ta e="T575" id="Seg_7166" s="T569">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.045 (001.100)</ta>
            <ta e="T600" id="Seg_7167" s="T594">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.046 (001.104)</ta>
            <ta e="T614" id="Seg_7168" s="T602">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.047 (001.106)</ta>
            <ta e="T628" id="Seg_7169" s="T617">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.048 (001.108)</ta>
            <ta e="T632" id="Seg_7170" s="T628">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.049 (001.110)</ta>
            <ta e="T639" id="Seg_7171" s="T635">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.050 (001.111)</ta>
            <ta e="T643" id="Seg_7172" s="T641">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.051 (001.112)</ta>
            <ta e="T652" id="Seg_7173" s="T644">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.052 (001.113)</ta>
            <ta e="T659" id="Seg_7174" s="T653">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.053 (001.115)</ta>
            <ta e="T668" id="Seg_7175" s="T661">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.054 (001.117)</ta>
            <ta e="T675" id="Seg_7176" s="T674">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.055 (001.120)</ta>
            <ta e="T703" id="Seg_7177" s="T698">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.056 (001.124)</ta>
            <ta e="T707" id="Seg_7178" s="T706">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.057 (001.126)</ta>
            <ta e="T712" id="Seg_7179" s="T710">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.058 (001.128)</ta>
            <ta e="T721" id="Seg_7180" s="T716">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.059 (001.129)</ta>
            <ta e="T724" id="Seg_7181" s="T722">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.060 (001.130)</ta>
            <ta e="T730" id="Seg_7182" s="T726">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.061 (001.132)</ta>
            <ta e="T732" id="Seg_7183" s="T730">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.062 (001.133)</ta>
            <ta e="T734" id="Seg_7184" s="T733">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.063 (001.134)</ta>
            <ta e="T738" id="Seg_7185" s="T736">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.064 (001.136)</ta>
            <ta e="T754" id="Seg_7186" s="T750">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.065 (001.140)</ta>
            <ta e="T760" id="Seg_7187" s="T755">ChGS_UoPP_20170724_SocCogOrder_conv.UoPP.066 (001.141)</ta>
         </annotation>
         <annotation name="st" tierref="st-UoPP">
            <ta e="T26" id="Seg_7188" s="T21">Ааа.</ta>
            <ta e="T55" id="Seg_7189" s="T38">А что по очереди, как это начиналось.</ta>
            <ta e="T73" id="Seg_7190" s="T66">((…))</ta>
            <ta e="T90" id="Seg_7191" s="T78">Это в тюрьму он попал, ((…)) олорор.</ta>
            <ta e="T97" id="Seg_7192" s="T91">А онно ((…))</ta>
            <ta e="T103" id="Seg_7193" s="T97">А потом ((…)) надо.</ta>
            <ta e="T123" id="Seg_7194" s="T113">(Арагила-) так это…</ta>
            <ta e="T132" id="Seg_7195" s="T124">Так, бу иттэ баар эбит начало.</ta>
            <ta e="T141" id="Seg_7196" s="T138">Арагилыы олороллор ити.</ta>
            <ta e="T147" id="Seg_7197" s="T142">Аа, бу үлэлии һылдьыбыт же.</ta>
            <ta e="T152" id="Seg_7198" s="T148">Илэ карчы оӈосто һылдьыбыт.</ta>
            <ta e="T166" id="Seg_7199" s="T157">Аа что-то огобут күччүгүй манна.</ta>
            <ta e="T170" id="Seg_7200" s="T168">Аһа.</ta>
            <ta e="T174" id="Seg_7201" s="T171">Күччүгүй…тюрмэгэ олорор.</ta>
            <ta e="T201" id="Seg_7202" s="T181">Койут кэлэн баран, аа конец же это, аа кэннитэ ити.</ta>
            <ta e="T208" id="Seg_7203" s="T202">А это что?</ta>
            <ta e="T211" id="Seg_7204" s="T209">Так.</ta>
            <ta e="T230" id="Seg_7205" s="T220">Кэлэн баран кэпсии олорор.</ta>
            <ta e="T242" id="Seg_7206" s="T238">Кайдиэттий кэ байдьыӈыбыт?</ta>
            <ta e="T257" id="Seg_7207" s="T243">Канна баарый байдьыӈы көрсөллөрө һаӈа (киир-).</ta>
            <ta e="T269" id="Seg_7208" s="T264">Наада.</ta>
            <ta e="T281" id="Seg_7209" s="T271">Үлэлии барар бу.</ta>
            <ta e="T293" id="Seg_7210" s="T283">Һуок, һуок, ээ бээбэ, көрүөкпүт.</ta>
            <ta e="T300" id="Seg_7211" s="T294">Тьорти… черт его знает хотела сказать.</ta>
            <ta e="T306" id="Seg_7212" s="T301">Так ребенок, огобут күччүгүй итиннэ.</ta>
            <ta e="T311" id="Seg_7213" s="T307">Так, бу кайыыга олорор.</ta>
            <ta e="T325" id="Seg_7214" s="T321">Бу күнүүлэн (дьак-)… подожди.</ta>
            <ta e="T337" id="Seg_7215" s="T327">Дьактарын кырбыыр этэ.</ta>
            <ta e="T365" id="Seg_7216" s="T355">Ээ, онтон кырбыыр иэ дии?</ta>
            <ta e="T374" id="Seg_7217" s="T373">Аһа.</ta>
            <ta e="T383" id="Seg_7218" s="T382">Мм.</ta>
            <ta e="T392" id="Seg_7219" s="T384">Аа, сюда(?).</ta>
            <ta e="T396" id="Seg_7220" s="T394">Так.</ta>
            <ta e="T410" id="Seg_7221" s="T400">Милициялар кэлэн илпиттэр.</ta>
            <ta e="T418" id="Seg_7222" s="T412">Милициялар кэлэн илпиттэр онтон мунтугун оксубуттугар.</ta>
            <ta e="T428" id="Seg_7223" s="T419">Кырбаабат дуо итиннэ кайдиэк да һуок дуо больше.</ta>
            <ta e="T444" id="Seg_7224" s="T440">Аһа.</ta>
            <ta e="T452" id="Seg_7225" s="T451">Һэ-э.</ta>
            <ta e="T462" id="Seg_7226" s="T455">Так, бу кэ огото күччүгүй эрдэгинэ.</ta>
            <ta e="T476" id="Seg_7227" s="T463">Үчүгэй багайыдык олорор этилэр эни, мм.</ta>
            <ta e="T483" id="Seg_7228" s="T479">Үлэлиир һылдьаллар таак-та.</ta>
            <ta e="T497" id="Seg_7229" s="T490">Э кэлэн баран ду?</ta>
            <ta e="T504" id="Seg_7230" s="T500">Аһа.</ta>
            <ta e="T563" id="Seg_7231" s="T555">Онтон начало-то кайдиэк баарый арагимсак буолбуттар.</ta>
            <ta e="T575" id="Seg_7232" s="T569">Ээ бу кэпсиир.</ta>
            <ta e="T600" id="Seg_7233" s="T594">Так, два, так да, дьактаргар барбыт.</ta>
            <ta e="T614" id="Seg_7234" s="T602">Һыт ол киһини кытта бугурдук һылдьыбыккын диир.</ta>
            <ta e="T628" id="Seg_7235" s="T617">Онтон оксубут, онтон милициялар кэлбиттэр.</ta>
            <ta e="T632" id="Seg_7236" s="T628">Аһа.</ta>
            <ta e="T639" id="Seg_7237" s="T635">Так.</ta>
            <ta e="T643" id="Seg_7238" s="T641">Так еще.</ta>
            <ta e="T652" id="Seg_7239" s="T644">А вот эти посмотреть надо.</ta>
            <ta e="T659" id="Seg_7240" s="T653">Началотыгар таӈас.</ta>
            <ta e="T668" id="Seg_7241" s="T661">Аа, вот еще.</ta>
            <ta e="T675" id="Seg_7242" s="T674">Таӈас.</ta>
            <ta e="T703" id="Seg_7243" s="T698">((…)) милициялар кырбыыллар, каньыыллар.</ta>
            <ta e="T707" id="Seg_7244" s="T706">Онтон.</ta>
            <ta e="T712" id="Seg_7245" s="T710">Так.</ta>
            <ta e="T721" id="Seg_7246" s="T716">А дьиэтигэр кэлэрин представляет.</ta>
            <ta e="T724" id="Seg_7247" s="T722">А…</ta>
            <ta e="T730" id="Seg_7248" s="T726">Кайдиэк этэй?</ta>
            <ta e="T732" id="Seg_7249" s="T730">Аа, аһа.</ta>
            <ta e="T734" id="Seg_7250" s="T733">Таксыбыт.</ta>
            <ta e="T738" id="Seg_7251" s="T736">((…)) так.</ta>
            <ta e="T754" id="Seg_7252" s="T750">Араги испэт буолбут.</ta>
            <ta e="T760" id="Seg_7253" s="T755">Все, так да, правильно?</ta>
         </annotation>
         <annotation name="ts" tierref="ts-UoPP">
            <ta e="T26" id="Seg_7254" s="T21">– Aː. </ta>
            <ta e="T55" id="Seg_7255" s="T38">– A što pa očʼerʼedʼi, kak eta načʼinalasʼ. </ta>
            <ta e="T73" id="Seg_7256" s="T66">– Bu navʼernaje ((…)). </ta>
            <ta e="T90" id="Seg_7257" s="T78">– Eta v tʼurmu on papal, ((…)) oloror. </ta>
            <ta e="T97" id="Seg_7258" s="T91">– A onno ((…)). </ta>
            <ta e="T103" id="Seg_7259" s="T97">– Aː, pa-dalganskʼi že naːda. </ta>
            <ta e="T123" id="Seg_7260" s="T113">– Aragiːta, tak eta, sʼuda. </ta>
            <ta e="T132" id="Seg_7261" s="T124">– Tak, bu itte baːr ebit načʼala. </ta>
            <ta e="T141" id="Seg_7262" s="T138">– Aragiːlɨː olorollor iti. </ta>
            <ta e="T147" id="Seg_7263" s="T142">Aː, bu üleliː hɨldʼɨbɨt že. </ta>
            <ta e="T152" id="Seg_7264" s="T148">Üle karčɨ oŋosto hɨldʼɨbɨt. </ta>
            <ta e="T166" id="Seg_7265" s="T157">– Tak, eta ogobut küččügüj manna. </ta>
            <ta e="T170" id="Seg_7266" s="T168">– Aha. </ta>
            <ta e="T174" id="Seg_7267" s="T171">Küččügüj, türmege oloror. </ta>
            <ta e="T201" id="Seg_7268" s="T181">– Kojut kelen barar, eta že kanʼec eta, aː kennite iti. </ta>
            <ta e="T208" id="Seg_7269" s="T202">– A eta što? </ta>
            <ta e="T211" id="Seg_7270" s="T209">– Tak. </ta>
            <ta e="T230" id="Seg_7271" s="T220">– Kelen baran kepsiː oloror. </ta>
            <ta e="T242" id="Seg_7272" s="T238">– Kajdi͡etij ke bajdʼɨŋɨbɨt? </ta>
            <ta e="T257" id="Seg_7273" s="T243">A kanna baːrɨj bajdʼɨŋ körsöllör haŋa (kiːr-). </ta>
            <ta e="T269" id="Seg_7274" s="T264">– Naːda. </ta>
            <ta e="T281" id="Seg_7275" s="T271">– Üleliː barar bu. </ta>
            <ta e="T293" id="Seg_7276" s="T283">– Hu͡ok, hu͡ok, aː beːbe, körü͡ökpüt. </ta>
            <ta e="T300" id="Seg_7277" s="T294">Tʼortʼü, čʼort jevo znajet xatʼela skazatʼ. </ta>
            <ta e="T306" id="Seg_7278" s="T301">Tak rebʼonok, ogobut küččügüj itinne. </ta>
            <ta e="T311" id="Seg_7279" s="T307">Tak, bu kaːjɨːga oloror. </ta>
            <ta e="T325" id="Seg_7280" s="T321">– Bu künüːleːn (dʼak-), padaždʼi. </ta>
            <ta e="T337" id="Seg_7281" s="T327">– Dʼaktarɨn kɨrbɨːr ete. </ta>
            <ta e="T365" id="Seg_7282" s="T355">– E, onton kɨrbɨːr i͡e diː? </ta>
            <ta e="T374" id="Seg_7283" s="T373">– Aha. </ta>
            <ta e="T383" id="Seg_7284" s="T382">– Aha. </ta>
            <ta e="T392" id="Seg_7285" s="T384">Da, sʼuda. </ta>
            <ta e="T396" id="Seg_7286" s="T394">– Tak. </ta>
            <ta e="T410" id="Seg_7287" s="T400">– Mʼilʼisʼijeler kelen ilpitter. </ta>
            <ta e="T418" id="Seg_7288" s="T412">– Mʼilʼisʼijeler kelen ilpitter onton muntugun oksubutugar. </ta>
            <ta e="T428" id="Seg_7289" s="T419">Kɨrbaːbat du͡o itinne kajdi͡ek da hu͡ok du͡o bolʼše. </ta>
            <ta e="T444" id="Seg_7290" s="T440">– Aha. </ta>
            <ta e="T452" id="Seg_7291" s="T451">– Heː. </ta>
            <ta e="T462" id="Seg_7292" s="T455">– Tak, bu ke ogoto küččügüj erdegine. </ta>
            <ta e="T476" id="Seg_7293" s="T463">– Üčügej bagajdɨk oloror etiler eni, hmm. </ta>
            <ta e="T483" id="Seg_7294" s="T479">Üleliːr hɨldʼallar tak da. </ta>
            <ta e="T497" id="Seg_7295" s="T490">– E, kelen baran du? </ta>
            <ta e="T504" id="Seg_7296" s="T500">– Aha. </ta>
            <ta e="T563" id="Seg_7297" s="T555">– Tak, onton načʼala kajdi͡ek baːr aragiːmsak bu͡olbuta. </ta>
            <ta e="T575" id="Seg_7298" s="T569">– Eː, bu kepsiːr. </ta>
            <ta e="T600" id="Seg_7299" s="T594">– Tak, dva, tak da, dʼaktarga barbɨt. </ta>
            <ta e="T614" id="Seg_7300" s="T602">– "Hɨt, ol kihini gɨtta bugurduk hɨldʼɨbɨkkɨn", diːr. </ta>
            <ta e="T628" id="Seg_7301" s="T617">– Onton oksubut, onton mʼilʼisʼijeler kelbitter. </ta>
            <ta e="T632" id="Seg_7302" s="T628">– Aha. </ta>
            <ta e="T639" id="Seg_7303" s="T635">– Tak. </ta>
            <ta e="T643" id="Seg_7304" s="T641">– Tak ešʼo. </ta>
            <ta e="T652" id="Seg_7305" s="T644">A vot etʼi pasmatrʼetʼ naːda. </ta>
            <ta e="T659" id="Seg_7306" s="T653">– Načala baːr taŋas. </ta>
            <ta e="T668" id="Seg_7307" s="T661">– Aː, vot ješʼo. </ta>
            <ta e="T675" id="Seg_7308" s="T674">– Taŋas. </ta>
            <ta e="T703" id="Seg_7309" s="T698">– ((…)) mʼilʼisʼijeler kɨrbɨːllar, kanʼɨːllar. </ta>
            <ta e="T707" id="Seg_7310" s="T706">– Onton. </ta>
            <ta e="T712" id="Seg_7311" s="T710">– Tak. </ta>
            <ta e="T721" id="Seg_7312" s="T716">– Aː, dʼi͡etiger kelerin prʼedstavlʼajet. </ta>
            <ta e="T724" id="Seg_7313" s="T722">A… </ta>
            <ta e="T730" id="Seg_7314" s="T726">– Kajdi͡ek etej? </ta>
            <ta e="T732" id="Seg_7315" s="T730">Aː, aha. </ta>
            <ta e="T734" id="Seg_7316" s="T733">Taksɨbɨt. </ta>
            <ta e="T738" id="Seg_7317" s="T736">– ((…)) tak. </ta>
            <ta e="T754" id="Seg_7318" s="T750">– Aragiː ispet bu͡olbut. </ta>
            <ta e="T760" id="Seg_7319" s="T755">Tak, da, pravʼilʼna? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-UoPP">
            <ta e="T26" id="Seg_7320" s="T21">aː</ta>
            <ta e="T68" id="Seg_7321" s="T66">bu</ta>
            <ta e="T71" id="Seg_7322" s="T68">navʼernaje</ta>
            <ta e="T90" id="Seg_7323" s="T89">olor-or</ta>
            <ta e="T93" id="Seg_7324" s="T91">a</ta>
            <ta e="T95" id="Seg_7325" s="T93">onno</ta>
            <ta e="T115" id="Seg_7326" s="T113">aragiː-ta</ta>
            <ta e="T126" id="Seg_7327" s="T124">tak</ta>
            <ta e="T128" id="Seg_7328" s="T126">bu</ta>
            <ta e="T129" id="Seg_7329" s="T128">itte</ta>
            <ta e="T130" id="Seg_7330" s="T129">baːr</ta>
            <ta e="T131" id="Seg_7331" s="T130">e-bit</ta>
            <ta e="T132" id="Seg_7332" s="T131">načʼala</ta>
            <ta e="T139" id="Seg_7333" s="T138">aragiː-l-ɨː</ta>
            <ta e="T140" id="Seg_7334" s="T139">olor-ol-lor</ta>
            <ta e="T141" id="Seg_7335" s="T140">iti</ta>
            <ta e="T143" id="Seg_7336" s="T142">aː</ta>
            <ta e="T144" id="Seg_7337" s="T143">bu</ta>
            <ta e="T145" id="Seg_7338" s="T144">ülel-iː</ta>
            <ta e="T146" id="Seg_7339" s="T145">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T147" id="Seg_7340" s="T146">že</ta>
            <ta e="T149" id="Seg_7341" s="T148">üle</ta>
            <ta e="T150" id="Seg_7342" s="T149">karčɨ</ta>
            <ta e="T151" id="Seg_7343" s="T150">oŋost-o</ta>
            <ta e="T152" id="Seg_7344" s="T151">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T159" id="Seg_7345" s="T157">tak</ta>
            <ta e="T162" id="Seg_7346" s="T159">eta</ta>
            <ta e="T164" id="Seg_7347" s="T162">ogo-but</ta>
            <ta e="T165" id="Seg_7348" s="T164">küččügüj</ta>
            <ta e="T166" id="Seg_7349" s="T165">manna</ta>
            <ta e="T170" id="Seg_7350" s="T168">aha</ta>
            <ta e="T172" id="Seg_7351" s="T171">küččügüj</ta>
            <ta e="T173" id="Seg_7352" s="T172">türme-ge</ta>
            <ta e="T174" id="Seg_7353" s="T173">olor-or</ta>
            <ta e="T183" id="Seg_7354" s="T181">kojut</ta>
            <ta e="T185" id="Seg_7355" s="T183">kel-en</ta>
            <ta e="T187" id="Seg_7356" s="T185">bar-ar</ta>
            <ta e="T197" id="Seg_7357" s="T195">aː</ta>
            <ta e="T199" id="Seg_7358" s="T197">kenni-te</ta>
            <ta e="T201" id="Seg_7359" s="T199">iti</ta>
            <ta e="T211" id="Seg_7360" s="T209">tak</ta>
            <ta e="T223" id="Seg_7361" s="T220">kel-en</ta>
            <ta e="T225" id="Seg_7362" s="T223">baran</ta>
            <ta e="T228" id="Seg_7363" s="T225">keps-iː</ta>
            <ta e="T230" id="Seg_7364" s="T228">olor-or</ta>
            <ta e="T240" id="Seg_7365" s="T238">kajdi͡et=ij</ta>
            <ta e="T241" id="Seg_7366" s="T240">ke</ta>
            <ta e="T242" id="Seg_7367" s="T241">bajdʼɨŋɨ-bɨt</ta>
            <ta e="T244" id="Seg_7368" s="T243">a</ta>
            <ta e="T245" id="Seg_7369" s="T244">kanna</ta>
            <ta e="T246" id="Seg_7370" s="T245">baːr=ɨj</ta>
            <ta e="T249" id="Seg_7371" s="T246">bajdʼɨŋ</ta>
            <ta e="T252" id="Seg_7372" s="T249">körs-öl-lör</ta>
            <ta e="T255" id="Seg_7373" s="T252">haŋa</ta>
            <ta e="T257" id="Seg_7374" s="T255">kiːr</ta>
            <ta e="T269" id="Seg_7375" s="T264">naːda</ta>
            <ta e="T275" id="Seg_7376" s="T271">ülel-iː</ta>
            <ta e="T277" id="Seg_7377" s="T275">bar-ar</ta>
            <ta e="T281" id="Seg_7378" s="T277">bu</ta>
            <ta e="T287" id="Seg_7379" s="T283">hu͡ok</ta>
            <ta e="T288" id="Seg_7380" s="T287">hu͡ok</ta>
            <ta e="T290" id="Seg_7381" s="T288">aː</ta>
            <ta e="T292" id="Seg_7382" s="T290">beːbe</ta>
            <ta e="T293" id="Seg_7383" s="T292">kör-ü͡ök-püt</ta>
            <ta e="T304" id="Seg_7384" s="T303">ogo-but</ta>
            <ta e="T305" id="Seg_7385" s="T304">küččügüj</ta>
            <ta e="T306" id="Seg_7386" s="T305">itinne</ta>
            <ta e="T308" id="Seg_7387" s="T307">tak</ta>
            <ta e="T309" id="Seg_7388" s="T308">bu</ta>
            <ta e="T310" id="Seg_7389" s="T309">kaːjɨː-ga</ta>
            <ta e="T311" id="Seg_7390" s="T310">olor-or</ta>
            <ta e="T322" id="Seg_7391" s="T321">bu</ta>
            <ta e="T323" id="Seg_7392" s="T322">künüːleː-n</ta>
            <ta e="T330" id="Seg_7393" s="T327">dʼaktar-ɨ-n</ta>
            <ta e="T334" id="Seg_7394" s="T330">kɨrbɨː-r</ta>
            <ta e="T337" id="Seg_7395" s="T334">e-t-e</ta>
            <ta e="T357" id="Seg_7396" s="T355">e</ta>
            <ta e="T359" id="Seg_7397" s="T357">onton</ta>
            <ta e="T361" id="Seg_7398" s="T359">kɨrbɨː-r</ta>
            <ta e="T363" id="Seg_7399" s="T361">i͡e</ta>
            <ta e="T365" id="Seg_7400" s="T363">diː</ta>
            <ta e="T374" id="Seg_7401" s="T373">aha</ta>
            <ta e="T383" id="Seg_7402" s="T382">aha</ta>
            <ta e="T396" id="Seg_7403" s="T394">tak</ta>
            <ta e="T403" id="Seg_7404" s="T400">mʼilʼisʼije-ler</ta>
            <ta e="T406" id="Seg_7405" s="T403">kel-en</ta>
            <ta e="T410" id="Seg_7406" s="T406">il-pit-ter</ta>
            <ta e="T413" id="Seg_7407" s="T412">mʼilʼisʼije-ler</ta>
            <ta e="T414" id="Seg_7408" s="T413">kel-en</ta>
            <ta e="T415" id="Seg_7409" s="T414">il-pit-ter</ta>
            <ta e="T416" id="Seg_7410" s="T415">onton</ta>
            <ta e="T417" id="Seg_7411" s="T416">mun-tu-gu-n</ta>
            <ta e="T418" id="Seg_7412" s="T417">oks-u-but-u-gar</ta>
            <ta e="T420" id="Seg_7413" s="T419">kɨrbaː-bat</ta>
            <ta e="T421" id="Seg_7414" s="T420">du͡o</ta>
            <ta e="T422" id="Seg_7415" s="T421">itinne</ta>
            <ta e="T423" id="Seg_7416" s="T422">kajdi͡ek</ta>
            <ta e="T424" id="Seg_7417" s="T423">da</ta>
            <ta e="T425" id="Seg_7418" s="T424">hu͡ok</ta>
            <ta e="T426" id="Seg_7419" s="T425">du͡o</ta>
            <ta e="T428" id="Seg_7420" s="T426">bolʼše</ta>
            <ta e="T444" id="Seg_7421" s="T440">aha</ta>
            <ta e="T452" id="Seg_7422" s="T451">heː</ta>
            <ta e="T456" id="Seg_7423" s="T455">tak</ta>
            <ta e="T457" id="Seg_7424" s="T456">bu</ta>
            <ta e="T458" id="Seg_7425" s="T457">ke</ta>
            <ta e="T459" id="Seg_7426" s="T458">ogo-to</ta>
            <ta e="T460" id="Seg_7427" s="T459">küččügüj</ta>
            <ta e="T462" id="Seg_7428" s="T460">er-deg-ine</ta>
            <ta e="T465" id="Seg_7429" s="T463">üčügej</ta>
            <ta e="T467" id="Seg_7430" s="T465">bagaj-dɨk</ta>
            <ta e="T470" id="Seg_7431" s="T467">olor-or</ta>
            <ta e="T472" id="Seg_7432" s="T470">e-ti-ler</ta>
            <ta e="T474" id="Seg_7433" s="T472">eni</ta>
            <ta e="T476" id="Seg_7434" s="T474">hmm</ta>
            <ta e="T480" id="Seg_7435" s="T479">üleliː-r</ta>
            <ta e="T481" id="Seg_7436" s="T480">hɨldʼ-al-lar</ta>
            <ta e="T482" id="Seg_7437" s="T481">tak</ta>
            <ta e="T483" id="Seg_7438" s="T482">da</ta>
            <ta e="T492" id="Seg_7439" s="T490">e</ta>
            <ta e="T493" id="Seg_7440" s="T492">kel-en</ta>
            <ta e="T495" id="Seg_7441" s="T493">baran</ta>
            <ta e="T497" id="Seg_7442" s="T495">du</ta>
            <ta e="T504" id="Seg_7443" s="T500">aha</ta>
            <ta e="T556" id="Seg_7444" s="T555">tak</ta>
            <ta e="T557" id="Seg_7445" s="T556">onton</ta>
            <ta e="T558" id="Seg_7446" s="T557">načʼala</ta>
            <ta e="T560" id="Seg_7447" s="T558">kajdi͡ek</ta>
            <ta e="T561" id="Seg_7448" s="T560">baːr</ta>
            <ta e="T562" id="Seg_7449" s="T561">aragiː-msak</ta>
            <ta e="T563" id="Seg_7450" s="T562">bu͡ol-but-a</ta>
            <ta e="T571" id="Seg_7451" s="T569">eː</ta>
            <ta e="T573" id="Seg_7452" s="T571">bu</ta>
            <ta e="T575" id="Seg_7453" s="T573">kepsiː-r</ta>
            <ta e="T599" id="Seg_7454" s="T598">dʼaktar-ga</ta>
            <ta e="T600" id="Seg_7455" s="T599">bar-bɨt</ta>
            <ta e="T603" id="Seg_7456" s="T602">hɨt</ta>
            <ta e="T605" id="Seg_7457" s="T603">ol</ta>
            <ta e="T606" id="Seg_7458" s="T605">kihi-ni</ta>
            <ta e="T607" id="Seg_7459" s="T606">gɨtta</ta>
            <ta e="T609" id="Seg_7460" s="T607">bugurduk</ta>
            <ta e="T611" id="Seg_7461" s="T609">hɨldʼ-ɨ-bɨk-kɨn</ta>
            <ta e="T614" id="Seg_7462" s="T611">diː-r</ta>
            <ta e="T619" id="Seg_7463" s="T617">onton</ta>
            <ta e="T620" id="Seg_7464" s="T619">oks-u-but</ta>
            <ta e="T622" id="Seg_7465" s="T620">onton</ta>
            <ta e="T625" id="Seg_7466" s="T622">mʼilʼisʼije-ler</ta>
            <ta e="T628" id="Seg_7467" s="T625">kel-bit-ter</ta>
            <ta e="T632" id="Seg_7468" s="T628">aha</ta>
            <ta e="T639" id="Seg_7469" s="T635">tak</ta>
            <ta e="T655" id="Seg_7470" s="T653">načala</ta>
            <ta e="T657" id="Seg_7471" s="T655">baːr</ta>
            <ta e="T659" id="Seg_7472" s="T657">taŋas</ta>
            <ta e="T675" id="Seg_7473" s="T674">taŋas</ta>
            <ta e="T700" id="Seg_7474" s="T699">mʼilʼisʼije-ler</ta>
            <ta e="T701" id="Seg_7475" s="T700">kɨrbɨː-l-lar</ta>
            <ta e="T703" id="Seg_7476" s="T701">kanʼɨː-l-lar</ta>
            <ta e="T707" id="Seg_7477" s="T706">onton</ta>
            <ta e="T712" id="Seg_7478" s="T710">tak</ta>
            <ta e="T718" id="Seg_7479" s="T716">aː</ta>
            <ta e="T719" id="Seg_7480" s="T718">dʼi͡e-ti-ger</ta>
            <ta e="T720" id="Seg_7481" s="T719">kel-er-i-n</ta>
            <ta e="T724" id="Seg_7482" s="T722">a</ta>
            <ta e="T728" id="Seg_7483" s="T726">kajdi͡ek</ta>
            <ta e="T730" id="Seg_7484" s="T728">e-t-e=j</ta>
            <ta e="T731" id="Seg_7485" s="T730">aː</ta>
            <ta e="T732" id="Seg_7486" s="T731">aha</ta>
            <ta e="T734" id="Seg_7487" s="T733">taks-ɨ-bɨt</ta>
            <ta e="T738" id="Seg_7488" s="T737">tak</ta>
            <ta e="T751" id="Seg_7489" s="T750">aragiː</ta>
            <ta e="T753" id="Seg_7490" s="T751">is-pet</ta>
            <ta e="T754" id="Seg_7491" s="T753">bu͡ol-but</ta>
         </annotation>
         <annotation name="mp" tierref="mp-UoPP">
            <ta e="T26" id="Seg_7492" s="T21">aː</ta>
            <ta e="T68" id="Seg_7493" s="T66">bu</ta>
            <ta e="T71" id="Seg_7494" s="T68">navʼernaje</ta>
            <ta e="T90" id="Seg_7495" s="T89">olor-Ar</ta>
            <ta e="T93" id="Seg_7496" s="T91">a</ta>
            <ta e="T95" id="Seg_7497" s="T93">onno</ta>
            <ta e="T115" id="Seg_7498" s="T113">aragiː-tA</ta>
            <ta e="T126" id="Seg_7499" s="T124">taːk</ta>
            <ta e="T128" id="Seg_7500" s="T126">bu</ta>
            <ta e="T129" id="Seg_7501" s="T128">itte</ta>
            <ta e="T130" id="Seg_7502" s="T129">baːr</ta>
            <ta e="T131" id="Seg_7503" s="T130">e-BIT</ta>
            <ta e="T132" id="Seg_7504" s="T131">načʼala</ta>
            <ta e="T139" id="Seg_7505" s="T138">aragiː-LAː-A</ta>
            <ta e="T140" id="Seg_7506" s="T139">olor-Ar-LAr</ta>
            <ta e="T141" id="Seg_7507" s="T140">iti</ta>
            <ta e="T143" id="Seg_7508" s="T142">aː</ta>
            <ta e="T144" id="Seg_7509" s="T143">bu</ta>
            <ta e="T145" id="Seg_7510" s="T144">üleleː-A</ta>
            <ta e="T146" id="Seg_7511" s="T145">hɨrɨt-I-BIT</ta>
            <ta e="T147" id="Seg_7512" s="T146">že</ta>
            <ta e="T149" id="Seg_7513" s="T148">üle</ta>
            <ta e="T150" id="Seg_7514" s="T149">karčɨ</ta>
            <ta e="T151" id="Seg_7515" s="T150">oŋohun-A</ta>
            <ta e="T152" id="Seg_7516" s="T151">hɨrɨt-I-BIT</ta>
            <ta e="T159" id="Seg_7517" s="T157">taːk</ta>
            <ta e="T162" id="Seg_7518" s="T159">eta</ta>
            <ta e="T164" id="Seg_7519" s="T162">ogo-BIt</ta>
            <ta e="T165" id="Seg_7520" s="T164">küččügüj</ta>
            <ta e="T166" id="Seg_7521" s="T165">manna</ta>
            <ta e="T170" id="Seg_7522" s="T168">aga</ta>
            <ta e="T172" id="Seg_7523" s="T171">küččügüj</ta>
            <ta e="T173" id="Seg_7524" s="T172">türme-GA</ta>
            <ta e="T174" id="Seg_7525" s="T173">olor-Ar</ta>
            <ta e="T183" id="Seg_7526" s="T181">kojut</ta>
            <ta e="T185" id="Seg_7527" s="T183">kel-An</ta>
            <ta e="T187" id="Seg_7528" s="T185">bar-Ar</ta>
            <ta e="T197" id="Seg_7529" s="T195">aː</ta>
            <ta e="T199" id="Seg_7530" s="T197">kelin-tA</ta>
            <ta e="T201" id="Seg_7531" s="T199">iti</ta>
            <ta e="T211" id="Seg_7532" s="T209">taːk</ta>
            <ta e="T223" id="Seg_7533" s="T220">kel-An</ta>
            <ta e="T225" id="Seg_7534" s="T223">baran</ta>
            <ta e="T228" id="Seg_7535" s="T225">kepseː-A</ta>
            <ta e="T230" id="Seg_7536" s="T228">olor-Ar</ta>
            <ta e="T240" id="Seg_7537" s="T238">kajdi͡ek=Ij</ta>
            <ta e="T241" id="Seg_7538" s="T240">ka</ta>
            <ta e="T242" id="Seg_7539" s="T241">maːjdiːn-BIt</ta>
            <ta e="T244" id="Seg_7540" s="T243">a</ta>
            <ta e="T245" id="Seg_7541" s="T244">kanna</ta>
            <ta e="T246" id="Seg_7542" s="T245">baːr=Ij</ta>
            <ta e="T249" id="Seg_7543" s="T246">maːjdiːn</ta>
            <ta e="T252" id="Seg_7544" s="T249">körüs-Ar-LAr</ta>
            <ta e="T255" id="Seg_7545" s="T252">haŋa</ta>
            <ta e="T257" id="Seg_7546" s="T255">kiːr</ta>
            <ta e="T269" id="Seg_7547" s="T264">naːda</ta>
            <ta e="T275" id="Seg_7548" s="T271">üleleː-A</ta>
            <ta e="T277" id="Seg_7549" s="T275">bar-Ar</ta>
            <ta e="T281" id="Seg_7550" s="T277">bu</ta>
            <ta e="T287" id="Seg_7551" s="T283">hu͡ok</ta>
            <ta e="T288" id="Seg_7552" s="T287">hu͡ok</ta>
            <ta e="T290" id="Seg_7553" s="T288">aː</ta>
            <ta e="T292" id="Seg_7554" s="T290">beːbe</ta>
            <ta e="T293" id="Seg_7555" s="T292">kör-IAK-BIt</ta>
            <ta e="T304" id="Seg_7556" s="T303">ogo-BIt</ta>
            <ta e="T305" id="Seg_7557" s="T304">küččügüj</ta>
            <ta e="T306" id="Seg_7558" s="T305">itinne</ta>
            <ta e="T308" id="Seg_7559" s="T307">taːk</ta>
            <ta e="T309" id="Seg_7560" s="T308">bu</ta>
            <ta e="T310" id="Seg_7561" s="T309">kaːjɨː-GA</ta>
            <ta e="T311" id="Seg_7562" s="T310">olor-Ar</ta>
            <ta e="T322" id="Seg_7563" s="T321">bu</ta>
            <ta e="T323" id="Seg_7564" s="T322">künüːleː-An</ta>
            <ta e="T330" id="Seg_7565" s="T327">dʼaktar-tI-n</ta>
            <ta e="T334" id="Seg_7566" s="T330">kɨrbaː-Ar</ta>
            <ta e="T337" id="Seg_7567" s="T334">e-TI-tA</ta>
            <ta e="T357" id="Seg_7568" s="T355">eː</ta>
            <ta e="T359" id="Seg_7569" s="T357">onton</ta>
            <ta e="T361" id="Seg_7570" s="T359">kɨrbaː-Ar</ta>
            <ta e="T363" id="Seg_7571" s="T361">eː</ta>
            <ta e="T365" id="Seg_7572" s="T363">diː</ta>
            <ta e="T374" id="Seg_7573" s="T373">aga</ta>
            <ta e="T383" id="Seg_7574" s="T382">aga</ta>
            <ta e="T396" id="Seg_7575" s="T394">taːk</ta>
            <ta e="T403" id="Seg_7576" s="T400">mʼilʼisʼija-LAr</ta>
            <ta e="T406" id="Seg_7577" s="T403">kel-An</ta>
            <ta e="T410" id="Seg_7578" s="T406">ilt-BIT-LAr</ta>
            <ta e="T413" id="Seg_7579" s="T412">mʼilʼisʼija-LAr</ta>
            <ta e="T414" id="Seg_7580" s="T413">kel-An</ta>
            <ta e="T415" id="Seg_7581" s="T414">ilt-BIT-LAr</ta>
            <ta e="T416" id="Seg_7582" s="T415">onton</ta>
            <ta e="T417" id="Seg_7583" s="T416">bu-tI-GI-n</ta>
            <ta e="T418" id="Seg_7584" s="T417">ogus-I-BIT-tI-GAr</ta>
            <ta e="T420" id="Seg_7585" s="T419">kɨrbaː-BAT</ta>
            <ta e="T421" id="Seg_7586" s="T420">du͡o</ta>
            <ta e="T422" id="Seg_7587" s="T421">itinne</ta>
            <ta e="T423" id="Seg_7588" s="T422">kajdi͡ek</ta>
            <ta e="T424" id="Seg_7589" s="T423">da</ta>
            <ta e="T425" id="Seg_7590" s="T424">hu͡ok</ta>
            <ta e="T426" id="Seg_7591" s="T425">du͡o</ta>
            <ta e="T428" id="Seg_7592" s="T426">bolʼše</ta>
            <ta e="T444" id="Seg_7593" s="T440">aga</ta>
            <ta e="T452" id="Seg_7594" s="T451">eː</ta>
            <ta e="T456" id="Seg_7595" s="T455">taːk</ta>
            <ta e="T457" id="Seg_7596" s="T456">bu</ta>
            <ta e="T458" id="Seg_7597" s="T457">ka</ta>
            <ta e="T459" id="Seg_7598" s="T458">ogo-tA</ta>
            <ta e="T460" id="Seg_7599" s="T459">küččügüj</ta>
            <ta e="T462" id="Seg_7600" s="T460">er-TAK-InA</ta>
            <ta e="T465" id="Seg_7601" s="T463">üčügej</ta>
            <ta e="T467" id="Seg_7602" s="T465">bagajɨ-LIk</ta>
            <ta e="T470" id="Seg_7603" s="T467">olor-Ar</ta>
            <ta e="T472" id="Seg_7604" s="T470">e-TI-LAr</ta>
            <ta e="T474" id="Seg_7605" s="T472">eni</ta>
            <ta e="T476" id="Seg_7606" s="T474">hmm</ta>
            <ta e="T480" id="Seg_7607" s="T479">üleleː-Ar</ta>
            <ta e="T481" id="Seg_7608" s="T480">hɨrɨt-Ar-LAr</ta>
            <ta e="T482" id="Seg_7609" s="T481">taːk</ta>
            <ta e="T483" id="Seg_7610" s="T482">da</ta>
            <ta e="T492" id="Seg_7611" s="T490">e</ta>
            <ta e="T493" id="Seg_7612" s="T492">kel-An</ta>
            <ta e="T495" id="Seg_7613" s="T493">baran</ta>
            <ta e="T497" id="Seg_7614" s="T495">du͡o</ta>
            <ta e="T504" id="Seg_7615" s="T500">aː</ta>
            <ta e="T556" id="Seg_7616" s="T555">taːk</ta>
            <ta e="T557" id="Seg_7617" s="T556">onton</ta>
            <ta e="T558" id="Seg_7618" s="T557">načʼala</ta>
            <ta e="T560" id="Seg_7619" s="T558">kajdi͡ek</ta>
            <ta e="T561" id="Seg_7620" s="T560">baːr</ta>
            <ta e="T562" id="Seg_7621" s="T561">aragiː-msAk</ta>
            <ta e="T563" id="Seg_7622" s="T562">bu͡ol-BIT-tA</ta>
            <ta e="T571" id="Seg_7623" s="T569">eː</ta>
            <ta e="T573" id="Seg_7624" s="T571">bu</ta>
            <ta e="T575" id="Seg_7625" s="T573">kepseː-Ar</ta>
            <ta e="T599" id="Seg_7626" s="T598">dʼaktar-GA</ta>
            <ta e="T600" id="Seg_7627" s="T599">bar-BIT</ta>
            <ta e="T603" id="Seg_7628" s="T602">hɨt</ta>
            <ta e="T605" id="Seg_7629" s="T603">ol</ta>
            <ta e="T606" id="Seg_7630" s="T605">kihi-nI</ta>
            <ta e="T607" id="Seg_7631" s="T606">kɨtta</ta>
            <ta e="T609" id="Seg_7632" s="T607">bugurduk</ta>
            <ta e="T611" id="Seg_7633" s="T609">hɨrɨt-I-BIT-GIn</ta>
            <ta e="T614" id="Seg_7634" s="T611">di͡e-Ar</ta>
            <ta e="T619" id="Seg_7635" s="T617">onton</ta>
            <ta e="T620" id="Seg_7636" s="T619">ogus-I-BIT</ta>
            <ta e="T622" id="Seg_7637" s="T620">onton</ta>
            <ta e="T625" id="Seg_7638" s="T622">mʼilʼisʼija-LAr</ta>
            <ta e="T628" id="Seg_7639" s="T625">kel-BIT-LAr</ta>
            <ta e="T632" id="Seg_7640" s="T628">aga</ta>
            <ta e="T639" id="Seg_7641" s="T635">taːk</ta>
            <ta e="T655" id="Seg_7642" s="T653">načʼala</ta>
            <ta e="T657" id="Seg_7643" s="T655">baːr</ta>
            <ta e="T659" id="Seg_7644" s="T657">taŋas</ta>
            <ta e="T675" id="Seg_7645" s="T674">taŋas</ta>
            <ta e="T700" id="Seg_7646" s="T699">mʼilʼisʼija-LAr</ta>
            <ta e="T701" id="Seg_7647" s="T700">kɨrbaː-Ar-LAr</ta>
            <ta e="T703" id="Seg_7648" s="T701">kanʼaː-Ar-LAr</ta>
            <ta e="T707" id="Seg_7649" s="T706">onton</ta>
            <ta e="T712" id="Seg_7650" s="T710">taːk</ta>
            <ta e="T718" id="Seg_7651" s="T716">aː</ta>
            <ta e="T719" id="Seg_7652" s="T718">dʼi͡e-tI-GAr</ta>
            <ta e="T720" id="Seg_7653" s="T719">kel-Ar-tI-n</ta>
            <ta e="T724" id="Seg_7654" s="T722">a</ta>
            <ta e="T728" id="Seg_7655" s="T726">kajdi͡ek</ta>
            <ta e="T730" id="Seg_7656" s="T728">e-TI-tA=Ij</ta>
            <ta e="T731" id="Seg_7657" s="T730">aː</ta>
            <ta e="T732" id="Seg_7658" s="T731">aga</ta>
            <ta e="T734" id="Seg_7659" s="T733">tagɨs-I-BIT</ta>
            <ta e="T738" id="Seg_7660" s="T737">taːk</ta>
            <ta e="T751" id="Seg_7661" s="T750">aragiː</ta>
            <ta e="T753" id="Seg_7662" s="T751">is-BAT</ta>
            <ta e="T754" id="Seg_7663" s="T753">bu͡ol-BIT</ta>
         </annotation>
         <annotation name="ge" tierref="ge-UoPP">
            <ta e="T26" id="Seg_7664" s="T21">ah</ta>
            <ta e="T68" id="Seg_7665" s="T66">this.[NOM]</ta>
            <ta e="T71" id="Seg_7666" s="T68">probably</ta>
            <ta e="T90" id="Seg_7667" s="T89">sit-PRS.[3SG]</ta>
            <ta e="T93" id="Seg_7668" s="T91">and</ta>
            <ta e="T95" id="Seg_7669" s="T93">there</ta>
            <ta e="T115" id="Seg_7670" s="T113">spirit-3SG.[NOM]</ta>
            <ta e="T126" id="Seg_7671" s="T124">so</ta>
            <ta e="T128" id="Seg_7672" s="T126">this</ta>
            <ta e="T129" id="Seg_7673" s="T128">EMPH</ta>
            <ta e="T130" id="Seg_7674" s="T129">there.is</ta>
            <ta e="T131" id="Seg_7675" s="T130">be-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_7676" s="T131">beginning.[NOM]</ta>
            <ta e="T139" id="Seg_7677" s="T138">spirit-VBZ-CVB.SIM</ta>
            <ta e="T140" id="Seg_7678" s="T139">sit-PRS-3PL</ta>
            <ta e="T141" id="Seg_7679" s="T140">that.[NOM]</ta>
            <ta e="T143" id="Seg_7680" s="T142">ah</ta>
            <ta e="T144" id="Seg_7681" s="T143">this</ta>
            <ta e="T145" id="Seg_7682" s="T144">work-CVB.SIM</ta>
            <ta e="T146" id="Seg_7683" s="T145">go-EP-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_7684" s="T146">EMPH</ta>
            <ta e="T149" id="Seg_7685" s="T148">work.[NOM]</ta>
            <ta e="T150" id="Seg_7686" s="T149">money.[NOM]</ta>
            <ta e="T151" id="Seg_7687" s="T150">make-CVB.SIM</ta>
            <ta e="T152" id="Seg_7688" s="T151">go-EP-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_7689" s="T157">so</ta>
            <ta e="T162" id="Seg_7690" s="T159">EMPH</ta>
            <ta e="T164" id="Seg_7691" s="T162">child-1PL.[NOM]</ta>
            <ta e="T165" id="Seg_7692" s="T164">small.[NOM]</ta>
            <ta e="T166" id="Seg_7693" s="T165">here</ta>
            <ta e="T170" id="Seg_7694" s="T168">mhm</ta>
            <ta e="T172" id="Seg_7695" s="T171">small.[NOM]</ta>
            <ta e="T173" id="Seg_7696" s="T172">prison-DAT/LOC</ta>
            <ta e="T174" id="Seg_7697" s="T173">sit-PRS.[3SG]</ta>
            <ta e="T183" id="Seg_7698" s="T181">later</ta>
            <ta e="T185" id="Seg_7699" s="T183">come-CVB.SEQ</ta>
            <ta e="T187" id="Seg_7700" s="T185">go-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_7701" s="T195">ah</ta>
            <ta e="T199" id="Seg_7702" s="T197">back-3SG.[NOM]</ta>
            <ta e="T201" id="Seg_7703" s="T199">that.[NOM]</ta>
            <ta e="T211" id="Seg_7704" s="T209">so</ta>
            <ta e="T223" id="Seg_7705" s="T220">come-CVB.SEQ</ta>
            <ta e="T225" id="Seg_7706" s="T223">after</ta>
            <ta e="T228" id="Seg_7707" s="T225">tell-CVB.SIM</ta>
            <ta e="T230" id="Seg_7708" s="T228">sit-PRS.[3SG]</ta>
            <ta e="T240" id="Seg_7709" s="T238">whereto=Q</ta>
            <ta e="T241" id="Seg_7710" s="T240">well</ta>
            <ta e="T242" id="Seg_7711" s="T241">recent-1PL.[NOM]</ta>
            <ta e="T244" id="Seg_7712" s="T243">and</ta>
            <ta e="T245" id="Seg_7713" s="T244">where</ta>
            <ta e="T246" id="Seg_7714" s="T245">there.is=Q</ta>
            <ta e="T249" id="Seg_7715" s="T246">not.long.ago</ta>
            <ta e="T252" id="Seg_7716" s="T249">meet-PRS-3PL</ta>
            <ta e="T255" id="Seg_7717" s="T252">new</ta>
            <ta e="T257" id="Seg_7718" s="T255">come.back</ta>
            <ta e="T269" id="Seg_7719" s="T264">need.to</ta>
            <ta e="T275" id="Seg_7720" s="T271">work-CVB.SIM</ta>
            <ta e="T277" id="Seg_7721" s="T275">go-PRS.[3SG]</ta>
            <ta e="T281" id="Seg_7722" s="T277">this</ta>
            <ta e="T287" id="Seg_7723" s="T283">no</ta>
            <ta e="T288" id="Seg_7724" s="T287">no</ta>
            <ta e="T290" id="Seg_7725" s="T288">ah</ta>
            <ta e="T292" id="Seg_7726" s="T290">wait</ta>
            <ta e="T293" id="Seg_7727" s="T292">see-FUT-1PL</ta>
            <ta e="T304" id="Seg_7728" s="T303">child-1PL.[NOM]</ta>
            <ta e="T305" id="Seg_7729" s="T304">small.[NOM]</ta>
            <ta e="T306" id="Seg_7730" s="T305">there</ta>
            <ta e="T308" id="Seg_7731" s="T307">so</ta>
            <ta e="T309" id="Seg_7732" s="T308">this</ta>
            <ta e="T310" id="Seg_7733" s="T309">prison-DAT/LOC</ta>
            <ta e="T311" id="Seg_7734" s="T310">sit-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_7735" s="T321">this</ta>
            <ta e="T323" id="Seg_7736" s="T322">be.jealous-CVB.SEQ</ta>
            <ta e="T330" id="Seg_7737" s="T327">woman-3SG-ACC</ta>
            <ta e="T334" id="Seg_7738" s="T330">thrash-PTCP.PRS</ta>
            <ta e="T337" id="Seg_7739" s="T334">be-PST1-3SG</ta>
            <ta e="T357" id="Seg_7740" s="T355">AFFIRM</ta>
            <ta e="T359" id="Seg_7741" s="T357">then</ta>
            <ta e="T361" id="Seg_7742" s="T359">thrash-PRS.[3SG]</ta>
            <ta e="T363" id="Seg_7743" s="T361">AFFIRM</ta>
            <ta e="T365" id="Seg_7744" s="T363">EMPH</ta>
            <ta e="T374" id="Seg_7745" s="T373">mhm</ta>
            <ta e="T383" id="Seg_7746" s="T382">mhm</ta>
            <ta e="T396" id="Seg_7747" s="T394">so</ta>
            <ta e="T403" id="Seg_7748" s="T400">police-PL.[NOM]</ta>
            <ta e="T406" id="Seg_7749" s="T403">come-CVB.SEQ</ta>
            <ta e="T410" id="Seg_7750" s="T406">bring-PST2-3PL</ta>
            <ta e="T413" id="Seg_7751" s="T412">police-PL.[NOM]</ta>
            <ta e="T414" id="Seg_7752" s="T413">come-CVB.SEQ</ta>
            <ta e="T415" id="Seg_7753" s="T414">bring-PST2-3PL</ta>
            <ta e="T416" id="Seg_7754" s="T415">then</ta>
            <ta e="T417" id="Seg_7755" s="T416">this-3SG-2SG-ACC</ta>
            <ta e="T418" id="Seg_7756" s="T417">beat-EP-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T420" id="Seg_7757" s="T419">thrash-NEG.[3SG]</ta>
            <ta e="T421" id="Seg_7758" s="T420">MOD</ta>
            <ta e="T422" id="Seg_7759" s="T421">there</ta>
            <ta e="T423" id="Seg_7760" s="T422">whereto</ta>
            <ta e="T424" id="Seg_7761" s="T423">NEG</ta>
            <ta e="T425" id="Seg_7762" s="T424">NEG.EX</ta>
            <ta e="T426" id="Seg_7763" s="T425">MOD</ta>
            <ta e="T428" id="Seg_7764" s="T426">more</ta>
            <ta e="T444" id="Seg_7765" s="T440">mhm</ta>
            <ta e="T452" id="Seg_7766" s="T451">AFFIRM</ta>
            <ta e="T456" id="Seg_7767" s="T455">so</ta>
            <ta e="T457" id="Seg_7768" s="T456">this</ta>
            <ta e="T458" id="Seg_7769" s="T457">well</ta>
            <ta e="T459" id="Seg_7770" s="T458">child-3SG.[NOM]</ta>
            <ta e="T460" id="Seg_7771" s="T459">small.[NOM]</ta>
            <ta e="T462" id="Seg_7772" s="T460">be-TEMP-3SG</ta>
            <ta e="T465" id="Seg_7773" s="T463">good.[NOM]</ta>
            <ta e="T467" id="Seg_7774" s="T465">very-ADVZ</ta>
            <ta e="T470" id="Seg_7775" s="T467">live-PTCP.PRS</ta>
            <ta e="T472" id="Seg_7776" s="T470">be-PST1-3PL</ta>
            <ta e="T474" id="Seg_7777" s="T472">apparently</ta>
            <ta e="T476" id="Seg_7778" s="T474">hmm</ta>
            <ta e="T480" id="Seg_7779" s="T479">work-PTCP.PRS</ta>
            <ta e="T481" id="Seg_7780" s="T480">go-PRS-3PL</ta>
            <ta e="T482" id="Seg_7781" s="T481">so</ta>
            <ta e="T483" id="Seg_7782" s="T482">EMPH</ta>
            <ta e="T492" id="Seg_7783" s="T490">eh</ta>
            <ta e="T493" id="Seg_7784" s="T492">come-CVB.SEQ</ta>
            <ta e="T495" id="Seg_7785" s="T493">after</ta>
            <ta e="T497" id="Seg_7786" s="T495">Q</ta>
            <ta e="T504" id="Seg_7787" s="T500">ah</ta>
            <ta e="T556" id="Seg_7788" s="T555">so</ta>
            <ta e="T557" id="Seg_7789" s="T556">then</ta>
            <ta e="T558" id="Seg_7790" s="T557">beginning.[NOM]</ta>
            <ta e="T560" id="Seg_7791" s="T558">whereto</ta>
            <ta e="T561" id="Seg_7792" s="T560">there.is</ta>
            <ta e="T562" id="Seg_7793" s="T561">spirit-PHIL.[NOM]</ta>
            <ta e="T563" id="Seg_7794" s="T562">become-PST2-3SG</ta>
            <ta e="T571" id="Seg_7795" s="T569">AFFIRM</ta>
            <ta e="T573" id="Seg_7796" s="T571">this</ta>
            <ta e="T575" id="Seg_7797" s="T573">tell-PRS.[3SG]</ta>
            <ta e="T599" id="Seg_7798" s="T598">woman-DAT/LOC</ta>
            <ta e="T600" id="Seg_7799" s="T599">go-PST2.[3SG]</ta>
            <ta e="T603" id="Seg_7800" s="T602">lie.[IMP.2SG]</ta>
            <ta e="T605" id="Seg_7801" s="T603">that</ta>
            <ta e="T606" id="Seg_7802" s="T605">human.being-ACC</ta>
            <ta e="T607" id="Seg_7803" s="T606">with</ta>
            <ta e="T609" id="Seg_7804" s="T607">like.this</ta>
            <ta e="T611" id="Seg_7805" s="T609">go-EP-PST2-2SG</ta>
            <ta e="T614" id="Seg_7806" s="T611">say-PRS.[3SG]</ta>
            <ta e="T619" id="Seg_7807" s="T617">then</ta>
            <ta e="T620" id="Seg_7808" s="T619">beat-EP-PST2.[3SG]</ta>
            <ta e="T622" id="Seg_7809" s="T620">then</ta>
            <ta e="T625" id="Seg_7810" s="T622">police-PL.[NOM]</ta>
            <ta e="T628" id="Seg_7811" s="T625">come-PST2-3PL</ta>
            <ta e="T632" id="Seg_7812" s="T628">mhm</ta>
            <ta e="T639" id="Seg_7813" s="T635">so</ta>
            <ta e="T655" id="Seg_7814" s="T653">beginning.[NOM]</ta>
            <ta e="T657" id="Seg_7815" s="T655">there.is</ta>
            <ta e="T659" id="Seg_7816" s="T657">clothes.[NOM]</ta>
            <ta e="T675" id="Seg_7817" s="T674">clothes.[NOM]</ta>
            <ta e="T700" id="Seg_7818" s="T699">police-PL.[NOM]</ta>
            <ta e="T701" id="Seg_7819" s="T700">thrash-PRS-3PL</ta>
            <ta e="T703" id="Seg_7820" s="T701">and.so.on-PRS-3PL</ta>
            <ta e="T707" id="Seg_7821" s="T706">then</ta>
            <ta e="T712" id="Seg_7822" s="T710">so</ta>
            <ta e="T718" id="Seg_7823" s="T716">ah</ta>
            <ta e="T719" id="Seg_7824" s="T718">house-3SG-DAT/LOC</ta>
            <ta e="T720" id="Seg_7825" s="T719">come-PTCP.PRS-3SG-ACC</ta>
            <ta e="T724" id="Seg_7826" s="T722">and</ta>
            <ta e="T728" id="Seg_7827" s="T726">whereto</ta>
            <ta e="T730" id="Seg_7828" s="T728">be-PST1-3SG=Q</ta>
            <ta e="T731" id="Seg_7829" s="T730">ah</ta>
            <ta e="T732" id="Seg_7830" s="T731">mhm</ta>
            <ta e="T734" id="Seg_7831" s="T733">go.out-EP-PST2.[3SG]</ta>
            <ta e="T738" id="Seg_7832" s="T737">so</ta>
            <ta e="T751" id="Seg_7833" s="T750">spirit.[NOM]</ta>
            <ta e="T753" id="Seg_7834" s="T751">drink-NEG.PTCP</ta>
            <ta e="T754" id="Seg_7835" s="T753">become-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-UoPP">
            <ta e="T26" id="Seg_7836" s="T21">ah</ta>
            <ta e="T68" id="Seg_7837" s="T66">dieses.[NOM]</ta>
            <ta e="T71" id="Seg_7838" s="T68">wahrscheinlich</ta>
            <ta e="T90" id="Seg_7839" s="T89">sitzen-PRS.[3SG]</ta>
            <ta e="T93" id="Seg_7840" s="T91">und</ta>
            <ta e="T95" id="Seg_7841" s="T93">dort</ta>
            <ta e="T115" id="Seg_7842" s="T113">Schnaps-3SG.[NOM]</ta>
            <ta e="T126" id="Seg_7843" s="T124">so</ta>
            <ta e="T128" id="Seg_7844" s="T126">dieses</ta>
            <ta e="T129" id="Seg_7845" s="T128">EMPH</ta>
            <ta e="T130" id="Seg_7846" s="T129">es.gibt</ta>
            <ta e="T131" id="Seg_7847" s="T130">sein-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_7848" s="T131">Anfang.[NOM]</ta>
            <ta e="T139" id="Seg_7849" s="T138">Schnaps-VBZ-CVB.SIM</ta>
            <ta e="T140" id="Seg_7850" s="T139">sitzen-PRS-3PL</ta>
            <ta e="T141" id="Seg_7851" s="T140">dieses.[NOM]</ta>
            <ta e="T143" id="Seg_7852" s="T142">ah</ta>
            <ta e="T144" id="Seg_7853" s="T143">dieses</ta>
            <ta e="T145" id="Seg_7854" s="T144">arbeiten-CVB.SIM</ta>
            <ta e="T146" id="Seg_7855" s="T145">gehen-EP-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_7856" s="T146">EMPH</ta>
            <ta e="T149" id="Seg_7857" s="T148">Arbeit.[NOM]</ta>
            <ta e="T150" id="Seg_7858" s="T149">Geld.[NOM]</ta>
            <ta e="T151" id="Seg_7859" s="T150">machen-CVB.SIM</ta>
            <ta e="T152" id="Seg_7860" s="T151">gehen-EP-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_7861" s="T157">so</ta>
            <ta e="T162" id="Seg_7862" s="T159">EMPH</ta>
            <ta e="T164" id="Seg_7863" s="T162">Kind-1PL.[NOM]</ta>
            <ta e="T165" id="Seg_7864" s="T164">klein.[NOM]</ta>
            <ta e="T166" id="Seg_7865" s="T165">hier</ta>
            <ta e="T170" id="Seg_7866" s="T168">aha</ta>
            <ta e="T172" id="Seg_7867" s="T171">klein.[NOM]</ta>
            <ta e="T173" id="Seg_7868" s="T172">Gefängnis-DAT/LOC</ta>
            <ta e="T174" id="Seg_7869" s="T173">sitzen-PRS.[3SG]</ta>
            <ta e="T183" id="Seg_7870" s="T181">später</ta>
            <ta e="T185" id="Seg_7871" s="T183">kommen-CVB.SEQ</ta>
            <ta e="T187" id="Seg_7872" s="T185">gehen-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_7873" s="T195">ah</ta>
            <ta e="T199" id="Seg_7874" s="T197">Hinterteil-3SG.[NOM]</ta>
            <ta e="T201" id="Seg_7875" s="T199">dieses.[NOM]</ta>
            <ta e="T211" id="Seg_7876" s="T209">so</ta>
            <ta e="T223" id="Seg_7877" s="T220">kommen-CVB.SEQ</ta>
            <ta e="T225" id="Seg_7878" s="T223">nachdem</ta>
            <ta e="T228" id="Seg_7879" s="T225">erzählen-CVB.SIM</ta>
            <ta e="T230" id="Seg_7880" s="T228">sitzen-PRS.[3SG]</ta>
            <ta e="T240" id="Seg_7881" s="T238">wohin=Q</ta>
            <ta e="T241" id="Seg_7882" s="T240">nun</ta>
            <ta e="T242" id="Seg_7883" s="T241">kürzlich-1PL.[NOM]</ta>
            <ta e="T244" id="Seg_7884" s="T243">und</ta>
            <ta e="T245" id="Seg_7885" s="T244">wo</ta>
            <ta e="T246" id="Seg_7886" s="T245">es.gibt=Q</ta>
            <ta e="T249" id="Seg_7887" s="T246">vor.Kurzem</ta>
            <ta e="T252" id="Seg_7888" s="T249">treffen-PRS-3PL</ta>
            <ta e="T255" id="Seg_7889" s="T252">neu</ta>
            <ta e="T257" id="Seg_7890" s="T255">zurückkommen</ta>
            <ta e="T269" id="Seg_7891" s="T264">man.muss</ta>
            <ta e="T275" id="Seg_7892" s="T271">arbeiten-CVB.SIM</ta>
            <ta e="T277" id="Seg_7893" s="T275">gehen-PRS.[3SG]</ta>
            <ta e="T281" id="Seg_7894" s="T277">dieses</ta>
            <ta e="T287" id="Seg_7895" s="T283">nein</ta>
            <ta e="T288" id="Seg_7896" s="T287">nein</ta>
            <ta e="T290" id="Seg_7897" s="T288">ah</ta>
            <ta e="T292" id="Seg_7898" s="T290">warte</ta>
            <ta e="T293" id="Seg_7899" s="T292">sehen-FUT-1PL</ta>
            <ta e="T304" id="Seg_7900" s="T303">Kind-1PL.[NOM]</ta>
            <ta e="T305" id="Seg_7901" s="T304">klein.[NOM]</ta>
            <ta e="T306" id="Seg_7902" s="T305">dort</ta>
            <ta e="T308" id="Seg_7903" s="T307">so</ta>
            <ta e="T309" id="Seg_7904" s="T308">dieses</ta>
            <ta e="T310" id="Seg_7905" s="T309">Gefängnis-DAT/LOC</ta>
            <ta e="T311" id="Seg_7906" s="T310">sitzen-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_7907" s="T321">dieses</ta>
            <ta e="T323" id="Seg_7908" s="T322">eifersüchtig.sein-CVB.SEQ</ta>
            <ta e="T330" id="Seg_7909" s="T327">Frau-3SG-ACC</ta>
            <ta e="T334" id="Seg_7910" s="T330">prügeln-PTCP.PRS</ta>
            <ta e="T337" id="Seg_7911" s="T334">sein-PST1-3SG</ta>
            <ta e="T357" id="Seg_7912" s="T355">AFFIRM</ta>
            <ta e="T359" id="Seg_7913" s="T357">dann</ta>
            <ta e="T361" id="Seg_7914" s="T359">prügeln-PRS.[3SG]</ta>
            <ta e="T363" id="Seg_7915" s="T361">AFFIRM</ta>
            <ta e="T365" id="Seg_7916" s="T363">EMPH</ta>
            <ta e="T374" id="Seg_7917" s="T373">aha</ta>
            <ta e="T383" id="Seg_7918" s="T382">aha</ta>
            <ta e="T396" id="Seg_7919" s="T394">so</ta>
            <ta e="T403" id="Seg_7920" s="T400">Polizei-PL.[NOM]</ta>
            <ta e="T406" id="Seg_7921" s="T403">kommen-CVB.SEQ</ta>
            <ta e="T410" id="Seg_7922" s="T406">bringen-PST2-3PL</ta>
            <ta e="T413" id="Seg_7923" s="T412">Polizei-PL.[NOM]</ta>
            <ta e="T414" id="Seg_7924" s="T413">kommen-CVB.SEQ</ta>
            <ta e="T415" id="Seg_7925" s="T414">bringen-PST2-3PL</ta>
            <ta e="T416" id="Seg_7926" s="T415">dann</ta>
            <ta e="T417" id="Seg_7927" s="T416">dieses-3SG-2SG-ACC</ta>
            <ta e="T418" id="Seg_7928" s="T417">schlagen-EP-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T420" id="Seg_7929" s="T419">prügeln-NEG.[3SG]</ta>
            <ta e="T421" id="Seg_7930" s="T420">MOD</ta>
            <ta e="T422" id="Seg_7931" s="T421">dort</ta>
            <ta e="T423" id="Seg_7932" s="T422">wohin</ta>
            <ta e="T424" id="Seg_7933" s="T423">NEG</ta>
            <ta e="T425" id="Seg_7934" s="T424">NEG.EX</ta>
            <ta e="T426" id="Seg_7935" s="T425">MOD</ta>
            <ta e="T428" id="Seg_7936" s="T426">mehr</ta>
            <ta e="T444" id="Seg_7937" s="T440">aha</ta>
            <ta e="T452" id="Seg_7938" s="T451">AFFIRM</ta>
            <ta e="T456" id="Seg_7939" s="T455">so</ta>
            <ta e="T457" id="Seg_7940" s="T456">dieses</ta>
            <ta e="T458" id="Seg_7941" s="T457">nun</ta>
            <ta e="T459" id="Seg_7942" s="T458">Kind-3SG.[NOM]</ta>
            <ta e="T460" id="Seg_7943" s="T459">klein.[NOM]</ta>
            <ta e="T462" id="Seg_7944" s="T460">sein-TEMP-3SG</ta>
            <ta e="T465" id="Seg_7945" s="T463">gut.[NOM]</ta>
            <ta e="T467" id="Seg_7946" s="T465">sehr-ADVZ</ta>
            <ta e="T470" id="Seg_7947" s="T467">leben-PTCP.PRS</ta>
            <ta e="T472" id="Seg_7948" s="T470">sein-PST1-3PL</ta>
            <ta e="T474" id="Seg_7949" s="T472">offenbar</ta>
            <ta e="T476" id="Seg_7950" s="T474">hmm</ta>
            <ta e="T480" id="Seg_7951" s="T479">arbeiten-PTCP.PRS</ta>
            <ta e="T481" id="Seg_7952" s="T480">gehen-PRS-3PL</ta>
            <ta e="T482" id="Seg_7953" s="T481">so</ta>
            <ta e="T483" id="Seg_7954" s="T482">EMPH</ta>
            <ta e="T492" id="Seg_7955" s="T490">äh</ta>
            <ta e="T493" id="Seg_7956" s="T492">kommen-CVB.SEQ</ta>
            <ta e="T495" id="Seg_7957" s="T493">nachdem</ta>
            <ta e="T497" id="Seg_7958" s="T495">Q</ta>
            <ta e="T504" id="Seg_7959" s="T500">ah</ta>
            <ta e="T556" id="Seg_7960" s="T555">so</ta>
            <ta e="T557" id="Seg_7961" s="T556">dann</ta>
            <ta e="T558" id="Seg_7962" s="T557">Anfang.[NOM]</ta>
            <ta e="T560" id="Seg_7963" s="T558">wohin</ta>
            <ta e="T561" id="Seg_7964" s="T560">es.gibt</ta>
            <ta e="T562" id="Seg_7965" s="T561">Schnaps-PHIL.[NOM]</ta>
            <ta e="T563" id="Seg_7966" s="T562">werden-PST2-3SG</ta>
            <ta e="T571" id="Seg_7967" s="T569">AFFIRM</ta>
            <ta e="T573" id="Seg_7968" s="T571">dieses</ta>
            <ta e="T575" id="Seg_7969" s="T573">erzählen-PRS.[3SG]</ta>
            <ta e="T599" id="Seg_7970" s="T598">Frau-DAT/LOC</ta>
            <ta e="T600" id="Seg_7971" s="T599">gehen-PST2.[3SG]</ta>
            <ta e="T603" id="Seg_7972" s="T602">liegen.[IMP.2SG]</ta>
            <ta e="T605" id="Seg_7973" s="T603">jenes</ta>
            <ta e="T606" id="Seg_7974" s="T605">Mensch-ACC</ta>
            <ta e="T607" id="Seg_7975" s="T606">mit</ta>
            <ta e="T609" id="Seg_7976" s="T607">so</ta>
            <ta e="T611" id="Seg_7977" s="T609">gehen-EP-PST2-2SG</ta>
            <ta e="T614" id="Seg_7978" s="T611">sagen-PRS.[3SG]</ta>
            <ta e="T619" id="Seg_7979" s="T617">dann</ta>
            <ta e="T620" id="Seg_7980" s="T619">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T622" id="Seg_7981" s="T620">dann</ta>
            <ta e="T625" id="Seg_7982" s="T622">Polizei-PL.[NOM]</ta>
            <ta e="T628" id="Seg_7983" s="T625">kommen-PST2-3PL</ta>
            <ta e="T632" id="Seg_7984" s="T628">aha</ta>
            <ta e="T639" id="Seg_7985" s="T635">so</ta>
            <ta e="T655" id="Seg_7986" s="T653">Anfang.[NOM]</ta>
            <ta e="T657" id="Seg_7987" s="T655">es.gibt</ta>
            <ta e="T659" id="Seg_7988" s="T657">Kleidung.[NOM]</ta>
            <ta e="T675" id="Seg_7989" s="T674">Kleidung.[NOM]</ta>
            <ta e="T700" id="Seg_7990" s="T699">Polizei-PL.[NOM]</ta>
            <ta e="T701" id="Seg_7991" s="T700">prügeln-PRS-3PL</ta>
            <ta e="T703" id="Seg_7992" s="T701">und.so.weiter-PRS-3PL</ta>
            <ta e="T707" id="Seg_7993" s="T706">dann</ta>
            <ta e="T712" id="Seg_7994" s="T710">so</ta>
            <ta e="T718" id="Seg_7995" s="T716">ah</ta>
            <ta e="T719" id="Seg_7996" s="T718">Haus-3SG-DAT/LOC</ta>
            <ta e="T720" id="Seg_7997" s="T719">kommen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T724" id="Seg_7998" s="T722">und</ta>
            <ta e="T728" id="Seg_7999" s="T726">wohin</ta>
            <ta e="T730" id="Seg_8000" s="T728">sein-PST1-3SG=Q</ta>
            <ta e="T731" id="Seg_8001" s="T730">ah</ta>
            <ta e="T732" id="Seg_8002" s="T731">aha</ta>
            <ta e="T734" id="Seg_8003" s="T733">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T738" id="Seg_8004" s="T737">so</ta>
            <ta e="T751" id="Seg_8005" s="T750">Schnaps.[NOM]</ta>
            <ta e="T753" id="Seg_8006" s="T751">trinken-NEG.PTCP</ta>
            <ta e="T754" id="Seg_8007" s="T753">werden-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-UoPP">
            <ta e="T26" id="Seg_8008" s="T21">ах</ta>
            <ta e="T68" id="Seg_8009" s="T66">этот.[NOM]</ta>
            <ta e="T71" id="Seg_8010" s="T68">наверное</ta>
            <ta e="T90" id="Seg_8011" s="T89">сидеть-PRS.[3SG]</ta>
            <ta e="T93" id="Seg_8012" s="T91">а</ta>
            <ta e="T95" id="Seg_8013" s="T93">там</ta>
            <ta e="T115" id="Seg_8014" s="T113">спиртное-3SG.[NOM]</ta>
            <ta e="T126" id="Seg_8015" s="T124">так</ta>
            <ta e="T128" id="Seg_8016" s="T126">этот</ta>
            <ta e="T129" id="Seg_8017" s="T128">EMPH</ta>
            <ta e="T130" id="Seg_8018" s="T129">есть</ta>
            <ta e="T131" id="Seg_8019" s="T130">быть-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_8020" s="T131">начало.[NOM]</ta>
            <ta e="T139" id="Seg_8021" s="T138">спиртное-VBZ-CVB.SIM</ta>
            <ta e="T140" id="Seg_8022" s="T139">сидеть-PRS-3PL</ta>
            <ta e="T141" id="Seg_8023" s="T140">тот.[NOM]</ta>
            <ta e="T143" id="Seg_8024" s="T142">ах</ta>
            <ta e="T144" id="Seg_8025" s="T143">этот</ta>
            <ta e="T145" id="Seg_8026" s="T144">работать-CVB.SIM</ta>
            <ta e="T146" id="Seg_8027" s="T145">идти-EP-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_8028" s="T146">EMPH</ta>
            <ta e="T149" id="Seg_8029" s="T148">работа.[NOM]</ta>
            <ta e="T150" id="Seg_8030" s="T149">деньги.[NOM]</ta>
            <ta e="T151" id="Seg_8031" s="T150">делать-CVB.SIM</ta>
            <ta e="T152" id="Seg_8032" s="T151">идти-EP-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_8033" s="T157">так</ta>
            <ta e="T162" id="Seg_8034" s="T159">EMPH</ta>
            <ta e="T164" id="Seg_8035" s="T162">ребенок-1PL.[NOM]</ta>
            <ta e="T165" id="Seg_8036" s="T164">маленький.[NOM]</ta>
            <ta e="T166" id="Seg_8037" s="T165">здесь</ta>
            <ta e="T170" id="Seg_8038" s="T168">ага</ta>
            <ta e="T172" id="Seg_8039" s="T171">маленький.[NOM]</ta>
            <ta e="T173" id="Seg_8040" s="T172">тюрьма-DAT/LOC</ta>
            <ta e="T174" id="Seg_8041" s="T173">сидеть-PRS.[3SG]</ta>
            <ta e="T183" id="Seg_8042" s="T181">позже</ta>
            <ta e="T185" id="Seg_8043" s="T183">приходить-CVB.SEQ</ta>
            <ta e="T187" id="Seg_8044" s="T185">идти-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_8045" s="T195">ах</ta>
            <ta e="T199" id="Seg_8046" s="T197">задняя.часть-3SG.[NOM]</ta>
            <ta e="T201" id="Seg_8047" s="T199">тот.[NOM]</ta>
            <ta e="T211" id="Seg_8048" s="T209">так</ta>
            <ta e="T223" id="Seg_8049" s="T220">приходить-CVB.SEQ</ta>
            <ta e="T225" id="Seg_8050" s="T223">после</ta>
            <ta e="T228" id="Seg_8051" s="T225">рассказывать-CVB.SIM</ta>
            <ta e="T230" id="Seg_8052" s="T228">сидеть-PRS.[3SG]</ta>
            <ta e="T240" id="Seg_8053" s="T238">куда=Q</ta>
            <ta e="T241" id="Seg_8054" s="T240">вот</ta>
            <ta e="T242" id="Seg_8055" s="T241">давешний-1PL.[NOM]</ta>
            <ta e="T244" id="Seg_8056" s="T243">а</ta>
            <ta e="T245" id="Seg_8057" s="T244">где</ta>
            <ta e="T246" id="Seg_8058" s="T245">есть=Q</ta>
            <ta e="T249" id="Seg_8059" s="T246">недавно</ta>
            <ta e="T252" id="Seg_8060" s="T249">встречать-PRS-3PL</ta>
            <ta e="T255" id="Seg_8061" s="T252">новый</ta>
            <ta e="T257" id="Seg_8062" s="T255">возвращаться</ta>
            <ta e="T269" id="Seg_8063" s="T264">надо</ta>
            <ta e="T275" id="Seg_8064" s="T271">работать-CVB.SIM</ta>
            <ta e="T277" id="Seg_8065" s="T275">идти-PRS.[3SG]</ta>
            <ta e="T281" id="Seg_8066" s="T277">этот</ta>
            <ta e="T287" id="Seg_8067" s="T283">нет</ta>
            <ta e="T288" id="Seg_8068" s="T287">нет</ta>
            <ta e="T290" id="Seg_8069" s="T288">ах</ta>
            <ta e="T292" id="Seg_8070" s="T290">подожди</ta>
            <ta e="T293" id="Seg_8071" s="T292">видеть-FUT-1PL</ta>
            <ta e="T304" id="Seg_8072" s="T303">ребенок-1PL.[NOM]</ta>
            <ta e="T305" id="Seg_8073" s="T304">маленький.[NOM]</ta>
            <ta e="T306" id="Seg_8074" s="T305">там</ta>
            <ta e="T308" id="Seg_8075" s="T307">так</ta>
            <ta e="T309" id="Seg_8076" s="T308">этот</ta>
            <ta e="T310" id="Seg_8077" s="T309">тюрьма-DAT/LOC</ta>
            <ta e="T311" id="Seg_8078" s="T310">сидеть-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_8079" s="T321">этот</ta>
            <ta e="T323" id="Seg_8080" s="T322">ревновать-CVB.SEQ</ta>
            <ta e="T330" id="Seg_8081" s="T327">жена-3SG-ACC</ta>
            <ta e="T334" id="Seg_8082" s="T330">бить-PTCP.PRS</ta>
            <ta e="T337" id="Seg_8083" s="T334">быть-PST1-3SG</ta>
            <ta e="T357" id="Seg_8084" s="T355">AFFIRM</ta>
            <ta e="T359" id="Seg_8085" s="T357">потом</ta>
            <ta e="T361" id="Seg_8086" s="T359">бить-PRS.[3SG]</ta>
            <ta e="T363" id="Seg_8087" s="T361">AFFIRM</ta>
            <ta e="T365" id="Seg_8088" s="T363">EMPH</ta>
            <ta e="T374" id="Seg_8089" s="T373">ага</ta>
            <ta e="T383" id="Seg_8090" s="T382">ага</ta>
            <ta e="T396" id="Seg_8091" s="T394">так</ta>
            <ta e="T403" id="Seg_8092" s="T400">милиция-PL.[NOM]</ta>
            <ta e="T406" id="Seg_8093" s="T403">приходить-CVB.SEQ</ta>
            <ta e="T410" id="Seg_8094" s="T406">приносить-PST2-3PL</ta>
            <ta e="T413" id="Seg_8095" s="T412">милиция-PL.[NOM]</ta>
            <ta e="T414" id="Seg_8096" s="T413">приходить-CVB.SEQ</ta>
            <ta e="T415" id="Seg_8097" s="T414">приносить-PST2-3PL</ta>
            <ta e="T416" id="Seg_8098" s="T415">потом</ta>
            <ta e="T417" id="Seg_8099" s="T416">этот-3SG-2SG-ACC</ta>
            <ta e="T418" id="Seg_8100" s="T417">бить-EP-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T420" id="Seg_8101" s="T419">бить-NEG.[3SG]</ta>
            <ta e="T421" id="Seg_8102" s="T420">MOD</ta>
            <ta e="T422" id="Seg_8103" s="T421">там</ta>
            <ta e="T423" id="Seg_8104" s="T422">куда</ta>
            <ta e="T424" id="Seg_8105" s="T423">NEG</ta>
            <ta e="T425" id="Seg_8106" s="T424">NEG.EX</ta>
            <ta e="T426" id="Seg_8107" s="T425">MOD</ta>
            <ta e="T428" id="Seg_8108" s="T426">больше</ta>
            <ta e="T444" id="Seg_8109" s="T440">ага</ta>
            <ta e="T452" id="Seg_8110" s="T451">AFFIRM</ta>
            <ta e="T456" id="Seg_8111" s="T455">так</ta>
            <ta e="T457" id="Seg_8112" s="T456">этот</ta>
            <ta e="T458" id="Seg_8113" s="T457">вот</ta>
            <ta e="T459" id="Seg_8114" s="T458">ребенок-3SG.[NOM]</ta>
            <ta e="T460" id="Seg_8115" s="T459">маленький.[NOM]</ta>
            <ta e="T462" id="Seg_8116" s="T460">быть-TEMP-3SG</ta>
            <ta e="T465" id="Seg_8117" s="T463">хороший.[NOM]</ta>
            <ta e="T467" id="Seg_8118" s="T465">очень-ADVZ</ta>
            <ta e="T470" id="Seg_8119" s="T467">жить-PTCP.PRS</ta>
            <ta e="T472" id="Seg_8120" s="T470">быть-PST1-3PL</ta>
            <ta e="T474" id="Seg_8121" s="T472">очевидно</ta>
            <ta e="T476" id="Seg_8122" s="T474">мм</ta>
            <ta e="T480" id="Seg_8123" s="T479">работать-PTCP.PRS</ta>
            <ta e="T481" id="Seg_8124" s="T480">идти-PRS-3PL</ta>
            <ta e="T482" id="Seg_8125" s="T481">так</ta>
            <ta e="T483" id="Seg_8126" s="T482">EMPH</ta>
            <ta e="T492" id="Seg_8127" s="T490">э</ta>
            <ta e="T493" id="Seg_8128" s="T492">приходить-CVB.SEQ</ta>
            <ta e="T495" id="Seg_8129" s="T493">после</ta>
            <ta e="T497" id="Seg_8130" s="T495">Q</ta>
            <ta e="T504" id="Seg_8131" s="T500">ах</ta>
            <ta e="T556" id="Seg_8132" s="T555">так</ta>
            <ta e="T557" id="Seg_8133" s="T556">потом</ta>
            <ta e="T558" id="Seg_8134" s="T557">начало.[NOM]</ta>
            <ta e="T560" id="Seg_8135" s="T558">куда</ta>
            <ta e="T561" id="Seg_8136" s="T560">есть</ta>
            <ta e="T562" id="Seg_8137" s="T561">спиртное-PHIL.[NOM]</ta>
            <ta e="T563" id="Seg_8138" s="T562">становиться-PST2-3SG</ta>
            <ta e="T571" id="Seg_8139" s="T569">AFFIRM</ta>
            <ta e="T573" id="Seg_8140" s="T571">этот</ta>
            <ta e="T575" id="Seg_8141" s="T573">рассказывать-PRS.[3SG]</ta>
            <ta e="T599" id="Seg_8142" s="T598">жена-DAT/LOC</ta>
            <ta e="T600" id="Seg_8143" s="T599">идти-PST2.[3SG]</ta>
            <ta e="T603" id="Seg_8144" s="T602">лежать.[IMP.2SG]</ta>
            <ta e="T605" id="Seg_8145" s="T603">тот</ta>
            <ta e="T606" id="Seg_8146" s="T605">человек-ACC</ta>
            <ta e="T607" id="Seg_8147" s="T606">с</ta>
            <ta e="T609" id="Seg_8148" s="T607">так</ta>
            <ta e="T611" id="Seg_8149" s="T609">идти-EP-PST2-2SG</ta>
            <ta e="T614" id="Seg_8150" s="T611">говорить-PRS.[3SG]</ta>
            <ta e="T619" id="Seg_8151" s="T617">потом</ta>
            <ta e="T620" id="Seg_8152" s="T619">бить-EP-PST2.[3SG]</ta>
            <ta e="T622" id="Seg_8153" s="T620">потом</ta>
            <ta e="T625" id="Seg_8154" s="T622">милиция-PL.[NOM]</ta>
            <ta e="T628" id="Seg_8155" s="T625">приходить-PST2-3PL</ta>
            <ta e="T632" id="Seg_8156" s="T628">ага</ta>
            <ta e="T639" id="Seg_8157" s="T635">так</ta>
            <ta e="T655" id="Seg_8158" s="T653">начало.[NOM]</ta>
            <ta e="T657" id="Seg_8159" s="T655">есть</ta>
            <ta e="T659" id="Seg_8160" s="T657">одежда.[NOM]</ta>
            <ta e="T675" id="Seg_8161" s="T674">одежда.[NOM]</ta>
            <ta e="T700" id="Seg_8162" s="T699">милиция-PL.[NOM]</ta>
            <ta e="T701" id="Seg_8163" s="T700">бить-PRS-3PL</ta>
            <ta e="T703" id="Seg_8164" s="T701">и.так.далее-PRS-3PL</ta>
            <ta e="T707" id="Seg_8165" s="T706">потом</ta>
            <ta e="T712" id="Seg_8166" s="T710">так</ta>
            <ta e="T718" id="Seg_8167" s="T716">ах</ta>
            <ta e="T719" id="Seg_8168" s="T718">дом-3SG-DAT/LOC</ta>
            <ta e="T720" id="Seg_8169" s="T719">приходить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T724" id="Seg_8170" s="T722">а</ta>
            <ta e="T728" id="Seg_8171" s="T726">куда</ta>
            <ta e="T730" id="Seg_8172" s="T728">быть-PST1-3SG=Q</ta>
            <ta e="T731" id="Seg_8173" s="T730">ах</ta>
            <ta e="T732" id="Seg_8174" s="T731">ага</ta>
            <ta e="T734" id="Seg_8175" s="T733">выйти-EP-PST2.[3SG]</ta>
            <ta e="T738" id="Seg_8176" s="T737">так</ta>
            <ta e="T751" id="Seg_8177" s="T750">спиртное.[NOM]</ta>
            <ta e="T753" id="Seg_8178" s="T751">пить-NEG.PTCP</ta>
            <ta e="T754" id="Seg_8179" s="T753">становиться-PST2.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-UoPP">
            <ta e="T26" id="Seg_8180" s="T21">interj</ta>
            <ta e="T68" id="Seg_8181" s="T66">dempro.[pro:case]</ta>
            <ta e="T71" id="Seg_8182" s="T68">adv</ta>
            <ta e="T90" id="Seg_8183" s="T89">v-v:tense.[v:pred.pn]</ta>
            <ta e="T93" id="Seg_8184" s="T91">conj</ta>
            <ta e="T95" id="Seg_8185" s="T93">adv</ta>
            <ta e="T115" id="Seg_8186" s="T113">n-n:(poss).[n:case]</ta>
            <ta e="T126" id="Seg_8187" s="T124">ptcl</ta>
            <ta e="T128" id="Seg_8188" s="T126">dempro</ta>
            <ta e="T129" id="Seg_8189" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_8190" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_8191" s="T130">v-v:tense.[v:pred.pn]</ta>
            <ta e="T132" id="Seg_8192" s="T131">n.[n:case]</ta>
            <ta e="T139" id="Seg_8193" s="T138">n-n&gt;v-v:cvb</ta>
            <ta e="T140" id="Seg_8194" s="T139">v-v:tense-v:pred.pn</ta>
            <ta e="T141" id="Seg_8195" s="T140">dempro.[pro:case]</ta>
            <ta e="T143" id="Seg_8196" s="T142">interj</ta>
            <ta e="T144" id="Seg_8197" s="T143">dempro</ta>
            <ta e="T145" id="Seg_8198" s="T144">v-v:cvb</ta>
            <ta e="T146" id="Seg_8199" s="T145">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T147" id="Seg_8200" s="T146">ptcl</ta>
            <ta e="T149" id="Seg_8201" s="T148">n.[n:case]</ta>
            <ta e="T150" id="Seg_8202" s="T149">n.[n:case]</ta>
            <ta e="T151" id="Seg_8203" s="T150">v-v:cvb</ta>
            <ta e="T152" id="Seg_8204" s="T151">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T159" id="Seg_8205" s="T157">ptcl</ta>
            <ta e="T162" id="Seg_8206" s="T159">ptcl</ta>
            <ta e="T164" id="Seg_8207" s="T162">n-n:(poss).[n:case]</ta>
            <ta e="T165" id="Seg_8208" s="T164">adj.[n:case]</ta>
            <ta e="T166" id="Seg_8209" s="T165">adv</ta>
            <ta e="T170" id="Seg_8210" s="T168">interj</ta>
            <ta e="T172" id="Seg_8211" s="T171">adj.[n:case]</ta>
            <ta e="T173" id="Seg_8212" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_8213" s="T173">v-v:tense.[v:pred.pn]</ta>
            <ta e="T183" id="Seg_8214" s="T181">adv</ta>
            <ta e="T185" id="Seg_8215" s="T183">v-v:cvb</ta>
            <ta e="T187" id="Seg_8216" s="T185">v-v:tense.[v:pred.pn]</ta>
            <ta e="T197" id="Seg_8217" s="T195">interj</ta>
            <ta e="T199" id="Seg_8218" s="T197">n-n:(poss).[n:case]</ta>
            <ta e="T201" id="Seg_8219" s="T199">dempro.[pro:case]</ta>
            <ta e="T211" id="Seg_8220" s="T209">ptcl</ta>
            <ta e="T223" id="Seg_8221" s="T220">v-v:cvb</ta>
            <ta e="T225" id="Seg_8222" s="T223">post</ta>
            <ta e="T228" id="Seg_8223" s="T225">v-v:cvb</ta>
            <ta e="T230" id="Seg_8224" s="T228">v-v:tense.[v:pred.pn]</ta>
            <ta e="T240" id="Seg_8225" s="T238">que=ptcl</ta>
            <ta e="T241" id="Seg_8226" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_8227" s="T241">adj-n:(poss).[n:case]</ta>
            <ta e="T244" id="Seg_8228" s="T243">conj</ta>
            <ta e="T245" id="Seg_8229" s="T244">que</ta>
            <ta e="T246" id="Seg_8230" s="T245">ptcl=ptcl</ta>
            <ta e="T249" id="Seg_8231" s="T246">adv</ta>
            <ta e="T252" id="Seg_8232" s="T249">v-v:tense-v:pred.pn</ta>
            <ta e="T255" id="Seg_8233" s="T252">adj</ta>
            <ta e="T257" id="Seg_8234" s="T255">v</ta>
            <ta e="T269" id="Seg_8235" s="T264">ptcl</ta>
            <ta e="T275" id="Seg_8236" s="T271">v-v:cvb</ta>
            <ta e="T277" id="Seg_8237" s="T275">v-v:tense.[v:pred.pn]</ta>
            <ta e="T281" id="Seg_8238" s="T277">dempro</ta>
            <ta e="T287" id="Seg_8239" s="T283">ptcl</ta>
            <ta e="T288" id="Seg_8240" s="T287">ptcl</ta>
            <ta e="T290" id="Seg_8241" s="T288">interj</ta>
            <ta e="T292" id="Seg_8242" s="T290">interj</ta>
            <ta e="T293" id="Seg_8243" s="T292">v-v:tense-v:poss.pn</ta>
            <ta e="T304" id="Seg_8244" s="T303">n-n:(poss).[n:case]</ta>
            <ta e="T305" id="Seg_8245" s="T304">adj.[n:case]</ta>
            <ta e="T306" id="Seg_8246" s="T305">adv</ta>
            <ta e="T308" id="Seg_8247" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_8248" s="T308">dempro</ta>
            <ta e="T310" id="Seg_8249" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_8250" s="T310">v-v:tense.[v:pred.pn]</ta>
            <ta e="T322" id="Seg_8251" s="T321">dempro</ta>
            <ta e="T323" id="Seg_8252" s="T322">v-v:cvb</ta>
            <ta e="T330" id="Seg_8253" s="T327">n-n:poss-n:case</ta>
            <ta e="T334" id="Seg_8254" s="T330">v-v:ptcp</ta>
            <ta e="T337" id="Seg_8255" s="T334">v-v:tense-v:poss.pn</ta>
            <ta e="T357" id="Seg_8256" s="T355">ptcl</ta>
            <ta e="T359" id="Seg_8257" s="T357">adv</ta>
            <ta e="T361" id="Seg_8258" s="T359">v-v:tense.[v:pred.pn]</ta>
            <ta e="T363" id="Seg_8259" s="T361">ptcl</ta>
            <ta e="T365" id="Seg_8260" s="T363">ptcl</ta>
            <ta e="T374" id="Seg_8261" s="T373">interj</ta>
            <ta e="T383" id="Seg_8262" s="T382">interj</ta>
            <ta e="T396" id="Seg_8263" s="T394">ptcl</ta>
            <ta e="T403" id="Seg_8264" s="T400">n-n:(num).[n:case]</ta>
            <ta e="T406" id="Seg_8265" s="T403">v-v:cvb</ta>
            <ta e="T410" id="Seg_8266" s="T406">v-v:tense-v:pred.pn</ta>
            <ta e="T413" id="Seg_8267" s="T412">n-n:(num).[n:case]</ta>
            <ta e="T414" id="Seg_8268" s="T413">v-v:cvb</ta>
            <ta e="T415" id="Seg_8269" s="T414">v-v:tense-v:pred.pn</ta>
            <ta e="T416" id="Seg_8270" s="T415">adv</ta>
            <ta e="T417" id="Seg_8271" s="T416">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T418" id="Seg_8272" s="T417">v-v:(ins)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T420" id="Seg_8273" s="T419">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T421" id="Seg_8274" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_8275" s="T421">adv</ta>
            <ta e="T423" id="Seg_8276" s="T422">que</ta>
            <ta e="T424" id="Seg_8277" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_8278" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_8279" s="T425">ptcl</ta>
            <ta e="T428" id="Seg_8280" s="T426">adv</ta>
            <ta e="T444" id="Seg_8281" s="T440">interj</ta>
            <ta e="T452" id="Seg_8282" s="T451">ptcl</ta>
            <ta e="T456" id="Seg_8283" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_8284" s="T456">dempro</ta>
            <ta e="T458" id="Seg_8285" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_8286" s="T458">n-n:(poss).[n:case]</ta>
            <ta e="T460" id="Seg_8287" s="T459">adj.[n:case]</ta>
            <ta e="T462" id="Seg_8288" s="T460">v-v:mood-v:temp.pn</ta>
            <ta e="T465" id="Seg_8289" s="T463">adj.[n:case]</ta>
            <ta e="T467" id="Seg_8290" s="T465">ptcl-ptcl&gt;adv</ta>
            <ta e="T470" id="Seg_8291" s="T467">v-v:ptcp</ta>
            <ta e="T472" id="Seg_8292" s="T470">v-v:tense-v:pred.pn</ta>
            <ta e="T474" id="Seg_8293" s="T472">ptcl</ta>
            <ta e="T476" id="Seg_8294" s="T474">interj</ta>
            <ta e="T480" id="Seg_8295" s="T479">v-v:ptcp</ta>
            <ta e="T481" id="Seg_8296" s="T480">v-v:tense-v:pred.pn</ta>
            <ta e="T482" id="Seg_8297" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_8298" s="T482">ptcl</ta>
            <ta e="T492" id="Seg_8299" s="T490">interj</ta>
            <ta e="T493" id="Seg_8300" s="T492">v-v:cvb</ta>
            <ta e="T495" id="Seg_8301" s="T493">post</ta>
            <ta e="T497" id="Seg_8302" s="T495">ptcl</ta>
            <ta e="T504" id="Seg_8303" s="T500">interj</ta>
            <ta e="T556" id="Seg_8304" s="T555">ptcl</ta>
            <ta e="T557" id="Seg_8305" s="T556">adv</ta>
            <ta e="T558" id="Seg_8306" s="T557">n.[n:case]</ta>
            <ta e="T560" id="Seg_8307" s="T558">que</ta>
            <ta e="T561" id="Seg_8308" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_8309" s="T561">n-n&gt;n.[n:case]</ta>
            <ta e="T563" id="Seg_8310" s="T562">v-v:tense-v:poss.pn</ta>
            <ta e="T571" id="Seg_8311" s="T569">ptcl</ta>
            <ta e="T573" id="Seg_8312" s="T571">dempro</ta>
            <ta e="T575" id="Seg_8313" s="T573">v-v:tense.[v:pred.pn]</ta>
            <ta e="T599" id="Seg_8314" s="T598">n-n:case</ta>
            <ta e="T600" id="Seg_8315" s="T599">v-v:tense.[v:pred.pn]</ta>
            <ta e="T603" id="Seg_8316" s="T602">v.[v:mood.pn]</ta>
            <ta e="T605" id="Seg_8317" s="T603">dempro</ta>
            <ta e="T606" id="Seg_8318" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_8319" s="T606">post</ta>
            <ta e="T609" id="Seg_8320" s="T607">adv</ta>
            <ta e="T611" id="Seg_8321" s="T609">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T614" id="Seg_8322" s="T611">v-v:tense.[v:pred.pn]</ta>
            <ta e="T619" id="Seg_8323" s="T617">adv</ta>
            <ta e="T620" id="Seg_8324" s="T619">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T622" id="Seg_8325" s="T620">adv</ta>
            <ta e="T625" id="Seg_8326" s="T622">n-n:(num).[n:case]</ta>
            <ta e="T628" id="Seg_8327" s="T625">v-v:tense-v:pred.pn</ta>
            <ta e="T632" id="Seg_8328" s="T628">interj</ta>
            <ta e="T639" id="Seg_8329" s="T635">ptcl</ta>
            <ta e="T655" id="Seg_8330" s="T653">n.[n:case]</ta>
            <ta e="T657" id="Seg_8331" s="T655">ptcl</ta>
            <ta e="T659" id="Seg_8332" s="T657">n.[n:case]</ta>
            <ta e="T675" id="Seg_8333" s="T674">n.[n:case]</ta>
            <ta e="T700" id="Seg_8334" s="T699">n-n:(num).[n:case]</ta>
            <ta e="T701" id="Seg_8335" s="T700">v-v:tense-v:pred.pn</ta>
            <ta e="T703" id="Seg_8336" s="T701">v-v:tense-v:pred.pn</ta>
            <ta e="T707" id="Seg_8337" s="T706">adv</ta>
            <ta e="T712" id="Seg_8338" s="T710">ptcl</ta>
            <ta e="T718" id="Seg_8339" s="T716">interj</ta>
            <ta e="T719" id="Seg_8340" s="T718">n-n:poss-n:case</ta>
            <ta e="T720" id="Seg_8341" s="T719">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T724" id="Seg_8342" s="T722">conj</ta>
            <ta e="T728" id="Seg_8343" s="T726">que</ta>
            <ta e="T730" id="Seg_8344" s="T728">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T731" id="Seg_8345" s="T730">interj</ta>
            <ta e="T732" id="Seg_8346" s="T731">interj</ta>
            <ta e="T734" id="Seg_8347" s="T733">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T738" id="Seg_8348" s="T737">ptcl</ta>
            <ta e="T751" id="Seg_8349" s="T750">n.[n:case]</ta>
            <ta e="T753" id="Seg_8350" s="T751">v-v:ptcp</ta>
            <ta e="T754" id="Seg_8351" s="T753">v-v:tense.[v:pred.pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-UoPP">
            <ta e="T26" id="Seg_8352" s="T21">interj</ta>
            <ta e="T68" id="Seg_8353" s="T66">dempro</ta>
            <ta e="T71" id="Seg_8354" s="T68">adv</ta>
            <ta e="T90" id="Seg_8355" s="T89">v</ta>
            <ta e="T93" id="Seg_8356" s="T91">conj</ta>
            <ta e="T95" id="Seg_8357" s="T93">adv</ta>
            <ta e="T115" id="Seg_8358" s="T113">n</ta>
            <ta e="T126" id="Seg_8359" s="T124">ptcl</ta>
            <ta e="T128" id="Seg_8360" s="T126">dempro</ta>
            <ta e="T129" id="Seg_8361" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_8362" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_8363" s="T130">cop</ta>
            <ta e="T132" id="Seg_8364" s="T131">n</ta>
            <ta e="T139" id="Seg_8365" s="T138">v</ta>
            <ta e="T140" id="Seg_8366" s="T139">v</ta>
            <ta e="T141" id="Seg_8367" s="T140">dempro</ta>
            <ta e="T143" id="Seg_8368" s="T142">interj</ta>
            <ta e="T144" id="Seg_8369" s="T143">dempro</ta>
            <ta e="T145" id="Seg_8370" s="T144">v</ta>
            <ta e="T146" id="Seg_8371" s="T145">v</ta>
            <ta e="T147" id="Seg_8372" s="T146">ptcl</ta>
            <ta e="T149" id="Seg_8373" s="T148">n</ta>
            <ta e="T150" id="Seg_8374" s="T149">n</ta>
            <ta e="T151" id="Seg_8375" s="T150">v</ta>
            <ta e="T152" id="Seg_8376" s="T151">v</ta>
            <ta e="T159" id="Seg_8377" s="T157">ptcl</ta>
            <ta e="T162" id="Seg_8378" s="T159">ptcl</ta>
            <ta e="T164" id="Seg_8379" s="T162">n</ta>
            <ta e="T165" id="Seg_8380" s="T164">adj</ta>
            <ta e="T166" id="Seg_8381" s="T165">adv</ta>
            <ta e="T170" id="Seg_8382" s="T168">interj</ta>
            <ta e="T172" id="Seg_8383" s="T171">adj</ta>
            <ta e="T173" id="Seg_8384" s="T172">n</ta>
            <ta e="T174" id="Seg_8385" s="T173">v</ta>
            <ta e="T183" id="Seg_8386" s="T181">adv</ta>
            <ta e="T185" id="Seg_8387" s="T183">v</ta>
            <ta e="T187" id="Seg_8388" s="T185">v</ta>
            <ta e="T197" id="Seg_8389" s="T195">interj</ta>
            <ta e="T199" id="Seg_8390" s="T197">n</ta>
            <ta e="T201" id="Seg_8391" s="T199">dempro</ta>
            <ta e="T211" id="Seg_8392" s="T209">ptcl</ta>
            <ta e="T223" id="Seg_8393" s="T220">v</ta>
            <ta e="T225" id="Seg_8394" s="T223">post</ta>
            <ta e="T228" id="Seg_8395" s="T225">v</ta>
            <ta e="T230" id="Seg_8396" s="T228">aux</ta>
            <ta e="T240" id="Seg_8397" s="T238">que</ta>
            <ta e="T241" id="Seg_8398" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_8399" s="T241">n</ta>
            <ta e="T244" id="Seg_8400" s="T243">conj</ta>
            <ta e="T245" id="Seg_8401" s="T244">que</ta>
            <ta e="T246" id="Seg_8402" s="T245">ptcl</ta>
            <ta e="T249" id="Seg_8403" s="T246">adv</ta>
            <ta e="T252" id="Seg_8404" s="T249">v</ta>
            <ta e="T255" id="Seg_8405" s="T252">adj</ta>
            <ta e="T257" id="Seg_8406" s="T255">v</ta>
            <ta e="T269" id="Seg_8407" s="T264">ptcl</ta>
            <ta e="T275" id="Seg_8408" s="T271">v</ta>
            <ta e="T277" id="Seg_8409" s="T275">v</ta>
            <ta e="T281" id="Seg_8410" s="T277">dempro</ta>
            <ta e="T287" id="Seg_8411" s="T283">ptcl</ta>
            <ta e="T288" id="Seg_8412" s="T287">ptcl</ta>
            <ta e="T290" id="Seg_8413" s="T288">interj</ta>
            <ta e="T292" id="Seg_8414" s="T290">interj</ta>
            <ta e="T293" id="Seg_8415" s="T292">v</ta>
            <ta e="T304" id="Seg_8416" s="T303">n</ta>
            <ta e="T305" id="Seg_8417" s="T304">adj</ta>
            <ta e="T306" id="Seg_8418" s="T305">adv</ta>
            <ta e="T308" id="Seg_8419" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_8420" s="T308">dempro</ta>
            <ta e="T310" id="Seg_8421" s="T309">n</ta>
            <ta e="T311" id="Seg_8422" s="T310">v</ta>
            <ta e="T322" id="Seg_8423" s="T321">dempro</ta>
            <ta e="T323" id="Seg_8424" s="T322">v</ta>
            <ta e="T330" id="Seg_8425" s="T327">n</ta>
            <ta e="T334" id="Seg_8426" s="T330">v</ta>
            <ta e="T337" id="Seg_8427" s="T334">aux</ta>
            <ta e="T357" id="Seg_8428" s="T355">ptcl</ta>
            <ta e="T359" id="Seg_8429" s="T357">adv</ta>
            <ta e="T361" id="Seg_8430" s="T359">v</ta>
            <ta e="T363" id="Seg_8431" s="T361">ptcl</ta>
            <ta e="T365" id="Seg_8432" s="T363">ptcl</ta>
            <ta e="T374" id="Seg_8433" s="T373">interj</ta>
            <ta e="T383" id="Seg_8434" s="T382">interj</ta>
            <ta e="T396" id="Seg_8435" s="T394">ptcl</ta>
            <ta e="T403" id="Seg_8436" s="T400">n</ta>
            <ta e="T406" id="Seg_8437" s="T403">v</ta>
            <ta e="T410" id="Seg_8438" s="T406">v</ta>
            <ta e="T413" id="Seg_8439" s="T412">n</ta>
            <ta e="T414" id="Seg_8440" s="T413">v</ta>
            <ta e="T415" id="Seg_8441" s="T414">v</ta>
            <ta e="T416" id="Seg_8442" s="T415">adv</ta>
            <ta e="T417" id="Seg_8443" s="T416">dempro</ta>
            <ta e="T418" id="Seg_8444" s="T417">v</ta>
            <ta e="T420" id="Seg_8445" s="T419">v</ta>
            <ta e="T421" id="Seg_8446" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_8447" s="T421">adv</ta>
            <ta e="T423" id="Seg_8448" s="T422">que</ta>
            <ta e="T424" id="Seg_8449" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_8450" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_8451" s="T425">ptcl</ta>
            <ta e="T428" id="Seg_8452" s="T426">adv</ta>
            <ta e="T444" id="Seg_8453" s="T440">interj</ta>
            <ta e="T452" id="Seg_8454" s="T451">ptcl</ta>
            <ta e="T456" id="Seg_8455" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_8456" s="T456">dempro</ta>
            <ta e="T458" id="Seg_8457" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_8458" s="T458">n</ta>
            <ta e="T460" id="Seg_8459" s="T459">adj</ta>
            <ta e="T462" id="Seg_8460" s="T460">cop</ta>
            <ta e="T465" id="Seg_8461" s="T463">adj</ta>
            <ta e="T467" id="Seg_8462" s="T465">adv</ta>
            <ta e="T470" id="Seg_8463" s="T467">v</ta>
            <ta e="T472" id="Seg_8464" s="T470">aux</ta>
            <ta e="T474" id="Seg_8465" s="T472">ptcl</ta>
            <ta e="T476" id="Seg_8466" s="T474">interj</ta>
            <ta e="T480" id="Seg_8467" s="T479">v</ta>
            <ta e="T481" id="Seg_8468" s="T480">aux</ta>
            <ta e="T482" id="Seg_8469" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_8470" s="T482">ptcl</ta>
            <ta e="T492" id="Seg_8471" s="T490">interj</ta>
            <ta e="T493" id="Seg_8472" s="T492">v</ta>
            <ta e="T495" id="Seg_8473" s="T493">post</ta>
            <ta e="T497" id="Seg_8474" s="T495">ptcl</ta>
            <ta e="T504" id="Seg_8475" s="T500">interj</ta>
            <ta e="T556" id="Seg_8476" s="T555">ptcl</ta>
            <ta e="T557" id="Seg_8477" s="T556">adv</ta>
            <ta e="T558" id="Seg_8478" s="T557">n</ta>
            <ta e="T560" id="Seg_8479" s="T558">que</ta>
            <ta e="T561" id="Seg_8480" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_8481" s="T561">n</ta>
            <ta e="T563" id="Seg_8482" s="T562">cop</ta>
            <ta e="T571" id="Seg_8483" s="T569">ptcl</ta>
            <ta e="T573" id="Seg_8484" s="T571">dempro</ta>
            <ta e="T575" id="Seg_8485" s="T573">v</ta>
            <ta e="T599" id="Seg_8486" s="T598">n</ta>
            <ta e="T600" id="Seg_8487" s="T599">v</ta>
            <ta e="T603" id="Seg_8488" s="T602">v</ta>
            <ta e="T605" id="Seg_8489" s="T603">dempro</ta>
            <ta e="T606" id="Seg_8490" s="T605">n</ta>
            <ta e="T607" id="Seg_8491" s="T606">post</ta>
            <ta e="T609" id="Seg_8492" s="T607">adv</ta>
            <ta e="T611" id="Seg_8493" s="T609">v</ta>
            <ta e="T614" id="Seg_8494" s="T611">v</ta>
            <ta e="T619" id="Seg_8495" s="T617">adv</ta>
            <ta e="T620" id="Seg_8496" s="T619">v</ta>
            <ta e="T622" id="Seg_8497" s="T620">adv</ta>
            <ta e="T625" id="Seg_8498" s="T622">n</ta>
            <ta e="T628" id="Seg_8499" s="T625">v</ta>
            <ta e="T632" id="Seg_8500" s="T628">interj</ta>
            <ta e="T639" id="Seg_8501" s="T635">ptcl</ta>
            <ta e="T655" id="Seg_8502" s="T653">n</ta>
            <ta e="T657" id="Seg_8503" s="T655">ptcl</ta>
            <ta e="T659" id="Seg_8504" s="T657">n</ta>
            <ta e="T675" id="Seg_8505" s="T674">n</ta>
            <ta e="T700" id="Seg_8506" s="T699">n</ta>
            <ta e="T701" id="Seg_8507" s="T700">v</ta>
            <ta e="T703" id="Seg_8508" s="T701">v</ta>
            <ta e="T707" id="Seg_8509" s="T706">adv</ta>
            <ta e="T712" id="Seg_8510" s="T710">ptcl</ta>
            <ta e="T718" id="Seg_8511" s="T716">interj</ta>
            <ta e="T719" id="Seg_8512" s="T718">n</ta>
            <ta e="T720" id="Seg_8513" s="T719">v</ta>
            <ta e="T724" id="Seg_8514" s="T722">conj</ta>
            <ta e="T728" id="Seg_8515" s="T726">que</ta>
            <ta e="T730" id="Seg_8516" s="T728">v</ta>
            <ta e="T731" id="Seg_8517" s="T730">interj</ta>
            <ta e="T732" id="Seg_8518" s="T731">interj</ta>
            <ta e="T734" id="Seg_8519" s="T733">v</ta>
            <ta e="T738" id="Seg_8520" s="T737">ptcl</ta>
            <ta e="T751" id="Seg_8521" s="T750">n</ta>
            <ta e="T753" id="Seg_8522" s="T751">v</ta>
            <ta e="T754" id="Seg_8523" s="T753">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-UoPP">
            <ta e="T90" id="Seg_8524" s="T89">0.3.h:Th</ta>
            <ta e="T95" id="Seg_8525" s="T93">adv:L</ta>
            <ta e="T132" id="Seg_8526" s="T131">np:Th</ta>
            <ta e="T140" id="Seg_8527" s="T139">0.3.h:Th</ta>
            <ta e="T146" id="Seg_8528" s="T145">0.3.h:A</ta>
            <ta e="T152" id="Seg_8529" s="T151">0.3.h:A</ta>
            <ta e="T164" id="Seg_8530" s="T162">np.h:Th</ta>
            <ta e="T173" id="Seg_8531" s="T172">np:L</ta>
            <ta e="T174" id="Seg_8532" s="T173">0.3.h:A</ta>
            <ta e="T183" id="Seg_8533" s="T181">adv:Time</ta>
            <ta e="T187" id="Seg_8534" s="T185">0.3.h:A</ta>
            <ta e="T230" id="Seg_8535" s="T225">0.3.h:A</ta>
            <ta e="T242" id="Seg_8536" s="T241">np:Th</ta>
            <ta e="T277" id="Seg_8537" s="T275">0.3.h:A</ta>
            <ta e="T293" id="Seg_8538" s="T292">0.1.h:A</ta>
            <ta e="T304" id="Seg_8539" s="T303">np.h:Th</ta>
            <ta e="T306" id="Seg_8540" s="T305">adv:L</ta>
            <ta e="T310" id="Seg_8541" s="T309">np:L</ta>
            <ta e="T311" id="Seg_8542" s="T310">0.3.h:Th</ta>
            <ta e="T330" id="Seg_8543" s="T327">0.3.h:Poss np.h:P</ta>
            <ta e="T337" id="Seg_8544" s="T330">0.3.h:A</ta>
            <ta e="T361" id="Seg_8545" s="T359">0.3.h:A</ta>
            <ta e="T403" id="Seg_8546" s="T400">np.h:A</ta>
            <ta e="T413" id="Seg_8547" s="T412">np.h:A</ta>
            <ta e="T417" id="Seg_8548" s="T416">pro.h:P</ta>
            <ta e="T418" id="Seg_8549" s="T417">0.3.h:A</ta>
            <ta e="T459" id="Seg_8550" s="T458">np.h:Th</ta>
            <ta e="T472" id="Seg_8551" s="T467">0.3.h:A</ta>
            <ta e="T481" id="Seg_8552" s="T479">0.3.h:A</ta>
            <ta e="T563" id="Seg_8553" s="T562">0.3.h:Th</ta>
            <ta e="T575" id="Seg_8554" s="T573">0.3.h:A</ta>
            <ta e="T599" id="Seg_8555" s="T598">np:G</ta>
            <ta e="T600" id="Seg_8556" s="T599">0.3.h:A</ta>
            <ta e="T607" id="Seg_8557" s="T605">pp:Com</ta>
            <ta e="T611" id="Seg_8558" s="T609">0.2.h:A</ta>
            <ta e="T614" id="Seg_8559" s="T611">0.3.h:A</ta>
            <ta e="T620" id="Seg_8560" s="T619">0.3.h:A</ta>
            <ta e="T625" id="Seg_8561" s="T622">np.h:A</ta>
            <ta e="T700" id="Seg_8562" s="T699">np.h:A</ta>
            <ta e="T730" id="Seg_8563" s="T728">0.3:Th</ta>
            <ta e="T734" id="Seg_8564" s="T733">0.3.h:A</ta>
            <ta e="T751" id="Seg_8565" s="T750">np:P</ta>
            <ta e="T754" id="Seg_8566" s="T751">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-UoPP">
            <ta e="T90" id="Seg_8567" s="T89">0.3.h:S v:pred</ta>
            <ta e="T130" id="Seg_8568" s="T129">ptcl:pred</ta>
            <ta e="T132" id="Seg_8569" s="T131">np:S</ta>
            <ta e="T139" id="Seg_8570" s="T138">s:adv</ta>
            <ta e="T140" id="Seg_8571" s="T139">0.3.h:S v:pred</ta>
            <ta e="T145" id="Seg_8572" s="T144">s:purp</ta>
            <ta e="T146" id="Seg_8573" s="T145">0.3.h:S v:pred</ta>
            <ta e="T151" id="Seg_8574" s="T149">s:purp</ta>
            <ta e="T152" id="Seg_8575" s="T151">0.3.h:S v:pred</ta>
            <ta e="T164" id="Seg_8576" s="T162">np.h:S</ta>
            <ta e="T165" id="Seg_8577" s="T164">adj:pred</ta>
            <ta e="T174" id="Seg_8578" s="T173">0.3.h:S v:pred</ta>
            <ta e="T187" id="Seg_8579" s="T185">0.3.h:S v:pred</ta>
            <ta e="T225" id="Seg_8580" s="T220">s:temp</ta>
            <ta e="T230" id="Seg_8581" s="T225">0.3.h:S v:pred</ta>
            <ta e="T240" id="Seg_8582" s="T238">pro:pred</ta>
            <ta e="T242" id="Seg_8583" s="T241">np:S</ta>
            <ta e="T277" id="Seg_8584" s="T275">0.3.h:S v:pred</ta>
            <ta e="T293" id="Seg_8585" s="T292">0.1.h:S v:pred</ta>
            <ta e="T304" id="Seg_8586" s="T303">np.h:S</ta>
            <ta e="T305" id="Seg_8587" s="T304">adj:pred</ta>
            <ta e="T311" id="Seg_8588" s="T310">0.3.h:S v:pred</ta>
            <ta e="T330" id="Seg_8589" s="T327">np.h:O</ta>
            <ta e="T337" id="Seg_8590" s="T330">0.3.h:S v:pred</ta>
            <ta e="T361" id="Seg_8591" s="T359">0.3.h:S v:pred</ta>
            <ta e="T403" id="Seg_8592" s="T400">np.h:S</ta>
            <ta e="T406" id="Seg_8593" s="T403">s:temp</ta>
            <ta e="T410" id="Seg_8594" s="T406">v:pred</ta>
            <ta e="T413" id="Seg_8595" s="T412">np.h:S</ta>
            <ta e="T414" id="Seg_8596" s="T413">s:temp</ta>
            <ta e="T415" id="Seg_8597" s="T414">v:pred</ta>
            <ta e="T418" id="Seg_8598" s="T416">s:temp</ta>
            <ta e="T462" id="Seg_8599" s="T458">s:temp</ta>
            <ta e="T472" id="Seg_8600" s="T467">0.3.h:S v:pred</ta>
            <ta e="T481" id="Seg_8601" s="T479">0.3.h:S v:pred</ta>
            <ta e="T497" id="Seg_8602" s="T492">s:temp</ta>
            <ta e="T562" id="Seg_8603" s="T561">n:pred</ta>
            <ta e="T563" id="Seg_8604" s="T562">0.3.h:S cop</ta>
            <ta e="T575" id="Seg_8605" s="T573">0.3.h:S v:pred</ta>
            <ta e="T600" id="Seg_8606" s="T599">0.3.h:S v:pred</ta>
            <ta e="T611" id="Seg_8607" s="T609">0.2.h:S v:pred</ta>
            <ta e="T614" id="Seg_8608" s="T611">0.3.h:S v:pred</ta>
            <ta e="T620" id="Seg_8609" s="T619">0.3.h:S v:pred</ta>
            <ta e="T625" id="Seg_8610" s="T622">np.h:S</ta>
            <ta e="T628" id="Seg_8611" s="T625">v:pred</ta>
            <ta e="T657" id="Seg_8612" s="T655">ptcl:pred</ta>
            <ta e="T700" id="Seg_8613" s="T699">np.h:S</ta>
            <ta e="T701" id="Seg_8614" s="T700">v:pred</ta>
            <ta e="T703" id="Seg_8615" s="T701">v:pred</ta>
            <ta e="T720" id="Seg_8616" s="T718">s:comp</ta>
            <ta e="T728" id="Seg_8617" s="T726">pro:pred</ta>
            <ta e="T730" id="Seg_8618" s="T728">0.3:S cop</ta>
            <ta e="T734" id="Seg_8619" s="T733">0.3.h:S v:pred</ta>
            <ta e="T751" id="Seg_8620" s="T750">np:O</ta>
            <ta e="T754" id="Seg_8621" s="T751">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-UoPP" />
         <annotation name="Top" tierref="Top-UoPP" />
         <annotation name="Foc" tierref="Foc-UoPP" />
         <annotation name="BOR" tierref="BOR-UoPP">
            <ta e="T71" id="Seg_8622" s="T68">RUS:mod</ta>
            <ta e="T93" id="Seg_8623" s="T91">RUS:gram</ta>
            <ta e="T126" id="Seg_8624" s="T124">RUS:disc</ta>
            <ta e="T132" id="Seg_8625" s="T131">RUS:core</ta>
            <ta e="T147" id="Seg_8626" s="T146">RUS:mod</ta>
            <ta e="T159" id="Seg_8627" s="T157">RUS:disc</ta>
            <ta e="T162" id="Seg_8628" s="T159">RUS:mod</ta>
            <ta e="T173" id="Seg_8629" s="T172">RUS:cult</ta>
            <ta e="T211" id="Seg_8630" s="T209">RUS:disc</ta>
            <ta e="T244" id="Seg_8631" s="T243">RUS:gram</ta>
            <ta e="T269" id="Seg_8632" s="T264">RUS:mod</ta>
            <ta e="T308" id="Seg_8633" s="T307">RUS:disc</ta>
            <ta e="T396" id="Seg_8634" s="T394">RUS:disc</ta>
            <ta e="T403" id="Seg_8635" s="T400">RUS:cult</ta>
            <ta e="T413" id="Seg_8636" s="T412">RUS:cult</ta>
            <ta e="T428" id="Seg_8637" s="T426">RUS:mod</ta>
            <ta e="T456" id="Seg_8638" s="T455">RUS:disc</ta>
            <ta e="T482" id="Seg_8639" s="T481">RUS:disc</ta>
            <ta e="T556" id="Seg_8640" s="T555">RUS:disc</ta>
            <ta e="T558" id="Seg_8641" s="T557">RUS:core</ta>
            <ta e="T625" id="Seg_8642" s="T622">RUS:cult</ta>
            <ta e="T639" id="Seg_8643" s="T635">RUS:disc</ta>
            <ta e="T655" id="Seg_8644" s="T653">RUS:core</ta>
            <ta e="T700" id="Seg_8645" s="T699">RUS:cult</ta>
            <ta e="T712" id="Seg_8646" s="T710">RUS:disc</ta>
            <ta e="T724" id="Seg_8647" s="T722">RUS:gram</ta>
            <ta e="T738" id="Seg_8648" s="T737">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-UoPP">
            <ta e="T173" id="Seg_8649" s="T172">Vsub Vsub</ta>
            <ta e="T403" id="Seg_8650" s="T400">lenition Vsub</ta>
            <ta e="T413" id="Seg_8651" s="T412">lenition Vsub</ta>
            <ta e="T625" id="Seg_8652" s="T622">lenition Vsub</ta>
            <ta e="T700" id="Seg_8653" s="T699">lenition Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-UoPP">
            <ta e="T71" id="Seg_8654" s="T68">dir:bare</ta>
            <ta e="T93" id="Seg_8655" s="T91">dir:bare</ta>
            <ta e="T126" id="Seg_8656" s="T124">dir:bare</ta>
            <ta e="T132" id="Seg_8657" s="T131">dir:bare</ta>
            <ta e="T147" id="Seg_8658" s="T146">dir:bare</ta>
            <ta e="T173" id="Seg_8659" s="T172">dir:infl</ta>
            <ta e="T244" id="Seg_8660" s="T243">dir:bare</ta>
            <ta e="T269" id="Seg_8661" s="T264">dir:bare</ta>
            <ta e="T308" id="Seg_8662" s="T307">dir:bare</ta>
            <ta e="T403" id="Seg_8663" s="T400">dir:infl</ta>
            <ta e="T413" id="Seg_8664" s="T412">dir:infl</ta>
            <ta e="T428" id="Seg_8665" s="T426">dir:bare</ta>
            <ta e="T456" id="Seg_8666" s="T455">dir:bare</ta>
            <ta e="T482" id="Seg_8667" s="T481">dir:bare</ta>
            <ta e="T556" id="Seg_8668" s="T555">dir:bare</ta>
            <ta e="T558" id="Seg_8669" s="T557">dir:bare</ta>
            <ta e="T625" id="Seg_8670" s="T622">dir:infl</ta>
            <ta e="T655" id="Seg_8671" s="T653">dir:bare</ta>
            <ta e="T700" id="Seg_8672" s="T699">dir:infl</ta>
            <ta e="T712" id="Seg_8673" s="T710">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-UoPP">
            <ta e="T55" id="Seg_8674" s="T38">RUS:ext</ta>
            <ta e="T86" id="Seg_8675" s="T78">RUS:int.ins</ta>
            <ta e="T103" id="Seg_8676" s="T97">RUS:ext</ta>
            <ta e="T123" id="Seg_8677" s="T115">RUS:int.ins</ta>
            <ta e="T162" id="Seg_8678" s="T157">RUS:int.alt</ta>
            <ta e="T195" id="Seg_8679" s="T187">RUS:int.ins</ta>
            <ta e="T208" id="Seg_8680" s="T202">RUS:ext</ta>
            <ta e="T211" id="Seg_8681" s="T209">RUS:ext</ta>
            <ta e="T300" id="Seg_8682" s="T294">RUS:ext</ta>
            <ta e="T303" id="Seg_8683" s="T301">RUS:int.ins</ta>
            <ta e="T325" id="Seg_8684" s="T324">RUS:int.ins</ta>
            <ta e="T392" id="Seg_8685" s="T384">RUS:ext</ta>
            <ta e="T396" id="Seg_8686" s="T394">RUS:ext</ta>
            <ta e="T598" id="Seg_8687" s="T594">RUS:int.ins</ta>
            <ta e="T643" id="Seg_8688" s="T641">RUS:ext</ta>
            <ta e="T652" id="Seg_8689" s="T644">RUS:ext</ta>
            <ta e="T668" id="Seg_8690" s="T661">RUS:ext</ta>
            <ta e="T721" id="Seg_8691" s="T720">RUS:int.ins</ta>
            <ta e="T724" id="Seg_8692" s="T722">RUS:ext</ta>
            <ta e="T738" id="Seg_8693" s="T737">RUS:ext</ta>
            <ta e="T760" id="Seg_8694" s="T755">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe-UoPP">
            <ta e="T26" id="Seg_8695" s="T21">– Ah.</ta>
            <ta e="T55" id="Seg_8696" s="T38">– Hence, consecutively, how it started.</ta>
            <ta e="T73" id="Seg_8697" s="T66">– This probably (…).</ta>
            <ta e="T90" id="Seg_8698" s="T78">– There he went into prison, (…) he is sitting.</ta>
            <ta e="T97" id="Seg_8699" s="T91">– And there (…).</ta>
            <ta e="T103" id="Seg_8700" s="T97">– Ah, and we have to [talk] in Dolgan.</ta>
            <ta e="T123" id="Seg_8701" s="T113">– Alcohol, so then, here.</ta>
            <ta e="T132" id="Seg_8702" s="T124">– So, that's probably the beginning.</ta>
            <ta e="T141" id="Seg_8703" s="T138">– They are sitting and drinking alcohol.</ta>
            <ta e="T147" id="Seg_8704" s="T142">Ah, here he went to work.</ta>
            <ta e="T152" id="Seg_8705" s="T148">Work, he went to make money.</ta>
            <ta e="T166" id="Seg_8706" s="T157">– So, here the child is small.</ta>
            <ta e="T170" id="Seg_8707" s="T168">– Aha.</ta>
            <ta e="T174" id="Seg_8708" s="T171">Small, he is in prison.</ta>
            <ta e="T201" id="Seg_8709" s="T181">– Later he is coming, that's the end, ah, that's the end.</ta>
            <ta e="T208" id="Seg_8710" s="T202">– And what's that?</ta>
            <ta e="T211" id="Seg_8711" s="T209">– So.</ta>
            <ta e="T230" id="Seg_8712" s="T220">– Having come he is telling.</ta>
            <ta e="T242" id="Seg_8713" s="T238">– Where is the recent one?</ta>
            <ta e="T257" id="Seg_8714" s="T243">And where is the one where they are just meeting.</ta>
            <ta e="T269" id="Seg_8715" s="T264">– We have to.</ta>
            <ta e="T281" id="Seg_8716" s="T271">– Here he goes to work.</ta>
            <ta e="T293" id="Seg_8717" s="T283">– No, no, ah, wait, we'll see.</ta>
            <ta e="T300" id="Seg_8718" s="T294">God, I wanted to say "heaven knows".</ta>
            <ta e="T306" id="Seg_8719" s="T301">So the child, our child is small there.</ta>
            <ta e="T311" id="Seg_8720" s="T307">So, here he is in prison.</ta>
            <ta e="T325" id="Seg_8721" s="T321">– Here out of jealousy, wait.</ta>
            <ta e="T337" id="Seg_8722" s="T327">– He thrashed his wife.</ta>
            <ta e="T365" id="Seg_8723" s="T355">– Yes, then he is thrashing, isn't he?</ta>
            <ta e="T374" id="Seg_8724" s="T373">– Aha.</ta>
            <ta e="T383" id="Seg_8725" s="T382">– Aha.</ta>
            <ta e="T392" id="Seg_8726" s="T384">Yes, here.</ta>
            <ta e="T396" id="Seg_8727" s="T394">– So.</ta>
            <ta e="T410" id="Seg_8728" s="T400">– The policemen have come and taken him.</ta>
            <ta e="T418" id="Seg_8729" s="T412">– The policemen have come and taken him, when he had beaten her.</ta>
            <ta e="T428" id="Seg_8730" s="T419">Besides that he doesn't thrash her anywhere.</ta>
            <ta e="T444" id="Seg_8731" s="T440">– Aha.</ta>
            <ta e="T452" id="Seg_8732" s="T451">– Yes.</ta>
            <ta e="T462" id="Seg_8733" s="T455">– So, here, when the child is small.</ta>
            <ta e="T476" id="Seg_8734" s="T463">– They were living very well, mhm.</ta>
            <ta e="T483" id="Seg_8735" s="T479">They are working like that.</ta>
            <ta e="T497" id="Seg_8736" s="T490">– Eh, after he has come?</ta>
            <ta e="T504" id="Seg_8737" s="T500">– Ah.</ta>
            <ta e="T563" id="Seg_8738" s="T555">– So, then the beginning, where did he become a drunkard?</ta>
            <ta e="T575" id="Seg_8739" s="T569">– Eh, there he is telling.</ta>
            <ta e="T600" id="Seg_8740" s="T594">– So, two, so, he went to the woman.</ta>
            <ta e="T614" id="Seg_8741" s="T602">– "Hey, he went like this with this man", he says.</ta>
            <ta e="T628" id="Seg_8742" s="T617">– Then he hit her, then the policemen came.</ta>
            <ta e="T632" id="Seg_8743" s="T628">– Aha.</ta>
            <ta e="T639" id="Seg_8744" s="T635">– So.</ta>
            <ta e="T643" id="Seg_8745" s="T641">– And so.</ta>
            <ta e="T652" id="Seg_8746" s="T644">And we have to look at these.</ta>
            <ta e="T659" id="Seg_8747" s="T653">– In the beginning clothes.</ta>
            <ta e="T668" id="Seg_8748" s="T661">– Ah, here also.</ta>
            <ta e="T675" id="Seg_8749" s="T674">– Clothes.</ta>
            <ta e="T703" id="Seg_8750" s="T698">– (…) the policemen are thrashing and so on.</ta>
            <ta e="T707" id="Seg_8751" s="T706">– Then.</ta>
            <ta e="T712" id="Seg_8752" s="T710">– So.</ta>
            <ta e="T721" id="Seg_8753" s="T716">– Ah, he is imagining how he'll come home.</ta>
            <ta e="T724" id="Seg_8754" s="T722">And…</ta>
            <ta e="T730" id="Seg_8755" s="T726">– Where is it?</ta>
            <ta e="T732" id="Seg_8756" s="T730">Ah, aha.</ta>
            <ta e="T734" id="Seg_8757" s="T733">He has come out.</ta>
            <ta e="T738" id="Seg_8758" s="T736">– (…) so.</ta>
            <ta e="T754" id="Seg_8759" s="T750">– He has stopped drinking alcohol.</ta>
            <ta e="T760" id="Seg_8760" s="T755">So, yes, right?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-UoPP">
            <ta e="T26" id="Seg_8761" s="T21">– Ah.</ta>
            <ta e="T55" id="Seg_8762" s="T38">– Also nacheinander, wie das angefangen hat.</ta>
            <ta e="T73" id="Seg_8763" s="T66">– Das wahrscheinlich (…).</ta>
            <ta e="T90" id="Seg_8764" s="T78">– Da ist er ins Gefängnis gekommen, (…) er sitzt.</ta>
            <ta e="T97" id="Seg_8765" s="T91">– Und dort (…).</ta>
            <ta e="T103" id="Seg_8766" s="T97">– Ah, auf Dolganisch müssen wir.</ta>
            <ta e="T123" id="Seg_8767" s="T113">– Schnaps, dann so, hierher.</ta>
            <ta e="T132" id="Seg_8768" s="T124">– So, hier ist dann wohl der Anfang.</ta>
            <ta e="T141" id="Seg_8769" s="T138">– Sie sitzen und trinken Schnaps.</ta>
            <ta e="T147" id="Seg_8770" s="T142">Ah, hier ist er arbeiten gegangen.</ta>
            <ta e="T152" id="Seg_8771" s="T148">Arbeit, er ist gegangen, um Geld zu machen.</ta>
            <ta e="T166" id="Seg_8772" s="T157">– So, hier ist das Kind klein.</ta>
            <ta e="T170" id="Seg_8773" s="T168">– Aha.</ta>
            <ta e="T174" id="Seg_8774" s="T171">Klein, er sitzt im Gefängnis.</ta>
            <ta e="T201" id="Seg_8775" s="T181">– Später kommt er, das ist dann das Ende, ah, das ist das Ende.</ta>
            <ta e="T208" id="Seg_8776" s="T202">– Und was ist das?</ta>
            <ta e="T211" id="Seg_8777" s="T209">– So.</ta>
            <ta e="T230" id="Seg_8778" s="T220">– Nachdem er gekommen ist, erzählt er.</ta>
            <ta e="T242" id="Seg_8779" s="T238">– Wo ist das von eben?</ta>
            <ta e="T257" id="Seg_8780" s="T243">Und wo ist das, wo sie sich gerade treffen.</ta>
            <ta e="T269" id="Seg_8781" s="T264">– Müssen wir.</ta>
            <ta e="T281" id="Seg_8782" s="T271">– Hier geht er arbeiten.</ta>
            <ta e="T293" id="Seg_8783" s="T283">– Nein, nein, ah, warte, wir schauen.</ta>
            <ta e="T300" id="Seg_8784" s="T294">Himmel, "weiß der Teufel" wollte ich sagen.</ta>
            <ta e="T306" id="Seg_8785" s="T301">So, das Kind, unser Kind ist hier klein.</ta>
            <ta e="T311" id="Seg_8786" s="T307">So, hier sitzt er im Gefängnis.</ta>
            <ta e="T325" id="Seg_8787" s="T321">– Hier aus Eifersucht, warte. </ta>
            <ta e="T337" id="Seg_8788" s="T327">– Er hat seine Frau verprügelt.</ta>
            <ta e="T365" id="Seg_8789" s="T355">– Ja, dann prügelt er, oder?</ta>
            <ta e="T374" id="Seg_8790" s="T373">– Aha.</ta>
            <ta e="T383" id="Seg_8791" s="T382">– Aha.</ta>
            <ta e="T392" id="Seg_8792" s="T384">Ja, hierhin.</ta>
            <ta e="T396" id="Seg_8793" s="T394">– So.</ta>
            <ta e="T410" id="Seg_8794" s="T400">– Die Polizisten sind gekommen und haben ihn mitgenommen.</ta>
            <ta e="T418" id="Seg_8795" s="T412">– Die Polizisten sind gekommen und haben ihn mitgenommen, als er sie geschlagen hat.</ta>
            <ta e="T428" id="Seg_8796" s="T419">Sonst schlägt er nirgendwo mehr. </ta>
            <ta e="T444" id="Seg_8797" s="T440">– Aha.</ta>
            <ta e="T452" id="Seg_8798" s="T451">– Ja.</ta>
            <ta e="T462" id="Seg_8799" s="T455">– So, hier, als das Kind klein ist.</ta>
            <ta e="T476" id="Seg_8800" s="T463">– Sie haben sehr gut gelebt, mhm.</ta>
            <ta e="T483" id="Seg_8801" s="T479">Sie arbeiten so.</ta>
            <ta e="T497" id="Seg_8802" s="T490">– Äh, nachdem er gekommen ist?</ta>
            <ta e="T504" id="Seg_8803" s="T500">– Ah.</ta>
            <ta e="T563" id="Seg_8804" s="T555">– So, dann der Anfang, wo ist er ein Trinker geworden?</ta>
            <ta e="T575" id="Seg_8805" s="T569">– Ah, da erzählt er.</ta>
            <ta e="T600" id="Seg_8806" s="T594">– So, zwei, so, er ging zur Frau.</ta>
            <ta e="T614" id="Seg_8807" s="T602">– "Hey, du bist so mit diesem Menschen gegangen", sagt er. </ta>
            <ta e="T628" id="Seg_8808" s="T617">– Dann hat er sie geschlagen, dann kamen die Polizisten.</ta>
            <ta e="T632" id="Seg_8809" s="T628">– Aha.</ta>
            <ta e="T639" id="Seg_8810" s="T635">– So.</ta>
            <ta e="T643" id="Seg_8811" s="T641">– Und so.</ta>
            <ta e="T652" id="Seg_8812" s="T644">Und diese müssen wir angucken.</ta>
            <ta e="T659" id="Seg_8813" s="T653">– Am Anfang Kleidung.</ta>
            <ta e="T668" id="Seg_8814" s="T661">– Ah, hier auch.</ta>
            <ta e="T675" id="Seg_8815" s="T674">– Kleidung.</ta>
            <ta e="T703" id="Seg_8816" s="T698">– (…) die Polizisten prügeln und so weiter.</ta>
            <ta e="T707" id="Seg_8817" s="T706">– Dann.</ta>
            <ta e="T712" id="Seg_8818" s="T710">– So.</ta>
            <ta e="T721" id="Seg_8819" s="T716">– Ah, er stellt sich vor, wie er nach Hause kommt.</ta>
            <ta e="T724" id="Seg_8820" s="T722">Und…</ta>
            <ta e="T730" id="Seg_8821" s="T726">– Wo ist das?</ta>
            <ta e="T732" id="Seg_8822" s="T730">Ah, ja.</ta>
            <ta e="T734" id="Seg_8823" s="T733">Er ist rausgekommen.</ta>
            <ta e="T738" id="Seg_8824" s="T736">– (…) so.</ta>
            <ta e="T754" id="Seg_8825" s="T750">– Er hat aufgehört, Schnaps zu trinken.</ta>
            <ta e="T760" id="Seg_8826" s="T755">So, ja, richtig?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-UoPP" />
         <annotation name="ltr" tierref="ltr-UoPP">
            <ta e="T90" id="Seg_8827" s="T78">Это в тюрьму он попал, ((…)) сидит.</ta>
            <ta e="T97" id="Seg_8828" s="T91">А там ((…))</ta>
            <ta e="T132" id="Seg_8829" s="T124">Так, вот оно начало.</ta>
            <ta e="T141" id="Seg_8830" s="T138">Пьют сидят.</ta>
            <ta e="T147" id="Seg_8831" s="T142">Аа, вот же он работать ходил.</ta>
            <ta e="T152" id="Seg_8832" s="T148">Правда деньги зарабатывал ходил.</ta>
            <ta e="T166" id="Seg_8833" s="T157">Аа что-то ребенок маленький здесь.</ta>
            <ta e="T174" id="Seg_8834" s="T171">Маленький… в тюрьме сидит.</ta>
            <ta e="T201" id="Seg_8835" s="T181">Потом как приехал, аа конец же это, конец это.</ta>
            <ta e="T230" id="Seg_8836" s="T220">После приезда рассказывает сидит.</ta>
            <ta e="T242" id="Seg_8837" s="T238">А где этот-то?</ta>
            <ta e="T257" id="Seg_8838" s="T243">Где это первый то где встречаются в начале…</ta>
            <ta e="T269" id="Seg_8839" s="T264">Надо.</ta>
            <ta e="T281" id="Seg_8840" s="T271">На работу едет он.</ta>
            <ta e="T293" id="Seg_8841" s="T283">Нет, нет, аа подожди, посмотрим.</ta>
            <ta e="T300" id="Seg_8842" s="T294">Тьорти… черт его знает хотела сказать.</ta>
            <ta e="T306" id="Seg_8843" s="T301">Так ребенок, ребенок маленький здесь.</ta>
            <ta e="T311" id="Seg_8844" s="T307">Так, он в тюрьме сидит.</ta>
            <ta e="T325" id="Seg_8845" s="T321">Это от ревности (жен-)… подожди.</ta>
            <ta e="T337" id="Seg_8846" s="T327">Жену бил.</ta>
            <ta e="T365" id="Seg_8847" s="T355">Аа, потом бьет, правда?</ta>
            <ta e="T410" id="Seg_8848" s="T400">Милиционеры пришли увели.</ta>
            <ta e="T418" id="Seg_8849" s="T412">Милиционеры пришли и увели его когда он ударил.</ta>
            <ta e="T428" id="Seg_8850" s="T419">Не бьет нигде больше.</ta>
            <ta e="T452" id="Seg_8851" s="T451">Да, да.</ta>
            <ta e="T462" id="Seg_8852" s="T455">Так, вот здесь когда ребенок маленький был.</ta>
            <ta e="T476" id="Seg_8853" s="T463">Очень хорошо жили кажется, мм.</ta>
            <ta e="T483" id="Seg_8854" s="T479">Работают так-то.</ta>
            <ta e="T497" id="Seg_8855" s="T490">После приезда?</ta>
            <ta e="T563" id="Seg_8856" s="T555">Потом начало где, где он стал пьяницей.</ta>
            <ta e="T575" id="Seg_8857" s="T569">Аа вот он рассказывает.</ta>
            <ta e="T600" id="Seg_8858" s="T594">Так, два, так да, пошел к жене.</ta>
            <ta e="T614" id="Seg_8859" s="T602">Ээ, вот с этим человеком так вот ходила говорит.</ta>
            <ta e="T628" id="Seg_8860" s="T617">Потом ударил, потом милиция пришла.</ta>
            <ta e="T659" id="Seg_8861" s="T653">В начале одежда.</ta>
            <ta e="T675" id="Seg_8862" s="T674">Одежда.</ta>
            <ta e="T703" id="Seg_8863" s="T698">((…)) милиционеры бьют и прочее.</ta>
            <ta e="T707" id="Seg_8864" s="T706">Потом.</ta>
            <ta e="T721" id="Seg_8865" s="T716">Как домой приходит представляет.</ta>
            <ta e="T730" id="Seg_8866" s="T726">Где это?</ta>
            <ta e="T734" id="Seg_8867" s="T733">Вышел.</ta>
            <ta e="T754" id="Seg_8868" s="T750">Стал непьющий.</ta>
            <ta e="T760" id="Seg_8869" s="T755">Все, так да, правильно?</ta>
         </annotation>
         <annotation name="nt" tierref="nt-UoPP" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-DCh"
                          name="ref"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-DCh"
                          name="st"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-DCh"
                          name="ts"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-DCh"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-DCh"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-DCh"
                          name="mb"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-DCh"
                          name="mp"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-DCh"
                          name="ge"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-DCh"
                          name="gg"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-DCh"
                          name="gr"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-DCh"
                          name="mc"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-DCh"
                          name="ps"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-DCh"
                          name="SeR"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-DCh"
                          name="SyF"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-DCh"
                          name="IST"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-DCh"
                          name="Top"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-DCh"
                          name="Foc"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-DCh"
                          name="BOR"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-DCh"
                          name="BOR-Phon"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-DCh"
                          name="BOR-Morph"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-DCh"
                          name="CS"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-DCh"
                          name="fe"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-DCh"
                          name="fg"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-DCh"
                          name="fr"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-DCh"
                          name="ltr"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-DCh"
                          name="nt"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-ChGS"
                          name="ref"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-ChGS"
                          name="st"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-ChGS"
                          name="ts"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-ChGS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-ChGS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-ChGS"
                          name="mb"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-ChGS"
                          name="mp"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-ChGS"
                          name="ge"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-ChGS"
                          name="gg"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-ChGS"
                          name="gr"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-ChGS"
                          name="mc"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-ChGS"
                          name="ps"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-ChGS"
                          name="SeR"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-ChGS"
                          name="SyF"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-ChGS"
                          name="IST"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-ChGS"
                          name="Top"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-ChGS"
                          name="Foc"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-ChGS"
                          name="BOR"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-ChGS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-ChGS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-ChGS"
                          name="CS"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-ChGS"
                          name="fe"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-ChGS"
                          name="fg"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-ChGS"
                          name="fr"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-ChGS"
                          name="ltr"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-ChGS"
                          name="nt"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-UoPP"
                          name="ref"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-UoPP"
                          name="st"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-UoPP"
                          name="ts"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-UoPP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-UoPP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-UoPP"
                          name="mb"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-UoPP"
                          name="mp"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-UoPP"
                          name="ge"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-UoPP"
                          name="gg"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-UoPP"
                          name="gr"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-UoPP"
                          name="mc"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-UoPP"
                          name="ps"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-UoPP"
                          name="SeR"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-UoPP"
                          name="SyF"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-UoPP"
                          name="IST"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-UoPP"
                          name="Top"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-UoPP"
                          name="Foc"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-UoPP"
                          name="BOR"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-UoPP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-UoPP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-UoPP"
                          name="CS"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-UoPP"
                          name="fe"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-UoPP"
                          name="fg"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-UoPP"
                          name="fr"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-UoPP"
                          name="ltr"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-UoPP"
                          name="nt"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
